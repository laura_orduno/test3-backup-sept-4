/*
###########################################################################
# File..................: SMB_Agreement_Bundle_OrderRequest
# Version...............: 1
# Created by............: Vinay Sharma
# Created Date..........: 04-Oct-2013
# Last Modified by......: 
# Description...........: Trigger fires of Agreement status as registered.
                            See Helper Class SMB_Agreement_Bundle_OrderRequest_Helper for more details
# Change Log:               
############################################################################
*/
trigger SMB_Agreement_Bundle_OrderRequest on Apttus__APTS_Agreement__c (before update) 
{
    //SMB_Agreement_Bundle_OrderRequest_Helper trgHelper = new SMB_Agreement_Bundle_OrderRequest_Helper();
    //trgHelper.Initialize(trigger.new,trigger.oldMap);

}