trigger trac_Prevent_ReadWrite_Team_Member on Product_Sales_Team__c (before insert, before update) {

    // Restricts the creation of PIT member records with Read/Write access to the opportunity owner.

    List<Id> opiIds = new List<Id>();
    List<Id> userIds = new List<Id>();
    for (Product_Sales_Team__c pit : trigger.new) {
        opiIds.add(pit.Product__c);
        userIds.add(pit.Member__c);
    }
    Map<Id, Opp_Product_Item__c> opiMap = new Map<Id, Opp_Product_Item__c>([Select Id, Opportunity__c, Opportunity__r.OwnerId From Opp_Product_Item__c Where Id In :opiIds]);
    List<Id> oppIds = new List<Id>();
    for (Opp_Product_Item__c opi : opiMap.values()) {
        oppIds.add(opi.Opportunity__c);
    }
    Map<Id, Map<Id, OpportunityTeamMember>> oppToTeamToTeamMemberMap = new Map<Id, Map<Id, OpportunityTeamMember>>();
    OpportunityTeamMember[] oppTeamMembers = [Select UserId, OpportunityId, TeamMemberRole, OpportunityAccessLevel From OpportunityTeamMember Where OpportunityId in :oppIds and UserId in :userIds];
    for (OpportunityTeamMember otm : oppTeamMembers) {
        if (oppToTeamToTeamMemberMap.get(otm.OpportunityId) == null) {
            oppToTeamToTeamMemberMap.put(otm.OpportunityId, new Map<Id, OpportunityTeamMember>());
        }
        Map<Id, OpportunityTeamMember> otmm = oppToTeamToTeamMemberMap.get(otm.OpportunityId);
        otmm.put(otm.UserId, otm);
    }
    Map<Id, OpportunityTeamMember> modifiedTeamMembers = new Map<Id, OpportunityTeamMember>();
    for (Product_Sales_Team__c pit : trigger.new) {
        Opp_Product_Item__c opi = opiMap.get(pit.Product__c);
        Map<Id, OpportunityTeamMember> otm = oppToTeamToTeamMemberMap.get(opi.Opportunity__c);
        if (otm == null || otm.get(pit.Member__c) == null) {
            if (UserInfo.getUserId() != opi.Opportunity__r.OwnerId) { pit.Access__c = 'Read'; }
        } else {
            OpportunityTeamMember otmm = otm.get(pit.Member__c);
            if (otmm.OpportunityAccessLevel != 'Read/Write' && otmm.OpportunityAccessLevel != 'Edit' && otmm.OpportunityAccessLevel != 'All') {
                if (UserInfo.getUserId() != opi.Opportunity__r.OwnerId) { pit.Access__c = 'Read'; }
            } else {
                pit.Access__c = 'Read/Write';
            }
            if (otmm.TeamMemberRole != null && pit.Team_Role__c != null && otmm.TeamMemberRole != pit.Team_Role__c) {
                otmm.TeamMemberRole = pit.Team_Role__c;
                modifiedTeamMembers.put(otmm.Id, otmm);
            }
        }
    }
    if (modifiedTeamMembers.size() > 0) {
        update modifiedTeamMembers.values();
    }
}