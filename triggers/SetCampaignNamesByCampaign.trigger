/**
 * Updates Campaign Names field in Condition records that reference camapigns processed
 * in by this trigger.
 * 
 *  @author Max Rudman
 *  @since 4/5/2009
 */
trigger SetCampaignNamesByCampaign on Campaign (after update) {
	// Build the map of changed camapigns
	Map<String,Campaign> changedById = new Map<String,Campaign>();
	for (Campaign campaign : Trigger.new) {
		if (Trigger.isUpdate) {
			Campaign old = Trigger.oldMap.get(campaign.Id);
			if ((campaign.Name == null) || (campaign.Name == old.Name)) {
				continue;
			}
			String cid = campaign.Id;
			if (cid.length() == 18) {
				cid = cid.substring(0,15);
			}
			changedById.put(cid,campaign);
		}
	}
	
	if (changedById.size() > 0) {
		// Load all conditions that reference campaign IDs
		List<ADRA__Condition__c> conditions = [SELECT Campaign_Names__c, ADRA__Value__c FROM ADRA__Condition__c WHERE ADRA__Value__c LIKE '%701%'];
		Set<String> otherCampaignIds = new Set<String>();
		for (ADRA__Condition__c cond : conditions) {
			if (cond.ADRA__Value__c != null) {
				String[] ids = cond.ADRA__Value__c.split(',');
				for (String id : ids) {
					id = id.trim();
					if (id.length() == 18) {
						id = id.substring(0,15);
					}
					if (!changedById.containsKey(id)) {
						otherCampaignIds.add(id);
					}
				}
			}
		}
		
		// Load and index other campaigns
		Map<Id,Campaign> othersById = new Map<Id,Campaign>([SELECT Id, Name FROM Campaign WHERE Id IN :otherCampaignIds]);
		
		// Bathc of Condition updates
		List<ADRA__Condition__c> batch = new List<ADRA__Condition__c>();
		
		// Now, regenerate Campaign Names in affected Conditions
		for (ADRA__Condition__c cond : conditions) {
			for (String campaignId : changedById.keySet()) {
				if (cond.ADRA__Value__c.contains(campaignId)) {
					// This condition is affected by campaign name change
					String[] ids = cond.ADRA__Value__c.split(',');
					String names = '';
					for (String id : ids) {
						id = id.trim();
						if (id.startsWith('701')) {
							if (id.length() == 18) {
								id = id.substring(0,15);
							}
							Campaign campaign = changedById.get(id);
							if (campaign == null) {
								// Try "other campaigns" index
								campaign = othersById.get((Id)id);
							}
							if (campaign != null) {
								if (names != '') {
									names += ',';
								}
								names += campaign.Name;
							}
						}
					}
					if (cond.Campaign_Names__c != names) {
						cond.Campaign_Names__c = names;
						batch.add(cond);
					}
				}
			}
		}
		
		update batch;
	}
}