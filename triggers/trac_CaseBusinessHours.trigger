trigger trac_CaseBusinessHours on SMET_Request__c (before insert, before update) {
   
    User myU = [SELECT Id, Business_Hours__c FROM User WHERE Id  = :UserInfo.getUserId()];
    
    if (Trigger.isInsert) {
        
        for (SMET_Request__c updatedCase :trigger.new) {
            
            updatedCase.Last_Status_Change__c = System.now();
            updatedCase.Time_With_Customer__c = 0;
            updatedCase.Time_With_Support__c = 0;
            updatedCase.Time_in_New_Status__c = 0;
            updatedCase.Time_In_Re_Open_Status__c = 0;
        }        
    } 
    else {
                                	
        BusinessHours defaultHours = new BusinessHours();
        
        if(myU.Business_Hours__c != null)
            defaultHours = [select Id from BusinessHours where Name = :myU.Business_Hours__c];
        else
            defaultHours = [select Id from BusinessHours where IsDefault=true];
        

        //Get New Statuses
        Set<String> newStatusSet = new Set<String>();
        
        newStatusSet.add('New');
        
        //Get Re-Opened Statuses
        Set<String> ReOpenStatusSet = new Set<String>();
        
        ReOpenStatusSet.Add('Re-Opened');
        
        //Get Customer Statuses
        Set<String> customerStatusSet = new Set<String>();
        
        customerStatusSet.add('Awaiting Customer');
        customerStatusSet.add('Ready to Test (Quote Only)');
        customerStatusSet.add('Deployment Complete (Quote Only)');

        //Get Support Statuses
        Set<String> supportStatusSet = new Set<String>();
        
        supportStatusSet.add('In Progress');
        supportStatusSet.add('Requires Follow up (Quote Only)');
        supportStatusSet.add('Ready to Deploy (Quote Only)');


        //Get Closed Statuses 
        Set<String> closedStatusSet = new Set<String>();
        
        closedStatusSet.add('Completed');
        closedStatusSet.add('Cancelled');
        closedStatusSet.add('Submitted as Requirement');
        

        //For any case where the status is changed, recalc the business hours in the buckets
        for (SMET_Request__c updatedCase:System.Trigger.new) {
                        
	        if(updatedCase.Time_With_Customer__c == null)
	        	updatedCase.Time_With_Customer__c = 0;
	        	
	        if(updatedCase.Time_With_Support__c == null)
	        	updatedCase.Time_With_Support__c = 0;
	        	
	        if(updatedCase.Time_in_New_Status__c == null)
	        	updatedCase.Time_in_New_Status__c = 0;
	        	
	        if(updatedCase.Time_In_Re_Open_Status__c == null)
	        	updatedCase.Time_In_Re_Open_Status__c = 0;
            
            SMET_Request__c oldCase = System.Trigger.oldMap.get(updatedCase.Id);

            if (oldCase.Status__c != updatedCase.Status__c && updatedCase.Last_Status_Change__c != null) {
                                    
                //OK, the status has changed
                if (!oldCase.IsClosed__c) {
                    //We only update the buckets for open cases

                    //On the off-chance that the business hours on the case are null, use the default ones instead
                    Id hoursToUse = defaultHours.Id;

                    //The diff method comes back in milliseconds, so we divide by 3600000 to get hours.
                    Double timeSinceLastStatus = BusinessHours.diff(hoursToUse, updatedCase.Last_Status_Change__c, System.now()) / 3600000.0;

                    //We decide which bucket to add it to based on whether it was in a stop status before
                    if(newStatusSet.contains(oldCase.Status__c)) {
                        updatedCase.Time_In_New_Status__c += timeSinceLastStatus;     
                    }               
                    else if(ReOpenStatusSet.contains(oldCase.Status__c)) {
                        updatedCase.Time_In_Re_Open_Status__c += timeSinceLastStatus;
                        updatedCase.Time_With_Customer__c += timeSinceLastStatus;
                    } 
                    else if(customerStatusSet.contains(oldCase.Status__c)) {
                        updatedCase.Time_With_Customer__c += timeSinceLastStatus;
                    } 
                    else if(supportStatusSet.contains(oldCase.Status__c)) {
                        updatedCase.Time_With_Support__c += timeSinceLastStatus;
                    }

                    //if(closedStatusSet.contains(updatedCase.Status__c)) {
                        updatedCase.Case_Age_In_Business_Hours__c = 
                            updatedCase.Time_With_Customer__c + updatedCase.Time_With_Support__c + updatedCase.Time_in_New_Status__c;
                    //}                   
                }

                updatedCase.Last_Status_Change__c = System.now();
            }
        }
    }
}