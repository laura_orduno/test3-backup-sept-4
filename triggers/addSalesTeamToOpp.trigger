trigger addSalesTeamToOpp on Product_Sales_Team__c (after insert) { //after update

    public final Map<String, String> ACCESS_LEVELS = new Map<String, String> {'Read' => 'Read',
        'Read/Write' => 'Edit'};
    public final String DEFAULT_ACCESS = 'Read';
    public final String OWNER_ACCESS = 'Owner';
        
    List<String> pliList = new List<String>();
    for (Product_Sales_Team__c pst : trigger.new) {
        pliList.add(pst.Product__c);
    }

    //get the opps for the plis
    Map<Id, Opp_Product_Item__c> pliMap 
        = new Map<Id, Opp_Product_Item__c>([Select Id, Opportunity__c from Opp_Product_Item__c where Id In :pliList]);
    System.debug('pliMap = ' + pliMap);
    Set<Id> opportunityIdSet = new Set<Id>();
    if(pliMap != null && !pliMap.isEmpty()) {
    	for (Opp_Product_Item__c oppProductItem : pliMap.values()) {
    		opportunityIdSet.add(oppProductItem.Opportunity__c);
    	}
    }
     Map<Id, Opportunity> opportunityMap 
        = new Map<Id, Opportunity>([Select Id,OwnerId,Name From Opportunity  where Id IN : opportunityIdSet]);

    List<OpportunityTeamMember> otmList = new List<OpportunityTeamMember>();
    List<OpportunityShare> oShareList = new List<OpportunityShare>();

    for (Product_Sales_Team__c pst : trigger.new) {
        System.debug('pst.Product__c = ' + pst.Product__c);
        Opp_Product_Item__c pli = pliMap.get(pst.Product__c);
        if (pli == null) {
            continue;
        }
        Id oppId = pli.Opportunity__c;
        boolean isOwner = false;
        if (opportunityMap != null && opportunityMap.containsKey(oppId)) {
        	if (opportunityMap.get(oppId).OwnerId == pst.Member__c) {
        		isOwner = true;
        	}
        }
        System.debug('pst.Team_Role__c----------->'+ pst.Team_Role__c);
        OpportunityTeamMember  otm = new OpportunityTeamMember(
	            OpportunityId = oppId,
	            UserId = pst.Member__c, 
	            TeamMemberRole = pst.Team_Role__c
	    );
	    /*
        if (isOwner) {
        	otm = new OpportunityTeamMember(
	            OpportunityId = oppId,
	            UserId = pst.Member__c, 
	            TeamMemberRole = pst.Team_Role__c
	            //TeamMemberRole = pst.Team_Role__c,
	            //OpportunityAccessLevel = OWNER_ACCESS
	        );
        } else {
	        otm = new OpportunityTeamMember(
	            OpportunityId = oppId,
	            UserId = pst.Member__c, 
	            TeamMemberRole = pst.Team_Role__c
	        );
        }
        */
        otmList.add(otm);

        //CREate shres for access level,
        String accessLevel = DEFAULT_ACCESS;
        if (pst.Access__c != null) {
            accessLevel = ACCESS_LEVELS.get(pst.Access__c);
        }
        System.debug('accessLevel----------->'+ accessLevel);
        if (!isOwner) {
        	
        
            
        OpportunityShare oShare = new OpportunityShare(OpportunityId =   oppId,
                                            UserorGroupId = pst.Member__c,
                                            OpportunityAccessLevel = accessLevel);  
                                                
        oShareList.add(oShare);
        }
            
    }
    System.debug('otmList = ' + otmList);
    System.debug('oShareList = ' + oShareList);

    try {
    	System.debug('Inserting otmList = ' + otmList);
        if (! otmList.isEmpty()) {
            insert otmList;
        }
        System.debug('Done Inserting otmList = ' + otmList);
        System.debug('Done Inserting oShareList = ' + oShareList);
        if (! oShareList.isEmpty()) {
            insert oShareList;
        }
        System.debug('Done Inserting oShareList = ' + oShareList);
    } catch (Exception e) {
        trigger.new.get(0).addError(e.getMessage());
    }

        /*if (trigger.isDelete) {
            Set<Id> deletedPst = trigger.oldMap.keySet();
            List<Opportunity_Sales_Team__c> ostList = [Select Id from Opportunity_Sales_Team__c where Product_Sales_Team__c In :deletedPst];
            if (! isEmpty(ostList)) {
                delete ostList;
            }
        }
        
        if (trigger.isUpdate) {
            Set<Id> updatedPst = trigger.newMap.keySet();
            List<Opportunity_Sales_Team__c> ostList = [Select Id from Opportunity_Sales_Team__c where Product_Sales_Team__c In :deletedPst];
            Map<Id, Opportunity_Sales_Team__c> pst_ost_Map = new Map<Id, Opportunity_Sales_Team__c>();
            for (Opportunity_Sales_Team__c ost : ostList) {
                pst_ost_Map.put(ost.Product_Sales_Team__c, ost);
            }
            
            for (Product_Sales_Team__c pst : trigger.new) {
                OpportunityLineItem pli = pliMap.get(pst.OpportunityLineItem__c);
                Id oppId = pli.Opportunity;
                Opportunity_Sales_Team__c ost = pst_ost_Map.get(ps.Id);
                ostList.add(ost);
            }
        }*/

}