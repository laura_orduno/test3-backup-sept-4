trigger SMB_OpportunityTrigger on Opportunity(before insert,after insert,before update,after update,before delete, after delete){
    //CreditTriggerHandler handler=new CreditTriggerHandler();
    if(trigger.isafter&&trigger.isupdate){
        String OppStages;
        if (Test.isRunningTest()) {
            OppStages ='Contract Acceptance Expired';
        }
        else
        { 
            OppStages= SMB_Order_Stage_for_WFM_Cancel__c.getInstance('Opportunity Stage').Data__c;
        }
        String[] arrOppStages = OppStages.split(',');
        set<Id> setOppID = new set<Id>();
        
        //get all opportunities that are modified
        for (Opportunity opp : trigger.new ) {
            system.Debug('Inside trigger new loop');
            system.Debug('arrOppStages : '+arrOppStages );
            if(opp.stageName !=  trigger.OldMap.get(opp.id).StageName) {
                for ( String stage : arrOppStages )
                {
                    system.Debug('Inside String Loop');
                    if( opp.stageName == stage ) {
                        System.Debug('StageName is in the list');
                        setOppID.add(opp.id);
                    }
                }
            }
        }
        
        if (setOppID.size() > 0 )
        {
            SMB_WebserviceHelper.CancelWorkOrder(setOppID,true);
        }
        
        /* Code Changes for BO enhancement FR803101 by Amit Rana Dt 16 Oct 2015*/
        // Code changes start
        try
        {
            if((SMB_Helper.Activate_AOO_To_NC) && (Trigger.IsUpdate) && (Trigger.IsAfter))
            {  
                // Loop through all the changed opportunities and send the AOO order for those opportunities that 
                // have a valid stage according to the custom setting values in SMB_Opportunity_Status_Mapping__c.
                List<ID> AOO_Order = new List<ID>();
                for (Opportunity objOpp : trigger.new)
                {
                    // Defect# -53113 - Sandip - 05 Feb 2016 - Removed && String.isBlank(objOpp.Related_Orders__c condition from below if statement,
                    //                                       - this was added to fix other defects but now its not require.
                    // Sandip - 11 Feb 2016 - Removed 'OpptyStatus.Contains(objOpp.StageName)' condition from below if statement, 
                    //                      - Moved the same logic in CreditAssessmentEncryptedFields trigger
                    if(String.isNotBlank(objOpp.CAR_Status__c)) 
                    {
                        AOO_Order.Add(SMB_Helper.CreateOrderforCarStatusChange(objOpp.id));
                    }
                }
                for(Id AOO_OBJ:AOO_Order)
                {
                    smb_OrderSubmit_Callout_Helper.SubmitOrderRequest(AOO_OBJ);
                }
            }
        }
        catch(exception e)
        {
            try
            {
                Webservice_Integration_Error_Log__c log=new Webservice_Integration_Error_Log__c(apex_class_and_method__c='SMB_OpportunityTrigger:Trigger',external_system_name__c='NC',webservice_name__c='SubmitOrder',object_name__c='Opportunity');
                log.error_code_and_message__c+=e.getmessage();
                log.stack_trace__c=e.getstacktracestring();
                log.error_code_and_message__c=(string.isnotblank(log.error_code_and_message__c)&&log.error_code_and_message__c.length()>32767?log.error_code_and_message__c.substring(0,32767):log.error_code_and_message__c);
                log.stack_trace__c=(string.isnotblank(log.stack_trace__c)&&log.stack_trace__c.length()>32767?log.stack_trace__c.substring(0,32767):log.stack_trace__c);
                insert log;
            }
            catch(exception ex)
            {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                List<String> lstEmail = new List<String>(); //use single custom setting for all data
                SMBCare_WebServices__c wsEmailAddress = SMBCare_WebServices__c.getValues('EmailAddress');
                string htmlBody = '<b>SMB_Helper.CreateOrderforCarStatusChange</b><br/>'+
                    '<b>------------------------------------------------------</b><br/>'+
                    '<b>===========================================================</b><br/>'+
                    '<b>An error was encoutered during the send of a CAR state change notification to the Back office.</b><br/><br/>'+
                    ex.getmessage()+'<br/><br/>Stack Trace:<br/>'+ex.getstacktracestring()+'<br/>'+
                    '<b> ---------------------------------------------</b><br/>';
                string eMail='andy.leung@telus.com';
                if (String.isNotBlank(wsEmailAddress.Value__c))
                {
                    eMail= wsEmailAddress.Value__c;
                }
                mail.setToAddresses(eMail.split(',', 0));
                mail.setSenderDisplayName('noreply@salesforce.com');
                mail.setReplyTo('noreply@salesforce.com');
                mail.setSubject('An error was encoutered during the send of a CAR state change notification to the Back office.');
                mail.setHtmlBody(htmlBody);
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }
        }
        //Code changes end for BO enhancement FR803101
    }
}