/**
*trac_S2S_UpdateCaseToRC
*@description This trigger will call the trac_CaseTriggerDispatcher class that extends the dispatcher in order to send cases to ringcentral via Salesforce to Salesforce.
*@authoer David Barrera
*@Date 8/28/14
*/
trigger trac_S2S_UpdateCaseToRC on Case (before insert,before update,after insert,after update) {
	trac_CaseTriggerDispatcher dis = new trac_CaseTriggerDispatcher();
	dis.init();
	dis.execute();
	dis.finish();

}