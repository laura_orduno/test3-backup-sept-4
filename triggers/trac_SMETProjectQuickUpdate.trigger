// "Quick Adds" Status Update related list items based on text in the New_Update__c field.
trigger trac_SMETProjectQuickUpdate on SMET_Project__c (before update) {
	List<SMET_Update__c> updates = new List<SMET_Update__c>();
	
	for (SMET_Project__c p : trigger.new) {
		if (p.New_Update__c != null) {
			updates.add(new SMET_Update__c(SMET_Project__c = p.id, Update_Note__c = p.New_Update__c, Date__c = Date.today()));
			p.New_Update__c = null;
		}
	}
	
	if (updates.size() > 0) {
		insert updates;
	}
}