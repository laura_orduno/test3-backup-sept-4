trigger trac_ChatterGroupHashTags on FeedItem (before insert) {

	Map<String,Set<String>> groupToTags = new Map<String,Set<String>>();
	for (Chatter_Hash_Groups__c chg : [SELECT Name, Group_Name__c FROM Chatter_Hash_Groups__c]) {
		if(!groupToTags.containsKey(chg.group_name__c)) {
			groupToTags.put(chg.group_name__c,new Set<String>());
		}
		
		groupToTags.get(chg.group_name__c).add(chg.name);
	}
	Map<String,Id> tagToGroupMap = new Map<String,Id>();
	for(CollaborationGroup cg : [SELECT name FROM CollaborationGroup WHERE name IN :groupToTags.keySet()]) {
		if (groupToTags.containsKey(cg.name)) {
			for(String tag : groupToTags.get(cg.name)) {
				tagToGroupMap.put(tag.toLowerCase(),cg.id);
			}
		}
	}
	
	if (tagToGroupMap.size() > 0) {
		FeedItem[] clonedFIs = new List<FeedItem>();
		FeedItem clonedFI;
		
		for(FeedItem fi : trigger.new) {
			for(String tag : tagToGroupMap.keySet()) {
				if(fi.body != null && fi.body.toLowerCase().contains('#' + tag) && fi.parentid != tagToGroupMap.get(tag)) { // prevent loop by ensuring post is not already in the group
					clonedFI = fi.clone(false,true,false,false);
					clonedFI.parentid = tagToGroupMap.get(tag);
					clonedFIs.add(clonedFI);
				}
			}
		}
		
		try {
			Database.SaveResult[] results = Database.insert(clonedFIs);
		} catch (DMLException de) {
			// group access rights related issue
		}
	}
}