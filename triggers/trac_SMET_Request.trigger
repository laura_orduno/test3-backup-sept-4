trigger trac_SMET_Request on SMET_Request__c (before insert) {
    Set<Integer> offsets = new Set<Integer>();
    Set<String> emails = new Set<String>();
    Map<String, Id> emailUserId = new Map<String, Id>();
    for (Integer i = 0; i < trigger.new.size(); i++) {
        String ownerId = (String) trigger.new[i].OwnerId;
        if (ownerId.substring(0,3) == '00G') {
            if (trigger.new[i].Email_From_Address__c != null) {
                offsets.add(i);
                emails.add(trigger.new[i].Email_From_Address__c);
            } else {
                // This one's questionable: if it's owned by a group, why not just leave it alone?
                trigger.new[i].OwnerId = UserInfo.getUserID();
            }
        }
    }
    if (emails.size() == 0) {
        return;
    }
    User[] users = [SELECT Email FROM User WHERE Email IN :emails];
    if (users != null) {
        for (User u : users) {
            emailUserId.put(u.Email, u.Id);
        }
    }
    for (Integer n : Offsets) {
        // Traction: Added if condition to resolve empty-owner exceptions. June 6/12 Alex
        Id userId = emailUserId.get(trigger.new[n].Email_From_Address__c);
        if (userId != null) {
            trigger.new[n].OwnerId = userId;
        }
    }
}