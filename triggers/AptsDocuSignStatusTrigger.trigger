/**
    when docusign enveloped status is Voided and voided reason is Envelope has expired, Apttus agreement status needs to be updated to Expired.  
*/
trigger AptsDocuSignStatusTrigger on dsfs__DocuSign_Status__c (before update,after update) {
    private static String AGREEMENT_STATUS_ACCEPTANCE_EXPIRED = 'Acceptance Expired';
    private static String AGREEMENT_STATUS_CATEGORY_IN_SIGNATURES = 'In Signatures';
    private static String DOCUSIGN_ENVELOPE_STATUS_VOIDED = 'Voided';
    private static String DOCUSIGN_ENVELOPE_STATUS_EXPIRED = 'Envelope has expired.';
     /*
    if(Trigger.isBefore) {
        if(Trigger.isUpdate) {
            set<Id> agreementIds = new set<Id>();
            for(dsfs__DocuSign_Status__c docuSignStatus : Trigger.New) {
                //envelope status = voided and voided reason = Envelope has expired, expire the agreement.  Also, this was not the status earlier, 
                //so that it happens only when the status changes from something else to Voided and Envelope has expired.
                if(string.isnotblank(docuSignStatus.Apttus_DocuSign__Apttus_Agreement_ID__c)){
                    if(docuSignStatus.dsfs__Envelope_Status__c == DOCUSIGN_ENVELOPE_STATUS_VOIDED && docuSignStatus.dsfs__Voided_Reason__c == DOCUSIGN_ENVELOPE_STATUS_EXPIRED) {
                        if(Trigger.oldMap.get(docuSignStatus.id) != null && Trigger.oldMap.get(docuSignStatus.id).dsfs__Envelope_Status__c != DOCUSIGN_ENVELOPE_STATUS_VOIDED
                           && Trigger.oldMap.get(docuSignStatus.id) != null && Trigger.oldMap.get(docuSignStatus.id).dsfs__Voided_Reason__c != DOCUSIGN_ENVELOPE_STATUS_EXPIRED) {
                               agreementIds.add(docuSignStatus.Apttus_DocuSign__Apttus_Agreement_ID__c);                        	
                           }
                    }
                }
            }
            if(agreementIds != null && agreementIds.size() > 0) {
                list<Apttus__APTS_Agreement__c> expiredAgreementList = new list<Apttus__APTS_Agreement__c>();
                for(Id agreementId : agreementIds) {
                    //update agreement status to expired
                    Apttus__APTS_Agreement__c agreement = new Apttus__APTS_Agreement__c(Id = agreementId);
                    agreement.Apttus__Status_Category__c = AGREEMENT_STATUS_CATEGORY_IN_SIGNATURES;
                    agreement.Apttus__Status__c = AGREEMENT_STATUS_ACCEPTANCE_EXPIRED;
                    
                    expiredAgreementList.add(agreement);
                }
                
                if(expiredAgreementList != null && expiredAgreementList.size() > 0) {
                    update expiredAgreementList;
                }
            }
        }
    }else
*/if(trigger.isafter){
        if(trigger.isupdate){
            list<contract> updateContracts=new list<contract>();
            map<string,string> contractStatusMap=new map<string,string>{'Sent'=>'Contract Sent','Completed'=>'Contract Accepted','Declined'=>'Client Declined','Voided'=>'Acceptance Expired'};
            for(sobject record:trigger.new) {
                dsfs__docuSign_status__c newStatus=(dsfs__docuSign_status__c)record;
                dsfs__docuSign_status__c oldStatus=(dsfs__docuSign_status__c)trigger.oldmap.get(newStatus.id);
                if(string.isnotblank(newStatus.dsfs__contract__c)){
                    contract updateContract=new contract(id=newStatus.dsfs__contract__c);
                    if(contractStatusMap.containskey(newStatus.dsfs__envelope_status__c)){
                        updateContract.status=contractStatusMap.get(newStatus.dsfs__envelope_status__c);
                    }
                    updateContracts.add(updateContract);
                }
            }
            if(!updateContracts.isempty()){
                try{
                    update updateContracts;
                }catch(dmlexception dmle){
                    if(!dmle.getmessage().containsignorecase('Invalid state transition from Cancelled to Acceptance Expired for Contract')){
                        util.emailSystemError(dmle);
                    }
                }catch(exception e){
                    util.emailSystemError(e);
                }
            }
        }
    }
}