/*
###########################################################################
# File..................: SMB_SetSalesPerson
# Version...............: 29
# Created by............: Puneet Khosla
# Created Date..........: 30-Jan-2014
# Last Modified by......: 
# Last Modified Date....: 
# Description...........: Setting Salesperson in SMB Care Opportunities
# Copyright (c) 2000-2012. Astadia, Inc. All Rights Reserved.
#
# Created by the Astadia, Inc. Modification must retain the above copyright notice. 
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any commercial purpose, without fee, and without a written
# agreement from Astadia, Inc., is hereby forbidden. Any modification to source
# code, must include this paragraph and copyright.
#
# Permission is not granted to anyone to use this software for commercial uses.
#
# Contact address: 2839 Paces Ferry Road, Suite 350, Atlanta, GA 30339
# Company URL : http://www.astadia.com
###########################################################################
*/
trigger SMB_SetSalesPerson on Opportunity (before insert)
{
    if(trigger.isBefore && trigger.isInsert)
    {
        try
        {
            set<Id> oppSMBRTIdSet=SMB_Helper.fetchOpportunitySMBCareRecordTypes(null);
            /*oppSMBRTList =  [select id from RecordType where (sobjectType = 'Opportunity' and name Like 'SMB Care%')];
            set<id> oppSMBRTIdSet = new set<id>();
            for(RecordType rt : oppSMBRTList)
                oppSMBRTIdSet.add(rt.Id);
        */
            for(Opportunity opp : trigger.new)
            {
                try
                {
                    if(opp.Salesperson__c == null && opp.OwnerId != null && oppSMBRTIdSet.contains(opp.RecordTypeId))
                        opp.Salesperson__c = opp.OwnerId;
                }
                catch(Exception e)
                {
                    System.debug('### Error in SMB_SetSalesPerson : ' + e);
                }
            }
        }
        catch(Exception oe)
        {
            System.debug('### Error in SMB_SetSalesPerson : ' + oe);
        }
        
    }
}