/*****************************************************
    Name: ExternalETLAuditLog
    Usage: This trigger expects the only one record to be inserted  at a time. 
          Trigger will execute the batch job for respetive action value
    Author – Sandip Chaudhari (IBM)
    Revision History -
    Created By/Date - Sandip Chaudhari / 08 Dec 2016
    
    Updated By/Date/Reason for Change - 
    
******************************************************/
trigger ExternalETLAuditLog on External_ETL_Audit_Log__c (after insert){
    if(Trigger.IsInsert && Trigger.IsAfter){
        ExternalAuditLogHelper.executeJob (Trigger.New);
    }
}