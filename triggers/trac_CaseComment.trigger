trigger trac_CaseComment on CaseComment (before insert,before update, after insert, after update) {
	
	// We should use this by default for all new classes in this trigger handler.
    trac_TriggerHandlerBase.triggerHandler(new trac_CaseComment());
}