/*
###########################################################################
# File..................: smb_AgreementLineItemTrigger
# Version...............: 1
# Created by............: Puneet Khosla
# Created Date..........: 18-Dec-2013
# Last Modified by......: 
# Last Modified Date....: 
# Description...........: This Trigger contains the methods for Agreement Line Item
# Change Log:               
############################################################################
*/
trigger smb_AgreementLineItemTrigger on Apttus__AgreementLineItem__c (after insert) 
{
    if(Trigger.isAfter)
    {
        if(Trigger.isInsert)
        {
            try
            {
                smb_AgreementLineItemTrigger_Helper.updateALI(trigger.new);
                if(test.isRunningTest())
                {
                    id temp = Id.valueOf('abc');
                }
            }
            catch(Exception e)
            {
                System.debug('### Exception in smb_AgreementLineItemTrigger : ' + e);
            }
        }
    }
}