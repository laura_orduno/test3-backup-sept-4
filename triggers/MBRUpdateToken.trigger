/**
 *  MBRUpdateToken.trigger
 *
 *  @description
 *      Trigger to create registration validation tokens for the My Business Requests Community. Copied, not Created by Dan.
 *
 *  @date - 05/11/2014
 *  @author - Jimmy Hsiao
 *  @author - Dan Reich
 */
trigger MBRUpdateToken on Contact ( after insert, after update ) {
    Customer_Interface_Settings__c cis = Customer_Interface_Settings__c.getInstance();
    List<Contact> conUpdate = new List<Contact>();
    if(!Test.isrunningtest()){
        system.assert(cis!=null, 'Failed to load Customer Interface Custom Settings');
        system.assert(cis.HMAC_Digest_Key__c!=null, 'Failed to load Customer Interface Custom Setting HMAC Digest Key');
    }
    for(Contact con : trigger.new) {
        if(con.Token__c == null || con.Token__c == '') {
            Blob digestBlob;
            if(!Test.isrunningtest()) {
                digestBlob = Crypto.generateMac('hmacSHA512', Blob.valueOf(con.Id), EncodingUtil.base64decode(cis.HMAC_Digest_Key__c));
            }
            else {
                digestBlob = Crypto.generateMac('hmacSHA512', Blob.valueOf(con.Id), EncodingUtil.base64decode('/eYPmJ0eXTb+8R/X7YE9Skv6Ly8tfChkR0gdWt9ngXg='));
            }
            System.assert(digestBlob != null, 'Failed to generate token for ' + con.Id);
            String token = EncodingUtil.base64Encode(digestBlob);
            System.assert(token != null, 'Failed to encode token for ' + con.Id);
            conUpdate.add(new contact(id=con.id, Token__c = token, Token_Encoded__c = EncodingUtil.urlEncode(token, 'UTF-8')));
        }
    }
    update conUpdate;
}