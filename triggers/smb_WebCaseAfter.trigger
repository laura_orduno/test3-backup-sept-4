/*
###########################################################################
# File..................: smb_WebCaseAfter
# Version...............: 26
# Created by............: Puneet Khosla
# Created Date..........: 07-Nov-2013
# Last Modified by......: 
# Last Modified Date....: 
# Description...........: Only to be run for Web to Case
###########################################################################
*/
trigger smb_WebCaseAfter on Case (before insert, after insert) 
{
    if(trigger.isInsert && trigger.isBefore)
    {
        try
        {
            smb_WebCaseAfterClass.chooseContact(trigger.new);
            if(Test.isRunningTest())
            {
            	// throw an exception
            	Id invalidId = Id.ValueOf('Telus');
            }
        }
        catch(Exception e)
        {
            system.debug('### Exception smb_WebCaseAfter Before Insert : ' + e.getMessage());
        }
    }
    if(trigger.isInsert && trigger.isAfter)
    {
        try
        {
            smb_WebCaseAfterClass.runAssignmentRule(trigger.new);
            if(Test.isRunningTest())
            {
            	// throw an exception
            	Id invalidId = Id.ValueOf('Telus');
            }
        }
        catch(Exception e)
        {
            system.debug('### Exception smb_WebCaseAfter After Insert : ' + e.getMessage());
        }
        
    }
}