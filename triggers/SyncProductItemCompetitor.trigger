/*****************************************************
    Trigger Name: SyncProductItemCompetitor 
    Usage: This trigger to sync the Opportunity Product Item records with the
           Competitor object
    Author – Clear Task    
    Date – 2/28/2011       
    Revision History
******************************************************/
trigger SyncProductItemCompetitor on Opp_Product_Item__c (after insert, after update, after delete){
    SyncProductItemCompetitor obj = new SyncProductItemCompetitor();
    if(trigger.isInsert || trigger.isUpdate){   
        obj.doSyncProductItemCompetitor(trigger.new);
    }else if(trigger.isDelete){
        System.debug('Old PIC='+ trigger.old);
        List<Opp_Product_Item__c> opiList = new List<Opp_Product_Item__c>();
        for(Opp_Product_Item__c o :trigger.old){
            Opp_Product_Item__c opi = new Opp_Product_Item__c();
            opi.Opportunity__c = o.Opportunity__c;
            opi.Competitor__c = null;
            opiList.add(opi);
        }
        obj.doSyncProductItemCompetitor(opiList);
    }
}