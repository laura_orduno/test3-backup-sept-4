trigger trac_VendorCommunication on Vendor_Communication__c (before insert,before update, after insert,after update) {
	trac_VendorCommunicationDispatcher dis = new trac_VendorCommunicationDispatcher();
	dis.init();
	dis.execute();
	dis.finish();
}