// ************************Version Updates***********************************************************************
    //
    // Updated Date     Updated By      Update Comments 
    // 02/19/2015       Rahul Badal     Code to conditionally execute logic for simple or Complex order
    //                                  isSRSSR boolean value will be set to True for complex order       
    // 06/17/2016    David Yuan   Code added per SMBO-349 at Jira. The begining of the trigger to exclude some new field entries
    //                   for the trigger to fire
    //08/05/2016     Hongda Cheng  Reset escalation status per SMBO-517 in JIRA  
    // 13/09/2016  Adhir Parchure  ISD Bamboo: TC ISD 1780918 - BSE - Mandatory field coding
    //12-Sept-2017 Rajan Tatuskar Non-TQ TF Case 1904646, BR: S2CP-82 :: added by Rajan ( JIRA URL : https://jira.tsl.telus.com/browse/S2CP-82 )
    //                              Added code to insert Service_Request_Contact__c as a requesting customer after inserting SR.
    // **************************************************************************************************************
    trigger smb_ServiceRequestTrigger on Service_Request__c (after delete, after insert, after undelete, 
                                                             after update, before delete, before insert, before update) 
    {    
        // START - ISD Bamboo TC ISD 1780918 - BSE - Mandatory field coding - [Adhir P - 12-Sept-2016]
        Schema.DescribeSObjectResult describeSerR = Schema.SObjectType.Service_request__C;
        Map<Id,Schema.RecordTypeInfo> SRRecTMapById = describeSerR.getRecordTypeInfosById(); 
        
        if(Trigger.isBefore){          
            if(Trigger.isUpdate){
                for (Service_Request__c serviceRequest: trigger.New){
                    if(serviceRequest.recordtypeid!=null){
                        schema.recordtypeinfo info=SRRecTMapById.get(serviceRequest.recordtypeid);
                        
                        if((!info.getname().equalsignorecase('Prequal') && serviceRequest.recordtypeid!=trigger.oldmap.get(serviceRequest.id).recordtypeid && Servicerequest.Target_Date_Type__c!=null && Servicerequest.Target_Date_Type__c.equalsignorecase('Prequal Requested'))
                           || (info.getname().equalsignorecase('Prequal') && serviceRequest.recordtypeid!=trigger.oldmap.get(serviceRequest.id).recordtypeid && Servicerequest.Target_Date_Type__c!=null && !Servicerequest.Target_Date_Type__c.equalsignorecase('Prequal Requested'))){
                               servicerequest.adderror(system.label.SRS_TargetDateTypeError);
                           }
                    }
                }
            }
        }
        // END - ISD Bamboo TC ISD 1780918 - BSE - Mandatory field coding
      
        Boolean isGoForTrigger = false;   
        
        if(trigger.old!=null) 
        {   
            for (Service_Request__c  filterServiceRequest : trigger.old) 
            { 
                map<string,schema.sobjectfield> filterFieldMap = filterServiceRequest.getsobjecttype().getdescribe().fields.getmap();    
                
                for(String fieldName:filterFieldMap.keyset())
                {                
                    schema.describefieldresult fieldResult = filterFieldMap.get(fieldName).getdescribe();
                    
                    if(fieldResult.iscustom() && fieldResult.isaccessible() && fieldResult.isupdateable())
                    {     
                           if(trigger.new != null)
                           {   
                               for(sobject newRecord:trigger.new)
                               {                           
                                   sobject oldRecord = trigger.oldmap.get(newRecord.id);                                   
                                   Boolean isChangedOrNew = false;                                   
                                   String filterFieldName = fieldResult.getname();  
                                   Set<String> fieldPathSet = SRS_ServiceRequestValidationHelper.getExpediteFieldSet();
                                   Boolean canBeIgnore = fieldPathSet.contains(filterFieldName);   
                                   //if a cloned SR per GUI, it will be brand new and this trigger needs to be called.
                                   Boolean isBrandNew = oldRecord.get(fieldResult.getname())== null &&  newRecord.get(fieldResult.getname()) ==null;
                                   if(isBrandNew)
                                   {
                                       isChangedOrNew = true;
                                   }
                                   else
                                   {
                                       if(!canBeIgnore)
                                       {
                                            isChangedOrNew = oldRecord.get(fieldResult.getname()) != newRecord.get(fieldResult.getname()); 
                                       }                                      
                                   }
                                
                                  // System.debug('dyy smbo-349 isChangedOrNew = ' + isChangedOrNew +  ' for filterFieldName=' + filterFieldName);
                                   
                                   if(isChangedOrNew)
                                   {
                                       isGoForTrigger = true;
                                   } 
                                   
                                   if(isGoForTrigger)
                                   { 
                                       break;
                                   }                               
                               }                        
                           }
                       }
                    
                    if(isGoForTrigger)
                    {
                        break;
                    }                 
                }
            }
        } 
        else
        {
            isGoForTrigger = true;
        }
        
    //  System.debug('... smb_ServiceRequestTrigger ... SMBO-349... isGoForTrigger = ' + isGoForTrigger);
        
        if(isGoForTrigger)
        { 
           if(Trigger.isUpdate)
           {          
                Boolean ersResult = SRS_ServiceRequestValidationHelper.resetEscalationStatus(Trigger.old, Trigger.New); 
                System.debug('SMBO-517 EsCalation Resuest Status Result=' +  ersResult);
           }    
            
      //  System.debug('smb_ServiceRequestTrigger: ' + Trigger.isBefore + ', ' + Trigger.isAfter + ', ' + Trigger.isUpdate + ', ' + Trigger.isDelete);
       
        if(trigger.old != null)
        {
            Integer Counter = 0;
          //  System.debug('######_ON START');
            for (Service_Request__c OBJTEST : trigger.old)
            {
                Counter = Counter+1;
              //  System.debug('######_ID' + OBJTEST.id);
               // system.debug('######_' + Counter + '_TRIGGER.OLD Status__C: ' + OBJTEST.status__c);
            }
        }
            
        if(trigger.New != null)
        {
            Integer Counter = 0;
            System.debug('######_ON START');
            for (Service_Request__c OBJTEST : trigger.New)
            {
                Counter = Counter+1;
              //  System.debug('######_ID'+ OBJTEST.id);
             //   System.debug('######_'+ Counter + '_TRIGGER.NEW = Status__C: '+ OBJTEST.status__c);
            }
        }
       
    
         Set<Id> oppIdSet = new Set<Id>();
         set<string> SRSRecordTypes = new Set<String>{'Change', 'Disconnect', 'Prequal','Move','Provide/Install','Records Only'};
         Schema.DescribeSObjectResult describeSR = Schema.SObjectType.Service_request__C; 
         Map<Id,Schema.RecordTypeInfo> rtSRSMapById = describeSR.getRecordTypeInfosById();
         //R3 SRS specific code 
         boolean isSRSSR = false;
                  
        for(Service_request__C sr :(Trigger.isDelete ? Trigger.old : Trigger.New))
        {
            if(sr.RecordTypeId != null)
            {
                if(SRSRecordTypes.contains(rtSRSMapById.get(sr.RecordTypeId).getName()))
                {
                    isSRSSR = true;
                    if((Trigger.isInsert || Trigger.isUpdate) && Trigger.isBefore && (sr.Description__c=='' || sr.Description__c==null))
                    {                     
                        sr.Description__c=sr.Opportunity_name__c;   
                    }
                }                 
            }
        }
             
       // System.debug('~~~~~ Is SRS for Service Request? ' + isSRSSR );
        if(Trigger.isAfter) 
        {        
            if (trigger.isInsert)
            {  
                  //create a new enhance tracking record when a new service request is created
                 // Non SRS code
                if(!isSRSSR)
                {
                    smb_ServiceRequestHelper.insertEnhanceTrackingRecord(trigger.New,null,true);    
                    smb_ServiceRequestHelper.syncOrderRequestStatus(trigger.New, false, null);
                    smb_ServiceRequestHelper.updateLegacyOrderOnOpportunity(trigger.New);
                }
            }     
            
            if (trigger.isUpdate)
            {  
                 // Non SRS code
                if(!isSRSSR)
                {
                    list<Service_Request__c> lstUpdatedReq = new list<Service_Request__c>();
                    for (Service_Request__c serReq : trigger.New)
                    {
                        if (serReq.Status__c != trigger.oldMap.get(serReq.Id).Status__c ||serReq.LastModifiedById != trigger.oldMap.get(serReq.Id).LastModifiedById)
                        {
                            lstUpdatedReq.add(serReq);
                        }                    
                    }
                    smb_ServiceRequestHelper.insertEnhanceTrackingRecord(lstUpdatedReq,trigger.oldMap,false);
                    smb_ServiceRequestHelper.syncOrderRequestStatus(trigger.New, true, trigger.oldMap); 
                   
                }
            } 
            
            if(trigger.isDelete)
            {        
                if(isSRSSR)
                {
                    SRS_ServiceRequestValidationHelper.updateOpportunityStatus(Trigger.oldMap.keyset());  
                }
                else 
                {
                    smb_ServiceRequestHelper.syncOrderRequestStatus(trigger.old, false, null);
                }   
            }
             
            if(trigger.isUnDelete)
            {
          
                if(!isSRSSR)
                {
                    smb_ServiceRequestHelper.syncOrderRequestStatus(trigger.new, false, null);  
                } 
            }
         }
         
         if(Trigger.isBefore)
         {          
             if(Trigger.isInsert)
             {
                System.debug('~~~~~ TRIGGER.INSERT.ISBEFORE + IS SRS Service Request? ' + isSRSSR );     
                // Non SRS code
                if(!isSRSSR)
                {
                    smb_ServiceRequestHelper.populateOpportunityAndOrderRequest(trigger.New);
                    smb_ServiceRequestHelper.populateStatusOnServiceRequest(trigger.new,false,null);
                    smb_ServiceRequestHelper.populateServiceRequestOwnerID(trigger.new,null);
                    smb_ServiceRequestHelper.populateServiceRequestProduct(trigger.new);
                }
                 
               // Update R3 'Required to Submit' field when a new Service Request record is created
                if(isSRSSR)
                {                    
                    SRS_ServiceRequestValidationHelper.validateServiceRequest(Trigger.new, trigger.oldMap,trigger.newMap);                     
                    // Code Change by Priyanka Dhomne (Tech M.) Dt. 14 Feb 2017 for BR = 005
                    /*  for(service_request__c serviceRequest:trigger.new)
                    {
                        schema.recordtypeinfo info=rtSRSMapById.get(serviceRequest.recordtypeid);
                        if(info.getname().equalsignorecase('change')||info.getname().equalsignorecase('disconnect'))                       
                        {
                            if(string.isblank(serviceRequest.SRS_PODS_Product__c))
                            {
                                serviceRequest.Required_To_Hand_Off_To_Care_checklist__c='Select Product\n';
                            }
                            else
                            {
                                serviceRequest.Required_To_Hand_Off_To_Care_checklist__c='';
                            }
                            
                            if(string.isblank(serviceRequest.Service_Identifier_Type__c))
                            {
                                serviceRequest.Required_To_Hand_Off_To_Care_checklist__c+='Select a Service Identifier Type\n';
                            }
                            else                           
                            {
                                serviceRequest.Required_To_Hand_Off_To_Care_checklist__c+='';
                            }
                        }
                        else{
                            serviceRequest.Required_To_Hand_Off_To_Care_checklist__c=serviceRequest.outstanding_issues__c;
                        }*/
                        
                      
                     }          
            }           
           
             
            if(Trigger.isUpdate)
            {  
            // Non SRS code     
                if(!isSRSSR) 
                {     
                   smb_ServiceRequestHelper.populateStatusOnServiceRequest(trigger.new,true,trigger.oldMap);
                   smb_ServiceRequestHelper.populateServiceRequestOwnerID(trigger.new, trigger.oldMap);
                }
                
                if(isSRSSR)
                {
                   System.debug('~~~~before inside validation helper class');
            // since the method validateServiceRequest may potentially change the SR's status,
            // this method should only be called on SRs whom have had certain kinds of fields updated only
            // getChangedServiceRequestsForValidation will provide the list of SRs to be vailidated based on the 
            // field exemption criteria specified
            // e.g. 
            //     for a given SR whos updated fields only consist of (condition__c', condition_color__c', and condition_sort_index__c)
            //     will be exempt from validation
            // IMPORTANT: exemption set field names must be specified in lower case only          
                   List<Service_Request__c> srListToValidate = 
                        SRS_ServiceRequestValidationHelper.getChangedServiceRequestsForValidation(
                           trigger.oldMap,
                           trigger.newMap, 
                           new List<Set<String>>{
                              new Set<String>{'condition__c', 'condition_color__c', 'condition_sort_index__c'}
                           }
                    );
                    
                    oppIdSet = SRS_ServiceRequestValidationHelper.validateServiceRequest(srListToValidate, trigger.oldMap,trigger.newMap);
                    SRS_ServiceRequestValidationHelper.validateRecTypeServiceRequest(Trigger.new, trigger.oldMap );
                    for(service_request__c serviceRequest:trigger.new)
                    {
                        schema.recordtypeinfo info=rtSRSMapById.get(serviceRequest.recordtypeid);
                        
                          // Code Change by Priyanka Dhomne (Tech M.) Dt. 14 Feb 2017 for BR = 005
                        /*if(info.getname().equalsignorecase('change')||info.getname().equalsignorecase('disconnect'))                      
                        {
                            serviceRequest.Required_To_Hand_Off_To_Care_checklist__c='';
                            
                            if(string.isblank(serviceRequest.SRS_PODS_Product__c))
                            {
                                serviceRequest.Required_To_Hand_Off_To_Care_checklist__c='Select Product\n';
                            }
                            else
                            {
                                serviceRequest.Required_To_Hand_Off_To_Care_checklist__c='';
                            }
                            
                            if(string.isblank(serviceRequest.Service_Identifier_Type__c))
                            {
                                serviceRequest.Required_To_Hand_Off_To_Care_checklist__c+='Select a Service Identifier Type\n';
                            }
                            else
                            {
                                serviceRequest.Required_To_Hand_Off_To_Care_checklist__c+='';
                            }
                           
                        }
                        else{
                            serviceRequest.Required_To_Hand_Off_To_Care_checklist__c=serviceRequest.outstanding_issues__c;
                        }*/
                        
                    }
                    //....Update FOX Jeop Code and Calculate Condition....  
                    SRS_CRDB_ConditionCal CCal= new SRS_CRDB_ConditionCal();
                    System.debug('Before Update:'+ trigger.new.size());
                    CCal.UpdateFOXJeopAndCond(Trigger.new, trigger.oldMap);
                }        
            }
        }
            
        if(Trigger.isAfter)
        {        
            if(Trigger.isInsert)
            {
                if(isSRSSR)
                {
                    SRS_ServiceRequestValidationHelper.updateOpportunityStatus(Trigger.newMap.keyset());
                    
                    //START :: Non-TQ TF Case 1904646, BR: S2CP-82 :: added by Rajan ( JIRA URL : https://jira.tsl.telus.com/browse/S2CP-82 )
                
                    smb_ServiceRequestHelper.populateServiceRequestContact(Trigger.new);
                    
                    //END :: Non-TQ TF Case 1904646, BR: S2CP-82 :: added by Rajan
                    
                }
            } 
            else if(Trigger.isUpdate)
            {            
                if(isSRSSR)
                {
                    set<id> setSRIds=new set<id>();
                    for (Service_Request__c newSR : trigger.New)
                    {                    
                        Service_Request__c oldSR= trigger.oldMap.get(newSR.Id); 
                        if(newSR.Service_Request_Status__c != oldSR.Service_Request_Status__c)
                        {
                            setSRIds.add(newSR.id);
                        }                                                              
                    }
                    
                    if(setSRIds.size()>0)
                    {
                        SRS_ServiceRequestValidationHelper.updateOpportunityStatus(setSRIds); 
                    }    
                } 
            }  
        }            
  
        if(trigger.old!=null)
        {
            Integer Counter = 0;
            System.debug('######_ON END');
            for (Service_Request__c OBJTEST : trigger.old)
            {
                Counter = Counter+1;
                System.debug('######_ID'+OBJTEST.id);
                system.debug('######_'+Counter+'_TRIGGER.OLD Status__C: '+ OBJTEST.status__c);
            }
        }
            
        if(trigger.New!=null)
        {
            Integer Counter = 0;
            System.debug('######_ON END');
            for (Service_Request__c OBJTEST : trigger.New)
            {
                Counter = Counter + 1;
                System.debug('######_ID'+OBJTEST.id);
                system.debug('######_'+Counter+'_TRIGGER.NEW = Status__C: '+ OBJTEST.status__c);
            }
        }
      }      
    }