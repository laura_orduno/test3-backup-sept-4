trigger CalculateTFCaseScoreMax100 on SMET_Request__c (before insert, before update) {

    for( SMET_request__c tfCase : Trigger.new)
    {        
    	Integer alignmentScore = tfCase.Alignment_Across_All_Impacted_Groups__c == 'Yes' ? 2 : 0;
        
        tfCase.TF_Case_Score_Max_100__c = 
            tfCase.Project_Prioritization_Score_max20__c + 
      		tfCase.Business_Impact_Score__c + 
      		tfCase.Impacts_to_Business_Units_Max_Score__c + 
      		tfCase.Impacts_to_Business_Users_Max_Score__c + 
      		alignmentScore;
        
      	System.debug('tfCase.TF_Case_Score_Max_100__c ' + tfCase.TF_Case_Score_Max_100__c);
        
    }       
}