trigger setManagerOnLead on User (after update) {
    Set<Id> updated = new Set<Id>();
    //identify the users whose managers have changed
    for(User u : Trigger.new){
        User oldUser = trigger.oldMap.get(u.Id);
        if((u.Manager_1__c != oldUser.Manager_1__c) || (u.Manager_2__c != oldUser.Manager_2__c) ||
            (u.Manager_3__c != oldUser.Manager_3__c) || (u.Manager_4__c != oldUser.Manager_4__c)){
                updated.add(u.Id);
            }
    }
    //get leads owned by these users and update them with new manager values
    if(!updated.isEmpty() && !RoleHierarchy.isRunningUserUpdate){
        OwnerManagerUpdates.updateLeads(updated);
//      OwnerManagerUpdates.updateLeads(updated, trigger.newMap);
    }
}