/*
###########################################################################
# File..................: SMB_OpportunityProductTrigger
# Version...............: 1
# Created by............: Pallavi Mittal
# Created Date..........: 24-oct-2013
# Last Modified by......: 
# Last Modified Date....: 24-Oct-2013
# Description...........: This trigger contains the methods calls used to update the Opportunity Products 
# Change Log:               
############################################################################
*/
trigger SMB_OpportunityProductTrigger on OpportunityLineItem (before delete,after delete, after insert, after update) {
    
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            smb_OpportunityProductHelper.createCase(Trigger.new, false, null);
            //smb_OpportunityProductHelper.updateOpportunitySvcAddr(trigger.new);  //CF:Added      
            smb_OpportunityProductHelper.updateOpportunitySvcAddr(trigger.new,null);
            smb_CPQPrimaryQuoteLineTrigger_Helper.updateOLI(Trigger.new);
            
        }
        
        if(Trigger.isUpdate){
            smb_OpportunityProductHelper.createCase(Trigger.new, true, Trigger.oldMap);
            //smb_OpportunityProductHelper.updateOpportunitySvcAddr(trigger.new);  //CF:Added
            smb_OpportunityProductHelper.updateOpportunitySvcAddr(trigger.new,trigger.oldMap);
            smb_OpportunityProductHelper.updateOpportunityForNecessaryDueDates(trigger.new,null);
        }
        
        if(Trigger.isDelete){          
          //smb_CPQPrimaryQuoteLineTrigger_Helper.rejectAgreements(Trigger.oldMap);
           //revalidate dates after lineitems are deleted
          List<Opportunity> opps = new List<Opportunity>();
          List<Id> oppIds = new List<Id>();          
          for(OpportunityLineItem oli : trigger.old) {
              oppIds.add(oli.Opportunityid);
          }
          opps = [select id, All_necessary_Due_Dates_booked__c from opportunity where id IN :oppIds];
          for(opportunity opp : opps) {
              opp.All_necessary_Due_Dates_booked__c = true;
          }
          update opps;
          smb_OpportunityProductHelper.updateOpportunityForNecessaryDueDates(null,opps);
        }
   
    }
    if(Trigger.isBefore && Trigger.isDelete)
    {
        if(Trigger.IsDelete)
            smb_OpportunityProductHelper.restrictDeletionUpdation(Trigger.Old);
    }
}