trigger CampaignMemberAfter on CampaignMember (after insert, after update) {
    Set<Id> leadIds = new Set<Id>();
    for (CampaignMember cm : Trigger.new) {
        if (Trigger.isInsert) {
            leadIds.add(cm.LeadId);
        }
    }
    
    if (!leadIds.isEmpty()) {
        List<Lead> leads = [SELECT OwnerId FROM Lead WHERE Id IN :leadIds AND OwnerId = '00G400000019qbR'];
        for (Lead lead : leads) {
            lead.OwnerId = '00G40000000uzaH';
        }
        if(!Test.isRunningTest()){
            update leads;
        }
    }
}