trigger tract_languageGroupChange on User (after update, after insert) {

	trac_UserLangMgmt tracTrigger = new trac_UserLangMgmt();
	if (trigger.isUpdate) { 
		tracTrigger.updateUserLangGroups(trigger.new, trigger.old);
	}
	
	
	/*if(trigger.isUpdate) {
		tracTrigger.updateUserLangGroups();
	} 
	
	if(trigger.isInsert) {
		try{
			tracTrigger.updateUserLangGroups();
		} catch(Exception e) {}
	}
	*/
}