/**
 * This is the Agreement Subscriber Event Trigger to link the new Subsriber event to a Current contract.
 * Setps:
 * Before update
 * Step 1:  Create Map to hold th all the events group by BAN ( Customer) Number.
 * Step 2:  Query All SFDC account with parent Ids in the Collected BAN Id list in Step 1
 *      Note: We need to get the BAN Parent Account ID ( RCID account)  
 * Step 3: Create a Map to store the BAN parent account id for each ban number.
 * Step 4: Query both Agreement which are active and Subsidairy Account under agreement and Account collected 
 * Step 5: Link Agreements id to Subscriber Event.
 * 
 * After Udpate:
 * Step 1:
 * Step 2: 
 * 
 */ 

trigger AgreementSubscriberEventTrigger on Agreement_Subscriber_Event__c (before insert,before update, after insert , after update) {
    
    if(trigger.isBefore && (Trigger.isUpdate || Trigger.isInsert)) {
        
        Map<String,Agreement_Subscriber_Event__c> banIdBySubscribers = new Map<String,Agreement_Subscriber_Event__c>();
        for(Agreement_Subscriber_Event__c event : Trigger.New) {
            if(String.isNotBlank(event.ban__c))
                banIdBySubscribers.put(event.ban__c,event);
        }
        
        //accountMap - CAN and ParentId
        Map<Id,account> accountMap = new Map<Id,account>([select id,can__c,parentid from account where can__c=:banIdBySubscribers.keySet() limit 50000]);
        //Map #1 - build a Map of CANs and their parent RCIDs
        Map<String,Id> canParentMap = new Map<String,Id>();
        String acctCanString = '';
        for(Id accId:accountMap.keySet()){
            Account acct = accountMap.get(accId);
            acctCanString += acct.can__c + ':';
            canParentMap.put(acct.can__c,acct.parentId);
        }
        system.debug('accRec CAN list:'+acctCanString);
        system.debug('accountMap.Keyset():'+accountMap.keySet());
        system.debug('accountMap.values():'+accountMap.values());
        
        //Find the list of contracts for their parents
        Map<String,Contract> accountContractMap = new Map<String,Contract>();
        //list<contract> contractList=[select id,accountid from contract where accountid in:accountMap.values() limit 50000];// and status='Contract Registered'
        list<contract> contractList=[select id,accountid from contract where accountid in:canParentMap.values() and RecordType.DeveloperName='Corporate_Wireless' limit 50000];// and status='Contract Registered'
        
        //Map #2 - get the map of Account to Contract.  Assumption is there is only 1 contract per account for wireless
        Map<id,id> accountContractIdMap = new Map<id,id>();
        for (contract contract: contractList){
           accountContractIdMap.put(contract.accountid,contract.id);
        }
        
            
       
        system.debug('ContractList:'+contractList);
        String contractIdString = '';
        
        //Get the BAN from the Contract's RCID
        //If the 
        /*for(contract contract:contractList){
            Account acct = accountMap.get(contract.accountid);
            if(acct != null) {
                accountContractMap.put(acct.can__c,contract);
                contractIdString += contract.id + ':';
            }
        }*/
        //Loop the Agreements, get the BANS and see if they had a parent matching the contract list
       for(Agreement_Subscriber_Event__c event : Trigger.New) {
           if (String.isNotBlank(event.ban__c) && 
               		canParentMap.get(event.ban__c) <> null && 
               		accountContractIdMap.get(canParentMap.get(event.ban__c)) <> null) {
               event.contract__c = accountContractIdMap.get(canParentMap.get(event.ban__c));
           }
       }
        
        /*for(Agreement_Subscriber_Event__c event : Trigger.New) {
            if(String.isNotBlank(event.ban__c)) {
                Contract ctrct = accountContractMap.get(event.ban__c);
                if(ctrct != null)
                    event.contract__c = ctrct.id;
            }
        }*/
    }
}