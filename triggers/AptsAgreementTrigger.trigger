trigger AptsAgreementTrigger on Apttus__APTS_Agreement__c (after update) {
    //after update trigger
    //activate an agreement if status is fully signed
    if(Trigger.isUpdate && Trigger.isAfter && Trigger.size == 1) {
        //activate an agreement
        if(Trigger.newMap.values().Apttus__Status__c != null && Trigger.newMap.values().Apttus__Status__c.equalsIgnoreCase('Fully Signed')) {
            String activateResponse = AptsComplyCustomAPI.doActivate(Trigger.newMap.values().Id);
            system.debug('*** activateResponse ***'+activateResponse);
        }
        
        //cancel an agreement
        //ERROR : System.CalloutException: Callout from triggers are currently not supported.: Class.DocuSignStatusAPI.APIServiceSoap.VoidEnvelope: line 678, column 1
        //if(Trigger.newMap.values().Apttus__Status_Category__c != null && Trigger.newMap.values().Apttus__Status_Category__c.equalsIgnoreCase('Cancelled')) {
        //  AptsComplyCustomAPI.doCancel(Trigger.newMap.values().Id);
        //}
    }
}