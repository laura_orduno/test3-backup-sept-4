/**
 *  MBRCaseClosed.trigger
 *
 *  @author     Alex Kong (akong@tractionondemand.com)
 *  @date       2015-03-16
 **/

 //** Replaced By B2BService_NotificationHelper
trigger MBRCaseClosed on Case (after update) {
/*
    try{
        if(Limits.getQueries() >= 100){
            System.debug('MBRCaseClosed: abort since SOQL limmit will be hit.');
            
            throw new QueryException();
        }

        // find the desired email template
        List<EmailTemplate> emailTemplates = [SELECT Id from EmailTemplate where name = 'MBR case is closed collaborators'];
        EmailTemplate tmpl = emailTemplates.size()>0 ? emailTemplates[0] : null;
        
        if (tmpl != null) {
    
            Id fromEmailId = null;
            List<OrgWideEmailAddress> fromEmails = [select Id from OrgWideEmailAddress where DisplayName = 'My Business Requests'];
            if (fromEmails.size() > 0) {
                fromEmailId = fromEmails[0].Id;
            }
    
            for (Case c : Trigger.new) {
                if (c.Origin == Label.MBRCCI_Self_Serve) {
                    Case oldC = Trigger.oldMap.get(c.Id);
                    if (oldC.Status != 'Closed' && c.Status == 'Closed') {
                        // MBR Case has closed; do we need to notify collaborators?
                        if (c.NotifyCollaboratorString__c != null && c.NotifyCollaboratorString__c.length() > 0) {
                            List<String> collabEmails = c.NotifyCollaboratorString__c.split(';', 0);
                            if (collabEmails.size() > 0) {
                                try {
                                    List<Messaging.SendEmailResult> mailResults = MBRUtils.sendTemplatedCaseEmails(collabEmails, fromEmailId, tmpl.Id, c.Id);
                                } catch (Exception e) {
                                    System.debug('Trigger MBRCaseClosed caught MBRUtls.sendTemplatedCaseEmails() exception: ' + e);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    catch(QueryException e){
        
    }*/
}