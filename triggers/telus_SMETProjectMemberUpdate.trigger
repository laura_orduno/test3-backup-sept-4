//Add project members if needed
trigger telus_SMETProjectMemberUpdate on SMET_Project__c (after insert, after update) {

	if(Trigger.isInsert){
		List<Cloud_Enablement_Project_Team__c> members = new List<Cloud_Enablement_Project_Team__c>();
		for(SMET_Project__c p : trigger.new){
			if(p.Project_Prime__c != null){
				Cloud_Enablement_Project_Team__c member1 = new Cloud_Enablement_Project_Team__c(Team_Member__c = p.Project_Prime__c, Cloud_Enablement_Project__c = p.id);
				members.add(member1);				
			}
		}
		if (members.size() > 0) {
			insert members;
		}
	}

	if(Trigger.isUpdate){

		List<Cloud_Enablement_Project_Team__c> members = new List<Cloud_Enablement_Project_Team__c>();
		Set<Id> projectIds = new Set<Id>();
	    
	    for (SMET_Project__c t : trigger.new) {
	        projectIds.add(t.id);
	    }
	    
	    List<Cloud_Enablement_Project_Team__c> projectMembers = [SELECT Id, Cloud_Enablement_Project__c, Team_Member__c FROM Cloud_Enablement_Project_Team__c WHERE Cloud_Enablement_Project__c IN :projectIds];

		for (SMET_Project__c p : trigger.new) {
		    if(p.Project_Prime__c != null){
			    Boolean foundPrime = false;
				for(Cloud_Enablement_Project_Team__c m : projectMembers){
					if (m.Cloud_Enablement_Project__c == p.id) {
						if(p.Project_Prime__c == m.Team_Member__c)
						{
							foundPrime = true;
						}
					}
				}
				if(!foundPrime){
					Cloud_Enablement_Project_Team__c member1 = new Cloud_Enablement_Project_Team__c(Team_Member__c = p.Project_Prime__c, Cloud_Enablement_Project__c = p.id);
					members.add(member1);
				}		    	
		    }
		}		
		if (members.size() > 0) {
			insert members;
		}		
	}
}