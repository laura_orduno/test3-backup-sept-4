trigger smb_CaseAfter on Case (after insert, after update) {
    Map<id, schema.Recordtypeinfo> recordTypeInfoMap = case.sobjecttype.getdescribe().getrecordtypeinfosbyid();
    // Added by Andy 28-June-2016 for TF case: 01751713 - Start  
    final static string LEGAL_NAME_CORRECTION_CASE_RECORD_TYPE = 'Legal Name Correction';
    final static string LEGAL_NAME_Update_CASE_RECORD_TYPE = 'Legal Name Update';

    static Set<Id> UpdatedcaseIdwithMembers = New Set<ID>();

    try {
        smb_CaseAfterClass.flagDeactivationOnAccount(Trigger.new, Trigger.isUpdate, Trigger.isInsert, Trigger.oldMap);
        smb_CaseAfterClass.establishTrackingRecords(Trigger.new, Trigger.isUpdate, Trigger.isInsert, Trigger.oldMap);

        if (trigger.isinsert) {
            set<id> accountIds = new set<id>();
            list<account> accountsWithUpdatedLegalName = new list<account>();
            for (case c : trigger.new) {
                if (recordTypeInfoMap.containskey(c.recordtypeid) && (recordTypeInfoMap.get(c.recordtypeid).getname().equalsignorecase(LEGAL_NAME_CORRECTION_CASE_RECORD_TYPE) || recordTypeInfoMap.get(c.recordtypeid).getname().equalsignorecase(LEGAL_NAME_Update_CASE_RECORD_TYPE))) {
                    if (string.isnotblank((string) c.get('updated_legal_name__c'))) {
                        if (string.isnotblank(c.accountid)) {
                            accountsWithUpdatedLegalName.add(new account(id = c.accountid, correct_legal_name__c = c.updated_legal_name__c));
                        }
                    }
                }
            }
            if (!accountsWithUpdatedLegalName.isempty()) {
                database.saveresult[] results = database.update(accountsWithUpdatedLegalName, false);
                Util.emailSystemError(results, 'smb_CaseBefore - Account Correct Legal Name Update Failure', false);
            }
        }
        //Added by Andy 28-June-2016 for TF case: 01751713 - End

        /* Editor: Adhir Parchure
           Date: 14-08-2015
           Project: BCX Callidus Integration
           Use case: Read-only access for Creator of Case (SIS 1088)
           Description: After case assignment rule execution Case submitter loses an access to the case, 
               below code adds case submitter to a case team with Read only access.
        */
        List<CaseTeamMember> caseTeamMemberList = new List<CaseTeamMember>();
        CaseTeamRole caseRole;
        Boolean isSalesEnquiry = false;
        if (trigger.isInsert && trigger.isAfter) {
            for (Case newCase : Trigger.new) {
                if (newCase .RecTypeName__c.equalsIgnoreCase('Sales Compensation Inquiries') ||
                        newCase.RecTypeName__c.equalsIgnoreCase('Sales Compensation Inquiries (Partner Solutions)') && isSalesEnquiry == false) {
                    isSalesEnquiry = true;
                    break;
                }
            }
        }

        if (isSalesEnquiry && caseRole == NULL) {
            caseRole = [select Id from CaseTeamRole where Name = 'Sales Compensation Inquiry creator' limit 1];
        }

        if (Limits.getQueries() >= 100) {
            System.debug('smb_CaseAfter: abort since SOQL limmit will be hit.');

            throw new QueryException();
        }

        if (trigger.isInsert && trigger.isAfter) {
            //caseRole= [select Id from CaseTeamRole where Name='Sales Compensation Inquiry creator' limit 1];

            for (Case newCase : Trigger.new) {
                if (!String.isBlank(newCase.RecTypeName__c) && !String.isBlank(newCase.createdbyid) && !UpdatedcaseIdwithMembers.contains(newCase.id)
                        && caseRole != NULL && (newCase .RecTypeName__c.equalsIgnoreCase('Sales Compensation Inquiries') ||
                        newCase.RecTypeName__c.equalsIgnoreCase('Sales Compensation Inquiries (Partner Solutions)'))) {
                    // Add a case submitter to the case team
                    caseTeamMemberList.add(new CaseTeamMember(ParentId = newCase.id, MemberId = newCase.createdbyId, TeamRoleId = caseRole.id));
                    UpdatedcaseIdwithMembers.add(newCase.id);

                }

            }
            //system.debug('>> caseTeamMemberList>>>>>>>'+caseTeamMemberList); 
            if (!caseTeamMemberList.isEmpty())
                insert caseTeamMemberList;
        }


        // START >> Added by Adhir - TBO User story<< add "Resolution Details" to ase comments when case is closed.
        if (trigger.isUpdate && trigger.isAfter) {
            //map<id,RecordType> TBORecordTypeMap =  new map<id,RecordType>([SELECT Id, Name, Developername from RecordType where Developername IN ('TBO_REQUEST','Legal_Name_Correction') limit 2]);

            if (trigger.newMap != null) {
                system.debug('--------Trigger::Case comment section------');
                List<Task> Tasks = new List<Task>();
                for (Case closedCase : trigger.newMap.values()) {
                    system.debug('>>>>>>>>>>> closedCase.owner.type >>' + closedCase.owner.type);

                    if (recordTypeInfoMap.containskey(closedCase.recordtypeid) && recordTypeInfoMap.containskey(closedCase.recordtypeid) && (recordTypeInfoMap.get(closedCase.recordtypeid).getname().equalsignorecase('TBO Request') || recordTypeInfoMap.get(closedCase.recordtypeid).getname().equalsignorecase('Legal Name Update')) && (closedCase.status.equalsIgnoreCase('Cancelled') ||
                            closedCase.status.equalsIgnoreCase('Closed')) && string.isNotBlank(closedCase.Resolution_Details__c)
                            && !String.valueof(closedCase.ownerid).startsWith('00G')) {
                        //CaseComment comment= new CaseComment(parentid=closedCase.id);
                        //comment.commentBody=closedCase.Resolution_Details__c;
                        //comment.isPublished=true;
                        //closedCaseComments.add(comment);

                        Task t = new Task();
                        t.OwnerId = closedCase.ownerid;
                        t.Subject = 'Closed case Resolution details - ' + closedCase.Resolution_Details__c;
                        t.Status = 'Completed';
                        t.ActivityDate = system.today();
                        t.Priority = 'Normal';
                        t.WhatId = closedCase.Id;
                        Tasks.add(t);

                    }
                }
                system.debug('>>>>>>>>>>> Tasks Size >>' + Tasks.size());
                system.debug('>>>>>>>>>>> Tasks >>' + Tasks);
                if (Tasks != null && Tasks.size() > 0) {
                    database.insert(Tasks);
                }
            }
        }

        // END >> Added by Adhir - TBO User story
    } catch (QueryException e) {

    }
}