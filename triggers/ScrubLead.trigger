/**
 * Runs logic that scrubs Company field.
 * 
 * @author Max Rudman
 * @since 6/27/2010
 */
trigger ScrubLead on Lead (before insert, before update) {
    List<Lead> changed = new List<Lead>();
    for (Lead lead : Trigger.new) {
        if (Trigger.isInsert) {
            changed.add(lead);
        } else {
            Lead old = Trigger.oldMap.get(lead.Id);
            if (old.Company != lead.Company) {
                changed.add(lead);
            }
        }
    }
    
    if (!changed.isEmpty()) {
        (new LeadCompanyScrubber()).scrub(changed);
    }
}