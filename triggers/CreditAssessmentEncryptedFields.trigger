trigger CreditAssessmentEncryptedFields on Credit_Assessment__c(before insert,after insert,before update,after update,before delete, after delete){
   // CreditTriggerHandler handler=new CreditTriggerHandler(); 
   // 28 Jan 2016 - Sandip - Commented above line to push BO Enhancement changes to SFStage
    if(Trigger.isBefore){
        if(Trigger.isInsert || Trigger.isUpdate) {
            
            for (Credit_Assessment__c car: Trigger.new) {
                if(String.isBlank(car.Driver_s_License__c)) 
                    car.Driver_s_License_Hash__c = null;
                else  
                    car.Driver_s_License_Hash__c = CreditCrypto.hash(car.Driver_s_License__c);
                
                if(String.isBlank(car.Health_Card_No__c)) 
                    car.Health_Card_No_Hash__c = null;
                else 
                    car.Health_Card_No_Hash__c = CreditCrypto.hash(car.Health_Card_No__c);
                
                if(String.isBlank(car.SIN__c)) 
                    car.SIN_Hash__c = null;
                else 
                    car.SIN_Hash__c = CreditCrypto.hash(car.SIN__c);
                
                if(String.isBlank(car.Provincial_ID__c)) 
                    car.Provincial_ID_Hash__c = null;
                else 
                    car.Provincial_ID_Hash__c = CreditCrypto.hash(car.Provincial_ID__c);
                
                if(String.isBlank(car.Passport__c)) 
                    car.Passport_Hash__c = null;
                else 
                    car.Passport_Hash__c = CreditCrypto.hash(car.Passport__c);
                
            }
            /* Code Changes for BO enhancement FR803101 by Amit Rana Dt 16 Oct 2015 */
            // Code changes start
            Try
            {
                system.debug('@@@@@'+Trigger.IsUpdate);
                system.debug('@@@@@'+Trigger.IsAfter);
                if(Trigger.IsUpdate && Trigger.Isbefore)
                {
                    if (SMB_Helper.Activate_AOO_To_NC ==  true) return; // Sandip - 04 Feb 2016 - Defect Id: 53111 - Added this statenment to execute below code only once
                    // Variable definitions
                    Set<String> CARFromStates = new Set<String>();
                    Set<String> CARToStates = new Set<String>();
                    Set<String> CARResult = new Set<String>();
                    Set<String> OpptyStatus = new Set<String>();
                    // Fetch Happy path CAR status from custom setting; custom setting provides the CAR states to send an AOO order to NC
                    // Allows the specification of CAR status to be adminstrated by SFDC Admins
                    Map<string,SMB_CAR_Status__c> smbCarStatusMapping = new Map<string,SMB_CAR_Status__c>();
                    smbCarStatusMapping = SMB_CAR_Status__c.getAll();
                    for(SMB_CAR_Status__c Obj : smbCarStatusMapping.values())
                    {
                        // Gather all the 'from' states.
                        if(Obj.Category__c.equalsIgnoreCase('From'))
                            CARFromStates.add(Obj.name);
                        
                        // Gather all the 'to' states.
                        if(Obj.Category__c == 'To')
                            CARToStates.add(Obj.name);
                        
                        // Gather all the 'Result' states
                        if(Obj.Category__c == 'Result')
                            CARResult.add(Obj.name);
                            
                        // Sandip - 11 Feb 2016 - Capture opportunity stage from custom setting
                        if(Obj.Category__c.equalsIgnoreCase('Opportunity Status'))
                            OpptyStatus.add(Obj.name);
                    }
                    
                    Set<String> allMatchedCars = new Set<String>();
                    List<Opportunity> Related_Opportunities_to_Update = new List<Opportunity>();
                    
                    // Validate if the previous CAR state is In-progress or Amend and the new CAR state is one of the 'to' states
                    // Collect all CAR records
                    for(Credit_Assessment__c ObjCAR: Trigger.new)
                    { 
                        if(
                            ((CARFromStates.contains(Trigger.oldMap.get(ObjCAR.Id).CAR_Status__c)) && (CARToStates.contains(ObjCAR.CAR_Status__c)) && (CARResult.contains(ObjCAR.CAR_Result__c))) ||
                            ((CARFromStates.contains(Trigger.oldMap.get(ObjCAR.Id).CAR_Status__c)) && (CARToStates.contains(ObjCAR.CAR_Status__c))))
                        {
                            // Create a set of all CAR records which match the 'from-to' state transitions
                            allMatchedCars.add(ObjCAR.id);
                            
                            // Store the previous and new CAR state, etc. in prep to communicate to the back office
                            SMB_Helper.CAR_STATUS_Container Obj_Wrapper_CAR = new SMB_Helper.CAR_STATUS_Container();
                            
                            obj_wrapper_car.OLD_CAR_Status = Trigger.oldMap.get(ObjCAR.Id).CAR_Status__c;
                            obj_wrapper_car.OLD_Car_result = Trigger.oldMap.get(ObjCAR.Id).CAR_Result__c;
                            obj_wrapper_car.OLD_Car_reason_code = Trigger.oldMap.get(ObjCAR.Id).CAR_Result_Reason__c;
                            
                            obj_wrapper_car.NEW_CAR_Status = ObjCAR.CAR_Status__c;
                            obj_wrapper_car.NEW_CAR_Result = ObjCAR.CAR_Result__c;
                            obj_wrapper_car.NEW_CAR_Reason_code = ObjCAR.CAR_Result_Reason__c;
                            
                            // populate CAR_Status_MAP with the from and to information to be sent to the BO in the AOO notes field.            
                            SMB_Helper.CAR_Status_MAP.put(ObjCAR.id,Obj_Wrapper_CAR);
                        }
                    }
                    
                    // if we have impacted CAR records
                    if(allMatchedCars.size()>0)
                    {
                        // Fetch all related opportunities associated to the CAR.  Note Credit_Assessment__c is a lookup on Oppty.
                        for(Opportunity ObjOppty :[Select Accountid,account.name,Order_ID__C,Credit_Assessment__r.name,id,Credit_Assessment__c,CAR_Status__c,
                                                    Related_Orders__c, Related_Orders__r.Related_Orders__c, Related_Orders__r.Related_Orders__r.Id,
                                                    Related_Orders__r.createdDate, createddate, StageName
                                                    from Opportunity 
                                                    where 
                                                    Credit_Assessment__c in:allMatchedCars 
                                                    and  StageName in: OpptyStatus
                                                    order by createddate]) 
                        {
                            
                                // Purpose of this field is to cause the opportunity associated to the CAR to fire its trigger
                                // where the AOO related order logic will be invoked to send the new CAR status changes to the BO.
                                // The value of 'approved' is arbitrary.  We just need something to update the opportunity.
                                
                                // Sandip 09 Feb 2016 - code fix for defect 53054 and 53111
                                //system.debug('@@@@ obj' + ObjOppty);
                                if (ObjOppty.Related_Orders__c == null){
                                  //  system.debug('@@@@ test' + ObjOppty.Related_Orders__c);
                                    ObjOppty.CAR_Status__c = 'Approved';
                                    Related_Opportunities_to_Update.add(ObjOppty);
                                    SMB_Helper.Oppty_CAR_Map.put(ObjOppty.id,ObjOppty);
                                }else if(ObjOppty.Related_Orders__c != null && ObjOppty.Related_Orders__r.Related_Orders__c != null){
                                    //system.debug('@@@@  1.0' + ObjOppty.Id );
                                    //system.debug('@@@@  1.1' + ObjOppty.Related_Orders__r.Id );
                                    //system.debug('@@@@  1.1' + ObjOppty.Related_Orders__r.Related_Orders__r.Id );
                                    if (ObjOppty.Id == ObjOppty.Related_Orders__r.Related_Orders__r.Id)
                                    {
                                        //system.debug('@@@@  2' );
                                        if(ObjOppty.Related_Orders__r.createdDate > ObjOppty.createdDate)
                                        {
                                            //system.debug('@@@@  3' + ObjOppty.Related_Orders__r.createdDate + ' ' + ObjOppty.createdDate );
                                            ObjOppty.CAR_Status__c = 'Approved';
                                            Related_Opportunities_to_Update.add(ObjOppty);
                                            SMB_Helper.Oppty_CAR_Map.put(ObjOppty.id,ObjOppty);
                                        }
                                    }
                                }
                           
                        }
                        //Sandip - 09 Feb 2016 - End code fix
                        
                        // Update the related opportunities so that the Opportunity trigger will execute
                        // and send AOO orders for the CAR's related opportunities.
                        if(Related_Opportunities_to_Update.size()>0)
                        {
                            SMB_Helper.Activate_AOO_To_NC = true;
                            Update Related_Opportunities_to_Update;
                            // Do not add any code until this trigger has completed
                        }
                    }
                }
            }
            catch(Exception e)
            {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.error,e.getMessage());
            }
            // Code changes end for BO enhancement FR803101
        }
    }
    
}