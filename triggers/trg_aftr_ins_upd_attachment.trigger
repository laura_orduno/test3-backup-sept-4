trigger trg_aftr_ins_upd_attachment on Attachment (after insert, after Update) {
    
    map<id, attachment> versionAttachmentMap = new map<id,attachment> (); 
    Map<string,Doc_url_settings__c> objNameMap = 	Doc_url_settings__c.getAll();
    set<string> objNames = objNameMap.keyset();
    system.debug('Objname' + objNames );
    list<vlocity_cmt__ContractVersion__c> ContractVersionList = new list<vlocity_cmt__ContractVersion__c>();
    if(trigger.isInsert){
        for (attachment a: Trigger.new) {
            
            String sObjName = a.ParentId.getSObjectType().getDescribe().getName();
            if(objNames.contains(sObjName))
                
            {
                if(!versionAttachmentMap.containsKey(a.ParentId))
                    versionAttachmentMap.put(a.ParentID, a);
                else if(versionAttachmentMap.get(a.ParentId).CreatedDate < a.CreatedDate  )
                    versionAttachmentMap.put(a.ParentID, a);
                 
            }
            
        } 
        
        if(versionAttachmentMap.size()>0 && !versionAttachmentMap.isEmpty() ){
            for(vlocity_cmt__ContractVersion__c cv: [select id, name, AttachmentId__c from 
                                                     vlocity_cmt__ContractVersion__c where id in :versionAttachmentMap.keyset()]){
                                                     
                                                         
                                                         attachment a = versionAttachmentMap.get(cv.id);
                                                          //system.debug('versionAttachmentMap.get(cv.id):'+versionAttachmentMap.get(cv.id));
                                                         cv.AttachmentId__c = a.id;
                                                         ContractVersionList.add(cv);
                                                     }
        }
        
        if(ContractVersionList.size() > 0 && !ContractVersionList.isEmpty() )
            update ContractVersionList;
    }
    
}