/*
*******************************************************************************************************************************
Class Name:     OCOM_OrderProductBeforeTrigger
Purpose:        OrderProduct Trigger        
Created by:     Francisco Hong           08-Apr-2016    No previous version
Modified by:    Thomas Su                06-Jan-2017    QC-6481
                Aditya Jamwal            16-Jan-2017    QC-6481
                Prashant Tiwari          15-Feb-2017    QC-10700/BN-1489: Updated to pull Multi-Site OLI to CAR
*******************************************************************************************************************************
*/ 
trigger OCOM_OrderProductBeforeTrigger on OrderItem (before insert,after insert,before update,after update,before delete,after delete) {

    // reterive additional field values for oldOLIs Rec in a map.
    Map<id,OrderItem> oldOLIwithAdditionalFieldsMap = OCOM_OrderProductTriggerHelper.oldOLIwithAdditionalFieldsMapContext;
    //System.debug('aaaaaaaaaaaaaa'+OCOM_OrderProductTriggerHelper.oldOLIwithAdditionalFieldsMapContext);
    Map<id,Order> orderToOrderIdMap = OCOM_OrderProductTriggerHelper.orderToOrderIdMapContext;//new Map <id,Order>();
    Set<Id> orderIdsSet = new Set<Id>();
    Set<Id> priceBookEntryIdSet=new Set<Id>();
    if(null != Trigger.New){
        for(orderItem oli : Trigger.New){
            orderIdsSet.add(oli.orderId);
            priceBookEntryIdSet.add(oli.PricebookEntryId);            
        }
        if(null != orderIdsSet && orderIdsSet.size()>0 && orderToOrderIdMap==null){
            orderToOrderIdMap = new Map<id,Order>( [select recordtype.developername,parentid__c,type,orderMgmtId__c,vlocity_cmt__ValidationStatus__c,vlocity_cmt__ValidationMessage__c,vlocity_cmt__ValidationDate__c,order_submit_date__c,statuscode,
                                                    (SELECT vlocity_cmt__rootitemid__c,Contract_Line_Item__r.vlocity_cmt__ContractId__r.Complex_Fulfillment_Contract__c,Contract_Line_Item__r.vlocity_cmt__ContractId__r.Status,Contract_Line_Item__r.vlocity_cmt__ContractId__c,Contract_Action__c,vlocity_cmt__ServiceAccountId__c,pricebookentryid,orderid,id,vlocity_cmt__JSONAttribute__c,vlocity_cmt__Product2Id__c,contract_request__r.SFDC_Contract__r.status,contract_request__c,contract_request__r.SFDC_Contract__c,vlocity_cmt__LineNumber__c, vlocity_cmt__AssetReferenceId__c FROM OrderItems) from order where id =:orderIdsSet]); 
            OCOM_OrderProductTriggerHelper.orderToOrderIdMapContext=orderToOrderIdMap;
        }
    }
    
    if(Trigger.isDelete && null != Trigger.Old){
        for(orderItem oli : Trigger.Old){
            orderIdsSet.add(oli.orderId);
                        
        }
        if(null != orderIdsSet && orderIdsSet.size()>0 ){
            orderToOrderIdMap = new Map<id,Order>( [select id,Type,(select vlocity_cmt__rootitemid__c,Contract_Line_Item__r.vlocity_cmt__ContractId__r.Complex_Fulfillment_Contract__c,Contract_Line_Item__r.vlocity_cmt__ContractId__r.Status,Contract_Line_Item__r.vlocity_cmt__ContractId__c,Contract_Action__c,vlocity_cmt__ServiceAccountId__c,pricebookentryid,orderid,id,vlocity_cmt__JSONAttribute__c,vlocity_cmt__Product2Id__c,contract_request__r.SFDC_Contract__r.status,contract_request__c,contract_request__r.SFDC_Contract__c ,vlocity_cmt__LineNumber__c,vlocity_cmt__AssetReferenceId__c from OrderItems) from order where id =:orderIdsSet]); 
            OCOM_OrderProductTriggerHelper.handleDelete(Trigger.isBefore,Trigger.isDelete,Trigger.old, orderToOrderIdMap);
        }
    }
    
    
    String currentUserName = UserInfo.getName(); 
    if(String.isNotBlank(currentUserName) && !currentUserName.equalsIgnoreCase('Application TOCP')){        
        OCOM_OrderProductTriggerHelper.checkIsOrderUpdateable(Trigger.isBefore,Trigger.isInsert,Trigger.isUpdate, Trigger.new,Trigger.newMap,orderToOrderIdMap);
    }
    Boolean isRetrieve=false;
    if(Trigger.old!=null){
        List<OrderItem> oiList=Trigger.old;
        if(oiList!=null && !oiList.isEmpty()){
            for(OrderItem oiInst:oiList){
                if(String.isNotBlank(oiInst.Contract_Line_Item__c)){
                    isRetrieve=true; 
                    if(Trigger.new!=null){
                        List<OrderItem> oiList1=Trigger.new;
                        if(oiList1!=null && !oiList1.isEmpty()){
                            for(OrderItem oiInst1:oiList1){
                                if(String.isNotBlank(oiInst1.Contract_Line_Item__c)){
                                    if(oiInst.Contract_Line_Item__c!=oiInst1.Contract_Line_Item__c){
                                     isRetrieve=true;
                                      break;
                                    } else {
                                        isRetrieve=false; 
                                    }
                                    
                                }
                            }
                        }
                    }
                    if(isRetrieve){
                        break;
                    }
                }
            }
        }
    }
    Map<id,PriceBookEntry> extendedPbeMap=new Map<id,PriceBookEntry>([select id,product2id,product2.Catalog__r.name from PriceBookEntry where id in :priceBookEntryIdSet]);
   if(Trigger.NewMap != null && Trigger.NewMap.size() > 0 && !Trigger.NewMap.isEmpty() && (isRetrieve||oldOLIwithAdditionalFieldsMap==null) ){
       //&& (OCOM_OrderProductTriggerHelper.oldOLIwithAdditionalFieldsMapContext==null || OCOM_OrderProductTriggerHelper.oldOLIwithAdditionalFieldsMapContext.keySet().size()==0) ){
        oldOLIwithAdditionalFieldsMap = new Map <id,OrderItem> ([Select Contract_Line_Item__r.vlocity_cmt__ContractId__r.Complex_Fulfillment_Contract__c,Contract_Line_Item__c,Contract_Line_Item__r.vlocity_cmt__JSONAttribute__c,Contract_Line_Item__r.vlocity_cmt__ContractId__c,Contract_Line_Item__r.vlocity_cmt__ContractId__r.Status,id,IsMultiSiteOLI__c,vlocity_cmt__ServiceAccountId__r.name,Service_Address__r.Service_Address_Full_Name__c,Order.Type,Order.CustomerAuthorizedById,pricebookentry.product2.ProductSpecification__c, pricebookentry.product2.ProductSpecification__r.Name,
                                                                 pricebookentry.product2.ProductSpecification__r.orderMgmtId__c, PricebookEntry.Product2.Name, Order.Service_Address_Text__c,Order.ServiceAddressSummary__c,
                                                                 Order.Status,Order.orderMgmtId__c, PricebookEntryId, PricebookEntry.Product2Id,pricebookentry.Product2.RelationshipType__c,
                                                                 vlocity_cmt__ParentItemId__c,vlocity_cmt__OneTimeTotal__c,pricebookentry.product2.vlocity_cmt__JSONAttribute__c
                                                                 FROM OrderItem where id =: (Trigger.NewMap).keySet()]);
       if(oldOLIwithAdditionalFieldsMap!=null){
           for(String kyStr:oldOLIwithAdditionalFieldsMap.keySet()){
               OrderItem oiObj=oldOLIwithAdditionalFieldsMap.get(kyStr);
               if(oiObj!=null && String.isNotBlank(oiObj.Contract_Line_Item__c)){
                   
               }
           }
       }
      OCOM_OrderProductTriggerHelper.oldOLIwithAdditionalFieldsMapContext=oldOLIwithAdditionalFieldsMap;
    }
    
  /*  if(Trigger.OldMap != null && Trigger.OldMap.size() > 0 && !Trigger.OldMap.isEmpty() && oldOLIwithAdditionalFieldsMap==null ){
       //&& (OCOM_OrderProductTriggerHelper.oldOLIwithAdditionalFieldsMapContext==null || OCOM_OrderProductTriggerHelper.oldOLIwithAdditionalFieldsMapContext.keySet().size()==0) ){
        oldOLIwithAdditionalFieldsMap = new Map <id,OrderItem> ([Select Contract_Line_Item__c,Contract_Line_Item__r.vlocity_cmt__JSONAttribute__c,Contract_Line_Item__r.vlocity_cmt__ContractId__c,Contract_Line_Item__r.vlocity_cmt__ContractId__r.Status,id,IsMultiSiteOLI__c,vlocity_cmt__ServiceAccountId__r.name,Service_Address__r.Service_Address_Full_Name__c,Order.Type,Order.CustomerAuthorizedById,pricebookentry.product2.ProductSpecification__c, pricebookentry.product2.ProductSpecification__r.Name,
                                                                 pricebookentry.product2.ProductSpecification__r.orderMgmtId__c, PricebookEntry.Product2.Name, Order.Service_Address_Text__c,Order.ServiceAddressSummary__c,
                                                                 Order.Status,Order.orderMgmtId__c, PricebookEntryId, PricebookEntry.Product2Id,pricebookentry.Product2.RelationshipType__c,
                                                                 vlocity_cmt__ParentItemId__c,vlocity_cmt__OneTimeTotal__c,pricebookentry.product2.vlocity_cmt__JSONAttribute__c
                                                                 FROM OrderItem where id =: (Trigger.OldMap).keySet()]);
      OCOM_OrderProductTriggerHelper.oldOLIwithAdditionalFieldsMapContext=oldOLIwithAdditionalFieldsMap;
    }*/
    
    OCOM_OrderProductTriggerHelper.populateTotalMrcNrcForMasterOrder(Trigger.isBefore,Trigger.isInsert,Trigger.isUpdate,Trigger.new,Trigger.newMap);
    //OCOM 977: Aruna:10/17/2016: To update "Term" Field on OrderProduct from JSON value
    system.debug('!!SOQL Called before populateContractTermFromJson consumed '+ Limits.getQueries() +' Available' +Limits.getLimitQueries());
    OCOM_OrderProductTriggerHelper.populateContractTermFromJson(Trigger.isBefore,Trigger.isInsert,Trigger.isUpdate,Trigger.new);   
    system.debug('!!SOQL Called after populateContractTermFromJson consumed '+ Limits.getQueries() +' Available' +Limits.getLimitQueries());
    // Thomas QC-8906
    system.debug('!!SOQL Called before checkCreditAssessmentApprovalNotification consumed '+ Limits.getQueries() +' Available' +Limits.getLimitQueries());
    OCOM_OrderProductTriggerHelper.checkCreditAssessmentApprovalNotification(Trigger.isBefore,Trigger.isInsert,Trigger.isUpdate,Trigger.oldMap,Trigger.new);   
   system.debug('!!SOQL Called after checkCreditAssessmentApprovalNotification consumed '+ Limits.getQueries() +' Available' +Limits.getLimitQueries());
    // Thomas QC-6481
   system.debug('!!SOQL Called before createCreditRelatedProducts consumed '+ Limits.getQueries() +' Available' +Limits.getLimitQueries());
  //uncomment  
    OCOM_OrderProductTriggerHelper.createCreditRelatedProducts(Trigger.isBefore,Trigger.isInsert,Trigger.isUpdate,Trigger.oldMap,Trigger.new,oldOLIwithAdditionalFieldsMap);  
    system.debug('!!SOQL Called after createCreditRelatedProducts consumed '+ Limits.getQueries() +' Available' +Limits.getLimitQueries());
    // Aditya QC-6481
    system.debug('!!SOQL Called before createNonDoableTaskForFOAgent consumed '+ Limits.getQueries() +' Available' +Limits.getLimitQueries());
  //uncomment  
    OCOM_OrderProductTriggerHelper.createNonDoableTaskForFOAgent(Trigger.isBefore,Trigger.isInsert,Trigger.isUpdate,Trigger.oldMap,Trigger.new);  
    system.debug('!!SOQL Called after createNonDoableTaskForFOAgent consumed '+ Limits.getQueries() +' Available' +Limits.getLimitQueries());
    // system.debug('@@!!Inside updateOLIAmendStatus Trigger ');   
   // String currentUserName = UserInfo.getName(); 
    if(String.isNotBlank(currentUserName)&& currentUserName.equalsIgnoreCase('Application TOCP')){
        //system.debug('@@@@@@@@ from trigger') ;
         OCOM_OrderProductTriggerHelper.checkOrder(Trigger.isBefore,Trigger.isAfter,Trigger.isInsert,Trigger.isUpdate,Trigger.isDelete,Trigger.oldMap,Trigger.new,oldOLIwithAdditionalFieldsMap,orderToOrderIdMap);  
        system.debug('!!SOQL Called before sendWelcomLetter consumed '+ Limits.getQueries() +' Available' +Limits.getLimitQueries());
  //uncomment   
        OCOM_OrderProductTriggerHelper.sendWelcomLetter(Trigger.isBefore,Trigger.isAfter,Trigger.isInsert,Trigger.isUpdate,Trigger.oldMap,Trigger.new,oldOLIwithAdditionalFieldsMap); 
        system.debug('!!SOQL Called after sendWelcomLetter consumed '+ Limits.getQueries() +' Available' +Limits.getLimitQueries());
        
    }else{
        OCOM_OrderProductTriggerHelper.updateOLIAmendStatus(Trigger.isBefore,Trigger.isAfter,Trigger.isInsert,Trigger.isUpdate,Trigger.isDelete,Trigger.oldMap,Trigger.new,oldOLIwithAdditionalFieldsMap,orderToOrderIdMap);  
        OCOM_OrderProductTriggerHelper.updateOLIContractAction(Trigger.isBefore,Trigger.isAfter,Trigger.isInsert,Trigger.isUpdate,Trigger.isDelete,Trigger.oldMap,Trigger.new ,oldOLIwithAdditionalFieldsMap,orderToOrderIdMap,extendedPbeMap);
    }
    // Aditya QC-6481   
    
    if(Trigger.isBefore && Trigger.isUpdate){
        OrdrUtilities.saveDecompMsg(Trigger.new);
    }

    List<OrderItem> orderItemList= new List<OrderItem>();
    set<string> orderIds= new set<string>();  
    set<string> orderItemBillingAccount= new set<string>();    
    OCOM_OrderProductBeforeTrigger_Helper callHelper= new OCOM_OrderProductBeforeTrigger_Helper();
    System.debug(' Inside OCOM_OrderProductBeforeTrigger ');
    if(!Trigger.isdelete){
        boolean isBPIIdPresent=false;
        
        boolean checkOrderItemStatus=false;
        boolean checkOrderItemSubStatus=false; 
        Set<String> orderIdSet=new Set<String>();
        for(orderItem OLI:trigger.new) 
        {   
            orderItemList.add(OLI);
            orderIds.add(OLI.orderId); 
            if(null!=OLI.vlocity_cmt__BillingAccountId__c){
                orderItemBillingAccount.add(OLI.vlocity_cmt__BillingAccountId__c);
            } 
            //Added By Santosh Begin
             Map<Id,OrderItem> currentOIMap=Trigger.oldMap;
            String currentBPIId=null;
            if(null != currentOIMap && null != currentOIMap.get(OLI.id)){
                 currentBPIId=currentOIMap.get(OLI.id).orderMgmt_BPI_Id__c;
            }
            String currentOrdStatus='';
            if(OLI!=null && oldOLIwithAdditionalFieldsMap!=null && oldOLIwithAdditionalFieldsMap.get(OLI.id)!=null && oldOLIwithAdditionalFieldsMap.get(OLI.id).Order!=null){
                currentOrdStatus=oldOLIwithAdditionalFieldsMap.get(OLI.id).Order.Status;
            }
                 
                 
            //Added By Santosh End
            if(Trigger.isUpdate && Trigger.isAfter && !isBPIIdPresent && ! System.isFuture()){               
                if(String.isNotBlank(OLI.orderMgmt_BPI_Id__c) &&  OLI.orderMgmt_BPI_Id__c!=currentBPIId && currentOrdStatus!='Cancelled'){//Modified by santosh
                    isBPIIdPresent=true;
                }          
            }
            if(Trigger.isUpdate && Trigger.isBefore && !checkOrderItemStatus){               
                if(String.isNotBlank(OLI.orderMgmtId_Status__c)){
                    checkOrderItemStatus=true;
                }          
            }
            if(Trigger.isUpdate && Trigger.isBefore && !checkOrderItemSubStatus){               
                if(String.isNotBlank(OLI.orderMgmtId_SubStatus__c)){
                    checkOrderItemSubStatus=true;
                }          
            }
            if(!orderIdSet.contains(OLI.orderid)){
                orderIdSet.add(OLI.orderid);
            }
            
        }
        System.debug(' isBPIIdPresent Inside Trigger is '+isBPIIdPresent);
        /*
        if(Trigger.isUpdate && Trigger.isBefore && (checkOrderItemSubStatus||checkOrderItemStatus)){
        OrdrUtilities.updateCharactersticsJsonBasedOnAmendReadiness(Trigger.newMap,false);
        }
        if(Trigger.isUpdate && Trigger.isBefore && (checkOrderItemSubStatus||checkOrderItemStatus) ){
        System.debug('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa Before updateCharactersticsJsonBasedOnAmendReadinessWithOIList');
        OrdrUtilities.updateCharactersticsJsonBasedOnAmendReadinessWithOIList(Trigger.new,false);
        for(orderItem OLI:trigger.new) { 
        System.debug('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa vlocity_cmt__JSONAttribute__c='+OLI.vlocity_cmt__JSONAttribute__c);
        }
        }*/
        system.debug('in trigger 121');
        if(null!=orderItemList && (Trigger.isUpdate || Trigger.isInsert) && Trigger.isBefore){
            //   system.debug('in trigger 122');
            callHelper.setServiceAccountAndBillingToOrderItem(orderItemList,orderIds,orderItemBillingAccount);
        }
        //isBPIIdPresent=true;
        if(Trigger.isUpdate && Trigger.isAfter && isBPIIdPresent){
            System.debug(' updateBPIInstanceId is about to be called ');
            OCOM_OrderProductBeforeTrigger_Helper.updateBPIInstanceId(Trigger.newMap);
            return;
        }
    } 
    //Added by Aditya Jamwal to cancel reserved WorkOrder after OLI is deleted.  
    if(Trigger.isDelete && Trigger.isAfter){
        System.debug(' Inside delete order after');}
    if(Trigger.isDelete && Trigger.isbefore){
        System.debug(' Inside delete order before');
        Set<String> orderItmIdList = new Set<String>();
        Set<Id> orderItmList = new Set<Id>();
        for(OrderItem oli : Trigger.old){
            if(null!=oli.work_order__c){
                orderItmIdList.add(oli.id);
            }
            else {
                orderItmList.add(oli.id);
            }
        }
        //   callHelper.cancelWorkOrderofDeletedOLIs(orderItmIdList);
        system.debug('!!SOQL Called before DeleteServiceAddressOrderEntry consumed '+ Limits.getQueries() +' Available' +Limits.getLimitQueries());
        callHelper.DeleteServiceAddressOrderEntry(orderItmList); //added on 22-NOV-2016 for multisite implementation By Piyush Nandan
        system.debug('!!SOQL Called after DeleteServiceAddressOrderEntry consumed '+ Limits.getQueries() +' Available' +Limits.getLimitQueries());
    }
    
    
    //.................................//   
    
    
    //Arpit : 14-Dec-2016 : WFM defect
    if(Trigger.isInsert && Trigger.isAfter ){    
        System.debug(' Inside create order line after'); 
        NAAS_UpdateOrderWFMHelper helper = new NAAS_UpdateOrderWFMHelper(); 
        system.debug('!!SOQL Called before NAAS setBookableProductCount  consumed '+ Limits.getQueries() +' Available' +Limits.getLimitQueries());
        helper.setBookableProductCount(trigger.new) ;
        system.debug('!!SOQL Called after NAAS setBookableProductCount consumed '+ Limits.getQueries() +' Available' +Limits.getLimitQueries());
        
    }
    
    if(Trigger.isDelete && Trigger.isBefore){
        System.debug(' Inside delete order Line before'); 
        NAAS_UpdateOrderWFMHelper helper = new NAAS_UpdateOrderWFMHelper(); 
          system.debug('!!SOQL Called before NAAS decreaseBookableProductCount consumed '+ Limits.getQueries() +' Available' +Limits.getLimitQueries());
        helper.decreaseBookableProductCount(trigger.old) ;
          system.debug('!!SOQL Called after NAAS decreaseBookableProductCount consumed '+ Limits.getQueries() +' Available' +Limits.getLimitQueries());
    }
    // Coding ends
    
    //Arpit : 6th-Jan-2017 : Checkout readiness : Oli Updation logic 
Boolean wasMultiSiteOLIDetected = false;
List<OrderItem> ListOliofMultiseiteOrder = new list<OrderItem>();
if(Trigger.isInsert || Trigger.isUpdate){
    for(OrderItem oliObj : Trigger.new){
        if(oliObj.IsMultiSiteOLI__c){ 
            wasMultiSiteOLIDetected  = true;
            ListOliofMultiseiteOrder.add(oliObj);
        }
    }
}

//Added by Prashant on 17/March/2017 ACD requirement- OLI Change scenario + Disconnect scenario
if(Trigger.Isbefore){
    if(Trigger.IsUpdate){
        
        //OCOM_ACD_ChangeClass.CompareJsonAttribute(trigger.oldmap, trigger.newmap);
          for(orderitem oli : trigger.newmap.values()){
            Map<String, Object> OldJson = null;
            Map<String, Object> NewJson = null;
            if(trigger.oldmap.get(oli.id) != null && trigger.oldmap.get(oli.id).vlocity_cmt__JSONAttribute__c != null) {
                OldJson = (Map<String, Object>)JSON.deserializeUntyped(trigger.oldmap.get(oli.id).vlocity_cmt__JSONAttribute__c);
            }
            if(trigger.newmap.get(oli.id) != null && trigger.newmap.get(oli.id).vlocity_cmt__JSONAttribute__c != null) {
                NewJson = (Map<String, Object>)JSON.deserializeUntyped(trigger.newmap.get(oli.id).vlocity_cmt__JSONAttribute__c);
            }
            //system.debug('>>>>>CompareJsonAttribute::OldJson.equals(NewJson):::'+OldJson.equals(NewJson));
            if(OldJson != null && NewJson != null && !OldJson.equals(NewJson) && trigger.oldmap.get(oli.id) != null && 'Active' == trigger.oldmap.get(oli.id).vlocity_cmt__ProvisioningStatus__c){
                oli.vlocity_cmt__ProvisioningStatus__c = 'Changed';  
                system.debug('ACD>>>CHANGE Scenario>>>>>>::'+oli.id) ;           
            }
            
        }
        
    }
    
}
    //if(Trigger.IsAfter && Trigger.IsUpdate && checkRecursiveOLIBeforeTrigger.isFirstTime){
if(Trigger.IsAfter && Trigger.IsUpdate && checkRecursiveOLIBeforeTrigger.isFirstTime){
    Set<id> SetOfOliDisconnected = new set<id>();
        Set<id> SetOfTOpOfferOliDisconnected = new set<id>();
        Set<id> SetOfRootId = New set<id>();
        set<id> SetOdOLIId = New set<id>();
        List<orderitem> ListOfOLItoUpdate =  new list<orderitem>();
        List<orderitem> ListOfOLItoUpdateByRootid =  new list<orderitem>();
        Map<id,orderitem> MapOfOLItoUpdate =  new Map<id,orderitem>();
        //Set<id> SetOfOLIIDtoupdate = new set<id>();
        system.debug('Disconnected>>>>>>>::'+trigger.newmap.keyset());
    // logic for Disconnected
        for(orderitem oli : trigger.newmap.values()){

            if(oli.vlocity_cmt__ProvisioningStatus__c == 'Disconnected' && trigger.oldmap.get(oli.id).vlocity_cmt__ProvisioningStatus__c != trigger.newmap.get(oli.id).vlocity_cmt__ProvisioningStatus__c ){
                if(oli.vlocity_cmt__ParentItemId__c == null || oli.vlocity_cmt__ParentItemId__c == '' ){
                    SetOfTOpOfferOliDisconnected.add(oli.id);
                    SetOfRootId.add(oli.vlocity_cmt__RootItemId__c);
                    system.debug('Disconnected>>>>>>>Top::'+oli.id);
                }
                else{
                    SetOfOliDisconnected.add(oli.id);
                    system.debug('Disconnected>>>>>>>NonTop::'+oli.id); 
                }
                
                SetOdOLIId.add(oli.id);
            }
         }   
    if(SetOfTOpOfferOliDisconnected.size()>0){
                ListOfOLItoUpdate = [select id,vlocity_cmt__RootItemId__c,vlocity_cmt__ProvisioningStatus__c,vlocity_cmt__ParentItemId__c  from orderitem where vlocity_cmt__RootItemId__c in:SetOfRootId and id not in: SetOfTOpOfferOliDisconnected];
                for(orderitem oli : ListOfOLItoUpdate ){
                    
                  
                    oli.vlocity_cmt__ProvisioningStatus__c  = 'Disconnected';
                    MapOfOLItoUpdate.put(oli.id,oli);
                  
                }
            }
            system.debug('>>>>>>SetOfOliDisconnected::'+SetOfOliDisconnected);
            if(SetOfOliDisconnected.size()>0){
                ListOfOLItoUpdateByRootid = [select id,vlocity_cmt__RootItemId__c,vlocity_cmt__ProvisioningStatus__c,vlocity_cmt__ParentItemId__c  from orderitem where vlocity_cmt__ParentItemId__c in: SetOfOliDisconnected and id not in: SetOfOliDisconnected];
                for(orderitem oli : ListOfOLItoUpdateByRootid ){
                    
                  
                    oli.vlocity_cmt__ProvisioningStatus__c  = 'Disconnected';
                    MapOfOLItoUpdate.put(oli.id,oli);
                  
                }
                
            }
           
        if(MapOfOLItoUpdate.size()>0 && checkRecursiveOLIBeforeTrigger.isFirstTime){
            checkRecursiveOLIBeforeTrigger.isFirstTime = false;
            system.debug('Disconnected>>>>>>>Update::'+MapOfOLItoUpdate.keyset());
            update MapOfOLItoUpdate.values();
        }
        system.debug('>>>>>>ListOfOLItoUpdate::'+ListOfOLItoUpdate);        
}
// to update the lookup fields
if(Trigger.IsAfter && (Trigger.IsInsert || Trigger.IsUpdate) && checkRecursiveOLIBeforeTrigger.isFirstTime){
    list<orderitem> listOfOlitoUpdate = new list<orderitem>();
    Map<id,orderitem> MapOfOlitoUpdate = new Map<id,orderitem>();
    set<id> SetOfOLIId = new Set<id>();
    set<id> SetOfRootOLIId = new Set<id>();
        
    for(orderitem oli : trigger.new){
        SetOfOLIId.add(oli.id);
         system.debug('OLI Before Trigger>>ACD11>>>>>'+oli.vlocity_cmt__ParentItemId__c+'>>>OLIID>>>::'+oli.id);
         if((oli.vlocity_cmt__ProvisioningStatus__c == 'New' || oli.vlocity_cmt__ProvisioningStatus__c == 'Disconnected') && oli.vlocity_cmt__ParentItemId__c != null){
            SetOfOLIId.add(oli.vlocity_cmt__RootItemId__c); 
            SetOfRootOLIId.add(oli.vlocity_cmt__RootItemId__c);
         }
         
    }
             
        for(orderitem oli : [Select id,orderMgmt_BPI_Id__c,vlocity_cmt__RootItemId__c,vlocity_cmt__ParentItemId__c,ParentOrderProductLookup__c,vlocity_cmt__ProvisioningStatus__c from orderitem where id in : SetOfOLIId ]){
            system.debug('OLI Before Trigger>>ACD>>>>>'+oli.vlocity_cmt__ParentItemId__c+'>>>OLIID>>>::'+oli.id);
            if(oli.vlocity_cmt__ParentItemId__c != null){
                oli.ParentOrderProductLookup__c = oli.vlocity_cmt__ParentItemId__c;
                
                listOfOlitoUpdate.add(oli);
                
                MapOfOlitoUpdate.put(oli.id,oli);
            }
            else if(oli.vlocity_cmt__ParentItemId__c == null && oli.id == oli.vlocity_cmt__RootItemId__c && SetOfRootOLIId.contains(oli.id) && oli.vlocity_cmt__ProvisioningStatus__c == 'Active'){ // update top offer's Provisioning status to Change
                oli.vlocity_cmt__ProvisioningStatus__c = 'Changed';
                
                system.debug('>>>>>>@@@@'+oli.id);
                MapOfOlitoUpdate.put(oli.id,oli);
            }
            
        }
        checkRecursiveOLIBeforeTrigger.isFirstTime = false;
        if(MapOfOlitoUpdate.size()>0){
            update MapOfOlitoUpdate.values();
        }
        system.debug('>>>>>>ListOfOLItoUpdate:: after insert'+ListOfOLItoUpdate);
    }
    
//End of Code added by Prashant
    system.debug(  'OCOM_CustomEligibilityHookHelper.addedOli --->' + OCOM_CustomEligibilityHookHelper.addedOli );

    if( Trigger.IsUpdate && Trigger.isBefore && wasMultiSiteOLIDetected) { // Commented on 2-Jan-2017 
        
        OCOM_CustomEligibilityHookHelper.checkoutReadinessOnOliUpdation(ListOliofMultiseiteOrder, OCOM_CustomEligibilityHook.deletedOliIDSet, OCOM_CustomEligibilityHookHelper.addedOli );
    }


    if(Trigger.IsInsert && Trigger.isAfter){
        List<OrderItem> updateOiObjList=new List<OrderItem>();
        if(orderToOrderIdMap!=null){
            for(String keyStr:orderToOrderIdMap.keySet()){
                Order ord=orderToOrderIdMap.get(keyStr);
                if(ord!=null && ord.OrderItems!=null){
                    for(OrderItem oiInst:ord.OrderItems){
                        if(!(oiInst.vlocity_cmt__AssetReferenceId__c instanceof Id)){
                            oiInst.vlocity_cmt__AssetReferenceId__c='';
                        }
                        if(String.isBlank(oiInst.vlocity_cmt__AssetReferenceId__c) || (oiInst.vlocity_cmt__AssetReferenceId__c!=oiInst.id)){
                            oiInst.vlocity_cmt__AssetReferenceId__c=oiInst.id;
                            updateOiObjList.add(oiInst);
                        }
                    }
                }
            }
        }        
        if(!updateOiObjList.isEmpty()){
            update updateOiObjList;
        }
    }

}