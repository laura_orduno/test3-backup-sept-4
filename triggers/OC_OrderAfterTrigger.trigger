/*
    *******************************************************************************************************************************
    Class Name:     OCOM_OrderBeforeTrigger
    Purpose:        OrderProduct Trigger        
    Created by:     Sandip Chaudhari           04-Aug-2017    BMPF -18 Mini PI No previous version
    *******************************************************************************************************************************
*/
trigger OC_OrderAfterTrigger on Order (after update) {
    OC_OrderStatusHelper.updateMasterOrderStatus(Trigger.new);
}