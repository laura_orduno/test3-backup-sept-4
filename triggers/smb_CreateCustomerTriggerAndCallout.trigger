/*
###########################################################################
# File..................: smb_CreateCustomerTriggerAndCallout
# Version...............: 26
# Created by............: Vinay Sharma
# Created Date..........: 04-Apr-2013
# Last Modified by......: 
# Last Modified Date....: 
# Description...........: This trigger is fired on acoount insertion, It involves future method call which then calls a web service to get customer details
#
# Copyright (c) 2000-2012. Astadia, Inc. All Rights Reserved.
#
# Created by the Astadia, Inc. Modification must retain the above copyright notice.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any commercial purpose, without fee, and without a written
# agreement from Astadia, Inc., is hereby forbidden. Any modification to source
# code, must include this paragraph and copyright.
#
# Permission is not granted to anyone to use this software for commercial uses.
#
# Contact address: 2839 Paces Ferry Road, Suite 350, Atlanta, GA 30339
# Company URL : http://www.astadia.com
###########################################################################
*/
trigger smb_CreateCustomerTriggerAndCallout on Account (after insert) 
{
/***** DEPRECATED: CP calls are now synchronous   
    smb_CreateCustomerTriggerCalloutHelper objhelper =  new smb_CreateCustomerTriggerCalloutHelper();
    objhelper.prepareCustomerServiceCall(trigger.new);
*/    
}