trigger tbo_CaseAccountBefore on Case_Account__c(before insert, before update, after delete) {
    
    set<Id> billingAcclist = new set<Id>();
    Map<id,SMBCare_Address__c> AccsVsSerLocations = new Map<Id, SMBCare_Address__c>();
    if(trigger.isbefore && (trigger.isinsert  || trigger.isupdate)){
        for(Case_Account__c caseAcc:trigger.new)
        {
            if(caseAcc.Account__c!=null)
                billingAcclist.add(caseAcc.Account__c);
        }
        list<SMBCare_Address__c> addrList= [select Id,Name,Account__c 
            from SMBCare_Address__c where Account__c in: billingAcclist];
        
        for(SMBCare_Address__c serloc:addrList)
        {
            if(serloc.Account__c!=null)
                AccsVsSerLocations.put(serloc.Account__c,serloc);
        }
        
        for(Case_Account__c caseAcc:trigger.new)
        {
            if(caseAcc.Account__c!=null)
            {
                if(AccsVsSerLocations.containskey(caseAcc.Account__c))
                    caseAcc.address__c=AccsVsSerLocations.get(caseAcc.Account__c).id;
                //else
                    //caseAcc.address__c=;
            }
            
        }
    
    }   
    
    
    // After delete code
    /*
    US# 138: Delete consolidated parent billing case account 
    if all childs associated to it are deleted from case account
    */
    //list<string> strBCAN = new list<string>();
    set<id> delAccsIds =new set<id>();
    set<id> caseRef = new set<id>();
    if(Trigger.isdelete && trigger.isAfter){
        
        for(case_account__c caseAcc: trigger.old)
        {
            if(caseAcc.recordtypeid==Schema.SObjectType.Case_account__c.getRecordTypeInfosByName().get('Existing').getRecordTypeId() && caseAcc.case__c!=null) caseRef.add(caseAcc.case__c); //
                
            if(caseAcc.recordtypeid==Schema.SObjectType.Case_account__c.getRecordTypeInfosByName().get('Existing').getRecordTypeId() && caseAcc.account__c!=null) delAccsIds.add(caseAcc.account__c);
        }
        
        list<case_account__c> cbnCaseAccs = new list<case_account__c>([select id,name, case__c, account__c,account__r.BCAN__c, account__r.BCAN_Pilot_Org_Indicator__c from case_account__c where case__c IN :caseRef ]);
        
        map<id, list<case_account__c>> CaseVscaseAcclist = new map<id, list<case_account__c>>();
        
        for (case_account__c ca: cbnCaseAccs){
            if(CaseVscaseAcclist.containskey(ca.case__c)){
                CaseVscaseAcclist.get(ca.case__c).add(ca);
            }
            else{
                list<case_account__c> calist= new list<case_account__c>();
                calist.add(ca);
                CaseVscaseAcclist.put(ca.case__c,calist);
            }
        }
        system.debug('>>>> CaseVscaseAcclist: '+ CaseVscaseAcclist);
        list<case_account__c> CaseAcctoDel = new list<case_account__c>();
        for(String Rcase: CaseVscaseAcclist.keyset()){
            list<string> strBCAN = new list<string>();
            map<string,list<case_account__c>> CBNvsCaseAcclist= new map<string,list<case_account__c>>();
            system.debug('>>> Case: '+Rcase+' >>> Case Accounts Size'+ CaseVscaseAcclist.get(Rcase).size());
            system.debug('>>> Case: '+Rcase+' >>> Case Accounts'+ CaseVscaseAcclist.get(Rcase));
            for(case_account__c ca: CaseVscaseAcclist.get(Rcase)){
                if(ca.account__r.BCAN__c != null && ca.account__r.BCAN_Pilot_Org_Indicator__c==1){
                    strBCAN.add(ca.account__r.BCAN__c);
                }
            }
            map<string,list<case_account__c>> BCANvslistCaseAcc = new map<string,list<case_account__c>>();
            system.debug('>>>> strBCAN: '+ strBCAN);
            
            for(case_account__c ca: CaseVscaseAcclist.get(Rcase)){
                for(String s: strBCAN){
                    //BCANvsCount.put(ca,0)
                    integer count=0;
                    if(ca.account__r.BCAN__c == s ){
                        if(BCANvslistCaseAcc.containskey(s)){
                            list<case_account__c> calist1= new list <case_account__c>(BCANvslistCaseAcc.get(s));
                            calist1.add(ca);
                            BCANvslistCaseAcc.put(s,calist1);
                        }
                        else{
                            list<case_account__c> calist= new list<case_account__c>();
                            calist.add(ca);
                            BCANvslistCaseAcc.put(s,calist);
                        }
                            
                    }
                }
                
            }
            system.debug('>>>> BCANvslistCaseAcc: '+ BCANvslistCaseAcc);
            
            for(String s: BCANvslistCaseAcc.keyset()){
                if(BCANvslistCaseAcc.get(s).size()==1){
                    CaseAcctoDel.addall(BCANvslistCaseAcc.get(s));
                }
            }
            system.debug('>>>> CaseAcctoDel: '+ CaseAcctoDel);
        }
        
        if(CaseAcctoDel!=null)
            delete CaseAcctoDel;
        
        
        
        
        
        
    }
    

}