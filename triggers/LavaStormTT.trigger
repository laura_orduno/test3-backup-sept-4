trigger LavaStormTT on LavaStorm_Trouble_Ticket__c (before insert , before update){
    List<String> billingAccounts = new List<String>();
    set<Id> accIdSet = new set<Id>();
    for(LavaStorm_Trouble_Ticket__c lsObj: trigger.new){
            if(lsObj.Problem_Description_Details__c != null){
                lsObj.Problem_Description_Details__c = lsObj.Problem_Description_Details__c.replace('**||**','\n\r');
            }
            if(lsObj.Status__c == 'Open' || lsObj.Status__c == 'Suspended' || lsObj.Status__c == '200' || lsObj.Status__c == '100'){
                if(lsobj.closed_date__c != null){
                    lsobj.closed_date__c = null;
                }
                if(lsobj.Time_of_Resolution__c != null){
                    lsobj.Time_of_Resolution__c = null;
                }
            }
            billingAccounts.add(lsObj.BAN__c);
    }
    System.debug('@@@billingAccounts ' + billingAccounts);
    List<Account> accList = new List<Account>();
    map<String,Id> accBanMap = new map<String,Id>();
    accList = [SELECT Id,BAN_CAN__c FROM Account 
               WHERE BAN_CAN__c != null AND BAN_CAN__c In: billingAccounts];
    System.debug('@@@accList  ' + accList);

    if(accList != null && accList.size()>0){
        for(Account accObj: accList){
            accBanMap.put(accObj.BAN_CAN__c, accObj.Id);
            accIdSet.add(accObj.Id);
        }
    }

    System.debug('@@@BAN List ' + accIdSet);
    map<Id,Id> accMap = smb_AccountUtility.getRcidAccountId(accIdSet);
    
    System.debug('@@@Map ' + accMap);
    if(accMap != null){
         for(LavaStorm_Trouble_Ticket__c lsObj: trigger.new){
            String errorFor = lsObj.BAN__c;
            if(accMap != null && accMap.size()>0){
                Id tmpAccId = accBanMap.get(lsObj.BAN__c);
                System.debug('@@@tmpAccId ' + tmpAccId);
                if(tmpAccId != null){
                    if(accMap.get(tmpAccId) != null){
                        lsObj.Account__c = accMap.get(tmpAccId);
                    }else{
                        lsObj.addError(System.Label.TT_RCIDNotExists + ' ' + errorFor);
                    }
                }else{
                    lsObj.addError(System.Label.TT_RCIDNotExists + ' ' + errorFor);
                }
            }else{
                lsObj.addError(System.Label.TT_RCIDNotExists + ' ' + errorFor);
            }
        }
    }
}