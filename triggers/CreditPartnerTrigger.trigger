trigger CreditPartnerTrigger on Credit_Partners__c (before insert, before delete) {
    
    System.debug('... dyy ... Trigger.isInsert = ' + Trigger.isInsert);
    System.debug('... dyy ... Trigger.isDelete = ' + Trigger.isDelete);
        
        
    System.debug('... dyy ... Trigger.size = ' + Trigger.size);
    System.debug('... dyy ... Trigger.new = ' + Trigger.new);
    System.debug('... dyy ... Trigger.old = ' + Trigger.old);
    
    List <Credit_Partners__c> partners;    
    if(Trigger.new!=null) partners = Trigger.new;
    if(Trigger.old!=null) partners = Trigger.old;
    
    String corporatePartnerId;
    String individualPartnerId;
    String creditProfileLinkId;
    
    if(partners!=null) 
        for(Credit_Partners__c partner:partners){
            if(partner.Corporate_Partner__c!=null) corporatePartnerId = partner.Corporate_Partner__c;
            if(partner.Individual_Partner__c!=null) individualPartnerId=partner.Individual_Partner__c;
            if(partner.Credit_Profile_Link__c!=null) creditProfileLinkId=partner.Credit_Profile_Link__c;
        }
    
    
    if(Trigger.isInsert){
        CreditProfileCalloutUpdate.triggerCalloutProxyToAddPartner(corporatePartnerId,individualPartnerId,creditProfileLinkId);
    }else if(Trigger.isDelete){        
        CreditProfileCalloutUpdate.triggerCalloutProxyToDeletePartner(corporatePartnerId,individualPartnerId,creditProfileLinkId);
    }
    
}