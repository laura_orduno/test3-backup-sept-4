trigger preventDupesOnProdSalesTeam on Product_Sales_Team__c (before insert, before update) {

//    Map<Id, Set<Id>> pliMembersMap = new Map<Id, Set<Id>>();
    Set<id> allMembers = new Set<id>();
    
    Map<String, Product_Sales_Team__c> pliMemberComboPstMap = new Map<String, Product_Sales_Team__c>();
  	Set<Id> allPLI = new Set<Id>();
  	  
    for (Product_Sales_Team__c pst : trigger.new) {
        if (Trigger.isUpdate) {
            if (pst.Member__c == trigger.oldMap.get(pst.Id).Member__c) {
                continue;
            }
        }
        Id pliId = pst.Product__c;
       /* Set<Id> members = pliMembersMap.get(pliId);
        if (members == null) {
            members = new Set<Id>();
            pliMembersMap.put(pliId, members);
        }
        members.add(pst.Member__c);*/
        allMembers.add(pst.Member__c);
        allPLI.add(pst.Product__c);
        
        String key = pst.Product__c + '' + pst.Member__c;

		if (pliMemberComboPstMap.containsKey(key)) {
			pst.addError('There is a duplicate Team Member on this Product.');
			continue;	
		}
		pliMemberComboPstMap.put(key, pst);
    }

    if (pliMemberComboPstMap.isEmpty()) {
        return; //nothing to check
    }

/*    
    if (pliMembersMap.isEmpty()) {
        return; //nothing to check
    }
*/    

    //get the opps for the plis
/*    List<Product_Sales_Team__c> pliList 
        = [Select Id, Member__c, Product__c 
            from Product_Sales_Team__c 
            where Product__c In :pliMembersMap.keySet() And Member__c In :allMembers];
    System.debug('pliList Size = ' + pliList.size());
*/    
    for (Product_Sales_Team__c pst : [Select Id, Member__c, Product__c 
            from Product_Sales_Team__c 
            where Product__c In :allPLI And Member__c In :allMembers]) {
        Id pliId = pst.Product__c;
        Id member = pst.Member__c;

		String key = pst.Product__c + '' + pst.Member__c;
		if (pliMemberComboPstMap.containsKey(key)) {
			Product_Sales_Team__c pst1 = pliMemberComboPstMap.get(key);
			pst1.addError('Member already exists on the Sales Team for this product.');
			continue;	
		}    	
    }
/*            
    for (Product_Sales_Team__c pst : pliList) {
        Id pliId = pst.Product__c;
        Id member = pst.Member__c;
        Set<id> pliMembers = pliMembersMap.get(pliId);
        System.debug('pliMembers = ' + pliMembers);
        System.debug('pliMembersMap = ' + pliMembersMap);
        System.debug('pliId = ' + pliId);
        if (pliMembers != null && pliMembers.contains(member)) {
           	trigger.newMap.get(pst.Id).addError('Member already exists on the Sales Team for this product');
            //trigger.new.get(0).addError('Member already exists on the Sales Team for this product');
            //break;
        }
    } 
 */   
}