trigger UpdateSerRequestTrigger on Service_Request_Contact__c (after insert, after update, after delete, after undelete) {
/*
###################################################################################################
# File..................: UpdateSerRequestTrigger
# Version...............: 26
# Created by............: Prashant Bhardwaj (IBM)
# Created Date..........: 14-Jul-2014
# Last Modified by......: I-yuan Han (IBM)
# Last Modified Date....: 22-Jul-2014
# Description ..........: Updates the 'Track__c' field of the Service Request to trigger the execution
#  of 'smb_ServiceRequestTrigger' which in turn executes class 'SRS_ServiceRequestValidationHelper'
#  to update the 'Required to Submit' field on the Service Request.
###################################################################################################
*/

    public List<Service_Request__c> serReq = new List<Service_Request__c>();
    public Set<Id> ServiceRequestIds;
    if(trigger.isInsert){   

        ServiceRequestIds= new Set<Id>();
        for(Service_Request_Contact__c srContact : trigger.new)
             ServiceRequestIds.add(srContact.Service_Request__c);
             
        for(Id srId : ServiceRequestIds){           
            Service_Request__c sr = new Service_Request__c(Id = srId);
            sr.Track__c = system.now();
            serReq.add(sr);
        }
    }
   /* if(trigger.isUpdate){
       ServiceRequestIds= new Set<Id>();
        for(Service_Request_Contact__c srContact : trigger.new)
             ServiceRequestIds.add(srContact.Service_Request__c);
             
        for(Id srId : ServiceRequestIds){           
            Service_Request__c sr = new Service_Request__c(Id = srId);
            sr.Track__c = system.now();
            serReq.add(sr);
        }
    }*/
    if(trigger.isDelete){
        ServiceRequestIds= new Set<Id>();
        for(Service_Request_Contact__c srContact : trigger.old)
             ServiceRequestIds.add(srContact.Service_Request__c);
             
        for(Id srId : ServiceRequestIds){           
            Service_Request__c sr = new Service_Request__c(Id = srId);
            sr.Track__c = system.now();
            serReq.add(sr);
        }      
    }
    if(trigger.isUndelete){
       ServiceRequestIds= new Set<Id>();
        for(Service_Request_Contact__c srContact : trigger.new)
             ServiceRequestIds.add(srContact.Service_Request__c);
             
        for(Id srId : ServiceRequestIds){           
            Service_Request__c sr = new Service_Request__c(Id = srId);
            sr.Track__c = system.now();
            serReq.add(sr);
        }     
    }
    system.debug('=====serReq===='+serReq);
    if(serReq != Null){
        update serReq;
    }   
}