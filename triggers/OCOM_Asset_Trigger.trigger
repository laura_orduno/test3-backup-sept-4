trigger OCOM_Asset_Trigger on Asset (after insert , before insert,before update, after update) {
    // get all newly created assets Id into Map with External Id
    if(Trigger.IsBefore)
    {
        Set<Id> orderItemIDs = new Set<Id>();
        Set<string> orderItemBPIDs = new Set<string>();
        List<Asset> updateAccountIdInAssetRecs=new List<Asset>();
        for(Asset assetrec: trigger.new)
        {
            if(String.isNotBlank(assetrec.vlocity_cmt__AssetReferenceId__c) && (assetrec.vlocity_cmt__AssetReferenceId__c instanceof Id)){
                orderItemIDs.add(assetrec.vlocity_cmt__AssetReferenceId__c);
            }
            if(String.isNotBlank(assetrec.vlocity_cmt__OrderProductId__c)){
                orderItemIDs.add(assetrec.vlocity_cmt__OrderProductId__c);
            }
            
            if( String.isnotBlank(assetrec.vlocity_cmt__AssetReferenceId__c) || String.isnotBlank(assetrec.vlocity_cmt__OrderProductId__c)){        //Trigger.IsInsert &&
                updateAccountIdInAssetRecs.add(assetrec);
            }
        }
        // Call OCOM helper class method
        if(updateAccountIdInAssetRecs.size()>0)
        {
            OCOM_Asset_TriggerHelper callUpdateAssetID = new OCOM_Asset_TriggerHelper();
            callUpdateAssetID.UpdateAccountIdinAssetRecBeforeUpsert(orderItemIDs,updateAccountIdInAssetRecs);
        }
        //Arpit : 20-April-2017 : ACD :: Resetting vlocity_cmt__ProvisioningStatus__c field to "Active" if current prov status was "Change"
        if(Trigger.isBefore && Trigger.isUpdate){
            for(Asset assetObj : Trigger.new){
                system.debug('assetObj.Status ------> ' + assetObj.Status);
                system.debug('assetObj.vlocity_cmt__ProvisioningStatus__c ------> ' +assetObj.vlocity_cmt__ProvisioningStatus__c);
                if(assetObj.Status != null  && (assetObj.vlocity_cmt__ProvisioningStatus__c != null || assetObj.vlocity_cmt__ProvisioningStatus__c != '')){
                    if(assetObj.Status.equalsIgnoreCase('Active') && assetObj.vlocity_cmt__ProvisioningStatus__c.equalsIgnoreCase('Changed') ) {
                        assetObj.vlocity_cmt__ProvisioningStatus__c = 'Active' ;
                    }
                }
            }
        }
    }
	if(Trigger.IsAfter && Trigger.IsInsert){
        if(Trigger.newMap==null){
            return;
        }
		Set<Id> assetIds=Trigger.newMap.keySet();
		//Set<Id> serviceAccountIds=new Set<Id>();
        Set<Id> orderIds=new Set<Id>();
		List<Asset> assetList=[ select vlocity_cmt__OrderId__c,vlocity_cmt__ServiceAccountId__c,vlocity_cmt__ParentItemId__c,vlocity_cmt__LineNumber__c,id from asset where id in :assetIds];
		for(Asset assetRef:assetList){
			/*if(String.isNotBlank(assetRef.vlocity_cmt__ServiceAccountId__c)){
				serviceAccountIds.add(assetRef.vlocity_cmt__ServiceAccountId__c);
			}*/
            if(String.isNotBlank(assetRef.vlocity_cmt__OrderId__c)){
				orderIds.add(assetRef.vlocity_cmt__OrderId__c);
			}
		}
		assetList=[ select vlocity_cmt__OrderId__c,vlocity_cmt__ServiceAccountId__c,vlocity_cmt__ParentItemId__c,vlocity_cmt__RootItemId__c ,vlocity_cmt__LineNumber__c,id from asset 
                   where vlocity_cmt__OrderId__c in :orderIds];
		Map<String,Id> parentLineNumMap=new Map<String,Id>();
		Map<String,Id> rootLineNumMap=new Map<String,Id>();
		Map<String,Id> lineNumMap=new Map<String,Id>();
		for(Asset assetRef:assetList){
			if(String.isNotBlank(assetRef.vlocity_cmt__LineNumber__c)){				
				lineNumMap.put(assetRef.vlocity_cmt__LineNumber__c,assetRef.id);
			}
		}
		
		//finally update 		
		for(Asset assetRef:assetList){
			if(String.isNotBlank(assetRef.vlocity_cmt__LineNumber__c) && assetRef.vlocity_cmt__LineNumber__c.indexOf('.')!=-1){
               String lineNum=assetRef.vlocity_cmt__LineNumber__c;
               String parentLineNum=lineNum.substringBeforeLast('.');
			   String rootLineNum=lineNum.substringBefore('.');	
                /*if(String.isBlank(parentLineNum)){
                    assetRef.vlocity_cmt__ParentItemId__c=assetRef.id;
                } else {
                    assetRef.vlocity_cmt__ParentItemId__c=lineNumMap.get(parentLineNum);
                }*/
                
                if(String.isNotBlank(parentLineNum)){
                    assetRef.vlocity_cmt__ParentItemId__c=lineNumMap.get(parentLineNum);
                }
                
                if(String.isBlank(rootLineNum)){
                    assetRef.vlocity_cmt__RootItemId__c=assetRef.id;
                } else {
                   assetRef.vlocity_cmt__RootItemId__c =lineNumMap.get(rootLineNum);
                }
					
            } else {
              // assetRef.vlocity_cmt__ParentItemId__c=assetRef.id;			   
				assetRef.vlocity_cmt__RootItemId__c =assetRef.id;	 
            }
            if(String.isNotBlank(assetRef.vlocity_cmt__ParentItemId__c) && (assetRef.vlocity_cmt__ParentItemId__c==assetRef.id) ){
                assetRef.vlocity_cmt__ParentItemId__c=null;
            }
		}
		update assetList;
	}
}