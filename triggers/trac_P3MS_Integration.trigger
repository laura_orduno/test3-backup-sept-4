trigger trac_P3MS_Integration on P3MS_Master_Product__c (after insert, after update) {

    Map<String, Id> pmpMap = new Map<String, Id>();
    Set<Id> XpmpIds = new Set<Id>();
    for (P3MS_Master_Product__c pmp : trigger.new) {
        pmpMap.put(pmp.Product_Cd__c, pmp.Id);
        XpmpIds.add(pmp.Id);
    }
    
    Product2[] changedProducts = new List<Product2>();
    for (Product2 product : [Select Id, Name, ProductCode From Product2 Where ProductCode in :pmpMap.keySet() and P3MS_Master_Product__c = null]) {
        if (pmpMap.containsKey(product.ProductCode)) {
            product.P3MS_Master_Product__c = pmpMap.get(product.ProductCode);
            XpmpIds.remove(pmpMap.get(product.ProductCode));
            changedProducts.add(product);
        }
    }
    update changedProducts;
    
    String[] missedProducts = new List<String>();
    for (Id i : XpmpIds) {
        P3MS_Master_Product__c p = trigger.newMap.get(i);
        if (p != null) {
            missedProducts.add(i + '('+p.Product_nm__c+' / '+p.Product_cd__c+')');
        }
    }
    String missedIds = String.join(missedProducts, ', ');
    if (missedIds != '') {
        QuoteUtilities.logNow(new P3MSException('Couldn\'t match P3MS Staging Records: ' + missedIds), false);
    }


    // Used to find Price Actions for term prices
    private static final string MtmMatch = 'mtm';
    private static final string OneYearMatch = '1 year';
    private static final string TwoYearMatch = '2 year';
    private static final string ThreeYearMatch = '3 year';

    // Accumulate all the PMP Ids
    Set<Id> pmpIds = trigger.newMap.keySet();
    
    // Fetch all products that reference these PMPs
    Map<Id, Product2> unwrappedProductMap = new Map<Id, Product2>(
        [Select Id, Name, P3MS_Master_Product__c From Product2 Where P3MS_Master_Product__c in :pmpIds]
    );
    
    // Accumulate all the unwrapped product Ids
    Set<Id> productIds = unwrappedProductMap.keySet();
    
    // Fetch all the editable PricebookEntry[s] for the loaded Products
    PricebookEntry[] unwrappedPricebookEntries = [Select Id, Product2Id From PricebookEntry Where Product2Id in :productIds and (UseStandardPrice = false or UseStandardPrice = null)];
    
    // Map PricebookEntries to Product
    Map<Id, PricebookEntry[]> productToPricebookEntryListMap = new Map<Id, PricebookEntry[]>();
    for (PricebookEntry entry : unwrappedPricebookEntries) {
        if (!productToPricebookEntryListMap.containsKey(entry.Product2Id)) {
            productToPricebookEntryListMap.put(entry.Product2Id, new List<PricebookEntry>());
        }
        productToPricebookEntryListMap.get(entry.Product2Id).add(entry);
    }
    
    // Fetch all the PriceActions that reference the loaded products
    SBQQ__PriceAction__c[] unwrappedPriceActions = [Select Id, Name, SBQQ__Rule__r.Name, SBQQ__Rule__r.SBQQ__Product__c From SBQQ__PriceAction__c Where SBQQ__Rule__r.SBQQ__Product__c in :productIds and SBQQ__Field__c = 'Unit Price'];
    
    // Map PriceActions to Product
    Map<Id, SBQQ__PriceAction__c[]> productToPriceActionListMap = new Map<Id, SBQQ__PriceAction__c[]>();
    for (SBQQ__PriceAction__c priceAction : unwrappedPriceActions) {
        if (!productToPriceActionListMap.containsKey(priceAction.SBQQ__Rule__r.SBQQ__Product__c)) {
            productToPriceActionListMap.put(priceAction.SBQQ__Rule__r.SBQQ__Product__c, new List<SBQQ__PriceAction__c>());
        }
        productToPriceActionListMap.get(priceAction.SBQQ__Rule__r.SBQQ__Product__c).add(priceAction);
    }
    
    // Wrap Products in preparation for data transform
    Map<Id, ProductWrapper> wrappedProductMap = new Map<Id, ProductWrapper>();
    for (Product2 product : unwrappedProductMap.values()) {
        ProductWrapper wrappedProduct = new ProductWrapper(product);
        
        // Nest PricebookEntries
        wrappedProduct.pricebookEntries = productToPricebookEntryListMap.get(product.id);
        
        // Nest Price Actions
        if (productToPriceActionListMap.containsKey(product.Id)) {
            for (SBQQ__PriceAction__c pa : productToPriceActionListMap.get(product.Id)) {
                String pan = pa.SBQQ__Rule__r.Name.toLowercase(); // Lowercase to catch typos and ease matching
                if (pan.contains(MtmMatch)) {
                    // Month to Month Price
                    wrappedProduct.priceActionMTM = pa;
                }
                else if (pan.contains(OneYearMatch)) {
                    // 1 Year Term Price
                    wrappedProduct.priceActionOneYear = pa;
                }
                else if (pan.contains(TwoYearMatch)) {
                    // 2 Year Term Price
                    wrappedProduct.priceActionTwoYear = pa;
                }
                else if (pan.contains(ThreeYearMatch)) {
                    // 3 Year Term Price
                    wrappedProduct.priceActionThreeYear = pa;
                }
            }
        }
        
        wrappedProductMap.put(product.Id, wrappedProduct);
    }
    
    // Process each wrapper - map fields
    for (ProductWrapper pw : wrappedProductMap.values()) {
        // Get the P3MS Master Product for this ProductWrapper
        P3MS_Master_Product__c pmp = trigger.newMap.get(pw.product.P3MS_Master_Product__c);
        
        // Use the ProductWrapper transfer data to do the actual data migration
        pw.transferData(pmp);
    }
    
    // Lastly, update all that data!
    update unwrappedProductMap.values();
    update unwrappedPricebookEntries;
    update unwrappedPriceActions;
    
    public static Decimal calculateTermPrice(Decimal basePrice, Decimal termDiscount) {
        if (basePrice == null) { return null; }
        if (termDiscount == null) { termDiscount = 0; }
        return notLessThanZero(basePrice - termDiscount);
    }
    
    public static Decimal notLessThanZero(Decimal input) {
        // Prevents negative device prices, just in case the ETL has difficulties
        if (input != null && input < 0) { input = 0; }
        return input;
    }
    
    public class ProductWrapper {
        public Product2 product {get; set;}
        public PricebookEntry[] pricebookEntries {get; set;}
        public SBQQ__PriceAction__c priceActionMTM {get; set;}
        public SBQQ__PriceAction__c priceActionOneYear {get; set;}
        public SBQQ__PriceAction__c priceActionTwoYear {get; set;}
        public SBQQ__PriceAction__c priceActionThreeYear {get; set;}
        
        public ProductWrapper(Product2 p) {
            product = p;
        }
        
        // Map data from P3MS Master Product to
        //    the native Product, PricebookEntry and custom PriceAction objects
        public void transferData(P3MS_Master_Product__c pmp) {
            // Text/date fields
            product.Name = pmp.Product_NM__c;
            product.ProductCode = pmp.Product_CD__c;
            product.Description = pmp.Product_Desc__c;
            product.End_of_Life_Date__c = pmp.Eol_Date__c;
            
            // Blanks all price fields to avoid partial overwrites - don't do this
            //emptyAllPrices();
            
            // If we don't have a base price, stop here.
            if (pmp.Base_Price_Amt__c == null) { return; }
            
            setDefaultPrice(pmp.Base_Price_Amt__c);
            setMTMPrice(pmp.Base_Price_Amt__c);
            if (pmp.Term_Credit_12__c != null) {
                setOneYearPrice(calculateTermPrice(pmp.Base_Price_Amt__c, pmp.Term_Credit_12__c));
            } else {
                setOneYearPrice(pmp.Base_Price_Amt__c);
            }
            if (pmp.Term_Credit_24__c != null) {
                setTwoYearPrice(calculateTermPrice(pmp.Base_Price_Amt__c, pmp.Term_Credit_24__c));
            } else {
                setTwoYearPrice(pmp.Base_Price_Amt__c);
            }
            if (pmp.Term_Credit_36__c != null) {
                setThreeYearPrice(calculateTermPrice(pmp.Base_Price_Amt__c, pmp.Term_Credit_36__c));
            } else {
                setThreeYearPrice(pmp.Base_Price_Amt__c);
            }
        }
        
        public void emptyAllPrices() {
            setDefaultPrice(null);
            setOneYearPrice(null);
            setTwoYearPrice(null);
            setThreeYearPrice(null);
        }
        
        public void setDefaultPrice(Decimal price) {
            // Updates the standard price for the product
            if (pricebookEntries == null) { return; }
            for (PricebookEntry pbe : pricebookEntries) {
                pbe.UnitPrice = price;
            }
        }
        
        public void setMTMPrice(Decimal price) {
            // Updates the no-contract price
            product.MTM__c = price; // UI Display field
            if (priceActionMTM == null) { return; }
            priceActionMTM.SBQQ__Value__c = String.valueOf(price);
        }
        
        public void setOneYearPrice(Decimal price) {
            product.X1_Year__c = price; // UI Display field
            if (priceActionOneYear == null) { return; }
            priceActionOneYear.SBQQ__Value__c = String.valueOf(price);
        }
        
        public void setTwoYearPrice(Decimal price) {
            product.X2_Year__c = price; // UI display field
            if (priceActionTwoYear == null) { return; }
            priceActionTwoYear.SBQQ__Value__c = String.valueOf(price);
        }
        
        public void setThreeYearPrice(Decimal price) {
            product.X3_Year__c = price; // UI display field
            if (priceActionThreeYear == null) { return; }
            priceActionThreeYear.SBQQ__Value__c = String.valueOf(price);
        }
    }
}