trigger UpdateSerRequest on SRS_Service_Request_Charge__c (after insert, after update, after delete, after undelete) {
/*
###################################################################################################
# File..................: UpdateSerRequest
# Version...............: 26
# Created by............: Prashant Bhardwaj (IBM)
# Created Date..........: 14-Jul-2014
# Last Modified by......: I-yuan Han (IBM)
# Last Modified Date....: 22-Jul-2014
# Description ..........: Updates the 'Track__c' field of the Service Request to trigger the execution
#  of 'smb_ServiceRequestTrigger' which in turn executes class 'SRS_ServiceRequestValidationHelper'
#  to update the 'Required to Submit' field on the Service Request.
###################################################################################################
*/

    public List<Service_Request__c> serReq = new List<Service_Request__c>();
    public Set<Id> ServiceRequestIds;
    public Static final Set<string> setINCLUDED_BILLABLE_CHARGE_TYPES = new Set<string>{'Access Construction','Pre-Wire','Expedite'};  
    if(trigger.isInsert){       
        ServiceRequestIds = new Set<Id>();
        for(SRS_Service_Request_Charge__c biilableCharges : trigger.new)
        {
             if(biilableCharges.Charge_Type__c!=null && setINCLUDED_BILLABLE_CHARGE_TYPES.contains(biilableCharges.Charge_Type__c))
             ServiceRequestIds.add(biilableCharges.Service_Request__c);
        }
             
        for(Id srId : ServiceRequestIds){           
            Service_Request__c sr = new Service_Request__c(Id = srId);
            sr.Track__c = system.now();
            serReq.add(sr);
        }
    }
    if(trigger.isUpdate){
        ServiceRequestIds = new Set<Id>();
        for(SRS_Service_Request_Charge__c newBiilableCharges : trigger.new)
        {
             SRS_Service_Request_Charge__c oldBiilableCharges = trigger.oldmap.get(newBiilableCharges.id);
             if((newBiilableCharges.Charge_Type__c!=oldBiilableCharges.Charge_Type__c) 
                 && setINCLUDED_BILLABLE_CHARGE_TYPES.contains(newBiilableCharges.Charge_Type__c))
             ServiceRequestIds.add(newBiilableCharges.Service_Request__c);
        }
             
        for(Id srId : ServiceRequestIds){           
            Service_Request__c sr = new Service_Request__c(Id = srId);
            sr.Track__c = system.now();
            serReq.add(sr);
        }
    }

    if(trigger.isDelete){        
        ServiceRequestIds = new Set<Id>();
        integer i=0;
        for(SRS_Service_Request_Charge__c biilableCharges : trigger.old)
        {
             if(biilableCharges.Status__c=='Submitted')
             trigger.old[i].addError('Billable charges cannot be deleted once submitted.');
             if(setINCLUDED_BILLABLE_CHARGE_TYPES.contains(biilableCharges.Charge_Type__c))
             ServiceRequestIds.add(biilableCharges.Service_Request__c);
                          
             i++;
        }
             
        for(Id srId : ServiceRequestIds){           
            Service_Request__c sr = new Service_Request__c(Id = srId);
            sr.Track__c = system.now();
            serReq.add(sr);
        }     
    }

    if(trigger.isUndelete){
        ServiceRequestIds= new Set<Id>();
        for(SRS_Service_Request_Charge__c biilableCharges : trigger.new)
        {
            if(biilableCharges.Charge_Type__c!=null && setINCLUDED_BILLABLE_CHARGE_TYPES.contains(biilableCharges.Charge_Type__c))
            { 
                ServiceRequestIds.add(biilableCharges.Service_Request__c);
            }
        }
             
        for(Id srId : ServiceRequestIds){           
            Service_Request__c sr = new Service_Request__c(Id = srId);
            sr.Track__c = system.now();
            serReq.add(sr);
        }       
    }
    system.debug('=====serReq===='+serReq);
    if(serReq != Null){
        update serReq;
    }   
}