/* 
###########################################################################
# File..................: smb_DeleteAssetForDeletedQL
# Version...............: 26
# Created by............: Vinay Sharma
# Created Date..........: 16-May-2013
# Last Modified by......: 
# Last Modified Date....: 
# Description...........: This trigger will delete a corrosponding asset on deletion of a quoteline Item.
#
# Copyright (c) 2000-2012. Astadia, Inc. All Rights Reserved.
#
# Created by the Astadia, Inc. Modification must retain the above copyright notice.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any commercial purpose, without fee, and without a written
# agreement from Astadia, Inc., is hereby forbidden. Any modification to source
# code, must include this paragraph and copyright.
#
# Permission is not granted to anyone to use this software for commercial uses.
#
# Contact address: 2839 Paces Ferry Road, Suite 350, Atlanta, GA 30339
# Company URL : http://www.astadia.com
###########################################################################
*/
trigger smb_DeleteAssetForDeletedQL on SBQQ__QuoteLine__c (before delete) 
{
	list<asset> lstAssetsForDeletion = new list<asset>();
	
	lstAssetsForDeletion = [Select Id,SBQQ__QuoteLine__c from Asset where SBQQ__QuoteLine__c in :trigger.old ];
	if (lstAssetsForDeletion.size()>0)
   		{
   			try
   			{   				
   				delete lstAssetsForDeletion;
   			} 
   			catch (DmlException e) 
   			{
        		System.debug(e.getMessage());
    		}
   		} 		
}