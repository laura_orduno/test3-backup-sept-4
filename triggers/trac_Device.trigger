trigger trac_Device on Device__c (before insert,before update,after insert,after update) {
	trac_DeviceTriggerDispatcher dis = new trac_DeviceTriggerDispatcher();
	dis.init();
	dis.execute();
	dis.finish();
}