/*
###########################################################################
# File..................: SMB_CreateTaskforSoonestFieldWorkDueDate 
# Version...............: 29
# Created by............: Kanishk Prasad
# Created Date..........: 23/0/2014
# Last Modified by......: 
# Last Modified Date....: 
# Description...........: This Trigger will create a Task for the Opportunity owner for reminder 2 days before the 
#                         Soonest FieldWork Date on before Insert ,After Insert and After Update events of Opportunity and 
#
# Copyright (c) 2000-2012. Astadia, Inc. All Rights Reserved.
#
# Created by the Astadia, Inc. Modification must retain the above copyright notice. 
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any commercial purpose, without fee, and without a written
# agreement from Astadia, Inc., is hereby forbidden. Any modification to source
# code, must include this paragraph and copyright.
#
# Permission is not granted to anyone to use this software for commercial uses.
#
# Contact address: 2839 Paces Ferry Road, Suite 350, Atlanta, GA 30339
# Company URL : http://www.astadia.com
###########################################################################
*/



trigger SMB_CreateTaskforSoonestFieldWorkDueDate on Opportunity (after update) 
{

    List<Opportunity> lstOpportunity=new List<Opportunity>();
    List<Opportunity> lstOpportunitiesTasksTobeDeleted=new List<Opportunity>();
    //Map<Id,RecordType> mapRecordTypes=new Map<Id,RecordType>([select id from recordtype where sobjectType ='Opportunity' and developername like 'SMB_Care%']);

    set<Id> setSMBRecordTypes=SMB_Helper.fetchOpportunitySMBCareRecordTypes(null);

    for(Opportunity opp : Trigger.New)
    {
//        if(mapRecordTypes.containsKey(opp.recordTypeId))
        if(setSMBRecordTypes.contains(opp.recordTypeId))
        {
            if(null!=opp.Soonest_Fieldwork_Due_Date__c && Trigger.oldMap.get(opp.id).Soonest_Fieldwork_Due_Date__c<>Trigger.NewMap.get(opp.id).Soonest_Fieldwork_Due_Date__c)
            {
                lstOpportunity.add(opp);
            }
            else if(Trigger.oldMap.get(opp.id).Soonest_Fieldwork_Due_Date__c<>null && Trigger.NewMap.get(opp.id).Soonest_Fieldwork_Due_Date__c==null )
            {
                lstOpportunitiesTasksTobeDeleted.add(opp);
            }
        }        
    }
    delete [Select id from Task where whatid in:lstOpportunitiesTasksTobeDeleted and subject = 'Follow up with Back Office on Legacy Order'];
    List<Task> lstTask=new List<Task>();
//    List<Task> lstExistingTasks=[Select id from Task where whatid in:lstOpportunity and subject = 'Follow up with Back Office on Legacy Order'];
    delete [Select id from Task where whatid in:lstOpportunity and subject = 'Follow up with Back Office on Legacy Order'];
    for(Opportunity opp:lstOpportunity)
    {
        Task newActivity = new Task();
        newActivity.WhatId = opp.Id;
        newActivity.Type='Email';
        newActivity.ActivityDate=Date.valueOf(opp.Soonest_Fieldwork_Due_Date__c)-2;
        newActivity.Ownerid = opp.ownerId;
        newActivity.Subject='Follow up with Back Office on Legacy Order';
        lstTask.add(newActivity );
    }
    insert lstTask;

}