/**
 * This Trigger will execute Following Functions
 * 1) Before Insert: Populate Sales Manager and Sales Director on the Deal Support Object: Sales Manager is defined  in User Profile
 * 2) Before Update: Increase Current Record version Number By 1, clone currnet one as a copy and update owner to create By user 
 *    when status changed to Approved, Review Required or In Progress
 * 3) After Update: Apply Manual View Sharing to Creator View Status when Record is Submitted
 */ 

trigger DealSupportTrigger on Offer_House_Demand__c (before insert, before update, after update,after Insert) {
    static List<SObject> shareOBjListToInsert = new List<SObject>();
    static LIst<Offer_House_Demand__c> dealVertoInsert= new  LIst<Offer_House_Demand__c>();
    static Map<ID, ID> ownerManagers = new Map<ID, ID>();
    static Map<ID, ID> ownerDirectors = new Map<ID, ID>();
    static Map<id,Offer_House_Demand__c> WLNDeals = new  Map<Id,Offer_House_Demand__c>();
    static String WLN_RECORD_TYPE_ID=[Select id from RecordType where sObjectType = 'Offer_House_Demand__c' and developerName ='WLN_DS' ].id;
    if(trigger.isBefore && Trigger.isInsert){
        initManagers();      
    }
    for (Offer_House_Demand__c current : trigger.new) {
        if(trigger.isBefore && Trigger.isInsert){
            handleBeforeInsert(current);
        }else if(Trigger.isBefore && Trigger.isUpdate ){
            handleBeforeUpdate(current);
        }else if(Trigger.isAfter && Trigger.isUpdate){
            handleAfterUpdate(current);
        }else if(Trigger.isAfter && Trigger.isInsert){
           System.debug(LoggingLevel.ERROR, 'After Insert Only ' +  current + ' ->' +current.RecordType.Name) ;
           if(WLN_RECORD_TYPE_ID.equalsIgnoreCase((String)current.RecordTypeID)){
                WLNDeals.put(current.OPPORTUNITY__c,current);
           }
           System.debug(LoggingLevel.ERROR, 'WLN Deals only ' + WLNDeals) ;  
        }
        
    }
    
    if(trigger.isAfter && Trigger.isInsert){
        updateOpportunityProductItems();
    }
     
    if(Trigger.isBefore && Trigger.isUpdate && dealVertoInsert.size()>0 ){
        insert dealVertoInsert;
    }else if(Trigger.isAfter && Trigger.isUpdate && shareOBjListToInsert.size()>0){
        Database.insert(shareOBjListToInsert,false); 
    }
   
    
    private static void handleBeforeInsert(Offer_House_Demand__c item){
        // Use Case A: OHD Owner has a manager and a director
        item.Owner_Manager__c = ownerManagers.containsKey(item.OwnerId) ? ownerManagers.get(item.OwnerId) :null;
        item.Owner_Director__c = ownerDirectors.containsKey(ownerManagers.get(item.OwnerId)) ?ownerDirectors.get(ownerManagers.get(item.OwnerId)) :null; 
    } 
    
   private static void  handleBeforeUpdate( Offer_House_Demand__c item){
        incrementVersion(item);
        updateOwner(item);
        updateDealSupportAgent(item);
        updateConractAgent(item);
   }
    
   private static void handleAfterUpdate( Offer_House_Demand__c item){
        applyApexSharing(item);
   }
 
   private static void incrementVersion(Offer_House_Demand__c theOffer) {
        if('Approved'.equalsIgnoreCase(theOffer.Status__c) && Util.hasChanges(new Set<String>{'Status__c'}, trigger.oldmap.get(theOffer.id), theOffer)){
            theOffer.Offer_House_Agent__c =  UserInfo.getUserId();
            Offer_House_Demand__c theNewVersion = theOffer.clone(false, true,false,true);
            theNewVersion.Type__c = 'Version';
            theNewVersion.Offer_House_Demand__c = theOffer.Id;
            theNewVersion.OPPORTUNITY__c =null;
            dealVertoInsert.add(theNewVersion);
            theOffer.Version_Number__c = theOffer.Version_Number__c == null ? 0 : theOffer.Version_Number__c + 1;
            if (!'Approved'.equalsIgnoreCase(theOffer.Approval_Action__c)) theOffer.Record_Modified__c = false;
       
        } 
    }
    
   private static void updateOwner(Offer_House_Demand__c theOffer) {
    	// get record type map
		Map<ID, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Offer_House_Demand__c.getRecordTypeInfosById();
        String recordTypeName = rtMap.get(theOffer.recordTypeId).getName();
       
        System.debug('Deal Support, updateOwner, recordTypeName = ' + recordTypeName);
        System.debug('Deal Support, updateOwner, theOffer.Status__c = ' + theOffer.Status__c);
       
        if(('In Progress'.equalsIgnoreCase(theOffer.Status__c) || 'Review Required'.equalsIgnoreCase(theOffer.Status__c) || 'Approved'.equalsIgnoreCase(theOffer.Status__c) 
           || 'Contract Incomplete'.equalsIgnoreCase(theOffer.Status__c) || 'Contract Ready'.equalsIgnoreCase(theOffer.Status__c)) ){
            theOffer.OwnerId = theOffer.CreatedById;
        }
//        else if(recordTypeName == 'EPP Deal Support' && 'Create Contract'.equalsIgnoreCase(theOffer.Status__c)){
//            theOffer.OwnerId = theOffer.CreatedById;           
//            System.debug('Deal Support, updateOwner, OWNER UPDATED!');
//        }
       
        if( ('Review Required'.equalsIgnoreCase(Trigger.OldMap.get(theOffer.id).Status__c) && 'Resubmitted'.equalsIgnoreCase(theOffer.Status__c))){
           theOffer.OwnerId = theOffer.Offer_House_Agent__c;
        }else if(('Contract Incomplete'.equalsIgnoreCase(Trigger.OldMap.get(theOffer.id).Status__c) && 'Contract Resubmitted'.equalsIgnoreCase(theOffer.Status__c))){
            theOffer.OwnerId = theOffer.Deal_Support_Contract_Agent__c;
        }
    }
    
    private static void updateDealSupportAgent(Offer_House_Demand__c theOffer) {
        if(('Open'.equalsIgnoreCase(Trigger.OldMap.get(theOffer.id).Status__c) || 'Submitted'.equalsIgnoreCase(Trigger.OldMap.get(theOffer.id).Status__c)) && 'Review Required'.equalsIgnoreCase(theOffer.Status__c)){
           theOffer.Offer_House_Agent__c =  UserInfo.getUserId();
        }
    }
    private static void updateConractAgent(Offer_House_Demand__c theOffer) {
        if(('Contract Ready'.equalsIgnoreCase(Trigger.OldMap.get(theOffer.id).Status__c) || 'Contract In Progress'.equalsIgnoreCase(Trigger.OldMap.get(theOffer.id).Status__c)) && 'Contract Incomplete'.equalsIgnoreCase(theOffer.Status__c)){
           theOffer.Deal_Support_Contract_Agent__c =  UserInfo.getUserId();
        }
    }
    
     
   private static void applyApexSharing(Offer_House_Demand__c deal){
      if('Submitted'.equalsIgnoreCase(deal.Status__c) || 'Resubmitted'.equalsIgnoreCase(deal.Status__c)
            || 'Create Contract'.equalsIgnoreCase(deal.Status__c) || 'Contract Resubmitted'.equalsIgnoreCase(deal.Status__c) ){
           SObject  obj = shareWithCreator(deal,'Read');
           if(obj!=null){
                shareOBjListToInsert.add(obj);
           }
       }
   }
    
   private static Offer_House_Demand__Share shareWithCreator(Offer_House_Demand__c oh,String accessLevel){
              Id recordId =oh.id;
              Offer_House_Demand__Share ohShare  = new Offer_House_Demand__Share();
              ohShare.ParentId = recordId;
              ohShare.UserOrGroupId = oh.CreatedById;
              ohShare.AccessLevel = accessLevel;
              ohShare.RowCause = Schema.Offer_House_Demand__Share.RowCause.Manual;
              return ohShare;
    }
    
   private static void initManagers(){
        //Load Deal Owner Managers and Directors
        if(Trigger.isBefore && Trigger.isInsert && ownerManagers.isEmpty()){
            Set<ID> owners = new Set<ID>();
            for (Offer_House_Demand__c deal : trigger.New){
              owners.add(deal.OwnerId);  
            } 
            List<User> theOwners = [SELECT Id, Sales_Manager__c FROM User WHERE Id IN : owners];
            owners = new Set<ID>();//Clear Owers and Add Managers in to 
            for (User u : theOwners) {
                ownerManagers.put(u.Id, u.Sales_Manager__c);
                owners.add(u.Sales_Manager__c);
            }
            List<User> theManagers = [SELECT Id, Sales_Manager__c FROM User WHERE Id IN : owners];
            for (User u : theManagers) {
                ownerDirectors.put(u.Id, u.Sales_Manager__c);
            }
        }
    }
    private static void updateOpportunityProductItems(){
        System.debug(LoggingLevel.ERROR, 'WLN Deals only ' + WLNDeals) ; 
        List<Opp_Product_Item__c> opiList = [select id , Opportunity__c from Opp_Product_Item__c where Opportunity__c in :WLNDeals.keySet()];
        System.debug(LoggingLevel.ERROR, 'WLN Deals only Before update ' + opiList) ; 
        for(Opp_Product_Item__c opi : opiList){
            opi.Deal_Support__c = WLNDeals.get(opi.Opportunity__c).id;
        }
        System.debug(LoggingLevel.ERROR, 'WLN Deals only after update ' + opiList) ;  
        update opiList;
    } 
}