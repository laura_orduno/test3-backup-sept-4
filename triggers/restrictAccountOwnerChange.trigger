/*****************************************************************/
/* Called before update of Account.                              */
/* Prevents non-Dataloader from changing ECH Account Owner       */
/* Author - Tom Muzyka                                           */
/* Updated - June 2010     v4.3                                  */
/*****************************************************************/
trigger restrictAccountOwnerChange on Account (before update) {

    /* Start Traction Modifications */
    Map<String, RestrictAccountOwnerExceptions__c > exceptions = RestrictAccountOwnerExceptions__c.getAll();
    Set<String> excludedRecordTypes = new Set<String>();
    Set<String> excludedProfileIds = new Set<String>();
    Set<String> excludedProfileNames = new Set<String>();
    
    if (exceptions != null) {
        for (RestrictAccountOwnerExceptions__c exclusion : exceptions.values()) {
            if (exclusion.Exclude_Record_Type_ID__c != null) {
                excludedRecordTypes.add((Id)exclusion.Exclude_Record_Type_Id__c);
            }
            if (exclusion.Exclude_Profile_ID__c != null) {
                excludedProfileIds.add((Id)exclusion.Exclude_Profile_Id__c);
            }
            if (exclusion.Exclude_Profile_Name__c != null) {
                excludedProfileNames.add(exclusion.Exclude_Profile_Name__c);
            }
        }
    }
    User u = [Select ProfileId, Profile.Name From User Where Id = :UserInfo.getUserId()];
    if (excludedProfileIds.contains(u.ProfileId) || excludedProfileNames.contains(u.Profile.Name)) {
        return;
    }
    /* End Traction Modifications */
    
    Set<String> fieldsTracked = new Set<String> {'OwnerId'};
    Set<Id> accIds = new Set<Id>();
        
    for (Account acc : trigger.new) {
        /* Traction Modifications: */
        if (excludedRecordTypes.contains(acc.RecordTypeId)) { continue; }
        /* End Traction Modifications */
        
        if (Util.hasChanges(fieldsTracked, acc, trigger.oldMap.get(acc.Id))) {
            accIds.add(acc.Id);
        }
    }
    
    if (accIds.isEmpty()) {
        return;
    }
    
    List<Account> accList = [Select Id, OwnerId, RCID__c, CBUCID_Parent_Id__c from Account where Id in :accIds];
    
    for (Integer i = 0; i < trigger.new.size(); i++) {
        Account acc = trigger.new[i];
        if (acc.RCID__c == null && acc.CBUCID_Parent_Id__c == null) {
            continue;
            // change from return - there may be errors in records other than the first.
        } else {
            Trigger.new[i].addError('You cannot change ECH Account Owners. Please follow the eCAT Process.');
        }
    }
}