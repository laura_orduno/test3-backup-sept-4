trigger activateAgreement on Apttus__APTS_Agreement__c (before update, after update) {
	//before update trigger
    if(Trigger.isUpdate && Trigger.isBefore && Trigger.size == 1) {
        if(Trigger.newMap.values().Apttus__Status__c != null && Trigger.newMap.values().Apttus__Status__c.equalsIgnoreCase('Fully Signed')) {
            //Update the Agreement Start Date
            Trigger.newMap.values().Apttus__Contract_Start_Date__c = Date.today();
            Trigger.newMap.values().Apttus__Contract_End_Date__c = Date.today().addMonths(Trigger.newMap.values().Apttus__Term_Months__c.intValue());
        }
    }
    //after update trigger
    //activate an agreement if status is fully signed
    //And this will not support bulk updates, so size should be 1
    if(Trigger.size == 1) {
        if(Trigger.isUpdate && Trigger.isAfter) {
            //prepare set of agreement ids
            set<Id> agreementIds = new set<Id>();
            for(Apttus__APTS_Agreement__c agreement : Trigger.newMap.values()) {
                agreementIds.add(agreement.Id);
            }
            
            //prepare docuSign Status by Agreement Id Map
            map<Id, dsfs__DocuSign_Status__c> docuSignStatusByAgreementIdMap = new map<Id, dsfs__DocuSign_Status__c>();
            //NOTE : in the case of multiple docusign status on one agreement, it will pick based on last modified date
            for(dsfs__DocuSign_Status__c docuSignStatus : [SELECT Id, Apttus_DocuSign__Apttus_Agreement_ID__c, Recipient_Signed_on_Paper__c FROM dsfs__DocuSign_Status__c WHERE Apttus_DocuSign__Apttus_Agreement_ID__c IN :agreementIds order by lastModifiedDate]) { 
                docuSignStatusByAgreementIdMap.put(docuSignStatus.Apttus_DocuSign__Apttus_Agreement_ID__c, docuSignStatus);
            }
            
            // prepare map to get attachments by agreement id
            //map<Id, Attachment> attachmentsMap = new map<Id, Attachment>([SELECT Id, ParentId FROM Attachment limit 999]);
            map<Id, Id> attachmentsMap = new map<Id, Id>();
            for(Attachment attachment : [SELECT Id, ParentId FROM Attachment WHERE ParentId IN :agreementIds order by createdDate desc]) {
                // use last attachment only
                if(!(attachmentsMap.get(attachment.ParentId) != null)) {
                    attachmentsMap.put(attachment.ParentId, attachment.Id);
                }
            }
            
            //loop over all agreement and call activate agreement api
            List<Apttus__APTS_Agreement__c> agmtsToBeUpdated = new List<Apttus__APTS_Agreement__c>();
            for(Apttus__APTS_Agreement__c agreement : Trigger.newMap.values()) {
                if(agreement.Apttus__Status__c != null && agreement.Apttus__Status__c.equalsIgnoreCase('Fully Signed')
                    && Trigger.oldMap.get(agreement.id).Apttus__Status__c != null && (!Trigger.oldMap.get(agreement.id).Apttus__Status__c.equalsIgnoreCase('Fully Signed'))) {
                    dsfs__DocuSign_Status__c docuSignStatus = docuSignStatusByAgreementIdMap.get(agreement.Id);
                    system.debug('*** docuSignStatus ***'+docuSignStatus);
                    //check if recipient signed on paper
                    if(docuSignStatus != null && docuSignStatus.Recipient_Signed_on_Paper__c != null && docuSignStatus.Recipient_Signed_on_Paper__c.equalsIgnoreCase('yes')) {
                        Apttus__APTS_Agreement__c agmt = new Apttus__APTS_Agreement__c(id = agreement.id);
                        agmt.Apttus__Status__c = 'Fax Contract Validation';
                        agmtsToBeUpdated.add(agmt);
                    } else {
                        Id agreementId = agreement.Id;
                        /*Id activateDocId = attachmentsMap.get(agreement.Id);
                        Boolean activateResponse;
                        system.debug('*** activateDocId ***'+activateDocId);
                        
                        //activate Agreement
                        if(activateDocId != null){
                            activateResponse = activateAgreement(agreementId, activateDocId);
                        } else {
                            activateResponse = false;
                        }*/
                        String activateResponse = AptsComplyCustomAPI.doActivate(agreementId);
                        
                        system.debug('*** activateResponse ***'+activateResponse);
                        system.debug('*** agreement status ***'+agreement.Apttus__Status__c);
                    }
                }           
            }
            if(!agmtsToBeUpdated.isEmpty()) {
                update agmtsToBeUpdated;
            }
        }
    }
    
    /***
        activate an agreement using api
        @agreementId = agreement id
        @activateDocId = attachment or doc id
    ***/
    private Boolean activateAgreement(Id agreementId, Id activateDocId) {
        list<String> activateDocIds = new list<String>();
        activateDocIds.add(activateDocId);
        String[] remDocIds = new String[]{};        
        Boolean response = Apttus.AgreementWebService.activateAgreement(agreementId, activateDocIds, remDocIds);
        
        if(response) {
            //publish document
            Boolean publishDocumentResponse = Apttus.AgreementWebService.publishToDocumentRepository(agreementId, activateDocId);
            return publishDocumentResponse;
        }
        return response;
    }
}