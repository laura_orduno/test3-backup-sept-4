trigger smb_EmailMessageAfter on EmailMessage (after insert) {

	Map<Id, List<EmailMessage>> incomingCaseEmailsMappedToCaseId = new Map<Id, List<EmailMessage>>();
	
	for (EmailMessage msg : trigger.new) {
		if (msg.Incoming && msg.ParentId != null) {
			if (!incomingCaseEmailsMappedToCaseId.containsKey(msg.ParentId)) {
				incomingCaseEmailsMappedToCaseId.put(msg.ParentId, new List<EmailMessage>());
			}
			incomingCaseEmailsMappedToCaseId.get(msg.ParentId).add(msg);
		}
	}
	
	if (incomingCaseEmailsMappedToCaseId.size() > 0) {
		smb_CaseEmailMessageUtility.updateCaseStatusOnIncomingEmails(incomingCaseEmailsMappedToCaseId);
	}

}