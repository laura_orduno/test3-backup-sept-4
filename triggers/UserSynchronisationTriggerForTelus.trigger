/**
* Trigger used by the SFDC to CPQ solution to synchronized users between SFDC and CPQ.
**/

trigger UserSynchronisationTriggerForTelus on User (before update) {
    User[] users = Trigger.new;
    UserSynchronisationUtilForTelus.resetUserSyncInCPQFlag(users);
}