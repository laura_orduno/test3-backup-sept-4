/*
*******************************************************************************************************************************
Class Name:     OCOM_OrderBeforeTrigger
Purpose:        OrderProduct Trigger
Created by:     Francisco Hong           08-Apr-2016    No previous version
Modified by:    Aditya Jamwal            17-Jan-2017    QC-6481
Modified by:    Santosh Rath             25-Mar-2017    QC-10596
*******************************************************************************************************************************
*/
trigger OCOM_OrderBeforeTrigger on Order (before insert,before update) {
    system.debug('@@@@Inside Order Before Trigger');
	
    //Aditya Jamwal QC-6481
    if(!OCOM_OrderBeforeTriggerHelper.OrderBeforeTrigger_Fired || Test.isRunningTest())
    {
        OCOM_OrderBeforeTriggerHelper.linkingOrderAndCARProductInfo(Trigger.isBefore,Trigger.isInsert,Trigger.isUpdate,Trigger.oldMap,Trigger.new);
      //start BMPF-3
      OCOM_OrderBeforeTriggerHelper.generateSubOrderNumber(Trigger.isInsert, Trigger.isUpdate, Trigger.new, Trigger.oldMap);
      //end BMPF-3
      //start BMPF-40
      OCOM_OrderBeforeTriggerHelper.updateTotalMRCAndNRCOnParentOrder(Trigger.isBefore,Trigger.isInsert,Trigger.isUpdate,Trigger.oldMap,Trigger.new,Trigger.oldMap);
      //end BMPF-40  
	   OCOM_OrderBeforeTriggerHelper.OrderBeforeTrigger_Fired = True;
	}
	
	for(Order orderObj:trigger.new) {             
        String currentUserName = UserInfo.getName();
        // Thomas QC-6738 End
          //added by danish on 5/8/2017 
     if (Trigger.isUpdate && Trigger.isAfter) {
         if(String.isNotBlank(currentUserName)&& currentUserName.equalsIgnoreCase('Application TOCP')){
             for(Order orderObjold : trigger.new ) {
                If(orderObjold.status == OrdrConstants.ORDR_ERROR_STATUS){
                    OCOM_OrderErrorStatusHelper.UpdateErrorStatusOnOli(OrderObj);
                }
           }
         }
      }
    }
    smb_Order_helper objHelper = new smb_Order_helper();
    objHelper.statusChange(trigger.new);
    //added by danish 5/3/2017
	
	OC_OrderStatusHelper.updateOrderStatus(Trigger.isBefore,Trigger.isInsert,Trigger.isUpdate,Trigger.isAfter, Trigger.new,Trigger.oldMap);

    
    //Arpit : 8-June-2017 : Order 2.0 : Sprint 4
    //Adding fix for default language on order : Setting its value from contact's preferred language
    if(Trigger.isUpdate && Trigger.isBefore && checkRecursiveOrderBeforeUpdateTrigger.isFirstTime){
        //Map<Id,Order> currentIdOldOrderData = Trigger.oldMap;
        for(Order orderObj : trigger.new) {           
            //if(orderObj.Type.equalsIgnoreCase('Change')){
            String contactPreferredLanguage = '';
            if(orderObj.CustomerAuthorizedById != null){
                List<Contact> contactOnOrder = [Select Language_Preference__c  from Contact where Id =: orderObj.CustomerAuthorizedById];
                contactPreferredLanguage  = contactOnOrder[0].Language_Preference__c ;
                system.debug('contactOnOrder ---->' + contactOnOrder[0] ) ;
            }
            system.debug('orderObj---->' + orderObj) ;
            //system.debug('contactPreferredLanguage  ---->' + contactPreferredLanguage ) ;
            if((contactPreferredLanguage  != null) &&  (orderObj.Chosen_Language__c == null) ){
                if(contactPreferredLanguage.equalsIgnoreCase('English')){
                    orderObj.Chosen_Language__c = 'en_US' ;
                }else if(contactPreferredLanguage.equalsIgnoreCase('French')){
                    orderObj.Chosen_Language__c = 'fr' ;
                }
            system.debug('orderObj.Chosen_Language__c changed ---->' + orderObj.Chosen_Language__c) ;
            }
          //}
            
        }
        checkRecursiveOrderBeforeUpdateTrigger.isFirstTime = false;
        
    }
}