trigger smb_ServiceRequestContactTrigger on Service_Request_Contact__c (after update, before update) {
	
	map<ID,Service_Request_Contact__c> mapSRSRC = new map<ID,Service_Request_Contact__c>();
	list<Service_Request__c> lstServiceRequest = new list<Service_Request__c>();
	list<Service_Request_Contact__c> lstServiceRequestContact = new list<Service_Request_Contact__c>();
		if(Trigger.isafter)
		{		
			for (Service_Request_Contact__c serReqContact : trigger.new)
			{
				if(serReqContact.PrimaryContact__c )
				{
					system.debug('=============serReqContact.PrimaryContact__c======'+serReqContact.PrimaryContact__c);
					mapSRSRC.put(serReqContact.Service_Request__c,serReqContact);
				}
			
			}
			system.debug('=============mapSRSRC======'+mapSRSRC);
			//update primary contact on service request
			lstServiceRequest = [select Id, name,PrimaryContact__c from Service_Request__c where ID in :mapSRSRC.keyset()];
			system.debug('=============lstServiceRequest======'+lstServiceRequest);
			for (Service_Request__c sr : lstServiceRequest)
			{
				system.debug('=============mapSRSRC.get(sr.id).Contact__c======'+mapSRSRC.get(sr.id).Contact__c);
				sr.PrimaryContact__c = mapSRSRC.get(sr.id).Contact__c;			
			}
			
			update lstServiceRequest;
			
			//update existing primary contact checkbox to false
			
			lstServiceRequestContact =[select ID,PrimaryContact__c,Service_Request__c from Service_Request_Contact__c where Service_Request__c in : mapSRSRC.keyset()];
			list <Service_Request_Contact__c> lstToUpdate = new list <Service_Request_Contact__c> ();
			for (Service_Request_Contact__c serReqContact : lstServiceRequestContact)
			{
				if (serReqContact.id != mapSRSRC.get(serReqContact.Service_Request__c).id)
				{
					serReqContact.PrimaryContact__c = false;
					lstToUpdate.add(serReqContact);
				}		
			}
			update lstToUpdate;		
		} 
	
}