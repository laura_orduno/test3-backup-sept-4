trigger telus_Cloud_Project_Note_Notifier on Note (after insert) {
    
    
    // First, let's get the list of notes.
    Savepoint sp = Database.setSavepoint();
    try {
        CloudProjectPortal__c eups = CloudProjectPortal__c.getInstance();
        if(!Test.isRunningTest()) {
            System.assert(eups != null, 'Failed to load critical portal settings');
            System.assert(eups.HMAC_Digest_Key__c != null, 'Failed to load critical digest settings');
        }

		Set<Id> userIds = new Set<Id>();
    	
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage>();
            
        List<Id> SignOffIds = new List<Id>();
        List<Id> KeyDeliverableIds = new List<Id>();
        List<Id> CloudProjectIds = new List<Id>();
        Map<Id,String> CloudProjectOwnerEmail = new Map<Id,String>();
        Map<Id,String> KDOwnerEmail = new Map<Id,String>();
            
        Integer ix = 0;
        while (ix < trigger.new.size()) {
            if (String.valueOf(trigger.new[ix].parentId).substring(0, 3) == 'a2x') {

                Note task = trigger.new[ix];
                if (task.parentId != null) {
                    SignOffIds.add(task.parentId);
                }
            }
            ix++;
        }

        Map<Id, Cloud_Project_Sign_Off__c> SignOffMap = new Map<Id, Cloud_Project_Sign_Off__c>();
        if (SignOffIds != null && SignOffIds.size() > 0) {
            SignOffMap = new Map<Id, Cloud_Project_Sign_Off__c>([Select Name, Assignee_Email__c, Cloud_Project_Key_Deliverable__c From Cloud_Project_Sign_Off__c Where Id In :SignOffIds]);
        }
        
        for(Id tempSO : SignOffMap.keySet()){
        	if(SignOffMap.get(tempSO) != null){
            	if(SignOffMap.get(tempSO).Cloud_Project_Key_Deliverable__c != null) KeyDeliverableIds.add(SignOffMap.get(tempSO).Cloud_Project_Key_Deliverable__c);
            }
        }
            
        Map<Id, SMET_Requirement__c> KDMap = new Map<Id, SMET_Requirement__c>();
        if (KeyDeliverableIds != null && KeyDeliverableIds.size() > 0) {
            KDMap = new Map<Id, SMET_Requirement__c>([Select Name, Ownerid, Related_SMET_Project__c From SMET_Requirement__c Where Id In :KeyDeliverableIds]);
        }
            
        for(Id tempKD : KDMap.keySet()){
        	if(KDMap.get(tempKD) != null){
            	if(KDMap.get(tempKD).Related_SMET_Project__c != null) CloudProjectIds.add(KDMap.get(tempKD).Related_SMET_Project__c);
            	if(KDMap.get(tempKD).Ownerid != null) userIds.add(KDMap.get(tempKD).Ownerid);
            }
        }
            
        Map<Id, SMET_Project__c> CPMap = new Map<Id, SMET_Project__c>();
        if (CloudProjectIds != null && CloudProjectIds.size() > 0) {
            CPMap = new Map<Id, SMET_Project__c>([Select Name, Ownerid From SMET_Project__c Where Id In :CloudProjectIds]);
        }
            
        for(SMET_Project__c temp:CPMap.values()){
        	userIds.add(temp.Ownerid);
        }
            
        Map<id,User> userEmails = new Map<id,User>([Select id, Email From User where Id IN :userIds]);
                        
        for(SMET_Requirement__c temp:KDMap.values()){
          	KDOwnerEmail.put(temp.Ownerid,(userEmails.get(temp.Ownerid)).Email);
        }
            
        for(SMET_Project__c temp:CPMap.values()){
           	CloudProjectOwnerEmail.put(temp.Ownerid,(userEmails.get(temp.Ownerid)).Email);
        }

        Note[] atasks = trigger.new;
        Note[] tasks = new List<Note>();
        for (Note t : atasks) {
            if (String.valueOf(t.parentId).substring(0, 3) != 'a2x') { continue; }
            tasks.add(t);
        }
        if (tasks != null && tasks.size() > 0) {
        
            Messaging.SingleEmailMessage[] infomessages = new List<Messaging.SingleEmailMessage>();
            
            SignOffIds = new List<Id>();            
            KeyDeliverableIds = new List<Id>();
            userIds = new Set<Id>();
            
            for (Note task : tasks) {
                userIds.add(task.CreatedById);
                if (task.parentid != null) {
                    SignOffIds.add(task.parentid);
                }
            }
            Map<Id, User> createdUsers = new Map<Id, User>([Select Name, Email From User Where Id in :userIds]);

            Map<Id, Cloud_Project_Sign_Off__c> SOMap = null;
            if (SignOffIds != null && SignOffIds.size() > 0) {
                SOMap = new Map<Id, Cloud_Project_Sign_Off__c>([Select Id, Project_Name__c, KD_Name__c, Asignee_Name__c, Token__c, Assignee_Email__c From Cloud_Project_Sign_Off__c Where Id In :SignOffIds]);
            }
            
            for (Note t : tasks) {
                
                String relatedTo = '';
                if (t.parentid != null) {
                    if (SOMap.get(t.parentid) != null) {
                        relatedTo += '<br/><br/>Cloud Project: ' + SOMap.get(t.parentid).Project_Name__c + '<br/>Key Deliverable: ' + SOMap.get(t.parentid).KD_Name__c;
                    }
                }
                
                User u;
                if(createdUsers.get(t.CreatedById) != null) u = createdUsers.get(t.CreatedById);
                String creatorName = 'No Name Assignee';

                if (u != null && u.name != null && !u.name.contains('Guest User')) {
                    creatorName = u.name;
                }
                else {
   	                if(SOMap.get(t.parentid).Asignee_Name__c != null) creatorName = SOMap.get(t.parentid).Asignee_Name__c;
                }
                
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setToAddresses(new String[] {KDOwnerEmail.get((KDMap.get(SignOffMap.get(t.parentid).Cloud_Project_Key_Deliverable__c)).Ownerid), CloudProjectOwnerEmail.get((CPMap.get((KDMap.get(SignOffMap.get(t.parentid).Cloud_Project_Key_Deliverable__c)).Related_SMET_Project__c)).Ownerid)});
                mail.setReplyTo('sfdcsupport@telus.com');
                mail.setSenderDisplayName('New Comment Added for Telus Cloud Project Sign Off Task');
                mail.setSubject('A New Comment for Cloud Project Sign Off Task Has Been Added.  Project: ' + SOMap.get(t.parentid).Project_Name__c + ' / Key Deliverable: ' + SOMap.get(t.parentid).KD_Name__c);
                mail.setBccSender(false);
                mail.setPlainTextBody(creatorName + ' has added a new comment following Cloud Project Sign Off task related to ' + relatedTo + ' \n\n' +
                    'Comment: ' + t.title + '\n\n' +
                    'Please go to ' + eups.Base_URL__c + '/' + SOMap.get(t.parentid).Id + ' to view details of this sign off task.');
                
                mail.setHtmlBody(creatorName + ' has added a new comment following Cloud Project Sign Off task related to ' + relatedTo + '<br/>\n<br/>\n' +
                	'Comment: ' + t.title + '<br/>\n<br/>\n' +
                    'Please <a href="' + eups.Base_URL__c + '/' + SOMap.get(t.parentid).Id + '" style="font-weight: bold; font-size: 1.1em;">click here</a> to view details of this sign off task.');
                infomessages.add(mail);

                mail = new Messaging.SingleEmailMessage();
                mail.setToAddresses(new String[] {SOMap.get(t.parentid).Assignee_Email__c});
                mail.setReplyTo('sfdcsupport@telus.com');
                mail.setSenderDisplayName('New Comment Added for Telus Cloud Project Sign Off Task');
                mail.setSubject('A New Comment for Cloud Project Sign Off Task Has Been Added.  Project: ' + SOMap.get(t.parentid).Project_Name__c + ' / Key Deliverable: ' + SOMap.get(t.parentid).KD_Name__c);
                mail.setBccSender(false);
                mail.setPlainTextBody(creatorName + ' has added a new comment following Cloud Project Sign Off task related to ' + relatedTo + ' \n\n' +
                    'Comment: ' + t.title + '\n\n' +
                    'Please go to ' + eups.Portal_URL__c + '/CloudProjectPortalSignOffInformation?oid=' + SOMap.get(t.parentid).Id + '&tok=' + SOMap.get(t.parentid).Token__c + ' to view details of your sign off task.');
                
                mail.setHtmlBody(creatorName + ' has added a new comment following Cloud Project Sign Off task related to ' + relatedTo + '<br/>\n<br/>\n' +
                	'Comment: ' + t.title + '<br/>\n<br/>\n' +
                    'Please <a href="' + eups.Portal_URL__c + '/CloudProjectPortalSignOffInformation?oid=' + SOMap.get(t.parentid).Id + '&tok=' + EncodingUtil.urlEncode(SOMap.get(t.parentid).Token__c, 'UTF-8') + '" style="font-weight: bold; font-size: 1.1em;">click here</a> to view details of your sign off task.');
                infomessages.add(mail);

            }
            if (infomessages != null && infomessages.size() > 0) { Messaging.sendEmail(infomessages); }
        }
        
    } catch (Exception e) { Database.rollback(sp); QuoteUtilities.log(e, false); trigger.new[0].addError(e.getMessage()); }
}