/*
###########################################################################
# File..................: SMB_SyncBANBetweenAccountAndOpportunties 
# Version...............: 29
# Created by............: Kanishk Prasad
# Created Date..........: 27/04/2014
# Last Modified by......: 
# Last Modified Date....: 
# Description...........: This Trigger will run on before Insert ,After Insert and After Update events of Opportunity and 
#                         update the BAN value of Opportunity, if it is present in its related Account.
#                         It will also update the Account with the BAN value if it was supplied in the Opportunity record.
#                         If however BAN is already present in Account, then a validation error would be thrown   
#
# Copyright (c) 2000-2012. Astadia, Inc. All Rights Reserved.
#
# Created by the Astadia, Inc. Modification must retain the above copyright notice. 
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any commercial purpose, without fee, and without a written
# agreement from Astadia, Inc., is hereby forbidden. Any modification to source
# code, must include this paragraph and copyright.
#
# Permission is not granted to anyone to use this software for commercial uses.
#
# Contact address: 2839 Paces Ferry Road, Suite 350, Atlanta, GA 30339
# Company URL : http://www.astadia.com
###########################################################################
*/


trigger SMB_SyncBANBetweenAccountAndOpportunties on Opportunity (before insert,after Insert,after update) {

//Map<Id,RecordType> mapRecordTypes=new Map<Id,RecordType>([select id from recordtype where sobjectType ='Opportunity' and developername like 'SMB_Care%']);

set<Id> setSMBRecordTypes=SMB_Helper.fetchOpportunitySMBCareRecordTypes(null);

if(Trigger.isBefore)
{
    Set<Id> setAccountIds=new Set<Id>();
    for(Opportunity opp:Trigger.New)
    {
            //if(mapRecordTypes.containsKey(opp.RecordTypeId))
            if(setSMBRecordTypes.contains(opp.RecordTypeId))
                setAccountIds.add(opp.Accountid);
    }
    Map<Id,Account> mapAccount=new Map<Id,Account>([Select id,BAN__c from Account where BAN__c <>'' and id in:setAccountIds]);
    for(Opportunity opp:Trigger.New)
    {
        if(mapAccount.containsKey(opp.Accountid))
        {
            opp.BAN__c=mapAccount.get(opp.Accountid).BAN__c;
        }
    }
}

if(Trigger.isAfter){
    Map<Id,Account> mapAccount=new Map<Id,Account>();
    Map<Id,Opportunity> mapOppAccount=new Map<Id,Opportunity>();
    for(Opportunity opp:Trigger.New)
    {
  //      if(mapRecordTypes.containsKey(opp.RecordTypeId))
            if(setSMBRecordTypes.contains(opp.RecordTypeId))
        {   
            if(String.isNotBlank(opp.BAN__c))
            {
              if((Trigger.isUpdate && Trigger.NewMap.get(opp.id).BAN__c<>Trigger.oldMap.get(opp.id).BAN__c) || Trigger.isInsert)
              {              
                  mapAccount.put(opp.Accountid,new Account(id=opp.Accountid,BAN__c=opp.BAN__c));          
                  mapOppAccount.put(opp.id,opp);
               }
            }
            else if(Trigger.isUpdate && String.isNotBlank(Trigger.oldMap.get(opp.id).BAN__c))
            {
                Trigger.NewMap.get(opp.id).BAN__c.addError('BAN already exists on  Account');
            }
        }
    }
  Map<Id,Account> mapAccountWithExistingBANs=new Map<Id,Account>([Select id,BAN__c from Account where BAN__c<>'' and id in : mapAccount.keySet()] );
//    System.debug('2222222222222222->'+mapAccountWithExistingBANs);
  for(String strOppId:mapOppAccount.keySet())
  {
      String strAccountId=mapOppAccount.get(strOppId).Accountid;
      if(mapAccountWithExistingBANs.containsKey(strAccountId))
      {
         if(mapAccountWithExistingBANs.get(strAccountId).BAN__c<>mapOppAccount.get(strOppId).BAN__c)
              Trigger.NewMap.get(strOppId).BAN__c.addError('BAN already exists on  Account');
          mapAccount.remove(strAccountId);
      }
  }  
  if(mapAccount.size()>0)
      update mapAccount.values();
 }     
}