/**
 * Copies Tier and Vertical values from User record to any Assignee records that reference the
 * User records being processed by this trigger.
 * 
 * @author Max Rudman
 * @since 4/2/2009
 */
trigger SetAssigneeFieldsByUser on User (after update) {
	// Build the map of changed users
	Map<Id,User> changedUsersById = new Map<Id,User>();
	for (User user : Trigger.new) {
		User old = Trigger.oldMap.get(user.Id);
		if ((old.Tier__c != user.Tier__c) || (old.User_Vertical__c != user.User_Vertical__c)) {
			changedUsersById.put(user.Id,user);
		}
	}
	
	if (changedUsersById.size() > 0) {
		// Load assignees referenced by this user
		List<ADRA__Assignee__c> assignees = [SELECT Assignee_Tier__c, Assignee_Vertical__c, ADRA__User__c FROM ADRA__Assignee__c WHERE ADRA__User__c IN :changedUsersById.keySet()];
		for (ADRA__Assignee__c assignee : assignees) {
			User user = changedUsersById.get(assignee.ADRA__User__c);
			if (user != null) {
				assignee.Assignee_Tier__c = user.Tier__c;
				if (user.User_Vertical__c == null) {
					assignee.Assignee_Vertical__c = null;
				} else {
					assignee.Assignee_Vertical__c = user.User_Vertical__c.replaceAll(';',',');
				}
			}
		}
		update assignees;
	}
}