/*****************************************************
    Trigger Name: Lead_SetOwnerName 
    Usage: This TRigger assign the Owner Name from owner of Lead 
    Author – Clear Task
    Date – 02/05/2011    
    Revision History
******************************************************/
trigger Opportunity_SetOwnerName on Opportunity (before insert, before update) {
    SetOwnerName.populateFieldOnOwnerName(trigger.new);    
}