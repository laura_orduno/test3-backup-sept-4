/*****************************************************
    Trigger Name: UpdateOpportunityByOpi 
    Usage: Update the Opportunity when following field is update in Opportunity Product Item
            Existing Monthly Recurring Revenue 
            Quantity 
            All Non Recurring Revenue 
            Monthly Recurring Revenue (Product) 
            Contract Length (Months) 
            Billing Frequency (Months)
    Author – Clear Task    
    Date – 4/7/2011        
    Revision History
******************************************************/
trigger UpdateOpportunityByOpi on Opp_Product_Item__c (after update) {
    //Opportunity Id which need to be update
    Set<Id> opportunitySet = new Set<Id>();    
    for(Opp_Product_Item__c newOpi :trigger.new){
        Opp_Product_Item__c oldOpi = System.Trigger.oldMap.get(newOpi.Id);
        if((newOpi.Existing_Monthly_Recurring_Revenue__c != null && oldOpi.Existing_Monthly_Recurring_Revenue__c != null && newOpi.Existing_Monthly_Recurring_Revenue__c != oldOpi.Existing_Monthly_Recurring_Revenue__c)){
            opportunitySet.add(newOpi.Opportunity__c);
        }else if((newOpi.Quantity__c != null && oldOpi.Quantity__c != null && newOpi.Quantity__c != oldOpi.Quantity__c)){
            opportunitySet.add(newOpi.Opportunity__c);
        }else if((newOpi.All_Non_Recurring_Revenue__c != null && oldOpi.All_Non_Recurring_Revenue__c != null && newOpi.All_Non_Recurring_Revenue__c != oldOpi.All_Non_Recurring_Revenue__c)){
            opportunitySet.add(newOpi.Opportunity__c);
        }else if((newOpi.Monthly_Recurring_Revenue__c != null && oldOpi.Monthly_Recurring_Revenue__c != null && newOpi.Monthly_Recurring_Revenue__c != oldOpi.Monthly_Recurring_Revenue__c)){
            opportunitySet.add(newOpi.Opportunity__c);
        }else if((newOpi.Contract_Length_Month__c != null && oldOpi.Contract_Length_Month__c != null && newOpi.Contract_Length_Month__c != oldOpi.Contract_Length_Month__c)){
            opportunitySet.add(newOpi.Opportunity__c);
        }else if((newOpi.Billing_Frequency_Month__c != null && oldOpi.Billing_Frequency_Month__c != null && newOpi.Billing_Frequency_Month__c != oldOpi.Billing_Frequency_Month__c)){
            opportunitySet.add(newOpi.Opportunity__c);
        }
    }
    ////Opportunity list for update 
    if(opportunitySet != null && opportunitySet.size()>0){
        List<Opportunity> oppList = [select o.Id, o.Name from Opportunity o where o.Id in :opportunitySet];
        if(oppList != null && oppList.size()>0){
            try{
                update oppList ;
            }catch(DMLException e){}
        }
    }
}