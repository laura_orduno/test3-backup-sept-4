trigger populateProductHistory on Product2 (after insert, after update) {

	List<Product_History__c> productHistoryList = new List<Product_History__c>();
	SObjectHistoryUtil historyUtil = new SObjectHistoryUtil();
	
	if (trigger.isInsert) {
		historyUtil.createHistoricalObject(trigger.new, productHistoryList, 'Product_History__c', ProductHistoryUtil.PROD_HISTORY_FIELD_MAP);
		System.debug('productHistoryList Size = ' + productHistoryList.size());
		
	} else if (trigger.isUpdate) {
		Map<Id, Product2> changedProducts = new Map<Id, Product2>();
		for (Product2 p : trigger.new) {
			if (historyUtil.isObjectChanged(p, trigger.oldMap.get(p.Id), ProductHistoryUtil.PROD_HISTORY_TRACKING_FIELDS)) {
				changedProducts.put(p.Id, p);
			}
		}

		if (! changedProducts.isEmpty()) {
			historyUtil.createHistoricalObject(changedProducts.values(), productHistoryList, 'Product_History__c', ProductHistoryUtil.PROD_HISTORY_FIELD_MAP);

			List<Product_History__c> updateProdHistory = [Select Id from Product_History__c where Product__c In :changedProducts.keySet() And End_Date__c = null];
			for (Product_History__c p2History : updateProdHistory) {
				p2History.End_Date__c = Date.today();
				productHistoryList.add(p2History);
			}
		}
	}

	if (! productHistoryList.isEmpty()) {
		upsert productHistoryList;
	}

}
		/*for (Product2 p : trigger.new) {
			Product_History__c prodHistory = new Product_History__c();
			for (String key : ProductHistoryUtil.PROD_HISTORY_FIELD_MAP.keySet()) {
				System.debug('key = ' + key);
				String productField = ProductHistoryUtil.PROD_HISTORY_FIELD_MAP.get(key);
				System.debug('productField = ' + productField);
				Object val = p.get(productField);
				prodHistory.put(key, val);
				prodHistory.Start_Date__c = Date.today();
			}
			productHistoryList.add(prodHistory);
		}*/