/*
###########################################################################
# File..................: SMB_restrictDummyCPQOpportunityUpdateAndDeletion 
# Version...............: 29
# Created by............: Kanishk Prasad
# Created Date..........: 15/06/2014
# Last Modified by......: 
# Last Modified Date....: 
# Description...........: This Trigger will restrict Dummy CPQ Opportunity update and Deletion unless Opportunity 
#                         in "SMB CPQ Dummy Opportunity Detail" is Blank
#
# Copyright (c) 2000-2012. Astadia, Inc. All Rights Reserved.
#
# Created by the Astadia, Inc. Modification must retain the above copyright notice. 
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any commercial purpose, without fee, and without a written
# agreement from Astadia, Inc., is hereby forbidden. Any modification to source
# code, must include this paragraph and copyright.
#
# Permission is not granted to anyone to use this software for commercial uses.
#
# Contact address: 2839 Paces Ferry Road, Suite 350, Atlanta, GA 30339
# Company URL : http://www.astadia.com
###########################################################################
*/



trigger SMB_restrictDummyCPQOpportunityUpdateAndDeletion on Opportunity (before update,before delete) 
{
     String order=null;
     if(Test.isRunningTest()==false)
     	order=SMB_CPQ_Dummy_Opportunity__c.getValues('CPQ').Opportunity__c;
          	
     if(String.isNotBlank(order))
     {
         for(Opportunity opp:Trigger.old)
         {
             if(opp.order_id__c==order)
             {
                 Opportunity opportunityRec=Trigger.oldMap.get(opp.id);
                 opportunityRec.addError('This Order Cannot be Modified or Deleted');
                 break;
             }
         }
     }

}