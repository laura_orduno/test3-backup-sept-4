trigger AptsAgreementBeforeUpdateTrigger on Apttus__APTS_Agreement__c (before update) {
    for(integer i=0;i<Trigger.new.size();i++) {
        if(Trigger.old[i].Docusign_Request_Expired1__c == false && Trigger.new[i].Docusign_Request_Expired1__c == true && Trigger.new[i].Apttus__Status__c.equalsIgnoreCase('Other Party Signatures')) {
                Trigger.new[i].Apttus__Status__c = 'Acceptance Expired';
        } else if (Trigger.old[i].Apttus__Status__c != Trigger.new[i].Apttus__Status__c && Trigger.new[i].Apttus__Status__c != null && Trigger.new[i].Apttus__Status__c.equalsIgnoreCase('Request')) {
            Trigger.new[i].Docusign_Request_Expired1__c = false;
        }
    }
}