trigger trac_QuoteBSOHelper on Quote_BSO__c (before insert) {
    for (Quote_BSO__c qbso : trigger.new) {
        qbso.Quote__c = (Id)qbso.Name;
    }
}