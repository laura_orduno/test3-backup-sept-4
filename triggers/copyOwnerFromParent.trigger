trigger copyOwnerFromParent on Account (before insert, after update) {
    if( Trigger.isUpdate && Trigger.isAfter )
    {
        // perform after update
        copyOwnerFromParent.afterUpdate(Trigger.old, Trigger.new);                
    }   
    else if(Trigger.isInsert && Trigger.isBefore)
    {
        // perform before insert
        copyOwnerFromParent.beforeInsert(Trigger.new);                
    }   
}