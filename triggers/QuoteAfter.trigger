trigger QuoteAfter on SBQQ__Quote__c (after insert, after update) {
    // If the agreement has been accepted, send a message to RWMS. Note - this can only be done on 10 records at a time.
    if (trigger.new.size() <= 10 && trigger.isUpdate) {
        for (SBQQ__Quote__c q : trigger.new) {
            if (q.Agreement_Accepted__c && !trigger.oldMap.get(q.Id).Agreement_Accepted__c) {
                QuoteBundleCommitmentClient.sendCommitmentMessage(q.Id);
            }
        }
    }
    
    // Purging Account Data
    Set<String> purgeStatuses = new Set<String>{
        'Client Declined',
        'Quote Expired',
        'Agreement Expired'
    };
    Set<Id> quoteIdsToPurge = new Set<Id>();
    for (SBQQ__Quote__c q : trigger.new) {
        if (purgeStatuses.contains(q.SBQQ__Status__c)) {
            quoteIdsToPurge.add(q.Id);
        }
    }
    if (quoteIdsToPurge.size() > 0) {
        SBQQ__Quote__c[] quotes = [Select SBQQ__Opportunity__r.Web_Account__c From SBQQ__Quote__c Where Id in :quoteIdsToPurge];
        Set<Id> webAccountIds = new Set<Id>();
        for (SBQQ__Quote__c quote : quotes) {
            webAccountIds.add(quote.SBQQ__Opportunity__r.Web_Account__c);
        }
        Web_Account__c[] accounts = new List<Web_Account__c>();
        for (Id i : webAccountIds) {
            if (i == null) { continue; }
            Web_Account__c a = new Web_Account__c(Id = i);
            for (String f : QuoteAccountController.protectedFields) {
                a.put(f, null);
            }
            for (String f : QuoteAccountController.protectedDateFields) {
                a.put(f, null);
            }
            accounts.add(a);
        }
        update accounts;
    }
}