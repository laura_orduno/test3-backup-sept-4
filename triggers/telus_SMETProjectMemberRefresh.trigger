//Update cloud project Project_Member_IDs__c field
trigger telus_SMETProjectMemberRefresh on Cloud_Enablement_Project_Team__c (after delete, after insert, after update) {

	Set<Id> projectIds = new Set<Id>();
    
    for (Cloud_Enablement_Project_Team__c t : (Trigger.isDelete ? trigger.old : trigger.new)) {
        projectIds.add(t.Cloud_Enablement_Project__c);
    }
    
    List<SMET_Project__c> projects = [SELECT Id, Project_Member_IDs__c FROM SMET_Project__c WHERE Id IN :projectIds];
    List<Cloud_Enablement_Project_Team__c> projectMembers = [SELECT Id, Cloud_Enablement_Project__c, Team_Member__c FROM Cloud_Enablement_Project_Team__c WHERE Cloud_Enablement_Project__c IN :projectIds];
    
	for (SMET_Project__c p : projects) {
		p.Project_Member_IDs__c = '';
		for(Cloud_Enablement_Project_Team__c m : projectMembers){
			if (m.Cloud_Enablement_Project__c == p.id) {
				p.Project_Member_IDs__c += m.Team_Member__c;
			}
		}
	}
	
	if (projects.size() > 0) {
		update projects;
	}
	
}