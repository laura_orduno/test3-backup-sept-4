// Author - Christian Wico - cwico@tractionondemand.com
// Usage: 
//	 - When a Lead is Converted update the Account's Team_TELUS_ID__c with the owner's (User) Team_TELUS_ID__c
// 	 - If, Opportunity is created dealer marker b will be marked as dealer if account is from dealer 
trigger trac_TeamTelusIdFromAccountOwner on Lead (after update) {
    //System.debug('trac_TeamTelusIdFromAccountOwner ');
    Set<Id> idSet = new Set<Id>();
    Set<Id> accountIdSet = new Set<Id>();
    Set<Id> ownerIdSet = new Set<Id>();
    Set<Id> oppIdSet = new Set<Id>();
    
    for(Lead l: trigger.new){
        // get only converted leads
        if (l.IsConverted) {
            idSet.add(l.Id);
            accountIdSet.add(l.ConvertedAccountId);
            ownerIdSet.add(l.OwnerId);
            if (l.ConvertedOpportunityId != null) oppIdSet.add(l.ConvertedOpportunityId);
        }
    }
    
    Map<Id, Account> accountMap = new Map<Id, Account>([select Id, Name, Dealer_Account__c, Team_TELUS_ID__c FROM Account WHERE Id IN :accountIdSet]);
    Map<Id, User> ownerMap = new Map<Id, User>([SELECT Id, Name, Team_TELUS_ID__c FROM User WHERE Id IN :ownerIdSet]);
    Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>([SELECT Id, Name, AccountId FROM Opportunity WHERE Id IN :oppIdSet]);
    
    //System.debug('Account Map: ' + accountMap);
    //System.debug('Owner Map: ' + ownerMap);
    
    List<Account> accounts = new List<Account>();
    List<Opportunity> opps = new List<Opportunity>();
    
    // update lead's account with Team_TELUS_ID  
    for(Lead l : trigger.new){
        if (l.IsConverted && accountMap.containsKey(l.ConvertedAccountId) && ownerMap.containsKey(l.OwnerId)) {
            Account a = accountMap.get(l.ConvertedAccountId);
            a.Team_TELUS_ID__c = ownerMap.get(l.OwnerId).Team_TELUS_ID__c;
            accounts.add(a);
            //System.debug('Account: ' + a.Name +  ' > Team_TELUS_ID__c ' + a.Team_TELUS_ID__c);
        }
    }
    
    if (accounts.size() > 0) update accounts;
    
    // System.debug('CW Account: ' + accounts);
    
    // update dealer marker b
    for (Opportunity opp: oppMap.values()) {
    	
    	if (accountMap.get(opp.AccountId).Team_TELUS_ID__c != null && accountMap.get(opp.AccountId).Team_TELUS_ID__c.toLowerCase().contains('dealer')) {
    		opp.xx_dealer_marker_b__c = 'dealer';
    	}
    	else {
    		opp.xx_dealer_marker_b__c = 'nondealer';
    	}
    	
    	opps.add(opp);
    }
    
    if (opps.size() > 0) update opps;
    
    //System.debug('CW Opps: ' + opps);
    
    //System.debug('Accounts Updated');
}