trigger trac_Quote_Channel on SBQQ__Quote__c (after insert) {
	List<Id> quoteIds = new List<Id>();
    List<Id> userids = new List<Id>();
    List<SBQQ__Quote__c> quotes = new List<SBQQ__Quote__c>();
    Map<Id, User> idusers = new Map<Id, User>();
    for (SBQQ__Quote__c q : trigger.new) {
    	quoteids.add(q.Id);
        userids.add(q.CreatedById);
    }
    if (quoteids.size() != 0 && userids.size() != 0)
    quotes = [Select Id, Channel__c, CreatedById From SBQQ__Quote__c Where Id in :quoteids];
    idusers = new Map<id,user>([Select Id, Channel__c from User where Id in :userids limit 50000]);
    if (quotes.size() != 0) {
	    for (SBQQ__Quote__c q : quotes) {
	        User u = idusers.get(q.CreatedById);
	        q.Channel__c = u.Channel__c;
	    }
	    update quotes;
    }
}