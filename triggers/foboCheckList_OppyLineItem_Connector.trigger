trigger foboCheckList_OppyLineItem_Connector on FOBOChecklist__c (after insert, after update) {
        
        foboCheckList_OppyLineItem_Helper.delete_current_lineItems(trigger.New);
        foboCheckList_OppyLineItem_Helper.convert_to_oppy_items(trigger.New);   
         
}