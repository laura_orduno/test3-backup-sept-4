trigger NotifyLeadConvertedToOpportunity on Lead (after update) 
{
    Profile currentUserProfile = [SELECT id, name FROM Profile where id = :UserInfo.getProfileId() LIMIT 1];
    
    Map<String, String> sendEmailOnLeadConversionProfileIdWhitelist = new Map<String, String>();     
    
    for(LeadConversionEmailNotificationProfiles__c profile : LeadConversionEmailNotificationProfiles__c.getAll().values())
    {
        sendEmailOnLeadConversionProfileIdWhitelist.put(profile.profile_id__c, profile.profile_name__c);
    }
    
    System.debug('sendEmailOnLeadConversionProfileIdWhitelist: ' + sendEmailOnLeadConversionProfileIdWhitelist);
	System.debug('currentUserProfile: ' + currentUserProfile);
    
    Boolean currentUserProfileIsValid = currentUserProfile != null && sendEmailOnLeadConversionProfileIdWhitelist.containsKey(currentUserProfile.id);    
    
    Set<Id> convertedLeadOppsIds = new Set<Id>();
    Set<Id> convertedLeadContactsIds = new Set<Id>();
    Set<Id> leadIds = new Set<Id>();
    
    for(Lead l : Trigger.new)   
    {
		convertedLeadOppsIds.add(l.convertedOpportunityId);        
		convertedLeadContactsIds.add(l.convertedContactId);        
		leadIds.add(l.ownerId);        
    }
    
	Map<id, Opportunity> convertedOpps = new Map<Id, Opportunity>([SELECT id, name, ownerId, account.name, account.dealer_account__c FROM Opportunity where id = :convertedLeadOppsIds]);
	Map<Id, User> leadOwners = new Map<Id, User>([SELECT id, name, email FROM User where id = :leadIds]);
	Map<Id, Contact> leadContacts = new Map<Id, Contact>([SELECT id, owner.name FROM Contact where id = :convertedLeadContactsIds]);
    
    for(Lead l : Trigger.new)   
    {        
System.debug('l.IsConverted: ' + l.IsConverted + ' && currentUserProfileIsValid: ' + currentUserProfileIsValid);
        if(l.IsConverted && currentUserProfileIsValid)
        {
            Opportunity leadOpportunity = convertedOpps.containsKey(l.convertedOpportunityId) ? convertedOpps.get(l.convertedOpportunityId): null;

System.debug('leadOpportunity.account.dealer_account__c: ' + leadOpportunity.account.dealer_account__c);
        	if(leadOpportunity.account.dealer_account__c)
            {
                
    			User leadOwner = leadOwners.containsKey(l.ownerId) ? leadOwners.get(l.ownerId): null;
            	Contact leadContact = leadContacts.containsKey(l.convertedContactId) ? leadContacts.get(l.convertedContactId): null;
                                                                
System.debug('leadOwner: ' + leadOwner + ' && leadOpportunity: ' + leadOpportunity + ' && leadContact: ' + leadContact);
                if(leadOwner != null && leadOpportunity != null && leadContact != null)
                {
                    
                    // send notification email
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    
                    // send via object ID to get around email limits in code
                    mail.setTargetObjectId(leadOwner.id);  
                    // this must be set to false in order o sen email via object ID
                    mail.setSaveAsActivity(false);       
                    
                    mail.setSubject( String.format('New Opportunity Ready - {0} has been converted/ Avis d’occasion d’affaires', new String[]{ l.company }) );                        
                    mail.setPlainTextBody
                        (
                            String.format
                            (
                                '\n\rYour request to convert a lead into an opportunity has been successfully completed: \n\r' + 
                                '\t Opportunity: {0} \n\r' + 
                                '\t\t http://telus.force.com/?startURL={1} \n\r' +
                                '\t Contact: {2} \n\r' + 
                                '\t\t http://telus.force.com/?startURL={3} \n\n\n\r' +
                                '________________________________________________________\n\n\r' +
                                 
                                'Votre demande de conversion d’une possibilité de vente auprès d’un client potentiel en occasion d’affaires a été acceptée: \n\n\r' +                              
                                '\t Occasion: {0} \n\r' +
                                '\t\t http://telus.force.com/CCSiteLoginFrench?startURL={1} \n\r' +                                                          
                                '\t Contact: {2} \n\r' + 
                                '\t\t http://telus.force.com/CCSiteLoginFrench?startURL={3}' 
                                , 
                                new String[]{ leadOpportunity.name, leadOpportunity.id, leadContact.owner.name, leadContact.id }
                            )                                    
                        );
            
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });            
                }
            }
        }    
    }
}