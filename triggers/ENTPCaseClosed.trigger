trigger ENTPCaseClosed on Case (after update) {
    List<Id> caseIds = new List<Id>();
    List<String> collabEmails = new List<String>();
    
    for (Case c : Trigger.new) {
        if (c.Origin == 'ENTP') {
            Case oldC = Trigger.oldMap.get(c.Id);
            if (oldC.Status != 'Closed' && c.Status == 'Closed') {
                // ENTP Case has closed; do we need to notify collaborators?
                if (c.NotifyCollaboratorString__c != null && c.NotifyCollaboratorString__c.length() > 0) {
                    caseIds.add(c.Id);
                    collabEmails.add(c.NotifyCollaboratorString__c);                    
                }
            }
        }
    }

    // only call if ENTP case id list filled
    // NOTE: this may need to be made into a future method down the road, if so, must add System.isFuture() check in trigger
    if(caseIds.size() > 0){
       // ENTPNewCaseTriggerHelper.sendCaseClosedEmailNotification(caseIds, collabEmails); 
       //  VITILcareCaseTriggerHelper.afterUpdate(Trigger.oldMap, Trigger.newMap);
    }
    
    

}