trigger trac_SMETreq_addCreatorToProjectTeam on SMET_Requirement__c (before insert) {
    /*List<Id> SmetProjectIds = new List<Id>();
    List<SMET_Project_Team__c> SmetProjectTeamMembers = new List<SMET_Project_Team__c>();
        
    Map<Id, List<Id>> SmetProjectIdToTeamMembers = new Map<Id, List<Id>>();
    List<SMET_Project_Team__c> SmetProjectNewTeamMembers = new List<SMET_Project_Team__c>();
    
    // First, let's collect information about all the projects.
    for (Integer i = 0; i < trigger.new.size(); i++) {
        SMET_Requirement__c sr = trigger.new[i];
        if (null != sr.Related_SMET_Project__c) {
            SmetProjectIds.add(sr.Related_SMET_Project__c);
            SmetProjectIdToTeamMembers.put(sr.Related_SMET_Project__c, new List<Id>());
        }
    }
    if (0 < SmetProjectIds.size()) {
        SmetProjectTeamMembers = [Select Team_Member__c, SMET_Project__c From SMET_Project_Team__c Where SMET_Project__c in :SmetProjectIds]; 
    }
    for (SMET_Project_Team__c tm : SmetProjectTeamMembers) {
        List<Id> smptms = SmetProjectIdToTeamMembers.get(tm.SMET_Project__c);
        smptms.add(tm.Team_Member__c);
        SmetProjectIdToTeamMembers.put(tm.SMET_Project__c, smptms);
    }
    
    for (Integer i = 0; i < trigger.new.size(); i++) {
        SMET_Requirement__c sr = trigger.new[i];
        if (null != sr.Related_SMET_Project__c) {
            List<Id> smptms = SmetProjectIdToTeamMembers.get(sr.Related_SMET_Project__c);
            Boolean AlreadyInTeam = false;
            
            for (Integer x = 0; x < smptms.size(); x++) {
                if (smptms.get(x) == sr.OwnerId) {
                    AlreadyInTeam = true;
                }
            }
            
            if (false == AlreadyInTeam) {
                // User not in team. Add the user to the team!
                SmetProjectNewTeamMembers.add( new SMET_Project_Team__c(Team_Member__c = sr.OwnerId, SMET_Project__c = sr.Related_SMET_Project__c, Role__c = 'Requirement Submitter'));
            }
        }
    }
    if (0 < SmetProjectNewTeamMembers.size()) {
        insert SmetProjectNewTeamMembers;
    } */
}