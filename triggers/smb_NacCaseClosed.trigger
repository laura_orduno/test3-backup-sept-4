trigger smb_NacCaseClosed on Case (after update) {
    
    List<String> NacCaseRecordTypeDeveloperNames = new List<String> {'NACOrder', 'SMB_Care_NAC_Order'};
    
    List<RecordType> NocRecordTypes = [SELECT Id FROM RecordType WHERE DeveloperName in :NacCaseRecordTypeDeveloperNames];
    
    Set<Id> NacCaseRecordTypeIds = new Set<Id>();
    
    for (RecordType rt : NocRecordTypes) {
        NacCaseRecordTypeIds.add(rt.Id);
    }
    
    list<EmailTemplate> lstEmailTemplate = new list<EmailTemplate>();
    lstEmailTemplate = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'Internal_Template_for_DR0798'];
    
    List<Messaging.SingleEmailMessage> notificationEmails = new List<Messaging.SingleEmailMessage>();
    
    for (Case aCase : Trigger.new) {
        if (NacCaseRecordTypeIds.contains(aCase.RecordTypeId) && aCase.IsClosed) {
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            if (lstEmailTemplate.size()>0)
            {
                mail.setTargetObjectId(aCase.Parent_Case_Owner__c);
                mail.setTemplateId(lstEmailTemplate[0].Id);
                mail.setWhatId(aCase.Id);
            
                notificationEmails.add(mail);
            }
            
        }
    }
    
    if (notificationEmails.size() > 0) {
        
        //Messaging.sendEmail(notificationEmails);
        
    }
    

}