/*
###################################################################################################
# File..................: trgUpdateSerReq 
# Version...............: 32
# Created by............: Manish Kumar Srivastava (IBM)
# Created Date..........: 04-Dec-2014
# Description ..........: Updates the 'Track__c' field of the Service Request to trigger the execution
#  of 'smb_ServiceRequestTrigger' which in turn executes class 'SRS_ServiceRequestValidationHelper'
#  to update the 'Required to Submit' field on the Service Request.
###################################################################################################
*/
trigger trgUpdateSerReq on Contracts__c (after insert, after update, after delete, after undelete) {
List<Service_Request__c> lstSR = new List<Service_Request__c>();
set<id> setSRIds=new set<id>();
    for(Contracts__c objCon: (Trigger.isDelete?Trigger.old:Trigger.New))
    {
        if(objCon.Contract__c!=null)
        setSRIds.add(objCon.Contract__c);
    }
    if(setSRIds.size()>0)
    {
        for(Id srId : setSRIds){           
            Service_Request__c sr = new Service_Request__c(Id = srId,Track__c=system.now());
            lstSR.add(sr);
         }
         if(lstSR.size()>0)
         update lstSR;
     }
}