trigger UpdateServiceRequestByTeamMember on Service_Request_Employee__c (after insert, after update, after delete, after undelete) {
/*
###################################################################################################
# File..................: UpdateServiceRequestByTeamMember
# Version...............: 26
# Created by............: I-yuan Han (IBM)
# Created Date..........: 09-Oct-2014
# Last Modified by......: 
# Last Modified Date....: 
# Description ..........: When a Service Request Team Member is added or updated then update the 
#  'Track__c' field of the Service Request to trigger the execution # of 'smb_ServiceRequestTrigger'
#   which in turn executes class 'SRS_ServiceRequestValidationHelper' to update the 'Required to Submit'
#   field on the Service Request.
###################################################################################################
*/
    public List<Service_Request__c> serReq = new List<Service_Request__c>();
    public Set<Id> ServiceRequestIds;
    if(trigger.isInsert){       
        ServiceRequestIds= new Set<Id>();
        for(Service_Request_Employee__c srTeamMember : trigger.new)
             ServiceRequestIds.add(srTeamMember.Service_Request__c);
             
        for(Id srId : ServiceRequestIds){           
            Service_Request__c sr = new Service_Request__c(Id = srId);
            sr.Track__c = system.now();
            serReq.add(sr);
        }
        
    }
    if(trigger.isUpdate){
         ServiceRequestIds= new Set<Id>();
        for(Service_Request_Employee__c srTeamMember : trigger.new)
             ServiceRequestIds.add(srTeamMember.Service_Request__c);
             
        for(Id srId : ServiceRequestIds){           
            Service_Request__c sr = new Service_Request__c(Id = srId);
            sr.Track__c = system.now();
            serReq.add(sr);
        }
    }
    if(trigger.isDelete){
         ServiceRequestIds= new Set<Id>();
        for(Service_Request_Employee__c srTeamMember : trigger.old)
             ServiceRequestIds.add(srTeamMember.Service_Request__c);
             
        for(Id srId : ServiceRequestIds){           
            Service_Request__c sr = new Service_Request__c(Id = srId);
            sr.Track__c = system.now();
            serReq.add(sr);
        }    
    }
    if(trigger.isUndelete){
         ServiceRequestIds= new Set<Id>();
        for(Service_Request_Employee__c srTeamMember : trigger.new)
             ServiceRequestIds.add(srTeamMember.Service_Request__c);
             
        for(Id srId : ServiceRequestIds){           
            Service_Request__c sr = new Service_Request__c(Id = srId);
            sr.Track__c = system.now();
            serReq.add(sr);
        }      
    }
    system.debug('=====serReq===='+serReq);
    if(serReq != Null){
        update serReq;
    }   
}