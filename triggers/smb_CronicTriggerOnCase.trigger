/*
###########################################################################
# File..................: smb_CronicTriggerOnCase
# Version...............: 26
# Created by............: Vinay Sharma
# Created Date..........: 30-Apr-2013
# Last Modified by......: 
# Last Modified Date....: 
# Description...........: This trigger is fired on case creation and updation,
                            A root cause will be included in  case and maximum count of a root cause in all
                            cases of an account will be updated in account as No_Chronic_Incidents__c field
#
# Copyright (c) 2000-2012. Astadia, Inc. All Rights Reserved.
#
# Created by the Astadia, Inc. Modification must retain the above copyright notice.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any commercial purpose, without fee, and without a written
# agreement from Astadia, Inc., is hereby forbidden. Any modification to source
# code, must include this paragraph and copyright.
#
# Permission is not granted to anyone to use this software for commercial uses.
#
# Contact address: 2839 Paces Ferry Road, Suite 350, Atlanta, GA 30339
# Company URL : http://www.astadia.com
###########################################################################
*/
trigger smb_CronicTriggerOnCase on Case (after insert, after update) 
{
/* original implentation, susceptable to SOQL limits    
//    smb_cronicTriggerOnCaseHelper trgHelper = new smb_cronicTriggerOnCaseHelper();
//    trgHelper.getCaseRootCauseCount(trigger.new,trigger.oldMap);
*/
    // utilizing static calls
    Set<Id> acctIds = smb_cronicTriggerOnCaseHelper.getAccountIds(trigger.new,trigger.oldMap);
 
    if(Test.isRunningTest()){
        if(!System.isFuture() && !System.isBatch()){
            smb_cronicTriggerOnCaseHelper.updateAccountsFuture(acctIds);        
        }        
    }
    else{
	    smb_cronicTriggerOnCaseHelper.updateAccounts(acctIds);                
    }
}