trigger trac_Attachment on Attachment (before insert,before update, after insert,after update) {   
    try{
        if(label.Apttus_Decommissions_Trigger_Switch.equalsignorecase('on')){
            trac_AttachmentTriggerDispatcher dis = new trac_AttachmentTriggerDispatcher();
            dis.init();
            dis.execute();
            dis.finish();
        }
    }catch(exception e){
        Util.emailSystemError(e);
    }
}