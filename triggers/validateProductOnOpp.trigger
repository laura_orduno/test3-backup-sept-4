/******************************************************************************************/
/* Called on update of opp. Checks if the opp Stage has changed.                          */
/* If yes validates if any of thepli is inactive.                                         */
/* If yes - shows an error message.                                                       */
/*                                                                                        */
/* Author - Eroca Ho                                                                      */
/* Created - Sept 2009                                                                    */
/*                                                                                        */
/* Updated - Tom Muzyka                                                                   */
/* - If Update performed by Dataload or Admin Team, bypass trigger                        */
/* Update Date - May 2010  ver 2.0                                                        */
/******************************************************************************************/

trigger validateProductOnOpp on Opportunity (before update,after insert) {
    /* Start Traction Modifications */
    Map<String, validateProductOnOppSettings__c> mcs = validateProductOnOppSettings__c.getAll();
    Set<String> excludedRecordTypes = new Set<String>();
    Set<String> excludedProfileIds = new Set<String>();
    Set<String> excludedProfileNames = new Set<String>();
   
    if (mcs != null && mcs.size() > 0) {
        for (validateProductOnOppSettings__c vpoos : mcs.values()) {
            if (vpoos.Exclude_Record_Type_ID__c != null) {
                excludedRecordTypes.add((Id)vpoos.Exclude_Record_Type_Id__c);
            }
            if (vpoos.Exclude_Profile_ID__c != null) {
                excludedProfileIds.add((Id)vpoos.Exclude_Profile_Id__c);
            }
            if (vpoos.Exclude_Profile_Name__c != null) {
                excludedProfileNames.add(vpoos.Exclude_Profile_Name__c);
            }
        }
    }
    /* End Traction Modifications */
    
    /* Traction Note: This trigger was created using hard-coded IDs.
    This is very poor practice and should be modified to look up these IDs by name etc */    
        /* Traction Note 2: Modified July 27th, 2012 to use custom settings
    Id id = UserInfo.getProfileId();
    // Traction Note: These are TELUS partner users. (Not used in custom settings)
    if(id == '00e40000000nZc2' || id == '00e40000000ooSn' 
    || id == '00e40000000ooSs' || id == '00e40000000nUDh'){
    return;
    }
    Id DataLoading = ('00e40000000oqV9');
    Id DataLoading2 = ('00e40000000oqsI');
    Id SystemAdmin = ('00e30000000bppo');
    Id SystemAdminTEAM = ('00e30000000fHqU');
    Set<Id> Bypass_Ids = new Set<Id> {DataLoading,DataLoading2,SystemAdmin,SystemAdminTEAM};
    */
    Set<String> fieldsTracked = new Set<String> {'StageName'};
        Set<Id> oppIds = new Set<Id>();
    Map<Id,Opportunity> opportunityMap = new Map<Id,Opportunity>();
    for (Opportunity opp : trigger.new) {
        /* Traction Modifications: */
        if (excludedRecordTypes.contains(opp.RecordTypeId)) { continue; }
        /* End Traction Modifications */
        if(Trigger.isUpdate){
            if (Util.hasChanges(fieldsTracked, opp, trigger.oldMap.get(opp.Id))) {
                oppIds.add(opp.Id);
                opportunityMap.put(opp.Id,opp);
            }
        }
        if (Trigger.isInsert){
            if(opp.StageName == 'Closed Won'){                         
                oppIds.add(opp.Id);
                opportunityMap.put(opp.Id,opp);
            }
        }                
    }
    
    if (oppIds.isEmpty()) {
        return;
    }
    
    User u = [Select ProfileId, Profile.Name From User Where Id = :UserInfo.getUserId()];
    if (excludedProfileIds.contains(u.ProfileId) || excludedProfileNames.contains(u.Profile.Name)) {
        return;
    }
    
    //get pli details
    List<Opp_Product_Item__c> pliList = [Select Id, name, Opportunity__c, product__c, product__r.IsActive, 
                                         Product__r.Product_Family__c, LastModifiedBy.ProfileId, LastModifiedBy.Profile.Name from Opp_Product_Item__c where Opportunity__c In :oppIds];
    Map<Id, Set<String>> opp_InactiveProduct_Map = new Map<Id, Set<String>>();
    Map<Id, List<String>> opp_ActiveProduct_Map = new Map<Id, List<String>>();
    
    //make a map of inactive products to display in the error msg
    for (Opp_Product_Item__c pli : pliList) {
        if (!pli.Product__r.isActive) {
            Set<String> inActiveProducts = opp_InactiveProduct_Map.get(pli.Opportunity__c);
            if (inActiveProducts == null) {
                inActiveProducts = new Set<String>();
                opp_InactiveProduct_Map.put(pli.Opportunity__c, inActiveProducts);
            }
            inActiveProducts.add(pli.Product__r.Product_Family__c);
        } else {
            if (!opp_ActiveProduct_Map.containsKey(pli.Opportunity__c)) {
                opp_ActiveProduct_Map.put(pli.Opportunity__c, new List<String>());
            }
            opp_ActiveProduct_Map.get(pli.Opportunity__c).add(pli.Product__r.Product_Family__c);
        }
    }
    
    if (! opp_InactiveProduct_Map.isEmpty()) {
        for(id oppId : opp_InactiveProduct_Map.keyset()) {
            trigger.newMap.get(oppId).addError('<BR>' + Util.join(opp_InactiveProduct_Map.get(oppId), ', ', ' ') + System.Label.Opp_Line_Item_Inactive);
            // break; removed 7/27/2012 by Traction
        }   
    }
    
    for (Opportunity opp : opportunityMap.values() ) {
        if (opp.StageName != null &&opp.StageName.equals('Closed Won')) {
            if (opp_ActiveProduct_Map.get(opp.Id) == null || opp_ActiveProduct_Map.get(opp.Id).size() == 0) {
                trigger.newMap.get(opp.Id).addError('<BR>' +  System.Label.Opportunity_Product_Does_Not_Exist);
            }
        }
    }
}