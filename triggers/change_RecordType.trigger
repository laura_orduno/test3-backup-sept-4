/*---------------------------------------------------------------------------------------
----------This Trigger to change Record type based on Request type----------------------
----------This trigger is to support Complex/simple order-------------------------------*/

trigger change_RecordType on FOBOChecklist__c (before insert, before update) {

    list<FOBOChecklist__c> checklist = new list<FOBOChecklist__c>();
    for(FOBOChecklist__c fobo: trigger.new){
        checklist.add(fobo);
    }
    change_RecordType_Utility.changeRecordtype(checklist);
}