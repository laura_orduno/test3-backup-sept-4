/**************************************/
/* Called on delete of Account.       */
/* Prevents ECH Account Deletions     */
/*                                    */
/* Author - Tom Muzyka                */
/* Created - June 2010     v1.0       */
/**************************************/
trigger restrictDeleteAccount on Account (before delete)
{
   Id DataLoader = '00540000001372x';
   Id DataLoader2 = '00540000001CG5h';
   
   for (Account acc : trigger.old) 
   {
      if (UserInfo.GetUserId() == DataLoader || UserInfo.GetUserId() == DataLoader2) {
         return; 
      } else {
         if (acc.RCID__c == null && acc.CBUCID_Parent_ID__c == null) {
            return;
         } else {
            acc.addError('You cannot delete ECH Accounts');
         }
      }
   }
}