/*
###########################################################################
# File..................: SMB_updateBANOnRelatedOpportunities
# Version...............: 29
# Created by............: Kanishk Prasad
# Created Date..........: 27/04/2014
# Last Modified by......: 
# Last Modified Date....: 
# Description...........: This Trigger will run on After Update event of Account and will update
#                         all its related Opportunities with the BAN value if it was supplied in the Account record.
#                         The BAN on the Account will overwrite the BAN field on the related Opportunities.   
#
# Copyright (c) 2000-2012. Astadia, Inc. All Rights Reserved.
#
# Created by the Astadia, Inc. Modification must retain the above copyright notice. 
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any commercial purpose, without fee, and without a written
# agreement from Astadia, Inc., is hereby forbidden. Any modification to source
# code, must include this paragraph and copyright.
#
# Permission is not granted to anyone to use this software for commercial uses.
#
# Contact address: 2839 Paces Ferry Road, Suite 350, Atlanta, GA 30339
# Company URL : http://www.astadia.com
###########################################################################
*/

trigger SMB_updateBANOnRelatedOpportunities  on Account (after update) {

    //List<RecordType> lstRecordTypes=[select id from recordtype where sobjectType ='Opportunity' and developername like 'SMB_Care%'];
    set<Id> setSMBRecordTypes=SMB_Helper.fetchOpportunitySMBCareRecordTypes(null);
    Map<Id,Opportunity> mapOpp=new Map<Id,Opportunity>([Select id,accountId,BAN__c from Opportunity where accountid in:Trigger.New and Related_Orders__c=null and recordtypeid in :setSMBRecordTypes]);
    for(String oppId:mapOpp.keySet())
    {
        Opportunity opp=mapOpp.get(oppId);
        Account account=Trigger.NewMap.get(opp.accountId);
//        if(String.isNotBlank(account.BAN__c))    
            opp.BAN__c=account.BAN__c;
  //      else
    //        mapOpp.remove(oppId);
    }
    if(mapOpp.size()>0)
        update mapOpp.values();


}