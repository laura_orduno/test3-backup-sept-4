trigger smb_CPQQuoteTrigger on scpq__SciQuote__c (after insert, after update) {
      
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            //smb_CPQQuoteTrigger_Helper.updateOpportunityFields(Trigger.new); //Added by Bharat to update the related Opportunity fields
           smb_CPQQuoteTrigger_Helper.updateOpportunityFieldsAndStatus(trigger.new,false,null);
        }
        
        if(Trigger.isUpdate){           
            //smb_CPQQuoteTrigger_Helper.updateOpportunityFields(Trigger.new); //Added by Bharat to update the related Opportunity fields
            smb_CPQQuoteTrigger_Helper.updateOpportunityFieldsAndStatus(trigger.new,true,Trigger.oldMap);
        }
    }    
}