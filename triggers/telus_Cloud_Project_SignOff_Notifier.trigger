trigger telus_Cloud_Project_SignOff_Notifier on Cloud_Project_Sign_Off__c (after insert, after update) {
    
    
    // First, let's get the list of tasks.
    Savepoint sp = Database.setSavepoint();
    try {
        CloudProjectPortal__c eups = CloudProjectPortal__c.getInstance();
        if(!Test.isRunningTest()) {
            System.assert(eups != null, 'Failed to load critical portal settings');
            System.assert(eups.HMAC_Digest_Key__c != null, 'Failed to load critical digest settings');
        }

        Cloud_Project_Sign_Off__c[] atasks = trigger.new;
        Cloud_Project_Sign_Off__c[] tasks = new List<Cloud_Project_Sign_Off__c>();
        for (Cloud_Project_Sign_Off__c t : atasks) {
            if (t.Assignee_Email__c == null) { continue; }
            String oea = '';
            if (trigger.oldMap != null && trigger.oldMap.get(t.Id) != null && trigger.oldMap.get(t.Id).Assignee_Email__c != null) {
                 oea = trigger.oldMap.get(t.Id).Assignee_Email__c;
            }
            String nea = t.Assignee_Email__c;
            if (nea == null) { nea = ''; }
            if (trigger.isUpdate && (oea == nea) && t.Resend_to_Assignee__c == false) { continue; }
            if (t.Status__c == 'Completed') { continue; }
            tasks.add(new Cloud_Project_Sign_Off__c(Id = t.Id, Resend_to_Assignee__c = false, role__c = t.role__c, Asignee_Name__c = t.Asignee_Name__c, Assignee_Email__c = t.Assignee_Email__c, Cloud_Project_Key_Deliverable__c = t.Cloud_Project_Key_Deliverable__c));
        }
        if (tasks != null && tasks.size() > 0) {
        
            Messaging.SingleEmailMessage[] infomessages = new List<Messaging.SingleEmailMessage>();
            
            List<Id> KeyDeliverableIds = new List<Id>();
            List<Id> userIds = new List<Id>();
            
            for (Cloud_Project_Sign_Off__c task : tasks) {
                userIds.add(task.CreatedById);
                if (task.Cloud_Project_Key_Deliverable__c != null) {
                    KeyDeliverableIds.add(task.Cloud_Project_Key_Deliverable__c);
                }
            }
            Map<Id, User> createdUsers = new Map<Id, User>([Select Name From User Where Id in :userIds]);
            
            Map<Id, SMET_Requirement__c> KDMap = null;
            if (KeyDeliverableIds != null && KeyDeliverableIds.size() > 0) {
                KDMap = new Map<Id, SMET_Requirement__c>([Select Name, Project_Name__c From SMET_Requirement__c Where Id In :KeyDeliverableIds]);
            }
            
            for (Cloud_Project_Sign_Off__c t : tasks) {
                Blob digestBlob = Crypto.generateMac('hmacSHA512', Blob.valueOf(t.Id), EncodingUtil.base64decode(eups.HMAC_Digest_Key__c));
                System.assert(digestBlob != null, 'Failed to generate token for ' + t.Id);
                String token = EncodingUtil.base64Encode(digestBlob);
                System.assert(token != null, 'Failed to encode token for ' + t.Id);
                t.Token__c = token;

                User u = createdUsers.get(t.CreatedById);
                String creatorName = 'A Cloud Project Sign Off user';
                if (u != null) {
                    creatorName = u.Name;
                }
                
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setToAddresses(new String[] {t.Assignee_Email__c});
                mail.setReplyTo('sfdcsupport@telus.com');
                mail.setSenderDisplayName('Telus Cloud Project Sign Off Task');
                mail.setSubject('A Cloud Project Sign Off Task Has Been Assigned To You: ' + KDMap.get(t.Cloud_Project_Key_Deliverable__c).Name + ' - ' + KDMap.get(t.Cloud_Project_Key_Deliverable__c).Project_Name__c);
                mail.setBccSender(false);
                mail.setPlainTextBody(creatorName + ' has submitted the following Cloud Project Sign Off task for:<br/><br/>' +
                	'Project Name: ' + KDMap.get(t.Cloud_Project_Key_Deliverable__c).Project_Name__c + '\n' +
                    'Key Deliverable: ' + KDMap.get(t.Cloud_Project_Key_Deliverable__c).Name + '\n\n' +
                    'Please go to ' + eups.Portal_URL__c + '/CloudProjectPortalSignOffInformation?oid=' + t.Id + '&tok=' + t.Token__c + ' to view details of your sign off task and mark it as “Complete” within the next 7 calendar days.');
                
                mail.setHtmlBody(creatorName + ' has submitted the following Cloud Project Sign Off task for:<br/><br/>' + 
                    'Project Name: ' + KDMap.get(t.Cloud_Project_Key_Deliverable__c).Project_Name__c + '<br/>\n' +
                    'Key Deliverable: ' + KDMap.get(t.Cloud_Project_Key_Deliverable__c).Name + '<br/>\n<br/>\n<br/>\n' +
                    'Please <a href="' + eups.Portal_URL__c + '/CloudProjectPortalSignOffInformation?oid=' + t.Id + '&tok=' + EncodingUtil.urlEncode(t.Token__c, 'UTF-8') + '" style="font-weight: bold; font-size: 1.1em;">click here</a> to view details of your sign off task and mark it as “Complete” within the next 7 calendar days.');
                infomessages.add(mail);
            }
            if (infomessages != null && infomessages.size() > 0) { Messaging.sendEmail(infomessages); }
            update tasks;
        }
        
        if (trigger.isUpdate) {
        	Set<Id> userIds = new Set<Id>();
        	userIds.add(UserInfo.getUserId());
            
            Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage>();
            /* Don't forget the "External Assignee" */
            
            List<Id> KeyDeliverableIds = new List<Id>();
            List<Id> CloudProjectIds = new List<Id>();
            Map<Id,String> CloudProjectOwnerEmail = new Map<Id,String>();
            Map<Id,String> KDOwnerEmail = new Map<Id,String>();            
            
            Integer ix = 0;
            while (ix < trigger.new.size()) {
                if (trigger.old[ix].Status__c != 'Completed' && trigger.new[ix].Status__c == 'Completed' && trigger.new[ix].Assignee_Email__c != null) {

                    Cloud_Project_Sign_Off__c task = trigger.new[ix];
                    if (task.Cloud_Project_Key_Deliverable__c != null) {
                        KeyDeliverableIds.add(task.Cloud_Project_Key_Deliverable__c);
                    }
                }
                ix++;
            }
            
            Map<Id, SMET_Requirement__c> KDMap = new Map<Id, SMET_Requirement__c>();
            if (KeyDeliverableIds != null && KeyDeliverableIds.size() > 0) {
                KDMap = new Map<Id, SMET_Requirement__c>([Select Name, Ownerid, Related_SMET_Project__c From SMET_Requirement__c Where Id In :KeyDeliverableIds]);
            }
            
            for(Id tempKD : KDMap.keySet()){
            	if(KDMap.get(tempKD) != null){
	            	if(KDMap.get(tempKD).Related_SMET_Project__c != null) CloudProjectIds.add(KDMap.get(tempKD).Related_SMET_Project__c);
	            	if(KDMap.get(tempKD).Ownerid != null) userIds.add(KDMap.get(tempKD).Ownerid);
	            }
            }
            
            Map<Id, SMET_Project__c> CPMap = new Map<Id, SMET_Project__c>();
            if (CloudProjectIds != null && CloudProjectIds.size() > 0) {
                CPMap = new Map<Id, SMET_Project__c>([Select Name, Ownerid From SMET_Project__c Where Id In :CloudProjectIds]);
            }
            
            for(SMET_Project__c temp:CPMap.values()){
            	userIds.add(temp.Ownerid);
            }
            
            Map<id,User> userEmails = new Map<id,User>([Select id, Email From User where Id IN :userIds]);
            
            User activeUser = userEmails.get(UserInfo.getUserId());
                        
            for(SMET_Requirement__c temp:KDMap.values()){
            	KDOwnerEmail.put(temp.Ownerid,(userEmails.get(temp.Ownerid)).Email);
            }
            
            for(SMET_Project__c temp:CPMap.values()){
            	CloudProjectOwnerEmail.put(temp.Ownerid,(userEmails.get(temp.Ownerid)).Email);
            }
            
            Integer index = 0;
            while (index < trigger.new.size()) {
                Cloud_Project_Sign_Off__c ct = trigger.new[index];
                if (trigger.old[index].Status__c != 'Completed' && ct.Status__c == 'Completed' && ct.Assignee_Email__c != null) {
                    Messaging.reserveSingleEmailCapacity(3);
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setToAddresses(new List<String>{KDOwnerEmail.get((KDMap.get(ct.Cloud_Project_Key_Deliverable__c)).Ownerid), CloudProjectOwnerEmail.get((CPMap.get((KDMap.get(ct.Cloud_Project_Key_Deliverable__c)).Related_SMET_Project__c)).Ownerid)});
                    if (activeUser != null && activeUser.Email != null) {
                        mail.setReplyTo(activeUser.Email);
                        mail.setSenderDisplayName(UserInfo.getName());
                    } else {
                        mail.setReplyTo('sfdcsupport@telus.com');
                        mail.setSenderDisplayName('Telus Cloud Project Sign Off Task');
                    }
                    
                    String relatedTo = '';
                
                    if (ct.Cloud_Project_Key_Deliverable__c != null) {
                        if (KDMap.get(ct.Cloud_Project_Key_Deliverable__c) != null) {
                            relatedTo += KDMap.get(ct.Cloud_Project_Key_Deliverable__c).Name + ' (Key Deliverable)';
                        }
                    }
                    
                    mail.setSubject('Telus Cloud Project Sign Off Task Completed: ' + ct.KD_Name__c + ' - ' + ct.Project_Name__c);
                    mail.setPlainTextBody('Hello,\n\n' +
                         'The following Telus Cloud Project Sign Off task has been completed:\n\n' +
                         'Subject: ' + ct.KD_Name__c + ' - ' + ct.Project_Name__c + '\n' +
                         'Related To: ' + relatedTo + '\n' +
                         'Project Name: ' + ct.Project_Name__c + '\n' +
                         'Key Deliverable: ' + ct.KD_Name__c + '\n' +
                         'Name: ' + ct.Asignee_Name__c + '\n' +
                         'Email: ' + ct.Assignee_Email__c + '\n' +
                         'Role: ' + ct.Role__c + '\n' +
                         'Status: ' + ct.Status__c + '\n' +
                         'Date Completed: ' + Date.today().format() + '\n\n' +
                         'For more details on this task, ' + eups.Base_URL__c + '/' + ct.Id + '\n');
                    mail.setHtmlBody('Hello,<br/>\n<br/>\n' +
                         'The following Telus Cloud Project Sign Off task has been completed:<br/>\n<br/>\n' +
                         'Subject: ' + ct.KD_Name__c + ' - ' + ct.Project_Name__c + '<br/>\n' +
                         'Related To: ' + relatedTo + '<br/>\n' +
                         'Project Name: ' + ct.Project_Name__c + '<br/>\n' +
                         'Key Deliverable: ' + ct.KD_Name__c + '<br/>\n' +                         
                         'Name: ' + ct.Asignee_Name__c + '<br/>\n' +
                         'Email: ' + ct.Assignee_Email__c + '<br/>\n' +
                         'Role: ' + ct.Role__c + '<br/>\n' +
                         'Status: ' + ct.Status__c + '<br/>\n' +
                         'Date Completed: ' + Date.today().format() + '<br/>\n<br/>\n' +
                         'For more details on this task, <a href="' + eups.Base_URL__c + '/' + ct.Id + '">click here.</a><br/>\n');
                    // Send the email you have created. 
                    messages.add(mail);
                    
                    mail = new Messaging.SingleEmailMessage();
                    mail.setToAddresses(new List<String>{ct.Assignee_Email__c});
                    if (activeUser != null && activeUser.Email != null) {
                        mail.setReplyTo(activeUser.Email);
                        mail.setSenderDisplayName(UserInfo.getName());
                    } else {
                        mail.setReplyTo('sfdcsupport@telus.com');
                        mail.setSenderDisplayName('Telus Cloud Project Sign Off Task');
                    }
                    
                    relatedTo = '';
                
                    if (ct.Cloud_Project_Key_Deliverable__c != null) {
                        if (KDMap.get(ct.Cloud_Project_Key_Deliverable__c) != null) {
                            relatedTo += KDMap.get(ct.Cloud_Project_Key_Deliverable__c).Name + ' (Key Deliverable)';
                        }
                    }
                    
                    mail.setSubject('Telus Cloud Project Sign Off Task Completed: ' + ct.KD_Name__c + ' - ' + ct.Project_Name__c);
                    mail.setPlainTextBody('Hello,\n\n' +
                         'The following Telus Cloud Project Sign Off task has been completed:\n\n' +
                         'Subject: ' + ct.KD_Name__c + ' - ' + ct.Project_Name__c + '\n' +
                         'Related To: ' + relatedTo + '\n' +
                         'Project Name: ' + ct.Project_Name__c + '\n' +
                         'Key Deliverable: ' + ct.KD_Name__c + '\n' +
                         'Name: ' + ct.Asignee_Name__c + '\n' +
                         'Email: ' + ct.Assignee_Email__c + '\n' +
                         'Role: ' + ct.Role__c + '\n' +
                         'Status: ' + ct.Status__c + '\n' +
                         'Date Completed: ' + Date.today().format() + '\n\n' +
                         'For more details on this task, ' + eups.Portal_URL__c + '/' + ct.Id + '\n');
                    mail.setHtmlBody('Hello,<br/>\n<br/>\n' +
                         'The following Telus Cloud Project Sign Off task has been completed:<br/>\n<br/>\n' +
                         'Subject: ' + ct.KD_Name__c + ' - ' + ct.Project_Name__c + '<br/>\n' +
                         'Related To: ' + relatedTo + '<br/>\n' +
                         'Project Name: ' + ct.Project_Name__c + '<br/>\n' +
                         'Key Deliverable: ' + ct.KD_Name__c + '<br/>\n' +                         
                         'Name: ' + ct.Asignee_Name__c + '<br/>\n' +
                         'Email: ' + ct.Assignee_Email__c + '<br/>\n' +
                         'Role: ' + ct.Role__c + '<br/>\n' +
                         'Status: ' + ct.Status__c + '<br/>\n' +
                         'Date Completed: ' + Date.today().format() + '<br/>\n<br/>\n' +
                         'For more details on this task, <a href="' + eups.Portal_URL__c + '/CloudProjectPortalSignOffInformation?oid=' + ct.Id + '&tok=' + EncodingUtil.urlEncode(ct.Token__c, 'UTF-8') + '">here.</a>');
                    // Send the email you have created. 
                    messages.add(mail);
                    
                }
                /* End External Assignee */
                index++;
            }
            if (messages != null && messages.size() > 0) { Messaging.sendEmail(messages); }
        }
    } catch (Exception e) { Database.rollback(sp); QuoteUtilities.log(e, false); trigger.new[0].addError(e.getMessage()); }
}