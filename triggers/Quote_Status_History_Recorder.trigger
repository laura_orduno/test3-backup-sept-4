trigger Quote_Status_History_Recorder on SBQQ__Quote__c (after update) {

    SBQQ__Quote__c[] timestampedQuotes = new SBQQ__Quote__c[]{};
    for (SBQQ__Quote__c newQuote : trigger.new) {
        if (newQuote.SBQQ__Status__c == trigger.oldMap.get(newQuote.Id).SBQQ__Status__c) { continue; }
        SBQQ__Quote__c quote = new SBQQ__Quote__c(Id = newQuote.Id);
        if (newQuote.SBQQ__Status__c == QuoteStatus.Signed_In_Person || newQuote.SBQQ__Status__c == QuoteStatus.Accepted_Awaiting_Provisioning) {
            quote.Timestamp_Agreement_Accepted__c = Datetime.now();
            quote.Timestamp_Agreement_Accepted_User__c = newQuote.LastModifiedById;
            timestampedQuotes.add(quote);
            continue;
        }
        if (newQuote.SBQQ__Status__c == QuoteStatus.Provisioning_In_Progress) {
            quote.Timestamp_Provisioning_in_Progress__c = Datetime.now();
            quote.Timestamp_Provisioning_in_Progress_User__c = newQuote.LastModifiedById;
            timestampedQuotes.add(quote);
            continue;
        }
        if (newQuote.SBQQ__Status__c == QuoteStatus.Wireline_Order_Entry_Complete) {
            quote.Timestamp_Wireline_Order_Entry_Complete__c = Datetime.now();
            quote.TS_Wireline_Order_Entry_Complete_User__c = newQuote.LastModifiedById;
            timestampedQuotes.add(quote);
            continue;
        }
        if (newQuote.SBQQ__Status__c == QuoteStatus.Wireless_Provisioning_Complete) {
            quote.Timestamp_Wireless_Provisioning_Complete__c = Datetime.now();
            quote.TS_Wireless_Provisioning_Complete_User__c = newQuote.LastModifiedById;
            timestampedQuotes.add(quote);
            continue;
        }
        if (newQuote.SBQQ__Status__c == QuoteStatus.Provisioning_Complete) {
            quote.Timestamp_Provisioning_Complete__c = Datetime.now();
            quote.Timestamp_Provisioning_Complete_User__c = newQuote.LastModifiedById;
            timestampedQuotes.add(quote);
            continue;
        }
    }
    if (timestampedQuotes.size() != 0) {
        update timestampedQuotes;
    }
    /*
        Records Quote Status History information for reporting
    */
    Quote_Status_History__c[] quoteStatusHistories = new List<Quote_Status_History__c>();
    for (SBQQ__Quote__c newQuote : trigger.new) {
        if (newQuote.SBQQ__Status__c == trigger.oldMap.get(newQuote.Id).SBQQ__Status__c) { continue; }
        quoteStatusHistories.add(new Quote_Status_History__c(User__c = UserInfo.getUserId(), Quote__c = newQuote.Id, New_Status__c = newQuote.SBQQ__Status__c, Old_Status__c = trigger.oldMap.get(newQuote.Id).SBQQ__Status__c));
    }
    insert quoteStatusHistories;
}