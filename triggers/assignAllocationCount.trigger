trigger assignAllocationCount on ADRA__Assignee__c (before insert) {
    
   Map<Id, List<ADRA__Assignee__c>> ruleIdToAssignees = new Map<Id, List<ADRA__Assignee__c>>();
     
   // Loading all the allocation rules that these assignees are part of.     
   for (ADRA__Assignee__c assignee : Trigger.new) {
    if(ruleIdToAssignees.containsKey(assignee.ADRA__Rule__c))
        ruleIdToAssignees.get(assignee.ADRA__Rule__c).add(assignee);
    else
        ruleIdToAssignees.put(assignee.ADRA__Rule__c, new List<ADRA__Assignee__c>{ assignee });
   }
   
   System.debug('Rule IDs----->:::'+ ruleIdToAssignees);
   if (ruleIdToAssignees.size() > 0) {
     if(Trigger.isBefore && Trigger.isInsert) {
            AssigneeService.assignAllocationCount(ruleIdToAssignees);
       }
   }
}