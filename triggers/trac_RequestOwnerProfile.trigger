trigger trac_RequestOwnerProfile on SMET_Request__c (before insert, before update) {
    for (SMET_Request__c r : trigger.new) {
        String oid = (String) r.OwnerId;
        if (oid.substring(0,3) != '00G'){
            r.Owner_Detail__c = r.OwnerId;
        }
    }
}