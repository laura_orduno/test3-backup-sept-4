trigger CaseTrigger on Case (after insert, before update, after update, before insert) {
     trac_TriggerHandlerBase.triggerHandler(new trac_Case());
}