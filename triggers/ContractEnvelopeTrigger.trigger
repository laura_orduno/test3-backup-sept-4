/**
 * Single trigger that will handle all scenarios by using handler class.  Please do not
 * build another trigger within same object to optimize SOQL and code reusability.
 */
trigger ContractEnvelopeTrigger on vlocity_cmt__ContractEnvelope__c (before insert,before update,after insert, after update) {
    try{
        //system.debug('CheckRecursion.runOnce()' +CheckRecursion.runOnce());
        //Adharsh: Commenting out as the Recurrsion needs to be run on event context
        //if(CheckRecursion.runOnce())
        //{
            system.debug('Envelop trigger call');
            if(label.Apttus_Decommissions_Trigger_Switch.equalsignorecase('on')){
                ContractEnvelopeTriggerHandler handler=new ContractEnvelopeTriggerHandler();
            }
        //}
    }catch(exception e){
        
            System.debug(LoggingLevel.ERROR, 'Exception is '+e);
            System.debug(LoggingLevel.ERROR, 'Exception stack trace '+e.getStackTraceString());
    }
}