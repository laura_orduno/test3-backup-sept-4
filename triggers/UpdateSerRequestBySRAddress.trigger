trigger UpdateSerRequestBySRAddress on SRS_Service_Address__c (after insert, after update, after delete, after undelete) {
/*
###################################################################################################
# File..................: UpdateSerRequestBySRAddress
# Version...............: 26
# Created by............: Prashant Bhardwaj (IBM)
# Created Date..........: 23-Jul-2014
# Last Modified by......: Hari Neelalaka K (IBM)
# Last Modified Date....: 13-Jan-2015
# Description ..........: Updates the 'Track__c' field of the Service Request to trigger the execution
#  of 'smb_ServiceRequestTrigger' which in turn executes class 'SRS_ServiceRequestValidationHelper'
#  to update the 'Required to Submit' field on the Service Request.
###################################################################################################
*/
    public List<Service_Request__c> serReq = new List<Service_Request__c>();
    public Set<Id> ServiceRequestIds;
    if(trigger.isInsert){       
        ServiceRequestIds= new Set<Id>();
        for(SRS_Service_Address__c srAddress : trigger.new)
             ServiceRequestIds.add(srAddress.Service_Request__c);
             
        for(Id srId : ServiceRequestIds){           
            Service_Request__c sr = new Service_Request__c(Id = srId);
            sr.Track__c = system.now();
            serReq.add(sr);
        }
        
    }
    
    // PAC R2 - Code commneted as this logic has been taken care in PAC 
    /*if(Trigger.isInsert && Trigger.isAfter){
       
        boolean flag = false;       
        String ServiceAddressId;
        List<Case> listCases = new List<Case>();
        Case objCase;       
        list<Messaging.Singleemailmessage> theEmailMessages = new list<Messaging.Singleemailmessage>(); 
        RecordType recType = [SELECT Id from Recordtype where sobjectType = 'Case' and developerName ='SMB_Care_SACG' and IsActive =true limit 1];
        User u = [select Id, Name from User where Id = :UserInfo.getUserId()];
        
                              
        List<SRS_Service_Address__c> addressL = [SELECT Id,Name,Full_Address__c,Address__c,city__c,province__c,Service_Request__c,Address__r.Send_to_SACG__c,Address__r.Notes__c,Service_Request__r.Account_Name__c,Service_Request__r.PrimaryContact__c,Service_Request__r.Opportunity__c FROM SRS_Service_Address__c WHERE Id IN :Trigger.new];
        for(SRS_Service_Address__c srAddress : trigger.new)
        { 
            if (addressL.size() > 0) {
                for (SRS_Service_Address__c address : addressL) {
                    if(address.Address__r.Send_to_SACG__c == True)  
                    {
                        flag = true;
                        ServiceAddressId = address.Address__c; 
                        objCase = new Case();
                        objCase.RecordTypeId = recType.Id;
                        objCase.OwnerId = u.Id;
                        objCase.Account_Name__c = address.Service_Request__r.Account_Name__c; // SR account name
                        objCase.Service_Location_Address__c = address.Full_Address__c +' '+ address.City__c +' ' +address.Province__c;
                        //objCase.Full_Address__c=address.Full_Address__c +' '+ address.City__c +' ' +address.Province__c;
                        objCase.Description = address.Address__r.Notes__c; // Notes for SACG team
                        objCase.Subject = 'SACG Request';
                        objCase.Priority = 'Medium';
                        objCase.Origin = 'SACG Request';
                        objCase.Status = 'Open';
                        ObjCase.Notes__c = ''; //not known
                        objCase.ContactId = address.Service_Request__r.PrimaryContact__c;  //SR primary contact
                        objCase.Opportunity__c = address.Service_Request__r.Opportunity__c; // SR opp name
                        objCase.Type ='SACG';               
                        Id contactId = objCase.ContactId;
                 //     Contact theContact = [SELECT Id, FirstName, LastName From Contact WHERE Id=:contactId];
                 //       if (theContact != null)
                  //              {
                  //                  objCase.Contact_First_Name__c = theContact.FirstName;
                  //                  objCase.Contact_Last_Name__c = theContact.LastName;
                  //              }
                  //      system.debug('>>> first name: '+objCase.Contact_First_Name__c);
                  //      system.debug('>>> last name: '+objCase.Contact_Last_Name__c);
                    }
                }

            }                                                                      
         
        }  
             listCases.add(objCase);
             system.debug('>>> cases list:'+listCases);
        if(flag)
        {
            if(listCases.size()>0)
            {
                insert listCases;
                map<String,SMB_SACGSettings__c> mapSACGSettings = SMB_SACGSettings__c.getAll();
                    String strUsers = mapSACGSettings.get('ContactIds').Data__c;
                    if(strUsers.trim().length()>0){
                        string [] arrUsers = strUsers.split(';',0);
                        if(arrUsers.size()>0){
                            for (Case theCase: listCases)
                            {
                                for (String str:arrUsers)
                                {
                                    system.debug('***** Creating Email: ' + str);
                                    theEmailMessages.add(CreateEmail(str, theCase.Id,mapSACGSettings.get('OrgWideEmailAddressId').Data__c,mapSACGSettings.get('TemplateId').Data__c)); 
                                }
                            }
                            if (theEmailMessages != null && theEmailMessages.size() > 0)
                            {
                            system.debug('>>>>> Sending Email:' + theEmailMessages);
                            Messaging.sendEmail(theEmailMessages, false);                   
                            }
                        }
                    
                    }
            }   

        }
        
    }
  
  public static Messaging.Singleemailmessage CreateEmail  (string targetId, Id theCaseId, String orgwideId, string templateId)
     {
        Messaging.Singleemailmessage theEmailMessage = new Messaging.Singleemailmessage();
        theEmailMessage.setOrgWideEmailAddressId(orgwideId);
        theEmailMessage.setTemplateId(templateId);                                                
        theEmailMessage.setSaveAsActivity(true);                        
        theEmailMessage.setTargetObjectId(targetId);                      
        theEmailMessage.setWhatId(theCaseId);       
        return theEmailMessage;     
     }
     */    
    if(trigger.isUpdate){
         ServiceRequestIds= new Set<Id>();
        for(SRS_Service_Address__c srAddress : trigger.new)
             ServiceRequestIds.add(srAddress.Service_Request__c);
             
        for(Id srId : ServiceRequestIds){           
            Service_Request__c sr = new Service_Request__c(Id = srId);
            sr.Track__c = system.now();
            serReq.add(sr);
        }
    }
    if(trigger.isDelete){
         ServiceRequestIds= new Set<Id>();
        for(SRS_Service_Address__c srAddress : trigger.old)
             ServiceRequestIds.add(srAddress.Service_Request__c);
             
        for(Id srId : ServiceRequestIds){           
            Service_Request__c sr = new Service_Request__c(Id = srId);
            sr.Track__c = system.now();
            serReq.add(sr);
        }    
    }
    if(trigger.isUndelete){
         ServiceRequestIds= new Set<Id>();
        for(SRS_Service_Address__c srAddress : trigger.new)
             ServiceRequestIds.add(srAddress.Service_Request__c);
             
        for(Id srId : ServiceRequestIds){           
            Service_Request__c sr = new Service_Request__c(Id = srId);
            sr.Track__c = system.now();
            serReq.add(sr);
        }      
    }
    system.debug('=====serReq===='+serReq);
    if(serReq != Null){
        update serReq;
    }   
}