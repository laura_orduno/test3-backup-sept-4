trigger BillingAdjustmentTrigger on Billing_Adjustment__c (before insert, before update,after insert, after update) {
    
    //System.debug('Trigger - BillingAdjustmentTrigger - Start : ' + getTriggerAction());
    //System.debug('before Handler ' + Trigger.new[0]);
    
    if(Trigger.isBefore && Trigger.isInsert) {
      BillingAdjustmentTriggerHandler.handleBeforeInsert(Trigger.new);
    } else if(Trigger.isBefore && Trigger.isUpdate) {
      BillingAdjustmentTriggerHandler.handleBeforeUpdate(Trigger.new);
    }else if(Trigger.isAfter && Trigger.isUpdate) {
      BillingAdjustmentTriggerHandler.handleAfterUpdate(Trigger.new);
    }else if(Trigger.isAfter && Trigger.isInsert) {
      BillingAdjustmentTriggerHandler.handleAfterInsert(Trigger.new);
    }
   // System.debug('After Handler ' + Trigger.new[0]);
    /* This method is used for Debug
    private String getTriggerAction(){
        String out ='';
        if(Trigger.isBefore){ out = 'Before';}else{ out = 'After';}
        if(Trigger.isUpdate){ out += ' Update';}
        if(Trigger.isInsert){ out += ' Insert';}
        return out;
    }
	*/
 
}