trigger EmailOptIn on Lead (after insert, after update) {
    List <Lead> leadList = trigger.new;
    System.debug('##########Lead!!!!!'+leadList);
    Map<String, Boolean> emailMap = new Map<String, Boolean>();
    for(Lead l : leadList){
        if(trigger.isUpdate){
            Lead beforeUpdate = System.Trigger.oldMap.get(l.Id);
            if(beforeUpdate.Email_Opt_In__c!=l.Email_Opt_In__c){
            	if(l.email != null){
                	emailMap.put(l.Email,l.Email_Opt_In__c);
            	}
            }
        }
        else{
	       	if(l.email != null){
           		emailMap.put(l.Email,l.Email_Opt_In__c);
	       	}
        }
    }    
    System.debug('##########emailMap !!!!!'+emailMap );
    List<Contact> conList =[select id,Email,Email_Opt_In__c from Contact where Email in :emailMap.keySet()];
    System.debug('##########Contact List!!!!!'+conList);   
    for(Contact c: conList){
            c.Email_Opt_In__c=emailMap.get(c.Email);
    }    
    try{
        update conList;
    }catch(Exception ex){} 
    

}