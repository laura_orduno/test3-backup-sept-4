/**
 * This trigger will Update the Credit Profile object when the encrypted fields are updated/changed
 * It will indicate whether a field is empty (or not) as well as store the hash values to allow for the CRIN search
 */
trigger CreditProfileEncryptedFields on Credit_Profile__c (before insert, before update) {
    for (Credit_Profile__c cp: Trigger.new) {
        if(String.isBlank(cp.Driver_s_License__c)) 
            cp.Driver_s_License_Hash__c = null;
        else  
            cp.Driver_s_License_Hash__c = CreditCrypto.hash(cp.Driver_s_License__c);
            
        if(String.isBlank(cp.Health_Card_No__c)) 
            cp.Health_Card_No_Hash__c = null;
        else 
            cp.Health_Card_No_Hash__c = CreditCrypto.hash(cp.Health_Card_No__c);

        if(String.isBlank(cp.SIN__c)) 
            cp.SIN_Hash__c = null;
        else 
            cp.SIN_Hash__c = CreditCrypto.hash(cp.SIN__c);

        if(String.isBlank(cp.Provincial_ID__c)) 
            cp.Provincial_ID_Hash__c = null;
        else 
            cp.Provincial_ID_Hash__c = CreditCrypto.hash(cp.Provincial_ID__c);

        if(String.isBlank(cp.Passport__c)) 
            cp.Passport_Hash__c = null;
        else 
            cp.Passport_Hash__c = CreditCrypto.hash(cp.Passport__c);		        
    }
    	
}