/**
 * Copies Tier and Vertical values from related User record to Assignee records being 
 * processed by this trigger.
 * 
 * @author Max Rudman
 * @since 4/2/2009
 */
trigger SetAssigneeFieldsByAssignee on ADRA__Assignee__c (before insert, before update) {
	List<ADRA__Assignee__c> changed = new List<ADRA__Assignee__c>();
	Set<Id> userIds = new Set<Id>();
	for (ADRA__Assignee__c assignee : Trigger.new) {
		if (assignee.ADRA__User__c == null) {
			continue;
		}
		
		if (Trigger.isInsert) {
			changed.add(assignee);
			userIds.add(assignee.ADRA__User__c );
		} else if (Trigger.isUpdate) {
			ADRA__Assignee__c old = Trigger.oldMap.get(assignee.Id);
			if (assignee.ADRA__User__c != old.ADRA__User__c) {
				changed.add(assignee);
				userIds.add(assignee.ADRA__User__c);
			}
		}
	}
	
	if (userIds.size() > 0) {
		// Load and index User records
		Map<Id,User> usersById = new Map<Id,User>([SELECT Id, Tier__c, User_Vertical__c FROM User WHERE Id IN :userIds]);
		
		for (ADRA__Assignee__c assignee : Trigger.new) {
			User user = usersById.get(assignee.ADRA__User__c);
			if (user != null) {
				assignee.Assignee_Tier__c = user.Tier__c;
				if (user.User_Vertical__c == null) {
					assignee.Assignee_Vertical__c = null;
				} else {
					assignee.Assignee_Vertical__c = user.User_Vertical__c.replaceAll(';',',');
				}
			}
		}
	}
}