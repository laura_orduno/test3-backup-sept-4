trigger QuoteBefore on SBQQ__Quote__c (before insert, before update) {
    for (SBQQ__Quote__c quote : Trigger.new) {
        quote.Kana_Queue__c = quote.Provision_Request_Email__c;
    }
    if (trigger.isInsert) {
        for (SBQQ__Quote__c quote : Trigger.new) {
            if (quote.SBQQ__SalesRep__c == null) {
                quote.SBQQ__SalesRep__c = UserInfo.getUserId();//quote.CreatedById;
            }
        }
    } else {
        for (SBQQ__Quote__c quote : Trigger.new) {
            if (quote.SBQQ__SalesRep__c == null) {
                quote.SBQQ__SalesRep__c = quote.CreatedById;
            }
        }
    }
}