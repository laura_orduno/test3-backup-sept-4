/*
add on line 45 to fix sending complete email twice on 2016/11/28
*/
trigger trac_Task_Completion_Notifier on Task (after insert, after update) {
    // If this is a new Task, create a(n) assignee record(s) for the creator, and potentially the assignee.
    if (Trigger.isInsert) {
        List<Task_Assignee__c> TAs = new List<Task_Assignee__c>();
        for (Task t : trigger.new) {
            Task_Assignee__c TA = new Task_Assignee__c (Name = t.Id, Assignee__c = t.CreatedById, Assignee_Order__c = 1);
            TAs.add(TA);
            if (t.OwnerId != t.CreatedById) {
                Task_Assignee__c TA2 = new Task_Assignee__c (Name = t.Id, Assignee__c = t.OwnerId, Assignee_Order__c = 2);
                TAs.add(TA2);
            }
        }
        Insert TAs;
    }
    
    // If these tasks for being updated, create new assignee records in their new and old 
    if (Trigger.isUpdate) {
        Map<Id, Integer> taskIdsOffset = new Map<Id, Integer>();
        Map<Id, Integer> taskIdsAssigneeNumber = new Map<Id, Integer>();
        for (Integer i = 0; i < trigger.new.size(); i++) {
            if ((trigger.new[i].OwnerId != trigger.old[i].OwnerId) && (trigger.new[i].Status != 'Status')) {
                taskIdsOffset.put(trigger.new[i].Id, i);
                taskIdsAssigneeNumber.put(trigger.new[i].Id, 0);
            }
        }
        List<Task_Assignee__c> previousAssignees = [SELECT Id, Name, Assignee_Order__c, Assignee__c FROM Task_Assignee__c WHERE Name IN :taskIdsOffset.keyset()];
        for (Task_Assignee__c p : previousAssignees) {
            if (taskIdsAssigneeNumber.get(p.Name) < p.Assignee_Order__c) {
                taskIdsAssigneeNumber.put(p.Name, Integer.valueOf(p.Assignee_Order__c));
            } 
        }
        
        List<Task_Assignee__c> newAssignees = new List<Task_Assignee__c>();
        for (Id x : taskIdsOffset.keyset()) {
            Task nt = trigger.new[taskIdsOffset.get(x)];
            Task ot = trigger.old[taskIdsOffset.get(x)];
            Integer o = taskIdsAssigneeNumber.get(x) + 1;
            Task_Assignee__c ta = new Task_Assignee__c(Name = nt.Id, Assignee__c = nt.OwnerId, Assignee_Order__c = o);
            newAssignees.add(ta);
        }
        Insert newAssignees;
    }
    
    // If the task has changed to 'Completed' send email notifications.
    if (Trigger.isUpdate && Trigger.new.size() == 1 && Trigger.new[0].Status == 'Completed' && Trigger.old[0].Status != 'Completed' && Trigger.new[0].SRS_Task_Completed_Date__c == null) {
        
        ExternalUserPortal__c eups = ExternalUserPortal__c.getInstance();
        System.assert(eups != null, 'Failed to load critical portal settings');
        
        String s = trigger.new[0].id;
        List<Task_Assignee__c> lta = [SELECT Name, Assignee__c, Assignee_Order__c FROM Task_Assignee__c WHERE Name = :s];
        Set<Id> userSet = new Set<Id>();
        for (Task_Assignee__c tx : lta) {
            userSet.add(tx.Assignee__c);
        }
        Task ct = trigger.new[0];
        User activeUser = [Select Email From User where Id = :UserInfo.getUserId() limit 1];
        List<User> uta = [Select Name, Email FROM User WHERE Id IN :userSet];
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage>();
        
        Contact contact = null;
        Lead lead = null;
        String rtWho = '';
        if (ct.WhoId != null) {
            if (String.valueOf(ct.WhoId).substring(0, 3) == '003') {
                // Contact!
                contact = [Select Name From Contact Where Id = :ct.WhoId Limit 1];
                if (contact != null) {
                    rtWho += contact.Name + ' (Contact)';
                }
            } else if (String.valueOf(ct.WhoId).substring(0, 3) == '00Q') {
                // Lead!
                lead = [Select Name From Lead Where Id = :ct.WhoId Limit 1];
                if (lead != null) {
                    rtWho += lead.Name + ' (Lead)';
                }
            }
        }
        
        Account account = null;
        Opportunity opportunity = null;
        String relatedTo = '';
        if (ct.WhatId != null) {
            if (String.valueOf(ct.WhatId).substring(0, 3) == '001') {
                account = [Select Name From Account Where Id = :ct.WhatId Limit 1];
                if (account != null) {
                    relatedTo += account.Name + ' (Account)';
                }
            } else if (String.valueOf(ct.WhatId).substring(0, 3) == '006') {
                opportunity = [Select Name From Opportunity Where Id = :ct.WhatId Limit 1];
                if (opportunity != null) {
                    relatedTo += opportunity.Name + ' (Opportunity)';
                }
            }
        }
        
        
        for (User u : uta) { If (u.Id != UserInfo.getUserId()) {
            Messaging.reserveSingleEmailCapacity(1);
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            //mail.setToAddresses(new List<String>{u.email});
            mail.setTargetObjectId(u.Id);
            if (activeUser != null && activeUser.Email != null) {
                mail.setReplyTo(activeUser.Email);
                mail.setSenderDisplayName(UserInfo.getName());
            } else {
                mail.setReplyTo('sfdcsupport@telus.com');
                mail.setSenderDisplayName('Salesforce Tasks');
            }
            mail.setSubject('Task Completed: ' + ct.Subject);
            mail.setBccSender(false);
            mail.setUseSignature(false);
            mail.setPlainTextBody('Hello '+u.Name + ',\n\n' +
                 'The task that you assigned has been completed.\n\n' +
                 'Subject: ' + ct.Subject + '\n' +
                 'Related To: ' + relatedTo + '\n' +
                 'Name: ' + rtWho + '\n' +
                 'Status: ' + ct.Status + '\n' +
                 'Date Completed: ' + Date.today().format() + '\n' +
                 'Comments: ' + String.valueOf(ct.Description) + '\n\n' +
                 'For more details on this task, go to ' + eups.Base_URL__c+'/'+ct.Id);
            mail.setHtmlBody('Hello '+u.Name + ',<br/>\n<br/>\n' +
                 'The task that you assigned has been completed.<br/>\n<br/>\n' +
                 'Subject: ' + ct.Subject + '<br/>\n' +
                 'Related To: ' + relatedTo + '<br/>\n' +
                 'Name: ' + rtWho + '<br/>\n' +
                 'Status: ' + ct.Status + '<br/>\n' +
                 'Date Completed: ' + Date.today().format() + '<br/>\n' +
                 'Comments: ' + String.valueOf(ct.Description) + '<br/>\n<br/>\n' +
                 'For more details on this task, <a href="'+ eups.Base_URL__c+'/'+ct.Id+'">click here.</a>');
             mail.setsaveAsActivity(false);      
            // Send the email you have created. 
            messages.add(mail);
        // End the for and if:    
        } }
        if (messages != null && messages.size() > 0) {
            Messaging.sendEmail(messages);
        }
    }
    
}