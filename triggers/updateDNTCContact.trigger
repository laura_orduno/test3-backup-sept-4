/*
Called on insert/update of Contact.
1) Updates Contact Restricted Fields.

Author - Tom Muzyka
Created - May 2010
*/
trigger updateDNTCContact on Contact (after insert, after update) 
{

    Id accStandardRecId = '01240000000DnnO'; //'012R00000000O0Z';
    Id accDNTCRecId = '01240000000DnnJ'; //'012R00000000O0U';
    Id ctcStandardRecId = '01240000000DnnL'; //'012R00000000OEg';
    Id ctcDNTCRecId = '01240000000DnnK'; //'012R00000000OEl';
    
    Set<String> fieldsTracked = new Set<String> {'AccountId'};
    Set<Id> ctcIds = new Set<Id>();
    
    for (Contact ctc : trigger.new) 
    {
        if (System.Trigger.isInsert) {
            ctcIds.add(ctc.Id);
        }
        if (System.Trigger.isUpdate) {
           if (Util.hasChanges(fieldsTracked, ctc, trigger.oldMap.get(ctc.Id))) {
           ctcIds.add(ctc.Id);
           }
        }
    }
    if (ctcIds.isEmpty()) {
        return;
    }
    
    //update Contacts
    List<Contact> ctcList = [Select Id ,ReportsToId ,Account.RecordTypeId ,RecordTypeId
                               from Contact where Id in :ctcIds];
    for (Contact ctc: ctcList)
    {
        if (ctc.Account.RecordTypeId == accDNTCRecId) {
            ctc.RecordTypeID = ctcDNTCRecId;
            ctc.ReportsToId = null;
        } else {
            ctc.RecordTypeID = ctcStandardRecId;
        }
    }
    update ctcList;
}