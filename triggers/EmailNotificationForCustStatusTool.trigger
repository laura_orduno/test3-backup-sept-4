/*****************************************************
    Trigger Name: EmailNotificationForCustStatusTool
    Usage: This class sends out notification for the AZ Cust status Tool object updates
    Author – Clear Task    
    Date – 12/27/2010        
    Revision History
******************************************************/
trigger EmailNotificationForCustStatusTool on AtoZ_CustStatusTool__c (after insert,after update) {
    List <AtoZ_CustStatusTool__c> custStatusList = trigger.new;
    System.debug('custStatusList=='+custStatusList); 
    System.debug('Custum Setting for Notifications=='+  AtoZ_Notification__c.getValues('Send Notification').Send_Notifications__c); 
    /*
     *  map for CustomStatusTool group by CBUCID for different Color Status
     */
    Map<String, AtoZ_CustStatusTool__c> redCBUCID = new Map<String, AtoZ_CustStatusTool__c> ();
    Map<String, AtoZ_CustStatusTool__c> yellowCBUCID = new Map<String, AtoZ_CustStatusTool__c> ();
    Map<String, AtoZ_CustStatusTool__c> greenCBUCID = new Map<String, AtoZ_CustStatusTool__c> ();
    Set<String> mapModiName=new Set<String>();
    /*
     *  map for Previous Color in CustomStatusTool group by CBUCID for different Color Status
     */
    Map<String, String> previousStatusForGreen=new Map<String, String>();
    Map<String, String> previousStatusForYellow=new Map<String, String>();
    Map<String, String> previousStatusForRed=new Map<String, String>();
    if(AtoZ_Notification__c.getValues('Send Notification').Send_Notifications__c ==true){
        for(AtoZ_CustStatusTool__c  c : custStatusList){
            System.debug('ModifiedById=='+c.LastModifiedById); 
            mapModiName.add(c.LastModifiedById);
            System.debug('NEW STATUS=='+c.Status__c); 
            //In case of CustomStatusTool is created       
            if (trigger.isInsert) {
                if((c.Status__c).equals('Red')){
                    redCBUCID.put(c.cbucid__c, c);
                    previousStatusForRed.put(c.cbucid__c, 'New CustomStatus Tool');
                    System.debug('RED PRIORITY DISTRUBUTION (Insert)');
                    
                } else if((c.Status__c).equals('Yellow')){
                    yellowCBUCID.put(c.cbucid__c, c);
                    previousStatusForYellow.put(c.cbucid__c, 'New CustomStatus Tool');
                    System.debug('YELLOW PRIORITY DISTRUBUTION (Insert)');                
                } else if((c.Status__c).equals('Green')){
                    greenCBUCID.put(c.cbucid__c, c);
                    previousStatusForGreen.put(c.cbucid__c, 'New CustomStatus Tool');
                    System.debug('GREEN PRIORITY DISTRUBUTION (Insert)');                
                }
            }
            //In case of CustomStatusTool is updated 
            if(trigger.isUpdate){
                AtoZ_CustStatusTool__c beforeUpdateCustTool = System.Trigger.oldMap.get(c.Id);
                System.debug('beforeUpdateCustTool - CURRENT STATUS=='+beforeUpdateCustTool.Status__c);
                 if(((c.Status__c).equals('Red') && (beforeUpdateCustTool.Status__c).equals('Red') && ((c.Notes__c!= null && beforeUpdateCustTool.Notes__c != null && !(c.Notes__c).equals(beforeUpdateCustTool.Notes__c))|| (c.Action_Plan__c!= null && beforeUpdateCustTool.Action_Plan__c != null && !(c.Action_Plan__c).equals(beforeUpdateCustTool.Action_Plan__c))))||((c.Status__c).equals('Red') && (beforeUpdateCustTool.Status__c).equals('Yellow')) || ((c.Status__c).equals('Red') && (beforeUpdateCustTool.Status__c).equals('Green')) || ((c.Status__c).equals('Green') && (beforeUpdateCustTool.Status__c).equals('Red'))|| ((c.Status__c).equals('Yellow') && (beforeUpdateCustTool.Status__c).equals('Red'))){
                    redCBUCID.put(c.cbucid__c, c);
                    previousStatusForRed.put(c.cbucid__c, 'Previous Status: '+ beforeUpdateCustTool.Status__c);
                    System.debug('RED PRIORITY DISTRUBUTION (Update)');
                } else if(((c.Status__c).equals('Yellow') && (beforeUpdateCustTool.Status__c).equals('Yellow') && ((c.Notes__c!= null && beforeUpdateCustTool.Notes__c != null && !(c.Notes__c).equals(beforeUpdateCustTool.Notes__c))|| (c.Action_Plan__c!= null && beforeUpdateCustTool.Action_Plan__c != null && !(c.Action_Plan__c).equals(beforeUpdateCustTool.Action_Plan__c))))||((c.Status__c).equals('Yellow') && (beforeUpdateCustTool.Status__c).equals('Green'))||((c.Status__c).equals('Green') && (beforeUpdateCustTool.Status__c).equals('Yellow')) ){
                    yellowCBUCID.put(c.cbucid__c, c);
                    previousStatusForYellow.put(c.cbucid__c,'Previous Status: '+ beforeUpdateCustTool.Status__c);
                    System.debug('YELLOW PRIORITY DISTRUBUTION (Update)');
                } else if((c.Status__c).equals('Green') && (beforeUpdateCustTool.Status__c).equals('Green') && ((c.Notes__c!= null && beforeUpdateCustTool.Notes__c != null && !(c.Notes__c).equals(beforeUpdateCustTool.Notes__c))|| (c.Action_Plan__c!= null && beforeUpdateCustTool.Action_Plan__c != null && !(c.Action_Plan__c).equals(beforeUpdateCustTool.Action_Plan__c)))){
                    greenCBUCID.put(c.cbucid__c, c);
                    previousStatusForGreen.put(c.cbucid__c,'Previous Status: '+ beforeUpdateCustTool.Status__c);
                    System.debug('GREEN PRIORITY DISTRUBUTION (Update)');
                }
            }
        }//end of for loop
        System.debug('GREEN PRIORITY DISTRUBUTION ='+greenCBUCID);                
        System.debug('YELLOW PRIORITY DISTRUBUTION ='+yellowCBUCID);                
        System.debug('RED PRIORITY DISTRUBUTION ='+redCBUCID);
        /*
         *  GREEN Distribution group by CBUCID 
         */
        if(greenCBUCID.size()>0){
            EmailNotificationForCustomStatusTool.emailNotificationForGreenDistribution(greenCBUCID,previousStatusForGreen, mapModiName);
        }
        /*
         *  YELLOW Distribution group by CBUCID 
         */
        if(yellowCBUCID.size()>0){
            EmailNotificationForCustomStatusTool.emailNotificationForYellowOrRedDistribution(yellowCBUCID, previousStatusForYellow,mapModiName,'Yellow');
        }
        /*
         *  RED Distribution group by CBUCID 
         */
        if(redCBUCID.size()>0){
            EmailNotificationForCustomStatusTool.emailNotificationForYellowOrRedDistribution(redCBUCID, previousStatusForRed,mapModiName, 'Red');
        } 
    }                
}