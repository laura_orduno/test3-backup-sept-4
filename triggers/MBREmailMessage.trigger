trigger MBREmailMessage on EmailMessage (after insert, before insert) {
    if(trigger.isAfter && trigger.isInsert){
        if(MBREmailMessageTriggerHelper.firstRun){
			MBREmailMessageTriggerHelper.firstRun = false;
        	MBREmailMessageTriggerHelper.updateCase(trigger.newMap);            
        }
        
        System.debug('MBREmailMessage trigger: VITILcareEmailMessageTriggerHelper.updateCase');
        
    	VITILcareEmailMessageTriggerHelper.updateCase(trigger.new, trigger.newMap);
    }
    if(trigger.isBefore && trigger.isInsert){
        MBREmailMessageTriggerHelper.preventDefault(trigger.new);
    	VITILcareEmailMessageTriggerHelper.preventInvalidEmails(trigger.new);
    }
}