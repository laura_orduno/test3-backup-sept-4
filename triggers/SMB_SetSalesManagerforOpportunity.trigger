trigger SMB_SetSalesManagerforOpportunity on Opportunity (after insert,after update) {
    public Map<String,User> mapAllUser;
    try
    {
        
        list<Opportunity> lstOpp = new list<Opportunity>();
        list<Opportunity> lstSmbOpp = new list<Opportunity>();
        if(trigger.IsUpdate)
        {
            for(Opportunity opp:Trigger.New)
            {
                if(Trigger.OldMap.get(opp.Id).OwnerId != opp.OwnerId)
                    lstOpp.add(opp);
            }
        }
        else if (trigger.IsInsert)
            lstOpp.addAll(Trigger.New);

        if(lstOpp.size() > 0)
        {
            Map<String,String> mapUserOpp=new Map<String,String>();
            Map<String,String> mapOppUser=new Map<String,String>();
            
//            List<RecordType> lstRecordType=[Select id,name from RecordType where sobjectType='Opportunity' and name='SMB Care Opportunity' limit 1];
            set<Id> setRecordTypeIds=SMB_Helper.fetchOpportunitySMBCareRecordTypes('SMB Care Opportunity');
            String recId='';
            if(setRecordTypeIds.size()>0)
            {
                //recId=lstRecordType[0].id;
                  recId=new List<Id>(setRecordTypeIds)[0];
                if(mapAllUser == Null){
                  mapAllUser = new Map<String,User>([Select id,Manager_1__c from user where isactive=true and profile.name like '%Care%' and manager_1__c<>null]);

                }
                Map<String,User> mapUser=new Map<String,User>();
                //for(Opportunity opp:Trigger.New)
                for(Opportunity opp : lstOpp)
                {
                    if(recId.equals(opp.RecordTypeId))
                    {
                        lstSmbOpp.add(opp);
                        mapUserOpp.put(opp.OwnerId,opp.Id);
                        mapOppUser.put(opp.id,opp.OwnerId);
                        if(mapAllUser.containsKey(opp.OwnerId)){
                          mapUser.put(opp.OwnerId,mapAllUser.get(opp.OwnerId));
                        }
                    }
                }
                System.debug('List size->'+mapUserOpp.size());
                
                Map<String,String> mapManagerNameUserId=new Map<String,String>();
                Map<String,String> mapUserIdManagerName=new Map<String,String>();
                for(String manager:mapUser.keyset())
                {
                    String managerName=mapUser.get(manager).Manager_1__c;
                    if(String.isBlank(managerName)==false)
                    {
                        mapManagerNameUserId.put(managerName,mapUser.get(manager).id);
                        mapUserIdManagerName.put(mapUser.get(manager).id,managerName);
                    }
                }
                List<User> lstUser=[Select id,name from User where name in:mapManagerNameUserId.keyset() and isactive=true];
                Map<String,String> mapManagerNameID=new Map<String,String>();
                for(User usr:lstUser)
                {
                    mapManagerNameID.put(usr.name,usr.id);
                }
                List<OpportunityTeamMember> lstOTM=new List<OpportunityTeamMember>();
                for(String str:mapOppUser.keyset() )
                {
                    String ownerId=mapOppUser.get(str);
                    String managerName='';
                    String managerId='';
                    if(String.isblank(ownerId)==false)
                      managerName=mapUserIdManagerName.get(ownerId);
                    if(String.isblank(managerName)==false)
                    managerId=mapManagerNameID.get(managerName);
                    if(String.isblank(managerId)==false)
                    {
                        OpportunityTeamMember OTM=new OpportunityTeamMember(Opportunityid=str,Userid=managerId,TeamMemberRole='Sales Manager');
                        lstOTM.add(OTM);
                    }
                }  
                List<OpportunityTeamMember> lstExistingOTM = new List<OpportunityTeamMember>();
                lstExistingOTM = [Select id from OpportunityTeamMember where Opportunityid in:lstSmbOpp and TeamMemberRole='Sales Manager'];
                if(lstExistingOTM.size() > 0)
                    Database.Delete(lstExistingOTM,false);
                     
                Database.insert(lstOTM,false);
            }
        }
    }
    catch(Exception e)
    {
        System.debug ('### Exception in SMB_SetSalesManagerforOpportunity : ' + e);
    }
}