trigger IC_Solution_ReserveInventoryItem on IC_Solution__c (after insert, after update) {

    System.debug('Trigger.isInsert: ' +Trigger.isInsert +  '  Trigger.isUpdate: ' + Trigger.isUpdate);

    //Only process the trigger the first time. The 2nd time round is because of the provider ID that got updated via the workflow
    if(IC_SolutionTriggerHandler.firstRun){    
        if (Trigger.isInsert) {
            IC_SolutionTriggerHandler handler = new IC_SolutionTriggerHandler();
            if(!handler.handleInsert(Trigger.newMap)){
                if(handler.ErrorMessage != '' ){
                  Trigger.new[0].addError(handler.ErrorMessage);
                }
            }
        }
        if (Trigger.isUpdate) {
            IC_SolutionTriggerHandler handler = new IC_SolutionTriggerHandler();
            if(!handler.handleUpdate(Trigger.newMap, Trigger.oldMap)){
                if(handler.ErrorMessage != ''){
                  Trigger.new[0].addError(handler.ErrorMessage);
                }
            }
        }
        IC_SolutionTriggerHandler.firstRun=false;
    }    
    
}