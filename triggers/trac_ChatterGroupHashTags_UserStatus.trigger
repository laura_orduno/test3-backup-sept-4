trigger trac_ChatterGroupHashTags_UserStatus on User (after update) {
	Map<String,Set<String>> groupToTags = new Map<String,Set<String>>();
	for (Chatter_Hash_Groups__c chg : [SELECT Name, Group_Name__c FROM Chatter_Hash_Groups__c]) {
		if(!groupToTags.containsKey(chg.group_name__c)) {
			groupToTags.put(chg.group_name__c,new Set<String>());
		}
		
		groupToTags.get(chg.group_name__c).add(chg.name);
	}
	Map<String,Id> tagToGroupMap = new Map<String,Id>();
	for(CollaborationGroup cg : [SELECT name FROM CollaborationGroup WHERE name IN :groupToTags.keySet()]) {
		if (groupToTags.containsKey(cg.name)) {
			for(String tag : groupToTags.get(cg.name)) {
				tagToGroupMap.put(tag.toLowerCase(),cg.id);
			}
		}
	}
	
	if (tagToGroupMap.size() > 0) {
		FeedItem[] newFIs = new List<FeedItem>();
		
		for (User u : trigger.new) {
			if(u.CurrentStatus != null && u.CurrentStatus != trigger.oldMap.get(u.id).CurrentStatus) {
				for(String tag : tagToGroupMap.keySet()) {
					if(u.CurrentStatus.toLowerCase().contains('#' + tag)) {
						newFIs.add(new FeedItem(parentId = tagToGroupMap.get(tag), body = u.CurrentStatus));
					}
				}
			}
		}
		
		try {
			Database.SaveResult[] results = Database.insert(newFIs);
		} catch (DMLException de) {
			// group access rights related issue
		}
	}
}