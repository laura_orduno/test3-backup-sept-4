trigger MBRUpdateLastCaseCommentInfo on CaseComment (after insert) {
    /* 
    START DANE REPLACING

    try{
                   
    System.debug('MBRUpdateLastCaseCommentInfo TRIGGER!');
    System.debug('MBRTotal Number of SOQL Queries allowed in this Apex code context: ' +  Limits.getLimitQueries());
        System.debug('MBRTotal Number of records that can be queried  in this Apex code context: ' +  Limits.getLimitDmlRows());
        System.debug('MBRTotal Number of DML statements allowed in this Apex code context: ' +  Limits.getLimitDmlStatements() );
        System.debug('MBRTotal Number of CPU usage time (in ms) allowed in this Apex code context: ' +  Limits.getLimitCpuTime());    
        Set<Id> caseCommentIds = new Set<Id>();
    
        Map<Id, List<CaseComment>> childCaseComments = new Map<Id, List<CaseComment>>();
        
        Set<Id> commentSubmitterUserIds = new Set<Id>();
        List<CaseComment> childComments = null;
        
        for(CaseComment cc : Trigger.new)
        {
            caseCommentIds.add(cc.id);
        
            if(!childCaseComments.containsKey(cc.ParentId))
            {
                childComments = new List<CaseComment>();
            }
            else
            {
                childComments = childCaseComments.get(cc.ParentId);
            }
            
            if(!commentSubmitterUserIds.contains(cc.CreatedById))
            {
                commentSubmitterUserIds.add(cc.CreatedById);
            }
            
            if(cc.isPublished)
            {        
                childComments.add(cc);
                childCaseComments.put(cc.ParentId, childComments);        
            }    
        }
               
        if(Limits.getQueries() >= 100){
            System.debug('MBRUpdateLastCaseCommentInfo: abort since SOQL limmit will be hit.');
            
            throw new QueryException();
        }
        
        Map<Id, User> commentSubmitters = new Map<Id, User>([SELECT id, name, profile.name FROM User WHERE id in :commentSubmitterUserIds]);
    /*   // LEAVE COMMENTED OUT 
         System.debug('MBR1. Number of Queries used in this Apex code so far: ' + Limits.getQueries());
        System.debug('MBR2. Number of rows queried in this Apex code so far: ' + Limits.getDmlRows());
        System.debug('MBR3. Number of DML statements used so far: ' +  Limits.getDmlStatements());    
        System.debug('MBR4. Amount of CPU time (in ms) used so far: ' + Limits.getCpuTime());
        for(Id submitterKey : commentSubmitters.keyset())
        {
            User commentSubmitter = commentSubmitters.get(submitterKey);
            
    //        System.debug('commentSubmitter: ' + commentSubmitter.name + ', ' + commentSubmitter.profile.name);
        }
    */    
        /*
        List<Case> caseUpdateList = new List<Case>();
        for( Case c : [SELECT id, owner.id, owner.name, owner.profile.name, createdById, createdBy.name, createdBy.email, createdBy.profile.name, Last_Case_Comment__c, Last_Case_Comment_Submitter__c FROM Case WHERE id IN :childCaseComments.keyset()])
        {
             System.debug('MBR1. Number of Queries used in this Apex code so far: ' + Limits.getQueries());
        System.debug('MBR2. Number of rows queried in this Apex code so far: ' + Limits.getDmlRows());
        System.debug('MBR3. Number of DML statements used so far: ' +  Limits.getDmlStatements());    
        System.debug('MBR4. Amount of CPU time (in ms) used so far: ' + Limits.getCpuTime());
            String caseCreatorProfileName = (c.createdBy != null && c.createdBy.profile != null) ? c.createdBy.profile.name.toLowerCase() : '';
                    
            //if(caseCreatorProfileName.contains('customer community user'))
           // {
                for( CaseComment cc : childCaseComments.get(c.id) ) 
                {
                    if(!cc.CommentBody.contains(Label.MBRCancelReason) && !cc.CommentBody.contains(Label.MBREmailCommentAuthor))
                    {
                        // comment is not a cancellation, and comment did not come in via e-mail
                        User commentSubmitter = commentSubmitters.get(cc.CreatedById);
                    
                        c.Last_Case_Comment_Submitted_By__c = cc.CreatedById;
                        c.Last_Case_Comment_Submitter__c = commentSubmitter != null ? commentSubmitter.name : 'Unknown';
                        c.Last_Case_Comment__c = cc.CommentBody;
    //System.debug('Last_Case_Comment_Submitted_By__c: ' + c.Last_Case_Comment_Submitted_By__c);        
    //System.debug('Last_Case_Comment_Submitter__c: ' + c.Last_Case_Comment_Submitter__c);        
    //System.debug('Last_Case_Comment__c: ' + c.Last_Case_Comment_Submitter__c);        
                    } else if (cc.CommentBody.contains(Label.MBREmailCommentAuthor)) {
                        String commenterInfo = cc.CommentBody.substringBefore(Label.MBREmailCommentAuthor).trim();
                        if (commenterInfo.contains(' - ')) {
                            // check if submitter is the case creator
                            String submitterEmail = commenterInfo.substringBefore(' - ').toLowerCase();
                            if (submitterEmail == c.CreatedBy.Email.toLowerCase()) {
                                c.Last_Case_Comment_Submitted_By__c = c.CreatedById;
                            }
                            c.Last_Case_Comment_Submitter__c = commenterInfo.substringAfter(' - ');
                            c.Last_Case_Comment__c = cc.CommentBody.substringAfter(Label.MBREmailCommentAuthor);
                        }
                    }
                }
           // }    
            
            caseUpdateList.add(c);
        }
        
        try
        {
            update caseUpdateList;
        }
        catch(DmlException e)
        {
            
        }
       
        if(Limits.getQueries() >= 100){
            System.debug('MBRUpdateLastCaseCommentInfo: abort since SOQL limmit will be hit.');
        }
        else{
            // do we need to send email notifications to external collaborators?
            List<EmailTemplate> emailTemplates = [SELECT Id from EmailTemplate where name = 'MBR case comment collaborator VF' LIMIT 1];
            System.debug('MBR1. Number of Queries used in this Apex code so far: ' + Limits.getQueries());
            System.debug('MBR2. Number of rows queried in this Apex code so far: ' + Limits.getDmlRows());
            System.debug('MBR3. Number of DML statements used so far: ' +  Limits.getDmlStatements());    
            System.debug('MBR4. Amount of CPU time (in ms) used so far: ' + Limits.getCpuTime());
            EmailTemplate tmpl = emailTemplates.size()>0 ? emailTemplates[0] : null;
        
            Id fromEmailId = null;
            List<OrgWideEmailAddress> fromEmails = [select Id from OrgWideEmailAddress where DisplayName = 'My Business Requests' LIMIT 1];
             System.debug('MBR1. Number of Queries used in this Apex code so far: ' + Limits.getQueries());
            System.debug('MBR2. Number of rows queried in this Apex code so far: ' + Limits.getDmlRows());
            System.debug('MBR3. Number of DML statements used so far: ' +  Limits.getDmlStatements());    
            System.debug('MBR4. Amount of CPU time (in ms) used so far: ' + Limits.getCpuTime());
            if (fromEmails.size() > 0) {
                fromEmailId = fromEmails[0].Id;
            }
        
            List<Id> caseIds = new List<Id>();
            for (CaseComment cc : Trigger.new) {
                caseIds.add(cc.ParentId);
            }
            System.debug('caseIds: ' + caseIds);
        
            Map<Id, Case> cases = new Map<Id, Case>([SELECT Id, NotifyCollaboratorString__c, Origin, Owner.Id, CreatedBy.Id, CreatedBy.Email, Last_Case_Comment_Submitted_By__c, Last_Case_Comment_Submitted_By__r.Id FROM Case WHERE Id IN :caseIds]);
             System.debug('MBR1. Number of Queries used in this Apex code so far: ' + Limits.getQueries());
            System.debug('MBR2. Number of rows queried in this Apex code so far: ' + Limits.getDmlRows());
            System.debug('MBR3. Number of DML statements used so far: ' +  Limits.getDmlStatements());    
            System.debug('MBR4. Amount of CPU time (in ms) used so far: ' + Limits.getCpuTime());
            
        
            for (CaseComment cc : Trigger.new) {
                System.debug('cc.ParentId: ' + cc.ParentId);
                Case parentCase = cases.get(cc.ParentId);
                System.debug('parentCase: ' + parentCase);
                if (parentCase.Origin == 'My Business Requests' && tmpl != null) {
                    System.debug('cc.CreatedById: ' + cc.CreatedById);
                    //if (cc.CreatedById == parentCase.Owner.Id || cc.CreatedById == parentCase.CreatedBy.Id || cc.CommentBody.startsWith(parentCase.CreatedBy.Email)) {
                    if ( (parentCase.Last_Case_Comment_Submitted_By__c != null && parentCase.Last_Case_Comment_Submitted_By__r.Id == cc.CreatedById) 
                        || (cc.CommentBody.startsWith(parentCase.CreatedBy.Email))
                    ) {
                        // case comment was not created by a collaborator; send out collaborator notifications!
                        if (parentCase.NotifyCollaboratorString__c != null && parentCase.NotifyCollaboratorString__c.length() > 0) {
                            List<String> collabEmails = parentCase.NotifyCollaboratorString__c.split(';', 0);
                            if (collabEmails.size() > 0) {
                                try {
                                    List<Messaging.SendEmailResult> mailResults = MBRUtils.sendTemplatedCaseEmails(collabEmails, fromEmailId, tmpl.Id, parentCase.Id);
                                } catch (Exception e) {
                                    System.debug('Trigger MBRCaseCommentNotifyCollaborator caught MBRUtls.sendTemplatedCaseEmails() exception: ' + e);
                                }
                            }
                        }
                    }
                }
            }
            
            
        //System.debug('MBRUpdateLastCaseCommentInfo, caseCommentIds' + caseCommentIds);    
            
            // VITILcare
            /////////////
            //VITILcareCaseCommentTriggerHelper.processCaseComments(caseCommentIds);
            //ENTPCaseCommentTriggerHelper.processCaseComments(caseCommentIds);
        }  
    }    
    catch(QueryException e){
        
    }   
     */ 
}