trigger OCOM_CARParallelWorkflow on Credit_Assessment__c(before update) {
    system.debug('OCOM_CARParallelWorkflow Trigger.isBefore('+Trigger.isBefore+') Trigger.isUpdate('+Trigger.isUpdate+') Trigger.isInsert('+Trigger.isInsert+')');
    if(Trigger.isBefore) {
        if(Trigger.isUpdate) {
            try {
                system.debug('OCOM_CARParallelWorkflow ActivateCARUpdateToNC('+OCOM_CARParallelWorkflowHelper.ActivateCARUpdateToNC+')');
                if (OCOM_CARParallelWorkflowHelper.ActivateCARUpdateToNC == true) return;
                Set<String> CARFromStates = new Set<String>();
                Set<String> CARToStates = new Set<String>();
                Set<String> CARResult = new Set<String>();
                Set<String> OrderStatus = new Set<String>();
                // Fetch Happy path CAR status from custom setting; custom setting provides the CAR states to send an AOO order to NC
                // Allows the specification of CAR status to be adminstrated by SFDC Admins
                Map<string,OCOM_CAR_Status__c> ocomCARStatusMapping = new Map<string,OCOM_CAR_Status__c>();
                ocomCARStatusMapping = OCOM_CAR_Status__c.getAll();
                for(OCOM_CAR_Status__c Obj : ocomCARStatusMapping.values()) {
                    if(Obj.Category__c.equalsIgnoreCase('From')) {
                        CARFromStates.add(Obj.name);
                    }
                    if(Obj.Category__c == 'To') {
                        CARToStates.add(Obj.name);
                    }
                    if(Obj.Category__c == 'Result') {
                        CARResult.add(Obj.name);
                    }
                }
                Map<string,OCOM_CAR_Order_Status__c> ocomCAROrderStatusMapping = new Map<string,OCOM_CAR_Order_Status__c>();
                ocomCAROrderStatusMapping = OCOM_CAR_Order_Status__c.getAll();
                for(OCOM_CAR_Order_Status__c orderStatusObj : ocomCAROrderStatusMapping.values()) {
                    OrderStatus.add(orderStatusObj.name);
                }
                system.debug('OCOM_CARParallelWorkflow CARFromStates('+CARFromStates+')');
                system.debug('OCOM_CARParallelWorkflow CARToStates('+CARToStates+')');
                system.debug('OCOM_CARParallelWorkflow CARResult('+CARResult+')');
                system.debug('OCOM_CARParallelWorkflow OrderStatus('+OrderStatus+')');
                
                Set<String> allMatchedCars = new Set<String>();
                List<Order> relatedOrdersToUpdate = new List<Order>();
                
                // Validate if the previous CAR state is In-progress or Amend and the new CAR state is one of the 'to' states
                for (Credit_Assessment__c ObjCAR: Trigger.new) {
                    system.debug('OCOM_CARParallelWorkflow ObjCAR('+ObjCAR+')');
                    if((CARFromStates.contains(Trigger.oldMap.get(ObjCAR.Id).CAR_Status__c)) && (CARToStates.contains(ObjCAR.CAR_Status__c)) && (CARResult.contains(ObjCAR.CAR_Result__c))) {
                        // Create a set of all CAR records which match the 'from/to' state transitions
                        allMatchedCars.add(ObjCAR.id);
                        
                        // Store the previous and new CAR state, etc. in prep to communicate to the back office
                        SMB_Helper.CAR_STATUS_Container objWrapperCAR = new SMB_Helper.CAR_STATUS_Container();
                        
                        objWrapperCAR.OLD_CAR_Status = Trigger.oldMap.get(ObjCAR.Id).CAR_Status__c;
                        objWrapperCAR.OLD_Car_result = Trigger.oldMap.get(ObjCAR.Id).CAR_Result__c;
                        objWrapperCAR.OLD_Car_reason_code = Trigger.oldMap.get(ObjCAR.Id).CAR_Result_Reason__c;
                        
                        objWrapperCAR.NEW_CAR_Status = ObjCAR.CAR_Status__c;
                        objWrapperCAR.NEW_CAR_Result = ObjCAR.CAR_Result__c;
                        objWrapperCAR.NEW_CAR_Reason_code = ObjCAR.CAR_Result_Reason__c;
                        system.debug('OCOM_CARParallelWorkflow objWrapperCAR('+objWrapperCAR+')');
                        
                        // populate CAR_Status_MAP with the from and to information to be sent to the BO in the notes field.            
                        OCOM_CARParallelWorkflowHelper.CARStatusMap.put(ObjCAR.id, objWrapperCAR);
                    }
                }
                // if we have impacted CAR records
                system.debug('OCOM_CARParallelWorkflow allMatchedCars('+allMatchedCars+')');
                if (allMatchedCars.size()>0) {
                    // Fetch all related orders associated to the CAR.  Note Credit_Assessment__c is a lookup on Order.
                    for(List<Order> objOrderList:[Select Accountid, account.name, OrderNumber, Credit_Assessment__r.name,id, Credit_Assessment__c, Credit_Assessment_Status__c,
                                                Related_Order__c, Related_Order__r.Related_Order__c, Related_Order__r.Related_Order__r.Id,
                                                Related_Order__r.createdDate, createddate, Status
                                                from Order
                                                where 
                                                Credit_Assessment__c in:allMatchedCars 
                                                and  Status in: OrderStatus
                                                order by createddate]) {
                        system.debug('OCOM_CARParallelWorkflow objOrderList size('+objOrderList.size()+')');
                        for (Order ObjOrder: objOrderList) {
                            system.debug('OCOM_CARParallelWorkflow ObjOrder('+ObjOrder+')');
                            if (ObjOrder.Related_Order__c == null) {
                                system.debug('OCOM_CARParallelWorkflow Related_Order__c('+ObjOrder.Related_Order__c+')');
                                relatedOrdersToUpdate.add(ObjOrder);
                            } else if(ObjOrder.Related_Order__c != null && ObjOrder.Related_Order__r.Related_Order__c != null) {
                                if (ObjOrder.Id == ObjOrder.Related_Order__r.Related_Order__r.Id) {
                                    if(ObjOrder.Related_Order__r.createdDate > ObjOrder.createdDate) {
                                        relatedOrdersToUpdate.add(ObjOrder);
                                    }
                                }
                            }
                        }
                    }
                    system.debug('OCOM_CARParallelWorkflow relatedOrdersToUpdate('+relatedOrdersToUpdate+')');
                    if (relatedOrdersToUpdate.size()>0) {
                        OCOM_CARParallelWorkflowHelper.ActivateCARUpdateToNC = true;
                        // Invoke helper method that will send CAR status updates to NC using order amend
                        for(Order anOrder: relatedOrdersToUpdate) {
                            system.debug('OCOM_CARParallelWorkflow OrdrUtilities.submitAmendedOrder('+anOrder.Id+')');
                            OrdrUtilities.submitAmendedOrder(anOrder.Id);
                        }
                    }
                }
            } catch(Exception e) {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.error,e.getMessage());
            }
        }
    }
}