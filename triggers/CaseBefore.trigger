trigger CaseBefore on Case (before insert, before update) {
    Map<Id, List<Id>> groupIdToCaseIds = new Map<Id, List<Id>> ();
    for( Case theCase : trigger.new ) {
        String ownerId = theCase.OwnerId;
        if( (trigger.isInsert || trigger.oldMap.get(theCase.Id).ownerId != theCase.ownerId ) && ownerId.startsWith('00G'))
        {
            if( !groupIdToCaseIds.containsKey(ownerId) )
                groupIdToCaseIds.put( theCase.ownerId, new List<Id> { theCase.Id } );
            else groupIdToCaseIds.get( theCase.ownerId ).add( theCase.Id );            
            system.debug('groupIdToCaseIds: '+groupIdToCaseIds);
        }
        
    }
    List<Group> groups = [SELECT Id, Name FROM Group WHERE Id IN :groupIdToCaseIds.keySet() ];
    for(Group theGroup : groups ) {
        List<Id> caseIds = groupIdToCaseIds.get(theGroup.Id);
        for(Id caseId : caseIds )
            trigger.newMap.get(caseId).Queue_Name__c = theGroup.Name;
    }
}