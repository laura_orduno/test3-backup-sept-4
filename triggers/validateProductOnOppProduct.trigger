/******************************************************************************************/
/* Called on update of Opportunity Product Item                                           */
/* Ensures Product Item is Active, if not raise error.                                    */
/*                                                                                        */
/* Author - Eroca Ho                                                                      */
/* Created - Sept 2009                                                                    */
/*                                                                                        */
/* Updated - Tom Muzyka                                                                   */
/* - If Update performed by Dataload or Admin Team, bypass trigger                        */
/* Update Date - May 2010  ver 2.0                                                        */
/******************************************************************************************/

trigger validateProductOnOppProduct on Opp_Product_Item__c (after insert, after update) {

    /* Start Traction Modifications */
    Map<String, validateProductOnOppSettings__c> mcs = validateProductOnOppSettings__c.getAll();
    Set<String> excludedRecordTypes = new Set<String>();
    Set<String> excludedProfileIds = new Set<String>();
    Set<String> excludedProfileNames = new Set<String>();
    
    if (mcs != null && mcs.size() > 0) {
        for (validateProductOnOppSettings__c vpoos : mcs.values()) {
            if (vpoos.Exclude_Record_Type_ID__c != null) {
                excludedRecordTypes.add((Id)vpoos.Exclude_Record_Type_Id__c);
            }
            if (vpoos.Exclude_Profile_ID__c != null) {
                excludedProfileIds.add((Id)vpoos.Exclude_Profile_Id__c);
            }
            if (vpoos.Exclude_Profile_Name__c != null) {
                excludedProfileNames.add(vpoos.Exclude_Profile_Name__c);
            }
        }
    }
    /* End Traction Modifications */
    
    
    
    List<Opp_Product_Item__c> pliList = [Select Id, name, Opportunity__c, Opportunity__r.RecordTypeId, product__c, product__r.IsActive, Product__r.Product_Family__c, LastModifiedBy.ProfileId, LastModifiedBy.Profile.Name from Opp_Product_Item__c where Id In :trigger.newMap.keySet()];
    for (Opp_Product_Item__c pli : pliList) {
        if (excludedRecordTypes.contains(pli.Opportunity__r.RecordTypeId)) { continue; }
        if (excludedProfileIds.contains(pli.LastModifiedBy.ProfileId) || excludedProfileNames.contains(pli.LastModifiedBy.Profile.Name)) {
            continue;
        }
        if (! pli.Product__r.isActive) {
            Trigger.newMap.get(pli.Id).addError('<BR>' + pli.Product__r.Product_Family__c + System.Label.Opp_Product_Inactive);
            break;
        }
    }
    
    S2C_OpportunityProductLineItemHelper.updateSRContractLength(Trigger.new, Trigger.old);
}