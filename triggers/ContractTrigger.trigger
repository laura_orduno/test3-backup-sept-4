/**
 * Single trigger that will handle all scenarios by using handler class.  Please do not
 * build another trigger within same object to optimize SOQL and code reusability.
 */
trigger ContractTrigger on Contract (before insert,before update,after insert, after update) {
    try{
        if(label.Apttus_Decommissions_Trigger_Switch.equalsignorecase('on')){
            ContractTriggerHandler handler=new ContractTriggerHandler();
        }
    }catch(exception e){
        if(!Test.isRunningTest()){
            Util.emailSystemError(e);
        }
        
    }
}