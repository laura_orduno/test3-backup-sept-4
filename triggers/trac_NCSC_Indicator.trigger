trigger trac_NCSC_Indicator on AtoZ_Assignment__c (before insert, before update) {
    for (AtoZ_Assignment__c a : trigger.new) {
        a.Is_NCSC__c = (a.AtoZ_Field__c != null && a.AtoZ_Field__c.equalsIgnoreCase('SvcSpec_NCSC'));
    }
}