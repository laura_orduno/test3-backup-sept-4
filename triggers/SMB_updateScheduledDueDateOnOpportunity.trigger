/*
###########################################################################
# File..................: SMB_updateScheduledDueDateOnOpportunity
# Version...............: 29
# Created by............: Kanishk Prasad
# Created Date..........: 18/05/2014
# Last Modified by......: 
# Last Modified Date....: 
# Description...........: This trigger will populate the latest work order's scheduled due date onto its related Opportunity
#                         Created as part of Defect #33408
# Copyright (c) 2000-2012. Astadia, Inc. All Rights Reserved.
#
# Created by the Astadia, Inc. Modification must retain the above copyright notice. 
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any commercial purpose, without fee, and without a written
# agreement from Astadia, Inc., is hereby forbidden. Any modification to source
# code, must include this paragraph and copyright.
#
# Permission is not granted to anyone to use this software for commercial uses.
#
# Contact address: 2839 Paces Ferry Road, Suite 350, Atlanta, GA 30339
# Company URL : http://www.astadia.com
###########################################################################
*/




trigger SMB_updateScheduledDueDateOnOpportunity on Work_Order__c (after insert,after update) {

    if(Trigger.isInsert)
    {
        Map<Id,Opportunity> mapOpportunity=new Map<Id,Opportunity>();
        for(Work_Order__c workOrder : Trigger.New)
        {
             System.debug('Opp =' +workOrder.Opportunity__c );
        if(!(String.isBlank(Trigger.new[0].Opportunity__c))){
            if('Fieldwork'.equalsIgnoreCase(workOrder.Install_Type__c) && 'Reserved'.equalsIgnoreCase(workOrder.status__c))
               mapOpportunity.put(workOrder.Opportunity__c,new Opportunity(id=workOrder.Opportunity__c,Soonest_Fieldwork_Due_Date__c=workOrder.Scheduled_Datetime__c));
            else if('Fieldwork'.equalsIgnoreCase(workOrder.Install_Type__c))
                mapOpportunity.put(workOrder.Opportunity__c,new Opportunity(id=workOrder.Opportunity__c,Soonest_Fieldwork_Due_Date__c=null));        
        }
        }
        if((mapOpportunity.values()).size()>0)
             update mapOpportunity.values();
    }
    else if(Trigger.IsUpdate)
    {
    if(!(String.isBlank(Trigger.new[0].Opportunity__c))){
        Set<Id> setOpportunityIds=new Set<Id>();
        for(Work_Order__c workOrder : Trigger.New)
        {
            setOpportunityIds.add(workOrder.Opportunity__c);
        }
        List<Work_Order__c> lstWorkOrders=[Select id,Status__c,Install_Type__c,Opportunity__c,Scheduled_Datetime__c from Work_Order__c where opportunity__c in :setOpportunityIds order by createddate desc ];
        Map<Id, Opportunity> mapOpportunity=new Map<Id, Opportunity> ();
        for(Work_Order__c workOrder : lstWorkOrders)
        {
            if(mapOpportunity.containsKey(workOrder.opportunity__c)==false)
            {
                if('Fieldwork'.equalsIgnoreCase(workOrder.Install_Type__c) && 'Reserved'.equalsIgnoreCase(workOrder.status__c))
                   mapOpportunity.put(workOrder.Opportunity__c,new Opportunity(id=workOrder.Opportunity__c,Soonest_Fieldwork_Due_Date__c=workOrder.Scheduled_Datetime__c));
                else if('Fieldwork'.equalsIgnoreCase(workOrder.Install_Type__c))
                    mapOpportunity.put(workOrder.Opportunity__c,new Opportunity(id=workOrder.Opportunity__c,Soonest_Fieldwork_Due_Date__c=null));        
            }   
        }       
            update mapOpportunity.values();
            }
    }   
}