/**
 *	MBRCaseCommentNotifyCollaborator.trigger
 *
 *  @author		Alex Kong (akong@tractionondemand.com)
 *  @date		2015-03-04
 **/

trigger MBRCaseCommentNotifyCollaborator on CaseComment (after insert) {

	// logic moved to end of MBRUpdateLastCaseCommentInfo.trigger

}