trigger smb_CaseBefore on Case (before insert, before update) {
    final static string LEGAL_NAME_CORRECTION_CASE_RECORD_TYPE='Legal Name Correction';
    final static String SMB_CARE_ENTITLEMENT_ID = [SELECT Metadata_Value__c FROM Apex_Org_Default__mdt WHERE DeveloperName = 'SMBCareEntitlementId' LIMIT 1].Metadata_Value__c;
    final static String LEGAL_NAME_QUEUE_ID = [SELECT Metadata_Value__c FROM Apex_Org_Default__mdt WHERE DeveloperName = 'SMBDefaultLegalNameCorrectionQueueId' LIMIT 1].Metadata_Value__c;

    try{
    //system.debug('##### ENTERING smb_CaseBefore Trigger');
        
        Map<Id, List<Id>> groupIdToCaseIds = new Map<Id, List<Id>> ();
        Map<Id,String> mapOfOwnerEmail = new Map<Id,String>();
        for( Case theCase : trigger.new ) {
            
            //added by Scott Fawcett
            String ownerId2 = theCase.OwnerId;
            
            if (Trigger.isInsert && ownerId2.startsWith('005'))
                theCase.Current_Owner__c = theCase.OwnerId; 
            else if (Trigger.isUpdate) {            
                if (ownerId2.startsWith('005'))
                    theCase.Current_Owner__c = theCase.OwnerId;
                else if (ownerId2.startsWith('00G'))
                    theCase.Current_Owner__c = null;
                String prevOwnerId = Trigger.oldMap.get(theCase.Id).OwnerId;
                if (Trigger.oldMap.get(theCase.Id).OwnerId != theCase.OwnerId && prevOwnerId.startsWith('005'))
                    theCase.Previous_Owner__c = Trigger.oldMap.get(theCase.Id).OwnerId;
            }   
            
            //end scott add
            
            String ownerId = theCase.OwnerId;
            if( (trigger.isInsert || trigger.oldMap.get(theCase.Id).ownerId != theCase.ownerId ) && ownerId.startsWith('00G'))
            {
                if( !groupIdToCaseIds.containsKey(ownerId) )
                    groupIdToCaseIds.put( theCase.ownerId, new List<Id> { theCase.Id } );
                else groupIdToCaseIds.get( theCase.ownerId ).add( theCase.Id );
            }
        }
        
        if(Limits.getQueries() >= 100){
            System.debug('smb_CaseBefore: abort since SOQL limmit will be hit.');
            
            throw new QueryException();
        }
        
        List<Group> groups = [SELECT Id, Name FROM Group WHERE Id IN :groupIdToCaseIds.keySet() ];
        for(Group theGroup : groups ) {
            List<Id> caseIds = groupIdToCaseIds.get(theGroup.Id);
            for(Id caseId : caseIds ){
                if(caseId != null)
                    trigger.newMap.get(caseId).Queue_Name__c = theGroup.Name;
            }
        }
        
        // added for SMB Evolution - 2/26/2013 by Scott Fawcett
         if(Limits.getQueries() >= 100){
            System.debug('smb_CaseBefore: abort since SOQL limmit will be hit.');
            
            throw new QueryException();
        }
        Set<Id> SMBCaseRTs = new Set<Id>();
        Set<Id> ownerIds = new Set<Id>();
        Map<Id, String> OwnerProfileMap = new Map<Id, String>();
        Map<Id, String> OwnerEscalationProfileMap = new Map<Id, String>();
        Id legalNameCorrectionQueue=LEGAL_NAME_QUEUE_ID;

        
        for (schema.recordtypeinfo r : case.sobjecttype.getdescribe().getrecordtypeinfosbyid().values()) {
            // add every found RT except "SMBEV_General_Inquiry"
            if (r.getname() != 'SMB Care General Inquiry')
                SMBCaseRTs.add(r.getrecordtypeid());
        }
        
        for (Case c1 : Trigger.new) {
            if (!ownerIds.contains(c1.OwnerId))
                ownerIds.add(c1.OwnerId);
        }
    
        if (!ownerIds.isEmpty()) {
            List<User> userList = [SELECT Care_Type__c, ProfileId, Profile.Name, state,ManagerId, Manager.Email, Channel_Callidus__c  FROM User WHERE Id IN :ownerIds];
            
            for (User u : userList) {
                OwnerProfileMap.put(u.Id, u.Profile.Name);          
                mapOfOwnerEmail.put(u.Id, u.Manager.Email);
                if (string.isNotBlank(u.Care_Type__c))
                {
                    if (u.Care_Type__c.containsIgnoreCase('escalation'))
                        OwnerEscalationProfileMap.put(u.Id, u.Care_Type__c);
                }
            }
            
            if(Limits.getQueries() >= 100){
                System.debug('smb_CaseBefore: abort since SOQL limmit will be hit.');
                
                throw new QueryException();
            }
            
            List<Group> groupList = [SELECT Id, Name FROM Group WHERE Id IN : ownerIds AND Type = 'Queue'];
            
            for (Group g : groupList) {
                OwnerProfileMap.put(g.Id, g.Name);
            }
            
            /*
            Purpose/Description: SIS-937. Auto-Population Sales Compensation Inquiry records with the Submitter Department and Region from their User Detail
            Update by Author: Rajan Tatuskar
            Updated Date: 11-Aug-2015
            Business: BT Sales Compensation Team
            */
            
            if (Trigger.isInsert) {
                Map<Id, User> ownerMap = new Map<Id, User>();
                if (ownerIds.size() > 0)
                {
                    for (User userObj : userList)
                    {
                        ownerMap.put(userObj.Id, userObj);
                    }
                    
                    for (Case updatedCase : Trigger.new)
                    {
                        if(ownerMap.containsKey(updatedCase.OwnerId)){
                            User currentUser = ownerMap.get(updatedCase.OwnerId);
                        
                            if(!String.isBlank(updatedCase.RecTypeName__c) && 
                               (updatedCase.RecTypeName__c.equalsIgnoreCase('Sales Compensation Inquiries') || 
                                updatedCase.RecTypeName__c.equalsIgnoreCase('Sales Compensation Inquiries (Partner Solutions)')) &&
                               (currentuser.Profile.name !=  'Sales Compensation' &&
                                currentuser.Profile.name != 'System Administrator' &&
                                currentuser.Profile.name != 'System Admin TEAM' &&
                                currentuser.Profile.name != 'System Admin TEAM no PW expiry' &&
                                currentuser.Profile.name != 'System Integration' &&
                                currentuser.Profile.name != 'System Vendor' &&
                                currentuser.Profile.name != 'Cloud Enablement Team')){
                                
                               //system.debug('>>>Current user profile'+currentuser.Profile.name);
                               //system.debug('>>>Case Dept'+ updatedCase.Dept__c);
                               //system.debug('>>>Case Region'+updatedCase.Region__c);
                               
                                /* Added by Adhir 
                                    User story: the Dept. & Region Accessibility (SIS1090) start */
                                
                                if(!String.isBlank(updatedCase.Dept__c) || !String.isBlank(updatedCase.Region__c))
                                   {
                                       updatedCase.addError(Label.Dept_and_region_blank);
                                       //system.debug('>>> Inside 1 Validation'); 
                                   }
                                
                                // User story: the Dept. & Region Accessibility (SIS1090) end 
                                
                                if(!String.isBlank(currentUser.Channel_Callidus__c))
                                    updatedCase.Dept__c = currentUser.Channel_Callidus__c;
                                   
                                if(!String.isBlank(currentUser.state))
                                    updatedCase.Region__c = currentUser.state;
                                
                                
                                
                            }   
                        }
                        
                    }
                }
            }
          // END of SIS-937 US.
        }
    
        // todo roll everything into class files
        smb_CaseBeforeClass.establishStartEndTimes(Trigger.new, Trigger.isUpdate, Trigger.isInsert, Trigger.oldMap);
        smb_CaseBeforeClass.syncParentCaseOwnerField(Trigger.new, Trigger.isUpdate, Trigger.isInsert, Trigger.oldMap);
        
        Set<String> exclusionOwnersForEscalations = new Set<String>();
        exclusionOwnersForEscalations.add('SMB Care Escalations'); // queue
        //exclusionOwnersForEscalations.add('SMB Care Escalation Manager'); // profile
        //exclusionOwnersForEscalations.add('SMB Care Escalation Agent'); // profile
        map<id,schema.Recordtypeinfo> recordTypeInfoMap=case.sobjecttype.getdescribe().getrecordtypeinfosbyid();
        list<account> accountsWithUpdatedLegalName=new list<account>();
        if (Trigger.isInsert) {
            for (Case c: Trigger.new) { 
              if (SMB_CARE_ENTITLEMENT_ID != null) { 
                
                    //stamp Case with the Owners User Profile or Queue Name
                    if (OwnerProfileMap.containsKey(c.OwnerId))
                        c.Case_Owner_Profile_Name__c = OwnerProfileMap.get(c.OwnerId);
                        
                    // for SMB Evolution Record Types, stamp the Entitlement Id on the Case
                    if (SMBCaseRTs.contains(c.RecordTypeId)) 
                        c.EntitlementId = SMB_CARE_ENTITLEMENT_ID;
                    
                    //if (exclusionOwnersForEscalations.contains(c.Case_Owner_Profile_Name__c))
                    //  c.Escalated_Case__c = true; 
              }
                //system.debug('Result = '+recordTypeInfoMap.containskey(c.recordtypeid)+''+recordTypeInfoMap.get(c.recordtypeid).getname().equalsignorecase(LEGAL_NAME_CORRECTION_CASE_RECORD_TYPE));
              if(recordTypeInfoMap.containskey(c.recordtypeid)&&recordTypeInfoMap.get(c.recordtypeid).getname().equalsignorecase(LEGAL_NAME_CORRECTION_CASE_RECORD_TYPE)){
                if(string.isblank(c.subject)){
                  c.subject='Legal Name Correction';
                }
                    // Commented as part of TBO project, as LNU will follow TBO queue assignment this is no longer required. - Adhir[18-05-2016]
                    if(legalNameCorrectionQueue!=null){
                        c.ownerid=legalNameCorrectionQueue;
                    }
              }
          }
        }
        if (Trigger.isUpdate) {
        
            // this portion of the code stamps updates to milestones 
            Set<Id> firstResponseIds = new Set<Id>(); // related to SMB Care First Response Time
            Set<Id> resolutionIds = new Set<Id>(); // related to SMB Care Resolution Time
            //Set<Id> escalationIds = new Set<Id>(); // related to SMB Care Escalations
            Set<Id> totalCaseIds = new Set<Id>();
            
            for (Case c2: Trigger.new) {
                    
                // stamp Case with the Owners User Profile or Queue Name
                if (OwnerProfileMap.containsKey(c2.OwnerId))
                    c2.Case_Owner_Profile_Name__c = OwnerProfileMap.get(c2.OwnerId);
                
             /*if(recordTypeInfoMap.containskey(c2.recordtypeid)&&recordTypeInfoMap.get(c2.recordtypeid).getname().equalsignorecase(LEGAL_NAME_CORRECTION_CASE_RECORD_TYPE)){
                    if(legalNameCorrectionQueue!=null){
                        c2.ownerid=legalNameCorrectionQueue.id;
                    }
                }*/
                // set escalation flag for ownership change and if it falls within the escalation "owners"
                //if (c2.OwnerId != Trigger.oldMap.get(c2.Id).OwnerId && exclusionOwnersForEscalations.contains(c2.Case_Owner_Profile_Name__c))
                //  c2.Escalated_Case__c = true;
         
                // for SMB Care First Response Time
                if (c2.Status == 'In Progress' && Trigger.oldMap.get(c2.Id).Status != 'In Progress' && SMBCaseRTs.contains(c2.RecordTypeId)) {
                    firstResponseIds.add(c2.Id);
                    if (!totalCaseIds.contains(c2.Id))
                        totalCaseIds.add(c2.Id);
                }
                
    
                
                                        /*
                // for SMB Care Escalations 
                                        /*if (c2.Escalated_Case__c == true && (!exclusionOwnersForEscalations.contains(c2.Case_Owner_Profile_Name__c) || (!OwnerEscalationProfileMap.isEmpty() && !OwnerEscalationProfileMap.containsKey(c2.OwnerId)))) {
                    //escalationIds.add(c2.Id);
                    //if (!totalCaseIds.contains(c2.Id))
                    //  totalCaseIds.add(c2.Id);
                        
                    // need to also stamp Escalation_End__c field 
                    if (c2.Escalated_Case__c && c2.Escalation_End__c == null)
                        c2.Escalation_End__c = System.now();
                }*/
    
                // for SMB Care Resolution Time
                if ((c2.Status == 'Closed' || c2.Status == 'Cancelled') && !c2.IsClosed && SMBCaseRTs.contains(c2.RecordTypeId)) {
                    resolutionIds.add(c2.Id);
                    if (!totalCaseIds.contains(c2.Id))
                        totalCaseIds.add(c2.Id);
                    
                    // also need to tie out the first response if it wasn't previously done
                    if (!firstResponseIds.contains(c2.Id))
                        firstResponseIds.add(c2.Id);
                        
                    // also need to tie out the escalation if it wasn't previously done
                    //if (!escalationIds.contains(c2.Id))
                    //  escalationIds.add(c2.Id);
                        
                    // need to also stamp Escalation_End__c field if it isn't already 
                    if (c2.Escalated_Case__c && c2.Escalation_End__c == null)
                        c2.Escalation_End__c = System.now();
                        
                }
                
                if (!totalCaseIds.isEmpty()) {
                    List<CaseMilestone> CMs = [SELECT CompletionDate, Id, CaseId, MilestoneType.Name FROM CaseMilestone 
                                                WHERE CaseId IN : totalCaseIds AND CompletionDate = null AND
                                                (MilestoneType.Name = 'SMB Care First Response Time' OR 
                                                MilestoneType.Name = 'SMB Care Resolution Time')];
                                                //MilestoneType.Name = 'SMB Care Escalations')];
                        
                    if (!CMs.isEmpty()) {
                        for (CaseMilestone cm : CMs) {
                            if (firstResponseIds.contains(cm.CaseId) && cm.MilestoneType.Name == 'SMB Care First Response Time')
                                cm.CompletionDate = System.now();
                            if (resolutionIds.contains(cm.CaseId) && cm.MilestoneType.Name == 'SMB Care Resolution Time')
                                cm.CompletionDate = System.now();
                            //if (escalationIds.contains(cm.CaseId) && cm.MilestoneType.Name == 'SMB Care Escalations')
                            //  cm.CompletionDate = System.now();
                        }
                        
                        update CMs; 
                    }
                }
            }
        }
        //system.debug('##### EXITING smb_CaseBefore Trigger');
        
        /*******
        ********    Added by Rajan - This section is for TBO user story
        ********/
        //final static string TBO_RECORD_TYPE_NAME = 'TBO_REQUEST';
        //RecordType TBORecordType = [SELECT Id, Name, Developername from RecordType where Developername = 'TBO_REQUEST' limit 1];
        if (trigger.isInsert || Trigger.isUpdate) {
            Set<Id> AccountIds = new Set<Id>(); 
            Set<Id> ContactIds = new Set<Id>();
            
            for (Case caseObj : Trigger.new) {
                ////system.debug('-----------caseObj-----' + caseObj);
                //system.debug('-----------caseObj.recordtype Name-----' + recordTypeInfoMap.get(caseObj.recordtypeid).getname());
                //system.debug('-----------caseObj.AccountId-----' + caseObj.AccountId);
                //system.debug('-----------caseObj.RecordType.developername-----' + caseObj.RecordType.developername);
                /***for US#23 ***/
                /*we need to update useremail with owners manager's email address*/
                /*US-16-LNC, this warranty defect(User Story 16 LNC) updated by Umesh (29June2016)
                added record type condition in below statement */
                if( recordTypeInfoMap.containskey(caseObj.recordtypeid) && (recordTypeInfoMap.get(caseObj.recordtypeid).getname().equalsignorecase('TBO REQUEST') || recordTypeInfoMap.get(caseObj.recordtypeid).getname().equalsignorecase('Legal Name Update')) && mapOfOwnerEmail.containsKey(caseObj.OwnerId)) {
                    caseObj.User_Email__c = mapOfOwnerEmail.get(caseObj.OwnerId);
                     //system.debug('------caseObj.User_Email__c----' + caseObj.User_Email__c);
                }
                
                /*** 25-Feb-2016
                Validation added for UAT Defect : TBO_US7_TC01 - Mandatory fields - At least the Outgoing Customer account Name or the Incoming Customer Account name and a Created Date need to populated to save as Draft. Created Date field should be auto-populated with today's date
                ***/
                /*
                if(caseObj.recordtypeid == TBORecordType.id && caseObj.Status.equalsignorecase('Draft') && CaseObj.AccountId == null && caseObj.Master_Account_Name__c == null){
                  
                        caseObj.addError(Label.TBO_OutgoingORIncomingCustomerMandatory);
                
                }
                
                if(recordTypeInfoMap.containskey(caseObj.recordtypeid) && recordTypeInfoMap.get(caseObj.recordtypeid).getname().equalsignorecase('TBO REQUEST') && !caseObj.Status.equalsignorecase('Draft') && !caseObj.Status.equalsignorecase('New') && (caseObj.Master_Account_Name__c == null || caseObj.Contact_phone_number__c == null)){
                    ////system.debug('-----------Inside addError-----');
                    ////system.debug('------>> CaseObj.Last_Case_Comment_Date__c-----'+CaseObj.Last_Case_Comment_Date__c);
                    ////system.debug('------>> Trigger.oldMap.get(caseObj.id).Status-----'+Trigger.oldMap.get(caseObj.id).Status);
                    ////system.debug('------>> caseObj.Status-----'+caseObj.Status);
                    ////system.debug('------>> caseObj.Status-----'+caseObj.Status);
                    
                   if(Trigger.isUpdate && !caseObj.Status.equalsignorecase('Assigned')  && !caseObj.Status.equalsignorecase('Cancelled')  && !caseObj.Status.equalsignorecase('Closed') && Trigger.oldMap.get(caseObj.id).Status != 'Cancelled' && Trigger.oldMap.get(caseObj.id).Status!=null && !Trigger.oldMap.get(caseObj.id).Status.equalsignorecase('Draft') && Trigger.oldMap.get(caseObj.id).Status != 'New'  && CaseObj.Last_Case_Comment_Date__c==Trigger.oldMap.get(caseObj.id).Last_Case_Comment_Date__c )
                            caseObj.addError(Label.TBO_IncomingAccountContactMandatory);
                    }
                */
                if(recordTypeInfoMap.containskey(caseObj.recordtypeid) && (recordTypeInfoMap.get(caseObj.recordtypeid).getname().equalsignorecase('TBO REQUEST') ||recordTypeInfoMap.get(caseObj.recordtypeid).getname().equalsignorecase('Legal Name Update')) && caseObj.Status.equalsignorecase('Draft') && CaseObj.AccountId == null && caseObj.Master_Account_Name__c == null)
                {
                  
                        caseObj.addError(Label.TBO_OutgoingORIncomingCustomerMandatory);
                }
                
                if(recordTypeInfoMap.containskey(caseObj.recordtypeid) && recordTypeInfoMap.get(caseObj.recordtypeid).getname().equalsignorecase('TBO REQUEST')  && (caseObj.Master_Account_Name__c == null || caseObj.Contact_phone_number__c == null))
                {
                    ////system.debug('-----------Inside addError-----');
                    ////system.debug('------>> CaseObj.Last_Case_Comment_Date__c-----'+CaseObj.Last_Case_Comment_Date__c);
                    ////system.debug('------>> Trigger.oldMap.get(caseObj.id).Status-----'+Trigger.oldMap.get(caseObj.id).Status);
                    ////system.debug('------>> caseObj.Status-----'+caseObj.Status);
                    ////system.debug('------>> caseObj.Status-----'+caseObj.Status);              
                   if(Trigger.isUpdate && 
                    (caseObj.Status.equalsignorecase('In Progress')||
                    caseObj.Status.equalsignorecase('Completed')||
                    caseObj.Status.equalsignorecase('Closed')) && (caseObj.Master_Account_Name__c==null || caseObj.Contact_phone_number__c == null) && caseObj.Status_Cancel_Case__c==null )
                    {
                        caseObj.addError(Label.TBO_IncomingAccountContactMandatory);
                    }
                }
               
            }
            
            
            for (Case caseObj : Trigger.new) {
                //system.debug('>>> caseObj.recordtype.developername: '+caseObj.recordtype.developername);
                if(recordTypeInfoMap.containskey(caseObj.recordtypeid) && (recordTypeInfoMap.get(caseObj.recordtypeid).getname().equalsignorecase('TBO REQUEST') || (recordTypeInfoMap.get(caseObj.recordtypeid).getname().equalsignorecase('Legal Name Update')))){
                    
                    //commented as part of US #140 -Adhir[21-04-2016]
                    /*if(caseObj.Outgoing_Account_Name__c!=null && trigger.isInsert)
                        caseObj.AccountId=caseObj.Outgoing_Account_Name__c;*/
                    
                    if(Trigger.isUpdate)
                    {
                        //system.debug('For Update case');
                        //system.debug('Credit Profile'+'s Account id   == >'+caseObj.accountid);
                        ////system.debug('>>> accountEntries == >'+accountEntries);
                        ////system.debug('Credit Profile  == >'+accountEntries.get(caseObj.accountid).Credit_Profile__c);
                        //commented as part of US #140 -Adhir[21-04-2016]
                        /*if(caseObj.Outgoing_Account_Name__c!=null && trigger.oldMap.get(caseObj.id).Outgoing_Account_Name__c!=null && caseObj.Outgoing_Account_Name__c !=trigger.oldMap.get(caseObj.id).Outgoing_Account_Name__c)
                            caseObj.AccountId=caseObj.Outgoing_Account_Name__c;*/
                            
                       system.debug('umesh = Old closedCase.status'+trigger.oldmap.get(caseObj.id).status);
                       system.debug('umesh =closedCase.status'+caseObj.status);
                        system.debug('umesh = closedcase.Status_Cancel_Case__c'+caseObj.Status_Cancel_Case__c);
                        
                        /*Check status changed then we will send the notification escalation messages(US#23).*/
                        if ((caseObj.Status != Trigger.oldMap.get(caseObj.Id).Status) || 
                        (caseObj.TBO_In_Progress_Status__c == 'Waiting for internal-credit' && Trigger.oldMap.get(caseObj.Id).TBO_In_Progress_Status__c !='Waiting for internal-credit')){
                            system.debug('today = '+System.now());
                            caseObj.CheckStatusDate__c = System.now();
                            system.debug('updated date is as follows :'+caseObj.CheckStatusDate__c );
                        }
                        
                        // Updated the correct status name - Adhir [17-05-2016]
                          if(caseObj.Status_Cancel_Case__c == 'Cancelled' && caseObj.status !='Draft' && caseObj.status !='In Progress' &&
                         caseObj.status !='New' && caseObj.status !='Completed'&& caseObj.status !='Assigned'){
                            caseObj.status = caseObj.Status_Cancel_Case__c;
                            caseObj.TBO_In_Progress_Status__c =''; //need to update this value if in progress case is cancelled - umesh - 18-may-2016
                            system.debug('umesh = Inside Cancel old closedCase.status'+trigger.oldmap.get(caseObj.id).status);
                            system.debug('umesh = Inside Cancel closedCase.status'+caseObj.status);
                        }
                    } 
                      
                    //commented as part of US #140 -Adhir[21-04-2016]
                    /*if(caseObj.Outgoing_Account_Name__c==null)
                        caseObj.AccountId=null;*/
                    
                    /*if(caseObj.Outgoing_Account_Name__c!=null && trigger.isInsert)
                        caseObj.AccountId=caseObj.Outgoing_Account_Name__c;*/
                    
                    //modified as part of US #140 -Adhir[21-04-2016]
                    
                    if(caseObj.Accountid!= null){
                        AccountIds.add(caseObj.AccountId);
                    }
                    if(caseObj.Master_Account_Name__c!= null){
                        //system.debug('caseObj.Master_Account_Name__c&&&&&UMESH&&&&& =>>'+caseObj.Master_Account_Name__c);
                        AccountIds.add(caseObj.Master_Account_Name__c);
                        //system.debug('account Id is added');
                    }
                    if(caseObj.Contact_phone_number__c != null){
                        ContactIds.add(caseObj.Contact_phone_number__c);
                    }   
                }
                
            }
            Map<Id, Account> accountEntries;
            Map<Id, Contact> contactEntries;
            if(AccountIds != null && AccountIds.size() > 0){
                accountEntries = new Map<Id, Account>([select Id,Name,CBU_Name__c,DNTC__c,Credit_Profile__c,Marketing_Sub_Segment_Description__c,Parent.Correct_Legal_Name__c,
                Parent.RCID__c,Parent.RCID_Name__c,Parent.CBUCID_formula__c,Parent.CBU_Code_RCID__c,Parent.Sub_Segment_Code__r.Name,Parent.Parent.Name,
                BCAN__c,Billing_System__c from Account where id in :AccountIds]);
            }
            if(ContactIds != null && ContactIds.size() > 0){
                contactEntries = new Map<Id, Contact>([select Id,Email from Contact where id in :ContactIds]);
            }
            for (Case caseObj : Trigger.new) {
                if(recordTypeInfoMap.containskey(caseObj.recordtypeid) && (recordTypeInfoMap.get(caseObj.recordtypeid).getname().equalsignorecase('TBO REQUEST') || (recordTypeInfoMap.get(caseObj.recordtypeid).getname().equalsignorecase('Legal Name Update')))){
                    //  Modified as part of US #140 - Adhir[21-04-2016]
                    if(caseObj.Accountid != null){
                        caseObj.Business_Unit_Outgoing__c = accountEntries.get(caseObj.Accountid).CBU_Name__c;
                        caseObj.Subsegment_Outgoing__c = accountEntries.get(caseObj.Accountid).Marketing_Sub_Segment_Description__c;
                        
                        /* ECH Reporting US #93 - These fields are populated for ECH report.
                        //  Modified as part of US #140 - Adhir[21-04-2016]
                        
                        if(accountEntries.get(caseObj.Accountid).Parent != null){
                            caseObj.RCID_Outgoing__c = accountEntries.get(caseObj.Accountid).Parent.RCID__c;
                            caseObj.RCID_Name_Outgoing__c = accountEntries.get(caseObj.Accountid).Parent.RCID_Name__c;
                            caseObj.CBUCID_Outgoing__c = accountEntries.get(caseObj.Accountid).Parent.CBUCID_formula__c;
                            caseObj.CBU_Code_Outgoing__c = accountEntries.get(caseObj.Accountid).Parent.CBU_Code_RCID__c;
                            caseObj.Marketing_Sub_Segment_A_Outgoing__c = accountEntries.get(caseObj.Accountid).Parent.Sub_Segment_Code__r.Name;
                            caseObj.Correct_Legal_Name_Outgoing__c = accountEntries.get(caseObj.Accountid).Parent.Correct_Legal_Name__c;
                        }
                        if(accountEntries.get(caseObj.Accountid).Parent != null && accountEntries.get(caseObj.Accountid).Parent.Parent != null){
                            caseObj.CBUCID_Name_Outgoing__c = accountEntries.get(caseObj.Accountid).Parent.Parent.Name;
                        }
                        //caseObj.CAN_highest_payable_account_Outgoing__c = accountEntries.get(caseObj.Accountid).CAN_BillingHighLevel_Indicator__c;
                        String BCANStr = accountEntries.get(caseObj.Accountid).BCAN__c;
                        if(BCANStr != null && BCANStr != ''){
                            String finalBCANStr = BCANstr.substring(4, BCANstr.length());
                            caseObj.BCAN_Outgoing__c = finalBCANStr;
                        }
                        caseObj.Billing_System_Outgoing__c = accountEntries.get(caseObj.Accountid).Billing_System__c;
                        */ 
                    }else{
                        caseObj.Business_Unit_Outgoing__c = null;
                        caseObj.Subsegment_Outgoing__c = null;
                    }   
                    if(caseObj.Master_Account_Name__c != null){
                        caseObj.Business_Unit_Incoming__c = accountEntries.get(caseObj.Master_Account_Name__c).CBU_Name__c;
                        /*US#53 we need to update DNTC flag due to unavailablity of Direct access Incoming Customer Fileds.*/
                        caseObj.Incoming_Customer_DNTC__c = accountEntries.get(caseObj.Master_Account_Name__c).DNTC__c;
                        /*US71 where we nedd to show incoming customer account name into ECH Queue.*/
                        caseObj.Incoming_Customer_Account__c = accountEntries.get(caseObj.Master_Account_Name__c).Name;
                        //system.debug('caseObj.Incoming_Customer_DNTC__c  $$$$$$$UMESH########'+caseObj.Incoming_Customer_DNTC__c);
                        caseObj.Subsegment_Incoming__c = accountEntries.get(caseObj.Master_Account_Name__c).Marketing_Sub_Segment_Description__c;
                        
                        /* ECH Reporting US #93 - These fields are populated for ECH report.*/
                        /*
                        if(accountEntries.get(caseObj.Master_Account_Name__c).Parent != null){
                            caseObj.RCID_Incoming__c = accountEntries.get(caseObj.Master_Account_Name__c).Parent.RCID__c;
                            caseObj.RCID_Name_Incoming__c = accountEntries.get(caseObj.Master_Account_Name__c).Parent.RCID_Name__c;
                            caseObj.CBUCID_Incoming__c = accountEntries.get(caseObj.Master_Account_Name__c).Parent.CBUCID_formula__c;
                            caseObj.CBU_Code_Incoming__c = accountEntries.get(caseObj.Master_Account_Name__c).Parent.CBU_Code_RCID__c;
                            caseObj.Marketing_Sub_Segment_A_Incoming__c = accountEntries.get(caseObj.Master_Account_Name__c).Parent.Sub_Segment_Code__r.Name;
                            caseObj.Correct_Legal_Name_Incoming__c = accountEntries.get(caseObj.Master_Account_Name__c).Parent.Correct_Legal_Name__c;
                        }
                        if(accountEntries.get(caseObj.Master_Account_Name__c).Parent != null && accountEntries.get(caseObj.Master_Account_Name__c).Parent.Parent != null){
                            caseObj.CBUCID_Name_Incoming__c = accountEntries.get(caseObj.Master_Account_Name__c).Parent.Parent.Name;
                        }
                        //caseObj.CAN_highest_payable_account_Incoming__c = accountEntries.get(caseObj.Master_Account_Name__c).CAN_BillingHighLevel_Indicator__c;
                        String BCANStr = accountEntries.get(caseObj.Master_Account_Name__c).BCAN__c;
                        if(BCANStr != null && BCANStr != ''){
                            String finalBCANStr = BCANstr.substring(4, BCANstr.length());
                            caseObj.BCAN_Incoming__c = finalBCANStr;
                        }
                        caseObj.Billing_System_Incoming__c = accountEntries.get(caseObj.Master_Account_Name__c).Billing_System__c;
                        */
                    }else{
                        caseObj.Business_Unit_Incoming__c = null;
                        caseObj.Subsegment_Incoming__c = null;
                    }
                    //system.debug('-------------caseObj.Contact_phone_number__c------ '+ caseObj.Contact_phone_number__c);
                    //system.debug('-------------caseObj.Contact_phone_number__r.Email------ '+ caseObj.Contact_phone_number__r.Email);
                    
                    if(caseObj.Contact_phone_number__c != null){
                        //caseObj.Contact_Email__c = contactEntries.get(caseObj.Contact_phone_number__c).Email;
                        caseObj.Incoming_Contact_Email__c = contactEntries.get(caseObj.Contact_phone_number__c).Email;
                    }
                }
                /*TBO US#82: Credit profile autopopulation for Incoming customer*/            
                if(Trigger.isUpdate && recordTypeInfoMap.containskey(caseObj.recordtypeid) && 
                recordTypeInfoMap.get(caseObj.recordtypeid).getname().equalsignorecase('TBO REQUEST') && 
                caseObj.Master_Account_Name__c != null && 
                (caseObj.Credit_Profile__c == null || caseObj.Master_Account_Name__c != trigger.oldMap.get(caseObj.Id).Master_Account_Name__c)  ){
                    //system.debug('Credit Profile'+'s Account id   == >'+caseObj.Master_Account_Name__c);
                    //system.debug('Credit Profile  == >'+accountEntries.get(caseObj.Master_Account_Name__c).Credit_Profile__c);
                    
                    if(accountEntries.get(caseObj.Master_Account_Name__c).Credit_Profile__c!=null)
                        caseObj.Credit_Profile__c = accountEntries.get(caseObj.Master_Account_Name__c).Credit_Profile__c;
                    else
                        caseObj.Credit_Profile__c =null;
                }
                //system.debug('For insert case');
                           
                if( trigger.isInsert && recordTypeInfoMap.containskey(caseObj.recordtypeid) && recordTypeInfoMap.get(caseObj.recordtypeid).getname().equalsignorecase('TBO REQUEST') && caseObj.Master_Account_Name__c != null && caseObj.Credit_Profile__c == null  && accountEntries.get(caseObj.Master_Account_Name__c).Credit_Profile__c !=null){
                           
                    caseObj.Credit_Profile__c = accountEntries.get(caseObj.Master_Account_Name__c).Credit_Profile__c;
                }
                /* end US#82*/
                
                /*TBO US#133: Credit profile autopopulation for Legal name Update*/            
                if(Trigger.isUpdate && recordTypeInfoMap.containskey(caseObj.recordtypeid) && 
                recordTypeInfoMap.get(caseObj.recordtypeid).getname().equalsignorecase('Legal Name Update') && 
                caseObj.accountid != null && 
                (caseObj.Credit_Profile__c == null || caseObj.accountid != trigger.oldMap.get(caseObj.Id).accountid)  ){
                    //system.debug('Credit Profile'+'s Account id   == >'+caseObj.accountid);
                    //system.debug('Credit Profile  == >'+accountEntries.get(caseObj.accountid).Credit_Profile__c);
                    
                    if(accountEntries.get(caseObj.accountid).Credit_Profile__c!=null)
                        caseObj.Credit_Profile__c = accountEntries.get(caseObj.accountid).Credit_Profile__c;
                    else
                        caseObj.Credit_Profile__c =null;
                }
                //system.debug('For insert case');
                //system.debug('Credit Profile'+'s Account id   == >'+caseObj.accountid);
                //system.debug('>>> accountEntries == >'+accountEntries);
                ////system.debug('Credit Profile  == >'+accountEntries.get(caseObj.accountid).Credit_Profile__c);
                           
                if( trigger.isInsert && recordTypeInfoMap.containskey(caseObj.recordtypeid) && recordTypeInfoMap.get(caseObj.recordtypeid).getname().equalsignorecase('Legal Name Update') && caseObj.accountid != null && caseObj.Credit_Profile__c == null  && accountEntries.get(caseObj.accountid).Credit_Profile__c !=null){
                           
                    caseObj.Credit_Profile__c = accountEntries.get(caseObj.accountid).Credit_Profile__c;
                }
                
                
               /* end US#133*/
            }
        }
        // END Added by Rajan - TBO user story
    } 
    catch(QueryException e){
        
    }    
}