/*
###########################################################################
# File..................: OCOM_UpdateOrderWFM 
# Version...............: 29
# Created by............: Nitin Saxena
# Created Date..........: 09/02/2016
# Last Modified by......: 
# Last Modified Date....: 
# Description...........: This trigger will populate the latest work order's scheduled due date onto its related order
#                         
###########################################################################
*/

trigger OCOM_UpdateOrderWFM on Work_Order__c (after insert,after update) { 

    if(Trigger.isInsert)
    {
        Map<Id,order> maporder=new Map<Id,order>();
        for(Work_Order__c workOrder : Trigger.New)
        {if(!(String.isBlank(Trigger.new[0].order__c))){
            if('Fieldwork'.equalsIgnoreCase(workOrder.Install_Type__c) && 'Reserved'.equalsIgnoreCase(workOrder.status__c))
               maporder.put(workOrder.order__c,new order(id=workOrder.order__c,Soonest_Fieldwork_Due_Date__c=workOrder.Scheduled_Datetime__c));
            else if('Fieldwork'.equalsIgnoreCase(workOrder.Install_Type__c))
                maporder.put(workOrder.order__c,new order(id=workOrder.order__c,Soonest_Fieldwork_Due_Date__c=null));        
        }}
          if((maporder.values()).size()>0)
         update maporder.values();
    }
    else if(Trigger.IsUpdate)
    {
        if(Trigger.new[0].order__c == null){
         if(!(String.isBlank(Trigger.new[0].order__c))){
            Set<string> setorderIds=new Set<string>();
        for(Work_Order__c workOrder : Trigger.New)
        {
           
            setorderIds.add(workOrder.order__c);
        }
        system.debug('>>>setorderIds:'+setorderIds );
        if(string.valueOf(setorderIds) <> '{null}'){
        system.debug(' setorderIds is empty :'+ (setorderIds).isEmpty());
         system.debug(' setorderIds is size:'+ (setorderIds).contains('null'));
        List<Work_Order__c> lstWorkOrders=[Select id,Status__c,Opportunity__c,Install_Type__c,order__c,Scheduled_Datetime__c from Work_Order__c where order__c in :setorderIds order by createddate desc ];
        
        Map<Id, order> maporder=new Map<Id, order> ();
        for(Work_Order__c workOrder : lstWorkOrders)
        {
            system.debug('lstWorkOrders##'+workOrder.order__c);
           
            if(maporder.containsKey(workOrder.order__c)==false)
            {
                if('Fieldwork'.equalsIgnoreCase(workOrder.Install_Type__c) && 'Reserved'.equalsIgnoreCase(workOrder.status__c))
                   maporder.put(workOrder.order__c,new order(id=workOrder.order__c,Soonest_Fieldwork_Due_Date__c=workOrder.Scheduled_Datetime__c));
                else if('Fieldwork'.equalsIgnoreCase(workOrder.Install_Type__c))
                    maporder.put(workOrder.order__c,new order(id=workOrder.order__c,Soonest_Fieldwork_Due_Date__c=null));        
            }   
                }       system.debug('MapOrder##'+maporder.values());
           update maporder.values();
         }
         }/*else{
             Trigger.New[0].addError('No Order found for this workorder. Please associate a WFM booking to the Order!');
         }*/
        }
        
    }   
}