/** 
*@author Update by: David Barrera (dbarrera@tractionondemand.com)
* 
*/



trigger CalculateBusinessHoursAges on Case (before insert, before update) {  

    private final static Id DEFAULT_HOURS_ID = [SELECT Metadata_Value__c FROM Apex_Org_Default__mdt WHERE DeveloperName = 'DefaultBusinessHoursId' LIMIT 1].Metadata_Value__c;


    try{        
        system.debug('CalculateBusinessHoursAge---TRIGGER!');
        System.debug('Total Number of SOQL Queries allowed in this Apex code context: ' +  Limits.getLimitQueries());
        System.debug('Total Number of records that can be queried  in this Apex code context: ' +  Limits.getLimitDmlRows());
        System.debug('Total Number of DML statements allowed in this Apex code context: ' +  Limits.getLimitDmlStatements() );
        System.debug('Total Number of CPU usage time (in ms) allowed in this Apex code context: ' +  Limits.getLimitCpuTime());
    
		Map<ID, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Case.getRecordTypeInfosById();
	        
        if (Trigger.isInsert) {
            for (Case updatedCase:System.Trigger.new) {
                updatedCase.Last_Status_Change__c = System.now();
                updatedCase.Time_With_Customer__c = 0;
                updatedCase.Time_With_Support__c = 0;
                updatedCase.time_with_vendor__c=0;
            }
        } else {
            //if(Trigger.isAfter){
                if(Trigger.isUpdate){
                //Get the stop statuses
                    // using hard-coded map in order to eliminate the soql query below
                    Map<String, String> statusToWaitingFor = new Map<String,String>{
                        'Submitted to JIRA' => 'Vendor',
                        'Awaiting Customer' => 'Customer',
                        'Waiting on Customer' => 'Customer',
                        'Submitted to BT' => 'Customer',
                        'In Progress - Waiting on Approval' => 'Customer',
                        'New' => 'Telus',
                        'In Progress' => 'Telus',
                        'Re-Opened' => 'Telus',
                        'Collecting Requirements' => 'Telus',
                        'Issue Pending' => 'Customer'
                    };        
    //            Map<String, String> statusToWaitingFor = new Map<String,String>();        
                String tmpStatus = '';
                //***** REMOVED, CAUSING SOQL LIMIT EXCEPTION                
                //List<Stop_Status__c> stopStatuses = [Select Name,Stop_Status_Name__c,Waiting_For__c From Stop_Status__c];
                //System.debug('1. Number of Queries used in this Apex code so far: ' + Limits.getQueries());
                //System.debug('2. Number of rows queried in this Apex code so far: ' + Limits.getDmlRows());
                //System.debug('3. Number of DML statements used so far: ' +  Limits.getDmlStatements());    
                //System.debug('4. Amount of CPU time (in ms) used so far: ' + Limits.getCpuTime());
    
                //for (Stop_Status__c stopStatus:stopStatuses) { 
                //    statusToWaitingFor.put(stopStatus.Name, stopStatus.Waiting_For__c);
                //}
                
                if(Limits.getQueries() >= 100){
                    System.debug('CalculateBusinessHoursAges: abort since SOQL limmit will be hit.');
                    
                    throw new QueryException();
                }
                    
                    
    //          Id defaultHoursId = Id.valueOf('01m40000000Gxg8AAC');
                        
        system.debug('defaultHours- ' + DEFAULT_HOURS_ID);
                     System.debug('1. Number of Queries used in this Apex code so far: ' + Limits.getQueries());
        System.debug('2. Number of rows queried in this Apex code so far: ' + Limits.getDmlRows());
        System.debug('3. Number of DML statements used so far: ' +  Limits.getDmlStatements());    
        System.debug('4. Amount of CPU time (in ms) used so far: ' + Limits.getCpuTime());
    
                //Get the closed statuses (because at the point of this trigger Case.IsClosed won't be set yet)
                Set<String> closedStatusSet = new Set<String>();
        
                if(Limits.getQueries() >= 100){
                    System.debug('CalculateBusinessHoursAges: abort since SOQL limmit will be hit.');
                    
                    throw new QueryException();
                }
    
                for (CaseStatus status:[Select MasterLabel From CaseStatus where IsClosed=true]) {
                    closedStatusSet.add(status.MasterLabel);
                }
        
                //For any case where the status is changed, recalc the business hours in the buckets
                for (Case updatedCase:System.Trigger.new) {
                    System.debug('updatedCase: recordTypeId: ' + updatedCase.recordTypeId);
                    String recordTypeName = rtMap.get(updatedCase.recordTypeId).getName();
                    System.debug('updatedCase: recordTypeName: ' + recordTypeName);                    
                    // do not run calculation for record types
                    if(recordTypeName == 'CEM Engagement'){
                        System.debug('Bypass business hours calculation for recordType: ' + recordTypeName);
                        
                        continue;
                    }

                    Case oldCase = System.Trigger.oldMap.get(updatedCase.Id);
        
                    if (oldCase.Status!=updatedCase.Status && updatedCase.Last_Status_Change__c!=null) {
                        //OK, the status has changed
                        if (!oldCase.IsClosed) {
                            //We only update the buckets for open cases
        
                            //On the off-chance that the business hours on the case are null, use the default ones instead
                            Id hoursToUse = updatedCase.BusinessHoursId!=null?updatedCase.BusinessHoursId:DEFAULT_HOURS_ID;
        
                            //The diff method comes back in milliseconds, so we divide by 3600000 to get hours.
                            Double timeSinceLastStatus = BusinessHours.diff(hoursToUse, updatedCase.Last_Status_Change__c, System.now())/3600000.0;
                            System.debug(timeSinceLastStatus);
                            //We decide which bucket to add it to based on whether the status of the case has an associated record with it in the 'Stop_Status' object.
                            if(statusToWaitingFor.containsKey(oldCase.Status)){
                                tmpStatus  = statusToWaitingFor.get(oldCase.Status);
                                if (tmpStatus == 'Customer') {
                                    if(updatedCase.Time_With_Customer__c != null){
                                        updatedCase.Time_With_Customer__c += timeSinceLastStatus;
                                        System.debug('Customer' + updatedCase.Time_With_Customer__c );
                                    }
                                } 
                                else if(tmpStatus == 'Telus') {
                                    if(updatedCase.Time_With_Support__c != null){
                                        updatedCase.Time_With_Support__c += timeSinceLastStatus;
                                        System.debug('Telus' + updatedCase.Time_With_Customer__c );
                                    }
                                }
                                else {
                                    if(updatedCase.Time_With_Vendor__c != null){
                                        updatedCase.Time_With_Vendor__c += timeSinceLastStatus;
                                    }
                                }
                            }
                            if(updatedCase!=null&&updatedCase.time_with_vendor__c==null){
                                updatedCase.time_with_vendor__c=0;
                            }
        
                            if (closedStatusSet.contains(updatedCase.Status)) {
                                updatedCase.Case_Age_In_Business_Hours__c = updatedCase.Time_With_Customer__c + updatedCase.Time_With_Support__c+ updatedCase.Time_With_Vendor__c;
                             System.debug('Case_Age_In_Business_Hours__c' + updatedCase.Case_Age_In_Business_Hours__c );
                            }
                        }
        
                        updatedCase.Last_Status_Change__c = System.now();
                        System.debug('Last_Status_Change__c' + updatedCase.Last_Status_Change__c );
                    }
                }
                
            }
        }
        System.debug('Final number of DML statements used so far: ' +  Limits.getDmlStatements());
            System.debug('Final heap size: ' +  Limits.getHeapSize());
    }
    catch(QueryException e){
        System.debug('CalculateBusinessHoursAges, QueryException: ' +  e);
        
    }
}