/*****************************************************
    Trigger Name: TeamTelusIdFromAccountOwner 
    Usage: TELUS - Field from Account Owner's User Record
    Author – Clear Task    
    Date – 3/22/2011       
    Revision History
******************************************************/
trigger TeamTelusIdFromAccountOwner on Account (before insert, before update){
    //account ownerId set
    Set<Id> userIdSet = new Set<Id>();
    set<id> accountParentIds=new set<id>();    
    for(Account a :trigger.new){  
        if(string.isnotblank(a.parentId)){
            accountParentIds.add(a.parentId);
        }      
        userIdSet.add(a.OwnerId);
        // Traction Change: Clear field then repopulate. Resolves issue with null TTIDs.
        a.Team_TELUS_ID__c = null;
        if(string.isblank((string)a.get('correct_legal_name__c'))){
          if(string.isnotblank(a.Legal_Business_Name__c)){
            a.put('correct_legal_name__c',a.legal_business_name__c);
          }else{
            a.put('correct_legal_name__c',a.name);
          }
      }    
    }
    if(userIdSet != null && userIdSet.size()>0){
        Map<Id, User> userMap = new Map<Id, User>([select Team_TELUS_ID__c from User where Id in :userIdSet]);
        System.debug('userMap =' +userMap );
        if(userMap != null && userMap.size()>0){
            for(Account a :trigger.new){
                User userObj = userMap.get(a.OwnerId);
                if(userObj != null && userObj.Team_TELUS_ID__c != null){
                    a.Team_TELUS_ID__c = userObj.Team_TELUS_ID__c;
                }
            }
        }
    }
    if(trigger.isbefore){
        if(trigger.isupdate){
            map<id,schema.recordtypeinfo> accountRecordTypeInfoMap=account.sobjecttype.getdescribe().getrecordtypeinfosbyid();
            map<id,account> parentAccountsMap=new map<id,account>([select id,recordtypeid from account where id=:accountParentIds]);
            set<id> accountIds=new set<id>();
            set<id> deactivateAssociationIds=new set<id>();
            for(account account:trigger.new){
                account oldAccount=(account)trigger.oldmap.get(account.id);
                if(accountRecordTypeInfoMap.get(account.recordtypeid).getname().equalsignorecase('can')){
                    account parentAccount=parentAccountsMap.get(account.parentid);
                    if(parentAccount!=null){
                        if(accountRecordTypeInfoMap.get(parentAccount.recordtypeid).getname().equalsignorecase('rcid')){
                            if(account.parentid!=oldAccount.parentid){
                                accountIds.add(account.id);
                            }
                        }
                    }
                }
            }
            list<contact_linked_billing_account__c> associations=[select id,contact__r.accountid,billing_account__r.parentid from contact_linked_billing_account__c where active__c=true and billing_account__c!=null and billing_account__c=:accountIds];
            for(contact_linked_billing_account__c association:associations){
                if(association.contact__r.accountid!=association.billing_account__r.parentid){
                    deactivateAssociationIds.add(association.id);
                }
            }/*
            if(!deactivateAssociationIds.isempty()){
                ContactLinkedBillingAccountExt ext=new ContactLinkedBillingAccountExt();
                ext.disassociateAssociations(deactivateAssociationIds);
            }*/
        }
    }
}