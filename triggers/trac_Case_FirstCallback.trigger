trigger trac_Case_FirstCallback on Task (after insert) {

    /*
        Initial Callback in Cases
        TELUS Prime: Yen Li Chong
        Start Date: August 10th
        
        Part of the TELUS Usability II projects
        
        Created By: Alex Miller <amiller@tractionondemand.com>
        Project Manager: Vince Liu <vliu@tractionondemand.com>
    */

    Id[] caseIds = new List<Id>();
    Map<Id, Case> caseMap = null;
    Task[] includedTasks = new List<Task>();
    
    String caseKeyPrefix = Schema.sObjectType.Case.getKeyPrefix();
    
    for (Task t : trigger.new) {
        if (t.WhatId != null && t.Subject != null) {
            String s = (String) t.WhatId;
            if (s.startsWith(caseKeyPrefix) && t.Subject.equalsIgnoreCase('first call')) {
                caseIds.add(t.WhatId);
                includedTasks.add(t);
            }
        }
    }
    
    if (caseIds == null || caseIds.size() == 0) { return; }
    
    caseMap = new Map<Id, Case>([Select First_Callback_Time__c, CreatedDate From Case Where Id In :caseIds]);
    /*list<casehistory> histories=[select caseid,case.owner.profile.name,createddate,field,newvalue from casehistory where caseid=:caseIds order by caseid,createddate desc];
    map<id,datetime> caseHistoryMap=new map<id,datetime>();
    for(casehistory history:histories){
        if(history.case.owner.profile.name.containsignorecase('smb care escalation')){
            caseHistoryMap.put(history.caseid,history.createddate);
        }else
        if(history.newvalue!=null&&((string)history.newvalue).equalsignorecase('smb care escalations')){
            caseHistoryMap.put(history.caseid,history.createddate);
        }
    }*/
    if (caseMap == null || caseMap.size() == 0) { return; }
    /*
    businesshours bh=null;
    try{
        bh=[select id from businesshours where isactive=true and name='Escalation'];
    }catch(exception e){
        bh=null;
        Util.emailSystemError(e);
    }
    integer increment=1;
    integer total=0;*/
    for (Task t : includedTasks) {
        Case c = caseMap.get(t.WhatId);
        if (c.First_Callback_Time__c == null) {
            c.First_Callback_Time__c = t.CreatedDate;
            /*
            if(bh!=null){
                if(c.escalation_queue_to_first_callback__c!=null){
                    if(caseHistoryMap.containskey(c.id)){
                        datetime assignedToQueueTime=datetime.newinstance(caseHistoryMap.get(c.id).date(),caseHistoryMap.get(c.id).time());
                        while(assignedToQueueTime<t.createddate){
                            if(businesshours.iswithin(bh.id,assignedToQueueTime)){
                                total+=increment;
                            }
                            assignedToQueueTime=assignedToQueueTime.addminutes(increment);
                        }
                        c.escalation_queue_to_first_callback__c=total;
                        total=0;
                    }
                }
            }*/
            c.Time_To_First_Callback__c = (Decimal)(DateTime.now().getTime() - c.CreatedDate.getTime()) / 1000.00 / 3600.00;
        }
    }
    update caseMap.values();
}