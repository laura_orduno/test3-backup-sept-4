/*
TEMP TRIGGER
Called on insert of Account.
1) Updates New Account Record Types to Standard.

Author - Tom Muzyka
Created - May 2010
*/
trigger tempStandardAccount on Account (after insert) 
{
    Id accStandardRecId = '01240000000DnnO'; //'012R00000000O0Z';
    Set<Id> accIds = new Set<Id>();
    
    for (Account acc : trigger.new) 
    {
        accIds.add(acc.Id);
    }
    
    //update Account Record Type
    List<Account> accList = [Select Id ,RecordTypeId 
                               from Account where Id In :accIds];
    for (Account acc: accList)
    {
        acc.RecordTypeId = accStandardRecId;
    }
    update accList;
}