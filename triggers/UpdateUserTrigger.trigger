/**
 * This trigger will Update the UserObject when the Employee records has been insert/updated with new Data. 
 * Following Two field will get update on User Table
 * User.ManagerId = Employee.Manager_SFDC_UserID__c
 * User.Release_Code__c = Employee.Relese_Code__c
 */
trigger UpdateUserTrigger on Employee__c (before insert,before update) {

    List<User> toUpdate = new List<User>();
    Map<String,Employee__c> employeeItemMap = new Map<String,Employee__c>();
    
    List<Employee__c> items = Trigger.new;

    for(Employee__c item :items) {
        employeeItemMap.put(item.Employee_ID__c,item);
    }
   
    List<User> userList = [Select id, EmployeeId__c, ManagerId, Release_Code__c from User where EmployeeId__c in : employeeItemMap.keySet()];
   
    for( User usr : userList ) {
        Employee__c  emp =  employeeItemMap.get(usr.EmployeeId__c);
        usr.ManagerId = emp.Manager_SFDC_UserID__c;
        usr.Release_Code__c = emp.Release_Code__c;
        toUpdate.add(usr);
    }
    
    try{
        update toUpdate;
    }catch(DMLException e){
        System.debug(e);
    }
    
}