/*****************************************************
    Trigger Name: ContactOwnerEmailFromContactOwner 
    Usage: TELUS - Field from Contact Owner's User Record
    Author – Allan Roszmann    
    Date – 10/17/2011       
    Revision History
******************************************************/
trigger UserEmailAddressFromContactOwner on Contact (before insert, before update){     
    //contact ownerId set
    Set<Id> userIdSet = new Set<Id>();    
    for(Contact a :trigger.new){        
        userIdSet.add(a.OwnerId);        
    }
    if(userIdSet != null && userIdSet.size()>0){
        Map<Id, User> userMap = new Map<Id, User>([select Email from User where Id in :userIdSet]);
        System.debug('userMap =' +userMap );
        if(userMap != null && userMap.size()>0){
            for(Contact a :trigger.new){
                User userObj = userMap.get(a.OwnerId);
                if(userObj != null && userObj.Email != null){
                    a.Contact_Owner_Email__c = userObj.Email ;   
                }
            }
        }
    }
}