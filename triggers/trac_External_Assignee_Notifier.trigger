trigger trac_External_Assignee_Notifier on Task (after insert, after update) {
    
    
    // First, let's get the list of tasks.
    Savepoint sp = Database.setSavepoint();
    try {
        ExternalUserPortal__c eups = ExternalUserPortal__c.getInstance();
        if(!Test.isRunningTest()) {
            System.assert(eups != null, 'Failed to load critical portal settings');
            System.assert(eups.HMAC_Digest_Key__c != null, 'Failed to load critical digest settings');
        }
            
        if (true) {
            Task[] atasks = trigger.new;
            Task[] tasks = new List<Task>();
            for (Task t : atasks) {
                if (t.External_Assignee__c == null) { continue; }
                String oea = '';
                if (trigger.oldMap != null && trigger.oldMap.get(t.Id) != null && trigger.oldMap.get(t.Id).External_Assignee__c != null) {
                     oea = trigger.oldMap.get(t.Id).External_Assignee__c;
                }
                String nea = t.External_Assignee__c;
                if (nea == null) { nea = ''; }
                if (trigger.isUpdate && (oea == nea)) { continue; }
                if (t.Status == 'Completed') { continue; }
                tasks.add(new Task(Id = t.Id, External_Assignee__c = t.External_Assignee__c, WhatId = t.WhatId, WhoId = t.WhoId));
            }
            if (tasks != null && tasks.size() > 0) {
            
                Messaging.SingleEmailMessage[] infomessages = new List<Messaging.SingleEmailMessage>();
                
                List<Id> leadIds = new List<Id>();
                List<Id> contactIds = new List<Id>();
                List<Id> accountIds = new List<Id>();
                List<Id> oppIds = new List<Id>();
                List<Id> userIds = new List<Id>();
                
                for (Task task : tasks) {
                    userIds.add(task.CreatedById);
                    if (task.WhoId != null) {
                        if (String.valueOf(task.WhoId).substring(0, 3) == '003') {
                            // Contact!
                            contactIds.add(task.WhoId);
                        } else if (String.valueOf(task.WhoId).substring(0, 3) == '00Q') {
                            // Lead!
                            leadIds.add(task.WhoId);
                        }
                    }
                    if (task.WhatId != null) {
                        if (String.valueOf(task.WhatId).substring(0, 3) == '001') {
                            // Contact!
                            accountIds.add(task.WhatId);
                        } else if (String.valueOf(task.WhatId).substring(0, 3) == '006') {
                            // Lead!
                            oppIds.add(task.WhatId);
                        }
                    }
                }
                Map<Id, User> createdUsers = new Map<Id, User>([Select Name From User Where Id in :userIds]);
                
                Map<Id, Lead> leadMap = null;
                if (leadIds != null && leadIds.size() > 0) {
                    leadMap = new Map<Id, Lead>([Select Name From Lead Where Id In :leadIds]);
                }
                Map<Id, Contact> contactMap = null;
                if (contactIds != null && contactIds.size() > 0) {
                    contactMap = new Map<Id, Contact>([Select Name From Contact Where Id In :contactIds]);
                }
                
                Map<Id, Account> accountMap = null;
                if (accountIds != null && accountIds.size() > 0) {
                    accountMap = new Map<Id, Account>([Select Name From Account Where Id In :accountIds]);
                }
                
                Map<Id, Opportunity> oppMap = null;
                if (oppIds != null && oppIds.size() > 0) {
                    oppMap = new Map<Id, Opportunity>([Select Name From Opportunity Where Id In :oppIds]);
                }
                
                for (Task t : tasks) {
                    Blob digestBlob = Crypto.generateMac('hmacSHA512', Blob.valueOf(t.Id), EncodingUtil.base64decode(eups.HMAC_Digest_Key__c));
                    System.assert(digestBlob != null, 'Failed to generate token for ' + t.Id);
                    String token = EncodingUtil.base64Encode(digestBlob);
                    System.assert(token != null, 'Failed to encode token for ' + t.Id);
                    t.Token__c = token;
                    
                    
                    String relatedTo = '';
                    if (t.WhatId != null) {
                        if (String.valueOf(t.WhatId).substring(0, 3) == '001') {
                            if (accountMap.get(t.WhatId) != null) {
                                relatedTo += accountMap.get(t.WhatId).Name + ' (Account)';
                            }
                        } else if (String.valueOf(t.WhatId).substring(0, 3) == '006') {
                            if (oppMap.get(t.WhatId) != null) {
                                relatedTo += oppMap.get(t.WhatId).Name + ' (Opportunity)';
                            }
                        }
                    }
                    
                    if (t.WhoId != null) {
                        if (String.valueOf(t.WhoId).substring(0, 3) == '003') {
                            // Contact!
                            if (contactMap.get(t.WhoId) != null) {
                                if (relatedTo != '') { relatedTo += ' and '; }
                                relatedTo += contactMap.get(t.whoId).Name + ' (Contact)';
                            }
                        } else if (String.valueOf(t.WhoId).substring(0, 3) == '00Q') {
                            // Lead!
                            if (leadMap.get(t.WhoId) != null) {
                                if (relatedTo != '') { relatedTo += ' and '; }
                                relatedTo += leadMap.get(t.whoId).Name + ' (Lead)';
                            }
                        }
                    }
                    
                    User u = createdUsers.get(t.CreatedById);
                    String creatorName = 'A TELUSforce user';
                    if (u != null) {
                        creatorName = u.Name;
                    }
                    
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setToAddresses(new String[] {t.External_Assignee__c});
                    mail.setReplyTo('sfdcsupport@telus.com');
                    mail.setSenderDisplayName('TELUSforce Tasks');
                    mail.setSubject('A Salesforce Task Has Been Assigned To You');
                    mail.setBccSender(false);
                    mail.setPlainTextBody(creatorName + ' has submitted the following task related to ' + relatedTo + ' for your action. \n\n' +
                        'Please go to ' + eups.Portal_URL__c + '/?oid=' + t.Id + '&tok=' + t.Token__c + ' to view details of your task and mark it as “Complete”.');
                    
                    mail.setHtmlBody(creatorName + ' has submitted the following task related to ' + relatedTo + ' for your action.<br/>\n<br/>\n' +
                        'Please <a href="' + eups.Portal_URL__c + '/ExternalServicePortalTaskInformation?oid=' + t.Id + '&tok=' + EncodingUtil.urlEncode(t.Token__c, 'UTF-8') + '" style="font-weight: bold; font-size: 1.1em;">click here</a> to view details of your task and mark it as “Complete”.');
                    infomessages.add(mail);
                }
                if (infomessages != null && infomessages.size() > 0) { Messaging.sendEmail(infomessages); }
                update tasks;
            }
        }
        
        if (trigger.isUpdate) {
            User activeUser = [Select Email From User where Id = :UserInfo.getUserId() limit 1];
            Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage>();
            /* Don't forget the "External Assignee" */
            
            List<Id> leadIds = new List<Id>();
            List<Id> contactIds = new List<Id>();
            List<Id> accountIds = new List<Id>();
            List<Id> oppIds = new List<Id>();
            List<Id> userIds = new List<Id>();
            
            Integer ix = 0;
            while (ix < trigger.new.size()) {
                if (trigger.old[ix].Status != 'Completed' && trigger.new[ix].Status == 'Completed' && trigger.new[ix].External_Assignee__c != null) {
                    Task task = trigger.new[ix];
                    if (task.WhoId != null) {
                        if (String.valueOf(task.WhoId).substring(0, 3) == '003') {
                            contactIds.add(task.WhoId);
                        } else if (String.valueOf(task.WhoId).substring(0, 3) == '00Q') {
                            leadIds.add(task.WhoId);
                        }
                    }
                    if (task.WhatId != null) {
                        if (String.valueOf(task.WhatId).substring(0, 3) == '001') {
                            accountIds.add(task.WhatId);
                        } else if (String.valueOf(task.WhatId).substring(0, 3) == '006') {
                            oppIds.add(task.WhatId);
                        }
                    }
                }
                ix++;
            }
            
            Map<Id, Lead> leadMap = null;
            if (leadIds != null && leadIds.size() > 0) {
                leadMap = new Map<Id, Lead>([Select Name From Lead Where Id In :leadIds]);
            }
            Map<Id, Contact> contactMap = null;
            if (contactIds != null && contactIds.size() > 0) {
                contactMap = new Map<Id, Contact>([Select Name From Contact Where Id In :contactIds]);
            }
            
            Map<Id, Account> accountMap = null;
            if (accountIds != null && accountIds.size() > 0) {
                accountMap = new Map<Id, Account>([Select Name From Account Where Id In :accountIds]);
            }
            
            Map<Id, Opportunity> oppMap = null;
            if (oppIds != null && oppIds.size() > 0) {
                oppMap = new Map<Id, Opportunity>([Select Name From Opportunity Where Id In :oppIds]);
            }
            
            Integer index = 0;
            while (index < trigger.new.size()) {
                Task ct = trigger.new[index];
                if (trigger.old[index].Status != 'Completed' && ct.Status == 'Completed' && ct.External_Assignee__c != null) {
                    Messaging.reserveSingleEmailCapacity(1);
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setToAddresses(new List<String>{ct.External_Assignee__c});
                    if (activeUser != null && activeUser.Email != null) {
                        mail.setReplyTo(activeUser.Email);
                        mail.setSenderDisplayName(UserInfo.getName());
                    } else {
                        mail.setReplyTo('sfdcsupport@telus.com');
                        mail.setSenderDisplayName('Salesforce Tasks');
                    }
                    
                    String relatedTo = '';
                
                    if (ct.WhatId != null) {
                        if (String.valueOf(ct.WhatId).substring(0, 3) == '001') {
                            if (accountMap.get(ct.WhatId) != null) {
                                relatedTo += accountMap.get(ct.WhatId).Name + ' (Account)';
                            }
                        } else if (String.valueOf(ct.WhatId).substring(0, 3) == '006') {
                            if (oppMap.get(ct.WhatId) != null) {
                                relatedTo += oppMap.get(ct.WhatId).Name + ' (Opportunity)';
                            }
                        }
                    }
                    
                    String rtWho = '';
                    if (ct.WhoId != null) {
                        if (String.valueOf(ct.WhoId).substring(0, 3) == '003') {
                            // Contact!
                            if (contactMap.get(ct.WhoId) != null) {
                                rtWho += contactMap.get(ct.whoId).Name + ' (Contact)';
                            }
                        } else if (String.valueOf(ct.WhoId).substring(0, 3) == '00Q') {
                            // Lead!
                            if (leadMap.get(ct.WhoId) != null) {
                                rtWho += leadMap.get(ct.whoId).Name + ' (Lead)';
                            }
                        }
                    }
                    
                    mail.setSubject('Task Completed: ' + ct.Subject);
                    mail.setPlainTextBody('Hello,\n\n' +
                         'The following task has been completed:\n\n' +
                         'Subject: ' + ct.Subject + '\n' +
                         'Related To: ' + relatedTo + '\n' +
                         'Name: ' + rtWho + '\n' +
                         'Status: ' + ct.Status + '\n' +
                         'Date Completed: ' + Date.today().format() + '\n' +
                         'Comments: ' + ct.Description + '\n\n' +
                         'For more details on this task, ' + eups.Base_URL__c + '/' + ct.Id + '\n'+
                         'If you are not a Salesforce user, more details are available here: ' + eups.Portal_URL__c + '/ExternalServicePortalTaskInformation?oid=' + ct.Id + '&tok=' + EncodingUtil.urlEncode(ct.Token__c, 'UTF-8'));
                    mail.setHtmlBody('Hello,<br/>\n<br/>\n' +
                         'The following task has been completed:<br/>\n<br/>\n' +
                         'Subject: ' + ct.Subject + '<br/>\n' +
                         'Related To: ' + relatedTo + '<br/>\n' +
                         'Name: ' + rtWho + '<br/>\n' +
                         'Status: ' + ct.Status + '<br/>\n' +
                         'Date Completed: ' + Date.today().format() + '<br/>\n' +
                         'Comments: ' + ct.Description + '<br/>\n<br/>\n' +
                         'For more details on this task, <a href="' + eups.Base_URL__c + '/' + ct.Id + '">click here.</a><br/>\n' +
                         'If you are not a Salesforce user, more details are available <a href="' + eups.Portal_URL__c + '/ExternalServicePortalTaskInformation?oid=' + ct.Id + '&tok=' + EncodingUtil.urlEncode(ct.Token__c, 'UTF-8') + '">here.</a>');
                    // Send the email you have created. 
                    messages.add(mail);
                }
                /* End External Assignee */
                index++;
            }
            if (messages != null && messages.size() > 0) { Messaging.sendEmail(messages); }
        }
    } catch (Exception e) { Database.rollback(sp); QuoteUtilities.log(e, false); trigger.new[0].addError(e.getMessage()); }
}