trigger setOwnerManagers on Lead (before insert, before update) {
    
    Set<Id> ownerIds = new Set<Id>();
    DataPrime__c dataPrimeSettings = DataPrime__c.getAll().get('Prime Lead Managers');
    Boolean prime = dataPrimeSettings == null ? false : dataPrimeSettings.Prime__c;
    System.debug('prime = ' + prime);
    
    if(trigger.IsInsert){
        for(Lead l : trigger.new) {
            if (l.isConverted) continue;
            ownerIds.add(l.ownerId);
        }
    }else{
        for(Lead l : trigger.new){
            if (l.isConverted) continue;
            if( prime || (l.ownerId != trigger.oldMap.get(l.Id).ownerId)) {
                ownerIds.add(l.ownerId);
            }
        }
    }
    if(!ownerIds.isEmpty()){
        OwnerManagerUpdates.setManagers(trigger.new, ownerids);
    }
}