trigger trac_TAS on Target_Account_Selling__c (before insert, before update) {

  trac_TAS.checkForDuplicates(trigger.new);
}