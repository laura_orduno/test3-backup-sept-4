trigger trac_RCID_CBUCID_NCSC_Indicator on AtoZ_Assignment__c (after insert, after update, before delete) {
    List<Id> rcidIds = new List<Id>();
    List<Id> cbucidIds = new List<Id>();
    if (trigger.isDelete) {
        for (AtoZ_Assignment__c a : trigger.old) {
            if (a.CBUCID__c != null) {
                cbucidIds.add(a.CBUCID__c);
            }
            if (a.RCID__c != null) {
                rcidIds.add(a.RCID__c);
            }
        }
    } else {
        for (AtoZ_Assignment__c a : trigger.new) {
            if (a.CBUCID__c != null) {
                cbucidIds.add(a.CBUCID__c);
            }
            if (a.RCID__c != null) {
                rcidIds.add(a.RCID__c);
            }
        }
    }
    AtoZ_Assignment__c[] allassignments = [Select Is_NCSC__c, RCID__c, CBUCID__c From AtoZ_Assignment__c Where RCID__c in :rcidIds OR CBUCID__c in :cbucidIds];
    Map<Id, List<AtoZ_Assignment__c>> rcidToAssignments = new Map<Id, List<AtoZ_Assignment__c>>();
    Map<Id, List<AtoZ_Assignment__c>> cbucidToAssignments = new Map<Id, List<AtoZ_Assignment__c>>();
    for (AtoZ_Assignment__c a : allassignments) {
        if (a.CBUCID__c != null) {
            if (!cbucidToAssignments.containsKey(a.CBUCID__c)) {
                cbucidToAssignments.put(a.CBUCID__c, new List<AtoZ_Assignment__c>());
            }
            cbucidToAssignments.get(a.CBUCID__c).add(a);
        }
        if (a.RCID__c != null) {
            if (!rcidToAssignments.containsKey(a.RCID__c)) {
                rcidToAssignments.put(a.RCID__c, new List<AtoZ_Assignment__c>());
            }
            rcidToAssignments.get(a.RCID__c).add(a);
        }
    }
    Account[] accounts = new List<Account>();
    Cust_Business_Unit_Cust_ID__c[] cbucids = new List<Cust_Business_Unit_Cust_ID__c>();
    if (rcidToAssignments != null && rcidToAssignments.size() > 0) {
        for (Id accountId : rcidToAssignments.keySet()) {
            AtoZ_Assignment__c[] assignments = rcidToAssignments.get(accountId);
            Account account = new Account(Id = accountId, Has_Direct_NCSC__c = false);
            for (AtoZ_Assignment__c ass : assignments) {
                if (ass.Is_NCSC__c != null && ass.Is_NCSC__c == true) {
                    if (trigger.isDelete && trigger.oldMap.containsKey(ass.Id)) {
                        // This record is being deleted! Don't count it!
                    } else {
                        account.Has_Direct_NCSC__c = true;
                        break;
                    }
                }
            }
            accounts.add(account);
        }
    }
    if (cbucidToAssignments != null && cbucidToAssignments.size() > 0) {
        for (Id cbucidId : cbucidToAssignments.keySet()) {
            AtoZ_Assignment__c[] assignments = cbucidToAssignments.get(cbucidId);
            Cust_Business_Unit_Cust_ID__c cbucid = new Cust_Business_Unit_Cust_ID__c(Id = cbucidId, Has_NCSC__c = false);
            for (AtoZ_Assignment__c ass : assignments) {
                if (ass.Is_NCSC__c != null && ass.Is_NCSC__c == true) {
                    if (trigger.isDelete && trigger.oldMap.containsKey(ass.Id)) {
                        // This record is being deleted! Don't count it!
                    } else {
                        cbucid.Has_NCSC__c = true;
                        break;
                    }
                }
            }
            cbucids.add(cbucid);
        }
    }
    update cbucids;
    update accounts;
}