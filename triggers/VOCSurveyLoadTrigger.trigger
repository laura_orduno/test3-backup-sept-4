trigger VOCSurveyLoadTrigger on VOC_Survey_Load__c (after insert) {
    system.schedulebatch(new VOCNewSurveyBatch(trac_Utilities.createObjectQuery('voc_survey__c')+' where sent__c=false'),''+system.now(),1);
}