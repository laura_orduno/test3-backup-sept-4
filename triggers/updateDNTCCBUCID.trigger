/*
Called on update of CBUCID DNTC Flag
1) Updates DNTC Flag in Accounts when Account Record Type changes.

Author - Tom Muzyka
Created - May 2010
*/
trigger updateDNTCCBUCID on Cust_Business_Unit_Cust_ID__c (after update) 
{

    Id accStandardRecId = '012R00000000O0Z';
    Id accDNTCRecId = '012R00000000O0U';
    Set<String> fieldsTracked = new Set<String> {'DNTC__c'};
    Set<String> CBUCIDs = new Set<String>();
    Set<Id> cbuIds = new Set<Id>();
    
    for (Cust_Business_Unit_Cust_ID__c cbu : trigger.new) 
    {
        if (Util.hasChanges(fieldsTracked, cbu, trigger.oldMap.get(cbu.Id))) {
            cbuIds.add(cbu.Id);
            CBUCIDs.add(cbu.CBUCID__c);
        }
    }

    if (cbuIds.isEmpty()) {
        return;
    }
    
    //Update CBUCID Fields
    List<Cust_Business_Unit_Cust_ID__c> cbuList = [Select Id
                                                         ,DNTC__c
                                                         ,Company_s_Total_Annual_Sales__c
                                                         ,Number_of_Employees_Here__c
                                                         ,Number_of_Employees_Total__c
                               from Cust_Business_Unit_Cust_ID__c where Id In :cbuIds];
    for (Cust_Business_Unit_Cust_ID__c cbu: cbuList)
    {
        if (cbu.DNTC__c) {
            if (cbu.Company_s_Total_Annual_Sales__c == null) {
               cbu.Company_s_Total_Annual_Sales__c = null;
            } else {
                cbu.Company_s_Total_Annual_Sales__c = null;
            }
            if (cbu.Number_of_Employees_Here__c == null) {
               cbu.Number_of_Employees_Here__c = null;
            } else {
                cbu.Number_of_Employees_Here__c = null;
            }
            if (cbu.Number_of_Employees_Total__c == null) {
               cbu.Number_of_Employees_Total__c = null;
            } else {
                cbu.Number_of_Employees_Total__c = null;
            }
        }
    }
    update cbuList;
    
    //Update Account DNTC flag  -- used only for initial load
    //List<Account> accList = [Select Id ,RecordTypeId ,DNTC__c ,CBUCID_NAME__r.DNTC__c
    //                         from Account where CBUCID__c In :CBUCIDs];
    //for (Account acc: accList)
    //{
    //    if (acc.CBUCID_NAME__r.DNTC__c) {
    //        acc.DNTC__c = TRUE;
    //        acc.RecordTypeId = accDNTCRecId;
    //    } else {
    //        acc.DNTC__c = FALSE;
    //        acc.RecordTypeId = accStandardRecId;
    //    }
    //}
    //update accList;
}