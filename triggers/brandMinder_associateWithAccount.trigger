trigger brandMinder_associateWithAccount on Case (before insert, before update) {
    /** REFACTORED BY DANE
    // get record type map
    Map<ID, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Case.getRecordTypeInfosById();
    // map to reference rcid to Account
    Map<String,Id> rcidToAccount = new Map<String,Id>();
    // set of RCIDs to be passed to SOQL query
    Set<String> rcids = new Set<String>();
        
    for(Case c : Trigger.new)
    {
    System.debug('Status : ' + c.Status);
        String recordTypeName = c.recordTypeId != null ? rtMap.get(c.recordTypeId).getName() : 'unknown';

        if(recordTypeName == 'SMB Care Technical Support' && c.brandminder__c == true && c.proactive_case__c == true)
        {
            if (c.rcid_for_account_lookup__c != null) { 
                rcids.add(c.rcid_for_account_lookup__c);
            } else {            
                c.addError('RCID is required!');
            }
        }    
    }
    
    if(!rcids.isEmpty())
    {        
        for(Account acct : [SELECT Id, rcid__c FROM Account WHERE rcid__c IN :rcids])
        {
            rcidToAccount.put(acct.RCID__c,acct.Id);
            system.debug('RCID#:'+acct.RCID__c+' AccountID:'+acct.Id);
        }
    }
    
    for(Case c : Trigger.new) {
        if(rcidToAccount.containsKey(c.rcid_for_account_lookup__c)){
            if(c.AccountId == null) c.AccountId = rcidToAccount.get(c.rcid_for_account_lookup__c);
        }
    } **/
}