trigger PopulatePatnerAccount on Opportunity (before insert, before update) {
    MapPartnerAccount.populateFieldOnObj(trigger.new);
}