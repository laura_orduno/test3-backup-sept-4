/*
    ###########################################################################
    # File..................: TBOBeforeInsertWorkRequest
    # Version...............: 1
    # Created by............: Umesh Atry (TechM)
    # Created Date..........: 17/02/2016 
    # Last Modified by......: Umesh Atry (TechM)
    # Last Modified Date....: 13/06/2016
    # Description...........: Trigger to validate that work request of a particular service type can only be made 
    # if an existing case account is present with the same service type. Also there can not be two work request for 
    # the same service type.
    #
    ############################################################################
    */

trigger TBOBeforeInsertWorkRequest on Work_Request__c(before insert,before update) {
  
    map<id,RecordType> TBORecordTypeMap =  new map<id,RecordType>([SELECT Id, Name, Developername from RecordType where Developername IN ('TBO_Work_Request','Legal_Name_Update') AND sobjecttype='Work_Request__c' limit 2]);
    map<id,Work_request__c> getMap = new map<id,Work_request__c>([select id, Service_type__c, case__c from Work_Request__c where Service_type__c <>'' and case__c !=null ]);
    List<case_account__c > existCaseAccount = new List<case_account__c >();
 
    public set<string> serviceTypeSet = new Set<String>();
   
    List<Work_Request__c> existwrList = new List<Work_Request__c>();
    set < id > cid = new set < id > ();
    set < id > workrequestId = new set < id > ();
    boolean exsitServiceTypeinCA = false;
    boolean existServiceTypeinWR = false;
   
    string svalue = '';
    for (Work_Request__c WorkReq: Trigger.new) {
    
                  
      // svalue = WorkReq.Service_type__c;
       
        cid.add(WorkReq.case__c);
    }
   //system.debug('svalue  size ='+svalue );
    
    
     /* List<Work_request__c> checkWorkList = [select id, Service_type__c, case__c from Work_Request__c where case__c in : cid];
      if(checkWorkList.size() == 1){
        checkWorkList.get(0).case__r.status = '';
      } */
    
    system.debug('getMap Size  ='+getMap.size());
    system.debug('getMap  ='+getMap);
    system.debug('cid =====>>>>>'+cid);
   
    for (case_account__c ca: [select Id, Case__c, Service_type__c from case_account__c where case__c IN :cid and Service_type__c <> '' ]) {
       
       existCaseAccount.add(ca);
   }
     
  
  
     

    for (Work_Request__c WorkR: Trigger.new) {
         
  
        if (TBORecordTypeMap.containskey(WorkR.recordtypeid)) {
            if(WorkR.Service_type__c == null){
              WorkR.addError(label.TBO_service_type_none_check);
              system.debug('Service type is null ====='+ WorkR.Service_type__c);
            }
            else{
              system.debug('existCaseAccount = '+existCaseAccount.size());
              system.debug('existwrList= '+existwrList.size());
             
              if (Trigger.isupdate && WorkR.Service_type__c != Trigger.oldmap.get(workR.id).Service_type__c){
                WorkR.addError('Service Type can not be updated' );
              }
              if (Trigger.isinsert && Trigger.isbefore){
              if(existCaseAccount!= null && getMap!= null){
                  for(case_account__c  ca : existCaseAccount){
                    SYSTEM.DEBUG('Service type is not null ====='+ WorkR.Service_type__c);
                    if(ca.Service_type__c.contains(WorkR.Service_type__c)){
                        exsitServiceTypeinCA = true;
                        system.debug('Service Type in  Case Account = '+WorkR.Service_type__c+ 'ca.Service_type__c = '+ca.Service_type__c );
                        break;
                    }
                   
                  }
                  if(exsitServiceTypeinCA){
                    for(Work_Request__c  wr : getMap.values()){
                    
                    //if(WorkR.service_type__c == wr.Service_type__c )
                        
                    
                    system.debug('Work Request List(Service List) = '+wr.Service_type__c);
                    system.debug(string.valueof(wr.Case__c) == string.valueof(cid));
                    system.debug('getmap Service type' + getMap.get(wr.id).Service_type__c );
                    system.debug('getMap.get(wr.id).Case__c'+getMap.get(wr.id).Case__c +'== wr.case__c =='+ WorkR.case__c);
                    system.debug('Work Request from list = '+wr.Service_type__c+ '>>> CAseId: '+WR.case__c +'>>> Id:'+wr.id);
                    system.debug('Current Work Request SR = '+WorkR.Service_type__c+ '>>> CAseId: '+WorkR.case__c+'>>> Id:'+WorkR.id);
                       if(wr.Service_type__c.contains(WorkR.Service_type__c) && getMap.get(wr.id).Case__c == WorkR.case__c ){
                       system.debug('Service Type in workRequest = '+wr.Service_type__c+ 'WorkR.Service_type__c = '+WorkR.Service_type__c);
                        existServiceTypeinWR = true;
                        system.debug('***** Found*****');
                          WorkR.addError(label.TBO_WorkRequestDuplicateMsg );
                        break;
                      }
                    }
                    }else{
                      WorkR.addError( label.TBO_WorkRequestNOCaseMsg  );   
                    }
                 
                
                   }
                }
            }
        }
    
    }
}