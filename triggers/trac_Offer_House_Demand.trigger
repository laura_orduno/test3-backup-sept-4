trigger trac_Offer_House_Demand on Offer_House_Demand__c (before insert) {
  
  trac_Offer_House_Demand.populateManagerDirector(trigger.new);
}