trigger voc_processRecords on VOC_Survey__c (before insert, after update) {
    if(trigger.isbefore){
        if(trigger.isinsert){
            map<string,voc_survey_emails__c> surveyEmailsMap=voc_survey_emails__c.getall();
            for (voc_survey__c survey:trigger.new) {
                if(survey.survey_type__c==null){
                    survey.adderror('Survey Type field required!');
                }
                if(string.isblank(survey.customer_email__c)){
                    survey.adderror('Customer email is required!');
                }
                if(string.isblank(survey.custom_id__c)){
                    survey.adderror('Customer ID is required!');
                }
                if(survey.survey_type__c!=null&&string.isnotblank(survey.customer_email__c)&&string.isnotblank(survey.custom_id__c)){
                    system.debug('Uploaded survey type:'+survey.survey_type__c);
                    if(surveyEmailsMap.containskey(string.valueof(integer.valueof(survey.survey_type__c)))){
                        survey.voc_survey_type__c=surveyEmailsMap.get(string.valueof(integer.valueof(survey.survey_type__c))).survey_type_id__c;
                    }else{
                        survey.adderror('Survey type is not found!  Survey type '+string.valueof(integer.valueof(survey.survey_type__c))+' was uploaded');
                    }
                }
            }
        }
    }else if(trigger.isafter){
        if(trigger.isupdate){
    		list<voc_survey__c> surveyHasManagerIdsOrEmails=new list<voc_survey__c>();
            for(voc_survey__c survey:trigger.new){
                if(survey.sent__c&&survey.manager_callback__c&&!trigger.oldmap.get(survey.id).manager_callback__c){
                    // Create tasks for these.
                    if(string.isnotblank(survey.manager_ids__c)||string.isnotblank(survey.manager_emails__c)){
                        surveyHasManagerIdsOrEmails.add(survey);
                    }
                }
            }
            if(!surveyHasManagerIdsOrEmails.isempty()){
                voc_processRecordsUtil processRequest=new voc_processRecordsUtil(surveyHasManagerIdsOrEmails);
                if(surveyHasManagerIdsOrEmails.size()>=200){
                    system.enqueueJob(processRequest);
                }else{
                    processRequest.runStandalone();
                }
            }
        }
    }
    /*
    if(trigger.isafter&&trigger.isinsert){
        voc_processRecordsUtil processRequest=new voc_processRecordsUtil(trigger.newmap.keyset(),surveyTypes,rcids,customIdToEmail,agentIds);
        system.enqueueJob(processRequest);
    }
    vocSurveyType = [SELECT Id, Survey_URL__c, Type__c FROM VOC_Survey_Type__c WHERE Type__c IN :surveyTypes];

    for(VOC_Survey_Type__c voc : vocSurveyType){
        typeToSurveyID.put(voc.Type__c,voc.Id);
        system.debug('Survey Type ID:'+voc.id+' Type #:'+voc.Type__c+' URL:'+voc.Survey_URL__c);
    }
    //Update Manager Email
    list<user> users=[select id,isactive,Team_TELUS_ID__c,employeeid__c,userrole.parentroleid from user where License_Type__c = 'Salesforce' and Team_TELUS_ID__c=:agentIds];
    for(user agent:users){
        agentParentRoleMap.put(agent.Team_TELUS_ID__c,agent.userrole.parentroleid);
        agentIdUserIdMap.put(agent.Team_TELUS_ID__c,agent.id);
        if(agent.isactive==true) activeAgentIdUserIdMap.put(agent.Team_TELUS_ID__c,agent.id);
    }
    // Setup parent role's email and ID map
    list<user> managers=[select id,email,userroleid from user where userroleid=:agentParentRoleMap.values()];
    for(user manager:managers){
        if(roleManagerEmailMap.containskey(manager.userroleid)){
            roleManagerIdMap.put(manager.userroleid,roleManagerIdMap.get(manager.userroleid)+','+manager.id);
            roleManagerEmailMap.put(manager.userroleid,roleManagerEmailMap.get(manager.userroleid)+','+manager.email);
        }else{
            roleManagerIdMap.put(manager.userroleid,manager.id);
            roleManagerEmailMap.put(manager.userroleid,manager.email);
        }
    }
    // Setup Agent Manager Map by using Parent Role's Email Map
    for(string agentId:agentParentRoleMap.keySet()){
        agentManagerIdMap.put(agentId,roleManagerIdMap.get(agentParentRoleMap.get(agentId)));
        agentManagerEmailMap.put(agentId,roleManagerEmailMap.get(agentParentRoleMap.get(agentId)));
    }
    list<VOC_Survey_Emails__c> vocSurveyEmails=VOC_Survey_Emails__c.getall().values();
    map<string,string> vocSurveyEmailsMap=new map<string,string>();
    for(VOC_Survey_Emails__c email:vocSurveyEmails){
        vocSurveyEmailsMap.put(email.name,email.emails__c);
    }

    rcidAccounts = [SELECT Id, Ownerid, RCID__c FROM Account WHERE RCID__c IN :rcids];

    for(Account acc : rcidAccounts){
        AccountToAccountOwner.put(acc.Id, acc.Ownerid);
        rcidToAccount.put(acc.RCID__c,acc.Id);
        rcidsFound.add(acc.RCID__c);
        system.debug('RCID#:'+acc.RCID__c+' AccountID:'+acc.Id);
    }

    rcidsNotFound = rcids.clone();
    rcidsNotFound.removeall(rcidsFound);
    system.debug('# of RCIDs:'+rcids.size());
    system.debug('# of RCIDs found:'+rcidsFound.size());
    system.debug('# of RCIDs not found:'+rcidsNotFound.size());
    system.debug('# of records without RCIDs:'+recordsWithoutRcids.size());

    for(String email : customIdToEmail.values()){
        emails.add(email);
    }
    contactIdToContact = new Map<Id,Contact>([SELECT Id, Email, FirstName, LastName, AccountId FROM Contact WHERE AccountId != null 
                                              AND (Email IN :customIdToEmail.values()
                                                   OR AccountId IN :rcidToAccount.values())
                                             ]);

    for(Contact con : contactIdToContact.values()){
        contactIdToAccount.put(con.Id,con.AccountId);
        if(con.Email!=null && con.Email!=''){
            emailToContact.put(con.Email,con.Id); //if multiple matches, last one wins
            emailsFound.add(con.Email);
        }
        else {
            noEmailContacts.add(con);
        }
    }
    
    //Associate survey type record to survey record
    // Added Update Manager Email
    if(Trigger.isBefore){
        for(VOC_Survey__c voc : trigger.new) {
            system.debug('voc.Survey_Type__c:'+voc.Survey_Type__c);
            system.debug('typeToSurveyID:'+typeToSurveyID);            
            if(voc.Survey_Type__c==null) voc.addError('Survey Type required!');
            else voc.VOC_Survey_Type__c = typeToSurveyID.get(voc.Survey_Type__c.intValue());
            if(voc.VOC_Survey_Type__c == null) voc.addError('Cannot find matching VOC Survey Type! Make sure it exists first.');

            voc.manager_callback__c = false;
            voc.Manager_Callback_Triggered__c = true;
            if(string.isnotblank(voc.manager_ids__c)||string.isnotblank(voc.manager_emails__c)){
                vocHasManagerIdsOrEmails.add(voc.id);
            }
            
            //Link the RCID account to survey, if no possible here, 2nd check at contacts level later
            if(rcidToAccount.containsKey(voc.RCID__c)){
                if(voc.Account__c == null) voc.Account__c = rcidToAccount.get(voc.RCID__c);
            }
            if(voc.Account__c != null){
                for(Contact con : contactIdToContact.values()){
                    if(con.AccountId == voc.Account__c && con.Email != null && con.Email.equals(voc.Customer_Email__c)){
                        if(voc.Contact_ID__c == null) voc.Contact_ID__c = con.Id;
                    }
                    if(voc.Account__c != null && con.AccountId == voc.Account__c && 
                        (
                            (con.FirstName!=null&&con.FirstName.equals('')&&
                            con.LastName!=null&&voc.Customer_Last_Name__c!=null&&
                            con.LastName.equals(voc.Customer_Last_Name__c))
                            ||
                            (con.FirstName!=null&&voc.Customer_First_Name__c!=null&&
                            con.FirstName.equals(voc.Customer_First_Name__c)&&
                            con.LastName!=null&&voc.Customer_Last_Name__c!=null&&
                            con.LastName.equals(voc.Customer_Last_Name__c))
                        )
                    ){
                        if(voc.Contact_ID__c == null) {
                            voc.Contact_ID__c = con.Id;
                            con.Email=voc.Customer_Email__c;
                        }
                    }
                }
            }
            if(voc.update_manager_emails__c==true){
                if(string.isnotblank(voc.agent_id__c)){
                    if(agentManagerEmailMap.containskey(voc.agent_id__c)){
                        voc.manager_ids__c=agentManagerIdMap.get(voc.agent_id__c);
                        voc.manager_emails__c=agentManagerEmailMap.get(voc.agent_id__c);
                        voc.agent_id_user__c=agentIdUserIdMap.get(voc.agent_id__c);
                    }else{
                        voc.manager_emails__c=vocSurveyEmailsMap.get(string.valueOf(voc.survey_type__c));
                    }
                }else{
                    voc.manager_emails__c=vocSurveyEmailsMap.get(string.valueOf(voc.survey_type__c));
                }
                voc.update_manager_emails__c=false;
            }
            if(activeAgentIdUserIdMap.get(voc.agent_id__c)!=null){
                voc.OwnerId = activeAgentIdUserIdMap.get(voc.agent_id__c);
            }
        }
    
        emailsNotFound = emails.clone();
        emailsNotFound.removeall(emailsFound);
        
        Map<Id, Id> accountIdsMappedToRcidAccountId = voc_ContactUtility.getAccountToRcidAccount(contactIdToContact.values());
        Contact newCon;
        
        for(String customId : customIdToEmail.keySet()){
            if(emailsFound.contains(customIdToEmail.get(customId))){
                //email matches existing contact with contact linkage to an RCID
                if(accountIdsMappedToRcidAccountId.get(contactIdToAccount.get(emailToContact.get(customIdToEmail.get(customId))))!=null){
                    //No RCID account linked on survey, use contact's RCID
                    if(customIdToVOC.get(customId).Account__c == null){
                        customIdToVOC.get(customId).Account__c = accountIdsMappedToRcidAccountId.get(contactIdToAccount.get(emailToContact.get(customIdToEmail.get(customId))));
                    //RCID account linked on survey, if same account rcid = contact rcid, link existing contact, else create new contact under account rcid
                    }else {
                        if(customIdToVOC.get(customId).Account__c == accountIdsMappedToRcidAccountId.get(contactIdToAccount.get(emailToContact.get(customIdToEmail.get(customId))))){
                            if(customIdToVOC.get(customId).Contact_ID__c == null){
                                customIdToVOC.get(customId).Contact_ID__c = emailToContact.get(customIdToEmail.get(customId));
                            }
                        }else{
                            if(customIdToVOC.get(customId).Contact_ID__c == null){
                                Contact tempCon;
                                if(AccountToAccountOwner.get(customIdToVOC.get(customId).Account__c)!=null){
                                    tempCon = new Contact(Phone=customIdToVOC.get
    
    (customId).Customer_Phone__c, Email=customIdToVOC.get(customId).Customer_Email__c, FirstName=customIdToVOC.get(customId).Customer_First_Name__c, 
    
    LastName=customIdToVOC.get(customId).Customer_Last_Name__c, AccountId=customIdToVOC.get(customId).Account__c);
                                } else {
                                    tempCon = new Contact(Phone=customIdToVOC.get(customId).Customer_Phone__c, Email=customIdToVOC.get
    
    (customId).Customer_Email__c, FirstName=customIdToVOC.get(customId).Customer_First_Name__c, LastName=customIdToVOC.get(customId).Customer_Last_Name__c, 
    
    AccountId=customIdToVOC.get(customId).Account__c);
                                }
                                contactsToAdd.put(customIdToVOC.get(customId).Custom_ID__c,tempCon);
                            }
                        }
                    }
                }
                //email matches existing contact with no linkage to an RCID.  Create new contact under RCID
                else {
                    list<VOC_Dummy_Account__c> vocDummyAccount = VOC_Dummy_Account__c.getall().values();
                    if(customIdToVOC.get(customId).Account__c == null){
                        customIdToVOC.get(customId).Account__c = vocDummyAccount.get(0).account_id__c;
                    }
                }
                if(customIdToVOC.get(customId).Contact_ID__c == null){
                    Contact tempCon;
                    if(AccountToAccountOwner.get(customIdToVOC.get(customId).Account__c)!=null){
                        tempCon = new Contact(Phone=customIdToVOC.get
    
    (customId).Customer_Phone__c, Email=customIdToVOC.get(customId).Customer_Email__c, FirstName=customIdToVOC.get(customId).Customer_First_Name__c, 
    
    LastName=customIdToVOC.get(customId).Customer_Last_Name__c, AccountId=customIdToVOC.get(customId).Account__c);
                    } else {
                        tempCon = new Contact(Phone=customIdToVOC.get(customId).Customer_Phone__c, Email=customIdToVOC.get(customId).Customer_Email__c, 
    
    FirstName=customIdToVOC.get(customId).Customer_First_Name__c, LastName=customIdToVOC.get(customId).Customer_Last_Name__c, AccountId=customIdToVOC.get
    
    (customId).Account__c);
                    }
                    contactsToAdd.put(customIdToVOC.get(customId).Custom_ID__c,tempCon);
                }
            }
            if(emailsNotFound.contains(customIdToEmail.get(customId))){
                Boolean found = false;
                for(Contact noemailcon : noEmailContacts){
                    if(customIdToVOC.get(customId).Account__c != null && 
                        customIdToVOC.get(customId).Account__c == noemailcon.AccountId && 
                        (
                            (noemailcon.FirstName!=null&&noemailcon.FirstName.equals('')&&
                            noemailcon.LastName!=null&&customIdToVOC.get(customId).Customer_Last_Name__c!=null&&
                            noemailcon.LastName.equals(customIdToVOC.get(customId).Customer_Last_Name__c))
                            ||
                            (noemailcon.FirstName!=null&&customIdToVOC.get(customId).Customer_First_Name__c!=null&&
                            noemailcon.FirstName.equals(customIdToVOC.get(customId).Customer_First_Name__c)&&
                            noemailcon.LastName!=null&&customIdToVOC.get(customId).Customer_Last_Name__c!=null&&
                            noemailcon.LastName.equals(customIdToVOC.get(customId).Customer_Last_Name__c))
                        )
                    ){                
                        found=true;
                        customIdToVOC.get(customId).Contact_ID__c = noemailcon.Id;
                        noemailcon.Email=customIdToVOC.get(customId).Customer_Email__c;
                    }
                }
                if(found==false){
                    list<VOC_Dummy_Account__c> vocDummyAccount = VOC_Dummy_Account__c.getall().values();
                    if(customIdToVOC.get(customId).Account__c == null) customIdToVOC.get(customId).Account__c = vocDummyAccount.get(0).account_id__c;
                    if(AccountToAccountOwner.get(customIdToVOC.get(customId).Account__c)!=null){
                        newCon = new Contact(Phone=customIdToVOC.get
    
    (customId).Customer_Phone__c, Email=customIdToVOC.get(customId).Customer_Email__c, FirstName=customIdToVOC.get(customId).Customer_First_Name__c, 
    
    LastName=customIdToVOC.get(customId).Customer_Last_Name__c, AccountId=customIdToVOC.get(customId).Account__c);
                    } else {
                        newCon = new Contact(Phone=customIdToVOC.get(customId).Customer_Phone__c, Email=customIdToVOC.get(customId).Customer_Email__c, 
    
    FirstName=customIdToVOC.get(customId).Customer_First_Name__c, LastName=customIdToVOC.get(customId).Customer_Last_Name__c, AccountId=customIdToVOC.get
    
    (customId).Account__c);
                    }
                    contactsToAdd.put(customId,newCon);
                }
            }
        }
    }
    /*
    if(trigger.isafter){
        if(trigger.isinsert){
            voc_processRecordsUtil processRequest=new voc_processRecordsUtil(trigger.newmap.keyset(),contactsToAdd,contactIdToContact,customIdToVOC);
            system.enqueueJob(processRequest);
        }else if(trigger.isupdate){
            if(!contactsToAdd.isempty()||!contactIdToContact.isempty()||!vocHasManagerIdsOrEmails.isempty()){
                voc_processRecordsUtil processRequest=new voc_processRecordsUtil(vocHasManagerIdsOrEmails,contactsToAdd,contactIdToContact,customIdToVOC);
                system.enqueueJob(processRequest);
            }
        }
    }
    /*
    if(Trigger.isBefore){
        insert contactsToAdd.values();
        update contactIdToContact.values();
    }
    for(String customId : contactsToAdd.keySet()){
        if(customIdToVOC.get(customId).Contact_ID__c == null) customIdToVOC.get(customId).Contact_ID__c = contactsToAdd.get(customId).Id;
    }
    if(Trigger.isAfter&&trigger.isinsert){
        voc_processRecordsUtil.postProcessSurveys(trigger.newmap.keyset());
        //RecordType rt = [SELECT Id FROM RecordType WHERE SObjectType = 'Task' AND DeveloperName = 'VOC_Manager_Callback_Task'];
        
        //Generate a map of tokens for the sObjects in your organization
        //Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();

        //Retrieve the describe result for the desired object
        //DescribeSObjectResult result = gd.get('Task').getDescribe();
        
        //Generate a map of tokens for all the Record Types for the desired object
        
        Map<String,Schema.RecordTypeInfo> recordTypeInfo = task.sobjecttype.getdescribe().getRecordTypeInfosByName();
        
        //Retrieve the record type id by name
        String taskRecordTypeId = recordTypeInfo.get('VOC Manager Callback Task').getRecordTypeId();
        
        String[] managerEmails = new String[]{};
        String[] managerIds = new String[]{};
        Set<Id> vocIdsToUpdate = new Set<Id>();
        for (VOC_Survey__c voc : trigger.new) {
            if (voc.manager_callback__c == true) {
                if(voc.Manager_IDs__c!=null) managerIds = voc.Manager_IDs__c.split(',', -2);
                if(voc.manager_emails__c!=null) managerEmails = voc.manager_emails__c.split(',', -2);
                for(String managerId : managerIds){
                    internalTasks.add(new Task(
                                    ActivityDate = Date.today().addDays(5),
                                    Subject = 'VOC Manager Callback Request',
                                    Description = 'Customer Comments:'+voc.Customer_Callback_Comments__c,
                                    WhoId = voc.Contact_ID__c,
                                    WhatId = voc.Account__c,
                                    RecordTypeId = taskRecordTypeId,
                                    OwnerId = managerId,
                                    VOC_Survey_ID__c = voc.id,                                    
                                    VOC_Survey__c = voc.Custom_ID__c,
                                    Status='Not Started'));
                    vocTasks.add(new VOC_Task_for_Email_Alert__c(
                        Agent_ID__c = voc.Agent_ID__c,
                        Business_Name__c = voc.Business_Name__c,
                        Case_Owner__c = voc.Case_Owner__c,
                        Case_Subject__c = voc.Case_Subject__c,
                        Customer_Callback_Comments__c = voc.Customer_Callback_Comments__c,
                        Customer_Callback_Email__c = voc.Customer_Callback_Email__c,
                        Customer_Callback_First_Name__c = voc.Customer_Callback_First_Name__c,
                        Customer_Callback_Last_Name__c = voc.Customer_Callback_Last_Name__c,
                        Customer_Callback_Phone_Number__c = voc.Customer_Callback_Phone_Number__c,
                        Customer_Email__c = voc.Customer_Email__c,
                        Customer_First_Name__c = voc.Customer_First_Name__c,
                        Customer_Last_Name__c = voc.Customer_Last_Name__c,
                        Customer_Phone__c = voc.Customer_Phone__c,
                        Custom_ID__c = voc.Custom_ID__c,
                        Interaction_Date__c = voc.Interaction_Date__c,
                        Location__c = voc.Location__c,
                        Manager_ID__c = managerId,
                        Preferred_Time_for_Contact__c = voc.Preferred_Time_for_Contact__c,
                        Reference__c = voc.Reference__c,
                        Ticket__c = voc.Ticket__c));
                }
                if(managerIds.size()==0){
                    for(String email : managerEmails){
                        externalTasks.add(new Task(
                                        External_Assignee__c = email,
                                        ActivityDate = Date.today().addDays(5),
                                        Subject = 'VOC Manager Callback Request',
                                        Description = 'Customer Comments:'+voc.Customer_Callback_Comments__c,
                                        WhoId = voc.Contact_ID__c,
                                        WhatId = voc.Account__c,
                                        RecordTypeId = taskRecordTypeId,
                                        OwnerId = voc.OwnerId,
                                        VOC_Survey_ID__c = voc.id,
                                        VOC_Survey__c = voc.Custom_ID__c,
                                        Status='Not Started'));
                        vocTasks.add(new VOC_Task_for_Email_Alert__c(
                            Agent_ID__c = voc.Agent_ID__c,
                            Business_Name__c = voc.Business_Name__c,
                            Case_Owner__c = voc.Case_Owner__c,
                            Case_Subject__c = voc.Case_Subject__c,
                            Customer_Callback_Comments__c = voc.Customer_Callback_Comments__c,
                            Customer_Callback_Email__c = voc.Customer_Callback_Email__c,
                            Customer_Callback_First_Name__c = voc.Customer_Callback_First_Name__c,
                            Customer_Callback_Last_Name__c = voc.Customer_Callback_Last_Name__c,
                            Customer_Callback_Phone_Number__c = voc.Customer_Callback_Phone_Number__c,
                            Customer_Email__c = voc.Customer_Email__c,
                            Customer_First_Name__c = voc.Customer_First_Name__c,
                            Customer_Last_Name__c = voc.Customer_Last_Name__c,
                            Customer_Phone__c = voc.Customer_Phone__c,
                            Custom_ID__c = voc.Custom_ID__c,
                            Interaction_Date__c = voc.Interaction_Date__c,
                            Location__c = voc.Location__c,
                            Manager_Email__c = email,
                            Preferred_Time_for_Contact__c = voc.Preferred_Time_for_Contact__c,
                            Reference__c = voc.Reference__c,
                            Ticket__c = voc.Ticket__c));
                    }
                }
                vocIdsToUpdate.add(voc.id);
                //voc.manager_callback__c = false;
                //voc.Manager_Callback_Triggered__c = true;
            }
        }
        List<VOC_Survey__c> vocsToUpdate = [SELECT Id, manager_callback__c, Manager_Callback_Triggered__c FROM VOC_Survey__c WHERE Id IN :vocIdsToUpdate];
        for(VOC_Survey__c voc : vocsToUpdate){
            voc.manager_callback__c = false;
            voc.Manager_Callback_Triggered__c = true;
        }
        update vocsToUpdate;
        
        insert internalTasks;
        insert externalTasks;
    
        ExternalUserPortal__c eups = ExternalUserPortal__c.getInstance();
        if(!Test.isRunningTest()) {
            System.assert(eups != null, 'Failed to load critical portal settings');
            System.assert(eups.HMAC_Digest_Key__c != null, 'Failed to load critical digest settings');
        }
        
        for(Task insertedTask : internalTasks){
            system.debug('internalTask task id:'+insertedTask.id);
            system.debug('internalTask task token:'+insertedTask.Token__c);
            for(VOC_Task_for_Email_Alert__c vocT : vocTasks){
                if(vocT.Custom_ID__c == insertedTask.VOC_Survey__c){
                    vocT.Task_ID__c = insertedTask.id;
                    vocT.External_Link__c = eups.Base_URL__c + '/' + insertedTask.Id;
                }
            }
        }
    
        for(Task insertedTask : externalTasks){
            taskIds.add(insertedTask.Id);
        }
        externalTasks = [SELECT Id, VOC_Survey__c, Token__c FROM Task WHERE Id IN :taskIds];
    
        for(Task insertedTask : externalTasks){
            system.debug('externalTask task id:'+insertedTask.id);
            system.debug('externalTask task token:'+insertedTask.Token__c);
            for(VOC_Task_for_Email_Alert__c vocT : vocTasks){
                if(vocT.Custom_ID__c == insertedTask.VOC_Survey__c){
                    vocT.Task_ID__c = insertedTask.id;
                    vocT.Token__c = insertedTask.Token__c; //only external needs token
                    vocT.External_Link__c = eups.Portal_URL__c + '/ExternalServicePortalTaskInformation?oid=' + insertedTask.Id + '&tok=' + 

EncodingUtil.urlEncode(insertedTask.Token__c, 'UTF-8');
                }
            }
        }
        
        insert vocTasks;
		
    } 
    */
}