trigger trac_AccountPlan on ESP__c (before delete) {

	List<Id> accountIds = new List<Id>();
	List<Id> accountPlanIds = new List<Id>();
	List<Id> departmentIds = new List<Id>();

	for(ESP__c plan : Trigger.old) {
		accountIds.add(plan.Account__c);
		accountPlanIds.add(plan.Id);
	}
	List<Department__c> departments = [SELECT Id FROM Department__c WHERE Account__c In :accountIds];
	for(Department__c department : departments) departmentIds.add(department.Id);

	List<Account_Map_Data__c> accountMapDataList = [SELECT Id FROM Account_Map_Data__c WHERE ESP__c IN :accountPlanIds OR Department__c IN :departmentIds];

	delete accountMapDataList;
	delete departments;
}