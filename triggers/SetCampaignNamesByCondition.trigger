/**
 * Resolves campaign name for campaign IDs specified as value on Condition objects.
 * Names are then stored in a custom fields for display.
 * 
 * @author Max Rudman
 * @since 4/5/2009
 */
trigger SetCampaignNamesByCondition on ADRA__Condition__c (before insert, before update) {
	// Build set of Conditions with changed value. 
	// Also build the set of referenced Campaign IDs
	List<ADRA__Condition__c> changed = new List<ADRA__Condition__c>();
	Set<Id> campaignIds = new Set<Id>();
	for (ADRA__Condition__c cond : Trigger.new) {
		if (Trigger.isUpdate) {
			ADRA__Condition__c old = Trigger.oldMap.get(cond.Id);
			if ((cond.ADRA__Value__c == null) || (cond.ADRA__Value__c == old.ADRA__Value__c)) {
				continue;
			}
		}
		changed.add(cond);
		if (cond.ADRA__Value__c != null) {
			String[] ids = cond.ADRA__Value__c.split(',');
			for (String id : ids) {
				id = id.trim();
				if (id.startsWith('701')) {
					campaignIds.add((Id)id);
				}
			}
		}
	}
	
	if (campaignIds.size() > 0) {
		// Load and index all campaigns at once
		Map<Id,Campaign> campaignsById = new Map<Id,Campaign>([SELECT Id, Name FROM Campaign WHERE Id IN :campaignIds]);
		for (ADRA__Condition__c cond : changed) {
			if (cond.ADRA__Value__c != null) {
				String[] ids = cond.ADRA__Value__c.split(',');
				String names = '';
				for (String id : ids) {
					id = id.trim();
					if (id.startsWith('701')) {
						Id campaignId = (Id)id;
						Campaign campaign = campaignsById.get(campaignId);
						if (campaign != null) {
							if (names != '') {
								names += ',';
							}
							names += campaign.Name;
						}
					}
				}
				cond.Campaign_Names__c = names;
			}
		}
	}
}