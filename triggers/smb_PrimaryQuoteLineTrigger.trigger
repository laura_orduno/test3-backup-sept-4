trigger smb_PrimaryQuoteLineTrigger on scpq__PrimaryQuoteLine__c (after insert) {

    if(Trigger.isAfter){
        if(Trigger.isInsert){           
            smb_CPQPrimaryQuoteLineTrigger_Helper.syncOpportunityLineItems(Trigger.new);
        }  
    }        
}