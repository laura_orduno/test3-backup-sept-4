trigger OCOM_Quote_Trigger on Quote (after insert, before update) 
{
    System.debug('######## OCOM_Quote_Trigger - Start ########');
    if(Trigger.isInsert && Trigger.isAfter)
    {
        List <Quote> quoteList = [select id, Name, Quote_Owner__c, ExpirationDate, Original_Quote_Id__c, Service_Address__c, Service_Address_Text__c, Status, Activities__c from Quote where id in :Trigger.new];
        System.debug('######## List of Quotes: '+ quoteList+ ' ########');
    
        for(Quote oneQuote : quoteList)
        {
            System.debug('######## Start processing Quote: ' + oneQuote.Name + ', '+ oneQuote.Id + ' ########');

            //New quote, so update Quote Owner with Quote Creator - QC53205 
            //for QC53428: oneQuote.Quote_Owner__c value could be the combination of some white space. So need to use String.isBlank method 
            //instead of just checking oneQuote.Quote_Owner__c is null or an empty string            
            if (String.isBlank(oneQuote.Quote_Owner__c))
            {
                System.debug('######## Set Quote Owner' + oneQuote.Name + ', '+ oneQuote.Id + ' ########');
                oneQuote.Quote_Owner__c = Trigger.newMap.get(oneQuote.id).CreatedById;
            }
           
                 
            if (String.isBlank(oneQuote.Original_Quote_Id__c))
            {                                
                newQuote(oneQuote);
            }           
            else if( oneQuote.Original_Quote_Id__c != oneQuote.Id)
            {              
                cloneQuote(oneQuote);
            }
        }
        
        update quoteList;
        
        System.debug('######## OCOM_Quote_Trigger - End ########');
    }
    
    private void newQuote(Quote aQuote)
    {
         //New quote, so update Original_Quote_Id__c with the Quote ID  
          System.debug('######## New Quote ' + aQuote.Name+ ', '+ aQuote.Id + ' ########');
          aQuote.Original_Quote_Id__c = aQuote.Id;
    }
    
    private void cloneQuote(Quote aQuote)
    {
          //Clone Quote: Clear the Service address fileds  
          System.debug('######## Clone Quote ' + aQuote.Name+ ', '+ aQuote.Id + ' ########');
          aQuote.Service_Address__c = null;
          aQuote.Service_Address_Text__c = '';
          aQuote.Maximum_Speed__c = '';
          aQuote.ExpirationDate = null;
          aQuote.Status= 'Draft'; 	
		  aQuote.RecordTypeId = [SELECT Id FROM RecordType where SobjectType = 'Quote' and name = 'Vlocity Quote'].id;
          aQuote.Activities__c = 'Cloned Quote;\r\n Pending:  Address validation; \r\n Action: Confirm Address;';     
    }
}