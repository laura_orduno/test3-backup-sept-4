/**
* Author Santosh Rath
* Single trigger that will handle all scenarios by using handler class.  Please do not
* build another trigger within same object to optimize SOQL and code reusability.
*/
trigger ContractRequestTrigger on Contracts__c (before insert,before update,after insert, after update) {    
    ContractRequestTriggerHandler handler=new ContractRequestTriggerHandler();      
}