trigger MBRAttachment on Attachment (after insert) {
    try{
        if(label.Apttus_Decommissions_Trigger_Switch.equalsignorecase('on')){
            if(trigger.isAfter && trigger.isInsert && MBRAttachmentTrigHelper.firstRun){
                MBRAttachmentTrigHelper.firstRun = false;
                MBRAttachmentTrigHelper.processNew(trigger.new);
            }
        }
    }catch(exception e){
        Util.emailSystemError(e);
    }
}