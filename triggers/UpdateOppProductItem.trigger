/*****************************************************
    Trigger Name: UpdateOppProductItem
    Usage: When opp stage changes, trigger an update on all associated opp prod items.
    Author – Clear Task    
    Date – 6/3/2011       
    Revision History
******************************************************/
trigger UpdateOppProductItem on Opportunity (after update) {
    /* Opportunity set in which stage is changed*/
    Set<Id> oppIds = new Set<Id>();
    
    for(Opportunity o :trigger.new){
        Opportunity oldOpp = System.Trigger.oldMap.get(o.Id);
        if( (o.StageName != null && oldOpp.StageName == null ) || 
                (o.StageName != null && oldOpp.StageName != null && !((o.StageName).equals(oldOpp.StageName)) ) ){
            oppIds.add(o.Id);   
        }
        else if(o.Probability != oldOpp.Probability){
            oppIds.add(o.Id);               
        }
    }
        
    /* collect Opportunity Product Items by oppIds*/  
    if(oppIds != null && oppIds.size()>0){
        List<Opp_Product_Item__c> oppProdItemsList = [Select o.Opportunity__c, o.Name, o.Id From Opp_Product_Item__c o 
                                                        where o.Opportunity__c in :oppIds];
        if(oppProdItemsList != null && oppProdItemsList.size()>0){            
            try{
                update oppProdItemsList;
            }catch(DMLException e){
                for(Integer i = 0; i < e.getNumDml(); i++) {
                    System.debug('message on DML:' + e.getDmlMessage(i)); 
                    System.debug('e:' + e); 
                }       
            }
        }
    }
}