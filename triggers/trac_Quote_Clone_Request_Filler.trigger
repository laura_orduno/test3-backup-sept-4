trigger trac_Quote_Clone_Request_Filler on Quote_Clone_Request__c (before insert, before update) {
    Id[] quoteIds = new List<Id>();
    Id[] requesterIds = new List<Id>();
    for (Quote_Clone_Request__c r : trigger.new) {
        quoteIds.add(r.Source_Quote__c);
        quoteIds.add(r.Destination_Quote__c);
        requesterIds.add(r.Requester__c);
        requesterIds.add(r.Cloner__c);
    }
    Map<Id, SBQQ__Quote__c> quoteMap = new Map<Id, SBQQ__Quote__c>(
        [Select Name, SBQQ__SalesRep__c, SBQQ__SalesRep__r.Name, SBQQ__SalesRep__r.Email From SBQQ__Quote__c Where Id in :quoteIds]
    );
    Map<Id, User> requesterMap = new Map<Id, User>(
        [Select Name, Email from User Where Id in :requesterIds]
    );
    for (Quote_Clone_Request__c r : trigger.new) {
        if (r.Source_Quote__c != null) {
            SBQQ__Quote__c quote = quoteMap.get(r.Source_Quote__c);
            if (quote != null) {
                r.Source_Quote_ID__c = quote.Id;
                r.Source_Quote_Owner__c = quote.SBQQ__SalesRep__c;
                r.Source_Quote_Owner_Name__c = quote.SBQQ__SalesRep__r.Name;
                r.Source_Quote_Owner_Email__c = quote.SBQQ__SalesRep__r.Email;
            } 
        }
        if (r.Destination_Quote__c != null) {
            SBQQ__Quote__c quote = quoteMap.get(r.Destination_Quote__c);
            if (quote != null) {
                r.Destination_Quote_ID__c = quote.Id;
                r.Destination_Quote_Owner__c = quote.SBQQ__SalesRep__c;
                r.Destination_Quote_Owner_Name__c = quote.SBQQ__SalesRep__r.Name;
                r.Destination_Quote_Owner_Email__c = quote.SBQQ__SalesRep__r.Email;
            } 
        }
        if (r.Requester__c != null) {
            User requester = requesterMap.get(r.Requester__c);
            if (requester != null) {
                r.Requester_Name__c = requester.Name;
                r.Requester_Email__c = requester.Email;
            }
        }
        if (r.Cloner__c != null) {
            User cloner = requesterMap.get(r.Cloner__c);
            if (cloner != null) {
                r.Cloner_Name__c = cloner.Name;
                r.Cloner_Email__c = cloner.Email;
            }
        }
    }
}