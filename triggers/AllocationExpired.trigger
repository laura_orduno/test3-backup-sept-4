/*
* Update allocations when expired flag is set to true. This is done to invoke the workflows.
* Author - Rucha Pradhan (Clear Task) - 07/26/2011
*/
trigger AllocationExpired on ADRA__Allocation__c (after update) {

    List<String> recsToUpdateIds = new List<String>();

    for(ADRA__Allocation__c alloc:Trigger.new){
        if(Trigger.oldMap.get(alloc.Id).ADRA__Expired__c == false && alloc.ADRA__Expired__c){
            recsToUpdateIds.add(alloc.Id);  
        }
    }   
    
    
    
    if(!recsToUpdateIds.isEmpty()){
        UpdateAllocationObjects.updateAllocations(recsToUpdateIds);
    }
}