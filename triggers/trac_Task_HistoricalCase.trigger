trigger trac_Task_HistoricalCase on Task (after insert) {

    Set<Id> WhatIds = new Set<Id>();
    Map<Id, Id> TaskMap = new Map<Id, Id>();
    
    for (Task t : trigger.new) {
        String sCaseId = t.WhatId;
        if (sCaseId != null && sCaseId.startsWith('500') == true) {
            WhatIds.Add(t.WhatId);
            TaskMap.put(t.WhatId, t.Id);
        }
    }
    
    if (WhatIds.size() == 0) {
        return;
    }
        
    List<Task> insTasks = new List<Task>();
    SMET_Request__c[] mySMETs = [SELECT Id, Historical_Case__c, OwnerId FROM SMET_Request__c WHERE Historical_Case__c IN :WhatIds];
    
    for (SMET_Request__c myS :mySMETs) {
        Task myTask = new Task();
        myTask.WhatId = myS.Id;
        //myTask.OwnerId = myS.OwnerId;
        myTask.Subject = 'Historical Case Activity Created';
        myTask.ActivityDate = Date.today();
        myTask.Description = 'Please see the following activity: https://login.salesforce.com/' + TaskMap.get(myS.Historical_Case__c);
        insTasks.Add(myTask);
    }
    
    insert insTasks;
}