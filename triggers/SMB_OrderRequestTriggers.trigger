trigger SMB_OrderRequestTriggers on Order_Request__c (after insert, after update) {
    if(Trigger.isAfter){
        
        if(Trigger.isInsert){
            SMB_OrderRequestHelper.updateOpportunityStatus(Trigger.new, false, null);
        }
        if(Trigger.isUpdate){
            SMB_OrderRequestHelper.updateOpportunityStatus(Trigger.new, true, Trigger.oldMap);
        }
    }
}