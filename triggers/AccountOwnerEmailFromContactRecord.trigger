/*****************************************************
    Trigger Name: AccountOwnerEmailFromContactRecord 
    Usage: TELUS - Field from Contact's Account Owner User Record
    Author – Allan Roszmann    
    Date – 11/01/2011       
    Revision History
******************************************************/
trigger AccountOwnerEmailFromContactRecord on Contact (before insert, before update){   
    if(trigger.isbefore){
        if(trigger.isupdate||trigger.isinsert){  
            //contact acctownerId set
            Set<Id> userIdSet = new Set<Id>();    
            for(Contact a :trigger.new){        
                userIdSet.add(a.Account_OwnerID__c);        
            }
            if(userIdSet != null){
                Map<Id, User> userMap = new Map<Id, User>([select Email from User where Id in :userIdSet]);
                System.debug('userMap =' +userMap );
                if(userMap != null){
                    for(Contact a :trigger.new){
                        User userObj = userMap.get(a.Account_OwnerID__c);
                        if(userObj != null){
                            a.Account_Owner_Email__c = userObj.Email ;
                        }
                    }
                }
            }
        }else if(trigger.isafter){
            if(trigger.isupdate){
                map<id,integer> activeAssociationMap=new map<id,integer>();
                aggregateresult[] results=[select contact__c,count(id) numAssociations from contact_linked_billing_account__c where active__c=true and contact__c!=null and contact__c=:trigger.new group by contact__c];
                for(aggregateresult result:results){
                    activeAssociationMap.put((string)result.get('contact__c'),(integer)result.get('numAssociations'));
                }
                for(Contact newContact:trigger.new){        
                    contact oldContact=(contact)trigger.oldmap.get(newContact.id);
                    if(oldContact.active__c&&!newContact.active__c){
                        if(activeAssociationMap.containskey(newContact.id)){
                            if(activeAssociationMap.get(newContact.id)>0){
                                newContact.adderror(label.Has_Active_Billing_Account_Associations);
                            }
                        }
                    }
                }
            }
        }
    }
}