<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>EasyDescribe__EasyDescribe</defaultLandingTab>
    <description>Etherios EasyDescribe - MetaData Viewer/Extractor</description>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>EasyDescribe</label>
    <tabs>EasyDescribe__EasyDescribe</tabs>
    <tabs>Custom_Ideas</tabs>
    <tabs>dsfs__DocuSignAccountConfiguration__c</tabs>
    <tabs>Sales_to_Care</tabs>
    <tabs>SRS_CRDB_Push_Tracker__c</tabs>
    <tabs>Opp_Product_Item__c</tabs>
    <tabs>vlocity_cmt__AttributeAssignment__c</tabs>
    <tabs>External_Event__c</tabs>
    <tabs>VL_Product_Translate__c</tabs>
</CustomApplication>
