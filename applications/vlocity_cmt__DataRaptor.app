<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>vlocity_cmt__DataRaptor</defaultLandingTab>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>Vlocity DataRaptor</label>
    <tabs>vlocity_cmt__DataRaptor</tabs>
    <tabs>vlocity_cmt__AttributeAssignment__c</tabs>
    <tabs>External_Event__c</tabs>
    <tabs>VL_Product_Translate__c</tabs>
</CustomApplication>
