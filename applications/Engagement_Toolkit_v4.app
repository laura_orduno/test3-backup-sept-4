<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>Astadia_Project__c</defaultLandingTab>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>Engagement Toolkit v4</label>
    <tabs>Astadia_Project__c</tabs>
    <tabs>Astadia_Milestone__c</tabs>
    <tabs>Astadia_Risk_and_Issues__c</tabs>
    <tabs>Astadia_Action_Item__c</tabs>
    <tabs>Astadia_High_Level_Requirement__c</tabs>
    <tabs>Astadia_Detailed_Requirements__c</tabs>
    <tabs>standard-Workspace</tabs>
    <tabs>Astadia_Component__c</tabs>
    <tabs>Astadia_Phase__c</tabs>
    <tabs>standard-report</tabs>
    <tabs>standard-Dashboard</tabs>
    <tabs>Astadia_Decision_Log__c</tabs>
    <tabs>Astadia_Change_Order__c</tabs>
    <tabs>dsfs__DocuSignAccountConfiguration__c</tabs>
    <tabs>Sales_to_Care</tabs>
    <tabs>SRS_CRDB_Push_Tracker__c</tabs>
    <tabs>Opp_Product_Item__c</tabs>
    <tabs>vlocity_cmt__AttributeAssignment__c</tabs>
    <tabs>External_Event__c</tabs>
    <tabs>VL_Product_Translate__c</tabs>
</CustomApplication>
