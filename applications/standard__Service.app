<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <tabs>standard-Chatter</tabs>
    <tabs>standard-File</tabs>
    <tabs>standard-Account</tabs>
    <tabs>standard-Contact</tabs>
    <tabs>standard-Case</tabs>
    <tabs>standard-Solution</tabs>
    <tabs>standard-report</tabs>
    <tabs>standard-Dashboard</tabs>
    <tabs>standard-Document</tabs>
    <tabs>Custom_Ideas</tabs>
    <tabs>dsfs__DocuSignAccountConfiguration__c</tabs>
    <tabs>Sales_to_Care</tabs>
    <tabs>SRS_CRDB_Push_Tracker__c</tabs>
    <tabs>Opp_Product_Item__c</tabs>
    <tabs>vlocity_cmt__AttributeAssignment__c</tabs>
    <tabs>External_Event__c</tabs>
    <tabs>VL_Product_Translate__c</tabs>
</CustomApplication>
