baseCtrl.prototype.$scope.stepIsActive = function() {

var i = 0;

var iframe = $('.ocom-billingl-details');

for(i=0;i<iframe.length;i++)
{
    if(iframe[i].src.indexOf('OC_SendForEsignature') !== -1)
    {
        //iframe[i].contentWindow.location.reload(true);
           if( window.localStorage )
               {
                  /* if( !localStorage.getItem('firstLoad') )
                   {
                       localStorage['firstLoad'] = true;
                       iframe[i].contentDocument.location.reload(true); 
                   }  
                   else*/
                      
                       localStorage.removeItem('firstLoad');
               }
           
     }
 }
}

baseCtrl.prototype.isAnyKeyContains = function(obj, text) {

  return obj === Object(obj)  && !!Object.keys(obj).find(function(key) {
     return !!~key.indexOf(text) && obj[key];
  })
}


baseCtrl.prototype.OpenModal = function(url, tabLabel)
{    
       sforce.console.getEnclosingPrimaryTabId(function(result) {
                    sforce.console.openSubtab(result.id, url, true, tabLabel, null, null, null);
                });
};



baseCtrl.prototype.$scope.validateSve = function(opration, selectedItem, noerror, elment, Index, cp)
{    
    
       //alert('remoting reponse' + baseCtrl.prototype.$scope.bpTree.response);
    if( noerror !== false ){
         baseCtrl.prototype.$scope.bpTree.showNewContact = false;
         }

};
baseCtrl.prototype.$scope.bpTree.propSetMap.saveObjectId = baseCtrl.prototype.$scope.bpTree.response.ContractReqId;
if(window.location.href.indexOf("OmniScriptInstanceId") > -1)
{

var element;
var element1;

//Added by Jaya for fixing Customer & Telus Signor
var elementCustSignorIterator;
var elementTelusSignorIterator;

var elementCreateContractRemote;
var elementWorkRequestRemote;
var elementWorkRequestTable;
var elementDRGetOrderInfo;
//Added by Jaya for fixing Customer & Telus Signor
var elementSetValuesCustomerSignor;
var elementSetValuesTelusSignor;

for(element in baseCtrl.prototype.$scope.bpTree.children)
{
    if(baseCtrl.prototype.$scope.bpTree.children[element] != null)
    {
        if(baseCtrl.prototype.$scope.bpTree.children[element].name == 'GetOrderInfo')
        {                          
             elementDRGetOrderInfo = baseCtrl.prototype.$scope.bpTree.children[element];
        }
        else if(baseCtrl.prototype.$scope.bpTree.children[element].name == 'ContractSignor')
        {
              
              for(elementCustSignorIterator in baseCtrl.prototype.$scope.bpTree.children[element].children)
              {
                     if(baseCtrl.prototype.$scope.bpTree.children[element].children[elementCustSignorIterator] != null)
                     {
                         if(baseCtrl.prototype.$scope.bpTree.children[element].children[elementCustSignorIterator].eleArray["0"].name == 'CustomerSignor')
                         {
                                  elementSetValuesCustomerSignor = baseCtrl.prototype.$scope.bpTree.children[element].children[elementCustSignorIterator];
                                   break;
                         }
                       }
                  }
         }
        else if(baseCtrl.prototype.$scope.bpTree.children[element].name == 'TelusSignor')
        {
              
              for(elementTelusSignorIterator in baseCtrl.prototype.$scope.bpTree.children[element].children)
              {
                     if(baseCtrl.prototype.$scope.bpTree.children[element].children[elementTelusSignorIterator] != null)
                     {
                         if(baseCtrl.prototype.$scope.bpTree.children[element].children[elementTelusSignorIterator].eleArray["0"].name == 'TelusApprover')
                         {
                                  elementSetValuesTelusSignor = baseCtrl.prototype.$scope.bpTree.children[element].children[elementTelusSignorIterator];
                                   break;
                         }
                       }
                  }
         }
        else if(baseCtrl.prototype.$scope.bpTree.children[element].name == 'CreateContractRemote')
        {               
              elementCreateContractRemote = baseCtrl.prototype.$scope.bpTree.children[element];                                          
        }
        else if(baseCtrl.prototype.$scope.bpTree.children[element].name == 'GetWorkRequestsAction')
        {
              elementWorkRequestRemote = baseCtrl.prototype.$scope.bpTree.children[element];
        }
        else if(baseCtrl.prototype.$scope.bpTree.children[element].name == 'ValidateContract')
        {
              
              for(element1 in baseCtrl.prototype.$scope.bpTree.children[element].children)
              {
                     if(baseCtrl.prototype.$scope.bpTree.children[element].children[element1] != null)
                     {
                         if(baseCtrl.prototype.$scope.bpTree.children[element].children[element1].eleArray["0"].name == 'WorkRequestTable')
                         {                  
                                  
                              elementWorkRequestTable = baseCtrl.prototype.$scope.bpTree.children[element].children[element1];
                               break;
                         }
                     }
              }
        }
    }
}

baseCtrl.prototype.$scope.callBackWorkRequest = function(opration, selectedItem, noerror, elment, Index, cp)
{
     
     baseCtrl.prototype.$scope.remoteCallInvoke(baseCtrl.prototype.$scope.bpTree.response, elementWorkRequestRemote, true,null,baseCtrl.prototype.$scope,null,null,baseCtrl.prototype.$scope.callBackSetValues,null,null,null,null);
};

baseCtrl.prototype.$scope.callBackSetValues = function(opration, selectedItem, noerror, elment, Index, cp)
{
        
        //alert('inside set values call back');
        baseCtrl.prototype.$scope.setElementValue(elementWorkRequestTable, 
                          baseCtrl.prototype.$scope.bpTree.response.WorkRequestDetails, true);
};

baseCtrl.prototype.$scope.drExtractInvoke(elementDRGetOrderInfo, true);

//Added by Jaya for fixing Customer & Telus Signor
baseCtrl.prototype.$scope.setElementValue(elementSetValuesCustomerSignor, 
                          baseCtrl.prototype.$scope.bpTree.response.OrderContact, true);

baseCtrl.prototype.$scope.setElementValue(elementSetValuesTelusSignor, 
                          baseCtrl.prototype.$scope.bpTree.response.DBTelusSignor, true);

    //Added by Jaya for calling the generic invoke
    baseCtrl.prototype.$scope.remoteCallInvoke(baseCtrl.prototype.$scope.bpTree.response, elementCreateContractRemote, true,null,baseCtrl.prototype.$scope,null,null,null,null,null,null,null);

    //call back for Work Request
    var inputWorkRequest = baseCtrl.prototype.$scope.bpTree.response;
    var sClassNameWorkRequest;
    var sMethodNameWorkRequest;
    var iTimeoutWorkRequest;
    var optionWorkRequest;

    sClassNameWorkRequest = elementWorkRequestRemote.propSetMap.remoteClass;
    sMethodNameWorkRequest = elementWorkRequestRemote.propSetMap.remoteMethod;
    iTimeoutWorkRequest = elementWorkRequestRemote.propSetMap.remoteTimeout;
    optionWorkRequest = elementWorkRequestRemote.propSetMap.remoteOptions;

    if(!(elementWorkRequestRemote.propSetMap.preTransformBundle || elementWorkRequestRemote.propSetMap.transformBundle) )    
    inputWorkRequest = baseCtrl.prototype.$scope.getSendResponseJSON(inputWorkRequest, elementWorkRequestRemote.propSetMap.sendJSONPath, elementWorkRequestRemote.propSetMap.sendJSONNode); 

    if(baseCtrl.prototype.$scope.bpService.placeholderEleTypeList.indexOf(elementWorkRequestRemote.type) >= 0)
    {
        optionWorkRequest.vlcJSONNode = elementWorkRequestRemote.name;
        if(elementWorkRequestRemote.JSONPath)
            optionWorkRequest.jsonPath = elementWorkRequestRemote.JSONPath;  
    }
    if(elementWorkRequestRemote.type === 'Filter Block')
        optionWorkRequest.vlcFilters = elementWorkRequestRemote.vlcJSONPath;

    baseCtrl.prototype.$scope.bpService.GenericInvoke(sClassNameWorkRequest, sMethodNameWorkRequest, angular.toJson(inputWorkRequest), angular.toJson(optionWorkRequest), iTimeoutWorkRequest, {label:null}).then(
       function(result) {
            var resp = angular.fromJson(result), hasError = false, errorMsg='';
            //alert(resp);
           
            angular.forEach(resp.messages, function(message) {
                if (message.severity === 'ERROR') {
                    hasError = true;
                    errorMsg = message.message;
                }
            });
            if(hasError) 
            {
               baseCtrl.prototype.$scope.handleRemoteCallError(null, errorMsg, true, false);
               return;
             }
             //alert('inside set values call back');
             baseCtrl.prototype.$scope.handleRemoteCallSuccess(elementWorkRequestRemote, resp, true, true, null, null, baseCtrl.prototype.$scope, null, null, null, null, null);

             baseCtrl.prototype.$scope.bpTree.response.WorkRequestDetails = resp.WorkRequestDetails;
baseCtrl.prototype.$scope.bpTree.response.IsWorkRequestExists = resp.IsWorkRequestExists;
baseCtrl.prototype.$scope.bpTree.response.closedWorkRequestExists = resp.closedWorkRequestExists;
baseCtrl.prototype.$scope.bpTree.response.closedWorkRequestStatus = resp.closedWorkRequestStatus;

        baseCtrl.prototype.$scope.setElementValue(elementWorkRequestTable.eleArray[0], 
                          baseCtrl.prototype.$scope.bpTree.response.WorkRequestDetails, true);

                                    });
                     
}



baseCtrl.prototype.onSelectItemCustomized = function(control, option, index, scp, bFlip)
        {
               
         
            if(control === undefined || control === null || option === undefined || option === null)
                return;
            // multi-select, single select
            var bSetVal = true;
            var response = [];
            if(bFlip)
            {
                if(option.vlcSelected === undefined || option.vlcSelected === null)
                {
                    option.vlcSelected = bSetVal;
                }
                else
                {
                    bSetVal = !option.vlcSelected;
                    option.vlcSelected = bSetVal;
                }
            }

            // update 'Selectable Items' response
            var recSet = control.vlcSI[control.itemsKey];
            // HP bug, use $$hashKey to check
            var isValid = true;
            for(var i=0; i<recSet.length; i++)
            {
                if(option.$$hashKey !== recSet[i].$$hashKey && bFlip && bSetVal && control.propSetMap.selectMode === 'Single')
                     recSet[i].vlcSelected = false;
                
            
                if(recSet[i].vlcSelected === true)
                {
                   
                    response.push(recSet[i]);
                        
                }
                
                if(scp.bpTree.response.ContractRequestObj.CustomerSignor==recSet[i].Id)
                {
                    recSet[i].vlcSelected=true;
                }
                else
                {
                    recSet[i].vlcSelected=false;
                }

            }
            if(response.length > 0)
                scp.bpTree.response.selectedCustomerContact = response;
            else
                scp.bpTree.response.selectedCustomerContact = null;

            if(scp.bpTree.propSetMap.dataJSON === true)
                scp.aggregate(scp, control.index, control.indexInParent, true, -1);

            
        };

baseCtrl.prototype.onSelectItemCustomized1 = function(control, option, index, scp, bFlip)
        {
           
            if(control === undefined || control === null || option === undefined || option === null)
                return;
            // multi-select, single select
            var bSetVal = true;
            var response = [];
            if(bFlip)
            {
                if(option.vlcSelected === undefined || option.vlcSelected === null)
                {
                    option.vlcSelected = bSetVal;
                }
                else
                {
                    bSetVal = !option.vlcSelected;
                    option.vlcSelected = bSetVal;
                }
            }

            // update 'Selectable Items' response
            var recSet = control.vlcSI[control.itemsKey];
            // HP bug, use $$hashKey to check
            var isValid = true;
            for(var i=0; i<recSet.length; i++)
            {
                if(option.$$hashKey !== recSet[i].$$hashKey && control.propSetMap.selectMode === 'Single')
                     recSet[i].vlcSelected = false;
                
            
                if(recSet[i].vlcSelected === true)
                {
                   
                    response.push(recSet[i]);
                        
                }
                
           }

            if(response.length > 0)
                scp.bpTree.response.selectedCustomerContact = response;
            else
                scp.bpTree.response.selectedCustomerContact = null;

            if(scp.bpTree.propSetMap.dataJSON === true)
                scp.aggregate(scp, control.index, control.indexInParent, true, -1);

            
        };

        baseCtrl.prototype.onSelectTelusSignor = function(control, option, index, scp, bFlip)
        {
            
            if(control === undefined || control === null || option === undefined || option === null)
                return;
            // multi-select, single select
            var bSetVal = true;
            var response = [];
            if(bFlip)
            {
                if(option.vlcSelected === undefined || option.vlcSelected === null)
                {
                    option.vlcSelected = bSetVal;
                }
                else
                {
                    bSetVal = !option.vlcSelected;
                    option.vlcSelected = bSetVal;
                }
            }

            // update 'Selectable Items' response
            var recSet = control.vlcSI[control.itemsKey];
            // HP bug, use $$hashKey to check
            var isValid = true;
            for(var i=0; i<recSet.length; i++)
            {
                if(option.$$hashKey !== recSet[i].$$hashKey && bFlip && bSetVal && control.propSetMap.selectMode === 'Single')
                     recSet[i].vlcSelected = false;
                
            
                if(recSet[i].vlcSelected === true)
                {
                   
                    response.push(recSet[i]);
                        
                }

                if(scp.bpTree.response.ContractRequestObj.TelusSignor==recSet[i].Id)
                {
                    recSet[i].vlcSelected=true;
                }
                else
                {
                    recSet[i].vlcSelected=false;
                }

            }
            if(response.length > 0)
                scp.bpTree.response.selectedTelusSignor = response;
            else
                scp.bpTree.response.selectedTelusSignor = null;

            if(scp.bpTree.propSetMap.dataJSON === true)
                scp.aggregate(scp, control.index, control.indexInParent, true, -1);

            
        };

baseCtrl.prototype.onSelectTelusSignor1 = function(control, option, index, scp, bFlip)
        {
   
            if(control === undefined || control === null || option === undefined || option === null)
                return;
            // multi-select, single select
            var bSetVal = true;
            var response = [];
            if(bFlip)
            {
                if(option.vlcSelected === undefined || option.vlcSelected === null)
                {
                    option.vlcSelected = bSetVal;
                }
                else
                {
                    bSetVal = !option.vlcSelected;
                    option.vlcSelected = bSetVal;
                }
            }

            // update 'Selectable Items' response
            var recSet = control.vlcSI[control.itemsKey];
            // HP bug, use $$hashKey to check
            var isValid = true;
            for(var i=0; i<recSet.length; i++)
            {
                if(option.$$hashKey !== recSet[i].$$hashKey && control.propSetMap.selectMode === 'Single')
                     recSet[i].vlcSelected = false;
                
            
                if(recSet[i].vlcSelected === true)
                {
                   
                    response.push(recSet[i]);
                        
                }
           }

            if(response.length > 0)
                scp.bpTree.response.selectedTelusSignor = response;
            else
                scp.bpTree.response.selectedTelusSignor = null;

            if(scp.bpTree.propSetMap.dataJSON === true)
                scp.aggregate(scp, control.index, control.indexInParent, true, -1);

            
        };