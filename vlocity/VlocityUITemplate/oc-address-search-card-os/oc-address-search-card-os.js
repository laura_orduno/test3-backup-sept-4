// /* Please use vlocity.cardframework.registerModule to register your Angular Controller i.e. vlocity.cardframework.registerModule.controller('TestController', ['$scope', function($scope) { ... }]); */

// vlocity.cardframework.registerModule.controller('OCAddressSearchController', ['$rootScope','$scope','$sldsTypeahead','$sldsPrompt','$sldsToast','$filter','$timeout', '$q','$http', 'PromiseQueueFactory','dataService','dataSourceService', function($rootScope, $scope, $sldsTypeahead, $sldsPrompt, $sldsToast, $filter, $timeout, $q, $http, PromiseQueueFactory, dataService, dataSourceService) { 
    
//     $scope.typeaheadTemplate = '<div class="slds-lookup__menu slds-size--1-of-1" ng-show="$isVisible()" style="margin-top:-16px; border-radius: 2px; box-shadow: 0px 2px 5px #ebedef">'+
//             '<ul tabindex="-1" class="slds-lookup__list" role="menu">'+
//                 '<li ng-repeat="match in $matches" ng-class="{active: $index == $activeIndex}">'+
//                     '<a href="javascript:void(0);" class="slds-lookup__item-action" role="menuitem" tabindex="-1" ng-click="selectAddress(match, $index, $event)" >'+
//                         '<div class="slds-col slds-size--6-of-12 slds-p-horizontal--x-small slds-truncate oc-addr-search-dropdown-bold" title="{{match.label}}">{{match.label}}</div>'+
//                     '</a>'+
//                 '</li>'+
//             '</ul>'+
//         '</div>';
        
    
//     $scope.selectAddress = function(address) {
//         $scope.selectedAddress = address;
//         console.log('Selected Address', address);
//         var params = {};
//         params.id = address.orderId;
//         params.cartId = address.orderId;
//         $rootScope.$broadcast('refreshLayout', params);
//     } 
    
//     $scope.selectAddressfromId = function(id) {
//         for (var i = 0; i < $scope.serviceAddresses.length; i++) {
//             if ($scope.serviceAddresses[i].orderId.toLowerCase().includes(id.toLowerCase())) {
//                 $scope.selectAddress($scope.serviceAddresses[i]);   
//             }
//         }
//     }
    
//     $scope.initAddress = function() {
//         if ($scope.obj && $scope.obj.CustomerOrderLocations){
//             $scope.serviceAddresses = $scope.obj.CustomerOrderLocations;
//             if (!$scope.params.id || $scope.params.id == '') {
//                 if ($scope.serviceAddresses) {
//                     $scope.selectAddress($scope.serviceAddresses[0]);
//                 }
//             } else {
//                 $scope.selectAddressfromId($scope.params.id);
//             }
//         }
//     }
    
//     $scope.getAddresses = function() {
//         if ($scope.records && $scope.records.CustomerOrderLocations) {
//           $scope.serviceAddresses = $scope.records.CustomerOrderLocations;
//           console.log('Service Addresses', $scope.serviceAddresses);
//           return;
//         }
//         var deferred = $q.defer();
//         var datasource = {};
//         datasource.type = 'ApexRemote';
//         datasource.value = {};
//         datasource.value.remoteClass = 'VlocityCPQUtil';
//         datasource.value.remoteMethod = 'getCustomerOrdersLocation';
//         datasource.value.inputMap = {'customerSolutionId': '801c000000034VyAAI'};
//         datasource.value.apexRemoteResultVar = 'result.records';
//         datasource.value.methodType = 'GET';
//         datasource.value.apexRestResultVar = 'result.records';
//         dataSourceService.getData(datasource, $scope, null).then(
//             function(data) {
//                 console.log('Address test data', data);
//                 if (data && data.CustomerOrderLocations){
//                     $scope.serviceAddresses = data.CustomerOrderLocations;
//                 }
//                 deferred.resolve(data);
//             }, function(error) {
//                 deferred.reject(error);
//             });

//         return deferred.promise;
//     }
        
//     $scope.initAddress();  

    
// }]);