<apex:page docType="html-5.0" controller="VITILcareLoginCtlr" showHeader="false" sidebar="false" standardStylesheets="false" applyHtmlTag="false" applyBodyTag="false" title="{!$Label.MBRSIgnIn}">
    <html lang="en" class="">

    <head>
        <c:VITILcareHead />

        <apex:stylesheet value="{!URLFOR($Resource.VITILcareCSS, 'css/login.css')}" />
        <apex:includeScript value="{!URLFOR($Resource.VITILcarejs, 'js/login.js')}" />
        <apex:includeScript value="{!URLFOR($Resource.MBRTracResources, 'js/vcenter.js')}" />

        
        <script language="JavaScript" type="text/javascript" src="{!URLFOR($Resource.scode_vitil)}"></script>
        <script language="JavaScript" type="text/javascript">
        /* You may give each page an identifying name, server, and channel on the next lines. */

        $(document).ready(function() {
            s.pageName = "VITILcare - Customer Login";
            s.server = "force.com";
            s.channel = "";
            s.events = "event1";
            /************ DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/
            <!-- var s_code = s.t();
            if (s_code) document.write(s_code) -->
        });
        </script>

    </head>

    <body class="overview base-quicklinks ">
        <c:VITILcareHeader />
        <div class="grid cci-authentication" id="cci-container">
            <div id="cci-content" class="box-shadow-small background-white">
                <div class="cci-authentication-left panel panel-left no-margin border-no-radius box-shadow-none border-none border-right full-height">
                    <div class="panel-body">
                        <h2 class="panel-header color-dark-purple">V<font color="#66cc00">ITIL</font>care | Soutien V<font color="#66cc00">ITIL</font>
                            <span class="beta-badge hidden">BETA</span>
                        </h2>

                        <p class="box-body-paragraph medium-font half-width left">
                            <apex:outputtext escape="false" value="{!$Label.VITILcareLoginWelcome} | {!$Label.VITILcareLoginWelcomeFR} " />
                        </p>

                        <p class="box-body-paragraph medium-font half-width left">
                            <img src="{!URLFOR($Resource.VITILcareTelusImages, 'lovbirds.png')}" alt="love bird" aria-hidden="true" />
                        </p>

                    </div>
                </div>


             <div class="cci-authentication-right panel panel-right no-margin border-no-radius border-none box-shadow-none border-left full-height">
                    <div class="panel-body">
                        <style>
                        @media (min-width: 768px) {
                            .mobile-header {
                                display: none;
                            }
                        }
                        @media (max-width: 768px) {
                            .mobile-header {
                                font-size: 36px !important;
                            }
                        }
                        </style>

                        <h2 class="mobile-header panel-header color-dark-purple">V<font color="#66cc00">ITIL</font>care | Soutien V<font color="#66cc00">ITIL</font>
                        </h2>

                        <h2 class="panel-header-medium">{!$label.VITILcareSignInTitle} | {!$Label.VITILcareSIgnInTitleFR}</h2>

                        <apex:outputPanel rendered="{!LEN(loginError)!=0}" styleClass="i-form-error-general form-error block">
                            <p class="no-margin no-padding">{!loginError}</p>
                        </apex:outputPanel>

                        <apex:form forceSSL="true">
                            <div class="form-group">
                                <apex:outputLabel for="username">{!$Label.MBREmailUsername} | {!$Label.VITILcareEmailUsernameFR}</apex:outputLabel>
                                <apex:inputText value="{!username}" id="username" styleClass="i-email form-control" />
                                <span class="i-error-icon-email frg-icon icon-warning-inverted input-field-icon error-red" aria-hidden="true"></span>
                                <div class="i-form-error-email form-field-error">
                                    <p class="no-margin no-padding"></p>
                                </div>
                                <apex:outputPanel rendered="{!LEN(usernameError)!=0}" styleClass="i-form-error-email form-field-error block">
                                    <p class="no-margin no-padding">{!usernameError}</p>
                                </apex:outputPanel>
                            </div>
                            <div class="form-group">
                                <apex:outputLabel for="password">{!$Label.VITILcarePassword} | {!$Label.VITILcarePasswordFR}</apex:outputLabel>
                                
                                <div id="input-password-wrap">
                                    <apex:inputSecret id="password" styleClass="i-password form-control" value="{!password}" />
                                </div>
                                <div id="unmask-password-wrap"><a href="#" title="Show" class="frg-button color-purple unmask-password" data-show="Show | Afficher" data-hide="Hide | Masquer">Show | Afficher</a></div>
                                <span class="i-error-icon-password frg-icon icon-warning-inverted input-field-icon error-red" aria-hidden="true"></span>
                                <div class="i-form-error-password form-field-error">
                                    <p class="no-margin no-padding"></p>
                                </div>
                                <apex:outputLink styleClass="color-purple bold-font forgot-link" value="{!URLFOR($Page.VITILcareForgotPassword)}">{!$Label.MBRForgotYourPassword} | {!$Label.VITILcareForgotPasswordFR}</apex:outputLink><br/>
                            </div>
                            <div class="form-group form-group-large-device">
                                <input type="button" class="i-submit-btn btn frg-button color-purple" value="{!$label.mbrSignIn} | {!$label.VITILcareSignInFR}" />

                            </div>
                            <div class="form-group form-group-small-device">
                                <input type="button" class="i-submit-btn btn frg-button color-purple button-font-medium full-width" value="{!$label.mbrSignIn} | {!$label.VITILcareSignInFR}" />
                                <br/>
                                <br/>
<!--                                
<apex:outputLink styleClass="color-purple bold-font" value="{!URLFOR($Page.MBRForgotPassword)}">{!$Label.MBRForgotYourPassword}/{!$Label.VITILcareForgotPasswordFR}</apex:outputLink>
-->
                            </div>

                            <apex:actionFunction name="onFormSubmit" action="{!login}" />
                        </apex:form>
                        <p>
                         <apex:outputtext escape="false" value="{!$Label.VITILcareLoginSystemNotice}" />
                         
                         <apex:outputtext escape="false" value="{!$Label.VITILcareLoginSystemNoticeFR}" />
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <c:MBRFooter />


        <script type="text/javascript">
        postcsrf = {
            csrf1: '65183179d092cbaec33c60aa277d471e'
        };
        post_csrf_token_name = 'csrf1';
        post_csrf_token_value = '65183179d092cbaec33c60aa277d471e';
        </script>

        <script type="text/javascript" data-main="//static.telus.com/my-account/154/assets/js/pages/usage/shared-usage" src="//static.telus.com/my-account/154/assets/js/libs/require.js"></script>

        <script type="text/javascript">
        require.config({
            urlArgs: "bust=1400114886"
        });

        $('.i-logout-dialog-close').click(function() {
            $('.i-logout-dialog').removeClass('block');
            handleResize()
        });
        </script>
    </body>

    </html>
</apex:page>