<apex:page controller="LRN.ManageCoreSettingsController" sidebar="false" applyBodyTag="false" docType="html-5.0" showheader="false">
    <html
        xmlns="http://www.w3.org/2000/svg"
        xmlns:xlink="http://www.w3.org/1999/xlink">
        <script>
            var Lurniture = {};
            var coreSettingId = null;
            
            function showLoader(){
                $('#loadingIconOP').addClass('slds-modal-backdrop--open');
            }
            
            function hideLoader(){
                $('#loadingIconOP').removeClass('slds-modal-backdrop--open');
            }
            
            $(document).ready(function(){
                Lurniture.CoreSettingManager.initUI();
                hideLoader();
            });
            
            Lurniture.CoreSettingManager = (function(){
                return{
                    initUI : function(){
                        Visualforce.remoting.Manager.invokeAction('{!$RemoteAction.ManageCoreSettingsController.loadSettingData}', function(event, result){
                               if(result.status){
                                    var isNotificationOnAssign = false;
                                    var isNotificationOnRecommend = false;
                                    var isNotificationOnRoomSubmittedVideo = false;
                                    var isPlaylistNotificationEnabled = false;
                                    var isNotificationSchedulerEnabled = false;
                                    var isCustomEmailFooter = false;
                                    var isProgramAnalyticsAllUsersHaveAccess = false;
                                    var isProgramSchedulerEnabled = false;
                                    var isProgramMetricSchedulerEnabled = false;
                                    var isQuizEmailEnabled = false;
                                    var isShowQuizIncorrectAnswers = false;
                                    var isHideVideoRelatedTo = false;
                                    var customEmailFooterText = '';
                                    var daysToConsiderVideoWatched = 0;
                                    var daysToConsiderVideoNew = 0;
                                    var playlistVideoDueReminderLimit = 0;
                                    
                                    if(Object.keys(result.result).length > 0){
                                        isNotificationOnAssign = result.result[0].notificationOnAssign;
                                        isNotificationOnRecommend = result.result[0].notificationOnRecommend;
                                        isNotificationOnRoomSubmittedVideo = result.result[0].notificationOnRoomSubmittedVideo;
                                        isPlaylistNotificationEnabled = result.result[0].playlistNotificationEnabled;
                                        isNotificationSchedulerEnabled = result.result[0].notificationSchedulerEnabled;
                                        isCustomEmailFooter = result.result[0].customEmailFooter;
                                        isProgramAnalyticsAllUsersHaveAccess = result.result[0].programAnalyticsAllUsersHaveAccess;
                                        isProgramSchedulerEnabled = result.result[0].programSchedulerEnabled;
                                        isProgramMetricSchedulerEnabled = result.result[0].programMetricSchedulerEnabled;
                                        isQuizEmailEnabled = result.result[0].quizEmailSchedulerEnabled;
                                        isShowQuizIncorrectAnswers = result.result[0].showQuizIncorrectAnswers;
                                        isHideVideoRelatedTo = result.result[0].hideVideoRelatedTo;
                                        
                                        customEmailFooterText = $("<div/>").html(result.result[0].customEmailFooterText).text();
                                        daysToConsiderVideoWatched = result.result[0].daysToConsiderVideoWatched;
                                        daysToConsiderVideoNew = result.result[0].daysToConsiderVideoNew;
                                        playlistVideoDueReminderLimit = result.result[0].playlistVideoDueReminderLimit;
                                        coreSettingId = result.result[0].settingId;
                                    }
                                    
                                    $('#checkboxNoytifyAssign').prop('checked', isNotificationOnAssign);
                                    $('#checkboxNoytifyRecommended').prop('checked', isNotificationOnRecommend);
                                    $('#checkboxNotifyRoomSubmittedVideo').prop('checked', isNotificationOnRoomSubmittedVideo);
                                    $('#checkboxLurnitureDigest').prop('checked', isNotificationSchedulerEnabled);
                                    $('#checkboxCustomEmailFooter').prop('checked', isCustomEmailFooter);
                                    $('#checkboxEnablePlaylist').prop('checked', isPlaylistNotificationEnabled);
                                    $('#chkProgramAnalyticsAllUsersHaveAccess').prop('checked', isProgramAnalyticsAllUsersHaveAccess);
                                    $('#chkProgramMaintenance').prop('checked', isProgramSchedulerEnabled);
                                    $('#chkProgramMetric').prop('checked', isProgramMetricSchedulerEnabled);
                                    
                                    $('#chkQuizEmail').prop('checked', isQuizEmailEnabled);
                                    $('#chkShowQuizIncorrectAnswers').prop('checked', isShowQuizIncorrectAnswers);
                                    $('#chkHideVideoRelatedTo').prop('checked', isHideVideoRelatedTo);
                                    
                                    $("#inputCustomEmailFooter").val(customEmailFooterText); 
                                    $("#inputNewVideo").val(daysToConsiderVideoNew); 
                                    $("#inputWatchedVideo").val(daysToConsiderVideoWatched);
                                    $("#inputVideoLimit").val(playlistVideoDueReminderLimit);
                                    
                                    hideLoader();
                                }
                            }
                        );
                    },
                    
                    refreshProgramMetric : function(){
                    	showLoader();
                    	Visualforce.remoting.Manager.invokeAction('{!$RemoteAction.ManageCoreSettingsController.refreshMetric}', function(result, event){
                    		console.log(result);
                    		hideLoader();
                    	});
                    },
                    
                    closeEditPopup : function(saveChanges,event){
                            event.preventDefault();
                            showLoader();
                            if(!saveChanges){
                                
                            }else{
                                var isNotificationOnAssign = $('#checkboxNoytifyAssign').is(':checked');
                                var isNotificationOnRecommend = $('#checkboxNoytifyRecommended').is(':checked');
                                var isNotificationOnRoomSubmittedVideo = $('#checkboxNotifyRoomSubmittedVideo').is(':checked');
                                var isPlaylistNotificationEnabled = $('#checkboxEnablePlaylist').is(':checked');
                                var isNotificationSchedulerEnabled = $('#checkboxLurnitureDigest').is(':checked');
                                var isCustomEmailFooter = $('#checkboxCustomEmailFooter').is(':checked');
                                var isProgramAnalyticsAllUsersHaveAccess = $('#chkProgramAnalyticsAllUsersHaveAccess').is(':checked');
                                var isProgramSchedulerEnabled = $('#chkProgramMaintenance').is(':checked');
                                var isProgramMetricSchedulerEnabled = $('#chkProgramMetric').is(':checked');
                                
                                var isQuizEmailEnabled = $('#chkQuizEmail').is(':checked');
                                var isShowQuizIncorrectAnswers = $('#chkShowQuizIncorrectAnswers').is(':checked');
                                var isHideVideoRelatedTo = $('#chkHideVideoRelatedTo').is(':checked');
                                
                                var customEmailFooterText = '';
                                if ($('#inputCustomEmailFooter').val() != undefined && $('#inputCustomEmailFooter').val() != null && $('#inputCustomEmailFooter').val().trim() != '') {
                                    customEmailFooterText = $('#inputCustomEmailFooter').val().trim();
                                } 
                                
                                var daysToConsiderVideoWatched = 0;
                                if($('#inputWatchedVideo').val() != undefined && $('#inputWatchedVideo').val() != null && 
                                   $('#inputWatchedVideo').val() != ''){
                                   daysToConsiderVideoWatched = $('#inputWatchedVideo').val();
                                } 
                                
                                var daysToConsiderVideoNew = 0;
                                if($('#inputNewVideo').val() != undefined && $('#inputNewVideo').val() != null && 
                                   $('#inputNewVideo').val() != ''){
                                   daysToConsiderVideoNew = $('#inputNewVideo').val();
                                } 
                                
                                var playlistVideoDueReminderLimit = 0;
                                if($('#inputVideoLimit').val() != undefined && $('#inputVideoLimit').val() != null && 
                                   $('#inputVideoLimit').val() != ''){
                                   playlistVideoDueReminderLimit = $('#inputVideoLimit').val();
                                } 
                                
                                var coreSettingWrapperJSON = {};
                                coreSettingWrapperJSON= {"settingId":coreSettingId,"notificationOnAssign":isNotificationOnAssign,
                                                         "notificationOnRecommend":isNotificationOnRecommend, 
                                                         "notificationOnRoomSubmittedVideo":isNotificationOnRoomSubmittedVideo, 
                                                         "daysToConsiderVideoWatched":daysToConsiderVideoWatched,
                                                         "daysToConsiderVideoNew":daysToConsiderVideoNew, 
                                                         "playlistVideoDueReminderLimit":playlistVideoDueReminderLimit,
                                                         "notificationSchedulerEnabled":isNotificationSchedulerEnabled,
                                                         "customEmailFooter":isCustomEmailFooter, 
                                                         "customEmailFooterText":customEmailFooterText, 
                                                         "playlistNotificationEnabled":isPlaylistNotificationEnabled,
                                                         "quizEmailSchedulerEnabled":isQuizEmailEnabled, 
                                                         "showQuizIncorrectAnswers":isShowQuizIncorrectAnswers,
                                                         "hideVideoRelatedTo":isHideVideoRelatedTo, 
                                                         "programAnalyticsAllUsersHaveAccess":isProgramAnalyticsAllUsersHaveAccess,
                                                         "programSchedulerEnabled":isProgramSchedulerEnabled,
                                                         "programMetricSchedulerEnabled":isProgramMetricSchedulerEnabled};
                                
                                Visualforce.remoting.Manager.invokeAction('{!$RemoteAction.ManageCoreSettingsController.updateCoreSetting}',
                                                                         JSON.stringify(coreSettingWrapperJSON), function(event, result){
                                    
                                    if(result.status){
                                        if(result.result.isSaved){
                                            Lurniture.CoreSettingManager.initUI();
                                        }else{
                                            Lurniture.CoreSettingManager.showPopupErrors(result.result.message);
                                        }
                                    }
                                    
                                    hideLoader();
                                });
                            }
                    }, 
                    
                    showPopupErrors : function(message){
                        $("#confirmationBody").html(message);
                        $('#confirmationModal').addClass('slds-fade-in-open');
                        $('#confirmationModalBackdrop').addClass('slds-backdrop--open');
                    },
                    
                    hidePopupErrors : function(){
                        $("#confirmationBody").html('');
                        $('#confirmationModal').removeClass('slds-fade-in-open');
                        $('#confirmationModalBackdrop').removeClass('slds-backdrop--open');
                    },
                    
                }   
                
            })();
            
        </script>
        <style>
            #settingsListContainer {
                margin-top: 1rem;
                padding-right: 1rem;
                padding-bottom: 1rem;
            }
            #settingsListContainer .slds-icon {
                padding: 4px;
            }
        </style>
        <apex:composition template="{!$Page.AdminSettingsTemplate}">
            <apex:define name="rightBodyContent">
                 <!-- Confirmation Modal -->
                 <div aria-hidden="false" role="dialog" class="slds-modal slds-modal--small"  id="confirmationModal">
                     <div class="slds-modal__container" style="max-width: 20rem !important;">
                         <div class="slds-modal__header">
                             <h2 class="slds-text-heading--medium" id="popupTitle"></h2>
                             <span style="cursor:pointer; color:#fff; top: -34px;right: -22px;" class="slds-button slds-modal__close dec-text-white" onclick="Lurniture.CoreSettingManager.hidePopupErrors();">
                                 <svg aria-hidden="true" class="slds-button__icon slds-button__icon--inverse slds-button__icon--large">
                                     <use xlink:href="{!URLFOR($Resource.SLDS, 'assets/icons/action-sprite/svg/symbols.svg#close')}"></use>
                                 </svg>
                                 <span class="slds-assistive-text">Close</span>
                             </span>
                         </div>
                         <div class="slds-modal__content slds-p-around--medium">
                          <div class="slds-form--stacked" id="confirmationBody">
                           
                          </div>
                        </div>
                         <div class="slds-modal__footer">
                             <button type="button" class="slds-button slds-button--neutral" id="cancelButton" onClick="Lurniture.CoreSettingManager.hidePopupErrors();">Cancel</button>
                         </div>
                     </div>
                 </div>
                 <div class="slds-modal-backdrop" id ="confirmationModalBackdrop"></div>
                 <!--  Confirm Modal ends -->
                 
                <div class="slds-page-header" role="banner">
                    <div class="slds-media slds-media--center">
                        <div class="slds-media__figure">
                            <span class="slds-icon_container">
                                <svg aria-hidden="true" class="slds-icon slds-icon--large slds-icon-standard-poll" style="padding:8px;background-color:#49bcd3">
                                    <use xlink:href="{!URLFOR($Resource.slds2, '/assets/icons/utility-sprite/svg/symbols.svg#settings')}"></use>
                                </svg>
                            </span>
                        </div>
                        <div class="slds-media__body">
                            <p class="slds-page-header__title slds-truncate slds-align-middle" title="Manage Core Setting">Manage Core Settings</p>
                            <p class="slds-text-body--small page-header__info">Configuration Settings</p>
                        </div>
                        <button class="slds-button slds-button--brand" id="newTagCreationButton" onClick = "Lurniture.CoreSettingManager.closeEditPopup(true,event)">Save</button>
                    </div>
                </div>
                
            <div id="settingsListContainer">
                <article class="slds-card slds-m-top--x-small slds-p-bottom--small" style="background:none;">
                  <div class="slds-card__header slds-grid">
                    <header class="slds-media slds-media--center slds-has-flexi-truncate">
                      <div class="slds-media__figure">
                        <span class="slds-icon_container slds-icon-standard-contact" title="">
                          <svg class="slds-icon slds-icon--small" aria-hidden="true">
                            <use xlink:href="{!URLFOR($Asset.SLDS, '/assets/icons/utility-sprite/svg/symbols.svg#notification')}"></use>
                          </svg>
                        </span>
                      </div>
                      <div class="slds-media__body">
                        <h2>
                            <span class="slds-text-heading--small">Notifications</span>
                        </h2>
                      </div>
                    </header>
                    
                  </div>
                  <div class="slds-card__body">
                    <div class="slds-form-element slds-m-left--xx-large slds-m-top--small">
                      <div class="slds-form-element__control">
                        <span class="slds-checkbox">
                          <label class="slds-checkbox__label" for="checkboxNoytifyAssign">
                             <input type="checkbox" name="options" id="checkboxNoytifyAssign" /> 
                            <span class="slds-checkbox--faux"></span>
                            <span class="slds-form-element__label">Notify assignees</span>
                          </label>
                        </span>
                      </div>
                    </div>
                    <div class="slds-form-element slds-m-left--xx-large slds-m-top--small">
                      <div class="slds-form-element__control">
                        <span class="slds-checkbox">
                          <label class="slds-checkbox__label" for="checkboxNoytifyRecommended">
                            <input type="checkbox" name="options" id="checkboxNoytifyRecommended" />
                            <span class="slds-checkbox--faux"></span>
                            <span class="slds-form-element__label">Notify recommendees</span>
                          </label>
                        </span>
                      </div>
                    </div>
                    <div class="slds-form-element slds-m-left--xx-large slds-m-top--small">
                      <div class="slds-form-element__control">
                        <span class="slds-checkbox">
                          <label class="slds-checkbox__label" for="checkboxNotifyRoomSubmittedVideo">
                            <input type="checkbox" name="options" id="checkboxNotifyRoomSubmittedVideo" />
                            <span class="slds-checkbox--faux"></span>
                            <span class="slds-form-element__label">Notify room owners and room managers about submitted videos</span>
                          </label>
                        </span>
                      </div>
                    </div>
                    <div class="slds-form-element slds-m-left--xx-large slds-m-top--small">
                      <div class="slds-form-element__control">
                        <span class="slds-checkbox">
                          <label class="slds-checkbox__label" for="checkboxLurnitureDigest">
                            <input type="checkbox" name="options" id="checkboxLurnitureDigest" />
                            <span class="slds-checkbox--faux"></span>
                            <span class="slds-form-element__label">Enable LevelJump Digests</span>
                          </label>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="slds-form-element slds-m-left--xx-large slds-m-top--small">
                    <div class="slds-form-element__control">
                      <span class="slds-checkbox">
                        <label class="slds-checkbox__label" for="checkboxCustomEmailFooter">
                          <input type="checkbox" name="options" id="checkboxCustomEmailFooter" />
                          <span class="slds-checkbox--faux"></span>
                          <span class="slds-form-element__label">Add custom email footer</span>
                        </label>
                      </span>
                    </div>
                  </div>
                  <div class="slds-form-element slds-m-top--small slds-m-right--large" style="margin-left:75px;">
                    <div class="slds-form-element__control">
                      <input type="text" id="inputCustomEmailFooter" maxlength="255" size="255" class="slds-input" placeholder="Enter custom email footer" />
                    </div>
                  </div>
                </article>
                <article class="slds-card slds-m-top--x-small slds-p-bottom--small" style="background:none;">
                  <div class="slds-card__header slds-grid">
                    <header class="slds-media slds-media--center slds-has-flexi-truncate">
                      <div class="slds-media__figure">
                        <span class="slds-icon_container slds-icon-standard-contact" title="">
                          <svg class="slds-icon slds-icon--small" aria-hidden="true">
                            <use xlink:href="{!URLFOR($Asset.SLDS, 'assets/icons/utility-sprite/svg/symbols.svg#video')}"></use>
                          </svg>
                        </span>
                      </div>
                      <div class="slds-media__body">
                        <h2>
                            <span class="slds-text-heading--small">Video Settings</span>
                        </h2>
                      </div>
                    </header>
                    
                  </div>
                  <div class="slds-card__body">
                    <div class="slds-form-element slds-m-left--xx-large slds-m-top--small" >
                      <label class="slds-form-element__label" for="inputNewVideo">Days to consider videos <b>new</b></label>
                          <div class="slds-form-element__control">
                            <input type="text" id="inputNewVideo" class="slds-input slds-size--1-of-2" placeholder="Enter Days" />
                          </div>
                    </div>
                    <div class="slds-form-element slds-m-left--xx-large slds-m-top--small">
                        <label class="slds-form-element__label" for="inputWatchedVideo">Days to consider videos as <b>watched</b></label>
                          <div class="slds-form-element__control">
                            <input type="text" id="inputWatchedVideo" class="slds-input slds-size--1-of-2" placeholder="Enter Days" />
                          </div>  
                    </div>
                    <div class="slds-form-element slds-m-left--xx-large slds-m-top--small" >
                        <div class="slds-form-element__control">
                            <span class="slds-checkbox">
                                <input type="checkbox" name="options" id="chkHideVideoRelatedTo" checked="" />
                                <label class="slds-checkbox__label" for="chkHideVideoRelatedTo">
                                    <span class="slds-checkbox--faux"></span>
                                    <span class="slds-form-element__label">Hide Video Related To</span>
                                </label>
                            </span>
                        </div>
                    </div>
                  </div>
                </article>
                <article class="slds-card slds-m-top--x-small slds-p-bottom--small" style="background:none;">
                  <div class="slds-card__header slds-grid">
                    <header class="slds-media slds-media--center slds-has-flexi-truncate">
                      <div class="slds-media__figure">
                        <span class="slds-icon_container slds-icon-standard-contact" title="">
                          <svg class="slds-icon slds-icon--small" aria-hidden="true">
                            <use xlink:href="{!URLFOR($Asset.SLDS, 'assets/icons/utility-sprite/svg/symbols.svg#side_list')}"></use>
                          </svg>
                        </span>
                      </div>
                      <div class="slds-media__body">
                        <h2>
                            <span class="slds-text-heading--small">Assigned Playlist Video Reminders</span>
                        </h2>
                      </div>
                    </header>
                    
                  </div>
                  <div class="slds-card__body">
                  <div class="slds-form-element slds-m-left--xx-large ">
                        <label class="slds-form-element__label" for="inputVideoLimit">Playlist video due reminder days threshold limit</label>
                          <div class="slds-form-element__control">
                            <input type="text" id="inputVideoLimit" class="slds-input slds-size--1-of-2" placeholder="Enter Days" />
                          </div>  
                    </div>
                  
                    <div class="slds-form-element slds-m-left--xx-large slds-m-top--small" >
                      <div class="slds-form-element__control">
                        <span class="slds-checkbox">
                          <input type="checkbox" name="options" id="checkboxEnablePlaylist" checked="" />
                          <label class="slds-checkbox__label" for="checkboxEnablePlaylist">
                            
                            <span class="slds-checkbox--faux"></span>
                            <span class="slds-form-element__label">Enable playlist video due reminders</span>
                          </label>
                        </span>
                      </div>
                    </div>
                  </div>
                </article>  
                
                <!-- Program maintenance -->
                <article class="slds-card slds-m-top--x-small slds-p-bottom--small" style="background:none;">
                  <div class="slds-card__header slds-grid">
                    <header class="slds-media slds-media--center slds-has-flexi-truncate">
                      <div class="slds-media__figure">
                        <span class="slds-icon_container slds-icon-standard-contact" title="">
                          <svg class="slds-icon slds-icon--small" aria-hidden="true" style="padding:0px;">
                            <use xlink:href="{!URLFOR($Asset.SLDS, 'assets/icons/standard-sprite/svg/symbols.svg#poll')}"></use>
                          </svg>
                        </span>
                      </div>
                      <div class="slds-media__body">
                        <h2>
                            <span class="slds-text-heading--small">Programs</span>
                        </h2>
                      </div>
                    </header>
                    
                  </div>
                  <div class="slds-card__body">
                    <div class="slds-form-element slds-m-left--xx-large slds-m-top--small">
                        <div class="slds-form-element__control">
                        <span class="slds-checkbox">
                          <input type="checkbox" name="options" id="chkProgramAnalyticsAllUsersHaveAccess" checked="" />
                          <label class="slds-checkbox__label" for="chkProgramAnalyticsAllUsersHaveAccess">
                            <span class="slds-checkbox--faux"></span>
                            <span class="slds-form-element__label">Allow program analytics access to all users</span>
                          </label>
                        </span>
                      </div>
                    </div>
                    
                    <div class="slds-form-element slds-m-left--xx-large slds-m-top--small">
                        <div class="slds-form-element__control">
                        <span class="slds-checkbox">
                          <input type="checkbox" name="options" id="chkProgramMaintenance" checked="" />
                          <label class="slds-checkbox__label" for="chkProgramMaintenance">
                            <span class="slds-checkbox--faux"></span>
                            <span class="slds-form-element__label">Enable program maintenance (evening)</span>
                          </label>
                        </span>
                      </div>
                    </div>
                  
                    <div class="slds-form-element slds-m-left--xx-large slds-m-top--small">
                        <div class="slds-form-element__control">
                        <span class="slds-checkbox" style="margin-bottom:10px;">
                          <input type="checkbox" name="options" id="chkProgramMetric" checked="" />
                          <label class="slds-checkbox__label" for="chkProgramMetric">
                            <span class="slds-checkbox--faux"></span>
                            <span class="slds-form-element__label">Enable program maintenance (morning)</span>
                          </label>
                        </span>
                        <button type="button" style="margin-left:28px;" class="slds-button slds-button--brand" id="refreshProgramMetric" onClick = "Lurniture.CoreSettingManager.refreshProgramMetric()">Run program maintenance</button>
                      </div>
                    </div>
                  
                   
                  </div>
                </article>
                <!-- Program maintenance end -->
                
                <!-- Quiz email scheduler -->
                <article class="slds-card slds-m-top--x-small slds-p-bottom--small" style="background:none;">
                  <div class="slds-card__header slds-grid">
                    <header class="slds-media slds-media--center slds-has-flexi-truncate">
                      <div class="slds-media__figure">
                        <span class="slds-icon_container slds-icon-standard-contact" title="">
                          <svg class="slds-icon slds-icon--small" aria-hidden="true">
                            <use xlink:href="{!URLFOR($Asset.SLDS, 'assets/icons/utility-sprite/svg/symbols.svg#question')}"></use>
                          </svg>
                        </span>
                      </div>
                      <div class="slds-media__body">
                        <h2>
                            <span class="slds-text-heading--small">Quizzes</span>
                        </h2>
                      </div>
                    </header>
                    
                  </div>
                  <div class="slds-card__body">
                    <div class="slds-form-element slds-m-left--xx-large slds-m-top--small">
                        <div class="slds-form-element__control">
                        <span class="slds-checkbox">
                          <input type="checkbox" name="options" id="chkQuizEmail" checked="" />
                          <label class="slds-checkbox__label" for="chkQuizEmail">
                            <span class="slds-checkbox--faux"></span>
                            <span class="slds-form-element__label">Enable quiz emails</span>
                          </label>
                        </span>
                      </div>
                    </div>
                      <div class="slds-form-element slds-m-left--xx-large slds-m-top--small">
                          <div class="slds-form-element__control">
                              <span class="slds-checkbox">
                                  <input type="checkbox" name="options" id="chkShowQuizIncorrectAnswers" checked="" />
                                  <label class="slds-checkbox__label" for="chkShowQuizIncorrectAnswers">
                                      <span class="slds-checkbox--faux"></span>
                                      <span class="slds-form-element__label">Show incorrect answers</span>
                                  </label>
                              </span>
                          </div>
                      </div>
                  </div>
                </article>
                <!-- Quiz email scheduler end -->
                
            </div>  
                <div class="slds-modal-backdrop slds-modal-backdrop--open" id="loadingIconOP" style="z-index:9999;background:  rgba(126, 140, 153, 0.4)">
                    <div class="slds-spinner--large loadingIcon" style="margin-left:50%; margin-top:20%;">
                        <img src="{!URLFOR($Resource.slds2, 'assets/images/spinners/slds_spinner_brand.gif')}" alt="Loading..." />
                    </div>
                </div>
            </apex:define>
        </apex:composition>
        </html>
</apex:page>