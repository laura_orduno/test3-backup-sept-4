<apex:page controller="ENTPSubmitFeedbackCtlr" language="{!userLanguage}" docType="html-5.0" showHeader="false" sidebar="false" showChat="false" standardStylesheets="false" applyHtmlTag="false" applyBodyTag="false">
       <apex:composition template="ENTPUserTemplate_new">
        <apex:define name="title">
            <apex:outputText escape="false" value="{!$Label.VITILcareFeedbackPageTitle}"/>
        </apex:define>

        <apex:define name="head">
            <apex:stylesheet value="{!URLFOR($Resource.VITILcareCSS, 'css/submitNewCase.css')}"/>
             <apex:includeScript value="{!URLFOR($Resource.ENTPjs, 'js/submitNewCaseFB.js')}"/>

  <!--         
            <apex:includeScript value="{!URLFOR($Resource.MBRCaseDetailJS, '')}"/>
-->
            <apex:includeScript value="/soap/ajax/31.0/connection.js"/>
            <apex:includeScript value="/soap/ajax/31.0/apex.js"/>

            <script type="text/javascript">
                postcsrf = {csrf1:'65183179d092cbaec33c60aa277d471e'};
                post_csrf_token_name = 'csrf1';
                post_csrf_token_value = '65183179d092cbaec33c60aa277d471e';
            
                function resetFields(){
                    $('.i-subject').val("");
                    $('.i-description').val("");
                    $('.i-request-type-select  option:first').attr("selected", true);
                }  
                <apex:outputText rendered="{!parentCase==null}">
                window.onload = resetFields;
                </apex:outputText>

                
                $('.i-submit-dialog-close').click(function(){
                    $('.i-form-new-case').removeClass('block');
                });
                             
                
                $(document).ready(function(){
                    <apex:outputText rendered="{!parentCase!=null}">
                    // conditional logic if parentCase is set
                    $('.i-product-type-select').attr('disabled', 'disabled').addClass('background-light-grey');
                    $('.i-request-type-select').attr('disabled', 'disabled').addClass('background-light-grey');
                    $('#requestInfo').show();
                    $('.hideComponent').hide();
                    $('#descriptionField').show(); 
                    $('#additionalInfo').show();
                    $('.form-attachment').show(); 
                    </apex:outputText>

                    // logic to clear errors when switching wls vs wln subforms
                    $('.radio-master').on('change', function(){
                        clearFormFieldErrors();
                    });
                });
                
            </script>  

        </apex:define>

        <apex:define name="pageTitle">
            <apex:outputText value="{!$Label.VITILcareSubmitFeedbackMainHeader}"/>
        </apex:define>

        <apex:define name="body">
                      
            <apex:outputPanel rendered="{!caseSubmitted}" styleClass="i-form-new-case text-bubble block">
                <p class="no-margin no-padding"><apex:outputtext value="{!$Label.VITILcareThankYouSubmit}"/>
                    <a class="i-submit-dialog-close" href="#">
                        <span class="frg-icon icon-x-circled text-bubble-close" ></span>
                    </a>
                </p>
            </apex:outputPanel> 

                      <apex:outputPanel rendered="{!$CurrentPage.parameters.ic=='1'}" styleClass="i-form-error-general form-error block"> 
                <p class="no-margin no-padding">{!$Label.VITILcareInvalidTicketLink}</p>
            </apex:outputPanel>
            
            <apex:outputPanel rendered="{!LEN(pageValidationError)!=0}" styleClass="i-form-error-general form-error block">
                <p class="no-margin no-padding"><apex:outputtext value="{!pageValidationError}"/></p>
            </apex:outputPanel>

            <div id="general-validation-error" class="form-error hidden">
                <p class="no-margin no-padding">{!$Label.VITILcareFormValidationError}</p>
            </div>

            <apex:form id="theform" forceSSL="true">
                <div class="row full-width">
                    <div class="col-xs-12 col-sm-8 primary-panel-col">
                        <div id="submit-request-panel" class="panel">
                           <!-- <h2 id="submit-request-panel-header" class="panel-header-medium no-margin color-purple no-margin panel-card-header">{!$label.VITILcareSubmitFeedbackPageCopy}
                            </h2> -->
                            <div class="std-panel-body">
                                <input id="form-error-text-1" type="hidden" value="{!$Label.VITILcareSubmitRequestFormFieldProjectNum}" />
                                <input id="form-error-text-2" type="hidden" value="{!$Label.VITILcareSubmitRequestFormFieldPHNumAffect}" />
                                <apex:inputHidden id="createNewOnSubmit" value="{!createNewOnSubmit}" />
                                <apex:outputpanel rendered="{!$CurrentPage.parameters.previousCaseSubmitted=='1'}">
                                    <div class="form-group background-light-purple no-padding medium-padding-v auto-center">
                                        <apex:outputLabel styleClass="full-width no-margin center-text" escape="false" value="You have submitted a case successfully." />
                                    </div>
                                </apex:outputpanel>
                                
                                
                                <div class="form-group">
                                    
                                    <span class="i-error-icon i-error-icon-request-type frg-icon icon-warning-inverted input-field-icon error-red" aria-hidden="true"></span>
                                    <div class="i-form-error-request-type form-field-error">
                                        <p class="no-margin no-padding"></p>
                                    </div>
                                </div>
                                <apex:inputHidden id="isMobile" value="{!caseToInsert.MobilityRequest__c}" />
                                <div id="requestInfo">
                                   <c:ENTPSubmitFeedbackInfo FormWrapper="{!FormWrapper}" caseToInsert="{!caseToInsert}" />
                                    
                                 </div>
                            </div>
                        </div>
                    </div>
                    
                </div>

                <div id="additionalInfo" >   
                    <div class="col-xs-12 col-sm-8 primary-panel-col">
                        <div class="form-group"> 
                            <button onclick="$('input[id$=createNewOnSubmit]').val('false'); return false;" class="i-submit-but frg-button button-wide color-purple">
                                {!$Label.Site.Submit}
                            </button>
                           
                        </div>
                    </div>
                </div>
            
                <apex:actionFunction name="onFormSubmit" action="{!createNewCase}"/>


                <div class="loading-container">
                    <div class="uss-loader">
                    </div>
                </div>
                            
                
            </apex:form>   

        </apex:define>

        <apex:define name="foot">
           <!--  <script>
                require.config({
                    urlArgs: "bust=1400114886"
                });       
            </script> -->
           
            <script language="JavaScript" type="text/javascript" src="{!URLFOR($Resource.scode_vitil)}"></script>
            <script language="JavaScript" type="text/javascript">         
                $(document).ready( function() { 
                    s.pageName="ENTP - Feedback";
                    s.server="force.com";
                    s.channel="";   
                    s.events="event1";
                 
                });
            </script>
        </apex:define>
    </apex:composition>

   
</apex:page>