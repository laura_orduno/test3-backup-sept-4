<apex:page standardController="Apttus__APTS_Agreement__c"
	extensions="Apttus_DocuSign.SelectAttachmentController" showHeader="true"
	sidebar="true" tabStyle="Apttus__APTS_Agreement__c">
	<apex:form >
	
		<apex:inputHidden value="{!Apttus__APTS_Agreement__c.Apttus__Parent_Agreement__c}" />
		<apex:inputHidden value="{!Apttus__APTS_Agreement__c.Apttus__FF_Agreement_Number__c}" />
		<apex:inputHidden value="{!Apttus__APTS_Agreement__c.Apttus__Status_Category__c}" />
		<apex:inputHidden value="{!Apttus__APTS_Agreement__c.Apttus__Status__c}" />
		<apex:inputHidden value="{!Apttus__APTS_Agreement__c.Name}" />
		
    	<apex:sectionHeader title="{!$Label.Apttus_DocuSign__SendForESignature}" subtitle="{!Apttus__APTS_Agreement__c.Name}"/>
		
		<apex:pageBlock >

			<apex:pageBlockButtons location="top">
				<apex:outputPanel id="idButtonsTopPanel" >
					<apex:commandButton action="{!doPrevious}" value="{!$Label.Apttus_DocuSign__Previous}" 
						id="cmdButtonDoPrevious" rendered="{!showOrderPage}" />
					<apex:commandButton action="{!doNext}" value="{!$Label.Apttus_DocuSign__Next}"
						id="cmdButtonDoNext" />
					<apex:commandButton action="{!doCancel}" value="{!$Label.Apttus_DocuSign__Cancel}"
						id="cmdButtonDoCancel" />
				</apex:outputPanel>		
				<b><apex:actionStatus startText="{!$Label.Apttus_DocuSign__StatusMessage}" id="waitStatus"/></b>								
			</apex:pageBlockButtons>

			<apex:pageBlockButtons location="bottom">
				<apex:outputPanel id="idButtonsBottomPanel" >
					<apex:commandButton action="{!doPrevious}" value="{!$Label.Apttus_DocuSign__Previous}" 
						id="cmdButtonDoPrevious"  rendered="{!showOrderPage}"/>
					<apex:commandButton action="{!doNext}" value="{!$Label.Apttus_DocuSign__Next}"
						id="cmdButtonDoNext" />
					<apex:commandButton action="{!doCancel}" value="{!$Label.Apttus_DocuSign__Cancel}"
						id="cmdButtonDoCancel" />
				</apex:outputPanel>									
			</apex:pageBlockButtons>
			<apex:pageMessages />

			<apex:outputPanel styleClass="searchFilterFields" 
					rendered="{!AND(showAssociatedAttachments, enableFilter)}">
				<apex:pageBlockSection title="{!$Label.Apttus_DocuSign__IncludeAssociatedAttachments}" columns="2" collapsible="false" >

					<apex:panelGrid columns="3" rendered="{!hasParent}" >
						<apex:inputCheckbox value="{!includeParent}" id="includeParent"/>
						<apex:outputLabel value="{!$Label.Apttus_DocuSign__ShowParentAttachments}"
							for="includeParent" />
						<apex:actionSupport event="onclick" action="{!doSearch}"
							rerender="idAttachments, idButtonsTopPanel, idButtonsBottomPanel" status="waitStatus" />
					</apex:panelGrid>
					
					<apex:panelGrid columns="2" rendered="{!hasRelated}" >
						<apex:inputCheckbox value="{!includeRelated}" id="includeRelated"/>
						<apex:outputLabel value="{!$Label.Apttus_DocuSign__ShowRelatedAttachments}"
							for="includeRelated" />
						<apex:actionSupport event="onclick" action="{!doSearch}"
							rerender="idAttachments, idButtonsTopPanel, idButtonsBottomPanel" status="waitStatus" />
					</apex:panelGrid>
					
					<apex:panelGrid columns="2" rendered="{!hasChildren}">
						<apex:inputCheckbox value="{!includeChildren}" id="includeChildren" />
						<apex:outputLabel value="{!$Label.Apttus_DocuSign__ShowChildAttachments}"
							for="includeChildren" />
						<apex:actionSupport event="onclick" action="{!doSearch}"
							rerender="idAttachments, idButtonsTopPanel, idButtonsBottomPanel" status="waitStatus" />
					</apex:panelGrid>
					
					<apex:panelGrid columns="2" rendered="{!hasAmendRenew}">
						<apex:inputCheckbox value="{!includeAmendRenew}" id="includeAmendRenew"  />
						<apex:outputLabel value="{!$Label.Apttus_DocuSign__ShowAmendRenewAttachments}"
							for="includeAmendRenew" />
						<apex:actionSupport event="onclick" action="{!doSearch}"
							rerender="idAttachments, idButtonsTopPanel, idButtonsBottomPanel" status="waitStatus" />
					</apex:panelGrid>
					
					<apex:panelGrid columns="2" rendered="{!hasSiblings}" >
						<apex:inputCheckbox value="{!includeSiblings}" id="includeSiblings" />
						<apex:outputLabel value="{!$Label.Apttus_DocuSign__ShowSiblingAttachments}"
							for="includeSiblings" />
						<apex:actionSupport event="onclick" action="{!doSearch}"
							rerender="idAttachments, idButtonsTopPanel, idButtonsBottomPanel" status="waitStatus" />
					</apex:panelGrid>

					
				</apex:pageBlockSection>
			</apex:outputPanel>
			<apex:outputPanel id="idAttachments">
			
				<apex:pageBlockSection title="{!$Label.Apttus_DocuSign__SelectAttachments}" columns="1" collapsible="false" rendered="{!showSelectPage}">
					<apex:pageBlockTable value="{!attachments}" var="attachData"
						width="100%" >
						<apex:column rendered="{!attachments.size > 0}">
							<apex:facet name="header">{!$Label.Apttus_DocuSign__Select}</apex:facet>
							<apex:inputCheckbox selected="{!attachData.selected}"
								value="{!attachData.selected}" />
						</apex:column>
						<apex:column rendered="{!attachments.size > 0}">
							<apex:facet name="header">{!$Label.Apttus_DocuSign__FileName}</apex:facet>
							<apex:outputLink value="/{!attachData.attachment.Id}">{!attachData.attachment.Name}</apex:outputLink>
						</apex:column>
						<apex:column rendered="{!attachments.size > 0}">
							<apex:facet name="header">{!$Label.Apttus_DocuSign__AgreementName}</apex:facet>
							<apex:outputLink value="/{!attachData.agmt.Id}">{!attachData.agmt.Name}</apex:outputLink>
						</apex:column>
						<apex:column rendered="{!AND((attachments.size > 0), enableFilter)}">
							<apex:facet name="header">{!$Label.Apttus_DocuSign__Relationship}</apex:facet>
							<apex:outputText value="{!attachData.agmtAssociationType}" />
						</apex:column>
						<apex:column rendered="{!attachments.size > 0}">
							<apex:facet name="header">{!$Label.Apttus_DocuSign__Type}</apex:facet>
							<apex:outputText value="{!attachData.agmt.Apttus__Status_Category__c}" />
						</apex:column>
						<apex:column value="{!attachData.agmt.Apttus__FF_Agreement_Number__c}" rendered="{!attachments.size > 0}" />
						<apex:column value="{!attachData.agmt.Apttus__Status__c}" rendered="{!attachments.size > 0}" />
						<apex:column rendered="{!attachments.size > 0}">
							<apex:facet name="header">{!$Label.Apttus_DocuSign__Size}</apex:facet>
							<apex:outputText value="{!attachData.attachment.BodyLength}" />
						</apex:column>
					</apex:pageBlockTable>
				</apex:pageBlockSection>
			</apex:outputPanel>
				
			<apex:outputPanel >
				<apex:pageBlockSection id="idSFAttachments" title="{!$Label.Apttus_DocuSign__OrderAttachments}" columns="1" 
										collapsible="false" rendered="{!showOrderPage}" >
					<apex:pageBlockTable value="{!sfAttachments}" var="sfAttachment" width="100%" >
							<apex:column >
								<apex:commandLink action="{!doMoveDown}" reRender="idSFAttachments">
									<apex:outputPanel rendered="{!NOT(sfAttachment.Id = lastAttachmentId)}">
										<img src="/img/s.gif" alt="Down"  class="downArrowIcon" title="{!$Label.ImageTitleDown}"/>
									</apex:outputPanel>
									<apex:param name="param" assignTo="{!moveDownId}" value="{!sfAttachment.Id}" />
								</apex:commandLink>
							</apex:column>
							<apex:column >
								<apex:commandLink action="{!doMoveUp}" reRender="idSFAttachments">
									<apex:outputPanel rendered="{!NOT(sfAttachment.Id = firstAttachmentId)}">
										<img src="/img/s.gif" alt="Up"  class="upArrowIcon" title="{!$Label.ImageTitleUp}"/>
									</apex:outputPanel>
									<apex:param name="param" assignTo="{!moveUpId}" value="{!sfAttachment.Id}" />
								</apex:commandLink>
							</apex:column>
						<apex:column >
							<apex:facet name="header">{!$Label.Apttus_DocuSign__FileName}</apex:facet>
							<apex:outputLink value="/{!sfAttachment.Id}">{!sfAttachment.Name}</apex:outputLink>
						</apex:column>
						<apex:column >
							<apex:facet name="header">{!$Label.Apttus_DocuSign__Size}</apex:facet>
							<apex:outputText value="{!sfAttachment.BodyLength}"/>
						</apex:column>
						<apex:column >
							<apex:facet name="header">{!$Label.Apttus_DocuSign__CreatedDate}</apex:facet>
							<apex:outputText value="{!sfAttachment.CreatedDate}"/>
						</apex:column>
					</apex:pageBlockTable>
				</apex:pageBlockSection>
			</apex:outputPanel>
			
		</apex:pageBlock>
	</apex:form>
	
</apex:page>