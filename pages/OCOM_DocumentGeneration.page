<apex:page standardStylesheets="false" showHeader="false" sidebar="false" controller="vlocity_cmt.ContractDocumentCreationController" extensions="OCOM_AutoAttachDocExt" action="{!attachDoc}">  
<apex:pageMessages id="msg"/>
  <html xmlns:ng="http://angularjs.org" ng-app="contractVersionPdfApp" lang="en">
      
       <apex:stylesheet value="{!URLFOR($Resource.vlocity_cmt__vlocity_assets, '/css/vlocity.css')}"/> 
        <apex:includeScript value="{!URLFOR($Resource.VLC_AutoAttach_JQUERYJS)}"/>
        <apex:includeScript value="{!URLFOR($Resource.VLC_AutoAttach_JSZip)}"/> 
        <apex:includeScript value="{!URLFOR($Resource.VLC_AutoAttach_FileSaver)}"/>
        <apex:includeScript value="{!URLFOR($Resource.VLC_AutoAttach_AngularJS)}"/>
        <apex:includeScript value="{!URLFOR($Resource.VLC_AutoAttach_ANGULARBOOT)}"/>
        <apex:includeScript value="{!URLFOR($Resource.VLC_AutoAttach_docxtemplater)}"/>
        <apex:includeScript value="/support/console/32.0/integration.js"/>    
      	<apex:includeScript value="/xdomain/xdomain.js"/> 
            
      <apex:includeScript value="/support/console/31.0/integration.js"/>  
      

       <style>       
		.svg-wrapper {
			width: 100%;
		    position: relative;
		    padding: 50px 0;
		}
		
		.svg-wrapper svg {
			width: 40px;
		    position: absolute;
		    left: 50%;
		    margin-left: -14px;
		}
		
		.svg-wrapper span.finishing {
			display: block;
		    text-align: center;
		    padding-top: 50px;
		    color: #8c8c8c;
		}
       </style>               
       <script type="text/javascript">
       
            function savePdf(objId, callback)
            {
                Visualforce.remoting.Manager.invokeAction(
                    '{!$RemoteAction.ContractDocumentCreationController.savePdf}', objId, callback,
                                {buffer: false, escape: false}); 
            }

            function attachDocx(objId, doc, callback)
            {
                Visualforce.remoting.Manager.invokeAction(
                    '{!$RemoteAction.ContractDocumentCreationController.saveDocx}', objId, doc, callback, {buffer: false, escape: false}); 
            }  
            

            function downloadDocx(objId, rels, callback)
            {
                Visualforce.remoting.Manager.invokeAction(
                    '{!$RemoteAction.ContractDocumentCreationController.getContractVersionDocument}', objId, rels, callback, {buffer: false, escape: false}); 
            }
            
            function getDocxTemplate(callback)
            {
                Visualforce.remoting.Manager.invokeAction(
                    '{!$RemoteAction.ContractDocumentCreationController.getDocxTemplate}', callback, {buffer: false, escape: false}); 
            }
            

            function getTemplateZip(result) {
                var zip;
                try{
                	zip = new JSZip(result.templateEncoded, {base64: true});
                }
                catch(ex){
                	event = new CustomEvent('ContractDocumentErrored',{detail:ex.message()});
                    window.parent.dispatchEvent(event); 
                    window.close();
                }
                return zip;
            }

            function getRelsFile(zip) {
                var file = zip.file('word/_rels/document.xml.rels');
                if (file != null) {
                    return file.asText();
                }
                return '';
            }

            function generateDocx(result, type, zip) {
                var contractData = result.contractData;
                var imageCount = result.imageData.numImages;
                for (var i = 0; i < imageCount; i++) {
                    if (typeof result.imageData['imageData'+i] !== 'undefined' && result.imageData['imageData'+i] !== null) {
                        zip.file('word/media/imageData'+i+'.png', result.imageData['imageData'+i], {base64:true});
                    }    
                }
                if (result.contractData.numberingXML!==undefined && result.contractData.numberingXML!==null) {
                   zip.remove('word/numbering.xml');
                   zip.file('word/numbering.xml',result.contractData.numberingXML,{});
               }
                if (typeof result.contractData['DocxRels'] !== 'undefined' && result.contractData['DocxRels'] !== null) {
                    zip.remove('word/_rels/document.xml.rels');
                    zip.file('word/_rels/document.xml.rels', result.contractData['DocxRels'], {});
                }    
                var doc = new Docxgen(zip);
                doc.setData(contractData);                            
                doc.render(); 

                var out;
                if (type === 'blob') {
                    out = doc.getZip().generate({type:"blob"});    
                }
                else {
                    out = doc.getZip().generate({type:"base64"});
                }
                
                return out;
            }
             
            function b64toBlob(b64Data, contentType, sliceSize)
            {
                contentType = contentType || '';
                sliceSize = sliceSize || 512;

                var byteCharacters = atob(b64Data);
                var byteArrays = [];

                for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                    var slice = byteCharacters.slice(offset, offset + sliceSize);

                    var byteNumbers = new Array(slice.length);
                    for (var i = 0; i < slice.length; i++) {
                        byteNumbers[i] = slice.charCodeAt(i);
                    }

                    var byteArray = new Uint8Array(byteNumbers);

                    byteArrays.push(byteArray);
                }

                var blob = new Blob(byteArrays, {type: contentType});
                return blob;
            }

            var contractVersionPdfApp = angular.module("contractVersionPdfApp", []);
            
            contractVersionPdfApp.factory('contractVersionPdfService', function($q, $rootScope)
            {
                var factory = {};
                factory.savePdf = function(objId)
                {
                    var deferred = $q.defer();
                    savePdf(objId, function(result){
                        $rootScope.$apply(function() {
                            deferred.resolve(result);
                        });
                    });
                    return deferred.promise;
                };
                
                factory.downloadPdf = function(objId, docName)
                {
                    var deferred = $q.defer();
                    downloadPdf(objId, docName, function(result){
                        $rootScope.$apply(function() {                           
                            deferred.resolve(result);
                        });
                    });
                    return deferred.promise;
                };

                factory.downloadDocx = function(objId, rels)
                {
                    var deferred = $q.defer();
                    downloadDocx(objId, rels, function(result){
                        $rootScope.$apply(function() {                           
                            deferred.resolve(result);
                        });
                    });
                    return deferred.promise;
                };

                factory.attachDocx = function(objId, data)
                {
                    var deferred = $q.defer();
                    attachDocx(objId, data, function(result){
                        $rootScope.$apply(function() {                           
                            deferred.resolve(result);
                        });
                    });
                    return deferred.promise;
                };
                
               
                
                factory.getDocxTemplate = function(objId, data)
                {
                    var deferred = $q.defer();
                    getDocxTemplate(function(result){
                        $rootScope.$apply(function() {                           
                            deferred.resolve(result);
                        });
                    });
                    return deferred.promise;
                };
                
                return factory;
            });
            
            contractVersionPdfApp.controller('contractVersionPdfController', function($scope, contractVersionPdfService)
            {
               $scope.parentId = '{!versionId}';
                $scope.contractId = '{!contractID}';
               console.log('ParentID' + $scope.parentId);
                console.log('VersionID' + '{!versionId}');
               
                $scope.nameSpaceprefix='vlocity_cmt__';
                $scope.isSforce = (typeof sforce != 'undefined' && typeof sforce.one != 'undefined')?(true):(false);               
                $scope.docName = '';
                $scope.docType = 'Word';
                $scope.serverError = '{!errorMessage}';
                
                
                console.log('$scope.docType'+$scope.docType);
                                    
                $scope.attachPdf = function()
                {
                      contractVersionPdfService.savePdf($scope.parentId).then(function(result, event){
                         event = new CustomEvent('ContractDocumentAttachDone',{detail:true});
                         window.parent.dispatchEvent(event);    
                         window.close();
                      });
                };
                              
                  
                $scope.saveDocx = function()
                 { 
                    debugger;                    
                    window.top.location.href = '{!$Site.BaseRequestUrl}/apex/'+$scope.nameSpaceprefix+'ContractVersionDocxGen?Id='+$scope.parentId;

                 };  

                $scope.attachDocx = function()
                {    
                   
                    if( $scope.serverError !== undefined &&  $scope.serverError !== '' &&  $scope.serverError !== null)  {
                        console.log('Exception Occured.....' + $scope.serverError);
                        event = new CustomEvent('ContractDocumentErrored',{detail:$scope.serverError});
                                               window.parent.dispatchEvent(event); 
                        window.parent.dispatchEvent(event); 
                    }  
                    else {   
                        contractVersionPdfService.getDocxTemplate().then(function(result, event){
                            if (result.errorString !== undefined) {
                                    event = new CustomEvent('ContractDocumentErrored',{detail:result.errorString});
                                window.parent.dispatchEvent(event); 
                                //window.close();   
                            }
                            else {
                                var zip = getTemplateZip(result);
                                contractVersionPdfService.downloadDocx($scope.parentId, getRelsFile(zip)).then(function(result, event){
                                    if (result.errorString !== undefined) {
                                        //alert (result.errorString); 
                                        event = new CustomEvent('ContractDocumentErrored',{detail:result.errorString});
                                               window.parent.dispatchEvent(event); 
                                               //window.close();
                                    }
                                    else {
                                        contractVersionPdfService.attachDocx($scope.parentId, generateDocx(result, 'base64', zip)).then(function(result, event) {
                                               // $scope.navigateBack();
                                               event = new CustomEvent('ContractDocumentAttachDone',{detail:true});
                                               window.parent.dispatchEvent(event); 
                                               //window.close();   
                                        });
                                    }                        
                                });
                            }                        
                        });
                    }
                    return false;
                };  

                
                
                $scope.generateFile = function()
                {        
 
            
                    if($scope.docType==='Word'){                      
                       $scope.attachDocx();
                    }
                    else if($scope.docType=='PDF'){
                       $scope.attachPdf();
                    }
                    else if($scope.docType=='Word,PDF'){
                       contractVersionPdfService.savePdf($scope.parentId).then(function(result, event){    
                      });
                      
                      $scope.attachDocx();
                    }                    
                
                };     
                                
                $scope.loading=true;

                $scope.generateFile();                     
                
            });
            
        
        </script>
        <body>
         <div class="vlocity" ng-controller="contractVersionPdfController">          
           <div class="svg-wrapper">
              <svg x="0px" y="0px" width="40" height="40" viewBox="0 0 48 48">
                    <g width="48" height="48">
                        <animateTransform attributeType="xml" attributeName="transform" type="rotate" from="0 24 24" to="360 24 24" dur="0.75s" repeatCount="indefinite"/>
                         <path fill="#dedede" d="M24,45C12.4,45,3,35.6,3,24S12.4,3,24,3V0l0,0C10.7,0,0,10.7,0,24c0,13.3,10.7,24,24,24V45z"/>
                         <path fill="#05a6df" d="M24,3c11.6,0,21,9.4,21,21s-9.4,21-21,21v3l0,0c13.3,0,24-10.7,24-24C48,10.7,37.3,0,24,0V3z"/>
                    </g>
              </svg>
            <span class="finishing">Creating Contract...</span>
           </div>
         </div> 
  </body>
  </html>
     
</apex:page>