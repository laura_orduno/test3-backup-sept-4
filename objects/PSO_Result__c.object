<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Adjusted_Loads__c</fullName>
        <description>For PSO Dashboard.  Contains the monthly &quot;Adjusted Loads Post Multiplier&quot; data</description>
        <externalId>false</externalId>
        <label>Adjusted Loads</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>BR_Actual__c</fullName>
        <description>Used for PSO Dashboards report of BR -provided from Callidus and is the $$ amount of BR the rep is compensated on. It is driven by results of wireless, wireline and the plan they are on, and calculated by Callidus.</description>
        <externalId>false</externalId>
        <label>BR Actual</label>
        <precision>9</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>BR_Attainpercent__c</fullName>
        <externalId>false</externalId>
        <formula>BR_Actual__c  /  BR_Target__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>BR Attain (%)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>BR_Target__c</fullName>
        <description>Used for PSO Dashboards report of BR -provided from Callidus and is the $$ amount of BR the rep is compensated on as a target. It is driven by targets of wireless, wireline and the plan they are on, and calculated by Callidus.</description>
        <externalId>false</externalId>
        <label>BR Target</label>
        <precision>9</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Billed_Revenue_Integrated_Actual__c</fullName>
        <externalId>false</externalId>
        <label>Billed Revenue Integrated Actual</label>
        <precision>9</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Billed_Revenue_Integrated_Attain__c</fullName>
        <externalId>false</externalId>
        <formula>Billed_Revenue_Integrated_Actual__c  /  Billed_Revenue_Integrated_Target__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Billed Revenue Integrated Attain (%)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Billed_Revenue_Integrated_Target__c</fullName>
        <externalId>false</externalId>
        <label>Billed Revenue Integrated Target</label>
        <precision>9</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Billed_Revenue_Wireless_Actual__c</fullName>
        <externalId>false</externalId>
        <label>Billed Revenue Wireless Actual</label>
        <precision>9</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Billed_Revenue_Wireless_Attain__c</fullName>
        <externalId>false</externalId>
        <formula>Billed_Revenue_Wireless_Actual__c  /  Billed_Revenue_Wireless_Target__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Billed Revenue Wireless Attain (%)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Billed_Revenue_Wireless_Target__c</fullName>
        <externalId>false</externalId>
        <label>Billed Revenue Wireless Target</label>
        <precision>9</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Billed_Revenue_Wireline_Actual__c</fullName>
        <externalId>false</externalId>
        <label>Billed Revenue Wireline Actual</label>
        <precision>9</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Billed_Revenue_Wireline_Attain__c</fullName>
        <externalId>false</externalId>
        <formula>Billed_Revenue_Wireline_Actual__c  /  Billed_Revenue_Wireline_Target__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Billed Revenue Wireline Attain (%)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Billed_Revenue_Wireline_Target__c</fullName>
        <externalId>false</externalId>
        <label>Billed Revenue Wireline Target</label>
        <precision>9</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>CMR_Growth_Actual_Post_Multiplier__c</fullName>
        <description>for PSO results</description>
        <externalId>false</externalId>
        <label>CMR Growth Actual Post-Multiplier</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>CMR_Growth_Actual_Pre_Multiplier__c</fullName>
        <description>For PSO results</description>
        <externalId>false</externalId>
        <label>CMR Growth Actual Pre-Multiplier</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>CMR_Growth_Growth_Attain__c</fullName>
        <externalId>false</externalId>
        <formula>CMR_Growth_Actual_Post_Multiplier__c / CMR_Growth_Growth_Monthly_Target__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>CMR Growth Growth Attain (%)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>CMR_Growth_Growth_Monthly_Target__c</fullName>
        <description>For PSO results</description>
        <externalId>false</externalId>
        <label>CMR Growth Growth Monthly Target</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>CMR_Renewal_Attain__c</fullName>
        <externalId>false</externalId>
        <formula>CMR_Renewal_Monthly_Actual__c  /  CMR_Renewal_Monthly_Target__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>CMR Renewal Attain (%)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>CMR_Renewal_Monthly_Actual__c</fullName>
        <description>for PSO results</description>
        <externalId>false</externalId>
        <label>CMR Renewal Monthly Actual</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>CMR_Renewal_Monthly_Target__c</fullName>
        <description>for PSO results</description>
        <externalId>false</externalId>
        <label>CMR Renewal Monthly Target</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Callidus_Transaction_ID__c</fullName>
        <description>Used to track transaction from Callidus</description>
        <externalId>false</externalId>
        <label>Callidus Transaction ID</label>
        <length>30</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Director__c</fullName>
        <description>Director who the Rep reports to</description>
        <externalId>false</externalId>
        <inlineHelpText>Director from Callidus, not from User Record</inlineHelpText>
        <label>Director</label>
        <length>60</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Loads_Actual__c</fullName>
        <externalId>false</externalId>
        <label>Loads Actual</label>
        <precision>9</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Loads_Attainpercent__c</fullName>
        <externalId>false</externalId>
        <formula>Loads_Actual__c  /  Loads_Target__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Loads Attain (%)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Loads_Target__c</fullName>
        <externalId>false</externalId>
        <label>Loads Target</label>
        <precision>9</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>One_Time_Attain__c</fullName>
        <externalId>false</externalId>
        <formula>One_Time_Monthly_Actual_Post_Multiplier__c  /  One_Time_Monthly_Target__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>One Time Attain (%)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>One_Time_Monthly_Actual_Post_Multiplier__c</fullName>
        <description>for PSO results</description>
        <externalId>false</externalId>
        <label>One Time Monthly Actual Post-Multiplier</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>One_Time_Monthly_Actual_Pre_Multiplier__c</fullName>
        <description>for PSO results</description>
        <externalId>false</externalId>
        <label>One Time Monthly Actual Pre-Multiplier</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>One_Time_Monthly_Target__c</fullName>
        <description>for PSO results</description>
        <externalId>false</externalId>
        <label>One Time Monthly Target</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>PayeeID__c</fullName>
        <description>stored if required from Callidus</description>
        <externalId>false</externalId>
        <label>PayeeID</label>
        <length>20</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Result_Date__c</fullName>
        <description>Used to refer to Month and Year only, even though the day is the first of the month. ie 05/01/2013 refers to May 1st, 2013 but actually referes to actuals for the END of May</description>
        <externalId>false</externalId>
        <inlineHelpText>Used to refer to Month and Year only, even though the day is the first of the month. ie 05/01/2013 refers to May 1st, 2013 but actually referes to actuals for the END of May</inlineHelpText>
        <label>Result Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>TCG_Actual_Post_M__c</fullName>
        <description>This is Post Multiplier value</description>
        <externalId>false</externalId>
        <inlineHelpText>This is Post Multiplier value</inlineHelpText>
        <label>TCG Actual (Post M)</label>
        <precision>9</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TCG_Actual__c</fullName>
        <description>This is Pre Multiplier value</description>
        <externalId>false</externalId>
        <inlineHelpText>This is Pre Multiplier value</inlineHelpText>
        <label>TCG Actual</label>
        <precision>9</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TCG_Attain_percent_Post_M__c</fullName>
        <externalId>false</externalId>
        <formula>TCG_Actual_Post_M__c /  TCG_Target__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>TCG Attain % (Post M)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>TCG_Attainpercent__c</fullName>
        <externalId>false</externalId>
        <formula>TCG_Actual__c /  TCG_Target__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>TCG Attain (%)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>TCG_Target__c</fullName>
        <externalId>false</externalId>
        <label>TCG Target</label>
        <precision>9</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TCR_Actual__c</fullName>
        <externalId>false</externalId>
        <label>TCR Actual</label>
        <precision>9</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TCR_Attain_percent__c</fullName>
        <externalId>false</externalId>
        <formula>TCR_Actual__c / TCR_Target__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>TCR Attain (%)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>TCR_Target__c</fullName>
        <externalId>false</externalId>
        <label>TCR Target</label>
        <precision>9</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TCV_Actual__c</fullName>
        <externalId>false</externalId>
        <formula>TCG_Actual__c + TCR_Actual__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>TCV Actual</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TCV_Attain_percent__c</fullName>
        <externalId>false</externalId>
        <formula>TCV_Actual__c / TCV_Target__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>TCV Attain (%)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>TCV_Target__c</fullName>
        <externalId>false</externalId>
        <formula>TCG_Target__c + TCR_Target__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>TCV Target</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>VP__c</fullName>
        <description>Name of VP from Callidus</description>
        <externalId>false</externalId>
        <inlineHelpText>Name of VP from Callidus</inlineHelpText>
        <label>VP</label>
        <length>40</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>PSO Result</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Result_Date__c</columns>
        <columns>OWNER.FIRST_NAME</columns>
        <columns>OWNER.LAST_NAME</columns>
        <columns>BR_Target__c</columns>
        <columns>BR_Actual__c</columns>
        <columns>BR_Attainpercent__c</columns>
        <columns>TCG_Target__c</columns>
        <columns>TCG_Actual__c</columns>
        <columns>TCG_Attainpercent__c</columns>
        <columns>Loads_Target__c</columns>
        <columns>Loads_Actual__c</columns>
        <columns>Loads_Attainpercent__c</columns>
        <columns>Adjusted_Loads__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>My_PSO_Results</fullName>
        <columns>Result_Date__c</columns>
        <columns>NAME</columns>
        <columns>BR_Target__c</columns>
        <columns>BR_Actual__c</columns>
        <columns>BR_Attainpercent__c</columns>
        <columns>TCG_Target__c</columns>
        <columns>TCG_Actual__c</columns>
        <columns>Loads_Target__c</columns>
        <columns>Loads_Actual__c</columns>
        <columns>TCR_Actual__c</columns>
        <columns>Loads_Attainpercent__c</columns>
        <filterScope>Mine</filterScope>
        <label>Chris phillips¸</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>PSO{000000}</displayFormat>
        <label>PSO Result Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>PSO Results</pluralLabel>
    <searchLayouts/>
    <sharingModel>Private</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
