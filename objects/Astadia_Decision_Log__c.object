<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Decisions and recommendations related to a project</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Date__c</fullName>
        <description>Date decision or recommendated was made/communicated to client</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter the date the decision or recommendation was made/communicated to client or project team</inlineHelpText>
        <label>Recommendation Date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Decision_Name__c</fullName>
        <externalId>false</externalId>
        <label>Decision Name</label>
        <length>200</length>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Decision__c</fullName>
        <description>Details of the final decision</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter the details of the final decision</inlineHelpText>
        <label>Final Decision</label>
        <length>32768</length>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Html</type>
        <visibleLines>15</visibleLines>
    </fields>
    <fields>
        <fullName>Decision_needed_by__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Drop dead date by which this decision is needed</inlineHelpText>
        <label>Decision needed by</label>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Decision_to_be_made__c</fullName>
        <description>High level decision detail</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter high level decision details</inlineHelpText>
        <label>Decision Details</label>
        <length>300</length>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>15</visibleLines>
    </fields>
    <fields>
        <fullName>Project__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Project related to this decision</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter the project related to this decision</inlineHelpText>
        <label>Project</label>
        <referenceTo>Astadia_Project__c</referenceTo>
        <relationshipLabel>Decision Logs</relationshipLabel>
        <relationshipName>Decision_Logs</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Recommender__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Consultant recommending or advising on decision</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter the consultant recommending or advising on decision</inlineHelpText>
        <label>Recommender</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Decision_Logs</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Release__c</fullName>
        <description>Release this decision applies to</description>
        <externalId>false</externalId>
        <inlineHelpText>Although a decision may apply to several releases, please use this field to indicate the current build to which this decision applies</inlineHelpText>
        <label>Release</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Release 1</fullName>
                    <default>true</default>
                    <label>Release 1</label>
                </value>
                <value>
                    <fullName>Release 1.5</fullName>
                    <default>false</default>
                    <label>Release 1.5</label>
                </value>
                <value>
                    <fullName>Release 2</fullName>
                    <default>false</default>
                    <label>Release 2</label>
                </value>
                <value>
                    <fullName>Release 3</fullName>
                    <default>false</default>
                    <label>Release 3</label>
                </value>
                <value>
                    <fullName>Release4</fullName>
                    <default>false</default>
                    <label>Release4</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <description>Current status of decision</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter the current status of decision</inlineHelpText>
        <label>Status</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Waiting for Decision</fullName>
                    <default>true</default>
                    <label>Waiting for Decision</label>
                </value>
                <value>
                    <fullName>Approved</fullName>
                    <default>false</default>
                    <label>Approved</label>
                </value>
                <value>
                    <fullName>Rejected</fullName>
                    <default>false</default>
                    <label>Rejected</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Decision Log</label>
    <listViews>
        <fullName>All</fullName>
        <columns>Decision_Name__c</columns>
        <columns>Decision_to_be_made__c</columns>
        <columns>OWNER.FIRST_NAME</columns>
        <columns>LAST_UPDATE</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>Decisions_for_R1</fullName>
        <columns>NAME</columns>
        <columns>Decision_Name__c</columns>
        <columns>Release__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Release__c</field>
            <operation>equals</operation>
            <value>Release 1</value>
        </filters>
        <label>Decisions for R1</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>DL-{0000}</displayFormat>
        <label>Decision Log ID</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Decision Logs</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Decision_Name__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Decision__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Decision_to_be_made__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Project__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Status__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Recommender__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>LAST_UPDATE</customTabListAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
