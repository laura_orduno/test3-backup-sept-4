<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>The Credit Request History is used to track the web integration calls with BT Credit to make sure duplicate calls are not made (or that if there is an error we have a way to resubmit it)</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Credit_ID__c</fullName>
        <description>The specific Credit ID (either Credit Profile, Credit ASsessment or other ID) that will help track back to the object that had the issue</description>
        <externalId>false</externalId>
        <label>Credit ID</label>
        <length>25</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Credit_Object__c</fullName>
        <description>This provides the list of objects that can be impacted by the Web Service calls (and need to be tracked specifically)</description>
        <externalId>false</externalId>
        <label>Credit Object</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Credit Profile</fullName>
                    <default>false</default>
                    <label>Credit Profile</label>
                </value>
                <value>
                    <fullName>Credit Partner</fullName>
                    <default>false</default>
                    <label>Credit Partner</label>
                </value>
                <value>
                    <fullName>Credit Assessment</fullName>
                    <default>false</default>
                    <label>Credit Assessment</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Operation_or_Method__c</fullName>
        <description>This field is to keep records of the Apex method names when we call BT Credit webservices, in order to track all the history of the calls, successful and/or in error.</description>
        <externalId>false</externalId>
        <label>Operation or Method</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Payload__c</fullName>
        <description>the payload or request sent to BT Credit in the call</description>
        <externalId>false</externalId>
        <label>Payload</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Result__c</fullName>
        <description>the response or errors got back from BT Credit calls</description>
        <externalId>false</externalId>
        <label>Result</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Retry_Count__c</fullName>
        <description>How many times have this been retried</description>
        <externalId>false</externalId>
        <label>Retry Count</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Retry_Status__c</fullName>
        <description>Status of the Retry progress (and whether this is still an issue or not)</description>
        <externalId>false</externalId>
        <label>Retry Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>In Progress</fullName>
                    <default>false</default>
                    <label>In Progress</label>
                </value>
                <value>
                    <fullName>Success</fullName>
                    <default>false</default>
                    <label>Success</label>
                </value>
                <value>
                    <fullName>Error</fullName>
                    <default>false</default>
                    <label>Error</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <description>Shows the status of the request. First time it is called it will go In Progress. If it failed (or was successful) the appropriate status will be selected.</description>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>In Progress</fullName>
                    <default>false</default>
                    <label>In Progress</label>
                </value>
                <value>
                    <fullName>Success</fullName>
                    <default>false</default>
                    <label>Success</label>
                </value>
                <value>
                    <fullName>Error</fullName>
                    <default>false</default>
                    <label>Error</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Credit Request History</label>
    <nameField>
        <displayFormat>CRH-{0000000000}</displayFormat>
        <label>Credit Request History ID</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Credit Request History</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
