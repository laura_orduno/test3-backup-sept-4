<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Project milestones related to a project</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Actual_Completion_Date__c</fullName>
        <description>Actual task completion date</description>
        <externalId>false</externalId>
        <inlineHelpText>Actual task completion date</inlineHelpText>
        <label>Actual Completion Date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Adjusted_Date__c</fullName>
        <description>Adjusted task completion date</description>
        <externalId>false</externalId>
        <inlineHelpText>Adjusted task completion date</inlineHelpText>
        <label>Adjusted Date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Client_Sign_Off__c</fullName>
        <externalId>false</externalId>
        <label>Client Sign Off</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Pending</fullName>
                    <default>true</default>
                    <label>Pending</label>
                </value>
                <value>
                    <fullName>Accepted</fullName>
                    <default>false</default>
                    <label>Accepted</label>
                </value>
                <value>
                    <fullName>Rejected</fullName>
                    <default>false</default>
                    <label>Rejected</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Estimated_Percent_Complete__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Estimated percent complete of this milestone</inlineHelpText>
        <label>Estimated Percent Complete</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Milestone_Name__c</fullName>
        <description>List the Milestone or Deliverable name such as Report of Findings - Acceptance, Initial Config Review, etc.</description>
        <externalId>false</externalId>
        <inlineHelpText>List the Milestone or Deliverable name such as Report of Findings - Acceptance, Initial Config Review, etc.</inlineHelpText>
        <label>Milestone Name</label>
        <required>true</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Notes__c</fullName>
        <externalId>false</externalId>
        <label>Notes</label>
        <length>32768</length>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>6</visibleLines>
    </fields>
    <fields>
        <fullName>Planned_Due_Date__c</fullName>
        <description>Planned Due Date</description>
        <externalId>false</externalId>
        <inlineHelpText>Planned task completion date</inlineHelpText>
        <label>Planned Due Date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Project_Phase__c</fullName>
        <externalId>false</externalId>
        <label>Project Phase</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Discover</fullName>
                    <default>false</default>
                    <label>Discover</label>
                </value>
                <value>
                    <fullName>Design</fullName>
                    <default>false</default>
                    <label>Design</label>
                </value>
                <value>
                    <fullName>Build</fullName>
                    <default>false</default>
                    <label>Build</label>
                </value>
                <value>
                    <fullName>Test</fullName>
                    <default>false</default>
                    <label>Test</label>
                </value>
                <value>
                    <fullName>Train</fullName>
                    <default>false</default>
                    <label>Train</label>
                </value>
                <value>
                    <fullName>Deploy</fullName>
                    <default>false</default>
                    <label>Deploy</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Project__c</fullName>
        <externalId>false</externalId>
        <label>Project</label>
        <referenceTo>Astadia_Project__c</referenceTo>
        <relationshipLabel>Milestones</relationshipLabel>
        <relationshipName>Milestones</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>On Schedule</fullName>
                    <default>true</default>
                    <label>On Schedule</label>
                </value>
                <value>
                    <fullName>Potential Delay</fullName>
                    <default>false</default>
                    <label>Potential Delay</label>
                </value>
                <value>
                    <fullName>Delayed</fullName>
                    <default>false</default>
                    <label>Delayed</label>
                </value>
                <value>
                    <fullName>Assistance Needed</fullName>
                    <default>false</default>
                    <label>Assistance Needed</label>
                </value>
                <value>
                    <fullName>Not Started</fullName>
                    <default>false</default>
                    <label>Not Started</label>
                </value>
                <value>
                    <fullName>Canceled</fullName>
                    <default>false</default>
                    <label>Canceled</label>
                </value>
                <value>
                    <fullName>Complete</fullName>
                    <default>false</default>
                    <label>Complete</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>User__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Person responsible for this milestone</description>
        <externalId>false</externalId>
        <inlineHelpText>Person responsible for this milestone</inlineHelpText>
        <label>Responsibility</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Milestones</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Milestone</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Milestone_Name__c</columns>
        <columns>Planned_Due_Date__c</columns>
        <columns>Actual_Completion_Date__c</columns>
        <columns>Status__c</columns>
        <columns>User__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>MI-{0000}</displayFormat>
        <label>Milestone ID</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Milestones</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Project__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Project_Phase__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Milestone_Name__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Client_Sign_Off__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Status__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Estimated_Percent_Complete__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Planned_Due_Date__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Actual_Completion_Date__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>User__c</customTabListAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
