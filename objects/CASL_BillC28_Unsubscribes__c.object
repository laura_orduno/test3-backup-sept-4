<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Unsubcribed contacts from various TELUS systems within various TELUS business units. The data is sourced from the central TELUS CASL database to ensure TELUS is compliant with the Canadian Bill C28 (CASL) legislation. Data is exported to the Eloqua system. 
Admin access only and should not be added to any layouts without further review. 
Created June 9, 2014 by Allan Roszmann
Updated July 2, 2015 by Allan Roszmann - Change &quot;Data exported to Eloqua&quot; to &quot;Data exported to SFMC&quot;</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>CASL_Reload_Tracking__c</fullName>
        <description>When email is tracked as not being in SFMC, this field tracks the most recent date the email is triggered to be reloaded to SFMC.</description>
        <externalId>false</externalId>
        <label>CASL Reload Tracking</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Date_CASL_SFMC_Compared__c</fullName>
        <description>Date the SFMC data has been compared against CASL Database data.</description>
        <externalId>false</externalId>
        <label>Date CASL / SFMC Compared</label>
        <length>25</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Effective_Date__c</fullName>
        <description>Date contact requested the unsubscribe</description>
        <externalId>false</externalId>
        <label>Effective Date</label>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Email__c</fullName>
        <externalId>true</externalId>
        <label>Email</label>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Email</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>File_Dt__c</fullName>
        <description>Field is populated by an ETL run by the Data Team, at the time CASL records are populated into this object.  Data in this field populates the &quot;Update Indicator&quot; formula field to trigger the SFDC to SFMC synchronization.</description>
        <externalId>false</externalId>
        <label>File Dt</label>
        <length>10</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>In_SFMC__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Identifies that the email address is in the CASL Database and Unsubscribed in the SFMC system.</description>
        <externalId>false</externalId>
        <label>In SFMC</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Invalid_Email_Reason__c</fullName>
        <description>Reason why Email address is noted as &quot;Invalid Email&quot;.</description>
        <externalId>false</externalId>
        <label>Invalid Email Reason</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Number_of_Days__c</fullName>
        <description>For Reference Only (Same formula to determine &quot;Update Indicator&quot; field) - Identifies the number of days between current date and LastModifiedDate.</description>
        <externalId>false</externalId>
        <formula>NOW()-LastModifiedDate</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Number of Days</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Reason_for_CASL_SFMC_Discrepancy__c</fullName>
        <description>Identifies the reason why an email is in the CASL master database and not in the SFMC system.</description>
        <externalId>false</externalId>
        <label>Reason for CASL/SFMC Discrepancy</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>In SFMC not in CASL</fullName>
                    <default>false</default>
                    <label>In SFMC not in CASL</label>
                </value>
                <value>
                    <fullName>Not in SFMC - Investigation Required</fullName>
                    <default>false</default>
                    <label>Not in SFMC - Investigation Required</label>
                </value>
                <value>
                    <fullName>Invalid Email Format from CASL</fullName>
                    <default>false</default>
                    <label>Invalid Email Format from CASL</label>
                </value>
                <value>
                    <fullName>Test Email from CASL not in SFMC</fullName>
                    <default>false</default>
                    <label>Test Email from CASL not in SFMC</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Source__c</fullName>
        <description>The source system the contact requested the unsubcribe</description>
        <externalId>false</externalId>
        <label>Source</label>
        <length>25</length>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Unit__c</fullName>
        <description>Business Unit the Unsubscribe was requested</description>
        <externalId>false</externalId>
        <label>Unit</label>
        <length>15</length>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Unsubscribe_Source__c</fullName>
        <description>A combination of the Unit and Source fields, to be auto synched into Eloqua</description>
        <externalId>false</externalId>
        <label>Unsubscribe Source</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Update_Indicator__c</fullName>
        <description>Updated Nov 2017:
Field is marked as TRUE when the &quot;File_Dt__c&quot; field is populated by ETL steps by the Data Team, at the time records are received from the CASL Master database.  This field is used in an admin report to synch records to SFMC.

Orig description:    If difference of number of days between Current Date and Last Modified Date is &lt;= 1 day, the field should be marked as &quot;true&quot; (checked).  These records are then synched to SFMC.</description>
        <externalId>false</externalId>
        <formula>IF( ISBLANK( File_Dt__c ) , FALSE, TRUE)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Update Indicator</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>CASL_BillC28_Unsubscribe</label>
    <nameField>
        <label>CASL_BillC28_Unsubscribes Name</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>CASL_BillC28_Unsubscribes</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
