<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>New object created to supplement the standard Activity Object as the maximum custom fields limit has been reached on the standard object.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Callback_Number__c</fullName>
        <description>New field for Note Generator project.  

This field captures the end customer&apos;s number to which an agent can call back for additional questions/comments.</description>
        <externalId>false</externalId>
        <label>Callback Number</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Client_Tenure__c</fullName>
        <description>New field for Note Generator project.  

This field captures the number of the years the customer has been with Telus, as viewed in our systems. The input could be a number relating to a year (ie. 2002, 2003, 2017, etc.) or could be a date (i.e 01/02/2000).</description>
        <externalId>false</externalId>
        <label>Client Tenure</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Comp_port_credit__c</fullName>
        <description>New field for Note Generator project.  

This field captures the port credit offered by the competitor</description>
        <externalId>false</externalId>
        <label>Competitor Port-Credit Amount</label>
        <precision>7</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Comp_rate_plan_cost__c</fullName>
        <description>New field for Note Generator project.  

This field captures the competitor&apos;s offer of rate plan</description>
        <externalId>false</externalId>
        <label>Competitor Rate Plan Cost</label>
        <precision>7</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Current_TLC__c</fullName>
        <description>New field for Note Generator project.  

This field captures the current TLC the client has with TELUS</description>
        <externalId>false</externalId>
        <label>Current TLC</label>
        <precision>7</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>DB_Exception_reason__c</fullName>
        <description>New field for Note Generator project.  

This field captures the reason for the potential change of service due to Device Balance exception scenarios</description>
        <externalId>false</externalId>
        <label>DB Exception Reason</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Hardware issues</fullName>
                    <default>false</default>
                    <label>Hardware issues</label>
                </value>
                <value>
                    <fullName>Competition</fullName>
                    <default>false</default>
                    <label>Competition</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>DB_Waived__c</fullName>
        <description>New field for Note Generator project.  

This field captures the percentage waived from the device balanced owed</description>
        <externalId>false</externalId>
        <label>DB Waived</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>0</fullName>
                    <default>false</default>
                    <label>0</label>
                </value>
                <value>
                    <fullName>30%</fullName>
                    <default>false</default>
                    <label>30%</label>
                </value>
                <value>
                    <fullName>50%</fullName>
                    <default>false</default>
                    <label>50%</label>
                </value>
                <value>
                    <fullName>75%</fullName>
                    <default>false</default>
                    <label>75%</label>
                </value>
                <value>
                    <fullName>100%</fullName>
                    <default>false</default>
                    <label>100%</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Dealer_Code__c</fullName>
        <description>New field for Note Generator project.  

This field captures the dealer code when the caller is identified as a dealer-type entity.   The dealer code captures the unique identifier for the dealer company</description>
        <externalId>false</externalId>
        <label>Dealer Code</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Device_Model__c</fullName>
        <description>New field for Note Generator project.  

This field captures the model of the phone currently being used by the client</description>
        <externalId>false</externalId>
        <label>Device Model</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Inbox__c</fullName>
        <defaultValue>false</defaultValue>
        <description>New field for Note Generator project.  

This field indicates whether the call was originated from the Inbox.  This is important for the Note Generated.   However, it is not important for reporting, therefore it is added as a checkbox rather than a new value under the field &quot;Call Received From&quot;</description>
        <externalId>false</externalId>
        <label>Inbox</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Mid_Contract_RPO_Reason__c</fullName>
        <description>New field for Note Generator project.  

This field captures the reason for the potential change of service midway through the customer&apos;s contract.  RPO = Rateplan Optimization.</description>
        <externalId>false</externalId>
        <label>Mid Contract RPO Reason</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Usage</fullName>
                    <default>false</default>
                    <label>Usage</label>
                </value>
                <value>
                    <fullName>Pricing</fullName>
                    <default>false</default>
                    <label>Pricing</label>
                </value>
                <value>
                    <fullName>Competition</fullName>
                    <default>false</default>
                    <label>Competition</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Month_Remaining__c</fullName>
        <description>New field for Note Generator project.  

This field captures the months remaining on the client&apos;s contract with TELUS</description>
        <externalId>false</externalId>
        <label>Months Remaining</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Plan_Name_SOC__c</fullName>
        <description>New field for Note Generator project.  

This field captures the name of the plan offered by the L&amp;R agent to the client.  This field is specific for the &quot;Mid Contract RPO&quot; request type</description>
        <externalId>false</externalId>
        <label>Plan Name/SOC</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Plan_Offered_SOC__c</fullName>
        <description>New field for Note Generator project.  

This field captures the plan offered by the L&amp;R agent to the client in an attempt to save the client from moving to a competitor.  

This field is specific for the &quot;Competitive Threat&quot; request type</description>
        <externalId>false</externalId>
        <label>Plan Offered/SOC</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Rateplan_Change_Fee__c</fullName>
        <defaultValue>false</defaultValue>
        <description>New field for Note Generator project.  

This field captures whether there is a fee charged for a change in the rate plan under the current contract</description>
        <externalId>false</externalId>
        <label>Rateplan Change Fee</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Renewal_Credit_Amount__c</fullName>
        <description>New field for Note Generator project.  

This field captures the renewal credit amount offered to the client.

This field is specific for the &quot;Hardware Exception&quot; request type</description>
        <externalId>false</externalId>
        <label>Renewal Credit Amount</label>
        <precision>7</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>SFDC_Case_Number__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>New field for Note Generator project.  

This field allows the L&amp;R agent to lookup and link to the SFDC Case number that the call is related to.</description>
        <externalId>false</externalId>
        <label>SFDC Case Number</label>
        <referenceTo>Case</referenceTo>
        <relationshipLabel>Supplemental Activity Data</relationshipLabel>
        <relationshipName>Supplemental_Activity_Data</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Sales_Rep_Code__c</fullName>
        <description>New field for Note Generator project.  

This field captures the Sales Rep code when the caller is identified as a dealer-type entity.   The Sales Rep code captures the unique idenifier for the specific individual caller from the dealer store</description>
        <externalId>false</externalId>
        <label>Sales Rep Code</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Service_Phone_Number_Worked_On__c</fullName>
        <description>New field for Note Generator project.  

This field captures the number of the phone of which service is being questioned/challenged</description>
        <externalId>false</externalId>
        <label>Service Phone Number Worked On</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Supplemental Activity Data</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Supplemental Activity Data Name</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Supplemental Activity Data</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
