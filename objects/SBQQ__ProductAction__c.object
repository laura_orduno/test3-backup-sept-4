<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Action taken when Product Rule fires.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>SBQQ__FilterField__c</fullName>
        <deprecated>false</deprecated>
        <description>The field that is updated when the rule is triggered.</description>
        <externalId>false</externalId>
        <label>Filter Field</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Product Code</fullName>
                    <default>false</default>
                    <label>Product Code</label>
                </value>
                <value>
                    <fullName>Product Family</fullName>
                    <default>false</default>
                    <label>Product Family</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>SBQQ__FilterValue__c</fullName>
        <deprecated>false</deprecated>
        <description>The value that will be updated in the chosen Filter Field.</description>
        <externalId>false</externalId>
        <label>Filter Value</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SBQQ__Operator__c</fullName>
        <deprecated>false</deprecated>
        <description>The operator that determines how the Filter Field will be applied to the Filter Value.</description>
        <externalId>false</externalId>
        <label>Operator</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>equals</fullName>
                    <default>false</default>
                    <label>equals</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>SBQQ__Product__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The product that is automatically selected on the quote.</description>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product2</referenceTo>
        <relationshipLabel>Product Actions</relationshipLabel>
        <relationshipName>ProductActions</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SBQQ__Required__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Determines whether the product in the Product Lookup field will be selected on the quote.</description>
        <externalId>false</externalId>
        <label>Required</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>SBQQ__Rule__c</fullName>
        <deprecated>false</deprecated>
        <description>Rule that runs this product action.</description>
        <externalId>false</externalId>
        <label>Rule</label>
        <referenceTo>SBQQ__ProductRule__c</referenceTo>
        <relationshipLabel>Actions</relationshipLabel>
        <relationshipName>Actions</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>SBQQ__Type__c</fullName>
        <deprecated>false</deprecated>
        <description>Type of action.</description>
        <externalId>false</externalId>
        <inlineHelpText>Specify the type of action.</inlineHelpText>
        <label>Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Add</fullName>
                    <default>false</default>
                    <label>Add</label>
                </value>
                <value>
                    <fullName>Remove</fullName>
                    <default>false</default>
                    <label>Remove</label>
                </value>
                <value>
                    <fullName>Enable</fullName>
                    <default>false</default>
                    <label>Enable</label>
                </value>
                <value>
                    <fullName>Disable</fullName>
                    <default>false</default>
                    <label>Disable</label>
                </value>
                <value>
                    <fullName>Enable &amp; Add</fullName>
                    <default>false</default>
                    <label>Enable &amp; Add</label>
                </value>
                <value>
                    <fullName>Disable &amp; Remove</fullName>
                    <default>false</default>
                    <label>Disable &amp; Remove</label>
                </value>
                <value>
                    <fullName>Show</fullName>
                    <default>false</default>
                    <label>Show</label>
                </value>
                <value>
                    <fullName>Hide</fullName>
                    <default>false</default>
                    <label>Hide</label>
                </value>
                <value>
                    <fullName>Show &amp; Add</fullName>
                    <default>false</default>
                    <label>Show &amp; Add</label>
                </value>
                <value>
                    <fullName>Hide &amp; Remove</fullName>
                    <default>false</default>
                    <label>Hide &amp; Remove</label>
                </value>
                <value>
                    <fullName>Default Filter</fullName>
                    <default>false</default>
                    <label>Default Filter</label>
                </value>
                <value>
                    <fullName>Optional Filter</fullName>
                    <default>false</default>
                    <label>Optional Filter</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>SBQQ__ValueField__c</fullName>
        <deprecated>false</deprecated>
        <description>This field will be used instead of the Filter Value when compared with the Filter Field.</description>
        <externalId>false</externalId>
        <inlineHelpText>Compares the Filter Field with this field instead of the Filter Value.</inlineHelpText>
        <label>Value Field</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Product Code</fullName>
                    <default>false</default>
                    <label>Product Code</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>SBQQ__ValueObject__c</fullName>
        <deprecated>false</deprecated>
        <description>The Salesforce Object that holds the Value Field,</description>
        <externalId>false</externalId>
        <inlineHelpText>Select the Salesforce Object where the Value Field is defined.</inlineHelpText>
        <label>Value Object</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Quote</fullName>
                    <default>false</default>
                    <label>Quote</label>
                </value>
                <value>
                    <fullName>Product</fullName>
                    <default>false</default>
                    <label>Product</label>
                </value>
                <value>
                    <fullName>Configuration Attributes</fullName>
                    <default>false</default>
                    <label>Configuration Attributes</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Product Action</label>
    <nameField>
        <displayFormat>PA-{0000000}</displayFormat>
        <label>Product Action #</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Product Actions</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
