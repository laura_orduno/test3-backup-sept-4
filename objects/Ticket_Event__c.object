<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Stores tickets events to be used by Apropos to auto-create interactions for manual ticket/activity creation by an agent. The object will be queried by the SFDC-Apropos connector to get transactions and will be updated with transaction status, ticket/activity information (on success) and failure reason (on failure).</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Case_Comment_Id__c</fullName>
        <description>Stores tickets events to be used by Apropos to auto-create interactions for manual ticket/activity creation by an agent. The object will be queried by the SFDC-Apropos connector to get transactions and will be updated with transaction status, ticket/activity information (on success) and failure reason (on failure).</description>
        <externalId>false</externalId>
        <label>Case Comment Id</label>
        <length>20</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Case_Comment__c</fullName>
        <externalId>false</externalId>
        <label>Case Comment</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>Case_Id__c</fullName>
        <externalId>false</externalId>
        <label>Case Id</label>
        <referenceTo>Case</referenceTo>
        <relationshipLabel>Ticket Events (Case Id)</relationshipLabel>
        <relationshipName>Ticket_Events</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>true</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Case_Number__c</fullName>
        <description>Stores tickets events to be used by Apropos to auto-create interactions for manual ticket/activity creation by an agent. The object will be queried by the SFDC-Apropos connector to get transactions and will be updated with transaction status, ticket/activity information (on success) and failure reason (on failure).</description>
        <externalId>false</externalId>
        <label>Case Number</label>
        <length>20</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Customer_Portal_Role__c</fullName>
        <externalId>false</externalId>
        <formula>TEXT(Case_Id__r.CreatedBy.Customer_Portal_Role__c)</formula>
        <label>Customer Portal Role</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Event_Type__c</fullName>
        <description>Stores tickets events to be used by Apropos to auto-create interactions for manual ticket/activity creation by an agent. The object will be queried by the SFDC-Apropos connector to get transactions and will be updated with transaction status, ticket/activity information (on success) and failure reason (on failure).</description>
        <externalId>false</externalId>
        <label>Event Type</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>New ticket</fullName>
                    <default>false</default>
                    <label>New ticket</label>
                </value>
                <value>
                    <fullName>New activity</fullName>
                    <default>false</default>
                    <label>New activity</label>
                </value>
                <value>
                    <fullName>Verify ticket</fullName>
                    <default>false</default>
                    <label>Verify ticket</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Failure_Reason__c</fullName>
        <description>Stores tickets events to be used by Apropos to auto-create interactions for manual ticket/activity creation by an agent. The object will be queried by the SFDC-Apropos connector to get transactions and will be updated with transaction status, ticket/activity information (on success) and failure reason (on failure).</description>
        <externalId>false</externalId>
        <label>Failure Reason</label>
        <length>32768</length>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>Lynx_Activity_Number__c</fullName>
        <externalId>false</externalId>
        <label>Lynx Activity Number</label>
        <length>25</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Lynx_Ticket_Number__c</fullName>
        <description>Stores tickets events to be used by Apropos to auto-create interactions for manual ticket/activity creation by an agent. The object will be queried by the SFDC-Apropos connector to get transactions and will be updated with transaction status, ticket/activity information (on success) and failure reason (on failure).</description>
        <externalId>false</externalId>
        <label>Lynx Ticket Number</label>
        <length>20</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Notify_Admin__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Notify Admin</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Remedy_BU__c</fullName>
        <externalId>false</externalId>
        <formula>Case_Id__r.Account.Remedy_Business_Unit_Id__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Remedy Business Unit Id</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Remedy_Company_Id__c</fullName>
        <externalId>false</externalId>
        <formula>Case_Id__r.Account.Remedy_Company_Id__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Remedy Company Id</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Remedy_PIN__c</fullName>
        <externalId>false</externalId>
        <formula>Case_Id__r.Contact.Remedy_PIN__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Remedy PIN</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <description>Stores tickets events to be used by Apropos to auto-create interactions for manual ticket/activity creation by an agent. The object will be queried by the SFDC-Apropos connector to get transactions and will be updated with transaction status, ticket/activity information (on success) and failure reason (on failure).</description>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>New</fullName>
                    <default>false</default>
                    <label>New</label>
                </value>
                <value>
                    <fullName>Failed</fullName>
                    <default>false</default>
                    <label>Failed</label>
                </value>
                <value>
                    <fullName>Complete</fullName>
                    <default>false</default>
                    <label>Complete</label>
                </value>
                <value>
                    <fullName>Validated</fullName>
                    <default>false</default>
                    <label>Validated</label>
                </value>
                <value>
                    <fullName>Verify</fullName>
                    <default>false</default>
                    <label>Verify</label>
                </value>
                <value>
                    <fullName>Read</fullName>
                    <default>false</default>
                    <label>Read</label>
                </value>
                <value>
                    <fullName>Cancel</fullName>
                    <default>false</default>
                    <label>Cancel</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Strategic_Account__c</fullName>
        <description>Indicates Critical Customer</description>
        <externalId>false</externalId>
        <formula>Case_Id__r.Account.Strategic__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Strategic Account</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Ticket Event</label>
    <nameField>
        <displayFormat>TE# {0}</displayFormat>
        <label>Ticket Event Name</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Ticket Events</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
