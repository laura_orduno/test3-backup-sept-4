<!--
Created by: Aditya Jamwal (IBM) 19/6/2017
This Component is created to create order with two different recordtypes.
Customer Solution for Parent Order
Customer Order    for Child  Order.
-->
<apex:component controller="OC_CreateOrderController" allowdml="true" >
 <script>   
    Visualforce.remoting.timeout = 60000;// 120000
    var RCID; 
    var timeoutSecs = 120000; //
    var VFRPageException = 'VFRPageException';
    
    //Sample for customer order
    //Customer Order = {
    //                  SolutionOrderId  : Parent Order Id,
    //                  serviceAddressId : SMBCare Address Id
    //                 }
    // 
    
    //method to create Parent Order and Child Orders i.e. Customer Solution and Customer Order.
    //By providing customerOrderList, RCID and Callback method.
    //return successful : 'success'
    //return failure    : error 
    function createSolutionOrderAndCustomerOrders(customerOrderList,RCID,orderType,callbackmethod){
        try{ 
           Visualforce.remoting.Manager.invokeAction(
                '{!$RemoteAction.OC_CreateOrderController.createSolutionOrderAndCustomerOrders}',customerOrderList,RCID,orderType,
                callbackmethod,
                {buffer: false,timeout: timeoutSecs,escape: false}
            );
        }catch(err){
            alert(''+err.message);
        }
    }
    
    //method to create Parent Order i.e. Customer Solution.
    //By providing RCID and Callback method.
    //return successful : Order Id
    //return failure    : error 
    //RTA 617 - fix - added oppId
    function createSolutionOrder(RCID,orderType,oppId,callbackmethod){
        try{ 
            Visualforce.remoting.Manager.invokeAction(
                '{!$RemoteAction.OC_CreateOrderController.createSolutionOrder}',RCID,orderType,oppId,
                callbackmethod,
                {buffer: false,timeout: timeoutSecs,escape: false}
            );
        }catch(err){
            alert(''+err.message);
        }
    }
    
    //method to create child order i.e. Customer Order.
    //By providing customerOrderList, RCID and Callback method.
    //return successful : 'success'
    //return failure    : error
    function createCustomerOrders(customerOrderList,RCID,callbackmethod){
        try{          
            Visualforce.remoting.Manager.invokeAction(
                '{!$RemoteAction.OC_CreateOrderController.createCustomerOrders}',customerOrderList,RCID,
                callbackmethod,
                {buffer: false,timeout: timeoutSecs,escape: false}
            );
        }catch(err){
            alert(''+err.message);
        }
    }
   
    //Placeholder
    function deleteOrders(OrderIdList,callbackmethod){
        try{
          
            Visualforce.remoting.Manager.invokeAction(
                '{!$RemoteAction.OC_CreateOrderController.deleteOrders}',OrderIdList,
                callbackmethod,
                {buffer: false,timeout: timeoutSecs,escape: false}
            );
        }catch(err){
            alert(''+err.message);
        }
    }
    
    //Start : BMPF-5
    function cancelOrders(OrderIdList,callbackmethod){
        try{
          
            Visualforce.remoting.Manager.invokeAction(
                '{!$RemoteAction.OC_CreateOrderController.cancelOrders}',OrderIdList,
                callbackmethod,
                {buffer: false,timeout: timeoutSecs,escape: false}
                //  console.log('In cancel order');
            );
        }catch(err){
            alert(''+err.message);
        }
    }
   //End : BMPF-5 
    
   //Start : BMPF-44
   function updateOrders(customerOrderList,callbackmethod){
        try{
          
            Visualforce.remoting.Manager.invokeAction(
                '{!$RemoteAction.OC_CreateOrderController.updateOrders}',customerOrderList,
                callbackmethod,
                {buffer: false,timeout: timeoutSecs,escape: false}
                //  console.log('In cancel order');
            );
        }catch(err){
            alert(''+err.message);
        }
    }
   //End : BMPF-44 
    
    //method to create customer order by providing parent order id and 
   // checkTechnicalAvailability_controller.addressWrapper object.
   function prepareDataToCreateCustomerOrder(ordersForAddresses,SolutionOrderId,orderType){
       
        var customerOrderList = new Array();        
        for(i=0; i< ordersForAddresses.length;i++){
        console.log('!@ ordersForAddresses[i] '+ordersForAddresses[i]);
            if(null != ordersForAddresses[i].addressId){
                var customerOrder =  new Object(); 
                if(ordersForAddresses[i].OrderId){
                 customerOrder.orderId = ordersForAddresses[i].OrderId;
                }
                customerOrder.SolutionOrderId = SolutionOrderId;
                customerOrder.serviceAddressId = ordersForAddresses[i].addressId;
                customerOrder.orderType = orderType;

                if(ordersForAddresses[i].moveType){  customerOrder.serviceAddressId = null;}
                
                if(ordersForAddresses[i].moveType && ordersForAddresses[i].moveType === 'Move In'){
                    if( ordersForAddresses[i].moveOutAddressWrapper){
                    customerOrder.moveServiceAddressId = ordersForAddresses[i].moveOutAddressWrapper.addressId;
                    }
                  customerOrder.serviceAddressId = ordersForAddresses[i].addressId;
                } 
                if(ordersForAddresses[i].moveType && ordersForAddresses[i].moveType === 'Move Out'){
                    if(ordersForAddresses[i].moveInAddressWrapper){
                    customerOrder.serviceAddressId = ordersForAddresses[i].moveInAddressWrapper.addressId;
                    }                 
                 customerOrder.moveServiceAddressId = ordersForAddresses[i].addressId;
                }        
                //customerOrder.moveType = 'Move In';
                customerOrderList.push(customerOrder);
            }
        }
        return customerOrderList;
    }
    
    //method to create customer order by providing parent order id and srevice address id.
      function prepareDataToCreateCustomerOrderWithoutAddressWrapper(SolutionOrderId,serviceAddressId,orderType,moveServiceAddressId){
        var moveserviceAddressId = null;           
        var customerOrder =  new Object(); 
            customerOrder.SolutionOrderId = SolutionOrderId;
            customerOrder.serviceAddressId = serviceAddressId;
            customerOrder.orderType = orderType;
            customerOrder.moveServiceAddressId = moveServiceAddressId;
           
        return customerOrder;
    }
    
    function callbackmethod(result){
        console.log(result);
     return result; 
    }
    
    //BMPF -19 - method to check existing service locations and create Parent Order if existing service locations not present.
    //By providing RCID and Callback method. Used in smb_svoc page.
    //return successful : Order Id
    //return failure    : error 
    function checkServiceLocationsAndCreateSolutionOrder(RCID){
        try{ 
            Visualforce.remoting.Manager.invokeAction(
                '{!$RemoteAction.OC_CreateOrderController.checkServiceLocationsAndCreateSolutionOrder}',RCID,
                 function(result,event) {
                    console.log('@@@ParentOrderId ' + result);
                    /*if(result){
                        return openSubTab('/apex/OC_NewAddress?rcid='+RCID+'&parentOrderId='+result,true,'New Address');
                    }else{
                        return openSubTab('/apex/OC_SelectAddress?rcid='+RCID, true, 'Select Address');
                    }*/
                    return openSubTab('/apex/OC_SelectAddress?rcid='+RCID, true, 'Select Address');
                 },
                 {buffer: false,timeout: timeoutSecs,escape: false}
            );
        }catch(err){
            alert(''+err.message);
        }
    }
 </script>  
</apex:component>