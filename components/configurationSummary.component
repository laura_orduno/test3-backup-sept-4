<apex:component controller="ConfigurationSummaryController">
	<apex:attribute name="selections" assignTo="{!selections}" type="ProductOptionSelectionModel" description="Configured product"/>
	<apex:attribute name="includeNested" assignTo="{!includeNested}" type="Boolean" description="Should nested options be included?"/>
	<apex:attribute name="includeBundled" assignTo="{!includeBundled}" type="Boolean" description="Should bundled options be included?"/>
	<apex:attribute name="groupField" assignTo="{!groupField}" type="String" description="Field to use for grouping"/>
	<apex:attribute name="totalFilterField" assignTo="{!totalFilterField}" type="String" description="Field for filtering options included in total"/>
	<apex:attribute name="totalFilterValue" assignTo="{!totalFilterValue}" type="String" description="Value for filtering options included in total"/>
	<apex:attribute name="totalFormatPattern" type="String" description="Pattern used in formatting total"/>
	<apex:attribute name="priceFormatPattern" type="String" description="Pattern used in formatting prices"/>
	 
	<apex:outputPanel layout="block" styleClass="configuration-summary">
		<apex:outputPanel layout="block" style="margin-top: 10px; color: red" rendered="{!totalProductCount > 90}">
		You are close to exceeding the limit on number of products that may be quoted on one quote. Clicking Continue may result in a fatal error. 
		</apex:outputPanel>
		<apex:outputPanel layout="block" styleClass="configuration-summary-total">
			<apex:outputText value="{!totalFormatPattern}">
				<apex:param value="{!total}"/>
			</apex:outputText>
		</apex:outputPanel>
		<apex:panelGrid columns="2" columnClasses="data-label,data-price">
			<apex:outputText value="{!selections.product.vo.name}"/>
			<apex:outputText value="{!priceFormatPattern}">
				<apex:param value="{!rootOption.SBQQ__UnitPrice__c}"/>
			</apex:outputText>
		</apex:panelGrid>
		<apex:repeat var="group" value="{!summaryGroups}">
			<apex:outputPanel layout="block" styleClass="configuration-summary-group">
				<div class="configuration-summary-title">
					<apex:outputText value="{!group.title}"/>
				</div>
				<apex:dataTable var="option" value="{!group.options}">
					<apex:column styleClass="data-label">
						<apex:outputText value="{!option.target.SBQQ__ProductName__c}"/>
						&nbsp;
						<apex:outputText value="({!ROUND(option.target.SBQQ__Quantity__c, 0)} x {!priceFormatPattern})">
							<apex:param value="{!option.target.SBQQ__UnitPrice__c}"/>
						</apex:outputText>
					</apex:column>
					<apex:column styleClass="data-price">
						<apex:outputText value="{!priceFormatPattern}">
							<apex:param value="{!option.totalPrice}"/>
						</apex:outputText>
					</apex:column>
				</apex:dataTable>
			</apex:outputPanel>
		</apex:repeat>
		<apex:outputPanel layout="block" styleClass="configuration-summary-product-total">
			<apex:outputText value="Selected Products: {!totalProductCount}"/>
		</apex:outputPanel>
	</apex:outputPanel>
</apex:component>