<apex:component >
<apex:attribute name="caseToInsert" type="Case" description="" required="true"/>
<apex:attribute name="caseDetailPage" type="Boolean" description="" required="false" default="false" />

    <div class="panel" id="notificationsPanel">
        <h2 class="panel-header-medium no-margin color-purple no-margin panel-card-header" id="">Notifications</h2>
        <div class="std-panel-body">
     <!--    <apex:form > -->
            <label class="checkbox-label">
                <apex:inputCheckbox value="{!caseToInsert.NotifyCustomer__c}" id="notifyCheck" styleClass="telus-hidden-input"/>
                <span class="checkbox-state"></span>
                {!$Label.ENTPNotificationUpdates}<span class="panel-header-subtext">{!$Label.MBROptional}</span>
            </label>
            <br/>
            <label class="checkbox-label">
                <input type="checkbox" id="notifyCollaborator" class="collaborator-check telus-hidden-input" />
                <apex:inputHidden id="collaboratorString" value="{!caseToInsert.NotifyCollaboratorString__c}" />
                <span class="checkbox-state"></span>
                {!$Label.ENTPNotificationsCollaboration}<span class="panel-header-subtext">{!$Label.MBROptional}</span>
            </label>
            <div class="collaborate-reveal">
                <div class="collaborate-email-input-list">
                    <div class="collaborate-email">
                        <input type="text" class="form-control xs-inline-field collab-email" placeholder="{!$Label.MBREnterEmail}"/><a href="javascript:;" class="xs-inline-submit frg-link new-case-remove-email"><span aria-hidden="true" class="frg-icon icon-minus-circled xs-inline-submit"></span></a><span class="i-error-icon i-error-icon-collab-email frg-icon icon-warning-inverted input-field-icon error-red" aria-hidden="true"></span>
                        <div class="i-error-collab-email form-field-error">
                            <p class="no-margin no-padding"></p>
                        </div>
                    </div>
                </div>
                 <a href ="javascript:void(0);" class="frg-link has-icon frg-link has-icon" id ="newCaseAddEmail"><i aria-hidden="true" class="frg-icon icon-plus-circled"></i><span class="icon-label">{!$Label.MBRAddEmail}</span></a>
            </div>
<!--            
-->        
      <!--       </apex:form> -->
        </div> 
    </div>

    <script type="text/javascript">

        var onCaseDetailPage = {!IF(caseDetailPage,'true','false')};

        $(document).ready(function() {

            // bind show / hide collaborators
            $('.collaborator-check').on('click', function(e){
                if (!collabShow()) {
                    e.preventDefault();
                    //e.stopPropagation();
                }
            });

            // bind add email                
            $('#newCaseAddEmail').on('click', function(){
                collabAddEmail();
                if ($('.form-control.collab-email').length >= 3) {
                    $('#newCaseAddEmail').hide();
                }
            });

            // initialize collaborators
            collabInitializeEmails();
            collabShow();
            collabBindChangeLogic();
            collabBindClearLogic();


        });

        function collabShow(){
            var retval = true;
            if ($('.collaborator-check').is(':checked')){
                $('.collaborate-reveal').fadeIn('fast');
            } else {
                var collabString = $('input[id$="collaboratorString"]').val();
                if (collabString.length > 0) {
                    if (confirm('Are you sure you wish to remove all collaborators from this request?')) {
                        $('.collaborate-reveal').fadeOut('fast');
                        $('input[id$="collaboratorString"]').val('');
                        update_subscription();
                        collabInitializeEmails();
                    } else {
                        retval = false;
                    }
                } else {
                    $('.collaborate-reveal').fadeOut('fast');
                }
            }
            return retval;
        }

        function collabBindClearLogic(){ 
            $('.new-case-remove-email').off('click').on('click',function() {
                $(this).parent().fadeOut("fast", function() {
                    $(this).remove();
                    collabSetEmailString();
                    if (onCaseDetailPage) {
                        update_subscription();
                    }
                    if ($('.form-control.collab-email').length < 3) {
                        $('#newCaseAddEmail').show();
                    }
                });
            });
        }

        function collabAddEmail(emailAddr) {

            if (typeof emailAddr != 'string') {
                emailAddr = '';
            }
            var newField ='<div class="collaborate-email"><input type="text" class="form-control xs-inline-field collab-email" placeholder="Enter Email"' + (emailAddr.length>0 ? ' value="'+emailAddr+'"' : '') + ' /><a href="javascript:void(0);" class="xs-inline-submit frg-link new-case-remove-email"><span aria-hidden="true" class="frg-icon icon-minus-circled xs-inline-submit"></span></a><span class="i-error-icon i-error-icon-collab-email frg-icon icon-warning-inverted input-field-icon error-red" aria-hidden="true"></span><div class="i-error-collab-email form-field-error"><p class="no-margin no-padding"></p></div></div>';
            $(newField).hide().appendTo('.collaborate-email-input-list').fadeIn("fast");
            collabBindChangeLogic();
            if (onCaseDetailPage) {
                collabBindOnBlurLogic();
            }
            collabBindClearLogic();
        }
        function collabSetEmailString() {
            var collabString = '';
            $('input.collab-email').each(function(){
                var thisVal = $(this).val();
                if (thisVal.length) {
                    collabString += (collabString.length ? ';' : '') + thisVal;
                }
            });
            $('input[id$="collaboratorString"]').val(collabString);
            //console.log('collabString: ' + $('input[id$="collaboratorString"]').val());
        }
        function collabBindChangeLogic() {
            $('input.collab-email').off('change keyup').on('change keyup', function(){
                collabSetEmailString();
            });
        }
        function collabBindOnBlurLogic() {
            $('input.collab-email').off('blur').on('blur', function(){
                // update_subscription defined in MBRCaseDetail.page via apex:actionFunction
                update_subscription();
            });
        }
        function collabInitializeEmails() {
            var emailStr = $('input[id$="collaboratorString"]').val();
            //console.log('emailStr: [' + emailStr + ']');
            $('.collaborate-email').remove(); // remove any existing fields
            if (typeof emailStr != 'undefined' && emailStr.length > 0) {
                var emails = emailStr.split(';');
                //console.log('emails: ', emails);
                for (var idx in emails) {
                    var email = emails[idx];
                    if (typeof email == 'string' && email.length > 0) {
                        //console.log('calling collabAddEmail('+email+')...');
                        collabAddEmail(email);
                    }
                }
                if ($('.form-control.collab-email').length >= 3) {
                    $('#newCaseAddEmail').hide();
                }
                $('.collaborator-check').click();
            } else {
                collabAddEmail('');
            }
        }
            
    </script>
    
</apex:component>