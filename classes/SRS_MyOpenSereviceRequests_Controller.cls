/*
###########################################################################
# File..................: SRS_MyOpenSereviceRequests_Controller
# Version...............: 1
# Created by............: Aditya Jamwal (IBM)
# Created Date..........: 16-Oct-2014 
# Last Modified by......: 
# Last Modified Date....: 
# Modification:           
# Description...........: Class is for MyOpenSereviceRequests Homepage component filtering Service request specific for logged in user.
#
############################################################################ 

*/
public with sharing class SRS_MyOpenSereviceRequests_Controller {

public transient List<Service_Request__c> SRList =new List<Service_Request__c>();
public Boolean showRelatedList{get;set;}
public String SRId {get; set;}
public transient List<SerReq> SerReqList = new List<SerReq>();
public boolean doSort= false;
private String sortDirection = 'DESC';
private String sortExp = 'LastModifiedDate';
string sortFullExp = ' order by LastModifiedDate desc '; 
// Which field should be considered for sorting
public static string SORT_FIELD ='Condition_Sort_Index__c' ;
// Sorting direction ASCENDING or DESCENDING
public static string SORT_DIR = 'DESC';   
public transient Map<ID, Schema.RecordTypeInfo> recordtypeMap = new Map<ID, Schema.RecordTypeInfo>();
//public ApexPages.StandardController tempController; 
public transient Map<String, Schema.SObjectField> HolidayFieldMap = new Map<String, Schema.SObjectField>();
public static string GREEN_COLOR = '#98FF98'; //'MINT'; 
public static string RED_COLOR = '#FF6347'; //'LIGHT RUBY RED' earlier > #FF7267;
public static string ORANGE_COLOR = '#FFB34F'; //'MARMALADE';
public static string PURPLE_COLOR = '#DA70D6'; //'Orchid';
public static string YELLOW_COLOR = '#FFFF66'; //'UNMELLOW YELLOW';
public static string WHITE_COLOR = '#FFFFFF'; //'WHITE';

public String sortExpression
           {
             get
             {
                return sortExp;
             }
             set
             {
               //if the column is clicked on then switch between Ascending and Descending modes
               if (value == sortExp)
           sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
           else
           sortDirection = 'ASC';
           sortExp = value;
              
             }
           }
           
           
public String getSortDirection()
         {
            //if not column is selected 
            if (sortExpression == null || sortExpression == '')
              return 'ASC';
            else
             return sortDirection;
         }
         
         public void setSortDirection(String value)
         {  
           sortDirection = value;
         }

    public Integer getSerReqListSize() {
        return serReqList != null ? serReqList.size() : 0;
    }
    
    public List<SerReq> getSerReqList() {
        Set<id> SerReqIds = new Set<id>();
        for(Service_Request_Employee__c TeamMember : [Select Id, Service_Request__r.id From Service_Request_Employee__c where Team_Member__r.User__c = :Userinfo.getUserId()])
        {SerReqIds.add(TeamMember.Service_Request__r.id);}
                  
        
        /*
        ###########################################################################
        #Start : TC ISD 1756718 - IFM home page & TC ISD 1846835 - Colour coding dates
        # Modified for..........: TC ISD 1756718 - IFM home page & TC ISD 1846835 - Colour coding dates
        # Last Modified by......: Umesh & Rajan
        # Last Modified Date....: 17-Aug-2016
        # Description .........: Querying for new 'Critical Notes' [Critical_Notes__c], Service_Address__c, City__c, Prov__c, DAC_Scheduled__c, DAC_Actual__c, RTI_Scheduled__c, RTI_Actual__c, PTD_Scheduled__c, PTD_Actual__c, DD_Actual__c, ERD_Scheduled__c, ERD_Actual__c, column in grid
                               
        ############################################################################
        */
        List<Holidays__c> holidayList = new List<Holidays__c>();
        
        SRList= [Select Id, Name, Opportunity__c, 
                 Contract__r.SFDC_Contract__r.ContractNumber, 
                 Contract__r.SFDC_Contract__r.complex_fulfillment_contract__r.new_econtract__c, 
                 Contract__r.SFDC_Contract__r.complex_fulfillment_contract__r.new_econtract__r.status,
                 Disconnect_Reason__c, Description__c,Workflow_Request_Type__c,Opportunity__r.name,Condition_Sort_Index__c,SRS_PODS_Product__r.Name,Order_ID__c,Account_Name__r.Name,Target_Date_Type__c,Target_Date__c,Fox__c,Jeopdary_Image__c, Jeopardy_Description__c, Service_Request_Type__c,Service_Request_Status__c,Requested_Date__c,RecordTypeId,Product_Type__c,LastModifiedDate,Submitted_Date__c,Last_Submitted_Date__c,DD_Scheduled__c,Legacy_Order_count__c,Condition__c,Condition_Color__c,Critical_Notes__c, Service_Address__c, City__c, Prov__c, DAC_Scheduled__c, DAC_Actual__c, RTI_Scheduled__c, RTI_Actual__c, PTD_Scheduled__c, PTD_Actual__c, DD_Actual__c, ERD_Scheduled__c, ERD_Actual__c,Next_to_work__c, CreatedDate,contract__c, (Select id,name,Full_Address__c,City__c,Province__c from Service_Address__r where Location__c = 'Location A') From Service_Request__c where (ownerid = :Userinfo.getUserId() or createdbyId = :Userinfo.getUserId()or Id IN :SerReqIds) and (Service_Request_Status__c Not IN('Cancelled','Completed')) limit 999 ]; 
        
        /*End : TC ISD 1756718 - IFM home page */
        
         
        // Use collect request IDs from service request, then get active (not cancelled) from contract based on Complex_Fulfillment_Contract__c to map the linked contract and status.
        set<id> contractRequestIds=new set<id>();
        for(Service_Request__c s:SRList){
            if(String.isNotBlank(s.contract__c)){
              contractRequestIds.add(s.contract__c);                
            }
        }
        
        list<contract> activeContracts=[select id, complex_fulfillment_contract__c, complex_fulfillment_contract__r.new_econtract__c,complex_fulfillment_contract__r.new_econtract__r.status,contractnumber,status from contract where status not in ('Cancelled') and complex_fulfillment_contract__c=:contractRequestIds];
        
        map<id,integer> contractRequestContractMap=new map<id,integer>();
        integer index=0;
        for(contract activeContract:activeContracts){
            contractRequestContractMap.put(activeContract.complex_fulfillment_contract__c,index);
            index++;
        }
        
        
        recordtypeMap = Schema.SObjectType.Service_request__c.getRecordTypeInfosById(); 
        SerReqList = new List<SerReq>();
        holidayList = Holidays__c.getAll().Values();
		
        Map<String, Schema.SObjectField> HolidayFieldMap = Schema.SObjectType.Holidays__c.fields.getMap();
        for(Service_Request__c s:SRList){
            SerReq req = new SerReq(s);

            if(contractRequestContractMap.containskey(s.contract__c)){
                contract activeContract=activeContracts.get(contractRequestContractMap.get(s.contract__c));
                if(activeContract!=null){
                    req.contractId=activeContract.complex_fulfillment_contract__r.new_econtract__c;
                    req.contractNumber=string.isnotblank(activeContract.complex_fulfillment_contract__r.new_econtract__c)?activeContract.contractnumber:'';
                    req.status=string.isnotblank(activeContract.complex_fulfillment_contract__r.new_econtract__r.status)?activeContract.complex_fulfillment_contract__r.new_econtract__r.status:'';
                }
            }
            
            
            if(null!=s.RecordTypeId)
                req.Recordtypename=recordtypeMap.get(s.RecordTypeId).getName();
            
            /*
            ###########################################################################
            #Start : TC ISD 1846835 - Colour coding dates
            # Modified for..........: TC ISD 1846835 - Colour coding dates
            # Last Modified by......: Rajan
            # Last Modified Date....: 17-Aug-2016
            # Description .........: Querying for new Service_Address__c, City__c, Prov__c, DAC_Scheduled__c, DAC_Actual__c, RTI_Scheduled__c, RTI_Actual__c, PTD_Scheduled__c, PTD_Actual__c, DD_Actual__c, ERD_Scheduled__c, ERD_Actual__c, column in grid
            ############################################################################
            */
        
            string province = null;
			IF(s.Service_Address__r != null && s.Service_Address__r.size() > 0){
                province = s.Service_Address__r[0].Province__c;                             
            }
			
			if(province!=Null && !HolidayFieldMap.containsKey((province+'__c').toLowerCase())){
				province='ON';
			}
			
								
            //system.debug('-------------calling color code for SR Number : ' + s.name);
            req.DAC_colorCode = getColorCode(s.DAC_Scheduled__c, s.DAC_Actual__c,null, province, holidayList);
            req.DAC_tooltip = getToolTipMsg(req.DAC_colorCode);
            
            req.ERD_colorCode = getColorCode(s.ERD_Scheduled__c, s.ERD_Actual__c,null, province, holidayList);
            req.ERD_tooltip = getToolTipMsg(req.ERD_colorCode);
            
            req.RTI_colorCode = getColorCode(s.RTI_Scheduled__c, s.RTI_Actual__c,null, province, holidayList);
            req.RTI_tooltip = getToolTipMsg(req.RTI_colorCode);
            
            req.PTD_colorCode = getColorCode(s.PTD_Scheduled__c, s.PTD_Actual__c,null, province, holidayList);
            req.PTD_tooltip = getToolTipMsg(req.PTD_colorCode);
            
            req.DD_colorCode = getColorCode(s.DD_Scheduled__c, s.DD_Actual__c, s.Jeopardy_Description__c,province, holidayList);
            req.DD_tooltip = getToolTipMsg(req.DD_colorCode);
            //End : TC ISD 1846835 - Colour coding dates
            
            SerReqList.add(req);
            system.debug('serReq'+ req);
        }
        SORT_DIR = sortDirection;
        SORT_FIELD =sortExpression;
        SerReqList.sort() ;    
        return SerReqList;
    }
    public List<Service_Request__c> getSRList() {
        return SRList;
    }
 
 public void SortRelatedListwithColumn()
    {
         
               doSort=true;          
               sortFullExp = '';
               //build the full sort expression
               sortFullExp = ' order by ' +sortExpression  + ' ' + sortDirection+' NULLS LAST ';
               
    } 
      
 public SRS_MyOpenSereviceRequests_Controller() 
    {       
      sortExpression='Condition_Sort_Index__c';
      sortDirection = 'DESC';
                        
    }   
 
 /*
        ###########################################################################
        #Start : TC ISD 1846835 - Colour coding dates
        # Modified for..........: TC ISD 1846835 - Colour coding dates
        # Last Modified by......: Rajan
        # Last Modified Date....: 17-Aug-2016
        # Description .........: Querying for new Service_Address__c, City__c, Prov__c, DAC_Scheduled__c, DAC_Actual__c, RTI_Scheduled__c, RTI_Actual__c, PTD_Scheduled__c, PTD_Actual__c, DD_Actual__c, ERD_Scheduled__c, ERD_Actual__c, column in grid
        ############################################################################
        */
    public String getToolTipMsg(String colorCode){
        if(!String.isBlank(colorCode)){
            if(colorCode.equalsIgnoreCase(GREEN_COLOR)){
               return label.SRS_GreenTooltip; 
            }else if(colorCode.equalsIgnoreCase(RED_COLOR)){
                return label.SRS_RedTooltip;
            }else if(colorCode.equalsIgnoreCase(ORANGE_COLOR)){
                return label.SRS_OrangeTooltip;
            }else if(colorCode.equalsIgnoreCase(PURPLE_COLOR)){
                return label.SRS_PurpleTooltip;
            }else if(colorCode.equalsIgnoreCase(YELLOW_COLOR)){
                return label.SRS_YellowTooltip;
            }else if(colorCode.equalsIgnoreCase(WHITE_COLOR)){
                return label.SRS_WhiteTooltip;
            }
            
        }
        return null;
    }
    
    // # Description .........: Find total number of working days excluding weekends for range of dates
    public Integer findNoOfWorkingDays( Date startDate , Date endDate ) {
        
        system.debug('startDate ====>>>'+startDate);
        system.debug('endDate ====>>>'+endDate);
        Integer NoOfDays = 0;
        system.debug('$$$$$$$$$$$$$$$$$$$$$$$ findNoOfWorkingDays start ==== NoOfDays ' + NoOfDays);
        Date tempStartDate = startDate;
        for( integer i = 1; tempStartDate < endDate; i ++ ) {
            if(tempStartDate.daysBetween(tempStartDate.toStartofWeek()) == 0 || 
                tempStartDate.daysBetween(tempStartDate.toStartofWeek()) == -6) {
                    tempStartDate = tempStartDate.adddays(1); 
                    continue;
            }
            NoOfDays = NoOfDays+1; 
            tempStartDate = tempStartDate.adddays(1);
        }
        system.debug('$$$$$$$$$$$$$$$$$$$$$$$ findNoOfWorkingDays end ==== NoOfDays ' + NoOfDays);
        return NoOfDays ;
    }

    // # Description .........: Find total number of holidays (public as well as province wise) for range of dates
    public Integer findNoOfHolidays(Date startDate , Date scheduleDate,String province,List<Holidays__c> holidays ){
        Integer intHolidayCount = 0;
        system.debug('findNoOfHolidays ==== intHolidayCount start ==== '+intHolidayCount);
        String provinceStr = '';
        string dayName = '';
        for(Holidays__c Holiday : holidays ){
            Datetime dt = DateTime.newInstance(Holiday.Date__c, Time.newInstance(0, 0, 0, 0));
            dayName = dt.format('EEEE');
            provinceStr = String.valueOf(holiday.get(province+'__c'));
            if((startDate.Year() == Holiday.Date__c.Year() || 
                (startDate.Year()+1 == Holiday.Date__c.Year() && Holiday.Date__c.Month() == 1 && 
                Holiday.Date__c.dayOfYear() >=1 && Holiday.Date__c.dayOfYear() <= 7)) && 
                dayName != 'Saturday' && dayName != 'Sunday' && provinceStr != null && provinceStr.equalsIgnoreCase('true') &&
                Holiday.ISD_Dispatch__c == TRUE){
                if(Holiday.Date__c >=  startDate && Holiday.Date__c <=  scheduleDate ){
                    /*system.debug('findNoOfHolidays ==== provinceStr = '+provinceStr);
                    system.debug('dayName ='+dayName);
                    system.debug('findNoOfHolidays ==== holidays.name = '+ holiday.name);
                    system.debug('findNoOfHolidays ==== holidays.Date__c = '+ holiday.Date__c);
                    system.debug('findNoOfHolidays ==== province = '+province);
                    system.debug('findNoOfHolidays ==== province.value = '+ holiday.get(province+'__c'));*/
                    intHolidayCount++;
                }
            }
        }
        system.debug('findNoOfHolidays ==== intHolidayCount end ==== '+intHolidayCount);
        return intHolidayCount;
    }

 public String getColorCode(Date scheduleDate, Date actualDate, String jeopardy_description, String province, List<Holidays__c> holidayList ){
     Integer intNoOfWorkingDays = 0;     
     Integer intNoHolidays = 0;
     Integer intTotalDays = 0;
    
        
    if(scheduleDate != null){
         //get number of working days in this period
         intNoOfWorkingDays = findNoOfWorkingDays(Date.TODAY(), scheduleDate);      
         //get number of province holidays in this period for the address specified in SR
         if(province != null){
            intNoHolidays = findNoOfHolidays(Date.TODAY(), scheduleDate, province, holidayList);        
         }      
         intTotalDays = intNoOfWorkingDays - (intNoHolidays);
    }
     /**
    **Condition -                                               **Technical Explanation -
    1. Milestone still approaching = white color                Actual Date is blank and more than 3 days are remaining for scheduled date
    2. Milestone Due Date within 3 day = purple color           Actual Date is blank and current date is within 3 day of scheduled date
    3. Milestone Due Date within 1 day = yellow color           Actual Date is blank and current date is within 1 day of scheduled date
    4. Milestone JEOP'd = orange color                          Actual Date is blank and jeopardy flag is set
    5. Milestone Missed = red color                             Actual Date is blank and current date is past scheduled date
    6. Milestone Due Date Completed = green color               Actual Date is present
    **/ 
        // jeopardy case - applicable as of now only for DD Scheduled
     if(!String.isBlank(jeopardy_description)){
      
         if(scheduleDate != null && Date.TODAY() > scheduleDate){
             return RED_COLOR;
         }else if(!String.isBlank(jeopardy_description)){
             return ORANGE_COLOR;
         }else if(scheduleDate != null && actualDate != null){
             return GREEN_COLOR;
         }else if(scheduleDate != null && (intTotalDays) <= 1){ //Date.TODAY().daysBetween(scheduleDate))
             return YELLOW_COLOR;
         }else if(scheduleDate != null && (intTotalDays) <= 3){ //Date.TODAY().daysBetween(scheduleDate)
             return PURPLE_COLOR;
         }else if(scheduleDate != null && (intTotalDays) > 3){ //Date.TODAY().daysBetween(scheduleDate)
             return WHITE_COLOR;
         }
         
     }else{
         // non jeopardy case for PTD, RTI, DAC, ERD (NON DD)
         if(scheduleDate != null && actualDate != null){
             return GREEN_COLOR;
         }else if(scheduleDate != null && Date.TODAY() > scheduleDate){
             return RED_COLOR;
         }else if(scheduleDate != null && (intTotalDays) <= 1){
             return YELLOW_COLOR;
         }else if(scheduleDate != null && (intTotalDays) <= 3){
             return PURPLE_COLOR;
         }else if(scheduleDate != null && (intTotalDays) > 3){
             return WHITE_COLOR;
         }
     }
     
     return null;
 }
 //End : TC ISD 1846835 - Colour coding dates
 
    public class SerReq implements Comparable{
        public Service_Request__c serReq {get; set;}
        public String Recordtypename{get; set;}
        public id contractId{get;set;}
        public string contractNumber{get;set;}
        public string status{get;set;}
        
        /*
        ###########################################################################
        #Start : TC ISD 1846835 - Colour coding dates
        # Modified for..........: TC ISD 1846835 - Colour coding dates
        # Last Modified by......: Rajan
        # Last Modified Date....: 17-Aug-2016
        # Description .........: Querying for new Service_Address__c, City__c, Prov__c, DAC_Scheduled__c, DAC_Actual__c, RTI_Scheduled__c, RTI_Actual__c, PTD_Scheduled__c, PTD_Actual__c, DD_Actual__c, ERD_Scheduled__c, ERD_Actual__c, column in grid
        ############################################################################
        */
        
        public string DAC_colorCode {get;set;}
        public string DAC_tooltip {get;set;}
        public string ERD_colorCode {get;set;}
        public string ERD_tooltip {get;set;}
        public string RTI_colorCode {get;set;}
        public string RTI_tooltip {get;set;}
        public string PTD_colorCode {get;set;}
        public string PTD_tooltip {get;set;}
        public string DD_colorCode {get;set;}
        public string DD_tooltip {get;set;}
        
        
        //End : TC ISD 1846835 - Colour coding dates
        
     
        public Integer compareTo(Object other) {
         if (SORT_FIELD == 'LastModifiedDate' || SORT_FIELD == 'Submitted_Date__c') 
         {
            return compareToSRDateTime(other); 
         } 
         else if (SORT_FIELD == 'DD_Scheduled__c' || SORT_FIELD =='Requested_Date__c' || SORT_FIELD =='Next_to_work__c' || SORT_FIELD =='ERD_Scheduled__c'
         || SORT_FIELD =='DAC_Scheduled__c' || SORT_FIELD =='RTI_Scheduled__c' || SORT_FIELD =='PTD_Scheduled__c') 
         {
            return compareToSRDate(other); 
         } else //if (SORT_FIELD=='Name'||SORT_FIELD== 'Fox__c'||SORT_FIELD=='Service_Request_Type__c'||SORT_FIELD=='SRS_PODS_Product__r.Name'||SORT_FIELD=='Legacy_Order_count__c'||SORT_FIELD=='Service_Request_Status__c') {
             return compareStringProperty(other); 
          return 0;
          
         }
                        
         // Compares SRString field
         Integer compareStringProperty(Object other) 
         {  
             String sortfield;
             String currInstField;
             
             system.debug('>>> SORT_FIELD :'+ SORT_FIELD);
             
            if(SORT_FIELD=='Name')
            {   
               
                 sortfield  =((serReq)other).serReq.Name  != null ?  ((serReq)other).serReq.Name : '';
                 currInstField  =this.serReq.Name  != null ?  this.serReq.Name: '';
            
            }
            else if(SORT_FIELD=='Fox__c')
            {
                
                 sortfield  =((serReq)other).serReq.Fox__c != null ?  ((serReq)other).serReq.Fox__c : '';
                 currInstField  =this.serReq.Fox__c != null ?  this.serReq.Fox__c: '';
            
            }
            else if(SORT_FIELD=='Service_Request_Type__c')
            {
                
                 sortfield  =((serReq)other).Recordtypename != null ?  ((serReq)other).Recordtypename: '';
                 currInstField  =this.Recordtypename != null ?  this.Recordtypename: '';
            
            }
            else if(SORT_FIELD=='Legacy_Order_count__c')
            {
                 
                 sortfield  = string.valueOf(((serReq)other).serReq.Legacy_Order_count__c != null ?  ((serReq)other).serReq.Legacy_Order_count__c : 0);
                 currInstField  = string.valueOf(this.serReq.Legacy_Order_count__c != null ?  this.serReq.Legacy_Order_count__c : 0);
            
            }
            else if(SORT_FIELD=='Service_Request_Status__c')
            {
                 sortfield  =((serReq)other).serReq.Service_Request_Status__c != null ?  ((serReq)other).serReq.Service_Request_Status__c : '';
                 currInstField  =this.serReq.Service_Request_Status__c != null ?  this.serReq.Service_Request_Status__c : '';
            
            }
            else if(SORT_FIELD=='Order_ID__c')
            {
                 sortfield  =((serReq)other).serReq.Order_ID__c != null ?  ((serReq)other).serReq.Order_ID__c : '';
                 currInstField  =this.serReq.Order_ID__c != null ?  this.serReq.Order_ID__c : '';
            
            }
            else if(SORT_FIELD=='Account_Name__r.name')
            {
                 sortfield  =((serReq)other).serReq.Account_Name__r.name != null ?  ((serReq)other).serReq.Account_Name__r.name : '';
                 currInstField  =this.serReq.Account_Name__r.name != null ?  this.serReq.Account_Name__r.name : '';
            
            }
            else if(SORT_FIELD=='Condition_Sort_Index__c')
            {
                 sortfield  =((serReq)other).serReq.Condition_Sort_Index__c != null ?  ((serReq)other).serReq.Condition_Sort_Index__c : '';
                 currInstField  =this.serReq.Condition_Sort_Index__c != null ?  this.serReq.Condition_Sort_Index__c : '';
            
            }
            else if(SORT_FIELD=='Jeopdary_Image__c')
            {
                 sortfield  =((serReq)other).serReq.Jeopdary_Image__c != null ?  ((serReq)other).serReq.Jeopdary_Image__c : '';
                 currInstField  =this.serReq.Jeopdary_Image__c != null ?  this.serReq.Jeopdary_Image__c : '';
            
            }
            else if(SORT_FIELD=='Product_Type__c')
            {
                 sortfield  =((serReq)other).serReq.Product_Type__c != null ?  ((serReq)other).serReq.Product_Type__c : '';
                 currInstField  =this.serReq.Product_Type__c != null ?  this.serReq.Product_Type__c : '';
            
            }
            else if(SORT_FIELD=='ContractNumber')
            {
                 sortfield=string.isnotblank(((serReq)other).ContractNumber)?((serReq)other).ContractNumber:'';
                 currInstField=string.isnotblank(this.ContractNumber)?this.ContractNumber:'';
            
            }
            else if(SORT_FIELD=='Status')
            {
                 sortfield  =((serReq)other).Status != null ?  ((serReq)other).Status : '';
                 currInstField  =this.status != null ?  this.status : '';
            
            }
             else if(SORT_FIELD=='Description__c')
            {
                 sortfield  =((serReq)other).serReq.Description__c != null ?  ((serReq)other).serReq.Description__c: '';
                 currInstField  =this.serReq.Description__c!= null ?  this.serReq.Description__c: '';
            
            }
            else if(SORT_FIELD=='Jeopdary_Image__c')
            {
                 sortfield  =((serReq)other).serReq.Jeopdary_Image__c != null ?  ((serReq)other).serReq.Jeopdary_Image__c : '';
                 currInstField  =this.serReq.Jeopdary_Image__c != null ?  this.serReq.Jeopdary_Image__c : '';
            
            }
            
            /*
        ###########################################################################
        #Start : TC ISD 1756718 - IFM home page & TC ISD 1846835 - Colour coding dates
        # Modified for..........: TC ISD 1756718 - IFM home page & TC ISD 1846835 - Colour coding dates
        # Last Modified by......: Umesh & Rajan
        # Last Modified Date....: 17-Aug-2016
        # Description .........: Querying for new 'Critical Notes' [Critical_Notes__c], Service_Address__c, City__c, Prov__c, DAC_Scheduled__c, DAC_Actual__c, RTI_Scheduled__c, RTI_Actual__c, PTD_Scheduled__c, PTD_Actual__c, DD_Actual__c, ERD_Scheduled__c, ERD_Actual__c, column in grid
        ############################################################################
        */   
            else if(SORT_FIELD=='Critical_Notes__c')
            {
                sortfield=string.isnotblank(((serReq)other).serReq.Critical_Notes__c)?((serReq)other).serReq.Critical_Notes__c:'';
                currInstField=string.isnotblank(this.serReq.Critical_Notes__c)?this.serReq.Critical_Notes__c:'';
            }
            else if(SORT_FIELD=='Service_Address__c')
            {
                sortfield=(((serReq)other).serReq.Service_Address__r != null && ((serReq)other).serReq.Service_Address__r.size() > 0 && string.isnotblank(((serReq)other).serReq.Service_Address__r[0].Full_Address__c))?((serReq)other).serReq.Service_Address__r[0].Full_Address__c:'';
                currInstField=(this.serReq.Service_Address__r != null && this.serReq.Service_Address__r.size() > 0 && string.isnotblank(this.serReq.Service_Address__r[0].Full_Address__c))? this.serReq.Service_Address__r[0].Full_Address__c:'';
            }
            else if(SORT_FIELD=='City__c')
            {
                sortfield=(((serReq)other).serReq.Service_Address__r != null && ((serReq)other).serReq.Service_Address__r.size() > 0 && string.isnotblank(((serReq)other).serReq.Service_Address__r[0].City__c))?((serReq)other).serReq.Service_Address__r[0].City__c:'';
                currInstField=(this.serReq.Service_Address__r != null && this.serReq.Service_Address__r.size() > 0 && string.isnotblank(this.serReq.Service_Address__r[0].City__c))? this.serReq.Service_Address__r[0].City__c:'';
            }
            else if(SORT_FIELD=='Prov__c')
            {
                sortfield=(((serReq)other).serReq.Service_Address__r != null && ((serReq)other).serReq.Service_Address__r.size() > 0 && string.isnotblank(((serReq)other).serReq.Service_Address__r[0].Province__c))?((serReq)other).serReq.Service_Address__r[0].Province__c:'';
                currInstField=(this.serReq.Service_Address__r != null && this.serReq.Service_Address__r.size() > 0 && string.isnotblank(this.serReq.Service_Address__r[0].Province__c))? this.serReq.Service_Address__r[0].Province__c:'';
            }
            else if(SORT_FIELD=='Target_Date_Type__c')
            {
                sortfield=string.isnotblank(((serReq)other).serReq.Target_Date_Type__c)?((serReq)other).serReq.Target_Date_Type__c:'';
                currInstField=string.isnotblank(this.serReq.Target_Date_Type__c)?this.serReq.Target_Date_Type__c:'';
            }
            else if(SORT_FIELD=='Opportunity__r.name')
            {
                sortfield=string.isnotblank(((serReq)other).serReq.Opportunity__r.name)?((serReq)other).serReq.Opportunity__r.name:'';
                currInstField=string.isnotblank(this.serReq.Opportunity__r.name)?this.serReq.Opportunity__r.name:'';
            }
            else if(SORT_FIELD=='Workflow_Request_Type__c')
            {
                sortfield=string.isnotblank(((serReq)other).serReq.Workflow_Request_Type__c)?((serReq)other).serReq.Workflow_Request_Type__c:'';
                currInstField=string.isnotblank(this.serReq.Workflow_Request_Type__c)?this.serReq.Workflow_Request_Type__c:'';
            }
            /*End : TC ISD 1756718 - IFM home page & TC ISD 1846835 - Colour coding dates */
            
            
             String otherSRString= other != null ?  sortfield : '';   
             if (SORT_DIR== 'ASC')     
               return currInstField.compareTo(otherSRString);   
             else
               return otherSRString.compareTo(currInstField);  
        
         }
         
         Integer compareToSRDateTime(Object other) 
         {
            DateTime sortfield;
            DateTime currInstField;
            
            if(SORT_FIELD=='LastModifiedDate')
            {
            
             sortfield  =((serReq)other).serReq.LastModifiedDate != null ?  ((serReq)other).serReq.LastModifiedDate: datetime.newInstance(1900, 01, 01);
             currInstField  =this.serReq.LastModifiedDate != null ?  this.serReq.LastModifiedDate : datetime.newInstance(1900, 01, 01);
            
            }
            else if(SORT_FIELD=='Submitted_Date__c')
            {
                sortfield  =((serReq)other).serReq.Submitted_Date__c != null ?  ((serReq)other).serReq.Submitted_Date__c : datetime.newInstance(1900,01, 01);
                currInstField  =this.serReq.Submitted_Date__c != null ?  this.serReq.Submitted_Date__c : datetime.newInstance(1900,01, 01);
            
            }
          
          DateTime otherDateTime = other != null ? sortfield  : datetime.newInstance(1900, 01, 01);   
          // use Datetime.getTime() to do get the numeric time in millis
          if (SORT_DIR == 'ASC')
           return (currInstField.getTime() - otherDateTime.getTime()).intValue();
          else
           return (otherDateTime.getTime() - currInstField.getTime()).intValue();
         }
         
         Integer compareToSRDate(Object other) 
         {    
             Date sortfield;
             Date currInstField;
            if(SORT_FIELD=='DD_Scheduled__c')
            {
                sortfield  =((serReq)other).serReq.DD_Scheduled__c != null ?  ((serReq)other).serReq.DD_Scheduled__c : date.newInstance(1900,01, 01);
                currInstField  =this.serReq.DD_Scheduled__c != null ?  this.serReq.DD_Scheduled__c : date.newInstance(1900,01, 01);
            
            }
            else if(SORT_FIELD=='Requested_Date__c')
            {
                sortfield  =((serReq)other).serReq.Requested_Date__c != null ?  ((serReq)other).serReq.Requested_Date__c : date.newInstance(1900,01, 01);
                currInstField  =this.serReq.Requested_Date__c != null ?  this.serReq.Requested_Date__c : date.newInstance(1900,01, 01);
            
            }
            
            /*
        ###########################################################################
        #Start : TC ISD 1846835 - Colour coding dates
        # Modified for..........: TC ISD 1846835 - Colour coding dates
        # Last Modified by......: Rajan
        # Last Modified Date....: 17-Aug-2016
        # Description .........: Querying for new Service_Address__c, City__c, Prov__c, DAC_Scheduled__c, DAC_Actual__c, RTI_Scheduled__c, RTI_Actual__c, PTD_Scheduled__c, PTD_Actual__c, DD_Actual__c, ERD_Scheduled__c, ERD_Actual__c, column in grid
        ############################################################################
        */
        
            else if(SORT_FIELD=='Next_to_work__c')
            {
                sortfield  =((serReq)other).serReq.Next_to_work__c != null ?  ((serReq)other).serReq.Next_to_work__c : date.newInstance(1900,01, 01);
                currInstField  =this.serReq.Next_to_work__c != null ?  this.serReq.Next_to_work__c : date.newInstance(1900,01, 01);
            
            }
            else if(SORT_FIELD=='ERD_Scheduled__c')
            {
                sortfield  =((serReq)other).serReq.ERD_Scheduled__c != null ?  ((serReq)other).serReq.ERD_Scheduled__c : date.newInstance(1900,01, 01);
                currInstField  =this.serReq.ERD_Scheduled__c != null ?  this.serReq.ERD_Scheduled__c : date.newInstance(1900,01, 01);
            
            }
            else if(SORT_FIELD=='DAC_Scheduled__c')
            {
                sortfield  =((serReq)other).serReq.DAC_Scheduled__c != null ?  ((serReq)other).serReq.DAC_Scheduled__c : date.newInstance(1900,01, 01);
                currInstField  =this.serReq.DAC_Scheduled__c != null ?  this.serReq.DAC_Scheduled__c : date.newInstance(1900,01, 01);
            
            }
            else if(SORT_FIELD=='RTI_Scheduled__c')
            {
                sortfield  =((serReq)other).serReq.RTI_Scheduled__c != null ?  ((serReq)other).serReq.RTI_Scheduled__c : date.newInstance(1900,01, 01);
                currInstField  =this.serReq.RTI_Scheduled__c != null ?  this.serReq.RTI_Scheduled__c : date.newInstance(1900,01, 01);
            
            }
            else if(SORT_FIELD=='PTD_Scheduled__c')
            {
                sortfield  =((serReq)other).serReq.PTD_Scheduled__c != null ?  ((serReq)other).serReq.PTD_Scheduled__c : date.newInstance(1900,01, 01);
                currInstField  =this.serReq.PTD_Scheduled__c != null ?  this.serReq.PTD_Scheduled__c : date.newInstance(1900,01, 01);
            
            }
            
          Date otherDate = other != null ? sortfield : date.newInstance(1900,01, 01);   
          // use Datetime.getTime() to do get the numeric time in millis
          if (SORT_DIR == 'ASC')
           return otherDate.daysBetween(currInstField);
           else
           return currInstField.daysBetween(otherDate);  
         }

      
      public SerReq(Service_Request__c sr)
      {
        serReq = sr;
      }
       public SerReq()
      {
        serReq = null;
      }
    } 
    
 }