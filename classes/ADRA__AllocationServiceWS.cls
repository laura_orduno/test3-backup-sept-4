/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class AllocationServiceWS {
    global AllocationServiceWS() {

    }
    webService static void allocate(ADRA.AllocationServiceWS.AllocateRequest request) {

    }
global class AllocateRequest {
    @WebService
    webService Boolean expiresOverride;
    @WebService
    webService List<ADRA.AllocationServiceWS.MatchResult> matchResults;
    @WebService
    webService String objectName;
    global AllocateRequest() {

    }
}
global class MatchResult {
    @WebService
    webService String baseRuleId;
    @WebService
    webService String extensionRuleId;
    @WebService
    webService String recordId;
    global MatchResult() {

    }
}
}
