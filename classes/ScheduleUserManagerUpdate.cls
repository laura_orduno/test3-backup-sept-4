global class ScheduleUserManagerUpdate implements Schedulable {
  
  global void execute(SchedulableContext SC) {
     RoleHierarchy  hierarchy = new RoleHierarchy() ;
     Database.executeBatch(hierarchy);
  }
  
}