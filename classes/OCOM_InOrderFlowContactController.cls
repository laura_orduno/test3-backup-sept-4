/*******************************************************************************
     * Project      : OCOM/US-1138
     * Helper       : OCOM_InOrderFlowContactController
     * Test Class   : OCOM_InOrderFlowContactController_Test
     * Description  : Apex class for OCOM_InOrderFlowPAC VF page that allows Agents to capture Contact inside OrderFlow
     * Author       : Arvind Yadav
     * Date         : July-18-2016
     *
     * Modification Log:
     * ------------------------------------------------------------------------------
     * Ver      Date                Modified By           Description
     * ------------------------------------------------------------------------------
     * 1.0     
     * 1.1   Aug-9-2016          Wael Elkhawass           OCOM-1202
     * 1.2   Aug-18-2016         Wael Elkhawass           OCOM-1207
     *******************************************************************************/

public without sharing class OCOM_InOrderFlowContactController { 
    public Account ServiceAccount  {get;set;} 
    public String oppId {get;set;}
    public String primaryContactId {get;set;}
    public String ContId{get;set;}
    public String typeOfOrder{get;set;}
    public id parentAccId;
    // Added by Wael - Start OCOM-1202  & OCOM-1207
    public String smbCareAddrId {get; set;}
    public String moveOutAddrId {get; set;}
    public String moveOutAccountId {get; set;}
    public String existingOrderId {get; set;}
    public String src {get;set;}
    public Boolean displayPageComp{get;set;}
    // Wael - End OCOM-1202 & OCOM-1207

    // fhong - outside order flow origination
    public String addressOrigin{get;set;}
    public Boolean addrNotValid{get;set;}
    public String tvAvailability{get;set;}
    public SMBCare_Address__c srvcAddress = new SMBCare_Address__c();
    public string forborne{get;set;}
    public String maximumSpeed{get;set;}
    public Boolean maxSpeedCopper{get;set;}
    
    private List<String> caseIds = new List<String>();

    public OCOM_InOrderFlowContactController(){
        String acctIdStr=System.currentPageReference().getParameters().get('id');
        Id acctId=null;
        if(acctIdStr instanceof Id){
            acctId=Id.valueOf(acctIdStr);
        }
       // Id acctId = System.currentPageReference().getParameters().get('id');
        String parentAccIdStr = System.currentPageReference().getParameters().get('ParentAccId');
        parentAccId=null;
        if(String.isNotBlank(parentAccIdStr) && parentAccIdStr instanceof Id){
           parentAccId = Id.valueOf(parentAccIdStr); 
        }
        //parentAccId = System.currentPageReference().getParameters().get('ParentAccId');
        oppId=null;
        String oppIdStr=System.currentPageReference().getParameters().get('oppId');
        if(String.isNotBlank(oppIdStr) && oppIdStr instanceof Id){
           oppId= oppIdStr;
        }
        primaryContactId=null;
        String primaryContactIdStr=System.currentPageReference().getParameters().get('primaryContactId');
        if(String.isNotBlank(primaryContactIdStr) && primaryContactIdStr instanceof Id){
            primaryContactId=primaryContactIdStr;
        }
        // Added by Wael - Start OCOM-1202  & OCOM-1207
        String smbCareAddrIdStr=System.currentPageReference().getParameters().get('smbCareAddrId');
        smbCareAddrId=null;
        if(String.isNotBlank(smbCareAddrIdStr) && smbCareAddrIdStr instanceof Id){
            smbCareAddrId=smbCareAddrIdStr;
        }
       // smbCareAddrId = System.currentPageReference().getParameters().get('smbCareAddrId');
        
        String moveOutAddrIdStr=System.currentPageReference().getParameters().get('moveOutAddrId');
        moveOutAddrId=null;
        if(String.isNotBlank(moveOutAddrIdStr) && moveOutAddrIdStr instanceof Id){
            moveOutAddrId=moveOutAddrIdStr;
        }
        //moveOutAddrId = System.currentPageReference().getParameters().get('moveOutAddrId');
        String moveOutAccountIdStr = System.currentPageReference().getParameters().get('moveOutAccountId');
        moveOutAccountId=null;
        if(String.isNotBlank(moveOutAccountIdStr) && moveOutAccountIdStr instanceof Id){
            moveOutAccountId= moveOutAccountIdStr;
        }
       // moveOutAccountId = System.currentPageReference().getParameters().get('moveOutAccountId');
        String cIds = System.currentPageReference().getParameters().get('caseIds');
        if (String.isNotBlank(cIds)) caseIds = cIds.split(':');
       
        String contIdStr = System.currentPageReference().getParameters().get('contactId');
        contId=null;
        if(String.isNotBlank(contIdStr) && contIdStr instanceof Id){
           contId= contIdStr;
        }
        //contId = System.currentPageReference().getParameters().get('contactId');
        String existingOrderIdStr = System.currentPageReference().getParameters().get('orderId');
        existingOrderId=null;
        if(String.isNotBlank(existingOrderIdStr) && existingOrderIdStr instanceof Id) {
            existingOrderId=existingOrderIdStr;
        } 
        //existingOrderId = System.currentPageReference().getParameters().get('orderId');
        if(existingOrderId != null && existingOrderId != ''){
            displayPageComp =true;
        }
        else {
            displayPageComp = false;
            addrNotValid = false;
            tvAvailability = '';
            forborne = '';
            maxSpeedCopper = false;
        }
        src= System.currentPageReference().getParameters().get('src');
        // Wael - End OCOM-1202 & OCOM-1207
        typeOfOrder = System.currentPageReference().getParameters().get('OrderType');
        if(acctId==null){
            acctId = System.currentPageReference().getParameters().get('aid');
        }

        if(acctId!=null){
            List<Account> account_list = [SELECT Id, parentId, parent.Name,Name, (SELECT ID, Service_Account_Id__c, Service_Account_Id__r.Id,
                                            FMS_Address_ID__c, Send_to_SACG__c, isTVAvailable__c, Forbone__c, Maximum_Speed__c, Status__c 
                                            FROM Addresses2__r WHERE ID =:smbCareAddrId  ) FROM Account WHERE id =  :acctId];
                        
            for (Account srvcAcc : account_list){
                ServiceAccount = srvcAcc;
                if(srvcAcc.Addresses2__r != null && srvcAcc.Addresses2__r.size() > 0){
                    srvcAddress = srvcAcc.Addresses2__r[0];
                }
            }
            if(srvcAddress != null){
                addrNotValid = (srvcAddress.Status__c != null && srvcAddress.Status__c != 'Valid')? true:false;
                tvAvailability = srvcAddress.isTVAvailable__c == true? Label.OCOM_TV_Available : Label.OCOM_TV_Not_Available ; 
                forborne = srvcAddress.Forbone__c == true? Label.OCOM_Forborne : Label.OCOM_Non_Forborne;
                maximumSpeed = srvcAddress.Maximum_Speed__c != null ? srvcAddress.Maximum_Speed__c : '';
                maxSpeedCopper = maximumSpeed == '' ? true:false;
             }
             else {
                tvAvailability = '' ; 
                forborne = '';
                maximumSpeed = '';
                maxSpeedCopper = false;
             }
        }

        // fhong - outside order flow origination
        addressOrigin = System.currentPageReference().getParameters().get('addressOrigin');
        
       // Added by Wael - Start OCOM-1202  & OCOM-1207
       
       if(src=='os' && !String.isBlank(existingOrderId)){
            Order theOrder = [SELECT Id, CustomerAuthorizedBy.Id, Service_Address__c, Service_Address__r.Id, AccountId FROM Order WHERE Id=: existingOrderId ];
            if(theOrder != null){
                // displayPageComp =true;
                contId = theOrder.CustomerAuthorizedBy.Id; 
                if( theOrder.Service_Address__c!=null){
                    SMBCare_Address__c theAddress = [SELECT Id, Service_Account_Id__c, Service_Account_Id__r.Id,FMS_Address_ID__c,Send_to_SACG__c,isTVAvailable__c FROM SMBCare_Address__c WHERE Id=: theOrder.Service_Address__r.Id LIMIT 1 ];
                    if(theAddress !=null){
                        smbCareAddrId = theAddress.Id;
                        List<Account> acc_list = [SELECT Id, parentId,parent.Name, Name FROM Account where id =: theAddress.Service_Account_Id__r.Id];
                        Account theAccount = new Account();
                        if(acc_list.size()==1){
                            theAccount = acc_list.get(0); 
                        }
                        acctId = theAccount.Id;
                        // addrNotValid = theAddress.FMS_Address_ID__c != null ? true:false;
                        // tvAvailability = theAddress.isTVAvailable__c == true? Label.OCOM_TV_Available : Label.OCOM_TV_Not_Available ;    
                    } else {
                        smbCareAddrId = '';
                        //tvAvailability = '' ; 
                    }
                }
                parentAccId = theOrder.AccountId;
            }
            if(acctId!=null){
                ServiceAccount = [SELECT Id, parentId,parent.Name, Name FROM Account where id =: acctId];
            }
        } 
    // Wael - End OCOM-1202 & OCOM-1207
    }

    // Vlocity Adding Create Order here
    // Wael renamed the method from CreateOrder to CreateUpdateOrder OCOM-1202/OCOM-1207
    public PageReference CreateUpdateOrder(){
        // Wael - start of code required for OCOM-1202 add the if condition
        String passedOrderId = '';
        if(existingOrderId==null || existingOrderId=='null' || existingOrderId==''){
              
            // here create order
            Id accountId = ServiceAccount.id;
            vlocity_cmt.VlocityOpenInterface vlOpenInt = new OCOM_HybridCPQUtil();
            Map<String, Object> options = new Map<String, Object>();
            Map<String, Object> input = new Map<String, Object>();
            Map<String, Object> output = new Map<String, Object>();

            // RCID Id
            // input.put('RCIDAcctId',);
            input.put('ParentAccId',parentAccId);
            // Service Adderess
            input.put('ServiceAcctId', ServiceAccount.Id);
            // Contact Id
            if(String.isBlank(ContId)){
                ContId=primaryContactId;
            }
            input.put('ContactId', ContId);
            input.put('oppId', oppId);
            // Order Type
            if(typeOfOrder != null  &&  typeOfOrder != '')
            input.put('Type', typeOfOrder);
            // Move Out Service Adderess
            system.debug('*************!moveOutAccountId:' + this.moveOutAccountId);
            if (String.isNotBlank(this.moveOutAccountId)) input.put('MoveOutServiceAcctId', this.moveOutAccountId);

            vlOpenInt.invokeMethod('createOcomOrder', input, output, options);
            
            Id OrderId = (Id)output.get('OrderId');
            System.debug('######' + OrderId);
            //return new PageReference('/apex/TESTING?ContId='+ContId + '&AccountId='+ServiceAccount.Id);
            //PageReference pg =  new PageReference('/apex/OCOM_InOrderFlowContact?id='+ServiceAccount.Id +'&OrderType='+typeOfOrder+ '&OrderId='+OrderId);
            //PageReference pg =  new PageReference('/apex/Order_PAC_Address?id='+OrderId+'&sfdc.override=1');
            //
            // Page redirects to HYBRID CPQ -Pete H.
            // Temp Change by Wael passing ServiceAcctId and ParentAccId in addition to OrderId that was initially passed, added ContactId as well
                  
            // Wael start
            passedOrderId = '' + OrderId; 
            // Wael end
             
        } else {
            passedOrderId = existingOrderId;
            Order order_to_update = [SELECT Id FROM Order WHERE Id=:existingOrderId ];
            order_to_update.CustomerAuthorizedById = ContId;
            order_to_update.CustomerSignedBy__c = ContId;
            order_to_update.Service_Address__c = smbCareAddrId;
            if(typeOfOrder != null &&  typeOfOrder != '')
            order_to_update.Type =  typeOfOrder ;
            if (typeOfOrder == 'Move' && String.isNotBlank(moveOutAddrId)) {
                order_to_update.MoveOutServiceAddress__c = moveOutAddrId;
            } else {
                order_to_update.MoveOutServiceAddress__c = null;
            }
                
            
            SMBCare_Address__c smbCareAddr = [SELECT Id, Suite_Number__c, Address__c, City__c, Province__c, Postal_Code__c, Country__c
                                              FROM SMBCare_Address__c WHERE Id=: smbCareAddrId];
            if(smbCareAddr !=null){
                order_to_update.Service_Address_Text__c = 
                                                            (smbCareAddr.Suite_Number__c == null ? '' : smbCareAddr.Suite_Number__c) + ' ' + 
                                                            (smbCareAddr.Address__c == null ? '' : smbCareAddr.Address__c) + ' ' + 
                                                            (smbCareAddr.City__c == null ? '' : smbCareAddr.City__c) + ' ' +
                                                            (smbCareAddr.Province__c == null ? '' : smbCareAddr.Province__c) + ' ' + 
                                                            (smbCareAddr.Postal_Code__c == null ? '' : smbCareAddr.Postal_Code__c) + ' ' + 
                                                            (smbCareAddr.Country__c == null ? '' : smbCareAddr.Country__c);
            }
            update order_to_update;
        }
        
        updateOrderAttachment(oppId,passedOrderId);
        linkOrderToCase(passedOrderId);
        
        // Before Wael changes for OCOM-1202
        // PageReference pg =  new PageReference('/apex/ocom_hybridcpq?id='+ OrderId + '&ServiceAcctId=' + ServiceAccount.Id + '&ParentAccId=' + ParentAccId + '&smbCareAddrId=' + smbCareAddrId + '&contactId=' + contId);
        // After Wael Changes for OCOM-1202 - Start
        //PageReference pg =  new PageReference('/apex/ocom_hybridcpq?id='+ passedOrderId + '&ServiceAcctId=' + ServiceAccount.Id + '&ParentAccId=' + ParentAccId + '&smbCareAddrId=' + smbCareAddrId + '&contactId=' + contId);
        // Wael End for OCOM-1202
        // Peter Adding navigation back to OOTB instead
        // PageReference pg =  new PageReference('/apex/vlocity_cmt__hybridcpq?id='+ passedOrderId + '&ServiceAcctId=' + ServiceAccount.Id + '&ParentAccId=' + ParentAccId + '&smbCareAddrId=' + smbCareAddrId + '&contactId=' + contId);
        PageReference pg =  new PageReference('/apex/ocom_hybridcpq?id='+ passedOrderId + '&ServiceAcctId=' + ServiceAccount.Id + '&ParentAccId=' + ParentAccId + '&smbCareAddrId=' + smbCareAddrId + '&contactId=' + contId + '&addressOrigin=' + addressOrigin);
            
        pg.setredirect(true);
        return pg;
    }
    private void updateOrderAttachment(String oppId,String orderId){
        if(String.isBlank(oppId) || String.isBlank(orderId)){
            return;
        }
            List<Attachment> attachmentOpList=[SELECT Id, Name, Body, OwnerId, CreatedById, CreatedDate, LastModifiedDate, LastModifiedById, Description, BodyLength, ContentType, ParentId FROM Attachment where ParentId=:oppId];
            List<Attachment> attachmentOrdList=new List<Attachment>();
            if(attachmentOpList!=null){
                for(Attachment opAttachObj:attachmentOpList){
                    Attachment ordAttachObj=opAttachObj.clone(false,true,false,false);
                    ordAttachObj.ParentId=orderId;
                    attachmentOrdList.add(ordAttachObj);
                }
            }
            insert attachmentOrdList;
        
    }
    private void linkOrderToCase(String orderId) {
        if (caseIds.size() > 0) {
            List<Case> cases = [select order__c, accountId from Case where Id in :caseIds];
            for (Case c : cases) {
                c.order__c = orderId;
                c.accountId = parentAccId;
                update c;
            }
        }
    }

    public void settypeOfOrder(String typeOfOrder) { this.typeOfOrder = typeOfOrder; }
    
    public PageReference onSelectContact() {
        return null;
    }
}