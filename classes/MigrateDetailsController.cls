public with sharing class MigrateDetailsController {
    public Id dealSupportId {get; set;}
    public Id selectDealSupportId {get; set;}
    public List<Offer_House_Demand__c> dealSupportList {get; set;}
    public MigrateDetailsController(ApexPages.StandardController controller) {
        dealSupportId = (Id)controller.getId();
        Offer_House_Demand__c dealSupportNew = [SELECT id, Existing_Contract__c FROM Offer_House_Demand__c WHERE id =: dealSupportId limit 1];
        
        if(dealSupportNew.Existing_Contract__c != null) {
            dealSupportList = [SELECT id, Name, Owner.Name, Liability_Type__c, 
                                      TRP__c, TLC__c, DCC__c, Late_Payment_Charges__c,  
                                      Credit_Assessment__c, Existing_BAN_Nbrs__c,
                                        REQUEST_TYPE__c, Product_Type__c, Subscriber_Term_Eligibility__c 
                               FROM Offer_House_Demand__c 
                               WHERE Contract__c =: dealSupportNew.Existing_Contract__c  
                                       ORDER BY CreatedDate DESC];          
        }
    }    
    @RemoteAction
    public static String goMigrateDetails(String dsId, String dsIdUpd) {        
        Savepoint sp = Database.setSavepoint();
        try {
                        
            List<Offer_House_Demand__c> dealSupportNewList = [SELECT id, Liability_Type__c, TRP__c, TLC__c, DCC__c, Late_Payment_Charges__c, Credit_Assessment__c, Existing_BAN_Nbrs__c, REQUEST_TYPE__c, Product_Type__c, Subscriber_Term_Eligibility__c FROM Offer_House_Demand__c WHERE Id =: dsIdUpd limit 1];
            List<Offer_House_Demand__c> dealSupportOldList = [SELECT id, Liability_Type__c, TRP__c, TLC__c, DCC__c, Late_Payment_Charges__c, Credit_Assessment__c, Existing_BAN_Nbrs__c, REQUEST_TYPE__c, Product_Type__c, Subscriber_Term_Eligibility__c FROM Offer_House_Demand__c WHERE Id =: dsId limit 1];
            
            if(dealSupportNewList.size() > 0 && dealSupportOldList.size() > 0) {
                Offer_House_Demand__c dealSupportNew = dealSupportNewList[0];
                Offer_House_Demand__c dealSupportOld = dealSupportOldList[0];
                
                dealSupportNew.Liability_Type__c = dealSupportOld.Liability_Type__c;
                dealSupportNew.TRP__c = dealSupportOld.TRP__c;
                dealSupportNew.TLC__c = dealSupportOld.TLC__c;
                dealSupportNew.DCC__c = dealSupportOld.DCC__c;          
                dealSupportNew.Late_Payment_Charges__c = dealSupportOld.Late_Payment_Charges__c;
                dealSupportNew.Credit_Assessment__c = dealSupportOld.Credit_Assessment__c;
                dealSupportNew.Existing_BAN_Nbrs__c = dealSupportOld.Existing_BAN_Nbrs__c; 
                dealSupportNew.REQUEST_TYPE__c = dealSupportOld.REQUEST_TYPE__c;
                dealSupportNew.Product_Type__c = dealSupportOld.Product_Type__c;
                dealSupportNew.Subscriber_Term_Eligibility__c = dealSupportOld.Subscriber_Term_Eligibility__c;                                         
                update dealSupportNewList;
                
                String expr = MigrateDetailsController.getAllFields('Rate_Plan_Item__c').get('Rate_Plan_Item__c');          
                String queryOld = 'SELECT id, Name_TPL__c, Parent_Rate_Plan_Item__r.Name_TPL__c, ' + expr + ', (SELECT Name_TPL__c, ' + expr + ' FROM Rate_Plans_Items__r)' + ' FROM Rate_Plan_Item__c WHERE Deal_Support__c = ' + '\'' + dealSupportOld.id + '\'';
                List<Rate_Plan_Item__c> queryOldRatePlanItems = Database.query(queryOld);     
                String queryNew = 'SELECT id, Name_TPL__c, Parent_Rate_Plan_Item__r.Name_TPL__c, ' + expr + ', (SELECT Name_TPL__c, ' + expr + ' FROM Rate_Plans_Items__r)' + ' FROM Rate_Plan_Item__c WHERE Deal_Support__c = ' + '\'' + dealSupportNew.id + '\'';
                List<Rate_Plan_Item__c> queryNewRatePlanItems = Database.query(queryNew);
                List<Rate_Plan_Item__c> forDeleteRatesList = new List<Rate_Plan_Item__c>();
                
                for(Rate_Plan_Item__c newItem : queryNewRatePlanItems) {
                    forDeleteRatesList.addall(newItem.Rate_Plans_Items__r);
                }
                
                delete forDeleteRatesList;
                
                List<Rate_Plan_Item__c> forInsertList = new List<Rate_Plan_Item__c>();
                set<id> deDuplicate = new set<Id>();
                for(Rate_Plan_Item__c oldItem : queryOldRatePlanItems) {
                    system.debug('1111111111111 ' + oldItem.id + ' ' + oldItem.Name_TPL__c);
                    if(oldItem.Rate_Plans_Items__r.size() > 0) {
                        for(Rate_Plan_Item__c newItem : queryNewRatePlanItems) {
                            
                            if(oldItem.Name_TPL__c == newItem.Name_TPL__c && !deDuplicate.contains(newItem.id)) {
                                system.debug('222222222222222 ' + oldItem.id + ' ' + oldItem.Name_TPL__c + ' ' + newItem.id + ' ' + newItem.Name_TPL__c);
                                system.debug('333333333333333333 ' + deDuplicate);
                                deDuplicate.add(newItem.id);
                                for(Rate_Plan_Item__c subOldItem : oldItem.Rate_Plans_Items__r) {
                                    system.debug('4444444444444444 ' + subOldItem.id);
                                    Rate_Plan_Item__c rpi = new Rate_Plan_Item__c();
                                    rpi = subOldItem.clone();
                                    //rpi.id = null;
                                    rpi.Parent_Rate_Plan_Item__c = newItem.id;
                                    forInsertList.add(rpi);
                                }
                            }
                        }
                    }
                }
                system.debug('forInsertList for test: ' + forInsertList);
                insert forInsertList;
            }
            
            return 'Good';
        } catch(Exception e) {          
            Database.rollback(sp);
            return e.getMessage();          
        }
        
    }
    public static Map<String, String> getAllFields(String sobjectname) {
        if(!Schema.getGlobalDescribe().containsKey(sobjectname)) {
            return new Map<String, String>{'Exception' => 'Invalid object name'};
        } 
        
        Map<String, Schema.SObjectField> fields = Schema.getGlobalDescribe().get(sobjectname).getDescribe().SObjectType.getDescribe().fields.getMap(); 
        List<String> updateablefields = new List<String>(); 
         
        for(Schema.SObjectField field : fields.values()){
            if(field.getDescribe().isUpdateable())
                updateablefields.add(field.getDescribe().getName());
        }
  
        String allfields='';
        
        for(String fieldname : updateablefields) {
            allfields += fieldname+',';
        }
        
        allfields = allfields.subString(0,allfields.length()-1);
        return new Map<String, String>{sobjectname => allfields};
    }   
}