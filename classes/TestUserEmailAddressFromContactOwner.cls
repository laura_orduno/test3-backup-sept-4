@isTest
private class TestUserEmailAddressFromContactOwner{
     static testmethod void testCodeCoverage(){
         // Update Contact -Start
            Contact con = new Contact(
                 LastName= 'Test123'
            );
            insert con;             
            Contact ConObj = [select OwnerId, Contact_Owner_Email__c from Contact where Id = :con.Id];
            System.debug('con Obj ='+conObj );
            if(conObj != null){
                User u= [select Email from User where Id = :conObj.OwnerId];
                System.assertEquals(u.Email, conObj.Contact_Owner_Email__c);
            } 
     }
}