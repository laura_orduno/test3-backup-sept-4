public class ContractDocumentDownloadPreviewCtrlExt{
  public Sobject currentObj {get;set;} 
  private  ApexPages.StandardController controller;
  public Id contractId  {get;set;}
  public Id attachmentId {get;set;}


  public ContractDocumentDownloadPreviewCtrlExt(ApexPages.StandardController stdController){
    controller = stdController;
    currentObj = stdController.getRecord();
    contractId = currentObj.Id;
    vlocity_cmt__ContractVersion__c contractDoc = [Select Id From vlocity_cmt__ContractVersion__c where vlocity_cmt__ContractId__c =: currentObj.Id and vlocity_cmt__Status__c = 'Active' Limit 1 ];
    if(contractDoc!= null){
      Attachment att =[SELECT Name,Id, parentId, Body, ContentType FROM Attachment where parentId = :contractDoc.Id LIMIT 1];
      if(att != null){
        attachmentId = att.id;
      }
    }
  }
}