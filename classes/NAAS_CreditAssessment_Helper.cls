/****************************************************
    @author             : Arpit Goyal
    Release / Sprint    : MVP Release / Sprint - 14
    Purpose             : Helper class for NAAS Credit assessment
****************************************************/

global with sharing class NAAS_CreditAssessment_Helper  implements vlocity_cmt.VlocityOpenInterface2 {
    
     public Boolean invokeMethod(String methodName, Map<String, Object> input, Map<String, Object> output, Map<String, Object> options)  {
        if (methodName.equals('creditCheckValidation')){
             creditCheckValidation(input, output, options);
             System.debug('Method called----> creditCheckValidation');
             return true;
        }
        return false;
     }

     public static  void creditCheckValidation(Map<String, Object> input, Map<String, Object> output, Map<String, Object> options) {
        Boolean isCarRequired = true;
        Boolean continueChecking = true;
        Boolean noCreditProfile = false;
        Boolean amendCar = false;
        Boolean isAmendOrder = false;
        Boolean isChangeOrder = false;
        Boolean validationError = false;
        Boolean existingCar = false;
        // Thomas OCOM-923
        Boolean moreThan10PercentChange = false;
        // Thomas OCOM-1331 Start
        Boolean hasContractableOffer = false;
        Decimal recurringTotal = 0.00;
        Decimal oneTimeTotal = 0.00;
        Decimal totalDiscount = 0.00;
        // Thomas OCOM-1331 End
        // Thomas Performance
        Integer newLineItemsCount = 0;
        Order orderObj;
        String validationFailureMessage='';
        Id orderId = (Id)input.get('ContextId');
        
        system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() credit check start');
        
        List<Order> orderObjList = [SELECT Id, Type, AccountId, Account.Credit_Profile__c, Credit_Assessment_ID__c, Credit_Assessment_Status__c, 
                                    requesteddate__c, Ban__c, Shipping_Address__c, Credit_Check_Not_Required__c, Credit_Reference_Number__c, 
                                    Rush__c, status, Shipping_Address1__c, Service_Address__c, ShipToContactId, BillToContactId, vlocity_cmt__ValidationMessage__c, 
                                    vlocity_cmt__ValidationStatus__c, vlocity_cmt__ValidationDate__c, vlocity_cmt__OneTimeTotal__c, vlocity_cmt__RecurringTotal__c, 
                                    vlocity_cmt__TotalMonthlyDiscount__c, vlocity_cmt__TotalOneTimeDiscount__c, orderMgmtId_Status__c 
                                    FROM Order
                                    WHERE id=:orderId limit 1];
        if(orderObjList!=null && !orderObjList.isEmpty()){
            orderObj=orderObjList.get(0);                                      
            system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() orderObj (' + orderObj + ')');
            
            // Thomas OCOM-1331 Start
            // retrieve list of line items
            Boolean proceed;
            // Thomas Performance
            Decimal totalAddAmount = 0.00;
            Decimal totalExistingAmount = 0.00;
            for(List<OrderItem> orderItemList:[SELECT Id, Credit_Assessment_Required__c, vlocity_cmt__RecurringTotal__c, vlocity_cmt__OneTimeTotal__c, vlocity_cmt__ProvisioningStatus__c, amendStatus__c FROM OrderItem WHERE orderid=:orderObj.id]) {
                system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() orderItemList(' + orderItemList + ')');
                // iterate through list of line items
                for (OrderItem orderItemObj: orderItemList) {
                    // Thomas Performance Start
                    if (orderItemObj.amendStatus__c == 'Add' && orderItemObj.Credit_Assessment_Required__c != 'No') {
                        if (orderItemObj.vlocity_cmt__RecurringTotal__c != null)
                            totalAddAmount += orderItemObj.vlocity_cmt__RecurringTotal__c;
                        if (orderItemObj.vlocity_cmt__OneTimeTotal__c != null)
                            totalAddAmount += orderItemObj.vlocity_cmt__OneTimeTotal__c;
                    } else if (orderItemObj.amendStatus__c != 'Add' && orderItemObj.Credit_Assessment_Required__c != 'No') {
                        if (orderItemObj.vlocity_cmt__RecurringTotal__c != null)
                            totalExistingAmount += orderItemObj.vlocity_cmt__RecurringTotal__c;
                        if (orderItemObj.vlocity_cmt__OneTimeTotal__c != null)
                            totalExistingAmount += orderItemObj.vlocity_cmt__OneTimeTotal__c;
                    }
                    if (orderItemObj.vlocity_cmt__ProvisioningStatus__c == 'New') {
                        newLineItemsCount++;
                    }
                    // Thomas Performance End
                    // check if line item should be removed/ignored/bypassed from credit assessment check
                    proceed = true;
                    if (orderItemObj.Credit_Assessment_Required__c != null) {
                        if (orderItemObj.Credit_Assessment_Required__c == 'No') {
                            proceed = false;
                        }
                    }
                    if (proceed == true) {
                        if (orderItemObj.vlocity_cmt__RecurringTotal__c != null) {
                            if (orderItemObj.vlocity_cmt__RecurringTotal__c > 0) {
                                // calculate MRC
                                recurringTotal += orderItemObj.vlocity_cmt__RecurringTotal__c;
                            } else {
                                // calculate total discount
                                totalDiscount += orderItemObj.vlocity_cmt__RecurringTotal__c;
                            }
                        }
                        if (orderItemObj.vlocity_cmt__OneTimeTotal__c != null) {
                            if (orderItemObj.vlocity_cmt__OneTimeTotal__c > 0) {
                                // calculate NRC
                                oneTimeTotal += orderItemObj.vlocity_cmt__OneTimeTotal__c;
                            } else {
                                // calculate total discount
                                totalDiscount += orderItemObj.vlocity_cmt__OneTimeTotal__c;
                            }
                        }
                    }
                }
                system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() recurringTotal('+recurringTotal+') oneTimeTotal('+oneTimeTotal+') totalDiscount('+totalDiscount+')');
            }
            // Thomas OCOM-1331 End
            
            // Thomas Performance
            // check if there are no order items in New provisioning status, there will be no items to be added to the CAR, therefore CAR is not required
            system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() newLineItemsCount('+newLineItemsCount+')');

            // check if account has a credit profile
            if (orderObj.Account.Credit_Profile__c == null) {
                noCreditProfile = true;
            }
            system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() noCreditProfile: ' + noCreditProfile);
            
            // check if order has an existing credit assessment
            Credit_Assessment__c aCreditAssessment = null;
            system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() orderObj.Credit_Assessment_ID__c: ' + orderObj.Credit_Assessment_ID__c);
            if (orderObj.Credit_Assessment_ID__c != null) {
                existingCar = true;
                // retrieving MRC and NRC
                List<Credit_Assessment__c> creditAssessmentList = [SELECT Total_Deal_Value__c, Total_Deal_Monthly_Recurring_Charges__c, Total_Deal_Non_Recurring_Charges__c FROM Credit_Assessment__c WHERE id=:orderObj.Credit_Assessment_ID__c limit 1];
                system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() creditAssessmentList: ' + creditAssessmentList);
                if (creditAssessmentList != null && !creditAssessmentList.isEmpty()) {
                    aCreditAssessment = creditAssessmentList.get(0);
                    system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() CAR MRC (' + aCreditAssessment.Total_Deal_Monthly_Recurring_Charges__c + ') and NRC (' + aCreditAssessment.Total_Deal_Non_Recurring_Charges__c + ')');
                }
            }
            system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() aCreditAssessment: ' + aCreditAssessment);
            
            //check if is an amend order
            system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() orderMgmtId_Status__c (' + orderObj.orderMgmtId_Status__c + ') and orderObj.status (' + orderObj.status + ')');
            if ((orderObj.orderMgmtId_Status__c == 'Processing' || orderObj.orderMgmtId_Status__c == 'Submitted' || orderObj.orderMgmtId_Status__c == 'Error') && (orderObj.status == 'Not Submitted')) {
                isAmendOrder = true;
            }
            system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() isAmendOrder: ' + isAmendOrder);
            
            //check if is change order
            system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() Type: ' + orderObj.Type);
            if (orderObj.Type == 'Change') {
                isChangeOrder = true;
            }
            system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() isChangeOrder: ' + isChangeOrder);
            
            //check if is move or disconnect order
            system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() Type: ' + orderObj.Type);
            // Thomas Performance
            if (orderObj.Type == 'Move' || orderObj.Type == 'Disconnect' || newLineItemsCount == 0) {
                isCarRequired = false;
                continueChecking = false;
                if (!orderObj.Credit_Check_Not_Required__c) {
                    if (!updateOrderCarNotRequired(orderObj, true)) {
                        
                        system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() order update failed; CAR set to required');
                        isCarRequired = true;
                    }
                }
                system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() ' + orderObj.Type + ' order');
            }
            
            //Thomas OCOM-1331 Start
            //check if there are contractable offers
            integer numberOfContractableOffers = 0;
            numberOfContractableOffers = [select count() from orderitem where orderid=:orderObj.id and Pricebookentry.Product2.vlocity_cmt__TrackAsAgreement__c=true];
            system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() numberOfContractableOffers('+numberOfContractableOffers+')');
            // Thomas OCOM-1331 End
            
            Boolean tlExists = false;
            if (continueChecking) {
                // check if credit profile has an external credit reference number (aka Credit Ref TL), updated via batch from the TELUS Credit Portal system
                system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() orderObj.AccountId: ' + orderObj.AccountId);
                // Thomas QC-7195
                List<Credit_Profile__c> creditProfile = [SELECT id, account__c, credit_reference_TL__c FROM Credit_Profile__c where account__c=:orderObj.AccountId and credit_reference_TL__c <> '' limit 1];
                system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() creditProfile: ' + creditProfile);
                tlExists = creditProfile.size() > 0;
                system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() tlExists: ' + tlExists);
                
                // CAR not required when MRC, NRC and promotions are $0
                // Thomas OCOM-1331
                system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() MRC('+recurringTotal+') NRC('+oneTimeTotal+') discount('+totalDiscount+ ')');
                // Thomas OCOM-1331
                if (numberOfContractableOffers<=0 && recurringTotal == 0 && oneTimeTotal == 0 && totalDiscount == 0) {
                    isCarRequired = false;
                    continueChecking = false;
                    system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() $0 MRC/NRC/promotions');
                    if (!orderObj.Credit_Check_Not_Required__c) {
                        if (!updateOrderCarNotRequired(orderObj, true)) {
                            
                            system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() order update failed; CAR set to required');
                            isCarRequired = true;
                        }
                    }
                }
            }
            
            // Thomas OCOM-923
            // check if credit assessment already performed and deemed CAR not required
            //if (continueChecking && aCreditAssessment == null && orderObj.Credit_Check_Not_Required__c) {
            //    isCarRequired = false;
            //    continueChecking = false;
            //    system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() Credit_Check_Not_Required__c');
            //}
            //Thomas QC-7195
            if (continueChecking && !isAmendOrder) {
                // retrieve account
                List<Account> accountList = [SELECT Account_Status__c FROM Account where id=:orderObj.AccountId];
                system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() accountList: ' + accountList);
                Account anAccount = null;
                if (accountList != null && !accountList.isEmpty()) {
                    anAccount = accountList.get(0);
                    system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() account status: ' + anAccount.Account_Status__c);
                }
                if (anAccount != null) {
                    system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() account found');
                    // check if account is active
                     if (anAccount.Account_Status__c == 'Active' && tlExists) {
                        system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() account status active and tlExists');
                        // check if CAR is required based on order MRC, NRC and discounts
                        // Thomas OCOM-1331
                        if (recurringTotal > 200 || oneTimeTotal > 100 || totalDiscount > 100) {
                            system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() MRC/NRC/promotions CAR required');
                            isCarRequired = true;
                            continueChecking = false;
                        // Thomas OCOM-1331
                        } else if (numberOfContractableOffers<=0) {
                            system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() MRC/NRC/promotions CAR not required');
                            isCarRequired = false;
                            if (!orderObj.Credit_Check_Not_Required__c) {
                                if (!updateOrderCarNotRequired(orderObj, true)) {
                                    
                                    system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() order update failed; CAR set to required');
                                    isCarRequired = true;
                                }
                            }
                        }
                    }
                }
            }
            
            // check if order total is greater than 10% of an existing CAR deal value
            if (continueChecking) {
                system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() starting 10% check');
                // Thomas - OCOM-1233/QC-7195/OCOM-1312 Start
                if (aCreditAssessment == null && isAmendOrder) {
                    // check for amend orders where original order did not require CAR
                    system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() totalAddAmount ' + totalAddAmount);
                    system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() totalExistingAmount ' + totalExistingAmount);
                    if (totalAddAmount > (totalExistingAmount * 0.1)) {
                        // Thomas OCOM-923
                        moreThan10PercentChange = true;
                        amendCar = true;
                        
                        system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() amend CAR for change order due to 10% change');
                    } else {
                        isCarRequired = false;
                        system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() amend CAR not required due to less than 10% change');
                    }
                } else if (aCreditAssessment != null) {
                // Thomas - OCOM-1233/QC-7195/OCOM-1312 End
                    // check for new or amend orders with existing CAR
                    // Thomas OCOM-1331
                    if ((recurringTotal + oneTimeTotal) > (aCreditAssessment.Total_Deal_Value__c * 1.1)) {
                        system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() MRC ('+recurringTotal+') NRC ('+oneTimeTotal+') total deal value ('+ aCreditAssessment.Total_Deal_Value__c+')');
                        amendCar = true;
                        // Thomas OCOM-923
                        moreThan10PercentChange = true;
                        
                        system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() amend CAR due to 10% change');
                    }
                }
            }
            
            // CAR required before order can be submitted
            if (isCarRequired && (aCreditAssessment == null)) {
                
                
                system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() CAR is required before order can be submitted');
                if (orderObj.Credit_Check_Not_Required__c == true) {
                    if (!updateOrderCarNotRequired(orderObj, false)) {
                         system.debug('NAAS_CreditAssessment_Helper::updateOrderCarNotRequired(False) called');
                    }
                }
            }
            
            String creditStatus = orderObj.Credit_Assessment_Status__c;
            if (isCarRequired && aCreditAssessment != null && creditStatus != null) {
                if (creditStatus.equalsIgnoreCase('DRAFT')) {
                    
                    system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() CAR cannot be in Draft status when order is submitted');
                
                }
            }
            
            if (noCreditProfile) {               
                system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() credit profile missing');
            }
        } else {
            system.debug('NAAS_CreditAssessment_Helper::creditCheckValidation() order '+orderId+' not found');
        }
            
        system.debug('Finally -- isCarRequired ===>' + isCarRequired);
        output.put('isCarRequired' , isCarRequired);
       
        
    }
    
    public static boolean updateOrderCarNotRequired(Order orderObj, boolean newValue) {
        try {
            system.debug('NAAS_CreditAssessment_Helper::updateOrderCarNotRequired newValue('+newValue+')');
            orderObj.credit_Check_not_required__c = newValue;
            update orderObj;
            return true;
        } catch(Exception e) {
            system.debug('NAAS_CreditAssessment_Helper::updateOrderCarNotRequired() exception' + e.getMessage());
            return false;
        }
    }



}