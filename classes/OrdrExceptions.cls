//[Roderick] 2016-11-09
//Updated by Aditya Jamwal [14-06-2017] added "BillingAccountCreationServiceException" method.
public class OrdrExceptions 
{
    public abstract class ServiceException extends OrdrServicesException{
        
        public virtual override String getErrorCode() {
            
            String errorMessageFromFault = super.getMessage();
            String errorCode = null;
            if(String.isNotBlank(errorMessageFromFault)){
				if (errorMessageFromFault.containsIgnoreCase('timed out')) {
                    errorCode = OrdrErrorCodes.WEBSERVICE_CALL_TIMEOUT_ERROR;
                } else if (errorMessageFromFault.containsIgnoreCase('Server.91009')) {
                    errorCode = OrdrErrorCodes.DEFAULT_CONNECTIVITY_ERROR;
                } else if (errorMessageFromFault.containsIgnoreCase('No response received')) {
                    errorCode = OrdrErrorCodes.DEFAULT_CONNECTIVITY_ERROR;
                }else{
                     errorCode = OrdrErrorCodes.DEFAULT_UNEXPECTED_ERROR;
                }
            }

            return errorCode;
        }
        
    }
    
    public class ProductEligibilityException extends ServiceException{
        public override String getMessage() {
            String errorCode = this.getErrorCode();
            String msg = '';
            if (String.isNotBlank(errorCode)) {
                msg = OrdrErrorCodes.ERROR_MESSAGES_MAP.get(errorCode);
                msg = msg.replace('{0}', 'Product Eligibility');
            } 
            return msg;
        }   
    }

    public class ProductFeasibilityException extends ServiceException{
        public override String getMessage() {
            String errorCode = this.getErrorCode();
            String msg = '';
            if (String.isNotBlank(errorCode)) {
                msg = OrdrErrorCodes.ERROR_MESSAGES_MAP.get(errorCode);
                msg = msg.replace('{0}', 'Product Feasibility');
            } 
            return msg;
        }   
    }    
    public class TechnicalAvailabilityServiceException extends ServiceException{
        public override String getMessage() {
            String errorCode = this.getErrorCode();
            String msg = '';
            if (String.isNotBlank(errorCode)) {
                msg = OrdrErrorCodes.ERROR_MESSAGES_MAP.get(errorCode);
                msg = msg.replace('{0}', 'Technical Availability');
            } 
            return msg;
        }        
    }        

    public class RateGroupInfoServiceException extends ServiceException{
        public override String getMessage() {
            String errorCode = this.getErrorCode();
            String msg = '';
            if (String.isNotBlank(errorCode)) {
                msg = OrdrErrorCodes.ERROR_MESSAGES_MAP.get(errorCode);
                msg = msg.replace('{0}', 'Rate Group and Forbearance Info');
            } 
            return msg;
        }        
    }        

    public class ServicePathServiceException extends ServiceException{
        public override String getMessage() {
            String errorCode = this.getErrorCode();
            String msg = '';
            if (String.isNotBlank(errorCode)) {
                msg = OrdrErrorCodes.ERROR_MESSAGES_MAP.get(errorCode);
                msg = msg.replace('{0}', 'Clearance Check');
            } 
            return msg;
        }        
    }
    // Added by Aditya Jamwal [14-06-2017] to capture Billing Account Creation Exceptions  
     public class BillingAccountCreationServiceException extends ServiceException{
        public override String getMessage() {
            String errorCode = this.getErrorCode();
            String msg = '';
            if (String.isNotBlank(errorCode)) {
                msg = OrdrErrorCodes.ERROR_MESSAGES_MAP_BILLING_ACCOUNT_CREATION.get(errorCode);
                msg = msg.replace('{0}', 'Billing Account Creation');
            } 
            return msg;
        }        
    }        
    
    public class QueryResourceException extends ServiceException{
        public override String getMessage() {
            String errorCode = this.getErrorCode();
            String msg = '';
            if (String.isNotBlank(errorCode)) {
                msg = OrdrErrorCodes.ERROR_MESSAGES_MAP.get(errorCode);
                msg = msg.replace('{0}', 'Clearance Check');
            }
            return msg;
        }
    }
   
    public class InvalidParameterException extends Exception{}
    public class ResultNotFoundException extends Exception{}
    

}