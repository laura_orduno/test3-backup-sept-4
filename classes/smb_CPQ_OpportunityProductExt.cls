/*
###########################################################################
# File..................: smb_CPQ_OpportunityProductExt
# Version...............: 1
# Created by............: Bharat Vikram chand
# Created Date..........: 14-Sept-2013
# Last Modified by......: 
# Last Modified Date....: 14-Nov-2013
# Description...........: This Extension to display the childrens of Opportunity Line Items if any
# Change Log:               
############################################################################
 */

public with sharing class smb_CPQ_OpportunityProductExt {
private List<OpportunityLineItem> oppProdz;    
    private OpportunityLineItem oli;     
    public smb_CPQ_OpportunityProductExt(ApexPages.StandardController controller) {        
        
        this.oli= (OpportunityLineItem)controller.getRecord(); 
        
/*****        
        if(!Test.isRunningTest())
        {    
	    	Controller.addFields(new list<string>{'Opportunityid'});       
            
        	this.oli= (OpportunityLineItem)controller.getRecord(); 
        }
*/
        
        system.debug('--OLI Id is '+oli.Id);   
        }    
        public List<OpportunityLineItem> getOppz()    
        {                           
/*            
            	if(!Test.isRunningTest())
                {    
                	oppProdz = [Select o.Parent_OLI_Id__c, o.Item_ID__c,Product_Name__c, o.Id From OpportunityLineItem o where Opportunityid=:this.oli.opportunityid and o.Parent_OLI_Id__c =: oli.id LIMIT 10];        
                }
                else
                {
                	return null;                    
                }
*/            
              	oppProdz = [Select o.Parent_OLI_Id__c, o.Item_ID__c,Product_Name__c, o.Id From OpportunityLineItem o where Opportunityid=:this.oli.opportunityid and o.Parent_OLI_Id__c =: oli.id LIMIT 10];        
            	
            	if(oppProdz.size() > 0){
                   return oppProdz;                 
                }
                return null;
                    
        }
}