public class ContactPicklistController {
	public PicklistValuesOptionModel titleCategory {get; private set;}
	public PicklistValuesOptionModel department {get; private set;}
	public PicklistValuesOptionModel contactRole {get; private set;}
	public PicklistValuesOptionModel language {get; private set;}
	public PicklistValuesOptionModel contactMethod {get; private set;}
	
	public ContactPicklistController(QuoteContactFormExtension controller) { init(); }
	
	public ContactPicklistController() {
        init();
    }
    public void init() {
    	titleCategory = new PicklistValuesOptionModel(Contact.Title_Category__c);
        department = new PicklistValuesOptionModel(Contact.Department_Category__c);
        contactRole = new PicklistValuesOptionModel(Contact.Quote_Role__c);
        language = new PicklistValuesOptionModel(Contact.Language_Preference__c);
        contactMethod = new PicklistValuesOptionModel(Contact.Preferred_Contact_Method_Quote__c);
    }

}