@IsTest
public class ValidateTelUsSignor_Test {
    static testmethod void invokeMethodPositiveTest(){
        Test.startTest();
        ValidateTelUsSignor validateTelSignorObj = new ValidateTelUsSignor();
        String methodName = 'validateSignor';
        Map<String,Object> inputMap = new  Map<String,Object>();
       Map<String,Object> outMap   = new Map<String,Object>();
       Map<String,Object> options  = new Map<String,Object>();
        //inputMap.put('contextId', TestDataHelper.testContractObj.Id);
        inputMap.put('contextId', TestDataHelper.testContractObj);
       //Boolean result  = validateTelSignorObj.invokeMethod(methodName,inputMap, outMap, options);
        system.assertEquals(validateTelSignorObj.invokeMethod(methodName,inputMap, outMap, options) , true);
		Test.stopTest();
    }

    static testmethod void invokeMethodNegativeTest(){
          Test.startTest(); 
        ValidateTelUsSignor validateTelSignorObj = new ValidateTelUsSignor();

        String methodName = 'validateSignor';
        Map<String,Object> inputMap;// = new  Map<String,Object>();
       Map<String,Object> outMap  ; //= new Map<String,Object>();
       Map<String,Object> options ; //= new Map<String,Object>();
       /* Map<String,Object> inputMap = new  Map<String,Object> inputMap();
        Map<String,Object> outMap   = new Map<String,Object> outMap();
        Map<String,Object> options  = new Map<String,Object> options();*/
        
        system.assertEquals(validateTelSignorObj.invokeMethod(methodName,inputMap, outMap, options), false);
        Test.stopTest();
    }

}