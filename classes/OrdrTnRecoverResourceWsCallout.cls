/**
 * @author Santosh Rath
 * 
 **/
public class OrdrTnRecoverResourceWsCallout {
 public static TpFulfillmentResourceOrderV3.ResourceOrder recoverResource(TpCommonMtosiV3.Header_T header,TpFulfillmentResourceOrderV3.ResourceOrder resourceOrder,Boolean isTollFree) {
        TpFulfillmentResourceOrderV3.ResourceOrder respResourceOrder;
        try {
				System.debug('The name of the TNR WebService called is recoverResource');
                 OrdrTnRecvrResOperation.RecoverResourceSOAPBinding binding = new OrdrTnRecvrResOperation.RecoverResourceSOAPBinding();
                binding = prepareCallout(binding,isTollFree); 
                if(!OrdrUtilities.useAuditFlag){
                	respResourceOrder = binding.recoverResource(resourceOrder);
                }
            
        } catch (CalloutException ce) {
            System.debug(ce.getStackTraceString());
		    System.debug('CalloutException occurred while invoking binding.recoverResource' +ce);
            System.debug(ce.getmessage());
            String msg=ce.getmessage();
            if(String.isNotBlank(msg) && (msg.indexOf(OrdrConstants.SDF_INTERNAL_SERVER_ERR)!=-1)){
                msg=msg+OrdrConstants.SDF_SUPPORT_MSG;
            }
			OrdrUtilities.auditLogs('OrdrTnRecoverResourceWsCallout.recoverResource','TOCP','recoverResource',null,null,true,false,msg,ce.getStackTraceString());
            OrdrServicesException ose=new OrdrServicesException(ce.getmessage(),ce);
            throw ose;
        } catch (Exception e) {
            System.debug(e.getStackTraceString());
		    System.debug('Exception occurred while invoking binding.recoverResource' +e);
            System.debug(e.getmessage());
            String msg=e.getmessage();
            if(String.isNotBlank(msg) && (msg.indexOf(OrdrConstants.SDF_INTERNAL_SERVER_ERR)!=-1)){
                msg=msg+OrdrConstants.SDF_SUPPORT_MSG;
            }
			OrdrUtilities.auditLogs('OrdrTnRecoverResourceWsCallout.recoverResource','TOCP','recoverResource',null,null,false,true,msg,e.getStackTraceString());
            OrdrServicesException ose=new OrdrServicesException(e.getmessage(),e);
            throw ose;
        }
        return respResourceOrder;
    }
    private static OrdrTnRecvrResOperation.RecoverResourceSOAPBinding prepareCallout(OrdrTnRecvrResOperation.RecoverResourceSOAPBinding binding,Boolean isTollFree) {
        Ordering_WS__c recoverResourceWirelineEndpoint = Ordering_WS__c.getValues('RecoverResourceWirelineEndpoint');  //ReserveResourceTollfreeEndpoint
        if(isTollFree){
			recoverResourceWirelineEndpoint = Ordering_WS__c.getValues('RecoverResourceTollfreeEndpoint');
		}
        binding.endpoint_x = recoverResourceWirelineEndpoint.value__c;
        System.debug('RecoverResourceWirelineEndpoint: '+ recoverResourceWirelineEndpoint.value__c);
      
       // servicePort.clientCertName_x = OrdrConstants.SDF_CERT_NAME;          
        // Set SFDC Webservice call timeout
        binding.timeout_x = OrdrConstants.WS_TIMEOUT_MS;
         //binding.timeout_x = 120000;
		if (binding.endpoint_x.startsWith(OrdrConstants.ENDPOINT_XML_GATEWAY_SYMBOL)) {
                Ordering_WS__c wsUserName = Ordering_WS__c.getValues(OrdrConstants.CREDENTIAL_USERNAME);
                Ordering_WS__c wsPassword =  Ordering_WS__c.getValues(OrdrConstants.CREDENTIAL_PASSWORD);  
                if (String.isNotBlank(wsUserName.value__c) && String.isNotBlank(wsPassword.value__c)) {
						String credentials = 'APP_CPQ' + ':' + wsPAssword.value__c;
						String encodedUserNameAndPassword = EncodingUtil.base64Encode(Blob.valueOf(credentials));
						binding.inputHttpHeaders_x = new Map<String, String>();
						binding.inputHttpHeaders_x.put(OrdrConstants.AUTHORIZATION, OrdrConstants.BASIC + encodedUserNameAndPassword);
					}   
                }
          else {
                binding.clientCertName_x = OrdrConstants.SDF_CERT_NAME;
            }
        return binding;
    } 

		/**
	 * Used for Real TN and Toll Free number reservation using the TOCP RecoverResource Business Contract
 * For SL, ML and Dark High Speed, the TOCP Contract is mapped to the following ASF API
 * RecoverResource
 * - Release telephone number(s) from FMS
	 * 
	 * @param  
	 * @return 
	 *  
	 */		
   public static void recoverResourceByReleasingTnList(OrdrTnServiceAddress serviceAddress, String userId, List<OrdrTnLineNumber> lineNumberList, OrdrTnReservationType reservationType) {
    	System.debug('-- RecoverResourceImpl.recoverResourceByReleasingTnList - processing starts at ' + System.now());
		TpCommonMtosiV3.Header_T header=new TpCommonMtosiV3.Header_T();
		
		TpFulfillmentResourceOrderV3.ResourceOrder resourceOrder = null;

		if (reservationType != null) {
			if (String.valueOf(reservationType).equals(String.valueOf(OrdrTnReservationType.WLN))) {
    			//Preparing the WS call to TOCP to reach ASF for releasing Wireline TN(s) in FMS
				resourceOrder = generateTocpResourceOrderForWirelineDarkDslAndRealTnSoapRequest(serviceAddress, lineNumberList, userId, null, false);
			}
			else if (String.valueOf(reservationType).equals(String.valueOf(OrdrTnReservationType.TOLLFREE))) {
    			//Releasing Toll Free Number(s) is not a functionality supported/required by SMS - therefore, no outbound WS call needed
			}

			if (resourceOrder != null) {
				// Invoke the TOCP - RecoverResource web service
				try {
		        	System.debug('Invoking ASF InventoryService.releaseTelephoneNumberList through TOCP RecoverResource');
					recoverResource(header, resourceOrder,false);
				} 
				catch (Exception e) {
					System.debug(e.getStackTraceString());
                    throw e;
				}
				
    			// Process the TOCP response
				// Nothing to look at/process in the response for releasing WLN TNs
			}
		}
    }
  
	/**
	 * Used for Real TN and Toll Free number reservation using the TOCP RecoverResource Business Contract
 * For SL, ML and Dark High Speed, the TOCP Contract is mapped to the following ASF API
 * RecoverResource
 * - Unreserve real wireline telephone number(s) in FMS
 * 
 * For Toll Free (TF), the TOCP Contract is mapped to the following SMS API
 * RecoverResource 
 * - Unreserve toll free number in SMS
	 * 
	 * @param  
	 * @return 
	 *  
	 */		
    
    public static List<OrdrTnLineNumber> recoverResourceByUnreservingTnList(OrdrTnServiceAddress serviceAddress, List<OrdrTnLineNumber> tnList, String userId, String referenceNumber, OrdrTnReservationType reservationType) {
    	if (reservationType != null) {
    		if (String.valueOf(reservationType).equals(String.valueOf(OrdrTnReservationType.WLN))) {
    			//Preparing the WS call to TOCP to reach ASF for un-reserving Wireline TN(s) - both real Tns and Dark DSL
    			// TOCP will accept and process more than 1 real TN as an input for unreserving TN(s) in ASF/FMS 
    			tnList = recoverResourceByUnreservingTnListForWirelineDarkDslAndRealTn(serviceAddress, tnList, userId, referenceNumber, reservationType);
    		}
    		else if (String.valueOf(reservationType).equals(String.valueOf(OrdrTnReservationType.TOLLFREE))) {
    			//Preparing the WS call to TOCP to reach SMS for un-reserving Toll Free Number(s)
    			// TOCP will process no more than 1 TollFree Number at a time, as an input for unreserving TF # in SMS/800 
    			tnList = recoverResourceByUnreservingTnListForTollFree(serviceAddress, tnList, userId, referenceNumber, reservationType);
    		}
    	}
		if (tnList == null) {
			tnList = new List<OrdrTnLineNumber>();
		}
    	return tnList;
    }

    
    public static List<OrdrTnLineNumber> recoverResourceByUnreservingTnListForWirelineDarkDslAndRealTn(OrdrTnServiceAddress serviceAddress, List<OrdrTnLineNumber> tnList, String userId, String referenceNumber, OrdrTnReservationType reservationType) {
    	
    	String asfBindingError = null;
    	String asfErrorMessage = null;

		System.debug('-- RecoverResourceImpl.recoverResourceByUnreservingTnList - processing starts at '+System.now() );

    	TpCommonMtosiV3.Header_T header=new TpCommonMtosiV3.Header_T();
    	
		TpFulfillmentResourceOrderV3.ResourceOrder resourceOrder = null;

    	//Preparing the WS call to TOCP to reach ASF for un-reserving Wireline TN(s) - both real Tns and Dark DSL
    	resourceOrder = generateTocpResourceOrderForWirelineDarkDslAndRealTnSoapRequest(serviceAddress, tnList, userId, referenceNumber, true);

    	if (resourceOrder != null) {
    		// Invoke the TOCP - RecoverResource web service
    			try {
    				System.debug( 'Invoking ASF InventoryService.unreserveTelephoneNumber through TOCP RecoverResource');
    				resourceOrder= recoverResource(header, resourceOrder,false);
    			}
    			catch (Exception e) {
				  System.debug('Exception occurred during unreserve callout '+e);
                    asfErrorMessage=e.getMessage();
                    throw e;
    			}

    		// Process the TOCP response
    		if (asfErrorMessage == null) {
    			tnList = processTocpResponseForUnreservingTnList(tnList, reservationType, resourceOrder);
    		}
    		else {
    			if (tnList != null) {
    				for (integer k=0; k<tnList.size(); k++) {
    					// if the TN unreservation failed - error message returned or exception/fault message thrown - tag this TN as still Reserved
    					tnList.get(k).setStatus(OrdrTnLineNumberStatus.RESERVED_UNASSIGNED);
    					System.debug('Error caught in RecoverResourceImpl.recoverResourceByUnreservingTnList for '+tnList.get(k).getTenDigitsTn()+' : '+asfErrorMessage);
    				}
    			}
    		}
    	}

    	if (tnList == null) {
    		tnList = new List<OrdrTnLineNumber>();
    	}
    	return tnList;
    }

   
    public static List<OrdrTnLineNumber> recoverResourceByUnreservingTnListForTollFree(OrdrTnServiceAddress serviceAddress, List<OrdrTnLineNumber> tnList, String userId, String referenceNumber, OrdrTnReservationType reservationType) {
    	String asfBindingError = null;
    	String asfErrorMessage = null;

    	System.debug('-- RecoverResourceImpl.recoverResourceByUnreservingTnList - processing starts at '+System.now());

		TpCommonMtosiV3.Header_T header=new TpCommonMtosiV3.Header_T();
    	
		TpFulfillmentResourceOrderV3.ResourceOrder resourceOrder = null;

    	
    	if ((tnList != null) && (tnList.size()>0)) {
    		for (integer i=0; i<tnList.size(); i++) {
    			//Preparing the WS call to TOCP to reach SMS for un-reserving Toll Free Number(s)
    			resourceOrder = generateTocpResourceOrderForTollFreeSoapRequest(serviceAddress, tnList.get(i), userId, referenceNumber);
    			if (resourceOrder != null) {
    				   asfErrorMessage = null;
    					// Invoke the TOCP - RecoverResource web service
    					try {
    						System.debug( 'Invoking SMS unreserveTelephoneNumber through TOCP RecoverResource');
    						recoverResource(header, resourceOrder,true);
    					} 
    					catch (Exception e) {
    						asfErrorMessage=e.getMessage();
                            System.debug(e.getStackTraceString());
                            throw e;
    					}
                    
    				// Process the TOCP response
    				if (asfErrorMessage == null) {
    					tnList = processTocpResponseForUnreservingTnList(tnList, reservationType, resourceOrder);
    				}
    				else {
    					// if the TN unreservation failed - error message returned or exception/fault message thrown - tag this TN as still Reserved
    					tnList.get(i).setStatus(OrdrTnLineNumberStatus.RESERVED_UNASSIGNED);
    					System.debug('Error caught in RecoverResourceImpl.recoverResourceByUnreservingTnList for '+tnList.get(i).getTenDigitsTn()+' : '+asfErrorMessage);
    				}
    			}
    		}
    	}
    	if (tnList == null) {
    		tnList = new List<OrdrTnLineNumber>();
    	}
    	return tnList;
    }

 	  
   
  @TestVisible  private static List<OrdrTnLineNumber> processTocpResponseForUnreservingTnList(List<OrdrTnLineNumber> tnList, OrdrTnReservationType reservationType, TpFulfillmentResourceOrderV3.ResourceOrder resourceOrder) {
    	// Process the TOCP response
    	if (resourceOrder != null) {
    		List<TpFulfillmentResourceOrderV3.ResourceOrderItem> listRoi = resourceOrder.ResourceOrderItem;
			//List<Object> listRoi = null;
    		if ((listRoi != null) && (listRoi.size() > 0)) {
    			for (Integer i=0; i<listRoi.size(); i++) {
    				if (listRoi.get(i) != null) {
    					TpCommonBusinessInteractionV3.BusinessInteractionEntity res = listRoi.get(i).Resource;
    					if ((res != null) && (res.CharacteristicValue!= null)) {
    						Map <String, String> hm = convertTocpV2CharacteristicValuesToMap (res.CharacteristicValue);
							//Map <String, String> hm = null;
    						String npa = null;
    						String nxx = null;
    						String line = null;
    						String telephoneNumberStatus = null;
    						String tollFreeNumber = null;
    						String lineNumberTenDigits = null;
    						if (hm.containsKey('npa')) {
    							npa = hm.get('npa');
    						}	
    						if (hm.containsKey('nxx')) {
    							nxx = hm.get('nxx');
    						}	
    						if (hm.containsKey('line')) {
    							line = hm.get('line');
    						}
    						if (hm.containsKey('telephoneNumberStatus')) {
    							telephoneNumberStatus = hm.get('telephoneNumberStatus');
    						}
    						if (hm.containsKey('tollFreeNumber')) {
    							tollFreeNumber = hm.get('tollFreeNumber');
    						}

    						if (String.isNotBlank(tollFreeNumber)) {
    							lineNumberTenDigits = tollFreeNumber;
    							telephoneNumberStatus = 'unreserved';
    						}
    						else if (String.isNotBlank(npa) && String.isNotBlank(nxx)  && String.isNotBlank(line)) {
    							lineNumberTenDigits = npa+nxx+line;
    						}
    						if (lineNumberTenDigits != null) {
    							Integer j = 0;
    							if (tnList != null) {
										j = tnList.size();
									}
    							for (Integer k=0; k<j; k++) {
    								String tnTenDigits = tnList.get(k).getNpa()+tnList.get(k).getNxx()+tnList.get(k).getLineNumber();
    								if ((tnTenDigits != null) && (tnTenDigits.equalsIgnoreCase(lineNumberTenDigits))) {
    									if (telephoneNumberStatus != null) {
    										if (telephoneNumberStatus.equalsIgnoreCase('unreserved')) {
    											// if the TN was successfully unreserved - tag this TN as available again - i.e. back in the Available TN Pool in FMS
    											tnList.get(k).setStatus(OrdrTnLineNumberStatus.AVAILABLE);
    										}
    										else
    										{
    											// if the TN unreservation failed - error message returned or exception/fault message thrown - tag this TN as still Reserved
    											tnList.get(k).setStatus(OrdrTnLineNumberStatus.RESERVED_UNASSIGNED);
    											System.debug('Error caught in RecoverResourceImpl.recoverResourceByUnreservingTnList for '+tnList.get(k).getTenDigitsTn());
    										}
    									}
    									break;
    								}
    							}
    						}
    					}
    				}
    			}
    		}
    	}
    	return tnList;
    }
    
	private static TpFulfillmentResourceOrderV3.ResourceOrder generateTocpResourceOrderForWirelineDarkDslAndRealTnSoapRequest(OrdrTnServiceAddress serviceAddress, List<OrdrTnLineNumber> tnList, String userId, String referenceNumber, boolean unreserveInd) {
		System.debug(' Begin inside TpFulfillmentResourceOrderV3.ResourceOrder generateTocpResourceOrderForWirelineDarkDslAndRealTnSoapRequest');
		TpFulfillmentResourceOrderV3.ResourceOrder resourceOrder = new TpFulfillmentResourceOrderV3.ResourceOrder();
		
		if (unreserveInd) {
			//Order action reference number only required for Unreserving TN(s) - not required for releasing TN(s)
			resourceOrder.Id=checkNullInput(referenceNumber);
		}
		resourceOrder.Specification=generateTocpV2EntitySpecificationForSoapRequest('Wireline', 'Telephone Number', 'Logical Resource');
		List<TpCommonBaseV3.CharacteristicValue> characteristicValueList=new List<TpCommonBaseV3.CharacteristicValue>();
		characteristicValueList.add(generateTocpV2CharacteristicValueForSoapRequest('action', (unreserveInd ? 'unreserve' : 'release') , true));
		characteristicValueList.add(generateTocpV2CharacteristicValueForSoapRequest('userId', 'SFDCTN', false));
		
		resourceOrder.CharacteristicValue=characteristicValueList;
        List<TpFulfillmentResourceOrderV3.ResourceOrderItem> resourceOrderItemList=new List<TpFulfillmentResourceOrderV3.ResourceOrderItem>();
		for (Integer i=0; i<tnList.size(); i++) {
			TpFulfillmentResourceOrderV3.ResourceOrderItem resourceOrderItem =  new TpFulfillmentResourceOrderV3.ResourceOrderItem();
			TpCommonBusinessInteractionV3.BusinessInteractionEntity businessInteractionEntity = new TpCommonBusinessInteractionV3.BusinessInteractionEntity();
			List<TpCommonBaseV3.CharacteristicValue> busIntCharacteristicValueList=new List<TpCommonBaseV3.CharacteristicValue>();
			busIntCharacteristicValueList.add(generateTocpV2CharacteristicValueForSoapRequest('npa', checkNullInput(tnList.get(i).getNpa()), true));
			busIntCharacteristicValueList.add(generateTocpV2CharacteristicValueForSoapRequest('nxx', checkNullInput(tnList.get(i).getNxx()), false));
			busIntCharacteristicValueList.add(generateTocpV2CharacteristicValueForSoapRequest('line', checkNullInput(tnList.get(i).getLineNumber()), false));
			busIntCharacteristicValueList.add(generateTocpV2CharacteristicValueForSoapRequest('releaseDate', checkNullInput(tnList.get(i).getReleaseDate()), false));
			
			businessInteractionEntity.CharacteristicValue=busIntCharacteristicValueList;
			resourceOrderItem.Resource=businessInteractionEntity;
			
			if (i == 0) {
				String switchNumber = tnList.get(i).getSwitchNumber();
				String coid = tnList.get(i).getServingCoid();
				/*if (String.isBlank(coid)) {
					coid = tnList.get(i).getCOID();
				}*/
				if (String.isBlank(coid)) {
					coid = serviceAddress.getCOID();
				}
				if (String.isBlank(switchNumber)) {
					switchNumber = OrdrConstants.SWITCH_CODE;
				}
				
				characteristicValueList.add(generateTocpV2CharacteristicValueForSoapRequest('coid', coid, false));
				characteristicValueList.add(generateTocpV2CharacteristicValueForSoapRequest('switchNumber', switchNumber, false));
				resourceOrder.CharacteristicValue=characteristicValueList;
				
				resourceOrderItemList.add(resourceOrderItem);
				
				resourceOrder.ResourceOrderItem=resourceOrderItemList;
			}
			else {                
			    resourceOrderItemList.add(resourceOrderItem);
				
			}
		}
        resourceOrder.ResourceOrderItem=resourceOrderItemList;
		System.debug(' end inside TpFulfillmentResourceOrderV3.ResourceOrder generateTocpResourceOrderForWirelineDarkDslAndRealTnSoapRequest');
		return resourceOrder;
	}

	

@TestVisible private static TpFulfillmentResourceOrderV3.ResourceOrder generateTocpResourceOrderForTollFreeSoapRequest(OrdrTnServiceAddress serviceAddress, OrdrTnLineNumber tn, String userId, String referenceNumber) {
		TpFulfillmentResourceOrderV3.ResourceOrder resourceOrder = new TpFulfillmentResourceOrderV3.ResourceOrder();
		
		//Order action reference number - digit alphanumeric correlation id used by SMS - must begin with a capital 'O'
		String id = String.valueOf(System.currentTimeMillis()).substring(4,13);
		// Total length of ID passed to SMS should be exactly 10 : 9 digits number prefixed by 'O'
		resourceOrder.Id=('O'+id);
		resourceOrder.Specification=generateTocpV2EntitySpecificationForSoapRequest('', 'Toll Free Telephone Number', 'Logical Resource');
		if (tn != null) {
			TpFulfillmentResourceOrderV3.ResourceOrderItem resourceOrderItem =  new TpFulfillmentResourceOrderV3.ResourceOrderItem();
			TpCommonBusinessInteractionV3.BusinessInteractionEntity businessInteractionEntity = new TpCommonBusinessInteractionV3.BusinessInteractionEntity();
			List<TpCommonBaseV3.CharacteristicValue> bsnssIntCharValList=new List<TpCommonBaseV3.CharacteristicValue>();
			bsnssIntCharValList.add(generateTocpV2CharacteristicValueForSoapRequest('respOrg', 'EUS01', true));
			bsnssIntCharValList.add(generateTocpV2CharacteristicValueForSoapRequest('tollFreeNumber', checkNullInput(tn.getTenDigitsTn()), false));
			businessInteractionEntity.CharacteristicValue=bsnssIntCharValList;
			
			resourceOrderItem.Resource=businessInteractionEntity;
			List<TpFulfillmentResourceOrderV3.ResourceOrderItem> resourceOrderItemList=new List<TpFulfillmentResourceOrderV3.ResourceOrderItem>();
			resourceOrderItemList.add(resourceOrderItem);
			resourceOrder.ResourceOrderItem=resourceOrderItemList;
			
		}
		return resourceOrder;
	}



	private static String checkNullInput(String sInput) {
		String sOutput = '';
		sOutput = (String.isBlank(sInput) ? sOutput : sInput);  
		return sOutput;
	}

	
	
@TestVisible private static List<TpCommonBaseV3.EntityWithSpecification> generateTocpSourceCriteriaForWirelineDarkDslAndRealTnSoapRequest(OrdrTnServiceAddress serviceAddress, String userId, String npa, String alternateNxx) {
    	List<TpCommonBaseV3.EntityWithSpecification> lews = new List<TpCommonBaseV3.EntityWithSpecification>();
    	TpCommonBaseV3.EntityWithSpecification sourceCriteria = new TpCommonBaseV3.EntityWithSpecification();
    	sourceCriteria.Specification=generateTocpV2EntitySpecificationForSoapRequest('Wireline', 'Telephone Number', 'Logical Resource');

		List<TpCommonBaseV3.CharacteristicValue> charValList=new List<TpCommonBaseV3.CharacteristicValue>();
		charValList.add(generateTocpV2CharacteristicValueForSoapRequest('userId', 'SFDCTN', true));
		charValList.add(generateTocpV2CharacteristicValueForSoapRequest('npa', checkNullInput(npa), false));
		charValList.add(generateTocpV2CharacteristicValueForSoapRequest('nxx', checkNullInput(alternateNxx), false));
		sourceCriteria.CharacteristicValue=charValList;
		lews.add(sourceCriteria);
    	
    	return lews;
    }
  
  @TestVisible private static List<TpCommonBaseV3.EntityWithSpecification> generateTocpSourceCriteriaForTollFreeSoapRequest(OrdrTnServiceAddress serviceAddress, String userId, String tn) {
		//Order action reference number - digit alphanumeric correlation id used by SMS - must begin with a capital 'O'
		String id = String.valueOf(System.currentTimeMillis()).substring(4,13);
		// Total length of ID passed to SMS should be exactly 10 : 9 digits number prefixed by 'O'
    	List<TpCommonBaseV3.EntityWithSpecification> lews = new List<TpCommonBaseV3.EntityWithSpecification>();
    	TpCommonBaseV3.EntityWithSpecification sourceCriteria = new TpCommonBaseV3.EntityWithSpecification();
    	sourceCriteria.Specification=generateTocpV2EntitySpecificationForSoapRequest('', 'Toll Free Telephone Number', 'Logical Resource');
    	sourceCriteria.Id=('O'+id);

		List<TpCommonBaseV3.CharacteristicValue> charValList=new List<TpCommonBaseV3.CharacteristicValue>();
		charValList.add(generateTocpV2CharacteristicValueForSoapRequest('respOrg', 'EUS01', true));
		charValList.add(generateTocpV2CharacteristicValueForSoapRequest('tollFreeNumber', checkNullInput(tn), false));
		sourceCriteria.CharacteristicValue=charValList;
		lews.add(sourceCriteria);
    	
    	return lews;
    }
 
    private static List<TpCommonBaseV3.EntityWithSpecification> generateTocpTargetCriteriaForTollFreeSoapRequest(OrdrTnServiceAddress serviceAddress, String userId, String tn) {
    	List<TpCommonBaseV3.EntityWithSpecification> lews = new List<TpCommonBaseV3.EntityWithSpecification>();
    	TpCommonBaseV3.EntityWithSpecification targetCriteria = new TpCommonBaseV3.EntityWithSpecification();
		List<TpCommonBaseV3.CharacteristicValue> charValList=new List<TpCommonBaseV3.CharacteristicValue>();
		charValList.add(generateTocpV2CharacteristicValueForSoapRequest('searchOption', 'telephoneNumber', true));
		targetCriteria.CharacteristicValue=charValList;
		
		lews.add(targetCriteria);
    	return lews;
    }
  
	

	


	public static Map <String, String> convertTocpV2CharacteristicValuesToMap (List<TpCommonBaseV3.CharacteristicValue> listCharVal) {
		Map <String, String> hm = new Map <String, String>();
		for(Integer z=0;z<listCharVal.size();z++) {
			TpCommonBaseV3.CharacteristicValue cv = listCharVal.get(z);
			if ((cv != null) && (cv.Characteristic != null)) {
				TpCommonBaseV3.Characteristic c = cv.Characteristic;
				String cName  = c.Name;
				if (String.isNotBlank(cName)) {
					String cValue = null;
					if ((cv.Value != null) && (cv.Value.size()>0)) {
						cValue = cv.Value.get(0);
						if (!hm.containsKey(cName)) {
							hm.put(cName, cValue);
						}
					}
				}
			}
		}
		return hm;
	}


	public static TpCommonBaseV3.EntitySpecification generateTocpV2EntitySpecificationForSoapRequest(
			String specificationName, String specificationType, String specificationCategory) {
    	// Setting service specifications
		TpCommonBaseV3.EntitySpecification entitySpec = new TpCommonBaseV3.EntitySpecification();
   		entitySpec.Name=checkNullInput(specificationName);
    	entitySpec.Type_x=checkNullInput(specificationType);
    	entitySpec.Category=checkNullInput(specificationCategory);
		return entitySpec;
	}

		public static TpCommonBaseV3.CharacteristicValue generateTocpV2CharacteristicValueForSoapRequest(
			String nameOfCharacteristic, String valueOfCharacteristic, boolean firstElement) {

		TpCommonBaseV3.CharacteristicValue charValueOne = null;
		if (String.isNotBlank(nameOfCharacteristic)) {
			charValueOne = new TpCommonBaseV3.CharacteristicValue();
			TpCommonBaseV3.Characteristic charOne = new TpCommonBaseV3.Characteristic();			 
			charOne.Name=nameOfCharacteristic;
			charValueOne.Characteristic=charOne;
			List<String> strValList=new List<String>();
			strValList.add(checkNullInput(valueOfCharacteristic));
			charValueOne.Value=strValList;
			
		}
		return charValueOne;
	}	
 }