/* Developed By: Hongda Cheng
** Created Date: 04-Aug-2016
*/
@isTest
private Class SRS_ServiceRequestValidationHelperTest 
{
    static testMethod void resetEscalationStatusTest()
    {
        Test.startTest();
        Account acc = new Account(Name = 'test');
        insert acc;
        
        SRS_PODS_Product__c Prod = new SRS_PODS_Product__c(Single_Address__c = true, Name = 'ISDN PRI');
        insert Prod;
        
        contact c = new contact(firstname = 'Hongda', lastname = 'Cheng',  email='hongda.cheng@telus.com');
        insert c;
        
        Opportunity opp = new Opportunity(Name = 'OppTest1', StageName = 'Originated', CloseDate = System.today(), 
                                          Primary_Order_Contact__c = c.id);
        insert opp;
        
        Id SrRecordtypeID = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Prequal').getRecordTypeId();
       
        Service_Request__c sereq1 =  new Service_Request__c(Service_Request_Type__c = 'BTN',
                                                           Target_Date_Type__c = 'Expedite',
                                                           opportunity__c = opp.id,
                                                           Account_Name__c = acc.Id,
                                                           Product_Template_Status__c = 'Change',
                                                           SRS_PODS_Product__c = Prod.id,
                                                           Target_Date__c = Date.today().addDays(-1),
                                                           Escalation_Request_Status__c = 'In progress – within network build',
                                                           recordtypeid = SrRecordtypeID);
        
         List < Service_Request__c > lstSR1 = new List < Service_Request__c > ();       
         lstSR1.add(sereq1);
         insert lstSR1;
         System.assertEquals(lstSR1.size(), 1);
      
         List < Service_Request__c > lstSR2 = new List < Service_Request__c > ();
         Service_Request__c sereq2 = new Service_Request__c(Service_Request_Type__c = 'BTN',
                                                           Target_Date_Type__c = 'VP Escalation',
                                                           opportunity__c = opp.id,
                                                           Account_Name__c = acc.Id,
                                                           Product_Template_Status__c = 'Change',
                                                           SRS_PODS_Product__c = Prod.id,
                                                           Target_Date__c = Date.today(),
                                                           Escalation_Request_Status__c = 'Approved – in progress',
                                                           recordtypeid = SrRecordtypeID);
        
         lstSR2.add(sereq2);
         System.assertEquals(lstSR2.size(), 1);
         Boolean resetResult = SRS_ServiceRequestValidationHelper.resetEscalationStatus(lstSR1, lstSR2);
         System.debug('first case resetEscalationStatus Result=' + resetResult);
         System.assertEquals(true, resetResult, 'resetEscalationStatus result should be true');

        
         Service_Request__c sereq3 = new Service_Request__c(Service_Request_Type__c = 'BTN',
                                                           Target_Date_Type__c = 'VP Escalation',
                                                           opportunity__c = opp.id,
                                                           Account_Name__c = acc.Id,
                                                           Product_Template_Status__c = 'Change',
                                                           SRS_PODS_Product__c = Prod.id,
                                                           Target_Date__c = Date.today(),
                                                           Escalation_Request_Status__c = 'In progress – within design teams',
                                                           recordtypeid = SrRecordtypeID);
        List < Service_Request__c > lstSR3 = new List < Service_Request__c > ();
        lstSR3.add(sereq3);
        resetResult = SRS_ServiceRequestValidationHelper.resetEscalationStatus(lstSR1, lstSR3);
        System.debug('second case resetEscalationStatus Result=' + resetResult);
        System.assertEquals(true, resetResult, 'resetEscalationStatus result should be true');
        
     
         Service_Request__c sereq4 = new Service_Request__c(Service_Request_Type__c = 'BTN',
                                                           Target_Date_Type__c = 'VP Escalation',
                                                           opportunity__c = opp.id,
                                                           Account_Name__c = acc.Id,
                                                           Product_Template_Status__c = 'Change',
                                                           SRS_PODS_Product__c = Prod.id,
                                                           Target_Date__c = Date.today().addDays(-2),
                                                           Escalation_Request_Status__c = 'Gating',
                                                           recordtypeid = SrRecordtypeID);
        List < Service_Request__c > lstSR4 = new List < Service_Request__c > (); 
        lstSR4.add(sereq4);
        insert lstSR4;
        System.assertEquals(lstSR4.size(), 1);
  
        Service_Request__c sereq5= new Service_Request__c(Service_Request_Type__c = 'BTN',
                                                           Target_Date_Type__c = 'TELUS-Caused Recovery',
                                                           opportunity__c = opp.id,
                                                           Account_Name__c = acc.Id,
                                                           Product_Template_Status__c = 'Change',
                                                           SRS_PODS_Product__c = Prod.id,
                                                           Target_Date__c = Date.today(),
                                                           Escalation_Request_Status__c = 'Approved – in progress',
                                                           recordtypeid = SrRecordtypeID);
        List < Service_Request__c > lstSR5 = new List < Service_Request__c > (); 
        lstSR5.add(sereq5);
        insert lstSR5;
        System.assertEquals(lstSR5.size(), 1);
        resetResult = SRS_ServiceRequestValidationHelper.resetEscalationStatus(lstSR4, lstSR5);
        System.debug('third case resetEscalationStatus Result=' + resetResult);
        System.assertEquals(false, resetResult, 'resetEscalationStatus result should be false');
        
             
        Service_Request__c sereq6= new Service_Request__c(Service_Request_Type__c = 'BTN',
                                                           Target_Date_Type__c = 'TELUS-Caused Recovery',
                                                           opportunity__c = opp.id,
                                                           Account_Name__c = acc.Id,
                                                           Product_Template_Status__c = 'Change',
                                                           SRS_PODS_Product__c = Prod.id,
                                                           Target_Date__c = Date.today(),
                                                           Escalation_Request_Status__c = 'Completed',
                                                           recordtypeid = SrRecordtypeID);
        List < Service_Request__c > lstSR6 = new List < Service_Request__c > (); 
        lstSR6.add(sereq6);
        insert lstSR6;
        System.assertEquals(lstSR6.size(), 1);
        resetResult = SRS_ServiceRequestValidationHelper.resetEscalationStatus(lstSR4, lstSR6);
        System.debug('fouth case resetEscalationStatus Result=' + resetResult);
        System.assertEquals(true, resetResult, 'resetEscalationStatus result should be true');
        
      
        Service_Request__c sereq7= new Service_Request__c(Service_Request_Type__c = 'BTN',
                                                           Target_Date_Type__c = 'TELUS-Caused Recovery',
                                                           opportunity__c = opp.id,
                                                           Account_Name__c = acc.Id,
                                                           Product_Template_Status__c = 'Change',
                                                           SRS_PODS_Product__c = Prod.id,
                                                           Target_Date__c = Date.today().addDays(-2),
                                                           Escalation_Request_Status__c = 'Completed',
                                                           recordtypeid = SrRecordtypeID);
        List < Service_Request__c > lstSR7 = new List < Service_Request__c > (); 
        lstSR7.add(sereq7);
        insert lstSR7;
   
        Test.stopTest();
    }
    
     
    static testMethod void testSRS_ServiceRequestValidationHelper() 
    {
       
        Account acc = new Account(Name = 'test');
        insert acc;
        
        SRS_PODS_Product__c Prod = new SRS_PODS_Product__c(Single_Address__c = true, Name = 'ISDN PRI');
        insert Prod;
        
        contact c = new contact(firstname = 'aditya', lastname = 'jamwal');
        insert c;
        
        Opportunity opp = new Opportunity(Name = 'OppTest1', StageName = 'Originated', CloseDate = system.today(), Primary_Order_Contact__c = c.id);
        insert opp;
        
        Id SrRecordtypeID = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Prequal').getRecordTypeId();
        
        Service_Request__c sereq = new Service_Request__c(Service_Request_Type__c = 'BTN', 
                                                          Service_Request_Status__c = 'In Progress', 
                                                          Target_Date_Type__c = 'Prequal Requested',
                                                          opportunity__c = opp.id, 
                                                          Account_Name__c = acc.Id, 
                                                          Product_Template_Status__c = 'Complete',
                                                          SRS_PODS_Product__c = Prod.id, 
                                                          Target_Date__c = system.today(), 
                                                          recordtypeid = SrRecordtypeID);
        
        Id SrmoveRecTypeId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Move').getRecordTypeId();
        
        Service_Request__c sereq1 = new Service_Request__c(Service_Request_Type__c = 'BTN', 
                                                           Service_Request_Status__c = 'Ready to Submit', 
                                                           Target_Date_Type__c = 'Prequal Requested',
                                                           opportunity__c = opp.id, 
                                                           Account_Name__c = acc.Id, 
                                                           Product_Template_Status__c = 'Complete',
                                                           SRS_PODS_Product__c = Prod.id, 
                                                           Target_Date__c = system.today(), 
                                                           recordtypeid = SrmoveRecTypeId);
        
        Service_Request__c sereq2 = new Service_Request__c(Service_Request_Type__c='BTN',
                                                           Target_Date_Type__c='Expedite',
                                                           opportunity__c=opp.id,
                                                           Account_Name__c=acc.Id,
                                                           Product_Template_Status__c='Change',
                                                           SRS_PODS_Product__c=Prod.id,
                                                           Target_Date__c=system.today(),
                                                           recordtypeid=SrRecordtypeID);
        
        List < Service_Request__c > lstSR = new List < Service_Request__c > ();
        
        lstSR.add(sereq);
        lstSR.add(sereq1);
         
        lstSR.add(sereq2);
        
        insert lstSR;
        System.assertEquals(lstSR.size(), 3);
        
        SMBCare_Address__c smbAddr = new SMBCare_Address__c(Account__c = acc.id, Street__c = '1st Ave', City__c = 'Burnaby', Province__c = 'AB', Postal_Code__c = '00000');
        insert smbAddr;
        
        SRS_Service_Address__c sa = new SRS_Service_Address__c(Service_Request__c = sereq.id, Address__c = smbAddr.id, Location__c = 'Location A', Demarcation_Location__c = 'test');
        insert sa;
        
        SRS_Service_Request_Charge__c objSR_charge1 = new SRS_Service_Request_Charge__c(Service_Request__c = sereq.id,
                                                                                        Location__c = 'Location A',
                                                                                        Approved_By_Customer__c = c.id, Requested_by__c = 'test', Charge_Type__c = 'Expedite', Pre_approved__c = 'Yes', Approved_Date__c = system.today());
        insert objSR_charge1;
        
        List < SRS_PODS_Template__c > lstQT = new List < SRS_PODS_Template__c > ();
        SRS_PODS_Template__c QuestionTemplate1 = new SRS_PODS_Template__c(ORDER_TYPE__c = 'Install',
                                                                        SYSTEM_GENERATED_QUESTION_ID__c = 1, FOX_PARAMETER__c = 'fox paramter', GROUP_EN__c = 'english',
                                                                        QUESTION_HELP_TEXT_EN__c = 'english', MANDATORY_QUESTION__c = 'O', CHILD__c = 2,
                                                                        SYSTEM_GENERATED_GROUP_ID__c = 1, REPEATABLE_GROUP__c = 'Y',
                                                                        QUESTION_EN__c = 'Quantity of PRIs', SRS_PODS_Product_Master__c = prod.id);
        
        SRS_PODS_Template__c QuestionTemplate2 = new SRS_PODS_Template__c(ORDER_TYPE__c = 'Install',
                                                                        SYSTEM_GENERATED_QUESTION_ID__c = 1, FOX_PARAMETER__c = 'fox paramter', GROUP_EN__c = 'english',
                                                                        QUESTION_HELP_TEXT_EN__c = 'english', MANDATORY_QUESTION__c = 'O', CHILD__c = 2,
                                                                        SYSTEM_GENERATED_GROUP_ID__c = 1, REPEATABLE_GROUP__c = 'Y',
                                                                        QUESTION_EN__c = 'TELUS switch type', SRS_PODS_Product_Master__c = prod.id);
        lstQT.add(QuestionTemplate1);
        lstQT.add(QuestionTemplate2);

        insert lstQT;                                                               
        System.assertEquals(lstQT.size(), 2);
        List < SRS_PODS_Answer__c > lstAns = new List < SRS_PODS_Answer__c > ();
        SRS_PODS_Answer__c Ans1 = new SRS_PODS_Answer__c(SRS_Group_Question__c = 1.0, GroupSerial__c = QuestionTemplate1.SYSTEM_GENERATED_GROUP_ID__c, SRS_PODS_Answer__c = 'answer',
                                                        SRS_PODS_Products__c = QuestionTemplate1.id,
                                                        SRS_Question_ID__c = 1, Service_Request__c = sereq.id);
        
        SRS_PODS_Answer__c Ans2 = new SRS_PODS_Answer__c(SRS_Group_Question__c = 1.0,
                                                        GroupSerial__c = QuestionTemplate2.SYSTEM_GENERATED_GROUP_ID__c, SRS_PODS_Answer__c = 'answer',
                                                        SRS_PODS_Products__c = QuestionTemplate2.id,
                                                        SRS_Question_ID__c = 1, Service_Request__c = sereq.id);
        
        lstAns.add(Ans1);
        lstAns.add(Ans2);
        insert lstAns;
        System.assertEquals(lstAns.size(), 2);
        set < id > setSRids = new set < id > ();
        setSRids.add(sereq.id);
        
        Map < Id, Service_Request__c > oldmap = new Map < Id, Service_Request__c > ();
        Map < Id, Service_Request__c > newmap = new Map < Id, Service_Request__c > ();
        
        oldmap.put(sereq.id, sereq);
        oldmap.put(sereq1.id, sereq1);
       
        oldmap.put(sereq2.id,sereq2);
        
        newmap.put(sereq.id, sereq);
        newmap.put(sereq1.id, sereq1);
        
        newmap.put(sereq2.id,sereq2);
        
        test.StartTest();
        SRS_ServiceRequestValidationHelper.validateServiceRequest(lstSR, oldmap, newmap);
        SRS_ServiceRequestValidationHelper.updateOpportunityStatus(setSRids);
        SRS_ServiceRequestValidationHelper.validateRecTypeServiceRequest(lstSR, oldmap);
        test.StopTest();
        
        sereq.recordtypeid = SrmoveRecTypeId;
        sereq.Target_Date_Type__c = 'Best Effort';
        Id SrRecordtypeFirm = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Provide/Install').getRecordTypeId();

        sereq1.recordtypeid = SrRecordtypeFirm;
        sereq1.Target_Date_Type__c = 'Expedite';
      
        sereq1.Recovery_Remarks__c='Test';
        sereq1.TELUS_Caused_Recovery_Reason__c='Test';        
		sereq1.Requested_By_TELUS__c = createTheTestuser().Id;      
        sereq1.Approved_By_Customer__c = c.Id;
        sereq1.Workflow_Request_Type__c = 'ESR';
        update lstSR;        
       
    }
    
     private static User createTheTestuser()
     {
        User user = new User();
        user.Username = 'smbo-473@telus.com';
        user.LastName = 'X33X';  
        user.Email = 'smbo-473@telus.com';   
        user.CommunityNickname = 'smbo-473';   
        user.TimeZoneSidKey = 'Pacific/Kiritimati';   
        user.LocaleSidKey = UserInfo.getLocale();   
        user.EmailEncodingKey = 'ISO-8859-1';   
        user.ProfileId = UserInfo.getProfileId();   
        user.LanguageLocaleKey = UserInfo.getLanguage();   
        user.Alias = 'smbo-473';
        insert user;
        return user;
    }
    
}