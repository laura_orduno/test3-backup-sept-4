/*
Title: Cloud Projects - Calendar Blocking View
Description: Queries active cloud projects for display in a gantt chart.
Author: Hans Vedo, hvedo@tractionondemand.com
2013-03-28: Updated to include the Id in the SELECT statement, supporting direct project links.
2013-03-13: Test added
2013-02-25: Created
*/

public class trac_CloudProjectsGanttViewController{
    public list<integer> selectedYears{get;set;}
    public list<string> selectedRecordTypes{get;set;}
    public list<ActiveProject> activeProjects{get;set;}
    public list<selectoption> getYears(){
    	list<selectoption> years=new list<selectoption>();
    	integer numProjects=[select count() from smet_project__c where planned_start_date__c!=null];
    	if(numProjects>0){
	    	list<aggregateresult> projects=[select calendar_year(planned_start_date__c) plannedStartDate from smet_project__c where planned_start_date__c!=null group by calendar_year(planned_start_date__c) order by calendar_year(planned_start_date__c) desc limit 10];
	    	for(aggregateresult project:projects){
	    		years.add(new selectoption(string.valueof((integer)project.get('plannedStartDate')),string.valueof((integer)project.get('plannedStartDate'))));
	    	}
    	}
    	return years;
    }
    public list<selectoption> getRecordTypes(){
    	list<selectoption> recordTypes=new list<selectoption>();
    	list<schema.recordtypeinfo> recordTypeInfos=smet_project__c.sobjecttype.getdescribe().getrecordtypeinfos();
    	for(schema.recordtypeinfo recordTypeInfo:recordTypeInfos){
    		if(recordTypeInfo.isavailable()){
    			recordTypes.add(new selectoption(recordTypeInfo.getrecordtypeid(),recordTypeInfo.getname()));
    		}
    	}
    	return recordTypes;
    }
    
    // Default constructor
    public trac_CloudProjectsGanttViewController(){
    }
    
    // Enables turning this view into a Visualforce page.
    public trac_CloudProjectsGanttViewController(ApexPages.StandardSetController controller) {
        controller.setPageSize(10);
    	init();
    }
    void init(){
        // By default, all record types and years are selected
        if(selectedRecordTypes!=null){
        	selectedRecordTypes.clear();
        }else{
    		selectedRecordTypes=new list<string>();
        }
    	list<selectoption> recordTypes=getRecordTypes();
    	if(!recordTypes.isempty()){
    		for(selectoption recordType:recordTypes){
    			selectedRecordtypes.add(recordType.getvalue());
    		}
    	}
        if(selectedYears!=null){
        	selectedYears.clear();	
        }else{
    		selectedYears=new list<integer>();
        }
    	list<selectoption> years=getYears();
    	if(!years.isempty()){
    		for(selectoption year:years){
    			selectedYears.add(integer.valueof(year.getvalue()));
    		}
    	}
    	retrieveProjects();
    }
    void retrieveProjects(){
        // Check if there are any statuses to ignore.
        String wordsToIgnore_string = trac_cloudGantt__c.getOrgDefaults().trac_cloudGanttIgnore__c;
        string[] wordsToIgnore_array=new string[]{'Completed (released and all tasks closed)','Archived (inactive and filed)','On Hold (pending more information)','Cancelled (no longer being worked)','Rejected (not approved)'};
        if(string.isnotblank(wordsToIgnore_string)){
        	wordsToIgnore_array = wordsToIgnore_string.split(',');
        }
        Set<String> wordsToIgnore_list = new Set<String>(wordsToIgnore_array);
        
        // Check if a record type filter exists.
        //String filter = ApexPages.currentPage().getParameters().get('filter');
        
        // Build up the query.
        String projects_sql = 'SELECT id,Name,recordtypeid, RecordType.Name, Milestone__c, Status__c, Planned_Start_Date__c, Planned_Release_Date__c,crm_team_resourced__c,evolution_roadmap__c FROM SMET_Project__c WHERE Milestone__c NOT IN :wordsToIgnore_list AND Planned_Start_Date__c != NULL AND Planned_Release_Date__c != NULL and crm_team_resourced__c!=null and evolution_roadmap__c!=null';
        /*
        projects_sql = projects_sql + ' FROM SMET_Project__c';
        
        // Set the correct ignore field based on sandbox vs. product.
        projects_sql = projects_sql + ' WHERE Milestone__c NOT IN :wordsToIgnore_list';
        
        // Require start and end dates.
        projects_sql = projects_sql + ' AND Planned_Start_Date__c != NULL AND Planned_Release_Date__c != NULL and crm_team_resourced__c!=null and evolution_roadmap__c!=null';
        */
        if(selectedYears!=null){
        	projects_sql+=' and calendar_year(planned_start_date__c) in:selectedYears';
        }
        
        if(selectedRecordTypes!=null){
        	projects_sql+=' and recordtypeid =:selectedRecordTypes';
        }
        
        // Include a filter if it exists.
        /*
        if(filter != NULL && filter != ''){
            projects_sql = projects_sql + ' AND RecordType.Name = :filter';
        }*/
        
        // Order by
        projects_sql = projects_sql + ' ORDER BY Planned_Start_Date__c ASC';  
        
        System.debug('SOQL String: '+wordsToIgnore_string);
        System.debug('SOQL: '+projects_sql);

        // Run the query
        List<SMET_Project__c> queriedProjects = database.query(projects_sql);
        if(activeProjects==null){
        	activeProjects=new list<ActiveProject>();
        }else{
        	activeProjects.clear();
        }
        if(!queriedProjects.isempty()){
            list<Cloud_Enablement_Project_Team__c> members=[select id,Cloud_Enablement_Project__c from Cloud_Enablement_Project_Team__c where Cloud_Enablement_Project__c=:queriedProjects];
            map<id,integer> projectNumMembersMap=new map<id,integer>();
            for(Cloud_Enablement_Project_Team__c member:members){
                if(projectNumMembersMap.containskey(member.cloud_enablement_project__c)){
                    projectNumMembersMap.put(member.Cloud_Enablement_Project__c,projectNumMembersMap.get(member.Cloud_Enablement_Project__c)+1);
                }else{
                    projectNumMembersMap.put(member.Cloud_Enablement_Project__c,1);
                }
            }
            for(smet_project__c project:queriedProjects){
                activeProjects.add(new ActiveProject(project.id,project.name,project.recordtypeid,project.recordtype.name,
                                                  project.planned_start_date__c,
                                                  project.planned_release_date__c,
                                                  project.milestone__c,
                                                  project.status__c,
                                                  (projectNumMembersMap.containskey(project.id)?projectNumMembersMap.get(project.id):0),
                                                  project.crm_team_resourced__c,
                                                  project.evolution_roadmap__c));
            }
        }
    }// Get the number of records to display.
    public Integer getGanttRecordsDisplay(){
        
        // Set a default record count to display.
        Integer GanttRecordsDisplay = (Integer) trac_cloudGantt__c.getOrgDefaults().trac_GanttRecordsDisplay__c;

        return(GanttRecordsDisplay);
    }
    //Get a list of Status
    public list<Schema.Picklistentry> getStatusEntries(){
        return SMET_Project__c.fields.Status__c.getDescribe().getpicklistvalues();
    }   
    /********  Back Up Of Code
    
    public pagereference updateProjects(){
    	system.debug('updating project');
    	retrieveProjects();
    	return null;
    }
    
    public integer startYear{get;set;}
    public integer endYear{get;set;}
    
    
    //Get a list of Status
    public list<Schema.Picklistentry> getStatusEntries(){
        return SMET_Project__c.fields.Status__c.getDescribe().getpicklistvalues();
    }   
    
    // Get the number of records to display.
    public Integer getGanttRecordsDisplay(){
        
        // Set a default record count to display.
        Integer GanttRecordsDisplay = (Integer) trac_cloudGantt__c.getOrgDefaults().trac_GanttRecordsDisplay__c;

        return(GanttRecordsDisplay);
    }
    
    // Get the return URL to get back to the list of projects.
    public String getReturnUrl(){
        return ApexPages.currentPage().getParameters().get('retURL');
    }
    
    
    // Get a list of active projects.
    public List<ActiveProject> getActiveProjects(){
        // Check if there are any statuses to ignore.
        String wordsToIgnore_string = trac_cloudGantt__c.getOrgDefaults().trac_cloudGanttIgnore__c;
        String[] wordsToIgnore_array = wordsToIgnore_string.split(',');
        Set<String> wordsToIgnore_list = new Set<String>(wordsToIgnore_array);
        
        // Check if a record type filter exists.
        String filter = ApexPages.currentPage().getParameters().get('filter');
        
        // Build up the query.
        String projects_sql = 'SELECT Name, RecordType.Name, Milestone__c, Status__c, Planned_Start_Date__c, Planned_Release_Date__c';
        projects_sql = projects_sql + ' FROM SMET_Project__c';
        
        // Set the correct ignore field based on sandbox vs. product.
        projects_sql = projects_sql + ' WHERE Milestone__c NOT IN :wordsToIgnore_list';
        
        // Require start and end dates.
        projects_sql = projects_sql + ' AND Planned_Start_Date__c != NULL';
        projects_sql = projects_sql + ' AND Planned_Release_Date__c != NULL';
        
        if(selectedYears!=null){
        	for(integer selectedYear:selectedYears){
        		system.debug('year:'+selectedYear);
        	}
        	projects_sql+=' and calendar_year(planned_start_date__c) in:selectedYears';
        }
        
        // Include a filter if it exists.
        if(filter != NULL && filter != ''){
            projects_sql = projects_sql + ' AND RecordType.Name = :filter';
        }
        
        // Order by
        projects_sql = projects_sql + ' ORDER BY Planned_Start_Date__c ASC';  
        
        System.debug('SOQL String: '+wordsToIgnore_string);
        System.debug('SOQL: '+projects_sql);

        // Run the query
        List<SMET_Project__c> activeProjects = database.query(projects_sql);
        list<ActiveProject> allProjects=new list<ActiveProject>();
        if(!activeProjects.isempty()){
            list<Cloud_Enablement_Project_Team__c> members=[select id,Cloud_Enablement_Project__c from Cloud_Enablement_Project_Team__c where Cloud_Enablement_Project__c=:activeProjects];
            map<id,integer> projectNumMembersMap=new map<id,integer>();
            for(Cloud_Enablement_Project_Team__c member:members){
                if(projectNumMembersMap.containskey(member.cloud_enablement_project__c)){
                    projectNumMembersMap.put(member.Cloud_Enablement_Project__c,projectNumMembersMap.get(member.Cloud_Enablement_Project__c)+1);
                }else{
                    projectNumMembersMap.put(member.Cloud_Enablement_Project__c,1);
                }
            }
            for(smet_project__c project:activeProjects){
                allProjects.add(new ActiveProject(project.id,project.name,project.recordtype.name,
                                                  project.planned_start_date__c,
                                                  project.planned_release_date__c,
                                                  project.milestone__c,
                                                  project.status__c,
                                                  (projectNumMembersMap.containskey(project.id)?projectNumMembersMap.get(project.id):0)));
            }
        }
        // Return the list to the VF page.
        return allProjects;
    }*/
    
    // Get a list of record types.
    /*
    public List<RecordType> getRecordTypes(){
        List<RecordType> RecordTypes = [
            SELECT Id, Name
            FROM RecordType
            WHERE isActive = true
            AND SobjectType = 'SMET_Project__c'
            ORDER BY Name ASC
        ];
        
        return(RecordTypes);
    }*/
    
    class ActiveProject{
        public id projectId{get;set;}
        public string name{get;set;}
        public id recordTypeId{get;set;}
        public string recordTypeName{get;set;}
        public date plannedStartDate{get;set;}
        public integer plannedStartYear{get;set;}
        public date plannedReleaseDate{get;set;}
        public string milestone{get;set;}
        public string status{get;set;}
        public integer numMembers{get;set;}
        public string crmTeamResourced{get;set;}
        public string evolutionRoadmap{get;set;}
        public ActiveProject(){
        }
        public ActiveProject(id projectId,string name,id recordTypeId,string recordTypeName,date plannedStartDate,date plannedReleaseDate,string milestone,string status,integer numMembers,string crmTeamResourced,string evolutionRoadmap){
            this.projectId=projectId;
            this.name=name;
            this.recordTypeId=recordTypeId;
            this.recordTypeName=recordTypeName;
            this.plannedStartDate=plannedStartDate;
            this.plannedStartYear=plannedStartDate.year();
            this.plannedReleaseDate=plannedReleaseDate;
            this.milestone=milestone;
            this.status=status;
            this.numMembers=numMembers;
            this.crmTeamResourced=crmTeamResourced;
            this.evolutionRoadmap=evolutionRoadmap;
        }
    }
}