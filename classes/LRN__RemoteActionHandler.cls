/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class RemoteActionHandler {
    @RemoteAction
    global static Id addResourceToFavorite(String favResourceJson, String parentObject) {
        return null;
    }
    @RemoteAction
    global static SObject getFavoriteResource(Id favResourceId) {
        return null;
    }
    @RemoteAction
    global static void insertResourceTracking(Id feedItemId, String location, String parentObject) {

    }
    @RemoteAction
    global static void removeResourceFromFavorite(Id favResourceId) {

    }
}
