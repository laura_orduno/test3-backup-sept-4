@isTest
private class QuoteBundleCommitmentClientTest {
    private static testMethod void testQuoteBundleCommitmentClient() {
        Account a = new Account(name = 'test');
        insert a;
        
        Web_Account__c wa = new Web_Account__c(Company_Legal_Name__c = 'Company Legal Name');
        insert wa;
        
        Contact c = new Contact(
            AccountId = a.id,
            FirstName = 'first',
            LastName = 'last',
                Phone = '123-456-7890',
                Email = 'contact@example.com',
                Title = 'title',
                Role_c__c = 'testrole'
        );
        insert c;
        
        Opportunity o = new Opportunity(
            name = 'test opp',
            AccountId = a.id,
            Web_Account__c = wa.id,
            closedate = Date.today(),
            stagename = 'test'
        );
        insert o;
        
        Address__c addr = new Address__c(
            Special_Location__c = 'special',
            Street__c = 'street',
            City__c = 'city',
            Rate_Band__c = 'F',
            State_Province__c = 'BC',
            Postal_Code__c = 'postalcode'
        );
        insert addr;
        
        SBQQ__Quote__c quote = new SBQQ__Quote__c(
                SBQQ__Account__c = a.id,
                SBQQ__PrimaryContact__c = c.id,
                Install_Location__c = addr.id,
                Province__c = 'BC',
                Rate_Band__c = 'F',
                Expiry_Date__c = Date.newInstance(2014,01,01),
                SBQQ__Opportunity__c = o.id,
                SBQQ__ShippingStreet__c = 'shipstreet',
                SBQQ__ShippingCity__c = 'shipcity',
                SBQQ__ShippingState__c = 'shipstate',
                SBQQ__ShippingPostalCode__c = 'shipcode',
                SBQQ__SubscriptionTerm__c = 3,
                SBQQ__CustomerDiscount__c = 5,
                SBQQ__PartnerDiscount__c = 10,
                SBQQ__MarkupRate__c = 4,
                SBQQ__StartDate__c = Date.newInstance(2014,1,1),
                SBQQ__EndDate__c = Date.newInstance(2015,1,1)
        );
        insert quote;
        
        Product2 p = new Product2(name = 'Test Bundle', RWMS_Bundle_Name__c = 'bundle', Wireless_Offer_Code__c = '12345');
        insert p;
        
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c(SBQQ__Quote__c = quote.id, SBQQ__Product__c = p.id, SBQQ__Quantity__c = 4);
        insert ql;
        
        ql = new SBQQ__QuoteLine__c(SBQQ__Quote__c = quote.id, SBQQ__Product__c = p.id, SBQQ__Quantity__c = null);
        insert ql;
        
        Quote_Portal__c qp = new Quote_Portal__c(Send_To_RWMS__c = true, RWMS_Endpoint__c = 'http://test.com/');
        
        Test.startTest();
        
        QuoteBundleCommitmentClient.sendCommitmentMessage(quote.id);
        
        Test.stopTest();
    }
}