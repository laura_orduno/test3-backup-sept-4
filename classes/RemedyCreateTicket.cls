public class RemedyCreateTicket {

	public class sendRequest {

		public RemedyCreateTicket.sendRequest SRequest(string description, string comments, string pin, string casenumber, string firstName, string lastName, string bu_id, string compID, string email, ID CaseId, string ticketType, string userLanguage) {

			// Create the request envelope
			DOM.Document doc = new DOM.Document();
			//String endpoint = 'http://205.206.192.11/arsys/services/ARService?server=205.206.195.52&webService=ExternalTicketSubmissionCreate';
			// String endpoint = 'https://soa-mp-toll-dv.tsl.telus.com:443/SMO/ProblemMgmt/ExternalTicketCreationService_v1_0_vs0';
			//String endpoint = 'https://xmlgwy-pt1.telus.com:9030/dv/SMO/ProblemMgmt/ExternalTicketCreationService_v1_0_vs0';
			string endpoint = '';
			// TODO: Re-leverage custom setting for endpoint, no hard-coding
			if (!Test.isRunningTest()) {
				endpoint = SMBCare_WebServices__c.getInstance('ENDPOINT_Remedy_Create').Value__c;
			} else {
				endpoint = 'https://xmlgwy.telus.com:9030/SMO/ProblemMgmt/ExternalTicketCreationService_v1_0_vs0';
			}
			// String endpoint = SMBCare_WebServices__c.getInstance('ENDPOINT_Remedy_Create').Value__c;
			//String endpoint = 'https://xmlgwy.telus.com:9030/SMO/ProblemMgmt/ExternalTicketCreationService_v1_0_vs0';
			////////////
			String soapNS = 'http://schemas.xmlsoap.org/soap/envelope/';
			string urn = 'urn:ExternalTicketSubmissionCreate';
			//string asr = 'http://assurance.ebonding.telus.com';
			datetime creationDateTime = datetime.now();
			date mydate = creationDateTime.date();
			datetime getmytime = datetime.now();
			time mytime = getmytime.time();
			if (comments != '') {
				comments = comments.escapeUnicode();
			}
			string username = 'salesforce';
			string password = 'SFDC2016';
			string assigned_group = 'SPOC FRONTLINE';
			string impact = '4-Minor/Localized';
			string reported_source = 'Self Service';
			String service_type = 'User Service Restoration';
			String status = 'New';
			String action = 'CREATE';
			String urgency = 'Item3';
			string testTime = string.valueOf(mydate) + 'T' + string.valueOf(mytime);
			string createdate = string.valueOfGmt(creationDateTime);
			string logicalId = 'TELUS-SFDC';
			string messageId = logicalId + creationDateTime;
			String encodemessageId = EncodingUtil.base64Encode(Blob.valueOf(messageId));
			if (userLanguage == 'en_US') {
				userLanguage = 'English';
			}
			if (userLanguage == 'fr') {
				userLanguage = 'French';
			}

			/*if (projectNum != '')
{
projectNum = 'CPS' + projectNum;
}*/

			dom.XmlNode envelope
					= doc.createRootElement('Envelope', soapNS, 'soapenv');
			envelope.setNamespace('urn', urn);
			//envelope.setNamespace('asr', asr);

			dom.XmlNode header = envelope.addChildElement('Header', soapNS, null);
			dom.XmlNode AuthenticationInfo = header.addChildElement('AuthenticationInfo', urn, 'urn');

			// dom.XmlNode sender = AuthenticationInfo.addChildElement('sender', ebon, null);
			AuthenticationInfo.addChildElement('userName', urn, null).
					addTextNode(username);
			AuthenticationInfo.addChildElement('password', urn, null).
					addTextNode(password);
			AuthenticationInfo.addChildElement('authentication', urn, null).
					addTextNode('');
			AuthenticationInfo.addChildElement('locale', urn, null).
					addTextNode('');
			AuthenticationInfo.addChildElement('timeZone', urn, null).
					addTextNode('');

			dom.XmlNode body = envelope.addChildElement('Body', soapNS, null);
			dom.XmlNode OpCreate = body.addChildElement('OpCreate', urn, 'urn');
			OpCreate.addChildElement('Action', urn, null).
					addTextNode('0');
			OpCreate.addChildElement('Alternate_Contact', urn, null).
					addTextNode('');
			OpCreate.addChildElement('Attachment_attachmentName', urn, null).
					addTextNode('');
			OpCreate.addChildElement('Attachment_attachmentData', urn, null).
					addTextNode('');
			OpCreate.addChildElement('Business_Unit_Id', urn, null).
					addTextNode(bu_id);
			OpCreate.addChildElement('Category', urn, null).
					addTextNode('');
			OpCreate.addChildElement('Class', urn, null).
					addTextNode('Service Issue');
			OpCreate.addChildElement('Company_Id', urn, null).
					addTextNode(compID);
			OpCreate.addChildElement('Configuration_id', urn, null).
					addTextNode('');
			OpCreate.addChildElement('Description', urn, null).
					addTextNode(comments);
			OpCreate.addChildElement('Key_Item', urn, null).
					addTextNode('');
			OpCreate.addChildElement('Language', urn, null).
					addTextNode(userLanguage);
			OpCreate.addChildElement('Origin', urn, null).
					addTextNode('SFDC');
			OpCreate.addChildElement('PIN', urn, null).
					addTextNode(pin);
			OpCreate.addChildElement('Severity', urn, null).
					addTextNode('2');
			OpCreate.addChildElement('Short_Description', urn, null).
					addTextNode(description);
			OpCreate.addChildElement('System_Name', urn, null).
					addTextNode('SALESFORCE');
			OpCreate.addChildElement('Type', urn, null).
					addTextNode('');
			OpCreate.addChildElement('z-IVR_Option', urn, null).
					addTextNode('ZAAQ');
			OpCreate.addChildElement('Customer_Ticket_', urn, null).
					addTextNode(casenumber);
			System.debug(doc.toXmlString());

			// TODO: add exception handling

			// Send the request
			HttpRequest req = new HttpRequest();
			req.setMethod('POST');
			req.setEndpoint(endpoint);
			// TODO: specify a timeout of 2 minutes
			req.setTimeout(120000);

			String creds;
			if (!Test.isRunningTest()) {
				SMBCare_WebServices__c wsUsername = SMBCare_WebServices__c.getValues('username_pc');
				SMBCare_WebServices__c wsPassword = SMBCare_WebServices__c.getValues('password');
				if (String.isNotBlank(wsUsername.Value__c) && String.isNotBlank(wsPassword.Value__c))
					creds = wsUsername.Value__c + ':' + wsPassword.Value__c;
			} else {
				creds = 'APP_SFDC:soaorgid';
			}

			/*   SMBCare_WebServices__c wsUsername = SMBCare_WebServices__c.getValues('username_pc');
			SMBCare_WebServices__c wsPassword = SMBCare_WebServices__c.getValues('password');

			   if (String.isNotBlank(wsUsername.Value__c)&& String.isNotBlank(wsPassword.Value__c))
				   creds = wsUsername.Value__c+':'+wsPassword.Value__c;*/

			String encodedusernameandpassword = EncodingUtil.base64Encode(Blob.valueOf(creds));

			//Blob headerValue = Blob.valueOf(encodedusernameandpassword);
			String authorizationHeader = 'BASIC ' + encodedusernameandpassword;
			// EncodingUtil.base64Encode(headerValue);
			req.setHeader('Authorization', authorizationHeader);
			req.setHeader('Content-Type', 'text/xml');

			req.setBodyDocument(doc);

			// TODO: Determine the level of detail webservice is returning in regards to errors.

			Http http = new Http();
			HttpResponse res = new HttpResponse();

			if (!Test.isRunningTest()) {
				res = http.send(req);
			} else {
				res = ENTPUtils.mockWebserviceResponse(ENTPUtils.REMEDY_XML_RESPONSE);
			}

			System.debug('GetBody line 110:' + res.getBody());
			// System.assertEquals(200, res.getStatusCode());

			dom.Document resDoc = res.getBodyDocument();
			XmlStreamReader reader = res.getXmlStreamReader();

			// Read through the XML
			String OKTicketId = '';
			while (reader.hasNext()) {
				System.debug('Event Type:' + reader.getEventType());
				if (reader.getEventType() == XmlTag.START_ELEMENT) {
					System.debug('getLocalName: ' + reader.getLocalName());
					if ('Ticket_Number' == (reader.getLocalName())) {
						boolean isSafeToGetNextXmlElement = true;
						while (isSafeToGetNextXmlElement) {

							if (reader.getEventType() == XmlTag.END_ELEMENT) {
								OKTicketId = reader.getLocalName();
								System.debug('OK Ticket Element Line 158 :' + OKTicketId);
								break;
							} else if (reader.getEventType() == XmlTag.CHARACTERS) {
								OKTicketId = reader.getText();
								System.debug('OK Ticket Element Line 162 :' + OKTicketId);
								if (OKTicketId != '' && OKTicketId != null) {
									Case tmpCase = [
											SELECT Id, CaseNumber, Lynx_Ticket_Number__c, Status, NotifyCollaboratorString__c
											FROM Case
											WHERE CaseNumber = :casenumber
									];
									System.debug('Select Case line 167 :' + tmpCase);
									if (tmpCase != null && OKTicketId != null) {
										tmpCase.Lynx_Ticket_Number__c = OKTicketId;
										System.debug('Collab tmpCase.NotifyCollaboratorString__c: ' + tmpCase.NotifyCollaboratorString__c);
									}
									System.debug('RemedyCreateTicket->UpdateCase, CaseNumber(' + tmpCase.CaseNumber + '), TicketNumber(' + OKTicketId + ')');
									update tmpCase;
									if (tmpCase.NotifyCollaboratorString__c != null && tmpCase.NotifyCollaboratorString__c.length() > 0) {
										List<String> collabEmails = tmpCase.NotifyCollaboratorString__c.split(';', 0);
										if (collabEmails.size() > 0) {
											ENTP_Case_Collaborator_Settings__c collabSettings = ENTP_Case_Collaborator_Settings__c.getOrgDefaults();
											System.debug('Collab collabSettings: ' + collabSettings);
											//String templateName = collabSettings.New_Case_Template_Name__c;
											//List<EmailTemplate> templates = [select id from EmailTemplate where DeveloperName = :templateName LIMIT 1];
											//System.debug('Collab templates: ' + templates);
											Id emailTemplateId = collabSettings.New_Case_Template_Id__c;
											if (emailTemplateId != null) {
												//Id emailTemplateId = templates[0].Id;
												//Id emailTemplateId = '00X4000000163uYEAQ';
												Id fromEmailId = null;
												List<OrgWideEmailAddress> fromEmails = [select Id from OrgWideEmailAddress where DisplayName = 'ENTP'];
												if (fromEmails.size() > 0) {
													fromEmailId = fromEmails[0].Id;
												}
												try {
													List<Messaging.SendEmailResult> mailResults = ENTPUtils.sendTemplatedCaseEmails(collabEmails, fromEmailId, emailTemplateId, tmpCase.Id);
												} catch (Exception e) {
													System.debug('Collab caught ENTPUtls.sendTemplatedCaseEmails() exception: ' + e);
												}
											}
										}
									}

								}

							}
							// Always use hasNext() before calling next() to confirm
							// that we have not reached the end of the stream

							if (reader.hasNext()) {

								reader.next();

							} else {

								isSafeToGetNextXmlElement = false;
								break;
							}
						}

					}
				}
				reader.next();
			}
			if (OKTicketId == null || OKTicketId == '') {
				try {
					Ticket_Event__c NewEvent = new Ticket_Event__c(
							Case_Number__c = caseNumber,
							Event_Type__c = 'New Ticket',
							Status__c = 'New',
							Failure_Reason__c = 'Remedy did not accept ticket create',
							//Lynx_Ticket_Number__c = ticketNum,
							Case_Id__c = CaseId
					);
					System.debug('RemedyTicketCreate->NewEvent, CaseNumber(' + caseNumber + ')');
					insert NewEvent;

				} catch (DmlException e) {
					System.debug('RemedyTicketCreate->TicketEvent, exception: ' + e.getMessage());
				}

			}

			envelope = resDoc.getRootElement();
			//  String troubleTick = 'http://www.telus.com/schema/servicestatus/troubleticket';
			//String wsa = 'http://schemas.xmlsoap.org/soap/envelope/';
			//Dom.XMLNode TroubleTicket = resDoc.getRootElement();
			// dom.XmlNode header1 = envelope.getChildElement('Header', soapNS);
			// String TELUSTicketId = TroubleTicket.getChildElement('TELUSTicketId',null).getText();
			//System.debug('TELUSTicketId: ' + TELUSTicketId);
			System.debug(resDoc.toXmlString());
			System.debug('Envelope=:' + envelope);
			//  System.debug('TroubleTicket=:' + TroubleTicket);
			//System.debug(header1);
			//System.debug(testTime);

			return null;
		}

	}

}