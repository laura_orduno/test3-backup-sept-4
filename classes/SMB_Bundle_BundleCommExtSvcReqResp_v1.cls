//Generated by wsdl2apex

public class SMB_Bundle_BundleCommExtSvcReqResp_v1 {
    public class CreateBundleCommitmentResponse {
    	public DateTime dateTimeStamp;
        public String errorCode;
        public String messageType;
        public String transactionId;
        public SMB_Bundle_EnterpriseCommonTypes_v7.Message[] messageList;
        public String contextData;
        private String[] dateTimeStamp_type_info = new String[]{'dateTimeStamp','http://www.w3.org/2001/XMLSchema','dateTime','1','1','false'};
        private String[] errorCode_type_info = new String[]{'errorCode','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] messageType_type_info = new String[]{'messageType','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] transactionId_type_info = new String[]{'transactionId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] messageList_type_info = new String[]{'messageList','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v7','Message','0','10','false'};
        private String[] contextData_type_info = new String[]{'contextData','http://www.w3.org/2001/XMLSchema','string','0','1','false'};       
private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v7','true','false'};
         private String[] field_order_type_info = new String[]{'dateTimeStamp','errorCode','messageType','transactionId','messageList','contextData'};
    }
    public class CreateBundleCommitment {
        public String quoteRefNum;
        public String bundleName;
        public String billingAccountNum;
        public SMB_Bundle_BundleCommitmentCommonType_v1.CustomerInfo customerInfo;
        public SMB_Bundle_BundleCommitmentCommonType_v1.WirelessOfferReference[] wirelessOfferRefList;
        private String[] quoteRefNum_type_info = new String[]{'quoteRefNum','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] bundleName_type_info = new String[]{'bundleName','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] billingAccountNum_type_info = new String[]{'billingAccountNum','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] customerInfo_type_info = new String[]{'customerInfo','http://xmlschema.tmi.telus.com/xsd/Product/ProductOffering/BundleCommitmentCommonType_v1','CustomerInfo','0','1','false'};
        private String[] wirelessOfferRefList_type_info = new String[]{'wirelessOfferRefList','http://xmlschema.tmi.telus.com/xsd/Product/ProductOffering/BundleCommitmentCommonType_v1','WirelessOfferReference','0','50','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/srv/CMO/ContactMgmt/BundleCommitmentExtSvcRequestResponse_v1','true','false'};
        private String[] field_order_type_info = new String[]{'quoteRefNum','bundleName','billingAccountNum','customerInfo','wirelessOfferRefList'};
    }
}