@isTest
private class CommunitiesLandingRestCalloutTest {
     private static void createData(){
          System.runAs(new User(Id = Userinfo.getUserId())) {
              OrdrTestDataFactory.createOrderingWSCustomSettings('ChannelPortalCacheMgmtEndPoint', 'https://webservice1.preprd.teluslabs.net/RMO/ResourceMgmt/ReserveResource_v2_0_vs1_DV');
              Account a = new Account(Name='TestAccount', CustProfId__c='123456',Channel_Org_Code__c='testV');
              insert a;
              Contact testContact = new Contact(LastName='TestContact',Phone = '9876543281' ,
                                                Email = 'TestMail@telus.com', 
                                                HomePhone = '6476486478', 
                                                MobilePhone = '6476486478');
              testContact.accountid=a.id;
              insert testContact;
              User userObj=[SELECT UserRoleId,Id, ContactId, Contact.Account.Channel_Org_Code__c,FederationIdentifier FROM User WHERE id=:Userinfo.getUserId()];
              userObj.FederationIdentifier=Userinfo.getUserId();
              //userObj.ContactId=testContact.id;
             
              UserRole role = new UserRole(name = 'Test Manager');    
              insert role;
              
              userObj.UserRoleId=role.id;
               update userObj;
              
          }
      }
 @isTest
    private static void doPostTest(){
         createData();        
        Test.startTest();
        
       String uuid=[select FederationIdentifier from user where id=:Userinfo.getUserId()].FederationIdentifier;
        CommunitiesLandingRestCallout.doPost('',uuid,'cacheKey','firstName','lastName','TEST@TEST.COM','salesRepPIN','portalRoleName','FR') ;
        Test.stopTest();
        //CommunitiesLandingController.doPost(String roleId,String uuid,String cacheKey,String firstName,String lastName,String salesRepBusinessEmail,String salesRepPIN,String portalRoleName,String salesRepPreferredContactLanguageCode) 
            
    }
}