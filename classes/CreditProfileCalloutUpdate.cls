global class CreditProfileCalloutUpdate {
    
    /*------------- Ping starts ------------------------------------------------*/
    @future (callout=true)
    webservice static void ping() {        
        try {    
            
            CreditProfileService.BusinessCreditProfileMgmtService_v1_0_SOAP 
                servicePort = new CreditProfileService.BusinessCreditProfileMgmtService_v1_0_SOAP();                
            CreditProfileCalloutUpdateHelper.prepareCallout(servicePort);   
            if ( !Test.isRunningTest() ) servicePort.Ping();  
            
        }catch(CalloutException e){
            System.debug(' ... dyy ... e.getMessage = ' + e.getMessage()); 
            System.debug(' ... dyy ... e.getCause = ' + e.getCause()); 
            System.debug(' ... dyy ... e.getStackTraceString = ' + e.getStackTraceString()); 
        }                   
    }
    /*------------------------------------------------------------------------*/
    
    
    @future(callout=true)
    webservice static void createBusinessCreditProfile(Long businessCustomerId,ID cpId) {  
        System.debug(' ... dyy ... createBusinessCreditProfile...'); 
        System.debug(' ... dyy ... businessCustomerId = ' + businessCustomerId); 
        
        CreditProfileServiceTypes.CreditAuditInfo auditInfo 
            = CreditWebServiceClientUtil.getProfileAuditInfo();
        
        
        String creditID = String.valueof(businessCustomerId);
        String creditObject = 'Credit_Profile__c';
        String operationOrMethod = 'CreditProfileCalloutUpdate.createBusinessCreditProfile';
        String payload = '... PAYLOAD ...: businessCustomerId = ' + businessCustomerId + ', ' + auditInfo;
        String result;
        Integer retryCount;
        String retryStatus;
        String status;
        String createdById = auditInfo.userId;
        DateTime createdDate = Datetime.now();
        
        if(businessCustomerId!=null)
            
            
            try {            
                
                CreditProfileService.BusinessCreditProfileMgmtService_v1_0_SOAP 
                    servicePort = new CreditProfileService.BusinessCreditProfileMgmtService_v1_0_SOAP();                
                CreditProfileCalloutUpdateHelper.prepareCallout(servicePort);  
                
                System.debug(' ... dyy ... calling to createBusinessCreditProfile ...'); 
	            CreditProfileServiceTypes.BusinessCreditProfileBase newBusinessCreditProfile 
        		        = new CreditProfileServiceTypes.BusinessCreditProfileBase();            
                if (cpId!=null) {
	        		Credit_Profile__c creditProfil = CreditProfileCalloutUpdateHelper.getCreditProfile(cpId);        
            		CreditProfileCalloutUpdateHelper.populateUpdatedBusinessCreditProfile(
                		newBusinessCreditProfile
                		,creditProfil);            
                }        
                
                servicePort.createBusinessCreditProfile(
                    businessCustomerId
                    ,newBusinessCreditProfile
                    ,null
                    ,auditInfo);   
                
                status = 'Success';
                
                CreditWebServiceClientUtil.writeToCreditRequestHistory(
                    creditID
                    , creditObject
                    , operationOrMethod
                    , payload
                    , result
                    , retryCount
                    , retryStatus
                    , status
                    , createdById
                    , createdDate);
                
                if ( Test.isRunningTest() ) throw new CalloutException();
            }catch(CalloutException e){
                
                CreditProfileCalloutUpdateHelper.deleteCreditProfileFromSFDC(cpId);
                
                CreditWebServiceClientUtil.logCalloutErrors(
                    creditID
                    , creditObject
                    , operationOrMethod
                    , payload
                    , result
                    , retryCount
                    , retryStatus
                    , status
                    , createdById
                    , createdDate
                    , e);
            }   
    }
    
    
    @future(callout=true)
    webservice static void associateCustomer(ID profileId){  
        System.debug('... dyy ... future callout to associateCustomer ...');  
        System.debug('... dyy ... profileId = ' + profileId);
        
        Credit_Profile__c cp = CreditProfileCalloutUpdateHelper.getCreditProfile(profileId);
        
        Long businessCustomerId = 0L;
        if(!Test.isRunningTest()) businessCustomerId = Long.valueof(cp.Account__r.CustProfId__c);
        Long individualCustomerId = Long.valueof(cp.Consumer_profile_ID__c);
        
        CreditProfileServiceTypes.CreditAuditInfo auditInfo 
            = CreditWebServiceClientUtil.getProfileAuditInfo();          
        
        String creditID = String.valueof(businessCustomerId) + ' , ' + String.valueof(individualCustomerId);
        String creditObject = 'Credit_Profile__c';
        String operationOrMethod = 'CreditProfileCalloutUpdate.associateCustomer';
        String payload = '... PAYLOAD ...: businessCustomerId = ' + businessCustomerId 
            + ', ' + individualCustomerId 
            + ', ' + auditInfo;
        String result;
        Integer retryCount;
        String retryStatus;
        String status;
        String createdById = auditInfo.userId;
        DateTime createdDate = Datetime.now();
        
        
        try {            
            
            CreditProfileService.BusinessCreditProfileMgmtService_v1_0_SOAP 
                servicePort = new CreditProfileService.BusinessCreditProfileMgmtService_v1_0_SOAP();                
            CreditProfileCalloutUpdateHelper.prepareCallout(servicePort);  
            
            servicePort.replaceBusinessProprietor(
                businessCustomerId
                ,individualCustomerId
                ,auditInfo);    
            
            status = 'Success';
            
            CreditWebServiceClientUtil.writeToCreditRequestHistory(
                creditID
                , creditObject
                , operationOrMethod
                , payload
                , result
                , retryCount
                , retryStatus
                , status
                , createdById
                , createdDate);
            
            if ( Test.isRunningTest() ) throw new CalloutException();
        }catch(CalloutException e){
            
            CreditWebServiceClientUtil.logCalloutErrors(
                creditID
                , creditObject
                , operationOrMethod
                , payload
                , result
                , retryCount
                , retryStatus
                , status
                , createdById
                , createdDate
                , e);
        }     
    }
    
    @future(callout=true)
    webservice static void updateCreditProfile(ID profileId){  
        System.debug('... dyy ... future callout to createAndOrUpdateCreditProfile');  
        System.debug('... dyy ... profileId = ' + profileId);  
        Credit_Profile__c creditProfil = CreditProfileCalloutUpdateHelper.getCreditProfile(profileId);        
        Long businessCustomerId = CreditProfileCalloutUpdateHelper.getBusinessCustomerId(creditProfil);         
        Long individualCustomerId = CreditProfileCalloutUpdateHelper.getIndividualCustomerId(creditProfil);         
        if(businessCustomerId!=null) {
            if(creditProfil.Legal_Entity_Type__c=='Sole Proprietor (PR)'){
                updateBusinessCreditProfile(businessCustomerId, creditProfil);
                if(individualCustomerId>0L)
                    updateIndivdualCreditProfile(individualCustomerId, creditProfil);
            } else 
                updateBusinessCreditProfile(businessCustomerId, creditProfil);
        }
    }
    
    webservice static void updateBusinessCreditProfile(
        Long businessCustomerId
        ,Credit_Profile__c creditProfil) {  
            
            CreditProfileServiceTypes.BusinessCreditProfileBase updatedBusinessCreditProfile 
                = new CreditProfileServiceTypes.BusinessCreditProfileBase();            
            CreditProfileCalloutUpdateHelper.populateUpdatedBusinessCreditProfile(
                updatedBusinessCreditProfile
                ,creditProfil);            
            
            Long proprietorshipIndividualCustomerId;
            if(creditProfil.Consumer_profile_ID__c!=null)
                proprietorshipIndividualCustomerId= Long.valueof(creditProfil.Consumer_profile_ID__c);
            System.debug('... dyy ... proprietorshipIndividualCustomerId = ' + proprietorshipIndividualCustomerId);
            
            CreditProfileServiceTypes.CreditAuditInfo auditInfo 
                = CreditWebServiceClientUtil.getProfileAuditInfo();             
            
            String creditID = String.valueof(businessCustomerId);
            String creditObject = 'Credit_Profile__c';
            String operationOrMethod = 'CreditProfileCalloutUpdate.updateBusinessCreditProfile';
            String payload = 'PAYLOAD: businessCustomerId = ' + businessCustomerId
                + ', ' + updatedBusinessCreditProfile
                + ', ' + 'proprietorshipIndividualCustomerId = ' + proprietorshipIndividualCustomerId
                + ', ' + auditInfo
                ;
            String result;
            Integer retryCount;
            String retryStatus;
            String status;
            String createdById = auditInfo.userId;
            DateTime createdDate = Datetime.now();
            
            
            try {            
                
                CreditProfileService.BusinessCreditProfileMgmtService_v1_0_SOAP 
                    servicePort = new CreditProfileService.BusinessCreditProfileMgmtService_v1_0_SOAP();                
                CreditProfileCalloutUpdateHelper.prepareCallout(servicePort);  
                
                System.debug(' ... dyy ... calling to updateBusinessCreditProfile ...'); 
                
                servicePort.updateBusinessCreditProfile(
                    businessCustomerId
                    ,updatedBusinessCreditProfile
                    ,proprietorshipIndividualCustomerId
                    ,auditInfo);    
                
                status = 'Success';
                
                /*CreditWebServiceClientUtil.writeToCreditRequestHistory(
creditID
, creditObject
, operationOrMethod
, payload
, result
, retryCount
, retryStatus
, status
, createdById
, createdDate);*/
                
                if ( Test.isRunningTest() ) throw new CalloutException();
            }catch(CalloutException e){
                
                
                CreditWebServiceClientUtil.logCalloutErrors(
                    creditID
                    , creditObject
                    , operationOrMethod
                    , payload
                    , result
                    , retryCount
                    , retryStatus
                    , status
                    , createdById
                    , createdDate
                    , e);
            }                   
        }
    
    
    /*--------------------------------------------------*/
    
    
    webservice static void createCreditProfileInSalesforceWithAccountId(ID accId){
        try{
            Account acc = [select id,name,rcid__c from Account where id = :accId limit 1];
            createCreditProfileForNewProspectAccount(acc);
            System.debug('... dyy ... call successful ... for createCreditProfileInSalesforceWithAccountId ...');
        }catch (Exception e){
            System.debug('... dyy ... ooops wrong call ... for createCreditProfileInSalesforceWithAccountId ...');
        }
    }
    
    
    
    public static void createCreditProfileForNewProspectAccount(Account acc){
        Credit_Profile__c cp = new Credit_Profile__c();
        System.debug('... dyy ... acc.RCID__c = ' + acc.RCID__c); 
        cp.Name = 'CP-' + getAccName(acc.Name) + '-' + acc.RCID__c; 
        cp.Account__c = acc.Id;
        cp.RecordTypeId = Schema.SObjectType.Credit_Profile__c
            .getRecordTypeInfosByName().get(Label.Credit_Business).getRecordTypeId(); 
        cp.OwnerId = UserInfo.getUserId();
        cp.is_auto_update__c = false;
        cp.risk_indicator__c = 'NOT ASSESSED';
        cp.Process_Timer_to_Create_CP__c = datetime.now().addMinutes(61);
        insert cp;
        
        acc.Credit_Profile__c = cp.id;
        update acc;   
    }
    
    private static String getAccName(String accName){
        if (String.isBlank(accName)) return null;
        Integer accNameLength = accName.length();   
        if(accNameLength>=65)
            accName = accName.substring(0,65); 
        return accName;
    }
    
    /*------------- trigger callout proxy ----------------------------------------*/    
    
    private static Long getMasterCustomerId (String cpid){
        System.debug(' ... dyy ... getMasterCustomerId.cpid = ' + cpid); 
        Long customerId = 0L;
        if(cpid==null) return customerId;
        Credit_Profile__c cp = CreditProfileCalloutUpdateHelper.getCreditProfile(cpid);
        if(cp!=null &&cp.Account__r!=null &&cp.Account__r.CustProfId__c!=null) 
            customerId = Long.valueof(cp.Account__r.CustProfId__c);
        return customerId;
    }
    
    private static Long getBusinessCustomerId (String cpLinkId){ 
        return getMasterCustomerId(cpLinkId);
    }    
    
    private static Long getIndividualCustomerId (String cpLinkId){
        System.debug(' ... dyy ... getIndividualCustomerId.profileLinkId = ' + cpLinkId); 
        Long customerId = 0L;
        if(cpLinkId==null) return customerId;
        Credit_Profile__c cp = CreditProfileCalloutUpdateHelper.getCreditProfile(cpLinkId);
        if(cp!=null &&cp.Consumer_profile_ID__c!=null) 
            customerId = Long.valueof(cp.Consumer_profile_ID__c);
        return customerId;
    }
    
    /*---------------------------------------------*/
    
    public static void triggerCalloutProxyToAddPartner(   
        String corporatePartnerId
        ,String individualPartnerId
        ,String creditProfileLinkId){
            
            System.debug(' ... dyy ... triggerCalloutProxyToAddPartner ... ');   
            System.debug(' ... dyy ... corporatePartnerId = ' + corporatePartnerId);   
            System.debug(' ... dyy ... individualPartnerId = ' + individualPartnerId); 
            System.debug(' ... dyy ... creditProfileLinkId = ' + creditProfileLinkId); 
            
            if(corporatePartnerId!=null) addBusinessPartner(corporatePartnerId, creditProfileLinkId);
            else if(individualPartnerId!=null) addIndividualPartner(individualPartnerId, creditProfileLinkId);
            
        }
    
    public static void triggerCalloutProxyToDeletePartner(   
        String corporatePartnerId
        ,String individualPartnerId
        ,String creditProfileLinkId){
            System.debug(' ... dyy ... triggerCalloutProxyToDeletePartner ... ');  
            if(corporatePartnerId!=null){
                removePartner(getMasterCustomerId(corporatePartnerId), getBusinessCustomerId(creditProfileLinkId));
            }else if (individualPartnerId!=null){
                removePartner(getMasterCustomerId(individualPartnerId), getIndividualCustomerId(creditProfileLinkId));
            } 
        }
    
    
    @future(callout=true)
    public static void addBusinessPartner(   
        String corporatePartnerId
        ,String creditProfileLinkId){            
            System.debug(' ... dyy ... addBusinessPartner ... ');     
            
            Long masterPartnerBusinessCustomerId = getMasterCustomerId(corporatePartnerId);
            Long partnerBusinessCustomerId = getBusinessCustomerId(creditProfileLinkId);
            
            CreditProfileServiceTypes.CreditAuditInfo 
                auditInfo = CreditWebServiceClientUtil.getProfileAuditInfo();               
            
            String creditID = String.valueof(masterPartnerBusinessCustomerId) 
                + ' '+ String.valueof(partnerBusinessCustomerId);
            String creditObject = 'Credit_Profile__c';
            String operationOrMethod = 'CreditProfileCalloutUpdate.addBusinessPartner';
            String payload = 'PAYLOAD: masterPartnerBusinessCustomerId = ' + masterPartnerBusinessCustomerId
                + ', ' + 'partnerBusinessCustomerId = ' + partnerBusinessCustomerId
                + ', ' + auditInfo
                ;
            String result;
            Integer retryCount;
            String retryStatus;
            String status;
            String createdById = auditInfo.userId;
            DateTime createdDate = Datetime.now();
            
            try {            
                
                CreditProfileService.BusinessCreditProfileMgmtService_v1_0_SOAP 
                    servicePort = new CreditProfileService.BusinessCreditProfileMgmtService_v1_0_SOAP();                
                CreditProfileCalloutUpdateHelper.prepareCallout(servicePort); 
                
                servicePort.addBusinessPartner(masterPartnerBusinessCustomerId
                                               ,partnerBusinessCustomerId
                                               ,auditInfo);    
                
                status = 'Success';
                /*
CreditWebServiceClientUtil.writeToCreditRequestHistory(
creditID
, creditObject
, operationOrMethod
, payload
, result
, retryCount
, retryStatus
, status
, createdById
, createdDate);*/
                
                if ( Test.isRunningTest() ) throw new CalloutException();
            }catch(CalloutException e){
                
                
                CreditWebServiceClientUtil.logCalloutErrors(
                    creditID
                    , creditObject
                    , operationOrMethod
                    , payload
                    , result
                    , retryCount
                    , retryStatus
                    , status
                    , createdById
                    , createdDate
                    , e);
            }  
            
        }
    
    
    @future(callout=true)
    public static void addIndividualPartner(   
        String individualPartnerId
        ,String creditProfileLinkId){    
            
            Long masterPartnerBusinessCustomerId = getMasterCustomerId(individualPartnerId);
            Long partnerBusinessCustomerId = getIndividualCustomerId(creditProfileLinkId);
            
            System.debug(' ... dyy ... addIndividualPartner ... '); 
            System.debug(' ... dyy ... individualPartnerId ... ' + individualPartnerId); 
            System.debug(' ... dyy ... creditProfileLinkId ... ' + creditProfileLinkId); 
            System.debug(' ... dyy ... masterPartnerBusinessCustomerId ... ' + masterPartnerBusinessCustomerId);   
            System.debug(' ... dyy ... partnerBusinessCustomerId ... ' + partnerBusinessCustomerId);    
            
            CreditProfileServiceTypes.CreditAuditInfo 
                auditInfo = CreditWebServiceClientUtil.getProfileAuditInfo();              
            
            String creditID = String.valueof(masterPartnerBusinessCustomerId) 
                + ' '+ String.valueof(partnerBusinessCustomerId);
            String creditObject = 'Credit_Profile__c';
            String operationOrMethod = 'CreditProfileCalloutUpdate.addIndividualPartner';
            String payload = 'PAYLOAD: masterPartnerBusinessCustomerId = ' + masterPartnerBusinessCustomerId
                + ', ' + 'partnerBusinessCustomerId = ' + partnerBusinessCustomerId
                + ', ' + auditInfo
                ;
            String result;
            Integer retryCount;
            String retryStatus;
            String status;
            String createdById = auditInfo.userId;
            DateTime createdDate = Datetime.now();
            
            try {            
                
                CreditProfileService.BusinessCreditProfileMgmtService_v1_0_SOAP 
                    servicePort = new CreditProfileService.BusinessCreditProfileMgmtService_v1_0_SOAP();                
                CreditProfileCalloutUpdateHelper.prepareCallout(servicePort); 
                
                servicePort.addIndividualPartner(masterPartnerBusinessCustomerId
                                                 ,partnerBusinessCustomerId
                                                 ,auditInfo); 
                
                status = 'Success';
                
                CreditWebServiceClientUtil.writeToCreditRequestHistory(
                    creditID
                    , creditObject
                    , operationOrMethod
                    , payload
                    , result
                    , retryCount
                    , retryStatus
                    , status
                    , createdById
                    , createdDate);
                
                
                if ( Test.isRunningTest() ) throw new CalloutException();
            }catch(CalloutException e){
                
                
                CreditWebServiceClientUtil.logCalloutErrors(
                    creditID
                    , creditObject
                    , operationOrMethod
                    , payload
                    , result
                    , retryCount
                    , retryStatus
                    , status
                    , createdById
                    , createdDate
                    , e);
            }  
            
        }
    
    
    @future(callout=true)
    public static void removePartner(   
        Long masterPartnerBusinessCustomerId
        ,Long partnerBusinessCustomerId ){     
            
            System.debug(' ... dyy ... removePartner ... ');    
            System.debug(' ... dyy ... masterPartnerBusinessCustomerId ... ' + masterPartnerBusinessCustomerId);   
            System.debug(' ... dyy ... partnerBusinessCustomerId ... ' + partnerBusinessCustomerId);  
            
            CreditProfileServiceTypes.CreditAuditInfo 
                auditInfo = CreditWebServiceClientUtil.getProfileAuditInfo();
            
            String creditID = String.valueof(partnerBusinessCustomerId);
            String creditObject = 'Credit_Profile__c';
            String operationOrMethod = 'CreditProfileCalloutUpdate.removePartner';
            String payload = 'PAYLOAD: masterPartnerBusinessCustomerId = ' + masterPartnerBusinessCustomerId
                + ', ' + 'partnerBusinessCustomerId = ' + partnerBusinessCustomerId
                + ', ' + auditInfo
                ; 
            String result;
            Integer retryCount;
            String retryStatus;
            String status;
            String createdById = auditInfo.userId;
            DateTime createdDate = Datetime.now();
            
            try {            
                
                CreditProfileService.BusinessCreditProfileMgmtService_v1_0_SOAP 
                    servicePort = new CreditProfileService.BusinessCreditProfileMgmtService_v1_0_SOAP();                
                CreditProfileCalloutUpdateHelper.prepareCallout(servicePort);  
                
                servicePort.removePartner(masterPartnerBusinessCustomerId
                                          ,partnerBusinessCustomerId
                                          ,auditInfo); 
                
                status = 'Success';
                
                CreditWebServiceClientUtil.writeToCreditRequestHistory(
                    creditID
                    , creditObject
                    , operationOrMethod
                    , payload
                    , result
                    , retryCount
                    , retryStatus
                    , status
                    , createdById
                    , createdDate);
                
                if ( Test.isRunningTest() ) throw new CalloutException();
            }catch(CalloutException e){
                
                
                CreditWebServiceClientUtil.logCalloutErrors(
                    creditID
                    , creditObject
                    , operationOrMethod
                    , payload
                    , result
                    , retryCount
                    , retryStatus
                    , status
                    , createdById
                    , createdDate
                    , e);
            }  
            
        }
    
    
    
    webservice static void updateIndivdualCreditProfile(
        Long individualCustomerId
        ,Credit_Profile__c creditProfil) {  
            
            CreditProfileServiceTypes.IndividualCreditProfile 
                individualCreditProfile = new CreditProfileServiceTypes.IndividualCreditProfile();
            CreditProfileCalloutUpdateHelper.populateIndividualCreditProfile(individualCreditProfile, creditProfil);
            
            CreditProfileServiceTypes.CreditAuditInfo auditInfo
                = CreditWebServiceClientUtil.getProfileAuditInfo(); 
            
            String creditID = String.valueof(individualCustomerId);
            String creditObject = 'Credit_Profile__c';
            String operationOrMethod = 'CreditProfileCalloutUpdate.updateIndivdualCreditProfile';
            String payload = 'PAYLOAD: individualCustomerId = ' + individualCustomerId
                + ', ' + 'individualCreditProfile = ' + individualCreditProfile
                + ', ' + auditInfo
                ;  
            String result;
            Integer retryCount;
            String retryStatus;
            String status;
            String createdById = auditInfo.userId;
            DateTime createdDate = Datetime.now();
            
            try {            
                
                CreditProfileService.BusinessCreditProfileMgmtService_v1_0_SOAP 
                    servicePort = new CreditProfileService.BusinessCreditProfileMgmtService_v1_0_SOAP();                
                CreditProfileCalloutUpdateHelper.prepareCallout(servicePort);   
                
                System.debug(' ... dyy ... calling to updateIndivdualCreditProfile ...'); 
                System.debug(' ... dyy ... individualCustomerId ... ' + individualCustomerId); 
                
                servicePort.updateIndivdualCreditProfile(
                    individualCustomerId
                    ,individualCreditProfile
                    ,auditInfo);  
                
                status = 'Success';
                /*
CreditWebServiceClientUtil.writeToCreditRequestHistory(
creditID
, creditObject
, operationOrMethod
, payload
, result
, retryCount
, retryStatus
, status
, createdById
, createdDate);*/
                
                if ( Test.isRunningTest() ) throw new CalloutException();
            }catch(CalloutException e){
                
                
                CreditWebServiceClientUtil.logCalloutErrors(
                    creditID
                    , creditObject
                    , operationOrMethod
                    , payload
                    , result
                    , retryCount
                    , retryStatus
                    , status
                    , createdById
                    , createdDate
                    , e);
                
            }                   
            
        }
    
    /*------------------------------------------------------------------------------*/
    
    webservice static String attachCreditReport(
        Long businessCustomerId
        , Document document
        , String documentTypeCd
        , String documentSourceCd
    ){
        
        String creditReportId;
        
        CreditProfileServiceTypes.BusinessCreditReportRequest businessCreditReportRequest
            = new CreditProfileServiceTypes.BusinessCreditReportRequest(); 
        
        CreditProfileCalloutUpdateHelper.populateBusinessCreditReportRequest(
            businessCreditReportRequest
            , document
            , documentTypeCd
            , documentSourceCd);
        
        CreditProfileServiceTypes.CreditAuditInfo auditInfo
            = CreditWebServiceClientUtil.getProfileAuditInfo();
        
        String payload;
        
        try {
            
            CreditProfileService.BusinessCreditProfileMgmtService_v1_0_SOAP 
                servicePort = new CreditProfileService.BusinessCreditProfileMgmtService_v1_0_SOAP();                
            CreditProfileCalloutUpdateHelper.prepareCallout(servicePort);   
            
            payload = 'PAYLOAD: businessCustomerId = ' + businessCustomerId
                + ', ' + auditInfo
                ;
            
            creditReportId 
                = servicePort.AttachCreditReport(
                    businessCustomerId
                    , businessCreditReportRequest
                    , auditInfo);
            
            System.debug(' ... dyy ... creditReportId = ' + creditReportId);
            
            if ( Test.isRunningTest() ) throw new CalloutException();
        } catch(CalloutException e){
            CreditWebServiceClientUtil.writeToWebserviceIntegrationErrorLogWithPayload(
                creditReportId
                , 'Credit_Related_Info__c'
                , 'CreditAssessmentCallout.attachCreditReport'
                , 'BusinessCreditAssessmentService_v1_0_SOAP.attachCreditReport'
                , payload
                , e);
            
            System.debug(' ... dyy ... e.getMessage = ' + e.getMessage()); 
            System.debug(' ... dyy ... e.getCause = ' + e.getCause()); 
            System.debug(' ... dyy ... e.getStackTraceString = ' + e.getStackTraceString()); 
        } 
        return creditReportId;
    }
    
    webservice static Document getCreditReport(Long creditReportId){
        
        Document doc = new Document();
        
        String payload;              
        
        try {
            
            CreditProfileService.BusinessCreditProfileMgmtService_v1_0_SOAP 
                servicePort = new CreditProfileService.BusinessCreditProfileMgmtService_v1_0_SOAP();                
            CreditProfileCalloutUpdateHelper.prepareCallout(servicePort);   
            
            payload = 'PAYLOAD: creditReportId = ' + creditReportId;
            
            CreditProfileServiceTypes.BusinessCreditReportFullDetails 
                response
                = servicePort.getCreditReport(creditReportId);   
            
            String documentId = response.documentId;
            String documentName = response.documentName;
            String creditReportContent = response.creditReportContent;
            doc.Name = documentName;
            doc.Body = EncodingUtil.base64Decode(creditReportContent);   
            System.debug(' ... dyy ... documentId = ' + documentId);
            System.debug(' ... dyy ... documentName = ' + documentName);
            
            if ( Test.isRunningTest() ) throw new CalloutException();
        } catch(CalloutException e) {
            CreditWebServiceClientUtil.writeToWebserviceIntegrationErrorLogWithPayload(
                String.valueof(creditReportId)
                , 'Credit_Related_Info__c'
                , 'CreditAssessmentCallout.getCreditReport'
                , 'BusinessCreditAssessmentService_v1_0_SOAP.getCreditReport'
                , payload
                , e);            
            System.debug(' ... dyy ... e.getMessage = ' + e.getMessage()); 
            System.debug(' ... dyy ... e.getCause = ' + e.getCause()); 
            System.debug(' ... dyy ... e.getStackTraceString = ' + e.getStackTraceString()); 
        } 
        return doc;
    }
    
      
    @future(callout=true)
    webservice static void voidCreditReport(String creditReportId){
        
        CreditProfileServiceTypes.CreditAuditInfo
            auditInfo = CreditWebServiceClientUtil.getProfileAuditInfo();
        
        String payload;              
        
        try {
            CreditProfileService.BusinessCreditProfileMgmtService_v1_0_SOAP 
                servicePort = new CreditProfileService.BusinessCreditProfileMgmtService_v1_0_SOAP();                
            CreditProfileCalloutUpdateHelper.prepareCallout(servicePort); 
            
            if ( !Test.isRunningTest()
                &&CreditWebServiceClientUtil.isLong(creditReportId)) 
                servicePort.voidCreditReport(Long.valueof(creditReportId),auditInfo);
            
            payload = 'PAYLOAD: creditReportId = ' + creditReportId
                + ', ' + auditInfo
                ;
            
            if ( Test.isRunningTest() ) throw new CalloutException();
        } catch(CalloutException e) {
            CreditWebServiceClientUtil.writeToWebserviceIntegrationErrorLogWithPayload(
                String.valueof(creditReportId)
                , 'Credit_Related_Info__c'
                , 'CreditAssessmentCallout.removeCreditReport'
                , 'BusinessCreditAssessmentService_v1_0_SOAP.getCreditReport'
                , payload
                , e);            
            System.debug(' ... dyy ... e.getMessage = ' + e.getMessage()); 
            System.debug(' ... dyy ... e.getCause = ' + e.getCause()); 
            System.debug(' ... dyy ... e.getStackTraceString = ' + e.getStackTraceString()); 
        } 
    }  
    
}