@isTest
private class OCOM_PricingRulesImplementationTest {
	
 static testMethod void PriceLineItemsTest() {
        OCOM_PricingRulesImplementationTest instance = new OCOM_PricingRulesImplementationTest();
        try {
            instance.executeTest();
            
        } catch (Exception ex) {
            System.Debug('ERROR: ' + ex.getMessage());
        }
    }


     public void executeTest() {

        String nsPrefix = 'vlocity_cmt__';
        
        List<PriceBookEntry> pricebookEntries = new List <PriceBookEntry>();

        // create product
        Product2 testProduct = new Product2(Name = 'Test Product');
        insert testProduct;
        
        Product2 testProduct2 = new Product2(Name = 'Test Product2');
        insert testProduct2;
        
         vlocity_cmt__ProductChildItem__c pci = new vlocity_cmt__ProductChildItem__c(name='overRidePci',OCOMOverrideType__c = 'Override Price',
                                                            vlocity_cmt__ParentProductId__c = testProduct.id ,
                                                            vlocity_cmt__ChildProductId__c = testProduct2.id ,
                                                            vlocity_cmt__Quantity__c = 2, vlocity_cmt__ChildLineNumber__c='1' );
        insert pci;                                                    
        
        Id pricebookId = Test.getStandardPricebookId();
        
        //Create a Pricebook
        Pricebook2 testPricebook = new Pricebook2(Name = 'TestPricebook', IsActive = true, vlocity_cmt__IsDefault__c = true);
        insert testPricebook;
        
        PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = pricebookId,
            Product2Id = testProduct.Id, UnitPrice = 10, IsActive = true, UseStandardPrice = false);
        insert standardPrice;
        
        PricebookEntry testPricebookEntryPlain = new PricebookEntry(Pricebook2Id = testPricebook.Id,
            Product2Id = testProduct.Id, UnitPrice = 10, IsActive = true, UseStandardPrice = false);
        insert testPricebookEntryPlain;
        pricebookEntries.add(testPricebookEntryPlain);
        
        PricebookEntry standardPrice2 = new PricebookEntry(Pricebook2Id = pricebookId,
            Product2Id = testProduct2.Id, UnitPrice = 20, IsActive = true, UseStandardPrice = false);
        insert standardPrice2;
        
        PricebookEntry testPricebookEntryPlain2 = new PricebookEntry(Pricebook2Id = testPricebook.Id,
            Product2Id = testProduct2.Id, UnitPrice = 20, IsActive = true, UseStandardPrice = false);
        insert testPricebookEntryPlain2;
        pricebookEntries.add(testPricebookEntryPlain2);
        
        
        Account testAccount = new Account(  Name = 'TestAccount');
        insert testAccount;
        
        order testOrder = new order(Name ='testOrder',
                                                AccountId = testAccount.Id,
                                                Status = 'Not Submitted',
                                                EffectiveDate = Date.today(),
                                                Pricebook2Id = testPricebook.Id);                                                                                                
        insert testOrder;
                
        // Entity filter
        vlocity_cmt__EntityFilter__c eFilter = new vlocity_cmt__EntityFilter__c(  vlocity_cmt__FilterOnObjectName__c = 'OrderItem',
                                                            vlocity_cmt__IsActive__c = true,
                                                        vlocity_cmt__Type__c = 'Qualification');
        insert eFilter;
        
        // Filter line items whose UnitPrice = 20.00
        vlocity_cmt__EntityFilterCondition__c  eFilterCondition = new vlocity_cmt__EntityFilterCondition__c (vlocity_cmt__EntityFilterId__c = eFilter.Id,
                                                                                    vlocity_cmt__FieldName__c = 'Quantity',
                                                                                    vlocity_cmt__FieldType__c = 'double',
                                                                                    vlocity_cmt__Index__c = 0.0,
                                                                                    vlocity_cmt__Operator__c = '>',
                                                                                    vlocity_cmt__Value__c = '0.00',
                                                                                    vlocity_cmt__Type__c ='Field');        
        insert eFilterCondition;
                
        
        // Query for Class Based record type id
        String objType =  'vlocity_cmt__'+ 'CalculationProcedure__c';
        List<RecordType> recordTypes = [SELECT Id,SobjectType,Name FROM RecordType WHERE Name = 'Class Based' AND SobjectType = :objType LIMIT 1];
        ID recordTypeId;
        if (recordTypes.size() > 0) {
            recordTypeId = recordTypes[0].Id;
        }

        // Calculation Procedure
        vlocity_cmt__CalculationProcedure__c calcProc;
        if (recordTypeId != null) {
            calcProc = new vlocity_cmt__CalculationProcedure__c(Name='OCOM_SetOverRidePricesToZero_test', RecordTypeId=recordTypeId,    vlocity_cmt__ActionClassName__c='OCOM_SetOverRidePricesToZero');
        }
        else {
            calcProc = new vlocity_cmt__CalculationProcedure__c(Name='OCOM_SetOverRidePricesToZero_test',   vlocity_cmt__ActionClassName__c='OCOM_SetOverRidePricesToZero');
        }
        insert calcProc;
       
        vlocity_cmt__CalculationProcedureVersion__c calcProcVers = new vlocity_cmt__CalculationProcedureVersion__c(Name='Version1',     vlocity_cmt__VersionNumber__c=100, vlocity_cmt__IsEnabled__c=false, vlocity_cmt__CalculationProcedureId__c=calcProc.Id);
        insert calcProcVers;

        // Rule       
        vlocity_cmt__Rule__c rule = new vlocity_cmt__Rule__c( Name = 'OCOM Text Calculation Procedure',
                                    vlocity_cmt__IsActive__c = true,
                                    vlocity_cmt__ObjectName__c = 'OrderItem',
                                    vlocity_cmt__Type__c = 'Pricing');
                                    
        insert rule;
        
        // Rule Filter
        vlocity_cmt__RuleFilter__c ruleFilter = new vlocity_cmt__RuleFilter__c (vlocity_cmt__EntityFilterId__c =  eFilter.Id,
                                                        vlocity_cmt__Index__c = 0.0,
                                                        vlocity_cmt__RuleId__c = rule.Id);
                                                        
        insert ruleFilter;
        
        // Rule Action
        vlocity_cmt__RuleAction__c ruleAction = new vlocity_cmt__RuleAction__c (vlocity_cmt__Index__c = 0.0,
                                                        vlocity_cmt__RuleId__c = rule.Id,
                                                        vlocity_cmt__CalculationProcedureId__c = calcProc.Id);
        insert ruleAction;
        
                       
        addProducts(testOrder.Id, testPricebookEntryPlain.Id);
        addProducts(testOrder.Id, testPricebookEntryPlain2.Id);

       

        // When this query is changed the one in LineItemManagementController needs to change.
        List<SObject> lineItems = Database.query('Select Id,' + 
                                'vlocity_cmt__LineNumber__c,' +
                                'PriceBookEntry.Product2.Name,' +
                                'PriceBookEntry.Product2.ProductCode,' +
                                'PriceBookEntry.Product2.Id,' +
                                'ListPrice,' +
                                'UnitPrice,' +
                                'Quantity,' +
                                'vlocity_cmt__RootItemId__c,' +
                                'vlocity_cmt__OneTimeCharge__c, ' +
                                'vlocity_cmt__OneTimeCalculatedPrice__c,' +
                                'vlocity_cmt__OneTimeManualDiscount__c,' +
                                'vlocity_cmt__OneTimeTotal__c,' +
                                
                                'vlocity_cmt__RecurringCharge__c,' +
                                'vlocity_cmt__RecurringCalculatedPrice__c,' +
                                'vlocity_cmt__RecurringManualDiscount__c,' +
                                'vlocity_cmt__RecurringDiscountPrice__c,' +
                                'vlocity_cmt__RecurringTotal__c,' +

                                'vlocity_cmt__ProvisioningStatus__c,' +
                                'vlocity_cmt__ServiceAccountId__r.Name,' +
                                'vlocity_cmt__BillingAccountId__r.Name' +
                                ' FROM  OrderItem WHERE OrderId = \'' + testOrder.Id + '\'');

        //System.debug('OCOM_SetOverRidePricesToZero_Test: after add products, lineItems ' + lineItems);

        string rootItemId = '';
        integer i =0;

       Map<Id, Sobject> sobjectIdToSobject = new Map<Id,Sobject>();
       Set<Id> qualifiedObjectIds = new Set<Id>();

        for(orderItem oi: (list<orderItem>)lineItems){
            
            system.debug('Root Item Id' + i+ '_____'+oi.vlocity_cmt__RootItemId__c );
            if(string.Valueof(oi.vlocity_cmt__LineNumber__c).contains('.')){
                oi.vlocity_cmt__RootItemId__c = rootItemId;
            }
            else{
                rootItemId = oi.id;
            }

            sobjectIdToSobject.put(oi.id,oi);
            qualifiedObjectIds.add(oi.id);

            i++;
        }
    
        update lineItems;
        

        //List<Id> ruleIds = new List<Id>();
        //ruleIds.add(rule.Id);

        //Map<String, Object> input = new Map<String, Object>();
        //Map<String, Object> output = new Map<String, Object>();
        //input.put('ruleIdentifierType', 'Id');
        //input.put('ruleIdentifiersList', ruleIds);
        //vlocity_cmt.FlowStaticMap.flowMap.put('parent',testOrder);
        //vlocity_cmt.FlowStaticMap.flowMap.put('itemList',lineItems); 

        ////FlowStaticMap.flowMap.put('pricebookIdToItemWrapper', pricebookIdToItemWrapper);
        //input.put('isTriggeredByFlow', false);
        //input.put('flowMap', vlocity_cmt.FlowStaticMap.flowMap);
        vlocity_cmt.RuleSupport ruleSupport = new vlocity_cmt.RuleSupport();

        OCOM_PricingRulesImplementation pricingImpl = new OCOM_PricingRulesImplementation();
        OCOM_PricingRulesHelper.executionType = 'OCOM_SetOverRidePricesToZeroOnly';


        Test.startTest();
            pricingImpl.priceLineItems(testOrder, lineItems);

        Test.stopTest();  

  
    }
    
    
    private static void  addProducts(ID objectId, ID priceBookEntryId){
      
        if(priceBookEntryId !=null)
        {
            vlocity_cmt.ProductLineItemActionParam productLineItemActionParam = new  vlocity_cmt.ProductLineItemActionParam();
            productLineItemActionParam.action = 'Add';
            productLineItemActionParam.priceBookEntryId = priceBookEntryId;
            productLineItemActionParam.parentId = objectId;
            vlocity_cmt.productLineItemEventHandler productLineItemEventHandler = new vlocity_cmt.ProductLineItemEventHandler();
            //vlocity_cmt.productLineItemEventHandler.handleLineItemAction(productLineItemActionParam);
            String message = productLineItemEventHandler.handleLIAction(productLineItemActionParam);
        }

    }

    public class ProductLineItemActionParam 
    {
		    public Id priceBookEntryId {get;set;}
		    public Id lineItemId {get;set;} // XLI Id to be deleted . If action is ADD ,based on addMode param, this will be the parent,sibling or new ROOT LI
		    public String action {get;set;} 
		    public Id parentId {get;set;}  // ORDER ID etc
		     // add flag ( addMode  ) ,then set the   line # based on the addMode
		     // addMode : asSibling,asChild, asRoot . Default is asSibling
		     // asRoot will have default  linenumber of 0001
		    public Map<String,Object> itemInfoMap{get;set;}   
		    public String JSONNode {get; set;}
		    public Object Filter {get; set;}
		    public String Query {get; set;} 
		    public SObject parentItem {get;set;}
		    public List<SObject> lineItems {get; set;}
		    public SObjectType lineItemType {get; set;}
		    public SObjectType parentObjectType {get; set;} 
		    public String productDefinition {get; set;} 
		    public void ProductLineItemActionParam() {}
    }
	
}