@isTest
private class SMB_AmendCancelOrderController_Test {
    
    static testMethod void amend() {
        setBasic();
        Opportunity testOpp   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        map<string,schema.recordtypeinfo> recordTypeInfoMap=webservice_integration_error_log__c.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        webservice_integration_error_log__c successLog=new webservice_integration_error_log__c(recordtypeid=recordTypeInfoMap.get('Success').getrecordtypeid(),object_name__c='Opportunity',
                                                                                        webservice_name__c='Cancel Order',apex_class_and_method__c='SMB_AmendCancelOrderController.cancelOrder',
                                                                                        external_system_name__c='CPQ',sfdc_record_id__c=testOpp.id);
        webservice_integration_error_log__c failureLog=new webservice_integration_error_log__c(recordtypeid=recordTypeInfoMap.get('Failure').getrecordtypeid(),object_name__c='Opportunity',
                                                                                        webservice_name__c='Cancel Order',apex_class_and_method__c='SMB_AmendCancelOrderController.cancelOrder',
                                                                                        external_system_name__c='CPQ',sfdc_record_id__c=testOpp.id);
        insert successLog;
        insert failureLog;
        testOpp.Type = 'All Other Orders';
        insert testOpp;
        smb_test_utility.createOrderRequest(Null,testOpp.Id,true);
        PageReference pageref = Page.SMB_AmendCancelOrder;
        Test.setCurrentPage(pageref);        
        pageref.getparameters().put('id', testOpp.Id);
        ApexPages.StandardController sc_1 = new ApexPages.standardController(testOpp);        
        Test.startTest();
        SMB_AmendCancelOrderController amendCancelOrder = new SMB_AmendCancelOrderController(sc_1);
        amendCancelOrder.DoOk();
        amendCancelOrder.objOpportunity.Change_Type__c = 'Test';
        amendCancelOrder.objOpportunity.Reason_Code__c = 'Test';
        amendCancelOrder.objOpportunity.Notes__c = 'Test';
        amendCancelOrder.objOpportunity.StageName = 'Contract Sent';
        amendCancelOrder.previousOpportunityStage = amendCancelOrder.objOpportunity.StageName;
        amendCancelOrder.objOpportunity.stage_update_time__c=system.now();
        amendCancelOrder.DoOk();
        amendCancelOrder.objOpportunity.StageName = 'Order Request New';
        amendCancelOrder.objOpportunity.Type = 'New Move Order';
        amendCancelOrder.objOpportunity.stage_update_time__c=system.now().addminutes(1);
        amendCancelOrder.DoOk();
        amendCancelOrder.objOpportunity.Type = 'All Other Orders';
        amendCancelOrder.objOpportunity.stage_update_time__c=system.now().addminutes(2);
        amendCancelOrder.DoOk();
        amendCancelOrder.objOpportunity.StageName = 'Order Request';
        amendCancelOrder.objOpportunity.stage_update_time__c=system.now().addminutes(3);
        amendCancelOrder.DoOk();
        SMB_AmendCancelOrderController.vfrPrepareOrderCancellationRequest(testOpp.id,'Test Change Type','Reason Code','Notes');
        SMB_AmendCancelOrderController.logCPQCancellation(testOpp.id,'"status":200','"status":200');
        SMB_AmendCancelOrderController.logCPQCancellation(testOpp.id,'"status":400','"status":400');
        SMB_AmendCancelOrderController.getCPQQuoteCancellationHistory(testOpp.id);
        Test.stopTest();
    }
    
    static testMethod void amendCancelOrder() {
        setBasic();
        Opportunity testOpp   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp.Type = 'All Other Orders';
        insert testOpp;
        Order_Request__c orderReq = smb_test_utility.createOrderRequest(Null,testOpp.Id,False); 
        insert orderReq;
        
        PageReference pageref = Page.SMB_AmendCancelOrder;
        Test.setCurrentPage(pageref);        
        pageref.getparameters().put('id', testOpp.Id);
        ApexPages.StandardController sc_1 = new ApexPages.standardController(testOpp);
        Test.startTest();
        SMB_AmendCancelOrderController amendCancelOrder = new SMB_AmendCancelOrderController(sc_1);
        amendCancelOrder.objOpportunity.Change_Type__c = 'Cancel';
        amendCancelOrder.objOpportunity.Reason_Code__c = 'Customer-initiated';
        amendCancelOrder.objOpportunity.Notes__c = 'Test';
        amendCancelOrder.objOpportunity.StageName = 'Contract Sent';
        System.debug('amendCancelOrder.objOpportunity.StageName: ' + amendCancelOrder.objOpportunity.StageName);        
        amendCancelOrder.previousOpportunityStage = amendCancelOrder.objOpportunity.StageName;
        amendCancelOrder.objOpportunity.stage_update_time__c=system.now().addminutes(1);
        amendCancelOrder.DoOk();
        amendCancelOrder.objOpportunity.StageName = 'Order Request';
        amendCancelOrder.objOpportunity.stage_update_time__c=system.now().addminutes(2);
        amendCancelOrder.DoOk();
        amendCancelOrder.orderReSubmit();
        
        Test.stopTest();
    }
    @istest
    static void amendCancelCPQOrder() {
        setBasic();
        Opportunity testOpp   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp.Type='New Provide/Upgrade Order';
        insert testOpp;
        Order_Request__c orderReq = smb_test_utility.createOrderRequest(Null,testOpp.Id,False); 
        insert orderReq;
        
        PageReference pageref = Page.SMB_AmendCancelOrder;
        Test.setCurrentPage(pageref);        
        pageref.getparameters().put('id', testOpp.Id);
        ApexPages.StandardController sc_1 = new ApexPages.standardController(testOpp);
        Test.startTest();
        SMB_AmendCancelOrderController amendCancelOrder = new SMB_AmendCancelOrderController(sc_1);
        amendCancelOrder.objOpportunity.Change_Type__c = 'Cancel';
        amendCancelOrder.objOpportunity.Reason_Code__c = 'Customer-initiated';
        amendCancelOrder.objOpportunity.Notes__c = 'Test';
        amendCancelOrder.objOpportunity.StageName = 'Contract Sent';
        System.debug('amendCancelOrder.objOpportunity.StageName: ' + amendCancelOrder.objOpportunity.StageName);        
        amendCancelOrder.previousOpportunityStage = amendCancelOrder.objOpportunity.StageName;
        amendCancelOrder.objOpportunity.stage_update_time__c=system.now().addminutes(1);
        amendCancelOrder.DoOk();
        amendCancelOrder.objOpportunity.StageName = 'Order Request';
        amendCancelOrder.objOpportunity.stage_update_time__c=system.now().addminutes(2);
        amendCancelOrder.DoOk();
        amendCancelOrder.orderReSubmit();
        
        Test.stopTest();
    }
    
    static testMethod void testUpdateContractStatus(){
        setBasic();
        TestDataHelper.testContractCreation();
        
        Opportunity testOpp1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp1.Type = 'All Other Orders';
        insert testOpp1;
        map<string,schema.recordtypeinfo> recordTypeInfoMap=webservice_integration_error_log__c.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        webservice_integration_error_log__c successLog=new webservice_integration_error_log__c(recordtypeid=recordTypeInfoMap.get('Success').getrecordtypeid(),object_name__c='Opportunity',
                                                                                        webservice_name__c='Cancel Order',apex_class_and_method__c='SMB_AmendCancelOrderController.cancelOrder',
                                                                                        external_system_name__c='CPQ',sfdc_record_id__c=testOpp1.id);
        webservice_integration_error_log__c failureLog=new webservice_integration_error_log__c(recordtypeid=recordTypeInfoMap.get('Failure').getrecordtypeid(),object_name__c='Opportunity',
                                                                                        webservice_name__c='Cancel Order',apex_class_and_method__c='SMB_AmendCancelOrderController.cancelOrder',
                                                                                        external_system_name__c='CPQ',sfdc_record_id__c=testOpp1.id);
        insert successLog;
        insert failureLog;
        smb_test_utility.createOrderRequest(Null,testOpp1.Id,true);
        PageReference pageref = Page.SMB_AmendCancelOrder;
        Test.setCurrentPage(pageref);        
        pageref.getparameters().put('id', testOpp1.Id);
        ApexPages.StandardController sc_1 = new ApexPages.standardController(testOpp1);        
        Test.startTest();
        SMB_AmendCancelOrderController test1 = new SMB_AmendCancelOrderController(sc_1);    
        
        String s1 = 'Fax Contract Validation Required';
        
        test1.UpdateContractStatus();
        //SMB_AmendCancelOrderController.UpdateContractStatus();
        Contract testContractObj1 = new Contract(vlocity_cmt__OpportunityId__c = testOpp1.Id,Contract_Type__c='Amendment',AccountId=TestDataHelper.testAccountObj.id,ContractTerm=12,StartDate=system.today(),CustomerSignedId=TestDataHelper.testContactObj.id,TELUS_Contract_Approver__c=userinfo.getuserid());
       insert testContractObj1;
        testContractObj1.Status = 'Fax Contract Validation Required';
        update testContractObj1;
        
        system.debug('testContractObj1'+testContractObj1);
       SMB_AmendCancelOrderController test2 = new SMB_AmendCancelOrderController(sc_1);
       test2.UpdateContractStatus();
        test2.invalidForm();
        test2.showRetryMessage();
        test2.showSystemErrorMessage();
         testOpp1.StageName = 'Trial';
         update testOpp1;
        
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('NCOrderCancellationSuccess');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type','text/xml;charset=UTF-8');
        Test.setMock(HttpCalloutMock.class, mock);
        
        test2.objOpportunity.Change_Type__c = 'Test';
        test2.objOpportunity.Reason_Code__c = 'Test';
        test2.objOpportunity.Notes__c = 'Test';
        test2.objOpportunity.StageName = 'Contract Sent';
        test2.previousOpportunityStage = test2.objOpportunity.StageName;
        test2.objOpportunity.stage_update_time__c=system.now();
 		test2.cancelOrder();
        test2.revertOrderCancellationRequest();
        test2.cancelWholeOrder();
        test2.cancelOrderTest();
      //  test2.getSalesforceInstance();
       
		test2.objOpportunity.StageName = 'Order Request New';
        test2.objOpportunity.Type = 'New Move Order';
         test2.previousOpportunityStage = test2.objOpportunity.StageName;
        test2.objOpportunity.stage_update_time__c=system.now().addminutes(1);
        test2.cancelOrder();
        
        // SMB_AmendCancelOrderController.UpdateContractStatus();
        Test.stopTest();
        
    }
    static testMethod void testAmendWithIntegration(){
        setBasic();
        TestDataHelper.testContractCreation();
        
        Opportunity testOpp1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp1.Type = 'All Other Orders';
        insert testOpp1;
        
        smb_test_utility.createOrderRequest(Null,testOpp1.Id,true);
        PageReference pageref = Page.SMB_AmendCancelOrder;
        Test.setCurrentPage(pageref);        
        pageref.getparameters().put('id', testOpp1.Id);
        ApexPages.StandardController sc_1 = new ApexPages.standardController(testOpp1);        
        
        SMB_AmendCancelOrderController test1 = new SMB_AmendCancelOrderController(sc_1);    
        
        String s1 = 'Fax Contract Validation Required';
        
        test1.UpdateContractStatus();
        //SMB_AmendCancelOrderController.UpdateContractStatus();
        Contract testContractObj1 = new Contract(vlocity_cmt__OpportunityId__c = testOpp1.Id,Contract_Type__c='Amendment',AccountId=TestDataHelper.testAccountObj.id,ContractTerm=12,StartDate=system.today(),CustomerSignedId=TestDataHelper.testContactObj.id,TELUS_Contract_Approver__c=userinfo.getuserid());
        insert testContractObj1;
        testContractObj1.Status = 'Fax Contract Validation Required';
        update testContractObj1;
        
        system.debug('testContractObj1'+testContractObj1);
        SMB_AmendCancelOrderController test2 = new SMB_AmendCancelOrderController(sc_1);
        test2.UpdateContractStatus();
        test2.invalidForm();
        test2.showRetryMessage();
        test2.showSystemErrorMessage();
        testOpp1.StageName = 'Trial';
        update testOpp1;
        Test.startTest();
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('NCOrderCancellationSuccess');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type','text/xml;charset=UTF-8');
        Test.setMock(HttpCalloutMock.class, mock);
        
        test2.objOpportunity.Change_Type__c = 'Test';
        test2.objOpportunity.Reason_Code__c = 'Test';
        test2.objOpportunity.Notes__c = 'Test';
        test2.objOpportunity.StageName = 'Contract Sent';
        test2.previousOpportunityStage = test2.objOpportunity.StageName;
        test2.objOpportunity.stage_update_time__c=system.now();
 		test2.cancelOrder();
        test2.revertOrderCancellationRequest();
        test2.cancelWholeOrder();
        test2.cancelOrderTest();
      //  test2.getSalesforceInstance();
       
		test2.objOpportunity.StageName = 'Order Request New';
        test2.objOpportunity.Type = 'New Move Order';
         test2.previousOpportunityStage = test2.objOpportunity.StageName;
        test2.objOpportunity.stage_update_time__c=system.now().addminutes(1);
        test2.cancelOrder();
        
        // SMB_AmendCancelOrderController.UpdateContractStatus();
        Test.stopTest();
        
    }
    static testMethod void testAmendWithCPQIntegration(){
        setBasic();
        TestDataHelper.testContractCreation();
        
        Opportunity testOpp1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp1.Type = 'New Provide/Upgrade Order';
        insert testOpp1;
        
        smb_test_utility.createOrderRequest(Null,testOpp1.Id,true);
        PageReference pageref = Page.SMB_AmendCancelOrder;
        Test.setCurrentPage(pageref);        
        pageref.getparameters().put('id', testOpp1.Id);
        ApexPages.StandardController sc_1 = new ApexPages.standardController(testOpp1);        
        
        SMB_AmendCancelOrderController test1 = new SMB_AmendCancelOrderController(sc_1);    
        
        String s1 = 'Fax Contract Validation Required';
        
        test1.UpdateContractStatus();
        //SMB_AmendCancelOrderController.UpdateContractStatus();
        Contract testContractObj1 = new Contract(vlocity_cmt__OpportunityId__c = testOpp1.Id,Contract_Type__c='Amendment',AccountId=TestDataHelper.testAccountObj.id,ContractTerm=12,StartDate=system.today(),CustomerSignedId=TestDataHelper.testContactObj.id,TELUS_Contract_Approver__c=userinfo.getuserid());
        insert testContractObj1;
        testContractObj1.Status = 'Fax Contract Validation Required';
        update testContractObj1;
        
        system.debug('testContractObj1'+testContractObj1);
        SMB_AmendCancelOrderController test2 = new SMB_AmendCancelOrderController(sc_1);
        test2.UpdateContractStatus();
        testOpp1.StageName = 'Trial';
        update testOpp1;
        Test.startTest();
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('NCOrderCancellationSuccess');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type','text/xml;charset=UTF-8');
        Test.setMock(HttpCalloutMock.class, mock);
        
        test2.objOpportunity.Change_Type__c = 'Test';
        test2.objOpportunity.Reason_Code__c = 'Test';
        test2.objOpportunity.Notes__c = 'Test';
        test2.objOpportunity.StageName = 'Contract Sent';
        test2.previousOpportunityStage = test2.objOpportunity.StageName;
        test2.objOpportunity.stage_update_time__c=system.now();
 		test2.cancelOrder();
        test2.revertOrderCancellationRequest();
        Test.stopTest();
        
    }
    
    static testMethod void getSalesforceInstanceTest(){
        Test.startTest();
         SMB_AmendCancelOrderController.getSalesforceInstance();
        Test.stopTest();
    }
    
    
    static void setBasic(){
        smb_test_utility.createCustomSettingData();
        SMB_AssignmentWFM_OrderTypes_v2.WorkOrder response_x = new SMB_AssignmentWFM_OrderTypes_v2.WorkOrder();
        response_x.workOrderId  = '1232';
        Test.setMock(WebServiceMock.class, new GenericWebserviceMock(response_x));  
    }
    
    
    
}