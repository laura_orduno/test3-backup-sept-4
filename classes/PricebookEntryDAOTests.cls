/**
 * Unit tests for <code>PricebookEntryDAO</code> class.
 * 
 * @author Max Rudman
 * @since 4/13/2011
 */
@isTest
private class PricebookEntryDAOTests {
	private static Product2 product;
	private static Pricebook2 pricebook;
	
	testMethod static void testLoadByProductIds() {
		setUp();
		
		PricebookEntryDAO target = new PricebookEntryDAO();
		PricebookEntry[] result = target.loadByProductIds(new Set<Id>{product.Id});
		System.assertEquals(2, result.size());
		
		result = target.loadByProductIds(new Set<Id>{product.Id}, pricebook.Id);
		System.assertEquals(1, result.size());
	}
	
	private static void setUp() {
		pricebook = [SELECT Id FROM Pricebook2 WHERE IsStandard = true];
		
		Pricebook2 pb = new Pricebook2(Name='Another');
		insert pb;
		
		product = new Product2(Name='Test');
		insert product;
		
		PricebookEntry e1 = new PricebookEntry(Product2Id=product.Id,UnitPrice=100,IsActive=true);
		e1.Pricebook2Id=pricebook.Id;
		insert e1;
		
		PricebookEntry e2 = new PricebookEntry(Product2Id=product.Id,UnitPrice=100,IsActive=true);
		e2.Pricebook2Id=pb.Id;
		insert e2;
	}
}