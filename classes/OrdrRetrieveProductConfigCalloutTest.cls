@isTest
public class OrdrRetrieveProductConfigCalloutTest {
	@isTest
    private static void testRetrieveProductConfigurationByCustomerAccount() {
        
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        // Call the method that invokes a callout
        TpInventoryProductConfigV3.Product response = OrdrRetrieveProductConfigurationCallout.retrieveProductConfigurationByCustomerAccount('10293');
        // Verify that a fake result is returned
        System.assertEquals('9144101066713938850', response.ComponentProduct[0].Id);
        
    }
    
	@isTest
    private static void testRetrieveProductConfigurationByServiceAccount() {
        
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        // Call the method that invokes a callout
        TpInventoryProductConfigV3.Product response = OrdrRetrieveProductConfigurationCallout.retrieveProductConfigurationByServiceAccount('10293', '38485');
        // Verify that a fake result is returned
        System.assertEquals('9144101066713938850', response.ComponentProduct[0].Id);
        
    }

	@isTest
    private static void testRetrieveProductConfigurationByProductInstance() {
        
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        // Call the method that invokes a callout
        TpInventoryProductConfigV3.Product response = OrdrRetrieveProductConfigurationCallout.retrieveProductConfigurationByProductInstance('843738');
        // Verify that a fake result is returned
        System.assertEquals('9144101066713938850', response.ComponentProduct[0].Id);
        
    }    
}