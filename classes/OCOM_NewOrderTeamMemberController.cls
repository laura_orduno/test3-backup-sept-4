public with sharing class OCOM_NewOrderTeamMemberController {
    private final Cloud_Enablement_Team_Member__c tmRec;
    private final Order ordObject;
    
    public OCOM_NewOrderTeamMemberController(ApexPages.StandardController stdController) {
        this.tmRec = (Cloud_Enablement_Team_Member__c)stdController.getRecord();
        
        String paramId = ApexPages.currentPage().getParameters().get('oid');
        try {
            ordObject = [select id from Order where id = :paramId limit 1];
        }
        catch(Exception e) {}
        if(ordObject == null)
            ordObject = new Order();
    }
    public Cloud_Enablement_Team_Member__c getTmRec() {
        return tmRec;
    }
    public String getOrderId() {
        return tmRec.Id;
    }
    public PageReference saveWorkReq() {
        if(ordObject != null)
            tmRec.order__c = ordObject.Id;
        try {
            upsert tmRec;
        }
        catch(Exception e) {
            system.debug('saveWorkReq:could not upsert tmRec'+tmRec);
            system.debug('saveWorkReq:Exception'+e);
        }
        String NHPTaskRecordType;
        RecordType taskRecordType;
        try{
            for(NHP_Setting__mdt itm: [select DeveloperName , value__c from NHP_Setting__mdt]){
                if(itm.DeveloperName.equalsIgnoreCase('Task_Record_Type'))
                    NHPTaskRecordType = itm.value__c;
                system.debug('saveWorkReq:Loading Custom Metadata Types: '+ itm.DeveloperName  + ' = '+itm.value__c);
            }
        }
        catch (Exception e) {
            System.debug('saveWorkReq: Query custom metadata types: received exception: ' + e.getMessage());
        }
        try{
            taskRecordType = [select Id, DeveloperName from RecordType where SObjectType = 'Task' and DeveloperName = : NHPTaskRecordType and IsActive = true limit 1];
        }
        catch (Exception e) {
            System.debug('saveWorkReq: Query Task record type: received exception: ' + e.getMessage());
        }
        try {
        Task taskRec = new Task();
        if(taskRecordType != null)
            taskRec.RecordTypeId = taskRecordType.Id;
        taskRec.Priority = 'Normal';
        taskRec.Activity_Type__c = 'Task';
        taskRec.Status = 'Not Started';
        taskRec.WhatId = tmRec.order__c;
        taskrec.Subject = System.Label.OCOM_Order_Manager_Task_Subject;
        taskrec.Description = System.Label.OCOM_Order_Manager_Task_Description;
        if(tmRec.Team_Member__c != null)
            taskRec.OwnerId = tmRec.Team_Member__c;
        Date dt = Date.today();
        taskRec.ActivityDate = dt.addDays(2);
        insert taskRec;
        system.debug('saveWorkReq:Created task:' + taskrec);
        }
        catch(Exception e) {
            System.debug('saveWorkReq: received exception while creating taskRec:' + e.getMessage());
        }
        PageReference pr = new PageReference('/' + tmRec.id);
        pr.setRedirect(true);
        return pr;
    }
    public PageReference cancelWorkReq() {
        PageReference pr = new PageReference('/' + tmRec.id);
        pr.setRedirect(true);
        return pr;
    }
}