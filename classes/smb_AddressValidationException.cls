public with sharing class smb_AddressValidationException extends Exception{
    
    public List<string> messages {get;set;}
    
    public smb_AddressValidationException(List<string> messages) {
        this.messages = messages;
        this.setMessage('');
    }
}