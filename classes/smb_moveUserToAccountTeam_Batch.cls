/*
###########################################################################
# File..................: smb_moveUserToAccountTeam_Batch
# Version...............: 26
# Created by............: Andrew Castillo
# Created Date..........: 16-Apr-2013
# Last Modified by......: 
# Last Modified Date....: 
# Description...........: This batch class is used by smb_moveUserToAccountTeam_sch.
Logic: 	1. Query for all team members and roles from Sales_Assignment__c
		2. Upsert team members into AccountTeamMember
#
# Copyright (c) 2000-2013. Astadia, Inc. All Rights Reserved.
#
# Created by the Astadia, Inc. Modification must retain the above copyright notice.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any commercial purpose, without fee, and without a written
# agreement from Astadia, Inc., is hereby forbidden. Any modification to source
# code, must include this paragraph and copyright.
#
# Permission is not granted to anyone to use this software for commercial uses.
#
# Contact address: 2839 Paces Ferry Road, Suite 350, Atlanta, GA 30339
# Company URL : http://www.astadia.com
###########################################################################
*/
global class smb_moveUserToAccountTeam_Batch implements Database.Batchable<sObject>
{
	global final String query;
	
	global smb_moveUserToAccountTeam_Batch(String q)
	{
		query = q;
    }

	/*
    * The start method is called at the beginning of a batch Apex job. Use the start method to collect the records or objects to be passed to the interface method execute. 
    * This method returns either a Database.QueryLocator object or an iterable that contains the records or objects being passed into the job.
    */ 
   	global Database.QueryLocator start(Database.BatchableContext BC)
   	{
      	return Database.getQueryLocator(query);
   	}

	/*
    *   The execute method is called for each batch of records passed to the method. Use this method to do all required processing for each chunk of data.
    *   @praram:- A reference to the Database.BatchableContext object.
    *   @param :- A list of sObjects, such as List<sObject>, or a list of parameterized types. If you are using a Database.QueryLocator, the returned list should be used.
    */
   	global void execute(Database.BatchableContext BC, List<sObject> scope)
   	{
   		moveTeamMembers(scope);
   	}

	 /*
     * The finish method is called after all batches are processed.
     */
    global void finish(Database.BatchableContext BC)
    {
    	// run account owner job
		AggregateResult AR_AO = [Select MAX(CreatedDate) CDate From AsyncApexJob a Where a.ApexClass.Name = 'smb_assignAccountOwnerTerritory_Batch'];
		
		Datetime lastRunAO = (Datetime) AR_AO.get('CDate');
		
		String aoQuery;
		if (lastRunAO == null)
		{
			// query for all active account owners and account territories
			aoQuery = 'Select Account__c, User__c, Territory_WLS__c, Territory_WLN__c From Sales_Assignment__c Where Role__c = \'ACCTPRIME\' And User__r.IsActive = true';
		}
		else
		{
			lastRunAO = lastRunAO.addDays(-2);
			
			// query for active account owners and account territories updated since the last time the job ran
			aoQuery = 'Select Account__c, User__c, Territory_WLS__c, Territory_WLN__c From Sales_Assignment__c Where Role__c = \'ACCTPRIME\' And User__r.IsActive = true And LastModifiedDate > ' + lastRunAO.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
		}
		
		system.debug('****'+aoQuery);
		
		//execute account owner batch
		smb_assignAccountOwnerTerritory_Batch runAOBatch = new smb_assignAccountOwnerTerritory_Batch(aoQuery);   
		database.executebatch(runAOBatch, 200);
    }
    
	/*
    *   This method refreshed the account team members for those accounts in the Sales_Assignment__c table
    */
    global static void moveTeamMembers(List<Sales_Assignment__c> SAs)
    {    	
    	// get role mappings
    	Map<String, eCAT_Role_Mapping__c> roleMap = eCAT_Role_Mapping__c.getAll();
    	
    	Set<Id> acctIds = new Set<Id>();
    	for (Sales_Assignment__c SA : SAs)
    	{
    		if (!acctIds.contains(SA.Account__c))
    		{
    			acctIds.add(SA.Account__c);
    		}
    	}
    	
    	// query for account team members for the accounts
    	List<AccountTeamMember> ATMs = [Select Id, AccountId, UserId, TeamMemberRole
    									From AccountTeamMember
    									Where LastModifiedById = :UserInfo.getUserId()
    									And AccountId IN :acctIds];
    	
    	// create list of account team member to delete
    	List<AccountTeamMember> deleteATMs = new List<AccountTeamMember>();
    	for (AccountTeamMember ATM : ATMs)
    	{
    		deleteATMs.add(ATM);
    	}

    	if (deleteATMs.size() > 0)
    	{
    		delete deleteATMs;
    	}
    	
    	List<Sales_Assignment__c> SAAccts = [Select Account__c, User__c, Role__c
    										 From Sales_Assignment__c 
    										 Where User__r.IsActive = true
    										 And Account__c IN :acctIds];
    	
    	// create list of new account team members
    	List<AccountTeamMember> newATMs = new List<AccountTeamMember>();
    	for (Sales_Assignment__c SA : SAAccts)
    	{    		
    		// translate roles
    		String role = SA.Role__c;
    		eCAT_Role_Mapping__c roleObj = roleMap.get(SA.Role__c);
    		if (roleObj != null)
    		{
    			role = roleObj.Account_Team_Role__c;
    		}
    		
    		AccountTeamMember newATM = new AccountTeamMember();
    		newATM.AccountId = SA.Account__c;
    		newATM.UserId = SA.User__c;
    		newATM.TeamMemberRole = role;
    		
    		newATMs.add(newATM);
    	}
    	
    	if (newATMs.size() > 0)
    	{
    		insert newATMs;
    	}
    }
}