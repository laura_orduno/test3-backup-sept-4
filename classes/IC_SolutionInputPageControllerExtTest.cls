@isTest
private class IC_SolutionInputPageControllerExtTest{

    @isTest
    static void testRecordTypeNameForLoanerDevice(){    
    PageReference pageRef = Page.IC_SolutionInputPage;
        Account acc= new Account(Name='Test Account');
        insert acc; 
        Contact contact=new Contact(LastName='Test', accountId = acc.id);
        contact.Email='test@test.com';
        insert contact;
        RecordType caseRecordType = [select Id, Name from RecordType where SObjectType = 'Case' and Name ='SMB Care Interim Connectivity'];        
        Case caseobj = new Case();        
        caseobj.RecordTypeId = caseRecordType.id;        
        caseobj.Status='New';        
        caseobj.Origin='Client';        
        caseobj.AccountId =acc.id;        
        caseobj.Subject ='test case';        
        caseobj.ContactId=contact.id;        
        caseobj.Contact_Email__c='prishan.fernando@telus.com';        
        insert caseobj;  
                    
        Test.setCurrentPageReference(pageRef);
        RecordType recordType = [select Id, Name from RecordType where SObjectType = 'IC_Solution__c' and Name = 'Loaner Device'];
        IC_Solution__c isc = new IC_Solution__c();
        isc.RecordTypeId = recordType.Id;
        isc.case__c = caseobj.id;
        ApexPages.StandardController sc = new ApexPages.standardController(isc);
        IC_SolutionInputPageControllerExt controller = new IC_SolutionInputPageControllerExt(sc);
        String RecordTypeName = controller.recordTypeName;
        System.assertEquals('Loaner Device', RecordTypeName);
    }
    @isTest
    static void testRecordTypeNameForInternetFax(){    
    PageReference pageRef = Page.IC_SolutionInputPage;
        Test.setCurrentPageReference(pageRef);
        RecordType recordType = [select Id, Name from RecordType where SObjectType = 'IC_Solution__c' and Name = 'Internet Fax'];
        IC_Solution__c isc = new IC_Solution__c();
        isc.RecordTypeId = recordType.Id;
        ApexPages.StandardController sc = new ApexPages.standardController(isc);
        IC_SolutionInputPageControllerExt controller = new IC_SolutionInputPageControllerExt(sc);
        String RecordTypeName = controller.recordTypeName;
        System.assertEquals('Internet Fax', RecordTypeName);
    }
    @isTest
    static void testRecordTypeNameForOOSCF(){    
    PageReference pageRef = Page.IC_SolutionInputPage;
        Test.setCurrentPageReference(pageRef);
        RecordType recordType = [select Id, Name from RecordType where SObjectType = 'IC_Solution__c' and Name = 'OOSCF'];
        IC_Solution__c isc = new IC_Solution__c();
        isc.RecordTypeId = recordType.Id;
        ApexPages.StandardController sc = new ApexPages.standardController(isc);
        IC_SolutionInputPageControllerExt controller = new IC_SolutionInputPageControllerExt(sc);
        String RecordTypeName = controller.recordTypeName;
        System.assertEquals('OOSCF', RecordTypeName);
    }
    @isTest
    static void testRecordTypeNameForTethering(){    
    PageReference pageRef = Page.IC_SolutionInputPage;
        Test.setCurrentPageReference(pageRef);
        RecordType recordType = [select Id, Name from RecordType where SObjectType = 'IC_Solution__c' and Name = 'Tethering'];
        IC_Solution__c isc = new IC_Solution__c();
        isc.RecordTypeId = recordType.Id;
        ApexPages.StandardController sc = new ApexPages.standardController(isc);
        IC_SolutionInputPageControllerExt controller = new IC_SolutionInputPageControllerExt(sc);
        String RecordTypeName = controller.recordTypeName;
        System.assertEquals('Tethering', RecordTypeName);
    }
}