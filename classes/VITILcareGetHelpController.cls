/**
 *  VITILcareGetHelpController
 *  @description Contoller for MBRGetHelp.page
 *  @author Majeda Mahfuz, Traction on Demand
 *  @date 2014-10-30
 */
public without sharing class VITILcareGetHelpController {


  public Boolean singleQuestion {get; set;}
  public String questionId {get;set;}
        public String CatName {get;set;}
  public Map<Id, CCIGetHelp__c> QuestionsList {get; set;}
  // used for custom exception messages
    public transient String pageError {get; set;}
 
  public VITILcareGetHelpController() {
    singleQuestion = false;
    if (ApexPages.currentPage().getParameters().get('q') !=null){
            questionId = ApexPages.currentPage().getParameters().get('q');
            singleQuestion = true;
        } 
     
  }
     
   
    public AggregateResult[] results {
        
        
        get {
            String userLanguage;
            String portal_string;
           
            if(userLanguage == null) {
                userLanguage = [SELECT LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()].LanguageLocaleKey;
            }
            
            if(userLanguage == 'en_US'){
                return [select Count(Id) aaa, Category__c bbb, name
                        from CCIGetHelp__c 
                        where Published__c = true and Portal__c = 'VITILcare'
                        GROUP BY name, Category__c
                        ORDER BY name];
            } 
            else 
            {  return [select Count(Id) aaa, Category__c bbb, name
                       from CCIGetHelp__c 
                       where Published__c = true and Portal__c = 'VITILcare-fr'
                       GROUP BY name, Category__c 
                       ORDER BY name];
            }
            
            
        }
        set;
    }
    
    public CCIGetHelp__c selectedQuestion {
        get{
            String urlQuestionNumber = ApexPages.currentPage().getParameters().get('questionNumber');
            selectedQuestion = [SELECT Question__c, Id, Answer__c from CCIGetHelp__c where Id = :urlQuestionNumber LIMIT 1];
            return selectedQuestion;
        }
        set;}
    
    public List<CCIGetHelp__c> getQuestions(){
        String userLanguage;
        String portal_string;
        if(userLanguage == null) {
            userLanguage = [SELECT LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()].LanguageLocaleKey;
        }
        
        if(userLanguage == 'en_US'){
            portal_string = 'VITILcare';
        } 
        else 
        {  portal_string = 'VITILcare-fr'; }
        
        String queryString = 'SELECT Id, Question__c, Category__c, Answer__c FROM CCIGetHelp__c ';
        if(String.isNotEmpty(questionId)) {
            queryString += ' WHERE Id = \'' + questionId + '\' Order By Name';
        } else {
            queryString += ' WHERE Published__c = true';
            
            if(String.isNotEmpty(portal_string)){
                queryString += ' AND Portal__c INCLUDES (\'' + portal_string + '\')';
            } 
            queryString += ' ORDER BY Name LIMIT 50';
        }
        
        System.debug('VITILcareGetHelpController: ' + queryString);
        List<CCIGetHelp__c> allQuestions = (List<CCIGetHelp__c>) database.query( queryString );
        
        
        return(allQuestions);
    }
    
    //public void mapQuestions(){
    //  List<CCIGetHelp__c> cciList =  getQuestions();
    //  for(CCIGetHelp__c cgh : cciList){
    //    System.debug(QuestionsList + '============' + cgh);
    //    QuestionsList.put(cgh.Id, cgh);
    //  }
    
    //}
    
    public String[] getCategories(){
        String userLanguage;
        String portal_string;
        if(userLanguage == null) {
            userLanguage = [SELECT LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()].LanguageLocaleKey;
        }
        String[] CList = new List<String>();
        Schema.sObjectType objType = CCIGetHelp__c.getSObjectType(); 
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();       
        Map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
        List<Schema.PicklistEntry> values = fieldMap.get('Category__c').getDescribe().getPickListValues();
        for (Schema.PicklistEntry a : values) {
            String CatValueT = a.getValue();
            System.debug('Cat Value: ' + CatValueT);
             if(userLanguage == 'en_US'){
            if((CatValueT != 'Submitting requests') && (CatValueT != 'Managing requests') && (CatValueT != 'Questions générales') && (CatValueT != 'Envoi de billets') && (CatValueT != 'Profil et paramètres') && (CatValueT != 'Gestion des billets')){
                CList.add(a.getValue()); 
            }
            }
             else{
            if((CatValueT != 'Submitting requests') && (CatValueT != 'Managing requests') && (CatValueT != 'General questions') && (CatValueT != 'Submitting tickets') && (CatValueT != 'Managing tickets') && (CatValueT != 'Profile and settings')){
                CList.add(a.getValue()); 
            }
            }
        
        }
        return(CList);
    }
}