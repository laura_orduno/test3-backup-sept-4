/******************************************************************************
@author     : Arpit Goyal
Date        : 29-Sept-2016
Requirement : MVP Release
Purpose     : This class contains utility methods that will be used for multi site flow  
/*******************************************************************************/



global with sharing class Naas_Multi_Site_Utility implements vlocity_cmt.VlocityOpenInterface {
    public static boolean firstRun = true;
    
    global Boolean invokeMethod(String methodName, Map<String, Object> input, Map<String, Object> output, Map<String, Object> options)  {        
        if (methodName.equals('createContract')){
             createContract(input, output, options); return true;             
        } return false; }
        
        
    
    
    
    //Arpit 14-Nov-2016 : Sprint 8 : This method cretes contracts from NAAS order omniscript
    public PageReference createContract(Map<String, Object> inputMap, Map<String, Object> outputMap, Map<String, Object> options){
        system.debug('inputMap ---->' + inputMap);
        Object contextId = inputMap.get('ContextId');
        Id orderId = (Id) contextId ;
        String baseUrl = System.Url.getSalesforceBaseURL().toExternalForm() ;
        system.debug('orderId ---->' + orderId);
        
            Id response = vlocity_cmt.ContractServiceResource.createContractWithTemplate(orderId) ;//Need to put try-catch here
            PageReference pageRef = new PageReference(baseUrl+'/' + response + '/e?retURL=%2F' + response);    
            return pageRef;
          
                   
    }
     
}