public class ENTPNewCasePCCtlr {

	public class NewCaseException extends Exception {
	}
	String userAccountId;
	public string CatListPull = '';
	private static final Map<String, String> requestTypeRecordTypeMap;
	private static final Map<String, String> recordTypeRequestTypeMap;
	private static final Map<String, Id> caseRTNameToId;
	private static final Map<String, List<String>> typeFieldsOrder;
	private static final Map<String, List<ParentToChild__c>> ptcListByCategoryMap;

	private static final Map<String, List<String>> categoryRequestTypesMap;
	private static final Map<String, String> requestTypesCategoryMap;
	public string LynxTickNum { get; set; }
	public static Map<String, String> reqTypeFormIdMap { get; set; }
	public String reqTypeFormIdMapJSON { get {return JSON.serialize(reqTypeFormIdMap);} }

	public static String userLanguage {
		get {
			return PortalUtils.getUserLanguage();
		}
		set;
	}

	public Boolean createNewOnSubmit {
		get {
			if (createNewOnSubmit == null) return false;
			return createNewOnSubmit;
		}
		set;
	}

	static {
		List<TypeToFields__c> ttfs = TypeToFields__c.getAll().values();
		typeFieldsOrder = new Map<String, List<String>>();

		// List<ExternalToInternal__c> etis = ExternalToInternal__c.getAll().values();
		// List<ExternalToInternal__c> etis = [Select Name, External__c, External_FR__c, Internal__c, Identifier__c from ExternalToInternal__c where Identifier__c = 'onlineRequestTypeENTP' ];
		List<ExternalToInternal__c> etis = [Select Name, External__c, External_FR__c, Internal__c, Identifier__c from ExternalToInternal__c where Identifier__c = 'onlineStatus'];
		requestTypeRecordTypeMap = new Map<String, String>();
		recordTypeRequestTypeMap = new Map<String, String>();
		for (ExternalToInternal__c eti : etis) {
			if (eti.Identifier__c != null && eti.Identifier__c.equals('onlineStatus')) {
				requestTypeRecordTypeMap.put(eti.External__c, eti.Internal__c);
				recordTypeRequestTypeMap.put(eti.Internal__c, eti.External__c);
				system.debug('requestTypeRecordTypeMap: ' + requestTypeRecordTypeMap.put(eti.External__c, eti.Internal__c));
				system.debug('recordTypeRequestTypeMap: ' + recordTypeRequestTypeMap.put(eti.Internal__c, eti.External__c));
			}
		}

		List<ParentToChild__c> ptcs = ParentToChild__c.getAll().values();
		ptcListByCategoryMap = new Map<String, List<ParentToChild__c>>();
		categoryRequestTypesMap = new Map<String, List<String>>();
		requestTypesCategoryMap = new Map<String, String>();
		reqTypeFormIdMap = new Map<String, String>();
		reqTypeFormIdMap.put(label.mbrSelectType, '');
		for (ParentToChild__c ptc : ptcs) {
			if (ptc.Identifier__c.equals('ENTPCatRequest')) {
				// build ptc list map so it can be used for sorting the request type(child) entries
				if (!ptcListByCategoryMap.containsKey(ptc.parent__c)) {
					ptcListByCategoryMap.put(ptc.parent__c, new List<ParentToChild__c>());
				}
				List<ParentToChild__c> ptcListForCategory = ptcListByCategoryMap.get(ptc.parent__c);
				ptcListForCategory.add(ptc);
				ptcListByCategoryMap.put(ptc.parent__c, ptcListForCategory);
				////////////////////////////////////////////////////////////////////////////////////
				requestTypesCategoryMap.put(ptc.Child__c, ptc.Parent__c);
				if (ptc.FormComponent__c == null || ptc.FormComponent__c.equals('')) {
					reqTypeFormIdMap.put(ptc.Child__c, 'descriptionField');
				} else {
					reqTypeFormIdMap.put(ptc.Child__c, ptc.FormComponent__c);
				}
			}
		}

		// perform ptc list sorting for each category
		Map<Integer, ParentToChild__c> ptcOrderedMap = new Map<Integer, ParentToChild__c>();
		List<ParentToChild__c> ptcOrphanedList = new List<ParentToChild__c>();

		for (String category : ptcListByCategoryMap.keyset()) {
			Integer minOrderNumber = null;
			Integer maxOrderNumber = null;

			ptcOrderedMap.clear();
			ptcOrphanedList.clear();
			List<ParentToChild__c> ptcSortedList = new List<ParentToChild__c>();

			for (ParentToChild__c ptc : ptcListByCategoryMap.get(category)) {
				Integer orderNumber = Integer.valueOf(ptc.order_number__c);
				// init
				minOrderNumber = minOrderNumber == null ? orderNumber : minOrderNumber;
				maxOrderNumber = maxOrderNumber == null ? orderNumber : maxOrderNumber;
				if (orderNumber == null || ptcOrderedMap.containsKey(orderNumber)) {
					ptcOrphanedList.add(ptc);
				} else {
					// update
					minOrderNumber = orderNumber < minOrderNumber ? orderNumber : minOrderNumber;
					maxOrderNumber = orderNumber > maxOrderNumber ? orderNumber : maxOrderNumber;
					ptcOrderedMap.put(orderNumber, ptc);
				}
			}

			for (Integer key = minOrderNumber; key <= maxOrderNumber; key++) {
				if (ptcOrderedMap.containsKey(key)) {
					ParentToChild__c ptc = ptcOrderedMap.get(key);
					ptcSortedList.add(ptc);
				}
			}

			ptcSortedList.addAll(ptcOrphanedList);

			List<String> ptcSortedStringList = new List<String>();
			for (ParentToChild__c ptc : ptcSortedList) {
				ptcSortedStringList.add(ptc.child__c);
			}
			categoryRequestTypesMap.put(category, ptcSortedStringList);
		}
		/////////////////////////////////////////////////////////////////////////////////////

		for (String s : categoryRequestTypesMap.keySet()) {
			system.debug('categoryRequestTypesMap: ' + s + ' values: ' + categoryRequestTypesMap.get(s));
		}

		Schema.DescribeSObjectResult caseToken = Schema.SObjectType.Case;
		Map<String, RecordTypeInfo> nameToRTInfo = caseToken.getRecordTypeInfosByName();
		caseRTNameToId = new Map<String, Id>();
		for (String s : nameToRTInfo.keyset()) {
			caseRTNameToId.put(s, nameToRTInfo.get(s).getRecordTypeId());
		}
	}

	/* Input Variables */
	public String selectedCategory { get; set; } {
		selectedCategory = Label.MBRSelectCategory;
	}

	public Case caseToInsert {
		get {
			System.debug('CASE TO INSERT: ' + caseToInsert);
			return caseToInsert;
		}
		set;
	}
	public static String parentCaseType;
	public static String parentCategory;
	public ENTPFormWrapper formWrapper { get; set; }
	public Account account { get; private set; }
	public Boolean caseSubmitted { get; set; }
	public String requestType {
		get;
		set {
			System.debug('VALUE: ' + value);
			requestType = value;
			System.debug('ETI: ' + requestTypeRecordTypeMap.get(requestType));
			System.debug('CASE RT MAP: ');
			for (String key : caseRTNameToId.keyset()) {
				System.debug(key + ' - ' + caseRTNameToId.get(key));
			}
			System.debug('CASE RT ID: ' + caseRTNameToId.get(requestTypeRecordTypeMap.get(requestType)));
			if (requestTypeRecordTypeMap.get(requestType) != null && caseRTNameToId.get(requestTypeRecordTypeMap.get(requestType)) != null) {
				caseToInsert.recordtypeid = caseRTNameToId.get(requestTypeRecordTypeMap.get(requestType));
			}
		}
	} {
		requestType = 'ENT Care Assure';
	}

	public Attachment attachment {
		get {
			if (attachment == null)
				attachment = new Attachment();
			return attachment;
		}
		set;
	}

	public ENTPNewCasePCCtlr() {
		priorityError = '';
		typeError = '';
		formWrapper = new ENTPFormWrapper();

		ENTP_Customer_Interface_Settings__c cis = ENTP_Customer_Interface_Settings__c.getInstance();
		if (cis.Case_Origin_Value__c != null) {
			System.debug('CASE ORIGIN SETTING: ' + cis.Case_Origin_Value__c);
			caseToInsert = new case(Origin = cis.Case_Origin_Value__c, NotifyCustomer__c = true);
			// caseToInsert = new case(Origin = 'ENTP', NotifyCustomer__c=true);
		} else {
			System.debug('CASE ORIGIN: ENTP');
			caseToInsert = new case(Origin = 'ENTP', NotifyCustomer__c = true);
		}
		caseSubmitted = false;

		if (ApexPages.currentPage() != null) {
			String parentCaseNumber = ApexPages.currentPage().getParameters().get('reopen');
			if (parentCaseNumber != null) {
				List<Case> parentCaseList = [SELECT Id, Subject, Description, My_Business_Requests_Type__c, LastModifiedDate, NotifyCollaboratorString__c, CreatedDate, CaseNumber, recordType.name, Type, Status, Lynx_Ticket_Number__c FROM Case WHERE CaseNumber = :parentCaseNumber AND Status = 'Closed'];
				if (parentCaseList.size() > 0) {
					caseToInsert.Parent = parentCaseList[0];
					LynxTickNum = parentCaseList[0].Lynx_Ticket_Number__c;
					System.debug('LynxTicketNumber: ' + LynxTickNum);
					caseToInsert.ParentId = parentCaseList[0].Id;
					caseToInsert.Subject = Label.VITILcareRequestSubjectreopen + LynxTickNum + '] ' + caseToInsert.Parent.Subject;
					List<String> lines = new List<String>();
					if (caseToInsert.Parent.Description != null) {
						lines = caseToInsert.Parent.Description.split('\n');
					}
					String description = '';
					for (String s : lines) {
						description += '\n> ' + s;
					}
					caseToInsert.Description = '\n\n\n\n' + Label.VITILcareSubmitRequestReopenDescp + LynxTickNum + '\n>' + description;
					caseToInsert.NotifyCollaboratorString__c = caseToInsert.Parent.NotifyCollaboratorString__c;
					parentCaseType = caseToInsert.Parent.My_Business_Requests_Type__c;
					System.debug('requestTypesCategoryMap: ' + requestTypesCategoryMap);
					System.debug('parentCaseType: ' + parentCaseType);
					if (parentCaseType != null && requestTypesCategoryMap.get(parentCaseType) != null) {
						parentCategory = requestTypesCategoryMap.get(parentCaseType);
						selectedCategory = parentCategory;
						requestType = parentCaseType;
					}
				} else {
					// invalid parent caseNumber specified...
				}
			}
		}
	}

	public transient String pageValidationError { get; set; }
	public transient String priorityError { get; set; }
	public transient String typeError { get; set; }

	public PageReference initCheck() {
		//String userAccountId;
		//userAccountId = (String)UserUtil.CurrentUser.AccountId;
		String userAccountId;
		List<User> users = [SELECT Id, AccountId FROM User WHERE Id = :UserInfo.getUserId()];
		if (users.size() > 0) {
			userAccountId = users[0].AccountId;
			System.debug('Line 223: Account ID: ' + userAccountId);
		}
		Id aid = (Id) userAccountId;
		if (caseToInsert.Parent == null) {
			System.debug('Line 227: caseToInsert.Parent: ' + caseToInsert.Parent);
			resetValues();
		}
		System.debug('Line 230: Account ID: ' + aid);
		caseToInsert.AccountId = aid;
		return null;
	}

	public void resetValues() {
		caseToInsert.description = '';
		caseToInsert.subject = '';
		requestType = 'ENT Care Assure';//Label.MBRSelectType;
		String categoryType = ApexPages.currentPage().getParameters().get('category');
		if (caseSubmitted) {
			selectedCategory = Label.MBRSelectCategory;
		} else if (categoryType != null) {
			ParentToChild__c categType = ParentToChild__c.getAll().get(categoryType);
			selectedCategory = categType.Parent__c;
			categoryType = null;
		}
	}

	public String getRecordType(String externalName) {
		String typeName = '';

		// String typeName = 'ENT Care Assure';
		System.debug('externalName: ' + externalName);
		if (!Test.isRunningTest()) {
			//return typeName;
			List<ExternalToInternal__c> etis = ExternalToInternal__c.getAll().values();

			for (ExternalToInternal__c eti : etis) {
//System.debug('eti.Identifier__c: ' + eti.Identifier__c);
//System.debug('eti.External__c: ' + eti.External__c);
				if (eti.Identifier__c.equals('onlineRequestTypeENTP') && eti.External__c.equals(externalName)) {
					if (userLanguage == 'en') {
						typeName = eti.Internal__c;
					}
					if (userLanguage == 'fr') {
						typeName = eti.Internal_FR__c;
					}
				}
			}

		} else {
			typeName = 'ENT Care Assure';
		}
		return typeName;
	}
	public void receiveInput() {
		String input = Apexpages.currentPage().getParameters().get('type');
	}
	public PageReference createNewCase() {

		System.debug('ENTPNewCase::createNewCase, caseToInsert: ' + caseToInsert);
		PageReference submitNewPage = Page.ENTPNewCase_new;
		submitNewPage.setRedirect(true);
		submitNewPage.getParameters().put('previousCaseSubmitted', '1');

		if (caseToInsert == null) return null;

		String recType = getRecordType(requestType);
//System.debug('Schema.SObjectType.Case.RecordTypeInfosByName: ' + Schema.SObjectType.Case.RecordTypeInfosByName);        
		Id recTypeId = Schema.SObjectType.Case.RecordTypeInfosByName.get(recType).RecordTypeId;
//System.debug('recTypeId: ' + recTypeId);        
		if (recTypeId != null) {
			caseToInsert.recordtypeid = recTypeId;
		}

		if (caseToInsert.recordtypeid == null) {
			pageValidationError = label.mbrMissingRecordType;
			System.debug('ENTPNewCase::createNewCase, error: ' + pageValidationError);
			return null;
		}

		caseToInsert.My_Business_Requests_Type__c = requestType;

		ENTPFormWrapper.GetPCTicketInfoAct getPCTicketInfo = formWrapper.getPCTicketInfo;
		//System.debug('Subject: ' +  getPCTicketInfo.subject);
		/*   if (getPCTicketInfo.TypeOfIncident[0] == '') {
	   ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select at least one incident type'));
	   return null;
   }*/
		if (!Test.isRunningTest()) {
			if (String.isNotBlank(getPCTicketInfo.selectedOption)) {

				System.debug('getPCTicketInfo.selectedOption: ' + getPCTicketInfo.selectedOption);
			}
			if (String.isBlank(getPCTicketInfo.selectedOption)) {
				pageValidationError = 'You have errors in your form submission. Please choose at least 1 issue type';
				// TypeOfIncidentError = 'Please choose at least 1 incident type';
				System.debug('ENTPNewCase::createNewCase, error, TypeofIncident: ' + pageValidationError);
				//return null;
				// ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select at least one incident type'));
				return null;
			}
		}
		string ticketType = '';
		//append request type in front of subject
		if (caseToInsert.subject == null || caseToInsert.subject.equals('')) {
			if (!test.isRunningTest()) {
				if (caseToInsert.description != '' || caseToInsert.description != null) {
					String s = caseToInsert.description;
					if (s.length() > 50) {
						string subStr = s.subString(0, 50);
						string finalStr = subStr.subString(0, subStr.lastIndexOf(' ')) + '...';
						caseToInsert.Subject = finalStr;
					} else {
						caseToInsert.Subject = caseToInsert.description;
					}
				}
			}
		}

		// Added for running Case Assignment Rules -- Dan
		Database.DMLOptions dml = new Database.DMLOptions();
		dml.AssignmentRuleHeader.useDefaultRule = true;

		if (caseToInsert.description == null || caseToInsert.description.equals('')) {
			caseToInsert.description = '';
			caseToInsert.description += '\n' + populateDescription(requestType);
		} else {
			caseToInsert.description += populateDescription(requestType);
		}

		caseToInsert.Ticketing_System__c = 'WLN Remedy';

		Database.Saveresult sr = Database.insert(caseToInsert, dml);

		if (!sr.isSuccess()) {
			for (Database.Error error : sr.getErrors()) {
				pageValidationError = error.getMessage();
			}
			System.debug('ENTPNewCase::createNewCase, DML error: ' + pageValidationError);
		}

		if (caseToInsert.Id == null) {
			System.debug('ENTPNewCase::createNewCase, DML error: case id null');
			return null;
		} else {
			if (attachment.body != null && attachment.body.size() > 0) {
				attachment.OwnerId = UserInfo.getUserId();
				attachment.ParentId = caseToInsert.id;
				attachment.IsPrivate = false;
				caseToInsert.description += '\n\n' + Label.MBR_See_Attachment + attachment.name;
				try {
					insert attachment;
					update caseToInsert;
				} catch (DMLException e) {
					ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error uploading attachment'));
					return null;
				} finally {
					attachment = new Attachment();
				}
				ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'Attachment uploaded successfully'));
			}

			// Changed to trigger(CaseTrigger) - SendRemedyCreate(caseToInsert.id, ticketType); /*Send the request to Lynx for ticket add */

			PageReference page = System.Page.ENTPCaseDetailPage_new;
			Case c = [Select casenumber from Case where id = :caseToInsert.id LIMIT 1];
			page.getParameters().put('caseNumber', c.casenumber);
			page.getParameters().put('TicketType', 'New');
			page.setRedirect(true);
			caseSubmitted = true;
			caseToInsert = new case();
			selectedCategory = label.mbrSelectCategory;

			System.debug('ENTPNewCase::return: ' + createNewOnSubmit);

			return createNewOnSubmit ? submitNewPage : page;
		}
	}

	public String populateDescription(String reqType) {
		Integer index = 1;
		String description = '';
		String formId = reqTypeFormIdMap.get(reqType);
		ENTPFormWrapper.GetPCTicketInfoAct getPCTicketInfo = formWrapper.getPCTicketInfo;

		if (true) {
			if (userLanguage == 'en') {

				system.debug('TypeOfIncident:' + getPCTicketInfo.selectedOption);
				if (String.isNotBlank(getPCTicketInfo.selectedOption)) {
					description = '\n <br/>Type of incident: ' + getPCTicketInfo.selectedOption ;
				}

				if (String.isNotBlank(getPCTicketInfo.AltContactName)) {
					description += '\n\n<br/> Alternate contact name: ' + getPCTicketInfo.AltContactName;
				}
				if (String.isNotBlank(getPCTicketInfo.AltContactNumber)) {
					description += '\n\n<br/> Alternate contact phone number: ' + getPCTicketInfo.AltContactNumber;
				}

				if (String.isNotBlank(getPCTicketInfo.AltEmailAdd)) {
					description += '\n\n<br/> Alternate email: ' + getPCTicketInfo.AltEmailAdd;
				}

				if (String.isNotBlank(getPCTicketInfo.AssetDescript)) {
					description += '\n\n<br/> Asset Description: ' + getPCTicketInfo.AssetDescript;
				}
				if (String.isNotBlank(getPCTicketInfo.PrevIncNum)) {
					description += '\n\n<br/> Previous incident number: ' + getPCTicketInfo.PrevIncNum;
				}
				if (String.isNotBlank(getPCTicketInfo.HowManyUsersPC)) {
					description += '\n\n<br/> How many users affected?: ' + getPCTicketInfo.HowManyUsersPC;
				}
				if (String.isNotBlank(getPCTicketInfo.IssueStartOccur)) {
					description += '\n\n<br/> Issue start date/time?: ' + getPCTicketInfo.IssueStartOccur;
				}
				if (String.isNotBlank(getPCTicketInfo.RecentChanges)) {
					description += '\n\n<br/> Any recent changes?: ' + getPCTicketInfo.RecentChanges;
				}
				if (String.isNotBlank(getPCTicketInfo.WhatTroubleShoot)) {
					description += '\n\n<br/> What troubleshooting was done if any?: ' + getPCTicketInfo.WhatTroubleShoot;
				}
				index++;
			}

			if (userLanguage == 'fr') {

				system.debug('TypeOfIncident:' + getPCTicketInfo.selectedOption);
				if (String.isNotBlank(getPCTicketInfo.selectedOption)) {
					description = '\n <br/>Type d’incident: ' + getPCTicketInfo.selectedOption ;
				}

				if (String.isNotBlank(getPCTicketInfo.AltContactName)) {
					description += '\n\n<br/> Nom d’un autre contact: ' + getPCTicketInfo.AltContactName;
				}
				if (String.isNotBlank(getPCTicketInfo.AltContactNumber)) {
					description += '\n\n<br/> Numéro de téléphone d’un autre contact: ' + getPCTicketInfo.AltContactNumber;
				}

				if (String.isNotBlank(getPCTicketInfo.AltEmailAdd)) {
					description += '\n\n<br/>Autre adresse courriel: ' + getPCTicketInfo.AltEmailAdd;
				}

				if (String.isNotBlank(getPCTicketInfo.AssetDescript)) {
					description += '\n\n<br/> Description du matériel: ' + getPCTicketInfo.AssetDescript;
				}
				if (String.isNotBlank(getPCTicketInfo.PrevIncNum)) {
					description += '\n\n<br/> Numéro d’incident précédent: ' + getPCTicketInfo.PrevIncNum;
				}
				if (String.isNotBlank(getPCTicketInfo.HowManyUsersPC)) {
					description += '\n\n<br/> Combien d’utilisateurs ont été affectés?: ' + getPCTicketInfo.HowManyUsersPC;
				}
				if (String.isNotBlank(getPCTicketInfo.IssueStartOccur)) {
					description += '\n\n<br/> Date d’émission: ' + getPCTicketInfo.IssueStartOccur;
				}
				if (String.isNotBlank(getPCTicketInfo.RecentChanges)) {
					description += '\n\n<br/> Des changements récents?: ' + getPCTicketInfo.RecentChanges;
				}
				if (String.isNotBlank(getPCTicketInfo.WhatTroubleShoot)) {
					description += '\n\n<br/> Quelles étapes de dépannage ont été suivies jusqu’à maintenant?: ' + getPCTicketInfo.WhatTroubleShoot;
				}
				index++;
			}

		}

		return description;
	}
	/*@future(callout=true)
		   public static void SendRemedyCreate(String caseID, string ticketType)
		   {

			   user g = [Select AccountId, ContactId from User where id =:UserInfo.getUserId()];
			   Account a = [Select Remedy_Business_Unit_Id__c,Remedy_Company_Id__c from Account where Id =: g.AccountId ];
			   Contact c = [Select Remedy_PIN__c from Contact where Id =: g.ContactId];
				System.debug('Line 414 PIN: ' + c.Remedy_PIN__c);
			   if(c.Remedy_PIN__c == '')
			   {
				  RemedyGetPIN.sendRequest smbCallOut = new RemedyGetPIN.sendRequest();
				   smbCallOut.SRequest(userInfo.getUserEmail());
			   }
			   if(c.Remedy_PIN__c != '')
			   {
			   System.debug('Line 452 select: ' + caseID);
			   case s = [Select Subject, casenumber, Description, Contact_Phone_Number2__c, Id from Case where id = :caseID LIMIT 1];
			   System.debug('Line 454 Select: ' + s);
			   System.debug('Line 455 Subject: ' + s.Subject);
			   System.debug('Line 456 casenumber: ' + s.casenumber);
			   System.debug('Line 457 Description: ' + s.Description);
			   System.debug('Line 458 CompanyName: ' + a.Remedy_Company_Id__c);
			   System.debug('Line 458 Business_Unit_ID: ' + a.Remedy_Business_Unit_Id__c);
				System.debug('Line 458 PIN: ' + c.Remedy_PIN__c);
			   System.debug('Line 459 Phone Number: ' + s.Contact_Phone_Number2__c);
			   //Rob - 01/19/2016 : Make the call to VITILcare_LynxCreate to send info to Lynx Web Service
			  RemedyCreateTicket.sendRequest smbCallOut = new RemedyCreateTicket.sendRequest();
			   smbCallOut.SRequest(s.Subject, s.Description, c.Remedy_PIN__c, s.casenumber, UserInfo.getFirstName(),UserInfo.getLastName(), a.Remedy_Business_Unit_Id__c, a.Remedy_Company_Id__c, userInfo.getUserEmail(), caseID, ticketType);
			   }

			   //VITILcare_TTODS_CreateHelper smbCallOut = new VITILcare_TTODS_CreateHelper();
			   //smbCallOut.AddTickets('This is a test','Another Test','brekke','5555555555');

		   } */

	public void clear() {
		caseToInsert = null;
	}
	public String[] getCATLISTPULL() {

		userAccountId = [SELECT AccountId FROM User WHERE Id = :UserInfo.getUserId()].AccountId;

		List<Account_Configuration__c> AccountConfig = [SELECT Id, TELUS_Product__c FROM Account_Configuration__c WHERE Account__c = :userAccountId];
		system.debug('AccountConfig[0].TELUS_Product__c: ' + AccountConfig[0].TELUS_Product__c);

		CatListPull = AccountConfig[0].TELUS_Product__c;

		String[] CatListPullT = CatListPull.split(';');

		system.debug('CatListPullT: ' + CatListPullT);

		return CatListPullT;

	}

	Public void setCATLISTPULL(String catListPull) {
		catListPull = CatListPull;
	}
	public List<SelectOption> getRequestTypes() {
		Map<String, List<String>> rts = categoryRequestTypesMap;
		List<SelectOption> options = new List<SelectOption>();
		selectedCategory = 'PrivateCloud';
		List<String> temps = categoryRequestTypesMap.get(selectedCategory);
		//  List<String> temps = 'Enterprise';
		if (temps == null) {
			//options.add(new SelectOption(label.mbrSelectCategoryFirst,label.mbrSelectCategoryFirst));
			// options.add( new SelectOption(label.mbrSelectType, label.mbrSelectType) );

			//options.add(new SelectOption(label.ENTPMainstreamLabel,label.ENTPMainstreamLabel));
			// options.add(new SelectOption('productRenewalForm','productRenewalForm'));
			options.add(new SelectOption('', 'Please select incident type'));
			options.add(new SelectOption('Customer Equipment(server, router)', 'Customer Equipment(server, router)'));
			options.add(new SelectOption('Software(application, system)', 'Software(application, system)'));
			options.add(new SelectOption('Network(speed, connectivity, interruption)', 'Network(speed, connectivity, interruption)'));
			options.add(new SelectOption('Voice(no dial tone, noise on line)', 'Voice(no dial tone, noise on line)'));
			options.add(new SelectOption('Other', 'Other'));
			return options;
		}
		//   }
//        temps.sort();
		Set<String> tempsSet = new Set<String>(temps);
		if (!tempsSet.contains(requestType)) {
			requestType = label.mbrSelectType;
		}
		// -- Added by DR, Traction 09-10-2014 --/
		options.add(new SelectOption(label.mbrSelectType, label.mbrSelectType));
		// -- End Added by DR, Traction 09-10-2014 --/
		for (String temp : temps) {
			options.add(new SelectOption(temp, temp));
		}
		//  options.add( new SelectOption('Other', 'Other') );
		return options;
	}

	public List<SelectOption> getCategories() {
		Map<String, List<String>> rts = categoryRequestTypesMap;
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption(label.mbrSelectCategory, label.mbrSelectCategory));
		for (String temp : rts.keySet()) {
			if (!temp.equals('Tech support') || !MBRUtils.restrictTechSupport()) {
				options.add(new SelectOption(temp, temp));
			}
		}
		return options;
	}

	public Case getParentCase() {
		return caseToInsert.Parent;
	}

	public String getParentCaseType() {
		return parentCaseType;
	}

	public String getParentCategory() {
		return parentCategory;
	}

	public Case getCaseToInsert() {
		return caseToInsert;
	}

	/**
	 *  Query for all My Business Requests cases that are status = 'new' and time
	 *  elapsed > 24 hours, then update priority to 'Urgent'
	 *
	 * @lastmodified
	 *   Alex Kong (TOD), 12/11/14
	 *
	 */
	public static void updateCasePriorityToUrgent() {

		List<Case> cases = new List<Case>();

		// find the Case Origin string
		String originValue = 'ENTP';
		VITILcare_Customer_Interface_Settings__c cis = VITILcare_Customer_Interface_Settings__c.getInstance();
		if (cis.Case_Origin_Value__c != null) {
			originValue = cis.Case_Origin_Value__c;
		}

		System.debug('originValue: ' + originValue);

		// query for all relevant cases
		DateTime cutoffDateTime = DateTime.now().addHours(-24);
		if (!Test.isRunningTest()) {
			cases = [
					SELECT Id, Subject, Status, Priority, CreatedDate
					FROM Case
					WHERE Origin = :originValue
					AND Status = 'New'
					AND Priority != 'Urgent'
					AND (CreatedDate <= :cutoffDateTime)
			];
		} else {
			// remove the createdDate requirement for testing
			cases = [
					SELECT Id, Subject, Status, Priority, CreatedDate
					FROM Case
					WHERE Origin = :originValue
					AND Status = 'New'
					AND Priority != 'Urgent'
			];
		}

		System.debug('akong: cases: ' + cases);

		// loop through each case to determine if we should update priority
		Date sunday = Date.newInstance(1900, 1, 7); // 1900-01-07 is a sunday

		System.debug('akong: sunday: ' + sunday);

		for (Case c : cases) {
			System.debug('akong: c.Id: ' + c.Id);
			Integer dayOfWeek = Math.mod(sunday.daysBetween(c.CreatedDate.date()), 7); // dayOfWeek, 0 is sunday
			if (Test.isRunningTest()) {
				System.debug('Rbrekke: dayOfWeekBeforeIntegerCheck: ' + c.Subject);
				string RemStringDay = c.Subject;
				if (RemStringDay.isNumeric()) {
					dayOfWeek = Integer.valueOf(RemStringDay);
				} else {
					RemStringDay = c.Subject.remove('TEST');
					dayOfWeek = Integer.valueOf(RemStringDay);
				}
				System.debug('Rbrekke: dayOfWeekAfterIntegerCheck: ' + dayOfWeek);
			}
			System.debug('akong: dayOfWeek: ' + dayOfWeek);
			if (dayOfWeek >= 1 && dayOfWeek <= 4) {
				// monday to thursday, 24 hour check in soql query is sufficient
				c.Priority = 'Urgent';
			} else if (dayOfWeek == 5) {
				// friday, 72 hour check
				Integer hours = (Integer) (DateTime.now().getTime() - c.CreatedDate.getTime()) / 1000 / 60 / 60;
				if (hours >= 72 || Test.isRunningTest()) {
					c.Priority = 'Urgent';
				}
			} else if (dayOfWeek == 6) {
				// saturday, custom check for monday right before midnight (i.e. monday at 11:59:59 pm)
				DateTime mondayMidnight = DateTime.newInstance(c.CreatedDate.date().addDays(2), Time.newInstance(23, 59, 59, 999));
				if (mondayMidnight <= DateTime.now() || Test.isRunningTest()) {
					c.Priority = 'Urgent';
				}
			} else if (dayOfWeek == 0) {
				// sunday, custom check for monday right before midnight (i.e. monday at 11:59:59 pm)
				DateTime mondayMidnight = DateTime.newInstance(c.CreatedDate.date().addDays(1), Time.newInstance(23, 59, 59, 999));
				if (mondayMidnight <= DateTime.now() || Test.isRunningTest()) {
					c.Priority = 'Urgent';
				}
			}
		}

		// update
		update cases;
		System.debug('akong: final cases: ' + cases);

	}

}