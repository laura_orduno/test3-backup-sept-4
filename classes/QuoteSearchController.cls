public without sharing class QuoteSearchController {
    private static final String SOSL_TEMPLATE = 
                'FIND {0} IN ALL FIELDS RETURNING ' +
                    'Web_Account__c (Id, Name, Billing_City__c, RCID__c, Billing_Province__c, Phone__c ORDER BY {1} {2}), ' +
                    'SBQQ__Quote__c (Id, Name, SBQQ__BillingName__c, SBQQ__BillingStreet__c, SBQQ__BillingState__c, ' + 
                    'Account_Name__c, Account_Phone__c, CreatedDate, SBQQ__Status__c, Number_of_quote_lines__c, Completed__c WHERE Number_of_quote_lines__c != 0 ORDER BY {3} {4})';
    
    private static final String SOSL_DEALER_TEMPLATE_R6 = 
        'FIND {0} IN ALL FIELDS RETURNING ' +
            'Web_Account__c (Id, Name, Billing_City__c, RCID__c, Billing_Province__c, Phone__c ORDER BY {1} {2}), ' +
            'SBQQ__Quote__c (Id, Name, SBQQ__BillingName__c, SBQQ__BillingStreet__c, SBQQ__BillingState__c, ' + 
            'Account_Name__c, Account_Phone__c, CreatedDate, SBQQ__Status__c, Number_of_quote_lines__c, Completed__c WHERE Number_of_quote_lines__c != 0 AND ' +
            'Dealership_ID__c = \'' + QuotePortalUtils.getDealershipId() + '\' ORDER BY {3} {4})';
    
    private static final String SOSL_DEALER_TEMPLATE = 
        'FIND {0} IN ALL FIELDS RETURNING ' +
            'Web_Account__c (Id, Name, Billing_City__c, RCID__c, Billing_Province__c, Phone__c WHERE Dealership_ID__c = \'' + QuotePortalUtils.getDealershipId() + '\' ORDER BY {1} {2}), ' +
            'SBQQ__Quote__c (Id, Name, SBQQ__BillingName__c, SBQQ__BillingStreet__c, SBQQ__BillingState__c, ' + 
            'Account_Name__c, Account_Phone__c, CreatedDate, SBQQ__Status__c, Number_of_quote_lines__c, Completed__c WHERE Number_of_quote_lines__c != 0 AND ' +
            'Dealership_ID__c = \'' + QuotePortalUtils.getDealershipId() + '\' ORDER BY {3} {4})';
    
    public String searchTerm {get; set;}
    
    public List<QuoteVO> quotes {get; private set;}
    public List<Web_Account__c> accounts {get; private set;}
    
    public String quoteSortField {get; set;}
    public Boolean quoteSortAsc {get; private set;}
    public String accountSortField {get; set;}
    public Boolean accountSortAsc {get; private set;}
    public Boolean dealerUser {get{return QuotePortalUtils.isUserDealer();}}
    
    public string portalURL {get{
        return Site.getCurrentSiteUrl();
    }}
    
    private String lastQuoteSortField;
    private String lastAccountSortField;
    
    public QuoteSearchController() {
        quoteSortField = String.valueOf(SBQQ__Quote__c.CreatedDate);
        quoteSortAsc = false;
        accountSortField = String.valueOf(Account.CreatedDate);
        accountSortAsc = false;
        loadMostRecent();
    }
    public PageReference onSearch() {
        system.debug('Search term:'+searchTerm);
        if ((searchTerm == null) || (searchTerm.trim().length() == 0)) {
            loadMostRecent();
        } else {
            if (searchTerm.trim().length() < 2) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Your search term must be at least 2 characters long'));
            }else{
                String filter = String.escapeSingleQuotes(searchTerm.trim()) + '*';
                system.debug('filter:'+filter);
                String sosl = SOSL_TEMPLATE;
                system.debug('sosl:'+sosl);
                system.debug('is user dealer:'+QuotePortalUtils.isUserDealer());
                if (QuotePortalUtils.isUserDealer()) {
                    Quote_Portal__c qp = Quote_Portal__c.getInstance();
                    if (qp.Allow_Dealers_To_View_All_Accounts__c) {
                        sosl = SOSL_DEALER_TEMPLATE_R6;
                    } else {
                        sosl = SOSL_DEALER_TEMPLATE;
                    }
                }
                sosl = sosl.replace('{1}', accountSortField);
                sosl = sosl.replace('{2}', accountSortAsc ? 'ASC' : 'DESC');
                sosl = sosl.replace('{3}', quoteSortField);
                sosl = sosl.replace('{4}', quoteSortAsc ? 'ASC' : 'DESC');
                sosl = sosl.replace('{0}', '\'' + filter + '\'');
                system.debug('sosl:'+sosl);
                List<List<SObject>> searchList = Search.query(sosl);
                system.debug('search list size:'+searchList.size());
                system.debug('search list 0 size:'+searchList[0].size());
                system.debug('search list 1 size:'+searchList[1].size());
                accounts = (Web_Account__c[])searchList[0];
                wrapQuotes((SBQQ__Quote__c[])searchList[1]);
            }
        }
        return null;
    }
    
    public PageReference onSortQuotes() {
        if (lastQuoteSortField == quoteSortField) {
            quoteSortAsc = !quoteSortAsc;
        } else {
            lastQuoteSortField = quoteSortField;
            quoteSortAsc = true;
        }
        return onSearch();
    }
    
    public PageReference onSortAccounts() {
        if (lastAccountSortField == accountSortField) {
            accountSortAsc = !accountSortAsc;
        } else {
            lastAccountSortField = accountSortField;
            accountSortAsc = true;
        }
        return onSearch();
    }
    
    private void loadMostRecent() {
        QueryBuilder qb = new QueryBuilder(SBQQ__Quote__c.sObjectType);
        qb.getSelectClause().addField(SBQQ__Quote__c.Name);
        qb.getSelectClause().addField(SBQQ__Quote__c.CreatedDate);
        qb.getSelectClause().addField(SBQQ__Quote__c.Account_Name__c);
        qb.getSelectClause().addField(SBQQ__Quote__c.Number_of_quote_lines__c);
        qb.getSelectClause().addField(SBQQ__Quote__c.Account_Phone__c);
        qb.getSelectClause().addField(SBQQ__Quote__c.SBQQ__BillingStreet__c);
        qb.getSelectClause().addField(SBQQ__Quote__c.SBQQ__BillingState__c);
        qb.getSelectClause().addField(SBQQ__Quote__c.SBQQ__Status__c);
        qb.getWhereClause().addExpression(qb.eq(SBQQ__Quote__c.SBQQ__SalesRep__c, UserInfo.getUserId()));
        
        /* Begin Traction Change - February 7, 2012 */
            //qb.getWhereClause().addExpression(qb.eq(SBQQ__Quote__c.Completed__c, true)); // Uncommented for testing.
            qb.getSelectClause().addField(SBQQ__Quote__c.Completed__c);
        /* End Traction Change - February 7, 2012 */
        
        if (QuotePortalUtils.isUserDealer()) {
            qb.getWhereClause().addExpression(qb.eq(SBQQ__Quote__c.Dealership_ID__c, QuotePortalUtils.getDealershipId()));
        }
        if (quoteSortAsc) {
            qb.getOrderByClause().addAscending(quoteSortField);
        } else {
            qb.getOrderByClause().addDescending(quoteSortField);
        }
        /* Begin Traction Change 2/14/2012 for Case TC11292 */
        qb.getLimitClause().setValue(1000); // was 25, see wrapQuotes();
        /* End Traction Change*/
        
        wrapQuotes(Database.query(qb.buildSOQL()));
        
        qb = new QueryBuilder(Web_Account__c.sObjectType);
        qb.getSelectClause().addField(Web_Account__c.Name);
        qb.getSelectClause().addField(Web_Account__c.Billing_City__c);
        qb.getSelectClause().addField(Web_Account__c.RCID__c);
        qb.getSelectClause().addField(Web_Account__c.Billing_Province__c);
        qb.getSelectClause().addField(Web_Account__c.Phone__c);
        qb.getWhereClause().addExpression(qb.eq(Web_Account__c.CreatedById, UserInfo.getUserId()));
        if (QuotePortalUtils.isUserDealer()) {
            qb.getWhereClause().addExpression(qb.eq(Web_Account__c.Dealership_ID__c, QuotePortalUtils.getDealershipId()));
        }
        if (accountSortAsc) {
            qb.getOrderByClause().addAscending(accountSortField);
        } else {
            qb.getOrderByClause().addDescending(accountSortField);
        }
        qb.getLimitClause().setValue(25);
        accounts = Database.query(qb.buildSOQL());
    }
    
    private void wrapQuotes(List<SBQQ__Quote__c> raw) {
        quotes = new List<QuoteVO>();
        /* Begin Traction Change 2/14/2012 for Case TC11292 */
        //for (SBQQ__Quote__c q : raw) {
        //    quotes.add(new QuoteVO(q));
        //}
        integer max = 25;
        integer current = 1;
        for (SBQQ__Quote__c q : raw) {
            QuoteVO qv = new QuoteVO(q);
            if (qv.hasLineItems == true && current < max) {
                quotes.add(qv);
            }
        }
        /* End Traction Change*/
    }
}