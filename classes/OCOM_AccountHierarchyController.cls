/*
###########################################################################
# Created by............: Derek
# Created Date..........: 
# Last Modified by......: Peter Hoang
# Last Modified Date....: March 11th, 2016
# Description...........: This class contains the new SVOC Account Heirarchy for Billing and Service Locations
# Change Log:    
# 1. Puneet Khosla - ICCD Changes
# 2. Peter Hoang - Remove "WTN" from Account Hierarchy 
############################################################################
 */
global without sharing class OCOM_AccountHierarchyController {
       
    global static final integer DEFAULT_PAGE_SIZE = 25;
    
    private static integer m_pageSize = 0;
    
    global static integer pageSize {
        get {
            if (m_pageSize == 0) {
                m_pageSize = DEFAULT_PAGE_SIZE;
                
                smb_Application_Settings__c setting = smb_Application_Settings__c.getInstance('NumAccountsToPreloadInHierarchy');
            
                if (setting != null && setting.value__c != null) {
                    try {
                        m_pageSize = integer.valueOf(setting.value__c);
                    }
                    catch (Exception ex) {}
                }
            }
            
            return m_pageSize;
        }
    }
    
    @RemoteAction
    global static OCOM_AccountHierarchyController.rootNodeResults getRootNode(Id accountId,boolean showAll) {
        
        integer counter = 0;
    
        Id currentId = accountId;   
        
        List<AccountNode> nodes;    
        
        Set<Id> accountsIdsInPathToRoot = new Set<Id>();

        while (counter < 6 && currentId != null) {
            counter++;
            accountsIdsInPathToRoot.add(currentId);
            List<AccountNode> parents = getSiblings(currentId,showAll);
            
            if (nodes != null && parents != null) {
                for (AccountNode parent : parents) {
                    if (parent.recordId == nodes.get(0).parentId) {
                        parent.children = nodes;
                        break;
                    }
                }
            }
            
            if (parents != null && parents.size() > 0) {
                currentId = parents.get(0).parentId;
            }
            
            nodes = parents;
            
        }
        
        if (nodes == null) {
            return null;
        }

        rootNodeResults result = new rootNodeResults();
        result.rootNode = nodes.get(0);
        result.accountsIdsInPathToRoot = accountsIdsInPathToRoot;
        
        //if no parents found
        if (result.rootNode.recordId == accountId && result.rootNode.children == null) { 
            result.rootNode.children = getChildNodesBilling(accountId, 0, null,showAll);
            result.rootNode.children = getChildNodesServiceLocation(accountId, 0, null,showAll);
            result.rootNode.childrenLoaded = true;
        }
        
        return result;
    }
    
    @RemoteAction
    global static List<OCOM_AccountHierarchyController.AccountNode> getChildNodesBilling(Id parentId, Integer page, List<Id> accountsToExclude,boolean showAll) {
        Integer offset = page * pageSize;

        /*The following is used to toggle between active and inactive records in the account hierarchy tree
          If the toggle is set to show All .. then there are two components that need to be filtered out
          This is mostly for the CAN/BAN accounts. They are set through the Billing_Account_Active_indicator__c field.
          A value of "Y" means that the account is active, a N value means that it is not. 
          The Inactive RCID accounts should never be displayed, so those will always be filtered out
          */
        String activeFilter='ALL';
        if(!showAll){
           activeFilter ='N'; 
        }
       // Query Changed from ICCD to add the WTN Display and Pic Away
        List<Account> accounts = [SELECT Id, Name, parentId, RecordType.DeveloperName, 
                                         CBUCID__c, RCID__c, CustProfId__c, CBUCID_RCID__c, Billing_System_ID__c, 
                                         BTN__c, CAN__c, BAN_CAN__c, Billing_Account_Active_Indicator__c,CAN_Level__c
                                         //(SELECT Id, Name, Phone__c, AccountId,WTN_Display__c,PIC_Away__c
     
                                         //   FROM Assets
                                         //   WHERE Type__c = 'WTN'
                                         //   ORDER BY Name
                                         //   LIMIT :pageSize)
                                  FROM Account
                                  WHERE ParentId = :parentId and Billing_Account_Active_Indicator__c != :activeFilter
                                  and RecordType.Name != 'Service'
                                  and inactive__c != true
                                  ORDER BY Can_Level__c desc, Billing_System_ID__c, CBUCID__c, RCID__c, CAN__c, BAN_CAN__c, Name // ICCD code changes
                                  LIMIT :pageSize OFFSET :offset];
                                  
        List<AccountNode> nodes = new List<AccountNode>();
        
        Set<Id> excludeAccountIds = new Set<Id>();
        
        if (accountsToExclude != null)
            excludeAccountIds.addAll(accountsToExclude);
        
        for (Account account : accounts) {        
            if (excludeAccountIds.contains(account.Id)) continue;
            nodes.add(createAccountNode(account, null));
        }
        
 //       nodes.sort();
        
        return nodes;
    }

    @RemoteAction
    global static List<OCOM_AccountHierarchyController.AccountNode> getChildNodesServiceLocation(Id parentId, Integer page, List<Id> accountsToExclude,boolean showAll) {
        Integer offset = page * pageSize;

        /*The following is used to toggle between active and inactive records in the account hierarchy tree
          If the toggle is set to show All .. then there are two components that need to be filtered out
          This is mostly for the CAN/BAN accounts. They are set through the Billing_Account_Active_indicator__c field.
          A value of "Y" means that the account is active, a N value means that it is not. 
          The Inactive RCID accounts should never be displayed, so those will always be filtered out
          */
        String activeFilter='ALL';
        if(!showAll){
           activeFilter ='N'; 
        }
       // Query Changed from ICCD to add the WTN Display and Pic Away
        List<Account> accounts = [SELECT Id, Name, parentId, RecordType.DeveloperName, 
                                         CBUCID__c, RCID__c, CustProfId__c, CBUCID_RCID__c, Billing_System_ID__c, 
                                         BTN__c, CAN__c, BAN_CAN__c, Billing_Account_Active_Indicator__c,CAN_Level__c
                                         //(SELECT Id, Name, Phone__c, AccountId,WTN_Display__c,PIC_Away__c
     
                                         //   FROM Assets
                                         //   WHERE Type__c = 'WTN'
                                         //   ORDER BY Name
                                         //   LIMIT :pageSize)
                                  FROM Account
                                  WHERE ParentId = :parentId and Billing_Account_Active_Indicator__c != :activeFilter
                                  and RecordType.Name != 'CAN'
                                  and RecordType.Name != 'BAN'
                                  and RecordType.Name != 'Billing'                                  
                                  and inactive__c != true
                                  ORDER BY Can_Level__c desc, Billing_System_ID__c, CBUCID__c, RCID__c, CAN__c, BAN_CAN__c, Name // ICCD code changes
                                  LIMIT :pageSize OFFSET :offset];
                                  
        List<AccountNode> nodes = new List<AccountNode>();
        
        Set<Id> excludeAccountIds = new Set<Id>();
        
        if (accountsToExclude != null)
            excludeAccountIds.addAll(accountsToExclude);
        
        for (Account account : accounts) {        
            if (excludeAccountIds.contains(account.Id)) continue;
            nodes.add(createAccountNode(account, null));
        }
        
 //       nodes.sort();
        
        return nodes;
    }

    @RemoteAction
    global static List<OCOM_AccountHierarchyController.AccountNode> getChildAssetNodes(Id parentId, Integer page,boolean showAll) {
        Integer offset = page * pageSize;
        // Query Changed from ICCD to add the WTN Display and Pic Away
        List<Asset> assets = [SELECT Id, Name, Phone__c, AccountId,WTN_Display__c,PIC_Away__c
                              FROM Asset
                              WHERE Type__c = 'WTN'
                                    AND AccountId = :parentId 
                              ORDER BY Name
                              LIMIT :pageSize OFFSET :offset];
        
        List<AccountNode> nodes = new List<AccountNode>();
                      
        for (Asset asset : assets) {
            nodes.add(createAccountNode(asset));    
        }
        
        return nodes;
    }
    
    private static List<AccountNode> getSiblings(Id siblingId,boolean showAll) {
        String activeFilter='ALL';
        if(!showAll){
           activeFilter ='N'; 
        }
        // Query Changed from ICCD to add the CAN Level, WTN Display and Pic Away
        List<Account> siblings = [SELECT Id, Name, parentId, RecordType.DeveloperName, 
                                         CBUCID__c, RCID__c, CustProfId__c, CBUCID_RCID__c, Billing_System_ID__c, 
                                         BTN__c, CAN__c, BAN_CAN__c, Billing_Account_Active_Indicator__c, CAN_Level__c
                                         //(SELECT Id, Name, Phone__c, AccountId,WTN_Display__c,PIC_Away__c
                                         //   FROM Assets
                                         //   WHERE Type__c = 'WTN'
                                         //   ORDER BY Name
                                         //   LIMIT :pageSize)
                                      FROM Account 
                                      WHERE Id = :siblingId] ;
                                      
        if (siblings.size() != 1) return null;
        
        Account primarySibling = siblings.get(0);
        
        if (primarySibling.parentId != null) {
            // Query Changed from ICCD to add the CAN Level, WTN Display and Pic Away                          
                                      
            siblings.addAll ([SELECT Id, Name, parentId, RecordType.DeveloperName, 
                                             CBUCID__c, RCID__c, CustProfId__c, CBUCID_RCID__c, Billing_System_ID__c, 
                                             BTN__c, CAN__c, BAN_CAN__c, Billing_Account_Active_Indicator__c, CAN_Level__c
                                             //(SELECT Id, Name, Phone__c, AccountId,WTN_Display__c,PIC_Away__c
                                             //   FROM Assets
                                             //   WHERE Type__c = 'WTN'
                                             //   ORDER BY Name
                                             //   LIMIT :pageSize)
                                      FROM Account
                                      WHERE ParentId = :primarySibling.parentId
                                                AND
                                            Id != :siblingId and Billing_Account_Active_Indicator__c != :activeFilter
                                            and inactive__c != true
                                      ORDER BY Billing_System_ID__c, CBUCID__c, RCID__c, CAN__c, BAN_CAN__c, Name
                                      LIMIT :pageSize]);
                                  
        }
                                  
        List<AccountNode> nodes = new List<AccountNode>();
        
        for (Account account : siblings) {  
            nodes.add(createAccountNode(account, siblingId));
        }
        
        nodes.sort();
        
        return nodes;
    }
    
    private static AccountNode createAccountNode(Account account, Id selectedId) {
        AccountNode result = new AccountNode();
        
        result.recordId = account.Id;
        result.objectType = 'Account';
        result.parentId = account.parentId;
        result.thisOrChildselected = account.Id == selectedId;
        result.active = (account.Billing_Account_Active_Indicator__c != 'N');
        
        result.recordName = account.Name;
        result.billingSystemName = getBillingSystemName(account.Billing_System_ID__c);
        
        result.recordTypeDeveloperName = account.RecordType.DeveloperName;
        result.accountNumber = smb_AccountUtility.getAccountNumber(account);
        result.pilotName = account.BTN__c;
         
        result.primaryAttributeLabel = smb_AccountUtility.getAccountNumberShortLabel(account.RecordType.DeveloperName);
        result.primaryAttributeValue = smb_AccountUtility.getAccountNumber(account);
        
        if (account.RecordType.DeveloperName == 'CBUCID' || account.RecordType.DeveloperName == 'RCID')
        {
            //result.secondaryAttributeLabel = 'CUSTPROFID';
            //result.secondaryAttributeValue = account.CustProfID__c;
        }
        // ICCD Changes to accomodate for the CAN Levels
        else if (account.RecordType.DeveloperName == 'CAN') {
          
            if(String.IsNotBlank(account.CAN_Level__c))
            {
              system.debug('account.CAN_Level__c ## ' + account.CAN_Level__c + ' : ' + account.Name);
                result.primaryAttributeLabel = account.CAN_Level__c;
                result.canAccountLevel = account.CAN_Level__c;
                if(account.CAN_Level__c.equalsIgnoreCase('CAN'))
                {
                    result.secondaryAttributeLabel = 'BTN';
                    //result.billingSystemName = '';
                }
                else
                {
                    result.secondaryAttributeLabel = 'CBN';
                    if(account.CAN_Level__c.containsIgnoreCase('Pilot BCAN')){//
                        result.recordName ='';
                        //result.primaryAttributeLabel = 'Pilot BCAN';
                    }
                    //else if(account.CAN_Level__c.containsIgnoreCase('Org CAN'))
                        //result.billingSystemName = '';
                }
            }
            //else
                //result.secondaryAttributeLabel = 'BTN';
            result.secondaryAttributeValue = account.BTN__c;
        }
        else if(account.RecordType.DeveloperName == 'BAN')
        {
            result.secondaryAttributeLabel = 'BTN';
            result.secondaryAttributeValue = account.BTN__c;
        }
        // ICCD Changes end
     
        if (account.assets.size() > 0) {
            result.children = new List<AccountNode>();

            for (Asset asset : account.assets) {
                result.children.add(createAccountNode(asset));
            }
        }
        
        return result;
    }
    
    private static String getBillingSystemName(String billingSystemId){
        String billingSystemName = '';
        
        if (billingSystemId != null){           
            LEGACY_ACCOUNT_ID__c legacyAccountId = LEGACY_ACCOUNT_ID__c.getValues(billingSystemId);    
            if(legacyAccountId!=null) billingSystemName = legacyAccountId.Billing_System_Name__c;
        } 
        return billingSystemName;       
    }
    
    private static AccountNode createAccountNode(Asset asset) {
        AccountNode result = new AccountNode();
        
        result.recordId = asset.Id;
        result.objectType = 'Asset';
        result.parentId = asset.AccountId;
        
        //result.recordNamePrefix = 'WTN';
        //// ICCD Changes to determine icon visibility
        //result.recordName = asset.WTN_Display__c;
        
        if(String.IsBlank(asset.PIC_Away__c) || (!(asset.PIC_Away__c.equalsIgnoreCase('Yes'))))
            result.hideIcon = true;
        // Changes end
        
        return result;
    }
    
    global class rootNodeResults {
        global Set<Id> accountsIdsInPathToRoot {get;set;}
        global AccountNode rootNode {get;set;}
    }
    
    global class AccountNode implements Comparable {
        global List<AccountNode> children {get;set;}
        global boolean childrenLoaded {get;set;}
        
        global Id recordId {get;set;}
        global string objectType {get;set;}
        global Id parentId {get;set;}
        
        global boolean active {get;set;}
        
        global string recordTypeDeveloperName {get;set;}
        global string accountNumber {get;set;}
        global string pilotName {get;set;}
        
        global boolean thisOrChildselected {get;set;}
        
        global string recordNamePrefix {get;set;}
        global string recordName {get;set;}
        
        global string billingSystemName {get;set;}        
        
        global string primaryAttributeLabel {get;set;}
        global string primaryAttributeValue {get;set;}
        
        global string secondaryAttributeLabel {get;set;}
        global string secondaryAttributeValue {get;set;}
        
        // ICCD Changes
        global string canAccountLevel{get;set;}
        global boolean hideIcon{get;set;}
        // Changes end
        
        global AccountNode() {
            this.childrenLoaded = false;
            this.thisOrChildselected = false;
            
            this.objectType = '';
            
            this.recordTypeDeveloperName = '';
            this.accountNumber = '';
            this.pilotName = '';
            
            this.recordNamePrefix = '';
            this.recordName = '';
            
            this.billingSystemName = '';            
            
            this.primaryAttributeLabel = '';
            this.primaryAttributeValue = '';
        
            this.secondaryAttributeLabel = '';
            this.secondaryAttributeValue = '';
            
            // ICCD Changes
            this.canAccountLevel = '';
            this.hideIcon = false;
            // Changes End
        }
        
        global Integer compareTo(Object compareTo) {
            AccountNode item = (AccountNode)compareTo;
            
            if (this.thisOrChildselected && !item.thisOrChildselected) return -1;
            
            if (!this.thisOrChildselected && item.thisOrChildselected) return 1;
            
            return this.recordName.compareTo(item.recordName);
        }
    }
    
}