/**
 * Utility class that logs arbitrary content to a custom Repair_Log__c SObject
 * Part of the SMB Repairs project
 *
 * @author: Grant Adamson, Traction On Demand 
 */
 
public without sharing class trac_RepairLogger {
		
	@future
	public static void log(String typeStr, String message, Id caseId) {
		Repair_Log__c rl = new Repair_Log__c(Type__c = typeStr, Message__c = message);
		
		if(caseId != null) {
			rl.case__c = caseId;
		}
		
		try {
			insert rl;
		} catch (Exception e) {} // discard any exceptions, don't want the logging code to break other functionality
	}
	
}