// Trigger handler in a structure that provides easier way to refactor everytime new code is added.
public class BulkOrderLoadTriggerHandler {
    map<id,schema.recordtypeinfo> recordTypeInfoMap=bulk_order_load_file__c.sobjecttype.getdescribe().getrecordtypeinfosbyid();
    public BulkOrderLoadTriggerHandler(){
        if(trigger.isexecuting){
            if(trigger.isbefore){
                beforeInit();
                if(trigger.isinsert){
                    beforeInsert();
                }else if(trigger.isupdate){
                    beforeUpdate();
                }
            }else if(trigger.isafter){
                afterInit();
                if(trigger.isinsert){
                    afterInsert();
                }else if(trigger.isupdate){
                    afterUpdate();
                }
            }
        }
    }
    //Shared initializations in all BEFORE events.
    void beforeInit(){}
    //Shared initiatizations in all AFTER events.
    void afterInit(){}
    void beforeInsert(){}
    void afterInsert(){}
    void beforeUpdate(){}
    void afterUpdate(){
        updateServiceForBulkSubmission();
    }
    void updateServiceForBulkSubmission(){
        set<id> bulkOrderLoadOpportunityIdSet=new set<id>();
        for(sobject sobj:trigger.new){
            bulk_order_load_file__c newBulkOrderLoad=(bulk_order_load_file__c)sobj;
            bulk_order_load_file__c oldBulkOrderLoad=(bulk_order_load_file__c)trigger.oldmap.get(newBulkOrderLoad.id);
            if(oldBulkOrderLoad!=null&&oldBulkOrderLoad.submission_status__c.equalsignorecase('sr creation in progress')&&newBulkOrderLoad.submission_status__c.equalsignorecase('complete')){
                bulkOrderLoadOpportunityIdSet.add(newBulkOrderLoad.opportunity__c);
            }
        }
        list<service_request__c> serviceRequests=database.query('select id,name,reason_to_send_code__c,outstanding_issues__c,Service_Request_Status__c from service_request__c where opportunity__c=:bulkOrderLoadOpportunityIdSet limit 50000 for update');
        system.debug('bulkOrderLoadOpportunityIdSet:'+bulkOrderLoadOpportunityIdSet.size()+', queried service requets:'+serviceRequests.size());
        list<service_request__c> updateServiceRequests=new list<service_request__c>();
        for(service_request__c serviceRequest:serviceRequests){
            system.debug('service request num:'+serviceRequest.name+', status:'+serviceRequest.Service_Request_Status__c+', outstanding issues:'+serviceRequest.outstanding_issues__c);
            if(string.isblank(serviceRequest.outstanding_issues__c)){
                if(serviceRequest.Service_Request_Status__c.equals('Originated')){
                    serviceRequest.reason_to_send_code__c='\n Bulk Submission';
                    updateServiceRequests.add(serviceRequest);
                }
            }
        }
        system.debug('updateServiceRequests:'+updateServiceRequests.size());
        if(!updateServiceRequests.isempty()){
            update updateServiceRequests;
        }
    }
}