@isTest
public class QuoteCreditCheckControllerTest {

	private static SBQQ__Quote__c quote;
	 
	static testMethod void test() {
		setUp();
		
	   	ApexPages.currentPage().getParameters().put('qid', quote.Id);
	   	ApexPages.currentPage().getParameters().put('summaryUrl', '');
		QuoteCreditCheckController controller = new QuoteCreditCheckController();
		controller.onInit();
		controller.onContinue();	
	}

	private static void setUp() {
		Web_Account__c account = new Web_Account__c(Name='UnitTest');
		insert account;
		
		Opportunity opp = new Opportunity(Name='Test',Web_Account__c=account.Id,CloseDate=System.today(),StageName='Prospecting');
		insert opp;
		
		quote = new SBQQ__Quote__c(SBQQ__Opportunity__c=opp.Id);
		quote.Rate_Band__c = 'F';
		quote.Province__c = 'BC';
		insert quote;
	}
}