@isTest
private without sharing class MBRCaseDetailCtlr_Test {

    static Case testCase {get;set;}

    static {
        insert new Customer_Interface_Settings__c( 
            Manage_Requests_List_Limit__c = 10,
            Overview_Case_Limit__c = 5,
            Case_Origin_Value__c = 'My Business Requests'
        );

        /* VIRILcare portal */
        insert new VITILcare_Customer_Interface_Settings__c( 
            Manage_Requests_List_Limit__c = 10,
            Overview_Case_Limit__c = 5,
            Case_Origin_Value__c = 'VITILcare'
        );
        
        /*
        Case c = new Case(Subject = 'Test Subject', Origin = 'My Business Requests', My_Business_Requests_Type__c = 'Dummy', RecordTypeId = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId);
        insert c;
        testCase = [SELECT Id, Subject, RecordTypeId, My_Business_Requests_Type__c, CaseNumber, Status, LastModifiedDate FROM Case WHERE Id = :c.Id LIMIT 1];
        */

        MBRTestUtils.createExternalToInternalCustomSettings();
        MBRTestUtils.createParentToChildCustomSettings();
    }
	
	@isTest static void testMBRCaseDetailPage1() {
		
        account a = new account(name = 'test acct');
        insert a;
        /*
        List<contact > cons= new List<contact >();
        contact ct = new contact(email='test@test.com', lastname = 'test', accountid = a.id);
        cons.add(ct);
        contact ct2 = new contact(email='test2@test.com', lastname = 'test', accountid = a.id);
        cons.add(ct2);
        insert cons;
        profile p = [select id from profile where name = 'Customer Community User' limit 1];
        List<user> users = new List<user>();

        User u = MBRTestUtils.createPortalUser('');
        */

        List<Case> testCases = MBRTestUtils.createCCICases(2, a.Id);

        MBRTestUtils.createAttachment(testCases[0].Id, 1, 'jpg');
        List<Attachment> caseAttachments = [SELECT Id, Name, CreatedDate, LastModifiedDate FROM Attachment WHERE ParentId = :testCases[0].Id];
        EmailMessage eMsg = MBRTestUtils.createEmail(testCases[0].Id, true, 'test incoming email ');
        EmailMessage eMsg2 = MBRTestUtils.createEmail(testCases[0].Id, false, 'test outbound email ');
        //List<Attachment> eMsgAttachments = MBRTestUtils.createAttachment(eMsg.Id, 1, 'png');

        MBRCaseDetailCtlr.AttachmentWrapper attWrap = new MBRCaseDetailCtlr.AttachmentWrapper( caseAttachments[0] );

        caseAttachments[0].Name = 'test.zip.' + caseAttachments[0].Name;
        update caseAttachments[0];
        attWrap = new MBRCaseDetailCtlr.AttachmentWrapper( caseAttachments[0] );        
        
        List<Case> caseList = [SELECT createdby.firstname, createdby.lastname, NotifyCustomer__c, NotifyCollaboratorString__c, OwnerId, Resolution_Details__c, My_Business_Requests_Type__c, ParentId, isClosed, Contact.FirstName, Contact.LastName, id,contact.name,subject,description,lastmodifieddate,case_resolution__c,createddate, caseNumber, recordType.name, account.name, type, status, priority, isEscalated, Escalation_History__c, (select Active__c,Collaborator_Email__c, Collaborator_Name__c, Role__c from Collaborator__r order by createddate desc),(select createdby.firstname, createdby.lastname,Id,Name,Description,ParentId,CreatedDate,LastModifiedDate, ContentType from Attachments order by LastModifiedDate asc), (select createdby.firstname, createdby.lastname,Id,CommentBody,ParentId,CreatedDate,LastModifiedDate from casecomments where IsPublished = true order by CreatedDate asc), (select id, toaddress,fromaddress,fromname,ccaddress,messagedate,subject,htmlbody,textbody,hasattachment from emailmessages) FROM Case WHERE Id = :testCases[0].Id];
        Case cs = caseList[0];
        Case cs1 = testCases[1];

        cs.ParentId = cs1.Id;
        update cs;

        System.debug('cs.CaseNumber: ' + cs.CaseNumber);

        PageReference pageRef = Page.MBRCaseDetail;
        pageRef.getParameters().put('caseNumber', cs.CaseNumber);
        Test.setCurrentPage( pageRef );

        Test.startTest();

        MBRCaseDetailCtlr ctlr = new MBRCaseDetailCtlr();

        String caseStatus = ctlr.getCaseStatus();

        String caseType = ctlr.getCaseType();

        List<String> descr = ctlr.getDescription();

        String durSince = ctlr.getDurationSinceSubmitted();
        durSince = MBRCaseDetailCtlr.calculateDurationSince(1000000000, 2000000000);
        System.debug('durSince: ' + durSince);

        ctlr.getCollaboratorsList();
        ctlr.getEmailList();
        ctlr.getCommentList();
        ctlr.updateSubscription();

        ctlr.newCollab.Collaborator_Email__c = null;
        ctlr.addCollaborator();
        ctlr.newCollab.Collaborator_Email__c = 'test1@collaborator.com';
        ctlr.addCollaborator();

        ctlr.onLoad();
        ctlr.addCollaborator();

        CaseComment cmt = ctlr.getNewComment();
        Collaborators__c clb = ctlr.getNewCollab();
        ctlr.empty();

        ctlr.upload();

        //MBRCaseDetailCtlr.changeCollabStatus(cs.Id, 0);

        ctlr.getCollaboratorWithIndex();

        ctlr.escalateCase();

        pageRef.getParameters().put('caseId', cs.Id);
        ctlr.unsubscribe();

        ctlr.onLoad();

        List<MBRCaseDetailCtlr.AttachmentWrapper> attWraps = ctlr.getAttachmentList();

        Test.stopTest();

	}
	

    @isTest static void testMBRCaseDetailPage2() {
        
        //account a = new account(name = 'test acct');
        //insert a;

        User u = MBRTestUtils.createPortalUser('');
        System.runAs(u) {

            Account a = [SELECT Id FROM Account LIMIT 1];

            List<Case> testCases = MBRTestUtils.createCCICases(1, a.Id);

            List<Case> caseList = [SELECT createdby.firstname, createdby.lastname, NotifyCustomer__c, NotifyCollaboratorString__c, OwnerId, Resolution_Details__c, My_Business_Requests_Type__c, ParentId, isClosed, Contact.FirstName, Contact.LastName, id,contact.name,subject,description,lastmodifieddate,case_resolution__c,createddate, caseNumber, recordType.name, account.name, type, status, priority, isEscalated, Escalation_History__c, (select Active__c,Collaborator_Email__c, Collaborator_Name__c, Role__c from Collaborator__r order by createddate desc),(select createdby.firstname, createdby.lastname,Id,Name,Description,ParentId,CreatedDate,LastModifiedDate, ContentType from Attachments order by LastModifiedDate asc), (select createdby.firstname, createdby.lastname,Id,CommentBody,ParentId,CreatedDate,LastModifiedDate from casecomments where IsPublished = true order by CreatedDate asc), (select id, toaddress,fromaddress,fromname,ccaddress,messagedate,subject,htmlbody,textbody,hasattachment from emailmessages) FROM Case ORDER BY CreatedDate DESC];
            //Case cs1 = caseList[0];
            Case cs = caseList[0];

            //cs.ParentId = cs1.Id;
            //update cs;

            System.debug('cs.CaseNumber: ' + cs.CaseNumber);

            PageReference pageRef = Page.MBRCaseDetail;
            pageRef.getParameters().put('caseNumber', cs.CaseNumber);
            Test.setCurrentPage( pageRef );

            Test.startTest();

            MBRCaseDetailCtlr ctlr = new MBRCaseDetailCtlr();

            String testInput = ctlr.testInput;

            ctlr.newComment.commentBody = 'test new case comment';
            ctlr.addComment();

            CaseComment caseCmt = [SELECT Id, CommentBody, CreatedBy.FirstName, CreatedBy.LastName, CreatedDate FROM CaseComment LIMIT 1];
            System.debug('caseCmt: ' + caseCmt);
            //CaseComment caseCmt = new CaseComment(CommentBody = 'test comment body', ParentId = cs.Id);
            //insert caseCmt;
            MBRCaseDetailCtlr.CommentWrapper wrappedCmt = new MBRCaseDetailCtlr.CommentWrapper( caseCmt );

            caseCmt.commentBody = null;
            wrappedCmt = new MBRCaseDetailCtlr.CommentWrapper( caseCmt );

            ctlr.onLoad();
            List<MBRCaseDetailCtlr.CommentWrapper> cmtWrappers = ctlr.getCommentWrapperList();

            ctlr.closeReason = 'testing close case';
            ctlr.closeCase();

            ctlr.newComment.commentBody = 'test new case comment';
            ctlr.addComment();

            ctlr.reopenCase();

            Test.stopTest();
        }
    }
    
    
    /* VITILcare portal */
	@isTest static void VITILcare_testMBRCaseDetailPage1() {
		
        account a = new account(name = 'test acct');
        insert a;
        /*
        List<contact > cons= new List<contact >();
        contact ct = new contact(email='test@test.com', lastname = 'test', accountid = a.id);
        cons.add(ct);
        contact ct2 = new contact(email='test2@test.com', lastname = 'test', accountid = a.id);
        cons.add(ct2);
        insert cons;
        profile p = [select id from profile where name = 'Customer Community User' limit 1];
        List<user> users = new List<user>();

        User u = MBRTestUtils.createPortalUser('');
        */

        List<Case> testCases = MBRTestUtils.createVITILcareCases(2, a.Id);

        MBRTestUtils.createAttachment(testCases[0].Id, 1, 'jpg');
        List<Attachment> caseAttachments = [SELECT Id, Name, CreatedDate, LastModifiedDate FROM Attachment WHERE ParentId = :testCases[0].Id];
        EmailMessage eMsg = MBRTestUtils.createEmail(testCases[0].Id, true, 'test incoming email ');
        EmailMessage eMsg2 = MBRTestUtils.createEmail(testCases[0].Id, false, 'test outbound email ');
        //List<Attachment> eMsgAttachments = MBRTestUtils.createAttachment(eMsg.Id, 1, 'png');

        VITILcareCaseDetailCtlr.AttachmentWrapper attWrap = new VITILcareCaseDetailCtlr.AttachmentWrapper( caseAttachments[0] );

        caseAttachments[0].Name = 'test.zip.' + caseAttachments[0].Name;
        update caseAttachments[0];
        attWrap = new VITILcareCaseDetailCtlr.AttachmentWrapper( caseAttachments[0] );        
        
        List<Case> caseList = [SELECT createdby.firstname, createdby.lastname, NotifyCustomer__c, NotifyCollaboratorString__c, OwnerId, Resolution_Details__c, My_Business_Requests_Type__c, ParentId, isClosed, Contact.FirstName, Contact.LastName, id,contact.name,subject,description,lastmodifieddate,case_resolution__c,createddate, caseNumber, recordType.name, account.name, type, status, priority, isEscalated, Escalation_History__c, (select Active__c,Collaborator_Email__c, Collaborator_Name__c, Role__c from Collaborator__r order by createddate desc),(select createdby.firstname, createdby.lastname,Id,Name,Description,ParentId,CreatedDate,LastModifiedDate, ContentType from Attachments order by LastModifiedDate asc), (select createdby.firstname, createdby.lastname,Id,CommentBody,ParentId,CreatedDate,LastModifiedDate from casecomments where IsPublished = true order by CreatedDate asc), (select id, toaddress,fromaddress,fromname,ccaddress,messagedate,subject,htmlbody,textbody,hasattachment from emailmessages) FROM Case WHERE Id = :testCases[0].Id];
        Case cs = caseList[0];
        Case cs1 = testCases[1];

        cs.ParentId = cs1.Id;
        update cs;

        System.debug('cs.CaseNumber: ' + cs.CaseNumber);

        PageReference pageRef = Page.VITILcareCaseDetail;
        pageRef.getParameters().put('caseNumber', cs.CaseNumber);
        Test.setCurrentPage( pageRef );

        Test.startTest();

        VITILcareCaseDetailCtlr ctlr = new VITILcareCaseDetailCtlr();
        String caseStatus = ctlr.getCaseStatus();

        String caseType = ctlr.getCaseType();

        List<String> descr = ctlr.getDescription();

        String durSince = ctlr.getDurationSinceSubmitted();
        durSince = VITILcareCaseDetailCtlr.calculateDurationSince(1000000000, 2000000000);
        System.debug('durSince: ' + durSince);

        ctlr.getCollaboratorsList();
        ctlr.getEmailList();
        ctlr.getCommentList();
        ctlr.updateSubscription();

        ctlr.newCollab.Collaborator_Email__c = null;
        ctlr.addCollaborator();
        ctlr.newCollab.Collaborator_Email__c = 'test1@collaborator.com';
        ctlr.addCollaborator();

        ctlr.onLoad();
        ctlr.addCollaborator();

        CaseComment cmt = ctlr.getNewComment();
        Collaborators__c clb = ctlr.getNewCollab();
        ctlr.empty();

        ctlr.upload();

        ctlr.getCollaboratorWithIndex();

        ctlr.escalateCase();

        pageRef.getParameters().put('caseId', cs.Id);
        ctlr.unsubscribe();

        ctlr.onLoad();

        List<VITILcareCaseDetailCtlr.AttachmentWrapper> attWraps = ctlr.getAttachmentList();
        
        Test.stopTest();

	}
	

    @isTest static void VITILcare_testMBRCaseDetailPage2() {
        
        //account testAccount = new account(name = 'test acct');
        //insert testAccount;

        User u = MBRTestUtils.createPortalUser('VITILcare');
//        System.runAs(u) {

            Account a = [SELECT Id FROM Account LIMIT 1];

            List<Case> testCases = MBRTestUtils.createVITILcareCases(1, a.Id);

System.debug('testCases: ' + testCases);
            
//            List<Case> caseList = [SELECT id FROM Case ORDER BY CreatedDate DESC];
            List<Case> caseList = [SELECT createdby.firstname, createdby.lastname, NotifyCustomer__c, NotifyCollaboratorString__c, OwnerId, Resolution_Details__c, My_Business_Requests_Type__c, ParentId, isClosed, Contact.FirstName, Contact.LastName, id,contact.name,subject,description,lastmodifieddate,case_resolution__c,createddate, caseNumber, recordType.name, account.name, type, status, priority, isEscalated, Escalation_History__c, (select Active__c,Collaborator_Email__c, Collaborator_Name__c, Role__c from Collaborator__r order by createddate desc),(select createdby.firstname, createdby.lastname,Id,Name,Description,ParentId,CreatedDate,LastModifiedDate, ContentType from Attachments order by LastModifiedDate asc), (select createdby.firstname, createdby.lastname,Id,CommentBody,ParentId,CreatedDate,LastModifiedDate from casecomments where IsPublished = true order by CreatedDate asc), (select id, toaddress,fromaddress,fromname,ccaddress,messagedate,subject,htmlbody,textbody,hasattachment from emailmessages) FROM Case ORDER BY CreatedDate DESC];
System.debug('VITILcare_testMBRCaseDetailPage2: ' + caseList);
            //Case cs1 = caseList[0];
            Case cs = caseList[0];

            //cs.ParentId = cs1.Id;
            //update cs;

            System.debug('cs.CaseNumber: ' + cs.CaseNumber);

            PageReference pageRef = Page.VITILcareCaseDetail;
            pageRef.getParameters().put('caseNumber', cs.CaseNumber);
            Test.setCurrentPage( pageRef );

            Test.startTest();

            VITILcareCaseDetailCtlr ctlr = new VITILcareCaseDetailCtlr();

            String testInput = ctlr.testInput;

            ctlr.newComment.commentBody = 'test new case comment';
            ctlr.addComment();

            CaseComment caseCmt = [SELECT Id, CommentBody, CreatedBy.FirstName, CreatedBy.LastName, CreatedDate FROM CaseComment LIMIT 1];
            System.debug('caseCmt: ' + caseCmt);
            //CaseComment caseCmt = new CaseComment(CommentBody = 'test comment body', ParentId = cs.Id);
            //insert caseCmt;
            VITILcareCaseDetailCtlr.CommentWrapper wrappedCmt = new VITILcareCaseDetailCtlr.CommentWrapper( caseCmt );

            caseCmt.commentBody = null;
            wrappedCmt = new VITILcareCaseDetailCtlr.CommentWrapper( caseCmt );

            ctlr.onLoad();
            List<VITILcareCaseDetailCtlr.CommentWrapper> cmtWrappers = ctlr.getCommentWrapperList();

            ctlr.closeReason = 'testing close case';
            ctlr.closeCase();

            ctlr.newComment.commentBody = 'test new case comment';
            ctlr.addComment();

            ctlr.reopenCase();

            Test.stopTest();
//        }
    }
    
}