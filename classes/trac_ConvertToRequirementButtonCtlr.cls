public with sharing class trac_ConvertToRequirementButtonCtlr {
    private Id requestId;
    
    public trac_ConvertToRequirementButtonCtlr() {
        requestId = System.currentPageReference().getParameters().get('id') ;
    }
    
    public PageReference doIt() {   
        SMET_Request__c request = [SELECT OwnerId, Subject__c, Priority__c, SMET_Requirement__c, Description__c
                                   FROM SMET_Request__c
                                   WHERE id = :requestId LIMIT 1];
        
        String priority; // priority picklist values are different on the two objects...
        if(request.Priority__c == 'Urgent' || request.Priority__c == 'High') {
            priority = 'Critical';
        } else {
            priority = 'Standard';
        }

        SMET_Requirement__c requirement = new SMET_Requirement__c(name = request.subject__c,
                                                                  status__c = 'New',
                                                                  Order__c = 0,
                                                                  priority__c = priority,
                                                                  requirement_detail__c =
                                                                  request.description__c,
                                                                  OwnerId = request.OwnerId);
        insert requirement;
        
        request.SMET_Requirement__c = requirement.id;
        update request;
        
        return new PageReference('/' + requirement.id);
    }
    
    static testMethod void testConverter() {
        // Retrieve two profiles, for the standard user and the system   
    
      // administrator, then populate a map with them.
      Map<String,ID> profiles = new Map<String,ID>();
      List<Profile> ps = [select id, name from Profile where name = 
         'Standard User' or name = 'System Administrator'];
      for(Profile p : ps){
         profiles.put(p.name, p.id);
      }

      // Create the users to be used in this test.  
      // First make a new user.

      User standard = new User(alias = 'standt', 
      email='standarduser123123@testorg.com', 
      emailencodingkey='UTF-8',
      phone = '6049969449',
      lastname='Testing', languagelocalekey='en_US', 
      localesidkey='en_US', 
      profileid = profiles.get('Standard User'), 
      timezonesidkey='America/Los_Angeles', 
      username='standarduser123123@testorg.com');

      insert standard;

      // Then instantiate a user from an existing profile  
    

      User admin = new User(alias = 'admint1', 
      email='adminuser123123@testorg.com', 
      emailencodingkey='UTF-8', 
      lastname='TestingAdmin', languagelocalekey='en_US', 
      localesidkey='en_US', 
      profileid = profiles.get('System Administrator'), 
      timezonesidkey='America/Los_Angeles', 
      phone = '6049969449',
      username='adminuser123123@testorg.com');

      insert admin;
      system.runas(admin) {
        SMET_Request__c req = new SMET_Request__c(Subject__c = 'Test Subject', Priority__c = 'Medium', Description__c = 'Test description', Ownerid = userinfo.getuserid());
        insert req;
        System.currentPageReference().getParameters().put('id', req.Id);
        trac_ConvertToRequirementButtonCtlr ctlr = new trac_ConvertToRequirementButtonCtlr();
        PageReference p = ctlr.doit();
        System.assertNotEquals(p, null);
      }
      system.runas(standard) {  
        SMET_Request__c req2 = new SMET_Request__c(Subject__c = 'Test Subject', Priority__c = 'High', Description__c = 'Test description', Ownerid = userinfo.getuserid());
        insert req2;
        System.currentPageReference().getParameters().put('id', req2.Id);
        trac_ConvertToRequirementButtonCtlr ctlr2 = new trac_ConvertToRequirementButtonCtlr();
        PageReference p2 = ctlr2.doit();
        System.assertNotEquals(p2, null);
      }
    }

}