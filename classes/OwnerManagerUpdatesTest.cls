@isTest
private class OwnerManagerUpdatesTest {
    
    private static testMethod void testSetManagers() {
    	final Id PROFILE_ID = [SELECT Id FROM Profile WHERE name = 'Standard User' LIMIT 1].id;
    	
    	User u = new User(
    		username = 'VMDFJREFSRI@TRACTIONSM.COM',
      	email = 'test@example.com',
	      title = 'test',
	      lastname = 'test',
	      alias = 'test',
	      TimezoneSIDKey = 'America/Los_Angeles',
	      LocaleSIDKey = 'en_US',
	      EmailEncodingKey = 'UTF-8',
	      ProfileId = PROFILE_ID,
	      LanguageLocaleKey = 'en_US',
	      Manager_1__c = 'test1',
	      Manager_2__c = 'test2',
	      Manager_3__c = 'test3',
	      Manager_4__c = 'test4'
	    );
      insert u;
    	
    	List<Lead> leads = new List<Lead>{ new Lead(lastname = 'test', company = 'test', ownerid = u.id) };
    	
    	OwnerManagerUpdates.setManagers(leads, new Set<Id>{ u.id });
    	
    	system.assertEquals('test1', leads[0].Manager_1__c);
    	system.assertEquals('test2', leads[0].Manager_2__c);
    	system.assertEquals('test3', leads[0].Manager_3__c);
    	system.assertEquals('test4', leads[0].Manager_4__c);
    }
    
    private static testMethod void testSetManagersOwnedByQueue() {
    	Group g = new Group(name = 'test', type = 'Queue');
    	insert g;
    	
    	List<Lead> leads = new List<Lead>{
    		new Lead(
    			lastname = 'test',
    			company = 'test',
    			ownerid = g.id,
    			Manager_1__c = 'test1',
	      	Manager_2__c = 'test2',
	      	Manager_3__c = 'test3',
	      	Manager_4__c = 'test4'
    		)
    	};
    	
    	OwnerManagerUpdates.setManagers(leads, new Set<Id>{ UserInfo.getUserId() });
    	
    	system.assertEquals(null, leads[0].Manager_1__c);
    	system.assertEquals(null, leads[0].Manager_2__c);
    	system.assertEquals(null, leads[0].Manager_3__c);
    	system.assertEquals(null, leads[0].Manager_4__c);
    	
    }
    
    
    @isTest(SeeAllData=true) // required due to ADRA lead trigger
    private static void testUpdateLeads() {
    	final Id PROFILE_ID = [SELECT Id FROM Profile WHERE name = 'Standard User' LIMIT 1].id;
    	
    	User u = new User(
    		username = 'VMDFJREFSRI@TRACTIONSM.COM',
      	email = 'test@example.com',
	      title = 'test',
	      lastname = 'test',
	      alias = 'test',
	      TimezoneSIDKey = 'America/Los_Angeles',
	      LocaleSIDKey = 'en_US',
	      EmailEncodingKey = 'UTF-8',
	      ProfileId = PROFILE_ID,
	      LanguageLocaleKey = 'en_US',
	      Manager_1__c = 'test1',
	      Manager_2__c = 'test2',
	      Manager_3__c = 'test3',
	      Manager_4__c = 'test4'
	    );
      insert u;
    	
    	Lead l = new Lead(lastname = 'test', company = 'test', ownerid = u.id);
    	insert l;
    	
    	Test.startTest();
    	
    	OwnerManagerUpdates.updateLeads(new Set<Id>{ u.id });
    	
    	Test.stopTest();
    	
    	l = [SELECT Manager_1__c, Manager_2__c, Manager_3__c, Manager_4__c FROM Lead WHERE Id = :l.id];
    	
    	system.assertEquals('test1', l.Manager_1__c);
    	system.assertEquals('test2', l.Manager_2__c);
    	system.assertEquals('test3', l.Manager_3__c);
    	system.assertEquals('test4', l.Manager_4__c);
    }
}