/**
* @author Alex Kong - akong@tractionondemand.com
* @description Unit Test for MBRCommunityInviteCtlr.cls
*/
@isTest(SeeAllData=false)
private class MBRCommunityInviteCtlr_Test {

	@isTest static void testCommunityInviteCtlr() {
		Account a = new Account(Name = 'Unit Test Account');
		insert a;

		// Fields: 'Name', 'FirstName', 'LastName', 'Email', 'AccountId', 'CanAllowPortalSelfReg'
		Contact c = new Contact(Phone = '6045551212', FirstName = 'Testy', LastName = 'Tester', CanAllowPortalSelfReg = false);
		insert c;

		Test.startTest();

		ApexPages.StandardController stdController = new ApexPages.StandardController(c);
		MBRCommunityInviteCtlr ctlr = new MBRCommunityInviteCtlr(stdController);

		// test with email set
		c.Email = 'test@unit-test.com';
		update c;

		stdController = new ApexPages.StandardController(c);
		ctlr = new MBRCommunityInviteCtlr(stdController);
		System.assertEquals('Contact is missing an account.', ctlr.errorMsg);

		// test with accountId set
		c.AccountId = a.Id;
		update c;

		stdController = new ApexPages.StandardController(c);
		ctlr = new MBRCommunityInviteCtlr(stdController);
		System.assertEquals('Contact must be linked to the RCID account', ctlr.errorMsg);

		// test with recordType RCID set
		List<RecordType> rts = [SELECT Id, Name FROM RecordType WHERE Name = 'RCID'];
		RecordType rt = null;
		if (rts.size() > 0) {
			rt = rts[0];
		}
		a.RecordTypeId = rt.Id;

		//Added by Arvind on 01/12/2016 to by pass the new validation rule added on Account

		Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
		User u = new User(Alias = 'testUser', Email = 'standardAccountuser@testorg.com.ocom',
				EmailEncodingKey = 'UTF-8', LastName = 'Testing123', LanguageLocaleKey = 'en_US',
				LocaleSidKey = 'en_US', ProfileId = p.Id,
				TimeZoneSidKey = 'America/Los_Angeles', UserName = 'standarduser99@testing.com.ocom', IsActive = true, Can_Update_Account_Name__c = True);
		insert u;
		System.runas(u) {
			update a;
		}

		stdController = new ApexPages.StandardController(c);
		ctlr = new MBRCommunityInviteCtlr(stdController);
		//System.assertEquals('Account is missing RCID', ctlr.errorMsg);

		// test with RCID__c set
		a.RCID__c = '12345';
		System.runas(u) {
			update a;
		}
		stdController = new ApexPages.StandardController(c);
		ctlr = new MBRCommunityInviteCtlr(stdController);
		System.assertEquals('RCID account is missing a parent CBUCID account', ctlr.errorMsg);

		// test with parentId set
		Account a2 = new Account(Name = 'Test CBUCID Account');
		insert a2;
		a.ParentId = a2.Id;
		update a;

		stdController = new ApexPages.StandardController(c);
		ctlr = new MBRCommunityInviteCtlr(stdController);
		System.assertEquals('RCID account is not linked to a valid CBUCID account', ctlr.errorMsg);

		// test with recordType CBUCID set
		rts = [SELECT Id, Name FROM RecordType WHERE Name = 'CBUCID'];
		rt = null;
		if (rts.size() > 0) {
			rt = rts[0];
		}
		a2.RecordTypeId = rt.Id;
		a2.CBUCID_Parent_Id__c = '23456';
		System.runas(u) {
			update a2;
		}

		PageReference pageRef = Page.MBRCommunityInvite;
		pageRef.getParameters().put('community', 'vitilcare');
		Test.setCurrentPage(pageRef);

		stdController = new ApexPages.StandardController(c);
		ctlr = new MBRCommunityInviteCtlr(stdController);
		ctlr.sendInvite();
		Boolean sent = ctlr.getEmailSent();
		ctlr.getMyContact();

		Account_Configuration__c ac = new Account_Configuration__c();
		ac.Restrict_Tech_Support__c = true;
		ac.Zenoss_URL__c = null;
		ac.Account__c = c.AccountId;
		insert ac;

		stdController = new ApexPages.StandardController(c);
		ctlr = new MBRCommunityInviteCtlr(stdController);
		ctlr.sendInvite();

		ctlr.test();
		ctlr.getItems();
		ctlr.setEInvite(ctlr.getEInvite());

		Test.stopTest();
	}
}