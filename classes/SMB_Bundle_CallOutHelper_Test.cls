@isTest
public class SMB_Bundle_CallOutHelper_Test {

    static testMethod void Test_SMB_Bundle_CallOutHelper() {
        SMB_DummyWebServiceResponse_Test.intializeMockWebservice();
        Opportunity test_opp = smb_test_utility.createOpportunity(Null, Null, Null, true);
        smb_test_utility.createQuote(Null,test_opp.Id,true);            
        map<string,integer> offerMap = new map<string,integer>();
        offerMap.put('1',1);
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SMB_Bundle_Service_Mock());  
        SMB_Bundle_CallOutHelper  bundle_CallOutHelper = new  SMB_Bundle_CallOutHelper();
        bundle_CallOutHelper.createBundleCommitmentHelper('Test Product', test_opp.Id, test_opp.AccountId, offerMap);
        smb_test_utility.createCustomSettingData();
        bundle_CallOutHelper.createBundleCommitmentHelper('Test Product', test_opp.Id, test_opp.AccountId, offerMap);
        Test.stopTest();
    }
    
     static testMethod void Test_OCOM_Bundle_CallOutHelper() {
        Id recRCIDTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId();
        Account RCIDacc = new Account(name='debs',recordtypeid=recRCIDTypeId);
        insert RCIDacc;
        smbcare_address__c address= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N5',FMS_Address_ID__c='987651', account__c=RCIDacc.id); 
        insert address;
        SMB_DummyWebServiceResponse_Test.intializeMockWebservice();
        order ord = new order(Ban__c='draft', effectivedate=system.today(),status='not submitted',service_address__c=address.id, accountId=RCIDacc.Id);   
        insert ord; 
        map<string,integer> offerMap = new map<string,integer>();
        offerMap.put('1',1);
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SMB_Bundle_Service_Mock());  
        SMB_Bundle_CallOutHelper  bundle_CallOutHelper = new  SMB_Bundle_CallOutHelper();
        bundle_CallOutHelper.createOCOMBundleCommitmentHelper('Test Product', ord.Id, RCIDacc.Id, offerMap, address.id, '1234');
        smb_test_utility.createCustomSettingData();
        bundle_CallOutHelper.createOCOMBundleCommitmentHelper('Test Product', ord.Id, RCIDacc.Id, offerMap, address.id, '1234');
        Test.stopTest();
    }
    
    public class SMB_Bundle_Service_Mock implements WebServiceMock {
      public void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {

           SMB_Bundle_BundleCommExtSvcReqResp_v1.CreateBundleCommitmentResponse response_x = new SMB_Bundle_BundleCommExtSvcReqResp_v1.CreateBundleCommitmentResponse();
           response_x.errorCode = '0';
           response.put('response_x', response_x);
    } 
    } 
}