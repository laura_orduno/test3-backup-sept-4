@isTest
private class ActivateOrderCtlr_vlocity_cmt_CPlsTest {
	static testMethod void TestCase_1() {
    	Test.startTest();

        TestDataHelper.testAccountCreation();
        TestDataHelper.testContactCreation();

        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'All Other Orders';
        insert testOpp_1;

		Contract testContract_1  = new Contract(Contract_Type__c='Amendment',AccountId=TestDataHelper.testAccountObj.Id,ContractTerm=12,StartDate=system.today(),vlocity_cmt__OpportunityId__c = testOpp_1.Id);
    	insert testContract_1;

 		vlocity_cmt__ContractVersion__c testContractVersion = new vlocity_cmt__ContractVersion__c(Name='Test Version 1', vlocity_cmt__ContractId__c = testContract_1.Id);
        insert testContractVersion;        
        
        list<Attachment> lstAttachment=new list<Attachment>();
        Blob b = Blob.valueOf('Test Data');  
	    Attachment attachment = new Attachment();  
	    attachment.ParentId = testContract_1.Id;  
	    attachment.Name = 'Test Attachment for Parent';  
	    attachment.Body = b;  
        lstAttachment.add(attachment);
        Attachment attachmentCV = new Attachment();  
	    attachmentCV.ParentId = testContractVersion.Id;  
	    attachmentCV.Name = 'Test Attachment for Content Version';  
	    attachmentCV.Body = b;  
        lstAttachment.add(attachmentCV);
	    insert(lstAttachment);  
    
       
        Test.stopTest();
        ApexPages.currentPage().getparameters().put('id', testContract_1.Id);
        ApexPages.StandardController sc_1 = new ApexPages.StandardController(testContract_1);
        ActivateOrderController_vlocity_cmt_CPls contToTest_1 = new ActivateOrderController_vlocity_cmt_CPls(sc_1);
        for(ActivateOrderController_vlocity_cmt_CPls.AttachmentWrapper attrVar:contToTest_1.attachmentWrapperList){
            attrVar.selectedVar=true;
        }
        contToTest_1.activateContract();
    }
    static testMethod void TestCase_2() {
    	Test.startTest();

        TestDataHelper.testAccountCreation();
        TestDataHelper.testContactCreation();

        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'All Other Orders';
        insert testOpp_1;

		Contract testContract_1  = new Contract(Contract_Type__c='Amendment',AccountId=TestDataHelper.testAccountObj.Id,ContractTerm=12,StartDate=system.today(),vlocity_cmt__OpportunityId__c = testOpp_1.Id);
    	insert testContract_1;

 		vlocity_cmt__ContractVersion__c testContractVersion = new vlocity_cmt__ContractVersion__c(Name='Test Version 1', vlocity_cmt__ContractId__c = testContract_1.Id);
        insert testContractVersion;        

        Test.stopTest();
        ApexPages.currentPage().getparameters().put('id', testContract_1.Id);
        ApexPages.StandardController sc_1 = new ApexPages.StandardController(testContract_1);
        ActivateOrderController_vlocity_cmt_CPls contToTest_1 = new ActivateOrderController_vlocity_cmt_CPls(sc_1);
        contToTest_1.activateContract();
    }
    static testMethod void TestCase_3() {
    	Test.startTest();

        TestDataHelper.testAccountCreation();
        TestDataHelper.testContactCreation();

        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'All Other Orders';
        insert testOpp_1;

		Contract testContract_1  = new Contract(Contract_Type__c='Amendment',AccountId=TestDataHelper.testAccountObj.Id,ContractTerm=12,StartDate=system.today(),vlocity_cmt__OpportunityId__c = testOpp_1.Id);
    	insert testContract_1;

 		vlocity_cmt__ContractVersion__c testContractVersion = new vlocity_cmt__ContractVersion__c(Name='Test Version 1', vlocity_cmt__ContractId__c = testContract_1.Id);
        insert testContractVersion;        
        
        list<Attachment> lstAttachment=new list<Attachment>();
        Blob b = Blob.valueOf('Test Data');  
	    Attachment attachment = new Attachment();  
	    attachment.ParentId = testContract_1.Id;  
	    attachment.Name = 'Test Attachment for Parent';  
	    attachment.Body = b;  
        lstAttachment.add(attachment);
        insert(lstAttachment);  
    
       
        Test.stopTest();
        ApexPages.currentPage().getparameters().put('id', testContract_1.Id);
        ApexPages.StandardController sc_1 = new ApexPages.StandardController(testContract_1);
        ActivateOrderController_vlocity_cmt_CPls contToTest_1 = new ActivateOrderController_vlocity_cmt_CPls(sc_1);
        for(ActivateOrderController_vlocity_cmt_CPls.AttachmentWrapper attrVar:contToTest_1.attachmentWrapperList){
            attrVar.selectedVar=true;
        }
        contToTest_1.activateContract();
    }
    static testMethod void TestCase_4() {
    	Test.startTest();

        TestDataHelper.testAccountCreation();
        TestDataHelper.testContactCreation();

        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'All Other Orders';
        insert testOpp_1;

		Contract testContract_1  = new Contract(Contract_Type__c='Amendment',AccountId=TestDataHelper.testAccountObj.Id,ContractTerm=12,StartDate=system.today(),vlocity_cmt__OpportunityId__c = testOpp_1.Id);
    	insert testContract_1;

 		vlocity_cmt__ContractVersion__c testContractVersion = new vlocity_cmt__ContractVersion__c(Name='Test Version 1', vlocity_cmt__ContractId__c = testContract_1.Id);
        insert testContractVersion;        
        
        list<Attachment> lstAttachment=new list<Attachment>();
        Blob b = Blob.valueOf('Test Data');  
	    Attachment attachment = new Attachment();  
	    attachment.ParentId = testContractVersion.Id;  
	    attachment.Name = 'Test Attachment for Parent';  
	    attachment.Body = b;  
        lstAttachment.add(attachment);
        insert(lstAttachment);  
    
       
        Test.stopTest();
        ApexPages.currentPage().getparameters().put('id', testContract_1.Id);
        ApexPages.StandardController sc_1 = new ApexPages.StandardController(testContract_1);
        ActivateOrderController_vlocity_cmt_CPls contToTest_1 = new ActivateOrderController_vlocity_cmt_CPls(sc_1);
        for(ActivateOrderController_vlocity_cmt_CPls.AttachmentWrapper attrVar:contToTest_1.attachmentWrapperList){
            attrVar.selectedVar=true;
        }
        contToTest_1.activateContract();
        //system.assertEquals(false, contToTest_1.isValidateSuccess);
        ActivateOrderController_vlocity_cmt_CPls.AttachmentWrapper objWrapper=new ActivateOrderController_vlocity_cmt_CPls.AttachmentWrapper(New attachment());
        //Cover Exception
        contToTest_1.contractObj=null;
        contToTest_1.activateContract();
        system.assertEquals(false, contToTest_1.isValidateSuccess);
    }
}