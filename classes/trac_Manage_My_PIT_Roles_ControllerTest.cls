@isTest
private class trac_Manage_My_PIT_Roles_ControllerTest {

    private static testMethod void testGetHasLines() {
    	trac_Manage_My_PIT_Roles_Controller ctlr = new trac_Manage_My_PIT_Roles_Controller();
    	
    	system.assertEquals(false,ctlr.getHasLines());
    }
    
    private static testMethod void testOnLoad() {
    	Opportunity o = new Opportunity(name = 'test', stagename = 'test', closedate = Date.today());
    	insert o;
    	
    	Product2 p = new Product2(name = 'Test Product', isActive = true);
      insert p;
    	
    	Opportunity_Solution__c os = new Opportunity_Solution__c(name = 'Test Solution', opportunity__c = o.id);
    	insert os;
    	
    	Opp_Product_Item__c opi = new Opp_Product_Item__c(opportunity__c = o.id, product__c = p.id, Opportunity_Solution__c = os.id);
    	insert opi;
    	
    	Product_Sales_Team__c pst = new Product_Sales_Team__c(Team_Role__c = 'test', Product__c = opi.id, Member__c = UserInfo.getUserId());
      insert pst;
    	
    	ApexPages.currentPage().getParameters().put('oid', o.id);
    	
    	trac_Manage_My_PIT_Roles_Controller ctlr = new trac_Manage_My_PIT_Roles_Controller();
    	
    	ctlr.onLoad();
    	
    	system.assert(ctlr.loaded);
    }
    
    private static testMethod void testOnLoadMissingOppId() {
    	trac_Manage_My_PIT_Roles_Controller ctlr = new trac_Manage_My_PIT_Roles_Controller();
    	
    	ctlr.onLoad();
    	
    	system.assertEquals(false,ctlr.loaded);
    }
    
    private static testMethod void testOnLoadMissingOpp() {
    	ApexPages.currentPage().getParameters().put('oid', '01p40000000DM8A');
    	
    	trac_Manage_My_PIT_Roles_Controller ctlr = new trac_Manage_My_PIT_Roles_Controller();
    	
    	ctlr.onLoad();
    	
    	system.assertEquals(false,ctlr.loaded);
    }
    
    private static testMethod void testSaveRoles() {
    	Opportunity o = new Opportunity(name = 'test', stagename = 'test', closedate = Date.today());
    	insert o;
    	
    	Product2 p = new Product2(name = 'Test Product', isActive = true);
      insert p;
    	
    	Opportunity_Solution__c os = new Opportunity_Solution__c(name = 'Test Solution', opportunity__c = o.id);
    	insert os;
    	
    	Opp_Product_Item__c opi = new Opp_Product_Item__c(opportunity__c = o.id, product__c = p.id, Opportunity_Solution__c = os.id);
    	insert opi;
    	
    	Product_Sales_Team__c pst = new Product_Sales_Team__c(Team_Role__c = 'test', Product__c = opi.id, Member__c = UserInfo.getUserId());
      insert pst;
      
      ApexPages.currentPage().getParameters().put('oid', o.id);
      
      trac_Manage_My_PIT_Roles_Controller ctlr = new trac_Manage_My_PIT_Roles_Controller();
      
			ctlr.onLoad();
    	
    	PageReference ref = ctlr.saveRoles();
    	
    	system.assertEquals('/' + o.id, ref.getUrl());
    }
    
    private static testMethod void testSaveRolesAddingRole() {
    	Opportunity o = new Opportunity(name = 'test', stagename = 'test', closedate = Date.today());
    	insert o;
    	
    	Product2 p = new Product2(name = 'Test Product', isActive = true);
      insert p;
    	
    	Opportunity_Solution__c os = new Opportunity_Solution__c(name = 'Test Solution', opportunity__c = o.id);
    	insert os;
    	
    	Opp_Product_Item__c opi = new Opp_Product_Item__c(opportunity__c = o.id, product__c = p.id, Opportunity_Solution__c = os.id);
    	insert opi;
      
      ApexPages.currentPage().getParameters().put('oid', o.id);
      
      trac_Manage_My_PIT_Roles_Controller ctlr = new trac_Manage_My_PIT_Roles_Controller();
      
			ctlr.onLoad();
			
			ctlr.lines[0].pit = new Product_Sales_Team__c(Team_Role__c = 'test', Product__c = opi.id, Member__c = UserInfo.getUserId());
    	
    	PageReference ref = ctlr.saveRoles();
    	
    	system.assertEquals('/' + o.id, ref.getUrl());
    }
    
    private static testMethod void testSaveRolesDeletingRole() {
    	Opportunity o = new Opportunity(name = 'test', stagename = 'test', closedate = Date.today());
    	insert o;
    	
    	Product2 p = new Product2(name = 'Test Product', isActive = true);
      insert p;
    	
    	Opportunity_Solution__c os = new Opportunity_Solution__c(name = 'Test Solution', opportunity__c = o.id);
    	insert os;
    	
    	Opp_Product_Item__c opi = new Opp_Product_Item__c(opportunity__c = o.id, product__c = p.id, Opportunity_Solution__c = os.id);
    	insert opi;
    	
    	Product_Sales_Team__c pst = new Product_Sales_Team__c(Team_Role__c = 'test', Product__c = opi.id, Member__c = UserInfo.getUserId());
      insert pst;
      
      ApexPages.currentPage().getParameters().put('oid', o.id);
      
      trac_Manage_My_PIT_Roles_Controller ctlr = new trac_Manage_My_PIT_Roles_Controller();
      
			ctlr.onLoad();
			
			ctlr.lines[0].pit.Team_Role__c = null;
    	
    	PageReference ref = ctlr.saveRoles();
    	
    	system.assertEquals('/' + o.id, ref.getUrl());
    }
    
    /* Cannot test this functionality, it is broken
    private static testMethod void testSaveRolesNoOppLines() {
    	Opportunity o = new Opportunity(name = 'test', stagename = 'test', closedate = Date.today());
    	insert o;
    	
    	Product2 p = new Product2(name = 'Test Product', isActive = true);
      insert p;
    	
    	Opportunity_Solution__c os = new Opportunity_Solution__c(name = 'Test Solution', opportunity__c = o.id);
    	insert os;
    	
    	Opp_Product_Item__c opi = new Opp_Product_Item__c(opportunity__c = o.id, product__c = p.id, Opportunity_Solution__c = os.id);
    	insert opi;
    	
    	Product_Sales_Team__c pst = new Product_Sales_Team__c(Team_Role__c = 'test', Product__c = opi.id, Member__c = UserInfo.getUserId());
      insert pst;
      
      ApexPages.currentPage().getParameters().put('oid', o.id);
      
      trac_Manage_My_PIT_Roles_Controller ctlr = new trac_Manage_My_PIT_Roles_Controller();
    	
    	ctlr.onLoad();
    	
    	ctlr.lines = null;
    	
    	PageReference ref = ctlr.saveRoles();
    	
    	system.assertEquals('/' + o.id, ref.getUrl());
    }
    */
    
    private static testMethod void testSaveRolesMustHaveRole() {
    	Opportunity o = new Opportunity(name = 'test', stagename = 'test', closedate = Date.today());
    	insert o;
    	
    	Product2 p = new Product2(name = 'Test Product', isActive = true);
      insert p;
    	
    	Opportunity_Solution__c os = new Opportunity_Solution__c(name = 'Test Solution', opportunity__c = o.id);
    	insert os;
    	
    	Opp_Product_Item__c opi = new Opp_Product_Item__c(opportunity__c = o.id, product__c = p.id, Opportunity_Solution__c = os.id);
    	insert opi;
    	
    	Product_Sales_Team__c pst = new Product_Sales_Team__c(Product__c = opi.id, Member__c = UserInfo.getUserId());
      insert pst;
      
      ApexPages.currentPage().getParameters().put('oid', o.id);
      
      trac_Manage_My_PIT_Roles_Controller ctlr = new trac_Manage_My_PIT_Roles_Controller();
      
			ctlr.onLoad();
    	
    	ctlr.lines[0].addThis = true;
    	
    	PageReference ref = ctlr.saveRoles();
    	
    	system.assertEquals(null, ref);
    	
    	system.assert(ApexPages.hasMessages(ApexPages.Severity.INFO));
    	system.assertEquals('You must select a role for ' + p.name, ApexPages.getMessages()[0].getSummary());
    }
    
    private static testMethod void testSaveRolesException() {
    	Opportunity o = new Opportunity(name = 'test', stagename = 'test', closedate = Date.today());
    	insert o;
    	
    	Product2 p = new Product2(name = 'Test Product', isActive = true);
      insert p;
    	
    	Opportunity_Solution__c os = new Opportunity_Solution__c(name = 'Test Solution', opportunity__c = o.id);
    	insert os;
    	
    	Opp_Product_Item__c opi = new Opp_Product_Item__c(opportunity__c = o.id, product__c = p.id, Opportunity_Solution__c = os.id);
    	insert opi;
    	
    	Product_Sales_Team__c pst = new Product_Sales_Team__c(Team_Role__c = 'test', Product__c = opi.id, Member__c = UserInfo.getUserId());
      insert pst;
      
      ApexPages.currentPage().getParameters().put('oid', o.id);
      
      trac_Manage_My_PIT_Roles_Controller ctlr = new trac_Manage_My_PIT_Roles_Controller();
      
			ctlr.onLoad();
    	
    	Test.setReadOnlyApplicationMode(true);
    	
    	PageReference ref = ctlr.saveRoles();
    	
    	system.assertEquals(null, ref);
    }
    
    private static testMethod void testSaveRolesNoOpp() {
    	trac_Manage_My_PIT_Roles_Controller ctlr = new trac_Manage_My_PIT_Roles_Controller();
    	
    	PageReference ref = ctlr.saveRoles();
    	
    	system.assertEquals(null,ref);
    }
    
    private static testMethod void testBackToOpp() {
    	Opportunity o = new Opportunity(name = 'test', stagename = 'test', closedate = Date.today());
    	insert o;
    	
    	Product2 p = new Product2(name = 'Test Product', isActive = true);
      insert p;
    	
    	Opp_Product_Item__c opi = new Opp_Product_Item__c(opportunity__c = o.id, product__c = p.id);
    	insert opi;
    	
    	ApexPages.currentPage().getParameters().put('oid', o.id);
    	
    	trac_Manage_My_PIT_Roles_Controller ctlr = new trac_Manage_My_PIT_Roles_Controller();
    	ctlr.onLoad();
    	
    	PageReference ref = ctlr.backToOpp();
    	
    	system.assertEquals('/' + o.id,ref.getUrl());
    }
    
    private static testMethod void testBackToOppNull() {
    	trac_Manage_My_PIT_Roles_Controller ctlr = new trac_Manage_My_PIT_Roles_Controller();
    	
    	PageReference ref = ctlr.backToOpp();
    	
    	system.assertEquals(null,ref);
    }
    
    private static testMethod void testOppLineRoleVO() {
    	Opp_Product_Item__c opi = new Opp_Product_Item__c();
    	Product_Sales_Team__c pst = new Product_Sales_Team__c();
    	
    	trac_Manage_My_PIT_Roles_Controller.OppLineRoleVO olr = new trac_Manage_My_PIT_Roles_Controller.OppLineRoleVO(opi, pst);
    }
    
    private static testMethod void testOppLineRoleVOException() {
    	Boolean caughtException = false;
    	
    	try {
    		trac_Manage_My_PIT_Roles_Controller.OppLineRoleVO olr = new trac_Manage_My_PIT_Roles_Controller.OppLineRoleVO(null, null);
    	} catch (Exception e) {
    		caughtException = true;
    		system.assertEquals('Opp product item cannot be null', e.getMessage());
    	}
    	
    	system.assert(caughtException);
    }
    
    private static testMethod void testOppLineRoleVONullProductSalesTeam() {
    	Opp_Product_Item__c opi = new Opp_Product_Item__c();
    	
    	trac_Manage_My_PIT_Roles_Controller.OppLineRoleVO olr = new trac_Manage_My_PIT_Roles_Controller.OppLineRoleVO(opi, null);
    	
    	system.assertNotEquals(null, olr.pit);
    }
}