public class NAAS_WFM_DueDate_Mgmt_Helper {
    Public static String getUserNamePassword(){
        SMBCare_WebServices__c wsUsername = SMBCare_WebServices__c.getValues('username');
        SMBCare_WebServices__c wsPassword = SMBCare_WebServices__c.getValues('password');
        String encodedusernameandpassword = EncodingUtil.base64Encode(Blob.valueOf(WSusername.Value__c+':'+wsPassword.Value__c));
        return encodedusernameandpassword;
    }
    Public static Integer getMinutes(String slot){
        string startTimeHours = slot.trim().substring(0, 2);
        string startTimeMins = slot.trim().substring(2, 4);
        integer mints=(Integer.valueOf(startTimeHours)*60)+ Integer.ValueOf(startTimeMins);
        
        return mints;
    }
    public static SMB_Appointment_WFMgmtOrderTypes_v2.WorkOrder setWorkOrder(String productCategoryCd,String workOrderActionCd,String jobTypeCd,String productTechnologyCd,String estimatedDurationNum,String serviceClassCd){
        SMB_Appointment_WFMgmtOrderTypes_v2.WorkOrder wOrder = new SMB_Appointment_WFMgmtOrderTypes_v2.WorkOrder();
        wOrder.originatingSystemId = '6161';/////////////////WFM will give this ID
        wOrder.classificationCd = 'ORDER';//workOrder.Classification_Code__c;
        wOrder.serviceClassCd = serviceClassCd;
        wOrder.productCategoryCd =productCategoryCd;
        wOrder.productTechnologyCd = productTechnologyCd;
        wOrder.jobTypeCd = jobTypeCd;
        wOrder.workOrderActionCd =workOrderActionCd;
        wOrder.estimatedDurationNum = Double.valueOf(estimatedDurationNum);
        wOrder.estimatedDurationUnitCd = 'HOURS';       
        return  wOrder;        
    }
    public static Object setWorkOrderBooking(String productCategoryCd,String workOrderActionCd,String jobTypeCd,String productTechnologyCd,String estimatedDurationNum,String serviceClassCd){
        SMB_AssignmentWFM_OrderTypes_v2.WorkOrder wOrder = new SMB_AssignmentWFM_OrderTypes_v2.WorkOrder();
        wOrder.originatingSystemId = '6161';/////////////////WFM will give this ID
        wOrder.classificationCd = 'ORDER';//workOrder.Classification_Code__c;
        //  wOrder.originatingSystemWorkOrderId='00087896';
        //  wOrder.originatingSystemWorkOrderInternalId='00087896';
        wOrder.serviceClassCd = serviceClassCd;
        wOrder.productCategoryCd =productCategoryCd;
        wOrder.productTechnologyCd = productTechnologyCd;
        wOrder.jobTypeCd = jobTypeCd;
        wOrder.workOrderActionCd =workOrderActionCd;
        wOrder.outofServiceInd = false;
        wOrder.priorityCd = '3';
        wOrder.hostReadyForDispatchInd = false;
        wOrder.estimatedDurationNum = Double.valueOf(estimatedDurationNum);
        wOrder.estimatedDurationUnitCd = 'HOURS';  
        wOrder.appointmentStartDate=null;
        wOrder.appointmentEndDate=null;    
        return  wOrder;        
    }
    
    Public static Object setLocationDetails(String fmsAddressId,String houseNumber,String streetName,String cityName,String provinceCode,String postalcode, String fmsCoid,String countryCode,String queryType){
        if(queryType=='SEARCH'){   
            SMB_Appointment_WFMgmtOrderTypes_v2.TypedLocationAddress typedLocationAddress = new SMB_Appointment_WFMgmtOrderTypes_v2.TypedLocationAddress();
            typedLocationAddress.typeCode = 'SERVICE';
            typedLocationAddress.dispatchLocationInd = true;
            typedLocationAddress.fmsAddress = new SMB_Appointment_WFMgmtOrderTypes_v2.FMSAddress();
            typedLocationAddress.fmsAddress.fmsAddressId =fmsAddressId;
            typedLocationAddress.fmsAddress.houseNumber=houseNumber;
            typedLocationAddress.fmsAddress.streetName= streetName;
            typedLocationAddress.fmsAddress.cityName = cityName;
            typedLocationAddress.fmsAddress.provinceCode =provinceCode;
            typedLocationAddress.fmsAddress.postalcode =postalcode;
            typedLocationAddress.fmsAddress.fmsCoid =fmsCoid;
            typedLocationAddress.fmsAddress.countryCode =countryCode;
            return  typedLocationAddress;   
        } else if(queryType=='BOOK'){
            SMB_AssignmentWFM_OrderTypes_v2.TypedLocationAddress  typedLocationAddress = new SMB_AssignmentWFM_OrderTypes_v2.TypedLocationAddress();
            typedLocationAddress.typeCode = 'SERVICE';
            typedLocationAddress.dispatchLocationInd = true;
            typedLocationAddress.fmsAddress = new SMB_AssignmentWFM_OrderTypes_v2.FMSAddress();
            typedLocationAddress.fmsAddress.fmsAddressId =fmsAddressId;
            typedLocationAddress.fmsAddress.houseNumber=houseNumber;
            typedLocationAddress.fmsAddress.streetName= streetName;
            typedLocationAddress.fmsAddress.cityName = cityName;
            typedLocationAddress.fmsAddress.provinceCode =provinceCode;
            typedLocationAddress.fmsAddress.postalcode =postalcode;
            typedLocationAddress.fmsAddress.fmsCoid =fmsCoid;
            typedLocationAddress.fmsAddress.countryCode =countryCode;
            return typedLocationAddress;
        } return null;       
    }
    public Static Object setContactDetails(String contactName,String phoneNumber){
        SMB_AssignmentWFM_OrderTypes_v2.Contact con = new SMB_AssignmentWFM_OrderTypes_v2.Contact();
        con.name =contactName;
        con.typeCd ='CBR';
        con.phoneNumberTxt =phoneNumber;
        con.phoneLocationCd = 'AUT'; 
        return con;
    }   
    //setSearchAppointmenMsgBody(iHeader,wOrder,typedLocationAddress,startDate,endDate,orderId,OLId);    
    public static String setSearchAppointmenMsgBody(SMB_Appointment_WFMgmtOrderTypes_v2.InputHeader iHeader,SMB_Appointment_WFMgmtOrderTypes_v2.WorkOrder wOrder,SMB_Appointment_WFMgmtOrderTypes_v2.TypedLocationAddress typedLocationAddress,Datetime startDate,Datetime endDate,String orderId,String OLId){
        string messageBody='Order (SFDC ID:):'+OLId+'):'+orderId+'\n\n'+
            'iHeader.requestDate:'+iHeader.requestDate+'\n'+
            'iHeader.systemSourceCd:'+iHeader.systemSourceCd+'\n'+
            'iHeader.userId:'+iHeader.userId+'\n'+
            'wOrder.originatingSystemId:'+wOrder.originatingSystemId+'\n'+
            'wOrder.originatingSystemWorkOrderId:'+wOrder.originatingSystemWorkOrderId+'\n'+
            'wOrder.originatingSystemWorkOrderInternalId:'+wOrder.originatingSystemWorkOrderInternalId+'\n'+
            'wOrder.classificationCd:'+wOrder.classificationCd+'\n'+
            'wOrder.serviceClassCd:'+wOrder.serviceClassCd+'\n'+
            'wOrder.productCategoryCd:'+'DSL'+'\n'+
            'wOrder.productTechnologyCd:'+wOrder.productTechnologyCd+'\n'+
            'wOrder.workOrderActionCd:'+wOrder.workOrderActionCd+'\n'+
            'wOrder.jobTypeCd:'+wOrder.jobTypeCd+'\n'+
            'wOrder.outofServiceInd:'+wOrder.outofServiceInd+'\n'+
            'wOrder.priorityCd:'+wOrder.priorityCd+'\n'+
            'wOrder.hostReadyForDispatchInd:'+wOrder.hostReadyForDispatchInd+'\n'+
            'wOrder.appointmentStartDate:'+wOrder.appointmentStartDate+'\n'+
            'wOrder.appointmentEndDate:'+wOrder.appointmentEndDate+'\n'+
            'wOrder.estimatedDurationNum:'+wOrder.estimatedDurationNum+'\n'+
            'wOrder.estimatedDurationUnitCd:'+wOrder.estimatedDurationUnitCd+'\n'+
            'typedLocationAddress.typeCode:'+typedLocationAddress.typeCode+'\n'+
            'typedLocationAddress.dispatchLocationInd:'+typedLocationAddress.dispatchLocationInd+'\n'+
            'typedLocationAddress.fmsAddress.fmsAddressId:'+typedLocationAddress.fmsAddress.fmsAddressId+'\n'+
            'wOrder.customerName:'+wOrder.customerName+'\n'+
            'wOrder.customerRatingCd:'+wOrder.customerRatingCd+'\n'+
            'wOrder.customerTypeCd:'+wOrder.customerTypeCd+'\n'+
            'wOrder.originatingSystemCreateDate:'+wOrder.originatingSystemCreateDate+'\n'+
            'startDate:'+startDate+'\n'+
            'endDate:'+endDate+'\n';
        return messageBody;
    } 
    public static String setBookAppointmenMsgBody(SMB_AssignmentWFM_OrderTypes_v2.InputHeader iHeader,SMB_AssignmentWFM_OrderTypes_v2.WorkOrder wOrder,SMB_AssignmentWFM_OrderTypes_v2.TypedLocationAddress typedLocationAddress,SMB_AssignmentWFM_OrderTypes_v2.Contact con,String orderId,String OLId){
        String messageBody='Order (SFDC ID:):'+OLId+'):'+orderId+'\n\n'+
            'iHeader.requestDate:'+iHeader.requestDate+'\n'+
            'iHeader.systemSourceCd:'+iHeader.systemSourceCd+'\n'+
            'iHeader.userId:'+iHeader.userId+'\n'+
            'wOrder.originatingSystemId:'+wOrder.originatingSystemId+'\n'+
            'wOrder.originatingSystemWorkOrderId:'+wOrder.originatingSystemWorkOrderId+'\n'+
            'wOrder.originatingSystemWorkOrderInternalId:'+wOrder.originatingSystemWorkOrderInternalId+'\n'+
            'wOrder.classificationCd:'+wOrder.classificationCd+'\n'+
            'wOrder.serviceClassCd:'+wOrder.serviceClassCd+'\n'+
            'wOrder.productCategoryCd:'+wOrder.productCategoryCd+'\n'+
            'wOrder.productTechnologyCd:'+wOrder.productTechnologyCd+'\n'+
            'wOrder.workOrderActionCd:'+wOrder.workOrderActionCd+'\n'+
            'wOrder.jobTypeCd:'+wOrder.jobTypeCd+'\n'+
            'wOrder.outofServiceInd:'+wOrder.outofServiceInd+'\n'+
            'wOrder.priorityCd:'+wOrder.priorityCd+'\n'+
            'wOrder.hostReadyForDispatchInd:'+wOrder.hostReadyForDispatchInd+'\n'+
            'wOrder.appointmentStartDate:'+wOrder.appointmentStartDate+'\n'+
            'wOrder.appointmentEndDate:'+wOrder.appointmentEndDate+'\n'+
            'wOrder.estimatedDurationNum:'+wOrder.estimatedDurationNum+'\n'+
            'wOrder.estimatedDurationUnitCd:'+wOrder.estimatedDurationUnitCd+'\n'+
            'typedLocationAddress.typeCode:'+typedLocationAddress.typeCode+'\n'+
            'typedLocationAddress.dispatchLocationInd:'+typedLocationAddress.dispatchLocationInd+'\n'+
            'typedLocationAddress.fmsAddress.fmsAddressId:'+typedLocationAddress.fmsAddress.fmsAddressId+'\n'+
            'wOrder.customerName:'+wOrder.customerName+'\n'+
            'wOrder.customerRatingCd:'+wOrder.customerRatingCd+'\n'+
            'wOrder.customerTypeCd:'+wOrder.customerTypeCd+'\n'+
            'con.name:'+con.name+'\n'+
            'con.typeCd:'+con.typeCd+'\n'+
            'con.phoneNumberTxt:'+con.phoneNumberTxt+'\n'+
            'con.phoneLocationCd:'+con.phoneLocationCd+'\n'+
            'wOrder.originatingSystemCreateDate:'+wOrder.originatingSystemCreateDate+'\n';
        return messageBody;
    }    
    public static Object setHeader(Object iHeader,DateTime requestedDate,String quertType){
        list<user> lstTid = [select id,Team_TELUS_ID__c from User where id = :Userinfo.getUserId()];
        String userId;
        if(lstTid != null && lstTid.size()>0)
        {  
            if (string.isNotBlank(lstTid[0].Team_TELUS_ID__c))
                userId= lstTid[0].Team_TELUS_ID__c;                    
            else
                userId = 'APP_SFDC';
        }
        if(quertType=='SEARCH'){
            SMB_Appointment_WFMgmtOrderTypes_v2.InputHeader  iHeaderVar = (SMB_Appointment_WFMgmtOrderTypes_v2.InputHeader)iHeader;
            iHeaderVar.requestDate =requestedDate;
            iHeaderVar.systemSourceCd = '6161'; //system source ID //WFM will give this ID
            iHeaderVar.userId=userId;
            return iHeaderVar;
        }else{
            SMB_AssignmentWFM_OrderTypes_v2.InputHeader  iHeaderVar = (SMB_AssignmentWFM_OrderTypes_v2.InputHeader)iHeader;
            iHeaderVar.requestDate =requestedDate;
            iHeaderVar.systemSourceCd = '6161'; //system source ID //WFM will give this ID
            iHeaderVar.userId=userId;
            return iHeaderVar;
        }
        return iHeader;
    }
    
    public static void insertWebserviceLog(String messageBody,String exceptionMessage,exception e,String causeType){
        map<string,schema.recordtypeinfo> errorLogRecordTypeMap=webservice_integration_error_log__c.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        webservice_integration_error_log__c log=new webservice_integration_error_log__c(apex_class_and_method__c=messageBody,external_system_name__c='WFM',webservice_name__c='WFM Appointment Search/book',object_name__c='Work_Order__c');   //sfdc_record_id__c=lineItem.orderId);
        if(errorLogRecordTypeMap.containskey(causeType)){
            log.recordtypeid=errorLogRecordTypeMap.get(causeType).getrecordtypeid();
        }
        exceptionMessage='';
        if(causeType.endsWithIgnoreCase('Success')){
            log.error_code_and_message__c='Successfully searched for slots on WFM:\n\n'+messageBody;
            log.error_code_and_message__c+='\n\n'+exceptionMessage;
            log.stack_trace__c=exceptionMessage;
        }else  if(causeType.endsWithIgnoreCase('Failure')){
            if(null!=e){
                exceptionMessage='Failed Exception ['+e.getmessage()+']:\n\n'+messageBody;
                system.debug('====exceptionMessage===='+exceptionMessage);
                log.error_code_and_message__c=exceptionMessage;
                log.stack_trace__c=e.getstacktracestring();                   
            }else{
                exceptionMessage='Failed Exception:\n\n'+messageBody;
                system.debug('====exceptionMessage===='+exceptionMessage);
                log.stack_trace__c=log.error_code_and_message__c=exceptionMessage;
            }
            
        }       
        if(string.isnotblank(log.stack_trace__c)&&log.stack_trace__c.length()>32768){
            log.stack_trace__c=log.stack_trace__c.substring(0,32767);
        }
        insert log; 
    }
    public static Map<String,NAAS_WFM_DueDate_MgmtComponentController.ServiceAddress> getServiceAddressDetailByOLIID(List<String> OLIIdsList){
        Map<String,NAAS_WFM_DueDate_MgmtComponentController.ServiceAddress> OLIToSADataMap = new Map<String,NAAS_WFM_DueDate_MgmtComponentController.ServiceAddress>();
        for(OrderItem oli:[select id,order.Service_Address__r.FMS_Address_ID__c,order.Service_Address__r.Street__c,order.Service_Address__r.Building_Number__c,order.Service_Address__r.City__c,order.Service_Address__r.Province__c,order.Service_Address__r.Postal_Code__c,order.Service_Address__r.COID__c,order.Service_Address__r.Country__c from OrderItem where Id =:OLIIdsList]){
            NAAS_WFM_DueDate_MgmtComponentController.ServiceAddress SAData = new NAAS_WFM_DueDate_MgmtComponentController.ServiceAddress();
            if(string.isNotblank(oli.order.Service_Address__r.FMS_Address_ID__c)){
                SAData.FmsAddressId=oli.order.Service_Address__r.FMS_Address_ID__c;
            } 
            if(string.isNotblank(oli.order.Service_Address__r.Building_Number__c)){
                SAData.HouseNumber=oli.order.Service_Address__r.Building_Number__c;
            }
            if(string.isNotblank(oli.order.Service_Address__r.Street__c)){
                SAData.StreetName=oli.order.Service_Address__r.Street__c.toUpperCase();
            }   
            if(string.isNotblank(oli.order.Service_Address__r.City__c)){  
                SAData.CityName=oli.order.Service_Address__r.City__c.toUpperCase();
            }
            if(string.isNotblank(oli.order.Service_Address__r.Province__c)){
                SAData.ProvinceCode=oli.order.Service_Address__r.Province__c;
            }
            if(string.isNotblank(oli.order.Service_Address__r.Postal_Code__c)){          
                SAData.Postalcode=oli.order.Service_Address__r.Postal_Code__c.toUpperCase();
            }  
            if(string.isNotblank(oli.order.Service_Address__r.COID__c)&& (oli.order.Service_Address__r.COID__c)!=Null ){  
                SAData.FmsCoid=oli.order.Service_Address__r.COID__c.toUpperCase();
            }
            if(string.isNotBlank(oli.order.Service_Address__r.Country__c) && oli.order.Service_Address__r.Country__c =='CAN'){ 
                SAData.CountryCode= 'CANADA';
            }
            OLIToSADataMap.put(oli.Id,SAData);
        }
        return OLIToSADataMap;
    }
    
    public static String readJSONCharactersticInOLI(String JSONString,String searchCharacterstic){
        String value;
        if(String.isNotBlank(JSONString) && String.isNotBlank(searchCharacterstic)) {
            Map<String, Object> attributes = (Map<String, Object>) JSON.deserializeUntyped(JSONString);                
            for (String key : attributes.keySet()) {
                for (Object attribute : (List<Object>) attributes.get(key)) {
                    Map<String, Object> attributeMap = (Map<String, Object>) attribute;
                    String attributeName = (String) attributeMap.get('attributedisplayname__c');
                    if(attributeName.toUpperCase() == searchCharacterstic.toUpperCase() && (String) attributeMap.get('uidisplaytype__c') == 'Dropdown'){
                        //   value = (String) attributeMap.get('value__c');
                        Map<String, Object> runTimeInfoMap = (Map<String, Object>) attributeMap.get('attributeRunTimeInfo');
                        if(String.isBlank(value)) {                            
                            Map<String, Object> selectedItemMap = (Map<String, Object>) runTimeInfoMap.get('selectedItem');
                            value= (String) selectedItemMap.get('value');
                        }
                    }
                }
            }           
        } 
        return value;
    }
    public static void updateBookingCharactersticInOLI(Map<String,String> WFMCharactersticValueMap,OrderItem Oli){
        List<OrderItem> product;
        try{
            product=[select id,PriceBookEntry.Product2.Name,vlocity_cmt__JSONAttribute__c from orderitem where id =:oli.id limit 1]; 
        }catch(exception ex){
            throw ex;
        }
        if(null !=product && product.size()>0 && String.isNotBlank(product[0].vlocity_cmt__JSONAttribute__c)) {
            Map<String, Object> attributes = (Map<String, Object>) JSON.deserializeUntyped(product[0].vlocity_cmt__JSONAttribute__c);                
            for (String key : attributes.keySet()) {
                for (Object attribute : (List<Object>) attributes.get(key)) {
                    Map<String, Object> attributeMap = (Map<String, Object>) attribute;
                    String attributeName = (String) attributeMap.get('attributedisplayname__c');
                    if(WFMCharactersticValueMap.containsKey(attributeName) && null != WFMCharactersticValueMap.get(attributeName) ){
                        String value = (String) attributeMap.get('value__c');
                        Map<String, Object> runTimeInfoMap = (Map<String, Object>) attributeMap.get('attributeRunTimeInfo');
                        runTimeInfoMap.put('value',WFMCharactersticValueMap.get(attributeName));
                        attributeMap.put('value__c',WFMCharactersticValueMap.get(attributeName));
                        System.debug('attributeName=' + attributeName);
                        System.debug('value=' + value);                            
                    }
                }
            }    
            Oli.vlocity_cmt__JSONAttribute__c= JSON.serialize(attributes);
        }      
    }
    public static OrderItem updateOLIAfterCancellation(OrderItem oli){
        oli.Work_Order__c = NULL;
        oli.Requested_Due_Date__c = NULL;
        oli.Scheduled_Due_Date__c = NULL;
        oli.Reservation_ID__c = NULL;
        oli.Install_Type__c = NULL ;
        oli.Onsite_Contact_Office__c = NULL;
        oli.Onsite_Contact_Cell__c = NULL ;
        oli.Onsite_Contact_Home__c = NULL;
        oli.Installation_Notes__c = NULL;
        oli.Work_Time__c = NULL;         
        return oli;
    }
    public static void cancelWFMBookingByOrderId(String orderId){
        list<Id> lstOLIIds = new list <Id>();
        integer count=0;
        try{
        
            List<OrderItem> OLIRecs = [select id,work_order__c,
                                       work_order__r.WFM_number__c, 
                                       orderId,
                                       work_order__r.status__c                        
                                       From OrderItem
                                       where OrderId =:orderId 
                                       and work_order__c != null and work_order__r.status__c= :Label.OCOM_WFMReserved];
            
            for(OrderItem o:OLIRecs) {
                if(String.isNotBlank(o.work_order__r.WFM_number__c)){
                    lstOLIIds.add(o.id); 
                    count++;
                    if((Math.mod(count,3) == 0) && count != 0 ){  
                        cancelWFMBookingByOLIIdList(lstOLIIds);
                        lstOLIIds.clear();
                        count=0;
                    }
                }                                 
            } 
        }catch(exception e){
            throw e;
        }
        if(count>0){
          cancelWFMBookingByOLIIdList(lstOLIIds); 
        }
        
    }
    
    @future(callout=true)
    Public static void cancelWFMBookingByOLIIdList(Map<String,String> oliIdToWFMNumberMap,Map<String,String> oliIdTowOrderIdMap,string userId){
        system.debug('>>>>>InsidecancelWFMBookingByOLIIdList::');    
        List <work_order__c> lstWo = new List<work_order__c> (); 
        system.debug('!!!oliIdToWFMNumberMap '+oliIdToWFMNumberMap);
        for(String oliIdKey:oliIdToWFMNumberMap.keySet()){
            if(String.isNotBlank(oliIdToWFMNumberMap.get(oliIdKey))){
                String wfmNumber=oliIdToWFMNumberMap.get(oliIdKey);
                //CALLOUT
                
                if (String.isNotBlank(wfmNumber)) 
                {
                    //Preapare Headers for the callout
                    SMB_AssignmentWFM_AssignMgmtService_2.FieldWorkAssignmentMgmtServicePort serPort = new SMB_AssignmentWFM_AssignMgmtService_2.FieldWorkAssignmentMgmtServicePort();
                    serPort.inputHttpHeaders_x = new Map<String, String>();
                    serPort.inputHttpHeaders_x.put('Authorization','Basic ' + getUserNamePassword());
                    //INPUT HEADER
                    SMB_AssignmentWFM_OrderTypes_v2.InputHeader iHeader = new SMB_AssignmentWFM_OrderTypes_v2.InputHeader();
                    iHeader.requestDate =datetime.now();
                    iHeader.systemSourceCd = '6161'; //system source ID //WFM will give this ID
                    iHeader.userId=userId;
                    //Work Order
                    SMB_AssignmentWFM_OrderTypes_v2.WorkOrder wOrder = new SMB_AssignmentWFM_OrderTypes_v2.WorkOrder();
                    wOrder.originatingSystemId = '6161';  
                    try
                    {
                        wOrder.workOrderId = wfmNumber;
                        String workOrderId= serPort.CancelWorkOrder(iHeader, wOrder);
                        work_Order__c wfm =  new work_Order__c();
                        wfm.Id = oliIdTowOrderIdMap.get(oliIdKey);
                        wfm.Status__c = Label.OCOM_WFMCANCELLED;
                        lstWo.add(wfm);  
                    }catch (CalloutException e) 
                    {
                        System.debug('Exception === ' + e.getMessage()); String exceptionMsg=' Cancel WFM Booking is failed for Booking Id '+wfmNumber; insertWebserviceLog('ClassName:NAAS_WFM_DueDate_Mgmt_Helper and MethodName:cancelWFMBookingByOLIIdList ',exceptionMsg,e,'Failure');                      
                    }                  
                }
            } else{
                work_Order__c wfm =  new work_Order__c();
                wfm.Id = oliIdTowOrderIdMap.get(oliIdKey);
                wfm.Status__c = Label.OCOM_WFMCANCELLED;
                lstWo.add(wfm); 
            } 
        }   
        try{
            if(lstWo.size()>0){
                update lstWo;
            }
            
        }
        catch (Exception e)
        {
            System.debug('Exception === ' + e.getMessage()); String exceptionMsg=' OrderLineItems Update Failed after WFM Booking Cancellation : '+lstWo;  insertWebserviceLog('ClassName:NAAS_WFM_DueDate_Mgmt_Helper and MethodName:cancelWFMBookingByOLIIdList ',exceptionMsg,e,'Failure');
            
        } 
        
    }
    
    @future(callout=true)
    Public static void cancelWFMBookingByOLIIdList(List<Id> lstOLIIds){
        list<OrderItem> lstOLI = new list <OrderItem>();
        List <work_order__c> lstWo = new List<work_order__c> ();   
        try{ lstOLI=  lstOLI = [select o.id,o.work_order__c,
                                o.work_order__r.WFM_number__c, 
                                o.orderId,
                                o.Requested_Due_Date__c,
                                o.Scheduled_Due_Date__c,
                                o.Reservation_ID__c,
                                o.Install_Type__c,
                                o.Onsite_Contact_Office__c,
                                o.Onsite_Contact_Cell__c,
                                o.Onsite_Contact_Home__c,
                                o.Installation_Notes__c,
                                o.Work_Time__c
                                From OrderItem o 
                                where id IN :lstOLIIds];
           }catch(exception e){ throw e; }
        for (OrderItem oli : lstOLI)
        {
            //CALLOUT
            if ( oli.work_order__r.WFM_number__c != null ) 
            {
                //Preapare Headers for the callout
                SMB_AssignmentWFM_AssignMgmtService_2.FieldWorkAssignmentMgmtServicePort serPort = new SMB_AssignmentWFM_AssignMgmtService_2.FieldWorkAssignmentMgmtServicePort();
                serPort.inputHttpHeaders_x = new Map<String, String>();
                serPort.inputHttpHeaders_x.put('Authorization','Basic ' + getUserNamePassword());
                //INPUT HEADER
                SMB_AssignmentWFM_OrderTypes_v2.InputHeader iHeader = new SMB_AssignmentWFM_OrderTypes_v2.InputHeader();
                iHeader= (SMB_AssignmentWFM_OrderTypes_v2.InputHeader) setHeader(iHeader,datetime.now(),'BOOK');            
                //Work Order
                SMB_AssignmentWFM_OrderTypes_v2.WorkOrder wOrder = new SMB_AssignmentWFM_OrderTypes_v2.WorkOrder();
                wOrder.originatingSystemId = '6161';  
                try
                {
                    wOrder.workOrderId = oli.work_order__r.WFM_number__c;
                    String workOrderId= serPort.CancelWorkOrder(iHeader, wOrder);
                    work_Order__c wfm =  new work_Order__c();
                    wfm.Id = oli.work_order__c;
                    wfm.Status__c = Label.OCOM_WFMCANCELLED;
                    lstWo.add(wfm);  
                    oli=updateOLIAfterCancellation(oli);  
                }catch (CalloutException e) 
                {
                    //  setOppIds.add(oli.orderId);
                    System.debug('Exception === ' + e.getMessage()); String exceptionMsg=' Cancel WFM Booking is failed for OLI Id: '+oli.id+' OrderId: '+oli.orderId+'  and Booking Id '+oli.work_order__r.WFM_number__c; insertWebserviceLog('ClassName:NAAS_WFM_DueDate_Mgmt_Helper and MethodName:cancelWFMBookingByOLIIdList ',exceptionMsg,e,'Failure');                    
                }                  
            }else{
                work_Order__c wfm =  new work_Order__c();
                wfm.Id = oli.work_order__c;
                wfm.Status__c = Label.OCOM_WFMCANCELLED;
                lstWo.add(wfm);  
                oli=updateOLIAfterCancellation(oli);
            }
        }
        try{
            if(lstWo.size()>0){
                System.debug('>>>>>>>>>>>>>>>>>List of work order to be removed'+lstWo);
                update lstWo;                
            }
            update lstOLI;
        }
        catch (Exception e)
        {
            System.debug('Exception === ' + e.getMessage()); String exceptionMsg=' OrderLineItems Update Failed after WFM Booking Cancellation : '+lstOLI; insertWebserviceLog('ClassName:NAAS_WFM_DueDate_Mgmt_Helper and MethodName:cancelWFMBookingByOLIIdList ',exceptionMsg,e,'Failure');
                        
        }
    }
}