/*------------------------
@Author: TELUS internal Team.
@Business Requirement: This is to convert FOBO checklists to opportunity line items. 
@Dependent : This logic is based on mapping, which is stored in foboCheckList_OppyLineItem_Map__c custom settings. 
TODO: To update exsting opportunity lines times, we need reference between Fobo checklist and opportunity line items. Check with Astadia and 
      implement similar reference. 
--------------------------*/
public class foboCheckList_OppyLineItem_Helper {
    
    private static String productId = 'TLSSMBVLTN-NC-TSTPRDCT';
    private static String oppyStageName= 'Quote Ordered';
    
    public static void convert_to_oppy_items(list<FOBOChecklist__c> checkList){     
        Product2 product= [SELECT Id FROM Product2 WHERE Sterling_Item_ID__c =: productId LIMIT 1];
        list<Opp_Product_Item__c>  oppyLineList = new list<Opp_Product_Item__c>();
        Map<String, String> fieldMap = new Map<String, String>();
        String notesforBO;
        list<foboCheckList_OppyLineItem_Map__c> mapping = new list<foboCheckList_OppyLineItem_Map__c>();
        //Get all fields from FOBO cheklist object
         list<Schema.FieldSetMember> fieldSetMemberList =  Util_Class_for_Complex_Order.readFieldSet('BO_Concatenation_List', 'FOBOChecklist__c');
          for(Schema.FieldSetMember fieldSetMemberObj : fieldSetMemberList )
            {
                    fieldMap.put(fieldSetMemberObj.getFieldPath().toUpperCase(), fieldSetMemberObj.getLabel());
                    system.debug('LabelSOQL ====>' + fieldSetMemberObj.getLabel());
            }
        mapping = [SELECT Id, checkList_field__c, Oppy_Line_Item__c FROM foboCheckList_OppyLineItem_Map__c];
        for(FOBOChecklist__c fb: checkList){        
            system.debug('========FOBOChekList=====' + fb);     
            Opp_Product_Item__c OppyLine = new  Opp_Product_Item__c(Product__c = product.Id);  
            System.debug('Oppy stage====' + fb.Opportunity_Stage__c + '===' + oppyStageName);
            // if(fb.Opportunity_Stage__c == oppyStageName){
            for(foboCheckList_OppyLineItem_Map__c setting: mapping){
                system.debug('========oppyItem=====' + setting);
                if(fb.get(setting.checkList_field__c) != null)
                    OppyLine.put(setting.Oppy_Line_Item__c, fb.get(setting.checkList_field__c));
                //system.debug('========oppy=====' + setting.Oppy_Line_Item__c + '====' + fb.get(setting.checkList_field__c));  
                //.debug('========oppyItemobj=====' + OppyLine);
                system.debug('========fieldmap=========' + fieldMap.get(String.valueof(setting).toUpperCase()));
                
                If(fieldMap.get(String.valueof(setting).toUpperCase()) != null){
                        fieldMap.Remove(String.valueof(setting).toUpperCase());
                } 
            }
            //Concatenate remaining fields into one string
            for(String field : fieldMap.keyset()){
                   if(fb.get(field) !=null){
                        if(notesforBO ==null)
                            notesforBO = fieldMap.get(field) + '= ' + String.valueOf(fb.get(field)) + '\r\n';
                        else 
                            notesforBO = notesforBO + fieldMap.get(field) + '= ' + String.valueOf(fb.get(field)) + '\r\n';
                    }
            }
            if(notesforBO !=null)
                OppyLine.Notes_For_BO__c = notesforBO;
            oppyLineList.add(OppyLine);
          // }
        }
        upsert oppyLineList;
    }
    
     public static void delete_current_lineItems(list<FOBOChecklist__c> checkList){
         set<Id> oppyIds = new set<Id>();
         //list<Opp_Product_Item__c> oppylines = new Opp_Product_Item__c ();
         for(FOBOChecklist__c fb: checkList){             
             oppyIds.add(fb.Opportunity__c);
         }
         list<Opp_Product_Item__c> oppylines = [SELECT Id FROM Opp_Product_Item__c WHERE Opportunity__c IN: oppyIds];
         delete oppylines;         
     }

}