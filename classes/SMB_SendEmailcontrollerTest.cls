@isTest
private class SMB_SendEmailcontrollerTest 
{
    public static testMethod void myUnitTest()
    {
        Opportunity myOpp = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        insert myOpp;
        Test.StartTest();
        PageReference p = Page.ESP;
        p.getParameters().put('p3_lkid', myOpp.Id);
        Test.setCurrentPageReference(p);
        ApexPages.StandardController stdCTR = new ApexPages.StandardController(myOpp);
        SMB_SendEmailcontroller obj = new SMB_SendEmailcontroller(stdCTR);
        obj.autoRun();
    }
     public static testMethod void myUnitTestNull()
    {
        Opportunity myOpp = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        insert myOpp;
        Test.StartTest();
        PageReference p = Page.ESP;
        p.getParameters().put('p3_lkid', null);
        Test.setCurrentPageReference(p);
        ApexPages.StandardController stdCTR = new ApexPages.StandardController(myOpp);
        SMB_SendEmailcontroller obj = new SMB_SendEmailcontroller(stdCTR);
        obj.autoRun();
    }
}