public class GenericRTContactSelectController {

    public Account account {get; private set;}
    public Account rcidAccount {get;set;}
    public Contact new_contact {get;set;}
    public String selectedContactId {get; set;}
    public String accountId {get;set;}
    public String targetObjType {get;set;}
    public String selectedCaseRecordType {get; set;}
    public String backUrl {get; set;}
	public String isConsole {get;set;}
    public String targetObjectLabel;
    public string searchTerm{get;set;}
    public List<Contact> contacts {get; private set;}
    public List<SelectOption> recordTypesList = new List<SelectOption>();
    public Set<String> filedNames = new Set<String>();
    Public Map<String,String> cobjFiedlIdMap = new Map<String,String>();
    private Schema.SObjectType targetType;
   
    
    public GenericRTContactSelectController(){
        accountId = ApexPages.currentPage().getParameters().get('aid');
        targetObjType = ApexPages.currentPage().getParameters().get('targetObjType');
        backUrl = ApexPages.currentPage().getParameters().get('backurl');
        targetType = Schema.getGlobalDescribe().get(targetObjType);
        this.isConsole =ApexPages.currentPage().getParameters().get('console');
       
        if(accountId!=null){
            rcidAccount = getParentRCID(accountId );
            List<Id> accountIdList = null;
            if(rcidAccount!=null){
           	   accountIdList= smb_AccountUtilityGlobal.getAccountsInHierarchy(accountId, rcidAccount.id);
            }else{//No Parent RCID account for the given account
                accountIdList = new List<Id>{accountId};
            }
            contacts = getContactList(accountIdList);
            account = [select name, RecordType.Name from Account where Id =:accountId];
        }    
        recordTypesList = getRecordTypeList();
        filedNames.add('Account');filedNames.add('Contact');
        
        initCustFieldIdMap(targetObjType,filedNames);
    }
    
    public List<SelectOption> getRecordTypesList(){
        return recordTypesList;
    }
    
    public PageReference continueToNext(){
        if(cobjFiedlIdMap.size()==0){
            showCustSettingNotSetupErrorMsg();
            return null;
        }
        
        if(selectedCaseRecordType =='Select'){
            ApexPages.Message msg= new ApexPages.Message(ApexPages.Severity.ERROR, label.Please_Select_a_Record_Type, label.Please_Select_a_Record_Type);
            ApexPages.AddMessage(msg);
            return null;
        }
        if(selectedContactId ==null || selectedContactId ==''){
            ApexPages.Message msg= new ApexPages.Message(ApexPages.Severity.ERROR,label.Please_Select_a_Contact , label.Please_Select_a_Contact);
            ApexPages.AddMessage(msg);
            return null;
        }
       
        String [] contactParts = selectedContactId.split(':');
        PageReference pr = new PageReference('/' + targetType.getDescribe().getKeyPrefix() + '/e');
        pr.getParameters().put('CF'+cobjFiedlIdMap.get('Account'), account.Name);
        pr.getParameters().put('CF'+cobjFiedlIdMap.get('Contact'), contactParts[1].trim());
        pr.getParameters().put('CF'+cobjFiedlIdMap.get('Account')+'_lkid', accountId);
        pr.getParameters().put('CF'+cobjFiedlIdMap.get('Contact')+'_lkid', contactParts[0]);
        
        pr.getParameters().put('ent', targetType.getDescribe().getName());
        pr.getParameters().put('RecordType', selectedCaseRecordType);
        pr.getParameters().put('retURL', account.Id);
        pr.setRedirect(true);
        return pr;
    }
   
    public void initCustFieldIdMap(String objectName,Set<String> fieldsSet){
        ContactSelectFieldMaping__c fieldMap = ContactSelectFieldMaping__c.getValues(objectName);
        if(fieldMap != null){
            cobjFiedlIdMap.put('Account', fieldMap.Account_Field_Id__c);
            cobjFiedlIdMap.put('Contact', fieldMap.Contact_Field_Id__c);     
        }else{
           showCustSettingNotSetupErrorMsg();
        }
        
        /*  Get the Object Filed Ids via Tooling API - 
        // Constructs the Tooling API wrapper (default constructor uses user session Id)
        ToolingAPILight toolingAPI = new ToolingAPILight();
         
        // Query CustomObject object by DeveloperName (note no __c suffix required)
        List<ToolingAPILight.CustomObject> customObjects = (List<ToolingAPILight.CustomObject>)
             toolingAPI.query('Select Id, DeveloperName, NamespacePrefix From CustomObject Where DeveloperName = \'Billing_Adjustment\'').records;
         
        // Query CustomField object by TableEnumOrId (use CustomObject Id not name for Custom Objects)
        ToolingAPILight.CustomObject customObject = customObjects[0];
        Id customObjectId = customObject.Id;
        List<ToolingAPILight.CustomField> customFields = (List<ToolingAPILight.CustomField>)
             toolingAPI.query('Select Id, DeveloperName, NamespacePrefix, TableEnumOrId From CustomField Where TableEnumOrId = \'' + customObjectId + '\'').records;
         
        for(ToolingAPILight.CustomField customField : customFields){
            String strid = customField.Id;
            strid = strid.substring(0,strid.length()-3);
            if(fieldsSet.contains(customField.DeveloperName)){
                cobjFiedlIdMap.put(customField.DeveloperName,strid);
            }
         }*/
    }
    
    private Account getParentRCID(String accid){
        Account acc = null;
        Account acc1 =null;
        try{
           acc = [select id,Name, parentId from Account where id =:accid]; 
           acc1 = [select Name, id, RecordType.Name  from Account where id = :acc.parentId]; 
        }catch(Exception e){
            return null;
        }  
        if(acc1.RecordType.Name == 'RCID'){
            return acc1;
        }else{
            return getParentRCID(acc1.id);
        }
    }
    
    public void onSearch() {
        try {
            list<Id> accountIdList = new list<Id>{accountId};
                if(rcidAccount!=null){
                    accountIdList= smb_AccountUtilityGlobal.getAccountsInHierarchy(accountId, rcidAccount.id);
                }
            if(string.isnotblank(searchTerm)&&searchTerm.length()>3&&!accountIdList.isempty()){
                system.debug('search term:'+searchTerm+', Account ID:'+account.id);
                List<List<sObject>> searchResult = [FIND :searchTerm IN ALL FIELDS RETURNING Contact (Name , Email, title, Active__c, Account.Name, Account.RecordType.Name, Phone Where AccountId=:accountIdList limit 50)];
                contacts = ((List<Contact>)searchResult[0]);
            }else{
                contacts = getContactList(accountIdList);
            }
        } catch (Exception e) { Util.emailSystemError(e); } return;
    }
    private Contact[] getContactList(List<Id> accountIds){
        return [Select Name , Email, title, Active__c, Account.Name, Account.RecordType.Name, Phone from Contact where AccountId in :accountIds order by Name limit 50];
    }
    
     private Contact loadContact(Id contid){
        return [Select Name , Email, title, Active__c, Account.Name, Account.RecordType.Name, Phone from Contact where id = :contid ];
    }
    private List<SelectOption> getRecordTypeList(){
        
        List<SelectOption> rtList = new List<SelectOption>();
        if(targetObjType !=null){
           // rtList.add(new SelectOption('Select','Choose Record Type'));
            Schema.DescribeSObjectResult dsr = targetType.getDescribe();
            for(Schema.RecordTypeInfo rtypInfo : dsr.getRecordTypeInfosByName().values()){
                if(rtypInfo.getName()=='Master')
                    continue;
                if(rtypInfo.isAvailable())
                rtList.add(new SelectOption(rtypInfo.getRecordTypeId(),rtypInfo.getName()));
                rtList.sort();
            }
        }
        if(rtList.size()>0){
        	rtList.add(0,new SelectOption('Select',Label.Select_Record_Type));    
        }else{
            rtList.add(new SelectOption('Select',Label.Select_Record_Type));
        }
        
        return rtList;
    }
    
    public String getTragetObjectName(){
        return targetObjType.substring(0,targetObjType.length()-3).replaceAll('_',' ');
    }
    
    public boolean displayPopup {get; set;}     
    
    public void closePopup() {        
        displayPopup = false;    
        new_contact = null;
    }     
    public void showPopup() {        
        displayPopup = true;
        id accId =null;
        if(this.rcidAccount==null){
            accId = account.id;
        }else{
            accId = rcidAccount.Id;
        }
     
        new_contact = new Contact(AccountId = accId);    
    }
    public PageReference goBack(){
        PageReference pr = new PageReference(backUrl);
        pr.setRedirect(true);
        return pr;
    }
    
    public PageReference saveContact(){
        try{
            insert new_contact;
            Contact cont = loadContact(new_contact.id);
            contacts.add(cont);
            displayPopup = false;
        }catch(Exception e){
            new_contact.addError(e.getMessage());
             displayPopup = true;
        }
        return null;
    }
    
    private void showCustSettingNotSetupErrorMsg(){
         String message ='Under \'Generic Contact Select Field Mapping\' , Custom Settings for ' + targetObjType + ', Account field Id and Contact field has not been Stup. Please Contact your System Admin to correct the issue.';
            ApexPages.Message msg= new ApexPages.Message(ApexPages.Severity.ERROR, message, message);
            ApexPages.AddMessage(msg);
    }
    
    public  String gettargetObjectLabel(){
        return this.targetType.getDescribe().getLabel();
    }
    
}