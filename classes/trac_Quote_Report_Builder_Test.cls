@isTest(SeeAllData=true)
private class trac_Quote_Report_Builder_Test {
    static testMethod void test_sendRegularEmailTest() {
        test.starttest();
        trac_Quote_Report_Builder tqrb = new trac_Quote_Report_Builder();
        tqrb.testDate = date.today();
        tqrb.testEmail = 'test@example.com';
        tqrb.sendDailyTest();
        tqrb = new trac_Quote_Report_Builder();
        trac_Quote_Report_Builder.Email cycleTimeEmail = tqrb.generateCycleTimeEmail(Date.today());
        tqrb = new trac_Quote_Report_Builder();
        tqrb.testDate = date.today();
        tqrb.testEmail = 'test@example.com';
        tqrb.sendWeeklyTest();
        tqrb = new trac_Quote_Report_Builder();
        tqrb.sendTodaysDailyEmail();
        tqrb = new trac_Quote_Report_Builder();
        tqrb.sendTodaysDailyEmail();
        tqrb = new trac_Quote_Report_Builder();
        tqrb.sendTodaysWeeklyEmail();
        tqrb = new trac_Quote_Report_Builder();
        trac_Quote_Report_Builder.Email dailyEmail = tqrb.generateDailyEmail(Date.today());
        test.stoptest();
    }
    /*
 
    static testMethod void test_sendWeeklyTest() {
        trac_Quote_Report_Builder tqrb = new trac_Quote_Report_Builder();
        tqrb.testDate = date.today();
        tqrb.testEmail = 'test@example.com';
        tqrb.sendWeeklyTest();
    }
    
    static testMethod void test_sendTodaysDailyEmail() {
        trac_Quote_Report_Builder tqrb = new trac_Quote_Report_Builder();
        tqrb.sendTodaysDailyEmail();
    }
    
    static testMethod void test_sendTodaysWeeklyEmail() {
        trac_Quote_Report_Builder tqrb = new trac_Quote_Report_Builder();
        tqrb.sendTodaysWeeklyEmail();
    }
    
    static testMethod void test_generateDailyEmail() {
        trac_Quote_Report_Builder tqrb = new trac_Quote_Report_Builder();
        trac_Quote_Report_Builder.Email e = tqrb.generateDailyEmail(Date.today());
    }
    */
    static testMethod void test_generateDailyEmailWithNullDate() {
        trac_Quote_Report_Builder tqrb = new trac_Quote_Report_Builder();
        Boolean error = false;
        try {
            trac_Quote_Report_Builder.Email e = tqrb.generateDailyEmail(null);
        } catch (Exception e) {
            error = true;
        }
        System.assert(error == true);
    }
    static testMethod void test_generateCycleTimeEmailWithNullDate() {
        trac_Quote_Report_Builder tqrb = new trac_Quote_Report_Builder();
        Boolean error = false;
        try {
            trac_Quote_Report_Builder.Email e = tqrb.generateCycleTimeEmail(null);
        } catch (Exception e) {
            error = true;
        }
        System.assert(error == true);
    }
}