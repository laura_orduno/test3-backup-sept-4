public class ChannelPortalCacheMgmtData {
    public List<ChannelOrgSalesRepsJSONExplicit> channelOrgSalesRepJsonObjList {get;set;}
    public List<ChannelOrgOutletsJSONExplicit> channelOrgOutletsJsonObjList {get;set;}
    public ChannelPortalCacheMgmtSrvReqRes.GetCacheListResponseType response {get;set;}
    public String salesRepName;
    public String currentOutletId;
    public String portalRole;
}