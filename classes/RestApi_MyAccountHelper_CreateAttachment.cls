global class RestApi_MyAccountHelper_CreateAttachment {

	// GetCasesRequestObject object classes     
	public class RequestUser {
		public String uuid = '';
	}

	public class RequestCreateAttachment {
		public String name = '';
		public String caseId = '';
		public Blob body = null;
	}

	// object to be returned as JSON response
	public CreateAttachmentResponse response = null;

	public RestApi_MyAccountHelper_CreateAttachment(String requestString) {
		// convert request to object
		CreateCaseAttachmentRequestObject createAttachmentRequest = (CreateCaseAttachmentRequestObject) JSON.deserialize(requestString, CreateCaseAttachmentRequestObject.class);
		response = new CreateAttachmentResponse(createAttachmentRequest);
	}

	public class CreateCaseAttachmentRequestObject extends RestApi_MyAccountHelper.RequestObject {
		public RequestUser user;
		public RequestCreateAttachment attachmentToCreate;
	}

	global class CreateAttachmentResponse extends RestApi_MyAccountHelper.Response {
		public String userName = '';
		public Attachment attachmentDetails = null;

		public CreateAttachmentResponse(CreateCaseAttachmentRequestObject request) {
			super(request);

			// CHECK REQUEST FOR COMPLETENESS

			// check if user object in request defined
			if (request.user == null) {
				throw new xException('User object for request not defined');
			}
			// check if uuid specified
			if (request.user.uuid == null) {
				throw new xException('Field "uuid" in user object for request not defined');
			}

			// check if case information was submitted
			if (request.attachmentToCreate == null) {
				throw new xException('No attachment information was submitted for creation');
			}

			// CONFIRM THAT DATA LIVES WITHIN SALESFORCE
			//System.debug('User: ' + request.user.uuid);
			// get user by uuid ( refers to the field "federationIdentifier" in the Salesforce User object)
			User u = RestApi_MyAccountHelper_Utility.getUser(request.user.uuid);

			// if user not found
			if (u == null) {
				throw new xException('User not found for uuid: ' + request.user.uuid);
			}

			if (!u.Contact.MASR_Enabled__c) {
				throw new xException('User is not MASR Enabled.');
			}

			Contact cont = RestApi_MyAccountHelper_Utility.getContact(u.ContactId);

			Case caseDetails = RestApi_MyAccountHelper_Utility.getCaseDetails(request.attachmentToCreate.caseId);

			if (caseDetails == null) {
				throw new xException('Case not found for Case Id: ' + request.attachmentToCreate.caseId);
			}

			// SD: Due to body possibly being quite large and exceeding heap limit (6 MB)
			// it is not recommended (in this case) to store an Attachment SObject in memory.
			// Instead we will do a direct insert and use the save result to obtain the ID
			/*Attachment attachmentToCreate = new Attachment();
			attachmentToCreate.ParentId = caseDetails.Id;
			attachmentToCreate.Name = request.attachmentToCreate.name;
			attachmentToCreate.Body = request.attachmentToCreate.body;*/

			try {
				Database.SaveResult sr = Database.insert(
						new Attachment(
								ParentId = caseDetails.Id,
								Name = request.attachmentToCreate.name,
								Body = request.attachmentToCreate.body
						)
				);

				if(sr.isSuccess()){
					update caseDetails; // Update Last Modified by field
					request.attachmentToCreate.body = null;
					attachmentDetails = RestApi_MyAccountHelper_Utility.getAttachmentDetails(sr.getId());
					request.attachmentToCreate.body = null;
				} else {
					throw new DMLException();
				}
				//insert attachmentToCreate;
				//attachmentDetails = RestApi_MyAccountHelper_Utility.getAttachmentDetails(attachmentToCreate.Id);
			} catch (DMLException e) {
				throw new xException('Attachment create error: ' + e.getMessage());
			}

		}
	}

}