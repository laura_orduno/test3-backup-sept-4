public class QuoteDocumentGenerator {
    public SBQQ__QuoteTemplate__c template {get; set;}
    public Boolean recombo {get; set;}
    public Boolean signatureBlock {get; set;}
    public String contactEmail {get; set;}
    
    public QuoteDocumentGenerator(SBQQ__QuoteTemplate__c template) {
        this.template = template;
        recombo = false;
        signatureBlock = false;
    }
    
    public Blob generate(String quoteIds) {
        String xsl = buildMarkup(quoteIds);
        string method='POST';
        string endpoint='https://telus.steelbrick.com/document2/xfo2pdf';
        HttpRequest req = new HttpRequest();
        quote_portal__c quotePortalDefault=quote_portal__c.getorgdefaults();
        if(quotePortalDefault!=null){
            if(string.isnotblank(quotePortalDefault.xfo2pdf_endpoint__c)){
                endpoint=quotePortalDefault.xfo2pdf_endpoint__c;
            }
            if(string.isnotblank(quotePortalDefault.xfo2pdf_method__c)){
                method=quotePortalDefault.xfo2pdf_method__c;
            }
        }
        req.setMethod(method);
        req.setEndpoint(endpoint);
        String body = 'base64DocumentSave=' + EncodingUtil.urlEncode(xsl, 'UTF-8');
        req.setBody(body);  
        Http http = new Http();
        return (Test.isRunningTest()) ? Blob.valueOf('Testing') : EncodingUtil.base64Decode(http.send(req).getBody());
    }
    
    public String preview(String quoteIds) {
        return buildMarkup(quoteIds);
    }
    
    private String buildMarkup(String quoteIds) {
        PageReference xslFO = Page.BAQuoteLayout;
        
        xslFO.getParameters().put('tid', template.Id);
        xslFO.getParameters().put('qids', quoteIds);
        if (signatureBlock) {
            xslFO.getParameters().put('signBlock', '1');
        }
        if (recombo) {
            xslFO.getParameters().put('recombo', '1');
            xslFO.getParameters().put('contactEmail', contactEmail);
        }
        xslFO.setRedirect(true);
        String xsl = Test.isRunningTest() ? '' : xslFO.getContent().toString();
        return EncodingUtil.base64Encode(Blob.valueOf(xsl.replaceAll('qq://company-logo', getCompanyLogoURL(template.SBQQ__LogoDocumentId__c)).replaceAll('together_within', 'together.within')));
    }
    
    private String getCompanyLogoURL(Id docId) {
        String[] parts = URL.getSalesforceBaseUrl().getHost().split('\\.');
        String inst = parts[parts.size()-3];
        return 'https://c.' + inst + '.content.force.com/servlet/servlet.ImageServer?id=' + docId + 
            '&amp;oid=' + UserInfo.getOrganizationId();             
    }
    
}