/*
 * Tests for trac_RepairUpdateCtlr
 * Part of the SMB Repairs project
 *
 * @author: Grant Adamson, Traction On Demand 
 */

@isTest
private class trac_RepairUpdateCtlrTest {
	private static testMethod void testCtor() {
		Case c = new Case(subject = 'test');
		insert c;
		
		trac_PageUtils.setParam('id',c.id);
		
		trac_RepairUpdateCtlr ctlr = new trac_RepairUpdateCtlr();
		
		system.assertEquals(c.id,ctlr.theCase.id);
	}
	
	
	private static testMethod void testDoUpdateRepair() {
		trac_RepairTestUtils.createTestEndpoint();
		
		Case c = new Case(subject = 'test', repair__c = trac_RepairTestUtils.REPAIR_ID_USE_CASE_ID);
		insert c;
		
		trac_PageUtils.setParam('id',c.id);
		
		trac_RepairUpdateCtlr ctlr = new trac_RepairUpdateCtlr();
		
		ctlr.doUpdateRepair();
		
		system.assertEquals(false,ctlr.theCase.Repair_Case_Id_Mismatch__c);
		
		system.assert(ctlr.success);
		system.assertEquals(false,ApexPages.hasMessages(ApexPages.Severity.ERROR));		
		
	}
	
	
	private static testMethod void testDoUpdateRepairIdMismatch() {
		trac_RepairTestUtils.createTestEndpoint();
		
		// insert a non-matching case first.
		Case mismatch = new Case(subject = 'mismatch');
		insert mismatch;
		
		Case c = new Case(subject = 'test', repair__c = trac_RepairTestUtils.REPAIR_ID_USE_CASE_ID);
		insert c;
		
		trac_PageUtils.setParam('id',c.id);
		
		trac_RepairUpdateCtlr ctlr = new trac_RepairUpdateCtlr();
		
		ctlr.doUpdateRepair();
		
		system.assertEquals(true,ctlr.theCase.Repair_Case_Id_Mismatch__c);
		
		system.assertEquals(false,ctlr.success);
		system.assertEquals(true,ApexPages.hasMessages(ApexPages.Severity.ERROR));
		
	}
	
	
	private static testMethod void testDoUpdateFailure() {
		trac_RepairTestUtils.createTestEndpoint();

		Case c = new Case(subject = 'test', repair__c = trac_RepairTestUtils.REPAIR_ID_FAILURE);
		insert c;
		
		trac_PageUtils.setParam('id',c.id);
		
		trac_RepairUpdateCtlr ctlr = new trac_RepairUpdateCtlr();
		
		ctlr.doUpdateRepair();

		system.assertEquals(false,ApexPages.hasMessages(ApexPages.Severity.INFO));
		system.assertEquals(true,ApexPages.hasMessages(ApexPages.Severity.ERROR));
	}
	
	
	private static testMethod void testDoUpdateMissingRepairId() {
		trac_RepairTestUtils.createTestEndpoint();

		Case c = new Case();
		insert c;
		
		trac_PageUtils.setParam('id',c.id);
		
		trac_RepairUpdateCtlr ctlr = new trac_RepairUpdateCtlr();
		
		ctlr.doUpdateRepair();

		system.assertEquals(false,ApexPages.hasMessages(ApexPages.Severity.INFO));
		system.assertEquals(true,ApexPages.hasMessages(ApexPages.Severity.ERROR));
		system.assertEquals(Label.REPAIR_ID_REQUIRED, ApexPages.getMessages()[0].getSummary());
	}
}