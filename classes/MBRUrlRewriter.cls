/**
*	@author Christian Wico - cwico@tractionondemand.com
*	@author - rename Dan Reich - dreich@tractiondemand.com
*	@description Friendly Url UrlRewriter for CCI community site / My Business Requests Site
*/

global class MBRUrlRewriter implements Site.UrlRewriter {

	public static final String OVERVIEW_PAGE = '/overview';
	public static final String VITILCARE_OVERVIEW_PAGE = '/vitilcare-overview';

    public static final String LOGIN_PAGE = '/Log-In';
	public static final String VITILCARE_LOGIN_PAGE = '/vitilcare-log-in';
    
	public static final String SUBMIT_REQUEST_PAGE = '/submit-request';
    public static final String VITILCARE_SUBMIT_REQUEST_PAGE = '/vitilcare-submit-request';
	public static final String MANAGE_REQUEST_PAGE = '/manage-requests';
	public static final String MY_PROFILE_PAGE = '/my-profile';
	public static final String UPDATE_MY_PROFILE_PAGE = '/update-my-profile';
	public static final String MY_CREDENTIAL_PAGE = '/my-credential';
	public static final String MY_CONTACT_PAGE = '/my-contact-info';
	public static final String MY_USERNAME_PAGE = '/my-username';
	public static final String MY_PASSWORD_PAGE = '/my-password';
	public static final String GET_HELP_PAGE = '/get-help';
	public static final String GET_ANSWER_PAGE = '/get-help-article';
	public static final String REQUEST_DETAIL_PAGE = '/request-details';
	public static final String FORGOT_PASSWORD_PAGE = '/forgot-password';
	public static final String CONFIRM_REGISTRATION_PAGE = '/confirm-registration';
	public static final String SELF_REGISTRATION_CONFIRM_PAGE = '/self-registration-confirm';    
	public static final String RESET_PASSWORD_PAGE = '/reset-password';
	
    // VITILcare
	public static final String VITILCARE_CONFIRM_REGISTRATION_PAGE = '/vitilcare-confirm-registration';

	public Map<String, PageReference> urlMap = new Map<String, PageReference>{
		OVERVIEW_PAGE => Page.MBROverview,
      
		LOGIN_PAGE => Page.MBRLogin,

		SUBMIT_REQUEST_PAGE => Page.MBRNewCase,
		MANAGE_REQUEST_PAGE => Page.MBRManageRequests,
//		MY_PROFILE_PAGE => Page.MBRMyProfile,
		MY_PROFILE_PAGE => Page.MBRMyProfileV2,
		UPDATE_MY_PROFILE_PAGE => Page.MBRUpdateMyProfile,
		GET_HELP_PAGE => Page.MBRGetHelp,
		REQUEST_DETAIL_PAGE => Page.MBRCaseDetail,
		FORGOT_PASSWORD_PAGE => Page.MBRForgotPassword,
		CONFIRM_REGISTRATION_PAGE => Page.MBRConfirmRegister,
		SELF_REGISTRATION_CONFIRM_PAGE => Page.MBRCommunitiesSelfRegConfirm,
		RESET_PASSWORD_PAGE => Page.MBRChangePassword,
		MY_CREDENTIAL_PAGE => Page.MBRMyProfileCredentials,
		MY_CONTACT_PAGE => Page.MBRMyProfileContactInfo,
		MY_USERNAME_PAGE => Page.MBRMyProfileUsername,
		MY_PASSWORD_PAGE => Page.MBRMyProfilePassword,
		GET_ANSWER_PAGE => Page.MBRGetHelpAnswer
            
        // VITILcare
        /*VITILCARE_LOGIN_PAGE => Page.VITILcareLogin,
        VITILCARE_OVERVIEW_PAGE => Page.VITILcareOverview,
        VITILCARE_SUBMIT_REQUEST_PAGE => Page.VITILcareNewCase,
        VITILCARE_CONFIRM_REGISTRATION_PAGE => Page.VITILcareConfirmRegister*/
    
	};

	public  Map<String, PageReference> vf2UrlMap = new Map<String, PageReference>();
	public  Map<String, PageReference> url2VFMap = new Map<String, PageReference>();

	public MBRUrlRewriter() {
		for (String k: urlMap.keySet()) {
			vf2UrlMap.put(k, urlMap.get(k));
		}
	}

	
	global PageReference mapRequestUrl(PageReference pageRef) {
		String url = pageRef.getUrl().substringBefore('?');
 
        for (String key: urlMap.keySet()) {
        	if (url.equalsIgnoreCase(key)) { 
        		PageReference mapPRef = urlMap.get(key);
        		mapPRef.getParameters().putAll(pageRef.getParameters());
        		mapPRef.setRedirect(pageRef.getRedirect());

        		return mapPRef;
        	}
        }
        
		return null;
	}
	
	global List<PageReference> generateUrlFor(List<PageReference> sfdcUrls) {
		List<PageReference> friendlyUrls = new List<PageReference>();

		for (PageReference pref: sfdcUrls) {
			String uri = pref.getUrl().toLowerCase().replace('/apex','').substringBefore('?');
			for (String key: vf2UrlMap.keySet()) {
				PageReference mapPref = vf2UrlMap.get(key);
				String mapUri = mapPref.getUrl().replace('/apex','');

				if (uri.equalsIgnoreCase(mapUri)) {
					PageReference newPageRef = new PageReference(key);
					newPageRef.getParameters().putAll(pref.getParameters());
					friendlyUrls.add(newPageRef);
					break;
				}
			}
		}

		return friendlyUrls;
	}
	
}