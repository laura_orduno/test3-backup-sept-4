@isTest (seeAllData = false)
public with sharing class DispatcherTest {
	
	static{
		Dispatcher.isTestFrom_DispatcherTest_Class = true;
	}

	private class DispatcherTestClass extends Dispatcher{
		public DispatcherTestClass(){
			super();
		}

		public override void init(){}
		public override void execute(){}

	}

	public static testMethod void constructorCoverage(){
		DispatcherTestClass d = new DispatcherTestClass();	
	}

	public static testMethod void coverLimitExecutionEvents(){
		Dispatcher.limitExecutionEvent(Dispatcher.TriggerEvent.AFTERINSERT);
		Dispatcher.limitExecutionCount('Test', 1);
		Dispatcher.countExecuted('Test');
		Dispatcher.resetProcessCount('Test');
		Dispatcher.resetProcessCount();
	}
	/*
	public static testMethod void coverDML(){
		DispatcherTestClass d = new DispatcherTestClass();
		Account a = new Account(Name = 'Test');
		d.insertList.add((SObject)a);
		d.updateList.add((SObject)a);
		d.deleteList.add((SObject)a);
		d.undeleteList.add((SObject)a);
		d.finish();
	}
	*/
	public static testMethod void coverDMLLists(){
		DispatcherTestClass d = new DispatcherTestClass();
		Account a = new Account(Name = 'Test');
		List<SObject> objList = new List<SObject>();
		objList.add((SObject)a);
		/*
		d.insertList.add((SObject)a);
		d.updateList.add((SObject)a);
		d.deleteList.add((SObject)a);
		d.undeleteList.add((SObject)a);
		
		Dispatcher.insertIndex.put(0, 1);
		Dispatcher.updateIndex.put(0, 1);
		Dispatcher.deleteIndex.put(0, 1);
		Dispatcher.undeleteIndex.put(0, 1);
		
		Dispatcher.insertIdentifier.put(0, 'Test');
        Dispatcher.updateIdentifier.put(0, 'Test');
        Dispatcher.deleteIdentifier.put(0, 'Test');
        Dispatcher.undeleteIdentifier.put(0, 'Test');
		
		d.insertDML(objList);
		d.insertDML((SObject)a);
		d.insertDML(objList, 'Test');

		d.updateDML(objList);
		d.updateDML((SObject)a);
		d.updateDML(objList, 'Test');

		d.deleteDML(objList);
		d.deleteDML((SObject)a);
		d.deleteDML(objList, 'Test');

		d.unDeleteDML(objList);
		d.unDeleteDML((SObject)a);
		d.unDeleteDML(objList, 'Test');
		*/
	}

	private class HandledDispatcherException extends Exception{}

	public static testMethod void coverExceptionHandler(){
		Dispatcher.handleException(new HandledDispatcherException('Exception Coverage'));
		Dispatcher.handleException(new HandledDispatcherException('Exception Coverage'), 'Test Identifier');
	}


}