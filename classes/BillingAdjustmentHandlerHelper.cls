public class BillingAdjustmentHandlerHelper{
   
    public static String PendingBUCAprval = 'Pending BUC Approval';
    
    public static String PendingCCAprval = 'Pending CC Approval';

    public static String PendingVPAprval = 'Pending VP Approval';
    
    public static void setReasonCode(Billing_Adjustment__c ba){
        if(BillingAdjustmentDataHelper.hasUpdatedAny(ba,new List<String>{'RC_Level_1__c',
               'RC_Level_2__c','RC_Level_3__c','RC_Product__c','RC_Responsible_Area__c'})){
                ba.Reason_Code__c = BillingAdjustmentDataHelper.getReasonCode(ba);
                BillingAdjustmentDataHelper.validateReleaseCode(ba);
         }
    } 
    
    public static void setParentAccount(Billing_Adjustment__c ba){
        if(BillingAdjustmentDataHelper.hasUpdatedAny(ba,new List<String>{'Account__c','Approval_Step_Number__c'})){
            ba.Parent_Account_Id__c = BillingAdjustmentDataHelper.getParentAccountId(ba);
        }
    }
    
    public static void setRequiredReleaseCode(Billing_Adjustment__c ba, List<Approval_Threshold__c> individualThreshold){
         //if(BillingAdjustmentDataHelper.hasUpdatedAny(ba,new List<String>{'Amount__c','GST__c','HST__c','PST__c','Late_Payment_Charge__c'})){
      if((ba.Status__c).equals('Submitted') || (ba.Status__c).equals('Draft')){
        Id currentApprover = ba.Next_Approver__c;

        if(Trigger.isBefore && Trigger.isInsert){
          if(ba.First_Approver__c =='Adjustment Requester'){
            currentApprover = ba.Requested_By__c;
          }else if(ba.First_Approver__c =='Other Manager'){
            currentApprover = ba.Other_Approval_Manager__c;
          }else{
            User usrMgr = BillingAdjustmentDataHelper.getManger(ba,ba.OwnerId);  
            if(ba.Approval_Status__c!='Approved'){
              if(usrMgr != null){
                currentApprover = usrMgr.id;
              }
            }
          }
        }

        ba.Required_Release_Code__c =String.valueOf(ApprovalThresholdHelper.getRequiredReleaseCode(ba.Total_Amount__c,ba.Transaction_Type__c,ba.Business_Unit__c, currentApprover, individualThreshold));
}
        //}
    }
    
    public static void setEmailNotifySalesPrimeUsers(Billing_Adjustment__c ba){ 
        
         if(BillingAdjustmentDataHelper.hasUpdatedAny(ba,new List<String>{'Account__c','Product_Type__c'})){
            User salesDir = BillingAdjustmentDataHelper.getUserById(BillingAdjustmentDataHelper.getSalesAssimnetUserIdByRole(ba, 'DIR'));
            
             
             //TC 02129808 - add sales assignemnt name - simon cheng
             if(BillingAdjustmentDataHelper.getSalesAssinmentUserByRole(ba,'DIR') != null){
            	 ba.DIR_User__c = BillingAdjustmentDataHelper.getSalesAssinmentUserByRole(ba,'DIR').Name;
             }else{
                 ba.DIR_User__c = null;
             }
             
             if (BillingAdjustmentDataHelper.getSalesAssinmentUserByRole(ba,'MD') != null){
                 ba.MD_User__c = BillingAdjustmentDataHelper.getSalesAssinmentUserByRole(ba,'MD').Name;
             }else{
                 ba.MD_User__c = null;
             }
            
            User nextApprover  = BillingAdjustmentDataHelper.getUserById(ba.Next_Approver__c);
            User accPrime =  BillingAdjustmentDataHelper.getSalesAssinmentUserByRole(ba, 'ACCTPRIME'); 
            if(nextApprover !=null && salesDir!=null && nextApprover.id != accPrime.id && !BillingAdjustmentDataHelper.isInUserHierarchy(nextApprover,salesDir.EmployeeId__c)){
                 ba.Notify_Email_1__c = BillingAdjustmentDataHelper.getSalesAssinmentUserEmailByRole(ba, 'ACCTPRIME');
               
                if(ba.Product_Type__c =='Wireless'){
                    if(BillingAdjustmentDataHelper.getSalesAssinmentUserEmailByRole(ba, 'WLS_DIR').length() > 0)
                        ba.Notify_Email_2__c = BillingAdjustmentDataHelper.getSalesAssinmentUserEmailByRole(ba, 'WLS_DIR');   
                    else    
                        ba.Notify_Email_2__c = BillingAdjustmentDataHelper.getSalesAssinmentUserEmailByRole(ba, 'DIR');  
                }else{
                    ba.Notify_Email_2__c = BillingAdjustmentDataHelper.getSalesAssinmentUserEmailByRole(ba, 'DIR');         
                }
            }else{
                ba.Notify_Email_1__c =null;
                ba.Notify_Email_2__c =null;
            } 
         }
    }
    
    public static void setNextApproverEmail(Billing_Adjustment__c ba){
        ba.Next_Approver_Email__c = BillingAdjustmentDataHelper.getUserEmail(ba.Next_Approver__c);
    }
    
    /**
     * Set Controller Approvers for Business Unit Controller and the Corporate Controller and 
     * the Business Unit Controller/Corportate Controller Required field based on the amount and transaction Type.
     * Code will only exceuted when if there is any update to the 'Amount__c','GST__c','HST__c','PST__c','Late_Payment_Charge__c' field.
     */
    public static void setControllerApprovers(Billing_Adjustment__c ba){
        if(BillingAdjustmentDataHelper.hasUpdatedAny(ba,new List<String>{'Business_Unit__c','Amount__c','GST__c','HST__c','PST__c','Late_Payment_Charge__c'})){
            String transType = ba.RecordType.Name !=null ? ba.RecordType.Name:ba.Transaction_Type__c;
            if(ApprovalThresholdHelper.needBUControllerApproval(ba.Total_Amount__c, transType) && getApprovalFlowSetting(ba).Controller_Approval__c ){
                 ba.Controller_Approver__c = BillingAdjustmentDataHelper.getBuController(ba);
                 ba.Controller_Approval_Required__c = true;
            }else{
                ba.Controller_Approver__c =null;
                ba.Controller_Approval_Required__c = false;
            }

            /**
             * START - TRACTION Added on FEB-22-2016
             */

            if(ApprovalThresholdHelper.needVPControllerApproval(ba.Total_Amount__c, transType) && getApprovalFlowSetting(ba).VP_Controller_Approval__c && BillingAdjustmentDataHelper.getVPController(ba) != null){
                ba.VP_Controller_Approver__c = BillingAdjustmentDataHelper.getVPController(ba); 
                ba.VP_Controller_Approval_Required__c = true;
            }else{
                ba.VP_Controller_Approver__c =null;
                ba.VP_Controller_Approval_Required__c = false;
            }
            /**
             * END
             */

            if(ApprovalThresholdHelper.needCorporateControllerApproval(ba.Total_Amount__c, transType) && getApprovalFlowSetting(ba).Controller_Approval__c){
                ba.Corporate_Controller_Approver__c = BillingAdjustmentDataHelper.getCoporateController(ba); 
                ba.Corporate_Controller_Approval_Required__c = true;
            }else{
                ba.Corporate_Controller_Approver__c =null;
                ba.Corporate_Controller_Approval_Required__c = false;
            }
         }
    }
    
    public static void setInitailNextApprover(Billing_Adjustment__c ba){
        if(BillingAdjustmentDataHelper.hasUpdatedAny(ba,new List<String>{'First_Approver__c','Requested_By__c','Next_Approver__c','RecordTypeId','Amount__c','GST__c','HST__c','PST__c','Late_Payment_Charge__c'}) 
            && ba.Status__c =='Draft'  ){
            User usr = BillingAdjustmentDataHelper.getUserById(ba.OwnerId);
            
            System.debug('ba.First_Approver__c' + ba.First_Approver__c);
            if(ba.First_Approver__c =='Adjustment Requester'){
                ba.Next_Approver__c = ba.Requested_By__c;
                System.debug('ba.Next_Approver__c Adjustment Requester' + ba.Next_Approver__c);
            }else if(ba.First_Approver__c =='Other Manager'){
                ba.Next_Approver__c = ba.Other_Approval_Manager__c;
                System.debug('ba.Next_Approver__c Other Manager' + ba.Next_Approver__c);
            }else{
                User usrMgr = BillingAdjustmentDataHelper.getManger(ba,((ba.Original_Owner__c != null) ? ba.Original_Owner__c : ba.OwnerId));  
                if(ba.Approval_Status__c!='Approved'){
                    if(usrMgr != null){
                        ba.Next_Approver__c = usrMgr.id;
                    }else{
                        throw new BillingAdjustmentException(Label.Manager_Not_Defined.replaceAll('<user>',usr.Name),ba);
                    }
                }
            }
                
            validateUserRelsaseCode(ba,BillingAdjustmentDataHelper.getUserById((ba.Original_Owner__c != null) ? ba.Original_Owner__c : ba.OwnerId)); 
            validateUserRelsaseCode(ba,BillingAdjustmentDataHelper.getUserById(ba.Next_Approver__c));   
            String ownerReleaseCode = BillingAdjustmentDataHelper.getUserById((ba.Original_Owner__c != null) ? ba.Original_Owner__c : ba.OwnerId).release_code__c;
            String nextApproverReleaseCode = BillingAdjustmentDataHelper.getUserById(ba.Next_Approver__c).release_code__c;
            ba.Approver_Release_Code__c = ownerReleaseCode;
            setNeedNextApprover(ba);
            if(ba.Need_Next_Approval__c && Integer.valueOf(ownerReleaseCode)<Integer.valueOf(ba.Required_Release_Code__c)){
               ba.Approver_Release_Code__c = nextApproverReleaseCode;
            }                
            
        }
    }
    
    public static void handleApproval(Billing_Adjustment__c ba){
        Integer approverRC = Integer.valueOf(ba.Approver_Release_Code__c);
        Integer requiredRC = Integer.valueOf(ba.Required_Release_Code__c);
        if(ba.Next_Approver__c != UserInfo.getUserId() && ba.Next_Hierarchy_Approver__c == null){
            validateUserRelsaseCode(ba,BillingAdjustmentDataHelper.getUserById(UserInfo.getUserId()));
            approverRC = Integer.valueOf(BillingAdjustmentDataHelper.getUserById(UserInfo.getUserId()).Release_Code__c);
            ba.Approver_Release_Code__c = BillingAdjustmentDataHelper.getUserById(UserInfo.getUserId()).Release_Code__c;
        }
        setNeedNextApprover(ba);
        Integer nextApproverRC;
        User nextApprover = null;
        updateContorllerApprovedStatus(ba);      
        if(ba.Next_Hierarchy_Approver__c ==null || switchToVPController(ba,approverRC)) {
            nextApprover = BillingAdjustmentDataHelper.getManger(ba, ba.Next_Approver__c);
            if(nextApprover==null &&  !ba.Need_Next_Approval__c){
                return; 
            }else{
                validateUserRelsaseCode(ba,nextApprover);
            }
            nextApproverRC  = Integer.valueOf(nextApprover.Release_Code__c);
            if(!handleSwithToSalseFlow(ba,nextApproverRC)){
                if (switchToCorpController(ba,approverRC)) {
                    updateNextApprover(ba,ba.Corporate_Controller_Approver__c,nextApprover.id,true,PendingCCAprval);       
                } else if (switchToBuController(ba,approverRC)) {
                    updateNextApprover(ba, ba.Controller_Approver__c,nextApprover.id,true,PendingBUCAprval);   
                } else if(switchToVPController(ba,approverRC) && !switchToBuController(ba,approverRC)){
                    updateNextApprover(ba, ba.VP_Controller_Approver__c,ba.Next_Hierarchy_Approver__c,true,PendingVPAprval);
                }else {
                    updateNextApprover(ba,nextApprover.id,null,ba.Need_Next_Approval__c,ba.Approval_Status__c); 
                    ba.Approver_Release_Code__c = String.valueOf(nextApproverRC);
                }
               
            }
        }else{
           User nextHierarchyApvr = BillingAdjustmentDataHelper.getUserById(ba.Next_Hierarchy_Approver__c);        
           validateUserRelsaseCode(ba,nextHierarchyApvr);
           updateContorllerApprovedStatus(ba);
           setNeedNextApprover(ba);
           ba.Approver_Release_Code__c = nextHierarchyApvr.Release_Code__c;
           updateNextApprover(ba,nextHierarchyApvr.id,null,ba.Need_Next_Approval__c,'Pending Approval');
        }
       updateLastApproverReleseCode(ba);
     }
   
    private static void updateLastApproverReleseCode(Billing_Adjustment__c ba){
        if(ba.Last_Approver_User__c != null){
             ba.Last_Approver_Release_Code__c =  Integer.valueOf(BillingAdjustmentDataHelper.getUserById(ba.Last_Approver_User__c).Release_Code__c);     
        }
        
    }
    
    private static boolean switchToCorpController(Billing_Adjustment__c ba, Integer approverRC){
       return (getApprovalFlowSetting(ba).Controller_Approval__c && ((approverRC >= 7) || (approverRC >= Integer.valueOf(ba.Required_Release_Code__c))))
                   && isCCApprovalPending(ba) && !ba.Corporate_Controller_Approved__c && !isBCApprovalPending(ba) && !isVPApprovalPending(ba);
    }
    
    private static boolean switchToBuController(Billing_Adjustment__c ba, Integer approverRC){
        
        return (getApprovalFlowSetting(ba).Controller_Approval__c && ((approverRC >= 6) || (approverRC >=Integer.valueOf(ba.Required_Release_Code__c))))
               && isBCApprovalPending(ba) && !ba.Controller_Approved__c;
    }

    /**
     * switchToVPController
     * @description Determine if the next approve is the VP controller
     * @author Thomas Tran, Traction on Demand
     * @date FEB-22-2016
     */
    private static boolean switchToVPController(Billing_Adjustment__c ba, Integer approverRC){
        
        return (getApprovalFlowSetting(ba).VP_Controller_Approval__c && ((approverRC >= 6) || (approverRC >=Integer.valueOf(ba.Required_Release_Code__c))))
               && isVPApprovalPending(ba) && !ba.VP_Controller_Approved__c && !isBCApprovalPending(ba);
    }
    
    private static void updateNextApprover(Billing_Adjustment__c ba, Id nxtApvrId ,Id nxtHierarchyApvrId, Boolean needNxtaprvl, String aprvlStatus){
        if(ba.Next_Hierarchy_Approver__c == null){
            ba.Last_Approver_User__c = UserInfo.getUserId(); 
        }
        if(aprvlStatus.endsWithIgnoreCase(PendingCCAprval) || aprvlStatus.endsWithIgnoreCase(PendingBUCAprval)){
            ba.Last_Approver_User__c =ba.Next_Approver__c; 
        }
        ba.Next_Approver__c = nxtApvrId;
        ba.Next_Hierarchy_Approver__c = nxtHierarchyApvrId;
        ba.Need_Next_Approval__c =needNxtaprvl;
        ba.Approval_Status__c =aprvlStatus;
    }
    
    private static  boolean handleSwithToSalseFlow(Billing_Adjustment__c ba, integer nextApproverRC){
         User SalesVP = BillingAdjustmentDataHelper.getSalesAssinmentUserByRole(ba, 'MD');
         
          if(getApprovalFlowSetting(ba).Engage_Sales_Approval__c && !ba.Sales_VP_Engaged__c && SalesVp != null && nextApproverRC >= 6 ){
              if (ba.Next_Approver__c != salesVP.id) { // next approver is VP or higher, so time to send to Sales VP
                  ba.Notify_Email_1__c =  BillingAdjustmentDataHelper.getUserEmail(BillingAdjustmentDataHelper.getMangerId(ba, ba.Next_Approver__c));  
                  ba.Notify_Email_2__c = null;
              }
              ba.Last_Approver_User__c =ba.Next_Approver__c; 
              ba.Next_Approver__c = SalesVP.id;
              ba.Sales_VP_Engaged__c = true;
              validateUserRelsaseCode(ba,SalesVP);
              ba.Approver_Release_Code__c = SalesVP.Release_Code__c;
              return true;
            }
        return false;
    }
    
       
    public static void updateContorllerApprovedStatus(Billing_Adjustment__c ba){
        if(ba.Approval_Status__c == PendingCCAprval){
           ba.Corporate_Controller_Approved__c = true;          
        }
        if(ba.Approval_Status__c == PendingBUCAprval){
           ba.Controller_Approved__c = true;             
        }

        if(ba.Approval_Status__c == PendingVPAprval){
           ba.VP_Controller_Approved__c = true; 
        }
    }
    
    public static void validateUserRelsaseCode(Billing_Adjustment__c ba , User usr){
    System.debug('usr ::: ' + usr.Release_code__c);
            if(usr.Release_code__c !=null && usr.Release_code__c != '' && usr.Release_code__c.isNumeric()){
                if((!(Integer.valueOf(usr.Release_code__c)>0)) && (ba.OwnerId != usr.id)){ // owner can have a zero release code.
                    throw new BillingAdjustmentException(Label.ReleseCode_ZERO + ' : ' + usr.Name,ba);
                }else if(!usr.IsActive){
                    if(ba.Status__c=='Draft'){
                       throw new BillingAdjustmentException(Label.Authorizer_Is_Not_Active.replaceAll('<user>',usr.Name),ba);   
                    }else{
                        throw new BillingAdjustmentException(Label.ReleseCode_NOT_DEFINED.replaceAll('<user>',usr.Name),ba);    
                    }
                }
            }else{
                if(ba.OwnerId == usr.id){
                   throw new BillingAdjustmentException(Label.ReleseCode_NOT_DEFINED_OWNER.replaceAll('<user>',usr.Name),ba); 
                }else if(ba.Status__c=='Draft'){
                    throw new BillingAdjustmentException(Label.ReleseCode_NOT_DEFINED_DRAFT.replaceAll('<user>',usr.Name),ba);
                }else{
                    throw new BillingAdjustmentException(Label.ReleseCode_NOT_DEFINED.replaceAll('<user>',usr.Name),ba);
                } 
            }
    }
   
    
    public static void setNeedNextApprover(Billing_Adjustment__c ba){
        if(ba.Approver_Release_Code__c != null && ba.Required_Release_Code__c !=null ){
            Integer approverReleaseCode = Integer.valueOf(ba.Approver_Release_Code__c);
            Integer requiredReleaseCode = Integer.valueOf(ba.Required_Release_Code__c);

            if((approverReleaseCode < requiredReleaseCode) || isBCApprovalPending(ba) || isCCApprovalPending(ba)){
                ba.Need_Next_Approval__c = true;
            }else{
                ba.Need_Next_Approval__c = false;
            }
        }
        if(!getApprovalFlowSetting(ba).Relase_Code_Base_Approval__c){
             ba.Need_Next_Approval__c = false;
        }
    }
    
    /*
     * Check if the Business Unit Controller Approval is Pending or Not
     */ 
    public static Boolean isBCApprovalPending(Billing_Adjustment__c ba){
        return ba.Controller_Approval_Required__c && !ba.Controller_Approved__c ;
    }

    /**
     * isVPApprovalPending
     * @description determines if the VP approval is pending
     * @author Thomas Tran, Traction on Demand
     * @date FEB-22-2016
     */
    public static Boolean isVPApprovalPending(Billing_Adjustment__c ba){
        return ba.VP_Controller_Approval_Required__c && !ba.VP_Controller_Approved__c ;
    }
    
    /*
     * Check if the Corporate Controller Approval is Pending or Not
     */ 
    public static Boolean isCCApprovalPending(Billing_Adjustment__c ba){
        return ba.Corporate_Controller_Approval_Required__c && !ba.Corporate_Controller_Approved__c ;
    }
    
    public static Billing_Adjustment_Approval_Flow__c getApprovalFlowSetting(Billing_Adjustment__c ba){
        Billing_Adjustment_Approval_Flow__c approvalFlowSettings;
        if(!Test.isrunningtest()){
           approvalFlowSettings = Billing_Adjustment_Approval_Flow__c.getValues(ba.Transaction_Type__c);
           approvalFlowSettings.Engage_Sales_Approval__c = approvalFlowSettings.Engage_Sales_Approval__c && !ba.Sales_Engagement_Override__c;
           return approvalFlowSettings;
        }else{
            return  new Billing_Adjustment_Approval_Flow__c(Relase_Code_Base_Approval__c=true,Engage_Sales_Approval__c =true,Controller_Approval__c =true, VP_Controller_Approval__c = true);
        }
    }

    /**
     * setPMApproval
     * @description Determines if the Product Manager is required
     *              to be the first approver.
     * @author Thomas Tran, Traction on Demand
     * @date FEB-24-2016
     */
    public static void setPMApproval(Billing_Adjustment__c ba){
        if(ba.Status__c == 'Submitted' && ba.Approval_Step_Number__c == 0){
          String apiName = '';
            if(BillingAdjustmentDataHelper.patSettings.containsKey(Schema.SObjectType.Billing_Adjustment__c.getRecordTypeInfosById().get(ba.RecordTypeId).getname())){
              apiName = BillingAdjustmentDataHelper.patSettings.get(Schema.SObjectType.Billing_Adjustment__c.getRecordTypeInfosById().get(ba.RecordTypeId).getname()).Threshold_API_Name__c;
            }
          
          Approval_Threshold__c pmThreshold = BillingAdjustmentDataHelper.productCodeToApprovalThreshold.get(ba.Product_Type__c + ba.RC_Product__c);

          if(pmThreshold != null){
            if(String.isNotEmpty(apiName) && pmThreshold.get(apiName) != null){
              Decimal threshold = Decimal.valueOf(String.valueOf(pmThreshold.get(apiName)));
              Id pmOwnerId = pmThreshold.OwnerId;

              if(ba.Product_Manager_Approval_Required__c){
                ba.Original_Owner__c = ba.OwnerId;
                ba.OwnerId = pmOwnerId;
              }
            }
          }
        }
    }

    /**
     * setPMApproval
     * @description Determines if the Product Manager is required
     *              to be the first approver.
     * @author Thomas Tran, Traction on Demand
     * @date FEB-24-2016
     */
    public static void setPMRequired(Billing_Adjustment__c ba){
        if(BillingAdjustmentDataHelper.hasUpdatedAny(ba,new List<String>{'Total_Amount__c', 'Product_Type__c','RC_Product__c'})){

            String apiName = '';
            if(BillingAdjustmentDataHelper.patSettings.containsKey(Schema.SObjectType.Billing_Adjustment__c.getRecordTypeInfosById().get(ba.RecordTypeId).getname())){
              apiName = BillingAdjustmentDataHelper.patSettings.get(Schema.SObjectType.Billing_Adjustment__c.getRecordTypeInfosById().get(ba.RecordTypeId).getname()).Threshold_API_Name__c;
            }
            
            Approval_Threshold__c pmThreshold = BillingAdjustmentDataHelper.productCodeToApprovalThreshold.get(ba.Product_Type__c + ba.RC_Product__c);

            if(pmThreshold != null){
              if(String.isNotEmpty(apiName) && pmThreshold.get(apiName) != null){
                Decimal threshold = Decimal.valueOf(String.valueOf(pmThreshold.get(apiName)));

                if(ba.Total_Amount__c >= threshold){
                  ba.Product_Manager_Approval_Required__c = true;
                } else{
                  ba.Product_Manager_Approval_Required__c = false;
                }

              }
            } else{
              ba.Product_Manager_Approval_Required__c = false;
            }
        }
    } 

    /**
     * restoreOriginalOwner
     * @description Restores the original owner the record when
     *              the product manager is set from true to false.
     * @author Thomas Tran, Traction on Demand
     * @date FEB-24-2016
     */
    public static void restoreOriginalOwner(Billing_Adjustment__c ba){
      if(ba.Original_Owner__c != null && (ba.Product_Manager_Approved__c || (ba.Status__c == 'Draft'))){
        SYstem.debug('Executing Restoration');
        ba.OwnerId = ba.Original_Owner__c;
        ba.Original_Owner__c = null;
      }
    }
    
    public class BillingAdjustmentException extends Exception{
        
        public BillingAdjustmentException(String errorMessage,sObject obj){
           this(errorMessage);
           obj.addError(errorMessage);
        }
    }

    
}