global class smb_SVOC_MyOpenOpp
{
    public Account acct {get; set;}
    public transient List<Opportunity> matchedOpportunity {get; set;}
    Date queryEndDate = System.today() - 7;
    public transient List<OCOM_QuoteOrderVO> matchedQuoteOrders {get;set;}
    public transient List<Service_Request__c> matchedServReq {get;set;}    
    public String RCID {get;set;}   

  
    public String pageInstanceId  {
        get {
            if (pageInstanceId == null) {
                pageInstanceId = smb_GuidUtil.NewGuid();
            }
            return pageInstanceId;
        }
        private set;
    }
     
        
    global transient List<Id> AllAccountIdsInHierarchy {get;set;}
        
        public PageReference init() 
        {           
            //Id accountId = ApexPages.currentPage().getParameters().get('id');
            Id UserId= UserInfo.getUserId();

            matchedQuoteOrders = new List<OCOM_QuoteOrderVO>();
          
            Set<String> exemptStages = new Set<String>{'Quote Cancelled'};
            matchedOpportunity = [select Id, Name,Account.id, Owner.Name,Accname__c, Total_Contract_Renewal__c, StageName, CloseDate,Credit_Assessment__r.name,
                  credit_request_status__c, Credit_Assessment__c , CreatedDate, Order_ID__c, Type, 
                                  Requested_Due_Date__c, Condition__c, Service_Address__c     
                from Opportunity 
                where 
                (StageName != 'Quote Cancelled') and (StageName != 'Order Request Cancelled') and (StageName != 'Order Request Completed') and (                                                       
                (recordtype.name='SMB Care Opportunity' and Order_ID__c != null and createdbyId =:UserId) or
                (recordtype.name='SMB Care Amend Order Opportunity' and Order_ID__c != null and createdbyId =:UserId) or
                (recordtype.name='SMB Care Locked Order Opportunity' and Order_ID__c != null and createdbyId =:UserId) or
                (recordtype.name='SRS Order Request' and Order_ID__c != null and createdbyId =:UserId))  
                order by Order_ID__c DESC limit 1000];
             

            List<Order> matchedOrders = [select id, status,vlocity_cmt__OpportunityId__c, Opportunity_Name__c, Account.id, Condition__c,name, OrderNumber, Account.Name, 
                                         	vlocity_cmt__OpportunityId__r.status__c, vlocity_cmt__OpportunityId__r.StageName,
                                         	order_number__c, CreatedDate, Type, Service_Address_Text__c, RequestedDate__c
                                            from Order where createdbyId =:UserId and Status != 'Completed'
                                            order by CreatedDate DESC ];
                                              
            List<Quote> quoteList = [Select q.id, q.status,q.Account_Id__c, q.QuoteNumber,q.Opportunity.Name,q.Opportunity.Id,q.Condition__c, q.Name,Quote.Opportunity.Account.Name, q.ExpirationDate,q.CreatedDate, q.vlocity_cmt__Type__c, Service_Address_Text__c, q.Opportunity.owner.name
                                 From Quote q 
                                 where q.OpportunityId in (select o.id 
                                                           from Opportunity o 
                                                           where o.Record_Type_Name__c = 'Vlocity_User' and o.createdbyId =:UserId) and q.ExpirationDate >:queryEndDate
                                order by q.CreatedDate DESC ];
            
            if(matchedOpportunity != null && matchedOpportunity.size() > 0)
            {
                for(Opportunity opp :matchedOpportunity)
                {
                    OCOM_QuoteOrderVO svOrder = new OCOM_QuoteOrderVO();
                    svOrder.Condition = opp.Condition__c;
                    svOrder.OrderId = opp.Order_ID__c; 
                    svOrder.Id = opp.Id;
                    svOrder.Name = opp.Name;
                    svOrder.OrderType = opp.Type;
                    svOrder.Status = opp.StageName;
                    svOrder.CreatedDate = opp.CreatedDate;
                    svOrder.OwnerFullName = opp.Owner.Name;
                    svOrder.RequestedDueDate = opp.Requested_Due_Date__c;
                    svOrder.ServiceAddress = opp.Service_Address__c;
                    svOrder.Account=opp.Account.id;
                    svorder.AccountName= opp.Accname__c;
                    svOrder.Credit_Assessment = opp.Credit_Assessment__r.name;
                    svOrder.Credit_Request_Status = opp.Credit_request_status__c;
                    svOrder.OppID = opp.Id;
                    svOrder.OppName = opp.Name;
                    
                    matchedQuoteOrders.add(svOrder);   
                    
                }
            }
            //Quotes lstquotes
        
            if(quoteList != null && quoteList.size() > 0)
            {
                for(quote qte :quoteList)
                {
                    OCOM_QuoteOrderVO svOrder = new OCOM_QuoteOrderVO();
                    svOrder.Condition = qte.Condition__c;
                    svOrder.OrderId = qte.QuoteNumber; 
                    svOrder.Id = qte.Id;
                    svOrder.Name = 'Quote';
                    svOrder.OrderType = qte.vlocity_cmt__Type__c;
                    svOrder.RecordType = 'vlocity_quote';
                    svOrder.ProductType = '';
                    svOrder.Account=qte.Account_Id__c;
                    svorder.AccountName= qte.Opportunity.Account.Name;
//                    svOrder.Status = 'qte.status';
                    svOrder.Status = qte.status;
                    svOrder.CreatedDate = qte.CreatedDate;
                    svOrder.ExpirationDate= qte.ExpirationDate;
                    svOrder.OwnerFullName = qte.Opportunity.Owner.name;
                    //svOrder.RequestedDueDate = '';
                    svOrder.ServiceAddress = qte.Service_Address_Text__c;
                    svOrder.OppID = qte.Opportunity.Id;
                    svOrder.OppName = qte.Opportunity.Name;
                    
                    matchedQuoteOrders.add(svOrder);   
                    
                }
            }
            
            if(matchedOrders != null && matchedOrders.size() > 0)
            {
                for(Order odr :matchedOrders)
                {
                    OCOM_QuoteOrderVO svOrder = new OCOM_QuoteOrderVO();
                    svOrder.Condition = odr.Condition__c;
                    svOrder.OrderId = odr.OrderNumber; 
                    svOrder.Id = odr.Id;
                    svOrder.Name = 'Order';
                    svOrder.OrderType = odr.Type;
                    svOrder.RecordType = 'vlocity_order';
                    svOrder.ProductType = '';
                    svOrder.Account=odr.Account.id;
                    svOrder.AccountName=odr.Account.Name;
                    // ensure status field gets populated
                    svOrder.Status = odr.status;
                    svOrder.Status = String.isEmpty(svOrder.Status) ? odr.vlocity_cmt__OpportunityId__r.status__c : svOrder.Status;
                    
                    svOrder.CreatedDate = odr.CreatedDate;
                   // svOrder.OwnerFullName = odr.Owner.name;
                    // svOrder.OwnerFullName = odr.Owner.name;
                    svOrder.RequestedDueDate = odr.RequestedDate__c;
                    svOrder.ServiceAddress = odr.Service_Address_Text__c;
                    svOrder.OppID = odr.vlocity_cmt__OpportunityId__c;
                    svOrder.OppName = odr.Opportunity_Name__c ;
                    
                    matchedQuoteOrders.add(svOrder);   
                    
                }
            }
            
            if (matchedQuoteOrders != null)
                matchedQuoteOrders.sort();
            integer quotesTotal = matchedQuoteOrders.size();
            if(quotesTotal > 1000)
            {
                for(Integer i = 1000; i < quotesTotal; i++)
                    matchedQuoteOrders.remove(1000);
            }
            
            
            matchedServReq = [select id, Legacy_Order_ID__c, Opportunity__r.Order_ID__c from Service_Request__c where Opportunity__c IN: matchedOpportunity];

            try{
             //   getLegacyOrders(acct.RCID__c, '1000');          
            }catch(CalloutException e){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Web Service Exception occured, Please try again'));         
            }
                 
            return null;
        }
 
       
       
/***** Not Users 
        private static Account getAccount(Id accountId) {
        if (accountId == null) return null;
        
        return [SELECT  Id, ParentId, Name, RCID__c, RecordTypeId, RecordType.DeveloperName, Account_Status__c
                FROM Account 
                WHERE Id =: accountId];
        }
*/

    @RemoteAction
    global static List<Opportunity> getCases(Id acctId, Id parentId){
      if(acctId == null) return null;

      //system.debug('***** acctId: ' + acctId);

      list<Id> allAccountIdsInHierarchy = new list<Id>();
      allAccountIdsInHierarchy = smb_AccountUtility.getAccountsInHierarchy(acctId, parentId);

      list<Opportunity> matchedOpportunity = new list<Opportunity>();
            matchedOpportunity = [SELECT Id, Name, Owner.Name, Total_Contract_Renewal__c, StageName, CloseDate, CreatedDate, Order_ID__c, Condition__c, Order_Type__c, Description, Status__c, Requested_Due_Date__c   
                                                FROM Opportunity
                                                WHERE AccountId IN :allAccountIdsInHierarchy ];         

      //system.debug('***** matchedOpportunity : ' + matchedOpportunity );
      return matchedOpportunity ;            
    }
        
  
        public String baseUrl {
        get {
            return 'https://' + URL.getSalesforceBaseUrl().getHost().substring(2,6) + '.salesforce.com';
        }
    }
         

}