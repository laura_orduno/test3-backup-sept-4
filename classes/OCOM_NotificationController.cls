public class OCOM_NotificationController 
{
    public  String maxSpeed{get;set;}
    public String notificationJSON{get;set;}
    public List<OCOM_Notification> notifications{get;set;}
    public integer count;
    
    public OCOM_NotificationController()
    {
        System.debug('##***## in OCOM_NotificationController const...');
        count = 1;
        
		if(notifications == null)
        {
            notifications = new List<OCOM_Notification>();
            System.debug('##***## initialize notifications...');
        }
        else
        {
            notifications.clear();
            System.debug('##***## clear notifications...');
        }
    }
    public OCOM_NotificationController(ApexPages.StandardController controller) 
    {
        System.debug('##**## in OCOM_NotificationController const...');
        count = 1;
        
		if(notifications == null)
        {
            notifications = new List<OCOM_Notification>();
            System.debug('##**## initialize notifications...');
        }
        else
        {
            notifications.clear();
            System.debug('##**## clear notifications...');
        }  
        
        OCOM_Notification msg = new OCOM_Notification();
        msg.type = 'Info';
        msg.message = 'Test message';
        msg.id = 'notif'+count++;
        
        notifications.add(msg);
    }
    
    
    public void generateJSON()
    {
        getMessages();
        if(notifications != null)
        {
            System.debug('##**## in generateJSON');
            notificationJSON = JSON.serializePretty(notifications);
        }
        	
        System.debug('##**## Generated JSON: ' +notificationJSON);
    }
    public void addNotification(String notificationId, String inType, String inMessage)
    {
        System.debug('##**## adding new notification. msg Type: ' +inType +' Msg: ' +inMessage);
        if(notifications == null)
            notifications = new List<OCOM_Notification>();
        
        OCOM_Notification msg = new OCOM_Notification();
        msg.type = inType;
        msg.message = inMessage;
        msg.id = notificationId;
        
        notifications.add(msg);
    }
    public void  getMessages() 
    {
        notifications.clear();
        count = 1;
        Id recordId=ApexPages.currentPage().getParameters().get('id');
        if(String.isNotBlank(recordId))
        {
            String objectType = recordId.getSObjectType().getDescribe().getName();
            if (objectType == 'Quote')
            {
                List<Quote> quotes = [Select id, Maximum_Speed__c, vlocity_cmt__ValidationMessage__c From Quote Where Id = :recordId];
                if(quotes.get(0)!=null)
                {
                    maxSpeed=quotes.get(0).Maximum_Speed__c;
                    if(maxSpeed!=null && maxSpeed.trim().length()>0)
                    {
                        String strSpeed = maxSpeed.trim().substring(0, maxSpeed.trim().indexOf(' '));
                        Integer speed = Integer.valueOf(strSpeed);
                        if(speed < 25 )
                        {
                           String msg = 'Note: Maximum download speed available at this location is '+maxSpeed;
                           addNotification('notif1', 'Info', msg); 
                        }
                    }
                    if(quotes.get(0).vlocity_cmt__ValidationMessage__c != null && quotes.get(0).vlocity_cmt__ValidationMessage__c.trim().length()>0)
                        addNotification('notif2', 'Warning', quotes.get(0).vlocity_cmt__ValidationMessage__c);
                }                
            }
			if (objectType == 'Order')
            {
                List<Order> quotes = [Select id, Maximum_Speed__c, vlocity_cmt__ValidationMessage__c From Order Where Id = :recordId];
                if(quotes.get(0)!=null)
                {
                    maxSpeed=quotes.get(0).Maximum_Speed__c;
                    if(maxSpeed!=null && maxSpeed.trim().length()>0)
                    {
                        String strSpeed = maxSpeed.trim().substring(0, maxSpeed.trim().indexOf(' '));
                        Integer speed = Integer.valueOf(strSpeed);
                        if(speed < 25 )
                        {
                           String msg = 'Note: Maximum download speed available at this location is '+maxSpeed;
                           addNotification('notif1', 'Info', msg); 
                        }
                    }
                    if(quotes.get(0).vlocity_cmt__ValidationMessage__c != null && quotes.get(0).vlocity_cmt__ValidationMessage__c.trim().length()>0)
                        addNotification('notif2', 'Warning', quotes.get(0).vlocity_cmt__ValidationMessage__c);
                }                
            }
        } 

    }
    
    public class OCOM_Notification
    {
        public String type{get;set;}
        public String message{get;set;}
        public String id{get;set;}
    }
}