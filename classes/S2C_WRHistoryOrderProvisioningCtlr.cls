public with sharing class S2C_WRHistoryOrderProvisioningCtlr {
    private Map<String, String> serviceRequestLabelMap = getLabels('Service_Request__c');
    private Map<String, String> serviceRequestContactLabelMap = getLabels('Service_Request_Contact__c');
    private Map<String, String> srsServiceRequestChargeLabelMap = getLabels('SRS_Service_Request_Charge__c');
    private Map<String, String> srsServiceAddressLabelMap = getLabels('SRS_Service_Address__c');
    private Id serviceRequestId {get; set;}

    public S2C_WRHistoryOrderProvisioningCtlr(ApexPages.StandardController controller) {
        Work_Request__c currentWR = (Work_Request__c) controller.getRecord();
        List<Work_Request__c> wrList = [SELECT Service_Request__c FROM Work_Request__c WHERE Id = :currentWR.Id];
        if(!wrList.isEmpty() && String.isNotBlank(wrList[0].Service_Request__c)){
            serviceRequestId = wrList[0].Service_Request__c;
        }
    }

    public List<SelectHistory> getSelectHistories(){
        List<SelectHistory> selectHistories = new List<SelectHistory>();
        List<Service_Request__History> srHistory = new List<Service_Request__History>();
        List<Service_Request_Contact__History> srcHistory = new List<Service_Request_Contact__History>();
        List<SRS_Service_Request_Charge__History> srsSRCHistory = new List<SRS_Service_Request_Charge__History>();
        List<SRS_Service_Address__History> srsSAHistory = new List<SRS_Service_Address__History>();

        if(String.isNotBlank(serviceRequestId)){
            srHistory = [SELECT ParentId,Field,  Parent.Name, CreatedDate, CreatedBy.Name, OldValue, NewValue 
                 FROM Service_Request__History WHERE Field IN (:'Existing_Service_ID__c', :'Service_Identifier_Type__c', :'SRS_PODS_Product__c',:'Target_Date__c',:'Service_Request_Type__c',:'Target_Date_Type__c')
                 AND ParentId = : serviceRequestId
                 ORDER BY CreatedDate Desc
                 LIMIT 200];

            srcHistory = [SELECT ParentId,Field,  Parent.Name, CreatedDate, CreatedBy.Name, OldValue, NewValue 
                    FROM Service_Request_Contact__History
                    WHERE Parent.Service_Request__c = :serviceRequestId
                    ORDER BY CreatedDate Desc
                    LIMIT 200];

            srsSRCHistory = [SELECT ParentId,Field,  Parent.Name, CreatedDate, CreatedBy.Name, OldValue, NewValue 
                     FROM SRS_Service_Request_Charge__History
                     WHERE Parent.Service_Request__c = :serviceRequestId
                     ORDER BY CreatedDate Desc
                     LIMIT 200];

            srsSAHistory = [SELECT ParentId,Field,  Parent.Name, CreatedDate, CreatedBy.Name, OldValue, NewValue 
                     FROM SRS_Service_Address__History 
                     WHERE Parent.Service_Request__c = :serviceRequestId
                     ORDER BY CreatedDate Desc
                     LIMIT 200];
            
            if(!Test.isRunningTest()){
                for(Service_Request__History currentSRHistoryTemp : srHistory){
                    if(!checkIfRealId(String.valueOf(currentSRHistoryTemp.OldValue))){
                        if(!checkIfRealId(String.valueOf(currentSRHistoryTemp.NewValue))){
                            selectHistories.add(new SelectHistory(currentSRHistoryTemp.CreatedDate, currentSRHistoryTemp.CreatedBy.Name,
                                                                    'Service Request', 
                                                                    actionValue(currentSRHistoryTemp.Field, serviceRequestLabelMap, currentSRHistoryTemp.OldValue,currentSRHistoryTemp.NewValue),
                                                                    currentSRHistoryTemp.ParentId));
                        }

                    }
                }

                for(Service_Request_Contact__History currentSRCHistoryTemp : srcHistory){
                    if(!checkIfRealId(String.valueOf(currentSRCHistoryTemp.OldValue))){
                        if(!checkIfRealId(String.valueOf(currentSRCHistoryTemp.NewValue))){
                            selectHistories.add(new SelectHistory(currentSRCHistoryTemp.CreatedDate, currentSRCHistoryTemp.CreatedBy.Name,
                                                                    'Service Request Contact', 
                                                                    actionValue(currentSRCHistoryTemp.Field, serviceRequestContactLabelMap, currentSRCHistoryTemp.OldValue, currentSRCHistoryTemp.NewValue),
                                                                    currentSRCHistoryTemp.ParentId));
                        }

                    }
                }

                for(SRS_Service_Request_Charge__History currentSRSSRCHistoryTemp : srsSRCHistory){
                    if(!checkIfRealId(String.valueOf(currentSRSSRCHistoryTemp.OldValue))){
                        if(!checkIfRealId(String.valueOf(currentSRSSRCHistoryTemp.NewValue))){
                            selectHistories.add(new SelectHistory(currentSRSSRCHistoryTemp.CreatedDate, currentSRSSRCHistoryTemp.CreatedBy.Name,
                                                                    'Billable Charge',
                                                                    actionValue(currentSRSSRCHistoryTemp.Field, srsServiceRequestChargeLabelMap, currentSRSSRCHistoryTemp.OldValue, currentSRSSRCHistoryTemp.NewValue),
                                                                    currentSRSSRCHistoryTemp.ParentId));
                        }

                    }
                }

                for(SRS_Service_Address__History currentSRSSAHistoryTemp : srsSAHistory){
                    if(!checkIfRealId(String.valueOf(currentSRSSAHistoryTemp.OldValue))){
                        if(!checkIfRealId(String.valueOf(currentSRSSAHistoryTemp.NewValue))){
                            selectHistories.add(new SelectHistory(currentSRSSAHistoryTemp.CreatedDate, currentSRSSAHistoryTemp.CreatedBy.Name,
                                                                    'Service Address', 
                                                                    actionValue(currentSRSSAHistoryTemp.Field, srsServiceAddressLabelMap, currentSRSSAHistoryTemp.OldValue, currentSRSSAHistoryTemp.NewValue),
                                                                    currentSRSSAHistoryTemp.ParentId));
                        }
                    }
                }
            } else{
                selectHistories.add(new SelectHistory(Date.today().addDays(-3), 'Service Request - User', 'Service Request', 'Changed ' + serviceRequestLabelMap.get(String.valueOf('Existing_Service_ID__c').toLowerCase()) + ' from null to test', null));
                selectHistories.add(new SelectHistory(Date.today().addDays(-10), 'SRS Service Request Charge - User', 'SRS Service Request Charge', 'Changed ' + serviceRequestLabelMap.get(String.valueOf('Amount__c').toLowerCase()) + ' from null to test', null));
                selectHistories.add(new SelectHistory(Date.today(), 'Service Request Contact - User', 'Service Request Contact', 'Changed ' + serviceRequestLabelMap.get(String.valueOf('Contact__c').toLowerCase()) + ' from null to test', null));
                selectHistories.add(new SelectHistory(Date.today().addDays(-3), 'SRS Service Address - User', 'SRS Service Address', 'Changed ' + serviceRequestLabelMap.get(String.valueOf('Demarcation_Location__c').toLowerCase()) + ' from null to test', null));
            }

            selectHistories.sort();

        }

        return selectHistories;
    }
    
    private String actionValue(String fieldName, Map<String, String> fieldLabelMap, Object oldValue, Object newValue) {
        if (fieldName == 'created') {
            return 'Created';
        }
        return 'Changed ' + fieldLabelMap.get(String.valueOf(fieldName).toLowerCase()) + ' from ' + oldValue + ' to ' + newValue;
    }
                                                                    
    public Boolean checkIfRealId(String potentialId){
        try {
            Id.valueOf(potentialId);
            return true;
        }
        catch(Exception ex) {
            return false;
        }
    }

    private Map<String,String> getLabels(String objectType) {
        Map<String, String> result   = new Map<String,String>();
        Map<String, Schema.SObjectField> fieldMap       = Schema.getGlobalDescribe().get(objectType).getDescribe().fields.getMap();

        for(String aFieldName :fieldMap.keySet()) {      
            Schema.DescribeFieldResult fieldDescribeResult = fieldMap.get(aFieldName).getDescribe();
            result.put(aFieldName,fieldDescribeResult.getLabel());
        }
        return result;
    }


    public class SelectHistory implements Comparable{
        public DateTime dateValue {get; set;}
        public String dateValueFormatted {get; set;}
        public String userName {get; set;}
        public String objectType {get; set;}
        public String action {get; set;}
        public String recordLink {get; set;}

        public SelectHistory(DateTime dateValue, String userName, String objectType, String action, String recordLink){
            this.dateValue = dateValue;
            this.dateValueFormatted = dateValue.format();
            this.userName = userName;
            this.objectType = objectType;
            this.action = action;
            this.recordLink = recordLink;
        }

        public Integer compareTo(Object compareTo){
            SelectHistory compareToSelectHistory = (SelectHistory) compareTo;

            if(this.dateValue == compareToSelectHistory.dateValue){
                return 0;
            }

            if(this.dateValue < compareToSelectHistory.dateValue){
                return 1;
            }

            return -1;
        }
    }
}