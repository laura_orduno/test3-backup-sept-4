public without sharing class trac_Quote_Report_Builder {
    public class ReportException extends Exception {}

    /* Testing only! */
    public Date testDate {get; set;}
    public String testEmail {get; set;}
    public String emailbody {get; set;}
    
    public PageReference sendDailyTest() {
        Email email = generateDailyEmail(testDate);
        emailbody = email.render();
        Messaging.SingleEmailMessage mail = email.package(); mail.setToAddresses(new String[] {testEmail});
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        return null;
    }
    
    public PageReference sendWeeklyTest() {
        Email email = generateCycleTimeEmail(testDate);
        emailbody = email.render();
        Messaging.SingleEmailMessage mail = email.package(); mail.setToAddresses(new String[] {testEmail});
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        return null;
    }
    
    public void sendTodaysDailyEmail() {
        Quote_Tool_Report_Recipients__c[] recipients = [Select Id, Recipient_Email__c From Quote_Tool_Report_Recipients__c Where Daily_Email__c = true];
        if (recipients.size() == 0) { return; }
        Quote_Portal__c qps = Quote_Portal__c.getInstance();
        Id emailAddressId = qps.Email_Address_Id__c;
        Date targetDate = Date.today();
        if (qps.Reporting_Daily_Email_Sent_After_Midnigh__c != null && qps.Reporting_Daily_Email_Sent_After_Midnigh__c == true) {
            targetDate = Date.today().addDays(-1);
        }
        Email email = generateDailyEmail(targetDate);
        Messaging.SingleEmailMessage[] messages = new Messaging.SingleEmailMessage[]{};
        for (Quote_Tool_Report_Recipients__c recipient : recipients) {
            Messaging.SingleEmailMessage mail = email.package();
            if (emailAddressId != null) {
                mail.setOrgWideEmailAddressId(emailAddressId);
            }
            mail.setToAddresses(new String[] {recipient.Recipient_Email__c});
            messages.add(mail);
        }
        Messaging.sendEmail(messages);
    }
    
    public void sendTodaysWeeklyEmail() {
        Quote_Tool_Report_Recipients__c[] recipients = [Select Id, Recipient_Email__c From Quote_Tool_Report_Recipients__c Where Weekly_Email__c = true];
        if (recipients.size() == 0) { return; }
        Quote_Portal__c qps = Quote_Portal__c.getInstance();
        Id emailAddressId = qps.Email_Address_Id__c;
        Date targetDate = Date.today();
        if (qps.Reporting_Weekly_Email_Sent_After_Midnig__c != null && qps.Reporting_Weekly_Email_Sent_After_Midnig__c == true) {
            targetDate = Date.today().addDays(-1);
        }
        Email email = generateCycleTimeEmail(targetDate);
        Messaging.SingleEmailMessage[] messages = new Messaging.SingleEmailMessage[]{};
        for (Quote_Tool_Report_Recipients__c recipient : recipients) {
            Messaging.SingleEmailMessage mail = email.package();
            if (emailAddressId != null) {
                mail.setOrgWideEmailAddressId(emailAddressId);
            }
            mail.setToAddresses(new String[] {recipient.Recipient_Email__c});
            messages.add(mail);
        }
        Messaging.sendEmail(messages);
    }
    
    
    public Email generateDailyEmail(Date targetDate) {
        if (targetDate == null) {
            throw new ReportException('Target date cannot be null');
        }
        
        // Determine "end statuses" for this report:
        List<String> statuses = new List<String>{
            // "Accepted Agreements"
            'Customer Accepted Agreement, Awaiting Provisioning',
            'Customer Accepted - Pending Dealer input',
            // "Rejected Agreements"
            'Client Declined',
            'Rejected to Sales',
            // "Completed Orders"
            'Provisioning Complete'
        };
        List<String> inProgressStatuses = new List<String>{
            'Customer Accepted Agreement, Awaiting Provisioning',
            'Customer Accepted - Pending Dealer input',
            'Provisioning in Progress',
            'Wireless Provisioning Complete',
            'Wireline Order Entry Complete',
            'Held for Customer',
            'Rejected to Sales'
        };
        
        // The offsets are to report on the Pacific Time day.
        DateTime startDt = DateTime.newInstanceGmt(targetDate, Time.newInstance(8,0,0,0));
        DateTime endDt = DateTime.newInstanceGmt(targetDate.addDays(1), Time.newInstance(7,59,59,999));
        Quote_Status_History__c[] quoteStatusHistories = loadQuoteStatusHistoryRecords(startDt, endDt, statuses);
        SBQQ__Quote__c[] quotes = loadQuotesByStatus(inProgressStatuses);
        
        Map<String, List<Quote_Status_History__c>> statToHistories = new Map<String, List<Quote_Status_History__c>> {
            'Today\'s Accepted Agreements' => new List<Quote_Status_History__c>(),
            'Today\'s Rejected Agreements' => new List<Quote_Status_History__c>(),
            'Today\'s Completed Orders' => new List<Quote_Status_History__c>()
        };
        
        if (quoteStatusHistories == null) {
            quoteStatusHistories = new List<Quote_Status_History__c>();
        }
        for (Quote_Status_History__c qsh : quoteStatusHistories) {
            if (qsh.New_Status__c.equalsIgnoreCase('Customer Accepted Agreement, Awaiting Provisioning') || qsh.New_Status__c.equalsIgnoreCase('Customer Accepted - Pending Dealer input')) {
                statToHistories.get('Today\'s Accepted Agreements').add(qsh);
                continue;
            }
            if (qsh.New_Status__c.equalsIgnoreCase('Client Declined') || qsh.New_Status__c.equalsIgnoreCase('Rejected to Sales')) {
                statToHistories.get('Today\'s Rejected Agreements').add(qsh);
                continue;
            }
            if (qsh.New_Status__c.equalsIgnoreCase('Provisioning Complete')) {
                statToHistories.get('Today\'s Completed Orders').add(qsh);
                continue;
            }
        }
        
        Statistic[] statistics = new Statistic[]{};
        Messaging.EmailFileAttachment[] attachments = new Messaging.EmailFileAttachment[]{};
        
        for (String statName : statToHistories.keySet()) {
            Statistic stat = buildStatisticFromHistories(statName, statToHistories.get(statName));
            statistics.add(stat);
        }
        // The "All Orders in Progress" stat
        Statistic ordersInProgress = new Statistic();
        ordersInProgress.name = 'All Orders in Progress';
        ordersInProgress.value = String.valueOf(quotes.size());
        ordersInProgress.breakdown = generateBreakdownFromQuotes(quotes);
        statistics.add(ordersInProgress);
        
        // Status Change CSV
        attachments.add(buildQuoteStatusChangesAttachment(quoteStatusHistories, targetDate));
        // Orders in Progress CSV
        attachments.add(buildOrdersInProgressAttachment(quotes));
        
        Email email = new Email();
        email.title = 'TELUS Business Anywhere Order Volume';
        email.subject = 'Business Anywhere Order Volume for ' + getFriendlyDateLong(targetDate);
        email.disclaimer = 'For a detailed breakdown of the data used to generate this summary, view the attached CSV files.';
        email.statistics.addAll(statistics);
        email.attachments.addAll(attachments);
        return email;
    }
    
    public Email generateCycleTimeEmail(Date endDate) {
        if (endDate == null) { throw new ReportException('End date cannot be null'); }
        Date startDate = endDate.addDays(-6);
        
        // The offsets are to report on the Pacific Time day.
        Datetime startTimestamp = Datetime.newInstanceGmt(startDate, Time.newInstance(8,0,0,0));
        Datetime endTimestamp = Datetime.newInstanceGmt(endDate.addDays(1), Time.newInstance(7,59,59,999));
        
        SBQQ__Quote__c[] quotes = loadQuotesByTimestamp(startTimestamp, endTimestamp);
        
        Statistic[] statistics = new Statistic[]{};
        Messaging.EmailFileAttachment[] attachments = new Messaging.EmailFileAttachment[]{};
        
        // The "All Orders in Progress" stat... it's not from histories, so it's handled separately.
        Statistic quotesReportedOn = new Statistic();
        quotesReportedOn.name = 'Quotes Reported On';
        quotesReportedOn.value = String.valueOf(quotes.size());
        quotesReportedOn.breakdown = generateBreakdownFromQuotes(quotes);
        statistics.add(quotesReportedOn);
        
        // Quote Cycle Time CSV
        Messaging.EmailFileAttachment cycleTimes = buildQuoteCycleTimesAttachment(quotes, endDate);
        attachments.add(cycleTimes);
        
        Email email = new Email();
        email.title = 'TELUS Business Anywhere Cycle Times';
        email.subject = 'Business Anywhere Cycle Times for the week ending ' + getFriendlyDateLong(endDate);
        email.disclaimer = 'For the data to create cycle time reports, view the attached CSV file.';
        email.statistics.addAll(statistics);
        email.attachments.addAll(attachments);
        return email;
    }
    
    public Messaging.EmailFileAttachment buildQuoteStatusChangesAttachment(Quote_Status_History__c[] quoteStatusHistories, Date targetDate) {
        if (quoteStatusHistories == null) {
            quoteStatusHistories = new Quote_Status_History__c[]{};
        }
        Messaging.EmailFileAttachment statusChanges = new Messaging.EmailFileAttachment();
        statusChanges.setContentType('text/csv');
        statusChanges.setInline(false);
        statusChanges.setFileName('TELUSQuoteStatusChanges - ' + targetDate.day() + targetDate.month() + targetDate.year() + '.csv');
        
        String statusChangeCsv = '"QuoteStatusTimestampGMT","QuoteNumber","Channel","PreviousStatus","NewStatus","CustomerAcceptedAgreementTimestampGMT","CreatorFirstName","CreatorLastName","CreatorEmail","ModifiedByFirstName","ModifiedByLastName","ModifiedByEmail"\n';
        for (Quote_Status_History__c h : quoteStatusHistories) {
            statusChangeCsv += '"' + h.Timestamp__c + '","' + h.Quote__r.Name + '","' + h.Quote__r.Channel__c + '","' + h.Old_Status__c + '","' + h.New_Status__c + '","' + h.Quote__r.Timestamp_Agreement_Accepted__c + '","' + h.Quote__r.CreatedBy.FirstName + '","' + h.Quote__r.CreatedBy.LastName + '","' + h.Quote__r.CreatedBy.Email + '","' + h.CreatedBy.FirstName + '","' + h.CreatedBy.LastName + '","' + h.CreatedBy.Email + '"\n';
        }
        statusChanges.setBody(Blob.valueOf(statusChangeCsv));
        return statusChanges;
    }
    
    public Messaging.EmailFileAttachment buildQuoteCycleTimesAttachment(SBQQ__Quote__c[] quotes, Date periodEndDate) {
        if (quotes == null) {
            quotes = new SBQQ__Quote__c[]{};
        }
        Messaging.EmailFileAttachment cycleTimes = new Messaging.EmailFileAttachment();
        cycleTimes.setContentType('text/csv');
        cycleTimes.setInline(false);
        cycleTimes.setFileName('TELUSCycleTime - week ending ' + periodEndDate.day() + periodEndDate.month() + periodEndDate.year() + '.csv');
        
        String cycleTimesCsv = '"QuoteNumber","Channel","ClientType","CurrentStatus","CreatorFirstName","CreatorLastName","CreatorEmail","CustomerAcceptedAgreementTimestampGMT","CustomerAcceptedAgreementTimestampUserChannel","ProvisioningInProgressTimestampGMT","ProvisioningInProgressTimestampUserChannel","WirelessProvisioningCompleteTimestampGMT","WirelessProvisioningCompleteTimestampUserChannel","WirelineOrderEntryCompleteTimestampGMT","WirelineOrderEntryCompleteTimestampUserChannel","ProvisioningCompleteTimestampGMT","ProvisioningCompleteTimestampUserChannel"\n';
        for (SBQQ__Quote__c q : quotes) {
            cycleTimesCsv += '"' + q.Name + '","'
                                 + q.Channel__c + '","'
                                 + q.SBQQ__Opportunity__r.Web_Account__r.Type_of_Customer__c + '","'
                                 + q.SBQQ__Status__c + '","'
                                 + q.CreatedBy.FirstName + '","'
                                 + q.CreatedBy.LastName + '","'
                                 + q.CreatedBy.Email + '","'
                                 + q.Timestamp_Agreement_Accepted__c + '","'
                                 + (q.Timestamp_Agreement_Accepted_User__c == null ? '' : q.Timestamp_Agreement_Accepted_User__r.Channel__c) + '","'
                                 + q.Timestamp_Provisioning_in_Progress__c + '","'
                                 + (q.Timestamp_Provisioning_in_Progress_User__c == null ? '' : q.Timestamp_Provisioning_in_Progress_User__r.Channel__c) + '","'
                                 + q.Timestamp_Wireless_Provisioning_Complete__c + '","'
                                 + (q.TS_Wireless_Provisioning_Complete_User__c == null ? '' : q.TS_Wireless_Provisioning_Complete_User__r.Channel__c) + '","'
                                 + q.Timestamp_Wireline_Order_Entry_Complete__c + '","'
                                 + (q.TS_Wireline_Order_Entry_Complete_User__c == null ? '' : q.TS_Wireline_Order_Entry_Complete_User__r.Channel__c) + '","'
                                 + q.Timestamp_Provisioning_Complete__c + '","'
                                 + (q.Timestamp_Provisioning_Complete_User__c == null ? '' : q.Timestamp_Provisioning_Complete_User__r.Channel__c) + '"\n';
        }
        cycleTimes.setBody(Blob.valueOf(cycleTimesCsv));
        return cycleTimes;
    }
    
    public Messaging.EmailFileAttachment buildOrdersInProgressAttachment(SBQQ__Quote__c[] quotes) {
        if (quotes == null) {
            quotes = new SBQQ__Quote__c[]{};
        }
        Messaging.EmailFileAttachment ordersInProgress = new Messaging.EmailFileAttachment();
        ordersInProgress.setContentType('text/csv');
        ordersInProgress.setInline(false);
        ordersInProgress.setFileName('TELUSOrdersInProgress - ' + Date.today().day() + Date.today().month() + Date.today().year() + '.csv');
        
        String ordersInProgressCsv = '"QuoteNumber","Channel","CurrentStatus",""CustomerAcceptedAgreementTimestampGMT","CreatorFirstName","CreatorLastName","CreatorEmail","LastModifiedDateGMT","LastModifiedByFirstName","LastModifiedByLastName","LastModifiedByEmail"\n';
        for (SBQQ__Quote__c q : quotes) {
            ordersInProgressCsv += '"' + q.Name + '","'
                                       + q.Channel__c + '","'
                                       + q.SBQQ__Status__c + '","'
                                       + q.Timestamp_Agreement_Accepted__c + '","'
                                       + q.CreatedBy.FirstName + '","'
                                       + q.CreatedBy.LastName + '","'
                                       + q.CreatedBy.Email + '","'
                                       + q.LastModifiedDate + '","'
                                       + q.LastModifiedBy.FirstName + '","'
                                       + q.LastModifiedBy.LastName + '","'
                                       + q.LastModifiedBy.Email + '"\n';
        }
        ordersInProgress.setBody(Blob.valueOf(ordersInProgressCsv));
        return ordersInProgress;
    }
    
    public class Email {
        public string title {get; set;}
        public string subject {get; set;}
        public string disclaimer {get; set;}
        public Statistic[] statistics {get; set;}
        public Messaging.EmailFileAttachment[] attachments {get; set;}
        
        public Email() {
            statistics = new Statistic[]{};
            attachments = new Messaging.EmailFileAttachment[]{};
        }
        
        public Messaging.SingleEmailMessage package() {
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.setSubject(subject);
            message.setHtmlBody(render());
            message.setFileAttachments(attachments);
            return message;
        }
        
        public string render() {
            String htmlBody = renderHeader();
            htmlBody += renderStatistics();
            htmlBody += renderDisclaimer();
            htmlBody += renderFooter();
            return htmlBody;
        }
        
        public String renderHeader() {
            return '<!DOCTYPE html><html><head><title>'+title+'</title><meta name="viewport" content="width=700px"/></head>'
                + '<body style="font-size: 16px;font-family: Helvetica Neue, Helvetica, Arial, sans-serif;background: #f5f5f5 url("http://i.imgur.com/Nyq85.png") repeat left top">'
                + '<br/><br/><table width="80%"><tr><td width="5%"><td width="89%" style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif;border-radius: 16px;background-color: #fbfbfb;border: 1px solid #999;box-shadow: 0 0 5px #999; padding: 0px;"><div style="margin: 8px; padding: 0px;">'
                + '<div class="header" style="border-bottom: 1px solid #999;background-color: #fbfbfb; padding: 0px;"><div style="margin: 11px 16px;padding: 0px;">'
                + '<img src="http://i.imgur.com/wVDQj.png" class="logo" alt="TELUS"/>'
                + '</div></div><table class="body" width="100%"><tr><td style="padding: 0.5em;">' +
                + '<div class="title" style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif;font-weight: bold;font-size: 19px;margin-bottom: 0.5em">'+subject+'</div>';
        }
        
        public String renderStatistics() {
            String html = '';
            if (statistics == null) { return html; }
            for (Statistic s : statistics) {
                html += s.render();
            }
            return html;
        }
        
        public String renderDisclaimer() {
            return '<table class="disclaimer" style="width: 100%"><tr><td style="font-size: 16px;margin: 8px 0 0;font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">' + disclaimer + '</td></tr></table>';
        }
        
        public String renderFooter() {
            return '<table class="footer" style="width: 100%; border-top: 1px solid #ccc;">'
                    + '<tr><td style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif; padding: 0.5em; color: #999; font-size: 12px;">'
                        + 'This email was sent from the TELUS Business Anywhere Quote Tool. Generated at ' + DateTime.now().time() + ' on ' + getFriendlyDateLong(Date.today()) + '. <strong>All timestamps are in GMT.</strong>'
                    + '</td></tr>'
                + '</table>'
            + '</td></tr></table></td></tr></table><br/><br/></body></html>';
        }
    }
    
    public class Statistic {
        public String name {get; set;}
        public String value {get; set;}
        public Map<String, String> breakdown {get; set;}
        public Boolean showBreakdown {get; set;}
        
        public Statistic() {
            name = '';
            value = '';
            showBreakdown = true;
            breakdown = new Map<String, String>();
        }
        
        public String render() {
            String s = '<table class="stat" width="100%"><tr><td style="margin: 2px 0.5em 2px 0.5em; border-bottom: 1px solid #999;">';
            s += renderPrimaryNumber();
            s += renderBreakdownIfEnabled();
            s += '</td></tr></table>';
            return s;
        }
        
        public String renderPrimaryNumber() {
            return '<table class="primary" width="100%"><tr>'
            + '<td width="80%" class="title" style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif;font-size: 24px;">'+ this.name +'</td>'
            + '<td width="20%" class="number" style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif;font-size: 24px;text-align: center;font-weight: bold;color:#000000">'+ this.value +'</td>'
            + '<td>'
            + '</tr></table>';
        }
        
        public String renderBreakdownIfEnabled() {
            if (showBreakdown == false || breakdown == null || breakdown.size() == 0) {
                return '';
            }
            String s = '<table class="breakdown" cellpadding="3" style="width: 90%;text-align: center; font-size: 16px; color:#333"><thead><tr>';
            List<String> keys = new List<String>();
            keys.addAll(breakdown.keySet());
            for (String key : keys) {
                s += renderStatisticBreakdownCell(key);
            }
            s += '</tr></thead><tbody><tr style ="font-weight:bold" class="numbers">';
            for (String key : keys) {
                s += renderStatisticBreakdownCell(breakdown.get(key));
            }
            s += '</tr></tbody></table>';
            return s;
        }
        
        public String renderStatisticBreakdownCell(String value) {
            return '<td style="margin: 3px 0;font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">' + value + '</td>';
        }
    }
    
    // Factory helpers
    public static Statistic buildStatisticFromHistories(String statName, Quote_Status_History__c[] histories) {
        SBQQ__Quote__c[] quotes = new SBQQ__Quote__c[]{};
        if (histories != null) {
            for (Quote_Status_History__c h : histories) {
                quotes.add(h.Quote__r);
            }
        }
        Statistic stat = new Statistic();
        stat.name = statName;
        stat.value = (histories == null ? '0' : String.valueOf(histories.size()));
        stat.breakdown = generateBreakdownFromQuotes(quotes);
        return stat;
    }
    
    public static Map<String, String> generateBreakdownFromQuotes(SBQQ__Quote__c[] quotes) {
        Map<String, Integer> channelToQuoteCount = new Map<String, Integer>();
        for (SBQQ__Quote__c q : quotes) {
            if (q.Channel__c == null) { continue; }
            if (channelToQuoteCount.containsKey(q.Channel__c)) {
                channelToQuoteCount.put(q.Channel__c, channelToQuoteCount.get(q.Channel__c) + 1);
            } else {
                channelToQuoteCount.put(q.Channel__c, 1);
            }
        }
        Map<String, String> breakdown = new Map<String, String>();
        for (String k : channelToQuoteCount.keySet()) {
            breakdown.put(k, channelToQuoteCount.get(k) + '');
        }
        return breakdown;
    }
    
    public static Quote_Status_History__c[] loadQuoteStatusHistoryRecords(Datetime startTimestamp, Datetime endTimestamp, String[] statuses) {
        if (startTimestamp == null || endTimestamp == null || statuses == null) {
            return null;
        }
        Quote_Status_History__c[] records = [Select Id, Quote__c, Quote__r.Name, Quote__r.Channel__c, User__c, User__r.FirstName,
            User__r.LastName, User__r.Email, New_Status__c, Old_Status__c, Timestamp__c, CreatedBy.FirstName, CreatedBy.LastName, CreatedBy.Email,
            Quote__r.CreatedBy.FirstName, Quote__r.CreatedBy.LastName, Quote__r.CreatedBy.Email, Quote__r.CreatedDate, Quote__r.Timestamp_Agreement_Accepted__c From Quote_Status_History__c
            Where Timestamp__c >= :startTimestamp and Timestamp__c <= :endTimestamp and New_Status__c IN :statuses];
        if (records == null) {
            records = new Quote_Status_History__c[]{};
        }
        return records;
    }
    
    public static SBQQ__Quote__c[] loadQuotesByStatus(String[] statuses) {
        if (statuses == null || statuses.size() == 0) {
            return new SBQQ__Quote__c[]{};
        }
        SBQQ__Quote__c[] records = [Select Id, Name, Channel__c, CreatedDate, Timestamp_Agreement_Accepted__c, CreatedBy.FirstName, CreatedBy.LastName,
            CreatedBy.Email, SBQQ__Status__c, LastModifiedDate, LastModifiedBy.FirstName, LastModifiedBy.LastName, LastModifiedBy.Email From SBQQ__Quote__c
            Where SBQQ__Status__c IN :statuses Order By CreatedDate Asc];
        if (records == null) {
            records = new SBQQ__Quote__c[]{};
        }
        return records;
    }
    
    public static SBQQ__Quote__c[] loadQuotesByTimestamp(Datetime earliest, Datetime latest) {
        if (earliest == null || latest == null) {
            return new SBQQ__Quote__c[]{};
        }
        SBQQ__Quote__c[] records = [Select Id, Name, Timestamp_Agreement_Accepted__c, Timestamp_Provisioning_in_Progress__c,
            Timestamp_Wireless_Provisioning_Complete__c, Timestamp_Provisioning_Complete__c, Timestamp_Wireline_Order_Entry_Complete__c,
            Channel__c, CreatedBy.FirstName, CreatedBy.LastName, CreatedBy.Email, SBQQ__Status__c,
            Timestamp_Agreement_Accepted_User__c,
            Timestamp_Agreement_Accepted_User__r.Channel__c,
            Timestamp_Provisioning_in_Progress_User__c,
            Timestamp_Provisioning_in_Progress_User__r.Channel__c,
            TS_Wireline_Order_Entry_Complete_User__c,
            TS_Wireline_Order_Entry_Complete_User__r.Channel__c,
            Timestamp_Provisioning_Complete_User__c,
            Timestamp_Provisioning_Complete_User__r.Channel__c, 
            TS_Wireless_Provisioning_Complete_User__c,
            TS_Wireless_Provisioning_Complete_User__r.Channel__c,
            SBQQ__Opportunity__r.Web_Account__r.Account_Status__c, SBQQ__Opportunity__r.Web_Account__r.Type_of_Customer__c
            From SBQQ__Quote__c
            Where (Timestamp_Agreement_Accepted__c >= :earliest and Timestamp_Agreement_Accepted__c <= :latest)
             or (Timestamp_Provisioning_in_Progress__c >= :earliest and Timestamp_Provisioning_in_Progress__c <= :latest)
             or (Timestamp_Wireless_Provisioning_Complete__c >= :earliest and Timestamp_Wireless_Provisioning_Complete__c <= :latest)
             or (Timestamp_Provisioning_Complete__c >= :earliest and Timestamp_Provisioning_Complete__c <= :latest)
             or (Timestamp_Wireline_Order_Entry_Complete__c >= :earliest and Timestamp_Wireline_Order_Entry_Complete__c <= :latest)
            Order By CreatedDate Asc];
        if (records == null) {
            records = new SBQQ__Quote__c[]{};
        }
        return records;
    }
    
    public static String getFriendlyDateLong(Date d) {
        return getMonthNameByNumber(d.month()) + ' ' + d.day() + getNumberSuffix(d.day()) + ', ' + d.year();
    }
    
    public static String getMonthNameByNumber(Integer monthNumber) {
        return (monthNumber == null ? '' : months[monthNumber - 1]);
    }
    public static String[] months = new String[]{'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'};
    
    public static String getNumberSuffix(Integer n) {
        if (n == 11) { return 'th'; }
        if (n > 10) { n = Math.Mod(n,10); }
        if (n == 0) { return 'th'; }
        if (n == 1) { return 'st'; }
        if (n == 2) { return 'nd'; }
        if (n == 3) { return 'rd'; }
        if (n >= 4) { return 'th'; }
        return '';
    }
}