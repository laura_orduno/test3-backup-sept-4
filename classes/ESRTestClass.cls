/********************************************************************************************
* Class Name : ESRTestClass
* Purpose    : This is test class for ESR
*    
* 
* Modification History 
*-------------------------------------------------------------------------------------------
* Author                         Date                Version             Remarks
*-------------------------------------------------------------------------------------------
* Tech Mahindra(Swapnil)       May/06/2016          1.0                 Initial Creation
* 
*
**********************************************************************************************/
@isTest
public class ESRTestClass {

    /**
        testESRXMLParser() - Test Method for ESRXMLParser class
    **/
    static testMethod void testESRXMLParser() {
        String strXML = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body><createServiceResponse xmlns="http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/ElectronicServiceRequestServiceRequestResponse_v1"><createServiceResult xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><contextData i:nil="true"/><dateTimeStamp>0001-01-01T00:00:00</dateTimeStamp><errorCode>0</errorCode><messageType>Inserted Success</messageType><transactionId i:nil="true"/><serviceResponseId>0</serviceResponseId></createServiceResult></createServiceResponse></s:Body></s:Envelope>';
        test.StartTest();
        ESRXMLParser objESRXMLParser = new ESRXMLParser(strXML);
        objESRXMLParser.ParseXMLString();

        try {
            strXML = 'test';
            ESRXMLParser objESRXMLParser2 = new ESRXMLParser(strXML);
            objESRXMLParser2.ParseXMLString();
        } 
        catch (Exception e) {}

        strXML = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Header/><s:Body><s:Fault><faultcode xmlns:a="http://schemas.microsoft.com/ws/2005/05/addressing/none">a:ActionNotSupported</faultcode><faultstring xml:lang="en-US">The message with  cannot be processed at the receiver, due to a ContractFilter mismatch at the EndpointDispatcher.</faultstring></s:Fault></s:Body></s:Envelope>';
        ESRXMLParser objESRXMLParser3 = new ESRXMLParser(strXML);
        objESRXMLParser3.ParseXMLString();
        test.StopTest();
    }

    /**
        testESRSubmitRequest() - Test Method for ESRSubmitRequest class
    **/
    static testMethod void testESRSubmitRequest() {
        //Create Account record
        Account esrAccount = new Account();
        esrAccount.Name = 'esrAccount';
        esrAccount.CAN__c = 'test';
        //esrAccount.BillingAddress = 'BilingAddress';
        esrAccount.BillingCity = 'BillingCity';
        esrAccount.BillingState = 'BillingState';
        esrAccount.BillingPostalCode = '0000';
        esrAccount.BillingStreet = 'BillingStreet';
        insert esrAccount;
        System.assertNotEquals(esrAccount.Id, NULL);
        
        //Create Opportunity record
        Opportunity esrOpportunity = new Opportunity();
        esrOpportunity.Name = 'OppName';
        esrOpportunity.Requested_Due_Date__c = System.today();
        esrOpportunity.Probability = 10;
        esrOpportunity.StageName = 'Originated';
        esrOpportunity.CloseDate = System.today();
        insert esrOpportunity;
        System.assertNotEquals(esrOpportunity.Id, NULL);

        Profile p = [SELECT Id FROM Profile WHERE Name = 'Standard User'];
        
        //Create User record
        User esrUser = new User();
        esrUser.Username = 'testSwapnil007@test.com';
        esrUser.LastName = 'lName';
        esrUser.Phone = '1234567890';
        esrUser.email = 'testSwapnil007@test.com';
        esrUser.Alias = 'alias';
        esrUser.CommunityNickname = 'CommunityNickname';
        esrUser.TimeZoneSidKey = 'GMT';
        esrUser.LocaleSidKey = 'en_CA';
        esrUser.EmailEncodingKey = 'ISO-8859-1';
        esrUser.ProfileId = p.Id;
        esrUser.LanguageLocaleKey = 'en_US';
        insert esrUser;
        System.assertNotEquals(esrUser.Id, NULL);
    
        //Create Contact record
        Contact esrContact = new Contact();
        esrContact.LastName = 'lName';
        esrContact.Email = 'Test@test.com';
        esrContact.Phone = '1234567890';
        insert esrContact;
        System.assertNotEquals(esrContact.Id, NULL);
        
        //Create Product2 record
        Product2 esrProd = new Product2();
        esrProd.Name = 'TestProd';
        esrProd.CurrencyIsoCode = 'CAD';
        esrProd.IsActive = true;
        insert esrProd;
        System.assertNotEquals(esrProd.Id, NULL);
        
        //Create SRS PDS Product record
        SRS_PODS_Product__c srsProduct = new SRS_PODS_Product__c();
        //srsProduct.Product_ID__c = esrProd.Id;
        srsProduct.Main_User__c = esrUser.Id;
        insert srsProduct;
        System.assertNotEquals(srsProduct.Id, NULL);
        
        //Create Opportunity Product Item record
        Opp_Product_Item__c esrOPI = new Opp_Product_Item__c();
        esrOPI.Total_Contract_Value__c = 50.00;
        esrOPI.Margin__c = 2.5;
        esrOPI.Opportunity__c = esrOpportunity.Id;
        esrOPI.Product__c = esrProd.Id;
        insert esrOPI;
        System.assertNotEquals(esrOPI.Id, NULL);
        
        //Create SRS PODS Template
        SRS_PODS_Template__c template = new SRS_PODS_Template__c();
        template.SRS_PODS_Product_Master__c = srsProduct.Id;
        template.QUESTION_EN__c = 'testQuesEN';
        template.VALID_ANSWER__c = 'Test Answer';
        template.ORDER_TYPE__c = 'ordertype';
        template.CHILD__c = 53012;  //CH01
        insert template;
        
        //Create SRS PODS Template
        SRS_PODS_Template__c objtemplate = new SRS_PODS_Template__c();
        objtemplate.SRS_PODS_Product_Master__c = srsProduct.Id;
        objtemplate.QUESTION_EN__c = 'Support Level';
        objtemplate.VALID_ANSWER__c = 'Test Answer';
        objtemplate.ORDER_TYPE__c = 'ordertype';
        objtemplate.CHILD__c = 53012;   //CH01
        objtemplate.SYSTEM_GENERATED_QUESTION_ID__c = 53012;    //CH01
        insert objtemplate;
        System.assertNotEquals(objtemplate.Id, NULL);

        //Create Service Request record
        Service_Request__c SR = new Service_Request__c();
        SR.OwnerId = esrUser.Id;
        //SR.Billing_Account_New__c
        SR.Is_RFP__c = 'Yes';
        SR.ESR_Opportunity_Type__c = 'ESR';
        SR.Opportunity__c = esrOpportunity.Id;
        SR.Account_Name__c = esrAccount.Id;
        SR.Billing_Address__c = 'testBillingAddress';
        SR.City__c = 'City';
        SR.Postal_Code__c = '0000';
        SR.Prov__c = 'Province__c';
        SR.Existing_Service_ID__c = '0000';
        SR.PrimaryContact__c = esrContact.Id;
        SR.Opportunity_Product_Item__c = esrOPI.Id;
        SR.SRS_PODS_Product__c = srsProduct.Id;
        insert SR;
        
        //Create ESR SFDC System Type Custom Setting record
        ESR_SFDC_SystemType__c ESRCustSetting = new ESR_SFDC_SystemType__c();
        ESRCustSetting.Name = 'Audio Visual';
        ESRCustSetting.Product_Name__c = 'testProductName';
        ESRCustSetting.SystemType__c = 'testSystemType';
        insert ESRCustSetting;
        
        //Create Service Request record
        /*Service_Request__c objSR = new Service_Request__c();
        objSR.OwnerId = esrUser.Id;
        //objSR.Billing_Account_New__c
        objSR.Is_RFP__c = 'Yes';
        objSR.ESR_Opportunity_Type__c = 'ESR';
        objSR.Opportunity__c = esrOpportunity.Id;
        objSR.Account_Name__c = esrAccount.Id;
        objSR.Billing_Address__c = '';
        objSR.City__c = 'City';
        objSR.Postal_Code__c = '0000';
        objSR.Prov__c = 'Province__c';
        objSR.Existing_Service_ID__c = '0000';
        objSR.PrimaryContact__c = esrContact.Id;
        objSR.Opportunity_Product_Item__c = esrOPI.Id;
        objSR.SRS_PODS_Product__c = srsProduct.Id;
        insert objSR;
        System.assertNotEquals(objSR.Id, NULL);*/
        
        //Create SRS Service Address record
        SRS_Service_Address__c serviceAddr = new SRS_Service_Address__c();
        serviceAddr.Service_Request__c = SR.Id;
        insert serviceAddr;
        System.assertNotEquals(serviceAddr.Id, NULL);
        
        //Create SRS Service Address record
        /*SRS_Service_Address__c objSRSerAddress = new SRS_Service_Address__c();
        objSRSerAddress.Service_Request__c = objSR.Id;
        insert objSRSerAddress;
        System.assertNotEquals(objSRSerAddress.Id, NULL);*/

        //Create SRS PODS Answer record
        SRS_PODS_Answer__c srsAnswer = new SRS_PODS_Answer__c();
        srsAnswer.Service_Request__c = SR.Id;
        srsAnswer.SRS_PODS_Products__c = template.id;
        srsAnswer.SRS_PODS_Answer__c = 'answer';
        insert srsAnswer;
        System.assertNotEquals(srsAnswer.Id, NULL);
        
        //Create SRS PODS Answer record
        SRS_PODS_Answer__c objSRSAnswer = new SRS_PODS_Answer__c();
        objSRSAnswer.Service_Request__c = SR.Id;
        objSRSAnswer.SRS_PODS_Products__c = objtemplate.id;
        objSRSAnswer.SRS_PODS_Answer__c = 'answer';
        insert objSRSAnswer;
        System.assertNotEquals(objSRSAnswer.Id, NULL);
        
        //Create ESR SFSC Configs record
        ESR_SFDC_Configs__c config = new ESR_SFDC_Configs__c();
        config.Name = 'ESR Configs';
        config.ServiceEndPoint__c = 'http://foo';
        config.UserName__c = 'tiger';
        config.Password__c = 'scott';
        insert config;
        System.assertNotEquals(config.Id, NULL);

        //Create Note record
        Note esrNote = new Note();
        esrNote.ParentId = SR.Id;
        esrNote.Title = 'Title';
        insert esrNote;
        System.assertNotEquals(esrNote.Id, NULL);
        
        //Create Attachment record
        Attachment esrAttachment = new Attachment();
        esrAttachment.ParentId = SR.Id;
        esrAttachment.Name = 'TestAttachment';
        esrAttachment.Body = Blob.valueOf('Unit Test Attachment Body');
        insert esrAttachment;
        System.assertNotEquals(esrAttachment.Id, NULL);
	Test.setMock(HttpCalloutMock.class, new ESRCalloutMock());
        test.StartTest();
        
        ESRSubmitRequest.SendRequest(SR.Id);
        SR.Last_Submitted_Date__c = system.now().addMinutes(-15);
        SR.ESR_ID__c = '888';
        update SR;  //update Service Request record

        ESRSubmitRequest.SendRequest(SR.Id);
        ESRSubmitRequest.updateStatus(SR.Id);
        ESRSubmitRequest.UpdateESRRequestId('12345');
        ESRSubmitRequest.sreviceRequestId='32323';
        ESRSubmitRequest.UpdateESRRequestId('12345');
        test.StopTest();
    }
    
    public class ESRCalloutMock implements HttpCalloutMock{
        
        public HTTPResponse respond(HttpRequest req)
        {
            try{
                if(req.getEndpoint() == 'http://fooerror'){
                    throw new CalloutException('Error');}
            HttpResponse resp = new HttpResponse();
            String strXML = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body><createServiceResponse xmlns="http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/ElectronicServiceRequestServiceRequestResponse_v1"><createServiceResult xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><contextData i:nil="true"/><dateTimeStamp>0001-01-01T00:00:00</dateTimeStamp><errorCode>0</errorCode><messageType>Inserted Success</messageType><transactionId i:nil="true"/><serviceResponseId>0</serviceResponseId></createServiceResult></createServiceResponse></s:Body></s:Envelope>';
            resp.setHeader('Content-Type', 'Text/xml');
            resp.setBody(strXML);
            resp.setStatusCode(200);
            return resp;
            }
            catch(Exception e){
            String respXML = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Header/><s:Body><s:Fault><faultcode xmlns:a="http://schemas.microsoft.com/ws/2005/05/addressing/none">a:ActionNotSupported</faultcode><faultstring xml:lang="en-US">The message</faultstring></s:Fault></s:Body></s:Envelope>';
            throw new CalloutException(respXML );
            }
        }
    
    }
}