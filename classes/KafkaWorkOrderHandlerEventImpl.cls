/*****************************************************
    Name: KafkaWorkOrderHandlerEventImpl
    Usage: This class is used to process event for KafkaWorkOrderSubscriber
    Author – Adhir P  (TechM)
    Revision History -
    Created By/Date - Adhir P / 19 Mar 2017
    
    Updated By/Date/Reason for Change - 
    
******************************************************/

global class KafkaWorkOrderHandlerEventImpl implements EventHandlerInterface{

	/*
    * Name - processEvent
    * Description - Process all events
    * Param - List<External_Event__c> eventObjects
    * Return Type - void
    */
    
    //Predefined HTTP Status code to be used in the service
    private static Integer OK = 200;
    private static Integer UNAUTHORIZED = 401;
    private static Integer INTERNAL_SERVER_ERROR = 500;
    
    global void processEvent(List<External_Event__c> eventObjects, List<ExternalEventRequestResponseParameter.ResponseEventData> responseData){
        system.debug('++++>> Adhir -> Reached Process Event in KalfkaWorkOrderHandlerEventImpl \n\n\n\n\n'+eventobjects[0].Event_Details__c+'\n\n\n\n\n');
        //SubscriberResponse rtn = new SubscriberResponse();
        
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        //Map<String, String> headers = req.headers;
        String xml = '';
        List<Work_Order__c> WOList = new List<Work_Order__c>();
        JobDetails[] jobs;
        ExternalEventRequestResponseParameter.EventError errorObject = new ExternalEventRequestResponseParameter.EventError();
        try{
                XMLStreamReader reader = new XMLStreamReader(eventobjects[0].Event_Details__c);
                jobs = getJobsfromWFM(reader);
                
                system.debug('++ Job Count :'+ jobs.size());
                system.debug('++ Jobs :'+ jobs);
                Database.SaveResult workOrdertoUpdate;
                if(jobs != null && jobs.size() > 0 ){
                    system.debug('\n\n$$$$$$$$$$$$$$$$$$$$$Parse Job WFM Number :'+jobs[0].WFMNumber);
                    WOList = [select id, WFM_Number__c,Status__c,Dispatch_Status__c from Work_Order__c where WFM_Number__c =:  jobs[0].WFMNumber limit 1];
                    system.debug('\n\n$$$$$$$$$$$$$$$$$$$$$WOLIST :'+wolist);
                    if(WOList != null && WOList.size() > 0){
                        Work_Order__c WOObj = WOList[0]; 
                        if(WOObj != null && WOObj.WFM_Number__c == jobs[0].WFMNumber && WOObj.Dispatch_Status__c != jobs[0].StatusCd){
                            WOObj.Dispatch_Status__c = jobs[0].StatusCd;
                            //WOObj.Status__c = 'RajanUmesh';
                            update(WOObj);
                            
                        }
                        //send response statuscode and error message in error list only. Receiving class will parse this to determine success or failure.
                        /*errorObject.errorCode = '200';
                        errorObject.errorDescription = ' Record updated in SFDC successfully.';
                        system.debug('+++++>>>> response received: '+responseData[0]);
                        responseData[0].error= new list <ExternalEventRequestResponseParameter.EventError>();
                        responseData[0].error.add(errorObject);*/
                        system.debug('+++++>>>> response after update: '+responseData[0]);
                    }
                    else{
                        system.debug('Exception occured. Record not found');
/*                        
                        errorObject.errorCode = '500';
                        errorObject.errorDescription = 'Error: Exception occured in SFDC. Record not found';
                        responseData[0].error= new list <ExternalEventRequestResponseParameter.EventError>();
                        responseData[0].error.add(errorObject);
                        EventHandler_Helper.logError('KafkaWorkOrderHandlerEventImpl.processEvent', eventObjects[0].Id ,'Exception occured in SFDC. Record not found.', 'Exception occured in SFDC. Record not found.', 'WFM');
*/                        
                    }
                }
        }
        catch(Exception e){
            system.debug('Exception occured in SFDC'+e.getStackTraceString());
            errorObject.errorCode = '500';
            errorObject.errorDescription = 'Error: Exception occured in SFDC.';
            responseData[0].error= new list <ExternalEventRequestResponseParameter.EventError>();
            responseData[0].error.add(errorObject);
            EventHandler_Helper.logError('KafkaWorkOrderHandlerEventImpl.processEvent', eventObjects[0].Id ,e.getMessage(), e.getStackTraceString(), 'WFM');
        }
        
        
        return;
    }
    
    global static void validateEventDetails(String eventDetails, List<ExternalEventRequestResponseParameter.EventError> errorList){
    	system.debug('++++>> Adhir -> Reached Validate Event in KafkaWorkOrderHandlerEventImpl====eventDetails========='+eventDetails);
        JobDetails[] jobs;
        XMLStreamReader reader = new XMLStreamReader(eventDetails);
        jobs = getJobsfromWFM(reader);
        system.debug('++++>> Adhir -> Reached Validate Event jobs are : ' + jobs);
        if(jobs.size()>0){
            if(String.isblank(jobs[0].WFMNumber)){
                system.debug('++++>>  -> WFM number missing : ' + jobs[0].WFMNumber);
				ExternalEventRequestResponseParameter.EventError errorObject = new ExternalEventRequestResponseParameter.EventError();
                errorObject.errorCode = 'EE-ERR-020';
                errorObject.errorDescription = 'Mandatory Data missing or corrupt in request for WorkOrderId' ; 
                errorList.add(errorObject);
            }
            if(String.isblank(jobs[0].StatusCd)){
                system.debug('++++>>  ->StatusCd missing : ' + jobs[0].StatusCd);
                ExternalEventRequestResponseParameter.EventError errorObject = new ExternalEventRequestResponseParameter.EventError();
                errorObject.errorCode = 'EE-ERR-020';
                errorObject.errorDescription = 'Mandatory Data is missing or corrupt in request for StatusCd' ;
                errorList.add(errorObject);
            }
        }
                
        return;
    }
    
    public static JobDetails[] getJobsfromWFM(XMLStreamReader reader){
        list<JobDetails> jobs= new list<JobDetails>();
        boolean isNext = true;
        while(isNext){
            if(reader.getEventType() == xmltag.START_ELEMENT){
                 
                if('workOrder'== reader.getLocalName()){
                    jobs.add(seeJobDetails(reader));
                    //jobs.add(job);
                }
            }
            if(reader.hasNext())
                reader.next();
            else{
                    isNext=false;
                    break;
            }
            
        }
        return jobs;
    }
    
    public static JobDetails seeJobDetails(xmlstreamreader reader){
        JobDetails job= new JobDetails();
        boolean isNext = true;
        boolean isNextWFMID = false;
        boolean isNextStatusCd = false;
        while(isNext){
            system.debug('++current element type:'+reader.getEventType()+' , Name :'+reader.getLocalName());//+'Value: '+reader.getText());
            //if('workOrder'== reader.getLocalName())
            if(reader.getEventType()== xmltag.START_ELEMENT ){
                if(reader.getLocalName()== 'assignment')
                   break;
                else if(reader.getLocalName()== 'workOrderId'){
                    isNextWFMID = true;
                    system.debug('have we reached here!!!!!!!!UMESH means isNextWFMID is TRUE');
                    //job.WFMNumber=reader.next().getText();
                    reader.next();
                	}
                else if(reader.getLocalName()== 'statusCd'){
                    isNextStatusCd = true;
                    reader.next();
                	}
                else{
                    isNextWFMID = false;
                    isNextStatusCd = false;
                    //reader.next();
                }
                
                if(isNextWFMID ){
                    try{
                        system.debug('WFM no will be reader.getText :Umesh'+reader.getText());
                        job.WFMNumber=reader.getText();
                    }
                    catch(Exception e){
                        job.WFMNumber='';
                    }
                }
               
                if(isNextStatusCd){
                    
                    try{
                        job.StatusCd=reader.getText();
                    }
                    catch(Exception e){
                        job.StatusCd='';
                    }
                }
            }        
            if(reader.hasNext())
                reader.next();
            else{
                    isNext=false;
                    break;
            }
            
        }
        return job;
    }
    
    /*
    public static String getToken () {
        /*
        * Predefined static token used for handshake.
        * Note: This is not ideal way but to align with how Kafka communicate with Falcon program
        * Future enchancement is required make use of OAuth.
        * Token generated from http://randomkeygen.com/ look for CodeIgniter Encryption Keys
        *
        String Token;
        if ( Test.isRunningTest() )
            Token='mLVMq7sT1xqVQwWRZr6ycJ2ofL0kMzV78QNOD4b5l2EZU0R5AvQD4CLR53Jhhu0k';   
        //AF10RUgpbbZBGuEBabhj7d21wr84xrt31ujhhZDr2W1y5AkyP89zl6ZopA77p3yl
        else
            Token= KafkaSettings__c.getInstance('Token').value__c;         
        return Token;
    }
    */
    
    
    global class JobDetails{
        String WFMNumber;
        String StatusCd;
    }
    /*
     global class SubscriberResponse {
        String messageid;
        //String partition;
        String topic;
        //String offset;
        Integer errorCode;
        String errorMessage;
        //Integer StatusCode;
        JobDetails request;
    }
    */
}