public class OCOM_QuoteOrderVO implements Comparable
{
    public string Id {get;set;}
    public string Condition {get;set;}
    public string OrderId {get;set;}
    public string Name {get;set;}
    public string  Account{get;set;}
    public string  AccountName{get;set;}
    public string OrderType {get;set;}
    public string RecordType {get; set;}
    public string ProductType {get; set;}
    public string Status {get;set;}
    public DateTime CreatedDate {get;set;}
    // Sandip - 08 June 2016 - Translation Phase II
    public String strCreatedDate {get;set;}
    // End
    public string OwnerFullName {get;set;}
    public Datetime RequestedDueDate {get;set;}
    // Sandip - 08 June 2016 - Translation Phase II
    public String strRequestedDueDate {get;set;}
    //End
    public Datetime ExpirationDate{get;set;}
    public string ServiceAddress {get;set;}
    public string Credit_Assessment{get;set;}
    public string Credit_Request_Status{get;set;}
    public string OppID {get;set;}
    public string OppName{get;set;}
    public string OppDes{get;set;}
    
    public Integer compareTo(Object compareTo) 
    {
        OCOM_QuoteOrderVO that = (OCOM_QuoteOrderVO) compareTo;
        if(this.CreatedDate < that.CreatedDate)
            return 1;
        else if(this.CreatedDate > that.CreatedDate)
            return -1;
        else
            return 0;
    }
    
        
}