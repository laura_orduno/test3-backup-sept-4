@isTest
private class TestAccountOwnerFieldSynchronization{
    static testmethod void test1()
    {
        Set<String> userNames = new Set<String>{'David Kuczek', 'Doug Morgan', 'Chris Frei'};
            
        Map<String, Id> userIdMap = new Map<String, Id>();
        Map<Id, String> userNameMap = new Map<Id, String>();
        List<String> userNameList = new List<String>{'David Kuczek', 'Doug Morgan', 'Chris Frei', 'David Kuczek', 'Doug Morgan', 'Chris Frei'};
        
        for(User u : [Select id, name from User where Name IN :userNames])
        {
            userIdMap.put(u.name, u.id);
            userNameMap.put(u.id, u.name);
            userNameList.add(u.name);
        }
        
System.debug('userIdMap: ' + userIdMap);
        
        List<Segment__c> segments = populateSubSegmentTable();
        
        Account cbucidAcct = createAccount('TEST CBUCID 001', 'CBUCID', null, true);
        cbucidAcct.Sub_Segment_Code__c = segments[0].id;
        update cbucidAcct;
        
        List<Account> banAccounts = new List<Account>();
        for(Integer rcidCount=0; rcidCount<3;rcidCount++)
        {
        	Account rcidAcct = createAccount('TEST RCID ' + (rcidCount+1), 'RCID', cbucidAcct.id, true);
            
//System.debug('Owner to be set: ' + userNameList[0]);           
System.debug('Owner to be set: ' + userNameList[userNameList.size()-(rcidCount+1)]);           
            
            rcidAcct.OwnerId = userIdMap.get(userNameList[userNameList.size()-(rcidCount+1)]);
//            rcidAcct.OwnerId = userIdMap.get(userNameList[0]);
            rcidAcct.Wireline_PIN__c = '123456';
            rcidAcct.Wireless_PIN__c = '789012';
            
            for(Integer banCount=0; banCount<10;banCount++)
            {
                Account banAcct = createAccount('CHILD BAN (' + rcidAcct.name + ') - ' + (banCount+1), 'BAN', rcidAcct.id, false);            
                banAcct.OwnerId = userIdMap.get(userNameList[rcidCount]);
//                banAcct.OwnerId = userIdMap.get(userNameList[0]);
                
                banAccounts.add(banAcct);
            }
            
            update rcidAcct;
        }    
        
        insert banAccounts;	
        
//        List<Account> accounts = new List<Account>();
//        accounts.add(cbucidAcct);
//        accounts.add(rcidAcct);
//        update accounts;
        
//		System.debug('*****************BEFORE*********************');        
//        synchronizeAccountFieldsWithParent.audit(userIdMap, userNameMap);
        
		System.debug('*****************BATCH*********************'); 
        
		batch_SynchAccountOwnerWithParent b = new batch_SynchAccountOwnerWithParent();
		database.executeBatch(b);
        
    }
    
    static Account createAccount(String name, String recordTypeName, Id parentId, Boolean insertAccount)
    {
    	// get record type map
		Map<String, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Account.getRecordTypeInfosByName();
        
        Account acct = new Account();        
        acct.name = name;
        acct.recordTypeId = rtMap.get(recordTypeName).getRecordTypeId();
        acct.parentId = parentId;
            
        if(insertAccount)
        {
            insert acct;        
        }
        return acct;
    }
       
    static Account getAccount(Id acctId)
    {
    	return [Select id, name, parentId, Wireline_PIN__c, Wireless_PIN__c from account where Id = :acctId limit 1];        
    }    
    
    static User getUserId(String name)
    {
    	return [Select id, name from User where Name = :name limit 1];        
    }   
    
    static List<Segment__c> populateSubSegmentTable()
    {
        Set<String> segmentCodes = new Set<String>{'GY', 'GR', 'CG', 'AW'};
        List<Segment__c> segments = new List<Segment__c>();
        for(String s : segmentCodes)
        {
            Segment__c seg = new Segment__c();
            seg.name = s;
            
            seg.CBU_Abbreviated_Name__c = 'Test';
            seg.CBU_Code__c = 'AB1';
            seg.CBU_Description__c = 'Test';
            seg.CBU_Name__c = 'Test';
            seg.CSH_Managing_Director__c = 'Test';
            seg.CSH_Segment_Name__c = 'Test';
            seg.CSH_Sub_Segment_Description__c = 'Test';
            seg.Channel_Region__c = 'Test';
            seg.ILEC_Flag__c = 'Test';
            seg.Marketing_Managing_Director__c = 'Test';
            seg.Marketing_Segment_Name__c = 'Test';
            seg.Marketing_Sub_Segment_Description__c = 'Test';
            seg.Marketing_Sub_Segment_Short_Description__c = 'Test';
                                                                                                                                                                                                                                                                                                                                              
//            System.debug('seg.name: ' + seg.name);
            
            segments.add(seg);
        }      
        
        try
        {
        	insert segments;
        }
        catch(Exception e)
        {
            System.debug('Exception: ' + e);            
        }

		return segments;        
    }
}