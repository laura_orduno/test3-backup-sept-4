public with sharing class smb_SVOC_Highlights_CaseController {

	public Account acct {get; set;}
	
	
	public PageReference init() {
		
		Id caseId = ApexPages.currentPage().getParameters().get('Id');
		
		Case c = [SELECT AccountId FROM Case WHERE Id=: caseId];
		
		 acct = [SELECT  Id, ParentId, Name, Tax_Exempt_Status__c, SicDesc, Quote_Threshold__c, Phone, PIC__c, Wireline_PIN__c, Wireless_PIN__c, 
                        Legal_Business_Name__c, DUNS__c, DNTC__c, REG_STATUS__C, LEGAL_NAME_CHECKED__C, LEGAL_NAME_LAST_CHECKED__C, DELINQUENCY_FLAG__C, TRADE_STYLE_NAME__C, CREDIT_PORTAL_TIER__C, Credit_Standing__c, Operating_Name_DBA__c, 
                        Credit_Class__c, CREDITREF_NUM__C, BillingStreet, BillingState, BillingPostalCode, BAN__c,
                        BillingCountry, BillingCity, Account_Open_Since__c, Account_Close_Day__c, Partner_Type__c, Industry,
                        Legacy_Client__c, BAN_CAN__c, Registration_Incorporation_Number__c, Churn_Threat_Flag__c, Fraud__c,
                        Chronic__c, Description, Account_Status__c, Incorporation_Jurisdiction__c, Type_of_Business__c, 
                        Customer_Segment__c, smb_Strategic_Customer_Icon_Name__c, smb_Churn_Threat_Icon_Name__c, 
                        smb_Fraud_Risk_Icon_Name__c, smb_Chronic_Icon_Name__c, Strategic__c
                        FROM Account WHERE Id =: c.AccountId];
		
		return null;
	}
}