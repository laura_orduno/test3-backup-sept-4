public class AssigningDealsupportController extends AssigningControllerTemplate {
    
    protected override String getQueueName(){            
        String[] idList = super.getIdList();   
        if (idList==null) return null;
        ID theFirstId = idList[0];  
        try{      
            Offer_House_Demand__c theDemand = [select Deal_Support_Team__c 
                         from Offer_House_Demand__c 
                         where id = :theFirstId];                     
            if(String.isNotBlank(theDemand.Deal_Support_Team__c))
                return  'Deal_Support_'+ theDemand.Deal_Support_Team__c; 
        }catch(Exception e){
            System.debug('Record ID = '+ theFirstId +' may have just been deleted.');
        } 
        return null;
    }
    
    protected override void assignToUser(String[] idList, ID userId) {         
        Offer_House_Demand__c[] selectedOfferHouseRecords = [
            select id, Previous_Owner__c, OwnerId, CreatedById, Status__c, 
                    Last_OH_Agent_Name__c, Owner_Fullname__c, Offer_House_Agent__c 
            from Offer_House_Demand__c 
            where id in :idList];        
        for (Offer_House_Demand__c theRecord:selectedOfferHouseRecords){
            System.debug(LoggingLevel.ERROR, 'id: ' + theRecord.id +', Status: ' +theRecord.Status__c);
            String currentStatus = theRecord.Status__c;
            if('Submitted'.equalsIgnoreCase(currentStatus)
               ||'Resubmitted'.equalsIgnoreCase(currentStatus)
               ||'Open'.equalsIgnoreCase(currentStatus)){
                theRecord.Last_OH_Agent_Name__c = theRecord.Owner_Fullname__c;
                theRecord.OwnerId = userId; 
                theRecord.Offer_House_Agent__c = userId;             
                theRecord.Status__c = 'Open';
           }else if('Create Contract'.equalsIgnoreCase(currentStatus)
               ||'Contract Incomplete'.equalsIgnoreCase(currentStatus)
               ||'Contract In Progress'.equalsIgnoreCase(currentStatus)){
                theRecord.OwnerId = userId; 
                theRecord.Deal_Support_Contract_Agent__c = userId;             
                theRecord.Status__c = 'Contract In Progress'; 
           }
        }        
        update selectedOfferHouseRecords;
    }    
        
}