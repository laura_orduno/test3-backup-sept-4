/*****************************************************
    Name: UpdateProductChildItemBatchJob
    Usage: This batch job will update quantity and minquantity to zero
            TO RUN THIS batch job FROM ANONYMOUS MODE  paste this code
            use below code to run next job
            UpdateProductChildItemBatchJob jobObject = new UpdateProductChildItemBatchJob(true);
            Id batchJobId = Database.executeBatch(jobObject,2000);
            
            To Restrict next job, use below code
            UpdateProductChildItemBatchJob jobObject = new UpdateProductChildItemBatchJob();
            Id batchJobId = Database.executeBatch(jobObject,2000);
    Author – Sandip Chaudhari (IBM)
    Revision History -
    Created By/Date - Sandip Chaudhari / 08 Dec 2016
    
    Updated By/Date/Reason for Change - 
    
******************************************************/
public class UpdateProductChildItemBatchJob implements Database.Batchable<sObject>{
    public String query = 'select vlocity_cmt__Quantity__c, vlocity_cmt__MinQuantity__c FROM vlocity_cmt__ProductChildItem__c ' +
                'WHERE vlocity_cmt__ChildProductId__r.Catalog__r.Name LIKE \'Essentials%\'';
    public String exceptionStr = '';
    public Boolean isError = false;
    public Boolean executeNextJob = false;
    
    // Constructor to get query from outside
    public UpdateProductChildItemBatchJob(String queryp){
        query = queryp;
    }
    
    public UpdateProductChildItemBatchJob(Boolean isExecuteNextJob){
        executeNextJob = isExecuteNextJob;
    }
    
    // Constructor to get query from outside and decide whether to execute next job or not
    public UpdateProductChildItemBatchJob(String queryp, Boolean isExecuteNextJob){
        query = queryp;
        executeNextJob = isExecuteNextJob;
    }
    
    // Constructor with default query
    public UpdateProductChildItemBatchJob(){
                      
    }
    
    // Start the batch job
    public Database.QueryLocator start(Database.BatchableContext BC){
       // Access initialState here
        return Database.getQueryLocator(query);
    }
    
    // Execute the batch job
    public void execute(Database.BatchableContext BC, List<sObject> scope) {
        // Access initialState here
        try{
            List<vlocity_cmt__ProductChildItem__c> lstUpdate = new List<vlocity_cmt__ProductChildItem__c>();
            for(Sobject sObj : scope){
                vlocity_cmt__ProductChildItem__c objProdChild = (vlocity_cmt__ProductChildItem__c)sObj;
                objProdChild.vlocity_cmt__Quantity__c = 0;
                objProdChild.vlocity_cmt__MinQuantity__c = 0;
                lstUpdate.add(objProdChild);
            }
            update lstUpdate;
        }catch (Exception ex){
            isError = true;
            exceptionStr = ex.getMessage();
        }
    }
    
    public void finish(Database.BatchableContext BC) {
        // Initiate next batch job
        ExternalAuditLogHelper.ExternalAuditLogCreateParameter paramObj = new  ExternalAuditLogHelper.ExternalAuditLogCreateParameter();
        paramObj.emailAddress = '';
        paramObj.jobName = ExternalAuditLogHelper.OCOM_UPDATE_PROD_CHILDITEM_BATCH_JOB;
        if(isError){
            paramObj.result = ExternalAuditLogHelper.OCOM_ETL_JOB_ERROR;
        }else{
            paramObj.result = ExternalAuditLogHelper.OCOM_ETL_JOB_SUCCESS;
        }
        paramObj.resultDescription = exceptionStr;
        paramObj.batchId = bc.getJobId();
        paramObj.emailSubject = System.Label.UpdateProductChildItem + ' ';
        paramObj.executeNextJob = executeNextJob;
        ExternalAuditLogHelper.createAuditLog (paramObj);
    }
}