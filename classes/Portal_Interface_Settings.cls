/**
 * Created by dpeterson on 2/20/2018.
 */

public with sharing class Portal_Interface_Settings {
    public String caseOriginValue;
    public Decimal manageRequestListLimits;
    public Decimal overviewCaseLimit;

    public Portal_Interface_Settings(String co, Decimal mr, Decimal ocl){
        this.caseOriginValue = co;
        this.manageRequestListLimits = mr;
        this.overviewCaseLimit = ocl;
    }
}