/*
    *******************************************************************************************************************************
    Class Name:     OCOM_ContractUtil 
    Purpose:        Util class for handling contract related logic
    Created By:     Adharsh 
    Modified by:           
    *******************************************************************************************************************************      
*/


global without sharing class OCOM_ContractUtil {
   // public cpy_SubmitOrderCntrl soController = new cpy_SubmitOrderCntrl();
    //public soController.LineItem lineItemWrapper;
   // public static Boolean isCallContinuation=false;

    public static String SetValueFromTestMethod = '';
    public static Map<String,object> createNewContract( Map<String,Object> inputMap, Map<String,Object> outMap){ //id orderId,Boolean isSuccess,String messageDisplay,String creditStatus,Boolean areContractable,String contractRequired){
        id orderId = (id)inputMap.get('orderId');
        Boolean isSuccess = false;
       // Boolean isSuccess = (Boolean)inputMap.get('isSuccess');
        String messageDisplay = '';
        String creditStatus = (String)inputMap.get('creditStatus');
        Boolean areContractable = (Boolean) inputMap.get('areContractable');
        Boolean isMoveOutOrder = (Boolean) inputMap.get('isMoveOutOrder');
        String contractStatus = '';
        List<contract> contractToupdate = new List<contract>();
        String contractLastModifiedDate = '';
        String contractLastModby = '';
        String contractNumber='';
        Boolean isOnlyCancelContract = false;
        Boolean validContractExists = false;
        Set<String> contractEndStatusSet = new Set<String> {'Cancelled Request', 'Client Declined', 'Deactivated', 'Acceptance Expired','Terminated'};
       
        String contractId;
        try {
             Id ContractRecordTypeId = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Vlocity Contracts').getRecordTypeId();
            system.debug('creditStatus' + creditStatus + areContractable);
            if( orderId != null && (creditStatus != null && (creditStatus.equalsIgnoreCase('Not Required') || creditStatus.equalsIgnoreCase('Completed') || creditStatus.startswithIgnoreCase('Approved'))) && areContractable == true && isMoveOutOrder == false) {
               

                //showContractSection = true;
                List<Contract>  lstExistingContracts = [Select Id,Status,BA_Contract_Status__c,Name,No_Amend__c,LastModifiedDate,LastModifiedBy.Name,ContractNumber,vlocity_cmt__OrderId__r.Status
                                                        From Contract where vlocity_cmt__OrderId__c =:orderId 
                                                        and Status NOT IN :contractEndStatusSet ];
                
                if(lstExistingContracts != null && lstExistingContracts.size() >0 ){  //&& (lstExistingContracts[0].status == 'Draft'
                    
                    for(Contract existingContract : lstExistingContracts){
                    //messageDisplay = Label.OCOM_Contract_Exists;
                    //Contract existingContract =  lstExistingContracts[0];
                    contractId = existingContract.id;
                    if(existingContract.status != 'Draft' && existingContract.status != 'Cancelled')
                    {   
                        outMap.put('attachmentRequired',false);
                        contractNumber = Label.OCOM_Contract_Number + ': ' + existingContract.ContractNumber;
                        contractStatus = existingContract.Status;
                        contractLastModifiedDate = existingContract.LastModifiedDate.format('MMMM d, yyyy hh:mm aaa');
                        contractLastModby = existingContract.LastModifiedBy.Name;
                        messageDisplay =  contractLastModby + ', '+ contractLastModifiedDate; 
                         isSuccess = true;
                        isOnlyCancelContract = false;
                        validContractExists = true;
                        break;
                       
                    }
                     else if(existingContract.status == 'Cancelled' && existingContract.vlocity_cmt__OrderId__r.Status == 'Cancelled')
                    {  
                        System.debug('Cancelled Order....'); 
                        outMap.put('attachmentRequired',false);
                        contractNumber = Label.OCOM_Contract_Number + ': ' + existingContract.ContractNumber;
                        contractStatus = existingContract.Status;
                        contractLastModifiedDate = existingContract.LastModifiedDate.format('MMMM d, yyyy hh:mm aaa');
                        contractLastModby = existingContract.LastModifiedBy.Name;
                        messageDisplay =  contractLastModby + ', '+ contractLastModifiedDate; 
                         isSuccess = true;
                         break;
                       
                    }
                    else if(existingContract.status == 'Cancelled' && existingContract.vlocity_cmt__OrderId__r.Status != 'Cancelled' && validContractExists == false)
                    {  
                        System.debug('Cancelled Contract....'); 
                        isOnlyCancelContract = true; 
                       
                    }
                    else if(existingContract.status == 'Draft')
                    {
                        isOnlyCancelContract = false;
                        validContractExists = true;
                        System.debug('Update Contract Clis...');
                        String result = vlocity_cmt.ContractServiceResource.updateContract(orderId);
                         
                        if(result == Label.vlocity_cmt.ContractUpdateSuccess){
                            system.debug('Contract Name_11___' + existingContract.Name);
                            List<Id> contractIds = new List<Id>{existingContract.id };
                            contractToUpdate = OCOM_ContractContractableTotal.setContractTotalMRCNRC(contractIds);
                            if(contractToUpdate != null && contractToUpdate.size()>0 && !contractToUpdate.isEmpty() ){
                                update contractToUpdate;
                            }
                            system.debug('Attachment Required______' + result);
                            system.debug('Contract Name____' + existingContract.Name);
                            outMap.put('attachmentRequired',true); 
                            contractNumber = Label.OCOM_Contract_Number + ': ' + existingContract.ContractNumber;
                            contractStatus = existingContract.Status;
                            contractLastModifiedDate = existingContract.LastModifiedDate.format('MMMM d, yyyy hh:mm aaa');
                            contractLastModby = existingContract.LastModifiedBy.Name;
                            messageDisplay =  contractLastModby + ', '+ contractLastModifiedDate; 
                             isSuccess = true;

                        }
                        else{

                            messageDisplay = result;
                            outMap.put('attachmentRequired',false);
                             isSuccess = true;
                        }
                    }
                }
                    
                }
                 if(lstExistingContracts == null || lstExistingContracts.size() ==0 || (isOnlyCancelContract == true && validContractExists == false)) {
                    contractNumber = Label.OCOM_Generate_Contract;
                    contractId = vlocity_cmt.ContractServiceResource.createContractWithTemplate(orderId);
                    system.debug('ContractID___::'+contractId);
                    if(contractId != null && contractId != ''){
                        List<Id> contractIds = new List<Id>{contractId};
                        contractToUpdate = OCOM_ContractContractableTotal.setContractTotalMRCNRC(contractIds);
                        if(contractToUpdate != null && contractToUpdate.size() > 0 && !contractToUpdate.isEmpty()){
                            Contract conToupdate = contractToUpdate[0];
                            conToupdate.No_Amend__c = true;
                            conToupdate.RecordTypeId = ContractRecordTypeId;
                       		//contractToupdate.add(new Contract(id= contractId, No_Amend__c = true, RecordTypeId = ContractRecordTypeId ));
                        }
                        if(contractToUpdate != null && contractToUpdate.size()>0 && !contractToUpdate.isEmpty() ){
                            update contractToUpdate;
                        }
                        isSuccess = true;
                        contractStatus = 'Draft';
                        contractNumber = Label.OCOM_Contract_Created;
                        outMap.put('attachmentRequired',true);

                    }
                }  
            }
            else if( creditStatus == null || (orderId != null && creditStatus != null && !(creditStatus.equalsIgnoreCase('Not Required') || creditStatus.equalsIgnoreCase('Completed') || creditStatus.startswithIgnoreCase('Approved')) && areContractable == true && isMoveOutOrder == false)){
                    contractStatus = Label.OCOM_Contract_Credit_Check_Pending;
                    isSuccess = true;
            }
            else {
                contractStatus = Label.OCOM_Contract_NotRequired;
                isSuccess = true;
            }
            system.debug('ContractID in method'+contractId);
            outMap.put('isSuccess',isSuccess);
            outMap.put('messageDisplay',messageDisplay);
            outMap.put('creditStatus',creditStatus);
            outMap.put('contractStatus',contractStatus);
            outMap.put('contractID',contractId);
            outMap.put('contractNumber',contractNumber);
            //outMap.put('contractObj',contractObj);
            
        }catch(exception e){
            System.debug(LoggingLevel.ERROR, 'Exception is '+e);
            System.debug(LoggingLevel.ERROR, 'Exception stack trace '+e.getStackTraceString());
            messageDisplay = e.getMessage();
            isSuccess = false;
            outMap.put('isSuccess',isSuccess);
            outMap.put('messageDisplay',messageDisplay);
            outMap.put('creditStatus',creditStatus);
            outMap.put('contractStatus',contractStatus);
            outMap.put('contractNumber',contractNumber);
            outMap.put('contractObj',null);
            return outMap;
        }
        return outMap;
    }
    
    //Not required
   /* public static Map<String,String> getcontractableName(){
         Map<String,String>ProdctToTemplateNameMap = new Map<String,String>();
        Map<String,Set_Doc_Template__c> objNameMap =  Set_Doc_Template__c.getAll();
            
            for ( Set_Doc_Template__c prodFamName : objNameMap.values() ){
                ProdctToTemplateNameMap.put(String.valueof(prodFamName.Name).toLowerCase(),String.valueof(prodFamName.DocumentTemplateName__c));
            }
 
         return ProdctToTemplateNameMap;
 
    }*/

// Template Picker Method for WP3 - Needs to be modified for WP4
      public static String getTemplateName(){
         String contractableTemplateName = '';
        Map<String,Set_Doc_Template__c> objNameMap =  Set_Doc_Template__c.getAll();
            
            for ( Set_Doc_Template__c prodFamName : objNameMap.values() ){
                if(prodFamName.Name.equalsIgnoreCase('OCOM Business Anywhere') )
                    contractableTemplateName = prodFamName.DocumentTemplateName__c;
            }
 
         return contractableTemplateName;
 
    }
    
    //Not required
    //TODO::  Remove this class and only Look for Track as Agreement field on Top offer.
    //
   /* public static Map<String,Object> contractableOrderCheck(Map<String, Object> inputMap , Map<String, Object> outMap){
      
        
        Set<id> porductIdSet = new Set<id>();
        Map<id,Boolean> contractableProductMap = new Map<id,Boolean>();
        if(inputMap.containsKey('product2Ids')){
            porductIdSet.addall((Set<id>)inputMap.get('product2Ids'));
        }
        else if(inputMap.containsKey('LiToProducts')){
            Map<id,id> liToProductMap = (Map<id,id>) inputMap.get('LiToProducts');
            porductIdSet.addall(liToProductMap.values());
        }
        if(porductIdSet != null && porductIdSet.size()>0){
        //for (cpy_SubmitOrderCntrl.LineItem liw:lineItemList) { 
        for(vlocity_cmt__CatalogProductRelationship__c  cpr: [select id,name,vlocity_cmt__CatalogId__r.name,vlocity_cmt__Product2Id__r.Id,vlocity_cmt__Product2Id__r.Name
                                                              from vlocity_cmt__CatalogProductRelationship__c  
                                                              where vlocity_cmt__Product2Id__c  in :porductIdSet]){
           // orderItem oi =  (orderItem)liw.li;
            Map<String,String> contractableNameMap = new Map<String,String>();
            contractableNameMap =OCOM_ContractUtil.getcontractableName();
            //if(oi != null){
                  String categoryName = cpr.vlocity_cmt__CatalogId__r.name != null ? String.valueof(cpr.vlocity_cmt__CatalogId__r.name):'';
                if( contractableNameMap.size() > 0 && !contractableNameMap.isEmpty() && contractableNameMap.containsKey(categoryName.toLowerCase()) ){     
                    Boolean areContractable = true;
                    String templatePicker= contractableNameMap.get(categoryName.toLowerCase());
                    contractableProductMap.put(cpr.vlocity_cmt__Product2Id__r.id,areContractable);
                    outMap.put('areContractable', areContractable);
                    outMap.put('templatePicker', templatePicker);
                    outMap.put('contractableProductId',contractableProductMap);
                    //break;
                }
            
            }
        }
        return outMap;
    }
    
    //Not required
    public static Map<String,Object> AmendOrder(Order orderObj){ 
     
       Map<String,Object> outMap = new Map<String,Object>();


        List<Contract>  lstExistingContracts = [Select a.Id, a.vlocity_cmt__OpportunityId__c,Status,BA_Contract_Status__c,Envelop_Id__c
                                                From Contract a where a.vlocity_cmt__OrderId__c =:orderObj.id 
                                                and Status NOT IN ('Cancelled Request', 'Cancelled','Client Declined', 'Deactivated', 'Acceptance Expired','Terminated')];
        
        
        if (lstExistingContracts != null && lstExistingContracts.size() >0  )
        {
           outMap =  UpdateContractStatus(lstExistingContracts);
           
        }  
        if(outMap.size() == 0  && outMap.isEmpty() ) {
            outMap.put('isCallContinuation',false);
             outMap.put('amendContractResponse',Label.SUCCESS);
        }
        return outmap;
    }*/

// Method to Amend the contracts

    public static Map<String,Object> AmendOrder(Order orderObj, String actionName){ 
     
       Map<String,Object> outMap = new Map<String,Object>();
       List<contract> contractWithoutEnvelope = new List<contract>();
       List<contract> contractWithEnvelope = new List<contract>();

       for(Contract  Contracts : [Select Id,vlocity_cmt__OrderId__c,Status,BA_Contract_Status__c,
                                    Envelop_Id__c,Name, OwnerId,Status_Category__c,No_Amend__c,
                                    (Select id from R00N80000002vvynEAA__r )
                                    From Contract where vlocity_cmt__OrderId__c =:orderObj.id 
                                    and Status NOT IN ('Cancelled Request', 'Cancelled','Client Declined', 'Deactivated', 'Acceptance Expired','Terminated')]){
                            String contractStatus = Contracts.Status;
                            List<dsfs__DocuSign_Status__c> docuSignStatus = new List<dsfs__DocuSign_Status__c>(); 
                            docuSignStatus = contracts.R00N80000002vvynEAA__r;
                            if(String.IsNotBlank(contractStatus) && (contractStatus.equalsIgnoreCase('Contract Sent') && docuSignStatus != null && docuSignStatus.size() > 0 && !docuSignStatus.isEmpty())) {
                                contractWithEnvelope.add(Contracts);

                            }
                            else if(Contracts.No_Amend__c != true){
                                contractWithoutEnvelope.add(Contracts);
                            }
                            else if(actionName.equalsIgnoreCase('Cancel') ||  docuSignStatus == null ||  docuSignStatus.size() == 0 || docuSignStatus.isEmpty()){
                                 contractWithoutEnvelope.add(Contracts);
                            }
       }

        if(contractWithoutEnvelope != null && contractWithoutEnvelope.size() > 0){
            outMap =  updateContractStatusNoEnvelope(contractWithoutEnvelope);
        }
        
        if (contractWithEnvelope != null && contractWithEnvelope.size() >0  )
        {
           outMap =  UpdateContractStatusWithEnvelope(contractWithEnvelope);
           
        }  
        if(outMap.size() == 0  && outMap.isEmpty() ) {
            outMap.put('isCallContinuation',false);
             outMap.put('amendContractResponse',Label.SUCCESS);
        }
        return outmap;
    }

//Method to update contracts that doesnot require voiding of Envelope
    public static Map<String,Object> updateContractStatusNoEnvelope(List<contract> lstContract){
        Boolean isCallContinuation = false;
        String amendContractResponse = '';
        Map<String,Object> outMap = new Map<String,Object>();
        try{
            if(lstContract!= null && lstContract.size()>0){
                 for(Contract cntrct: lstContract){
                    List<dsfs__DocuSign_Status__c> docuSignStatus = new List<dsfs__DocuSign_Status__c>(); 
                    docuSignStatus = cntrct.R00N80000002vvynEAA__r;
                    String contractStatus = cntrct.Status;
                     if(String.IsNotBlank(contractStatus) && (contractStatus.equalsIgnoreCase('Fax Contract Validation Required'))){
                            cntrct.Status = 'Cancelled';
                      }else if(String.IsNotBlank(contractStatus) && (contractStatus.equalsIgnoreCase('Contract Registered'))){
                            cntrct.Status = 'Deactivated';
                        }
                        else if (String.IsNotBlank(contractStatus) && (contractStatus.equalsIgnoreCase('Draft'))) {
                            cntrct.Status = 'Cancelled';
                        }
                        else if(String.IsNotBlank(contractStatus) && (contractStatus.equalsIgnoreCase('Contract Sent')) && (docuSignStatus == null ||  docuSignStatus.size() == 0 || docuSignStatus.isEmpty())){
                            cntrct.Status = 'Cancelled';
                        }
                        else {
                            cntrct.Status = 'Terminated';
                        }
                    }
                 update lstContract;
            }
            if(vlocity_cmt.FlowStaticMap.FlowMap.Containskey('forContractTestCoverageNoEnvelopException')){
                throw new fotTestCoverageException('FAILURE');
            }
            outMap.put('isCallContinuation',false);
            outMap.put('amendContractResponse',Label.SUCCESS);
           // return outMap;
            System.debug('@@lstContract- with no Envelope____:'+lstContract);

            }catch(exception e){
                System.debug(LoggingLevel.ERROR, 'Exception is '+e);
                System.debug(LoggingLevel.ERROR, 'Exception stack trace '+e.getStackTraceString());
                isCallContinuation=false;
                outMap.put('isCallContinuation',isCallContinuation);
                outMap.put('amendContractResponse',e.getMessage());
                return outMap;
            }
        return outMap;
    }
    //Method to Cancel Contract and Void envelope 

    public static Map<String,Object> UpdateContractStatusWithEnvelope(List<contract> lstContract){
        Boolean isCallContinuation = false;
        String amendContractResponse = '';
         Map<String,Object> outMap = new Map<String,Object>();
         //Added line for Test Coverage
          try{
            if(vlocity_cmt.FlowStaticMap.FlowMap.Containskey('forContractTestCoverageNoEnvelopException')){
                            throw new fotTestCoverageException('FAILURE');
                        } 
            if(lstContract!= null && lstContract.size()>0){
              
                    for(Contract cntrct: lstContract){
                        String contractStatus = cntrct.Status;
                       
                    
                     if(String.IsNotBlank(contractStatus) && (contractStatus.equalsIgnoreCase('Contract Sent'))){
                            if(Test.isRunningTest())
                            amendContractResponse = SetValueFromTestMethod;
                            else
                                amendContractResponse = OCOM_DocuSignUtilClass.doCancel_vlocity(cntrct);
                            if(amendContractResponse == Label.SUCCESS){
                            system.debug('Response is:::' + amendContractResponse  );
                                //isCallContinuation=true;
                                //outMap.put('isCallContinuation',isCallContinuation);
                                outMap.put('amendContractResponse',amendContractResponse);
                               
                                break;
                            }else{
                                
                                //isCallContinuation=false;
                                //outMap.put('isCallContinuation',isCallContinuation);
                                outMap.put('amendContractResponse',amendContractResponse);
                                break;
                            }

                            
                        }
                        
                    }
                 }   
                    System.debug('@@lstContract'+lstContract);
            }catch(exception e){
                System.debug(LoggingLevel.ERROR, 'Exception is '+e);
                System.debug(LoggingLevel.ERROR, 'Exception stack trace '+e.getStackTraceString());
                isCallContinuation=false;
                outMap.put('isCallContinuation',isCallContinuation);
                outMap.put('amendContractResponse',e.getMessage());
                return outMap;
                //return outMap.put('sendOrderResponse',e.getStackTraceString());
            }
        return outMap;
    }

/******************************* Method to check for Contractable Offers    **************************/
public static Map<String, Object> getContractableOfferList(Map<String,Object> inputMap){
    String CONTRACTTERM = 'Contract Term';
    Boolean isContractable = false;
    //List<Order> orderList = (List<Order>) inputMap.get('orderList');
    Map<id,List<OrderItem>> ordertoOLIMap = (Map<Id,List<OrderItem>>)inputMap.get('ordertoOLIMap');
    Map<Id,List<OrderItem>> orderToContractableOfferMap = new Map<Id, List<OrderItem>>();
    Map<Id,String> oliToContractTermMap = new Map<Id,String>();
    Map<String,Object> outMap = new Map<String, Object>();

    if(null != ordertoOLIMap && ordertoOLIMap.size() > 0 && !ordertoOLIMap.isEmpty()){
        //Iterate on All Order
        for(Id ordId: ordertoOLIMap.keySet()){
            OrderItem topOffer = new OrderItem();
            List<OrderItem> contractableTopOffersList = new List<OrderItem>();
            List<OrderItem> oliList = ordertoOLIMap.get(ordId);  
                //Iterate on all orderItems for the current Order
                for(OrderItem oli: oliList){
                     // Assuming that the OrderLineitems will be sorted by LineNumber.
                     if(oli.vlocity_cmt__ParentItemId__c == null || oli.vlocity_cmt__ParentItemId__c == ''){
                         topOffer = Oli;
                     }
                    Map<String,OCOM_VlocityJSONParseUtil.JSONWrapper>  attributeMap = OCOM_VlocityJSONParseUtil.getAttributesFromJSON(oli.vlocity_cmt__JSONAttribute__c);  
                     //Check for Contractable offers
                     if(null!= attributeMap.get(CONTRACTTERM) && !String.valueof(attributeMap.get(CONTRACTTERM).value).equalsIgnoreCase('MTM') ){
                         oliToContractTermMap.put(oli.id,attributeMap.get(CONTRACTTERM).value);
                         contractableTopOffersList.add(topOffer);
                         
                        }  
                    }
                
                //Generate a map of order to Contractable Top Offers
                if(contractableTopOffersList.size() > 0 && !contractableTopOffersList.isEmpty()) {
                    orderToContractableOfferMap.put(ordId, contractableTopOffersList);
                }  
            }
        }

        // contractable identfier. When set true this will invoke contract per Master Order
        if(orderToContractableOfferMap.size() > 0 && !orderToContractableOfferMap.isEmpty() ){
            isContractable = true;
        }

        outMap.put('orderToContractableOfferMap',orderToContractableOfferMap);
        outMap.put('OliToContractTermMap',OliToContractTermMap);
        outMap.put('isContractable',isContractable);

        return outMap;

    }

     public static String getRootLineNumber(String lineNumber){
        List<String> lineNumberList = new List<String>();
        String parentLineNumber = 'D';
        if(lineNumber != null || lineNumber != ''){
            lineNumberList = lineNumber.split('\\.');
            if(lineNumberList.size() > 0 && !lineNumberList.isEmpty()){
                parentLineNumber = lineNumberList[0];
            }

        }
        return parentLineNumber;
    }
      
      public class fotTestCoverageException extends Exception{}     
    
}