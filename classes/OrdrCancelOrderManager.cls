//Update by Aditya Jamwal(IBM)		10-Aug-2017   BMPF-5 - Cancel Sub Order for Multisite Orders
global class OrdrCancelOrderManager implements Queueable,Database.AllowsCallouts { //BMPF-5 Added Queueable interface
    
    public  static final String SUCCESS_SYMBOL = 'success';
    public String ObjectId;
    private static final String EXTERNAL_ORDER_ID_NOT_FOUND = 'External order Id not found with the order ID as ' ;    
    private static final String APPEX_CLASS_AND_METHOD_NAME = 'CancelCustomerOrder_v2_1_vs0.OrdrCancelOrderManager#cancelOrder';
    private static final String ORDER_NOT_FOUND = 'The expected order not found with the order ID as ' ; 
    private static final String NOT_FOUND_SYMBOL = 'notFound';
    
    //BMPF-5 constructor to set object id to cancel order via queueable job.
    global OrdrCancelOrderManager(String objectId){
        this.objectId = objectId;
    }
    global OrdrCancelOrderManager(){
       
    }
    //BMPF-5 execute method to process cancel request in a queue.
    public void execute(QueueableContext context) {
        if(String.isNotBlank(objectId)){   
            Order ord = new Order(id = objectId,status='Cancelled');
            try{
                cancelOrder(objectId);
                
            }catch(exception ex){
                map<string,schema.recordtypeinfo> recordTypeInfoMap = webservice_integration_error_log__c.sobjecttype.getdescribe().getrecordtypeinfosbyname();
                Webservice_Integration_Error_Log__c log=new Webservice_Integration_Error_Log__c(apex_class_and_method__c='OrdrCancelOrderManager.cancelOrder',external_system_name__c='NC',webservice_name__c='Cancel Order',object_name__c='ORDER',sfdc_record_id__c = objectId);
                log.recordtypeid=recordTypeInfoMap.get('Failure').getrecordtypeid();
                log.error_code_and_message__c = ex.getmessage();
                log.stack_trace__c = ex.getstacktracestring();
                ord.Status = 'Error';
                insert log;
            }            
            update ord; 
        }
    }
  
    /*
   		SFDC CRM agent will trigger the corresponding operation per GUI to invoke this method
   		that invokes OrdrCancelOrderWsCallout#cancelOrder() method
   */ 
    public static TpFulfillmentCustomerOrderV3.CustomerOrder cancelOrder(Id orderId)
    {   
        System.debug('Enter OrdrCancelOrderManager#cancelOrder() with orderId=' + orderId);        
        String result = NOT_FOUND_SYMBOL;        
        TpFulfillmentCustomerOrderV3.CustomerOrder customerOrder = null;
        
        try
        {   
            String externalOrderId =  retrieveExternalOrderId(orderId);
            System.debug('retrieveExternalOrderId=' + externalOrderId);
              
            if( String.isNotBlank(externalOrderId) )
            {                   
                customerOrder = OrdrCancelOrderWsCallout.cancelOrder(externalOrderId);
         
                if( customerOrder != null)
                {                
                	List <TpCommonBaseV3.CharacteristicValue>  characteristicValues =  customerOrder.CharacteristicValue;
               
                	if(characteristicValues != null)
                	{
                   		for(TpCommonBaseV3.CharacteristicValue characteristicValue: characteristicValues)
                   		{                  
                        	TpCommonBaseV3.Characteristic characteristic = characteristicValue.Characteristic;
                        	if(characteristic!= null && String.isNotBlank( characteristic.Name)) 
                        	{                 
                            	if(OrdrConstants.STATUS.equalsIgnoreCase(characteristic.Name))
                            	{
                                	if( characteristicValue.Value != null && !characteristicValue.Value.isEmpty())
                                	{
                                    	if(SUCCESS_SYMBOL.equalsIgnoreCase(characteristicValue.Value[0]))
                                    	{
                                        	result = SUCCESS_SYMBOL;
                                    	}
                                	}
                            	}                    
                        	}
                    	}                
                	}
            	}
           }
           else
           {
               //should never be reached
              OrdrErrUtils.externalOrderIdNotFoundHandler(orderId, APPEX_CLASS_AND_METHOD_NAME, EXTERNAL_ORDER_ID_NOT_FOUND );   
           }  
        }
        catch (CalloutException e)
        {//system exception such as WS server down 
            system.debug('  order issue'+string.valueOf(orderid).length()+' '+orderid +' '+e);
        //    OrdrErrUtils.logCalloutError(orderid, null, e, APPEX_CLASS_AND_METHOD_NAME, OrdrConstants.EXTERNAL_SYSTEM_NAME_NC, OrdrCancelOrderWsCallout.WS_NAME,OrdrConstants.OBJECT_NAME,null);
            throw e;
        }
        
        if( NOT_FOUND_SYMBOL.equals(result))
        {
            //should never be reached
           OrdrErrUtils.orderNotFoundHandler(orderId, ORDER_NOT_FOUND, APPEX_CLASS_AND_METHOD_NAME );             
        }
        
        return customerOrder;

    }  
   
    private static String retrieveExternalOrderId(Id orderid)
	{   
	   String externalOrderId = '';
	   try
	   {
	   		Order ordr = [select orderMgmtId__c from order where id = :orderid LIMIT  1];
	   		externalOrderId = ordr.orderMgmtId__c;
	   } 
	   catch(Exception e) 
	   {      
	   	 	OrdrErrUtils.logCalloutError(orderid, null, e, APPEX_CLASS_AND_METHOD_NAME, OrdrConstants.EXTERNAL_SYSTEM_NAME_NC, OrdrCancelOrderWsCallout.WS_NAME, OrdrConstants.OBJECT_NAME, null);
	   }
        
	   return externalOrderId;
	  
	  }     
}