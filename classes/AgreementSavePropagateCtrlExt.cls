public class AgreementSavePropagateCtrlExt {

    public String mode {get;set;}
    public String modeLabel {get;set;}
    private  ApexPages.StandardController controller;
    
    public Map<id,sObject> selectedItemMap {get; set;}
    public List<ItemSelect> displayItems {get;set;}
    public Sobject oldValues {get;set;}
    public List<String> updatedFields {get;set;}
    
    public Sobject currentObj {get;set;} 
    
    public Static String PROPERGATE_TRGET_OBJNAME = 'Subsidiary_Account__c';
    public Static String AGREEMENT_OBJNAME = 'Apttus__APTS_Agreement__c';
    public Static String[] DETAIL_FIELDSETS = new String[]{'New_Details','New_Detail_Activations','New_Details_Billing','New_Detail_Renewal','New_Detail_Churn'};
    
    public AgreementSavePropagateCtrlExt(ApexPages.StandardController stdController){
        controller = stdController;
        currentObj = stdController.getRecord();
        displayItems = new  List<ItemSelect>();
        updatedFields = new List<string>();
        mode = 'maindetail';
    }
    
    public void doEdit(){
        mode = 'edit';
        modeLabel = Label.Edit;
    }
    
    public PageReference doCancel(){
        mode = 'maindetail';
        PageReference pr =  Page.AgreementCareViewPage;
        pr.getParameters().put('Id',currentObj.id);
        pr.setRedirect(true);
        return pr;
    }
    
    public void doSave(){
        mode = 'maindetail';
        modeLabel = Label.Detail;
        controller.save();
    }
    
    public PageReference SavePropagate() {
        List<Sobject> toUpdate = new List<Sobject>();
        for(ItemSelect obj : displayItems){
            if(obj.selected){
                for(String f :updatedFields){
                    obj.obj.put(f, currentObj.get(f));
                }
              toUpdate.add(obj.obj);
            }
        }
        controller.save();
        update toUpdate;
        PageReference pg = Page.AgreementCareViewPage;
        pg.setRedirect(true);
        pg.getParameters().put('Id',currentObj.id);
        return pg;
    }
    
    public PageReference showPropagate(){
        displayItems.clear();
        updatedFields.clear();
        mode = 'detail';
        PageReference pg =Page.AgreementPropargatePage;
        currentObj = controller.getRecord();
        selectedItemMap = new Map<id,sObject>();
        String query = Util.queryBuilder(PROPERGATE_TRGET_OBJNAME,new List<String>{'id','Account_Name__c','Account__c','Subsidiary_Account__c.Name' },
                                         new List<string>{'Agreement__c'+'=\''+currentObj.id+'\''});
        List<Sobject> itemList = Database.query(query);
        List<String>  fields = Util.getFieldSetFields('Apttus__APTS_Agreement__c', 'New_Detail_Activations');
        String oldRecQuery = Util.queryBuilder('Apttus__APTS_Agreement__c',Util.getUpdatableFields('Apttus__APTS_Agreement__c'),new List<string>{'id'+'=\''+currentObj.id+'\''});
        List<Sobject> oldRecs =Database.query(oldRecQuery);
        oldValues = oldRecs.get(0);
        for(Sobject item : itemList) {
                if(selectedItemMap.containsKey(item.id)){
                    displayItems.add(new ItemSelect(item,true));    
                }else{
                    displayItems.add(new ItemSelect(item,false));
                }
        }
        Set<String> restrictedTo = new Set<String>();
        for(String fs :DETAIL_FIELDSETS){
            restrictedTo.addALL(Util.getFieldSetFields(AGREEMENT_OBJNAME, fs));
        }
        Set<String> excludeFieldSet = new Set<String>();
        excludeFieldSet.addAll(Util.getFieldSetFields(AGREEMENT_OBJNAME, 'Exclude_Propagat'));
        updatedFields = getChangesFields(AGREEMENT_OBJNAME, currentObj,oldValues,restrictedTo, excludeFieldSet);
        return pg;
        
    }
    
    private List<String> getChangesFields(String objName, SObject newInstance, Sobject oldInstance,Set<String> restrictList,Set<String> exclude){
        List<String> updatedField = new List<String>();
        List<String>  updatableField = Util.getUpdatableFields(objName);
        for(String field : updatableField){
            if( !exclude.contains(field) && restrictList.contains(field))
            if(newInstance.get(field) != oldInstance.get(field)){
                updatedField.add(field);
            }
        }
        return updatedField;
        
    }
    public Boolean getShowEditButton(){
       Schema.DescribeSObjectResult od = Util.getObjectTypeDescribe(AGREEMENT_OBJNAME);
        return od.isUpdateable();
    }
    
    public class ItemSelect{
        public sObject obj {get; set;}
        public boolean selected {get; set;}
        public ItemSelect(sObject obj,boolean sel) {
            this.obj = obj;
            selected = sel;
        }
    }
    
}