/*
###########################################################################
# File..................: SRS_BatchConditioncalculation
# Version...............: 1
# Created by............: Aditya Jamwal (IBM)
# Created Date..........: 04-Oct-2014 
# Last Modified by......: Aditya Jamwal (IBM)
# Last Modified Date....: 16-Jan-2015
# Modification:           Email notification to support team, incase there is any job failed.
# Description...........: Class is calculate condition for related orders,service requests and Opportunities on daily basis.
#
############################################################################ 

*/
//Implementing the Database.Batchable Interface
global class SRS_BatchConditioncalculation implements  Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {
    global final string query;
    global string JobFailed='';
    global integer countFailure=1;

    //This is the query that is passed to the execute method.
    global SRS_BatchConditioncalculation(String q)
    {       
        system.debug('@@@@Inside Batch class' +q);
        if(q == null || q == '') 
        {       
           query ='Select Id,Name,Account_Name__r.Name,Condition_Color__c,recordtypeID,Opportunity__r.Name,FMOEnabled__c,Fox__c,SubmitToFOXFlag__c,Service_Request_Type__c,Target_Date_Type__c,Reason_to_Send_Code__c,FOX_Diary_Remarks__c,Reason_to_Cancel__c,Submitted_Date__c,Target_Date__c,SRS_PODS_Product__r.Name,Service_Request_Status__c,SRS_Service_Address__r.Service_Location__c,City__c,Prov__c,Legacy_Orders_count__c,Requested_Date__c,Next_to_work__c,DAC_Scheduled__c,DAC_Actual__c,RTI_Scheduled__c,RTI_Actual__c,PTD_Scheduled__c,PTD_Actual__c,DD_Scheduled__c,DD_Actual__c,Solution_Prime__c,Solution_Prime__r.name From Service_Request__c where Recordtype.name IN (\'Provide/Install\',\'change\',\'disconnect\') and Service_Request_Status__c Not In(\'Cancelled\',\'completed\')';
            
        }
        else
        {
            query = q;
        }
    }
    //The start method is called at the beginning of a batch Apex job to collect the records or objects to be passed to the interface method execute
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    //close start method
    
    //The execute method is called for each batch of records passed to this method to do all required processing
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<Service_Request__c> lstSRupdateCondition_PmoEnabled = new List<Service_Request__c>();
        List<Service_Request__c> lstSRupdateCondition_FmoEnabled = new List<Service_Request__c>();
        
        try
           {
                for(sObject sr : scope){
                        Service_Request__c obj = (Service_Request__c)sr;   
                        if(obj.Condition_Color__c=='green'|| obj.Condition_Color__c=='yellow')     
                        lstSRupdateCondition_PmoEnabled.add(obj);
                        if(obj.FMOEnabled__c)
                        lstSRupdateCondition_FmoEnabled.add(obj);
                }
                system.debug('@@@@Inside Batch class' +lstSRupdateCondition_PmoEnabled);
                    if(lstSRupdateCondition_PmoEnabled.size()>0)
                        {
                            SRS_CRDB_ConditionCal UpdateCondition= new SRS_CRDB_ConditionCal();
                            UpdateCondition.BatchForFMOEnabledSRs(lstSRupdateCondition_FmoEnabled);
                            UPdateCondition.BatchPMOProcessing(lstSRupdateCondition_PmoEnabled);
                        }
                
             }  
             catch(exception e)
             {
            
              // JobFailed +=ex.getmessage()+'/n';
               system.debug('inside exception');
               JobFailed += countFailure+ ') Type: ' + e.getTypeName() + '<br/> &nbsp;&nbsp;&nbsp;&nbsp;' + 'Message: ' 
    + e.getMessage() + '<br/> &nbsp;&nbsp;&nbsp;&nbsp;' + 'Line #: ' + e.getLineNumber() + '\n' + e.getStackTraceString() + '<br/><br/>';
             countFailure++;
             }         
    }

    //The finish method is called after all batches are processed to send confirmation emails or execute post-processing operations
    global void finish(Database.BatchableContext BC){
    
        if(String.isnotBlank(JobFailed))
            { 
                string message;
                string njob='job';
                List<string> emailaddress;
                countFailure--;
                if(countFailure>1)
                njob='jobs('+countFailure+')';
                SMBCare_WebServices__c wsEmailAddress = SMBCare_WebServices__c.getValues('EmailAddress');
                if(null!= wsEmailAddress)
                {
                     emailaddress = new List<string>();                   
                     for(string eaddr:wsEmailAddress.value__c.split(','))                                   
                     emailaddress.add(eaddr);               
                     Messaging.Singleemailmessage theEmailMessage = new Messaging.Singleemailmessage();
                    theEmailMessage.setToAddresses(emailaddress);
                    theEmailMessage.setSubject('SRS condition calculation '+njob+' failed');
                    message = 'Hi Team, <br/><br/>SRS condition calculation '+njob+' failed, Please look in to below error trace: <br/><br/>'+JobFailed+'<br/>Please contact the concern team.<br/><br/> Thanks<br/> IBM Developement Team';
                    theEmailMessage.setHtmlBody(message);
                    system.debug('>>> :'+theEmailMessage);
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { theEmailMessage});
                }
           } 
        
    }
}