/*
    ###########################################################################
    # File..................: TBOshowCaseAccountPdfExtTest
    # Version...............: 1
    # Created by............: Umesh Atry (TechM)
    # Created Date..........: 17/02/2016 
    # Last Modified by......: Umesh Atry (TechM)
    # Last Modified Date....: 18/02/2016
    # Description...........: Test Class TBOshowCaseAccountPdfExtTest.
    #
    ############################################################################
    */
@isTest
private class TBOshowCaseAccountPdfExtTest {

    static testMethod void TestShowCasePDF_Positive() {
      list<account> listTestacc = new list<account> ();
        Schema.DescribeSObjectResult tboSchema = Schema.SObjectType.Account;
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = tboSchema.getRecordTypeInfosByName(); 

        Schema.DescribeSObjectResult tboCaseSchema = Schema.SObjectType.Case;
        Map<String,Schema.RecordTypeInfo> caseRecordTypeInfo = tboCaseSchema.getRecordTypeInfosByName(); 

        Account RCIDAcc = new Account(Name='testTBORCID',recordtypeid=AccountRecordTypeInfo .get('RCID').getRecordTypeId(), RCID__C='RCID123',  Inactive__c=false,Billing_Account_Active_Indicator__c='Y' );
        insert RCIDAcc;

        Case tboCase= new Case(Subject='testTBO',Created_Date__c=system.TODAY(),status='Draft',
                                recordtypeid=caseRecordTypeInfo.get('TBO Request').getRecordTypeId(),
                                Accountid=RCIDAcc.id);
        insert tboCase;

        Account RCIDchild1= new account(Name='RCIDchild1',recordtypeid=AccountRecordTypeInfo.get('CAN').getRecordTypeId(), parentid=RCIDAcc.id,CAN__C='CAN001',BTN__c='CBN001',BCAN__c='111-CAN001',BAN_CAN__c='111-CAN001', BCAN_Pilot_Org_Indicator__c=1,Inactive__c=false,Billing_Account_Active_Indicator__c='Y');
        insert RCIDchild1;
        listTestacc.add(RCIDchild1);
        
        Account CBNchild1= new account(Name='CBNchild1',recordtypeid=AccountRecordTypeInfo.get('CAN').getRecordTypeId(), parentid=RCIDchild1.id,CAN__C='CAN167',BTN__c='FRA001',BCAN__c='111-CAN001',BAN_CAN__c='111-CAN167', Inactive__c=false,Billing_Account_Active_Indicator__c='Y');
        insert CBNchild1;
        listTestacc.add(CBNchild1);
        
        asset ast = new asset();
        ast.AccountId = CBNchild1.id;
        ast.Type__c = '77050';
        ast.Name = 'WTN';
        insert ast;
        
        asset ast1 = new asset();
        ast1.AccountId = CBNchild1.id;
        ast1.Type__c = '77051';
        ast1.Name = 'WTN';
        insert ast1;
        
        Account CBNchild2= new account(Name='CBNchild2',recordtypeid=AccountRecordTypeInfo.get('CAN').getRecordTypeId(), parentid=RCIDchild1.id,CAN__C='CAN233',BTN__c='FRA002',BCAN__c='111-CAN001',BAN_CAN__c='111-CAN233', BCAN_Pilot_Org_Indicator__c=2,Inactive__c=false,Billing_Account_Active_Indicator__c='Y');
        insert CBNchild2;
        listTestacc.add(CBNchild2);
        
        Account CBNInnerChild1= new account(Name='CBNInnerChild1',recordtypeid=AccountRecordTypeInfo.get('CAN').getRecordTypeId(), parentid=CBNchild2.id,CAN__C='CAN789',BTN__c='FRA002',BCAN__c='111-CAN001',BAN_CAN__c='111-CAN789',Inactive__c=false, Billing_Account_Active_Indicator__c='Y');
        insert CBNInnerChild1;
        listTestacc.add(CBNInnerChild1);    
    
    //different hierarchy
    Account RCIDAcc1 = new Account(Name='testanotherTBORCID',recordtypeid=AccountRecordTypeInfo .get('RCID').getRecordTypeId(), RCID__C='RCID12399');
        insert RCIDAcc1;

        
        Account RCIDchild199= new account(Name='RCIDchild1',recordtypeid=AccountRecordTypeInfo.get('CAN').getRecordTypeId(), parentid=RCIDAcc1.id,CAN__C='CAN00199',BTN__c='CBN00199',BCAN__c='111-CAN00199',BAN_CAN__c='111-CAN00199', BCAN_Pilot_Org_Indicator__c=1,Inactive__c=false,Billing_Account_Active_Indicator__c='Y');
        insert RCIDchild199;
    
    Account RCIDchild1999= new account(Name='RCIDchild1',recordtypeid=AccountRecordTypeInfo.get('CAN').getRecordTypeId(), CAN__C='CAN001999',BTN__c='CBN001999',BCAN__c='111-CAN001999',BAN_CAN__c='111-CAN001999', BCAN_Pilot_Org_Indicator__c=1,Inactive__c=false,Billing_Account_Active_Indicator__c='Y');
        insert RCIDchild1999;
        
       SMBCare_Address__c  smbobj = new SMBCare_Address__c(Account__c = CBNchild1.id,Type__c = 'new', Street__c = 'karveroad', Postal_Code__c = '411036', State__c = 'california', city__C = 'pune'); 
        insert smbobj;
        
            PageReference pageRef = Page.TBOCaseAccountPDF;
            Test.setCurrentPageReference(pageRef);
            test.startTest();
            TBOshowCaseAccountPdfExt extTest = new TBOshowCaseAccountPdfExt(new ApexPages.StandardController(tboCase));     
            
            ApexPages.StandardsetController setcontest = new apexpages.standardsetcontroller(listTestacc);
            System.assertEquals(extTest.AssestMap, null);
            System.assertEquals(extTest.finalAccList, null);
            System.assertEquals(extTest.PdfDetailsList, null);
        
            map<id,account> testAccmap= new map<id,account>(listTestacc);
            extTest.finalAccList= testAccmap; 
            
            extTest.accSearchType='CAN'; //set search type
            extTest.AccSearchNumber='CAN789'; //set accountsearch number CAN789
            extTest.caseID = tboCase.id;
            extTest.mnc = 'searchAndAddSingleAccount';
            
            //start search method call
            extTest.callSearchAllAtRCID();
            System.assertNotEquals(extTest.finalAccList, null);
           
            extTest.searchAndAddSingleAccount();
            extTest.searchAccountsFromCBN();
            extTest.callMe();
            
        pagereference pg =    extTest.CallPdf();
        System.assertNotEquals(extTest.AssestMap, null);
        System.assertNotEquals(extTest.finalAccList, null);
        System.assertNotEquals(extTest.PdfDetailsList, null);
       
    } 
}