@isTest
public class PartnerDashboardControllerTests {
	testMethod static void testOnGenerate() {
		User partner = [SELECT Id FROM User WHERE UserType = 'PowerPartner' AND IsActive = true LIMIT 1];
		
		System.runAs(partner) {
			PartnerDashboardController target = new PartnerDashboardController();
			target.onGenerate();
		}
	}
}