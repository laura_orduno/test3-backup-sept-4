public class smb_Controller_MyOpenCases {
    
    public list<case> lstCases{get{
        return sortList(lstCases);
    } set;}
    
    public String sortBy { get; set; }
    public String sortDir { get; set; }
    
    public smb_Controller_MyOpenCases(){
        /*
        list<case> lstHighPriority = new list<case>();
        list<case> lstMediumPriority = new list<case>();
        list<case> lstLowPriority = new list<case>();
        
        for(case c1 : [select id,CaseNumber,RecTypeName__c,Account.Name,Priority,Status,Subject,type,createddate,
                       Escalation_History__c from case where ownerid =: Userinfo.getUserId() and status != 'Closed']){
            
            if(c1.Priority=='High'){  
               lstHighPriority.add(c1);
            }else if(c1.Priority=='Medium'){
               lstMediumPriority.add(c1);
            }else{
               lstLowPriority.add(c1);
            }
        }
        
        
        lstCases = new list<case>(); 
                       
        if(lstHighPriority.size()>0){
            lstCases.addAll(lstHighPriority);
        }
        if(lstMediumPriority.size()>0){
            lstCases.addAll(lstMediumPriority);
        }
        if(lstLowPriority.size()>0){
            lstCases.addAll(lstLowPriority);
        }
        */

/*  	
		//CF: commented out and changed 2/11        
        lstCases = [select id,CaseNumber,RecTypeName__c,Account.Name,
                    Priority,Status,Subject,type,createddate,
                    Escalation_History__c 
                    from case 
                    where ownerid =: Userinfo.getUserId() and status != 'Closed' order by createddate ASC];
*/
		//CF: Change query to use boolean rather than status for closed cases
        lstCases = [select id,CaseNumber,RecTypeName__c, Account.Name,
                    Priority,Status,Subject,type,createddate,
                    Escalation_History__c 
                    from case 
                    where ownerid =: Userinfo.getUserId() and isClosed != true order by createddate ASC];
        
         system.debug('@@@@lstCases : '+lstCases);
     }
    
    
    public PageReference empty() { return null; }
    
    public List<SObject> sortList(List<SObject> cleanList){
        
        system.debug('@@@@@cleanList : '+cleanList);
        system.debug('@@@@@sortBy : '+sortBy);
        
        if (sortBy == null) { return cleanList; }
        
        List<SObject> resultList = new List<SObject>();
        Map<Object, List<SObject>> objectMap = new Map<Object, List<SObject>>();
        
        for (SObject item : cleanList) {
          system.debug('@@@@@item:'+item);
          
          if (objectMap.get(item.get(sortBy)) == null) {
            objectMap.put(item.get(sortBy), new List<SObject>());
          }
          objectMap.get(item.get(sortBy)).add(item);
        }
        
        List<Object> keys = new List<Object>(objectMap.keySet());
        keys.sort();
        
        for(Object key : keys) {
          resultList.addAll(objectMap.get(key));
        }
        
        cleanList.clear();
        
        if (sortDir == 'ASC') {
          for (SObject item : resultList) {
            cleanList.add(item);
          }
        } else {
          for (Integer i = resultList.size()-1; i >= 0; i--) {
            cleanList.add(resultList[i]);
          }
        }
        
        return cleanList;
  }
  
}