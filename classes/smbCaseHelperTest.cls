/**
 * Created by rdraper on 1/17/2018.
 */

@IsTest
public class smbCaseHelperTest {

    static User otherUser;
    //slight risk that there won't be at least 2 Case queues in the Org, but not likely the case for this Org
    static List<QueueSobject> queues = [SELECT Id, QueueId, Queue.Name FROM QueueSobject WHERE SobjectType = 'Case' LIMIT 2];
    private static Map<String, Schema.RecordtypeInfo> recordTypeInfoMapByName = Case.SObjectType.getDescribe().getRecordTypeInfosByName();
    private static Map<String, Schema.RecordtypeInfo> accountRecordTypeInfoMapByName = Account.SObjectType.getDescribe().getRecordTypeInfosByName();

    static {
        otherUser = createUser();
    }

    static testMethod void testSetOwnerUser(){

        Case testCase = new Case();
        insert testCase;

        testCase = [SELECT Current_Owner__c FROM Case WHERE Id = :testCase.Id];
        System.assertEquals(UserInfo.getUserId(),testCase.Current_Owner__c,'Owner not correct');

        testCase.OwnerId = queues[0].QueueId;
        update testCase;

        testCase = [SELECT Current_Owner__c,Previous_Owner__c FROM Case WHERE Id = :testCase.Id];

        System.assertEquals(null,testCase.Current_Owner__c,'Owner not correct');
        System.assertEquals(UserInfo.getUserId(),testCase.Previous_Owner__c,'Owner not correct');

    }

    static testMethod void testSetOwnerQueueChangeToUser(){

        Case testCase = new Case();
        testCase.OwnerId = queues[0].QueueId;
        insert testCase;

        testCase = [SELECT Current_Owner__c FROM Case WHERE Id = :testCase.Id];
        System.assertEquals(null,testCase.Current_Owner__c,'Owner not correct');

        testCase.OwnerId = otherUser.Id;
        update testCase;

        testCase = [SELECT Current_Owner__c FROM Case WHERE Id = :testCase.Id];
        System.assertEquals(otherUser.Id,testCase.Current_Owner__c,'Owner not correct');

    }

    static testMethod void testDeptAndRegion(){

        Case testCase;
        System.runAs(otherUser){

            testCase = new Case();
            testCase.RecordTypeId = recordTypeInfoMapByName.get('Sales Compensation Inquiries').getRecordTypeId();

            insert testCase;

        }

        testCase = [SELECT Dept__c,Region__c FROM Case WHERE Id = :testCase.Id];
        System.assertEquals(otherUser.Channel_Callidus__c,testCase.Dept__c,'Dept not correct');
        System.assertEquals(otherUser.State,testCase.Region__c,'Region not correct');

    }

    static testMethod void testDeptAndRegionError(){

        Case testCase;
        String errorMessage;
        System.runAs(otherUser){

            testCase = new Case();
            testCase.RecordTypeId = recordTypeInfoMapByName.get('Sales Compensation Inquiries').getRecordTypeId();
            testCase.Region__c = 'AB';
            try {
                insert testCase;
            }catch(Exception ex){
                errorMessage = ex.getMessage();
            }

        }

        System.assert(errorMessage.containsIgnoreCase(Label.Dept_and_region_blank),'validation for Region did not fire');

    }

    static testMethod void testSMBFields(){

        Case testCase;
        System.runAs(otherUser){

            Account acc = new Account(Name='Testing Software', BillingCountry = 'US', BillingState = 'IL');
            acc.RecordTypeId = accountRecordTypeInfoMapByName.get('RCID').getRecordTypeId();
            Account acc2 = new Account(Name='Testing Software2', BillingCountry = 'US', BillingState = 'IL');
            acc2.RecordTypeId = accountRecordTypeInfoMapByName.get('RCID').getRecordTypeId();
            insert new List<Account>{acc,acc2};

            Contact contTest = new Contact();
            contTest.LastName = 'test1';
            contTest.AccountId = acc.Id;
            insert contTest;

            testCase = new Case();
            testCase.RecordTypeId = recordTypeInfoMapByName.get(smbCaseHelper.LEGAL_NAME_UPDATE_CASE_RECORD_TYPE).getRecordTypeId();
            testCase.Updated_Legal_Name__c = 'Testing Software 2';
            testCase.AccountId = acc.Id;
            testCase.ContactId = contTest.Id;
            testCase.Master_Account_Name__c = acc2.Id;
            testCase.Status = 'Draft';
            testCase.transfer_type__c = 'Legal Name Change';
            testCase.Business_Type__c ='Society/Association/Council';
            testCase.Jurisdiction__c ='FEDERAL';
            testCase.Requested_Transfer_Date__c = System.today();
            testCase.Escalated_Case__c = true;
            insert testCase;

            testCase.Status = 'In Progress';
            testCase.TBO_In_Progress_Status__c = 'Default';

            update testCase;

        }

        //Case_Owner_Profile_Name__c
        otherUser = [SELECT Name, Profile.Name FROM User WHERE Id = :otherUser.Id];
        testCase = [SELECT Id, Case_Owner_Profile_Name__c, EntitlementId FROM Case WHERE Id = :testCase.Id];

        System.assertEquals(smbCaseHelper.SMB_CARE_ENTITLEMENT_ID, testCase.EntitlementId, 'EntitlementId not set');
        System.assertEquals(otherUser.Profile.Name, testCase.Case_Owner_Profile_Name__c, 'Owner Profile name not set');

    }

    static testMethod void testTBOCases(){

        Case testCase;
        System.runAs(otherUser){

            Account acc = new Account(Name='Testing Software', BillingCountry = 'US', BillingState = 'IL');
            acc.RecordTypeId = accountRecordTypeInfoMapByName.get('RCID').getRecordTypeId();
            Account acc2 = new Account(Name='Testing Software2', BillingCountry = 'US', BillingState = 'IL');
            acc2.RecordTypeId = accountRecordTypeInfoMapByName.get('RCID').getRecordTypeId();
            insert new List<Account>{acc,acc2};

            Contact contTest = new Contact();
            contTest.LastName = 'test1';
            contTest.AccountId = acc2.Id;
            insert contTest;

            testCase = new Case();
            testCase.RecordTypeId = recordTypeInfoMapByName.get(smbCaseHelper.TBO_CASE_RECORD_TYPE).getRecordTypeId();

            testCase.AccountId = acc.Id;
            testCase.ContactId = contTest.Id;
            testCase.Master_Account_Name__c = acc2.Id;
            testCase.Status = 'Draft';
            testCase.transfer_type__c = 'Legal Name Change';
            testCase.Business_Type__c ='Society/Association/Council';
            testCase.Jurisdiction__c ='FEDERAL';
            testCase.Requested_Transfer_Date__c = System.today();
            insert testCase;

            testCase.Status = 'In Progress';
            testCase.Contact_phone_number__c = contTest.Id;
            testCase.TBO_In_Progress_Status__c = 'Default';
            testCase.Transfer_Type__c = 'Legal Name Change';
            testCase.Other_Working_TELUS_Service__c = 'Test123';
            testCase.Directory_Information__c = 'Listed';
            testCase.Listing_Name__c = 'sdsdsads';
            testCase.Business_Type__c = 'Society/Association/Council';
            testCase.Jurisdiction__c = 'BC';
            testCase.Enter_State__c ='US';
            testCase.Incoming_Only__c = true;
            testCase.Verified_Outgoing_Customer_Not_Available__c = 'qwdwdasdas';
            update testCase;

        }

    }

    static testMethod void testTBOCasesErrorNoAccount(){

        Case testCase;
        String errorMessage;
        System.runAs(otherUser){

            Account acc = new Account(Name='Testing Software', BillingCountry = 'US', BillingState = 'IL');
            acc.RecordTypeId = accountRecordTypeInfoMapByName.get('RCID').getRecordTypeId();
            Account acc2 = new Account(Name='Testing Software2', BillingCountry = 'US', BillingState = 'IL');
            acc2.RecordTypeId = accountRecordTypeInfoMapByName.get('RCID').getRecordTypeId();
            insert new List<Account>{acc,acc2};

            Contact contTest = new Contact();
            contTest.LastName = 'test1';
            contTest.AccountId = acc2.Id;
            insert contTest;

            testCase = new Case();
            testCase.RecordTypeId = recordTypeInfoMapByName.get(smbCaseHelper.TBO_CASE_RECORD_TYPE).getRecordTypeId();

            testCase.AccountId = null;
            testCase.ContactId = contTest.Id;
            testCase.Master_Account_Name__c = null;
            testCase.Status = 'Draft';
            testCase.transfer_type__c = 'Legal Name Change';
            testCase.Business_Type__c ='Society/Association/Council';
            testCase.Jurisdiction__c ='FEDERAL';
            testCase.Requested_Transfer_Date__c = System.today();
            try{
                insert testCase;
            }catch(Exception ex){
                errorMessage =  ex.getMessage();
            }
            System.assert(errorMessage.containsIgnoreCase(Label.TBO_OutgoingORIncomingCustomerMandatory),'incoming/outgoing customer blank validation did not fire');

        }

    }

    static testMethod void testTBOCasesErrorNoAccountOnStatusChange(){

        Case testCase;
        String errorMessage;
        System.runAs(otherUser){

            Account acc = new Account(Name='Testing Software', BillingCountry = 'US', BillingState = 'IL');
            acc.RecordTypeId = accountRecordTypeInfoMapByName.get('RCID').getRecordTypeId();
            Account acc2 = new Account(Name='Testing Software2', BillingCountry = 'US', BillingState = 'IL');
            acc2.RecordTypeId = accountRecordTypeInfoMapByName.get('RCID').getRecordTypeId();
            insert new List<Account>{acc,acc2};

            Contact contTest = new Contact();
            contTest.LastName = 'test1';
            contTest.AccountId = acc2.Id;
            insert contTest;

            testCase = new Case();
            testCase.RecordTypeId = recordTypeInfoMapByName.get(smbCaseHelper.TBO_CASE_RECORD_TYPE).getRecordTypeId();

            testCase.AccountId = acc.Id;
            testCase.ContactId = contTest.Id;
            testCase.Master_Account_Name__c = acc2.Id;
            testCase.Status = 'Draft';
            testCase.transfer_type__c = 'Legal Name Change';
            testCase.Business_Type__c ='Society/Association/Council';
            testCase.Jurisdiction__c ='FEDERAL';
            testCase.Requested_Transfer_Date__c = System.today();
            insert testCase;

            try{
                testCase.Status = 'Closed';
                update testCase;
            }catch(Exception ex){
                errorMessage =  ex.getMessage();
            }
            System.assert(errorMessage.containsIgnoreCase(Label.TBO_IncomingAccountContactMandatory),'incoming/outgoing customer blank validation did not fire');

        }

    }

    static testMethod void testTBOCasesClosed(){

        Case testCase;
        System.runAs(otherUser){

            Account acc = new Account(Name='Testing Software', BillingCountry = 'US', BillingState = 'IL');
            acc.RecordTypeId = accountRecordTypeInfoMapByName.get('RCID').getRecordTypeId();
            Account acc2 = new Account(Name='Testing Software2', BillingCountry = 'US', BillingState = 'IL');
            acc2.RecordTypeId = accountRecordTypeInfoMapByName.get('RCID').getRecordTypeId();
            insert new List<Account>{acc,acc2};

            Contact contTest = new Contact();
            contTest.LastName = 'test1';
            contTest.AccountId = acc2.Id;
            insert contTest;

            testCase = new Case();
            testCase.RecordTypeId = recordTypeInfoMapByName.get(smbCaseHelper.TBO_CASE_RECORD_TYPE).getRecordTypeId();

            testCase.AccountId = acc.Id;
            testCase.ContactId = contTest.Id;
            testCase.Master_Account_Name__c = acc2.Id;
            testCase.Status = 'Draft';
            testCase.transfer_type__c = 'Legal Name Change';
            testCase.Business_Type__c ='Society/Association/Council';
            testCase.Jurisdiction__c ='FEDERAL';
            testCase.Requested_Transfer_Date__c = System.today();

            insert testCase;

            testCase.Status = 'Closed';
            testCase.Resolution_Details__c = 'foo';
            testCase.Contact_phone_number__c = contTest.Id;
            testCase.Transfer_Type__c = 'Legal Name Change';
            testCase.Other_Working_TELUS_Service__c = 'Test123';
            testCase.Directory_Information__c = 'Listed';
            testCase.Listing_Name__c = 'sdsdsads';
            testCase.Business_Type__c = 'Society/Association/Council';
            testCase.Jurisdiction__c = 'BC';
            testCase.Enter_State__c ='US';
            testCase.Incoming_Only__c = true;
            testCase.Verified_Outgoing_Customer_Not_Available__c = 'qwdwdasdas';

            update testCase;

        }

    }

    //LEGAL_NAME_CORRECTION_CASE_RECORD_TYPE

    static testMethod void testTBOCasesLegalNameUpdate() {

        Case testCase;

        Account acc = new Account(Name = 'Testing Software', BillingCountry = 'US', BillingState = 'IL');
        acc.RecordTypeId = accountRecordTypeInfoMapByName.get('RCID').getRecordTypeId();
        Account acc2 = new Account(Name = 'Testing Software2', BillingCountry = 'US', BillingState = 'IL');
        acc2.RecordTypeId = accountRecordTypeInfoMapByName.get('RCID').getRecordTypeId();
        insert new List<Account>{acc, acc2};

        Contact contTest = new Contact();
        contTest.LastName = 'test1';
        contTest.AccountId = acc.Id;
        insert contTest;
        contTest = [SELECT AccountId, Id FROM Contact WHERE Id = :contTest.Id];

        testCase = new Case();
        testCase.RecordTypeId = recordTypeInfoMapByName.get(smbCaseHelper.LEGAL_NAME_UPDATE_CASE_RECORD_TYPE).getRecordTypeId();

        testCase.AccountId = acc.Id;
        testCase.ContactId = contTest.Id;
        testCase.Master_Account_Name__c = acc2.Id;
        testCase.Status = 'Draft';
        testCase.transfer_type__c = 'Legal Name Change';
        testCase.Business_Type__c = 'Society/Association/Council';
        testCase.Jurisdiction__c = 'FEDERAL';
        testCase.Requested_Transfer_Date__c = System.today();
        testCase.AccountId = acc.Id;
        testCase.ContactId = contTest.Id;
        testCase.Master_Account_Name__c = acc2.Id;
        testCase.Updated_Legal_Name__c = 'test foo';

        insert testCase;

        testCase.Status = 'Closed';
        testCase.Resolution_Details__c = 'foo';
        testCase.Subject = 'foo';
        testCase.Transfer_Type__c = 'Legal Name Change';
        testCase.Other_Working_TELUS_Service__c = 'Test123';
        testCase.Directory_Information__c = 'Listed';
        testCase.Listing_Name__c = 'sdsdsads';
        testCase.Business_Type__c = 'Society/Association/Council';
        testCase.Jurisdiction__c = 'BC';
        testCase.Enter_State__c = 'US';
        testCase.Incoming_Only__c = true;
        testCase.Verified_Outgoing_Customer_Not_Available__c = 'qwdwdasdas';
        update testCase;

    }

    private static User createUser() {
        Profile p = [SELECT Id FROM Profile WHERE Name = 'SMB Sales Manager / Director / MD'];
        User user1 = new User(
                Alias = 'testcat9',
                Email = 'standarduser@testorg.com32wfsdfsdfsdf',
                EmailEncodingKey = 'UTF-8',
                LastName = 'Robinson',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                ProfileId = p.Id,
                TimeZoneSidKey = 'America/Los_Angeles',
                UserName = 'standarduser@testorg.com32wfsdfsdfsdf',
                Release_Code__c = '05',
                Channel_Callidus__c = 'test channel',
                State = 'BC'
        );
        insert user1;
        return user1;

    }
}