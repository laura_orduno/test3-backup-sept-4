/*
###########################################################################
# File..................: smb_assignAccountOwnerTerritory_Batch
# Version...............: 26
# Created by............: Andrew Castillo
# Created Date..........: 23-Apr-2013
# Last Modified by......: 
# Last Modified Date....: 
# Description...........: This batch class is used by smb_moveUserToAccountTeam_sch.
Logic: 	1. Query for all account prime users and their territories from Sales_Assignment__c
		2. Update account owners
#
# Copyright (c) 2000-2013. Astadia, Inc. All Rights Reserved.
#
# Created by the Astadia, Inc. Modification must retain the above copyright notice.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any commercial purpose, without fee, and without a written
# agreement from Astadia, Inc., is hereby forbidden. Any modification to source
# code, must include this paragraph and copyright.
#
# Permission is not granted to anyone to use this software for commercial uses.
#
# Contact address: 2839 Paces Ferry Road, Suite 350, Atlanta, GA 30339
# Company URL : http://www.astadia.com
###########################################################################
*/
global class smb_assignAccountOwnerTerritory_Batch implements Database.Batchable<sObject>
{
	global final String query;
	
	global smb_assignAccountOwnerTerritory_Batch(String q)
	{
		query = q;
    }

	/*
    * The start method is called at the beginning of a batch Apex job. Use the start method to collect the records or objects to be passed to the interface method execute. 
    * This method returns either a Database.QueryLocator object or an iterable that contains the records or objects being passed into the job.
    */ 
   	global Database.QueryLocator start(Database.BatchableContext BC)
   	{
      	return Database.getQueryLocator(query);
   	}

	/*
    *   The execute method is called for each batch of records passed to the method. Use this method to do all required processing for each chunk of data.
    *   @praram:- A reference to the Database.BatchableContext object.
    *   @param :- A list of sObjects, such as List<sObject>, or a list of parameterized types. If you are using a Database.QueryLocator, the returned list should be used.
    */
   	global void execute(Database.BatchableContext BC, List<sObject> scope)
   	{
   		assignAccountOwner(scope);
   	}

	 /*
     * The finish method is called after all batches are processed.
     */
    global void finish(Database.BatchableContext BC)
    {

    }
    
	/*
    *   This method assigned the account owner based on the RCID and account prime
    */    
    global static void assignAccountOwner(List<Sales_Assignment__c> SAs)
    {
    	if (SAs.size() == 0)
    	{
    		return;
    	}
    	
    	// create map of RCID to sales assignment records
    	Map<Id, Sales_Assignment__c> rcidMap = new Map<Id, Sales_Assignment__c>();
    	for (Sales_Assignment__c SA : SAs)
    	{
    		rcidMap.put(SA.Account__c, SA);
    	}
    	
    	// query for territories
    	List<Terr_Code__c> Terrs = [Select Id, Territory_Code__c
    								From Terr_Code__c];
	
		Map<String, Id> TerrCodeMap = new Map<String, Id>();
		
    	for (Terr_Code__c Terr : Terrs)
    	{
    		TerrCodeMap.put(Terr.Territory_Code__c, Terr.Id);
    	}
    	
    	List<Account> accts = new List<Account>();
    	
    	// query for related RCID accounts
    	List<Account> RCIDs = [Select Id, ParentId, OwnerId, Wireline_Territory__c, Wireless_Territory__c
    						   From Account
    						   Where Id IN :rcidMap.keySet()];
    	
    	// loop through accounts and create list of accounts to update
    	Map<Id, Account> updatedAcctMap = new Map<Id, Account>();
    	Map<Id, Id> parentAcctMap = new Map<Id, Id>();
    	List<Id> childAcctIds = new List<Id>();
    	for (Account acct : RCIDs)
    	{
    		if (acct.ParentId != null)
    		{
    			parentAcctMap.put(acct.ParentId, acct.Id);
    		}
    		childAcctIds.add(acct.Id);
    		
    		// process account owner and territories
    		Sales_Assignment__c SA = rcidMap.get(acct.Id);
    		
    		Account updateAcct = processOwnerTerritory(SA, acct, TerrCodeMap);
    		
    		if (updateAcct != null)
    		{
	    		updatedAcctMap.put(updateAcct.Id, updateAcct);
    		}
    	}
    	
    	// query for related CBUCID accounts
    	List<Account> CBUCIDs = [Select Id, OwnerId, Wireline_Territory__c, Wireless_Territory__c
    						     From Account
    						     Where Id IN :parentAcctMap.keySet()];
		
		// loop through accounts and create list of accounts to update
    	for (Account acct : CBUCIDs)
    	{
    		Id rcidId = parentAcctMap.get(acct.Id);
    		
    		if (rcidId == null)
    		{
    			continue;
    		}
    		
    		Sales_Assignment__c SA = rcidMap.get(rcidId);
    		
    		Account updateAcct = processOwnerTerritory(SA, acct, TerrCodeMap);
    		
    		if (updateAcct != null)
    		{
	    		updatedAcctMap.put(updateAcct.Id, updateAcct);
    		}
    	} 
    	
    	// query for related BAN/CAN accounts
    	List<Account> CANs = [Select Id, ParentId, OwnerId, Wireline_Territory__c, Wireless_Territory__c
    						  From Account
    						  Where ParentId IN :childAcctIds];

    	// loop through accounts and create list of accounts to update
    	for (Account acct : CANs)
    	{
    		Sales_Assignment__c SA = rcidMap.get(acct.ParentId);
    		
    		Account updateAcct = processOwnerTerritory(SA, acct, TerrCodeMap);
    		
    		if (updateAcct != null)
    		{
	    		updatedAcctMap.put(updateAcct.Id, updateAcct);
    		}
    	} 
    	
    	if (updatedAcctMap.size() > 0)
    	{    		
    		update updatedAcctMap.values();
    	}
    }
    
    global static Account processOwnerTerritory(Sales_Assignment__c SA, Account acct, Map<String, Id> TerrCodeMap)
    {
    	if (SA == null)
		{
			return null;
		}
		else
		{
    		String terrWLN = SA.Territory_WLN__c;
    		String terrWLS = SA.Territory_WLS__c;
    		
    		String terrWLNId = null;
    		if (terrWLN != null && terrWLN != '')
    		{
    			terrWLNId = TerrCodeMap.get(terrWLN);
    		}
    		
    		String terrWLSId = null;
    		if (terrWLS != null && terrWLS != '')
    		{
    			terrWLSId = TerrCodeMap.get(terrWLS);
    		}
    		
    		// check if something has changed
    		if (acct.Wireline_Territory__c != terrWLNId
    			|| acct.Wireless_Territory__c != terrWLSId
    			|| acct.OwnerId != SA.User__c)
    		{
    			acct.Wireline_Territory__c = terrWLNId;
    			acct.Wireless_Territory__c = terrWLSId;
    			acct.OwnerId = SA.User__c;
    			
    			System.debug('****update Account ID: ' + acct.Id);
    			
    			return acct;
    		}
    		
    		return null;
		}    	
    }
}