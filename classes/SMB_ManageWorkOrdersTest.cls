@isTest(SeeAllData=true)
class SMB_ManageWorkOrdersTest {
    static testMethod void createCancelFieldworkWorkOrder() {
    	smb_test_utility.createCustomSettingData();       
        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'New Provide/Upgrade Order';
        insert testOpp_1;
        OpportunityContactRole opprole = new OpportunityContactRole(opportunityID = testOpp_1.Id,contactId = testOpp_1.Contract_Signor__c,isPrimary = true);
        insert opprole;
        smb_test_utility.createPQLI('NewPQL',testOpp_1.Id);
        PageReference pageref = Page.SMB_ManageWorkOrders;
        Test.setCurrentPage(pageref);        
        pageref.getparameters().put('OppID', testOpp_1.Id);
        ApexPages.StandardController sc_1 = new ApexPages.standardController(testOpp_1);    
        Work_Order__c workOrder = new Work_Order__c(Opportunity__c=testOpp_1.id,Job_Type__c = 'Test' ,Install_Type__c = 'Fieldwork', Status__c = 'Reserved', Product_Name__c = 'Test Product Name',
        						 Scheduled_Datetime__c = DateTime.now() , Time_Slot__c = 'Test Slot', Unlinked_Work_Order__c = True ,WFM_Number__c = 'Test Number', Scheduled_Due_Date_Location__c = 'Test Location', Permanent_Comments__c = 'test test');
        insert workOrder;
        list<OpportunityLineItem> olis = [select o.Id,o.work_order__c,o.work_Order__r.Install_Type__c from OpportunityLineItem o where OpportunityId =:testOpp_1.Id 
                                                AND o.Prod_Type__c in ('VOICE','INTERNET')
                                                AND o.Element_Type__c != 'Over line Product'];
        
        Test.startTest();
        SMB_DummyWebServiceResponse_Test.intializeMockWebservice();
        SMB_ManageWorkOrders workOrderManager = new SMB_ManageWorkOrders();
        workOrderManager.workOrderPlaceHolder.location_contact__c=testOpp_1.contract_signor__c;
        workOrderManager.readContact();
        workOrderManager.selectedOpportunityLineItemId=olis[0].id;
        workOrderManager.searchAvailableSlots();
        workOrderManager.workOrderPlaceholder.start_date__c=date.today();
        workOrderManager.searchAvailableSlots();
        workOrderManager.workOrderPlaceholder.duration__c=0.1;
        workOrderManager.searchAvailableSlots();
        workOrderManager.workOrderPlaceholder.install_type__c='Fieldwork';
        workOrderManager.searchAvailableSlots();
        workOrderManager.workOrderPlaceholder.install_type__c='Rackwork';
        workOrderManager.workOrderPlaceholder.duration__c=0.5;
        workOrderManager.searchAvailableSlots();
        workOrderManager.errorCodes='ERR001,ERR002';
        workOrderManager.showErrorMessages();
        workOrderManager.showSystemErrorMessage();
        workOrderManager.CloseCancelRemarks();
        workOrderManager.next();
        workOrderManager.prev();
        workOrderManager.getWeeks();
        workOrderManager.getMonth();
        workOrderManager.showPopUp();
        SMB_ManageWorkOrders.WorkOrderPlaceholderRequest placeHolderRequest=new SMB_ManageWorkOrders.WorkOrderPlaceholderRequest();
        placeHolderRequest.installType='Fieldwork';
        placeHolderRequest.localTimeZone=userinfo.getTimeZone().getdisplayname();
        placeHolderRequest.hasTimeZoneWarning=false;
        placeHolderRequest.bookingRemarks='Test booking remarks';
        placeHolderRequest.changeReason='Test change reason';
        placeHolderRequest.serviceCode='B';
        placeHolderRequest.startDate='02/01/2015';
        placeHolderRequest.timeSlot='08:00-10:00';
        placeHolderRequest.scheduledDueDateLocation='2015-04-22 08:00:00';
        placeHolderRequest.duration='0.5';
        placeHolderRequest.contactId=testOpp_1.contract_signor__c;
        placeHolderRequest.contactName='test name';
        placeHolderRequest.contactEmail='test@telsuemail.com';
        placeHolderRequest.contactHomePhone='416-266-2661';
        placeHolderRequest.contactCellPhone='416-266-2662';
        placeHolderRequest.contactOfficePhone='416-266-2663';
        placeHolderRequest.opportunityLineItemId=olis[0].id;
        work_order__c placeHolderWorkOrder=SMB_ManageWorkOrders.getPlaceholder(placeHolderRequest);
        SMB_ManageWorkOrders.WorkOrderAppointmentRequest appointmentRequest=new SMB_ManageWorkOrders.WorkOrderAppointmentRequest();
        appointmentRequest.workOrderId=placeHolderWorkOrder.id;
        appointmentRequest.eventStart='2015-04-22 08:00:00';
        appointmentRequest.eventStop='2015-04-22 10:00:00';
        appointmentRequest.hasTimeZoneWarning=false;
        SMB_DummyWebServiceResponse_Test.intializeMockWebservice();
        SMB_ManageWorkOrders.bookAppointment(appointmentRequest);
        SMB_DummyWebServiceResponse_Test.intializeMockWebservice();
        SMB_ManageWorkOrders.cancelWorkOrder(placeHolderWorkOrder.id,'Test Cancellation Remark');
        Test.stopTest();
    }
    static testMethod void createCancelSoftwareWorkOrder() {
    	smb_test_utility.createCustomSettingData();      
        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'New Provide/Upgrade Order';
        insert testOpp_1;
        OpportunityContactRole opprole = new OpportunityContactRole(opportunityID = testOpp_1.Id,contactId = testOpp_1.Contract_Signor__c,isPrimary = true);
        insert opprole;
        smb_test_utility.createPQLI('NewPQL',testOpp_1.Id);
        PageReference pageref = Page.SMB_ManageWorkOrders;
        Test.setCurrentPage(pageref);        
        pageref.getparameters().put('OppID', testOpp_1.Id);
        ApexPages.StandardController sc_1 = new ApexPages.standardController(testOpp_1);    
        Work_Order__c workOrder = new Work_Order__c(Opportunity__c=testOpp_1.id,Job_Type__c = 'Test' ,Install_Type__c = 'Fieldwork', Status__c = 'Reserved', Product_Name__c = 'Test Product Name',
        						 Scheduled_Datetime__c = DateTime.now() , Time_Slot__c = 'Test Slot', Unlinked_Work_Order__c = True ,WFM_Number__c = 'Test Number', Scheduled_Due_Date_Location__c = 'Test Location', Permanent_Comments__c = 'test test');
        insert workOrder;
        list<OpportunityLineItem> olis = [select o.Id,o.work_order__c,o.work_Order__r.Install_Type__c from OpportunityLineItem o where OpportunityId =:testOpp_1.Id 
                                                AND o.Prod_Type__c in ('VOICE','INTERNET')
                                                AND o.Element_Type__c != 'Over line Product'];
        
        Test.startTest();
        SMB_DummyWebServiceResponse_Test.intializeMockWebservice();  
        SMB_ManageWorkOrders softwareWorkOrderManager = new SMB_ManageWorkOrders();
        softwareWorkOrderManager.workOrderPlaceHolder.location_contact__c=testOpp_1.contract_signor__c;
        softwareWorkOrderManager.readContact();
        softwareWorkOrderManager.selectedOpportunityLineItemId=olis[0].id;
        softwareWorkOrderManager.searchAvailableSlots();
        softwareWorkOrderManager.workOrderPlaceholder.start_date__c=date.today();
        softwareWorkOrderManager.searchAvailableSlots();
        softwareWorkOrderManager.workOrderPlaceholder.duration__c=0.1;
        softwareWorkOrderManager.searchAvailableSlots();
        softwareWorkOrderManager.workOrderPlaceholder.install_type__c='Software';
        softwareWorkOrderManager.searchAvailableSlots();
        softwareWorkOrderManager.workOrderPlaceholder.duration__c=0.5;
        softwareWorkOrderManager.searchAvailableSlots();
        softwareWorkOrderManager.errorCodes='ERR001,ERR002';
        softwareWorkOrderManager.showErrorMessages();
        softwareWorkOrderManager.showSystemErrorMessage();
        softwareWorkOrderManager.CloseCancelRemarks();
        softwareWorkOrderManager.next();
        softwareWorkOrderManager.prev();
        softwareWorkOrderManager.getWeeks();
        softwareWorkOrderManager.getMonth();
        softwareWorkOrderManager.showPopUp();
        SMB_ManageWorkOrders.WorkOrderPlaceholderRequest softwareRequest=new SMB_ManageWorkOrders.WorkOrderPlaceholderRequest();
        softwareRequest.installType='Software';
        softwareRequest.localTimeZone=userinfo.getTimeZone().getdisplayname();
        softwareRequest.hasTimeZoneWarning=false;
        softwareRequest.bookingRemarks='Test booking remarks';
        softwareRequest.changeReason='Test change reason';
        softwareRequest.serviceCode='B';
        softwareRequest.startDate='02/01/2015';
        softwareRequest.timeSlot='08:00-10:00';
        softwareRequest.scheduledDueDateLocation='2015-04-22 08:00:00';
        softwareRequest.duration='0.5';
        softwareRequest.contactId=testOpp_1.contract_signor__c;
        softwareRequest.contactName='test name';
        softwareRequest.contactEmail='test@telsuemail.com';
        softwareRequest.contactHomePhone='416-266-2661';
        softwareRequest.contactCellPhone='416-266-2662';
        softwareRequest.contactOfficePhone='416-266-2663';
        softwareRequest.opportunityLineItemId=olis[0].id;
        work_order__c softwarePlaceHolderWorkOrder=SMB_ManageWorkOrders.getPlaceholder(softwareRequest);
        SMB_ManageWorkOrders.WorkOrderAppointmentRequest softwareAppointmentRequest=new SMB_ManageWorkOrders.WorkOrderAppointmentRequest();
        softwareAppointmentRequest.workOrderId=softwarePlaceHolderWorkOrder.id;
        softwareAppointmentRequest.eventStart='2015-04-22 08:00:00';
        softwareAppointmentRequest.eventStop='2015-04-22 10:00:00';
        softwareAppointmentRequest.hasTimeZoneWarning=false;
        SMB_DummyWebServiceResponse_Test.intializeMockWebservice();
        SMB_ManageWorkOrders.bookAppointment(softwareAppointmentRequest);
        SMB_DummyWebServiceResponse_Test.intializeMockWebservice();
        SMB_ManageWorkOrders.cancelWorkOrder(softwarePlaceHolderWorkOrder.id,'Test Cancellation Remark');
        OpportunityCallback.onWorkOrderDueDateChange(softwarePlaceHolderWorkOrder.opportunity__c);
        Test.stopTest();
    }
}