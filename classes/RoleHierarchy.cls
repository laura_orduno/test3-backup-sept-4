global class RoleHierarchy implements Database.Batchable<SObject>, Database.Stateful {
    
    //added by ClearTask for preventing the leads from being updated on update of user
    global static Boolean isRunningUserUpdate = false;
    static List<User> userList;
    static Map<id, UserRole> roleMap;
    public String query = 'select id,name, userRoleId, userrole.parentroleid, userrole.name, Manager_1__c, Manager_2__c, Manager_3__c, Manager_4__c from user where Do_Not_Update_Manager_1_4__c = false and UserType <> \'PowerPartner\' and (not userrole.name LIKE \'#%\') ';//and (name = \'Greg Coutts\' OR name = \'Elie A.Hay\')
    // removed the condition isactive = true as per document change request
    //added condition userrole.name NOT like \'#%\' to exclude roles with # in name
    global Set<id> uIds;
    //private final Set<Id> initialState; 
    //set of id of users being updated in the batch 
    global Set<id> userIds = new Set<Id>();
    
    public RoleHierarchy() {
    }
    
    public void switchOffSync() {
        uIds = new Set<Id>();
        for (User u : [Select Id from User where uas__Sync_to_Contact__c = true])
        {
            uIds.add(u.Id);
        }
        System.debug(LoggingLevel.INFO, 'In start: ' + uIds);
        setSync(false);
    }
    
    global database.querylocator start(Database.BatchableContext BC){
        switchOffSync();
        System.debug('query = ' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){
    
        Map<User,List<User>> userHierarchy = createRoleHierarchy(scope);

        List<User> userstoUpdate = new List<User>();
        for(User user : userHierarchy.keySet()) {
            String userRoleName = '';
            if(roleMap.containsKey(user.UserRoleId)){
                userRoleName= roleMap.get(user.userroleId).name;
            }
            if (userRoleName == null || userRoleName.equals('')) continue;
            
            List<User> userManagerList = userHierarchy.get(user);
            List<User> finalManagerList = new List<User>();
            if(!userRoleName.startsWith('_')){//changed top level role symbol to _
                for(User manager : userManagerList) {
                    String managerRoleName = '';
                    if(roleMap.containsKey(manager.UserRoleId)){
                        managerRoleName = roleMap.get(manager.userroleId).name;
                    }
                    if(managerRoleName.startsWith('_')) {
                        finalManagerList.add(manager);
                        break; 
                    }
                    else if(managerRoleName.contains('#')){
                        continue;
                    }
                     else{
                        finalManagerList.add(manager);
                     }
                }
            }
            //if(!finalManagerList.isEmpty()){
                setManagers(user, finalManagerList);
                userstoUpdate.add(user);
                userIds.add(user.Id);
            //}
        }
        if(!userstoUpdate.isEmpty()){
            System.debug('userstoUpdate = ' + userstoUpdate);
            isRunningUserUpdate = true;
            update userstoUpdate;
        }
        
        System.debug(LoggingLevel.INFO, 'In execute: ' + uIds);
    }
    
    //method to check if the user's role name has '+'. If yes, this is the leaf level role and cannot be assigned to the manager field  
    static void setManagers(User u, List<User> managers){
        System.debug(Logginglevel.INFO, 'Managers list for user - '+ u + ' = ' + managers );
        //case where user role has '+'
        //change leaf level role symbol to ~
        if(u.userRole.Name.startsWith('~')){
            if(managers.size() == 0){//if user has no manager then set all manager fields = user himself
             /*   u.Manager_1__c = u.name;
                u.Manager_2__c = u.name;
                u.Manager_3__c = u.name;
                u.Manager_4__c = u.name;*/
            }else if(managers.size() == 1){//if only one manager in the list then set that at 4th manager and rest user himself
                u.Manager_1__c = getName(managers[0]);
                u.Manager_2__c = getName(managers[0]);
                u.Manager_3__c = getName(managers[0]);
                u.Manager_4__c = getName(managers[0]);
            }else if(managers.size() == 2){//if 2 managers then at 3 and 4 positions
                u.Manager_1__c = getName(managers[0]);
                u.Manager_2__c = getName(managers[0]);
                u.Manager_3__c = getName(managers[0]);
                u.Manager_4__c = getName(managers[1]);
            }else if (managers.size() == 3){
                u.Manager_1__c = getName(managers[0]);
                u.Manager_2__c = getName(managers[0]);
                u.Manager_3__c = getName(managers[1]);
                u.Manager_4__c = getName(managers[2]);
            }else{
                u.Manager_1__c = getName(managers[0]);
                u.Manager_2__c = getName(managers[1]);
                u.Manager_3__c = getName(managers[2]);
                u.Manager_4__c = getName(managers[3]); 
            }
        }else{
            if(managers.size() == 0){//if user has no manager then set all manager fields = user himself
                u.Manager_1__c = u.name;
                u.Manager_2__c = u.name;
                u.Manager_3__c = u.name;
                u.Manager_4__c = u.name;
            }else if(managers.size() == 1){//if only one manager in the list then set that at 4th manager and rest user himself
                u.Manager_1__c = u.name;
                u.Manager_2__c = u.name;
                u.Manager_3__c = u.name;
                u.Manager_4__c = getName(managers[0]);
            }else if(managers.size() == 2){//if 2 managers then at 3 and 4 positions
                u.Manager_1__c = u.name;
                u.Manager_2__c = u.name;
                u.Manager_3__c = getName(managers[0]);
                u.Manager_4__c = getName(managers[1]);
            }else if (managers.size() == 3){
                u.Manager_1__c = u.name;
                u.Manager_2__c = getName(managers[0]);
                u.Manager_3__c = getName(managers[1]);
                u.Manager_4__c = getName(managers[2]);
            }else{
                u.Manager_1__c = getName(managers[0]);
                u.Manager_2__c = getName(managers[1]);
                u.Manager_3__c = getName(managers[2]);
                u.Manager_4__c = getName(managers[3]); 
            }
        }
    }
    
    private static String getName(User u){
        String name ;
        if(u.name == null){
            String fname = u.firstname == null ? '' : u.firstname;
            String lname = u.lastname == null ? '' : u.lastname;
            name = fname + lname;
        }else{
            name = u.name;
        }
        return name;
    }
    
    public static Map<User,List<User>> createRoleHierarchy(List<sObject> scope) {
        
         roleMap = new Map<Id, UserRole>(
             [select id,name, ParentRoleId, 
                    (select id, name, userrole.name, userroleid, Manager_1__c, Manager_2__c, Manager_3__c, 
                    Manager_4__c from Users where Do_Not_Include_Name_in_Mgr_1_4__c = false and isactive = true and UserType <> 'PowerPartner') 
                    from UserRole  ]);
//will returns a map of role and all the users asosciated to that role.
        //dont build the hierarchy for the users which have Do_Not_Update_Manager_1_4__c as true, can go in the query itself
        //update only temp managers for now.Manager_1__c ... Manager_4__c     
        
        
        for(sObject s : scope){
            User user = (User)s;
            //System.debug(Logginglevel.INFO, 'Building hierarchy for - ' + user.Name);
            List<User> managerList = new List<User>();
            roleHierarchy.put(user, managerList);           
            String parentRoleId = user.userRole.parentRoleId;
            String userRoleId   = user.UserRoleId;          
            buildHierarchyForUser(user, parentRoleId, userRoleId);          
            //System.debug(Logginglevel.INFO, '*******************');
        }
        
        return roleHierarchy;
    }
    static Map<User,List<User>> roleHierarchy = new Map<User, List<User>>();
    
    private static void buildHierarchyForUser(User user, String parentRoleId, String userRoleId){
        UserRole userManagerRole = roleMap.get(parentRoleId);
        if(userManagerRole == null ) {
            //no manager so return
            return;
        }
        //check if there are users in the manager role
        if(!userManagerRole.users.isEmpty()){
            User manager = userManagerRole.users.get(0);
            roleHierarchy.get(user).add(manager);
        }else{
            User u = new User(alias = 'standt', email='standarduser@testorg.com', emailencodingkey='UTF-8', languagelocalekey='en_US', 
                        localesidkey='en_US', timezonesidkey='America/Los_Angeles', username='standarduser23@testorg.com', userRoleid = parentRoleid);
            String name = userManagerRole.Name.indexOf('(') != -1 ? userManagerRole.Name.subString(userManagerRole.Name.indexOf('(')+1,userManagerRole.Name.indexOf(')')): userManagerRole.Name;
            System.debug('Name = ' + name);
            u.lastName = name;  
            roleHierarchy.get(user).add(u); 
            System.debug('No user in this role-'+roleHierarchy);
        }
        if(userManagerRole.ParentRoleId == null) {
            return;
        }
        buildHierarchyForUser(user, userManagerRole.ParentRoleId, parentRoleId);
    }
    
    global void finish(Database.BatchableContext BC) {
        System.debug(LoggingLevel.INFO, 'In finish: ' + uIds);
        sendEmailToSync();
        sendEmailToService();
    }
    
    public void setSync(Boolean flag) {
        System.debug(LoggingLevel.INFO, 'flag = ' + flag);
        System.debug(LoggingLevel.INFO, 'ids = ' + uIds);
        if (uIds == null) {
            return;
        }
        
        List<User> users = new List<User>();
        for (Id userId : uIds) {
            users.add(new User(Id = userId, uas__Sync_to_Contact__c = flag)); 
        }
        if (! users.isEmpty()) update users;
    }
    
    public void sendEmailToSync() {
        System.debug(LoggingLevel.INFO, 'ids = ' + uIds);
        if (uIds == null || uIds.isEmpty()) {
            return;
        }
        
        String allUsers = '$';
        for (Id userId : uIds) {
            allUsers += userId + ','; 
        }
        allUsers = allUsers.substring(0, allUsers.length() - 1) + '^';
        
        Messaging.Email[] mails = new List<Messaging.Email>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mails.add(mail);
        mail.setSubject('UserContactSync');
        mail.setPlainTextBody(allUsers);
        //SB
        //mail.setToAddresses(new List<String> {'setcontactsyncusersservice@n-69y23cfkl9cd3hf23hjcwy90y.in.sandbox.salesforce.com'});
        //PROD
        mail.setToAddresses(new List<String> {'setcontactsyncusersservice@1sawuicgl6rik57jw38cnh8o7.in.salesforce.com'});
        
        mail.setCcAddresses(new List<String> {'rmehta@apprivo.com'});
        Messaging.sendEmail(mails);
    }
    
    //this method sends email to the email service once the batch completes with the ids of the users being updated. 
    //The email service initiates another batch tp update leads owned by these users  
    public void sendEmailToService() {
        System.debug(LoggingLevel.INFO, 'ids = ' + userIds);
        if (userIds == null || userIds.isEmpty()) {
            return;
        }
        
        String allUsers = '$';
        for (Id userId : userIds) {
            allUsers += userId + ','; 
        }
        allUsers = allUsers.substring(0, allUsers.length() - 1) + '^';
        
        Messaging.Email[] mails = new List<Messaging.Email>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mails.add(mail);
        mail.setSubject('UpdatedUserIds');
        mail.setPlainTextBody(allUsers);
        //SB
       // mail.setToAddresses(new List<String> {'initiateleadupdateonuserroleupdate@2h5wcztlckogyp8t4ix3klhi3.t5vvxmaa.t.apex.sandbox.salesforce.com'});
        //PROD
        mail.setToAddresses(new List<String> {'initiateleadupdateonuserroleupdate@y-1xqxkafmp3r5zqs12fibomqeo.36o0eaa.4.apex.salesforce.com'});
        
        mail.setCcAddresses(new List<String> {'rmehta@apprivo.com'});
        Messaging.sendEmail(mails);
    }


    public static String buildInExpression(Set<Id> ids) {
        String rids = '(';
        for (Id id : ids) {
            rids += ('\'' + id + '\',');
        }
        return rids.substring(0,rids.length() - 1) + ')';
    }
        
}