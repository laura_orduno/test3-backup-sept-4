public class ProductOptionPriceRuleEvaluator extends AbstractPriceRuleEvaluator {
    private final SBQQ__ProductOption__c[] productOptions;
    private List<SBQQ__PriceRule__c> priceRules;
    private List<SObject> targetObjects;
    
    public ProductOptionPriceRuleEvaluator(Id quoteId, SBQQ__ProductOption__c[] productOptions) {
        super(quoteId);
        this.productOptions = productOptions;
    }    
    
    public override SBQQ__PriceRule__c[] getPriceRules() {
        if (priceRules == null) {
            loadPriceRules();
        }
        return priceRules;
    }

    private void loadPriceRules() {
        List<Id> optionalSKUs = new List<Id>();
        for (SBQQ__ProductOption__c productOption : productOptions) {
            if (productOption.SBQQ__OptionalSKU__c != null) {
                optionalSKUs.add(productOption.SBQQ__OptionalSKU__c);
            }
        }
        priceRules = [SELECT Id, SBQQ__ConditionsMet__c, SBQQ__Product__c 
                    FROM SBQQ__PriceRule__c 
                    WHERE SBQQ__TargetObject__c = 'Product Option' AND 
                    (SBQQ__Product__c = null OR SBQQ__Product__c in :optionalSKUs) AND
                    SBQQ__Active__c = true
                    ORDER BY SBQQ__Product__c];
    }
    
    public override SObject[] getTargetObjects() {
        return productOptions;
    }
}