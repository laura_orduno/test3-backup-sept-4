public with sharing class MBRCaseDetailCtlr {
    /************ Static Constants ***************/
 
    
    public String testInput {
        get{ return testInput; }
        set;
    }
    
    public Attachment attachedFile {
        get {
            if (attachedFile == null)
                attachedFile = new Attachment();
            return attachedFile;
        }
        set;
    }
    
    private static final Map<String, String> requestTypeRecordTypeMap;
    private static final Map<String, String> recordTypeRequestTypeMap;
    private static final Map<String, String> extStatusIntStatusMap;
    private static final Map<String, String> intStatusExtStatusMap;
    public static String caseOwner { get; set;}
    public transient List<Collaborators__c> collabs {get; set;}
    public transient List<Attachment> attachs {get; set;}
    public transient List<Attachment> emailAttachs {get; set;}
    public transient Map<Id, List<Attachment>> emailIdToAttachs {get; set;}
    public transient List<CaseComment> comments {get; set;}
    public transient List<EmailMessage> emails {get; set;}
    private CaseComment newComment {get; set;}
    private Collaborators__c newCollab {get; set;}
    public String caseNumber { get { return caseDetail != null ? caseDetail.caseNumber : ' (Not found)'; } set; }
    public String parentCaseNumber { get; set; }
    public Case caseDetail { get; set; }
    public Boolean caseCreatedByLoggedInUser { get; set; }
    public String closeReason { get; set; }
    public List<AttachmentWrapper> caseAttachments {get; set; }

    static {
        List<ExternalToInternal__c> etis = ExternalToInternal__c.getAll().values();
        extStatusIntStatusMap = new Map<String, String>();
        intStatusExtStatusMap = new Map<String, String>();
        for (ExternalToInternal__c eti : etis) {
            if(eti.Identifier__c.equals('onlineStatus')) {
                extStatusIntStatusMap.put(eti.External__c, eti.Internal__c);
                intStatusExtStatusMap.put(eti.Internal__c.toLowerCase(), eti.External__c);
            }
        }
        requestTypeRecordTypeMap= new Map<String, String>();
        recordTypeRequestTypeMap= new Map<String, String>();
        for (ExternalToInternal__c eti : etis) {
            if(eti.Identifier__c.equals('onlineRequestTypeToCaseRT')) {
                requestTypeRecordTypeMap.put(eti.External__c, eti.Internal__c);
            }
            if(eti.Identifier__c.equals('onlineCaseRTToRequestType')) {
                recordTypeRequestTypeMap.put(eti.Internal__c,eti.External__c);
            }
        }
    }
        
    public String getCaseStatus() {
        if(intStatusExtStatusMap.get(caseDetail.status.toLowerCase())==null)
            return Label.MBRUnknownStatus;
        return intStatusExtStatusMap.get(caseDetail.status.toLowerCase());
    }
    
    public String getCaseType() {
        system.debug('caseDetail.recordType.name:'+recordTypeRequestTypeMap.get(caseDetail.recordType.name));
        String requestType = caseDetail.My_Business_Requests_Type__c;
        if(recordTypeRequestTypeMap.get(caseDetail.recordType.name)!=null){
            return recordTypeRequestTypeMap.get(caseDetail.recordType.name);
        }
        else if(requestType != '' && requestType != null){
            return requestType;
        }
        return Label.MBRUnknownStatus;
    }

    public List<String> getDescription() {
       List<String> retval = new List<String>();
       if (caseDetail.description != null) {
            retval = caseDetail.description.split('\n');
       }
       return retval;
    }
        
    public MBRCaseDetailCtlr () {                
system.debug('MBRCaseDetailCtlr CONSTRUCTOR: ' + caseDetail);        
        try
        {
            newComment = new CaseComment();
            newCollab = new Collaborators__c();
            onLoad();
            attachs = caseDetail.getsobjects('Attachments');
            comments = caseDetail.getsobjects('casecomments');
            //collabs = caseDetail.getsobjects('Collaborator__r');
            collabs = new List<Collaborators__c>();
            emails = caseDetail.getsobjects('EmailMessages');
            emailIdToAttachs = new Map<Id,List<Attachment>>();
            if(emails != null) {
                List<Id> emailIds = new List<Id>();
                for(EmailMessage em : emails) {
                    emailIds.add(em.id);
                }
                emailAttachs = [select createdby.firstname, createdby.lastname,Id,Name,Description,ParentId,CreatedDate,LastModifiedDate from Attachment where parentid IN :emailIds order by LastmodifiedDate asc];
                for(Attachment a : emailAttachs) {
                    if(emailIdToAttachs.get(a.parentid)==null) {
                        emailIdToAttachs.put(a.parentid,new List<Attachment>());
                    }
                    emailIdToAttachs.get(a.parentid).add(a);
                }
            }
            
            caseCreatedByLoggedInUser = caseDetail.CreatedById == Userinfo.getUserId();
        }
        catch(Exception e)
        {
            system.debug(e);
            caseDetail = null;            
        }
     }
    
    public PageReference onLoad() {
        String urlCaseNumber = ApexPages.currentPage().getParameters().get('caseNumber');
        System.debug('urlCaseNumber: ' + urlCaseNumber);
        List<Case> caseList = new List<Case>();
        try {
            caseList = [SELECT createdby.firstname, createdby.lastname, NotifyCustomer__c, NotifyCollaboratorString__c, OwnerId, 
                    Resolution_Details__c, My_Business_Requests_Type__c, ParentId, isClosed, Contact.FirstName, Contact.LastName, id, contact.name,
                    subject, description, lastmodifieddate, case_resolution__c, createddate, caseNumber, recordType.name, account.name, type, status, 
                    priority, isEscalated, Escalation_History__c, 
                    (select Active__c,Collaborator_Email__c, Collaborator_Name__c, Role__c from Collaborator__r order by createddate desc),
                    (select createdby.firstname, createdby.lastname,Id,Name,Description,ParentId,CreatedDate,LastModifiedDate, ContentType from Attachments order by LastModifiedDate asc), 
                    (select createdby.firstname, createdby.lastname,Id,CommentBody,ParentId,CreatedDate,LastModifiedDate from casecomments where IsPublished = true order by CreatedDate asc), 
                    (select id, toaddress,fromaddress,fromname,ccaddress,messagedate,subject,htmlbody,textbody,hasattachment from emailmessages) 
                FROM Case 
                WHERE CaseNumber = :urlCaseNumber];
        } catch (Exception e) {
            System.debug('Exception caught: ' + e);
        }
        System.debug('caseList: ' + caseList);
        if(caseList.size() > 0) {
            caseDetail = caseList[0];
System.debug('MBRCaseDetailCtlr onLoad caseDetail: ' + caseDetail);
            attachs = caseDetail.getsobjects('Attachments');
            comments = caseDetail.getsobjects('casecomments');
            caseOwner = caseDetail.Contact.FirstName + ' ' + caseDetail.Contact.LastName;
            if (caseDetail.ParentId != null) {
                List<Case> parentCases = [SELECT Id, CaseNumber FROM Case WHERE Id = :caseDetail.ParentId];
                if (parentCases.size() > 0) {
                    parentCaseNumber = parentCases[0].CaseNumber;
                }
            }
        }
        else {
            PageReference pref = Page.MBRNewCase;
            pref.getParameters().put('ic','1');
            pref.setRedirect(true);
            if (!Userinfo.getUserType().equals('Guest')) return pref;
        }

        return null;
    }
    
    public String getDurationSinceSubmitted(){
        
        String durSince = '';
        if(caseDetail != null)
        {
            Long msNow = datetime.now().getTime();
            Long msCreated = caseDetail.createdDate.getTime();
            durSince = calculateDurationSince(msCreated, msNow);
        }
        return durSince;
    }

    public Static String calculateDurationSince(Long msCreated, Long msNow) {

        if (msNow != null && msCreated != null) {

            Long diff = msNow - msCreated;
            Long seconds = diff / 1000;
            
            Long weeks = seconds / (60 * 60 * 24 * 7);
            seconds -= weeks * 7 * 24 * 60 * 60;
            
            Long days = seconds / (60 * 60 * 24);
            seconds -= days * 24 * 60 * 60;
            
            Long hours = seconds / (60 * 60);
            seconds -= hours * 60 * 60;
            
            Long minutes = seconds / 60;
            seconds -= minutes * 60;
            
            String durationSinceSubmitted = '';
            Integer criteriaCount = 0;
            
            if(weeks > 0)
            {
                durationSinceSubmitted += weeks + ( weeks > 1 ? ' weeks' : ' week');
                criteriaCount++;
            }
            
            if(days > 0)
            {
                durationSinceSubmitted += ( criteriaCount > 0 ? ' ' : '') + days + ( days > 1 ? ' days' : ' day');
                criteriaCount++;
            }
            
            if(hours > 0)
            {
                if(criteriaCount < 2)
                {
                    durationSinceSubmitted += ( criteriaCount > 0 ? ' ' : '') + hours + ( hours > 1 ? ' hours' : ' hour');
                    criteriaCount++;
                }    
            }
            
            if(minutes > 0)
            {
                if(criteriaCount == 0)
                {
                    durationSinceSubmitted += ( criteriaCount > 0 ? ' ' : '') + minutes + ( minutes > 1 ? ' minutes' : ' minute');
                    criteriaCount++;
                }    
            }
            
            if(seconds > 0)
            {
                if(criteriaCount == 0)
                {
                    durationSinceSubmitted += ( criteriaCount > 0 ? ' ' : '') + seconds + ( seconds > 1 ? ' seconds' : ' second' );
                    criteriaCount++;
                }    
            }
            
            if(criteriaCount > 0)
            {
                durationSinceSubmitted += ' ago';
            }
            
            return durationSinceSubmitted;
        }
        
        return '';
    }
    
    public List<AttachmentWrapper> getAttachmentList(){
        caseAttachments = new List<AttachmentWrapper>{};
        if(attachs!=null){
            for(Attachment att: attachs){
                caseAttachments.add(new AttachmentWrapper(att));
            }
        }
        return caseAttachments;
    }
    
    public List<Collaborators__c> getCollaboratorsList(){
        return collabs;
    }
    
    public List<EmailMessage> getEmailList(){
        return emails;
    }
    
    public List<CaseComment> getCommentList(){
        return comments;
    }


    public PageReference updateSubscription(){
        if(caseDetail.NotifyCustomer__c == null){
            caseDetail.NotifyCustomer__c = true;
        }

        System.debug('!!!!!!!!!!!11 '+caseDetail);
        update caseDetail;
        return null;
    }
    
    /**
     * Method to Add Comment and Attachments
     *
     * TODO:  Add validation and Exception Handling.  ie. .addError
     * 
     * @datetime 10/17 7PM
     * @LastModified Dan Reich Traction on demand
     *  
     */
    public PageReference addComment() {
        try {
            System.debug( 'addComment[START]' );
            if (caseDetail == null || caseDetail.isClosed) { 
                return null;
            }

            newComment.parentId = caseDetail.id;
            if(newComment.CommentBody!= null){
                if(attachedFile.body != null && attachedFile.body.size() > 0) {
                    newComment.commentBody += '\n\n' + Label.MBR_See_Attachment + ' ' + attachedFile.name;// getCommentStringFromAttachment( attachment );
                    upload();
                }
                insert newComment;
            }

            if(intStatusExtStatusMap.get(caseDetail.status.toLowerCase()) == Label.MBRAwaitingCustomerStatus) {
                caseDetail.status = Label.MBROpenStatus;
                update caseDetail;
            }
            
            PageReference pr = ApexPages.currentPage();
            pr.setRedirect(true);
            return pr;
        } catch ( Exception e ) {
            QuoteUtilities.log(e); 
        } 
        System.debug( 'addComment[END]' );
        return null;
    }
    
    public PageReference addCollaborator() {
        try {
            if (caseDetail == null) { return null;}
            newCollab.case__c = caseDetail.id;
            if (newCollab.Collaborator_Email__c != null) {
                for (Collaborators__c c : collabs) {
                    if(c.Collaborator_Email__c.equals(newCollab.Collaborator_Email__c)) {
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Collaborator Email already exists'));
                        return null;
                    }
                }
                insert newCollab;
                PageReference pr = ApexPages.currentPage();
                pr.setRedirect(true);
                return pr;
            }
            else{
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Email is required'));
                return null;
            }
        } catch (Exception e) { QuoteUtilities.log(e); } return null;
    }
    
    public CaseComment getNewComment(){
      return newComment;
    }
    
    public Collaborators__c getNewCollab(){
      return newCollab;
    }
    
    public PageReference empty() { return null; }
    
    public void upload() {
        attachedFile.OwnerId = UserInfo.getUserId();
        attachedFile.ParentId = caseDetail.id;
        attachedFile.IsPrivate = false;
    
        try {
          insert attachedFile;
        } catch (DMLException e) {
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
         // return null;
        } finally {
          attachedFile = new Attachment(); 
        }
    
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
        //PageReference pr = ApexPages.currentPage();
        //pr.setRedirect(true);
        //return pr;
    }
    
    @RemoteAction
    public static String changeCollabStatus(Id caseId, Integer i) {
        List<Collaborators__c> collabs = [select Active__c,Collaborator_Email__c, Collaborator_Name__c, Role__c from Collaborators__c where case__c = :caseId order by createddate desc];
        List<CollaboratorWrapper> collabsWrapped = new List<CollaboratorWrapper>();
        Integer idex = 0;
        for (Collaborators__c collab : collabs) {
            collabsWrapped.add(new CollaboratorWrapper(collab, idex));
            idex++;
        }
        if (collabsWrapped[i].collab.active__c == true) {
            collabsWrapped[i].collab.active__c = false;
        }
        else {
            collabsWrapped[i].collab.active__c = true;
        }
        update collabsWrapped[i].collab;
        return 'Done';
    }
    
    public List<CollaboratorWrapper> getCollaboratorWithIndex() {
        List<Collaborators__c> collabs = this.getCollaboratorsList();
        List<CollaboratorWrapper> collabsWrapped = new List<CollaboratorWrapper>();
        if(collabs == null) return collabsWrapped;
        Integer index = 0;
        for (Collaborators__c collab : collabs) {
            collabsWrapped.add(new CollaboratorWrapper(collab, index));
            index++;
        }
        return collabsWrapped;
    }

     public PageReference escalateCase(){
        caseDetail.status = 'escalated';
        caseDetail.isEscalated = true;
        update caseDetail;
        return null;
    }
    
     public PageReference closeCase(){
       if(string.valueOf(caseDetail.OwnerId).startsWith('005')){
            caseDetail.status = 'Closed';
        }
        else{
            caseDetail.Status = 'Client Issue Resolved';
        }
        caseDetail.Resolution_Details__c = closeReason;
        update caseDetail;
        CaseComment reasonComment = new CaseComment(
                                    CommentBody = Label.MBRCancelReason + ' ' + caseDetail.Resolution_Details__c,
                                    ParentId = caseDetail.Id);
        insert reasonComment;     
        onLoad();
        
        return null;
    }

    public PageReference unsubscribe(){
        String caseId = ApexPages.currentPage().getParameters().get('caseId');
            if(caseId!=null){
            Case tmpCase = [SELECT NotifyCustomer__c
                                FROM Case
                                WHERE Id = :caseId];
            if(tmpCase!=null && tmpCase.NotifyCustomer__c){
                    tmpCase.NotifyCustomer__c = false;
            }
            update tmpCase;
        }
        return null;
    }


    public PageReference reopenCase(){
        PageReference submitPage = Page.MBRNewCase;
        submitPage.getParameters().put('reopen', caseDetail.CaseNumber);
        submitPage.setRedirect(true);
        return submitPage;
    }
    
    public class CollaboratorWrapper {
        public Collaborators__c collab { get; set; }
        public Integer index { get; set; }
        public CollaboratorWrapper(Collaborators__c collab, Integer index) {
            this.collab = collab;
            this.index = index;
        }
    }
    
    public List<CommentWrapper> getCommentWrapperList() {
        List<CaseComment> comments = caseDetail.getsobjects('casecomments');
        List<CommentWrapper> commentsWrapped = new List<CommentWrapper>();
        if(comments == null) return commentsWrapped;
        for (CaseComment comment : comments) {
            CommentWrapper cw = new CommentWrapper(comment);
            caseOwner = caseDetail.Contact.FirstName + ' ' + caseDetail.Contact.LastName;
            if(!comment.commentBody.contains(Label.MBREmailCommentAuthor) && !caseOwner.equals(cw.author)){
                cw.author = Label.MBRGenericName;
            }

            commentsWrapped.add( cw );
        }
        return commentsWrapped;
    }

    private Attachment getAttachmentByCommentString( String commentBody ) {
        String attachmentString = commentBody.substringAfter(Label.MBR_See_Attachment);
        String idString = attachmentString.split( '-' )[1].trim();
        if(idString != null && idString != ''){
            Id attachmentId = Id.valueOf(idString );
            return ( new Map<Id, Attachment>( attachs ) ).get( attachmentId );
        }
        return null;
    }

    private String getCommentStringFromAttachment( Attachment commentAttachment ) {
        return commentAttachment.name + ' - ' + commentAttachment.Id;
    }


    /**
     * Internal 
     *
     * TODO:  Add validation and Exception Handling.  ie. .addError
     * 
     * @datetime 10/17 7PM
     * @LastModified Dan Reich Traction on demand
     *  
     */
    public class CommentWrapper {
        public String author { get; set; }
        public Attachment attachedFile {get; set;}
        public List<String> parsedComment { get; set; }
        public String createdDate { get; set; }
        public String durationSince { get; set; }

        /**
         * Constructor of the CommentWrapper class takes a CaseComment and resolves all details required by the View to build out the correct markup
         *
         * @datetime 10/17 7PM
         * @author Rauza Zhenissova Traction
         * @LastModified Dan Reich Traction on demand
         * @LastModified Alex Kong Traction on demand - 12/03 - added durationSince logic
         *  
         */
        public CommentWrapper(CaseComment comment) {
            Boolean guestUser =  comment.Createdby.LastName.equals('Site Guest User');

            String commentBody = '';
            String parsedName = '';
            if(comment.commentBody == null){
                comment.commentBody = '';
            }
            if (guestUser && comment.commentBody.indexOf(']:') != -1 && comment.commentBody.indexOf(']:') > 12) {
                parsedName = 'Collaborator: ' + comment.commentBody.substring(13,comment.commentBody.indexOf(']:'));
                commentBody = comment.commentBody.substring(comment.commentBody.indexOf(']:')+2);
            } 
            else if(comment.commentBody.contains(Label.MBREmailCommentAuthor)){
                commentBody = comment.commentBody.substringAfter(Label.MBREmailCommentAuthor);
                parsedName = comment.commentBody.substring(0, comment.commentBody.indexOf(Label.MBREmailCommentAuthor));
                // remove email address prefix
                if (parsedName.contains('- ')) parsedName = parsedName.substring(parsedName.lastIndexOf('- ') + 2);
            }
            else {
                parsedName = comment.Createdby.firstname + ' '+ comment.Createdby.lastname;
                commentBody = comment.commentBody;
            }

            this.author = parsedName;
            this.parsedComment = getPreservedComment(commentBody);
            this.createdDate = comment.CreatedDate.format('MMM dd, YYY');

            Long msNow = datetime.now().getTime();
            Long msCreated = comment.createdDate.getTime();
            this.durationSince = calculateDurationSince(msCreated, msNow);
        }

        public List<String> getPreservedComment(String comment) {
           if (comment == null) {
                return new List<String>();
           }
           return comment.split('\n');
        }
    }
    

    public class AttachmentWrapper{
        public String iconName {get; set; }
        public String info {get; set; }
        public String fileName {get; set; }
        public Id id {get; set; }

        public AttachmentWrapper(Attachment attach){
            this.filename = attach.name;
            this.iconName = getAttachmentType(this.filename);// attach.ContentType;
            this.info = getAttachmentInfo(attach);
            this.id = attach.Id;

        }
        public String getAttachmentType(String fileName){
            if(fileName.contains('.png') || fileName.contains('.gif') || fileName.contains('.jpg'))
            {
                return 'image';
            }
            
            if(fileName.contains('.zip'))
            {
                return 'zip';
            }
            
            return 'file';
        }
        public String getAttachmentInfo(Attachment attach){
            String attachmentInfo = '';
            DateTime todayDate  = DateTime.now();
            DateTime modifDate = attach.LastModifiedDate;
            String parsedName = ' by ' + caseOwner;

            if(modifDate!=null){
                Integer hourDif = Integer.valueOf((todayDate.getTime() - modifDate.getTime())/(1000*60*60));
                Integer hoursInMonth = 24*30;

                if(hourDif==0){
                    attachmentInfo = 'Uploaded now';
                }
                else if(hourDif == 1){
                    attachmentInfo = 'Uploaded 1 hour ago';
                }
                else if(hourDif<24){
                    attachmentInfo = 'Uploaded ' + hourDif + ' hours ago';
                }
                else if(hourDif < 48){
                    attachmentInfo = 'Uploaded 1 day ago';
                }
                else if(hourDif < hoursInMonth){
                    attachmentInfo = 'Uploaded ' + hourDif/24 + ' days ago';
                }
                else if(hourDif < hoursInMonth*2){
                    attachmentInfo = 'Uploaded over 1 month';
                }
                else{
                    attachmentInfo = 'Uploaded ' + 'over ' + hourDif/hoursInMonth + 'months ago';
                }
            }
            return attachmentInfo + parsedName;
        }

    }
}