/*
###########################################################################
# File..................: SRS_CRDB_ConditionCal_Test
# Version...............: 1
# Created by............: Aditya Jamwal (IBM)
# Created Date..........: 12-Jan-2015 
# Last Modified by......: Aditya Jamwal (IBM)
# Last Modified Date....: 28-Sept-2015
# Modification:           Added assert statements and increase code coverage to 90%  
# Description...........: Class is to check code coverage for SRS_CRDB_ConditionCal apex class
############################################################################
*/

@isTest

private class SRS_CRDB_ConditionCal_Test {

    //...test method for PMOProcessing method..
    
    static testMethod void SRS_CRDB_ConditionCal_test() {
    
     //......................................................
        List<OrderStatusUpdates_ComplexTypes.LineItem> legacyOrdersLineItems = new List<OrderStatusUpdates_ComplexTypes.LineItem>();
        SRS_CRDB_ConditionCal src= new SRS_CRDB_ConditionCal();
        Test.startTest();

        SRS_Test_Data_Utility testData= new SRS_Test_Data_Utility();
        testData.SRS_ConditionCal_Data_Utility();
             
        OrderStatusUpdates_ComplexTypes.LineItem Li = new OrderStatusUpdates_ComplexTypes.LineItem();
             OrderStatusUpdates_ComplexTypes.DesignAssessmentCompleteDate dac= new OrderStatusUpdates_ComplexTypes.DesignAssessmentCompleteDate();
            dac.DACScheduleDate= date.valueof('2014-12-28');
            dac.DACActualDate=null;
            OrderStatusUpdates_ComplexTypes.DueDate dd = new OrderStatusUpdates_ComplexTypes.DueDate();
            dd.DDScheduleDate=date.valueof('2014-03-04');
            dd.DDActualDate=null;
            OrderStatusUpdates_ComplexTypes.EngineeringReadyDate erd = new OrderStatusUpdates_ComplexTypes.EngineeringReadyDate();
            erd.ERDScheduleDate=date.valueof('2014-03-04');
            erd.ERDActualDate=null;
            OrderStatusUpdates_ComplexTypes.PlantReadyDate prd = new OrderStatusUpdates_ComplexTypes.PlantReadyDate();
            prd.PRDScheduleDate=date.valueof('2014-03-04');
            prd.PRDActualDate=null;
            OrderStatusUpdates_ComplexTypes.PlantTestDate ptd = new OrderStatusUpdates_ComplexTypes.PlantTestDate();
            ptd.PTDScheduleDate=date.valueof('2014-03-04');
            ptd.PTDActualDate=null;
            OrderStatusUpdates_ComplexTypes.ReadyToDate rtd = new OrderStatusUpdates_ComplexTypes.ReadyToDate();
            rtd.RTIScheduleDate=date.valueof('2014-03-04');
            rtd.RTIActualDate=null;
            Li.LegacyOrderID='I250352SEED';
            Li.Sequence='02';
            Li.IssueDate=String.valueof(system.today());
            Li.BillingSystem='CRIS3-BC';
            Li.FoxOrderID='12312345';
            Li.FoxJEOPCode='hrss';
            Li.CRDBJEOPCode='';
            Li.CRIS3HoldCode='';
            Li.CRIS3OrderStatus='';
            Li.DACDate=dac;
            Li.DDate=dd;
            Li.ERDate=erd;
            Li.PRDate=prd;
            Li.PTDate=ptd;
            Li.RTDate=rtd;
            
            List<OrderStatusUpdates_ComplexTypes.LineItem> lolistRed = new List<OrderStatusUpdates_ComplexTypes.LineItem>();
            lolistRed.add(Li);
            src.PMOProcessing(lolistRed);
            
            Test.stopTest();
            
            
            
    }
    
    //...test method for UpdateFOXJeopAndCond,UpdateConToGreenfrmROTrigger methods..
    
    static testMethod void SRS_CRDB_ConditionCal_test2() {
    
        List<Service_Request__c> sereqList = new List<Service_Request__c>();
        Set<Service_Request__c> sereqSet = new Set<Service_Request__c>();
        List<SRS_Service_Request_Related_Order__c> relatedOderList = new List<SRS_Service_Request_Related_Order__c>();
        Set<SRS_Service_Request_Related_Order__c> relatedOderSet = new Set<SRS_Service_Request_Related_Order__c>();
        List<OrderStatusUpdates_ComplexTypes.LineItem> legacyOrdersLineItems = new List<OrderStatusUpdates_ComplexTypes.LineItem>();
        List<SRS_Service_Request_Related_Order__c> relatedOderList1 = new List<SRS_Service_Request_Related_Order__c>();
        SRS_CRDB_ConditionCal src= new SRS_CRDB_ConditionCal();
        Test.startTest();

        SRS_Test_Data_Utility testData= new SRS_Test_Data_Utility();
        testData.SRS_ConditionCal_Data_Utility();
      //  Test.stopTest();

        sereqList=testData.sereqList;
        system.debug('@@serlisdt'+sereqList);
        relatedOderList=testData.relatedOderList;
    
        for(Service_Request__c sr :sereqList)
        sereqSet.add(sr);
        
        for(SRS_Service_Request_Related_Order__c ro :relatedOderList)
        relatedOderSet.add(ro);  
                 
              src.UpdateFOXJeopAndCond(testData.sereqListJeopTrigger,testData.JeopContriggerMap);
              src.UpdateConToGreenfrmROTrigger(relatedOderSet,sereqSet);
              List<OrderStatusUpdates_ComplexTypes.LineItem> lolistGreen = new List<OrderStatusUpdates_ComplexTypes.LineItem>();
            OrderStatusUpdates_ComplexTypes.LineItem L2 = new OrderStatusUpdates_ComplexTypes.LineItem();
            OrderStatusUpdates_ComplexTypes.DueDate ddgreen = new OrderStatusUpdates_ComplexTypes.DueDate();
            ddgreen.DDScheduleDate= date.valueof('2115-12-28');
            ddgreen.DDActualDate=null;
            OrderStatusUpdates_ComplexTypes.EngineeringReadyDate erdgreen = new OrderStatusUpdates_ComplexTypes.EngineeringReadyDate();
            erdgreen.ERDScheduleDate=date.valueof('2115-12-28');
            erdgreen.ERDActualDate=null;
            OrderStatusUpdates_ComplexTypes.PlantReadyDate prdgreen = new OrderStatusUpdates_ComplexTypes.PlantReadyDate();
            prdgreen.PRDScheduleDate=date.valueof('2115-12-28');
            prdgreen.PRDActualDate=null;
            OrderStatusUpdates_ComplexTypes.PlantTestDate ptdgreen = new OrderStatusUpdates_ComplexTypes.PlantTestDate();
            ptdgreen.PTDScheduleDate=date.valueof('2115-12-28');
            ptdgreen.PTDActualDate=null;
            OrderStatusUpdates_ComplexTypes.ReadyToDate rtdgreen = new OrderStatusUpdates_ComplexTypes.ReadyToDate();
            rtdgreen.RTIScheduleDate=date.valueof('2115-12-28');
            rtdgreen.RTIActualDate=null;
            L2.LegacyOrderID='19869';
            L2.IssueDate=String.valueof(system.today());
            L2.BillingSystem='SIMS';
            L2.DDate=ddgreen;
            L2.ERDate=erdgreen;
            L2.PRDate=prdgreen;
            L2.PTDate=ptdgreen;
            L2.RTDate=rtdgreen;
            lolistGreen.add(L2);
            src.PMOProcessing(lolistGreen);
            
           Test.stopTest();    
    }
    
    
    //...test method for CalCondforSearchedROorManualwithTrackerUpdates method..
    
    static testMethod void SRS_CRDB_ConditionCal_tracker3() 
    {
    List<SRS_Service_Request_Related_Order__c> relatedOderList1 = new List<SRS_Service_Request_Related_Order__c>();
        SRS_CRDB_ConditionCal src= new SRS_CRDB_ConditionCal();
          Test.starttest();
               
        //  Create Tracker Records
       List <SRS_CRDB_Push_Tracker__c> trackerlist=new List<SRS_CRDB_Push_Tracker__c>();
         
         Schema.DescribeSObjectResult dsr = Schema.SObjectType.SRS_Service_Request_Related_Order__c; 
          Map<String,Schema.RecordTypeInfo> rtMapByName1 = dsr.getRecordTypeInfosByName();
          Schema.RecordTypeInfo rtByName1 =  rtMapByName1.get('SIMS');
           Map<String,Schema.RecordTypeInfo> rtMapByName2 = dsr.getRecordTypeInfosByName();
          Schema.RecordTypeInfo rtByName2 =  rtMapByName2.get('CRIS3-BC');
          Service_Request__c sj154  = new Service_Request__c();
          insert sj154;
          
         SRS_Service_Request_Related_Order__c ro12= new SRS_Service_Request_Related_Order__c(name='19869',RO_Service_Request__c =sj154.id,CRDBJEOPCode__c='ayx',recordtypeid=rtByName1.getRecordtypeid(),System__c='SIMS',Jeop_Hold_Code__c='xyz',Originating_Date__c=system.today(),ERD_Actual__c=null,ERD_Scheduled__c=(system.today().adddays(-1)),Status__c='O',DD_Actual__c=(system.today().adddays(-1)),DD_Scheduled__c=(system.today().adddays(-1)),PTD_Actual__c=(system.today().adddays(-1)),PTD_Scheduled__c=(system.today().adddays(-1)),PRD_Actual__c=null,PRD_Scheduled__c=null);
         
        SRS_Service_Request_Related_Order__c ro187= new SRS_Service_Request_Related_Order__c(name='I250352SEED',RO_Service_Request__c =sj154.id,recordtypeid=rtByName2.getRecordtypeid(),System__c='CRIS3-BC',Seq__c='02',Status__c='COMPLETED',Jeop_Hold_Code__c='',Originating_Date__c=system.today(),ERD_Actual__c=(system.today().adddays(-1)),ERD_Scheduled__c=(system.today().adddays(-1)),DD_Actual__c=(system.today().adddays(-1)),DD_Scheduled__c=(system.today().adddays(-1)),PTD_Actual__c=null,PTD_Scheduled__c=(system.today().adddays(-1)),PRD_Actual__c=null,PRD_Scheduled__c=null);   
        relatedOderList1.add(ro12);relatedOderList1.add(ro187);
        insert relatedOderList1;
          SRS_CRDB_Push_Tracker__c ro1= new SRS_CRDB_Push_Tracker__c(LegacyOrderID__c='19869',CRIS3OrderStatus__c='O',BillingSystem__c='SIMS',CRDBJEOPCode__c='a',CRIS3HoldCode__c='sa',IssueDate__c=string.valueof(system.today())+' 00:00:00.0',DDateSch__c=String.valueof(system.today().adddays(-1)),DDateAct__c=String.valueof(system.today().adddays(-1)), ERDateSch__c=String.valueof(system.today().adddays(-1)),ERDateAct__c=String.valueof(system.today().adddays(-1)), PRDateSch__c=String.valueof(system.today().adddays(-1)), PRDateAct__c=String.valueof(system.today().adddays(-1)), PTDateSch__c=String.valueof(system.today().adddays(-1)), PTDateAct__c=String.valueof(system.today().adddays(-1)), RTDateAct__c=String.valueof(system.today().adddays(-1)));   
      
          SRS_CRDB_Push_Tracker__c ro2= new SRS_CRDB_Push_Tracker__c(LegacyOrderID__c='19869',CRIS3OrderStatus__c='O',BillingSystem__c='SIMS',CRDBJEOPCode__c='asd',IssueDate__c=string.valueof(system.today())+' 00:00:00.0',DDateSch__c=String.valueof(system.today().adddays(-1)),DDateAct__c=String.valueof(system.today().adddays(-1)),CRIS3HoldCode__c='sa', ERDateSch__c=String.valueof(system.today().adddays(-1)),ERDateAct__c=null, PRDateSch__c=String.valueof(system.today().adddays(-1)), PRDateAct__c='', PTDateSch__c=String.valueof(system.today().adddays(-1)), PTDateAct__c=String.valueof(system.today().adddays(-1)), RTDateAct__c=String.valueof(system.today().adddays(-1)));   
      
          SRS_CRDB_Push_Tracker__c ro3= new SRS_CRDB_Push_Tracker__c(LegacyOrderID__c='I250352SEED',BillingSystem__c='CRIS3-BC',Sequence__c='02',CRDBJEOPCode__c='xyz',CRIS3OrderStatus__c='COMPLETED',CRIS3HoldCode__c='sa',IssueDate__c=string.valueof(system.today())+' 00:00:00.0',DDateSch__c=String.valueof(system.today().adddays(-1)),DDateAct__c=String.valueof(system.today().adddays(-1)), ERDateSch__c=String.valueof(system.today().adddays(-1)),ERDateAct__c=String.valueof(system.today().adddays(-1)), PRDateSch__c=String.valueof(system.today().adddays(-1)), PRDateAct__c=String.valueof(system.today().adddays(-1)), PTDateSch__c=String.valueof(system.today().adddays(-1)), PTDateAct__c=null, RTDateAct__c=String.valueof(system.today().adddays(-1)));
      
      
        trackerlist.add(ro1);trackerlist.add(ro2);trackerlist.add(ro3);//trackerlist.add(ro1a);trackerlist.add(ro4);trackerlist.add(ro5);
            insert trackerlist;
     
        System.assertEquals(trackerlist.size(),3);  
        
        src.CalCondforSearchedROorManualwithTrackerUpdates(relatedOderList1);
        
           Test.stoptest();
    }
    
    //...test method for PMOProcessing and SetLOtoROMapandROtoSRMap methods..
    
    static testMethod void SRS_CRDB_ConditionCal_tracker4() 
    {
        List<SRS_Service_Request_Related_Order__c> relatedOderList1 = new List<SRS_Service_Request_Related_Order__c>();
        SRS_CRDB_ConditionCal src= new SRS_CRDB_ConditionCal();
          Test.starttest();
              
        //  Create Tracker Records
       List <SRS_CRDB_Push_Tracker__c> trackerlist=new List<SRS_CRDB_Push_Tracker__c>();
         
         Schema.DescribeSObjectResult dsr = Schema.SObjectType.SRS_Service_Request_Related_Order__c; 
          Map<String,Schema.RecordTypeInfo> rtMapByName1 = dsr.getRecordTypeInfosByName();
          Schema.RecordTypeInfo rtByName1 =  rtMapByName1.get('SIMS');
           Map<String,Schema.RecordTypeInfo> rtMapByName2 = dsr.getRecordTypeInfosByName();
          Schema.RecordTypeInfo rtByName2 =  rtMapByName2.get('CRIS3-BC');
          Service_Request__c sj154  = new Service_Request__c();
          insert sj154;
          
         SRS_Service_Request_Related_Order__c ro12= new SRS_Service_Request_Related_Order__c(name='19869',RO_Service_Request__c =sj154.id,CRDBJEOPCode__c='ayx',recordtypeid=rtByName1.getRecordtypeid(),System__c='SIMS',Jeop_Hold_Code__c='xyz',Originating_Date__c=system.today(),ERD_Actual__c=null,ERD_Scheduled__c=(system.today().adddays(-1)),Status__c='O',DD_Actual__c=(system.today().adddays(-1)),DD_Scheduled__c=(system.today().adddays(-1)),PTD_Actual__c=(system.today().adddays(-1)),PTD_Scheduled__c=(system.today().adddays(-1)),PRD_Actual__c=null,PRD_Scheduled__c=null);
         
     SRS_Service_Request_Related_Order__c ro13= new SRS_Service_Request_Related_Order__c(name='19870',RO_Service_Request__c =sj154.id,CRDBJEOPCode__c='ayx',recordtypeid=rtByName1.getRecordtypeid(),System__c='SIMS',Jeop_Hold_Code__c='xyz',Originating_Date__c=system.today(),ERD_Actual__c=null,ERD_Scheduled__c=(system.today().adddays(-1)),Status__c='O',DD_Actual__c=(system.today().adddays(-1)),DD_Scheduled__c=(system.today().adddays(-1)),PTD_Actual__c=(system.today().adddays(-1)),PTD_Scheduled__c=(system.today().adddays(-1)),PRD_Actual__c=null,PRD_Scheduled__c=null);
      
      SRS_Service_Request_Related_Order__c ro14= new SRS_Service_Request_Related_Order__c(name='19870',RO_Service_Request__c =sj154.id,CRDBJEOPCode__c='ayx',recordtypeid=rtByName1.getRecordtypeid(),System__c='SIMS',Jeop_Hold_Code__c='xyz',Originating_Date__c=system.today(),ERD_Actual__c=null,ERD_Scheduled__c=(system.today().adddays(-1)),Status__c='O',DD_Actual__c=(system.today().adddays(-1)),DD_Scheduled__c=(system.today().adddays(-1)),PTD_Actual__c=(system.today().adddays(-1)),PTD_Scheduled__c=(system.today().adddays(-1)),PRD_Actual__c=null,PRD_Scheduled__c=null);
         
         
        SRS_Service_Request_Related_Order__c ro187= new SRS_Service_Request_Related_Order__c(name='I250352SEED',RO_Service_Request__c =sj154.id,recordtypeid=rtByName2.getRecordtypeid(),System__c='CRIS3-BC',Seq__c='02',Status__c='COMPLETED',Jeop_Hold_Code__c='',Originating_Date__c=system.today(),ERD_Actual__c=(system.today().adddays(-1)),ERD_Scheduled__c=(system.today().adddays(-1)),DD_Actual__c=(system.today().adddays(-1)),DD_Scheduled__c=(system.today().adddays(-1)),PTD_Actual__c=null,PTD_Scheduled__c=(system.today().adddays(-1)),PRD_Actual__c=null,PRD_Scheduled__c=null);   
        relatedOderList1.add(ro12);relatedOderList1.add(ro13);relatedOderList1.add(ro14);relatedOderList1.add(ro187);
        insert relatedOderList1;
         
            List<OrderStatusUpdates_ComplexTypes.LineItem> legacyOrdersLineItems = new List<OrderStatusUpdates_ComplexTypes.LineItem>();
                 
            List<OrderStatusUpdates_ComplexTypes.LineItem> lolistGreen = new List<OrderStatusUpdates_ComplexTypes.LineItem>();
            OrderStatusUpdates_ComplexTypes.LineItem L2 = new OrderStatusUpdates_ComplexTypes.LineItem();
            OrderStatusUpdates_ComplexTypes.DueDate ddgreen = new OrderStatusUpdates_ComplexTypes.DueDate();
            ddgreen.DDScheduleDate= date.valueof(system.today().adddays(-1));
            ddgreen.DDActualDate=null;
            OrderStatusUpdates_ComplexTypes.EngineeringReadyDate erdgreen = new OrderStatusUpdates_ComplexTypes.EngineeringReadyDate();
            erdgreen.ERDScheduleDate=date.valueof(system.today().adddays(-1));
            erdgreen.ERDActualDate=null;
            OrderStatusUpdates_ComplexTypes.PlantReadyDate prdgreen = new OrderStatusUpdates_ComplexTypes.PlantReadyDate();
            prdgreen.PRDScheduleDate=date.valueof(system.today().adddays(-1));
            prdgreen.PRDActualDate=null;
            OrderStatusUpdates_ComplexTypes.PlantTestDate ptdgreen = new OrderStatusUpdates_ComplexTypes.PlantTestDate();
            ptdgreen.PTDScheduleDate=date.valueof(system.today().adddays(-1));
            ptdgreen.PTDActualDate=null;
            OrderStatusUpdates_ComplexTypes.ReadyToDate rtdgreen = new OrderStatusUpdates_ComplexTypes.ReadyToDate();
            rtdgreen.RTIScheduleDate=date.valueof(system.today().adddays(-1));
            rtdgreen.RTIActualDate=null;
            L2.LegacyOrderID='19869';
            L2.IssueDate=String.valueof(system.today());
            L2.BillingSystem='SIMS';
            L2.CRIS3OrderStatus='COMPLETED';
            L2.DDate=ddgreen;
            L2.ERDate=erdgreen;
            L2.PRDate=prdgreen;
            L2.PTDate=ptdgreen;
            L2.RTDate=rtdgreen;
            lolistGreen.add(L2);
           
      
      OrderStatusUpdates_ComplexTypes.LineItem L3 = new OrderStatusUpdates_ComplexTypes.LineItem();
      L3.LegacyOrderID='I250352SEED';
            L3.IssueDate=String.valueof(system.today());
            L3.BillingSystem='CRIS3-BC';
      L3.CRIS3OrderStatus='COMPLETED';
            L3.DDate=null;
            L3.ERDate=null;
            L3.PRDate=null;
            L3.PTDate=null;
            L3.RTDate=null;
      L3.Sequence='02';

      OrderStatusUpdates_ComplexTypes.LineItem L5 = new OrderStatusUpdates_ComplexTypes.LineItem();
      L5.LegacyOrderID=null;
            L5.IssueDate=null;
            L5.BillingSystem='CRIS3-BC';      
      src.ValidateLegacyOrder(L5);  
      OrderStatusUpdates_ComplexTypes.LineItem L6 = new OrderStatusUpdates_ComplexTypes.LineItem();
      src.ValidateLegacyOrder(L6);
      
      src.PMOProcessing(lolistGreen);
      OrderStatusUpdates_ComplexTypes.LineItem L4 = new OrderStatusUpdates_ComplexTypes.LineItem();
      L4.LegacyOrderID='19870';
            L4.IssueDate=String.valueof(system.today());
            L4.BillingSystem='SIMS';
      L4.CRIS3OrderStatus='COMPLETED';
            L4.DDate=null;
            L4.ERDate=null;
            L4.PRDate=null;
            L4.PTDate=null;
            L4.RTDate=null;
      lolistGreen.add(L4);
      src.SetLOtoROMapandROtoSRMap(lolistGreen);
        List<OrderStatusUpdates_ComplexTypes.LineItem> lolist= new List<OrderStatusUpdates_ComplexTypes.LineItem>();
      lolist.add(L3);
      src.SetLOtoROMapandROtoSRMap(lolist);
           Test.stoptest();    
       
    }
    
  //...test method for UpdateFOXJeopAndCond method..
  
  static testMethod void SRS_CRDB_ConditionCal_tracker5() {
    
        List<Service_Request__c> sereqList = new List<Service_Request__c>();
    Map<id,Service_Request__c> mapdd  = new Map<id,Service_Request__c> ();
        
        SRS_CRDB_ConditionCal src= new SRS_CRDB_ConditionCal();
        Test.startTest();
      Service_Request__c s1 = new Service_Request__c(IsJEOPFromFOX__c=true,service_request_status__c='working',Condition_Color__c='green',   IsFoxJeopResolved__c='N',Jeopardy_Description__c='asfdsas');
    
    Service_Request__c s2  = new Service_Request__c(IsJEOPFromFOX__c=true,service_request_status__c='working',Condition_Color__c='green',IsFoxJeopResolved__c='N',Jeopardy_Description__c=' ');
    
    Service_Request__c s3  = new Service_Request__c(IsJEOPFromFOX__c=true,service_request_status__c='working',Condition_Color__c='green',IsFoxJeopResolved__c='Y',Jeopardy_Description__c=' ');
    
         sereqList.add(s1);sereqList.add(s2);sereqList.add(s3); insert sereqList;
     
     mapdd.put(s1.id,s1);  mapdd.put(s2.id,s2);  mapdd.put(s3.id,s3);
     
     src.UpdateFOXJeopAndCond(sereqList,mapdd);
           System.assertEquals(s3.service_request_status__c,'working');  
        Test.stopTest();
    
    }
  
  //...test method for UpdateConToGreyfrmSR,GetMapServiceRequestwithID and ValidateLegacyOrder methods..
  
  static testMethod void SRS_CRDB_ConditionCal_tracker6() {
    
        List<Service_Request__c> sereqList = new List<Service_Request__c>();
     List<Service_Request__c> sereqList1 = new List<Service_Request__c>();
    set<Service_Request__c> sereqSet = new set<Service_Request__c>();
    List<Opportunity> OppList = new List<Opportunity>();
    
        
        SRS_CRDB_ConditionCal src= new SRS_CRDB_ConditionCal();
        Test.startTest();
    Opportunity opp= new Opportunity(Name='OppTest', StageName='Originated',Project_Number__c='1234', CloseDate=system.today());
    Opportunity opp1= new Opportunity(Name='OppTest1', StageName='Originated',Project_Number__c='1234', CloseDate=system.today());
    Opportunity opp2= new Opportunity(Name='OppTest2', StageName='Originated',Project_Number__c='1234', CloseDate=system.today());
    OppList.add(opp);OppList.add(opp1);OppList.add(opp2);
    insert OppList;
     
      System.assertEquals(opp1.name,'OppTest1');
      
    Service_Request__c s1 = new Service_Request__c(opportunity__c=opp.id,Condition_Color__c='green',service_request_status__c=' avxd');
    
    Service_Request__c s2  = new Service_Request__c(opportunity__c=opp.id,Condition_Color__c='red',service_request_status__c='asbc');
    
    Service_Request__c s3  = new Service_Request__c(opportunity__c=opp.id,Condition_Color__c='yellow',service_request_status__c='ftrd');
    
    Service_Request__c s4 = new Service_Request__c(opportunity__c=opp.id,Condition_Color__c='green',service_request_status__c=' avxd');
    
    Service_Request__c s5  = new Service_Request__c(opportunity__c=opp.id,Condition_Color__c='red',service_request_status__c='asbc');
    
    Service_Request__c s6  = new Service_Request__c(opportunity__c=opp.id,Condition_Color__c='yellow',service_request_status__c='ftrd');
    
    
        sereqList.add(s1);sereqList.add(s2);sereqList.add(s3); insert sereqList;
    sereqList1.add(s4);sereqList1.add(s5);sereqList1.add(s6);
    src.GetMapServiceRequestwithID(sereqList1);
    insert sereqList1;
    
    System.assertEquals(sereqList1.size(),sereqList.size());
     
    for(Service_Request__c s :sereqList)
    
     sereqSet.add(s);
    
     src.UpdateConToGreyfrmSR(sereqSet);
     OrderStatusUpdates_ComplexTypes.LineItem L= new OrderStatusUpdates_ComplexTypes.LineItem();
            src.ValidateLegacyOrder(L);
         Test.stopTest();    
    }
  
  
    //...test method for BatchPMOProcessing method..
    
    static testMethod void SRS_CRDB_ConditionCal_tracker7() {
    
        List<Service_Request__c> sereqList = new List<Service_Request__c>();
    List<Service_Request__c> sereqList1 = new List<Service_Request__c>();
    set<Service_Request__c> sereqSet = new set<Service_Request__c>();
    
    List<SRS_Service_Request_Related_Order__c> relatedOderList1 = new List<SRS_Service_Request_Related_Order__c>();
    List<Opportunity> OppList = new List<Opportunity>();
     Schema.DescribeSObjectResult dsr = Schema.SObjectType.SRS_Service_Request_Related_Order__c; 
          Map<String,Schema.RecordTypeInfo> rtMapByName1 = dsr.getRecordTypeInfosByName();
          Schema.RecordTypeInfo rtByName1 =  rtMapByName1.get('SIMS');
           Map<String,Schema.RecordTypeInfo> rtMapByName2 = dsr.getRecordTypeInfosByName();
          Schema.RecordTypeInfo rtByName2 =  rtMapByName2.get('CRIS3-BC');
        
        SRS_CRDB_ConditionCal src= new SRS_CRDB_ConditionCal();
        Test.startTest();
    Opportunity opp= new Opportunity(Name='OppTest', StageName='Originated',Project_Number__c='1234', CloseDate=system.today());    
    OppList.add(opp);
    insert OppList;
     System.assertEquals(opp.name,'OppTest');
      Service_Request__c s1 = new Service_Request__c(opportunity__c=opp.id,Condition_Color__c='green',service_request_status__c=' avxd');
    
    Service_Request__c s2  = new Service_Request__c(opportunity__c=opp.id,Condition_Color__c='red',service_request_status__c='asbc');    
     sereqList.add(s1);sereqList.add(s2);insert sereqList;
     SRS_Service_Request_Related_Order__c ro14= new SRS_Service_Request_Related_Order__c(name='19870',RO_Service_Request__c =s1.id,CRDBJEOPCode__c='ayx',recordtypeid=rtByName1.getRecordtypeid(),System__c='SIMS',Jeop_Hold_Code__c='xyz',Originating_Date__c=system.today(),ERD_Actual__c=null,ERD_Scheduled__c=(system.today().adddays(-1)),Status__c='C',DD_Actual__c=(system.today().adddays(-1)),DD_Scheduled__c=(system.today().adddays(-1)),PTD_Actual__c=(system.today().adddays(-1)),PTD_Scheduled__c=(system.today().adddays(-1)),PRD_Actual__c=null,PRD_Scheduled__c=null);
         
     
         
          SRS_Service_Request_Related_Order__c ro187= new SRS_Service_Request_Related_Order__c(name='I250352SEED',RO_Service_Request__c =s2.id,recordtypeid=rtByName2.getRecordtypeid(),System__c='CRIS3-BC',Seq__c='02',Status__c='COMPLETED',Jeop_Hold_Code__c='',Originating_Date__c=system.today(),ERD_Actual__c=(system.today().adddays(-1)),ERD_Scheduled__c=(system.today().adddays(-1)),DD_Actual__c=(system.today().adddays(-1)),DD_Scheduled__c=(system.today().adddays(-1)),PTD_Actual__c=null,PTD_Scheduled__c=(system.today().adddays(-1)),PRD_Actual__c=null,PRD_Scheduled__c=null);   
    
    relatedOderList1.add(ro14);relatedOderList1.add(ro187);
        insert relatedOderList1;
    
     System.assertEquals(relatedOderList1.size(),2);  
     
     src.BatchPMOProcessing(sereqList);

         Test.stopTest();    
    }
  
  //...test method for MilestoneConditionUpdateFMO method..
  
    static testMethod void SRS_CRDB_ConditionCal_tracker8() {
    
        List<Service_Request__c> sereqList = new List<Service_Request__c>();
    List<Service_Request__c> sereqList1 = new List<Service_Request__c>();
    set<Service_Request__c> sereqSet = new set<Service_Request__c>();
    
    List<SRS_Service_Request_Related_Order__c> relatedOderList1 = new List<SRS_Service_Request_Related_Order__c>();
    List<Opportunity> OppList = new List<Opportunity>();
     Schema.DescribeSObjectResult dsr = Schema.SObjectType.SRS_Service_Request_Related_Order__c; 
          Map<String,Schema.RecordTypeInfo> rtMapByName1 = dsr.getRecordTypeInfosByName();
          Schema.RecordTypeInfo rtByName1 =  rtMapByName1.get('SIMS');
           Map<String,Schema.RecordTypeInfo> rtMapByName2 = dsr.getRecordTypeInfosByName();
          Schema.RecordTypeInfo rtByName2 =  rtMapByName2.get('CRIS3-BC');
        
        SRS_CRDB_ConditionCal src= new SRS_CRDB_ConditionCal();
        Test.startTest();
    Opportunity opp= new Opportunity(Name='OppTest', StageName='Originated',Project_Number__c='1234', CloseDate=system.today());    
    OppList.add(opp);
    insert OppList;
    
     System.assertEquals(opp.name,'OppTest');
     
      Service_Request__c s1 = new Service_Request__c(opportunity__c=opp.id,Condition_Color__c='green',service_request_status__c=' avxd',DAC_Scheduled__c=(system.today().adddays(-1)),DD_Scheduled__c=(system.today().adddays(-1)),PTD_Scheduled__c=(system.today().adddays(-1)),RTI_Scheduled__c=(system.today().adddays(-1)),DAC_Actual__c=(system.today().adddays(-1)),DD_Actual__c = (system.today().adddays(-1)),PTD_Actual__c=(system.today().adddays(-1)) ,RTI_Actual__c=(system.today().adddays(-1)));
    
    Service_Request__c s2  = new Service_Request__c(opportunity__c=opp.id,Condition_Color__c='red',service_request_status__c='asbc',DAC_Scheduled__c=(system.today().adddays(-1)),DD_Scheduled__c=(system.today().adddays(-1)),PTD_Scheduled__c=(system.today().adddays(-1)),RTI_Scheduled__c=(system.today().adddays(-1)),DAC_Actual__c=(system.today().adddays(-1)),DD_Actual__c = (system.today().adddays(-1)),PTD_Actual__c=(system.today().adddays(-1)) ,RTI_Actual__c=(system.today().adddays(-1)));    
     sereqList.add(s1);sereqList.add(s2);insert sereqList;
       
     src.MilestoneConditionUpdateFMO(sereqList);
     
            System.assertEquals(sereqList.size(),2);
            
         Test.stopTest();    
    }
    
    //...test method for PMOProcessing method..
    
    static testMethod void SRS_CRDB_ConditionCal_test3() {
    
        SRS_CRDB_ConditionCal src= new SRS_CRDB_ConditionCal();
        Test.startTest();

        SRS_Test_Data_Utility testData= new SRS_Test_Data_Utility();
        testData.SRS_ConditionCal_Data_Utility();
        
        
        // src.UpdateConToRedfrmSR(sereqSet);        
         List<OrderStatusUpdates_ComplexTypes.LineItem> lolistYellow = new List<OrderStatusUpdates_ComplexTypes.LineItem>();
            OrderStatusUpdates_ComplexTypes.LineItem L3 = new OrderStatusUpdates_ComplexTypes.LineItem();
            OrderStatusUpdates_ComplexTypes.DueDate ddyellow = new OrderStatusUpdates_ComplexTypes.DueDate();
            ddyellow.DDScheduleDate= date.valueof('2015-12-28');
            ddyellow.DDActualDate=null;
             L3.LegacyOrderID='19870';
            L3.IssueDate=String.valueof(system.today());
            L3.BillingSystem='SIMS';
            L3.CRDBJEOPCode='GHfk';
            L3.CRIS3HoldCode='sahs';
            L3.CRIS3OrderStatus='';
            L3.DDate=ddyellow;
            lolistyellow.add(L3);
            src.PMOProcessing(lolistyellow);
        Test.stopTest();
    
    
    }
    
    //...test method for BatchForFMOEnabledSRs and MilestoneConditionUpdateFMO methods..
        
    static testMethod void SRS_CRDB_ConditionCal_test4() {
    
     //......................................................
        SRS_CRDB_ConditionCal src= new SRS_CRDB_ConditionCal();
        Test.startTest();

        SRS_Test_Data_Utility testData= new SRS_Test_Data_Utility();
        testData.SRS_ConditionCal_Data_Utility();
        
        
          src.BatchForFMOEnabledSRs(testData.sereqListFMOMilestone);
          src.MilestoneConditionUpdateFMO(testData.sereqListFMOMilestone);
            
            Test.stopTest();
            
            
            
    }
    
    //...test method for BatchPMOProcessing and UpdateConToGreyfrmSR methods..
    
    static testMethod void SRS_CRDB_ConditionCal_test5() {
    
     //......................................................
      List<Service_Request__c> sereqList = new List<Service_Request__c>();
      Set<Service_Request__c> sereqSet = new Set<Service_Request__c>();
      SRS_CRDB_ConditionCal src= new SRS_CRDB_ConditionCal();
        Test.startTest();

        SRS_Test_Data_Utility testData= new SRS_Test_Data_Utility();
        testData.SRS_ConditionCal_Data_Utility();
        sereqList=testData.sereqList;
        system.debug('@@serlisdt'+sereqList);
        for(Service_Request__c sr :sereqList)
        sereqSet.add(sr);
        
          src.BatchPMOProcessing(testData.sereqListFMOMilestone); 
          src.UpdateConToGreyfrmSR(sereqSet);
            
            Test.stopTest();
            
            
            
    }
    
    
}