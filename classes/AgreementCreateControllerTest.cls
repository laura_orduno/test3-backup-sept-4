@isTest
public class AgreementCreateControllerTest {

    @isTest
    public static void createAgreementTest(){
        //test Agreement New
        Test.startTest();
        //Data Setup
        id accountId = crateAccount();
        id oppId = createOpportunity(accountId);
        id dealShowError = createDealSupport(oppId,'Approved','CCA',null);
        ApexPages.currentPage().getParameters().put('id', dealShowError);
        AgreementCreateController newAgreementError = new AgreementCreateController();
        newAgreementError.onLoad();
        
        id dealId = createDealSupport(oppId,AgreementCreateController.STATUS_CONTRACT_INPROGRESS,'CCA',null);
        System.debug(LoggingLevel.INFO, 'dealId ' + dealId);
        createRelatedListItems(dealId);
        ApexPages.currentPage().getParameters().put('id', dealId);
        ApexPages.currentPage().getParameters().put('rln','Rate_Plan_Item__c,Subsidiary_Account__c,Agreement_Contact__c');
        AgreementCreateController newAgreement = new AgreementCreateController();
        PageReference prRenew = newAgreement.onLoad();
        newAgreement.doCancel();
        //Agreement has alreaedy Created for Deal Support
        System.debug(LoggingLevel.INFO, 'Load Agreement for exsisting Agreement');
        newAgreement.onLoad();
        
        Apttus__APTS_Agreement__c  extAgr = [select id from  Apttus__APTS_Agreement__c where Offer_House_Demand__c=:dealId Limit 1];
        System.debug(LoggingLevel.INFO, 'extAgr ' + extAgr);
        //test Amendment
        id oppAmdId = createOpportunity(accountId);
        id dealAmdId = createDealSupport(oppAmdId,AgreementCreateController.STATUS_CONTRACT_INPROGRESS,'Wireless Amendment',extAgr.id);
        System.debug(LoggingLevel.INFO, 'dealAmdId ' + dealAmdId);
        ApexPages.currentPage().getParameters().put('id', dealAmdId);
        ApexPages.currentPage().getParameters().put('rln','Rate_Plan_Item__c,Subsidiary_Account__c,Agreement_Contact__c');
        AgreementCreateController agreementAmd = new AgreementCreateController();
       	agreementAmd.onLoad();
        Test.stopTest();
    }
    
     @isTest
    public static void paramTest(){
        //test Agreement New
        Test.startTest();
        Id accId =  crateAccount();
        ApexPages.currentPage().getParameters().put('id',null);
        AgreementCreateController newAgreementError = new AgreementCreateController();
        newAgreementError.onLoad();
        ApexPages.currentPage().getParameters().put('id',accId);
        newAgreementError.onLoad();
        Test.stopTest();
    }
      
    
    private static id crateAccount(){
        Account acc = new Account(name ='Test Account');
        insert acc;
        System.debug(LoggingLevel.INFO,'New Account' +acc);
        return acc.id;
    }
    
    private static id createOpportunity(id accId){
        Opportunity opp = new Opportunity();
        opp.Name = 'TEST OPP';
        opp.AccountId = accId;
        opp.StageName = 'NewStage';
        Opp.CloseDate = Date.newInstance(2015,12,25);
        insert opp;
        return opp.id;
    }
    
    private static id createDealSupport(id opporunityId,String status,String typeOfContract,id extAgr){
        map<string,schema.recordtypeinfo> recordTypeMap=offer_house_demand__c.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        Offer_House_Demand__c dealSupport = new Offer_House_Demand__c();
        dealSupport.recordtypeid=recordTypeMap.get('WLN Deal Support').getrecordtypeid();
        dealSupport.OPPORTUNITY__c = opporunityId;
        dealSupport.TERM_OF_CONTRACT__c = 24;
        dealSupport.Type__c ='Current';
        dealSupport.Status__c= status;
        dealSupport.TYPE_OF_CONTRACT__c= typeOfContract;
        dealSupport.Existing_Agreement__c = extAgr;
        dealSupport.signature_required__c='Yes';
        insert dealSupport; 
        return dealSupport.id;
    }
    
      
    private static void createRelatedListItems(id dealSupportId){
        List<sObject> itemsCreate = new List<sObject>();
        List<String> relatedList = new LIst<String>{'Rate_Plan_Item__c', 'Agreement_Contact__c','Agreement_Dealer__c','Subsidiary_Account__c'}; 
            for(String objName :relatedList){
               System.debug(LoggingLevel.INFO,'Creating Objs for ' + objName);
                Type t = type.forName(objName);
                for(Integer i=0;i<2;i++){
                	sObject obj = (sObject)t.newInstance();
                	obj.put('Deal_Support__c',dealSupportId);
                    itemsCreate.add(obj);
                }    
            }
    	insert itemsCreate;
    }
        
}