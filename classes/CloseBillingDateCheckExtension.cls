public with sharing class CloseBillingDateCheckExtension {
	
	public String errorLevel {get; set;}
	public String messageName {get; set;}
	private final Opportunity opp;
	private list<Opp_Product_Item__c> oppProductItems;
	private ApexPages.StandardController stdController;
	
	public void showMessage() {
		if(errorLevel == 'WARNING') {
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, messageName));
  		}
	}
	
	public CloseBillingDateCheckExtension(ApexPages.StandardController stdController) {
        this.opp = (Opportunity)stdController.getRecord();
        this.stdController = stdController;
    }
    
	public boolean getRecordPermission(){
    	UserRecordAccess recordAccess = [SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE UserId = :UserInfo.getUserId() AND RecordId = :this.opp.id];
		if(recordAccess != null && recordAccess.HasEditAccess == True) return true;
		else return false;
	}
    
    public  list<Opp_Product_Item__c> getOppProductItems() {
        oppProductItems = new list<Opp_Product_Item__c>();

        for (Opp_Product_Item__c a: [Select id,  name, Suggested_Billing_Start_Date__c, Reason_for_Billing_Start_Date_Deviation__c, Product_Family__c, Billing_Start_Date__c From Opp_Product_Item__c where Opportunity__c = :opp.id]){
            oppProductItems.add(a);
        }
        return oppProductItems;              
    }
	
	public PageReference saveOpp() {
		
		try {
			this.opp.LockCloseDate__c = false;
			update this.opp;
			this.stdController.save();
			this.opp.LockCloseDate__c = true;
			update this.opp;
		}
		catch (Exception ex) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Fatal, ex.getMessage(), ex.getStackTraceString()));
			System.debug('------------ DC ------------');
			System.debug(ex);
			return null;
		}
		
		PageReference ref = Page.CloseBillingDateCheck;
        ref.getParameters().put('ID', Apexpages.currentPage().getParameters().get('id'));
        ref.getParameters().put('returl', Apexpages.currentPage().getParameters().get('retURL'));
		ref.setRedirect(true);
		return ref;	
	}
	
	public PageReference inlineSave() {
		try {
			update oppProductItems;
			//CloseBillingDateCheckHelper.updateBillingStartDates(new List<Id> {opp.Id});
		}
		catch (Exception ex) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Fatal, ex.getMessage(), ex.getStackTraceString()));
			System.debug('------------ DC ------------');
			System.debug(ex);
		}
		
		this.opp.LockCloseDate__c = false;
		update this.opp;
		this.stdController.save();
		this.opp.LockCloseDate__c = true;
		update this.opp;
		
		PageReference ref = Page.CloseBillingDateCheck;
        ref.getParameters().put('ID', Apexpages.currentPage().getParameters().get('id'));
        ref.getParameters().put('returl', Apexpages.currentPage().getParameters().get('retURL'));
		ref.setRedirect(true);
		return ref;	
	}
	
	public PageReference saveDates() {
		try {
			update oppProductItems;
			//CloseBillingDateCheckHelper.updateBillingStartDates(new List<Id> {opp.Id});

			this.opp.LockCloseDate__c = false;
			update this.opp;
			this.stdController.save();
			this.opp.LockCloseDate__c = true;
			update this.opp;
		}
		catch (Exception ex) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Fatal, ex.getMessage(), ex.getStackTraceString()));
			System.debug('------------ DC ------------');
			System.debug(ex);
			return null;
		}
				
		if(Apexpages.currentPage().getParameters().get('retURL')!=null){
			PageReference ref = new PageReference(Apexpages.currentPage().getParameters().get('retURL'));
			ref.setRedirect(true);
			return ref;
		}
		else return null;
	}
}