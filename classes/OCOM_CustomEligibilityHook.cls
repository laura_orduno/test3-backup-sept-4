/**
OCOM_CustomEligibilityHook used by HookInterface

@author

IBM     Aditya
Vlocity Adharsh
Vlocity Lakshimi

postCartsItems.PostInvoke
- Updating Characteristic Market Value from Service Address in Order Header
(Looking up Country Code in Custom Setting Country_Province_Code__c)
- -


getCarts.PreInvoke

getCarts.PostInvoke

deleteCartsItems.PreInvoke

deleteCartsItems.PostInvo



*/

global with sharing class OCOM_CustomEligibilityHook implements vlocity_cmt.VlocityOpenInterface {
    private String  ALL_PBE_BASE_SQL = 'Select Id, Pricebook2Id, Product2Id, ProductCode,Product2.Description, Product2.OCOM_Promotion_Applies_To__c,Product2.ProductSpecification__r.Name,Product2.RelationshipType__c,UnitPrice,Name,Product2.Name, Product2.vlocity_cmt__IsConfigurable__c,Product2.vlocity_cmt__Type__c,Product2.vlocity_cmt__SubType__c,vlocity_cmt__RecurringPrice__c,IsActive,Product2.OCOM_Promotion_Applies_To__c from PricebookEntry where  (IsActive = true) AND (Product2.recordTypeId = null OR Product2.recordType.DeveloperName = \'Product\')';
    private String  ALL_PBE_BASE_SQL_WITH_JSON = 'Select Id, Product2.ProductSpecification__r.Name, Product2.OCOM_Promotion_Applies_To__c,Product2.RelationshipType__c,Pricebook2Id, Product2Id, ProductCode,Product2.Description, UnitPrice,Name,Product2.Name, Product2.vlocity_cmt__IsConfigurable__c, Product2.vlocity_cmt__JSONAttribute__c, Product2.vlocity_cmt__Type__c,Product2.vlocity_cmt__SubType__c,vlocity_cmt__RecurringPrice__c,IsActive,Product2.OCOM_Promotion_Applies_To__c from PricebookEntry where  (IsActive = true) AND (Product2.recordTypeId = null OR Product2.recordType.DeveloperName = \'Product\')';
    public static Map<String, List<OCOM_CustomHookUtil.CalcMatrixdataWrapper>> cmrExtIDtoWrapperMap =  new Map<String, List<OCOM_CustomHookUtil.CalcMatrixdataWrapper>>();
    public static Map<String, List<String>> prodToRelatedProdMap = new Map<String, List<String>>();
    public static Map<String, List<String>> relatedProdToProdMap = new Map<String, List<String>>();
    public static integer nodeCounter = 0;
    public static Map<String, integer> grandChildNodeCounter = new Map<String, integer>();
    public  Map<String, Object> indexToOrderIDMap = new Map<String, Object>();
    public static Map<String, String> chargeableItemsMap = new Map<String, String>();
    public static Map<String, String> chargeableItems_True_Map = new Map<String, String>();
    public boolean updateTopOfferCounter = false;
    integer chargeableCount = 0;
    Map<Integer, String>oiToChargeableCountMap = new Map<Integer, String>();
    public Boolean isSuperUser = false;
    public static Map<String, Decimal> topOfferIncFeatureMap = new Map<String, Decimal>();
    public Map<id, priceBookEntry> availablePriceBookEntryMap;
    public set<String> excludeWirelssProductSet = new set<String> {'telus wireless product', 'wireless rate plan product component', 'wireless device product component', 'sim card product component'};
//Start Added By Aditya Jamwal (IBM)
    public static final String rateSubBandCharacteristicName = 'Rate Sub-Band';
    public static final String rateBandCharacteristicName = 'Rate Band';
    public static final String marketCharacteristicName = 'Market';
    public static final String forborneCharacteristicName = 'Forborne';
    public static final String lineTypeCharacteristicName = 'Line Type';
    public String methodNameglobal ;

//Arpit : 10-Jan-2017 : Sprint 12 : Checkout readiness
    public static Set<Id> deletedOliIDSet = new Set<Id>();

    private final String CLASS_NAME = 'OCOM_CustomEligibilityHook';



    global Boolean invokeMethod(String methodName, Map<String, Object> input, Map<String, Object> output, Map<String, Object> options)  {

        methodNameglobal = methodName;

        //##################################################################
        //##################################################################
        // POST CARTS ITEMS
        //##################################################################
        //##################################################################

        if ( methodName.equalsIgnoreCase('postCartsItems.PreInvoke')) {

            system.debug('PostcartItems.PreInvoke_____');
            postCartsItems_PreInvoke(methodName, input, output, options);

        }

        if ( methodName.equalsIgnoreCase('postCartsItems.PostInvoke')) {
            //###################################################################
            //VlocityLogger.e(CLASS_NAME+'-'+ methodName + ': ENTER ALL');
            //###################################################################


            /*****************************Aditya's + Dissconnect Service ***************************************/
            postCartsItems_PostInvoke(methodName, input, output, options);
            /**** Adharsh: Commenting Out Auto Add TelUs Connetivity code as this is handled by OOB Rules ********/
            // if( (Test.isRunningTest() || OCOM_AddTelusConnectivityHelper.runPost)){
            /*****************************Adharsh's  ***************************************/
            postCartsItems_GetCartsItems_PostInvoke(methodName, input, output, options);
            //}
            //###################################################################
            //VlocityLogger.x(CLASS_NAME+'-'+ methodName+ ': EXIT ALL');
            //###################################################################

        }

        if ( methodName.equalsIgnoreCase('putCartsItems.PreInvoke') ) {
            
            vlocity_cmt.FlowStaticMap.flowMap.put('putCartsItems.PreInvoke', true);
        }

        if ( methodName.equalsIgnoreCase('putCartsItems.PostInvoke') ) {
            //###################################################################
            //VlocityLogger.e(CLASS_NAME+'-'+ methodName + ': ENTER ALL');
            //###################################################################

            //System.debug('putcartItems___Input ' + input);
            //System.debug('putCartItems___output' + output);

            /*****************************Add Office Internet***************************************/
            /**** Adharsh: Commenting Out Auto Add TelUs Connetivity code as this is handled by OOB Rules ********/
            //putCartsItems_PostInvoke_AddTELUSConnectivity(methodName, input, output,options);

            //###################################################################
            //VlocityLogger.e(CLASS_NAME+'-'+ methodName + ': Exit ALL');
            //###################################################################

        }

        if ( methodName.equalsIgnoreCase('cloneItems.PostInvoke') ) {
            //###################################################################
            //VlocityLogger.e(CLASS_NAME+'-'+ methodName + ': ENTER ALL');
            //###################################################################

            System.debug('cloneItems___Input ' + input);
            postCartsItems_GetCartsItems_PostInvoke(methodName, input, output, options);
           

        }

        //##################################################################
        //##################################################################
        // GET CARTS
        //##################################################################
        //##################################################################


        if (methodName.equalsIgnoreCase('getCarts.PreInvoke')) {
            //###################################################################
            // VlocityLogger.e(CLASS_NAME+'-'+ methodName);
            //##################################################################


            input.put('price', false);
            input.put('validate', false);

            //##################################################################
            //VlocityLogger.x(CLASS_NAME+'-'+ methodName);
            //##################################################################

        }
        /**** Adharsh: Commenting Out Auto Add TelUs Connetivity code as this is handled by OOB Rules ********/
        // if( (Test.isRunningTest() || OCOM_AddTelusConnectivityHelper.runPost)){

        if ( methodName.equalsIgnoreCase('getCartsItems.postInvoke')) {

            /*****************************Adharsh's  ***************************************/
            postCartsItems_GetCartsItems_PostInvoke(methodName, input, output, options);
        }
        // }


        if (methodName.equalsIgnoreCase('deleteCartsItems.PreInvoke')) {


            //##################################################################
            //VlocityLogger.e(CLASS_NAME+'-'+ methodName);
            //##################################################################

            String deletedItemId = '';
            String OrderId = '';
            String parentId = '';
            String rootItemId = '';
            try {
                System.debug('deleteCartsItems____ >>> keys ' + input);
                /******* Adharsh:: Commenting out Auto Add TeluConnectivity as this is been handled by OOB Rules ************/
                //OCOM_AddTelusConnectivityHelper.deteleTelusCon = true;
                //OCOM_AddTelusConnectivityHelper.deleteUndelyingRec((String) input.get('id'), (String) input.get('cartId'));
                //Start Added By Aditya Jamwal (IBM) : Automatic WFM booking cancellation, if a deleted OLI has the booking against it.
                System.debug('Testing!' + input);
                String Id = (String) input.get('id');

                if (String.isNotBlank(Id)) {
                    OCOM_OrderProductBeforeTrigger_Helper helperObj = new OCOM_OrderProductBeforeTrigger_Helper();
                    Set<String> IDset = new Set<String>();
                    IDset.add(Id);

                    //Arpit : 11-Jan-2017 : Sprint 12  - AddedDeleted case for check out readiness
                    for (String strID : IDset) {
                        deletedOliIDSet.add((ID)strID);
                    }

                    System.debug('to be deleted oliIDs----->' + deletedOliIDSet );
                    helperObj.cancelWorkOrderofDeletedOLIs(IDset);
                }

                //End Added By Aditya Jamwal (IBM)

                if (input.containsKey('id'))
                    deletedItemId = (String) input.get('id');
                if (input.containsKey('cartId'))
                    OrderId =  (String) input.get('cartId');
                if (input.containsKey('items')) {
                    Object  items = (Object) input.get('items');
                    if (items instanceof List<Object> ) {
                        Object parentNode = ((List<Object>)items)[0];
                        Map<String, Object> parentRecords  = (Map<String, Object>)((Map<String, Object>)parentNode).get('parentRecord');
                        Map<String, Object> records  = (Map<String, Object>)((List<Object>)(parentRecords).get('records'))[0];

                        Map<String, object> rootItemMap = (Map<String, Object>)records.get('vlocity_cmt__RootItemId__c');
                        rootItemId = (String)rootItemMap.get('value');
                        system.debug('>>>> rootItem ' + rootItemId );

                        Map<String, Object>parentRecInfo = (Map<String, Object>)records.get('Id');
                        parentId = (String)parentRecInfo.get('value');
                        // parentId = id.valueOf(parentIdString);
                        system.debug('>>>>>>> parentId >>' + parentId);
                    }
                }

                if (parentId != '' && deletedItemId != '')
                    OCOM_CustomHookUtil.decrementRunningCount(rootItemId, deletedItemId);
            } catch (exception e) {
                throw e;
                System.debug('Exception iN delete Cart Items Preinvoke:: ' + e.getStackTraceString() );
                System.debug('Exception iN delete Cart Items Preinvoke:: ' + e.getMessage() );
            }
            //##################################################################
            //VlocityLogger.x(CLASS_NAME+'-'+ methodName);
            //##################################################################
        }


        if (methodName.equalsIgnoreCase('deleteCartsItems.PostInvoke')) {

            //##################################################################
            //VlocityLogger.e(CLASS_NAME+'-'+ methodName);
            //##################################################################

            String chargeableProductId = '';
            String topOfferId = '';
            List<OrderItem> orderItemstoupdate = new List<OrderItem>();
            Map<String, OrderItem> childLineItemsMap = new Map<String, OrderItem>();
            vlocity_cmt.JSONResult results1 = (vlocity_cmt.JSONResult)output.get(vlocity_cmt.JSONResult.JSON_KEY_RESULT);
            System.debug('%%%%%results1_Test1' + results1);
            List<vlocity_cmt.JSONRecord>pbeList  =  (List<vlocity_cmt.JSONRecord>)results1.records;
            System.debug('%%%%%pbeList_test1' + pbeList);

        }




        if (methodName.equalsIgnoreCase('getCartsProducts.postInvoke')) {


            //##################################################################
            //VlocityLogger.e(CLASS_NAME+'-'+ methodName);
            //###################################################################

            try {

                if (!input.Containskey('parentId')) {
                    Order o = [SELECT Id, RateBand__c, Rate_Sub_Band__c, Market__c, Forbone__c, Address_PDS_Output__c, Forbearance__c FROM Order where Id = :String.ValueOf(input.get('cartId')) LIMIT 1];
                    String sRateband = o.RateBand__c;
                    String sRateSubband = o.Rate_Sub_Band__c;
                    Boolean bForborn = o.Forbone__c;
                    String sMarket = '';
                    if (String.isNotBlank(o.Market__c)) {
                        Country_Province_Codes__c FullProvValue = Country_Province_Codes__c.getValues(o.Market__c);
                        sMarket = o.Market__c;
                        if (null != FullProvValue) { sMarket  = FullProvValue.Description__c;}
                    }
                    String sForborn = '';
                    /*if(o.Forbone__c  == true)
                    {
                        sForborn = 'Yes';}
                    else{
                        sForborn = 'No';
                    }*/
                    if (null != o.Forbearance__c)
                        sForborn = o.Forbearance__c;
                    String orderCharValue = '';
                    if (o.Address_PDS_Output__c != null && o.Address_PDS_Output__c != '') {
                        orderCharValue = o.Address_PDS_Output__c;
                    }

                    System.debug('output**' + output);



                    vlocity_cmt.JSONResult results2 = (vlocity_cmt.JSONResult)output.get(vlocity_cmt.JSONResult.JSON_KEY_RESULT);
                    //System.debug('******stringtest'+String.valueOf(results2));
                    List<vlocity_cmt.JSONRecord> pbeList  =  (List<vlocity_cmt.JSONRecord>)results2.records;


                    List<String> entryIds = new List<String>();

                    String CalcMatrixName = System.label.OCOM_Text_Price_Matrix;
                    System.debug('cmrExtIDtoWrapperMap***' + cmrExtIDtoWrapperMap);
                    if (cmrExtIDtoWrapperMap.Size() == 0 || cmrExtIDtoWrapperMap.isEmpty() ) {

                        cmrExtIDtoWrapperMap = OCOM_CustomHookUtil.getCalcMatrixdata(CalcMatrixName);
                    }



                    for (vlocity_cmt.JSONRecord pbe : pbeList ) {
                        //System.debug('FieldsKeyset*********' + JSON.serialize(product.fields.keyset()));
                        //Map<String,Object> pbeRecurringPriceMap = new Map<String,Object>();

                        if (pbe.fields.containskey('Product2')) {
                            product2 produt2Fields = (product2)pbe.fields.get('Product2');

                            if (cmrExtIDtoWrapperMap.containsKey(String.valueOf(produt2Fields.get('orderMgmtId__c'))) ) {

                                for (OCOM_CustomHookUtil.CalcMatrixdataWrapper cmWrapper : cmrExtIDtoWrapperMap.get(String.valueOf(produt2Fields.get('orderMgmtId__c')))) {
                                    if (cmWrapper.charactersticName.equalsIgnoreCase('Forborne') && cmWrapper.charactersticValue.equalsIgnoreCase(sForborn)) {
                                        ((Map<String, object>)pbe.fields.get('vlocity_cmt__RecurringPrice__c')).put('value', cmWrapper.outputMRC);
                                    } else if (cmWrapper.charactersticName.equalsIgnoreCase('Rate Band') && cmWrapper.charactersticValue.equalsIgnoreCase(sRateband)) {
                                        // system.debug('Setting the Market pricing222____' + cmWrapper.productName + cmWrapper.outputMRC);
                                        Map<String, Object> pbeRecurringPriceMap  = (Map<String, Object>)pbe.fields.get('vlocity_cmt__RecurringPrice__c');
                                        System.debug('pbeRecurringPriceMap__' + pbeRecurringPriceMap);
                                        pbeRecurringPriceMap.put('value', cmWrapper.outputMRC);
                                    } else if (cmWrapper.charactersticName.equalsIgnoreCase('Rate Sub-Band') && cmWrapper.charactersticValue.equalsIgnoreCase(sRateSubband)) {

                                        ((Map<String, object>)pbe.fields.get('vlocity_cmt__RecurringPrice__c')).put('value', cmWrapper.outputMRC);
                                    } else if (cmWrapper.charactersticName.equalsIgnoreCase('Market') && cmWrapper.charactersticValue.equalsIgnoreCase(sMarket)) {

                                        ((Map<String, object>)pbe.fields.get('vlocity_cmt__RecurringPrice__c')).put('value', cmWrapper.outputMRC);
                                    }
                                    if ( cmWrapper.charactersticValue.equalsIgnoreCase(orderCharValue)) {
                                        ((Map<String, object>)pbe.fields.get('vlocity_cmt__RecurringPrice__c')).put('value', cmWrapper.outputMRC);
                                        system.debug('Setting the Market pricing____' + cmWrapper.productName + cmWrapper.outputMRC);

                                    }

                                }
                            }

                        }
                    }


                }
            } catch (Exception e) {
                System.debug(LoggingLevel.ERROR, 'Exception is ' + e);
                System.debug(LoggingLevel.ERROR, 'Exception stack trace ' + e.getStackTraceString());

                //##################################################################
                //VlocityLogger.x(CLASS_NAME+'-'+ methodName);
                //##################################################################

                throw e;
                return false;
            }


            //##################################################################
            //VlocityLogger.x(CLASS_NAME+'-'+ methodName);
            //##################################################################

            return true;
        }

        return false;
    }// Hook Method Ends

    public Set<String> getAddedOLIsPBEId(Map<String, object> inputMap) {
        Set<String> PBEIds = new Set<String>();
        if (null != inputMap.get('items') && inputMap.get('items') instanceof List<Object>) {
            List<Object>  items = (List<Object>) inputMap.get('items');
            for (Object itm : items) {
                if (itm instanceof Map<String, Object>) {
                    PBEIds.add((String)((Map<String, Object>)itm).get('itemId'));
                }
            }
        }
        return PBEIds;
    }

    public Map<String, Object> getPutCartItemsInputInfo(Map<String, Object> inputMap) {
        Map<String, Object> updatedItemInfoMap = new Map<String, Object>();

        if (inputMap.containsKey('items')) {

            Object itemsNode = inputMap.get('items');
            if (itemsNode instanceof Map<String, Object>) {
                Map<String, Object> itemsNodeMap = (Map<String, Object>)itemsNode;
                if (itemsNodeMap.containsKey('records')) {
                    Object recordsInfo = itemsNodeMap.get('records');
                    if (recordsInfo instanceof List<Object>)  {
                        List<Object> recordsInfoList = (List<Object>) recordsInfo;
                        if (recordsInfoList.Size() > 0 && !recordsInfoList.isEmpty()) {
                            Map<String, Object> recordsMap  = (Map<String, Object>)recordsInfoList[0];

                            if (recordsMap != null && recordsMap.containsKey('Id') && (recordsMap.get('Id') instanceof Map<String, Object>)  ) {
                                Map<String, Object>updatedRecInfo = (Map<String, Object>)recordsMap.get('Id');
                                String updatedOliId = (String) updatedRecInfo.get('value');
                                updatedItemInfoMap.put('updatedOfferID', updatedOliId);
                                system.debug('****** updatedOliId *******' + updatedOliId);


                            }
                            if (recordsMap != null && recordsMap.containsKey('vlocity_cmt__LineNumber__c') &&  (recordsMap.get('vlocity_cmt__LineNumber__c') instanceof Map<String, Object>) ) {
                                Map<String, Object> updatedRecordLineNumMap = (Map<String, Object>)recordsMap.get('vlocity_cmt__LineNumber__c');
                                String  updatedRecordLineNum = (String) updatedRecordLineNumMap.get('value');
                                updatedItemInfoMap.put('updatedRecordLineNum', updatedRecordLineNum);
                            }
                            if (recordsMap != null && recordsMap.containsKey('Product2') && (recordsMap.get('Product2') instanceof Map<String, Object>) ) {

                                Map<String, Object> updatedRecordProdInfo = (Map<String, Object>)recordsMap.get('Product2');
                                //system.debug('>>>>>>>Product2 info____' + JSON.Serialize(updatedRecordProdInfo.keySet()));
                                String  updatedRecordProdName = (String) updatedRecordProdInfo.get('Name');
                                String  updatedRecordProdExtId = (String) updatedRecordProdInfo.get('orderMgmtId__c');
                                updatedItemInfoMap.put('updatedRecordProdName', updatedRecordProdName);
                                updatedItemInfoMap.put('updatedRecordProdExtId', updatedRecordProdExtId);
                            }
                            // recordsMap.clear();
                        }

                    }
                }
            }
        }
        System.debug('updatedItemInfoMap' + updatedItemInfoMap);
        return updatedItemInfoMap;


    }


    /**
    ######################################
       postCartsItems.PreInvoke
    ######################################
    */
    private Boolean postCartsItems_PreInvoke(String methodName, Map<String, Object> input, Map<String, Object> output, Map<String, Object> options)  {
        //##################################################################
        //VlocityLogger.e(CLASS_NAME+'-'+ methodName);
        //##################################################################

        vlocity_cmt.FlowStaticMap.FlowMap.put('preInvoke.addedItem', JSON.serialize(input));
        Set<String> PBEIds = getAddedOLIsPBEId(input);
        vlocity_cmt.FlowStaticMap.FlowMap.put('AddedOLIsPBEIdSet', PBEIds);

        //##################################################################
        //VlocityLogger.x(CLASS_NAME+'-'+ methodName);
        //##################################################################
        return true;
    }

    /**
    ######################################
       postCartsItems.PostInvoke: Aditya's and Disconnect Service
    ######################################
    */
    private Boolean postCartsItems_PostInvoke(String methodName, Map<String, Object> input, Map<String, Object> output, Map<String, Object> options)  {
        system.debug('!!@@inCustomHook ');

        Set<String> PBEIds =  new Set<String>();
        PBEIds = (Set<String>) vlocity_cmt.FlowStaticMap.FlowMap.get('AddedOLIsPBEIdSet');
        system.debug('!!@@Ad$$ ' + PBEIds);


        //############## Query Order Header

        //Start Added By Aditya Jamwal (IBM) : To Update  Market,Rate band, Rate Sub Band and Forborne attributes in OLI JSON.

        order ordr;
        List<OrderItem> orderItemList;
        List<OrderItem> updateOrderItemList = new List<OrderItem>();

        String orderId = '';
        if (input.containsKey('cartId')) {
            orderId = (String) input.get('cartId');
            return OCOM_UpdateOliAttributes.setOliJSONAttribtributes(orderId);
        }
        return true;

    }



    /**
    ######################################
       postCartsItemsAndGetCartsItems.PostInvoke: Adharsh's logic
    ######################################
    */

    private Boolean postCartsItems_GetCartsItems_PostInvoke (String methodName, Map<String, Object> input, Map<String, Object> output, Map<String, Object> options) {

        //##################################################################
        //VlocityLogger.e(CLASS_NAME+'-'+ methodName +': ADHARSH');
        //##################################################################

        try {

            System.debug('inputMap______' + input);
            System.debug('outputMap______' + output);
            String OrderId = String.ValueOf(input.get('cartId'));
            Map<String, Object> inputItemInfoMap = new Map<String, Object>();
            //Add Telus Connectivity
            String OrderItemId = String.ValueOf(input.get('id'));

            OCOM_PostCartItems postCartItemsObj = new OCOM_PostCartItems();
            postCartItemsObj.genericMethod(input, output, methodName);


        } catch (exception e) {
            System.debug(LoggingLevel.ERROR, 'Exception is ' + e);
            System.debug(LoggingLevel.ERROR, 'Exception stack trace ' + e.getStackTraceString());

            //##################################################################
            //VlocityLogger.x(CLASS_NAME+'-'+ methodName+': ADHARSH' + 'Exception='+e +' : ');
            //##################################################################

            throw e;
            return false;
        }


        //##################################################################
        //VlocityLogger.x(CLASS_NAME+'-'+ methodName+': ADHARSH');
        //##################################################################

        return true;
    }



}