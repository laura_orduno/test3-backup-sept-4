global class smb_NewAccountController {

    public Account account {get;set;}    
    public Contact contact {get;set;}
    public Boolean PACMandatory {get;set;} //Added by Aditya Jamwal for showing mandatory sign on PAC Address field. 
    public Boolean validateAddress {get;set;}
    
    public Boolean duplicateDetected {get;set;}
    
    public Id newAccountId {get;set;}
    
    private transient Map<String, Schema.RecordTypeInfo> rtMapByName;
    private transient Map<Id, Schema.RecordTypeInfo> rtMapById;

    private Map<String, Id> rtIdMap;
    private Map<Id, String> rtNameMap;
    private Map<String, String> recordTypeNameAdjustment;
    private List<String> selectableRecordTypes;
    
    public String currentRecordTypeSelected {get; set;}
    
    // field availability
    public Boolean contactFieldsAvailable {get; set;}
    public Boolean prospectAccountFieldsAvailable {get; set;}
    
    // used for custom exception messages
    public String msgTitle {get; set;}
    public String msgSummary {get; set;}
    public Boolean msgRendered {get; set;}
    public String msgDetail {get; set;}
    public Integer msgStrength {get; set;}
    public String msgSeverity {get; set;}
    
    //CTI Parameters
    public String CallerId {get; set;}
    public String CallingAbout {get; set;}
    public String IVRPromptSelection {get; set;}
    public String CallBack {get; set;}
    public String ConnectionId {get; set;}
    public String CallerCAN {get; set;}
    
    // Sandip - PAC - input to PAC componenet added and selected address will be stored in this object only
    public AddressData PAData  { get; set; }
    
    public string ctiParamString {
        get {
            return smb_CtiUtility.getCtiParameterString(ApexPages.currentPage().getUrl()); 
        }
    }
    
    public String searchTabId {
        get {
            if (!ApexPages.currentPage().getParameters().containsKey('stid'))
                return '';
            return ApexPages.currentPage().getParameters().get('stid');
        }
    }
    
    public Boolean canadianAddress {
        get {
            return account.BillingCountry == 'CAN';
        }
    }
    
    public smb_NewAccountController() {
        rtMapByName = Schema.SObjectType.Account.getRecordTypeInfosByName();
        rtMapById = Schema.SObjectType.Account.getRecordTypeInfosById();
        rtIdMap = new Map<String, Id>();
        rtNameMap = new Map<Id, String>();
        
        selectableRecordTypes = new List<String>{'New Account', 'New Prospect Account'};
        recordTypeNameAdjustment = new Map<String, String>{'New Non-Validated Account' => 'New Account', 'New Validated Account' => 'New Prospect Account'};
        // only support "New RCID" for the near future 
//        recordTypeNameAdjustment = new Map<String, String>{'New RCID' => 'New Prospect Account'};

        // init field available flags       
        contactFieldsAvailable = true;
        // for account of record type "New Account" ( known as "New Prospect Account" in recordTypeNameAdjustment map since names are originally reversed)
        // 
        prospectAccountFieldsAvailable = false;
        
        for(String recordTypeName : rtMapByName.keyset()) {
            Schema.RecordTypeInfo rt = rtMapByName.get(recordTypeName);

            rtIdMap.put(recordTypeName, rt.getRecordTypeId()); 
            rtNameMap.put(rt.getRecordTypeId(), recordTypeName); 
        }

        this.account = new Account();
        this.contact = new Contact();
        applyDefaults();
        populateDefaultsFromCTI();
        
        //Sandip - PAC - Instantiate the object and make city as required field
        PAData = new AddressData();
// ******        PAData.isCityRequired = true;
    }
    
    private void populateDefaultsFromCTI() {
        
        Map<string, string> urlParameters = smb_CtiUtility.parseUrl(ApexPages.currentPage().getUrl());
        
        CallingAbout = urlParameters.get('TCCS_UTN');
        CallerId = urlParameters.get('TCCS_ANI');
        IVRPromptSelection = urlParameters.get('RSV4NL_LAST_IVR_FUNC');
        CallBack = 'No';
        CallerCAN = urlParameters.get('RSV4NL_CUST_ACCOUNTNO');
        ConnectionId = urlParameters.get('ConnectionID');

        
    }
    
    private void applyDefaults() {
        this.account.BillingCountry = 'CAN';
        this.contact.WBS_Language_Preference__c = 'English';
        this.validateAddress = false;
        
        clearExceptionArea();
        displayAccountsMappedToAccountId = new Map<Id, displayAccount>();
        displaySearchResults = false;
    }
    
    public void updatePage() {
        if (!canadianAddress) {
            this.validateAddress = false;
        }
    }
    
    public PageReference cancel() {
        Pagereference pr = null;
        if (ApexPages.currentPage().getParameters().containsKey('retUrl')) {
            pr = new Pagereference(ApexPages.currentPage().getParameters().get('retUrl'));
            pr.setRedirect(true);
        }
        return pr;
    }
    
    public void refresh() { 
        return;
    }
    
    public PageReference saveOverride() {
        mapPACData(); // map selected address from PAC componenet before save
        //Added By Sandip 03/12/2015
        if(currentRecordTypeSelected == 'New Validated Account'){
            if(account.BillingStreet == '' || account.BillingCity == '' || account.BillingState == ' '){
                throwCustomExceptionMsg('Error -','You must enter value, within the address field', true, '', 3, 'ERROR');     
                return null;
            }
        } 
        clearExceptionArea();
        newAccountId = null;
        duplicateDetected = false;
        account.Language_Preference__c = contact.WBS_Language_Preference__c;
        
        SavePoint sp = Database.setSavepoint();
        
        try {
            account = account.Clone();
            contact = contact.Clone();
            
            /*if(string.isNotBlank(account.BillingPostalCode)&& string.isblank(account.BillingStreet))
            {
                throwCustomExceptionMsg('Address InComplete', 'Please provide complete address details.', true, null, 3, 'ERROR');
            
                return null; 
            }*/
            
            CRMfusionDBR101.DB_WebServices.bypassBlock();
            
            insert account;  // If it does not throw DML error from triggers, it simply means all triggers ran successfully, so it goes to next line
            
            String recordTypeName = '';
            if(rtNameMap.containsKey(account.recordTypeId)){
                recordTypeName = rtNameMap.get(account.recordTypeId);
            }
            
            Account acctRecordTypeInfo = [SELECT id, recordtype.developerName, Error_Message__c  FROM Account WHERE id = :account.id];
            
System.debug('acctRecordTypeInfo.recordtype.developerName: ' + acctRecordTypeInfo.recordtype.developerName);
            
            if(acctRecordTypeInfo.recordtype.developerName != 'New_Account'){
                smb_CreateCustomerTriggerCalloutHelper.performCustomerServiceCall(new list<id>{account.id});   
                // set Error_Message__c to in progress so that the conversion can be tracked
                acctRecordTypeInfo.Error_Message__c = 'convert to rcid in progress';                              
                update acctRecordTypeInfo;       
                         
/*****                    
                list<CreateCustomerProfileResponse> result = smb_CreateCustomerTriggerCalloutHelper.performCustomerServiceCall(new list<id>{account.id});   
                                
                System.debug('convertToRCID: ' + result);   
                
                String errorMessage = '';
                for(CreateCustomerProfileResponse response : result){
                    errorMessage = response.errorMessage; 
                    
                    break;
                }
                
                System.debug('saveOverride, performCustomerServiceCall: ' + errorMessage);                             
                    
                if(String.isBlank(errorMessage)){
                    // if no error, set to in progress
                    acctRecordTypeInfo .Error_Message__c = '';      
                                
                    update acctRecordTypeInfo;                
                                     
                }  
*/                              
            }                
            
            contact.accountId = account.id;
            
            if(contactFieldsAvailable){
                insert contact;            
            }
            return new pageReference( '/' + account.id );

        }
        catch (QueryException ex) {
            contact.AccountId = null;

            String exceptionString = String.valueOf(ex);
            
            throwCustomExceptionMsg('Failed to Save Account', 'The account could not be created because record type developer name could not be retrieved for account.', true, ex.getMessage(), 3, 'ERROR');
            
            return null;
        }   
        catch (DMLException ex) {
            Database.rollback(sp);
            contact.AccountId = null;

            String exceptionString = String.valueOf(ex);
            
            if (exceptionString.contains('DupeBlocker: Duplicate found')||exceptionString.contains('DupeBlocker Duplicate Prevention')) {

                duplicateDetected = true;
                throwCustomExceptionMsg('Duplicate Account Found', 'The account could not be created because a duplicate was found.', true, ex.getMessage(), 3, 'ERROR');

            }
            
            else {
                throwCustomExceptionMsg('Failed to Save Account', 'The account could not be created because of the following problems.', true, ex.getMessage(), 3, 'ERROR');
            }
            
            return null;
        }   
        catch (Exception ex) {
            Database.rollback(sp);
            
            throwCustomExceptionMsg('Failed to Save Account', 'The account could not be created because of the following problems.', true, ex.getMessage(), 3, 'FATAL');
            
            return null;
        }
        return null;
    }
    
    public PageReference save () {
   
    
        mapPACData(); // map selected address from PAC componenet before save
        //Added By Sandip 03/12/2015
        if(currentRecordTypeSelected == 'New Validated Account'){
           if(account.BillingStreet == '' || account.BillingCity == '' || String.isBlank(account.BillingState)){
                throwCustomExceptionMsg('Error -','You must enter Address,City,Province/State, on selecting Capture New Address', true, '', 3, 'ERROR');     
                return null;
            }
        } 
        displayAccountsMappedToAccountId = new Map<Id, displayAccount>();
        clearExceptionArea();
        newAccountId = null;
        duplicateDetected = false;
        account.Language_Preference__c = contact.WBS_Language_Preference__c;
        
        SavePoint sp = Database.setSavepoint();
        
        try {
            account = account.Clone();
            contact = contact.Clone();
            
//            if(string.isNotBlank(account.BillingPostalCode)&& string.isblank(account.BillingStreet))
//            {
//                throwCustomExceptionMsg('Address InComplete', 'Please provide complete address details.', true, null, 3, 'ERROR');
            
//                return null; 
           

            List<CRMfusionDBR101.DB_Api.Matches> matchSets = CRMfusionDBR101.DB_Api.findMatches( account );
            if ( matchSets.isEmpty() )
            {
                // No matches were found, try to save.
                try
                {
                    insert account;
                    contact.accountId = account.id;
                    
                    if(contactFieldsAvailable){
                        insert contact;
                    }
                    system.debug('Insert contact successful, calling async to cp');
                    
                    String recordTypeName = '';
                    if(rtNameMap.containsKey(account.recordTypeId)){
                        recordTypeName = rtNameMap.get(account.recordTypeId);
                    }

                    Account acctRecordTypeInfo = [SELECT id, recordtype.developerName FROM Account WHERE id = :account.id];
            
                   if(acctRecordTypeInfo.recordtype.developerName != 'New_Account'){
                        smb_CreateCustomerTriggerCalloutHelper.performCustomerServiceCall(new list<id>{account.id});                                                
                        // set Error_Message__c to in progress so that the conversion can be tracked
                        acctRecordTypeInfo.Error_Message__c = 'convert to rcid in progress';                              
                        update acctRecordTypeInfo;       
                         
/*****                    
                        list<CreateCustomerProfileResponse> result = smb_CreateCustomerTriggerCalloutHelper.performCustomerServiceCall(new list<id>{account.id});   
                                        
                        System.debug('convertToRCID: ' + result);   
                        
                        String errorMessage = '';
                        for(CreateCustomerProfileResponse response : result){
                            errorMessage = response.errorMessage; 
                            
                            break;
                        }
                        
                        System.debug('saveOverride, performCustomerServiceCall: ' + errorMessage);                             
                            
                        if(String.isBlank(errorMessage)){
                            // if no error, set to in progress
                            acctRecordTypeInfo .Error_Message__c = '';      
                                        
                            update acctRecordTypeInfo;                
                                             
                        }  
*/                                     
                    }
                }
                catch (QueryException e) {
                    ApexPages.addMessages( e );
                    return null;
                }   
                catch ( DMLException e )
                {
                    ApexPages.addMessages( e );
                    return null;
                }
                catch ( Exception e )
                {
                    ApexPages.addMessages( e );
                    return null;
                }
                return new pageReference( '/' + account.id );
            }
            else
            {
                // Matches were found, just display the list of matching ids in an error
                // message as a basic example.
                displaySearchResults = true;
                String errorMessage = '';
                Set<Id> accIds = new Set<Id>();
                
                for ( CRMfusionDBR101.DB_Api.Matches matchSet : matchSets ){
                    for ( Id accId : matchSet.matchedIds ){
                        errorMessage += accId + ', ';
                        accIds.add(accId);
                    }
                }
                // Trim the trailing ', ';
                List<Account> matchedAccs = [SELECT Id, ParentId, Name, CBUCID_Parent_Id__c, Marketing_Sub_Segment_Description__c,
                                                   Phone, BillingStreet, BillingCity, BillingPostalCode, CBU_Name__c,
                                                   BillingState, RCID__c, CAN__c, BTN__c, 
                                                   RecordType.Name, Billing_Account_Active_Indicator__c, 
                                                    (SELECT Id, Unit_Number__c,
                                                            City__c,State__c,Postal_Code__c,
                                                            Street_Address__c
                                                     FROM Addresses1__r
                                                     LIMIT 1 )
                                            FROM Account
                                            WHERE Id In :accIds
                                            ORDER BY NAME, RCID__c NULLS last
                                            LIMIT 10];
                
                List<Id> accountIdsToCheckAccess = new List<Id>();
                
                for (Account a2 : matchedAccs) {
                    if (displayAccountsMappedToAccountId.containsKey(a2.id)) continue;
                    displayAccountsMappedToAccountId.put(a2.Id, new displayAccount(a2));
                    accountIdsToCheckAccess.add(a2.id);
                }

                //sets user access on record based on record access permission
                accessibleAccounts = [SELECT RecordId FROM UserRecordAccess WHERE UserId=:UserInfo.getUserId() AND HasReadAccess = true AND RecordId IN :accountIdsToCheckAccess LIMIT 1000];
                Set<Id> accessibleAccountsSet = new Set<Id>(); 
                        
                for (UserRecordAccess accId : accessibleAccounts) {
                    accessibleAccountsSet.add(accId.RecordId);
                }

                for (Id accId : displayAccountsMappedToAccountId.keySet()) {
                    if(!accessibleAccountsSet.contains(accId)){
                        displayAccount temp = displayAccountsMappedToAccountId.get(accId);
                        temp.accountAccess = false;
                        displayAccountsMappedToAccountId.put(accId,temp);
                    }
                }

                errorMessage = errorMessage.subString( 0, errorMessage.length() - 2);                
                errorMessage = 'Duplicate accounts found: '+errorMessage;
                errorMessage = '';
                // Return the error.

                duplicateDetected = true;

                throwCustomExceptionMsg('Duplicate Account(s) Found', 'Before proceeding to create the new account, please review the list of existing accounts below as potential matches. If desired account is located, select and continue. If not, then proceed with new account creation by clicking on \'Save New Account\' button.', true, errorMessage, 3, 'ERROR');                

                ApexPages.Message pageMessage = new ApexPages.Message( ApexPages.Severity.ERROR, errorMessage );
                ApexPages.addMessage( pageMessage );
                return null;
            }
                        
        }
        catch (Exception ex) {
            Database.rollback(sp);
            return null;
        }
        
        return null;     
    }

    public Boolean isFieldAvailableForRecordType(){
        return false;               
    }
    
    public String getCurrentRecordTypeSelected(){
        return 'RCID';
    }

    public void setCurrentRecordTypeSelected(){
        String currentRecordType = recordTypeNameAdjustment.get(Apexpages.currentPage().getParameters().get('currentRecordType'));
        currentRecordTypeSelected = Apexpages.currentPage().getParameters().get('currentRecordType');
        System.debug('setCurrentRecordTypeSelected: ' + currentRecordType);
        
        // record type name confunsion
        // New Account: equals "New Prospect Account"
        // New Prospect Account: "New RCID Account" 
        
//        Set<String> availableForRecordType = new Set<String>{'New Prospect Account'};
        Set<String> availableForRecordType = new Set<String>{'New Account', 'New Prospect Account'};
//        Set<String> prospectFieldsAvailableForRecordType = new Set<String>{'New Account'};
        Set<String> prospectFieldsAvailableForRecordType = new Set<String>();
        
        // start added by Aditya Jamwal for showing Mandatory sign on PAC address...
             if(currentRecordTypeSelected == 'New Validated Account')
                 PACMandatory=true;
             else
                 PACMandatory=false;
        //.....
            
        contactFieldsAvailable = availableForRecordType.contains(currentRecordType);
        prospectAccountFieldsAvailable = prospectFieldsAvailableForRecordType.contains(currentRecordType);
        
        account.recordTypeId = rtIdMap.get(currentRecordType);        
    }
    
    public List<SelectOption> getRecordTypesAsSelectOptions() { 
        List<SelectOption> results = new List<SelectOption>();
        
        for(String recordTypeName : recordTypeNameAdjustment.keyset()) {            
            results.add(new SelectOption(recordTypeName, recordTypeName));      
        }
        return results;
    }
    
    public List<SelectOption> getCountriesAsSelectOptions() {
        List<SelectOption> results = new List<SelectOption>();
        
        results.add(new SelectOption('', 'Not Specified'));     
        
        for (Country_Province_Codes__c country : [SELECT Name, Description__c 
                                                  FROM Country_Province_Codes__c
                                                  WHERE Type__c = 'Country'
                                                  ORDER BY Description__c]) {
            results.add(new SelectOption(country.Name, country.Description__c));    
        }
        
        return results;
    }
    
    public List<SelectOption> getProvincesAsSelectOptions() { 
        List<SelectOption> results = new List<SelectOption>();
        
        results.add(new SelectOption('', ''));  
        
        for (Country_Province_Codes__c province : [SELECT Name, Description__c 
                                                   FROM Country_Province_Codes__c
                                                   WHERE Type__c = 'Canadian Province'
                                                   ORDER BY Description__c]) {
            results.add(new SelectOption(province.Name, province.Description__c));      
        }
        
        return results;
    }
    
    public List<SelectOption> getAmericanStatesAsSelectOptions() { 
        List<SelectOption> results = new List<SelectOption>();
        
        results.add(new SelectOption('', ''));  
        
        for (Country_Province_Codes__c province : [SELECT Name, Description__c 
                                                   FROM Country_Province_Codes__c
                                                   WHERE Type__c = 'American State'
                                                   ORDER BY Description__c]) {
            results.add(new SelectOption(province.Name, province.Description__c));      
        }
        
        return results;
    }

    private void clearExceptionArea() {
        
        msgTitle = '';
        msgSummary = '';
        msgRendered = false;
        msgDetail = '';
        msgStrength = 1;
        msgSeverity = 'confirm';
    }
    
    private void throwCustomExceptionMsg(String msgTitle1, String msgSummary1, Boolean msgRendered1, String msgDetail1, Integer msgStrength1, String msgSeverity1) {
        
        msgTitle = msgTitle1;
        msgSummary = msgSummary1;
        msgRendered = msgRendered1;
        msgDetail = msgDetail1;
        msgStrength = msgStrength1;
        msgSeverity = msgSeverity1;
    }
    
    public class displayAccount {
        public Account acct {get; set;}
        public List<SMBCare_Address__c> serviceAddresses {get;set;}
        public string parentRCID {get;set;}
        public String businessName {get; set;}
        public String billingAddress {get;set;}
        public String serviceAddress {get;set;}
        public Id activityAccountId {get;set;}
        public String telephoneNumber {get;set;}
        public Boolean accountActive {get;set;}
        public Boolean accountAccess {get;set;}
        
        public String businessNameJSSafe {
            get {
                return makeJSSafe(businessName);
            }
        }
        
        public displayAccount(Account account) {
            this.acct = account;

            this.accountAccess = true;
            this.businessName = account.Name;
            this.billingAddress = renderAddress('', account.BillingStreet, account.BillingCity, account.BillingState);
            this.accountActive = account.Billing_Account_Active_Indicator__c != 'N';
            this.telephoneNumber = account.BTN__c;
            
            if (account.Addresses1__r.size() > 0) {
                SMBCare_Address__c serviceAddress = account.Addresses1__r.get(0);
                this.serviceAddress = renderAddress(serviceAddress.unit_number__c, serviceAddress.Street_Address__c, serviceAddress.City__c, serviceAddress.State__c);
            }
        }
        
        private string renderAddress(string unitNumber, string street, string city, string state) {
            string address = '';
            
            if (!string.isBlank(unitNumber)) {
                address = unitNumber;
            }
            
            address = appendIfNotBlank(address, ' - ', street);
            address = appendIfNotBlank(address, ', ', city);
            address = appendIfNotBlank(address, ', ', state);
            
            return address;
        }
        
        private string appendIfNotBlank(string originalText, string seperator, string appendage) {
            if (string.isBlank(appendage)) return originalText;
            
            string result = originalText;
            if (!string.isBlank(result)) {
                result += seperator;
            }
            result += appendage;
            
            return result;
        }
                
    }
    
    webservice static String validateAccountAddress(Id accountId){        
        try{
            Account acctToValidate = [SELECT BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry FROM Account WHERE id=:accountId];
                        
            smb_AddressValidationUtility.Address addr = new smb_AddressValidationUtility.Address();
            
            addr.addressLine1 = acctToValidate.BillingStreet;
            addr.addressLine2 = '';
            addr.addressLine3 = '';            
            addr.city = acctToValidate.BillingCity;
            addr.provState = acctToValidate.BillingState;
            addr.postalCode = acctToValidate.BillingPostalCode;
            addr.country = acctToValidate.BillingCountry;
                         
            smb_AddressValidationUtility.ValidateAddressResponse response = smb_AddressValidationUtility.validate(addr, new Set<String>{'C', 'V', 'I'});
                        
            return JSON.serialize(response);
        }
        catch(QueryException e){
            return '{"errorMessages": [QueryException: ' + e.getMessage() + ']}';
        }    
        catch(Exception e){
            return '{"errorMessages": [Exception: ' + e.getMessage() + ']}';
        }        
    }
        
    webservice static String updateAccountAddress(Id accountId, String BillingStreet, String BillingCity, String BillingState, String BillingPostalCode, String BillingCountry){        
        try{
            Account acctToUpdate = [SELECT BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, Name, Correct_Legal_Name__c FROM Account WHERE id=:accountId];
                        
            acctToUpdate.BillingStreet = BillingStreet;
            acctToUpdate.BillingCity = BillingCity;
            acctToUpdate.BillingState = BillingState;
            acctToUpdate.BillingPostalCode = BillingPostalCode;
            acctToUpdate.BillingCountry = BillingCountry;
          
            // ensure legal name is synced with account name
            acctToUpdate.Correct_Legal_Name__c = acctToUpdate.name;
                
            update acctToUpdate;
            
            return '{"success": true}';
        }
        catch(DMLException e){
            return '{"success": false, "errorMessage": "DMLException: ' + e.getMessage() + '"}';
        }    
        catch(Exception e){
            return '{"success": false, "errorMessage": "Exception: ' + e.getMessage() + '"}';
        }        
    }
    
    webservice static String convertToRCID(Id accountId){        
        try{
            Account acct = [SELECT id, name, Error_Message__c FROM Account WHERE id=:accountId];
            if(String.isBlank(acct.Error_Message__c)){
                list<CreateCustomerProfileResponse> result = smb_CreateCustomerTriggerCalloutHelper.performCustomerServiceCall_Synchronous(new list<id>{accountid});                        
                
                System.debug('convertToRCID: ' + result);   
                
                String errorMessage = '';
                for(CreateCustomerProfileResponse response : result){
                    errorMessage = response.errorMessage; 
                    
                    break;
                }
                
                System.debug('convertToRCID, errorMessage: ' + errorMessage);                             
                    
                if(!String.isBlank(errorMessage)){
                    if(errorMessage.contains('You have uncommitted work pending')){
                
                        // clear the error so that CP call can be retried
                        acct.Error_Message__c = '';      
                                
                        update acct;                
                    }               

                    return '{"success": false, "errorMessage" : "CP_ERROR", "errorDetail": "' + errorMessage + '"}';     
                                     
                }                
            }                           
            else{
                if(acct.Error_Message__c == 'convert to rcid in progress'){
                    return '{"success": false, "errorMessage" : "RCID_CONVERSION_IN_PROGRESS"}';     
                }
                else if(acct.Error_Message__c.contains('The connection to Customer Profile is down')){
                
                    // clear the error so that CP call can be retried
                    acct.Error_Message__c = '';      
                                
                    update acct;
                
                    return '{"success": false, "errorMessage" : "CP_CONNECTION_IS_DOWN"}';     
                }                                
            }
            
        }
        catch(DMLException e){
            return '{"success": false, "errorMessage": "DMLException: ' + e.getMessage() + '"}';
        }    
        catch(Exception e){
            return '{"success": false, "errorMessage": "Exception: ' + e.getMessage() + '"}';
        }  
     
        return '{"success": true}';
          
    }
    
    public List<displayAccount> displayAccounts {
        get {
            if (displayAccountsMappedToAccountId == null) return null;
            return displayAccountsMappedToAccountId.values();
        }
    }
    
    private transient List<UserRecordAccess> accessibleAccounts;
    private Map<Id, displayAccount> displayAccountsMappedToAccountId;
    public boolean displaySearchResults {get;set;}
    
    public static string makeJSSafe(string text) {
        if (text == null) return null;
        return text.escapeEcmaScript();
    }
    
     public boolean openAccountInCurrentWindow {
        get {
            return ( ApexPages.currentPage().getParameters().containsKey('c') &&
                     ApexPages.currentPage().getParameters().get('c') == '1' );
        }
    }
    
    /* mapPACData - Map the address details selected from PAC componenet
       Parameter -
       return type - void
    */
    public void mapPACData(){
        account.BillingStreet = PAData.addressLine1;
        account.BillingCity = PAData.city;
        account.BillingState = PAData.province; //PAData.provinceCode;
        account.BillingCountry = PAData.country;
        account.BillingPostalCode = PAData.postalCode;
    }      
}