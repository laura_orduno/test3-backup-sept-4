/*
###########################################################################
# File.........................: E2VaultBaseController.cls
# Created by.............: Gaurav Tiwari (IBM)
# Created Date.........: 24-Dec-2015
# Description...........: The class is an custom controller & handle all basic operations for E2Vault system
#
#
# ****************************************** VERSION UPDATES *********************************************************
#
# 22-Feb-2016    Gaurav Tiwari        Created File
#
*/

public class E2VaultBaseController {
    
    public String responseStr{set; get;}
    public List<SelectOption> billDates {get;set;}
    public String selectedBillDate{get;set;}
    public String accountNumber {set; get;}
    public String RType {set; get;}
    public String accountName {set; get;}
    String pdfEndPointURL;
    public boolean showPdf{get; set;}
    public String userLanguage{get; set;}
    
    String classAndMethodName;
    String errorMsg;
    String stackTrace;
    Boolean isError = false;
    String fullBcan;
    String chBcanId;
    String sourceSystem;
    Boolean isRetry = false;
    E2VaultConfig__c e2ConfigBan = E2VaultConfig__c.getInstance('BAN');
    E2VaultConfig__c e2ConfigNonBan = E2VaultConfig__c.getInstance('NonBAN');
    public Boolean showErrMsg{get;set;}
    public Boolean showErrMsg1{get;set;}
    /*
    * initialize constructor
    */
    public E2VaultBaseController(){
        showErrMsg = false;
        showErrMsg1 = false;
        
        String currentUser = ApexPages.currentPage().getParameters().get('currentUser');
        if(currentUser != null && currentUser!= ''){
            User userObj = [Select LanguageLocaleKey from User where Id =: currentUser];
            userLanguage = userObj.LanguageLocaleKey;
            system.debug('@@@userLanguage' + userLanguage);
        }
        
        String accId = ApexPages.currentPage().getParameters().get('ACCID');    // get account Number from URL
        
        Account accObj =[Select Id, Name, RecordType.DeveloperName,
                        BCAN__c,BAN__c,Billing_System_ID__c, CAN__c
                        FROM Account where Id =: accId];
        if(accObj != null){
            system.debug('@@@accObj  ' + accObj );
            accountName = accObj.Name;    // get account Name from URL
            fullBcan = accObj.BCAN__c;
            chBcanId = accObj.Billing_System_ID__c;
            RType = accObj.RecordType.DeveloperName;
            system.debug('@@@@' + fullBcan);
            if(fullBcan == null || fullBcan == ''){
                accountNumber = accObj.CAN__c;
            }else{
                try{
                    system.debug('full bcan 1' + fullBcan);
                    fullBcan = fullBcan.replace(' ','');
                    system.debug('full bcan 2' + fullBcan);
                    accountNumber = fullBcan.SubString(fullBcan.IndexOf('-')+1);
                    accountNumber = accountNumber.trim();
                    system.debug('@@@ Account Number' + accountNumber );
                }catch(Exception ex){
                    //CallToE2Vault.addErrorMessage(System.Label.E2vault_Error_Message);
                    showErrMsg = true;
                    return;
                }
            }
        }  
        
        SMBCare_WebServices__c pdfEndPointURLCall = SMBCare_WebServices__c.getInstance('E2VaultSecondCallURL');
        String pdfEndPointURLTmp = pdfEndPointURLCall.value__c+'';
        pdfEndPointURL = prepareEndPointURL(pdfEndPointURLTmp);
   
        System.debug('@@@@URLafterchange' + pdfEndPointURL );
        billDates = new List<SelectOption>();
        showPdf = true;
        sendRequestToGetLocatorIds();
        sendRequestToGetPdfURL();
        
        
    }
    
    /*
    * replace system Id and account Id in end point
    */
    private string prepareEndPointURL(String endpoint){
        endpoint = endpoint.replace('~~BAN~~', accountNumber)+'';
        
        if(isRetry == false){
            if(fullBcan == null || fullBcan == ''){
                E2VaultConfig__c e2Config;
                if(chBcanId != null && chBcanId != ''){
                    e2Config = E2VaultConfig__c.getInstance(chBcanId);
                }
                if(e2Config != null){
                     endpoint = endpoint.replace('~~PAR~~', e2Config.value__c)+'';
                     sourceSystem = e2Config.value__c;
                }else{
                    if(RType == 'BAN'){
                        endpoint = endpoint.replace('~~PAR~~', e2ConfigBan.value__c)+'';
                        sourceSystem = e2ConfigBan.value__c;
                    }else{
                        endpoint = endpoint.replace('~~PAR~~', e2ConfigNonBan.value__c)+'';
                        sourceSystem = e2ConfigNonBan.value__c;
                    }
                }
            }else{
                fullBcan = fullBcan.replace(' ','');
                Integer indexVal = fullBcan.IndexOf('-');
                String sysId;
                if(indexVal != -1){
                    sysId = (fullBcan.subString(0,indexVal)).trim();
                    E2VaultConfig__c e2ConfigObj;
                    if(sysId != null && sysId != ''){
                        e2ConfigObj = E2VaultConfig__c.getInstance(sysId);
                    }
                     
                    if(e2ConfigObj != null){
                        endpoint = endpoint.replace('~~PAR~~', e2ConfigObj.value__c)+'';
                        sourceSystem = e2ConfigObj.value__c;
                    }else{
                        endpoint = endpoint.replace('~~PAR~~', e2ConfigNonBan.value__c)+'';
                        sourceSystem = e2ConfigNonBan.value__c;
                    }
                }else{
                    endpoint = endpoint.replace('~~PAR~~', e2ConfigBan.value__c)+'';
                    sourceSystem = e2ConfigBan.value__c;
                }
            }
            return endpoint;
        }else{
            String newSrc = switchWlsWln(sourceSystem);
            endpoint = endpoint.replace('~~PAR~~', newSrc)+'';
            return endpoint;
        }
    }
    
    Private String switchWlsWln(String srcSystem){
        E2VaultConfig__c e2ConfigBan = E2VaultConfig__c.getInstance('BAN');
        E2VaultConfig__c e2ConfigNonBan = E2VaultConfig__c.getInstance('NonBAN');
        if(srcSystem == e2ConfigBan.value__c){
            srcSystem = e2ConfigNonBan.value__c;
        }else{
            srcSystem = e2ConfigBan.value__c;
        }
        return srcSystem;
    }
    /*
    * Send the second request to SDF to get the PDF
    */
    public pageReference sendRequestToGetPdfURL(){
        String pdfURL = '';
        try
        {
            if(selectedBillDate != null){
                pdfURL = pdfEndPointURL + EncodingUtil.urlEncode(selectedBillDate, 'UTF-8');
            }else{
                showPdf = false;
                //CallToE2Vault.addErrorMessage(System.Label.E2vault_Error_Message);
                return null;
            }
            //Get Signature of End point URL
            System.debug('E2Vault second URL ---> '+ pdfURL );
            responseStr = CallToE2Vault.getPdfURL(pdfURL);
            responseStr = responseStr.replace('/download/', '/view/');
       }catch(Exception e){
            isError = true;
            classAndMethodName='E2VaultBaseController.sendRequestToGetPdfURL';
            errorMsg = e.getmessage();
            stackTrace = e.getstacktracestring();
            isError = true;
            System.debug('@@@@Exception' + e);
            showPdf = false;
            showErrMsg = true;
            //CallToE2Vault.addErrorMessage(System.Label.E2vault_Error_Message);
        }
        return null;
    }
    
    /*
    * Send the first request to SDF to get the Bill dates and locator id
    */
    public List<string> sendRequestToGetLocatorIds(){
        try
        {
            
            Map<String, String> billResponse  = new Map<String, String>();
            //Get Signature of End point URL
            SMBCare_WebServices__c locatorIdEndPoint = SMBCare_WebServices__c.getInstance('E2VaultFirstCallURL');
            String endPointURLvalue = locatorIdEndPoint.value__c+'';
            String locatorIdEndPointURL = prepareEndPointURL(endPointURLvalue);
            
            System.debug('E2Vault_Rest Test URL ---> '+ locatorIdEndPointURL);
            
            billResponse  = CallToE2Vault.getLocatorIds(locatorIdEndPointURL);
            showErrMsg1 = CallToE2Vault.showErrMsg1;
            if(billResponse  == NULL){
                //if(isRetry == false){
                //    isRetry = true;
                //    showErrMsg = false;
                //    showErrMsg1 = false;
                 //   CallToE2Vault.showErrMsg1 = false;
                //    CallToE2Vault.showErrMsg = false;
                //    return sendRequestToGetLocatorIds();
                //}else{
                    //showErrMsg = true;
                    showPdf = false;
                    showErrMsg1 = true;
                    CallToE2Vault.addErrorMessage(System.Label.E2vault_Error_Message);
                    return null;
                //}
            }else{
                for(String br: billResponse .keyset()){
                    if(selectedBillDate == null){
                        selectedBillDate = String.valueOf(billResponse.get(br));
                    }
                    billDates.add(new SelectOption(String.valueOf(billResponse.get(br)), br));
                }
            }
        }catch(Exception e){
            isError = true;
            classAndMethodName='E2VaultBaseController.sendRequestToGetLocatorIds';
            errorMsg = e.getmessage();
            stackTrace = e.getstacktracestring();
            System.debug('@@@@Exception' + e);
            showPdf = false;
            showErrMsg = true;
            //CallToE2Vault.addErrorMessage(System.Label.E2vault_Error_Message);
        } 
        return null;
    }
    /*
    * Used to log error with method name and account number
    */
    public void createLog(){
        if(isError == true){
           CallToE2Vault.logError(classAndMethodName, '' , 'Error For Account Number='+accountNumber + '->' + errorMsg, stackTrace);
        }
    }
}