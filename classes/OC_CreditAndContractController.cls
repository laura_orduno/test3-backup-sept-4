/********************************************************************************************************************************
Class Name:     OC_CreditAndContractController
Purpose:        This class is a controller class for OC_CreditAndContract VF .      
Test class Name : OC_CreditAndContractController_test
Created by:     Sandip Chaudhari        
*********************************************************************************************************************************
*/

public class OC_CreditAndContractController{
    public Order objOrder{get;set;}
    public OC_CreditAndContractController(){
        String orderId=ApexPages.currentPage().getparameters().get('parentOrderid');        
        objOrder = getOrderDetail(orderId);
    }
    
     public Order getOrderDetail(String orderId){
        system.debug('%%######%% START: getOrderDetail  %%######%% ');
        Order orderObj;
        try {
            List<Order> orderObjList = [SELECT id,ContactEmail__c,
                            ContactName__c,
                            ContactPhone__c,ContactTitle__c,DeliveryMethod__c,
                            Service_Address_Text__c,Account.Id,AccountId,
                            CustomerAuthorizedByID,CustomerAuthorizedBy.Name,CustomerSignedBy__c,
                            CustomerAuthorizedBy.Email,CustomerAuthorizedBy.Phone,CustomerAuthorizedBy.Title,
                            CustomerSignedBy__r.Name,CustomerSignedBy__r.Email,CustomerSignedBy__r.Phone,CustomerSignedBy__r.Title
                             FROM Order
                             WHERE Id =: orderId   
                             LIMIT 1];
            if(!orderObjList.isEmpty()){
                orderObj=orderObjList.get(0);
            }
         }catch(Exception anException){
            system.debug('OC_CreditAndContractController::GetOrderDetails() exception(' + anException + ')');
            throw anException;
            return null; 
        }
        system.debug('%%######%% END: OC_CreditAndContractController:getOrderDetail  %%######%% ');
        return orderObj;
    }
    public void consolidateContractAttributes(){
        List<OrderItem> oiList=[ select Order.Service_Address__c,Service_Address__c,term__c,order.statuscode, vlocity_cmt__RootItemId__c,Contract_Action__c,id,Contract_Line_Item__c, Contract_Request__c from OrderItem where order.statuscode !='A' and order.parentid__c=:objOrder.id ];
        Map<String,String> rootIdCrMap=new Map<String,String>();
        for(OrderItem oiInst:oiList){            
            if(!rootIdCrMap.containsKey(oiInst.vlocity_cmt__RootItemId__c)){
                rootIdCrMap.put(oiInst.vlocity_cmt__RootItemId__c,oiInst.Contract_Request__c);
            }            
        }
        Boolean isDirty=false;
        for(OrderItem oiInst:oiList){
             String currentOrdrStatusCat=oiInst.order.statuscode;
            if(String.isNotBlank(currentOrdrStatusCat) && 'A'.equalsIgnoreCase(currentOrdrStatusCat)){
                continue;
            }
            if(String.isBlank(oiInst.Contract_Request__c)){
                oiInst.Contract_Request__c=rootIdCrMap.get(oiInst.vlocity_cmt__RootItemId__c);
                isDirty=true;
            } 
            if(String.isBlank(oiInst.Contract_Action__c)){
                oiInst.Contract_Action__c='Add';
                isDirty=true;
            }
            if(String.isNotBlank(oiInst.Term__c)){
                String term=oiInst.Term__c;
                term=term.trim();
                if(term.indexOf(' ')!=-1){
                    term=term.substringBefore(' ');
                    oiInst.Term__c=term;
                    isDirty=true;
                }
            }
            if(String.isBlank(oiInst.Service_Address__c)){
                oiInst.Service_Address__c=oiInst.Order.Service_Address__c;
                isDirty=true;
            }
        }
        if(isDirty){
           // OCOM_OrderProductTriggerHelper.updatefromContractCreditPage=true;
            update oiList;
        }
    }
}