public with sharing class trac_Related_Offer_House_Demands_Ext {
	
	private final Account acct;
	public String systemURL {get; set;} // base URL of the current salesforce instance, used to launch new pages
	public Boolean isFrench {get; set;}
	
	public trac_Related_Offer_House_Demands_Ext(ApexPages.StandardController stdController) {
		this.acct = (Account)stdController.getRecord();
		
        systemURL = URL.getSalesforceBaseUrl().getHost();
        initLanguage();
	}
	
	public List<Offer_House_Demand__c> getTheOffers() {
		
		List<Offer_House_Demand__c> currentOffers = [SELECT Id, Name, ACCOUNT__c, Type__c, Version_Number__c, CreatedDate,
                                                        Opportunity__c, Opportunity__r.Name, REQUEST_NUMBER__c, Status__c,
                                                        OH_EXPIRATION_DATE__c, REQUEST_TYPE__c, Closed_Status__c, OH_Flag__c,
                                                          OwnerId, LastModifiedById, LastModifiedBy.Name, Owner.Name
																								 FROM Offer_House_Demand__c
																								 WHERE ACCOUNT__c = :acct.Id
																								 	AND Type__c = 'Current' ORDER BY CreatedDate DESC];
		return currentOffers;
	}
  
  public void initLanguage() {
    isFrench = !(UserInfo.getLanguage() == 'en_US');
  }
	
  public static testMethod void testMyController() {

    // User must have a profile and a role
    Id PROFILEID = [SELECT id FROM Profile WHERE name='Sales Manager / Director / MD' LIMIT 1].id;
    
    // Get a user to test
    User tester = [SELECT Id FROM User WHERE isActive = true AND ProfileId = :PROFILEID LIMIT 1];
    
    // Create Account
    Account a = new Account(name = 'Test Sales Account', BillingCountry = 'US');
    system.runAs(tester) {  insert a; }
    
    
    ApexPages.StandardController sc = new ApexPages.standardController(a);
    
    // create an instance of the controller
    trac_Related_Offer_House_Demands_Ext myPageCon = new trac_Related_Offer_House_Demands_Ext(sc);
    myPageCon.getTheOffers();
    
  }
}