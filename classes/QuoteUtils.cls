public class QuoteUtils {
	public static String getSeparatorSymbol() {
		String test = (10.15).format();
		return (test.contains('.') ? ',' : '.');
	}
	
	public static String getDecimalSymbol() {
		String test = (10.15).format();
		return (test.contains('.') ? '.' : ',');
	}
	
	public static Double toDouble(String value) {
		if (StringUtils.isBlank(value)) {
			return null;
		}
		String[] parts = value.split('\\' + getDecimalSymbol(), 2);
		parts[0] = parts[0].replaceAll('\\' + getSeparatorSymbol(), '');
		if (parts.size() == 2) {
			return Double.valueOf(parts[0] + '.' + parts[1]);
		}
		return Double.valueOf(parts[0]);
	}
	
	/**
	 * Collects Opportunity IDs from specified list of quotes.
	 */
	public static Set<Id> collectOpportunityIds(List<SBQQ__Quote__c> quotes) {
		Set<Id> ids = new Set<Id>();
		for (SBQQ__Quote__c quote : quotes) {
			if (quote.SBQQ__Opportunity__c != null) {
				ids.add(quote.SBQQ__Opportunity__c);
			}
		}
		return ids;
	}
	
	/**
	 * Collects IDs from specified list of quotes.
	 */
	public static Set<Id> collectIds(List<SBQQ__Quote__c> quotes) {
		Set<Id> ids = new Set<Id>();
		for (SBQQ__Quote__c quote : quotes) {
			if (quote.Id != null) {
				ids.add(quote.Id);
			}
		}
		return ids;
	}
	
	public static String getCurrencySymbol(SObject record) {
		if (UserInfo.isMultiCurrencyOrganization()) {
			Object isoCode = record.get('CurrencyIsoCode');
			if ((record != null) && (isoCode != null)) {
				return isoCode + ' ';
			} else {
				return '';
			}
		}
		
		return '$';
	}
	
	/**
	 * Returns pattern that should be used in formatting prices.
	 */
	public static String getCurrencyFormatPattern(SObject record, Integer scale) {
		String symbol = getCurrencySymbol(record);
		return getCurrencyFormatPattern(symbol, scale);
	}
	
	/**
	 * Returns pattern that should be used in formatting prices.
	 */
	public static String getCurrencyFormatPattern(String symbol, Integer scale) {
		String result = symbol;
		result += '#,##0';
		if (scale > 0) {
			result += '.';
		}
		for (Integer i=0;i<scale;i++) {
			result += '0';
		}
		return result;
	}
	
	public static String formatDecimal(Decimal value) {
		return formatDecimal(value, false);
	}
	
	public static String formatDecimal(Decimal value, Boolean maintainScale) {
		String format = value.toPlainString();
		String decimalFormat = null;
		String[] parts = format.split('\\.',2);
		if (parts.size() == 2) {
			for (Integer i=parts[1].length();i<value.scale();i++) {
				parts[1] += '0';
			}
			decimalFormat = parts[1];
		}
		format = Decimal.valueOf(parts[0]).format();
		if (decimalFormat != null) {
			// Test if fractional part is greater than 0
			Decimal whole = Decimal.valueOf(parts[0]);
			if (maintainScale || ((value - whole) != 0)) {
				format += (getDecimalSymbol() + decimalFormat);
			}
		}
		return format;
	}
	
	public static String htmlEncode(String value) {
		if (value == null) {
			return null;
		}
		value = value.replaceAll('&','&amp;');
		value = value.replaceAll('<','&lt;');
		value = value.replaceAll('>','&gt;');
		value = value.replaceAll('\n\n','<p>&#160;</p>');
		value = value.replaceAll('\n','<br/>');
		return value;
	}
	
	/**
	 * Collects IDs of products referenced by quote lines of specified quotes.
	 */
	public static Set<Id> collectProductIds(List<SBQQ__Quote__c> quotes) {
		Set<Id> productIds = new Set<Id>();
		for (SBQQ__Quote__c quote : quotes) {
			for (SBQQ__QuoteLine__c line : quote.SBQQ__LineItems__r) {
				productIds.add(line.SBQQ__Product__c);
			}
		}
		return productIds;
	}
	
	/**
	 * Tests of specified field should be treated as unit price field. 
	 * Unit price fields may be formatted with configurable scale of precision.
	 */
	public static Boolean isUnitPriceField(Schema.SObjectField field) {
        return isUnitPriceField(field.getDescribe().getName());
    }
	
	public static Decimal toPercent(Decimal total, Decimal amount) {
		total = (total == null) ? 0 : total;
		amount = (amount == null) ? 0 : amount;
		return (amount / total) * 100;
	}
	
	public static Decimal toAmount(Decimal total, Decimal percent) {
		total = (total == null) ? 0 : total;
		percent = (percent == null) ? 0 : percent; 
		return (total * percent / 100);
	}
	
	public static Boolean isFieldMappable(Schema.SObjectField field1, Schema.SObjectField field2) {
		if ((field1 == null) || (field2 == null)) {
			return false;
		}
		
		if (field1.getDescribe().getType() != field2.getDescribe().getType()) {
			return false;
		}
		if (field1.getDescribe().getType() == Schema.DisplayType.Reference) {
			return field1.getDescribe().getReferenceTo()[0] == field2.getDescribe().getReferenceTo()[0];
		}
		return true;
	}
	
	public static Boolean isValueTrue(Object value) {
    	if (value == null) {
    		return false;
    	}
    	if (value instanceof Decimal) {
    		return ((Decimal)value) != 0;
    	} else if (value instanceof Boolean) {
    		return ((Boolean)value);
    	}
    	return String.valueOf(value).equalsIgnoreCase('yes') || String.valueOf(value).equalsIgnoreCase('1') || String.valueOf(value).equalsIgnoreCase('true');
    }
	
	public static Decimal multiply(Decimal value1, Decimal value2) {
		if ((value1 == null) || (value2 == null)) {
			return null;
		}
		return value1 * value2;
	}
	
	public static Decimal multiplyAsZero(Decimal value1, Decimal value2) {
		return ensureValue(multiply(value1, value2));
	}
	
	public static Decimal subtract(Decimal value1, Decimal value2) {
		if ((value1 == null) || (value2 == null)) {
			return null;
		}
		return value1 - value2;
	}
	
	public static Decimal divide(Decimal value1, Decimal value2) {
		if ((value1 == null) || (value2 == null)) {
			return null;
		}
		return value1 / value2;
	}
	
	public static Decimal add(Decimal value1, Decimal value2) {
		if ((value1 == null) || (value2 == null)) {
			return null;
		}
		return value1 + value2;
	}
	
	public static Decimal addAsZero(Decimal value1, Decimal value2) {
		if (value1 == null) {
			value1 = 0;
		}
		if (value2 == null) {
			value2 = 0;
		}
		return value1 + value2;
	}
	
	public static Decimal ensureValue(Decimal value) {
		return (value == null) ? 0: value;
	}
	
	public static Map<String,Schema.SObjectField> indexFieldsByName(List<Schema.SObjectField> fields) {
		Map<String,Schema.SObjectField> result = new Map<String,Schema.SObjectField>();
		for (Schema.SObjectField field : fields) {
			result.put(field.getDescribe().getName(),field);
		}
		return result;
	}
	
	 public static Boolean isUnitPriceField(String fieldName) {
        return String.valueOf(SBQQ__QuoteLine__c.SBQQ__ListPrice__c).equalsIgnoreCase(fieldName) ||
            String.valueOf(SBQQ__QuoteLine__c.SBQQ__CustomerPrice__c).equalsIgnoreCase(fieldName) ||
            String.valueOf(SBQQ__QuoteLine__c.SBQQ__NetPrice__c).equalsIgnoreCase(fieldName) ||
            String.valueOf(SBQQ__QuoteLine__c.SBQQ__RegularPrice__c).equalsIgnoreCase(fieldName);
    }
    
    public static Boolean isUnitCostField(String fieldName) {
    	return String.valueOf(SBQQ__QuoteLine__c.SBQQ__UnitCost__c).equalsIgnoreCase(fieldName);
    }
    
    
    public static void copyAdditionalInformation(SBQQ__ProductOption__c productOption, String encodedAdditionalInfo) {
		if (!StringUtils.isBlank(encodedAdditionalInfo)) {
			Map<String, String> result = parseAdditionalInfo(encodedAdditionalInfo);
			Set<String> fields = result.keySet();
			
			for (String destFieldName : fields) {
				Schema.SObjectField destField = MetaDataUtils.getField(productOption.getSObjectType(), destFieldName);
				if ((destField != null) && !destField.getDescribe().isCalculated()) {
					productOption.put(destField, result.get(destFieldName));
				}
			}		
		}
	}	

    private static Map<String, String> parseAdditionalInfo(String encodedInfo) {
        Map<String, String> result = new Map<String, String>();
        String[] pairs = encodedInfo.split('(?<!\\\\)\\;');
        for (String chunk : pairs) {
            String[] pair = chunk.split('(?<!\\\\):');
            String name = pair[0].replaceAll('\\\\\\;', '\\;').replaceAll('\\\\:', ':').replaceAll('\\\\\\\\', '\\\\');
            result.put(name, pair[1].replaceAll('\\\\\\;', ';').replaceAll('\\\\:', ':').replaceAll('\\\\\\\\', '\\'));
        }
        return result;
    }
    
 	public static String appendAdditionalInfo(String encodedAdditionalInfo, String fieldName, String value) {
        encodedAdditionalInfo += (encodedAdditionalInfo.equals('') ? '' : ';')
                + fieldName.replaceAll('\\\\', '\\\\\\\\').replaceAll('\\;', '\\\\;').replaceAll(':', '\\\\:')
                + ':' + value.replaceAll('\\\\', '\\\\\\\\').replaceAll('\\;', '\\\\;').replaceAll(':', '\\\\:');
        return encodedAdditionalInfo;
    }

    public static void mapAdditionalInfoCustomFields(SBQQ__QuoteLine__c line, SBQQ__ProductOption__c productOption) {
    	mapCustomFields(line, productOption);					
    }
    
    public static void mapCustomLineFields(SBQQ__QuoteLine__c line, Product2 product, List<Schema.SObjectField> fields) {
    	mapCustomFields(line, product, fields);
	}
    
    public static void mapCustomFields(SObject dest, SObject src) {
    	List<Schema.SObjectField> fields = new List<Schema.SObjectField>();
    	for (Schema.SObjectField field : MetaDataUtils.getFields(dest.getSObjectType())) {
    		if (field.getDescribe().isCustom() && field.getDescribe().isUpdateable()) {
    			fields.add(field);
    		}
    	}
    	mapCustomFields(dest, src, fields);
    }
    
    public static void mapCustomFields(SObject dest, SObject src, List<Schema.SObjectField> fields) {
    	List<String> fieldNames = new List<String>();
    	for (Schema.SObjectField field : fields) {
    		fieldNames.add(field.getDescribe().getName());
    	}
    	mapCustomFields(dest, src, fieldNames);
    }
    
    public static void mapCustomFields(SObject dest, SObject src, List<String> fields) {
		for (String destFieldName : fields) {
			Schema.SObjectField destField = MetaDataUtils.getField(dest.getSObjectType(), destFieldName);
			if ((destField == null) || destField.getDescribe().isCalculated()) {
				continue;
			}
			Schema.SObjectField srcField = MetaDataUtils.getField(src.getSObjectType(), destFieldName);
			if (QuoteUtils.isFieldMappable(destField, srcField)) {
				if (src.get(srcField) != null) {
					dest.put(destField, src.get(srcField));
					if (destField.getDescribe().getType() == Schema.DisplayType.Reference) {
						SObject related = src.getSObject(srcField);
						if (related != null) {
							dest.putSObject(destField, related);
						} else {
							dest.putSObject(destField, null);
						}
					}
				}
			}
		}
	}
    
    public static Map<String,List<SObject>> groupByField(List<SObject> records, Schema.SObjectField field) {
    	Map<String,List<SObject>> result = new Map<String,List<SObject>>();
    	for (SObject rec : records) {
    		Object value = rec.get(field);
    		String key = '';
        	if (value != null) {
        		key = String.valueOf(value);
        	}
        	List<SObject> grp = result.get(key);
    		if (grp == null) {
    			grp = new List<SObject>();
    			result.put(key, grp);
    		}
    		grp.add(rec);
    	}
    	return result;
    }
    
    public static Object toObject(Schema.SObjectField field, String value) {
    	if (field.getDescribe().getSOAPType() == Schema.SOAPType.Integer) {
    		return Integer.valueOf(value);
    	} else if (field.getDescribe().getSOAPType() == Schema.SOAPType.Double) {
    		return Double.valueOf(value);
    	} else if (field.getDescribe().getSOAPType() == Schema.SOAPType.Date) {
    		return Date.valueOf(value);
    	} else if (field.getDescribe().getSOAPType() == Schema.SOAPType.DateTime) {
    		return DateTime.valueOf(value);
    	} else {
    		return value;
    	}
    }
    
    public static String getCurrencyCode(SObject record) {
    	if (UserInfo.isMultiCurrencyOrganization()) {
    		return (String)record.get('CurrencyIsoCode');
    	}
    	return null;
    }
    
    public static void setCurrencyCode(SObject record, String code) {
    	if (UserInfo.isMultiCurrencyOrganization() && (code != null)) {
    		record.put('CurrencyIsoCode', code);
    	}
    }
    
    public static Object convertNumber(Schema.SObjectField field, Object value) {
    	if (value == null) {
    		return null;
    	} else if (field.getDescribe().getSOAPType() == Schema.SOAPType.Integer && value instanceof Double) {
    		return Double.valueOf(value);
    	} else if (field.getDescribe().getSOAPType() == Schema.SOAPType.Double && value instanceof Integer) {
    		return Integer.valueOf(value);
    	} else {
    		return value;
    	}
    }
    
    public static String getInstanceName() {
    	String host = URL.getSalesforceBaseUrl().getHost();
    	String[] parts = host.split('\\.');
    	if (parts[0].equalsIgnoreCase('SBQQ')) {
    		return parts[1];
    	} else {
    		return parts[0];
    	}
    }
    
    public static ApexPages.Severity toSeverity(String value) {
    	if ((value == null) || value.equalsIgnoreCase('info')) {
    		return ApexPages.Severity.INFO;
    	} else if (value.equalsIgnoreCase('error')) {
    		return ApexPages.Severity.ERROR;
    	} else if (value.equalsIgnoreCase('warning')) {
    		return ApexPages.Severity.WARNING;
    	} else if (value.equalsIgnoreCase('fatal')) {
    		return ApexPages.Severity.FATAL;
    	} else if (value.equalsIgnoreCase('confirm')) {
    		return ApexPages.Severity.CONFIRM;
    	}
    	return ApexPages.Severity.INFO;
    }
    
    public static String toHTML(String text) {
    	if (text != null) {
    		text = text.replaceAll('\n', '<br>');
        	text = text.replaceAll('"', '&quot;');
    	}
    	return text;
    }
    
    public static String getDeveloperPrefix() {
    	return String.valueOf(SBQQ__Quote__c.sObjectType).toLowerCase().startsWith('sbqq__') ? 'SBQQ__' : '';
    }
}