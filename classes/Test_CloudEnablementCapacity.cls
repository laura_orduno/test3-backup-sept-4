@isTest
private class Test_CloudEnablementCapacity{
    static testmethod void test1()
    {
        CloudProjectTeamCapacity.clearTeams();
        CloudProjectTeamCapacity.populateTeams();

        Id cloudProjectId = addCloudProject('TEST CLOUD PROJECT 0001', Date.newInstance(2014, 3, 1), Date.newInstance(2014, 5, 1));
        Id userId = addCloudProjectTeamMember('Cindy Nancoo', cloudProjectId);
        
		CloudProjectTeamCapacity cptCapacity = new CloudProjectTeamCapacity();        		        
        cptCapacity.getCapacityData();
    }
    
    static Id addCloudProject(String name, Date startDate, Date releaseDate)
    {
        // get record type map
        Map<String, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.SMET_Project__c.getRecordTypeInfosByName();
        
        SMET_Project__c cloudProject = new SMET_Project__c();
        
        cloudProject.recordTypeID = rtMap.get('Stakeholder Project').getRecordTypeId();
        cloudProject.name = name;
        cloudProject.project_prime__c = [SELECT id, name FROM User WHERE name = 'Travis Cote' LIMIT 1].id;
        cloudProject.evolution_roadmap__c = 'Yes';
        cloudProject.crm_team_resourced__c = 'Yes';
        cloudProject.milestone__c = 'In Progress';
        cloudProject.planned_start_date__c = startDate;
        cloudProject.Planned_Release_Date__c = releaseDate;
        insert cloudProject;
        
        return cloudProject.id;
    }
    
    static Id addCloudProjectTeamMember(String name, Id cloudProjectId)
    {
        Cloud_Enablement_Project_Team__c user = new Cloud_Enablement_Project_Team__c();
        user.team_member__c = [SELECT id, name FROM User WHERE name = :name LIMIT 1].id;
        user.role__c = 'Developer';
        user.of_workload_for_this_project__c = 50;
        user.Cloud_Enablement_Project__c = cloudProjectId;
        
        insert user;
        
        return user.id;
    }
}