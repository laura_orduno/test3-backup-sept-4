/* Developed By: Hari Neelalaka, IBM
*  Created Date: 18-Dec-2014
*/
@isTest

private Class SRS_ContactLookupController_Test{     
       
    static testMethod void testLookup()
        {  
            test.startTest();            
            //Create test data for Account
            Account acc = new Account();
            acc.Name = 'testAccount';
            insert acc;            
            // Create test data for Contact
            Contact con = new Contact();            
            con.AccountId = acc.Id;
            con.LastName = 'Test';
            insert con;
            //Creating page reference to current VF page "SRS_ContactLookup"
            PageReference pageRef = Page.SRS_ContactLookup;
            // Copy Account Id created 
            String Id = acc.Id;
            Test.setCurrentPage(pageRef);
            // Add parameters to page URL
            ApexPages.currentPage().getParameters().put('accId',Id);    
            String searchString = ApexPages.currentPage().getParameters().put('lksrch', 'I-yuan Han'); 
            // Instantiate the controller with all parameters in the page
            SRS_ContactLookupController conLkup = new SRS_ContactLookupController();            
            //Run the code to test the controller "SRS_ContactLookupController"
            conLkup.search();
            conLkup.cancel();
            conLkup.save();
            conLkup.runSearch();
            Account acc1 = new Account();
            acc1.Name = 'testAccount1';
            conLkup.account=acc1;
            conLkup.saveAccount();
            conLkup.getFormTag();
            conLkup.getTextBox();
            conLkup.newContact();
            //conLkup.performSearch(searchString);
            conLkup.performSearch('Test');
        }

}