/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 * Name - SMB_CPQConnectionController_Test
 * 1. Created By - Sandip - 23 Sept 2016
 * 2. Modified By 
 */
 
@isTest(SeeAllData=true)
public class SMB_CPQConnectionController_Test{
    public static testMethod void testCPQCoonactionController(){
        try{
        //SMB_CPQ_Dummy_Opportunity__c obj2 = new SMB_CPQ_Dummy_Opportunity__c (Name = 'CPQ', Opportunity__c = 'OR-00118806');
        //insert obj2;
        
        test.startTest();
        SMB_CPQConnectionController obj1 = new SMB_CPQConnectionController();
        obj1.redirect();
        test.stopTest();
        }catch(Exception ex){}
    }
}