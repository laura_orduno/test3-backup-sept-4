/**
 * Unit tests for <code>ConfigurationRuleValidator</code>
 * 
 * @author Max Rudman
 * @since 5/2/2011
 */
@isTest
private class ConfigurationRuleValidatorTests {
	private static Product2 bundle;
	private static Product2 p1;
	private static Product2 p2;
	
	testMethod static void testValidate() {
		setUp();
		
		SBQQ__ProductRule__c rule = new SBQQ__ProductRule__c(Name='Test',SBQQ__Active__c=true);
		rule.SBQQ__ConditionsMet__c = 'All';
		rule.SBQQ__ErrorMessage__c = 'Test error';
		rule.SBQQ__Scope__c = 'Product';
		insert rule;
		
		SBQQ__ConfigurationRule__c crule = new SBQQ__ConfigurationRule__c(SBQQ__Product__c=bundle.Id);
		crule.SBQQ__ProductRule__c = rule.Id;
		crule.SBQQ__Active__c = true;
		insert crule;
		
		SBQQ__SummaryVariable__c v1 = new SBQQ__SummaryVariable__c();
		v1.SBQQ__TargetObject__c = 'Product Option';
		v1.SBQQ__AggregateFunction__c = 'Sum';
		v1.SBQQ__AggregateField__c = 'Quantity';
		v1.SBQQ__FilterField__c = String.valueOf(SBQQ__ProductOption__c.SBQQ__OptionalSKU__c);
		v1.SBQQ__FilterValue__c = p1.Id;
		v1.SBQQ__Operator__c = 'equals';
		
		SBQQ__SummaryVariable__c v2 = new SBQQ__SummaryVariable__c();
		v2.SBQQ__TargetObject__c = 'Product Option';
		v2.SBQQ__AggregateFunction__c = 'Sum';
		v2.SBQQ__AggregateField__c = 'Quantity';
		v2.SBQQ__FilterField__c = String.valueOf(SBQQ__ProductOption__c.SBQQ__OptionalSKU__c);
		v2.SBQQ__FilterValue__c = p2.Id;
		v2.SBQQ__Operator__c = 'equals';
		insert new List<SBQQ__SummaryVariable__c>{v1, v2};
		
		SBQQ__ErrorCondition__c cond = new SBQQ__ErrorCondition__c(SBQQ__Rule__c=rule.Id);
		cond.SBQQ__TestedVariable__c = v1.Id;
		cond.SBQQ__Operator__c = 'not equals';
		cond.SBQQ__FilterVariable__c = v2.Id;
		cond.SBQQ__FilterType__c = 'Variable';
		insert cond;
		
		SBQQ__ProductOption__c o1 = new SBQQ__ProductOption__c(SBQQ__ConfiguredSKU__c=bundle.Id,SBQQ__OptionalSKU__c=p1.Id,SBQQ__Quantity__c=5);
		SBQQ__ProductOption__c o2 = new SBQQ__ProductOption__c(SBQQ__ConfiguredSKU__c=bundle.Id,SBQQ__OptionalSKU__c=p2.Id,SBQQ__Quantity__c=3);
		
		ConfigurationRuleValidator target = new ConfigurationRuleValidator(bundle.Id, new List<SBQQ__ProductOption__c>{o1,o2});
		List<String> result = target.validate();
		System.assertEquals(1, result.size(), 'Messages: ' + result);
		System.assertEquals(rule.SBQQ__ErrorMessage__c, result[0]);
		
		o2.SBQQ__Quantity__c = 5;
		target = new ConfigurationRuleValidator(bundle.Id, new List<SBQQ__ProductOption__c>{o1,o2});
		result = target.validate();
		System.assertEquals(0, result.size(), 'Messages: ' + result);
	}
	
	private static void setUp() {
		bundle = new Product2(Name='Budle');
		p1 = new Product2(Name='Test1');
		p2 = new Product2(Name='Test2');
		insert new List<Product2>{bundle,p1,p2};
	}
}