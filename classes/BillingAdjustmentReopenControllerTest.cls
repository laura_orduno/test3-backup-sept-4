@isTest
public class BillingAdjustmentReopenControllerTest{
	
    private static final String TRANSCATION_TYPE ='Credit';
    private static final String BUSINESS_UNIT ='THPS';
    
    @isTest
    public static void testRedirect(){
      loadProductApprovalThresholdSettings();
      BillingAdjustmentCommonTest.createReleaseCodeThresholdData(TRANSCATION_TYPE,BUSINESS_UNIT);  
      Billing_Adjustment__c ba = BillingAdjustmentCommonTest.createBillingAdjustment(2000, TRANSCATION_TYPE, BUSINESS_UNIT);
      ba.Original_Recordtype__c='Credit';
      update ba;  
      ba = [select id,name,CreatedById,Original_Recordtype__c from Billing_Adjustment__c where id =:ba.id];   
      ApexPages.StandardController sc = new ApexPages.standardController(ba);
      BillingAdjustmentReopenController ctrl = new BillingAdjustmentReopenController(sc);
      PageReference pr = ctrl.redirect();
      ba = [select id, Status__c ,Approval_Status__c,RecordType.Name from Billing_Adjustment__c where id =:ba.id]; 
	  System.assertNotEquals(null, pr, 'After redirect page reference should not be null');
      System.assertEquals('Credit',ba.RecordType.Name, 'Record Type name should be Credit');
      System.assertEquals(null,ba.Approval_Status__c, 'Record Type name should be Credit');
      System.assertEquals('Draft',ba.Status__c, 'Record Type name should be Credit');
        
    }

    /**
     * loadProductApprovalThresholdSettings
     * @description Loads the threshold fields to search for in the query
     * @author Thomas Tran, Traction on Demand
     * @date 02-29-2016
     */
    private static void loadProductApprovalThresholdSettings(){
        List<Product_Approval_Threshold_Settings__c> patSettings = new List<Product_Approval_Threshold_Settings__c>();

        patSettings.add(new Product_Approval_Threshold_Settings__c(Name='Credit', Threshold_API_Name__c='Credit_Threshold__c'));
        patSettings.add(new Product_Approval_Threshold_Settings__c(Name='Debit', Threshold_API_Name__c='Debit_Threshold__c'));
        patSettings.add(new Product_Approval_Threshold_Settings__c(Name='TLC Charge', Threshold_API_Name__c='TLC_Charge_Threshold__c'));
        patSettings.add(new Product_Approval_Threshold_Settings__c(Name='TLC Waive', Threshold_API_Name__c='TLC_Waive_Threshold__c'));

        insert patSettings;

    }
}