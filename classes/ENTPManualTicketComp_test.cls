@isTest
public class ENTPManualTicketComp_test {
    
    
    @isTest(SeeAllData=false) static List<Ticket_Event__c> getTestAssets(){
        List<Ticket_Event__c> testAssets = new List<Ticket_Event__c>{};
          Ticket_Event__c ta = new Ticket_Event__c();
        ta.Case_Number__c = '12345';
        ta.Lynx_Ticket_Number__c = '2345678';
        //ta.Case_Id__c = '11111111';
        //ta.Status__c = 'In Progress';
        testAssets.add(ta);
        return testAssets;
    }
        
       
    
    private static testmethod void ManualTicketCompTrigger() {
         account a = new account(name = 'test acct');
        insert a;
        

        List<Case> testCases = MBRTestUtils.createENTPCases(2, a.Id);

        MBRTestUtils.createAttachment(testCases[0].Id, 1, 'jpg');
        List<Attachment> caseAttachments = [SELECT Id, Name, CreatedDate, LastModifiedDate FROM Attachment WHERE ParentId = :testCases[0].Id];
        EmailMessage eMsg = MBRTestUtils.createEmail(testCases[0].Id, true, 'test incoming email ');
        EmailMessage eMsg2 = MBRTestUtils.createEmail(testCases[0].Id, false, 'test outbound email ');
        //List<Attachment> eMsgAttachments = MBRTestUtils.createAttachment(eMsg.Id, 1, 'png');

        ENTPCaseDetail.AttachmentWrapper attWrap = new ENTPCaseDetail.AttachmentWrapper( caseAttachments[0] );

        caseAttachments[0].Name = 'test.zip.' + caseAttachments[0].Name;
        update caseAttachments[0];
        attWrap = new ENTPCaseDetail.AttachmentWrapper( caseAttachments[0] );        
        
        List<Case> caseList = [SELECT createdby.firstname, createdby.lastname, NotifyCustomer__c, NotifyCollaboratorString__c, OwnerId, Resolution_Details__c, My_Business_Requests_Type__c, ParentId, isClosed, Contact.FirstName, Contact.LastName, id,contact.name,subject,description,lastmodifieddate,case_resolution__c,createddate, caseNumber, recordType.name, account.name, type, status, priority, isEscalated, Escalation_History__c, (select Active__c,Collaborator_Email__c, Collaborator_Name__c, Role__c from Collaborator__r order by createddate desc),(select createdby.firstname, createdby.lastname,Id,Name,Description,ParentId,CreatedDate,LastModifiedDate, ContentType from Attachments order by LastModifiedDate asc), (select createdby.firstname, createdby.lastname,Id,CommentBody,ParentId,CreatedDate,LastModifiedDate from casecomments where IsPublished = true order by CreatedDate asc), (select id, toaddress,fromaddress,fromname,ccaddress,messagedate,subject,htmlbody,textbody,hasattachment from emailmessages) FROM Case WHERE Id = :testCases[0].Id];
        Case cs = caseList[0];
        Case cs1 = testCases[1];

        cs.ParentId = cs1.Id;
        update cs;

        System.debug('cs.CaseNumber: ' + cs.CaseNumber);
        
        List<Ticket_Event__c> testAssets = getTestAssets();
        testAssets[0].Status__c = 'New';
        testAssets[0].Event_Type__c = 'New Ticket';
        testAssets[0].Case_id__c = cs.id;
        insert testAssets;
        
    Test.startTest();
    testAssets[0].Status__c = 'Complete';
        update testAssets;
   // Ticket_Event__c[] chk = [select Case_Number__c, Case_Id__c, Lynx_Ticket_Number__c, Id from Ticket_Event__c where Status__c = 'Complete' order by LastModifiedDate desc limit 1];
    
    
    Test.stopTest();	

}

}