public class OrdrGenericException extends Exception {   
    public String errorCode{get;set;}   
    public String sourceSystem{get;set;} 
    public String nativeErrorCode{get;set;} 
    public String exceptionType{get;set;}     
   
}