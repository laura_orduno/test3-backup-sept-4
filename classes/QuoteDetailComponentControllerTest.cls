@isTest
private class QuoteDetailComponentControllerTest {
    testMethod static void test() {
        Web_Account__c account = new Web_Account__c(Name='Test');
        insert account;
        
        Opportunity opp = new Opportunity(Name='Test',CloseDate=System.today(),StageName='Prospecting');
        opp.Web_Account__c = account.Id;
        insert opp;
        
        SBQQ__Quote__c quote = new SBQQ__Quote__c(SBQQ__Opportunity__c=opp.Id);
        insert quote;
        
        Product2 p = new Product2(Name='Test');
        insert p;
        
        SBQQ__QuoteLine__c line = new SBQQ__QuoteLine__c(SBQQ__Quote__c=quote.Id,SBQQ__Product__c=p.Id);
        insert line;
        
        SBQQ__QuoteTemplate__c template = new SBQQ__QuoteTemplate__c(Name='Test');
        insert template;
        
        QuoteDetailComponentController target = new QuoteDetailComponentController();
        target.quoteId = quote.Id;
        System.assertEquals(1, target.getLines().size());
    }
}