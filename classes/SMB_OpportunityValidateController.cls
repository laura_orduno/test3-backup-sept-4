public with sharing class SMB_OpportunityValidateController {

    private final Opportunity opp;
    public String errorMessage {get;set;}
    String retURL='';
    String saveURL='';
    Id id=null;
    Boolean isValidRecordType=false;
    
    public SMB_OpportunityValidateController()
    {
        id=ApexPages.currentPage().getParameters().get('oppId');        
        System.debug('PARAMTERS123->'+ApexPages.currentPage().getParameters());
        System.debug('ID------>'+id);
    }

    public SMB_OpportunityValidateController(ApexPages.StandardController controller) 
    {
        opp=(Opportunity )controller.getRecord();
         System.debug('PARAMTERS456->'+ApexPages.currentPage().getParameters());

        System.debug('OPPORUNITY->'+opp.id);
        if(null==opp.id)
            id=ApexPages.currentPage().getParameters().get('oppId');  
            
        System.debug('id->'+id);
        Integer recTypeCount=[Select count() from opportunity where id=:id and recordtype.developername like 'SMB_Care%'];
        if(recTypeCount>0)
            isValidRecordType=true;
        else
            isValidRecordType=false;      
    }
    public PageReference redirect()
    {
        if(null==id && null!=opp)
            id=opp.id;
        
        
        Integer count=[Select count() from Profile where name like '%Care%' and id=:UserInfo.getProfileId()];
        
        System.debug('count->'+count);
        PageReference pageRef=null;
        if(count==0 || isValidRecordType==false)
        {
                String retURL=ApexPages.currentPage().getParameters().get('retURL'); 
                System.debug('retURL->'+retURL); 
                pageRef=new PageReference('/'+id+'/e?nooverride=1&retURL='+retURL);        
                pageRef.getParameters().put('nonce', ApexPages.currentPage().getParameters().get('nonce')); 
                pageRef.getParameters().put('sfdcIFrameOrigin', ApexPages.currentPage().getParameters().get('sfdcIFrameOrigin')); 
        }
        else
        {
                errorMessage='You do not have permission to update Opportunity Record';
        }
        //PageReference pageRef = new PageReference('/'+ id);

/*    PageReference pageRef=null;
    retURL=ApexPages.currentPage().getParameters().get('retURL');
    saveURL=ApexPages.currentPage().getParameters().get('saveURL');
    String count=ApexPages.currentPage().getParameters().get('count');
        String nooverride=ApexPages.currentPage().getParameters().get('nooverride');
            System.debug('count->'+count);
    System.debug('nooverride->'+nooverride);
    System.debug('retURL->'+retURL);
    System.debug('saveURL->'+saveURL);

    if(String.isBlank(saveURL) && String.isBlank(count) )
    {
        System.debug('HERE->'+id);
//        pageRef=new PageReference('/'+opp.id+'/e?retURL='+retURL+'&saveURL=/'+ opp.id); 
        pageRef=new PageReference('/'+id+'/e?nooverride=1&saveURL='+EncodingUtil.urlEncode('/apex/SMB_OpportunityValidate?oppId='+id+'&count=1', 'UTF-8')); 
        //pageRef=new PageReference('/'+id+'/e?nooverride=1'); 
        pageRef.setRedirect(true);
        
    }
    else if(id<>null)
    {
       System.debug('THERE111->');
        pageRef=validate();

    }
    else
    {
        System.debug('THERE->');
        pageRef=validate();
    }   */

       return pageRef;
    }
    
    public PageReference nowRedirect()
    {
        System.debug('nowRedirect->'+id);
        PageReference pageRef = new PageReference('/'+ id);
        pageRef.getParameters().put('nonce', ApexPages.currentPage().getParameters().get('nonce')); 
        pageRef.getParameters().put('sfdcIFrameOrigin', ApexPages.currentPage().getParameters().get('sfdcIFrameOrigin')); 

        return pageRef ;
    }

    
    public PageReference validate()
    {
        String page='';
        Integer count=[Select count() from Profile where name like '%Care%' and id=:UserInfo.getProfileId()];
        System.debug('count->'+count);
            PageReference pageRef = new PageReference('/'+ id);
            pageRef.getParameters().put('nonce', ApexPages.currentPage().getParameters().get('nonce')); 
            pageRef.getParameters().put('sfdcIFrameOrigin', ApexPages.currentPage().getParameters().get('sfdcIFrameOrigin')); 
            //pageRef.setRedirect(true);
        if(count>0)
        {
            System.debug('HERE-->');
            return pageRef;
        }
        else
        {
            errorMessage='You do not have permission to update Opportunity Record';
                        System.debug('THERE-->');
            return null;
         }
    }

}