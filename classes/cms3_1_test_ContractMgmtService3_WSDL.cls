/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class cms3_1_test_ContractMgmtService3_WSDL {
 static testMethod void cms3_1ContractManagementSvc_3() {
     smb_test_utility.createCustomSettingData();
     SMB_Dummy1WebServiceResponse_Test.intializeMockWebservice();
    cms3_1ContractManagementSvc_3 smbCMSV = new cms3_1ContractManagementSvc_3();
    cms3_1ContractManagementSvc_3.ContractManagementSvcPort servicePort = new cms3_1ContractManagementSvc_3.ContractManagementSvcPort();
    cms3_1ContractManagementSvcTypes_v3.Contract contractcms = new cms3_1ContractManagementSvcTypes_v3.Contract();
    cms3_1ContractManagementSvcTypes_v3.ContractList contractlistcms = new cms3_1ContractManagementSvcTypes_v3.ContractList();
    cms3_1ContractManagementSvcTypes_v3.ContractRequest contractreqcms = new cms3_1ContractManagementSvcTypes_v3.ContractRequest();
    cms3_1ContractManagementSvcTypes_v3.LegacyContractIdList legacyContractIdListcms = new cms3_1ContractManagementSvcTypes_v3.LegacyContractIdList();
    cms3_1ContractManagementSvcTypes_v3.ContractDocument  contractdoc = new cms3_1ContractManagementSvcTypes_v3.ContractDocument();
    cms3_1ContractManagementSvcTypes_v3.ContractDocumentList contractdoclist = new cms3_1ContractManagementSvcTypes_v3.ContractDocumentList();
    cms3_1ContractManagementSvcReqRes_v3.TerminationChargesInformation termchargesInfo = new cms3_1ContractManagementSvcReqRes_v3.TerminationChargesInformation();
     list<cms3_1ContractManagementSvcReqRes_v3.TerminationChargesParameter> termchargesparam = new  list<cms3_1ContractManagementSvcReqRes_v3.TerminationChargesParameter>();
     list<cms3_1ContractManagementSvcTypes_v3.ContractAmendmentList> contractAmedlist = new list<cms3_1ContractManagementSvcTypes_v3.ContractAmendmentList>();
    cms3_1ContractManagementSvcReqRes_v3.ContractData contractDataCMS = new cms3_1ContractManagementSvcReqRes_v3.ContractData();
        Test.startTest();
     
     Boolean xyz = servicePort.verifyValidReplacement('123','12'); 
     String bcd = servicePort.ping(); 
     servicePort.getContract('487382');
     servicePort.replaceContractSubmission(contractreqcms,legacyContractIdListcms);
     servicePort.cancelContractSubmission('354','34245245'); 
     servicePort.triggerResendContract('54','7567','Test','Test','yes','31343');
     servicePort.findContractsByAgentId('3534');
     servicePort.getContractDocument('676767');
     servicePort.getContractDocumentMetaData('98989');
     servicePort.createContractSubmission(contractreqcms);
     servicePort.calculateTerminationCharges('7708',termchargesparam);
     servicePort.findContractAmendmentsByAssociateNum('H112',2);
     servicePort.findContractsByCustomerId('78978978');
     servicePort.findContractData('545456456778',TRUE,1,1);
     servicePort.updateContractSubmission('1','4','3453453');
        Test.stopTest();
     }
}