/**
 * @author Santosh Rath
 *
 *OrdrTnInventoryServiceTnListResponse will be:
 initialized by the getNpaNxxList method of the OrdrTnReservationImpl de-scribed in the previous  API section, and will be composed of the following: 
 ServiceAddress
expecting the provinceStateCode, municipalityName and COID attributes to be populated. 
 List of OrdrTnNpaNxx
The OrdrTnNpaNxxdomain object will be composed of the following attributes:
 String npa	(3 digits representing the area code)
 String nxx	(next 3 digits following the area code)
 String switchNumber (identifier of the switch)
 boolean default (identifies the OrdrTnNpaNxxwhich should be selected by default)

 and updated by the requestOrdrTnList method of the OrdrTnRe-servationImpl described in the previous  API section, enriched with the following: 
 OrdrTnNpaNxx
Selected OrdrTnNpaNxx; expecting the Npa and Nxx attributes to be populated. 
 String lineNumberPrefix: optional value passed for a vanity search using the first x digits (up to 4 digits) of a line number (i.e. the 7th, 8th, 9th and eventually 10th digit of a TN)
 List of LineNumber
The LineNumber domain object will be composed of the following attributes:
 String lineNumber:	representing the last 4 digits of a TN (7th to 10th digits)
 Enum status:		Available (by default),	Reserved
**/
public class OrdrTnInventoryServiceTnListResponse {
  
     private List<TELUS_TN_RESERVATION__c> tnrObjList;
	 private TELUS_TN_RESERVATION__c selectedTnrObj;
	 private List<OrdrTnNpaNxx> returnNpaNxxList;
	 private List<OrdrTnLineNumber> returnOrdrLineNumberList;
	 private OrdrTnNpaNxx returnSelectedOrdrTnNpaNxx;
	 private OrdrTnNpaNxx returnNpaNxxToUseForRequestingTns;
	 private OrdrTnNpaNxx returnDefaultNpaNxx;
	 private String returnLineNumberPrefix;
	 private TELUS_TN_RESERVATION__c defaultTnrObj;
	 private List<TELUS_TN_RESERVATION__c> tnrNpaNxxObjList;
	 private List<TELUS_TN_RESERVATION__c> tnrLineNumObjList;
	 private OrdrTnServiceAddress cachedServiceAddress;

	public OrdrTnServiceAddress getCachedServiceAddress() {
		return cachedServiceAddress;
	}

	public void setCachedServiceAddress(OrdrTnServiceAddress cachedServiceAddress) {
		this.cachedServiceAddress = cachedServiceAddress;
	}
	 
	public List<TELUS_TN_RESERVATION__c> getTnrNpaNxxObjList() {
		return tnrNpaNxxObjList;
	}
	public void setTnrNpaNxxObjList(List<TELUS_TN_RESERVATION__c> tnrNpaNxxObjList) {
		this.tnrNpaNxxObjList = tnrNpaNxxObjList;
	}
	public List<TELUS_TN_RESERVATION__c> getTnrLineNumObjList() {
		return tnrLineNumObjList;
	}
	public void setTnrLineNumObjList(List<TELUS_TN_RESERVATION__c> tnrLineNumObjList) {
		this.tnrLineNumObjList = tnrLineNumObjList;
	}
	 public TELUS_TN_RESERVATION__c getDefaultTnrObj() {
		return defaultTnrObj;
	}
	public void setDefaultTnrObj(TELUS_TN_RESERVATION__c defaultTnrObj) {
		this.defaultTnrObj = defaultTnrObj;
	}
	 public List<TELUS_TN_RESERVATION__c> getTnrObjList() {
		return tnrObjList;
	}
	public void setTnrObjList(List<TELUS_TN_RESERVATION__c> tnrObjList) {
		this.tnrObjList = tnrObjList;
	}
	public TELUS_TN_RESERVATION__c getSelectedTnrObj() {
		return selectedTnrObj;
	}
	public void setSelectedTnrObj(TELUS_TN_RESERVATION__c selectedTnrObj) {
		this.selectedTnrObj = selectedTnrObj;
	}

	public List<OrdrTnNpaNxx> getReturnNpaNxxList() {
		return returnNpaNxxList;
	}
	public void setReturnNpaNxxList(List<OrdrTnNpaNxx> returnNpaNxxList) {
		this.returnNpaNxxList = returnNpaNxxList;
	}
	public List<OrdrTnLineNumber> getReturnOrdrLineNumberList() {
		return returnOrdrLineNumberList;
	}
	public void setReturnOrdrLineNumberList(
			List<OrdrTnLineNumber> returnOrdrLineNumberList) {
		this.returnOrdrLineNumberList = returnOrdrLineNumberList;
	}
	public OrdrTnNpaNxx getReturnSelectedOrdrTnNpaNxx() {
		return returnSelectedOrdrTnNpaNxx;
	}
	public void setReturnSelectedOrdrTnNpaNxx(
			OrdrTnNpaNxx returnSelectedOrdrTnNpaNxx) {
		this.returnSelectedOrdrTnNpaNxx = returnSelectedOrdrTnNpaNxx;
	}
	public OrdrTnNpaNxx getReturnNpaNxxToUseForRequestingTns() {
		return returnNpaNxxToUseForRequestingTns;
	}
	public void setReturnNpaNxxToUseForRequestingTns(
			OrdrTnNpaNxx returnNpaNxxToUseForRequestingTns) {
		this.returnNpaNxxToUseForRequestingTns = returnNpaNxxToUseForRequestingTns;
	}
	public OrdrTnNpaNxx getReturnDefaultNpaNxx() {
		return returnDefaultNpaNxx;
	}
	public void setReturnDefaultNpaNxx(OrdrTnNpaNxx returnDefaultNpaNxx) {
		this.returnDefaultNpaNxx = returnDefaultNpaNxx;
	}
	public String getReturnLineNumberPrefix() {
		return returnLineNumberPrefix;
	}
	public void setReturnLineNumberPrefix(String returnLineNumberPrefix) {
		this.returnLineNumberPrefix = returnLineNumberPrefix;
	}
	// Criteria provided by the UI for retrieving native OrdrTnNpaNxxand Spare TNs from backend
	private OrdrTnServiceAddress serviceAddress = null;
	private OrdrTnNpaNxx selectedOrdrTnNpaNxx= null;
	private boolean overridedNativeNpaNxx= false;
	private String lineNumberPrefix = null;
	private String fmsId=null;
	private String lpdsId=null;
	private String provinceCode=null;
	private String reservationType=null;
	private String serviceAddressId=null;

	// SA COID's native OrdrTnNpaNxxlist and Spare TNs retrieved from backend
	private boolean defaultNpaNxxInitialised = false;
	private Map <String, OrdrTnNpaNxx> npaNxxMap = null;
	private Map <String, OrdrTnLineNumber> lineNumberList = null;
	
	// Error conditions encountered while retrieving native OrdrTnNpaNxxor Spare TNs from backend
	//private boolean noSpareTnForAllNativeNpaNxxList = false;
	//private boolean noSpareTnForSelectedNpaNxx= false;
	
	private Boolean noSpareTnForAllNativeNpaNxxFlag = false;
	private Boolean noSpareTnForSelectedNpaNxxFlag= false;
	
	public OrdrTnInventoryServiceTnListResponse() {
    }

    public OrdrTnInventoryServiceTnListResponse(
    		OrdrTnServiceAddress serviceAddress,
    		List<OrdrTnNpaNxx> npaNxxList, String fmsId,String lpdsId,String provinceCode,String reservationType ) {
		if (serviceAddress!=null){
			this.serviceAddress = serviceAddress;
		}
		if (npaNxxList!=null){
			this.setNpaNxxList(npaNxxList);
		}
		if (String.isNotBlank(fmsId)){
			this.fmsId=fmsId;
		}
		if (String.isNotBlank(lpdsId)){
			this.lpdsId=lpdsId;
		}
		if (String.isNotBlank(provinceCode)){
			this.provinceCode=provinceCode;
		}
		if (String.isNotBlank(reservationType)){
			this.reservationType=reservationType;
		}            
    }

		public String getFmsId() {
			return this.fmsId;
		}
		public void setFmsId(String fmsId) {
			this.fmsId=fmsId;
		}
		public String getReservationType() {
			return this.reservationType;
		}
		public void setReservationType(String reservationType) {
			this.reservationType=reservationType;
		}
		public String getProvinceCode() {
			return this.provinceCode;
		}
		public void setProvinceCode(String provinceCode) {
			this.provinceCode=provinceCode;
		}
		public String getLpdsId() {
			return this.lpdsId;
		}
		public void setLpdsId(String lpdsId) {
			this.lpdsId=lpdsId;
		}
	

	public OrdrTnServiceAddress getServiceAddress() {
		return serviceAddress;
	}
	
	public void setServiceAddress(OrdrTnServiceAddress serviceAddress) {
		this.serviceAddress = serviceAddress;
	}

	public String getServiceAddressId() {
		return serviceAddressId;
	}
	
	public void setServiceAddressId(String serviceAddressId) {
		this.serviceAddressId = serviceAddressId;
	}
		
	public boolean isOverridedNativeNpaNxx() {
		return overridedNativeNpaNxx;
	}


	public void setOverridedNativeNpaNxx(boolean overridedNativeNpaNxx) {
		this.overridedNativeNpaNxx= overridedNativeNpaNxx;
	}

	
	public boolean isDefaultNpaNxxInitialised() {
		return defaultNpaNxxInitialised;
	}

	
	public void setDefaultNpaNxxInitialised(boolean defaultNpaNxxInitialised) {
		this.defaultNpaNxxInitialised = defaultNpaNxxInitialised;
	}  	
	
	public OrdrTnNpaNxx getSelectedNpaNxx() {
		return selectedOrdrTnNpaNxx;
	}
	
	public void setSelectedNpaNxx(OrdrTnNpaNxx selectedNpaNxx) {
		this.selectedOrdrTnNpaNxx= selectedNpaNxx;
	}
	
	public String getLineNumberPrefix() {
		return lineNumberPrefix;
	}
	
	public void setLineNumberPrefix(String lineNumberPrefix) {
		this.lineNumberPrefix = lineNumberPrefix;
	}
	

	public List<OrdrTnLineNumber> getLineNumberList() {
		//returns the LineNumber list sorted by TN
		if (this.lineNumberList != null) {
			//Map <String, OrdrTnLineNumber> tm = new Map <String, OrdrTnLineNumber>( this.lineNumberList );
			return new List<OrdrTnLineNumber>(this.lineNumberList.values());
		}
		else
			return null;
	}

	public OrdrTnLineNumber getLineNumberFromList (String tn) {
		if ((tn != null) && (this.lineNumberList != null) && (this.lineNumberList.containsKey(tn))) {
			return (OrdrTnLineNumber)this.lineNumberList.get(tn);
		}
		else
			return null;
	}
	

	public void setLineNumberList(List<OrdrTnLineNumber> lineNumberList) {
		this.lineNumberList = new Map <String, OrdrTnLineNumber>();
		if (lineNumberList != null) {
			for(OrdrTnLineNumber tn : lineNumberList){
				if ((this.lineNumberList != null) && (tn != null) && (tn.getTenDigitsTn() != null) && (tn.getTenDigitsTn().length() == 10)) {
					if (!this.lineNumberList.containsKey(tn.getTenDigitsTn())) {
						this.lineNumberList.put(tn.getTenDigitsTn(), tn);
					}
				}
			}
		}
	}	
	
	public void addLineNumberToList (OrdrTnLineNumber tn) {
		if (this.lineNumberList == null) {
			this.lineNumberList = new Map <String, OrdrTnLineNumber>();
		}
		if ((this.lineNumberList != null) && (tn != null) && (tn.getTenDigitsTn() != null) && (tn.getTenDigitsTn().length() == 10)) {
			if (this.lineNumberList.containsKey(tn.getTenDigitsTn())) {
				lineNumberList.remove(tn.getTenDigitsTn());
			}
			lineNumberList.put(tn.getTenDigitsTn(), tn);
		}
	}	
	
	public void removeLineNumberFromList (String tn) {
		if ((this.lineNumberList != null) && (tn != null) && (this.lineNumberList.containsKey(tn))) {
			this.lineNumberList.remove(tn);
		}
	}

	public List<OrdrTnNpaNxx> getNpaNxxList() {
		//returns the OrdrTnNpaNxxlist sorted
		if (this.npaNxxMap != null) {
			//Map <String, OrdrTnNpaNxx> tm = new Map <String, OrdrTnNpaNxx>( this.npaNxxMap );
			return new List<OrdrTnNpaNxx>(npaNxxMap.values());
		}
		else
			return null;
	}

	public OrdrTnNpaNxx getNpaNxxFromList (String npa, String nxx) {
	   System.debug('check if the npa nxx map is populated and the map size is'+this.npaNxxMap.size());
		if ( String.isNotBlank(npa) &&  String.isNotBlank(nxx)  && (this.npaNxxMap != null) && (this.npaNxxMap.containsKey(npa + nxx))) {
			return (OrdrTnNpaNxx)this.npaNxxMap.get(npa + nxx);
		}
		else
			return null;
	}	
	
	
public void setNpaNxxList(List<OrdrTnNpaNxx> npaNxxList) {
	this.npaNxxMap =new Map<String , OrdrTnNpaNxx>();
	if(npaNxxList != null) {
			for(OrdrTnNpaNxx nxx : npaNxxList){
				if ((nxx != null) && (nxx.getSixDigitsNpaNxx() != null) && (nxx.getSixDigitsNpaNxx().length() > 2)) {
					if (!this.npaNxxMap.containsKey(nxx.getSixDigitsNpaNxx())) {
						this.npaNxxMap.put(nxx.getSixDigitsNpaNxx(), nxx);
					}
				}
			}
		}
	}


	
	public Boolean getNoSpareTnForAllNativeNpaNxxFlag() {
		return noSpareTnForAllNativeNpaNxxFlag;
	}
	public void setNoSpareTnForAllNativeNpaNxxFlag(
			Boolean noSpareTnForAllNativeNpaNxxFlag) {
		this.noSpareTnForAllNativeNpaNxxFlag = noSpareTnForAllNativeNpaNxxFlag;
	}
	public Boolean getNoSpareTnForSelectedNpaNxxFlag() {
		return noSpareTnForSelectedNpaNxxFlag;
	}
	public void setNoSpareTnForSelectedNpaNxxFlag(
			Boolean noSpareTnForSelectedNpaNxxFlag) {
		this.noSpareTnForSelectedNpaNxxFlag = noSpareTnForSelectedNpaNxxFlag;
	}
}