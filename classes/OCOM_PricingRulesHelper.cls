/**
Vlocity

Dec 21 2016

Helper class to execute pricing logic from apex 
- Combines OCOM_SetManualOverride and OCOM_SetOverRidePricesToZero logic to reduce pricing rule and the dml counts

*/

global with sharing class OCOM_PricingRulesHelper implements vlocity_cmt.VlocityOpenInterface {
  
    global Boolean invokeMethod(String methodName, Map<String, Object> input, Map<String, Object> output, Map<String, Object> options)  {
        
        if (methodName.equals('priceItems')){

            //System.debug('@@@@ OCOM_PricingRulesHelper invokeMethod priceItems' );
            
            Map<String, Object> filterEvaluatorOutput = (Map<String, Object>)input.get('filterEvaluatorOutput');
            Map<Id, Sobject> sobjectIdToSobject = (Map<Id, Sobject>)filterEvaluatorOutput.get('sobjectIdToSobject');
            List<Id> qualifiedObjectIds = new List<Id>((Set <Id>)input.get('qualifiedObjectIds'));
            Map<Id,OrderItem> postProcResultMap = new Map<Id,OrderItem>();

            List<OrderItem> orderItemToUpdate = getListToUpdate(sobjectIdToSobject, qualifiedObjectIds,postProcResultMap);
            if(orderItemToUpdate.size()>0){
                    update orderItemToUpdate;
            }
        }
        return true;
   }
   global static  String executionType = 'All'; 

   global static  List<OrderItem> getListToUpdate(Map<Id, Sobject>  sobjectIdToSobject, List<Id> qualifiedObjectIds, Map<Id,OrderItem> postProcResultMap)  {
        List<OrderItem> orderItemToUpdate = new List<OrderItem>();
        List<OrderItem>  manualOverrideList = new List<OrderItem>();
        Map<Id,OrderItem> overRidePricesToZeroMap = new Map<Id,OrderItem>();

         //System.debug('@@@@ OCOM_PricingRulesHelper getListToUpdate 1' );
         if(executionType == 'All' || executionType == 'OCOM_SetManualOverrideOnly'){
            manualOverrideList = OCOM_SetManualOverride(sobjectIdToSobject, qualifiedObjectIds,postProcResultMap);
         }
         //System.debug('@@@@ OCOM_PricingRulesHelper getListToUpdate 2 manualOverrideList.size() = ' + manualOverrideList.size());
         if(executionType == 'All' || executionType == 'OCOM_SetOverRidePricesToZeroOnly'){
            overRidePricesToZeroMap = OCOM_SetOverRidePricesToZero(sobjectIdToSobject, qualifiedObjectIds);
         }
         //System.debug('@@@@ OCOM_PricingRulesHelper getListToUpdate 3 overRidePricesToZeroMap.size() = ' + overRidePricesToZeroMap.size() );
         //System.debug('manualOverrideList____' + manualOverrideList);
         //System.debug('overRidePricesToZeroMap_____' + overRidePricesToZeroMap);

         // manual override > over ride prices to zero

         for(OrderItem oli :manualOverrideList){
            orderItemToUpdate.add(oli);
            if(overRidePricesToZeroMap.containsKey(oli.id)){
                // if contains add toUpdateList 
                // and then remove mapping from overRidePricesToZeroMap
                
                overRidePricesToZeroMap.remove(oli.id);
            }
         }

         for(OrderItem oli :overRidePricesToZeroMap.values()){
                orderItemToUpdate.add(oli);
         }
        System.debug('orderItemToUpdate___' +orderItemToUpdate);
       
         List<OrderItem> oliToUpdate = new List<OrderItem>();

        //################################################
        // go throuh otherOLIs and set priority 
        //################################################
        System.debug('postProcResultMap___' +postProcResultMap);
         for(OrderItem oli :orderItemToUpdate){
          oliToUpdate.add(oli);
          if(postProcResultMap.containsKey(oli.id)){
            // if contains add toUpdateList 
            // and then remove mapping from tempOliToUpdate

            postProcResultMap.remove(oli.id);
          }
         }
         for(OrderItem oli: postProcResultMap.values()){
           oliToUpdate.add(oli);
         }
             
        System.debug('oliToUpdate___' +oliToUpdate);

        return oliToUpdate;
   }


//#########################################
// OCOM_SetManualOverride logic
//#########################################   

    global static List<OrderItem>  OCOM_SetManualOverride(Map<Id, Sobject>  sobjectIdToSobject, List<Id> qualifiedObjectIds,Map<Id,OrderItem> postProcResultMap){
        Decimal nrcvalue = 0.00;
        Decimal mrcvalue = 0.00;
        set<string> rootProductItemIds = new set<string>();
        map<id,sobject> rootItTosObjectMap = new map<id,sObject>();
        map<string,Object> rootObjectMap = new map<string,Object>();
        List<Id> childOliToupdate = new list<Id>();
        map<id,id>oiToProdIDMap = new map<id,id>();
        map<id,id>prodToOiMap = new map<id,id>();
        Map<id,OrderItem> parentToWaiveOrderMap = new Map<Id, OrderItem>();

        //Map<String, Object> filterEvaluatorOutput = (Map<String, Object>)inputMap.get('filterEvaluatorOutput');
        //Map<Id, Sobject> sobjectIdToSobject = (Map<Id, Sobject>)filterEvaluatorOutput.get('sobjectIdToSobject');
        //List<Id> qualifiedObjectIds = new List<Id>((Set <Id>)inputMap.get('qualifiedObjectIds'));
        
        list<OrderItem> orderItemToUpdate = new list<OrderItem>();
        set<id> orderIDSet = new set<id>();
        Map<id,OrderItem> parentIdOliMap = new Map<id,OrderItem>();
        
     for(id objID: qualifiedObjectIds){
        if(sobjectIdToSobject.ContainsKey(objID)){
             orderItem oi = (OrderItem)sobjectIdToSobject.get(objID);
            String productCodeName =oi.PricebookEntry.product2.ProductCode;
            Map<String,Object> vlConfigData = new Map<String,Object>();
            if (oi.vlocity_cmt__Product2Id__r.VLAdditionalConfigData__c != null && oi.vlocity_cmt__Product2Id__r.VLAdditionalConfigData__c != '')
                vlConfigData = (Map<String,Object>)JSON.deserializeUntyped(oi.vlocity_cmt__Product2Id__r.VLAdditionalConfigData__c);
            if(vlConfigData != null && vlConfigData.size() > 0 && !vlConfigData.isEmpty() && vlConfigData.containsKey('Waive') && String.valueof(vlConfigData.get('Waive')).equalsIgnoreCase('WAIVE PROMO'))
        
          // if(oi.PricebookEntry.product2.Name != null && oi.PricebookEntry.product2.Name.startsWithIgnoreCase('Waive') && productCodeName != null && productCodeName.equalsIgnoreCase('WAIVE PROMO') )
            {
                parentToWaiveOrderMap.put(oi.vlocity_cmt__ParentItemId__c, oi);
               system.debug('>> Waive Product Added2- ______' + parentToWaiveOrderMap);
               
                 //system.debug('>> recurring charge ______' + oi.vlocity_cmt__RecurringTotal__c );
            }
           
          if(oi.vlocity_cmt__JSONAttribute__c != null){
             Map<String, Object> deserialized = (Map<String, Object>)JSON.deserializeUntyped(oi.vlocity_cmt__JSONAttribute__c);
                List<Object> attributesList = new List<Object>();
                List<Object> allAttributes = new List<Object>();
                List<Object> attrList = new List<Object>();
                Map<String, Object> attrMap = new Map<String, Object>();
                List<Map<String,Object>> attrMapList = new List<Map<String,Object>>();
                for(String cat : deserialized.keySet()){
                                  System.debug(cat);
                  attrList = (List<Object>)deserialized.get(cat);
                    System.debug(attrList);
                                }
                for(Object attrib :attrList) {
                    System.debug(attrib);
                     attrMap = (Map<String, Object>)attrib;
                    attrMapList.add(attrMap);
                }
                //SYstem.debug('###'+attrMapList);
                 for(Map<String,Object> test:attrMapList) {
                   
                        if(test.get('attributedisplayname__c')=='One-time Charges, NRC')
                        {
                            Map<String,Object> getValueNRC = (Map<String, Object>)test.get('attributeRunTimeInfo');
                            System.debug('NRCValue-->'+getValueNRC.get('value') );
                            //System.debug('OI NAME-->'+ oi.PriceBookEntry.product2.Id);
                            nrcvalue = getValueNRC.get('value')!= null? Decimal.valueof(String.Valueof(getValueNRC.get('value'))) : null;
                            if(nrcvalue != null  )
                            {   
                                if(oi.get('vlocity_cmt__OneTimeTotal__c') != null && oi.get('vlocity_cmt__OneTimeTotal__c') != nrcvalue){
                                    oi.put('vlocity_cmt__OneTimeTotal__c', nrcvalue);
                                    oi.put('vlocity_cmt__EffectiveOneTimeTotal__c',nrcvalue);
                                    orderIDSet.add(oi.id);
                                }
                                parentIdOliMap.put(oi.id,oi);
                                
                            }
                        }
                        
                        if(test.get('attributedisplayname__c')=='Recurring Charges, MRC')
                        {
                            Map<String,Object> getValueMRC = (Map<String, Object>)test.get('attributeRunTimeInfo');
                            System.debug('MRCValue-->'+getValueMRC.get('value'));
                            mrcvalue = getValueMRC.get('value')!= null?Decimal.valueof(String.ValueOf(getValueMRC.get('value'))):null;
                            if(mrcvalue != null ) {
                                if( oi.get('vlocity_cmt__RecurringTotal__c') != null && oi.get('vlocity_cmt__RecurringTotal__c') != mrcvalue){
                                    oi.put('vlocity_cmt__RecurringTotal__c',mrcvalue);
                                    oi.put('vlocity_cmt__EffectiveRecurringTotal__c',mrcvalue);
                                    orderIDSet.add(oi.id); 
                                }
                                parentIdOliMap.put(oi.id,oi);
                            }
                        }
                        
                }
                if(orderIDSet.Contains(oi.id) )
                         orderItemToUpdate.add(oi); 
            }
          }
        }

        if(postProcResultMap != null && postProcResultMap.size()>0 && !postProcResultMap.isEmpty()){
            for (id oriId : postProcResultMap.keySet()){
                  orderItem parentProd = postProcResultMap.get(oriId);
                if(parentToWaiveOrderMap != null && parentToWaiveOrderMap.size() > 0 && !parentToWaiveOrderMap.isEmpty()) {
                    if(parentProd.vlocity_cmt__OneTimeTotal__c != null && parentProd.vlocity_cmt__OneTimeTotal__c != 0){
                      if(parentToWaiveOrderMap.ContainsKey(parentProd.id) && !parentIdOliMap.containsKey(parentProd.id)){
                             orderItem wiaveOrderToUpdate =  parentToWaiveOrderMap.get(parentProd.id);

                               wiaveOrderToUpdate.vlocity_cmt__OneTimeTotal__c = (-1 * parentProd.vlocity_cmt__OneTimeTotal__c);
                              // wiaveOrderToUpdate.vlocity_cmt__OneTimeCharge__c = (-1 * parentProd.vlocity_cmt__OneTimeCharge__c);
                              // wiaveOrderToUpdate.vlocity_cmt__EffectiveOneTimeTotal__c = (-1 * parentProd.vlocity_cmt__EffectiveOneTimeTotal__c);
                               orderItemToUpdate.add(wiaveOrderToUpdate);
                               system.debug('Waive prod One time value_______' + wiaveOrderToUpdate.vlocity_cmt__OneTimeTotal__c);
                           // orderIDSet.add(oi.id);
                        }
                        else if(parentToWaiveOrderMap.ContainsKey(parentProd.id) && parentIdOliMap.containsKey(parentProd.id)){
                              orderItem wiaveOrderToUpdate =  parentToWaiveOrderMap.get(parentProd.id);
                               OrderItem parentOli = parentIdOliMap.get(parentProd.id);
                               wiaveOrderToUpdate.vlocity_cmt__OneTimeTotal__c = (-1 * parentOli.vlocity_cmt__OneTimeTotal__c);
                              // wiaveOrderToUpdate.vlocity_cmt__OneTimeCharge__c = (-1 * parentOli.vlocity_cmt__OneTimeCharge__c);
                              // wiaveOrderToUpdate.vlocity_cmt__EffectiveOneTimeTotal__c = (-1 * parentOli.vlocity_cmt__EffectiveOneTimeTotal__c);
                               orderItemToUpdate.add(wiaveOrderToUpdate);
                               //system.debug('Waive prod before commit_______' + wiaveOrderToUpdate.vlocity_cmt__OneTimeTotal__c);
                        }
                    } 
                }
            }
        }
        else {
            for (OrderItem parentProd: (List<OrderItem>)sobjectIdToSobject.values() ){
                 if(parentToWaiveOrderMap != null && parentToWaiveOrderMap.size() > 0 && !parentToWaiveOrderMap.isEmpty() )   { 
                        if(parentProd.vlocity_cmt__OneTimeTotal__c != null && parentProd.vlocity_cmt__OneTimeTotal__c != 0){
                              if(parentToWaiveOrderMap.ContainsKey(parentProd.id) && !parentIdOliMap.containsKey(parentProd.id)){
                                     orderItem wiaveOrderToUpdate =  parentToWaiveOrderMap.get(parentProd.id);

                                       wiaveOrderToUpdate.vlocity_cmt__OneTimeTotal__c = (-1 * parentProd.vlocity_cmt__OneTimeTotal__c);
                                       //wiaveOrderToUpdate.vlocity_cmt__OneTimeCharge__c = (-1 * parentProd.vlocity_cmt__OneTimeCharge__c);
                                       //wiaveOrderToUpdate.vlocity_cmt__EffectiveOneTimeTotal__c = (-1 * parentProd.vlocity_cmt__EffectiveOneTimeTotal__c);
                                       orderItemToUpdate.add(wiaveOrderToUpdate);
                                       system.debug('Waive prod One time value_______' + wiaveOrderToUpdate.vlocity_cmt__OneTimeTotal__c);
                                   // orderIDSet.add(oi.id);
                                  }
                                  else if(parentToWaiveOrderMap.ContainsKey(parentProd.id) && parentIdOliMap.containsKey(parentProd.id)){
                                      orderItem wiaveOrderToUpdate =  parentToWaiveOrderMap.get(parentProd.id);
                                       OrderItem parentOli = parentIdOliMap.get(parentProd.id);
                                       wiaveOrderToUpdate.vlocity_cmt__OneTimeTotal__c = (-1 * parentOli.vlocity_cmt__OneTimeTotal__c);
                                       //wiaveOrderToUpdate.vlocity_cmt__OneTimeCharge__c = (-1 * parentOli.vlocity_cmt__OneTimeCharge__c);
                                      // wiaveOrderToUpdate.vlocity_cmt__EffectiveOneTimeTotal__c = (-1 * parentOli.vlocity_cmt__EffectiveOneTimeTotal__c);
                                       orderItemToUpdate.add(wiaveOrderToUpdate);
                                       //system.debug('Waive prod before commit_______' + wiaveOrderToUpdate.vlocity_cmt__OneTimeTotal__c);
                                  }
                        } 
                    }
                }
        }

        System.debug('orderItemToUpdate___' + orderItemToUpdate.size());
        return orderItemToUpdate;         
    }

//#########################################
// OCOM_SetOverRidePricesToZero logic 
//#########################################

    global static Map<Id,OrderItem>   OCOM_SetOverRidePricesToZero(Map<Id, Sobject>  sobjectIdToSobject, List<Id> qualifiedObjectIds){
        
             
        set<string> rootProductItemIds = new set<string>();
        set<string> rootProductItemIds1 = new set<string>();
         //Map<id,id> OliTOrootProductItemIds = new Map<id,id>;
        map<id,sobject> rootItTosObjectMap = new map<id,sObject>();
        map<string,Object> rootObjectMap = new map<string,Object>();
        Set<Id> childOliToupdate = new Set<Id>();
        Map<Id,Set<Id>> parentToChildProdMap = new Map<Id,Set<Id>>();
        map<id,id>oiToProdIDMap = new map<id,id>();
        map<id,id>prodToOiMap = new map<id,id>();
        string rootItemId1;
        Map <Id, OrderItem> mapToUpdates = new Map <Id, OrderItem> ();
        
         system.debug('qualifiedObjectIds______' +qualifiedObjectIds);
        // here 
        Map<Id,Id> rootIdTooverilineIdMap = new Map<Id,Id>();
        if(qualifiedObjectIds !=null && qualifiedObjectIds.size() > 0)
        {
            //Added by jaya to get the root item id
            Sobject objectSO1 = sobjectIdToSobject.get(qualifiedObjectIds[0]);
            //string rootItemId = String.Valueof(objectSO1.get('vlocity_cmt__ParentItemId__c'));
             rootItemId1 = String.ValueOf(objectSO1.get('vlocity_cmt__RootItemId__c'));
             system.debug('**rootItemId1'+rootItemId1); //Order product 80222000000HQSWAA4
             system.debug('sobjectIdToSobject***'+sobjectIdToSobject); //Order Product Id to Order Product.
            
             List<OrderItem> rootOrderItemList = [select Id, PricebookEntry.UnitPrice,
                                    PricebookEntry.vlocity_cmt__RecurringPrice__c,
                                    PricebookEntry.product2.ProductCode,
                                    PricebookEntry.product2.Name,
                                    PricebookEntry.product2.Id,
                                    PricebookEntry.Product2.orderMgmtId__c ,
                                    vlocity_cmt__Product2Id__c,
                                    vlocity_cmt__ParentItemId__c,
                                    Quantity, OfferName__c, OrderId,
                                    vlocity_cmt__JSONAttribute__c,
                                    vlocity_cmt__LineNumber__c,
                                    vlocity_cmt__OneTimeTotal__c,
                                    vlocity_cmt__RecurringTotal__c,
                                    vlocity_cmt__EffectiveOneTimeTotal__c,
                                    vlocity_cmt__EffectiveRecurringTotal__c,
                                    vlocity_cmt__RootItemId__c,
                                    Order.RateBand__c,
                                    Order.Rate_Sub_Band__c,
                                    Order.Market__c, Order.Forbone__c,
                                    Order.Forbearance__c,
                                    vlocity_cmt__Product2Id__r.VLAdditionalConfigData__c,
                                    vlocity_cmt__OneTimeManualDiscount__c,
                                    vlocity_cmt__RecurringManualDiscount__c from OrderItem where Id = :rootItemId1];
             
             if( rootOrderItemList != null && rootOrderItemList.size() > 0){
                    OrderItem rootOrderItem = rootOrderItemList[0];
                    sobjectIdToSobject.put(rootItemId1,rootOrderItem);
                    qualifiedObjectIds.add(rootItemId1);
                 }
             //oiToProdIDMap.put(rootOrderItem.id,rootOrderItem.PriceBookEntry.product2.Id);
             //System.assertEquals(rootItemId1,'abc');
             //prodToOiMap.put(rootOrderItem.PriceBookEntry.product2.Id,rootOrderItem.id);
             /*if(rootOrderItem.PriceBookEntry.product2.Name != null && rootOrderItem.PriceBookEntry.product2.Name.containsIgnoreCase('Overline') ){
                rootIdTooverilineIdMap.put(rootOrderItem.vlocity_cmt__RootItemId__c,rootOrderItem.PriceBookEntry.product2.id);
            }*/

        for (orderItem oi: (List<OrderItem>)sobjectIdToSobject.values()){
            oiToProdIDMap.put(oi.id,oi.PriceBookEntry.product2.Id);
            System.debug('>>>> OliID' + oi.id);
            prodToOiMap.put(oi.PriceBookEntry.product2.Id,oi.id);
            if(oi.PriceBookEntry.product2.Name != null && oi.PriceBookEntry.product2.Name.containsIgnoreCase('Overline') ){
                rootIdTooverilineIdMap.put(oi.vlocity_cmt__RootItemId__c,oi.PriceBookEntry.product2.id);
            }
        }
        
        
       /* String Product2Id = [select Id from Product2 where ]
        oiToProdIDMap.put(rootItemId1,);*/
            
        for(Integer i = 0; i < qualifiedObjectIds.size(); i++){
            Sobject objectSO = sobjectIdToSobject.get(qualifiedObjectIds[i]);
            String parentItemId = String.Valueof(objectSO.get('vlocity_cmt__ParentItemId__c'));

             sObject rootObject = sobjectIdToSobject.get(parentItemId);
             sObject rootObject1 = sobjectIdToSobject.get(rootItemId1);
             System.debug('**rootObject1'+rootObject1);
             String currentOliID = String.Valueof(objectSO.get('Id'));
             Map<String,Object> prceBookEntry;
             if(!rootItTosObjectMap.containskey(parentItemId)){
                //system.debug('rootItemId_____'+rootItemId);
                rootItTosObjectMap.put(parentItemId,rootObject);
              
                prceBookEntry = (map<string,Object>)rootObjectMap.get('PricebookEntry');
                if(oiToProdIDMap.ContainsKey(parentItemId)){
                        rootProductItemIds.add(String.valueOf(oiToProdIDMap.get(parentItemId)));
                        //OliTOrootProductItemIds.put(currentOliID,String.valueOf(oiToProdIDMap.get(rootItemId)));
                    }
                if(rootIdTooverilineIdMap != null && !rootIdTooverilineIdMap.isEmpty() && rootIdTooverilineIdMap.size() > 0 && rootIdTooverilineIdMap.containsKey(parentItemId))  {
                    rootProductItemIds.add(String.valueOf(rootIdTooverilineIdMap.get(parentItemId)));
                }  
             }
             
             //Added by Jaya
             if(!rootItTosObjectMap.containskey(rootItemId1)){
                rootItTosObjectMap.put(rootItemId1,rootObject1);
                if(oiToProdIDMap.ContainsKey(rootItemId1)){
                        rootProductItemIds1.add(String.valueOf(oiToProdIDMap.get(rootItemId1)));
                        //OliTOrootProductItemIds.put(currentOliID,String.valueOf(oiToProdIDMap.get(rootItemId)));
                    }
                if(rootIdTooverilineIdMap != null && !rootIdTooverilineIdMap.isEmpty() && rootIdTooverilineIdMap.size() > 0 && rootIdTooverilineIdMap.containsKey(rootItemId1))  {
                    rootProductItemIds1.add(String.valueOf(rootIdTooverilineIdMap.get(rootItemId1)));
                }  
             }
             
         }
         /*OrderItem rootOrderItem = [select Id, PriceBookEntry.product2.Id from OrderItem where Id = :rootItemId1];
         oiToProdIDMap.put(rootItemId1,rootOrderItem.PriceBookEntry.product2.Id);*/
         //System.assertEquals(rootProductItemIds,null);
        system.debug('List of RootProdct Items ID ---> ' + rootProductItemIds );
        system.debug('List of RootProdct Items ID1 ---> ' + rootProductItemIds1 );
        //System.assertEquals(rootProductItemIds,null);
        List<String> combinedList = new List<String>();
        combinedList.addAll(rootProductItemIds);
        combinedList.addAll(rootProductItemIds1);
        
        if(combinedList != null && combinedList.size() > 0 && !combinedList.isEmpty() ){
                 for(vlocity_cmt__ProductChildItem__c pci: [select id,name,OCOMOverrideType__c,
                                                            vlocity_cmt__ParentProductId__c,vlocity_cmt__ParentProductId__r.name,
                                                            vlocity_cmt__ChildProductId__c 
                                                            from vlocity_cmt__ProductChildItem__c 
                                                            where vlocity_cmt__ParentProductId__r.id in :combinedList]){
                        if(pci.OCOMOverrideType__c == 'Override Price'){
                            
                            if(parentToChildProdMap != null && parentToChildProdMap.containsKey(pci.vlocity_cmt__ParentProductId__c) ){
                                parentToChildProdMap.get(pci.vlocity_cmt__ParentProductId__c).add(pci.vlocity_cmt__ChildProductId__c);
                            }
                            else if(!parentToChildProdMap.containsKey(pci.vlocity_cmt__ParentProductId__c))  {
                              parentToChildProdMap.put(pci.vlocity_cmt__ParentProductId__c,(new Set<Id>{pci.vlocity_cmt__ChildProductId__c}));
                            }
                            childOliToupdate.add(pci.vlocity_cmt__ChildProductId__c);
                            
                        }
                }
        }
        System.debug('childOliToupdate**'+childOliToupdate);
        
        System.debug('oiToProdIDMap'+oiToProdIDMap);
        String rootItemIdInsideFor;
        for(id orderitemID : oiToProdIDMap.keySet()){           
                id product2ID = oiToProdIDMap.get(orderitemID);
                 //System.debug('childOliToupdate____' +childOliToupdate);
               
            if(childOliToupdate.contains(product2ID) && sobjectIdToSobject.Containskey(orderitemID)){
                Sobject objectSO2 = sobjectIdToSobject.get(orderitemID);
                system.debug('objectSO2**'+objectSO2);
                //String rootItemId = String.Valueof(objectSO2.get('vlocity_cmt__RootItemId__c'));
                //rootItemIdInsideFor = String.Valueof(objectSO2.get('vlocity_cmt__RootItemId__c'));
                String parentItemId = String.Valueof(objectSO2.get('vlocity_cmt__ParentItemId__c'));
                /*if( parentItemId != rootItemId && rootIdTooverilineIdMap.containsKey(rootItemId) && parentToChildProdMap != null && parentToChildProdMap.containsKey(String.valueOf(rootIdTooverilineIdMap.get(rootItemId)))){
                    set<Id> childOverLinePciProdIdSet = parentToChildProdMap.get(String.valueOf(rootIdTooverilineIdMap.get(rootItemId)));
                        if(childOverLinePciProdIdSet != null && childOverLinePciProdIdSet.size() > 0 && !childOverLinePciProdIdSet.isEmpty() && childOverLinePciProdIdSet.contains(product2ID)){
                             system.debug('product2ID-Overline____' + product2ID);
                            //system.debug('parentProductID____' +String.valueOf(rootIdTooverilineIdMap.get(rootItemId)));
                            objectSO2.put('vlocity_cmt__OneTimeTotal__c', 0.0);
                            objectSO2.put('vlocity_cmt__RecurringTotal__c', 0.0);
                            objectSO2.put('vlocity_cmt__OneTimeCharge__c', 0.0);
                            objectSO2.put('vlocity_cmt__RecurringCharge__c', 0.0);
                            objectSO2.put('vlocity_cmt__EffectiveOneTimeTotal__c',0.0);
                            objectSO2.put('vlocity_cmt__EffectiveRecurringTotal__c',0.0); 
                            OrderItem oli = (OrderItem)objectSO2;
                            mapToUpdates.put(oli.id, oli);
                        }
                }*/
                if( parentItemId != rootItemId1 && oiToProdIDMap != null && oiToProdIDMap.containsKey(parentItemId) && parentToChildProdMap != null && parentToChildProdMap.containsKey(String.valueOf(oiToProdIDMap.get(parentItemId)))){
                  system.debug('ParentItemID ----' + parentItemId);
                  system.debug('***parentToChildProdMap'+parentToChildProdMap);
                  system.debug('***String.valueOf(oiToProdIDMap.get(parentItemId))'+String.valueOf(oiToProdIDMap.get(parentItemId)));
                    Set<Id> childPciProdIdSet = parentToChildProdMap.get(String.valueOf(oiToProdIDMap.get(parentItemId)));
                    //Added by Jaya
                    //childPciProdIdSet.addAll(parentToChildProdMap.get(String.valueOf(oiToProdIDMap.get(rootItemId))));
                   // Set<Id> childPciProdIdSet2 = parentToChildProdMap.get(String.valueOf(oiToProdIDMap.get(rootItemId)));
                    System.debug('childPciProdIdSet_____' + childPciProdIdSet);
                        if(childPciProdIdSet != null && childPciProdIdSet.size() > 0 && !childPciProdIdSet.isEmpty() && childPciProdIdSet.contains(product2ID)){
                             //system.debug('product2ID-Overline____' + product2ID);
                            //system.debug('parentProductID____' +String.valueOf(rootIdTooverilineIdMap.get(rootItemId)));
                            objectSO2.put('vlocity_cmt__OneTimeTotal__c', 0.0);
                            objectSO2.put('vlocity_cmt__RecurringTotal__c', 0.0);
                            objectSO2.put('vlocity_cmt__OneTimeCharge__c', 0.0);
                            objectSO2.put('vlocity_cmt__RecurringCharge__c', 0.0);
                            objectSO2.put('vlocity_cmt__EffectiveOneTimeTotal__c',0.0);
                            objectSO2.put('vlocity_cmt__EffectiveRecurringTotal__c',0.0); 
                            OrderItem oli = (OrderItem)objectSO2;
                            mapToUpdates.put(oli.id, oli);
                            System.debug(mapToUpdates);
                        }
                        
                }

                else if( parentItemId == rootItemId1 && oiToProdIDMap.containsKey(rootItemId1) && parentToChildProdMap != null && parentToChildProdMap.containsKey(String.valueOf(oiToProdIDMap.get(rootItemId1)))){
                    set<Id> childPciProdIdSet = parentToChildProdMap.get(String.valueOf(oiToProdIDMap.get(rootItemId1)));
                    if(childPciProdIdSet != null && childPciProdIdSet.size() > 0 && !childPciProdIdSet.isEmpty() && childPciProdIdSet.contains(product2ID)){
                        system.debug('product2ID_____' + product2ID);
                        // system.debug(LoggingLevel.ERROR,'objectSO2:::::' + objectSO2);
                        objectSO2.put('vlocity_cmt__OneTimeTotal__c', 0.0);
                        objectSO2.put('vlocity_cmt__RecurringTotal__c', 0.0);
                        objectSO2.put('vlocity_cmt__OneTimeCharge__c', 0.0);
                        objectSO2.put('vlocity_cmt__RecurringCharge__c', 0.0);
                        objectSO2.put('vlocity_cmt__EffectiveOneTimeTotal__c',0.0);
                        objectSO2.put('vlocity_cmt__EffectiveRecurringTotal__c',0.0); 
                        OrderItem oli = (OrderItem)objectSO2;
                        mapToUpdates.put(oli.id, oli);
                    }
                } 
                
                
                      
            } 
        }
        /*OrderItem rootOI = [select Id, vlocity_cmt__OneTimeTotal__c, vlocity_cmt__RecurringTotal__c, vlocity_cmt__OneTimeCharge__c, vlocity_cmt__RecurringCharge__c, vlocity_cmt__EffectiveOneTimeTotal__c, vlocity_cmt__EffectiveRecurringTotal__c from OrderItem where Id = :rootItemIdInsideFor];
        
        rootOI.vlocity_cmt__OneTimeTotal__c = 0.0;
        rootOI.vlocity_cmt__RecurringTotal__c = 0.0;
                        rootOI.vlocity_cmt__OneTimeCharge__c = 0.0;
                        rootOI.vlocity_cmt__RecurringCharge__c = 0.0;
                        rootOI.vlocity_cmt__EffectiveOneTimeTotal__c = 0.0;
                        rootOI.vlocity_cmt__EffectiveRecurringTotal__c = 0.0;
                        
                        update rootOI;
                        system.debug('rootOI**'+rootOI);*/
        
        system.debug('mapToUpdates___' +mapToUpdates.keySet());
        system.debug('mapToUpdatesValueSet___' +mapToUpdates.Values());
        }
        return mapToUpdates;         
    }


}