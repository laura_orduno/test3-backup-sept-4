/*
###########################################################################
# File..................: GlobysBillAnalyzerHistoryChartController.cls
# Created by............: Sandip Chaudhari (IBM)
# Created Date..........: 12-Aug-2016
# Description...........: This Class is used to get report data from globys. This report data wil be used in VF page to show chart using javascript
#                         It also contains the method to parse the resoponse.
# 
# *********** VERSION UPDATES ***********************************************************************
#
# 12-Aug-2016    Sandip Chaudhari      Created File
#
*/

public class GlobysBillAnalyzerHistoryChartController{
   public String globysPortalURL {get;set;} // Link stored in this field will be used to navigate to Globys system using SSO-SAML
   private static String reportId; // To identity which globys report you are trying to run with reportClass and reportId, this is one of the parameter to REST service
   private static String reportClass; // To identity which globys report you are trying to run with reportClass and reportId, this is one of the parameter to REST service
   private static String tokenURL; // URL to invoke REST service to get Access Token
   private static String reportURL; // URL to invoke REST service to get report Data
   private static String serverAccessToken; // Server Access Token for Globys system
   public String defaultFillColor{get;set;}
   //public String wirelineFillColor{get;set;}
   //public String wirelessFillColor{get;set;}
   public boolean isFrenchUser{get;set;} // check if user locale setting is French
   public String borderColor{get;set;} // border color of the bar chart, fetched from custom setting
   public Map<String, GlobysSourceMapping__c> globysSourceMap{get;set;}
   public String mapGlobysSourceJSON {get{return JSON.serialize(globysSourceMap);}}
   
   /* Constructor
   */
   public GlobysBillAnalyzerHistoryChartController(){
       if (UserInfo.getLocale().startsWith('fr')){
           isFrenchUser = true;
       }else{
           isFrenchUser = false;
       }
       // Following code will fetch link which will be used to navigate to Globys system using SSO-SAML
       GlobysConfig__c globysObj = GlobysConfig__c.getInstance('GlobysPortalURL');
       if(globysObj != null){
           globysPortalURL = globysObj.value__c;
       }
       
       // Following code will fetch configuration from custom setting
       globysObj = GlobysConfig__c.getInstance('defaultFillColor');
       if(globysObj != null){
           defaultFillColor = globysObj.value__c;
       }
     
       globysObj = GlobysConfig__c.getInstance('BorderColor');
       if(globysObj != null){
           borderColor = globysObj.value__c;
       }
       
       globysSourceMap = GlobysSourceMapping__c.getAll();
   }

   /* Name - getAccessToken
    * Description - Get access Token from Globys system
    * Param - 
    * Return type - void
    */
   public static boolean getAccessToken(AccountDetails accDtls) {
        Http httpObj = new Http();
        
        // Get an access token for TELUS services ...
        HttpRequest req = new HttpRequest();
        //req.setEndpoint('https://telus1012.uat.globysonline.com/auth/oidc/connect/token');
        system.debug('@@@tokenURL ' + tokenURL);
        req.setEndpoint(tokenURL);
        req.setMethod('POST');
        req.setBody('grant_type=client_credentials&client_id=telus.services&client_secret=this_is_the_secret&scope=StatementReports');
        HttpResponse res = httpObj.send(req);
        system.debug('@@@ Access Token Res ' + res );
        if(res.getStatusCode() != 200){
            logGlobysError(accDtls.selectedAccountId, String.valueOf(res), String.valueOf(res), 'GlobysBillAnalyzerHistoryChartController.getAccessToken');
            accDtls.customMessage = System.Label.GlobysChartExceptionMessage;
            return false;
        }
        String serverTokenJson = res.getBody();
        
        Integer serverExpiry;
        JSONParser parser = JSON.createParser(serverTokenJson);
        while (parser.nextToken() != null) {
          if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'access_token')) {
            parser.nextToken();
            serverAccessToken = parser.getText();
          }
        }
        return true;
   }
   
   /* Name - doReportDataCall
    * Description - Get report data from Globys system
    * Param - cbucid, pcan, username
    * Return type - String
    */
   public static void doReportDataCall(AccountDetails accDtls) {
        String username = UserInfo.getUserName();
        //cbucid = '0002026881';
        //billingNumber = '';
        //reportId = '1363';
        Http httpObj = new Http();     
        // Get the chart data ...
        HttpRequest req = new HttpRequest();
        //req.setEndpoint('https://telus1012.uat.globysonline.com/reports/v1/execute');
        req.setEndpoint(reportURL);
        req.setMethod('POST');
        req.setHeader('Authorization', 'Bearer ' + serverAccessToken);
        req.setHeader('X-Account', accDtls.cbucid);
        req.setHeader('X-On-Behalf-Of', username);
        req.setHeader('Content-Type', 'application/json');
        //req.setBody('{"class":"' + reportClass + '", "id": ' + string.valueOf(reportId) + ', "filter": { "type": "Account", "accounts": ["' + billingNumber + '"] }}');
        String requestProperties = '"class":"' + reportClass + '", "id": ' + string.valueOf(reportId);
        if (accDtls.billingNumber != null && accDtls.billingNumber.length() > 0) {
          requestProperties += ', "filter": { "type": "Account", "accounts": ["' + accDtls.billingNumber + '"] }';
        }
        system.debug('@@@billingNumber ' + accDtls.billingNumber );
        system.debug('@@@req ' + accDtls.billingNumber + ' ' + requestProperties);
        req.setBody('{' + requestProperties + '}');
        HttpResponse res = httpObj.send(req);
        //system.debug('http res' + res);
        if(res.getStatusCode() != 200){
            system.debug('@@@@ http res' + res);
            accDtls.chartData = '';
            //system.debug('@@@@ body ' + accDtls.chartData);
            if(res.getStatusCode() != 400){
                logGlobysError(accDtls.selectedAccountId, String.valueOf(res), String.valueOf(res), 'GlobysBillAnalyzerHistoryChartController.doReportDataCall');
                accDtls.customMessage = System.Label.GlobysChartExceptionMessage;
            }else{
                accDtls.chartData = res.getBody();
                accDtls.customMessage = System.Label.GlobysChartDataNotAvailable;
            }
        }else{
            accDtls.chartData = res.getBody();
        }
        //system.debug('chartData ' + chartData);
    }
    
    
    /* Name - getConfig
    * Description - get configuration from custom setting GlobysConfig__c
    * Param - 
    * Return type - void
    */
    public static void getConfig(){
        Map<String, GlobysConfig__c> globysConfigObj = GlobysConfig__c.getAll();
        if(globysConfigObj != null){
            GlobysConfig__c globysObj = globysConfigObj.get('ReportId');
            reportId = globysObj.value__c;
            globysObj = globysConfigObj.get('ReportClass');
            reportClass = globysObj.value__c;
            globysObj = globysConfigObj.get('TokenURL');
            tokenURL = globysObj.value__c;
            globysObj = globysConfigObj.get('ReportURL');
            reportURL = globysObj.value__c;
        }
    }
    
    /* Name - initChartReportService
    * Description - Initiate the process to invoke REST services to get report data
    * Param - accountId, devRecordType, pcan
    * Return type - String
    */
    @RemoteAction
    public static AccountDetails initChartReportService(String accId, String recType) {
       AccountDetails accDtls = new AccountDetails();
       accDtls.selectedAccountId = accId;
       accDtls.recType = recType;
       try{
            getConfig(); // get Configuration
            Boolean isAccessTokenRecieved = getAccessToken(accDtls);  // get access token
            if(isAccessTokenRecieved){
                getAccountDetails(accDtls); // get account details like rcid, billingnumber and cbucid
                doReportDataCall(accDtls);  // get report data
            }
       }catch(Exception ex){
            // log the Error
            accDtls.chartData = '';
            accDtls.customMessage = System.Label.GlobysChartExceptionMessage;
            logGlobysError(accId, ex.getmessage(), ex.getstacktracestring(), 'GlobysBillAnalyzerHistoryChartController.initChartReportService');
       }
        return accDtls;
    }
    
    /* Name - getcbucidNumber
    * Description - get cbucid, rcid, billingNumber from account hirerchy
    * Param - accId, recType
    * Return type - String
    */
    public static void getAccountDetails(AccountDetails accDtls) {
            account accObj;
            if(accDtls.recType == 'CBUCID'){
                accObj = [SELECT CBUCID_RCID__c FROM ACCOUNT WHERE ID =: accDtls.selectedAccountId];
                if(accObj != null){
                    accDtls.cbucid = accObj.CBUCID_RCID__c;
                }
            }else if(accDtls.recType == 'RCID'){
                accObj = [SELECT Parent.CBUCID_RCID__c, RCID__c FROM ACCOUNT WHERE ID =: accDtls.selectedAccountId  AND ParentId != null];
                if(accObj != null){
                    accDtls.cbucid = accObj.Parent.CBUCID_RCID__c;
                    accDtls.rcid = accObj.RCID__c;
                } 
            }else{
               accObj = [SELECT Parent.CBUCID_RCID__c, BCAN__c, BAN_CAN__c FROM ACCOUNT WHERE ID =: accDtls.selectedAccountId AND ParentId != null];
               if(accObj != null){
                    if(accObj.BCAN__c != null){
                        accDtls.billingNumber = accObj.BCAN__c;
                    }else{
                        accDtls.billingNumber = accObj.BAN_CAN__c;
                    }
               }
 
               system.debug('@@@getbillingNumber ' + accDtls.billingNumber );
 
               set<ID> accIds = new set<ID>();
               accIds.add(accDtls.selectedAccountId);
               map<Id,Id> accMap = smb_AccountUtility.getRcidAccountId(accIds);
               if(accMap != null){
                    if(accMap.get(accDtls.selectedAccountId)!= null){
                       Id rcid = accMap.get(accDtls.selectedAccountId);
                       accObj = [SELECT Parent.CBUCID_RCID__c, BCAN__c, BAN_CAN__c FROM ACCOUNT WHERE ID =: rcid AND ParentId != null];
                       if(accObj != null){
                            accDtls.cbucid = accObj.Parent.CBUCID_RCID__c;
                       }
                    }
                }
            }

            /*
            if(accDtls.cbucid == ''){
                accObj = [SELECT CBUCID_RCID__c FROM ACCOUNT WHERE ID =: accId];
                if(accObj != null){
                    accDtls.cbucid = accObj.CBUCID_RCID__c;
                }
            }
            */
            //return cbucid;
    }
   
   @RemoteAction
   public static void logGlobysError(String sfdcRecId, String errMsg, String stacktrace, String methodName){
         webservice_integration_error_log__c errorLog = new webservice_integration_error_log__c
                                                    (apex_class_and_method__c= methodName,
                                                     external_system_name__c='Globys Bill Analyzer',
                                                     webservice_name__c='Globys Report Chart',
                                                     object_name__c='Account',
                                                     sfdc_record_id__c=sfdcRecId,
                                                     Error_Code_and_Message__c = errMsg,
                                                     Stack_Trace__c = stacktrace
                                                     );
         insert errorLog;
    }
    // Sub class to return details to VF page
    public class AccountDetails {
        String selectedAccountId {get;set;}
        String cbucid {get;set;}
        String rcid {get;set;}
        String billingNumber{get;set;}
        String chartData{get;set;}
        String customMessage{get;set;}
        String recType{get;set;}
        public AccountDetails (){
            selectedAccountId = '';
            cbucid = '';
            rcid = '';
            billingNumber = '';
            chartData = '';
            customMessage = '';
            recType = '';
        }
    }
    
    
}