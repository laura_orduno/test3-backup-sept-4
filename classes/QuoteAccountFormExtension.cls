/*
    Quote Account Form Extension
    QuoteAccountFormExtension.cls
    Part of the TELUS QuoteQuickly Portal
    
    Author: Alex Miller @ Traction <amiller@tractionondemand.com>
    Created: March 5, 2012
    
    Since: Release 5
    Initial Definition: R5rq10
    
    Allows you to control the 'requireness' of fields on the Account Form component.
    This is used in conjunction with validation in QuoteDetailController.cls to meet R5rq10

    Page Parameters:
     - Name -        - Type -        - Description -                         - Since -
    stage           String          Quote stage as in SBQQ__Status__c on    March 5, 2012
                                     SBQQ__Quote__c
    qid             String          Quote ID of the current quote           March 5, 2012

    Properties:
     - Method -          - Returns -     - Description -                     - Since -
    rDraft              Boolean         True when quote is at or beyond     March 5, 2012
                                         the draft stage
    rCreditCheck        Boolean         True when quote is at or beyond     March 5, 2012
                                         the credit check stage
    rAgreement          Boolean         True when quote is at or beyond     April 20, 2012
                                         the agreement stage
    isChannelInbound    Boolean         True when user's channel is set     April 23, 2012
                                         to "Inbound"
    isChannelOutbound   Boolean         True when user's channel is set     April 23, 2012
                                         to "Outbound"
*/
public without sharing class QuoteAccountFormExtension {
    private SBQQ__Quote__c quote;
    public Boolean rAgreement {get; private set;}
    public Boolean rCreditCheck {get; private set;}
    public Boolean rDraft {get; private set;}
    public Boolean isChannelInbound {get{ try {return QuotePortalUtils.getChannel().equalsIgnoreCase('inbound') || QuotePortalUtils.getChannel().equalsIgnoreCase('inbound pilot'); } catch (Exception e) { QuoteUtilities.log(e); } return null; }}
    public Boolean isChannelOutbound {get{ try {return QuotePortalUtils.getChannel().equalsIgnoreCase('outbound'); } catch (Exception e) { QuoteUtilities.log(e); } return null; }}
    
    public static final String REQUIRED_STAGE_CREDIT_CHECK = 'Sent for Credit Check'; 
    public static final String REQUIRED_STAGE_AGREEMENT = 'Sent Agreement, waiting acceptance';
    public static final String REQUIRED_STAGE_CUSTOMER_ACCEPTED = 'Customer Accepted - Pending Dealer input';
    public static final String REQUIRED_STAGE_DRAFT = 'Draft';
    private static final Map<String, Integer> requiredOrdinalByStatus = new Map<String, Integer>();
    static {
        try {
        Schema.DescribeFieldResult quoteStatusField = SBQQ__Quote__c.SBQQ__Status__c.getDescribe();
        List<Schema.PicklistEntry> quoteStatusList = quoteStatusField.getPicklistValues();
        Integer ordinal = 1;
        for(Schema.PicklistEntry ple : quoteStatusList) {
            requiredOrdinalByStatus.put(ple.getValue(), ordinal);
            ordinal++;
        }
        } catch (Exception e) { QuoteUtilities.log(e); }
    }
    
    
    public PageReference bogusAction() {
        return null;
    }
    
    public QuoteAccountFormExtension() {
        try {
        
        // Let's try always requiring draft fields:
        rDraft = true;
        
        if (ApexPages.currentPage() != null) {
            String stage = ApexPages.currentPage().getParameters().get('stage');
            String qid = ApexPages.currentPage().getParameters().get('qid');
            if (qid != null) {
                rDraft = true;
                rCreditCheck = false;
                rAgreement = false;
                quote = QuoteUtilities.loadQuote( qid );
                if (quote != null && quote.id != null) {
                    if (requiredOrdinalByStatus.get(quote.SBQQ__Status__c) >= requiredOrdinalByStatus.get(REQUIRED_STAGE_CREDIT_CHECK)) {
                        rCreditCheck = true;
                    }
                    if (requiredOrdinalByStatus.get(quote.SBQQ__Status__c) >= requiredOrdinalByStatus.get(REQUIRED_STAGE_AGREEMENT)) {
                        rAgreement = true;
                    }
                }
            }
            if (stage != null) {
                if (requiredOrdinalByStatus.get(stage) >= requiredOrdinalByStatus.get(REQUIRED_STAGE_CREDIT_CHECK)) {
                    rCreditCheck = true;
                }
                if (requiredOrdinalByStatus.get(stage) >= requiredOrdinalByStatus.get(REQUIRED_STAGE_AGREEMENT)) {
                    rAgreement = true;
                }
            }
        }
        } catch (Exception e) { QuoteUtilities.log(e); }
    }
    
    private static testMethod void testQuoteAccountFormExt() {
        Opportunity opp = new Opportunity(Name='Test',CloseDate=System.today(),StageName='Prospecting');
        insert opp;
        
        SBQQ__Quote__c quote = new SBQQ__Quote__c(SBQQ__Opportunity__c=opp.Id);
        insert quote;
        
        ApexPages.currentPage().getParameters().put('qid', quote.Id);
        QuoteAccountFormExtension ext = new QuoteAccountFormExtension();
        
        system.assertEquals(true, ext.rDraft);
        system.assertEquals(false, ext.rCreditCheck);
        system.assertEquals(false, ext.rAgreement);
        
        ApexPages.currentPage().getParameters().put('stage', REQUIRED_STAGE_CREDIT_CHECK); // 'faking' the stage via URL
        ext = new QuoteAccountFormExtension();
        
        system.assertEquals(true, ext.rDraft);
        system.assertEquals(true, ext.rCreditCheck);
        system.assertEquals(false, ext.rAgreement);
        
        ApexPages.currentPage().getParameters().put('stage', REQUIRED_STAGE_AGREEMENT); // 'faking' the stage via URL
        ext = new QuoteAccountFormExtension();
        
        system.assertEquals(true, ext.rDraft);
        system.assertEquals(true, ext.rCreditCheck);
        system.assertEquals(true, ext.rAgreement);
        
        ApexPages.currentPage().getParameters().remove('stage');
        quote.SBQQ__Status__c = REQUIRED_STAGE_DRAFT;
        update quote;
        
        ext = new QuoteAccountFormExtension();
        
        system.assertEquals(true, ext.rDraft);
        system.assertEquals(false, ext.rCreditCheck);
        system.assertEquals(false, ext.rAgreement);
        
        quote.SBQQ__Status__c = REQUIRED_STAGE_CREDIT_CHECK;
        update quote;
        
        ext = new QuoteAccountFormExtension();
        
        system.assertEquals(true, ext.rDraft);
        system.assertEquals(true, ext.rCreditCheck);
        system.assertEquals(false, ext.rAgreement);
        
        quote.SBQQ__Status__c = REQUIRED_STAGE_AGREEMENT;
        update quote;
        
        ext = new QuoteAccountFormExtension();
        
        system.assertEquals(true, ext.rDraft);
        system.assertEquals(true, ext.rCreditCheck);
        system.assertEquals(true, ext.rAgreement);
        
        
    }
}