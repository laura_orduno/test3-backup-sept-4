public class AgreementVO {
    public Agreement__c r {get; private set;}
    
    public String id {get{return r.Id;}}
    public String name {get{return r.Name;}}
    
    public DateTime createdDate {get{return r.CreatedDate;}}
    public String createdDateFormatted {get{return r.CreatedDate.format();}}
    public String createdBy {get{return r.CreatedBy.Name;}}
    public DateTime lastModifiedDate {get{return r.LastModifiedDate;}}
    public String lastModifiedDateFormatted {get{return r.LastModifiedDate.format();}}
    public String lastModifiedBy {get{return r.LastModifiedBy.Name;}}
    
    public String url {get{return r.Url__c;}}
    public String type {get{return r.Type__c;}}
    public String waybillId {get{return r.Waybill_Id__c;}}
    public Id quote {get{return r.Quote__c;}}
    public Id recipient {get{return r.Recipient__c;}}
    
    public DateTime sentDate {get{return (r.Sent_Date__c != null) ? r.Sent_Date__c.date() : null;}}
    public String sentDateFormatted {get{return (r.Sent_Date__c != null) ? r.Sent_Date__c.format() : '';}}
    public Date expiryDate {get{return r.Expiry_Date__c;}}
    public String expiryDateFormatted {get{return (r.Expiry_Date__c != null) ? r.Expiry_Date__c.format() : '';}}
    public DateTime closedDate {get{return r.Closed_Date__c;}}
    public String closedDateFormatted {get{return (r.Closed_Date__c != null) ? r.Closed_Date__c.format() : '';}}
    
    public DateTime signedDate {get{return (r.Signed_Date__c != null) ? r.Signed_Date__c.date() : null;}}
    public String signedDateFormatted {get{return (r.Signed_Date__c != null) ? r.Signed_Date__c.format() : '';}}
    
    public Boolean accepted {get{return (r.Signed_Date__c != null);}}
    
    public String status {get{return r.Status__c;}}
    
    public AgreementVO(Agreement__c re) {this.r = re;}
    
    static testMethod void testAgreementVO() {
        Account a = new Account(Name = 'Test', Phone = '6049969449');
        insert a;
        Contact c = new Contact(FirstName = 'Alex', LastName = 'Miller', Phone = '6049969449');
        insert c;
        Agreement__c ag = new Agreement__c(Recipient__c = c.Id, Type__c = 'Recombo', Waybill_Id__c = 'testwbid', Status__c = 'Pending', Sent_Date__c = Datetime.now(), Expiry_Date__c = Date.today().addDays(2));
        insert ag;
        ag = [Select Id, Name, CreatedDate, CreatedBy.Name, LastModifiedDate, LastModifiedBy.Name, Url__c, Type__c, Waybill_id__c, Quote__c, Recipient__c, Sent_Date__c, Expiry_Date__c, Closed_Date__c, Signed_Date__c, Status__c From Agreement__c Where Id = :ag.id];
        AgreementVO avo = new AgreementVO(ag);
        system.assertEquals(avo.id, ag.id);
        system.assertEquals(avo.name, ag.name);
        system.assertEquals(avo.createdDate, ag.CreatedDate);
        system.assertEquals(avo.createdDateFormatted, ag.CreatedDate.format());
        system.assertEquals(avo.lastModifiedDate, ag.LastModifiedDate);
        system.assertEquals(avo.lastModifiedDateFormatted, ag.LastModifiedDate.format());
        system.assertEquals(avo.createdBy, ag.CreatedBy.Name);
        system.assertEquals(avo.lastModifiedBy, ag.LastModifiedBy.Name);
        system.assertEquals(avo.url, ag.Url__c);
        system.assertEquals(avo.type, ag.Type__c);
        system.assertEquals(avo.waybillid, ag.Waybill_Id__c);
        system.assertEquals(avo.quote, null);
        system.assertEquals(avo.recipient, ag.Recipient__c);
        
        system.assertEquals(avo.sentDate, ag.Sent_Date__c.date());
        system.assertEquals(avo.sentDateFormatted, ag.Sent_Date__c.format());
        system.assertEquals(avo.expiryDate, ag.Expiry_Date__c);
        system.assertEquals(avo.expiryDateFormatted, ag.Expiry_Date__c.format());
        system.assertEquals(avo.signedDate, null);
        system.assertEquals(avo.signedDateFormatted, '');
        system.assertEquals(avo.accepted, false);
        system.assertEquals(avo.status, ag.Status__c);
        
    }
}