@isTest
private class OCOM_ACD_serviceAddressHelperTest {
@IsTest
    private static void testInit(){
        
        Schema.DescribeSObjectResult d = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
        Schema.RecordTypeInfo rtByName =  rtMapByName.get('CAN');
        Id recTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Billing').getRecordTypeId();
        Id recSerTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        Id recRCIDTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId();
        
        Account RCIDacc = new Account(name='debs',recordtypeid=recRCIDTypeId);
        Account acc = new Account(name='Billing account', BAN_CAN__c='draft', recordtypeid=recTypeId,Billing_System__c = '102',Billingstreet = 'Test Street',BillingCity = 'Test city',BillingState = 'Test state',BillingPostalCode = '1m1 1k1',BillingCountry = 'CANADA');
        Account serAcc = new Account(name='123 street', recordtypeid=recSerTypeId, parentid=RCIDacc.id);
        
        
        List<Account> acclist= new List<Account>();          
        acclist.add(RCIDacc);
        acclist.add(acc);
        acclist.add(serAcc);
        insert acclist;
        
        SMBCare_Address__c address = new SMBCare_Address__c(Account__c=RCIDacc.Id, Postal_Code__c='V1P1P4', Street_Number__c='5350', Street_Name__c='BIG WHITE RD', Province__c='BC', Suite_Number__c='BC', COID__c='RTLD', Building_Number__c='5350', Country__c='CAN', FMS_Address_ID__c='005552708',Service_Account_Id__c = serAcc.Id);
       // insert address;
        
        
         List<SMBCare_Address__c> addList = new List<SMBCare_Address__c>();
        addList.add(address);
        insert addList;
        
        Product2 prod = new Product2(Name = 'Laptop X200', Family = 'Hardware');
        insert prod;

        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod.Id, UnitPrice = 10000, IsActive = true);
        insert standardPrice;

        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;

        PricebookEntry customPrice = new PricebookEntry(Pricebook2Id = customPB.Id, Product2Id = prod.Id, UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        Contact c= new Contact(FirstName='abc', LastName='xyz');
        insert c;
        
        List< Credit_Assessment__c> CARList= new List<Credit_Assessment__c>();
        Credit_Assessment__c CAR= new Credit_Assessment__c(CAR_Status__c='test', CAR_Result__c='Denied');
        CARList.add(CAR);
        Credit_Assessment__c CAR2= new Credit_Assessment__c(CAR_Status__c='test',CAR_Result__c='Denied');
        CARList.add(CAR2);
        insert CARList;
         
        Order ord = new Order(Credit_Assessment__c=car.id,Ban__c='draft',Reason_Code__c='34aas',General_Notes_Remarks__c='123123', accountId=serAcc.id, effectivedate=system.today(), status='not submitted', Pricebook2Id=customPB.id, CustomerAuthorizedById=c.id, service_address__c=null,ServiceAccountId__c=serAcc.Id);   
        Order ord2 = new Order(Credit_Assessment__c=car2.id,Ban__c='draft',Reason_Code__c='34aas',General_Notes_Remarks__c='123', accountId=acc.id, effectivedate=system.today(), status='Draft', Pricebook2Id=customPB.id, CustomerAuthorizedById=c.id, service_address__c=address.id,ServiceAccountId__c=serAcc.Id);   
        List<Order> ordlist = new List<Order>();   
        ordList.add(ord);ordList.add(ord2);
        insert ordList; 
         
             
        Work_Order__c wkOrder = new Work_Order__c(Scheduled_Due_Date_Location__c='2016-03-11 08:00:00',
                                                        Install_Type__c ='FieldWork',
                                                        WFM_Number__c='1234567', Time_Slot__c='0800-1200');
                                                        
        wkOrder.status__c = Label.OCOM_WFMReserved;
        wkOrder.Order__c = ord.Id;
        
        insert wkOrder;
        
        OrderItem  OLI2= new orderItem(vlocity_cmt__LineNumber__c='1',Work_Order__c=wkOrder.id,UnitPrice=12,orderId= ord2.Id,
                                                vlocity_cmt__BillingAccountId__c=RCIDacc.id,vlocity_cmt__ServiceAccountId__c=seracc.id,Quantity=2,
                                                PricebookEntryId=customPrice.id);
        OLI2.vlocity_cmt__ProvisioningStatus__c = 'Active';   
        
        OrderItem  OLI= new orderItem(vlocity_cmt__LineNumber__c='1',Work_Order__c=wkOrder.id,UnitPrice=12,orderId= ord.Id, vlocity_cmt__AssetReferenceId__c=OLI2.id,vlocity_cmt__BillingAccountId__c=null,vlocity_cmt__ServiceAccountId__c=null,Quantity=2,PricebookEntryId=customPrice.id);
        OLI.vlocity_cmt__ProvisioningStatus__c = 'New';    
       
       // OrderItem OLI2 = new OrderItem(UnitPrice=12,vlocity_cmt__ProvisioningStatus__c = 'Active', orderId= ord.Id, orderMgmt_BPI_Id__c='9871379163517', vlocity_cmt__BillingAccountId__c=acc.id, vlocity_cmt__ServiceAccountId__c=serAcc.id, Quantity=2, PricebookEntryId=customPrice.id);
        //OrderItem  OLI = new OrderItem(UnitPrice=12,vlocity_cmt__ProvisioningStatus__c = 'Active', orderId= ord.Id, vlocity_cmt__AssetReferenceId__c=OLI2.id, vlocity_cmt__BillingAccountId__c=acc.id, vlocity_cmt__ServiceAccountId__c=serAcc.id, Quantity=2, PricebookEntryId=customPrice.id);
          
        List<OrderItem>OLIlist= new List<OrderItem>();   
        OLIList.add(OLI2);
       OLIList.add(OLI);
       insert OLIList;
        
        
        
        asset ast= new asset(name='test', accountID=RCIDacc.id, vlocity_cmt__OrderId__c=ord2.id, status='Shipped');
        insert ast; 
        //Custom Setting for Endpoint Field
         
     
     

       
        PageReference pageRef =Page.OCOM_HybridCPQ;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', ord.Id);
        ApexPages.currentPage().getParameters().put('src', 'os');
        ApexPages.currentPage().getParameters().put('smbCareAddrId', address.id);
        ApexPages.currentPage().getParameters().put('contactId', null);
        Object controller;
       OCOM_HybridCPQExt myPageExt = new OCOM_HybridCPQExt(controller);
       //ApexPages.StandardController stdController = new ApexPages.standardController(ord);
       // OCOM_ACD_ServiceAddresshelper myPageExt = new OCOM_ACD_ServiceAddresshelper(stdController);
        OCOM_ACD_ServiceAddresshelper addHelper = new OCOM_ACD_ServiceAddresshelper();
       
        Test.startTest();
        PageReference p = myPageExt.init();
       addHelper.PopulateServiceAddressOnOrder(ord.id, OLIList);
         addHelper.CheckIfAnylineItemsAreActiveAsset(ord.Id);
        addHelper.updateServiceAddDetailsFMS(address.FMS_Address_ID__c, address.Province__c, address.Id);
        addHelper.RetriveBillingAddress(acc.id);
        addHelper.RetriveBANCAN(acc.id);
        OCOM_ACD_ServiceAddresshelper.ProductAvailiblity(address.id);
        acc.id=null;
        addHelper.RetriveBillingAddress(acc.id);
        addHelper.RetriveBANCAN(acc.id);
        //Added by danish on 27/03/2017 to cover UpdatePrivisioningStatusOnDisconnect method passing an order item Id as Parameter
        String IdOfLineItem = OLI.Id;
        Test.stopTest();
                        
        List<ApexPages.Message> messages = ApexPages.getMessages();
        
        
        }
        
    
    }