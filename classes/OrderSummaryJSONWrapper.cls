public class OrderSummaryJSONWrapper {
    
    public String CreatedDate {get;set;} 
    public String LastModifiedDate {get;set;} 
    public String Id {get;set;} 
    public String OrderNumber {get;set;}
    public String AccountId {get;set;} 
    public String Sub_order_numberc {get;set;}
    public String Status {get;set;} 
    public String Service_Addressc {get;set;} 
    public Double vlocity_cmtEffectiveOneTimeTotalc {get;set;} 
    public Double vlocity_cmtTotalOneTimeDiscountc {get;set;} 
    public Double vlocity_cmtOneTimeTotalc {get;set;} 
    public Double vlocity_cmtEffectiveRecurringTotalc {get;set;} 
    public Double vlocity_cmtRecurringTotalc {get;set;} 
    public Double TotalAmount {get;set;} 
    public String RecordTypeId {get;set;} 
    public String CurrencyIsoCode {get;set;} 
    public OrderItems OrderItems {get;set;} 
    public Service_Addressr Service_Addressr {get;set;} 
    public Boolean validBillingAddress {get;set;}
    public Boolean validShippingAddress {get;set;}
    public Boolean validCreditAssessment {get;set;}
    public Boolean validContract {get;set;}
    public Boolean dueDateBookingComplete {get;set;}
    public Boolean validConfiguration {get;set;}
       
    public static void consumeObject(JSONParser parser) {
        Integer depth = 0;
        do {
            JSONToken curr = parser.getCurrentToken();
            if (curr == JSONToken.START_OBJECT || 
                curr == JSONToken.START_ARRAY) {
                    depth++;
                } else if (curr == JSONToken.END_OBJECT ||
                           curr == JSONToken.END_ARRAY) {
                               depth--;
                           }
        } while (depth > 0 && parser.nextToken() != null);
    }
    
    public class OrderItems {
        public Integer totalSize {get;set;} 
        public Boolean done {get;set;} 
        public List<Records> records {get;set;} 
        
        public OrderItems(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'totalSize') {
                            totalSize = parser.getIntegerValue();
                        } else if (text == 'done') {
                            done = parser.getBooleanValue();
                        } else if (text == 'records') {
                            records = new List<Records>();
                            while (parser.nextToken() != JSONToken.END_ARRAY) {
                                records.add(new Records(parser));
                            }
                        } else {
                            System.debug(LoggingLevel.WARN, 'OrderItems consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public OrderSummaryJSONWrapper(JSONParser parser) {
        while (parser.nextToken() != JSONToken.END_OBJECT) {
            if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                String text = parser.getText();
                if (parser.nextToken() != JSONToken.VALUE_NULL) {
                    if (text == 'CreatedDate') {
                        CreatedDate = parser.getText();
                    } else if (text == 'LastModifiedDate') {
                        LastModifiedDate = parser.getText();
                    } else if (text == 'AccountId') {
                        AccountId = parser.getText();
                    } else if (text == 'Id') {
                        Id = parser.getText();
                    } else if (text == 'OrderNumber') {
                        OrderNumber = parser.getText();
                    }else if (text == 'Sub_order_numberc') {
                        Sub_order_numberc = parser.getText();
                        if(String.isNotBlank(Sub_order_numberc) && Sub_order_numberc.length()>3){
                            String numericStr=Sub_order_numberc.substring(3);
                            Decimal decimalValOfNmrcStr=Decimal.valueOf(numericStr);
                            String finalStr='OR-'+String.valueOf(decimalValOfNmrcStr);
                            Sub_order_numberc=finalStr;
                        }
                        
                    } else if (text == 'Status') {
                        Status = parser.getText();
                    } else if (text == 'Service_Addressc') {
                        Service_Addressc = parser.getText();
                    } else if (text == 'vlocity_cmtEffectiveOneTimeTotalc') {
                        vlocity_cmtEffectiveOneTimeTotalc = parser.getDoubleValue();
                    } else if (text == 'vlocity_cmtTotalOneTimeDiscountc') {
                        vlocity_cmtTotalOneTimeDiscountc = parser.getDoubleValue();
                    } else if (text == 'vlocity_cmtOneTimeTotalc') {
                        vlocity_cmtOneTimeTotalc = parser.getDoubleValue();
                    } else if (text == 'vlocity_cmtEffectiveRecurringTotalc') {
                        vlocity_cmtEffectiveRecurringTotalc = parser.getDoubleValue();
                    } else if (text == 'vlocity_cmtRecurringTotalc') {
                        vlocity_cmtRecurringTotalc = parser.getDoubleValue();
                    } else if (text == 'TotalAmount') {
                        TotalAmount = parser.getDoubleValue();
                    } else if (text == 'RecordTypeId') {
                        RecordTypeId = parser.getText();
                    } else if (text == 'CurrencyIsoCode') {
                        CurrencyIsoCode = parser.getText();
                    } else if (text == 'OrderItems') {
                        OrderItems = new OrderItems(parser);
                    } else if (text == 'Service_Addressr') {
                        Service_Addressr = new Service_Addressr(parser);
                    } else {
                        System.debug(LoggingLevel.WARN, 'OrderSummaryJSONWrapper consuming unrecognized property: '+text);
                        consumeObject(parser);
                    }
                }
            }
        }
    }
    
    public class Product2 {
        public String Id {get;set;} 
        public String Name {get;set;} 
        public String RecordTypeId {get;set;} 
        public String CurrencyIsoCode {get;set;} 
        
        public Product2(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'Id') {
                            Id = parser.getText();
                        } else if (text == 'Name') {
                            Name = parser.getText();
                        } else if (text == 'RecordTypeId') {
                            RecordTypeId = parser.getText();
                        } else if (text == 'CurrencyIsoCode') {
                            CurrencyIsoCode = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Product2 consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Service_Addressr {
        public String Id {get;set;} 
        public String Streetc {get;set;} 
        public String Street_Addressc {get;set;} 
        public String Cityc {get;set;} 
        public String Provincec {get;set;} 
        public String RecordTypeId {get;set;} 
        public String CurrencyIsoCode {get;set;} 
        public String addresstextc {get;set;}
        public String Street_Numberc {get;set;} // Added service_address__r.Street_Number__c as per defect BSBD_RTA-998
        
        public Service_Addressr(JSONParser parser) {
            System.debug('san1');
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'Id') {
                            Id = parser.getText();
                        } else if (text == 'Streetc') {
                            Streetc = parser.getText();
                        } else if (text == 'Street_Addressc') {
                            Street_Addressc = parser.getText();
                        } else if (text == 'Cityc') {
                            Cityc = parser.getText();
                        } else if (text == 'Provincec') {
                            Provincec = parser.getText();
                        } else if (text == 'RecordTypeId') {
                            RecordTypeId = parser.getText();
                        } else if (text == 'CurrencyIsoCode') {
                            CurrencyIsoCode = parser.getText();
                        } else if (text == 'AddressTextc') {
                            addresstextc = parser.getText();
                        } else if(text == 'Street_Numberc'){ // Added service_address__r.Street_Number__c as per defect BSBD_RTA-998
                        	Street_Numberc = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Service_Addressr consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Records {
        public String OrderId {get;set;} 
        public String Id {get;set;} 
        public String CreatedDate {get;set;} 
        public String LastModifiedDate {get;set;} 
        public String vlocity_cmtProvisioningStatusc {get;set;} 
        public String vlocity_cmtLineNumberc {get;set;} 
        public String PricebookEntryId {get;set;} 
        public String CurrencyIsoCode {get;set;} 
        public PricebookEntry PricebookEntry {get;set;} 
        public String OrderItemNumber {get;set;}
        
        public Records(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'OrderId') {
                            OrderId = parser.getText();
                        } else if (text == 'Id') {
                            Id = parser.getText();
                        } else if (text == 'CreatedDate') {
                            CreatedDate = parser.getText();
                        } else if (text == 'LastModifiedDate') {
                            LastModifiedDate = parser.getText();
                        } else if (text == 'vlocity_cmtProvisioningStatusc') {
                            vlocity_cmtProvisioningStatusc = parser.getText();
                        } else if (text == 'vlocity_cmtLineNumberc') {
                            vlocity_cmtLineNumberc = parser.getText();
                        } else if (text == 'PricebookEntryId') {
                            PricebookEntryId = parser.getText();
                        } else if (text == 'CurrencyIsoCode') {
                            CurrencyIsoCode = parser.getText();
                        } else if (text == 'PricebookEntry') {
                            PricebookEntry = new PricebookEntry(parser);
                        } else if(text =='OrderItemNumber'){
                            OrderItemNumber=parser.getText();
                        }else {
                            System.debug(LoggingLevel.WARN, 'Records consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class PricebookEntry {
        public String Id {get;set;} 
        public String Product2Id {get;set;} 
        public String CurrencyIsoCode {get;set;} 
        public Product2 Product2 {get;set;} 
        
        public PricebookEntry(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'Id') {
                            Id = parser.getText();
                        } else if (text == 'Product2Id') {
                            Product2Id = parser.getText();
                        } else if (text == 'CurrencyIsoCode') {
                            CurrencyIsoCode = parser.getText();
                        } else if (text == 'Product2') {
                            Product2 = new Product2(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'PricebookEntry consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    
    public static OrderSummaryJSONWrapper parse(String json) {
        return new OrderSummaryJSONWrapper(System.JSON.createParser(json));
    }
}