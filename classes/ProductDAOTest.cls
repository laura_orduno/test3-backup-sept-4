@isTest
private class ProductDAOTest {
	private static testMethod void testProductDAO() {
		Product2 p = new Product2(name = 'test');
		insert p;
		
		SBQQ__Cost__c cost = new SBQQ__Cost__c(SBQQ__Product__c = p.id, SBQQ__Active__c = true, SBQQ__UnitCost__c = 20);
		insert cost;
		
		SBQQ__ProductFeature__c f = new SBQQ__ProductFeature__c(SBQQ__ConfiguredSKU__c = p.id, SBQQ__Number__c = 1);
		insert f;
		
		SBQQ__ProductOption__c po = new SBQQ__ProductOption__c(
			SBQQ__ConfiguredSKU__c = p.id,
			SBQQ__Feature__c = f.id,
			SBQQ__Number__c = 1
		);
		insert po;
		
		ProductDAO pd = new ProductDAO();
		pd.loadByIds(null);
		
		pd.loadBlockPrices = true;
		pd.loadConfigurationRules = true;
		pd.loadCosts = true;
		pd.loadCustomField = true;
		pd.loadOptions = true;
		pd.loadPriceEntries = true;
		pd.loadRelatedContent = true;
		pd.maxDepth = 2;
		
		pd.loadById(p.id);
		
		ProductVO pv = new ProductVO(p);
		pd.save(pv);
	}
}