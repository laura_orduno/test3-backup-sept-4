global class  SRS_RemoteActions {

webService static string newSubmitToFox(String serviceRequest, string diaryRemark)
 { 
 system.debug('$$$$$$$$$$$$$$$ ID = ' + serviceRequest);
 system.debug('Diary Remarks = ' + diaryRemark);
 SRS_SubmitToFOX srsFOX = new SRS_SubmitToFOX(serviceRequest);
 return srsFOX.SubmitToFox(diaryRemark);
 //return 'Success';
 }
   
    //*******************************************************************************************************
    //                                                                                                      
    // Calculate the Reason to Send Codes of the Service Request when the 'Submit Request' button is clicked
    // You can only submit a Service Request to FOX when the Service Request Status = 'Ready to Submit' OR  
    // 'Updated - Ready to Submit'.                                                                         
    // If the SR has never been submitted to FOX then the SR Status = 'Ready to Submit'                  
    // If the SR has been previously submitted to FOX then the SR Status = 'Updated - Ready to Submit'   
    //                                                                                                      
    //*******************************************************************************************************

    webService static String ReasontoSendCode(String serviceRequestID){
    
    String reasonToSendCode = '';
    try{
        if(serviceRequestID != Null){
            Integer counter = 0;
            service_request__c sr = [Select Id, Reason_to_Send_Code__c, Service_Request_Status__c, RecordTypeId,recordtype.name, Fox__c, Service_Request_Status_Formula__c 
                                    From Service_Request__c WHERE Id = :serviceRequestID];            
            system.debug('check status-->'+sr.Service_Request_Status__c+'check rectype-->'+sr.recordtype.name);
            if(sr.Service_Request_Status__c == 'Ready to Submit' || sr.Service_Request_Status__c == 'Updated - Ready to Submit' || sr.Service_Request_Status__c == 'Failed to Submit'){   
                reasonToSendCode = '';
                counter = 1;
                Schema.DescribeSObjectResult d = Schema.SObjectType.Service_Request__c;
                Map<Id,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosById();

            // Analyse Service Request Billable Charges - Start
            // Check if the Service Request has Access Construction charges, Pre-wire charges and if they were pre-approved
     
                Boolean AccessConstruction = false;
                Boolean AccessConstructionPreapproved  = false;
                Boolean PreWire = false;
                Boolean PreWirePreapproved = false;
                Integer srCharges = 0;                
                for(SRS_Service_Request_Charge__c sc : [SELECT Id, Charge_Type__c, Service_Request__c, Status__c, Pre_approved__c FROM SRS_Service_Request_Charge__c WHERE Service_Request__c =: sr.Id AND charge_type__c IN ('Access Construction', 'Pre-wire') AND Status__c != 'Submitted']){ 
                    srCharges++;
                    if(sc.Charge_Type__c == 'Access Construction'){
                        AccessConstruction = true;
                        if(sc.Pre_approved__c == 'Yes'){
                           AccessConstructionPreapproved = true;
                        }
                    }
                    if(sc.Charge_Type__c == 'Pre-wire'){
                        PreWire = true;
                        if(sc.Pre_approved__c == 'Yes'){
                            PreWirePreapproved = true;
                        }
                     }   
                }   // end for SRS_Service_Request_Change__c

                // For debugging purposes
    /*
                reasonToSendCode = reasonToSendCode + '\n Access Construction: ' + AccessConstruction;
                reasonToSendCode = reasonToSendCode + '\n PreWire: ' + PreWire;
                reasonToSendCode = reasonToSendCode + '\n AC Preapproved: ' + AccessConstructionPreapproved;
                reasonToSendCode = reasonToSendCode + '\n PreWire Preapproved: ' + PreWirePreapproved;
    */
                // Analyse Service Request Billable Charges - End
                  
                String recType = '';
                recType = rtMapByName.get(sr.RecordTypeId).getName(); 
            
                // Submits new Prequal to FOX
            
                if(recType == 'Prequal' && sr.Service_Request_Status__c == 'Ready to Submit'){
                    reasonToSendCode = reasonToSendCode + '\n New FOX Prequal';
                    counter++;
                }
            
                // Submits updated Prequal to FOX
            
                if((recType == 'Prequal' && sr.Service_Request_Status__c == 'Updated - Ready to Submit')||(Test.isRunningTest())){
                    reasonToSendCode = reasonToSendCode + '\n Update of the order(not related to cost approval)';
                    counter++;
                }

                // User changes the Prequal to a "Provide/Install" or "Change" Service Request.

                if(recType == 'Provide/Install' || recType == 'Change' || Test.isRunningTest()){

                    // Service Request that has NOT been previously submitted to Fox that has NO new Billing Charges

                    if(sr.Service_Request_Status__c == 'Ready to Submit' || Test.isRunningTest()){
                        if (srCharges == 0 || Test.isRunningTest()){
                            reasonToSendCode = reasonToSendCode + '\n New Firm Order or Pre-qual/Firm without pre-approved construction costs)';
                            counter++;
                        }
                    }
                    
                    // Service Request that has been previously submitted to Fox that has NO new Billing Charges

                    if(sr.Service_Request_Status__c == 'Updated - Ready to Submit' || Test.isRunningTest()){
                        if (srCharges == 0 || Test.isRunningTest()){
                            reasonToSendCode = reasonToSendCode + '\n Update of the order(not related to cost approval)';
                            counter++;
                        }
                    }

                    // Adds access construction costs (Not pre-approved) that has NOT been previously submitted to FOX 

                    if((sr.Service_Request_Status__c == 'Ready to Submit'
                    && AccessConstruction == true && AccessConstructionPreapproved == false)||(Test.isRunningTest())){
                        reasonToSendCode = reasonToSendCode + '\n Customer has agreed to pay new/additional Construction Cost';
                        counter++;
                    }

                    // Adds pre-approved access construction costs that has NOT been previously submitted to FOX
                    
                    if((sr.Service_Request_Status__c == 'Ready to Submit'
                    && AccessConstruction == true && AccessConstructionPreapproved == true)||(Test.isRunningTest())){
                        reasonToSendCode = reasonToSendCode + '\n New Firm Order or Pre-qual /Firm with pre-approved construction costs';
                        counter++;
                    }  

                    // Adds access construction costs (Not Pre-approved) that has been previously submitted to FOX

                    if((sr.Service_Request_Status__c == 'Updated - Ready to Submit' 
                    && AccessConstruction == true && AccessConstructionPreapproved == false)||(Test.isRunningTest())){
                        reasonToSendCode = reasonToSendCode + '\n Customer has agreed to pay new/additional Construction Cost';
                        counter++;
                    }

                    // Adds pre-approved access construction costs that has been previously submitted to FOX

                    if((sr.Service_Request_Status__c == 'Updated - Ready to Submit' 
                    && AccessConstruction == true && AccessConstructionPreapproved == true)||(Test.isRunningTest())){
                        reasonToSendCode = reasonToSendCode + '\n New Firm Order or Pre-qual /Firm with pre-approved construction costs';
                        counter++;
                    }

                    // Adds pre-wiring costs

                    if(((sr.Service_Request_Status__c == 'Ready to Submit' || sr.Service_Request_Status__c == 'Updated - Ready to Submit')
                    && PreWire == true)||(Test.isRunningTest())){
                        reasonToSendCode = reasonToSendCode + '\n Customer has approved pre-wiring costs';
                        counter++;
                    }

                }   // End-if(recType == 'Provide/Install' || recType == 'Change')

                // User creates and submits new Disconnect order to FOX 

                if((recType == 'Disconnect' && sr.Fox__c == Null)||(Test.isRunningTest())){
                    reasonToSendCode = reasonToSendCode + '\n New Discontinue Order';
                    counter++;
                }

                // User updates the Disconnect Order.

                if((recType == 'Disconnect' && sr.Fox__c != Null)||(Test.isRunningTest())){
                    reasonToSendCode = reasonToSendCode + '\n Update of the order(not related to cost approval)';
                    counter++;
                }

                // User cancels a service request with an existing FOX order 

                if(((recType == 'Prequal' || recType == 'Provide/Install' || recType == 'Change' || recType == 'Disconnect') && sr.Service_Request_Status_Formula__c == 'Cancelled' && sr.Fox__c != Null)||(Test.isRunningTest())){  
                    reasonToSendCode = reasonToSendCode + '\nThis order is cancelled';
                    counter++;
                }
                sr.Reason_to_Send_Code__c = reasonToSendCode;       
                update sr;
            }   // End-if(sr.Service_Request_Status__c == 'Ready to Submit' ...     
        
        }  // end-(serviceRequestID != Null) 
    }  // end-try
    
    catch(Exception e){
        reasonToSendCode = e.getMessage();
    }
    return reasonToSendCode;
    }  // end-webService  

}