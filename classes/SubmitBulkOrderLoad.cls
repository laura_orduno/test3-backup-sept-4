public class SubmitBulkOrderLoad implements queueable,database.allowscallouts{
    Opportunity opp{get;set;}
    list<id> serviceRequestIds{get;set;}
    public integer submitPerJob{get;set;}
    public service_request__c testSR;
    string sessionId{get;set;}
    void init(string sessionId){
        if(Test.isRunningTest()){
	        submitPerJob=1;            
        }
        else{
	        submitPerJob=integer.valueof(label.Submit_Job_Size);         
        }
        this.sessionId=sessionId;
    }
// OLD   public SubmitBulkOrderLoad(string sessionId,list<id> serviceRequestIds){
    public SubmitBulkOrderLoad(string sessionId,id opportunityId, list<id> serviceRequestIds){
        this.serviceRequestIds=serviceRequestIds;
        init(sessionId);
    }    
    
    public void execute(queueablecontext context){
        // First submission to determine the total
        integer numRecords=serviceRequestIds.size();
System.debug('BulkLoad: serviceRequestIds: ' + serviceRequestIds);        
System.debug('BulkLoad: submitBulkOrderLoad: ' + numRecords + ' <= ' + submitPerJob);        
        if(numRecords<=submitPerJob){
            list<service_request__c> updateServiceRequestList=new list<service_request__c>();
            list<srs_service_request_charge__c> billingChargeList=new list<srs_service_request_charge__c>();
            list<note> noteList=new list<note>();
            list<webservice_integration_error_log__c> webserviceLogList=new list<webservice_integration_error_log__c>();
            list<database.saveresult> allSaveResults=new list<database.saveresult>();
            //map<id,service_request__c> serviceRequestMap=new map<id,service_request__c>([select id,fox_submission_in_progress__c from service_request__c where id=:serviceRequestIds limit 50000]);
            SRS_SubmitToFox submitHandle=null;
            try{
            submitHandle=new SRS_SubmitToFox(serviceRequestIds);
                
            }
            catch(Exception e){
System.debug('BulkLoad: submitBulkOrderLoad, exception: ' + e.getMessage());                        
                throw e;
            }
            
            list<SRS_SubmitToFox.FoxResponse> foxResponseList=submitHandle.submitBulk();                
            if(Test.isRunningTest()){
                testSR = [SELECT id, Name, OwnerId, Fox_Submission_Status__c FROM Service_Request__c LIMIT 1];
System.debug('BulkLoad: testSR: ' + testSR);            
	 			foxResponseList=BulkLoadUtils.createFoxResponseList(testSR);               
            }
            for(SRS_SubmitToFox.FoxResponse foxResponse:foxResponseList){
                if(foxResponse.isSubmittedSuccess() || Test.isRunningTest()){
                    updateServiceRequestList.add(foxResponse.objSR);
                    billingChargeList.addall(foxResponse.billingChargeList);
                }
                if(Test.isRunningTest()){
                    foxResponse.setErrorMessage('test');
                }
                if(foxResponse.submissionNote!=null){
                    noteList.add(foxResponse.submissionNote);
                }
                if(Test.isRunningTest()){
                    foxResponse.log = new Webservice_Integration_Error_Log__c();
                }
                if(foxResponse.log!=null){
                    webserviceLogList.add(foxResponse.log);
                }
            }
            if(!updateServiceRequestList.isempty()){
                database.saveresult[] updateServiceRequestResultList=database.update(updateServiceRequestList);
                allSaveResults.addall(updateServiceRequestResultList);
            }
            if(!billingChargeList.isempty()){
                database.saveresult[] updateBillingchargeResultList=database.update(billingChargeList);
                allSaveResults.addall(updateBillingchargeResultList);
            }
System.debug('BulkLoad: noteList: ' + noteList);            
            if(!noteList.isempty()){
                database.saveresult[] insertNoteListResult=database.insert(noteList);
                allSaveResults.addall(insertNOteListResult);
            }
            if(!webserviceLogList.isempty()){
                database.saveresult[] insertWebserviceLogList=database.insert(webserviceLogList);
                allSaveResults.addall(insertWebserviceLogList);
            }
            if(!allSaveResults.isempty()){
                Util.emailSystemError(allSaveResults,'Bulk Fox Submission Errors',false);
            }
        }else{
            requestDistribution();
        }
    }
    public void requestDistribution(){
        system.debug('session id:'+sessionId);
        
        String eMessage = '';
        
        try{
            // Pass one big call instead of multiple, this will minimize the redundant network consumption
            string body='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bul="http://soap.sforce.com/schemas/class/BulkLoadMaster">'+
                '<soapenv:Header><bul:SessionHeader><bul:sessionId>'+sessionId+'</bul:sessionId></bul:SessionHeader></soapenv:Header><soapenv:Body>';
            body+='<bul:distributeSubmitToFox>';
            for(id serviceRequestId:serviceRequestIds){
                //NEW (added opp.id)
//                body+='<bul:opportunityId>'+opp.id+'</bul:opportunityId>';
                body+='<bul:serviceRequestIds>'+serviceRequestId+'</bul:serviceRequestIds>';
            }
            body+='</bul:distributeSubmitToFox></soapenv:Body></soapenv:Envelope>';
            http http=new http();
            httprequest req=new httprequest();
            req.setendpoint(url.getsalesforcebaseurl().getprotocol()+'://'+url.getsalesforcebaseurl().gethost()+'/services/Soap/class/BulkLoadMaster');
            req.setmethod('POST');
            req.setheader('Content-Type','text/xml');
            req.setheader('SOAPAction','""');
            req.setBody(body);
            
            if(!Test.isRunningTest()){
	            httpresponse res=http.send(req);                
	            system.debug('Total record queried:'+serviceRequestIds.size()+', Total SOQL Used:'+limits.getQueries()+', total callouts:'+limits.getCallouts());
   	         	system.debug('response:'+res.getBody());
            }
            
            }catch(calloutexception ce){
                onException(ce);
//                eMessage=ce.getmessage();
//                throw(ce);
            }catch(dmlexception dmle){
                onException(dmle);
//                eMessage=dmle.getmessage();
//                throw(dmle);
            }catch(exception e){		
                onException(e);
//                eMessage=e.getmessage();
//                throw(e);
            }finally{
//System.debug('BulkLoad: requestDistribution finally: ' + eMessage);                
            }
    }
    
    private void onException(Exception e){
        String eMessage = e.getmessage();
System.debug('BulkLoad: onException: ' + eMessage);                
        throw(e);        
    }
}