@isTest
global class OCOM_DocuSignStatusAPI_MockUp implements WebServiceMock {
global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
               DocuSignStatusAPI.VoidEnvelopeStatus Obj = new DocuSignStatusAPI.VoidEnvelopeStatus();
               Obj.VoidSuccess = True;
               DocuSignStatusAPI.VoidEnvelopeResponse_element MainObj = new DocuSignStatusAPI.VoidEnvelopeResponse_element();
               MainObj.VoidEnvelopeResult = Obj;
               response.put('response_x', MainObj);
           }

}