/*
###########################################################################
# File..................: smb_processECAT_sch
# Version...............: 26
# Created by............: Andrew Castillo
# Created Date..........: 16-Apr-2013
# Last Modified by......: 
# Last Modified Date....: 
# Description...........: This is a scheduler class which runs the batch jobs to process eCAT data
#
# Copyright (c) 2000-2012. Astadia, Inc. All Rights Reserved.
#
# Created by the Astadia, Inc. Modification must retain the above copyright notice.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any commercial purpose, without fee, and without a written
# agreement from Astadia, Inc., is hereby forbidden. Any modification to source
# code, must include this paragraph and copyright.
#
# Permission is not granted to anyone to use this software for commercial uses.
#
# Contact address: 2839 Paces Ferry Road, Suite 350, Atlanta, GA 30339
# Company URL : http://www.astadia.com
###########################################################################
*/
global class smb_processECAT_sch implements Schedulable
{
	global void execute(SchedulableContext SC)
	{
		AggregateResult AR_ATM = [Select MAX(CreatedDate) CDate From AsyncApexJob a Where a.ApexClass.Name = 'smb_moveUserToAccountTeam_Batch'];
		
		Datetime lastRunATM = (Datetime) AR_ATM.get('CDate');
		
		String atmQuery;
		if (lastRunATM == null)
		{
			// query for all active account team members
			atmQuery = 'Select Account__c From Sales_Assignment__c Where User__r.IsActive = true';
		}
		else
		{
			//lastRunATM = lastRunATM.addDays(-2);
			
	    	// query for active account team members updated since the last time the jon ran
			atmQuery = 'Select Account__c From Sales_Assignment__c Where User__r.IsActive = true And LastModifiedDate > ' + lastRunATM.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
		}
		
		system.debug('****'+atmQuery); 
		
		//execute account team member batch
		smb_moveUserToAccountTeam_Batch runATMBatch = new smb_moveUserToAccountTeam_Batch(atmQuery);   
		database.executebatch(runATMBatch, 2000);		
		
		// synchronize territories
		syncTerritories();
		
		// synchronize territory users
		syncTerritoryUsers();
	}
	
	/*
    *   This method synchronizes the territories in Sales_Assignment__c with the Terr_Code__c
    */
    global static void syncTerritories()
    {
    	List<AggregateResult> ARs = [Select Territory_WLS__c, Territory_WLN__c 
    								 From Sales_Assignment__c
    								 Where Territory_WLS__c != null
    								 Or Territory_WLN__c != null
    								 Group By Territory_WLS__c, Territory_WLN__c];    	
    	
    	Map<String, Terr_Code__c> upsertTerrs = new Map<String, Terr_Code__c>();
    	for (AggregateResult AR : ARs)
    	{
    		String terrWLN = (String) AR.get('Territory_WLN__c');
    		String terrWLS = (String) AR.get('Territory_WLS__c');
    		
    		if (terrWLN != null && terrWLN.trim() != '')
    		{
    			terrWLN = terrWLN.trim();
    			
    			Terr_Code__c Terr = new Terr_Code__c();
    			
    			Terr.Name = terrWLN;
    			Terr.Territory_Code__c = terrWLN;
    			Terr.Type__c = 'Wireline';
    			
    			upsertTerrs.put(terrWLN, Terr);
    		}
    		
    		if (terrWLS != null && terrWLS.trim() != '')
    		{
    			terrWLS = terrWLS.trim();
    			
    			Terr_Code__c Terr = new Terr_Code__c();
    			
    			Terr.Name = terrWLS;
    			Terr.Territory_Code__c = terrWLS;
    			Terr.Type__c = 'Wireless';
    			
    			upsertTerrs.put(terrWLS, Terr);
    		}
    	}
    	
    	if (upsertTerrs.size() > 0)
    	{
    		upsert upsertTerrs.values() Territory_Code__c;
    	}
    }
	
	/*
    *   This method synchronizes the territories users in Sales_Assignment__c with the Territory_User__c
    */
    @future
    global static void syncTerritoryUsers()
    {
    	// query for territories
    	List<Terr_Code__c> Terrs = [Select Id, Territory_Code__c
    								From Terr_Code__c];
    								
    	Map<String, Id> TerrCodeMap = new Map<String, Id>();
    	for (Terr_Code__c Terr : Terrs)
    	{
    		TerrCodeMap.put(Terr.Territory_Code__c, Terr.Id);
    	}
    					
    	// query for wireline territory users			
    	List<AggregateResult> wlnARs = [Select User__c, Territory_WLN__c, User__r.EmployeeId__c TID
										From Sales_Assignment__c 
										Where Role__c = 'ACCTPRIME'
										And Territory_WLN__c != null
										And User__r.IsActive = true
										Group By User__c, Territory_WLN__c, User__r.EmployeeId__c];
    	
    	Map<String, Territory_User__c> updatedTerrUsers = new Map<String, Territory_User__c>();
    	for (AggregateResult AR : wlnARs)
    	{
    		String terrWLN = (String) AR.get('Territory_WLN__c');
    		String TID = (String) AR.get('TID');
    		Id uID = (Id) AR.get('User__c');
    		
    		if (TID == null || TID.trim() == '')
    		{
    			continue;
    		}
    		
			Territory_User__c TerrUser = new Territory_User__c();
			
			TerrUser.Name = terrWLN + '-' + TID;
			TerrUser.TerritoryUser__c = terrWLN + '-' + TID;
			TerrUser.Territory__c = TerrCodeMap.get(terrWLN);
			TerrUser.Type__c = 'Wireline';
			TerrUser.User__c = uID;
			
			updatedTerrUsers.put(TerrUser.TerritoryUser__c, TerrUser);
    	}

		// query for wireless territory users			
    	List<AggregateResult> wlsARs = [Select User__c, Territory_WLS__c, User__r.EmployeeId__c TID
										From Sales_Assignment__c 
										Where Role__c = 'ACCTPRIME'
										And Territory_WLS__c != null
										And User__r.IsActive = true
										Group By User__c, Territory_WLS__c, User__r.EmployeeId__c];
										
    	for (AggregateResult AR : wlsARs)
    	{
    		String terrWLS = (String) AR.get('Territory_WLS__c');
    		String TID = (String) AR.get('TID');
    		Id uID = (Id) AR.get('User__c');
    		
    		if (TID == null || TID.trim() == '')
    		{
    			continue;
    		}
    		
			Territory_User__c TerrUser = new Territory_User__c();
			
			TerrUser.Name = terrWLS + '-' + TID;
			TerrUser.TerritoryUser__c = terrWLS + '-' + TID;
			TerrUser.Territory__c = TerrCodeMap.get(terrWLS);
			TerrUser.Type__c = 'Wireline';
			TerrUser.User__c = uID;
			
			updatedTerrUsers.put(TerrUser.TerritoryUser__c, TerrUser);
    	}
    	
    	if (updatedTerrUsers.size() > 0)
    	{
    		upsert updatedTerrUsers.values() TerritoryUser__c;
    	}
    }
}