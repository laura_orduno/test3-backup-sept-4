@isTest(seeAllData = true)
public class smb_Test_AccountUtility {

    static testMethod void testAccountUtilityGlobal() {

        List<Id> tempIds = smb_AccountUtilityGlobal.getAccountsInHierarchy('0014000000PctSw','0014000000PctSw');
        tempIds = smb_AccountUtilityGlobal.getAccountsInHierarchy('0014000000PctSw');
        Id tempId = smb_AccountUtilityGlobal.getRcidAccountId('0014000000PctSw');
        tempId = smb_AccountUtilityGlobal.getRcidAccountId('0014000000rfsYbAAI');
        tempId = smb_AccountUtilityGlobal.getRcidAccountId('0014000000c0kM2AAI');        

        Set<Id> fixedSearchResults= new Set<Id>{'0014000000QJzuZAAT',
                        '0014000000bydkKAAQ',
                        '0014000000r44OGAAY',
                        '0014000000qt3SnAAI',
                        '0014000000r2MOwAAM',
                        '0014000000r4N1VAAU'};
 
        Map<Id,Id> tempIdsMap = smb_AccountUtilityGlobal.getRcidAccountId(fixedSearchResults);
        tempId = smb_AccountUtilityGlobal.getUltimateParentId('0014000000PctSw');
        
        Account tempAcc = [SELECT Id, CBUCID_Parent_Id__c, CBUCID_RCID__c, CAN__c, rcid__c, ParentId, CBUCID_Name_formula__c, Name, RecordType.DeveloperName, Inactive__c, Account_Status__c
                   FROM Account
                   WHERE Id='0014000000PctSw'];

        String tempIdStr = smb_AccountUtilityGlobal.getAccountNumber(tempAcc);

        tempAcc = [SELECT Id, CBUCID_Parent_Id__c, CBUCID_RCID__c, rcid__c, CAN__c, ParentId, CBUCID_Name_formula__c, Name, RecordType.DeveloperName, Inactive__c, Account_Status__c
                   FROM Account
                   WHERE Id='0014000000rfsYbAAI'];

        tempIdStr = smb_AccountUtilityGlobal.getAccountNumber(tempAcc);
        
        tempIdStr = smb_AccountUtilityGlobal.getAccountNumberLabel('CBUCID');
        tempIdStr = smb_AccountUtilityGlobal.getAccountNumberLabel('RCID');
        tempIdStr = smb_AccountUtilityGlobal.getAccountNumberLabel('BAN');
        tempIdStr = smb_AccountUtilityGlobal.getAccountNumberLabel('CAN');
        tempIdStr = smb_AccountUtilityGlobal.getAccountNumberLabel('BCAN');
        tempIdStr = smb_AccountUtilityGlobal.getAccountNumberLabel('test');
        
        tempIdStr = smb_AccountUtilityGlobal.getAccountNumberShortLabel('CBUCID');
        tempIdStr = smb_AccountUtilityGlobal.getAccountNumberShortLabel('RCID');
        tempIdStr = smb_AccountUtilityGlobal.getAccountNumberShortLabel('BAN');
        tempIdStr = smb_AccountUtilityGlobal.getAccountNumberShortLabel('CAN');
        tempIdStr = smb_AccountUtilityGlobal.getAccountNumberShortLabel('BCAN');
        tempIdStr = smb_AccountUtilityGlobal.getAccountNumberShortLabel('test');

    }
    
    static testMethod void testAccountUtility() {

        List<Id> tempIds = smb_AccountUtility.getAccountsInHierarchy('0014000000PctSw','0014000000PctSw');
        tempIds = smb_AccountUtility.getAccountsInHierarchy('0014000000PctSw');
        Id tempId = smb_AccountUtility.getRcidAccountId('0014000000PctSw');
        tempId = smb_AccountUtility.getRcidAccountId('0014000000rfsYbAAI');
        tempId = smb_AccountUtility.getRcidAccountId('0014000000c0kM2AAI');        

        List<Account> acctList = [SELECT Id, CBUCID_Parent_Id__c, CBUCID_RCID__c, CAN__c, rcid__c, ParentId, CBUCID_Name_formula__c, Name, RecordType.DeveloperName, Inactive__c, Account_Status__c
                   FROM Account
                   WHERE Id='0014000000PctSw' or Id='0014000000rfsYbAAI' or Id='0014000000c0kM2AAI'];
                   
        Map<Id,Id> acctMaps = smb_AccountUtility.getAccountsInHierarchy(acctList );
        //acctMaps = smb_AccountUtility.getAccountsInHierarchyToCBUCID(acctList );

        Set<Id> fixedSearchResults= new Set<Id>{'0014000000QJzuZAAT',
                        '0014000000bydkKAAQ',
                        '0014000000r44OGAAY',
                        '0014000000qt3SnAAI',
                        '0014000000r2MOwAAM',
                        '0014000000r4N1VAAU'};
 
        Map<Id,Id> tempIdsMap = smb_AccountUtility.getRcidAccountId(fixedSearchResults);
        tempId = smb_AccountUtility.getUltimateParentId('0014000000PctSw');
        
        Account tempAcc = [SELECT Id, CBUCID_Parent_Id__c, CBUCID_RCID__c, CAN__c, rcid__c, ParentId, CBUCID_Name_formula__c, Name, RecordType.DeveloperName, Inactive__c, Account_Status__c
                   FROM Account
                   WHERE Id='0014000000PctSw'];

        String tempIdStr = smb_AccountUtility.getAccountNumber(tempAcc);

        tempAcc = [SELECT Id, CBUCID_Parent_Id__c, CBUCID_RCID__c, rcid__c, CAN__c, ParentId, CBUCID_Name_formula__c, Name, RecordType.DeveloperName, Inactive__c, Account_Status__c
                   FROM Account
                   WHERE Id='0014000000rfsYbAAI'];

        tempIdStr = smb_AccountUtility.getAccountNumber(tempAcc);
        
        tempIdStr = smb_AccountUtility.getAccountNumberLabel('CBUCID');
        tempIdStr = smb_AccountUtility.getAccountNumberLabel('RCID');
        tempIdStr = smb_AccountUtility.getAccountNumberLabel('BAN');
        tempIdStr = smb_AccountUtility.getAccountNumberLabel('CAN');
        tempIdStr = smb_AccountUtility.getAccountNumberLabel('BCAN');
        tempIdStr = smb_AccountUtility.getAccountNumberLabel('test');
        
        tempIdStr = smb_AccountUtility.getAccountNumberShortLabel('CBUCID');
        tempIdStr = smb_AccountUtility.getAccountNumberShortLabel('RCID');
        tempIdStr = smb_AccountUtility.getAccountNumberShortLabel('BAN');
        tempIdStr = smb_AccountUtility.getAccountNumberShortLabel('CAN');
        tempIdStr = smb_AccountUtility.getAccountNumberShortLabel('BCAN');
        tempIdStr = smb_AccountUtility.getAccountNumberShortLabel('test');

    }
}