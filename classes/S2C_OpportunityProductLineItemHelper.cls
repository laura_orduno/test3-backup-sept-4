public class S2C_OpportunityProductLineItemHelper {
	
    public static void updateSRContractLength(List<Opp_Product_Item__c> oppProdLineItemsNew, List<Opp_Product_Item__c> oppProdLineItemsOld){
        Map<Id, Opp_Product_Item__c> oppProdLineItemMapNew = new Map<Id, Opp_Product_Item__c>();
        Map<Id, Opp_Product_Item__c> oppProdLineItemMapOld = new Map<Id, Opp_Product_Item__c>();
		Set<Id> changedOppProdLineItemIds = new Set<Id>();
            
        for(Opp_Product_Item__c oppProdLineItemNew : oppProdLineItemsNew){
            oppProdLineItemMapNew.put(oppProdLineItemNew.id, oppProdLineItemNew); 
        }    
        if(oppProdLineItemsOld != null){
            for(Opp_Product_Item__c oppProdLineItemOld : oppProdLineItemsOld){
                oppProdLineItemMapOld.put(oppProdLineItemOld.id, oppProdLineItemOld); 
            }    
        }    
        
        for(Id oppProdLineItemId : oppProdLineItemMapNew.keyset()){
            Opp_Product_Item__c oppProdLineItemNew = oppProdLineItemMapNew.get(oppProdLineItemId);
            Opp_Product_Item__c oppProdLineItemOld = oppProdLineItemMapOld.get(oppProdLineItemId);
            
            // check if contract length has changed
            if(oppProdLineItemNew != null && oppProdLineItemOld != null && oppProdLineItemNew.Contract_Length_Month__c != oppProdLineItemOld.Contract_Length_Month__c){
            	changedOppProdLineItemIds.add(oppProdLineItemNew.id); 
            }            
        }    

        List<Service_Request__c> serviceRequests = [SELECT id, contract_length__c, Opportunity_Product_Item__c FROM Service_Request__c WHERE Opportunity_Product_Item__c IN :changedOppProdLineItemIds];
        
        for(Service_Request__c serviceRequest : serviceRequests){   
            Opp_Product_Item__c oppProdLineItem = oppProdLineItemMapNew.get(serviceRequest.Opportunity_Product_Item__c);
            serviceRequest.contract_length__c = oppProdLineItem.Contract_Length_Month__c;
        }    
        
        try{
            update serviceRequests;
        }
        catch(DmlException e){
            throw e;
        }
    } 
}