public without sharing class trac_Forecast_Report {

    private Set<Id> setReg_Users;
    private Set<Id> setEnt_Users; 

    public trac_Forecast_Report() {

        //Populate User Sets                
        setEnt_Users = this.getEnterpriseUsers();
        setReg_Users = this.getRegularUsers();
    }
    
    private Set<Id> getEnterpriseUsers() {
        
        Set<Id> ProfileIds = new Set<Id>();
                
        Map<Id, Profile> pMap = new Map<Id, Profile>([SELECT Id FROM Profile WHERE Name IN ('ENT Sales Manager / Director / MD','ENT Sales Prime/ASR/Coordinator')]);

        ProfileIds = pMap.keySet();         
        
        Map<Id, User> uMap = new Map<Id, User>([SELECT Id FROM User WHERE ProfileId IN :ProfileIds AND IsActive = true]);
        
        return uMap.keySet();
    }

    private Set<Id> getRegularUsers() {
                
        Set<Id> ProfileIds = new Set<Id>();

        Map<Id, Profile> pMap = new Map<Id, Profile>([SELECT Id FROM Profile WHERE Name NOT IN ('ENT Sales Manager / Director / MD','ENT Sales Prime/ASR/Coordinator')]);

        ProfileIds = pMap.keySet();         
        
        Map<Id, User> uMap = new Map<Id, User>([SELECT Id FROM User WHERE ProfileId IN :ProfileIds AND IsActive = true]);
        
        return uMap.keySet();
    }
        
    private AggregateResult[] OppsByAccountOwnerId_TCG() {

        AggregateResult[] mySum = new List<AggregateResult>();
        
        mySum = [SELECT Opportunity.Account.OwnerId, Close_Period__c, SUM(TCG_Non_Mobility_For_Reporting__c) FROM Opportunity
                        WHERE TCG_Non_Mobility_For_Reporting__c > 0
                            AND Close_Period__c = THIS_FISCAL_YEAR
                            AND IsWon = true
                            AND Opportunity.Account.OwnerId IN :setEnt_Users
                        GROUP BY Opportunity.Account.OwnerId, Close_Period__c];

        return mySum;
    }

    private AggregateResult[] OppsByOwnerId_TCG() {

        AggregateResult[] mySum = new List<AggregateResult>();
        
        mySum = [SELECT OwnerId, Close_Period__c, SUM(TCG_Non_Mobility_For_Reporting__c) FROM Opportunity
                        WHERE TCG_Non_Mobility_For_Reporting__c > 0
                            AND Close_Period__c = THIS_FISCAL_YEAR  
                            AND IsWon = true
                            AND OwnerId IN :setReg_Users                    
                        GROUP BY OwnerId, Close_Period__c];

        return mySum;
    }

    private AggregateResult[] OppsQtyByAccountOwnerId_Load() {

        AggregateResult[] mySum = new List<AggregateResult>();
        
        mySum = [SELECT Opportunity.Account.OwnerId, Close_Period__c, SUM(Forecast_Report_Mobility__c) FROM Opportunity
                        WHERE Forecast_Report_Mobility__c > 0
                            AND Close_Period__c = THIS_FISCAL_YEAR
                            AND IsWon = true
                            AND Opportunity.Account.OwnerId IN :setEnt_Users
                        GROUP BY Opportunity.Account.OwnerId, Close_Period__c];

        return mySum;
    }
            
    private AggregateResult[] OppsQtyByOwnerId_Load() {

        AggregateResult[] mySum = new List<AggregateResult>();
        
        mySum = [SELECT OwnerId, Close_Period__c, SUM(Forecast_Report_Mobility__c) FROM Opportunity
                        WHERE Forecast_Report_Mobility__c > 0
                            AND Close_Period__c = THIS_FISCAL_YEAR
                            AND IsWon = true
                            AND OwnerId IN :setReg_Users
                        GROUP BY OwnerId, Close_Period__c];
        
        return mySum;
    }
    
    
    //START - Target - Load  
    public void getForecasts_Target_Load() {
        
        Forecast_Report__c[] delReport = [SELECT Id FROM Forecast_Report__c WHERE Target_Load__c > 0 AND IsDeleted = false];
        
        delete delReport;
        
        
        Forecasting__c[] myFs = [SELECT Id, Amount_Type__c, Amount__c, Over_Ride_Value__c, Period__c, OwnerId, Role_Name__c 
                                FROM Forecasting__c
                                WHERE Amount_TYPE__c IN ('Target - Load')
                                    AND Period__c = THIS_FISCAL_YEAR
                                ORDER BY Period__c];
        
        List<Forecast_Report__c> insReport = new List<Forecast_Report__c>();
        
        //Create Records Based On Targets
        for(Forecasting__c myF :myFs) {
            
            if(myF.Amount_TYPE__c == 'Target - Load') {
                
                Forecast_Report__c myR = new Forecast_Report__c();
                
                myR.OwnerId = myF.OwnerId;
                myR.Target_Load__c = myF.Amount__c;
                myR.Period__c = myF.Period__c;
                myR.User__c = myF.OwnerId;
                myR.Amount_TYPE__C = myF.Amount_TYPE__c;
                
                insReport.Add(myR);
            }           
        }

        //Start Populate Load Actuals
        Map<String, Decimal> mapOppLoadActuals = new Map<String, Decimal>();
        
        AggregateResult[] myOppLoads = this.OppsQtyByOwnerId_Load();
        
        for (AggregateResult myR :myOppLoads) {
            
            mapOppLoadActuals.put(String.ValueOf(myR.get('Close_Period__c')) + '|' + String.ValueOf(myR.get('OwnerId')), Double.ValueOf(myR.get('expr0')) );
        }
        //End Populate Load Actuals
        
        //Start Populate Load Actuals By Account
        Map<String, Decimal> mapOppLoadActualsByAccount = new Map<String, Decimal>();
        
        AggregateResult[] myOppLoadsByAccount = this.OppsQtyByAccountOwnerId_Load();
        
        for (AggregateResult myR :myOppLoadsByAccount) {
            
            mapOppLoadActualsByAccount.put(String.ValueOf(myR.get('Close_Period__c')) + '|' + String.ValueOf(myR.get('OwnerId')), Double.ValueOf(myR.get('expr0')) );
        }
        //End Populate Load Actuals By Account
        
        for(Forecast_Report__c myR :insReport) {
            
            if(setReg_Users.Contains(myR.OwnerId)) {
            
                Decimal dOppLoadActual = mapOppLoadActuals.get(myR.Period__c + '|' + myR.OwnerId);
                
                myR.Actual_Load__c = dOppLoadActual;
            }
            else {
        
                Decimal dOppLoadActualByAccount = mapOppLoadActualsByAccount.get(myR.Period__c + '|' + myR.OwnerId);
                
                myR.Actual_Load__c = dOppLoadActualByAccount;
            }   
        }
        
        Id BatchId = Database.executeBatch(new trac_Forecast_Report_Batch_Insert(insReport));       
    }
    //END - Target - Load

    
    //START - Target - TCG
    public void getForecasts_Target_TCG() {
        
        Forecast_Report__c[] delReport = [SELECT Id FROM Forecast_Report__c WHERE Target_TCG__c > 0 AND IsDeleted = false];
        
        delete delReport;
        
        
        Forecasting__c[] myFs = [SELECT Id, Amount_Type__c, Amount__c, Over_Ride_Value__c, Period__c, OwnerId, Role_Name__c
                                FROM Forecasting__c
                                WHERE Amount_TYPE__c IN ('Target - TCG')
                                    AND Period__c = THIS_FISCAL_YEAR
                                ORDER BY Period__c];
        
        List<Forecast_Report__c> insReport = new List<Forecast_Report__c>();
        
        //Create Records Based On Targets
        for(Forecasting__c myF :myFs) {
            
            if(myF.Amount_TYPE__c == 'Target - TCG') {
                
                Forecast_Report__c myR = new Forecast_Report__c();
                
                myR.OwnerId = myF.OwnerId;
                myR.Target_TCG__c = myF.Amount__c;
                myR.Period__c = myF.Period__c;
                myR.User__c = myF.OwnerId;
                myR.Amount_TYPE__C = myF.Amount_TYPE__c;
                                
                insReport.Add(myR);
            }           
        }

        //Start Populate TCG Actuals
        Map<String, Decimal> mapOppTCGActuals = new Map<String, Decimal>();
        
        AggregateResult[] myOppTCGs = this.OppsByOwnerId_TCG();
        
        for (AggregateResult myR :myOppTCGs) {
            
            mapOppTCGActuals.put(String.ValueOf(myR.get('Close_Period__c')) + '|' + String.ValueOf(myR.get('OwnerId')), Double.ValueOf(myR.get('expr0')) );
        }
        //End Populate TCG Actuals
        
        //Start Populate TCG Actuals By Account
        Map<String, Decimal> mapOppTCGActualsByAccount = new Map<String, Decimal>();
        
        AggregateResult[] myOppTCGsByAccount = this.OppsByAccountOwnerId_TCG();
        
        for (AggregateResult myR :myOppTCGsByAccount) {
            
            mapOppTCGActualsByAccount.put(String.ValueOf(myR.get('Close_Period__c')) + '|' + String.ValueOf(myR.get('OwnerId')), Double.ValueOf(myR.get('expr0')) );
        }
        //End Populate TCG Actuals By Account
        
        for(Forecast_Report__c myR :insReport) {
            
            if(setReg_Users.Contains(myR.OwnerId)) {
            
                Decimal dOppTCGActual = mapOppTCGActuals.get(myR.Period__c + '|' + myR.OwnerId);
                
                myR.Actual_TCG__c = dOppTCGActual;
            }
            else {
        
                Decimal dOppTCGActualByAccount = mapOppTCGActualsByAccount.get(myR.Period__c + '|' + myR.OwnerId);
                
                myR.Actual_TCG__c = dOppTCGActualByAccount;
            }   
        }
        
        Id BatchId = Database.executeBatch(new trac_Forecast_Report_Batch_Insert(insReport));
    }
    //END - Target - TCG


    //START - Commit - Load
    public void getForecasts_Commit_Load() {
        
        Forecast_Report__c[] delReport = [SELECT Id 
        								  FROM Forecast_Report__c 
        								  WHERE IsDeleted = false AND (Amount_TYPE__c IN ('Final Commit Loads', 'Final Commit Loads - Acct Based') OR Commit_Load__c > 0)];
        
        delete delReport;
        
        
        Forecasting__c[] myFs = [SELECT Id, Amount_Type__c, Amount__c, Over_Ride_Value__c, Period__c, OwnerId, Role_Name__c
                                FROM Forecasting__c
                                WHERE Amount_TYPE__c IN ('Final Commit Loads','Final Commit Loads - Acct Based')
                                    AND Period__c = THIS_FISCAL_YEAR
                                ORDER BY Period__c];
        
        List<Forecast_Report__c> insReport = new List<Forecast_Report__c>();
        
        //Create Records Based On Commits
        for(Forecasting__c myF :myFs) {
                            
            if((setReg_Users.Contains(myF.OwnerId) && myF.Amount_TYPE__c == 'Final Commit Loads')
                || (setEnt_Users.Contains(myF.OwnerId) && myF.Amount_TYPE__c == 'Final Commit Loads - Acct Based')) {
                
                Forecast_Report__c myR = new Forecast_Report__c();
                    
                myR.OwnerId = myF.OwnerId;
                myR.Period__c = myF.Period__c;
                myR.User__c = myF.OwnerId;
                myR.Forecast_Role__c = myF.Role_Name__c;
                myR.Amount_TYPE__C = myF.Amount_TYPE__c;

                if(myF.Over_Ride_Value__c > 0) {
                    
                    myR.Commit_Load__c = myF.Over_Ride_Value__c;
                    myR.Override__c = true;
                }
                else
                    myR.Commit_Load__c = myF.Amount__c;
                    
                insReport.Add(myR);
            }
        }
        
        String sQuery = 'SELECT Id, OwnerId, Account.OwnerId, Account.Owner.Manager_1__c, Account.Owner.Manager_2__c, Account.Owner.Manager_3__c, Account.Owner.Manager_4__c, Owner.Manager_1__c, Owner.Manager_2__c, Owner.Manager_3__c, Owner.Manager_4__c, Amount, Close_Period__c, Forecast_Report_Mobility__c FROM Opportunity WHERE Forecast_Report_Mobility__c > 0 AND Close_Period__c = THIS_FISCAL_YEAR AND Probability = 100';
        
        /*
        if(test.isRunningTest()) {
        
            List<Forecast_Report__c> myTest = new List<Forecast_Report__c>();
        
            sQuery = sQuery = ' LIMIT 100';
                        
            for(Integer i=0;i<100;i++) {
                
                myTest.add(insReport[i]);
            }
            
            //System.Debug('Forecast Record Count: ' + myTest.Size());
            
            Id BatchTest = Database.executeBatch(new trac_Forecast_Report_Batch_Insert(myTest));            
        } 
        */            
        //if(!test.isRunningTest())
        
        Id BatchId = Database.executeBatch(new trac_Forecast_Report_Batch_Insert(insReport));

        Id BatchId2 = Database.executeBatch(new trac_Forecast_Report_Batch_Commit_Load(sQuery, setReg_Users, setEnt_Users));     
    }
    //END - Commit - Load


    //START - Commit - TCG
    public void getForecasts_Commit_TCG() {
        
        Forecast_Report__c[] delReport = [SELECT Id 
        								  FROM Forecast_Report__c 
        								  WHERE IsDeleted = false AND (Amount_TYPE__c IN ('Final Commit TCG','Final Commit TCG - Acct Based') OR Commit_TCG__c > 0)];
        /*if (delReport.size() >= 10000) {
        	delete delReport;
        	delReport = [SELECT Id FROM Forecast_Report__c WHERE IsDeleted = false AND Commit_TCG__c != null LIMIT 10000];
        }*/
        delete delReport;
        
        
        
        Forecasting__c[] myFs = [SELECT Id, Amount_Type__c, Amount__c, Over_Ride_Value__c, Period__c, OwnerId, Role_Name__c
                                FROM Forecasting__c
                                WHERE Amount_TYPE__c IN ('Final Commit TCG','Final Commit TCG - Acct Based')
                                    AND Period__c = THIS_FISCAL_YEAR
                                ORDER BY Period__c];
        
        List<Forecast_Report__c> insReport = new List<Forecast_Report__c>();
        
        //Create Records Based On Commits
        for(Forecasting__c myF :myFs) {
                            
            if((setReg_Users.Contains(myF.OwnerId) && myF.Amount_TYPE__c == 'Final Commit TCG')
                || (setEnt_Users.Contains(myF.OwnerId) && myF.Amount_TYPE__c == 'Final Commit TCG - Acct Based')) {
                
                Forecast_Report__c myR = new Forecast_Report__c();
                    
                myR.OwnerId = myF.OwnerId;
                myR.Period__c = myF.Period__c;
                myR.User__c = myF.OwnerId;
                myR.Forecast_Role__c = myF.Role_Name__c;
                myR.Amount_TYPE__C = myF.Amount_TYPE__c;
                    
                if(myF.Over_Ride_Value__c > 0) {
                    
                    myR.Commit_TCG__c = myF.Over_Ride_Value__c;
                    myR.Override__c = true;
                }
                else
                    myR.Commit_TCG__c = myF.Amount__c;
                
                insReport.Add(myR);
            }
        }
        
        String sQuery = 'SELECT Id, OwnerId, Account.OwnerId, Account.Owner.Manager_1__c, Account.Owner.Manager_2__c, Account.Owner.Manager_3__c, Account.Owner.Manager_4__c, Owner.Manager_1__c, Owner.Manager_2__c, Owner.Manager_3__c, Owner.Manager_4__c, Amount, Close_Period__c, TCG_Non_Mobility_For_Reporting__c FROM Opportunity WHERE TCG_Non_Mobility_For_Reporting__c > 0 AND Close_Period__c = THIS_FISCAL_YEAR AND Probability = 100';
        
        Id BatchId = Database.executeBatch(new trac_Forecast_Report_Batch_Insert(insReport));
        
        Id BatchId2 = Database.executeBatch(new trac_Forecast_Report_Batch_Commit_TCG(sQuery, setReg_Users, setEnt_Users));             
    }
    //END - Commit - TCG
}