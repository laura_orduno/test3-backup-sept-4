/*
 * Controller for the RepairCreate page
 * Part of the SMB Repairs project
 *
 * @author: Grant Adamson, Traction On Demand 
 */

public without sharing class trac_RepairCreateCtlr extends trac_RepairCaseCtlr{
	
	public Boolean missingFields {get; private set;}
		
	public trac_RepairCreateCtlr() {
		super();
		missingFields = false;
	}
	
	
	public void doCreateRepair() {	
		// check for missing required fields (business requirement, not required for callout)
		Boolean blankFields = false;
		
		if(theCase.Issue_System_sfdc_only__c == null) {
			trac_PageUtils.addError(Label.Issue_System_Required);
			blankFields = true;
		}
		if(theCase.Defects_behaviour__c == null) {
			trac_PageUtils.addError(Label.Defects_Behaviour_Required);
			blankFields = true;
		}
		if(theCase.Descrip_of_Defect_Troubleshooting_Perf__c == null) {
			trac_PageUtils.addError(Label.Description_of_Defect_Troubleshooting_Required);
			blankFields = true;
		}
		
		if(theCase.Use_Alternate_Address__c && 
			(theCase.Address_shipping__c == null ||
			 theCase.City_shipping__c == null ||
			 theCase.Province_shipping__c == null ||
			 theCase.Postal_Code_shipping__c == null)
		  ) {
			trac_PageUtils.addError(Label.Shipping_Address_Required);
			blankFields = true;
		}
		
		if(blankFields) {
			complete = true;
			missingfields = true;
			return;
		}
		
		// call create repair service and process response
		trac_RepairCalloutResponse resp = trac_RepairCallout.initCreateRepair(theCase);
		
		if(resp.success) {
			success = true;
		} else {
			trac_PageUtils.addError(resp.messageBody);
		}
		
		complete = true;
		update theCase;
	}
}