@isTest
private class TestEmailNotiForCustStatusTools{
    static testmethod void testEmailNotification(){
         //create user -Start
             Profile p = [select id from Profile where name like 'System Admin%' limit 1];  
             User u = new User(alias = 'standt', email='standarduser@testorg.com', emailencodingkey='UTF-8',
                             languagelocalekey='en_US', localesidkey='en_US', timezonesidkey='America/Los_Angeles', 
                             username='u@testorg.com', firstname = 'fname', lastname = 'lname', profileId = p.Id);
             insert u;
         //create user -End
         
         //create Employee -Start          
             Employee__c e = new Employee__c(Name='Employee', Director_Email__c='d@d.com', CST_Email_Yellow__c=true, Email__c='e@e.com', Manager_Email__c='m@m.com', User__c = u.Id, Employee_ID__c='abcd1234', TMODS_TEAM_MEMBER_ID__c = 98765);
             insert e;
               
             Employee__c e3 = new Employee__c(Name='Employee3', Director_Email__c='d@d.com', CST_Email_Red__c=true, Email__c='e@e.com', Manager_Email__c='m@m.com', User__c = u.Id, Employee_ID__c='abcd1235', TMODS_TEAM_MEMBER_ID__c = 98765);
             insert e3;
         //create Employee -End
         
         //create CBUCID-Start          
             Cust_Business_Unit_Cust_ID__c  c = new Cust_Business_Unit_Cust_ID__c(Name='CBUCID', CBUCID__c='0000203040',CBU_Name__c='CBUCIDName', CBU_CD__c='CB' );
             insert c;
               
             Cust_Business_Unit_Cust_ID__c  c2 = new Cust_Business_Unit_Cust_ID__c(Name='CBUCID2', CBUCID__c='0000203050',CBU_Name__c='CBUCIDName2', CBU_CD__c='CB2' );
             insert c2;
             
             Cust_Business_Unit_Cust_ID__c  c3 = new Cust_Business_Unit_Cust_ID__c(Name='CBUCID3', CBUCID__c='0000203060',CBU_Name__c='CBUCIDName3', CBU_CD__c='CB3' );
             insert c3;
         //create CBUCID-End
         
         //create AtoZ-Start          
             AtoZ_Assignment__c  aToz1 = new AtoZ_Assignment__c( AtoZ_Contact__c=e.id, AtoZ_Field__c='SvcSpec_WLS_CSM', CBUCID__c=c.id, Email__c='a@a.com' );
             insert aToz1;
              
             AtoZ_Assignment__c  aToz2 = new AtoZ_Assignment__c( AtoZ_Contact__c=e.id, AtoZ_Field__c='SvcSpec_WLS_CSM', CBUCID__c=c2.id, Email__c='a@a.com' );
             insert aToz2;
                
             AtoZ_Assignment__c  aToz3 = new AtoZ_Assignment__c( AtoZ_Contact__c=e3.id, AtoZ_Field__c='SvcSpec_WLS_CSM', CBUCID__c=c3.id, Email__c='a@a.com' );
             insert aToz3;
         //create AtoZ-End
         
         //create Account - start
              Account a= new Account(name='test account',cbucid__c='0000203040', CBUCID_Name__c=c.id);
              insert a;
         //create Account - End
         //create Account - start
              Account a2= new Account(name='test account2', cbucid__c='0000203050', CBUCID_Name__c=c2.id);
              insert a2;
         
              Account a3= new Account(name='test account3', cbucid__c='0000203060', CBUCID_Name__c=c3.id);
              insert a3;
         //create Account - End
         
         //create Sales Assignment- start
              Sales_Assignment__c  s1= new Sales_Assignment__c(name='SalesAssignment1',Account__c=a.id, User__c=u.id, Role__c='DIR', AcctRole__c='abcd12341');
              insert s1;
              Sales_Assignment__c  s2= new Sales_Assignment__c(name='SalesAssignment2',Account__c=a.id, User__c=u.id, Role__c='WLS_DIR', AcctRole__c='abcd12342');
              insert s2;
              Sales_Assignment__c  s3= new Sales_Assignment__c(name='SalesAssignment3',Account__c=a.id, User__c=u.id, Role__c='ACCTPRIME', AcctRole__c='abcd12343');
              insert s3;
              Sales_Assignment__c  s4= new Sales_Assignment__c(name='SalesAssignment4',Account__c=a.id, User__c=u.id, Role__c='WLS_SR', AcctRole__c='abcd12344');
              insert s4;
              Sales_Assignment__c  s5= new Sales_Assignment__c(name='SalesAssignment5',Account__c=a.id, User__c=u.id, Role__c='WLS_MGR', AcctRole__c='abcd12345');
              insert s5;
        
              Sales_Assignment__c  s12= new Sales_Assignment__c(name='SalesAssignment12',Account__c=a2.id, User__c=u.id, Role__c='DIR', AcctRole__c='abcd123412');
              insert s12;
              Sales_Assignment__c  s22= new Sales_Assignment__c(name='SalesAssignment22',Account__c=a2.id, User__c=u.id, Role__c='WLS_DIR', AcctRole__c='abcd123422');
              insert s22;
              Sales_Assignment__c  s32= new Sales_Assignment__c(name='SalesAssignment32',Account__c=a2.id, User__c=u.id, Role__c='ACCTPRIME', AcctRole__c='abcd123432');
              insert s32;
              Sales_Assignment__c  s42= new Sales_Assignment__c(name='SalesAssignment42',Account__c=a2.id, User__c=u.id, Role__c='WLS_SR', AcctRole__c='abcd123442');
              insert s42;
              Sales_Assignment__c  s52= new Sales_Assignment__c(name='SalesAssignment52',Account__c=a2.id, User__c=u.id, Role__c='WLS_MGR', AcctRole__c='abcd123452');
              insert s52;
        
              Sales_Assignment__c  s13= new Sales_Assignment__c(name='SalesAssignment13',Account__c=a3.id, User__c=u.id, Role__c='DIR', AcctRole__c='abcd123413');
              insert s13;
              Sales_Assignment__c  s23= new Sales_Assignment__c(name='SalesAssignment23',Account__c=a3.id, User__c=u.id, Role__c='WLS_DIR', AcctRole__c='abcd123423');
              insert s23;
              Sales_Assignment__c  s33= new Sales_Assignment__c(name='SalesAssignment33',Account__c=a3.id, User__c=u.id, Role__c='ACCTPRIME', AcctRole__c='abcd123433');
              insert s33;
              Sales_Assignment__c  s43= new Sales_Assignment__c(name='SalesAssignment43',Account__c=a3.id, User__c=u.id, Role__c='WLS_SR', AcctRole__c='abcd123443');
              insert s43;
              Sales_Assignment__c  s53= new Sales_Assignment__c(name='SalesAssignment53',Account__c=a3.id, User__c=u.id, Role__c='WLS_MGR', AcctRole__c='abcd123453');
              insert s53;
         //create Sales Assignment- End
          
        //create aTozTool-Start 
            AtoZ_CustStatusTool__c aTozTool=new AtoZ_CustStatusTool__c();
            aTozTool.CBUCID__c='0000203040';
            aTozTool.Service_Type__c='Wireless';
            aTozTool.Status__c='Green';
            aTozTool.Notes__c='Green Distrubution';
        
            AtoZ_CustStatusTool__c aTozTool2=new AtoZ_CustStatusTool__c();
            aTozTool2.CBUCID__c='0000203050';
            aTozTool2.Service_Type__c='Wireless';
            aTozTool2.Status__c='Yellow';
            aTozTool2.Notes__c='Yellow Distrubution';
         
            AtoZ_CustStatusTool__c aTozTool3=new AtoZ_CustStatusTool__c();
            aTozTool3.CBUCID__c='0000203060';
            aTozTool3.Service_Type__c='Wireless';
            aTozTool3.Status__c='Red';
            aTozTool3.Notes__c='Red Distrubution';
        //create AtoZTool-End
        
        
        //Insert aToZ-Start
            List<AtoZ_CustStatusTool__c> aTozList=new List<AtoZ_CustStatusTool__c>();
            aTozList.add(aTozTool);
            aTozList.add(aTozTool2);
            aTozList.add(aTozTool3);
            insert aTozList;
        //Insert aToZ-Start- End
        
        
        //update on smae color by changing Notes
            aTozList.clear();
            aTozTool.Status__c='Green';
            aTozTool.Notes__c='Green Distrubution.';
            
            aTozTool2.Status__c='Red';
            aTozTool2.Notes__c='Red Distrubution.';
            
            aTozTool3.Status__c='Yellow';
            aTozTool3.Notes__c='Red Distrubution.';
            
            aTozList.add(aTozTool);
            aTozList.add(aTozTool2);
            aTozList.add(aTozTool3);
            update aTozList;
        //update on smae color by changing Notes
        
      /*  //change color to Yellow from Green
        aTozTool.Status__c='Yellow';
        update aTozTool;
                
        //update on smae color by changing Notes
        aTozTool.Status__c='Yellow';
        aTozTool.Notes__c='Yellow Distrubution.';
        update aTozTool;
        
        //change color to Red from Yellow 
        aTozTool.Status__c='Red';
        update aTozTool;
        
        //update on smae color by changing Notes
        aTozTool.Status__c='Red';
        aTozTool.Notes__c='Red Distrubution.';
        update aTozTool;
        
        //update on smae color by changing Notes
        aTozTool.Status__c='Green';
        aTozTool.Notes__c='Red Distrubution.';
        update aTozTool;
        
        //update on smae color by changing Notes
        aTozTool.Status__c='Yellow';
        aTozTool.Notes__c='Yellow Distrubution.';
        update aTozTool; */
        
    }
}