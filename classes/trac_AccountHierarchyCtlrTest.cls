@isTest
private class trac_AccountHierarchyCtlrTest {
	
	private static testMethod void testTreeHtml() {
    Account cbucid = new Account(
        Name= 'CbucidTest123', phone = '1234567890', RecordTypeId = trac_AccountHierarchyCtlr.ACCT_RT_CBUCID
    );
   
    insert cbucid;
    
    Account rcid = new Account(
        Name= 'RcidTest123', phone = '1234567890', RecordTypeId = trac_AccountHierarchyCtlr.ACCT_RT_RCID, ParentId = cbucid.Id
    );
    insert rcid;
    
    Billing_Account__c ban = new Billing_Account__c(
    	ACTIVE_IND__c = 'Y',
    	Billing_System_Account_Number__c = '123',
    	BILLG_ACCT_NO__c = '123',
    	Account__c = rcid.Id,
    	CustProfId__c = '123'
    );
    insert ban;
    
    trac_AccountHierarchyCtlr tahc = new trac_AccountHierarchyCtlr();
    tahc.accountId = rcid.id;
    tahc.getTreeHtml();
    tahc = new trac_AccountHierarchyCtlr();
    tahc.accountId = cbucid.id;
    tahc.getTreeHtml();
  }
    
	private static testMethod void testTreeHtmlTooManyBans() {
    Account cbucid = new Account(
        Name= 'CbucidTest123', phone = '1234567890', RecordTypeId = trac_AccountHierarchyCtlr.ACCT_RT_CBUCID
    );
    insert cbucid;
    Account rcid = new Account(
        Name= 'RcidTest123', phone = '1234567890', RecordTypeId = trac_AccountHierarchyCtlr.ACCT_RT_RCID, ParentId = cbucid.Id
    );
    insert rcid;

    Billing_Account__c[] bans = new List<Billing_Account__c>();
    for (Integer i = 0; i <= trac_AccountHierarchyCtlr.BAN_LIMIT; i++) {
        Billing_Account__c ban = new Billing_Account__c(
        	ACTIVE_IND__c = 'Y',
        	Billing_System_Account_Number__c = '123' + i,
        	BILLG_ACCT_NO__c = '123' + i,
        	Account__c = rcid.Id,
        	CustProfId__c = '123'
        );
        bans.add(ban);
    }
    insert bans;
    
    Test.startTest();
    trac_AccountHierarchyCtlr tahc = new trac_AccountHierarchyCtlr();
    tahc.accountId = rcid.id;
    tahc.getTreeHtml();
    Test.stopTest();
  }
}