/**
*trac_CaseTriggerDispatcher
*@description Class that extends Dispatcher class and will send a Case, along with any case comments and related devices, to 
*             Ring Central via salesforce to salesforce.
*@authoer David Barrera
*@Date 8/28/14
*/
public with sharing class trac_CaseTriggerDispatcher extends Dispatcher{

    public List<Case> newList {get; set;}
    public List<Case> oldList {get; set;}
    public Map<Id, Case> newMap {get; set;}
    public Map<Id, Case> oldMap {get; set;}
    public static boolean firstRun = true;

    //Change by Belmar - 26AUG2015 - "RecordTypeInfosByName" with string not supporting French translated recordtype name
    //private final Id rcid = Schema.SObjectType.Account.RecordTypeInfosByName.get('RCID').RecordTypeId;
    private final Id rcid = Schema.SObjectType.Account.RecordTypeInfosByName.get(Label.RecordType_Account_RCID).RecordTypeId;
    
    //Change by Belmar - 26AUG2015 - "RecordTypeInfosByName" with string not supporting French translated recordtype name
    //private final Id banReID = Schema.SObjectType.Account.RecordTypeInfosByName.get('BAN').RecordTypeId;
    private final Id banReID = Schema.SObjectType.Account.RecordTypeInfosByName.get(Label.RecordType_Account_BAN).RecordTypeId;

    //Change by Belmar - 26AUG2015 - "RecordTypeInfosByName" with string not supporting French translated recordtype name
    //private final Id telusServiceId = Schema.SObjectType.Case.RecordTypeInfosByName.get('SMB Care TELUS Service').RecordTypeId;
    private final Id telusServiceId = Schema.SObjectType.Case.RecordTypeInfosByName.get(Label.RecordType_Case_SMB_Care_TELUS_Service).RecordTypeId;
    
    //Change by Belmar - 26AUG2015 - "RecordTypeInfosByName" with string not supporting French translated recordtype name
    //private final Id telusTechnicalId = Schema.SObjectType.Case.RecordTypeInfosByName.get('SMB Care Technical Support').RecordTypeId;
    private final Id telusTechnicalId = Schema.SObjectType.Case.RecordTypeInfosByName.get(Label.RecordType_Case_SMB_Care_Technical_Support).RecordTypeId;
    
    //private final Id currentUserId = UserInfo.getUserId() ;// to be used if current Id is need instead of owner of Case.
    //private final Id careTechSupport = Schema.SObjectType.Case.RecordTypeInfosByName.get('SMB_Care_Technical_Support').RecordTypeId;


    public trac_CaseTriggerDispatcher() {
        super();
    }

    public override void init() {

        newList = (trigger.new != null) ? (List<Case>)trigger.new : new List<Case>();
        oldList = (trigger.old != null) ? (List<Case>)trigger.old : new List<Case>();
        newMap = (trigger.newMap != null) ? (Map<Id, Case>)trigger.newMap : new Map<Id, Case>();
        oldMap = (trigger.oldMap != null) ? (Map<Id, Case>)trigger.oldMap : new Map<Id, Case>();

    }

    public override void execute(){
        //when recieiving a case from ring central make sure that the case has the appropriate record type
        if(LimitExecutionEvent(TriggerEvent.BEFOREINSERT)){
            recieveCase(newList);
        }    
         //send a case to ring central
        if(  LimitExecutionEvent(TriggerEvent.AFTERINSERT) ){
            if(LimitExecutionCount('trac_CaseTriggerDispatcher_sendCase', 1)){
                System.Debug('updating Cases...');
                  updateCaseInfo(newList);
            }
            if( LimitExecutionCount('trac_CaseTriggerDispatcher_touchCases', 1) ){
                touchCases(newList);
            }
        }   
        if( LimitExecutionEvent(TriggerEvent.AFTERUPDATE)  ){
            if(LimitExecutionCount('trac_CaseTriggerDispatcher_sendCase', 1)){
                    System.Debug('updating Cases...');
                  updateCaseInfo(newList);
            }
            if( LimitExecutionCount('trac_CaseTriggerDispatcher_touchClosedCases', 1) ){
                touchClosedCases(newList);
            }
        }
    }
    /**    
    *@description Function to update or create a case in the ring central org. Will also send any devices or case comments 
    *             associated with the case. THe functionailty of sending a case VIA S2S has been moved to trac_CaseFlow
    *@authoer David Barrera
    *@Date 9/3/14
    */
    public void updateCaseInfo(List<Case> newCases){
        
        String ccQuery = '';
        String deviceQuery = '';

        Set<Id> caseWithPNRC = new Set<Id>();
        Set<Id> caseIdsToTouch = new Set<Id>();
        Set<Id> vendorIds = new Set<Id>();
        //List<PartnerNetworkRecordConnection> pnrcList = new List<PartnerNetworkRecordConnection>();
        List<Case> localCases = new List<Case>();
        List<Vendor_Communication__c> vCtoCaseComment = new List<Vendor_Communication__c>();
        
        List<CaseComment> ccToSend = new List<CaseComment>();
        List<CaseComment> ccToCreate = new List<CaseComment>();
        //List<Device__c> localDevices = new List<Device__c>();
        Map<Id,String> relatedrecords = new Map<Id,String>();
        Map<Id,Id> parentCases = new Map<Id,Id>();

        
        //Query the PartnerNetworkRecordConnection to ensure that we do not send the record twice.
        //pnrcList =[ SELECT id,status,LocalRecordId,StartDate,PartnerRecordId
        //            FROM PartnerNetworkRecordConnection
        //            WHERE LocalRecordId in :newCases
        //            AND Status != 'Inactive'];
        //for(PartnerNetworkRecordConnection pnrc: pnrcList){
        //    caseWithPNRC.add(pnrc.LocalRecordId);
        //}

        for(Case aCase :newCases){
            //System.Debug( 'Cases transfer to queue: ' + aCase.Transfer_to_Queue__c );
            if(aCase.RecordTypeId == telusServiceId || aCase.RecordTypeId == telusTechnicalId){
                if ( !caseWithPNRC.contains(aCase.Id) && aCase.ConnectionReceivedId == null && aCase.Transfer_to_Queue__c == 'RingCentral Tier 2' && aCase.Status != 'Closed'){             
                    //System.Debug( 'adding ' + aCase.Id + ' to the localCases list');
                    localCases.add(aCase);  
                }else if (aCase.ConnectionReceivedId != null ){
                    caseIdsToTouch.add(aCase.Id);
                }
            }
        }

        
        //after gettng the local cases that need to be sent over via salesforce to salesforce 
        //query all the case comments that are associated with that case, are private and are not from RC
        System.Debug( 'The size of local cases: ' + localCases.size() );
        if(localCases.size() > 0){
            ccToSend = [
                SELECT CommentBody, Id,ConnectionReceivedId, Ispublished, ParentId 
                FROM CaseComment 
                WHERE ParentId in :localCases    
                AND ParentId != null                        
                AND ConnectionReceivedId = null
            ];  

            vCtoCaseComment = [
                SELECT Id,Case__c,OwnerId, Case_Comment_ID__c
                FROM Vendor_Communication__c            
                WHERE Case__c != null
                AND Case__c in :localCases
            ]; 
            for(Vendor_Communication__c vc : vCtoCaseComment){
                vendorIds.add(vc.Case_Comment_ID__c);
            }
        }
            if(ccToSend.size() > 0 && vendorIds.size() > 0){
                for(CaseComment cc : ccToSend){
                    if(!vendorIds.contains(cc.Id)){
                        ccToCreate.add(cc);
                    }
                }
                trac_CaseCommentDispatcher.createVendorComm(ccToCreate);
            }      
            ////now get all the devices associated with the case and send those off to ring central.
            //localDevices = [
            //    SELECT VoIP_Case__c, Id,ConnectionReceivedId, Parent_Id_At_Source__c ,OwnerId
            //    FROM Device__c 
            //    WHERE VoIP_Case__c in :localCases
            //    AND VoIP_Case__c != null
            //    AND ConnectionReceivedId = null 
            //];           
        //System.Debug('localDevices.size():' + localDevices.size() );
       /*
       * Replaced with Process builder.
       */ 
        //if(localCases.size() > 0  ){
        //    System.Debug('Calling the updateChildrensOwners');
        //    updateChildrensOwners(localCases,oldMap,localDevices,vCtoCaseComment);

        //}
        //System.Debug( ccToSend.size() );
        
        

        //for(Vendor_Communication__c ven : vCtoCaseComment){
        //    if(relatedrecords.containsKey(ven.Case__c)){
        //        String tmp = relatedrecords.get(ven.Case__c);
        //        tmp = tmp + ',Vendor_Communication__c';
        //        relatedrecords.put(ven.Case__c, tmp);
        //    }
        //    else{
        //        relatedrecords.put(ven.Case__c, 'Vendor_Communication__c');
        //    }
        //}

        //for(Device__c dev:localDevices){

        //    if(relatedrecords.containsKey(dev.VoIP_Case__c)){
        //        String tmp = relatedrecords.get(dev.VoIP_Case__c);
        //        tmp = tmp + ',Device__c';
        //        relatedrecords.put(dev.VoIP_Case__c, tmp);
        //    }
        //    else{
        //        relatedrecords.put(dev.VoIP_Case__c, 'Device__c');
        //    }
        //}

        
        //if(localCases.size() > 0 ){
        //    System.Debug('Callig sendToRingCentral with related records: ' + relatedrecords);
        //    trac_S2S_connection_Helper.sendToRingCentral(localCases, null, relatedrecords, 'RingCentral',true,true,true);  
        //    getVendorCaseId(localCases);     
        //}
       

        //if(localDevices.size() > 0 && Test.isRunningTest()){
        //    trac_DeviceTriggerDispatcher.sendDevice(localDevices);
        //}
        
        
    } 
    public static void getVendorCaseId(List<Case> localCases){

        List<Case> casesToUpdate = new List<Case>();
        
        List<PartnerNetworkRecordConnection> pnrcList = new List<PartnerNetworkRecordConnection>();
        Map<Id,Id> localToPartner = new Map<Id,Id>();

        casesToUpdate = [SELECT Id,Status, Vendor_Case_ID__c
                          FROM Case
                          WHERE Id In :localCases
                          ];

        pnrcList = [SELECT Id,Status,LocalRecordId,StartDate,PartnerRecordId
                     FROM PartnerNetworkRecordConnection
                     WHERE LocalRecordId In :casesToUpdate
                     ];

        for(PartnerNetworkRecordConnection pnrc: pnrcList){            
            localToPartner.put(pnrc.LocalRecordId, pnrc.PartnerRecordId);
        }

        for(Case aCase:casesToUpdate){
            if(localToPartner.containsKey(aCase.Id)){
                aCase.Vendor_Case_ID__c = localToPartner.get(aCase.Id);
            }
        }


        if(casesToUpdate.size() > 0 ){                    
            update casesToUpdate;            
        }
    }

     /**    
    *@description Upon receiving a case from Ring Central look for appropriate accounts and contacts to add to the case. As well 
    *             insure that the case is created with the correct record type.
    *@authoer David Barrera
    *@Date 9/18/14
    */
    
    public void recieveCase(List<Case> newList){ 
        //for each of the cases check the 'case type' and find the associated record type with that type.
        Set<Id> caseIds = new Set<Id>();

        List<Case> incomingCases = new List<Case>();
        List<CaseRecordType__c> crtList = new List<CaseRecordType__c>();

        Map<String,String> caseTypeToRecordType = new Map<String,String>();

        crtList = CaseRecordType__c.getAll().values();
        for(CaseRecordType__c crt: crtList){            
            caseTypeToRecordType.put(crt.Case_Type__c, crt.Record_Type_Name__c);
        }
        for(Case aCase:newList){
            if( ( aCase.ConnectionReceivedId != null ||Test.isRunningTest() ) && caseTypeToRecordType.containsKey(aCase.Record_Type_Name__c)){
                aCase.RecordTypeId = getRecordType(caseTypeToRecordType.get(aCase.Record_Type_Name__c));
                incomingCases.add(aCase);
                caseIds.add(aCase.Id);
            }            
        }
        if(incomingCases.size() > 0){
            getAccounts(incomingCases);
            getContacts(incomingCases);
            
        }
    }

    public Id getRecordType(String recordTypeName){
        Id recordTypeID;
        if(String.isNotBlank(recordTypeName)){
            recordTypeID = Schema.SObjectType.Case.RecordTypeInfosByName.get(recordTypeName).RecordTypeId; 
            
        }
        return recordTypeID;       
    }

    /**
    *@description Upon receiving a case from Ring Central look for appropriate accounts and contacts to add to the case. As well 
    *             insure that the case is created with the correct record type.
    *@authoer David Barrera
    *@Date 9/18/14
    */
    
    public void getAccounts(List<Case> incomingCases){        

        Set<String> banAccountSet = new Set<String>();        
        Set<String>  caseAccountName = new Set<String>();

        List<Case> connectionCases = new List<Case>();
        List<Account> banAccounts = new List<Account>();
        List<Account> rcidAccounts = new List<Account>();

        Map<String, Case> banToCase = new Map<String,Case>();
        Map<String, Case> acctNameToCase = new Map<String,Case>();
        Map<String, Account> nameToRcidAcc = new Map<String, Account>();
        Map<String, Account> banToAcc = new Map<String,Account>();


        for(Case c:incomingCases){
            if(String.isNotBlank(c.ConnectionReceivedId ) || Test.isRunningTest()){
                //set the record type of the case to . 
                                             
                if(c.BAN_Nbr__c != null){                    
                    banAccountSet.add(c.BAN_Nbr__c);                    
                }
                else if(String.isNotBlank(c.Business_Name__c)){
                    caseAccountName.add(c.Business_Name__c);                    
                }                                
            }
        }
        //get the related ban account.
        //Update : -DAVID (dbarrera@tractionondemand.com) Changing the value we match to Can__c from Ban__c.
        if( banAccountSet.size() > 0){
            banAccounts = [SELECT Name,Id, ParentId,Can__c,Parent.RecordTypeId,Parent.RecordType.Name
                            FROM Account
                            WHERE Can__c in: banAccountSet
                            AND RecordTypeId = :banReID
                            ];
       }
       //search for all accounts that have the same name as the Account.Name on the Case, that doesn't
       // have a Ban.
       if(caseAccountName.size() > 0){
            rcidAccounts = [SELECT Name,Id, ParentId,RecordTypeId
                            FROM Account
                            WHERE Name in: caseAccountName
                            AND RecordTypeId = :rcid
                            ];
       }
       //Update : -DAVID (dbarrera@tractionondemand.com) Changing the value we match to Can__c from Ban__c..
       for(Account acc: banAccounts){
            banToAcc.put(acc.Can__c, acc);
       }
       for(Account acc: rcidAccounts){
            nameToRcidAcc.put(acc.Name, acc);
       }
       
       for(Case aCase:incomingCases){
            if(banToAcc.containsKey(aCase.BAN_Nbr__c)){
                Account tmpAcc = new Account();
                tmpAcc = banToAcc.get(aCase.BAN_Nbr__c);
                aCase.Related_BAN_Account__c = tmpAcc.Id;                            
                if(tmpAcc.Parent.RecordTypeId == rcid){                    
                    aCase.AccountId = tmpAcc.ParentId;                    
                }
            }
            else if(nameToRcidAcc.containsKey(aCase.Business_Name__c)){
                aCase.Account = nameToRcidAcc.get(aCase.Business_Name__c);

            }
       }
    }

    public void getContacts(List<Case> incomingCases){  

        String emailPhoneName = '';
        String email = '';
        String phoneName = ''; 

        Set<String> emails = new Set<String>();
        Set<String> phones = new Set<String>();
        Set<String> lastNames = new Set<String>();

        List<Contact> contacts = new List<Contact>();        

        Map<Case, List<Contact>> caseToContactList = new Map<Case, List<Contact>>();
        Map<String, Case> emailPhoneNameToCase = new Map<String,Case>();
        Map<String, Case> emailToCase = new Map<String,Case>();
        Map<String, Case> phoneNameToCase = new Map<String,Case>();

        //Build sets of fields to be able to query for in contacts.
        for(Case aCase: incomingCases){
            if(aCase.ConnectionReceivedId != null || Test.isRunningTest() ){
                if( String.isNotBlank(aCase.Contact_Email__c) && String.isNotBlank(aCase.Contact_phone_number2__c) && String.isNotBlank(aCase.Contact_Last_Name__c) ){
                    emails.add(aCase.Contact_Email__c);
                    phones.add(aCase.Contact_phone_number2__c);
                    lastNames.add(aCase.Contact_Last_Name__c);

                    emailToCase.put(aCase.Contact_Email__c,aCase);
                    emailPhoneNameToCase.put(joinFields(new List<String>{aCase.Contact_Email__c,aCase.Contact_phone_number2__c,aCase.Contact_Last_Name__c}), aCase);
                    phoneNameToCase.put(joinFields (new List<String>{aCase.Contact_phone_number2__c,aCase.Contact_Last_Name__c}), aCase);
                    

                }
                else if(String.isNotBlank(aCase.Contact_Email__c)){
                    emails.add(aCase.Contact_Email__c);
                    emailToCase.put(aCase.Contact_Email__c,aCase);
                }
                else if(String.isNotBlank(aCase.Contact_phone_number2__c) && String.isNotBlank(aCase.Contact_Last_Name__c)){
                    phones.add(aCase.Contact_phone_number2__c);
                    lastNames.add(aCase.Contact_Last_Name__c);
                    phoneNameToCase.put(joinFields(new List<String>{aCase.Contact_phone_number2__c,aCase.Contact_Last_Name__c}), aCase);

                }
            }
        }
        //System.Debug('emailToCase.get(email): ' + emailToCase);
        //System.Debug('emailPhoneNameToCase.get(email): ' + emailPhoneNameToCase);
        //System.Debug('phoneNameToCase.get(email): ' + phoneNameToCase);

        contacts = [
            SELECT Id, Email,LastName, Phone,Account.Name, AccountId
            FROM Contact
            WHERE Email IN :emails
            OR (LastName IN :lastNames
            AND Phone IN :phones)
        ];

         for(Contact cont:contacts){
            emailPhoneName = joinFields(new List<String>{cont.Email,cont.Phone,cont.LastName});
            email = cont.Email;
            phoneName = joinFields(new List<String>{cont.Phone,cont.LastName});
            Id tmpId ;
            if(emailPhoneNameToCase.containsKey(emailPhoneName)){
                //System.Debug('emailPhoneNameToCase.get(email): ' + emailPhoneNameToCase.get(email));
                //check if master map contains the case id of this map.
                setContacts(emailPhoneNameToCase.get(email),cont,caseToContactList);
            }
            else if(emailToCase.containsKey(email)){
                //System.Debug('emailToCase.get(email): ' + emailToCase.get(email));
                setContacts(emailToCase.get(email),cont,caseToContactList);
            }
            else if(phoneNameToCase.containsKey(phoneName)){
                //System.Debug('phoneNameToCase.get(phoneName): ' + phoneNameToCase.get(phoneName));
                setContacts(phoneNameToCase.get(phoneName),cont,caseToContactList);
            }
         }

         //System.Debug('caseToContactList.size(): ' + caseToContactList.size() );

         for(Case aCase:incomingCases){
            Contact tmpContact;
            //System.Debug('caseToContactList.containsKey(aCase): ' + caseToContactList.containsKey(aCase));
            if(caseToContactList.containsKey(aCase)){
                List<Contact> tmpContList = caseToContactList.get(aCase);
                //System.Debug('tmpContList.size(): ' + tmpContList.size());                
                if(tmpContList.size() == 1){
                    tmpContact = tmpContList.get(0);
                    //System.Debug('tmpContact: ' + tmpContact); 
                }
                else if(tmpContList.size() > 1){
                    //check to see which one of these contacts has the the same account as the Case.
                    tmpContact = contactWithCaseAccount(tmpContList,aCase.Account.Name);
                }                
            }
            //System.Debug('tmpContact != null: ' + tmpContact != null);
            if(tmpContact != null){
                aCase.ContactId = tmpContact.Id;
            }
            // Removed as the handiling of not finding a case will be to leave the information in the Contact_Last_Name__c,Contact_phone_number2__c
            // and Contact_Email__c Fields on the case.
            //else{ 
            //    //there are no contacts related to this Case and we shall place the Contact information from ring central 
            //    //into the description field.
            //    aCase.Description = aCase.Description + '\r\nRingCentral Contact Information:\r\nLast Name: ' + aCase.Contact_Last_Name__c + '\r\n'
            //                                          + 'Phone Number: ' + aCase.Contact_phone_number2__c + '\r\n'
            //                                          + 'E-mail: ' + aCase.Contact_Email__c + '\r\n';
            //}
         }
    }

    private String joinFields(List<String> fieldsToJoin){
        return String.join(fieldsToJoin, '-');
    }

    private void setContacts(Case aCase ,Contact cont ,Map<Case, List<Contact>> caseToContactList ){
        List<Contact> contactsInMap = new List<Contact>();
        if(caseToContactList.containsKey(aCase)){
            //setContacts with existing list of contacts. 
            contactsInMap = caseToContactList.get(aCase);
            contactsInMap.add(cont);            
        }
        else{
            contactsInMap.add(cont);            
        }
        caseToContactList.put(aCase, contactsInMap);
    }
    //Function that will find whether a contact has the same account as the case that we are currently looking at.
    //  If more than one contact has the same account then we return null to signify that there is no 
    private Contact contactWithCaseAccount(List<Contact> contacts,String caseAccName){
        List<Contact> returnContactList = new List<Contact>();

        for(Contact cont:contacts){
            if(cont.Account.Name == caseAccName){
                returnContactList.add(cont);
            }
        }
        if(returnContactList!= null){
            if(returnContactList.size() == 1){
                return returnContactList.get(0);
            }
        }
        return null;
    }
    
    public static void touchClosedCases(List<Case> cases){  
        Set<Id> closedCaseIds = new Set<Id>();      
         for(Case aCase: cases){
            if(String.isNotBlank(String.valueOf(aCase.Vendor_Case_Status__c))){
                //System.Debug('This is the status of the case:' + aCase.Vendor_Case_Status__c + ':' + String.valueOf(aCase.Vendor_Case_Status__c));
                if(String.valueOf(aCase.Vendor_Case_Status__c).contains('Closed')){
                    closedCaseIds.add(aCase.Id);
                }            
            }
        } 
        if(closedCaseIds.size() > 0 && !System.isFuture()){
            touchCasesFuture(closedCaseIds);        
        }
    }
    
    public static void touchCases(List<Case> cases){
        

        Set<Id> newCaseIds = new Set<Id>();
        try{
            PartnerNetworkConnection connection = trac_S2S_Connection_Helper.getRingCentralConnectionId('RingCentral');
            for(Case aCase: cases){
                if(aCase.ConnectionReceivedID != null && aCase.ConnectionReceivedId == connection.Id ){
                    newCaseIds.add(aCase.Id);
                }
            } 

            if(newCaseIds.size() > 0 && !System.isFuture() && !System.isBatch()){
                touchCasesFuture(newCaseIds);
            }                      
        }Catch(System.Exception ex){
            System.Debug('Caught Exception: ' + ex.getMessage() + ' on line ' + ex.getLineNumber() + '\n stack:' + ex.getStackTraceString());
        }
    }

    @future
    public static void touchCasesFuture(Set<Id> caseIds){
        List<Case> casesToTouch = [SELECT Id
                        FROM Case
                        WHERE Id in: caseIds
                        ];
        update casesToTouch;                        
    }
    
    /*
    * updateChildrensOwners    
    *@description method that will look at the owners of the chlidren of a case, and set them to be the same owner as the case.
    *@authoer David Barrera
    *@Date 11/9/14    
    
    
    public void updateChildrensOwners(List<Case> cases,Map<Id,Case> oldCasesMap,List<Device__c> devices,List<Vendor_Communication__c> vendorComms){
        Map<Id, Id> caseIdToOwnerId = new Map<Id,Id>();
        //Set<Id> caseIds = new Set<Ids>();
        for(Case aCase : cases){
            if( !String.valueOf(aCase.OwnerId).startsWith('00G')  && (aCase.RecordTypeId == telusServiceId || aCase.RecordTypeId == telusTechnicalId) ){
                Case oldCase = oldCasesMap.get(aCase.Id);
                //System.Debug('this is the case owner old then new : ' + oldCase.OwnerId + aCase.OwnerId);                
                System.Debug('this is the case owner Id: ' + aCase.OwnerId);
                caseIdToOwnerId.put(aCase.Id, aCase.OwnerId);              
            }
        }
        if(caseIdToOwnerId.size() > 0 ){
            if(devices.size() > 0){
                System.Debug('calling updateDeviceOwners');                
                updateDeviceOwners( caseIdToOwnerId,devices );
            }
            if(vendorComms.size() >0 ){
                System.Debug('calling updateVendorCommOwners');
                updateVendorCommOwners( caseIdToOwnerId,vendorComms );
            }
        }
    }
    
    
    *updateDeviceOwners
    *@description Given a map of Owner Id to Case Id find the appropriate Devices and assign the same owner as the case.
    *@authoer David Barrera
    *@Date 11/9/14
    
    
    public void updateDeviceOwners(Map<Id, Id> caseIdToOwnerId,List<Device__c> devices){
        
        
        for(Device__c dev : devices){
            if(caseIdToOwnerId.containsKey( dev.VoIP_Case__c )){
                Id tmpId = caseIdToOwnerId.get( dev.VoIP_Case__c );
                if( dev.OwnerId != tmpId ){
                    dev.OwnerId = tmpId;
                }
            }
        }
        update devices;
    }
        
    * updateVendorCommOwners
    *@description Given a map of the owner Id find the appropreate Vendor Communication and assign the same owner as the case.
    *@authoer David Barrera
    *@Date 9/18/14
    
    public void updateVendorCommOwners(Map<Id, Id> caseIdToOwnerId,List<Vendor_Communication__c> vendorComms){

        
        for(Vendor_Communication__c ven : vendorComms){
            if(caseIdToOwnerId.containsKey( ven.Case__c )){
                Id tmpId = caseIdToOwnerId.get( ven.Case__c );
                if( ven.OwnerId != tmpId ){
                    ven.OwnerId = tmpId;
                }
            }
        }
        Dispatcher.CountExecuted('updateCaseComment');
        update vendorComms;
        Dispatcher.ResetProcessCount('updateCaseComment');
    }
    */
}