@isTest
public class GetNextCarTest {
    
    
    @isTest
    public static void getNextCar() {  
        
        Test.startTest();    
        
        createTestuser();         
        User testUser = getTestuser();
        System.debug('... dyy ... testuser = ' + testUser.Name);
        
        
        System.runAs(testUser){  
            
            createSkillset();
            assignSkillset();
            createCars();
            
            Test.setCurrentPageReference(new PageReference('dyy test page'));         
            System.currentPageReference().getParameters().put('ids', null);           
            String retUrl = '/' + SObjectType.Credit_Assessment__c.keyPrefix;   
            System.currentPageReference().getParameters().put('retUrl', retUrl);
            
            GetNextCarController getNextCarController = new GetNextCarController();  
            getNextCarController.getNextCar();  
        }
        
        Test.StopTest();    
    }
    
    
    
    
    @isTest
    public static void sanity() {  
        
        Test.startTest();    
        
        createTestuser();         
        User testUser = getTestuser();
        System.debug('... dyy ... testuser = ' + testUser.Name);
        
        
        System.runAs(testUser){  
            
            createSkillset();
            assignSkillset();
            Credit_Assessment__c[] cars = createCars();
            
            Test.setCurrentPageReference(new PageReference('dyy test page'));         
            System.currentPageReference().getParameters().put('ids', null);           
            String retUrl = '/' + SObjectType.Credit_Assessment__c.keyPrefix;   
            System.currentPageReference().getParameters().put('retUrl', retUrl);
            
            GetNextCarController controller = new GetNextCarController();  
            
            controller.refresh();              
            controller.getQuerystringAgentOrManager();    
            controller.getIsManagerForReviewQueryString();
            controller.setSkillsetNotdefined();            
            
            controller.singleAssign(cars[0]);
            controller.bulkAssign(cars);            
            
            ApexPages.StandardController 
                sc = new ApexPages.StandardController(new Credit_Assessment__c());                
            GetNextCarController 
                controller2 = new GetNextCarController(sc);  
            
        }
        
        Test.StopTest();    
    }
    
    
    /*--------------------------------------------------*/
    
    private static Credit_Assessment__c[] createCars(){
        System.debug('... dyy ... createCars ...');
        Credit_Assessment__c[] cars = new List<Credit_Assessment__c> ();
        for(Integer i=0; i<2; i++){
            cars.add(CreditUnitTestHelper.getCar());
        }
        //insert cars;
        return cars;
    }
    
    private static void createTestuser(){
        User user = new User();
        user.Username = 'dyy.dyy@telus.com';
        user.LastName = 'X33X';  
        user.Email = 'dyy.dyy@telus.com';   
        user.CommunityNickname = 'dyy';   
        user.TimeZoneSidKey = 'Pacific/Kiritimati';   
        user.LocaleSidKey = UserInfo.getLocale();   
        user.EmailEncodingKey = 'ISO-8859-1';   
        user.ProfileId = UserInfo.getProfileId();   
        user.LanguageLocaleKey = UserInfo.getLanguage();  
        user.UserRoleId = getCreditUserRoleId();
        user.Alias = 'dyy';
        insert user;
    }
    
    private static void createSkillset(){
        Credit_Skill_Set__c skillset = new Credit_Skill_Set__c();
        skillset.Name = 'Level 1';
        skillset.Maximum__c = 0.0;
        skillset.Assessment_Type__c = 'Legal Name Validation';
        skillset.Business_Unit__c = 'Small';
        insert skillset;   
    }
    
    private static void assignSkillset(){
        Credit_Skill_Set__c skillset = [select id from Credit_Skill_Set__c where name ='Level 1'];        
        Credit_Skill_Set_Assignment__c assignment = new Credit_Skill_Set_Assignment__c();
        assignment.Credit_Agent__c = getTestuser().Id;
        assignment.Work_Queue_Definition__c = skillset.id;
        insert assignment;        
    }
    
    private static ID getCreditUserRoleId(){
        Userrole role = [
            select id,
            name
            from userrole
            where name = 'Credit User'
            limit 1
        ];
        return role.id;
    }
    
    private static User getTestuser(){
        User testuser = [select id,name,UserRoleId from User where Username = 'dyy.dyy@telus.com'];            
        return testuser;
    }
    
}