@isTest
private class telus_SMETProjectMemberRefreshTest {

    static testMethod void quickAddTest() {
    	
        SMET_Project__c p = new SMET_Project__c(name = 'test', Impact__c = 5, Complexity__c = 5);
        insert p;
        
        p = [SELECT new_update__c FROM SMET_Project__c where id = :p.id];
        p.new_update__c = 'test update';
        update p;
        
        Cloud_Enablement_Project_Team__c m = new Cloud_Enablement_Project_Team__c(Cloud_Enablement_Project__c = p.id, Team_Member__c = UserInfo.getUserId() );
        insert m;
        
        p = [SELECT My_Project__c, Project_Member_IDs__c FROM SMET_Project__c where id = :p.id];
        system.debug('Team_Member__c: '+m.Team_Member__c);
        system.debug('My_Project__c: '+p.My_Project__c);
        system.debug('Project_Member_IDs__c: '+p.Project_Member_IDs__c);        
        System.assertEquals(true,p.My_Project__c); // confirm field is true
        
    }
    
    static testMethod void quickDeleteTest() {
    	
        SMET_Project__c p = new SMET_Project__c(name = 'test', Impact__c = 5, Complexity__c = 5);
        insert p;
        
        p = [SELECT new_update__c FROM SMET_Project__c where id = :p.id];
        p.new_update__c = 'test update';
        update p;
        
        Cloud_Enablement_Project_Team__c m = new Cloud_Enablement_Project_Team__c(Cloud_Enablement_Project__c = p.id, Team_Member__c = UserInfo.getUserId() );
        insert m;
        
        p = [SELECT My_Project__c, Project_Member_IDs__c FROM SMET_Project__c where id = :p.id];
        system.debug('Team_Member__c: '+m.Team_Member__c);
        system.debug('My_Project__c: '+p.My_Project__c);
        system.debug('Project_Member_IDs__c: '+p.Project_Member_IDs__c);
        System.assertEquals(true,p.My_Project__c); // confirm field is true
        
        delete m;
        
        p = [SELECT My_Project__c FROM SMET_Project__c where id = :p.id];
        System.assertEquals(false,p.My_Project__c); // confirm field is false after delete
    }

}