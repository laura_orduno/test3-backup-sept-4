global with sharing class CustomGetProductListImplementation implements vlocity_cmt.VlocityOpenInterface{
  global Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {
    Boolean success = true;
    if(methodName == 'getProductList')
    { 
        getProductList(inputMap, outMap, options); 
    }
    
       return success; 
  }
    
  private void getProductList (Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options){ 
    
    List<Product2> prodList = [SELECT Id, Name From Product2 WHERE vlocity_cmt__TrackAsAgreement__c = true Order By Name LIMIT 50000];
      outMap.put('productList', prodList);
  }
}