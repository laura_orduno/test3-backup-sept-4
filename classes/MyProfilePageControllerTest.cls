@isTest(SeeAllData=true)
private class MyProfilePageControllerTest {
		static testMethod void testSave() {         
        // Modify the test to query for a portal user that exists in your org
        User existingPortalUser = [
        	SELECT id, profileId, userRoleId
        	FROM User
        	WHERE UserRoleId <> null
        	AND IsActive = true
        	AND UserType = 'PowerPartner' LIMIT 1];
        System.assert(existingPortalUser != null, 'This test depends on an existing test portal user to run');
        
        String randFax = Math.rint(Math.random() * 1000) + '5551234';
        
        System.runAs(existingPortalUser) {
            MyProfilePageController controller = new MyProfilePageController();
            System.assertEquals(existingPortalUser.Id, controller.getUser().Id, 'Did not successfully load the current user');
            System.assert(controller.getIsEdit() == false, 'isEdit should default to false');
            controller.edit();
            System.assert(controller.getIsEdit() == true);
            
            controller.cancel();
            System.assert(controller.getIsEdit() == false);
            
            controller.getUser().Fax = randFax;
            controller.save();
            System.assert(controller.getIsEdit() == false);
        }
        
        // verify that the user and contact were updated
        existingPortalUser = [Select id, fax, Contact.Fax from User where id =: existingPortalUser.Id];
        //System.assert(existingPortalUser.fax == randFax);
        //System.assert(existingPortalUser.Contact.fax == randFax);
    }
}