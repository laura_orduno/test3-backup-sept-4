public class smbLanguageUtils 
{  
   /* 
    * This method returns French Date String value  with its format as 'MM dd, YYYY'. 
    * The input parameter is English Date String value 
    */  
   public static String translateEntoFr(String enDateStr)
   {
       String frDateStr = enDateStr;
       if(!String.isEmpty(enDateStr) && enDateStr.length() >= 3)
        {
       	 	String[] ary = enDateStr.split(' ');
            String dateSubString = enDateStr.substring(ary[0].length());           
            frDateStr = buildMonthShortNameMap().get(ary[0]) + ' ' + dateSubString.trim();
        }
       
       return frDateStr;
   }
 
    /*
     * Use EN short name as the key with FR short name as the value. 
     *EN short name is defined in smb_DateUtility.apex file.
     */
    private static Map<String, String> buildMonthShortNameMap()
    {
        Map <String, String> monthShortMap = new Map<String, String>(); 
       
        monthShortMap.put('Jan', 'Janv');
        monthShortMap.put('Feb', 'Févr');
        monthShortMap.put('Mar', 'Mars');
        monthShortMap.put('Apr', 'Avril');        
        monthShortMap.put('May', 'Mai');       
        monthShortMap.put('Jun', 'Juin');       
        monthShortMap.put('Jul', 'Juil'); 
        monthShortMap.put('Aug', 'Août');
        monthShortMap.put('Sep', 'Sept'); 
        monthShortMap.put('Oct', 'Oct');
        monthShortMap.put('Nov', 'Nov'); 
        monthShortMap.put('Dec', 'Déc');
        
        return monthShortMap;  
    }
}