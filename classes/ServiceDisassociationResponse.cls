public class ServiceDisassociationResponse {
    public Status status{get;set;}
    public class Status{
        public string statusCd{get;set;}
        public string statusSubCd{get;set;}
        public string statusTxt{get;set;}
        public string systemErrorTimestamp{get;set;}
        public string systemErrorCd{get;set;}
        public string systemErrorTxt{get;set;}
    }
    public static ServiceDisassociationResponse parse(httpresponse response){
        jsonparser parser=json.createparser(response.getbody());
        ServiceDisassociationResponse saResponse=(ServiceDisassociationResponse)parser.readvalueas(ServiceDisassociationResponse.class);
        return saResponse;
    }
}