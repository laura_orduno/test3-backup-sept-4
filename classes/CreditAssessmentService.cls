//Generated by FuseIT WSDL2Apex (http://www.fuseit.com/Solutions/SFDC-Explorer/Help-WSDL-Parser.aspx)
//Methods Included: performBusinessCreditAssessment, performAutoUpdateBusinessCreditProfile, getBusinessCreditAssessmentList, attachCreditReport, removeCreditReport, getCreditReportList, getCreditReport, ping
// Primary Port Class Name: BusinessCreditAssessmentService_v1_0_SOAP	
public class CreditAssessmentService {
    public class BusinessCreditAssessmentService_v1_0_SOAP {
        
        
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Customer/Customer/BusinessCreditCommonTypes_v1','CreditAssessmentBusinessTypes','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/ping_v1','CreditAssessmentPing','http://xmlschema.tmi.telus.com/xsd/common/exceptions/Exceptions_v1_0','CreditAssessmentExceptions','http://telus.com/wsdl/CMO/OrderMgmt/BusinessCreditAssessmentService_1','CreditAssessmentService','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v9','CreditAssessmentEnterpriseTypes','http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditAssessmentServiceRequestResponse_v1','CreditAssessmentRequestResponse'};
            
            
            
            public String endpoint_x; 
        
        public BusinessCreditAssessmentService_v1_0_SOAP(){
            if ( !Test.isRunningTest() ){
                endpoint_x = SMBCare_WebServices__c.getInstance('CreditAssessmentEndpoint').value__c; 
            }
        }
        
        
        public String attachCreditReportWithCarId(
            String externalCarId
            , Long businessCustomerId
            ,CreditAssessmentBusinessTypes.BusinessCreditReportRequest businessCreditReportRequest
            ,CreditAssessmentBusinessTypes.CreditAuditInfo auditInfo) 
        {
            CreditAssessmentRequestResponse.attachCreditReport_element request_x = new CreditAssessmentRequestResponse.attachCreditReport_element();
            CreditAssessmentRequestResponse.attachCreditReportResponse_element response_x;
            request_x.externalCarId = externalCarId;
            request_x.businessCustomerId = businessCustomerId;
            request_x.businessCreditReportRequest = businessCreditReportRequest;
            request_x.auditInfo = auditInfo;
            Map<String, CreditAssessmentRequestResponse.attachCreditReportResponse_element> response_map_x = new Map<String, CreditAssessmentRequestResponse.attachCreditReportResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
                this,
                request_x,
                response_map_x,
                new String[]{endpoint_x,
                    'attachCreditReport',
                    'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditAssessmentServiceRequestResponse_v1',
                    'attachCreditReport',
                    'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditAssessmentServiceRequestResponse_v1',
                    'attachCreditReportResponse',
                    'CreditAssessmentRequestResponse.attachCreditReportResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.creditReportId;
        }
        
        
        public String attachCreditReport(
            Long businessCustomerId
            ,CreditAssessmentBusinessTypes.BusinessCreditReportRequest businessCreditReportRequest
            ,CreditAssessmentBusinessTypes.CreditAuditInfo auditInfo) 
        {
            CreditAssessmentRequestResponse.attachCreditReport_element request_x = new CreditAssessmentRequestResponse.attachCreditReport_element();
            CreditAssessmentRequestResponse.attachCreditReportResponse_element response_x;
            request_x.businessCustomerId = businessCustomerId;
            request_x.businessCreditReportRequest = businessCreditReportRequest;
            request_x.auditInfo = auditInfo;
            Map<String, CreditAssessmentRequestResponse.attachCreditReportResponse_element> response_map_x = new Map<String, CreditAssessmentRequestResponse.attachCreditReportResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
                this,
                request_x,
                response_map_x,
                new String[]{endpoint_x,
                    'attachCreditReport',
                    'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditAssessmentServiceRequestResponse_v1',
                    'attachCreditReport',
                    'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditAssessmentServiceRequestResponse_v1',
                    'attachCreditReportResponse',
                    'CreditAssessmentRequestResponse.attachCreditReportResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.creditReportId;
        }
        
        public CreditAssessmentRequestResponse.BusinessCreditAssessmentResult[] 
            getBusinessCreditAssessmentList(Long[] creditAssessmentIdList,CreditAssessmentBusinessTypes.CreditAuditInfo auditInfo) {
                CreditAssessmentRequestResponse.getBusinessCreditAssessmentList_element request_x = new CreditAssessmentRequestResponse.getBusinessCreditAssessmentList_element();
                CreditAssessmentRequestResponse.getBusinessCreditAssessmentListResponse_element response_x;
                request_x.creditAssessmentIdList = creditAssessmentIdList;
                request_x.auditInfo = auditInfo;
                Map<String, CreditAssessmentRequestResponse.getBusinessCreditAssessmentListResponse_element> response_map_x = new Map<String, CreditAssessmentRequestResponse.getBusinessCreditAssessmentListResponse_element>();
                response_map_x.put('response_x', response_x);
                WebServiceCallout.invoke(
                    this,
                    request_x,
                    response_map_x,
                    new String[]{endpoint_x,
                        'getBusinessCreditAssessmentList',
                        'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditAssessmentServiceRequestResponse_v1',
                        'getBusinessCreditAssessmentList',
                        'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditAssessmentServiceRequestResponse_v1',
                        'getBusinessCreditAssessmentListResponse',
                        'CreditAssessmentRequestResponse.getBusinessCreditAssessmentListResponse_element'}
                );
                response_x = response_map_x.get('response_x');
                return response_x.businessCreditAssessmentTransactionList;
            }
        
        public CreditAssessmentBusinessTypes.BusinessCreditReportFullDetails getCreditReport(
            Long creditReportId
            ,CreditAssessmentBusinessTypes.CreditAuditInfo auditInfo) {
                CreditAssessmentRequestResponse.getCreditReport_element request_x = new CreditAssessmentRequestResponse.getCreditReport_element();
                CreditAssessmentRequestResponse.getCreditReportResponse_element response_x;
                request_x.creditReportId = creditReportId;
                request_x.auditInfo = auditInfo;
                Map<String, CreditAssessmentRequestResponse.getCreditReportResponse_element> response_map_x = new Map<String, CreditAssessmentRequestResponse.getCreditReportResponse_element>();
                response_map_x.put('response_x', response_x);
                WebServiceCallout.invoke(
                    this,
                    request_x,
                    response_map_x,
                    new String[]{endpoint_x,
                        'getCreditReport',
                        'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditAssessmentServiceRequestResponse_v1',
                        'getCreditReport',
                        'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditAssessmentServiceRequestResponse_v1',
                        'getCreditReportResponse',
                        'CreditAssessmentRequestResponse.getCreditReportResponse_element'}
                );
                response_x = response_map_x.get('response_x');
                
                System.debug('... dyy ... response_x = ' + response_x);
                System.debug('... dyy ... response_x.businessCreditReportFullDetails = ' 
                             + response_x.businessCreditReportFullDetails);
                
                return response_x.businessCreditReportFullDetails;
            }
        
        public CreditAssessmentBusinessTypes.BusinessCreditReportMetaData[] 
            getCreditReportList(Long[] creditReportIdList,CreditAssessmentBusinessTypes.CreditAuditInfo auditInfo) {
                CreditAssessmentRequestResponse.getCreditReportList_element request_x = new CreditAssessmentRequestResponse.getCreditReportList_element();
                CreditAssessmentRequestResponse.getCreditReportListResponse_element response_x;
                request_x.creditReportIdList = creditReportIdList;
                request_x.auditInfo = auditInfo;
                Map<String, CreditAssessmentRequestResponse.getCreditReportListResponse_element> response_map_x = new Map<String, CreditAssessmentRequestResponse.getCreditReportListResponse_element>();
                response_map_x.put('response_x', response_x);
                WebServiceCallout.invoke(
                    this,
                    request_x,
                    response_map_x,
                    new String[]{endpoint_x,
                        'getCreditReportList',
                        'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditAssessmentServiceRequestResponse_v1',
                        'getCreditReportList',
                        'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditAssessmentServiceRequestResponse_v1',
                        'getCreditReportListResponse',
                        'CreditAssessmentRequestResponse.getCreditReportListResponse_element'}
                );
                response_x = response_map_x.get('response_x');
                if ( Test.isRunningTest() ) return null;
                return response_x.businessCreditReportMetaDataList;
            }
        
        public CreditAssessmentRequestResponse.AutoUpdateBusinessCreditProfileResult performAutoUpdateBusinessCreditProfile(
            String externalCARId
            ,CreditAssessmentRequestResponse.performAutoUpdateBusinessCreditProfile_carBusinessProfile_element carBusinessProfile
            ,CreditAssessmentBusinessTypes.CreditAuditInfo auditInfo) {
                CreditAssessmentRequestResponse.performAutoUpdateBusinessCreditProfile_element request_x = new CreditAssessmentRequestResponse.performAutoUpdateBusinessCreditProfile_element();
                CreditAssessmentRequestResponse.performAutoUpdateBusinessCreditProfileResponse_element response_x;
                request_x.externalCARId = externalCARId;
                request_x.carBusinessProfile = carBusinessProfile;
                request_x.auditInfo = auditInfo;
                Map<String, CreditAssessmentRequestResponse.performAutoUpdateBusinessCreditProfileResponse_element> response_map_x = new Map<String, CreditAssessmentRequestResponse.performAutoUpdateBusinessCreditProfileResponse_element>();
                response_map_x.put('response_x', response_x);
                WebServiceCallout.invoke(
                    this,
                    request_x,
                    response_map_x,
                    new String[]{endpoint_x,
                        'performAutoUpdateBusinessCreditProfile',
                        'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditAssessmentServiceRequestResponse_v1',
                        'performAutoUpdateBusinessCreditProfile',
                        'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditAssessmentServiceRequestResponse_v1',
                        'performAutoUpdateBusinessCreditProfileResponse',
                        'CreditAssessmentRequestResponse.performAutoUpdateBusinessCreditProfileResponse_element'}
                );
                response_x = response_map_x.get('response_x');
                CreditAssessmentRequestResponse.AutoUpdateBusinessCreditProfileResult result;
                
                if ( !Test.isRunningTest() ){
                    result = response_x.autoUpdateBusinessCreditProfileResult;
                }
                return result;
            }
        
        public CreditAssessmentRequestResponse.BusinessCreditAssessmentResult performBusinessCreditAssessment(
            String externalCARId
            ,CreditAssessmentBusinessTypes.BusinessProfile BusinessProfile
            ,CreditAssessmentRequestResponse.performBusinessCreditAssessment_orderData_element orderData
            ,CreditAssessmentBusinessTypes.CreditAuditInfo auditInfo) {
                CreditAssessmentRequestResponse.performBusinessCreditAssessment_element request_x = new CreditAssessmentRequestResponse.performBusinessCreditAssessment_element();
                CreditAssessmentRequestResponse.performBusinessCreditAssessmentResponse_element response_x;
                request_x.externalCARId = externalCARId;
                request_x.BusinessProfile = BusinessProfile;
                request_x.orderData = orderData;
                request_x.auditInfo = auditInfo;
                Map<String, CreditAssessmentRequestResponse.performBusinessCreditAssessmentResponse_element> response_map_x = new Map<String, CreditAssessmentRequestResponse.performBusinessCreditAssessmentResponse_element>();
                response_map_x.put('response_x', response_x);
                WebServiceCallout.invoke(
                    this,
                    request_x,
                    response_map_x,
                    new String[]{endpoint_x,
                        'performBusinessCreditAssessment',
                        'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditAssessmentServiceRequestResponse_v1',
                        'performBusinessCreditAssessment',
                        'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditAssessmentServiceRequestResponse_v1',
                        'performBusinessCreditAssessmentResponse',
                        'CreditAssessmentRequestResponse.performBusinessCreditAssessmentResponse_element'}
                );
                response_x = response_map_x.get('response_x');
                return response_x.businessCreditAssessmentResult;
            }
        
        public String ping() {
            CreditAssessmentPing.ping_element request_x = new CreditAssessmentPing.ping_element();
            CreditAssessmentPing.pingResponse_element response_x;
            Map<String, CreditAssessmentPing.pingResponse_element> response_map_x = new Map<String, CreditAssessmentPing.pingResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
                this,
                request_x,
                response_map_x,
                new String[]{endpoint_x,
                    'ping',
                    'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/ping_v1',
                    'ping',
                    'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/ping_v1',
                    'pingResponse',
                    'CreditAssessmentPing.pingResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.version;
        }
        
        public void removeCreditReport(
            String creditReportId
            ,CreditAssessmentBusinessTypes.CreditAuditInfo auditInfo
        ) {
            CreditAssessmentRequestResponse.removeCreditReport_element request_x = new CreditAssessmentRequestResponse.removeCreditReport_element();
            CreditAssessmentRequestResponse.removeCreditReportResponse_element response_x;
            request_x.creditReportId = creditReportId;
            request_x.auditInfo = auditInfo;
            Map<String, CreditAssessmentRequestResponse.removeCreditReportResponse_element> response_map_x = new Map<String, CreditAssessmentRequestResponse.removeCreditReportResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
                this,
                request_x,
                response_map_x,
                new String[]{endpoint_x,
                    'removeCreditReport',
                    'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditAssessmentServiceRequestResponse_v1',
                    'removeCreditReport',
                    'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditAssessmentServiceRequestResponse_v1',
                    'removeCreditReportResponse',
                    'CreditAssessmentRequestResponse.removeCreditReportResponse_element'}
            );
            response_x = response_map_x.get('response_x');
        }
    }
}