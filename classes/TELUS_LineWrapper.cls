public with sharing class TELUS_LineWrapper implements Comparable{
    public SObject sObjectItem;
    public static String sortBy;
    public TELUS_LineWrapper(SObject item){
        sObjectItem = item;
    }
    
    public Integer compareTo(Object compareTo){
        TELUS_LineWrapper compareToLine = (TELUS_LineWrapper)compareTo;
        Integer returnValue = 0;
        if(sortBy ==  null || sortBy == 'Descending'){
            if(String.ValueOf(sObjectItem.get('vlocity_cmt__LineNumber__c')) > String.ValueOf(compareToLine.sObjectItem.get('vlocity_cmt__LineNumber__c'))){
                returnValue = -1;
            
            } else if (String.ValueOf(sObjectItem.get('vlocity_cmt__LineNumber__c')) < String.ValueOf(compareToLine.sObjectItem.get('vlocity_cmt__LineNumber__c'))){
                returnValue = 1;
                
            }
        
        } else if(sortBy == 'Ascending'){
            if(String.ValueOf(sObjectItem.get('vlocity_cmt__LineNumber__c')) > String.ValueOf(compareToLine.sObjectItem.get('vlocity_cmt__LineNumber__c'))){
                returnValue = 1;
            
            } else if (String.ValueOf(sObjectItem.get('vlocity_cmt__LineNumber__c')) < String.ValueOf(compareToLine.sObjectItem.get('vlocity_cmt__LineNumber__c'))){
                returnValue = -1;
                
            }
        
        
        }
        
         return returnValue;
    }
    
}