/**
* Description of the purpose of the class:Bean/Model to capture address details returned by Canada post or LPDS.
* Author:
* Date:
* Project/Release:Predictive Adddress Capture
* Change Control Information:
* Date Who Reason for the change
**/
public without sharing class AddressData {
    
    
    public boolean addAsException{get;set;}
    public String PAId{get;set;}
    
    public Boolean isManualCapture { get; set; }
    public string captureNewAddrStatus{get;set;} // PAC R2 - Status update to 'Valid' when checked 'Capture New Address' otherwise 'Invalid'
     
    public string searchString{get;set;}
    public boolean isSearchStringRequired{get;set;}
    
    public String addressLine1{get;set;}
    public boolean isAddressLine1Required{get;set;}
    
    public String addressLine2{get;set;}
    public boolean isAddressLine2Required{get;set;}
    
    public String addressLine3{get;set;}
    public boolean isAddressLine3Required{get;set;}
    
    public String city{get;set;}
    public boolean isCityRequired{get;set;}
    
    public String state{get;set;}
    public boolean isStateRequired{get;set;}
    
    // do not use zip code
    public String zipCode{get;set;}
    public boolean isZipRequired{get;set;}
    
    public String PostalCode{get;set;}
    public boolean isPostalCodeRequired{get;set;}
    
    public String province{get;set;}
    public boolean isProvinceRequired{get;set;}
    
    public String canadaProvince { get; set; }
    public String usState { get; set; }
    
    public String country{get;set;}
    public boolean isCountryRequired{get;set;}
    
    // added by Prashant on 22-May-2017 for billing System in billing address page
    public String billingSystem{get;set;}
    // end of Prashant's code
    
    public String billingLanguage{get;set;}//added  by Barkha on 13/06/2017 for Billing Language support
    public String billingFormat{get;set;}//Added  by Barkha on 13/06/2017 for Billing Format support
   
    public String organization{get;set;}
    public boolean isOrganizationRequired{get;set;}
    
    public String domesticId {get;set;}
    public Boolean isDomesticIdRequired {get;set;}
    
    public String language {get;set;}
    public Boolean isLanguageRequired {get;set;}
    
    public String languageAlternatives {get;set;}
    public Boolean isLanguageAlternativesRequired {get;set;}
    
    public String department {get;set;}
    public Boolean isDepartmentRequired {get;set;}
    
    public String company {get;set;}
    public Boolean isCompanyRequired {get;set;}
    
    public String subBuilding {get;set;}
    public Boolean isSubBuildingRequired {get;set;}
    
    public String buildingNumber {get;set;}
    public Boolean isBuildingNumberRequired {get;set;}
    
    public String buildingName {get;set;}
    public Boolean isBuildingNameRequired {get;set;}
    
    public String secondaryStreet {get;set;}
    public Boolean isSecondaryStreetRequired {get;set;}
    
    public String street {get;set;}
    public Boolean isStreetRequired {get;set;}
    
    public String block {get;set;}
    public Boolean isBlockRequired {get;set;}
    
    public String neighbourhood {get;set;}
    public Boolean isNeighbourhoodRequired {get;set;}
    
    public String district {get;set;}
    public Boolean isDistrictRequired {get;set;}
    
    public String addressLine4 {get;set;}
    public Boolean isAddressLine4Required {get;set;}
    
    public String addressLine5 {get;set;}
    public Boolean isAddressLine5Required {get;set;}
    
    public String adminAreaName {get;set;}
    public Boolean isAdminAreaNameRequired {get;set;}
    
    public String adminAreaCode {get;set;}
    public Boolean isAdminAreaCodeRequired {get;set;}
    
    public String provinceName {get;set;}
    public Boolean isProvinceNameRequired {get;set;}
    
    public String provinceCode {get;set;}
    public Boolean isProvinceCodeRequired {get;set;}
    
    public String countryIso2 {get;set;}
    public Boolean isCountryIso2Required {get;set;}
    
    public String countryIso3 {get;set;}
    public Boolean isCountryIso3Required {get;set;}
    
    public String countryIsoNumber {get;set;}
    public Boolean isCountryIsoNumberRequired {get;set;}
    
    public String sortingNumber1 {get;set;}
    public Boolean isSortingNumber1Required {get;set;}
    
    public String sortingNumber2 {get;set;}
    public Boolean isSortingNumber2Required {get;set;}
    
    public String barcode {get;set;}
    public Boolean isBarcodeRequired {get;set;}
    
    public String pOBoxNumber {get;set;}
    public Boolean isPOBoxNumberRequired {get;set;}
    
    public String PAlabel {get;set;}
    public Boolean isPALabelRequired {get;set;}
    
    public String PAType {get;set;}
    public Boolean isPATypeRequired {get;set;}
    
    public String PADataLevel {get;set;}
    public Boolean isPADataLevelRequired {get;set;}
    
    public String onLoadSearchAddress{get;set;}
    
    //Start: Added by Aditya Jamwal(IBM) for Billing  A/c Creation
    public String StreetType {get;set;}
    public Boolean isStreetTypeRequired {get;set;}
    
    public String StreetName {get;set;}
    public Boolean isStreetNameRequired {get;set;}
    
    public String StreetNumber {get;set;}
    public Boolean isStreetNumberRequired {get;set;}
    
    public String StreetAddress {get;set;}
    public Boolean isStreetAddressRequired {get;set;}
    
    public String domainCode {get;set;}
    public Boolean isdomainRequired {get;set;}
    
    public String subDomainCode {get;set;}
    public Boolean issubDomainRequired {get;set;}
    
    public String responsibleGroupCode {get;set;}
    public Boolean isresponsibleGroupRequired {get;set;}
    //End: Added by Aditya Jamwal(IBM) for Billing  A/c Creation
}