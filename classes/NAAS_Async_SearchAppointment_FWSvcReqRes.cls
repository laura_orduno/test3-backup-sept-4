public class NAAS_Async_SearchAppointment_FWSvcReqRes {
    
    public class SearchAvailableAppointmentListResponseFuture extends System.WebServiceCalloutFuture {
        public string OLIId;
        public string SWT;
        public string RIT;
        public void setOLIDIdSWTRIT(string oliId,string SWT,string RIT){        
            this.OLIId=oliId;
            this.SWT=SWT;
            this.RIT=RIT;
        }
        public SMB_Appointment_FWAppointmentSvcReqRes.SearchAvailableAppointmentListResponse getValue() {
            SMB_Appointment_FWAppointmentSvcReqRes.SearchAvailableAppointmentListResponse response = (SMB_Appointment_FWAppointmentSvcReqRes.SearchAvailableAppointmentListResponse)System.WebServiceCallout.endInvoke(this);
             system.debug(' inAsyn 1'+response);
            return response;
        }
    }
}