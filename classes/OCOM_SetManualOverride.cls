global with sharing class OCOM_SetManualOverride implements vlocity_cmt.VlocityOpenInterface {
     global Boolean invokeMethod(String methodName, Map<String, Object> input, Map<String, Object> output, Map<String, Object> options)  {
        if (methodName.equals('priceItems')){
            return priceItems(input,output,options);
        }
        return true;
    }
    
    private Boolean priceItems(Map<String, Object> inputMap, Map<String, Object> outputMap, Map<String, Object> optionsMap){
        Decimal nrcvalue = 0.00;
        Decimal mrcvalue = 0.00;
        set<string> rootProductItemIds = new set<string>();
        map<id,sobject> rootItTosObjectMap = new map<id,sObject>();
        map<string,Object> rootObjectMap = new map<string,Object>();
        List<Id> childOliToupdate = new list<Id>();
        map<id,id>oiToProdIDMap = new map<id,id>();
        map<id,id>prodToOiMap = new map<id,id>();
        Map<id,OrderItem> parentToWaiveOrderMap = new Map<Id, OrderItem>();
        Map<String, Object> filterEvaluatorOutput = (Map<String, Object>)inputMap.get('filterEvaluatorOutput');
        system.debug('filterEvaluatorOutput-PriceItems_____' +filterEvaluatorOutput);
        Map<Id, Sobject> sobjectIdToSobject = (Map<Id, Sobject>)filterEvaluatorOutput.get('sobjectIdToSobject');

        system.debug('sobjectIdToSobject-PriceItems______' +  sobjectIdToSobject);
        List<Id> qualifiedObjectIds = new List<Id>((Set <Id>)inputMap.get('qualifiedObjectIds'));
        system.debug('qualifiedObjectIds______' +qualifiedObjectIds);
        list<OrderItem> orderItemToUpdate = new list<OrderItem>();
        set<id> orderIDSet = new set<id>();
        
     for(id objID: qualifiedObjectIds){
        if(sobjectIdToSobject.ContainsKey(objID)){
             orderItem oi = (OrderItem)sobjectIdToSobject.get(objID);
           if(oi.PricebookEntry.product2.Name != null && oi.PricebookEntry.product2.Name.startsWithIgnoreCase('Waive'))
             {
                    parentToWaiveOrderMap.put(oi.vlocity_cmt__ParentItemId__c, oi);
                    system.debug('>> Waive Product Added ______' + parentToWaiveOrderMap);
                   // oi.vlocity_cmt__OneTimeTotal__c = (-1 * onetimetotalMap.get(oi.vlocity_cmt__ParentItemId__c));
                   // orderIDSet.add(oi.id);
             }
           
          if(oi.vlocity_cmt__JSONAttribute__c != null){
             Map<String, Object> deserialized = (Map<String, Object>)JSON.deserializeUntyped(oi.vlocity_cmt__JSONAttribute__c);
                List<Object> attributesList = new List<Object>();
                List<Object> allAttributes = new List<Object>();
                List<Object> attrList = new List<Object>();
                Map<String, Object> attrMap = new Map<String, Object>();
                List<Map<String,Object>> attrMapList = new List<Map<String,Object>>();
                for(String cat : deserialized.keySet()){
                                //  System.debug(cat);
                  attrList = (List<Object>)deserialized.get(cat);
                    //System.debug(attrList);
                                }
                for(Object attrib :attrList) {
                   // System.debug(attrib);
                     attrMap = (Map<String, Object>)attrib;
                    attrMapList.add(attrMap);
                }
                SYstem.debug('###'+attrMapList);
                 for(Map<String,Object> test:attrMapList) {
                   
                        if(test.get('attributedisplayname__c')=='One-time Charges, NRC')
                        {
                            Map<String,Object> getValueNRC = (Map<String, Object>)test.get('attributeRunTimeInfo');
                            System.debug('NRCValue-->'+getValueNRC.get('value') );
                            //System.debug('OI NAME-->'+ oi.PriceBookEntry.product2.Id);
                            nrcvalue = getValueNRC.get('value')!= null? Decimal.valueof(String.Valueof(getValueNRC.get('value'))) : null;
                            if(nrcvalue != null)
                            {
                                oi.put('vlocity_cmt__OneTimeTotal__c', nrcvalue);
                                oi.put('vlocity_cmt__EffectiveOneTimeTotal__c',nrcvalue);
                                orderIDSet.add(oi.id);
                                
                            }
                        }
                        
                        if(test.get('attributedisplayname__c')=='Recurring Charges, MRC')
                        {
                            Map<String,Object> getValueMRC = (Map<String, Object>)test.get('attributeRunTimeInfo');
                            System.debug('MRCValue-->'+getValueMRC.get('value'));
                            mrcvalue = getValueMRC.get('value')!= null?Decimal.valueof(String.ValueOf(getValueMRC.get('value'))):null;
                            if(mrcvalue != null){
                                oi.put('vlocity_cmt__RecurringTotal__c',mrcvalue);
                                oi.put('vlocity_cmt__EffectiveRecurringTotal__c',mrcvalue);
                                orderIDSet.add(oi.id);
                                
                            }
                        }
                        
                }
                if(orderIDSet.Contains(oi.id) )
                         orderItemToUpdate.add(oi); 
            }
          }
        }

        for (OrderItem parentProd: [Select Id,vlocity_cmt__OneTimeTotal__c,vlocity_cmt__Product2Id__c,vlocity_cmt__OneTimeCharge__c,vlocity_cmt__EffectiveOneTimeTotal__c from OrderItem where id IN:parentToWaiveOrderMap.keySet() ]){
            if(parentProd.vlocity_cmt__OneTimeTotal__c != null){
              if(parentToWaiveOrderMap.ContainsKey(parentProd.id)){
                     orderItem wiaveOrderToUpdate =  parentToWaiveOrderMap.get(parentProd.id);
                //orderItem wiaveOrderToUpdate = parentToWaiveOrderMap.get(parentProd.vlocity_cmt__Product2Id__c);
               wiaveOrderToUpdate.vlocity_cmt__OneTimeTotal__c = (-1 * parentProd.vlocity_cmt__OneTimeTotal__c);
               wiaveOrderToUpdate.vlocity_cmt__OneTimeCharge__c = (-1 * parentProd.vlocity_cmt__OneTimeCharge__c);
               wiaveOrderToUpdate.vlocity_cmt__EffectiveOneTimeTotal__c = (-1 * parentProd.vlocity_cmt__EffectiveOneTimeTotal__c);
               orderItemToUpdate.add(wiaveOrderToUpdate);
               system.debug('Waive prod One time value_______' + wiaveOrderToUpdate.vlocity_cmt__OneTimeTotal__c);
                   // orderIDSet.add(oi.id);
            }
        }
      }

        if(orderItemToUpdate.Size()>0 && !orderItemToUpdate.isEmpty() ){
            system.debug('Updating OrderItem');
            update orderItemToUpdate;
        }
        return true;         
    }
}