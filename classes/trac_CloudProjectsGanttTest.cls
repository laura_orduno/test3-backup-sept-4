/*
Title: Test Class for trac_CloudProjectsGanttViewController
Description:
Author: Hans Vedo, hvedo@tractionondemand.com
2013-03-13: Created
*/

// seealldata provides access to the custom setting used.
@istest(seealldata=true)
private class trac_CloudProjectsGanttTest{

    static testmethod void getActiveProjectsTest(){
        // Instantiate the class
        /*
        PageReference pageRef = Page.trac_CloudProjectsGanttView;
        Test.setCurrentPage(pageRef);
        SMET_Project__c testObject1 = new SMET_Project__c();
        SMET_Project__c testProject = [SELECT Name FROM SMET_Project__c LIMIT 1];
        trac_CloudProjectsGanttViewController testObject = new trac_CloudProjectsGanttViewController(testProject);       
        
        // Set a test parameter.
        String key = 'filter';
        String value = 'test';
        ApexPages.currentPage().getParameters().put(key, value);
        */

        // Test methods
        Test.startTest();
    	apexpages.Standardsetcontroller controller=new apexpages.Standardsetcontroller(new list<smet_project__c>{new smet_project__c()});
    	trac_CloudProjectsGanttViewController testObject=new trac_CloudProjectsGanttViewController(controller);
        testObject.getRecordTypes();
        testObject.getGanttRecordsDisplay();
        testObject.getStatusEntries();
        testObject.getYears();
        Test.stopTest();
    }
}