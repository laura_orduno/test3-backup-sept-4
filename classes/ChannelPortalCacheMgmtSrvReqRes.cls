//Generated by wsdl2apex

public class ChannelPortalCacheMgmtSrvReqRes {
    public class SaveCacheListType {
        public ChannelPortalCacheMgmtSrvReqRes.MapItem[] customCacheList;
        private String[] customCacheList_type_info = new String[]{'customCacheList','http://xmlschema.tmi.telus.com/srv/SPO/PartnerMgmt/ChannelPortalCacheMgmtSvcRequestResponse_v1',null,'1','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/srv/SPO/PartnerMgmt/ChannelPortalCacheMgmtSvcRequestResponse_v1','true','false'};
        private String[] field_order_type_info = new String[]{'customCacheList'};
    }
    public class GetCacheListType {
        public String referenceKey;
        private String[] referenceKey_type_info = new String[]{'referenceKey','http://xmlschema.tmi.telus.com/srv/SPO/PartnerMgmt/ChannelPortalCacheMgmtSvcRequestResponse_v1',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/srv/SPO/PartnerMgmt/ChannelPortalCacheMgmtSvcRequestResponse_v1','true','false'};
        private String[] field_order_type_info = new String[]{'referenceKey'};
    }
    public class GetCacheListResponseType {
        public DateTime dateTimeStamp;
             private String[] dateTimeStamp_type_info = new String[]{'dateTimeStamp','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v9',null,'0','1','false'};
        public ChannelPortalCacheMgmtSrvReqRes.MapItem[] customCacheList;
        private String[] customCacheList_type_info = new String[]{'customCacheList','http://xmlschema.tmi.telus.com/srv/SPO/PartnerMgmt/ChannelPortalCacheMgmtSvcRequestResponse_v1',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/srv/SPO/PartnerMgmt/ChannelPortalCacheMgmtSvcRequestResponse_v1','true','false'};
        private String[] field_order_type_info = new String[]{'dateTimeStamp','customCacheList'};
    }
    public class SaveCacheListResponseType {
        public String referenceKey;
        private String[] referenceKey_type_info = new String[]{'referenceKey','http://xmlschema.tmi.telus.com/srv/SPO/PartnerMgmt/ChannelPortalCacheMgmtSvcRequestResponse_v1',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/srv/SPO/PartnerMgmt/ChannelPortalCacheMgmtSvcRequestResponse_v1','true','false'};
        private String[] field_order_type_info = new String[]{'referenceKey'};
    }
    public class MapItem {
        public String key;
        public String value;
        private String[] key_type_info = new String[]{'key','http://xmlschema.tmi.telus.com/srv/SPO/PartnerMgmt/ChannelPortalCacheMgmtSvcRequestResponse_v1',null,'1','1','false'};
        private String[] value_type_info = new String[]{'value','http://xmlschema.tmi.telus.com/srv/SPO/PartnerMgmt/ChannelPortalCacheMgmtSvcRequestResponse_v1',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/srv/SPO/PartnerMgmt/ChannelPortalCacheMgmtSvcRequestResponse_v1','true','false'};
        private String[] field_order_type_info = new String[]{'key','value'};
    }
}