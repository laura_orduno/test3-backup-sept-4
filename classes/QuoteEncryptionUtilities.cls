/*
    Quote Encryption Utilities
    Author: Alex Miller @ Traction <amiller@tractionondemand.com>
    Created: March 14, 2012
*/
public without sharing class QuoteEncryptionUtilities {
    public class QuoteEncryptionUtilitiesException extends Exception {}

    private static String algorithmName {get; set;}
    private static Blob privateKey {get; set;}
    
    static {
        loadEncryptionSettings();
    }
    
    private static void loadEncryptionSettings() {
        Quote_Tool_Encryption_Settings__c settings = Quote_Tool_Encryption_Settings__c.getInstance();
        if (settings == null) {
            return;
        }
        algorithmName = settings.Algorithm_Name__c;
        if (settings.Private_Key__c != null) {
            privateKey = EncodingUtil.base64Decode(settings.Private_Key__c);
        }
    }
    
    public static String encryptStringAsBase64(String s) {
        return (s == null) ? null : encryptAsBase64(Blob.valueOf(s));
    }
    
    public static String encryptAsBase64(Blob b) {
        Blob x = encrypt(b);
        return (x == null) ? null : EncodingUtil.base64Encode(x);
    }
    
    public static Blob encrypt(Blob b) {
        if (algorithmName == null) {
            throw new QuoteEncryptionUtilitiesException('Missing algorithm name.');
        }
        if (privateKey == null) {
            throw new QuoteEncryptionUtilitiesException('Missing private key.');
        }
        if (b == null) {
            throw new QuoteEncryptionUtilitiesException('Argument is null.');
        }
        return Crypto.encryptWithManagedIV(algorithmName, privateKey, b);
    }
    
    public static String decryptStringFromBase64(String s) {
        Blob b = decryptFromBase64(s);
        return (b == null) ? null : b.toString();
    }
    
    public static Blob decryptFromBase64(String s) {
        return (s == null) ? null : decrypt(EncodingUtil.base64Decode(s));
    }
    
    public static Blob decrypt(Blob b) {
        if (algorithmName == null) {
            throw new QuoteEncryptionUtilitiesException('Missing algorithm name.');
        }
        if (privateKey == null) {
            throw new QuoteEncryptionUtilitiesException('Missing private key.');
        }
        if (b == null) {
            throw new QuoteEncryptionUtilitiesException('Argument is null.');
        }
        return Crypto.decryptWithManagedIV(algorithmName, privateKey, b);
    }
}