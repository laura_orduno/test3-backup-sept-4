/**
 * Test suite for ResolveCampaignNames trigger.
 * 
 * @author Max Rudman
 * @since 4/5/2009
 */

@isTest
public class SetCampaignNamesTests {
	testMethod static void testInCondition() {
		Campaign c1 = new Campaign(Name='Test1');
		insert c1;
		
		Campaign c2 = new Campaign(Name='Test2');
		insert c2;
		
		ADRA__AllocationRule__c rule = new ADRA__AllocationRule__c(Name='Test');
		insert rule;
		
		ADRA__Condition__c cond = new ADRA__Condition__c();
		cond.ADRA__Rule__c = rule.Id;
		cond.ADRA__Value__c = c1.Id;
		insert cond;
		
		ADRA__Condition__c result = [SELECT Campaign_Names__c FROM ADRA__Condition__c WHERE Id = :cond.Id];
		System.assertEquals(c1.Name,result.Campaign_Names__c);
		cond.ADRA__Value__c = c1.Id + ', ' + c2.Id;
		update cond;
		
		result = [SELECT Campaign_Names__c FROM ADRA__Condition__c WHERE Id = :cond.Id];
		System.assertEquals(c1.Name + ',' + c2.Name,result.Campaign_Names__c);
	}
	
	testMethod static void testInCampaign() {
		Campaign c1 = new Campaign(Name='Test1');
		insert c1;
		
		Campaign c2 = new Campaign(Name='Test2');
		insert c2;
		
		ADRA__AllocationRule__c rule = new ADRA__AllocationRule__c(Name='Test');
		insert rule;
		
		ADRA__Condition__c cond = new ADRA__Condition__c();
		cond.ADRA__Rule__c = rule.Id;
		cond.ADRA__Value__c = c1.Id + ', ' + c2.Id;
		insert cond;
		
		c1.Name='Test1.U';
		update c1;
		
		ADRA__Condition__c result = [SELECT Campaign_Names__c FROM ADRA__Condition__c WHERE Id = :cond.Id];
		System.assertEquals(c1.Name + ',' + c2.Name,result.Campaign_Names__c);
	}
}