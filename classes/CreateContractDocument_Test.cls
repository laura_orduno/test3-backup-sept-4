@isTest
public class CreateContractDocument_Test {
	public TestMethod static void TestCreateWordDoc() {
    	String methodName = 'createWordDoc';
        Map<String,Object> inputMap = new Map<String,Object>();
        Map<String,Object> outMap =  new Map<String,Object>();
        Map<String,Object> options = new Map<String,Object>();
        CreateContractDocument obj = new CreateContractDocument();
        
        TestDataHelper.testAccountCreation();
        TestDataHelper.testContactCreation();
		Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'All Other Orders';
        insert testOpp_1;
        Contract testContract_1  = new Contract(Contract_Type__c='Amendment',AccountId=TestDataHelper.testAccountObj.id,ContractTerm=12,StartDate=system.today(),vlocity_cmt__OpportunityId__c = testOpp_1.Id);
        insert testContract_1;
        vlocity_cmt__ContractVersion__c testContractVersion = new vlocity_cmt__ContractVersion__c(Name='Test Version 1', vlocity_cmt__ContractId__c = testContract_1.Id);
        insert testContractVersion;
		String relsXml='<test></test>';
        inputMap.put('relXml', relsXml);
        Map<String,Object> conObjMap = new Map<String,Object>();
        conObjMap.put('Id', testContract_1.Id);
        inputMap.put('contractObj', conObjMap);
        Test.startTest();
        obj.invokeMethod(methodName,inputMap,outMap,options);
        Test.stopTest();
    }
    
    public TestMethod static void TestAttachDocx() {
    	String methodName = 'attachDocx';
        Map<String,Object> inputMap = new Map<String,Object>();
        Map<String,Object> outMap =  new Map<String,Object>();
        Map<String,Object> options = new Map<String,Object>();
        CreateContractDocument obj = new CreateContractDocument();
        TestDataHelper.testAccountCreation();
        TestDataHelper.testContactCreation();
		Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'All Other Orders';
        insert testOpp_1;
        Contract testContract_1  = new Contract(Contract_Type__c='Amendment',AccountId=TestDataHelper.testAccountObj.id,ContractTerm=12,StartDate=system.today(),vlocity_cmt__OpportunityId__c = testOpp_1.Id);
        insert testContract_1;
        vlocity_cmt__ContractVersion__c testContractVersion = new vlocity_cmt__ContractVersion__c(Name='Test Version 1', vlocity_cmt__ContractId__c = testContract_1.Id);
        insert testContractVersion;
		String relsXml='<test></test>';
        inputMap.put('relXml', relsXml);
        Map<String,Object> conObjMap = new Map<String,Object>();
		conObjMap.put('Id', testContract_1.Id);
        Map<String,Object> createContractResponse = new Map<String,Object>();
        createContractResponse.put('contractObj',conObjMap);
        inputMap.put('createContractResponse', createContractResponse);
        
        Test.startTest();
        try{
        	obj.invokeMethod(methodName,inputMap,outMap,options);
        }catch(Exception ex){}
        Test.stopTest();
    }
    
    public TestMethod static void TestGetDocxTemplate() {
    	String methodName = 'getDocxTemplate';
        Map<String,Object> inputMap = new Map<String,Object>();
        Map<String,Object> outMap =  new Map<String,Object>();
        Map<String,Object> options = new Map<String,Object>();
        CreateContractDocument obj = new CreateContractDocument();
        TestDataHelper.testAccountCreation();
        TestDataHelper.testContactCreation();
		Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'All Other Orders';
        insert testOpp_1;
        Contract testContract_1  = new Contract(Contract_Type__c='Amendment',AccountId=TestDataHelper.testAccountObj.id,ContractTerm=12,StartDate=system.today(),vlocity_cmt__OpportunityId__c = testOpp_1.Id);
        insert testContract_1;
        vlocity_cmt__ContractVersion__c testContractVersion = new vlocity_cmt__ContractVersion__c(Name='Test Version 1', vlocity_cmt__ContractId__c = testContract_1.Id);
        insert testContractVersion;
		String relsXml='<test></test>';
        inputMap.put('relXml', relsXml);
        Map<String,Object> conObjMap = new Map<String,Object>();
        conObjMap.put('Id', testContract_1.Id);
        inputMap.put('contractObj', conObjMap);
        Test.startTest();
        obj.invokeMethod(methodName,inputMap,outMap,options);
        Test.stopTest();
    }
}