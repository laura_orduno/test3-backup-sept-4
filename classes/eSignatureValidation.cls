/**
 * Provide validation to eSignature
 */
global class eSignatureValidation {
    /**
     * When the user clicks Send for eSignature, the system will validate the following that the Release Code of the TELUS Signor (TELUS_Signor__c.Release_Code__c) 
     * is equal or greater than the Required Release Code (see below) for the Total Contract Value (TCV) (Total_Contract_Value__c) for the contract. If the Signor’s 
     * release code is less, return the message: The TELUS signor selected does not have sufficient signing authority. Please select a team member with the right 
     * level of signing authority based on the total contract (Deal) value.  To find out the Required Release Code, you need to query the following object by “TCV”, 
     * and where Transaction Type equal “Contract” and Business Unit equals “Default”.
     */
    webservice static list<string> validate(id contractId){
        list<string> errorMessages=new list<string>();
        approval_threshold__c threshold;
        list<schema.fieldsetmember> releaseCodeFieldList;
        try{
            string thresholdQuery='select id';
            schema.fieldset releaseCodeFieldSet=approval_threshold__c.sobjecttype.getdescribe().fieldsets.getmap().get('Release_Code');
            releaseCodeFieldList=releaseCodeFieldSet.getfields();
            for(schema.fieldsetmember releaseCodeField:releaseCodeFieldList){
                thresholdQuery+=','+releaseCodeField.getfieldpath();
            }
            thresholdQuery+=' from approval_threshold__c where business_unit__c=\'Default\' and transaction_type__c=\'Contract\' limit 1';
            threshold=database.query(thresholdQuery);
        }catch(queryexception qe){
            errorMessages.add(label.No_Approval_Threshold_Found_Error);
        }catch(exception e){
            errorMessages.add(e.getmessage());
            system.debug(e.getstacktracestring());
        }
        if(errorMessages.isempty()&&threshold!=null){
            try{
                contract currentContract=[select id,total_contract_value__c,telus_signor__c,telus_signor__r.release_code__c from contract where id=:contractId];
                if(string.isblank(currentContract.telus_signor__c)){
                    errorMessages.add(label.No_TELUS_Signor_Assigned_Error);
                }else{
                    if(string.isblank(currentContract.telus_signor__r.release_code__c)){
                        errorMessages.add(label.Blank_TELUS_Signor_Release_Code_Error);
                    }
                }
                if(errorMessages.isempty()){
                    string releaseCode=string.valueof(integer.valueof(currentContract.telus_signor__r.release_code__c));
                    for(schema.fieldsetmember releaseCodeField:releaseCodeFieldList){
                        if(releaseCodeField.getfieldpath().containsignorecase(releaseCode)){
                            double tcvThreshold=(double)(threshold.get(releaseCodeField.getfieldpath()));
                            if(currentContract.total_contract_value__c>tcvThreshold){
                                errorMessages.add(label.Contract_Exceeded_Signing_Authority_Error);
                                break;
                            }
                        }
                    }
                }
            }catch(queryexception qe){
                errorMessages.add(label.No_Contract_Found_Error);
            }catch(exception e){
                errorMessages.add(e.getmessage());
                system.debug(e.getstacktracestring());
            }
        }
        return errorMessages;
    }
}