public class ChannelOrgOutletsJSON {
    public String currentChannelOutletID {get;set;} 
    public String currentChannelOutletDescription {get;set;} 
    public List<String> currentOutletCategoryKeys {get;set;} 
   
    public ChannelOrgOutletsJSON(JSONParser parser) {
        
        while (parser.nextToken() != JSONToken.END_OBJECT) {                
            if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                String text = parser.getText();
                if (parser.nextToken() != JSONToken.VALUE_NULL) {
                    if (text == 'currentChannelOutletID') {
                        currentChannelOutletID = parser.getText();
                    } else if (text == 'currentChannelOutletDescription') {
                        currentChannelOutletDescription = parser.getText();
                    } else if (text == 'currentOutletCategoryKeys') {
                        currentOutletCategoryKeys = new List<String>();
                        while (parser.nextToken() != JSONToken.END_ARRAY) {
                            //currentOutletCategoryKeys.add(new String(parser));
                            if(parser.getCurrentToken()==JSONToken.VALUE_STRING){
                                currentOutletCategoryKeys.add(parser.getText());
                            }
                            
                        }
                    } else {
                        System.debug(LoggingLevel.WARN, 'ChannelOrgOutletsJSON consuming unrecognized property: '+text);
                        consumeObject(parser);
                    }
                }
            }
        }        
    }
    
    public static void consumeObject(JSONParser parser) {
        Integer depth = 0;
        do {
            JSONToken curr = parser.getCurrentToken();
            if (curr == JSONToken.START_OBJECT || 
                curr == JSONToken.START_ARRAY) {
                    depth++;
                } else if (curr == JSONToken.END_OBJECT ||
                           curr == JSONToken.END_ARRAY) {
                               depth--;
                           }
        } while (depth > 0 && parser.nextToken() != null);
    }
    
    public static ChannelOrgOutletsJSON parse(String json) {
        return new ChannelOrgOutletsJSON(System.JSON.createParser(json));
    }
}