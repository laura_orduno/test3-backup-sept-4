@isTest
private class AddressWrapperTest {
    private static testMethod void testAddressWrapper() {
        final String STREET = 'Street';
        final String CITY = 'City';
        final String STATE_PROVINCE = 'BC';
        final String COUNTRY = 'Canada';
        final String POSTAL_CODE = 'V6V 1V1';
        final String BUILDING_NAME = 'The Test Building';
        final String FLOOR = '20';
        final String SUITE = '201';
        final String STREET_TYPE = 'Street';
        final String STREET_DIRECTION = 'North';
        
        Address__c addr = new Address__c();
        insert addr;
        
        AddressWrapper aw = new AddressWrapper(addr, 'Address', 'key');
        
        system.assert(aw.isEmpty());
        system.assertNotEquals(null, aw.getRecordId());
        
        
        addr.Street__c = STREET;
        addr.City__c = CITY;
        addr.State_Province__c = STATE_PROVINCE;
        addr.Country__c = COUNTRY;
        addr.Postal_Code__c = POSTAL_CODE;
        addr.Building_Name__c = BUILDING_NAME;
        addr.Floor__c = FLOOR;
        addr.Suite_Unit__c = SUITE;
        addr.Street_Type__c = STREET_TYPE;
        addr.Street_Direction__c = STREET_DIRECTION;
        
        aw = new AddressWrapper(addr, 'Address', 'key');
        
        system.assertEquals(false,aw.isEmpty());
        system.assertEquals(STREET, aw.getStreet());
        system.assertEquals(CITY, aw.getCity());
        system.assertEquals(STATE_PROVINCE, aw.getState());
        system.assertEquals(COUNTRY, aw.getCountry());
        system.assertEquals(POSTAL_CODE, aw.getPostalCode());
        system.assertEquals(BUILDING_NAME, aw.getBuildingName());
        system.assertEquals(FLOOR, aw.getFloor());
        system.assertEquals(SUITE, aw.getSuiteUnit());
        system.assertEquals(STREET_TYPE, aw.getStreetType());
        system.assertEquals(STREET_DIRECTION, aw.getStreetDirection());
        
        system.assertEquals(null, aw.getObjectDisplay());
    }
}