@isTest
private class QuoteCloneRequestFillerTest {
    private static Web_Account__c account;
    private static Address__c location;
    private static SBQQ__Quote__c quote;
    
    
    testMethod static void testInsert() {
        setUp();
        
        Quote_Clone_Request__c r = new Quote_Clone_Request__c();
        r.Source_Quote__c = quote.id;
        r.Destination_Quote__c = quote.id;
        r.Requester__c = UserInfo.getUserId();
        r.Cloner__c = UserInfo.getUserId();
        
        insert r;
    }
    
    
    private static void setUp() {
        account = new Web_Account__c(Name='UnitTest');
        insert account;
        
        location = new Address__c(Web_Account__c=account.Id,Special_Location__c='Test');
        location.Rate_Band__c = 'F';
        location.State_Province__c = 'BC';
        insert location;
        
        Opportunity opp = new Opportunity(Name='Test',Web_Account__c=account.Id,CloseDate=System.today(),StageName='Prospecting');
        insert opp;
        
        quote = new SBQQ__Quote__c(SBQQ__Opportunity__c=opp.Id);
        quote.Rate_Band__c = 'F';
        quote.Province__c = 'BC';
        insert quote;
        
        Product2 p = new Product2(Name='Test');
        insert p;
        
        SBQQ__QuoteLine__c line = new SBQQ__QuoteLine__c(SBQQ__Quote__c=quote.Id,SBQQ__Bundle__c=true,SBQQ__Product__c=p.Id);
        insert line;
    }
}