public class RecomboClient {
    public static final String VERSION = '4.1';
    private static final String DEFAULT_CANCEL_MESSAGE = 'Your TELUS Business Anywhere agreement has been cancelled. Please contact your sales representative for more information.';
    
    public Recombo.Document document {get; set;}
    public Recombo.EmailInfo emailInfo {get; set;}
    public Recombo.Person[] persons {get; private set;}
    public String errorCode {get; set;}
    public String errorMessage {get; set;}
    public String documentURL {get; private set;}
    public String waybillId {get; private set;}
    
    private String username;
    private String password;
    private String endpoint;
    private static String sessionId;
       
    
    public RecomboClient(String endpoint, String username, String password) {
        this.endpoint = endpoint;
        this.username = username;
        this.password = password;
        this.persons = new List<Recombo.Person>();
    }
    
    
    public String login() {   
        HttpRequest request = new HttpRequest();
        request.setMethod('GET');
        String url = endpoint + '/Login?version=' + VERSION + '&sysId=' + username + '&sysPwd=' + password;                   
        request.setEndpoint(url);
        
        Http http = new Http();
        HTTPResponse response = Test.isRunningTest() ? RecomboClientTest.createTestLoginResponse() : http.send(request);

        if (response.getStatusCode() > 200) {
            String errorBody = response.getBody();
            throw new Recombo.LoginException('Response code ' + String.valueOf(response.getStatusCode()) + ': ' + errorBody);
        }
        
        Dom.XmlNode rootElement = response.getBodyDocument().getRootElement(); 
        Dom.Xmlnode errorElement = rootElement.getChildElement('error', null);
        String errorCode = errorElement.getText();
        
        if (!'0'.equals(errorCode)) {
            String message = rootElement.getChildElement('message', null).getText();
            throw new Recombo.LoginException('API error ' + errorCode + '. Message: ' + message);
        }
        
        Dom.XmlNode sidElement = rootElement.getChildElement('sid', null);
        sessionId = sidElement.getText();
        return sessionId;
    }
    
    
    public void sendDocument() {
        checkLogin();
        String docId = document.Id;

        MultipartRequest multipartRequest = new MultipartRequest();         
        MultipartRequest.Part part = new MultipartRequest.Part();
        
        part.setContentDisposition('form-data');
        part.addContentDispositionAttribute('name', 'version');
        part.setContent(VERSION);
        multipartRequest.addPart(part);
        
        part = new MultipartRequest.Part();
        part.setContentDisposition('form-data');
        part.addContentDispositionAttribute('name', 'sid');
        part.setContent(sessionId);
        multipartRequest.addPart(part);         

        part = new MultipartRequest.Part();
        part.setContentDisposition('form-data');
        part.addContentDispositionAttribute('name', 'docId');
        part.setContent(docId);
        multipartRequest.addPart(part); 
        
        part = new MultipartRequest.Part();
        part.setContentDisposition('form-data');
        part.addContentDispositionAttribute('name', 'isBase64');
        part.setContent('1');
        multipartRequest.addPart(part); 

        part = new MultipartRequest.Part();
        part.setContentDisposition('form-data');
        part.addContentDispositionAttribute('name', 'docData');
        part.addContentDispositionAttribute('filename', document.fileName);
        part.setContentType(document.contentType);
        part.setContentTransferEncoding('base64');
        part.setContent(Encodingutil.base64Encode(document.Body));
        multipartRequest.addPart(part);

        if (emailInfo != null) {
            part = new MultipartRequest.Part();
            part.setContentDisposition('form-data');
            part.addContentDispositionAttribute('name', 'emailInfo');
            part.addContentDispositionAttribute('filename', 'emailInfo.xml');
            part.setContentType('text/xml');
            part.setContent(buildEmailInfo());
            multipartRequest.addPart(part);            
        }               
        
        if (persons != null) {
            part = new MultipartRequest.Part();
            part.setContentDisposition('form-data');
            part.addContentDispositionAttribute('name', 'userInfo');
            part.addContentDispositionAttribute('filename', 'userInfo.xml');
            part.setContentType('text/xml');
            part.setContent(buildUserInfo());
            multipartRequest.addPart(part);  
        }
        
        part = new MultipartRequest.Part();
        part.setContentDisposition('form-data');
        part.addContentDispositionAttribute('name', 'sigMap');
        part.addContentDispositionAttribute('filename', 'userInfo.xml');
        part.setContent('');
        multipartRequest.addPart(part);         
        
        multipartRequest.prepare();
        map<string,schema.recordtypeinfo> logRecordTypeMap=webservice_integration_error_log__c.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        webservice_integration_error_log__c log=new webservice_integration_error_log__c(recordtypeid=logRecordTypeMap.get('Success').getrecordtypeid(),
                                                                                        status__c='Passed',apex_class_and_method__c='RecomboClient.sendDocument',
                                                                                        external_system_name__c='Recombo',
                                                                                       webservice_name__c='SendDocument');
        String boundary = multipartRequest.getBoundary();
        String body = multipartRequest.getBody(); 
        
        String url = endpoint + '/SendDocument';
        HttpRequest request = new HttpRequest();
        request.setMethod('POST');
        request.setHeader('Accept', 'text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8');
        request.setHeader('Connection','keep-alive');
        request.setHeader('Accept-Charset', 'utf-8;q=0.7,*;q=0.7');
        request.setEndpoint(url);
        request.setHeader('Content-Type', 'multipart/form-data; boundary=' + boundary);
        request.setBody(body);
        log.request_payload__c=(string.isnotblank(body)&&body.length()>32768?body.left(327677):body);
        system.debug('\n\n\n############\n\n\nDEBUGGING REQUEST:\n\n\n');
        system.debug(request);
        system.debug('\n\n\n############\n\n\nEND DEBUG\n\n\n');  

        Http http = new Http();         
        HTTPResponse response = Test.isRunningTest() ? RecomboClientTest.createTestSendDocumentResponse() : http.send(request);
        system.debug(response);
        system.debug(response.getBody());
		log.error_code_and_message__c=response.getbody();
        if (response.getStatusCode() != 200) {
            log.recordtypeid=logRecordTypeMap.get('Failure').getrecordtypeid();
            log.status__c='Failed';
            //RecomboClient.logRecomboWebservice(log);
            Util.logWebservice(log);
            throw new Recombo.SendException(response.getStatusCode() + ': ' + response.getBody());
        }else{
            //RecomboClient.logRecomboWebservice(log);
            Util.logWebservice(log);
        }
        
        Dom.Document doc = new Dom.Document();
        doc.load(response.getBody());
        
        Dom.XmlNode error = doc.getRootElement().getChildElement('error', null);
        Dom.XmlNode message = doc.getRootElement().getChildElement('message', null);
        
        if (error.getText().trim() != '0') {
            throw new Recombo.SendException('Error sending document: ' + message.getText());
        }
        
        waybillId = doc.getRootElement().getChildElement('waybillId', null).getText();
        documentUrl = parseDocURL(response.getBody());
    }
    /*
    @future
    public static void logRecomboWebservice(webservice_integration_error_log__c log){
        try{
            insert log;
        }catch(exception e){
            Util.emailSystemError(e);
        }
    }*/
    
    public integer deleteDocument(String waybill, String reason) {
        return deleteDocument(waybill, reason, null);
    }
    
    
    public integer deleteDocument(String waybill, String reason, String replacementWaybill) {
        checkLogin();
        if (waybill == null || waybill.length() < 32) {
            throw new Recombo.SendException('Invalid Waybill ID');
        }
        
        if (reason == null) {
            reason = DEFAULT_CANCEL_MESSAGE;
        }
        
        if (replacementWaybill != null && replacementWaybill.length() < 32) {
            throw new Recombo.SendException('Invalid Replacement Waybill ID');
        }
        
        String url = endpoint + '/DeleteAgreement';
        String body =
        	'version=' + version +
        	'&sid=' + sessionId +
        	'&agreementGuid=' + waybill.trim() +
        	'&comment=' + reason + 
        	(replacementWaybill != null ? '&agreementguidToReplace=' + replacementWaybill.trim() : '');
        
        HttpRequest request = new HttpRequest();
        request.setMethod('POST');
        request.setEndpoint(url);
        request.setBody(body);

        Http http = new Http();         
        HTTPResponse response = Test.isRunningTest() ? RecomboClientTest.createTestDeleteDocumentResponse() : http.send(request);
        
        system.debug('\n\n\n############\n\n\nDEBUGGING RESPONSE:\n\n\n');
        system.debug(response);
        system.debug('\n\n\n############\n\n\nEND DEBUG\n\n\n');
          
        if (response.getStatusCode() != 200) {
            throw new Recombo.SendException('Error deleting document: ' + response.getStatusCode() + ': ' + response.getBody());
        }
        
        String responseBody = response.getBody();
        if(responseBody == null || responseBody.length() == 0) {
        	throw new Recombo.SendException('Error deleting document: Received empty response');
        }
        
        DOM.Document doc = new DOM.Document();
        doc.load(responseBody);
        
        Dom.XmlNode error = doc.getRootElement().getChildElement('error', null);
        String message = doc.getRootElement().getChildElement('message', null).getText();
        
        if (error.getText().trim() != '0') {
            if (responseBody.toLowerCase().contains('agreement already cancelled')) {
                return 2;
            } else if (responseBody.toLowerCase().contains('agreement cancelled')) {
                return 1;
            }
            throw new Recombo.SendException('Error deleting document: ' + message);
        }
        
        return 1;
    }   
    
    
    private void checkLogin() {
        if (sessionId == null) {
            login();
        }   
    }
   
   
    private String buildEmailInfo() {       
        return '<?xml version="1.0" encoding="utf-8"?>'
            + '\r\n<emailInfo>'
            + '\r\n<subject>' + emailInfo.subject + '</subject>'
            + '\r\n<message>' + emailInfo.message + '</message>'
            + '\r\n</emailInfo>';       
    }
    
    
    private String buildUserInfo() {
        String xmlString = '<?xml version="1.0" encoding="utf-8"?>' 
                        + '\r\n<userInfo>';
        for (Recombo.Person person : persons) {
            if (person.uid != null) {
                xmlString += '\r\n<person>'
                         + '\r\n<uid>' + person.uid.toLowerCase() + '</uid>'
                         + '\r\n<firstName>' + person.firstName + '</firstName>'
                         + '\r\n<lastName>' + person.lastName + '</lastName>'
                         + '\r\n</person>';
            }
        }
        xmlString += '\r\n</userInfo>';     
        return xmlString;
    }
    
    
    /**
     * For some reason DOM.Document class deosn't handle CDATA sections
     * so we can't use it to return docuURL which is inclosed in CDATA.
     * This methos uses XMLStreamReader instead;
     */
    private String parseDocURL(String xml) {
        XmlStreamReader xsr = new XmlStreamReader(xml);
        while (xsr.hasNext()) {
            if (xsr.isStartElement() && xsr.getLocalName().equalsIgnoreCase('docUrl')) {
               xsr.next();
               return xsr.getText();
            }
           xsr.next();
        }
        return null;
    }
}