@isTest
public class AccountTaskListExt_Test {
    
    @isTest(SeeAllData=false) 
    static void testOne(){
        
        Account acct = new Account(name = 'test');
        insert acct;
        
        Task newTask = new Task(Subject='test subject', WhatId = acct.id);
        insert newTask;

//        Task t = new Task(AccountId = acct.id);
//        insert t;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(acct);
		AccountTaskListExt ctlr = new AccountTaskListExt(sc);
        
        PageReference pageRef = Page.AccountTaskList;
        pageRef.getParameters().put('id', String.valueOf(acct.Id));
        Test.setCurrentPage(pageRef);    
		System.currentPageReference().getParameters().put('column', 'General');
		System.currentPageReference().getParameters().put('search', 'Test');
        
        ctlr.searchText = 'test';
        ctlr.searchTasks();
        ctlr.searchTasksByColumn();
		System.currentPageReference().getParameters().put('column', 'Subject');
        ctlr.searchTasksByColumn();
    }        
}