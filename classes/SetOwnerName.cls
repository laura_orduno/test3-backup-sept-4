/*****************************************************
    Name: SetOwnerName 
    Usage: This class assign the Owner NAme from owner of Lead or Opportunity
    Author – Clear Task
    Date – 02/05/2011    
    Revision History
******************************************************/
public with sharing class SetOwnerName {
    public static void populateFieldOnOwnerName(List<sObject> objList){ 
    //Queue Pattern standard     
     String QUEUE_PATTERN = '00G';
    //User Pattern standard 
     String USER_PATTERN = '005';
    //owner id field Api
     String OWNER_ID = 'OwnerId';
    //owner Name field Api
     String OWNER_NAME = 'Owner_Name__c';
    //partner user as profile
     String PARTNER_USER = 'PowerPartner';
    //partner account as profile
     String PARTNER_Account = 'Partner_Account__c';
    //create set of Id as user or queue from owner -Start
        Set<Id> userId = new Set<Id> ();
        Set<Id> queueId = new Set<Id> ();
        for(SObject s :objList){            
            if(s.get(OWNER_ID) != null){                
                if(((''+ (Id)s.get(OWNER_ID)).subString(0, 3)).equals(USER_PATTERN)){
                    userId.add((Id)s.get(OWNER_ID));
                }else if(((''+ (Id)s.get(OWNER_ID)).subString(0, 3)).equals(QUEUE_PATTERN)){
                    queueId.add((Id)s.get(OWNER_ID));
                }
            }
        } 
    //create set of Id as user or queue from owner -End      
    //create map of Owner Name -Start
        Map<Id,User> userMap = new Map<Id,User>([select u.UserType, u.Contact.AccountId, u.FirstName, u.LastName from User u where id in :userId]);
        System.debug('userMap=' + userMap );
        Map<Id, Group> queueMap = new Map<Id, Group>([Select g.Name, g.Id From Group g where g.Id in :queueId]);
        System.debug('queueMap=' + queueMap );
        if((userMap != null && userMap.size()>0) || (queueMap != null && queueMap.size()>0)){
            for(SObject s :objList){            
               if(s.get(OWNER_ID) != null){    
                    if(((''+ (Id)s.get(OWNER_ID)).subString(0, 3)).equals(USER_PATTERN)){
                        User usr = userMap.get((Id)s.get(OWNER_ID));
                        System.debug('usr.UserType='+ usr.UserType);
                        if(usr != null && usr.UserType.equalsIgnoreCase(PARTNER_USER)){
                            s.put(OWNER_NAME, '');                     
                        }else if(usr != null){
                            s.put(OWNER_NAME, usr.FirstName + ' ' + usr.LastName);                      
                        }
                    }else if(((''+ (Id)s.get(OWNER_ID)).subString(0, 3)).equals(QUEUE_PATTERN)){
                        Group q = queueMap.get((Id)s.get(OWNER_ID));
                        if(q != null){
                            s.put(OWNER_NAME, q.Name); 
                            s.put(PARTNER_Account, '');   
                       }                 
                    }
                }                     
            }
        } 
    //create map of Owner Name -End       
   }
}