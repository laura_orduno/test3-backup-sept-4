@isTest
public class EmailToCaseTriggerHandler_Test {
    private static Account createAccount() { 
        //RecordType accRecordType = [select Id, Name from RecordType where SObjectType = 'Account' and Name ='RCID'];
        map<string,schema.recordtypeinfo> recordTypeInfo=account.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        schema.recordtypeinfo accRecordType=recordTypeInfo.get('RCID');
        Account acc = new Account(Name = 'Test Account', RecordTypeId = accRecordType.getrecordtypeid());
        insert acc;
        return acc;
    }
    
    static testMethod void createCase() {
        Account acc = createAccount();
        Contact contact = new Contact(LastName='Test Contact ', accountId=acc.id );
        contact.Email='johnson.wong@telus.com';
        insert contact;
        //RecordType caseRecordType = [select Id, Name from RecordType where SObjectType = 'Case' and Name ='SMB Care Interim Connectivity'];
        map<string,schema.recordtypeinfo> recordTypeInfo=case.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        schema.recordtypeinfo caseRecordType=recordTypeInfo.get('SMB Care Interim Connectivity');
        Case caseobj = new Case();
        caseobj.RecordTypeId = caseRecordType.getrecordtypeid();
        caseobj.Status='Open';
        caseobj.Origin='Client';
        caseobj.AccountId=acc.id;
        caseobj.Subject ='test case';
        caseobj.ContactId=contact.id;
        caseobj.Contact_Email__c='johnson.wong@telus.com';
        caseobj.SuppliedEmail='kelly.lu@telus.com';
        insert caseobj;
    } 
}