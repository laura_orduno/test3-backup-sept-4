global class smb_ConsolidatedBillingHierarchy implements Database.Batchable<sObject> {

	global smb_ConsolidatedBillingHierarchy() {}
	
	global Database.QueryLocator start(Database.BatchableContext BC){
		return Database.getQueryLocator(
			'SELECT id, parentId, bcan__c, ban_can__c ' +
			'FROM account ' +
			'WHERE RecordType.DeveloperName=\'CAN\' OR RecordType.DeveloperName=\'BAN\' ' +
			'order by createddate desc'
		);
   }
   
   global void execute(Database.BatchableContext BC, List<sObject> scope) {
   	
   		List<String> bcans = new List<String>();
   		Map<String, List<Account>> accountsMappedToBcan = new Map<String, List<Account>>();
   	
		for (sObject item : scope) {
			Account acc = (Account)item;
			
			if (acc.bcan__c == null || acc.bcan__c == acc.ban_can__c) continue;
			
			bcans.add(acc.bcan__c);
			if (!accountsMappedToBcan.containsKey(acc.bcan__c)) {
				accountsMappedToBcan.put(acc.bcan__c, new List<Account>());
			}
			accountsMappedToBcan.get(acc.bcan__c).add(acc);
		}
		
		List<Account> accountsToUpdate = new List<Account>();
		
		for (Account parentAccount : [SELECT Id, BAN_CAN__c FROM Account WHERE BAN_CAN__c IN :bcans]) {
			
			if (!accountsMappedToBcan.containsKey(parentAccount.BAN_CAN__c)) continue;
			
			for (Account childAccount : accountsMappedToBcan.get(parentAccount.BAN_CAN__c)) {
				if (childAccount.parentId != parentAccount.Id) {
					childAccount.parentId = parentAccount.Id;
					accountsToUpdate.add(childAccount);
				}
			}
			
		}
		
		update accountsToUpdate;
   	
   }
   
   global void finish(Database.BatchableContext BC) {}

}