/* Developed By: Hari Neelalaka, IBM
*  Created Date: 22-Dec-2014
*/
@isTest

private Class SRS_MyCRIS3Orders_Controller_Test{
    
    static testMethod void testSRS_MyCRIS3Orders_Controller(){  
        
        // creating an account
        Account acc = new Account();
        acc.Name = 'test';
        insert acc;
        // creating a service request referencing an account created above
        Service_Request__c sr = new Service_Request__c();
        sr.Account_Name__c = acc.Id;
        sr.Service_Request_Status__c='originated';
        insert sr;
  /* test.startTest();     
Employee__c emp = new Employee__c(User__c=Userinfo.getUserId());
        insert emp;
        test.stopTest();
Service_Request_Employee__c sremp= new Service_Request_Employee__c(Service_Request__c=sr.id,Team_Member__c=emp.id);    
        insert sremp;
*/         
        // This call returns an array of DescribeSObjectResult objects
        Schema.DescribeSObjectResult dsr = Schema.SObjectType.SRS_Service_Request_Related_Order__c; 
        // This method "getRecordTypeInfosByName" return maps that associate RecordTypeInfo with record IDs and record labels
        Map<String,Schema.RecordTypeInfo> rtMapByName = dsr.getRecordTypeInfosByName();
        Schema.RecordTypeInfo rtByName =  rtMapByName.get('SRT2');
        SRS_Service_Request_Related_Order__c objLegacyOrder = new SRS_Service_Request_Related_Order__c(
                    RO_Service_Request__c=sr.id,name='1234567',Seq__c='2',Originating_Date__c=system.today(),
                    recordtypeid=rtByName.getRecordtypeid(),
                    SRS_LegacyOrderToActionFlag__c=true,
                    System__c='SRT2');
        // Insert new record in "SRS_Service_Request_Related_Order__c" object    
        
        Database.insert(objLegacyOrder);
        
        // Instantiate "SRS_MyCRIS3Orders_Controller"
        
        SRS_MyCRIS3Orders_Controller objCtrl=new SRS_MyCRIS3Orders_Controller();
        objCtrl.getSortDirection();
        objCtrl.setSortDirection('test');
        objCtrl.sortExpression='LastModifiedDate';
        objCtrl.getsortDirection();      
        objCtrl.sortExpression='DD_Scheduled__c';
        objCtrl.getsortDirection();
        objCtrl.SortRelatedListwithColumn();
        objCtrl.getLegacyOrderList();
        
        List<SRS_MyCRIS3Orders_Controller.LegOrder> LegOrderL = new List<SRS_MyCRIS3Orders_Controller.LegOrder>();
        
        //................Sorting by City.................... 
        
        SRS_MyCRIS3Orders_Controller.SORT_FIELD = 'city';
        SRS_MyCRIS3Orders_Controller.SORT_DIR = 'ASC';
        SRS_MyCRIS3Orders_Controller.LegOrder objLegOrder=new SRS_MyCRIS3Orders_Controller.LegOrder();
        objLegOrder.LOrder=objLegacyOrder;
        objLegOrder.OrderNumber='1234567';
        LegOrderL.add(objLegOrder);
        LegOrderL.sort();
        
        //................Sorting by Last Modified Date....................
         
        SRS_MyCRIS3Orders_Controller.SORT_FIELD = 'LastModifiedDate';
        SRS_MyCRIS3Orders_Controller.LegOrder objLegOrder1=new SRS_MyCRIS3Orders_Controller.LegOrder();
        objLegOrder1.LOrder=objLegacyOrder;
        objLegOrder1.OrderNumber='1234567';
        LegOrderL.add(objLegOrder1);
        LegOrderL.sort();
        
        //................Sorting by DD_Scheduled__c....................
         
        SRS_MyCRIS3Orders_Controller.SORT_FIELD = 'DD_Scheduled__c';
        SRS_MyCRIS3Orders_Controller.LegOrder objLegOrder2=new SRS_MyCRIS3Orders_Controller.LegOrder();
        objLegOrder2.LOrder=objLegacyOrder;
        objLegOrder2.OrderNumber='1234567';
        LegOrderL.add(objLegOrder2);
        LegOrderL.sort();
        
        //................Sorting by RO_Service_Request__r.Name....................
         
        SRS_MyCRIS3Orders_Controller.SORT_FIELD = 'RO_Service_Request__r.Name';
        SRS_MyCRIS3Orders_Controller.LegOrder objLegOrder3=new SRS_MyCRIS3Orders_Controller.LegOrder();
        objLegOrder3.LOrder=objLegacyOrder;
        objLegOrder3.OrderNumber='1234567';
        LegOrderL.add(objLegOrder3);
        LegOrderL.sort();   
        
         SRS_MyCRIS3Orders_Controller.LegOrder objLegOrder4=new SRS_MyCRIS3Orders_Controller.LegOrder(objLegacyOrder);
        
    }
}