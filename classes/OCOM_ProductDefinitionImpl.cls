global with sharing class OCOM_ProductDefinitionImpl implements vlocity_cmt.GLobalInterfaces.ProductDefinitionInterface {
    SObject parentItem;
    SobjectType parentObjectTypeForPage;
    SObjectType lineItemTypeForPage = null;
    vlocity_cmt.ProductLineItemActionParam productLineItemActionParam;
    Map<String, Object> outputMap;
    Id lnItemId;
    Map<Id, List<Id>> parentProductToChildProds = new Map<Id, List<Id>>();
    Map<String, vlocity_cmt__ProductChildItem__c> parentChildToProductChildItemMap = new Map<String, vlocity_cmt__ProductChildItem__c>();
    Map<Id, Map<String, Object>> offerProdIdToInfoMap = new Map<Id, Map<String, Object>>();

    global void handleAction(vlocity_cmt.ProductLineItemActionParam productLineItemActionParam, Map<String, Object> outputMap)
    {

        this.productLineItemActionParam = productLineItemActionParam;
        this.outputMap = outputMap;
        vlocity_cmt.DefaultProductDefinitionImplementation prodDef = new vlocity_cmt.DefaultProductDefinitionImplementation();
        if (productLineItemActionParam.action.equalsIgnoreCase('AddLineItems')) {
            // check for product.overrideType == 'override child'
            
            // this if from default impl
            System.Debug('#### productLineItemActionParam.productDefinition = '+ productLineItemActionParam.productDefinition);
            System.Debug('#### productLineItemActionParam.pricebookEntryId = '+ productLineItemActionParam.pricebookEntryId);
            System.Debug('#### productLineItemActionParam.itemInfoMap = '+ productLineItemActionParam.itemInfoMap);
            productLineItemActionParam.itemInfoMap = new Map<String,Object> ();
            String productDefinition = productLineItemActionParam.productDefinition;
            
            List<vlocity_cmt.ProductWrapper> products = null;
            // if(productDefinition == null){
                //getProductHierarchy(rootLineNumber, pricebookEntryId);
                productLineItemActionParam.action = 'getProductHierarchy';
                prodDef.handleAction(productLineItemActionParam, this.outputMap);
                System.Debug('#### this.outputMap = '+ this.outputMap);

                products = (List<vlocity_cmt.ProductWrapper>)this.outputMap.get('prodHierarchy');
                System.Debug(LoggingLevel.ERROR,'#### products.size() = '+ products.size());
                
                Set<Id> allProductIds = new Set<Id>();
                for(Integer i=0; i < products.size(); i++) {
                    vlocity_cmt.ProductWrapper prod = products[i];
                    if(prod.productId !=null){
                        allProductIds.add(prod.productId);
                    } 

                }
                
                List<vlocity_cmt__ProductChildItem__c> prodChildList = [select Id, 
                                                                    vlocity_cmt__ChildProductId__c,
                                                                    vlocity_cmt__ParentProductId__c, 
                                                                    vlocity_cmt__ChildProductId__r.name, 
                                                                    OCOMOverrideType__c 
                                                                    from vlocity_cmt__ProductChildItem__c
                                                                    where vlocity_cmt__ParentProductId__c in :allProductIds 
                                                                    and vlocity_cmt__ChildProductId__c in  :allProductIds 
                                                                    and OCOMOverrideType__c != 'Override Child'];
                
                System.Debug('#### prodChildList.size() = '+ prodChildList.size());
                
                
                Map<Id,vlocity_cmt__ProductChildItem__c>  mapProd2IdAndProdChildItem =  new  Map<Id,vlocity_cmt__ProductChildItem__c>();                                              
                
                for(vlocity_cmt__ProductChildItem__c cp: prodChildList){
                    System.Debug(LoggingLevel.ERROR,'#### cp.name = '+ cp.vlocity_cmt__ChildProductId__r.Name + ', cp.childId='+cp.vlocity_cmt__ChildProductId__c) ;
                    mapProd2IdAndProdChildItem.put(cp.vlocity_cmt__ChildProductId__c,cp);
                }

                String prodDefSer = '[';
                
                Map<Id, Product2> mapProds = new Map<Id, Product2>([select id,Name from Product2 where  Sellable__c =true and Id in :allProductIds ]); 
                for(Product2 p: mapprods.values()){
                    System.Debug(LoggingLevel.ERROR,'#### p.Name = '+ p.Name ) ;
                }
                System.Debug(LoggingLevel.ERROR,'#### mapProds.size() = '+ mapProds.values().size() ) ;
                Integer count =0;
                for(Integer i=0; i < products.size(); i++) {
                    vlocity_cmt.ProductWrapper prod = products[i];
                    
                    if(!mapProd2IdAndProdChildItem.containsKey(prod.productId) && !mapprods.containsKey(prod.productId)) {
                        System.Debug(LoggingLevel.ERROR,'#### prod.name = '+ prod.name + ', prod.prod.productId='+prod.productId) ;
                        continue;
                    }

                    else  {  
                        count ++;
                        prodDefSer = prodDefSer + prod.toJSON() +',';
                    }
                }
                prodDefSer = prodDefSer.trim().removeEnd(',');
                prodDefSer += ']';
                System.Debug('#### prod actual count = '+ count);
                
                productLineItemActionParam.productDefinition = prodDefSer;

                productLineItemActionParam.action = 'AddLineItems';

            
         
            prodDef.handleAction(productLineItemActionParam, outputMap);



        } else {
            
            prodDef.handleAction(productLineItemActionParam, outputMap);
        }
    }



}