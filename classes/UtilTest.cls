@isTest
private class UtilTest {
	testMethod static void unit() {
		Account a = new Account(Name = 'Test-1');
		insert a;
		Account a1 = new Account(Name = 'Test-2');
		insert a1;
		Set<String> s = new Set<String> {'Name'};
		Util.hasChanges(s, a, a1);
		Util.hasChanges(s, null, a1);
		
		List<String> l = new List<String> {'a', 'b', 'c'};
		Util.join(l, ',', ',');
		Util.join(l, null, ',');
		//Util.join(null, ',', ',');
		Util.join(l, ',', null);
		
		Util.isBlank('tester');
	}
    
    testMethod static void testGetPickListValues() {
		
		List<String>  listValues = Util.getPickListValues(new Account(), 'Rating');
        System.assert(listValues.size()>0, 'Account Rating pick list values should contain more than 1 value');
	}
    
    testMethod static void getRecordTypeIdByName() {		        
        ID recordTypeId = Util.getRecordTypeIdByName('Product2', 'Rate Plan');
        System.assert(recordTypeId!=null, 'recordTypeId not found...');
	}
    @isTest
    public static void testQueryBuilder() {
        String query1= Util.queryBuilder('Product2', 'Rate_Plan_Columns',new List<String>{'Id:tId'});
        String query2= Util.queryBuilder('Account', new List<String>{'Id','Name'},null);
        System.assertEquals('SELECT Id,Name FROM Account', query2,'tesing queryBuilder with List');
	}
    @isTest
    public static void testGetFieldSetFields() {		        
        List<String> fields = Util.getFieldSetFields('Product2', 'Rate_Plan_Columns');
        System.assert(fields!=null, 'Field Rate Show not setup in Product2 object');
	}
    @isTest
    static void testGetRecordTypeIdByName() {
        test.starttest();
        Product2 prod = new Product2();
        Id recordTypeId = Util.getRecordTypeIdByName(prod,'Rate Plan');
        map<id,schema.recordtypeinfo> telusAccountRecordTypeMap=account.sobjecttype.getdescribe().getrecordtypeinfosbyid();
        Util.getRecordTypeNameById('Product2',recordTypeId);
        Util.getRecordTypeNameById(prod,recordTypeId);
        Util.getRecordTypesAsSelectOptions('Product2');
        Util.getUpdatableFields('Product2');
        Util.getFieldDescribe('Product2','Name');
        Util.join(new set<string>{'test1','test2'},',','{}');
        test.stoptest();
	}
    @isTest
    static void testIndexOf() {
        Product2 prod3 = new Product2(Name='Test Prod 1');
        
        List<Product2> items =new List<Product2>{new Product2(Name='Test Prod 1'),new Product2(Name='Test Prod 2')};
		Test.startTest();
        insert items;
        insert prod3; 
        Integer index1 = Util.indexOf(items, items[0]);
        Integer index2 = Util.indexOf(items, prod3);
        System.assert(index1==0, 'Index of the objec os not correct');
        System.assert(index2==-1, 'Index of the objec os not correct');
        System.debug(LoggingLevel.INFO,'managerAssign SOQL Usage : ' +  Limits.getQueries());
        Test.StopTest();
	}
    @istest
    static void testSendSystemErrorDatabase(){
        test.starttest();
		Account a = new Account(Name = 'Test-1');
		database.saveresult saveResults=database.insert(a,false);
        Util.emailSystemError(new list<database.saveresult>{saveResults}, 'test subject', true);
        test.stoptest();
    }
    @istest
    static void testSendSystemError(){
        test.starttest();
        try{
            Util.emailTriggerThreshold();
            user testQueryException=database.query('select id,test_field_that_should_not_exist__c from user where name=\'Should throw exception\'');
        }catch(exception e){
            Util.emailSystemError(e);
        }
        test.stoptest();
    }
}