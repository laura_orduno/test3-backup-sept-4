/**
Vlocity
Version July 5 2016
Util class for HybridCPQ implements Vlocity Open Interface
*/

global without sharing class OCOM_HybridCPQUtil implements vlocity_cmt.VlocityOpenInterface {
    
    // create Order before
    global Boolean invokeMethod(String methodName, Map<String, Object> input, Map<String, Object> output, Map<String, Object> options)  {
        
        if (methodName.equals('createOcomOrder')) {
            createOCOMOrder(input,output,options);
        }
        
        return true;
    }
    
    // This method will create OCOM Specific Order
    private void createOcomOrder(Map<String, Object> input, Map<String, Object> output, Map<String, Object> options){
        try{
            Order newOcomOrder = new Order();
            String RcidAcctId;
            String ServiceAcctId;
            String MoveOutServiceAcctId;
            String ContactId;
            String OrderType;
            String oppId= (String)input.get('oppId'); 
            // RCID Id
            RcidAcctId = (String)input.get('ParentAccId');
            system.debug('inputmap::' + input);
            system.debug('ServiceAccountId' + (String)input.get('ServiceAcctId') + RcidAcctId);
            // Service Adderess
            ServiceAcctId = (String)input.get('ServiceAcctId');
            
            // Vlociyt: Oct 26 modifying for stamping Move-OUT 
            // Move Out Service Address
            MoveOutServiceAcctId = (String)input.get('MoveOutServiceAcctId');
            
            // Contact Id
            ContactId = (String)input.get('ContactId');
            //Order Type
            OrderType = (String)input.get('Type');
            
            if(!String.isempty(RcidAcctId)){
                if(!String.isEmpty(ServiceAcctId)){
                    // Assumption
                    // Service Account must have RCID Account as parent
                    Account servAcct = [select parentId from Account where Id = :ServiceAcctId ];
                    if(RcidAcctId == null || RcidAcctId == '')
                        RcidAcctId = servAcct.parentId;
                    //////////////////////////
                    //Assigning RCID Account AccountId
                    //////////////////////////
                    newOcomOrder.accountId = RcidAcctId;
                    
                    if(String.isNotBlank(oppId) && oppId!='null'){
                        newOcomOrder.vlocity_cmt__OpportunityId__c=oppId;
						newOcomOrder.opportunityid=oppId;
                    }
                    // Vlociyt: Oct 26 modifying for stamping Move-OUT 
                    Set<Id> acctIds = new Set<Id>();
                    if(String.isNotEmpty(ServiceAcctId)){
                        acctIds.add(ServiceAcctId);
                    }
                    if(String.isNotEmpty(MoveOutServiceAcctId) && OrderType == 'Move'){
                        acctIds.add(MoveOutServiceAcctId);
                    }
                    
                    List<SmbCare_Address__c> serAddress= [select Id,
                                                          FMS_Address_ID__c,
                                                          Suite_Number__c,
                                                          Address__c,
                                                          City__c,
                                                          Province__c,
                                                          Postal_Code__c,
                                                          Service_Account_Id__c,
                                                          Country__c
                                                          from Smbcare_address__c where Service_Account_Id__c in :acctIds];
                    
                    if(serAddress.size()>0){
                        //newOcomOrder.Service_Address_Text__c = 
                        //                                     (serAddress[0].Suite_Number__c == null ? '' : serAddress[0].Suite_Number__c) + ' ' + 
                        //                                     (serAddress[0].Address__c == null ? '' : serAddress[0].Address__c) + ' ' + 
                        //                                     (serAddress[0].City__c == null ? '' : serAddress[0].City__c) + ' ' +
                        //                                     (serAddress[0].Province__c == null ? '' : serAddress[0].Province__c) + ' ' + 
                        //                                     (serAddress[0].Postal_Code__c == null ? '' : serAddress[0].Postal_Code__c) + ' ' + 
                        //                                     (serAddress[0].Country__c == null ? '' : serAddress[0].Country__c);
                        
                        // newOcomOrder.FMS_Address_ID__c=serAddress[0].FMS_Address_ID__c;
                        // newOcomOrder.service_address__c=serAddress[0].Id;
                        // newOcomOrder.EffectiveDate = Date.today();
                        // newOcomOrder.Status = 'Not Submitted';
                        
                        // // service rep
                        // newOcomOrder.Sales_Representative__c = UserInfo.getUserId();
                        // // customer contact
                        // newOcomOrder.CustomerAuthorizedById= ContactId;
                        // newOcomOrder.Type = OrderType;
                        
                        for(SmbCare_Address__c smb :serAddress){ 
                            system.debug('*************!smb.Service_Account_Id__c = ' + smb.Service_Account_Id__c);
                            system.debug('*************!ServiceAcctId = ' + ServiceAcctId);
                            system.debug('*************!MoveOutServiceAcctId = ' + MoveOutServiceAcctId);
                            
                            if(smb.Service_Account_Id__c == ServiceAcctId){
                                newOcomOrder.Service_Address_Text__c = 
                                    (smb.Suite_Number__c == null ? '' : smb.Suite_Number__c) + ' ' + 
                                    (smb.Address__c == null ? '' : smb.Address__c) + ' ' + 
                                    (smb.City__c == null ? '' : smb.City__c) + ' ' +
                                    (smb.Province__c == null ? '' : smb.Province__c) + ' ' + 
                                    (smb.Postal_Code__c == null ? '' : smb.Postal_Code__c) + ' ' + 
                                    (smb.Country__c == null ? '' : smb.Country__c);
                                
                                newOcomOrder.FMS_Address_ID__c=smb.FMS_Address_ID__c;
                                newOcomOrder.service_address__c=smb.Id;
                                newOcomOrder.EffectiveDate = Date.today();
                                newOcomOrder.Status = 'Not Submitted';
                                
                                // service rep
                                newOcomOrder.Sales_Representative__c = UserInfo.getUserId();
                                // customer contact
                               if (ContactId != null && ContactId != '') {
                                    newOcomOrder.CustomerAuthorizedById= ContactId;
                                    newOcomOrder.CustomerSignedBy__c = ContactId;
                                }
                                newOcomOrder.Type = OrderType; 
                            }else if(smb.Service_Account_Id__c == MoveOutServiceAcctId){
                                newOcomOrder.MoveOutServiceAddress__c =smb.Id;
                                //newOcomOrder.CustomerSignedBy__c = ContactId;
                            }
                        }
                    }
                    
                }else
                {
                    // Throw CPQ exception
                }
            }
            
            insert newOcomOrder;
            // put to output
            Output.put('OrderId', newOcomOrder.Id);
        }
        
        catch (DMLException de){
            
        }
        
    }
    
}