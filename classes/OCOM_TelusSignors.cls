global with sharing class OCOM_TelusSignors implements vlocity_cmt.VlocityOpenInterface {

    public Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {

        if(methodName == 'getAuthorizedTELUSApprovers') {
           if(inputMap.Containskey('TelusSignor')){
                Map<String,Object> telusSignorMap = (Map<String,Object>) inputMap.get('TelusSignor');
                Decimal totalDealValue = (Decimal) telusSignorMap.get('totalDealValue');
                 String searchString = (String) telusSignorMap.get('searchString');
                List<User> authorizedApprovers = getAuthorizedTELUSApprovers(totalDealValue,searchString );
                if (outMap == null) outMap = new Map<String, Object>();
                    outMap.put('AuthorizedApprovers', authorizedApprovers);
            }
        } else {
            return false;
        }

        return true;
    }

    private List<User> getAuthorizedTELUSApprovers(Decimal totalDealValue,String searchString ) {
        List<Approval_Threshold__c> approvalThreshold = new List<Approval_Threshold__c>();

        approvalThreshold = [
        select Release_Code_0_Threshold__c,Release_Code_1_Threshold__c,Release_Code_2_Threshold__c,Release_Code_3_Threshold__c,Release_Code_4_Threshold__c,Release_Code_5_Threshold__c,Release_Code_6_Threshold__c,Release_Code_7_Threshold__c,Release_Code_8_Threshold__c,Release_Code_9_Threshold__c,Release_Code_10_Threshold__c
        from Approval_Threshold__c
        where RecordType.Name = 'Release Code Threshold'
        and Transaction_Type__c = 'Contract' limit 1
        ];

        Integer releaseCode = 0;
        for (Integer i=0; i<=10; i++) {
            Decimal threshold = (Decimal) approvalThreshold[0].get('Release_Code_' + i + '_Threshold__c');
            if (threshold >= totalDealValue) {
                releaseCode = i;
                break;
            }
        }
          
          //Modified by Jaya to add isActive = true filter
          String soql = ' select name, release_code__c, email, phone, title, isActive, lastmodifieddate from user Where release_code__c != null and isActive = true';
        if(searchString != '' && searchString != null){
          soql = soql + ' and name LIKE \'%' + searchString +'%\'' + ' order by Name limit 500';
        }
        else{
            soql = soql  + ' order by Name limit 5';
        }      
        List<User> users = database.query(soql); 


       /* List<User> users = [
                select name, release_code__c, email, phone, title, isActive, lastmodifieddate
                from user 
                where release_code__c != null Limit 5
        ];*/

        for (Integer j = 0; j < users.size(); j++) {
            if (Integer.valueOf(users[j].release_code__c) < releaseCode) {
                users.remove(j);
            }
        }

        return users;
    }
}