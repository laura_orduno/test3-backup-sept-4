@isTest
private class trac_PRM_statsTests {
	  private static User tUser;
    private static Boolean maketUserManager;
	
	@isTest(SeeAllData=true)
    static void setupTest() {
        Account a = new Account(Name = 'PRM Partner Account', Phone = '(604) 996-9449');
        insert a;
        
        Contact c = new Contact(AccountId = a.id,
                                FirstName = 'unit',
                                Lastname = 'tester',
                                email = 'not@example.com',
                                phone = '(604) 996-9449',
                                title = 'Tester',
                                title_category__c = 'IT Dept',
                                language_preference__c = 'English',
                                Department_Category__c = 'IT',
                                Role_c__c = 'Purchaser',
                                MailingStreet = '1234 Street',
                                MailingCity = 'Vancouver',
                                MailingState = 'AB',
                                MailingPostalCode = 'A1A 1A1');
        insert c;
    
        try{
            Profile p;
            if (maketUserManager != null && maketUserManager == true) {
                p = [select id from profile where name='Telus Partner Manager' or name='Telus Partner User 2' limit 1];
            } else {
                p = [select id from profile where name='Telus Partner User 2' limit 1];
            }
            tUser = new User(alias = 'prmparnt', phone='(604) 996-9449', email='prmpartnertest@example.com', emailencodingkey='UTF-8', lastname='Partner', languagelocalekey='en_US', localesidkey='en_US', profileid = p.Id, contactid = c.id, timezonesidkey='America/Los_Angeles', username=c.id+'prmpartnertest@example.com');
            insert tUser;
        }catch(queryexception qc){
            system.assert(false,qc.getmessage());
            // Unable to setup user.  Need to review this test class and all scenarios.
        }
    }
    @isTest(SeeAllData=true)
    static void testIsManager() {
        maketUserManager = true;
        setupTest();
        System.runAs(tUser) {
            trac_PRM_stats tps = new trac_PRM_stats();
            //System.assert(tps.getIsManager() == true, 'This user should be a manager.');
        }
        maketUserManager = false;
        setupTest();
        System.runAs(tUser) {
            trac_PRM_stats tps = new trac_PRM_stats();
            //System.assert(tps.getIsManager() == false, 'This user should not be a manager.');
        }
    }
    @isTest(SeeAllData=true)
    static void testRecentLoginChart() {
        maketUserManager = true;
        setupTest();
        
        //System.setPassword(tUser.id, '901pass0908');
        /* Integer i = 0;
        while (i < 10) {
            Site.login(tUser.username, '901pass0908', '');
            i++;
        } */
        
        System.runAs(tUser) {
            trac_PRM_stats tps = new trac_PRM_stats();
            // System.assertEquals(10, tps.getRecentLoginsCount());
            tps.getRecentLoginsCount();
            // System.assertEquals(10, tps.getRecentLoginsChart());
            tps.getRecentLoginsChart();
        }
    }
    @isTest(SeeAllData=true)
    static void testOpenProspectsCount() {
        maketUserManager = true;
        setupTest();
        System.runAs(tUser) {
            RecordType rt = [Select Id From RecordType Where Name = 'Campaign Record Type' and SobjectType = 'Lead'];
            Lead l = new Lead(Phone = '6041001010', PostalCode = 'V4N 0Z5', LastName = 'Test1', Company = 'PRMdash Co', Status = 'Open - Unread', RecordTypeId = rt.Id);
            insert l; l.OwnerId = UserInfo.getUserId(); update l;
            rt = [Select Id From RecordType Where Name = 'Cluster Record Type' and SobjectType = 'Lead'];
            l = new Lead(Phone = '6041001010', PostalCode = 'V4N 0Z5', LastName = 'Test2', Company = 'PRMdash Co', Status = 'Open - Uncontacted', RecordTypeId = rt.Id);
            insert l; l.OwnerId = UserInfo.getUserId(); update l;
            trac_PRM_stats tps = new trac_PRM_stats();
            //System.assertEquals(2, tps.getOpenProspectsCount());
        }
    }
    
    @isTest(SeeAllData=true)
    static void testNewlyCreatedLeadsCount() {
        maketUserManager = true;
        setupTest();
        System.runAs(tUser) {
            RecordType rt = [Select Id From RecordType Where Name = 'Campaign Record Type' and SobjectType = 'Lead'];
            Lead l = new Lead(Phone = '6041001010', PostalCode = 'V4N 0Z5', LastName = 'Test1', Company = 'PRMdash Co', Status = 'Open - Unread', RecordTypeId = rt.Id);
            insert l; l.OwnerId = UserInfo.getUserId(); update l;
            rt = [Select Id From RecordType Where Name = 'Cluster Record Type' and SobjectType = 'Lead'];
            l = new Lead(Phone = '6041001010', PostalCode = 'V4N 0Z5', LastName = 'Test2', Company = 'PRMdash Co', Status = 'Open - Uncontacted', RecordTypeId = rt.Id);
            insert l; l.OwnerId = UserInfo.getUserId(); update l;
            trac_PRM_stats tps = new trac_PRM_stats();
            //System.assertEquals(2, tps.getNewlyCreatedLeadsCount());
        }
    }
    
    @isTest(SeeAllData=true)
    static void testOpenRenewalCount() {
        maketUserManager = true;
        setupTest();
        System.runAs(tUser) {
            RecordType rt = [Select Id From RecordType Where Name = 'Campaign Record Type' and SobjectType = 'Lead'];
            Lead l = new Lead(Phone = '6041001010', PostalCode = 'V4N 0Z5', LastName = 'Test1', Company = 'PRMdash Co', Status = 'Open - Unread', RecordTypeId = rt.Id);
            insert l; l.OwnerId = UserInfo.getUserId(); update l;
            rt = [Select Id From RecordType Where Name = 'Renewal Record Type' and SobjectType = 'Lead'];
            l = new Lead(Phone = '6041001010', PostalCode = 'V4N 0Z5', LastName = 'Test2', Company = 'PRMdash Co', Status = 'Open - Uncontacted', RecordTypeId = rt.Id);
            insert l; l.OwnerId = UserInfo.getUserId(); update l;
            trac_PRM_stats tps = new trac_PRM_stats();
            //System.assertEquals(1, tps.getOpenRenewalCount());
        }
    }
    
    @isTest(SeeAllData=true)
    static void testRecentlyClosedOppsChart() {
        maketUserManager = true;
        setupTest();
        System.runAs(tUser) {
            RecordType rt = [Select Id From RecordType Where Name = 'Campaign Record Type' and SobjectType = 'Lead'];
            Lead l = new Lead(Phone = '6041001010', PostalCode = 'V4N 0Z5', LastName = 'Test1', Company = 'PRMdash Co', Status = 'Open - Unread', RecordTypeId = rt.Id);
            insert l; l.OwnerId = UserInfo.getUserId(); update l;
            rt = [Select Id From RecordType Where Name = 'Renewal Record Type' and SobjectType = 'Lead'];
            l = new Lead(Phone = '6041001010', PostalCode = 'V4N 0Z5', LastName = 'Test2', Company = 'PRMdash Co', Status = 'Open - Uncontacted', RecordTypeId = rt.Id);
            insert l; l.OwnerId = UserInfo.getUserId(); update l;
            trac_PRM_stats tps = new trac_PRM_stats();
            String expectedResult = '[[\'Status\', \'Count\'], [\'Closed Lost\', 0], [\'Closed Won\', 0]]';
            //System.assertEquals(expectedResult, tps.getRecentlyClosedOppsChart());
            //System.assertEquals(false, tps.getHasRecentlyClosedOpps());
        }
    }
    @isTest(SeeAllData=true)
    static void testLeadPipeline() {
        maketUserManager = true;
        setupTest();
        System.runAs(tUser) {
            RecordType rt = [Select Id From RecordType Where Name = 'Campaign Record Type' and SobjectType = 'Lead'];
            Lead l = new Lead(Phone = '6041001010', PostalCode = 'V4N 0Z5', LastName = 'Test1', Company = 'PRMdash Co', Status = 'Open - Unread', RecordTypeId = rt.Id);
            insert l; l.OwnerId = UserInfo.getUserId(); update l;
            
            
            Database.LeadConvert lc = new Database.LeadConvert();
            lc.setLeadId(l.id);
            LeadStatus convertStatus = [SELECT Id, MasterLabel
                                        FROM LeadStatus
                                        WHERE IsConverted = true
                                        LIMIT 1];
            lc.setConvertedStatus(convertStatus.MasterLabel);
            Database.LeadConvertResult lcr = Database.convertLead(lc);
            //System.assert(lcr.isSuccess());
            Account a = [SELECT name FROM Account WHERE Id = :lcr.getAccountId()];
            Contact c = [SELECT name FROM Contact WHERE Id = :lcr.getContactId()];
            Opportunity o = [SELECT name FROM Opportunity WHERE Id = :lcr.getOpportunityId()];
            
            
            trac_PRM_stats tps = new trac_PRM_stats();
            //System.assertEquals(1, tps.getLeadPipelineReceivedCount());
            //System.assertEquals(1, tps.getLeadPipelineConvertedCount());
            tps.getLeadPipelineConvertedRatio();
            //System.assertEquals(0, tps.getLeadPipelineWonCount());
            tps.getLeadPipelineWonRatio();
        }
    }
    @isTest(SeeAllData=true)
    static void testLeadPipelineNoReceived() {
        maketUserManager = true;
        setupTest();
        System.runAs(tUser) {
            trac_PRM_stats tps = new trac_PRM_stats();
            //System.assertEquals(0, tps.getLeadPipelineReceivedCount());
        }
    }
    @isTest(SeeAllData=true)
    static void testLeadPipelineNoConverted() {
        maketUserManager = true;
        setupTest();
        System.runAs(tUser) {
            RecordType rt = [Select Id From RecordType Where Name = 'Campaign Record Type' and SobjectType = 'Lead'];
            Lead l = new Lead(Phone = '6041001010', PostalCode = 'V4N 0Z5', LastName = 'Test1', Company = 'PRMdash Co', Status = 'Open - Unread', RecordTypeId = rt.Id);
            insert l; l.OwnerId = UserInfo.getUserId(); update l;
            
            trac_PRM_stats tps = new trac_PRM_stats();
            //System.assertEquals(1, tps.getLeadPipelineReceivedCount());
            //System.assertEquals(0, tps.getLeadPipelineConvertedCount());
        }
    }
    @isTest(SeeAllData=true)
    static void testOppPipeline() {
        maketUserManager = true;
        setupTest();
        System.runAs(tUser) {
            RecordType rt = [Select Id From RecordType Where Name = 'Campaign Record Type' and SobjectType = 'Lead'];
            Lead l = new Lead(Phone = '6041001010', PostalCode = 'V4N 0Z5', LastName = 'Test1', Company = 'PRMdash Co', Status = 'Open - Unread', RecordTypeId = rt.Id);
            insert l; l.OwnerId = UserInfo.getUserId(); update l;
            
            Database.LeadConvert lc = new Database.LeadConvert();
            lc.setLeadId(l.id);
            LeadStatus convertStatus = [SELECT Id, MasterLabel
                                        FROM LeadStatus
                                        WHERE IsConverted = true
                                        LIMIT 1];
            lc.setConvertedStatus(convertStatus.MasterLabel);
            Database.LeadConvertResult lcr = Database.convertLead(lc);
            //System.assert(lcr.isSuccess());
            Account a = [SELECT name FROM Account WHERE Id = :lcr.getAccountId()];
            Contact c = [SELECT name FROM Contact WHERE Id = :lcr.getContactId()];
            Opportunity o = [SELECT name FROM Opportunity WHERE Id = :lcr.getOpportunityId()];
            
            trac_PRM_stats tps = new trac_PRM_stats();
            //System.assertEquals(1, tps.getOppPipelinePotentialCount());
            tps.getOppPipelinePotentialRatio();
            //System.assertEquals(0, tps.getOppPipelineQualifiedCount());
            tps.getOppPipelineQualifiedRatio();
            //System.assertEquals(0, tps.getOppPipelineCompetingCount());
            tps.getOppPipelineCompetingRatio();
            //System.assertEquals(0, tps.getOppPipelineClosingCount());
            tps.getOppPipelineClosingRatio();
            //System.assertEquals(0, tps.getOppPipelineWonCount());
            tps.getOppPipelineWonRatio();
        }
    }
}