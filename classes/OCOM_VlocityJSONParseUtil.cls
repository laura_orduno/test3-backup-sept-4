//Created by Aditya/Santosh (IBM) 
//This class will provide utility method to parse Vlocity JSON.
public class OCOM_VlocityJSONParseUtil {
     
    public final static string attributeFieldNameInJSON = 'attributedisplayname__c'; 
    public final static string isHiddenAttributeInJSON = 'ishidden__c';
    public final static string isRequiredAttributeInJSON = 'isrequired__c';
    public final static string isReadOnlyAttributeInJSON = 'isreadonly__c';
    public final static string attributeValueFieldNameInJSON = 'value__c'; 
    public final static string runTimeInfoValueFieldNameInJSON = 'value';  
    public final static string attributeRunTimeINfoMapNameInJSON = 'attributeRunTimeInfo';  
    public final static string runTimeInfoDefaultFieldNameInJSON = 'default'; 
    public final static string DefaultDisplayTextFieldNameInJSON = 'displayText'; 
    public final static string selectedItemMapNameInJSON = 'selectedItem'; 
    public final static string selectedItemDisplayTextFieldNameInJSON = 'displayText'; 
    public final static string selectedItemDisplayTextFieldValueInJSON = 'value'; 
    public final static string selectedItemNCIdJSON = 'id'; 
    public final static string selectedItemValueFieldNameInJSON = 'value';     
    public final static string uiDisplayTypeFieldNameInJSON     = 'uiDisplayType';
    public final static string dataTypeFieldNameInJSON     = 'dataType';                
    public final static string picklistDataType     = 'Picklist';
    public final static string checkboxDataType     = 'Checkbox';
    public final static string dropdownDataType     = 'Dropdown';
    public final static string textDataType     = 'Text';
    public final static string numberDataType     = 'Number';
    public final static string radiobuttonDataType     = 'Radiobutton';
    public final static string dateDataType     = 'Date';
    public final static string attributeuniquecodeInJSON  = 'attributeuniquecode__c';
    
  // This Method will return Map of all attributes in Map.  
    public static Map<String,JSONWrapper> getAttributesFromJSON(String JSONString){
        Map<String,JSONWrapper>  JSONAttrMap = new  Map<String,JSONWrapper>();
        if(String.isNotBlank(JSONString)){
            Map<String, Object> attributes =  new Map<String, Object>();
            try{
                attributes = (Map<String, Object>) JSON.deserializeUntyped(JSONString);
            }catch(exception ex){
               // throw ex;
                System.debug('Exception occurred while parsing the json '+ex.getMessage());
                return JSONAttrMap;
            }
            for (String key : attributes.keySet()) {
                List<Object> JSONAttriList = (List<Object>)attributes.get(key);
                for (Object attribute : JSONAttriList) {
                    JSONWrapper JSONWrap = new JSONWrapper();
                    Map<String, Object> attributeMap = (Map<String, Object>) attribute;
                    String attributeName = (String) attributeMap.get(attributeFieldNameInJSON); 
                    if(String.isBlank(attributeName)){
                        continue;
                    }
                    //Populate if an Attribute is hidden
                    if(null !=attributeMap.get(isHiddenAttributeInJSON)){
                        JSONWrap.isHiddenOnUI = (boolean) attributeMap.get(isHiddenAttributeInJSON);
                    }
                    //Populate if an Attribute is ready only
                    if(null !=attributeMap.get(isReadOnlyAttributeInJSON)){
                        JSONWrap.isReadOnly = (boolean) attributeMap.get(isReadOnlyAttributeInJSON);
                    }
                    //Populate if an Attribute is required
                    if(null !=attributeMap.get(isRequiredAttributeInJSON)){
                        JSONWrap.isRequired = (boolean) attributeMap.get(isRequiredAttributeInJSON);
                    }
                    //Populate if an Attribute is required
                    if(null !=attributeMap.get(attributeuniquecodeInJSON)){
                        JSONWrap.attributeuniquecode = (String) attributeMap.get(attributeuniquecodeInJSON);
                    }
                    //.....................................................................//
                    Object attrRunTimeInfoMap=attributeMap.get(attributeRunTimeINfoMapNameInJSON); String charactersticsValue; String runTimeInfoCharactersticValue;
                    if(attrRunTimeInfoMap instanceof Map<String,Object>){
                        Map<String,Object> attrRunTimeInfoKeyValMap=(Map<String,Object>)attrRunTimeInfoMap;
                        String dataType=String.valueOf(attrRunTimeInfoKeyValMap.get(dataTypeFieldNameInJSON));   // string value is : 'dataType'
                        if(dataType!=null){
                            if(dataType.equals(checkboxDataType)){ // string value is : 'Checkbox'
                                charactersticsValue=null;
                                String defaultVal=String.valueOf(attrRunTimeInfoKeyValMap.get(runTimeInfoDefaultFieldNameInJSON)); // string value is : 'default'
                                if(defaultVal!=null && defaultVal.equals('true')){
                                    charactersticsValue='Selected';
                                } else{
                                    String selectedOrNot=String.valueOf(attributeMap.get(attributeValueFieldNameInJSON)); // string value is : 'value__c'
                                    if(selectedOrNot!=null && selectedOrNot.equals('Selected')){
                                        charactersticsValue='Selected';
                                    }
                                }
                            }
                            if(dataType.equals(picklistDataType)){ // string value is : 'Picklist'
                                String uiDisplayTypeVal=String.valueOf(attrRunTimeInfoKeyValMap.get(uiDisplayTypeFieldNameInJSON)); // string value is : 'uiDisplayType'
                                if(uiDisplayTypeVal!=null){
                                    if(uiDisplayTypeVal.equals(dropdownDataType)){ // string value is : 'Dropdown'
                                        charactersticsValue=null;
                                        Object selectedItemObj=attrRunTimeInfoKeyValMap.get(selectedItemMapNameInJSON); // string value is : 'selectedItem'
                                        if(selectedItemObj!=null && selectedItemObj instanceof Map<String,Object>){
                                            Map<String,Object> selectedItemObjMap=(Map<String,Object>)selectedItemObj;
                                            Map<String,Object> selectedItemMap=(Map<String,Object>)selectedItemObj;
                                            charactersticsValue = String.valueOf(selectedItemMap.get(selectedItemDisplayTextFieldValueInJSON));// String value is 'value'
                                            if(String.isBlank(charactersticsValue)){
                                                charactersticsValue = String.valueOf(selectedItemMap.get(selectedItemDisplayTextFieldNameInJSON)); // string value is : 'displayText'
                                            }
                                            
                                            JSONWrap.selectedPicklistValueNCId =  String.valueOf(selectedItemMap.get(selectedItemNCIdJSON)); // string value is : 'id'
                                        }
                                        if(charactersticsValue==null || charactersticsValue.equals('null') || String.isEmpty(charactersticsValue)){
                                            charactersticsValue=null;
                                            List<Object> defaultValList=(List<Object>)attrRunTimeInfoKeyValMap.get(runTimeInfoDefaultFieldNameInJSON); // string value is : 'default'
                                            for(Object defaultValMapObj : defaultValList){
                                                Map<String,Object> defaultValMap = (Map<String,Object> ) defaultValMapObj;
                                                charactersticsValue = String.valueOf(defaultValMap.get(DefaultDisplayTextFieldNameInJSON));  // string value is : 'displayText'
                                                JSONWrap.selectedPicklistValueNCId =  String.valueOf(defaultValMap.get(selectedItemNCIdJSON)); // string value is : 'id'
                                            }
                                        }
                                        //Fix for ETL gap to set default value for a picklist added by Aditya Jamwal 
                                        if(charactersticsValue==null || charactersticsValue.equals('null') || String.isEmpty(charactersticsValue)){
                                            charactersticsValue=null;
                                            charactersticsValue= String.valueOf(attributeMap.get(attributeValueFieldNameInJSON)); // string value is : 'value__c'
                                        }
                                    }
                                    if(uiDisplayTypeVal.equals(radiobuttonDataType)){ // string value is : 'Radiobutton'
                                        charactersticsValue=null;
                                        List<Object> defaultValList=(List<Object>)attrRunTimeInfoKeyValMap.get(runTimeInfoDefaultFieldNameInJSON); // string value is : 'default'
                                        for(Object defaultValMapObj : JSONAttriList){
                                            Map<String,Object> defaultValMap = (Map<String,Object> ) defaultValMapObj;
                                            charactersticsValue=String.valueOf(defaultValMap.get(DefaultDisplayTextFieldNameInJSON));  // string value is : 'displayText'
                                        }
                                    }
                                }
                            }
                            if(dataType.equals(textDataType)){ // string value is : 'Text'
                                
                                charactersticsValue= String.valueOf(attrRunTimeInfoKeyValMap.get(runTimeInfoValueFieldNameInJSON)); // string value is : 'value'
                                if(String.isBlank(charactersticsValue)){
                                    charactersticsValue= String.valueOf(attributeMap.get(attributeValueFieldNameInJSON)); // string value is : 'value'
                                }
                                if(String.isBlank(charactersticsValue)){                               
                                    charactersticsValue=String.valueOf(attrRunTimeInfoKeyValMap.get(runTimeInfoDefaultFieldNameInJSON)); // string value is : 'default'
                                }
                                
                                runTimeInfoCharactersticValue = String.valueOf(attrRunTimeInfoKeyValMap.get(runTimeInfoValueFieldNameInJSON));
                                
                            }
                            if(dataType.equals(dateDataType)){ // string value is : 'Date'
                                charactersticsValue= String.valueOf(attributeMap.get(attributeValueFieldNameInJSON)); // string value is : 'value__c'
                                if(String.isBlank(charactersticsValue)){                               
                                    charactersticsValue=String.valueOf(attrRunTimeInfoKeyValMap.get(runTimeInfoDefaultFieldNameInJSON)); // string value is : 'default'
                                }
                                if(String.isBlank(charactersticsValue)){
                                    charactersticsValue=String.valueOf(attrRunTimeInfoKeyValMap.get(runTimeInfoValueFieldNameInJSON)); // string value is : 'value'
                                } 
                                 runTimeInfoCharactersticValue = String.valueOf(attrRunTimeInfoKeyValMap.get(runTimeInfoValueFieldNameInJSON));
                            }
                            if(dataType.equals(numberDataType)){ // string value is : 'Number'
                                charactersticsValue= String.valueOf(attributeMap.get(attributeValueFieldNameInJSON)); // string value is : 'value__c'
                                if(String.isBlank(charactersticsValue)){                               
                                    charactersticsValue=String.valueOf(attrRunTimeInfoKeyValMap.get(runTimeInfoDefaultFieldNameInJSON)); // string value is : 'default'
                                }
                                if(String.isBlank(charactersticsValue)){
                                    charactersticsValue=String.valueOf(attrRunTimeInfoKeyValMap.get(runTimeInfoValueFieldNameInJSON));  // string value is : 'value'
                                }
                                 runTimeInfoCharactersticValue = String.valueOf(attrRunTimeInfoKeyValMap.get(runTimeInfoValueFieldNameInJSON));
                            }
                        }
                        
                        JSONWrap.attributeName = attributeName; JSONWrap.value = charactersticsValue; JSONWrap.runTimeInfoValue = runTimeInfoCharactersticValue;
                        JSONWrap.dataType = dataType ;
                        JSONAttrMap.put(JSONWrap.attributeName,JSONWrap);    
                    }
                    //.....................................................................//
                }
            }
        }        
        return JSONAttrMap;        
    }    
    
    // This Method will return Updated JSON String with update values passed in attributes Map as input.  
    public static String setAttributesInJSON(String JSONString, Map<String,String> AttributeWithValueMap){
       Map<String, Object> attributes =  new Map<String, Object>();
        if(String.isNotBlank(JSONString) && null != AttributeWithValueMap && AttributeWithValueMap.size() > 0){            
            try{
                attributes = (Map<String, Object>) JSON.deserializeUntyped(JSONString);
            }catch(exception ex){
                throw ex;
                return null;
            }
            
            for (String key : attributes.keySet()) {
                List<Object> JSONAttriList = (List<Object>)attributes.get(key);
                for (Object attribute : JSONAttriList) {
                    JSONWrapper JSONWrap = new JSONWrapper();
                    Map<String, Object> attributeMap = (Map<String, Object>) attribute;                    
                    String attributeName = (String) attributeMap.get(attributeFieldNameInJSON);
                    if(null != AttributeWithValueMap.get(attributeName)){
                         String charactersticsValue = AttributeWithValueMap.get(attributeName);
                    //.....................................................................//
                    Object attrRunTimeInfoMap=attributeMap.get(attributeRunTimeINfoMapNameInJSON); 
                    if(attrRunTimeInfoMap instanceof Map<String,Object>){
                        Map<String,Object> attrRunTimeInfoKeyValMap=(Map<String,Object>)attrRunTimeInfoMap;
                        String dataType=String.valueOf(attrRunTimeInfoKeyValMap.get(dataTypeFieldNameInJSON));   // string value is : 'dataType'
                        if(dataType!=null){
                            if(dataType.equals(checkboxDataType)){ // string value is : 'Checkbox'
                                if(String.isNotBlank(charactersticsValue) && charactersticsValue == 'Selected') {
                                if(null != attrRunTimeInfoKeyValMap.containsKey(runTimeInfoDefaultFieldNameInJSON)){
                                    attrRunTimeInfoKeyValMap.put(runTimeInfoDefaultFieldNameInJSON,'true');
                                }
                                if(null != attributeMap.get(attributeValueFieldNameInJSON) ){
                                    attributeMap.put(attributeValueFieldNameInJSON,'true');
                                }
                             
                            }
                            }
                            if(dataType.equals(picklistDataType)){  // string value is : 'Picklist'
                                String uiDisplayTypeVal=String.valueOf(attrRunTimeInfoKeyValMap.get(uiDisplayTypeFieldNameInJSON)); // string value is : 'uiDisplayType'
                                if(uiDisplayTypeVal!=null){
                                    if(uiDisplayTypeVal.equals(dropdownDataType)){  // string value is : 'Dropdown'
                                         system.debug('!!picklist value 0');
                                        List<Object> valuesObjList = (List<Object>) attrRunTimeInfoKeyValMap.get('values'); 
                                        if(null != valuesObjList && valuesObjList.size() > 0){
                                            system.debug('!!picklist value 1');
                                            for(Object valuesObjMap : valuesObjList){
                                                system.debug('!!picklist value 3');
                                                Map<String,Object> PickListValueMap = (Map<String,Object> ) valuesObjMap;
                                                string displayText = String.valueOf(PickListValueMap.get(selectedItemDisplayTextFieldValueInJSON));
                                                if(String.isBlank(displayText)){
                                                    displayText = String.valueOf(PickListValueMap.get(selectedItemDisplayTextFieldNameInJSON));  // string value is : 'displayText'
                                                }
                                                 
                                                if(String.isNotBlank(displayText) && displayText.equalsIgnoreCase(charactersticsValue)){
                                                    attrRunTimeInfoKeyValMap.put(selectedItemMapNameInJSON,PickListValueMap);  // string value is : 'selectedItem'
                                                    system.debug('!!picklist value '+charactersticsValue + ' '+displayText);
                                                }
                                            }                                                                                           
                                        }
                                        attributeMap.put(attributeRunTimeINfoMapNameInJSON,attrRunTimeInfoKeyValMap);                                        
                                        if(attributeMap.containsKey(attributeValueFieldNameInJSON)){
                                            attributeMap.put(attributeValueFieldNameInJSON,charactersticsValue);  
                                            system.debug('!!!charactersticsValue text 0 '+ charactersticsValue);  
                                        }
                                        
                                    }
                                    if(uiDisplayTypeVal.equals(radiobuttonDataType)){ // string value is : 'Radiobutton'
                                        List<Object> defaultValList=(List<Object>)attrRunTimeInfoKeyValMap.get(runTimeInfoDefaultFieldNameInJSON); // string value is : 'default'
                                        for(Object defaultValMapObj : defaultValList){ //need to ask santosh
                                            Map<String,Object> defaultValMap = (Map<String,Object> ) defaultValMapObj;
                                            if( null != defaultValMap.get(DefaultDisplayTextFieldNameInJSON)) {   // string value is : 'displayText'
                                                defaultValMap.put(DefaultDisplayTextFieldNameInJSON,charactersticsValue);
                                            }
                                        }
                                        attrRunTimeInfoKeyValMap.put(runTimeInfoDefaultFieldNameInJSON,defaultValList);
                                        attributeMap.put(attributeRunTimeINfoMapNameInJSON,attrRunTimeInfoKeyValMap);
                                        
                                    }
                                }
                            }
                            if(dataType.equals(textDataType)){ // string value is : 'Text'
                               if(attributeMap.containsKey(attributeValueFieldNameInJSON)){
                                  attributeMap.put(attributeValueFieldNameInJSON,charactersticsValue);  
                                  system.debug('!!!charactersticsValue text 0 '+ charactersticsValue);  
                                }
                                if(attrRunTimeInfoKeyValMap.containsKey(runTimeInfoDefaultFieldNameInJSON)){    
                                    attrRunTimeInfoKeyValMap.put(runTimeInfoDefaultFieldNameInJSON,charactersticsValue); // string value is : 'default'
                                    attributeMap.put(attributeRunTimeINfoMapNameInJSON,attrRunTimeInfoKeyValMap); 
                                                                        
                                }
                                system.debug('!!!charactersticsValue text 1 0 '+ attrRunTimeInfoKeyValMap.get(runTimeInfoDefaultFieldNameInJSON));  
                                system.debug('!!!charactersticsValue text 1 1 '+ attrRunTimeInfoKeyValMap.containsKey(runTimeInfoDefaultFieldNameInJSON));  
                                if(attrRunTimeInfoKeyValMap.containsKey(runTimeInfoValueFieldNameInJSON)){
                                    attrRunTimeInfoKeyValMap.put(runTimeInfoValueFieldNameInJSON,charactersticsValue);  // string value is : 'value'
                                    attributeMap.put(attributeRunTimeINfoMapNameInJSON,attrRunTimeInfoKeyValMap);  
                                    system.debug('!!!charactersticsValue text 2 '+ charactersticsValue);  
                                }
                                 system.debug('!!!charactersticsValue text '+ charactersticsValue);  
                            }
                            if(dataType.equals(dateDataType)){ // string value is : 'Date'
                                if(attributeMap.containsKey(attributeValueFieldNameInJSON)){
                                  attributeMap.put(attributeValueFieldNameInJSON,charactersticsValue);  
                                }
                                if(attrRunTimeInfoKeyValMap.containsKey(runTimeInfoDefaultFieldNameInJSON)){                               
                                    attrRunTimeInfoKeyValMap.put(runTimeInfoDefaultFieldNameInJSON,charactersticsValue); // string value is : 'default'
                                   attributeMap.put(attributeRunTimeINfoMapNameInJSON,attrRunTimeInfoKeyValMap); 
                                }
                                if(attrRunTimeInfoKeyValMap.containsKey(runTimeInfoValueFieldNameInJSON)){
                                    attrRunTimeInfoKeyValMap.put(runTimeInfoValueFieldNameInJSON,charactersticsValue);  // string value is : 'value'
                                    attributeMap.put(attributeRunTimeINfoMapNameInJSON,attrRunTimeInfoKeyValMap); 
                                } 
                            }
                            if(dataType.equals(numberDataType)){ // string value is : 'Number'
                                if(attributeMap.containsKey(attributeValueFieldNameInJSON)){
                                  attributeMap.put(attributeValueFieldNameInJSON,charactersticsValue);  
                                }
                                if(attrRunTimeInfoKeyValMap.containsKey(runTimeInfoDefaultFieldNameInJSON)){                               
                                   attrRunTimeInfoKeyValMap.put(runTimeInfoDefaultFieldNameInJSON,charactersticsValue); // string value is : 'default'
                                   attributeMap.put(attributeRunTimeINfoMapNameInJSON,attrRunTimeInfoKeyValMap); 
                                }
                                if(attrRunTimeInfoKeyValMap.containsKey(runTimeInfoValueFieldNameInJSON)){
                                    attrRunTimeInfoKeyValMap.put(runTimeInfoValueFieldNameInJSON,charactersticsValue);  // string value is : 'value'
                                    attributeMap.put(attributeRunTimeINfoMapNameInJSON,attrRunTimeInfoKeyValMap); 
                                }
                            }
                        }                        
                        system.debug('!!!charactersticsValue '+charactersticsValue);  
                    }
                    //.....................................................................//
                  }
                }
            }
        }
        String JSONStr;
        if(null!=attributes && attributes.size()>0){
            try{
                JSONStr = jsonCreator(attributes);
              //JSONStr = JSON.serialize(attributes);
            }catch(exception ex){
                throw ex;
                return null;
            }
         
        }
        return JSONStr;        
    }    

    
    
    public class JSONWrapper{
        public String attributeName {get;set;}
        public String value {get;set;}
        public String selectedPicklistValueNCId {get;set;}
        public String dataType {get;set;}
        public boolean isHiddenOnUI {get;set;}
        public boolean isRequired {get;set;}
        public boolean isReadOnly {get;set;}
        public String attributeuniquecode {get;set;}
        public String runTimeInfoValue {get;set;}
        
        public JSONWrapper(String attributeName,String value,String dataType){
            this.attributeName = attributeName;
            this.value = value;
            this.dataType = dataType;
        }   
        public JSONWrapper(){          
            
        }
    } 
    
      public static String jsonCreator(Map < String, Object > jsonMap) {
        JSONGenerator jsonGenerator = JSON.createGenerator(false);
        jsonGenerator.writeStartObject();
        //Iterate through the Categories and construct JSON for each Attribute in the Category
        for (String categoryCode: jsonMap.keySet()) {
            jsonGenerator.writeFieldName(categoryCode);
            jsonGenerator.writeStartArray();
            List < Object > attributeList = (List < Object > ) jsonMap.get(categoryCode);
            for (Object attribute: attributeList) {
                jsonGenerator.writeStartObject();
                jsonGenerator.writeNullField('$$AttributeDefinitionStart$$');
                Map < String, Object > attributeAssignmentMap = (Map < String, Object > ) attribute;
                for (String objField: attributeAssignmentMap.keySet()) {
                    if (!(objField.equals('$$AttributeDefinitionStart$$') || objField.equals('$$AttributeDefinitionEnd$$'))) {
                        if (attributeAssignmentMap.get(objField) == null) {
                            jsonGenerator.writeNullField(objField);

                        } else {
                            jsonGenerator.writeObjectField(objField, attributeAssignmentMap.get(objField));

                        }

                    }

                }

                jsonGenerator.writeNullField('$$AttributeDefinitionEnd$$');
                jsonGenerator.writeEndObject();

            }

            jsonGenerator.writeEndArray();

        }

        jsonGenerator.writeEndObject();
        return jsonGenerator.getAsString();

    }

  
}