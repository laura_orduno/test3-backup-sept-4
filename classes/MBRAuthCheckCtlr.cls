/**
 *  MBRAuthCheckCtlr.cls
 *
 *  @description
 *		Class used to do authentication check and redirect in the My Business Requests Community.  Copied, not created by Dan
 *
 *	@date - 03/01/2014
 *	@author - Dan Reich: Traction on Demand, Christian Wico: Traction on Demand, Alex Kong: Traction on Demand
 */
public virtual with sharing class MBRAuthCheckCtlr {
  
    public String UserLangLocale {get; set;}
   
    public PageReference initCheck() {
        if (Userinfo.getUserType().equals('Guest')) {
            String startUrl = ApexPages.currentPage().getUrl();
            PageReference pageRef = Page.MBRLogin;
            pageRef.getParameters().put('startUrl', startUrl.replace('/apex/',''));
            pageRef.setRedirect(true);
            return pageRef;
        }
       
        String userAccountId;
         String langLocale;
        //if(UserUtil.CurrentUser.AccountId!=null) userAccountId = (String)UserUtil.CurrentUser.AccountId;
        // UserUtil is extremely inefficient; better just to get account ID straight from database
        List<User> users = [SELECT Id, AccountId, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()];
        if (users.size() > 0) {
            userAccountId = users[0].AccountId;
            langLocale = users[0].LanguageLocaleKey;
            UserLangLocale = langLocale;
        }
        if (userAccountId == null || userAccountId == '' || userAccountId.substring(0, 3) != '001') { 
            PageReference pageRef = Page.MBRNonPortalUsersError;
            pageRef.setRedirect(true);
            return pageRef;
        }
        return null;
    }
}