/**
 * Unit tests for <code>QuoteDAO</code> class.
 * 
 * @author Max Rudman
 * @since 11/23/2010
 */
@isTest
private class QuoteDAOTests {
    private static SBQQ__Quote__c quote;
    
    testMethod static void testSaveDeletedGroup() {
        setUp();
        
        SBQQ__QuoteLineGroup__c g = new SBQQ__QuoteLineGroup__c(SBQQ__Quote__c=quote.Id);
        insert g;
        
        SBQQ__QuoteLine__c l1 = new SBQQ__QuoteLine__c(SBQQ__Quote__c=quote.Id,SBQQ__Group__c=g.Id);
        SBQQ__QuoteLine__c l2 = new SBQQ__QuoteLine__c(SBQQ__Quote__c=quote.Id,SBQQ__Group__c=g.Id);
        insert new List<SBQQ__QuoteLine__c>{l1,l2};
        
        QuoteVO vo = new QuoteVO(quote);
        
        QuoteDAO target = new QuoteDAO();
        target.save(vo);
        
        System.assertEquals(0, [SELECT count() FROM SBQQ__QuoteLineGroup__c WHERE SBQQ__Quote__c = :quote.Id]);
        System.assertEquals(0, [SELECT count() FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c = :quote.Id]);
    }
    
    testMethod static void testSaveNested() {
        setUp();
        
        QuoteVO vo = new QuoteVO(quote);
        QuoteVO.LineItem li1 = vo.addLineItem(new SBQQ__QuoteLine__c());
        QuoteVO.LineItem li2 = vo.addLineItem(new SBQQ__QuoteLine__c());
        li1.addComponent(li2);
        QuoteVO.LineItem li3 = vo.addLineItem(new SBQQ__QuoteLine__c());
        li2.addComponent(li3);
        
        QuoteDAO target = new QuoteDAO();
        target.save(vo);
        
        SBQQ__QuoteLine__c l1 = [SELECT SBQQ__RequiredBy__c FROM SBQQ__QuoteLine__c WHERE Id = :li1.line.Id];
        System.assert(l1.SBQQ__RequiredBy__c == null);
        
        SBQQ__QuoteLine__c l2 = [SELECT SBQQ__RequiredBy__c FROM SBQQ__QuoteLine__c WHERE Id = :li2.line.Id];
        System.assertEquals(l1.Id, l2.SBQQ__RequiredBy__c);
        
        SBQQ__QuoteLine__c l3 = [SELECT SBQQ__RequiredBy__c FROM SBQQ__QuoteLine__c WHERE Id = :li3.line.Id];
        System.assertEquals(l2.Id, l3.SBQQ__RequiredBy__c);
    }
    
    private static void setUp() {
        OpportunityStage stage = [SELECT MasterLabel FROM OpportunityStage WHERE IsActive = true LIMIT 1];
        
        Opportunity opp = new Opportunity(Name='Test',CloseDate=System.today(),StageName=stage.MasterLabel);
        insert opp;
        
        quote = new SBQQ__Quote__c(SBQQ__Opportunity__c=opp.Id);
        insert quote;
    }
}