public with sharing class NAAS_MACD_ServiceAccountAndAddressExt {
    public NAAS_InOrderFlowPACController controllerRef;
    public NAAS_PAC_MACD_Controller MacdcontrollerRef;
    public NAAS_OutOrderFlowPACController OutsideFlowControllerRef;
    Public Boolean ShowDedupeError = False;
    public NAAS_MACD_ServiceAccountAndAddressExt(NAAS_InOrderFlowPACController controller) {
            this.controllerRef=controller;
    }
    
    public NAAS_MACD_ServiceAccountAndAddressExt(NAAS_PAC_MACD_Controller controller) {
            this.MacdcontrollerRef=controller;
    }

    public NAAS_MACD_ServiceAccountAndAddressExt(NAAS_OutOrderFlowPACController controller) {
            this.OutsideFlowControllerRef=controller;
    }
 
// Added by Aditya Jamwal(IBM),This method will return Servcice Account
Public Account CreateServiceAccount(Id ParentAccountID, SMBCare_Address__c smbCareAddr, ServiceAddressData SAData)
    {
        Id ServiceRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        List<SMBCare_Address__c> SMBCareList = New List<SMBCare_Address__c>();
        NAAS_ServiceAddressControllerHelper callSAHelper = new NAAS_ServiceAddressControllerHelper();        
                
                //**********************************************************************************************************/     
        
        if(SAData.isManualCapture){
            smbCareAddr.Address__c =SAData.address;
            //City fix for WP1 
            if (municipality__c.getValues(SAData.city) == null) {
                smbCareAddr.city__c = SAData.city;
            } else { 
                smbCareAddr.city__c = municipality__c.getValues(SAData.city).decode__c;   
            }
            smbCareAddr.Province__c = SAData.province;
            smbCareAddr.Postal_Code__c= SAData.postalCode;  
            
            smbCareAddr.Country__c=SAData.country;    
            smbCareAddr.Status__c = 'Invalid';         
            Account ac = CreateServiceAcc(SAData, ParentAccountID); 
            system.debug('!! 2 acc '+ac);
            id smbCareAddrId =callSAHelper.createServiceAddress(smbCareAddr, SAData,ParentAccountID,ac.Id);
            system.debug('!! 3 acc '+ac);
            return ac;                                                                       
      }

          boolean serviceAccountFound=false;
   
          SMBCareList = [select Service_Account_Id__c,id, Account__c, FMS_Address_ID__c, Status__c,Postal_Code__c , Street_Number__c, city__c , Street__c, Street_Name__c, Street_Direction_Code__c , Suite_Number__c , Province__c, COID__c , Building_Number__c , Country__c,  State__c, Civic_Number__c, Street_Type_Code__c , Location_Id__c , Address__c , Address_Type__c ,  LocalRoutingNumber__c , NPA_NXX__c , SwitchNumber__c , TerminalNumber__c, Service_Account_Id__r.RecordTypeId  from SMBCare_Address__c where Account__c =:ParentAccountID and  FMS_Address_ID__c=:SAData.fmsId];
          system.debug('!! 1 '+SMBCareList);
        if(SMBCareList!=null) {
            for(SMBCare_Address__c SmbCare: SMBCareList){         
              if(String.isNotBlank(SmbCare.Service_Account_Id__c) && SmbCare.Service_Account_Id__r.RecordTypeId.equals(ServiceRecordType) && SmbCare.Account__c ==ParentAccountID){
                  serviceAccountFound=true;
                  callSAHelper.updateServiceAddress(SmbCare, SAData, SmbCare.id);
                   system.debug('!! 1acc '+SmbCare.Service_Account_Id__c);
                   system.debug('$$$$serviceAddressId:'+SmbCare.id);
                  return new Account(id=SmbCare.Service_Account_Id__c,parentId=ParentAccountID);
              }  
          }
        }
          
          if(!serviceAccountFound && SMBCareList.size()>0){
              List<SMBCare_Address__c> toUpdateList=new List<SMBCare_Address__c>();
                          Account ac = new Account();
              for(SMBCare_Address__c SmbCare: SMBCareList){
                  if(String.isBlank(SmbCare.Service_Account_Id__c) && SmbCare.Account__c ==ParentAccountID){
                      ac = CreateServiceAcc(SAData, ParentAccountID);
                      SmbCare.Service_Account_Id__c=ac.id;  
                      toUpdateList.add(SmbCare);  
                    break;   
                  }
              }
              if(toUpdateList.size()>0){                  
                  update toUpdateList;  
              } 
              system.debug('!! 2 acc '+ac);
                        return ac;      
          }
  
          if(SMBCareList==null || SMBCareList.size()==0){
               System.debug('No record found###'); 
               Account ac = CreateServiceAcc(SAData, ParentAccountID); 
               id smbCareAddrId =callSAHelper.createServiceAddress(smbCareAddr, SAData,ParentAccountID,ac.Id);
              system.debug('!! 3 acc '+ac);
                           return ac;
          }
                system.debug('!! 4 acc ');  
                 return null; 
                //**********************************************************************************************************/         
    }
    
    // Piyush: This method is overloaded
    Public Account CreateServiceAccount(Id ParentAccountID, SMBCare_Address__c smbCareAddr, ServiceAddressData SAData,String OrderId)
    {
        System.debug('OrderId-------------->'+ OrderId);
        Id ServiceRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        List<SMBCare_Address__c> SMBCareList = New List<SMBCare_Address__c>();
        NAAS_ServiceAddressControllerHelper callSAHelper = new NAAS_ServiceAddressControllerHelper();        
                
              
        if(SAData.isManualCapture){
            smbCareAddr.Address__c =SAData.address;
            //City fix for WP1 
            if (municipality__c.getValues(SAData.city) == null) {
                smbCareAddr.city__c = SAData.city;
            } else { 
                smbCareAddr.city__c = municipality__c.getValues(SAData.city).decode__c;   
            }
            smbCareAddr.Province__c = SAData.province;
            smbCareAddr.Postal_Code__c= SAData.postalCode;  
            
            smbCareAddr.Country__c=SAData.country;    
            smbCareAddr.Status__c = 'Invalid';         
            Account ac = CreateServiceAcc(SAData, ParentAccountID); 
            system.debug('!! 2 acc '+ac);
            id smbCareAddrId =callSAHelper.createServiceAddress(smbCareAddr, SAData,ParentAccountID,ac.Id);
            system.debug('OrderId------>' + OrderId);
            system.debug('smbCareAddrId------>' + smbCareAddrId);
            //if(smbCareAddrId != null && OrderId != null) {
                   system.debug('ac.Id------>' + ac.Id);
                   system.debug('smbCareAddrId------>' + smbCareAddrId);
                   system.debug('ParentAccountID------>' + ParentAccountID);
                   system.debug('smbCareAddr.TVServicability__c---------->'+smbCareAddr.TVServicability__c);
                   CreateServiceAddressOrderEntryObj(OrderId,smbCareAddrId);
                   
            //}
            system.debug('!! 3 acc '+ac);
            return ac;                                                                       
       }
        
          boolean serviceAccountFound=false;
   
          SMBCareList = [select Service_Account_Id__c,TVServicability__c,MaxSpeedWithType__c,LTE_Serviceability__c,HSIA_Serviceability__c ,
                          GPON_Serviceability__c  ,id, Account__c, FMS_Address_ID__c, Status__c,Postal_Code__c , Street_Number__c, city__c , 
                          Street__c, Street_Name__c, Street_Direction_Code__c , Suite_Number__c , Province__c, COID__c , 
                          Building_Number__c , Country__c,  State__c, Civic_Number__c, Street_Type_Code__c , Location_Id__c , Address__c , 
                          Address_Type__c ,  LocalRoutingNumber__c , NPA_NXX__c , SwitchNumber__c , TerminalNumber__c, 
                          Service_Account_Id__r.RecordTypeId  
                          from SMBCare_Address__c 
                          where Account__c =:ParentAccountID and  FMS_Address_ID__c=:SAData.fmsId];
          system.debug('!! 1 '+SMBCareList);
          //added on 5-oct-2016
          if(SMBCareList.size()>0){
              SMBCareList[0].TVServicability__c= smbCareAddr.TVServicability__c;
              SMBCareList[0].LTE_Serviceability__c=  smbCareAddr.LTE_Serviceability__c;
              SMBCareList[0].HSIA_Serviceability__c = smbCareAddr.HSIA_Serviceability__c; 
              SMBCareList[0].GPON_Serviceability__c  = smbCareAddr.GPON_Serviceability__c; 
              SMBCareList[0].MaxSpeedWithType__c= smbCareAddr.MaxSpeedWithType__c; 
              
           }
           List<ServiceAddressOrderEntry__c> lstjunctionObj = new List<ServiceAddressOrderEntry__c>();        
          //ends here
        if(SMBCareList!=null) {
            for(SMBCare_Address__c SmbCare: SMBCareList){         
              if(String.isNotBlank(SmbCare.Service_Account_Id__c) && SmbCare.Service_Account_Id__r.RecordTypeId.equals(ServiceRecordType) && SmbCare.Account__c ==ParentAccountID){
                  serviceAccountFound=true;
                  callSAHelper.updateServiceAddress(SmbCare, SAData, SmbCare.id);
                  CreateServiceAddressOrderEntryObj(OrderId,SmbCare.id);
                  
                   system.debug('!! 1acc '+SmbCare.Service_Account_Id__c);
                   system.debug('$$$$serviceAddressId:'+SmbCare.id);
                  return new Account(id=SmbCare.Service_Account_Id__c,parentId=ParentAccountID);
              }  
          }
        } 
          
         
          if(!serviceAccountFound && SMBCareList.size()>0){
              List<SMBCare_Address__c> toUpdateList=new List<SMBCare_Address__c>();
                          Account ac = new Account();
              for(SMBCare_Address__c SmbCare: SMBCareList){
                  if(String.isBlank(SmbCare.Service_Account_Id__c) && SmbCare.Account__c ==ParentAccountID){
                      ac = CreateServiceAcc(SAData, ParentAccountID);
                      SmbCare.Service_Account_Id__c=ac.id;  
                      toUpdateList.add(SmbCare);  
                    break;   
                  }
              }
              if(toUpdateList.size()>0){                  
                  update toUpdateList;  
              } 
              system.debug('!! 2 acc '+ac);
                        return ac;      
          }
  
          if(SMBCareList==null || SMBCareList.size()==0){
               System.debug('No record found###'); 
               Account ac = CreateServiceAcc(SAData, ParentAccountID); 
               id smbCareAddrId =callSAHelper.createServiceAddress(smbCareAddr, SAData,ParentAccountID,ac.Id);
               system.debug('***** smbCareAddrId: ' + smbCareAddrId);
               //if((smbCareAddrId != null && smbCareAddrId !='') && (OrderId != null && OrderId != '')) {
                   system.debug('***** smbCareAddrId not null');
                   CreateServiceAddressOrderEntryObj(OrderId,smbCareAddrId);
                   
               //}
              
              system.debug('!! 3 acc '+ac);
              return ac;
          }
                system.debug('!! 4 acc ');  
                 return null; 
                        
    }
    Public Account CreateServiceAccount(Id ParentAccountID, SMBCare_Address__c smbCareAddr, ServiceAddressData SAData,String OrderId, Boolean CreateJunctionObj)
    {
        System.debug('OrderId-------------->'+ OrderId);
        Id ServiceRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        List<SMBCare_Address__c> SMBCareList = New List<SMBCare_Address__c>();
        NAAS_ServiceAddressControllerHelper callSAHelper = new NAAS_ServiceAddressControllerHelper();        
                
                //**********************************************************************************************************/     
        
        if(SAData.isManualCapture){
            smbCareAddr.Address__c =SAData.address;
            //City fix for WP1 
            if (municipality__c.getValues(SAData.city) == null) {
                smbCareAddr.city__c = SAData.city;
            } else { 
                smbCareAddr.city__c = municipality__c.getValues(SAData.city).decode__c;   
            }
            smbCareAddr.Province__c = SAData.province;
            smbCareAddr.Postal_Code__c= SAData.postalCode;  
            
            smbCareAddr.Country__c=SAData.country;    
            smbCareAddr.Status__c = 'Invalid';         
            Account ac = CreateServiceAcc(SAData, ParentAccountID); 
            system.debug('!! 2 acc '+ac);
            id smbCareAddrId =callSAHelper.createServiceAddress(smbCareAddr, SAData,ParentAccountID,ac.Id);
            system.debug('OrderId------>' + OrderId);
            system.debug('smbCareAddrId------>' + smbCareAddrId);
            //if(smbCareAddrId != null && OrderId != null) {
                   system.debug('ac.Id------>' + ac.Id);
                   system.debug('smbCareAddrId------>' + smbCareAddrId);
                   system.debug('ParentAccountID------>' + ParentAccountID);
                   system.debug('smbCareAddr.TVServicability__c---------->'+smbCareAddr.TVServicability__c);
                   List<ServiceAddressOrderEntry__c> checkforDedupe = new  List<ServiceAddressOrderEntry__c>();
                   checkforDedupe = [SELECT id,Address__r.Address__c,Address__r.Province__c,Address__r.city__c,Address__r.Country__c,Address__r.Postal_Code__c,
                                       Address__r.GPON_Serviceability__c,Address__r.HSIA_Serviceability__c,Address__r.LTE_Serviceability__c,Address__r.TVServicability__c,
                                       Address__r.MaxSpeedWithType__c
                                       FROM ServiceAddressOrderEntry__c 
                                       Where Order__c =: OrderId AND Address__r.Address__c =: SAData.address AND Address__r.city__c =: SAData.city AND Address__r.Province__c=: SAData.Province AND Address__r.Postal_Code__c =:SAData.postalCode ];
                   System.Debug('SAData.address+SAData.city+SAData.Province+SAData.postalCode'+SAData.address+SAData.city+SAData.Province+SAData.postalCode);
                   
                   IF(checkforDedupe.size() == 0){
                   If(CreateJunctionObj){
                       CreateServiceAddressOrderEntryObj(OrderId,smbCareAddrId);
                    }
                   }
                IF(checkforDedupe.size() >= 1){
                    showdedupe();
                }
            //}
            system.debug('!! 3 acc '+ac);
            return ac;                                                                       
       }
        
          boolean serviceAccountFound=false;
   
          SMBCareList = [select Service_Account_Id__c,TVServicability__c,MaxSpeedWithType__c,LTE_Serviceability__c,HSIA_Serviceability__c ,
                          GPON_Serviceability__c  ,id, Account__c, FMS_Address_ID__c, Status__c,Postal_Code__c , Street_Number__c, city__c , Street__c, 
                          Street_Name__c, Street_Direction_Code__c , Suite_Number__c , Province__c, COID__c , Building_Number__c , Country__c,  State__c, 
                          Civic_Number__c, Street_Type_Code__c , Location_Id__c , Address__c , Address_Type__c ,  LocalRoutingNumber__c , NPA_NXX__c , 
                          SwitchNumber__c , TerminalNumber__c, Service_Account_Id__r.RecordTypeId  
                          from SMBCare_Address__c 
                          where Account__c =:ParentAccountID and  FMS_Address_ID__c=:SAData.fmsId];
          system.debug('!! 1 '+SMBCareList);
            system.debug('!! 1 1'+SMBCareList.size());
          
           List<ServiceAddressOrderEntry__c> lstjunctionObj = new List<ServiceAddressOrderEntry__c>();        
          //ends here
        if(SMBCareList!=null) {
            for(SMBCare_Address__c SmbCare: SMBCareList){         
              if(String.isNotBlank(SmbCare.Service_Account_Id__c) && SmbCare.Service_Account_Id__r.RecordTypeId.equals(ServiceRecordType) && SmbCare.Account__c ==ParentAccountID){
                  serviceAccountFound=true;
                  callSAHelper.updateServiceAddress(SmbCare, SAData, SmbCare.id);
                  if(CreateJunctionObj){
                      CreateServiceAddressOrderEntryObj(OrderId,SmbCare.id);
                  }
                  /*ServiceAddressOrderEntry__c ServiceAddressOrderEntryObj = new ServiceAddressOrderEntry__c();
                  ServiceAddressOrderEntryObj.Order__c = OrderId;
                   ServiceAddressOrderEntryObj.Address__c = SmbCare.id;
                   lstjunctionObj.add(ServiceAddressOrderEntryObj);
                   System.debug('lstjunctionObj.size()--------->' + lstjunctionObj.size());
                   if(lstjunctionObj.size()>0){
                       insert lstjunctionObj;
                   }*/
                   system.debug('!! 1acc '+SmbCare.Service_Account_Id__c);
                   system.debug('$$$$serviceAddressId:'+SmbCare.id);
                  return new Account(id=SmbCare.Service_Account_Id__c,parentId=ParentAccountID);
              }  
          }
        }
          //added on 5-oct-2016
         
         /* System.debug('lstjunctionObj.size()--------->' + lstjunctionObj.size());
          if(lstjunctionObj.size()>0){
              insert lstjunctionObj;
          }*/
          //ends here
          system.debug('serviceAccountFound: '+serviceAccountFound);
          system.debug('List Size :' +SMBCareList.size());
        if(!serviceAccountFound && SMBCareList.size()>0){
              List<SMBCare_Address__c> toUpdateList=new List<SMBCare_Address__c>();
                          Account ac = new Account();
              for(SMBCare_Address__c SmbCare: SMBCareList){
                  if(String.isBlank(SmbCare.Service_Account_Id__c) && SmbCare.Account__c ==ParentAccountID){
                      ac = CreateServiceAcc(SAData, ParentAccountID);
                      SmbCare.Service_Account_Id__c=ac.id;  
                      toUpdateList.add(SmbCare);  
                    break;   
                  }
              }
              if(toUpdateList.size()>0){                  
                  update toUpdateList;  
              } 
              system.debug('!! 2 acc '+ac);
                        return ac;      
          }
  
          if(SMBCareList==null || SMBCareList.size()==0){
               System.debug('No record found###'); 
               Account ac = CreateServiceAcc(SAData, ParentAccountID); 
               id smbCareAddrId =callSAHelper.createServiceAddress(smbCareAddr, SAData,ParentAccountID,ac.Id);
               system.debug('***** smbCareAddrId: ' + smbCareAddrId);
               //if((smbCareAddrId != null && smbCareAddrId !='') && (OrderId != null && OrderId != '')) {
                   system.debug('***** smbCareAddrId not null');
                   if(CreateJunctionObj){
                       CreateServiceAddressOrderEntryObj(OrderId,smbCareAddrId);
                   }
                   
               //}
              
              system.debug('!! 3 acc '+ac);
              return ac;
          }
                system.debug('!! 4 acc ');  
                 return null; 
                //**********************************************************************************************************/         
    }
        
    Public void CreateServiceAddressOrderEntryObj(Id OrdrID, Id smbCareAddrId){
                   List<ServiceAddressOrderEntry__c> ListofJunctionObj = new List<ServiceAddressOrderEntry__c>();
                   ListofJunctionObj = [select id,Order__c, Address__c from ServiceAddressOrderEntry__c where Order__c =:OrdrID and Address__c =:smbCareAddrId];
                   
                   if(ListofJunctionObj.size() == 0){
                       ServiceAddressOrderEntry__c junctionObj = new ServiceAddressOrderEntry__c();
                       junctionObj.Order__c = OrdrID;
                       junctionObj.Address__c = smbCareAddrId;
                       insert junctionObj;
                   }
                   if(ListofJunctionObj.size() != 0){
                   
                         ShowDedupe();              
                   }
    }
    Public Account CreateServiceAcc(ServiceAddressData SAData, String parent)
    {
        Id ServiceRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        Account Acc;
        if(SAData.ismanualcapture){
         String sAddr = ' '+(String)SAData.address+ ' ' +(String)SAData.city+ ' ' +(String)SAData.province+ ' '+(String)SAData.postalCode+' ' +(String)SAData.country;
            Acc = New Account(Name=sAddr +' '+SAData.fmsId, RecordTypeId=ServiceRecordType, ParentId=parent);
        }else{
            Acc = New Account(Name=SAData.searchString +' '+SAData.fmsId, RecordTypeId=ServiceRecordType, ParentId=parent);
        }        
        try
        {
        Insert Acc;
        //added by danish to see if add is working--------------------->10/10/2016
        
        }
        catch(exception e)
        { return null;
        }
        
       system.debug('Account----------------------------------->'+Acc);
       return Acc; 
    }
    
    /* Service Account creation end */
   public pagereference ShowDedupe() {        
        ShowDedupeError = True;
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Address is already added to list'));
       return null;    
    }       
}