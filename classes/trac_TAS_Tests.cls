@isTest //(SeeAllData=true)
private class trac_TAS_Tests {

  static testMethod void testDuplicateTrigger() {
    
    // insert a TAS
    Target_Account_Selling__c theFirstTAS = new Target_Account_Selling__c(Name = 'Test Name');
    insert theFirstTAS;
    
    // insert a TAS with the same name
    Target_Account_Selling__c theSecondTAS = new Target_Account_Selling__c(Name = 'Test Name');
    Boolean exceptionThrown = false;
    try {
      insert theSecondTAS;
    } catch (Exception e) {
      system.debug('Duplicate Check - Exception Thrown: ' + e.getMessage());
      exceptionThrown = true;
    }
    
    // confirm an exception was thrown
    system.assertEquals(true, exceptionThrown);
    
    
    
    // insert a TAS with different name
    Target_Account_Selling__c theThirdTAS = new Target_Account_Selling__c(Name = 'Test Name Changed');
    insert theThirdTAS;
    
    exceptionThrown = false;
    try {
      theThirdTAS.Name = 'Test Name';
      update theThirdTAS;
    } catch (Exception e) {
      system.debug('Duplicate Check - Exception Thrown: ' + e.getMessage());
      exceptionThrown = true;
    }
    
    // confirm an exception was thrown
    system.assertEquals(true, exceptionThrown);
  }
  
  public static TestMethod void testTASCreation() {
    
    // Insert custom settings
    TAS_Settings__c theSettings = new TAS_Settings__c(Long_Record_Type__c = 'Target Account Selling Plan Full Version',
                                                      Short_Record_Type__c = 'Target Account Selling Plan Short Version');
    insert theSettings;
    
    // User must have a profile and a role
    Id PROFILEID = [SELECT id FROM Profile WHERE name='System Admin TEAM no PW expiry' LIMIT 1].id;
    
    // Get a user to test
    User tester = [SELECT Id FROM User WHERE isActive = true AND ProfileId = :PROFILEID LIMIT 1];
    
    // Create Account
    Account a = new Account(name = 'Test TAS Account', BillingCountry = 'US');
    system.runAs(tester) {  insert a; }
    
    // Create contact for Political Map (TAS Contact)
    Contact c = new Contact(LastName = 'Test TAS Contact');
    system.runAs(tester) {  insert c; }
    
    // Create opportunity
    Opportunity o = new Opportunity(name = 'Test Opp', StageName = 'Trial', CloseDate = date.today().addDays(5));
    system.runAs(tester) {  insert o; }
    
    // Create pre-existing TAS
    Target_Account_Selling__c existingTAS = new Target_Account_Selling__c(Name = 'Test TAS');
    insert existingTAS;
    
    Test.startTest();
    
    // Create intermediary page
    PageReference theTASChooserPage = Page.trac_TAS_Chooser;
    theTASChooserPage.getParameters().put('OPPID', o.Id);
    Test.setCurrentPage(theTASChooserPage);
    
    // Choose an existing TAS
    trac_TAS_Chooser_Controller theChooserController = new trac_TAS_Chooser_Controller();
    theChooserController.selectedTAS = existingTAS.Id;
    system.assert(theChooserController.doChooseTAS() != null);
    
    // Verify existing TAS was linked to the opportunity
    o = [SELECT Id, Name, Target_Account_Selling_Plan__c FROM Opportunity WHERE Id = :o.Id];
    system.assertEquals(o.Target_Account_Selling_Plan__c, existingTAS.Id);
    
    // Clear the opportunity lookup
    o.Target_Account_Selling_Plan__c = null;
    update o;
    
    // Create a new TAS, fail because no record type is selected
    PageReference theTASPage = theChooserController.doCreateTAS();
    system.assert(theTASPage == null);
    
    // Select long record type and create new TAS
    theChooserController.selectedTASRecordType = theChooserController.longRecordType;
    theTASPage = theChooserController.doCreateTAS();
    system.assert(theTASPage != null);
    
    // Set the tas page
    Test.setCurrentPage(theTASPage);
    
    // Init TAS controller
    trac_TAS_Controller theController = new trac_TAS_Controller();
    theController.doInitialize();
    
    // Save the TAS
    theController.theTAS.Name = 'Test New TAS';
    theTASPage = theController.doSave();
    
    // Reload page
    if(null!=theTASPage)
    Test.setCurrentPage(theTASPage);
    theController = new trac_TAS_Controller();
    theController.doInitialize();
    system.assert(theController.theId != null);
    
    // Verify the new TAS was linked to the opportunity
    o = [SELECT Id, Name, Target_Account_Selling_Plan__c FROM Opportunity WHERE Id = :o.Id];
    system.assertEquals(o.Target_Account_Selling_Plan__c, theController.theTAS.Id);
    
  }
  
  
  public static TestMethod void testTASRelatedLists() {
    insertTestExternalUserPortalSettings();
    // Insert custom settings
    TAS_Settings__c theSettings = new TAS_Settings__c(Long_Record_Type__c = 'Target Account Selling Plan Full Version',
                                                      Short_Record_Type__c = 'Target Account Selling Plan Short Version');
    insert theSettings;
    
    // User must have a profile and a role
    Id PROFILEID = [SELECT id FROM Profile WHERE name='System Admin TEAM no PW expiry' LIMIT 1].id;
    
    // Get a user to test
    User tester = [SELECT Id FROM User WHERE isActive = true AND ProfileId = :PROFILEID LIMIT 1];
    
    // Create Account
    Account a = new Account(name = 'Test TAS Account', BillingCountry = 'US');
    system.runAs(tester) {  insert a; }
    
    // Create contact for Political Map (TAS Contact)
    Contact c = new Contact(LastName = 'Test TAS Contact');
    system.runAs(tester) {  insert c; }
    
    // Create opportunity
    Opportunity o = new Opportunity(name = 'Test Opp', StageName = 'Trial', CloseDate = date.today().addDays(5));
    system.runAs(tester) {  insert o; }
    
    Test.startTest();
    
    // Set the tas page
    PageReference theTASPage = Page.trac_TAS;
    theTASPage.getParameters().put('OPPID', o.Id);
    Test.setCurrentPage(theTASPage);
    
    // Init TAS controller
    trac_TAS_Controller theController = new trac_TAS_Controller();
    theController.doInitialize();
    
    // Save the TAS
    theController.theTAS.Name = 'Test New TAS';
    theTASPage = theController.doSave();
    
    // Reload page
    if(null!=theTASPage)
        Test.setCurrentPage(theTASPage);
    theController = new trac_TAS_Controller();
    theController.doInitialize();
    system.assert(theController.theId != null);
    
    // Verify the new TAS was linked to the opportunity
    o = [SELECT Id, Name, Target_Account_Selling_Plan__c FROM Opportunity WHERE Id = :o.Id];
    system.assertEquals(o.Target_Account_Selling_Plan__c, theController.theTAS.Id);
    
    // Add Scores to the TAS
    theController.theTAS.Access_to_Funds_Score__c = 3;
    theController.theTAS.Compelling_Event_Score__c = 2;
    theController.theTAS.Competition_Score__c = 1;
    theController.doSave();
    system.assert(theController.theTAS.Total_WS__c > 0);
    system.assert(!theController.tasError);
    
    // Add Activity
    theController.newActivity.Subject = 'Test TAS Activity';
    theController.newActivity.Resources_Required__c = 'Some Resource';
    theController.newActivity.ActivityDate = date.today().addDays(4);
    theController.theTAS.New_Activity_Owner__c = UserInfo.getUserId();
    theController.addActivity();
    Task daNewActivity = [SELECT Id, WhoId, WhatId FROM Task WHERE Subject = 'Test TAS Activity'];
    system.assertEquals(daNewActivity.WhatId, theController.theTAS.Id);
    system.assert(!theController.tasError);
    
    theController.removeActivityID = daNewActivity.Id;
    theController.removeActivity();
    system.assert(!theController.tasError);
    
    // Add Milestone
    theController.newMilestone.Subject = 'Test TAS Milestone';
    theController.newMilestone.ActivityDate = date.today().addDays(3);
    theController.newMilestone.Name_Customer_Responsible__c = 'Customer Responsible';
    theController.addMilestone();
    Task daNewMilestone = [SELECT Id, WhoId, WhatId FROM Task WHERE Subject = 'Test TAS Milestone'];
    system.assertEquals(daNewMilestone.WhatId, theController.theTAS.Id);
    system.assert(!theController.tasError);
    
    theController.removeMilestoneID = daNewMilestone.Id;
    theController.removeMilestone();
    system.assert(!theController.tasError);
    
    // Add Business Partner
    theController.newBusinessPartner.Partner_Name__c = 'Test TAS Partner';
    theController.newBusinessPartner.Is_there_an_opportunity__c = 'Is there an opportunity';
    theController.addBusinessPartner();
    TAS_Business_Partners_Competitors__c daNewBP = [SELECT Id, Target_Account_Selling__c, Partner_Name__c
                                                    FROM TAS_Business_Partners_Competitors__c
                                                    WHERE Partner_Name__c = 'Test TAS Partner'];
    system.assert(!theController.tasError);
    system.assertEquals(daNewBP.Target_Account_Selling__c, theController.theTAS.Id);
    
    theController.removeBusinessPartnerID = daNewBP.Id;
    theController.removeBusinessPartner();
    system.assert(!theController.tasError);
    
    
    // Add Competitor
    theController.newCompetitor.Competitor_Name__c = 'Test TAS Competitor';
    theController.newCompetitor.Solution_Proposed__c = 'Is there an opportunity';
    theController.addCompetitor();
    TAS_Business_Partners_Competitors__c daNewC = [SELECT Id, Target_Account_Selling__c, Competitor_Name__c
                                                    FROM TAS_Business_Partners_Competitors__c
                                                    WHERE Competitor_Name__c = 'Test TAS Competitor'];
    system.assertEquals(daNewC.Target_Account_Selling__c, theController.theTAS.Id);
    system.assert(!theController.tasError);
    
    theController.removeCompetitorID = daNewC.Id;
    theController.removeCompetitor();
    system.assert(!theController.tasError);
    
    
    // Add Decision Criteria
    theController.newDecisionCriteria.Decision_Criteria_Key_Issue__c = 'Test TAS Decision Criteria';
    theController.addDecisionCriteria();
    TAS_Relationship_Questionare__c daNewDC = [SELECT Id, Target_Account_Selling__c
                                                    FROM TAS_Relationship_Questionare__c
                                                    WHERE Decision_Criteria_Key_Issue__c = 'Test TAS Decision Criteria'];
    system.assertEquals(daNewDC.Target_Account_Selling__c, theController.theTAS.Id);
    system.assert(!theController.tasError);
    
    
      // Add Decision Criteria Player
      theController.theDecisionCriteriaWrapped[0].theNewPlayer.Name = 'Test TAS Decision Criteria Player';
      theController.theDecisionCriteriaWrapped[0].theNewPlayer.Score__c = 'Medium';
      theController.decisionCriteriaPlayerParent = daNewDC.Id;
      theController.addDecisionCriteriaPlayer();
      TAS_Player__c daNewDCP = [SELECT Id, TAS_Relationship_Questionare__c
                                FROM TAS_Player__c
                                WHERE Name = 'Test TAS Decision Criteria Player'];
      system.assertEquals(daNewDCP.TAS_Relationship_Questionare__c, daNewDC.Id);
      system.assert(!theController.tasError);
      
      theController.removeDecisionCriteriaPlayerID = daNewDCP.Id;
      theController.removeDecisionCriteriaPlayer();
      system.assert(!theController.tasError);
    
    
    theController.removeDecisionCriteriaID = daNewDC.Id;
    theController.removeDecisionCriteria();
    system.assert(!theController.tasError);
    
    
    // Add Relationship Strategy
    theController.newRelationshipStrategy.Key_Player_Name__c = 'Test TAS Relationship Strategy';
    theController.addRelationshipStrategy();
    TAS_Relationship_Questionare__c daNewRS = [SELECT Id, Target_Account_Selling__c
                                                    FROM TAS_Relationship_Questionare__c
                                                    WHERE Key_Player_Name__c = 'Test TAS Relationship Strategy'];
    system.assertEquals(daNewRS.Target_Account_Selling__c, theController.theTAS.Id);
    system.assert(!theController.tasError);
    
    theController.removeRelationshipStrategyID = daNewRS.Id;
    theController.removeRelationshipStrategy();
    system.assert(!theController.tasError);
    
    // Add TASContact
    theController.newTASContact.Contact__c = c.Id;
    theController.addTASContact();
    TAS_Relationship_Questionare__c daNewTS = [SELECT Id, Target_Account_Selling__c
                                               FROM TAS_Relationship_Questionare__c
                                               WHERE Contact__c = :c.Id];
    system.assertEquals(daNewTS.Target_Account_Selling__c, theController.theTAS.Id);
    system.assert(!theController.tasError);
    
    theController.removeTASContactID = daNewTS.Id;
    theController.removeTASContact();
    system.assert(!theController.tasError);
    
  }
  
  
  public static TestMethod void testTASButtons() {
    insertTestExternalUserPortalSettings();
    // Insert custom settings
    TAS_Settings__c theSettings = new TAS_Settings__c(Long_Record_Type__c = 'Target Account Selling Plan Full Version',
                                                      Short_Record_Type__c = 'Target Account Selling Plan Short Version');
    insert theSettings;
    
    // User must have a profile and a role
    Id PROFILEID = [SELECT id FROM Profile WHERE name='System Admin TEAM no PW expiry' LIMIT 1].id;
    
    // Get a user to test
    User tester = [SELECT Id FROM User WHERE isActive = true AND ProfileId = :PROFILEID LIMIT 1];
    
    // Create Account
    Account a = new Account(name = 'Test TAS Account', BillingCountry = 'US');
    system.runAs(tester) {  insert a; }
    
    // Create contact for Political Map (TAS Contact)
    Contact c = new Contact(LastName = 'Test TAS Contact');
    system.runAs(tester) {  insert c; }
    
    // Create opportunity
    Opportunity o = new Opportunity(name = 'Test Opp', StageName = 'Trial', CloseDate = date.today().addDays(5));
    system.runAs(tester) {  insert o; }
    
    Test.startTest();
    
    // Set the tas page
    PageReference theTASPage = Page.trac_TAS;
    theTASPage.getParameters().put('OPPID', o.Id);
    Test.setCurrentPage(theTASPage);
    
    // Init TAS controller
    trac_TAS_Controller theController = new trac_TAS_Controller();
    theController.doInitialize();
    
    // Save the TAS
    theController.theTAS.Name = 'Test New TAS';
    theTASPage = theController.doSave();
    
    // Reload page
    if(null!=theTASPage)
        Test.setCurrentPage(theTASPage);
    theController = new trac_TAS_Controller();
    theController.doInitialize();
    system.assert(theController.theId != null);
    
    // Verify the new TAS was linked to the opportunity
    o = [SELECT Id, Name, Target_Account_Selling_Plan__c FROM Opportunity WHERE Id = :o.Id];
    system.assertEquals(o.Target_Account_Selling_Plan__c, theController.theTAS.Id);
    
    // Add Scores to the TAS
    theController.theTAS.Access_to_Funds_Score__c = 3;
    theController.theTAS.Compelling_Event_Score__c = 2;
    theController.theTAS.Competition_Score__c = 1;
    theController.doSave();
    system.assert(theController.theTAS.Total_WS__c > 0);
    system.assert(!theController.tasError);
    
    // Add Activity
    theController.newActivity.Subject = 'Test TAS Activity';
    theController.newActivity.Resources_Required__c = 'Some Resource';
    theController.newActivity.ActivityDate = date.today().addDays(4);
    theController.theTAS.New_Activity_Owner__c = UserInfo.getUserId();
    theController.addActivity();
    Task daNewActivity = [SELECT Id, WhoId, WhatId FROM Task WHERE Subject = 'Test TAS Activity'];
    system.assertEquals(daNewActivity.WhatId, theController.theTAS.Id);
    system.assert(!theController.tasError);
    
    
    // Add Milestone
    theController.newMilestone.Subject = 'Test TAS Milestone';
    theController.newMilestone.ActivityDate = date.today().addDays(3);
    theController.newMilestone.Name_Customer_Responsible__c = 'Customer Responsible';
    theController.addMilestone();
    Task daNewMilestone = [SELECT Id, WhoId, WhatId FROM Task WHERE Subject = 'Test TAS Milestone'];
    system.assertEquals(daNewMilestone.WhatId, theController.theTAS.Id);
    system.assert(!theController.tasError);
    
    // Add Business Partner
    theController.newBusinessPartner.Partner_Name__c = 'Test TAS Partner';
    theController.newBusinessPartner.Is_there_an_opportunity__c = 'Is there an opportunity';
    theController.addBusinessPartner();
    TAS_Business_Partners_Competitors__c daNewBP = [SELECT Id, Target_Account_Selling__c, Partner_Name__c
                                                    FROM TAS_Business_Partners_Competitors__c
                                                    WHERE Partner_Name__c = 'Test TAS Partner'];
    system.assert(!theController.tasError);
    system.assertEquals(daNewBP.Target_Account_Selling__c, theController.theTAS.Id);
    
    // Add Competitor
    theController.newCompetitor.Competitor_Name__c = 'Test TAS Competitor';
    theController.newCompetitor.Solution_Proposed__c = 'Is there an opportunity';
    theController.addCompetitor();
    TAS_Business_Partners_Competitors__c daNewC = [SELECT Id, Target_Account_Selling__c, Competitor_Name__c
                                                    FROM TAS_Business_Partners_Competitors__c
                                                    WHERE Competitor_Name__c = 'Test TAS Competitor'];
    system.assertEquals(daNewC.Target_Account_Selling__c, theController.theTAS.Id);
    system.assert(!theController.tasError);
    
    // Add Decision Criteria
    theController.newDecisionCriteria.Decision_Criteria_Key_Issue__c = 'Test TAS Decision Criteria';
    theController.addDecisionCriteria();
    TAS_Relationship_Questionare__c daNewDC = [SELECT Id, Target_Account_Selling__c
                                                    FROM TAS_Relationship_Questionare__c
                                                    WHERE Decision_Criteria_Key_Issue__c = 'Test TAS Decision Criteria'];
    system.assertEquals(daNewDC.Target_Account_Selling__c, theController.theTAS.Id);
    system.assert(!theController.tasError);
    
      // Add Decision Criteria Player
      theController.theDecisionCriteriaWrapped[0].theNewPlayer.Name = 'Test TAS Decision Criteria Player';
      theController.theDecisionCriteriaWrapped[0].theNewPlayer.Score__c = 'Medium';
      theController.decisionCriteriaPlayerParent = daNewDC.Id;
      theController.addDecisionCriteriaPlayer();
      TAS_Player__c daNewDCP = [SELECT Id, TAS_Relationship_Questionare__c
                                FROM TAS_Player__c
                                WHERE Name = 'Test TAS Decision Criteria Player'];
      system.assertEquals(daNewDCP.TAS_Relationship_Questionare__c, daNewDC.Id);
      system.assert(!theController.tasError);
      
      
    // Add Relationship Strategy
    theController.newRelationshipStrategy.Key_Player_Name__c = 'Test TAS Relationship Strategy';
    theController.addRelationshipStrategy();
    TAS_Relationship_Questionare__c daNewRS = [SELECT Id, Target_Account_Selling__c
                                                    FROM TAS_Relationship_Questionare__c
                                                    WHERE Key_Player_Name__c = 'Test TAS Relationship Strategy'];
    system.assertEquals(daNewRS.Target_Account_Selling__c, theController.theTAS.Id);
    system.assert(!theController.tasError);
    
    // Add TASContact
    theController.newTASContact.Contact__c = c.Id;
    theController.addTASContact();
    TAS_Relationship_Questionare__c daNewTS = [SELECT Id, Target_Account_Selling__c
                                               FROM TAS_Relationship_Questionare__c
                                               WHERE Contact__c = :c.Id];
    system.assertEquals(daNewTS.Target_Account_Selling__c, theController.theTAS.Id);
    system.assert(!theController.tasError);
    
    // Store original TAS name
    String originalTASName = theController.theTAS.Name;
    
    // Clone the TAS
    PageReference theClonedTASPage = theController.doClone();
    Test.setCurrentPage(theClonedTASPage);
    theController = new trac_TAS_Controller();
    theController.doInitialize();
    
    // Get the cloned TAS
    ID theClonedTASId = theClonedTASPage.getParameters().get('ID');
    Target_Account_Selling__c theClonedTAS = [SELECT Id, Name
                                              FROM Target_Account_Selling__c 
                                              WHERE Id = :theClonedTASId];
    system.assertEquals(originalTASName + ' - Clone', theClonedTAS.Name);
    system.assert(!theController.theActivities.isEmpty());
    system.assert(!theController.theMilestones.isEmpty());
    system.assert(!theController.theActivities.isEmpty());
    system.assert(!theController.theActivities.isEmpty());
    
    // Delete the TAS
    theController.doDelete();
    List<Target_Account_Selling__c> emptyList = [SELECT Id FROM Target_Account_Selling__c WHERE Id = :theClonedTASId];
    system.assert(emptyList.isEmpty());
     
  }
  
  public static TestMethod void testTASRelatedListEdit() {
    insertTestExternalUserPortalSettings();
    // Insert custom settings
    TAS_Settings__c theSettings = new TAS_Settings__c(Long_Record_Type__c = 'Target Account Selling Plan Full Version',
                                                      Short_Record_Type__c = 'Target Account Selling Plan Short Version');
    insert theSettings;
    
    // User must have a profile and a role
    Id PROFILEID = [SELECT id FROM Profile WHERE name='System Admin TEAM no PW expiry' LIMIT 1].id;
    
    // Get a user to test
    User tester = [SELECT Id FROM User WHERE isActive = true AND ProfileId = :PROFILEID LIMIT 1];
    
    // Create Account
    Account a = new Account(name = 'Test TAS Account', BillingCountry = 'US');
    system.runAs(tester) {  insert a; }
    
    // Create contact for Political Map (TAS Contact)
    Contact c = new Contact(LastName = 'Test TAS Contact');
    system.runAs(tester) {  insert c; }
    
    // Create opportunity
    Opportunity o = new Opportunity(name = 'Test Opp', StageName = 'Trial', CloseDate = date.today().addDays(5));
    system.runAs(tester) {  insert o; }
    
    Test.startTest();
    
    // Set the tas page
    PageReference theTASPage = Page.trac_TAS;
    theTASPage.getParameters().put('OPPID', o.Id);
    Test.setCurrentPage(theTASPage);
    
    // Init TAS controller
    trac_TAS_Controller theController = new trac_TAS_Controller();
    theController.doInitialize();
    
    // Save the TAS
    theController.theTAS.Name = 'Test New TAS';
    theTASPage = theController.doSave();
    
    // Reload page
    if(null!=theTASPage)
        Test.setCurrentPage(theTASPage);
    theController = new trac_TAS_Controller();
    theController.doInitialize();
    system.assert(theController.theId != null);
    
    // Verify the new TAS was linked to the opportunity
    o = [SELECT Id, Name, Target_Account_Selling_Plan__c FROM Opportunity WHERE Id = :o.Id];
    system.assertEquals(o.Target_Account_Selling_Plan__c, theController.theTAS.Id);
    
    // Add Scores to the TAS
    theController.theTAS.Access_to_Funds_Score__c = 3;
    theController.theTAS.Compelling_Event_Score__c = 2;
    theController.theTAS.Competition_Score__c = 1;
    theController.doSave();
    system.assert(theController.theTAS.Total_WS__c > 0);
    system.assert(!theController.tasError);
    
    // Add Activity
    theController.newActivity.Subject = 'Test TAS Activity';
    theController.newActivity.Resources_Required__c = 'Some Resource';
    theController.newActivity.ActivityDate = date.today().addDays(4);
    theController.theTAS.New_Activity_Owner__c = UserInfo.getUserId();
    theController.addActivity();
    Task daNewActivity = [SELECT Id, WhoId, WhatId FROM Task WHERE Subject = 'Test TAS Activity'];
    system.assertEquals(daNewActivity.WhatId, theController.theTAS.Id);
    system.assert(!theController.tasError);
    
    // Change activity values
    theController.theActivities[0].Subject = 'Test TAS Activity Modified';
    theController.editableFieldId = theController.theActivities[0].Id;
    theController.saveEditable();
    Task daEditedActivity = [SELECT Id, WhoId, WhatId FROM Task WHERE Subject = 'Test TAS Activity Modified'];
    system.assertEquals(daEditedActivity.WhatId, theController.theTAS.Id);
    system.assert(!theController.tasError);
    
    // Add Milestone
    theController.newMilestone.Subject = 'Test TAS Milestone';
    theController.newMilestone.ActivityDate = date.today().addDays(3);
    theController.newMilestone.Name_Customer_Responsible__c = 'Customer Responsible';
    theController.addMilestone();
    Task daNewMilestone = [SELECT Id, WhoId, WhatId FROM Task WHERE Subject = 'Test TAS Milestone'];
    system.assertEquals(daNewMilestone.WhatId, theController.theTAS.Id);
    system.assert(!theController.tasError);
    
    // Change milestone values
    theController.theMilestones[0].Subject = 'Test TAS Milestone Modified';
    theController.editableFieldId = theController.theMilestones[0].Id;
    theController.saveEditable();
    Task daEditedMilestone = [SELECT Id, WhoId, WhatId FROM Task WHERE Subject = 'Test TAS Milestone Modified'];
    system.assertEquals(daEditedMilestone.WhatId, theController.theTAS.Id);
    system.assert(!theController.tasError);
    
    // Add Business Partner
    theController.newBusinessPartner.Partner_Name__c = 'Test TAS Partner';
    theController.newBusinessPartner.Is_there_an_opportunity__c = 'Is there an opportunity';
    theController.addBusinessPartner();
    TAS_Business_Partners_Competitors__c daNewBP = [SELECT Id, Target_Account_Selling__c, Partner_Name__c
                                                    FROM TAS_Business_Partners_Competitors__c
                                                    WHERE Partner_Name__c = 'Test TAS Partner'];
    system.assert(!theController.tasError);
    system.assertEquals(daNewBP.Target_Account_Selling__c, theController.theTAS.Id);
    
    // Change business partner values
    theController.theBusinessPartners[0].Partner_Name__c = 'Test TAS Partner Modified';
    theController.editableFieldId = theController.theBusinessPartners[0].Id;
    theController.saveEditable();
    TAS_Business_Partners_Competitors__c daEditedPartner = [SELECT Id, Target_Account_Selling__c, Partner_Name__c
                                                            FROM TAS_Business_Partners_Competitors__c
                                                            WHERE Partner_Name__c = 'Test TAS Partner Modified'];
    system.assertEquals(daEditedPartner.Target_Account_Selling__c, theController.theTAS.Id);
    system.assert(!theController.tasError);
    
    // Add Competitor
    theController.newCompetitor.Competitor_Name__c = 'Test TAS Competitor';
    theController.newCompetitor.Solution_Proposed__c = 'Is there an opportunity';
    theController.addCompetitor();
    TAS_Business_Partners_Competitors__c daNewC = [SELECT Id, Target_Account_Selling__c, Competitor_Name__c
                                                    FROM TAS_Business_Partners_Competitors__c
                                                    WHERE Competitor_Name__c = 'Test TAS Competitor'];
    system.assertEquals(daNewC.Target_Account_Selling__c, theController.theTAS.Id);
    system.assert(!theController.tasError);
    
    // Change competitor values
    theController.theCompetitors[0].Competitor_Name__c = 'Test TAS Competitor Modified';
    theController.editableFieldId = theController.theCompetitors[0].Id;
    theController.saveEditable();
    TAS_Business_Partners_Competitors__c daEditedCompetitor = [SELECT Id, Target_Account_Selling__c, Partner_Name__c
                                                            FROM TAS_Business_Partners_Competitors__c
                                                            WHERE Competitor_Name__c = 'Test TAS Competitor Modified'];
    system.assertEquals(daEditedCompetitor.Target_Account_Selling__c, theController.theTAS.Id);
    system.assert(!theController.tasError);
    
    // Add Decision Criteria
    theController.newDecisionCriteria.Decision_Criteria_Key_Issue__c = 'Test TAS Decision Criteria';
    theController.addDecisionCriteria();
    TAS_Relationship_Questionare__c daNewDC = [SELECT Id, Target_Account_Selling__c
                                                    FROM TAS_Relationship_Questionare__c
                                                    WHERE Decision_Criteria_Key_Issue__c = 'Test TAS Decision Criteria'];
    system.assertEquals(daNewDC.Target_Account_Selling__c, theController.theTAS.Id);
    system.assert(!theController.tasError);
    
      // Add Decision Criteria Player
      theController.theDecisionCriteriaWrapped[0].theNewPlayer.Name = 'Test TAS Decision Criteria Player';
      theController.theDecisionCriteriaWrapped[0].theNewPlayer.Score__c = 'Medium';
      theController.decisionCriteriaPlayerParent = daNewDC.Id;
      theController.addDecisionCriteriaPlayer();
      TAS_Player__c daNewDCP = [SELECT Id, TAS_Relationship_Questionare__c
                                FROM TAS_Player__c
                                WHERE Name = 'Test TAS Decision Criteria Player'];
      system.assertEquals(daNewDCP.TAS_Relationship_Questionare__c, daNewDC.Id);
      system.assert(!theController.tasError);
      
    
    // Change Decision Criteria
    theController.theDecisionCriteriaWrapped[0].theObj.Decision_Criteria_Key_Issue__c = 'Test TAS Decision Criteria Modified';
    theController.editableFieldId = theController.theDecisionCriteriaWrapped[0].theObj.Id;
    theController.saveEditable();
    TAS_Relationship_Questionare__c daEditedDC = [SELECT Id, Target_Account_Selling__c
                                                  FROM TAS_Relationship_Questionare__c
                                                  WHERE Decision_Criteria_Key_Issue__c = 'Test TAS Decision Criteria Modified'];
    system.assertEquals(daEditedDC.Target_Account_Selling__c, theController.theTAS.Id);
    system.assert(!theController.tasError);
    
      // Change Decision Criteria Player
      theController.theDecisionCriteriaWrapped[0].thePlayers[0].Name = 'Test TAS Decision Criteria Player Modified';
      theController.editableFieldId = theController.theDecisionCriteriaWrapped[0].thePlayers[0].Id;
      theController.saveEditable();
      TAS_Player__c daEditedDCP = [SELECT Id, TAS_Relationship_Questionare__c
                                  FROM TAS_Player__c
                                  WHERE Name = 'Test TAS Decision Criteria Player Modified'];
      system.assertEquals(daEditedDCP.TAS_Relationship_Questionare__c, daNewDC.Id);
      system.assert(!theController.tasError);
    
    // Add Relationship Strategy
    theController.newRelationshipStrategy.Key_Player_Name__c = 'Test TAS Relationship Strategy';
    theController.addRelationshipStrategy();
    TAS_Relationship_Questionare__c daNewRS = [SELECT Id, Target_Account_Selling__c
                                                    FROM TAS_Relationship_Questionare__c
                                                    WHERE Key_Player_Name__c = 'Test TAS Relationship Strategy'];
    system.assertEquals(daNewRS.Target_Account_Selling__c, theController.theTAS.Id);
    system.assert(!theController.tasError);
    
    // Change Relationship Strategy
    theController.theRelationshipStrategies[0].Key_Player_Name__c = 'Test TAS Relationship Strategy Modified';
    theController.editableFieldId = theController.theRelationshipStrategies[0].Id;
    theController.saveEditable();
    TAS_Relationship_Questionare__c daEditedRS = [SELECT Id, Target_Account_Selling__c
                                                  FROM TAS_Relationship_Questionare__c
                                                  WHERE Key_Player_Name__c = 'Test TAS Relationship Strategy Modified'];
    system.assertEquals(daEditedRS.Target_Account_Selling__c, theController.theTAS.Id);
    system.assert(!theController.tasError);
    
    // Add TASContact
    theController.newTASContact.Contact__c = c.Id;
    theController.addTASContact();
    TAS_Relationship_Questionare__c daNewTS = [SELECT Id, Target_Account_Selling__c
                                               FROM TAS_Relationship_Questionare__c
                                               WHERE Contact__c = :c.Id];
    system.assertEquals(daNewTS.Target_Account_Selling__c, theController.theTAS.Id);
    system.assert(!theController.tasError);
        
    // Change TAS Contact
    theController.theTASContacts[0].Coverage__c = 'Test Political Map Modified';
    theController.editableFieldId = theController.theTASContacts[0].Id;
    theController.saveEditable();
    TAS_Relationship_Questionare__c daEditedTS = [SELECT Id, Target_Account_Selling__c, Coverage__c
                                                  FROM TAS_Relationship_Questionare__c
                                                  WHERE Contact__c = :c.Id];
    system.assertEquals('Test Political Map Modified', daEditedTS.Coverage__c);
    system.assertEquals(daEditedTS.Target_Account_Selling__c, theController.theTAS.Id);
    system.assert(!theController.tasError);
     
  }
  
  
  public static TestMethod void testTASPermissions() {
    insertTestExternalUserPortalSettings();
    // Insert custom settings
    TAS_Settings__c theSettings = new TAS_Settings__c(Long_Record_Type__c = 'Target Account Selling Plan Full Version',
                                                      Short_Record_Type__c = 'Target Account Selling Plan Short Version');
    insert theSettings;
    
    // User must have a profile and a role
    Id PROFILEID = [SELECT id FROM Profile WHERE name='System Admin TEAM no PW expiry' LIMIT 1].id;
    Id NOACCESSPROFILEID = [SELECT id FROM Profile WHERE name='SMB Sales Prime/ASR/Coordinator' LIMIT 1].id;
    
    
    // Get a user to test
    User tester = [SELECT Id FROM User WHERE isActive = true AND ProfileId = :PROFILEID LIMIT 1];
    
    // Get a user to test
    User theUser = new User(username = Datetime.now().getTime() + 'offer@TRACTIONSM.COM',
                           email = 'test@example.com',
                           title = 'test',
                           lastname = 'test',
                           alias = 'test',
                           TimezoneSIDKey = 'America/Los_Angeles',
                           LocaleSIDKey = 'en_US',
                           EmailEncodingKey = 'UTF-8',
                           ProfileId = NOACCESSPROFILEID,
                           LanguageLocaleKey = 'en_US');
    insert theUser;
    
    // Create Account
    Account a = new Account(name = 'Test TAS Account', BillingCountry = 'US');
    system.runAs(tester) {  insert a; }
    
    // Create contact for Political Map (TAS Contact)
    Contact c = new Contact(LastName = 'Test TAS Contact');
    system.runAs(tester) {  insert c; }
    
    // Create opportunity
    Opportunity o = new Opportunity(name = 'Test Opp', StageName = 'Trial', CloseDate = date.today().addDays(5));
    system.runAs(tester) {  insert o; }
    
    // Add opportunity team member
    OpportunityTeamMember newMember = new OpportunityTeamMember(OpportunityId = o.Id,
                                                                UserId = theUser.Id);
    insert newMember;
    
    OpportunityShare newOppShare = new OpportunityShare(UserOrGroupId = theUser.Id,
                                                        OpportunityId = o.Id,
                                                        OpportunityAccessLevel = 'Edit');
    insert newOppShare;
    
    Test.startTest();
    
    // Set the tas page
    PageReference theTASPage = Page.trac_TAS;
    theTASPage.getParameters().put('OPPID', o.Id);
    Test.setCurrentPage(theTASPage);
    
    // Init TAS controller
    trac_TAS_Controller theController = new trac_TAS_Controller();
    theController.doInitialize();
    
    // Save the TAS
    theController.theTAS.Name = 'Test New TAS';
    theTASPage = theController.doSave();
    
    // Reload page
    if(null!=theTASPage)
        Test.setCurrentPage(theTASPage);
    theController = new trac_TAS_Controller();
    theController.doInitialize();
    system.assert(theController.theId != null);
    
    // Verify the new TAS was linked to the opportunity
    o = [SELECT Id, Name, Target_Account_Selling_Plan__c FROM Opportunity WHERE Id = :o.Id];
    system.assertEquals(o.Target_Account_Selling_Plan__c, theController.theTAS.Id);
    
    // Add Scores to the TAS
    theController.theTAS.Access_to_Funds_Score__c = 3;
    theController.theTAS.Compelling_Event_Score__c = 2;
    theController.theTAS.Competition_Score__c = 1;
    theController.doSave();
    system.assert(theController.theTAS.Total_WS__c > 0);
    system.assert(!theController.tasError);
    
    
    // LOAD TAS AS USER WITH NO ACCESS
    system.runAs(theUser) {
    
    // Init TAS controller
    theController = new trac_TAS_Controller();
    theController.doInitialize();
    system.debug(LoggingLevel.ERROR, 'User Access Before Grant: ' + theController.hasEditAccess);
    
    }
    
    // Delete Sharing Rules
    delete newOppShare;
    
    system.runAs(theUser) {
    
    // Reload page
    Test.setCurrentPage(theTASPage);
    theController = new trac_TAS_Controller();
    theController.doInitialize();
    system.assert(theController.theId != null);
    system.debug(LoggingLevel.ERROR, 'User Access After Grant: ' + theController.hasEditAccess);
    
    }
        
  }
  
  @isTest
  private static void insertTestExternalUserPortalSettings() {
    // if org already contains custom settings, remove for test
    delete [SELECT Id from ExternalUserPortal__c];
    
    ExternalUserPortal__c eups = new ExternalUserPortal__c(
      Base_URL__c = 'http://login.salesforce.com',
      HMAC_Digest_Key__c = '/eYPmJ0eXTb+8R/X7YE9Skv6Ly8tfChkR0gdWt9ngXg=',
      Portal_URL__c = 'http://telus.force.com/externalservice'
    );
    
    insert eups;
   }
}