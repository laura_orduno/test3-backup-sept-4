/**
 * Custom business logic for Product object.
 * 
 * @author Max Rudman
 * @since 4/6/2011
 */
public class ProductService {
    private static final Integer MAX_PRODUCT_LOAD_DEPTH = 3;
    private static ProductService INSTANCE = new ProductService();
    
    /**
     * Singleton factory method.
     */
    public static ProductService getInstance() {
        return INSTANCE;
    }
    
    /**
     * Finds product identified by specified ID.
     */
    public Map<Id,ProductVO> findProductsWithPricesAndCosts(Set<Id> productIds, Id pricebookId, String currencyCode) {
        ProductDAO dao = new ProductDAO();
        dao.loadPriceEntries = true;
        dao.loadCosts = true;
        dao.pricebookId = pricebookId;
        dao.currencyCode = currencyCode;
        return dao.loadByIds(productIds);
    }
    
    /**
     * Finds product identified by specified ID.
     */
    public Map<Id,ProductVO> findProductsWithOptionsAndPrices(Set<Id> productIds, Id accountId, Id pricebookId, String currencyCode) {
        ProductDAO dao = new ProductDAO();
        dao.loadOptions = true;
        dao.loadPriceEntries = true;
        dao.pricebookId = pricebookId;
        dao.currencyCode = currencyCode;
        Map<Id,ProductVO> result = dao.loadByIds(productIds);
        return result;
    }
    
    public Map<Id,ProductVO> findProductsWithSelectedOptionsAndPrices(Set<Id> productIds, Set<Id> optionIds, Id pricebookId, String currencyCode) {
        ProductDAO dao = new ProductDAO();
        dao.loadOptions = true;
        dao.loadPriceEntries = true;
        dao.optionIds = optionIds;
        dao.pricebookId = pricebookId;
        dao.currencyCode = currencyCode;
        dao.maxDepth = MAX_PRODUCT_LOAD_DEPTH;
        return dao.loadByIds(productIds);
    }
    
    public Map<Id,ProductVO> findProductsWithNestedOptionsAndPrices(Set<Id> productIds, Id pricebookId, String currencyCode) {
        ProductDAO dao = new ProductDAO();
        dao.loadOptions = true;
        dao.loadPriceEntries = true;
        dao.pricebookId = pricebookId;
        dao.currencyCode = currencyCode;
        dao.maxDepth = MAX_PRODUCT_LOAD_DEPTH;
        return dao.loadByIds(productIds);
    }
    
    public ProductVO findProductWithOptionPrices(Id productId, Id accountId, Id pricebookId, String currencyCode) {
        return findProductsWithOptionPrices(new Set<Id>{productId},accountId,pricebookId,currencyCode).get(productId);
    }
    
    public Map<Id,ProductVO> findProductsWithOptionPrices(Set<Id> productIds, Id accountId, Id pricebookId, String currencyCode) {
        ProductDAO dao = new ProductDAO();
        dao.loadOptions = true;
        dao.loadPriceEntries = true;
        dao.pricebookId = pricebookId;
        dao.currencyCode = currencyCode;
        Map<Id,ProductVO> vos = dao.loadByIds(productIds);
        Set<Id> optionalProductIds = new Set<Id>();
        for (ProductVO vo : vos.values()) {
            optionalProductIds.addAll(vo.getOptionProductIds());
        }
        if (!optionalProductIds.isEmpty()) {
            Map<Id,PricebookEntry> entriesByProductId = 
                findPricesByProductsAndPricebook(optionalProductIds, pricebookId, currencyCode);
            for (ProductVO vo : vos.values()) {
                for (ProductVO.Option option : vo.options) {
                    Id optionalProductId = option.record.SBQQ__OptionalSKU__c;
                    if ((option.record.SBQQ__UnitPrice__c == null) && entriesByProductId.containsKey(optionalProductId)) {
                        option.record.SBQQ__UnitPrice__c = entriesByProductId.get(optionalProductId).UnitPrice;
                    }
                }
            }
        }
        
        return vos;
    }
    
    public Map<Id,PricebookEntry> findPricesByProductsAndPricebook(Set<Id> productIds, Id pricebookId, String currencyCode) {
        PricebookEntryDAO dao = new PricebookEntryDAO();
        Map<Id,PricebookEntry> entriesByProductId = new Map<Id,PricebookEntry>();
        for (PricebookEntry entry : dao.loadByProductIds(productIds, pricebookId, currencyCode)) {
            entriesByProductId.put(entry.Product2Id, entry);
        }
        return entriesByProductId;
    }
    
}