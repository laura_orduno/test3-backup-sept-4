public class EmailToCaseTriggerHandler{
	public EmailToCaseTriggerHandler(){
        if(trigger.isexecuting){
            if(trigger.isbefore){
                beforeInit();
                if(trigger.isinsert){
                     beforeInsert();
                }
            }
        }
	}

    void beforeInit(){}
	void beforeInsert(){    
    	for(sObject record:trigger.new){
	        Case c=(Case)record;
            map<string, BlockEmailtoCaseList__c> allemails = BlockEmailtoCaseList__c.getAll();
            map<string, BlockEmailtoCaseList__c> emails = new map<string, BlockEmailtoCaseList__c>();
            for(BlockEmailtoCaseList__c email : allemails.values()){
               if(string.isnotblank(c.SuppliedEmail)&&(allemails.containsKey(c.SuppliedEmail)||(c.SuppliedEmail.containsignorecase('microsoftexchange329e71ec88ae4615bbc36ab6ce41109e@telus.com'))||(c.SuppliedEmail.containsignorecase('ssc.blackberry10support-soutiendeblackberry10.spc@canada.ca')))){
	                c.Subject.addError('Not saving e2c');
            }
               
	            }
	    }
	}
    void afterInsert(){}
}