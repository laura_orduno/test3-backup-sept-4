global class ContractCreateRelatedObj {
    
    /*
*  Main Method Called from Controller
*   
*/
	public static boolean isDocumentRequired = true;    
    public static map<string,string> mapObjectWithRelName = new map<string,string>();
    public static map<string,string> sourceDestFieldsMap_local = new map<string,string>();
    webservice static string contractUpdateFlow(Id contractID,boolean isUpdate){
        string sSuccessMessage='';
        try{
            isDocumentRequired = false;
            ContractCreateRelatedObj.createRelatedObject(isUpdate,contractID);
            updateConLineItem(contractID);
            sSuccessMessage='Success';
        }
        catch(exception ex){
            sSuccessMessage=ex.getMessage();
        }
        return sSuccessMessage;
    }
    //Update Record Type for Contract Line Item -- Corporate Wireless Line Item
    public static void updateConLineItem(Id contractId)
    {
        List<vlocity_cmt__ContractLineItem__c> contractLineItemList = new List<vlocity_cmt__ContractLineItem__c>();
        List<vlocity_cmt__ContractLineItem__c> contractLineItemListUpdated = new List<vlocity_cmt__ContractLineItem__c>();

        //Query Line Item
        contractLineItemList = [Select Id, recordTypeId, RP_Included_Features_Other__c,Included_Features_All__c,RP_Included_Features_Select__c 
                                From vlocity_cmt__ContractLineItem__c where vlocity_cmt__ContractId__c = : contractId];
        
        for(vlocity_cmt__ContractLineItem__c conLineItemVar : contractLineItemList)
        {
            String features = conLineItemVar.RP_Included_Features_Select__c ==null? '' : conLineItemVar.RP_Included_Features_Select__c;
            String otherfeatures = conLineItemVar.RP_Included_Features_Other__c == null? '' : conLineItemVar.RP_Included_Features_Other__c; 
            conLineItemVar.Included_Features_All__c = features.replaceAll(';','\n') +'\n'+ otherfeatures;
            contractLineItemListUpdated.add(conLineItemVar);
        }
        
        update contractLineItemListUpdated;
        
    }
    //Update Deal Support
    public static void updateDealSupport(Id dealSupportId, Id contractId)
    {
        
        Offer_House_Demand__c dealSupportObj = new Offer_House_Demand__c(Id= dealSupportId);
        dealSupportObj.Contract__c = contractId;
        update dealSupportObj;
    }
    public static List<Contract> createRelatedObject(boolean isUpdate,Id contractID){
        system.debug('inside related Objects');
        map<string,string> mapSourceDestinationFieldsMap = new map<string,string>();
        List<Contract> updateContractNewList = new List<Contract> ();
        set<string> setContractFields=new set<string>{'Id', 'Name', 'Account.Name', 'Offer_House_Demand__c','Order_Fulfillment_Type__c','Telus_Affiliated_Accounts__c','vlocity_cmt__OriginalContractId__c','vlocity_cmt__OriginalContractId__r.Amendment_Count__c', 'vlocity_cmt__OriginalContractId__r.Status', 'Services__c','Services_All__c', 'Status'};      
            mapSourceDestinationFieldsMap=new map<string,string>();
        
        if(isUpdate == true && contractID != null){
            list<vlocity_cmt__CustomFieldMap__c> lstCustomFieldMap=[select vlocity_cmt__DestinationFieldName__c,vlocity_cmt__DestinationSObjectType__c,vlocity_cmt__SourceFieldName__c,
                                                                    vlocity_cmt__SourceFieldSObjectType__c  ,vlocity_cmt__SourceFieldType__c,vlocity_cmt__SourceSObjectType__c from vlocity_cmt__CustomFieldMap__c where vlocity_cmt__SourceSObjectType__c='Offer_House_Demand__c' and vlocity_cmt__DestinationSObjectType__c='Contract'];
            
            for(vlocity_cmt__CustomFieldMap__c objCustomFieldMap : lstCustomFieldMap){
                setContractFields.add(objCustomFieldMap.vlocity_cmt__DestinationFieldName__c);
                setContractFields.add('Offer_House_Demand__r.'+objCustomFieldMap.vlocity_cmt__SourceFieldName__c);
                mapSourceDestinationFieldsMap.put('Offer_House_Demand__r.'+objCustomFieldMap.vlocity_cmt__SourceFieldName__c,objCustomFieldMap.vlocity_cmt__DestinationFieldName__c);
            }    
        }
        
        list<string> lstContractFields=new list<string>();
        
        lstContractFields.addall(setContractFields);
        List<Contract> contractNewList = new List<Contract> ();
        List<String> lstContractFilter = new List<String>{'ID =: contractID'};
            String sContractQuery = Util.queryBuilder('Contract',lstContractFields, lstContractFilter);
        system.debug('\n--sContractQuery--'+sContractQuery);
        contractNewList = Database.query(sContractQuery);
		if(! isUpdate)
        {
       		
           //Call Method for Updating Contract on Deal Support
            if(contractNewList.size()>0)
            {
                updateDealSupport(contractNewList[0].Offer_House_Demand__c, contractNewList[0].Id);
            }
        }
        
        if(contractNewList.size()>0)
        {
            system.debug('\n--contractNewList--'+contractNewList);
            createRelatedRecords(contractNewList[0], isUpdate,mapSourceDestinationFieldsMap);
        }
        return contractNewList;
    }
    
    
    public static void createRelatedRecords(Contract contractObj, boolean isUpdate, map<string,string> sourceDestinationFieldsMap){
        
        if(sourceDestinationFieldsMap != null && sourceDestinationFieldsMap .size()>0 && !sourceDestinationFieldsMap.isEmpty())
            sourceDestFieldsMap_local = sourceDestinationFieldsMap;
        //Creating List of Related Object
        List<String> relatedObjectNameList = new List<String>();
        relatedObjectNameList.add('Subsidiary_Account__c');
        relatedObjectNameList.add('Agreement_Contact__c');
        relatedObjectNameList.add('Agreement_Dealer__c');
        relatedObjectNameList.add('Agreement_Hardware_Allotment__c');
        relatedObjectNameList.add('Agreement_Churn_Allotment__c');
        relatedObjectNameList.add('Agreement_Offer_Code__c');
        relatedObjectNameList.add('Agreement_Comment__c');
        relatedObjectNameList.add('Agreement_Billing_Hierarchy__c');
        
        //Calling Method to Link sub Account with Contract
        linkRelatedListItemsToSubAccount(contractObj);
        //Calling Method to Link related Object To Contract
        linkRelatedListItemsToContract(contractObj,relatedObjectNameList);
        //Calling method to populate Subsidary Account on Contract
        populateSubsidiaryAccounts(contractObj, isUpdate );
    }
    
    
    /*
	*  Method To Link sub Account with Contract
	*   
	*/
    public static void linkRelatedListItemsToSubAccount(Contract contractObj){
        
        Id dealId = contractObj.Offer_House_Demand__c;
        
        List<sobject> itemstoInsert = new List<sobject>();
        
        String[] relListtoLink = new String[]{'Agreement_Contact__c','Agreement_Dealer__c','Agreement_Comment__c'};
        string[] allRelListToDS = new String[]{'Agreement_Contact__c','Agreement_Dealer__c','Subsidiary_Account__c'};
        string sSubsidiaryAccountAPIName='Subsidiary_Account__c';
        
        string sChildQuery='';
        mapObjectWithRelName=new map<string,string>();
        mapObjectWithRelName.put('Agreement_Contact__c','DealSupport_Contacts__r');
        string sSubsidiaryAccountRelName='';
        
        Schema.ChildRelationship[] lstRelationships =   Schema.getGlobalDescribe().get('Offer_House_Demand__c').getDescribe().getChildRelationships();
        
        for(Schema.ChildRelationship objChildRelationship : lstRelationships){
            if(objChildRelationship.getChildSObject() == Schema.getGlobalDescribe().get(sSubsidiaryAccountAPIName)){
                sSubsidiaryAccountRelName=objChildRelationship.getRelationshipName();
                break;
            }
        }
        sChildQuery +=getChildQuery(allRelListToDS,lstRelationships);
    
        List<sOBject> lstDealSupport = Database.query('select id ' + sChildQuery +',Contract__c from Offer_House_Demand__c where id=:dealId');
        
        if(lstDealSupport != null && lstDealSupport.size() > 0){
            for(String relatedObjName : relListtoLink){
                
                if(mapObjectWithRelName.containsKey(relatedObjName)){
                    list<SObject> recordToUpdate = lstDealSupport[0].getSObjects(mapObjectWithRelName.get(relatedObjName));
                    
                    if(recordToUpdate != null)
                    {
                        for(sObject obj : recordToUpdate){
                            
                            if(mapObjectWithRelName.containsKey(sSubsidiaryAccountAPIName))
                            {
                                for(sOBject subAcc : lstDealSupport[0].getSObjects(mapObjectWithRelName.get(sSubsidiaryAccountAPIName))){
                                    map<string,schema.sobjectfield> fields=obj.getsobjecttype().getdescribe().fields.getmap();
                                    if(fields.containskey('Contract__c')){
                                        if(obj.get('Contract__c') == null ){
                                            sObject newObj = obj.clone(false, true);
                                            newObj.put('Subsidiary_Account__c',subAcc.Id);
                                            newObj.put('Deal_Support__c',null);
                                            //newObj.put('Agreement__c',null);
                                            itemstoInsert.add(newObj);
                                        }
                                    }
                                } 
                            }
                            
                        }
                    }
                }
            }
        }
        insert itemstoInsert;
    }
    
    /*
	*  Method To Link Related Object with Contract
	*   
	*/
    public static void linkRelatedListItemsToContract(Contract contractObj,List<String> relatedObjectList){
        
        id DealId = contractObj.Offer_house_demand__c;
        Map<id,sobject> itemsToUpdate = new  Map<id,sobject>();
        mapObjectWithRelName = new map<string, string> ();
        string sSubsidiaryAccountRelName='';
        schema.ChildRelationship[] lstRelationships = Schema.getGlobalDescribe().get('Offer_House_Demand__c').getDescribe().getChildRelationships();
        
        string sChildQuery =getChildQuery(relatedObjectList,lstRelationships);
        List<sOBject> lstDealSupport = Database.query('select id ' + sChildQuery +' from Offer_House_Demand__c where id=:dealId');
        
        if(lstDealSupport != null && lstDealSupport.size() > 0){
            system.debug('relatedObj::' + relatedObjectList);
            for(String relatedObjName : relatedObjectList){
                if(mapObjectWithRelName.containsKey(relatedObjName)){
                    list<SObject> recordToUpdate = lstDealSupport[0].getSObjects(mapObjectWithRelName.get(relatedObjName));
                    
                    if(recordToUpdate != null)
                    {
                                              
                        for(sObject obj : recordToUpdate){
                            obj.put('Contract__c',contractObj.Id);
                            if('Subsidiary_Account__c'.equalsIgnorecase(relatedObjName)){
                                obj.put('Order_Fulfillment_Type__c',contractObj.Order_Fulfillment_Type__c);
                            }else if((('Agreement_Contact__c'.equalsIgnorecase(relatedObjName) ||'Agreement_Dealer__c'.equalsIgnorecase(relatedObjName))) && obj.get('Subsidiary_Account__c')!= null){
                                obj.put('Contract__c',null);
                            }
                            itemsToUpdate.put(obj.id,obj);
                        } 
                    }
                    
                }
            }
        }
        if(itemsToUpdate.size() > 0 && ! itemsToUpdate.isEmpty() )
            update itemsToUpdate.values();
    }
    /*
	*  Method To populate Subsidiary Account with Contract
	*   
	*/
    public static void populateSubsidiaryAccounts(Contract contractObj, boolean  isUpdate){
        
        contractObj.Telus_Affiliated_Accounts__c ='';
        list<contract> contractsToUpdate = new list<contract>(); 
        if(isUpdate == false && contractObj != null && contractObj.vlocity_cmt__OriginalContractId__c != null 
           	&&  contractObj.vlocity_cmt__OriginalContractId__r.Amendment_Count__c !=null 
          	&& isDocumentRequired == true){
            contract mastercontract = new contract(id=contractObj.vlocity_cmt__OriginalContractId__c  );
            contractObj.Amendment_Count__c =contractObj.vlocity_cmt__OriginalContractId__r.Amendment_Count__c + 1; 
            mastercontract.Status = 'Amended';  
            contractsToUpdate.add(mastercontract);
           	system.debug('Master contract Status:' +  mastercontract.Status);
        }
        else if(isUpdate == false && contractObj != null && contractObj.vlocity_cmt__OriginalContractId__c != null 
           	&&  contractObj.vlocity_cmt__OriginalContractId__r.Amendment_Count__c !=null 
                && isDocumentRequired == false){
                    system.debug('Master contract Status for update:' +  contractObj.vlocity_cmt__OriginalContractId__r.status);
                   contractObj.Amendment_Count__c =contractObj.vlocity_cmt__OriginalContractId__r.Amendment_Count__c;  
                }
        Id dealId = contractObj.Offer_House_Demand__c;
        List<String> dealfilter = new List<String>{'Deal_Support__c =:dealId'};
            String query = Util.queryBuilder('Subsidiary_Account__c', new List<String>{'Account_Name__c'}, dealfilter);
        List<sOBject> objects = Database.query(query);
        for(sOBject obj : objects ){
            contractObj.Telus_Affiliated_Accounts__c +=obj.get('Account_Name__c') +'\n';
            
        }
	
        //Update contract header by referring to Field Mapper
        if(isUpdate)
            updateContractInfo(contractObj,  sourceDestFieldsMap_local);
        	
        if(isDocumentRequired == false){
           contractObj.status = 'Contract Registered'; 
        }
        String services = contractObj.Services__c == null ? '' : contractObj.Services__c ;
        string formattedSrvs = vlocity_addon_DynamicTableHelper_CP.formatMultiSelectfield(services);
        system.debug('formattedSrvs-->'+ formattedSrvs);
        contractObj.Services_All__c = formattedSrvs;
        contractObj.Name = contractObj.Account.Name + ' - ' + 'AGR';
        contractsToUpdate.add(contractObj);
        
        if(contractsToUpdate.size()>0 && !contractsToUpdate.isEmpty() )  {
            update contractsToUpdate;   
        } 
        
    }
    
    /*
	*  Method To get related object record for update
	*   
	*/
    public static List<sobject> getUpdateRecordForRelatedObject(String objectName, Id dealSupportId ){
        Id dealId = dealSupportId;
        List<String> dealfilter = new List<String>{'Deal_Support__c =:dealId'};
            String query = Util.queryBuilder(objectName,new List<String>{'Id', 'Contract__c'},dealfilter);
        List<sOBject> objects = Database.query(query);
        return objects;
    }
    
    public static string getChildQuery(List<String> relatedObjectList,Schema.ChildRelationship[] lstRelationships){
        string sChildQuery='';
        for(String relatedObjName : relatedObjectList){
            List<String> updatableFields = Util.getUpdatableFields(relatedObjName);
            Schema.SObjectType childType  = Schema.getGlobalDescribe().get(relatedObjName);
            string sChildObjectName='';
           
            for(Schema.ChildRelationship objChildRelationship : lstRelationships){
              
                if(objChildRelationship.getChildSObject() == childType){
                    mapObjectWithRelName.put(relatedObjName,objChildRelationship.getRelationshipName());
                    sChildObjectName=objChildRelationship.getRelationshipName();
                    break;
                }
            }
            if(sChildObjectName != ''){
                sChildQuery += ',(select ' + string.join(updatableFields,',') + ' from ' + sChildObjectName +' )';
                system.debug('Child Query:: ' + sChildQuery);
            }
        }
        return sChildQuery;
    }
    
    public static Sobject updateContractInfo(Sobject objContract, map<string,string> sourceDestMap){
        system.debug('Update Cusom Mapping::');

        for(string sSourceField : sourceDestMap.keyset()){
            if(sSourceField.contains('__r')){
                list<string> objTempName = sSourceField.split('\\.');
                if(objTempName != null && objTempName.size()>0){
                    
                    if(objTempName.size()==4 && objContract.getSobject(objTempName[0])!=null && objContract.getSobject(objTempName[0]).getSobject(objTempName[1])!=null &&  objContract.getSobject(objTempName[0]).getSobject(objTempName[1]).getSobject(objTempName[2])!=null &&  objContract.getSobject(objTempName[0]).getSobject(objTempName[1]).getSobject(objTempName[2]).get(objTempName[3])!=null && objContract.getSobject(objTempName[0]).getSobject(objTempName[1]).getSobject(objTempName[2]).get(objTempName[3])!='')
                    {
                        objContract.put(sourceDestMap.get(sSourceField),objContract.getSobject(objTempName[0]).getSobject(objTempName[1]).getSobject(objTempName[2]).get(objTempName[3]));
                    }
                    else if(objTempName.size()==3 && objContract.getSobject(objTempName[0]) != null && objContract.getSobject(objTempName[0]).getSobject(objTempName[1]) != null && objContract.getSobject(objTempName[0]).getSobject(objTempName[1]).get(objTempName[2]) != null && objContract.getSobject(objTempName[0]).getSobject(objTempName[1]).get(objTempName[2]) != '')
                    {
                        objContract.put(sourceDestMap.get(sSourceField),objContract.getSobject(objTempName[0]).getSobject(objTempName[1]).get(objTempName[2]));
                    }
                    else if(objTempName.size()==2 && objContract.getSobject(objTempName[0]) != null && objContract.getSobject(objTempName[0]).get(objTempName[1]) != null && objContract.getSobject(objTempName[0]).get(objTempName[1]) != '')
                    {
                        objContract.put(sourceDestMap.get(sSourceField),objContract.getSobject(objTempName[0]).get(objTempName[1]));
                    }
                    else
                        objContract.put(sourceDestMap.get(sSourceField),null);
                }
            }
        }
        return objContract;
    }
    
    
}