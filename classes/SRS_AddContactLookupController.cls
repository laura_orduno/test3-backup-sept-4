public with sharing class SRS_AddContactLookupController {

    public Contact contact {get;set;}
    public Service_Request_Contact__c srContact {get;set;}
    public Id serReqId{get;set;}
    public Id accId {get;set;}  
    public Boolean renderShowandCancel{get;set;}
    public SRS_AddcontactLookupController() {
       //contact = new Contact();
      // flag = true;
      serReqId = ApexPages.currentPage().getParameters().get('srId'); 
      system.debug('===serReqId==='+serReqId);
      srContact = new Service_Request_Contact__c();
      if(serReqId!=Null){
            accId = [Select Id, Account_Name__c From Service_Request__c Where Id =: serReqId Limit 1].Account_Name__c;
            system.debug('-----accId-----'+accId);  
      }
    }
    public void init()
    {
        serReqId = ApexPages.currentPage().getParameters().get('srId'); 
        renderShowandCancel= true;
      if(serReqId!=Null)
          accId = [Select Id, Account_Name__c From Service_Request__c Where Id =: serReqId Limit 1].Account_Name__c;
    }
    public PageReference cancel() {
        PageReference pageRef = new PageReference('/'+serReqId);
       // flag = false;
        return pageRef;
    }

    public PageReference saveAndNew(){
      if(serReqId!=Null){
            srContact.Service_Request__c = serReqId;
            try{
                insert srContact;
                system.debug('@@@@@srContact'+srContact);
            }catch(Exception e){ApexPages.addMessages(e);}
            system.debug('-=-=serReqId-=-=-'+serReqId);
            if(srContact.Id != Null){
                srContact = new Service_Request_Contact__c();
              //  return new PageReference('/apex/AddContactLookup?srId=' + serReqId);
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, 'New contact saved successfully.'));                
                return null;
                
            }else{
                return null;
            }
        }else{
            return null;
        }
        
    }
    public PageReference save(){                
        system.debug('@@@@@srContact'+srContact);
        system.debug('@@@@@@serReqId'+serReqId);
        if(serReqId!=Null){
            srContact.Service_Request__c = serReqId;
            try{
                insert srContact;
                system.debug('@@@@@srContact'+srContact);
            }catch(Exception e){ApexPages.addMessages(e);}
            if(srContact.Id != Null){
             //   return new PageReference('/' + serReqId);
            // srContact = new Service_Request_Contact__c();
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, 'New contact saved successfully.'));
             renderShowandCancel= false;
             return null;
            }else{
                return null;
            }
        }else{
            return null;
        }    
    }
 
    
}