public class AccountFlagsHelper {
    private final Account acctObj;
    
    public string AccountRecordId {get; set;}
    public string VOCRecordId {get; set;}
    public string CaseRecordId {get; set;}
    public string VOCRecordName {get; set;}
    public string CaseRecordNumber {get; set;}
    public string CaseRecordURL {get; set;}
    
    public AccountFlagsHelper(ApexPages.StandardController stdController) {
        this.acctObj = (Account)stdController.getRecord();
        AccountRecordId = this.acctObj.Id;
        VOCRecordId = getVOCSurveyIdFromAccount(this.acctObj.Id,VOCRecordName);
        CaseRecordId = getProactiveCaseIdForAccount(this.acctObj.Id,CaseRecordNumber);
        setCaseURL();
    }
    
    public void setCaseURL() {
        if(CaseRecordId != '') {
            CaseRecordURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + CaseRecordId;
        }
    }
    
    public string getVOCSurveyIdFromAccount(Id accountId, string altName) {
        if (accountId == null) {
            return '';
        }
        try {
            List<VOC_Survey__c> tempList = [SELECT Id, Name FROM VOC_Survey__c
                                            WHERE Account__c = :accountId and SVOC_Status_Icon__c != NULL
                                            ORDER BY Received_Date__c DESC Limit 1];
            
            if(tempList.size()>0 && tempList[0].id != null){
                altName = tempList[0].Name;
                return Id.valueof(tempList[0].id);
            }
        }
        catch(queryexception qe) {
            System.debug('AccountFlagsHelper:getVOCSurveyIdFromAccount():queryexception = '+qe.getmessage());
        }
        return '';
    }
    
    public string getProactiveCaseIdForAccount(Id accountId, string altName) {
        if (accountId == null) {
            return '';
        }
        try {
            List<Case> tempList = [SELECT Id, Subject FROM Case
                                   WHERE Accountid = :accountId and Proactive_Case__c = true and Status != 'Closed'
                                   ORDER BY createddate DESC Limit 1];
            if(tempList.size()>0 && tempList[0].id != null){
                altName = tempList[0].Subject;
                return Id.valueof(tempList[0].id);
            }
        }
        catch(queryexception qe) {
            System.debug('AccountFlagsHelper:getProactiveCaseIdForAccount():queryexception = '+qe.getmessage());
        }
        return '';
    }
}