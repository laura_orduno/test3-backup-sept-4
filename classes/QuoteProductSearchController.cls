public without sharing class QuoteProductSearchController {
    private static final Map<String,Id> PRICEBOOK_MAP = new Map<String,Id>{
        'A-D_AB' => '01s40000000F0kf',
        'F_AB' => '01s40000000F0kp',
        'E,G_AB' => '01s40000000F0kk',
        'A-D_BC' => '01s40000000F0ku',
        'F_BC' => '01s40000000F0l4',
        'E,G_BC' => '01s40000000F0kz'
    };
    
    public String rateBand {get; set;}
    public String province {get; set;}
    public String locationId {get; set;}
    public List<ProductWrapper> products {get; set;}
    public String selectedProductId {get; set;}
    public SObjectListOptionModel locations {get; private set;}
    public Boolean addOn {get; private set;}
    
    public Boolean isDealer {get {return QuotePortalUtils.isUserDealer();}}
    
    private Id pricebookId;
    private Id quoteGroupId;
    private Boolean multiSite;
    private Id accountId;
    
    public QuoteProductSearchController() {
        quoteGroupId = ApexPages.currentPage().getParameters().get('qg');
        String ms = ApexPages.currentPage().getParameters().get('ms');
        multiSite = (ms != null) && ms.equals('1');
        String ao = ApexPages.currentPage().getParameters().get('addon');
        addOn = (ao != null) && ao.equals('1');
        accountId = ApexPages.currentPage().getParameters().get('aid');
        if (accountId != null) {
            locations = new SObjectListOptionModel([SELECT Id, Special_Location__c FROM Address__c WHERE Web_Account__c = :accountId], 'Id', 'Special_Location__c');
        }
    }
    
    public Integer getProductCount() {
        if (products == null) {
            return 0;
        }
        return products.size();
    }
    
    public PageReference onSearch() {
        String bundleType = 'New';
        if (addOn) {
            Address__c loc = [SELECT Rate_Band__c, State_Province__c FROM Address__c WHERE Id = :locationId];
            rateBand = loc.Rate_Band__c;
            province = loc.State_Province__c;
            bundleType = 'Existing';
        }
        
        pricebookId = mapPricebook(rateBand, province);
        products = new List<ProductWrapper>();
        for (PriceBookEntry entry :
                [SELECT UnitPrice, Product2.Name, Product2.Description, Product2.SBQQ__Specifications__c
                 FROM PricebookEntry WHERE IsActive = true AND Pricebook2Id = :pricebookId AND 
                    Product2.IsActive = true AND Product2.SBQQ__Component__c = false AND Product2.Bundle_Type__c = :bundleType
                 ORDER BY Product2.Display_Order__c]) {
            products.add(new ProductWrapper(entry));
        }
        return null;
    }
    
    public PageReference onContinue() {
        String oppName = 'BusinessAnywhere Bundle';
        if (addOn) {
            oppName = oppName + ' (Add-On)';
        }
        Opportunity opp = new Opportunity(Name=oppName);
        opp.CloseDate = System.today();
        opp.Pricebook2Id = pricebookId;
        opp.StageName = 'Prospecting';
        if (addOn) {
            opp.Web_Account__c = accountId;
        }
        insert opp;
        
        if (multiSite && (quoteGroupId == null)) {
            Quote_Group__c grp = new Quote_Group__c();
            insert grp;
            quoteGroupId = grp.Id;
        }
        
        SBQQ__Quote__c quote = new SBQQ__Quote__c(SBQQ__Opportunity__c=opp.Id);
        quote.Group__c = quoteGroupId;
        quote.Rate_Band__c = rateBand;
        quote.Province__c = province;
        quote.Channel_Outlet_ID__c = QuotePortalUtils.getChannelOutLetId();
        if (addOn) {
            quote.SBQQ__Type__c = 'Add-On';
        }
        
        if (QuotePortalUtils.isAssignedAccountsUser()) {
            quote.Assigned_Account_Order__c = 'Yes';
        }
        if (addOn) {
            Web_Account__c account =
                [SELECT Name, Billing_Address__c, Billing_City__c, Billing_Province__c, Billing_Postal_Code__c, 
                 Shipping_Address__c, Shipping_City__c, Shipping_Province__c, Shipping_Postal_Code__c
                 FROM Web_Account__c WHERE Id = :accountId];
            QuotePortalUtils.updateQuoteAddress(account, locationId, quote);
        }
        quote.Site_URL__c = Site.getCurrentSiteUrl();
        insert quote;
        
        PageReference pref = Page.SiteConfigureProducts;
        pref.getParameters().put('pids', selectedProductId);
        pref.getParameters().put('qid', quote.Id);
        pref.getParameters().put('step', 'step2');
        if (multiSite) {
            pref.getParameters().put('ms', '1');
            if(accountId != null) {
            	pref.getParameters().put('aid', accountId); // Traction - GA - 2013/05/14 - MultiSite Account Search Fix
            }
        }
        if (addOn) {
            pref.getParameters().put('addon', '1');
        }
        QuotePortalUtils.addConfigurationSummaryParams(pref);
        pref.getParameters().put('retURL', Site.getCurrentSiteUrl() + Page.QuoteHome.getUrl().replaceAll('/apex/',''));
        if (addOn) {
            pref.getParameters().put('saveURL', Site.getCurrentSiteUrl() + Page.QuoteWizardContact.getUrl().replaceAll('/apex/','') + '?qid=' + quote.Id + '&step=step3' + (addon ? '&addon=1' : ''));
        } else {
            pref.getParameters().put('saveURL', Site.getCurrentSiteUrl() + Page.QuoteWizardAccount.getUrl().replaceAll('/apex/','') + '?qid=' + quote.Id + '&step=step3' + (multiSite ? '&ms=1' + (accountId != null? '&aid=' + accountId : '') : ''));
        }
        pref.setRedirect(true);
        return pref;
    }
    
    public static Id mapPricebook(String rateBand, String province) {
        return PRICEBOOK_MAP.get(rateBand + '_' + province);
    }
    
    public class ProductWrapper {
        private PriceBookEntry entry;
        public String id {get{return entry.Product2.Id;}}
        public String name {get{return entry.Product2.Name;}}
        public String description {
            get {
                if (entry.Product2.Description != null) {
                    return entry.Product2.Description.replaceAll('\n','<br/>');
                }
                return null;
            }
        }
        public String specifications {get{return entry.Product2.SBQQ__Specifications__c;}}
        
        public ProductWrapper(PriceBookEntry entry) {
            this.entry = entry;
        }
        
    }
}