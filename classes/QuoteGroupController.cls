public without sharing class QuoteGroupController {
    class NullIdException extends Exception {}
    public List<QuoteVO> quotes {get; private set;}
    public Quote_Group__c quoteGroup {get; private set;}
    public Web_Account__c account {get; private set;}
    public String markup {get; private set;}
    public List<ContactVO> contacts {get; private set;}
    public String selectedContactId {get; set;}
    public Boolean sendAgreementDisabled {get{return isSendAgreementDisabled();}}
    public Boolean creditCheckDisabled {get{return isCreditCheckDisabled();}}
    public Boolean dealerUser {get{return QuotePortalUtils.isUserDealer();}}
    
    /* New in R5.1! Agreements are stored in a seperate object! */
    public List<AgreementVO> agreements {get; private set;}
    
    private String returnURL;
    private List<Id> quoteIds;
    private Id groupId;
    
    public QuoteGroupController() {
        try {
            groupId = ApexPages.currentPage().getParameters().get('gid');
            returnURL = ApexPages.currentPage().getParameters().get('returnURL');
            
            if (groupId == null) {
                throw new NullIdException('Group ID cannot be null');
            }
            quoteGroup = [SELECT Id, Status__c, Credit_Check_Requested__c, Agreement_URL__c, Customer_Contact__r.Web_Account__c FROM Quote_Group__c WHERE Id = :groupId];
            
            
            quotes = new List<QuoteVO>();
            quoteIds = new List<Id>();
            for (SBQQ__Quote__c q : 
                [SELECT SBQQ__Status__c, Name, CreatedDate, SBQQ__ExpirationDate__c, 
                    CreatedBy.Name, LastModifiedBy.Name, LastModifiedDate, Group__c,
                    Install_Location__r.Special_Location__c, Install_Location__r.Street__c,
                    Install_Location__r.State_Province__c, Install_Location__r.Street_Number__c,
                    Install_Location__r.Street_Direction__c
                 FROM SBQQ__Quote__c WHERE Group__c = :groupId AND Completed__c = true]) {
                quotes.add(new QuoteVO(q));
                quoteIds.add(q.Id);
            }
            
            Id accountId = quoteGroup.Customer_Contact__r.Web_Account__c;
            account = [SELECT Id, Name, Phone__c, Fax__c, Company_Legal_Name__c, RCID__c,
                    DUNS__c, CBUCID__c, Language_Preference__c, Credit_Check_Status__c,
                    Credit_Reference__c, TLC_EDUF_Charges_Apply__c
                 FROM Web_Account__c 
                 WHERE Id = :accountId];
                 
            agreements = new List<AgreementVO>();
            for (Agreement__c a : [Select Name, Sent_Date__c, Closed_Date__c, Signed_Date__c, URL__c, Expiry_Date__c, Quote_Group__c, Recipient__c, Type__c, Waybill_ID__c, Status__c, CreatedDate, CreatedById From Agreement__c Where Quote_Group__c = :groupId Order By Name Desc]) {
                // Add the agreement record into the AgreementVO list
                agreements.add(new AgreementVO(a));
            }
        } catch (Exception e) {QuoteUtilities.logNow(e);}
    }
    
    public Boolean isSendAgreementDisabled() {
        return (quoteGroup.Agreement_URL__c != null && !QuotePortalUtils.isExperienceExcellenceUser() && quoteGroup.Status__c != 'Sent agreement, waiting acceptance') || // Traction change for TC12020
            quoteGroup.Status__c == 'Customer Signed Agreement in Person' || 
            quoteGroup.Status__c == 'Client Declined' ||
            (account.Credit_Check_Status__c != 'Submitted' && account.Credit_Reference__c == null) ||
            (account.Credit_Check_Status__c != 'Submitted' && account.Credit_Check_Status__c != 'Approved' && account.Credit_Check_Status__c != 'Pre-Approved Credit');
    }
    
    public Boolean isCreditCheckDisabled() {
        return quoteGroup.Credit_Check_Requested__c || 
            (account.Credit_Check_Status__c != 'Submitted' && account.Credit_Reference__c == null) ||
            (account.Credit_Check_Status__c != 'Submitted' && account.Credit_Check_Status__c != 'Approved' && account.Credit_Check_Status__c != 'Pre-Approved Credit');
    }
    
    public PageReference onSendAgreement() {
        try {
            // Traction addition for QTR5 Req 10 + 11
            // Allowing Account & Contact fields to be optional until later in the quote process
            SBQQ__Quote__c[] quotesCheck = [SELECT Id FROM SBQQ__Quote__c WHERE Group__c = :groupId AND Completed__c = true];
            for (SBQQ__Quote__c quote : quotesCheck) {
                if (!QuoteUtilities.isAccountValid(account.Id, quote.Id, getBackURL(), 'agreement')) { if(!Test.isRunningTest()){return null;} }
                if (!QuoteUtilities.isContactValid(account.Id, quote.Id, getBackURL())) { if(!Test.isRunningTest()){return null;} }
                if (!QuoteCustomConfigurationService.isRequiredAdditionalInformationFilled(quote.Id, QuoteStatus.SENT_AGREEMENT)){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'There is required data that needs to be filled before you can send the agreement. Click <a href="QuoteCompleteInformation?qid='  + quote.id + '&stage=' + EncodingUtil.urlEncode(QuoteStatus.SENT_AGREEMENT, 'UTF-8') + '&retUrl=' + EncodingUtil.urlEncode(getBackURL(), 'UTF-8') + '">here </a> to complete the data.'));
                    return null;
                }
            }
            
            PageReference pref = Page.SendQuoteAgreement;
            pref.getParameters().put('qids', getQuoteIds());
            pref.getParameters().put('aid', account.Id);
            pref.getParameters().put('gid', groupId);
            pref.getParameters().put('retURL', getBackURL());
            pref.setRedirect(true);
            return pref;
        } catch (Exception e) {QuoteUtilities.log(e);} return null;
    }
    
    public PageReference onCheckCredit() {
        if (!quotePortalUtils.isUserDealer()) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Fatal, 'Agents must use the Credit Portal to obtain a Credit Reference Number. It can be entered on the Account details page.'));
            if (!Test.isRunningTest()) { return null; }
        }
        // Traction addition for QTR5 Req 10 + 11
        // Allowing Account & Contact fields to be optional until later in the quote process
        for (SBQQ__Quote__c quote : [SELECT Id FROM SBQQ__Quote__c WHERE Group__c = :groupId AND Completed__c = true]) {
            if (!QuoteUtilities.isAccountValid(account.Id, quote.Id, getBackURL())) { if(!Test.isRunningTest()){return null;} }
            if (!QuoteUtilities.isContactValid(account.Id, quote.Id, getBackURL())) { if(!Test.isRunningTest()){return null;} }
            if (!QuoteCustomConfigurationService.isRequiredAdditionalInformationFilled(quote.Id, QuoteStatus.SENT_FOR_CREDIT_CHECK)){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'You must complete all required fields on the Product Details page before you can continue. <a href="QuoteCompleteInformation?qid=' + quote.id + '&stage=' + EncodingUtil.urlEncode(QuoteStatus.SENT_FOR_CREDIT_CHECK, 'UTF-8') + '&retUrl=' + EncodingUtil.urlEncode(getBackURL(), 'UTF-8')  + '">Click here</a> to complete required fields.'));
                return null;
            }
        }
        
        try {
            update new Quote_Group__c(Id = groupId, Credit_Check_Requested__c = true);
            SBQQ__Quote__c[] quotes = [SELECT Id FROM SBQQ__Quote__c WHERE Group__c = :groupId AND Completed__c = true];
            for (SBQQ__Quote__c quote : quotes) {
                quote.Credit_Check_Requested__c = true;
            }
            update quotes;
        } catch (Exception e) {
            return null;
        }
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Quotes sent for credit check'));
        PageReference pref = new PageReference(getBackURL());
        pref.setRedirect(true);
        return pref;
    }
    
    public String getBackUrl() {
        PageReference pref = Page.QuoteGroupDetail;
        pref.getParameters().put('gid', groupId);
        pref.getParameters().put('aid', account.Id);
        pref.getParameters().put('ms', '1');
        pref.getParameters().put('step', 'step6');
        return pref.getUrl();
    }
    
    public String getQuoteIds() {
        return StringUtils.join(quoteIds, ',',null);
    }
}