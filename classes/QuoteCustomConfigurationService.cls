public without sharing class QuoteCustomConfigurationService {

    private static final Map<String, List<QuoteConfigFieldVO>> fieldVOMap = new Map<String, List<QuoteConfigFieldVO>>();
    private static final Map<String, Integer> requiredOrdinalByStatus = new Map<String, Integer>();
    private static final String defaultErrorMessage = Label.QuoteCustomConfigurationDefaultErrorMessage;
    
    
    static {
        Map<String, Schema.SObjectField> addlFieldsMap = Schema.SObjectType.AdditionalInformation__c.fields.getMap();
        
        ////load fields configuration
        //Wireless
        List<QuoteConfigFieldVO> fieldVOList = new List<QuoteConfigFieldVO>();
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('First_Name__c'), 1, QuoteStatus.Sent_For_Credit_Check, false, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Last_Name__c'), 2, QuoteStatus.Sent_For_Credit_Check, false, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('City_of_Use__c'), 3, QuoteStatus.Sent_For_Credit_Check, false, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Province__c'), 4, QuoteStatus.Sent_For_Credit_Check, false, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('ESN_IMEI__c'), 5, ''/*QuoteStatus.Accepted_Pending_Dealer_Input*/, false, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('SIM__c'), 6, ''/*QuoteStatus.Accepted_Pending_Dealer_Input*/, false, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Mobile__c'), 7, QuoteStatus.Sent_For_Credit_Check, false, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Old_Service_Provider__c'), 8, QuoteStatus.Sent_For_Credit_Check, false, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Porting__c'), 9, QuoteStatus.Sent_For_Credit_Check, false, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Old_Service_Provider_Account_Number__c'), 10, QuoteStatus.Sent_For_Credit_Check, false, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Special_Instructions__c'), 11, '', true, 'height: 50px; width: 200px;', defaultErrorMessage));
        fieldVOMap.put('Wireless', fieldVOList);
        
        //Existing Device
        fieldVOList = new List<QuoteConfigFieldVO>();
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('First_Name__c'), 1, QuoteStatus.Sent_For_Credit_Check, true, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Last_Name__c'), 2, QuoteStatus.Sent_For_Credit_Check, true, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('City_of_Use__c'), 3, QuoteStatus.Sent_For_Credit_Check, true, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Province__c'), 4, QuoteStatus.Sent_For_Credit_Check, true, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Mobile__c'), 5, QuoteStatus.Sent_For_Credit_Check, false, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Special_Instructions__c'), 6, '', true, 'height: 50px; width: 200px;', defaultErrorMessage));
        fieldVOMap.put('Existing Device', fieldVOList);
        
        //Office Phone
        fieldVOList = new List<QuoteConfigFieldVO>();
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Phone_Number_Setup__c'), 1, QuoteStatus.Draft, true, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Directory_Listing__c'), 2, QuoteStatus.Sent_For_Credit_Check, false, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Directory_Listing_Same_As_Business_Name__c'), 3, QuoteStatus.Sent_For_Credit_Check, false, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Directory_Listing_Display__c'), 4, QuoteStatus.Sent_For_Credit_Check, false, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Destination_Number__c'), 5, '', false, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Billing_Telephone_Number_BTN__c'), 6, QuoteStatus.Sent_For_Credit_Check, false, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('CRIS_Order_Number__c'), 7, '', false, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Special_Instructions__c'), 8, '', true, 'height: 50px; width: 200px;', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Street__c'), 8, QuoteStatus.Sent_For_Credit_Check, false, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('City__c'), 9, QuoteStatus.Sent_For_Credit_Check, false, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Province__c'), 10, QuoteStatus.Sent_For_Credit_Check, false, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Postal_Code__c'), 11, QuoteStatus.Sent_For_Credit_Check, false, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Porting__c'), 12, QuoteStatus.Sent_For_Credit_Check, false, '', defaultErrorMessage));
        
        fieldVOMap.put('Office Phone', fieldVOList);
        
        //Shared Hosting
        fieldVOList = new List<QuoteConfigFieldVO>();
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Website_is__c'), 1, QuoteStatus.Sent_For_Credit_Check, true, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Existing_Domain_Name__c'), 2, '', true, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('New_Domain_Name_Option_1__c'), 3, '', true, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('New_Domain_Name_Option_2__c'), 4, '', true, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('New_Domain_Name_Option_3__c'), 5, '', true, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Special_Instructions__c'), 6, '', true, 'height: 50px; width: 200px;', defaultErrorMessage));
        fieldVOMap.put('Shared Hosting', fieldVOList);
        
        //Toll Free
        fieldVOList = new List<QuoteConfigFieldVO>();
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Toll_Free__c'), 1, QuoteStatus.Draft, true, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Toll_Free_Terminating_Number__c'), 2, QuoteStatus.Sent_For_Credit_Check, true, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Existing_Toll_Free_Number__c'), 3, QuoteStatus.Draft, true, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Specify_Existing_Terminating_Number__c'), 4, '', true, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Toll_Free_Coverage_Includes__c'), 5, QuoteStatus.Sent_For_Credit_Check, true, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Specify_Toll_Free_provinces_and_or_state__c'), 6, '', true, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Toll_Free_Number_in_Directory_Assistance__c'), 7, QuoteStatus.Sent_For_Credit_Check, true, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Name_to_appear_in_Directory_Assistance__c'), 8, '', true, '', defaultErrorMessage));
        fieldVOMap.put('Toll Free', fieldVOList);

        //Unified Communications
        fieldVOList = new List<QuoteConfigFieldVO>();
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Customer_requests_custom_Domain_Name__c'), 1, QuoteStatus.Sent_For_Credit_Check, true, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Domain__c'), 2, '', true, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('A_Record_For_Workspace_Only__c'), 3, '', true, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('User_Has_Existing_e_mail_Address__c'), 4, '', true, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Wish_to_Mantain_Existing_e_mail_Seats__c'), 5, '', true, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Special_Instructions__c'), 6, '', true, 'height: 50px; width: 200px;', defaultErrorMessage));
        fieldVOMap.put('Unified Communications', fieldVOList);

        //Internet Fax
        fieldVOList = new List<QuoteConfigFieldVO>();
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Special_Instructions__c'), 1, '', true, 'height: 50px; width: 200px;', defaultErrorMessage));
        fieldVOMap.put('Internet Fax', fieldVOList);
        
        //Computer Backup
        fieldVOList = new List<QuoteConfigFieldVO>();
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Email__c'), 1, QuoteStatus.Sent_For_Credit_Check, true, '', defaultErrorMessage));
        fieldVOList.add(new QuoteConfigFieldVO(addlFieldsMap.get('Special_Instructions__c'), 1, '', true, 'height: 50px; width: 200px;', defaultErrorMessage));
        fieldVOMap.put('Computer Backup', fieldVOList);
                
        Schema.DescribeFieldResult quoteStatusField = SBQQ__Quote__c.SBQQ__Status__c.getDescribe();
        List<Schema.PicklistEntry> quoteStatusList = quoteStatusField.getPicklistValues();
        Integer ordinal = 1;
        for(Schema.PicklistEntry ple : quoteStatusList) {
            requiredOrdinalByStatus.put(ple.getValue(), ordinal);
            ordinal++;
        }
        
    }   
    
    public static List<QuoteConfigFieldVO> listQuoteConfigFieldsByProductCategory(String productCategory) {
        return fieldVOMap.get(productCategory);
    }

    public static Integer getRequiredOrdinalByStatus(String quoteStatus){
        return requiredOrdinalByStatus.get(quoteStatus);
    }
    
    public static List<AdditionalInformation__c> loadAdditionalData(Id quoteId){
        QueryBuilder qb = new QueryBuilder(AdditionalInformation__c.sObjectType);
        for (Schema.SObjectField field : Schema.SObjectType.AdditionalInformation__c.fields.getMap().values()) {
            qb.getSelectClause().addField(field);
        }
        qb.getSelectClause().addField(AdditionalInformation__c.Quote_Line__r.SBQQ__Product__r.Name);
        qb.getSelectClause().addField(AdditionalInformation__c.Quote_Line__c, SBQQ__QuoteLine__c.SBQQ__Product__c, Product2.Network__c);
        qb.getSelectClause().addField(AdditionalInformation__c.Quote_Line__c, SBQQ__QuoteLine__c.SBQQ__Product__c, Product2.Configuration_Category__c);
        qb.getWhereClause().addExpression(qb.eq(AdditionalInformation__c.Quote__c, quoteId));
        return Database.query(qb.buildSOQL());
    }
    
    public static List<SBQQ__QuoteLine__c> loadQuoteLines(Id quoteId) {
        return [SELECT  Id, SBQQ__Quote__c, SBQQ__Quote__r.SBQQ__Status__c, SBQQ__Product__r.Configuration_Category__c, SBQQ__Quantity__c, 
                        SBQQ__Product__r.Name, Selection_Id__c, SBQQ__Product__r.Network__c, SBQQ__ProductOption__r.SBQQ__Feature__r.Name,
                        SBQQ__ProductOption__r.SBQQ__Feature__r.SBQQ__Number__c, SBQQ__ProductOption__r.SBQQ__OptionalSKU__r.Network__c,
                        SBQQ__ProductOption__r.SBQQ__Number__c
                FROM    SBQQ__QuoteLine__c ql 
                WHERE   SBQQ__Product__r.Configuration_Category__c != null AND 
                        SBQQ__Quote__c = :quoteId
                ORDER BY SBQQ__ProductOption__r.SBQQ__Feature__r.SBQQ__Number__c, SBQQ__ProductOption__r.SBQQ__Number__c];
    }

    public static Map<SBQQ__QuoteLine__c, List<QuoteConfigFieldSetModel>> buildQuoteConfigFieldSetModelByQuoteLine(String quoteStatus, List<AdditionalInformation__c> persistedAddlList, List<SBQQ__QuoteLine__c> quoteLines) {
        Map<Id, List<AdditionalInformation__c>> existingAddlByQuoteLine = additionalInformationListToMap(persistedAddlList);
        Map<SBQQ__QuoteLine__c, List<QuoteConfigFieldSetModel>> allAddlByQuoteLine = new Map<SBQQ__QuoteLine__c, List<QuoteConfigFieldSetModel>>();
        AdditionalInformation__c addlInfo;
        for(SBQQ__QuoteLine__c ql : quoteLines) {
            String productCategory = ql.SBQQ__Product__r.Configuration_Category__c;
            List<QuoteConfigFieldVO> quoteConfigFieldVOList = listQuoteConfigFieldsByProductCategory(productCategory);
            List<QuoteConfigFieldSetModel> fieldSetModelList = allAddlByQuoteLine.get(ql);
            if(fieldSetModelList == null) {
                fieldSetModelList = new List<QuoteConfigFieldSetModel>();
                allAddlByQuoteLine.put(ql, fieldSetModelList);
            }
            List<AdditionalInformation__c> existingAddlRecords = existingAddlByQuoteLine.get(ql.Id); 
            Integer index = 0;
            if(existingAddlRecords != null) {
                for(AdditionalInformation__c addl : existingAddlRecords){
                    fieldSetModelList.add(new QuoteConfigFieldSetModel(index, addl, quoteConfigFieldVOList, productCategory, quoteStatus, ql.SBQQ__ProductOption__r.SBQQ__OptionalSKU__r));
                    index++;
                }
            }
            while(ql.SBQQ__Quantity__c != null && ql.SBQQ__Quantity__c > fieldSetModelList.size()) {
                addlInfo = new AdditionalInformation__c();
                addlInfo.Quote__c = ql.SBQQ__Quote__c;
                addlInfo.Quote_Line__c = ql.Id;
                addlInfo.Quote_Line__r = ql;
                addlInfo.Selection_Id__c = ql.Selection_Id__c;
                fieldSetModelList.add(new QuoteConfigFieldSetModel(index, addlInfo, quoteConfigFieldVOList, productCategory, quoteStatus, ql.SBQQ__ProductOption__r.SBQQ__OptionalSKU__r));
                index++;
            }
        }
        
        return allAddlByQuoteLine;
    }
    
    private static Map<Id, List<AdditionalInformation__c>> additionalInformationListToMap(List<AdditionalInformation__c> additionalInformationList) {
        Map<Id, List<AdditionalInformation__c>> addlByQuoteLine = new Map<Id, List<AdditionalInformation__c>>();
        for(AdditionalInformation__c addlInfo : additionalInformationList) {
            List<AdditionalInformation__c> addlList = addlByQuoteLine.get(addlInfo.Quote_Line__r.Id);
            if(addlList == null) {
                addlList = new List<AdditionalInformation__c>();
                addlByQuoteLine.put(addlInfo.Quote_Line__r.Id, addlList);
            }
            addlList.add(addlInfo);
        }
        return addlByQuoteLine;
    }
    
    public static Boolean isRequiredAdditionalInformationFilled(Id quoteId, String quoteStatus) { 
        List<AdditionalInformation__c> additionalInformationList = loadAdditionalData(quoteId);
        Map<Id, List<AdditionalInformation__c>> existingAddlByQuoteLine = additionalInformationListToMap(additionalInformationList);
        List<SBQQ__QuoteLine__c> quoteLines = loadQuoteLines(quoteId); 
        List<QuoteConfigFieldVO> quoteConfigFieldVOList;
        List<AdditionalInformation__c> quoteLinesAddlList;
        Boolean valid = true;
        Integer quoteStatusOrdinal = requiredOrdinalByStatus.get(quoteStatus);
        Integer stageOrdinal;
        String productCategory;
        for (SBQQ__QuoteLine__c ql : quoteLines) {
            productCategory = ql.SBQQ__Product__r.Configuration_Category__c;
            quoteConfigFieldVOList = listQuoteConfigFieldsByProductCategory(productCategory);
            quoteLinesAddlList = existingAddlByQuoteLine.get(ql.Id);
            if (quoteLinesAddlList != null) {
                if (ql.SBQQ__Quantity__c != null && quoteLinesAddlList.size() < ql.SBQQ__Quantity__c) {
                    for (QuoteConfigFieldVO quoteConfigFieldVO : quoteConfigFieldVOList) {
                        stageOrdinal = getRequiredOrdinalByStatus(quoteConfigFieldVO.getRequiredStage());
                        if (stageOrdinal <= quoteStatusOrdinal) {
                            return false;
                        }
                    }
                }
                for (AdditionalInformation__c additionalInformation : quoteLinesAddlList) {
                    for (QuoteConfigFieldVO quoteConfigFieldVO : quoteConfigFieldVOList) {
                        if (!isFieldValueValid(additionalInformation, quoteConfigFieldVO, quoteStatusOrdinal, productCategory)) {
                            
                            system.debug('Missing field! qlid is ' + ql.Id + ' field is ');
                            system.debug(quoteConfigFieldVO);
                            return false;
                        } 
                    }
                }
            }
        }
        return true;
    }
    
    public static Boolean isFieldValueValid(AdditionalInformation__c additionalInformation, QuoteConfigFieldVO quoteConfigFieldVO, Integer quoteStatusOrdinal, String productCategory) {
        Integer fieldRequiredStageOrdinal = QuoteCustomConfigurationService.getRequiredOrdinalByStatus(quoteConfigFieldVO.getRequiredStage());
        
        Boolean mustValidate = true;
        String cffn = quoteConfigFieldVO.fieldName;

        Boolean isPorting = 'Porting'.equalsIgnoreCase(additionalInformation.Phone_Number_Setup__c) ;
        Boolean isMaintainWC = 'Maintain - with change'.equalsIgnoreCase(additionalInformation.Phone_Number_Setup__c) ;
        Boolean isMaintainNC = 'Maintain - no change'.equalsIgnoreCase(additionalInformation.Phone_Number_Setup__c) ;
        Boolean isMaintain = 'Maintain'.equalsIgnoreCase(additionalInformation.Phone_Number_Setup__c) ;
        if (!isMaintain){ if (isMaintainWC || isMaintainNC) {isMaintain=true;} }
        Boolean isNewNumber = 'New Number'.equalsIgnoreCase(additionalInformation.Phone_Number_Setup__c) ;
                        
        if('Wireless'.equalsIgnoreCase(productCategory)) {
            if(!isPorting && ('Old_Service_Provider__c'.equalsIgnoreCase(cffn) ||
                'Porting__c'.equalsIgnoreCase(cffn) ||
                'Old_Service_Provider_Account_Number__c'.equalsIgnoreCase(cffn))) {
                mustValidate = false;               
            } else if (!isMaintain && 'Mobile__c'.equalsIgnoreCase(cffn)) {
                mustValidate = false;
            } else if ('SIM__c'.equalsIgnoreCase(cffn) && additionalInformation.Network_Type__c != 'HSPA') {
                mustValidate = false;
            }
            if (isMaintainNC && !'Mobile__c'.equalsIgnoreCase(cffn)){
                mustValidate = false;
            }
        } else if('Office Phone'.equalsIgnoreCase(productCategory)) {
            if(!isPorting && ('Street__c'.equalsIgnoreCase(cffn) ||
                'City__c'.equalsIgnoreCase(cffn) ||
                'Province__c'.equalsIgnoreCase(cffn) ||
                'Postal_Code__c'.equalsIgnoreCase(cffn) ||
                'Porting__c'.equalsIgnoreCase(cffn))) {
                mustValidate = false;
            }
            if (!isMaintain && 'Billing_Telephone_Number_BTN__c'.equalsIgnoreCase(cffn)) {
                mustValidate = false;
            }
            if (isMaintainNC && !'Billing_Telephone_Number_BTN__c'.equalsIgnoreCase(cffn)) {
                mustValidate = false;
            }
        } else if('Existing Device'.equalsIgnoreCase(productCategory)) {
            if(!isNewNumber && ('City_of_Use__c'.equalsIgnoreCase(cffn) ||
                'Province__c'.equalsIgnoreCase(cffn))) {
                mustValidate = false;
            } else if(!isMaintain && 'Mobile__c'.equalsIgnoreCase(cffn)) {
                mustValidate = false;
            }
        }
        
        
        if(StringUtils.isBlank((String)additionalInformation.get(cffn))
              &&
          fieldRequiredStageOrdinal <= quoteStatusOrdinal
              &&
          mustValidate) {
            return false;
        } 
        return true;
    }
}