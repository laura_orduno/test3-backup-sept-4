/********************************************************************************************
* Class Name : ESRInboundServiceTest
* Purpose    : This is the test class for ESRInboundService class
*    
* 
* Modification History 
*-------------------------------------------------------------------------------------------
* Author                         Date                Version             Remarks
*-------------------------------------------------------------------------------------------
* Tech Mahindra(Swapnil)       May/17/2016          1.0                 Initial Creation
* 
*
**********************************************************************************************/
@isTest
public class ESRInboundServiceTest {    
    /**
testESRInboundService() - Test Method for ESRInboundService inbound service class
**/
    static testMethod void testESRInboundService(){
        
        //Create Account record
        Account esrAccount = new Account();
        esrAccount.Name = 'testESRAccount';
        esrAccount.CAN__c = 'test';
        //esrAccount.BillingAddress = 'BilingAddress';
        esrAccount.BillingCity = 'testBillingCity';
        esrAccount.BillingState = 'testBillingState';
        esrAccount.BillingPostalCode = '0000';
        esrAccount.BillingStreet = 'testBillingStreet';
        insert esrAccount;
        System.assertNotEquals(esrAccount.Id, NULL);
        
        Profile p = [SELECT Id FROM Profile WHERE Name = 'Standard User'];
        
        //Create User record
        User esrUser = new User();
        esrUser.Username = 'testDeepak007@test.com';
        esrUser.LastName = 'lName';
        esrUser.Phone = '1234567890';
        esrUser.email = 'testDeepak007@test.com';
        esrUser.Alias = 'alias';
        esrUser.CommunityNickname = 'CommunityNickname';
        esrUser.TimeZoneSidKey = 'GMT';
        esrUser.LocaleSidKey = 'en_CA';
        esrUser.EmailEncodingKey = 'ISO-8859-1';
        esrUser.ProfileId = p.Id;
        esrUser.LanguageLocaleKey = 'en_US';
        insert esrUser;
        System.assertNotEquals(esrUser.Id, NULL);
        
        //Create SRS PDS Product record
        SRS_PODS_Product__c srsProduct = new SRS_PODS_Product__c();
        //srsProduct.Product_ID__c = esrProd.Id;
        srsProduct.Main_User__c = esrUser.Id;
        insert srsProduct;
        System.assertNotEquals(srsProduct.Id, NULL);
        
        //Create SRS PODS Template
        SRS_PODS_Template__c template = new SRS_PODS_Template__c();
        template.SRS_PODS_Product_Master__c = srsProduct.Id;
        template.QUESTION_EN__c = 'Support Level';
        template.VALID_ANSWER__c = 'Test Answer';
        template.ORDER_TYPE__c = 'ordertype';
        template.Child__c = 1234;
        template.SYSTEM_GENERATED_QUESTION_ID__c = 1234;
        insert template;
        System.assertNotEquals(template.Id, NULL);
        
        //Create Opportunity record
        Opportunity esrOpportunity = new Opportunity();
        esrOpportunity.Name = 'testOppName';
        esrOpportunity.Requested_Due_Date__c = System.today();
        esrOpportunity.Probability = 10;
        esrOpportunity.StageName = 'Originated';
        esrOpportunity.CloseDate = System.today();
        insert esrOpportunity;
        System.assertNotEquals(esrOpportunity.Id, NULL);
        
        //Create Contact record
        Contact esrContact = new Contact();
        esrContact.LastName = 'testLastName';
        esrContact.Email = 'TestEmail@test.com';
        esrContact.Phone = '1234567890';
        insert esrContact;
        System.assertNotEquals(esrContact.Id, NULL);
        
        //Create Product2 record
        Product2 esrProd = new Product2();
        esrProd.Name = 'TestProd';
        esrProd.CurrencyIsoCode = 'CAD';
        esrProd.IsActive = true;
        insert esrProd;
        System.assertNotEquals(esrProd.Id, NULL);
        
        //Create Opportunity Product Item record
        Opp_Product_Item__c esrOPI = new Opp_Product_Item__c();
        esrOPI.Total_Contract_Value__c = 50.00;
        esrOPI.Margin__c = 2;
        esrOPI.Opportunity__c = esrOpportunity.Id;
        esrOPI.Product__c = esrProd.Id;
        insert esrOPI;
        System.assertNotEquals(esrOPI.Id, NULL);
        
        //Create Service Request record
        Service_Request__c objSR = new Service_Request__c();
        ObjSR.OwnerId = esrUser.Id;
        ObjSR.Is_RFP__c = 'Yes';
        ObjSR.ESR_Opportunity_Type__c = 'ESR';
        ObjSR.Opportunity__c = esrOpportunity.Id;
        ObjSR.Account_Name__c = esrAccount.Id;
        ObjSR.Billing_Address__c = 'test';
        ObjSR.City__c = 'City';
        ObjSR.Postal_Code__c = '0000';
        ObjSR.Prov__c = 'Province__c';
        ObjSR.Existing_Service_ID__c = '0000';
        ObjSR.PrimaryContact__c = esrContact.Id;
        ObjSR.SRS_PODS_Product__c = srsProduct.Id;
        ObjSR.Opportunity_Product_Item__c = esrOPI.Id;
        insert ObjSR;
        system.assertNotEquals(ObjSR.id, null);
        
        Service_Request__c SR = [Select Id, Name from Service_Request__c WHERE Id =: objSR.Id];
        String SRName = SR.Name;
        
        //Create Enhanced Tracking (ESR History) child record
        Enhanced_Tracking__c oTrack = new Enhanced_Tracking__c();
        oTrack.Service_Request__c = objSR.Id;
        oTrack.Version_Status__c = 'In Progress';
        oTrack.ESR_Id__c = '1242';
        insert oTrack;
        system.assertNotEquals(oTrack.id, null);
        
        Enhanced_Tracking__c oVer = new Enhanced_Tracking__c();
        oVer.Service_Request__c = objSR.Id;
        oVer.ESR_Id__c = '1243';
        over.Version_Status__c = 'In Progress';
        insert oVer; 
        system.assertNotEquals(oVer.id, null);
        system.debug('$$$$ oTrack'+oTrack);
        system.debug('$$$$ oVer'+oVer);
                
        //Create SRS PODS Answer record
        SRS_PODS_Answer__c srsAnswer = new SRS_PODS_Answer__c();
        srsAnswer.Service_Request__c = objSR.Id;
        srsAnswer.SRS_PODS_Products__c = template.id;
        srsAnswer.SRS_PODS_Answer__c = 'answer';
        insert srsAnswer;
        System.assertNotEquals(srsAnswer.Id, NULL);
        
        List<ESRInboundService.EsrLog> lstLog = new List<ESRInboundService.EsrLog>();
        ESRInboundService.EsrLog log = new ESRInboundService.EsrLog();
        log.eventText = 'testeventText';
        log.logTxt = 'testlogTxt';
        lstLog.add(log);
        
        List<ESRInboundService.ProdTemplate> lstTemp = new List<ESRInboundService.ProdTemplate>();
        ESRInboundService.ProdTemplate temp = new ESRInboundService.ProdTemplate();
        temp.questionText = 'testquestionText';
        temp.answerText = 'testanswerText';
        lstTemp.add(temp);
        
        ESRInboundService.ESRServiceRequest SReq = new ESRInboundService.ESRServiceRequest();
        SReq.customerStatusCd = true;
        SReq.requestForProposalTxt = 'testrequestForProposalTxt';
        SReq.budgetaryFirmTxt = 'testbudgetaryFirmTxt';
        SReq.requestDueDt = date.today();
        SReq.salesQuoteRequestCd = 'testsalesQuoteRequestCd';
        SReq.projectCd = 'testprojectCd';
        SReq.opportunityValueNum = '11.11';
        SReq.hardwareMarginTxt = '12.20';
        SReq.notes = lstLog;
        SReq.requestStatusCd = 'teststatus';
        SReq.salesForceId = SRName;
        SReq.template = lstTemp;
        SReq.productCategoryTxt = 'testproductCategoryTxt';
        SReq.productName = 'testproductName';
        test.startTest();
          
        try{
            ESRInboundService.creatVersion(null);
        }
        catch(Exception ex){}
        try{
            ESRInboundService.CreateAttachmet(null);
        }
        catch(Exception ex){}
        ESRInboundService.ESRVersion ver = new ESRInboundService.ESRVersion();
        ver.esrId = '1234';
        ver.VersionId = 'testVerId';
        ver.VersionTitle = 'testVersionTitle';
        ver.description = 'testdescription';
        ver.orderType = 'testorderType';
        ver.systemType = 'testsystemType';
        ver.opportunityValueNum = String.valueOf(10);
        ver.OpportunityType = 'testOpportunityType';
        ver.notes = lstLog;
        ver.salesForceId = SRName;
        ver.requestStatusCd = 'CLOSED: AUTOQUOTE';
        
            ESRInboundService.creatVersion(ver);
            ver.requestStatusCd = 'CANCELLED : VSE';
            ver.esrId = '1235';
            ESRInboundService.creatVersion(ver);
            ver.requestStatusCd = 'SOLD: AUTOQUOTE';
            ver.esrId = '1236';
            ESRInboundService.creatVersion(ver);
        try{            
            ESRInboundService.creatVersion(ver);
        }catch(Exception e){}           
        try{  
            ver.salesForceId = 'SR-00000';      
            ESRInboundService.creatVersion(ver);
        }catch(Exception e){}        
        test.stopTest();
    }
    static testMethod void testESRInboundUpdateService(){
        
        //Create Account record
        Account esrAccount = new Account();
        esrAccount.Name = 'testESRAccount';
        esrAccount.CAN__c = 'test';
        //esrAccount.BillingAddress = 'BilingAddress';
        esrAccount.BillingCity = 'testBillingCity';
        esrAccount.BillingState = 'testBillingState';
        esrAccount.BillingPostalCode = '0000';
        esrAccount.BillingStreet = 'testBillingStreet';
        insert esrAccount;
        System.assertNotEquals(esrAccount.Id, NULL);
        
        Profile p = [SELECT Id FROM Profile WHERE Name = 'Standard User'];
        
        //Create User record
        User esrUser = new User();
        esrUser.Username = 'testDeepak007@test.com';
        esrUser.LastName = 'lName';
        esrUser.Phone = '1234567890';
        esrUser.email = 'testDeepak007@test.com';
        esrUser.Alias = 'alias';
        esrUser.CommunityNickname = 'CommunityNickname';
        esrUser.TimeZoneSidKey = 'GMT';
        esrUser.LocaleSidKey = 'en_CA';
        esrUser.EmailEncodingKey = 'ISO-8859-1';
        esrUser.ProfileId = p.Id;
        esrUser.LanguageLocaleKey = 'en_US';
        insert esrUser;
        System.assertNotEquals(esrUser.Id, NULL);
        
        //Create SRS PDS Product record
        SRS_PODS_Product__c srsProduct = new SRS_PODS_Product__c();
        //srsProduct.Product_ID__c = esrProd.Id;
        srsProduct.Main_User__c = esrUser.Id;
        insert srsProduct;
        System.assertNotEquals(srsProduct.Id, NULL);
        
        //Create SRS PODS Template
        SRS_PODS_Template__c template = new SRS_PODS_Template__c();
        template.SRS_PODS_Product_Master__c = srsProduct.Id;
        template.QUESTION_EN__c = 'Support Level';
        template.VALID_ANSWER__c = 'Test Answer';
        template.ORDER_TYPE__c = 'ordertype';
        template.Child__c = 1234;
        template.SYSTEM_GENERATED_QUESTION_ID__c = 1234;
        insert template;
        System.assertNotEquals(template.Id, NULL);
        
        //Create Opportunity record
        Opportunity esrOpportunity = new Opportunity();
        esrOpportunity.Name = 'testOppName';
        esrOpportunity.Requested_Due_Date__c = System.today();
        esrOpportunity.Probability = 10;
        esrOpportunity.StageName = 'Originated';
        esrOpportunity.CloseDate = System.today();
        insert esrOpportunity;
        System.assertNotEquals(esrOpportunity.Id, NULL);
        
        //Create Contact record
        Contact esrContact = new Contact();
        esrContact.LastName = 'testLastName';
        esrContact.Email = 'TestEmail@test.com';
        esrContact.Phone = '1234567890';
        insert esrContact;
        System.assertNotEquals(esrContact.Id, NULL);
        
        //Create Product2 record
        Product2 esrProd = new Product2();
        esrProd.Name = 'TestProd';
        esrProd.CurrencyIsoCode = 'CAD';
        esrProd.IsActive = true;
        insert esrProd;
        System.assertNotEquals(esrProd.Id, NULL);
        
        //Create Opportunity Product Item record
        Opp_Product_Item__c esrOPI = new Opp_Product_Item__c();
        esrOPI.Total_Contract_Value__c = 50.00;
        esrOPI.Margin__c = 2;
        esrOPI.Opportunity__c = esrOpportunity.Id;
        esrOPI.Product__c = esrProd.Id;
        insert esrOPI;
        System.assertNotEquals(esrOPI.Id, NULL);
        
        //Create Service Request record
        Service_Request__c objSR = new Service_Request__c();
        ObjSR.OwnerId = esrUser.Id;
        ObjSR.Is_RFP__c = 'Yes';
        ObjSR.ESR_Opportunity_Type__c = 'ESR';
        ObjSR.Opportunity__c = esrOpportunity.Id;
        ObjSR.Account_Name__c = esrAccount.Id;
        ObjSR.Billing_Address__c = 'test';
        ObjSR.City__c = 'City';
        ObjSR.Postal_Code__c = '0000';
        ObjSR.Prov__c = 'Province__c';
        ObjSR.Existing_Service_ID__c = '0000';
        ObjSR.PrimaryContact__c = esrContact.Id;
        ObjSR.SRS_PODS_Product__c = srsProduct.Id;
        ObjSR.Opportunity_Product_Item__c = esrOPI.Id;
        insert ObjSR;
        system.assertNotEquals(ObjSR.id, null);
        
        Service_Request__c SR = [Select Id, Name from Service_Request__c WHERE Id =: objSR.Id];
        String SRName = SR.Name;
        
        //Create Enhanced Tracking (ESR History) child record
        Enhanced_Tracking__c oTrack = new Enhanced_Tracking__c();
        oTrack.Service_Request__c = objSR.Id;
        oTrack.Version_Status__c = 'In Progress';
        oTrack.ESR_Id__c = '1245';
        insert oTrack;
        system.assertNotEquals(oTrack.id, null);
        
        Enhanced_Tracking__c oVer = new Enhanced_Tracking__c();
        oVer.Service_Request__c = objSR.Id;
        oVer.ESR_Id__c = '1246';
        over.Version_Status__c = 'In Progress';
        insert oVer; 
        system.assertNotEquals(oVer.id, null);
        system.debug('$$$$ oTrack'+oTrack);
        system.debug('$$$$ oVer'+oVer);
                
        //Create SRS PODS Answer record
        SRS_PODS_Answer__c srsAnswer = new SRS_PODS_Answer__c();
        srsAnswer.Service_Request__c = objSR.Id;
        srsAnswer.SRS_PODS_Products__c = template.id;
        srsAnswer.SRS_PODS_Answer__c = 'answer';
        insert srsAnswer;
        System.assertNotEquals(srsAnswer.Id, NULL);
        
        List<ESRInboundService.EsrLog> lstLog = new List<ESRInboundService.EsrLog>();
        ESRInboundService.EsrLog log = new ESRInboundService.EsrLog();
        log.eventText = 'testeventText';
        log.logTxt = 'testlogTxt';
        lstLog.add(log);
        
        List<ESRInboundService.ProdTemplate> lstTemp = new List<ESRInboundService.ProdTemplate>();
        ESRInboundService.ProdTemplate temp = new ESRInboundService.ProdTemplate();
        temp.questionText = 'testquestionText';
        temp.answerText = 'testanswerText';
        lstTemp.add(temp);
        
        ESRInboundService.ESRServiceRequest SReq = new ESRInboundService.ESRServiceRequest();
        SReq.customerStatusCd = true;
        SReq.requestForProposalTxt = 'testrequestForProposalTxt';
        SReq.budgetaryFirmTxt = 'testbudgetaryFirmTxt';
        SReq.requestDueDt = date.today();
        SReq.salesQuoteRequestCd = 'testsalesQuoteRequestCd';
        SReq.projectCd = 'testprojectCd';
        SReq.opportunityValueNum = '11.11';
        SReq.hardwareMarginTxt = '12.20';
        SReq.notes = lstLog;
        SReq.requestStatusCd = 'teststatus';
        SReq.salesForceId = SRName;
        SReq.template = lstTemp;
        SReq.productCategoryTxt = 'testproductCategoryTxt';
        SReq.productName = 'testproductName';
        
        ESRInboundService.updateRequest(SReq);     
        test.startTest();
        try{
            SReq.requestStatusCd = 'CANCELLED : VSE';
            ESRInboundService.updateRequest(SReq);
        }
        catch(Exception ex){}
         try{            
            SReq.requestStatusCd = 'CLOSED: AUTOQUOTE';
            ESRInboundService.updateRequest(SReq);
        }
        catch(Exception ex){}
        try{
            SReq = new ESRInboundService.ESRServiceRequest();
            SReq.salesForceId = 'Dummy';
            ESRInboundService.updateRequest(SReq);
        }
        catch(Exception ex){}
        
        try{
            ESRInboundService.updateRequest(null);            
        }
        catch(Exception ex){}
        ESRInboundService.EsrAttachment att = new ESRInboundService.EsrAttachment();
        att.ParentId = SRName;
        att.title = 'testtitle';
        att.fileTxt = 'testfileTxt';
        att.attachmentPathTxt = 'testattachmentPathTxt';
        att.attachmentTypeCd = 'testattachmentTypeCd';
        
        ESRInboundService.CreateAttachmet(att);
        test.stopTest();
    }
}