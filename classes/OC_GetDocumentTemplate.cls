global with sharing class OC_GetDocumentTemplate implements vlocity_cmt.VlocityOpenInterface{
	private static string CONTRACTREQUESTOBJ = 'Contracts__c';
    private static string DEALSUBPPORTOBJ = 'Offer_House_Demand__c';
    private static string ORDER = 'Order';

	global Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {
		Boolean success = true;
        system.debug('>>>>>>>>+122131' +methodName);
		if(methodName == 'getContractTemplateName')
		{ 
				getContractTemplateName(inputMap, outMap, options); 
		}		
		return success; 
   }
		
  private void getContractTemplateName (Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options){
		//get Contract Id
		Id contextId = (Id) inputMap.get('contextObjId');
		Sobject contractObj = (Sobject) inputMap.get('contractObj');
		List<SObject> contractLines = (List<SObject>) inputMap.get('itemsObj');
		String documentTemplateName ;

		 Schema.SObjectType contextObjType =  Id.valueOf(contextId).getSObjectType();
	   	 String contextObjName = contextObjType.getDescribe().getName();
		

		system.debug('contextObjName::'+ contextObjName);
        List<SObject> items = new List<SObject> ();
         
        if(CONTRACTREQUESTOBJ.equalsignoreCase(contextObjName)){
             // TODO:: Need to account for Service Request for Complex flow
            //items = [Select Id, Name from Service_Request__c  where Contract__c =:contextId];
            items = [Select Id,Type_Of_Contract__c from Contracts__c where Id =:contextId  ];
            if(items.size() > 0 && ! items.isEmpty()){
            	 String contractType = (String)items[0].get('Type_Of_Contract__c');
            	 if(contractType != null && contractType.equalsIgnoreCase('New')  ){
            	 	documentTemplateName = 'OC Communications Services Agreement New';
            	 }
            	 else if(contractType.equalsIgnoreCase('Amendment')){
            	 	//documentTemplateName = 'Amendment Agreement';
                    documentTemplateName = 'OC Amendment Agreement';
            	 }
            	
            }
        }
        else if(DEALSUBPPORTOBJ.equalsignoreCase(contextObjName)) {
           // items = [Select Id, Name from Rate_Plan_Item__c where Deal_Support__c =:contextId];
        }  
        else if(ORDER.equalsignoreCase(contextObjName)) {
            //items = [Select Id from OrderItem where OrderId =:contextId];
        } 

		outMap.put('templateName', documentTemplateName);
		
	}

}