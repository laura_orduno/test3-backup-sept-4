/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData = true)
private class smb_Test_customerSearchController {

    static testMethod void testCustomerSearch() {
        // TO DO: implement unit test
        RecordType recordType = [Select r.Id, r.DeveloperName From RecordType r where r.SobjectType = 'Account' and r.DeveloperName='RCID' limit 1];
        Account acc1 = new Account(Name='Testing Software', BillingCountry = 'US', BillingState = 'IL', recordTypeId=recordType.Id , No_Chronic_Incidents__c =10.0);
	   insert acc1;   
	   
	   Account insertedAcc = [Select ID, name, BillingCountry,BillingState from account where id = : acc1.id] ;
	   
	   Contact cont = new Contact(Lastname= 'lastname', Account = acc1,Email = 'test@test.com');
			insert cont;
		   
		   
		   Opportunity opp = new Opportunity(Name = 'Test Opp', AccountId = acc1.id, Type = 'New Customer', CloseDate = Date.today(), StageName = 'Prospecting');
	       insert opp;
			
		   
		   
		   SBQQ__Quote__c quote = new SBQQ__Quote__c(SBQQ__Account__c = acc1.id, SBQQ__BillingCity__c ='noida', SBQQ__Opportunity__c = opp.id); 
		   insert quote;
	       
	       Product2 prod = new Product2(Name='abc');
	       insert prod;  
	       
	       SBQQ__QuoteLine__c quoteLine = new SBQQ__QuoteLine__c(Category__c ='abc', SBQQ__Quote__c = quote.id , SBQQ__Product__c=prod.id);
	       
	       insert quoteLine;
	       
	       Asset asset=  new Asset(Account=acc1, SBQQ__QuoteLine__c = quoteLine.id , Name='abc', Contact=cont, AccountId = acc1.id );
		   insert asset;	
        // TO DO: implement unit test
       
			Case theCase = new Case();
           theCase.Account__c = acc1.id;
           theCase.Subject = 'TEST SUBJECT';
           theCase.Description = 'TEST';
		   theCase.Status = 'Closed';
		   theCase.Request_Type__c = 'Other';
		   theCase.Priority = 'Medium';
		   theCase.Root_Cause__c = 'Billing';
		   //theCase.IsClosed = false;
		   insert theCase;
        
        
        
       
       string url = 'https:;&TCCS_UTN::DD::CC::\\?drive.google.com;&TCCS_UTN\\?::DD::CC::&TCCS_UTN\\?::DD::CC::';
        PageReference ref =new PageReference(url);
       Test.setCurrentPage(ref);
       ApexPages.currentPage().getParameters().put('C','1');
       ApexPages.currentPage().getParameters().put('q','Testin');
        
        map<string, string>  mapOfStrings = new map<string, string>(); 
        mapOfStrings = smb_CtiUtility.parseUrl(url);
       CustomerSearchController smb = new CustomerSearchController();
       SMBCare_Address__c smbAddress = new SMBCare_Address__c();
     	
     	CustomerSearchController.displayAccount dAccount = new CustomerSearchController.displayAccount(acc1);
     	CustomerSearchController.displayAsset dAsset = new CustomerSearchController.displayAsset(asset);
     	CustomerSearchController.displayCase dCase = new CustomerSearchController.displayCase(theCase);
     	CustomerSearchController.displayContact dContact = new CustomerSearchController.displayContact(cont);
      
       CustomerSearchController.exactResultMatch exactMatch = new CustomerSearchController.exactResultMatch();
       CustomerSearchController.redirectDetails rDetails = new CustomerSearchController.redirectDetails();

               
        ref = smb.init();
    }
}