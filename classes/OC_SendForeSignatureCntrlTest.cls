@isTest
public class OC_SendForeSignatureCntrlTest{

    static testMethod void sendForSignatureMethodsTest(){
        Id contractReqRecId= Schema.SObjectType.Contracts__c.getRecordTypeInfosByName().get('eContract').getRecordTypeId();
        Id rcidAccntRecordTypeId=Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('RCID').getRecordTypeId();
        Account acct = new Account(Name='testacct', Phone='9222022768', RecordTypeId=rcidAccntRecordTypeId);
        insert acct;
        Contact testContactObj = new Contact(firstname = 'TestConFirstName', LastName = 'TestConLastname', accountId = acct.Id , 
                                             Phone = '123456' , fax = '1234567', Email = 'test@gmail.com');
        insert testContactObj;
        Opportunity opp = new Opportunity(AccountId=acct.id, Name='testopp', CloseDate=system.today(),Probability=0,StageName='test stage');
        insert opp; 
        User user = newUser(); 
        Contracts__c ConReq= new Contracts__c(RecordTypeId=contractReqRecId,Contract_Type__c='New',Opportunity__c=opp.id );
        insert ConReq;
        
        Contract testContractObj = new Contract(Complex_Fulfillment_Contract__c=ConReq.id,Status='Draft',Contract_Type__c='Amendment',
                                                AccountId=acct.id,ContractTerm=12,StartDate=system.today(),
                                                Customer_Signor__c=testContactObj.id,TELUS_Signor__c=userinfo.getuserid(), Agreement_Type__c = 'Amendment');
        insert testContractObj;
        
        
		PageReference pref = Page.OC_SendForEsignature;
        pref.getParameters().put('id',testContractObj.id);
        Test.setCurrentPage(pref);
        
        vlocity_cmt__ContractVersion__c contractVersionObj = new vlocity_cmt__ContractVersion__c();
        contractVersionObj.vlocity_cmt__ContractId__c = testContractObj.id ;
        insert contractVersionObj;
        vlocity_cmt__ContractEnvelope__c contractEnvelopeObj = new vlocity_cmt__ContractEnvelope__c();
        contractEnvelopeObj.vlocity_cmt__ContractVersionId__c = contractVersionObj.Id;
        contractEnvelopeObj.vlocity_cmt__ContractId__c = testContractObj.id ;
        insert contractEnvelopeObj; 
        vlocity_cmt__ContractRecipient__c contractRecipientObj = new vlocity_cmt__ContractRecipient__c();
        contractRecipientObj.vlocity_cmt__ContractId__c = testContractObj.id ;
        contractRecipientObj.vlocity_cmt__EnvelopeId__c = contractEnvelopeObj.Id;
        contractRecipientObj.Signer_Type__c = 'Customer Signor';
        contractRecipientObj.vlocity_cmt__RoutingOrder__c = 234;
        insert contractRecipientObj; 
        
        Test.startTest();
        vlocity_cmt.DocuSignEnvelopeController docu = new vlocity_cmt.DocuSignEnvelopeController();
        OC_SendForeSignatureCntrl sfs = new OC_SendForeSignatureCntrl(docu);
        OC_SendForeSignatureCntrl.SignerTypes st = new OC_SendForeSignatureCntrl.SignerTypes('label1','value1');
        OC_SendForeSignatureCntrl.CustomerInfo stt = new OC_SendForeSignatureCntrl.CustomerInfo('Email1@gmail.com','testName','testId');
        OC_SendForeSignatureCntrl.recipientContract rc = new OC_SendForeSignatureCntrl.recipientContract();
        rc.routingOrder=12;
        rc.signerType='testsigner';
        OC_SendForeSignatureCntrl.getSignerTypes();
        OC_SendForeSignatureCntrl.saveSignerType(); 
        sfs.updateContractMethod();
        
        OC_SendForeSignatureCntrl.recipientContract rcc = new OC_SendForeSignatureCntrl.recipientContract();
        rcc.routingOrder=123;
        rcc.signerType='testsignerr';
        
        list<OC_SendForeSignatureCntrl.recipientContract> rcclst = new list<OC_SendForeSignatureCntrl.recipientContract>();
        rcclst.add(rc);
        rcclst.add(rcc);
        String jsondata = JSON.serialize(rcclst);
        
            
        OC_SendForeSignatureCntrl.updateRecipient(testContractObj.id,jsondata);
        Test.stopTest();
    }
    
    
    
    
    private static user newuser()
   {
      Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User user1= new User(
            Alias = 'testcat9',
            Email='standarduser@testorg.com32wfsdfsdfsdf', 
            EmailEncodingKey='UTF-8',
            LastName='Robinson',
            LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US',
            ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles',
            UserName='standarduser@testorg.com32wfsdfsdfsdf',
            Release_Code__c = '05'
        );
       insert user1;
       return user1;
       
   }
}