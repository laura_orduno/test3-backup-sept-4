@isTest(seeAllData=false)
private class SRS_SearchESDControllerExt_test {
	
	@isTest static void create_service_address() {

		Account newAccount = new Account(
			Name='Test'
		);
		insert newAccount;

		Opportunity newOpp = new Opportunity(
			AccountID = newAccount.Id,
			Name = 'Test',
			CloseDate = Date.today(),
			StageName = 'Orignated'
		);
		insert newOpp;

		Service_Request__c newSR = new Service_Request__c(
			Opportunity__c = newOpp.Id,
			Account_Name__c = newAccount.Id
		);
		insert newSR;

		SRS_SearchESDController.Wrapper wrapper = new SRS_SearchESDController.Wrapper('12312414', 'Test', 'Test Customer', 'Test', 'Test Location', 'Test City', 'AB', 0);
		
		Test.startTest();

		SRS_SearchESDControllerExt.createServiceAddress(wrapper, newSR.Id, newAccount.Id, 'Test',  'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test');

		Test.stopTest();
	}
	
	@isTest static void retrieveProductId() {
		Product2 newProduct = S2C_TestUtility.createProduct('Test Product', 'Test');
		insert newProduct;

		SRS_PODS_Product__c newSRSPodsProduct = S2C_TestUtility.createSRSPODSProduct(newProduct.Product_Family__c);
		insert newSRSPodsProduct;

		Test.startTest();

		SRS_SearchESDControllerExt.retrieveProductId(newSRSPodsProduct.Name);

		Test.stopTest();
	}
	
}