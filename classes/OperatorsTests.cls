/**
 * Tests uite for <code>Operators</code> class.
 * 
 * @author Max Rudman
 * @since 4/12/2010
 */
@isTest
private class OperatorsTests {
    testMethod static void testEqualsBoolean() {
        Operators.Logical op = Operators.getInstance('equals');
        System.assert(op.evaluate(true,'true'),'Wrong result on equality');
        System.assert(!op.evaluate(false,'true'),'Wrong result on non equality');
    }
    
    testMethod static void testEqualsDecimal() {
        Operators.Logical op = Operators.getInstance('equals');
        System.assert(op.evaluate(Decimal.valueOf('34.5'),'34.50'),'Wrong result on equality');
        System.assert(!op.evaluate(Decimal.valueOf('34.56'),'34.50'),'Wrong result on non equality');
    }
    
    testMethod static void testEqualsDate() {
        Operators.Logical op = Operators.getInstance('equals');
        System.assert(op.evaluate(Date.valueOf('2008-10-15'),'2008-10-15'),'Wrong result on equality');
        System.assert(!op.evaluate(Date.valueOf('2008-10-15'),'2008-10-17'),'Wrong result on non equality');
    }
    
    testMethod static void testEqualsDateTime() {
        Operators.Logical op = Operators.getInstance('equals');
        System.assert(op.evaluate(DateTime.valueOf('2008-10-15 20:05:00'),'2008-10-15 20:05:00'),'Wrong result on equality');
        System.assert(!op.evaluate(DateTime.valueOf('2008-10-15 20:05:00'),'2008-10-15 20:06:00'),'Wrong result on non equality');
    }
    
    testMethod static void testEqualsString() {
        Operators.Logical op = Operators.getInstance('equals');
        System.assert(op.evaluate(null,null));
        System.assert(!op.evaluate('v1',null));
        System.assert(op.evaluate('v1','V1'),'Wrong result on equality');
        System.assert(!op.evaluate('v1','v2'),'Wrong result on non equality');
        System.assert(op.createQueryExpression('fname', 'value') != null);
    }
    
    testMethod static void testEqualsStringMultiple() {
        Operators.Logical op = Operators.getInstance('equals');
        System.assert(op.evaluate('V2','V1, v2'),'Wrong result on equality');
        System.assert(!op.evaluate('v1','v2, v3'),'Wrong result on non equality');
    }
    
    testMethod static void testGreaterThanDecimal() {
        Operators.Logical op = Operators.getInstance('greater than');
        System.assert(!op.evaluate(Decimal.valueOf('12.34'),null));
        System.assert(op.evaluate(Decimal.valueOf('12.34'),'11'));
        System.assert(!op.evaluate(Decimal.valueOf('12.34'),'22'));
        System.assert(op.createQueryExpression('fname', 1) != null);
    }
    
    testMethod static void testGreaterThanDate() {
        Operators.Logical op = Operators.getInstance('greater than');
        System.assert(!op.evaluate(Date.valueOf('2008-10-15'),null));
        System.assert(op.evaluate(Date.valueOf('2008-10-15'),'2008-09-15'));
        System.assert(!op.evaluate(Date.valueOf('2008-10-15'),'2008-10-15'));
    }
    
    testMethod static void testGreaterThanDateTime() {
        Operators.Logical op = Operators.getInstance('greater than');
        System.assert(!op.evaluate(DateTime.valueOf('2008-10-15 14:05:00'),null));
        System.assert(op.evaluate(DateTime.valueOf('2008-10-15 14:05:00'),'2008-10-15 14:04:59'));
        System.assert(!op.evaluate(DateTime.valueOf('2008-10-15 14:05:00'),'2008-10-15 14:05:00'));
    }
    
    testMethod static void testGreaterThanEqualsDecimal() {
        Operators.Logical op = Operators.getInstance('greater or equals');
        System.assert(!op.evaluate(Decimal.valueOf('12.34'),null));
        System.assert(op.evaluate(Decimal.valueOf('12.34'),'11'));
        System.assert(op.evaluate(Decimal.valueOf('12.34'),'12.34'));
        System.assert(!op.evaluate(Decimal.valueOf('12.34'),'22'));
        System.assert(op.createQueryExpression('fname', 1) != null);
    }
    
    testMethod static void testGreaterThanEqualsDate() {
        Operators.Logical op = Operators.getInstance('greater or equals');
        System.assert(!op.evaluate(Date.valueOf('2008-10-15'),null));
        System.assert(op.evaluate(Date.valueOf('2008-10-15'),'2008-09-15'));
        System.assert(op.evaluate(Date.valueOf('2008-10-15'),'2008-10-15'));
        System.assert(!op.evaluate(Date.valueOf('2008-10-15'),'2008-11-17'));
    }
    
    testMethod static void testGreaterThanEqualsDateTime() {
        Operators.Logical op = Operators.getInstance('greater or equals');
        System.assert(!op.evaluate(DateTime.valueOf('2008-10-15 14:05:00'),null));
        System.assert(op.evaluate(DateTime.valueOf('2008-10-15 14:05:00'),'2008-10-15 14:04:59'));
        System.assert(op.evaluate(DateTime.valueOf('2008-10-15 14:05:00'),'2008-10-15 14:05:00'));
        System.assert(!op.evaluate(DateTime.valueOf('2008-10-15 14:05:00'),'2008-10-15 14:06:00'));
    }
    
    testMethod static void testLessThanDecimal() {
        Operators.Logical op = Operators.getInstance('less than');
        System.assert(!op.evaluate(Decimal.valueOf('12.34'),null));
        System.assert(op.evaluate(Decimal.valueOf('12.34'),'22'),'Wrong result on equality');
        System.assert(!op.evaluate(Decimal.valueOf('12.34'),'11'),'Wrong result on non equality');
        System.assert(op.createQueryExpression('fname', 1) != null);
    }
    
    testMethod static void testLessThanDate() {
        Operators.Logical op = Operators.getInstance('less than');
        System.assert(!op.evaluate(Date.valueOf('2008-10-15'),null));
        System.assert(op.evaluate(Date.valueOf('2008-10-15'),'2008-11-15'));
        System.assert(!op.evaluate(Date.valueOf('2008-10-15'),'2008-09-17'));
    }
    
    testMethod static void testLessThanDateTime() {
        Operators.Logical op = Operators.getInstance('less than');
        System.assert(!op.evaluate(DateTime.valueOf('2008-10-15 14:05:00'),null));
        System.assert(op.evaluate(DateTime.valueOf('2008-10-15 14:05:00'),'2008-11-15 14:05:01'));
        System.assert(!op.evaluate(DateTime.valueOf('2008-10-15 14:05:00'),'2008-09-17 14:04:59'));
    }
    
    testMethod static void testLessThanEqualsDecimal() {
        Operators.Logical op = Operators.getInstance('less or equals');
        System.assert(!op.evaluate(Decimal.valueOf('12.34'),null));
        System.assert(op.evaluate(Decimal.valueOf('12.34'),'22'));
        System.assert(op.evaluate(Decimal.valueOf('12.34'),'12.34'));
        System.assert(!op.evaluate(Decimal.valueOf('12.34'),'11'));
        System.assert(op.createQueryExpression('fname', 1) != null);
    }
    
    testMethod static void testLessThanEqualsDate() {
        Operators.Logical op = Operators.getInstance('less or equals');
        System.assert(!op.evaluate(Date.valueOf('2008-10-15'),null));
        System.assert(op.evaluate(Date.valueOf('2008-10-15'),'2008-11-15'));
        System.assert(op.evaluate(Date.valueOf('2008-10-15'),'2008-10-15'));
        System.assert(!op.evaluate(Date.valueOf('2008-10-15'),'2008-09-17'));
    }
    
    testMethod static void testLessThanEqualsDateTime() {
        Operators.Logical op = Operators.getInstance('less or equals');
        System.assert(!op.evaluate(DateTime.valueOf('2008-10-15 14:05:00'),null));
        System.assert(op.evaluate(DateTime.valueOf('2008-10-15 14:05:00'),'2008-11-15 14:05:01'));
        System.assert(op.evaluate(DateTime.valueOf('2008-10-15 14:05:00'),'2008-10-15 14:05:00'));
        System.assert(!op.evaluate(DateTime.valueOf('2008-10-15 14:05:00'),'2008-09-17 14:04:59'));
    }
    
    testMethod static void testStartsWith() {
        Operators.Logical op = Operators.getInstance('starts with');
        System.assert(!op.evaluate(null,null));
        System.assert(!op.evaluate('Field Value',null));
        System.assert(!op.evaluate('XXX Field Value','Field'));
        System.assert(op.evaluate('Field Value','field'));
        System.assert(op.evaluate('Field Value','Field value'));
    }
    
    testMethod static void testEndsWith() {
        Operators.Logical op = Operators.getInstance('ends with');
        System.assert(!op.evaluate(null,null));
        System.assert(!op.evaluate('Field Value',null));
        System.assert(!op.evaluate('Field Value XXX','Field Value'));
        System.assert(op.evaluate('Field Value','value'));
        System.assert(op.evaluate('Field Value','Field value'));
    }
    
    testMethod static void testContains() {
        Operators.Logical op = Operators.getInstance('contains');
        System.assert(!op.evaluate(null,null));
        System.assert(!op.evaluate('Field Value',null));
        System.assert(!op.evaluate('Field XXX Value','Field Value'));
        System.assert(op.evaluate('Field Value','value'));
        System.assert(op.evaluate('Field Value','Field value'));
        System.assert(op.evaluate('Field XxX Value','xxx'));
    }
}