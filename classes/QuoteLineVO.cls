public class QuoteLineVO {
    public SBQQ__QuoteLine__c line {get; private set;}
    public String id {get{return line.Id;}}
    public String productName {get{return line.SBQQ__ProductName__c;}}
    public String customerProductName {get{return line.Customer_Product_Name__c;}}
    public String productCode {get{return line.SBQQ__ProductCode__c;}}
    public Decimal price {get{return (line.SBQQ__NetTotal__c == null) ? 0 : line.SBQQ__NetTotal__c;}}
    public Decimal listPrice {get{return (line.SBQQ__ListTotal__c == null) ? 0 : line.SBQQ__ListTotal__c;}}
    public Decimal discount {get{return listPrice - price;}}
    public String action {get{return line.SBQQ__Product__r.Bundle_Type__c;}}
    public Decimal level {get{return line.SBQQ__OptionLevel__c == null ? 0 : line.SBQQ__OptionLevel__c;}}
    public Integer quantity {get{return (line.SBQQ__Quantity__c == null) ? null : line.SBQQ__Quantity__c.intValue();}}
    public String serviceTerm {get{return line.Service_Term__c;}}

    public QuoteLineVO[] components {get; private set;}
    
    public QuoteLineVO(SBQQ__QuoteLine__c line) {
        this.line = line;
        components = new List<QuoteLineVO>();
    }
    
    public QuoteLineVO[] getAll() {
        QuoteLineVO[] result = new List<QuoteLineVO>();
        result.add(this);
        for (QuoteLineVO cvo : components) {
            result.addAll(cvo.getAll());
        }
        return result;
    }
    
    public String getConfigureLink() {
        PageReference pref = Page.SiteConfigureProducts;
        pref.getParameters().put('qid', line.SBQQ__Quote__c);
        pref.getParameters().put('lid', line.Id);
        pref.getParameters().put('step', 'trickstep'); // Added for case 1054. 'trickstep' is added as a class to the body element of the SiteConfigureProducts VF page.
        PageReference ret = Page.QuoteSummary;
        ret.getParameters().put('qid', line.SBQQ__Quote__c);
        // Instructs QuoteDetailController to re-link Addl Info records to quote lines.
        ret.getParameters().put('link', '1');
        
        // Adds migrate button
        pref.getParameters().put('cbc', '1');
        pref.getParameters().put('cba0', 'save');
        pref.getParameters().put('cbl0', 'Switch Bundle Types');
        pref.getParameters().put('cbu0', Site.getCurrentSiteUrl() + 'quotemigrate?qid=' + line.SBQQ__Quote__c );
        
        QuotePortalUtils.addConfigurationSummaryParams(pref);
        QuotePortalUtils.addReturnUrlParam(pref, ret);
        return pref.getUrl();
    }
    
    public String getQuoteCompleteInformationLink() {
        PageReference pref = Page.QuoteCompleteInformation;
        pref.getParameters().put('qid', line.SBQQ__Quote__c);
        //pref.getParameters().put('stage', EncodingUtil.urlEncode(QuoteStatus.SENT_FOR_CREDIT_CHECK, 'UTF-8'));
        return pref.getUrl();
    }
    
    public ConfigurationField[] getConfigurationFields() {
        if (line.AdditionalInformation__r.isEmpty()) {
            return getConfigurationFieldsFromLine();
        } else {
            return getConfigurationFieldsFromAddlInfo();
        }
    }
    
    private ConfigurationField[] getConfigurationFieldsFromLine() {
        ConfigurationField[] fields = new List<ConfigurationField>();
        String cfields = line.SBQQ__Product__r.SBQQ__ConfigurationFields__c;
        if (cfields != null) {
            for (String cf : cfields.split(',')) {
                cf = cf.trim();
                cf = cf.split('\\+')[0];
                Schema.SObjectField field = MetaDataUtils.getField(SBQQ__QuoteLine__c.sObjectType, cf);
                if (field != null) {
                    fields.add(new ConfigurationField(field.getDescribe().getLabel(), line.get(field)));
                }
            }
        }
        return fields;
    }
    
    private ConfigurationField[] getConfigurationFieldsFromAddlInfo() {
        ConfigurationField[] fields = new List<ConfigurationField>();
        for (AdditionalInformation__c ai : line.AdditionalInformation__r) {
            for (Schema.SObjectField field : QuotePortalUtils.getAdditionalInfoFieldList()) {
                Object value = ai.get(field);
                if (value != null) {
                    fields.add(new ConfigurationField(field.getDescribe().getLabel(), value));
                }
            }
        }
        return fields;
    }
    
    public class ConfigurationField {
        public String label {get; private set;}
        public Object value {get; private set;}
        
        public ConfigurationField(String label, Object value) {
            this.label = label;
            this.value = value;
        }
    }
    
}