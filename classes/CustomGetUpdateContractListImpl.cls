global with sharing class CustomGetUpdateContractListImpl implements vlocity_cmt.VlocityOpenInterface{
    private static string CONTRACTOBJ = 'Contracts__c';
    private static string DEALSUBPPORTOBJ = 'Offer_House_Demand__c';
	private static string ORDEROBJ = 'Order';

    public Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {
        Boolean success = true;
        if(methodName == 'getUpdateContractList')
        { 
                getUpdateContractList(inputMap, outMap, options); 
        }
        
          return success; 
  }
        
    private void getUpdateContractList (Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options){
        id contextId = (id) inputMap.get('contextObjId');
        Schema.SObjectType contextObjType =  Id.valueOf(contextId).getSObjectType();
        String contextObjName = contextObjType.getDescribe().getName();
        List<String> statusList = new List<String> ();
        statusList.add('Draft');
        string queryStr = '';
        if(CONTRACTOBJ.equalsignoreCase(contextObjName)){
         queryStr = 'Select Id,name from Contract where ';
         queryStr += 'Complex_Fulfillment_Contract__c =:contextId AND Status IN :statusList' ;
         queryStr += ' Order by Name LIMIT 50000';
        }
        else if(DEALSUBPPORTOBJ.equalsignoreCase(contextObjName)) {
          statusList.add('Contract Registered');
            queryStr = 'Select Id,name from Contract where ';
         queryStr += 'Offer_House_Demand__c =:contextId AND Status IN :statusList' ;
         queryStr += ' Order by Name LIMIT 50000';
            
        } 
        else if(ORDEROBJ.equalsignoreCase(contextObjName)) {
             queryStr = 'Select Id,name from Contract where ';
         queryStr += 'vlocity_cmt__OrderId__c =:contextId AND Status IN :statusList' ;
         queryStr += ' Order by Name LIMIT 50000';
            
        }
        
        List<SObject> objList;
        try{
            objList = database.query(queryStr);
            System.debug('objList>>>' +objList);
        }catch(Exception e){
            throw e;
        }
        
        outMap.put('contractList', objList);
        
    }

}