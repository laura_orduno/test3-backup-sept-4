/*
 * Tests for trac_RepairValidateCtlr
 * Part of the SMB Repairs project
 *
 * @author: Grant Adamson, Traction On Demand 
 */

@isTest
private class trac_RepairValidateCtlrTest {
	private static testMethod void testCtor() {
		Case c = new Case(subject = 'test');
		insert c;
		
		trac_PageUtils.setParam('id',c.id);
		
		trac_RepairValidateCtlr ctlr = new trac_RepairValidateCtlr();
		
		system.assertEquals(c.id,ctlr.theCase.id);
	}
	
	
	private static testMethod void testDoValidateSuccess() {
		trac_RepairTestUtils.createTestEndpoint();
		
		Case c = new Case(
			ESN_MEID_IMEI__c = trac_RepairTestUtils.REPAIR_ESN_SUCCESS,
			Repair_Type__c = 'Device',
			Mobile_number__c = '6041112222'
		);
											
		insert c;
		
		trac_PageUtils.setParam('id',c.id);
		
		trac_RepairValidateCtlr ctlr = new trac_RepairValidateCtlr();
		
		ctlr.doValidate();
		
		system.assert(ctlr.success);
		system.assertEquals(false,ApexPages.hasMessages(ApexPages.Severity.WARNING));
		system.assertEquals(false,ApexPages.hasMessages(ApexPages.Severity.ERROR));
		
		
		c = [
			SELECT repair_validated__c
			FROM Case
			WHERE Id = :c.id
		];
		
		system.assertEquals(true,c.repair_validated__c);
	}
	
	
	private static testMethod void testDoValidateWarning() {
		trac_RepairTestUtils.createTestEndpoint();
		
		Case c = new Case(
			ESN_MEID_IMEI__c = trac_RepairTestUtils.REPAIR_ESN_WARNING,
			Repair_Type__c = 'Device',
			Mobile_number__c = '6041112222'
		);
											
		insert c;
		
		trac_PageUtils.setParam('id',c.id);
		
		trac_RepairValidateCtlr ctlr = new trac_RepairValidateCtlr();
		
		ctlr.doValidate();
		
		system.assertEquals(false,ctlr.success);
		system.assertEquals(true,ApexPages.hasMessages(ApexPages.Severity.WARNING));
		system.assertEquals(false,ApexPages.hasMessages(ApexPages.Severity.ERROR));
		
		ctlr.doContinue();
		
		c = [
			SELECT repair_validated__c
			FROM Case
			WHERE Id = :c.id
		];
		
		system.assertEquals(true,c.repair_validated__c);
	}
	
	
	private static testMethod void testDoValidateFailure() {
		trac_RepairTestUtils.createTestEndpoint();
		
		Case c = new Case(
			ESN_MEID_IMEI__c = trac_RepairTestUtils.REPAIR_ESN_NO_MATCH,
			Repair_Type__c = 'Device',
			Mobile_number__c = '6041112222'
		);
											
		insert c;
		
		trac_PageUtils.setParam('id',c.id);
		
		trac_RepairValidateCtlr ctlr = new trac_RepairValidateCtlr();
		
		ctlr.doValidate();
		
		system.assertEquals(false,ctlr.success);
		system.assertEquals(false,ApexPages.hasMessages(ApexPages.Severity.WARNING));
		system.assertEquals(true,ApexPages.hasMessages(ApexPages.Severity.ERROR));
		system.assertEquals(true,ctlr.showManualLink);
		
		c = [
			SELECT repair_validated__c
			FROM Case
			WHERE Id = :c.id
		];
		
		system.assertEquals(false,c.repair_validated__c);
	}
	
}