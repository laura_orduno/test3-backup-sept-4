public class ChannelPortalCacheMgmtWSCallout {
    
    public static ChannelPortalCacheMgmtSrvReqRes.GetCacheListResponseType getCacheList(String referenceKey) {
        ChannelPortalCacheMgmtSrvReqRes.GetCacheListResponseType response;
        try {
            ChannelPortalCacheMgmtSvcV1.ChannelPortalCacheMgmtSvc_v1_0_SOAP servicePort = new ChannelPortalCacheMgmtSvcV1.ChannelPortalCacheMgmtSvc_v1_0_SOAP();
            servicePort = prepareCallout(servicePort);
            response=servicePort.getCacheList(servicePort,referenceKey);            
        } 
        catch (CalloutException e) {
            System.debug(e.getmessage());
            OrdrUtilities.auditLogs('ChannelPortalCacheMgmtWSCallout.getCacheList', 
                                    'CMS', 
                                    'Channel Portal Cache Manager', 
                                    null, 
                                    null, 
                                    true, 
                                    e.getMessage(), 
                                    e.getStackTraceString());
            
            //throw e;
        }
        ChannelPortalCacheMgmtData dataObj=new ChannelPortalCacheMgmtData();
        dataObj.response=response;
        ChannelPortalCacheMgmtSrvReqRes.MapItem[] customCacheList=response.customCacheList;
        String cacheKey='';
        String firstName='';
        String lastName='';
        String loggedInuserId='';
        String portalUserId='';
        for(ChannelPortalCacheMgmtSrvReqRes.MapItem mapItemObj:customCacheList){
            System.debug('key='+mapItemObj.key);
            System.debug('value='+mapItemObj.value);
            
            if(String.isNotBlank(mapItemObj.key) && 'portalUserID'.equalsIgnoreCase(mapItemObj.key)){
                cacheKey=mapItemObj.value;
                portalUserId=mapItemObj.value;
                //dataObj.salesRepPortalId=mapItemObj.value;
            }
             if(String.isNotBlank(mapItemObj.key) && 'loginUserID'.equalsIgnoreCase(mapItemObj.key)){
                loggedInuserId=mapItemObj.value;
                //dataObj.salesRepPortalId=mapItemObj.value;
            }
            
            if(String.isNotBlank(mapItemObj.key) && 'firstName'.equalsIgnoreCase(mapItemObj.key)){
                firstName=mapItemObj.value.replace('\'', '\\\'');
            }
            if(String.isNotBlank(mapItemObj.key) && 'lastName'.equalsIgnoreCase(mapItemObj.key)){
                lastName=mapItemObj.value.replace('\'', '\\\'');
            }
            dataObj.salesRepName = firstName + ' ' + lastName;
            if(String.isNotBlank(mapItemObj.key) && 'portalRoleName'.equalsIgnoreCase(mapItemObj.key)){
                dataObj.portalRole=mapItemObj.value;
            }
            if(String.isNotBlank(mapItemObj.key) && 'currentChannelOutletID'.equalsIgnoreCase(mapItemObj.key)){
                dataObj.currentOutletId=mapItemObj.value;
            }
            if(String.isNotBlank(mapItemObj.key) && 'channelOrgSalesReps_JSON'.equalsIgnoreCase(mapItemObj.key)){
                if(String.isNotBlank(mapItemObj.value)){
                    List<ChannelOrgSalesRepsJSONExplicit> objList = ChannelOrgSalesRepsJSONExplicit.parse(mapItemObj.value);
                    for(ChannelOrgSalesRepsJSONExplicit obj : objList){
                        obj.firstName = obj.firstName.replace('\'', '');
                        obj.lastName = obj.lastName.replace('\'', '');
                    }
                    dataObj.channelOrgSalesRepJsonObjList=objList;
                    System.debug('ChannelOrgSalesRepsJSONExplicit List='+objList);
                }
                
            }
            if(String.isNotBlank(mapItemObj.key) && 'channelOrgOutlets_JSON'.equalsIgnoreCase(mapItemObj.key)){
                if(String.isNotBlank(mapItemObj.value)){
                    List<ChannelOrgOutletsJSONExplicit> objList = ChannelOrgOutletsJSONExplicit.parse(mapItemObj.value);
                    System.debug('ChannelOrgOutletsJSONExplicit list='+objList);
                    dataObj.channelOrgOutletsJsonObjList=objList;
                }
               
            }
        }
        
        String userInfoId=UserInfo.getUserId();
       if(String.isNotBlank(userInfoId)){
            String key = 'local.orderingCache.KEY' + userInfoId;
           createOrUpdateAppData(key,dataObj);
           Cache.Org.put(key, dataObj);
       }        
        return response;
    }
    @TestVisible
    private static void createOrUpdateAppData(String key,ChannelPortalCacheMgmtData dataObj){        
        RecordType rt=[select id from RecordType where developername='CMS_Data'];
        List<Application_General_Data__C> appDataList=[select id,key__c,value__c from Application_General_Data__C where recordTypeId=:rt.id and key__c=:key order by createddate desc];
        if(appDataList!=null && appDataList.isEmpty()){
             Application_General_Data__C appData=new Application_General_Data__c();
                appData.key__c=key;
                appData.RecordTypeId=rt.id;
                appData.value__c=Json.serialize(dataObj);
                insert appData;
        } else {
            Application_General_Data__C appData=appDataList.get(0);
                appData.value__c=Json.serialize(dataObj);
               update appData;
        }
               
    }
    private static ChannelPortalCacheMgmtSvcV1.ChannelPortalCacheMgmtSvc_v1_0_SOAP prepareCallout(ChannelPortalCacheMgmtSvcV1.ChannelPortalCacheMgmtSvc_v1_0_SOAP servicePort) {
        
        Ordering_WS__c channelPortalCacheMgmtEndPoint = Ordering_WS__c.getValues('ChannelPortalCacheMgmtEndPoint'); 
        System.debug('servicePort='+servicePort);
        System.debug('channelPortalCacheMgmtEndPoint='+channelPortalCacheMgmtEndPoint);
        System.debug('channelPortalCacheMgmtEndPoint.value='+channelPortalCacheMgmtEndPoint.value__c);
        servicePort.endpoint_x =channelPortalCacheMgmtEndPoint.value__c;
        System.debug('ChannelPortalCacheMgmtEndPoint: '+ channelPortalCacheMgmtEndPoint.value__c);
        
        
        servicePort.clientCertName_x = OrdrConstants.SDF_CERT_NAME;            
        // Set SFDC Webservice call timeout
        servicePort.timeout_x = OrdrConstants.WS_TIMEOUT_MS;
        return servicePort;
    } 
}