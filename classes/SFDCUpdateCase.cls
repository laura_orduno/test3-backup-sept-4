global class SFDCUpdateCase {
      webservice static Case UpdateCase(String caseNumber, String caseStatus, String TicketNo, String errorCode, String CaseCommentId) {        
        return SFDCUpdateCaseHelper.UpdateCase(caseNumber, caseStatus, TicketNo, errorCode, CaseCommentId);
    }

}