@isTest
public class ApprovalThresholdHelperTest {
	public static final String TRANSCATION_TYPE ='Credit';
    public static final String BUSINESS_UNIT ='TBS';
    static{
         BillingAdjustmentCommonTest.createReasonCodeData();
         BillingAdjustmentCommonTest.createReleaseCodeThresholdData(TRANSCATION_TYPE,BUSINESS_UNIT);
         BillingAdjustmentCommonTest.createReleaseCodeThresholdData(TRANSCATION_TYPE,'Default');
         BillingAdjustmentCommonTest.createControllerThresholdData(TRANSCATION_TYPE,BUSINESS_UNIT);
         loadApprovalThreshold();
    }

    @isTest()
    public static void testReleaseCodeandControllerMap(){
    
        System.assertNotEquals(true, ApprovalThresholdHelper.getReleaseCodeMap().isEmpty(), 'Release code map size should not be empty');
        System.assertNotEquals(true, ApprovalThresholdHelper.getControllerMap().isEmpty(), 'Release code map size should not be empty');
    }

    @isTest()
    public static void testGetRequiredReleaseCode(){
        Id individualThresholdApprovalId = Schema.SObjectType.Approval_Threshold__c.getRecordTypeInfosByName().get('Individual Threshold').getRecordTypeId();

        User newUser = newUser();
        insert newUser;

        Approval_Threshold__c individualThreshold = newApprovalThreshold(newUser, individualThresholdApprovalId);
        insert individualThreshold;

        List<Approval_Threshold__c> individualList = [SELECT Business_Unit__c, Transaction_Type__c, Individual_User__c, Individual_User_Release_Code__c, Individual_Threshold__c
                                                      FROM Approval_Threshold__c
                                                      WHERE Individual_User__c = :newUser.Id AND Transaction_Type__c = :TRANSCATION_TYPE
                                                                    AND Business_Unit__c = :BUSINESS_UNIT AND RecordTypeId = :individualThresholdApprovalId];
        System.debug('individualList ::: ' + individualList);
   
        System.assertEquals(5, ApprovalThresholdHelper.getRequiredReleaseCode(299999999.00,TRANSCATION_TYPE,BUSINESS_UNIT, newUser.Id, individualList), 'Test Release code 300000000');
        System.assertEquals(1, ApprovalThresholdHelper.getRequiredReleaseCode(400.00,TRANSCATION_TYPE,BUSINESS_UNIT, UserInfo.getUserId(), new List<Approval_Threshold__c>()), 'Test Release code 400');
        System.assertEquals(3, ApprovalThresholdHelper.getRequiredReleaseCode(401.00,TRANSCATION_TYPE,BUSINESS_UNIT, UserInfo.getUserId(), new List<Approval_Threshold__c>()), 'Test Release code 401');
        System.assertEquals(3, ApprovalThresholdHelper.getRequiredReleaseCode(5000.00,TRANSCATION_TYPE,BUSINESS_UNIT, UserInfo.getUserId(), new List<Approval_Threshold__c>()), 'Test Release code 5000');
        System.assertEquals(5, ApprovalThresholdHelper.getRequiredReleaseCode(5001.00,TRANSCATION_TYPE,BUSINESS_UNIT, UserInfo.getUserId(), new List<Approval_Threshold__c>()), 'Test Release code 5001');
        System.assertEquals(5, ApprovalThresholdHelper.getRequiredReleaseCode(5001.00,TRANSCATION_TYPE,BUSINESS_UNIT, UserInfo.getUserId(), new List<Approval_Threshold__c>()), 'Test Release code 5001');
        System.assertEquals(5, ApprovalThresholdHelper.getRequiredReleaseCode(5001.00,TRANSCATION_TYPE,'Enterprise', UserInfo.getUserId(), new List<Approval_Threshold__c>()), 'Test Release for default business unit');
        System.assertEquals(null, ApprovalThresholdHelper.getRequiredReleaseCode(5001.00,'Debit','Enterprise', UserInfo.getUserId(), new List<Approval_Threshold__c>()), 'Test Release code for not exsistant Transaction type');
        
    }

	@isTest()
    public static void testGetControllerThresholds(){
        System.assertEquals(15000.00, ApprovalThresholdHelper.getBusinessUnitThreshold(TRANSCATION_TYPE), 'Test BusinessUnit Threshold');
    	System.assertEquals(100000.00, ApprovalThresholdHelper.getCorporateThreshold(TRANSCATION_TYPE), 'Test Corporate Threshold ');
        System.assertEquals(null, ApprovalThresholdHelper.getCorporateThreshold('Debit'), 'Test Corporate Threshold when No entry for transactio type');
        System.assertEquals(null, ApprovalThresholdHelper.getBusinessUnitThreshold('Debit'), 'Test BusinessUnit Threshold when No entry for transactio type');
    }
    
    @isTest()
    public static void testneedControllerApproval(){
        
        System.assertEquals(false, ApprovalThresholdHelper.needBUControllerApproval(10000.00,TRANSCATION_TYPE), 'Test needBUContollerApproval for 10000');
        System.assertEquals(true, ApprovalThresholdHelper.needBUControllerApproval(20000.00,TRANSCATION_TYPE), 'Test needBUContollerApproval for 20000');
        System.assertEquals(false, ApprovalThresholdHelper.needCorporateControllerApproval(90000.00,TRANSCATION_TYPE), 'Test needCorporateControllerApproval for 90000');
    	System.assertEquals(true, ApprovalThresholdHelper.needCorporateControllerApproval(105000.00,TRANSCATION_TYPE), 'Test needCorporateControllerApproval for 105000');
        System.assertEquals(false, ApprovalThresholdHelper.needBUControllerApproval(10000.00,'Debit'), 'Test needBUControllerApprovalwhen No entry for transaction type');
        System.assertEquals(false, ApprovalThresholdHelper.needCorporateControllerApproval(10000.00,'Debit'), 'Test needCorporateControllerApproval No entry for transaction type');
    }
    
    
    @isTest()
    public static void testThresholdFilterEqualmethod(){
        ApprovalThresholdHelper.ThresholdFilter obj1  = new ApprovalThresholdHelper.ThresholdFilter('Credit', 'TBS');
        ApprovalThresholdHelper.ThresholdFilter obj2  = new ApprovalThresholdHelper.ThresholdFilter('Credit', 'TBS');
        ApprovalThresholdHelper.ThresholdFilter obj3  = new ApprovalThresholdHelper.ThresholdFilter('Credit', 'Enterprise');
        System.assertEquals(true, obj1.equals(obj2),'Obj1 should equals to obj2');
        System.assertEquals(false, obj1.equals(obj3),'Obj1 should not equals to obj3');
        System.assertEquals(false, obj1.equals(new Account()),'Obj1 should not equals to Account object');
        
    }

    private static void loadApprovalThreshold(){
        Set<ApprovalThresholdHelper.ThresholdFilter> filter = new Set<ApprovalThresholdHelper.ThresholdFilter>{
          new ApprovalThresholdHelper.ThresholdFilter(TRANSCATION_TYPE,BUSINESS_UNIT)
          };
        ApprovalThresholdHelper.loadThresholdList(filter);
    }

    private static User newUser(){
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        return new User(
            Alias = 'testcat9',
            Email='standarduser@testorg.com32wfsdfsdfsdf', 
            EmailEncodingKey='UTF-8',
            LastName='Testing',
            LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US',
            ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles',
            UserName='standarduser@testorg.com32wfsdfsdfsdf',
            Release_Code__c = '05'
        );
    }

    private static Approval_Threshold__c newApprovalThreshold(User currentUser, Id recordTypeId){
        return new Approval_Threshold__c(
            RecordTypeId = recordTypeId,
            Individual_User__c = currentUser.Id,
            Individual_Threshold__c = 300000000.00,
            Transaction_Type__c = TRANSCATION_TYPE,
            Business_Unit__c = BUSINESS_UNIT
        );
    }
}