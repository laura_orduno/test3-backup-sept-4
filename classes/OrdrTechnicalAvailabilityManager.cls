global without sharing class OrdrTechnicalAvailabilityManager {
public static boolean TechAvailabilityCall = False;


  
    global class ProductCategory {
        global String category;
        global String availability;
        public String accessType;
        public String downloadSpeed;
        public String ports;
        global String connectionType; // need to remove
        global List<Characteristic> resources; // need to remove
    }
    
    global class Characteristic {
        global String name; 
        global String value; 
    }
    
    public class TnPortability {
        //WLN
        public String phoneNumber;
        public String portableIndicator;
        public String localRoutingNumber;
        public String previousLocalServiceProviderRequiredIndicator;
        public String telusNpaNxxIndicator;
        public String useableIndicator;
        //WLS
        public String oldServiceProviderId;
        public String cdmaPrepaidCoverage;
        public String cdmaPostpaidCoverage;
        public String cdmaCoverage;
        public String hspaPrepaidCoverage;
        public String hspaPostpaidCoverage;
        public String hspaCoverage;
        public String mikeCoverage;
        public String npDirectionIndicator;
        public String currentBrand;
        public String responseCode;
        public String platform;
    }
    
    
    public class ServicePath {
        public String primaryTelephoneNumber; // Tel # / Circuit #
        public String customerName; // Name
        public String clearanceDate; // Clearance Date
        public String serviceTypeCode; // Service Type
        public String servicePathStatusCode; // Status
        public String cablePairDedicatedPlantCode; // Line Designation
        public String leasedLoopFlag; // Leased Loop
    }
    
     public class GponDropStatus {
        public String fsa;
        public String dropType;
        public String dropPermission;
        public String dropStatus;
        public String fsaReadyDate;
        public String coid;
        public String gisId;
        public String roeStatus;
        public String premiseType;
    }
    
    
    public static List<ProductCategory> checkProductTechnicalAvailability(Id serviceAddressId) {
        Map<String, String> inputMap = OrdrUtilities.constructServiceAddress(serviceAddressId);
        TpFulfillmentResourceOrderV3.ResourceOrder resourceOrder = OrdrTechnicalAvailabilityWsCallout.checkProductTechnicalAvailability(inputMap);
        TechAvailabilityCall = True;
        System.debug('TechAvailabilityCall###3'+TechAvailabilityCall);
        
        List<ProductCategory> productCategories = mapTechnicalAvailabilityResponse(resourceOrder);
        saveConnectionType(serviceAddressId, productCategories);
        
        return productCategories;
    }
    
    public static TnPortability checkWirelineTnPortability(Id serviceAddressId, String tn) {
        
        Map<String, Object> inputMap = OrdrUtilities.constructServiceAddress(serviceAddressId);
        inputMap.put(OrdrConstants.TN, tn);
        
        TpFulfillmentResourceOrderV3.ResourceOrder resourceOrder = OrdrTechnicalAvailabilityWsCallout.checkTnPortability(OrdrConstants.WIRELINE, inputMap);
        
        return mapTnPortabilityResponse(resourceOrder);
    } 
    
    public static TnPortability checkWirelessTnPortability(String tn, String productType, String brandId) {


        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put(OrdrConstants.TN, tn);
        inputMap.put(OrdrConstants.PRODUCT_TYPE, productType);
        inputMap.put(OrdrConstants.BRAND, brandId);
        
        TpFulfillmentResourceOrderV3.ResourceOrder resourceOrder = OrdrTechnicalAvailabilityWsCallout.checkTnPortability(OrdrConstants.WIRELESS, inputMap);
        
        return mapTnPortabilityResponse(resourceOrder);
    }  
    
    public static TnPortability mapTnPortabilityResponse(TpFulfillmentResourceOrderV3.ResourceOrder resourceOrder) {
        TnPortability tnPortability = new TnPortability();
        TpCommonBusinessInteractionV3.BusinessInteractionEntity resource = resourceOrder.ResourceOrderItem[0].Resource;
        
        for (TpCommonBaseV3.CharacteristicValue charVal : resource.CharacteristicValue) {
            //WLN
            if (charVal.Characteristic.Name == 'phoneNumber') tnPortability.phoneNumber = charVal.Value[0];
            if (charVal.Characteristic.Name == 'portableIndicator') tnPortability.portableIndicator = charVal.Value[0];
            if (charVal.Characteristic.Name == 'localRoutingNumber') tnPortability.localRoutingNumber = charVal.Value[0];
            if (charVal.Characteristic.Name == 'previousLocalServiceProviderRequiredIndicator') tnPortability.previousLocalServiceProviderRequiredIndicator = charVal.Value[0];
            if (charVal.Characteristic.Name == 'telusNpaNxxIndicator') tnPortability.telusNpaNxxIndicator = charVal.Value[0];
            if (charVal.Characteristic.Name == 'useableIndicator') tnPortability.useableIndicator = charVal.Value[0];
            //WLS
            if (charVal.Characteristic.Name == 'oldServiceProviderId') tnPortability.oldServiceProviderId = charVal.Value[0];
            if (charVal.Characteristic.Name == 'cdmaPrepaidCoverage') tnPortability.cdmaPrepaidCoverage = charVal.Value[0];
            if (charVal.Characteristic.Name == 'cdmaPostpaidCoverage') tnPortability.cdmaPostpaidCoverage = charVal.Value[0];
            if (charVal.Characteristic.Name == 'cdmaCoverage') tnPortability.cdmaCoverage = charVal.Value[0];
            if (charVal.Characteristic.Name == 'hspaPrepaidCoverage') tnPortability.hspaPrepaidCoverage = charVal.Value[0];
            if (charVal.Characteristic.Name == 'hspaPostpaidCoverage') tnPortability.hspaPostpaidCoverage = charVal.Value[0];
            if (charVal.Characteristic.Name == 'hspaCoverage') tnPortability.hspaCoverage = charVal.Value[0];
            if (charVal.Characteristic.Name == 'mikeCoverage') tnPortability.mikeCoverage = charVal.Value[0];
            if (charVal.Characteristic.Name == 'npDirectionIndicator') tnPortability.npDirectionIndicator = charVal.Value[0];
            if (charVal.Characteristic.Name == 'currentBrand') tnPortability.currentBrand = charVal.Value[0];
            if (charVal.Characteristic.Name == 'responseCode') tnPortability.responseCode = charVal.Value[0];
            if (charVal.Characteristic.Name == 'platform') tnPortability.platform = charVal.Value[0];
        }

        return tnPortability;
    }
    
    //aditya made it to public
    public static List<ProductCategory> mapTechnicalAvailabilityResponse(TpFulfillmentResourceOrderV3.ResourceOrder resourceOrder) {
        
        List<ProductCategory> productCategories = new List<ProductCategory>();
        if (resourceOrder != null) {
            for (TpFulfillmentResourceOrderV3.ResourceOrderItem resourceOrderItem : resourceOrder.ResourceOrderItem) {
                ProductCategory productCategory = new ProductCategory();
                for (TpCommonBaseV3.CharacteristicValue charVal : resourceOrderItem.CharacteristicValue) {
                    if (charVal.Characteristic.Name == 'name') productCategory.category = charVal.Value[0];
                    if (charVal.Characteristic.Name == 'available') productCategory.availability = charVal.Value[0];
                }
                if (resourceOrderItem.Resource != null) {
                    TpCommonBusinessInteractionV3.BusinessInteractionEntity resource = resourceOrderItem.Resource;
                    if(resource!=null && resource.CharacteristicValue!=null){
                        for (TpCommonBaseV3.CharacteristicValue charVal : resource.CharacteristicValue) {
                            if (charVal.Characteristic.Name == 'Access Type') productCategory.accessType = charVal.Value[0];
                            if (charVal.Characteristic.Name == 'Download speed') productCategory.downloadSpeed = charVal.Value[0];
                            if (charVal.Characteristic.Name == 'Available Ports') productCategory.ports = charVal.Value[0];
                        }
                    }
                }
                
                if (String.isNotBlank(productCategory.category)) {
                    productCategories.add(productCategory);
                }
            }
        }
            
        return productCategories;
    }
    
    //aditya made it to public
    public static void saveConnectionType(Id serviceAddressId, List<ProductCategory> productCategories) {
        
        Boolean isTvAvailable = false;
        String copperSpeed = '';
        String bondedCopperSpeed = '';
        String fibreSpeed = '';
        String fibreFifaSpeed = '';
        String  connectionType = '';
        String  downloadSpeed = '';
        Boolean isInternetAvailable = false;
		Boolean isVoiceAvailable = false;
        Boolean isMobileAvailable = false;
        List<Integer> downloadSpeedList=new List<Integer>();
        String availablePorts = '';
        
        Map<Integer,String> downloadSpeedMap=new  Map<Integer,String>();
        for (ProductCategory category : productCategories) {
            System.debug('CategoryC###'+category.category + 'Avail###'+category.availability);
            
            if(category.category.equals('TELUS TV Product') && !category.availability.equalsIgnoreCase('unavailable')) {
                isTvAvailable = true;
            }
            if( Null != OrdrConstants.GTACategoryMap.get(category.category)  && OrdrConstants.GTACategoryMap.get(category.category) =='TV' && category.availability!='unavailable')
            {
                isTvAvailable = true;
            }
            if( Null != OrdrConstants.GTACategoryMap.get(category.category)  && OrdrConstants.GTACategoryMap.get(category.category) =='MOBILE' && category.availability!='unavailable')
            {
                isMobileAvailable = true;
            }
			if( Null != OrdrConstants.GTACategoryMap.get(category.category)  && OrdrConstants.GTACategoryMap.get(category.category) =='VOICE' && category.availability!='unavailable')
            {
                isVoiceAvailable = true;
            } 
            if( Null != OrdrConstants.GTACategoryMap.get(category.category)  && OrdrConstants.GTACategoryMap.get(category.category) =='INTERNET' && category.availability!='unavailable')
            {
                isInternetAvailable = true;
            }
                    
          if (String.isNotBlank(category.accessType)) {
                if (category.accessType.containsIgnoreCase('Fibre - FIFA') && !connectionType.contains(';GPON') && category.availability.equalsIgnoreCase('available')) {
                    connectionType += ';GPON';
                    fibreFifaSpeed = '1G+ Mbps (FIFA)';
                } else if (category.accessType.containsIgnoreCase('Fibre - Non-FIFA') && !connectionType.contains(';GPON') && category.availability.equalsIgnoreCase('available')) {
                    connectionType += ';GPON';
                    fibreSpeed = '150+ Mbps';
                } else if (category.accessType.equalsIgnoreCase('Bonded Copper') && !connectionType.contains(';BONDED COPPER')) {
                    connectionType +=  ';BONDED COPPER';
                    bondedCopperSpeed  = !category.availability.equalsIgnoreCase('available') ? 'unconfirmed' : category.downloadSpeed;
                } else if (category.accessType.equalsIgnoreCase('Copper')  && !connectionType.contains(';COPPER')) {
                    connectionType +=  ';COPPER';
                    copperSpeed  = !category.availability.equalsIgnoreCase('available') ? 'unconfirmed' : category.downloadSpeed;
                }
            } 
            
            if(String.isNotBlank(category.ports)){
                availablePorts = category.ports;
            }            
        }
        
        if (String.isNotBlank(fibreFifaSpeed)) {
            downloadSpeed = fibreFifaSpeed;
        } else if (String.isNotBlank(fibreSpeed)) {
            downloadSpeed = fibreSpeed;
        } else if (String.isNotBlank(bondedCopperSpeed)) {
            downloadSpeed = bondedCopperSpeed;
        } else if (String.isNotBlank(copperSpeed)) {
            downloadSpeed = copperSpeed;
        } else {
            downloadSpeed = 'unconfirmed';
        }
        
        connectionType = connectionType.removeStart(';');
        System.debug('productCategories##'+productCategories);
        System.debug('serviceAddressId = ' + serviceAddressId);
        System.debug('connectionType = ' + connectionType);
        
       
        SMBCare_Address__c addr = [SELECT Id,PortsAvailable__c,connectivity_type__c, maximum_speed__c, isTVAvailable__c,isInternetAvailable__c,isMobileAvailable__c,isVoiceAvailable__c FROM SMBCare_Address__c where Id = :serviceAddressId LIMIT 1];
        addr.connectivity_type__c = connectionType;
        addr.maximum_speed__c = downloadSpeed;
        addr.isTVAvailable__c = isTVAvailable;
        addr.isInternetAvailable__c = isInternetAvailable;
        addr.isMobileAvailable__c = isMobileAvailable;
        addr.isVoiceAvailable__c = isVoiceAvailable;
        addr.PortsAvailable__c = availablePorts;
        update addr;
        
    }
    private static Integer getNumericPartFromString(String speedStr){
        if(String.isNotBlank(speedStr)){
            List<String> ls = speedStr.splitByCharacterType();
            if(ls!=null){
                for(String str:ls){
                    if(String.isNotBlank(str) && str.isNumeric()){
                        return Integer.valueOf(str);
                    }
                } 
            }                                 
        }
        return null;
    }
    public static List<ServicePath> getServicePath(Id serviceAddressId) {
        List<ServicePath> servicePaths = new List<ServicePath>();

        Map<String, String> inputMap = OrdrUtilities.constructServiceAddress(serviceAddressId);
        OrdrTnResourceInfoRetrievalConfiguration.ResourceConfigurationCollectionMessage resourceConfig = OrdrServicePathWsCallout.findResource(inputMap);
        if (resourceConfig != null) {
            for (TpInventoryResourceConfigV3.PhysicalResource physicalResource : resourceConfig.physicalResource) {
                ServicePath servicePath = new ServicePath();
                if (physicalResource.CharacteristicValue != null) {
                    for (TpCommonBaseV3.CharacteristicValue charVal : physicalResource.CharacteristicValue) {
                        if (charVal.Characteristic.Name == 'primaryTelephoneNumber') servicePath.primaryTelephoneNumber = charVal.Value[0];
                        if (charVal.Characteristic.Name == 'customerName') servicePath.customerName = charVal.Value[0];
                        if (charVal.Characteristic.Name == 'clearanceDate') servicePath.clearanceDate = charVal.Value[0];
                        if (charVal.Characteristic.Name == 'serviceTypeCode') servicePath.serviceTypeCode = charVal.Value[0];
                        if (charVal.Characteristic.Name == 'servicePathStatusCode') servicePath.servicePathStatusCode = charVal.Value[0];
                        if (charVal.Characteristic.Name == 'cablePairDedicatedPlantCode') servicePath.cablePairDedicatedPlantCode = charVal.Value[0];
                        if (charVal.Characteristic.Name == 'leasedLoopFlag') servicePath.leasedLoopFlag = charVal.Value[0];
                    }
                    servicePaths.add(servicePath);
                }
            }
        }

        return servicePaths;
    }
    
        public static GponDropStatus getGponDropStatus(Id serviceAddressId) {
        GponDropStatus dropStatus = new GponDropStatus();

        Map<String, String> inputMap = OrdrUtilities.constructServiceAddress(serviceAddressId);
        OrdrTnResQryResConfMsg.ResourceConfigurationMessage resourceConfig = OrdrQueryResourceWsCallout.queryResource(inputMap);
        if (resourceConfig != null) {
            for (TpInventoryResourceConfigV3.PhysicalResource physicalResource : resourceConfig.physicalResource) {
                system.debug(' physicalResource '+physicalResource.CharacteristicValue);
                if (physicalResource.CharacteristicValue != null) {
                    for (TpCommonBaseV3.CharacteristicValue charVal : physicalResource.CharacteristicValue) {
                         system.debug(' physicalResource charVal '+charVal.Characteristic.Name +' '+charVal.Value[0]);
                        if (charVal.Characteristic.Name == 'fsa') dropStatus.fsa = charVal.Value[0];
                        if (charVal.Characteristic.Name == 'dropType') dropStatus.dropType = charVal.Value[0];
                        if (charVal.Characteristic.Name == 'dropPermission') dropStatus.dropPermission = charVal.Value[0];
                        if (charVal.Characteristic.Name == 'dropStatus') dropStatus.dropStatus = charVal.Value[0];
                        if (charVal.Characteristic.Name == 'fsaReadyDate') dropStatus.fsaReadyDate = charVal.Value[0];
                        if (charVal.Characteristic.Name == 'coid') dropStatus.coid = charVal.Value[0];
                        if (charVal.Characteristic.Name == 'gisId') dropStatus.gisId = charVal.Value[0];
                        if (charVal.Characteristic.Name == 'roeStatus') dropStatus.roeStatus = charVal.Value[0];
                        if (charVal.Characteristic.Name == 'premiseType') dropStatus.premiseType = charVal.Value[0];
                    }
                }
            }
        }
        
        //save FIFA indicator and Drop Status
        SMBCare_Address__c addr = [SELECT Id, isFIFA__c, isDropReady__c FROM SMBCare_Address__c where Id = :serviceAddressId LIMIT 1];
		addr.isFIFA__c = String.isNotBlank(dropStatus.fsaReadyDate);
		addr.isDropReady__c = dropStatus.dropStatus.equalsIgnoreCase('Constructed');
        update addr;
          system.debug(' physicalResource addr '+addr);
        return dropStatus;
    }    

}