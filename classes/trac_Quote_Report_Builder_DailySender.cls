global class trac_Quote_Report_Builder_DailySender implements Schedulable{
   global void execute(SchedulableContext SC) {
      // Send the weekly email
      new trac_Quote_Report_Builder().sendTodaysDailyEmail();
   }
   private static testMethod void testSender() {
       trac_Quote_Report_Builder_DailySender sender = new trac_Quote_Report_Builder_DailySender();
       sender.execute(null);
   }
}