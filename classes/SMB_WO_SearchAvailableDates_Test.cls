/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class SMB_WO_SearchAvailableDates_Test {
    static testMethod void createOrder() {
    	smb_test_utility.createCustomSettingData();
        SMB_DummyWebServiceResponse_Test.intializeMockWebservice();        
        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'New Provide/Upgrade Order';
        insert testOpp_1;
        OpportunityContactRole opprole = new OpportunityContactRole(opportunityID = testOpp_1.Id,contactId = testOpp_1.Contract_Signor__c,isPrimary = true);
        insert opprole;
        system.debug(opprole);
        smb_test_utility.createPQLI('NewPQL',testOpp_1.Id);
        PageReference pageref = Page.SMB_WO_SelectAvailableDate;
        Test.setCurrentPage(pageref);        
        pageref.getparameters().put('OppID', testOpp_1.Id);
        ApexPages.StandardController sc_1 = new ApexPages.standardController(testOpp_1);        
        
        
        Work_Order__c workOrder = new Work_Order__c(Opportunity__c=testOpp_1.id,Job_Type__c = 'Test' ,Install_Type__c = 'Fieldwork', Status__c = 'Reserved', Product_Name__c = 'Test Product Name',
        						 Scheduled_Datetime__c = DateTime.now() , Time_Slot__c = 'Test Slot', Unlinked_Work_Order__c = True ,WFM_Number__c = 'Test Number', Scheduled_Due_Date_Location__c = 'Test Location', Permanent_Comments__c = 'test test');
        insert workOrder;
        
        Test.startTest();
        SMB_WO_SearchAvailableDates searchAvailableDates = new SMB_WO_SearchAvailableDates();
        searchAvailableDates.workOrder.Install_Type__c = '';
        searchAvailableDates.EventId = '2013-08-06 3:30:22 -TO- 2013-08-06 3:30:23';
        searchAvailableDates.EventSlot = 'Test';
        searchAvailableDates.RefreshCalender();
        pageref.getparameters().put('key', '1');
        system.assert(searchAvailableDates.mapWrapEnv.size() > 0);
        searchAvailableDates.selectProduct();
        searchAvailableDates.getAvailableBookingDateOption();
               
        system.assert(searchAvailableDates.selectedRows.size() > 0);
       
        list<OpportunityLineItem> olis = [select o.Id from OpportunityLineItem o where OpportunityId =:testOpp_1.Id 
                                                AND o.Prod_Type__c in ('VOICE','INTERNET')
                                                AND o.Element_Type__c != 'Over line Product'
                                                ];
        //system.assert(olis.size()>0);
        searchAvailableDates.workOrder.Permanent_Comments__c ='Test';
        searchAvailableDates.workOrder.Install_Type__c='Fieldwork';
        searchAvailableDates.workOrder.Duration__c = 0.5;
        searchAvailableDates.SearchAvailableSlots();
        searchAvailableDates.RefreshCalender();
        searchAvailableDates.CreateWorkOrder();
        searchAvailableDates.submitOrderOnClose();
        searchAvailableDates.readContact();
        searchAvailableDates.selectedEventId();
        searchAvailableDates.Cancel();
        searchAvailableDates.CancelUnlinkedWorkOrder();
        Test.stopTest();
        
    }
    static testMethod void searchAvailableSlots() {
        smb_test_utility.createCustomSettingData();
        SMB_DummyWebServiceResponse_Test.intializeMockWebservice();
        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'New Provide/Upgrade Order';
        insert testOpp_1;
        OpportunityContactRole opprole = new OpportunityContactRole(opportunityID = testOpp_1.Id,contactId = testOpp_1.Contract_Signor__c,isPrimary = true);
        insert opprole;
        system.debug(opprole);
        smb_test_utility.createPQLI('NewPQL',testOpp_1.Id);
        PageReference pageref = Page.SMB_WO_SelectAvailableDate;
        Test.setCurrentPage(pageref);        
        pageref.getparameters().put('OppID', testOpp_1.Id);
        ApexPages.StandardController sc_1 = new ApexPages.standardController(testOpp_1);        
        
        
        Work_Order__c workOrder = new Work_Order__c(Opportunity__c=testOpp_1.id,Job_Type__c = 'Test' ,Install_Type__c = 'Fieldwork', Status__c = 'Reserved', Product_Name__c = 'Test Product Name',Start_Date__c = Date.Today(), End_Date__c = Date.Today().AddDays(2),
        						 Scheduled_Datetime__c = DateTime.now() , Time_Slot__c = 'Test Slot', Unlinked_Work_Order__c = True ,WFM_Number__c = 'Test Number', Scheduled_Due_Date_Location__c = 'Test Location', Permanent_Comments__c = 'test test');
        insert workOrder;
        Test.startTest();
        system.assert(workOrder.Id != Null);
        SMB_WO_SearchAvailableDates searchAvailableDates = new SMB_WO_SearchAvailableDates();
        searchAvailableDates.workOrder = workOrder;
        pageref.getparameters().put('key', '1');
        system.assert(searchAvailableDates.mapWrapEnv.size() > 0);
        searchAvailableDates.selectProduct();       
        system.assert(searchAvailableDates.keyResult == 1);
        searchAvailableDates.workOrder.Install_Type__c = 'Software';
        searchAvailableDates.workOrder.Permanent_Comments__c = 'Text Text';
        searchAvailableDates.workOrder.Duration__c = 0.5;
        searchAvailableDates.workOrder.Start_Date__c = date.today().addDays(5);
        searchAvailableDates.SearchAvailableSlots();
        searchAvailableDates.next();
        searchAvailableDates.prev();
        searchAvailableDates.getWeeks();
        searchAvailableDates.getMonth();
        
        list<OpportunityLineItem> olis = [select o.Id,o.work_order__c,o.work_Order__r.Install_Type__c from OpportunityLineItem o where OpportunityId =:testOpp_1.Id 
                                                AND o.Prod_Type__c in ('VOICE','INTERNET')
                                                AND o.Element_Type__c != 'Over line Product'];
        searchAvailableDates.selectedRows = new list<SMB_WO_SearchAvailableDates.WrapperEnvelope>();
        olis[0].Work_Order__c = workOrder.Id;
        
        SMB_WO_SearchAvailableDates.WrapperEnvelope wrapper = new SMB_WO_SearchAvailableDates.WrapperEnvelope(olis[0]);
        searchAvailableDates.selectedRows.add(wrapper);
        searchAvailableDates.CancelWorkOrder();
        searchAvailableDates.getTimeZoneDifference(DateTime.now());
        searchAvailableDates.showPopUp();
        searchAvailableDates.showCancelPopUp();
        searchAvailableDates.CloseCancelRemarks();
        List<Work_Order_Info_Audit__c> workOrderAuditInfo = [SELECT Action__c, Error__c, Install_Type__c, Stack_Trace__c, Status__c, Web_Service_Work_Order_ID__c, WFM_Number__c, Work_Order_Record_ID__c FROM Work_Order_Info_Audit__c];
        
        System.debug('workOrderAuditInfo: ' + workOrderAuditInfo);
        
        Test.stopTest();
    }
    static testMethod void createOrder2() {
    	smb_test_utility.createCustomSettingData();
        SMB_DummyWebServiceResponse_Test.intializeMockWebservice();        
        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Locked_Order_Opportunity', Null, Null, false);
        testOpp_1.Type = 'New Provide/Upgrade Order';
        testOpp_1.Type = 'Order Request In Progress';
        insert testOpp_1;
        OpportunityContactRole opprole = new OpportunityContactRole(opportunityID = testOpp_1.Id,contactId = testOpp_1.Contract_Signor__c,isPrimary = true);
        insert opprole;
        system.debug(opprole);
        smb_test_utility.createPQLI('NewPQL',testOpp_1.Id);
        PageReference pageref = Page.SMB_WO_SelectAvailableDate;
        Test.setCurrentPage(pageref);        
        pageref.getparameters().put('OppID', testOpp_1.Id);
        ApexPages.StandardController sc_1 = new ApexPages.standardController(testOpp_1);        
        
        
        Work_Order__c workOrder = new Work_Order__c(Opportunity__c=testOpp_1.id,Job_Type__c = 'Test' ,Install_Type__c = 'Fieldwork', Status__c = 'Reserved', Product_Name__c = 'Test Product Name',
        						 Scheduled_Datetime__c = DateTime.now() , Time_Slot__c = 'Test Slot', Unlinked_Work_Order__c = True ,WFM_Number__c = 'Test Number', Scheduled_Due_Date_Location__c = 'Test Location', Permanent_Comments__c = 'test test');
        insert workOrder;
        
        Test.startTest();
        SMB_WO_SearchAvailableDates searchAvailableDates = new SMB_WO_SearchAvailableDates();
        searchAvailableDates.workOrder.Install_Type__c = '';
        searchAvailableDates.EventId = '2013-08-06 3:30:22 -TO- 2013-08-06 3:30:23';
        searchAvailableDates.EventSlot = 'Test';
        searchAvailableDates.RefreshCalender();
        pageref.getparameters().put('key', '1');
        system.assert(searchAvailableDates.mapWrapEnv.size() > 0);
        searchAvailableDates.selectProduct();
        searchAvailableDates.getAvailableBookingDateOption();
               
        system.assert(searchAvailableDates.selectedRows.size() > 0);
       
        list<OpportunityLineItem> olis = [select o.Id from OpportunityLineItem o where OpportunityId =:testOpp_1.Id 
                                                AND o.Prod_Type__c in ('VOICE','INTERNET')
                                                AND o.Element_Type__c != 'Over line Product'
                                                ];
        //system.assert(olis.size()>0);
        searchAvailableDates.workOrder.Permanent_Comments__c ='Test';
        searchAvailableDates.workOrder.Install_Type__c='Fieldwork';
        searchAvailableDates.workOrder.Duration__c = 0.5;
        searchAvailableDates.SearchAvailableSlots();
        searchAvailableDates.RefreshCalender();
        searchAvailableDates.CreateWorkOrder();
        searchAvailableDates.submitOrderOnClose();
        searchAvailableDates.readContact();
        searchAvailableDates.selectedEventId();
        searchAvailableDates.Cancel();
        searchAvailableDates.CancelUnlinkedWorkOrder();
        Test.stopTest();
        
    }
   /* static void initiateWebService(){
   }*/
}