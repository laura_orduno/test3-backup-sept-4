@isTest
private class OCOM_OrderProductTrigger_Helper_Test 
{
    static TestMethod void testMethod1() {
             Test.startTest();
             Schema.DescribeSObjectResult d = Schema.SObjectType.Account; 
              Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
              Schema.RecordTypeInfo rtByName =  rtMapByName.get('CAN');
              system.debug(' id'+rtByName.getRecordTypeId());
              Id recTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Billing').getRecordTypeId();
              Id recSerTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
              Id recRCIDTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId();
            
              Account RCIDacc = new Account(name='debs',recordtypeid=recRCIDTypeId);
              Account acc = new Account(name='Billing account',BAN_CAN__c='draft',recordtypeid=recTypeId,Billingstreet='street',BillingCity='city',BillingState='state',BillingPostalCode='m123456',BillingCountry='CAN');
              Account acc1 = new Account(name='Billing account',BAN_CAN__c='',recordtypeid=recTypeId,Billingstreet='street',BillingCity='city',BillingState='state',BillingPostalCode='m123456',BillingCountry='CAN');
              Account seracc = new Account(name='123 street',recordtypeid=recSerTypeId,parentid=RCIDacc.id);
              
              
              List<Account> acclist= new List<Account>();          
              acclist.add(RCIDacc);
              acclist.add(acc);
              acclist.add(seracc );
              insert acclist;
              
             smbcare_address__c address= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N5',FMS_Address_ID__c='987651',account__c=seracc.id); 
             insert address;
             
             SMBCare_Address__c smbCareAddr = new SMBCare_Address__c(Account__c=RCIDacc.Id, Postal_Code__c='M1T3N3', Street_Number__c='Street 1', Street_Name__c='Street Name 1', Province__c='ONT', Suite_Number__c='1001', COID__c='COID 1', Building_Number__c='Building 1', Country__c='CAN', FMS_Address_ID__c='FMSID 121');
             insert smbCareAddr;

              
            // First, set up test price book entries.
            // Insert a test product.
            Product2 prod = new Product2(Name = 'Laptop X200', 
                Family = 'Hardware');
            insert prod;
             System.assertEquals(prod.name, 'Laptop X200');
            // Get standard price book ID.
            // This is available irrespective of the state of SeeAllData.
            Id pricebookId = Test.getStandardPricebookId();
            
            // 1. Insert a price book entry for the standard price book.
            // Standard price book entries require the standard price book ID we got earlier.
            PricebookEntry standardPrice = new PricebookEntry(
                Pricebook2Id = pricebookId, Product2Id = prod.Id,
                UnitPrice = 10000, IsActive = true);
            insert standardPrice;
           
            // Create a custom price book
            Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
            insert customPB;
            
            // 2. Insert a price book entry with a custom price.
            PricebookEntry customPrice = new PricebookEntry(
                Pricebook2Id = customPB.Id, Product2Id = prod.Id,
                UnitPrice = 12000, IsActive = true);
            insert customPrice;
            contact c= new contact(firstname='abc',lastname='xyz ');
            insert c;
            work_order__c wO = new work_order__c(status__c= Label.OCOM_WFMReserved,WFM_number__c='0001212');   
            work_order__c wO2 = new work_order__c(status__c= Label.OCOM_WFMReserved); 
            work_order__c wO3 = new work_order__c(status__c= Label.OCOM_WFMReserved,WFM_number__c='0001212');   
            work_order__c wO4 = new work_order__c(status__c= Label.OCOM_WFMReserved);
            List<work_order__c> wOList = new List<work_order__c>(); wOList.add(wO);wOList.add(wO2);wOList.add(wO3);wOList.add(wO4);
            insert wOList; 
            order ord = new order(Ban__c='draft',accountId=acc.id,effectivedate=system.today(),status='not submitted',Pricebook2Id=customPB.id,CustomerAuthorizedById=c.id,service_address__c=address.id);   
            order ord2 = new order(Ban__c='draft',accountId=acc.id,effectivedate=system.today(),status='not submitted',Pricebook2Id=customPB.id,CustomerAuthorizedById=c.id,service_address__c=smbCareAddr.id);   
            List<Order>ordlist= new List<Order>();   
            ordList.add(ord);ordList.add(ord2);
            insert ordList;  
            System.assertEquals(ordList.size(), 2);
            
            orderItem  OLI2= new orderItem(work_order__c = wO.Id,UnitPrice=12,orderId= ord2.Id,orderMgmt_BPI_Id__c='9871379163517',vlocity_cmt__BillingAccountId__c=acc.id,vlocity_cmt__ServiceAccountId__c=seracc.id,Quantity=2,PricebookEntryId=customPrice.id);
            orderItem  OLI= new orderItem(work_order__c = wO2.Id,UnitPrice=12,orderId= ord.Id,vlocity_cmt__RecurringCharge__c=1000 ,vlocity_cmt__AssetReferenceId__c=OLI2.id,vlocity_cmt__BillingAccountId__c=acc1.id,vlocity_cmt__ServiceAccountId__c=seracc.id,Quantity=2,PricebookEntryId=customPrice.id);
            List<OrderItem>OLIlist= new List<OrderItem>();   
            OLIList.add(OLI2);OLIList.add(OLI);
            insert OLIList;
            System.assertEquals(OLIList.size(), 2);
            system.debug('Id  aAS'+String.valueOf(OLI.id).substring(0,15)+' '+OLI.id);
            //Insert a junction object----added for testing multisite---Danish Naas--08/12/2016
            //ServiceAddressOrderEntry__c juncObj = new ServiceAddressOrderEntry__c (Order__c=Ord.Id,Address__c = address.Id);
            //Insert juncObj;
            Asset testassetRec = new Asset(name='Asset1',accountId=RCIDacc.id,vlocity_cmt__AssetReferenceId__c=String.valueOf(OLI.id).substring(0,15),vlocity_cmt__BillingAccountId__c=null,orderMgmtId__c=null,vlocity_cmt__OrderId__c=null,vlocity_cmt__OrderProductId__c=null,vlocity_cmt__ServiceAccountId__c=seracc.Id);
            insert testassetRec;
            OLI.orderMgmt_BPI_Id__c='987654322114566';
            OLI2.orderMgmt_BPI_Id__c='98765222114567';
            List<OrderItem>updateOLIlist= new List<OrderItem>();   
            updateOLIlist.add(OLI2);
            updateOLIlist.add(OLI);
            upsert updateOLIlist;
            Set<Id> OLIIDList = new Set<Id>();
            OLIIDList.add(OLI.Id);
            OLIIDList.add(OLI2.Id);
            Set<String> OLIIDListString = new Set<String>();
            OLIIDListString.add(OLI.Id);
            OLIIDListString.add(OLI2.Id);
            OCOM_OrderProductBeforeTrigger_Helper OrdrProdBeforeTriggerClass = new OCOM_OrderProductBeforeTrigger_Helper();
            OrdrProdBeforeTriggerClass.DeleteServiceAddressOrderEntry(OLIIDList);
            //OrdrProdBeforeTriggerClass.cancelWorkOrderofDeletedOLIs(OLIIDListString);
            OCOM_OrderProductBeforeTrigger_Helper.isRunning();
            //OrdrProdBeforeTriggerClass.createAssets(OLIIDListString);
            
          
            Test.stopTest();
            
    }
    //Added by Manpreet --------------06/04/2017
    static TestMethod void testMethod2() {
    Test.startTest();
    Schema.DescribeSObjectResult d = Schema.SObjectType.Account; 
              Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
              Schema.RecordTypeInfo rtByName =  rtMapByName.get('CAN');
              system.debug(' id'+rtByName.getRecordTypeId());
              Id recTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Billing').getRecordTypeId();
              Id recSerTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
              Id recRCIDTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId();
            
              Account RCIDacc = new Account(name='debs',recordtypeid=recRCIDTypeId);
              Account acc = new Account(name='Billing account',BAN_CAN__c='draft',recordtypeid=recTypeId,Billingstreet='street',BillingCity='city',BillingState='state',BillingPostalCode='m123456',BillingCountry='CAN');
              Account acc1 = new Account(name='Billing account',BAN_CAN__c='',recordtypeid=recTypeId,Billingstreet='street',BillingCity='city',BillingState='state',BillingPostalCode='m123456',BillingCountry='CAN');
              Account seracc = new Account(name='123 street',recordtypeid=recSerTypeId,parentid=RCIDacc.id);
              
              
              List<Account> acclist= new List<Account>();          
              acclist.add(RCIDacc);
              acclist.add(acc);
              acclist.add(seracc );
              insert acclist;
              
             smbcare_address__c address= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N5',FMS_Address_ID__c='987651',account__c=seracc.id); 
             insert address;
             
             SMBCare_Address__c smbCareAddr = new SMBCare_Address__c(Account__c=RCIDacc.Id, Postal_Code__c='M1T3N3', Street_Number__c='Street 1', Street_Name__c='Street Name 1', Province__c='ONT', Suite_Number__c='1001', COID__c='COID 1', Building_Number__c='Building 1', Country__c='CAN', FMS_Address_ID__c='FMSID 121');
             insert smbCareAddr;

              
            // First, set up test price book entries.
            // Insert a test product.
            Product2 prod = new Product2(Name = 'Laptop X200', 
                Family = 'Hardware');
            insert prod;
             System.assertEquals(prod.name, 'Laptop X200');
            // Get standard price book ID.
            // This is available irrespective of the state of SeeAllData.
            Id pricebookId = Test.getStandardPricebookId();
            
            // 1. Insert a price book entry for the standard price book.
            // Standard price book entries require the standard price book ID we got earlier.
            PricebookEntry standardPrice = new PricebookEntry(
                Pricebook2Id = pricebookId, Product2Id = prod.Id,
                UnitPrice = 10000, IsActive = true);
            insert standardPrice;
           
            // Create a custom price book
            Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
            insert customPB;
            
            // 2. Insert a price book entry with a custom price.
            PricebookEntry customPrice = new PricebookEntry(
                Pricebook2Id = customPB.Id, Product2Id = prod.Id,
                UnitPrice = 12000, IsActive = true);
            insert customPrice;
            contact c= new contact(firstname='abc',lastname='xyz ');
            insert c;
            work_order__c wO = new work_order__c(status__c= Label.OCOM_WFMReserved,WFM_number__c='0001212');   
            work_order__c wO2 = new work_order__c(status__c= Label.OCOM_WFMReserved); 
            work_order__c wO3 = new work_order__c(status__c= Label.OCOM_WFMReserved,WFM_number__c='0001212');   
            work_order__c wO4 = new work_order__c(status__c= Label.OCOM_WFMReserved);
            List<work_order__c> wOList = new List<work_order__c>(); wOList.add(wO);wOList.add(wO2);wOList.add(wO3);wOList.add(wO4);
            insert wOList; 
            order ord = new order(Ban__c='draft',accountId=acc.id,effectivedate=system.today(),status='not submitted',Pricebook2Id=customPB.id,CustomerAuthorizedById=c.id,service_address__c=address.id);   
            order ord2 = new order(Ban__c='draft',accountId=acc.id,effectivedate=system.today(),status='not submitted',Pricebook2Id=customPB.id,CustomerAuthorizedById=c.id,service_address__c=address.id);   
            List<Order>ordlist= new List<Order>();   
            ordList.add(ord);ordList.add(ord2);
            insert ordList;  
            System.assertEquals(ordList.size(), 2);
            
            orderItem  OLI2= new orderItem(work_order__c = wO.Id,UnitPrice=12,orderId= ord2.Id,orderMgmt_BPI_Id__c='9871379163517',vlocity_cmt__RecurringCharge__c = 100,vlocity_cmt__BillingAccountId__c=acc.id,vlocity_cmt__ServiceAccountId__c=seracc.id,Quantity=2,PricebookEntryId=customPrice.id);
            orderItem  OLI= new orderItem(work_order__c = wO2.Id,UnitPrice=12,orderId= ord.Id,vlocity_cmt__RecurringCharge__c = 100,vlocity_cmt__AssetReferenceId__c=OLI2.id,vlocity_cmt__BillingAccountId__c=acc1.id,vlocity_cmt__ServiceAccountId__c=seracc.id,Quantity=2,PricebookEntryId=customPrice.id);
            List<OrderItem>OLIlist= new List<OrderItem>();   
            OLIList.add(OLI2);OLIList.add(OLI);
            insert OLIList;
            System.assertEquals(OLIList.size(), 2);
            system.debug('Id  aAS'+String.valueOf(OLI.id).substring(0,15)+' '+OLI.id);
            
            ServiceAddressOrderEntry__c JuncObj1 = new ServiceAddressOrderEntry__c (Order__c=OLI.orderId,Address__c = address.Id);
            ServiceAddressOrderEntry__c JuncObj2 = new ServiceAddressOrderEntry__c (Order__c=OLI.orderId,Address__c = smbCareAddr.Id );
            
            system.debug('OLI.Service_Address__c------->' + OLI.Service_Address__r.Id);
            system.debug('OLI.Service_Address__c------->' + OLI2.Service_Address__r.Id);
            system.debug('OLI.Id------->' + OLI.Id);
            List<ServiceAddressOrderEntry__c> SAlist= new List<ServiceAddressOrderEntry__c>();  
            SAlist.add(JuncObj1 ); SAlist.add(JuncObj2 );
            insert SAlist;
            System.assertEquals(SAlist.size(), 2);
            system.debug('SAlist------->' + SAlist);
          
            Set<Id> OLIIDList = new Set<Id>();
            OLIIDList.add(address.Id);
            OLIIDList.add(smbCareAddr.Id);
            
            OLI.Service_Address__c = JuncObj1.Address__c ;
            OLI2.Service_Address__c = JuncObj2.Address__c ;

      OCOM_OrderProductBeforeTrigger_Helper OrdrProdBeforeTriggerClass = new OCOM_OrderProductBeforeTrigger_Helper();
      OrdrProdBeforeTriggerClass.UpdateServiceAddressOrderEntry(OLIIDList,OLI.orderId,OLIlist);
     
      
      
    Test.stopTest();
    }
   
    @isTest
    private static void cancelWorkOrderofDeletedOLIsTest(){
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        Test.startTest();
        Set<String> oliIdSet=new Set<String>();
        List<OrderItem> oiList=[select id,vlocity_cmt__RootItemId__c from orderitem where order.parentid__c=:orderId];
        for(OrderItem oiInst:oiList){			
            oliIdSet.add(oiInst.id);
        }
        OCOM_OrderProductBeforeTrigger_Helper obj = new OCOM_OrderProductBeforeTrigger_Helper();
        obj.cancelWorkOrderofDeletedOLIs(oliIdSet);
        Test.stopTest();
        
        
    }
   
}