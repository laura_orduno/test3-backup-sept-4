//Generated by wsdl2apex

public class SMB_Appointment_FWAppointmentSvcReqRes {
    public class SearchAvailableAppointmentList {
        public SMB_Appointment_WFMgmtOrderTypes_v2.InputHeader inputHeader;
        public SMB_Appointment_WFMgmtOrderTypes_v2.WorkOrder workOrder;
        public String appointmentProfileName;
        public DateTime startDate;
        public DateTime endDate;
        public Boolean gradeAppointmentInd;
        public Boolean fullSearchInd;
        private String[] inputHeader_type_info = new String[]{'inputHeader','http://xmlschema.tmi.telus.com/xsd/Resource/Resource/WorkForceManagementOrderTypes_v2','InputHeader','1','1','false'};
        private String[] workOrder_type_info = new String[]{'workOrder','http://xmlschema.tmi.telus.com/xsd/Resource/Resource/WorkForceManagementOrderTypes_v2','WorkOrder','1','1','false'};
        private String[] appointmentProfileName_type_info = new String[]{'appointmentProfileName','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] startDate_type_info = new String[]{'startDate','http://www.w3.org/2001/XMLSchema','dateTime','1','1','false'};
        private String[] endDate_type_info = new String[]{'endDate','http://www.w3.org/2001/XMLSchema','dateTime','1','1','false'};
        private String[] gradeAppointmentInd_type_info = new String[]{'gradeAppointmentInd','http://www.w3.org/2001/XMLSchema','boolean','1','1','false'};
        private String[] fullSearchInd_type_info = new String[]{'fullSearchInd','http://www.w3.org/2001/XMLSchema','boolean','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/srv/RMO/ProcessMgmt/FieldWorkAppointmentServiceRequestResponse_v2','true','false'};
        private String[] field_order_type_info = new String[]{'inputHeader','workOrder','appointmentProfileName','startDate','endDate','gradeAppointmentInd','fullSearchInd'};
    }
    public class SearchAvailableAppointmentListResponse {
        public SMB_Appointment_WFMgmtOrderTypes_v2.WorkOrder workOrder;
        public SMB_Appointment_WFMgmtOrderTypes_v2.AvailableAppointment[] availableAppointmentList;
        private String[] workOrder_type_info = new String[]{'workOrder','http://xmlschema.tmi.telus.com/xsd/Resource/Resource/WorkForceManagementOrderTypes_v2','WorkOrder','0','1','false'};
        private String[] availableAppointmentList_type_info = new String[]{'availableAppointmentList','http://xmlschema.tmi.telus.com/xsd/Resource/Resource/WorkForceManagementOrderTypes_v2','AvailableAppointment','0','500','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/srv/RMO/ProcessMgmt/FieldWorkAppointmentServiceRequestResponse_v2','true','false'};
        private String[] field_order_type_info = new String[]{'workOrder','availableAppointmentList'};
    }
}