@isTest(SeeAllData=true)
private class QuoteSendAgreementControllerTest {
	testMethod static void test() {
        Web_Account__c account = new Web_Account__c(Name='UnitTest', phone__c='6049969449');
        insert account;
        
        Contact c = new Contact(FirstName='John',LastName='Adams',Email='test@telus.com',Web_Account__c=account.Id, phone='6049969449');
        insert c;
        
        Opportunity opp = new Opportunity(Name='Test',Web_Account__c=account.Id,CloseDate=System.today(),StageName='Prospecting');
        insert opp;
        
        SBQQ__Quote__c quote = new SBQQ__Quote__c(SBQQ__Opportunity__c=opp.Id);
        quote.Completed__c = true;
        quote.Rate_Band__c = 'F';
        quote.Province__c = 'BC';
        insert quote;
        
        ApexPages.currentPage().getParameters().put('qids', quote.Id);
        ApexPages.currentPage().getParameters().put('aid', account.Id);
        QuoteSendAgreementController target = new QuoteSendAgreementController();
        target.selectedContactId = c.Id;
        target.onSend();
        //target.addErrorMessage('test');
        
        Quote_BSO__c qbso = new Quote_BSO__c(Name = quote.Id);
        insert qbso;
    }
}