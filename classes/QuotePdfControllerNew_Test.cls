/*******************************************************************************
     * Project      : OCOM
     * Helper       : QuotePdfControllerNew
     * Test Class   : QuotePdfControllerNew_Test
     * Description  : Test class for QuotePdfControllerNew(Apex class written by Priya to Create a PDF file.)
     * Author       : Arvind Yadav
     * Date         : January-22-2016
     *
     * Modification Log:
     * ------------------------------------------------------------------------------
     * Ver      Date                Modified By           Description
     * ------------------------------------------------------------------------------
     * 1.0     
     
     *******************************************************************************/
@isTest
private class QuotePdfControllerNew_Test {

    static testMethod void savePdfTest() {
        // TO DO: implement unit test
       Test.StartTest();
       Opportunity Opp= new Opportunity(name='test11',StageName='Discovery',CloseDate=System.today().addDays(5));
	   insert Opp; 
	    
	    Quote Qt= new Quote(Name='test Quote11',opportunityid=opp.id);
		insert Qt;
        
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(Qt);
        
        ApexPages.currentPage().getParameters().put('Id',Qt.id);
        ApexPages.currentPage().getParameters().put('ignoredColumns','');
        
       QuotePdfControllerNew QPdfController = new QuotePdfControllerNew(sc);
       List<QuotePdfControllerNew.qLineItem> lineItems = new List<QuotePdfControllerNew.qLineItem>();
       
      
                 
       QPdfController.getQuote();
       QPdfController.savePdf();
       QPdfController.initializeData();
       
       QPdfController.getLineItems();
        
        Test.StopTest();
    }
}