/**
 * Controller for Agreement Creation from Deal Support Object
 * This Controller will handle the autopopulation of field from deal support and link the new agreement id to the related list records
 * 
 * Prarameter API:
 * id    : Id of  the deal support record which agrrement will be linked to 
 * rln : Realetd list names which should be link to agreement via agreement id and should be provied via comma seperated. 
 * example rln=Rate_Plan_Item__c,Subsidiary_Account__c,Agreement_Contact__c
 * 
 */ 
public class AgreementCreateController {
    
    public id  RecordId {get;set;}
    public id  ExistingAgreementId {get;set;}
    public Offer_House_Demand__c dealSupport {get;set;}
    public Apttus__APTS_Agreement__c agreement {get;set;}
    private static String DS_COPY_FS = 'CopyFields';
    private List<String> relatedObjectNameList = new List<String>();
    public static String STATUS_CONTRACT_RESUBMITTED = 'Contract Resubmitted';
	public static String STATUS_CONTRACT_INPROGRESS = 'Contract In Progress';
    public static String STATUS_CONTRACT_CREATE = 'Create Contract';
    public static String STATUS_CONTRACT_READY = 'Contract Ready';  
    
    private static List<String> dealfilter = new List<String>{'Deal_Support__c =:dealId'};
    
    public AgreementCreateController(){
             
    }
    
    private void loadDealSupport(){
         List<String> filter = new List<String>{'id =:RecordId'};
         String query = Util.queryBuilder('Offer_House_Demand__c', DS_COPY_FS, filter);
         dealSupport= Database.query(query);
         this.ExistingAgreementId = dealSupport.Existing_Agreement__c;      
    }
  
    public PageReference doCancel(){
        return  new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
    }
    
    public PageReference onLoad(){
        try{
        	initRequestParameters();
            String relatedObjectName = ApexPages.currentPage().getParameters().get('rln');
            if(relatedObjectName !=null){
               relatedObjectNameList.addAll(relatedObjectName.split(','));
            }
            System.debug(LoggingLevel.INFO,'CreateAgreementController RecordId ' + RecordId );
            System.debug(LoggingLevel.INFO,'CreateAgreementController relatedObjectNameList ' + relatedObjectNameList );
            loadDealSupport();
            PageReference pr =null;
            if('current'.equalsIgnoreCase(dealSupport.type__c) &&
               ( STATUS_CONTRACT_READY.equalsIgnoreCase(dealSupport.Status__c) || STATUS_CONTRACT_CREATE.equalsIgnoreCase(dealSupport.Status__c) || STATUS_CONTRACT_RESUBMITTED.equalsIgnoreCase(dealSupport.Status__c) || STATUS_CONTRACT_INPROGRESS.equalsIgnoreCase(dealSupport.Status__c))){
                id agreementId= getAgreement(dealSupport.id);
                System.debug(LoggingLevel.INFO,'agreementId ' + agreementId);   
                if(agreementId==null){ 
                    agreement=createAgreement();
                    agreementId =agreement.id;
                    createRelatedRecords(agreementId);
                    update agreement;
                    pr= new PageReference('/'+agreementId);
               } else{
                   agreement= new Apttus__APTS_Agreement__c(id=agreementId);
                   populateAgreementFields(agreement);
                   createRelatedRecords(agreementId);
                   update agreement;
                   pr = new PageReference('/'+agreementId);
                   pr.setRedirect(true);
               }
                return pr;
           } else{
                 ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,Label.Agreement_Launch_Error);
                 ApexPages.addMessage(myMsg);
                 return null;
           }
       }catch(Exception e){
			 System.debug(LoggingLevel.ERROR, e.getStackTraceString());           
             ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,e.getMessage());
             ApexPages.addMessage(myMsg);
             return null; 
        }     
    }
       
    public static Id getAgreement(Id offerHouseid){
        List<Apttus__APTS_Agreement__c> agreement= [Select id from Apttus__APTS_Agreement__c where  Offer_House_Demand__c =:offerHouseid Limit 1];
        if(agreement!=null && agreement.size() !=0 ){
            return  agreement.get(0).id;
        }else{
            return null;            
        }
    }
    
    public void createRelatedRecords(Id agreementId){
        linkRelatedListItemsToSubAccount();
        linkRelatedListItemsToAgreement(agreementId,relatedObjectNameList);
        populateSubsidiaryAccounts();
        
        
    }
    
    private Apttus__APTS_Agreement__c createAgreement(){
        Apttus__APTS_Agreement__c  agrmnt = null;
        if(this.dealSupport.TYPE_OF_CONTRACT__c ==null){
            String field = Schema.sObjectType.Offer_house_demand__c.fields.TYPE_OF_CONTRACT__c.Label;
            throw new RequestParmeterException(field +' is Empty');
        }
        if(ExistingAgreementId!=null){
            if('Renew'.equalsIgnoreCase(this.dealSupport.TYPE_OF_CONTRACT__c)){
                agrmnt = Apttus.AgreementWebService.renewAgreement(ExistingAgreementId);
            }else if('Amendment'.equalsIgnoreCase(this.dealSupport.TYPE_OF_CONTRACT__c)){
                agrmnt = Apttus.AgreementWebService.amendAgreement(ExistingAgreementId);    
            }
            agrmnt.Apttus__Parent_Agreement__c = ExistingAgreementId;
            populateAgreementFields(agrmnt);
            insert agrmnt;
            System.debug(LoggingLevel.INFO,'createAgreement New Aggreement AMD/Renew ' + agrmnt.id );
            return agrmnt;
        }else{
            agrmnt = new Apttus__APTS_Agreement__c();
            populateAgreementFields(agrmnt);
            insert agrmnt;
            System.debug(LoggingLevel.INFO,'createAgreement New Aggreement ' + agrmnt.id );
            return agrmnt;
        }    
    }
    
    private void populateAgreementFields(Apttus__APTS_Agreement__c agrmnt){
        //Deal Support Level Fields
		 agrmnt.Apttus__Account__c = this.dealSupport.Account_Id__c;
        agrmnt.Apttus__Related_Opportunity__c = this.dealSupport.OPPORTUNITY__c;
        System.debug(LoggingLevel.INFO,'ExistingAgreementId :' + ExistingAgreementId);
        if('Renew'.equalsIgnoreCase(this.dealSupport.TYPE_OF_CONTRACT__c) || 'Amendment'.equalsIgnoreCase(this.dealSupport.TYPE_OF_CONTRACT__c)){
            Apttus__APTS_Agreement__c exAg = [Select RecordTypeId from Apttus__APTS_Agreement__c where  id =:ExistingAgreementId];
            agrmnt.RecordTypeId = exAg.RecordTypeId;
            agrmnt.Apttus__Subtype__c = this.dealSupport.TYPE_OF_CONTRACT__c;
        }else{
            agrmnt.RecordTypeId = Util.getRecordTypeIdByName(agrmnt, this.dealSupport.TYPE_OF_CONTRACT__c);    
        }
        agrmnt.Apttus__Term_Months__c = this.dealSupport.TERM_OF_CONTRACT__c; 
        agrmnt.Offer_House_Demand__c = this.dealSupport.id;
        agrmnt.Apttus__Primary_Contact__c = this.dealSupport.Primary_Contact__c;
        agrmnt.TELUS_Representative__c = this.dealSupport.TELUS_Representative__c;
        agrmnt.Authorized_Customer_Representative__c = this.dealSupport.Authorized_Customer_Representative__c;
        agrmnt.Services__c = this.dealSupport.Services__c ==null ?'':this.dealSupport.Services__c.replaceAll(';','\n');
        agrmnt.Hardware_Refresh_Type__c = this.dealSupport.Hardware_Refresh_Type__c ==null ?'':this.dealSupport.Hardware_Refresh_Type__c.replaceAll(';','\n');
        agrmnt.Selected_Rate_Plans__c = getSelectedRatePlansTypes();
        agrmnt.Support_Restrictions__c = this.dealSupport.Comments__c;
        agrmnt.Order_Fulfillment_Type__c = this.dealSupport.Order_Fulfillment_Type__c;
        // Update DS Request Status        
        if(!STATUS_CONTRACT_READY.equalsIgnoreCase(this.dealSupport.Status__c)){
        	this.dealSupport.Status__c  = STATUS_CONTRACT_INPROGRESS;
        }
        this.dealSupport.Agreement_Id_Apttus__c = agrmnt.id;
        update dealSupport;
    }
    
    public void linkRelatedListItemsToAgreement(id agreementId,List<String> relatedObjectList){
        Map<id,sobject> itemsToUpdate = new  Map<id,sobject>();
        for(String relatedObjName : relatedObjectList){
            List<sObject> recordToUpdate = getUpdateRecordForRelatedObject(relatedObjName);
            for(sObject obj : recordToUpdate){
                obj.put('Agreement__c',agreementId);
                if('Subsidiary_Account__c'.equalsIgnorecase(relatedObjName)){
                    obj.put('Order_Fulfillment_Type__c',agreement.Order_Fulfillment_Type__c);
                }else if((('Agreement_Contact__c'.equalsIgnorecase(relatedObjName) ||'Agreement_Dealer__c'.equalsIgnorecase(relatedObjName))) && obj.get('Subsidiary_Account__c')!= null){
                    obj.put('agreement__c',null);
                }
                itemsToUpdate.put(obj.id,obj);
            }
        }
       	update itemsToUpdate.values();
    }
     public void linkRelatedListItemsToSubAccount(){
        Id dealId = dealSupport.id;
        List<sobject> itemstoInsert = new List<sobject>();
        String[] relListtoLink = new String[]{'Agreement_Contact__c','Agreement_Dealer__c'};
        String subAccQ = Util.queryBuilder('Subsidiary_Account__c', new List<String>{'id'}, dealfilter);
        List<sOBject> subAccList = Database.query(subAccQ); 
        
        for(String relatedObjName : relListtoLink){
            List<String> updatableFields = Util.getUpdatableFields(relatedObjName);
            String query = Util.queryBuilder(relatedObjName,updatableFields,dealfilter);
            List<sOBject> recordToUpdate = Database.query(query);
       		for(sObject obj : recordToUpdate){
                for(sOBject subAcc : subAccList){
                    if(obj.get('agreement__c') == null ){
                        sObject newObj = obj.clone(false, true);
                        newObj.put('Subsidiary_Account__c',subAcc.Id);
                        newObj.put('Deal_Support__c',null);
                        itemstoInsert.add(newObj);
                    }    
                }    
            }
        }
        insert itemstoInsert;
    }
    public void populateSubsidiaryAccounts(){
        agreement.Affiliated_Accounts__c ='';
        Id dealId = dealSupport.id;
        String query = Util.queryBuilder('Subsidiary_Account__c', new List<String>{'Account_Name__c'}, dealfilter);
        List<sOBject> objects = Database.query(query);
        for(sOBject obj : objects ){
           agreement.Affiliated_Accounts__c +=obj.get('Account_Name__c') +'\n';
        }
            
    }
        
    public List<sobject> getUpdateRecordForRelatedObject(String objectName){
        Id dealId = dealSupport.id;
        String query = Util.queryBuilder(objectName,new List<String>{'Id', 'Agreement__c'},dealfilter);
        List<sOBject> objects = Database.query(query);
        return objects;
    }
    
    private String getSelectedRatePlansTypes(){
       Set<string> selectedPlans = new Set<String>(); 
       List<Rate_Plan_Item__c> toUpdate = new List<Rate_Plan_Item__c>();
       List<String> queryFields = new List<String>{'Id', 'Plan_Type__c','RP_Included_Features__c','RP_Included_Features_Other__c'};
       Id dealId = this.dealSupport.id;
       String query =  Util.queryBuilder('Rate_Plan_Item__c',queryFields,dealfilter);
       List<Rate_Plan_Item__c> ratePlanList = Database.query(query); 
       for(Rate_Plan_Item__c rpItem : ratePlanList){
            String features = rpItem.RP_Included_Features__c ==null? '' : rpItem.RP_Included_Features__c;
            String otherfeatures = rpItem.RP_Included_Features_Other__c == null? '' : rpItem.RP_Included_Features_Other__c; 
            rpItem.Included_Features_All__c = features.replaceAll(';','\n') +'\n'+ otherfeatures;
            toUpdate.add(rpItem);
            selectedPlans.add(rpItem.Plan_Type__c);
       }
       List<String> asLIst = new List<String>();
       asList.addAll(selectedPlans);
       update toUpdate;  
       return String.join(asList, ',');
        
    }
    private void initRequestParameters(){
        String recId = ApexPages.currentPage().getParameters().get('id');
        System.debug(LoggingLevel.INFO, 'recId ' + recId);
        if(recId==null || String.isEmpty(recId)){
            throw new RequestParmeterException('Parameter "Id" can not be null or empty');
        }else if((Id.valueOf(recId).getSobjectType() != Schema.Offer_House_Demand__c.SObjectType)){
            throw new RequestParmeterException('Id Should be Type:' + Schema.Offer_House_Demand__c.SObjectType +  ' got ' + Id.valueOf(recId).getSobjectType());
        }else{
            this.recordId = recId;
        }
    }
    
    public class RequestParmeterException extends Exception{
        
    }
   
}