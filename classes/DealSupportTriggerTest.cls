@isTest
public class DealSupportTriggerTest {

    @isTest
    public static void testDealSupportTrigger(){
        Test.startTest();
        Offer_House_Demand__c deal = new Offer_House_Demand__c();
        map<string,schema.recordtypeinfo> recordTypeMap=offer_house_demand__c.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        deal.recordtypeid=recordTypeMap.get('WLN Deal Support').getrecordtypeid();
        deal.type_of_contract__c='CCA';
        deal.signature_required__c='Yes';
        insert deal;
        deal=loadOBject(deal.id);
        System.assertEquals('In Progress', deal.Status__c);
        
        deal.Status__c = 'Submitted';
        update deal;
        deal= loadOBject(deal.id);
        System.assertEquals('Submitted', deal.Status__c);
        
        deal.Status__c = 'Approved';
        update deal; 
        deal= loadOBject(deal.id);
        System.assert(deal.Version_Number__c>0);
        
        deal.Status__c = 'Cancelled';
        update deal; 
        deal=loadOBject(deal.id);
        System.assertEquals('Cancelled', deal.Status__c);
        
        deal.Status__c = 'Review Required';
        update deal; 
        System.debug(LoggingLevel.INFO,'testDealSupportTrigger SOQL Usage : ' +  Limits.getQueries());
        
    }
    
     @isTest
    public static void testWLNDealsWithOPI(){
    	Test.startTest();
        Date todayDate = Date.today();
        
        Opportunity opp = new Opportunity(Name='Test',StageName='Open',CloseDate=todayDate);
        insert opp;
        Product2 p = new Product2(Name='Test', isActive=true);
        insert p;
        Opp_Product_Item__c opi = new Opp_Product_Item__c();
        opi.Opportunity__c = opp.id;
        opi.Product__c = p.id;
        insert opi;
        
        Offer_House_Demand__c deal = new Offer_House_Demand__c();
        deal.OPPORTUNITY__c = opp.id;
        deal.RecordTypeId = Util.getRecordTypeIdByName(deal,'WLN Deal Support');
        deal.type_of_contract__c='CCA';
        deal.signature_required__c='Yes';
        try{
            insert deal;
        }catch(exception e){
            // No idea what flow this is triggering
        }
    }  
    
    public static Offer_House_Demand__c loadOBject(Id oId){
        return  [select Name, Status__c,Version_Number__c from Offer_House_Demand__c where id=:oId];
    }
    
   // @isTest
    public static void testDealSupportTriggerBulk(){
        List<Offer_House_Demand__c> deals = new List<Offer_House_Demand__c>();
        for(integer i=0;i<100;i++){
            deals.add(new Offer_House_Demand__c());
        }
        insert deals;
        map<string,schema.recordtypeinfo> recordTypeMap=offer_house_demand__c.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        for(Offer_House_Demand__c deal : deals){
            deal.Status__c = 'Approved';
            deal.recordtypeid=recordTypeMap.get('WLN Deal Support').getrecordtypeid();
            deal.type_of_contract__c='CCA';
            deal.signature_required__c='Yes';
        }
        Test.startTest();
        System.debug(LoggingLevel.INFO,'testDealSupportTriggerBulk Before Update call  SOQL Usage : ' +  Limits.getQueries());
        update deals;
        System.debug(LoggingLevel.INFO,'deals.size() : ' +  deals.size());
        System.debug(LoggingLevel.INFO,'testDealSupportTriggerBulk SOQL Usage : ' +  Limits.getQueries());
        Test.StopTest();
        System.debug(LoggingLevel.INFO,'StopTest() : ' +  Limits.getQueries());
    }
    
}