/*****************************************************
    Name: EventHandlerInterface
    Usage: This is interface class which should be implemented for each external event type
    Author – Sandip Chaudhari (IBM)
    Revision History -
    Created By/Date - Sandip Chaudhari / 04 July 2016
    
    Updated By/Date/Reason for Change - 
    
******************************************************/
global interface EventHandlerInterface{
    void processEvent(List<External_Event__c> eventObjects, List<ExternalEventRequestResponseParameter.ResponseEventData> responseData);
    void validateEventDetails(String eventDetails, List<ExternalEventRequestResponseParameter.EventError> errorList);
}