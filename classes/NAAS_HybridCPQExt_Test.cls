/*
    *******************************************************************************************************************************
    Class Name:    naas_HybridCPQExt_Test
    Purpose:       Test Class for naas_HybridCPQExt        
    Created by:    Danish Mushtaq    23-Nov-2016     No previous version
    Modified by:      
    *******************************************************************************************************************************
*/
@isTest
public class NAAS_HybridCPQExt_Test{
    
    @IsTest
    private static void testInit(){
        
        Schema.DescribeSObjectResult d = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
        Schema.RecordTypeInfo rtByName =  rtMapByName.get('CAN');
        Id recTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Billing').getRecordTypeId();
        Id recSerTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        Id recRCIDTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId();
        
        Account RCIDacc = new Account(name='debs',recordtypeid=recRCIDTypeId);
        Account acc = new Account(name='Billing account', BAN_CAN__c='draft', recordtypeid=recTypeId);
        Account serAcc = new Account(name='123 street', recordtypeid=recSerTypeId, parentid=RCIDacc.id);
        
        
          
          
        List<Account> acclist= new List<Account>();          
        acclist.add(RCIDacc);
        acclist.add(acc);
        acclist.add(serAcc);
        
        insert acclist;
          
        SMBCare_Address__c address = new SMBCare_Address__c(account__c=serAcc.id);
        Insert address; 
         
        Product2 prod = new Product2(Name = 'Laptop X200', Family = 'Hardware');
        insert prod;

        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod.Id, UnitPrice = 10000, IsActive = true);
        insert standardPrice;

        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;

        PricebookEntry customPrice = new PricebookEntry(Pricebook2Id = customPB.Id, Product2Id = prod.Id, UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        Contact c= new Contact(FirstName='abc', LastName='xyz');
        insert c;
        
        List< Credit_Assessment__c> CARList= new List<Credit_Assessment__c>();
        Credit_Assessment__c CAR= new Credit_Assessment__c(CAR_Status__c='test', CAR_Result__c='Denied');
        CARList.add(CAR);
        Credit_Assessment__c CAR2= new Credit_Assessment__c(CAR_Status__c='test',CAR_Result__c='Denied');
        CARList.add(CAR2);
        insert CARList;
         
        Order ord = new Order(Credit_Assessment__c=car.id,Ban__c='draft',Reason_Code__c='34aas',General_Notes_Remarks__c='123123', accountId=serAcc.id, effectivedate=system.today(), status='not submitted', Pricebook2Id=customPB.id, CustomerAuthorizedById=c.id, service_address__c=address.id);   
        Order ord2 = new Order(Credit_Assessment__c=car2.id,Ban__c='draft',Reason_Code__c='34aas',General_Notes_Remarks__c='123', accountId=acc.id, effectivedate=system.today(), status='not submitted', Pricebook2Id=customPB.id, CustomerAuthorizedById=c.id, service_address__c=address.id);   
        List<Order> ordlist = new List<Order>();   
        ordList.add(ord);ordList.add(ord2);
        insert ordList; 
          
        OrderItem OLI2 = new OrderItem(UnitPrice=12, orderId= ord2.Id, orderMgmt_BPI_Id__c='9871379163517', vlocity_cmt__BillingAccountId__c=acc.id, vlocity_cmt__ServiceAccountId__c=serAcc.id, Quantity=2, PricebookEntryId=customPrice.id);
        OrderItem  OLI = new OrderItem(UnitPrice=12, orderId= ord.Id, vlocity_cmt__AssetReferenceId__c=OLI2.id, vlocity_cmt__BillingAccountId__c=acc.id, vlocity_cmt__ServiceAccountId__c=serAcc.id, Quantity=2, PricebookEntryId=customPrice.id);
          
        List<OrderItem>OLIlist= new List<OrderItem>();   
        OLIList.add(OLI2);
        OLIList.add(OLI);
        insert OLIList;
        
     //   Address__c siteAdd = new Address__c(Street__c = 'Test street',City__c = 'Test City',Postal_Code__c='1m1 1k1');
     //   Insert siteAdd;
        
       // List<Address__c> siteaddd = [select Id,Account__c from Address__c WHERE Account__c =: serAcc.Id]; 
        
        asset ast= new asset(name='test', accountID=RCIDacc.id, vlocity_cmt__OrderId__c=ord2.id, status='Shipped');
        insert ast; 
        System.debug('ord.Id'+ord.Id);
        System.debug('address'+address.Id);
        //system.debug('siteaddd[0].Id-----'+ siteaddd[0].Id);
        ServiceAddressOrderEntry__c SelectedServiceAddress = new ServiceAddressOrderEntry__c (Order__c=ord.Id,Address__c = address.Id);
        insert  SelectedServiceAddress; 
        
                        
        List<ApexPages.Message> messages = ApexPages.getMessages();
        
        System.assertEquals(0, messages.size());
        
        Order theOrder = [SELECT Id, Service_Address__c, Service_Address__r.Id, Service_Address__r.Service_Account_Id__c, Service_Address__r.Service_Account_Id__r.Id, Service_Address__r.Service_Account_Id__r.ParentId, CustomerAuthorizedBy.Id FROM Order WHERE Id=: ord.Id];
        PageReference myPageRef = Page.naas_hybridcpq;
        Test.setcurrentPage(myPageRef);
        ApexPages.currentPage().getParameters().put('src','os');
        ApexPages.currentPage().getParameters().put('id', ord.Id);
        ApexPages.currentPage().getParameters().put('smbCareAddrId',address.Id);
        //vlocity_cmt.CardCanvasController controller = new vlocity_cmt.CardCanvasController();
        Test.startTest();
        Object controller;
        NAAS_HybridCPQExt hybridCpq = new NAAS_HybridCPQExt(controller);
        hybridCpq.SelectedServiceAddress = 'TestAddress';
        NAAS_HybridCPQExt.setCheckoutReadiness(theOrder.Service_Address__c,theOrder.Id);
        NAAS_HybridCPQExt.setCheckoutReadinessFalse(theOrder.Service_Address__c,theOrder.Id);
        hybridCpq.init();
        NAAS_HybridCPQExt.saveStatus(ord.Id);
        NAAS_HybridCPQExt.UpdateOrderHeader(SelectedServiceAddress.Id,ord.Id);
        NAAS_HybridCPQExt.AddOrderlineItems('Testing');
         
        Test.stopTest();
        
    }
}