@isTest
private class TelusPRMForgotPasswordControllerTest {
	public static testMethod void testForgotPasswordController() {
        // Instantiate a new controller with all parameters in the page
        TelusPRMForgotPasswordController controller = new TelusPRMForgotPasswordController();
        controller.username = 'test@salesforce.com';        
    
        System.assertEquals(controller.forgotPassword(),null);
        System.assertEquals(controller.forgotPasswordFr(),null);  
        controller.username = '';        
    
        System.assertEquals(controller.forgotPassword(),null);
        System.assertEquals(controller.forgotPasswordFr(),null);  
    }
}