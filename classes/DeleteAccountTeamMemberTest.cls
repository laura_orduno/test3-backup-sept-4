@isTest
private class DeleteAccountTeamMemberTest {
// TESTS
  
  private static testMethod void testInit() {
    Id PROFILEID = [SELECT id FROM Profile WHERE name='System Administrator' LIMIT 1].id;
    
    User runner = getRunnerUser();
    
    UserRole role = new UserRole(name = 'Test Manager');
    
    system.runas(runner) {
        insert role;
    }
    
    User mgr = new User(
      username = Datetime.now().getTime() + '@TRACTIONSM.COM',
      email = 'test@example.com',
      title = 'test',
      lastname = 'test',
      alias = 'test',
      TimezoneSIDKey = 'America/Los_Angeles',
      LocaleSIDKey = 'en_US',
      EmailEncodingKey = 'UTF-8',
      ProfileId = runner.ProfileId,
      LanguageLocaleKey = 'en_US',
      UserRoleId = role.id
    );

    system.runas(runner) {
      insert mgr;
    }
    
    User mgr2 = mgr.clone(false, false, false, false);
    mgr2.username = Datetime.now().getTime() + 'std@TRACTIONSM.COM';
    mgr2.Manager_1__c = mgr.id;
    
    system.runas(runner) {
      insert mgr2;
    }
    
    Account a = new Account(name = 'test', ownerid = mgr2.id);
    
    system.runas(runner) {
        insert a;
        
        AccountTeamMember atm = new AccountTeamMember(UserId = mgr.id, AccountId = a.id);
        AccountTeamMember atm2 = new AccountTeamMember(UserId = mgr2.id, AccountId = a.id);
        insert new AccountTeamMember[] { atm, atm2 };
    }

        system.runas(mgr2) {
        ApexPages.currentPage().getParameters().put('actId', a.id);
        
        DeleteAccountTeamMember del = new DeleteAccountTeamMember();
        
        del.init();
        
        //del.accountTeamWrapperList[0].checked = true;
        
        del.deleteAccountTeamMember();
    }
  }
  
  private static testMethod void testCancel() {
    DeleteAccountTeamMember del = new DeleteAccountTeamMember();
    
    PageReference ref = del.cancel();
    
    system.assertEquals(null, ref);
    
    Account a = new Account(name = 'test');
    insert a;
    
    ApexPages.currentPage().getParameters().put('actId', a.id);
    
    del = new DeleteAccountTeamMember();
    
    ref = del.cancel();
    
    system.assertEquals('/001/o', ref.getUrl());
  }
  
  @isTest
  private static User getRunnerUser() {
    Id PROFILEID = [SELECT id FROM Profile WHERE name='System Administrator' LIMIT 1].id;
    
    User runner = new User(
      username = Datetime.now().getTime() + 'runner@TRACTIONSM.COM',
      email = 'test@example.com',
      title = 'test',
      lastname = 'test',
      alias = 'test',
      TimezoneSIDKey = 'America/Los_Angeles',
      LocaleSIDKey = 'en_US',
      EmailEncodingKey = 'UTF-8',
      ProfileId = PROFILEID,
      LanguageLocaleKey = 'en_US'
    );
    
    return runner;
  }
}