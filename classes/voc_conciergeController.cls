global without sharing class voc_conciergeController {

    public VOC_Survey__c voc {get;set;}
    public string redirect_url {get;set;}
    public String step1_status { get; private set; }
    public String step2_status { get; private set; }
    public Id vocId {get;set;}
    public String lang {get;set;}
    public String mgr {get;set;}

    //update "Survey_Link_Clicked_Date__c" first
    public void start() {
        
        try{
            vocId = ApexPages.currentPage().getParameters().get('vocid');
            system.debug('ApexPages.currentPage().getParameters().get("vocid"):'+ApexPages.currentPage().getParameters().get('vocid'));
            system.debug('vocid param:'+vocId);
            lang = ApexPages.currentPage().getParameters().get('lang');
            mgr = ApexPages.currentPage().getParameters().get('mgr');
            system.debug('vocid:'+vocId);
            voc = [select Received_Date__c, Survey_Link_Clicked_Date__c, Fielded__c, Sent_Date__c, Survey_URL__c, Interaction_Date__c, Requires_Fielded__c, Received__c, Line_of_Service__c, Survey_Type__c FROM VOC_Survey__c WHERE id = :vocId];
            Date today = Date.today();
            Integer diff;
            if(voc.Sent_Date__c != null) diff = voc.Sent_Date__c.date().daysBetween(today);
            else diff=1;
            system.debug('Days DIfference:'+diff);
            String surveyURL = '';
            if(mgr != null && mgr.equalsIgnoreCase('1')){
                if(lang != null && lang.equalsIgnoreCase('fr')){
                    redirect_url = 'http://www.clicktools.com/survey?iv=2s7zt43vpn705&language=fr_CA&q1='+voc.Id;
                    //return new PageReference('http://www.clicktools.com/survey?iv=2s7zt43vpn705&language=fr_CA&q6='+voc.Id);
                }
                else {
                    redirect_url = 'http://www.clicktools.com/survey?iv=2s7zt43vpn705&q1='+voc.Id;
                    //return new PageReference('http://www.clicktools.com/survey?iv=2s7zt43vpn705&q6='+voc.Id);
                }
            }
            else{
                if(lang != null && lang.equalsIgnoreCase('fr')){
                    surveyURL = voc.Survey_URL__c+'&language=fr_CA';
                }
                else {
                    surveyURL = voc.Survey_URL__c;
                }
                if(diff<15){ //Adjust Survey Frequency(EA) - 10/13/2016
                    if(voc.Requires_Fielded__c == true){
                        system.debug('redirect:'+surveyURL+'&q1='+voc.Id+'&q14='+voc.Fielded__c);
                        redirect_url = surveyURL+'&q1='+voc.Id+'&q14='+voc.Fielded__c;
                        //return new PageReference(surveyURL+'&q6='+voc.Id+'&q7='+voc.Fielded__c);
                    }
                    else {
                        system.debug('redirect:'+surveyURL+'&q1='+voc.Id);
                        redirect_url = surveyURL+'&q1='+voc.Id;                        
                        //return new PageReference(surveyURL+'&q6='+voc.Id);
                    }
                    if(voc.Survey_Link_Clicked_Date__c == null) {
                        voc.Survey_Link_Clicked_Date__c = datetime.now();
                        update voc;
                    }
                }
                else {
                    //return new PageReference('/voc/voc_expired');
                }
            }
        } catch(Exception ex){
            //return new PageReference('/voc/voc_error');
        }
        //return null;
    }
    
    public PageReference complete() {
        
        try{
            system.debug('vocid param:'+vocId);
            voc = [select Received_Date__c, Survey_Link_Clicked_Date__c, Fielded__c, Sent_Date__c, Id, Survey_URL__c, Requires_Fielded__c, Received__c, Line_of_Service__c,Survey_Type__c FROM VOC_Survey__c WHERE id = :vocId];
            Date today = Date.today();
            Integer diff;
            if(voc.Sent_Date__c != null) diff = voc.Sent_Date__c.date().daysBetween(today);
            else diff=1;
            system.debug('Days DIfference:'+diff);
            String surveyURL = '';
            if(mgr != null && mgr.equalsIgnoreCase('1')){
                if(voc.Received_Date__c != null) return new PageReference('/voc/voc_dupe');
                if(lang != null && lang.equalsIgnoreCase('fr')){
                    redirect_url = 'http://www.clicktools.com/survey?iv=2s7zt43vpn705&language=fr_CA&q1='+voc.Id;
                    return new PageReference('http://www.clicktools.com/survey?iv=2s7zt43vpn705&language=fr_CA&q1='+voc.Id);
                }
                else {
                    redirect_url = 'http://www.clicktools.com/survey?iv=2s7zt43vpn705&q1='+voc.Id;
                    return new PageReference('http://www.clicktools.com/survey?iv=2s7zt43vpn705&q1='+voc.Id);
                }
            }
            else{
                if(lang != null && lang.equalsIgnoreCase('fr')){
                    surveyURL = voc.Survey_URL__c+'&language=fr_CA';
                }
                else {
                    surveyURL = voc.Survey_URL__c;
                }
                if(diff<15){ //Adjust Valid Survey Frequency(EA) - 10/13/2016
                    if(voc.Received_Date__c != null) return new PageReference('/voc/voc_dupe');
                    
                    //Line of Service change for VOC 26 (EA) - 04/08/2016
                    if(voc.Requires_Fielded__c == true){
                        // Q8 scenario
                        if(voc.Survey_Type__c==26 && (voc.Line_of_Service__c =='D2D' ||voc.Line_of_Service__c =='Inbound' ||voc.Line_of_Service__c =='Outbound')){
                            system.debug('redirect:'+surveyURL+'&q1='+voc.Id+'&q8='+voc.Line_of_Service__c);
                            redirect_url = surveyURL+'&q1='+voc.Id+'&q8='+voc.Line_of_Service__c;
                            return new PageReference(surveyURL+'&q1='+voc.Id+'&q8='+voc.Line_of_Service__c);
                        }
                        // default Q14 scenario
                        else{
                            system.debug('redirect:'+surveyURL+'&q1='+voc.Id+'&q14='+voc.Fielded__c);
                            redirect_url = surveyURL+'&q1='+voc.Id+'&q14='+voc.Fielded__c;
                            return new PageReference(surveyURL+'&q1='+voc.Id+'&q14='+voc.Fielded__c);
                        }  
                    }    
                    else {
                        system.debug('redirect:'+surveyURL+'&q1='+voc.Id);
                        redirect_url = surveyURL+'&q1='+voc.Id;         
                        return new PageReference(surveyURL+'&q1='+voc.Id);
                    }
                        
                    if(voc.Survey_Link_Clicked_Date__c == null) {
                        voc.Survey_Link_Clicked_Date__c = datetime.now();
                        update voc;
                    }
                }
                else {
                    return new PageReference('/voc/voc_expired');
                }
            }
            
        } catch(Exception ex){
            Util.emailSystemError(ex);
            return new PageReference('/voc/voc_error');
        }
        return null;
    } 
}