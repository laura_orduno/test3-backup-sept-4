@isTest
private class QuoteSearchControllerTests {
    private static Web_Account__c account;
    private static Address__c location;
    private static SBQQ__Quote__c quote;
    
    testMethod static void testPortalURL() {
        setUp();
        QuoteSearchController target = new QuoteSearchController();
        System.assertEquals(Site.getCurrentSiteUrl(), target.portalURL);
    }
    
    testMethod static void testOnSearch() {
        setUp();
        
        QuoteSearchController target = new QuoteSearchController();
        target.searchTerm = 'test';
        target.onSearch();
    }
    
    testMethod static void testOnSearchShortTerm() {
        setUp();
        
        QuoteSearchController target = new QuoteSearchController();
        target.searchTerm = 't';
        PageReference p = target.onSearch();
        system.assertEquals(null, p);
    }
    
    testMethod static void testSortAscDesc() {
        setUp();
        
        QuoteSearchController target = new QuoteSearchController();
        target.searchTerm = 'testing';
        target.onSearch();
        target.quoteSortField = 'SBQQ__Quote__c.Account_Name__c';
        target.onSortQuotes();
        target.quoteSortField = 'SBQQ__Quote__c.Name';
        target.onSortQuotes();
        system.assertEquals(true, target.quoteSortAsc);
        target.quoteSortField = 'SBQQ__Quote__c.Name';
        target.onSortQuotes();
        system.assertEquals(false, target.quoteSortAsc);
    }    
    
    testMethod static void testLoadRecentSort() {
        setUp();
        
        QuoteSearchController target = new QuoteSearchController();
        target.onSearch();
        target.quoteSortField = 'SBQQ__Quote__c.Account_Name__c';
        target.onSortQuotes();
        target.quoteSortField = 'SBQQ__Quote__c.Name';
        target.onSortQuotes();
        system.assertEquals(true, target.quoteSortAsc);
        target.onSearch();
    }   
    
    testMethod static void testAccountSort() {
        setUp();
        
        QuoteSearchController target = new QuoteSearchController();
        target.onSearch();
        target.accountSortField = 'Web_Account__c.RCID__c';
        target.onSortAccounts();
        target.accountSortField = 'Web_Account__c.Billing_City__c';
        target.onSortAccounts();
        system.assertEquals(true, target.accountSortAsc);
        target.accountSortField = 'Web_Account__c.Billing_City__c';
        target.onSortAccounts();
        system.assertEquals(false, target.accountSortAsc);
    }   
    
    private static void setUp() {
        account = new Web_Account__c(Name='UnitTest');
        insert account;
        
        Opportunity opp = new Opportunity(Name='Test',Web_Account__c=account.Id,CloseDate=System.today(),StageName='Prospecting');
        insert opp;
        
        quote = new SBQQ__Quote__c(SBQQ__Opportunity__c=opp.Id);
        quote.Completed__c = true;
        quote.Rate_Band__c = 'F';
        quote.Province__c = 'BC';
        insert quote;
    }
}