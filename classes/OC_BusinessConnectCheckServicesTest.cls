@isTest
private class OC_BusinessConnectCheckServicesTest {
	
	@isTest
    private static void OC_BusinessConnectCheckServicesInitTest() {
        OC_BusinessConnectCheckServices ocBC =  new  OC_BusinessConnectCheckServices();
        ocBC.portabilityCheckFailure =  false;
        ocBC.portabilityCheckMessage =  '';
        ocBC.portabilityCheckReturned =  false;
        ocBC.availabilityCheckMessage =  '';
        Integer num = ocBC.availableNumbers;
        List<SelectOption> options = ocBC.options;
        List<String> bcRateCenters =  ocBC.bcRateCenters;
        String bcRateCenter =  ocBC.rateCenter;
        List<SelectOption> regions = ocBC.getRegions();
        List<SelectOption> sortOptions = ocBC.getSortingOptions();
        ocBC.initBusinessConnectPortability();
        ocBC.initAvailabilityCheck();
        ocBC.region = 'BC';
        ocBC.updateRateCenters();

	}
}