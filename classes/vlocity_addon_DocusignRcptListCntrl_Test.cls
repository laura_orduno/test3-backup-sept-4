@IsTest
public class vlocity_addon_DocusignRcptListCntrl_Test {
	static testmethod void docuSignRecptListUnitTest(){
        TestDataHelper.testContractLineItemListCreation_ConE();
        Test.startTest();
		Map<String,Object> outMap = new Map<String,Object>();
		Apexpages.currentPage().getparameters().put('id',TestDataHelper.testContractObj.id);
		vlocity_addon_DocusignRcptListCntrl objvlocity_addon_DocusignRcptListCntrl=new vlocity_addon_DocusignRcptListCntrl(new vlocity_cmt.DocuSignEnvelopeController());
		system.assertEquals(objvlocity_addon_DocusignRcptListCntrl.sCustomerInfo !=null, true);
		
     //   vlocity_addon_DocusignRcptListCntrl c1 = new vlocity_addon_DocusignRcptListCntrl(new vlocity_cmt.DocuSignEnvelopeController());
     //   c1.getSignerTypes();
        vlocity_addon_DocusignRcptListCntrl.getSignerTypes();
        vlocity_addon_DocusignRcptListCntrl.saveSignerType();
        Test.stopTest();
    }
    
	static testmethod void docuSignRecptListUpdateUnitTest(){
        TestDataHelper.testContractLineItemListCreation_ConE();
        Test.startTest();
		Map<String,Object> outMap=new Map<String,Object>();
		Apexpages.currentPage().getparameters().put('id',TestDataHelper.testContractObj.id);
		vlocity_addon_DocusignRcptListCntrl objvlocity_addon_DocusignRcptListCntrl=new vlocity_addon_DocusignRcptListCntrl(new vlocity_cmt.DocuSignEnvelopeController());
		system.assertEquals(objvlocity_addon_DocusignRcptListCntrl.sCustomerInfo !=null, true);
     //   vlocity_addon_DocusignRcptListCntrl c1 = new vlocity_addon_DocusignRcptListCntrl(new vlocity_cmt.DocuSignEnvelopeController());
     //   c1.getSignerTypes();
        vlocity_addon_DocusignRcptListCntrl.getSignerTypes();
        vlocity_addon_DocusignRcptListCntrl.saveSignerType();
        String s1 = TestDataHelper.testContractObj.id ;
        vlocity_cmt__ContractVersion__c contractVersionObj = new vlocity_cmt__ContractVersion__c();
        contractVersionObj.vlocity_cmt__ContractId__c = TestDataHelper.testContractObj.id ;
        insert contractVersionObj;
        vlocity_cmt__ContractEnvelope__c contractEnvelopeObj = new vlocity_cmt__ContractEnvelope__c();
        contractEnvelopeObj.vlocity_cmt__ContractVersionId__c = contractVersionObj.Id;
        contractEnvelopeObj.vlocity_cmt__ContractId__c = TestDataHelper.testContractObj.id ;
        insert contractEnvelopeObj;
        vlocity_cmt__ContractRecipient__c contractRecipientObj = new vlocity_cmt__ContractRecipient__c();
        contractRecipientObj.vlocity_cmt__ContractId__c = TestDataHelper.testContractObj.id ;
        contractRecipientObj.vlocity_cmt__EnvelopeId__c = contractEnvelopeObj.Id;
        contractRecipientObj.Signer_Type__c = 'Customer Signor';
        contractRecipientObj.vlocity_cmt__RoutingOrder__c = 234;
        insert contractRecipientObj;
        vlocity_addon_DocusignRcptListCntrl.recipientContract recipientContractWrap = new vlocity_addon_DocusignRcptListCntrl.recipientContract();
        recipientContractWrap.routingOrder = 2;
        recipientContractWrap.signerType = 'Customer Signor';
        List<vlocity_addon_DocusignRcptListCntrl.recipientContract> contractWrapList = new List<vlocity_addon_DocusignRcptListCntrl.recipientContract>();
        contractWrapList.add(recipientContractWrap);
        String s2 = JSON.serialize(contractWrapList);
        vlocity_addon_DocusignRcptListCntrl.updateRecipient(s1,s2);
        Test.stopTest();
    }
}