/**
 * Data Access Object (DAO) helper class for loading pricebook entries.
 * 
 * @author Max Rudman
 * @since 4/12/2011
 */
public class PricebookEntryDAO extends AbstractDAO { 
	public List<PricebookEntry> loadByProductIds(Set<Id> productIds) {
		return loadByProductIds(productIds, null, null);
	}
	
	public List<PricebookEntry> loadByProductIds(Set<Id> productIds, Id pricebookId) {
		return loadByProductIds(productIds, pricebookId, null);
	}
	
	public List<PricebookEntry> loadByProductIds(Set<Id> productIds, Id pricebookId, String currencyCode) {
		QueryBuilder builder = new QueryBuilder(PricebookEntry.sObjectType);
		builder.getSelectClause().addField(PricebookEntry.Pricebook2Id);
		builder.getSelectClause().addField(PricebookEntry.UnitPrice);
		builder.getSelectClause().addField(PricebookEntry.Product2Id); 
		if (UserInfo.isMultiCurrencyOrganization()) {
			builder.getSelectClause().addField('CurrencyIsoCode');
		}
		builder.getWhereClause().addExpression(builder.eq(PricebookEntry.IsActive, true));
		builder.getWhereClause().addExpression(builder.inExpr(PricebookEntry.Product2Id, productIds));
		if (pricebookId != null) {
			builder.getWhereClause().addExpression(builder.eq(PricebookEntry.Pricebook2Id, pricebookId));
		}
		if (currencyCode != null) {
			builder.getWhereClause().addExpression(builder.eq('CurrencyIsoCode', currencyCode));
		}
		return Database.query(builder.buildSOQL());
	}
}