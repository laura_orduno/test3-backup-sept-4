/* 2017-07-12[Roderick de Vera]
 * Helper class which invokes the Web Service Callout for Query Resource
 */
public class OrdrQueryResourceWsCallout {
    
    public static OrdrTnResQryResConfMsg.ResourceConfigurationMessage queryResource(Map<String, Object> inputMap) {
        OrdrTnResQryResConfMsg.ResourceConfigurationMessage resourceConfig;
    	
        TpCommonMessage.LookupRequestMessage lookupRequest = preparePayload(inputMap);
        try { 
            OrdrTnResQryInvOperation.QueryResourceSOAPBinding servicePort = new OrdrTnResQryInvOperation.QueryResourceSOAPBinding(); 
            servicePort = prepareCallout(servicePort);          
            resourceConfig = servicePort.queryResource(lookupRequest.header, lookupRequest.sourceCriteria, lookupRequest.targetCriteria);   
        } 
        catch (CalloutException e) {

            OrdrUtilities.auditLogs('OrdrQueryResourceWsCallout.queryResource', 
                                    'NetCracker', 
                                    'Query Resource (FIFA Drop Info)', 
                                    null, 
                                    null, 
                                    true,
                                    e.getMessage(), 
                                    e.getStackTraceString());
            
            throw new OrdrExceptions.QueryResourceException(e.getMessage(), e);
        }
        return resourceConfig;
    }
    
    private static OrdrTnResQryInvOperation.QueryResourceSOAPBinding 
        prepareCallout(OrdrTnResQryInvOperation.QueryResourceSOAPBinding servicePort) {
            Ordering_WS__c queryResourceEndpoint = Ordering_WS__c.getValues('QueryResourceWirelineEndpoint');           
            servicePort.endpoint_x = queryResourceEndpoint.value__c;
            // Set SFDC Webservice call timeout
            servicePort.timeout_x = OrdrConstants.WS_TIMEOUT_MS;    
            
            if (servicePort.endpoint_x.startsWith('https://xmlgwy')) {
                Ordering_WS__c wsUserName =  Ordering_WS__c.getValues(OrdrConstants.CREDENTIAL_USERNAME);
                Ordering_WS__c wsPassword =  Ordering_WS__c.getValues(OrdrConstants.CREDENTIAL_PASSWORD); 
                String credentials = wsUserName.value__c + ':' + wsPAssword.value__c;
                String encodedUserNameAndPassword = EncodingUtil.base64Encode(Blob.valueOf(credentials));   
                
                servicePort.inputHttpHeaders_x = new Map<String, String>();
                servicePort.inputHttpHeaders_x.put('Authorization', 'Basic ' + encodedUserNameAndPassword);
            } else {
                servicePort.clientCertName_x = OrdrConstants.SDF_CERT_NAME;
            }
            
            return servicePort;
    } 
    
    private static TpCommonMessage.LookupRequestMessage preparePayload(Map<String, Object> inputMap) {
        
        TpCommonMessage.LookupRequestMessage lookupRequest = new TpCommonMessage.LookupRequestMessage();
        TpCommonBaseV3.EntityWithSpecification sourceCriteria = new TpCommonBaseV3.EntityWithSpecification();
        TpCommonBaseV3.EntityWithSpecification targetCriteria = new TpCommonBaseV3.EntityWithSpecification();
        
        Map<String, String> mandatoryFields = new Map<String, String>();
        mandatoryFields.put(OrdrConstants.LOCATION_ID, OrdrConstants.LOCATION_ID);
        
        /*create sourceCriteria/Specification section
        */
        TpCommonBaseV3.EntitySpecification specification = new TpCommonBaseV3.EntitySpecification();
        specification.Name = 'GIS';
        specification.Type_x = 'Connectivity Path';
        specification.Category = 'Physical Resource';
        sourceCriteria.Specification = specification;
        
        /*create sourceCriteria/CharacteristicValue/Characteristic/Name section
        */
        List<TpCommonBaseV3.CharacteristicValue> characteristicValues = new List<TpCommonBaseV3.CharacteristicValue>();
        if (inputMap.containsKey(OrdrConstants.LOCATION_ID) && inputMap.get(OrdrConstants.LOCATION_ID) != null) {
            characteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.LOCATION_ID, (String)inputMap.get(OrdrConstants.LOCATION_ID)));
            mandatoryFields.remove(OrdrConstants.LOCATION_ID);
        } 
        characteristicValues.add(OrdrUtilities.constructCharacteristicValue('transportType', 'GPON'));
        
        if (mandatoryFields.size() > 0) {
            String requiredFields = JSON.serialize(mandatoryFields.values());
            System.debug('requiredFields:' + requiredFields);
            String msg = Label.DFLT0004;
            msg = msg.replace('{0}', 'Query Resource');
            msg = msg.replace('{1}', requiredFields);
            throw new OrdrExceptions.InvalidParameterException(msg);            
        }
        
        sourceCriteria.CharacteristicValue = characteristicValues;
        lookupRequest.SourceCriteria = new List<TpCommonBaseV3.EntityWithSpecification>();
        lookupRequest.SourceCriteria.add(sourceCriteria);
        
		characteristicValues = new List<TpCommonBaseV3.CharacteristicValue>();
        characteristicValues.add(OrdrUtilities.constructCharacteristicValue('queryPathInformation', 'false'));        
        targetCriteria.CharacteristicValue = characteristicValues;
        lookupRequest.targetCriteria = new List<TpCommonBaseV3.EntityWithSpecification>();
        lookupRequest.targetCriteria.add(targetCriteria);
        
        return lookupRequest;
    }

    
}