public with sharing class trac_TaskController {
    public List<Task> tasks {get;set;}
    public Map<ID, List<String>> taskAccounts {get;set;}
    public Map<ID, List<String>> taskWhos {get;set;}
    public Map<ID, List<String>> taskWhats {get;set;}
    public ID taskId {get;set;}
    public String tasklistmode {get;set;}
    public List<List<String>> newTaskList {get;set;}
    
    public String getFilterByVal(String val){
    	
        // Tasks overdue
        if(val == '0') return 'AND ActivityDate < ' + ((DateTime)System.now()).format('yyyy-MM-dd');

    	// Tasks due today
    	if(val == '1') return 'AND ActivityDate = ' + ((DateTime)System.now()).format('yyyy-MM-dd');
    	
    	// Tasks due today + overdue
    	if(val == '2') return 'AND ActivityDate <= ' + ((DateTime)System.now()).format('yyyy-MM-dd');
    	
    	// Tasks due tomorrow
    	if(val == '3') return 'AND ActivityDate = ' + ((DateTime)System.now()).addDays(1).format('yyyy-MM-dd');	

    	//Tasks due within the next 7 days
       	if(val == '4') return 'AND ActivityDate >= ' + ((DateTime)System.now()).format('yyyy-MM-dd') + ' AND ActivityDate <= ' + ((DateTime)System.now()).addDays(7).format('yyyy-MM-dd');
    	
    	//Tasks due within the next 7 days + overdue
    	if(val == '5') return 'AND ActivityDate <= ' + ((DateTime)System.now()).addDays(7).format('yyyy-MM-dd');

    	//Tasks due this month
    	if(val == '6') return 'AND ActivityDate >= ' + ((DateTime)System.now()).format('yyyy-MM-01') + ' AND ActivityDate <= ' + DateTime.newInstance(System.now().year(), System.now().month(), 1).addMonths(1).addDays(-1).format('yyyy-MM-dd');
		   	
    	// Don't filter tasks by date
    	return '';
    }
	
	/**
	Deprecated - Too many SOQL
	
    public SObject relatedAccount(ID whatID)
    {

        Schema.SObjectType myObj;
        String target = whatID;
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        string keyPrefix;
        for(Schema.SObjectType describe: gd.values() ){
                keyPrefix = describe.getDescribe().getKeyPrefix();
                if(keyPrefix != null && target.startsWith(keyPrefix)){
                        myObj = describe;
                        break; //no need to keep looking
                }
        }

        String ot = String.valueOf(myObj);
        
        List<Schema.DescribeSObjectResult> t = Schema.describeSObjects(new String[] {ot});

        DescribeSObjectResult mr = t[0];
        if(ot == 'Account'){
            try{
                SObject a = Database.query('SELECT ID, NAME FROM Account WHERE ID=\'' + whatId + '\'');
                return a;
            }catch(Exception e){
                return null;
            }
        }
        if(mr.fields.getMap().containsKey('AccountId')){
            try{
                SObject a = Database.query('SELECT ID, Name FROM Account WHERE ID IN (SELECT accountId FROM ' + ot + ' WHERE id=\'' + whatId + '\')');
                return a;
            }catch(Exception e){
                return null;
            }
        }else if(mr.fields.getMap().containsKey('Account__c')){
            try{
                SObject a = Database.query('SELECT ID, Name FROM Account WHERE ID IN(SELECT account__c FROM ' + ot + ' WHERE id=\'' + whatID + '\')');
                return a;
            }catch(Exception e)
            {
                return null;
            }
        }
        return null;
    }*/
    /**
    Sept 8, 2014
    Author: Andy Leung
    TF Case: 256408
    
    1.  Query all accounts using whatIds
    2.  Remove all account ids from whatIds
    3.  Use prefix from whatIds to find each of the object type
    4.  For each object type, execute SOQL to find their Account Name
    
    This should sustain as there should not be more than unique whatIds from returning Task in fetchTasks.
   */
	map<id,map<id,string>> getAccountNames(set<id> whatIds){
		map<id,map<id,string>> whatIdAccountNameMap=new map<id,map<id,string>>();
		set<id> accountIds=new set<id>();
		list<account> acts=database.query('select id,name from account where id=:whatIds');
		for(account act:acts){
			whatIdAccountNameMap.put(act.id,new map<id,string>{act.id=>act.name});
			accountIds.add(act.id);
		}
		whatIds.removeall(accountIds);
		system.debug('Total What ID After Removing Account:'+whatIds.size());
		list<string> prefixes=new list<string>();
		map<string,schema.sobjecttype> objectMap=schema.getglobaldescribe();
		map<id,schema.describesobjectresult> whatIdObjectMap=new map<id,schema.describesobjectresult>();
		for(schema.sobjecttype obj:objectMap.values()){
			schema.describesobjectresult result=obj.getdescribe();
			for(id whatId:whatIds){
				if(string.isnotblank(whatId)&&string.isnotblank(result.getkeyprefix())&&((string)whatId).startswith(result.getkeyprefix())){
					if(result.fields.getmap().containskey('AccountId')||result.fields.getmap().containskey('Account__c')||result.fields.getmap().containskey('account__c')){
						whatIdObjectMap.put(whatId,result);
					}
				}
			}
		}
		// Before executing SOQL, group same object type together to minimize SOQL
		map<boolean,map<string,set<id>>> customNameWhatIdMap=new map<boolean,map<string,set<id>>>();
		for(id whatId:whatIds){
			schema.describesobjectresult result=whatIdObjectMap.get(whatId);
			if(result!=null){
				string name=result.getname();
				boolean isCustom=result.iscustom();
				map<string,set<id>> nameWhatIdMap=customNameWhatIdMap.get(isCustom);
				if(nameWhatIdMap==null){
					nameWhatIdMap=new map<string,set<id>>();
				}
				set<id> whatIdSet=nameWhatIdMap.get(name);
				if(whatIdSet==null){
					whatIdSet=new set<id>();
				}
				whatIdSet.add(whatId);
				nameWhatIdMap.put(name,whatIdSet);
				customNameWhatIdMap.put(isCustom,nameWhatIdMap);
			}
		}
		// Execute SOQL based on name
		string accountName='';
		for(boolean isCustom:customNameWhatIdMap.keyset()){
			map<string,set<id>> nameWhatIdCustomMap=customNameWhatIdMap.get(isCustom);
			for(string objectName:nameWhatIdCustomMap.keyset()){
				set<id> whatIdSet=nameWhatIdCustomMap.get(objectName);
				system.debug('query:'+'select id,'+(isCustom?'account__c,account__r.name':'accountid,account.name')+' from '+objectName+' where id=:whatIdSet');
				try{
					// Check if object has relationship with account
					
					list<sobject> queryResult=database.query('select id,'+(isCustom?'account__c,account__r.name':'accountid,account.name')+' from '+objectName+' where id=:whatIdSet');
					system.debug('object:'+objectName+',size:'+queryResult.size());
					for(sobject sobj:queryResult){
						if((isCustom&&sobj.get('account__c')!=null)||(!isCustom&&sobj.get('accountid')!=null)){
							accountName=(string)(isCustom?(sobj.getsobject('account__c').get('name')==null?'':sobj.getsobject('account__c').get('name')):(sobj.getsobject('account').get('name')==null?'':sobj.getsobject('account').get('name')));
							whatIdAccountNameMap.put(sobj.id,new map<id,string>{(id)(isCustom?sobj.get('account__c'):sobj.get('accountid'))=>accountName});
						}
					}
				}catch(queryexception qe){
					// No account relationship found, ignore
					system.debug('Query Exception:'+qe.getmessage());
				}catch(exception e){
					system.debug('**Exception:'+e.getmessage());
					system.debug('**Stack Trace:'+e.getstacktracestring());
				}
			}
		}
		
		return whatIdAccountNameMap;
    }
    /**
    Sept 10, 2014
    Author: Andy Leung
    Updated code to use new method to get account id and name.
    */
    public void fetchTasks() {
        taskAccounts = new Map<ID, List<String>>();
        taskWhos = new Map<ID, List<String>>();
        taskWhats = new Map<ID, List<String>>();
		String qs = 'SELECT Status, ActivityDate, Subject, WhoId,Who.Name, WhatId, What.Name, AccountId, Account.Name, Account.ID, Priority FROM Task WHERE Status != \'Completed\' ' + getFilterByVal(tasklistmode) + ' AND OwnerId = \'' + UserInfo.getUserId() + '\' limit 50';
        tasks = Database.query(qs);
        set<id> whatIdsSet=new set<id>();
        for(Task t : tasks) {
        	if(t.whatid!=null){
        		whatIdsSet.add(t.whatid);
        	}
        }
		system.debug('Total What ID:'+whatIdsSet.size());
        map<id,map<id,string>> whatIdAccountNameMap=getAccountNames(whatIdsSet);
        for(Task t : tasks) {

            taskAccounts.put(t.Id, new List<String>{'', ''});
            taskWhos.put(t.Id, new List<String>{'', ''});
            taskWhats.put(t.Id, new List<String>{'', ''});

            if(t.WhoId != null) taskWhos.put(t.id, new List<String>{String.valueOf(t.WhoId), t.Who.Name});
            
            
            if(t.WhatId != null) taskWhats.put(t.id, new List<String>{String.valueOf(t.WhatId), t.What.Name});
            
            if(t.WhatId != null ) {
            	/**
                SObject tmpObj = relatedAccount(t.whatId);
                if(tmpObj != null) taskAccounts.put(t.Id,new List<String>{String.valueOf(tmpObj.get('Id')), String.valueOf(tmpObj.get('Name'))});
            	*/
                map<id,string> accountNameMap=whatIdAccountNameMap.get(t.whatId);
                if(accountNameMap!=null){
                	for(id accountId:accountNameMap.keyset()){
                		taskAccounts.put(t.id,new list<string>{accountId,accountNameMap.get(accountId)});
                	}
                }
            }
        }
    }

    public trac_TaskController() {
    	tasklistmode = '7';
        fetchTasks();
    }

    public PageReference changeFilter(){
    	tasklistmode  = ApexPages.currentPage().getParameters().get('trac_tasklistmode_num');
    	fetchTasks();
    	return null;
    }

    public PageReference closeTask(){
        if(taskId != null){
	        Task task = [SELECT status FROM Task WHERE Id=:taskId];
    	    task.status = 'Completed';
        	PageReference pr = new ApexPages.StandardController(task).edit();
         	pr.getParameters().put('close', '1'); 
         	return pr;
        }
        return null;
    }

 }