/*
    Quote Layout Controller Extension
    QuoteLayoutExtension.cls
    Part of the TELUS QuoteQuickly Portal
    
    Controller extension for the Quote Layout page
    
    -- Quote Bundle structure code can be found in QuoteBundleView.cls --
    
    Author: Alex Miller @ Traction <amiller@tractionondemand.com>
    Created: March 20, 2012
    
    Since: Release 5
    Initial Definition: R5rq4
    
    Known Issues:
     -- None Yet --
*/
public without sharing class QuoteLayoutExtension {
    /* Column Widths */
    public string column1Width {get{return '40%';}} // Service Name
    public string column2Width {get{return '39%';}} // Feature Name
    public string column3Width {get{return '7%';}} // Quantity
    public string column4Width {get{return '14%';}} // Price
    
    public string deviceColumn1Width {get{return '25%';}} // Device Name
    public string deviceColumn2Width {get{return '5%';}} // Quantity
    public string deviceColumn3Width {get{return '14%';}} // Action
    public string deviceColumn4Width {get{return '8%';}} // Service Term
    public string deviceColumn5Width {get{return '12%';}} // No Term Price
    public string deviceColumn6Width {get{return '12%';}} // Term Discount
    public string deviceColumn7Width {get{return '12%';}} // Final Price
    public string deviceColumn8Width {get{return '12%';}} // Airtime Credits
    /* End Column Widths */
    
    /* Fonts */
    public string standardFontSize {get{return '9pt';}}
    public string standardFontFamily {get{return 'sans-serif';}}
    public string tableHeaderFontWeight {get{return 'bold';}}
    public string sectionHeaderFontSize {get{return '11pt';}}
    public string sectionHeaderFontWeight {get{return 'normal';}}
    public string sectionFooterFontWeight {get{return 'bold';}}
    
    public string standardTextAlign {get{return 'left';}}
    public string standardQuantityTextAlign {get{return 'center';}}
    public string headerQuantityTextAlign {get{return 'center';}}
    public string standardCurrencyTextAlign {get{return 'right';}}
    public string headerCurrencyTextAlign {get{return 'right';}}
    /* End Fonts */
    
    /* Padding */
    public string standardPadding {get{return '4px';}}
    /* End Padding */
    
    /* Colours */
    public string standardColour {get{return '#000000';}}
    public string sectionHeaderBG {get{return '#49166d';}}
    public string sectionHeaderColour {get{return '#ffffff';}}
    public string sectionFooterBG {get{return '#b799cc';}}
    public string sectionFooterColour {get{return '#000000';}}
    public string tableHeaderBG {get{return '#b799cc';}}
    public string tableHeaderColour {get{return '#000000';}}
    public string primaryLineBG {get{return '#e8d9f1';}}
    public string secondaryLineBG {get{return '#ffffff';}}
    /* End Colours */
    
    public QuoteLayoutExtension(GenerateQuoteDocumentController controller) {}
    public QuoteLayoutExtension() {}
    
}