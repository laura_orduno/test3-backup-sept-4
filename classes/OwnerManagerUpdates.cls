public with sharing class OwnerManagerUpdates {

    public static void setManagers(List<Lead> leads, Set<Id> ids) {
        Map<id, User> ownerMap = new Map<Id,User>([
        	SELECT Manager_1__c, Manager_2__c, Manager_3__c, Manager_4__c
        	FROM User
        	WHERE id in :ids
        ]);
        
        for(Lead l : leads){
            if(((String)(l.ownerId)).startsWith('005')) {
                l.Manager_1__c = ( ownerMap.get(l.ownerId) == null ? null : ownerMap.get(l.ownerId).Manager_1__c );
                l.Manager_2__c = ( ownerMap.get(l.ownerId) == null ? null : ownerMap.get(l.ownerId).Manager_2__c );
                l.Manager_3__c = ( ownerMap.get(l.ownerId) == null ? null : ownerMap.get(l.ownerId).Manager_3__c );
                l.Manager_4__c = ( ownerMap.get(l.ownerId) == null ? null : ownerMap.get(l.ownerId).Manager_4__c );
            } else {
                l.Manager_1__c = null;
                l.Manager_2__c = null;
                l.Manager_3__c = null;
                l.Manager_4__c = null;
            }
        }
    }
    
    @future
    public static void updateLeads(Set<Id> ids) {
        Map<id, User> ownerMap = new Map<Id,User>([
        	SELECT Manager_1__c, Manager_2__c, Manager_3__c, Manager_4__c
        	FROM User
        	WHERE id in :ids
        ]);
        
        List<Lead> leads = [
        	SELECT id, manager_1__c, manager_2__c, manager_3__c, manager_4__c, ownerid
        	FROM Lead
        	WHERE ownerid in :ids
        	AND isConverted = false
        ];
        
        for(Lead l : leads) {
            l.Manager_1__c = ownerMap.get(l.ownerId).Manager_1__c;
            l.Manager_2__c = ownerMap.get(l.ownerId).Manager_2__c;
            l.Manager_3__c = ownerMap.get(l.ownerId).Manager_3__c;
            l.Manager_4__c = ownerMap.get(l.ownerId).Manager_4__c; 
        }
        update leads;
    }
    
}