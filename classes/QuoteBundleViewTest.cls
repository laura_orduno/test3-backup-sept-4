@isTest
private class QuoteBundleViewTest {

    private static testMethod void testQuoteBundleView() {
        SBQQ__QuoteLine__c ql = insertTestLine();
        
        List<SBQQ__QuoteLine__c> additionalLines = new List<SBQQ__QuoteLine__c>{
            new SBQQ__QuoteLine__c(SBQQ__Quote__c = ql.SBQQ__Quote__c, SBQQ__NetPrice__c = 10, SBQQ__Quantity__c = 1,
                SBQQ__Product__c = ql.SBQQ__Product__c, Category__c = 'Devices'),
            new SBQQ__QuoteLine__c(SBQQ__Quote__c = ql.SBQQ__Quote__c, SBQQ__NetPrice__c = 10, SBQQ__Quantity__c = 1,
                SBQQ__Product__c = ql.SBQQ__Product__c, Category__c = 'Services'),
            new SBQQ__QuoteLine__c(SBQQ__Quote__c = ql.SBQQ__Quote__c, SBQQ__NetPrice__c = 10, SBQQ__Quantity__c = 1,
                SBQQ__Product__c = ql.SBQQ__Product__c, Category__c = 'Features'),
            new SBQQ__QuoteLine__c(SBQQ__Quote__c = ql.SBQQ__Quote__c, SBQQ__NetPrice__c = 10, SBQQ__Quantity__c = 1,
                SBQQ__Product__c = ql.SBQQ__Product__c, Category__c = 'Credits')
        };
        insert additionalLines;
        
        Id nullId;
        QuoteBundleView ctlr = new QuoteBundleView(nullId);
        
        ctlr = new QuoteBundleView(ql.id);
        ctlr = new QuoteBundleView(ql.SBQQ__Quote__r);
        
        String name = ctlr.name;
        
        Boolean isPlus = ctlr.isplus;
        
        Decimal bundleOnlyTotal = ctlr.BundleOnlyMonthlyTotal;
        
        List<QuoteBundleView.CoreLine> coreLines = ctlr.getCoreLines();
        Integer coreLinesCount = ctlr.coreLinesCount;
        Decimal coreMonthlyTotal = ctlr.CoreMonthlyTotal;
        
        List<QuoteBundleView.AddlLine> addlLines = ctlr.getAddlLines();
        Integer addlLinesCount = ctlr.addlLinesCount;
        
        List<QuoteBundleView.DeviceLine> deviceLines = ctlr.getDeviceLines();
        Integer deviceLinesCount = ctlr.deviceLinesCount;
        Decimal deviceOneTimeTotal = ctlr.deviceOneTimeTotal;
        Decimal deviceAirtimeCreditTotal = ctlr.DeviceAirtimeCreditTotal;

        Map<Id, String> lineToList = ctlr.lineToList;
            Map<Id, List<Id>> lineToAdditionalInformations = ctlr.lineToAdditionalInformations;
    }
    
    private static testMethod void testStdLine() {
        SBQQ__QuoteLine__c ql = insertTestLine();
        
        Product2 p = new Product2(Name='Test');
        insert p;
        
        SBQQ__QuoteLine__c qlf = new SBQQ__QuoteLine__c(SBQQ__Quote__c = ql.SBQQ__Quote__c,SBQQ__Product__c=p.Id);
        insert qlf;
        
        List<AdditionalInformation__c> infoList = new List<AdditionalInformation__c> {
            new AdditionalInformation__c()
        };
        
        // std ctor
        QuoteBundleView.StdLine sl = new QuoteBundleView.StdLine();
        
        sl = new QuoteBundleView.StdLine(ql, infoList);
        
        String userString = sl.getUserString();
        
        system.assertEquals('', userString);
        
        String total = sl.getTotal();
        
        List<QuoteBundleView.FeatureLine> features = sl.getFeatures();
        
      QuoteBundleView.FeatureLine fl = new QuoteBundleView.FeatureLine(qlf);
      
      sl.addFeature(fl);
        
            sl.removeFeature(qlf.id);
            
            sl.addFeature(fl);
            
            sl.removeFeature(fl);
    }
    
    private static testMethod void testStdLineGetUserString() {
        SBQQ__QuoteLine__c ql = insertTestLine();
        
        List<AdditionalInformation__c> infoList = new List<AdditionalInformation__c> {
            new AdditionalInformation__c(
                first_name__c = 'first',
                last_name__c = 'last',
                Directory_Listing__c = 'Test User',
                    Mobile__c = '9999999999',
                    Billing_Telephone_Number_BTN__c = '2222222222',
                    Destination_Number__c = '3333333333',
                    Porting__c = '4444444444'
                )
        };
        
        QuoteBundleView.StdLine sl = new QuoteBundleView.StdLine(ql, infoList);
        
        String userString = sl.getUserString();
        system.assertEquals('<block>first last  9999999999</block>', userString);
        
        
        infoList[0].first_name__c = null;
        infoList[0].mobile__c = null;
        sl = new QuoteBundleView.StdLine(ql, infoList);
        
        userString = sl.getUserString();
        system.assertEquals('<block>last  2222222222</block>', userString);
        
        infoList[0].first_name__c = 'first';
        infoList[0].last_name__c = null;
        infoList[0].Billing_Telephone_Number_BTN__c = null;
        sl = new QuoteBundleView.StdLine(ql, infoList);
        
        userString = sl.getUserString();
        system.assertEquals('<block>first  3333333333</block>', userString);
        
        infoList[0].first_name__c = null;
        infoList[0].Destination_Number__c = null;
        sl = new QuoteBundleView.StdLine(ql, infoList);
        
        userString = sl.getUserString();
        system.assertEquals('<block>Test User  4444444444</block>', userString);
        
        infoList[0].Directory_Listing__c = null;
        sl = new QuoteBundleView.StdLine(ql, infoList);
        
        userString = sl.getUserString();
        system.assertEquals('<block>4444444444</block>', userString);
        
        infoList[0].Directory_Listing__c = 'Test User';
        infoList[0].Porting__c = null;
        sl = new QuoteBundleView.StdLine(ql, infoList);
        
        userString = sl.getUserString();
        system.assertEquals('<block>Test User</block>', userString);
        
    }
    
    private static testMethod void testCoreLine() {
        SBQQ__QuoteLine__c ql = insertTestLine();
        
        List<AdditionalInformation__c> infoList = new List<AdditionalInformation__c>{ new AdditionalInformation__c() };
        
        QuoteBundleView.CoreLine cl = new QuoteBundleView.CoreLine(ql, infoList);
        
        QuoteBundleView.CoreLine cl2 = new QuoteBundleView.CoreLine(ql, infoList);
        
        QuoteBundleView.sortCore(new List<QuoteBundleView.CoreLine>{cl, cl2}, 'asc');
        
        QuoteBundleView.sortCore(new List<QuoteBundleView.CoreLine>{cl, cl2}, 'desc');
    }
    
    private static testMethod void testAddlLine() {
        Opportunity o = new Opportunity(name = 'test', closedate = Date.today(), stagename = 'test');
        insert o;
        
        SBQQ__Quote__c quote = new SBQQ__Quote__c(SBQQ__Opportunity__c = o.id);
        insert quote;
        
        SBQQ__QuoteLine__c ql = insertTestLine();
        
        List<AdditionalInformation__c> infoList = new List<AdditionalInformation__c>{ new AdditionalInformation__c() };
        
        QuoteBundleView.AddlLine al = new QuoteBundleView.AddlLine(ql, infoList);
        QuoteBundleView.AddlLine al2 = new QuoteBundleView.AddlLine(ql, infoList);
        
        QuoteBundleView.sortAddl(new List<QuoteBundleView.AddlLine>{al, al2}, new Map<Id, SBQQ__QuoteLine__c>{ql.id => ql});
    }
    
    
    @isTest(SeeAllData=true) // for Pricebooks
    private static void testDeviceLine() {
        
        SBQQ__QuoteLine__c ql = insertTestLine();
        
        List<AdditionalInformation__c> infoList = new List<AdditionalInformation__c>{ new AdditionalInformation__c() };
        
        // no additional info
        QuoteBundleView.DeviceLine dl = new QuoteBundleView.DeviceLine(ql, null);
        
        String action = dl.getAction();
        system.assertEquals('', action);
        
        String term = dl.getTerm();
        system.assertEquals('', term);
        
        Decimal noTermPrice = dl.getNoTermPrice();
        system.assertEquals(null, noTermPrice);
        
        Decimal termDiscount = dl.getTermDiscount();
        system.assertEquals(null, termDiscount);
        
        Decimal credit = dl.getAirtimeCredit();
        system.assertEquals(null, credit);
        
        
        // additional info with empty fields
        dl = new QuoteBundleView.DeviceLine(ql, infoList);
        
        action = dl.getAction();
        system.assertEquals('', action);
        
        term = dl.getTerm();
        system.assertEquals('', term);
        
        
        // additional info with populated fields
        
        infoList[0].Phone_Number_Setup__c = 'phone_number_setup';
        infoList[0].Phone_Number_Setup_QED__c = 'phone_number_setup_QED';
        infoList[0].Service_Term__c = 'service_term';
        infoList[0].Service_Term_QED__c = 'service_term_QED';
        dl = new QuoteBundleView.DeviceLine(ql, infoList);
        
        action = dl.getAction();
        system.assertEquals('phone_number_setup', action);
        
        term = dl.getTerm();
        system.assertEquals('service_term', term);
        
        // QED fields populated, regular null
        
        infoList[0].Phone_Number_Setup__c = null;
        infoList[0].Service_Term__c = null;
        dl = new QuoteBundleView.DeviceLine(ql, infoList);
        
        action = dl.getAction();
        system.assertEquals('phone_number_setup_QED', action);
        
        term = dl.getTerm();
        system.assertEquals('service_term_QED', term);
        
        // Discount fields
        
        ql.SBQQ__Product__r.MTM__c = 100;
        update ql.SBQQ__Product__r;
        dl = new QuoteBundleView.DeviceLine(ql, infoList);
        
        noTermPrice = dl.getNoTermPrice();
        system.assertEquals(100, noTermPrice);
        
        termDiscount = dl.getTermDiscount();
        system.assertEquals(100, termDiscount);
        
        ql.SBQQ__NetPrice__c = 10;
        ql.SBQQ__Quantity__c = 1;
        update ql;
        
        ql = [SELECT SBQQ__Quote__c, SBQQ__Product__r.MTM__c, SBQQ__NetTotal__c FROM SBQQ__QuoteLine__c WHERE Id = :ql.id];

            dl = new QuoteBundleView.DeviceLine(ql, infoList);
            
        termDiscount = dl.getTermDiscount();
        system.assertEquals(90, termDiscount);
        
    }
    
    private static testMethod void testFeatureLine() {
            SBQQ__QuoteLine__c ql = insertTestLine();
        
        QuoteBundleView.FeatureLine fl = new QuoteBundleView.FeatureLine(ql);
    }
    
    
    // TEST HELPERS
    
    @isTest
    private static SBQQ__QuoteLine__c insertTestLine() {
        Opportunity o = new Opportunity(name = 'test', closedate = Date.today(), stagename = 'test');
        insert o;
        
        SBQQ__Quote__c quote = new SBQQ__Quote__c(SBQQ__Opportunity__c = o.id);
        insert quote;
        
        Product2 p = new Product2(name = 'test_product');
        insert p;
        
        SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c(
            SBQQ__Quote__c = quote.id,
            SBQQ__Quote__r = quote,
            SBQQ__Product__c = p.id,
            SBQQ__Product__r = p
        );
        insert ql;
        
        AdditionalInformation__c ai =
            new AdditionalInformation__c(quote__c = quote.id, quote__r = quote, quote_line__c = ql.id, quote_line__r = ql);
        insert ai;
        
        return ql;
    }
}