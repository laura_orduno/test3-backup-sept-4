public without sharing class QuoteContactController extends QuoteComponentParent {
    public ContactVO selectedContact {get; private set;}
    public String selectedContactId {get; set;}
    public Id accountId {get; set;}
    public Integer contactCount {get; private set;}
    public Web_Account__c account {get; private set;}
    
    private Id quoteId;
    private List<ContactVO> contacts;
    private String returnUrl;
    private Boolean multiSite;
    private Boolean addOn;
    private Boolean accountCreated;
    private String searchterm;
    
    public String dummyText {get; set;}
    public String dummyBoolean {get; set;}
    
    private String stage = ApexPages.currentPage().getParameters().get('stage');
    private String qid = ApexPages.currentPage().getParameters().get('qid');
    private SBQQ__Quote__c quote;
    public Boolean rCreditCheck;
    public Boolean rDraft;
    public Boolean rAgreement;
    
    private static final Map<String, Integer> requiredOrdinalByStatus = new Map<String, Integer>();
    static {
        Schema.DescribeFieldResult quoteStatusField = SBQQ__Quote__c.SBQQ__Status__c.getDescribe();
        List<Schema.PicklistEntry> quoteStatusList = quoteStatusField.getPicklistValues();
        Integer ordinal = 1;
        for(Schema.PicklistEntry ple : quoteStatusList) {
            requiredOrdinalByStatus.put(ple.getValue(), ordinal);
            ordinal++;
        }
    }
    public static Integer getRequiredOrdinalByStatus(String quoteStatus){
        return requiredOrdinalByStatus.get(quoteStatus);
    }
    
    
    public QuoteContactFormExtension quoteContactFE { get; set; }    
    public override void setComponentController(QuoteComponentController compController) {
       quoteContactFE = (QuoteContactFormExtension)compController;
    } 
    public override QuoteComponentController getComponentController() {
       return quoteContactFE;
    }
    
    public QuoteContactController() {
        returnUrl = ApexPages.currentPage().getParameters().get('retURL');
        accountId = ApexPages.currentPage().getParameters().get('aid');
        quoteId = ApexPages.currentPage().getParameters().get('qid');
        selectedContactId = ApexPages.currentPage().getParameters().get('id');
        multiSite = (ApexPages.currentPage().getParameters().get('ms') == '1');
        addOn = (ApexPages.currentPage().getParameters().get('addon') == '1');
        accountCreated = (ApexPages.currentPage().getParameters().get('ac') == '1');
        searchTerm = ApexPages.currentPage().getParameters().get('st');
        
        if (selectedContactId != null) {
            QueryBuilder qb = new QueryBuilder(Contact.sObjectType);
            for (Schema.SObjectField field : Schema.SObjectType.Contact.fields.getMap().values()) {
                qb.getSelectClause().addField(field);
            }
            qb.getWhereClause().addExpression(qb.eq(Contact.Id, (Id)selectedContactId));
            selectedContact = new ContactVO(Database.query(qb.buildSOQL()));
            accountId = selectedContact.record.Web_Account__c;
        } else {
            if (quoteId != null) {
                SBQQ__Quote__c quote = [SELECT SBQQ__Opportunity__r.Web_Account__c FROM SBQQ__Quote__c WHERE Id = :quoteId];
                accountId = quote.SBQQ__Opportunity__r.Web_Account__c;
                contactCount = [SELECT count() FROM Contact WHERE Web_Account__c = :accountId];
            }
            initContact();
        }
        account = QuotePortalUtils.loadAccountById(accountId);
             
       // Status Code
       rCreditCheck = false;
       rAgreement = false;
       if (qid != null) {
           quote = QuoteUtilities.loadQuote( qid );
           if (quote.id != null) {
               if (getRequiredOrdinalByStatus(quote.SBQQ__Status__c) >= getRequiredOrdinalByStatus(QuoteStatus.SENT_FOR_CREDIT_CHECK)) {
                   rCreditCheck = true;
               }
               if (getRequiredOrdinalByStatus(quote.SBQQ__Status__c) >= getRequiredOrdinalByStatus(QuoteStatus.SENT_AGREEMENT)) {
                   rAgreement= true;
               }
           }
        }
        rDraft = true;
        
        if (getRequiredOrdinalByStatus(stage) >= getRequiredOrdinalByStatus(QuoteStatus.SENT_FOR_CREDIT_CHECK)) {
            rCreditCheck = true;
        }
        if (getRequiredOrdinalByStatus(stage) >= getRequiredOrdinalByStatus(QuoteStatus.SENT_AGREEMENT)) {
            rAgreement = true;
        }
        // End Status Code
    }
    
    private void initContact() {
        selectedContact = new ContactVO(new Contact(Role_c__c='Payment Responsible',Language_Preference__c='English'));
        selectedContact.record.Web_Account__c = accountId;
    }
    
    
    public List<ContactVO> getContacts() {
        try {
        if (accountId != null) {
            loadContacts();
            contactCount = contacts.size();
        }
        return contacts; } catch (Exception e) { QuoteUtilities.log(e); } return null;
    }
    
    private void loadContacts() {
        this.contacts = new List<ContactVO>();
        for (Contact contact :
            [SELECT Id, Name, FirstName, LastName, Title, Phone, Role_c__c, Email FROM Contact
             WHERE Web_Account__c = :accountId]) {
            contacts.add(new ContactVO(contact));
        }
    }
    
    public String getPreviousPageURL() {
        try {
        PageReference pref = Page.QuoteWizardAccount;
        if (addOn) {
            pref = Page.SiteConfigureProducts;
            SBQQ__QuoteLine__c bundle = [SELECT Id FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c = :quoteId AND SBQQ__Bundle__c = true LIMIT 1];
            pref.getParameters().put('lid', bundle.Id);
            pref.getParameters().put('step', 'step2');
            pref.getParameters().put('addon', '1');
            QuotePortalUtils.addConfigurationSummaryParams(pref);
            pref.getParameters().put('retURL', Site.getCurrentSiteUrl() + Page.QuoteHome.getUrl().replaceAll('/apex/',''));
            pref.getParameters().put('saveURL', Site.getCurrentSiteUrl() + Page.QuoteWizardContact.getUrl().replaceAll('/apex/','') + '?qid=' + quoteId + '&step=step3' + (multiSite ? '&ms=1' : ''));
        } else {
            if (multiSite) {
                pref.getParameters().put('ms', '1');
            }
            pref.getParameters().put('step', 'step3');
        }
        pref.getParameters().put('qid', quoteId);
        if (accountCreated) {
            pref.getParameters().put('la', '1');
        }
        pref.getParameters().put('st', searchTerm);
        return pref.getUrl(); } catch (Exception e) { QuoteUtilities.log(e); } return null;
    }
    
    public PageReference getNextPage() {
        try {
        PageReference pref = Page.QuoteSummary;
        pref.setRedirect(true);
        pref.getParameters().put('qid', quoteId);
        pref.getParameters().put('step', 'step5');
        if (multiSite) {
            pref.getParameters().put('ms', '1');
        }
        if (addOn) {
            pref.getParameters().put('addon', '1');
        }
        return pref; } catch (Exception e) { QuoteUtilities.log(e); } return null;
    }
    
    public PageReference onSaveAndContinue() {
        System.Savepoint sp = Database.setSavepoint();
        try {
        // Validation!
        if (quoteContactFE != null && !quoteContactFE.doValidation()) {return null;}
        
        selectedContact.record.Web_Account__c = accountId;
        // Assign contact to a holding account
        selectedContact.record.AccountId = Quote_Portal__c.getInstance().Default_Account_Id__c;
        Contact temp = selectedContact.record.clone(false, false);
        insert temp;
        SBQQ__Quote__c quote = [SELECT Id, Group__c FROM SBQQ__Quote__c WHERE Id = :quoteId];
        quote.SBQQ__PrimaryContact__c = temp.Id;
        quote.Completed__c = true;
        QuoteCustomConfigurationController.linkUnassignedAdditionalInformation(quoteId);
        update quote;
        
        if (quote.Group__c != null) {
            Quote_Group__c qg = new Quote_Group__c(Id=quote.Group__c,Customer_Contact__c=temp.Id);
            update qg;
        }
        return getNextPage(); } catch (Exception e) { Database.rollback(sp); QuoteUtilities.log(e); } return null;
    }
    
    public PageReference onAddContact() {
        System.Savepoint sp = Database.setSavepoint();
        try {
        // Validation!
        if (quoteContactFE != null && !quoteContactFE.doValidation()) {return null;}
        
        selectedContact.record.AccountId = Quote_Portal__c.getInstance().Default_Account_Id__c;
        upsert selectedContact.record;
        contactCount++;
        initContact(); } catch (Exception e) {Database.rollback(sp); QuoteUtilities.log(e); } return null;
    }
    
    public PageReference onSelectAndContinue() {
        System.Savepoint sp = Database.setSavepoint();
        try {
        SBQQ__Quote__c quote = [SELECT Id, Group__c FROM SBQQ__Quote__c WHERE Id = :quoteId];
        quote.SBQQ__PrimaryContact__c = selectedContactId;
        QuoteCustomConfigurationController.linkUnassignedAdditionalInformation(quoteId);
        quote.Completed__c = true;
        update quote;
        
        if (quote.Group__c != null) {
            Quote_Group__c qg = new Quote_Group__c(Id=quote.Group__c,Customer_Contact__c=selectedContactId);
            update qg;
        } } catch (Exception e) { QuoteUtilities.log(e); Database.rollback(sp); return null; } return getNextPage();
    }
    
    public PageReference onSave() {
        System.Savepoint sp = Database.setSavepoint();
        try {
            // Validation!
            if (quoteContactFE != null && !quoteContactFE.doValidation()) {return null;}
            
            // Assign contact to a holding account
            selectedContact.record.AccountId = Quote_Portal__c.getInstance().Default_Account_Id__c;
            upsert selectedContact.record; } catch (Exception e) { QuoteUtilities.log(e); Database.rollback(sp); return null; }
        if (returnUrl == null) { 
            PageReference pref = ApexPages.currentPage();
            pref.setRedirect(true);
            return pref;
        }
        PageReference pref = new PageReference(returnUrl);
        pref.setRedirect(true);
        return pref;
    }
    
    public PageReference onDelete() {
        System.Savepoint sp = Database.setSavepoint();
        try { delete selectedContact.record; } catch (Exception e) { QuoteUtilities.log(e); Database.rollback(sp); return null; }
        PageReference pref = new PageReference(returnUrl);
        pref.setRedirect(true);
        return pref;
    }
    
    public PageReference onCancel() {
        PageReference pref = new PageReference(returnUrl);
        pref.setRedirect(true);
        return pref;
    }
}