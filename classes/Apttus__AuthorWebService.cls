/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global abstract class AuthorWebService {
    global AuthorWebService() {

    }
    webService static Boolean checkInAgreement(Id agreementId, Id docId) {
        return null;
    }
    webService static Id checkOutAgreement(Id agreementId, Boolean withLock) {
        return null;
    }
    webService static Apttus.CM.AgreementLockDO getAgreementLockDO(Id agreementId) {
        return null;
    }
    webService static Apttus__Agreement_Protection__c getAgreementProtectionSO(Id profileId, String actionName) {
        return null;
    }
    webService static List<Apttus.PageInfo> getAuthorPageInfos() {
        return null;
    }
    webService static Apttus__APTS_Template__c getClauseSO(Id clauseId) {
        return null;
    }
    webService static Apttus.CM.ClauseInfoTree getClauseTreeForType(String clauseType, Id agreementId) {
        return null;
    }
    webService static String getDefaultSortFieldExpr(String sObjectName) {
        return null;
    }
    webService static Apttus.SObjectDescribeInfo getSObjectAttributeDescribe(String sObjectName) {
        return null;
    }
    webService static Apttus.SObjectDescribeInfo getSObjectChildDescribe(String sObjectName) {
        return null;
    }
    webService static Apttus.SObjectDescribeInfo getSObjectDescribe(String sObjectName) {
        return null;
    }
    webService static Apttus.SObjectDescribeInfo getSObjectFieldDescribe(String sObjectName) {
        return null;
    }
    webService static Apttus.SObjectDescribeInfo getSObjectPartialDescribe(String sObjectName) {
        return null;
    }
    webService static Id getTemplateIdForReference(String referenceId) {
        return null;
    }
    webService static List<Attachment> getWordDocumentInfosForParent(Id parentId) {
        return null;
    }
    webService static Boolean isAgreementLocked(Id agreementId) {
        return null;
    }
    webService static Boolean isAgreementLockedBy(Id agreementId, Id userId) {
        return null;
    }
    webService static Boolean isMultipleCheckoutEnabled() {
        return null;
    }
    webService static Boolean isVersionControlInEffect() {
        return null;
    }
    webService static Boolean lockAgreement(Id agreementId) {
        return null;
    }
    webService static Id publishTemplate(Id templateVersionId, Apttus.SessionInfo sessionInfo) {
        return null;
    }
    webService static Boolean unlockAgreement(Id agreementId) {
        return null;
    }
}
