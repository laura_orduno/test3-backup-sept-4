//Controller for OC_SelectContact VF.
//Created By: Aditya Jamwal (IBM)
public class OC_SelectContactController {   
 
 	public String rcid{get;set;}
    public String ContId{get;set;}
    public String typeOfOrder{get;set;}
    public id parentAccId;
    public String existingOrderId {get; set;}
    public String selectedChildOrderId {get; set;}
    public String src {get;set;}
    public Boolean displayPageComp{get;set;}
    public String addressOrigin{get;set;}
    public Boolean addrNotValid{get;set;}
    public String tvAvailability{get;set;}
    public SMBCare_Address__c srvcAddress = new SMBCare_Address__c();
    public string forborne{get;set;}
    public String maximumSpeed{get;set;}
    public Boolean maxSpeedCopper{get;set;}
    public String primaryContactId {get;set;} // RTA - 616
    
    private List<String> caseIds = new List<String>();
    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%//
    
    
    /* Start Capture contact Code */
    public Boolean loaded {get; private set;}
    public String searchTerm {get; set;}
    public Contact[] contacts {get; private set;}
    public Contact new_contact {get; set;}
    public Integer contactsCount {get {if(contacts == null){return 0;} return contacts.size();}}
    @TestVisible public Boolean showNewContactForm {get; private set;}
    public static String selectedContactId{get;set;}
    public Id mostRecentlyCreatedContactId {get; private set;}
    public String errorMessage {get;set;}
    public Account account {get; set;}
    /* End Capture contact Code */
    
    
    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%//

    public OC_SelectContactController(){
        
        Id acctId = System.currentPageReference().getParameters().get('rcid');
        String cIds = System.currentPageReference().getParameters().get('caseIds');
        if (String.isNotBlank(cIds)){ caseIds = cIds.split(':'); }
        
        rcid = apexpages.currentpage().getparameters().get('rcid');
        
        primaryContactId = apexpages.currentpage().getparameters().get('primaryContactId'); // RTA - 616 fix
        // RTA - 616 fix
        if(String.isNotBlank(primaryContactId)){
            ContId = primaryContactId;
        }
        // End RTA - 616 fix
        ContId = System.currentPageReference().getParameters().get('contactId');
        existingOrderId = System.currentPageReference().getParameters().get('parentOrderId');
        selectedChildOrderId = System.currentPageReference().getParameters().get('id');
        system.debug('!@#! existingOrderId '+existingOrderId);
        if(String.isNotBlank(existingOrderId)){
            displayPageComp =true;
            Order theOrder = [SELECT Id, CustomerAuthorizedBy.Id, Service_Address__c, Service_Address__r.Id, AccountId FROM Order WHERE Id=: existingOrderId ];
            if(theOrder != null){
                // displayPageComp =true;
                if(String.isNotBlank(theOrder.CustomerAuthorizedBy.Id)){ //RTA - 616 fix
                	ContId = theOrder.CustomerAuthorizedBy.Id;                
                }
                parentAccId = theOrder.AccountId;
            }
        }
        else {
            displayPageComp = false;
            addrNotValid = false;
            tvAvailability = '';
            forborne = '';
            maxSpeedCopper = false;
        }
    }
    
   public PageReference UpdateOrder(){
        
        String passedOrderId = '';
        List<Order> order_to_updateList = new List<Order>();
        PageReference pg ;
        String childOrderId;
        if(String.isNotBlank(existingOrderId)){   

            passedOrderId = existingOrderId;
            pg  =  new PageReference('/apex/OC_SelectAndConfigure?id='+ passedOrderId + '&parentOrderId=' + passedOrderId +'&contactId=' + contId +'&rcid=' + rcid); 
            
           // RTA - 647 - added status Not Equal to Cancelled condition and Type field in a query 
           for(Order order_to_update : [SELECT Id,parentId__c, CustomerAuthorizedById, CustomerSignedBy__c, Type
                                        FROM Order WHERE (parentId__c =:existingOrderId OR Id =:existingOrderId) AND status != 'Cancelled']){
                order_to_update.CustomerAuthorizedById = ContId;
                order_to_update.CustomerSignedBy__c = ContId;
                if(typeOfOrder != null &&  typeOfOrder != ''){
                    order_to_update.Type =  typeOfOrder ;  
                }  
                
                order_to_updateList.add(order_to_update);
                if(String.isNotBlank(order_to_update.parentId__c) && String.isBlank(selectedChildOrderId)){
                  childOrderId = order_to_update.Id;
                }
            }    

            if(String.isNotBlank(selectedChildOrderId)){ childOrderId = selectedChildOrderId;}   
            
            if(null != order_to_updateList && order_to_updateList.size()>0){
                update order_to_updateList;
                if(order_to_updateList.size() > 1){
                    system.debug('!! order_to_updateList '+order_to_updateList.size());
                    pg  =  new PageReference('/apex/OC_SelectAndConfigure?id='+ childOrderId + '&parentOrderId=' + passedOrderId +'&contactId=' + contId +'&rcid=' + rcid); 
                }
            }
        }
        
        linkOrderToCase(passedOrderId);
        pg.setredirect(true);
        return pg;
    }
    
    private void linkOrderToCase(String orderId) {
        if (caseIds.size() > 0) {
            List<Case> cases = [select order__c, accountId from Case where Id in :caseIds];
            for (Case c : cases) {
                c.order__c = orderId;
                c.accountId = parentAccId;
                update c;
            }
        }
    }

    public void settypeOfOrder(String typeOfOrder) { this.typeOfOrder = typeOfOrder; }
  
    /* Start Capture contact Code */
    @TestVisible
    private void clear() {
        account = null;
        contacts= null;
    }
    @TestVisible
    private void loadContacts(){
        system.debug('@@account.Id'+account.id);
        contacts = [Select Name, Title, Phone, Email, Active__c,LastModifiedDate  From Contact 
                    Where AccountId = :account.id and firstName!=null Order By Active__c DESC, CreatedDate DESC limit 50];
        system.debug('@@Contacts'+contacts);
    }
    
    public PageReference onSelectContact() {
        errorMessage = '';
         if (selectedContactId == null ){
               return null;
         }

         return null;
    }

    public PageReference cancelNewContact(){
		showNewContactForm = !showNewContactForm;
        system.debug('Inside contact '+showNewContactForm );
        return null;
    }
    
    public PageReference toggleNewContactForm(){
        new_contact = new Contact(AccountId = account.id);
        showNewContactForm = !showNewContactForm;
        system.debug('Inside contact '+showNewContactForm );

        return null;
    }
     public PageReference createNewContact() {
        system.debug('>>>>>> 0 ' + new_contact);
        ApexPages.standardController sc = new ApexPages.standardController(new_contact);
        sc.save();

        system.debug('>>>>>> '+new_contact.name +' sc.getId() '+sc.getId());
        if(sc.getId() == null) {
            system.debug('>>>>>> 1 '+sc);
            return null;
        }
        
        List<Contact> contact_list = [Select FirstName, LastName, Name, Email, Title, Phone From Contact Where Id = :sc.getId()];
        if(contact_list.size()>0){
        	new_contact = contact_list.get(0);
        }	
        
        if (new_contact.Id != null){
            system.debug('>>>>>> 2 '+new_contact);
            showNewContactForm = false;
            mostRecentlyCreatedContactId = new_contact.id;
            new_contact = new Contact(AccountId = account.id);
            loadContacts();
        }
        return null;
    }

    public void onSearch(){
        try {
            if (searchTerm == null || searchTerm == '') { 
                loadContacts(); 
                return; 
            }
            searchTerm = '%' + searchTerm + '%';
            selectedContactId='';
            // List<List<sObject>> searchResult = [FIND :searchTerm IN All FIELDS RETURNING Contact (name, email, phone, title, Active__c,LastModifiedDate   Where AccountId = :account.parentId) limit 50];
            List<Contact>searchResult =[select id, name, email, phone, title, Active__c,LastModifiedDate from Contact where AccountId =:account.id and firstName!=null and (Name Like :searchTerm OR email Like :searchTerm OR phone Like :searchTerm) limit 50];
            // contacts = ((List<Contact>)searchResult[0]);
            contacts = searchResult;
        } catch (Exception e) {
        
        } 
        return;
    } 
        // OnLoad method invoked on click of Move & VF page load.
    public void onLoad() {

        loaded = false;
        showNewContactForm = false;
        
        try {
                if (ApexPages.currentPage() == null || ApexPages.currentPage().getParameters() == null){
                    clear();
                }
                String aid = ApexPages.currentPage().getParameters().get('rcid');
                if (aid == null || aid == '' || aid.substring(0, 3) != '001') {
                    clear();
                }
                Id xid = (Id) aid;
                account = [Select id,Name, Owner.Name,parentid From Account Where Id = :xid Limit 1];
                if (account == null){
                    clear();
                }
                loadContacts();
                if (contacts == null || contacts.size() == 0) { showNewContactForm = true; }
                new_contact = new Contact(AccountId = account.id);
            }
            catch (Exception e){
                clear();
            }
            loaded = true;   
    }
}