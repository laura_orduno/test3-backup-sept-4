public without sharing class ENTPUtils {
    
    private static Map<String, List<String>> CACHE_CATEGORY_TO_REQUESTTYPES;
    private static Map<String, List<String>> CACHE_STATUS_EXTERNALTOINTERNAL;
    private static Map<String, List<String>> CACHE_REQUESTTYPE_EXTERNALTOINTERNAL;
    private static Boolean HAS_CACHED_EXTERNAL_TO_INTERNAL = false;

    private static Account_Configuration__c CACHE_ACCOUNT_CONFIGURATION;
    private static Boolean CACHE_RESTRICT_TECH_SUPPORT;

    public static String LYNX_TICKET_SEARCH_XML_RESPONSE = 
        '<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' +
		'<soapenv:Body>' +
        '<TroubleTicketList xmlns="http://www.telus.com/schema/servicestatus/troubleticket">' +
        '<TroubleTicket xsi:type="TroubleTicket">' +
        '<TELUSTicketId>1000143457</TELUSTicketId>' +
        '<description>TEST</description>' +
        '<sourceTypeCode>CUSTOMER</sourceTypeCode>' +
        '<ticketTypeCode>Ticket</ticketTypeCode>' +
        '<severityCode>7 - NORMAL</severityCode>' +
        '<priorityCode>In Service</priorityCode>' +
        '<statusCode>Open</statusCode>' +
        '<subStatusCode>Assigned</subStatusCode>' +
        '<customerDueDate>2016-05-22T15:00:03.000Z</customerDueDate>' +
        '<eventStartDate>2016-05-20T15:00:04.000Z</eventStartDate>' +
        '<createDate>2016-05-20T15:00:04.000Z</createDate>' +
        '<modifyDateTime>2016-12-27T01:10:58.000Z</modifyDateTime>' +
        '<createdByName>T000003</createdByName>' +
        '<assignToWorkgroupName>Enhanced - MSS - ADSSI - SAS</assignToWorkgroupName>' +
        '<comments>Test Ticket</comments>' +
        '<appointmentStartDate>2016-05-20T15:07:52.000Z</appointmentStartDate>' +
        '<appointmentDuration>2</appointmentDuration>' +
        '<lastDispatchSystem>WFM</lastDispatchSystem>' +
        '<SLAFlag>N</SLAFlag>' +
        '<estimatedOutage>0</estimatedOutage>' +
        '<currentEscalation>0</currentEscalation>' +
        '<accessInformation>8-4</accessInformation>' +
        '<workforceRequired>Enhanced Services</workforceRequired>' +
        '<nextEscalationDateTime>2016-05-20T16:05:04.000Z</nextEscalationDateTime>' +
        '<controllingWorkGroupCode>Enhanced - AB 46020 Alarms</controllingWorkGroupCode>' +
        '<lineOfBusinessCode>Enhanced</lineOfBusinessCode>' +
        '<troubleTypeCode>NETWORK</troubleTypeCode>' +
        '<serviceAddress>' +
        '<houseNumber>1702</houseNumber><streetName>23 St N</streetName><cityName>Lethbridge</cityName>' +
        '<provinceStateName>AB</provinceStateName><postalCode>T1H5B3</postalCode></serviceAddress>' +
        '<contractData xsi:type="ContractData"><contractIdentification><contractIdTypeCode>SLA</contractIdTypeCode>' +
        '<contractIdValue>Internet Standard 24x7 incl weekend &amp; holiday</contractIdValue></contractIdentification></contractData>' +
        '<customer xsi:type="Customer"><customerIdentification><customerIdTypeCode>SIEBEL_LYNX</customerIdTypeCode>' +
        '<customerIdValue>1-4N3-343</customerIdValue></customerIdentification><customerIdentification>' +
        '<customerIdTypeCode>SIEBEL_LYNX_STG</customerIdTypeCode><customerIdValue>1-ZKG-8614</customerIdValue></customerIdentification>' +
        '<customerIdentification><customerIdTypeCode>ESD</customerIdTypeCode><customerIdValue>3393</customerIdValue></customerIdentification>' +
        '<customerName>TELUS ADVANCED COMMUNICATIONS CORE</customerName></customer>' +
        '<customerSubscription xsi:type="CustomerSubscription"><productType>Managed Object</productType>' +
        '<serviceDesignation>NULL</serviceDesignation><classOfService>NULL</classOfService><COID>0250</COID>' +
        '<customerSubscriptionIdentification><customerSubscriptionIdTypeCode>SIEBEL</customerSubscriptionIdTypeCode>' +
        '<customerSubscriptionIdValue>LTBR18-DSLM01</customerSubscriptionIdValue></customerSubscriptionIdentification>' +
        '<customerSubscriptionIdentification><customerSubscriptionIdTypeCode>ESD</customerSubscriptionIdTypeCode>' +
        '<customerSubscriptionIdValue>MO7130</customerSubscriptionIdValue></customerSubscriptionIdentification></customerSubscription>' +
        '<contact xsi:type="Contact"><contactIdentification><contactID>0</contactID></contactIdentification><firstName>Bill</firstName>' +
        '<middleName>T</middleName><fullName>Bill T Stuewe</fullName><lastName>Stuewe</lastName>' +
        '<telecommunicationsaddress><telecommunicationsAddressUsageType>CBR</telecommunicationsAddressUsageType>' +
        '<telephoneNumber>9999999999</telephoneNumber></telecommunicationsaddress><electronicaddress>' +
        '<electronicAddressUsageType>EMAIL</electronicAddressUsageType><electronicAddressValue>Bill.Stuewe@telus.com</electronicAddressValue></electronicaddress></contact>' +
        '<troubleTicketActivity><TELUSTroubleTicketId>1000143457</TELUSTroubleTicketId><activityTypeCode>Notify Customer</activityTypeCode><TELUSActivityId>1-1VYPI8N</TELUSActivityId><statusCode>Submit</statusCode><createdBy>T000001</createdBy><resolutionCondition>In Service</resolutionCondition>' +
        '<workforceRequired>Enhanced Services</workforceRequired><workgroupTime>0</workgroupTime><totalActivityTime>0</totalActivityTime><CWCTotalTimeHours>0</CWCTotalTimeHours><CWCTotalTimeMinutes>0</CWCTotalTimeMinutes><CWCEquipmentCost>0</CWCEquipmentCost><assignToWorkgroup>Enhanced - MSS - ADSSI - SAS</assignToWorkgroup><activityOwner>T000001</activityOwner><activityComments>test notify</activityComments><notifyCustomerFlag>Y</notifyCustomerFlag><activityCreateDateTime>2016-06-24T17:31:16.000Z</activityCreateDateTime><activityIdTypeCode>SIEBEL</activityIdTypeCode><completedFlag>N</completedFlag><modifiedDateTime>2016-06-24T17:31:38.000Z</modifiedDateTime>' +
        '<actvitiyLastUpdateTimeTTODS>2016-06-24T17:31:44.000Z</actvitiyLastUpdateTimeTTODS></troubleTicketActivity><troubleTicketActivity><TELUSTroubleTicketId>1000143457</TELUSTroubleTicketId><activityTypeCode>Route</activityTypeCode><TELUSActivityId>1-1VYPI85</TELUSActivityId>' +
        '<statusCode>Unassigned</statusCode><createdBy>T000001</createdBy><resolutionCondition>In Service</resolutionCondition>' +
        '<workforceRequired>Enhanced Services</workforceRequired><workgroupTime>0</workgroupTime><totalActivityTime>0</totalActivityTime><CWCTotalTimeHours>0</CWCTotalTimeHours><CWCTotalTimeMinutes>0</CWCTotalTimeMinutes><CWCEquipmentCost>0</CWCEquipmentCost><assignToWorkgroup>Enhanced - MSS - ADSSI - SAS</assignToWorkgroup><activityOwner>T000001</activityOwner><activityComments>testing re-routing, sorry for stealing your ticket!</activityComments><notifyCustomerFlag>Y</notifyCustomerFlag><plannedStartDateTime>2016-06-24T17:30:00.000Z</plannedStartDateTime><activityCreateDateTime>2016-06-24T17:28:29.000Z</activityCreateDateTime><activityIdTypeCode>SIEBEL</activityIdTypeCode><completedFlag>N</completedFlag>'+
        '<modifiedDateTime>2016-06-24T17:30:01.000Z</modifiedDateTime><actvitiyLastUpdateTimeTTODS>2016-06-24T17:30:01.000Z</actvitiyLastUpdateTimeTTODS></troubleTicketActivity><troubleTicketActivity><TELUSTroubleTicketId>1000143457</TELUSTroubleTicketId><activityTypeCode>Route</activityTypeCode><TELUSActivityId>1-1VXSCT4</TELUSActivityId><statusCode>Done</statusCode><createdBy>SADMIN</createdBy><resolutionCondition>In Service</resolutionCondition><workforceRequired>Enhanced Services</workforceRequired><workgroupTime>0</workgroupTime><totalActivityTime>33079.07</totalActivityTime><CWCTotalTimeHours>0</CWCTotalTimeHours>' +
        '<CWCTotalTimeMinutes>0</CWCTotalTimeMinutes><CWCEquipmentCost>0</CWCEquipmentCost><assignToWorkgroup>LYNX - Common Cause</assignToWorkgroup>' +
        
        '<activityOwner>SADMIN</activityOwner><activityComments>Child ticket relationship created with parent ticket. Routed to Common Cause by LYNX Workflow.</activityComments><notifyCustomerFlag>Y</notifyCustomerFlag><plannedStartDateTime>2016-06-01T18:10:56.000Z</plannedStartDateTime><closedDate>2016-06-24T17:30:00.000Z</closedDate><activityCreateDateTime>2016-06-01T18:10:56.000Z</activityCreateDateTime><activityIdTypeCode>SIEBEL</activityIdTypeCode><completedFlag>N</completedFlag><modifiedDateTime>2016-06-24T17:30:00.000Z</modifiedDateTime><actvitiyLastUpdateTimeTTODS>2016-06-24T17:30:01.000Z</actvitiyLastUpdateTimeTTODS></troubleTicketActivity>' +
        '<troubleTicketActivity><TELUSTroubleTicketId>1000143457</TELUSTroubleTicketId><activityTypeCode>Route</activityTypeCode>' +
        '<TELUSActivityId>1-1VW8YLT</TELUSActivityId><statusCode>Done</statusCode><createdBy>T000003</createdBy>' +
        '<resolutionCondition>In Service</resolutionCondition><workforceRequired>Enhanced Services</workforceRequired><workgroupTime>17461.43</workgroupTime><totalActivityTime>17462.32</totalActivityTime><CWCTotalTimeHours>0</CWCTotalTimeHours><CWCTotalTimeMinutes>0</CWCTotalTimeMinutes><CWCEquipmentCost>0</CWCEquipmentCost><assignToWorkgroup>DMC - Dispatch</assignToWorkgroup><activityOwner>T000003</activityOwner><activityComments>route to dispatch</activityComments><notifyCustomerFlag>Y</notifyCustomerFlag><plannedStartDateTime>2016-05-20T15:09:29.000Z</plannedStartDateTime><assignedDateTime>2016-05-20T15:09:29.000Z</assignedDateTime>' +
        '<closedDate>2016-06-01T18:10:55.000Z</closedDate><activityCreateDateTime>2016-05-20T15:08:36.000Z</activityCreateDateTime><activityIdTypeCode>SIEBEL</activityIdTypeCode><completedFlag>N</completedFlag><modifiedDateTime>2016-06-01T18:10:55.000Z</modifiedDateTime><actvitiyLastUpdateTimeTTODS>2016-06-01T18:10:56.000Z</actvitiyLastUpdateTimeTTODS></troubleTicketActivity><troubleTicketActivity><TELUSTroubleTicketId>1000143457</TELUSTroubleTicketId><activityTypeCode>Route</activityTypeCode><TELUSActivityId>1-1VWBP6W</TELUSActivityId><statusCode>Done</statusCode><createdBy>SADMIN</createdBy><resolutionCondition>In Service</resolutionCondition><workforceRequired>Enhanced Services</workforceRequired><workgroupTime>0</workgroupTime>' +
        '<totalActivityTime>4.13</totalActivityTime><CWCTotalTimeHours>0</CWCTotalTimeHours><CWCTotalTimeMinutes>0</CWCTotalTimeMinutes><CWCEquipmentCost>0</CWCEquipmentCost>' +
        '<assignToWorkgroup>Enhanced - AB 46020 Alarms</assignToWorkgroup><activityOwner>SADMIN</activityOwner><activityComments>New TT within SLA coverage - page sent to on call employee XMONDRA. Routed to responsible workgroup by LYNX Workflow.</activityComments><notifyCustomerFlag>Y</notifyCustomerFlag><plannedStartDateTime>2016-05-20T15:05:21.000Z</plannedStartDateTime><closedDate>2016-05-20T15:09:29.000Z</closedDate><activityCreateDateTime>2016-05-20T15:05:21.000Z</activityCreateDateTime><activityIdTypeCode>SIEBEL</activityIdTypeCode><completedFlag>N</completedFlag><modifiedDateTime>2016-05-20T15:09:29.000Z</modifiedDateTime><actvitiyLastUpdateTimeTTODS>2016-05-20T15:09:29.000Z</actvitiyLastUpdateTimeTTODS></troubleTicketActivity><lastUpdateTimeTTODS>2016-12-27T01:11:55.000Z</lastUpdateTimeTTODS>' +
        '</TroubleTicket>' +
        '</TroubleTicketList>' +
        '</soapenv:Body>' +
        '</soapenv:Envelope>';

    public static String REMEDY_XML_RESPONSE = 
		'<?xml version="1.0" encoding="UTF-8"?>' + 
        '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' + 
        '<soapenv:Body>' + 
        '<ns0:OpCreateResponse xmlns:ns0="urn:ExternalTicketSubmissionView" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' +
		'<ns0:Business_Unit_Id>PCLOUD</ns0:Business_Unit_Id>' +
		'<ns0:Client_Phone/>' +
		'<ns0:Company_Id>TCS</ns0:Company_Id>' +
		'<ns0:Configuration_id/>' +
		'<ns0:Customer_Update_Info/>' +
		'<ns0:Description> 01/24/17 11:53:25 salesforcIuyoiuyoiyiouyoio</ns0:Description>' +
		'</ns0:OpCreateResponse>' + 
		'</soapenv:Body>' + 
        '</soapenv:Envelope>';
    public static String LYNX_XMLCREATE_RESPONSE =
    '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' +
   '<soapenv:Body>'+
      '<OK xmlns="http://ebonding.telus.com"/>'+
   '</soapenv:Body>'+
'</soapenv:Envelope>';
    /**
     *  Utility method for accessing cached category to request types map
     *
     * @lastmodified
     *   Dan Reich (TOD), 10/22/14
     *
     */
    public static Map<String, List<String>> getCategoryRequestTypes() {
        if ( CACHE_CATEGORY_TO_REQUESTTYPES == null ) {
            List<ParentToChild__c> ptcs = ParentToChild__c.getAll().values();
            CACHE_CATEGORY_TO_REQUESTTYPES = new Map<String, List<String>>();
            for (ParentToChild__c ptc : ptcs) {
                if(ptc.Identifier__c.equals('ENTPCatRequest')) {
                    if(CACHE_CATEGORY_TO_REQUESTTYPES.get(ptc.Parent__c) == null) {
                        CACHE_CATEGORY_TO_REQUESTTYPES.put(ptc.Parent__c, new List<String>());
                    }
                    CACHE_CATEGORY_TO_REQUESTTYPES.get(ptc.Parent__c).add(ptc.Child__c);
                }
            }
        }
        return CACHE_CATEGORY_TO_REQUESTTYPES;
    }
    
    /**
     *  Utility method for accessing cached status picklist values list
     *
     * @lastmodified
     *   Dan Reich (TOD), 10/22/14
     *
     */
    public static Map<String, List<String>> getExternalToInternalStatuses() {
        if ( CACHE_STATUS_EXTERNALTOINTERNAL == null ) {
            cacheExternalToInternalMaps();
        }
        return CACHE_STATUS_EXTERNALTOINTERNAL;
    }
    
    /**
     *  Utility method for accessing cached status picklist values list
     *
     * @lastmodified
     *   Dan Reich (TOD), 10/22/14
     *
     */
    public static Map<String, List<String>> getRequestTypeToRecordTypeNames() {
        if ( CACHE_REQUESTTYPE_EXTERNALTOINTERNAL == null ) {
            cacheExternalToInternalMaps();
        }
        return CACHE_REQUESTTYPE_EXTERNALTOINTERNAL;
    }   

    private static void cacheExternalToInternalMaps() {
        if( !HAS_CACHED_EXTERNAL_TO_INTERNAL ) {
            List<ExternalToInternal__c> extToInts = ExternalToInternal__c.getAll().values();
            CACHE_STATUS_EXTERNALTOINTERNAL = new Map<String, List<String>>();
            CACHE_REQUESTTYPE_EXTERNALTOINTERNAL = new Map<String, List<String>>();
            for (ExternalToInternal__c  extToInt :  extToInts) {
                if( extToInt.Identifier__c.equals('onlineStatus')) {
                    if(CACHE_STATUS_EXTERNALTOINTERNAL.get( extToInt.External__c) == null) {
                        CACHE_STATUS_EXTERNALTOINTERNAL.put( extToInt.External__c, new List<String>());
                    }
                    CACHE_STATUS_EXTERNALTOINTERNAL.get( extToInt.External__c).add( extToInt.Internal__c);
                } else if( extToInt.Identifier__c.equals( 'onlineRequestTypeToCaseRT' ) ) {
                    if(CACHE_REQUESTTYPE_EXTERNALTOINTERNAL.get( extToInt.External__c) == null) {
                        CACHE_REQUESTTYPE_EXTERNALTOINTERNAL.put( extToInt.External__c, new List<String>());
                    }
                    CACHE_REQUESTTYPE_EXTERNALTOINTERNAL.get( extToInt.External__c).add( extToInt.Internal__c);
                }
            }
        }
    }

    public static Set<String> getRecordTypeIdsByCategory( String category ) {
        Set<String> recordTypeIds = new Set<String>();
        List<String> requestTypes = getCategoryRequestTypes().get( category );
        if( requestTypes != null && !requestTypes.isEmpty() ) {
            List<String> recordTypeNames = new List<String>();
            for( String requestType : requestTypes ) {
                List<String> typeList = getRequestTypeToRecordTypeNames().get( requestType );
                if( typeList != null ) {
                    recordTypeNames.addAll( typeList );
                }
            }
            if( !recordTypeNames.isEmpty() ) {
                Map<String, Schema.RecordTypeInfo> rtNameToInfo = Schema.SObjectType.Case.getRecordTypeInfosByName();
                for( String name : recordTypeNames ) {
                    Schema.RecordTypeInfo rtInfo = rtNameToInfo.get( name );
                    if( rtInfo != null ) {
                        recordTypeIds.add( rtInfo.getRecordTypeId() );
                    }
                }
            }
        }
        return recordTypeIds;
    }

    public static String getRequestTypeFilterForCategory( String category ) {
        return getInClause( getCategoryRequestTypes().get( category ) );
    }

    public static String getInternalStatusFilterByExternal( String external ) {
        return getInClause( getExternalToInternalStatuses( ).get( external ) );
    }

    public static String getInClause( List<String> values ) {
       
        return '( \'' + String.join( values, '\', \'' ) + '\' )';
       
    }

    public static String getInClause( Set<String> values ) {
        return getInClause( new List<String>( values ) );
    }

    /**
     *  Utility method for accessing cached Account_Configuration__c object
     *
     * @lastmodified
     *   Alex Kong (TOD), 11/18/14
     *
     */
    public static Account_Configuration__c getAccountConfiguration() {
        if (CACHE_ACCOUNT_CONFIGURATION == null) {
            List<Account_Configuration__c> configs = new List<Account_Configuration__c>();
            Account_Configuration__c config = new Account_Configuration__c();
            Id userId = UserInfo.getUserId();
            User u = [SELECT Id, Contact.Account.Id FROM User WHERE Id = :userId];
            //System.debug('akong: helperGetAccountConfiguration: u.Contact.Account.Id: ' + u.Contact.Account.Id);
            if (u.Contact.Account.Id != null) {
                configs = [
                    SELECT Id, Restrict_Tech_Support__c 
                    FROM Account_Configuration__c 
                    WHERE Account__c = :u.Contact.Account.Id
                    ORDER BY CreatedDate ASC
                ];
            }
            //System.debug('akong: helperGetAccountConfiguration: configs: ' + configs);
            if (configs.size() > 0) {
                CACHE_ACCOUNT_CONFIGURATION = configs[0];
            }
        }
        return CACHE_ACCOUNT_CONFIGURATION;
    }

    /**
     *  Utility method for accessing cached Account_Configuration__c.Restrict_Tech_Support__c boolean
     *
     * @lastmodified
     *   Alex Kong (TOD), 11/18/14
     *
     */
    public static Boolean restrictTechSupport() {
        if (CACHE_RESTRICT_TECH_SUPPORT == null) {
            Account_Configuration__c cfg = getAccountConfiguration();
            if (cfg != null && cfg.Restrict_Tech_Support__c) {
                CACHE_RESTRICT_TECH_SUPPORT = true;
            } else {
                CACHE_RESTRICT_TECH_SUPPORT = false;
            }
        }
        return CACHE_RESTRICT_TECH_SUPPORT;
    }

    /**
     *  Utility method for sending templated Case-related emails to an arbitrary list of email addresses.
     *  This method uses the transaction rollback trick so we can merge data into a SingleEmailMessage object without
     *  being forced to send to a Contact.
     *
     * @lastmodified
     *   Alex Kong (TOD), 2015-03-04
     *   Christian Wico (TOD), 2015-03-05
     */
    public static List<Messaging.SendEmailResult> sendTemplatedCaseEmails(List<String> toAddresses, Id fromEmailAddressId, Id templateId, Id caseId) {
        List<Messaging.SendEmailResult> mailResults = new List<Messaging.SendEmailResult>();
        if (toAddresses.size() > 0) {
           
            // get a prepopulated list of email messages from Case Email Template 
            List<Messaging.SingleEmailMessage> lstMsgsToSend = getMergedEmailsForCase(toAddresses, fromEmailAddressId, templateId, caseId);            
            System.debug('Collab lstMsgsToSend: ' + lstMsgsToSend);
            
            // reserve capacity
            Integer reserveCount = lstMsgsToSend.size() * toAddresses.size();
            System.debug('Collab reserveCount: ' + reserveCount);
            Messaging.reserveSingleEmailCapacity(reserveCount);
            
            // execute mail out
            mailResults = Messaging.sendEmail(lstMsgsToSend);
            System.debug('Collab mailResults: ' + mailResults);
        }
        return mailResults;
    }

    /**
    * Retrieves a list of email merged templates for a case
    * @lastmodified
    *       Christian Wico (TOD), 2015-03-05
    *       Alex Kong (TOD), 2015-03-09
    */
    public static List<Messaging.SingleEmailMessage> getMergedEmailsForCase(List<String> toAddresses, Id fromEmailAddressId, Id templateId, Id caseId) {
        // setup the SingleEmailMessage as if sending to a contact

        ENTP_Case_Collaborator_Settings__c collabSettings = ENTP_Case_Collaborator_Settings__c.getOrgDefaults();
        //System.debug('Collab collabSettings: ' + collabSettings);
        Id dummyContactId = collabSettings.Dummy_Contact_id__c;
        //Contact c = [SELECT Id, Email FROM Contact WHERE Id = :dummyContactId LIMIT 1];

        List<Messaging.SingleEmailMessage> lstMsgs = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
        //msg.setTemplateId( [select id from EmailTemplate where DeveloperName='MBR_new_case_created_VF'].id );
        //msg.setTemplateId( '00X4000000163uYEAQ' );
        msg.setTemplateId( templateId );
        msg.setWhatId( caseId );
        msg.setTargetObjectId(dummyContactId);
        msg.setToAddresses( toAddresses );
        if (fromEmailAddressId != null) {
            msg.setOrgWideEmailAddressId(fromEmailAddressId);
        }
        System.debug('Collab toAddresses: ' + toAddresses);
        
        lstMsgs.add(msg);
        
        // Send the emails in a transaction, then roll it back (so nothing actually goes out)
        Savepoint sp = Database.setSavepoint();
        Messaging.sendEmail(lstMsgs);
        Database.rollback(sp);

       // For each SingleEmailMessage that was just populated by the sendEmail() method, copy its
        // contents to a new SingleEmailMessage. Then send those new messages.
        List<Messaging.SingleEmailMessage> lstMsgsToSend = new List<Messaging.SingleEmailMessage>();

        for (Messaging.SingleEmailMessage email : lstMsgs) {
            Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
            emailToSend.setToAddresses(email.getToAddresses());
            //emailToSend.setPlainTextBody(email.getPlainTextBody());
            emailToSend.setHTMLBody(email.getHTMLBody());
            emailToSend.setSubject(email.getSubject());
            if (fromEmailAddressId != null) {
                emailToSend.setOrgWideEmailAddressId(fromEmailAddressId);
            }
            lstMsgsToSend.add(emailToSend);
        }
        return lstMsgsToSend;
    }

    public static HttpResponse mockWebserviceResponse(String body){
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(body);
        res.setStatusCode(200);        
        
        return res;
    }	
    
    public static Boolean validateEmail(String email) {
        Boolean res = true;
        String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$'; // source: <a href="http://www.regular-expressions.info/email.html" target="_blank" rel="nofollow">http://www.regular-expressions.info/email.html</a>
        Pattern MyPattern = Pattern.compile(emailRegex);
        Matcher MyMatcher = MyPattern.matcher(email);

System.debug('MyMatcher.matches(): ' + MyMatcher.matches());        
        
        if (!MyMatcher.matches()){
            res = false;        
        }
        
        return res;
    }    
}