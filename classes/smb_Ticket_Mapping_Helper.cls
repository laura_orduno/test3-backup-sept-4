/*
###########################################################################
# File..................: smb_Ticket_Mapping_Helper
# Version...............: 36
# Created by............: Sandip Chaudhari
# Created Date..........: 08 March 2016

# Description...........: This class contains methods to map troble ticket details to TicketVO object from various systems
#
###########################################################################
# Last Modified Details......: 
*/

public class smb_Ticket_Mapping_Helper {
    public static Map<String, TT_Aggrgation_Status_Mapping__c> statusMapping = new Map<String, TT_Aggrgation_Status_Mapping__c>();
    /* Method to invoke mapping methods for each source system */
    public static void doMapping(TicketVO.ServiceRequestAndResponseParameter reqResObj){
        try{
            reqResObj.lstTickets = new List<TicketVO.TicketData>();
            doLavaStormMapping(reqResObj);
            HttpResponse response1 = Continuation.getResponse('Continuation-1');
            system.debug('@@@ test status ' + response1.getStatusCode());
            if(response1.getStatusCode()==200){
                doTTODSMapping(reqResObj.ttListFuture.getValue(), reqResObj); // TTODS Mapping
            }
            // 19 May 2016 - Sandip - Removed commentted line for deploying SRM changes
            HttpResponse response2 = Continuation.getResponse('Continuation-2');
            if(response2.getStatusCode()==200){
                doSRMMapping(reqResObj.srmTTListFuture.getValue(), reqResObj); // SRM Mapping
            }
        }catch(Exception ex){
            system.debug('##### Exception ' + ex);
        }
    }
    
    /* Method used to do mapping for data received from TTODS system */
    public static void doTTODSMapping(list<smb_TTODS_troubleticket.TroubleTicket> lstTrobleTicket, TicketVO.ServiceRequestAndResponseParameter reqResObj){
        statusMapping = getStatusMapping();
        if(lstTrobleTicket!=null && lstTrobleTicket.size()>0){
            for(smb_TTODS_troubleticket.TroubleTicket obj: lstTrobleTicket){
                // mapping for each ticket
                TicketVO.TicketData ticketVOobj = new TicketVO.TicketData();
                ticketVOobj.sourceSystem = TicketVO.SRC_TTODS;
                ticketVOobj.TELUSTicketId = obj.TELUSTicketId;
                ticketVOobj.RelatedTicketId = obj.RelatedTicketId;
                ticketVOobj.description = obj.description;
                ticketVOobj.sourceTypeCode = obj.sourceTypeCode;
                ticketVOobj.ticketTypeCode = obj.ticketTypeCode;
                ticketVOobj.priorityCode =  obj.priorityCode;
                ticketVOobj.statusCode = obj.statusCode;
                
                // Sort Logic on Status
                if(statusMapping != null && statusMapping.get('DefaultSortPriority') != null && statusMapping.get('DefaultSortPriority').Sort_Priority__c != null){
                     ticketVOobj.sortOrder = Integer.ValueOf(statusMapping.get('DefaultSortPriority').Sort_Priority__c);
                }
               
                if(obj.statusCode != null && obj.statusCode != ''){
                    TT_Aggrgation_Status_Mapping__c statusObj = statusMapping.get(obj.statusCode);
                    if(statusObj != null){
                        ticketVOobj.statusCode = statusObj.value__c;
                        if(statusObj.Sort_Priority__c != null){
                            ticketVOobj.sortOrder = Integer.ValueOf(statusObj.Sort_Priority__c);
                        }
                        // Sandip 06 June 2016 - production fix convert status as per locale
                        if (UserInfo.getLocale().startsWith('fr')){
                            if(statusObj.French_Translation__c != null){
                                ticketVOobj.statusCode = statusObj.French_Translation__c;
                            }
                        }
                        // end
                    }
                }
                /*if(ticketVOobj.statusCode == 'Open'){
                    ticketVOobj.sortOrder = 1;
                }else{
                    ticketVOobj.sortOrder = 3;
                }*/
                //End
                ticketVOobj.subStatusCode = obj.subStatusCode;
                ticketVOobj.customerDueDate = obj.customerDueDate;
                try{
                    ticketVOobj.closeDate = obj.closeDate;
                    ticketVOobj.closeDateStr = getLocaleDate(ticketVOobj.closeDate);
                    ticketVOobj.createDate = obj.createDate;
                    ticketVOobj.createDateStr = getLocaleDate(ticketVOobj.createDate);
                }catch (Exception ex){ system.debug('@@@## Date Format Exception..' + ex); }
                ticketVOobj.troubleTypeCode = obj.troubleTypeCode;
                ticketVOobj.modifyDateTime = obj.modifyDateTime;
                ticketVOobj.createdByName = obj.createdByName;
                ticketVOobj.assignToWorkgroupName =  obj.assignToWorkgroupName;
                ticketVOobj.comments = obj.comments;
                ticketVOobj.toolTipComments = obj.comments;
                ticketVOobj.severityCode = obj.severityCode;
                ticketVOobj.eventStartDate = obj.eventStartDate;
                ticketVOobj.eventEndDate = obj.eventEndDate;
                ticketVOobj.specialInstructionComment = obj.specialInstructionComment;
                ticketVOobj.repeatFlag = obj.repeatFlag;
                ticketVOobj.chronicFlag = obj.chronicFlag;
                ticketVOobj.CRTCFlag = obj.CRTCFlag;
                ticketVOobj.WFMWoID = obj.WFMWoID;
                ticketVOobj.ETTSFlag = obj.ETTSFlag;
                ticketVOobj.GPONFlag = obj.GPONFlag;
                ticketVOobj.loopBondedFlag = obj.loopBondedFlag;
                ticketVOobj.appointmentStartDate = obj.appointmentStartDate;
                ticketVOobj.appointmentDuration = obj.appointmentDuration;
                ticketVOobj.lastDispatchSystem = obj.lastDispatchSystem;
                ticketVOobj.SLAFlag = obj.SLAFlag;
                ticketVOobj.specialStudies = obj.specialStudies;
                ticketVOobj.SMCAlliance = obj.SMCAlliance;
                ticketVOobj.outageType = obj.outageType;
                ticketVOobj.estimatedOutage = obj.estimatedOutage;
                ticketVOobj.currentEscalation = obj.currentEscalation;
                ticketVOobj.mainPhoneNumber = obj.mainPhoneNumber;
                ticketVOobj.accessInformation = obj.accessInformation;
                ticketVOobj.dotNetCustomerUserID = obj.dotNetCustomerUserID;
                ticketVOobj.stationField = obj.stationField;
                ticketVOobj.modemType = obj.modemType;
                ticketVOobj.contactMode = obj.contactMode;
                ticketVOobj.workforceRequired = obj.workforceRequired;
                ticketVOobj.nextEscalationDateTime = obj.nextEscalationDateTime;
                ticketVOobj.NTMSReferredFlag = obj.NTMSReferredFlag;
                ticketVOobj.troubleticketRelationTypeCode = obj.troubleticketRelationTypeCode;
                ticketVOobj.lineTestResult = obj.lineTestResult;
                ticketVOobj.lineTestOhmsShort = obj.lineTestOhmsShort;
                ticketVOobj.lineTestCapacitance = obj.lineTestCapacitance;
                ticketVOobj.plannedStartDateTime = obj.plannedStartDateTime;
                ticketVOobj.plannedEndDateTime = obj.plannedEndDateTime;
                ticketVOobj.controllingWorkGroupCode = obj.controllingWorkGroupCode;
                ticketVOobj.assignToBIN = obj.controllingWorkGroupCode;
                ticketVOobj.nonPubFlag = obj.nonPubFlag;
                ticketVOobj.level1Code = obj.level1Code;
                ticketVOobj.level2Code = obj.level2Code;
                ticketVOobj.level3Code = obj.level3Code;
                ticketVOobj.lineOfBusinessCode = obj.lineOfBusinessCode;
                ticketVOobj.conditionCode = obj.conditionCode;
                if(obj.contact != null && obj.contact.get(0) != null){
                    ticketVOobj.customerAddress  = obj.contact.get(0).street_vector+' '+ obj.contact.get(0).street_suffix+' '+obj.contact.get(0).street_name+' '+obj.contact.get(0).province+' '+obj.contact.get(0).country;
                }
                if(obj.contact != null && obj.contact.get(0) != null && obj.contact.get(0).telecommunicationsaddress != null && obj.contact.get(0).telecommunicationsaddress.get(0) != null){
                    ticketVOobj.ticketCustomerContact = obj.contact.get(0).fullName+' / '+formatphonenum(obj.contact.get(0).telecommunicationsaddress.get(0).telephoneNumber);
                }
                // mapping for activity of respective ticket
                if(obj.troubleTicketActivity != null){
                    for(smb_TTODS_troubleticket.TroubleTicketActivity ttact: obj.troubleTicketActivity){
                        TicketVO.TTActivity ttActicketVOobj = new TicketVO.TTActivity();
                        ttActicketVOobj.TELUSTroubleTicketId=ttact.TELUSTroubleTicketId;
                        ttActicketVOobj.activityTypeCode=ttact.activityTypeCode;
                        ttActicketVOobj.activityCategoryCode=ttact.activityCategoryCode;
                        ttActicketVOobj.TELUSActivityId=ttact.TELUSActivityId;
                        ttActicketVOobj.statusCode=ttact.statusCode;
                        ttActicketVOobj.createdBy=ttact.createdBy;
                        ttActicketVOobj.assignedTo=ttact.assignedTo;
                        ttActicketVOobj.resolutionCondition=ttact.resolutionCondition;
                        ttActicketVOobj.workforceRequired=ttact.workforceRequired;
                        ttActicketVOobj.workgroupTime=ttact.workgroupTime;
                        ttActicketVOobj.totalActivityTime=ttact.totalActivityTime;
                        ttActicketVOobj.CWCTimeType=ttact.CWCTimeType;
                        ttActicketVOobj.CWCTotalTimeHours=ttact.CWCTotalTimeHours;
                        ttActicketVOobj.CWCTotalTimeMinutes=ttact.CWCTotalTimeMinutes;
                        ttActicketVOobj.CWCEquipmentCost=ttact.CWCEquipmentCost;
                        ttActicketVOobj.assignToWorkgroup=ttact.assignToWorkgroup;
                        ttActicketVOobj.activityOwner=ttact.activityOwner;
                        ttActicketVOobj.activityComments=ttact.activityComments;
                        ttActicketVOobj.resolutionCode1=ttact.resolutionCode1;
                        ttActicketVOobj.resolutionCode2=ttact.resolutionCode2;
                        ttActicketVOobj.resolutionCode3=ttact.resolutionCode3;
                        ttActicketVOobj.notifyCustomerFlag=ttact.notifyCustomerFlag;
                        ttActicketVOobj.plannedStartDateTime=ttact.plannedStartDateTime;
                        ttActicketVOobj.plannedCompleteDateTime=ttact.plannedCompleteDateTime;
                        ttActicketVOobj.assignedDateTime=ttact.assignedDateTime;
                        ttActicketVOobj.actualCompleteDateTime=ttact.actualCompleteDateTime;
                        ttActicketVOobj.actualStartDateTime=ttact.actualStartDateTime;
                        ttActicketVOobj.closedDate=ttact.closedDate;
                        ttActicketVOobj.activityCreateDateTime=ttact.activityCreateDateTime;
                        ttActicketVOobj.externalTTSystemSequenceNumber=ttact.externalTTSystemSequenceNumber;
                        ttActicketVOobj.activityIdTypeCode=ttact.activityIdTypeCode;
                        ttActicketVOobj.categoryCode=ttact.categoryCode;
                        ttActicketVOobj.classCode=ttact.classCode;
                        ttActicketVOobj.completedFlag=ttact.completedFlag;
                        ttActicketVOobj.escalationTypeCode=ttact.escalationTypeCode;
                        ttActicketVOobj.overDueFlag=ttact.overDueFlag;
                        ttActicketVOobj.alarmFlag=ttact.alarmFlag;
                        ttActicketVOobj.modifiedDateTime=ttact.modifiedDateTime;
                        ttActicketVOobj.actvitiyLastUpdateTimeTTODS=ttact.actvitiyLastUpdateTimeTTODS;                                
                        ticketVOobj.ttActivity.add(ttActicketVOobj);
                    }  
                }
                reqResObj.lstTickets.add(ticketVOobj);
            }
        }
        system.debug('##### reqResObj.lstTickets' + reqResObj.lstTickets);
    }
    
    /* Method used to do mapping for data received from SRM system 
Invoke this method to map list of tickets
*/
    public static void doSRMMapping(list<SRM1_2_TQTroubleTicketTypes_v1.TroubleTicket> lstSrmTrobleTicket, TicketVO.ServiceRequestAndResponseParameter reqResObj){
        statusMapping = getStatusMapping();
        if(lstSrmTrobleTicket!=null && lstSrmTrobleTicket.size()>0){
            for(SRM1_2_TQTroubleTicketTypes_v1.TroubleTicket obj: lstSrmTrobleTicket){
                doSRMTicketMapping(obj,reqResObj);
            }
        }
    }
    
    /* Method used to do mapping for data received from SRM system 
Invoke this method for individual ticket       */
    
    public static void doSRMTicketMapping(SRM1_2_TQTroubleTicketTypes_v1.TroubleTicket obj, TicketVO.ServiceRequestAndResponseParameter reqResObj){
        TicketVO.TicketData ticketVOobj = new TicketVO.TicketData();
        ticketVOobj.sourceSystem = TicketVO.SRC_SRM;
        ticketVOobj.TELUSTicketId = obj.number_x;
        //ticketVOobj.ticketTypeCode = obj.typeCode;
        ticketVOobj.ticketTypeCode = System.Label.TT_TicketType;
        ticketVOobj.finalServiceCode = obj.finalServiceCode;
        ticketVOobj.agencyCode = obj.agencyCode;
        ticketVOobj.level1Code = obj.agencyCode;
        ticketVOobj.proximityCode = obj.proximityCode;
        ticketVOobj.serviceClassificationCode = obj.serviceClassificationCode;
        ticketVOobj.customerName  =  obj.customerName ;
        ticketVOobj.statusCode = obj.statusCode;
        
         // Sort Logic on Status
        if(statusMapping != null && statusMapping.get('DefaultSortPriority') != null && statusMapping.get('DefaultSortPriority').Sort_Priority__c != null){
             ticketVOobj.sortOrder = Integer.ValueOf(statusMapping.get('DefaultSortPriority').Sort_Priority__c);
        }
       
        if(obj.statusCode != null && obj.statusCode != ''){
            TT_Aggrgation_Status_Mapping__c statusObj = statusMapping.get(obj.statusCode);
            if(statusObj != null){
                ticketVOobj.statusCode = statusObj.value__c;
                if(statusObj.Sort_Priority__c != null){
                    ticketVOobj.sortOrder = Integer.ValueOf(statusObj.Sort_Priority__c);
                }
                // Sandip 06 June 2016 - production fix convert status as per locale
                if (UserInfo.getLocale().startsWith('fr')){
                    if(statusObj.French_Translation__c != null){
                        ticketVOobj.statusCode = statusObj.French_Translation__c;
                    }
                }
                // end
            }
        }
        
        //End
        
        ticketVOobj.priorityCode =  obj.priorityCode;
        ticketVOobj.callCardModeCode = obj.callCardModeCode;
        ticketVOobj.customerPhoneNumber = obj.customerPhoneNumber;
        if(obj.ownerTeamMember != null && obj.ownerTeamMember.name != null){
            ticketVOobj.assignToBIN = obj.ownerTeamMember.workGroupName;
            if(obj.ownerTeamMember.name.givenName == null || obj.ownerTeamMember.name.givenName =='null'){
                obj.ownerTeamMember.name.givenName = '';
            }
            if(obj.ownerTeamMember.name.familyName == null || obj.ownerTeamMember.name.familyName == 'null'){
                obj.ownerTeamMember.name.familyName = '';
            }
            ticketVOobj.assignToWorkgroupName = obj.ownerTeamMember.name.givenName + ' ' +   obj.ownerTeamMember.name.familyName;
        }
        if(obj.createdByTeamMember != null && obj.createdByTeamMember.name != null){
            ticketVOobj.createdByName = obj.createdByTeamMember.name.givenName + ' ' +   obj.createdByTeamMember.name.familyName;
        }

        
        if(obj.creationDateTime != null){
            try{
                System.debug('@@@ For Ticket' + obj.number_x);
                ticketVOobj.createDate = formatEstDate(obj.creationDateTime);
                ticketVOobj.createDateStr = getLocaleDate(ticketVOobj.createDate);
            }catch(Exception ex){
                system.debug('@@@## creationDateTime Date Format Exception..' + obj.creationDateTime + ' ' + ex);
            }
        }
        if(obj.closureDateTime != null){
            try{
                System.debug('@@@ Close Date Ticket' + obj.number_x + ' ' + obj.closureDateTime);
                ticketVOobj.closeDate = formatEstDate(obj.closureDateTime);
                ticketVOobj.closeDateStr = getLocaleDate(ticketVOobj.closeDate);
            }catch(Exception ex){
                system.debug('@@@## closureDateTime Date Format Exception..' + obj.closureDateTime + ' ' + ex);
            }
        }
        ticketVOobj.customerPhoneNumberExt = obj.customerPhoneNumberExt;
        ticketVOobj.npaNumber  = obj.npaNumber;
        if(obj.statusDateTime != null){
            try{
                ticketVOobj.modifyDateTime = formatEstDate(obj.statusDateTime);
            }catch(Exception ex){
                system.debug('@@@## statusDateTime Date Format Exception..' + obj.statusDateTime + ' ' + ex);
            }
        }
        
        if(obj.finalServiceCodeDescriptionList != null){
            if(obj.finalServiceCodeDescriptionList.description != null && obj.finalServiceCodeDescriptionList.description.size()>0){
                ticketVOobj.comments = obj.finalServiceCodeDescriptionList.description[0].descriptionText;
                ticketVOobj.description = obj.finalServiceCodeDescriptionList.description[0].descriptionText;
                ticketVOobj.toolTipComments = obj.finalServiceCodeDescriptionList.description[0].descriptionText;
            }else{
                ticketVOobj.comments = ticketVOobj.TELUSTicketId;
            }
        }else{
                ticketVOobj.comments = ticketVOobj.TELUSTicketId;
        }
       
        /*if(obj.descriptionList != null){
            if(obj.descriptionList.description != null && obj.descriptionList.description.size()>0){
                ticketVOobj.description = obj.descriptionList.description[0].descriptionText;
            }
        }*/
        ticketVOobj.customerUserId = obj.customerUserId;
        ticketVOobj.customerServiceIndicatorsText = obj.customerServiceIndicatorsText;
        ticketVOobj.customerTypeText = obj.customerTypeText;
        ticketVOobj.customerAccountNumber = obj.customerAccountNumber;
        ticketVOobj.customerBillingNumber = obj.customerBillingNumber;
        ticketVOobj.productGroupNumber = obj.productGroupNumber;
        ticketVOobj.serviceTypeCode = obj.serviceTypeCode;
        ticketVOobj.level5Code = obj.level5Code;
        ticketVOobj.dispatchLevelCode = obj.dispatchLevelCode;
        ticketVOobj.responsibleGroupCode = obj.responsibleGroupCode;
        ticketVOobj.circuitNumber  = obj.circuitNumber ;
        ticketVOobj.sourceTypeCode = obj.systemOriginId;
        ticketVOobj.accountTypeCode = obj.accountTypeCode;
        if(obj.customerPhoneNumberExt == null || obj.customerPhoneNumberExt == 'null' ){
            obj.customerPhoneNumberExt = '';
        }
        if(obj.customerPhoneNumber == null || obj.customerPhoneNumber == 'null' ){
            obj.customerPhoneNumber = '';
        }
        if(obj.customerPhoneNumber != ''){
            ticketVOobj.ticketCustomerContact = obj.customerName +  ' / ' +  obj.customerPhoneNumber +' ' +obj.customerPhoneNumberExt;
        }else{
           ticketVOobj.ticketCustomerContact = obj.customerName;
        }
        
        if(obj.customerAddress != null){
            if(obj.customerAddress.streetName == null || obj.customerAddress.streetName == 'null'){
                obj.customerAddress.streetName = '';
            }
            if(obj.customerAddress.cityName == null || obj.customerAddress.cityName == 'null'){
                obj.customerAddress.cityName = '';
            }
            if(obj.customerAddress.districtName == null || obj.customerAddress.districtName == 'null'){
                obj.customerAddress.districtName = '';
            }
            ticketVOobj.customerAddress  = obj.customerAddress.streetName+' '+ obj.customerAddress.cityName+' '+obj.customerAddress.districtName;
        }
        
        if(obj.commentList != null){
            for(SRM1_2_TQTroubleTicketTypes_v1.Comment ttact: obj.commentList){
                TicketVO.TTActivity ttActicketVOobj = new TicketVO.TTActivity();
                if(ttact.commentText != null){
                    if(ttact.commentText.startsWithIgnoreCase('% TRBLE') || ttact.commentText.startsWithIgnoreCase('% TRB:') 
                        || ttact.commentText.startsWithIgnoreCase('% TBL')){
                        String tmpComment = ttact.commentText.replace('% TRBLE:' , '');
                        tmpComment = tmpComment.replace('% TRB:' , ''); 
                        tmpComment = tmpComment.replace('% TBL:' , ''); 
                        tmpComment = tmpComment.replace('%' , ''); 
                        ticketVOobj.comments = tmpComment;
                        ttActicketVOobj.activityComments=tmpComment;
                    }else{
                        ttActicketVOobj.activityComments=ttact.commentText;
                    }
                }else{
                    ttact.commentText = '';
                }
                /*if(ttact.commentText != null && (ttact.commentText.Contains('% TRBLE') || ttact.commentText.Contains('% TBL'))){
                    ticketVOobj.comments = ttact.commentText;
                }*/
                ttActicketVOobj.TELUSTroubleTicketId = ticketVOobj.TELUSTicketId;
                //ttActicketVOobj.activityComments=ttact.commentText;
                if(ttact.commentDateTime != null){
                    try{
                        ttActicketVOobj.actvitiyLastUpdateTimeTTODS=formatEstDate(ttact.commentDateTime);
                        ttActicketVOobj.actvitiyLastUpdateTimeTTODSStr = getLocaleDate(ttActicketVOobj.actvitiyLastUpdateTimeTTODS);
                    }catch(Exception ex){
                        system.debug('@@@## commentDateTime Date Format Exception..' + ttact.commentDateTime + ' ' + ex);
                    }
                }
                ttActicketVOobj.assignToWorkgroup=ttact.userId;
                ttActicketVOobj.activityTypeCode=ttact.commentCode;
                ticketVOobj.ttActivity.add(ttActicketVOobj);
            }
        }
        if(obj.dispositionCodeList != null){
            for(SRM1_2_TQTroubleTicketTypes_v1.DispositionCode  ttact: obj.dispositionCodeList){
                TicketVO.TTActivity ttActicketVOobj = new TicketVO.TTActivity();
                ttActicketVOobj.TELUSTroubleTicketId = ticketVOobj.TELUSTicketId;
                //ttActicketVOobj.activityTypeCode=ttact.dispositionCode;
                ttActicketVOobj.resolutionCode1=ttact.dispositionCode;
                ttActicketVOobj.activityComments=ttact.dispositionCodeName;
                if(ttact.dispositionDateTime != null){
                    try{
                        ttActicketVOobj.actvitiyLastUpdateTimeTTODS = formatEstDate(ttact.dispositionDateTime);
                        ttActicketVOobj.actvitiyLastUpdateTimeTTODSStr = getLocaleDate(ttActicketVOobj.actvitiyLastUpdateTimeTTODS);
                    }catch(Exception ex){
                        system.debug('@@@## dispositionDateTime Date Format Exception..' + ttact.dispositionDateTime+ ' ' + ex);
                    }
                }
                ticketVOobj.ttActivity.add(ttActicketVOobj);
            }
        }
        if(ticketVOobj.ttActivity != null && ticketVOobj.ttActivity.size()>0){
             ticketVOobj.ttActivity.sort();
        }
            
        reqResObj.lstTickets.add(ticketVOobj);
        
    }
    
    /* Method used to do mapping for data received from Lavastorm */
    public static void doLavaStormMapping(TicketVO.ServiceRequestAndResponseParameter reqResObj){

        // Query to Lavastorm object
        Map<String,TicketVO.TicketData> lavaStormTTMap = New Map<String, TicketVO.TicketData>();
        List<LavaStorm_Trouble_Ticket__c> lavaStormTTList = New List<LavaStorm_Trouble_Ticket__c>();
        statusMapping = getStatusMapping();
        String queryString = 'SELECT Id, Trouble_Id__c, Customer_Id__c, Account__c, Created_Date__c, ' +
                               'Assigned_Group__c, Assigned_To__c,  BAN_Segment__c,' +
                               'BAN_Sub_Type__c, BAN_Type__c, Category1__c, Category2__c, Category3__c, City__c,' +
                               'Closed_Date__c, Contact_Name__c, Contact_Number__c, Country__c, Province__c, CreatedById, ' +
                               'CreatedDate, Submitter_Group__c, CurrencyIsoCode, IsDeleted, ' +
                               'Contact_Email__c, Equipment_Type_Code__c, LastActivityDate, LastModifiedById, ' +
                               'LastModifiedDate, Name, MDS_Support_Level__c, Mule_Model__c, Mule_Product_Name__c, ' +
                               'Mule_Vendor_Name__c, Priority__c, Problem_Description_Details__c, ConnectionReceivedId, ' +
                               'Resolution_Code_1__c, Resolution_Code_2__c, Resolution_Code_3__c, ' +
                               'Resolution_Details__c, Resolved_By__c, Resolved_By_Group__c, ConnectionSentId, Serial_Number__c, ' +
                               'Short_Description__c, Status__c, Status_Reason__c, Street1_Street2__c, Submitter_Name__c, ' +
                               'SystemModstamp, Technology__c, Time_of_Resolution__c, Trouble_Date__c, ' +
                               'Type__c, Work_Log_Create_Date__c, Work_Log_Details__c, Work_Log_Full_Name__c, ' +
                               'Work_Log_Summary__c, Work_Log_type__c, Mobile_Number__c ' +
                               'FROM LavaStorm_Trouble_Ticket__c ';
        String hyphenStr = '%-%';
        if(reqResObj.isCallForLavaStormIndividualTT==false){
            queryString = queryString + ' ' + 'WHERE Account__c = \'' + reqResObj.accountId +'\' AND (NOT Trouble_Id__c LIKE \'' + hyphenStr +'\')';
            queryString = queryString + ' ORDER BY LastModifiedDate Desc LIMIT ' + reqResObj.upToXLatestModified;
        }else{
            queryString = queryString + ' ' + 'WHERE Trouble_Id__c LIKE \'%' + reqResObj.ticketIdToSearch +'%\'';
        }
        
        //system.debug('@@@ Lava Query ' + queryString );
        
        lavaStormTTList = Database.query(queryString);
        
        if(lavaStormTTList != null && lavaStormTTList.size()>0){
            for(LavaStorm_Trouble_Ticket__c lavaObj:lavaStormTTList){
                if(lavaObj.Trouble_Id__c != null && lavaObj.Trouble_Id__c.Contains('-')){
                    Integer index = lavaObj.Trouble_Id__c.indexOf('-');
                    String ticketId = lavaObj.Trouble_Id__c.substring(0,index);
                    if(lavaStormTTMap.containsKey(ticketId)){
                        TicketVO.TicketData ticketObj = lavaStormTTMap.get(ticketId);
                        TicketVO.LavaStormWorkLogDetails workLogObj = new TicketVO.LavaStormWorkLogDetails();
                        workLogObj.workLogId = lavaObj.Trouble_Id__c.subString(index+1);
                        system.debug('# work log Id' + workLogObj.workLogId);
                        workLogObj.WorkLogCreateDate = lavaObj.Work_Log_Create_Date__c;
                        if(lavaObj.Work_Log_Details__c != null){
                             workLogObj.WorkLogDetails = lavaObj.Work_Log_Details__c.replace('\r\n', '<br>');
                        }
                        workLogObj.WorkLogFullName = lavaObj.Work_Log_Full_Name__c;
                        workLogObj.WorkLogSummary = lavaObj.Work_Log_Summary__c;
                        workLogObj.WorkLogtype = lavaObj.Work_Log_type__c;
        
                        ticketObj.workLogList.add(workLogObj);
                        lavaStormTTMap.put(ticketId, ticketObj);
                    }
                }else{
                    TicketVO.TicketData ticketDataObj2 = doLavaStormTicketMapping(lavaObj);
                    lavaStormTTMap.put(ticketDataObj2.TELUSTicketId, ticketDataObj2);
                    //reqResObj.lstTickets.add(ticketDataObj2);
                }
            }
            if(lavaStormTTMap.values() != null && lavaStormTTMap.values().size()>0){
                for(TicketVO.TicketData lavaObj: lavaStormTTMap.values()){
                    lavaObj.workLogList.sort();
                    reqResObj.lstTickets.add(lavaObj);
                }
            }
        }
    }
    
    public static TicketVO.TicketData doLavaStormTicketMapping(LavaStorm_Trouble_Ticket__c lavaObj){
        TicketVO.TicketData ticketDataObj = new TicketVO.TicketData();
        ticketDataObj.sourceSystem = TicketVO.SRC_LavaStorm;
        ticketDataObj.TELUSTicketId = lavaObj.Trouble_Id__c;
        if(lavaObj.Short_Description__c != null && lavaObj.Short_Description__c != ''){
            ticketDataObj.comments = lavaObj.Short_Description__c;
            ticketDataObj.toolTipComments = lavaObj.Short_Description__c;
        }else{
            ticketDataObj.comments = lavaObj.Trouble_Id__c;
        }
        try{
            ticketDataObj.createDate = lavaObj.Created_Date__c;
            if(lavaObj.Created_Date__c != null)
                ticketDataObj.createDateStr = getLocaleDate(ticketDataObj.createDate);
            
            ticketDataObj.closeDate = lavaObj.Closed_Date__c;
            if(lavaObj.Closed_Date__c != null)
                ticketDataObj.closeDateStr = getLocaleDate(ticketDataObj.closeDate);
           /* if(ticketDataObj.closeDateStr == 'Jan 1, 1970'){
                ticketDataObj.closeDateStr = '';
            }*/
        }catch (Exception ex){ system.debug('@@@## Date Format Exception..' + ex); }
        ticketDataObj.ticketTypeCode = lavaObj.Type__c;
        //ticketDataObj.ticketTypeCode = System.Label.TT_TicketType;
        ticketDataObj.priorityCode = lavaObj.Priority__c;
        if(lavaObj.Problem_Description_Details__c != null){
            ticketDataObj.description = lavaObj.Problem_Description_Details__c.replace('\r\n', '<br>');
        }
        //ticketDataObj.description = lavaObj.Problem_Description_Details__c;
        ticketDataObj.level1Code = lavaObj.Category1__c;
        ticketDataObj.level2Code = lavaObj.Category2__c;
        ticketDataObj.level3Code = lavaObj.Category3__c;
        
        ticketDataObj.assignToWorkgroupName = lavaObj.Assigned_Group__c;
        ticketDataObj.createdByName = lavaObj.Submitter_Name__c;
        ticketDataObj.statusCode = lavaObj.Status__c;
        // Sort Logic on Status
        if(statusMapping != null && statusMapping.get('DefaultSortPriority') != null && statusMapping.get('DefaultSortPriority').Sort_Priority__c != null){
             ticketDataObj.sortOrder = Integer.ValueOf(statusMapping.get('DefaultSortPriority').Sort_Priority__c);
        }
       
        if(lavaObj.Status__c != null && lavaObj.Status__c != ''){
            TT_Aggrgation_Status_Mapping__c statusObj = statusMapping.get(lavaObj.Status__c);
            if(statusObj != null){
                ticketDataObj.statusCode = statusObj.value__c;
                if(statusObj.Sort_Priority__c != null){
                    ticketDataObj.sortOrder = Integer.ValueOf(statusObj.Sort_Priority__c);
                }
                // Sandip 06 June 2016 - production fix convert status as per locale
                if (UserInfo.getLocale().startsWith('fr')){
                    if(statusObj.French_Translation__c != null){
                        ticketDataObj.statusCode = statusObj.French_Translation__c;
                    }
                }
                // end
            }
        }
        //End
        
        if(lavaObj.Contact_Name__c == null || lavaObj.Contact_Name__c =='null'){
            lavaObj.Contact_Name__c = '';
            ticketDataObj.contactName = lavaObj.Contact_Name__c;
        }
        if(lavaObj.Contact_Number__c == null || lavaObj.Contact_Number__c =='null'){
            lavaObj.Contact_Number__c = '';
            ticketDataObj.contactNumber = lavaObj.Contact_Number__c;
        }
        if(lavaObj.Contact_Number__c != '' && lavaObj.Contact_Name__c != ''){
            ticketDataObj.ticketCustomerContact = lavaObj.Contact_Name__c + '/' + lavaObj.Contact_Number__c;
        }else if(lavaObj.Contact_Number__c != ''){
            ticketDataObj.ticketCustomerContact = lavaObj.Contact_Number__c;
        }else {
            ticketDataObj.ticketCustomerContact = lavaObj.Contact_Name__c;
        }
        if(lavaObj.Street1_Street2__c == null || lavaObj.Street1_Street2__c == 'null'){
            lavaObj.Street1_Street2__c = '';
        }
        if(lavaObj.City__c == null || lavaObj.City__c == 'null'){
            lavaObj.City__c = '';
        }
        if(lavaObj.Province__c == null || lavaObj.Province__c == 'null'){
            lavaObj.Province__c = '';
        }
        if(lavaObj.Country__c == null || lavaObj.Country__c == 'null'){
            lavaObj.Country__c = '';
        }
        ticketDataObj.customerAddress = lavaObj.Street1_Street2__c + ' ' + lavaObj.City__c + ' ' + lavaObj.Province__c + ' ' + lavaObj.Country__c;
        
        ticketDataObj.mobileNumber = lavaObj.Mobile_Number__c;
        
        ticketDataObj.telusNumber = lavaObj.Mobile_Number__c;
        ticketDataObj.serialNumber = lavaObj.Serial_Number__c;
        ticketDataObj.contactName = lavaObj.Contact_Name__c;
        ticketDataObj.emailAddress = lavaObj.Contact_Email__c;
        ticketDataObj.contactNumber = lavaObj.Contact_Number__c;
        ticketDataObj.street = lavaObj.Street1_Street2__c;
        ticketDataObj.city = lavaObj.City__c;
        ticketDataObj.province = lavaObj.Province__c;
        ticketDataObj.country = lavaObj.Country__c;
        ticketDataObj.technology = lavaObj.Technology__c;
        ticketDataObj.banType = lavaObj.BAN_Type__c;
        ticketDataObj.banSubType = lavaObj.BAN_Sub_Type__c;
        ticketDataObj.banSegment = lavaObj.BAN_Segment__c;
        ticketDataObj.mdsSupportLevel = lavaObj.MDS_Support_Level__c;
        ticketDataObj.submitterGroup = lavaObj.Submitter_Group__c;
        ticketDataObj.statusReason = lavaObj.Status_Reason__c;
        ticketDataObj.equipmentTypeCode = lavaObj.Equipment_Type_Code__c;
        ticketDataObj.muleVendorName = lavaObj.Mule_Vendor_Name__c;
        ticketDataObj.muleModel = lavaObj.Mule_Model__c;
        ticketDataObj.muleProductName = lavaObj.Mule_Product_Name__c;
             
        ticketDataObj.resolutionCode1 = lavaObj.Resolution_Code_1__c;
        ticketDataObj.resolutionCode2 = lavaObj.Resolution_Code_2__c;
        ticketDataObj.resolutionCode3 = lavaObj.Resolution_Code_3__c;
        ticketDataObj.resolvedGroup = lavaObj.Resolved_By_Group__c;
        ticketDataObj.resolvedBy = lavaObj.Resolved_By__c;
        ticketDataObj.timeOfResoultion = lavaObj.Time_Of_Resolution__c;
        ticketDataObj.resoultionDetails = lavaObj.Resolution_Details__c;
        /*ticketDataObj.WorkLogCreateDate = lavaObj.Work_Log_Create_Date__c;
        if(lavaObj.Work_Log_Details__c != null){
            ticketDataObj.WorkLogDetails = lavaObj.Work_Log_Details__c;
            ticketDataObj.WorkLogDetails = ticketDataObj.WorkLogDetails.replace('\n', '<br></br>');
        }
        ticketDataObj.WorkLogFullName = lavaObj.Work_Log_Full_Name__c;
        ticketDataObj.WorkLogSummary = lavaObj.Work_Log_Summary__c;
        ticketDataObj.WorkLogtype = lavaObj.Work_Log_type__c;
        */
        return ticketDataObj;
    }
    
    //Formating for contact number for TTODS
    private static String formatphonenum (String s) {
        s = '(' + s.substring(0, 3) + ') ' + s.substring(3, 6) + '-' + s.substring(6);
        return s;
    }
    
    private static DateTime formatEstDate (String dt) {
        system.debug('### Original Date' + dt);
        if(dt != null && dt != 'null' && dt.trim() != ''){
            if(dt.length()== 15){
                system.debug('### Start Format');
                String year = dt.subString(0,4);
                String month = dt.subString(4,6);
                String day = dt.subString(6,8);
                String hh = dt.subString(9,11);
                String mm = dt.subString(11,13);
                String ss = dt.subString(13);
                string stringDate = year + '-' + month + '-' + day + ' ' + hh + ':' + mm + ':' + ss;
                system.debug('### SstringDate' + stringDate);
                DateTime dtTime = datetime.valueOf(stringDate);
                /*String locale_formatted_date_time_value = dtTime.format('MMM dd yyyy HH:MM:ss');
                System.debug('@@ Test Locale' + locale_formatted_date_time_value);
                DateTime convertedDt = datetime.valueOf(locale_formatted_date_time_value);*/
                return dtTime;
            }else{
                return datetime.valueOf(dt);
            }
        }else{
            return datetime.valueOf(dt);
        }
        return null;
    }
    private static Map<String, TT_Aggrgation_Status_Mapping__c> getStatusMapping(){
        return TT_Aggrgation_Status_Mapping__c.getAll();
    }
    private static String getLocaleDate(DateTime dtParam){
        String localeDt = '';
        if(dtParam != null){
            // TT Aggegation Defect fix
            if (UserInfo.getLocale().startsWith('fr')) {
                string mon = smb_DateUtility.getMonthName(dtParam.month());
                localeDt = dtParam.day() + ' ' + mon + ', ' + dtParam.year();
            }else{
                localeDt  = dtParam.format('MMM dd, yyyy');
            }
            //End
        }
        return localeDt;
    }
}