/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CMS3_1_test_Types_v3_WSDL {
static testMethod void cms3_1ContractManagementSvcTypes_v3(){
        SMB_Dummy1WebServiceResponse_Test.intializeMockWebservice();
       cms3_1ContractManagementSvcTypes_v3 smb22 = new cms3_1ContractManagementSvcTypes_v3();
       cms3_1ContractManagementSvcTypes_v3.AssignedProductIdList b1 = new cms3_1ContractManagementSvcTypes_v3.AssignedProductIdList();
       cms3_1ContractManagementSvcTypes_v3.BillingTelephoneNumList b2 = new cms3_1ContractManagementSvcTypes_v3.BillingTelephoneNumList();
       cms3_1ContractManagementSvcTypes_v3.Contract b3 = new cms3_1ContractManagementSvcTypes_v3.Contract();
       cms3_1ContractManagementSvcTypes_v3.ContractAmendment b4 = new cms3_1ContractManagementSvcTypes_v3.ContractAmendment();
       cms3_1ContractManagementSvcTypes_v3.ContractAmendmentList b5 = new cms3_1ContractManagementSvcTypes_v3.ContractAmendmentList();
       cms3_1ContractManagementSvcTypes_v3.ContractAttribute b6 = new cms3_1ContractManagementSvcTypes_v3.ContractAttribute();
       cms3_1ContractManagementSvcTypes_v3.ContractAttributeList b7 = new cms3_1ContractManagementSvcTypes_v3.ContractAttributeList();
       cms3_1ContractManagementSvcTypes_v3.ContractDetail b8 = new cms3_1ContractManagementSvcTypes_v3.ContractDetail();
       cms3_1ContractManagementSvcTypes_v3.ContractDetailList b9 = new cms3_1ContractManagementSvcTypes_v3.ContractDetailList();
       cms3_1ContractManagementSvcTypes_v3.ContractDocument b10 = new cms3_1ContractManagementSvcTypes_v3.ContractDocument();
       cms3_1ContractManagementSvcTypes_v3.ContractDocumentList b11 = new cms3_1ContractManagementSvcTypes_v3.ContractDocumentList();
       cms3_1ContractManagementSvcTypes_v3.ContractList b12 = new cms3_1ContractManagementSvcTypes_v3.ContractList();
       cms3_1ContractManagementSvcTypes_v3.ContractRequest b13 = new cms3_1ContractManagementSvcTypes_v3.ContractRequest();
       cms3_1ContractManagementSvcTypes_v3.ContractRequestDetail b14 = new cms3_1ContractManagementSvcTypes_v3.ContractRequestDetail();
       cms3_1ContractManagementSvcTypes_v3.ContractRequestDetailList b15 = new cms3_1ContractManagementSvcTypes_v3.ContractRequestDetailList();
       cms3_1ContractManagementSvcTypes_v3.CustomerSignor customerSignor= new cms3_1ContractManagementSvcTypes_v3.CustomerSignor();
       
       cms3_1ContractManagementSvcTypes_v3.LegacyContractIdList b17 = new cms3_1ContractManagementSvcTypes_v3.LegacyContractIdList();
       cms3_1ContractManagementSvcTypes_v3.OldAssignedProductIdList b18 = new cms3_1ContractManagementSvcTypes_v3.OldAssignedProductIdList();
       cms3_1ContractManagementSvcTypes_v3.ServiceAddress b19 = new cms3_1ContractManagementSvcTypes_v3.ServiceAddress();
       cms3_1ContractManagementSvcTypes_v3.ServiceAddressList b20 = new cms3_1ContractManagementSvcTypes_v3.ServiceAddressList();
       cms3_1ContractManagementSvcTypes_v3.ServiceDetail b21 = new cms3_1ContractManagementSvcTypes_v3.ServiceDetail();        
       cms3_1ContractManagementSvcTypes_v3.ServiceDetailList b22 = new cms3_1ContractManagementSvcTypes_v3.ServiceDetailList();
        
    }
}