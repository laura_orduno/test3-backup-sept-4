/*****************************************************
    Name: KafkaWorkOrderSubscriberTest
    Usage: This class is used to test coverage for following classes 
			1.)	KafkaWorkOrderSubscriber
			2.) KafkaWorkOrderHandlerEventImpl (process event for KafkaWorkOrderSubscriber)
    Author – Umesh Atry   (TechM)
    Revision History -
    Created By/Date - Umesh Atry P / 15 Mar 2017
					  Umesh Atry R/21 Mar 2017
    
    Updated By/Date/Reason for Change - also added KafkaWorkOrderHandlerEventImpl class on 21 march 2017 by Umesh Atry
    
******************************************************/



@isTest
public class KafkaWorkOrderSubscriberTest {
    
    
    static String message = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><ns2:workOrderUpdateNotification xmlns:ns2="http://xmlschema.tmi.telus.com/xsd/Resource/Resource/WorkForceManagementOrderTypes_v3"><eventList><event>JOB_ASSIGNMENT_UPDATE_EVENT</event></eventList><systemSourceCd>8391</systemSourceCd><userId>svc_rvadmin</userId><requestDate>2017-03-08T21:43:10</requestDate><workOrder><workOrderId>100000322809</workOrderId><originatingSystemId>6161</originatingSystemId><originatingSystemWorkOrderId>A492300000059WRAAY</originatingSystemWorkOrderId><originatingSystemWorkOrderInternalId>A492300000059WRAAY</originatingSystemWorkOrderInternalId><callId>100000319922</callId><classificationCd>ORDER</classificationCd><serviceClassCd>B</serviceClassCd><productCategoryCd>DSL</productCategoryCd><productTechnologyCd>COPPER</productTechnologyCd><workOrderActionCd>I</workOrderActionCd><jobTypeCd>HS</jobTypeCd><outofServiceInd>false</outofServiceInd><priorityCd>3</priorityCd><hostReadyForDispatchInd>false</hostReadyForDispatchInd><overrideInd>false</overrideInd><scheduleRemarkInd>false</scheduleRemarkInd><appointmentStartDate>2017-03-09T16:00:00.000Z</appointmentStartDate><appointmentEndDate>2017-03-09T20:00:00.000Z</appointmentEndDate><dueDate>2017-03-09T20:00:00.000Z</dueDate><estimatedDurationNum>1.5</estimatedDurationNum><estimatedDurationUnitCd>HOURS</estimatedDurationUnitCd><location><locationList><locationId>3899833</locationId><typeCode>SERVICE</typeCode><dispatchLocationInd>true</dispatchLocationInd><locationAddress><locationId>3899833</locationId><civicAddress><civicNumber>1215</civicNumber><streetName>FALCON</streetName><streetNamePostTypeCode>DRIVE</streetNamePostTypeCode><municipalityCode>COQUITLAM</municipalityCode><provinceStateCode>BC</provinceStateCode><countryName>CANADA</countryName><postalCode>V3E1X9</postalCode></civicAddress><geocodeAddress><matchCode>High</matchCode><latitudeTxt>49.285221</latitudeTxt><longitudeTxt>-122.817809</longitudeTxt></geocodeAddress><regionHierarchy><provinceCode>BC</provinceCode><provinceName>British Columbia</provinceName><provinceLocationId>100001</provinceLocationId><regionName>Lower Mainland</regionName><regionLocationId>100024</regionLocationId><districtName>Westwood</districtName><districtLocationId>100229</districtLocationId><serviceAreaName>Westwood</serviceAreaName><serviceAreaClliCd>PTCMBC02</serviceAreaClliCd><serviceAreaLocationId>100686</serviceAreaLocationId></regionHierarchy></locationAddress></locationList><timezoneCd>America/Vancouver</timezoneCd><regionHierarchy><provinceCode>BC</provinceCode><provinceName>British Columbia</provinceName><regionName>FFB BC</regionName><districtName>FFB - LML Metro</districtName><serviceAreaName>Westwood (PTCMBC02)</serviceAreaName><serviceAreaClliCd>PTCMBC02</serviceAreaClliCd><serviceAreaLocationId>100686</serviceAreaLocationId></regionHierarchy></location><customerName>RCIDNAME 0004228184</customerName><customerRatingCd>S1</customerRatingCd><customerTypeCd>CUSTOMER</customerTypeCd><contactList><name>Steph Curry</name><typeCd>CBR</typeCd><phoneNumber>(780) 444-5555</phoneNumber><phoneLocationCd>AUT</phoneLocationCd></contactList><originatingSystemCreateDate>2017-03-08T18:05:17.000Z</originatingSystemCreateDate><workOrderAttributeList><typeCode><typeCd>NOTIFICATION_IND</typeCd><descriptionTxt>false</descriptionTxt></typeCode></workOrderAttributeList><workOrderDetailList/><remarkList/><internalTeamWorkerRequiredInd>false</internalTeamWorkerRequiredInd><requiredTeamWorkerList/><prohibitedTeamWorkerList/><preferredTeamWorkerList/><statusCd>OPEN</statusCd><requiredTeamWorkerNum>1</requiredTeamWorkerNum><customerPremiseEquipmentList/><componentList/></workOrder><job><jobIdent><jobId>333373</jobId></jobIdent><statusCd>TENTATIVE</statusCd><assignment><teamWorkerId>T062037</teamWorkerId><statusCd>TENTATIVE</statusCd><startDate>2017-03-09T10:40:00.000-08:00</startDate><endDate>2017-03-09T12:10:00.000-08:00</endDate><execution><executionDate>2017-03-08T21:43:10</executionDate></execution></assignment></job></ns2:workOrderUpdateNotification>';
    static String messagenotwfm = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><ns2:workOrderUpdateNotification xmlns:ns2="http://xmlschema.tmi.telus.com/xsd/Resource/Resource/WorkForceManagementOrderTypes_v3"><eventList><event>JOB_ASSIGNMENT_UPDATE_EVENT</event></eventList><systemSourceCd>8391</systemSourceCd><userId>svc_rvadmin</userId><requestDate>2017-03-08T21:43:10</requestDate><workOrder><workOrderId></workOrderId><originatingSystemId>6161</originatingSystemId><originatingSystemWorkOrderId>A492300000059WRAAY</originatingSystemWorkOrderId><originatingSystemWorkOrderInternalId>A492300000059WRAAY</originatingSystemWorkOrderInternalId><callId>100000319922</callId><classificationCd>ORDER</classificationCd><serviceClassCd>B</serviceClassCd><productCategoryCd>DSL</productCategoryCd><productTechnologyCd>COPPER</productTechnologyCd><workOrderActionCd>I</workOrderActionCd><jobTypeCd>HS</jobTypeCd><outofServiceInd>false</outofServiceInd><priorityCd>3</priorityCd><hostReadyForDispatchInd>false</hostReadyForDispatchInd><overrideInd>false</overrideInd><scheduleRemarkInd>false</scheduleRemarkInd><appointmentStartDate>2017-03-09T16:00:00.000Z</appointmentStartDate><appointmentEndDate>2017-03-09T20:00:00.000Z</appointmentEndDate><dueDate>2017-03-09T20:00:00.000Z</dueDate><estimatedDurationNum>1.5</estimatedDurationNum><estimatedDurationUnitCd>HOURS</estimatedDurationUnitCd><location><locationList><locationId>3899833</locationId><typeCode>SERVICE</typeCode><dispatchLocationInd>true</dispatchLocationInd><locationAddress><locationId>3899833</locationId><civicAddress><civicNumber>1215</civicNumber><streetName>FALCON</streetName><streetNamePostTypeCode>DRIVE</streetNamePostTypeCode><municipalityCode>COQUITLAM</municipalityCode><provinceStateCode>BC</provinceStateCode><countryName>CANADA</countryName><postalCode>V3E1X9</postalCode></civicAddress><geocodeAddress><matchCode>High</matchCode><latitudeTxt>49.285221</latitudeTxt><longitudeTxt>-122.817809</longitudeTxt></geocodeAddress><regionHierarchy><provinceCode>BC</provinceCode><provinceName>British Columbia</provinceName><provinceLocationId>100001</provinceLocationId><regionName>Lower Mainland</regionName><regionLocationId>100024</regionLocationId><districtName>Westwood</districtName><districtLocationId>100229</districtLocationId><serviceAreaName>Westwood</serviceAreaName><serviceAreaClliCd>PTCMBC02</serviceAreaClliCd><serviceAreaLocationId>100686</serviceAreaLocationId></regionHierarchy></locationAddress></locationList><timezoneCd>America/Vancouver</timezoneCd><regionHierarchy><provinceCode>BC</provinceCode><provinceName>British Columbia</provinceName><regionName>FFB BC</regionName><districtName>FFB - LML Metro</districtName><serviceAreaName>Westwood (PTCMBC02)</serviceAreaName><serviceAreaClliCd>PTCMBC02</serviceAreaClliCd><serviceAreaLocationId>100686</serviceAreaLocationId></regionHierarchy></location><customerName>RCIDNAME 0004228184</customerName><customerRatingCd>S1</customerRatingCd><customerTypeCd>CUSTOMER</customerTypeCd><contactList><name>Steph Curry</name><typeCd>CBR</typeCd><phoneNumber>(780) 444-5555</phoneNumber><phoneLocationCd>AUT</phoneLocationCd></contactList><originatingSystemCreateDate>2017-03-08T18:05:17.000Z</originatingSystemCreateDate><workOrderAttributeList><typeCode><typeCd>NOTIFICATION_IND</typeCd><descriptionTxt>false</descriptionTxt></typeCode></workOrderAttributeList><workOrderDetailList/><remarkList/><internalTeamWorkerRequiredInd>false</internalTeamWorkerRequiredInd><requiredTeamWorkerList/><prohibitedTeamWorkerList/><preferredTeamWorkerList/><statusCd>OPEN</statusCd><requiredTeamWorkerNum>1</requiredTeamWorkerNum><customerPremiseEquipmentList/><componentList/></workOrder><job><jobIdent><jobId>333373</jobId></jobIdent><statusCd></statusCd><assignment><teamWorkerId>T062037</teamWorkerId><statusCd>TENTATIVE</statusCd><startDate>2017-03-09T10:40:00.000-08:00</startDate><endDate>2017-03-09T12:10:00.000-08:00</endDate><execution><executionDate>2017-03-08T21:43:10</executionDate></execution></assignment></job></ns2:workOrderUpdateNotification>';       
	static String messageNowfmNoStatus = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><ns2:workOrderUpdateNotification xmlns:ns2="http://xmlschema.tmi.telus.com/xsd/Resource/Resource/WorkForceManagementOrderTypes_v3"><eventList><event>JOB_ASSIGNMENT_UPDATE_EVENT</event></eventList><systemSourceCd>8391</systemSourceCd><userId>svc_rvadmin</userId><requestDate>2017-03-08T21:43:10</requestDate><workOrder><originatingSystemId>6161</originatingSystemId><originatingSystemWorkOrderId></originatingSystemWorkOrderId><originatingSystemWorkOrderInternalId>A492300000059WRAAY</originatingSystemWorkOrderInternalId><callId>100000319922</callId><classificationCd>ORDER</classificationCd><serviceClassCd>B</serviceClassCd><productCategoryCd>DSL</productCategoryCd><productTechnologyCd>COPPER</productTechnologyCd><workOrderActionCd>I</workOrderActionCd><jobTypeCd>HS</jobTypeCd><outofServiceInd>false</outofServiceInd><priorityCd>3</priorityCd><hostReadyForDispatchInd>false</hostReadyForDispatchInd><overrideInd>false</overrideInd><scheduleRemarkInd>false</scheduleRemarkInd><appointmentStartDate>2017-03-09T16:00:00.000Z</appointmentStartDate><appointmentEndDate>2017-03-09T20:00:00.000Z</appointmentEndDate><dueDate>2017-03-09T20:00:00.000Z</dueDate><estimatedDurationNum>1.5</estimatedDurationNum><estimatedDurationUnitCd>HOURS</estimatedDurationUnitCd><location><locationList><locationId>3899833</locationId><typeCode>SERVICE</typeCode><dispatchLocationInd>true</dispatchLocationInd><locationAddress><locationId>3899833</locationId><civicAddress><civicNumber>1215</civicNumber><streetName>FALCON</streetName><streetNamePostTypeCode>DRIVE</streetNamePostTypeCode><municipalityCode>COQUITLAM</municipalityCode><provinceStateCode>BC</provinceStateCode><countryName>CANADA</countryName><postalCode>V3E1X9</postalCode></civicAddress><geocodeAddress><matchCode>High</matchCode><latitudeTxt>49.285221</latitudeTxt><longitudeTxt>-122.817809</longitudeTxt></geocodeAddress><regionHierarchy><provinceCode>BC</provinceCode><provinceName>British Columbia</provinceName><provinceLocationId>100001</provinceLocationId><regionName>Lower Mainland</regionName><regionLocationId>100024</regionLocationId><districtName>Westwood</districtName><districtLocationId>100229</districtLocationId><serviceAreaName>Westwood</serviceAreaName><serviceAreaClliCd>PTCMBC02</serviceAreaClliCd><serviceAreaLocationId>100686</serviceAreaLocationId></regionHierarchy></locationAddress></locationList><timezoneCd>America/Vancouver</timezoneCd><regionHierarchy><provinceCode>BC</provinceCode><provinceName>British Columbia</provinceName><regionName>FFB BC</regionName><districtName>FFB - LML Metro</districtName><serviceAreaName>Westwood (PTCMBC02)</serviceAreaName><serviceAreaClliCd>PTCMBC02</serviceAreaClliCd><serviceAreaLocationId>100686</serviceAreaLocationId></regionHierarchy></location><customerName>RCIDNAME 0004228184</customerName><customerRatingCd>S1</customerRatingCd><customerTypeCd>CUSTOMER</customerTypeCd><contactList><name>Steph Curry</name><typeCd>CBR</typeCd><phoneNumber>(780) 444-5555</phoneNumber><phoneLocationCd>AUT</phoneLocationCd></contactList><originatingSystemCreateDate>2017-03-08T18:05:17.000Z</originatingSystemCreateDate><workOrderAttributeList><typeCode><typeCd>NOTIFICATION_IND</typeCd><descriptionTxt>false</descriptionTxt></typeCode></workOrderAttributeList><workOrderDetailList/><remarkList/><internalTeamWorkerRequiredInd>false</internalTeamWorkerRequiredInd><requiredTeamWorkerList/><prohibitedTeamWorkerList/><preferredTeamWorkerList/><requiredTeamWorkerNum>1</requiredTeamWorkerNum><customerPremiseEquipmentList/><componentList/></workOrder><job><jobIdent><jobId>333373</jobId></jobIdent><assignment><teamWorkerId>T062037</teamWorkerId><startDate>2017-03-09T10:40:00.000-08:00</startDate><endDate>2017-03-09T12:10:00.000-08:00</endDate><execution><executionDate>2017-03-08T21:43:10</executionDate></execution></assignment></job></ns2:workOrderUpdateNotification>';
	
    // Test that the XML string contains specific values
    static testMethod void testKafkaMessageProcessor() {
       // KafkaWorkOrderSubscriber.processMessage(message);
    }
	public static void createEEConfig(String eventName, String handlerName, Boolean isAsync){
        External_Event_Config__c obj1 = new External_Event_Config__c (Name = eventName, Event_Handler_Name__c = handlerName , Is_Asynchronous_Execution__c = isAsync);
        Database.insert(obj1 , false);
    }
    
    static testmethod void testForCoverage_XmlNotFound(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();        
        req.requestURI = 'https://coins-telus.cs15.force.com/subscribers/services/apexrest/KafkaWorkOrderSubscriber'; 
        //req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        req.addHeader('Token', 'mLVMq7sT1xqVQwWRZr6ycJ2ofL0kMzV78QNOD4b5l2EZU0R5AvQD4CLR53Jhhu0k');        
       	req.requestBody = Blob.valueof('');
       
        KafkaWorkOrderSubscriber.doGet();        
		createEEConfig('wfm.workorder', 'KafkaWorkOrderHandlerEventImpl', false);
         
        KafkaWorkOrderSubscriber.doPost();         
    }
	// check code if xml is not assigend
	 static testmethod void testForCoverage_JobsNotFound(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();        
        req.requestURI = 'https://coins-telus.cs15.force.com/subscribers/services/apexrest/KafkaWorkOrderSubscriber'; 
        //req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        req.addHeader('Token', 'mLVMq7sT1xqVQwWRZr6ycJ2ofL0kMzV78QNOD4b5l2EZU0R5AvQD4CLR53Jhhu0k');        
       	req.requestBody = Blob.valueof(messageNowfmNoStatus);
       
        KafkaWorkOrderSubscriber.doGet();        
		createEEConfig('wfm.workorder', 'KafkaWorkOrderHandlerEventImpl', false);
         
        KafkaWorkOrderSubscriber.doPost();        
        List<Work_order__c > upsertedwfm = [select id,name,WFM_Number__c from work_order__c ];
        
    }
	// check code if work order is not made
    static testmethod void testForCoverage_withoutWFM(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();        
        req.requestURI = 'https://coins-telus.cs15.force.com/subscribers/services/apexrest/KafkaWorkOrderSubscriber'; 
        //req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        req.addHeader('Token', 'mLVMq7sT1xqVQwWRZr6ycJ2ofL0kMzV78QNOD4b5l2EZU0R5AvQD4CLR53Jhhu0k');        
      	req.requestBody =  Blob.valueof(message);
        
        KafkaWorkOrderSubscriber.doGet();
        
		createEEConfig('wfm.workorder', 'KafkaWorkOrderHandlerEventImpl', false);
        KafkaWorkOrderSubscriber.doPost();  
        
        List<Work_order__c > upsertedwfm = [select id,name,WFM_Number__c from work_order__c ];
        
    }
	// check code if work order is already created.
    static testmethod void testForCoverage_withWFM(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();        
        req.requestURI = 'https://coins-telus.cs15.force.com/subscribers/services/apexrest/KafkaWorkOrderSubscriber'; 
        //req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        req.addHeader('Token', 'mLVMq7sT1xqVQwWRZr6ycJ2ofL0kMzV78QNOD4b5l2EZU0R5AvQD4CLR53Jhhu0k');        
      	req.requestBody =  Blob.valueof(message);
        
        KafkaWorkOrderSubscriber.doGet();
        
        Work_Order__c wkOrder_1 = new Work_Order__c(Scheduled_Due_Date_Location__c='2016-03-11 08:00:00', status__c = Label.OCOM_WFMReserved,
                                                        Install_Type__c ='FieldWork', Onsite_Contact_HomePhone__c = '647.981.9999', 
                                                        Onsite_Contact_Name__c = 'Test is a test', Onsite_Contact_Email__c = 'test@testcompany.ca', 
                                                        Onsite_Contact_CellPhone__c = '416.999.8888', Scheduled_Datetime__c=System.now().AddDays(1),
                                                        WFM_Number__c='100000322809', Dispatch_Status__c = 'OPEN', Time_Slot__c='0800-1200');
		insert wkOrder_1;        
        
		createEEConfig('wfm.workorder', 'KafkaWorkOrderHandlerEventImpl', false);
        
        
        KafkaWorkOrderSubscriber.doPost();        
        List<Work_order__c > upsertedwfm = [select id,name,WFM_Number__c from work_order__c ];
        
    }

    
}