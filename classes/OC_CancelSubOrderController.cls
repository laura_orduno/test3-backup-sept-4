global class OC_CancelSubOrderController {
    
    // --------------------------------------------
    // Page Getters, Setters & General Declarations
    // --------------------------------------------
    
    public String PageTitle{get;set;}
    public String RedirectPage{get;set;}
    public Order parentOrder {get; set;}
    public Order dummyOrder {get; set;}
    public List<Order> childOrders {get; set;}
    public Boolean reSumbitOrder {get;set;}
    public boolean isCancelCompleted{get;set;}
    public string profileName{get;set;}
    public string parentTabId{get;set;}
    public integer mockTestVariable=0;
    public String previousOrderStatus{get;set;}
    public string errorLevel{get;set;}
    public string messageName{get;set;}
    public String RCID{get;set;}
    public String childOrderIdStr {get;set;}
    public Boolean isValidContractExists{get;set;}
    public Boolean isAnyOrderInListToCancel{get;set;}
    public String orderNumCanNotProcess {get;set;}
    public String orderNumCanProcess {get;set;}
    Map<String, Object> inputMap = new Map<String, Object>();
    Map<String, Object> outputMap = new Map<String, Object>();
    Map<Id, Order> childOrdersMap  = new Map<Id, Order> ();
    global OC_CancelSubOrderController(ApexPages.StandardController objController) {
        reSumbitOrder= false;
        orderNumCanNotProcess = '';
        orderNumCanProcess = '';
        // Determine whether the page references an Id
        dummyOrder = new Order();
        Id thisId = ApexPages.currentPage().getParameters().get('id');
        String childOrderNumStr = ApexPages.currentPage().getParameters().get('selectedChildOrderIds');
        String[] childOrderIds = childOrderNumStr.split(',');
        
        parentTabId=ApexPages.currentPage().getParameters().get('parentTabID');  

        if(String.isNotBlank(thisId)){
            parentOrder = [Select id,accountId,parentId__c,OrderNumber, status, Master_Order_Status__c,
                           (SELECT Id,ContractNumber,vlocity_cmt__OrderId__c,Status 
                                     From vlocity_cmt__Contracts__r 
                                     where Status NOT IN ('Cancelled Request', 'Cancelled','Client Declined', 'Deactivated', 'Acceptance Expired','Terminated')) 
                           FROM order 
                           WHERE Id =:thisId ];
            if(null != parentOrder){
                RCID = parentOrder.accountId;
                List<Contract> contractList = parentOrder.vlocity_cmt__Contracts__r;
                if(contractList != null && contractList.size() > 0 && !contractList.isEmpty()){
                    isValidContractExists = true;
                }else{ 
                    isValidContractExists = false;
                }
            }
        }
        
        childOrders = [Select o.orderMgmtId_Status__c,o.ordermgmtid__c,o.OrderNumber,o.Order_Number__c,o.Credit_Assessment__c,o.Credit_Assessment__r.CAR_Status__c,o.vlocity_cmt__AccountId__c,
                    o.RecordTypeId, o.Name,o.Id,o.Reason_Code__c,o.General_Notes_Remarks__c,status, sub_order_number__c,
                    (SELECT Id,ContractNumber,vlocity_cmt__OrderId__c,Status From vlocity_cmt__Contracts__r 
                     where Status NOT IN ('Cancelled Request', 'Cancelled','Client Declined', 'Deactivated', 'Acceptance Expired','Terminated')) 
                    from Order o where o.OrderNumber IN: childOrderIds];
        // if(objOrder.General_Notes_Remarks__c !='' && !Test.isRunningTest())
        String pageTitleTmp='';
        if(childOrders != null){
            List<String> cancelOrderIds = new List<String>();
            for(Order orderObj: childOrders){
                orderObj.General_Notes_Remarks__c='';
                childOrdersMap.put(orderObj.Id, orderObj);
                if(orderObj.Status != 'Complete' && orderObj.Status != 'Cancelled'){
                    orderObj.Change_Type__c = 'Cancel Entire Order';
                    cancelOrderIds.add(orderObj.Id);
                    orderNumCanProcess = orderNumCanProcess + ',' + orderObj.sub_order_number__c;
                    if(String.IsBlank(childOrderIdStr)){
                        childOrderIdStr = orderObj.Id;
                    }else{
                        childOrderIdStr = childOrderIdStr + ',' + orderObj.Id;
                    }
                }else{
                    orderNumCanNotProcess = orderNumCanNotProcess + ',' + orderObj.sub_order_number__c;
                }
                pageTitle = pageTitle + ' ' + orderObj.OrderNumber;
            }
            if(cancelOrderIds.size()>0){
                isAnyOrderInListToCancel = true;
            }
            inputMap.put('cancelOrderIds', cancelOrderIds);
        }
        
        PageTitle = 'Cancel Order: ' + pageTitleTmp;

        isCancelCompleted=false;
        user currentUser=[select profile.name from user where id=:userinfo.getuserid()];
        this.profileName=currentUser.profile.name;
    } 
    
    public void DoOk(){
        RedirectPage='';
        isCancelCompleted=false;
        
        if(String.isBlank(dummyOrder.Reason_Code__c) || String.isBlank(dummyOrder.General_Notes_Remarks__c) ){
            //RedirectPage='Error:One or More fields have been left Blank';
            apexpages.addmessage(new apexpages.message(apexpages.severity.error,'Reason Code and General Notes Remarks cannot be blank.'));
        }else {
                VlocityCPQUtil vlcCPQUtil = new VlocityCPQUtil();
                inputMap.put('reasonCode',dummyOrder.Reason_Code__c);
                inputMap.put('generalNotesRemark',dummyOrder.General_Notes_Remarks__c);
                vlcCPQUtil.invokeMethod('cancelOrders', inputMap, outputMap, null);
                if(null != outputMap && null != outputMap.get('cancelOrdersResult')){
                     Map<String,VlocityCPQUtil.OrderWrapper>  cancelOrderResultMap =  ( Map<String,VlocityCPQUtil.OrderWrapper>)outputMap.get('cancelOrdersResult');
                     if(null != cancelOrderResultMap && cancelOrderResultMap.size() >0){
                         isCancelCompleted=true;
                         for(VlocityCPQUtil.OrderWrapper OrderWrapper :cancelOrderResultMap.values() ){
                             if(OrderWrapper.isError){
                                String childOrderNumber = OrderWrapper.orderId;
                                if(null != childOrdersMap && null != childOrdersMap.get(OrderWrapper.orderId)){
                                    childOrderNumber = (childOrdersMap.get(OrderWrapper.orderId)).sub_order_number__c;
                                }                               
                               apexpages.addmessage(new apexpages.message(apexpages.severity.error,'  '+childOrderNumber+'  '+  OrderWrapper.message));  
                             }
                         }
                     }
                }else if(null != outputMap && null != outputMap.get('error')){
                    apexpages.addmessage(new apexpages.message(apexpages.severity.error,'  ' +  outputMap.get('error')));
                }
            
            /*if(!apexpages.hasmessages()){
                Boolean result=DoCancel(objOrder.id,objOrder.Reason_Code__c,objOrder.General_Notes_Remarks__c);
                if(result)
                { 
                    //apexpages.addmessage(new apexpages.message(apexpages.severity.confirm,'Your Order has been cancelled successfully.  Please click the Close button.'));
                    isCancelCompleted=true;
                    system.debug('Is cancel Completed' + isCancelCompleted);
                }
                
            }*/        
        }
        // return null;
    }
    
    public void showMessage() {
        if(errorLevel == 'CONFIRM') 
            ApexPages.addMessage(new ApexPages.Message(apexpages.severity.confirm, messageName));
    }
    
    public pagereference invalidForm(){
        apexpages.addmessage(new apexpages.message(apexpages.severity.error,' Reason Code, and Notes cannot be blank.'));
        return null;
    }
    public PageReference CancelButton()
    { 
        PageReference pageRef = new PageReference('/apex/OC_OrderSummaryMultiSite?parentOrderId='+parentOrder.Id);
        pageRef.setRedirect(true);
        return pageRef;
    }
    
   /* public boolean DoCancel(string orderId, string reasonCode, string notes)
    {       
        Order ord = [Select change_type__c, reason_code__c,Status, General_Notes_Remarks__c,Credit_Assessment__r.CAR_Result__c,Credit_Assessment__r.CAR_Status__c,Credit_Assessment__c,Credit_Assessment__r.CAR_Code__c from Order where Id=:orderId];
        ord.Change_Type__c = 'Cancel Entire Order';
        ord.Reason_Code__c = reasonCode;
        ord.General_Notes_Remarks__c = notes;
        ord.Status ='Cancelled';
        ord.Activities__c='N/A';
        try{update ord;}catch(exception e){ apexpages.addmessage(new apexpages.message(apexpages.severity.error,'Please contact System Admin' + e.getMessage()));return false;}
        system.debug('inside car cancellation 1'+ord);
        //CAR Cancellation
        string carNo = ord.Credit_Assessment__c;
        string carStatus;
        string carResult;    
        if(string.isNotBlank(carNo)){
            carStatus =ord.Credit_Assessment__r.CAR_Status__c;
            carResult =ord.Credit_Assessment__r.CAR_Result__c;
        }
        string orderStatus =ord.Status;
        // Thomas QC-10813 Start
        //if(string.isnotblank(carNo) && string.isnotblank(orderStatus) && orderStatus=='Cancelled' && carStatus !='Cancelled' && carStatus !='Completed' && carResult!='Approved'){
        if(string.isnotblank(carNo) && string.isnotblank(orderStatus) && orderStatus=='Cancelled' && carStatus !='Cancelled'){
        // Thomas QC-10813 End
            system.debug('inside car cancellation 1 '+ord);
            Credit_Assessment__c CAR= new Credit_Assessment__c();
            CAR.id=carNo;
            CAR.CAR_Status__c='Cancelled';     
            CAR.CAR_Code__c='Cancel order received: TELUS Initiated'; //o.Reason_Code__c;
            // need to add field for reason to cancel           
            try{update CAR; }catch(exception e){apexpages.addmessage(new apexpages.message(apexpages.severity.error,'Please contact System Admin' + e.getMessage()));return false;}
        } 
        return true;
    }*/
    
   /* @remoteaction
    global static String vfrPrepareOrderCancellationRequest(string orderId, string reasonCode, string notes)
    {       
        Order ord = [Select change_type__c, reason_code__c,Status, General_Notes_Remarks__c,Credit_Assessment__r.CAR_Result__c,Credit_Assessment__r.CAR_Status__c,Credit_Assessment__c,Credit_Assessment__r.CAR_Code__c from Order where Id=:orderId];
        ord.Change_Type__c = 'Cancel Entire Order';
        ord.Reason_Code__c = reasonCode;
        ord.General_Notes_Remarks__c = notes;
        String previousStatus = ord.Status;
        
        return previousStatus;
    }*/

    @remoteaction
    public static string cancelEnvelope(String  childOrderIdsP){
        String errorMsgs; 
        boolean isRemoteCall = false;
        string callSuccessfulMessage = '';
        string sErrorMessage = '';
        try{ 
            //String  OrderId = (string)inputMap.get('ContextId');
            String[] childIds = childOrderIdsP.split(',');
            for(String OrderId: childIds){
                Order objOrder = new Order(id=OrderId);
                system.debug('Order Id_____:' + objOrder);
                //Iterate trough Order Items to see if any of them are Amended.--- Check if we need to use a Specific value for AmendStatus
                if(OrderId != null){
                    map<String,object> outMap = OCOM_ContractUtil.AmendOrder(objOrder,'Cancel');
                    sErrorMessage=(string)outMap.get('amendContractResponse');
                    isRemoteCall=(boolean)outMap.get('isCallContinuation');
                    string envelopeId = (string)outMap.get('envelopId'); 
                    System.debug('OutMap--->' + outMap);
                }
                errorMsgs = sErrorMessage;
                system.debug('****isRemoteCall*****'+isRemoteCall + '***sErrorMessage****'+sErrorMessage);
            }
        }catch(exception e){
            String errorMessage=Label.OCOM_ExceptionMessagePrefix+e.getMessage();
            System.debug(LoggingLevel.ERROR, 'Exception is '+e);
            System.debug(LoggingLevel.ERROR, 'Exception stack trace '+e.getStackTraceString());
            
            return errorMsgs = errorMessage;
            // outPutMap.put('error',errorMessage);
        }
        return errorMsgs;
        
    }   
    
}