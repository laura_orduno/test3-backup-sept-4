/**
* @author Alex Kong - akong@tractionondemand.com
* @description Unit Test for MBRAccountConfigurationCtlr.cls
*/
@isTest(SeeAllData=false)
private class MBRAccountConfigurationCtlr_Test {
    
    @isTest static void testAccountConfigurationCtlr() {
        Account a = new Account(Name = 'Unit Test Account');
        insert a;
        ApexPages.StandardController stdController = new ApexPages.StandardController(a);
        MBRAccountConfigurationCtlr ctlr = new MBRAccountConfigurationCtlr(stdController);
        System.assert(!ctlr.RestrictTechSupport);
        
        Test.startTest();
        ctlr.RestrictTechSupport = true;
        PageReference pageRef = ctlr.save();
        Test.stopTest();
        
        List<Account_Configuration__c> cfgs = [SELECT Id, Name, Restrict_Tech_Support__c FROM Account_Configuration__c WHERE Account__c = :a.Id];
        System.assertEquals(1, cfgs.size());
        System.assert(cfgs[0].Restrict_Tech_Support__c);
    }

}