@isTest
public class CreditUitClassTest {

    
    
    @isTest
    public static void CreditWebServiceClientUtil() { 
        
        String creditID = '123456';
        String creditObject = '123456';
        String operationOrMethod = '123456';
        String webserviceName = '123456';
        
        String payload = '123456';
        String result = '123456';
        Integer retryCount = 1;
        String retryStatus = '123456';
        String status = '123456';
        String createdById = CreditUnitTestHelper.getTestuser().id;
        DateTime createdDate = Datetime.Now();
        CalloutException e = new CalloutException();
        
        CreditWebServiceClientUtil.logCalloutErrors(
            creditID
            , creditObject
            , operationOrMethod
            , payload
            , result
            , retryCount
            , retryStatus
            , status
            , createdById
            , createdDate
            , e);
        
        
        CreditWebServiceClientUtil.writeToWebserviceIntegrationErrorLog(
            creditID
            , creditObject
            , operationOrMethod
            , webserviceName
            , e);
        
        CreditWebServiceClientUtil.getFaultcode(e);
        CreditWebServiceClientUtil.getErrorCode(e);
        CreditWebServiceClientUtil.getAssessmentAuditInfo();
        
        
        CreditWebServiceClientUtil.getProfileAuditInfo();
        CreditWebServiceClientUtil.getPhoneNumberDigitOnly('604-813-6666');        
        
    }
    
    
    /*
  */
    @isTest
    public static void CreditReferenceCode() { 
        
        LegalEntityType__c legalEntityType = new LegalEntityType__c();
        legalEntityType.Name = 'LTD';
        legalEntityType.Refpds_Code__c = 'LTD';
        legalEntityType.Salesforce_Value__c = 'Limited (LTD)';
        insert legalEntityType;        
        CreditReferenceCode.getLegalEntityTypeCd('Limited (LTD)');
        
        
        
        
        RiskIndicator__c risk = new RiskIndicator__c();
        risk.Name = 'TST';
        risk.Refpds_Code__c = 'TST';
        risk.Salesforce_Value__c = 'Custom Setting Test';
        insert risk;        
        CreditReferenceCode.getCreditRiskIndicatorCd('Custom Setting Test');
        
        
        
        CustomerType__c custype = new CustomerType__c();
        custype.Name = 'TST';
        custype.Refpds_Code__c = 'TST';
        custype.Salesforce_Value__c = 'Custom Setting Test';
        insert custype;        
        CreditReferenceCode.getCustomerStatusCd('Custom Setting Test');
        
        CorporateRegistryStatus__c corpcode = new CorporateRegistryStatus__c();
        corpcode.Name = 'TST';
        corpcode.Refpds_Code__c = 'TST';
        corpcode.Salesforce_Value__c = 'Custom Setting Test';
        insert corpcode;        
        CreditReferenceCode.getCoporateRegistryStatusCd('Custom Setting Test');
        
        
        RegistrationJurisdictionCountry__c country = new RegistrationJurisdictionCountry__c();
        country.Name = 'TST';
        country.Refpds_Code__c = 'TST';
        country.Salesforce_Value__c = 'Custom Setting Test';
        insert country;        
        CreditReferenceCode.getJurisdictionCountryCd('Custom Setting Test');
        CreditReferenceCode.getJurisdictionCountryName('TST');
        
        
        RegistrationJurisdictionProvince__c province = new RegistrationJurisdictionProvince__c();
        province.Name = 'TST';
        province.Refpds_Code__c = 'TST';
        province.Salesforce_Value__c = 'Custom Setting Test';
        insert province;        
        CreditReferenceCode.getJurisdictionProvinceCd('Custom Setting Test');
        CreditReferenceCode.getJurisdictionProvinceName('TST');
        
        
        
        
        PrimaryOrderType__c order = new PrimaryOrderType__c();
        order.Name = 'TST';
        order.Refpds_Code__c = 'TST';
        order.Salesforce_Value__c = 'Custom Setting Test';
        insert order;        
        CreditReferenceCode.getOrderTypeCd('Custom Setting Test');
        
        
        Credit_Report_Type__c rptype = new Credit_Report_Type__c();
        rptype.Name = 'TST';
        rptype.Refpds_Code__c = 'TST';
        rptype.Salesforce_Value__c = 'Custom Setting Test';
        insert rptype;        
        CreditReferenceCode.getReportTypeCd('Custom Setting Test');
        
        
        
        Credit_Report_Source__c rpsrc = new Credit_Report_Source__c();
        rpsrc.Name = 'TST';
        rpsrc.Refpds_Code__c = 'TST';
        rpsrc.Salesforce_Value__c = 'Custom Setting Test';
        insert rpsrc;        
        CreditReferenceCode.getReportSourceCd('Custom Setting Test');
        
        
        CorporateRegistryStatus__c regstatus = new CorporateRegistryStatus__c();
        regstatus.Name = 'TST95';
        regstatus.Refpds_Code__c = 'TST95';
        regstatus.Salesforce_Value__c = 'Custom Setting Test95';
        insert regstatus;        
        CreditReferenceCode.getCorporateRegistryStatus('Custom Setting Test95');
        
        
        
        CreditReferenceCode.getSingleLetterCode('Yes');
        CreditReferenceCode.getSingleLetterCode('No');
        CreditReferenceCode.getYesNo('Yes');    
        
    }
    
    
}