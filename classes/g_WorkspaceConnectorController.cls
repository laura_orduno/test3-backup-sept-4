/* g_WorkspaceConnectorController
TELUS - Controller Class for iWS Connector */

global class g_WorkspaceConnectorController {
    
    @RemoteAction
    global static Case findCaseFromNumber(String num) {
        system.debug('[WSC] findCaseFromNumber using  ' + num);
        List < Case > cases = [SELECT CaseNumber FROM Case WHERE CaseNumber = : num];
         if (!cases.isEmpty()) {
               return cases.get(0);// return the first case found (should only be one)           
         }
        return null;
    }
    
    @RemoteAction
    global static Object findObjectFromANI(String ANI) {
        system.debug('*** findObjectFromANIfor ' + ANI);
        Integer listSize = 0;
        //List<SObject> objList;
        SObject obj_found = null;
        List < List < SObject >> aobjects = [FIND: ANI IN PHONE FIELDS RETURNING Account];
        if (!aobjects.isEmpty()) {
            List < Account > accounts = ((List < Account > ) aobjects[0]);
            listSize = accounts.size();
            system.debug('*** accounts listSize = ' + listSize);
            if (listSize > 1) {
                return 'multiple found';
            }
            if (listSize != 0) {
                for (List < SObject > objList: aobjects)
                    for (SObject aobj: objList) {
                        obj_found = aobj;
                        system.debug('*** findObjectFromANI account = ' + obj_found.Id);
                    }
            }
        }
            
        List < List < SObject >> cobjects = [FIND: ANI IN PHONE FIELDS RETURNING Contact];
        if (!cobjects.isEmpty()) {
            List < Contact > contacts = ((List < Contact > ) cobjects[0]);
            listSize = contacts.size();
            system.debug('*** contacts listSize = ' + listSize);
            if (listSize > 1) {
                return 'multiple found';
            }
            if (listSize == 1 && obj_found != null) {
                return 'multiple found';
            }
            if (listSize != 0) {
                for (List < SObject > objList: cobjects)
                    for (SObject cobj: objList) {
                        obj_found = cobj;
                        system.debug('*** findObjectFromANI contact = ' + obj_found.Id);
                    }
            }
        }
            
            /*Lead information not required for BSE at this time */
            /*List < List < SObject >> lobjects = [FIND: ANI IN PHONE FIELDS RETURNING Lead];
            if (!lobjects.isEmpty()) {
                List < Lead > leads = ((List < Lead > ) lobjects[0]);
                listSize = leads.size();
                system.debug('*** leads listSize = ' + listSize);
                if (listSize > 1) {
                    return 'multiple found';
                }
                if (listSize == 1 && obj_found != null) {
                    return 'multiple found';
                }
                if (listSize != 0) {
                    for (List < SObject > objList: lobjects)
                        for (SObject lobj: objList) {
                            obj_found = lobj;
                            system.debug('*** findObjectFromANI lead= ' + obj_found.Id);
                        }
                }
            } */
            
            if (obj_found != null) return obj_found;
            return 'not found';
    }
    
    
    @RemoteAction
    global static Object findContactFromANI(String ANI) {
        system.debug('*** findContactFromANI ' + ANI);
        List < List < SObject >> cobjects = [FIND: ANI IN PHONE FIELDS RETURNING Contact];
        SObject obj_found = null;
        Integer listSize = cobjects.size();
        system.debug('*** listSize = ' + listSize);
        if (!cobjects.isEmpty()) {
            List < Contact > contacts = ((List < Contact > ) cobjects[0]);
            listSize = contacts.size();
            system.debug('*** contacts listSize = ' + listSize);
            if (listSize > 1) {
                return 'multiple found';
            }else if (listSize == 1) {
                obj_found = contacts.get(0);
            }
        }
        if (obj_found != null) return obj_found.id;
           return 'not found';
      
            
        
    }
    
    @RemoteAction
    global static Object findContactFromEmailAddress(String address) {
        system.debug('*** findObjectFromEmailAddress' + address);
        List < Contact > objects = [select name from contact where email = : address];
        if (!objects.isEmpty()) {
            for (Contact obj: objects) {
                system.debug('*** findContactFromEmailAddress contact = ' + obj.Name);
                return obj;
            }
        }
        return null;
    }
    
    @RemoteAction
    global static Object findContactFromChatAddress(String theName) {
        system.debug('*** findObjectFromChatAddress' + theName);
        List < Contact > objects = [select name from contact where name = : theName];
        if (!objects.isEmpty()) {
            for (Contact obj: objects) {
                system.debug('*** findObjectFromChatAddresscontact = ' + obj.Name);
                return obj;
            }
        }
        return null;
        
    }
    
    
    @RemoteAction
    global static Object findContactFromWorkItemAddress(String theName) {
        system.debug('*** findContactFromWorkItemAddress' + theName);
        List < Contact > objects = [select name from contact where name = : theName];
        if (!objects.isEmpty()) {
            for (Contact obj: objects) {
                system.debug('*** findContactFromWorkItemAddress = ' + obj.Name);
                return obj;
            }
        }
        return null;
      
    }
    
    
    @RemoteAction
    global static Object findContactFromOpenMediaAddress(String theName) {
        system.debug('*** findContactFromOpenMediaAddress' + theName);
        List < Contact > objects = [select name from contact where name = : theName];
        if (!objects.isEmpty()) {
            for (Contact obj: objects) {
                system.debug('*** findContactFromOpenMediaAddress = ' + obj.Name);
                return obj;
            }
        }
        return null;
    }
    
    
    @RemoteAction global static String createActivity(Map < String, String > activityMap) {
        system.debug('*** createActivity');
        String federatedId = '';
        User myAgent;
        String result = 'not found';
        RecordType activityRecordType;
        activityRecordType = [SELECT Id from RecordType Where SobjectType = 'Task'
                                      and DeveloperName = 'SMB_Care_Call_Tracker'];
            //Added for CES20-321 Lookup the User's Federated ID for currently logged in User
        List<User> myAgents = [SELECT Id, FederationIdentifier from User where id = : UserInfo.getUserId()];
        if(myAgents.size()==1){
            myAgent = myAgents.get(0);
            federatedId = myAgent.FederationIdentifier;
        }
        String ANI = activityMap.get('ANI');
        String myDNIS = activityMap.get('DNIS');
        String lookupSource = activityMap.get('LOOKUP');
        system.debug('*** createActivity for ' + lookupSource + ' - ANI = ' + ANI);
        system.debug('*** duration = ' + activityMap.get('Call Duration'));
        String callType = activityMap.get('IXN Type');
        String mediaType = activityMap.get('Media Type');
        //get Voice Call Direction from activityMap
        String callDx = activityMap.get('Voice Call Direction');
        String subject = callType + ' ' + mediaType + ' ' + activityMap.get('DATE'); 
        //if ((activityMap.get('Subject') != null)) {
        //    subject = activityMap.get('Subject');
        //} else {
        //    subject = callDx + ' call ';
        //}
        String TypeOfTask = callDx;
        String objectToUse = '';
        String duration = activityMap.get('Call Duration');
        Integer durationInSecs = Integer.valueOf(duration);
        
        String pushedCaseId = '';
        String pushedCaseBrand = '';
        
        //workitem handling        
        if (callType == 'InteractionWorkItem') {
            subject = 'Work Item';
            TypeOfTask = 'Work Item';
            String workitemObj = '';
            if(!String.isEmpty(activityMap.get('Outcome_CaseId'))){
                workitemObj = activityMap.get('Outcome_CaseId');
                Case myCase = [Select Id from Case where Id = : workitemObj];
                pushedCaseId = myCase.Id;
            }
            
            else if (String.isEmpty(workitemObj)){
                workitemObj = activityMap.get('Outcome_TaskId');
            } 
            
            objectToUse = workitemObj;
            system.debug('*** workitemObj = ' + workitemObj);
        }
        String pushedWorkitem = activityMap.get('Outcome_TaskId');
        system.debug('******* PushedworkItem : ' + pushedWorkitem);
        
        String pushedItemSubject = '';
        String myPushedTaskBrand = '';
        
        if (!String.isEmpty(pushedWorkitem)) {
            try {
                Task myTask = [SELECT Id, Subject FROM Task WHERE Id = : pushedWorkitem];
                pushedItemSubject = myTask.Subject;
            } catch (Exception e) {
                system.debug(e);
            }
        }
        
        DateTime startDate = null;
        try {
            startDate = (activityMap.get('StartDate') == '' && activityMap.get('StartDate') == null) ? null : dateTime.valueOf(activityMap.get('StartDate'));
        } catch (Exception e) {
            system.debug(e);
        }
        system.debug('*** start date = ' + startDate);
        
        DateTime endDate = null;
        try {
            endDate = (activityMap.get('EndDate') == '' && activityMap.get('EndDate') == null) ? null : dateTime.valueOf(activityMap.get('EndDate'));
        } catch (Exception e) {
            system.debug(e);
        }
        
        system.debug('*** end date = ' + endDate);
        
        if (activityMap.get('sfdc Object Id') != '') {
            system.debug('*** createActivity sfdc Object Id = ' + activityMap.get('sfdc Object Id'));
            objectToUse = activityMap.get('sfdc Object Id');
        }
        
        system.debug('*** createActivity for object ' + objectToUse);
        
        String accountPrefix = Schema.SObjectType.Account.getKeyPrefix();
        String contactPrefix = Schema.SObjectType.Contact.getKeyPrefix();
        String leadPrefix = Schema.SObjectType.Lead.getKeyPrefix();
        //added for getting Case keyPrefix
        String casePrefix = Schema.SObjectType.Case.getKeyPrefix();
        String taskPrefix = Schema.SObjectType.Task.getKeyPrefix();
        //log it just to see if its 500
        if (casePrefix != '') system.debug('*** Case keyPrefix is ' + casePrefix);
        String prefix = '';
        if (objectToUse != '') prefix = objectToUse.substring(0, 3);
        system.debug('*** prefix = ' + prefix);
        
        string TN_NUM = '';
        if (activityMap.get('Subscriber Phone Number') == '') {
            TN_NUM = 'No Subscriber Number Present In Attached Data';
        } else TN_NUM = activityMap.get('Subscriber Phone Number');
        
        //Added for CES20-321
        String GenesysAgentId = '';
        system.debug('*** GenesysAgentId from KVP : ' + activityMap.get('Agent ID'));
        
        //Handle blank GenesysId by looking up User Federated ID 
        if ((String.isEmpty(activityMap.get('Agent ID'))) || (activityMap.get('Agent ID') == '') || (activityMap.get('Agent ID') == null)) {
            GenesysAgentId = federatedId;
            system.debug('*** Setting GenesysAgentId to Federation Id of User : ' + GenesysAgentId);
        } else {
            GenesysAgentId = activityMap.get('Agent ID');
            system.debug('*** Setting GenesysAgentId to [RTargetAgentSelected] KVP : ' + GenesysAgentID);
            myAgents = [SELECT Id, FederationIdentifier from User where FederationIdentifier = : GenesysAgentID ];
            if(myAgents.size()==1){
            	myAgent = myAgents.get(0);
            }
        }
        
        Task t = new Task(
            Subject = subject,
            //Type has to match Subject
            Type = 'Call',
            Status = 'Completed',
            CallDurationInSeconds = durationInSecs,
            Account_Number__c=activityMap.get('Account Number'),
            Description = activityMap.get('Comments'),
            CallDisposition = activityMap.get('Disposition'),
            CallObject = activityMap.get('GenesysId'),
            // Added Custom Fields for Activity Creation
            // Comment for Package creation
            Disposition__c = activityMap.get('Disposition'),
            Call_Duration__c = activityMap.get('Formatted Duration'),
            Calling_Line_ID__c = activityMap.get('Calling Line Id'),
            Call_Topic__c = activityMap.get('Call Topic'),
            Call_Type__c = activityMap.get('Call Type'),
            DNIS__c = myDNIS,
            GenesysId__c = activityMap.get('GenesysId'),
            Last_IVR_Menu__c = activityMap.get('last IVR'),
            TN_Number__c = TN_NUM,
            Transfer_History__c = activityMap.get('transfer history'),
            //Activity_Date__c = activityMap.get('DATE'),
            //Pushed_Item_ID__c = pushedWorkitem,
            //Pushed_Item_Subject__c = pushedItemSubject,
            //Changed for CES20-401
           	CallType = callDx    
           	// Verified with this Query that no Records exist past Outcome 3.0 in March
           	// Where CallType and Subject do not match
           	//SELECT Id,CallType,Subject,Activity_Date__c,LastModifiedDate FROM Task where CallType = 'Inbound' 
           	//and Subject = 'Outbound Call' and LastModifiedDate > 2015-03-01T00:00:00.000Z
            //Genesys_Agent_ID__c = GenesysAgentId
            );
        
        system.debug('****DNIS : ' + myDNIS);
        
        Account myAccount;
        String accountBrand = '';
        Contact myContact;
        String contactBrand = '';
        Case myCase;
        String caseBrand = '';
        
        if (objectToUse == '') {
            //Do nothing
            system.debug('**** objectToUse was blank');
        } else {
            if (prefix == accountPrefix) {
                //added objectToUse in debug so we can see ID 
                system.debug('*** create task for Account ' + objectToUse);
                t.put('WhatId', objectToUse);
                try{
                myAccount = [Select Id from Account where Id = : objectToUse];
                }
                catch(Exception aa){
                 System.debug(' Could not fetch brand from objectToUse : ' + objectToUse + ' , setting to TELUS');
                }
            }
            //Check for Case
            else if (prefix == casePrefix) {
                system.debug('*** create task for Case ' + objectToUse);
                t.put('WhatId', objectToUse);
                                try{
                myCase = [Select Id from Case where Id = : objectToUse];
                }
                catch(Exception aa){
                 System.debug(' Could not fetch brand from objectToUse : ' + objectToUse + ' , setting to TELUS');
                }
                
            } else if (prefix == taskPrefix) {
                system.debug('*** create new task using Contact under the current Task ' + objectToUse);
                List<Task> myTask = [SELECT WhoId FROM Task WHERE Id = : objectToUse];
                
                if (myTask.size() == 1){
                 t.put('WhoId', myTask.get(0).WhoId);
                }
            } else {
                t.put('WhoId', objectToUse);
            }
        }
        if (prefix == contactPrefix) {
            List<Contact> myContacts = [SELECT AccountId, Id FROM Contact WHERE Id = : objectToUse];
            system.debug('*** create task for Contact ' + objectToUse);
            if (myContacts.size() ==1){
               myContact = myContacts.get(0);
               t.put('WhatId', myContact.AccountId);
            } 
        }
        
        String mySFDCfield = '';
        String mySFDCvalue = '';
        if (activityMap.get('SFDC1field') != '' && (activityMap.get('SFDC1field') != null) && activityMap.get('SFDC1value') != '' && activityMap.get('SFDC1value') != null) {
            mySFDCfield = activityMap.get('SFDC1field');
            mySFDCvalue = activityMap.get('SFDC1value');
            system.debug('*** mySFDCfield1 ' + mySFDCfield + ' -- ' + mySFDCvalue);
            t.put(mySFDCfield, mySFDCvalue);
        }
        if (activityMap.get('SFDC2field') != '' && (activityMap.get('SFDC2field') != null) && activityMap.get('SFDC2value') != '' && activityMap.get('SFDC2value') != null) {
            mySFDCfield = activityMap.get('SFDC2field');
            mySFDCvalue = activityMap.get('SFDC2value');
            system.debug('*** mySFDCfield2 ' + mySFDCfield + ' -- ' + mySFDCvalue);
            t.put(mySFDCfield, mySFDCvalue);
        }
        if (activityMap.get('SFDC3field') != '' && (activityMap.get('SFDC3field') != null) && activityMap.get('SFDC3value') != '' && activityMap.get('SFDC3value') != null) {
            mySFDCfield = activityMap.get('SFDC3field');
            mySFDCvalue = activityMap.get('SFDC3value');
            system.debug('*** mySFDCfield3 ' + mySFDCfield + ' -- ' + mySFDCvalue);
            t.put(mySFDCfield, mySFDCvalue);
        }
        if (activityMap.get('SFDC4field') != '' && (activityMap.get('SFDC4field') != null) && activityMap.get('SFDC4value') != '' && activityMap.get('SFDC4value') != null) {
            mySFDCfield = activityMap.get('SFDC4field');
            mySFDCvalue = activityMap.get('SFDC4value');
            system.debug('*** mySFDCfield4 ' + mySFDCfield + ' -- ' + mySFDCvalue);
            t.put(mySFDCfield, mySFDCvalue);
        }
        if (activityMap.get('SFDC5field') != '' && (activityMap.get('SFDC5field') != null) && activityMap.get('SFDC5value') != '' && activityMap.get('SFDC5value') != null) {
            mySFDCfield = activityMap.get('SFDC5field');
            mySFDCvalue = activityMap.get('SFDC5value');
            system.debug('*** mySFDCfield5 ' + mySFDCfield + ' -- ' + mySFDCvalue);
            t.put(mySFDCfield, mySFDCvalue);
        }
        
        //Activity Record Type Attempt to map Call_Activity_Record_Type
        if (activityRecordType != null) {
            t.RecordTypeId = activityRecordType.Id;
        } else {
            system.debug('*** No activityRecordType');
        }
        
        try {
            insert t;
        } catch (DmlException e) {
            t.put('WhatId', objectToUse);
            system.debug('*** Invalid Id provided. Caught DMLException and using this object to create Activity :' + objecttoUse);
            insert t;
        }
        system.debug('*** Task id = ' + t.Id);
        result = t.Id;
        return result;
    }
    
    @RemoteAction
    global static String createCase(Map < String, String > caseMap) {
        system.debug('*** createCase');
        String result = 'case not created';
        String ixnType = caseMap.get('IXN Type');
        String mediaType = caseMap.get('Media Type');
        String subject = 'Inbound Call';
        Case c = new Case(
            Subject = subject,
            Priority = 'Medium',
            Origin = ixnType);
        
        
        try {
            insert c;
        } catch (QueryException e) {
            return result;
        }
        system.debug('*** Case id = ' + c.Id);
        result = c.Id;
        return result;
    }
    
    @RemoteAction
    global static String addAttachment(String objectId, String descriptionText, String nameText, String mimeType, Blob attachmentBody, String attachmentId) {
        system.debug('*** addAttachment to ' + objectId);
         if (attachmentBody != null) {
                Attachment att = getAttachment(attachmentId);
                
                String newBody = '';
                if (att.Body != null) {
                    newBody = EncodingUtil.base64Encode(att.Body);
                }
                
                String newAttachmentBody = EncodingUtil.base64Encode(attachmentBody);
                
                newBody += newAttachmentBody;
                
                att.Body = EncodingUtil.base64Decode(newBody);
                
                if (attachmentId == null) {
                    system.debug('*** First time through');
                    att.Name = nameText;
                    att.parentId = objectId;
                }
                upsert att;
                return att.Id;
         } else {
             return 'error';
         }
      
    }
    
    private static Attachment getAttachment(String attId) {
        list < Attachment > attachments = [SELECT Id, Body
                                           FROM Attachment
                                           WHERE Id = : attId];
        if (attachments.isEmpty()) {
            Attachment a = new Attachment();
            return a;
        } else {
            return attachments[0];
        }
    }
    
    
    @RemoteAction
    global static String testConnection() {
        return 'Active';
    }
    
}