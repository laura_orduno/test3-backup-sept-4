public with sharing class trac_AccountHierarchyCtlr {
    public static final Id ACCT_RT_RCID = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' AND Name = 'RCID'].Id;
    public static final Id ACCT_RT_CBUCID = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' AND Name = 'CBUCID'].Id;
    public static final Integer BAN_LIMIT = (!Test.isRunningTest() ? 2000 : 5);
    
    public Id accountId {get; set;}
    
    private Account CBUCIDaccount;
    private Map<Id, List<Account>> RCIDlevel;
    private Map<Id, List<Billing_Account__c>> BANlevel;
    private String treeHTML;
    
    
    public trac_AccountHierarchyCtlr() {
    	RCIDlevel = new Map<Id, List<Account>>();
    	BANlevel = new Map<Id, List<Billing_Account__c>>();
    }
    
    
    private void stuffRCID(Account rcid) {
        if (rcid == null || rcid.RecordTypeId != ACCT_RT_RCID) { return; }
        if (RCIDlevel.get(rcid.ParentId) == null) {
            RCIDlevel.put(rcid.ParentId, new List<Account>());
        }
        RCIDlevel.get(rcid.ParentId).add(rcid);
    }
    
    
    private void stuffBAN(Billing_Account__c ban) {
        if (ban == null) { return; }
        if (BANlevel.get(ban.Account__c) == null) {
            BANlevel.put(ban.Account__c, new List<Billing_Account__c>());
        }
        BANlevel.get(ban.Account__c).add(ban);
    }
    
    
    public String getTreeHTML() {
        if (treeHTML != null) { return treeHTML; }
        
        String tc = '';
        if (accountId == null) { return null; }
        Account thisAccount = [
        	SELECT Name, ParentId, RecordTypeId, CBUCID__c, CBUCID_Parent_Id__c, RCID__c, CustProfId__c
        	FROM Account
        	WHERE Id = :accountid
        	LIMIT 1
        ];
        
        if (thisAccount == null) { return null; }
        
        if (thisAccount.RecordTypeId == ACCT_RT_RCID) { 
            if (thisAccount.ParentId == null) {
                treeHTML = 'np';
                return treeHTML;
            } else {
                Account[] acl = [
                	Select Name, ParentId, RecordTypeId, CBUCID__c, CBUCID_Parent_Id__c, RCID__c, CustProfId__c
                	From Account
                	Where Id = :thisAccount.parentId
                	LIMIT 1
                ];
                
                if (acl != null && acl.size() == 1) {
                    CBUCIDaccount = acl.get(0);
                }
            }
        } else if (thisAccount.RecordTypeId == ACCT_RT_CBUCID) {
            CBUCIDaccount = thisAccount;
        } else {
            treeHTML = 'nih';
            return treeHTML;
        }
        
        if (CBUCIDaccount == null) {
        	treeHTML = 'nap';
        	return treeHTML;
        }
        
        List<Id> rcidIds = new List<Id>();
        List<Account> tempRCIDs = [
        	Select Name, ParentId, RecordTypeId, CBUCID__c, CBUCID_Parent_Id__c, RCID__c, CustProfId__c
        	From Account
        	Where ParentId = :CBUCIDaccount.Id
        	LIMIT :BAN_LIMIT
        ];
        
        if (tempRCIDs != null && tempRCIDs.size() > 0) {
            for (Account a : tempRCIDs) {
                stuffRCID(a);
                rcidIds.add(a.Id);
            }
        }
        
        List<Billing_Account__c> tempBANs = [
        	Select Id, Name, BILLG_ACCT_NO__c, Account__c, CustProfId__c, Billing_System_Account_Number__c
        	From Billing_Account__c
        	Where Account__c In :rcidIds
        	and ACTIVE_IND__c = :'Y'
        	LIMIT :BAN_LIMIT
        ];
        
        if (tempBANs != null && tempBANs.size() > 0) {
            for (Billing_Account__c ba : tempBANs) {
                stuffBAN(ba);
            }
            if (tempBANs.size() >= BAN_LIMIT ) {
                tc += '<span style="display: inline-block; font-weight: bold; padding: 5px;">This CBUCID account has more than the ' +
                	BAN_LIMIT + ' BAN records displayed below. To view them all, visit the associated RCID account and look at the Billing Account related list.</span>';
            }
        }

        // Let's put together some HTML!
        String currentFlag = '';
        if (CBUCIDaccount.Id == thisAccount.Id) { currentFlag = 'current'; }
        
        tc += '<ul id="hierarchy"><img src="/s.gif" class="tree-logo cbucidlogo ' + currentFlag + '"/>&nbsp;<span class="cbucid ' +
        	currentFlag + '"><a target="_top" href="/' + CBUCIDaccount.Id + '">' + CBUCIDaccount.Name + '</a>,&nbsp;CBUCID:&nbsp;' +
        	CBUCIDaccount.CBUCID_Parent_Id__c + ',&nbsp;CUSTPROFID:&nbsp;' + CBUCIDaccount.CustProfID__c + '</span>';
        
        currentFlag = '';
            if (RCIDlevel.get(CBUCIDaccount.Id) != null) { for (Account ra : RCIDlevel.get(CBUCIDaccount.Id)) {
                // For each account, let's build the HTML
                currentFlag = '';
                if (ra.Id == thisAccount.Id) { currentFlag = 'current'; }
                tc += '<li class="rcid closed ' + currentFlag + '"><img src="/s.gif" class="tree-logo"/>&nbsp;<span><a target="_top" href="/' +
                	ra.Id + '">' + ra.Name + '</a>,&nbsp;RCID:&nbsp;' + ra.RCID__c + ',&nbsp;CUSTPROFID:&nbsp;' + ra.CustProfID__c + '</span>';
                    List<Billing_Account__c> bans = BANlevel.get(ra.Id);
                    if (bans != null) {
                        tc += '<ul>';
                        for (Billing_Account__c ba : bans) {
                            tc += '<li><img src="/s.gif" class="tree-logo"/>&nbsp;<span><a target="_top" href="/' + ba.Id + '">' +
                            	ba.Name + '</a>&nbsp;BAN:&nbsp' + ba.Billing_System_Account_Number__c + ',&nbsp;CUSTPROFID:&nbsp;' +
                            	ba.CustProfID__c + '</span></li>';
                        }
                        tc += '</ul>';
                    }
                tc += '</li>';
            } }
        tc += '</ul>';
        treeHTML = tc;
        return treeHTML;
    }
}