public class AgreementCommentCtrlExt {
    public boolean IsPropargate {get;set;}
    public boolean showPropergate {get;set;}
    public List<ItemSelect> displayItems {get;set;}
    private String linkField = null;
    private String returnURL;
    
    public String mode {get;set;}
    public String modeLabel {get;set;}
    protected  ApexPages.StandardController controller;
    public Sobject currentObj {get;set;} 
    public String RecType {get;set;}
    
    private Static final String MODE_EDIT = 'edit';
    private Static final String MODE_DETAIL = 'maindetail'; 
    
    
    
    public void edit(){
        mode = MODE_EDIT;
        modeLabel = Label.Edit;
    }
    
    public PageReference cancel(){
        mode = MODE_DETAIL;
        return new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
    }
    
    public PageReference save(){
        controller.save();
        if(ApexPages.hasMessages()){
            return null;
        }
        mode = MODE_DETAIL;
        modeLabel = Label.Detail;
        return new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
    }
    
    public void setToEditMode(){
        mode = MODE_EDIT;
        modeLabel = Label.Edit; 
    }
    
    public void setToDetailMode(){
        
        mode = MODE_DETAIL;
        modeLabel = Label.Detail;
    }

    
    public AgreementCommentCtrlExt(ApexPages.StandardController controller){
        returnURL = ApexPages.currentPage().getParameters().get('retURL');
        this.controller = controller;
        this.currentObj = controller.getRecord();
        try{
         recType = Util.getRecordTypeNameById(this.currentObj,(String)this.currentObj.get('RecordTypeId'));
         System.debug(LoggingLevel.ERROR,recType);
        }catch(Exception e){
            System.debug(LoggingLevel.ERROR,'No RecordTypes for this object : ' + this.currentObj);
        } 
        system.debug('currentobj: ' + currentObj);
       /* if(this.currentObj.get('Agreement__c') != null){
           linkField = 'Agreement__c'; 
           showPropergate = true;  
        }*/
        //Added For Contract
          if(this.currentObj.get('Contract__c') != null){
           linkField = 'Contract__c'; 
           showPropergate = true;  
        }
        else if(this.currentObj.get('Subsidiary_Account__c') != null){
           linkField = 'Subsidiary_Account__c'; 
           showPropergate = false; 
        }else if(this.currentObj.get('Deal_Support__C') != null){
           linkField = 'Deal_Support__C'; 
           showPropergate = false; 
        }
        if(linkField == null){
            String errmsg= 'Unexpected Error Occured: Link Field = ' +linkField;
            System.debug(LoggingLevel.ERROR,ApexPages.currentPage().getParameters());
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL,errmsg);
            ApexPages.addMessage(msg);
            
        }else{                 
            setToEditMode();
            this.displayItems = new List<ItemSelect>();
            initPropagateToItemList();
        }
    }
    
    public void showPropergateList(){
        controller.save();
        if(ApexPages.hasMessages()){
            return;
        }
        setToDetailMode();
        IsPropargate = true; 
    }
    
    public PageReference propagate(){
        Sobject beforeSaveObj = controller.getRecord();
        System.debug(LoggingLevel.ERROR,beforeSaveObj);
        controller.save();
        List<Sobject> toInsert = new LIst<Sobject>();
        System.debug(LoggingLevel.ERROR,this.displayItems);
        for(ItemSelect item : this.displayItems){
            if(item.selected == true){
                Sobject newObj= beforeSaveObj.clone(false);
                newObj.id = null;
                newObj.put('Subsidiary_Account__c',item.obj.id);
                newObj.put('Contract__c',null);
                toInsert.add(newObj);
            }
        }
        if(toInsert.isEmpty()){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,'Select at least one Item');
            ApexPages.addMessage(msg);
            return null;
        }
        try{
            insert toInsert;
        }catch(Exception e ){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL,e.getMessage());
            ApexPages.addMessage(msg);      
            return null;
        }    
        return new PageReference(returnURL);
    }
    
    private void InitPropagateToItemList(){
        String query = Util.queryBuilder('Subsidiary_Account__c',new List<String>{'id','Account_Name__c','Account__c','Subsidiary_Account__c.Name' },
                                         new List<string>{'Contract__c'+'=\''+currentObj.get(linkField)+'\''});
        List<Sobject> itemList = Database.query(query);
        for(Sobject item : itemList) {
              displayItems.add(new ItemSelect(item,false));
         }
    }
    
    public class ItemSelect{
        public sObject obj {get; set;}
        public boolean selected {get; set;}
        public ItemSelect(sObject obj,boolean sel) {
            this.obj = obj;
            selected = sel;
        }
    }
     
}