@isTest
global class BusinessCreditProfileMgmtServiceMock implements WebServiceMock
{
    global void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction,
                         String requestName, String responseNS, String responseName, String responseType)
	{
        if ( 'searchIndivdualCreditProfile'.equalsIgnoreCase(requestName))
        {
            response.put('response_x', buildSearchResponse());
        }
        else if ( 'createIndividualCustomerWithCreditWorthiness'.equalsIgnoreCase(requestName) )
        {
            response.put('response_x', buildCreateResponse());
        }
        else if ( 'ping'.equalsIgnoreCase(requestName) )
        {
            response.put('response_x', buildPingResponse());
        }
	}

    private CreditProfileServiceRequestResponse.searchIndivdualCreditProfileResponse_element buildSearchResponse()
    {
        CreditProfileServiceRequestResponse.searchIndivdualCreditProfileResponse_element responseElement = new CreditProfileServiceRequestResponse.searchIndivdualCreditProfileResponse_element();

        responseElement.individualProfileSearchResultList = buildIndividualProfile();

        return responseElement;
    }

    private List<CreditProfileServiceTypes.IndividualProfile> buildIndividualProfile()
    {
        CreditProfileServiceTypes.IndividualProfile[] profiles = new List<CreditProfileServiceTypes.IndividualProfile>();

        CreditProfileServiceTypes.IndividualProfile profile = new CreditProfileServiceTypes.IndividualProfile();

        CreditProfileServiceTypes.IndividualCustomerProfile individualCustomerProfile = new CreditProfileServiceTypes.IndividualCustomerProfile();

        CreditProfileServiceTypes.IndividualCreditProfile individualCreditProfile = new CreditProfileServiceTypes.IndividualCreditProfile();

        individualCreditProfile.individualCreditProfileId = 1;

        individualCustomerProfile.firstName = 'Mock';
        individualCustomerProfile.middleName = null;
        individualCustomerProfile.lastName = 'Test';
        individualCustomerProfile.individualCustomerId = 1;

        individualCreditProfile.creditAddress = new CreditProfileServiceTypes.CreditCheckAddress();
        individualCreditProfile.creditAddress.addressLineOneTxt = '3777 Kingsway';
        individualCreditProfile.creditAddress.cityName = 'Burnaby';
        individualCreditProfile.creditAddress.provinceCd = 'BC';
        individualCreditProfile.creditAddress.postalCd = 'V5H 3Z7';
        individualCreditProfile.creditAddress.countryCd = 'CAN';

        individualCreditProfile.personalInfo = new CreditProfileServiceTypes.IndividualPersonalInfo();

        individualCreditProfile.personalInfo.birthdate = null;
        individualCreditProfile.personalInfo.creditCheckConsentCd = 'N';

        CreditProfileServiceTypes.IndividualCreditIdentification creditIdentification = new CreditProfileServiceTypes.IndividualCreditIdentification();
        creditIdentification.sin = '220380919';

        creditIdentification.driverLicense = new CreditProfileServiceTypes.DriverLicense();
        creditIdentification.driverLicense.driverLicenseNum = 'MockDL123456';
        creditIdentification.driverLicense.provinceCd = 'BC';

        creditIdentification.provincialIdCard = new CreditProfileServiceTypes.ProvincialIdCard();
        creditIdentification.provincialIdCard.provincialIdNum = 'MockProv123456';
        creditIdentification.provincialIdCard.provinceCd = 'BC';

        creditIdentification.passport = new CreditProfileServiceTypes.Passport();
        creditIdentification.passport.passportNum = 'MockP123456';
        creditIdentification.passport.countryCd = 'CAN';

        creditIdentification.healthCard = new CreditProfileServiceTypes.HealthCard();
        creditIdentification.healthCard.healthCardNum = 'MockH123456';

        individualCreditProfile.creditIdentification = creditIdentification;

        profile.individualCustomerProfile = individualCustomerProfile;
        profile.individualCreditProfile = individualCreditProfile;

        profiles.add(profile);

        return profiles;
    }

    private CreditProfileServiceRequestResponse.createIndividualCustomerWithCreditWorthinessResponse_element buildCreateResponse()
    {
        CreditProfileServiceRequestResponse.createIndividualCustomerWithCreditWorthinessResponse_element responseElement = new CreditProfileServiceRequestResponse.createIndividualCustomerWithCreditWorthinessResponse_element();

        responseElement.individualCustomerId = 7;

        return responseElement;
    }

    private CreditProfileServicePing.pingResponse_element buildPingResponse()
    {
        CreditProfileServicePing.pingResponse_element pingResponse = new CreditProfileServicePing.pingResponse_element();

        pingResponse.version = 'Mock-1.0';

        return pingResponse;
    }
}