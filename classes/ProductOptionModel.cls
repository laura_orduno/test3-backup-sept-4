public class ProductOptionModel {
	public Integer level {get{return configuredProduct.level;}}
	public ProductVO.Option vo {get; set;}
	public ProductModel configuredProduct {get; private set;}
	public ProductModel optionalProduct {get; private set;}
	public ProductFeatureModel feature {get; private set;}
	public Map<String,String> externalConfigurationData {get; private set;}
	
	
	public Decimal configuredAdditionalDiscountAmount {get; set;}
	public Decimal configuredAdditionalDiscountRate {get; set;}
	public Decimal configuredPartnerDiscountRate {get; set;}
	public Date configuredEndDate {get; set;}
	
	
	// Convenience properties
	public Id id {get{return vo.record.Id;}}
	public Boolean quantityEditable {get{return isQuantityEditable();}}
	public Boolean priceEditable {get{return isPriceEditable();}}
	public Boolean nested {get{return (optionalProduct != null) && !vo.product.options.isEmpty();}}
	public Boolean required {get{return vo.record.SBQQ__Required__c;}}
	public Boolean refreshRequired {get{return isRefreshRequired();}}
	public String productName {get{return (vo == null) ? '' : vo.record.SBQQ__ProductName__c;}}
	public Id optionalProductId {get{return vo.record.SBQQ__OptionalSKU__c;}}
	public Boolean configurable {get{return vo.isConfigurationAllowed() || configurationRequired;}}
	public Boolean configurationRequired {get{return vo.isConfigurationRequired();}}
	public Boolean configurableOption {get{return true;}}
	
	/**
	 * Default constructor.
	 */
	public ProductOptionModel(ProductModel configuredProduct, ProductFeatureModel feature, ProductVO.Option vo) {
		System.assert(vo != null, 'ProductOptionModel: vo argument is null: ' + vo.record.Id);
		System.assert(feature != null, 'ProductOptionModel: feature argument is null' + vo.record.Id);
		
		this.vo = vo;
		this.feature = feature;
		this.configuredProduct = configuredProduct;
		if (vo.product != null) {
			optionalProduct = new ProductModel(vo.product, configuredProduct.level+1);
		}
		
		externalConfigurationData = new Map<String,String>();
	}
	
	/**
	 * Returns the title to use for Configure Option dialog.
	 */
	public String getConfigureDialogTitle() {
		String title = 'Configure Option';
		Schema.SObjectField titleField = MetaDataUtils.getField(SBQQ__ProductOption__c.sObjectType, 'ConfigureDialogTitle__c');
		if ((titleField != null) && (vo.record.get(titleField) != null)) {
			title = String.valueOf(vo.record.get(titleField));
		}
		return title;
	}
	
	
	public SBQQ__ProductOption__c mergeWithSelection(SBQQ__ProductOption__c edited) {
		SBQQ__ProductOption__c result = vo.record.clone(false, false);
		result.SBQQ__Quantity__c = edited.SBQQ__Quantity__c;
		result.SBQQ__UnitPrice__c = edited.SBQQ__UnitPrice__c;
		return result;
	}
	
	public Boolean isRefreshRequired() {
		return false;
	}
	
	public Boolean isSuggested() {
		return (vo.record.SBQQ__Feature__c == null) && (level == 0) && (!vo.record.SBQQ__Required__c) && (!vo.record.SBQQ__Bundled__c);
	}
	
	public Boolean isQuantityEditable() {
		return (vo.record.SBQQ__Quantity__c == null) && (vo.record.SBQQ__Bundled__c == false);
	}
	
	public Boolean isPriceEditable() {
		return (vo.record.SBQQ__PriceEditable__c.equalsIgnoreCase('yes') && (vo.record.SBQQ__Bundled__c == false));
	}
	
	public void changeOptionalProduct(ProductVO product) {
		optionalProduct = new ProductModel(product, configuredProduct.level+1);
	}
}