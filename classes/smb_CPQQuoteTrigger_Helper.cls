/*
###########################################################################
# File..................: smb_CPQQuoteTrigger_Helper
# Version...............: 1
# Created by............: Bharat Vikram chand
# Created Date..........: 19-Sept-2013
# Last Modified by......: Pallavi Mittal
# Last Modified Date....: 14-Feb-2014
# Description...........: This Helper class contains the trigger methods on CPQ Quote methods
############################################################################
*/

public without sharing class smb_CPQQuoteTrigger_Helper {
    
    public static void updateOpportunityFieldsAndStatus(list<scpq__SciQuote__c> lstQuote, Boolean isUpdate, map<Id,scpq__SciQuote__c> mapOldQuote){
            
        // This will set all the requisite Opportunity fields from Quote Object field values    
        Map<Id,Opportunity> mapOPPUpdate=updateOpportunityFields(lstQuote);
        
        list<scpq__SciQuote__c> lstValidQuotes = [Select Id, scpq__Status__c,scpq__Primary__c,scpq__OpportunityId__c,(Select Id From Order_Requests__r) From scpq__SciQuote__c where Id IN:lstQuote and scpq__Primary__c = true];
        map<Id,String> mapOpportunityStatus = new map<Id, String>();
        set<Id> setOppIds = new set<Id>();
        list<Opportunity> lstOpportunity = new list<Opportunity>();
        if(lstValidQuotes.size()>0){
            for(Integer i = 0; i < lstValidQuotes.size(); i++){
                if(lstValidQuotes[i].Order_Requests__r != null && lstValidQuotes[i].Order_Requests__r.size() > 0 ){
                    lstValidQuotes.remove(i);
                }
            }
        }
        
        if(lstValidQuotes.size() > 0){
            for(scpq__SciQuote__c objQuote: lstValidQuotes){
                
                    if(isUpdate){
                        if(objQuote.scpq__Status__c != mapOldQuote.get(objQuote.Id).scpq__Status__c){
                            setOppIds.add(objQuote.scpq__OpportunityId__c);
                            if(!mapOpportunityStatus.containsKey(objQuote.scpq__OpportunityId__c)){
                                if(objQuote.scpq__Status__c == 'Pending Service Address Validation'){
                                    mapOpportunityStatus.put(objQuote.scpq__OpportunityId__c,'Quote Pending Address Validation');
                                }else{
                                    mapOpportunityStatus.put(objQuote.scpq__OpportunityId__c,objQuote.scpq__Status__c);
                                }
                            }
                        }
                    }
                    else{
                        setOppIds.add(objQuote.scpq__OpportunityId__c);
                        if(!mapOpportunityStatus.containsKey(objQuote.scpq__OpportunityId__c)){
                            if(objQuote.scpq__Status__c == 'Pending Service Address Validation'){
                                    mapOpportunityStatus.put(objQuote.scpq__OpportunityId__c,'Quote Pending Address Validation');
                                }else{
                                    mapOpportunityStatus.put(objQuote.scpq__OpportunityId__c,objQuote.scpq__Status__c);
                                }
                            }
                    }
                
            }
            
            if(setOppIds.size()>0){
                RecordType objRecTypeOrder = [Select Id from RecordType where sObjectType = 'Opportunity' and isActive = true and DeveloperName = 'SMB_Care_Amend_Order_Opportunity' limit 1];
                lstOpportunity = [Select Id, StageName, recordTypeId from Opportunity where Id IN :setOppIds];
                if(lstOpportunity.size()>0){
                    for(Opportunity objOpp:lstOpportunity){
                    	if(!objOpp.StageName.equalsignorecase('Order Request Cancelled'))
                    	{
                         if(!((objOpp.StageName == 'Contract On Hold (Customer)' || 
                         objOpp.StageName == 'Order Request on Hold (Customer)') && 
                         objOpp.RecordTypeId == objRecTypeOrder.Id)){
                       
                            if(mapOpportunityStatus.containsKey(objOpp.Id)){
                                if(mapOpportunityStatus.get(objOpp.Id).startsWith('Quote')){
                                    //objOpp.StageName =  mapOpportunityStatus.get(objOpp.Id);
                                    mapOPPUpdate.get(objOpp.Id).StageName=mapOpportunityStatus.get(objOpp.Id);
                                    /*
									* Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
									* START
									*/
										mapOPPUpdate.get(objOpp.Id).Stage_update_time__c=System.Now();
									/*
									* Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
									* END
									*/
                                }
                                else{
                                    //objOpp.StageName =  'Quote ' + mapOpportunityStatus.get(objOpp.Id);
                                    mapOPPUpdate.get(objOpp.Id).StageName='Quote ' + mapOpportunityStatus.get(objOpp.Id);
                                    /*
									* Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
									* START
									*/
										mapOPPUpdate.get(objOpp.Id).Stage_update_time__c=System.Now();
									/*
									* Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
									* END
									*/
                                }
                            }
                         }
                 
                    	}
                    }
                    
                    //update lstOpportunity;
                    update mapOPPUpdate.values();
                }
            }
        }
    } 

    /* This method helps to copy CPQ Quote fields on Opportunity*/
    private static Map<Id,Opportunity> updateOpportunityFields(List<scpq__SciQuote__c> quoteList){

        List<Opportunity> oppList = new List<Opportunity>();
        Map<Id,Opportunity> mapOpp=new Map<Id,Opportunity>();
        System.debug('updateOpportunityFields-->');
        for(scpq__SciQuote__c cpqQuote : quoteList){
            if(cpqQuote.scpq__Primary__c && cpqQuote.scpq__OpportunityId__c!=null){
                Opportunity obj = new Opportunity(Id = cpqQuote.scpq__OpportunityId__c);
 				// BRW 02-26-2015   obj.Notes__c = cpqQuote.Notes__c;
                obj.Offer_Date__c = cpqQuote.Offer_Date__c;
                obj.Order_Delivery_Preference__c = cpqQuote.Order_Delivery_Preference__c;
                obj.Priority__c = cpqQuote.Priority__c;
                obj.Quote_Expiration_Date__c = cpqQuote.Quote_Expiration_Date__c;
                obj.Order_Header_Key__c =   cpqQuote.scpq__OrderHeaderKey__c;
                obj.Quote_Id__c = cpqQuote.scpq__QuoteId__c ;
                obj.Status__c = cpqQuote.scpq__Status__c;
                obj.Quote_Number__c =   cpqQuote.smb_QuoteNumber__c;
                obj.smb_Type__c = cpqQuote.smb_Type__c;
                obj.Primary__c = cpqQuote.scpq__Primary__c;
                obj.Total_Monthly_Recurring_Charges__c = cpqQuote.Total_Monthly_Recurring_Charges__c;
                obj.Total_Non_Recurring_Charges__c = cpqQuote.Total_Non_Recurring_Charges__c;
                obj.Total_Original_Monthly_Recurring_Charges__c = cpqQuote.Total_Original_Monthly_Recurring_Charges__c; 
                obj.Total_Original_Non_Recurring_Charges__c = cpqQuote.Total_Original_Non_Recurring_Charges__c;
                obj.Discount__c = cpqQuote.scpq__TotalDiscountPercent__c;
                if(cpqQuote.scpq__SciLastModifiedDate__c!= null){
                    obj.Last_Modified_Timestamp__c = String.valueOf(cpqQuote.scpq__SciLastModifiedDate__c);
                }
                obj.Value_After_Discounts__c = cpqQuote.scpq__QuoteValueAfterDiscounts__c;
                obj.Value_Before_Discounts__c = cpqQuote.scpq__QuoteValueBeforeDiscounts__c;
                //Change by Puneet Khosla - Code Starts
                obj.Printed_Total_MRC__c = cpqQuote.Printed_Total_MRC__c;
                obj.Printed_Total_NRC__c = cpqQuote.Printed_Total_NRC__c;
                //obj.Printed_Agreement_Type__c = cpqQuote.Printed_Agreement_Type__c;
                //obj.Printed_Service_Address__c = cpqQuote.Printed_Service_Address__c;
                //Change by Puneet Khosla - Code Ends
                oppList.add(obj);
                mapOpp.put(obj.id,obj);
            }
        }
        return mapOpp;
    }
}