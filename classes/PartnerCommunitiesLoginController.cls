global without sharing class PartnerCommunitiesLoginController {
    
    private class PartnerUserException extends Exception{}
    
    private static final String DUMMY_ORG_CODE = '00000';
    
    public String siteBaseUrl {get;set;}
    public String cacheKey {get;set;}
    public String roleId {get;set;}
  //  public String salesRepJson {get;set;}
    //public String outletJson {get;set;}
    public String respStr {get;set;}
    public List<ChannelOrgOutletsJSONExplicit> channelOrgOutletsJsonObjList {get;set;}
    public List<ChannelOrgSalesRepsJSONExplicit> channelOrgSalesRepJsonObjList {get;set;}
    public List<String> channelOrgOutletsStrList {get;set;}
    public List<String> channelOrgSalesRepStrList {get;set;}
    public Map<String,String> keyValMap {get;set;}
    public Map<String,String> channelOrgOutletsStrMap {get;set;}
    public Map<String,String> channelOrgSalesRepStrMap {get;set;}
    public Set<String> keySet1 {get;set;}
    public Set<String> keySet2 {get;set;}
   /* public List<MyData> channelOrgOutletsStrData {get;set;}
    public List<MyData> channelOrgSalesRepStrData {get;set;}
    public class MyData{
        public String key{get;set;}
        public String value{get;set;}
    }*/
    global PartnerCommunitiesLoginController () {
        System.debug(' PartnerCommunitiesLoginController constructor called');
    }
    
    // Code we will invoke on page load.
    global PageReference forwardToLandingPage() {
        cacheKey = System.currentPageReference().getParameters().get('key');
        System.debug('cacheKey=' + cacheKey);
        //roleId = System.currentPageReference().getParameters().get('roleId');
        roleId = UserInfo.getUserRoleId(); //testing
        System.debug('roleId=' + roleId);
        siteBaseUrl = Site.getBaseUrl();                
        System.debug('baseUrl=' + siteBaseUrl);
        //temp
        //cacheKey=null;
        if(String.isNotBlank(cacheKey)) { 
            cacheKey = cacheKey.trim();
            ChannelPortalCacheMgmtSrvReqRes.GetCacheListResponseType resp = ChannelPortalCacheMgmtWSCallout.getCacheList(cacheKey);
            
            ChannelPortalCacheMgmtSrvReqRes.MapItem[] customCacheList = resp.customCacheList;
            
            keyValMap = new Map<String,String>();
            for(ChannelPortalCacheMgmtSrvReqRes.MapItem mapItemObj:customCacheList){
                if(String.isNotBlank(mapItemObj.key) && 'channelOrgSalesReps_JSON'.equalsIgnoreCase(mapItemObj.key)){
                    continue;
                }
                if(String.isNotBlank(mapItemObj.key) && 'channelOrgOutlets_JSON'.equalsIgnoreCase(mapItemObj.key)){
                    continue;
                }
                keyValMap.put(mapItemObj.key, mapItemObj.value); 
            }
            
            respStr = JSON.serialize(resp);
            String cmsKey = 'local.orderingCache.KEY' + keyValMap.get('loginUserID');
            System.debug('PartnerCommunitiesLoginController cmsKey= '+ cmsKey);
            Object ob1 = Cache.Org.get(cmsKey);
            System.debug('DATA FROM LoginController '+JSON.serialize(ob1));
            ChannelPortalCacheMgmtData dataObj = (ChannelPortalCacheMgmtData)Cache.Org.get(cmsKey);
            channelOrgSalesRepJsonObjList = dataObj.channelOrgSalesRepJsonObjList;
            channelOrgSalesRepStrList = new List<String>();
            channelOrgOutletsStrMap = new Map<String,String>();
            channelOrgSalesRepStrMap = new Map<String,String>();
            if(channelOrgSalesRepJsonObjList != null){
                for(ChannelOrgSalesRepsJSONExplicit ob:channelOrgSalesRepJsonObjList){
                    channelOrgSalesRepStrList.add('firstName:'+ob.firstName);
                    channelOrgSalesRepStrList.add('lastName:'+ob.lastName);
                    channelOrgSalesRepStrList.add('salesRepPin:'+ob.salesRepPin);
                    channelOrgSalesRepStrList.add('salesRepCategoryKeys:'+ob.salesRepCategoryKeys);
                    channelOrgSalesRepStrMap.put('firstName',ob.firstName);
                    channelOrgSalesRepStrMap.put('lastName',ob.lastName);
                    channelOrgSalesRepStrMap.put('salesRepPin',ob.salesRepPin);
                    //channelOrgSalesRepStrMap.put('salesRepCategoryKeys',ob.salesRepCategoryKeys);                    
                }
                keySet1 = channelOrgSalesRepStrMap.keySet();
            }
            
            channelOrgOutletsStrList = new List<String>();
            channelOrgOutletsJsonObjList = dataObj.channelOrgOutletsJsonObjList;
            if(channelOrgOutletsJsonObjList != null){
                for(ChannelOrgOutletsJSONExplicit ob:channelOrgOutletsJsonObjList){
                    channelOrgOutletsStrList.add('currentChannelOutletID:'+ob.currentChannelOutletID);
                    channelOrgOutletsStrList.add('currentChannelOutletDescription:'+ob.currentChannelOutletDescription);
                    channelOrgOutletsStrList.add('currentOutletCategoryKeys:'+ob.currentOutletCategoryKeys);
                    channelOrgOutletsStrMap.put('currentChannelOutletID',ob.currentChannelOutletID);
                    channelOrgOutletsStrMap.put('currentChannelOutletDescription',ob.currentChannelOutletDescription);
                    //channelOrgOutletsStrMap.put('currentOutletCategoryKeys',ob.currentOutletCategoryKeys);
                }
                if(null != channelOrgOutletsStrMap){
                    keySet2 = channelOrgOutletsStrMap.keySet();
                }
            }
        }
 
        updatePartnerUser();
 
        
        String landingPageUrl = Site.getPathPrefix() + '/home/home.jsp';
        return Network.forwardToAuthPage(landingPageUrl);
        //return null;
    }
    
    private void updatePartnerUser() {
        if(keyValMap==null){
            return;
        }
        String uuid = keyValMap.get('loginUserID');
        //String uuid = 'testdldpa';
        if (String.isNotBlank(uuid)) {
            User user = [SELECT Id, ContactId, Contact.Account.Channel_Org_Code__c FROM User WHERE FederationIdentifier = :uuid];
            
            String currentChannelOrgCode = user.Contact.Account.Channel_Org_Code__c;
            if(Test.isRunningTest()){
                currentChannelOrgCode='testV';
            }
            //if (String.isNotBlank(currentChannelOrgCode) && DUMMY_ORG_CODE.equals(currentChannelOrgCode)) {
            if (String.isNotBlank(currentChannelOrgCode)) {
                String userChannelOrgCode = keyValMap.get('channelOrgCode');
                //String userChannelOrgCode = '00000';
                List<Account> accounts = [SELECT Id, Name FROM Account WHERE Channel_Org_Code__c = :userChannelOrgCode];
                If (accounts.size() > 0) {
	
                    updateContact(keyValMap, user.ContactId, accounts[0].Id);

                    if (String.isBlank(roleId)) {
                        String userRole = accounts[0].Name + ' Partner User';
                        String portalRoleName = keyValMap.get('portalRoleName');
                        if (String.isNotBlank(portalRoleName)) {
                            if (portalRoleName == 'Dealer Principal') {
                                userRole = accounts[0].Name + ' Partner Executive';
                            }
                            else if (portalRoleName == 'Administration Personnel') {
                                userRole = accounts[0].Name + ' Partner Manager';
                            }
                        }
		                
                        updateUser(siteBaseUrl, keyValMap, uuid, userRole);
                    } 
                    else {
                        updateRole(uuid, roleId);
                    }
                } 
                else {
                    throw new PartnerUserException('Unable to find the Partner Account (Org Code = ' + userChannelOrgCode + ') to associate user ' + uuid);
                }

            }
        }

    }
    @TestVisible
    private static void updateContact(Map<String,String> keyValMap, String contactId, String accountId) {
       List<Contact> contactList = [SELECT Id, AccountId, Account.Channel_Org_Code__c, FirstName, LastName, Email, Title, WBS_Language_Preference__c FROM Contact where Id = :contactId]; 
        if(contactList==null || contactList.size()==0){return;}
        Contact contact=contactList.get(0);
        contact.AccountId = accountId;
        if (String.isNotBlank(keyValMap.get('firstName'))) contact.FirstName = keyValMap.get('firstName');
        if (String.isNotBlank(keyValMap.get('lastName'))) contact.LastName = keyValMap.get('lastName');
        if (String.isNotBlank(keyValMap.get('salesRepBusinessEmail'))) contact.Email = keyValMap.get('salesRepBusinessEmail');
        if (String.isNotBlank(keyValMap.get('portalRoleName'))) contact.Title = keyValMap.get('portalRoleName');
        if(String.isNotBlank(keyValMap.get('salesRepPreferredContactLanguageCode'))) {
            String language = keyValMap.get('salesRepPreferredContactLanguageCode');
            if(language.equalsIgnoreCase('FR')){
                contact.WBS_Language_Preference__c = 'French';
            } else {
                contact.WBS_Language_Preference__c = 'English';
            }
        } else {
            contact.WBS_Language_Preference__c = 'English';
        }
        update(contact);
    }
    
   //@future as workaround for exception below
    //MIXED_DML_OPERATION, DML operation on setup object is not permitted after you have updated a non-setup object (or vice versa): Contact, original object: User
    @TestVisible
    @Future(callout=true)
    private static void updateUser(String baseUrl, Map<String,String> keyValMap, String uuid, String userRole) {
        List<UserRole> roles = [SELECT Id, Name FROM UserRole where Name = :userRole];
        if (roles.size() > 0 && !Test.isRunningTest()) {
            String remoteURL = baseUrl + '/PartnerCommunitiesLogin?roleId=' + roles[0].Id;
            system.debug('remoteURL = ' + remoteURL);
            
            HttpRequest httpReq = new HttpRequest();
            httpReq.setMethod('POST');
            httpReq.setEndpoint(remoteURL);
            
            HttpResponse HttpResp = new Http().send(httpReq);
            system.debug('HttpResp.getBody() = ' + HttpResp.getBody());
        }
        
        User user = [SELECT Id, UserName, FirstName, LastName, Email, Sales_Rep_ID__c, UserRoleId, Title, LanguageLocaleKey, LocaleSidKey FROM User WHERE FederationIdentifier = :uuid];        
        if (String.isNotBlank(keyValMap.get('firstName'))) user.FirstName = keyValMap.get('firstName');
        if (String.isNotBlank(keyValMap.get('lastName'))) user.LastName = keyValMap.get('lastName');
        if (String.isNotBlank(keyValMap.get('salesRepBusinessEmail'))) user.Email = keyValMap.get('salesRepBusinessEmail');
        if (String.isNotBlank(keyValMap.get('salesRepPIN'))) user.Sales_Rep_ID__c = keyValMap.get('salesRepPIN');
        if (String.isNotBlank(keyValMap.get('portalRoleName'))) user.Title = keyValMap.get('portalRoleName');
        if(String.isNotBlank(keyValMap.get('salesRepPreferredContactLanguageCode'))) {
            String language = keyValMap.get('salesRepPreferredContactLanguageCode');
            if(language.equalsIgnoreCase('FR')){
                user.LanguageLocaleKey = 'fr';
                user.LocaleSidKey = 'fr_CA';
            } else {
                user.LanguageLocaleKey = 'en_US';
                user.LocaleSidKey = 'en_CA';
            }
        } else {
            user.LanguageLocaleKey = 'en_US';
            user.LocaleSidKey = 'en_CA';
        }
        update(user);
        
    }
    @TestVisible
    private void updateRole(String uuid, String roleId) {
        system.debug('updating user role to -> ' + roleId);
        User user = [SELECT Id, UserRoleId FROM User WHERE FederationIdentifier = :uuid];
        user.UserRoleId = roleId;
        update(user);
    }    
}