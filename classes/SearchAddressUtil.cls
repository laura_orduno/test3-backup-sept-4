public without sharing class SearchAddressUtil {
	
	public Id accountId {get; private set;}
	public Integer maxRecords {get; private set;}

	//fields that need to be retrieved
	public static final List<String> addressFields = new List<String> {
		'Id', 'Name', 'Street__c', 'City__c', 'State_Province__c', 'Country__c', 
		'Postal_Code__c', 'Building_Name__c', 'Floor__c', 'Suite_Unit__c', 
		'Street_Type__c', 'Street_Direction__c'
		};
	
	//account field		
	public static final Map<String, String> ADDRESS_ACCOUNT_MAP = new Map<String, String> { 'Street__c' => 'BillingStreet',
		'City__c' => 'BillingCity', 'State_Province__c' => 'BillingState', 'Country__c' => 'BillingCountry',
		'Postal_Code__c' => 'BillingPostalCode',
		'Building_Name__c' => '', 'Floor__c' => '', 'Suite_Unit__c' => '', 
		'Street_Type__c' => '', 'Street_Direction__c' => ''
		}; 

	//ban fields
	public static final Map<String, String> ADDRESS_BAN_MAP = new Map<String, String> { 'Street__c' => 'BADDR_ADDRESS_TXT__c',
		'City__c' => 'BADDR_CITY_NM__c', 'State_Province__c' => 'BADDR_PROV_CD__c', 'Country__c' => 'Country__c',
		'Postal_Code__c' => 'BADDR_POSTAL_CD__c',
		'Building_Name__c' => '', 'Floor__c' => '', 'Suite_Unit__c' => '', 
		'Street_Type__c' => '', 'Street_Direction__c' => ''
		}; 

	//cbucid fields
	public static final Map<String, String> ADDRESS_CBUCID_MAP = new Map<String, String> { 
		'Id' => 'CBUCID_Name__c',
		'Name' => 'CBUCID_Name__r.Name',
		'Street__c' => 'CBUCID_Name__r.Street__c',
		'City__c' => 'CBUCID_Name__r.City__c',
		'State_Province__c' => 'CBUCID_Name__r.State_Province__c', 
		'Country__c' => 'CBUCID_Name__r.Country__c',
		'Postal_Code__c' => 'CBUCID_Name__r.Postal_Code__c',
		'Building_Name__c' => '', 'Floor__c' => '', 'Suite_Unit__c' => '', 
		'Street_Type__c' => '', 'Street_Direction__c' => ''
		};

	public static final Map<String, Map<String, String>> OBJECT_MAP = new Map<String, Map<String, String>> {
		'BillingAccount' => ADDRESS_ACCOUNT_MAP, 'Billing_Account__c' => ADDRESS_BAN_MAP,
		'Cust_Business_Unit_Cust_ID__c' => ADDRESS_CBUCID_MAP, 'Address__c' => null
	};//
	
	//friendly object type name to be displayed in the search result
	public static final Map<String, String> OBJECT_DISPLAY_MAP = new Map<String, String> {
		'BillingAccount' => 'Account Address', 'Billing_Account__c' => 'BAN Address',
		'Cust_Business_Unit_Cust_ID__c' => 'DUNS Address', 'Address__c' => 'Custom Address'
	};//

	public static final List<String> objs = new List<String> {'Cust_Business_Unit_Cust_ID__c', 'BillingAccount',
		'Billing_Account__c', 'Address__c'};

	//this layer introduced so that we could have maps for Account.Billing / Account.Shipping
	public static final Map<String, String> OBJECT_NAME_MAP = new Map<String, String> {
		'BillingAccount' => 'Account', 'Billing_Account__c' => 'Billing_Account__c',
		'Cust_Business_Unit_Cust_ID__c' => 'Account', 'Address__c' => 'Address__c'
	};
	
	//constraint on the account object
	public static final Map<String, String> ACCOUNT_CONSTRAINT_MAP = new Map<String, String> {
		'Account' => 'Id', 'Billing_Account__c' => 'Account__c',
		'Cust_Business_Unit_Cust_ID__c' => 'Id', 'Address__c' => 'Account__c'
	};	
	public SearchAddressUtil() {
	}
	
	
	public SearchAddressUtil(Id accountId, Integer maxRecords) {
		this.accountId = accountId;
		this.maxRecords = maxRecords;
	}

		
	public List<AddressWrapper> getAddresses(SObject addressObj) {
		List<AddressWrapper> addresses = new List<AddressWrapper>();
		
		Integer key = 0;
		
		for (Integer i = 0; i < objs.size(); i++) {
			String objectName = objs.get(i);
			System.debug('objectName = ' + objectName);
			String query = buildQuery(addressObj, OBJECT_NAME_MAP.get(objectName), SearchAddressUtil.addressFields, 
				OBJECT_MAP.get(objectName), (this.maxRecords - key));
			System.debug('query = ' + query);
				
			for (SObject sobj : Database.query(query)) {
				String sKey = String.valueOf(key);
				AddressWrapper wrapper = new AddressWrapper(sobj, objectName, sKey);
				if (wrapper.isEmpty()) continue;
				addresses.add(wrapper);
				key++;
			}
		}
		return addresses;
		
	} 

	public String buildQuery(SObject addressObj, String sObjectName, List<String> fields, Map<String, String> mapping, Integer recordsLimit ) {
		String searchCriteria = ACCOUNT_CONSTRAINT_MAP.get(sObjectName) + ' = \'' + this.accountId + '\'';
		List<String> fieldsToSelect = new List<String>();
		for (String field : fields) {
			String mappedField = field;
			if (mapping != null) {
				mappedField = (mapping.get(field) == null) ? field : mapping.get(field);
			}
			if (! Util.isBlank(mappedField) ) {
				fieldsToSelect.add(mappedField);
			}
			//System.debug('field = ' + field);
			if (! Util.isBlank((String) addressObj.get(field) ) && (! Util.isBlank(mappedField))) {
				if (! Util.isBlank(searchCriteria)) searchCriteria += ' And';
				searchCriteria += ' ' + mappedField + ' like \'%' + addressObj.get(field) + '%\'';
			}
		
		}
		System.debug('searchCriteria = ' + searchCriteria);
		if (! Util.isBlank(searchCriteria)) searchCriteria = ' where ' + searchCriteria;
		//List<String> fieldsToSelect = (mapping == null) ? addressFields : mapping.values();
		String query = 'Select ' + Util.join(fieldsToSelect, ',', null) + ' from ' + sObjectName + searchCriteria + ' limit ' + recordsLimit;
		return query; 
	}


	testMethod static void unit() {
		Account a = new Account(Name = 'TestAccount', BillingStreet = '1 Market', BillingCity = 'SFO', BillingState = 'CA', 
			BillingCountry = 'USA', BillingPostalCode = '98745');
		insert a;
		a = [Select Id, Name, BillingStreet, BillingCity, BillingState, BillingCountry, BillingPostalCode from Account where Id = :a.Id];

		SearchAddressUtil searchUtil = new SearchAddressUtil(a.Id, SelectAddressController.MAX_NO_OF_RECORDS);

		Address__c address = new Address__c(Street__c = '1 Market', City__c = 'SFO', State_Province__c = 'CA', 
			Country__c = 'USA', Postal_Code__c = '98745');
		String query = searchUtil.buildQuery(address, 'Account', addressFields, ADDRESS_ACCOUNT_MAP, 200); 
		System.debug('query = ' + query);
		System.debug(Database.query(query));
		
		query = searchUtil.buildQuery(address, 'Billing_Account__c', addressFields, ADDRESS_BAN_MAP, 200); 
		System.debug('query = ' + query);
		System.debug(Database.query(query));

		query = searchUtil.buildQuery(address, 'Account', 
			SearchAddressUtil.addressFields, ADDRESS_CBUCID_MAP, 200);
		System.debug('query = ' + query);
		System.debug(Database.query(query));

	}
	
	testMethod static void unit1() {
		SearchAddressUtil searchUtil = new SearchAddressUtil('00130000003oS6z', 5);
		Address__c address = new Address__c(City__c='Windsor');
		List<AddressWrapper> w = searchUtil.getAddresses(address);
		for (AddressWrapper a : w) {
			System.debug('a = ' + a);
		}
		
	}
}

/*
UI Page - 

loop on List<AddressWrapper>


link click action:

we will get Id of object
need to get that from either iterating thru wrapper or have all data in a Map<Id, SObject>; 
*/