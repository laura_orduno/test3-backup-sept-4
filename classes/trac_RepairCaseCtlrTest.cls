@isTest
private class trac_RepairCaseCtlrTest {
	private static testMethod void testCtor() {
		Case c = new Case(subject = 'test');
		insert c;
		
		ApexPages.currentPage().getParameters().put('Id',c.id);
		
		trac_RepairCaseCtlr ctlr = new trac_RepairCaseCtlr();
		
		system.assertEquals(c.id,ctlr.theCase.id);
		system.assertEquals(false,ctlr.closed);
	}
	
	
	private static testMethod void testCtorClosedCase() {
		Case c = new Case(subject = 'test', status = 'Closed');
		insert c;
		
		ApexPages.currentPage().getParameters().put('Id',c.id);
		
		trac_RepairCaseCtlr ctlr = new trac_RepairCaseCtlr();
		
		system.assert(ctlr.closed);
		system.assert(ctlr.complete);
		system.assertEquals(false, ctlr.success);
		system.assert(ApexPages.hasMessages(ApexPages.Severity.ERROR));
		system.assertEquals(Label.REPAIR_CLOSED_NO_REOPEN, ApexPages.getMessages()[0].getSummary());
	}
	
	
	private static testMethod void testCtorMissingId() {		
		Boolean caughtException = false;
		try {
			trac_RepairCaseCtlr ctlr = new trac_RepairCaseCtlr();
		} catch (Exception e) {
			caughtException = true;
			system.assertEquals(Label.NO_CASE_ID, e.getMessage());
		}

		system.assert(caughtException);
	}
	
	
	private static testMethod void testCtorInvalidId() {		
		ApexPages.currentPage().getParameters().put('Id','X000000000000000000');
		
		Boolean caughtException = false;
		try {
			trac_RepairCaseCtlr ctlr = new trac_RepairCaseCtlr();
		} catch (Exception e) {
			caughtException = true;
			system.assertEquals(Label.INVALID_CASE_ID, e.getMessage());
		}

		system.assert(caughtException);
	}
	
	
	private static testMethod void testCtorInvalidCaseId() {		
		ApexPages.currentPage().getParameters().put('Id','5000000000000000000');
		
		Boolean caughtException = false;
		try {
			trac_RepairCaseCtlr ctlr = new trac_RepairCaseCtlr();
		} catch (Exception e) {
			caughtException = true;
			system.assertEquals(Label.INVALID_CASE_ID, e.getMessage());
		}

		system.assert(caughtException);
	}
	
	
	private static testMethod void testCtorUnmatchedCaseId() {
		ApexPages.currentPage().getParameters().put('Id','500f0000002xyJmAAI');
		
		Boolean caughtException = false;
		try {
			trac_RepairCaseCtlr ctlr = new trac_RepairCaseCtlr();
		} catch (Exception e) {
			caughtException = true;
			system.assertEquals(Label.CASE_NOT_FOUND, e.getMessage());
		}

		system.assert(caughtException);
	}
	
	
	private static testMethod void testBuildRedirectDeviceAndSim() {
		system.runas(trac_RepairTestUtils.getTestUser()) {
			Case c = new Case(Repair_type__c = 'Device',
												Mobile_number__c = '6049170274',
												ESN_MEID_IMEI__c = trac_RepairTestUtils.REPAIR_ESN_SUCCESS);
			insert c;
			
			c= [SELECT Repair_Type__c, CaseNumber, Mobile_number__c, esn_MEID_IMEI__c
				  FROM Case
				  WHERE Id = :c.id];
			
			ApexPages.currentPage().getParameters().put('Id',c.id);
			
			trac_RepairCaseCtlr ctlr = new trac_RepairCaseCtlr();
			
/*            
			system.assertEquals(Label.Repair_Create_Link + '&caseID=' + c.id + '&caseNO=' + c.caseNumber + '&languageCode=EN' +
				'&repairTypeCd=' + c.Repair_Type__c + '&mobileNum=6049170274&itemESN=' + trac_RepairTestUtils.REPAIR_ESN_SUCCESS,
				ctlr.redirectUrl);
*/				
			c.Repair_Type__c = 'SIM';
			update c;
			
			ctlr = new trac_RepairCaseCtlr();
/*			
			system.assertEquals(Label.Repair_Create_Link + '&caseID=' + c.id + '&caseNO=' + c.caseNumber + '&languageCode=EN' +
				'&repairTypeCd=' + c.Repair_Type__c + '&mobileNum=6049170274&itemESN=' + trac_RepairTestUtils.REPAIR_ESN_SUCCESS,
				ctlr.redirectUrl);
*/
		}
	}
	
	
	private static testMethod void testBuildRedirectAccessory() {
		system.runas(trac_RepairTestUtils.getTestUser()) {
			Case c = new Case(Repair_type__c = 'Accessory',
												Accessory_SKU__c = 'testmodel',
												Accessory_Purchase_Date__c = Date.today());
			insert c;
			
			c= [SELECT Repair_Type__c, CaseNumber, Accessory_SKU__c
				  FROM Case
				  WHERE Id = :c.id];
			
			ApexPages.currentPage().getParameters().put('Id',c.id);
			
			trac_RepairCaseCtlr ctlr = new trac_RepairCaseCtlr();
/*			
			system.assertEquals(Label.Repair_Create_Link + '&caseID=' + c.id + '&caseNO=' + c.caseNumber + '&languageCode=EN' +
				'&repairTypeCd=' + c.Repair_Type__c + '&itemSKU=testmodel',
				ctlr.redirectUrl);
*/
		}
	}
	
	/*
	private static testMethod void testGetLangCode() {
		Id PROFILEID = [SELECT id FROM Profile WHERE name='Standard User' OR name='Utilisateur standard' LIMIT 1].id;
		User u = new User(username = 'LANGTEST@TRACTIONSM.COM',
						  email = 'test@example.com',
						  title = 'test',
						  lastname = 'test',
						  alias = 'test',
						  TimezoneSIDKey = 'America/Los_Angeles',
						  LocaleSIDKey = 'en_US',
						  EmailEncodingKey = 'UTF-8',
						  ProfileId = PROFILEID,
						  LanguageLocaleKey = 'en_US');
		insert u;
		
		system.runAs(u) {
			system.assertEquals('EN', getLangCode());
		}
		
		u.LocaleSIDKey = 'fr_CA';
		u.LanguageLocaleKey = 'fr';
		update u;
		
		system.runAs(u) {
			system.assertEquals('FR', getLangCode());
		}
	}
	*/
}