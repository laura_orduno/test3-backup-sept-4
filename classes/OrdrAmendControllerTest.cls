/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
*/
@isTest //(SeeAllData=true)
private class OrdrAmendControllerTest {
    
    @isTest
    private static void testPrepareForAmend1() {
        String orderId= OrdrTestDataFactory.singleMethodForOrderId();
        List<Order> orderRecordList = [Select id, status,vlocity_cmt__ValidationMessage__c,vlocity_cmt__ValidationStatus__c,vlocity_cmt__ValidationDate__c From order Where Id = :orderId];
        Order orderInst=orderRecordList.get(0);
        orderInst.General_Notes_Remarks__c='test comments';
        update orderInst;
        ApexPages.currentPage().getParameters().put('id',orderId);
        ApexPages.StandardController sc = new ApexPages.StandardController(orderInst);
        OrdrAmendController amendController=new OrdrAmendController(sc);
        //amendController.setAmendComments('Test Comment');
        amendController.amendComments='test comment';
        // String testComment=amendController.getAmendComments();
        amendController.prepareForAmend();
        //System.assert(response != null);
    }
    @isTest
    private static void testPrepareForAmend2() {
        String orderId= OrdrTestDataFactory.singleMethodForOrderId();
        List<Order> orderRecordList = [Select id, status,vlocity_cmt__ValidationMessage__c,vlocity_cmt__ValidationStatus__c,vlocity_cmt__ValidationDate__c From order Where Id = :orderId];
        Order orderInst=orderRecordList.get(0);
        ApexPages.currentPage().getParameters().put('id',orderId);
        ApexPages.StandardController sc = new ApexPages.StandardController(orderInst);
        OrdrAmendController amendController=new OrdrAmendController(sc);
        //amendController.setAmendComments('Test Comment');
        amendController.amendComments='test comment';
        // String testComment=amendController.getAmendComments();
        amendController.prepareForAmend();
        //System.assert(response != null);
    }
     @isTest
    private static void testPrepareForAmend3() {
        String orderId= OrdrTestDataFactory.singleMethodForOrderId();
        List<Order> orderRecordList = [Select id, status,vlocity_cmt__ValidationMessage__c,vlocity_cmt__ValidationStatus__c,vlocity_cmt__ValidationDate__c From order Where Id = :orderId];
        Order orderInst=orderRecordList.get(0);
        orderInst.General_Notes_Remarks__c='TEST';
        update orderInst;
        ApexPages.currentPage().getParameters().put('id',orderId);
        ApexPages.StandardController sc = new ApexPages.StandardController(orderInst);
        OrdrAmendController amendController=new OrdrAmendController(sc);
        //amendController.setAmendComments('Test Comment');
        amendController.amendComments=null;
        System.debug(amendController.amendComments);
        // String testComment=amendController.getAmendComments();
        orderInst.General_Notes_Remarks__c='TEST';
        update orderInst;
        amendController.ordObj=orderInst;
        amendController.prepareForAmend();
        //System.assert(response != null);
    }
}