public class smb_SC_External_LinksController {

	public List<sclink> scLinks {get; set;}
	
	public class sclink {
		
		public String label {get; set;}
		public String url {get; set;}
	}


	public PageReference init() {
		
		scLinks = new List<sclink>();
		
		//query Custom Settings for External Links
		
		List<SC_External_Links__c> links = [SELECT Link_URL__c, Label__c, Category__c FROM SC_External_Links__c ORDER BY Display_Order__c];
		
		for (SC_External_Links__c sc : links) {
			sclink workingsclink = new sclink();
			workingsclink.label = sc.Label__c;
			workingsclink.url = sc.Link_URL__c;
			sclinks.add(workingsclink);
		}

		return null;
	}
}