/*
############################################################################
# Last Modified by......: Nitin Saxena
# Last Modified Date....: 14-Jun-2016
# Description...........: changed smb_ContractManagementSvcTypes_v3 to cms3_1ContractManagementSvcTypes_v3.1.
  The essence of the change was to accommodate a parameter type change (long to string) for 
  the findContractAmendmentsByAssociateNum() operation on the CMS web service endpoint.  A new operation was introduced on CMS V3, 
  findContractAmendmentsByAssociateNumber(), in order to accommodate this version change. 
  Due to SOA Governance policies, the web service endpoint was not to be versioned.  But instead, adding two new operations that 
  referenced an updated XSD version of 3.1 was implemented by the BTO CMS Development team.  As a result, we are still 
  naming this service within SFDC as 3.1 to show that there was a change to the service endpoint contract. 
  Technically, adding operations should result in a new end point version.  But, BTO decided against this in order to meet
  the TQ project timelines.  This is why you may see the endpoint URL pointing to version 3.0 of the web service, 
    even though it is actually version 3.1 due to the addition of two new operations
#####################################################
*/
global class smb_Contract implements Comparable {

    global String contactId;
    global String contractId{set;get;}
    global String customerId;
    global String customerLegalName;
    global String contactIdentifierTxt{get;set;}
    global String regionalCustomerId;
    global String contractNum{set;get;}
    global String contractName {get;set;}
    global String contractAssociateNum{set;get;}
    global String contractAmendmentNum{set;get;}
    global String contractAmendmentNumber{set;get;}
    global integer contractValue{set;get;}        
    global String signedDate{set;get;}
    global String registeredDate{set;get;}
    global String effectiveDate{set;get;}
    global String expiryDate{set;get;}
    global String contractType{set;get;}
    global String billingAddress;
    global String contractStatus{set;get;}
    global Integer duration;
    global string contractTerm{set;get;}   
    global boolean masterContractInd {set;get;} 
    global Boolean autoRenewInd {set;get;}
    global string contractObjectType{get;set;}
    
    global Integer CompareTo(Object compareTo) {
        smb_Contract contractToCompare = (smb_Contract)compareTo;

        if (effectiveDate == null) 
            return 1;
        if (contractToCompare == null || contractToCompare.effectiveDate == null)   
            return -1;
        if (effectiveDate > contractToCompare.effectiveDate)
            return 1;
        if (effectiveDate == contractToCompare.effectiveDate)
            return 0;
        return -1;
    }
  
     //changed the below method from smb_ContractManagementSvcTypes_v3 to cms3_1
    public smb_Contract (cms3_1ContractManagementSvcTypes_v3.Contract contract) {
        this.contactId = contract.contactId;
    this.contactIdentifierTxt= contract.contactIdentifierTxt;
        this.contractId = contract.contractId;
        this.customerId = contract.customerId;
        this.customerLegalName = contract.customerLegalName;
        this.regionalCustomerId = contract.regionalCustomerId;
        this.contractNum = contract.contractNum;
        this.contractName = contract.contractNum 
                            +'-'
                            +zeroFill(contract.contractAssociateNum)
                            +'-'
                            +zeroFill(contract.contractAmendmentNumber);
        this.contractAssociateNum = contract.contractAssociateNum;
        this.contractAmendmentNumber = contract.contractAmendmentNumber;
         if (contract.contractValue!= null) {
        this.contractValue = parseDecimal(double.valueof(contract.contractValue));  
        //this.contractValue = parseDecimal(contract.contractValue);  
        }
        // Change Added by Nitin for fix of date defect

        if(contract.signedDate<>null && contract.signedDate<>'')
        this.signedDate = formatDateData(contract.signedDate);
        if(contract.registeredDate<>null && contract.registeredDate<>'')
        this.registeredDate= formatDateData(contract.registeredDate);
        if(contract.effectiveDate<>null && contract.effectiveDate<>'')
        this.effectiveDate = formatDateData(contract.effectiveDate);
           
         if(contract.expiryDate<>null && contract.expiryDate<>'')
        this.expiryDate = formatDateData(contract.expiryDate);

         // Change End by Nitin for fix of date defect
        /*this.signedDate = parseDateAndFormat(contract.signedDate);
        this.registeredDate = parseDateAndFormat(contract.registeredDate);
        this.effectiveDate = parseDateAndFormat(contract.effectiveDate);
        this.expiryDate = parseDateAndFormat(contract.expiryDate);
        */
        this.contractType = contract.contractType;
        this.billingAddress = contract.billingAddress;
        this.contractStatus = contract.contractStatus;
        //System.debug('CONTRACT TERM->'+contract.contractDetailList.contractDetail[0]);
        if (null != contract.contractDetailList && contract.contractDetailList.contractDetail.size() > 0 ) {
        this.contractTerm = contract.contractDetailList.contractDetail[0].contractTermUnit == 'Day'
                            ?contract.contractDetailList.contractDetail[0].contractTerm + '(D)'
                            :contract.contractDetailList.contractDetail[0].contractTerm;
        this.masterContractInd = contract.contractDetailList.contractDetail[0].masterContractInd;
        this.autoRenewInd = contract.contractDetailList.contractDetail[0].autoRenewInd;     
        }
        
        if (effectiveDate != null && expiryDate != null) {
              this.duration =  Date.valueOf(contract.expiryDate).year() - Date.valueOf(contract.effectiveDate).year();
        }
        contractObjectType='humbolt';
    }
    public smb_Contract (contract contract) {
        //this.contactId = contract.contactId;//select id,recordtype.name,contractnumber,status from contract
        this.contractId = contract.id;
        this.contractName = contract.contractnumber;
        this.contractNum = contract.contractnumber;
        this.contractType = contract.recordtype.name;
        this.effectiveDate=string.valueof(contract.effective_date__c);
        this.contractTerm=(contract.contractterm!=null?string.valueof(contract.contractterm):'');
        this.contractStatus = contract.status;
        contractObjectType='vlocity';
    }
    
    public smb_Contract(cms3_1ContractManagementSvcReqRes_v3.ContractData contractData) {
        this.customerLegalName = contractData.customerName;
        this.customerId = contractData.customerId;
    }
    
     
    
   @TestVisible  private Date parseDate(string dateValue) {
        if (dateValue == null) return null;
        
        return  (Date)json.deserialize(dateValue, date.class);
    }
    
   @TestVisible  private String parseDateAndFormat(string dateValue) {
        if (dateValue == null) return null;
        
        return smb_DateUtility.formatDate(parseDate(dateValue));
    }
   @TestVisible  private String parseDateAndFormat(Date dateValue) {
        if (dateValue == null) return null;
        
        return smb_DateUtility.formatDate(dateValue); 
    }
    // changed decimalValue from string to Double
   @TestVisible  private integer parseDecimal(Double decimalValue) {
        if (decimalValue == null) return null;
        
        return Decimal.valueOf(decimalValue).intValue();
    }
   
   @TestVisible  private string zeroFill(string value) {
        if (Value == null) return '000';
        
        if(value.length()==1)
            return '00'+value;
        else if (value.length()==2)
        return '0'+value;
        else if (value == '')
        return '000';
        else
        return value;
    }

     public static string formatDateData(string strDate){
   string strDat=strDate;
    strDat=strDat.left(10);
String[] strLLL=strDat.split('-');
datetime myDate= Datetime.newInstance(integer.valueof(strLLL[0]),integer.valueof(strLLL[1]),integer.valueof(strLLL[2]));
    string monthName='';
    String finalDate='';
    integer month=myDate.date().month();
    integer intdate=myDate.date().day();
    integer year=myDate.date().year();
    if(month==1)
        monthName='Jan';
    if(month==2)
        monthName='Feb';
    if(month==3)
        monthName='Mar';
    if(month==4)
        monthName='Apr';
    if(month==5)
        monthName='May';
    if(month==6)
        monthName='Jun';
    if(month==7)
        monthName='Jul';
    if(month==8)
        monthName='Aug';
    if(month==9)
        monthName='Sep';
    if(month==10)
        monthName='Oct';
    if(month==11)
        monthName='Nov';
    if(month==12)
        monthName='Dec';
    finalDate= monthName+' '+string.valueOf(intdate)+','+ string.valueOf(year);
    return finalDate;
}


}