/*
###########################################################################
# File..................: SRS_MyCRIS3Orders_Controller
# Version...............: 1
# Created by............: Aditya Jamwal (IBM)
# Created Date..........: 16-Oct-2014 
# Last Modified by......: 
# Last Modified Date....: 
# Modification:           
# Description...........: Class is for MyCRIS3Orders Homepage component filtering Cris3 order specific for logged in user.
#
############################################################################ 

*/
public with sharing class SRS_MyCRIS3Orders_Controller {

    public List<LegOrder> LegacyOrderList = new List<LegOrder>();
    public boolean doSort= false;
    private String sortDirection = 'DESC';
    private String sortExp = 'LastModifiedDate';
    string sortFullExp = ' order by LastModifiedDate desc '; 
    // Which field should be considered for sorting
    public static string SORT_FIELD ='city' ;
    // Sorting direction ASCENDING or DESCENDING
    public static string SORT_DIR = 'ASC'; 
    public List<Service_Request__c> SRList =new List<Service_Request__c>();


    public String sortExpression
               {
                 get
                 {
                    return sortExp;
                 }
                 set
                 {
                   //if the column is clicked on then switch between Ascending and Descending modes
                   if (value == sortExp)
               sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
               else
               sortDirection = 'ASC';
               sortExp = value;
                  
                 }
               }
               
               
    public String getSortDirection()
             {
                //if not column is selected 
                if (sortExpression == null || sortExpression == '')
                  return 'ASC';
                else
                 return sortDirection;
             }
             
             public void setSortDirection(String value)
             {  
               sortDirection = value;
             }


     
     public void SortRelatedListwithColumn()
        {
             
                   doSort=true;          
                   sortFullExp = '';
                   //build the full sort expression
                   sortFullExp = ' order by ' +sortExpression  + ' ' + sortDirection+' NULLS LAST ';
                   
        } 

    public List<LegOrder> getLegacyOrderList() {
        
        Set<id> LegOrderIds = new Set<id>();
        Set<id> SerRelatedLegOrderIds = new Set<id>();
        List<SRS_Service_Request_Related_Order__c> LeOrderList;
        for(Service_Request_Employee__c TeamMember : [Select Id, Service_Request__r.id,Service_Request__r.Service_Request_Status__c From Service_Request_Employee__c where Team_Member__r.User__c = :Userinfo.getUserId() and (Service_Request__r.Service_Request_Status__c Not IN('Cancelled','Completed'))])
        {LegOrderIds.add(TeamMember.Service_Request__r.id);}
        
        for(Service_Request__c SR: [Select Id, Name,Opportunity__c,Opportunity__r.name,Condition_Sort_Index__c,SRS_PODS_Product__r.Name,Order_ID__c,Account_Name__r.Name,Target_Date_Type__c,Target_Date__c,Fox__c,Jeop_code__c, Service_Request_Type__c,Service_Request_Status__c,Requested_Date__c,Product_Type__c,LastModifiedDate,Submitted_Date__c,Last_Submitted_Date__c,DD_Scheduled__c,Legacy_Order_count__c,Condition__c,Condition_Color__c,CreatedDate From Service_Request__c where (ownerid = :Userinfo.getUserId() or createdbyId = :Userinfo.getUserId()or Id IN :LegOrderIds) and (Service_Request_Status__c Not IN('Cancelled','Completed'))])
        {SerRelatedLegOrderIds.add(SR.id);}
          
          LeOrderList= [Select id,Name,Seq__c,DD_Scheduled__c,System__c,RO_Service_Request__c,RO_Service_Request__r.Name,RO_Service_Request__r.Account_Name__r.Name,Jeop_Hold_Code__c,Originating_Date__c,LastModifiedDate,SRS_LegacyOrderToActionFlag__c from SRS_Service_Request_Related_Order__c where (createdbyId = :Userinfo.getUserId()or Id IN :SerRelatedLegOrderIds)and(System__c ='CRIS3-AB' or System__c='CRIS3-BC') and((Jeop_Hold_Code__c!= null and Jeop_Hold_Code__c!='MLK' and Jeop_Hold_Code__c!='DSW') or (SRS_LegacyOrderToActionFlag__c=true or CRDBJEOPCode__c!= null)) limit 999];
          
    //      LeOrderList= [Select id,Name,DD_Scheduled__c,System__c,RO_Service_Request__c,RO_Service_Request__r.Name,RO_Service_Request__r.Account_Name__r.Name,Jeop_Hold_Code__c,Originating_Date__c,LastModifiedDate,SRS_LegacyOrderToActionFlag__c from SRS_Service_Request_Related_Order__c where  (createdbyId = :Userinfo.getUserId()or Id IN :SerRelatedLegOrderIds)and(System__c ='CRIS3-AB' or System__c='CRIS3-BC') and (Jeop_Hold_Code__c!= null or SRS_LegacyOrderToActionFlag__c=true) limit 999];
          List<LegOrder> LegacyOrderList = new List<LegOrder>();
                      for(SRS_Service_Request_Related_Order__c l:LeOrderList)
                       {
                           LegOrder Lo = new LegOrder(l);
                           Lo.OrderNumber = l.Name +'-'+l.Seq__c;                      
                           LegacyOrderList.add(Lo);
                       }
                SORT_DIR = sortDirection;
                SORT_FIELD =sortExpression;
                LegacyOrderList.sort() ;    
            return LegacyOrderList;
        }
        
         public SRS_MyCRIS3Orders_Controller() 
        {       
              sortExpression='DD_Scheduled__c';
              sortDirection = 'DESC';                        
        } 

        // sorting support for Lists using 'Comparable' interface
        public class LegOrder  implements Comparable
        {
          public SRS_Service_Request_Related_Order__c LOrder {get; set;} 
          public String OrderNumber {get; set;}   
          
            // Implement the compareTo() method
            public Integer compareTo(Object other) {
             if (SORT_FIELD == 'LastModifiedDate') 
             {
                return compareToSRDateTime(other); 
             } 
             else if (SORT_FIELD == 'DD_Scheduled__c') 
             {
                return compareToSRDate(other); 
             } else 
                 return compareStringProperty(other); 
              return 0;
              
             }
                            
             // Compares SRString field
             Public Integer compareStringProperty(Object other) 
             {  
                 String sortfield;
                 String currInstField;
                
                if(SORT_FIELD=='RO_Service_Request__r.Name')
                {
                    
                     sortfield  =((LegOrder)other).LOrder.RO_Service_Request__r.Name != null ?  ((LegOrder)other).LOrder.RO_Service_Request__r.Name : '';
                     currInstField  =this.LOrder.RO_Service_Request__r.Name != null ?  this.LOrder.RO_Service_Request__r.Name: '';
                
                }               
              
                
                 String otherSRString= other != null ?  sortfield : '';   
                 if (SORT_DIR== 'ASC')     
                   return currInstField.compareTo(otherSRString);   
                 else
                   return otherSRString.compareTo(currInstField);  
            
             }
             
            // Compares SRDateTime field
             public Integer compareToSRDateTime(Object other) 
             {
                DateTime sortfield;
                DateTime currInstField;
                
                if(SORT_FIELD=='LastModifiedDate')
                {
                
                 sortfield  =((LegOrder)other).LOrder.LastModifiedDate != null ?  ((LegOrder)other).LOrder.LastModifiedDate: datetime.newInstance(1900, 01, 01);
                 currInstField  =this.LOrder.LastModifiedDate != null ?  this.LOrder.LastModifiedDate : datetime.newInstance(1900, 01, 01);
                
                }
              
              DateTime otherDateTime = other != null ? sortfield  : datetime.newInstance(1900, 01, 01);   
              // use Datetime.getTime() to do get the numeric time in millis
              if (SORT_DIR == 'ASC')
               return (currInstField.getTime() - otherDateTime.getTime()).intValue();
              else
               return (otherDateTime.getTime() - currInstField.getTime()).intValue();
             }
             
            public Integer compareToSRDate(Object other) 
             {    
                 Date sortfield;
                 Date currInstField;
               if(SORT_FIELD=='DD_Scheduled__c')
                {
                    sortfield  =((LegOrder)other).LOrder.DD_Scheduled__c != null ?  ((LegOrder)other).LOrder.DD_Scheduled__c : date.newInstance(1900,01, 01);
                    currInstField  =this.LOrder.DD_Scheduled__c != null ?  this.LOrder.DD_Scheduled__c : date.newInstance(1900,01, 01);
                
                }
              Date otherDate = other != null ? sortfield : date.newInstance(1900,01, 01);   
              // use Datetime.getTime() to do get the numeric time in millis
              if (SORT_DIR == 'ASC')
               return otherDate.daysBetween(currInstField);
               else
               return currInstField.daysBetween(otherDate);  
             }

          
          public LegOrder(SRS_Service_Request_Related_Order__c LO)
          {
            LOrder= LO;
          }
           public LegOrder()
          {
            LOrder= null;
          }
        }
}