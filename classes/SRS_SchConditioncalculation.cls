/*
###########################################################################
# File..................: SRS_SchConditioncalculation 
# Version...............: 1
# Created by............: Aditya Jamwal (IBM)
# Created Date..........: 04-Oct-2014 
# Last Modified by......: 
# Last Modified Date....: 
# Modification:           
# Description...........: Class is to schedule SRS_BatchConditioncalculation batch.
#
############################################################################ 

*/

global class SRS_SchConditioncalculation implements Schedulable {

   //......This methos is to schedule same batch Class.....//
global void execute(SchedulableContext sc) {
system.debug('@@@@Inside executeSch' +sc);
  Database.executeBatch(new SRS_BatchConditioncalculation(null),20);
}

}