/*
 * Controller for the RepairQuote page
 * Part of the SMB Repairs project
 *
 * @author: Grant Adamson, Traction On Demand 
 */

public with sharing class trac_RepairQuoteCtlr extends trac_RepairCaseCtlr{
	private static final Set<String> VALID_STATUSES = new Set<String>{'ACCEPT','REJECT'};
	
	private String quoteStatus;
	
	
	public trac_RepairQuoteCtlr() {
		super();
		quoteStatus = getQuoteStatus();
	}
	
	
	private static String getQuoteStatus() {
		if(trac_PageUtils.isParamNull('status')) {
			throw new RepairCaseException(Label.NO_STATUS);
		}
		
		String quoteStatus = trac_PageUtils.getParam('status').toUpperCase();
		if(VALID_STATUSES.contains(quoteStatus)) {
			return quoteStatus;
		} else {
			throw new RepairCaseException(Label.INVALID_STATUS);
		}
	}
	
	
	public void doUpdateQuoteStatus() {		
		if(theCase.repair__c != null) {
			trac_RepairCalloutResponse resp = trac_RepairCallout.updateQuoteStatus(theCase, quoteStatus);
			
			if(resp.success) {
				success = true;
				theCase.Repair_Quote_Available__c = false;
			} else {
				trac_PageUtils.addError(resp.messageBody);
			}
			
			update theCase;
		} else {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, Label.REPAIR_ID_REQUIRED));
		}
		
		complete = true;
	}	

}