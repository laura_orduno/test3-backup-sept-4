/**
 * An apex page controller that exposes the site forgot password functionality
 */
public class TelusPRMForgotPasswordController {
    public String username {get; set;}   
    public String errorMsg {get; set;}
    public Boolean errFlag = false;
    
    private Map<String, String> efMap = new Map<String, String>{    
            'Your username was not found.' => 'Impossible de trouver le nom d\'utilisateur fourni.'
    };
    
    public Boolean getErrFlag(){
        return errFlag;
    }
           
    public TelusPRMForgotPasswordController() {}
    
    public PageReference forgotPassword() {
        errFlag = false;
        errorMsg = '';
        if(username == null || username.equals('')) {
            errFlag = true;
            errorMsg = 'Username is required.';
            return null;
        }
        boolean success = Site.forgotPassword(username);
        PageReference pr = Page.TelusPRMForgotPasswordConfirm;
        pr.setRedirect(true);       
        if (success) {              
            return pr;
        }
        else{
            errFlag = true;
            ApexPages.Message[] msg = ApexPages.getMessages();
            
            for (ApexPages.Message m : msg) {
                errorMsg += m.getDetail();                            
            }
        return null;
        }
    }
    
    public PageReference forgotPasswordFr() {
        errFlag = false;
        errorMsg = '';
        if(username == null || username.equals('')) {
            errFlag = true;
            errorMsg = 'Veuillez entrer votre nom d’utilisateur.';

            return null;
        }

        boolean success = Site.forgotPassword(username);
        PageReference pr = Page.TelusPRMForgotPasswordConfirmFr;
        System.debug('success: '+success);
        if (success) {              
            return pr;
        } else {
            errFlag = true;
            ApexPages.Message[] msg = ApexPages.getMessages();
            
            for (ApexPages.Message m : msg) {
                
                if (efMAp.get(m.getDetail()) != null) {
                    errorMsg += efMAp.get(m.getDetail());                            
                }
                 
                System.debug('errorMsg : ' + errorMsg );                      
            }
            System.debug('errorMsg : ' + errorMsg );
            return null;
        }
    }
    
}