//Generated by FuseIT WSDL2Apex (http://www.fuseit.com/Solutions/SFDC-Explorer/Help-WSDL-Parser.aspx)
	
public class TpResourceOrderMessage {
	public class ResourceOrderCollectionMessage {
		public TpCommonMtosiV3.Header_T header;
		private String[] header_type_info = new String[]{'header','','Header_T','0','1','false'};
		public TpFulfillmentResourceOrderV3.ResourceOrder[] resourceOrder;
		private String[] resourceOrder_type_info = new String[]{'resourceOrder','http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/schema/ResourceOrderMessage','ResourceOrder','0','-1','false'};
		private String[] apex_schema_type_info = new String[]{'http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/schema/ResourceOrderMessage','true','false'};
		private String[] field_order_type_info = new String[]{'header','resourceOrder'};
	}
	public class ResourceOrderMessage {
		public TpCommonMtosiV3.Header_T header;
		private String[] header_type_info = new String[]{'header','','Header_T','0','1','false'};
		public TpFulfillmentResourceOrderV3.ResourceOrder resourceOrder;
		private String[] resourceOrder_type_info = new String[]{'resourceOrder','http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/schema/ResourceOrderMessage','ResourceOrder','1','1','false'};
		private String[] apex_schema_type_info = new String[]{'http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/schema/ResourceOrderMessage','true','false'};
		private String[] field_order_type_info = new String[]{'header','resourceOrder'};
	}
}