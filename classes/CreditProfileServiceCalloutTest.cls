@isTest
public class CreditProfileServiceCalloutTest
{
    @isTest
    public static void ping()
    {
        Test.setMock(WebServiceMock.class, new BusinessCreditProfileMgmtServiceMock());

        Test.startTest();
        CreditProfileServiceCallout.ping();        
        Test.StopTest();    
    }

    @isTest
    public static void searchIndivdualCreditProfile()
    {
        Test.setMock(WebServiceMock.class, new BusinessCreditProfileMgmtServiceMock());

        CreditProfileServiceCallout.CreditSearchParamForIndividualProfile param = new CreditProfileServiceCallout.CreditSearchParamForIndividualProfile();
        param.sin = 'MockSin123456';
        param.healthCardNum = 'MockH123456';

        Test.startTest();

        List<CreditProfileServiceCallout.CreditSearchProfileResult> results = CreditProfileServiceCallout.searchIndivdualCreditProfile(param);

        //System.debug('Profile ID: ' + results.get(0).profileId);

        //System.assertEquals(1, results.size());

        Test.stopTest();
    }

    @isTest
    public static void createIndividualCustomerWithCreditWorthiness()
    {
        Test.setMock(WebServiceMock.class, new BusinessCreditProfileMgmtServiceMock());

        CreditProfileServiceCallout.CreateConsumerCreditProfileRequest request = new CreditProfileServiceCallout.CreateConsumerCreditProfileRequest();
        request.sin = 'MockSin123456';
        request.driversLicenseNum = 'MockDL123456';
        request.driversLicenseProvinceCd = 'BC';
        request.provincialIdNum = 'MockProv123456';
        request.provincialIdProvinceCd = 'BC';
        request.passportNum = 'MockP123456';
        request.passportCountry = 'CAN';
        request.healthCardNum = 'MockH123456';
        request.addressLine = '3777 Kingsway';
        request.city = 'Burnaby';
        request.provinceStateCode = 'BC';
        request.postalCode = 'V5H 3Z7';
        request.countryCode = 'CAN';
        request.applicationProvinceCd = 'BC';
        request.creditCheckConsentCd = 'Y';
        request.emailTxt = 'noreply@telus.com';
        request.homePhoneNumber = '6046041111';
        request.cellPhoneNumber = '6046042222';
        request.firstName = 'Mock';
        request.lastName = 'Test';

        Test.startTest();

        Long id = CreditProfileServiceCallout.createIndividualCustomerWithCreditWorthiness(request);

        //System.assertEquals(7, id);

        Test.stopTest();
    }

}