/**
 * trac_CustomContactLookup_Ctlr.cls - Controller for CustomContactLookup Page
 * @description Controller Extension handles querying for contacts.
 *
 * @author Ryan Draper, Traction on Demand
 * @date 2013-08-12
 */
public with sharing class trac_CustomContactLookup_Ctlr extends Base_Ext {
	public String query {get; set;}
	public List<Contact> contacts {get; set;}
	
	public String fullNameLabel {get;set;}//Full_Name;
	public String activeLabel {get;set;}//Active;
	public String roleLabel {get;set;}//Role;
	public String searchForUserLabel {get;set;}//trac_search_for_a_user
	public String searchResultsLabel {get;set;}//trac_Search_Results
	public String lookupErrorLabel {get;set;}//trac_lookup_error
	public String goLabel {get;set;}//trac_GO
	public String searchLabel {get;set;}//trac_Search
	public String searchLookupLabel {get;set;}//trac_Lookup
	public String contactOwnerAliasLabel {get;set;}
	
	
	public trac_CustomContactLookup_Ctlr() {
		contacts = new List<Contact>();
		query = ApexPages.currentPage().getParameters().get('nameField');
		
		fullNameLabel  = Label.Full_Name;
		activeLabel = Label.Active;
		roleLabel = Label.Role;
		searchForUserLabel = Label.trac_search_for_a_user;
		searchResultsLabel = Label.trac_Search_Results;
		lookupErrorLabel = Label.trac_lookup_error;
		goLabel = Label.trac_GO;
		searchLabel = Label.trac_Search;
		searchLookupLabel = Label.trac_Lookup;
		contactOwnerAliasLabel = Label.Contact_Owner_Alias;
	}
	
	public void runQuery(){	
		query = query.replace('*','');
		String hiddenQuery = '%' + query + '%';
		//excludes contacts where the owning account is a partner account
		//so that no portal contacts can be found by the user.
		contacts = [SELECT Id, Name, Account.Name, Phone, email, Owner.Alias 
					FROM Contact 
					WHERE Name 
					like :hiddenQuery 
					and Account.IsPartner = false
					and ( not email like '%telus.com%')
					limit 30];
	}

	//Tests
	@isTest(SeeAllData=false)
	private static void testOnSearch() {
		Id PROFILEID = [SELECT id FROM Profile WHERE name='Standard User' LIMIT 1].id;
        
		User u = new User(username = 'VMDFJREFSRI@TRACTIONSM.COM',
                                                  email = 'test@example.com',
                                                  title = 'test',
                                                  firstname = 'traction',
                                                  lastname = 'test',
                                                  alias = 'test',
                                                  TimezoneSIDKey = 'America/Los_Angeles',
                                                  LocaleSIDKey = 'en_US',
                                                  EmailEncodingKey = 'UTF-8',
                                                  ProfileId = PROFILEID,
                                                  LanguageLocaleKey = 'en_US');
        insert u;
        
        Account a = new Account(name = 'test account', ownerid = u.id);
        insert a;
        
		Contact c = new Contact(	lastname = 'lastname', 
									accountid = a.id,
									email = 'test@example.com'
									);
		
        insert c;

		trac_CustomContactLookup_Ctlr ctlr = new trac_CustomContactLookup_Ctlr();
		
		ctlr.query = c.lastname;
		
		ctlr.runQuery();
		
		system.assertNotEquals(null, ctlr.contacts);
	}
}