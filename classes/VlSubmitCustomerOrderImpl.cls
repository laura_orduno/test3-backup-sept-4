/*
* -------------------------------------------------------------------------------------------------
* This class checks type of the incoming object from context and invoke the process. This creates a quote
* from the opportunity and creates an order from quote. This validates before the order is submitted.
* -------------------------------------------------------------------------------------------------
* @author         Santosh K Rath   
* @modifiedBy     Santosh K Rath   
* @version        1.0
* @created        2015-11-01
* @modified       YYYY-MM-DD
* --------------------------------------------------------------------------------------------------
* @changes
* vX.X            Santosh K Rath
* YYYY-MM-DD      Explanation of the change.  
*
* vX.X            Santosh K Rath
* YYYY-MM-DD      
* -------------------------------------------------------------------------------------------------
*/

global with sharing class VlSubmitCustomerOrderImpl implements vlocity_cmt.VlocityOpenInterface
{
    global Boolean invokeMethod(String methodName, Map<String, Object> input, Map<String, Object> output, Map<String, Object> options)  
    {
        if (methodName.equals('executeCustomAction'))
        {
            return executeCustomAction(input,output,options);
        }
        return true;
    }
    
    private Boolean executeCustomAction(Map<String, Object> inputMap, Map<String, Object> outputMap, Map<String, Object> optionsMap)
    {  
          
        system.debug('VlSubmitCustomerOrderImpl::executeCustomAction(): start'); 
            
        Map<String, Object> input2 = (Map<String, Object>) vlocity_cmt.FlowStaticMap.flowMap.get('inputMap');
        Id parentId = (Id) input2.get('objectId');
        System.debug('this is my id: ' + parentId);
        if(parentId !=null)
        {
            String objType = parentId.getSObjectType().getDescribe().getName();
            system.debug('TELUSSubmitOrder::executeCustomAction(): objType is ' + objType);       
            if(objType =='Opportunity')
            {
                //this keeps the original implementation as is - for example when creating a Quote from Oppty - keep existing functionality OOTB
                vlocity_cmt.DefaultObjectCopierImplementation objCopier =  new vlocity_cmt.DefaultObjectCopierImplementation();
                vlocity_cmt.FlowStaticMap.flowMap.put('objectCopier',objCopier.copyObject(parentId, ''));
            }
            else if (objType =='Quote')
            {
                system.debug('TELUSSubmitOrder::executeCustomAction(): Quote objType');
                // retrieve quote
                quote quoteRecord = [Select account.id, Status, activities__c,
                                     vlocity_cmt__ValidationDate__c, vlocity_cmt__ValidationMessage__c, vlocity_cmt__ValidationStatus__c, Service_Address__c
                                     From quote Where id=:parentId limit 1];
                if (quoteRecord != null)
                {
                    system.debug('VlSubmitCustomerOrderImpl::executeCustomAction(): quote id is ' + parentId);
                    // retrieve service address
                    List<SMBCare_Address__c> addressRecord = [Select Status__c
                                                              From SMBCare_Address__c Where id=:quoteRecord.Service_Address__c limit 1];
                    if (addressRecord.size() > 0) {
                        system.debug('VlSubmitCustomerOrderImpl::executeCustomAction(): SMBCare_Address__c id is ' + quoteRecord.Service_Address__c);
                        if(addressRecord[0].Status__c == 'Valid')
                            // quote can only be submitted if address is valid
                        {
                            
                            // Vlocity Feb 10 2016 OCOM - 436
                            // quote can not be submitted if Expired or Draft
                            
                            if (quoteRecord.Status == 'Expired'){
                                try 
                                {
                                    system.debug('VlSubmitCustomerOrderImpl::executeCustomAction(): quote expired');
                                    // output error message
                                    quoteRecord.vlocity_cmt__ValidationMessage__c = 'Quote can not be submitted if status is Expired';
                                    quoteRecord.vlocity_cmt__ValidationStatus__c = 'Failed';
                                    quoteRecord.vlocity_cmt__ValidationDate__c = system.now();
                                    update quoteRecord;
                                    
                                    String quoteId= '/'+parentId;
                                    vlocity_cmt.ObjectCopier oc = new vlocity_cmt.ObjectCopier();
                                    oc.NewObjectId = quoteId;
                                    vlocity_cmt.FlowStaticMap.flowMap.put('objectCopier',oc);
                                }
                                catch(Exception e)
                                {
                                    system.debug(LoggingLevel.ERROR, 'Error while updating Quote: '+ e);
                                }
                                return false;
                            } else if (quoteRecord.Status == 'Draft'){
                                try 
                                {
                                    system.debug('VlSubmitCustomerOrderImpl::executeCustomAction(): quote draft');
                                    // output error message
                                    quoteRecord.vlocity_cmt__ValidationMessage__c = 'Quote can not be submitted if status is Draft';
                                    quoteRecord.vlocity_cmt__ValidationStatus__c = 'Failed';
                                    quoteRecord.vlocity_cmt__ValidationDate__c = system.now();
                                    update quoteRecord;
                                    
                                    String quoteId= '/'+parentId;
                                    vlocity_cmt.ObjectCopier oc = new vlocity_cmt.ObjectCopier();
                                    oc.NewObjectId = quoteId;
                                    vlocity_cmt.FlowStaticMap.flowMap.put('objectCopier',oc);
                                }
                                catch(Exception e)
                                {
                                    system.debug(LoggingLevel.ERROR, 'Error while updating Quote: '+ e);
                                }
                                return false;
                                
                            }else {
                                system.debug('VlSubmitCustomerOrderImpl::executeCustomAction(): original implementation');
                                //this keeps the original implementation as is - for example when creating an Order from Quote - keep existing functionality OOTB             
                                vlocity_cmt.DefaultObjectCopierImplementation objCopier =  new vlocity_cmt.DefaultObjectCopierImplementation();
                                vlocity_cmt.FlowStaticMap.flowMap.put('objectCopier',objCopier.copyObject(parentId, ''));
                                
                                quoteRecord.Status = 'Ordered';
                                quoteRecord.activities__c = 'Ordered on ' + String.valueOf(System.Now());
                                update quoteRecord;
                                
                            }
                        }
                        else
                        {
                            try 
                            {
                                system.debug(LoggingLevel.ERROR, 'VlSubmitCustomerOrderImpl::executeCustomAction(): address not valid');
                                // output error message
                                quoteRecord.vlocity_cmt__ValidationMessage__c = 'Address not valid';
                                quoteRecord.vlocity_cmt__ValidationStatus__c = 'Failed';
                                quoteRecord.vlocity_cmt__ValidationDate__c = system.now();
                                update quoteRecord;
                                
                                String quoteId= '/'+parentId;
                                vlocity_cmt.ObjectCopier oc = new vlocity_cmt.ObjectCopier();
                                oc.NewObjectId = quoteId;
                                vlocity_cmt.FlowStaticMap.flowMap.put('objectCopier',oc);
                            }
                            catch(Exception e)
                            {
                                system.debug(LoggingLevel.ERROR, 'Error while updating Quote: '+ e);
                            }
                            return false;
                        }
                    }
                    else
                    {
                        try 
                        {
                            system.debug(LoggingLevel.ERROR, 'VlSubmitCustomerOrderImpl::executeCustomAction(): service address not found');
                            // output error message
                            quoteRecord.vlocity_cmt__ValidationMessage__c = 'Service address not found';
                            quoteRecord.vlocity_cmt__ValidationStatus__c = 'Failed';
                            quoteRecord.vlocity_cmt__ValidationDate__c = system.now();
                            update quoteRecord;
                            
                            String quoteId= '/'+parentId;
                            vlocity_cmt.ObjectCopier oc = new vlocity_cmt.ObjectCopier();
                            oc.NewObjectId = quoteId;
                            vlocity_cmt.FlowStaticMap.flowMap.put('objectCopier',oc);
                        }
                        catch(Exception e)
                        {
                            system.debug(LoggingLevel.ERROR, 'Error while updating Quote: '+ e);
                        }
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else if (objType =='Order')
            {   
                String msgKey=parentId;
                storeInSession(msgKey.substring(0,15)+'submitflow','Y');
                       
                
                system.debug('VlSubmitCustomerOrderImpl::executeCustomAction() order start');
                
                            
                system.debug('@@Inside Order submission');
                String opptyId = '/'+parentId;
                String orderId= parentId;
                boolean validationError= false;
                String vaildationFailsMessage='Before submitting this order, please provide:<br/>';
                //Validate the order before submit
                
                //List<Order> orderObjList = [SELECT Id, requesteddate__c, Ban__c, Shipping_Address__c, Credit_Check_Not_Required__c, Credit_Assessment__r.Assessment_Status__c ,  status,Shipping_Address1__c, Service_Address__c,ShipToContactId,BillToContactId,vlocity_cmt__ValidationMessage__c,vlocity_cmt__ValidationStatus__c,vlocity_cmt__ValidationDate__c FROM Order  where id=:orderId limit 1];
                List<Order> orderObjList = [SELECT Id, AccountId, Account.Credit_Profile__c, Credit_Assessment_ID__c, Credit_Assessment_Status__c, requesteddate__c, Ban__c, Shipping_Address__c, Credit_Check_Not_Required__c, Credit_Reference_Number__c, Rush__c, status,Shipping_Address1__c, Service_Address__c, ShipToContactId, BillToContactId, vlocity_cmt__ValidationMessage__c, vlocity_cmt__ValidationStatus__c, vlocity_cmt__ValidationDate__c, vlocity_cmt__OneTimeTotal__c, vlocity_cmt__RecurringTotal__c, vlocity_cmt__TotalMonthlyDiscount__c, vlocity_cmt__TotalOneTimeDiscount__c FROM Order  where id=:orderId limit 1];
                
                //String requestedDate=orderObj.Scheduled_Due_Date__c;
                Order orderObj=null;
                if(orderObjList!=null && !orderObjList.isEmpty()){
                    orderObj=orderObjList.get(0);                                      
                   
                }
                if(orderObj!=null) {    
                    if(String.isNotBlank(orderObj.vlocity_cmt__ValidationStatus__c) || Test.isRunningTest()){
                        orderObj.vlocity_cmt__ValidationStatus__c=null;
                        update orderObj;
                    }
                    
                    
                    // Added as part of Defect Id# 53681.
                    
                    if(string.isBlank(orderObj.Ban__c))
                    {   
                        vaildationFailsMessage  +='Billing Account'+'<br/>'; 
                        validationError= true;
                    }
                    
                    if(string.isBlank(orderObj.Shipping_Address__c))
                    {
                        vaildationFailsMessage  +='Shipping Address'+'<br/>'; 
                        validationError= true;
                    }                    
                    
                    // This is placeholder for WorkOrder booking.Mentioned fields are anticipated and might not be the same in future. 
                    for(orderItem orditems:[Select id, Work_Order__c, PricebookEntry.Name, Work_Time__c from orderItem where OrderId=:orderObj.Id]){
                    if(orditems.Work_Order__c==null && ((orditems.PricebookEntry.Name=='HSIA Premise Visit') || (orditems.PricebookEntry.Name=='TELUS Connectivity')) )    
                    {      
                    vaildationFailsMessage  +='Booking for '+orditems.PricebookEntry.Name+'<br/>'; 
                    validationError= true;
                    }
                    } 
              
                    
                    
                    
                    // Addition End for Defect fix 53681
                    
                    
                    // CREDIT CHECK START
                    
                    system.debug('VlSubmitCustomerOrderImpl::executeCustomAction() credit check start');
                    
                    Boolean isCarRequired = true;
                    Boolean continueChecking = true;
                    Boolean noCreditProfile = false;
                    Boolean amendCar = false;
                    
                    // check if account has a credit profile
                    if (orderObj.Account.Credit_Profile__c == null) {
                        noCreditProfile = true;
                    }
                    system.debug('VlSubmitCustomerOrderImpl::executeCustomAction() noCreditProfile: ' + noCreditProfile);
                    
                    // check if order has an existing credit assessment
                    Credit_Assessment__c aCreditAssessment = null;
                    system.debug('VlSubmitCustomerOrderImpl::executeCustomAction() orderObj.Credit_Assessment_ID__c: ' + orderObj.Credit_Assessment_ID__c);
                    if (orderObj.Credit_Assessment_ID__c != null) {
                        List<Credit_Assessment__c> creditAssessmentList = [SELECT Total_Deal_Value__c, Total_Deal_Monthly_Recurring_Charges__c, Total_Deal_Non_Recurring_Charges__c FROM Credit_Assessment__c WHERE id=:orderObj.Credit_Assessment_ID__c limit 1];
                        system.debug('VlSubmitCustomerOrderImpl::executeCustomAction() creditAssessmentList: ' + creditAssessmentList);
                        if (creditAssessmentList != null && !creditAssessmentList.isEmpty()) {
                            aCreditAssessment = creditAssessmentList.get(0);
                            system.debug('VlSubmitCustomerOrderImpl::executeCustomAction() CAR MRC (' + aCreditAssessment.Total_Deal_Monthly_Recurring_Charges__c + ') and NRC (' + aCreditAssessment.Total_Deal_Non_Recurring_Charges__c + ')');
                        }
                    }
                    system.debug('VlSubmitCustomerOrderImpl::executeCustomAction() aCreditAssessment: ' + aCreditAssessment);
                    
                    // check if credit profile has an external credit reference number (aka Credit Ref TL), updated via batch from the TELUS Credit Portal system
                    system.debug('VlSubmitCustomerOrderImpl::executeCustomAction() orderObj.AccountId: ' + orderObj.AccountId);
                    List<Credit_Profile__c> creditProfile = [SELECT credit_reference_TL__c FROM Credit_Profile__c where account__c=:orderObj.AccountId and credit_reference_TL__c <> '' limit 1];
                    Boolean tlExists = creditProfile.size() > 0;
                    system.debug('VlSubmitCustomerOrderImpl::executeCustomAction() tlExists: ' + tlExists);
                    
                    // CAR not required when MRC, NRC and promotions are $0
                    system.debug('VlSubmitCustomerOrderImpl::executeCustomAction() MRC (' + orderObj.vlocity_cmt__RecurringTotal__c + ') NRC (' + orderObj.vlocity_cmt__OneTimeTotal__c + ') MRC promo (' + orderObj.vlocity_cmt__TotalMonthlyDiscount__c + ') NRC promo (' + orderObj.vlocity_cmt__TotalOneTimeDiscount__c + ')');
                    if (orderObj.vlocity_cmt__RecurringTotal__c == 0 && orderObj.vlocity_cmt__OneTimeTotal__c == 0 && orderObj.vlocity_cmt__TotalMonthlyDiscount__c == 0 && orderObj.vlocity_cmt__TotalOneTimeDiscount__c == 0) {
                        isCarRequired = false;
                        system.debug('VlSubmitCustomerOrderImpl::executeCustomAction() $0 MRC/NRC/promotions');
                    }
                    
                    // check if credit assessment already performed and deemed CAR not required
                    if (aCreditAssessment == null && orderObj.Credit_Check_Not_Required__c) {
                        isCarRequired = false;
                        continueChecking = false;
                        system.debug('VlSubmitCustomerOrderImpl::executeCustomAction() Credit_Check_Not_Required__c');
                    }
                    
                    if (continueChecking) {
                        // retrieve account
                        List<Account> accountList = [SELECT Account_Status__c FROM Account where id=:orderObj.AccountId];
                        system.debug('VlSubmitCustomerOrderImpl::executeCustomAction() accountList: ' + accountList);
                        Account anAccount = null;
                        if (accountList != null && !accountList.isEmpty()) {
                            anAccount = accountList.get(0);
                            system.debug('VlSubmitCustomerOrderImpl::executeCustomAction() account status: ' + anAccount.Account_Status__c);
                        }
                        if (anAccount != null) {
                            system.debug('VlSubmitCustomerOrderImpl::executeCustomAction() account found');
                             if (anAccount.Account_Status__c == 'Active' && tlExists) {
                                system.debug('VlSubmitCustomerOrderImpl::executeCustomAction() account status active and tlExists');
                                if (orderObj.vlocity_cmt__RecurringTotal__c > 200 || orderObj.vlocity_cmt__OneTimeTotal__c > 100 || ((orderObj.vlocity_cmt__TotalMonthlyDiscount__c + orderObj.vlocity_cmt__TotalOneTimeDiscount__c) > 100)) {
                                    system.debug('VlSubmitCustomerOrderImpl::executeCustomAction() MRC/NRC/promotions CAR required');
                                    isCarRequired = true;
                                    continueChecking = false;
                                } else {
                                    system.debug('VlSubmitCustomerOrderImpl::executeCustomAction() MRC/NRC/promotions CAR not required');
                                    isCarRequired = false;
                                }
                            }
                        }
                    }
                    
                    // check if order total is greater than 10% of an existing CAR deal value
                    system.debug('VlSubmitCustomerOrderImpl::executeCustomAction(): starting 10% check');
                    if (aCreditAssessment != null && ((orderObj.vlocity_cmt__RecurringTotal__c + orderObj.vlocity_cmt__OneTimeTotal__c) > (aCreditAssessment.Total_Deal_Value__c * 1.1))) {
                        system.debug('VlSubmitCustomerOrderImpl::executeCustomAction() MRC (' + orderObj.vlocity_cmt__RecurringTotal__c + ') NRC (' + orderObj.vlocity_cmt__OneTimeTotal__c + ') total deal value (' + aCreditAssessment.Total_Deal_Value__c + ')');
                        amendCar = true;
                        vaildationFailsMessage += string.valueof(system.Label.Credit_Amend_CAR)+'\n';
                        //validationError = true;
                        system.debug('VlSubmitCustomerOrderImpl::executeCustomAction() amend CAR');
                    }
                    
                    // CAR required before order can be submitted
                    if (isCarRequired && (aCreditAssessment == null)) {
                        vaildationFailsMessage += 'A Credit Assessment is required before the order can be submitted'+'\n';
                        validationError = true;
                        system.debug('VlSubmitCustomerOrderImpl::executeCustomAction() CAR is required before order can be submitted');
                    }
                    
                    String creditStatus = orderObj.Credit_Assessment_Status__c;
                    if (isCarRequired && aCreditAssessment != null && creditStatus != null) {
                        if (creditStatus.equalsIgnoreCase('DRAFT')) {
                            // check if CAR status is Draft during order submission
                            vaildationFailsMessage += 'Order cannot be submitted if the Credit Assessment status is still in Draft status'+'\n';
                            validationError = true;
                            system.debug('VlSubmitCustomerOrderImpl::executeCustomAction() CAR cannot be in Draft status when order is submitted');
                        } else if (!creditStatus.equalsIgnoreCase('APPROVED')) {
                            // check if CAR status is approved during order submission
                            vaildationFailsMessage += 'Order cannot be submitted if the Credit Assessment status is not approved'+'\n';
                            validationError = true;
                            system.debug('VlSubmitCustomerOrderImpl::executeCustomAction() CAR not approved when order is submitted');
                        }
                    }
                    
                    if (noCreditProfile) {
                        vaildationFailsMessage += string.valueof(system.Label.Credit_Profile_Missing)+'\n';
                        validationError = true;
                        system.debug('VlSubmitCustomerOrderImpl::executeCustomAction() credit profile missing');
                    }
                    
                    system.debug('VlSubmitCustomerOrderImpl::executeCustomAction() credit check end');
                    
                    // CREDIT CHECK END
                    
                    
                    if(null==orderObj.requesteddate__c){                     
                        vaildationFailsMessage  += string.valueof(System.Label.Cancel_Order_Validation_Error_for_Requested_Date)+'\n'; 
                        validationError= true;
                    }  
                }
                //validationError=false;
                if(validationError){
                    String key=orderId.substring(0,15)+'msg';
                    
                        if(String.isNotBlank(vaildationFailsMessage)){
                            storeInSession(key,'Warn');
                           // Cache.Session.put(key,'Warn');
                           storeInSession('WarnMsg',vaildationFailsMessage);
                           // Cache.Session.put('WarnMsg',vaildationFailsMessage);                            
                        } else {
                            storeInSession(key,'Warn');
                            //Cache.Session.put(key,'Warn');                           
                        }                    
                    
                                 
                    
                    vlocity_cmt.ObjectCopier oc = new vlocity_cmt.ObjectCopier();
                    oc.NewObjectId = '/'+orderId;                        
                    System.debug('oc: ' + oc);
                    vlocity_cmt.FlowStaticMap.flowMap.put('objectCopier',oc);
                     system.debug('sANTOSH ::return IN validationError ');
                    return true;
                }
                try{                 
                     storeInSession(orderId.substring(0,15)+'msg','Success');
                    /* if(!Test.isRunningTest()){                       
                        Cache.Session.remove(key); 
                        Cache.Session.put(key,'Success');
                        system.debug('sANTOSH ::BEFORE CREATE ASSETS ');
                    } */    
                    //createAssets(orderId);
                    
                     if(!Test.isRunningTest()){
                         createAssets(orderId);
                        Cache.Session.remove(orderId.substring(0,15)+'msg'); 
                       // system.debug('sANTOSH ::REMOVE KEY  CREATE ASSETS ');                       
                    } 
                }catch(Exception assetException){
                   /* String key=orderId.substring(0,15)+'msg';
                    if(!Test.isRunningTest()){
                        Cache.Session.put(key,'Fail');    
                       
                    }*/
                    System.debug('Santosh ' +assetException);
                    storeInSession(orderId.substring(0,15)+'msg','Fail');
                                         
                    vlocity_cmt.ObjectCopier oc = new vlocity_cmt.ObjectCopier();
                    oc.NewObjectId = '/'+orderId;
                    System.debug('oc: ' + oc);
                    vlocity_cmt.FlowStaticMap.flowMap.put('objectCopier',oc);
                   //  system.debug('sANTOSH ::RETURN IN  CREATE ASSETS FAIL ');                      
                    return true;
                }    
                /*if(!Test.isRunningTest()){
                        String key=orderId.substring(0,15)+'msg';
                        Cache.Session.put(key,'InProgress');      
                         system.debug('sANTOSH ::ORDER SUBMIT InProgress  ');                    
                    }*/
                 storeInSession(orderId.substring(0,15)+'msg','InProgress');
                /* Order orderRecordBeforeSubmit = [Select id, status,vlocity_cmt__ValidationStatus__c From order Where Id = :orderId];
                orderRecordBeforeSubmit.vlocity_cmt__ValidationStatus__c='InProgress';
                update orderRecordBeforeSubmit; */
                    
                submitCustomerOrder(orderId);
                vlocity_cmt.ObjectCopier oc = new vlocity_cmt.ObjectCopier();
                oc.NewObjectId = opptyId;
                System.debug('oc: ' + oc);
                vlocity_cmt.FlowStaticMap.flowMap.put('objectCopier',oc);                           
                return true;                
            }
        }
        return true;
    }
    public static void storeInSession(String key,String Value){
        if(!Test.isRunningTest()){                    
                Cache.Session.put(key,Value);
           }
    }
    public static void createAssets(String objId){
       vlocity_cmt.DefaultObjectCopierImplementation objCopier =  new vlocity_cmt.DefaultObjectCopierImplementation();
       vlocity_cmt.FlowStaticMap.flowMap.put('objectCopier',objCopier.copyObject(objId, '')); 
        return;
    }
    @future(callout=true)
    public static void submitCustomerOrder(String objId){
        /*Order orderRecordBeforeSubmit = [Select id, status,vlocity_cmt__ValidationMessage__c,vlocity_cmt__ValidationStatus__c,vlocity_cmt__ValidationDate__c From order Where Id = :objId];
        orderRecordBeforeSubmit.vlocity_cmt__ValidationStatus__c='InProgress';
        update orderRecordBeforeSubmit;*/
        String externalOrdNo='';
        Object returnObject=OrdrSubmitCustomerOrder.submitOrder(objId);
        List<Order> orderRecordList = [Select id, status,vlocity_cmt__ValidationMessage__c,vlocity_cmt__ValidationStatus__c,vlocity_cmt__ValidationDate__c From order Where Id = :objId];
        Order orderRecord=null;
        if(orderRecordList!=null && orderRecordList.size()>0){
          orderRecord=orderRecordList.get(0);  
        } else {
            return;
        }
        if(returnObject==null){            
            orderRecord.vlocity_cmt__ValidationMessage__c=null;
            //system.debug('!! success'+ orderRecord.vlocity_cmt__ValidationMessage__c);
            orderRecord.vlocity_cmt__ValidationStatus__c = 'SubmitFailed';
            orderRecord.vlocity_cmt__ValidationDate__c = system.now();
            orderRecord.orderMgmtId__c=externalOrdNo;
            orderRecord.id=objId;
            update orderRecord; 
            return;                     
        }
        processSubmitOrderSuccess(returnObject,orderRecord,objId);
       /* TpFulfillmentCustomerOrderV3.CustomerOrder customerOrderResponse =(TpFulfillmentCustomerOrderV3.CustomerOrder) returnObject;
        TpCommonBaseV3.CharacteristicValue[] charactersticsValList=customerOrderResponse.CharacteristicValue;                    
        for(TpCommonBaseV3.CharacteristicValue charactersticsVal : charactersticsValList){
            if(null!=charactersticsVal){
                TpCommonBaseV3.Characteristic charRes= charactersticsVal.Characteristic;                                       
                System.debug(charactersticsVal.Value[0]);
                externalOrdNo=charactersticsVal.Value[0];
            }                    
        }
        if(String.isNotBlank(externalOrdNo)){                 
            orderRecord.status = 'Activated';
            orderRecord.vlocity_cmt__ValidationMessage__c=null;
            //system.debug('!! success'+ orderRecord.vlocity_cmt__ValidationMessage__c);
            orderRecord.vlocity_cmt__ValidationStatus__c = 'Success';
            orderRecord.vlocity_cmt__ValidationDate__c = system.now();
            orderRecord.orderMgmtId__c=externalOrdNo;
            orderRecord.id=objId;
            update orderRecord;
        }  */    
        
    }
    public static void processSubmitOrderSuccess(Object returnObject, Order orderRecord,String objId){
        String externalOrdNo='';
         TpFulfillmentCustomerOrderV3.CustomerOrder customerOrderResponse =(TpFulfillmentCustomerOrderV3.CustomerOrder) returnObject;
        TpCommonBaseV3.CharacteristicValue[] charactersticsValList=customerOrderResponse.CharacteristicValue;                    
        for(TpCommonBaseV3.CharacteristicValue charactersticsVal : charactersticsValList){
            if(null!=charactersticsVal){
                TpCommonBaseV3.Characteristic charRes= charactersticsVal.Characteristic;                                       
                System.debug(charactersticsVal.Value[0]);
                externalOrdNo=charactersticsVal.Value[0];
            }                    
        }
        if(String.isNotBlank(externalOrdNo)){                 
            orderRecord.status = 'Activated';
            orderRecord.vlocity_cmt__ValidationMessage__c=null;
            //system.debug('!! success'+ orderRecord.vlocity_cmt__ValidationMessage__c);
            orderRecord.vlocity_cmt__ValidationStatus__c = 'Success';
            orderRecord.vlocity_cmt__ValidationDate__c = system.now();
            orderRecord.orderMgmtId__c=externalOrdNo;
            orderRecord.id=objId;
            update orderRecord;
        }
        System.debug('NC External Orderid is successfully updated');
    }
}