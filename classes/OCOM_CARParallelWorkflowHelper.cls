global class OCOM_CARParallelWorkflowHelper {
    public static Boolean ActivateCARUpdateToNC = false;
    public static final map<id,Order> OrderCARMap = new map<id,Order>();
    public static final map<ID,SMB_Helper.CAR_STATUS_Container> CARStatusMap = new MAP<ID,SMB_Helper.CAR_STATUS_Container>();
    
    // This method generates the AOO order that informs the back office that a CAR status change occured.
    // The AOO order is related to the customer order (i.e., opportunity) to which the CAR is associated.
    public static Id CreateOrderforCarStatusChange(ID relatedOrderId) {
        try {
            Order relatedOrder = OrderCARMap.get(relatedOrderId);
            Order amendOrder= new Order();
            amendOrder.AccountId = relatedOrder.Accountid;
            amendOrder.Name = 'Order for ' + relatedOrder.account.name;
            amendOrder.Related_Order__c=relatedOrder.id;
            //amendOrder.CloseDate = Date.today();
            amendOrder.Status='Draft';
            //amendOrder.type='All Other Orders';
            //amendOrder.Order_type__c='All Other Orders';
            Id recordType_ID = Schema.SObjectType.Order.getRecordTypeInfosByName().get('<TODO Record Type Name Here>').getRecordTypeId();
            if (RecordType_ID != null) {
                amendOrder.RecordTypeId = RecordType_ID;
            }
          
            //Get Related CAR record of particular Opportunity
            Id CARID = OrderCARMap.get(relatedOrderId).Credit_Assessment__c;
            amendOrder.Credit_Assessment__c = CARID;
            amendOrder.Credit_Check_Not_Required__c = false;
            insert amendOrder;
            
            /*
            PriceBookEntry pbe=[Select Product2Id,product2.name, Pricebook2Id From PricebookEntry  where Product2Id in (select id  from Product2 where Sterling_Item_ID__c ='TLSSMBVLTN-NC-TSTPRDCT') limit 1];
            OpportunityLineItem OLI=new OpportunityLineItem();
            OLI.PricebookEntryId=pbe.id;
            OLI.opportunityid=FOBOOrder.id;
            OLI.product_name__c=pbe.product2.name;
            OLI.LineNo__c=1;
            OLI.ParentLineNo__c='-1';
            OLI.Item_ID__c='TLSSMBVLTN-NC-TSTPRDCT';
            OLI.FOBO_Request_Type__c='Other Changes';
            OLI.Quantity=1;
            OLI.TotalPrice=1;
            OLI.Element_Type__c='Product';
          
            String notesForBO='';
            notesforBO = notesforBO+ 'Related Order ID:-'+relatedOpp.Order_ID__C+ '\r\n';
            notesforBO = notesforBO+ 'Credit Assessment ID:-'+relatedOpp.Credit_Assessment__r.name+ '\r\n';
            notesforBO = notesforBO+ 'CAR State Transition from: '+CAR_Status_MAP.get(CARID).OLD_CAR_Status+' to: '+CAR_Status_MAP.get(CARID).NEW_CAR_Status+'\r\n';
            notesforBO = notesforBO+ 'CAR Result Transition from: '+CAR_Status_MAP.get(CARID).OLD_CAR_Result+' to: '+CAR_Status_MAP.get(CARID).NEW_CAR_Result+'\r\n';
            notesforBO = notesforBO+ 'CAR Reason Code Transition from: '+CAR_Status_MAP.get(CARID).OLD_CAR_Reason_code+' to: '+CAR_Status_MAP.get(CARID).NEW_CAR_Reason_code+'\r\n';
            notesforBO = notesforBO+ '\r\n';
            OLI.Notes__c=notesforBO;
            insert OLI;
            */
            
            return amendOrder.id;
        } catch(exception e) {
            try { 
                Webservice_Integration_Error_Log__c log=new Webservice_Integration_Error_Log__c(apex_class_and_method__c='OCOM_CARParallelWorkflowHelper.CreateOrderforCarStatusChange',external_system_name__c='NC',webservice_name__c='SubmitOrder',object_name__c='Order',sfdc_record_id__c=relatedOrderId);
                log.error_code_and_message__c+=e.getmessage();
                log.stack_trace__c=e.getstacktracestring();
                log.error_code_and_message__c=(string.isnotblank(log.error_code_and_message__c)&&log.error_code_and_message__c.length()>32767?log.error_code_and_message__c.substring(0,32767):log.error_code_and_message__c);
                log.stack_trace__c=(string.isnotblank(log.stack_trace__c)&&log.stack_trace__c.length()>32767?log.stack_trace__c.substring(0,32767):log.stack_trace__c);
                insert log;
            } catch(exception ex) {
                Util.emailSystemError(ex);
            }
            return null;
        }
    }
}