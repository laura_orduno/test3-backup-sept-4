@isTest
public class OC_ContractDetailsTest {
    static void setUpData() {
        Order order = OrdrTestDataFactory.createOrderWithContract();
        ApexPages.currentPage().getParameters().put('id', order.id);
    }
    
   /* @isTest
    private static void testGetContactableOrderItems() {
        setUpData();
        Test.startTest();
        OC_ContractDetails contractDetails = new OC_ContractDetails();
        List<OrderItem> orderItems = contractDetails.oliRequiringContracts;
        Boolean hasOliRequiringContracts = contractDetails.hasOliRequiringContracts;
        //System.assert(true, hasOliRequiringContracts);
        Test.stopTest();
    } */
    @isTest
    private static void OC_ContractDetails() {
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        PageReference pageRef = Page.OC_CreditAndContract;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('parentOrderId', orderId );
        Order ordObj=[select id from Order where id=:orderId];        
        Test.startTest();
        OrdrTestDataFactory.associateContractWithOrder(orderId);
		List<Contracts__c> contrRList=[select id,order__c from Contracts__c ];
        for(Contracts__c crIn:contrRList){
            crIn.order__c=orderId;
        }
        update contrRList;
		 List<vlocity_cmt__ContractLineItem__c> cliList =[SELECT Id,OrderLineItemId__c,vlocity_cmt__AssetReferenceId__c FROM vlocity_cmt__ContractLineItem__c];
        vlocity_cmt__ContractLineItem__c cliObj=cliList.get(0);
		List<OrderItem> oiList=[select id,vlocity_cmt__RootItemId__c,contract_request__c from orderitem where order.parentid__c=:orderId];
		for(OrderItem oiInst:oiList){			
			oiInst.vlocity_cmt__RootItemId__c=oiInst.id;
			if(!contrRList.isEmpty()){
				oiInst.contract_request__c=contrRList.get(0).id;
			}
			
		}
		update oiList;
        cliObj.vlocity_cmt__AssetReferenceId__c=oiList.get(0).id;
        cliObj.OrderLineItemId__c=oiList.get(0).id;
        update cliObj;
        OC_ContractDetails ob=new OC_ContractDetails();
		System.debug(ob.canMoveForward);
		System.debug(ob.amendReady);
		OrderItem oiInst1=oiList.get(0);
	    oiInst1.Contract_Action__c='Add';
		update oiInst1;
		System.debug(ob.contractRequestObj);
		System.debug(ob.isContractable);
		System.debug(ob.hasOliRequiringContracts);
		System.debug(ob.hasContractRequests);
		System.debug(ob.oliRequiringContracts);
		ob.getContractActivities();
		ob.getContractRequestContractLineMap();
		System.debug(ob.resumeURL);
		System.debug(ob.contractReqId1);
		System.debug(ob.requestType1);
		
		System.debug(ob.contractReqId);
		System.debug(ob.redirectUrl);
		System.debug(ob.CRIdURL);
		System.debug(ob.JsonCRUrlMap);
        System.debug(ob.isCrOiDisplay);
        System.debug(ob.isCrCliDisplay);
        Test.stopTest();
    }
    @isTest
    private static void isUpdateable() {
    }
    @isTest
    private static void isRevocable() {
    }
    @isTest
    private static void isCancelled() {
    }
    @isTest
    private static void message() {
    }
    @isTest
    private static void canMoveForward() {
    }
    @isTest
    private static void amendReady(){
    }
    @isTest
    private static void contractRequestObj() {
    }
    @isTest
    private static void isContractable() {
    }
    @isTest
    private static void hasOliRequiringContracts() {
    }
    @isTest
    private static void hasContractRequests() {
    }
    @isTest
    private static void oliRequiringContracts() {
    }
    
    @isTest
    private static void getContractActivities() {
    }
    @isTest
    private static void getContractRequestContractLineMap() {
    }
    @isTest
    private static void getContractRequestOrderItemsMap() {
    }
    @isTest
    private static void generateResumeURL(){
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        PageReference pageRef = Page.OC_CreditAndContract;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('parentOrderId', orderId );
        Order ordObj=[select id,OpportunityId from Order where id=:orderId];
        Test.startTest();
        Id contractReqRecId = Contracts__c.SObjectType.getDescribe().getRecordTypeInfosByName().get('eContract').getRecordTypeId();
        Contracts__c ConReq= new Contracts__c(RecordTypeId=contractReqRecId,Contract_Type__c='New',Opportunity__c=ordObj.OpportunityId );
        ApexPages.currentPage().getParameters().put('contractRequestId', ConReq.id );
        ApexPages.currentPage().getParameters().put('requestType', 'New' );
		OC_ContractDetails ob=new OC_ContractDetails();
		ob.generateResumeURL();
        Test.stopTest();
    }
    @isTest
    private static void createOpportunity(){
		String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        PageReference pageRef = Page.OC_CreditAndContract;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('parentOrderId', orderId );
        Order ordObj=[select id,account.name,account.BillingCity,account.BillingState,account.BillingStreet,accountId from Order where id=:orderId];
        Test.startTest();
		List<OrderItem> oiList=[select id,vlocity_cmt__RootItemId__c,contract_request__c from orderitem where order.parentid__c=:orderId];
		List<Contracts__c> contrRList=[select id from Contracts__c ];
		for(OrderItem oiInst:oiList){			
			oiInst.vlocity_cmt__RootItemId__c=oiInst.id;
			if(!contrRList.isEmpty()){
				oiInst.contract_request__c=contrRList.get(0).id;
			}
		}
		update oiList;
		OC_ContractDetails ob=new OC_ContractDetails();
		ob.createOpportunity(ordObj);
		Test.stopTest();
    }
    @isTest
    private static void createContractRequest() {
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        PageReference pageRef = Page.OC_CreditAndContract;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('parentOrderId', orderId );
        Order ordObj=[select id from Order where id=:orderId];
        Test.startTest();
        //OrdrTestDataFactory.associateContractWithOrder(orderId);
        //Id contractReqRecId = Contracts__c.SObjectType.getDescribe().getRecordTypeInfosByName().get('eContract').getRecordTypeId();
        //Contracts__c ConReq= new Contracts__c(RecordTypeId=contractReqRecId,Contract_Type__c='New',Opportunity__c=ordObj.OpportunityId );
        List<Order> ordList=[select id,(select id from orderitems) from order where parentid__C=:orderId];
        String selectedOLIs='';
        Order childOrder=ordList.get(0);
        for(OrderItem oiInstance:childOrder.orderitems){
            selectedOLIs=selectedOLIs+oiInstance.id+',';
        }
        
        ApexPages.currentPage().getParameters().put('selectedOLIs', selectedOLIs );
        ApexPages.currentPage().getParameters().put('requestType', 'Amend' );
		List<OrderItem> oiList=[select id,vlocity_cmt__RootItemId__c,contract_request__c from orderitem where order.parentid__c=:orderId];
		List<Contracts__c> contrRList=[select id from Contracts__c ];
		for(OrderItem oiInst:oiList){			
			oiInst.vlocity_cmt__RootItemId__c=oiInst.id;
			if(!contrRList.isEmpty()){
				oiInst.contract_request__c=contrRList.get(0).id;
			}
		}
		update oiList;
		OC_ContractDetails ob=new OC_ContractDetails();
		ob.createContractRequest();
        Test.stopTest();
    }
    @isTest
    private static void createAmendContractRequest() {
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        PageReference pageRef = Page.OC_CreditAndContract;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('parentOrderId', orderId );
        Order ordObj=[select id from Order where id=:orderId];        
        Test.startTest();
        OrdrTestDataFactory.associateContractWithOrder(orderId);
		List<Contract> contrList=[select id,status from contract];
		if(!contrList.isEmpty()){
			Contract contr=contrList.get(0);
			contr.status='Contract Registered';
			update contr;
		}
        OC_ContractDetails cntrl= new OC_ContractDetails();
		List<vlocity_cmt__ContractLineItem__c> cliList=[select id from vlocity_cmt__ContractLineItem__c];
		List<OrderItem> oiList=[select id,ordermgmtid__C,Contract_Action__c,Contract_Line_Item__c,vlocity_cmt__RootItemId__c,contract_request__c from orderitem where order.parentid__c=:orderId];
		List<Contracts__c> contrRList=[select id from Contracts__c ];
		for(OrderItem oiInst:oiList){
			oiInst.Contract_Action__c='Amend';	
			oiInst.vlocity_cmt__RootItemId__c=oiInst.id;
			if(!contrRList.isEmpty()){
				oiInst.contract_request__c=contrRList.get(0).id;
			}			
		}
		oiList.get(0).Contract_Line_Item__c=cliList.get(0).id;
		update oiList;
		
        cntrl.createAmendContractRequest();
        Test.stopTest();
    }
    @isTest
    private static void updateOrderItems() {
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        PageReference pageRef = Page.OC_CreditAndContract;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('parentOrderId', orderId );
        Order ordObj=[select id,OpportunityId from Order where id=:orderId];
        Test.startTest();
        OrdrTestDataFactory.associateContractWithOrder(orderId);
        //Id contractReqRecId = Contracts__c.SObjectType.getDescribe().getRecordTypeInfosByName().get('eContract').getRecordTypeId();
        //Contracts__c ConReq= new Contracts__c(RecordTypeId=contractReqRecId,Contract_Type__c='New',Opportunity__c=ordObj.OpportunityId );
        List<Order> ordList=[select id,(select id from orderitems) from order where parentid__C=:orderId];
        String selectedOLIs='';
        Order childOrder=ordList.get(0);
        for(OrderItem oiInstance:childOrder.orderitems){
            selectedOLIs=selectedOLIs+oiInstance.id+',';
        }
        
        ApexPages.currentPage().getParameters().put('selectedOLIs', selectedOLIs );
        List<Contracts__c> contrRList=[select id from Contracts__c WHERE Opportunity__c=:ordObj.OpportunityId];
        String contractRequestId='';
        if(!contrRList.isEmpty()){		
            contractRequestId=contrRList.get(0).id;
        }
        ApexPages.currentPage().getParameters().put('contractRequestId', contractRequestId );
        List<OrderItem> oiList=[select id,vlocity_cmt__RootItemId__c,contract_request__c from orderitem where order.parentid__c=:orderId];
        for(OrderItem oiInst:oiList){			
            oiInst.vlocity_cmt__RootItemId__c=oiInst.id;
            oiInst.contract_request__c=contractRequestId;
        }
        update oiList;
        OC_ContractDetails dtl=new OC_ContractDetails();
        dtl.updateOrderItems();
       
        Test.stopTest();
    }
     @isTest
    private static void updateOrderItems2() {
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        PageReference pageRef = Page.OC_CreditAndContract;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('parentOrderId', orderId );
        Order ordObj=[select id,OpportunityId from Order where id=:orderId];
        Test.startTest();
        OrdrTestDataFactory.associateContractWithOrder(orderId);
        //Id contractReqRecId = Contracts__c.SObjectType.getDescribe().getRecordTypeInfosByName().get('eContract').getRecordTypeId();
        //Contracts__c ConReq= new Contracts__c(RecordTypeId=contractReqRecId,Contract_Type__c='New',Opportunity__c=ordObj.OpportunityId );
        List<Order> ordList=[select id,(select id from orderitems) from order where parentid__C=:orderId];
        String selectedOLIs='';
        Order childOrder=ordList.get(0);
        for(OrderItem oiInstance:childOrder.orderitems){
            selectedOLIs=selectedOLIs+oiInstance.id+',';
        }
        
        ApexPages.currentPage().getParameters().put('selectedOLIs', selectedOLIs );
        List<Contracts__c> contrRList=[select id from Contracts__c ];//WHERE Opportunity__c=:ordObj.OpportunityId
        String contractRequestId='';
        if(!contrRList.isEmpty()){		
            contractRequestId=contrRList.get(0).id;
        }
        ApexPages.currentPage().getParameters().put('contractRequestId', contractRequestId );
        List<OrderItem> oiList=[select id,vlocity_cmt__RootItemId__c,contract_request__c from orderitem where order.parentid__c=:orderId];
        for(OrderItem oiInst:oiList){			
            oiInst.vlocity_cmt__RootItemId__c=oiInst.id;
            oiInst.contract_request__c=contractRequestId;
        }
        update oiList;
        OC_ContractDetails dtl=new OC_ContractDetails();
        
        OC_ContractDetails.orderItems=null;
        dtl.updateOrderItems();
        Test.stopTest();
    }
    @isTest
    private static void createContractAttributeSnapshot() {
    }
    @isTest
    private static void createContractInstanceAttributes1() {
		String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        PageReference pageRef = Page.OC_CreditAndContract;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('parentOrderId', orderId );
        
        Test.startTest();
        OrdrTestDataFactory.associateContractWithOrder(orderId);
		List<Contract> contrList=[select id from contract ];
		if(!contrList.isEmpty()){
			OC_ContractDetails dtl=new OC_ContractDetails();
			List<OrderItem> oiList=[select id,vlocity_cmt__RootItemId__c,contract_request__c from orderitem where order.parentid__c=:orderId];
			List<Contracts__c> contrRList=[select id from Contracts__c ];
		for(OrderItem oiInst:oiList){			
			oiInst.vlocity_cmt__RootItemId__c=oiInst.id;
			if(!contrRList.isEmpty()){
				oiInst.contract_request__c=contrRList.get(0).id;
			}
		}
		update oiList;
			OC_ContractDetails.createContractInstanceAttributes(contrList.get(0).id);
		}
		
    }
	
	 @isTest
    private static void createContractInstanceAttributes2() {
		String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        PageReference pageRef = Page.OC_CreditAndContract;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('parentOrderId', orderId );
        
        Test.startTest();
        OrdrTestDataFactory.associateContractWithOrder(orderId);
		
		List<Contracts__c> contrRList=[select id from Contracts__c ];
		if(!contrRList.isEmpty()){
			OC_ContractDetails dtl=new OC_ContractDetails();
			List<OrderItem> oiList=[select id,contract_request__c,vlocity_cmt__RootItemId__c from orderitem where order.parentid__c=:orderId];
			
			for(OrderItem oi:oiList){
				oi.contract_request__c=contrRList.get(0).id;
				oi.vlocity_cmt__RootItemId__c=oi.id;
				if(!contrRList.isEmpty()){
					oi.contract_request__c=contrRList.get(0).id;
				}
			}
			update oiList;
			OC_ContractDetails.createContractInstanceAttributes(contrRList.get(0).id);
		}
    }
    @isTest
    private static void auxiliaryTest1() {
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
		Test.startTest();
		List<OrderItem> oiList=[select id,vlocity_cmt__RootItemId__c from orderitem where order.parentid__c=:orderId];
		for(OrderItem oiInst:oiList){			
			oiInst.vlocity_cmt__RootItemId__c=oiInst.id;
		}
		update oiList;
		OC_ContractDetails.ContractableProduct obj1=new OC_ContractDetails.ContractableProduct();
		obj1.category='';
		obj1.contractableTerm=0;
		obj1.productid='';
		OC_ContractDetails.ContractInstanceAttributes obj2=new OC_ContractDetails.ContractInstanceAttributes();
		obj2.accountLegalName='';
		obj2.billingAddress='';
		obj2.signatories=null;
		obj2.lineItems=null;
		
		OC_ContractDetails.LineItem obj3=new OC_ContractDetails.LineItem();
		obj3.orderMgmtId='';
		obj3.term='';
		obj3.quantity=0;
		obj3.totalMonthlyCharges=0;
		obj3.totalOneTimeCharges=0;
		
		OrdrTestDataFactory.associateContractWithOrder(orderId);
		
		List<Contracts__c> contrRList=[select id,SFDC_Contract__c,SFDC_Contract__r.status,Type_of_contract__c from Contracts__c ];
		Contracts__c contractReq=null;
		if(!contrRList.isEmpty()){
			contractReq=contrRList.get(0);
		}
		List<Contract> contrObjList=[select id,status from contract];
		if(!contrObjList.isEmpty()){
			Contract contrObj=contrObjList.get(0);
			contrObj.status='Draft';
			update contrObj;
		}
		
		OC_ContractDetails.ContractActivity obj4=new OC_ContractDetails.ContractActivity();
		obj4.contractStatus='';
		obj4.contractNum='';
		obj4.startDate='';
		obj4.endDate='';
		obj4.recurringTotal='';
		obj4.oneTimeTotal='';
		obj4.contractRequest=contractReq;
		System.debug(obj4.isUpdateable);
		System.debug(obj4.isRevocable);
		System.debug(obj4.isCancelled);
		System.debug(obj4.message);
		contractReq.SFDC_Contract__c=null;
		update contractReq;
		System.debug(obj4.message);
        
		Test.stopTest();
	}
	@isTest
	private static void handleReplacements(){
		String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        PageReference pageRef = Page.OC_CreditAndContract;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('parentOrderId', orderId );
        
        Test.startTest();
        OrdrTestDataFactory.associateContractWithOrder(orderId);
		List<Contracts__c> contrRList=[select id from Contracts__c ];
		List<OrderItem> oiList=[select id,ordermgmtid__C,vlocity_cmt__RootItemId__c,contract_request__c,Contract_Action__c from orderitem where order.parentid__c=:orderId];
		for(OrderItem oiInst:oiList){
			oiInst.ordermgmtid__C='1234567';
			oiInst.vlocity_cmt__RootItemId__c=oiInst.id;
			if(!contrRList.isEmpty()){
				oiInst.contract_request__c=contrRList.get(0).id;
			}
			oiInst.Contract_Action__c='Change_Replace';
		}
		Database.SaveResult[] updateOiSaveResultList= Database.update(oiList, true);
		OC_ContractDetails oc=new OC_ContractDetails();
		oc.handleReplacements(updateOiSaveResultList);
		Test.stopTest();
	}
    
    @IsTest
    private static void getContractRequestDataMapTest(){
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
       
        
        Test.startTest();
        OrdrTestDataFactory.associateContractWithOrder(orderId);
        List<Contract> contractList=[select id from contract];
		List<Contracts__c> contrRList=[select SFDC_Contract__c,id,order__C from Contracts__c ];
        for(Contracts__c c:contrRList){
            c.order__C=orderId;
            c.SFDC_Contract__c=contractList.get(0).id;
        }
        update contrRList;
        System.debug('from testclass contrRList.size()='+contrRList.size());
        List<OrderItem> oiList=[select Term__c,id,ordermgmtid__C,vlocity_cmt__RootItemId__c,contract_request__c,Contract_Action__c,contract_line_item__C from orderitem where order.parentid__c=:orderId];
		String firstId=oiList.get(0).id;
       List<vlocity_cmt__ContractLineItem__c> clilist=[select id,orderlineitemid__C from vlocity_cmt__ContractLineitem__c];
        String firstCli=clilist.get(0).id;
        for(OrderItem oiInst:oiList){
            oiInst.contract_line_item__C=firstCli;
			oiInst.ordermgmtid__C='1234567';
            oiInst.Term__c='36';
			oiInst.vlocity_cmt__RootItemId__c=firstId;
			if(!contrRList.isEmpty()){
				oiInst.contract_request__c=contrRList.get(0).id;
			}
			//oiInst.Contract_Action__c='Change_Replace';
		}
        update oiList;
        
        integer i=0;
        for(vlocity_cmt__ContractLineitem__c cliIn:clilist){
            cliIn.orderlineitemid__C=oiList.get(i).id;
            i++;
        }
        update clilist;
         PageReference pageRef = Page.OC_CreditAndContract;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('parentOrderId', orderId );
		OC_ContractDetails oc=new OC_ContractDetails();
		oc.getContractRequestDataMap();
		Test.stopTest();
    }
}