@isTest
private class MBRUtils_Test {
	
	@isTest static void testGetCategoryRequestTypes() {
		List<ParentToChild__c> ptcList = new List<ParentToChild__c>();
        ptcList.add( new ParentToChild__c(Name='TestCatRequest1', Identifier__c='CCICatRequest', Parent__c='parent1') );
        ptcList.add( new ParentToChild__c(Name='TestCatRequest2', Identifier__c='CCICatRequest', Parent__c='parent2') );
        ptcList.add( new ParentToChild__c(Name='TestCatRequest3', Identifier__c='CCIDogRequest', Parent__c='parent1') );
        ptcList.add( new ParentToChild__c(Name='TestCatRequest4', Identifier__c='CCICatRequest', Parent__c='parent1') );
        ptcList.add( new ParentToChild__c(Name='TestCatRequest5', Identifier__c='CCICatRequest', Parent__c='parent1') );
        insert ptcList;

        Test.startTest();
        Map<String, List<String>> catToReqTypes = MBRUtils.getCategoryRequestTypes();
        Test.stopTest();

        System.assertEquals(2, catToReqTypes.size());
        System.assertEquals(3, catToReqTypes.get('parent1').size());
	}

    @isTest static void testGetExternalToInternalStatuses() {
        List<ExternalToInternal__c> e2iList = new List<ExternalToInternal__c>();
        e2iList.add( new ExternalToInternal__c(Name='e2iA', Identifier__c='onlineStatus', External__c='external1') );
        e2iList.add( new ExternalToInternal__c(Name='e2iB', Identifier__c='onlineStatus', External__c='external2') );
        e2iList.add( new ExternalToInternal__c(Name='e2iC', Identifier__c='onlineStatus', External__c='external1') );
        e2iList.add( new ExternalToInternal__c(Name='e2iD', Identifier__c='onlineStatus', External__c='external1') );
        e2iList.add( new ExternalToInternal__c(Name='e2iE', Identifier__c='beepbeep', External__c='external1') );
        insert e2iList;

        Test.startTest();
        Map<String, List<String>> e2iStatuses = MBRUtils.getExternalToInternalStatuses();
        Test.stopTest();

        System.assertEquals(2, e2iStatuses.size());
        System.assertEquals(3, e2iStatuses.get('external1').size());
    }

    @isTest static void testGetRequestTypeToRecordTypeNames() {
        List<ExternalToInternal__c> e2iList = new List<ExternalToInternal__c>();
        e2iList.add( new ExternalToInternal__c(Name='e2iA', Identifier__c='onlineRequestTypeToCaseRT', External__c='external1') );
        e2iList.add( new ExternalToInternal__c(Name='e2iB', Identifier__c='onlineRequestTypeToCaseRT', External__c='external2') );
        e2iList.add( new ExternalToInternal__c(Name='e2iC', Identifier__c='onlineRequestTypeToCaseRT', External__c='external1') );
        e2iList.add( new ExternalToInternal__c(Name='e2iD', Identifier__c='onlineRequestTypeToCaseRT', External__c='external1') );
        e2iList.add( new ExternalToInternal__c(Name='e2iE', Identifier__c='beepbeep', External__c='external1') );
        insert e2iList;

        Test.startTest();
        Map<String, List<String>> reqTypes2RecTypes = MBRUtils.getRequestTypeToRecordTypeNames();
        Test.stopTest();

        System.assertEquals(2, reqTypes2RecTypes.size());
        System.assertEquals(3, reqTypes2RecTypes.get('external1').size());
    }

    @isTest static void testGetRecordTypeIdsByCategory() {
        List<ParentToChild__c> ptcList = new List<ParentToChild__c>();
        ptcList.add( new ParentToChild__c(Name='ptc1', Child__c='external1', Identifier__c='CCICatRequest', Parent__c='parent1') );
        ptcList.add( new ParentToChild__c(Name='ptcA', Child__c='externalA', Identifier__c='CCICatRequest', Parent__c='parent2') );
        ptcList.add( new ParentToChild__c(Name='ptcB', Child__c='externalB', Identifier__c='CCIDogRequest', Parent__c='parent1') );
        ptcList.add( new ParentToChild__c(Name='ptcC', Child__c='externalC', Identifier__c='CCICatRequest', Parent__c='parent1') );
        ptcList.add( new ParentToChild__c(Name='ptc2', Child__c='external2', Identifier__c='CCICatRequest', Parent__c='parent1') );
        insert ptcList;

        List<ExternalToInternal__c> e2iList = new List<ExternalToInternal__c>();
        //e2iList.add( new ExternalToInternal__c(Name='e2iA', Identifier__c='onlineRequestTypeToCaseRT', External__c='external1', Internal__c='ASR Activities') );
        //e2iList.add( new ExternalToInternal__c(Name='e2iB', Identifier__c='onlineRequestTypeToCaseRT', External__c='external2', Internal__c='BSR Activities') );
        e2iList.add( new ExternalToInternal__c(Name='e2iC', Identifier__c='onlineRequestTypeToCaseRT', External__c='external1', Internal__c='Generic') );
        e2iList.add( new ExternalToInternal__c(Name='e2iD', Identifier__c='onlineRequestTypeToCaseRT', External__c='external1', Internal__c='Feedback') );
        e2iList.add( new ExternalToInternal__c(Name='e2iE', Identifier__c='beepbeep', External__c='external1', Internal__c='Contact Center Activities') );
        insert e2iList;

        Test.startTest();
        Set<String> recTypes = MBRUtils.getRecordTypeIdsByCategory( 'parent1' );
        Test.stopTest();

        System.assertEquals(2, recTypes.size());
    }

    @isTest static void testGetInClause() {
        List<String> testList = new List<String>();
        testList.add('one');
        testList.add('two');
        testList.add('three');
        testList.add('four');
        String retval = MBRUtils.getInClause( testList );
        System.assert(retval.length() > 0, 'Failed list join test');

        Set<String> testSet = new Set<String>();
        testSet.add('foo');
        testSet.add('bar');
        testSet.add('baz');
        testSet.add('beep');
        String retval2 = MBRUtils.getInClause( testSet );
        System.assert(retval2.length() > 0, 'Failed set join test');
    }

    @isTest static void testGetRequestTypeFilterForCategory() {
        List<ParentToChild__c> ptcList = new List<ParentToChild__c>();
        ptcList.add( new ParentToChild__c(Name='ptc1', Child__c='external1', Identifier__c='CCICatRequest', Parent__c='parent1') );
        ptcList.add( new ParentToChild__c(Name='ptcA', Child__c='externalA', Identifier__c='CCICatRequest', Parent__c='parent2') );
        ptcList.add( new ParentToChild__c(Name='ptcB', Child__c='externalB', Identifier__c='CCIDogRequest', Parent__c='parent1') );
        ptcList.add( new ParentToChild__c(Name='ptcC', Child__c='externalC', Identifier__c='CCICatRequest', Parent__c='parent1') );
        ptcList.add( new ParentToChild__c(Name='ptc2', Child__c='external2', Identifier__c='CCICatRequest', Parent__c='parent1') );
        insert ptcList;

        Test.startTest();
        String myString = MBRUtils.getRequestTypeFilterForCategory('parent1');
        Test.stopTest();

        System.assert(myString.length() > 0);
    }

    @isTest static void testGetInternalStatusFilterByExternal() {
        List<ExternalToInternal__c> e2iList = new List<ExternalToInternal__c>();
        e2iList.add( new ExternalToInternal__c(Name='e2iA', Identifier__c='onlineStatus', External__c='external1', Internal__c='ASR Activities') );
        e2iList.add( new ExternalToInternal__c(Name='e2iB', Identifier__c='onlineStatus', External__c='external2', Internal__c='BSR Activities') );
        e2iList.add( new ExternalToInternal__c(Name='e2iC', Identifier__c='onlineStatus', External__c='external1', Internal__c='Generic') );
        e2iList.add( new ExternalToInternal__c(Name='e2iD', Identifier__c='onlineStatus', External__c='external1', Internal__c='Feedback') );
        e2iList.add( new ExternalToInternal__c(Name='e2iE', Identifier__c='beepbeep', External__c='external1', Internal__c='Contact Center Activities') );
        insert e2iList;

        Test.startTest();
        String myString = MBRUtils.getInternalStatusFilterByExternal('external1');
        Test.stopTest();

        System.assert(myString.length() > 0);
    }

    @isTest static void testGetAccountConfiguration() {

        User u = MBRTestUtils.createPortalUser('');
        System.debug('akong: u: ' + u);
        User user = [SELECT Id, Contact.Account.Id FROM User WHERE Id = :u.Id][0];
        System.debug('akong: user: ' + user);
        System.debug('akong: user.Contact.Account.Id: ' + user.Contact.Account.Id);
        //System.debug('akong: u.Contact.Account: ' + u.Contact.Account);

        Account_Configuration__c ac = new Account_Configuration__c();
        ac.Account__c = user.Contact.Account.Id;
        ac.Name = 'Account Config for Unit Test Account';
        ac.Restrict_Tech_Support__c = true;
        insert ac;

        System.runAs(u) {
            Account_Configuration__c ac2 = MBRUtils.getAccountConfiguration();
            System.assertEquals(ac.Id, ac2.Id);

            Boolean restrictTechSupport = MBRUtils.restrictTechSupport();
            System.assertEquals(ac.Restrict_Tech_Support__c, restrictTechSupport);
        }
    }

}