/* 
###########################################################################
# File..................: smb_moveProductToAsset_sch
# Version...............: 26
# Created by............: Vinay Sharma
# Created Date..........: 19-Mar-2013
# Last Modified by......: 
# Last Modified Date....: 
# Description...........: This batch class is used by smb_moveProductToAsset_sch.
Logic:  1.Collect all unique quote line items. A quote line item is unique based on combination of product namr and parentQuoteline ID
        2. Create a new asset record based on different values in quoteline item such as pproduct name, price etc.
#
# Copyright (c) 2000-2012. Astadia, Inc. All Rights Reserved.
#
# Created by the Astadia, Inc. Modification must retain the above copyright notice.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any commercial purpose, without fee, and without a written
# agreement from Astadia, Inc., is hereby forbidden. Any modification to source
# code, must include this paragraph and copyright.
#
# Permission is not granted to anyone to use this software for commercial uses.
#
# Contact address: 2839 Paces Ferry Road, Suite 350, Atlanta, GA 30339
# Company URL : http://www.astadia.com
###########################################################################
*/
global class smb_moveProductToAsset_Batch implements Database.Batchable<sObject>
{
   global final String query;
   public static list<SBQQ__QuoteLine__c> lstDeletedQuoteLine = new list<SBQQ__QuoteLine__c>();

   global smb_moveProductToAsset_Batch(String q)
   {
        query=q;
   }
    
    /**
    * The start method is called at the beginning of a batch Apex job. Use the start method to collect the records or objects to be passed to the interface method execute. 
    * This method returns either a Database.QueryLocator object or an iterable that contains the records or objects being passed into the job.
    */ 
   global Database.QueryLocator start(Database.BatchableContext BC)
   {
        return Database.getQueryLocator(query);
   }

    /**
    *   The execute method is called for each batch of records passed to the method. Use this method to do all required processing for each chunk of data.
    *   This method reassigns the next approver based on configuration in the custom setting for the region/ product/ approval step.
    *   @praram:-A reference to the Database.BatchableContext object.
    *   @param :- A list of sObjects, such as List<sObject>, or a list of parameterized types. If you are using a Database.QueryLocator, the returned list should be used.
    * 
    */
   global void execute(Database.BatchableContext BC, List<sObject> scope)
   {
        if(scope.size()>0)
        smb_moveProductToAsset_Batch.moveProducts(scope);
   }

     /*
     * The finish method is called after all batches are processed. 
     * 
     */
   global void finish(Database.BatchableContext BC)
   {
        //run a batch job again to set parent Asset ID for assets, created/modified.
        String query  = 'select id,'+
                        'SBQQ__QuoteLine__c,'+
                        'ParentId__c,'+
                        'SBQQ__QuoteLine__r.SBQQ__RequiredBy__c'+' '+
                        'from Asset';
        system.debug('---query---'+query); 
        
        //execute batch
        smb_UpdateHierarchyOnAsset_batch runBatch = new smb_UpdateHierarchyOnAsset_batch(query);   
        database.executebatch(runBatch,200);

   }
   
   /*
   This method will further process all quoteline Items provided by schedular   
   */ 
   global static void moveProducts(List<SBQQ__QuoteLine__c> lstQuotesLines){
   
       list<SBQQ__QuoteLine__c> lstUniqueQuoteLine = new list<SBQQ__QuoteLine__c>();
       
       
       
       map<id,id> mapOfQuoteandAccWithCommonRCID = new map<id,id>();
        
        system.debug('-----lstQuotesLines----'+lstQuotesLines.size());
        //remove duplicate and deleted
        if(lstQuotesLines.size()>0)
        {           
            lstUniqueQuoteLine = removeDuplicateQuoteLines(lstQuotesLines);          
        }       
        
        
        system.debug('-----lstUniqueQuoteLine----'+lstUniqueQuoteLine.size());
        //Associate RCID for Quote and Account 
            mapOfQuoteandAccWithCommonRCID = getQuoteAccountMap(lstUniqueQuoteLine);
            //use upsert on assets
            if (lstUniqueQuoteLine.size()>0 && !mapOfQuoteandAccWithCommonRCID.isEmpty() )
            {
                createAssetFromProduct(lstUniqueQuoteLine,mapOfQuoteandAccWithCommonRCID);
            }
   }
   
  
   /*
   This method will create an Asset record and will fill related values from quote, quoteLine, product etc
   One asset corrosponds to one product in a quote line item.List of all such assets will be inserted   
   */
   private static void createAssetFromProduct(list<SBQQ__QuoteLine__c> lstEligibleQuoteLines,map<id,id> mapQuoteAccount)
   {
        system.debug('------lstEligibleQuoteLines-------'+ lstEligibleQuoteLines.size());
        list <Asset> lstNewAsset = new list<Asset>();
        list <Asset> lstModifiedAsset = new list<Asset>();
        
        map<string,Asset> mapExistingAsset = new map<string,Asset>();
        list<SBQQ__QuoteLine__c> lstExistingQuoteLinesInAsset = new list<SBQQ__QuoteLine__c>();
        list<SBQQ__QuoteLine__c> lstNewQuoteLinesInAsset = new list<SBQQ__QuoteLine__c>();
        for(Asset a : [Select Id,AccountId,Name,CurrencyIsoCode,
                        ContactId,Description,InstallDate,Price,Product2Id,Quantity,Status,UsageEndDate,
                        ParentId__c,SBQQ__QuoteLine__c from Asset where SBQQ__QuoteLine__c in : lstEligibleQuoteLines and isDeleted = false])
        {
            mapExistingAsset.put(a.SBQQ__QuoteLine__c,a);       
        
        }
        integer i = 0;
        while (i < lstEligibleQuoteLines.size())
        {
            if(mapExistingAsset.containsKey(lstEligibleQuoteLines.get(i).id))
            {
                lstExistingQuoteLinesInAsset.add(lstEligibleQuoteLines.get(i));
                
            }else
            {
                lstNewQuoteLinesInAsset.add(lstEligibleQuoteLines.get(i));
                
            }
            i++;
        }           
        
        //In case of new Assets
        for (SBQQ__QuoteLine__c ql : lstNewQuoteLinesInAsset)           
        {               
            if(String.isnotBlank(ql.SBQQ__Product__r.name))
            {
                Asset newAsset = new Asset();
                newAsset.AccountId = mapQuoteAccount.get(ql.SBQQ__Quote__r.ID);
                newAsset.Name = ql.SBQQ__Product__r.name;
                newAsset.CurrencyIsoCode = ql.SBQQ__Product__r.CurrencyIsoCode;
                newAsset.ContactId = ql.SBQQ__Quote__r.SBQQ__PrimaryContact__c;
                newAsset.Description = ql.SBQQ__Product__r.Description;
                newAsset.InstallDate = ql.CreatedDate.date();
                newAsset.Price = ql.SBQQ__NetTotal__c;
                newAsset.Product2Id = ql.SBQQ__Product__c;
                newAsset.Quantity = ql.SBQQ__Quantity__c;
                newAsset.Status = ql.SBQQ__Quote__r.SBQQ__Status__c;
                newAsset.UsageEndDate = ql.SBQQ__Quote__r.SBQQ__EndDate__c;
                //newAsset.ParentId__c = ql.SBQQ__RequiredBy__r.SBQQ__Product__c;
                newAsset.SBQQ__QuoteLine__c = ql.ID;//use this field to check new update or insert for null
                
                lstNewAsset.add(newAsset);  
            }                   
        } 
        
        //In case of existing assets
                for (SBQQ__QuoteLine__c ql : lstExistingQuoteLinesInAsset)          
        {               
            if(String.isnotBlank(ql.SBQQ__Product__r.name))
            {
                Asset newAsset = mapExistingAsset.get(ql.ID);
                newAsset.AccountId = mapQuoteAccount.get(ql.SBQQ__Quote__r.ID);
                newAsset.Name = ql.SBQQ__Product__r.name;
                newAsset.CurrencyIsoCode = ql.SBQQ__Product__r.CurrencyIsoCode;
                newAsset.ContactId = ql.SBQQ__Quote__r.SBQQ__PrimaryContact__c;
                newAsset.Description = ql.SBQQ__Product__r.Description;
                newAsset.InstallDate = ql.CreatedDate.date();
                newAsset.Price = ql.SBQQ__NetTotal__c;
                newAsset.Product2Id = ql.SBQQ__Product__c;
                newAsset.Quantity = ql.SBQQ__Quantity__c;
                newAsset.Status = ql.SBQQ__Quote__r.SBQQ__Status__c;
                newAsset.UsageEndDate = ql.SBQQ__Quote__r.SBQQ__EndDate__c;
                //newAsset.ParentId__c = ql.SBQQ__RequiredBy__r.SBQQ__Product__c;
                //newAsset.SBQQ__QuoteLine__c = ql.ID;//use this field to check new update or insert for null
                
                lstModifiedAsset.add(newAsset);  
            }                   
        }
        system.debug('-------lstnewAsset-------'+lstNewAsset.size());
        system.debug('-------lstModifiedAsset-------'+lstModifiedAsset.size());
        system.debug('-------lstModifiedAsset-------'+lstModifiedAsset);
        
        try
        {  
            if (lstNewAsset.size()>0)
            {                               
                insert lstNewAsset ;
            } 
            else if (lstModifiedAsset.size()>0)
            {
                update lstModifiedAsset;
            
            }               
            
        }catch (DmlException e) 
        {
            System.debug(e.getMessage());
        }                   
   }

    /*
   This method will pass quote line through a map to remove duplicate entries
   Key of map will be combination of product name and parent quoteline Item name   
   */
   private static list<SBQQ__QuoteLine__c> removeDuplicateQuoteLines(list<SBQQ__QuoteLine__c> lstQuoteLinesGroup)
   {
        map <string,SBQQ__QuoteLine__c> mapUniqueQuoteLines  = new map <string,SBQQ__QuoteLine__c>();
        list<SBQQ__QuoteLine__c> lstUniqueQuoteLines =  new List<SBQQ__QuoteLine__c>();
        string KeyCombination = '';
        
        for (SBQQ__QuoteLine__c ql : lstQuoteLinesGroup)        
        {
            KeyCombination = ql.SBQQ__Product__r.name + ql.SBQQ__RequiredBy__r.name;
            mapUniqueQuoteLines.put(KeyCombination,ql);
        
        }
        system.debug('-----mapUniqueQuoteLines.Values(----'+mapUniqueQuoteLines.Values().size());
            return mapUniqueQuoteLines.Values();   
   }
   

   //Associate RCID for Quote and Account 
   private static map<id,id> getQuoteAccountMap (list<SBQQ__QuoteLine__c> lstQLines)
   {
        list<string> lstrcid = new list<string>();
        list <Account> lstAccWithrcID = new List<Account>();
        map<string,Account> mapAccountWithRcID = new map <string,Account>();
        map<id,id> mapOfQuoteandAccWithRCID = new map<id,id>();
        
        for (SBQQ__QuoteLine__c qLines : lstQLines)
        {           
            lstrcid.add(qLines.SBQQ__Quote__r.SBQQ__Opportunity__r.Web_Account__r.RCID__c);                  
        }
                    
        if(lstrcid.size()>0)
            lstAccWithrcID = [Select Id, RCID__c from Account where RCID__c in :lstrcid ];
                    
        if(lstAccWithrcID.size()>0)
        {
            for (Account a :lstAccWithrcID)
            {
                mapAccountWithRcID.put(a.RCID__c,a);
            }
        
        
            for (SBQQ__QuoteLine__c  qLines1:lstQLines )
            {
                if (mapAccountWithRcID.containsKey(qLines1.SBQQ__Quote__r.SBQQ__Opportunity__r.Web_Account__r.RCID__c))
                {
                    mapOfQuoteandAccWithRCID.put(qLines1.SBQQ__Quote__r.id,(mapAccountWithRcID.get(qLines1.SBQQ__Quote__r.SBQQ__Opportunity__r.Web_Account__r.RCID__c)).id);
                }
             
            }
        }
        return mapOfQuoteandAccWithRCID;
   
   
   }
}