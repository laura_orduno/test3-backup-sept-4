/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData = true)
private class smb_test_AddressValidationUtility {

    static testMethod void myUnitTest() {
    	
    	
       PageReference ref = new PageReference('/a00/e'); 
       //PageReference ref = Page.smb_New_Case_From_Account;
       //Test.setCurrentPage(ref);     	
    	
    	
	   RecordType recordType = [Select r.Id, r.DeveloperName From RecordType r where r.SobjectType = 'Account' and r.DeveloperName='RCID' limit 1];
	        
	   Account acc1 = new Account(Name='Testing Software', BillingCountry = 'US', BillingState = 'IL', recordTypeId=recordType.Id , No_Chronic_Incidents__c =10.0);
	   insert acc1;    	
        // TO DO: implement unit test
       
        smb_AddressValidationUtility smb = new smb_AddressValidationUtility();
        

        smb_AddressValidationUtility.Address add = new smb_AddressValidationUtility.Address();
        add.addressLine1 = 'qwert';
        add.addressLine2 = 'asdfg';
        add.addressLine3 = 'zxcvb';
        add.city = 'Noida';
        add.seladdrType = 'Type';
        add.seladdrForm = 'Form';  
        add.provState = 'state';
        add.country='U.S';
        add.postalCode = '12345678';
        add.counter = 1; 
        string addCont = add.addressConcat;
        
        
        //smb_AddressValidationUtility.ValidateAddressResponse valAddRes = new  smb_AddressValidationUtility.ValidateAddressResponse();
        smb_AddressValidationUtility.ValidateAddressResponse valAdd = new smb_AddressValidationUtility.ValidateAddressResponse();
        
        list<String> errorMsg = new list<String>{'asd', 'dhb'};
        valAdd.errorMessages = errorMsg;
        
        list<String> infoMsg = new list<String>{'kjh','asd'};
        valAdd.infoMessages = infoMsg;
        
        list<smb_AddressValidationUtility.Address> returnAdd = new list<smb_AddressValidationUtility.Address>{add};
        valAdd.returnedAddresses = returnAdd;     
        
        valAdd = smb_AddressValidationUtility.validate(add); 
        String toString = add.toString();
     
        
        //valAdd = smb_AddressValidationUtility.processResponse  
        smb_AddressValidationUtility.createAddressFromAccount(acc1 , 'Billing');
        smb_AddressValidationUtility.createAddressFromAccount(acc1 , 'Shipping');        
        
        list<string> mesg = new list<string>{'test', 'test2'};
        AddressValidationException smbExp = new AddressValidationException(mesg);
        
       
        
    }
}