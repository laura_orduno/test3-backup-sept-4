/**
 * @author Dane Peterson, Traction on Demand
 * @description Class to handle notifications for B2BService
 * @created Nov 1, 2017
 */


public class B2BService_NotificationHelper {


    private static Boolean ranOnceUpdated = false;
    private static Boolean ranOnceCanceled = false;
    private static Set<String> ALL_PORTAL_CASE_ORIGINS = new Set<String>{PortalConstants.MBR_ORIGIN, PortalConstants.VITIL_ORIGIN, PortalConstants.MYACCOUNT_ORIGIN, PortalConstants.ENTP_ORIGIN};
    private static Set<String> NONLYNX_PORTAL_CASE_ORIGINS = new Set<String>{PortalConstants.MBR_ORIGIN, PortalConstants.MYACCOUNT_ORIGIN};
    private static Set<String> LYNX_PORTAL_CASE_ORIGINS = new Set<String>{PortalConstants.VITIL_ORIGIN, PortalConstants.ENTP_ORIGIN};


    public static void processComments(List<Case> cases, Map<Id, Case> oldMap){
        Set<Id> executeCanceled = new Set<Id>();
        Set<Id> executeUpdated = new Set<Id>();
        Set<Id> executeNew = new Set<Id>();

        for(Case c : cases){
            System.debug('Traction: In Cases Loop');
            if(isCanceled(c, oldMap)){
                executeCanceled.add(c.Id);
            } else if(isUpdated(c, oldMap)){
                executeUpdated.add(c.Id);
            } else if(isNewLynx(c, oldMap) || isNew(c,oldMap)){
                executeNew.add(c.Id);
            }
        }

        if(!executeCanceled.isEmpty()){
            executeCanceled(executeCanceled);
        }

        if(!executeUpdated.isEmpty()){
            executeUpdated(executeUpdated);
        }

        if(!executeNew.isEmpty()){
            executeNew(executeNew);
        }
    }

    public static void sendNotification(List<Case> cases, String emailTemplate, String emailId){
        if(emailTemplate == '' || emailTemplate == null || emailId == '' || emailId == null ){
            return;
        }

        for(Case c : cases){
            List<String> toAddresses = c.NotifyCollaboratorString__c.split(';');
            if (toAddresses.size() > 0) {
                PortalUtils.sendTemplatedCaseEmails(toAddresses, emailId, emailTemplate, c.Id);
            }
        }
    }

     /**
    *    Determine if a case is considered new (When a Lynx Ticket Number is added)
    *
    * @author Dane Peterson, Traction on Demand
    * @date 2017-10-15
    * @param Case c | The record being checked for status changed
    * @param Map<Id,Case> oldMap | A map of the previous values of cases that have been modified in this trigger context
    */
    public static Boolean isNewLynx(Case c, Map<Id,Case> oldMap){
        System.debug('Checking Lynx' );
        return c.NotifyCollaboratorString__c != null
            && c.NotifyCollaboratorString__c.length() > 0
            && c.Lynx_Ticket_Number__c != null
            && c.Lynx_Ticket_Number__c.length() > 0
            && oldMap != null
            && oldMap.containsKey(c.id) 
            && oldMap.get(c.id).Lynx_Ticket_Number__c != c.Lynx_Ticket_Number__c
            && LYNX_PORTAL_CASE_ORIGINS.contains(c.origin);
    }

    /**
   *    Determine if a case has been created
   *
   * @author Dane Peterson, Traction on Demand
   * @date 2017-10-15
   * @param Case c | The record being checked for status changed
   * @param Map<Id,Case> oldMap | A map of the previous values of cases that have been modified in this trigger context
   */
    public static Boolean isNew(Case c, Map<Id,Case> oldMap){
        System.debug('Checking New' );
        return c.NotifyCollaboratorString__c != null
            && c.NotifyCollaboratorString__c.length() > 0
            && (oldMap == null || !oldMap.containsKey(c.id))// does contain the ID.
            && NONLYNX_PORTAL_CASE_ORIGINS.contains(c.origin);

    }

    /**
    *    Determine if a case status has been changed to cancel or was closed on create and there is a need to notify collaborators. 
    *
    * @author Dane Peterson, Traction on Demand
    * @date 2017-10-15
    * @param Case c | The record being checked for status changed
    * @param Map<Id,Case> oldMap | A map of the previous values of cases that have been modified in this trigger context
    */
    public static Boolean isCanceled(Case c, Map<Id,Case> oldMap){
        Set<String> closedStatuses = CacheQuerySelector.getClosedCaseStatuses();
        return c.NotifyCollaboratorString__c != null
        && c.NotifyCollaboratorString__c.length() > 0
        && closedStatuses.contains(c.status)
        && ALL_PORTAL_CASE_ORIGINS.contains(c.origin)
        && (oldMap == null
            || (oldMap != null && oldMap.containsKey(c.id) && !closedStatuses.contains(oldMap.get(c.id).status)));

    }

    /**
    *    Determine if a case comment has been added and there is a need to notify collaborators. 
    *
    * @author Dane Peterson, Traction on Demand
    * @date 2017-10-15
    * @param Case c | The record being checked for status changed
    * @param Map<Id,Case> oldMap | A map of the previous values of cases that have been modified in this trigger context
    */
    public static Boolean isUpdated(Case c, Map<Id,Case> oldMap){
        return c.NotifyCollaboratorString__c != null 
            && c.NotifyCollaboratorString__c.length() > 0 
            && oldMap != null
            && oldMap.containsKey(c.Id) 
            && oldMap.get(c.Id).Last_Case_Comment__c != c.Last_Case_Comment__c
            && c.Last_Case_Comment__c != null
            && ALL_PORTAL_CASE_ORIGINS.contains(c.origin);
    }


    /**
    * If a case has been changed to closed and has an origin of My Account then send emails to the collaborators providing the update.
    *
    * @author Dane Peterson, Traction on Demand
    * @date 2017-10-15
    * @param Set<Id> | Cases that have been modified in this trigger context and meet the basic criteria to be processed
    */
    @Future
    public static void executeCanceled(Set<Id> caseIds){
        List<Case> cases = [SELECT NotifyCollaboratorString__c, Origin, Status, Last_Case_Comment__c, Last_Case_Comment_Public__c, Last_Case_Comment_Submitter__c, Last_Case_Comment_Submitted_By__c, CreatedById, CreatedBy.Email, CreatedBy.Name FROM Case Where Id in :caseIds];

        if(ranOnceCanceled){
            return;
        }

        List<Case> myAccountCanceledCases = new List<Case>();
        List<Case> mbrCanceledCases = new List<Case>();
        List<Case> entpCanceledCases = new List<Case>();
        List<Case> vitilCanceledCases = new List<Case>();

        for(Case c : cases){
            if(c.origin == PortalConstants.MYACCOUNT_ORIGIN){
                myAccountCanceledCases.add(c);
            } else if(c.origin == PortalConstants.MBR_ORIGIN){
                mbrCanceledCases.add(c);
            } else if(c.origin == PortalConstants.ENTP_ORIGIN){
                entpCanceledCases.add(c);
            } else if(c.origin == PortalConstants.VITIL_ORIGIN){
                vitilCanceledCases.add(c);
            }
        }

        if(myAccountCanceledCases.size() > 0){
            sendNotification(myAccountCanceledCases, PortalConstants.MYACCOUNTCANCELTEMPLATEID, PortalConstants.MYACCOUNTFROMEMAILID);
        }

        if(mbrCanceledCases.size() > 0){
            sendNotification(mbrCanceledCases, PortalConstants.MBRCANCELTEMPLATEID, PortalConstants.MBRFROMEMAILID);
        }

        if(entpCanceledCases.size() > 0){
            sendNotification(entpCanceledCases, PortalConstants.ENTPCANCELTEMPLATEID, PortalConstants.ENTPFROMEMAILID);
        }

        if(vitilCanceledCases.size() > 0){
            sendNotification(vitilCanceledCases, PortalConstants.VITILCANCELTEMPLATEID, PortalConstants.VITILFROMEMAILID);
        }

        ranOnceCanceled = true;
    }

    /**
    * If a case is considered New than send appropriate emails.
    *
    * @author Dane Peterson, Traction on Demand
    * @date 2018-01-12
    * @param Set<Id> | Cases that have been modified in this trigger context and meet the basic criteria to be processed
    */
    @Future
    public static void executeNew(Set<Id> caseIds){
        List<Case> cases = [SELECT NotifyCollaboratorString__c, Status, Origin, Last_Case_Comment__c, Last_Case_Comment_Public__c, Last_Case_Comment_Submitter__c, Last_Case_Comment_Submitted_By__c, CreatedById, CreatedBy.Email, CreatedBy.Name FROM Case Where Id in :caseIds];

        if(ranOnceCanceled){
            return;
        }
        
        List<Case> vitilMatchingCases = new List<Case>();
        List<Case> entpMatchingCases = new List<Case>();
        List<Case> mbrMatchingCases = new List<Case>();
        List<Case> myAccountMatchingCases = new List<Case>();


        for(Case c : cases) {
            if (c.Origin == PortalConstants.MBR_ORIGIN && PortalConstants.MBRNEWTEMPLATEID != null) {
                mbrMatchingCases.add(c);
            }
            if (c.Origin == PortalConstants.MYACCOUNT_ORIGIN && PortalConstants.MYACCOUNTNEWTEMPLATEID != null) {
                myAccountMatchingCases.add(c);
            }
            if (c.Origin == PortalConstants.VITIL_ORIGIN && PortalConstants.VITILNEWTEMPLATEID != null) {
                vitilMatchingCases.add(c);
            }

            if (c.Origin == PortalConstants.ENTP_ORIGIN && PortalConstants.ENTPNEWTEMPLATEID != null) {
                entpMatchingCases.add(c);
            }
        }

        if(mbrMatchingCases.size() > 0){
            sendNotification(mbrMatchingCases, PortalConstants.MBRNEWTEMPLATEID, PortalConstants.MBRFROMEMAILID);
        }

        if(myAccountMatchingCases.size() > 0) {
            sendNotification(myAccountMatchingCases, PortalConstants.MYACCOUNTNEWTEMPLATEID, PortalConstants.MYACCOUNTFROMEMAILID);
        }
        if(vitilMatchingCases.size() > 0){
            sendNotification(vitilMatchingCases, PortalConstants.VITILNEWTEMPLATEID, PortalConstants.VITILFROMEMAILID);
        }

        if(entpMatchingCases.size() > 0){
            sendNotification(entpMatchingCases, PortalConstants.ENTPNEWTEMPLATEID, PortalConstants.ENTPFROMEMAILID);
        }

    }

    /**
    * If a new comment has been added to the case then notify the collaborators 
    *
    * @author Dane Peterson, Traction on Demand
    * @date 2017-10-15
    * @param Set<Id> | Cases that have been modified in this trigger context and meet the basic criteria to be processed
    */
    @Future
    public static void executeUpdated(Set<Id> caseIds){
        List<Case> cases = [SELECT NotifyCollaboratorString__c, Origin, Status, Last_Case_Comment__c, Last_Case_Comment_Public__c, Last_Case_Comment_Submitter__c, Last_Case_Comment_Submitted_By__c, CreatedById, CreatedBy.Email, CreatedBy.Name FROM Case Where Id in :caseIds];

        if(ranOnceUpdated){
            return;
        }

        // Lists to hold maching cases that will need notifications sent to collaborators. 
        List<Case> myAccountMatchingCases = new List<Case>();
        List<Case> vitilMatchingCases = new List<Case>();
        List<Case> entpMatchingCases = new List<Case>();
        List<Case> mbrMatchingCases = new List<Case>();

        for(Case c : cases){
            if(c.Last_Case_Comment_Public__c){
                if(c.origin == PortalConstants.MYACCOUNT_ORIGIN){
                    //MyAccount
                    myAccountMatchingCases.add(c);
                } else if ((c.Origin == PortalConstants.VITIL_ORIGIN && PortalConstants.VITILTEMPLATEID != null) && (c.Last_Case_Comment_Submitted_By__c != null || c.Last_Case_Comment__c.startsWith(c.CreatedBy.Email))){ 
                    // VITIL
                    vitilMatchingCases.add(c);
                } else if((c.origin == PortalConstants.ENTP_ORIGIN && PortalConstants.ENTPUPDATETEMPLATEID != null) && (c.Last_Case_Comment_Submitted_By__c != null || c.Last_Case_Comment__c.startsWith(c.CreatedBy.Email))){
                    // ENTP
                    entpMatchingCases.add(c);
                } else if ((c.origin == PortalConstants.MBR_ORIGIN && PortalConstants.MBRUPDATETEMPLATEID != null) && (c.Last_Case_Comment_Submitted_By__c != null || c.Last_Case_Comment__c.startsWith(c.CreatedBy.Email))){ 
                    // MBR
                    mbrMatchingCases.add(c);
                }
            }
        }

        if(myAccountMatchingCases.size() > 0){
            sendNotification(myAccountMatchingCases, PortalConstants.MYACCOUNTUPDATETEMPLATEID, PortalConstants.MYACCOUNTFROMEMAILID);
        }
        if(vitilMatchingCases.size() > 0){
            sendNotification(vitilMatchingCases, PortalConstants.VITILTEMPLATEID, PortalConstants.VITILFROMEMAILID);
        }
        if(entpMatchingCases.size() > 0){
            sendNotification(entpMatchingCases, PortalConstants.ENTPUPDATETEMPLATEID, PortalConstants.ENTPFROMEMAILID);
        }
        if(mbrMatchingCases.size() > 0){
           sendNotification(mbrMatchingCases, PortalConstants.MBRUPDATETEMPLATEID, PortalConstants.MBRFROMEMAILID);
        }
        ranOnceUpdated = true;
    }
}