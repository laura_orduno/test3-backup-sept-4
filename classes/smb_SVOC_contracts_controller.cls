/*
############################################################################
# Last Modified by......: Nitin Saxena
# Last Modified Date....: 14-Jun-2016
# Description...........: changed smb_ContractManagementSvcTypes_v3 to cms3_1ContractManagementSvcTypes_v3.1.
  The essence of the change was to accommodate a parameter type change (long to String) for 
  the findContractAmendmentsByAssociateNum() operation on the CMS web service endpoint.  A new operation was introduced on CMS V3, 
  findContractAmendmentsByAssociateNumber(), in order to accommodate this version change. 
  Due to SOA Governance policies, the web service endpoint was not to be versioned.  But instead, adding two new operations that 
  referenced an updated XSD version of 3.1 was implemented by the BTO CMS Development team.  As a result, we are still 
  naming this service within SFDC as 3.1 to show that there was a change to the service endpoint contract. 
  Technically, adding operations should result in a new end point version.  But, BTO decided against this in order to meet
  the TQ project timelines.  This is why you may see the endpoint URL pointing to version 3.0 of the web service, 
    even though it is actually version 3.1 due to the addition of two new operations
#################################################################
*/

public class smb_SVOC_contracts_controller
{
    public class wrapCls
    {
        // declare variables just like a grown-up class!
        public String sLocations {get;set;}
        public list<String> sProducts {get;set;}

        // constructor, again just like a grown-up class.
        public wrapCls(String loc,list<String> pro) {
            sProducts = new list<String>();
            // Set initial variables
            sLocations = loc;
            sProducts.addall(pro);
        }
    }
    
    public Account acct {get; set;}
    public list<smb_contract> lstContracts{get;set;} 
    //public list<smb_contract> lstcontractcms{get;set;}
    public list<smb_Amendments> lstAmendments{get;set;} 
    List <cms3_1ContractManagementSvcTypes_v3.ContractDetailList>lstContDetail = new list<cms3_1ContractManagementSvcTypes_v3.ContractDetailList>(); // changed from cms3_1ContractManagementSvcTypes_v3 to smb_ContractManagementSvcTypes_v3 
    public transient list<String> lstBTN {get;set;}
    public map<String,String> mapContractNumCompanyName = new map<String,String>();
    public String companyLegalName{get;set;}
    smb_ContactCallOuts cc = new smb_ContactCallOuts();
    public transient String customerLName {get;set;}
    public transient list<wrapCls> lstWrap {get;set;}
    public map<String, list<String>> mapANumAndLocationList = new map<String, list<String>> ();
    public map<String,list<String>> mapAddressAndDetailList;
    //need  to declare it for service location details. Claim it as  public because test class needs to access it
    public Map <String, Map<String, List<String>>> mapANumberandAddressandDetails;
    public String strConNum;
    public String strConAssoNum;
    public integer rowCounter {get;set;}
    public String selectedContractId {get;set;}
    public Set<String> activeContractStatuses {get; set;}
    public String userLanguage {get; set;}   
    //Need to declare it as the global private variable for TQ contract's BTN list
    private Set<String> tqContractNumSet = new Set<String>();
    private Map<String, List<String>> mapContractBTNList = new Map<String, List<String>>();

    public String HumboldtUrl {
      get {
        smb_ContextualLink__c humboldtSettings = smb_ContextualLink__c.getInstance('Humboldt');
        if (humboldtSettings == null) return null;
        return humboldtSettings.Location__c;
      }
    }
    
    public PageReference init() {
        System.debug('Enter init() method');
        //initalize these three public variables referenced by vf page.
        userLanguage = UserInfo.getLanguage();  
        lstContracts = new list<smb_contract>(); 
        lstBTN = new list<String>();
        
        Id accountId = ApexPages.currentPage().getParameters().get('accountId');
        strConNum = ApexPages.currentPage().getParameters().get('conNum');
        strConAssoNum = ApexPages.currentPage().getParameters().get('conAssoNum'); 
        
        acct = [SELECT  Id, ParentId, Name, Tax_Exempt_Status__c, SicDesc, Quote_Threshold__c, Phone, PIC__c, Wireline_PIN__c, Wireless_PIN__c, 
                        Legal_Business_Name__c, DUNS__c, DNTC__c, REG_STATUS__C, LEGAL_NAME_CHECKED__C, LEGAL_NAME_LAST_CHECKED__C, DELINQUENCY_FLAG__C, TRADE_STYLE_NAME__C, CREDIT_PORTAL_TIER__C, Credit_Standing__c, Operating_Name_DBA__c, 
                        Credit_Class__c, CREDITREF_NUM__C, BillingStreet, BillingState, BillingPostalCode, BAN__c,
                        BillingCountry, BillingCity, Account_Open_Since__c, Account_Close_Day__c, Partner_Type__c, Industry,
                        Legacy_Client__c, BAN_CAN__c, Registration_Incorporation_Number__c, Churn_Threat_Flag__c, Fraud__c,
                        Chronic__c, Description, Account_Status__c, Incorporation_Jurisdiction__c, Type_of_Business__c, 
                        Customer_Segment__c, smb_Strategic_Customer_Icon_Name__c, smb_Churn_Threat_Icon_Name__c, Owner.Name,
                        smb_Fraud_Risk_Icon_Name__c, smb_Chronic_Icon_Name__c, Strategic__c, CBUCID_RCID__c, RCID__c, CustProfId__c
                        FROM Account WHERE Id =: accountId];
                           
        //fill contract List
        try
        {
            findContracts(acct.CustProfId__c,acct.id,acct.parentid);
            activeContractStatuses = obtainActiveContractStatuses();
        }
        catch (Exception e)
        {
            System.debug('Exception=' + e.getStackTraceString());
            return null;
        }
          
        if(String.isNotBlank(strConNum) && String.isNotBlank(strConAssoNum)){ 
       	 	System.debug('strConAssoNum=' + strConAssoNum);    
        	if( strConAssoNum == 'undefined' || strConAssoNum == 'null' ) {
               strConAssoNum = '0';
            
         	}
            // Is this part needed? Does not seem like it; therefore, commented out.
//         	findAmendments(strConNum, integer.valueof(strConAssoNum));
        }

            dateValueConversion(); 
        
            return null;
    }

    public void findContracts(String customerId,id accountId,id accountParentId){
        
        System.debug('enter findContracts with customerId=' + customerId);
        
        try{
            cms3_1ContractManagementSvcTypes_v3.ContractList contracts = cc.getContractsByCustomerId(customerId);
            System.debug('contracts size=>'+contracts.contract.size());           
            if (contracts.contract!=null){
                integer counter=0;
                
                List<String> tqBtnList = new List<String>();
              
                for (cms3_1ContractManagementSvcTypes_v3.Contract contract : contracts.contract) 
                {                    
                    System.debug('checking contract for contract.contactId='+ contract.contactId + ' and contractNum=' + contract.contractNum);
                    
                    if(null==contract.contractAssociateNum) {
                        contract.contractAssociateNum='0';
                    }
                    //Added by Nitin
                      if(null==contract.contractAmendmentNumber) {                      
                        contract.contractAmendmentNumber='0';
                    }
                    // Condition added by Arvind as part of TF Case#01877768 to exclude the Archiv?? Contracts..
                   // Get All the statuses defined under SVOC_Contract_List_View_Display_Statuses__c custom setting.
                    //List<SVOC_Contract_List_View_Display_Statuses__c> ArchiveStatuses = SVOC_Contract_List_View_Display_Statuses__c.getall().values();
                        if (!(SVOC_Contract_List_View_Display_Statuses__c.getAll().keyset()).contains(contract.contractStatus))
                        {
                        	lstContracts.add(new smb_Contract(contract));
                        }
                    // Addition end TF Case#01877768.
                    if(strConNum!=null && strConAssoNum!=null){
                        if(contract.contractNum==strConNum && zeroFill(contract.contractAssociateNum)==strConAssoNum){
                            rowCounter=counter;
                        }
                        
                    }
                    counter++;
                    //lstContDetail.add(contract.contractDetailList);
                    mapContractNumCompanyName.put(contract.contractNum,contract.customerLegalName);
                    System.debug('---------contract='+contract);
                    if (null  != contract.contractDetailList)
                    {
                        //System.debug('---------contract.contractDetailList=' + contract.contractDetailList);
                        List<String> btnListOfContract = new List<String>();                      
                        Boolean isTqContract = false;                        
                        for (cms3_1ContractManagementSvcTypes_v3.ContractDetail CDL :contract.contractDetailList.ContractDetail )
                        {                          
                            if(CDL.contractId == null)
                            {
                                tqContractNumSet.add(contract.contractNum);
                                isTqContract = true;
                            	System.debug('is TQ contract becase contractDetail.contractId is null and its contractStatus='  + contract.contractStatus + ' and tqContractNumSet.size=' + tqContractNumSet.size()); 
                            }
                            else
                            {
                              	System.debug('is not TQ contract because contractDetail.contractId=' + CDL.contractId ); 
                            }    
                              
                            if( null != CDL.billingTelephoneNumList && null != CDL.billingTelephoneNumList.billingTelephoneNum)
                            {
                                //billingTelephoneNum is an array of String
                                for(String individualBTN : CDL.billingTelephoneNumList.billingTelephoneNum)
                                {
                                    if(!String.isEmpty(individualBTN))
                                    {    
                                        if(isTqContract)
                                        {//contract status could be Archivé                                           
                                           System.debug('for TQ contract individualBTN=' + individualBTN  + ' and its contractStatus=' + contract.contractStatus  + ' and its contractNum=' + contract.contractNum);
                                           tqBtnList.add(individualBTN);                        
                                        }
                                        else
                                        {
                                            // lstBTN.add(formatphonenumber(individualBTN));
                                             //bug fix : modified to display BTNs associated with a selected contract
                                            btnListOfContract.add(formatphonenumber(individualBTN));
                                         }                                       
                                     }//BTN is not empty
                                  }//end looping BTN
                            }//billingTelephoneNumList and billingTelephoneNum is not null                            
                        }//end of lopping ContractDetailList
                        
                        if(isTqContract)
                        {
                           // System.debug('Yes, it is TqContract + tqBtnList.size()=' + tqBtnList.size() + ' btnListOfContract.size()=' + btnListOfContract.size());                            
                            if(!tqBtnList.isEmpty())
                            {
                                //System.debug('tqBtnList.size()=' + tqBtnList.size());  
                                btnListOfContract.clear();                               
                                btnListOfContract.addAll(tqBtnList); 
                                //System.debug('tqContractNumSet.size()=' + tqContractNumSet.size()  + ' and btnListOfContract.size=' + btnListOfContract.size());
                                for(String oContractNum : tqContractNumSet)
                                {                                  
                                   // System.debug('oContractNum=' + oContractNum); 
                                    mapContractBTNList.put(oContractNum, btnListOfContract);
                                }
                            }
                        }
                        
                        if(!btnListOfContract.isEmpty())
                        {
                            btnListOfContract.sort();
                            String contractNumkey = contract.contractNum + '-' + contract.contractAssociateNum + '-' + contract.contractAmendmentNumber;
                            mapContractBTNList.put(contractNumkey, btnListOfContract);
                        }                       
                    }//contract.contractDetailList not null                    
                }//end looping cms3_1ContractManagementSvcTypes_v3.Contract
            }
        }
        catch(exception e)
        {
            system.debug(e.getmessage());
        }

        list<id> allAccountIdsInHierarchy = smb_AccountUtility.getAccountsInHierarchy(accountId, accountParentId);
        list<contract> vlocityContracts=[select id,recordtype.name,contractnumber,contractterm,status,effective_date__c 
                                         from contract where accountid=:allAccountIdsInHierarchy order by createddate desc limit 50000];
        for(contract vlocityContract:vlocityContracts){
            lstContracts.add(new smb_Contract(vlocityContract));
        }
        lstContracts.sort();
              
    }
    public PageReference getAmendmentDetails() {
        String contractNum;
        String contractId;
        integer associateNum;
        String amendmentNum;
        map<String,String> params=apexpages.currentpage().getparameters();
        String cType=params.get('cType');
        contractId=params.get('contractId');
        this.selectedContractId = contractId;
        pagereference returnPage=null;
        //Added as part of defect Id#59318 to fix the rendering issue..
        lstWrap = new List<wrapCls>();
        if(String.isnotblank(cType)){
            if(cType.equalsignorecase('humbolt')){
                contractNum = String.valueof(params.get('cNum'));
                associateNum = integer.valueOf(params.get('cAssoNum'));
                amendmentNum = String.valueof(params.get('cAmendNum'));
                findAmendments(contractNum,associateNum,amendmentNum);  
            }else if(cType.equalsignorecase('vlocity')){
                returnPage=new pagereference('/'+contractId);
                returnPage.setredirect(true);
            }
        }
        return returnPage;
    }
  
    public void findAmendments(String conNum, Integer conAssoNum, String conAmendNum){
        if (null != conNum){
            String contractNumKey = conNum + '-' + conAssoNum + '-' + conAmendNum;
            lstBTN = mapContractBTNList.get(contractNumKey);
        }

        if(conAssoNum>-1){
            customerLName = mapContractNumCompanyName.get(String.valueof(conNum));
            
            String ServiceLocation;
            list<string> lstLocation = new list<String>();          
            lstAmendments = new list<smb_Amendments>();
            mapAddressAndDetailList = new map<String,list<String>>();
            mapANumberandAddressandDetails = new Map <String,Map<String,List<String>>>();
            List<cms3_1ContractManagementSvcTypes_v3.ContractAmendmentList> lstAmendmentList = cc.getContractAmendmentsByAssociateNum(conNum,conAssoNum);
            if(lstAmendmentList == null || lstAmendmentList.isEmpty()){
                //system should do nothing
                 return;
            }
            
            System.debug('lstAmendmentList.size='+ lstAmendmentList.size());
            
            for (cms3_1ContractManagementSvcTypes_v3.ContractAmendmentList singlelst : lstAmendmentList){
                if (singlelst.contractAmendment == null){ 
                    return ;
                }
                
                //each amendment
                for (cms3_1ContractManagementSvcTypes_v3.ContractAmendment amendment : singlelst.contractAmendment) {                   
                    System.debug('One amendment=' + amendment);
                    lstAmendments.add(new smb_Amendments(amendment));  
                    //each Address in amendment
                    if(null != amendment.serviceAddressList && null != amendment.serviceAddressList.serviceAddress )
                    {
                        Integer i = 0;
                        //System.debug('---------amendment Still here='+amendment);
                        for(cms3_1ContractManagementSvcTypes_v3.ServiceAddress sAddress:amendment.serviceAddressList.serviceAddress)                           
                        {
                            ServiceLocation = (null != sAddress.houseNum ?sAddress.houseNum + ' ' : '') + (null != sAddress.streetName?sAddress.streetName+' ':'')
                                +(null !=sAddress.city?sAddress.city+' ':'') + (null !=sAddress.provStateCd?sAddress.provStateCd+' ':'')
                                +(null !=sAddress.postalCd?sAddress.postalCd+' ':'') + (null !=sAddress.countryCd?sAddress.countryCd+' ':'')
                                +(null !=sAddress.countryName?sAddress.countryName+' ':'') +(null !=sAddress.provStateName?sAddress.provStateName:'');
                             
                            //string uniqueCounter = string.valueof(i)+'@#$';
                            // Updated by Arvind as part of Production Defect Id #59318..
                            String uniqueCounter = zeroFill(amendment.contractAmendmentNumber)+'@#$';
                            ServiceLocation = uniqueCounter+ServiceLocation;
                            lstLocation.add(ServiceLocation);
                             
                            List<String>  lstProduct = new list<String>();
                            //each service detail in address
                            if (null != sAddress.serviceDetailList && null != sAddress.serviceDetailList.serviceDetail)
                            {                              
                                for(cms3_1ContractManagementSvcTypes_v3.ServiceDetail sDetail :sAddress.serviceDetailList.serviceDetail)
                                    //for(cmsv3_ContractManagementSvcTypes_v3.ServiceDetail sDetail :sAddress.serviceDetailList.serviceDetail)
                                {                      
                                    lstProduct.add(sDetail.serviceName);                    
                                }//each service detail in address ends                                
                                mapAddressAndDetailList.put(ServiceLocation, lstProduct);
                                i++;
                                System.debug('got service address and the increased mapAddressAndDetailList.size='+ mapAddressAndDetailList.size());
                            }                           
                            else
                            {
                               System.debug('either address.serviceDetailList or address.serviceDetailList.serviceDetail is null but application will still add in service location');  
                               String emptyProduct = ' ';
                               lstProduct.add(emptyProduct);
                               mapAddressAndDetailList.put(ServiceLocation, lstProduct);
                            } 
                           
                        } //loop each Address in amendment ends 
                        
                        System.debug('---------amendment.contractAmendmentNumber='+ amendment.contractAmendmentNumber);
                        mapANumberandAddressandDetails.put(zeroFill(amendment.contractAmendmentNumber), mapAddressAndDetailList);
                        //System.debug('---------mapANumberandAddressandDetails size='+mapANumberandAddressandDetails.size());                       
                    }
                } //each amendment ends                
            }            
            lstAmendments.sort();
        }            
    }
    
    public void getServiceDetails(){
      // Re-Instantiate List
      lstWrap = new List<wrapCls>();
      String ServiceLocation; 
      List<String> lstProduct = new list<String>();   
      String amndmentNumber =ApexPages.currentPage().getParameters().get('cAmndNum');
      System.debug('------Enter getServiceDetails with amndmentNumber=' + amndmentNumber);   
      if(mapANumberandAddressandDetails.containsKey(amndmentNumber))
      {
        System.debug('------mapANumberandAddressandDetails.containsKey(amndmentNumber)----'+ mapANumberandAddressandDetails.containsKey(amndmentNumber));
        for (String adr : mapANumberandAddressandDetails.get(amndmentNumber).keyset()){
         	System.debug('***************---------adr---'+adr);
         	System.debug('***************---------adr.split[1]---'+adr.substringAfterLast('@#$'));
         // Updated by Arvind as part of Production Defect Id #59318..
         	if(adr.contains(amndmentNumber+'@#$')){
         		lstWrap.add(new wrapCls(adr.substringAfterLast('@#$'),mapAddressAndDetailList.get(adr))); 
         	}
                  
         	System.debug('------lstWrap----'+lstWrap);
       }
      
      }          
    }

    public static String formatphonenumber(String cphone) {
        String fphone = cphone.replaceAll('\\\\D','');
        if (fphone.length() == 10) {
            fphone = formatphonenum(fphone);
            return fphone;
        }
        else {
            return cphone;
        }
    }

    static String formatphonenum (String s) {
        s = '(' + s.substring(0, 3) + ') ' + s.substring(3, 6) + '-' + s.substring(6);
        return s;
    }

    @TestVisible
    private Set<String> obtainActiveContractStatuses()
    {
        Set<String> activeContractStatuses = new Set<String>();

        List<Contract_Status__c> contractStatuses = Contract_Status__c.getall().values();

        for(Contract_Status__c status : contractStatuses)
        {
            activeContractStatuses.add(status.Name);
        }

        return activeContractStatuses;
    }

    @TestVisible
    private String zeroFill(String value)
    {
        if (Value == null) return null;

        if(value.length()==1)
            return '00'+value;
        else if (value.length()==2)
            return '0'+value;
        else
                return value;
    }
   
    @TestVisible
    private void dateValueConversion()
    {     
        if(lstContracts != null && !lstContracts.isEmpty())
        {
            //System.debug('userLanguage=' + userLanguage);
            //only do the conversion for userLanguage as French
            if(userLanguage.equalsIgnoreCase('fr'))
        	{            	
                for(smb_contract contract: lstContracts)
            	{               
                    contract.signedDate = translateEntoFr(contract.signedDate); 
                    contract.expiryDate = translateEntoFr(contract.expiryDate);
                    contract.effectiveDate =  translateEntoFr( contract.effectiveDate);         
                    contract.registeredDate = translateEntoFr(contract.registeredDate);
               }
        	}
        } 
     }
    
     @TestVisible
     private String translateEntoFr(String dateStr)
     {          
      return  smbLanguageUtils.translateEntoFr(dateStr);
     } 
  
}