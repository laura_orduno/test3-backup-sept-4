@isTest
public with sharing class trac_CloneAccountPlanTests {
    
    @isTest public static void testCloneAccountPlan() {
        Account sourceAccount = new Account(name='Source Account');
        insert sourceAccount;
        Account tmpAccount = [SELECT Id, RecordTypeId FROM Account WHERE Id=:sourceAccount.Id];
        System.debug(tmpAccount);

        Contact contact = new Contact();
        contact.FirstName='John';
        contact.LastName='Travolta';
        contact.AccountId = sourceAccount.Id;
        insert contact;

        Contact contact2 = new Contact();
        contact2.FirstName='Peter Ackins';
        contact2.LastName='Travolta';
        contact2.AccountId = sourceAccount.Id;
        contact2.ReportsToId = contact.Id;
        insert contact2;

        Contact tmpContact = [SELECT Id, ReportsToId FROM Contact WHERE id=:contact2.id];
        System.debug('Test contact insertion: ');
        System.debug(tmpContact);

        Account targetAccount = new Account(name='Target Account');
        insert targetAccount;
        
        ESP__c accountPlan = new ESP__c();
        accountPlan.Account__c = sourceAccount.Id;
        insert accountPlan;

        Product_Presence__c productPresence = new Product_Presence__c(ESP__c = accountPlan.Id);
        insert productPresence;

        Department__c department = new Department__c(Account__c = sourceAccount.Id);
        insert department;

        Account_Map_Data__c data = new Account_Map_Data__c(ESP__c = accountPlan.Id, Department__c = department.Id);
        insert data;

        Account_Plan_Financial__c financial = new Account_Plan_Financial__c(Account_Plan__c = accountPlan.Id);
        insert financial;

        ESP_User_Contact_Roles__c contactRole = new ESP_User_Contact_Roles__c(ESP__c=accountPlan.Id);
        contactRole.Contact__c = contact2.Id;
        insert contactRole;

        ESP_Contact_Attribute__c contactAttribute = new ESP_Contact_Attribute__c(ESP_User_Contact_Role__c = contactRole.Id);
        insert contactAttribute;

        Goal__c goal = new Goal__c(ESP__c = accountPlan.Id);
        insert goal;

        Goal__c goal2 = new Goal__c(ESP__c = accountPlan.Id);
        insert goal2;

        Objectives__c objective = new Objectives__c(Goal__c = goal.Id);
        insert objective;

        Task task = new Task(WhatId=objective.Id);
        insert task;

        Task task2 = new Task(WhatId=objective.Id);
        insert task2;

        trac_CloneAccountPlan.cloneAccountPlanService(targetAccount.Id, accountPlan.Id);
        ESP__c newAccountPlan = [SELECT Id FROM ESP__C WHERE Account__c = :targetAccount.Id LIMIT 1];

        List<ESP_User_Contact_Roles__c> newContactRoles = [SELECT Id FROM ESP_User_Contact_Roles__c WHERE ESP__c = :newAccountPlan.Id];
        //System.assertEquals(2, newContactRoles.size());

        List<Product_Presence__c> productPresenceTest = [SELECT Id FROM Product_Presence__c WHERE ESP__c = :newAccountPlan.Id];
        System.assertEquals(1, productPresenceTest.size());


        List<Goal__c> checkNewTasks = [SELECT Id FROM Goal__c WHERE ESP__c = :newAccountPlan.Id ];
        System.assertEquals(2, checkNewTasks.size());
        delete newAccountPlan;
    }
    
    @isTest public static void testRetrieveAccountPlansForCBUCID() {
        List<RecordType> rts = [SELECT Id FROM RecordType WHERE Name='CBUCID'];
        
        Account subjectAccount = new Account(name='Test Account', RecordTypeId = rts.get(0).ID);
        insert subjectAccount;

        Account existingAccount = new Account(name='Existing Account', RecordTypeId = rts.get(0).ID);
        insert existingAccount;

        ESP__c accountPlan = new ESP__c(Account__c = existingAccount.Id);
        insert accountPlan;

        trac_CloneAccountPlan.retrieveExistingAccountPlans(subjectAccount.Id);
    }

    @isTest public static void testRetrieveAccountPlansForRCID() {
        List<RecordType> rts = [SELECT Id FROM RecordType WHERE Name='RCID'];
        
        Account subjectAccount = new Account(name='Test Account', RecordTypeId = rts.get(0).ID);
        insert subjectAccount;

        Account existingAccount = new Account(name='Existing Account', RecordTypeId = rts.get(0).ID);
        insert existingAccount;

        ESP__c accountPlan = new ESP__c(Account__c = existingAccount.Id);
        insert accountPlan;

        trac_CloneAccountPlan.retrieveExistingAccountPlans(subjectAccount.Id);
    }

    @isTest public static void testRetrieveAccountPlansWithExistingPlan() {
        List<RecordType> rts = [SELECT Id FROM RecordType WHERE Name='RCID'];
        
        Account subjectAccount = new Account(name='Test Account', RecordTypeId = rts.get(0).ID);
        insert subjectAccount;
    
        ESP__c accountPlan = new ESP__c(Account__c = subjectAccount.Id);
        insert accountPlan;

        trac_CloneAccountPlan.retrieveExistingAccountPlans(subjectAccount.Id);
    }
    
}