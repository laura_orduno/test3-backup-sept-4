public class vlocity_addon_AutoCreateContractDocCntBA {

    public id versionId {get;set;}
 
    string updateParam = '';
    public id contractID{get;set;}
    public static vlocity_cmt__ContractVersion__c activeContVersion {get;set;}
    public list<vlocity_cmt__ContractVersion__c> conVersion = new list<vlocity_cmt__ContractVersion__c>();
    public boolean isUpdate { get;set;}
    public string templatePicker = '';
    
    public boolean isInConsole {
     get{
        Map<String, String> parametersMap = ApexPages.currentPage().getParameters();   
        for(String key : parametersMap.keySet()){
            if(key.trim().equalsIgnoreCase('isInConsole')){
                String value=parametersMap.get(key);
                if(value.trim().equals('true')){
                   return true;
                }
                else if(value.trim().equals('true')){
                    return false;
                }
            }
        }

        return false;
     }
     set;
   }
   
    public vlocity_addon_AutoCreateContractDocCntBA(vlocity_cmt.ContractDocumentCreationController con){ 
        contractID = ApexPages.currentPage().getParameters().get('id');
        updateParam = ApexPages.currentPage().getParameters().get('isUpdate');
        templatePicker = ApexPages.currentPage().getParameters().get('templatePicker');

        if(updateParam != null && updateParam.trim().equalsIgnoreCase('true'))
            isUpdate = true;
        else 
            isUpdate = false;
            
        system.debug('isUpdate'+ isUpdate);
       // setStatResPath(statResName);
        
    }
    
    public void attachDoc(){
        //Create new Version if the request is update
        if(isUpdate == true && contractID != null){
            versionId = vlocity_cmt.ContractServiceResource.createNewContractVersionDocument(contractID);
            System.debug('New Version ID:' + versionId);
        }else {
            // Attach a Template to the esixting Version and then attach a Doc.
            conVersion = [Select ID,vlocity_cmt__ContractId__r.Contract_Type__c ,vlocity_cmt__ContractId__r.Agreement_Type__c  from vlocity_cmt__ContractVersion__c where vlocity_cmt__ContractId__c = :contractID 
                          and vlocity_cmt__Status__c = 'Active' ]; 
            if(conVersion.size()>0 && !conVersion.isEmpty() ) {
                activeContVersion = conVersion[0];
                versionId = conVersion[0].id;
                System.debug('Old Version ID:' + versionId);
               
                if(activeContVersion != null && activeContVersion.vlocity_cmt__ContractId__r.Agreement_Type__c != null   ){
                    String documentTemplateName = '';
                    // get Template name based on Contract Type
                    documentTemplateName = getDocumentTemplateName (activeContVersion.vlocity_cmt__ContractId__r.Agreement_Type__c);
                    if(documentTemplateName != null){
                        Map<String, String> objNameMap = new Map<String, String> ();
                        Id documentTemplateId = getTemplateId(documentTemplateName, 'Contract');
                       // Attach template to the Contract Version
                        vlocity_cmt.ContractDocumentDisplayController.createContractSections(documentTemplateId, activeContVersion.id); 
                    }                    
                }              
            }
        }      
    }
    
    // Get Template name fromt the custom settings
    @testvisible
    private String getDocumentTemplateName (String ContractType){
         
         Set_Doc_Template__c docTemplate =  Set_Doc_Template__c.getInstance(ContractType);
    
         if(docTemplate!=null){
            String documentTemplateName = docTemplate.DocumentTemplateName__c; 
            System.debug('Template Name::' + documentTemplateName);
            return documentTemplateName;
        }
        return null;
    }
    
   //get active template ID for the contract 
       @testvisible
  private Id getTemplateId(String documentTemplateName, String objTypeName){
        
        List<vlocity_cmt__DocumentTemplate__c> templateIds = [Select Id from vlocity_cmt__DocumentTemplate__c 
                                                              where Name=:documentTemplateName And vlocity_cmt__IsActive__c=true 
                                                              AND vlocity_cmt__ApplicableTypes__c INCLUDES (:objTypeName)];
        
        if(templateIds !=null && templateIds.size()>0){
            return templateIds[0].Id;
        }
        else{
            String message = Label.vlocity_cmt.PDF_NoTemplateId;
            throw new NoTemplateIdException(message);
        }
        return null;    
    }
    
  
 /*String message = Label.vlocity_cmt.PDF_NoTemplateId;
throw new NoTemplateIdException(message);
*/    
public class NoTemplateIdException extends Exception{}    
}