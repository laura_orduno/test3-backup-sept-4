/**

** Changes **
July 4, 2014
Andy Leung
-  Defect found in isAmend introduced and so reset Opportunity.isamend__c field to false once the AOO is submitted for amendment.
*/
global class SMB_SubmitOrderController{
    
    // 1. General page work variables
    public Opportunity objOpportunity {get; set;}
    public String PageTitle {get;set;}
    public String ContractSignor {get;set;}
    public Boolean RenderSuccess {get;set;}
    public string ServiceAddress {get;set;}
    public RecordType LockedRecordType {get;set;}
    public String TabTitle{get;set;}
    public String operation{get;set;}
    public Boolean isEnabled {get;set;}
    global CPQQuoteCancellationHistory history{get;set;}
    public Boolean isCallAutoAttachDoc {get;set;}
    public Boolean disableCloseButton {get;set;}
    public id contId {get;set;}
    public string cancelCPQOrderEndPoint{get;set;}
    public string selectFlow {get;set;}
    public static string VLOCITY = 'vlocity';
    public static string APTTUS = 'Apttus';
         
        public SMB_SubmitOrderController(){
            RenderSuccess=false;

        }
        public SMB_SubmitOrderController(ApexPages.StandardController objController) {
        System.debug('=============parameters' + Apexpages.currentPage().getParameters());
        
        isCallAutoAttachDoc=false;
        // Determine whether the page references an Id
        Id thisId = null; 
        disableCloseButton = true;

        try {
            thisId = ApexPages.currentPage().getParameters().get('id');
            operation = ApexPages.currentPage().getParameters().get('operation');
            //This parameter is used to select Apttus or Vlocity flow
            selectFlow = ApexPages.currentPage().getParameters().get('selectFlow');
            RenderSuccess=false;
        }
        catch (Exception ex1) { System.debug('=============ex1' + ex1.getMessage());}
 
        // Instantiate the object this page maintains
        if (thisId == null) {
            this.objOpportunity = new Opportunity();
        }
        else {
            this.objOpportunity = (Opportunity)objController.getRecord();
        }
        
        //isEnabled = false;
        LockedRecordType = [Select Id from RecordType where sObjectType = 'Opportunity' and isActive = true and DeveloperName = 'SMB_Care_Locked_Order_Opportunity'];
         
        list<Opportunity> lstOpportunity = [Select Id,isamend__c,Related_Orders__c,Agreement_Required__c,Reason_Code__c,Printed_Agreement_Type__c,Product_Type__c,Delivery_Method__c,Amend_Stage__c,Order_ID__c,Type,Name,OwnerId,Provide_Order__c,StageName,RecordTypeId,RecordType.DeveloperName,Contract_Signor__r.FirstName, Contract_Signor__r.LastName, Apttus_Flow__c ,
             (Select Scheduled_Datetime__c,Product_Name__c,Scheduled_Due_Date_Location__c From Work_Orders__r where Status__c ='Reserved' and Unlinked_Work_Order__c = false),
             (Select Id, Order__c, NC_Order_ID__c, Operation_Failed_Status__c from Order_Requests__r where Order__c NOT IN ('Completed','Cancelled')),
             (Select Service_Civic_Number__c,Service_Street__c,Service_City__c,Service_State__c,Service_Country__c,Service_Postal_Code__c,Service_Address_Formula__c From OpportunityLineItems  where Service_Address_Formula__c != null limit 1) From Opportunity where Id =:objOpportunity.Id ];
        
        if(lstOpportunity.size()>0){
            objOpportunity = lstOpportunity[0];
        }
        System.debug('operation   ' + operation);
        if(operation != null){
            if(operation.equalsIgnoreCase('submit')){
                if(objOpportunity.RecordType.DeveloperName == 'SMB_Care_Amend_Order_Opportunity'){
                    TabTitle = 'Order Amended';
                    isCallAutoAttachDoc=true;
                }
                else{
                     TabTitle = 'Order Submitted';
                    isCallAutoAttachDoc=true;
                }
            }
            else if(operation.equalsIgnoreCase('cancelSMBOpp')){
                TabTitle = 'Order Cancelled';
                if(!objOpportunity.type.equalsignorecase('all other orders')){
                    refreshCPQQuoteCancellationHistory();
                    scpq__sciquote__c quote;
                    cancelCPQOrderEndPoint='';
                    try{
                        quote=database.query('select id,scpq__orderheaderkey__c from scpq__sciquote__c where scpq__opportunityid__c=\''+objOpportunity.id+'\' and scpq__primary__c=true limit 1');
                        scpq__cpqparameters__c cpqParam=scpq__cpqparameters__c.getall().get('Active');
                        cancelCPQOrderEndPoint=cpqParam.scpq__serverurl__c+'integration/extn_sfdccancellquote.do?cancelQuote_Input={"Order":{"OrderHeaderKey":"'+quote.scpq__OrderHeaderKey__c+'","UserId":"'+userinfo.getusername()+'"}}&scFlag=Y&ParentApp=SFDC&partnerURL=https://scpq.'+SMB_AmendCancelOrderController.getSalesforceInstance()+'.visual.force.com/services/Soap/u/19.0/'+quote.id+'&sessionID='+userinfo.getsessionid()+'&uiAction=customer&sfsorgcode=TELUS&EnterpriseCode='+cpqParam.scpq__enterprisecode__c;
                    }catch(queryexception qe){
                        apexpages.addmessage(new apexpages.message(apexpages.severity.error,qe.getmessage()));
                    }
                }
            }
        }
    }
    
    @TestVisible
    void refreshCPQQuoteCancellationHistory(){    
        user thisUser=[select profile.name from user where id=:userinfo.getuserid()];
        if(history!=null){
            history.clear();
        }else{
        	history=new CPQQuoteCancellationHistory();
        }
        map<string,schema.recordtypeinfo> recordTypeInfoMap=webservice_integration_error_log__c.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        // Perform this only for non-system admin so system admin can resume without checking.
        if(!thisUser.profile.name.containsignorecase('system admin')){
            list<webservice_integration_error_log__c> logHistoryRecords=[select recordtype.name from webservice_integration_error_log__c where sfdc_record_id__c=:objOpportunity.Id and object_name__c='Opportunity' and webservice_name__c='Cancel Order' and apex_class_and_method__c='SMB_AmendCancelOrderController.cancelOrder' and external_system_name__c='CPQ'];
            for(webservice_integration_error_log__c logHistoryRecord:logHistoryRecords){
                if(logHistoryRecord.recordtype.name.containsignorecase('success')){
                    history.numCPQCancellationSuccess++;
                }else{
                    history.numCPQCancellationFailure++;
                }
            }
        }        
    }    
    public void loadOrderSuccess(){
        
        if(RenderSuccess){
            if(operation.equalsIgnoreCase('submit')){
                if(objOpportunity.RecordType.DeveloperName == 'SMB_Care_Amend_Order_Opportunity'){
                    PageTitle = 'Success: ' + objOpportunity.Order_ID__c + ' was Amended';
                }
                else{
                    PageTitle = 'Success: ' + objOpportunity.Order_ID__c + ' was submitted';
                }
                if(String.isblank(objOpportunity.Printed_Agreement_Type__c)==true){
                        ContractSignor='Your Order for '+objOpportunity.Product_Type__c+':';
                    }
            }
            else if(operation.equalsIgnoreCase('cancelSMBOpp')){
                 PageTitle = 'Success: ' + objOpportunity.Order_ID__c + ' was Cancelled';
                 ContractSignor='Your Order for '+objOpportunity.Product_Type__c+':';
              
            }
            if(objOpportunity.OpportunityLineItems != null && objOpportunity.OpportunityLineItems.size()>0){
                ServiceAddress = objOpportunity.OpportunityLineItems[0].Service_Address_Formula__c;            
            }
            else{
                ServiceAddress = '';
            }
            //RenderSuccess = true;
        }
        else{
            if(operation.equalsIgnoreCase('submit')){
                if(objOpportunity.RecordType.DeveloperName == 'SMB_Care_Amend_Order_Opportunity'){
                    PageTitle = 'Failed: ' + objOpportunity.Order_ID__c + ' was not amended';
                }
                else{
                    PageTitle = 'Failed: ' + objOpportunity.Order_ID__c + ' was not submitted';
                }
            }
            else if(operation.equalsIgnoreCase('cancelSMBOpp')){
                PageTitle = 'Failed: ' + objOpportunity.Order_ID__c + ' was not cancelled';
            }
            disableCloseButton = false;
        }
        
        
    }

    public void runActionPoller(){
        isEnabled=true; 
    }
    public void SubmitOrder(){
        ContractSignor ='Generating Contract...';
        if(selectFlow != null && selectFlow.equalsIgnoreCase(VLOCITY))
            isEnabled = false;
        else{
        	isEnabled=true;
        }
        if(operation != null){
            if(operation.equalsIgnoreCase('submit')){
                
                if(objOpportunity.RecordType.DeveloperName == 'SMB_Care_Opportunity'){
                    submitOriginalOrder();
                }  
                else if(objOpportunity.RecordType.DeveloperName == 'SMB_Care_Amend_Order_Opportunity'){
                    submitAmendedOrder();
                }
            	loadOrderSuccess();
            }
            else if(operation.equalsIgnoreCase('cancelSMBOpp')){
                if(objOpportunity.type.equalsignorecase('all other orders')){
                    CancelSMBCareOrder();
                    loadOrderSuccess();
                }else{
                    if(history.numCPQCancellationSuccess>0){
                        CancelSMBCareOrder();
                        loadOrderSuccess();
                    }else if(history.numCPQCancellationFailure>2){
                        // Attempted more than 2 times, will display error message without attempting to cancel again
                        apexpages.addmessage(new apexpages.message(apexpages.severity.error,'Unable to cancel CPQ order, please contact CRM Team.'));
                        RenderSuccess = false;                    
                    }
                }
            }
            else {
                RenderSuccess = false;
            	loadOrderSuccess();
            }
        }
    }
    global pagereference cancelSMBOrder(){
        CancelSMBCareOrder();
        loadOrderSuccess();
        return null;
    }
    public pagereference showSystemErrorMessage(){
        apexpages.addmessage(new apexpages.message(apexpages.severity.error,'A system error has occurred, if you have not submitted a TELUSforce case, please submit one to CRM Ops team.'));
        return null;
    }
    @remoteaction
    global static CPQQuoteCancellationHistory getCPQQuoteCancellationHistory(string orderId){       
        user thisUser=[select profile.name from user where id=:userinfo.getuserid()];
        CPQQuoteCancellationHistory history=new CPQQuoteCancellationHistory();
        map<string,schema.recordtypeinfo> recordTypeInfoMap=webservice_integration_error_log__c.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        // Perform this only for non-system admin so system admin can resume without checking.
        if(!thisUser.profile.name.containsignorecase('system admin')){
            list<webservice_integration_error_log__c> logHistoryRecords=[select recordtype.name from webservice_integration_error_log__c where sfdc_record_id__c=:orderId and object_name__c='Opportunity' and webservice_name__c='Cancel Order' and apex_class_and_method__c='SMB_AmendCancelOrderController.cancelOrder' and external_system_name__c='CPQ'];
            for(webservice_integration_error_log__c logHistoryRecord:logHistoryRecords){
                if(logHistoryRecord.recordtype.name.containsignorecase('success')){
                    history.numCPQCancellationSuccess++;
                }else{
                    history.numCPQCancellationFailure++;
                }
            }
        }
        return history;
    }
    @remoteaction
    global static void logCPQCancellation(string orderId,string response,string textStatus){
        system.debug('order id:'+orderId+', response:'+response);
        Webservice_Integration_Error_Log__c log=new Webservice_Integration_Error_Log__c(apex_class_and_method__c='SMB_AmendCancelOrderController.cancelCPQQuote',external_system_name__c='CPQ',webservice_name__c='Cancel CPQ Quote',object_name__c='Opportunity',sfdc_record_id__c=orderId);
        try{
            map<string,schema.recordtypeinfo> recordTypeInfoMap=webservice_integration_error_log__c.sobjecttype.getdescribe().getrecordtypeinfosbyname();
            if(response.containsignorecase('"status":200')){
                log.recordtypeid=recordTypeInfoMap.get('Success').getrecordtypeid();
            }else{
                log.recordtypeid=recordTypeInfoMap.get('Failure').getrecordtypeid(); 
            }
            log.error_code_and_message__c=(string.isnotblank(response)&&response.length()>32767?response.substring(0,32767):response);
            log.stack_trace__c=(string.isnotblank(textStatus)&&textStatus.length()>32767?textStatus.substring(0,32767):textStatus);
            insert log;
        }catch(dmlexception dmle){
            system.debug(dmle.getmessage());
        }
    }
    global class CPQQuoteCancellationHistory{
        global integer numCPQCancellationSuccess{get;set;}
        global integer numCPQCancellationFailure{get;set;}
        global CPQQuoteCancellationHistory(){
            clear();
        }
        global void clear(){
            numCPQCancellationSuccess=0;
            numCPQCancellationFailure=0;
        }
    }

public void submitAmendedOrder(){
        isEnabled=false; 
System.debug('=======objOpportunity.Amend_Stage__c'+objOpportunity.Amend_Stage__c);
   // if(objOpportunity.Amend_Stage__c == 'Contract Sent'){
    if(objOpportunity.StageName.equalsIgnorecase('Contract On Hold (Customer)')){
        //generateAgreementDocument();
        if(selectFlow != null && selectFlow.equalsIgnoreCase(VLOCITY)){
            contId =  SMB_CPQQuoteHelper.createAgreement_new(objOpportunity.Id);
        	//isEnabled = true;
        }
        else {
            //SMB_CPQQuoteHelper.createAgreement(objOpportunity.Id);            
            isEnabled = true;
        }
        RenderSuccess = true;
        objOpportunity.RecordTypeId = LockedRecordType.Id;
         objOpportunity.StageName = 'Contract Sent';
        // objOpportunity.is_Wireless_Products_Added_or_Changed__c =false;
        /*
		* Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
		* START
		*/
			objOpportunity.stage_update_time__c=system.now().addseconds((math.random()>0.5?1:2));
		/*
		* Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
		* END
		*/
         
        update objOpportunity;
    } 
    else if(objOpportunity.Amend_Stage__c == 'Order Request New'){
        //If its a CPQ Order
        if(objOpportunity.Type == 'New Move Order' || objOpportunity.Type == 'New Provide/Upgrade Order'){
            System.debug('=======inside If');
            //if any changes were made to WLS Products
                 if(String.isNotBlank(objOpportunity.Agreement_Required__c) && objOpportunity.Agreement_Required__c.equalsIgnoreCase('Yes') && (objOpportunity.Printed_Agreement_Type__c == 'Business Anywhere Plus' || objOpportunity.Printed_Agreement_Type__c == 'Business Anywhere')){
         
                        
                     if(selectFlow != null && selectFlow.equalsIgnoreCase(VLOCITY)){
                         SMB_CPQQuoteHelper.UpdateContractsToCancelled_vlocity(objOpportunity.Id);
                         contId =  SMB_CPQQuoteHelper.createAgreement_new(objOpportunity.Id);
                         
                     }
                     else {
                         //SMB_CPQQuoteHelper.UpdateContractsToCancelled(objOpportunity.Id);
                         //SMB_CPQQuoteHelper.createAgreement(objOpportunity.Id);
                         isEnabled = true;
                     }
                  
                        objOpportunity.RecordTypeId = LockedRecordType.Id;
                        objOpportunity.StageName = 'Contract Sent';
                    //  objOpportunity.is_Wireless_Products_Added_or_Changed__c = false;
                    /*
					* Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
					* START
					*/
						objOpportunity.stage_update_time__c=system.now().addseconds((math.random()>0.5?1:2));
					/*
					* Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
					* END
					*/
                        update objOpportunity;
                  }
                  else{
                    ContractSignor ='';
                    
                     smb_OrderSubmit_Callout_Helper.SubmitOrderRequest(objOpportunity.Id);
                     objOpportunity.RecordTypeId = LockedRecordType.Id;
                     objOpportunity.StageName = 'Order Request New';
                      /*
					* Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
					* START
					*/
						objOpportunity.stage_update_time__c=system.now().addseconds((math.random()>0.5?1:2));
					/*
					* Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
					* END
					*/
                      disableCloseButton = false;
                     update objOpportunity;
                  }
                   RenderSuccess = true;

        }
        else if(objOpportunity.Type == 'All Other Orders'){
            	//Its a FOBO Order
                //Web service call to send full order to Netcracker directly   

                smb_OrderSubmit_Callout_Helper.SubmitOrderRequest(objOpportunity.Id);          
                RenderSuccess = true;          
                objOpportunity.RecordTypeId = LockedRecordType.Id;
                objOpportunity.StageName = 'Order Request New';
                objOpportunity.isamend__c=false;
                 /*
				* Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
				* START
				*/
					objOpportunity.stage_update_time__c=system.now().addseconds((math.random()>0.5?1:2));
				/*
				* Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
				* END
				*/
                update objOpportunity;
            
        }
    }
    else{
        RenderSuccess = false;
    }
}   
    
public void CancelSMBCareOrder(){
         
         System.debug('### Reached Cancelled');
         isEnabled=false;
         List<Opportunity> lstUpdateOpp=new List<Opportunity>();
         if(objOpportunity.Provide_Order__c && objOpportunity.Related_Orders__c!=null){
              Opportunity objRelatedOpportunity = [Select Id,StageName,Amend_Stage__c, (Select Id, NC_Order_ID__c From Order_Requests__r where Order__c NOT IN ('Cancelled', 'Completed') ) from Opportunity where Id=:objOpportunity.Related_Orders__c limit 1];
              if(objRelatedOpportunity != null && objRelatedOpportunity.Amend_Stage__c == 'Order Request New' && objRelatedOpportunity.Order_Requests__r!= null && objRelatedOpportunity.Order_Requests__r.size()>0){
                 String strSuccess = SMB_WebserviceHelper.sendWebserviceRequest(SMBCare_WebServices__c.getValues('SMB_ResumeOrderEndpoint').Value__c,Label.SMB_ResumeOrderXML, objRelatedOpportunity.Order_Requests__r[0].NC_Order_ID__c,SMBCare_WebServices__c.getValues('SMB_ResumeOrderSoapAction').value__c,'Resume Order');
                 RenderSuccess = (strSuccess.equalsIgnoreCase('Success')?true:false);
                 if(RenderSuccess){
                    objRelatedOpportunity.StageName = objRelatedOpportunity.Amend_Stage__c;
                     /*
					* Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
					* START
					*/
						objRelatedOpportunity.Stage_update_time__c=System.Now();
					/*
					* Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
					* END
					*/
                    lstUpdateOpp.add(objRelatedOpportunity);
                 }
                 else
                 {
                    strSuccess = SMB_WebserviceHelper.sendWebserviceRequest(SMBCare_WebServices__c.getValues('SMB_ResumeOrderEndpoint').Value__c,Label.SMB_ResumeOrderXML, objRelatedOpportunity.Order_Requests__r[0].NC_Order_ID__c,SMBCare_WebServices__c.getValues('SMB_ResumeOrderSoapAction').value__c,'Resume Order');       
                    RenderSuccess = (strSuccess.equalsIgnoreCase('Success')?true:false);
                    if(RenderSuccess){
                        objRelatedOpportunity.StageName = objRelatedOpportunity.Amend_Stage__c;
                        /*
						* Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
						* START
						*/
							objRelatedOpportunity.Stage_update_time__c=System.Now();
						/*
						* Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
						* END
						*/
                        lstUpdateOpp.add(objRelatedOpportunity);
                    }
                 }
              }
              
         }
         else{
            RenderSuccess = true;
         }
         SMB_WebServiceHelper.CancelWorkOrder(objOpportunity.Id);
         
         List<Opportunity> lstOpp=new List<Opportunity>();
         lstOpp.add(objOpportunity);
         smb_OpportunityProductHelper.updateOpportunityForNecessaryDueDates(null,lstOpp);
         objOpportunity.StageName = 'Quote Cancelled';
         /*
			* Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
			* START
			*/
				objOpportunity.stage_update_time__c=system.now().addseconds((math.random()>0.5?1:2));
			/*
			* Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
			* END
			*/
         objOpportunity.RecordTypeId = LockedRecordType.Id;
         lstUpdateOpp.add(objOpportunity);  
         update lstUpdateOpp;
}
    
    public PageReference sendDocumentToApttus(){
        if(isEnabled==false)
            return null;
        System.debug('SOQL Limit in sendDocumentToApttus before generateAgreementDocument:'+limits.getqueries());
        if(selectFlow != null && selectFlow.equalsIgnoreCase(VLOCITY))
            generateAgreementDocument_vlocity();
        else{
             //generateAgreementDocument();
        }
        System.debug('SOQL Limit in sendDocumentToApttus after generateAgreementDocument:'+limits.getqueries());
        isEnabled = false;
        if(String.isblank(objOpportunity.Printed_Agreement_Type__c)==false && objOpportunity.Delivery_Method__c == 'Email' )
            ContractSignor = 'Contract generated & sent to ' + objOpportunity.Contract_Signor__r.FirstName + ' ' +  objOpportunity.Contract_Signor__r.LastName;
        else if(String.isblank(objOpportunity.Printed_Agreement_Type__c)==false && objOpportunity.Delivery_Method__c != 'Email')
            ContractSignor = 'Contract generated';
        disableCloseButton = false;
        return null;
    }
    public void generateAgreementDocument() {
        //get all the agreements for the opportunity
        /*
        list<Apttus__APTS_Agreement__c> lstAgreement = [select Id from Apttus__APTS_Agreement__c where Apttus__Related_Opportunity__c =: objOpportunity.Id and Apttus__Status__c IN ('Request','Ready for Signatures')];
        
        System.debug('Before APTS call------>lstAgreement'+lstAgreement);
        if(lstAgreement!= null && lstAgreement.size()>0){
            
            for(Apttus__APTS_Agreement__c agmt : lstAgreement){
                String sendResponse = AptsComplyCustomAPI.doGenerate (agmt.Id);
                System.debug('APTTUS->'+sendResponse);
                if(sendResponse.startsWith('APT')){
                    //error has occurred
                    
                }
                else{
                    if(objOpportunity.Delivery_Method__c == 'Email'){
                        sendAgreementDocument(agmt.Id,sendResponse);
                    }
                }
            }
            
        }*/
    }
    public void generateAgreementDocument_vlocity() {
        //get all the agreements for the opportunity  
        list<attachment> attachments = new list <attachment>();
        string documentID = '';
        list<vlocity_cmt__ContractVersion__c> conVersionList = new list<vlocity_cmt__ContractVersion__c>(); 
        vlocity_cmt__ContractVersion__c contractVersion = new vlocity_cmt__ContractVersion__c();
        if(contId != null) {
            for ( vlocity_cmt__ContractVersion__c contversion:  [select id, name, (select id,name  from attachments order by lastmodifieddate desc ) 
                                                                 from  vlocity_cmt__ContractVersion__c 
                                                                 where vlocity_cmt__ContractId__c = :contId and vlocity_cmt__Status__c = 'Active' ] ){
                                                                     attachments.addall(contversion.attachments);  
                                                                     conVersionList.add(contversion);
                                                                     system.debug('::::: conVersionlist' +conVersionList + '::' + attachments );
                                                                 }
            if(attachments.size()> 0 && !attachments.isEmpty() ) {
                documentID = attachments[0].id;
            }
            if(conVersionList.size()>0 && !conVersionList.isEmpty() ){
                contractVersion = conVersionList[0];
            }
            if(objOpportunity.Delivery_Method__c == 'Email'){
                sendAgreementDocument_vlocity(contId,documentID, contractVersion.id);
                system.debug('>>> Send Document for Esingature..........' + contId + '+++' + documentID );
                
            }
            
        }
    }
  
@Future(Callout=true)
public static void sendAgreementDocument(String agreementID, String documentID) {
    /*
        System.debug('SOQL Limit in sendAgreementDocument before doSend:'+limits.getqueries());
        //String sendResponse = AptsComplyCustomAPI.doSend(agreementID, documentID);
        System.debug('SOQL Limit in sendAgreementDocument after doSend:'+limits.getqueries());
        System.debug('APTTUS->'+sendResponse);*/
        //return sendResponse;
}
    
@Future(Callout=true)
    public static void sendAgreementDocument_vlocity(String agreementID, String documentID,string conversionID) {
        {
            System.debug('SOQL Limit in sendAgreementDocument before doSend:'+limits.getqueries()); 
            string sendResponse = AptsComplyCustomAPI.doSend_vlocity(agreementID,documentID, conversionID);
            System.debug('SOQL Limit in sendAgreementDocument after doSend:'+limits.getqueries());
            System.debug('Response Status->'+sendResponse);           
        }
    }
    


private void submitOriginalOrder(){
        RenderSuccess = false;
        isEnabled=false;     
        objOpportunity.RecordTypeId = LockedRecordType.Id;
       // objOpportunity.is_Wireless_Products_Added_or_Changed__c=false;
        if(String.isNotBlank(objOpportunity.Agreement_Required__c) && objOpportunity.Agreement_Required__c.equalsIgnoreCase('Yes') && (objOpportunity.Printed_Agreement_Type__c == 'Business Anywhere Plus' || objOpportunity.Printed_Agreement_Type__c == 'Business Anywhere')){
                        //Contracts are necessary
            if(selectFlow != null && selectFlow.equalsIgnoreCase(VLOCITY)){
                contId =  SMB_CPQQuoteHelper.createAgreement_new(objOpportunity.Id);
            }
            else {
                //SMB_CPQQuoteHelper.createAgreement(objOpportunity.Id);
                isEnabled = true;
                objOpportunity.Apttus_Flow__c = false;
            }
                        
                        //generateAgreementDocument();
                        RenderSuccess = true;                    
                        objOpportunity.StageName = 'Contract Sent';
                        /*
						* Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
						* START
						*/
							objOpportunity.stage_update_time__c=system.now().addseconds((math.random()>0.5?1:2));
						/*
						* Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
						* END
						*/
                       // isEnabled=true;
         }
         else{
                    //contracts are not necessary                 
                   ContractSignor='';
                    //Web service call to send full order to Netcracker directly
                    smb_OrderSubmit_Callout_Helper.SubmitOrderRequest(objOpportunity.Id);
                    RenderSuccess = true;
                    objOpportunity.StageName = 'Order Request New';
                    /*
					* Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
					* START
					*/
						objOpportunity.stage_update_time__c=system.now().addseconds((math.random()>0.5?1:2));
					/*
					* Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
					* END
					*/
             disableCloseButton = false;
        }
        update objOpportunity;
   }    
}