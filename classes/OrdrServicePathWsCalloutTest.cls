@isTest
public class OrdrServicePathWsCalloutTest {
    @isTest
    private static void testFindResource() {
		Map<String, Object> inputMap = new Map<String, Object>(); 
        inputMap.put(OrdrConstants.FMS_ID, '227625');
        inputMap.put(OrdrConstants.ADDR_PROVINCE_CODE, 'BC');
        inputMap.put(OrdrConstants.ADDR_MUNICIPALITY, 'VANCOUVER');
        inputMap.put(OrdrConstants.COID, 'HMLK');        
        
        OrdrTestDataFactory.createOrderingWSCustomSettings('ClearanceCheckEndpoint', 'https://xmlgwy-pt1.telus.com:9030/pt01/RMO/InventoryMgmt/FindResource_v2_0_vs1_PT');
            
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        // Call the method that invokes a callout
        OrdrTnResourceInfoRetrievalConfiguration.ResourceConfigurationCollectionMessage resource = OrdrServicePathWsCallout.findResource(inputMap);
        // Verify that a fake result is returned
        System.assertEquals(3, resource.physicalResource.size());
        Test.stopTest();
        
    }
    
    @isTest
    private static void testFindResource_MissingMandatoryFields() {
		Map<String, Object> inputMap = new Map<String, Object>(); 
        inputMap.put(OrdrConstants.FMS_ID, '227625');
        inputMap.put(OrdrConstants.ADDR_PROVINCE_CODE, 'BC');
        inputMap.put(OrdrConstants.ADDR_MUNICIPALITY, 'VANCOUVER');
        
        OrdrTestDataFactory.createOrderingWSCustomSettings('ClearanceCheckEndpoint', 'https://xmlgwy-pt1.telus.com:9030/pt01/RMO/InventoryMgmt/FindResource_v2_0_vs1_PT');
            
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        try {
            // Call the method that invokes a callout
            OrdrTnResourceInfoRetrievalConfiguration.ResourceConfigurationCollectionMessage resource = OrdrServicePathWsCallout.findResource(inputMap);
            System.assert(false, 'Exception expected');
        }
        catch(Exception e) {
            System.debug(e.getTypeName() + ': ' + e.getMessage());
        }
        Test.stopTest();
    }    
    
    @isTest
    private static void testFindResource_Exception() {
		Map<String, Object> inputMap = new Map<String, Object>(); 
        inputMap.put(OrdrConstants.FMS_ID, '227625');
        inputMap.put(OrdrConstants.ADDR_PROVINCE_CODE, 'BC');
        inputMap.put(OrdrConstants.ADDR_MUNICIPALITY, 'VANCOUVER');
        inputMap.put(OrdrConstants.COID, 'HMLK');        
        
        OrdrTestDataFactory.createOrderingWSCustomSettings('ClearanceCheckEndpoint', 'https://xmlgwy-pt1.telus.com:9030/pt01/RMO/InventoryMgmt/FindResource_v2_0_vs1_PT');
            
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl(false));
        try {
        	// Call the method that invokes a callout
            OrdrTnResourceInfoRetrievalConfiguration.ResourceConfigurationCollectionMessage resource = OrdrServicePathWsCallout.findResource(inputMap);
            System.assert(false, 'Exception expected');
        }
        catch(Exception e) {
            System.debug(e.getTypeName() + ': ' + e.getMessage());
        }
        Test.stopTest();
        
    }
    
}