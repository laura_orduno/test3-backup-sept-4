/* Developed By: Hari Neelalaka K (IBM)
*  Created Date: 29-Oct-2014
*  Modified Date: 23-Jan-2015
*/
@isTest(SeeAllData=true) 
private Class SRS_NewServiceAddress_Test{   

        
    static testMethod void testServiceAdress(){
        
         //................Setup test data................. 
            
        Account acc = new Account();
        acc.Name = 'test';
        insert acc;
        System.assertEquals('test',acc.Name);
        
        Service_Request__c sr = new Service_Request__c();
        sr.Account_Name__c = acc.Id;
        insert sr;  

        SMBCare_Address__c testSmbCareAddr  = new SMBCare_Address__c(Account__c=acc.Id,Address_Type__c='Standard',
                                            Street_Number__c='2305',Suite_Number__c='345',Street_Name__c='Jasper Street',Street_Direction__c='East',City__c='Toronto',Province__c='AB',Postal_Code__c='Canada',Status__c='Invalid',Use_Addresses_Entered__c=true); 
        insert testSmbCareAddr;
        System.assertEquals('Toronto',testSmbCareAddr.City__c);
        
        SRS_Service_Address__c sa1 = new SRS_Service_Address__c(Address__c=testSmbCareAddr.Id,Building_Name__c='Building Test',Demarcation_Location__c='Demar Test',NPA_NXX__c='123456',Service_Request__c=sr.Id);
        insert sa1;       
                
        Id saId = sa1.Id;
        PageReference pageTest1 = Page.SRS_NewServiceAddress;
        pageTest1.getParameters().put('Id',saId);
        Test.setCurrentPage(pageTest1);      
        ApexPages.Standardcontroller stdcon1 = new ApexPages.Standardcontroller(sa1);
        SRS_NewServiceAddressController consa1 = new SRS_NewServiceAddressController(stdcon1);
        consa1.save();
        consa1.chckbx_fn();
        consa1.chckbx_checked();
        consa1.getItems();
        
        String radioFields = 'New Construction';        
                                    
        SRS_Service_Address__c sa2 = new SRS_Service_Address__c();
        Id srId = sr.Id; 
        PageReference pageTest2 = Page.SRS_NewServiceAddress; 
        pageTest2.getParameters().put('SRID', srId);    
        Test.setCurrentPage(pageTest2);
        ApexPages.StandardController stdcon2 = new ApexPages.StandardController(sa2);
        SRS_NewServiceAddressController consa2 = new SRS_NewServiceAddressController(stdcon2);
        consa2.save(); 
        consa2.Cancel();
        consa2.chckbx_fn();        
        consa2.chckbx_checked();
    //   String Index = 'One';   
        
        //......................................................................
         
        testSmbCareAddr.City__c='Canada';           
        update testSmbCareAddr;        
        sa1.NPA_NXX__c='123456';        
        update sa1;
        consa1.save();      
        consa1.Cancel();             
        consa1.chckbx_fn();        
        consa1.chckbx_checked();       
    } 
    
    //......................FMS Address Id value NOT NULL.........................
    
    static testMethod void fmsValueNotNull(){
        Account acc = new Account();
        acc.Name = 'test';
        insert acc;
        
        Service_Request__c sr = new Service_Request__c();
        sr.Account_Name__c = acc.Id;
        insert sr; 
        string recTypeId = Schema.SObjectType.SMBCare_Address__c.getRecordTypeInfosByName().get('Address').getRecordTypeid(); 
        SMBCare_Address__c sca  = new SMBCare_Address__c(Account__c=acc.Id,Address_Type__c='Standard',
                                Street_Number__c='1234',Suite_Number__c='123',Street_Name__c='1st Ave',
                                Street_Direction__c='East',City__c='Canada',Province__c='AB',Postal_Code__c='00000',
                                Status__c='Valid',FMS_Address_ID__c='01234', recordTypeId=recTypeId); 
        insert sca; 
        
        SRS_Service_Address__c sa = new SRS_Service_Address__c(Address__c=sca.Id,Building_Name__c='Test1',Demarcation_Location__c='Test1',NPA_NXX__c='123456',Service_Request__c=sr.Id);
        insert sa;
        
        Id srId = sr.Id; 
        PageReference pageTest3 = Page.SRS_NewServiceAddress; 
        pageTest3.getParameters().put('SRID', srId);    
        Test.setCurrentPage(pageTest3);
        ApexPages.StandardController stdcon = new ApexPages.StandardController(sa);
        SRS_NewServiceAddressController consa = new SRS_NewServiceAddressController(stdcon);
        consa.FMSvalue='01234';
        consa.save();       

        SRS_Service_Address__c sa1 = new SRS_Service_Address__c(Address__c=sca.Id,Building_Name__c='Test',Demarcation_Location__c='Test',NPA_NXX__c='123456',Service_Request__c=sr.Id);
        insert sa1;
        
        SRS_Service_Address__c sa2 = new SRS_Service_Address__c(Address__c=sca.Id,Building_Name__c='Test',Demarcation_Location__c='Test',NPA_NXX__c='123456',Service_Request__c=sr.Id);
       // insert sa1;                 
            
        ApexPages.Standardcontroller stdcon1 = new ApexPages.Standardcontroller(sa1);
        SRS_NewServiceAddressController consa1 = new SRS_NewServiceAddressController(stdcon1);
        
        consa1.save();
        
        ApexPages.Standardcontroller stdcon2 = new ApexPages.Standardcontroller(sa2);
        SRS_NewServiceAddressController consa2 = new SRS_NewServiceAddressController(stdcon2);
        consa2.FMSvalue='01234';
        consa2.save();
        consa2.populateData();
        consa2.pacOuterChkVal = 'doValid';
        consa2.populateData();
        //consa2.showCheckAddress();
        

    ///////////////////////////////////////////////////////////////

        List<SRS_NewServiceAddressController.wrapper> wrapperL = new List<SRS_NewServiceAddressController.wrapper>();
        SRS_NewServiceAddressController.wrapper req1= new SRS_NewServiceAddressController.wrapper('testparam1', 'testparam2', true, 1);                 
        wrapperL.add(req1);
        
        SRS_NewServiceAddressController.wrapper req2=new SRS_NewServiceAddressController.wrapper('testunitumber', 1);
        wrapperL.add(req2);
        
        SRS_NewServiceAddressController.wrapper req3= new SRS_NewServiceAddressController.wrapper('testparam1', 'testparam2', false, 1);
        SRS_NewServiceAddressController.wrapper req4= new SRS_NewServiceAddressController.wrapper('testparam1', 'testparam2', 'testparam3','testparam4','testparam5', 1);
        
        
   }
   
    //.......................Sample Test data for validating address against FMS....................
   
   static testMethod void testCheckAddress(){
     
        Account acc = new Account(Name='test account');
        insert acc;        
        Contact testcon = new Contact(LastName='testLastname');
        insert testcon;        
        Service_Request__c testsr = new Service_Request__c(Account_Name__c=acc.Id,PrimaryContact__c=testcon.Id);
        insert testsr;
        
        SMBCare_Address__c testSmbAddr1  = new SMBCare_Address__c(Account__c=acc.Id,Address_Type__c='Standard',City__c='TestMunicipal',Province__c='TA',
                                                Street_Number__c='2305',Suite_Number__c='345',Street_Name__c='1st Ave',Postal_Code__c='00000',Status__c = 'Invalid',FMS_Address_ID__c='01234'); 
        insert testSmbAddr1;
        SRS_Service_Address__c testsa1 = new SRS_Service_Address__c(Address__c=testSmbAddr1.Id,Building_Name__c='Building Test',
                                             Demarcation_Location__c='Demar Test',NPA_NXX__c='123456',Service_Request__c=testsr.Id);
        insert testsa1;            
    
        String index='0';
        String id='City';
        Id srId = testsr.Id; 
        PageReference pageTest1 = Page.SRS_NewServiceAddress; 
        pageTest1.getParameters().put('SRID', srId);
        pageTest1.getParameters().put('index', index);
        pageTest1.getParameters().put('paramid', id);   
        Test.setCurrentPage(pageTest1);
        ApexPages.StandardController stdcon1 = new ApexPages.StandardController(testsa1);
        SRS_NewServiceAddressController consa1 = new SRS_NewServiceAddressController(stdcon1);
        consa1.save();
                      
        
        SMBCare_Address__c testSmbAddr2  = new SMBCare_Address__c(Account__c=acc.Id,Address_Type__c='Standard',City__c='TestMunicipal',Province__c='TA',
                                                Street_Number__c='2305',Suite_Number__c='345',Street_Name__c='TestStreet',Postal_Code__c='00000',Status__c = 'Valid',FMS_Address_ID__c='01234'); 
        insert testSmbAddr2; 
    
        SRS_Service_Address__c testsa2 = new SRS_Service_Address__c(Address__c=testSmbAddr2.Id,Building_Name__c='Building Test',
                                             Demarcation_Location__c='Demar Test',NPA_NXX__c='123456',Service_Request__c=testsr.Id);
        insert testsa2;
             
      
        ApexPages.StandardController stdcon2 = new ApexPages.StandardController(testsa2);
        SRS_NewServiceAddressController consa2 = new SRS_NewServiceAddressController(stdcon2);
        
        
        consa2.save();
        
        List<SRS_NewServiceAddressController.wrapper> wrapperL = new List<SRS_NewServiceAddressController.wrapper>();
        SRS_NewServiceAddressController.wrapper req1= new SRS_NewServiceAddressController.wrapper('testparam1', 'testparam2', true, 1);                 
        wrapperL.add(req1);
        consa1.lstw = wrapperL;
        
        Test.setMock(WebServiceMock.class, new SRS_ASFServiceMockTest());         
        Test.startTest();
        consa1.CheckAddress();  
        consa1.processLinkClick();
        consa2.CheckAddress();
        
        String index1='0';
        String id1='Street Name';
        PageReference pageTest2=Page.SRS_NewServiceAddress;
        pageTest2.getParameters().put('index', index1);
        pageTest2.getParameters().put('paramid', id1);
        Test.setCurrentPage(pageTest2);  
        consa1.CheckAddress();  
        consa1.processLinkClick();
        consa2.CheckAddress();
        
        String index2='0';
        String id2='Unit Number';
        PageReference pageTest3=Page.SRS_NewServiceAddress;
        pageTest3.getParameters().put('index', index2);
        pageTest3.getParameters().put('paramid', id2);
        Test.setCurrentPage(pageTest3);  
        consa1.CheckAddress();  
        consa1.processLinkClick();
        consa2.CheckAddress();      
                
        String index3='0';
        String id3='House Number';
        PageReference pageTest4=Page.SRS_NewServiceAddress;
        pageTest4.getParameters().put('index', index3);
        pageTest4.getParameters().put('paramid', id3);
        Test.setCurrentPage(pageTest4);  
        consa1.CheckAddress();  
        consa1.processLinkClick();
        consa2.CheckAddress();
        Test.stoptest(); 

        //............. No Match found in FMS (NULL).................................
        Test.setMock(WebServiceMock.class, new SRS_ASFServiceMockTest1());  
        consa1.CheckAddress();  
        consa1.processLinkClick();
        consa2.CheckAddress();  
    } 
    
    public class SRS_ASFServiceMockTest1 implements WebServiceMock {
      public void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {

       if(soapAction == 'searchServiceAddress'){
        SRS_ASFValidationReqResponse.searchServiceAddressResponse_element responseElement = new SRS_ASFValidationReqResponse.searchServiceAddressResponse_element();
        SRS_ASFValidationReqResponse.LikeMunicipalityItem testMunicipal =  new SRS_ASFValidationReqResponse.LikeMunicipalityItem();
        SRS_ASFValidationReqResponse.LikeStreetItem testStreet = new SRS_ASFValidationReqResponse.LikeStreetItem();
        list<SRS_ASFValidationReqResponse.LikeStreetItem> lstStreet = new list<SRS_ASFValidationReqResponse.LikeStreetItem>();
        SRS_ASFValidationReqResponse.LikeHouseItem testHouse = new SRS_ASFValidationReqResponse.LikeHouseItem();
        list<SRS_ASFValidationReqResponse.LikeHouseItem> lstHouse = new list<SRS_ASFValidationReqResponse.LikeHouseItem>();
        SRS_ASFValidationReqResponse.LikeApartmentItem testApartment = new SRS_ASFValidationReqResponse.LikeApartmentItem();
        list<SRS_ASFValidationReqResponse.LikeApartmentItem> lstApartment = new list<SRS_ASFValidationReqResponse.LikeApartmentItem>();
        SRS_ASFValidationReqResponse.ServiceAddress address = new SRS_ASFValidationReqResponse.ServiceAddress();
       }
    } 
    }
    
    public static testMethod void ASFDataSet_test()
    {      
    SRS_ASFValidationReqResponse.LikeApartmentItem t1 = new SRS_ASFValidationReqResponse.LikeApartmentItem();
    SRS_ASFValidationReqResponse.LikeStreetItem t2 = new SRS_ASFValidationReqResponse.LikeStreetItem();
    SRS_ASFValidationReqResponse.ServiceAddress t3 = new SRS_ASFValidationReqResponse.ServiceAddress();
    SRS_ASFValidationReqResponse.LikeHouseList t4 = new SRS_ASFValidationReqResponse.LikeHouseList();
    SRS_ASFValidationReqResponse.ResponseMessageList t5 = new SRS_ASFValidationReqResponse.ResponseMessageList();
    SRS_ASFValidationReqResponse.getServiceAddressDetails_element t6 = new SRS_ASFValidationReqResponse.getServiceAddressDetails_element();
    SRS_ASFValidationReqResponse.ResponseMessage t7 = new SRS_ASFValidationReqResponse.ResponseMessage();
    SRS_ASFValidationReqResponse.LikeMunicipalityItem t8 = new SRS_ASFValidationReqResponse.LikeMunicipalityItem();
    SRS_ASFValidationReqResponse.searchServiceAddress_element t9 = new SRS_ASFValidationReqResponse.searchServiceAddress_element();
    SRS_ASFValidationReqResponse.LikeApartmentList t10 = new SRS_ASFValidationReqResponse.LikeApartmentList();
    SRS_ASFValidationReqResponse.LikeMunicipalityList t11 = new SRS_ASFValidationReqResponse.LikeMunicipalityList();
    SRS_ASFValidationReqResponse.LikeStreetList t12 = new SRS_ASFValidationReqResponse.LikeStreetList();
    SRS_ASFValidationReqResponse.getServiceAddressDetailsResponse_element t13 = new SRS_ASFValidationReqResponse.getServiceAddressDetailsResponse_element();
    SRS_ASFValidationReqResponse.LikeHouseItem t14 = new SRS_ASFValidationReqResponse.LikeHouseItem();
    SRS_ASFValidationReqResponse.searchServiceAddressResponse_element t15 = new SRS_ASFValidationReqResponse.searchServiceAddressResponse_element();
    } 
    
    
}