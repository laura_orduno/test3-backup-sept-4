/*
###########################################################################
# Created by............: Bhanu Priya
# Created Date..........: 04-Nov-2015
# Last Modified by......: Bhanu Priya
# Last Modified Date....: 
# Description...........: This is the controller class for interim page for New Quote button on SVOC Page
############################################################################
*/
public class OCOM_NewQuoteButtonFromAccountInterim {
        public Account account {get; set;}
        public Opportunity quot_Req_Oppty = new Opportunity();
        public id opptyIDonPage{get;set;}      //To display Oppty & Quote Id's on VF Page.
        public id quoteIDonPage{get;set;}
        public ServiceAddressData SAData {get;set;} //To get Service Address details
        //public Boolean flagErrorServAddr{get;set;}
        String saDataAddress;
       // public SRS_Service_Address__c srsSrvcAddr {get; set;}
        public SMBCare_Address__c smbCareAddr {get; set;}
        //String radioFields = NULL;
        public String FMSvalue {get;set;}
        Id profileId=userinfo.getProfileId();  
        public String errorMessage {get;set;}
        
        Public String actInProgress;
        Public String actDraft;
        //Varibles for Contact slection 
        public Contact[] contacts {get; private set;}
        public Boolean showNewContactForm {get; private set;}
        public Boolean loaded {get; private set;}
        public Contact new_contact {get; set;}
        public Id mostRecentlyCreatedContactId {get; private set;}
        public String selectedContactId{get;set;} 
        public Integer contactsCount {get {if(contacts == null){return 0;} return contacts.size();}}

        public Boolean showCreateQuote {get; private set;}
        public String searchTerm {get; set;}
        smb_newQuoteFromAccountController newQuote =new smb_newQuoteFromAccountController();
        
       /* public void MapPACAddress(ServiceAddressData SAData, SMBCare_Address__c smbCareAddr){
            smbCareAddr.City__c = SAData.city;
            smbCareAddr.Province__c = SAData.province;
            smbCareAddr.Street_Name__c = SAData.streetName;
            smbCareAddr.Address__c= SAData.address;
            smbCareAddr.Street_Address__c= SAData.address;
            smbCareAddr.Street__c= SAData.street;
            smbCareAddr.State__c= SAData.state;
            smbCareAddr.Postal_Code__c = SAData.postalCode;
            smbCareAddr.Country__c= SAData.country;
            smbCareAddr.Suite_Number__c = SAData.suiteNumber;
            smbCareAddr.Street_Direction__c = SAData.streetDirection;
            //smbCareAddr.CLLI__c = SAData.clliCode;
            smbCareAddr.COID__c = SAData.coid;
            smbCareAddr.LocalRoutingNumber__c = SAData.localRoutingNumber;
            smbCareAddr.NPA_NXX__c = SAData.ratingNpaNxx;
            smbCareAddr.SwitchNumber__c = SAData.switchNumber;
            smbCareAddr.TerminalNumber__c = SAData.terminalNumber;
             system.debug('=======OnselectFMSID1======'+SAData.fmsId);
        }*/
                    
                   
     
        // OnLoad method invoked on click of New Quote & VF page load.
        /***Working Line/*** public PageReference onLoad() {  ***/
        public Void onLoad() {
            
            loaded = false;
            showNewContactForm = false;
            showCreateQuote = true;
            Savepoint sp = database.setSavepoint();
            // To get Account Id parameter from SVOC page
            try 
            {
                if (ApexPages.currentPage() == null || ApexPages.currentPage().getParameters() == null) 
                { 
                    clear(); 
                 //  /***Working Line/***   return null; 
                }
                String aid = ApexPages.currentPage().getParameters().get('aid');
                if (aid == null || aid == '' || aid.substring(0, 3) != '001') { 
                    clear(); 
                //   /***Working Line/***   return null; 
                }
                Id xid = (Id) aid;
                account = [Select Name, Owner.Name From Account Where Id = :xid Limit 1];
                if (account == null) 
                { 
                    clear(); throw new NewQuoteException('Unable to load account'); 
                  // /***Working Line/***   return null; 
                }
                loadContacts();
                if (contacts == null || contacts.size() == 0) { showNewContactForm = true; }
                new_contact = new Contact(AccountId = account.Id);
            } 
            catch (Exception e) 
            { 
                clear();  
            }
            loaded = true;
           /***Working Line/***  PageReference pr = New PageReference ('/'+ quote); 
            
            return pr;   ***/       
        }
    
    
        private void loadContacts() 
        {
            system.debug('@@account.Id'+account.Id);
            contacts = [Select Name, Title, Email, Active__c From Contact Where AccountId = :account.Id Order By Active__c DESC, CreatedDate DESC limit 50];
            system.debug('@@Contacts'+contacts);
        }
       
        public PageReference toggleNewContactForm() 
        {
            try 
            {
                showNewContactForm = !showNewContactForm;
            } 
            catch (Exception e) { clear(); QuoteUtilities.log(e); }
                return null;
        }    

        Public PageReference showQuoteB ()
        {  
            if(selectedContactId== null)
            {  
                showCreateQuote = true; 
            }else{
                showCreateQuote = false;
            }
            return null;
        }
                     
        public void onSearch() 
        {
            try {
                if (searchTerm == null || searchTerm == '') { loadContacts(); return; }
                //searchTerm = '*' + searchTerm + '*';
                List<List<sObject>> searchResult = [FIND :searchTerm IN ALL FIELDS RETURNING Contact (name, email, title, Active__c Where AccountId = :account.Id) limit 50];
                contacts = ((List<Contact>)searchResult[0]);
            } catch (Exception e) { QuoteUtilities.log(e); } return;
        }
                         
        public PageReference createNewContact() {
        
            ApexPages.standardController sc = new ApexPages.standardController(new_contact);
            sc.save();
            system.debug('>>>>>>'+new_contact.id);
            if(sc.getId() == null) {
                return null; 
            }
            new_contact = [Select FirstName, LastName, Name, Email, Title, Phone From Contact Where Id = :sc.getId()];
            if (new_contact.Id != null) 
            {   
                showNewContactForm = false;
                mostRecentlyCreatedContactId = new_contact.id;
                new_contact = new Contact(AccountId = account.id);
                loadContacts();
                showQuoteB();
            }
            return null;
         }
    
                     
                     
         public PageReference onSelectContact() 
          {  //Id oId;
             Id quote;
             errorMessage = '';
             //flagErrorContact = false;
             system.debug('=======Onselect()======'+selectedContactId);
             system.debug('=======selectedContactValue======'+selectedContactId);
              system.debug('=======OnselectFMSID======'+SAData.fmsId);
             
             if (selectedContactId == null )
             {     showQuoteB();
                   throw new NewQuoteException('You must select a valid contact to create a quote.');
                   return null;
             }
             
              showQuoteB();
                 system.debug('=======ELSE: selectedContactValue======'+selectedContactId);
             
                 system.debug('=======Onselect()======');
                 
                 system.debug('=======Contact ID======'+selectedContactId);
                 //New Opportunity & quote create methods invoked here.
                quote = quoteCreation(opptyCreation());
                 // Address add start
                 UpdateAddressAndQuote(quote);
                //Address add end
                //Updated by Arvind as partof Defect Id# 54268
              Pagereference pref= new pagereference('/'+quote); 
              //Pagereference pref= new pagereference('/apex/OCOM_Quote_PAC_ServiceAddress?id='+quote);
              return pref; 
            
          }
                     
                        

                   
       /***Quote Creation logic & Opportunity assignment. ***/
        public Id quoteCreation(Id opptyId) {
            Quote newquote = New Quote ();
            Id qId;
            Opportunity newopp = [select Id, Name, accountId from Opportunity where id = :opptyId];
            newquote.OpportunityId= newopp.Id;
            newquote.Name = newopp.Name;
            newquote.Account_Id__c = newopp.accountId; 
            newquote.Sales_Representative__c = UserInfo.getUserId();
           Map<String, OCOM__c> ocomP = OCOM__c.getAll();
             for(OCOM__c Oc:ocomP.values())
             {
               if (Oc.Reference__c.equalsIgnoreCase('Price Book Detail')) 
                     newquote.pricebook2id =Oc.Name;
               }
           // newquote.pricebook2id= '01se00000005s0C';
           //   newquote.RecordTypeId = '012e00000000tAxAAI';
           newquote.RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Vlocity Quote').getRecordTypeId();
             // newquote.AccountId= newopp.accountId;
            newquote.vlocity_cmt__Type__c = 'Existing Customer - Upgrade';
            Try{
                 insert newquote;
                 quoteIDonPage= newquote.Id;
                 qId= newquote.Id;
            }catch(Exception e){  system.debug('##########'+e.getCause());}
            //PageReference  q = New PageReference ('/'+ qId);
            return qId;
        }


                   

        /*** Constructor for this Controller ***/
        public OCOM_NewQuoteButtonFromAccountInterim(){
            system.debug('#############Nitin');
            SAData  = new ServiceAddressData();
            SAData.isAdressRequired = true;
            SAData.isProvinceRequired = true;
           SAData.isCityRequired = true;
            smbCareAddr = new SMBCare_Address__c();
            smbCareAddr.Address_Type__c = 'Standard'; // Service Address Type defaults to Standard.
            
            //smbCareAddr.Status__c='Valid';
            
            smbCareAddr.recordtypeid = Schema.SObjectType.SMBCare_Address__c.getRecordTypeInfosByName().get('Address').getRecordTypeId();       
            system.debug('>>> Address Record Type: '+smbCareAddr.recordtypeid);
            system.debug('>>> Selected contact ID: '+selectedContactId);
                              
        }
                    
                    
                    
        public class NewQuoteException extends Exception {}

                       
       public void clear() {
            account = null;
            contacts= null;
        }
                    
        public quote UpdateAddressAndQuote(id quoteidforredirect)
        {
            OCOM_ServiceAddressControllerHelper svcAddrHelper = new OCOM_ServiceAddressControllerHelper();
            Quote qttoupdate = svcAddrHelper.UpdateAddressAndQuote(quoteidforredirect, smbCareAddr, SAData);
         
            qttoupdate.Contactid=selectedContactId;
            update qttoupdate;
            
            quot_Req_Oppty.Primary_Customer_Contact__c=selectedContactId;
            update quot_Req_Oppty;
            return qttoupdate;
        }
                    
        /*** New Opportunity creation logic with required fields assignments. ***/
        public Id opptyCreation() {
            system.debug('------Account1 -----'+ account);
            //Opportunity quot_Req_Oppty = New Opportunity ();
            Id oId;
            String oStage = Label.New_Quote_Opportunity_Stage;
            //Id recordType_ID = Scheme.SObjectType.Opportunity.getRecordTypeInfosByName().get('SMB Care Opportunity').getRecordTypeId();
           // Id recordType_ID = Scheme.SObjectType.Opportunity.getRecordTypeInfosByName().get('SBS Opportunity').getRecordTypeId();
           // RecordType oppRT = [Select Id From RecordType Where SobjectType = 'Opportunity' And Name ='Vlocity User'];  
            quot_Req_Oppty.Name=  account.Name + '- Simple Quote - ' +  System.now();
            quot_Req_Oppty.Amount = 0;
            quot_Req_Oppty.Salesperson__c = UserInfo.getUserId();
            quot_Req_Oppty.CloseDate = system.today();
            quot_Req_Oppty.AccountId = account.Id;
            Contact cont= getContact();
            quot_Req_Oppty.Primary_Order_Contact__c = cont.Id;
            cont.Opportunity__c = quot_Req_Oppty.Id;
            cont.SRS_Contact_Type__c = 'Primary Contact'; 
           // Id txt ='01se00000005s0C';
             //quot_Req_Oppty.pricebook2Id = txt ;
           
             Map<String, OCOM__c> ocomOppty = OCOM__c.getAll();
             for(OCOM__c Op:ocomOppty.values())
             {
               if (Op.Reference__c.equalsIgnoreCase('Price Book Detail')) 
                     quot_Req_Oppty.pricebook2Id = Op.Name;
               }
            
            quot_Req_Oppty.StageName= oStage;
           quot_Req_Oppty.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Vlocity User').getRecordTypeId();
            //quot_Req_Oppty.RecordTypeId = oppRT.Id;
            try{
                Insert  quot_Req_Oppty;
                opptyIDonPage = quot_Req_Oppty.id;
                
                //Update contact woth Opportunity Id.
                Update cont;
            }catch(Exception e){ 
                system.debug('##########'+e.getCause());
            }

            if (quot_Req_Oppty.Id != null || quot_Req_Oppty.Id!= '')
            {
                oId= quot_Req_Oppty.Id;
                return oId;
            }
            else return Null;
        }
        
          /*** Method to fetch the contact details***/
        Public Contact getContact(){
             Contact c = [Select id, Name, AccountId, vlocity_cmt__IsActive__c, SRS_Contact_Type__c, Opportunity__c from Contact Where AccountId=: account.Id  Order By Active__c DESC, CreatedDate DESC Limit 1];
             return c;
        }
                    
    /*    public Boolean AllowCreationWithNewQuoteButton{
            get {
            // List<OCOM__c> ocom= OCOM__c.getall().values();
            Map<String, OCOM__c> ocom = OCOM__c.getAll();
            if (account == null) 
                return false;
            if (account.RecordType == null) 
                return false;
            if (account .RecordType.DeveloperName == 'RCID' || account .RecordType.DeveloperName == 'New_Prospect_Account') {
                if(account .Account_Status__c == 'Obsolete' || account .Account_Status__c == 'Terminated'){
                    if (ocom.containskey(profileId)){
                        return false;
                    }
                }else{
                    return true;
                }
            }            
            return false;
        }
}*/


}