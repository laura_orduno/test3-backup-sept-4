/**
 * trac_CustomUserLookup_Ctlr.cls - Controller for CustomUserLookup Page
 * @description Controller Extension handles querying for users.
 *
 * @author Ryan Draper, Traction on Demand
 * @date 2013-08-12
 */
public with sharing class trac_CustomUserLookup_Ctlr {

	public String query {get; set;}
	public List<User> users {get; set;}
	
	public String fullNameLabel {get;set;}//Full_Name;
	public String activeLabel {get;set;}//Active;
	public String roleLabel {get;set;}//Role;
	public String searchForUserLabel {get;set;}//trac_search_for_a_user
	public String searchResultsLabel {get;set;}//trac_Search_Results
	public String lookupErrorLabel {get;set;}//trac_lookup_error
	public String goLabel {get;set;}//trac_GO
	public String searchLabel {get;set;}//trac_Search
	public String searchLookupLabel {get;set;}//trac_Lookup
	
	
	public trac_CustomUserLookup_Ctlr() {
		users = new List<User>();
		query = ApexPages.currentPage().getParameters().get('nameField');
		
		fullNameLabel  = Label.Full_Name;
		activeLabel = Label.Active;
		roleLabel = Label.Role;
		searchForUserLabel = Label.trac_search_for_a_user;
		searchResultsLabel = Label.trac_Search_Results;
		lookupErrorLabel = Label.trac_lookup_error;
		goLabel = Label.trac_GO;
		searchLabel = Label.trac_Search;
		searchLookupLabel = Label.trac_Lookup;
	}
	
	public void runQuery(){	
		query = query.replace('*','');
		String hiddenQuery = '%' + query + '%';
		
		users = [	SELECT Id, Name, IsActive, UserRole.Name
					FROM User 
					WHERE Name 
					like :hiddenQuery
					limit 30];
	}

	@isTest(SeeAllData=false)
	private static void testOnSearch() {
		Id PROFILEID = [SELECT id FROM Profile WHERE name='Standard User' LIMIT 1].id;
		User u = new User(username = 'VMDFJREFSRI@TRACTIONSM.COM',
                                                  email = 'test@example.com',
                                                  title = 'test',
                                                  firstname = 'traction',
                                                  lastname = 'test',
                                                  alias = 'test',
                                                  TimezoneSIDKey = 'America/Los_Angeles',
                                                  LocaleSIDKey = 'en_US',
                                                  EmailEncodingKey = 'UTF-8',
                                                  ProfileId = PROFILEID,
                                                  LanguageLocaleKey = 'en_US');
                insert u;
                
		trac_CustomUserLookup_Ctlr ctlr = new trac_CustomUserLookup_Ctlr();
		
		ctlr.query = u.firstname + ' ' + u.lastname;
		
		ctlr.runQuery();
		
		system.assertNotEquals(null, ctlr.users);
	}
}