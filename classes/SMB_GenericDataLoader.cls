/*
###########################################################################
# File..................: SMB_GenericDataLoader
# Version...............: 1
# Created by............: Vinay Sharma
# Created Date..........: 27/02/2014
# Last Modified by......: Vinay Sharma
# Last Modified Date....: 27/02/2014
# Description...........: This Class used in Test Class for Creating a List<sobject> record from csv file.  
#
# Copyright (c) 2000-2012. Astadia, Inc. All Rights Reserved.
#
# Created by the Astadia, Inc. Modification must retain the above copyright notice. 
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any commercial purpose, without fee, and without a written
# agreement from Astadia, Inc., is hereby forbidden. Any modification to source
# code, must include this paragraph and copyright.
#
# Permission is not granted to anyone to use this software for commercial uses.
#
# Contact address: 2839 Paces Ferry Road, Suite 350, Atlanta, GA 30339
# Company URL : http://www.astadia.com
###################################################9########################
*/
public class SMB_GenericDataLoader {
    private list<string[]> listData = new list<string[]>();
    private String[]   fieldArray;
    private String     objectApiName = '';
    private String     field = '';
    private String     staticResourceName = '';
    private string Updatable = 'NOT UPDATABLE';
    private list<sobject> records;
    public list<sobject> makeData(String objectApiName,String staticResourceName){
        this.objectApiName = objectApiName;
        this.staticResourceName = staticResourceName;
        StaticResource sr = [Select  s.Name, s.Id, s.Body From StaticResource s  where name =:staticResourceName];
        blob tempBlob = sr <> null?sr.Body:null;
        String tempString = tempBlob <> null?tempBlob.toString().trim():null;
        try{
        CsvReader(tempString);
        if(listData.size()<= 1 )
            throw new MyException('File have No Data.');
            createRecord();      
        }catch(MyException e){
            throw e;
        }
        catch(Exception ex){
            throw ex;
        }
        
        return records;   
    }
    private void createRecord(){
        system.debug('Enter in createRecord ##############');
        try{
            sobject insertObject;
            setFieldType();
            records = new list<sobject>();
            for(string[] line:listData){
                insertObject = Schema.getGlobalDescribe().get(objectApiName).newSObject();
                for(integer i=0;i<fieldArray.size();i++){
                    field = fieldArray[i];
                    if(! field.equalsIgnoreCase(Updatable)){
                        system.debug(field +' = '+line[i]);
                        String[] splitedfield = field.split(';');
                        insertObject = putValue(insertObject,splitedfield[1],splitedfield[0],line[i].trim());
                        //system.debug('object ##############'+insertObject);
                    }
                }
                records.add(insertObject);
            }
        }catch(Exception e){
            throw e;
        }
        
    }
    private void setFieldType(){
        //system.debug('Enter in Set Field ##############'+fieldArray);
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        boolean IsFieldExsist = false; 
        try{
            if(!gd.containsKey(objectApiName)){
                throw new MyException(objectApiName +' Object Not Found.');
            }else{
                Schema.SObjectType ctype = gd.get(objectApiName); 
                Map<String, Schema.SobjectField> fmap = ctype.getDescribe().fields.getMap();
                for(integer i=0;i<fieldArray.size();i++){
                    field = fieldArray[i].trim();       
                    //system.debug(field+'contains in ##############');
                    if(!fmap.containsKey(field)){
                        fieldArray[i] = Updatable;
                    }else{
                        IsFieldExsist = true;
                        Schema.SobjectField saleforceField = fmap.get(field);
                        Schema.DescribeFieldResult fr = saleforceField.getDescribe();        
                        fieldArray[i] = fr.isUpdateable()?fieldArray[i].trim() + ';' + fr.getType():Updatable;
                    }
                }
                if(!IsFieldExsist){
                    throw new MyException(' Fields Not Found in '+objectApiName);
                }
            }
        }catch(Exception e){
            throw e;
        }
    }
    
    private static sObject putValue(sObject obj,string fieldType,string fieldName,string fieldValue){
        //NOTE::::::: Check to see if fieldValue is null or empty then do not type cast
        fieldValue = fieldValue <> '' ? fieldValue : Null;
        if(fieldValue!=null ) {
            if(fieldType.equalsIgnoreCase('BOOLEAN')){
                obj.put(fieldName,Boolean.valueOf(fieldValue));
            }
            else if(fieldType.equalsIgnoreCase('Currency')){
                obj.put(fieldName,Decimal.valueOf(fieldValue));
            }
            else if(fieldType.equalsIgnoreCase('Integer') ){
                obj.put(fieldName,Integer.valueOf(fieldValue));             
            }
            else if(fieldType.equalsIgnoreCase('DOUBLE')){
                obj.put(fieldName,DOUBLE.valueOf(fieldValue));             
            }
            else if(fieldType.equalsIgnoreCase('Decimal')|| fieldType.equalsIgnoreCase('PERCENT')){
                obj.put(fieldName,Decimal.valueOf(fieldValue));
            }
            else if(fieldType.equalsIgnoreCase('Date') || fieldType.equalsIgnoreCase('Date/Time')){
                if(fieldValue.indexOf('-')>-1){
                    List<String> lstVal = fieldValue.split('-');
                    if(lstVal != null && lstVal.size()>2){
                        if(fieldType.equalsIgnoreCase('Date')){
                            List<String> lstTime = lstVal[2].split(' ');
                            obj.put(fieldName,Date.newInstance(Integer.valueOf(lstVal[0]),Integer.valueOf(lstVal[1]),Integer.valueOf(lstTime[0])));
                        }
                        if(fieldType.equalsIgnoreCase('Date/Time')){
                            List<String> lstTime = lstVal[2].split(' ');
                            if(lstTime != null && lstTime.size()>1){
                                List<String> lstTimeComp = lstTime[1].split(':');
                                if(lstTimeComp != null && lstTimeComp.size()>2){
                                    obj.put(fieldName,DateTime.newInstance(Integer.valueOf(lstVal[0]),Integer.valueOf(lstVal[1]),Integer.valueOf(lstTime [0]),Integer.valueOf(lstTimeComp[0]),Integer.valueOf(lstTimeComp[1]),Integer.valueOf(lstTimeComp[2])));
                                }
                            }
                        }
                    }
                }
            }
            else{
                obj.put(fieldName,fieldValue);
            }   
        }
        else {
            obj.put(fieldName,fieldValue);
        }       
        return obj;
    }
    /*---------------------------------CSV Reader -----------------------------*/
    
    private String     delim = ',';
    private String[]   buffer;
    private String[]   line;              // the input data for entire CSV file
    
    private void CsvReader(String data){
        this.buffer = (data == null ? new List<String>() : data.split('\n'));
        fieldArray = readLine();
        line = readLine();
        while(line <> null){
            listData.add(line);
            line = readLine();
        }
    }
    
    private void CsvReader(String data, String delim){
        this.buffer = (data == null ? new List<String>() : data.split('\n'));
        this.delim = delim;
        fieldArray = readLine();
        line = readLine();
        while(line <> null){
            listData.add(line);
            line = readLine();
        }
    }
    
    private String[] readLine(){
        if(this.buffer.size() == 0) return null;
        String     line     = this.buffer.remove(0).replace('\n','');    // grab first part of stream up to newline; remove from buffer
        String[]   parts     = new String[] {};        // result list of tokens for one line
            while(line != ''){
                //system.debug(line.trim() + 'In read line');
                Integer next = 0;
                if(line.startsWith('"')){
                    line = line.substring(1); // strip initial "
                    Integer quoteIndex = findQuote(line, 0);    // Look for closing " on same line
                    while(quoteIndex == -1){            //  not found, we must have a newline within a quoted token
                        if(buffer.size() == 0){
                            // EOT!
                            quoteIndex = line.length();
                        } 
                        else {
                            // grab the next line and look to see if closing " can be found
                            Integer skip = line.length();
                            line += '\n' + this.buffer.remove(0);
                            quoteIndex = findQuote(line, skip);
                        }
                    }
                    // we have a quoted token, advance to comma
                    next = quoteIndex + 1;
                    parts.add(line.substring(0, quoteIndex).replace('""', '"'));
                } 
                else {    // non-quoted token, token end is at delim
                    next = line.indexOf(this.delim, next);
                    if(next == -1)
                        next = line.length();
                    // NB in Substring, "endindex" is the index of the character AFTER the last index to get
                    parts.add(line.substring(0, next));
                }
                if(next == line.length() - 1)
                    // case of a terminating comma.
                    parts.add('');
                line = next < line.length() ? line.substring(next+1) : '';
            }
        if(parts.size() == 0)
            // empty string - we still want to return something...
            parts.add('');
        return parts;
    }
    
    static private Pattern quotePattern = Pattern.compile('(?<!")"(?!")');
    //  -------------------------------------------------
    //  Helper: findQuote - find next quote " in line
    private Integer findQuote(String line, Integer skip){
        Matcher m = quotePattern.matcher(line);
        m.region(skip, m.regionEnd());
        if(!m.find())
            return -1;
        return m.start();
    }
    
}