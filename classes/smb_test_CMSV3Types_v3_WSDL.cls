/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class smb_test_CMSV3Types_v3_WSDL {
static testMethod void cmsv3_ContractManagementSvcTypes_v3(){
        //SMB_DummyWebServiceResponse_Test.intializeMockWebservice();
        cmsv3_ContractManagementSvcTypes_v3 smb22 = new cmsv3_ContractManagementSvcTypes_v3();
        cmsv3_ContractManagementSvcTypes_v3.AssignedProductIdList b1 = new cmsv3_ContractManagementSvcTypes_v3.AssignedProductIdList();
        cmsv3_ContractManagementSvcTypes_v3.BillingTelephoneNumList b2 = new cmsv3_ContractManagementSvcTypes_v3.BillingTelephoneNumList();
        cmsv3_ContractManagementSvcTypes_v3.Contract b3 = new cmsv3_ContractManagementSvcTypes_v3.Contract();
        cmsv3_ContractManagementSvcTypes_v3.ContractAmendment b4 = new cmsv3_ContractManagementSvcTypes_v3.ContractAmendment();
        cmsv3_ContractManagementSvcTypes_v3.ContractAmendmentList b5 = new cmsv3_ContractManagementSvcTypes_v3.ContractAmendmentList();
        cmsv3_ContractManagementSvcTypes_v3.ContractAttribute b6 = new cmsv3_ContractManagementSvcTypes_v3.ContractAttribute();
        cmsv3_ContractManagementSvcTypes_v3.ContractAttributeList b7 = new cmsv3_ContractManagementSvcTypes_v3.ContractAttributeList();
        cmsv3_ContractManagementSvcTypes_v3.ContractDetail b8 = new cmsv3_ContractManagementSvcTypes_v3.ContractDetail();
        cmsv3_ContractManagementSvcTypes_v3.ContractDetailList b9 = new cmsv3_ContractManagementSvcTypes_v3.ContractDetailList();
        cmsv3_ContractManagementSvcTypes_v3.ContractDocument b10 = new cmsv3_ContractManagementSvcTypes_v3.ContractDocument();
        cmsv3_ContractManagementSvcTypes_v3.ContractDocumentList b11 = new cmsv3_ContractManagementSvcTypes_v3.ContractDocumentList();
        cmsv3_ContractManagementSvcTypes_v3.ContractList b12 = new cmsv3_ContractManagementSvcTypes_v3.ContractList();
        cmsv3_ContractManagementSvcTypes_v3.ContractRequest b13 = new cmsv3_ContractManagementSvcTypes_v3.ContractRequest();
        cmsv3_ContractManagementSvcTypes_v3.ContractRequestDetail b14 = new cmsv3_ContractManagementSvcTypes_v3.ContractRequestDetail();
        cmsv3_ContractManagementSvcTypes_v3.ContractRequestDetailList b15 = new cmsv3_ContractManagementSvcTypes_v3.ContractRequestDetailList();
        cmsv3_ContractManagementSvcTypes_v3.CustomerSignor customerSignor= new cmsv3_ContractManagementSvcTypes_v3.CustomerSignor();
       
        cmsv3_ContractManagementSvcTypes_v3.LegacyContractIdList b17 = new cmsv3_ContractManagementSvcTypes_v3.LegacyContractIdList();
        cmsv3_ContractManagementSvcTypes_v3.OldAssignedProductIdList b18 = new cmsv3_ContractManagementSvcTypes_v3.OldAssignedProductIdList();
        cmsv3_ContractManagementSvcTypes_v3.ServiceAddress b19 = new cmsv3_ContractManagementSvcTypes_v3.ServiceAddress();
        cmsv3_ContractManagementSvcTypes_v3.ServiceAddressList b20 = new cmsv3_ContractManagementSvcTypes_v3.ServiceAddressList();
        cmsv3_ContractManagementSvcTypes_v3.ServiceDetail b21 = new cmsv3_ContractManagementSvcTypes_v3.ServiceDetail();        
        cmsv3_ContractManagementSvcTypes_v3.ServiceDetailList b22 = new cmsv3_ContractManagementSvcTypes_v3.ServiceDetailList();
        
    }
}