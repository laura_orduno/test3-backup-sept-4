public without sharing class SMB_CPQConnectionController
{
    public SMB_CPQConnectionController(){}
    
    public PageReference redirect()
    {
        String order=SMB_CPQ_Dummy_Opportunity__c.getValues('CPQ').Opportunity__c;
        if(String.isBlank(order))
        {
          System.debug('NO ORDER FOUND');
          return null;
         }
        //String hostURL = System.URL.getCurrentRequestUrl().getHost();            
        //hostURL = hostURL.substring(hostURL.indexOf('.') + 1, hostURL.indexOf('.', hostURL.indexOf('.') + 1));
       // change for MYDomain start
          String hostURL =URLMethodUtility.getInstanceName();
       
        String oppId=[Select id from opportunity where order_id__c=:order limit 1].id;
        
        scpq__SciQuote__c Quote=[SELECT id FROM scpq__SciQuote__c where scpq__Primary__c=true AND scpq__OpportunityId__c=:oppId LIMIT 1] ;
        PageReference PR = new PageReference('https://scpq.'+hostURL+'.visual.force.com/apex/VisualforceSCIQuoteEdit?id=' + Quote.id+ '&retURL=' + oppId + '&sfdc.override=1&isdtp=vw');
        System.debug('URL->'+PR);
        PR.setRedirect(true);        
        return PR;
    }
}