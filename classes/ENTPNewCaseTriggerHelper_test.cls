@isTest(SeeAllData=false)
private class ENTPNewCaseTriggerHelper_test {

	@isTest(SeeAllData=false) static void ENTPNewCaseTriggerHelperLynx() {
		ENTPTestUtils.createSMBCareWebServicesCustomSetting();
		//User u = MBRTestUtils.createENTPPortalUser('test-user-1', false, 'en_US');
		Account a = new Account(Name = 'Unit Test Account');
		a.Remedy_Business_Unit_Id__c = 'TCI';
		a.Remedy_Company_Id__c = 'TCI';
		a.strategic__c = false;
		insert a;

		Contact c = new Contact(Email = 'test-user-1@unit-test.com', LastName = 'Test', AccountId = a.Id);
		c.Remedy_PIN__c = '70208';
		insert c;
		Profile p = [SELECT Id FROM Profile WHERE UserType = 'CSPLitePortal' LIMIT 1];
		User u = new User(Alias = 'TestUser', Email = 'test-user-1@unit-test.com', ContactId = c.Id,
				EmailEncodingKey = 'UTF-8', FirstName = 'Unit', LastName = 'Testing', LanguageLocaleKey = 'en_US',
				LocaleSidKey = 'en_US', CommunityNickname = 'test-user-1UNTST', /*UserRoleId = portalRole.Id,*/ ProfileId = p.Id,
				TimeZoneSidKey = 'America/Los_Angeles',
				UserName = 'test-user-1@unit-test.com');

		u.Customer_Portal_Role__c = 'tps';

		insert u;
		System.runAs(u) {
			Case caseToInsert = ENTPTestUtils.createENTPCases(1, a.id, false)[0];
			caseToInsert.Lynx_Ticket_Number__c = null;
			insert caseToInsert;

			Case parentCase = [SELECT Id, ContactId, CaseNumber, My_Business_Requests_Type__c, LastModifiedDate, AccountId FROM Case ORDER BY CreatedDate DESC LIMIT 1];
			ENTPNewCaseTriggerHelper.SendLynxCreate(parentCase.id);

			// ENTPNewCaseTriggerHelper.SendRemedyCreate(parentCase.id);
		}
	}

	@isTest(SeeAllData=false) static void ENTPNewCaseTriggerHelperMits() {
		ENTPTestUtils.createSMBCareWebServicesCustomSetting();
		//User u = MBRTestUtils.createENTPPortalUser('test-user-1', false, 'en_US');
		Account a = new Account(Name = 'Unit Test Account');
		a.Remedy_Business_Unit_Id__c = 'TCI';
		a.Remedy_Company_Id__c = 'TCI';
		a.strategic__c = false;
		insert a;

		Contact c = new Contact(Email = 'test-user-1@unit-test.com', LastName = 'Test', AccountId = a.Id);
		c.Remedy_PIN__c = '70208';
		insert c;
		Profile p = [SELECT Id FROM Profile WHERE UserType = 'CSPLitePortal' LIMIT 1];
		User u = new User(Alias = 'TestUser', Email = 'test-user-1@unit-test.com', ContactId = c.Id,
				EmailEncodingKey = 'UTF-8', FirstName = 'Unit', LastName = 'Testing', LanguageLocaleKey = 'en_US',
				LocaleSidKey = 'en_US', CommunityNickname = 'test-user-1UNTST', /*UserRoleId = portalRole.Id,*/ ProfileId = p.Id,
				TimeZoneSidKey = 'America/Los_Angeles',
				UserName = 'test-user-1@unit-test.com');

		u.Customer_Portal_Role__c = 'mits';

		insert u;
		System.runAs(u) {
			Case caseToInsert = ENTPTestUtils.createENTPCases(1, a.id, false)[0];
			caseToInsert.Lynx_Ticket_Number__c = null;
			insert caseToInsert;

			Case parentCase = [SELECT Id, ContactId, CaseNumber, My_Business_Requests_Type__c, LastModifiedDate, AccountId FROM Case ORDER BY CreatedDate DESC LIMIT 1];
			ENTPNewCaseTriggerHelper.SendLynxCreate(parentCase.id);

			// ENTPNewCaseTriggerHelper.SendRemedyCreate(parentCase.id);
		}
	}

	@isTest(SeeAllData=false) static void ENTPNewCaseTriggerHelperRemedy() {
		//User u = MBRTestUtils.createENTPPortalUser('test-user-1', false, 'en_US');
		Account a = new Account(Name = 'Unit Test Account');
		a.Remedy_Business_Unit_Id__c = 'PCLOUD';
		a.Remedy_Company_Id__c = 'TCS';
		a.strategic__c = false;
		insert a;

		Contact c = new Contact(Email = 'test-user-1@unit-test.com', LastName = 'Test', AccountId = a.Id);
		c.Remedy_PIN__c = '70208';
		insert c;
		Profile p = [SELECT Id FROM Profile WHERE UserType = 'CSPLitePortal' LIMIT 1];
		User u = new User(Alias = 'TestUser', Email = 'test-user-1@unit-test.com', ContactId = c.Id,
				EmailEncodingKey = 'UTF-8', FirstName = 'Unit', LastName = 'Testing', LanguageLocaleKey = 'en_US',
				LocaleSidKey = 'en_US', CommunityNickname = 'test-user-1UNTST', /*UserRoleId = portalRole.Id,*/ ProfileId = p.Id,
				TimeZoneSidKey = 'America/Los_Angeles',
				UserName = 'test-user-1@unit-test.com');

		u.Customer_Portal_Role__c = 'private cloud';

		insert u;
		Test.startTest();
		System.runAs(u) {

			Case caseToInsert = ENTPTestUtils.createENTPCases(1, a.id, false)[0];
			caseToInsert.Lynx_Ticket_Number__c = null;
			insert caseToInsert;

			Case parentCase = [SELECT Id, ContactId, CaseNumber, My_Business_Requests_Type__c, LastModifiedDate, AccountId FROM Case ORDER BY CreatedDate DESC LIMIT 1];
			Account k = [Select id, Remedy_Business_Unit_Id__c, Remedy_Company_Id__c from Account where id = :a.id];
			//ENTPNewCaseTriggerHelper.SendLynxCreate(parentCase.id);

			ENTPNewCaseTriggerHelper.SendRemedyCreate(parentCase.id);
		}
		Test.stopTest();
	}

	@isTest(seeAllData=false)
	static void sendCaseClosedEmailNotificationTest() {
		List<Id> caseIds = new List<Id>();
		List<String> collabEmails = new List<String>();
		String userName = 'test-user-1';
		String lang = 'en_US';
		Account a = new Account(Name = 'Unit Test Account');
		a.strategic__c = true;
		insert a;
		Contact c = new Contact(Email = userName + '@unit-test.com', LastName = 'Test', AccountId = a.Id);
		insert c;
		Profile p = [SELECT Id FROM Profile WHERE Name = 'Customer Community User Custom' AND UserType = 'CSPLitePortal' LIMIT 1];
		User u = new User(Alias = 'TestUser', Email = userName + '@unit-test.com', ContactId = c.Id,
				EmailEncodingKey = 'UTF-8', FirstName = 'Unit', LastName = 'Testing', LanguageLocaleKey = lang,
				LocaleSidKey = lang, CommunityNickname = userName + 'UNTST', ProfileId = p.Id,
				TimeZoneSidKey = 'America/Los_Angeles',
				UserName = userName + '@unit-test.com');
		u.Customer_Portal_Role__C = 'tps';
		insert u;

		Test.startTest();
		System.runAs(u) {
			List<Case> testCases = ENTPTestUtils.createENTPCases(2, a.Id);

			//List<Case> c = [Select id, NotifyCollaboratorString__c from Case where ParentId = :testCases[0].Id];
			if (testCases[0].NotifyCollaboratorString__c != null && testCases[0].NotifyCollaboratorString__c.length() > 0) {
				caseIds.add(testCases[0].Id);
				collabEmails.add(testCases[0].NotifyCollaboratorString__c);
			}
			if (caseIds.size() > 0) {
				//ENTPNewCaseTriggerHelper.sendCaseClosedEmailNotification(caseIds, collabEmails);
			}
		}
		Test.stopTest();
	}

	@isTest(SeeAllData=false) static void ENTPNewCaseTriggerHelperVitil() {
		trac_TriggerHandlerBase.blockTrigger = true;
		test.startTest();
		ENTPTestUtils.createSMBCareWebServicesCustomSetting();
		//User u = MBRTestUtils.createENTPPortalUser('test-user-1', false, 'en_US');
		Account a = new Account(Name = 'Unit Test Account');
		a.Remedy_Business_Unit_Id__c = 'TCI';
		a.Remedy_Company_Id__c = 'TCI';
		a.strategic__c = false;
		insert a;

		Contact c = new Contact(Email = 'test-user-1@unit-test.com', LastName = 'Test', AccountId = a.Id);
		c.Remedy_PIN__c = '70208';
		insert c;
		Profile p = [SELECT Id FROM Profile WHERE UserType='CSPLitePortal' LIMIT 1];
		User u = new User(Alias = 'TestUser', Email='test-user-1@unit-test.com', ContactId=c.Id,
				EmailEncodingKey='UTF-8', FirstName='Unit', LastName='Testing', LanguageLocaleKey='en_US',
				LocaleSidKey='en_US', CommunityNickname='test-user-1UNTST', /*UserRoleId = portalRole.Id*/ProfileId=p.Id,
				TimeZoneSidKey='America/Los_Angeles',
				UserName='test-user-1@unit-test.com');

		u.Customer_Portal_Role__c = null;
		u.Customer_Portal_Name__c = 'vitilcare';

		insert u;
		System.runAs(u) {
			Case caseToInsert = ENTPTestUtils.createENTPCases(1,a.id, false)[0];
			caseToInsert.Lynx_Ticket_Number__c = null;
			insert caseToInsert;

			Case parentCase = [SELECT Id, ContactId, CaseNumber, My_Business_Requests_Type__c, LastModifiedDate, AccountId FROM Case ORDER BY CreatedDate DESC LIMIT 1];
			ENTPNewCaseTriggerHelper.SendLynxVitil(parentCase.id);

			// ENTPNewCaseTriggerHelper.SendRemedyCreate(parentCase.id);
		}
		test.stopTest();
		trac_TriggerHandlerBase.blockTrigger = false;
	}
}