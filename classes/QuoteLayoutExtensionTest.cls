@isTest
private class QuoteLayoutExtensionTest {
	testMethod static void testLayoutExtension() {
        QuoteLayoutExtension qle = new QuoteLayoutExtension();
        /* Column Widths */
        System.assertEquals('40%', qle.column1Width); // Service Name
        System.assertEquals('39%', qle.column2Width); // Feature Name
        System.assertEquals('7%', qle.column3Width); // Quantity
        System.assertEquals('14%', qle.column4Width); // Price
        
        System.assertEquals('25%', qle.deviceColumn1Width); // Device Name
        System.assertEquals('5%', qle.deviceColumn2Width); // Quantity
        System.assertEquals('14%', qle.deviceColumn3Width); // Action
        System.assertEquals('8%', qle.deviceColumn4Width); // Service Term
        System.assertEquals('12%', qle.deviceColumn5Width); // No Term Price
        System.assertEquals('12%', qle.deviceColumn6Width); // Term Discount
        System.assertEquals('12%', qle.deviceColumn7Width); // Final Price
        System.assertEquals('12%', qle.deviceColumn8Width); // Airtime Credits
        /* End Column Widths */
        
        /* Fonts */
        System.assertEquals('9pt', qle.standardFontSize);
        System.assertEquals('sans-serif', qle.standardFontFamily);
        System.assertEquals('bold', qle.tableHeaderFontWeight);
        System.assertEquals('11pt', qle.sectionHeaderFontSize);
        System.assertEquals('normal', qle.sectionHeaderFontWeight);
        System.assertEquals('bold', qle.sectionFooterFontWeight);
        
        System.assertEquals('left', qle.standardTextAlign);
        System.assertEquals('center', qle.standardQuantityTextAlign);
        System.assertEquals('center', qle.headerQuantityTextAlign);
        System.assertEquals('right', qle.standardCurrencyTextAlign);
        System.assertEquals('right', qle.headerCurrencyTextAlign);
        /* End Fonts */
        
        /* Padding */
        System.assertEquals('4px', qle.standardPadding);
        /* End Padding */
        
        /* Colours */
        System.assertEquals('#000000', qle.standardColour);
        System.assertEquals('#49166d', qle.sectionHeaderBG);
        System.assertEquals('#ffffff', qle.sectionHeaderColour);
        System.assertEquals('#b799cc', qle.sectionFooterBG);
        System.assertEquals('#000000', qle.sectionFooterColour);
        System.assertEquals('#b799cc', qle.tableHeaderBG);
        System.assertEquals('#000000', qle.tableHeaderColour);
        System.assertEquals('#e8d9f1', qle.primaryLineBG);
        System.assertEquals('#ffffff', qle.secondaryLineBG);
        /* End Colours */
    }
}