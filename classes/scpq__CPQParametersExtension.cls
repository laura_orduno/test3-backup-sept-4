/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class CPQParametersExtension {
    global scpq__CPQParameters__c CPQParameters {
        get;
    }
    global String UserInfoSessionId {
        get;
    }
    global CPQParametersExtension(ApexPages.StandardController controller) {

    }
    global CPQParametersExtension(ApexPages.StandardSetController controller) {

    }
}
