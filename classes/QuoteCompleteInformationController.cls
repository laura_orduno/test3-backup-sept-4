public without sharing class QuoteCompleteInformationController {

    public List<ProductAddlInfoWrapper> productAddlInfoWrapperList {get; set;}
    public SBQQ__Quote__c quote {get; set;}
    public String quoteNumber {get{return quote.Name;} set;}
    public String companyName {get{return quote.SBQQ__BillingName__c;} set;}
    public String stage {get; private set;}
    public Boolean phoneNumberSetupValid {get; private set;}
    public Integer productIndex {get; set;}
    public List<SBQQ__QuoteLine__c> quoteLines;
    private List<AdditionalInformation__c> persistedAddlList;
    private Id quoteId;
    
        
    public QuoteCompleteInformationController() {
        try {
            productIndex = 1;
            quoteId = ApexPages.currentPage().getParameters().get('qid');
            quote = QuoteUtilities.loadQuote(quoteId);
            stage = ApexPages.currentPage().getParameters().get('stage');
            stage = (stage == null) ? quote.SBQQ__Status__c : EncodingUtil.urlDecode(stage, 'UTF-8');
            quoteLines = QuoteCustomConfigurationService.loadQuoteLines(quoteId);                               
            persistedAddlList = QuoteCustomConfigurationService.loadAdditionalData(quoteId);
            Map<SBQQ__QuoteLine__c, List<QuoteConfigFieldSetModel>> addlInfoWrapperByQuoteLine = 
                    QuoteCustomConfigurationService.buildQuoteConfigFieldSetModelByQuoteLine(stage, persistedAddlList, quoteLines);
            buildProductAddlInfoWrapperList(addlInfoWrapperByQuoteLine);
            updateFieldsState(); } catch (Exception e) { QuoteUtilities.log(e); }
    }
    
    public void buildProductAddlInfoWrapperList(Map<SBQQ__QuoteLine__c, List<QuoteConfigFieldSetModel>> fieldSetModelByQuoteLine) {
        productAddlInfoWrapperList = new List<ProductAddlInfoWrapper>();
        ProductAddlInfoWrapper paiw;
        for(SBQQ__QuoteLine__c ql : fieldSetModelByQuoteLine.keySet()) {
            paiw = new ProductAddlInfoWrapper();
            paiw.product = ql.SBQQ__Product__r;
            paiw.category = ql.SBQQ__Product__r.Configuration_Category__c;
            paiw.featureName = ql.SBQQ__ProductOption__r.SBQQ__Feature__r.Name;
            paiw.optionNumber = ql.SBQQ__ProductOption__r.SBQQ__Number__c;
            paiw.featureNumber = ql.SBQQ__ProductOption__r.SBQQ__Feature__r.SBQQ__Number__c;
            paiw.fieldSetModelWrapperList = fieldSetModelByQuoteLine.get(ql);
            productAddlInfoWrapperList.add(paiw);
        }
        productAddlInfoWrapperList = sortProductAdditionalWrapperListByFeatureAndOptionNumber(productAddlInfoWrapperList);
    }
    
    private List<ProductAddlInfoWrapper> sortProductAdditionalWrapperListByFeatureAndOptionNumber(List<ProductAddlInfoWrapper> unsortedList) {
        Map<Decimal, List<ProductAddlInfoWrapper>> productsByFeature = new Map<Decimal, List<ProductAddlInfoWrapper>>();
        List<ProductAddlInfoWrapper> productWrapperList;
        for(ProductAddlInfoWrapper productAddlInfoWrapper : unsortedList) {
            productWrapperList = productsByFeature.get(productAddlInfoWrapper.featureNumber);
            if(productWrapperList == null) {
                productWrapperList = new List<ProductAddlInfoWrapper>();
                productsByFeature.put(productAddlInfoWrapper.featureNumber, productWrapperList);
            }
            productWrapperList.add(productAddlInfoWrapper);
        }
        List<ProductAddlInfoWrapper> finalList = new List<ProductAddlInfoWrapper>();
        List<Decimal> sortedFeatureNumbers = new List<Decimal>(); 
        sortedFeatureNumbers.addAll(productsByFeature.keySet());
        sortedFeatureNumbers.sort();
        List<ProductAddlInfoWrapper> productByFeatureList;
        Map<Decimal, List<ProductAddlInfoWrapper>> productByNumber; 
        for(Decimal key : sortedFeatureNumbers) {
            productByFeatureList = productsByFeature.get(key);
            productByNumber = new Map<Decimal, List<ProductAddlInfoWrapper>>();
            for(ProductAddlInfoWrapper product : productByFeatureList) {
                productWrapperList = productByNumber.get(product.optionNumber);
                if(productWrapperList == null) {
                    productWrapperList = new List<ProductAddlInfoWrapper>();
                    productByNumber.put(product.optionNumber, productWrapperList);
                }
                productWrapperList.add(product);
            }
            List<Decimal> optionNumbers = new List<Decimal>(productByNumber.keySet());
            optionNumbers.sort();
            List<ProductAddlInfoWrapper> sortedList = new List<ProductAddlInfoWrapper>();
            for(Decimal optionKey : optionNumbers) {
                sortedList.addAll(productByNumber.get(optionKey));
            }
            finalList.addAll(sortedList);
        }
        return finalList;       
    } 
    
    public PageReference onSave() {
        try {
            if(validate()) {
                AdditionalInformation__c[] addlInfos = new List<AdditionalInformation__c>();
                for(ProductAddlInfoWrapper productAddlInfoWrapper : productAddlInfoWrapperList) {
                    for(QuoteConfigFieldSetModel quoteConfigFieldSetModel : productAddlInfoWrapper.fieldSetModelWrapperList) {
                        AdditionalInformation__c additionalInformation = quoteConfigFieldSetModel.additionalInformation;
                        if(!StringUtils.isBlank(productAddlInfoWrapper.product.Network__c)) {
                            additionalInformation.Network_Type__c = productAddlInfoWrapper.product.Network__c;
                        }
                        addlInfos.add(additionalInformation);
                    }
                }
                if (addlInfos != null && addlInfos.size() != 0) {
                    upsert addlInfos;
                }
                
            } else {
                return null;
            }
            String retUrl = ApexPages.currentPage().getParameters().get('retUrl'); 
            if(!StringUtils.isBlank(retUrl)) {
                return new Pagereference(EncodingUtil.urlDecode(retUrl, 'UTF-8'));  
            } else {
                return getQuoteSummaryPageReference();
            }
        } catch (Exception e) { QuoteUtilities.log(e); } return null;
    }
    
    private PageReference getQuoteSummaryPageReference() {  
        PageReference pref = Page.QuoteSummary;
        pref.getParameters().put('qid', quote.Id);
        pref.setRedirect(true);
        return pref;
    }
        
    public PageReference onCancel() {
        return getQuoteSummaryPageReference();
    }
    
    private boolean validate() {
        try {
        Boolean valid = true;
        String phoneNumberSetup;
        for(ProductAddlInfoWrapper productAddlInfoWrapper : productAddlInfoWrapperList) {
            Integer index = 0;
            for(QuoteConfigFieldSetModel quoteConfigFieldSetModel : productAddlInfoWrapper.fieldSetModelWrapperList) {
                AdditionalInformation__c additionalInformation = quoteConfigFieldSetModel.additionalInformation;
                if(productAddlInfoWrapper.category == 'Office Phone') {
                    if(index == 0) {
                        phoneNumberSetup = quoteConfigFieldSetModel.additionalInformation.Phone_Number_Setup__c;
                    } else {
                        quoteConfigFieldSetModel.additionalInformation.Phone_Number_Setup__c = phoneNumberSetup;
                    }
                    if (StringUtils.isBlank(phoneNumberSetup)) {
                        valid =  false;
                        productAddlInfoWrapper.phoneNumberSetupValid = false;
                    } else {
                        productAddlInfoWrapper.phoneNumberSetupValid = true;
                    }
                }
                for(QuoteConfigFieldSetModel.ConfigFieldViewModel fieldViewModel : quoteConfigFieldSetModel.configFieldViewModelList){
                    QuoteConfigFieldVO quoteConfigFieldVO = fieldViewModel.quoteConfigFieldVO;
                    if (StringUtils.isBlank((String)quoteConfigFieldSetModel.additionalInformation.get(quoteConfigFieldVO.fieldName)) && 
                        fieldViewModel.visible && fieldViewModel.required) {
                        valid =  false;
                        fieldViewModel.valid = false;
                    } else {
                        fieldViewModel.valid  = true;
                    }
                }
                index++;
            }
        }
        return valid;
        } catch (Exception e) { QuoteUtilities.log(e); } return null;
    }
    
    public PageReference updateFieldsState() {
        try {
        Boolean isPorting;
        Boolean isMaintainWC;
        Boolean isMaintainNC;
        Boolean isMaintain;
        Boolean isNewNumber;
        String phoneNumberSetup;
        for (ProductAddlInfoWrapper productAddlInfoWrapper : productAddlInfoWrapperList) {
            for (QuoteConfigFieldSetModel quoteConfigFieldSetModel : productAddlInfoWrapper.fieldSetModelWrapperList) {
                if(productAddlInfoWrapper.category == 'Existing Device') {
                    phoneNumberSetup = quoteConfigFieldSetModel.additionalInformation.Phone_Number_Setup_QED__c;
                } else {
                    phoneNumberSetup = quoteConfigFieldSetModel.additionalInformation.Phone_Number_Setup__c;                    
                }
                isPorting = 'Porting'.equalsIgnoreCase(phoneNumberSetup) ;
                isMaintainWC = 'Maintain - with change'.equalsIgnoreCase(phoneNumberSetup) ;
                isMaintainNC = 'Maintain - no change'.equalsIgnoreCase(phoneNumberSetup) ;
                isMaintain = 'Maintain'.equalsIgnoreCase(phoneNumberSetup) ;
                if (!isMaintain){ if (isMaintainWC) {isMaintain=true;} if (isMaintainNC) {isMaintain=true;} }
                isNewNumber = 'New Number'.equalsIgnoreCase(phoneNumberSetup) ;
                
                QuoteUtilities.configureFieldVisibility(quoteConfigFieldSetModel, productAddlInfoWrapper.category, isNewNumber, isPorting, isMaintain, isMaintainWC, isMaintainNC);
            }
        }
        } catch (Exception e) { QuoteUtilities.log(e); } return null;
    } 
        
    public class ProductAddlInfoWrapper {
        public Product2 product {get;set;}
        public String category {get;set;}
        public String featureName {get;set;}
        public Decimal featureNumber {get;set;}
        public Decimal optionNumber {get;set;}
        public Boolean phoneNumberSetupValid {get;set;}
        public List<QuoteConfigFieldSetModel> fieldSetModelWrapperList {get;set;}
        
        public ProductAddlInfoWrapper() {
            phoneNumberSetupValid = true;
        }
    }
}