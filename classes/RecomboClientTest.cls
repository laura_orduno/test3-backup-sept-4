@isTest
public class RecomboClientTest {
	private testMethod static void test() {
        Document doc = new Document(Name='Test.pdf',ContentType='application/pdf',Body=Blob.valueOf('test'));
        
        Recombo.EmailInfo emailInfo = new Recombo.EmailInfo('Apex Test','Apex Test Body');
        List<Recombo.Person> persons = new List<Recombo.Person>();
        Recombo.Person person = new Recombo.Person('test@recombo.com','Test','User');
        RecomboClient target = new RecomboClient('https://www.example.com/hapi', 'test','test');
        target.persons.add(person);
        target.document = new Recombo.Document('test', doc.Name, doc.ContentType, doc.Body);
        target.emailInfo = emailInfo;
        target.sendDocument();
        System.assert(target.documentURL != null);
        System.assert(target.waybillId != null);
        
        target.deleteDocument('983148923535353536497364976347634', null);
    }
    
    @isTest
    public static HttpResponse createTestLoginResponse() {
        HttpResponse response = new HttpResponse();
        response.setStatusCode(200);
        response.setBody('<document><error>0</error><sid>SID</sid></document>');
        return response;
    }
    
    @isTest
    public static HttpResponse createTestDeleteDocumentResponse() {
        HttpResponse response = new HttpResponse();
        response.setStatusCode(200);
        response.setBody('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><response><error>0</error><message>1</message></response>');
        return response;
    }
    
    @isTest
    public static HttpResponse createTestSendDocumentResponse() {
        HttpResponse response = new HttpResponse();
        response.setStatusCode(200);
        response.setBody('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><response><error>0</error><message>Success</message><waybillId>D7033E97-09B5-9C9C-F64C-BBB5CB27C1F8</waybillId><docUrl><![CDATA[https://agreementexpress.net/68585/05189/1AEE7/Express_Agreement_Test.html?launchId=215311:345a84acd1f4b9f54d0c756e93dcd960e79fc45e6ebbb74ca9aa9d3e3c12e82fac0de4a6206206b708264ff01e1d90ea7ef71a4a5158f1f1928db909252f70d12d0c83c4451f7cae71854f449acf6041d639350ff8ac2b2be9374b106acc504eb07611efc5d7536ce8a1567411ca4674a6d7d4deb619f5a7f1674aa4bc0f5c8b&username=test@recombo.com&st=1&g=0]]></docUrl></response>');
        return response;
    }
}