public class FrontlineHelper {
    
    static public Case createCase(String subject, String description, String sfdcAccountId, String lynxTicketNo)
    {
        // get record type map
        Map<String, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Case.getRecordTypeInfosByName();
        //get user info
        
        Case c = new Case();
        try{
                     User cnt = [SELECT Id, ContactId FROM User WHERE Id = :sfdcAccountId LIMIT 1];
                    if (cnt != null)
                        c.ContactId = cnt.ContactId;
                    // Specify DML options to ensure the assignment rules are executed
                    Database.DMLOptions dmlOpts = new Database.DMLOptions();
                    dmlOpts.assignmentRuleHeader.useDefaultRule = true;
                    c.setOptions(dmlOpts);
        c.subject = subject;
        c.description = description;
        c.lynx_ticket_number__c = lynxTicketNo;
        c.recordTypeId = rtMap.get('ENT Care Assure').getRecordTypeId();
        c.origin = 'VITILcare';
            
System.debug('FrontlineHelper->createCase, subject(' + c.subject + '), description(' + c.description + ')');
        
        
            insert c;        
            return c;
        }
        catch(Exception e){
            System.debug('FrontlineHelper->createCase, exception: ' + e.getMessage());
        }
    
        return null;        
    }    
}