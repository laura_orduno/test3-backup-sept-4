/*
* -------------------------------------------------------------------------------------------------
* This class helps in Order amending. 
* 
* -------------------------------------------------------------------------------------------------
* @author         Santosh K Rath   
* @modifiedBy     Santosh K Rath   
* @version        1.0
* @created        2016-01-20
* @modified       YYYY-MM-DD
* --------------------------------------------------------------------------------------------------
* @changes
* vX.X            Santosh K Rath
* YYYY-MM-DD      Explanation of the change.  
*
* vX.X            Santosh K Rath
* YYYY-MM-DD      
* -------------------------------------------------------------------------------------------------
*/

public class OrdrAmendController {
    public boolean showDueDateWarning{get;set;}
  String orderId { get; set; } 
  Public Order ordObj{ get; set; }   
public String ExistingRemark = '';
   public  String amendComments{
    get{
        if(amendComments==null){
            //amendComments='Please provide the reasons for Amend';
            amendComments='';
        }
        return amendComments;
    }
    set;
    }
    
    public OrdrAmendController(ApexPages.StandardController controller) 
    {
        orderId=ApexPages.currentPage().getParameters().get('id');
        ordObj=[select type,status, General_Notes_Remarks__c from Order where id=:orderId];
        DueDateWarningMsg();
        if(!String.isBlank(ordObj.General_Notes_Remarks__c))
                ExistingRemark = ordObj.General_Notes_Remarks__c;
        ordObj.General_Notes_Remarks__c='';
    }
    
    public void DueDateWarningMsg()
    {
        String orderId=ApexPages.currentPage().getParameters().get('id');
        //List<Order> orderList=[select RequestedDate__c from Order where id=:orderId];
        showDueDateWarning = isDueDateLessThan48Hrs( orderId);
    }
    
    public static boolean isDueDateLessThan48Hrs(ID orderId)
    {
        List<OrderItem> orderItems = [select id, Scheduled_Due_Date__c from OrderItem where OrderId=:orderId];
        if(orderItems != null)
        {
                for(OrderItem item:orderItems)
                {
                        if(item.Scheduled_Due_Date__c!=null)
                        {
                                DateTime dueDate = item.Scheduled_Due_Date__c;
                                DateTime today = DateTime.now();
                                DateTime todayPlus = today.addDays(2);
                                if(todayPlus > dueDate)
                                        return true;
                        }
                }
        }
        return false;
    }

    public Pagereference prepareForAmend()
    {
          System.debug('Preparing for command ');
   // Commented and moved into Constructor 
     //String orderId=ApexPages.currentPage().getParameters().get('id');
    // List<Order> orderList=[select status from Order where id=:orderId];
     try{
        
        
        //ordObj.General_Notes_Remarks__c=amendComments;
        if(String.isBlank(ordObj.General_Notes_Remarks__c)){
        apexpages.addmessage(new apexpages.message(apexpages.severity.error,'General Notes Remarks cannot be blank.'));
        return null;
        }
        else
      {
        ordObj.status='Not Submitted';
       // ordObj.orderMgmtId__c=null;
       ordObj.General_Notes_Remarks__c = ExistingRemark + ordObj.General_Notes_Remarks__c;
        ordObj.vlocity_cmt__ValidationStatus__c=null;
        ordObj.id=orderId;
        ordObj.type='';  
        update ordObj;
        
       /* List<OrderItem> orderItemList=[SELECT id,vlocity_cmt__AssetId__c FROM OrderItem WHERE ORDERID=:orderId];
        for(OrderItem orderItemObj:orderItemList){
            orderItemObj.vlocity_cmt__AssetId__c=null;
            orderItemObj.vlocity_cmt__AssetReferenceId__c=null;
            update orderItemObj;
        }*/
       }
      System.debug('returning from prepareForAmend');   
     }catch(Exception e){
        System.debug(e);
        return null;
     }     
    Pagereference pref= new pagereference('/apex/Order_PAC_Address?id='+orderId);
    // Pagereference pref= new pagereference('/'+orderId);
     pref.setRedirect(true);                            
     return pref; 
  } 
        
   
}