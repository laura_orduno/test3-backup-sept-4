public class DefaultConditionalTemplateSections {
    
    public static Map<String,Object> getTempSectionsBasedonProdcts (Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options){
        List<vlocity_cmt__DocumentTemplateSection__c> qualifiedSections = new List<vlocity_cmt__DocumentTemplateSection__c> ();
        
        Map<Id, vlocity_cmt__DocumentTemplateSection__c> sectionsMap = (Map<Id, vlocity_cmt__DocumentTemplateSection__c>) inputMap.get('allSections');
        List<SObject> items =(List<SObject>) inputMap.get('items');  
       // Logger.dbg(' In DefaultGetConditionalTemplateSections, items are '+items);
        Id contextId = (Id) inputMap.get('contextId');
        String itemObjName;
        if(items !=null && items.size()>0){
            Schema.SObjectType itemObjType =  Id.valueOf(items[0].Id).getSObjectType();
            itemObjName = itemObjType.getDescribe().getName();
            system.debug('>>> ObjName' + itemObjName );
        boolean hasConditionalSection = hasConditionalSection(sectionsMap);
        if(hasConditionalSection){
           // Logger.dbg(' This template has conditional section ');
            List<Id> productIds = getProductIds(items);
            
            List<vlocity_cmt__DocumentTemplateSectionCondition__c> productSections = [SELECT vlocity_cmt__Product2Id__c, vlocity_cmt__EntityFilterId__c ,vlocity_cmt__DocumentTemplateSectionId__c,vlocity_cmt__ApplicableItemType__c 
                                                                                      FROM vlocity_cmt__DocumentTemplateSectionCondition__c 
                                                                                      WHERE vlocity_cmt__DocumentTemplateSectionId__c IN :sectionsMap.keySet() 
                                                                                      order by vlocity_cmt__DocumentTemplateSectionId__c LIMIT 10000];
            if(productSections !=null && productSections.size()>0){
                Map<Id, Map<Id, String>> conditionMap = new Map<Id, Map<Id, String>> ();
                Map<Id, List<Id>> entityConditionMap = new Map<Id, List<Id>> ();
                List<Id> entityFilterList = new List<Id> ();
                Map<Id, String> sectionHasEntityConditionMap = new Map<Id, String> ();
                //construct section and product maps
                for(vlocity_cmt__DocumentTemplateSectionCondition__c productSection  : productSections){
                    Id sectionId = productSection.vlocity_cmt__DocumentTemplateSectionId__c;
                    Id productId = productSection.vlocity_cmt__Product2Id__c;
                    Id entityFilterId = productSection.vlocity_cmt__EntityFilterId__c;
                    String applicableType = productSection.vlocity_cmt__ApplicableItemType__c;
                    String namedApplicableType = 'vlocity_cmt__'+applicableType;
                    System.debug(' In defaultGetConditionalSections, applicable type is '+namedApplicableType + 'EntityFilter' + entityFilterId);
                    if(entityFilterId !=null && itemObjName!=null && namedApplicableType==itemObjName){
                        if(entityConditionMap.get(entityFilterId) ==null){
                            List<Id> sectionIds = new List<Id> ();
                            sectionIds.add(sectionId);
                            entityConditionMap.put(entityFilterId, sectionIds);
                        }
                        else{
                            List<Id> sectionIds = entityConditionMap.get(entityFilterId);
                            sectionIds.add(sectionId);
                        }
                        entityFilterList.add(entityFilterId);
                        sectionHasEntityConditionMap.put(sectionId, 'Yes');
                    }
                    else if(productId !=null){
                        if(conditionMap.get(sectionId) == null){
                            Map<Id, String> productMap = new Map<Id, String> ();
                            productMap.put(productId,'Yes');
                            conditionMap.put(sectionId, productMap);
                        }
                        else{
                            Map<Id, String> productMap = conditionMap.get(sectionId);
                            productMap.put(productId,'Yes');
                        }
                	}
                }
                system.debug(' In defaultGetConditionalSections, conditionMap is '+conditionMap+' Size is '+conditionMap.size());
                system.debug(' In defaultGetConditionalSections, sectionsMap is '+sectionsMap +' Size is '+sectionsMap.size());
                
                //check entity filter first
                Map<Id, String> entityQualifiedSectionMap = checkEntityFilterQualification(entityFilterList, items, entityConditionMap);
                 system.debug(' after checking entity filter qualification, qualified sections are '+entityQualifiedSectionMap);

                //exclude the conditional sections not qualified
                for(Id sectionId : sectionsMap.keySet()){
                    //if the section is not part of any condition, it is qualified
                    if(conditionMap.get(sectionId) == null && sectionHasEntityConditionMap.get(sectionId)==null){
                        qualifiedSections.add(sectionsMap.get(sectionId));
                        system.debug(' In defaultGetConditionalSections, section Id '+sectionId+ ' has no condition, it is qualified');
                      }
                    else if(entityQualifiedSectionMap.get(sectionId) =='Yes'){
                        qualifiedSections.add(sectionsMap.get(sectionId));
                    }
                    //if it is part of some condition, check products
                    else{
                        Map<Id, String> productMap = conditionMap.get(sectionId);
                         system.debug(' productmap is '+productMap);                   
                      if(productMap !=null && productMap.size()>0){
                        for(Id prodId : productIds){
                            //only need to qualify one product
                            if(productMap.get(prodId) =='Yes'){
                                qualifiedSections.add(sectionsMap.get(sectionId));
                                system.debug(' In defaultGetConditionalSections, section Id '+sectionId+ ' has product '+prodId +', it is qualified');
                                break;
                            }
                        }
                    }
                }
            }
        }
      }
        //else all sections are qualified
      else{
            for(Id sectionId : sectionsMap.keySet()){
                qualifiedSections.add(sectionsMap.get(sectionId));
            	}
        	}
        }
        
    //if no item is available, all conditional sections should not be available. 
    else {
        Map<Id, boolean> conditionalSectionsMap=returnConditionalSections(sectionsMap);
        for(Id sectionId : sectionsMap.keySet()){
            boolean hasCondition = conditionalSectionsMap.get(sectionId);
            if(hasCondition ==null || !hasCondition){
                qualifiedSections.add(sectionsMap.get(sectionId));
            }
        }
    }
        system.debug(' In defaultGetConditionalSections, qualified sections size is '+qualifiedSections.size());
        outMap.put('qualifiedSections', qualifiedSections);
        return outMap;
    
 }
    
    private static boolean hasConditionalSection(Map<Id, vlocity_cmt__DocumentTemplateSection__c> sectionsMap){
        for(Id sectionId : sectionsMap.keySet()){
            vlocity_cmt__DocumentTemplateSection__c section = sectionsMap.get(sectionId);
            if(section.vlocity_cmt__CountOfSectionConditions__c !=null && section.vlocity_cmt__CountOfSectionConditions__c >0){
                return true;
            }
        }
        return false;
        
    }
    
    public static Map<Id, boolean> returnConditionalSections(Map<Id, vlocity_cmt__DocumentTemplateSection__c> sectionsMap){
        Map<Id, boolean> conditionalSectionsMap = new Map<Id, boolean> ();
        for(Id sectionId : sectionsMap.keySet()){
            vlocity_cmt__DocumentTemplateSection__c section = sectionsMap.get(sectionId);
            if(section.vlocity_cmt__CountOfSectionConditions__c !=null && section.vlocity_cmt__CountOfSectionConditions__c >0){
                conditionalSectionsMap.put(sectionId, true);
            }
        }
        return conditionalSectionsMap;
        
    }
    
    
    
    
    private static List<Id> getProductIds(List<SObject>items){
        List<Id> pricebookEntryIds = new List<Id> ();
        String nameSpaceprefix = 'vlocity_cmt__';
        List<Id> prodIds = new List<Id> ();
        if(items !=null && items.size()>0){
            Schema.SObjectType itemObjType =  Id.valueOf(items[0].Id).getSObjectType();
            String itemObjName = itemObjType.getDescribe().getName();
            //for ContractLineItem, the Product2Id__c is always populated
            for(Sobject item : items){
                if(itemObjName==nameSpaceprefix+'ContractLineItem__c'){
                    prodIds.add((Id)item.get(nameSpaceprefix+'Product2Id__c'));
                }
                //else if opty/quote/order standard object
                else if(!itemObjName.contains('__c')){
                    pricebookEntryIds.add((Id)item.get('PricebookEntryId'));	    			
                }
            }
            //Logger.dbg('pricebookentryList '+pricebookEntryIds);
            //only when needed, query pricebookentryList to get productId
            if(pricebookEntryIds !=null && pricebookEntryIds.size()>0){
                AggregateResult [] pricebookEntryList = [Select Product2Id from PricebookEntry where Id IN : pricebookEntryIds group by Product2Id LIMIT 10000];
                system.debug('pricebookentryList '+pricebookEntryIds);
                if(pricebookEntryList !=null && pricebookEntryList.size()>0){
                    for(AggregateResult pbe : pricebookEntryList){
                        prodIds.add((Id)pbe.get('Product2Id'));
                    }
                }
            }
        }
        
        return prodIds;
    }
    
    
        private static Map<Id, String> checkEntityFilterQualification(List<Id>entityFilterList, List<SObject>items, Map<Id, List<Id>> entityConditionMap){
          Map<Id, String> qualifiedSections = new Map<Id, String> ();
          Map<String, Object> inputMap = new Map<String, Object> ();
          Map<String, Object> outputMap = new Map<String, Object> ();
          Map<String, Object> options = new Map<String, Object> ();
          String className='vlocity_cmt.EntityFilterSupport';
          Type classType = Type.forName(className);
                        	
            
          List<Id> itemIds = new List<Id> ();
          for(SObject item : items){
            itemIds.add(item.Id);
          }
          inputMap.put('objectIds', itemIds);          
          inputMap.put('entityFilterIds', entityFilterList); 
          
            if(!Test.isRunningTest()  ){
                vlocity_cmt.VlocityOpenInterface vpsi = (vlocity_cmt.VlocityOpenInterface)classType.newInstance();  
				 vpsi.invokeMethod('executeFilters',inputMap, outputMap, options);
            }
          
          
          Map<Id, List<Id>> filterIdToEntityIdsList = new Map<Id, List<Id>> ();
          Map<Id, boolean> filterIdToEvalutation = new Map<Id, boolean> ();
          filterIdToEvalutation = (Map<Id, boolean>) outputMap.get('filterIdToEvalutation');                     
          List<Id> qualifiedSectonIds = new List<Id> ();
          if(filterIdToEvalutation!=null && filterIdToEvalutation.size()>0){
              for(Id filterId : filterIdToEvalutation.keySet()){
                boolean evaluation = filterIdToEvalutation.get(filterId);
                if(evaluation){
                    qualifiedSectonIds.addAll(entityConditionMap.get(filterId));
                }
              }
          }
          
          if(qualifiedSectonIds.size()>0){
            for(Id sectionId : qualifiedSectonIds){
                qualifiedSections.put(sectionId, 'Yes');
                System.debug(' In defaultGetConditionalSections, entity filter qualifies section '+qualifiedSections);
            }
          }
    
        return qualifiedSections;
    }
    
}