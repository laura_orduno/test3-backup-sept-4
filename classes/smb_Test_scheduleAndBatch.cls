/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class smb_Test_scheduleAndBatch {

    static testMethod void smb_processECAT_sch_Test() {
        
        // Setup test data
        // This code runs as the system user
        /*Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standard@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, EmployeeId__c ='12' , 
        TimeZoneSidKey='America/Los_Angeles', UserName='standard@trg.com');*/
        User u = [SELECT Id FROM User WHERE Name ='Informatica Integration Application' limit 1];

        System.runAs(u) {
    
            RecordType recordType = [Select r.Id, r.DeveloperName From RecordType r where r.SobjectType = 'Account' and r.DeveloperName='RCID' limit 1];
            
            Account acc = new Account(Name='Testing Software', BillingCountry = 'US', BillingState = 'IL', recordTypeId=recordType.Id );
            insert acc;
            
            Sales_Assignment__c sa = new Sales_Assignment__c( Account__c = acc.id, AcctRole__c ='xyz', Name='abc' , Territory_WLS__c = 'wqe', 
                                                              Role__c = 'ACCTPRIME', Territory_WLN__c ='rst', User__c = u.id );
            insert sa;    
            
            
            // TO DO: implement unit test 
            smb_processECAT_sch testSched = new smb_processECAT_sch();
            testSched.execute(null);
        }    
    }
    
    static testMethod void smb_moveUserToAccountTeam_Batch_Test() {

        // Setup test data
        // This code runs as the system user
        /*Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt1', Email='ffffg1@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, EmployeeId__c ='12' , 
        TimeZoneSidKey='America/Los_Angeles', UserName='ffffg1@te.in');*/
        
        User u = [SELECT Id FROM User WHERE Name ='Informatica Integration Application' limit 1];

        System.runAs(u) {      
            RecordType recordType = [Select r.Id, r.DeveloperName From RecordType r where r.SobjectType = 'Account' and r.DeveloperName='RCID' limit 1];
            
            Account acc = new Account(Name='Testing Software', BillingCountry = 'US', BillingState = 'IL', recordTypeId=recordType.Id );
            insert acc;
            
            Sales_Assignment__c sa = new Sales_Assignment__c( Account__c = acc.id, AcctRole__c ='xyz', Name='abc' , Territory_WLS__c = 'wqe', 
                                                              Role__c = 'ACCTPRIME', Territory_WLN__c ='rst', User__c = u.id );
            insert sa;    
            
            String atmQuery;
            atmQuery = 'Select Account__c From Sales_Assignment__c Where User__r.IsActive = true';
      
           Test.startTest();
           smb_moveUserToAccountTeam_Batch batchjob = new smb_moveUserToAccountTeam_Batch(atmQuery);
           ID batchProcessId = Database.executeBatch(batchjob,200);
           Test.stopTest();
        }  
    }
    
    static testMethod void smb_assignAccountOwnerTerritory_Batch_Test() {

        // Setup test data
        // This code runs as the system user
        /*Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt1', Email='ftttt@testo.com', 
        EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, EmployeeId__c ='12' , 
        TimeZoneSidKey='America/Los_Angeles', UserName='ftttt@te.in');*/
        User u = [SELECT Id FROM User WHERE Name ='Informatica Integration Application' limit 1];

        System.runAs(u) {      
            RecordType recordType = [Select r.Id, r.DeveloperName From RecordType r where r.SobjectType = 'Account' and r.DeveloperName='RCID' limit 1];
            
            Account parAcc = new Account(Name='Testing Software', BillingCountry = 'US', BillingState = 'IL', recordTypeId=recordType.Id );
            insert parAcc;
            
                    
            Terr_Code__c wAcc = new Terr_Code__c(Name='Testing Software1', Territory_Code__c ='tyy' );
            insert wAcc;
            
            Account acc = new Account(Name='Testing Software2', BillingCountry = 'US', BillingState = 'IL', recordTypeId=recordType.Id , ParentId =parAcc.id,
                                      Wireless_Territory__c = wAcc.id);
            insert acc;
            
            Sales_Assignment__c sa = new Sales_Assignment__c( Account__c = acc.id, AcctRole__c ='xyz', Name='abc' , Territory_WLS__c = 'wqe', 
                                                              Role__c = 'ACCTPRIME', Territory_WLN__c ='rst', User__c = u.id );
            insert sa;    
            
            String atmQuery;
            atmQuery = 'Select Account__c, Territory_WLN__c, Territory_WLS__c, User__c From Sales_Assignment__c Where User__r.IsActive = true';
      
           Test.startTest();
           smb_assignAccountOwnerTerritory_Batch batchjob = new smb_assignAccountOwnerTerritory_Batch(atmQuery);
           ID batchProcessId = Database.executeBatch(batchjob,200);
           Test.stopTest();
        }  
    }    
   
    static testMethod void smb_moveProductToAsset_sch_Test() {

        // Setup test data
        // This code runs as the system user
        /*Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt1', Email='ffffg1@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, EmployeeId__c ='12' , 
        TimeZoneSidKey='America/Los_Angeles', UserName='ffffg1@te.in');*/
        User u = [SELECT Id FROM User WHERE Name ='Informatica Integration Application' limit 1];

        System.runAs(u) {      
    
           RecordType recordType = [Select r.Id, r.DeveloperName From RecordType r where r.SobjectType = 'Account' and r.DeveloperName='RCID' limit 1];
                
           Account acc1 = new Account(Name='Testing Software', BillingCountry = 'US', BillingState = 'IL', recordTypeId=recordType.Id , No_Chronic_Incidents__c =10.0);
           insert acc1;
           
           Contact cont = new Contact(Lastname= 'lastname', Account = acc1);
           insert cont;
           
           
           Opportunity opp = new Opportunity(Name = 'Test Opp', AccountId = acc1.id, Type = 'New Customer', CloseDate = Date.today(), StageName = 'Prospecting');
           insert opp;
            
           
           
           SBQQ__Quote__c quote = new SBQQ__Quote__c(SBQQ__Account__c = acc1.id, SBQQ__BillingCity__c ='noida', SBQQ__Opportunity__c = opp.id); 
           insert quote;
           
           Product2 prod = new Product2(Name='abc');
           insert prod;  
           
           SBQQ__QuoteLine__c quoteLine = new SBQQ__QuoteLine__c(Category__c ='abc', SBQQ__Quote__c = quote.id , SBQQ__Product__c=prod.id);
           
           insert quoteLine;
           
           Asset asset=  new Asset(Account=acc1, SBQQ__QuoteLine__c = quoteLine.id , Name='abc', Contact=cont, AccountId = acc1.id );
           insert asset;
           
           Product2 p = new Product2(Name='Test');
           insert p;
           
           SBQQ__QuoteLine__c quoteLineSecond = new SBQQ__QuoteLine__c(Category__c ='xyz', SBQQ__Quote__c = quote.id,SBQQ__Product__c=p.Id);
           
           insert quoteLineSecond;
            smb_moveProductToAsset_sch testSched = new smb_moveProductToAsset_sch();
            testSched.execute(null);
        }   
    }   
    
    static testMethod void smb_moveProductToAsset_Batch_Test() {
 
        // Setup test data
        // This code runs as the system user
        /*Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt1', Email='ffffg1@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, EmployeeId__c ='12' , 
        TimeZoneSidKey='America/Los_Angeles', UserName='ffffg1@te.in');*/
        User u = [SELECT Id FROM User WHERE Name ='Informatica Integration Application' limit 1];

        System.runAs(u) {      
    
           RecordType recordType = [Select r.Id, r.DeveloperName From RecordType r where r.SobjectType = 'Account' and r.DeveloperName='RCID' limit 1];
                
           Account acc1 = new Account(Name='Testing Software', BillingCountry = 'US', BillingState = 'IL', recordTypeId=recordType.Id , No_Chronic_Incidents__c =10.0);
           insert acc1;
           
           Contact cont = new Contact(Lastname= 'lastname', Account = acc1);
           insert cont;
           
           
           Opportunity opp = new Opportunity(Name = 'Test Opp', AccountId = acc1.id, Type = 'New Customer', CloseDate = Date.today(), StageName = 'Prospecting');
           insert opp;
            
           
           
           SBQQ__Quote__c quote = new SBQQ__Quote__c(SBQQ__Account__c = acc1.id, SBQQ__BillingCity__c ='noida', SBQQ__Opportunity__c = opp.id); 
           insert quote;
           
           Product2 prod = new Product2(Name='abc');
           insert prod;  
           
           SBQQ__QuoteLine__c quoteLine = new SBQQ__QuoteLine__c(Category__c ='abc', SBQQ__Quote__c = quote.id , SBQQ__Product__c=prod.id);
           
           insert quoteLine;
           
           Asset asset=  new Asset(Account=acc1, SBQQ__QuoteLine__c = quoteLine.id , Name='abc', Contact=cont, AccountId = acc1.id );
           insert asset;
           
           Product2 p = new Product2(Name='Test');
           insert p;
           
           SBQQ__QuoteLine__c quoteLineSecond = new SBQQ__QuoteLine__c(Category__c ='xyz', SBQQ__Quote__c = quote.id, SBQQ__Product__c=p.Id);
           
           insert quoteLineSecond;

            
            String atmQuery;
           
                atmQuery  = 'select id,'+
                        'CreatedById,'+
                        'CreatedDate,'+
                        'LastModifiedById,'+
                        'LastModifiedDate,'+
                        'SBQQ__NetTotal__c,'+
                        'SBQQ__Quantity__c,'+
                        'SBQQ__EndDate__c,'+
                        'SBQQ__Product__c,'+
                        
                        'SBQQ__Product__r.name,'+
                        'SBQQ__Product__r.CurrencyIsoCode,'+
                        'SBQQ__Product__r.Description,'+
                        'SBQQ__RequiredBy__c,'+
                        'SBQQ__RequiredBy__r.name,'+
                        'SBQQ__RequiredBy__r.SBQQ__Product__c,'+
                        
                        'SBQQ__Quote__r.id,'+
                        'SBQQ__Quote__r.SBQQ__Account__c,'+
                        'SBQQ__Quote__r.SBQQ__PrimaryContact__c,'+
                        'SBQQ__Quote__r.SBQQ__Status__c,'+
                        'SBQQ__Quote__r.SBQQ__EndDate__c,'+
                        'SBQQ__Quote__r.LastModifiedDate,'+
                        'SBQQ__Quote__r.Completed__c,'+
                        'SBQQ__Quote__r.SBQQ__Opportunity__r.Web_Account__r.RCID__c'+' '+
                        'FROM SBQQ__QuoteLine__c '+' ';
           
      
           Test.startTest();
           smb_moveProductToAsset_Batch batchjob = new smb_moveProductToAsset_Batch(atmQuery);
           ID batchProcessId = Database.executeBatch(batchjob,200);
           Test.stopTest();
        }  
    }    
    
    static testMethod void smb_UpdateHierarchyOnAsset_batch_Test() {

        // Setup test data
        // This code runs as the system user
        /*Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt1', Email='ffffg1@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, EmployeeId__c ='12' , 
        TimeZoneSidKey='America/Los_Angeles', UserName='ffffg1@te.in');*/
        User u = [SELECT Id FROM User WHERE Name ='Informatica Integration Application' limit 1];

        System.runAs(u) {      
    
           RecordType recordType = [Select r.Id, r.DeveloperName From RecordType r where r.SobjectType = 'Account' and r.DeveloperName='RCID' limit 1];
                
           Account acc1 = new Account(Name='Testing Software', BillingCountry = 'US', BillingState = 'IL', recordTypeId=recordType.Id , No_Chronic_Incidents__c =10.0);
           insert acc1;
           
           Contact cont = new Contact(Lastname= 'lastname', Account = acc1);
           insert cont;
           
           
           Opportunity opp = new Opportunity(Name = 'Test Opp', AccountId = acc1.id, Type = 'New Customer', CloseDate = Date.today(), StageName = 'Prospecting');
           insert opp;
            
           
           SBQQ__Quote__c quote = new SBQQ__Quote__c(SBQQ__Account__c = acc1.id, SBQQ__BillingCity__c ='noida', SBQQ__Opportunity__c = opp.id); 
           insert quote;
            
           Product2 p = new Product2(Name='Test');
           insert p;       
      
           SBQQ__QuoteLine__c quoteLine = new SBQQ__QuoteLine__c(Category__c ='abc', SBQQ__Quote__c = quote.id,SBQQ__Product__c=p.Id);
           
           insert quoteLine;
           
           Asset asset=  new Asset(Account=acc1, SBQQ__QuoteLine__c = quoteLine.id , Name='abc', Contact=cont, AccountId = acc1.id );
           insert asset;

                 
          String atmQuery  = 'select id,'+
                        'SBQQ__QuoteLine__c,'+
                        'ParentId__c,'+
                        'SBQQ__QuoteLine__r.SBQQ__RequiredBy__c'+ ' ' + 
                        'from Asset';
           
      
           Test.startTest();
           smb_UpdateHierarchyOnAsset_batch batchjob = new smb_UpdateHierarchyOnAsset_batch(atmQuery);
           ID batchProcessId = Database.executeBatch(batchjob,200);
           Test.stopTest();
        }  
    }    
    
}