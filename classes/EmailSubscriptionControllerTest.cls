@isTest
private class EmailSubscriptionControllerTest {
	static testMethod void testSubscribe() {
        Account a = new Account(Name = 'Test Account ESC');
        insert a;
        Contact c = new Contact(FirstName = 'ESCFirst', LastName = 'ESCLast', Phone = '6049170274', Email = 'esctesttelus@example.com');
        insert c;
        ApexPages.StandardController sc = new ApexPages.StandardController(c);
        ApexPages.currentPage().getParameters().put('id', c.id);
        EmailSubscriptionController esc = new EmailSubscriptionController(sc);
        esc.doSubscribe();
        System.assertEquals(esc.refreshParent, true);
        Contact cx = [Select SubscribeContact__c, Subscribe_Request_Date__c From Contact Where Id = :c.id];
        System.assertEquals(cx.SubscribeContact__c, true);
        System.assertEquals(cx.Subscribe_Request_Date__c, Date.today());
    }
    static testMethod void testUnsubscribe() {
        Account a = new Account(Name = 'Test Account ESC');
        insert a;
        Contact c = new Contact(FirstName = 'ESCFirst', LastName = 'ESCLast', Phone = '6049170274', Email = 'esctesttelus@example.com');
        insert c;
        ApexPages.StandardController sc = new ApexPages.StandardController(c);
        ApexPages.currentPage().getParameters().put('id', c.id);
        EmailSubscriptionController esc = new EmailSubscriptionController(sc);
        esc.doUnsubscribe();
        System.assertEquals(esc.refreshParent, true);
        Contact cx = [Select UnsubscribeContact__c, Unsubscribe_Request_Date__c From Contact Where Id = :c.id];
        System.assertEquals(cx.UnsubscribeContact__c, true);
        System.assertEquals(cx.Unsubscribe_Request_Date__c, Date.today());
    }
}