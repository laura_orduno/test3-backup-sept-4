@isTest
private class FillNCSCTest {
    private static testMethod void testFillNCSC() {
    	AtoZ_Assignment__c az = new AtoZ_Assignment__c(AtoZ_Field__c = 'SvcSpec_NCSC');
    	insert az;
    	
    	final Datetime timestamp = Datetime.now();
    	
    	Test.startTest();
    	
    	Database.executeBatch(new FillNCSC(), 1000);
    	
    	Test.stopTest();
    }
}