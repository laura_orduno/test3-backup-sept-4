/*
 * Tests for trac_RepairUpdateBatch
 * Part of the SMB Repairs project
 *
 * @author: Grant Adamson, Traction On Demand 
 */

@isTest
private class trac_RepairUpdateBatchTest {
	
	private static testMethod void testRun() {
		trac_RepairTestUtils.createTestEndpoint();
		
		List<Case> cases = new List<Case>{
			new Case(repair__c = trac_RepairTestUtils.REPAIR_ID_USE_CASE_ID, SRD_Repair_Status__c = 'test'),
			new Case(repair__c = trac_RepairTestUtils.REPAIR_ID_NO_MATCH, SRD_Repair_Status__c = 'test')
		};
		insert cases;
		
		trac_RepairUpdateBatch.run();
	}
	
	private static testMethod void testRunException() {
		trac_RepairTestUtils.createTestEndpoint();
		
		List<Case> cases = new List<Case>{
			new Case(repair__c = trac_RepairTestUtils.REPAIR_ID_EXCEPTION)
		};
		insert cases;
		
		trac_RepairUpdateBatch.run();
	}
	
}