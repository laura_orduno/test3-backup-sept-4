/*
    *******************************************************************************************************************************
    Class Name:     OCOM_NoteAttachmentTest 
    Purpose:        Test class for OCOM_NoteAttachment        
    Created by:     Thomas Su             30-Dec-2016     QC-9301       No previous version
    Modified by:    
    *******************************************************************************************************************************
*/

@isTest
private class OCOM_NoteAttachmentTest {
    @isTest
    // test scenario where 
    private static void noteAttachmentTest1() {
        List<OCOM_NoteAttachment> noteAttachmentList = new List<OCOM_NoteAttachment>();
        OCOM_NoteAttachment aNoteAttachment1 = new OCOM_NoteAttachment();
        aNoteAttachment1.id = UserInfo.getUserId();
        aNoteAttachment1.type = 'Note';
        aNoteAttachment1.typeDisplay = 'Note';
        aNoteAttachment1.title = 'Note 1';
        aNoteAttachment1.createdDate = system.now();
        aNoteAttachment1.lastModifiedDate = system.now().format();
        aNoteAttachment1.createdByName = 'tester';
        aNoteAttachment1.createdBy = UserInfo.getUserId();
        noteAttachmentList.add(aNoteAttachment1);
        OCOM_NoteAttachment aNoteAttachment2 = new OCOM_NoteAttachment();
        aNoteAttachment2.id = UserInfo.getUserId();
        aNoteAttachment2.type = 'Note';
        aNoteAttachment2.typeDisplay = 'Note';
        aNoteAttachment2.title = 'Note 2';
        aNoteAttachment2.createdDate = aNoteAttachment1.createdDate;
        aNoteAttachment2.lastModifiedDate = system.now().format();
        aNoteAttachment2.createdByName = 'tester';
        aNoteAttachment2.createdBy = UserInfo.getUserId();
        noteAttachmentList.add(aNoteAttachment2);
        OCOM_NoteAttachment aNoteAttachment3 = new OCOM_NoteAttachment();
        aNoteAttachment3.id = UserInfo.getUserId();
        aNoteAttachment3.type = 'Note';
        aNoteAttachment3.typeDisplay = 'Note';
        aNoteAttachment3.title = 'Note 2';
        aNoteAttachment3.createdDate = system.now().addMinutes(2);
        aNoteAttachment3.lastModifiedDate = system.now().format();
        aNoteAttachment3.createdByName = 'tester';
        aNoteAttachment3.createdBy = UserInfo.getUserId();
        noteAttachmentList.add(aNoteAttachment3);
        OCOM_NoteAttachment aNoteAttachment4 = new OCOM_NoteAttachment();
        aNoteAttachment4.id = UserInfo.getUserId();
        aNoteAttachment4.type = 'Note';
        aNoteAttachment4.typeDisplay = 'Note';
        aNoteAttachment4.title = 'Note 2';
        aNoteAttachment4.createdDate = system.now().addMinutes(1);
        aNoteAttachment4.lastModifiedDate = system.now().format();
        aNoteAttachment4.createdByName = 'tester';
        aNoteAttachment4.createdBy = UserInfo.getUserId();
        noteAttachmentList.add(aNoteAttachment4);
        noteAttachmentList.sort();
    }

}