public class ProductHistoryUtil {

	/*Mapping of Product History fields to Product fields */
	public static final Map<String, String> PROD_HISTORY_FIELD_MAP = new Map<String, String> {
		'Product__c' => 'Id', 'Active__c' => 'IsActive', 'name' => 'Name',
		'BPC1__c' => 'BPC_1__c', 'BPC2__c' => 'BPC_2__c', 'BPC3__c' => 'BPC_3__c', 
		'BPC_3_Code__c' => 'BPC_3_CODE__c', 'Comp_Inclusion__c' => 'COMP_INCLUSION__c', 
		'Date__c' => 'DATE_Loaded__c', 'EPP1__c' => 'EPP1__c', 'EPP2__c' => 'EPP2__c', 
		'EPP3__c' => 'EPP3__c', 'EPP3_Code__c' => 'EPP3_CODE__c', 'EPP5__c' => 'Product_Family__c', 
		'Family_Code__c' => 'Product_Family_Code__c', 'Margin__c' => 'MARGIN__c', 
		'Portfolio__c' => 'PORTFOLIO__c', 'Portfolio_Code__c' => 'PORTFOLIO_CODE__c', 
		'Product__c' => 'Id', 'Revenue_Type__c' => 'REVENUE_TYPE__c', 
		'RN_1__c' => 'RN_1__c', 'RN_2__c' => 'RN_2__c', 'RN_3__c' => 'RN_3__c', 
		'RN_4__c' => 'RN_4__c', 'Solution_Set_1__c' => 'SOLUTION_SET_1__c', 
		'Solution_Set_2__c' => 'SOLUTION_SET_2__c', 'Solution_Set_3__c' => 'SOLUTION_SET_3__c', 
		'Specialist__c' => 'SPECIALIST__c', 'Target_RN_1__c' => 'TARGET_RN_1__c', 
		'Target_RN_1A__c' => 'TARGET_RN_1A__c', 'Target_RN_2__c' => 'TARGET_RN_2__c', 
		'Target_RN_3__c' => 'TARGET_RN_3__c', 'Target_RN_4__c' => 'TARGET_RN_4__c'
	};

	/*Product fields for history tracking*/
	public static final Set<String> PROD_HISTORY_TRACKING_FIELDS = new Set<String> { 
		'IsActive', 'BPC_1__c', 'BPC_2__c', 'BPC_3__c', 'BPC_3_CODE__c', 
		'COMP_INCLUSION__c', 'DATE_Loaded__c', 'EPP1__c', 'EPP2__c', 
		'EPP3__c', 'EPP3_CODE__c', 'Product_Family__c', 'Product_Family_Code__c', 
		'MARGIN__c', 'PORTFOLIO__c', 'PORTFOLIO_CODE__c', 'REVENUE_TYPE__c', 
		'RN_1__c', 'RN_2__c', 'RN_3__c', 'RN_4__c', 'SOLUTION_SET_1__c', 
		'SOLUTION_SET_2__c', 'SOLUTION_SET_3__c', 'SPECIALIST__c', 'TARGET_RN_1__c', 
		'TARGET_RN_1A__c', 'TARGET_RN_2__c', 'TARGET_RN_3__c', 'TARGET_RN_4__c'
	};

/*
	public static final Map<String, String> PROD_HISTORY_FIELD_MAP = new Map<String, String> {
		'Product__c' => 'Id', 'Active__c' => 'IsActive', 'BPC1__c' => 'BPC_1__c',
		'BPC2__c' => 'BPC_2__c', 'BPC3__c' => 'BPC_3__c', 'Margin__c' => 'Margin__c',
		'name' => 'Name'
	};

	public static final Set<String> PROD_HISTORY_TRACKING_FIELDS = new Set<String> { 
		'BPC_1__c', 'BPC_2__c', 'BPC_3__c', 'Product_Family_Code__c', 'IsActive'
	};
*/

}