@IsTest
public with sharing class RestApi_MyAccountHelper_UtilityTest {

	public static final String DEFAULT_ORIGIN = 'My Account';
	public static final string EMAIL2CASE_PROFILE = [SELECT Id FROM Profile WHERE Name = 'System Integration - token access' LIMIT 1].Id;
	public static final string INTEGRATION_PROFILE = [SELECT Id FROM Profile WHERE Name = 'System Integration - token access' LIMIT 1].Id;
	public static final String UUID = '90123901231';
	public static final String NOTUUID = '923123111';

	private static final String REQUEST_TYPE_GET_CASE_DETAILS = 'get-case-details';
	private static final String CASE_ORIGIN = 'My Account';
	private static final String BILLINGACCOUNTNUMBER = '1337';
	private static final String COMMENT_BODY = 'This is the comment body';
	private static final String CONTACTFIRST = 'Phillip';
	private static final String CONTACTLAST = 'Smith';
	private static final String CONTACTFULL = CONTACTFIRST + ' ' + CONTACTLAST;
	private static final String USERNAME = 'psmith@telus.com' + System.currentTimeMillis();
	private static final String DEFAULTSTATUS = 'In Progress';
	private static final List<String> COLLABSLIST = new List<String>{
			'email@email.com', 'another@another.com'
	};
	private static final String COLLABSSTRING = 'email@email.com;another@another.com';

	public static final String BAN = Schema.SObjectType.Account.getRecordTypeInfosByName().get('BAN').getRecordTypeId();
	public static final String CAN = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CAN').getRecordTypeId();
	private static final String MOCKCASE = '500c000000AW1s8AAD';

	private static final String CHANGEDEMAIL = 'arandomemail@gmail.com';
	private static final String CHANGEDLAST = 'NewLastName';
	private static final String CHANGEDFIRST = 'NewFirstName';

	private static Account portalAcct;
	private static Account myAcct;
	private static User myUser;
	private static Case myCase;
	private static Contact myContact;
	private static Attachment myAttachment;

	private static void setup() {
		trac_TriggerHandlerBase.blockTrigger = true;
		portalAcct = new Account(Name = 'Portal Account Inc.');
		insert portalAcct;

		myAcct = new Account(Name = 'Account Inc.', BAN_CAN__c = RestApi_MyAccountHelper_TestUtils.BILLINGACCOUNTNUMBER, RecordTypeId = CAN);
		insert myAcct;

		myUser = RestApi_MyAccountHelper_TestUtils.createPortalUser(portalAcct);
		insert myUser;

		myCase = RestApi_MyAccountHelper_TestUtils.createCase(myAcct, myUser);
		insert myCase;

		myAttachment = RestApi_MyAccountHelper_TestUtils.createAttachment(myCase);
		insert myAttachment;

		myContact = new Contact(AccountId = myAcct.Id, FirstName = 'fail', LastName = 'fail', Email = 'fail@fail.com');
		insert myContact;

		insert RestApi_MyAccountHelper_TestUtils.createEtoI();
		//TODO: create comments/attachements for myCase
		trac_TriggerHandlerBase.blockTrigger = false;
	}

	@IsTest
	public static void testSearchUserPositive() {
		setup();
		Test.startTest();
		User u = RestApi_MyAccountHelper_Utility.getUser(RestApi_MyAccountHelper_TestUtils.UUID);
		Test.stopTest();

		System.assertEquals(u.FederationIdentifier, RestApi_MyAccountHelper_TestUtils.UUID);
	}

	@IsTest
	public static void testSearchUserNegative() {
		setup();
		Test.startTest();
		User u = RestApi_MyAccountHelper_Utility.getUser(RestApi_MyAccountHelper_TestUtils.UUIDNOT);
		Test.stopTest();

		System.assert(u == null);
	}

	@IsTest
	public static void testSearchUserNegative2() {
		setup();

		Test.startTest();
		User u = RestApi_MyAccountHelper_Utility.getUser(null);
		Test.stopTest();

		System.assert(u == null);
	}

	@IsTest
	public static void testGetContactPositive() {
		setup();
		Test.startTest();
		Contact c = RestApi_MyAccountHelper_Utility.getContact(myContact.Id);
		Test.stopTest();

		System.assertEquals(c.Id, myContact.Id);
	}

	@IsTest
	public static void testGetContactNegative() {
		/*setup();
		Test.startTest();
		Contact c = RestApi_MyAccountHelper_Utility.getContact(myAcct.Id);
		Test.stopTest();

		System.assertEquals(c.Id, myContact.Id);*/
	}

	@IsTest
	public static void testGetCaseDetails() {
		setup();
		Test.startTest();
		Case c = RestApi_MyAccountHelper_Utility.getCaseDetails(myCase.Id);
		Test.stopTest();

		System.assertEquals(c.Id, myCase.id);
	}

	@IsTest
	public static void testGetCaseDetailsForUser() {
		setup();
		myCase.Status = RestApi_MyAccountHelper_TestUtils.INTERNALSTATUS;
		myCase.Related_BCAN_Account__c = myAcct.Id;
		update myCase;

		Test.startTest();
		List<String> accounts = new List<String>{
				RestApi_MyAccountHelper_TestUtils.BILLINGACCOUNTNUMBER
		};
		Case c = RestApi_MyAccountHelper_Utility.getCaseDetailsForUser(myCase.Id, myContact.Id, accounts);
		Test.stopTest();

		//System.assertEquals(c.Id, myCase.id);
	}

	@IsTest
	public static void testGetCasesForUserWithFilters() {
		setup();
		myCase.Status = RestApi_MyAccountHelper_TestUtils.INTERNALSTATUS;
		myCase.Related_BCAN_Account__c = myAcct.Id;
		update myCase;

		List<String> accounts = new List<String>{
				RestApi_MyAccountHelper_TestUtils.BILLINGACCOUNTNUMBER
		};
		List<String> statuses = new List<String>{
				RestApi_MyAccountHelper_TestUtils.INTERNALSTATUS
		};

		Test.startTest();
		List<Case> c = RestApi_MyAccountHelper_Utility.getCasesForUserWithFilters(myContact.Id, accounts, statuses);
		Test.stopTest();

		//System.assertEquals(c[0].id, myCase.id);

	}

	@IsTest
	public static void testGetCasesForUserWithFiltersNegative() {
		setup();
		myCase.Status = 'NO MATCH';
		update myCase;

		List<String> accounts = new List<String>{
				RestApi_MyAccountHelper_TestUtils.BILLINGACCOUNTNUMBER
		};
		List<String> statuses = new List<String>{
				DEFAULTSTATUS
		};

		try {
			Test.startTest();
			List<Case> c = RestApi_MyAccountHelper_Utility.getCasesForUserWithFilters(myContact.Id, accounts, statuses);
			System.assert(c.isEmpty());

			c = RestApi_MyAccountHelper_Utility.getCasesForUserWithFilters(myContact.Id, new List<String>(), statuses);
			System.assert(c.isEmpty());

			c = RestApi_MyAccountHelper_Utility.getCasesForUserWithFilters(myCase.Id, accounts, statuses);
			Test.stopTest();
		} catch (xException e) {
			System.assert(true, 'Exception was thrown as expected');
		}

	}

	@IsTest
	public static void getAttachmentDetails() {
		setup();

		Test.startTest();
		Attachment a = RestApi_MyAccountHelper_Utility.getAttachmentDetails(myAttachment.id);
		Test.stopTest();

		System.assertEquals(a.id, myAttachment.id);
	}

	@IsTest
	public static void getAttachmentDetailsNegative() {
		setup();

		try {
			Test.startTest();
			Attachment a = RestApi_MyAccountHelper_Utility.getAttachmentDetails(null);
			Test.stopTest();
			System.assert(false, 'Expected an exception to be thrown');
		} catch (Exception e) {
			System.assert(true, 'Exception thrown as expected');
		}
	}

	@IsTest
	public static void convertListStatusToInternal() {
		setup();

		Test.startTest();
		List<String> status = RestApi_MyAccountHelper_Utility.convertListStatusToInternal(new List<String>{
				RestApi_MyAccountHelper_TestUtils.EXTERNALSTATUS
		});
		Test.stopTest();

		System.assertEquals(status[0], RestApi_MyAccountHelper_TestUtils.INTERNALSTATUS);
	}

	@IsTest
	public static void convertListStatusToExternal() {
		setup();
		myCase.Status = RestApi_MyAccountHelper_TestUtils.INTERNALSTATUS;
		Test.startTest();
		List<Case> status = RestApi_MyAccountHelper_Utility.convertListStatusToExternal(new List<Case>{
				myCase
		}, new List<String>{
				RestApi_MyAccountHelper_TestUtils.INTERNALSTATUS
		});
		Test.stopTest();

		System.assertEquals(status[0].status, RestApi_MyAccountHelper_TestUtils.EXTERNALSTATUS);
	}

	@IsTest
	public static void translateStatus() {
		setup();
		myCase.Status = RestApi_MyAccountHelper_TestUtils.INTERNALSTATUS;

		Test.startTest();
		String ext = RestApi_MyAccountHelper_Utility.translateStatus(RestApi_MyAccountHelper_TestUtils.INTERNALSTATUS);
		Test.stopTest();

		System.assertEquals(ext, RestApi_MyAccountHelper_TestUtils.EXTERNALSTATUS);
	}

	@IsTest
	public static void translateStatusError() {
		try {
			setup();
			myCase.Status = RestApi_MyAccountHelper_TestUtils.INTERNALSTATUS;

			Test.startTest();
			String ext = RestApi_MyAccountHelper_Utility.translateStatus('FindThis');
			Test.stopTest();
		} catch (Exception e) {
			System.assert(true);
		}
	}

	@IsTest static void caseVisibleToUser() {
		setup();

		Test.startTest();
		Boolean vis = RestApi_MyAccountHelper_Utility.caseVisibleToUser(myCase, myUser);
		Test.stopTest();

		System.assert(!vis);
	}

	@IsTest static void createCollaborators() {
		setup();

		Test.startTest();
		String collabs = RestApi_MyAccountHelper_Utility.createCollaborators(COLLABSLIST);
		Test.stopTest();

		System.assertEquals(COLLABSSTRING, collabs);
	}

	@IsTest static void updateUser() {
		setup();

		Map<String, String> newMap = new Map<String, String>();

		newMap.put('email', CHANGEDEMAIL);
		newMap.put('firstName', CHANGEDFIRST);
		newMap.put('lastName', CHANGEDLAST);

		Test.startTest();
		RestApi_MyAccountHelper_Utility.updateUser(myUser.Id, newMap);
		Test.stopTest();

		User u = [SELECT Id, Email, FirstName, LastName FROM User WHERE Id = :myUser.Id];
		System.assertEquals(CHANGEDEMAIL, u.email);
		System.assertEquals(CHANGEDFIRST, u.firstname);
		System.assertEquals(CHANGEDLAST, u.lastname);

	}

	@IsTest static void updateUserNegative() {
		setup();

		Map<String, String> newMap = new Map<String, String>();

		newMap.put('email', 'NotAnEmailAddress');

		try {
			Test.startTest();
			RestApi_MyAccountHelper_Utility.updateUser(myUser.Id, newMap);
			Test.stopTest();
			System.assert(false, 'Expected an exception to be thrown');
		} catch (xException e) {
			System.assert(true, 'Exception was thrown as expected');
		}
	}

	@IsTest static void getCaseDetailsForUserError() {
		setup();

		Test.startTest();
		Case c = RestApi_MyAccountHelper_Utility.getCaseDetailsForUser(MOCKCASE, myContact.Id, new List<String>{
				'Fail'
		});
		Test.stopTest();

		System.assert(c == null);

	}

	@IsTest static void createCommunityUserTest() {
		setup();
		Map<String, String> mapUser = new Map<String, String>();
		mapUser.put('email', myContact.email);
		mapUser.put('firstName', myContact.firstName);
		mapUser.put('lastName', myContact.lastName);
		mapUser.put('id', myContact.Id);

		Test.startTest();
		RestApi_MyAccountHelper_Utility.createCommunityUser('1021021022102', mapUser);
		Test.stopTest();

		List<User> check = [SELECT Id, Email FROM User WHERE FederationIdentifier = '1021021022102'];
		System.assert(check[0].email == myContact.email);
	}

	@IsTest static void createCommunityUserNegativeTest() {
		setup();
		Map<String, String> mapUser = new Map<String, String>();
		mapUser.put('email', null);

		try {
			Test.startTest();
			RestApi_MyAccountHelper_Utility.createCommunityUser('1021021022102', mapUser);
			Test.stopTest();
			System.assert(false, 'Expected an exception to be thrown');
		} catch (xException e) {
			System.assert(true, 'Exception was thrown as expected');
		}
	}

	/// HELPERS

	public static Attachment createAttachment(Case c) {
		return new Attachment(
				Name = 'Test.txt',
				Body = Blob.valueOf('Unit Test Attachment'),
				ParentId = c.id);
	}
}