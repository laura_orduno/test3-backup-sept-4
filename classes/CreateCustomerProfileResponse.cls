global class CreateCustomerProfileResponse{
    global id webserviceLogId{get;set;}
    global boolean isSuccess{get;set;}
    global string errorMessage{get;set;}
    global id accountId{get;set;}
    global string customerProfileId{get;set;}
    global string rcid{get;set;}
    global string cbucid{get;set;}
}