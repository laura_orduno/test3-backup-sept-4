@isTest(SeeAllData=true)
    private class SMB_Helper_Test {
        
        static testMethod void Test_generateBundleProductHierarchy() {
            Opportunity Opp = smb_test_utility.createOpportunity(Null, Null, Null, false);
            Opp.StageName = 'Contract Created' ;
            insert Opp;
            //smb_test_utility.createPQLI('PQLTestData', opp.Id);
            smb_test_utility.createPQLI('PQL_Test_Data', opp.Id); // 12 Feb 2016 - changed as deployment error 
            
            Map<Id,OpportunityLineItem> mapOLIs = new Map<Id,OpportunityLineItem>([  SELECT ID, Agreement__c, LineNo__c, ParentLineNo__c, Item_ID__c, Parent_Bundle_No__c, Element_Type__c, Printed_Indent_Level__c, Order_Attribute__c, Printed_MRC__c, Printed_MRC_Bundling_Bonus__c, Printed_Document_Display_in_MRC__c, Parent_Product_No__c FROM OpportunityLineItem
                                                                                   WHERE OpportunityId =:opp.Id ]);
            Test.startTest();
            SMB_Helper Helper = new SMB_Helper();
            SMB_Helper.generateBundleProductHierarchy(mapOLIs);
            SMB_Helper.formattedAmount('$-23.01');
            SMB_Helper.doAllProductsHaveWorkOrdersAssociated(Opp.Id);
            Test.stopTest();        
            
        }
        static testMethod void Test_createFOBOOrderAndRelatedOLIs() {
            Opportunity Opp = smb_test_utility.createOpportunity(Null, Null, Null, false);
            Opp.StageName = 'Contract Created' ;
            insert Opp;
            list<FOBOChecklist__c> list_FOBOCheckList= new list<FOBOChecklist__c>(); 
            list_FOBOCheckList.add(new FOBOChecklist__c(Opportunity__c=opp.id,Request_Type__c='Move'));
            list_FOBOCheckList.add(new FOBOChecklist__c(Opportunity__c=opp.id,Request_Type__c='Password Change'));
            list_FOBOCheckList.add(new FOBOChecklist__c(Opportunity__c=opp.id,Request_Type__c='Voice Feature Change'));
            list_FOBOCheckList.add(new FOBOChecklist__c(Opportunity__c=opp.id,Request_Type__c='Internet Change'));
            list_FOBOCheckList.add(new FOBOChecklist__c(Opportunity__c=opp.id,Request_Type__c='C Field Work'));
            list_FOBOCheckList.add(new FOBOChecklist__c(Opportunity__c=opp.id,Request_Type__c='Other Changes'));
            list_FOBOCheckList.add(new FOBOChecklist__c(Opportunity__c=opp.id,Request_Type__c='TBO'));
            
            insert list_FOBOCheckList;
            
            Service_Contact_Role__c serviceContactRole = new Service_Contact_Role__c(opportunity__c=opp.id, type__c='Test',Contact__c = opp.Contract_Signor__c);
            insert serviceContactRole;
            Test.StartTest();
            SMB_Helper.createFOBOOrderAndRelatedOLIs(Opp.Id);
            SMB_Helper.createFOBOOrderAndRelatedOLIs(Opp.Id);
            Test.stopTest();
        }
        static testMethod void Test_TaskCreateORCloseForOpportunity() {
            list<Opportunity> list_Opportunity = new list<Opportunity>();
            Contact contact = smb_test_utility.createContact(Null, Null, true);
            Opportunity Opp1 = smb_test_utility.createOpportunity(Null, contact.AccountId, contact.Id, false);
            Opp1.StageName = 'Contract Created' ;
            Opportunity Opp2 = smb_test_utility.createOpportunity(Null, contact.AccountId, contact.Id, false);
            Opp2.StageName = 'Order Request On Hold (Customer)';
            list_Opportunity.add(Opp1);
            list_Opportunity.add(Opp2);
            insert list_Opportunity;
            smb_test_utility.createServiceRequest(Opp1.Id);
            Opp2.StageName = 'Contract Created' ;
            /*
            * Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
            * START
            */
                Opp2.Stage_update_time__c=System.Now();
            /*
            * Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
            * END
            */
            Opp1.StageName = 'Order Request On Hold (Customer)';
            /*
            * Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
            * START
            */
                Opp1.Stage_update_time__c=System.Now();
            /*
            * Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
            * END
            */
            Test.StartTest();
            update list_Opportunity;
            Opp1.StageName = 'Contract Created' ;
            /*
            * Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
            * START
            */
                Opp1.Stage_update_time__c=System.Now();
            /*
            * Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
            * END
            */
            update list_Opportunity;
            Test.stopTest();
        }
    }