/*
    *******************************************************************************************************************************
    Class Name:     OCOM_OrderBeforeTriggerHelperTest
    Purpose:        Test class to do coverage requirement for OCOM_OrderBeforeTriggerHelper.     
    Created by:     Aditya Jamwal     16-Jan-2017   
    Modified by:    
    *******************************************************************************************************************************
*/
@isTest
private class OCOM_OrderBeforeTriggerHelperTest{
    @isTest
    private static void linkingOrderAndCARProductInfo_Testmethod() {
        String orderId = OrdrTestDataFactory.singleMethodForOrderId();
        Order ordObj=[select id, Credit_Assessment__c,Related_Order__c,accountId FROM Order where id=:orderId];
        system.debug(' Order '+ ordObj);
        Order ordObj1=new Order();
        ordObj1.AccountId=ordObj.accountID;
        ordObj1.ParentId__c = ordObj.id;
        ordObj1.Bill_To_a_New_Account__c=FALSE;
        ordObj1.CurrencyIsoCode='CAD';
        ordObj1.EffectiveDate=Date.today();
        ordObj1.IsReductionOrder=FALSE;
        ordObj1.Name='TestOrderChild';
        ordObj1.Status='Not Submitted';
        ordObj.Type = 'New';
        insert ordObj1;
        Credit_Assessment__c aCAR = new Credit_Assessment__c(CAR_Status__c='Queued');
        insert aCAR;
        ordObj.Credit_Assessment__c =aCAR.Id;   
        system.debug('!! OCOM_OrderBeforeTriggerHelperTest Account method linkingOrderAndCARProdcutInfo '+ordObj.AccountId);
        ordObj.Related_Order__c = getOrderId(ordObj.AccountId);
       //  ordObj.status='Submitted';
       //  ordObj.orderMgmtId_Status__c = OrdrConstants.NC_ORDR_ENTERING_STATUS;
       update ordObj;
        
    }
    
    @isTest
    private static void updateOrderNCStatus_Testmethod() {
        
        //Test.startTest();
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User us = new User(Alias = 'standt', Email='applicationTocp1@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Application TOCP', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='applicationTocp1@testorg.com');
        System.runAs (us) {
        String orderId = OrdrTestDataFactory.singleMethodForOrderId();
        Order ordObj=[select id, Credit_Assessment__c,Related_Order__c,accountId FROM Order where id=:orderId];
        
        ordObj.status='Submitted';
        ordObj.orderMgmtId_Status__c = OrdrConstants.NC_ORDR_ENTERING_STATUS;
        update ordObj;
        ordObj.orderMgmtId_Status__c = OrdrConstants.NC_ORDR_SUBMITTING_STATUS;
        update ordObj;
        ordObj.orderMgmtId_Status__c = OrdrConstants.NC_ORDR_PROCESSING_STATUS;
        update ordObj;
        ordObj.orderMgmtId_Status__c = OrdrConstants.NC_ORDR_PROCESSED_STATUS;
        update ordObj;
        ordObj.orderMgmtId_Status__c = OrdrConstants.NC_ORDR_ERROR_STATUS;
        update ordObj;
        ordObj.orderMgmtId_Status__c = OrdrConstants.NC_ORDR_PONR_STATUS;
        update ordObj;
        ordObj.orderMgmtId_Status__c = OrdrConstants.NC_ORDR_ENTERING_STATUS;
        update ordObj;
        ordObj.status='Activated';
        update ordObj;
        }
        
    }
    
    
    private static String getOrderId(String accountId) {         
        Schema.DescribeSObjectResult d = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
        Schema.RecordTypeInfo rtByName =  rtMapByName.get('CAN');
        system.debug(' id'+rtByName.getRecordTypeId());
        Id recTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Billing').getRecordTypeId();
        Id recSerTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        Id recRCIDTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId();
        
        Account acc = new Account(name='Billing account',BAN_CAN__c='draft',recordtypeid=recTypeId,Billingstreet='street',BillingCity='city',BillingState='state',BillingPostalCode='m123456',BillingCountry='CAN');
        Account acc1 = new Account(name='Billing account',BAN_CAN__c='',recordtypeid=recTypeId,Billingstreet='street',BillingCity='city',BillingState='state',BillingPostalCode='m123456',BillingCountry='CAN');
        Account seracc = new Account(name='123 street',recordtypeid=recSerTypeId,parentid=accountId);         
        
        List<Account> acclist= new List<Account>();          
        acclist.add(acc);
        acclist.add(seracc );
        insert acclist;
        
        smbcare_address__c address= new smbcare_address__c(Is_ILEC__c=false,Connectivity_Type__c='COPPER;GPON;BONDED COPPER',Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio',
                                                           Postal_Code__c='M1t3 N5',FMS_Address_ID__c='987651',account__c=accountId, Service_Account_Id__c= seracc.id); 
        insert address;
        Product2 prod2 = new Product2(Name = 'multiline prod Spec', 
                                      Family = 'Hardware4',orderMgmtId__c='9143976374713722153');
        insert prod2;
        Product2 prod0 = new Product2(Name = 'multiline', 
                                      Family = 'Hardware2',orderMgmtId__c='9143976374713722152',ProductSpecification__c=prod2.id);
        insert prod0;
        // Insert a test product.
        Product2 prod = new Product2(Name = 'overline', 
                                     Family = 'Hardware',OCOM_Bookable__c='Yes',ProductSpecification__c=prod0.id,orderMgmtId__c='9143976374713722151');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        // Create a custom price book
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        // 2. Insert a price book entry with a custom price.
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        contact c= new contact(firstname='abc',lastname='xyz ');
        insert c;
        order ord = new order(Type ='New',Ban__c='draft',accountId=accountId,effectivedate=system.today(),status='not submitted',Pricebook2Id=customPB.id,CustomerAuthorizedById=c.id,service_address__c=address.id);   
        List<Order>ordlist= new List<Order>();   
        ordList.add(ord);
        insert ordList; 
        orderItem  OLI= new orderItem(orderMgmtId__c='54321',amendStatus__c='Change',UnitPrice=12,orderId= ord.Id,vlocity_cmt__BillingAccountId__c=acc1.id,vlocity_cmt__ServiceAccountId__c=seracc.id,Quantity=2,PricebookEntryId=customPrice.id);
        //   insert oli;
        system.debug('!! OCOM_OrderBeforeTriggerHelperTest Account method getOrderId '+ord.AccountId);
        return ord.Id;    
    }
    @isTest
    private static void generateSubOrderNumber(){ 
        Test.startTest();
         String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        Test.stopTest();
    }
     @isTest
    private static void setPriceBookTest(){ 
         String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        Test.startTest();
        List<Order> ordList=[select id,pricebook2id from order];
        OCOM_OrderBeforeTriggerHelper.setPriceBook(true, true,ordList);
        Test.stopTest();
    }
}