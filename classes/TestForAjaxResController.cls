@isTest
private class TestForAjaxResController{
        static testMethod void testMethodForCodeCoverage(){
         //create user -Start
             Profile p = [select id from Profile where name like 'System Admin%' limit 1];  
             User u = new User(alias = 'standt', email='standarduser@testorg.com', emailencodingkey='UTF-8',
                             languagelocalekey='en_US', localesidkey='en_US', timezonesidkey='America/Los_Angeles', 
                             username='u@testorg.com', firstname = 'fname', lastname = 'lname', profileId = p.Id);
             insert u;
         //create user -End
         //create Employee -Start          
             Employee__c e = new Employee__c(Name='Employee', Director_Email__c='d@d.com', CST_Email_Yellow__c=true, Email__c='e@e.com', Manager_Email__c='m@m.com', User__c = u.Id, Employee_ID__c='abcd1234', TMODS_TEAM_MEMBER_ID__c = 98765);
             insert e;
          //create Employee -end
            
            AjaxResController obj=new AjaxResController(); 
            System.currentPagereference().getParameters().put('q',u.id); 
            obj.doUserSearch(); 
            System.currentPagereference().getParameters().put('q',e.id); 
            obj.doEmployeeSearch(); 
            
            obj.doShowPopupView();
            obj.doUpdatePopupView();
            //obj.getEmployeeRecords(); 
            //obj.getUserRecords();
            obj.getResult();
        }
}