@isTest
global class trac_BundleCommitmentExtSvc_1Mock implements WebServiceMock{
	global void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction,
           							 String requestName, String responseNS, String responseName, String responseType) {
      
      svc_BundleCommitmentExtSvcRequestRespons.CreateBundleCommitmentResponse resp =
      	new svc_BundleCommitmentExtSvcRequestRespons.CreateBundleCommitmentResponse();
       
      response.put('response_x', resp);
  	}
}