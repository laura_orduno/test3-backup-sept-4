public class VfSearchController {

	//Page Size
	private Static Final Integer PAGE_NUMBER = 10;

	//Search String used in ArticleList tag
	public String searchstring { get; set; }
	String userAccountId;
	//public string TelusProducts = '';
	public string additionalFunc = '';
	public string CatListProd = '';
	public string CatListPull = '';
	public Boolean morePages { get; set; }

	public string AddFuncPull = '';
	@TestVisible private Integer pageSize;
	public Integer totalPages { get; set; }
	public Integer selectedPage { get; set; }
	public Integer currentPage { get; set; }
	public string[] AddFuncPullT;
	public string AddFuncPull1 { get; set; }
	public string CustRole = '';
	public string CustRole1 { get; set; }
	public PageReference test() {
		return null;
	}
	public String userLanguageUS { get; set; }
	public static String userLanguage {
		
  		get {
			return PortalUtils.getUserLanguage();
		}
		set;
	}
	private ENTP_Customer_Interface_Settings__c cis;
	//Is new List reqd
	private boolean isRefRequired = true;
	//Exclude filter criteria for UI only
	private static final String EXCLUDE_CRITERIA_FILTER = 'All';

	//Keeps track of current page & max size of article list
	// Integer currentPage = 1;

	@TestVisible Integer maxSize = 1;
	//currentPage = 1;

	//Get Account_configuration_Info
	//
	//
	//string additionalFunc;
	// system.debug('userAccountId: ' + userAccountId);
	public string getCustRole() {

		cis = ENTP_Customer_Interface_Settings__c.getInstance();
		if (cis == null) {
			cis = new ENTP_Customer_Interface_Settings__c(
					Manage_Requests_List_Limit__c = 10,
					Overview_Case_Limit__c = 5
			);
		}
		pageSize = Integer.valueOf(cis.Manage_Requests_List_Limit__c); //sets the page size or number of rows
		system.debug('PageSize:' + pageSize);
		List<User> TelusProducts = [SELECT Customer_Portal_Role__C FROM User WHERE id = :UserInfo.getUserId()];
		system.debug('Customer_Portal_Role__C: ' + TelusProducts);

		CustRole = TelusProducts[0].Customer_Portal_Role__C;
		System.debug('CustRole: ' + CustRole);
		if (CustRole != '' && CustRole != null) {
			string[] CustRoleT = CustRole.split(';');
			System.debug('CustRoleT: ' + CustRoleT);
			CustRole1 = CustRoleT[0];

			system.debug('CustRole1: ' + CustRole1);

			List<ENTP_portal_additional_functions__c> ENTPPortalMap = [Select Name,Customer_Portal_Role__c,Additional_Function__c from ENTP_portal_additional_functions__c where Customer_Portal_Role__c = :CustRole];
			CatListPull = ENTPPortalMap[0].Additional_Function__c;

			String[] CatListPullT = CatListPull.split(';');
			AddFuncPull1 = CatListPullT[0];
			system.debug('CatListPullT: ' + CatListPullT);
			//return CatListPullT;
			// TelusProducts = AccountConfig[0].TELUS_Product__c;
			// system.debug('TelusProducts: ' + TelusProducts);
			//  return TelusProducts;
		}
		return CustRole;
	}

	//Returns array of Category Groups
	public DataCategoryGroupInfo[] getDataCategoryGroupInfo() {
		return DataCategoryUtil.getInstance().getAllCategoryGroups();
	}

	//Returns category keyword required to filter articleList.
	public String getCategoryKeyword() {
		DataCategoryGroupInfo[] categoryGroups = DataCategoryUtil.getInstance().getAllCategoryGroups();
		String categoryCondition = '';
		for (DataCategoryGroupInfo categoryGroup : categoryGroups) {
			String selectedCategoryName = System.currentPageReference().getParameters().Get('categoryType_' + categoryGroup.getName());
			if (selectedCategoryName != null && !selectedCategoryName.equals('NoFilter')) {
				if (categoryCondition == '' && selectedCategoryName != null) {
					categoryCondition = categoryCondition + categoryGroup.getName() + ':' + System.currentPageReference().getParameters().Get('categoryType_' + categoryGroup.getName());
					system.debug('categoryCondition' + categoryCondition);
				} else {
					categoryCondition = categoryCondition + ',' + categoryGroup.getName() + ':' + System.currentPageReference().getParameters().Get('categoryType_' + categoryGroup.getName());
					system.debug('categoryCondition' + categoryCondition);
				}

			}
		}

		String categoryFilter = '';
		for (DataCategoryGroupInfo categoryGroup : categoryGroups) {

			String categoryType = System.currentPageReference().getParameters().Get('categoryType_' + categoryGroup.getName());
			system.debug('categoryType' + categoryType);

			if (categoryType != NULL && !categoryType.equals('NoFilter')) {

				if (categoryFilter == '') {
					categoryFilter = categoryGroup.getName() + '__c ABOVE_OR_BELOW ' + categoryType + '__c';
					system.debug('CategoryFilter' + categoryFilter);
				} else {
					categoryFilter = categoryFilter + ' AND ' + categoryGroup.getName() + '__c ABOVE_OR_BELOW ' + categoryType + '__c';
					system.debug('CategoryFilter' + categoryFilter);
				}

			}
		}

		if (currentPage == null || currentPage <= 0) {
			currentPage = 1;
		}
system.debug('userLanguage:' + userLanguage);
		if (userLanguage == 'en'){
	       userLanguageUS = 'en_US';
	       
              }else
              {
              userLanguageUS = userLanguage;	
              }

		system.debug('CategoryFilterLength: ' + categoryFilter.length());
		
		try {
			
			if (categoryFilter.length() > 0) {
				if (searchString != null && searchString.length() > 0) {
					String searchquery = 'FIND \'' + searchString + '*\'IN ALL FIELDS RETURNING KnowledgeArticleVersion(Id, title, UrlName, LastPublishedDate,LastModifiedById, KnowledgeArticleId where PublishStatus =\'online\' and Language = \''+ userLanguageUS +'\') WITH DATA CATEGORY ' + categoryFilter ;
					system.debug('Search String=' + searchquery);
					List<List<SObject>>searchList = search.query(searchquery);
					List<KnowledgeArticleVersion> articleList = (List<KnowledgeArticleVersion>) searchList[0];
					maxSize = articleList.size() ;
					system.debug('Search String=' + searchquery);
					system.debug('ArticleList=' + articleList);
					system.debug('maxSize=' + maxSize);
					Integer counter = maxSize * (currentPage - 1);

					//                    maxSize = maxSize.divide(PAGE_NUMBER,2,System.RoundingMode.UP);
				} else {
					String qryString = 'SELECT Id, title, UrlName, LastPublishedDate,LastModifiedById, KnowledgeArticleId FROM KnowledgeArticleVersion WHERE (PublishStatus = \'online\' and Language = \''+ userLanguageUS +'\') WITH DATA CATEGORY ' + categoryFilter;
					system.debug('Search String=' + qryString);

					List<KnowledgeArticleVersion> articleList = Database.query(qryString);
					maxSize = articleList.size() ;
					system.debug('ArticleList=' + articleList);
					system.debug('maxSize=' + maxSize);
					Integer counter = maxSize * (currentPage - 1);

					//                    maxSize = maxSize.divide(PAGE_NUMBER,2,System.RoundingMode.UP);
				}
			} else {
				String qryString = 'SELECT Id, title, UrlName, LastPublishedDate,LastModifiedById, KnowledgeArticleId FROM KnowledgeArticleVersion WHERE (PublishStatus = \'online\' and Language = \''+ userLanguageUS +'\')';
			system.debug('Search String=' + qryString);
				List<KnowledgeArticleVersion> articleList = Database.query(qryString);
				maxSize = articleList.size() ;
				system.debug('Search String=' + qryString);
				system.debug('ArticleList=' + articleList);
				system.debug('maxSize=' + maxSize);
				Integer counter = maxSize * (currentPage - 1);

				//                maxSize = maxSize.divide(PAGE_NUMBER,2,System.RoundingMode.UP);
			}

		} catch (Exception e) {
			Apexpages.addmessages(e);
		}

		System.debug('CustRole Cetegorykeyword: ' + CustRole1);
		if (categoryFilter == '') {

			//            maxSize = 0;
			//  if(TelusProducts!=''){
			//categoryCondition = 'TELUS_product:ENTP_Portal';
			if (CustRole1 == 'private cloud') {
				categoryCondition = 'Target_Audience:Private_Cloud';

			}
			if (CustRole1 == 'tps') {
				categoryCondition = 'Target_Audience:TPS';

			}
			if (CustRole1 == 'mits') {
				categoryCondition = 'Target_Audience:MITS_Customers';

			}

			system.debug('CategoryCondition: ' + categoryCondition);
		}
		return categoryCondition;

	}

	// Action call when the new list needs to be fetched
	public PageReference refreshSearchResult() {
		maxSize = currentPage = 1;
		return null;
	}
	public PageReference refreshSearchResultPC() {
		maxSize = currentPage = 1;
		return null;
	}
	// Returns whether we need to see previous button or not
	public boolean getPrevRequired() {
		return currentPage > 1;
	}

	// Returns whether we need to see next button or not
	public boolean getNextRequired() {
		return currentPage * PAGE_NUMBER < maxSize;
	}

	//Returns current page number
	public Decimal getCurrentPageNumber() {
		return this.currentPage;
	}

	//action for next click
	public PageReference next() {
		if (maxSize > this.currentPage * PAGE_NUMBER) {
			this.currentPage = this.currentPage + 1;
		}
		return null;
	}

	//action for previous click
	public PageReference previous() {
		if (this.currentPage > 1)
			this.currentPage = this.currentPage - 1;
		return null;
	}

	public List<Integer> pageNumbers {
		get {
			Integer lowerLimit = 1;
			system.debug('Current Page:' + currentPage);
			system.debug('totalPages:' + maxSize);
			if (currentPage >= 4 && maxSize > 4) {
				Integer nearestEvenNumber = currentPage - Math.mod(currentPage, 2);
				lowerLimit = (maxSize - nearestEvenNumber) >= 3 ? nearestEvenNumber - 1 : maxSize - 3;
			}

			Integer upperLimit = (lowerLimit + 3 >= maxSize) ? maxSize : lowerLimit + 3;

			List<Integer> pageNumbers = new List<Integer>();
			for (Integer i = lowerLimit; i <= upperLimit; i++) {
				pageNumbers.add(i);
			}

			if (upperLimit < maxSize) {
				morePages = true;
			} else {
				morePages = false;
			}
			system.debug('pageNumbers??: ' + pageNumbers);
			return pageNumbers;
		}
		set;
	}

	public List<KnowledgeArticleVersion> getKavResults() {
		if (userLanguage == 'en'){
	       userLanguageUS = 'en_US';
	       
              }else
              {
              userLanguageUS = userLanguage;	
              }
		List<KnowledgeArticleVersion> lstSKAV = new List<KnowledgeArticleVersion>();
		try {
			string qryString = 'SELECT Id, title, UrlName, LastPublishedDate,LastModifiedById FROM KnowledgeArticleVersion WHERE (PublishStatus = \'online\' and Language = \''+ userLanguageUS +'\')';
system.debug('KavResults:' + qryString);
			lstSKAV = Database.query(qryString);
			maxSize = lstSKAV.size() ;
			system.debug('LINE 188 - lstSKAV=' + lstSKAV);
			system.debug('LINE 189 - maxSize=' + maxSize);
		} catch (Exception e) {
			Apexpages.addmessages(e);
		}

		return lstSKAV;
	}
	@TestVisible private Integer getTotalPages() {
		Integer totalPages = maxSize / pageSize;
		if (math.mod(maxSize, pageSize) > 0) {
			totalPages += 1;
		}
		return totalPages;
	}

	public String[] getCATLISTPULL() {

		userAccountId = [SELECT AccountId FROM User WHERE Id = :UserInfo.getUserId()].AccountId;

		List<Account_Configuration__c> AccountConfig = [SELECT Id, TELUS_Product__c FROM Account_Configuration__c WHERE Account__c = :userAccountId];
		//system.debug('AccountConfig[0].TELUS_Product__c: ' + AccountConfig[0].TELUS_Product__c);

		CatListPull = AccountConfig[0].TELUS_Product__c;

		String[] CatListPullT = CatListPull.split(';');
		//system.debug('CatListPullT: ' +  CatListPullT);

		return CatListPullT;

	}

	Public void setCATLISTPULL(String catListPull) {
		catListPull = CatListPull;
	}
	public PageReference resetSearch() {
		searchstring = '';
		totalPages = getTotalPages();
		System.debug('totalPages: ' + totalPages);
		selectedPage = 0;
		//resetRequestView();
		return null;
	}
	public String getLanguageLocaleKey() {
		return PortalUtils.getUserLanguage();
	}

}