public class smb_TestUtility {

	public static Account createAccount() {
        Account acc = new Account(Name='Testing Software', BillingCountry = 'US', BillingState = 'IL');
        insert acc;
		return acc;
	}

	
	public static List<Case> createCases(Integer numToInsert, Id accountId, Boolean doInsert ) {
        List<Case> cases = new List<Case>();
        for( Integer i = 0; i < numToInsert; i++ ) {
        	Case theCase = new Case();
        	theCase.Account__c = accountId;
        	theCase.Subject = 'TEST SUBJECT';
        	theCase.Description = 'TEST';
			theCase.Status = 'New';
			theCase.Request_Type__c = 'Other';
			theCase.Priority = 'Medium';
			theCase.Root_Cause__c = 'Unknown';
			cases.add( theCase ); 
        }
        if( doInsert )
        	insert cases;
		return cases;
	}
	
}