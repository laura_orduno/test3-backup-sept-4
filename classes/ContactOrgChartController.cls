/**
* Controller class for the Contact Organization Chart
* Copyright(c) 2013 TELUS 
*/
public with sharing class ContactOrgChartController {

    public Account account {get; set;}
    private String accountId;

    public ESP__c esp {get; set;}
    private String espId;
    
    public Contact contact {get; set;}
    public ESP_User_Contact_Roles__c newContactRole{get; set;}

   
        
    public  void init(){       
        this.contact = new Contact(); 
        newContactRole = new ESP_User_Contact_Roles__c();
    }

  

    /**
    * Default Constructor
    */
    public ContactOrgChartController() {

        espId =  ApexPages.currentPage().getParameters().get('espId');
        
        if (espId !=null){
            List<ESP__c> espList = [Select Id,Name,Account__c From ESP__c Where Id = :espId];
            if (espList !=null && espList.size()>0){
                esp = espList[0];

                List<account> accList = [SELECT Id, Name FROM Account WHERE Id= :esp.Account__c];
                if (accList !=null && accList.size()>0){
                    account = accList[0];
                    accountId = account.Id;
                    
                    //this.contact.AccountId = account.Id;
                }
            }

        }
        //this.contact = (Contact)stdController.getRecord();
        
    }
    
    /**
    * populates a list of Contact object for the given AccountId
    */
    public List<ContactOrgChart.ContactHierarchy>  getAllContactsByAccount(){

        List<ContactOrgChart.ContactHierarchy> contacts;
        if (accountId != null && espId != null) {
           //System.debug('getAllContactsByAccount()');   
           contacts = ContactOrgChart.getAllContacts(accountId,espId);
        }
        return contacts;                        
    }

    public List<ContactOrgChart.ContactHierarchy> getParentContactsWithoutChildren(){
        if (ContactOrgChart.parentsWihoutChildren == null || ContactOrgChart.parentsWihoutChildren.size() ==0){
            return new List<ContactOrgChart.ContactHierarchy>();
        }else{
            return ContactOrgChart.parentsWihoutChildren;
        }
    }

    public Account getAccount() {
        return account;
    }
    /*
    public void updateReportsTo()
    {
        Contact contact;
        String contactReportsToId = ApexPages.currentPage().getParameters().get('contactReportsToId');
        String contactId = ApexPages.currentPage().getParameters().get('contactId');
        if(contactReportsToId =='-999') {
            contactReportsToId = null;
        }
        if(contactId != null ){
            contact = [Select Id, Title, Name, Phone, Email, ReportsToId From Contact where Id=:contactId];
        }
        if(contact != null ){
           contact.ReportsToId = contactReportsToId;
            try {
                update contact;
            } catch (DmlException e) {
                System.debug('The following exception has occurred: ' + e.getMessage());
            } 
        }
    }*/
    
    public void updateReportsTo()
    {
        Contact contact;
        Contact contactReportsto;
        String contactReportsToId = ApexPages.currentPage().getParameters().get('contactReportsToId');
        String contactId = ApexPages.currentPage().getParameters().get('contactId');
        if(contactReportsToId =='-999') {
            contactReportsToId = null;
        }
        if(contactId != null ){
            contact = [Select Id, Title, Name, Phone, Email, AccountId, Account.ParentId,Account.RecordType.Name, ReportsToId From Contact where Id=:contactId];
        }

        if(contactReportsToId != null ){
            contactReportsto = [Select Id, Title, Name, Phone, Email, AccountId, Account.ParentId, Account.RecordType.Name, ReportsToId From Contact where Id=:contactReportsToId];
        }
        
        

        if(contact != null ){
            Id contactParentAccountId =getParentAccountId(contact);
            Id contactReportstoParentAccountId =getParentAccountId(contactReportsto); 
            if(contactParentAccountId == contactReportstoParentAccountId){
                contact.ReportsToId = contactReportsToId;
                updateContact(contact);
            } 
        }
    }

    private void updateContact(Contact c){
        try {
                update c;
        } catch (DmlException e) {
                System.debug('The following exception has occurred: ' + e.getMessage());
        } 
    }
    
    private id getParentAccountId(Contact conct){
        if(conct.Account.RecordType.Name=='CBUCID'){
           return conct.AccountId;
        }else{
           return conct.Account.ParentId;
        }
    }
    
    public PageReference saveContact() {
        if (esp !=null && esp.Account__c !=null){
            //this.contact.AccountId = esp.Account__c;
            boolean saveContact= true;
            if(this.contact.ReportsToId != null){
                Contact reportsToContact = [select id, accountId, Account.ParentId,Account.RecordType.Name from Contact where id =:this.contact.ReportsToId];
                Account contactParentAccount = [select id, ParentId, RecordType.Name from account where id =:this.contact.AccountId];
                id reportsToContactParentAccId = getParentAccountId(reportsToContact);
                id contactParentAccountId=this.contact.AccountId;
                if(contactParentAccount.RecordType.Name !='CBUCID'){
                    contactParentAccountId=contactParentAccount.ParentId;
                }
                System.debug('contactParentAccountId and Type  ' + contactParentAccountId +'  ' +contactParentAccount.RecordType.Name );    
                System.debug('reportsToContactParentAccId ' + reportsToContactParentAccId);
                if(contactParentAccountId != reportsToContactParentAccId){
                        this.contact.ReportsToId.addError('Invalid Report To selection. The selected Contact does not belong to the same Account Hierarchy.');
                    
                    return null;
                }
                                
            }
          
            if (this.contact.Id !=null){
                Update this.contact;
            }else{            
                insert this.contact;  //create the contact
            }
            
            //insert the new contact to ESP_User_Contact_Roles__c
            ESP_User_Contact_Roles__c c = new ESP_User_Contact_Roles__c();
            c.Contact__c = this.contact.Id;
            c.ESP__c = esp.Id;
            c.Background_Comment__c = newContactRole.Background_Comment__c;
            c.Primary_Owner_of_Relationship__c = newContactRole.Primary_Owner_of_Relationship__c;
            insert c;
            
        }
        this.contact = new Contact();
        return null;
    }
    
    public PageReference deleteContact() {
        String contactId = ApexPages.currentPage().getParameters().get('delContactId');
        if(contactId != null ){
           Contact contact = [Select Id,ReportsToId From Contact where Id=:contactId];
           //List<ESP_User_Contact_Roles__c> ucr = [Select Id From ESP_User_Contact_Roles__c Where Contact__c =:contactId];
            if(contact !=null){
                Contact ct = new Contact(Id = contactId);
                ct.ReportsToId = null;
                Update ct;
                /* Based on client request we dont remove the contact from ESP. We just remove the reportstoId
                Contact ct = new Contact(Id = contactId);
                try {
                    //delete ct;  the contact record should be deleted from the ESP User Contact Roles table only (pivotal: s/projects/987508/stories/64840460)
                    if (ucr !=null && ucr.size()>0){
                        delete ucr;
                    }
                }catch (DmlException e) {
                    System.debug('The following exception has occurred: ' + e.getMessage());
                } */

            }
        }
        return null;
    }

    public PageReference updatePoliticalMap(){
        espId =  ApexPages.currentPage().getParameters().get('espId');
        Id contactID = ApexPages.currentPage().getParameters().get('attrContactId');
        String OptionVal = ApexPages.currentPage().getParameters().get('optVal');
        String OptionKey = ApexPages.currentPage().getParameters().get('optKey');
        Boolean isNew = true;

        //first if a contact role with this esp and this contact exists
        List<ESP_User_Contact_Roles__c> espRolesList = [Select Id From ESP_User_Contact_Roles__c Where ESP__c = :espId AND Contact__c = :contactId Limit 1];
        if (espRolesList !=null && espRolesList.size()>0){
            String espRoleContactId = String.valueOf(espRolesList[0].Id);

            //find if there is existing ESP_Contact_Attribute__c with that contact role id
            List<ESP_Contact_Attribute__c> espContactAttrList = [Select Id ,Adaptability_To_Change__c,Coverage__c,Decision_Orientation__c,Your_Status__c,ESP_User_Contact_Role__c
                                                                 From ESP_Contact_Attribute__c
                                                                 Where ESP_User_Contact_Role__c = :espRoleContactId Limit 1];

            ESP_Contact_Attribute__c ca;                                                                 
            if (espContactAttrList !=null && espContactAttrList.size()>0){ //Existing
                ca = espContactAttrList[0];
                isNew = false;
            }
            else{ //Create new 
                ca = new ESP_Contact_Attribute__c();
                ca.ESP_User_Contact_Role__c = espRoleContactId; 
                isNew = true;
            }

        
            if(OptionKey.toUpperCase().trim() =='AC'){
                ca.Adaptability_To_Change__c =  OptionVal;
            } else if(OptionKey.toUpperCase().trim() =='YS'){
                ca.Your_Status__c =  OptionVal;
            }else if(OptionKey.toUpperCase().trim() =='CV'){
                ca.Coverage__c =  OptionVal;
            }else if(OptionKey.toUpperCase().trim() =='DO'){
                ca.Decision_Orientation__c =  OptionVal;
            }

            if (isNew){
                Insert ca;

            }else{
                Update ca;
            }                                                                

        }
 
        return null;
    }

    @RemoteAction
    public static String updateMoreLinkDataContent(String espId,String contactID){
       return ContactOrgChart.updateMoreLinkDataContent(espId,contactID);
    }

    @RemoteAction
    public static void updatePoliticalMapRemote(String espId,String contactID,String OptionKey,String OptionVal){        
         Boolean isNew = true;
        //first if a contact role with this esp and this contact exists
        List<ESP_User_Contact_Roles__c> espRolesList = [Select Id From ESP_User_Contact_Roles__c Where ESP__c = :espId AND Contact__c = :contactId Limit 1];
        //System.debug('espRolesList ' + espRolesList );
        if (espRolesList !=null && espRolesList.size()>0){
            String espRoleContactId = String.valueOf(espRolesList[0].Id);

            //find if there is existing ESP_Contact_Attribute__c with that contact role id
            List<ESP_Contact_Attribute__c> espContactAttrList = [Select Id ,Adaptability_To_Change__c,Coverage__c,Decision_Orientation__c,Your_Status__c,ESP_User_Contact_Role__c
                                                                 From ESP_Contact_Attribute__c
                                                                 Where ESP_User_Contact_Role__c = :espRoleContactId Limit 1];

            //System.debug('espContactAttrList ' + espContactAttrList);
            ESP_Contact_Attribute__c ca;                                                                 
            if (espContactAttrList !=null && espContactAttrList.size()>0){ //Existing
                ca = espContactAttrList[0];
                isNew = false;
            }
            else{ //Create new 
                ca = new ESP_Contact_Attribute__c();
                ca.ESP_User_Contact_Role__c = espRoleContactId; 
                isNew = true;
            }

        
            if(OptionKey.toUpperCase().trim() =='AC'){
                ca.Adaptability_To_Change__c =  OptionVal;
            } else if(OptionKey.toUpperCase().trim() =='YS'){
                ca.Your_Status__c =  OptionVal;
            }else if(OptionKey.toUpperCase().trim() =='CV'){
                ca.Coverage__c =  OptionVal;
            }else if(OptionKey.toUpperCase().trim() =='DO'){
                ca.Decision_Orientation__c =  OptionVal;
            }

            if (isNew){
                Insert ca;

            }else{
                Update ca;
            }                                                                

        }

    }

    public PageReference addContactToRole(){
        espId =  ApexPages.currentPage().getParameters().get('espId');
        Id contactID = ApexPages.currentPage().getParameters().get('addContactId');
        if (espId !=null && contactID !=null){
            ESP_User_Contact_Roles__c e = new ESP_User_Contact_Roles__c();
            List<ESP_User_Contact_Roles__c> ecrList = [SELECT Id FROM ESP_User_Contact_Roles__c Where ESP__c =:espId AND Contact__c =:contactID];           
            if (ecrList !=null && ecrList.size()>0){
                Set<Id> roleIds = new Set<Id>();
                for(ESP_User_Contact_Roles__c temp:ecrList){
                    roleIds.add(temp.Id);
                }                
                List<ESP_Contact_Attribute__c> eaList = new List<ESP_Contact_Attribute__c>();
                eaList = [Select Id from ESP_Contact_Attribute__c Where ESP_User_Contact_Role__c in:roleIds];
                if (eaList.size()>0){
                    Delete eaList;
                }

                Delete ecrList; //to prevent potential duplicates in ESP_User_Contact_Roles__c
            }            
            e.ESP__c = espId;
            e.Contact__c = contactID;
            Insert e;
        }
        return null;
    }

    public PageReference cancelOrgMap(){
        espId =  ApexPages.currentPage().getParameters().get('espId');
        String url = '/' + espId;
        PageReference pageRef = new PageReference(url);
        pageRef.setRedirect(true);
        return pageRef;
    }

     public List<SelectOption> getAccountList(){
        List<SelectOption> accountList = new List<SelectOption>();
        List<Account> accounts = [select id, Name, RecordType.Name,RCID__c from Account where ((parentid =:this.esp.Account__c or id= :this.esp.Account__c)
        and RecordType.Name IN ('CBUCID','RCID'))];
        for(Account item : accounts){
            String lable = item.Name +'-'+ item.RecordType.Name;
            if(item.RCID__c != null && item.RCID__c!=''){
                lable = lable+ '-' + item.RCID__c;
            }
            accountList.add(new SelectOption(item.id,lable));
        }
        return accountList;
    }
}