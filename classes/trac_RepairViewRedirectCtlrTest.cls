/*
 * Tests for trac_RepairViewRedirectCtlr
 * Part of the SMB Repairs project
 *
 * @author: Grant Adamson, Traction On Demand 
 */

@isTest
private class trac_RepairViewRedirectCtlrTest {
// Tests
	
	private static testMethod void testRedirect() {
		Case c = new Case(repair__c = '12345');
		insert c;
		
		trac_PageUtils.setParam('id', c.id);
		
		trac_RepairViewRedirectCtlr ctlr = new trac_RepairViewRedirectCtlr();
		
		PageReference ref = ctlr.redirect();
		
		system.assertEquals(
			Label.VIEW_IN_SRD_LINK + '&languageCode=' + trac_RepairViewRedirectCtlr.getLangCode() + '&repairId=' + c.repair__c,
			ref.getUrl()
		);
	}
	
	
	private static testMethod void testGetLangCode() {
		Id PROFILEID = [
			SELECT id
			FROM Profile
			WHERE name='Standard User'
				 OR name='Utilisateur standard'
			LIMIT 1
		].id;
		
		User u = new User(
			username = 'LANGTEST@TRACTIONSM.COM',
			email = 'test@example.com',
		  title = 'test',
		  lastname = 'test',
		  alias = 'test',
		  TimezoneSIDKey = 'America/Los_Angeles',
		  LocaleSIDKey = 'en_US',
		  EmailEncodingKey = 'UTF-8',
		  ProfileId = PROFILEID,
		  LanguageLocaleKey = 'en_US'
		);
		insert u;
		
		system.runAs(u) {
			system.assertEquals('EN', trac_RepairViewRedirectCtlr.getLangCode());
		}
		
		u.LocaleSIDKey = 'fr_CA';
		u.LanguageLocaleKey = 'fr';
		update u;
		
		system.runAs(u) {
			system.assertEquals('FR', trac_RepairViewRedirectCtlr.getLangCode());
		}
		
	}
}