//Generated by wsdl2apex

public class svc_EnterpriseCommonTypes_v2 {
    public class MultilingualCodeDescTextList {
        public svc_EnterpriseCommonTypes_v2.CodeDescText[] codeDescText;
        private String[] codeDescText_type_info = new String[]{'codeDescText','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v2','CodeDescText','1','10','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v2','true','false'};
        private String[] field_order_type_info = new String[]{'codeDescText'};
    }
    public class Name {
        public String locale;
        public String name;
        private String[] locale_type_info = new String[]{'locale','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v2','localeType','1','1','false'};
        private String[] name_type_info = new String[]{'name','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v2','true','false'};
        private String[] field_order_type_info = new String[]{'locale','name'};
    }
    public class MultilingualNameList {
        public svc_EnterpriseCommonTypes_v2.Name[] name;
        private String[] name_type_info = new String[]{'name','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v2','Name','1','10','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v2','true','false'};
        private String[] field_order_type_info = new String[]{'name'};
    }
    public class Description {
        public String locale;
        public String descriptionText;
        private String[] locale_type_info = new String[]{'locale','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v2','localeType','1','1','false'};
        private String[] descriptionText_type_info = new String[]{'descriptionText','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v2','true','false'};
        private String[] field_order_type_info = new String[]{'locale','descriptionText'};
    }
    public class MultilingualDescriptiontList {
        public svc_EnterpriseCommonTypes_v2.Description[] description;
        private String[] description_type_info = new String[]{'description','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v2','Description','1','10','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v2','true','false'};
        private String[] field_order_type_info = new String[]{'description'};
    }
    public class Message {
        public String locale;
        public String errorMessage;
        private String[] locale_type_info = new String[]{'locale','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] errorMessage_type_info = new String[]{'errorMessage','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v2','true','false'};
        private String[] field_order_type_info = new String[]{'locale','errorMessage'};
    }
    public class CodeDescText {
        public String locale;
        public String codeDescText;
        private String[] locale_type_info = new String[]{'locale','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v2','localeType','1','1','false'};
        private String[] codeDescText_type_info = new String[]{'codeDescText','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v2','codeDescTextType','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v2','true','false'};
        private String[] field_order_type_info = new String[]{'locale','codeDescText'};
    }
    public class MessageType {
        public svc_EnterpriseCommonTypes_v2.MultilingualCodeDescTextList message;
        private String[] message_type_info = new String[]{'message','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v2','MultilingualCodeDescTextList','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v2','true','false'};
        private String[] field_order_type_info = new String[]{'message'};
    }
    public class ResponseMessage {
        public DateTime dateTimeStamp;
        public String errorCode;
        public String transactionId;
        public svc_EnterpriseCommonTypes_v2.Message[] errorMessageList;
        public String contextData;
        private String[] dateTimeStamp_type_info = new String[]{'dateTimeStamp','http://www.w3.org/2001/XMLSchema','dateTime','1','1','false'};
        private String[] errorCode_type_info = new String[]{'errorCode','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] transactionId_type_info = new String[]{'transactionId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] errorMessageList_type_info = new String[]{'errorMessageList','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v2','Message','0','10','false'};
        private String[] contextData_type_info = new String[]{'contextData','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v2','true','false'};
        private String[] field_order_type_info = new String[]{'dateTimeStamp','errorCode','transactionId','errorMessageList','contextData'};
    }
    
  private static testMethod void testSvc_EnterpriseCommonTypes_v2() {
  	svc_EnterpriseCommonTypes_v2.CodeDescText ect2cdt =
			new svc_EnterpriseCommonTypes_v2.CodeDescText();
		svc_EnterpriseCommonTypes_v2.Description ect2d =
			new svc_EnterpriseCommonTypes_v2.Description();
		svc_EnterpriseCommonTypes_v2.Message ect2m =
			new svc_EnterpriseCommonTypes_v2.Message();
		svc_EnterpriseCommonTypes_v2.MessageType ect2mt =
			new svc_EnterpriseCommonTypes_v2.MessageType();
		svc_EnterpriseCommonTypes_v2.MultilingualCodeDescTextList ect2mcdtl =
			new svc_EnterpriseCommonTypes_v2.MultilingualCodeDescTextList();
		svc_EnterpriseCommonTypes_v2.MultilingualDescriptiontList ect2mdl =
			new svc_EnterpriseCommonTypes_v2.MultilingualDescriptiontList();
		svc_EnterpriseCommonTypes_v2.MultilingualNameList ect2mnl =
			new svc_EnterpriseCommonTypes_v2.MultilingualNameList();
		svc_EnterpriseCommonTypes_v2.Name ect2n =
			new svc_EnterpriseCommonTypes_v2.Name();
		svc_EnterpriseCommonTypes_v2.ResponseMessage ect2rm =
			new svc_EnterpriseCommonTypes_v2.ResponseMessage();
  }
}