//Generated by wsdl2apex

public class smb_orders_address_types_1_2 {
    public class usZipCodeStateType {
        public String zipCode;
        public String state;
        private String[] zipCode_type_info = new String[]{'zipCode','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/telus_address_types_1_2.xsd',null,'1','1','false'};
        private String[] state_type_info = new String[]{'state','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/telus_address_types_1_2.xsd',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/telus_address_types_1_2.xsd','true','false'};
        private String[] field_order_type_info = new String[]{'zipCode','state'};
    }
    public class addressType {
        public String[] addressLine;
        public String city;
        public String postalCd;
        public String province;
        private String[] addressLine_type_info = new String[]{'addressLine','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/telus_address_types_1_2.xsd',null,'1','2','false'};
        private String[] city_type_info = new String[]{'city','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/telus_address_types_1_2.xsd',null,'1','1','false'};
        private String[] postalCd_type_info = new String[]{'postalCd','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/telus_address_types_1_2.xsd',null,'1','1','false'};
        private String[] province_type_info = new String[]{'province','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/telus_address_types_1_2.xsd',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/telus_address_types_1_2.xsd','true','false'};
        private String[] field_order_type_info = new String[]{'addressLine','city','postalCd','province'};
    }
    public class municipalAddressType {
        public String apartmentNumber;
        public String houseNumber;
        public String houseNumberSuffix;
        public String streetVector;
        public String streetName;
        public String streetSuffix;
        public String city;
        public String postalCd;
        public String province;
        private String[] apartmentNumber_type_info = new String[]{'apartmentNumber','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/telus_address_types_1_2.xsd',null,'0','1','false'};
        private String[] houseNumber_type_info = new String[]{'houseNumber','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/telus_address_types_1_2.xsd',null,'1','1','false'};
        private String[] houseNumberSuffix_type_info = new String[]{'houseNumberSuffix','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/telus_address_types_1_2.xsd',null,'0','1','false'};
        private String[] streetVector_type_info = new String[]{'streetVector','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/telus_address_types_1_2.xsd',null,'0','1','false'};
        private String[] streetName_type_info = new String[]{'streetName','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/telus_address_types_1_2.xsd',null,'1','1','false'};
        private String[] streetSuffix_type_info = new String[]{'streetSuffix','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/telus_address_types_1_2.xsd',null,'0','1','false'};
        private String[] city_type_info = new String[]{'city','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/telus_address_types_1_2.xsd',null,'1','1','false'};
        private String[] postalCd_type_info = new String[]{'postalCd','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/telus_address_types_1_2.xsd',null,'1','1','false'};
        private String[] province_type_info = new String[]{'province','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/telus_address_types_1_2.xsd',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/telus_address_types_1_2.xsd','true','false'};
        private String[] field_order_type_info = new String[]{'apartmentNumber','houseNumber','houseNumberSuffix','streetVector','streetName','streetSuffix','city','postalCd','province'};
    }
    public class cdnPostalCodeProvinceType {
        public String postalCode;
        public String province;
        private String[] postalCode_type_info = new String[]{'postalCode','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/telus_address_types_1_2.xsd',null,'1','1','false'};
        private String[] province_type_info = new String[]{'province','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/telus_address_types_1_2.xsd',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/telus_address_types_1_2.xsd','true','false'};
        private String[] field_order_type_info = new String[]{'postalCode','province'};
    }
}