@isTest
public class GetConditionalSections_Test {

    static testMethod void conditionSectionsTest() {
		
        TestDataHelper.testAccountCreation();
		TestDataHelper.testContractCreationwithDealSupport();
		TestDataHelper.testvlocityDocTemplateteCreation();
		TestDataHelper.testProductCreation_CP('Voice Plan');
        
		vlocity_cmt__DocumentTemplateSection__c objDocTemplateSection=new vlocity_cmt__DocumentTemplateSection__c(vlocity_cmt__DocumentTemplateId__c=TestDataHelper.testvlocityDocTemplateObj.Id,name='test');
		insert objDocTemplateSection;
		
		Contract_Section_Filter__c objContactMapping=new Contract_Section_Filter__c(name = 'Test Section', Field_API_Name__c='Offer_House_Demand__r.Loyalty_Credit__c',Filter_Value__c='12',Filter_Operator__c='=',Section_Name__c='test');
		insert objContactMapping;
		
		
		Id ContractRecordTypeId = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Corporate Wireless').getRecordTypeId();
		
		
		contract objcontract=new contract(Offer_House_Demand__c=TestDataHelper.testDealSupport.id,name='Test',Services__c='Voice',recordTypeID=ContractRecordTypeId,Hardware_Refresh_Type__c='Test',Contract_Type__c='Amendment', AccountId= TestDataHelper.testAccountObj.id); //Fulfillment_Type__c='Test'
		insert objcontract;
		
		vlocity_cmt__ContractLineItem__c  objCLI=new vlocity_cmt__ContractLineItem__c (vlocity_cmt__ContractId__c  =objcontract.id,name='test',Renewal_Credit__c='12',Hardware_Discounts__c='12' , vlocity_cmt__Product2Id__c  = TestDataHelper.testProductObj2.id);
		insert objCLI;
		
		Contract_Template_Section_Mapping__c objContractTemplateSectionMapping = new Contract_Template_Section_Mapping__c(name='Voice',Template_Section_Name__c='test');
		insert objContractTemplateSectionMapping;
		
		Map<String,Object> inputMap=new Map<String,Object>();
		inputMap.put('allSections',new Map<Id, vlocity_cmt__DocumentTemplateSection__c>{objContractTemplateSectionMapping.id=>objDocTemplateSection});
		inputMap.put('contextId',objcontract.id);
		
		Test.startTest();
		GetConditionalSections objGetConditionalSections=new GetConditionalSections();
		objGetConditionalSections.invokeMethod('getConditionalTemplateSections',inputMap,new Map<String,Object>(),new Map<String,Object>());
		Test.stopTest();
	}


}