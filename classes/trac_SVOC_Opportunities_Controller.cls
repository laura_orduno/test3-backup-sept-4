global with sharing class trac_SVOC_Opportunities_Controller {

    public Account acct {get; set;}
    public transient List<Opportunity> matchedOpenOpportunity {get; set;}
    public transient List<Opportunity> matchedClosedOpportunity {get; set;}
    public transient List<Service_Request__c> matchedServReq {get;set;}    
    public String RCID {get;set;}   

    public transient list<smb_orders_trackingtypes_v4.OrderRequest> lstOrders {get;set;}
    public transient list<SMB_WebserviceHelper.orderWrapper> orderList {get;set;}

    global transient List<Id> AllAccountIdsInHierarchy {get;set;}
    
    public String pageInstanceId  {
        get {
            if (pageInstanceId == null) {
                pageInstanceId = smb_GuidUtil.NewGuid();
            }
            return pageInstanceId;
        }
        private set;
    }
        
    private static Account getAccount(Id accountId) {
        if (accountId == null) return null;
    
        return [SELECT  Id, ParentId, Name, RCID__c, RecordTypeId, RecordType.DeveloperName, Account_Status__c
            FROM Account 
            WHERE Id =: accountId];
    }

    public Boolean AllowCreationOfRelatedRecords {
        get {
            if (acct == null) return false;
            
            if (acct.RecordType == null) return false;
            
            if (acct.RecordType.DeveloperName == 'RCID' || acct.RecordType.DeveloperName == 'New_Prospect_Account') {
                if(acct.Account_Status__c == 'Obsolete' || acct.Account_Status__c == 'Terminated'){
                    return false;
                }else{
                    return true;
                }
            }            
            return false;
        }
    }
              
    public PageReference init() 
    {           
        Id accountId = ApexPages.currentPage().getParameters().get('id');
        
        acct = getAccount(accountId);
        
        allAccountIdsInHierarchy = smb_AccountUtility.getAccountsInHierarchy(accountId, acct.ParentId);               

        matchedOpenOpportunity = [select Id, Name, Owner.Name, Total_Contract_Value__c, Total_Contract_Renewal__c, StageName, CloseDate, CreatedDate, Order_ID__c
            from Opportunity 
            where IsClosed=false AND accountid =:accountId AND 
            (recordtype.name!='SMB Care Opportunity' AND recordtype.name!='SMB Care Amend Order Opportunity' AND 
                recordtype.name != 'SMB Care Locked Order Opportunity' AND recordtype.name !='SRS Order Request') 
            ORDER BY CloseDate DESC limit 1000];
        matchedClosedOpportunity = [select Id, Name, Owner.Name, Total_Contract_Value__c, Total_Contract_Renewal__c, StageName, CloseDate, CreatedDate, Order_ID__c
            from Opportunity 
            where IsClosed=true AND accountid =:accountId AND 
            (recordtype.name!='SMB Care Opportunity' AND recordtype.name!='SMB Care Amend Order Opportunity' AND 
                recordtype.name != 'SMB Care Locked Order Opportunity' AND recordtype.name !='SRS Order Request')
            ORDER BY CloseDate DESC limit 1000];

            
        return null;
    }
 
    public String baseUrl {
        get {
            // Changes as per in Mydomain imlplementation
        	//return 'https://' + URL.getSalesforceBaseUrl().getHost().substring(2,6) + '.salesforce.com';
        	String inst = URLMethodUtility.getInstanceName();
        	return 'https://' + inst + '.salesforce.com';
        }
    }
}