global class RestApi_MyAccountHelper_UpdateCase {

	public RestApi_MyAccountHelper_UpdateCase() {

	}

	public class RequestUpdateCase {
		public String id = '';
		public Boolean sendNotification = false;    //  * notifications flag (if False, system will not send notification to originator)
		public List<String> collaborators = null;               //  * List of Collaborators (max 3) (data types TBD)
		public Boolean cancelCase = false;
	}

	public class UpdateCaseRequestObject extends RestApi_MyAccountHelper.RequestObject {
		public String uuid = '';
		public RequestUpdateCase caseToUpdate;
		public List<String> billing;
	}

	global class UpdateCaseResponse extends RestApi_MyAccountHelper.Response {
		public String userName = '';
		public Case caseDetails = null;

		public UpdateCaseResponse(UpdateCaseRequestObject request) {
			super(request);
			// CHECK REQUEST FOR COMPLETENESS
			// check if uuid specified
			if (String.isBlank(request.uuid)) {
				throw new xException('Field "uuid" in request not defined');
			}

			// check if case information was submitted
			if (request.caseToUpdate == null) {
				throw new xException('No case information was submitted for update');
			}

			if (request.billing == null) {
				throw new xException('No Account IDs sent in the request');
			}

			// CONFIRM THAT DATA LIVES WITHIN SALESFORCE
			// get user by uuid ( refers to the field "federationIdentifier" in the Salesforce User object)
			User u = RestApi_MyAccountHelper_Utility.getUser(request.uuid);

			// if user not found
			if (u == null) {
				throw new xException('User not found for uuid: ' + request.uuid);
			}

			if (!u.Contact.MASR_Enabled__c) {
				throw new xException('User is not MASR Enabled.');
			}

			Case secureCase = RestApi_MyAccountHelper_Utility.getCaseDetailsForUser(request.caseToUpdate.id, u.contactId, request.billing);

			if (secureCase == null)
				throw new xException('Not authorized to update the Case');

			username = u.name;

			Case caseToUpdate = new Case();
			caseToUpdate.Id = request.caseToUpdate.id;

			Boolean atLeastOneFieldSupplied = false;

			if (request.caseToUpdate.sendNotification != null) {
				atLeastOneFieldSupplied = true;
				caseToUpdate.NotifyCustomer__c = request.caseToUpdate.sendNotification;
			}
			if (request.caseToUpdate.collaborators != null) {
				atLeastOneFieldSupplied = true;
				caseToUpdate.NotifyCollaboratorString__c = RestApi_MyAccountHelper_Utility.createCollaborators(request.caseToUpdate.collaborators); // This will set caseDetails.Contributors to what we need.
			}
			// cancel case update status
			if (request.caseToUpdate.cancelCase != null) {
				atLeastOneFieldSupplied = true;
				if (request.caseToUpdate.cancelCase == true)
					caseToUpdate.Status = 'Cancelled';
				if (secureCase.Owner.type == 'Queue')
					caseToUpdate.OwnerId = secureCase.CreatedById;
			}

			if (!atLeastOneFieldSupplied)
				throw new xException('No case information was submitted for update');

			try {
				update caseToUpdate;
			} catch (DMLException e) {
				throw new xException('Case Update: ' + e.getMessage());
			}

			caseDetails = RestApi_MyAccountHelper_Utility.getCaseDetails(caseToUpdate.Id);
			caseDetails.Status = RestApi_MyAccountHelper_Utility.translateStatus(caseDetails.Status);

		}
	}

	// object to be returned as JSON response
	public UpdateCaseResponse response = null;

	public RestApi_MyAccountHelper_UpdateCase(String requestString) {
		// convert request to object
		UpdateCaseRequestObject updateCaseRequest = (UpdateCaseRequestObject) JSON.deserialize(requestString, UpdateCaseRequestObject.class);
		response = new UpdateCaseResponse(updateCaseRequest);
	}

}