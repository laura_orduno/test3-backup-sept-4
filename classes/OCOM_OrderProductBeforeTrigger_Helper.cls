public class OCOM_OrderProductBeforeTrigger_Helper{
    static public Boolean isNotApplicable=false; 
    public static Boolean running=false;
    set<string> serviceAddressId= new set<string>();
    Map<string,string>serviceAddressIDToAcoountMap= new Map<string,string>();
    Map<string,string> OrderIdToServiceAddressMap = new Map<string,string>(); // Added on 14-14-2016 for multisite
    set<Id> serviceAddressIdforMultisite= new set<Id>(); // Added on 14-14-2016 for multisite
    Map<string,order> mapOrder = new Map<string,order>();// Added on 14-14-2016 for multisite
    List<OrderItem> ListOrderItem= new List<OrderItem>();
    Map<string,string>orderIdToBanMap= new Map<string,string>();
    Map<string,Account>banToAcoountIdMap= new Map<string,Account>();    
    Map<string,Account>AccountIDToAcoountIdMap= new Map<string,Account>();
    
    public static Boolean isRunning(){
        if(!running){
            running=true;
            return running;
        } else {
            running=false;
        }
        return running;
    }
    //   set<string> BANs = new Set<string>();
    //This method will add service A/c and Billing A/c to OrderItem , when new product is added to Order.
    public void setServiceAccountAndBillingToOrderItem(List<OrderItem> OrdItems,Set<string>OrderIds,Set<string>OIBillingAccIDs){
    Id OrderObjectId ;
        system.debug('inside inhelper out');
        if(!isNotApplicable){
            system.debug('inside inhelper');
            for(order ord:[select id,Service_Address_Full_Name__c,service_address__r.Account__c,service_address__r.service_Account_Id__c,service_address__c,BAN__c 
                                      from order where id =:OrderIds])
            {
                serviceAddressId.add(ord.service_address__c);
                serviceAddressIDToAcoountMap.put(ord.id,ord.service_address__r.service_Account_Id__c);
                OrderIdToServiceAddressMap.put(ord.id,ord.service_address__c);
                mapOrder.put(ord.id,ord);
                /*    if(String.isnotblank(ord.BAN__c)){
BANs.add(ord.BAN__c);
} 
*/   
                //  orderIdToBanMap.put(ord.Id,ord.BAN__c);
                //  system.debug('inside aaa '+ord.service_address__r.service_Account_Id__c);
            }
            /*            system.debug('inside Outside Account List '+OIBillingAccIDs);
for(Account billingAccount:[select id,BAN_CAN__c,name,Billingstreet, BillingCity,recordtype.name, BillingState, BillingPostalCode, BillingCountry from Account where (Id=:OIBillingAccIDs)and recordtype.Name In('Billing','BAN','CAN')]){
//BAN_CAN__c=:BANs OR name=:BANs OR
if(string.isnotblank(billingAccount.BAN_CAN__c)){
banToAcoountIdMap.put(billingAccount.BAN_CAN__c,billingAccount);
}
else{
banToAcoountIdMap.put(billingAccount.name,billingAccount);
}
AccountIDToAcoountIdMap.put(billingAccount.id,billingAccount);
}           
*/          
            
            for(orderItem orI:OrdItems)
            {
                //if(String.isBlank(orI.vlocity_cmt__ServiceAccountId__c)){
                
                if(Trigger.isInsert && Trigger.isBefore){// if condition is added on 14-oct-2014 for multisite implementation By Prashant Tiwari
                    // Adharsh:: Setting Manual discounts to zero for Lean pricing
                    ori.vlocity_cmt__RecurringManualDiscount__c  = 0.00;
                    ori.vlocity_cmt__OneTimeManualDiscount__c = 0.00;
                    orI.vlocity_cmt__ServiceAccountId__c=serviceAddressIDToAcoountMap.get(orI.orderId);
                    orI.Service_Address__c = OrderIdToServiceAddressMap.get(orI.orderId);//added on 14-oct-2016 for multisite implementation By Prashant Tiwari
                    if(mapOrder.get(orI.orderId) != null && mapOrder.containsKey(orI.orderId)) {
                        orI.Shipping_Address__c =  mapOrder.get(orI.orderId).Service_Address_Full_Name__c;        
                    }
                    serviceAddressIdforMultisite.add(orI.Service_Address__c);
                    ListOrderItem.add(orI);
                    OrderObjectId = orI.orderId;
                }
                if(Trigger.isInsert){
                    orI.vlocity_cmt__BillingAccountId__c= null; // Added to set billing account to Null asked by Shuji for defect#6722 fix
                }
                /*            if(null!=banToAcoountIdMap && null!=orderIdToBanMap && banToAcoountIdMap.size()>0  && orderIdToBanMap.size()>0 && orI.vlocity_cmt__BillingAccountId__c==null){
Account acc=banToAcoountIdMap.get(orderIdToBanMap.get(orI.orderId));
if(null!=acc){
setUpBillingAccount(acc,orI);                        
}    
system.debug('Inside If ');
} 
else if(null!=banToAcoountIdMap && banToAcoountIdMap.size()>0 && orI.vlocity_cmt__BillingAccountId__c!=null){
String BillAddr='';

system.debug('inside Account Map '+banToAcoountIdMap);
system.debug('inside Account Map 2 '+AccountIDToAcoountIdMap);
system.debug('Inside Else account 122 '+orI.vlocity_cmt__BillingAccountId__c);  

if(null!=AccountIDToAcoountIdMap && AccountIDToAcoountIdMap.containsKey(orI.vlocity_cmt__BillingAccountId__c)){

Account ExistingBillingacc=AccountIDToAcoountIdMap.get(orI.vlocity_cmt__BillingAccountId__c);

Account Newacc=AccountIDToAcoountIdMap.get(orI.vlocity_cmt__BillingAccountId__c);
if(null!=Newacc){
setUpBillingAccount(Newacc,orI);
system.debug('Inside Else account 123'+Newacc);                 
}
}else if(String.isBlank(orI.Billing_Address__c) || String.isblank(orI.Billing_Account__c)){
Account Newacc=banToAcoountIdMap.get(orderIdToBanMap.get(orI.orderId));
system.debug('Inside Else '+orI.vlocity_cmt__BillingAccountId__c);                                
if(null!=Newacc){

setUpBillingAccount(Newacc,orI);  
system.debug('Inside Else account '+Newacc);
}  
system.debug('Inside Else OrderItem '+oRI);                   
}

} */
            }
        }

        if(Trigger.isUpdate && Trigger.isBefore){
            Set<Id> disconnectIdSet = new Set<Id>();
            Map<Id,Id> oliToSrvcAddrIdMap = new Map<Id,Id>();
            for(orderItem orI:OrdItems)
            {
                //system.debug(' vlocity_cmt.FlowStaticMap.FlowMap.put' + vlocity_cmt.FlowStaticMap.FlowMap);
                //Map<String,OCOM_VlocityJSONParseUtil.JSONWrapper>  attributeMap = OCOM_VlocityJSONParseUtil.getAttributesFromJSON(ori.vlocity_cmt__JSONAttribute__c);
                if(vlocity_cmt.FlowStaticMap.FlowMap != null && vlocity_cmt.FlowStaticMap.FlowMap.containsKey('disconnectSeriveceOliSet') ){
                    oliToSrvcAddrIdMap = (Map<Id,Id>)vlocity_cmt.FlowStaticMap.FlowMap.get('disconnectSeriveceOliSet');
                    
                }
                if( oliToSrvcAddrIdMap != null && oliToSrvcAddrIdMap.size() > 0 && oliToSrvcAddrIdMap.containsKey(orI.vlocity_cmt__RootItemId__c)){
                    orI.vlocity_cmt__ServiceAccountId__c = oliToSrvcAddrIdMap.get(orI.vlocity_cmt__RootItemId__c); 
                    system.debug('>> Order Item: ' + ori.id + 'SeriveAccount' + orI.vlocity_cmt__ServiceAccountId__c);    
                }                  
            }  
        } 

        UpdateServiceAddressOrderEntry(serviceAddressIdforMultisite, OrderObjectId , ListOrderItem);
    }
    
    /*       public void setUpBillingAccount(Account acc,OrderItem orI){
String BillAddr='';
if(acc.Billingstreet != null)
BillAddr=BillAddr+' '+acc.Billingstreet;
if(acc.BillingCity != null)
BillAddr=BillAddr+' '+acc.BillingCity;
if(acc.BillingState != null)
BillAddr=BillAddr+' '+acc.BillingState;
if(acc.BillingPostalCode != null)
BillAddr=BillAddr+' '+acc.BillingPostalCode;
if(acc.BillingCountry!= null)
BillAddr=BillAddr+' '+acc.BillingCountry;   

orI.vlocity_cmt__BillingAccountId__c=acc.id;
orI.Billing_Address__c=BillAddr;
orI.Billing_Account__c=acc.BAN_CAN__c;         
system.debug('Inside '+ orI.vlocity_cmt__BillingAccountId__c+' '+orI.Billing_Account__c);
}  
*/ 
    //This method is written for multisite functionality on 14-10-2016 by Prashant Tiwari
    Public void UpdateServiceAddressOrderEntry(set<id> serviceAddressIdforMultisite, Id orderObjId, List<orderItem> ListOfOrderItem){
        //List<ServiceAddressOrderEntry__c> ListOfServiceAddressOrderEntry = [select id,PricePerSite__c,Address__c,Address__r.Address__c,Address__r.Service_Account_Id__c, order__c from ServiceAddressOrderEntry__c where order__c =: orderObjId and  Address__c in : serviceAddressIdforMultisite];
        Map<Id,ServiceAddressOrderEntry__c> MapServiceAddressIDToSAO = new Map<Id,ServiceAddressOrderEntry__c>();
        for(ServiceAddressOrderEntry__c SAO : [select id,PricePerMonth__c  ,PricePerSite__c,Address__c,Address__r.Address__c,Address__r.Service_Account_Id__c, order__c from ServiceAddressOrderEntry__c where order__c=:orderObjId and  Address__c in : serviceAddressIdforMultisite]){
                MapServiceAddressIDToSAO.put(SAO.Address__c,SAO);
            }
        system.debug('ListOfOrderItem-------------->'+ ListOfOrderItem);
        
        for(orderitem orlObj:ListOfOrderItem){
            if(MapServiceAddressIDToSAO.containskey(orlObj.Service_Address__c)){
                    Decimal oldPricePerSite = MapServiceAddressIDToSAO.get(orlObj.Service_Address__c).PricePerSite__c ;//4-nov-2016 Arpit Added
                    Decimal oldPricePerMonth = MapServiceAddressIDToSAO.get(orlObj.Service_Address__c).PricePerMonth__c ;//4-nov-2016 Arpit Added 
                    system.debug('oldPricePerMonth -------------->'+ oldPricePerMonth );
                    system.debug('oldPricePerSite -------------->'+ oldPricePerSite );
                    system.debug('orlObj.Service_Address__c -----> '+MapServiceAddressIDToSAO.get(orlObj.Service_Address__c));
                    system.debug('Unit price & Quantity ---->'+ orlObj.UnitPrice+':::'+orlObj.Quantity);
                    if(MapServiceAddressIDToSAO.containskey(orlObj.Service_Address__c) && orlObj.UnitPrice!= null && orlObj.Quantity != null){
                        
                        if(MapServiceAddressIDToSAO.get(orlObj.Service_Address__c).PricePerSite__c == null){
                            MapServiceAddressIDToSAO.get(orlObj.Service_Address__c).PricePerSite__c = orlObj.UnitPrice * orlObj.Quantity;
                        }
                        else{
                            //MapServiceAddressIDToSAO.get(orlObj.Service_Address__c).PricePerSite__c  = MapServiceAddressIDToSAO.get(orlObj.Service_Address__c).PricePerSite__c + orlObj.UnitPrice * orlObj.Quantity; //4-nov-2016 Arpit Commented temporarily
                            MapServiceAddressIDToSAO.get(orlObj.Service_Address__c).PricePerSite__c = oldPricePerSite + orlObj.UnitPrice * orlObj.Quantity;//4-nov-2016 Arpit Added
                        }
                        if(MapServiceAddressIDToSAO.get(orlObj.Service_Address__c).PricePerMonth__c == null){
                            MapServiceAddressIDToSAO.get(orlObj.Service_Address__c).PricePerMonth__c = orlObj.vlocity_cmt__RecurringCharge__c * orlObj.Quantity;
                        }
                        else{
                            //MapServiceAddressIDToSAO.get(orlObj.Service_Address__c).PricePerMonth__c  = MapServiceAddressIDToSAO.get(orlObj.Service_Address__c).PricePerMonth__c + orlObj.vlocity_cmt__RecurringCharge__c * orlObj.Quantity;//4-nov-2016 Arpit Commented temporarily
                            MapServiceAddressIDToSAO.get(orlObj.Service_Address__c).PricePerMonth__c  = oldPricePerMonth + (orlObj.vlocity_cmt__RecurringCharge__c * orlObj.Quantity);//4-nov-2016 Arpit Added
                            system.debug('Per month price for this SAO---->'+ MapServiceAddressIDToSAO.get(orlObj.Service_Address__c).PricePerMonth__c);
                        }
                    }
            }//if
        }
        if(MapServiceAddressIDToSAO.size()>0){
            update MapServiceAddressIDToSAO.values();
            system.debug('>>>>>>>>>>::::::::'+MapServiceAddressIDToSAO.values());
        }
    
    }
    
    //This method is written for multisite functionality to delete one time total and monthly total on delete of orderLineitem on 22-11-2016 by Piyush nandan
    Public void DeleteServiceAddressOrderEntry(Set<Id> oliIdSet){
        Map<Id, OrderItem> orderItemsMap;
        set<id> serviceAddressIdforMultisite = new set<id>();
        Id orderObjId;
        List<orderItem> ListOfOrderItem = new List<orderItem>();
     
         try{
            orderItemsMap  = new Map<Id, OrderItem>([SELECT Id,Order.Id,Service_Address__r.Id,UnitPrice,Quantity,vlocity_cmt__RecurringCharge__c
                                                     FROM OrderItem 
                                                     WHERE Id IN : oliIdSet]);   
            system.debug('orderItemsMap '+orderItemsMap);
            orderObjId = orderItemsMap.values()[0].Order.Id;
            
            system.debug('orderObjId------'+ orderObjId );
            
        }catch(exception e){
            string exceptionMsg='We have encountered an exception : '+e.getMessage();
            OCOM_WFM_DueDate_Mgmt_Helper.insertWebserviceLog('ClassName:OCOM_OrderProductBeforeTrigger_Helper and MethodName:cancelWorkOrderofDeletedOLIs ',exceptionMsg,e,'Failure');
        }
        
        if(null != orderItemsMap)
            for(OrderItem o:orderItemsMap.values()) {
                serviceAddressIdforMultisite.add(o.Service_Address__r.Id);
                ListOfOrderItem.add(o); 
                
            } 
            system.debug('serviceAddressIdforMultisite--------->'+serviceAddressIdforMultisite);
                
         Map<Id,ServiceAddressOrderEntry__c> MapServiceAddressIDToSAO = new Map<Id,ServiceAddressOrderEntry__c>();
         for(ServiceAddressOrderEntry__c SAO : [select id,PricePerMonth__c  ,PricePerSite__c,Address__c,Address__r.Address__c,Address__r.Service_Account_Id__c, order__c from ServiceAddressOrderEntry__c where order__c=:orderObjId and  Address__c in : serviceAddressIdforMultisite]){
                MapServiceAddressIDToSAO.put(SAO.Address__c,SAO);
            }     
         system.debug('MapServiceAddressIDToSAO of -------------->'+ MapServiceAddressIDToSAO);
        
         for(orderitem orlObj:ListOfOrderItem){
            if(MapServiceAddressIDToSAO.containskey(orlObj.Service_Address__c)){
                    Decimal oldPricePerSite = MapServiceAddressIDToSAO.get(orlObj.Service_Address__c).PricePerSite__c ;//4-nov-2016 Arpit Added
                    Decimal oldPricePerMonth = MapServiceAddressIDToSAO.get(orlObj.Service_Address__c).PricePerMonth__c ;//4-nov-2016 Arpit Added 
                    system.debug('oldPricePerMonth DeleteServiceAddressOrderEntry -------------->'+ oldPricePerMonth );
                    system.debug('oldPricePerSite DeleteServiceAddressOrderEntry -------------->'+ oldPricePerSite );
                    system.debug('orlObj.Service_Address__c DeleteServiceAddressOrderEntry -----> '+MapServiceAddressIDToSAO.get(orlObj.Service_Address__c));
                    system.debug('Unit price & Quantity DeleteServiceAddressOrderEntry---->'+ orlObj.UnitPrice+':::'+orlObj.Quantity);
                    if(orlObj.UnitPrice != null && orlObj.Quantity != null){
                        if(MapServiceAddressIDToSAO.get(orlObj.Service_Address__c).PricePerSite__c != null){
                            MapServiceAddressIDToSAO.get(orlObj.Service_Address__c).PricePerSite__c = oldPricePerSite - orlObj.UnitPrice * orlObj.Quantity;
                            system.debug('MapServiceAddressIDToSAO.get(orlObj.Service_Address__c).PricePerSite__c'+  MapServiceAddressIDToSAO.get(orlObj.Service_Address__c).PricePerSite__c);
                        }
                        
                        if(MapServiceAddressIDToSAO.get(orlObj.Service_Address__c).PricePerMonth__c != null){
                             MapServiceAddressIDToSAO.get(orlObj.Service_Address__c).PricePerMonth__c  = oldPricePerMonth - (orlObj.vlocity_cmt__RecurringCharge__c * orlObj.Quantity);
                        }
                        
                    }
            }
        }
        if(MapServiceAddressIDToSAO.size()>0){
            update MapServiceAddressIDToSAO.values();
            system.debug('>>>>>>>>>>::::::::'+MapServiceAddressIDToSAO.values());
        }
    }
     
    public static void updateBPIInstanceId(Map<id,OrderItem> orderItemArgMap){
        System.debug('Debug inside updateBPIInstanceId ');
        List<String> orderItemIdList=new List<String>();
        Set<String> orderIdSet=new Set<String>();
        Map<String,List<Asset>> orderIdAssetMap=new Map<String,List<Asset>>();
        Map<String,String> orderItemIdBPIInstanceMap=new  Map<String,String>();
        for(Id oId: orderItemArgMap.keySet()) {
            OrderItem orderItemArg =orderItemArgMap.get(oId);
             orderIdSet.add(orderItemArg.orderid);           
        }
       createAssetsFuture(orderIdSet);
 
    }
    
  /*  private boolean createAssets(List<String> orderIdList){
        boolean isAssetCreated=false;
        for(string orderId: orderIdList ){
        if(!Test.isrunningTest()){
            vlocity_cmt.DefaultObjectCopierImplementation objCopier =  new vlocity_cmt.DefaultObjectCopierImplementation();
            vlocity_cmt.FlowStaticMap.flowMap.put('objectCopier',objCopier.copyObject(orderId, ''));
            isAssetCreated=true;
            }
        }
        return isAssetCreated;
    }*/
    @future
    private static void createAssetsFuture(Set<String> orderIdSet){
        List<String> orderItemIdList=new List<String>();    
        Map<String,String> orderItemIdBPIInstanceMap=new  Map<String,String>();
        Map<String,String> orderItemIdOrderIdMap=new  Map<String,String>();
        List<OrderItem> oiList=[select id,orderid,orderMgmt_BPI_Id__c from orderitem where orderid in :orderIdSet];
        if(oiList!=null){
            for(OrderItem oiObj:oiList){
                if(String.isNotBlank(oiObj.orderMgmt_BPI_Id__c)){
                    orderItemIdBPIInstanceMap.put(oiObj.id,oiObj.orderMgmt_BPI_Id__c);
                }
                orderItemIdOrderIdMap.put(oiObj.id,oiObj.orderid);
               orderItemIdList.add(oiObj.id);               
            }
        }
        Set<String> orderIdExclusionSet=new Set<String>();
        List<Asset> assetToUpdateList=getAssetsByOrderItemIdList(orderItemIdList);
        System.debug('debugging assetToUpdateList size is '+assetToUpdateList.size());
        if(assetToUpdateList!=null && assetToUpdateList.size()>0){
            for(Asset assetToUpdateObj:assetToUpdateList){
                if(String.isNotBlank(orderItemIdOrderIdMap.get(assetToUpdateObj.vlocity_cmt__OrderProductId__c)) && !orderIdExclusionSet.contains(orderItemIdOrderIdMap.get(assetToUpdateObj.vlocity_cmt__OrderProductId__c))){
                    orderIdExclusionSet.add(orderItemIdOrderIdMap.get(assetToUpdateObj.vlocity_cmt__OrderProductId__c));
                }
            }
            updateAssets(assetToUpdateList,orderItemIdBPIInstanceMap);
        }       
        List<webservice_integration_error_log__c> errorList=new List<webservice_integration_error_log__c>();
        for(string orderId: orderIdSet ){
            if(!orderIdExclusionSet.contains(orderId)){
                Long startTime=System.currentTimeMillis();
                if(!Test.isrunningTest()){
                 vlocity_cmt.DefaultObjectCopierImplementation objCopier =  new vlocity_cmt.DefaultObjectCopierImplementation();
                 vlocity_cmt.FlowStaticMap.flowMap.put('objectCopier',objCopier.copyObject(orderId, ''));    
                 }
                Long endTime=System.currentTimeMillis();
                Long timeTaken=(endTime-startTime);
                String msg=' Time taken to create Assets for the orderid '+orderId +' took '+timeTaken+' milliseconds';
                webservice_integration_error_log__c errorLog=new webservice_integration_error_log__c(apex_class_and_method__c='DefaultObjectCopierImplementation.copyObject',external_system_name__c='Vlocity',webservice_name__c='None',object_name__c='Order',sfdc_record_id__c=orderId);
                errorLog.error_code_and_message__c=msg;
                errorList.add(errorLog);
                    assetToUpdateList=getAssetsByOrderItemIdList(orderItemIdList);
                    if(assetToUpdateList!=null && assetToUpdateList.size()>0){
                        updateAssets(assetToUpdateList,orderItemIdBPIInstanceMap);
                    }
            }
        }
        if(errorList!=null && errorList.size()>0){
            insert errorList;
        }
    }
   private static List<Asset> getAssetsByOrderItemIdList(List<String> orderItemIdList){
        List<Asset> assetToUpdateList=[Select  Contract_Line_Item__c,MCM_External_ID__c,vlocity_cmt__OrderId__c,orderMgmt_BPI_Id__c,vlocity_cmt__OrderProductId__c  From Asset where vlocity_cmt__OrderProductId__c in :orderItemIdList];
        return assetToUpdateList;
    }
  /*  private  List<Asset> getAssetsByOrderIdList(List<String> orderItemIdList){
        List<Asset> assetToUpdateList=[Select MCM_External_ID__c,vlocity_cmt__OrderId__c,orderMgmt_BPI_Id__c,vlocity_cmt__OrderProductId__c  From Asset where vlocity_cmt__OrderId__c in :orderItemIdList];
        return assetToUpdateList;
    }
    private Map<String,List<Asset>> createOrderIdAssetMap(List<Asset> assetToUpdateList,Map<String,List<Asset>> orderIdAssetMap){
        if(orderIdAssetMap==null){
            orderIdAssetMap=new Map<String,List<Asset>>();
        }
        List<Asset> assetsListByOrderId=null;
        for(Asset assetToUpdate:assetToUpdateList){
            if(orderIdAssetMap.get(assetToUpdate.vlocity_cmt__OrderId__c)==null){
                assetsListByOrderId=new List<Asset>();
            } else {
                assetsListByOrderId=orderIdAssetMap.get(assetToUpdate.vlocity_cmt__OrderId__c);
            }
            assetsListByOrderId.add(assetToUpdate);
            orderIdAssetMap.put(assetToUpdate.vlocity_cmt__OrderId__c,assetsListByOrderId);
        }
        return orderIdAssetMap;
    }
    */
    private static void updateAssets(List<Asset> assetToUpdateList,Map<String,String> orderItemIdBPIInstanceMap){
        System.debug('Update contract line is called assetToUpdateList size is '+assetToUpdateList.size());
        System.debug('debugging assetToUpdateList='+JSON.serializePretty(assetToUpdateList));
        System.debug('debugging orderItemIdBPIInstanceMap='+JSON.serializePretty(orderItemIdBPIInstanceMap));
        try{
            List<vlocity_cmt__ContractLineItem__c> cliObjList=[SELECT Id, OrderLineItemId__c,createdby.name,lastmodifiedby.name FROM vlocity_cmt__ContractLineItem__c where OrderLineItemId__c in :orderItemIdBPIInstanceMap.keySet()   order by lastmodifieddate desc];
            
           /* List<OrderItem> oiObjList=[select id,Contract_Line_Item__c from orderItem where id in :orderItemIdBPIInstanceMap.keySet()];
            for(OrderItem oiObj:oiObjList){
                String cliId=getCliId(cliObjList,oiObj.id);
                   if(String.isNotBlank(cliId)){
                      oiObj.Contract_Line_Item__c= cliId;
                    }
            }
            update oiObjList;*/
            for(Asset assetToUpdate:assetToUpdateList){
                if(assetToUpdate!=null){
                    String cliId=getCliId(cliObjList,assetToUpdate.vlocity_cmt__OrderProductId__c);
                    System.debug('debugging cliId='+cliId);
                     System.debug('debugging assetToUpdate.vlocity_cmt__OrderProductId__c='+assetToUpdate.vlocity_cmt__OrderProductId__c);
                    assetToUpdate.MCM_External_ID__c=orderItemIdBPIInstanceMap.get(assetToUpdate.vlocity_cmt__OrderProductId__c);
                    assetToUpdate.orderMgmt_BPI_Id__c=orderItemIdBPIInstanceMap.get(assetToUpdate.vlocity_cmt__OrderProductId__c);
                    if(String.isNotBlank(cliId)){
                      assetToUpdate.Contract_Line_Item__c= cliId;
                    }
                }
            }
            update assetToUpdateList;
        }catch(Exception e){
            System.debug('Exception occurred while creating assets '+e);
        }
    }
    private static String getCliId(List<vlocity_cmt__ContractLineItem__c> cliObjList,String orderItemId){
        for(vlocity_cmt__ContractLineItem__c cliObj:cliObjList){
            if(String.isNotBlank(cliObj.OrderLineItemId__c) && String.isNotBlank(orderItemId) && orderItemId.equalsIgnoreCase(cliObj.OrderLineItemId__c)){
                return cliObj.id;
            }
        }
        return null;
    }
    public void cancelWorkOrderofDeletedOLIs(Set<String> oliIdSet){
        String userId = 'APP_SFDC';
        Map<Id, OrderItem> orderItemsMap;
        set<id> serviceAddressIdforMultisite;
        Id orderObjId;
        List<orderItem> ListOfOrderItem;
        try{
            orderItemsMap  = new Map<Id, OrderItem>([SELECT Id,Order.Id,Service_Address__r.Id,work_order__c,work_order__r.status__c,work_order__r.WFM_number__c
                                                     FROM OrderItem 
                                                     WHERE (Id IN :oliIdSet OR vlocity_cmt__RootItemId__c  IN :oliIdSet ) and work_order__c != null and work_order__r.status__c= :Label.OCOM_WFMReserved]);   
            system.debug('orderItemsMap '+orderItemsMap);
            //orderObjId = orderItemsMap.values()[0].Id;
            if(null!= orderItemsMap  && orderItemsMap.size()>0){
                list<user> lstTid = [select id,Team_TELUS_ID__c from User where id = :Userinfo.getUserId()];
                
                if(lstTid != null && lstTid.size()>0)
                {  
                    if (string.isNotBlank(lstTid[0].Team_TELUS_ID__c))
                        userId= lstTid[0].Team_TELUS_ID__c;
                }
            }  
        }catch(exception e){
            string exceptionMsg='We have encountered an exception : '+e.getMessage();
            OCOM_WFM_DueDate_Mgmt_Helper.insertWebserviceLog('ClassName:OCOM_OrderProductBeforeTrigger_Helper and MethodName:cancelWorkOrderofDeletedOLIs ',exceptionMsg,e,'Failure');
        }
        integer count=0; 
        Map<String,String> oliIdToWFMNumberMap = new Map<String,String>(); 
        Map<String,String> oliIdTowOrderIdMap  = new Map<String,String>(); 
        if(null != orderItemsMap)
            for(OrderItem o:orderItemsMap.values()) {
                oliIdToWFMNumberMap.put(o.id,o.Work_Order__r.WFM_number__c);
                oliIdTowOrderIdMap.put(o.id,o.Work_Order__c);
                
                 //Added by OCOM Team,This is NAAS code and it was failing for System.NullPointerException: Attempt to de-reference a null object.
                 // serviceAddressIdforMultisite and ListOfOrderItem are defined at both global and method level but logic will pick always method level set and
                // since it is null, this piece of code will always return exception and never get executed.
               // Please fix this.It seems is not accomplishing any requirement.
                if(null != o.Service_Address__c && null != serviceAddressIdforMultisite ){
                    serviceAddressIdforMultisite.add(o.Service_Address__r.Id);
                }
                if(null != ListOfOrderItem){
                    ListOfOrderItem.add(o); 
                }
                //End by OCOM Team.
                
                count++;
                if((Math.mod(count,10) == 0) && count != 0 ){       
                    OCOM_WFM_DueDate_Mgmt_Helper.cancelWFMBookingByOLIIdList(oliIdToWFMNumberMap,oliIdTowOrderIdMap,userId);
                    oliIdToWFMNumberMap = new Map<String,String>();
                    oliIdTowOrderIdMap = new Map<String,String>();
                    count=0;
                }    
                
            }   
        if(count>0){
            if(!Test.isRunningTest()){
                 OCOM_WFM_DueDate_Mgmt_Helper.cancelWFMBookingByOLIIdList(oliIdToWFMNumberMap,oliIdTowOrderIdMap,userId); 
            }          
           
        }
    }
    
    
    
    
}