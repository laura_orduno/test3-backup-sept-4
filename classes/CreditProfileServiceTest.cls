@isTest
public class CreditProfileServiceTest {
    
    public static CreditProfileService.BusinessCreditProfileMgmtService_v1_0_SOAP 
        servicePort 
        = new CreditProfileService.BusinessCreditProfileMgmtService_v1_0_SOAP();
            
    @isTest
    public static void ping(){
        Test.setMock(WebServiceMock.class, new CreditProfileServiceMock());
        Test.startTest();
        servicePort.ping();        
        Test.StopTest();    
    }
    
    
    @isTest
    public static void addBusinessPartner(){
        Test.setMock(WebServiceMock.class, new CreditProfileServiceMock());
        Test.startTest();
        Long masterPartnerBusinessCustomerId;
        Long partnerBusinessCustomerId;
        CreditProfileServiceTypes.CreditAuditInfo auditInfo;        
        servicePort.addBusinessPartner( masterPartnerBusinessCustomerId
                                       ,partnerBusinessCustomerId
                                       ,auditInfo);        
        Test.StopTest();    
    }
    
    
    @isTest
    public static void addIndividualPartner(){
        Test.setMock(WebServiceMock.class, new CreditProfileServiceMock());
        Test.startTest();
        Long masterBusinessCustomerId;
        Long partnerIndividualCustomerId;
        CreditProfileServiceTypes.CreditAuditInfo auditInfo;           
        servicePort.addIndividualPartner( masterBusinessCustomerId
                                         ,partnerIndividualCustomerId
                                         ,auditInfo);        
        Test.StopTest();    
    }
    
    
    @isTest
    public static void attachCreditReport(){
        Test.setMock(WebServiceMock.class, new CreditProfileServiceMock());
        Test.startTest();
        Long businessCustomerId;
        CreditProfileServiceTypes.BusinessCreditReportRequest businessCreditReportRequest;
        CreditProfileServiceTypes.CreditAuditInfo auditInfo;              
        servicePort.attachCreditReport( businessCustomerId
                                       ,businessCreditReportRequest
                                       ,auditInfo);        
        Test.StopTest();    
    }
    
    
    @isTest
    public static void createBusinessCreditProfile(){
        Test.setMock(WebServiceMock.class, new CreditProfileServiceMock());
        Test.startTest();        
        Long businessCustomerId;
        CreditProfileServiceTypes.BusinessCreditProfileBase newBusinessCreditProfile;
        Long proprietorshipIndividualCustomerId;
        CreditProfileServiceTypes.CreditAuditInfo auditInfo;        
        servicePort.createBusinessCreditProfile( businessCustomerId
                                                ,newBusinessCreditProfile
                                                ,proprietorshipIndividualCustomerId
                                                ,auditInfo);        
        Test.StopTest();    
    }
    
    
    @isTest
    public static void createIndividualCustomerWithCreditWorthiness(){
        Test.setMock(WebServiceMock.class, new CreditProfileServiceMock());
        Test.startTest();  
        CreditProfileServiceTypes.IndividualCustomerProfile individualCustomer;
        CreditProfileServiceTypes.IndividualCreditProfileBase individualCreditProfile;
        CreditProfileServiceTypes.CreditAuditInfo auditInfo;
        
        servicePort.createIndividualCustomerWithCreditWorthiness( individualCustomer
                                                                 ,individualCreditProfile
                                                                 ,auditInfo
                                                                );        
        Test.StopTest();    
    }
    
    
    @isTest
    public static void getBusinessCreditProfileSummary(){
        Test.setMock(WebServiceMock.class, new CreditProfileServiceMock());
        Test.startTest();  
        Long businessCustomerId;
        CreditProfileServiceTypes.CreditAuditInfo auditInfo;
        servicePort.getBusinessCreditProfileSummary( businessCustomerId
                                                    ,auditInfo          
                                                   );        
        Test.StopTest();    
    }
    
    
    @isTest
    public static void getCreditReport(){
        Test.setMock(WebServiceMock.class, new CreditProfileServiceMock());
        Test.startTest(); 
        Long creditReportId;
        servicePort.getCreditReport(creditReportId);        
        Test.StopTest();    
    }
    
    
    @isTest
    public static void getCreditReportList(){
        Test.setMock(WebServiceMock.class, new CreditProfileServiceMock());
        Test.startTest(); 
        Long businessCustomerId;
        servicePort.getCreditReportList(businessCustomerId);        
        Test.StopTest();    
    }
    
    
    @isTest
    public static void removePartner(){
        Test.setMock(WebServiceMock.class, new CreditProfileServiceMock());
        Test.startTest(); 
        Long masterBusinessCustomerId;
        Long partnerCustomerId;
        CreditProfileServiceTypes.CreditAuditInfo auditInfo;        
        servicePort.removePartner(masterBusinessCustomerId
                                  ,partnerCustomerId
                                  ,auditInfo
                                 );        
        Test.StopTest();    
    }
    
    
    @isTest
    public static void replaceBusinessProprietor(){
        Test.setMock(WebServiceMock.class, new CreditProfileServiceMock());
        Test.startTest(); 
        Long masterBusinessCustomerId;
        Long proprietorIndividualCustomerId;
        CreditProfileServiceTypes.CreditAuditInfo auditInfo;            
        servicePort.replaceBusinessProprietor(masterBusinessCustomerId
                                              ,proprietorIndividualCustomerId
                                              ,auditInfo
                                             );        
        Test.StopTest();    
    }
    
    
    @isTest
    public static void searchBusinessCreditProfile(){
        Test.setMock(WebServiceMock.class, new CreditProfileServiceMock());
        Test.startTest(); 
        String registrationIncorporationNum;
        String secondaryDunsNum;
        CreditProfileServiceTypes.CreditAuditInfo auditInfo;
        servicePort.searchBusinessCreditProfile(registrationIncorporationNum
                                                ,secondaryDunsNum
                                                ,auditInfo
                                               );        
        Test.StopTest();    
    }
    
    
    @isTest
    public static void searchIndivdualCreditProfile(){
        Test.setMock(WebServiceMock.class, new CreditProfileServiceMock());
        Test.startTest(); 
        CreditProfileServiceTypes.IndividualCreditIdentification creditIdentification;
        CreditProfileServiceTypes.CreditAuditInfo auditInfo;
        servicePort.searchIndivdualCreditProfile(creditIdentification
                                                 ,auditInfo
                                                );        
        Test.StopTest();    
    }
    
    
    @isTest
    public static void updateBusinessCreditProfile(){
        Test.setMock(WebServiceMock.class, new CreditProfileServiceMock());
        Test.startTest(); 
        Long businessCustomerId;
        CreditProfileServiceTypes.BusinessCreditProfileBase updatedBusinessCreditProfile;
        Long proprietorshipIndividualCustomerId;
        CreditProfileServiceTypes.CreditAuditInfo auditInfo;
        servicePort.updateBusinessCreditProfile(businessCustomerId
                                                ,updatedBusinessCreditProfile
                                                ,proprietorshipIndividualCustomerId
                                                ,auditInfo
                                               );        
        Test.StopTest();    
    }
    
    
    @isTest
    public static void updateIndivdualCreditProfile(){
        Test.setMock(WebServiceMock.class, new CreditProfileServiceMock());
        Test.startTest();
        Long individualCustomerId;
        CreditProfileServiceTypes.IndividualCreditProfile individualCreditProfile;
        CreditProfileServiceTypes.CreditAuditInfo auditInfo;
        servicePort.updateIndivdualCreditProfile(individualCustomerId
                                                 ,individualCreditProfile
                                                 ,auditInfo
                                                );        
        Test.StopTest();    
    }
    
    
    @isTest
    public static void voidCreditReport(){
        Test.setMock(WebServiceMock.class, new CreditProfileServiceMock());
        Test.startTest();
        Long creditReportId;
        CreditProfileServiceTypes.CreditAuditInfo auditInfo;            
        servicePort.voidCreditReport(creditReportId
                                     ,auditInfo
                                    );        
        Test.StopTest();    
    }
    
           
}