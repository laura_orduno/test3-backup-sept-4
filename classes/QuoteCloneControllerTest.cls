@isTest
private class QuoteCloneControllerTest {
    private static testMethod void testQuoteCloneController() {
        Opportunity o = new Opportunity(name = 'test', stagename = 'test', closedate = Date.today());
        insert o;
        
        SBQQ__Quote__c quote = new SBQQ__Quote__c(SBQQ__Opportunity__c = o.id);
        insert quote;
        
        Quote_Clone_Request__c request = new Quote_Clone_Request__c(
            cloner__c = UserInfo.getUserId(),
            Source_Quote__c = quote.id,
            requester__c = UserInfo.getUserId(),
            Status__c = 'Sent'
        );
        insert request;
        
        ApexPages.currentPage().getParameters().put('returl', 'http://example.com');
        ApexPages.currentPage().getParameters().put('messages', 'msg1|msg2');
        ApexPages.currentPage().getParameters().put('qid', quote.id);
        
        QuoteCloneController ctlr = new QuoteCloneController();
        
        ctlr.updateSelectedOwner(); // empty method
        
        ctlr.findUsers();
        
        ctlr.initRequest();
        
        ctlr.onSubmitRequest();
        
        ctlr.onDeclineRequest();
        
        ctlr.onClone();
    }
    
    private static testMethod void testStaticMethods() {
        system.assertEquals(false, QuoteCloneController.channelCanClone(null));
        system.assertEquals(false, QuoteCloneController.channelCanRequestClone(null));
    }
    
    private static testMethod void testOwnerOption() {
        Id PROFILEID = [SELECT id FROM Profile WHERE name='Standard User' LIMIT 1].id;
        
        User u = new User(
            username = Datetime.now().getTime() + Math.random() + '@TRACTIONSM.COM',
          email = 'test@example.com',
          title = 'test',
          firstname = 'first',
          lastname = 'last',
          alias = 'test',
          TimezoneSIDKey = 'America/Los_Angeles',
          LocaleSIDKey = 'en_US',
          EmailEncodingKey = 'UTF-8',
          ProfileId = PROFILEID,
          LanguageLocaleKey = 'en_US',
          channel__c = 'channel'
        );
      insert u;
      
      u = [SELECT username, email, name, firstname, lastname, channel__c FROM User WHERE Id = :u.id];
        
        QuoteCloneController.OwnerOption oo = new QuoteCloneController.OwnerOption(u, 'test');
        
        system.assertEquals(u.id, oo.id);
        system.assertEquals(u.firstname + ' ' + u.lastname, oo.name);
        system.assertEquals(u.email, oo.email);
        system.assertEquals(u.channel__c, oo.channel);
        system.assertEquals('test', oo.rel);
        
        system.assertEquals(u.firstname + ' ' + u.lastname + ' ' + u.email + ' ' + u.channel__c, oo.optionName);
        
        oo = new QuoteCloneController.OwnerOption(u);
        
        system.assertEquals('', oo.rel);
    }
}