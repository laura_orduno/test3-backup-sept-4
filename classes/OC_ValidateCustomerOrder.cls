/*
    *******************************************************************************************************************************
    Class Name:     OC_ValidateCustomerOrder
    Purpose:        Controller for validate customer Order Step in OC Flow        
    Created by:     Sandip Chaudhari    01-Aug-2017     BMPF-17         No previous version
    Modified by:    

*/

global without sharing class OC_ValidateCustomerOrder{
     
     public static Credit_Assessment__c aCreditAssessment = null;
     public static integer numberOfContractableOffers = 0;
     public static List<Credit_Profile__c> creditProfile = new List<Credit_Profile__c>();
     
     global OC_ValidateCustomerOrder (){
     }
     
     
     public static Map<Id, Map<Id,List<String>>> validateOrderOnSubmit(Map<ID,List<ID>> orderMap){
         Map<Id, Map<Id,List<String>>> validationResult = new Map<Id, Map<Id,List<String>>>();
         List<Order> orderListToUpdate = new List<Order>();
         List<OrderItem> orderItemList = new List<OrderItem>();
         Set<Id> masterOrderSet = orderMap.keySet();
         ID masterId;
         List<Order> ordersToValidate;
         Map<Id,List<String>> errorMessageMap = new Map<Id,List<String>>();
         Order masterOrder;
         if(masterOrderSet != null && masterOrderSet.size()==1){
             masterId = (new list<Id>(masterOrderSet) )[0];
             masterOrder = [SELECT Id, Type, AccountId, Account.Account_Status__c, Account.Credit_Profile__c, Credit_Assessment_ID__c, Credit_Assessment_Status__c, 
                                        requesteddate__c, Ban__c, Shipping_Address__c, Credit_Check_Not_Required__c, Credit_Reference_Number__c, CPQ_Configuration_Status__c ,
                                        Rush__c, status, Shipping_Address1__c, Service_Address__c, ShipToContactId, BillToContactId, vlocity_cmt__ValidationMessage__c, 
                                        vlocity_cmt__ValidationStatus__c, vlocity_cmt__ValidationDate__c, vlocity_cmt__OneTimeTotal__c, vlocity_cmt__RecurringTotal__c, 
                                        vlocity_cmt__TotalMonthlyDiscount__c, vlocity_cmt__TotalOneTimeDiscount__c, orderMgmtId_Status__c 
                                     FROM Order 
                                     WHERE Id =: masterId];
             List<Id> childOrderIds = orderMap.get(masterId);
             // get child orders
             if(childOrderIds != null && childOrderIds.size()>0){
                 ordersToValidate = [SELECT Id, Type, AccountId, Account.Account_Status__c, Account.Credit_Profile__c, Credit_Assessment_ID__c, Credit_Assessment_Status__c, 
                                        requesteddate__c, Ban__c, Shipping_Address__c, Credit_Check_Not_Required__c, Credit_Reference_Number__c, CPQ_Configuration_Status__c, 
                                        Rush__c, status, Shipping_Address1__c, Service_Address__c, ShipToContactId, BillToContactId, vlocity_cmt__ValidationMessage__c, 
                                        vlocity_cmt__ValidationStatus__c, vlocity_cmt__ValidationDate__c, vlocity_cmt__OneTimeTotal__c, vlocity_cmt__RecurringTotal__c, 
                                        vlocity_cmt__TotalMonthlyDiscount__c, vlocity_cmt__TotalOneTimeDiscount__c, orderMgmtId_Status__c 
                                     FROM Order 
                                     WHERE ParentId__c =: masterId 
                                     AND Id IN: childOrderIds];
             }else{
                 ordersToValidate = [SELECT Id, Type, AccountId, Account.Account_Status__c, Account.Credit_Profile__c, Credit_Assessment_ID__c, Credit_Assessment_Status__c, 
                                        requesteddate__c, Ban__c, Shipping_Address__c, Credit_Check_Not_Required__c, Credit_Reference_Number__c, CPQ_Configuration_Status__c, 
                                        Rush__c, status, Shipping_Address1__c, Service_Address__c, ShipToContactId, BillToContactId, vlocity_cmt__ValidationMessage__c, 
                                        vlocity_cmt__ValidationStatus__c, vlocity_cmt__ValidationDate__c, vlocity_cmt__OneTimeTotal__c, vlocity_cmt__RecurringTotal__c, 
                                        vlocity_cmt__TotalMonthlyDiscount__c, vlocity_cmt__TotalOneTimeDiscount__c, orderMgmtId_Status__c 
                                     FROM Order 
                                     WHERE ParentId__c =: masterId];
             }
             
            // get orderitems
            // BSBD_RTA-892 - added Term__c in query
            orderItemList=[SELECT ID, Term__c, OrderId, amendStatus__c, Scheduled_Due_Date__c, vlocity_cmt__LineNumber__c, vlocity_cmt__BillingAccountId__c, 
                                             vlocity_cmt__ParentItemId__c, PriceBookEntry.product2.IncludeQuoteContract__c, 
                                             PriceBookEntry.product2.OCOM_Shippable__c, Shipping_Address__c,
                                             Work_Order__c, PricebookEntry.Name, vlocity_cmt__ProvisioningStatus__c, Work_Time__c,
                                             pricebookentry.product2.OCOM_Bookable__c, vlocity_cmt__JSONAttribute__c,
                                             Credit_Assessment_Required__c, vlocity_cmt__RecurringTotal__c, 
                                             vlocity_cmt__OneTimeTotal__c, vlocity_cmt__IsProductTrackAgreement__c
                                         FROM OrderItem 
                                         WHERE OrderId IN:ordersToValidate order by vlocity_cmt__LineNumber__c];
             Map<Id, List<OrderItem>> orderItemMap = new Map<Id, List<OrderItem>>();
             Map<Id, Integer> contractableOffersMap = new Map<Id, Integer>();
             // prepare map for order Id and associated list of orderitems
             if(orderItemList != null && orderItemList.size()>0){
                 for(OrderItem orderItemObj: orderItemList){
                     List<OrderItem> tmpOIList;
                     if(orderItemMap.containsKey(orderItemObj.OrderId)){
                         tmpOIList = orderItemMap.get(orderItemObj.OrderId);
                     }else{
                         tmpOIList = new List<OrderItem>();
                     }
                     tmpOIList.add(orderItemObj);
                     orderItemMap.put(orderItemObj.OrderId, tmpOIList);
                     
                     //count contractible offers per ordres
                  /*   if(orderItemObj.vlocity_cmt__IsProductTrackAgreement__c==true){
                         if(contractableOffersMap.get(orderItemObj.OrderId)!=null){
                             Integer counter = contractableOffersMap.get(orderItemObj.OrderId);
                             counter++;
                             contractableOffersMap.put(orderItemObj.OrderId, counter);
                         }else{
                             contractableOffersMap.put(orderItemObj.OrderId, 1);
                         }
                     }*/
                 }
             }
            
             Set<Id> CARIds = new Set<Id>();
             Set<Id> accIds = new Set<Id>();
             Map<Id, Credit_Assessment__c> carMap = new Map<Id, Credit_Assessment__c>();
             //for(Order orderRecord: ordersToValidate){
             if(masterOrder!=null){
                 CARIds.add(masterOrder.Credit_Assessment_ID__c);
                 accIds.add(masterOrder.AccountId);
             }
             //}
             
             //get credit assessement
             List<Credit_Assessment__c> creditAssessmentList = [SELECT CAR_Status__c, Total_Deal_Value__c, Total_Deal_Monthly_Recurring_Charges__c, Total_Deal_Non_Recurring_Charges__c 
                                                                FROM Credit_Assessment__c 
                                                                WHERE id IN:CARIds];
             for(Credit_Assessment__c carObj: creditAssessmentList ){
                 carMap.put(carObj.Id, carObj);
             }
             
             //get credit profile
             List<Credit_Profile__c> creditProfileList = [SELECT id, account__c, credit_reference_TL__c 
                                                     FROM Credit_Profile__c where account__c IN:accIds and credit_reference_TL__c <> ''];
             
             Map<Id, List<Credit_Profile__c>> cpMap = new Map<Id, List<Credit_Profile__c>>();
             // prepare map for account Id and associated list of credit profiles
             for(Credit_Profile__c cpObj: creditProfileList){
                 List<Credit_Profile__c> tmpList;
                 if(cpMap.get(cpObj.account__c) != null){
                     tmpList = cpMap.get(cpObj.account__c);
                 }else{
                     tmpList = new List<Credit_Profile__c>();
                 }
                 tmpList.add(cpObj);
                 cpMap.put(cpObj.account__c, tmpList);
             }
             aCreditAssessment = null;
             if(carMap.get(masterOrder.Credit_Assessment_ID__c)!=null){
                aCreditAssessment =  carMap.get(masterOrder.Credit_Assessment_ID__c);
             }   
             for(Order orderRecord: ordersToValidate){
                // Credit check variable initialization to bulkify the code
                //aCreditAssessment = null;
                if(carMap.get(orderRecord.Credit_Assessment_ID__c)!=null){
                    aCreditAssessment =  carMap.get(orderRecord.Credit_Assessment_ID__c);
                }
                numberOfContractableOffers = 0;
                if(contractableOffersMap.get(orderRecord.Id)!=null){
                    numberOfContractableOffers = contractableOffersMap.get(orderRecord.Id);
                }
                
                creditProfile = new List<Credit_Profile__c>();
                if(cpMap.get(orderRecord.AccountId)!=null){
                    creditProfile =  cpMap.get(orderRecord.AccountId);
                }
                // End
                boolean customerOrderValidationError=false;
                List<String> errorMsgList;
                if(errorMessageMap.containsKey(orderRecord.Id)){
                    errorMsgList = errorMessageMap.get(orderRecord.Id);
                }else{
                    errorMsgList = new List<String>();
                }
                
                // check on order status
                if (orderRecord.status == 'Draft') {
                    errorMsgList.add(Label.OCOM_Order_In_Draft_Status_Invalid_For_Submission); 
                    customerOrderValidationError = true;
                }
                if (orderRecord.status == 'Submitted' || orderRecord.status == 'Processing' || orderRecord.status == 'In Progress' || orderRecord.status == 'Activated') {
                    errorMsgList.add(Label.OCOM_Order_has_already_been_submitted); 
                    customerOrderValidationError = true;
                }
                // check if configuration done
                if (orderRecord.CPQ_Configuration_Status__c != null && (!orderRecord.CPQ_Configuration_Status__c.equalsIgnoreCase(Label.CPQConfigureSuccessMessage))) {
                    errorMsgList.add(orderRecord.CPQ_Configuration_Status__c); 
                    customerOrderValidationError = true;
                }else if(orderRecord.CPQ_Configuration_Status__c == null){
                    errorMsgList.add(orderRecord.CPQ_Configuration_Status__c); 
                    customerOrderValidationError = true;
                }
                
               // if (!customerOrderValidationError) {
                    if(orderItemMap != null && orderItemMap.containsKey(orderRecord.Id)){
                        List<OrderItem> orderItems = orderItemMap.get(orderRecord.Id);
                        if(orderItems != null){
                            boolean amendError = false;
                            boolean missingBillingAddress = false;
                            boolean missingShippingAddress = false;
                            // check biiling address and shipping address
                            for(OrderItem item: orderItems) {
                                if (item.vlocity_cmt__BillingAccountId__c==null && item.vlocity_cmt__ParentItemId__c==null && item.PriceBookEntry.product2.IncludeQuoteContract__c==true) {
                                    missingBillingAddress = true;
                                    system.debug('OCOM_OrderSubmitController::validateCustomerOrder() itemId(' + item.Id + ') missing billing account');
                                }
                                if (item.PriceBookEntry.product2.OCOM_Shippable__c == 'Yes' && String.isBlank(item.Shipping_Address__c)) {
                                    missingShippingAddress = true;
                                    system.debug('OCOM_OrderSubmitController::validateCustomerOrder() itemId(' + item.Id + ') is missing shipping address');
                                }
                            }
                            if (missingBillingAddress) {   
                                errorMsgList.add(Label.OCOM_Billing_Account_Missing); 
                                customerOrderValidationError = true;
                            }
                            if (missingShippingAddress) {
                                errorMsgList.add(Label.OCOM_Shipping_Address_Missing); 
                                customerOrderValidationError = true;
                            }
                        }
                        // Check for WFM 
                        Set<String> overLineParentIdSet = OCOM_WFM_DueDate_Mgmt_Helper.getOverlineOffersIdSet(orderItems);
                        for(orderItem orditemObj: orderitems) {
                            if(orditemObj.pricebookentry.product2.OCOM_Bookable__c =='Yes' && !overLineParentIdSet.contains(orditemObj.vlocity_cmt__ParentItemId__c)){ 
                                if (orditemObj.Work_Order__c==null && orditemObj.vlocity_cmt__ProvisioningStatus__c != 'Deleted' && orditemObj.vlocity_cmt__ProvisioningStatus__c != 'Active') {      
                                    errorMsgList.add(Label.OCOM_PROVIDE_BOOKING_FOR + ' ' + orditemObj.PricebookEntry.Name + '.'); 
                                    customerOrderValidationError = true;
                                }
                            }
                            if (orditemObj.Scheduled_Due_Date__c != null) {
                                if (orditemObj.Scheduled_Due_Date__c <= system.now()) {
                                    errorMsgList.add(Label.OCOM_BOOKING_DATE_PASSED_START + ' ' + orditemObj.PricebookEntry.Name + ' ' + Label.OCOM_BOOKING_DATE_PASSED_END); 
                                    customerOrderValidationError = true;
                                }
                            }
                        }
                        // Credit Ceck
                        /*Map<String, Object> creditCheckValidationOutputMap = new Map<String, Object>();
                        if (creditCheckValidation(orderRecord, orderItems, creditCheckValidationOutputMap)) {
                            if(creditCheckValidationOutputMap.containsKey('validationFailureMessage') && creditCheckValidationOutputMap.get('validationFailureMessage') != null){
                                errorMsgList.add(string.valueOf(creditCheckValidationOutputMap.get('validationFailureMessage')));
                                customerOrderValidationError = true; 
                            }
                            if(creditCheckValidationOutputMap.containsKey('isOrderUpdateRequire') && creditCheckValidationOutputMap.get('isOrderUpdateRequire')==true){
                                orderListToUpdate.add(orderRecord);
                            }
                        }*/
                    }
                    if (orderRecord.requesteddate__c == null) {
                        errorMsgList.add(System.Label.Cancel_Order_Validation_Error_for_Requested_Date);
                        customerOrderValidationError = true;
                    } 
               // }// end if for customerOrderValidationError == false
                errorMessageMap.put(orderRecord.Id, errorMsgList);
                validationResult.put(masterId,errorMessageMap);
            }
            // Credit Ceck
            Map<String, Object> creditCheckValidationOutputMap = new Map<String, Object>();
            if (creditCheckValidation(masterOrder, orderItemList, creditCheckValidationOutputMap)) {
                if(creditCheckValidationOutputMap.containsKey('validationFailureMessage') && creditCheckValidationOutputMap.get('validationFailureMessage') != null){
                    List<String> masterErrorMsgList = new List<String>();
                    if(errorMessageMap.get(masterId)!=null){
                        masterErrorMsgList = errorMessageMap.get(masterId);
                    }
                    masterErrorMsgList.add(string.valueOf(creditCheckValidationOutputMap.get('validationFailureMessage')));
                    errorMessageMap.put(masterId, masterErrorMsgList);
                    validationResult.put(masterId,errorMessageMap);
                }
                if(creditCheckValidationOutputMap.containsKey('isOrderUpdateRequire') && creditCheckValidationOutputMap.get('isOrderUpdateRequire')==true){
                    orderListToUpdate.add(masterOrder);
                }
            }
            updateOrderCarNotRequired(orderListToUpdate);
        }
         return validationResult;
    }
    
    public static boolean creditCheckValidation(Order orderObj, List<OrderItem> orderItemList, Map<String, Object> outputMap) {
        Boolean isCarRequired = true;
        Boolean continueChecking = true;
        Boolean noCreditProfile = false;
        Boolean amendCar = false;
        Boolean isAmendOrder = false;
        Boolean isChangeOrder = false;
        Boolean validationError = false;
        Boolean existingCar = false;
        Boolean moreThan10PercentChange = false;
        Boolean hasContractableOffer = false;
        Decimal recurringTotal = 0.00;
        Decimal oneTimeTotal = 0.00;
        // BSBD_RTA-892
        Decimal recurringTotalWithTerm = 0.00;
        //Decimal oneTimeTotalWithTerm = 0.00;
        // End BSBD_RTA-892
        Decimal totalDiscount = 0.00;
        Integer newLineItemsCount = 0;
        String validationFailureMessage='';
        Boolean isUpdateOrder = false;
        
        if(orderObj!=null){
            system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() orderObj (' + orderObj + ')');
            Boolean proceed;
            Decimal totalAddAmount = 0.00;
            Decimal totalExistingAmount = 0.00;
            String creditStatus = orderObj.Credit_Assessment_Status__c;
            /*if(creditStatus != null && creditStatus.startsWithIgnoreCase('APPROVED')){
                return false;
            }*/
            for (OrderItem orderItemObj: orderItemList) {
                if (orderItemObj.amendStatus__c == 'Add' && orderItemObj.Credit_Assessment_Required__c != 'No') {
                    if (orderItemObj.vlocity_cmt__RecurringTotal__c != null)
                        totalAddAmount += orderItemObj.vlocity_cmt__RecurringTotal__c;
                    if (orderItemObj.vlocity_cmt__OneTimeTotal__c != null)
                        totalAddAmount += orderItemObj.vlocity_cmt__OneTimeTotal__c;
                } else if (orderItemObj.amendStatus__c != 'Add' && orderItemObj.Credit_Assessment_Required__c != 'No') {
                    if (orderItemObj.vlocity_cmt__RecurringTotal__c != null)
                        totalExistingAmount += orderItemObj.vlocity_cmt__RecurringTotal__c;
                    if (orderItemObj.vlocity_cmt__OneTimeTotal__c != null)
                        totalExistingAmount += orderItemObj.vlocity_cmt__OneTimeTotal__c;
                }
                if (orderItemObj.vlocity_cmt__ProvisioningStatus__c == 'New') {
                    newLineItemsCount++;
                }
               // check if line item should be removed/ignored/bypassed from credit assessment check
                proceed = true;
                if (orderItemObj.Credit_Assessment_Required__c != null) {
                    if (orderItemObj.Credit_Assessment_Required__c == 'No') {
                        proceed = false;
                    }
                }
                if (proceed == true) {
                    if (orderItemObj.vlocity_cmt__RecurringTotal__c != null) {
                        if (orderItemObj.vlocity_cmt__RecurringTotal__c > 0) {
                            // calculate MRC
                            recurringTotal += orderItemObj.vlocity_cmt__RecurringTotal__c;
                            // BSBD_RTA-892 - Mulitplied by term
                            try{
                                // Added vlocity_cmt__ProvisioningStatus__c condition as per BSBD_RTA-1269 
                                if(orderItemObj.vlocity_cmt__ProvisioningStatus__c == 'New'){
                                    if (orderItemObj.Term__c != null && Integer.valueOf(orderItemObj.Term__c) >0) {
                                        recurringTotalWithTerm += orderItemObj.vlocity_cmt__RecurringTotal__c * Integer.valueOf(orderItemObj.Term__c);
                                    }else{
                                        recurringTotalWithTerm += orderItemObj.vlocity_cmt__RecurringTotal__c;
                                    }
                                }
                            }Catch (Exception e){System.debug('Term is not a number' + orderItemObj.Term__c);}//end BSBD_RTA-892
                        } else {
                            // calculate total discount
                            totalDiscount += orderItemObj.vlocity_cmt__RecurringTotal__c;
                        }
                    }
                    if (orderItemObj.vlocity_cmt__OneTimeTotal__c != null) {
                        if (orderItemObj.vlocity_cmt__OneTimeTotal__c > 0) {
                            // calculate NRC
                            oneTimeTotal += orderItemObj.vlocity_cmt__OneTimeTotal__c;
                        } else {
                            // calculate total discount
                            totalDiscount += orderItemObj.vlocity_cmt__OneTimeTotal__c;
                        }
                    }
                }
            }
            system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() newLineItemsCount('+newLineItemsCount+')');

            // check if account has a credit profile
            if (orderObj.Account.Credit_Profile__c == null && !orderObj.Credit_Check_Not_Required__c) {
                noCreditProfile = true;
            }
            system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() noCreditProfile: ' + noCreditProfile);
            
            // check if order has an existing credit assessment
            
            system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() orderObj.Credit_Assessment_ID__c: ' + orderObj.Credit_Assessment_ID__c);
            if (orderObj.Credit_Assessment_ID__c != null) {
                existingCar = true;
            }
            system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() aCreditAssessment: ' + aCreditAssessment);
            
            //check if is an amend order
            system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() orderMgmtId_Status__c (' + orderObj.orderMgmtId_Status__c + ') and orderObj.status (' + orderObj.status + ')');
            if ((orderObj.orderMgmtId_Status__c == 'Processing' || orderObj.orderMgmtId_Status__c == 'Submitted' || orderObj.orderMgmtId_Status__c == 'Error') && (orderObj.status == 'Not Submitted')) {
                isAmendOrder = true;
            }
            system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() isAmendOrder: ' + isAmendOrder);
            
            //check if is change order
            system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() Type: ' + orderObj.Type);
            if (orderObj.Type == 'Change') {
                isChangeOrder = true;
            }
            system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() isChangeOrder: ' + isChangeOrder);
            
            //check if is move or disconnect order
            system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() Type: ' + orderObj.Type);
            if (orderObj.Type == 'Move' || orderObj.Type == 'Disconnect' || newLineItemsCount == 0) {
                isCarRequired = false;
                continueChecking = false;
                if (!orderObj.Credit_Check_Not_Required__c) {
                    isUpdateOrder = true;
                    orderObj.credit_Check_not_required__c = true;
                    /*if (!updateOrderCarNotRequired(orderObj, true)) {
                        system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() order update failed; CAR set to required');
                        isCarRequired = true;
                    }*/
                }
                system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() ' + orderObj.Type + ' order');
            }
            
            //check if there are contractable offers
           
            system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() numberOfContractableOffers('+numberOfContractableOffers+')');
            Boolean tlExists = false;
            if (continueChecking) {
                // check if credit profile has an external credit reference number (aka Credit Ref TL), updated via batch from the TELUS Credit Portal system
                system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() orderObj.AccountId: ' + orderObj.AccountId);
                system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() creditProfile: ' + creditProfile);
                tlExists = creditProfile.size() > 0;
                system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() tlExists: ' + tlExists);
                
                // CAR not required when MRC, NRC and promotions are $0
                system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() MRC('+recurringTotal+') NRC('+oneTimeTotal+') discount('+totalDiscount+ ')');
                if (numberOfContractableOffers<=0 && recurringTotal == 0 && oneTimeTotal == 0 && totalDiscount == 0) {
                    isCarRequired = false;
                    continueChecking = false;
                    system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() $0 MRC/NRC/promotions');
                    if (!orderObj.Credit_Check_Not_Required__c) {
                        isUpdateOrder = true;
                        orderObj.credit_Check_not_required__c = true;
                        /*if (!updateOrderCarNotRequired(orderObj, true)) {
                            system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() order update failed; CAR set to required');
                            isCarRequired = true;
                        }*/
                    }
                }
            }
            
            if (continueChecking && !isAmendOrder) {
                if (orderObj.AccountId != null) {
                    system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() account found');
                    // check if account is active
                     if (orderObj.Account.Account_Status__c == 'Active' && tlExists) {
                        system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() account status active and tlExists');
                        // check if CAR is required based on order MRC, NRC and discounts
                        if (recurringTotal > 200 || oneTimeTotal > 100 || totalDiscount > 100) {
                            system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() MRC/NRC/promotions CAR required');
                            isCarRequired = true;
                            continueChecking = false;
                        } else if (numberOfContractableOffers<=0) {
                            system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() MRC/NRC/promotions CAR not required');
                            isCarRequired = false;
                            if (!orderObj.Credit_Check_Not_Required__c) {
                                isUpdateOrder = true;
                                orderObj.credit_Check_not_required__c = true;
                                /*if (!updateOrderCarNotRequired(orderObj, true)) {
                                    system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() order update failed; CAR set to required');
                                    isCarRequired = true;
                                }*/
                            }
                        }
                    }
                }
            }
            
            // check if order total is greater than 10% of an existing CAR deal value
            system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() starting 10% check');
            if (aCreditAssessment == null && isAmendOrder) {
                // check for amend orders where original order did not require CAR
                system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() totalAddAmount ' + totalAddAmount);
                system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() totalExistingAmount ' + totalExistingAmount);
                if (totalAddAmount > (totalExistingAmount * 0.1) && (recurringTotal > 200 || oneTimeTotal > 100 || totalDiscount > 100)) {
                    amendCar = true;
                    system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() amend CAR for change order due to 10% change');
                } else {
                    isCarRequired = false;
                    system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() amend CAR not required due to less than 10% change');
                }
            } else if (aCreditAssessment != null) {
                // check for new or amend orders with existing CAR
                // Commented as per BSBD_RTA-892
                //if ((recurringTotal + oneTimeTotal) > (aCreditAssessment.Total_Deal_Value__c * 1.1)) {
                if ((recurringTotalWithTerm + oneTimeTotal + totalDiscount) > (aCreditAssessment.Total_Deal_Value__c * 1.1)) {
                    system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() MRC ('+recurringTotal+') NRC ('+oneTimeTotal+') total deal value ('+ aCreditAssessment.Total_Deal_Value__c+')');
                    amendCar = true;
                    moreThan10PercentChange = true;
                    validationFailureMessage += string.valueof(system.Label.OCOM_ChangesToOrderMoreThan10Percent)+' ';
                    validationError = true;
                    system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() amend CAR due to 10% change');
                }
            }
            
            // CAR required before order can be submitted
            system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() isCarRequired - ' + isCarRequired );
            system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() aCreditAssessment - ' + aCreditAssessment);
            if (isCarRequired && (aCreditAssessment == null)) {
                validationFailureMessage += string.valueof(system.Label.OCOM_Credit_Assessment_Required)+' ';
                validationError = true;
                system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() CAR is required before order can be submitted');
                if (orderObj.Credit_Check_Not_Required__c == true) {
                    isUpdateOrder = true;
                    orderObj.credit_Check_not_required__c = false;
                    /*if (!updateOrderCarNotRequired(orderObj, false)) {
                        //validationFailureMessage += string.valueof(system.Label.CAR_Unexpected_Error_Updating_Order)+' ';
                        //validationError = true;
                    }*/
                }
            }

            if (isCarRequired && aCreditAssessment != null && creditStatus != null) {
                if (creditStatus.equalsIgnoreCase('DRAFT')) {
                    // check if CAR status is Draft during order submission
                    validationFailureMessage += string.valueof(system.Label.OCOM_Cannot_Submit_Order_with_CAR_in_Draft_Status)+' ';
                    validationError = true;
                    system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() CAR cannot be in Draft status when order is submitted');
                }
            }
            if (aCreditAssessment != null && creditStatus != null) 
            {
                if(numberOfContractableOffers > 0 && !creditStatus.startsWithIgnoreCase('APPROVED'))
                {
                    validationFailureMessage += string.valueof(system.Label.OCOM_CAR_Approval_Required_for_Contractible_Order)+' ';
                    validationError = true;
                    system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation()  CAR for Contractible Order');
                }
            }
            // Added Credit_Check_Not_Required__c check as per BSBD_RTA-1166
            //if((!orderObj.Credit_Check_Not_Required__c) && orderObj.Credit_Assessment_ID__c != null && orderObj.Credit_Assessment_Status__c != 'Approved'){
            // Added creditStatus.containsIgnoreCase('Approved') check as per BSBD_RTA-1275
            if((!orderObj.Credit_Check_Not_Required__c) && orderObj.Credit_Assessment_ID__c != null 
               	&& String.IsNotBlank(creditStatus) && 
                (!creditStatus.containsIgnoreCase('Approved'))){
                    if(numberOfContractableOffers > 0){    // added as per BSBD_RTA-1275
                        system.debug('OC_ValidateCustomerOrder::creditCheckValidation() In.. '+numberOfContractableOffers);
                        isCarRequired = true;
                        validationError = true;
                        validationFailureMessage += string.valueof('CAR is required !!!');
                    }
            } // end BSBD_RTA-1166
            if (noCreditProfile) {
                validationFailureMessage += string.valueof(system.Label.Credit_Profile_Missing);
                validationError = true;
                system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() credit profile missing');
            }
        } else {
            validationError = true;
            validationFailureMessage += string.valueof(system.Label.Order_Not_Found);
            system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() order '+orderObj.Id+' not found');
        }
            
        outputMap.put('orderId', orderObj.Id);
        outputMap.put('validationError', validationError);
        outputMap.put('validationFailureMessage', validationFailureMessage);
        if (orderObj != null) {
            outputMap.put('isCarRequired', isCarRequired);
            outputMap.put('isOrderUpdateRequire', isUpdateOrder);
            outputMap.put('existingCar', existingCar);
            outputMap.put('noCreditProfile', noCreditProfile);
            outputMap.put('moreThan10PercentChange', moreThan10PercentChange);
            outputMap.put('accountId', orderObj.AccountId);
            outputMap.put('currentCARId', orderObj.Credit_Assessment_ID__c);
        }
        
        system.debug('OCOM_CreditAssessment_Helper::creditCheckValidation() outputMap(' + outputMap + ')');
        return validationError;
    }
    @TestVisible
    private static void updateOrderCarNotRequired(List<Order> orderList) {
        try {
            if(orderList != null && orderList.size() > 0){
                system.debug('OCOM_CreditAssessment_Helper::updateOrderCarNotRequired ');
                update orderList;
            }
        } catch(Exception e) {
            system.debug('OCOM_CreditAssessment_Helper::updateOrderCarNotRequired() exception' + e.getMessage());
        }
    }
}