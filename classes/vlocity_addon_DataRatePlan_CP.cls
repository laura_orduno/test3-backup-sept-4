global with sharing class vlocity_addon_DataRatePlan_CP implements vlocity_cmt.VlocityOpenInterface {

  public Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {
      list<string> queryFields = new list<string>{ 'Name', 'Monthly_Plan_Rate__c','RP_Included_Data__c','RP_Additional_Data__c','RP_Data_Roaming__c','Activation_Credit__c','Renewal_Credit__c','Conversion_Credit__c','RP_Terms_and_Conditions__c','RP_Region__c','Hardware_Discounts__c'};
      list<string> filters = new list<string>{'vlocity_cmt__ContractId__c=:contractId','Plan_Type__c'+'=\''+'Data Plan'+'\''};
      Boolean success = true;
      Map<id, List<Map<String, String>>> cliRowData = new Map<id, List<Map<String, String>>>(); 
      Map<string, List<Map<String, String>>> eachTableData = new Map<string, List<Map<String, String>>>(); 
      map<string,object> outputMap = new map<string,object>();
      string content = '';
      if(methodName == 'buildDocumentSectionContent')
      { 
          Id contractId = (Id) inputMap.get('contextObjId');
          inputMap.put('queryfields', queryFields);
          inputMap.put('filters',filters);
          map<string,object> mapTableInfo = vlocity_addon_DynamicTableHelper_CP.getVoiceDataRatePlan(inputMap);
          system.debug('mapTableInfo'+ mapTableInfo);          
          if(mapTableInfo.size() > 0 && !mapTableInfo.isEmpty()){
              cliRowData = (Map<id, List<Map<String, String>>>)	mapTableInfo.get('headerRowDataMap');
              for(id cliId: cliRowData.keyset() ){
                  list<map<string,string>> cliTableRows = cliRowData.get(cliID);
                  eachTableData.put('NA',cliTableRows);
                  mapTableInfo.put('tableRowsMap', eachTableData);
                  inputMap.putall(mapTableInfo);
                  outputMap =  vlocity_addon_DynamicTableHelper_CP.buildDocumentSectionContent(inputMap,outMap, options);
                  content += outputMap.get('sectionContent');
                  system.debug('content::: '+ content);
              }
              
              outMap.put('sectionContent',content);
              system.debug('::All Table Content::  ' + content);
          }
          else {
              return false;
          }
          
          
      }       
      return success; 
  }

}