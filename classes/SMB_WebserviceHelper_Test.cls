@isTest(SeeAllData=true)
private class SMB_WebserviceHelper_Test {

    static testMethod void Test_CancelWorkOrder() {
        smb_test_utility.createCustomSettingData();
        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'All Other Orders';
        insert testOpp_1;
        Set<Id> opptyIdSet = new Set<id>();
        opptyIdSet.add(testOpp_1.id);
        smb_test_utility.createPQLI('PQL_Test_Data',testOpp_1.Id);
        SMB_WebserviceHelper webserviceHelper = new SMB_WebserviceHelper();
       // String workorderId = smb_test_utility.createWorkOrder(Null,true).Id;
        Work_Order__c workOrder = new Work_Order__c(WFM_number__c='1232',Job_Type__c = 'Test' ,Install_Type__c = 'Test');
         insert workOrder;
        list<OpportunityLineItem> lstOLI = [select o.work_order__c,o.work_order__r.WFM_number__c, o.opportunityId From OpportunityLineItem o where OpportunityId =:testOpp_1.Id 
                  AND o.PricebookEntry.Product2.Product_Type__c in ('VOICE','INTERNET') limit 1];
        system.assert(lstOLI.size()>0);
        for(OpportunityLineItem oli:lstOLI){
            oli.work_order__c = workOrder.Id;
        }
        update lstOLI;
        setDummyResponse(testOpp_1.Id);
        Test.startTest();
       // SMB_WebserviceHelper.CancelWorkOrder(testOpp_1.Id);
       SMB_WebserviceHelper.CancelWorkOrder(new set<Id>{String.valueof( workOrder.Id)});
        SMB_WebserviceHelper.CancelWorkOrder(new set<Id>{String.valueof( testOpp_1.Id)},true);
        Apttus__APTS_Agreement__c contr= new Apttus__APTS_Agreement__c(Apttus__Status__c='qweq',Apttus__Status_Category__c='qweqw',Apttus__Related_Opportunity__c =testOpp_1.Id);
        insert contr;
        SMB_WebserviceHelper.UpdateContractsToCancelled(testOpp_1.Id);
            
        //SMB_WebserviceHelper.CancelWorkOrder(new set<Id>{workorderId});
        
        SMB_WebserviceHelper.CancelWorkOrder(opptyIdSet, true);
        Test.stopTest();
    }
    static testMethod void Test_sendWebserviceRequest() {
        Test.startTest();
        string xml = '<?xml version="1.0" encoding="UTF-8"?> <note><to> Tove</to><from>Jani</from><heading>Reminder</heading><body>Don\'t forget me this weekend!</body></note>';
        SMB_WebserviceHelper.sendWebserviceRequest('Test_endpoint', xml, 'Test_orderID', 'Test_soapAction', 'Test_operation');
        //SMB_WebserviceHelper.logWebserviceError(externalSystemName, webserviceName, objectName, request, response, errorMessage, apexClassAndMethod);
        //SMB_WebserviceHelper.UpdateContractsToCancelled(OppID);
        Test.stopTest();
    }
    static testMethod void Test_getLegacyOrder() {
        smb_test_utility.createCustomSettingData();
        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'All Other Orders';
        insert testOpp_1;
        smb_test_utility.createPQLI('PQL_Test_Data',testOpp_1.Id);
        setDummyResponse(testOpp_1.Id);
       
        SMB_WebserviceHelper webserviceHelper = new SMB_WebserviceHelper();
        Test.startTest();
         SMB_WebserviceHelper.getLegacyOrder('Test', 'Test', String.valueOf(testOpp_1.Id),null);
          SMB_WebserviceHelper.logWebserviceError('NC','SMB','Opportunity','test','test','test','test','test');
      
        Test.stopTest();
    }
 
   static testMethod void Test_getLegacyOrder1() {
        smb_test_utility.createCustomSettingData();
        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'All Other Orders';
        insert testOpp_1;
        
         smb_test_utility.createPQLI('PQL_Test_Data',testOpp_1.Id);
        setDummyResponse(testOpp_1.Id);
        SMB_WebserviceHelper webserviceHelper = new SMB_WebserviceHelper();
        Test.startTest();
        SMB_WebserviceHelper.getLegacyOrder('Test', 'Test', String.valueOf(testOpp_1.Id),'2');
        SMB_WebserviceHelper.logWebserviceError('NC','SMB','Opportunity','test','test','test','test','test');
        Test.stopTest();
    }
    
static testMethod void Test_OppLineItem() {
// ---------- BEGIN SET UP ---------- //
Test.startTest();
// Set up some local variables
String opportunityName = 'My Opportunity';
String standardPriceBookId = '';
 
PriceBook2 pb2Standard = [select Id from Pricebook2 where isStandard=true];
standardPriceBookId = pb2Standard.Id;
 
// set up opp and Verify that the results are as expected.
Opportunity o = new Opportunity(AccountId=null, Name=opportunityName, 
StageName='Prospecting', CloseDate=Date.today());
insert o;
Opportunity opp = [SELECT Name FROM Opportunity WHERE Id = :o.Id];
System.assertEquals(opportunityName, opp.Name);
 
// set up product2 and Verify that the results are as expected.
Product2 p2 = new Product2(Name='Test Product',Product_Type__c='VOICE',isActive=true);
insert p2;
Product2 p2ex = [SELECT Name FROM Product2 WHERE Id = :p2.Id];
System.assertEquals('Test Product', p2ex.Name);
 
// set up PricebookEntry and Verify that the results are as expected.
PricebookEntry pbe = new PricebookEntry(Pricebook2Id=standardPriceBookId, Product2Id=p2.Id, UnitPrice=99, isActive=true);
insert pbe;
PricebookEntry pbeex = [SELECT Pricebook2Id FROM PricebookEntry WHERE Id = :pbe.Id];
System.assertEquals(standardPriceBookId, pbeex.Pricebook2Id);
 Work_Order__c workOrder = new Work_Order__c(WFM_number__c='1232',Job_Type__c = 'Test' ,Install_Type__c = 'Test');
         insert workOrder;
// set up OpportunityLineItem and Verify that the results are as expected.
OpportunityLineItem oli = new OpportunityLineItem(work_order__c=workOrder.id,Element_Type__c='uyio',PriceBookEntryId=pbe.Id, OpportunityId=o.Id, Quantity=1, TotalPrice=99);
insert oli;
OpportunityLineItem oliex = [SELECT PriceBookEntryId FROM OpportunityLineItem WHERE Id = :oli.Id];
System.assertEquals(pbe.Id, oliex.PriceBookEntryId); 
Order_Request__c oreq= new Order_Request__c(Order__c='Cancellednot', Opportunity__c=opp.Id,Operation_Failed_Status__c='');
      insert oreq; 
        
        Test.stopTest();
        setDummyResponse(opp.Id);
        SMB_WebserviceHelper.CancelWorkOrder(new set<Id>{String.valueof(opp.Id)},true);
        SMB_WebserviceHelper.CancelWorkOrder(opp.id);
    }
    static testMethod void Test_OppLineItem1() {
// ---------- BEGIN SET UP ---------- //
Test.startTest();
// Set up some local variables
String opportunityName = 'My Opportunity';
String standardPriceBookId = '';
 
PriceBook2 pb2Standard = [select Id from Pricebook2 where isStandard=true];
standardPriceBookId = pb2Standard.Id;
 
// set up opp and Verify that the results are as expected.
Opportunity o = new Opportunity(AccountId=null, Name=opportunityName, 
StageName='Prospecting', CloseDate=Date.today());
insert o;
Opportunity opp = [SELECT Name FROM Opportunity WHERE Id = :o.Id];
System.assertEquals(opportunityName, opp.Name);
 
// set up product2 and Verify that the results are as expected.
Product2 p2 = new Product2(Name='Test Product',Product_Type__c='VOICE',isActive=true);
insert p2;
Product2 p2ex = [SELECT Name FROM Product2 WHERE Id = :p2.Id];
System.assertEquals('Test Product', p2ex.Name);
 
// set up PricebookEntry and Verify that the results are as expected.
PricebookEntry pbe = new PricebookEntry(Pricebook2Id=standardPriceBookId, Product2Id=p2.Id, UnitPrice=99, isActive=true);
insert pbe;
PricebookEntry pbeex = [SELECT Pricebook2Id FROM PricebookEntry WHERE Id = :pbe.Id];
System.assertEquals(standardPriceBookId, pbeex.Pricebook2Id);
 Work_Order__c workOrder = new Work_Order__c(WFM_number__c='1232',Job_Type__c = 'Test' ,Install_Type__c = 'Test');
         insert workOrder;
// set up OpportunityLineItem and Verify that the results are as expected.
OpportunityLineItem oli = new OpportunityLineItem(work_order__c=workOrder.id,Element_Type__c='uyio',PriceBookEntryId=pbe.Id, OpportunityId=o.Id, Quantity=1, TotalPrice=99);
insert oli;
OpportunityLineItem oliex = [SELECT PriceBookEntryId FROM OpportunityLineItem WHERE Id = :oli.Id];
System.assertEquals(pbe.Id, oliex.PriceBookEntryId); 
Order_Request__c oreq= new Order_Request__c(Order__c='Cancellednot', Opportunity__c=opp.Id,Operation_Failed_Status__c='');
      insert oreq;        
    Test.stopTest();    
        setDummyResponse(opp.Id);
        SMB_WebserviceHelper.CancelWorkOrder(opp.id);
    }
 
    
        static testMethod void Test_OrderWorkOrder() {
        
        Schema.DescribeSObjectResult d = Schema.SObjectType.Account; 
          Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
          Schema.RecordTypeInfo rtByName =  rtMapByName.get('CAN');
          system.debug(' id'+rtByName.getRecordTypeId());
          Id recTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Billing').getRecordTypeId();
          Id recSerTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
          Id recRCIDTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId();
        
          Test.startTest();
          Account RCIDacc = new Account(name='debs',recordtypeid=recRCIDTypeId);
          Account acc = new Account(name='Billing account',BAN_CAN__c='draft',recordtypeid=recTypeId);
          Account seracc = new Account(name='123 street',recordtypeid=recSerTypeId,parentid=RCIDacc.id);
          
          
          List<Account> acclist= new List<Account>();          
          acclist.add(RCIDacc);
          acclist.add(acc);
          acclist.add(seracc );
          insert acclist;
          
          smbcare_address__c address= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N5',FMS_Address_ID__c='987651',account__c=seracc.id); 
         insert address;
          
           // First, set up test price book entries.
        // Insert a test product.
        Product2 prod = new Product2(Name = 'Laptop X200', 
            Family = 'Hardware');
        insert prod;
         System.assertEquals(prod.name, 'Laptop X200');
        // Get standard price book ID.
        // This is available irrespective of the state of SeeAllData.
        Id pricebookId = Test.getStandardPricebookId();
        
        // 1. Insert a price book entry for the standard price book.
        // Standard price book entries require the standard price book ID we got earlier.
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
       
        // Create a custom price book
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        // 2. Insert a price book entry with a custom price.
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        contact c= new contact(firstname='abc',lastname='xyz');
        insert c;
           // Pricebook2Id=customPB.id;
         order ord = new order(Ban__c='draft',accountId=acc.id,effectivedate=system.today(),status='not submitted',Pricebook2Id=customPB.id,CustomerAuthorizedById=c.id,service_address__c=address.id);   
         order ord2 = new order(Ban__c='draft',accountId=acc.id,effectivedate=system.today(),status='not submitted',Pricebook2Id=customPB.id,CustomerAuthorizedById=c.id,service_address__c=address.id);   
         List<Order>ordlist= new List<Order>();   
         ordList.add(ord);ordList.add(ord2);
          insert ordList; 
          System.assertEquals(ordList.size(), 2);
             
         Work_Order__c workOrder = new Work_Order__c(WFM_number__c='1232',Job_Type__c = 'Test' ,Install_Type__c = 'Test');
         insert workOrder;
         
          orderItem  OLI2= new orderItem(work_order__c = workorder.Id,UnitPrice=12,orderId= ord2.Id,orderMgmt_BPI_Id__c='9871379163517',vlocity_cmt__BillingAccountId__c=acc.id,vlocity_cmt__ServiceAccountId__c=seracc.id,Quantity=2,PricebookEntryId=customPrice.id);
          orderItem  OLI= new orderItem(UnitPrice=12,orderId= ord.Id,vlocity_cmt__AssetReferenceId__c=OLI2.id,vlocity_cmt__BillingAccountId__c=acc.id,vlocity_cmt__ServiceAccountId__c=seracc.id,Quantity=2,PricebookEntryId=customPrice.id);
          
          List<OrderItem>OLIlist= new List<OrderItem>();   
          OLIList.add(OLI2);OLIList.add(OLI);
          insert OLIList;
        System.assertEquals(OLIList.size(), 2);
        
          system.debug('Id  aAS'+String.valueOf(OLI.id).substring(0,15)+' '+OLI.id);
           Asset testassetRec = new Asset(name='Asset1',accountId=RCIDacc.id,vlocity_cmt__AssetReferenceId__c=String.valueOf(OLI.id).substring(0,15),vlocity_cmt__BillingAccountId__c=null,orderMgmtId__c=null,vlocity_cmt__OrderId__c=null,vlocity_cmt__OrderProductId__c=null,vlocity_cmt__ServiceAccountId__c=null);
        insert testassetRec;
        
        System.assertEquals(testassetRec.name, 'Asset1');
         setDummyResponse(ord2.Id); 
        SMB_WebserviceHelper.OCOM_CancelWorkOrder(ord2.Id);
        
         SMB_WebserviceHelper.orderWrapper po=  new SMB_WebserviceHelper.orderWrapper();
            po.orderRequestedDate=system.today();
            po.facilityTrackingUsso='test';
            po.facilityTrackingCircuit='test';
           Test.stopTest();
    }
    static void setDummyResponse(String oppId){
        map<string,object> responseMap = new map<string,object>();
        smb_orders_request_response_v4.getOrderListByRCIDResponse_element response_x = new smb_orders_request_response_v4.getOrderListByRCIDResponse_element();
        response_x.summaryRequestList  = new list<smb_orders_trackingtypes_v4.OrderRequest>();
        smb_orders_trackingtypes_v4.OrderRequest orderRequest = new smb_orders_trackingtypes_v4.OrderRequest();
        orderRequest.orderKey = new smb_orders_trackingtypes_v4.OrderRequestKey();
        orderRequest.orderKey.legacyOrderId=oppId;
         orderRequest.orderKey.sequenceNum='2';
        orderRequest.orderStatusSet = new smb_orders_trackingtypes_v4.OrderRequestStatus();
        orderRequest.orderStatusSet.orderStatus = 'Test Status';
        orderRequest.orderDueDateSet = new smb_orders_trackingtypes_v4.DueDate();
        orderRequest.orderDueDateSet.dueDate = system.today();
        orderRequest.orderAddress = new list<smb_orders_trackingtypes_v4.OrderRequestAddress>();
        smb_orders_trackingtypes_v4.OrderRequestAddress OrderRequestAddress = new smb_orders_trackingtypes_v4.OrderRequestAddress();
        OrderRequestAddress.address = 'Test';
        orderRequest.orderAddress.add(OrderRequestAddress);
        orderRequest.orderIdSet = new smb_orders_trackingtypes_v4.OrderRequestId();
        orderRequest.orderTypeSet = new smb_orders_trackingtypes_v4.OrderRequestType();
        orderRequest.orderTypeSet.orderServiceType = 'Test orderServiceType';
        orderRequest.orderTypeSet.dataOrderType = 'Test dataOrderType';
        orderRequest.orderTypeSet.legacyOrderType = 'Test legacyOrderType';
        orderRequest.orderTypeSet.orderType = 'Test orderType';
        orderRequest.orderTypeSet.voiceOrderType = 'Test voiceOrderType';
        orderRequest.customer = new smb_orders_trackingtypes_v4.Customer();
        orderRequest.orderCompletedDateSet = new smb_orders_trackingtypes_v4.OrderCompletedDate();
        orderRequest.remarkList = new smb_orders_trackingtypes_v4.RemarkList();
        orderRequest.outOfBandInfo = new smb_orders_trackingtypes_v4.OutOfBand();
        orderRequest.orderAttributeList = new smb_orders_trackingtypes_v4.Attribute();
        response_x.summaryRequestList.add(orderRequest);
        SMB_AssignmentWFM_ServiceReqRes_v2.CancelWorkOrderResponse response_y = new SMB_AssignmentWFM_ServiceReqRes_v2.CancelWorkOrderResponse();
        response_y.workOrderId = oppId;
        responseMap.put('SMB_AssignmentWFM_ServiceReqRes_v2.CancelWorkOrderResponse',response_y);
        responseMap.put('smb_orders_request_response_v4.getOrderListByRCIDResponse_element',response_x);
        GenericWebserviceMock genericWebserviceMock = new GenericWebserviceMock(responseMap);
        Test.setMock(WebServiceMock.class, genericWebserviceMock);
    } 
    
    static testMethod void testcase_1()
    {
         Test.startTest();

        TestDataHelper.testAccountCreation();
        TestDataHelper.testContactCreation();

        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'All Other Orders';
        insert testOpp_1;

        Contract testContract_1  = new Contract(Contract_Type__c='Contract Sent',AccountId=TestDataHelper.testAccountObj.Id,ContractTerm=12,StartDate=system.today(),vlocity_cmt__OpportunityId__c = testOpp_1.Id,status='Draft');
        insert testContract_1;

        Blob b = Blob.valueOf('Test Data');  
        Attachment attachment = new Attachment();  
        attachment.ParentId = testContract_1.Id;  
        attachment.Name = 'Test Attachment for Parent';  
        attachment.Body = b;  
        insert(attachment);  
    
        vlocity_cmt__ContractVersion__c testContractVersion = new vlocity_cmt__ContractVersion__c(Name='Test Version 1', vlocity_cmt__ContractId__c = testContract_1.Id);
        insert testContractVersion;
        
        SMB_WebserviceHelper.UpdateContractsToCancelled_vlocity(testOpp_1.id);
        
        SMB_WebserviceHelper.logWebserviceError('Aptus', 'test', 'Opportunity', 'Test', 'Test', 'Test', 'Test', 'Test');
                Test.stopTest();

    }
    
}