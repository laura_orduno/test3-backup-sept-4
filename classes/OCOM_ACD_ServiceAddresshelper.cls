public class OCOM_ACD_ServiceAddresshelper {
public Integer ActiveAssetCount = 0;
    public void PopulateServiceAddressOnOrder(string OrderId,List<orderItem> orderItems){
      Order theOrder = [SELECT Id, Service_Address__c,ServiceAccountId__c,AccountId  FROM Order WHERE Id=: OrderId Limit 1];
         If(theOrder != null ){
            If(theOrder.AccountId != null){
                If(theOrder.Service_Address__c == null ){
                    //String ServiceAccountId = theOrder.AccountId;
                    String ServiceAccountId = theOrder.ServiceAccountId__c;
                    SMBCare_Address__c ServiceAddress = [select Id,Name,Account__c,FMS_Address_ID__c,Province__c from SMBCare_Address__c where Service_Account_Id__c =: ServiceAccountId lIMIT 1];
                    If(ServiceAddress != null){
                        String SmbcareId = ServiceAddress.Id;
                        If(ServiceAccountId != null & SmbcareId != null){
                               theOrder.Service_Address__c = SmbcareId;
                               theOrder.ServiceAccountId__c = ServiceAccountId;
                               update theOrder;
                        }
                      
                         List<Orderitem> OLIlisttobeupdated = new List<orderItem>();
                         for(OrderItem o : orderItems){
                               o.vlocity_cmt__ServiceAccountId__c = ServiceAccountId;
                               o.Billing_Account2__c = o.vlocity_cmt__BillingAccountId__c;
                               o.Billing_Account__c = RetriveBANCAN(o.vlocity_cmt__BillingAccountId__c);
                               o.Billing_Address__c = RetriveBillingAddress(o.vlocity_cmt__BillingAccountId__c);
                           
                               OLIlisttobeupdated.Add(o);
                           }
                           update OLIlisttobeupdated;
                           System.debug('List of OLI--->'+OLIlisttobeupdated);
                           If(SmbcareId != null){
                               ProductAvailiblity(SmbcareId);
                           }
                           If(ServiceAddress.FMS_Address_ID__c != null && ServiceAddress.Province__c != null){
                           String regionId =  ServiceAddress.Province__c;
                           String addressId = ServiceAddress.FMS_Address_ID__c;
                                updateServiceAddDetailsFMS(addressId,regionId,SmbcareId);
                           }   
                    }
             }
              
          }    
       }
   }
    Public void updateServiceAddDetailsFMS(String addressId, String regionId,String SmbcareId){
        if(!Test.isRunningTest()){
            OCOM_InOrderFlowPACController.FutureCallServiceAddDetailsFMS(addressId,regionId,SmbcareId);
        }
    }
    Public Boolean CheckIfAnylineItemsAreActiveAsset(String OrderId){
    Boolean IsACDOrder = false;
     List<OrderItem> orderItemsfromOrder =[SELECT Id,vlocity_cmt__ProvisioningStatus__c,vlocity_cmt__ServiceAccountId__c,vlocity_cmt__BillingAccountId__c from OrderItem where OrderId =: OrderId];
        If(orderItemsfromOrder.size() > 0 && orderItemsfromOrder != null){
            For(OrderItem o : orderItemsfromOrder){
                If(o.vlocity_cmt__ProvisioningStatus__c == 'Active'){
                    ActiveAssetCount++;
                }
             }
                
            If(ActiveAssetCount > 0){
                    PopulateServiceAddressOnOrder(OrderId,orderItemsfromOrder);
                    return True;
                 }
            else{
                return false;
            }
            }
        else{
                return false;
            }
        }
     @future
     public Static void ProductAvailiblity(String smbCareAddrId)
    {
        try{
        OrdrTechnicalAvailabilityManager.checkProductTechnicalAvailability(smbCareAddrId);      
        }
        catch(exception e){
             
        }
    }
    
    
 Public String RetriveBillingAddress(Id billingAccountId){ 
     If(billingAccountId != null){
     List<Account> accountList = [SELECT Id, Ban_Can__c, Billing_System__c, Billingstreet, BillingCity, BillingState, BillingPostalCode, BillingCountry
                                             FROM Account 
                                             WHERE id=:billingAccountId Limit 1];
                              String BillAddr='';
                              if(accountList[0].Billingstreet != null)
                                 BillAddr=BillAddr+' '+accountList[0].Billingstreet;
                              if(accountList[0].BillingCity != null)
                                 BillAddr=BillAddr+' '+accountList[0].BillingCity;
                              if(accountList[0].BillingState != null)
                                 BillAddr=BillAddr+' '+accountList[0].BillingState;
                              if(accountList[0].BillingPostalCode != null)
                                 BillAddr=BillAddr+' '+accountList[0].BillingPostalCode;
                              if(accountList[0].BillingCountry!= null)
                                 BillAddr=BillAddr+' '+accountList[0].BillingCountry;
     return BillAddr;
    }
  else{
      return null;
  }
 }
  Public String RetriveBANCAN(String billingAccountId){
      If(billingAccountId != null){
      
          String BANCAN;
          Account billingAccount = [Select Id,BAN_CAN__c from Account where Id =: billingAccountId Limit 1];
          BANCAN = billingAccount.BAN_CAN__c;
          return BANCAN;
      
      }
      else{
          return null;
      }
  }
}