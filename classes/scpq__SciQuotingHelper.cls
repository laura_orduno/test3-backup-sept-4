/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class SciQuotingHelper {
    global SciQuotingHelper() {

    }
    webService static List<SObject> getOpportunityAccountPrimaryContact(Id opportunityId, List<String> opportunityFields, List<String> accountFields, List<String> primaryContactFields) {
        return null;
    }
    webService static scpq__SciQuote__c upsertQuoteAndPrimaryQuoteLines(scpq__SciQuote__c sciQuote, Boolean applyUpdateEvenIfTimestampUnchanged, List<scpq__PrimaryQuoteLine__c> quoteLines) {
        return null;
    }
    webService static scpq__SciQuote__c upsertQuote(scpq__SciQuote__c sciQuote, List<OpportunityLineItem> quoteLines) {
        return null;
    }
}
