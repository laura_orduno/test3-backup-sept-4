/**
* IMPORTANT NOTE: This logic assumed there is only one contract per account.
*/
global class ContractSubscriberEventBatch implements database.batchable<sobject>{
    global string query{get;set;}
    global ContractSubscriberEventBatch(){
        //query='select id,ban__c,contract__c from agreement_subscriber_event__c where contract__c = \'800230000000l1v\'';
        query='select id,ban__c,contract__c from agreement_subscriber_event__c where ban__c!=null and contract__c=null';
    }
    global database.querylocator start(database.batchablecontext bc) {
        return database.getquerylocator(query);
    }
    global void execute(database.batchablecontext bc,list<sobject> scope) {
        set<String> accountBans=new set<String>();
        map<string,id> accountCanMap=new map<string,id>();
        map<string,id> canAccountMap=new map<string,id>();
        //map<string,id> agreeAccMap=new map<string,id>();
        for(sobject obj:scope){
            agreement_subscriber_event__c event=(agreement_subscriber_event__c)obj;
            accountBans.add(event.ban__c);
        }
        list<account> accountList=[select id,can__c,parentid from account where can__c=:accountBans limit 50000];
        system.debug('Done Query on accountList');
        //String acctCanString = ' ';
        for(account account:accountList){
            accountCanMap.put(account.can__c, account.parentid);
            canAccountMap.put(account.can__c, account.id);
            //acctCanString += account.can__c + ':';
        }
        //system.debug('account CAN list:'+acctCanString);
        //system.debug('canAccountMap: '+canAccountMap);
                
        list<contract> contractList=[select id,accountid from contract where accountid=:accountCanMap.values() and recordtype.developername='Corporate_Wireless' and status='Contract Registered' limit 50000]; //and status='Contract Registered' (id ='800230000000lgo' or id ='800230000000lgtAAA')

        processAgreementSubscriberEvents(contractList,accountCanMap,canAccountMap,scope);
    }
    
    global void processAgreementSubscriberEvents(list<contract> contractList,
                                                 map<string,id> accountCanMap,
                                                 map<string,id> canAccountMap,
                                                 list<sobject> scope) {
        id accountId=null;
        //String contractIdString = '';
        map<id,id> accountContractMap=new map<id,id>();
        list<agreement_subscriber_event__c> updateEvents=new list<agreement_subscriber_event__c>();
        string A = 'A';
        string C = 'C'; 
        string R = 'R'; 
        string S = 'S';

        for(contract contract:contractList){
            accountContractMap.put(contract.accountid,contract.id);
            //contractIdString += contract.id + ':';
        }
        //system.debug('contract ID list:'+contractIdString);
        for(sobject obj:scope){
            agreement_subscriber_event__c event=(agreement_subscriber_event__c)obj;
            accountId=canAccountMap.get(event.ban__c);
            if(string.isnotblank(accountId)){
                if(accountContractMap.containskey(accountId)){
                    event.contract__c=accountContractMap.get(accountId);
                    updateEvents.add(event);
                }
            }
        }
        if(!updateEvents.isempty()){
            update updateEvents;
        }
        String[] supportedRecTypes = new String[]{'Fixed Amount For Contract Term','Percentage Of Contract Minimum Commitment'};
        //system.debug('accountContractMap:'+accountContractMap);  
        
            Map<id,contract> agrwithChurnAndHardware = new Map<ID, contract>([select id, name,
                                                                              (select id from Agreement_Churn_Allotments__r where RecordType.Name in :supportedRecTypes order by createdDate Limit 1),
                                                                              (select id from Agreement_Hardware_Float_Allotments__r where RecordType.Name in :supportedRecTypes order by createdDate Limit 1)
                                                                              from contract where id in :accountContractMap.values()]);
        
        String[] alltRecTypeswithDateRange = new String[]{'Dynamic Churn Allowance Per Year','Dynamic Hardware Allowance Per Year','Percentage Of Amount For Specified Period','Specified Amount For a Specific Period'};
            
            Map<id,contract> agrwithChurnAndHardwareForDateRange = new Map<ID, contract>([select id, name,
                                                                                          (select id, contract__c,Start_date__c, End_Date__c from Agreement_Churn_Allotments__r where RecordType.Name in :alltRecTypeswithDateRange),
                                                                                          (select id, contract__c,Start_date__c, End_Date__c from Agreement_Hardware_Float_Allotments__r where RecordType.Name in :alltRecTypeswithDateRange) 
                                                                                          from contract where id in :accountContractMap.values()]);
        
        System.debug('agrwithChurnAndHardwareForDateRange' + agrwithChurnAndHardwareForDateRange);
        //get list of events with the contracts marked against the current list
        List<AggregateResult> consResults = [select contract__c, Subscriber_Status__c , count(id) total from Agreement_Subscriber_Event__c 
                                             where contract__c=:accountContractMap.values() and Subscriber_Status_Reason_Code__c Not In ('DEMO','SEAS','AIE','MTMD') 
                                             group by contract__c,Subscriber_Status__c ];
        System.debug(LoggingLevel.INFO,'consResult' + consResults);
        
        Map<id,Map<String,Integer>> consResultMap = new Map<id,Map<String,Integer>>();
        
        for(AggregateResult conAGResult : consResults){
            Id agId = (id) conAGResult.get('contract__c');
            if(consResultMap.containsKey(agId)){
                consResultMap.get(agId).put((String)conAGResult.get('Subscriber_Status__c'),(Integer)conAGResult.get('total'));
            }else{
                Map<String,Integer> StatusMap = new Map<String,Integer>();
                StatusMap.put((String)conAGResult.get('Subscriber_Status__c'),(Integer)conAGResult.get('total'));
                consResultMap.put(agId,StatusMap); 
            }
        }         
        
        System.debug('agrwithChurnAndHardware' + agrwithChurnAndHardware);
        system.debug('consResultMap:'+consResultMap);
        Set<contract> toUPdate = new Set<contract>();  
        List<Sobject> toupdateChurn = new List<Sobject>();        
        for(String agid : accountContractMap.values()){
            contract ag = new contract(id=agid);
            Integer cancel = 0;
            Integer suspended = 0;
            Integer totalActive = 0;
            Integer totalCancelandSuspended = 0; 
            //system.debug('ag.id:'+ag.id);
            if (consResultMap.size()>0 && consResultMap.get(ag.id) <> null)
            {
            	cancel = consResultMap.get(ag.id).get(C) == null ? 0: consResultMap.get(ag.id).get(C); 
            	suspended = consResultMap.get(ag.id).get(S) == null ? 0: consResultMap.get(ag.id).get(S);
            	totalActive = consResultMap.get(ag.id).get(A) == null ? 0: consResultMap.get(ag.id).get(A);
            }            
            totalCancelandSuspended = cancel + suspended;
            
            
            ag.Active_Units__c = totalActive - totalCancelandSuspended;
            
            toUPdate.add(ag);
        }
        for(contract ag : agrwithChurnAndHardware.values()){
            List<Agreement_Churn_Allotment__c> churnRecs = ag.Agreement_Churn_Allotments__r;
            List<Agreement_Hardware_Allotment__c> hardwareRecs = ag.Agreement_Hardware_Float_Allotments__r;
            if(churnRecs.size()>0){
                String churnRecKey = churnRecs.get(0).id;
                Integer cancel = 0;
                Integer suspended = 0;
                if (consResultMap.size()>0 && consResultMap.get(ag.id) <> null)
                {
                	cancel = consResultMap.get(ag.id).get(C) == null ? 0: consResultMap.get(ag.id).get(C); 
                	suspended = consResultMap.get(ag.id).get(S) == null ? 0: consResultMap.get(ag.id).get(S);
                }
                Integer totalUsed = cancel+suspended;
                Agreement_Churn_Allotment__c chruRec = new Agreement_Churn_Allotment__c(id = churnRecKey  ,Churn_Used__c = totalUsed);
                toupdateChurn.add(chruRec);
            }
            if(hardwareRecs.size()>0){
                String hardwareRecKey = hardwareRecs.get(0).id;
                Integer renewal = 0;
                if (consResultMap.size()>0 && consResultMap.get(ag.id) <> null)
                {
                	renewal = consResultMap.get(ag.id).get(R) == null ? 0: consResultMap.get(ag.id).get(R); 
                }
                Agreement_Hardware_Allotment__c hardRec = new Agreement_Hardware_Allotment__c(id = hardwareRecKey  ,Hardware_Used__c = renewal);
                toupdateChurn.add(hardRec);
            }
            //System.debug(LoggingLevel.INFO,'toupdateChurn' + toupdateChurn);
        }
        List <contract> agToUpdate  = new List <contract>();
        agToUpdate.addAll(toUPdate);
        update agToUpdate;
        update toupdateChurn;
        
        Map<Id,List<Agreement_Subscriber_Event__c>> eventMap=new Map<Id,List<Agreement_Subscriber_Event__c>>();
        List<Agreement_Subscriber_Event__c> events = [select id, Contract__c, Subscriber_Status__c,Subscriber_Status_Reason_Code__c, Effective_Date__c from Agreement_Subscriber_Event__c
                                                      where Contract__c=:agrwithChurnAndHardwareForDateRange.keySet() and Subscriber_Status_Reason_Code__c Not in ('DEMO','SEAS','AIE','MTMD') ];
        system.debug('Line 155 events:'+ events);
        system.debug('line 156 agrwithChurnAndHardwareForDateRange.keySet()'+agrwithChurnAndHardwareForDateRange.keySet());
        for(Agreement_Subscriber_Event__c ev : events){
            if(eventMap.containsKey(ev.Contract__c)){
                eventMap.get(ev.Contract__c).add(ev);
            }else{
                List<Agreement_Subscriber_Event__c> evList = new List<Agreement_Subscriber_Event__c>();
                evList.add(ev);
                eventMap.put(ev.Contract__c,evList);
            }
        }
        List<sObject> toudpateList = new List<sObject>();
        for(contract ag : agrwithChurnAndHardwareForDateRange.values()){
            List<Agreement_Churn_Allotment__c> churnRecs = ag.Agreement_Churn_Allotments__r;
            List<Agreement_Hardware_Allotment__c> hardwareRecs = ag.Agreement_Hardware_Float_Allotments__r;
            for(Agreement_Hardware_Allotment__c hardware: hardwareRecs){
                Integer hardwareUsed = 0;
                if (eventMap.size()>0 && eventMap.get(ag.id) <> null)
                {
                    for(Agreement_Subscriber_Event__c event:eventMap.get(ag.id)){
                        if(hardware.Contract__c==event.Contract__c
                           && hardware.Start_Date__c<event.Effective_Date__c
                           && hardware.End_Date__c>event.Effective_Date__c
                           && R.equalsignorecase(event.Subscriber_Status__c)  
                          ) {
                              hardwareUsed++;
                          }
                    }                    
                }
                hardware.Hardware_Used__c = hardwareUsed;
            }
            
            for(Agreement_Churn_Allotment__c churn : churnRecs){
                Integer churnUsed = 0;
                //system.debug('line 184 eventMap:'+eventMap);
                //system.debug('line 187 ag:'+ag);
                if (eventMap.size()>0 && eventMap.get(ag.id) <> null)
                {
                    for(Agreement_Subscriber_Event__c event:eventMap.get(ag.id)){
                        if(churn.Contract__c==event.Contract__c
                           && churn.Start_Date__c < event.Effective_Date__c
                           && churn.End_Date__c > event.Effective_Date__c
                           && !(('DEMO'.equalsignorecase(event.Subscriber_Status_Reason_Code__c)) || ('SEAS'.equalsignorecase(event.Subscriber_Status_Reason_Code__c)) || ('AIE'.equalsignorecase(event.Subscriber_Status_Reason_Code__c)) ||('MTMD'.equalsignorecase(event.Subscriber_Status_Reason_Code__c)) )
                           && (C.equalsignorecase(event.Subscriber_Status__c) || S.equalsignorecase(event.Subscriber_Status__c)) 
                          ) {
                              churnUsed++;
                              
                          }
                    }
                }
                churn.Churn_Used__c = churnUsed;
                //system.debug('Line 200 Churn for date range:'+churn);
            }    
            
            toudpateList.addAll((List<sobject>) hardwareRecs);
            toudpateList.addAll((List<sobject>) churnRecs);
            update toudpateList; 
        }
    }
    global void finish(database.batchablecontext bc) {}
}