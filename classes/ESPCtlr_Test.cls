/**
 *	@author Christian Wico - cwico@tractionondemand.com
 *	@description ESPCtlr Controller unit test
 */

@isTest
private class ESPCtlr_Test {
	
        @isTest static void testController() {
		
        	Account a = new Account(Name = 'Traction Test Account');
                insert a;
                
                ESP__c esp = new ESP__c(Name = 'Traction Test ESP', Account__c = a.Id);
                insert esp;

                PageReference p = Page.ESP;
                p.getParameters().put('id', esp.Id);
                Test.setCurrentPageReference(p);

                ESPCtlr controller = new ESPCtlr();
                
                System.assert(controller.esp != null);
                System.assert(controller.acc != null);
                System.assert(controller.getSolutionJSON() != null);
                System.assert(controller.clearMap() == null);
	}
	


}