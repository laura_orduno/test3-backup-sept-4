/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 * Name - ServiceRequestDetailController_test
 * 1. Created By - Sandip - 06 Feb 2016
 * 2. Modified By 
 */
@isTest
private class ServiceRequestDetailController_test{
    static testMethod void runTestCase() {
        test.startTest();

        Service_Request__c sr = new Service_Request__c(Billing_Address__c = '703 carabob, ON, CAN, M1T 3N3');
        insert sr;
       
        PageReference page = new PageReference('/apex/ServiceRequestDetail'); 
        page.getParameters().put('Id', sr.Id);
        Test.setCurrentPage(page);
        ApexPages.standardController controller = new ApexPages.standardController(sr);
        ServiceRequestDetailController obj = new ServiceRequestDetailController(controller);
        obj.updateBillingAddress();
        test.stopTest();             
    }
    
}