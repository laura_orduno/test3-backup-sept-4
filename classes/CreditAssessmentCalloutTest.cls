/*
    *******************************************************************************************************************************
    Class Name:     CreditAssessmentCalloutHelper 
    Purpose:        Helper class       
    Updated by:     Aditya Jamwal           16-11-2107  QC- 9728,9717
  *******************************************************************************************************************************
*/
@isTest
public class CreditAssessmentCalloutTest {
    
        
    /*-------------------------------------------------------------------*/
    
    private static CreditAssessmentRequestResponse.BusinessCreditAssessmentResult 
        getBusinessCreditAssessmentResult(){
            CreditAssessmentRequestResponse.BusinessCreditAssessmentResult 
                businessCreditAssessmentResult = new CreditAssessmentRequestResponse.BusinessCreditAssessmentResult ();
            CreditAssessmentEnterpriseTypes.MultilingualCodeDescriptionList 
                assessmentResult = new CreditAssessmentEnterpriseTypes.MultilingualCodeDescriptionList ();
                        CreditAssessmentEnterpriseTypes.MultilingualCodeDescriptionList 
                assessmentResultReason = new CreditAssessmentEnterpriseTypes.MultilingualCodeDescriptionList ();
            CreditAssessmentEnterpriseTypes.MultilingualCodeDescriptionList[] 
                assessmentResultReasonListList = new List<CreditAssessmentEnterpriseTypes.MultilingualCodeDescriptionList>();
            businessCreditAssessmentResult.assessmentResult = assessmentResult;
            businessCreditAssessmentResult.assessmentResultReasonList = assessmentResultReasonListList; 
            assessmentResultReason.code = 'SIMPL_ASSMT';
            assessmentResultReasonListList.add(assessmentResultReason);
            return businessCreditAssessmentResult;
        }
    
    private static CreditAssessmentRequestResponse.Ruleset getRuleset(){
        CreditAssessmentRequestResponse.Ruleset ruleset = new CreditAssessmentRequestResponse.Ruleset();       
        
        CreditAssessmentRequestResponse.RuleCondition[] 
            ruleConditionList = new List<CreditAssessmentRequestResponse.RuleCondition>();
        CreditAssessmentRequestResponse.RuleCondition 
            ruleCondition = new CreditAssessmentRequestResponse.RuleCondition();
        CreditAssessmentEnterpriseTypes.MultilingualCodeDescriptionList[] 
            conditionResultReasonList = new List<CreditAssessmentEnterpriseTypes.MultilingualCodeDescriptionList>();
        CreditAssessmentEnterpriseTypes.MultilingualCodeDescriptionList 
            conditionResultReason = new CreditAssessmentEnterpriseTypes.MultilingualCodeDescriptionList ();
        
        CreditAssessmentEnterpriseTypes.MultilingualCodeDescriptionList 
                    conditionName = new CreditAssessmentEnterpriseTypes.MultilingualCodeDescriptionList();
        CreditAssessmentEnterpriseTypes.Description[] 
            descriptions = new List<CreditAssessmentEnterpriseTypes.Description>();
        
        CreditAssessmentEnterpriseTypes.Description 
            description = new CreditAssessmentEnterpriseTypes.Description ();
        descriptions.add(description);
        descriptions.add(description);
        descriptions.add(description);
        
        conditionName.description = descriptions;
        
        ruleCondition.conditionResultReasonList = conditionResultReasonList;
        ruleCondition.conditionName = conditionName;
            
        conditionResultReasonList.add(conditionResultReason);
        conditionResultReasonList.add(conditionResultReason);
        conditionResultReasonList.add(conditionResultReason);
        conditionResultReasonList.add(conditionResultReason);
        
        ruleConditionList.add(ruleCondition);
        ruleConditionList.add(ruleCondition);
        ruleConditionList.add(ruleCondition);
        ruleConditionList.add(ruleCondition);
        ruleset.ruleConditionList = ruleConditionList;
        
        return ruleset;
    }
    
    /*-------------------------------------------------------------------*/
    
    /**/
    @isTest
    public static void saveAutoUpdateBusinessCreditProfileResult(){
        
        CreditAssessmentRequestResponse.AutoUpdateBusinessCreditProfileResult  
            result 
            = new CreditAssessmentRequestResponse.AutoUpdateBusinessCreditProfileResult();
        
        CreditAssessmentEnterpriseTypes.MultilingualCodeDescriptionList 
            customerMatchedTypeCd 
            = new CreditAssessmentEnterpriseTypes.MultilingualCodeDescriptionList ();
        
        result.customerMatchedTypeCd = customerMatchedTypeCd;
        customerMatchedTypeCd.code = 'EXCT';
        
        Credit_Assessment__c car = CreditUnitTestHelper.getCar();        
        Credit_Profile__c cp = CreditUnitTestHelper.getCreditProfile();
        car.Credit_Profile__c = cp.id;
        CreditAssessmentCalloutHelper.saveAutoUpdateBusinessCreditProfileResult(result, car);
        
   }
    
    @isTest
    public static void populateAditionalDetailsPerBTCredit(){
        CreditAssessmentRequestResponse.MatchedIndividualCustomer[] 
            matchedIndividualCustomerList = new List< CreditAssessmentRequestResponse.MatchedIndividualCustomer>();
        CreditAssessmentCalloutHelper.populateAditionalDetailsPerBTCredit(CreditUnitTestHelper.getCreditProfile(), matchedIndividualCustomerList);        
    }
    
    
    
    @isTest
    public static void populateAdditionalDetailsPerCar(){
        CreditAssessmentCalloutHelper.populateAdditionalDetailsPerCar(
            CreditUnitTestHelper.getCreditProfile()
            , CreditUnitTestHelper.getCar()
        );        
    }
    
    @isTest
    public static void buildAssociation() {   
        Credit_Profile__c cp = CreditUnitTestHelper.getCreditProfile();
        CreditAssessmentCalloutHelper.buildAssociation(cp, '123456');
    }
    
    
    @isTest
    public static void retrieveBusinessCreditProfile() {   
        Credit_Profile__c cp = CreditUnitTestHelper.getCreditProfile();
        CreditAssessmentCalloutHelper.retrieveBusinessCreditProfile('123456');
    }
    
    
    @isTest
    public static void getCreditProfile() {   
        Credit_Profile__c cp = CreditUnitTestHelper.getCreditProfile();
        CreditAssessmentCalloutHelper.getCreditProfile(cp.id);
    }
    
    
    @isTest
    public static void getBusinessCustomerId() {   
        CreditAssessmentCalloutHelper.getBusinessCustomerId('12345678');
    }
        
    
    @isTest
    public static void getIndividualCustomerId() {   
        
        CreditAssessmentRequestResponse.MatchedIndividualCustomer[] 
            matchedIndividualCustomerList 
            = new List<CreditAssessmentRequestResponse.MatchedIndividualCustomer>();
        CreditAssessmentRequestResponse.MatchedIndividualCustomer
            matchedIndividualCustomer
            = new CreditAssessmentRequestResponse.MatchedIndividualCustomer();
        matchedIndividualCustomerList.add(matchedIndividualCustomer);
        CreditAssessmentCalloutHelper.getIndividualCustomerId(matchedIndividualCustomerList);
        
    }
    
    
    
    @isTest
    public static void populateBusinessCustomerProfile() {   
        Test.startTest();   
        CreditAssessmentBusinessTypes.BusinessCustomerProfile 
            businessCustomerProfile 
            = new  CreditAssessmentBusinessTypes.BusinessCustomerProfile ();
        Credit_Assessment__c 
            car = CreditUnitTestHelper.getCar();
        
        CreditAssessmentCalloutHelper.populateBusinessCustomerProfile(
            businessCustomerProfile
            , car);        
        Test.StopTest();    
    }
    
    
    @isTest
    public static void populateAutoUpdateBusinessCreditProfile() {   
        Test.startTest();   
        Credit_Profile__c cp = CreditUnitTestHelper.getCreditProfile();
        CreditAssessmentRequestResponse.MatchedIndividualCustomer 
            matchedIndividualCustomer
            = new CreditAssessmentRequestResponse.MatchedIndividualCustomer();
        
        CreditAssessmentBusinessTypes.IndividualCreditIdentification 
            matchedCreditIdentification
            = new CreditAssessmentBusinessTypes.IndividualCreditIdentification();      
        
        matchedIndividualCustomer.matchedCreditIdentification = matchedCreditIdentification;
        CreditAssessmentCalloutHelper.populateAutoUpdateBusinessCreditProfile(
            cp
            , matchedIndividualCustomer);        
        Test.StopTest();    
    }
    
    @isTest
    public static void populateAutoUpdateMatchedCreditIdentification() {   
        Test.startTest();   
        Credit_Profile__c cp = CreditUnitTestHelper.getCreditProfile();
        CreditAssessmentBusinessTypes.IndividualCreditIdentification 
            identification  
            = new CreditAssessmentBusinessTypes.IndividualCreditIdentification ();
        CreditAssessmentBusinessTypes.DriverLicense 
            driverLicense 
            = new CreditAssessmentBusinessTypes.DriverLicense ();
        identification.driverLicense = driverLicense;
        CreditAssessmentCalloutHelper.populateAutoUpdateMatchedCreditIdentification(
            cp
            , identification);        
        Test.StopTest();    
    }
    
    @isTest
    public static void populateAutoUpdateAdditionalConsumerCreditIdentification() {   
        Test.startTest();   
        Credit_Profile__c cp = CreditUnitTestHelper.getCreditProfile();
        CreditAssessmentBusinessTypes.IndividualCreditIdentification 
            identification  
            = new CreditAssessmentBusinessTypes.IndividualCreditIdentification ();
        CreditAssessmentBusinessTypes.Passport 
            passport = new CreditAssessmentBusinessTypes.Passport();
        identification.passport = passport;
        CreditAssessmentCalloutHelper.populateAutoUpdateAdditionalConsumerCreditIdentification(
            cp
            , identification);        
        Test.StopTest();    
    }
    
    @isTest
    public static void ping() {     
        Test.setMock(WebServiceMock.class, new CreditAssessmentCalloutMock());
        Test.startTest();   
        CreditAssessmentCallout.ping();        
        Test.StopTest();    
    }
    

    @isTest
    public static void performBusinessCreditAssessment() {     
        Test.setMock(WebServiceMock.class, new CreditAssessmentCalloutMock());
        Test.startTest();   
        Credit_Assessment__c car = CreditUnitTestHelper.getCar();
        CreditAssessmentCallout.performBusinessCreditAssessment(car.id);        
        Test.StopTest();    
    }

    @isTest
    public static void performAutoUpdateBusinessCreditProfileProxy() {     
        Test.setMock(WebServiceMock.class, new CreditAssessmentCalloutMock());
        Test.startTest();  
        
        Credit_Assessment__c car = CreditUnitTestHelper.getCar();
        CreditAssessmentCallout.performAutoUpdateBusinessCreditProfileProxy(car.id);   
        
        car.Legal_Entity_Type__c = Label.Legal_Entity_Type_Sole_Proprietor;
        update car;
        CreditAssessmentCallout.performAutoUpdateBusinessCreditProfileProxy(car.id);  
            
        Test.StopTest();    
    }
    

    @isTest
    public static void attachCreditReport() {     
        Test.setMock(WebServiceMock.class, new CreditAssessmentCalloutMock());
        Test.startTest();   
        
        Long businessCustomerId = 123456L;
        Document document = CreditUnitTestHelper.getDocument();
        String documentTypeCd = '123';
        String documentSourceCd = '123';
        
        System.debug('... dyy ... document = ' + document);
        
        CreditAssessmentCallout.attachCreditReport(
            businessCustomerId
            ,document
            ,documentTypeCd
            ,documentSourceCd);      
        
        CreditAssessmentCallout.attachCreditReportWithCarId(
            CreditUnitTestHelper.getCar().id
            ,  businessCustomerId
            ,  document
            ,  documentTypeCd
            ,  documentSourceCd
        );
            
        Test.StopTest();    
    }
    

    @isTest
    public static void getCreditReport() {     
        Test.setMock(WebServiceMock.class, new CreditAssessmentCalloutMock());
        Test.startTest();   
        CreditAssessmentCallout.getCreditReport(12345L);        
        Test.StopTest();    
    }
    
    
    

    @isTest
    public static void removeCreditReport() {     
        Test.setMock(WebServiceMock.class, new CreditAssessmentCalloutMock());
        Test.startTest();   
        CreditAssessmentCallout.removeCreditReport('12345L');        
        Test.StopTest();    
    }
    
    
    
    
    @isTest
    public static void setCarStatusAfterCallWebserviceSuccessful() {     
        Test.setMock(WebServiceMock.class, new CreditAssessmentCalloutMock());
        Test.startTest();   
        Credit_Assessment__c car = CreditUnitTestHelper.getCar();
        CreditAssessmentRequestResponse.BusinessCreditAssessmentResult 
            result = getBusinessCreditAssessmentResult();
        
        result.assessmentResult.code = Label.Assessment_Result_Approved;        
        CreditAssessmentCalloutHelper.setCarStatusAfterCallWebserviceSuccessful(car, result);   
        
        result.assessmentResult.code = Label.Assessment_Result_Manual;       
        car.Request_credit_escalation__c = False;
        car.CAR_Status__c = Label.CAR_Status_Queued;
        CreditAssessmentCalloutHelper.setCarStatusAfterCallWebserviceSuccessful(car, result);   
         
        
        result.assessmentResult.code = Label.Assessment_Result_Manual;       
        car.Request_credit_escalation__c = True;
        car.CAR_Status__c = Label.CAR_Status_Queued;
        CreditAssessmentCalloutHelper.setCarStatusAfterCallWebserviceSuccessful(car, result);   
         
        
        result.assessmentResult.code = Label.Assessment_Result_Manual; 
        car.Is_order_related_to_TBO__c = Label.Yes;
        car.CAR_Status__c = Label.CAR_Status_Queued;
        CreditAssessmentCalloutHelper.setCarStatusAfterCallWebserviceSuccessful(car, result);   
        
        Test.StopTest();    
    }
    
    
    
    
    @isTest
    public static void populateAssociationsProfileForPartner(){ 
        Test.setMock(WebServiceMock.class, new CreditAssessmentCalloutMock());
        Test.startTest();   
        CreditAssessmentBusinessTypes.BusinessProfile_associationsProfile_element 
            associationsProfile = new CreditAssessmentBusinessTypes.BusinessProfile_associationsProfile_element();
        Credit_Assessment__c car = CreditUnitTestHelper.getCar();        
        CreditAssessmentCalloutHelper.populateAssociationsProfileForPartner(associationsProfile, car);
        Test.StopTest();    
    }
    
    @isTest
    public static void populatePartnerBusinessProfileList(){ 
        Test.setMock(WebServiceMock.class, new CreditAssessmentCalloutMock());
        Test.startTest();  
        CreditAssessmentBusinessTypes.BusinessProfile_associationsProfile_partnerBusinessProfileList_element[] 
        partnerBusinessProfileList = new List<CreditAssessmentBusinessTypes.BusinessProfile_associationsProfile_partnerBusinessProfileList_element>();
        Credit_Partners__c[] partners = new List< Credit_Partners__c>();
        partners.add(CreditUnitTestHelper.getPartner());
        partners.add(CreditUnitTestHelper.getPartner());
        partners.add(CreditUnitTestHelper.getPartner());
        CreditAssessmentCalloutHelper.populatePartnerBusinessProfileList(partnerBusinessProfileList, partners);
        Test.StopTest();    
    }
    
    @isTest
    public static void populatePartnerIndividualProfileList(){ 
        Test.setMock(WebServiceMock.class, new CreditAssessmentCalloutMock());
        Test.startTest();  
        
        CreditAssessmentBusinessTypes.BusinessProfile_associationsProfile_partnerIndividualProfileList_element[] 
            partnerIndividualProfileList = new List<CreditAssessmentBusinessTypes.BusinessProfile_associationsProfile_partnerIndividualProfileList_element>();
        
        Credit_Partners__c[] partners = new List< Credit_Partners__c>();
        partners.add(CreditUnitTestHelper.getPartner());
        partners.add(CreditUnitTestHelper.getPartner());
        partners.add(CreditUnitTestHelper.getPartner());
        
        CreditAssessmentCalloutHelper.populatePartnerIndividualProfileList(partnerIndividualProfileList, partners);
        Test.StopTest();    
    }
    
    @isTest
    public static void savetoCreditCondition(){ 
        Test.setMock(WebServiceMock.class, new CreditAssessmentCalloutMock());
        Test.startTest();  
        
        CreditAssessmentRequestResponse.BusinessCreditAssessmentResult 
            result = new CreditAssessmentRequestResponse.BusinessCreditAssessmentResult();
        CreditAssessmentRequestResponse.BusinessCreditAssessmentResult_ruleEvaluationDetails_element 
            ruleEvaluationDetails = new CreditAssessmentRequestResponse.BusinessCreditAssessmentResult_ruleEvaluationDetails_element();
        
        CreditAssessmentRequestResponse.Ruleset[] 
            rulesetList = new List<CreditAssessmentRequestResponse.Ruleset>();
        
        result.ruleEvaluationDetails = ruleEvaluationDetails;
        ruleEvaluationDetails.rulesetList = rulesetList;
        
        rulesetList.add(getRuleset());
        rulesetList.add(getRuleset());
        rulesetList.add(getRuleset());
        rulesetList.add(getRuleset());
        
        Credit_Assessment__c car = CreditUnitTestHelper.getCar();
        
        CreditAssessmentCalloutHelper.savetoCreditCondition(result, car);
        Test.StopTest();    
    }

    
    @isTest
    public static void populatePartnerBusinessCreditProfile(){     
        Test.startTest();  
        CreditAssessmentBusinessTypes.PartnerBusinessCreditProfile 
            partnerBusinessCreditProfile = new CreditAssessmentBusinessTypes.PartnerBusinessCreditProfile();
        CreditAssessmentBusinessTypes.RegistrationIncorporation 
            registrationIncorporation = new CreditAssessmentBusinessTypes.RegistrationIncorporation();
        
        CreditAssessmentCalloutHelper.populatePartnerBusinessCreditProfile(
            partnerBusinessCreditProfile
            , registrationIncorporation
            , CreditUnitTestHelper.getCreditProfile());
        Test.StopTest();    
    }
    
    
    @isTest
    public static void refreshOpportunitProduct(){     
        Test.startTest();  
        Credit_Assessment__c car = CreditUnitTestHelper.getCar();
        
        CreditAssessmentCallout.refreshProducts(car.id);
        
        List<Credit_Related_Product__c> allCarProducts = new List<Credit_Related_Product__c>();
        allCarProducts.add(CreditUnitTestHelper.getCreditRelatedProduct(car));
        allCarProducts.add(CreditUnitTestHelper.getCreditRelatedProduct(car));
        allCarProducts.add(CreditUnitTestHelper.getCreditRelatedProduct(car));
        
        CreditAssessmentCalloutHelper.refreshOpportunitProduct(car.id, allCarProducts);
        CreditAssessmentCalloutHelper.refreshFOBOChecklistTotally(car.id, allCarProducts);        
        
        List<FOBOChecklist__c> foboItems = new List<FOBOChecklist__c>();
        foboItems.add(CreditUnitTestHelper.getFOBOChecklist(car));
        foboItems.add(CreditUnitTestHelper.getFOBOChecklist(car));
        foboItems.add(CreditUnitTestHelper.getFOBOChecklist(car));
        
        
        List<OpportunityLineItem> oppItems = new List<OpportunityLineItem>();
        oppItems.add(CreditUnitTestHelper.getOpportunityLineItem(car));
        oppItems.add(CreditUnitTestHelper.getOpportunityLineItem(car));
        oppItems.add(CreditUnitTestHelper.getOpportunityLineItem(car));
        CreditAssessmentCalloutHelper.removeItemFromListLogistics(
            CreditUnitTestHelper.getOpportunityLineItem(car)
            , oppItems
            , oppItems);
        
        CreditAssessmentCalloutHelper.getAllCarProducts(car.id);
        
        Test.StopTest();    
    }
    
    
    @isTest
    public static void isClickedTooManyTimesTooFast(){   
        Test.startTest();  
        Credit_Assessment__c car = CreditUnitTestHelper.getCar();
        CreditAssessmentCallout.isClickedTooManyTimesTooFast(car);
        Test.StopTest();    
    }   
    
    @isTest
    public static void cancelCarFromOrderProxy(){   
        Test.startTest();  
        Credit_Assessment__c car = CreditUnitTestHelper.getCar();
        List<Opportunity> orders = new List<Opportunity>();
        Opportunity order = CreditUnitTestHelper.getOpportunity();
        orders.add(order);
        List<ID> orderIds = new List<ID>();
        ID orderId = CreditUnitTestHelper.getOpportunity().id;
        orderIds.add(orderId);
        CreditAssessmentCallout.cancelCarFromOrderProxy(orderIds);           
        CreditAssessmentCallout.cancelCar(orderId, orders); 
        Test.StopTest();    
    }   

    
    
        
    
    @isTest
    public static void testCreditWebServiceClientUtil(){   
        Test.startTest(); 
        
        CalloutException e = new CalloutException();
        CreditWebServiceClientUtil.getFaultcode(e);
        
        CreditWebServiceClientUtil.getProfileAuditInfo();
        
        String str = '    0000 123456789012345678901234567890';
        CreditWebServiceClientUtil.removeAllSpaces(str);
        CreditWebServiceClientUtil.removeLeadingZeros(str);
        CreditWebServiceClientUtil.isLong(str);
        CreditWebServiceClientUtil.getListOfStringsPerLinebreak(str);
        CreditWebServiceClientUtil.getStringOfLimit(str,10);
        
        Test.StopTest();    
    }   
    //Start : Aditya Jamwal 16-11-2107  QC- 9728,9717
    
   //OCOM: Order CAR Start
    @isTest
    public static void refreshOrderProduct(){     
        Test.startTest();  
        Credit_Assessment__c car = CreditUnitTestHelper.getCar();
        
        CreditAssessmentCallout.refreshProducts(car.id);
        
        List<Credit_Related_Product__c> allCarProducts = new List<Credit_Related_Product__c>();
        allCarProducts.add(CreditUnitTestHelper.getCreditRelatedProduct(car));
        allCarProducts.add(CreditUnitTestHelper.getCreditRelatedProduct(car));
        allCarProducts.add(CreditUnitTestHelper.getCreditRelatedProduct(car));
        
        CreditAssessmentCalloutHelper.refreshOrderProduct(car.id, allCarProducts);
        
        List<OrderItem> orderItems = new List<OrderItem>();
        orderItems.add(CreditUnitTestHelper.getOrderLineItem(car));
        orderItems.add(CreditUnitTestHelper.getOrderLineItem(car));
        orderItems.add(CreditUnitTestHelper.getOrderLineItem(car));
        CreditAssessmentCalloutHelper.removeOrderItemFromListLogistics(
            CreditUnitTestHelper.getOrderLineItem(car)
            , orderItems
            , orderItems);
        
        CreditAssessmentCalloutHelper.getAllCarProducts(car.id);
        
        Test.StopTest();    
    }
    
    
    @isTest
    public static void refreshOrderProduct2(){     
        Test.startTest();
        Id pricebookId= Test.getStandardPricebookId();
        
        Credit_Assessment__c car = CreditUnitTestHelper.getCar();
        
        //CreditAssessmentCallout.refreshProducts(car.id);
        
        List<Credit_Related_Product__c> allCarProducts = new List<Credit_Related_Product__c>();
        Credit_Related_Product__c aCreditRelatedProduct;
        aCreditRelatedProduct = CreditUnitTestHelper.getCreditRelatedProduct(car);
        aCreditRelatedProduct.Product__c = 'Office Internet Basic';
        aCreditRelatedProduct.Monthly_Recurring_Charges__c = 10.00;
        aCreditRelatedProduct.Non_Recurring_Charge__c = 9.00;
        update aCreditRelatedProduct;
        allCarProducts.add(aCreditRelatedProduct);
        aCreditRelatedProduct = CreditUnitTestHelper.getCreditRelatedProduct(car);
        aCreditRelatedProduct.Product__c = 'Office Internet';
        aCreditRelatedProduct.Monthly_Recurring_Charges__c = 10.00;
        aCreditRelatedProduct.Non_Recurring_Charge__c = 9.00;
        update aCreditRelatedProduct;
        allCarProducts.add(aCreditRelatedProduct);
        aCreditRelatedProduct = CreditUnitTestHelper.getCreditRelatedProduct(car);
        aCreditRelatedProduct.Product__c = 'Office Internet with 4G';
        aCreditRelatedProduct.Monthly_Recurring_Charges__c = 300.00;
        aCreditRelatedProduct.Non_Recurring_Charge__c = 10.00;
        update aCreditRelatedProduct;
        allCarProducts.add(aCreditRelatedProduct);
        
        List<Account> accts = new List<Account>();
        for(Integer i=0; i<1; i++) {
            Account accntObjInst = new Account(Name='TestAccount' + i);
            accts.add(accntObjInst);
        }
        insert accts;
        Account accntObj=accts[0];
        String accountID=accntObj.id;
        RecordType accntRecTypeId=[SELECT Id FROM RecordType where SobjectType='Account' and Name='RCID' limit 1];
        accntObj.CAN__c='12345678';
        accntObj.Company_Legal_Name__c='CompanyLegalName';
        accntObj.Legal_Entity_Type__c='Corporate';
        accntObj.Operating_Name_DBA__c='Telus';
        accntObj.BillingCity='TestCity';
        accntObj.BillingCountry='TestCity';
        accntObj.BillingPostalCode='TestCity';
        accntObj.BillingState='TestCity';
        accntObj.BillingStreet='TestCity';
        accntObj.CustProfId__c='12345';
        accntObj.RecordTypeId =accntRecTypeId.id;
        accntObj.id=accountID;
        //update accntObj;

        Order ordr=new Order();
        ordr.AccountId=accountID;
        ordr.Bill_To_a_New_Account__c=FALSE;
        ordr.CurrencyIsoCode='CAD';
        ordr.EffectiveDate=Date.today();
        ordr.IsReductionOrder=FALSE;
        ordr.Name='TestOrder';
        ordr.Opportunity_Name__c='TestOpportunity';
        ordr.Quote_Number__c='12345678';
        ordr.Rush__c=FALSE;
        ordr.Shipping_same_as_Service__c=FALSE;
        ordr.Status='Not Submitted';
        ordr.vlocity_cmt__CreatedByAPI__c=FALSE;
        ordr.vlocity_cmt__IsContractRequired__c=FALSE;
        ordr.vlocity_cmt__IsSyncing__c=FALSE;
        ordr.vlocity_cmt__OriginatingChannel__c='Call Center';
        //ordr.QuoteId=quote.id;
        //ordr.vlocity_cmt__QuoteId__c=quote.id;
        ordr.Pricebook2Id=pricebookId;
        ordr.Credit_Assessment__c=car.id;
        insert ordr;
                
        CreditAssessmentCalloutHelper.refreshOrderProduct(car.id, allCarProducts);

        Product2 productInst;        
        OrderItem anOrderItem;
        PricebookEntry pbe;
        List<OrderItem> orderItems = new List<OrderItem>();
        
        productInst = new Product2(name = 'Office Internet Basic', productcode = 'TESTCODE', isActive = true);
        productInst.Sellable__c=true;
        productInst.orderMgmtId__c='123456789';
        insert productInst;
        pbe = new PricebookEntry(product2id = productInst.id, unitprice = 1,  isactive = true, pricebook2id = pricebookId);
        insert pbe;
        anOrderItem = CreditUnitTestHelper.getOrderLineItem(car);
        anOrderItem.vlocity_cmt__Product2Id__c = productInst.id;
        anOrderItem.vlocity_cmt__OneTimeTotal__c = 9.00;
        anOrderItem.vlocity_cmt__RecurringTotal__c = 10.00;
        anOrderItem.UnitPrice=0.00;
        anOrderItem.Quantity = 1;
        //anOrderItem.Credit_Assessment__c = car.id;
        anOrderItem.OrderId=ordr.id;
        anOrderItem.PricebookEntryId=pbe.id;
        insert anOrderItem;
        orderItems.add(anOrderItem);
        productInst = new Product2(name = 'Office Internet Basic', productcode = 'TESTCODE', isActive = true);
        productInst.Sellable__c=true;
        productInst.orderMgmtId__c='2123456789';
        insert productInst;
        pbe = new PricebookEntry(product2id = productInst.id, unitprice = 1,  isactive = true, pricebook2id = pricebookId);
        insert pbe;
        anOrderItem = CreditUnitTestHelper.getOrderLineItem(car);
        anOrderItem.vlocity_cmt__Product2Id__c = productInst.id;
        anOrderItem.vlocity_cmt__OneTimeTotal__c = 9.00;
        anOrderItem.vlocity_cmt__RecurringTotal__c = 10.00;
        anOrderItem.UnitPrice=0.00;
        anOrderItem.Quantity = 1;
        //anOrderItem.Credit_Assessment__c = car.id;
        anOrderItem.OrderId=ordr.id;
        anOrderItem.PricebookEntryId=pbe.id;
        insert anOrderItem;
        orderItems.add(anOrderItem);
        productInst = new Product2(name = 'Office Internet', productcode = 'TESTCODE', isActive = true);
        productInst.Sellable__c=true;
        productInst.orderMgmtId__c='3123456789';
        insert productInst;
        pbe = new PricebookEntry(product2id = productInst.id, unitprice = 1,  isactive = true, pricebook2id = pricebookId);
        insert pbe;
        anOrderItem = CreditUnitTestHelper.getOrderLineItem(car);
        anOrderItem.vlocity_cmt__Product2Id__c = productInst.id;
        anOrderItem.vlocity_cmt__OneTimeTotal__c = 9.00;
        anOrderItem.vlocity_cmt__RecurringTotal__c = 11.00;
        anOrderItem.Credit_Assessment__c = null;
        anOrderItem.UnitPrice=150.00;
        anOrderItem.Quantity = 1;
        //anOrderItem.Credit_Assessment__c = car.id;
        anOrderItem.OrderId=ordr.id;
        anOrderItem.PricebookEntryId=pbe.id;
        insert anOrderItem;
        orderItems.add(anOrderItem);
        productInst = new Product2(name = 'Test Product', productcode = 'TESTCODE', isActive = true);
        productInst.Sellable__c=true;
        productInst.orderMgmtId__c='4123456789';
        insert productInst;
        pbe = new PricebookEntry(product2id = productInst.id, unitprice = 1,  isactive = true, pricebook2id = pricebookId);
        insert pbe;
        anOrderItem = CreditUnitTestHelper.getOrderLineItem(car);
        anOrderItem.vlocity_cmt__Product2Id__c = productInst.id;
        anOrderItem.vlocity_cmt__RecurringCharge__c = 50.00;
        anOrderItem.UnitPrice=0.00;
        anOrderItem.Quantity = 1;
        //anOrderItem.Credit_Assessment__c = car.id;
        anOrderItem.OrderId=ordr.id;
        anOrderItem.PricebookEntryId=pbe.id;
        insert anOrderItem;
        orderItems.add(anOrderItem);
        
        CreditAssessmentCalloutHelper.refreshOrderProduct(car.id, allCarProducts);
        
        CreditAssessmentCalloutHelper.removeOrderItemFromListLogistics(
            CreditUnitTestHelper.getOrderLineItem(car)
            , orderItems
            , orderItems);
        
        CreditAssessmentCalloutHelper.getAllCarProducts(car.id);
        
        Test.StopTest();    
    }
   //OCOM: Order CAR End
   //Start : Aditya Jamwal  16-11-2107  QC- 9728,9717
        
}