@isTest
private class TestForSendEmailFromContactOrLead{
    static testMethod void testMethodForCodeCoverage(){
           
       String contactId='0034000000Ztchp';
       String leadId='00Q4000000W6g0W';
       System.currentPagereference().getParameters().put('contId','0034000000Ztchp');
       System.currentPagereference().getParameters().put('leadId','00Q4000000W6g0W');
           
       SendEmailFromContactOrLead obj=new SendEmailFromContactOrLead();
       obj.sendEmailStatus();
       obj.getEmailStatus();
       PageReference pageObj = obj.cancel();    
       PageReference expObj = new PageReference('/0034000000Ztchp');
       System.assertEquals(expObj.getURL() , pageObj.getURL());
       
    }
}