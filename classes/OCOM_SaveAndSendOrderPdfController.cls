/*
    *******************************************************************************************************************************
    Class Name:     OCOM_SaveAndSendOrderPdfController 
    Purpose:        Controller for Saving and Sending Order PDF       
    Test Class Name: OCOM_SaveAndSendOrderPdfControllerTest 
    Created by:    Arvind Yadav ,  19/09/2016 2:43 PM
    *******************************************************************************************************************************
*/
public with sharing class OCOM_SaveAndSendOrderPdfController 
{

    public Order o {get;set;}
    //Arpit: 22-May-2017 : Sprint 3 : TQ-French Transition : English-French language Toggeling 
    public static String choosenLanguage {get ; set ;}
    
    public OCOM_SaveAndSendOrderPdfHelper pdfHelper {get;set;}
    
    /**
    Method: OCOM_SaveAndSendOrderPdfController(ApexPages.StandardController stdController) 
    This method is parameterized constructor which is initializing the current Order object
    and fetching records from Order object where Id is equal to current order Id 
    Inputs Args: ApexPages.StandardController
    Output Args: none
    **/
    public OCOM_SaveAndSendOrderPdfController(ApexPages.StandardController stdController) 
    {
        this.o = (Order)stdController.getRecord();
        //Arpit: 22-May-2017 : Sprint 3 : TQ-French Transition : English-French language Toggeling 
        //Setting choosen language on wrapper from url
        choosenLanguage = ApexPages.currentPage().getParameters().get('lang');
        /*if(null == choosenLanguage  && null != pdfHelper.choosenLanguage  ){
            choosenLanguage  = pdfHelper.choosenLanguage ;
        }*/
       // system.debug('OCOM_SaveAndSendOrderPdfController : choosenLanguage --->' + choosenLanguage );
        if(o.id!=null)
            o = [select Id, Name, Status, OrderNumber, CustomerAuthorizedById,CustomerAuthorizedBy.Email  from Order where Id = :o.Id];
        ////Arpit: 22-May-2017 : Sprint 3 : TQ-French Transition : English-French language Toggeling  
        //Modified Controller call to pass extra parametr of choosenLanguage
        pdfHelper = new OCOM_SaveAndSendOrderPdfHelper(o.id, choosenLanguage ); 

    }
    
    //Arpit : 22-May-2017 : French-English Toggleing
    //Added new parameterized constructor
    /**
    @author          : Arpit Goyal
    Created date     : 22-May-2017
    Project / Sprint : Buy1 - TQ-stories : Sprint 3
    Requirement      : French-English Toggleing of PDF and mailers
    args             : ApexPages.StandardController, String
    Purpose          : Constructor of class
    **/
    public OCOM_SaveAndSendOrderPdfController(ApexPages.StandardController stdController, String choosenLang) 
    {
       // system.debug('OCOM_SaveAndSendOrderPdfController : inside parameterized constructor --->' + choosenLang);
        this.o = (Order)stdController.getRecord();
        //Arpit: 22-May-2017 : Sprint 3 : TQ-French Transition : English-French language Toggeling 
        //Setting choosen language on wrapper from url
        if(choosenLang == null) choosenLanguage = ApexPages.currentPage().getParameters().get('lang');
        
       // system.debug('OCOM_SaveAndSendOrderPdfController : choosenLanguage --->' + choosenLanguage );
        if(o.id!=null)
            o = [select Id, Name, Status, OrderNumber, CustomerAuthorizedById,CustomerAuthorizedBy.Email  from Order where Id = :o.Id];
        ////Arpit: 22-May-2017 : Sprint 3 : TQ-French Transition : English-French language Toggeling  
        //Modified Controller call to pass extra parametr of choosenLanguage
        if(choosenLang != null || choosenLanguage != null){            
             pdfHelper = new OCOM_SaveAndSendOrderPdfHelper(o.id, choosenLang); 
        }

    }


    /*********************
    @author          : Arpit Goyal
    Created date     : 22-May-2017
    Project / Sprint : Buy1 - TQ-stories : Sprint 3
    Requirement      : French-English Toggleing of PDF and mailers
    args             : null
    Purpose          : Calls helper class for sending mail logic 
    ***********************/
    public void callSendEmail(){
        //system.debug('OCOM_SaveAndSendOrderPdfController, callSendEmail() : choosenLanguage --->' + pdfhelper.choosenLanguage );
        pdfHelper.sendEmailWithLang(choosenLanguage);
    }

}