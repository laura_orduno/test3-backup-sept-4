@isTest
private class TestNotMyLeadController {
    static testMethod void testMethodForCodeCoverage(){ 
          // created Account -Start
             Account acc = new Account(
                 Name= 'Test123', phone = '1234567890'
             );
             insert acc;
             System.debug('Account='+acc); 
         //created Account -End  
         //Contact created  -Start 
             Contact con = new Contact(
                 LastName = 'Test Contact',
                 AccountId= acc.Id, phone = '1234567890'
             );
             insert con;
             System.debug('contact='+con); 
         //Contact created  -End
         //Created a Partner User - Start             
             Profile p = [select id,name from profile where usertype = 'PowerPartner' limit 1];
             
             System.debug('Profile='+p);         
             User u = new User(
                 LastName = 'TestName',
                 Alias = 'tsVN',
                 Email = 'tessgdt@abc.com',
                 Username = 'partkgfsner@abc.com',
                 CommunityNickname = 'tbjsti',
      phone = '6049969449',
                 emailencodingkey = 'UTF-8',
                 languagelocalekey = 'en_US',
                 localesidkey = 'en_US',
                 ContactId = con.Id,                 
                 timezonesidkey = 'America/Los_Angeles',             
                 profileId = p.Id
             );
             insert u;             
       //Created a Partner User - End
       //Created A lead -Start
             Lead led = new Lead(                 
                 Company = 'Testing Lead for Owner', 
                 LastName = 'Test', 
                 Phone = '111111111',
                 OwnerId = u.id, 
                 Status = 'Qualified'
             );        
             insert led;    
       //Created A lead -End              
       System.currentPagereference().getParameters().put('leadId',led.Id);
       NotMyLeadController obj = new NotMyLeadController ();
       obj.init();
       obj.cancel();
       Lead leadObj = [select ownerId from Lead where Id= :led.Id];
       Group qObj = [Select g.Name, g.Id From Group g where g.Name= :NotMyLeadController.ALLOCATION_CATCHER_QUEUE]; 
       //owner ID of Lead should be Queue ID
       System.assertEquals( qObj.Id, leadObj.ownerId );
   }
    
    static testMethod void testMethodForCodeCoverage2(){ 
          // created Account -Start
             Account acc = new Account(
                 Name= 'Test123', phone = '1234567890', dealer_account__c = true
             );
             insert acc;
        		        
             System.debug('Account='+acc); 
         //created Account -End  
         //Contact created  -Start 
             Contact con = new Contact(
                 LastName = 'Test Contact',
                 AccountId= acc.Id, phone = '1234567890'
             );
             insert con;
             System.debug('contact='+con); 
         //Contact created  -End
         //Created a Partner User - Start             
             Profile p = [select id,name from profile where name = 'Gold Partner User' limit 1];
             
        
             System.debug('Profile='+p);         
             User u = new User(
                 LastName = 'TestName',
                 Alias = 'tsVN',
                 Email = 'tessgdt@abc.com',
                 Username = 'partkgfsner@abc.com',
                 CommunityNickname = 'tbjsti',
      phone = '6049969449',
                 emailencodingkey = 'UTF-8',
                 languagelocalekey = 'en_US',
                 localesidkey = 'en_US',
                 ContactId = con.Id,                 
                 timezonesidkey = 'America/Los_Angeles',             
                 profileId = p.Id
             );
             insert u;             
       //Created a Partner User - End
       //Created A lead -Start
             Lead led = new Lead(                 
                 Company = 'Testing Lead for Owner', 
                 LastName = 'Test', 
                 Phone = '111111111',
                 OwnerId = u.id, 
                 Status = 'Qualified'
             );        
             insert led;    
                
        Database.LeadConvert[] leadsToConvert = new Database.LeadConvert[0];
        Database.LeadConvert converter;
        
        converter = new Database.LeadConvert();
        converter.setLeadId( led.id );
        converter.setAccountId( acc.id );
        converter.setContactId( con.id );
        converter.setOwnerId( u.id );
        converter.setConvertedStatus( 'Qualified' );
        converter.setDoNotCreateOpportunity( false );
        leadsToConvert.add( converter );
        
        Database.ConvertLead( leadsToConvert, true );  
   }
}