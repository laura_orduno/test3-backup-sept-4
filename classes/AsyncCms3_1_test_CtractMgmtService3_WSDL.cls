/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class AsyncCms3_1_test_CtractMgmtService3_WSDL {
 static testMethod void AsyncCms3_1ContractManagementSvc_3() {
     smb_test_utility.createCustomSettingData();
     Continuation con = new Continuation(200);
     SMB_Dummy1WebServiceResponse_Test.intializeMockWebservice();
    AsyncCms3_1ContractManagementSvc_3 smbCMSV = new AsyncCms3_1ContractManagementSvc_3();
    AsyncCms3_1ContractManagementSvc_3.AsyncContractManagementSvcPort servicePort = new AsyncCms3_1ContractManagementSvc_3.AsyncContractManagementSvcPort();
    AsyncCms3_1ping_v1.pingResponse_elementFuture  contractcms = new AsyncCms3_1ping_v1.pingResponse_elementFuture();
    AsyncCms3_1ContractMgmtSvcReqRes_v3.getContractResponse_elementFuture contractlistcms = new  AsyncCms3_1ContractMgmtSvcReqRes_v3.getContractResponse_elementFuture();
    AsyncCms3_1ContractMgmtSvcReqRes_v3.replaceContractSubmissionResponse_elementFuture contractreqcms1 = new AsyncCms3_1ContractMgmtSvcReqRes_v3.replaceContractSubmissionResponse_elementFuture();
    AsyncCms3_1ContractMgmtSvcReqRes_v3.cancelContractSubmissionResponse_elementFuture legacyContractIdListcms1 = new AsyncCms3_1ContractMgmtSvcReqRes_v3.cancelContractSubmissionResponse_elementFuture();
    cms3_1ContractManagementSvcTypes_v3.ContractRequest contractreqcms = new cms3_1ContractManagementSvcTypes_v3.ContractRequest();
    cms3_1ContractManagementSvcTypes_v3.LegacyContractIdList legacyContractIdListcms = new cms3_1ContractManagementSvcTypes_v3.LegacyContractIdList();
    AsyncCms3_1ContractMgmtSvcReqRes_v3.triggerResendContractResponse_elementFuture  contractdoc = new AsyncCms3_1ContractMgmtSvcReqRes_v3.triggerResendContractResponse_elementFuture();
    AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractsByAgentIdResponse_elementFuture contractdoclist = new AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractsByAgentIdResponse_elementFuture();
    AsyncCms3_1ContractMgmtSvcReqRes_v3.getContractDocumentResponse_elementFuture termchargesInfo = new AsyncCms3_1ContractMgmtSvcReqRes_v3.getContractDocumentResponse_elementFuture();
   //  list<cmsv3_1ContractManagementSvcReqRes_v3.TerminationChargesParameter> termchargesparam = new  list<cms3_1ContractManagementSvcReqRes_v3.TerminationChargesParameter>();
   //  list<cmsv3_1ContractManagementSvcTypes_v3.ContractAmendmentList> contractAmedlist = new list<cms3_1ContractManagementSvcTypes_v3.ContractAmendmentList>();
    AsyncCms3_1ContractMgmtSvcReqRes_v3.getContractDocumentMetaDataResponse_elementFuture contractDataCMS = new AsyncCms3_1ContractMgmtSvcReqRes_v3.getContractDocumentMetaDataResponse_elementFuture();
    AsyncCms3_1ContractMgmtSvcReqRes_v3.createContractSubmissionResponse_elementFuture  contractd = new AsyncCms3_1ContractMgmtSvcReqRes_v3.createContractSubmissionResponse_elementFuture();
    AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractAmendmentsByAssociateNumberResponse_elementFuture contractdocl = new AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractAmendmentsByAssociateNumberResponse_elementFuture();
    AsyncCms3_1ContractMgmtSvcReqRes_v3.calculateTerminationChargesResponse_elementFuture contrl = new AsyncCms3_1ContractMgmtSvcReqRes_v3.calculateTerminationChargesResponse_elementFuture();
    AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractsByCustomerIdResponse_elementFuture custid = new AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractsByCustomerIdResponse_elementFuture(); 
     AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractDataResponse_elementFuture  contdata =new  AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractDataResponse_elementFuture();
     AsyncCms3_1ContractMgmtSvcReqRes_v3.updateContractSubmissionResponse_elementFuture contsub = new AsyncCms3_1ContractMgmtSvcReqRes_v3.updateContractSubmissionResponse_elementFuture();
     Test.startTest();
     
     servicePort.beginVerifyValidReplacement(con,'123','12'); 
     servicePort.beginPing(con); 
     servicePort.beginGetContract(con,'487382');
     servicePort.beginReplaceContractSubmission(con,contractreqcms,legacyContractIdListcms);
     servicePort.beginCancelContractSubmission(con,'354','34245245'); 
     servicePort.beginTriggerResendContract(con,'54','7567','Test','Test','yes','31343');
     servicePort.beginFindContractsByAgentId(con,'3534');
     servicePort.beginGetContractDocument(con,'676767');
     servicePort.beginGetContractDocumentMetaData(con,'98989');
     servicePort.begincreateContractSubmission(con,contractreqcms);
     //servicePort.begincalculateTerminationCharges(con,'7708',termchargesparam);
     servicePort.beginfindContractAmendmentsByAssociateNumber(con,'H112',2);
     servicePort.beginfindContractsByCustomerId(con,'78978978');
     servicePort.beginfindContractData(con,'545456456778',TRUE,1,1);
     servicePort.beginupdateContractSubmission(con,'1','4','3453453');
        Test.stopTest();
     }
}