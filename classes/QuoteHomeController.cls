public with sharing class QuoteHomeController {
    public String channelOutletId {get; set;}
    private String oldChannelOutletId {get; set;}
    public Boolean dealerUser {get; private set;}
    public Boolean displayCOIerror {get; private set;}
    
    private static User uAgent;
    private static User uDealer;
    
    public QuoteHomeController() {
        displayCOIerror = false;
        channelOutletId = QuotePortalUtils.getChannelOutletId();
        oldChannelOutletId = QuotePortalUtils.getChannelOutletId();
        dealerUser = QuotePortalUtils.isUserDealer();
    }
  
    private String[] greetings = new List<String>{'Hello, %u', 'Welcome back, %u'};
    
    public String UserGreeting {get {
        
        Datetime dt = Datetime.now();
        Integer hour = dt.hour();
        if (hour < 5) {
            greetings.add('Up late, %u?');
        } if (hour < 7) {
            greetings.add('Starting early, %u?');
        } else if (hour < 12) {
            greetings.add('Good morning, %u');
        } else if (hour < 18) {
            greetings.add('Good afternoon, %u');
        } else if (hour < 23) {
            greetings.add('Good evening, %u');
        } else if (hour >= 23) {
            greetings.add('Up late, %u?');
        }
        
        User u = [Select Name From User Where Id = :UserInfo.getUserId()];
        if (u != null) {
            Integer rI = 0;
            if (greetings.size() != 1) {
                rI = Integer.valueof(Math.ceil(Math.random() * (greetings.size())) - 1);
            }
            return greetings[rI].replace('%u', '<strong>' + u.Name + '</strong>');
        } else {
            return '';
        }
    }
    }
  
    public PageReference requireCOI() {
        /* Not until R6 guys!
        if (dealerUser != null && dealerUser == true) {
            if (channelOutletId == null || channelOutletId.trim() == '') {
                return Page.QuoteConfirmOutletId.setRedirect(true);
            } else if (ApexPages.currentPage().getCookies().get('confirmedOutletId') == null || ApexPages.currentPage().getCookies().get('confirmedOutletId').getValue() != '1') {
                return Page.QuoteConfirmOutletId.setRedirect(true);
            }
            PageReference pr = Page.QuoteConfirmOutletId;
            pr.setRedirect(true);
            return pr;
        } */
        return null;
    }
    public PageReference goHomeIfNotDealer() {
        if (dealerUser == null || dealerUser == false) {
            return Page.QuoteHome.setRedirect(true);
        }
        return null;
    }
    
    public static Boolean isValidCOI(String coi) {
        if (coi == null) { return false; }
        coi = coi.trim();
        String coiRegex = '.*\\d.*'; //'[?!\\dA-Za-z]+';
        Pattern coiPattern = Pattern.compile(coiRegex);
        Matcher coiMatcher = coiPattern.matcher(coi);
        String icoiRegex = '[\\dA-Za-z]*[^\\dA-Za-z]+[\\dA-Za-z]*';
        Pattern icoiPattern = Pattern.compile(icoiRegex);
        Matcher icoiMatcher = icoiPattern.matcher(coi);
        
        return (coiMatcher.matches() && !icoiMatcher.matches() && coi.length() == 10);
    }
    
    public PageReference onUpdateChannelOutletId() {
        displayCOIerror = false;
        if (isValidCOI(channelOutletId)) {
            Cookie coi = new Cookie('channelOutletId', channelOutletId, null, -1, false);
            Cookie ccoi = new Cookie('confirmedOutletId', '1', null, -1, false);
            ApexPages.currentPage().setCookies(new Cookie[]{coi, ccoi});
            PageReference pr = Page.QuoteHome;
            pr.setRedirect(true);
            return pr;
        }
        Cookie ccoi = new Cookie('confirmedOutletId', '0', null, -1, false);
        displayCOIerror = true;
        return null;
    }
  
    public PageReference onCancel() {
        PageReference pr = Page.QuoteHome;
        pr.setRedirect(true);
        return pr;
    }
    
  /* Traction - added 2/17/2012 for TELUS QT R5 */
  public QTAnnouncement[] getQTAs() {
    Quote_Tool_Announcement__c[] QTAOs = new List<Quote_Tool_Announcement__c>();
    QTAnnouncement[] QTAs = new List<QTAnnouncement>();
    
    User u = [Select Id, Channel__c From User Where Id = :UserInfo.getUserId()];
    
    String q = 'Select Name, Title__c, Content__c, Image__c, Icon__c, Channel__c, Display_From__c, Display_Until__c From Quote_Tool_Announcement__c';
    q += ' WHERE ((Display_From__c <= TODAY and Display_Until__c >= TODAY) or '
    + '(Display_From__c = null and Display_Until__c >= TODAY) or '
    + '(Display_From__c <= TODAY and Display_Until__c = null) or '
    + '(Display_From__c = null and Display_Until__c = null))';
    
    if (null != u.Channel__c) {
      q += ' AND (Channel__c includes (\'' + u.Channel__c + '\') OR Channel__c = null)';
    }
    q += ' ORDER BY Weight__c DESC NULLS LAST LIMIT 10';

    QTAOs = Database.query(q);
    
    for (Quote_Tool_Announcement__c QTAO : QTAOs) {
        QTAs.add(new QTAnnouncement(QTAO));
    }
    
    return QTAs;
  }
  
  public class QTAnnouncement {
    Public Quote_Tool_Announcement__c record {get; private set;}
    
    Public Id Id {get; private set;}
    Public String Title {get; private set;}
    Public String Content {get; private set;}
    Public String Image {get; private set;}
    Public String PubDate {get; private set;}
    Public String Icon {get; private set;}
    Public String IconAssetName {get; private set;}
   
    public QTAnnouncement(Quote_Tool_Announcement__c qtao) {
      record = qtao;
      Id = qtao.id;
      Title = qtao.title__c;
      Content = qtao.content__c;
      if ('<br>' == Content) { Content = null; }
      Image = qtao.image__c;
      if (null != qtao.Display_from__c) {
        PubDate = qtao.display_from__c.format();
      } else { PubDate = null; }
      Icon = '';
      IconAssetName = '';
      if (qtao.Icon__c == 'Green Checkmark') {IconAssetName = 'tick';}
      if (qtao.Icon__c == 'Green Plus Sign') {IconAssetName = 'plus';}
      if (qtao.Icon__c == 'Exclamation Mark') {IconAssetName = 'exclamation';}
      if (qtao.Icon__c == 'Exclamation Mark in Yellow Triangle') {IconAssetName = 'exclamation';}
      if (qtao.Icon__c == 'Red Minus Sign') {IconAssetName = 'minus';}
      if (qtao.Icon__c == 'Minus Sign in Red Circle') {IconAssetName = 'minus-circle';}
      if (qtao.Icon__c == 'X in Red Circle') {IconAssetName = 'cross-circle';}
      if (qtao.Icon__c == 'Red X') {IconAssetName = 'cross';}
    }
  }
  
    static testMethod void testSetup() {
        User u = [Select Id From User Where UserRole.Name = '# System Admin' And IsActive = true And UserType = 'Standard' And Profile.Name = 'System Admin TEAM' Limit 1];
        Account a = new Account(Name = 'Test', OwnerId = u.Id, BillingState = 'AB', phone = '1234567890');
        Insert a;
        Account a2 = new Account(Name = 'Test2', OwnerId = u.Id, BillingState = 'AB', phone = '1234567890');
        Insert a2;
        Contact c = new Contact(LastName = 'Test', OwnerId = u.Id, AccountId = a.Id, phone = '1234567890');
        Contact c2 = new Contact(LastName = 'Test2', OwnerId = u.Id, AccountId = a2.Id, phone = '1234567890');
        Insert c;
        Insert c2;
        Profile pAgent = [Select Id From Profile Where Name = 'Quote Tool - Agent' Limit 1];
        Profile pDealer = [Select Id From Profile Where Name = 'Quote Tool - Dealer' Limit 1];
        
        uAgent = new User(
            ContactId = c.Id,
            Channel__c = 'Outbound',
            EmailEncodingKey = 'ISO-8859-1',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            TimeZoneSidKey = 'America/Los_Angeles',
            LastName = 'AgentUser',
            Email = 'task-test1@example.com',
            Alias = 'Ttest1',
            Username = 'task-test1@example.com',
            ProfileId = pAgent.Id);
                
        uDealer = new User(
            ContactId = c2.Id,
            Channel__c = 'Dealer',
            Quoting_Dealer__c = true,
            EmailEncodingKey = 'ISO-8859-1',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            TimeZoneSidKey = 'America/Los_Angeles',
            LastName = 'DealerUser',
            Email = 'task-test2@example.com',
            Alias = 'Ttest2',
            Username = 'task-test2@example.com',
            ProfileId = pDealer.Id);
        Insert uAgent;
        Insert uDealer;
    }
  
    static testMethod void testSetupQTAs() {
        Quote_Tool_Announcement__c[] qtas = new List<Quote_Tool_Announcement__c>();
        Quote_Tool_Announcement__c qta1 = new Quote_Tool_Announcement__c(
            Title__c = 'QTA One',
            Content__c = 'The content of Example QTA One',
            Icon__c = 'Green Checkmark',
            Image__c = 'ImageOne',
            Display_From__c = Date.today().addDays(-1),
            Display_Until__c = Date.today().addDays(1),
            Channel__c = 'Inbound;Outbound;Inside Sales;Dealer;Assigned EE; Unassigned EE'
        ); qtas.add(qta1);
        Quote_Tool_Announcement__c qta2 = new Quote_Tool_Announcement__c(
            Title__c = 'QTA Two',
            Content__c = 'The content of Example QTA Two',
            Icon__c = 'Green Plus Sign',
            Image__c = 'ImageTwo',
            Display_From__c = Date.today().addDays(-1),
            Display_Until__c = Date.today().addDays(1),
            Channel__c = 'Dealer'
        ); qtas.add(qta2);
        Quote_Tool_Announcement__c qta3 = new Quote_Tool_Announcement__c(
            Title__c = 'QTA Three',
            Content__c = 'The content of Example QTA Three',
            Icon__c = 'Exclamation Mark',
            Image__c = 'ImageThree',
            Display_From__c = Date.today().addDays(-1),
            Display_Until__c = Date.today().addDays(1),
            Channel__c = 'Inbound;Outbound;Inside Sales;Assigned EE; Unassigned EE'
        ); qtas.add(qta3);
        Quote_Tool_Announcement__c qta4 = new Quote_Tool_Announcement__c(
            Title__c = 'QTA Four',
            Content__c = 'The content of Example QTA Four',
            Icon__c = 'Red Minus Sign',
            Image__c = 'ImageFour',
            Display_From__c = Date.today().addDays(-1),
            Display_Until__c = Date.today().addDays(1),
            Channel__c = 'Inbound;Outbound;Inside Sales;Assigned EE; Unassigned EE'
        ); qtas.add(qta4);
        Quote_Tool_Announcement__c qta5 = new Quote_Tool_Announcement__c(
            Title__c = 'QTA Five',
            Content__c = 'The content of Example QTA Five',
            Icon__c = 'Minus Sign in Red Circle',
            Image__c = 'ImageFive',
            Display_From__c = null,
            Display_Until__c = null,
            Channel__c = 'Inbound;Outbound;Inside Sales;Dealer;Assigned EE; Unassigned EE'
        ); qtas.add(qta5);
        Quote_Tool_Announcement__c qta6 = new Quote_Tool_Announcement__c(
            Title__c = 'QTA Six',
            Content__c = 'The content of Example QTA Six',
            Icon__c = 'X in Red Circle',
            Image__c = 'ImageSix',
            Display_From__c = null,
            Display_Until__c = Date.today().addDays(1),
            Channel__c = 'Inbound;Outbound;Inside Sales;Dealer;Assigned EE; Unassigned EE'
        ); qtas.add(qta6);
        Quote_Tool_Announcement__c qta7 = new Quote_Tool_Announcement__c(
            Title__c = 'QTA Seven',
            Content__c = 'The content of Example QTA Seven',
            Icon__c = 'Red X',
            Image__c = 'ImageSeven',
            Display_From__c = Date.today().addDays(-1),
            Display_Until__c = null,
            Channel__c = 'Inbound;Outbound;Inside Sales;Dealer;Assigned EE; Unassigned EE'
        ); qtas.add(qta7);
        Insert qtas;
    }
    static testMethod void qtaTestAgent() {
        testSetup();
        testSetupQTAs();
        Test.startTest();
        System.runAs(uAgent) {
            QuoteHomeController qhc = new QuoteHomeController();
            QTAnnouncement[] QTAs = qhc.getQTAs();
            System.assertEquals(QTAs.size(), 6);
        }
        Test.stopTest();
    }
    
    static testMethod void qtaTestDealer() {
        testSetup();
        testSetupQTAs();
        Test.startTest();
        System.runAs(uDealer) {
            QuoteHomeController qhc = new QuoteHomeController();
            QTAnnouncement[] QTAs = qhc.getQTAs();
            System.assertEquals(QTAs.size(), 5);
        }
        Test.stopTest();
    }
  
    static testMethod void homepageTestAgent() {
        testSetup();
        Test.startTest();
        System.runAs(uAgent) {
            QuoteHomeController qhc = new QuoteHomeController();
            System.assertEquals(qhc.onCancel().getUrl(), Page.QuoteHome.getUrl());
            System.assertEquals(qhc.dealerUser, false);
        }
        Test.stopTest();
    }
    
    static testMethod void homepageTestCOI() {
        testSetup();
        Test.startTest();
        System.runAs(uDealer) {
            QuoteHomeController qhc = new QuoteHomeController();
            System.assertNotEquals(null, qhc.UserGreeting);
            qhc.channelOutletId = '112233aa'; // Invalid COI
            System.assertEquals(null, qhc.onUpdateChannelOutletId());
            System.assertEquals(true, qhc.displayCOIerror);
            qhc.channelOutletId = '112233aa77'; // Valid COI is 10 alphanum
            System.assertNotEquals(null, qhc.onUpdateChannelOutletId());
            System.assertEquals(false, qhc.displayCOIerror);
        }
        Test.stopTest();
    }
    static testMethod void homepageTestGoHomeINDa() {
        testSetup();
        Test.startTest();
        System.runAs(uAgent) {
            QuoteHomeController qhc = new QuoteHomeController();
            System.assertNotEquals(null, qhc.goHomeIfNotDealer());
        }
        Test.stopTest();
    }
    static testMethod void homepageTestGoHomeINDd() {
        testSetup();
        Test.startTest();
        System.runAs(uDealer) {
            QuoteHomeController qhc = new QuoteHomeController();
            System.assertEquals(null, qhc.goHomeIfNotDealer());
        }
        Test.stopTest();
    }
    
    static testMethod void TestRequireCOI() {
        testSetup();
        Test.startTest();
        System.runAs(uDealer) {
            QuoteHomeController qhc = new QuoteHomeController();
            System.assertEquals(null, qhc.goHomeIfNotDealer());
        }
        System.runAs(uAgent) {
            QuoteHomeController qhc = new QuoteHomeController();
            System.assertEquals(null, qhc.requireCOI());
        }
        Test.stopTest();
    }
}