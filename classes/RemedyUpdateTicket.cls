public class RemedyUpdateTicket {
    
    public class sendRequest {
       
        public RemedyUpdateTicket.sendRequest SRequest(String CaseNumber, String CaseID, string pin, string ticketNumber, string RemUpdate, String Comment_id)
        {
            
            // Create the request envelope
            DOM.Document doc = new DOM.Document();
            //String endpoint = 'https://soa-mp-toll-st01.tsl.telus.com:443/SMO/ProblemMgmt/LynxTroubleTicketMgmtService_v1_0_vs0';
           //String endpoint = 'https://xmlgwy-pt1.telus.com:9030/dv/SMO/ProblemMgmt/ExternalTicketUpdate_v1_0_vs0';
            
            
            // TODO: Re-leverage custom setting for endpoint, no hard-coding
            String endpoint = SMBCare_WebServices__c.getInstance('ENDPOINT_Remedy_Update').Value__c;
            //String endpoint = 'https://xmlgwy.telus.com:9030/SMO/ProblemMgmt/LynxTroubleTicketMgmtService_v1_0_vs0';
            ////////////
             String soapNS = 'http://schemas.xmlsoap.org/soap/envelope/';
            string urn = 'urn:ExternalTicketSubmissionUpdate';
            //string asr = 'http://assurance.ebonding.telus.com';
            datetime creationDateTime = datetime.now();
            date mydate = creationDateTime.date();
            datetime getmytime = datetime.now();
            time mytime = getmytime.time();
             string username = 'salesforce';
            string password = 'SFDC2016';
            string assigned_group = 'SPOC FRONTLINE';
            string impact = '4-Minor/Localized';
            string reported_source = 'Self Service';
            String service_type = 'User Service Restoration';
            String status = 'New';
            String action = 'Update';
            String urgency = 'Item3';
            string testTime = string.valueOf(mydate)+'T'+string.valueOf(mytime);
            string createdate = string.valueOfGmt(creationDateTime);
            string logicalId = 'TELUS-SFDC';
            string messageId = logicalId + creationDateTime;
            String encodemessageId = EncodingUtil.base64Encode(Blob.valueOf(messageId));
            
            /*if (projectNum != '')
{
projectNum = 'CPS' + projectNum;
}*/
            
                      dom.XmlNode envelope
                = doc.createRootElement('Envelope', soapNS, 'soapenv');
            envelope.setNamespace('urn', urn);
            //envelope.setNamespace('asr', asr);
            
            
            dom.XmlNode header = envelope.addChildElement('Header', soapNS, null);
            dom.XmlNode AuthenticationInfo = header.addChildElement('AuthenticationInfo', urn, 'urn');
           
           // dom.XmlNode sender = AuthenticationInfo.addChildElement('sender', ebon, null);
            AuthenticationInfo.addChildElement('userName', urn, null).
                addTextNode(username);
            AuthenticationInfo.addChildElement('password', urn, null).
                addTextNode(password);
            AuthenticationInfo.addChildElement('authentication', urn, null).
                addTextNode('');
            AuthenticationInfo.addChildElement('locale', urn, null). 
                addTextNode('');
            AuthenticationInfo.addChildElement('timeZone', urn, null).
                addTextNode('');
           
         
            dom.XmlNode body = envelope.addChildElement('Body', soapNS, null);
            dom.XmlNode OpCreate = body.addChildElement('OpCreate', urn, 'urn');
            OpCreate.addChildElement('Action', urn, null).
                addTextNode(action);
            OpCreate.addChildElement('PIN', urn, null).
                addTextNode(pin);
            OpCreate.addChildElement('Ticket_Number', urn, null).
                addTextNode(ticketNumber);
             OpCreate.addChildElement('Customer_Update_Info', urn, null).
                addTextNode(RemUpdate);
            System.debug(doc.toXmlString());
            
            // TODO: add exception handling
            
            // Send the request
            HttpRequest req = new HttpRequest();
            req.setMethod('POST');
            req.setEndpoint(endpoint);
            // TODO: specify a timeout of 2 minutes 
           req.setTimeout(120000);
            
            String creds;
         SMBCare_WebServices__c wsUsername = SMBCare_WebServices__c.getValues('username_pc');
         SMBCare_WebServices__c wsPassword = SMBCare_WebServices__c.getValues('password');
          
            if (String.isNotBlank(wsUsername.Value__c)&& String.isNotBlank(wsPassword.Value__c))
                creds = wsUsername.Value__c+':'+wsPassword.Value__c;
            String encodedusernameandpassword = EncodingUtil.base64Encode(Blob.valueOf(creds));
            
            //Blob headerValue = Blob.valueOf(encodedusernameandpassword);
            String authorizationHeader = 'BASIC ' + encodedusernameandpassword;
            // EncodingUtil.base64Encode(headerValue);
            req.setHeader('Authorization', authorizationHeader);
            req.setHeader('Content-Type', 'text/xml');
            
            req.setBodyDocument(doc);
            
            // TODO: Determine the level of detail webservice is returning in regards to errors.
            
            Http http = new Http();
            HttpResponse res = http.send(req);
            //System.assertEquals(500, res.getStatusCode());
            
 //list<Remedy_Trouble_Ticket.TroubleTicket> XMLReturn = res.getBody();           
            System.debug('GetBody line 110:' +res.getBody());
            // System.assertEquals(200, res.getStatusCode());
            
            dom.Document resDoc = res.getBodyDocument();
            XmlStreamReader reader = res.getXmlStreamReader();
            
            // Read through the XML 
            String OKTicketId = '';
           String DescriptionView1 = '';
           String ClientPhoneView = '';
           while(reader.hasNext()) { 
                System.debug('Event Type:' + reader.getEventType()); 
                if (reader.getEventType() == XmlTag.START_ELEMENT) { 
                    System.debug('getLocalName: ' +reader.getLocalName());
                    if ('Client_Phone' == (reader.getLocalName())){
                        boolean isSafeToGetNextXmlElement = true; 
                        while(isSafeToGetNextXmlElement) { 
                           if (reader.getEventType() == XmlTag.END_ELEMENT) {
                                OKTicketId = reader.getLocalName();
                                System.debug('OK Ticket Element Line 128 :' + OKTicketId);
                                break; 
                            } else if (reader.getEventType() == XmlTag.CHARACTERS) { 
                                ClientPhoneView = reader.getText();
                                System.debug('ClientPhoneView Line 132 :' + ClientPhoneView);
          
                            } 
         
                            if (reader.hasNext()) { 
                                
                                reader.next(); 
                                
                            } else { 
                                
                                isSafeToGetNextXmlElement = false; 
                                break; 
                            } 
                        } 
                        
                    }
                    
 if ('Description' == (reader.getLocalName())){
                        boolean isSafeToGetNextXmlElement = true; 
                        while(isSafeToGetNextXmlElement) { 
                           if (reader.getEventType() == XmlTag.END_ELEMENT) {
                                OKTicketId = reader.getLocalName();
                                System.debug('OK Ticket Element Line 154 :' + OKTicketId);
                                break; 
                            } else if (reader.getEventType() == XmlTag.CHARACTERS) { 
                                DescriptionView1 = reader.getText();
                                System.debug('Description Line 158 :' + DescriptionView1);
          
                            } 
         
                            if (reader.hasNext()) { 
                                
                                reader.next(); 
                                
                            } else { 
                                
                                isSafeToGetNextXmlElement = false; 
                                break; 
                            } 
                        } 
                        
                    }                    
                    
                    
                }
                reader.next(); 
            } 
            
            dom.XMLNode TroubleTicket = resDoc.getRootElement();
           system.debug('TroubleTicketDOM: '+TroubleTicket);
            //  String troubleTick = 'http://www.telus.com/schema/servicestatus/troubleticket';
            //String wsa = 'http://schemas.xmlsoap.org/soap/envelope/';
           // Dom.XMLNode TroubleTicket = resDoc.getRootElement();
           // String DescriptionView = TroubleTicket.getChildElement('Description',null).getText();
            //system.debug('Description: '+DescriptionView);
            // dom.XmlNode header1 = envelope.getChildElement('Header', soapNS);
            // String TELUSTicketId = TroubleTicket.getChildElement('TELUSTicketId',null).getText();
            //System.debug('TELUSTicketId: ' + TELUSTicketId);
            System.debug(resDoc.toXmlString());
            //string XMLReturn = resDoc.toXmlString();
            System.debug('Envelope=:' + envelope);
            //  System.debug('TroubleTicket=:' + TroubleTicket);
            //System.debug(header1);
            //System.debug(testTime);
           
            return null;
        }
        
 }


}