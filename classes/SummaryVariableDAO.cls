public class SummaryVariableDAO {
	private static final Schema.SObjectField[] DEFAULT_VARIABLE_FIELDS = new Schema.SObjectField[]{
		SBQQ__SummaryVariable__c.SBQQ__TargetObject__c, SBQQ__SummaryVariable__c.SBQQ__AggregateFunction__c,
		SBQQ__SummaryVariable__c.SBQQ__AggregateField__c, SBQQ__SummaryVariable__c.SBQQ__FilterField__c,
		SBQQ__SummaryVariable__c.SBQQ__Operator__c, SBQQ__SummaryVariable__c.SBQQ__FilterValue__c,
		SBQQ__SummaryVariable__c.SBQQ__ConstraintField__c, SBQQ__SummaryVariable__c.SBQQ__CombineWith__c
	};
	
	public SBQQ__SummaryVariable__c[] loadByIds(Set<Id> variableIds) {
		QueryBuilder query = createVariableQuery();
		query.getWhereClause().addExpression(query.inExpr(SBQQ__SummaryVariable__c.Id, variableIds));
		SBQQ__SummaryVariable__c[] vars = Database.query(query.buildSOQL());
		
        Set<Id> combinedVarIds = new Set<Id>(); 
        for (SBQQ__SummaryVariable__c var : vars) {
            if (var.SBQQ__CombineWith__c != null) {
                combinedVarIds.add(var.SBQQ__CombineWith__c);
            }
        }
        if (!combinedVarIds.isEmpty()) {
            vars.addAll(loadByIds(combinedVarIds));
        }
        return vars;
	}
	
	private QueryBuilder createVariableQuery() {
		QueryBuilder query = new QueryBuilder(SBQQ__SummaryVariable__c.sObjectType);
		query.getSelectClause().addFields(DEFAULT_VARIABLE_FIELDS);
		return query;
	}
}