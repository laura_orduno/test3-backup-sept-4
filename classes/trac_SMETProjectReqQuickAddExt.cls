public with sharing class trac_SMETProjectReqQuickAddExt {
    public ID parentId {get; private set;}
    private SMET_Requirement__c[] existingItems; // loaded from DB
    public Map<Integer,SMET_Requirement__c> cart {get; set;} // container
    private SMET_Requirement__c[] itemsForDeletion;
    public Integer itemToDelete {get; set;} // id of item where "delete" was clicked
    public String errorLevel {get; set;}
	public String messageName {get; set;}
    
    public trac_SMETProjectReqQuickAddExt(ApexPages.StandardController controller) {
        parentId = controller.getId();
        initialize();
    }
    
    public void showMessage() {
		if(errorLevel == 'WARNING') {
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, messageName));
  		}
	}
    
    private void initialize() {
        cart = new Map<Integer,SMET_Requirement__c>();
        itemsForDeletion = new SMET_Requirement__c[]{};
        existingItems = [SELECT name, Order__c, status__c, priority__c, requirement_detail__c, Accountable__c, Assigned_To__c, End_Date__c, Start_Date__c, owner.alias
                        FROM SMET_Requirement__c
                        WHERE Related_SMET_Project__c = :parentId
                        ORDER BY Order__c NULLS LAST, Status__c DESC, Priority__c];
        
        // populate the cart with current records               
        if (existingItems.size() > 0) {
            Integer itemId = 0;
            for (SMET_Requirement__c i : existingItems) {
                cart.put(itemId,i);
                itemId++;
            }
        } else { // no records, pre-add a blank one
            addItem();
        }
    }
    
    public void addItem() {
        Integer itemId = 0;
        
        // find the largest itemId in the current map. Ugly, but it works
        List<Integer> itemIds = new List<Integer>();
        itemIds.addAll(cart.keySet());
        if (itemIds.size() > 0) {
            itemIds.sort();
            itemId = itemIds[itemIds.size() - 1] + 1; // new itemId continues incrementing where previous one left off
        }
        
        cart.put(itemId,new SMET_Requirement__c(Related_SMET_Project__c = parentId, Status__c = 'New', Priority__c = 'Standard', requirement_detail__c = ''));
    }
    
    public void addDefaultItems() {
        Integer itemId = 0;
        
        // find the largest itemId in the current map. Ugly, but it works
        List<Integer> itemIds = new List<Integer>();
        itemIds.addAll(cart.keySet());
        if (itemIds.size() > 0) {
            itemIds.sort();
            itemId = itemIds[itemIds.size() - 1] + 1; // new itemId continues incrementing where previous one left off
        }
        
        Decimal lastItem = (cart.get(itemIds[itemIds.size() - 1])).Order__c;
		if(lastItem==null) lastItem=1;
        
        //get entries from custom settings
        List<CloudProjectKeyDeliverableTemplate__c> entries = CloudProjectKeyDeliverableTemplate__c.getAll().values();
        if(entries.size()>0){
	        for(integer i=0;i<entries.size();i++){
	        	for(integer j=i; j<entries.size();j++){
	        		if(entries.get(j).Order__c < entries.get(i).Order__c){
	        			CloudProjectKeyDeliverableTemplate__c tempObj = entries.get(j);
	        			entries.set(j,entries.get(i));
	        			entries.set(i,tempObj);
	        		}	
	        	}
	        }
	        
	        for(CloudProjectKeyDeliverableTemplate__c temp: entries){
		        lastItem++;
		        cart.put(itemId,new SMET_Requirement__c(Order__c = lastItem, Name = temp.KD_Name__c, Related_SMET_Project__c = parentId, Status__c = 'New', Priority__c = 'Standard', requirement_detail__c = ''));
		        itemId++;
	        }
        }
    }
    
    public PageReference removeFromCart() {    
        if(cart.get(itemToDelete).id != null) {
            itemsForDeletion.add(cart.get(itemToDelete)); // existing shoot line item, needs to be deleted from DB
        }
        cart.remove(itemToDelete);
        if(cart.size() == 0) { // if the last item has been removed, add a blank one
            addItem();
        }
        system.debug('delete size: '+itemsForDeletion.size());
        return null;
    }
    
    public PageReference undo() {
        initialize();
        return null;
    }
    
    public PageReference onSave() {
        List<SMET_Requirement__c> newItems = cart.values();
        system.debug('delete size: '+itemsForDeletion.size());
        if(itemsForDeletion.size()>0) {
            try {
                delete(itemsForDeletion);
                itemsForDeletion.clear(); // in case there is an error later on, make sure we clear the list to prevent trying to delete a 2nd time -- GA
            } catch(Exception e) {
                ApexPages.addMessages(e);
            }
        }
        
        
        try {
            upsert(newItems);
            return null;
        } catch(Exception e) {
            ApexPages.addMessages(e);
            return null;
        }
        
        
    }

}