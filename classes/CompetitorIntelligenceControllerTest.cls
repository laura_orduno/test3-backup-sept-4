@isTest(SeeAllData=true)
public class CompetitorIntelligenceControllerTest {
		testMethod static void testCompetitorIntelligenceController() {
        Account acc = TestUtil.createAccount();
        Opportunity opp = TestUtil.createOpportunity(acc.Id);
        Opportunity_Solution__c solution = TestUtil.createOpportunitySolution(opp.Id);
        List<Opp_Product_Item__c> items = TestUtil.createPLIList(opp.Id);
        Competitor_Intelligence__c ci = new Competitor_Intelligence__c(Competitor_Service__c = 'Competitor Service',
                                                                      Sales_Opportunity__c = opp.id,
                                                                      Product_ID__c = items.get(0).product__c);
        
        PageReference pageRef = Page.CompetitorIntelligenceProductPicker;
        Test.setCurrentPage(pageRef);
            
        ApexPages.currentPage().getParameters().put('ciid', ci.id);
        
        CompetitorIntelligenceProductController c = new CompetitorIntelligenceProductController();
            
        c.getSearchOptions();
        
        c.component.searchText = 'BPC1';
        c.component.searchOption = 'BPC_1__c';
        c.searchProducts();
        c.getSearchResults();
        c.bpc1 = 'BPC11';
        c.bpc1Selected();
        
        c.bpc2 = 'BPC21';
        c.bpc2Selected();

        c.bpc3 = 'BPC31';
        c.bpc3Selected();

        c.selectedProduct = items.get(0).product__c;
        c.selectedProductName = 'Test';
        System.debug('items.get(0): ' + items.get(0));
        c.selectProduct();
        //c.save();

//with pli
        pageRef = Page.CompetitorIntelligenceProductPicker;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('ciid', ci.id);
        ApexPages.currentPage().getParameters().put('pliId', items.get(0).Id);
        c = new CompetitorIntelligenceProductController();

        c.searchProducts();
        c.component.searchText = 'B';
        c.searchProducts();
        
        c.component.searchText = 'BPC1';
        c.component.searchOption = 'BPC_1__c';
        
        c.searchProducts();
        c.bpc1 = 'BPC11';
        c.bpc1Selected();
        
        c.bpc2 = 'BPC21';
        c.bpc2Selected();

        c.bpc2 = 'BPC31';
        c.bpc3Selected();

        c.selectedProduct = items.get(0).product__c;
        c.selectedProductName = 'Test';
        System.debug('items.get(0): ' + items.get(0));
        c.selectProduct();
        //c.save();
        
        //wrong id - throw an erroir
        c.selectedProduct = items.get(0).Id;
        c.selectProduct();
        
        //call std methods
        c.component.searchOption = 'BPC_2__c';
        c.searchProducts();

        c.component.searchOption = 'BPC_3__c';
        c.searchProducts();

        c.cancel();
        c.addProduct();
    }
}