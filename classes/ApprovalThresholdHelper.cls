public with sharing class ApprovalThresholdHelper {
    public static final String RELEASE_CODE_THRESHOLD = 'Release Code Threshold';
        
    public static final String CONTROLLER_THRESHOLD = 'Controller Threshold';
    
    private static Map<ThresholdFilter ,List<ApprvalThreshold>> releaseCodeMap =  new Map<ThresholdFilter, List<ApprvalThreshold>>();
    
    private static Map<String,Approval_Threshold__c> controllerMap =  new Map<String, Approval_Threshold__c>();
    
    public static void loadThresholdList(Set<ThresholdFilter> filterList){
        List<String> transtypeLits = new List<String>();
        List<String> buLits = new List<String>{'Default'};
        for (ThresholdFilter filter :filterList){
            transtypeLits.add(filter.TransactionType);
            buLits.add(filter.businessUnit);
        }   
        List<Approval_Threshold__c> thresholdList =  [select Transaction_Type__c ,Business_unit__c,Release_Code_0_Threshold__c, 
                                                 Release_Code_1_Threshold__c,Release_Code_2_Threshold__c,Release_Code_3_Threshold__c,
                                                 Release_Code_4_Threshold__c,Release_Code_5_Threshold__c,Release_Code_6_Threshold__c,
                                                 Release_Code_7_Threshold__c,Release_Code_8_Threshold__c,Release_Code_9_Threshold__c,
                                                 Release_Code_10_Threshold__c,     
                                                 BU_Threshold__c, Corporate_Threshold__c,Type__c, VP_Threshold__c
                                                 from Approval_Threshold__c                                                     
                                                 where Transaction_type__c in :transtypeLits or Business_unit__c in :buLits];
        
        for (Approval_Threshold__c threshold : thresholdList){
            if(threshold.Type__c == ApprovalThresholdHelper.RELEASE_CODE_THRESHOLD ){
                 loadReleaseCodeMap(threshold);
            }else if(threshold.Type__c == ApprovalThresholdHelper.CONTROLLER_THRESHOLD){
                 loadControlerMap(threshold);
            }
         }
    }
        
    private static void loadReleaseCodeMap(Approval_Threshold__c AppThreshold){
        ThresholdFilter filter = new ThresholdFilter(AppThreshold.Transaction_type__c ,AppThreshold.Business_unit__c );
        List<ApprvalThreshold> apprvalRCThresholdList = new List<ApprvalThreshold>();
        apprvalRCThresholdList.add(new ApprvalThreshold(1,AppThreshold.Release_Code_1_Threshold__c));apprvalRCThresholdList.add(new ApprvalThreshold(2,AppThreshold.Release_Code_2_Threshold__c));
        apprvalRCThresholdList.add(new ApprvalThreshold(3,AppThreshold.Release_Code_3_Threshold__c));apprvalRCThresholdList.add(new ApprvalThreshold(4,AppThreshold.Release_Code_4_Threshold__c));
        apprvalRCThresholdList.add(new ApprvalThreshold(5,AppThreshold.Release_Code_5_Threshold__c));apprvalRCThresholdList.add(new ApprvalThreshold(6,AppThreshold.Release_Code_6_Threshold__c));
        apprvalRCThresholdList.add(new ApprvalThreshold(7,AppThreshold.Release_Code_7_Threshold__c));apprvalRCThresholdList.add(new ApprvalThreshold(8,AppThreshold.Release_Code_8_Threshold__c));
        apprvalRCThresholdList.add(new ApprvalThreshold(9,AppThreshold.Release_Code_9_Threshold__c));apprvalRCThresholdList.add(new ApprvalThreshold(0,AppThreshold.Release_Code_0_Threshold__c));
        apprvalRCThresholdList.add(new ApprvalThreshold(10,AppThreshold.Release_Code_10_Threshold__c));
        apprvalRCThresholdList.sort();
        releaseCodeMap.put(filter, apprvalRCThresholdList);
    }
    
    public static void loadControlerMap(Approval_Threshold__c AppThreshold){
        controllerMap.put(AppThreshold.Transaction_type__c,AppThreshold);
    }
    
    public static Map<String ,Approval_Threshold__c> getControllerMap(){
        return controllerMap;
    }
    
    public static Map<ThresholdFilter ,List<ApprvalThreshold>> getReleaseCodeMap(){
        return releaseCodeMap;
    }
    
    public static  Integer getRequiredReleaseCode(Decimal amount, String transactionType,String businessUnit, Id nextApprover, List<Approval_Threshold__c> individualThresholds) {
        System.debug('@!amount :' + amount + ' transactionType: ' + transactionType+ ' businessUnit: '+ businessUnit);
        System.debug('individualThresholds ::: ' + individualThresholds);
        Approval_Threshold__c individualThreshold = null;
        for(Approval_Threshold__c currentIT : individualThresholds){
            if(nextApprover.equals(currentIT.Individual_User__c) && businessUnit.equals(currentIT.Business_Unit__c) && transactionType.equals(currentIT.Transaction_Type__c)){
                individualThreshold = currentIT;
            }
        }

        ThresholdFilter filter = new ThresholdFilter(transactionType, businessUnit);
        ThresholdFilter defaultItem = new ThresholdFilter(transactionType, 'Default');
        List<ApprvalThreshold> apprvalRCThresholdList = releaseCodeMap.get(filter);
        if(apprvalRCThresholdList == null){
            apprvalRCThresholdList = releaseCodeMap.get(defaultItem);
        
        }
        
        if(apprvalRCThresholdList !=null){
             for(ApprvalThreshold apprvalThreshold : apprvalRCThresholdList){
                if(individualThreshold != null && individualThreshold.Individual_User_Release_Code__c == apprvalThreshold.releaseCode){
                    if(individualThreshold.Individual_Threshold__c>= amount){
                         return Integer.valueOf(individualThreshold.Individual_User_Release_Code__c);
                     }
                }else{
                     if(apprvalThreshold.Threshold>= amount){
                         return apprvalThreshold.releaseCode;
                     }
                 }
             }
            return 10;
        } 
        return null;
    }
    
    public static Decimal getBusinessUnitThreshold(String transactionType) {
         Approval_Threshold__c controllerThreshold = controllerMap.get(transactionType);
        if(controllerThreshold!=null){
            return controllerThreshold.BU_Threshold__c;
        }
        return null;
    }
        
    public static Decimal getCorporateThreshold(String transactionType) {
         Approval_Threshold__c controllerThreshold = controllerMap.get(transactionType);
        if(controllerThreshold!=null){
            return controllerThreshold.Corporate_Threshold__c;
        }
        return null;
    }

    /**
     * getVPThreshold
     * @description Returns the VP Threshold value
     * @author Thomas Tran, Traction on Demand
     * @date FEB-22-2016
     */
    public static Decimal getVPThreshold(String transactionType) {
        Approval_Threshold__c controllerThreshold = controllerMap.get(transactionType);
        if(controllerThreshold!=null){
            return controllerThreshold.VP_Threshold__c;
        }
        return null;
    }
    
    public static boolean needBUControllerApproval(Decimal amount, String transactionType){
        if(getBusinessUnitThreshold(transactionType)==null)
          return false;
        return amount > getBusinessUnitThreshold(transactionType);
    }
        
    public static boolean needCorporateControllerApproval(Decimal amount, String transactionType){
        if(getCorporateThreshold(transactionType)==null)
          return false;
        return  amount > getCorporateThreshold(transactionType);   
    }

    /**
     * needVPControllerApproval
     * @description Determines if the current flow requires the VP Approval
     * @author Thomas Tran, Traction on Demand
     * @date FEB-22-2016
     */
    public static Boolean needVPControllerApproval(Decimal amount, String transactionType){
        if(getVPThreshold(transactionType)==null)
          return false;
        
        return  amount > getVPThreshold(transactionType);   
    }

   public class ApprvalThreshold implements Comparable{
        Integer releaseCode {get;set;}
        Decimal threshold {get;set;}
        
        public ApprvalThreshold(integer rc, Decimal threshold ){
            this.releaseCode= rc;
            this.threshold= threshold;
        }
        
        public Integer compareTo(Object compareTo) {
            ApprvalThreshold compareToOther = (ApprvalThreshold)compareTo;
            Integer returnValue = 0;
            if (this.releaseCode > compareToOther.releaseCode) {
                returnValue = 1;
            } else if (this.releaseCode < compareToOther.releaseCode) {
                returnValue = -1;
            }
            return returnValue;       
        }
        
        public Integer hashCode() {
            return (31 * releaseCode) ^ (Integer) threshold;
        }
    }
       
    public class ThresholdFilter{
        public String TransactionType {get;set;}
        public String BusinessUnit{get;set;}
         
        public ThresholdFilter(String transactionType, String businessUnit){
            this.transactionType = transactionType;
            this.businessUnit = businessUnit;
            
        }
        
        public Boolean equals(Object obj) {
            if (obj instanceof ThresholdFilter) {
                ThresholdFilter p = (ThresholdFilter)obj;
                return (TransactionType.equals(p.TransactionType)) && (businessUnit.equals(p.businessUnit));
            }
            return false;
        }
        public Integer hashCode() {
            return (31 * TransactionType.hashCode()) ^ BusinessUnit.hashCode();
        }
    }   

}