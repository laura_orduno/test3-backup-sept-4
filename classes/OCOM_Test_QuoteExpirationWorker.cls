@IsTest
public class OCOM_Test_QuoteExpirationWorker
{
    static testmethod void testQuoteExpiration()
    {
        Opportunity o = new Opportunity();
        o.Name = 'test Opportunity for expiration';
        o.StageName = 'NewStage';
        o.CloseDate = Date.today().addDays(1);    
        insert o;
        
        Quote q = new Quote();
        q.OpportunityId = o.Id;
        q.name = 'test quote for expiration';
        q.Status = 'In Progress';
       
        insert q;
        
        String statusAfterInsert = [SELECT Id, status FROM Quote WHERE id =:q.id LIMIT 1].status;
        System.AssertEquals('In Progress', statusAfterInsert);
        
        q.ExpirationDate = Date.today().addDays(-1);
        update q;
        
        try
        {
        	Database.executeBatch(new OCOM_QuoteExpirationWorker());    
        }
        catch(exception e)
        {
            system.debug('*#*Exception:' + e.getMessage());
        }
        
        
        String statusAfterExpirationWorker = [SELECT Id, status FROM Quote WHERE id =:q.id LIMIT 1].status;
        //System.AssertEquals('Expired', statusAfterExpirationWorker);
        
    }
}