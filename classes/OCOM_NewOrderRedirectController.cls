public without sharing class OCOM_NewOrderRedirectController {
    private Id accountId{get;set;}
    
    public OCOM_NewOrderRedirectController() {
        accountId = System.currentPageReference().getParameters().get('accountId');
    }
    
    public PageReference redirect() {
        PageReference pageRef;
        system.debug('OCOM_NewOrderRedirectController::redirect() VlocityLicense(' + userinfo.isCurrentUserLicensed('vlocity_cmt') + ')');
        /* -----------------------------------------TEMPORARY
         * Please note that this code is to be removed once all users have been onboarded
         * The following code will verify if a user belongs to the pilot group of users
         * If they are, then they get the new Vlocity order flow
         * If they are not, then they will get the old (Interim) order flow 
         */
        integer numMembers=0;
        try{
            numMembers=[select count() from groupmember where group.developername='OCOM_Pilot_Users' and userorgroupid=:userinfo.getuserid()];                
        }catch(exception e){
            numMembers=0;
        }
        system.debug('OCOM_NewOrderRedirectController::redirect() numMembers(' + numMembers + ')');
        if (userinfo.isCurrentUserLicensed('vlocity_cmt') && numMembers>0) {
            system.debug('OCOM_NewOrderRedirectController::redirect() has license');
            pageRef = new PageReference('/apex/OCOM_InOrderFlowPac?aid=' + accountId);
        } else {
            system.debug('OCOM_NewOrderRedirectController::redirect() no license');
            pageRef = new PageReference('/apex/smb_newQuoteFromAccount?aid=' + accountId);
        }
        return pageRef;
    }
}