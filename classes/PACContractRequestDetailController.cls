global class PACContractRequestDetailController {
    public AddressData PAData  { get; set; }
    Contracts__c saObj;
    public String isError{ get; set; }
    public Map<String,String> countryMap = new Map<String,String>(); 
    public Map<String,String> provinceCodeMap = new Map<String,String>();   
    public String jsonMap{get;set;}
    
    
    public PACContractRequestDetailController(ApexPages.StandardController controller)
    {
            isError = 'onLoad';
            PAData = new AddressData();
            

        List<Country_Province_Codes__c> allData = Country_Province_Codes__c.getAll().values();
        
        if(allData != null){
            for (Country_Province_Codes__c obj: allData){
                if (obj.Type__c == 'Country'){
                    countryMap.put(obj.Description__c,obj.Name);
                    //Added by Jaya
                    //countryWithoutOpenBracket = obj.Description__c
                    //countryMapNameDescription.put(obj.Name,obj.Description__c.replaceAll('/[^\w_ ]/g','');
                    //countryMapNameDescription.put(obj.Name,obj.Description__c.replaceAll('\\(','').replaceAll('\\)','').replaceAll(',','').replaceAll("'",''));
                    //jsonMap = JSON.serialize(countryMapNameDescription);
                    //countryMapNameDescription.put(obj.Name,obj.Description__c);                    
                }
                else if(obj.Type__c == 'Canadian Province')
                {
                    provinceCodeMap.put(obj.Description__c,obj.Name);
                }
            }
        }

            PAData.isAddressLine1Required = true;
            PAData.isProvinceRequired = true;
            PAData.isCityRequired = true;
            saObj = (Contracts__c)controller.getRecord(); //Billing_City1__c,Billing_Country1__c,Billing_Postal_Code1__c,Billing_Province1__c,Billing_Street1__c 
            saObj = [SELECT id,Contract_Billing_City__c,Contract_Billing_Country__c,
                    Contract_Billing_Postal_Code__c , 
                    Contract_Billing_Province__c  , Contract_Billing_Street__c
                            FROM Contracts__c
                            WHERE Id=: saObj.Id];
                            
            //,Billing_Address_Status__c
            if(saObj != null )
            {
                
                       // PAData.searchString = saObj.Billing_Street1__c+''+saObj.Billing_City1__c +''+saObj.Billing_Province1__c +''+saObj.Billing_Country1__c+''+
                      //  saObj.Billing_Postal_Code1__c;
               PAData.searchString= '';
                       // if(saObj.Billing_Address_Status__c == System.Label.PACInValidCaptureNewAddrStatus){
                            //PAData.isManualCapture = true;
                       // }else{
                            PAData.isManualCapture = false;
                       // }

                            if (saObj.Contract_Billing_Street__c!= null)
                            {
                                PAData.addressLine1 = saObj.Contract_Billing_Street__c;
                            }
                            if (saObj.Contract_Billing_City__c!= null)
                            {
                                PAData.city  = saObj.Contract_Billing_City__c;
                            }
                              if (saObj.Contract_Billing_Province__c!= null)
                            {
                                //Changed by Jaya - If the country in the Contract Request obj is not the code but the description
                                //then the defaulting does not work.
                                //PAData.canadaProvince = saObj.Contract_Billing_Province__c;
                                PAData.canadaProvince = provinceCodeMap.get(saObj.Contract_Billing_Province__c);
                                 PAData.usState = saObj.Contract_Billing_Province__c;
                            PAData.province = saObj.Contract_Billing_Province__c;
                            }
                              if (saObj.Contract_Billing_Country__c!= null )
                            {
                                //Changed by Jaya - If the country in the Contract Request obj is not the code but the description
                                //then the defaulting does not work. 
                                
                                //PAData.country = saObj.Contract_Billing_Country__c;
                                PAData.country = countryMap.get(saObj.Contract_Billing_Country__c);
                                //countryMap.get(saObj.Contract_Billing_Country__c);
                            }
                            else
                            {
                                PAData.country='CAN';
                            }
                              if (saObj.Contract_Billing_Postal_Code__c!= null)
                            {
                                PAData.postalCode = saObj.Contract_Billing_Postal_Code__c;
                            }
                    
                   
        }
        else
        {
         //PAData.country='Canada';
        }
        System.debug('COntractRequest >>> data' +  PAData);
        
    }
    
    @RemoteAction
    global static String[] getCountryDescription(String countryCode,String provinceCode)
    {       
        //Added by Jaya
        Map<String,String> countryMapNameDescription = new Map<String,String>();
        Map<String, String> provinceMap = new Map<String, String>();
        
        String[] resultArray = new String[]{};
        List<Country_Province_Codes__c> allCSData = Country_Province_Codes__c.getAll().values();        

        if(allCSData != null){
            for (Country_Province_Codes__c obj: allCSData){
                if (obj.Type__c == 'Country'){
                    countryMapNameDescription.put(obj.Name,obj.Description__c);
                }
                else if(obj.Type__c == 'Canadian Province')
                {
                    provinceMap.put(obj.Name,obj.Description__c);
                }
            }
       }
         
        resultArray.add(countryMapNameDescription.get(countryCode));
        resultArray.add(provinceMap.get(provinceCode));         
        return resultArray;
    }
     /**
    * Name - mapAddress
    * Description - Update Billing address in existing Billing_Address__c field
    * Param - 
    * Return type - void
    **/
    
    public pageReference updateBillingAddress (){
        mapAddress();
        try{
            isError = 'inSave';
            update saObj;
        }catch(Exception ex){
            isError = 'InError';
            ApexPages.addMessages(ex) ;
        }
       
        return null;
    }
    
    /**
    * Name - mapAddress
    * Description - Map Billing address with existing Billing_Address__c field
    * Param - 
    * Return type - void
    **/
    public void mapAddress(){
        saObj.Contract_Billing_Street__c  = PAData.addressLine1.trim();
        saObj.Contract_Billing_City__c =PAData.city.trim();
        saObj.Contract_Billing_Province__c =PAData.province;
        saObj.Contract_Billing_Country__c=PAData.country;
        saObj.Contract_Billing_Postal_Code__c =PAData.postalCode;
        
    }
}