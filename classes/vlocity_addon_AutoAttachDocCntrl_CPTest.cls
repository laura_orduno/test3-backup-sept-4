@IsTest
public class vlocity_addon_AutoAttachDocCntrl_CPTest {
    static testmethod void autoCreateContractUnitTest(){
        
        vlocity_cmt__CustomFieldMap__c lstCustomFieldMap = new vlocity_cmt__CustomFieldMap__c();
        lstCustomFieldMap.Name ='Offer_House_De>Contract2';
        lstCustomFieldMap.vlocity_cmt__DestinationFieldName__c = 'vlocity_cmt__OriginalContractId__c';
        lstCustomFieldMap.vlocity_cmt__DestinationSObjectType__c = 'Contract';
        lstCustomFieldMap.vlocity_cmt__SourceFieldName__c ='Existing_Contract__c';
        lstCustomFieldMap.vlocity_cmt__SourceFieldSObjectType__c = 'Contract';
        lstCustomFieldMap.vlocity_cmt__SourceFieldType__c = 'reference';
        lstCustomFieldMap.vlocity_cmt__SourceSObjectType__c = 'Offer_House_Demand__c';
        insert lstCustomFieldMap;
        TestDataHelper.testContractCreationwithDealSupport();
        createRelatedListItems(TestDataHelper.testDealSupport.Id);
        
        vlocity_cmt__ContractLineItem__c contractLineItem = new vlocity_cmt__ContractLineItem__c(vlocity_cmt__RecurringTotal__c=9,
                                             vlocity_cmt__ContractId__c=TestDataHelper.testContractObj.id,
                                             Monthly_Variable_Total__c=10,vlocity_cmt__OneTimeTotal__c=12,
                                             name='test',vlocity_cmt__OneTimeCharge__c=2,
                                             vlocity_cmt__Quantity__c=12);
        insert contractLineItem;
        
        TestDataHelper.testContractCreationwithDealSupport();
        TestDataHelper.testContractVersionCreation();
        TestDataHelper.testCustomSettingCreation();
        TestDataHelper.testvlocityDocTemplateteCreation();
        
        Test.startTest();
        Apexpages.currentPage().getparameters().put('isInConsole','true');
        Apexpages.currentPage().getparameters().put('id',TestDataHelper.testContractObj.id);
       // ApexPages.currentPage().getParameters().put('isUpdate','true');
        vlocity_addon_AutoAttachDocCntrl_CP objvlocity_addon_AutoAttachDocCntrl_CP=new vlocity_addon_AutoAttachDocCntrl_CP(new vlocity_cmt.ContractDocumentCreationController());
        boolean isInConsole=objvlocity_addon_AutoAttachDocCntrl_CP.isInConsole;
        system.assertEquals(isInConsole, true);
        objvlocity_addon_AutoAttachDocCntrl_CP.attachDoc();
       //objvlocity_addon_AutoAttachDocCntrl_CP.updateContractInfo(TestDataHelper.testContractObj);

       // ApexPages.currentPage().getParameters().put('isUpdate','false');
        Apexpages.currentPage().getparameters().put('isInConsole','false');
        ApexPages.currentPage().getParameters().put('isUpdate','true');
        Apexpages.currentPage().getparameters().put('id',TestDataHelper.testContractObj.id);
        vlocity_addon_AutoAttachDocCntrl_CP objvlocity_addon_AutoAttachDocCntrl_CP1=new vlocity_addon_AutoAttachDocCntrl_CP(new vlocity_cmt.ContractDocumentCreationController());
        boolean isInConsole1=objvlocity_addon_AutoAttachDocCntrl_CP1.isInConsole;
        
        Apexpages.currentPage().getparameters().put('isInConsole','false');
        ApexPages.currentPage().getParameters().put('isUpdate','true');
        Apexpages.currentPage().getparameters().put('id',TestDataHelper.testContractObj.id);
        vlocity_addon_AutoAttachDocCntrl_CP objvlocity_addon_AutoAttachDocCntrl_CP2=new vlocity_addon_AutoAttachDocCntrl_CP(new vlocity_cmt.ContractDocumentCreationController());
        boolean isInConsole2=objvlocity_addon_AutoAttachDocCntrl_CP2.isInConsole;
       // system.assertEquals(isInConsole1, false);
        objvlocity_addon_AutoAttachDocCntrl_CP2.attachDoc();
       // objvlocity_addon_AutoAttachDocCntrl_CP2.updateContractInfo(TestDataHelper.testContractObj);
        
        Test.stopTest();
    }
    
     
    
      
    
    private static void createRelatedListItems(id dealSupportId){
        List<sObject> itemsCreate = new List<sObject>();
        List<String> relatedList = new LIst<String>{'Rate_Plan_Item__c', 'Agreement_Contact__c','Agreement_Dealer__c','Subsidiary_Account__c'}; 
            for(String objName :relatedList){
               System.debug(LoggingLevel.INFO,'Creating Objs for ' + objName);
                Type t = type.forName(objName);
                for(Integer i=0;i<2;i++){
                  sObject obj = (sObject)t.newInstance();
                  obj.put('Deal_Support__c',dealSupportId);
                    itemsCreate.add(obj);
                }    
            }
      insert itemsCreate;
    }
}