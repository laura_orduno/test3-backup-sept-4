@isTest
private class ICSReserveInventoryTriggerTest{

    @isTest(SeeAllData=true)
    static void testDeviceLoanOut(){
            
            RecordType accountRecordType = [select Id, Name from RecordType where  SObjectType = 'Account' and Name = 'Dealer Account'];
            RecordType icsRecordType = [select Id, Name from RecordType where SObjectType = 'IC_Solution__c' and Name = 'Loaner Device'];
            RecordType caseRecordType = [select Id, Name from RecordType where SObjectType = 'Case' and Name = 'SMB Care Interim Connectivity' Limit 1];
            Account testAccount = createAccountobject('Test account',accountRecordType.id,'Customer Pick-up');
            Case testCase = createCaseobject(caseRecordType.id,testAccount);
            IC_Inventory__c device = createICPicUpDeviceItem();
       
            IC_Solution__c solution  = new IC_Solution__c();
            solution.Case__c = testCase.id;
            solution.RecordTypeId = icsRecordType.id;
            solution.Inventory_Item__c = device.Id;
            System.assertEquals(false, device.Loaned_Out__c );
            insert solution;
            device = [select id,Loaned_Out__c from IC_Inventory__c where id = :device.id];
            System.assertEquals( true, device.Loaned_Out__c );  
            List<Apexpages.Message> msgs = ApexPages.getMessages();
            System.debug(msgs);

            
     }
    
    
   // @isTest(SeeAllData=true)
    static void testDeviceLoanOutFail(){
            
            RecordType accountRecordType = [select Id, Name from RecordType where  SObjectType = 'Account' and Name = 'Dealer Account'];
            RecordType icsRecordType = [select Id, Name from RecordType where SObjectType = 'IC_Solution__c' and Name = 'Loaner Device'];
            RecordType caseRecordType = [select Id, Name from RecordType where SObjectType = 'Case' and Name = 'SMB Care Interim Connectivity' Limit 1];
            Account testAccount = createAccountobject('Test account',accountRecordType.id,'Customer Pick-up');
            Case testCase = createCaseobject(caseRecordType.id,testAccount);
        
            IC_Inventory__c device = createICPicUpDeviceItem();
            IC_Solution__c solution1  = new IC_Solution__c();
            solution1.Case__c = testCase.id;
            solution1.RecordTypeId = icsRecordType.id;
            solution1.Inventory_Item__c = device.Id;
            System.assertEquals(false, device.Loaned_Out__c );
            insert solution1;
            device = [select id,Loaned_Out__c from IC_Inventory__c where id = :device.id];
            System.assertEquals( true, device.Loaned_Out__c );  
        
            IC_Solution__c solution2  = new IC_Solution__c();
            solution2.Case__c = testCase.id;
            solution2.RecordTypeId = icsRecordType.id;
            solution2.Inventory_Item__c = device.Id;
            System.assertEquals(true, device.Loaned_Out__c );
            try{
                insert solution2;
                System.assert(false,'Should have throw the exception');
            }catch(DmlException e){
                System.debug(e.getDmlMessage(0));
                String expectedMsg = 'The selected Loaner Device has been selected by another user. Please try again.';
                System.assertEquals(expectedMsg, e.getDmlMessage(0));
            }
            device = [select id,Loaned_Out__c from IC_Inventory__c where id = :device.id];
            System.assertEquals( true, device.Loaned_Out__c );
     }
    
     
    @isTest(SeeAllData=true)
    static void testDeviceLoneOutforICSCreate(){
            
            IC_Solution__c solution  = createIC_Solution();
            IC_Inventory__c pickupDevice = createICPicUpDeviceItem();
            solution.Inventory_Item__c = pickupDevice.Id;
            System.assertEquals(false, pickupDevice.Loaned_Out__c );
            insert solution;
            pickupDevice = [select id,Loaned_Out__c from IC_Inventory__c where id = :pickupDevice.id];
            System.assertEquals( true, pickupDevice.Loaned_Out__c );    
    }
    
   // @isTest(SeeAllData=true)
    static void testValidationErrorWhenLoanedOutDeviceisPicked(){
            
        IC_Solution__c solution  = createIC_Solution();
            IC_Inventory__c pickupDevice = createICPicUpDeviceItem();
            solution.Inventory_Item__c = pickupDevice.Id;
            insert solution;
            pickupDevice = [select id,Loaned_Out__c from IC_Inventory__c where id = :pickupDevice.id];
            System.assertEquals( true, pickupDevice.Loaned_Out__c );
        
            IC_Solution__c errorSolution  = createIC_Solution();
            errorSolution.Inventory_Item__c = pickupDevice.Id;
            try{
                insert errorSolution;
                System.assert(false,'Should have throw the exception');
            }catch(DmlException e){
                System.debug('test method DML message: '+ e.getDmlMessage(0));
                String expectedMsg = 'The selected Loaner Device has been selected by another user. Please try again.';
                System.assertEquals(expectedMsg, e.getDmlMessage(0));
            }
            pickupDevice = [select id,Loaned_Out__c from IC_Inventory__c where id = :pickupDevice.id];
            System.assertEquals( true, pickupDevice.Loaned_Out__c );
    }
    
   // @isTest(SeeAllData=true)
    static void testChangingDeviceOnICS(){
    
            IC_Solution__c solution  = createIC_Solution();
            IC_Inventory__c pickupDevice1 = createICPicUpDeviceItem();
            IC_Inventory__c pickupDevice2 = createICPicUpDeviceItem();
            System.debug('Solution Insert  Start');
            solution.Inventory_Item__c = pickupDevice1.Id;
            insert solution;
            pickupDevice1 = [select id,Loaned_Out__c, name from IC_Inventory__c where id = :pickupDevice1.id];
            pickupDevice2 = [select id,Loaned_Out__c, name from IC_Inventory__c where id = :pickupDevice2.id];
            System.assertEquals( true, pickupDevice1.Loaned_Out__c );
            System.debug('Solution Insert  End');
            //Change the Device to another one
            solution.Inventory_Item__c = pickupDevice2.Id;
        
            System.debug('Solution Update Start==========================' +'Change from ' + pickupDevice1.Name + ' to ' + pickupDevice2.Name);
            update solution;
            System.debug('Solution Update End===========================');
            pickupDevice1 = [select id,Loaned_Out__c from IC_Inventory__c where id = :pickupDevice1.id];
            pickupDevice2 = [select id,Loaned_Out__c from IC_Inventory__c where id = :pickupDevice2.id];
            System.assertEquals( false, pickupDevice1.Loaned_Out__c );
            System.assertEquals( true, pickupDevice2.Loaned_Out__c );
    }
   //  @isTest(SeeAllData=true)
      static void testChangingDeviceOnICSFail(){
            IC_Solution__c solution1  = createIC_Solution();
            IC_Solution__c solution2  = createIC_Solution();
            IC_Inventory__c pickupDevice1 = createICPicUpDeviceItem();
            IC_Inventory__c pickupDevice2 = createICPicUpDeviceItem();
            
            solution1.Inventory_Item__c = pickupDevice1.Id;
            solution2.Inventory_Item__c = pickupDevice2.Id;
            insert solution1;
            insert solution2;
            pickupDevice1 = [select id,Loaned_Out__c from IC_Inventory__c where id = :pickupDevice1.id];
            pickupDevice2 = [select id,Loaned_Out__c from IC_Inventory__c where id = :pickupDevice2.id];
            System.assertEquals( true, pickupDevice1.Loaned_Out__c );
            System.assertEquals( true, pickupDevice2.Loaned_Out__c );
            //Change the Device to another one
            solution1.Inventory_Item__c = pickupDevice2.Id;
        
            try{
                update solution1;
                System.assert(false,'Should have throw the exception');
            }catch(DmlException e){
                String expectedMsg = 'The selected Loaner Device has been selected by another user. Please try again.';
                System.assertEquals(expectedMsg, e.getDmlMessage(0));
            }
            
    }
    
    // @isTest(SeeAllData=true)
      static void testICSUpdateFail(){
            IC_Solution__c solution1  = createIC_Solution();
            IC_Inventory__c pickupDevice1 = createICPicUpDeviceItem();
            
            solution1.Inventory_Item__c = pickupDevice1.Id;
            insert solution1;
            pickupDevice1 = [select id,Loaned_Out__c from IC_Inventory__c where id = :pickupDevice1.id];
            System.assertEquals( true, pickupDevice1.Loaned_Out__c );
            //Change the Device to another one
          
          try{
                solution1.Inventory_Item__c = 'a5te0000000JjJX';
                update solution1;
                System.assert(false,'Should have throw the exception for update solution2');
            }catch(DMLException e){
                System.debug('Update solution1 error' +  e.getDmlMessage(0) + ' Long Error ' +e.getMessage() +'=>' +e );
            }
            
    }
    
    // @isTest(SeeAllData=true)
    static void testICSCancelorClosed(){
        IC_Solution__c solution1  = createIC_Solution();
        IC_Solution__c solution2  = createIC_Solution();
        IC_Inventory__c pickupDevice1 = createICPicUpDeviceItem();
        IC_Inventory__c pickupDevice2 = createICPicUpDeviceItem();
            
            solution1.Inventory_Item__c = pickupDevice1.Id;
            solution2.Inventory_Item__c = pickupDevice2.Id;
            insert solution1;
            insert solution2;
            
            pickupDevice1 = [select id,Loaned_Out__c from IC_Inventory__c where id = :pickupDevice1.id];
            pickupDevice2 = [select id,Loaned_Out__c from IC_Inventory__c where id = :pickupDevice2.id];
            System.assertEquals( true, pickupDevice1.Loaned_Out__c );
            System.assertEquals( true, pickupDevice2.Loaned_Out__c );
            
            //Change the Device to another one
            solution1.status__c = 'Cancelled';
            solution2.status__c = 'Closed';
        
            update solution1;
            update solution2;
            
            pickupDevice1 = [select id,Loaned_Out__c from IC_Inventory__c where id = :pickupDevice1.id];
            pickupDevice2 = [select id,Loaned_Out__c, Device_Provider__r.ICS_Notification_Email__c from  IC_Inventory__c where id = :pickupDevice2.id];
            System.assertEquals( false, pickupDevice1.Loaned_Out__c );
            System.assertEquals( false, pickupDevice2.Loaned_Out__c );
    }
    
    
                
    public static Account createAccountobject(String name, id recordTypeId, String icsDeleveryMethod) { 
        Account account = new Account(Name = name , RecordTypeId =recordTypeId );
        insert account;
        return account;
    }
    
    public static Case createCaseobject(id recordTypeId,Account acc) { 
        Case caseobj = new Case(RecordTypeId =recordTypeId );
        caseobj.Status='Order Fulfilment';
        caseobj.Origin='Escalated';
        caseobj.AccountId =acc.id;
        caseobj.Subject ='test';
        insert caseobj;
        return caseobj;
    }
    
    public static IC_Inventory__c createICPicUpDeviceItem(){
        RecordType recordType = [select Id, Name from RecordType where SObjectType = 'IC_Inventory__c' and Name = 'Device' Limit 1];
        IC_Device_Provider__c dealer = createDealerProvider();
        IC_Inventory__c item = new IC_Inventory__c();
        item.RecordTypeId=recordType.Id;
        item.Device_Type__c = 'PCS Device';
        item.STN__c = 'abcd';
        item.Device_Provider__c=dealer.id;
        insert item;
        return item;
    }
    
    public static IC_Inventory__c createICCourierDeviceItem(){
        RecordType recordType = [select Id, Name from RecordType where SObjectType = 'IC_Inventory__c' and Name = 'Device' Limit 1];
        IC_Device_Provider__c dealer = createWarHouseProvider();
        IC_Inventory__c item = new IC_Inventory__c();
        item.RecordTypeId=recordType.Id;
        item.Device_Type__c = 'SmartHub';
        item.STN__c = 'abcd';
        item.Device_Provider__c=dealer.id;
        insert item;
        return item;
    }

    
    public static IC_Inventory__c createICInventoryFaxAccountItem(){
        RecordType recordType = [select Id, Name from RecordType where SObjectType = 'IC_Inventory__c' and Name = 'Internet Fax Account' Limit 1];
        IC_Inventory__c item = new IC_Inventory__c();
        item.RecordTypeId=recordType.Id;
        item.Fax_Number__c='7787789874';
        insert item;
        return item;
    }
    
   /*
    * Create new Solution object 
    */
    public static IC_Solution__c createIC_Solution() { 
        
        Case c = createCase();
        RecordType icsRecordType = [select Id, Name from RecordType where SObjectType = 'IC_Solution__c' and Name = 'Loaner Device'];
        IC_Solution__c solution  = new IC_Solution__c();
        solution.Case__c = c.id;
        solution.RecordTypeId =icsRecordType.id;
        return solution;
    }
    
    
    public static Case createCase() { 
        Account acc = createAccount();
        Case caseobj = new Case();
        caseobj.Status='Order Fulfilment';
        caseobj.Origin='Escalated';
        caseobj.AccountId =acc.id;
        caseobj.Subject ='test';
        insert caseobj;
        return caseobj;
    }
    
    public static Account createAccount() { 
        Account account = new Account(Name = 'Test Account');
        insert account;
        return account;
    }
    
    
    /*
    public Id getICSRecordTypeId(String recordType){
        Map<String,id> recodMap = new Map<String,id>();
        List<RecordType> typeList = [select Id, Name from RecordType where SObjectType = 'IC_Solution__c'];
        for(RecordType rt : typeList){
            recodMap.put(rt.Name, rt.Id);
        } 
        return recodMap.get(recordType);
    }*/
    
    public static IC_Device_Provider__c createDealerProvider() { 
        IC_Device_Provider__c provider = new IC_Device_Provider__c(Name = 'Test Account-Dealer');
        provider.ICS_Delivery_Method__c = 'Customer Pick-up';
        provider.City__c='Burnaby';
        provider.Province__c='BC';
        provider.Active__c=true;
        provider.ICS_Notification_Email__c = 'prishan.fernando@telus.com,mark.heimburger@telus.com';
        insert provider;
        return provider;
    }
    
     public static IC_Device_Provider__c createWarHouseProvider() { 
        IC_Device_Provider__c provider = new IC_Device_Provider__c(Name = 'Test Account-WareHouse');
        provider.ICS_Delivery_Method__c = 'Courier to Customer';
        provider.City__c='Burnaby';
        provider.Province__c='BC';
        provider.Active__c=true;
        provider.ICS_Notification_Email__c = 'prishan.fernando@telus.com,mark.heimburger@telus.com';
        insert provider;
        return provider;
    }
   
}