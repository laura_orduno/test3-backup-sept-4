@isTest(SeeAllData=true)
private class ProductPickerControllerTest {
	testMethod static void testProductPickerController() {
        Account acc = TestUtil.createAccount();
        Opportunity opp = TestUtil.createOpportunity(acc.Id);
        Opportunity_Solution__c solution = TestUtil.createOpportunitySolution(opp.Id);
        List<Opp_Product_Item__c> items = TestUtil.createPLIList(opp.Id);
        
        PageReference pageRef = Page.productPicker;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('solutionId', solution.id);
        ProductPickerController c = new ProductPickerController();
        
        c.getSearchOptions();
        
        c.component.searchText = 'BPC1';
        c.component.searchOption = 'BPC_1__c';
        c.searchProducts();
        c.getSearchResults();
        c.bpc1 = 'BPC11';
        c.bpc1Selected();
        
        c.bpc2 = 'BPC21';
        c.bpc2Selected();

        c.bpc3 = 'BPC31';
        c.bpc3Selected();

        c.selectedProduct = items.get(0).product__c;
        c.selectedProductName = 'Test';
        System.debug('items.get(0): ' + items.get(0));
        c.selectProduct();
        c.save();

//with pli
        pageRef = Page.productPicker;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('solutionId', solution.id);
        ApexPages.currentPage().getParameters().put('pliId', items.get(0).Id);
        c = new ProductPickerController();

        c.searchProducts();
        c.component.searchText = 'B';
        c.searchProducts();
        
        c.component.searchText = 'BPC1';
        c.component.searchOption = 'BPC_1__c';
        
        c.searchProducts();
        c.bpc1 = 'BPC11';
        c.bpc1Selected();
        
        c.bpc2 = 'BPC21';
        c.bpc2Selected();

        c.bpc2 = 'BPC31';
        c.bpc3Selected();

        c.selectedProduct = items.get(0).product__c;
        c.selectedProductName = 'Test';
        System.debug('items.get(0): ' + items.get(0));
        c.selectProduct();
        c.save();
        
        //wrong id - throw an erroir
        c.selectedProduct = items.get(0).Id;
        c.selectProduct();
        
        //call std methods
        c.component.searchOption = 'BPC_2__c';
        c.searchProducts();

        c.component.searchOption = 'BPC_3__c';
        c.searchProducts();

        c.cancel();
        c.addProduct();
    }
}