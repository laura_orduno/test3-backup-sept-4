public abstract class AssigningControllerTemplate {
    
    
    /*-----------the Template Methods common to all ------------------------*/
    
    public String selectedTeamMember {get; set;}     
    public List<SelectOption> teamMemberList;    
    
    public List<SelectOption> getTeamMemberList() {   
        List<SelectOption> teamMemberList = new List<SelectOption>(); 
		teamMemberList.add(new SelectOption('None','None'));
             
        ID theQueueId = getQueueId(getQueueName());
        if (theQueueId==null) return teamMemberList;
        
        GroupMember[] theQueueMembers = [SELECT Group.Name, UserOrGroupId FROM GroupMember 
                                         WHERE Group.Id = :theQueueId];                            
        ID[] userOrGroupIds = new List<ID>();
        for(GroupMember  theMember:theQueueMembers){
            userOrGroupIds.add(theMember.UserOrGroupId);
        }   
           
        User[] theUserList = [SELECT Id, Name FROM User WHERE IsActive=true AND Id in :userOrGroupIds];           
        for(User theUser:theUserList){       
            teamMemberList.add(new SelectOption(theUser.id,theUser.name));
        }  
        return teamMemberList;
    }
    
    
    public PageReference assignToMe(){
        if(getIdList()!=null) assignToUser(getIdList(), UserInfo.getUserId());
        return getPreviousPage(); 
    } 
    
    public PageReference managerAssign(){ 
        if(getIdList()!=null) assignToUser(getIdList(), selectedTeamMember);
        return getPreviousPage(); 
    } 
    
    /*------------------------------------------------------------*/
    
    private ID getQueueId(String queueName) { 
        try{
            Group theQ = [select Id from Group where DeveloperName = :queueName and Type = 'Queue' Limit 1];  
            if(theQ!=null) return theQ.id; 
            else return null; 
        }catch(Exception e){            
            return null; 
        }
    }
    
    private PageReference getPreviousPage(){
        String url = ApexPages.currentPage().getParameters().get('retUrl');   
        if(String.isNotBlank(url)) return new PageReference(url);
        else return null;
    }
    
    protected String[] getIdList() {  
        String ids = ApexPages.currentPage().getParameters().get('ids');
        if(String.isNotBlank(ids)) return ids.split(',');
        else return null;
    }  
    
    /*----------- Hollywood Principle: Don't call us, we'll call you ... ------------*/    
    protected abstract String getQueueName();   
    protected abstract void assignToUser(String[] idList, ID userId); 
        
}