/**
 * Created by rdraper on 2/14/2018.
 */

public with sharing abstract class Portal_AuthCheckCtlr {

	public User theUser;

	public PageReference loginPageReference;
	public PageReference errorPageReference;

	public static String userLanguage {
		get {
			return PortalUtils.getUserLanguage();
		}
		set;
	}

	public Portal_AuthCheckCtlr(PageReference loginPageRef, PageReference errorPageRef) {
		theUser = queryUserDetails();
		loginPageReference = loginPageRef;
		errorPageReference = errorPageRef;
	}

	private User queryUserDetails() {
		try {
			return [SELECT Id, AccountId, Customer_Portal_Role__c FROM User WHERE Id = :UserInfo.getUserId() Limit 1];
		} catch (QueryException ex) {
			return null;
		}
	}

	public PageReference redirectIfGuest() {
		if (Userinfo.getUserType().equals('Guest')) {
			loginPageReference.setRedirect(true);
			return loginPageReference;
		}
		return null;
	}

	private Boolean notAccountId(String accountId) {
		return(accountId == null || accountId == '' || accountId.substring(0, 3) != '001');
	}

	public PageReference redirectIfNoAccount() {
		if (notAccountId(theUser.AccountId)) {
			errorPageReference.setRedirect(true);
			return errorPageReference;
		}
		return null;
	}

	public static void switchLanguage() {
		PortalUtils.setUserLanguage();
	}
}