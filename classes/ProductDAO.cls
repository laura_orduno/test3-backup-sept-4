/**
 * Data Access Object (DAO) helper class for loading Product and appropriate Pricebook Entry.
 * 
 * @author Max Rudman
 * @since 2/11/2011
 */
public class ProductDAO extends AbstractDAO {
	private static final List<Schema.SObjectField> STANDARD_FIELDS = new List<Schema.SObjectField> {
		Product2.Id, Product2.Name, Product2.ProductCode, Product2.Description,
		Product2.SBQQ__DiscountSchedule__c, Product2.SBQQ__SubscriptionPricing__c,
		Product2.SBQQ__SubscriptionPercent__c, Product2.SBQQ__PriceEditable__c,
		Product2.SBQQ__NonDiscountable__c,
		Product2.SBQQ__SubscriptionTerm__c, Product2.SBQQ__SubscriptionBase__c,
		Product2.SBQQ__PricingMethod__c, Product2.SBQQ__PricingMethodEditable__c,
		Product2.SBQQ__ConfigurationValidator__c, Product2.SBQQ__OptionSelectionMethod__c,
		Product2.SBQQ__OptionLayout__c, Product2.SBQQ__ConfigurationFields__c, Product2.SBQQ__ConfigurationFormTitle__c,
		Product2.SBQQ__ConfigurationType__c, Product2.SBQQ__Optional__c, Product2.SBQQ__Taxable__c,
		Product2.SBQQ__CustomConfigurationPage__c, Product2.SBQQ__CustomConfigurationRequired__c, Product2.SBQQ__Hidden__c,
		Product2.SBQQ__ReconfigurationDisabled__c, Product2.SBQQ__ExcludeFromOpportunity__c, Product2.SBQQ__DescriptionLocked__c,
		Product2.SBQQ__ConfiguredCodePattern__c, Product2.SBQQ__ConfigurationEvent__c, Product2.SBQQ__SubscriptionCategory__c,
		Product2.Core_Product_Ids__c, Product2.Provisioning_Summary_Fields__c
	};
	
	// Public properties
	public Boolean loadCustomField {get; set;}
	public Boolean loadOptions {get; set;}
	public Boolean loadPriceEntries {get; set;}
	public Boolean loadCosts {get; set;}
	public Boolean loadBlockPrices {get; set;}
	public Boolean loadConfigurationRules {get; set;}
	public Boolean loadRelatedContent {get; set;}
	public Integer maxDepth {get; set;}
	public Id pricebookId {get; set;}
	public String currencyCode {get; set;}
	public Set<Id> optionIds {get; set;}
	
	/**
	 * Default constructor.
	 */
	public ProductDAO() {
		loadCustomField = false;
		loadOptions = false;
		loadPriceEntries = false;
		loadCosts = false;
		loadBlockPrices = false; 
		loadConfigurationRules = false;
		loadRelatedContent = false;
		maxDepth = 0;
	}
	
	/**
	 * Loads products and related child objects identified by specified set of Ids.
	 * Which child objects, if any, are loaded is determined by loadXXX flags.
	 */
	public Map<Id,ProductVO> loadByIds(Set<Id> ids) {
		if ((ids == null) || ids.isEmpty()) {
			return new Map<Id,ProductVO>();
		}
		return loadByIds(ids, 0);
	}
	
	public ProductVO loadById(Id id) {
		return loadByIds(new Set<Id>{id}).get(id);
	}
	
	private Map<Id,ProductVO> loadByIds(Set<Id> ids, Integer depth) {
		QueryBuilder builder = createProductQuery();
		if (loadPriceEntries) {
			builder.getSelectClause().addExpression(createPricebookEntrySubquery());
		}
		if (loadOptions) {
			builder.getSelectClause().addExpression(createOptionSubquery());
			builder.getSelectClause().addExpression(createFeatureSubquery());
			builder.getSelectClause().addExpression(createConstraintSubquery());
		}
		if (loadConfigurationRules) {
			builder.getSelectClause().addExpression(createConfigurationRuleSubquery());
		}
		builder.getWhereClause().addExpression(builder.inExpr(Product2.Id, ids));
		
		Map<Id,ProductVO> result = new Map<Id,ProductVO>();
		for (Product2 product : Database.query(builder.buildSOQL())) {
			if (UserInfo.isMultiCurrencyOrganization()) {
				QuoteUtils.setCurrencyCode(product, (currencyCode != null) ? currencyCode : UserInfo.getDefaultCurrency());
			}
			ProductVO vo = new ProductVO(product);
			if (loadConfigurationRules) {
				vo.configurationRules = product.SBQQ__ConfigurationRules__r;
			}
			result.put(product.Id, vo);
		}
		
		if ((loadOptions) && (depth < maxDepth)) {
			loadOptionProducts(result, depth);
		}
		
		return result;
	}
	
	private void loadOptionProducts(Map<Id,ProductVO> productsById, Integer depth) {
		// Load nested products referenced by options
		Set<Id> optionProductIds = new Set<Id>();
		for (ProductVO vo : productsById.values()) {
			optionProductIds.addAll(vo.getOptionProductIds());
		}
		if (optionProductIds.isEmpty()) {
			// There are no options; we are done.
			return;
		}
		//loadOptions = (depth == (maxDepth - 1));
		Map<Id,ProductVO> optionProductsById = loadByIds(optionProductIds, depth + 1);
		for (ProductVO product : productsById.values()) {
			for (ProductVO.Option option : product.options) {
				Id optionProductId = option.record.SBQQ__OptionalSKU__c;
				if (optionProductsById.containsKey(optionProductId)) {
					option.product = optionProductsById.get(optionProductId);
				}
			}
		}
		loadOptions = true;
	}
	
	
	/**
	 * Creates query to retrieve product records.
	 */
	private QueryBuilder createProductQuery() {
		QueryBuilder builder = new QueryBuilder(Product2.sObjectType);
		
		// Add default product fields to the query.
		for (Schema.SObjectField field : STANDARD_FIELDS) {
			builder.getSelectClause().addField(field);
		}
		 
		return builder;
	}
	
	/**
	 * Creates sub-query to retrieve child Pricebook Entry records.
	 */
	private QueryBuilder createPricebookEntrySubquery() {
		String typeName = MetaDataUtils.findRelationship(Product2.sObjectType, PricebookEntry.Product2Id).getRelationshipName();
		QueryBuilder builder = new QueryBuilder(typeName);
		builder.getSelectClause().addField(PricebookEntry.Pricebook2Id);
		builder.getSelectClause().addField(PricebookEntry.UnitPrice);
		if (UserInfo.isMultiCurrencyOrganization()) {
			builder.getSelectClause().addField('CurrencyIsoCode');
		}
		if (pricebookId != null) {
			builder.getWhereClause().addExpression(builder.eq(PricebookEntry.Pricebook2Id, pricebookId));
		}
		if (currencyCode != null) {
			builder.getWhereClause().addExpression(builder.eq('CurrencyIsoCode', currencyCode));
		}
		return builder;
	}
	
	/**
	 * Creates sub-query to retrieve child Product Option records.
	 */
	private QueryBuilder createOptionSubquery() {
		String typeName = MetaDataUtils.findRelationship(Product2.sObjectType, SBQQ__ProductOption__c.SBQQ__ConfiguredSKU__c).getRelationshipName();
		QueryBuilder builder = new QueryBuilder(typeName);
		for (Schema.SObjectField field : MetaDataUtils.getFields(SBQQ__ProductOption__c.sObjectType)) {
			builder.getSelectClause().addField(field);
		}
		builder.getWhereClause().addExpression(builder.neq(SBQQ__ProductOption__c.SBQQ__OptionalSKU__c, null));
		if (optionIds != null) {
			QueryBuilder.OrExpression lor = builder.lor();
			if (!optionIds.isEmpty()) {
				lor.addExpression(builder.inExpr(SBQQ__ProductOption__c.Id, optionIds));
			}
			lor.addExpression(builder.eq(SBQQ__ProductOption__c.SBQQ__Required__c, true));
			// Need to load selected so nested packages with pre-selected options are properly handled
			lor.addExpression(builder.eq(SBQQ__ProductOption__c.SBQQ__Selected__c, true));
			builder.getWhereClause().addExpression(lor);
		}
		builder.getOrderByClause().addAscending(SBQQ__ProductOption__c.SBQQ__Feature__c.getDescribe().getRelationshipName() + '.' + SBQQ__ProductFeature__c.SBQQ__Number__c);
		builder.getOrderByClause().addAscending(SBQQ__ProductOption__c.SBQQ__Number__c);
		return builder;
	}
	
	/**
	 * Creates sub-query to retrieve child Feature records.
	 */
	private QueryBuilder createFeatureSubquery() {
		String typeName = MetaDataUtils.findRelationship(Product2.sObjectType, SBQQ__ProductFeature__c.SBQQ__ConfiguredSKU__c).getRelationshipName();
		QueryBuilder builder = new QueryBuilder(typeName);
		for (Schema.SObjectField field : MetaDataUtils.getFields(SBQQ__ProductFeature__c.sObjectType)) {
			builder.getSelectClause().addField(field);
		}
		builder.getOrderByClause().addAscending(SBQQ__ProductFeature__c.SBQQ__Number__c);
		return builder;
	}
	
	/**
	 * Creates sub-query to retrieve child Option Constraint records.
	 */
	private QueryBuilder createConstraintSubquery() {
		String typeName = MetaDataUtils.findRelationship(Product2.sObjectType, SBQQ__OptionConstraint__c.SBQQ__ConfiguredSKU__c).getRelationshipName();
		QueryBuilder builder = new QueryBuilder(typeName);
		builder.getSelectClause().addField(SBQQ__OptionConstraint__c.SBQQ__ConstrainingOption__c);
		builder.getSelectClause().addField(SBQQ__OptionConstraint__c.SBQQ__ConstrainedOption__c);
		builder.getSelectClause().addField(SBQQ__OptionConstraint__c.SBQQ__Type__c);
		builder.getWhereClause().addExpression(builder.eq(SBQQ__OptionConstraint__c.SBQQ__Active__c, true));
		return builder;
	}
	
	
	/**
	 * Creates sub-query to retrieve child Related Content records.
	 */
	private QueryBuilder createConfigurationRuleSubquery() {
		String typeName = MetaDataUtils.findRelationship(Product2.sObjectType, SBQQ__ConfigurationRule__c.SBQQ__Product__c).getRelationshipName();
		QueryBuilder builder = new QueryBuilder(typeName);
		addCustomFields(builder, SBQQ__ConfigurationRule__c.sObjectType);
		return builder;
	}
	
	public void save(ProductVO vo) {
		save(new List<ProductVO>{vo});
	}
	
	public void save(List<ProductVO> vos) {
		List<SBQQ__BlockPrice__c> prices = new List<SBQQ__BlockPrice__c>();
		List<SBQQ__Cost__c> costs = new List<SBQQ__Cost__c>();
		List<SBQQ__ProductOption__c> options = new List<SBQQ__ProductOption__c>();
		List<SBQQ__ProductFeature__c> features = new List<SBQQ__ProductFeature__c>();
		List<SBQQ__OptionConstraint__c> constraints = new List<SBQQ__OptionConstraint__c>();
		List<SBQQ__RelatedContent__c> relatedContent = new List<SBQQ__RelatedContent__c>();
		List<SBQQ__ConfigurationRule__c> rules = new List<SBQQ__ConfigurationRule__c>();
		
		
		for (ProductVO vo : vos) {
			if (vo.blockPrices != null) {
				prices.addAll(vo.blockPrices);
			}
			if (vo.costs != null) {
				costs.addAll(vo.costs);
			}
			if (vo.features != null) {
				for (ProductVO.Feature feature : vo.features) {
					features.add(feature.record);
				}
			}
			if (vo.options != null) {
				for (ProductVO.Option option : vo.options) {
					options.add(option.record);
				}
			}
			if (vo.constraints != null) {
				for (ProductVO.Constraint constr : vo.constraints) {
					constraints.add(constr.record);
				}
			}
			if (vo.configurationRules != null) {
				rules.addAll(vo.configurationRules);
			}
			if (vo.relatedContent != null) {
				relatedContent.addAll(vo.relatedContent);
			}
		}
		saveChildren(SBQQ__BlockPrice__c.SBQQ__Product__c, prices);
		saveChildren(SBQQ__Cost__c.SBQQ__Product__c, costs);
		saveChildren(SBQQ__ProductFeature__c.SBQQ__ConfiguredSKU__c, features);
		saveChildren(SBQQ__RelatedContent__c.SBQQ__Product__c, relatedContent);
		saveChildren(SBQQ__ConfigurationRule__c.SBQQ__Product__c, rules);
		
		// Now that any new features have been inserted copy ID from related objeck into FK field.
		for (SBQQ__ProductOption__c option : options) {
			if ((option.SBQQ__Feature__r != null) && (option.SBQQ__Feature__c == null)) {
				option.SBQQ__Feature__c = option.SBQQ__Feature__r.Id;
				option.SBQQ__Feature__r = null;
			}
		}
		saveChildren(SBQQ__ProductOption__c.SBQQ__ConfiguredSKU__c, options);
		
		// Now that any new options have been inserted copy ID from related objeck into FK field.
		for (SBQQ__OptionConstraint__c constr : constraints) {
			if ((constr.SBQQ__ConstrainingOption__r != null) && (constr.SBQQ__ConstrainingOption__c == null)) {
				constr.SBQQ__ConstrainingOption__c = constr.SBQQ__ConstrainingOption__r.Id;
				constr.SBQQ__ConstrainingOption__r = null;
			}
			if ((constr.SBQQ__ConstrainedOption__r != null) && (constr.SBQQ__ConstrainedOption__c == null)) {
				constr.SBQQ__ConstrainedOption__c = constr.SBQQ__ConstrainedOption__r.Id;
				constr.SBQQ__ConstrainedOption__r = null;
			}
		}
		saveChildren(SBQQ__OptionConstraint__c.SBQQ__ConfiguredSKU__c, constraints);
	}

}