/** 
Class Name : NAAS_Capture_OLI_BillingAddrController
Purpose: Controller for Billing Address in Additional Details step       
**/
public class NAAS_Capture_OLI_BillingAddrController
{
    public String OderId {get; set;}
    public List<OLIWrapper> OLIWrapperList;
    public Map<String,List<OLIWrapper>> OLIWrapperToBillingAddressMap;
    public AddressData PAData  { get; set; }
    public AddressData BAData  { get; set; }
    public string searchString{get;set;} // search keyword
    public List<Account> results{get;set;} // search results
    public String selectedAccId{get;set;}
    public String selectedOliBillAcc{get;set;}
    public String billingNotes {get;set;}
   //commented by danish on 22/03/2017 because it is not used on the vf page in multi site flow
   /* public boolean disableAddrBtn{
        get{
            if(OLIWrapperToBillingAddressMap == null || OLIWrapperToBillingAddressMap.size() < 1)
                return true;
            return false;
        }
        set;
    }*/
    
     /**
     Method:OCOM_Capture_OLI_BillingAddrController()
     This is a Constructor.
     Inputs Args:None
    Output Args:None
    **/  
    public NAAS_Capture_OLI_BillingAddrController()
    {
        //system.debug('%%######%% START: OCOM_Capture_OLI_BillingAddrController:OCOM_Capture_OLI_BillingAddrController  %%######%% ');
        PAData = new AddressData();
        BAData = new AddressData();
        billingNotes = '';
        //OLIWrapperToBillingAddressMap = new Map<String,List<OLIWrapper>>();
        //system.debug('%%######%% END: OCOM_Capture_OLI_BillingAddrController:OCOM_Capture_OLI_BillingAddrController  %%######%% ');
    }
    
     /**
     Method:getOLIWrapperToBillingAddressMap()
     This method is maps Billing Address to OLI wrapper.
     Inputs Args:None
    Output Args:Map
    **/  
    public Map<String,List<OLIWrapper>> getOLIWrapperToBillingAddressMap()
    {
       // system.debug('%%######%% START: OCOM_Capture_OLI_BillingAddrController:getOLIWrapperToBillingAddressMap  %%######%% ');
       // system.debug('OCOM_BillAddCont constructor OderId(' + OderId + ')');
        Order orderObj=[select id,ServiceAddressSummary__c,name,Update_Billing_Address__c, Billing_Notes_Remarks__c from Order where id=:OderId];
        billingNotes = orderObj.Billing_Notes_Remarks__c;

        // Thomas - OCOM-1292 
       /* List<OrderItem> items = [Select Id, OrderId,Service_Address_Full_Name__c , Billing_Address__c,vlocity_cmt__BillingAccountId__c,Billing_Account__c, vlocity_cmt__ServiceAccountId__c, PriceBookEntry.Product2.Name, orderMgmtId_Status__c, order.ServiceAddressSummary__c, order.Service_Address__c 
                                 from OrderItem 
                                 where orderId=:OderId and vlocity_cmt__ParentItemId__c = null and PriceBookEntry.product2.IncludeQuoteContract__c = true];*/
            
        OLIWrapperList = new List<OLIWrapper>();
        OLIWrapperToBillingAddressMap = new Map<String,List<OLIWrapper>>();
        boolean notesFound = false;
        for(OrderItem o :[Select Id, OrderId,Service_Address_Full_Name__c , Billing_Address__c,vlocity_cmt__BillingAccountId__c,Billing_Account__c, vlocity_cmt__ServiceAccountId__c, PriceBookEntry.Product2.Name, orderMgmtId_Status__c, order.ServiceAddressSummary__c, order.Service_Address__c 
                                 from OrderItem 
                                 where orderId=:OderId and vlocity_cmt__ParentItemId__c = null and PriceBookEntry.product2.IncludeQuoteContract__c = true])
        {
           OLIWrapper OLIWrapper = new OLIWrapper(o);
            if(OLIWrapperToBillingAddressMap.containskey(orderObj.ServiceAddressSummary__c)){
               OLIWrapperToBillingAddressMap.get(orderObj.ServiceAddressSummary__c).add(OLIWrapper); 
            }
            else{
                 OLIWrapperToBillingAddressMap.put(orderObj.ServiceAddressSummary__c,new List<OLIWrapper>{OLIWrapper});
            }
            /*String Key = getServiceAddressSummary(o.vlocity_cmt__ServiceAccountId__c);
            OLIWrapperList = OLIWrapperToBillingAddressMap.get(Key);
            if(OLIWrapperList != null )
            {
                OLIWrapper OLIWrapper = new OLIWrapper(o);
                OLIWrapperList.add(OLIWrapper);
            }
            else
            {
                OLIWrapperList = new List<OLIWrapper>();
                OLIWrapper OLIWrapper = new OLIWrapper(o);
                OLIWrapperList.add(OLIWrapper);
            }
            
            OLIWrapperToBillingAddressMap.put(Key,OLIWrapperList);*/
        }
        
        results = performSearch(searchString);
        //system.debug('%%######%% END: OCOM_Capture_OLI_BillingAddrController:getOLIWrapperToBillingAddressMap  %%######%% ');    
        return OLIWrapperToBillingAddressMap;
    }
    
     /**
    Method:search()
     This method is to call runSearch.
  Inputs Args:None
    Output Args:None
    **/  
    Public PageReference search() 
    {
        runSearch();
        return null;
    }
    
     /**
    Method:runSearch()
     This method is to prepare the query and issue the search command.
  Inputs Args:None
    Output Args:None
    **/  
    private void runSearch() 
    {
        // TODO prepare query string for complex serarches & prevent injections
        results= performSearch(searchString);         
    } 

  /**
    Method:performSearch(string searchString) 
     This method is to run the search and return the records found.
  Inputs Args:string
    Output Args:List
    **/  
    private List<Account> performSearch(string searchString) 
    {
        //system.debug('%%######%% START: OCOM_Capture_OLI_BillingAddrController:performSearch  %%######%% ');
        if(String.isBlank(searchString))
            searchString = '';
       // system.debug('OCOM_Capture_OLI_BillingAddrController::performSearch searchString(' + searchString + ')');
        Id Billing = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Billing').getRecordTypeId();
        Id BAN = Schema.SObjectType.Account.getRecordTypeInfosByName().get('BAN').getRecordTypeId();
        Id CAN = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CAN').getRecordTypeId();
        Set<Id> AccRecordTypeId = new Set<ID>{Billing,BAN,CAN};
    
        Order Odrupdate2=[select id,name,account.id,account.name, Status from Order where id=:OderId limit 1];
        id aid=Odrupdate2.accountid;
          //query accounts by merging the variable name inside the query string
          List<Account> accountList = Database.query('SELECT Id,Account.Billing_System__c,Account.Billingstreet, Account.BillingCity, Account.BillingState, Account.BillingPostalCode, Account.BillingCountry, Account.name , Account.BAN_CAN__c FROM Account WHERE ( Name Like \'%'+searchString +'%\') and RecordTypeId in:AccRecordTypeId  and parentid=:aid and (Billing_Account_Active_Indicator__c=\'Y\' or Billing_Account_Active_Indicator__c=\'\')  order by LastModifiedDate Desc limit 999');        
        //system.debug('%%######%% END: OCOM_Capture_OLI_BillingAddrController:performSearch  %%######%% ');
        return accountList; 

    }
  
    /**
     Method:SaveBillingAccount()
     This method is to Save Billing Account.
     Inputs Args:None
     Output Args:Pagereference
    **/ 
    Public Pagereference SaveBillingAccount()
    {
        //system.debug('%%######%% START: OCOM_Capture_OLI_BillingAddrController:SaveBillingAccount  %%######%% ');
        Id BillingRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Billing').getRecordTypeId();
        List <OrderItem> oiBUpt=New List<OrderItem> ();
         
        Order OrderObj= [select id, AccountId, Name, Billing_Address__c, Ban__c from Order where id=:OderId];
        //system.debug('OCOM_Capture_OLI_BillingAddrController::SaveBillingAccount() OrderObj('+OrderObj+')');
        String BillingAddr =   ' ' +(String)PAData.addressLine1+ 
                               ' ' +(String)PAData.city+ 
                               ' ' +(String)PAData.province+ 
                               ' ' +(String)PAData.postalCode+
                               ' ' +(String)PAData.country;
        //system.debug('OCOM_Capture_OLI_BillingAddrController::SaveBillingAccount() BillingAddr('+BillingAddr+')');
        Integer draftCount = 1;
        
        // Thomas QC-8591 Start
        List<Account> accountList = new List<Account>();
        //system.debug('OCOM_Capture_OLI_BillingAddrController::SaveBillingAccount() BillingRecordType('+BillingRecordType+')');
        String billingAccountNamePrefix = 'Draft-00000';
        String selectFilter = billingAccountNamePrefix + '%';
        // obtain billing account name prefix from custom setting
        List<BillingAccountNamePrefix__c> billingAccountNamePrefixList = BillingAccountNamePrefix__c.getall().values();
       // system.debug('OCOM_Capture_OLI_BillingAddrController::SaveBillingAccount() billingAccountNamePrefixList('+billingAccountNamePrefixList+')');
        if (billingAccountNamePrefixList.size() > 0) {
            // build SOQL SELECT filter using billig account name prefix custom setting
            billingAccountNamePrefix = billingAccountNamePrefixList[0].Name;
            selectFilter = billingAccountNamePrefix+'%';
        }
       // system.debug('OCOM_Capture_OLI_BillingAddrController::SaveBillingAccount() billingAccountNamePrefix('+billingAccountNamePrefix+') selectFilter('+selectFilter+')');
        // obtain most up-to-date draft account
        accountList = [SELECT id, name FROM Account WHERE RecordTypeId=:BillingRecordType and name LIKE :selectFilter ORDER BY createddate DESC LIMIT 1];
        //system.debug('OCOM_Capture_OLI_BillingAddrController::SaveBillingAccount() accountList('+accountList+')');
        if(accountList.size() > 0)
        {
            // lock mechanism to minimize race conditions
            Account accountObj = [SELECT name FROM Account WHERE id=:accountList LIMIT 1 FOR UPDATE];
            // retrieve number from most up-to-date draft account name
            String accountName = accountObj.name;
            string[] accountNameSplitted = accountName.split('-');
           // system.debug('OCOM_Capture_OLI_BillingAddrController::SaveBillingAccount() accountName('+accountName+') accountNameSplitted('+accountNameSplitted+')');
            if (accountNameSplitted[1].isNumeric()) {
                draftCount = Integer.valueof(accountNameSplitted[1]) + 1;
            }
        }
        //system.debug('OCOM_Capture_OLI_BillingAddrController::SaveBillingAccount() draftCount('+draftCount+')');
        // build name of new draft account
        String newAccountName = billingAccountNamePrefix + String.valueof(draftCount);
       // system.debug('OCOM_Capture_OLI_BillingAddrController::SaveBillingAccount() newAccountName('+newAccountName+')');
        // added by prashant on 22-May-2017 for billing system
       // system.debug('BillingSystem>>>>>>'+PAData.billingSystem);
       // list<LEGACY_ACCOUNT_ID__c> lstLegacyAccount  = [select name,Billing_System_Name__c from LEGACY_ACCOUNT_ID__c where Billing_System_Name__c=:PAData.billingSystem limit 1];
        // end of Prashant's code
        Account Acc = new Account(ParentId = OrderObj.AccountId, 
                                  RecordTypeId =BillingRecordType, 
                                  Name=newAccountName, 
                                  Ban__c=newAccountName, 
                                  Ban_Can__c=newAccountName, 
                                  BillingStreet =PAData.addressLine1,  
                                  BillingCity =PAData.city, 
                                  BillingState =PAData.state, 
                                  BillingPostalCode=PAData.postalCode, 
                                  BillingCountry = PAData.country);
        // added by prashant on 22-May-2017 for billing system
        //if(lstLegacyAccount != null && lstLegacyAccount.size() > 0){
          //  Acc.Billing_System_ID__c = lstLegacyAccount[0].Name;
       // }
        // end of Prashant's code
        //system.debug('OCOM_Capture_OLI_BillingAddrController::SaveBillingAccount() Acc('+Acc+')');
        try {
            Insert Acc;
        } catch (DmlException aDmlException) {
            OrdrUtilities.auditLogs('_Capture_OLI_BillingAddrController.SaveBillingAccount','OCOM','saveDraftBillingAccount',null,null,true,false,aDmlException.getmessage(),aDmlException.getStackTraceString());ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,Label.Warning1_label));}
            catch (Exception generalException) {OrdrUtilities.auditLogs('OCOM_Capture_OLI_BillingAddrController.SaveBillingAccount','OCOM','saveDraftBillingAccount',null,null,true,false,generalException.getmessage(),generalException.getStackTraceString());ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,Label.Warning3_label));
        }
        // Thomas QC-8591 End

        if(String.isBlank(OrderObj.Billing_Address__c))
        {
            OrderObj.Billing_Address__c=BillingAddr;
            OrderObj.Ban__c=Acc.BAN_CAN__c;
        }
        
        Set<String> BillAddrUpdateSet = new Set<String>();
        for(String Key: OLIWrapperToBillingAddressMap.keySet())
        {
            for(OLIWrapper oliOLIWrapper: OLIWrapperToBillingAddressMap.get(Key))
            {
                if(oliOLIWrapper.isSelected)
                    BillAddrUpdateSet.add(oliOLIWrapper.oli.id);
            }    
        }
        
        
        if(BillAddrUpdateSet.size() > 0)
        {
           // List<OrderItem> items = [Select Id, OrderId, Billing_Address__c,vlocity_cmt__BillingAccountId__c,Billing_Account__c from OrderItem where id in :BillAddrUpdateSet];
    
            for(OrderItem oib :  [Select Id, OrderId, Billing_Address__c,vlocity_cmt__BillingAccountId__c,Billing_Account__c from OrderItem where id in :BillAddrUpdateSet]) 
            {
                oib.vlocity_cmt__BillingAccountId__c=Acc.id;
                oib.Billing_Address__c=BillingAddr;
                oib.Billing_Account__c=Acc.BAN_CAN__c;
    
                oiBUpt.add(oib); 
                //system.debug('Inside save' +oib);   
            }    
        }
            
        
        try
        {
            update OrderObj;
            if(oiBUpt.size() > 0)
                update oiBUpt; 
            Pagereference Pr= new pagereference('/apex/Order_PAC_Address?id='+OrderObj.id);
            Pr.setRedirect(true);
           // system.debug('%%######%% END: OCOM_Capture_OLI_BillingAddrController:SaveBillingAccount  %%######%% ');
            return null; 
        }
        catch(exception e)
        { return null;
        } 
    }
    
      /**
    Method:SaveBillingOnSelect()
     This method is to Save Billing After Selection.
  Inputs Args:None
    Output Args:Pagereference
    **/ 
    Public Pagereference SaveBillingOnSelect()
    {
       // system.debug('%%######%% START: OCOM_Capture_OLI_BillingAddrController:SaveBillingOnSelect  %%######%% ');
        //system.debug('==>Inside SaveBillingOnSelect, selectedAccId '+selectedAccId);
        if(String.isNotBlank(selectedAccId))
        {
            List <OrderItem> OLineItemUpd=New List<OrderItem> ();
            Order OrderToUpdate=[select id, AccountId, name, BAN__c,Billing_Address__c from Order where id=:OderId];
            List<Account> accountList = [SELECT Id, Ban__c, Ban_Can__c, Billing_System__c, Billingstreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, Name 
                                         FROM Account 
                                         WHERE id=:selectedAccId];
            //system.debug('==>Inside SaveBillingOnSelect, accountList '+accountList);
            String BillAddr='';
            if(accountList[0].Billingstreet != null)
                BillAddr=BillAddr+' '+accountList[0].Billingstreet;
            if(accountList[0].BillingCity != null)
                BillAddr=BillAddr+' '+accountList[0].BillingCity;
            if(accountList[0].BillingState != null)
                BillAddr=BillAddr+' '+accountList[0].BillingState;
            if(accountList[0].BillingPostalCode != null)
                BillAddr=BillAddr+' '+accountList[0].BillingPostalCode;
            if(accountList[0].BillingCountry!= null)
                BillAddr=BillAddr+' '+accountList[0].BillingCountry;
            
            if(String.isBlank(OrderToUpdate.Billing_Address__c))
            {
                OrderToUpdate.Billing_Address__c=BillAddr;
                OrderToUpdate.Ban__c=accountList[0].BAN_CAN__c;
            }
    
            //system.debug('==>Inside SaveBillingOnSelect::Selected Bill Acc: '+accountList[0].name);
            
            Set<String> BillAddrUpdateSet = new Set<String>();
            /*
            for(OLIWrapper oliOLIWrapper: OLIWrapperList)
            {
                if(oliOLIWrapper.isSelected)
                    BillAddrUpdateSet.add(oliOLIWrapper.oli.id);
            }
            */
            for(String Key: OLIWrapperToBillingAddressMap.keySet())
            {
                for(OLIWrapper oliOLIWrapper: OLIWrapperToBillingAddressMap.get(Key))
                {
                    if(oliOLIWrapper.isSelected)
                        BillAddrUpdateSet.add(oliOLIWrapper.oli.id);
                }    
            }
            //system.debug('==>Inside SaveBillingOnSelect::Selected OLI IDs: '+BillAddrUpdateSet);
            if(BillAddrUpdateSet.size() > 0)
            {
                // Thomas - OCOM-1292
               /* List<OrderItem> items = [Select Id, OrderId, Billing_Address__c,vlocity_cmt__BillingAccountId__c,Billing_Account__c 
                                         from OrderItem 
                                         where Id in :BillAddrUpdateSet and orderMgmtId_Status__c!='Completed'];*/
                //system.debug('==>Inside SaveBillingOnSelect::Selected OLIs: '+items);
                for(OrderItem OlItem :[Select Id, OrderId, Billing_Address__c,vlocity_cmt__BillingAccountId__c,Billing_Account__c 
                                         from OrderItem 
                                         where Id in :BillAddrUpdateSet and orderMgmtId_Status__c!='Completed'])
                {   
                    OlItem.vlocity_cmt__BillingAccountId__c=accountList[0].id;
                    OlItem.Billing_Address__c=BillAddr;
                    OlItem.Billing_Account__c=accountList[0].BAN_CAN__c;
                    OLineItemUpd.add(OlItem);           
                    //system.debug('==>Inside onselect' +OlItem);
                }           
            }
             try
             {
                update OrderToUpdate;
                if(OLineItemUpd.size()>0)              
                    update OLineItemUpd;
                //system.debug('%%######%% END: OCOM_Capture_OLI_BillingAddrController:SaveBillingOnSelect  %%######%% ');
                return null; 
             }
             catch(exception e)
             {return null;
             }
         } 
        return null;
    }    
    
     /**
    Method:UpdateBillingForMACD()
     This method is to update Billing For MACD.
    Inputs Args:None
    Output Args:Pagereference
    **/ 
    Public Pagereference UpdateBillingForMACD()
    {
        //system.debug('%%######%% START: OCOM_Capture_OLI_BillingAddrController:UpdateBillingForMACD  %%######%% ');
        //system.debug('==>Inside UpdateBillingForMACD, selectedOliId'+selectedOliBillAcc);
        String sAddr = ' '+(String)BAData.addressLine1+ ' ' +(String)BAData.city+ ' ' +(String)BAData.province+ ' '+(String)BAData.postalCode+' ' +(String)BAData.country;
        
        if(sAddr.length() > 8 && null != OderId && String.isNotBlank(selectedOliBillAcc))
        {
            String notes = selectedOliBillAcc + '  ' +Label.OCOM_Update_Billing_Address+': ' +  sAddr;
            if(String.isNotBlank(billingNotes))
                notes = billingNotes + '; ' + notes;
            Order osupdate= new Order(id=OderId,Update_Billing_Address__c=sAddr, Billing_Notes_Remarks__c=notes);
            try
            {
                update osupdate;  
               // system.debug('%%######%% END: OCOM_Capture_OLI_BillingAddrController:UpdateBillingForMACD  %%######%% ');
                return null; 
            }
            catch(exception e)
            { return null;
            }
        }
        else
        { return null;
        }
        
        return null;
    } 
  
    /**
    Method:UpdateNotes()
   This method is for updating Notes.
    Inputs Args:None
    Output Args:Pagereference
    **/ 
    Public Pagereference UpdateNotes()
    {
       // system.debug('%%######%% Start: OCOM_Capture_OLI_BillingAddrController:UpdateNotes New Notes: '+billingNotes);   
        Order osupdate= new Order(id=OderId, Billing_Notes_Remarks__c=billingNotes);
        try
        {
            update osupdate;   
            return null; 
        }
        catch(exception e)
        {return null;
        }
        
        //return null
    }
    
     /**
    Class:OLIWrapper
     This is a Wrapper class.
    Inputs Args:None
    Output Args:None
    **/ 
    Public class OLIWrapper
    {
        Public boolean isSelected {get;set;}
        Public boolean showUpdateLink {get;set;}
        public OrderItem oli {get;set;}
        
         /**
         Method:OLIWrapper(orderItem o)
         This is a paramterized constructor.
         Inputs Args:orderItem
         Output Args:None
         **/ 
        public OLIWrapper(orderItem o)
        {
            showUpdateLink = false;
            if(String.isNotBlank(o.Billing_Address__c))
                showUpdateLink = true;
            this.isSelected= true;
            this.oli=o;
        }
    }
}