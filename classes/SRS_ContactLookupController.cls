/******************************************************************************
 * Project      : SRS R3
 * Class        : SRS_ContactLookupController
 * Description  : Apex class for SRS_ContactLookup VF page
 * Author       : Prashant Bhardwaj, IBM
 * Modification Log:
 * ---------------------------------------------------------------------------
 * Ver      Date                Modified By           Description
 * ---------------------------------------------------------------------------
 * 
 * ---------------------------------------------------------------------------
******************************************************************************/
public with sharing class SRS_ContactLookupController {

    public Account account {get;set;} // new account to create
    public List<Contact> results{get;set;} // search results
    public string searchString{get;set;} // search keyword
 
    public Contact contact {get;set;}
    public List<Contact> resultsContact{get;set;}
    public PageReference cancel() {
        flag = true;
        return null;
    }
    
    public Contact new_contact {get; set;}
    public Boolean flag {get;set;}
    public PageReference newContact() {
        String id = System.currentPageReference().getParameters().get('accId');
        Contact contact = new Contact(AccountId = id);
        flag = false;        
        return null;
    } 
 
    public SRS_ContactLookupController() {
        //account = new Account();
        // get the current search string
        flag = true;
        contact = new Contact();
        String id = System.currentPageReference().getParameters().get('accId');
        new_contact = new Contact(AccountId = Id);
        /*new_contact.FirstName = '';
        new_contact.LastName = '';
        new_contact.Email = '';
        new_contact.MailingStreet = '';
        new_contact.MailingCity = '';
        new_contact.MailingState = '';
        new_contact.MailingPostalCode = '';
        new_contact.MailingCountry = '';
        new_contact.Phone = '';
        new_contact.Fax = '';*/
        searchString = System.currentPageReference().getParameters().get('lksrch');
        runSearch();  
    }
    
    //insert new contact
    public PageReference save(){
        try{
            Schema.DescribeSObjectResult d = Schema.SObjectType.Contact;
            Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
            new_contact.RecordTypeId = rtMapByName.get('Service Request Contact').getRecordTypeId();
            insert new_contact;
        }catch(Exception e){
        system.debug('~~Error in saving Contact: ' + e);
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error Saving Contact. Please contact Support: '+ e.getmessage()));
        
        }
        flag = true;
        /*String id = System.currentPageReference().getParameters().get('accId');
        PageReference pageRef = new PageReference('/apex/SRS_ContactLookup?accId='+id);
        pageRef.setRedirect(true);
        return pageRef;*/
        PageReference curPage=new PageReference(ApexPages.currentPage().getURL());
        curPage.setRedirect(true);
        return curPage;
    }
     
 
    // performs the keyword search
    public PageReference search() {
        runSearch();
        return null;
    }
 
    // prepare the query and issue the search command
   @TestVisible private void runSearch() {
        // TODO prepare query string for complex serarches & prevent injections
        results = performSearch(searchString);               
    } 
 
    // run the search and return the records found. 
    @TestVisible private List<Contact> performSearch(string searchString) {
         //String id = '001f000000FiLrTAAV';
         searchString=string.escapeSingleQuotes(searchString);
        String id = System.currentPageReference().getParameters().get('accId');
        String soql = 'select Id, Name,MobilePhone,AccountId,Account.Name,Email,Phone from Contact where AccountId=';
        if(searchString != '' && searchString != null){
          soql = soql + '\'' + id + '\'' + ' and name LIKE \'%' + searchString +'%\'' + ' order by Name limit 9999';
        }
        else{
            soql = soql + '\'' + id + '\'' + ' order by Name limit 9999';
        }      
    
    System.debug('@@@@@@@@'+soql);
    return database.query(soql);  
    }
 
    // save the new account record
    public PageReference saveAccount() {
        insert account;
        // reset the account
        account = new Account();
        return null;
    }
 
    // used by the visualforce page to send the link to the right dom element
    public string getFormTag() {
        return System.currentPageReference().getParameters().get('frm');
    }
 
  // used by the visualforce page to send the link to the right dom element for the text box
    public string getTextBox() {
        return System.currentPageReference().getParameters().get('txt');
    }
 
}