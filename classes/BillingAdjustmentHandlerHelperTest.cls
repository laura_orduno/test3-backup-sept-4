@isTest
public class BillingAdjustmentHandlerHelperTest {

    @isTest
    public static void testValidateUserRelsaseCode(){
       Billing_Adjustment__c ba = new Billing_Adjustment__c();
       
       User agent= [Select Release_Code__c, Name from user where id =:userinfo.getUserId()];
       List<String> rcCases = new List<String> { Null, '00' };
       for(String val : rcCases){
             try{
               agent.Release_Code__c =val;
               BillingAdjustmentHandlerHelper.validateUserRelsaseCode(ba, agent);
             }catch(BillingAdjustmentHandlerHelper.BillingAdjustmentException e){
                     System.assertNotEquals(null, e.getMessage());
             } 
        }
        List<String> statusCases = new List<String> {'Authorized', 'Draft'} ;
 	    for(String val : statusCases){
             try{
               agent.Release_Code__c =null;  
               ba.Status__c =val;
               BillingAdjustmentHandlerHelper.validateUserRelsaseCode(ba, agent);
             }catch(BillingAdjustmentHandlerHelper.BillingAdjustmentException e){
                     System.assertNotEquals(null, e.getMessage());
             } 
        }
        try{
            agent.Release_Code__c ='01';
            agent.IsActive =false;
            ba.Status__c ='Pending';
            BillingAdjustmentHandlerHelper.validateUserRelsaseCode(ba, agent);
        }catch(BillingAdjustmentHandlerHelper.BillingAdjustmentException e){
            System.assertNotEquals(null, e.getMessage());
           
        
        }
        try{
            ba.Status__c ='Draft';
            agent.IsActive =false;
            BillingAdjustmentHandlerHelper.validateUserRelsaseCode(ba, agent);
        }catch(BillingAdjustmentHandlerHelper.BillingAdjustmentException e){
            System.assertNotEquals(null, e.getMessage());
            
        }
    }
    @isTest
    public static void testUpdateContorllerApprovedStatus(){
       Billing_Adjustment__c ba = new Billing_Adjustment__c();
       ba.Approval_Status__c =BillingAdjustmentHandlerHelper.PendingCCAprval; 
       BillingAdjustmentHandlerHelper.updateContorllerApprovedStatus(ba);
       System.assertEquals(true,ba.Corporate_Controller_Approved__c, 'Corporate Approver Should be true'); 
       ba.Approval_Status__c = BillingAdjustmentHandlerHelper.PendingBUCAprval; 
       BillingAdjustmentHandlerHelper.updateContorllerApprovedStatus(ba);
       System.assertEquals(true,ba.Controller_Approved__c, 'Corporate Approver Should be true');  
        
    }
    
}