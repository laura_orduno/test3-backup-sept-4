@IsTest
public class vlocity_addon_AutoCreateDocCntrl_Test {
    static testmethod void autoCreateContractUnitTest(){
        TestDataHelper.testContractVersionCreation();
        TestDataHelper.testCustomSettingCreation();
        TestDataHelper.testvlocityDocTemplateteCreation();
        Test.startTest();
        Apexpages.currentPage().getparameters().put('isInConsole','true');
        Apexpages.currentPage().getparameters().put('id',TestDataHelper.testContractObj.id);
       // ApexPages.currentPage().getParameters().put('isUpdate','true');
        vlocity_addon_AutoCreateContractDocCntrl objvlocity_addon_AutoCreateContractDocCntrl=new vlocity_addon_AutoCreateContractDocCntrl(new vlocity_cmt.ContractDocumentCreationController());
        boolean isInConsole=objvlocity_addon_AutoCreateContractDocCntrl.isInConsole;
        system.assertEquals(isInConsole, true);
        objvlocity_addon_AutoCreateContractDocCntrl.attachDoc();
       // ApexPages.currentPage().getParameters().put('isUpdate','false');
        Apexpages.currentPage().getparameters().put('isInConsole','false');
        ApexPages.currentPage().getParameters().put('isUpdate','true');
        Apexpages.currentPage().getparameters().put('id',TestDataHelper.testContractObj.id);
        vlocity_addon_AutoCreateContractDocCntrl objvlocity_addon_AutoCreateContractDocCntrl1=new vlocity_addon_AutoCreateContractDocCntrl(new vlocity_cmt.ContractDocumentCreationController());
        boolean isInConsole1=objvlocity_addon_AutoCreateContractDocCntrl1.isInConsole;
        
        Apexpages.currentPage().getparameters().put('isInConsole','false');
        ApexPages.currentPage().getParameters().put('isUpdate','true');
        Apexpages.currentPage().getparameters().put('id',TestDataHelper.testContractObj.id);
        vlocity_addon_AutoCreateContractDocCntrl objvlocity_addon_AutoCreateContractDocCntrl2=new vlocity_addon_AutoCreateContractDocCntrl(new vlocity_cmt.ContractDocumentCreationController());
        boolean isInConsole2=objvlocity_addon_AutoCreateContractDocCntrl2.isInConsole;
       // system.assertEquals(isInConsole1, false);
        objvlocity_addon_AutoCreateContractDocCntrl2.attachDoc();
        
        Test.stopTest();
    }
}