public class OrdrErrorCodes {
    
    public final static String TNR_WEBSERVICE_CALL_TIMEOUT_ERROR = 'TIMEOUTERR';
    public final static String ORDR_SUBMIT_MISSING_MANDATORY_FIELDS = 'ORDR0001';
	
    // Error Codes - Real-Time integration to TOCP/ASF-NC(for wireline)/(for wireless) checkGenericTechnicalAvailability TOCP Business Service through SOA Service Manager - for TN Portability	
	//-- checkgenerictechnicalavailability
	public final static String TNR_NXX_NOT_PORTABLE = 'TNR0101'; // Nxx entered is not portable to this address. Select a valid Nxx and search again
	
	
	// Error Codes - Real-Time integration to TOCP/ASF for Wireline real TN and Dark ADSL - TN reservation - TOCP QueryResource, FindResource, ReserveResource, RecoverResource	
	//--  queryResource, findResource, reserveResource, recoverResource sub-packages
	public final static String TNR_GENERAL_CONNECTIVITY_ERROR = 'TNR0001'; // Error: The reservation system is temporarily unavailable. Please try again
	public final static String TNR_UNEXPECTED_ERROR = 'TNR0002'; // Error: The reservation system is temporarily unavailable. Please try again
	public final static String TNR_INVALID_NXX = 'TNR0201'; // The Nxx entered is not valid. Check the Nxx and search again
	public final static String TNR_NO_SPARE_TN = 'TNR0003'; // No spare telephone numbers are available at this COID
	public final static String TNR_RESERVATION_FAILED = 'TNR0004'; // Error: Some of the selected numbers were not reserved. Error: None of the selected numbers were reserved.
	public final static String TNR_NO_TN_AVAILABLE_FOR_SELECTED_NPANXX = 'TNR0005'; // TO BE CHECKED // No Spare Telephone Number Found for the given NpaNxx
	public final static String TNR_INVALID_NXX_FOR_SELECTED_NPANXX = 'TNR0007'; // The NXX entered is not valid. Check the NXX entered and search again
	
	
	// Error Codes - Real-Time integration to TOCP/ASF for Toll Free Number reservation - TOCP FindResource, ReserveResource, RecoverResource	
	//--  findResource, reserveResource, recoverResource sub-packages
	public final static String TNT_GENERAL_CONNECTIVITY_ERROR = 'TNT0001'; // Error: The reservation system is temporarily unavailable. Please try again
	public final static String TNT_UNKNOWN_EXCEPTION = 'TNT0002'; // Error: The reservation system is temporarily unavailable. Please try again
	public final static String TNT_TOLLFREE_NOT_AVAILABLE = 'TNT0004'; // Telephone number(s) selected are not available, please search and select new TN.
	public final static String TNT_NXX_CLOSED = 'TNT0003'; // NXX is closed, please select an alternate NXX and try again
	public final static String TNT_SEARCH_FAILED = 'TNT0005'; // Telephone number search has failed, please try again
	public final static String TNT_INVALID_NXX = 'TNT0201'; // The NXX entered is not valid. Check the NXX entered and search again.
	public final static String TNT_NOT_ENOUGH_SPARES = 'TNT0006'; // Cannot find enough spare numbers for requested quantity, please reduce amount of TNs requested
	public final static String TNT_NOT_FOUND = 'TNT0007'; // Telephone number(s) cannot be found, please search and select new TN
	public final static String TNT_RANDOM_SEL_UNAVAILABLE = 'TNT0008'; // Random TN selection not available, please search again
	public final static String TNT_RESERVATION_FAILED = 'TNT0009'; // Some of the Telephone number(s) selected were not reserved
	public final static String TNT_NEAR_RESERVATION_LIMIT = 'TNT0010'; // Nearing the reservation limit
	public final static String TNT_INVALID_CRITERIA_LAST4 = 'TNT0301'; // Invalid search criteria, Last 4 characters must be '*' or '&'
	public final static String TNT_INVALID_CRITERIA_AMPER = 'TNT0303'; // Invalid search criteria, number cannot contain a single '&'
	public final static String TNT_INVALID_VANITY_NUMBER = 'TNT0304'; // Invalid Vanity Number
	public final static String TNT_RESERVATION_IN_PROGRESS = 'TNT0306'; // TN reservation in progress
	public final static String TNT_CREATE_CUST_IN_PROGRESS = 'TNT0302'; // Customer record creation in progress
	public final static String TNT_INVALID_NPA = 'TNT0307'; // The NPA entered is not valid. Check the NPA entered and search again
	public final static String TNT_DB_ERROR = 'TNT0011'; // System error-database inconsistency, contact the SMS/800 Help Desk
	public final static String TNT_INVALID_CRITERIA_WILDCARD = 'TNT0308'; // Invalid search criteria, value cannot have wildcard in both NPA and NXX entries
	public final static String TNT_INVALID_CRITERIA_NPANXX = 'TNT0309'; // Invalid search criteria, A specific NPA or NXX entry is required
	public final static String TNT_INVALID_CRITERIA_NPA = 'TNT0310'; // Invalid search criteria, A specific NPA entry is required
	
    

    //[Roderick] 2016-11-09
    // Product Eligibility (PCOF) Exceptions
    public final static String PRODUCTS_NOTELIGIBLE_ERROR = 'PCOF0001';

    //[Roderick] 2016-11-14
    // Account/Service Account Exceptions
    public final static String SERVICE_ACCOUNT_CREATE_ERROR = 'ACCT0001';

    //[Roderick] 2016-11-09
    // Default Error/Generic Exceptions
    public final static String WEBSERVICE_CALL_TIMEOUT_ERROR = 'TIMEOUTERR';
    public final static String DEFAULT_UNEXPECTED_ERROR = 'DFLT0001';
    public final static String ADDRESS_NOTFOUND_ERROR = 'DFLT0002';   
    public final static String DEFAULT_CONNECTIVITY_ERROR = 'DFLT0003';
    public final static String MISSING_REQUIRED_PARAMETER_ERROR = 'DFLT0004';
    
    //Error codes to message mapping
    public static final Map<String, String> ERROR_MESSAGES_MAP;
    static {
        ERROR_MESSAGES_MAP = new Map<String, String>();
        ERROR_MESSAGES_MAP.put(WEBSERVICE_CALL_TIMEOUT_ERROR, Label.TIMEOUTERR);
        ERROR_MESSAGES_MAP.put(DEFAULT_UNEXPECTED_ERROR, Label.DFLT0001);
        ERROR_MESSAGES_MAP.put(ADDRESS_NOTFOUND_ERROR, Label.DFLT0002);
        ERROR_MESSAGES_MAP.put(DEFAULT_CONNECTIVITY_ERROR, Label.DFLT0003);
        ERROR_MESSAGES_MAP.put(MISSING_REQUIRED_PARAMETER_ERROR, Label.DFLT0004);
        ERROR_MESSAGES_MAP.put(PRODUCTS_NOTELIGIBLE_ERROR, Label.PCOF0001);
        ERROR_MESSAGES_MAP.put(SERVICE_ACCOUNT_CREATE_ERROR, Label.ACCT0001);
    }
    
     //Error codes to message mapping
    public static final Map<String, String> ERROR_MESSAGES_MAP_BILLING_ACCOUNT_CREATION;
    static {
        ERROR_MESSAGES_MAP_BILLING_ACCOUNT_CREATION = new Map<String, String>();
        ERROR_MESSAGES_MAP_BILLING_ACCOUNT_CREATION.put(WEBSERVICE_CALL_TIMEOUT_ERROR, Label.TIMEOUTERR);
        ERROR_MESSAGES_MAP_BILLING_ACCOUNT_CREATION.put(DEFAULT_UNEXPECTED_ERROR, Label.DFLT0001);
        ERROR_MESSAGES_MAP_BILLING_ACCOUNT_CREATION.put(ADDRESS_NOTFOUND_ERROR, Label.DFLT0002);
        ERROR_MESSAGES_MAP_BILLING_ACCOUNT_CREATION.put(DEFAULT_CONNECTIVITY_ERROR, Label.DFLT0003);
        ERROR_MESSAGES_MAP_BILLING_ACCOUNT_CREATION.put(MISSING_REQUIRED_PARAMETER_ERROR, Label.DFLT0004);
    }
  
}