/*
########################################################################################
# Created by............: Bhanu Priya
# Created Date..........: 1-Dec-2015
# Last Modified by......: Bhanu Priya
# Last Modified Date....: 
# Description...........: This is a class for QuotePdfNew Controller
########################################################################################
*/


global with sharing class QuotePdfControllerNew {
    public Quote quote {get; set;}
  
    public ID quoteId {get;set;}
    public String dateTmStr {get;set;}
    
    public String pdfFileName {get;set;}
    public string doSave{get;set;}
    public string ignoredColumns{get;set;}
    
    public string documentLogoUrl {get;set;}
    public List<qLineItem> lineItems {get;set;}
    
    
    ApexPages.Standardcontroller cont;
    
    global QuotePdfControllerNew(ApexPages.StandardController stdController) {
        //System.debug('::::IN QuotePdfControllerNew');
        cont = stdController;
        this.quote = (Quote)stdController.getRecord();
        quoteId = quote.Id;
        initializeData();
    } 
    
    public void initializeData()
    {
        //if (initializedData) return;
        //initializedData = true;
        
       // ignoredColumns = { 'IsDeleted','CreatedDate','CreatedById','LastModifiedDate','LastModifiedById','SystemModstamp','LastActivityDate','CleanStatus' };

        quote = getQuote();
        lineItems = getLineItems(); 
    
    }   
    
    public Quote getQuote()
    {
        // Set<String> ignoredColumns = new Set<String>{ };

        
        String query = 'Select ';
        Boolean firstField = true;
        for (Schema.SObjectField field : Quote.getSobjectType().getDescribe().fields.getMap().values())
        {
           // if (!ignoredColumns.contains(String.valueOf(field)))
           // {
                if (!firstField)
                {
                    query += ',';
                }

                query += String.valueOf(field);

                firstField = false;
            //}
        } 

        query += ',Account.Name';
        query += ',LastModifiedBy.FirstName,LastModifiedBy.LastName';
        query += ',LastModifiedBy.Email';
        query += ',Contact.FirstName,Contact.LastName';
        //query += ',BillingName,ShippingName';
        query += ' FROM Quote' ;
        query += ' WHERE Id = \'' + quote.Id + '\'';
        query += ' LIMIT ' + 1;
       
        return Database.query(query); 
        
    }
     
    public List<qLineItem> getLineItems() 
     {  
        //Logger.err('getLineItems');
        //String nsp = ApplicationUtilities.getNamespacePrefix();
            if(lineItems == null) {
                lineItems = new List<qLineItem>();
                for (QuoteLineItem li: [Select Id,
                                vlocity_cmt__LineNumber__c,
                                PriceBookEntry.Product2.Name,
                                PriceBookEntry.Product2.ProductCode,
                                PriceBookEntry.Product2.Id,
                                ListPrice,
                                UnitPrice,
                                Quantity,
                                vlocity_cmt__JSONAttribute__c,

                                vlocity_cmt__OneTimeCharge__c,
                                vlocity_cmt__OneTimeCalculatedPrice__c,
                                vlocity_cmt__OneTimeManualDiscount__c,
                                vlocity_cmt__OneTimeDiscountPrice__c,
                                vlocity_cmt__OneTimeTotal__c,
                                
                                vlocity_cmt__RecurringCharge__c,
                                vlocity_cmt__RecurringCalculatedPrice__c,
                                vlocity_cmt__RecurringManualDiscount__c,
                                vlocity_cmt__RecurringDiscountPrice__c,
                                vlocity_cmt__RecurringTotal__c,

                                vlocity_cmt__ProvisioningStatus__c,
                                vlocity_cmt__ServiceAccountId__r.Name,
                                vlocity_cmt__BillingAccountId__r.Name
                                FROM QuoteLineItem 
                                WHERE QuoteId = :quote.Id  
                                ORDER BY vlocity_cmt__LineNumber__c])  {
                        lineItems.add(new qLineItem(li));
                     }
            }  
            return lineItems;
    }       

    // qLineItem is wrapper 
    public class qLineItem {
    
        public QuoteLineItem li {get; set;}
        public String indentSpace {get; set;}
        public Map<String,OCOM_ParseJSONUtil.TELUSCHAR> Attributes{get;set;}
        public List<OCOM_ParseJSONUtil.TELUSCHAR> ListAttributes {get{ return Attributes.values();}}

                                            
    // Constructor for qLineItem
        public qLineItem(QuoteLineItem qotItem) {
            li = qotItem;
     
            if (li.vlocity_cmt__LineNumber__c == null){
    System.debug('::::In if null'); 
                indentSpace='';
            }
            else{
    System.debug('::::LineNumber:'+ li.vlocity_cmt__LineNumber__c + ':end');    
                integer sz = li.vlocity_cmt__LineNumber__c.countMatches('.');
        
                if (sz > 0){
                //indentSpace = '&nbsp;';
                    sz *= 3;
                    indentSpace = padSpace(sz);
                    indentSpace += '&nbsp;';
                } else {
                    indentSpace='';
                }
            }
            //Vlocity Feb 16 2016 Adding Attributes as key values 
            if(String.isNotEmpty(li.vlocity_cmt__JSONAttribute__c)){
                Attributes = OCOM_ParseJSONUtil.getMapOfTeluschar(li.vlocity_cmt__JSONAttribute__c);
                system.debug('Attributes values '+Attributes );
            }else{
                Attributes = new Map<String,OCOM_ParseJSONUtil.TELUSCHAR>();
                system.debug('Attributes values1 '+Attributes );
            }
        }

        //public String getJSONAttribute(String attr, String val) {
        //    String myValue = '';
        //    Map<String, Object> attributes = (Map<String, Object>)li.vlocity_cmt__JSONAttribute__c();
        //    List<Object> tChar = attributes.get('TELUSCHAR');
        //    for(Object t: tChar) {
        //        Map<String, Object> myAttr = (Map<String,Object>)t;
        //        if(myAttr.get('Name') == attr) {
        //            return myAttr.get(val);
        //        }
        //    }
        //}     
    }

    public static String padSpace(Integer i) {
        String str = '';

        for( integer j=0; j < i; j++) {
            str += '&nbsp;' ;
        }
    System.debug('::::str:'+ str + 'end');          
        return str;
    }       
   
    public PageReference savePdf(){ 
      
      if(doSave == 'No'){
        return null;
      }
          
         initializeData();
System.debug('::::AfterInitializeData');

      PageReference pagePdf = new PageReference('/apex/QuotePdf_New');
System.debug('::::in savePDF');
          pagePdf.getParameters().put('id', Quote.Id);
          pagePdf.getParameters().put('doSave', 'No');
          Blob pdfPageBlob;
          if (Test.isRunningTest()){
                String strUrlUTF8 = EncodingUtil.urlEncode('Test', 'UTF-8');
                
                pdfPageBlob = Blob.valueOf(strUrlUTF8);
 System.debug('Blob: [' + pdfPageBlob.toString() + ']');
          } else {
                pdfPageBlob = pagePdf.getContentAsPDF();
          }
          
          Datetime dateTm = Datetime.now();
          dateTmStr = dateTm.format();
         //pdfFileName = 'PDF_' + dateTmStr + '.pdf';
          Integer size= [SELECT Count() FROM Attachment where ParentID= :Quote.Id];
          size=size+1;           
          pdfFileName = Quote.QuoteNumber+'-'+'V'+size;          
          Attachment a = new Attachment();
          a.Body = pdfPageBlob;
          a.ParentID = Quote.Id;
          a.Name = pdfFileName;
          a.Description = 'PDF';
          insert a;
           
          //return new PageReference('/' + Quote.Id);
          return new PageReference('/apex/Email_Quote_Pdf?id=' + Quote.Id);
      //return pageRef;
    }       
}