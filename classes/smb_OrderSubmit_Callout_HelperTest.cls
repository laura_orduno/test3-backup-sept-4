@isTest(SeeAllData=true)
private class smb_OrderSubmit_Callout_HelperTest {

    static testMethod void Test_SubmitOrderRequest() {
        smb_test_utility.createCustomSettingData();
        SMB_DummyWebServiceResponse_Test.intializeMockWebservice();
        Opportunity test_opp = smb_test_utility.createOpportunity(Null, Null, Null, false);
        test_opp.Type = 'New Provide/Upgrade Order';
        test_opp.Requested_Due_Date__c=date.today();
        test_opp.Order_Remarks_Permanent__c='TestPermanent';
        test_opp.Order_Remarks_Temporary__c = 'TestTemporary';
        test_opp.Billing_notes__c= 'TestBillingNotes';
        Insert test_opp;
        smb_test_utility.createQuote(Null,test_opp.Id,true);
        smb_test_utility.createPQLI('CPQ_PrimaryQuoteLines', test_opp.Id);
        Order_Request__c oReq = smb_test_utility.createOrderRequest(Null, test_opp.Id,false);
        oReq.Order__c = 'In Progress';
        insert oReq;
        String ReqId = oReq.Id;
        smb_OrderSubmit_Callout_Helper OrderSubmit_Callout_Helper = new smb_OrderSubmit_Callout_Helper();
        Test.startTest();
        smb_OrderSubmit_Callout_Helper.SubmitOrderRequest(test_opp.Id);
        OrderSubmit_Callout_Helper.SubmitOrderRequestBatch(test_opp.Id);
        smb_OrderSubmit_Callout_Helper.calloutOrderRequest(test_opp.Id, ReqId); 
        
        Test.stopTest();

        

    }
    
        static testMethod void Test_SubmitOrderRequest2() {
            smb_test_utility.createCustomSettingData();
            SMB_DummyWebServiceResponse_Test.intializeMockWebservice();
            Opportunity test_opp = smb_test_utility.createOpportunity(Null, Null, Null, false);
            test_opp.Type = 'New Provide/Upgrade Order';
            Insert test_opp;
            smb_test_utility.createQuote(Null,test_opp.Id,true);
            smb_test_utility.createPQLI('CPQ_PrimaryQuoteLines', test_opp.Id);
            smb_OrderSubmit_Callout_Helper OrderSubmit_Callout_Helper = new smb_OrderSubmit_Callout_Helper();
            Test.startTest();
            smb_OrderSubmit_Callout_Helper.SubmitOrderRequest(test_opp.Id);
            OrderSubmit_Callout_Helper.SubmitOrderRequestBatch(test_opp.Id);

           Test.stopTest();
    
            
    
    }
    
    
        static testMethod void Test_SubmitOrderRequest3() {
            smb_test_utility.createCustomSettingData();
            SMB_DummyWebServiceResponse_Test.intializeMockWebservice();
            Opportunity test_opp = smb_test_utility.createOpportunity(Null, Null, Null, false);
            test_opp.Type = 'New Move Order';
            Insert test_opp;
            smb_test_utility.createQuote(Null,test_opp.Id,true);
            smb_test_utility.createPQLI('PQL_Test_Data_2', test_opp.Id);
            Order_Request__c oReq = smb_test_utility.createOrderRequest(Null, test_opp.Id,True);
            smb_OrderSubmit_Callout_Helper OrderSubmit_Callout_Helper = new smb_OrderSubmit_Callout_Helper();
            Test.startTest();
            smb_OrderSubmit_Callout_Helper.SubmitOrderRequest(test_opp.Id);
            OrderSubmit_Callout_Helper.SubmitOrderRequestBatch(test_opp.Id);

           Test.stopTest();
    
            
    
    }
    
    static testMethod void Test_SubmitOrderRequest4() {
        smb_test_utility.createCustomSettingData();
        SMB_DummyWebServiceResponse_Test.intializeMockWebservice();
        Opportunity test_opp = smb_test_utility.createOpportunity(Null, Null, Null, false);
        test_opp.Type = 'New Provide/Upgrade Order';
        test_opp.Requested_Due_Date__c=date.today();
        test_opp.Order_Remarks_Permanent__c='TestPermanent';
        test_opp.Order_Remarks_Temporary__c = 'TestTemporary';
        test_opp.Billing_notes__c= 'TestBillingNotes';
        Insert test_opp;
        smb_test_utility.createQuote(Null,test_opp.Id,true);
        smb_test_utility.createPQLI('NewPQL', test_opp.Id);
        Order_Request__c oReq = smb_test_utility.createOrderRequest(Null, test_opp.Id,false);
        oReq.Order__c = 'In Progress';
        insert oReq;
        String ReqId = oReq.Id;
        smb_OrderSubmit_Callout_Helper OrderSubmit_Callout_Helper = new smb_OrderSubmit_Callout_Helper();
        Test.startTest();
        //smb_OrderSubmit_Callout_Helper.SubmitOrderRequest(test_opp.Id);
        //OrderSubmit_Callout_Helper.SubmitOrderRequestBatch(test_opp.Id);
        smb_OrderSubmit_Callout_Helper.calloutOrderRequest(test_opp.Id, ReqId); 
        
        Test.stopTest();
    }
        static testMethod void Test_SubmitOrderRequest5() {
        smb_test_utility.createCustomSettingData();
        SMB_DummyWebServiceResponse_Test.intializeMockWebservice();
        Opportunity test_opp = smb_test_utility.createOpportunity(Null, Null, Null, false);
        test_opp.Type = 'New Provide/Upgrade Order';
        test_opp.Requested_Due_Date__c=date.today();
        test_opp.Order_Remarks_Permanent__c='TestPermanent';
        test_opp.Order_Remarks_Temporary__c = 'TestTemporary';
        test_opp.Billing_notes__c= 'TestBillingNotes';
        Insert test_opp;
        smb_test_utility.createQuote(Null,test_opp.Id,true);
        smb_test_utility.createPQLI('PQL_Test_Data', test_opp.Id);
        Order_Request__c oReq = smb_test_utility.createOrderRequest(Null, test_opp.Id,false);
        oReq.Order__c = 'In Progress';
        insert oReq;
        String ReqId = oReq.Id;
        smb_OrderSubmit_Callout_Helper OrderSubmit_Callout_Helper = new smb_OrderSubmit_Callout_Helper();
        Test.startTest();
        //smb_OrderSubmit_Callout_Helper.SubmitOrderRequest(test_opp.Id);
        //OrderSubmit_Callout_Helper.SubmitOrderRequestBatch(test_opp.Id);
        smb_OrderSubmit_Callout_Helper.calloutOrderRequest(test_opp.Id, ReqId); 
        
        Test.stopTest();
    }
    /*static testMethod void Test_SubmitOrderRequest6() {
        smb_test_utility.createCustomSettingData();
        SMB_DummyWebServiceResponse_Test.intializeMockWebservice();
        Opportunity test_opp = smb_test_utility.createOpportunity(Null, Null, Null, false);
        test_opp.Type = 'New Provide/Upgrade Order';
        test_opp.Requested_Due_Date__c=date.today();
        test_opp.Order_Remarks_Permanent__c='TestPermanent';
        test_opp.Order_Remarks_Temporary__c = 'TestTemporary';
        test_opp.Billing_notes__c= 'TestBillingNotes';
        Insert test_opp;
        
        Opportunitylineitem OLI = new Opportunitylineitem();
        OLI.Product_Name__c = 'Voicemail to text';
        OLI.Opportunity_Id__c = test_opp.id;
        OLI.OpportunityId = test_opp.id;
        Insert OLI;
        Opportunitylineitem OLI1 = new Opportunitylineitem();
        OLI1.Product_Name__c = 'Name Display';
        OLI1.Opportunity_Id__c = test_opp.id;
        OLI1.OpportunityId = test_opp.id;
        Insert OLI1;
        
        smb_test_utility.createQuote(Null,test_opp.Id,true);
        smb_test_utility.createPQLI('PQL_Test_Data', test_opp.Id);
        Order_Request__c oReq = smb_test_utility.createOrderRequest(Null, test_opp.Id,false);
        oReq.Order__c = 'In Progress';
        insert oReq;
        String ReqId = oReq.Id;
        smb_OrderSubmit_Callout_Helper OrderSubmit_Callout_Helper = new smb_OrderSubmit_Callout_Helper();
        Test.startTest();
        //smb_OrderSubmit_Callout_Helper.SubmitOrderRequest(test_opp.Id);
        //OrderSubmit_Callout_Helper.SubmitOrderRequestBatch(test_opp.Id);
        smb_OrderSubmit_Callout_Helper.calloutOrderRequest(test_opp.Id, ReqId); 
        
        Test.stopTest();
    }*/
    static testMethod void Test_SubmitOrderRequest7() {
            smb_test_utility.createCustomSettingData();
            SMB_DummyWebServiceResponse_Test.intializeMockWebservice();
            Opportunity test_opp = smb_test_utility.createOpportunity(Null, Null, Null, false);
            test_opp.Type = 'New Provide/Upgrade Order';
            Insert test_opp;
            //smb_test_utility.createProduct();
            //Insert test_pdt;

            list<OpportunityLineItem> test_oli = [select id, opportunityId from OpportunityLineItem where opportunityId =:test_opp.id limit 1];
            //test_oli.OpportunityId = test_opp.Id;
            //test_oli.Product2.Id = test_pdt.Id;
            //test_oli.PriceBookEntryId = test_pbe.Id;
            //test_oli.TotalPrice = 420.00;
            //test_oli.Quantity = 2.0;
            if(test_oli != null && test_oli.size() > 0){
            test_oli[0].Product_Name__c = 'Voicemail to text';
            test_oli[0].Quantity =1.0;
            test_oli[0].Item_ID__c = 'Phone Number Setup';
            update test_oli;
            
            system.debug('&&&&&&&&&&&&&&&&&&&&'+test_oli);
            }


            smb_test_utility.createQuote(Null,test_opp.Id,true);
            smb_test_utility.createPQLI('CPQ_PrimaryQuoteLines', test_opp.Id);
            Order_Request__c oReq = smb_test_utility.createOrderRequest(Null, test_opp.Id,True);
            smb_OrderSubmit_Callout_Helper OrderSubmit_Callout_Helper = new smb_OrderSubmit_Callout_Helper();
            Test.startTest();
            smb_OrderSubmit_Callout_Helper.SubmitOrderRequest(test_opp.Id);
            OrderSubmit_Callout_Helper.SubmitOrderRequestBatch(test_opp.Id);

           Test.stopTest();
    } 

}