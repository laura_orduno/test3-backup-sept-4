@isTest
private class OCOM_PostChangeHandlerImpl_Test 
{
    static TestMethod void testMethod1() {
         
         OCOM_PostChangeHandlerImplementation impObj = new OCOM_PostChangeHandlerImplementation();
          Map<String, Object> input=new Map<String, Object>();
          Map<String, Object> output=new Map<String, Object>();
          Map<String, Object> options=new Map<String, Object>();
          
          
          
          Schema.DescribeSObjectResult d = Schema.SObjectType.Account; 
          Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
          Schema.RecordTypeInfo rtByName =  rtMapByName.get('CAN');
          system.debug(' id'+rtByName.getRecordTypeId());
          Id recTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Billing').getRecordTypeId();
          Id recSerTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
          Id recRCIDTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId();
        

          Account RCIDacc = new Account(name='debs',recordtypeid=recRCIDTypeId);
          Account acc = new Account(name='Billing account',BAN_CAN__c='draft',recordtypeid=recTypeId);
          Account seracc = new Account(name='123 street',recordtypeid=recSerTypeId,parentid=RCIDacc.id);
          
          
          List<Account> acclist= new List<Account>();          
          acclist.add(RCIDacc);
          acclist.add(acc);
          acclist.add(seracc );
          insert acclist;
          
          smbcare_address__c address= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N5',FMS_Address_ID__c='987651',account__c=RCIDacc.id,Service_account_Id__c=seracc.id); 
          insert address;
         
          
           // First, set up test price book entries.
        // Insert a test product.
        Product2 prod = new Product2(Name = 'Laptop X200', 
            Family = 'Hardware');
        insert prod;
        System.assertEquals(prod.name, 'Laptop X200');
        // Get standard price book ID.
        // This is available irrespective of the state of SeeAllData.
        Id pricebookId = Test.getStandardPricebookId();
        
        // 1. Insert a price book entry for the standard price book.
        // Standard price book entries require the standard price book ID we got earlier.
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        // Create a custom price book
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        // 2. Insert a price book entry with a custom price.
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        contact c= new contact(firstname='abc',lastname='xyz');
        insert c;
         order ord = new order(Ban__c='draft',accountId=seracc.id,effectivedate=system.today(),status='not submitted',Pricebook2Id=customPB.id,CustomerAuthorizedById=c.id);   
         order ord2 = new order(Ban__c='draft',accountId=acc.id,effectivedate=system.today(),status='not submitted',Pricebook2Id=customPB.id,CustomerAuthorizedById=c.id);   
         List<Order>ordlist= new List<Order>();   
         ordList.add(ord);ordList.add(ord2);
         insert ordList; 
         System.assertEquals(ordList.size(), 2);
        
          orderItem  OLI2= new orderItem(UnitPrice=12,orderId= ord2.Id,vlocity_cmt__BillingAccountId__c=RCIDacc.id,vlocity_cmt__ServiceAccountId__c=seracc.id,Quantity=2,PricebookEntryId=customPrice.id);
          insert OLI2;  
          orderItem  OLI= new orderItem(UnitPrice=12,orderId= ord.Id,vlocity_cmt__AssetReferenceId__c=OLI2.id,vlocity_cmt__BillingAccountId__c=null,vlocity_cmt__ServiceAccountId__c=null,Quantity=2,PricebookEntryId=customPrice.id);
          
          List<OrderItem>OLIlist= new List<OrderItem>();   
        //  OLIList.add(OLI2);
          OLIList.add(OLI);
          //insert OLIList;
                  Test.startTest();
          System.assertEquals(OLIList.size(), 1);
            //input|{accountId=0015600000456WFAAY, assetIds=(02i560000005bzmAAA, 02i560000005bznAAA, 02i560000005bzoAAA, 02i560000005bzpAAA), createdObjectId=80156000000DhaPAAS
            input.put('createdObjectId',ord.id);
            input.put('accountId',seracc.id);
             system.debug('Inside OrderLineItem '+OLIList);
            impObj.invokeMethod('postChangeForOrder',input,output, options);
          
            opportunity opp = new opportunity(name='abd',stagename='Discovery',closedate=system.today());
            insert opp;
            Quote quot1 = new Quote(name='quot1',Ban__c='draft',opportunityId=opp.id,status='not submitted',Pricebook2Id=customPB.id);   
            Quote quot2 = new Quote(name='quot2',Ban__c='draft',opportunityId=opp.id,status='not submitted',Pricebook2Id=customPB.id);   
            List<Quote>quotlist= new List<Quote>();   
           quotlist.add(quot1);quotlist.add(quot2);
           insert quotlist; 
           System.assertEquals(quotlist.size(),2);
        
            quoteLineItem  QLI2= new quoteLineItem(UnitPrice=12,quoteId= quot2.Id,vlocity_cmt__BillingAccountId__c=RCIDacc.id,vlocity_cmt__ServiceAccountId__c=seracc.id,Quantity=2,PricebookEntryId=customPrice.id);
            insert QLI2;  
            quoteLineItem  QLI= new quoteLineItem(UnitPrice=12,quoteId= quot1.Id,vlocity_cmt__AssetReferenceId__c=OLI2.id,vlocity_cmt__BillingAccountId__c=null,vlocity_cmt__ServiceAccountId__c=seracc.id,Quantity=2,PricebookEntryId=customPrice.id);
          
          List<quoteLineItem>QLIlist= new List<quoteLineItem>();   
        //  OLIList.add(OLI2);
          QLIList.add(QLI);
          insert QLIList;
            input.put('createdObjectId',quot1.id);            
            impObj.invokeMethod('postChangeForQuote',input,output, options);
           
           Test.stopTest();
             
    }
  @isTest
    private static void testMethod2() {
         String orderId=OrdrTestDataFactory.singleMethodForOrderId();
        OCOM_PostChangeHandlerImplementation impObj = new OCOM_PostChangeHandlerImplementation();
          Map<String, Object> input=new Map<String, Object>();
          Map<String, Object> output=new Map<String, Object>();
          Map<String, Object> options=new Map<String, Object>();
            Id recTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Billing').getRecordTypeId();
          Id recSerTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
          Id recRCIDTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId();
        

          Account RCIDacc = new Account(name='debs',recordtypeid=recRCIDTypeId);
          Account acc = new Account(name='Billing account',BAN_CAN__c='draft',recordtypeid=recTypeId);
          Account seracc = new Account(name='123 street',recordtypeid=recSerTypeId,parentid=RCIDacc.id);
        Test.startTest();
        input.put('createdObjectId',orderId);
            input.put('accountId',seracc.id);
            // system.debug('Inside OrderLineItem '+OLIList);
            impObj.invokeMethod('postChangeForOrder',input,output, options);
        Test.stopTest();
    }
}