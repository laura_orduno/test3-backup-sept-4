global class ScheduleSynchParentChildAccountFields implements Schedulable{

    global void execute(SchedulableContext sc) {
        batch_SyncParentChildAccountFields b = new batch_SyncParentChildAccountFields();
        database.executeBatch(b);
    }
}