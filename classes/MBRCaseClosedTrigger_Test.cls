@isTest
private class MBRCaseClosedTrigger_Test {
    
    @isTest static void testMBRCaseClosedTrigger() {

        /*    
        Account a = new Account(Name = 'Unit Test Account');
        insert a;
        */
        
        User u = MBRTestUtils.createPortalUser('');
        System.debug('u: ' + u);
        
        System.runAs(u) {
        
            // setup email template
            EmailTemplate templ = new EmailTemplate();
            templ.isActive = true;
            templ.Name = 'MBR case is closed collaborators';
            templ.DeveloperName = 'MBR_case_is_closed_collaborators';
            templ.TemplateType = 'text';
            templ.FolderId = UserInfo.getUserId();
            insert templ;
            System.debug('templ: ' + templ);
    
            Account a = [SELECT Id FROM Account LIMIT 1];
    
            System.debug('a: ' + a);
    
            List<Case> cases = MBRTestUtils.createCCICases(1, a.Id);
    
            System.debug('cases: ' + cases);
    
            Case c = cases[0];
            
            System.debug('c: ' + c);
    
            Test.startTest();
    
            c.NotifyCollaboratorString__c = 'testtest@unit-test.com;testtest2@unit-test.com';
            c.Status = 'Closed';
            update c;
    
            System.debug('c2: ' + c);
    
            Test.stopTest();
        }
    }

}