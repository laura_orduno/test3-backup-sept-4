public without sharing class QuotePrintController {
	public String markup {get; private set;}
	public String previewError {get; private set;}
	
	private String returnUrl;
	
	public QuotePrintController() {
		System.assert(Quote_Portal__c.getInstance() != null, 'Quote Portal settings are missing');
		
		String qids = ApexPages.currentPage().getParameters().get('qids');
		String tid = ApexPages.currentPage().getParameters().get('tid');
		if (tid == null) {
			tid = Quote_Portal__c.getInstance().Quote_Template_Id__c;
		}
		returnUrl = ApexPages.currentPage().getParameters().get('retURL');
		
		SBQQ__QuoteTemplate__c template = [SELECT SBQQ__LogoDocumentId__c FROM SBQQ__QuoteTemplate__c WHERE Id = :tid];
		QuoteDocumentGenerator generator = new QuoteDocumentGenerator(template);
		generator.recombo = false;
		markup = generator.preview(qids);
	}
	
	public PageReference onFinish() {
		return new PageReference(returnUrl);
	}
	
}