/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class smb_test_ValidateAddressControllerExt {

    static testMethod void testValidateAddress() {
     
    	
    	string url = 'https:;&TCCS_UTN::DD::CC::\\?drive.google.com;&TCCS_UTN\\?::DD::CC::&TCCS_UTN\\?::DD::CC::';
    	PageReference ref = new PageReference('/apex/smb_NewAccount');
       	Test.setCurrentPage(ref);
        smb_CtiUtility smbCti = new smb_CtiUtility();
        
        map<string, string>  mapOfStrings = new map<string, string>(); 
        mapOfStrings = smb_CtiUtility.parseUrl(url);
        
        string getCtiParameterString = smb_CtiUtility.getCtiParameterString(url);    	
    	
    	smb_NewAccountController smbAcc = new smb_NewAccountController();
        smb_ValidateAddressControllerExtension smb = new smb_ValidateAddressControllerExtension(smbAcc);

        smb_AddressValidationUtility.Address add = new smb_AddressValidationUtility.Address();
        add.addressLine1 = 'qwert';
        add.addressLine2 = 'asdfg';
        add.addressLine3 = 'zxcvb';
        add.city = 'Noida';
        add.seladdrType = 'Type';
        add.seladdrForm = 'Form';  
        add.provState = 'state';
        add.country='U.S';
        add.postalCode = '12345678';
        add.counter = 1; 
        string addCont = add.addressConcat;
        
        smb_AddressValidationUtility.ValidateAddressResponse valAdd = new smb_AddressValidationUtility.ValidateAddressResponse();
        
       valAdd = smb_ValidateAddressControllerExtension.validateAddress(add);
                
    }
}