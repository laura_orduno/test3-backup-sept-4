/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 * Name - AccountDetailController_test
 * 1. Created By - Sandip - 07 March 2016
 * 2. Modified By 
 */

@isTest(SeeAllData=true)
public class CallToE2VaultTest implements HttpCalloutMock{

    private HttpResponse resp;
    
    public CallToE2VaultTest (String testBody, Integer statusCode, String status) {
        resp = new HttpResponse();
        resp.setBody(testBody);
        resp.setStatusCode(statusCode);
        resp.setStatus(status);
    }
    
    public HTTPResponse respond(HTTPRequest req) {
        return resp;
    }
    
    public static testMethod void testWS() {
        Account acc = new Account();
        acc.Name = 'Telus';
        acc.Phone = '1-800-888-9999';
        acc.BCAN__c = '119-10016063';
        acc.CAN__c = '10016063';
        Insert acc;
        
        test.startTest();
        String testBody = '{"billArchivesResponse":{"status":{"statusCd":"200","statusTxt":"OK"},"billArchiveList":[{"locatorId":"wlsprod&#124;10016063&#124;2016/02/09&#124;20160212005504-20160209-15641-2-wls-pcs&#124;00361479000CD6B5","billDt":"2016-02-09-05:00"},' +
                          '{"locatorId":"wlsprod&#124;10016063&#124;2016/01/09&#124;20160111214558-20160109-15281-2-wls-pcs&#124;00371F87000C790E","billDt":"2016-01-09-05:00"}]}}';
        PageReference pageRef = Page.E2VaultDetailPage;
        pageRef.getParameters().put('ACCID', acc.Id);
        Test.setCurrentPage(pageRef);
        HttpCalloutMock mock = new CallToE2VaultTest(testBody,200,'OK');
        Test.setMock(HttpCalloutMock.class, mock);
        E2VaultBaseController obj = new E2VaultBaseController();
        CallToE2Vault.logError('test', 'test', 'test', 'test');
        
   //     HttpResponse res = CallToE2Vault.doCallout('https://webservices1.telus.com/rest/v2-4/cmo/selfmgmt/invoiceinfo_vs1/account/10016063/bill-archives');
         CallToE2Vault.addErrorMessage('Test');
        test.stopTest();
    }
    
    public static testMethod void testWS1() {
        Account acc = new Account();
        acc.Name = 'Telus';
        acc.Phone = '1-800-888-9999';
        acc.BCAN__c = '119-10016063';
        acc.CAN__c = '10016063';
        Insert acc;
        
        test.startTest();
        
        String testBody = '{"billArchivesResponse":{"status":{"statusCd":"200","statusTxt":"OK"},"billArchiveList":[{"locatorId":"wlsprod&#124;10016063&#124;2016/02/09&#124;20160212005504-20160209-15641-2-wls-pcs&#124;00361479000CD6B5","billDt":"2016-02-09-05:00"},' +
                          '{"locatorId":"wlsprod&#124;10016063&#124;2016/01/09&#124;20160111214558-20160109-15281-2-wls-pcs&#124;00371F87000C790E","billDt":"2016-01-09-05:00"}]}}';
        PageReference pageRef = Page.E2VaultDetailPage;
        pageRef.getParameters().put('ACCID', acc.Id);
        Test.setCurrentPage(pageRef);
        HttpCalloutMock mock = new CallToE2VaultTest(testBody,400,'Fail');
        Test.setMock(HttpCalloutMock.class, mock);
        E2VaultBaseController obj = new E2VaultBaseController();
        test.stopTest();
    }
    
    public static testMethod void testWS2() {
        Account acc = new Account();
        acc.Name = 'Telus';
        acc.Phone = '1-800-888-9999';
        acc.BCAN__c = '119-10016063';
        acc.CAN__c = '10016063';
        Insert acc;
        
        test.startTest();
        
        String testBody = '{"billArchivesResponse":{"status":{"statusCd":"200","statusTxt":"OK"},"billArchiveList":[{"locatorId":"wlsprod&#124;10016063&#124;2016/02/09&#124;20160212005504-20160209-15641-2-wls-pcs&#124;00361479000CD6B5","billDt":"2016-02-09-05:00"},' +
                          '{"locatorId":"wlsprod&#124;10016063&#124;2016/01/09&#124;20160111214558-20160109-15281-2-wls-pcs&#124;00371F87000C790E","billDt":"2016-01-09-05:00"}]}}';
        PageReference pageRef = Page.E2VaultDetailPage;
        pageRef.getParameters().put('ACCID', acc.Id);
        Test.setCurrentPage(pageRef);
        HttpCalloutMock mock = new CallToE2VaultTest(testBody,404,'Fail');
        Test.setMock(HttpCalloutMock.class, mock);
        E2VaultBaseController obj = new E2VaultBaseController();
        test.stopTest();
    }
    
   public static testMethod void testWS3() {
        Account acc = new Account();
        acc.Name = 'Telus';
        acc.Phone = '1-800-888-9999';
        acc.BCAN__c = '119-10016063';
        acc.CAN__c = '10016063';
        Insert acc;
        
        test.startTest();
        
        
        String testBody = '{"billArchivesResponse":{"status":{"statusCd":"200","statusTxt":"OK"},"billArchiveList":[{"locatorId":"wlsprod&#124;10016063&#124;2016/02/09&#124;20160212005504-20160209-15641-2-wls-pcs&#124;00361479000CD6B5","billDt":"2016-02-09-05:00"},' +
                          '{"locatorId":"wlsprod&#124;10016063&#124;2016/01/09&#124;20160111214558-20160109-15281-2-wls-pcs&#124;00371F87000C790E","billDt":"2016-01-09-05:00"}]}}';
        PageReference pageRef = Page.E2VaultDetailPage;
        pageRef.getParameters().put('ACCID', acc.Id);
        Test.setCurrentPage(pageRef);
        HttpCalloutMock mock = new CallToE2VaultTest(testBody,405,'Fail');
        Test.setMock(HttpCalloutMock.class, mock);
        E2VaultBaseController obj = new E2VaultBaseController();
       
        test.stopTest();
    } 
 /*     public static testMethod void testWS4() {
        test.startTest();
        
        
        String testBody = '{"billArchiveUrlResponse":{"status":{"statusCd":"200","statusTxt":"OK"},"url":"http://um-billpresent-west-pr.tsl.telus.com/v2/cmo/billinginquirymgmt/billdocumentretrievalservlet-v2-0/download/TELUS_OTHER_10016063_2016_02_09.pdf?document=Omi2pyy8qtfR3Zpw8lgcaWoUIjuRPrzX3/RJJomCAVOiaoKY5PPcLUszYjE81jGKnVShZ2saAJvqdCivFBQ4%2B72GY5%2Bp83fVn2hhBlrEWORkd7aauZnu/XRYXYIm611mhps09G/JHTwRizQYXQlI17EJCEki5tiTmvAd3MEINpk="}}';
                          
        PageReference pageRef = Page.E2VaultDetailPage;
        pageRef.getParameters().put('ACCID', '10016063');
        Test.setCurrentPage(pageRef);
        HttpCalloutMock mock = new CallToE2VaultTest(testBody,201,'OK');
        Test.setMock(HttpCalloutMock.class, mock);
        E2VaultBaseController obj = new E2VaultBaseController();
        test.stopTest();
    } */
    
}