@isTest
global class CreditAssessmentCalloutMock implements WebServiceMock {
    
    
    global void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction,
                         String requestName, String responseNS, String responseName, String responseType)  
    {
        if ( 'ping'.equalsIgnoreCase(requestName) ){
            response.put('response_x', ping());
        }else if('performAutoUpdateBusinessCreditProfileProxy'.equalsIgnoreCase(requestName)){
            response.put('response_x', performAutoUpdateBusinessCreditProfileProxy());
        }else if('getCreditReport'.equalsIgnoreCase(requestName)){
            response.put('response_x', getCreditReport());
        }else if('removeCreditReport'.equalsIgnoreCase(requestName)){
            response.put('response_x', removeCreditReport());
        }
        
    }
    
    
    private CreditAssessmentPing.pingResponse_element ping(){
        CreditAssessmentPing.pingResponse_element 
            response_x = new CreditAssessmentPing.pingResponse_element();
        response_x.version = 'Mock-1.0';
        return response_x;
    }
    
    
    private CreditAssessmentRequestResponse.AutoUpdateBusinessCreditProfileResult 
        performAutoUpdateBusinessCreditProfileProxy(){
            
            CreditAssessmentRequestResponse.AutoUpdateBusinessCreditProfileResult  
                response_x = new CreditAssessmentRequestResponse.AutoUpdateBusinessCreditProfileResult();
            
            Long autoUpdateTransactionId = 12345L;
            
            CreditAssessmentEnterpriseTypes.MultilingualCodeDescriptionList 
                customerMatchedTypeCd = new CreditAssessmentEnterpriseTypes.MultilingualCodeDescriptionList();
            CreditAssessmentRequestResponse.MatchedIndividualCustomer[] 
                matchedIndividualCustomerList = new List<CreditAssessmentRequestResponse.MatchedIndividualCustomer>();
            CreditAssessmentRequestResponse.MatchedIndividualCustomer 
                matchedIndividualCustomer = new CreditAssessmentRequestResponse.MatchedIndividualCustomer();
            matchedIndividualCustomerList.add(matchedIndividualCustomer);
            
            response_x.autoUpdateTransactionId = autoUpdateTransactionId;
            response_x.customerMatchedTypeCd = customerMatchedTypeCd;
            response_x.matchedIndividualCustomerList = matchedIndividualCustomerList;
            
            customerMatchedTypeCd.code = 'EXACT';
            
            return response_x;
        }
    
    
    private CreditAssessmentRequestResponse.getCreditReportResponse_element getCreditReport(){
        CreditAssessmentRequestResponse.getCreditReportResponse_element 
            response_x = new CreditAssessmentRequestResponse.getCreditReportResponse_element();
        CreditAssessmentBusinessTypes.BusinessCreditReportFullDetails 
            businessCreditReportFullDetails = new CreditAssessmentBusinessTypes.BusinessCreditReportFullDetails();
        response_x.businessCreditReportFullDetails = businessCreditReportFullDetails;
        businessCreditReportFullDetails.documentId = 'abcd1234';
        businessCreditReportFullDetails.creditReportContent = 'creditReportContent';
        return response_x;
    }
    
    
    private CreditAssessmentRequestResponse.removeCreditReportResponse_element removeCreditReport(){
        CreditAssessmentRequestResponse.removeCreditReportResponse_element
            response_x = new CreditAssessmentRequestResponse.removeCreditReportResponse_element();
        return response_x;
    }
    
    
    
}