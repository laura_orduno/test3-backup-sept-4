//Methods Included: reserveResource
// Primary Port Class Name: ReserveResourceSOAPBinding	
public class OrdrTnResrResInvOprtn {
	public class ReserveResourceSOAPBinding {
		public String endpoint_x = 'https://soa-mp-toll-pt.tsl.telus.com:443/RMO/ResourceMgmt/ReserveResource_v1_1_vs0';
		public Map<String,String> inputHttpHeaders_x;
		public Map<String,String> outputHttpHeaders_x;
		public String clientCertName_x;
		public String clientCert_x;
		public String clientCertPasswd_x;
		public Integer timeout_x;
		private String[] ns_map_type_info = new String[]{'http://www.ibm.com/telecom/common/schema/business_interaction/v3_0','TpCommonBusinessInteractionV3','http://www.ibm.com/telecom/common/schema/urban_property_address/v3_0','TpCommonUrbanPropertyAddressV3','http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/ReserveResource','OrdrTnResrResInvOprtn','http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/schema/ResourceOrderMessage','TpResourceOrderMessage','http://www.ibm.com/telecom/fulfillment/schema/resource_order/v3_0','TpFulfillmentResourceOrderV3','http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/common/schema/Message','TpCommonMessage','http://www.ibm.com/telecom/common/schema/place/v3_0','TpCommonPlaceV3','http://www.ibm.com/telecom/common/schema/base/v3_0','TpCommonBaseV3','http://www.ibm.com/telecom/common/schema/mtosi/v3_0','TpCommonMtosiV3','http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/types/schema/ReserveResource','OrdrTnResrResInvOprtnSch','http://www.ibm.com/telecom/common/schema/party/v3_0','TpCommonPartyV3'};

		public TpFulfillmentResourceOrderV3.ResourceOrder reserveResource(TpFulfillmentResourceOrderV3.ResourceOrder resourceOrder) {
			TpResourceOrderMessage.ResourceOrderMessage request_x = new TpResourceOrderMessage.ResourceOrderMessage();
			TpResourceOrderMessage.ResourceOrderMessage response_x;
			request_x.resourceOrder = resourceOrder;
			Map<String, TpResourceOrderMessage.ResourceOrderMessage> response_map_x = new Map<String, TpResourceOrderMessage.ResourceOrderMessage>();
			response_map_x.put('response_x', response_x);
			WebServiceCallout.invoke(
				this,
				request_x,
				response_map_x,
				new String[]{endpoint_x,
				'http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/ReserveResource/reserveResource',
				'http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/types/schema/ReserveResource',
				'reserveResource',
				'http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/types/schema/ReserveResource',
				'reserveResourceResponse',
				'TpResourceOrderMessage.ResourceOrderMessage'}
			);
			//OrdrTnResrResInvOprtnSch.ResourceOrderMessage
			response_x = response_map_x.get('response_x');
            if(response_x!=null){
                return response_x.resourceOrder;
            }
			return null;
		}
	}
	
}