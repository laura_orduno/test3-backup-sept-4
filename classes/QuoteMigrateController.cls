/*
    Quote Migrate Controller
    QuoteMigrateController.cls
    Part of the TELUS QuoteQuickly Portal
    
    Controller for the Quote Migrate page
    
    -- Quote Migration code can be found in QuoteMigrator.cls --
    
    Author: Alex Miller @ Traction <amiller@tractionondemand.com>
    Created: March 15, 2012
    
    Since: Release 5
    Initial Definition: R5rq12
    
    Known Issues:
     -- None Yet --
*/
public without sharing class QuoteMigrateController {
	
	private SBQQ__Quote__c quote {get; set;}
	private Map<Id, SBQQ__QuoteLine__c> quoteLines {get; set;}
	
	public String destinationBundle {get; set;}
	public SelectOption[] destinationBundleChoices {get; set;}
  public Boolean showOptions {get; set;}
  public Boolean showMigratedMessage {get; set;}
	
	
	public QuoteMigrateController() {
		String qid = ApexPages.currentPage().getParameters().get('qid');
		quote = null;
		if (qid != null) { quote = QuoteUtilities.loadQuote(qid); }
		if (quote == null || quote.Id == null) { return; }
            
    // If page messages are specified in the URL, display them
    String messagesURLEN = ApexPages.currentPage().getParameters().get('messages');
    if (messagesURLEN != null) {
        String[] messages = EncodingUtil.urlDecode(messagesURLEN, 'UTF-8').split('\\|');
        if (messages != null && messages.size() != 0) {
            for (String s : messages) {
                ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.INFO,  s) );
            }
        }
    }
            
		// Load quote lines
		quoteLines = QuoteUtilities.loadQuotelinesMap(quote.Id);
		if (quoteLines == null || quoteLines.size() == 0) { return; }
        
    // Load destination bundle choices
    SBQQ__QuoteLine__c bundleLine = QuoteUtilities.findBundleLine(quoteLines);
    if (bundleLine == null || bundleLine.Id == null) { return; }
    destinationBundleChoices = new List<SelectOption>();
    
    Quote_Migration_Route__c[] qmrs = [
    	Select Source_Product__c, Source_Product__r.Name, Destination_Product__c, Destination_Product__r.Name
    	From Quote_Migration_Route__c
    	Where Source_Product__c = :bundleLine.SBQQ__Product__c
    ];
    
    if (qmrs == null || qmrs.size() == 0) { return; } else {
    	Boolean first = true;
    	for (Quote_Migration_Route__c qmr : qmrs) {
    		if (first) { first = false; destinationBundle = qmr.Id; }
    		String qmroname = qmr.Source_Product__r.Name + ' to ' + qmr.Destination_Product__r.Name;
    		destinationBundleChoices.add( new SelectOption(qmr.Id, qmroname) );
    	}
    }
    
    showOptions = true;
    showMigratedMessage = false;
	}
	
	
	public PageReference onMigrate() {
		if (destinationBundle == null || destinationBundle == '') {
			ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.INFO,  'You must choose a bundle migration to continue.') );
			return null;
		}
		
		QuoteMigrator migrator = new QuoteMigrator();
		migrator.doMigration(quote.Id, destinationBundle);
		
		String[] messages = migrator.pagemessages;
    if (messages != null) {
        for (String s : messages) {
            ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.INFO,  s) );
        }
    }
    
    if (migrator.success) {
    	showOptions = false;
    	
    	PageReference pr = Page.SiteConfigureProducts;
	    QuotePortalUtils.addConfigurationSummaryParams(pr);
	    pr.getParameters().put('qid', quote.Id);
	    pr.getParameters().put('stop', 'trickstep');
	    pr.getParameters().put('retURL', Site.getCurrentSiteUrl() + Page.QuoteSummary.getUrl().replaceAll('/apex/','') + '?qid=' + quote.Id );
	    pr.getParameters().put('saveURL', Site.getCurrentSiteUrl() + Page.QuoteSummary.getUrl().replaceAll('/apex/','') + '?qid=' + quote.Id );
	    QuoteVO qv = new QuoteVO(quote);
	    pr.getParameters().put('pids', qv.pid);
	    
	    pr.getParameters().put('messages', migrator.messages);
	    
	    return pr;
    } else {
    	showMigratedMessage = true;
    }
		
		return ApexPages.currentPage();
	}
	
	
	public PageReference onCancel() {
		PageReference pr = Page.SiteConfigureProducts;
		
		QuotePortalUtils.addConfigurationSummaryParams(pr);
		pr.getParameters().put('qid', quote.Id);
		pr.getParameters().put('stop', 'trickstep');
    pr.getParameters().put('retURL', Site.getCurrentSiteUrl() + Page.QuoteSummary.getUrl().replaceAll('/apex/','') + '?qid=' + quote.Id );
    pr.getParameters().put('saveURL', Site.getCurrentSiteUrl() + Page.QuoteSummary.getUrl().replaceAll('/apex/','') + '?qid=' + quote.Id );
		
		QuoteVO qv = new QuoteVO(quote);
    pr.getParameters().put('pids', qv.pid);
    pr.getParameters().put('cbc', '1');
    pr.getParameters().put('cba0', 'save');
    pr.getParameters().put('cbl0', 'Switch Bundle Types');
    pr.getParameters().put('cbu0', Site.getCurrentSiteUrl() + Page.QuoteMigrate.getUrl().replaceAll('/apex/','') + '?qid=' + quote.Id );
        
		return pr;
	}
	
}