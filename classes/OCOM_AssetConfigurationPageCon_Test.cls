//OCOM_AssetConfigurationPageController
@isTest
public class OCOM_AssetConfigurationPageCon_Test {


 
   public static testmethod void testHybridCPQHeaderController() {
        Account acc = OrdrTestDataFactory.createServiceAccountWithAssets();
        
         Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        OCOM_AssetConfigurationPageController con = new OCOM_AssetConfigurationPageController(sc);
        //con.acct = acc;
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        con.calibrateServiceAccountAssets();
        Test.stopTest();
        }
}