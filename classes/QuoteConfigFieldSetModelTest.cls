@isTest
public class QuoteConfigFieldSetModelTest {
	
	private static AdditionalInformation__c addlInfo;
	private static Product2 p;
	
	static testMethod void test() {
		setUp();

		new QuoteConfigFieldSetModel(1, addlInfo, QuoteCustomConfigurationService.listQuoteConfigFieldsByProductCategory('Wireless'), 'Wireless', QuoteStatus.SENT_FOR_CREDIT_CHECK, p);
	}

 	private static void setUp() {
		Web_Account__c account = new Web_Account__c(Name='UnitTest');
		insert account;
		
		p = new Product2(Name='Test',SBQQ__ConfigurationFields__c='Service_Term__c',Network__c='CDMA',Category__c='Devices');
		insert p;
		
		Opportunity opp = new Opportunity(Name='Test',Web_Account__c=account.Id,CloseDate=System.today(),StageName='Prospecting');
		insert opp;
		
		SBQQ__Quote__c quote = new SBQQ__Quote__c(SBQQ__Opportunity__c=opp.Id);
		quote.Rate_Band__c = 'F';
		quote.Province__c = 'BC';
		insert quote;
		
		SBQQ__ProductOption__c productOption = new SBQQ__ProductOption__c(SBQQ__Number__c=1,SBQQ__OptionalSKU__c=p.Id);
		insert productOption;
		
		SBQQ__QuoteLine__c line = new SBQQ__QuoteLine__c(SBQQ__Quote__c=quote.Id,SBQQ__Bundle__c=true,SBQQ__Product__c=p.Id,Category__c='Devices',SBQQ__ProductOption__c=productOption.Id);
		insert line;
	
		addlInfo = new AdditionalInformation__c(Quote__c=quote.Id, Quote_Line__c=line.Id);
		insert addlInfo;
	}

}