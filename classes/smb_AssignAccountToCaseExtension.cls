public with sharing class smb_AssignAccountToCaseExtension {


	
	public List<Account> accountResults {get;set;}
	
	public List<Contact> contactResults {get;set;}
	
	public boolean suggestedMatch {get;set;}
	
	public Account suggestedAccount {get;set;}
	
	public Contact suggestedContact {get;set;}
	
	public Id selectedAccountId {get;set;}
	
	public Id selectedContactId {get;set;}
	
	public string selectedRecordTypeId {get;set;}
	
	public string searchText {get;set;}
	
	public boolean resultsAreSuggested {get; private set;}
	
	public Contact newContact {get;set;}
	
	public boolean showNewContactForm {get;set;}
	
	private final Case caseRecord {get;set;}
	
	private final List<String> requiredFields = new List<String> {'SuppliedEmail', 'SuppliedCompany', 'SuppliedName', 'SuppliedPhone'};

	private final Set<String> excludedDomains = new Set<String> 
		{ 'gmail.com',
		  'hotmail.com',
		  'outlook.com',
		  'yahoo.ca',
		  'yahoo.com',
		  'me.com',
		  'icloud.com',
		  'live.com',
		  'telus.com' };
		  
	    public List<SelectOption> caseRecordTypes {
        get {
            if (caseRecordTypes != null) return caseRecordTypes;
        
            // Use dynamic Apex to generate a select list of Record Ids + Name
            Map<Id,Schema.RecordTypeInfo> caseRTMapById = Schema.SObjectType.Case.getRecordTypeInfosById();

            caseRecordTypes = new List<SelectOption>();
            caseRecordTypes.add(new SelectOption('CHOOSEAGAIN', '<-- Choose Record Type for Case -->'));
                
            // Format the select list in case record type name order
            for (RecordType recType : [Select id, Name, DeveloperName, SobjectType from RecordType where SobjectType = 'Case' order by Name]) {
                // Get the schema info for the record type using the record type id
                Schema.RecordTypeInfo caseRT = caseRTMapById.get(recType.id);

                // the record type is available to the logged in user
                // Exclude the Master record type as it shouldn't be visible
                if (caseRT.isAvailable() && caseRT.getName() != 'Master') { 
                    caseRecordTypes.add(new SelectOption(caseRT.getRecordTypeId(), caseRT.getName()));
                }
            }
            return caseRecordTypes;	
        }
        private set;
    }
    
	public smb_AssignAccountToCaseExtension(ApexPages.StandardController stdController) {
		stdController.addFields(requiredFields);
		stdController.reset();
		
        this.caseRecord = (Case)stdController.getRecord();
        
        initialse();
    }
    
    public void reset() {
    	accountResults = new List<Account>();
    	contactResults = new List<Contact>();
    	this.selectedAccountId = null;
    	this.selectedContactId = null;
    	this.searchText = '';
    	initialse();
    }
    
    private void initialse() {
    	findSuggested();
    	resetContact();
    }
    
    public void showContactForm() {
    	this.showNewContactForm = true;
    }
    
    public void findAccounts() {
    	string searchClause = createSearchClause(new List<string> { searchText });
    	
    	resultsAreSuggested = false;
    	
    	List<List<SObject>> searchList = [FIND :searchClause
    									 IN ALL FIELDS 
    									 RETURNING Account(Id, Name, ParentId,
    									 		   		   RecordType.DeveloperName,
    									 		   		   RecordType.Name,
    									 				   RecordTypeId,
    									 				   CAN__c,
    									 				   RCID__c,
    									 				   CBUCID_RCID__c,
    									 				   CBUCID_Parent_Id__c,
    									 				   BTN__c,
    									 				   Phone,
    									 				   BillingStreet,
    									 				   BillingCity,
    									 				   BillingState,
    									 				   BillingPostalCode,
    									 				   BillingCountry
    									 			LIMIT 100)];
    	
    	accountResults = searchList.get(0);
    }
    
    public void findContacts() {
  
  	    resultsAreSuggested = false;
  	    
  	    List<Id> accountIds = smb_AccountUtility.getAccountsInHierarchy(this.selectedAccountId);
	    	  	
    	if (string.isNotBlank(searchText)) {
	    	string searchClause = createSearchClause(new List<string> { searchText });

	    	List<List<SObject>> searchList = [FIND :searchClause
	    									 IN ALL FIELDS 
	    									 RETURNING Contact(Id, Title, FirstName, 
	    									 				   LastName, Name, Email, Phone,
	    									 				   Active__c,
	    									 				   AccountId, Account.Name, 
	    									 				   Account.ParentId,
	    									 				   Account.RecordType.DeveloperName,
	    									 				   Account.RecordType.Name,
	    									 				   Account.RecordTypeId,
	    									 				   Account.CAN__c,
	    									 				   Account.RCID__c,
	    									 				   Account.CBUCID_RCID__c,
	    									 				   Account.CBUCID_Parent_Id__c,
	    									 				   Account.BTN__c,
	    									 				   Account.Phone,
	    									 				   Account.BillingStreet,
	    									 				   Account.BillingCity,
	    									 				   Account.BillingState,
	    									 				   Account.BillingPostalCode,
	    									 				   Account.BillingCountry
	    									 			WHERE AccountId in :accountIds
	    									 			LIMIT 100)];
	    	
	    	contactResults = searchList.get(0);
    	}
    	else {
    		contactResults = [SELECT Id, Title, FirstName, 
				 				   LastName, Name, Email, Phone,
				 				   Active__c,
				 				   AccountId, Account.Name, 
				 				   Account.ParentId,
				 				   Account.RecordType.DeveloperName,
				 				   Account.RecordType.Name,
				 				   Account.RecordTypeId,
				 				   Account.CAN__c,
				 				   Account.RCID__c,
				 				   Account.CBUCID_RCID__c,
				 				   Account.CBUCID_Parent_Id__c,
				 				   Account.BTN__c,
				 				   Account.Phone,
				 				   Account.BillingStreet,
				 				   Account.BillingCity,
				 				   Account.BillingState,
				 				   Account.BillingPostalCode,
				 				   Account.BillingCountry
				 			FROM Contact
				 			WHERE AccountId in :accountIds
				 			LIMIT 100];
    	
    	}
    }
    
    public void selectAccount() {
    	this.selectedAccountId = ApexPages.currentPage().getParameters().get('selectedAccountId');
    	this.searchText = '';
    	findContacts();
    }
    
    public PageReference selectContact() {
    	this.selectedContactId = ApexPages.currentPage().getParameters().get('selectedContactId');
    	this.searchText = '';
    	
    	if (string.isBlank(this.selectedAccountId)) {
    		this.selectedAccountId = ApexPages.currentPage().getParameters().get('selectedAccountId');
    	}
    	
		return saveCase();
    }
    
    public PageReference saveCase() {
    	if (string.isBlank(this.selectedRecordTypeId) || this.selectedRecordTypeId == 'CHOOSEAGAIN') {
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'To create a Case, first select a Case Record Type.'));
    		return null;
    	}
    	
    	Account selectedAccount = [SELECT Id, RecordType.DeveloperName FROM Account WHERE Id = :this.selectedAccountId];
    	
    	Id RcidAccount = this.selectedAccountId;
    	Id BanCanAccountId = null;
    	
    	if (selectedAccount.RecordType.DeveloperName != 'RCID') {
    		if (selectedAccount.RecordType.DeveloperName == 'BAN') {
    			BanCanAccountId = this.selectedAccountId;
    		}
    		
    		RcidAccount = smb_AccountUtility.getRcidAccountId(this.selectedAccountId);
    	}
    	
    	caseRecord.AccountId = RcidAccount;
    	caseRecord.contactId = selectedContactId;
    	caseRecord.RecordTypeId = this.selectedRecordTypeId;
    	caseRecord.Related_BAN_Account__c = BanCanAccountId;
    	
    	update caseRecord;
    	
    	ApexPages.StandardController sc = new ApexPages.StandardController(caseRecord);
    	
    	return sc.view();
    }
    
    public void resetContact() {
    	this.newContact = new Contact();
    	this.showNewContactForm = false;
    }
    
    public void createNewContact() {
    	
    	this.newContact.AccountId = smb_AccountUtility.getRcidAccountId(this.selectedAccountId);
    	
    	insert this.newContact;
    	
    	contactResults.addAll(
			[SELECT Id, Title, FirstName, 
 				   LastName, Name, Email, Phone,
 				   Active__c,
 				   AccountId, Account.Name, 
 				   Account.ParentId,
 				   Account.RecordType.DeveloperName,
 				   Account.RecordType.Name,
 				   Account.RecordTypeId,
 				   Account.CAN__c,
 				   Account.RCID__c,
 				   Account.CBUCID_RCID__c,
 				   Account.CBUCID_Parent_Id__c,
 				   Account.BTN__c,
 				   Account.Phone,
 				   Account.BillingStreet,
 				   Account.BillingCity,
 				   Account.BillingState,
 				   Account.BillingPostalCode,
 				   Account.BillingCountry
 			FROM Contact
 			WHERE Id = :this.newContact.Id]
		);
    	
    	resetContact();
    }
    
    private void findSuggested() {
    	
    	resultsAreSuggested = true;
    	
    	Map<Id, Account> accounts = findSuggestedAccounts();
    	
    	List<Contact> contacts = findSuggestedContacts();	
    	
    	if (accounts.size() == 0 && contacts.size() == 0) {
    		contacts = findContactsFromDomain();
    	}   
    	
    	for (Contact contact : contacts) {
    		if (accounts.containsKey(contact.accountId)) {
    			continue;
    		}
    		
    		accounts.put(contact.accountId, contact.Account);
    	}
    	
    	this.accountResults = accounts.values();
    	this.contactResults = contacts;
   	
    }
    
    private Map<Id, Account> findSuggestedAccounts() {
    	
    	Map<Id, Account> results = new Map<Id, Account>();
    	
    	if (string.isNotBlank(caseRecord.SuppliedCompany)) {
    		List<string> searchTerms = new List<String> {caseRecord.SuppliedCompany, caseRecord.SuppliedPhone};
    	
	    	string searchClause = createSearchClause(searchTerms);		
	    	
	    	List<List<SObject>> searchList = [FIND :searchClause
    									 IN NAME FIELDS 
    									 RETURNING Account(Id, Name, ParentId,
    									 		   		   RecordType.DeveloperName,
    									 		   		   RecordType.Name,
    									 				   RecordTypeId,
    									 				   CAN__c,
    									 				   RCID__c,
    									 				   CBUCID_RCID__c,
    									 				   CBUCID_Parent_Id__c,
    									 				   BTN__c,
    									 				   Phone,
    									 				   BillingStreet,
    									 				   BillingCity,
    									 				   BillingState,
    									 				   BillingPostalCode,
    									 				   BillingCountry)];
    		
    		for (sObject item : searchList.get(0)) {
    			results.put(item.Id, (Account)item);
    		}
    	}
    
    	if (string.isNotBlank(caseRecord.SuppliedPhone)) {
    		
    		List<string> searchTerms = new List<String> {caseRecord.SuppliedPhone};
    	
	    	string searchClause = createSearchClause(searchTerms);		
	    	
	    	List<List<SObject>> searchList = [FIND :searchClause
	    									 IN PHONE FIELDS 
	    									 RETURNING Asset(Id, AccountId, Account.Name, 
    									 				   Account.ParentId,
    									 				   Account.RecordType.DeveloperName,
    									 				   Account.RecordType.Name,
    									 				   Account.RecordTypeId,
    									 				   Account.CAN__c,
    									 				   Account.RCID__c,
    									 				   Account.CBUCID_RCID__c,
    									 				   Account.CBUCID_Parent_Id__c,
    									 				   Account.BTN__c,
    									 				   Account.Phone,
    									 				   Account.BillingStreet,
    									 				   Account.BillingCity,
    									 				   Account.BillingState,
    									 				   Account.BillingPostalCode,
    									 				   Account.BillingCountry)];
    									 				   
    		for (sObject item : searchList.get(0)) {
    			Asset asset = (Asset)item;
    			
    			if (asset.AccountId == null) continue;
    			
    			if (!results.containsKey(asset.AccountId)) {
    				results.put(asset.AccountId, asset.Account);
    			}
    		}
    		
    	}
    	
    	return results;	 				   
   	
    }
    
    private List<Contact> findSuggestedContacts() {
    	//find contacts by name, email, domain 
    	if (string.isBlank(caseRecord.SuppliedEmail) &&
    		string.isBlank(caseRecord.SuppliedName) &&
    		string.isBlank(caseRecord.SuppliedPhone)) {
    	
    		return new List<Contact>();
    			
    	}
    	
    	Map<Id, Contact> results = new Map<Id, Contact>();
    	
    	List<string> searchTerms = new List<String> {caseRecord.SuppliedEmail, caseRecord.SuppliedName, caseRecord.SuppliedPhone};
    	
    	string searchClause = createSearchClause(searchTerms);
    	
    	List<List<SObject>> searchList = [FIND :searchClause
    									 IN ALL FIELDS 
    									 RETURNING Contact(Id, Title, FirstName, 
    									 				   LastName, Name, Email, Phone,
    									 				   Active__c,
    									 				   AccountId, Account.Name, 
    									 				   Account.ParentId,
    									 				   Account.RecordType.DeveloperName,
    									 				   Account.RecordType.Name,
    									 				   Account.RecordTypeId,
    									 				   Account.CAN__c,
    									 				   Account.RCID__c,
    									 				   Account.CBUCID_RCID__c,
    									 				   Account.CBUCID_Parent_Id__c,
    									 				   Account.BTN__c,
    									 				   Account.Phone,
    									 				   Account.BillingStreet,
    									 				   Account.BillingCity,
    									 				   Account.BillingState,
    									 				   Account.BillingPostalCode,
    									 				   Account.BillingCountry)];
    	for (sObject item : searchList.get(0)) {
    		results.put(item.Id, (Contact)item);
    	}
    	
    	if (results.size() == 0) {
    		searchList = [FIND :searchClause
    									 IN ALL FIELDS 
    									 RETURNING Case(Id, CaseNumber, SuppliedEmail, 
    									 		   		SuppliedCompany, SuppliedName,
    									 		   		SuppliedPhone,
    									 				   ContactId,
    									 				   Contact.Title,
    									 				   Contact.FirstName, 
    									 				   Contact.LastName, 
    									 				   Contact.Name, 
    									 				   Contact.Email, 
    									 				   Contact.Phone,
    									 				   Contact.Active__c,
    									 				   Contact.AccountId, 
    									 				   Contact.Account.Name, 
    									 				   Contact.Account.ParentId,
    									 				   Contact.Account.RecordType.DeveloperName,
    									 				   Contact.Account.RecordType.Name,
    									 				   Contact.Account.RecordTypeId,
    									 				   Contact.Account.CAN__c,
    									 				   Contact.Account.RCID__c,
    									 				   Contact.Account.CBUCID_RCID__c,
    									 				   Contact.Account.CBUCID_Parent_Id__c,
    									 				   Contact.Account.BTN__c,
    									 				   Contact.Account.Phone,
    									 				   Contact.Account.BillingStreet,
    									 				   Contact.Account.BillingCity,
    									 				   Contact.Account.BillingState,
    									 				   Contact.Account.BillingPostalCode,
    									 				   Contact.Account.BillingCountry
    									 			  WHERE Id != :CaseRecord.Id
    									 			  			AND
    									 			  		(AccountId != null
    									 			  			OR
    									 			  		 ContactId != null))];
        
        
        
	    	for (sObject item : searchList.get(0)) {
	    		Case caseInstance = (Case)item;
	    		
	    		if (caseInstance.Contact == null) continue;
	    		
	    		if (caseInstance.Contact.Account == null) continue;
	    		
	    		if (!results.containsKey(caseInstance.ContactId)) {
	    			results.put(caseInstance.ContactId, caseInstance.Contact);
	    		}
	    		
	    	}
    	}
    	
    	return results.values();
    	
    }
    
    private List<Contact> findContactsFromDomain() {
    	if (string.isBlank(caseRecord.SuppliedEmail))
    		return new List<Contact>();
    		
    	string domainSearchString = getDomainFromEmail(caseRecord.SuppliedEmail);
    	
		if (string.isBlank(domainSearchString) || 
			excludedDomains.contains(domainSearchString.ToLowerCase())) {
			return new List<Contact>();
    	}
    	
    	Map<Id, Contact> results = new Map<Id, Contact>();
    	
    	string searchClause = createSearchClause(new List<string> {domainSearchString});
    	
    	List<List<SObject>> searchList = [FIND :searchClause
    									 IN EMAIL FIELDS 
    									 RETURNING Contact(Id, Title, FirstName, 
    									 				   LastName, Name, Email, Phone,
    									 				   Active__c,
    									 				   AccountId, Account.Name, 
    									 				   Account.ParentId,
    									 				   Account.RecordType.DeveloperName,
    									 				   Account.RecordType.Name,
    									 				   Account.RecordTypeId,
    									 				   Account.CAN__c,
    									 				   Account.RCID__c,
    									 				   Account.CBUCID_RCID__c,
    									 				   Account.CBUCID_Parent_Id__c,
    									 				   Account.BTN__c,
    									 				   Account.Phone,
    									 				   Account.BillingStreet,
    									 				   Account.BillingCity,
    									 				   Account.BillingState,
    									 				   Account.BillingPostalCode,
    									 				   Account.BillingCountry),
    									 		   Case(Id, CaseNumber, SuppliedEmail, 
    									 		   		SuppliedCompany, SuppliedName,
    									 		   		SuppliedPhone,
    									 		   		ContactId,
									 				   Contact.Title,
									 				   Contact.FirstName, 
									 				   Contact.LastName, 
									 				   Contact.Name, 
									 				   Contact.Email, 
									 				   Contact.Phone,
									 				   Contact.Active__c,
									 				   Contact.AccountId, 
									 				   Contact.Account.Name, 
									 				   Contact.Account.ParentId,
									 				   Contact.Account.RecordType.DeveloperName,
									 				   Contact.Account.RecordType.Name,
									 				   Contact.Account.RecordTypeId,
									 				   Contact.Account.CAN__c,
									 				   Contact.Account.RCID__c,
									 				   Contact.Account.CBUCID_RCID__c,
									 				   Contact.Account.CBUCID_Parent_Id__c,
									 				   Contact.Account.BTN__c,
									 				   Contact.Account.Phone,
									 				   Contact.Account.BillingStreet,
									 				   Contact.Account.BillingCity,
									 				   Contact.Account.BillingState,
									 				   Contact.Account.BillingPostalCode,
									 				   Contact.Account.BillingCountry
    									 			  WHERE Id != :CaseRecord.Id
    									 			  			AND
    									 			  		(AccountId != null
    									 			  			OR
    									 			  		 ContactId != null))];
        for (sObject item : searchList.get(0)) {
    		results.put(item.Id, (Contact)item);
    	}
        
        for (sObject item : searchList.get(1)) {
    		Case caseInstance = (Case)item;
    		
    		if (caseInstance.Contact == null) continue;
    		
    		if (caseInstance.Contact.Account == null) continue;
    		
    		if (!results.containsKey(caseInstance.ContactId)) {
    			results.put(caseInstance.ContactId, caseInstance.Contact);
    		}
    		
    	}
    	
    	return results.values();
       
    }
    
    private static string createSearchClause(List<string> searchTerms) {

        String searchClause = '';

        for (string searchTerm : searchTerms) {
            if (string.isNotBlank(searchTerm)) {
                
                string searchClause1 = searchTerm;
                string searchClause2 = searchTerm.replaceAll('[^\\w ]', '');
                
                searchClause1 = '"' + searchClause1 + '"';
                searchClause2 = '"' + searchClause2 + '"';
    
                String combinedSearchClause = string.escapeSingleQuotes(searchClause1);
                
                if (searchClause1 != searchClause2) {
                    combinedSearchClause = '(' + combinedSearchClause + ' OR ' + searchClause2 + ')';
                }
                
                if (string.isNotBlank(searchClause)) {
                    searchClause += ' OR ';
                }
                
                searchClause += combinedSearchClause;
            }
           
        }
        
        return searchClause;
    }
    
    private string getDomainFromEmail(string emailAddress) {
    	
    	if (string.isBlank(emailAddress)) return emailAddress;
    	
    	if (!emailAddress.contains('@')) return emailAddress;
    	
    	string[] parts = emailAddress.split('@', 2);
    	
    	return parts[1];
    }
}