/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
* 
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
 * The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
*/
@isTest
private class smb_ServiceRequestHelperTest {

    static testMethod void insertEnhanceTrackingRecordTest() {
      Opportunity myOpp = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
      insert myOpp;
        Order_Request__c orRqst = new Order_Request__c(Opportunity__c=myOpp.id,CurrencyIsoCode='CAD', Originating_Agent__c=Userinfo.getUserId());
        insert orRqst;
        
        system.debug('----->>>>' + orRqst);
        
        list<Service_Request__c> lstSerRequests = new list<Service_Request__c>();
/*        lstSerRequests.add(new Service_Request__c(Opportunity__c=myOpp.Id, Service_Address__c='Dummy Service Address', Scheduled_Due_Date__c=Datetime.now(),Order_Label__c='test Order', External_Order_ID__c='ExtTestId1', Customer_Order_ID__c='CustTestId1', CurrencyIsoCode='CAD', Status__c='Order Entry is Completed', Product_ID__c='Test1', Order_Request_Lookup__c=orRqst.Id)); */
        lstSerRequests.add(new Service_Request__c(Opportunity__c=myOpp.Id, Service_Address__c='Dummy Service Address', Scheduled_Due_Date__c=Datetime.now(),Order_Label__c='test Order', External_Order_ID__c='ExtTestId2', Customer_Order_ID__c='CustTestId2', CurrencyIsoCode='CAD', Status__c='In Progress', Product_ID__c='Test2', Order_Request_Lookup__c=orRqst.Id));
        lstSerRequests.add(new Service_Request__c(Opportunity__c=myOpp.Id, Service_Address__c='Dummy Service Address', Scheduled_Due_Date__c=Datetime.now(),Order_Label__c='test Order', External_Order_ID__c='ExtTestId2', Customer_Order_ID__c='CustTestId2', CurrencyIsoCode='CAD', Status__c='Delivery', Product_ID__c='Test3', Order_Request_Lookup__c=orRqst.Id));
/*        lstSerRequests.add(new Service_Request__c(Opportunity__c=myOpp.Id, Service_Address__c='Dummy Service Address', Scheduled_Due_Date__c=Datetime.now(),Order_Label__c='test Order', External_Order_ID__c='ExtTestId2', Customer_Order_ID__c='CustTestId2', CurrencyIsoCode='CAD', Status__c='Critical Fulfillment Error', Product_ID__c='Test4', Order_Request_Lookup__c=orRqst.Id));
        lstSerRequests.add(new Service_Request__c(Opportunity__c=myOpp.Id, Service_Address__c='Dummy Service Address', Scheduled_Due_Date__c=Datetime.now(),Order_Label__c='test Order', External_Order_ID__c='ExtTestId2', Customer_Order_ID__c='CustTestId2', CurrencyIsoCode='CAD', Status__c='Order Details Missing', Product_ID__c='Test5', Order_Request_Lookup__c=orRqst.Id));
        lstSerRequests.add(new Service_Request__c(Opportunity__c=myOpp.Id, Service_Address__c='Dummy Service Address', Scheduled_Due_Date__c=Datetime.now(),Order_Label__c='test Order', External_Order_ID__c='ExtTestId2', Customer_Order_ID__c='CustTestId2', CurrencyIsoCode='CAD', Status__c='BO Hold', Product_ID__c='Test6', Order_Request_Lookup__c=orRqst.Id));
        lstSerRequests.add(new Service_Request__c(Opportunity__c=myOpp.Id, Service_Address__c='Dummy Service Address', Scheduled_Due_Date__c=Datetime.now(),Order_Label__c='test Order', External_Order_ID__c='ExtTestId2', Customer_Order_ID__c='CustTestId2', CurrencyIsoCode='CAD', Status__c='Suspended', Product_ID__c='Test7', Order_Request_Lookup__c=orRqst.Id)); */
        
        lstSerRequests.add(new Service_Request__c(Opportunity__c=myOpp.Id, Service_Address__c='Dummy Service Address', Scheduled_Due_Date__c=Datetime.now(),Order_Label__c='test Order', External_Order_ID__c='ExtTestId2', Customer_Order_ID__c='CustTestId2', CurrencyIsoCode='CAD', Status__c='Non-Doable', Product_ID__c='Test8', Order_Request_Lookup__c=orRqst.Id));
        insert lstSerRequests;
        
        lstSerRequests[0].Service_Address__c = 'Dummy Service Address 2';
        update lstSerRequests;  
      
      list<Contact> lstContact = new list<Contact>();
      lstContact.add(smb_test_utility.createContact(null,null,false));
      lstContact.add(smb_test_utility.createContact(null,null,false));
    
      insert lstContact;
    
      Service_Request_Contact__c SRC = new Service_Request_Contact__c(Contact__c =lstContact[0].id, Service_Request__c = lstSerRequests[0].id,PrimaryContact__c=true );
      insert SRC;
      
      Service_Request_Contact__c SRC2 = new Service_Request_Contact__c(Contact__c =lstContact[1].id, Service_Request__c = lstSerRequests[0].id,PrimaryContact__c=false );
      insert SRC2;
      
      SRC2.PrimaryContact__c = true;
      update SRC2;
    }
    
    static testMethod void testTwo() {
      Opportunity myOpp = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
      insert myOpp;
        Order_Request__c orRqst = new Order_Request__c(Opportunity__c=myOpp.id,CurrencyIsoCode='CAD', Originating_Agent__c=Userinfo.getUserId());
        insert orRqst;
        
        system.debug('----->>>>' + orRqst);
        
        list<Service_Request__c> lstSerRequests = new list<Service_Request__c>();

		Id devRecordTypeId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Legacy').getRecordTypeId();
        lstSerRequests.add(new Service_Request__c(Opportunity__c=myOpp.Id, RecordTypeId = devRecordTypeId, Service_Address__c='Dummy Service Address', Scheduled_Due_Date__c=Datetime.now(),Order_Label__c='test Order', External_Order_ID__c='ExtTestId2', Customer_Order_ID__c='CustTestId2', CurrencyIsoCode='CAD', Status__c='Need Additional Information', Product_ID__c='Test8', Order_Request_Lookup__c=orRqst.Id));
        lstSerRequests.add(new Service_Request__c(Opportunity__c=myOpp.Id, RecordTypeId = devRecordTypeId, Service_Address__c='Dummy Service Address', Scheduled_Due_Date__c=Datetime.now(),Order_Label__c='test Order', External_Order_ID__c='ExtTestId2', Customer_Order_ID__c='CustTestId2', CurrencyIsoCode='CAD', Status__c='In Progress', Product_ID__c='Test8', Order_Request_Lookup__c=orRqst.Id));
        lstSerRequests.add(new Service_Request__c(Opportunity__c=myOpp.Id, RecordTypeId = devRecordTypeId, Service_Address__c='Dummy Service Address', Scheduled_Due_Date__c=Datetime.now(),Order_Label__c='test Order', External_Order_ID__c='ExtTestId2', Customer_Order_ID__c='CustTestId2', CurrencyIsoCode='CAD', Status__c='Delivery', Product_ID__c='Test8', Order_Request_Lookup__c=orRqst.Id));
        lstSerRequests.add(new Service_Request__c(Opportunity__c=myOpp.Id, RecordTypeId = devRecordTypeId, Service_Address__c='Dummy Service Address', Scheduled_Due_Date__c=Datetime.now(),Order_Label__c='test Order', External_Order_ID__c='ExtTestId2', Customer_Order_ID__c='CustTestId2', CurrencyIsoCode='CAD', Status__c='On Hold (Customer)', Product_ID__c='Test8', Order_Request_Lookup__c=orRqst.Id));
        lstSerRequests.add(new Service_Request__c(Opportunity__c=myOpp.Id, RecordTypeId = devRecordTypeId, Service_Address__c='Dummy Service Address', Scheduled_Due_Date__c=Datetime.now(),Order_Label__c='test Order', External_Order_ID__c='ExtTestId2', Customer_Order_ID__c='CustTestId2', CurrencyIsoCode='CAD', Status__c='On Hold (TELUS)', Product_ID__c='Test8', Order_Request_Lookup__c=orRqst.Id));
        lstSerRequests.add(new Service_Request__c(Opportunity__c=myOpp.Id, RecordTypeId = devRecordTypeId, Service_Address__c='Dummy Service Address', Scheduled_Due_Date__c=Datetime.now(),Order_Label__c='test Order', External_Order_ID__c='ExtTestId2', Customer_Order_ID__c='CustTestId2', CurrencyIsoCode='CAD', Status__c='Completed', Product_ID__c='Test8', Order_Request_Lookup__c=orRqst.Id));
        lstSerRequests.add(new Service_Request__c(Opportunity__c=myOpp.Id, RecordTypeId = devRecordTypeId, Service_Address__c='Dummy Service Address', Scheduled_Due_Date__c=Datetime.now(),Order_Label__c='test Order', External_Order_ID__c='ExtTestId2', Customer_Order_ID__c='CustTestId2', CurrencyIsoCode='CAD', Status__c='New', Product_ID__c='Test8', Order_Request_Lookup__c=orRqst.Id));
        lstSerRequests.add(new Service_Request__c(Opportunity__c=myOpp.Id, RecordTypeId = devRecordTypeId, Service_Address__c='Dummy Service Address', Scheduled_Due_Date__c=Datetime.now(),Order_Label__c='test Order', External_Order_ID__c='ExtTestId2', Customer_Order_ID__c='CustTestId2', CurrencyIsoCode='CAD', Status__c='Non-Doable', Product_ID__c='Test8', Order_Request_Lookup__c=orRqst.Id));

    	insert lstSerRequests;
        
        lstSerRequests[0].Service_Address__c = 'Dummy Service Address 2';
        update lstSerRequests;  
      
      list<Contact> lstContact = new list<Contact>();
      lstContact.add(smb_test_utility.createContact(null,null,false));
      lstContact.add(smb_test_utility.createContact(null,null,false));
    
      insert lstContact;
    
      Service_Request_Contact__c SRC = new Service_Request_Contact__c(Contact__c =lstContact[0].id, Service_Request__c = lstSerRequests[0].id,PrimaryContact__c=true );
      insert SRC;
      
      Service_Request_Contact__c SRC2 = new Service_Request_Contact__c(Contact__c =lstContact[1].id, Service_Request__c = lstSerRequests[0].id,PrimaryContact__c=false );
      insert SRC2;
      
      SRC2.PrimaryContact__c = true;
      update SRC2;
    }

    static testMethod void testThree() {
      Opportunity myOpp = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
      insert myOpp;
        Order_Request__c orRqst = new Order_Request__c(Opportunity__c=myOpp.id,CurrencyIsoCode='CAD', Originating_Agent__c=Userinfo.getUserId());
        insert orRqst;
        
        system.debug('----->>>>' + orRqst);
        
        list<Service_Request__c> lstSerRequests = new list<Service_Request__c>();

		Id devRecordTypeId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Legacy').getRecordTypeId();
        lstSerRequests.add(new Service_Request__c(Opportunity__c=myOpp.Id, RecordTypeId = devRecordTypeId, Service_Address__c='Dummy Service Address', Scheduled_Due_Date__c=Datetime.now(),Order_Label__c='test Order', External_Order_ID__c='ExtTestId2', Customer_Order_ID__c='CustTestId2', CurrencyIsoCode='CAD', Status__c='In Progress', Product_ID__c='Test8', Order_Request_Lookup__c=orRqst.Id));

    	insert lstSerRequests;
        
        lstSerRequests[0].Service_Address__c = 'Dummy Service Address 2';
        update lstSerRequests;  
      
      list<Contact> lstContact = new list<Contact>();
      lstContact.add(smb_test_utility.createContact(null,null,false));
      lstContact.add(smb_test_utility.createContact(null,null,false));
    
      insert lstContact;
    
      Service_Request_Contact__c SRC = new Service_Request_Contact__c(Contact__c =lstContact[0].id, Service_Request__c = lstSerRequests[0].id,PrimaryContact__c=true );
      insert SRC;
      
      Service_Request_Contact__c SRC2 = new Service_Request_Contact__c(Contact__c =lstContact[1].id, Service_Request__c = lstSerRequests[0].id,PrimaryContact__c=false );
      insert SRC2;
      
      SRC2.PrimaryContact__c = true;
      update SRC2;
    }
    
    static testMethod void testFour() {
      Opportunity myOpp = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
      insert myOpp;
        Order_Request__c orRqst = new Order_Request__c(Opportunity__c=myOpp.id,CurrencyIsoCode='CAD', Originating_Agent__c=Userinfo.getUserId());
        insert orRqst;
        
        system.debug('----->>>>' + orRqst);
        
        list<Service_Request__c> lstSerRequests = new list<Service_Request__c>();

		Id devRecordTypeId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Legacy').getRecordTypeId();
        lstSerRequests.add(new Service_Request__c(Opportunity__c=myOpp.Id, RecordTypeId = devRecordTypeId, Service_Address__c='Dummy Service Address', Scheduled_Due_Date__c=Datetime.now(),Order_Label__c='test Order', External_Order_ID__c='ExtTestId2', Customer_Order_ID__c='CustTestId2', CurrencyIsoCode='CAD', Status__c='Delivery', Product_ID__c='Test8', Order_Request_Lookup__c=orRqst.Id));

    	insert lstSerRequests;
        
        lstSerRequests[0].Service_Address__c = 'Dummy Service Address 2';
        update lstSerRequests;  
      
      list<Contact> lstContact = new list<Contact>();
      lstContact.add(smb_test_utility.createContact(null,null,false));
      lstContact.add(smb_test_utility.createContact(null,null,false));
    
      insert lstContact;
    
      Service_Request_Contact__c SRC = new Service_Request_Contact__c(Contact__c =lstContact[0].id, Service_Request__c = lstSerRequests[0].id,PrimaryContact__c=true );
      insert SRC;
      
      Service_Request_Contact__c SRC2 = new Service_Request_Contact__c(Contact__c =lstContact[1].id, Service_Request__c = lstSerRequests[0].id,PrimaryContact__c=false );
      insert SRC2;
      
      SRC2.PrimaryContact__c = true;
      update SRC2;
    }
    
    static testMethod void testFive() {
      Opportunity myOpp = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
      insert myOpp;
        Order_Request__c orRqst = new Order_Request__c(Opportunity__c=myOpp.id,CurrencyIsoCode='CAD', Originating_Agent__c=Userinfo.getUserId());
        insert orRqst;
        
        system.debug('----->>>>' + orRqst);
        
        list<Service_Request__c> lstSerRequests = new list<Service_Request__c>();

		Id devRecordTypeId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Legacy').getRecordTypeId();
        lstSerRequests.add(new Service_Request__c(Opportunity__c=myOpp.Id, RecordTypeId = devRecordTypeId, Service_Address__c='Dummy Service Address', Scheduled_Due_Date__c=Datetime.now(),Order_Label__c='test Order', External_Order_ID__c='ExtTestId2', Customer_Order_ID__c='CustTestId2', CurrencyIsoCode='CAD', Status__c='Need Additional Information', Product_ID__c='Test8', Order_Request_Lookup__c=orRqst.Id));

    	insert lstSerRequests;
        
        lstSerRequests[0].Service_Address__c = 'Dummy Service Address 2';
        update lstSerRequests;  
      
      list<Contact> lstContact = new list<Contact>();
      lstContact.add(smb_test_utility.createContact(null,null,false));
      lstContact.add(smb_test_utility.createContact(null,null,false));
    
      insert lstContact;
    
      Service_Request_Contact__c SRC = new Service_Request_Contact__c(Contact__c =lstContact[0].id, Service_Request__c = lstSerRequests[0].id,PrimaryContact__c=true );
      insert SRC;
      
      Service_Request_Contact__c SRC2 = new Service_Request_Contact__c(Contact__c =lstContact[1].id, Service_Request__c = lstSerRequests[0].id,PrimaryContact__c=false );
      insert SRC2;
      
      SRC2.PrimaryContact__c = true;
      update SRC2;
    }    
   
    static testMethod void testSix() {
      Opportunity myOpp = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
      insert myOpp;
        Order_Request__c orRqst = new Order_Request__c(Opportunity__c=myOpp.id,CurrencyIsoCode='CAD', Originating_Agent__c=Userinfo.getUserId());
        insert orRqst;
        
        system.debug('----->>>>' + orRqst);
        
        list<Service_Request__c> lstSerRequests = new list<Service_Request__c>();

		Id devRecordTypeId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Legacy').getRecordTypeId();
     	lstSerRequests.add(new Service_Request__c(Opportunity__c=myOpp.Id, RecordTypeId = devRecordTypeId, Service_Address__c='Dummy Service Address', Scheduled_Due_Date__c=Datetime.now(),Order_Label__c='test Order', External_Order_ID__c='ExtTestId2', Customer_Order_ID__c='CustTestId2', CurrencyIsoCode='CAD', Status__c='On Hold (TELUS)', Product_ID__c='Test8', Order_Request_Lookup__c=orRqst.Id));
   
    	insert lstSerRequests;
        
        lstSerRequests[0].Service_Address__c = 'Dummy Service Address 2';
        update lstSerRequests;  
      
      list<Contact> lstContact = new list<Contact>();
      lstContact.add(smb_test_utility.createContact(null,null,false));
      lstContact.add(smb_test_utility.createContact(null,null,false));
    
      insert lstContact;
    
      Service_Request_Contact__c SRC = new Service_Request_Contact__c(Contact__c =lstContact[0].id, Service_Request__c = lstSerRequests[0].id,PrimaryContact__c=true );
      insert SRC;
      
      Service_Request_Contact__c SRC2 = new Service_Request_Contact__c(Contact__c =lstContact[1].id, Service_Request__c = lstSerRequests[0].id,PrimaryContact__c=false );
      insert SRC2;
      
      SRC2.PrimaryContact__c = true;
      update SRC2;
    }    
    
    static testMethod void testSeven(){
      Opportunity myOpp = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
      insert myOpp;
        Order_Request__c orRqst = new Order_Request__c(Opportunity__c=myOpp.id,CurrencyIsoCode='CAD', Originating_Agent__c=Userinfo.getUserId());
        insert orRqst;
        
        system.debug('----->>>>' + orRqst);
        
        list<Service_Request__c> lstSerRequests = new list<Service_Request__c>();

		Id devRecordTypeId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Legacy').getRecordTypeId();
  		lstSerRequests.add(new Service_Request__c(Opportunity__c=myOpp.Id, RecordTypeId = devRecordTypeId, Service_Address__c='Dummy Service Address', Scheduled_Due_Date__c=Datetime.now(),Order_Label__c='test Order', External_Order_ID__c='ExtTestId2', Customer_Order_ID__c='CustTestId2', CurrencyIsoCode='CAD', Status__c='On Hold (Customer)', Product_ID__c='Test8', Order_Request_Lookup__c=orRqst.Id));
        
    	insert lstSerRequests;
        
        lstSerRequests[0].Service_Address__c = 'Dummy Service Address 2';
        update lstSerRequests;  
      
      list<Contact> lstContact = new list<Contact>();
      lstContact.add(smb_test_utility.createContact(null,null,false));
      lstContact.add(smb_test_utility.createContact(null,null,false));
    
      insert lstContact;
    
      Service_Request_Contact__c SRC = new Service_Request_Contact__c(Contact__c =lstContact[0].id, Service_Request__c = lstSerRequests[0].id,PrimaryContact__c=true );
      insert SRC;
      
      Service_Request_Contact__c SRC2 = new Service_Request_Contact__c(Contact__c =lstContact[1].id, Service_Request__c = lstSerRequests[0].id,PrimaryContact__c=false );
      insert SRC2;
      
      SRC2.PrimaryContact__c = true;
      update SRC2;
    }    
}