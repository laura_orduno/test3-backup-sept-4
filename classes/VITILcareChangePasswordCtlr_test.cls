@isTest(SeeAllData=false)
public class VITILcareChangePasswordCtlr_test {
    
       
    /* VITILcare portal */
    @isTest static void VITILcare_testWeakPassword() {
        User u = MBRTestUtils.createPortalUser('VITILcare');
        VITILcareChangePasswordCtlr testController = new VITILcareChangePasswordCtlr();

        System.runAs(u) {
            testController.oldPassword = null;
            testController.newPassword = 'pass';
            testController.verifyNewPassword = testController.newPassword;
            testController.changePassword();
        }

        System.assertEquals(label.MBRWeakPasswordError, testController.weakPassword);
    }

    @isTest static void VITILcare_testPasswordMismatch() {
        User u = MBRTestUtils.createPortalUser('VITILcare');
        VITILcareChangePasswordCtlr testController = new VITILcareChangePasswordCtlr();

        System.runAs(u) {
            testController.oldPassword = null;
            testController.newPassword = 'ASDFasdf!@#$1234';
            //testController.verifyNewPassword = testController.newPassword;
            testController.verifyNewPassword = 'QWERqwer&*()7890';
            testController.changePassword();
        }

        System.assertEquals(label.mbrPasswordDoNotMatchError, testController.passwordNotMatch);
    }

    @isTest static void VITILcare_testOldPasswordNullNewPasswordGood() {
        User u = MBRTestUtils.createPortalUser('VITILcare');
        VITILcareChangePasswordCtlr testController = new VITILcareChangePasswordCtlr();

        System.runAs(u) {
            testController.oldPassword = null;
            testController.newPassword = 'ASDFasdf!@#$1234';
            testController.verifyNewPassword = testController.newPassword;
            testController.changePassword();
        }

        Boolean noErrors = testController.errors == null || testController.errors.isEmpty();
        System.assert(noErrors, 'Null old password / Strong new password test has failed');
    }

    @isTest static void VITILcare_testOldPasswordGoodNewPasswordGood() {
        User u = MBRTestUtils.createPortalUser('VITILcare');
        VITILcareChangePasswordCtlr testController = new VITILcareChangePasswordCtlr();

        System.runAs(u) {
            // manually change the current empty password to testController.oldPassword
            testController.oldPassword = 'QWERqwer&*()7890';
            Site.changePassword(testController.oldPassword, testController.oldPassword);
            // now set testController.newPassword to a strong, different password
            testController.newPassword = 'ASDFasdf!@#$1234';
            testController.verifyNewPassword = testController.newPassword;
            testController.changePassword();
        }

        Boolean noErrors = testController.errors == null || testController.errors.isEmpty();
        System.assert(noErrors, 'Strong old password / strong new password test has failed');
    }

    @isTest
    public static void VITILcare_testRandomErrorMessage()
    {
        User u = MBRTestUtils.createPortalUser('VITILcare');
        VITILcareChangePasswordCtlr testController = new VITILcareChangePasswordCtlr();

        system.runAs(u) {
            PageReference pageRef = Page.VITILcareChangePassword;
            Test.setCurrentPage(pageRef);

            // manually change the current empty password to testController.oldPassword
            testController.oldPassword = 'QWERqwer&*()7890';
            Site.changePassword(testController.oldPassword, testController.oldPassword);
            // now set testController.newPassword to a strong, different password
            testController.newPassword = 'ASDFasdf!@#$1234';
            testController.verifyNewPassword = testController.newPassword;

            String randomText = 'This is an additional exception!';
            ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, randomText ) );

            testController.changePassword();
            System.assertEquals( randomText, testController.weakPassword );
        }    
    }

}