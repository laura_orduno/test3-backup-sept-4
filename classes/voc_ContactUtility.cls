public without sharing class voc_ContactUtility {
    
    //returns AccountIdsMappedToRcidAccountId
    public static Map<Id,Id> getAccountToRcidAccount(List<Contact> contacts) {
        
        List<Id> initialAccountIds = new List<Id>();
        List<Id> parentIds = new List<Id>();

        Map<Id,Account> allAccounts = new Map<Id,Account>();
        Map<Id,Id> results = new Map<Id,Id>();
        
        List<Account> parents = new List<Account>();
        List<Account> initialAccounts = new List<Account>();
        
        for(Contact con : contacts){
            initialAccountIds.add(con.AccountId);
        }

        //make sure we have all the fields we need
        initialAccounts = [SELECT Id, ParentId, RecordType.DeveloperName
                   FROM Account
                   WHERE Id In :initialAccountIds];
                         
        allAccounts.putAll(initialAccounts);
        
        //Traverse parents if current record is not an RCID
        for (Account acc : initialAccounts) {
            if(acc.ParentId != null && acc.RecordType.DeveloperName != 'RCID') {
                parentIds.add(acc.ParentId);
            }
        }
        
        //Traverse until all RCIDs are added or exhausted
        while (parentIds.size() > 0) {
            parents = [SELECT Id, ParentId, RecordType.DeveloperName
                       FROM Account
                       WHERE Id In :parentIds AND Id NOT IN :allAccounts.keySet()];
            
            parentIds = new List<Id>(); 
             
            for (Account parent : parents) {
                if (parent.ParentId != null && parent.RecordType.DeveloperName != 'RCID')
                    parentIds.add(parent.ParentId);
            }
            
            allAccounts.putAll(parents);
        }
 
        //construct result where an account is mapped to its RCID       
        for (Account account : allAccounts.values()) {
            if (account.RecordType.DeveloperName == 'RCID') {
                results.put(account.Id, account.Id);
            }
            else {
                Account parent =  allAccounts.get(account.ParentId);
                
                while(parent != null &&
                      parent.Id != null &&
                      parent.Id != parent.parentId &&
                      parent.RecordType.DeveloperName != 'RCID') {
                    parent =  allAccounts.get(parent.ParentId);
                }
                
                if (parent != null) {
                    results.put(account.Id, parent.Id);
                }
            }
        }
        return results;
    }
}