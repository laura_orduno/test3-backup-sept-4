public class CaseAfterClass {

	public static void flagDeactivationOnAccount(List<Case> triggerNew, Boolean isUpdate, Boolean isInsert, Map<Id, Case> triggerOldMap) {
		
		// need to look for Cases that have Closed with the L&R Request Type field marked as Deactivation so that the Account can be flagged		
		Set<Id> updateAccountIds = new Set<Id>();
		
		if (isInsert) {
			for (Case c1: triggerNew) {
				if (c1.Status == 'Closed' && c1.L_R_Request_Type__c == 'Deactivation' && c1.AccountId != null)
					updateAccountIds.add(c1.AccountId);
			}			
		} else if (isUpdate) {
			for (Case c2: triggerNew) {
				System.debug('Status : ' + c2.Status);
				System.debug('Old Status : ' + triggerOldMap.get(c2.Id).Status);
				System.debug('Request Type : ' + c2.L_R_Request_Type__c);
				System.debug('Account Id : ' + c2.AccountId);
				if (c2.Status == 'Closed' &&  triggerOldMap.get(c2.Id).Status != 'Closed' && c2.L_R_Request_Type__c == 'Deactivation' && c2.AccountId != null)
					updateAccountIds.add(c2.AccountId);
			}			
		}
		
		if (!updateAccountIds.isEmpty()) {
			List<Account> updatedAccounts = [SELECT Id, Last_Deactivation_Request_Date__c FROM Account WHERE Id IN : updateAccountIds];
			
			for (Account a : updatedAccounts) {
				a.Last_Deactivation_Request_Date__c = System.today();
			}
			
			update updatedAccounts;
		}
	}

	public static void establishTrackingRecords(List<Case> triggerNew, Boolean isUpdate, Boolean isInsert, Map<Id, Case> triggerOldMap) {
			
		// enhanced tracking code
		List<Enhanced_Tracking__c> etInserts = new List<Enhanced_Tracking__c>();

		// gather up OwnerIds to get User Names and/or Group Names
		Set<Id> OwnerIds = new Set<Id>();
		for (Case c : triggerNew) {
			if (!OwnerIds.contains(c.OwnerId))
				OwnerIds.add(c.OwnerId);
		}

		Map <Id, String> OwnerIdMap = new Map<Id, String>();
		
		List<Group> groups = [SELECT Id, Name FROM Group WHERE Id IN :OwnerIds];
		
		for (Group g: groups) {
			if (!OwnerIdMap.containsKey(g.Id))
				OwnerIdMap.put(g.Id, g.Name);
		}
		
		List<User> users = [SELECT Id, Name FROM User WHERE Id IN : OwnerIds];
		
		for (User u: users) {
			if (!OwnerIdMap.containsKey(u.Id))
				OwnerIdMap.put(u.Id, u.Name);
		}
		
		if (isInsert) {
			for (Case c0 : triggerNew) {

				// create initial Enhancement Records 
				etInserts.add(createTrackingRecord(c0, 'Owner', OwnerIdMap));
				etInserts.add(createTrackingRecord(c0, 'Status', OwnerIdMap));
		
			} 	
		}
		
		if (isUpdate) {
			
			Set<Id> totalCaseIds = new Set<Id>();
			Map<Id, String> statusCaseMap = new Map<Id, String>();
			Map<Id, String> ownerCaseMap = new Map<Id, String>();
			Map<Id, Case> nonClosedCaseMap = new Map<Id, Case>();
			
			// look for Cases that had either Status Changes and/or Owner Changes
			for (Case c1 : triggerNew) {
	
				if (c1.Status != triggerOldMap.get(c1.Id).Status && !CaseTriggerHelper.hasStatusCaseId(c1.Id)) {
					statusCaseMap.put(c1.Id, c1.Status);
					if (!totalCaseIds.contains(c1.Id))
						totalCaseIds.add(c1.Id);
					CaseTriggerHelper.addStatusCaseIds(c1.Id);
				}
				
			 	// checking for Transfer to Queue to be null since this trigger fires twice when that value is set by the user.
				// we don't want to take the first iteration of the trigger in those cases
				if (c1.OwnerId != triggerOldMap.get(c1.Id).OwnerId && c1.Transfer_to_Queue__c == null && !CaseTriggerHelper.hasOwnerCaseId(c1.Id)) {
					ownerCaseMap.put(c1.Id, c1.OwnerId);
					if (!totalCaseIds.contains(c1.Id))
						totalCaseIds.add(c1.Id);
					CaseTriggerHelper.addOwnerCaseIds(c1.Id);
				}
				if (c1.Status != 'Closed' && c1.Status != 'Cancelled')
					nonClosedCaseMap.put(c1.Id, c1);
			}
	
			if (!totalCaseIds.isEmpty()) {
			
				List<Enhanced_Tracking__c> existingET = [SELECT Id, Field__c, Start_Value__c, Case__c, End_Value__c, End__c
														FROM Enhanced_Tracking__c WHERE Case__c IN : totalCaseIds
														AND End__c = NULL];
														
				if (!existingET.isEmpty()) {
					
					for (Enhanced_Tracking__c et0 : existingET) {
						if (ownerCaseMap.containsKey(et0.Case__c) && et0.Field__c == 'Owner') {
							
							// need to close out existing record
							et0.End__c = System.now();
							et0.End_Value__c = OwnerIdMap.get(ownerCaseMap.get(et0.Case__c));
							et0.Changed_By__c = UserInfo.getUserId();
							
							// if case isn't closed, need to start new tracking record
							if (nonClosedCaseMap.containsKey(et0.Case__c)) {
								etInserts.add(createTrackingRecord(nonClosedCaseMap.get(et0.Case__c), 'Owner', OwnerIdMap));	
							}
								
						}
						
						if (statusCaseMap.containsKey(et0.Case__c) && et0.Field__c == 'Status') {
							
							// need to close out existing record
							et0.End__c = System.now();
							et0.End_Value__c = statusCaseMap.get(et0.Case__c);
							et0.Changed_By__c = UserInfo.getUserId();
							
							// if case isn't closed, need to start new tracking record
							if (nonClosedCaseMap.containsKey(et0.Case__c)) {
								etInserts.add(createTrackingRecord(nonClosedCaseMap.get(et0.Case__c), 'Status', OwnerIdMap));	
							}	
						}
					}
					update existingET;
				}
			}
		}
		
		// todo add try/catch
		insert etInserts;
	}
	
	private static Enhanced_Tracking__c createTrackingRecord (Case workingCase, String RecType, Map <Id, String> OwnerMap) {
		
		Enhanced_Tracking__c returnET = new Enhanced_Tracking__c();
		
		if (RecType == 'Owner') {
			returnET.Field__c = 'Owner';
			returnET.Start_Value__c = OwnerMap.get(workingCase.OwnerId);
		} else if (RecType == 'Status') {
			returnET.Field__c = 'Status';
			returnET.Start_Value__c = workingCase.Status;
		}
		returnET.Case__c = workingCase.Id;
		returnET.Start__c = System.now();
		returnET.Changed_By__c = UserInfo.getUserId();
		
		return returnET;
	}
}