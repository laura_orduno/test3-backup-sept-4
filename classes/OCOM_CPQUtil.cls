/**
Vlocity

Feb 27 2017

Helper class to implement generic CPQ logic from apex 
-Includes the logic to stamp display sequence on OLIs

*/

global with sharing class OCOM_CPQUtil implements vlocity_cmt.VlocityOpenInterface {
    public static final String oneTimeChargesNRC = 'One-time Charges, NRC';
    public static final String recurringChargesMRC = 'Recurring Charges, MRC'; 
    public static String orderId=null;
  
    global Boolean invokeMethod(String methodName, Map<String, Object> input, Map<String, Object> output, Map<String, Object> options)  {
        
        if (methodName.equals('checkoutItems')){
         system.debug('____input___'+input);
          List<OrderItem> orderItemToUpdList = new List<OrderItem>();
          Map<Id,OrderItem> oliToDisConnectUpdMap = new Map<Id,OrderItem>();
          Map<Id,OrderItem> oliToDispSeqUpdMap = new Map<Id,OrderItem>();
          output = setOliDisplaySequenceNumber(input, output);
          if(output != null && !output.isEmpty() && output.containsKey('OrderItemsToUpdate')){
            oliToDispSeqUpdMap.putAll((Map<Id,OrderItem>)output.get('OrderItemsToUpdate'));
            //output.clear();
          }
          Map<String,Object> disconnectSrvcOutput = identifyDisconnectService(input, output);
          if(disconnectSrvcOutput != null && !disconnectSrvcOutput.isEmpty() && disconnectSrvcOutput.containsKey('disconOrderItemsToUpdate')){
              oliToDisConnectUpdMap.putAll((Map<Id,OrderItem>)disconnectSrvcOutput.get('disconOrderItemsToUpdate'));
          }

		 if( oliToDispSeqUpdMap != null && !oliToDispSeqUpdMap.isEmpty() && oliToDispSeqUpdMap.size() > 0){
             for(OrderItem oli :oliToDispSeqUpdMap.values()){
                 
                 orderItemToUpdList.add(oli);
                 if(oliToDisConnectUpdMap != null && oliToDisConnectUpdMap.containsKey(oli.id)){
                     
                     // if contains add toUpdateList 
                     // and then remove mapping from oliToDisConnectUpdMap
                     oli.vlocity_cmt__ServiceAccountId__c = oliToDisConnectUpdMap.get(oli.id).vlocity_cmt__ServiceAccountId__c;
                     oli.vlocity_cmt__JSONAttribute__c = oliToDisConnectUpdMap.get(oli.id).vlocity_cmt__JSONAttribute__c;
                     oliToDisConnectUpdMap.remove(oli.id);
                 }
             }
         }

            if(oliToDisConnectUpdMap != null && !oliToDisConnectUpdMap.isEmpty()  && oliToDisConnectUpdMap.size() > 0){
                for(OrderItem oli :oliToDisConnectUpdMap.values()){
                    orderItemToUpdList.add(oli);
                }
            }

        
          if(orderItemToUpdList != null && orderItemToUpdList.size() > 0 && !orderItemToUpdList.isEmpty()){
            update orderItemToUpdList;
          }
         
           OrdrUtilities.consolidateOrdItems(orderId);
          
        }
        //Added by Jaya for setting CPQ_Configuration_Status__c
        else if(methodName.equals('setCPQConfigurationStatus'))
        {
            setCPQConfigurationStatus(input,output);
        }
        return true;
    }
    
    //Added by Jaya for setting CPQ_Configuration_Status__c
    public void setCPQConfigurationStatus(Map<String, Object> inputMap, Map<String, Object> output)
    {
        Map<String,Object> orderObjStatusMap = new  Map<String,Object>();
        object orderObj = inputMap.get('CPQConfigurationSetup');
        System.debug('>>>> InputMap' + orderObj);
        orderObjStatusMap = (Map<String,Object>) orderObj;
        System.debug('>>>> orderObjStatusMap' + orderObjStatusMap);
        
        /*Map<String, List<Id>> configMessageOrderIds = new Map<String, List<Id>>();
       
        
        configMessageOrderIds = (Map<String, List<Id>>)inputMap.get('orderConfiguration');
        for(Id orId: configMessageOrderIds.get('orderConfigComplete'))
        {
            orderIdsConfigComplete.add(orId);
        }
        for(Id orId: configMessageOrderIds.get('orderConfigNotComplete'))
        {
            orderIdsConfigIncomplete.add(orId);
        }*/
         Map<String,String> orderIdsConfigMsg = new Map<String,String>();
       
        if(orderObjStatusMap.size() > 0 && !orderObjStatusMap.isEmpty() ){
            for(String orId: orderObjStatusMap.keySet()){
                    String isorderStatusComplete = (String) orderObjStatusMap.get(orID);
                    String orderStatusmsg = isorderStatusComplete.equalsIgnoreCase('complete') ? Label.CPQConfigureSuccessMessage : Label.CPQConfigurationStatusIncomplete ; 
                    System.debug('>>>orderStatusmsg____ '+ orderStatusmsg);
                    orderIdsConfigMsg.put(orId, orderStatusmsg);
                }
            }  
        
        
        if(orderIdsConfigMsg.size() > 0 && !orderIdsConfigMsg.isEmpty() )
        	updateCPQConfigurationStatus(orderIdsConfigMsg);                    
    }
    
    //Added by Jaya for setting CPQ_Configuration_Status__c
    //@future // Removed as per BSBD_RTA-1266
    public static void updateCPQConfigurationStatus(Map<String,String> orderIdsConfigMsg)
    {
        List<Order> orderUpdateList = new List<Order>();
        Map<String,Object> orderResult = new Map<String,Object>();
        
        for(String orId:orderIdsConfigMsg.keySet())
        {
            Order ord = new Order(Id=orId);
            ord.CPQ_Configuration_Status__c = orderIdsConfigMsg.get(orID);
            orderUpdateList.add(ord);
        }
        
       /* for(String orId:ordersConfigIncomplete.keySet())
        {
            Order ord = new Order(Id=orId);
            ord.CPQ_Configuration_Status__c = ordersConfigIncomplete.get(orId);
            orderUpdateList.add(ord);
        }
        /*Order ord = new Order(Id = orId);
               
        if(statusMessage == 'Configuration Complete')
        {
            ord.CPQ_Configuration_Status__c = 'Complete';
        }
        else if(statusMessage == 'Configuration Incomplete')
        {
            ord.CPQ_Configuration_Status__c = 'Incomplete';
        }
        
        update ord;*/
        if(orderUpdateList.size() > 0)
        {
            Database.SaveResult[] srList = Database.update(orderUpdateList,false);
            /*for(Database.SaveResult sr:srList)
            {
                if(sr.isSuccess())
                {
                    
                }
            }*/
        }
    }

//Adharsh: Commenting out till NC implment to update PCI sequence number
// This Method set the display sequence number on OLI.
// This display sequence number will be used by view pdf and contract table for sorting the OLIs{}

    public Map<String,Object>  setOliDisplaySequenceNumber( Map<String, Object> inputMap, Map<String, Object> output){
      Map<Id,OrderItem> orderItemToUpdMap = new Map<Id,OrderItem>();
      Map<String,Object> oliToDispSeqNumMap = new Map<String,String>();
      if(inputMap != null && inputMap.size()> 0 && inputMap.containsKey('OLISequenceMap')){
              oliToDispSeqNumMap = (Map<String,Object>) inputMap.get('OLISequenceMap');  
              System.debug('oliToDispSeqNumMap___' +oliToDispSeqNumMap);
          }
          
          if(oliToDispSeqNumMap != null && oliToDispSeqNumMap.size() > 0 && !oliToDispSeqNumMap.isEmpty()){
            
            for(String oliId: oliToDispSeqNumMap.keySet()){
               Schema.SObjectType objType;
               Id recId = Id.valueOf(oliId);
              objType = recId.getSobjectType();
              if(objType != null && objType.getDescribe().getName() == 'OrderItem'){
                String dispSeqNumber = (String)oliToDispSeqNumMap.get(oliId);
                OrderItem updateOli = new OrderItem(id = oliId, Display_Seq_Number__c = dispSeqNumber);
                orderItemToUpdMap.put(updateOli.id, updateOli);
              }
            }
          }
          System.debug('orderItemToUpdList_Size()___' +orderItemToUpdMap.size());
          if(orderItemToUpdMap != null && orderItemToUpdMap.size() > 0 && !orderItemToUpdMap.isEmpty()){
              output.put('OrderItemsToUpdate' , orderItemToUpdMap);
          } 
          return output;
    }

//This method identified the Disconnect serivces in the Cart and sets the static Map
// The Static Map will be referneced by Update Trigger to set the MoveOut address for Disconnect Service.

    public static Map<String,Object> identifyDisconnectService(Map<String, Object> inputMap, Map<String, Object> output){
      Map<Id,OrderItem> orderItemToUpdMap = new Map<Id,OrderItem>();
     

      Map<String,Object> oliToDispSeqNumMap = new Map<String,String>();
      Set<Id> disConnectIdSet = new Set<Id>(); 
      Map<Id,Id> oliToSrvcAddrIdMap = new Map<Id,Id>();
        Boolean isUpdateAttrsRequired = false;

        if(inputMap != null && inputMap.size()> 0 && inputMap.containsKey('OLISequenceMap')){
              oliToDispSeqNumMap = (Map<String,Object>) inputMap.get('OLISequenceMap');  
              System.debug('oliToDispSeqNumMap___' +oliToDispSeqNumMap);
          }

        for(OrderItem oli:[select id,vlocity_cmt__Product2Id__r.ProductSpecification__r.Name,
                           order.MoveOutServiceAddress__c, 
                           order.MoveOutServiceAddress__r.Service_Account_Id__c,
                           vlocity_cmt__ServiceAccountId__c,vlocity_cmt__JSONAttribute__c,
                           vlocity_cmt__RootItemId__c,vlocity_cmt__ParentItemId__c, 
                           vlocity_cmt__OneTimeTotal__c, 
                           vlocity_cmt__RecurringTotal__c,
                           vlocity_cmt__Product2Id__r.vlocity_cmt__JSONAttribute__c,orderid
                           from orderItem 
                           where ID =: oliToDispSeqNumMap.keySet()]){
                               orderId=oli.orderid;
                                Map<String,String>  updateAttributeValuesMap = new Map<String,String>();
                               
                               Map<String,OCOM_VlocityJSONParseUtil.JSONWrapper>  attributeMap = OCOM_VlocityJSONParseUtil.getAttributesFromJSON(oli.vlocity_cmt__JSONAttribute__c); 
                               if(attributeMap != null && attributeMap.containsKey(oneTimeChargesNRC)){
                                   updateAttributeValuesMap.put(oneTimeChargesNRC,String.valueof(oli.vlocity_cmt__OneTimeTotal__c));
                                   isUpdateAttrsRequired = true;
                                   
                               }
                               if(attributeMap != null && attributeMap.containsKey(recurringChargesMRC)){
                                   updateAttributeValuesMap.put(recurringChargesMRC,String.valueof(oli.vlocity_cmt__RecurringTotal__c));
                                   isUpdateAttrsRequired = true;
                               }
                               if(isUpdateAttrsRequired == true){
                                    String newJsonAttrString =  setOliAttributeRuntimeData(updateAttributeValuesMap,attributeMap, oli);
                                   //system.debug('newJsonAttrString_______' +Json.Serialize(newJsonAttrString));
                                   if(newJsonAttrString != null && newJsonAttrString != ''){
                                       oli.vlocity_cmt__JSONAttribute__c = newJsonAttrString;
                                       orderItemToUpdMap.put(oli.id,oli); 
                                   }
                                }
                                if( oli.vlocity_cmt__Product2Id__r.ProductSpecification__r.Name== 'TELUS Lightweight Product' &&  oli.order.MoveOutServiceAddress__c !=null &&  oli.order.MoveOutServiceAddress__r.Service_Account_Id__c !=null)
                                  {  
                                      
                                      if( attributeMap.ContainsKey('Offering Category') && attributeMap.get('Offering Category') != null  && attributeMap.get('Offering Category').value == 'Disconnect Services'){
                                          System.debug('#### Disconnect Service 2:'+oli.order.MoveOutServiceAddress__r.Service_Account_Id__c);
                                          oli.vlocity_cmt__ServiceAccountId__c = oli.order.MoveOutServiceAddress__r.Service_Account_Id__c;
                                          disConnectIdSet.add(oli.Id);
                                          oliToSrvcAddrIdMap.put(oli.id, oli.vlocity_cmt__ServiceAccountId__c);
                                          vlocity_cmt.FlowStaticMap.FlowMap = new Map<String,Object>{'disconnectSeriveceOliSet' => oliToSrvcAddrIdMap};
                                              //system.debug(' vlocity_cmt.FlowStaticMap.FlowMap.put' + vlocity_cmt.FlowStaticMap.FlowMap.get('disconnectSeriveceOliSet'));
                                          orderItemToUpdMap.put(oli.id,oli);
                                    	}  
                                  } 
                                  else if( oli.vlocity_cmt__RootItemId__c != null && disConnectIdSet.contains(oli.vlocity_cmt__RootItemId__c) 
                                          && oli.order.MoveOutServiceAddress__c !=null &&  oli.order.MoveOutServiceAddress__r.Service_Account_Id__c !=null  )
                                  {
                                      System.debug('#### Disconnect Service 3:'+oli.order.MoveOutServiceAddress__r.Service_Account_Id__c);                                 
                                      oli.vlocity_cmt__ServiceAccountId__c = oli.order.MoveOutServiceAddress__r.Service_Account_Id__c;  
                                      orderItemToUpdMap.put(oli.id,oli);                         
                                  } 
                               //system.debug('newJsonAttrString_______' +Json.Serialize(oli.vlocity_cmt__JSONAttribute__c));
                           }
                                  output.put('disconOrderItemsToUpdate',orderItemToUpdMap);
                                  return  output; 

    }
    
    public static String setOliAttributeRuntimeData(Map<String,String> updateAttributeValuesMap , Map<String,OCOM_VlocityJSONParseUtil.JSONWrapper>  attributeMap, OrderItem oli){
        String JSONString;
        if(updateAttributeValuesMap != null && updateAttributeValuesMap.size() > 0 && !updateAttributeValuesMap.isEmpty() ){
            List<vlocity_cmt.JSONAttributeSupport.JSONAttributeActionRequest> actionRequestsList = new List<vlocity_cmt.JSONAttributeSupport.JSONAttributeActionRequest>();

            for(string key : updateAttributeValuesMap.keySet()){
                system.debug('!!@@modifiedJSON 0' + key);
                
                OCOM_VlocityJSONParseUtil.JSONWrapper JWrap = attributeMap.get(key);
                //system.debug('!!@@JWrap_____' + JWrap);
                
                if(null !=JWrap){
                    actionRequestsList.add(new vlocity_cmt.JSONAttributeSupport.JSONAttributeActionRequest(JWrap.attributeuniquecode, vlocity_cmt.JSONAttributeSupport.ActionType.ASSIGN,updateAttributeValuesMap.get(key) ));        
                    System.debug('#### postCartsItems.PostInvoke: JWrap.attributeuniquecode='+ JWrap.attributeuniquecode + ',value=' + updateAttributeValuesMap.get(key)   ); 
                }
            }
            system.debug('!!@@modifiedJSON 0 1' + actionRequestsList);
            // then 
            
            Map<String, object> inputMap = new Map<String, object>();
            Map<String, object> outputMap = new Map<String, object>();
            Map<String, object> optionsMap = new Map<String, object>();
                  
            // objectSO can be null 
            //inputMap.put('objectSO', null);                                
            inputMap.put('runTimeAttributesJSON',  oli.vlocity_cmt__JSONAttribute__c);
            inputMap.put('originalAttributesJSON', oli.vlocity_cmt__Product2Id__r.vlocity_cmt__JSONAttribute__c);                                
            inputMap.put('JSONAttributeActionRequestList', actionRequestsList);
            
            vlocity_cmt.JSONAttributeSupport jsonSupport = new vlocity_cmt.JSONAttributeSupport();
            
            jsonSupport.invokeMethod('applyAttributeActions', inputMap, outputMap, optionsMap);
            
            //  Here get the JSON that got rebuilt 
            // String modifiedJSON = (String)outputMap.get('modifiedJSON');   
            JSONString = (String)outputMap.get('modifiedJSON');
        }
        return JSONString;
    }



}