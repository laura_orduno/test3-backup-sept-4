public class AsyncLogger implements queueable{
    public list<webservice_integration_error_log__c> logs{get;set;}
    public AsyncLogger(list<webservice_integration_error_log__c> logs){
        this.logs=logs;
    }
    public void execute(queueablecontext qc){
        if(logs!=null&&!logs.isempty()){
            try{
                insert logs;
            }catch(exception e){
                Util.emailSystemError(e);
            }
        }
    }
}