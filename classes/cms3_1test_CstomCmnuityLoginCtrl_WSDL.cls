/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class cms3_1test_CstomCmnuityLoginCtrl_WSDL{
  static testMethod void cms3_1CustomCommunityLoginController(){
       cms3_1CustomCommunityLoginController smb13 = new cms3_1CustomCommunityLoginController();
       cms3_1CustomCommunityLoginController.CivicAddress x11= new cms3_1CustomCommunityLoginController.CivicAddress();
       cms3_1CustomCommunityLoginController.MilitaryAddress x12= new cms3_1CustomCommunityLoginController.MilitaryAddress();
       cms3_1CustomCommunityLoginController.RuralAddress x13= new cms3_1CustomCommunityLoginController.RuralAddress();
       cms3_1CustomCommunityLoginController.Address x14= new cms3_1CustomCommunityLoginController.Address();
       cms3_1CustomCommunityLoginController.CustomerMarketSegment x15= new cms3_1CustomCommunityLoginController.CustomerMarketSegment();
       cms3_1CustomCommunityLoginController.StationSquareAddress x16= new cms3_1CustomCommunityLoginController.StationSquareAddress();
       cms3_1CustomCommunityLoginController.Name x17= new cms3_1CustomCommunityLoginController.Name();
        
}
}