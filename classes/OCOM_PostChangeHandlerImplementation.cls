/**
    Vlocity
        
    Custom Implementation for PostChangeHandler interface 
*/


global with sharing class OCOM_PostChangeHandlerImplementation  implements vlocity_cmt.VlocityOpenInterface{
    global static String CUSTOMER_SOLUTION_RECORDTYPE_NAME = 'Customer Solution';
    global static String CUSTOMER_ORDER_RECORDTYPE_NAME = 'Customer Order';
    
    global Boolean invokeMethod(String methodName, Map<String, Object> input, Map<String, Object> output, Map<String, Object> options)  {
        system.debug('!@ methodName '+methodName);
        system.debug('!@ input '+input);
        system.debug('!@ output '+output);
        system.debug('!@ options '+options);
        
            if (methodName.equals('postChangeForQuote')){
                return postChangeForQuote(input,output,options);
            }else if (methodName.equals('postChangeForOrder')){
                return postChangeForOrder(input,output,options);
            }
            return false;
        }
    private Boolean postChangeForQuote(Map<String, Object> input, Map<String, Object> output, Map<String, Object> options){
        try{
      Set<string> referenceId= new Set<string>();
            Id createdQuoteId = (Id)input.get('createdObjectId');
            List<Quote> createdQuoteList = [SELECT Id, Name, vlocity_cmt__TrackingNumber__c FROM Quote WHERE Id = :createdQuoteId];
      
      if(null!=createdQuoteList && createdQuoteList.size()>0){
      
        Quote createdQuote = createdQuoteList[0];

                // To Aditya. I am adding this one line here Apl 21 2016 Shuji
                createdQuote.New_Location_Or_MACD__c ='Change';

        Set<string> serviceAccountIds= new Set<string>();
        List<QuoteLineItem> createdQuoteLineItemList = [SELECT Id,vlocity_cmt__AssetReferenceId__c,vlocity_cmt__ServiceAccountId__c FROM QuoteLineItem WHERE quoteId = :createdQuoteId];
        if(null != createdQuoteLineItemList && createdQuoteLineItemList.size()>0){
            for(QuoteLineItem qLineItem:createdQuoteLineItemList){
                            if(null!=qLineItem.vlocity_cmt__AssetReferenceId__c && qLineItem.vlocity_cmt__AssetReferenceId__c.substring(0, 3) == '802'){
               referenceId.add(qLineItem.vlocity_cmt__AssetReferenceId__c);   
                            } 
                            if(null!=qLineItem.vlocity_cmt__ServiceAccountId__c){
                                   serviceAccountIds.add(qLineItem.vlocity_cmt__ServiceAccountId__c);
                                system.debug(' SMB Care 0 '+qLineItem.vlocity_cmt__ServiceAccountId__c);
                              }   
            }
                    string serviceAccountID='';String AddressString='';String SMBAddressId='';
                    system.debug(' size set'+serviceAccountIds.size());                   
                    if(null!=serviceAccountIds && serviceAccountIds.size()==1){
                         List<string> OnlyOneServiceAccount= new List<string>();
                        OnlyOneServiceAccount.addAll(serviceAccountIds);
                        serviceAccountID=OnlyOneServiceAccount[0];  system.debug(' SMB Care 1 '+serviceAccountID);
                       List<Smbcare_address__c> smbCareAddr =[select Id,Address__c,City__c,Province__c,Postal_Code__c,Country__c from smbcare_address__c where service_account_Id__c=:serviceAccountID]; 
                     system.debug(' SMB Care '+smbCareAddr);
                        AddressString = smbCareAddr[0].Address__c + ' ' + smbCareAddr[0].City__c + ' ' + smbCareAddr[0].Province__c + ' ' + smbCareAddr[0].Postal_Code__c + ' ' + smbCareAddr[0].Country__c;
                       SMBAddressId=smbCareAddr[0].Id;  
                    }
          List<OrderItem> createdOrderItemList = [SELECT Id,vlocity_cmt__AssetReferenceId__c,orderId,order.Service_Address_Text__c,order.Service_Address__c,order.Service_Address__r.Status__c,order.CustomerAuthorizedById,order.BAN__C,vlocity_cmt__ServiceAccountId__c,order.Billing_Address__c FROM OrderItem WHERE id = :referenceId];
          if(null!= createdOrderItemList && createdOrderItemList.size()>0){
                        
           createdQuote.Service_Address__c=SMBAddressId; //createdOrderItemList[0].order.Service_Address__c;
           createdQuote.Service_Address_Text__c=AddressString; //createdOrderItemList[0].order.Service_Address_Text__c;
                     createdQuote.ServiceAccountId__c=serviceAccountID; //createdOrderItemList[0].vlocity_cmt__ServiceAccountId__c;                        
                     createdQuote.contactid=createdOrderItemList[0].order.CustomerAuthorizedById;
                     createdQuote.status='In Progress'; 
                     createdQuote.Sales_Representative__c=UserInfo.getUserId(); 
                     createdQuote.Ban__c=createdOrderItemList[0].order.BAN__C; 
                     createdQuote.Billing_Address__c=createdOrderItemList[0].order.Billing_Address__c; 
                     Date today = Date.today();
                     Date ExpDate = null;
                    OCOM_Quote_Settings__c qs = OCOM_Quote_Settings__c.getOrgDefaults();
                    system.debug('Quote Settings: '+ qs);
                        
                    if(qs!=null && qs.Days_to_expiry__c > 0 )
                    {
                        ExpDate = today.addDays((Integer)qs.Days_to_expiry__c);
                    }   
                        createdQuote.ExpirationDate = ExpDate;
                        if(createdOrderItemList[0].order.Service_Address__r.Status__c=='valid'){
                          createdQuote.Activities__c='Address confirmed'+ ';\r\n Prices valid until '+ ExpDate.format();
             }else {
              createdQuote.Activities__c='Created by '+UserInfo.getName() +' \n Pending: Address validation \n Action: Confirm Address Created by ' + UserInfo.getName();
            }  
                        
           }
        }
      
          update createdQuote;
          System.debug('Updated quote is :: '+createdQuote);
        
      }  
            return true;
        }catch(Exception e){
            System.debug(LoggingLevel.ERROR, e);
        }
        return false;
    }

    private Boolean postChangeForOrder(Map<String, Object> input, Map<String, Object> output, Map<String, Object> options){
        try{
            System.Debug('#### Called ');

            Id createdOrderId = (Id)input.get('createdObjectId');
            
            //Start : BSBD_RTA-339 redirected to Single Site ACD to Order 2.0 Flow Added by Aditya Jamwal(IBM)  17-10-2017
            if(String.isBlank(createdOrderId)){
                createdOrderId = (Id)input.get('fdoId');
            }
            //End : BSBD_RTA-339 redirected to Single Site ACD to Order 2.0 Flow Added by Aditya Jamwal(IBM)  17-10-2017
            
            //List<Order> createdOrderList = [SELECT Id, Name, vlocity_cmt__TrackingNumber__c,accountid,account.recordtype.name,account.parentID,Service_Address_Text__c,Service_Address__c,CustomerAuthorizedById,BAN__C FROM Order WHERE Id = :createdOrderId];
             
             //Jaya - Changing the query to retreive service account
             List<Order> createdOrderList = [SELECT Id, Name, vlocity_cmt__TrackingNumber__c,accountId,account.recordtype.name,account.parentID,Service_Address_Text__c,Service_Address__c,CustomerAuthorizedById,BAN__C,ServiceAccountId__c FROM Order WHERE Id = :createdOrderId];
             System.debug('OrderList** '+createdOrderList);
             List<OrderItem> orderItems = [SELECT Id,contract_request__c,contract_line_item__r.vlocity_cmt__ContractId__r.Complex_Fulfillment_Contract__c ,vlocity_cmt__AssetReferenceId__c,vlocity_cmt__ServiceAccountId__c,vlocity_cmt__BillingAccountId__c from OrderItem where OrderId = :createdOrderId];
             Map<string,orderItem> NewOrderItemToOldOrdItemID= new Map<string,orderItem>();   
             Set<string> NewOrderItemIDs= new Set<string>();   
             string OrderItemId;
             LIST<orderItem> OLI = new LIST<orderItem>();
             
           
             
            //Changing a sample field  Tracking Number
            if(createdOrderList != null) { 
                Map<id,Order> oldmap = new Map<id,Order>();
                Order oldchildOrder = createdOrderList[0].clone(true);
                oldchildOrder.parentId__c = null;
                oldmap.put(oldchildOrder.id,oldchildOrder);
                Order createdOrder = createdOrderList[0];
                // To Aditya. I am adding this one line here Apl 21 2016 Shuji
                createdOrder.Type ='Change';

                for(OrderItem ordItem : orderItems) 
                 {   
                    NewOrderItemIDs.add(ordItem.vlocity_cmt__AssetReferenceId__c);    
                 }
                 
                if(orderItems.size()>0){
                  OLI =[select id,contract_request__c,contract_line_item__r.vlocity_cmt__ContractId__r.Complex_Fulfillment_Contract__c,orderId,vlocity_cmt__ServiceAccountId__c,Billing_Account__c,Billing_Address__c,
                            order.Service_Address_Text__c,order.Service_Address__c,order.Billing_Address__c,order.CustomerAuthorizedById,
                            order.BAN__c,vlocity_cmt__BillingAccountId__c,vlocity_cmt__AssetReferenceId__c 
                        from orderItem where id=:NewOrderItemIDs];
                }                 
                for(orderItem ord:OLI){
                    NewOrderItemToOldOrdItemID.put(string.ValueOf((ord.Id)).substring(0,15),ord);            
                }
                    system.debug(' NewOrderItem '+NewOrderItemToOldOrdItemID);
                    String contactId;
                    if(null!=OLI &&OLI.size()>0 )
                       { 
                        system.debug(' createdOrder inside '+createdOrder);
                        createdOrder.Service_Address_Text__c = OLI[0].order.Service_Address_Text__c;
                        createdOrder.Service_Address__c= OLI[0].order.Service_Address__c;
                        contactId = createdOrder.CustomerAuthorizedById=OLI[0].order.CustomerAuthorizedById;
                        output.put('contactId',contactId);
                        createdOrder.BAN__c=OLI[0].order.BAN__c;
                        createdOrder.Billing_Address__c=OLI[0].order.Billing_Address__c;  
                        //Jaya - adding the below line to set the serviceAccountId of the Order
                        createdOrder.ServiceAccountId__c = OLI[0].vlocity_cmt__ServiceAccountId__c;
                         for(OrderItem OrI:orderItems){  
                             
                             if(null!=NewOrderItemToOldOrdItemID && null!= NewOrderItemToOldOrdItemID.get((OrI.vlocity_cmt__AssetReferenceId__c).substring(0,15))){
                               string assetRef = (OrI.vlocity_cmt__AssetReferenceId__c).substring(0,15);
                                orderItem oldORItem=NewOrderItemToOldOrdItemID.get(assetRef);
                                OrI.vlocity_cmt__BillingAccountId__c=oldORItem.vlocity_cmt__BillingAccountId__c; 
                                OrI.Billing_Address__c=oldORItem.Billing_Address__c;
                                OrI.Billing_Account__c=oldORItem.Billing_Account__c;
                                OrI.vlocity_cmt__ServiceAccountId__c= oldORItem.vlocity_cmt__ServiceAccountId__c;
                                 if(String.isNotBlank(OrI.contract_line_item__r.vlocity_cmt__ContractId__r.Complex_Fulfillment_Contract__c)){
                                    OrI.contract_request__c=OrI.contract_line_item__r.vlocity_cmt__ContractId__r.Complex_Fulfillment_Contract__c; 
                                 } 
                                 
                                 system.debug(' createdOrder OrderItem 2 '+OrI);    
                             }
                           }
                      } 
                
                   List<Order> updateOrderList  = new List<Order>();   
                //Start : BSBD_RTA-339 redirected to Single Site ACD to Order 2.0 Flow Added by Aditya Jamwal(IBM)  17-10-2017
                    VlocityCPQUtil vloCPQutil = new VlocityCPQUtil();
                    Map<String,Object>inputMap = new Map<String,Object>();
                    Map<String,Object>outputMap = new Map<String,Object>();
                    Map<String,Object>optionsMap = new Map<String,Object>();
                    inputMap.put('rcid',createdOrder.accountId);
                    inputMap.put('orderType',createdOrder.Type);
                    inputMap.put('contactId',contactId);
                    vloCPQutil.invokeMethod('createCustomerSolution', inputMap, outputMap, optionsMap);
                    if(null != outputMap && outputMap.size()>0){
                        system.debug(' createdOrder -1 '+inputMap);
                        String parentID = (String) outputMap.get('customerSolutionId');
                        if(String.isNotBlank(parentID)){     
                            system.debug(' createdOrder -0 '+parentID);                    
                            output.put('parentId',parentID);
                            createdOrder.parentId__c = parentID;
                            Order parentOrder = new Order(id = parentID);
                          //  updateOrderList.add(parentOrder);
                          //   oldmap.put(parentID,parentOrder);
                        }else if(null != outputMap.get('error')) {
                            outputMap.put('error',outputMap.get('error'));
                        }                   
                    }
                    output.put('rcid',createdOrder.accountId);
                    createdOrder.recordtypeId = vloCPQutil.getRecordTypeId(CUSTOMER_ORDER_RECORDTYPE_NAME);      
                //End : BSBD_RTA-339 redirected to Single Site ACD to Order 2.0 Flow Added by Aditya Jamwal(IBM) 17-10-2017
                 updateOrderList.add(createdOrder);                
                 system.debug(' createdOrder 1 '+createdOrder+'\n outputMap '+output); 
                 OCOM_OrderBeforeTriggerHelper.generateSubOrderNumber(false,true,updateOrderList,oldmap);
                 update updateOrderList;
                 system.debug(' createdOrder 2'+createdOrder);    
                if(orderItems.size()>0){
                   update orderItems;
                }   
            }  
            return true;
        }catch(Exception e){
            System.debug(LoggingLevel.ERROR, e.getMessage());
            
            system.debug('!!!Exception Search ' +e.getStackTraceString());
        }
        return false;
    }

    
}