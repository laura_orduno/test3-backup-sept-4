@isTest(SeeAllData=false) 
public class smb_CreateRemarkControllerExtTest {
    
    static {
        Call_Tracker_Note_Template__c template1 = new Call_Tracker_Note_Template__c();
        template1.Call_Received_From__c = 'Source';
        template1.Request_Type__c = 'Reason';
        template1.Inbox__c = False;
        template1.Name = 'template1';
        template1.Handlebars_Template__c = 'this is template1 {{Fake_Field__c}}';
        template1.Call_Service_Type__c = 'abc wireless';
        insert template1;
        
        Call_Tracker_Note_Template__c template2 = new Call_Tracker_Note_Template__c();
        template2.Call_Received_From__c = 'Source';
        template2.Request_Type__c = 'Reason';
        template2.Inbox__c = True;
        template2.Name = 'template2';
        template2.Handlebars_Template__c = 'this is template2 {{Fake_Field__c}}';
        template2.Call_Service_Type__c = 'abc wireless';        
        insert template2;
        
    }
    
    
    static testMethod void testCreateRemark() {
        test.startTest();
        
        String STATUS_SUBMIT = 'SUBMITTING REMARK';
        String STATUS_GENERATE = 'GENERATING REMARK';        
        
        // load new data
        Call_Tracker_Picklist_Data__c newData = new Call_Tracker_Picklist_Data__c();
        newData.Service_Type__c = 'testData';
        newData.Picklist_Values__c ='disposition,Volcanic,*\ndisposition,Tropical,*\ndisposition,Pacific,*\ndisposition,Central,Equatorial\ndisposition,Central,North\ndisposition,West,West\ncalltype,West,\ncalltype,South,Fiji\ncalltype,South,Tahiti\ncalltype,Equatorial,Samoa\ncalltype,Equatorial,Polynesia\ncalltype,North,Maui\ncalltype,North,Kauai\ncalltype,North,Hawaii\ncalltype,North,*';
        insert newData;
        
        smb_CreateRemarkControllerExtension.getPicklistHTML('testData');
        
        Task newTask = new Task(Subject='test subject');
        newTask.Priority = 'High';
        
        smb_CreateRemarkControllerExtension.callServiceTypeFld = 'servicetype';
        smb_CreateRemarkControllerExtension.callDispositionFld = 'disposition';
        smb_CreateRemarkControllerExtension.callTypeFld = 'calltype';
        smb_CreateRemarkControllerExtension.callTopicFld = 'calltopic';
        
        PageReference pageRef = Page.smb_CreateRemark;
        pageRef.getParameters().put('Subject',newTask.Subject);
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller stdcon = new ApexPages.Standardcontroller(newTask);
        
        smb_CreateRemarkControllerExtension crObj = new smb_CreateRemarkControllerExtension(stdcon);
        crObj.saveRecord();
        
        // Save the same record (ID) again to increase coverage
        crObj.saveRecord();
        
        smb_CreateRemarkControllerExtension.callServiceTypeFld = '';
        smb_CreateRemarkControllerExtension.callDispositionFld = '';
        smb_CreateRemarkControllerExtension.callTypeFld = '';
        smb_CreateRemarkControllerExtension.callTopicFld = '';
        crObj.saveRecord();
        
        crObj.getRecordType();
        smb_CreateRemarkControllerExtension.getAccountId();
        smb_CreateRemarkControllerExtension.getCurrentUserCallCentreStatus();
        
        // delete the test data
        delete newData;
        
        // load new data, test parse errors - not enough fields (expecting 3 per line)
        Call_Tracker_Picklist_Data__c newData2 = new Call_Tracker_Picklist_Data__c();
        newData2.Service_Type__c = 'testData2';
        newData2.Picklist_Values__c ='disposition,Volcanic,stuff\ndisposition,Volcanic,more\ndisposition,nothing';
        insert newData2;
        smb_CreateRemarkControllerExtension.getPicklistHTML('testData2');
        delete newData2;
        
        crObj.resetLnRRequestType();
        
        crObj.taskRecord.Call_Service_Type_Picklist__c = 'lnr wireline';
        crObj.validateLnR(STATUS_GENERATE);
        
        crObj.taskRecord.Request_Type__c = 'competitive threat';
        crObj.taskRecord.Competitive_Threat__c = 'Other';
        crObj.taskRecord.Competitor_Name__c = 'Other';        
        crObj.validateLnR(STATUS_GENERATE);
        
        crObj.taskRecord.Request_Type__c = 'soft shopping - no specific competitor';
        crObj.validateLnR(STATUS_GENERATE);
        
        crObj.taskRecord.Request_Type__c = 'escalation';
        crObj.validateLnR(STATUS_GENERATE);
        
        crObj.taskRecord.Request_Type__c = 'deactivation (cancel)';
        crObj.taskRecord.Deactivation_Reason__c = 'Other';
        crObj.validateLnR(STATUS_SUBMIT);
        
        crObj.taskRecord.Request_Type__c = 'general inquiry';
        crObj.validateLnR(STATUS_GENERATE);
        
        crObj.taskRecord.Request_Type__c = 'misdirect';
        crObj.validateLnR(STATUS_GENERATE);
        
        crObj.taskRecord.Call_Service_Type_Picklist__c = 'lnr business anywhere';
        crObj.validateLnR(STATUS_GENERATE);
        
        crObj.taskRecord.Request_Type__c = 'competitive threat';
        crObj.validateLnR(STATUS_GENERATE);
        
        crObj.taskRecord.Request_Type__c = 'mid contract RPO';
        crObj.validateLnR(STATUS_GENERATE);
        
        crObj.taskRecord.Request_Type__c = 'hardware exception';
        crObj.validateLnR(STATUS_GENERATE);
        
        crObj.taskRecord.Request_Type__c = 'device balance exception';
        crObj.validateLnR(STATUS_GENERATE);
        
        crObj.taskRecord.Call_Received_From__c = 'corporate stores';
        crObj.validateLnR(STATUS_GENERATE);
        
        test.stopTest();
        
    }
    
    
    /**
*  @Description    Test for RemoteAction getTemplate().
*  @author         Allison Ng, Traction on Demand
*  @date           2017-01-20
*/
    private static testmethod void testGetTemplate_withoutInbox() {
        test.startTest();
        
        // Template exists for values below for both Inbox = False & True. 
        // getRemote() should choose the template since Inbox = False (usually Inbox takes precedence over CallReceivedFrom)
        String requestType = 'Reason';
        String callReceivedFrom = 'Source';
        String callServiceType = 'abc wireless';
        Boolean inbox = False;
        
        Call_Tracker_Note_Template__c expectedTemplate = [SELECT Call_Received_From__c, Request_Type__c, Inbox__c, Handlebars_Template__c, Call_Service_Type__c FROM Call_Tracker_Note_Template__c WHERE Request_Type__c = :requestType AND Call_Received_From__c = :callReceivedFrom AND Inbox__c = :inbox AND Call_Service_Type__c = :callServiceType];    
        
        Call_Tracker_Note_Template__c resultTemplate = smb_CreateRemarkControllerExtension.getTemplate(expectedTemplate.Call_Received_From__c, expectedTemplate.Request_Type__c, expectedTemplate.Inbox__c, expectedTemplate.Call_Service_Type__c );
        
        System.assertNotEquals(null, resultTemplate,'No Call_Tracker_Note_Template was returned');
        System.assertEquals(expectedTemplate.Handlebars_Template__c, resultTemplate.Handlebars_Template__c, 'The template is incorrect');
        
        test.stopTest();
        
    }
    
    /**
*  @Description    Test for RemoteAction getTemplate().
*  @author         Allison Ng, Traction on Demand
*  @date           2017-01-20
*/
    private static testmethod void testGetTemplate_withInbox() {
        // Template exists for values below for both Inbox = False & True. 
        // getRemote() should choose the template with Inbox + RequestType over CallReceivedFrom + Inbox
        test.startTest();
        
        String requestType = 'Reason';
        String callReceivedFrom = 'Source';
        String callServiceType = 'abc wireless';        
        Boolean inbox = True;
        
        Call_Tracker_Note_Template__c expectedTemplate = [SELECT Call_Received_From__c, Request_Type__c, Inbox__c, Handlebars_Template__c, Call_Service_Type__c FROM Call_Tracker_Note_Template__c WHERE Request_Type__c = :requestType AND Call_Received_From__c = :callReceivedFrom AND Inbox__c = :inbox AND Call_Service_Type__c = :callServiceType];    
        
        Call_Tracker_Note_Template__c resultTemplate = smb_CreateRemarkControllerExtension.getTemplate(expectedTemplate.Call_Received_From__c, expectedTemplate.Request_Type__c, expectedTemplate.Inbox__c, expectedTemplate.Call_Service_Type__c);
        
        System.assertNotEquals(null, resultTemplate,'No Call_Tracker_Note_Template was returned');
        System.assertEquals(expectedTemplate.Handlebars_Template__c, resultTemplate.Handlebars_Template__c, 'The template is incorrect');
        test.stopTest();
        
    }
    
    /**
*  @description    Tests the validateBeforeCreatingRemark() that is used in Call Tracker Note Template generation
*  @author         Allison Ng, Traction on Demand
*  @date           2017-01-20
*/
    private static testmethod void testValidateBeforeCreatingRemark() {
        test.startTest();
        
        Task taskRecord = new Task(Subject='test subject');
        
        taskRecord.Priority = 'High';
        taskRecord.Call_Service_Type_Picklist__c = 'LnR Wireless';
        taskRecord.Product_Subcategory__c = 'Mike';
        taskRecord.BAN__c =  1234;
        taskRecord.Call_Received_From__c = 'Direct Sales';
        taskRecord.Dealer_Direct_Sales_Detail__c = 'Johnny Apples';
        taskRecord.Request_Type__c = 'General Inquiry';
        taskRecord.General_Inquiry__c = 'Customer Service';
        taskRecord.Client_Region__c = 'BC';
        taskRecord.Churn_Threat_Gauge__c = 'Low';
        taskRecord.Offer_Status__c = 'Pending';
        taskRecord.No_Followup_Required__c = True;
        
        ApexPages.Standardcontroller stdcon = new ApexPages.Standardcontroller(taskRecord);
        smb_CreateRemarkControllerExtension crObj = new smb_CreateRemarkControllerExtension(stdcon);
        
        PageReference pageRef = Page.smb_CreateRemark;
        pageRef.getParameters().put('Subject', taskRecord.Subject);
        Test.setCurrentPage(pageRef);
        
        // SAD obj is created in constructor of smb_CreateRemarkControllerExtension
        Supplemental_Activity_Data__c taskExtend = crObj.taskExtend;
        taskExtend.Service_Phone_Number_Worked_On__c = '111-222-1234';
        taskExtend.Callback_Number__c = '604-670-6040';
        taskExtend.Client_Tenure__c = '1991';
        taskExtend.Dealer_Code__c = '111222';
        
        // When "Generate Note" button is clicked, "validateBeforeCreatingRemark()" is run
        // Expecting fail due to missing taskExtend.SalesRepCode
        crObj.validateBeforeCreatingRemark();
        System.assertEquals(crObj.taskFields, '');
        System.assertEquals(crObj.taskExtendFields, '');
        
        taskExtend.Sales_Rep_Code__c = '101010';
        crObj.validateBeforeCreatingRemark();
        
        System.assertNotEquals(crObj.taskFields, null);
        System.assertNotEquals(crObj.taskFields, '');
        System.assertNotEquals(crObj.taskExtendFields, null);
        System.assertNotEquals(crObj.taskExtendFields, '');
        test.stopTest();
        
    }  
    
}