/*
    *******************************************************************************************************************************
    Class Name:     OC_OrderStatusHelper
    Purpose:        update status on child order and Master order        
    Created by:     Sandip Chaudhari         08-Apr-2016    No previous version
    Modified by:    Santosh Rath  04-Feb-2018 Reducing no of SOQLs executed
    *******************************************************************************************************************************
*/

Public class OC_OrderStatusHelper{
    public static List<OC_MasterOrderStatusPriorityMapping__mdt> listStatusSeq;
    public static List<OrderItem> orderItemsList=null;
    static{
        listStatusSeq = [SELECT DeveloperName, Sequence__c, MasterLabel, Master_Order_Status__c FROM OC_MasterOrderStatusPriorityMapping__mdt];
    }
    public static List<Order> allOrders=null;
    public static void updateOrderStatus(Boolean isBefore,Boolean isInsert,Boolean isUpdate, Boolean isAfter, List<Order> newOrderList, Map<Id,Order> oldOrderMap){
        // Start BMPF-20
        Map<Id,Map<Id,String>> childOrderStatusMap = new  Map<Id,Map<Id,String>>();
        Map<Id, Map<Id, List<OrderItem>>> orderMap = new Map<Id, Map<Id, List<OrderItem>>>();
        Map<Id, List<OrderItem>> orderItemMap = new Map<Id, List<OrderItem>>();
        List<Id> orderIdList=new List<String>();
        for(Order oInt:newOrderList){
            orderIdList.add(oInt.id);
        }
        System.debug('debugging orderIdList='+orderIdList);
        
        if(orderItemsList==null || orderItemsList.isEmpty()){
            orderItemsList = [
            SELECT Id, PriceBookEntry.product2.Catalog__r.Name, OfferName__c, 
                    Term__c, ServiceDate, EndDate,orderMgmtId_Status__c,
                    Service_Address_Full_Name__c, service_address__r.Province__c,
                    vlocity_cmt__MonthlyTotal__c, vlocity_cmt__OneTimeTotal__c,
                    vlocity_cmt__ParentItemId__c , vlocity_cmt__JSONAttribute__c,
                    contract_request__c, contract_request__r.Name,Contract_Action__c,
                    vlocity_cmt__RootItemId__c, vlocity_cmt__Product2Id__c, Order.ParentId__c
            FROM OrderItem
            WHERE OrderId in :orderIdList
            ORDER BY vlocity_cmt__LineNumber__c
        ];
        }
         
        if(orderItemsList != null){
            for(OrderItem oli: orderItemsList){
                List<OrderITem> tmpList = new List<OrderItem>();
                if(orderItemMap.get(oli.OrderId) != null){
                    tmpList = orderItemMap.get(oli.OrderId);
                }
                tmpList.add(oli);
                orderItemMap.put(oli.OrderId, tmpList);
            }
        }
        for(Order orderObj:newOrderList){
            if(orderItemMap.get(orderObj.Id) != null && orderObj.ParentId__c != null){
                Map<Id, List<OrderItem>> tmpMap = new Map<ID, List<OrderItem>>();
                orderMap.put(orderObj.ParentId__c, tmpMap);
            }
        }
        if(orderMap != null && orderMap.size()>0){
            childOrderStatusMap = getChildOrderStatus(orderMap);
        }
        // End BMPF-20
        
        for(Order orderObj:newOrderList) { 
        String currentUserName = UserInfo.getName();
        if(String.isNotBlank(currentUserName)&& currentUserName.equalsIgnoreCase('Application TOCP')){
        //Modified the following block for the defect 10596
        if(isUpdate && isBefore){               
            if(String.isNotBlank(orderObj.orderMgmtId_Status__c)){
                
                //Map<Id,Order> currentIdOrdrMap=Trigger.oldMap;
                Map<Id,Order> currentIdOrdrMap=oldOrderMap;
                String currentOrdrStatusCat=currentIdOrdrMap.get(orderObj.id).statuscode;
                Boolean changeStatus=(String.isNotBlank(currentOrdrStatusCat) && 'A'.equalsIgnoreCase(currentOrdrStatusCat));
                String extOrderStatus=orderObj.orderMgmtId_Status__c;
                    if(String.isNotBlank(extOrderStatus)){
                        extOrderStatus=extOrderStatus.trim();
                    }
                String previousOrdrStatus=currentIdOrdrMap.get(orderObj.id).status;
                String currentOrderStatus=(orderObj.status==null?'':orderObj.status);
                system.debug('!! Order Trigger 1 '+currentOrderStatus +' currentOrdrStatusCat '+currentOrdrStatusCat + '  changeStatus '+changeStatus + ' extOrderStatus '+extOrderStatus);
                
                if(!currentOrderStatus.equals('Not Submitted')){
                    system.debug('!! Order Trigger 2 '+extOrderStatus);
                    if(changeStatus && extOrderStatus.equalsIgnoreCase(OrdrConstants.NC_ORDR_ENTERING_STATUS)){
                        orderObj.status=OrdrConstants.ORDR_ENTERING_STATUS;
                    }else if(changeStatus && extOrderStatus.equalsIgnoreCase(OrdrConstants.NC_ORDR_SUBMITTING_STATUS)){
                        orderObj.status=OrdrConstants.ORDR_SUBMITTED_STATUS;
                    }else if(changeStatus && extOrderStatus.equalsIgnoreCase(OrdrConstants.NC_ORDR_PROCESSING_STATUS)){
                        // BMPF-20
                        if(childOrderStatusMap != null && childOrderStatusMap.get(orderObj.parentId__c) != null){
                            Map<Id,String> tmpStatusMap = childOrderStatusMap.get(orderObj.parentId__c);
                            if(tmpStatusMap != null && tmpStatusMap.get(orderObj.Id) != null){
                                orderObj.status=tmpStatusMap.get(orderObj.Id);
                            }else{
                                orderObj.status=OrdrConstants.ORDR_INPROGRESS_STATUS;
                            }
                        }else{
                            orderObj.status=OrdrConstants.ORDR_INPROGRESS_STATUS;
                        } // End BMPF-20
                    }else if(changeStatus && extOrderStatus.equalsIgnoreCase(OrdrConstants.NC_ORDR_PROCESSED_STATUS)){
                        orderObj.status=OrdrConstants.ORDR_COMPLETED_STATUS;
                    }else if(changeStatus && extOrderStatus.equalsIgnoreCase(OrdrConstants.NC_ORDR_ERROR_STATUS)){
                        orderObj.status=OrdrConstants.ORDR_ERROR_STATUS;
                    }else if(changeStatus && extOrderStatus.equalsIgnoreCase(OrdrConstants.NC_ORDR_PONR_STATUS)){
                            orderObj.status=OrdrConstants.ORDR_COMPLETED_STATUS;
                        }
                    }
                    if(currentOrderStatus.equals('Submitted') || currentOrderStatus.equals('Activated')){
                        if(String.isNotBlank(previousOrdrStatus) && (previousOrdrStatus.equals(OrdrConstants.ORDR_ENTERING_STATUS)||previousOrdrStatus.equals(OrdrConstants.ORDR_SUBMITTED_STATUS)||previousOrdrStatus.equals(OrdrConstants.ORDR_INPROGRESS_STATUS)||previousOrdrStatus.equals(OrdrConstants.ORDR_COMPLETED_STATUS)||previousOrdrStatus.equals(OrdrConstants.ORDR_ERROR_STATUS))){
                            orderObj.status=previousOrdrStatus;
                        }
                    }
                    String previousNCOrdrStatus=currentIdOrdrMap.get(orderObj.id).orderMgmtId_Status__c;
                    if(String.isNotBlank(previousNCOrdrStatus) && previousNCOrdrStatus.equalsIgnoreCase(OrdrConstants.ORDR_CANCEL_TIMEOUT_ERROR) && (extOrderStatus.equalsIgnoreCase(OrdrConstants.ORDR_SUSPENDED_STATUS) ||  extOrderStatus.equalsIgnoreCase(OrdrConstants.ORDR_SUPERSEDED_STATUS))){
                       orderObj.status=OrdrConstants.ORDR_CANCELLED_STATUS;
                    }
                }
            }
        }        
        if (isUpdate) {
            if (orderObj.status=='Activated') {
                orderObj.status='Submitted';
            }
        }
     if (isUpdate && isAfter) {
         if(String.isNotBlank(currentUserName)&& currentUserName.equalsIgnoreCase('Application TOCP')){
             for(Order orderObjold : newOrderList ) {
                If(orderObjold.status == OrdrConstants.ORDR_ERROR_STATUS){
                    OCOM_OrderErrorStatusHelper.UpdateErrorStatusOnOli(OrderObj);
                }
           }
         }
      }
    }
    smb_Order_helper objHelper = new smb_Order_helper();
    objHelper.statusChange(newOrderList);
    }
    
    //BMPF -18 Mini PI
    public static void updateMasterOrderStatus(List<Order> newOrderList){
        Set<Id> parentIds = new Set<Id>();
        Map<Id, String> statusMap = new Map<Id,String>();
        Map<Id, List<Order>> childOrderMap = new Map<Id,List<Order>>();
        Map<String, OC_MasterOrderStatusPriorityMapping__mdt> statusSeqMap = new Map<String,OC_MasterOrderStatusPriorityMapping__mdt>();
        if(listStatusSeq==null){
            listStatusSeq = [SELECT DeveloperName, Sequence__c, MasterLabel, Master_Order_Status__c FROM OC_MasterOrderStatusPriorityMapping__mdt];
        }         
        for(OC_MasterOrderStatusPriorityMapping__mdt mospObj: listStatusSeq){
            statusSeqMap.put(mospObj.MasterLabel, mospObj);
        }
        for(Order orderObj: newOrderList){
            if(orderObj.ParentId__c != null){
                parentIds.add(orderObj.ParentId__c);
            }   
        }
        if(allOrders==null || allOrders.isEmpty()){
            allOrders=[Select Status, ParentId__c From Order Where ParentId__c IN: parentIds or Id IN: parentIds];
        }
        //List<Order> allOrders=[Select Status, ParentId__c From Order Where ParentId__c IN: parentIds or Id IN: parentIds];
       // List<Order> allChildOrders = [Select Status, ParentId__c From Order Where ParentId__c IN: parentIds];
       // List<Order> allMasterOrders = [Select Status, Master_Order_Status__c From Order Where Id IN: parentIds];
       List<Order> allChildOrders=new List<Order>();
        List<Order> allMasterOrders=new List<Order>();
        for(Order ordObj:allOrders){
            if(parentIds.isEmpty()){continue;}
            if(String.isNotBlank(ordObj.ParentId__c) && parentIds.contains(ordObj.ParentId__c)){
                allChildOrders.add(ordObj);
            }
            if( parentIds.contains(ordObj.id)){
                allMasterOrders.add(ordObj);
            }
        }
        for(Order orderObj: allChildOrders){
            List<Order> tmpOrders;
            if(childOrderMap.get(orderObj.ParentId__c) != null){
                tmpOrders = childOrderMap.get(orderObj.ParentId__c);
            }else{
                tmpOrders = new List<Order>();
            }
            tmpOrders.add(orderObj);
            childOrderMap.put(orderObj.ParentId__c, tmpOrders);
        }
        
        if(allMasterOrders != null){
            for(Order parentObj: allMasterOrders){
                if(childOrderMap.get(parentObj.Id) != null){
                    List<Order> childOrders = childOrderMap.get(parentObj.Id);
                    List<MasterStatusOrder> statusList = new List<MasterStatusOrder>();
                    for(Order orderObj: childOrders){
                        if(orderObj.status != null){
                            if(statusSeqMap.get(orderObj.Status)!=null){
                                Integer sequence = 1;
                                String status = orderObj.Status;
                                OC_MasterOrderStatusPriorityMapping__mdt mospObj = statusSeqMap.get(orderObj.Status);
                                if(mospObj.Sequence__c != null){
                                    sequence = Integer.valueOf(mospObj.Sequence__c);
                                }
                                if(!String.isBlank(mospObj.Master_Order_Status__c)){
                                    status = mospObj.Master_Order_Status__c;
                                }
                                MasterStatusOrder statusObj = new MasterStatusOrder(status,sequence);
                                statusList.add(statusObj);
                            }
                        }
                    }
                    if(statusList != null && statusList.size()>0){
                        statusList.sort();
                        parentObj.Master_Order_Status__c = statusList[0].status;
                    }
                }
            }
            //This code needs to be changed.
            update allMasterOrders;
        }
    }
    
    //BMPF -20 Mini PI
    public static Map<Id, Map<Id,String>> getChildOrderStatus(Map<Id, Map<Id, List<OrderItem>>> masterOrderMap){
        Map<Id,Map<Id,String>> statusMap = new Map<Id,Map<Id,String>>();
        Map<Id,Set<Id>> orderMap = new Map<Id,Set<Id>>();
        for (String masterKey : masterOrderMap.keySet()){
            Map<Id, List<OrderItem>> childOrderMap = masterOrderMap.get(masterKey);
            Set<Id> childOrderIdSet = childOrderMap.keySet();
            orderMap.put(masterKey, childOrderIdSet);
        }
        Map<Id, Map<Id,List<OrderItem>>> contractableOrderItems = getContractableOrderItems(masterOrderMap);
        Map<Id, Map<Id,List<OrderItem>>> decomposedOrderMap = getDecomposedOrders(masterOrderMap);
        for (Id masterOrderId: orderMap.keySet()){
            set<Id> childOrderSet = orderMap.get(masterOrderId);
            Map<Id, String> childStatusMap = new Map<Id,String>();
            if(statusMap.get(masterOrderId) != null){
                childStatusMap = statusMap.get(masterOrderId);
            }
            
            if(childOrderSet != null){
                Boolean isMultiSite = false;
                Boolean isContractable = false;
                Boolean isDecomposed = false;
                if(childOrderSet.size()>1){
                    isMultiSite = true;
                }
                if(contractableOrderItems.get(masterOrderId)!=null){
                    isContractable = true;
                }
                if(decomposedOrderMap.get(masterOrderId)!=null){
                    isDecomposed = true;
                }
                for (Id childOrderId: childOrderSet){
                    
                    String status = OrdrConstants.ORDR_SUBMITTING_STATUS;
                    if(isContractable){
                        if(isMultiSite){
                            status = OrdrConstants.ORDR_INPROGRESS_STATUS;
                        }else{
                            status = OrdrConstants.ORDR_PENDING;
                        }
                    }else{
                        if(isDecomposed){
                            if(isMultiSite){
                                status = OrdrConstants.ORDR_INPROGRESS_STATUS;
                            }else{
                                status = OrdrConstants.ORDR_SUBMITTING_STATUS;
                            }
                        }else{
                            if(isMultiSite){
                                status = OrdrConstants.ORDR_INPROGRESS_STATUS;
                            }else{
                                status = OrdrConstants.ORDR_INPROGRESS_STATUS;
                            }
                        }
                    }
                    childStatusMap.put(childOrderId, status);
                    statusMap.put(masterOrderId, childStatusMap);
                }
            }
        }
        return statusMap;
    }
    
    //BMPF -20 Mini PI
    public static Map<Id, Map<Id, List<OrderItem>>> getDecomposedOrders(Map<Id, Map<Id, List<OrderItem>>> masterOrderMap){
        Map<Id, Map<Id, List<OrderItem>>> decomposedOrderMap = new Map<Id, Map<Id, List<OrderItem>>>();
        for (String masterKey : masterOrderMap.keySet()) {
            Map<Id, List<OrderItem>> tmpChildOrderMap = masterOrderMap.get(masterKey);
            for(String childKey : tmpChildOrderMap.keySet()) {
                List<OrderItem> orderItems = tmpChildOrderMap.get(childKey);
                Map<Id, List<OrderItem>> childOrderMap = new Map<Id, List<OrderItem>>();
                for (OrderItem oli: orderItems) {
                    if(decomposedOrderMap.get(oli.Order.ParentId__c) != null){
                        childOrderMap = decomposedOrderMap.get(oli.Order.ParentId__c);
                    }
                    if(oli.orderMgmtId_Status__c != null){
                        List<OrderItem> orderItemList = new List<OrderItem>();
                        if(childOrderMap.get(oli.OrderId)!= null){
                            orderItemList=childOrderMap.get(oli.OrderId);
                        }
                        orderItemList.add(oli);
                        childOrderMap.put(oli.OrderId, orderItemList);
                        decomposedOrderMap.put(oli.Order.ParentId__c, childOrderMap);
                    }
                }
            }
        }
        return decomposedOrderMap;
    }
    
    //BMPF -20 Mini PI
    public static Map<Id, Map<Id,List<OrderItem>>> getContractableOrderItems(Map<Id, Map<Id, List<OrderItem>>> orderMap){
        Map<Id, Map<Id,List<OrderItem>>> contractableOrderItems = new Map<Id, Map<Id,List<OrderItem>>>();
        Map<String, Boolean> contractRulesMap = new Map<String, Boolean>();
        Map<String, ContractableProduct> contractableProductsMap = new Map<String, ContractableProduct>();
        for (Contract_Rule__mdt rule : [select Id, DeveloperName, Is_Contract_Required_on_MTM__c from Contract_Rule__mdt where Contract_Record_Type__c = 'Contract']) {
            contractRulesMap.put(rule.DeveloperName, rule.Is_Contract_Required_on_MTM__c);
        }
        
        for (vlocity_cmt__CatalogProductRelationship__c cpr : [Select id, vlocity_cmt__Product2Id__c, vlocity_cmt__CatalogId__r.Name From vlocity_cmt__CatalogProductRelationship__c where vlocity_cmt__CatalogId__r.Name in :contractRulesMap.keySet()]) {
            ContractableProduct cp = new ContractableProduct();
            cp.category = cpr.vlocity_cmt__CatalogId__r.Name;
            cp.contractableTerm = contractRulesMap.get(cpr.vlocity_cmt__CatalogId__r.Name) ? 0 : 1;
            cp.productId = cpr.vlocity_cmt__Product2Id__c;
            contractableProductsMap.put(cpr.vlocity_cmt__Product2Id__c, cp);
        }
        
        ContractableProduct cp;
        for (Id masterKey : orderMap.keySet()) {
            Map<Id, List<OrderItem>> tmpChildOrderMap = orderMap.get(masterKey);
            for (Id childkey : tmpChildOrderMap.keySet()){
                List<OrderItem> oliList = tmpChildOrderMap.get(childkey);
                Map<Id, List<OrderItem>> childOrderMap = new Map<Id, List<OrderItem>>();
                Map<Id, OrderItem> oliMap = new Map<Id, OrderItem>();
                for (OrderItem oli: oliList){
                    oliMap.put(oli.Id, oli);
                }
                for (OrderItem oli: oliList){
                    if(contractableOrderItems.get(oli.Order.ParentId__c) != null){
                        childOrderMap = contractableOrderItems.get(oli.Order.ParentId__c);
                    }
                    //if top offer reset Contractable Product Object
                    if (oli.vlocity_cmt__ParentItemId__c == null) {
                        cp = null;
                    }
                    if (contractableProductsMap.containsKey(oli.vlocity_cmt__Product2Id__c)) {
                        cp = contractableProductsMap.get(oli.vlocity_cmt__Product2Id__c);
                    }
                    //Check for contract term
                    String termValStr=oli.Term__c;
                    if(Test.isRunningTest()){termValStr='36 months';}
                    if(String.isNotBlank(termValStr)){
                       termValStr=termValStr.trim();
                       List<String> s1List = termValStr.split(' ');
                       if(s1List.size()>0){
                         termValStr=s1List.get(0);
                       }              
                     }
                    
                    if (cp != null
                            && String.isNotBlank(termValStr) 
                            && Integer.valueOf(termValStr) >= cp.contractableTerm 
                            && oliMap.containsKey(oli.vlocity_cmt__RootItemId__c)) {
                        
                        List<OrderItem> childOrderItem = new List<OrderItem>();
                        if(childOrderMap.get(oli.OrderId) != null){
                            childOrderItem = childOrderMap.get(oli.OrderId);
                        }
                        if(String.isBlank(oli.Contract_Action__c) || (!'Change_Amend'.equalsIgnoreCase(oli.Contract_Action__c) && !'Change_Replace'.equalsIgnoreCase(oli.Contract_Action__c))){
                            if(String.isBlank(oli.contract_request__c)){
                                childOrderItem.add(oli);
                                childOrderMap.put(oli.OrderId, childOrderItem);
                                contractableOrderItems.put(oli.Order.ParentId__c, childOrderMap);
                            }
                        }
                    } 
                }
            }
        }
        return contractableOrderItems;
    }
    
    public class MasterStatusOrder implements Comparable {
        String status;
        Integer sequence;
        public MasterStatusOrder(){}
        public MasterStatusOrder(String statusP, Integer sequenceP){
            status=statusP;
            sequence=sequenceP;
        } 
        // Implement the compareTo() method
        public Integer compareTo(Object compareTo) {
            MasterStatusOrder compareToEmp = (MasterStatusOrder)compareTo;
            Integer returnValue = 0;
            if (sequence > compareToEmp.sequence){
                returnValue = 1;
            }else if(sequence < compareToEmp.sequence){
                returnValue = -1;
            }
            return returnValue;      
        }
    }
    
    public class ContractableProduct {
        public String category;
        public Integer contractableTerm;
        public String productid;
    }
}