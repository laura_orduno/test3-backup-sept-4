global class batch_SyncParentChildAccountFields implements Database.Batchable<sObject>
{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {  
        String query = 
            ' SELECT ' +
            	' ownerId, sub_segment_code__c, Wireline_PIN__c, Wireless_PIN__c, ' +  	// set of account fields to be potentially updated 
            	' Account_Owner_Synched_With_Parent__c, ' +        				   		// (BAN/CAN) formula field that checks if the account's paternal RCID's ownerId is the same (True/False)            
            	' Account_Sub_Segment_Synched_With_Parent__c, ' +  				 		// (RCID) formula field that checks if the account's paternal CBUCID's sub_segment_code__c is the same (True/False)
            	' Account_Master_PIN_Synched_With_Parent__c, ' +   						// (BAN/CAN) formula field that checks if the account's paternal RCID's Master PIN is the same (True/False)            
            	' Account_Support_PIN_Synched_With_Parent__c, ' +  						// (BAN/CAN) formula field that checks if the account's paternal RCID's Support is the same (True/False)            
            	' Paternal_RCID_Owner_Id__c, ' +                   						// (BAN/CAN) formula field that returns the ownerId value of the account's paternal RCID
            	' Paternal_CBUCID_Sub_Segment_Code__c, ' + 		   						// (RCID) formula field that returns the sub_segment_code__c value of the account's paternal RCID
            	' Paternal_RCID_Master_PIN__c, ' + 		   		   						// (BAN/CAN) formula field that returns the Paternal_RCID_Master_PIN__c value of the account's paternal RCID
            	' Paternal_RCID_Support_PIN__c ' + 		           						// (BAN/CAN) formula field that returns the Paternal_RCID_Support_PIN__c value of the account's paternal RCID
            ' FROM ' + 
            	' Account ' + 
            ' WHERE ' +             
            	' parentId != null ' + 
            	' AND ' + 
            	' ( ' +
            		' ( Account_Owner_Synched_With_Parent__c = False AND Paternal_RCID_Owner_Id__c != null ) ' +
            		' OR ' +
            		' ( Account_Sub_Segment_Synched_With_Parent__c = False AND Paternal_CBUCID_Sub_Segment_Code__c != null ) ' + 
/*****            
            		' OR ' +
            		' ( Account_Master_PIN_Synched_With_Parent__c = False ) ' + 
            		' OR ' +
            		' ( Account_Support_PIN_Synched_With_Parent__c = False ) ' +
*/
            	' )' + 
            ' LIMIT 250000';
        
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Account> scope)
    { 
        List<Account> updateList = new List<Account>();

        for(Account a : scope)
        {                           
            Boolean addToUpdateList = false;
            
            if(!a.Account_Owner_Synched_With_Parent__c)
            {
                a.ownerId = a.Paternal_RCID_Owner_Id__c;
                addToUpdateList = true;                                    
            }
              
            if(!a.Account_Sub_Segment_Synched_With_Parent__c)
            {
                a.sub_segment_code__c = a.Paternal_CBUCID_Sub_Segment_Code__c;                
                addToUpdateList = true;
            }
            
            if(!a.Account_Master_PIN_Synched_With_Parent__c)
            {
                a.Wireline_PIN__c = a.Paternal_RCID_Master_PIN__c;
                addToUpdateList = true;
            }

            if(!a.Account_Support_PIN_Synched_With_Parent__c)
            {
                a.Wireless_PIN__c = a.Paternal_RCID_Support_PIN__c;
                addToUpdateList = true;
            }
            
            if(addToUpdateList)
            {
                updateList.add(a);
            }
        }
        
        if(updateList.size() > 0)
        {
            update updateList;
        }        
    }
        
    global void finish(Database.BatchableContext BC)
    {
    }
}