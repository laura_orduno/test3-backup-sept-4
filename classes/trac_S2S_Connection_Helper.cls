/**
* @Author: David Barrera
*@Description: Class that will assist in getting information of the Partner Network Connection Object.
*  
*/

public without sharing class trac_S2S_Connection_Helper {
	
	/**
	* @Author: David Barrera -Traction On Demand (dbarrera@tractionondemand.com)
	*@Description: Helper function that will return the Id of a connection based on the name being passed.
	*  
	*/
	//Change to get rid of 'getpnc' and place logic into the method below.
	public static PartnerNetworkConnection getRingCentralConnectionId(String connectionName){
		if( connectionName.length() <= 0 ){
				return null;
		}
		List<PartnerNetworkConnection> pncList = [ SELECT createdById, Id 
													FROM PartnerNetworkConnection 
					   								WHERE connectionStatus = 'Accepted'  
					   								AND connectionName = :connectionName 
					   								LIMIT 1
					   								];

		if( pncList.size() > 0 ){
			return pncList.get(0);
		}			   
		return null;
	}
	/**
	* @Author: David Barrera -Traction On Demand (dbarrera@tractionondemand.com)
	*@Description: Function that will send the a list of objects to a given connection name, and also with a relatedRecordsMap, parentRecordIdMap.
	*  @Params: 
	*/
	public static void sendToRingCentral( List<sObject> objectList,
										  Map<Id,Id> parentRecordIdMap,
										  Map<Id,String> relatedRecordsMap,
										  String connectionName,
										  Boolean closedTasks,
										  Boolean openTasks,
										  Boolean emails){

		if ( objectList == null && objectList.size() < 0 ){
			return;
		}

		PartnerNetworkConnection rcConnection = getRingCentralConnectionId( connectionName );
		List<PartnerNetworkRecordConnection> objectConnections = 
							new List<PartnerNetworkRecordConnection>();
		if (rcConnection != null ){
			for (sObject obj: objectList ){				
				objectConnections.add( new PartnerNetworkRecordConnection( ConnectionId = rcConnection.Id,
																		LocalRecordId = obj.Id,
																		
																		ParentRecordId = parentRecordIdMap != null ?
																						 parentRecordIdMap.get( obj.Id ):	
																						 null,
																		RelatedRecords = relatedRecordsMap != null ?
																						 relatedRecordsMap.get(obj.Id):
																						 null,
																		SendClosedtasks = closedTasks,
																		SendOpentasks = openTasks,
																		SendEmails = emails
				));
			}
		}
		System.Debug('Connection objects size: ' + objectConnections.size());
		if( objectConnections.size() > 0 ){
			try{
				System.Debug('Upsering objectConnections...');
				database.upsert(objectConnections);
			}
			catch( DmlException dmlEx ){
                System.Debug('There was a problem update the object to the RC opr: ' + dmlEx.getMessage() );
            }
        }
    }
}