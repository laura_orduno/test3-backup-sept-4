/*****************************************************
    Name: ExternalEventService_RS_v1
    Usage: This class used to expose rest call to external systems to create one event in salesforce per call
    Author – Sandip Chaudhari (IBM)
    Revision History -
    Created By/Date - Sandip Chaudhari / 04 July 2016
    
    Updated By/Date/Reason for Change - 
    
******************************************************/
@RestResource(urlMapping='/CreateEvent/*')
global class SFDCExternalEventRSService_v1_0{
   
    @HttpPost 
    global static ExternalEventRequestResponseParameter.ResponseEventData createEvent(ExternalEventRequestResponseParameter.RequestEventData eventData){
       List<ExternalEventRequestResponseParameter.RequestEventData> requestList = new List<ExternalEventRequestResponseParameter.RequestEventData>();
       requestList.add(eventData);
       List<ExternalEventRequestResponseParameter.ResponseEventData> responseData =  ExternalEventService_Helper.processEvent(requestList);
       //if(responseData != null && responseData.size() > 0){
           return responseData[0];
       //}else{
       //    return null;
       //}
    }

}