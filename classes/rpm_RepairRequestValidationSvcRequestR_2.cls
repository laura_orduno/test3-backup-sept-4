public class rpm_RepairRequestValidationSvcRequestR_2 {
    public class validateRepairRequest_element {
        public String defectiveSerialNumber;
        public String defectiveTelusSku;
        public String repairTypeCd;
        public String telephoneNumber;
        public String locale;
        private String[] defectiveSerialNumber_type_info = new String[]{'defectiveSerialNumber','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] defectiveTelusSku_type_info = new String[]{'defectiveTelusSku','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] repairTypeCd_type_info = new String[]{'repairTypeCd','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] telephoneNumber_type_info = new String[]{'telephoneNumber','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] locale_type_info = new String[]{'locale','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/srv/RMO/LifeCycleMgmt/RepairRequestValidationSvcRequestResponse_v3','false','false'};
        private String[] field_order_type_info = new String[]{'defectiveSerialNumber','defectiveTelusSku','repairTypeCd','telephoneNumber','locale'};
    }
    public class defectiveSummary_element {
        public String defectTelusSku;
        public DateTime doaExpiryDate;
        public String warrantyStatusCd;
        public DateTime warrantyExpiryDate;
        public rpm_EnterpriseCommonTypes_v7.MultilingualNameList productName;
        public rpm_CustomerExternalViewCommon_v6.Name clientName;
        public rpm_TelusCommonAddressTypes_v5.CivicAddress clientAddress;
        private String[] defectTelusSku_type_info = new String[]{'defectTelusSku','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] doaExpiryDate_type_info = new String[]{'doaExpiryDate','http://www.w3.org/2001/XMLSchema','dateTime','0','1','false'};
        private String[] warrantyStatusCd_type_info = new String[]{'warrantyStatusCd','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] warrantyExpiryDate_type_info = new String[]{'warrantyExpiryDate','http://www.w3.org/2001/XMLSchema','dateTime','0','1','false'};
        private String[] productName_type_info = new String[]{'productName','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v7','MultilingualNameList','0','1','false'};
        private String[] clientName_type_info = new String[]{'clientName','http://xmlschema.tmi.telus.com/xsd/Customer/Customer/CustomerExternalViewCommon_v6','Name','0','1','false'};
        private String[] clientAddress_type_info = new String[]{'clientAddress','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/TelusCommonAddressTypes_v5','CivicAddress','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/srv/RMO/LifeCycleMgmt/RepairRequestValidationSvcRequestResponse_v3','false','false'};
        private String[] field_order_type_info = new String[]{'defectTelusSku','doaExpiryDate','warrantyStatusCd','warrantyExpiryDate','productName','clientName','clientAddress'};
    }
    public class validateRepairRequestResponse_element {
        public String validationStatus;
        public rpm_EnterpriseCommonTypes_v7.ValidationMessageList validationMessageList;
        public rpm_RepairRequestValidationSvcRequestR_2.defectiveSummary_element defectiveSummary;
        private String[] validationStatus_type_info = new String[]{'validationStatus','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] validationMessageList_type_info = new String[]{'validationMessageList','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v7','ValidationMessageList','0','1','false'};
        private String[] defectiveSummary_type_info = new String[]{'defectiveSummary','http://xmlschema.tmi.telus.com/srv/RMO/LifeCycleMgmt/RepairRequestValidationSvcRequestResponse_v3','defectiveSummary_element','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/srv/RMO/LifeCycleMgmt/RepairRequestValidationSvcRequestResponse_v3','false','false'};
        private String[] field_order_type_info = new String[]{'validationStatus','validationMessage','defectiveSummary'};
    }
}