@isTest(SeeAllData=false)
public class BulkLoad_Test {
/*
	@isTest static void test1() {
        Bulk_Order_Load_File__c bl = BulkLoadUtils.createBulkLoad('Bulkload_Test_SR_CSV');
        
        BulkLoadMaster.validateBulkOrderLoad(bl.id);
        BulkLoadMaster.commitBulkOrderLoad(bl.id);
        BulkLoadMaster.submitBulkOrderLoad(bl.Opportunity__c);
    }
*/
	@isTest static void test2() {
        
        Test.startTest();
     
        BulkLoadUtils.createSMBCareWebServicesCustomSetting();
        
//        BulkLoadMaster.validateBulkOrderLoad(bl.id);
//        BulkLoadMaster.commitBulkOrderLoad(bl.id);
//        BulkLoadMaster.submitBulkOrderLoad(bl.Opportunity__c);
  
        
//        Bulk_Order_Load_File__c bl = BulkLoadUtils.createBulkLoad('Bulkload_Test_SR_CSV');
        Bulk_Order_Load_File__c bl = BulkLoadUtils.createBulkLoad('Bulkload_Test_SR_CSV_Large');

System.debug('BulkLoad: bl: ' + bl);        
        
        BulkLoadMaster.testParameters();
        BulkLoadMaster.validateBulkOrderLoad(bl.id);
        BulkLoadMaster.commitBulkOrderLoad(bl.id);
        
		BulkOrderLoadRecordConverter blrc = new BulkOrderLoadRecordConverter(userinfo.getsessionid(), bl.id);    	
        blrc.conversionPerJob = 1;
/*        
        CommitBulkOrderLoad.DataMappingUtil dataMappingUtil=new CommitBulkOrderLoad.DataMappingUtil();
        service_request__c serviceRequest=dataMappingUtil.createServiceRequest(blrc);
        List<service_request__c> newServiceRequests = new List<service_request__c>();
        newServiceRequests.add(serviceRequest);
        insert newServiceRequests;
*/       
		Opportunity opp = [SELECT id, AccountId FROM Opportunity WHERE id=:bl.opportunity__c];
System.debug('BulkLoad: opp: ' + opp);        
		service_request__c serviceRequest = BulkLoadUtils.createServiceRequest(opp.id, opp.accountId);
        insert serviceRequest;
 
System.debug('BulkLoad: serviceRequest: ' + serviceRequest);    
        
        Id oppId = opp.id;	
        list<service_request__c> serviceRequests=database.query('select id,Fox_Submission_In_Progress__c,reason_to_send_code__c,recordtype.name from service_request__c where opportunity__c=:oppId and service_request_status__c=\'Ready to Submit\' and Fox_Submission_In_Progress__c=false limit 50000 for update');
//        list<service_request__c> serviceRequests=database.query('select id,opportunity__c, Status__c, Fox_Submission_In_Progress__c,reason_to_send_code__c,recordtype.name from service_request__c limit 10 for update');
//        list<service_request__c> serviceRequests=database.query('select id,opportunity__c, Status__c, Fox_Submission_In_Progress__c,reason_to_send_code__c,recordtype.name from service_request__c where opportunity__c=:oppId limit 10 for update');
System.debug('BulkLoad: SR CHECK??, SRs: ' + serviceRequests);        
        
        List<Id> newServiceRequestIds = new List<Id>();
        newServiceRequestIds.add(serviceRequest.id);

        
    	BulkLoadMaster.distributeSubmitToFox(newServiceRequestIds);
        
        
        SubmitBulkOrderLoad sbol1 = new SubmitBulkOrderLoad(userinfo.getsessionid(), bl.Opportunity__c, newServiceRequestIds);
        sbol1.testSR = serviceRequest;
System.debug('BulkLoad: sbol1.testSR: ' + sbol1.testSR);    
        sbol1.requestDistribution();
/*        
        sbol1.execute(null);
*/        
        
        queueablecontext qc;
        blrc.execute(qc);
        
System.debug('Bulkload: blrc.bulkOrderLoadRecordRows: ' + blrc.bulkOrderLoadRecordRows);        
System.debug('Bulkload: blrc.processBulkOrderLoadRecords()');        
//        blrc.processBulkOrderLoadRecords();

        BulkLoadMaster.bulkOrderLoadRecordsDistributor(bl.id, blrc.bulkOrderLoadRecordRows);
        BulkLoadMaster.commitBulkOrderLoad(bl.id);
        BulkLoadMaster.submitBulkOrderLoad(bl.Opportunity__c);
        
        list<schema.fieldsetmember> srCreationQueryFieldSetMembers=schema.sobjecttype.bulk_order_load_record__c.fieldsets.getmap().get('SR_Creation_Query').getfields();
        list<string> fieldnames=new list<string>();
        for(schema.fieldsetmember member:srCreationQueryFieldSetMembers){
            fieldnames.add(member.getfieldpath());
        }        
       
        Id bulkOrderLoadId = bl.id;
System.debug('Bulkload: bulkOrderLoadId: ' + bulkOrderLoadId);        
       
        bulk_order_load_file__c bulkOrderLoadFile = [select id, opportunity__r.accountid, submission_status__c, submission_error__c, total_records_in_template__c from bulk_order_load_file__c where id=:bulkOrderLoadId];        
        blrc.bulkOrderLoadFile = bulkOrderLoadFile;
        blrc.failProcessingBulkOrderLoad(new BulkLoadException('Failed'));
//        blrc.bulkOrderLoadFile = null;
        blrc.failProcessingBulkOrderLoad(null);
         
        
        
System.debug('Bulkload: bulkOrderLoadFile: ' + bulkOrderLoadFile);

        SMBCare_Address__c sr = FOBO_Test_Util.createserviceAddress();
		sr.FMS_Address_ID__c = '1234';
        update sr;
        
        Contact c = FOBO_Test_Util.createCOntact();
		c.accountId = bl.opportunity__r.accountid;
        c = [SELECT name FROM COntact LIMIT 1];        
System.debug('Bulkload: contact: ' + c);

        list<smbcare_address__c> smbcareAddresses=[select id, fms_address_id__c, Address_Type__c,Suite_Number__c,City__c,Status__c,
                                                   Street_Number__c,Province__c,Street_Name__c,Postal_Code__c,Street_Direction__c,
                                                   Google_Map_Link__c,Lot__c,Block__c,Plan__c,SRS_Quarter__c,Section__c,Township__c,
                                                   Range__c,Meridian__c,Subdivision_ID__c,SRS_Type__c,Number__c,Address__c,state__c,SRS_State__c 
                                                    from smbcare_address__c LIMIT 100];
        
System.debug('Bulkload: smbcareAddresses: ' + smbcareAddresses);
        
        
        
        bulk_order_load_record__c insertBulkOrderLoadRecord = 
            new bulk_order_load_record__c(bulk_order_load_file__c=bulkOrderLoadId, FMS_Address_ID__c = sr.FMS_Address_ID__c,  contact_name__c = c.name, template_row_nbr__c=1);
        insert insertBulkOrderLoadRecord;
        
        list<bulk_order_load_record__c> bulkOrderLoadRecordList = 
            [SELECT status__c, FMS_Address_ID__c, contact_name__c, contact_email__c, contact_phone__c, address__c, city__c, province__c, country__c, Postal_Code__c, 
             Order_Type__c, Additional_Service_Details__c, SR_Customer_Req_Date__c, template_row_nbr__c, bulk_order_load_file__r.opportunity__c, bulk_order_load_file__r.opportunity__r.accountid FROM bulk_order_load_record__c];        
        
System.debug('Bulkload: bulkOrderLoadRecordList: ' + bulkOrderLoadRecordList);        
        
        CommitBulkOrderLoad cbol = new CommitBulkOrderLoad(bulkOrderLoadRecordList);
        cbol.execute(qc);
            
        system.enqueuejob(blrc);

        Test.stopTest();
    }
}