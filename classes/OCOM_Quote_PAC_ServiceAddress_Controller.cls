public with sharing class OCOM_Quote_PAC_ServiceAddress_Controller {

    
    public boolean showOrhide {get;set;}
    public SMBCare_Address__c smbCareAddr {get; set;}    
    public ServiceAddressData SAData  { get; set; }
    public AddressData PAData  { get; set; }
    public quote Qttoupdate    { get; set; }
    ApexPages.StandardController stdCtrl;
    Public String actInProgress;
    Public String actDraft;
    
   
   public OCOM_Quote_PAC_ServiceAddress_Controller(ApexPages.StandardController controller){
       system.debug('###-in Const 2');
        stdCtrl = controller;
         if(!Test.isRunningTest()) {
       controller.addFields(new List<String> {'QuoteNumber'});
       }  
        Qttoupdate = (quote ) controller.getRecord();  
        system.debug('###-Qttoupdate in Const 2: ' + Qttoupdate );
        SAData  = new ServiceAddressData();
        SAData.isPostalCodeRequired = true;
        SAData.isSearchStringRequired = true;
        //Data  = new AddressData();
        //Data.isCityRequired = true;
        showOrhide=false;
        smbCareAddr = new SMBCare_Address__c();
        smbCareAddr.recordtypeid = Schema.SObjectType.SMBCare_Address__c.getRecordTypeInfosByName().get('Address').getRecordTypeId();
        
        system.debug('###-smbCareAddr in Const 2: ' + smbCareAddr);
   }
    
  
        public Pagereference Save()
        {
            OCOM_ServiceAddressControllerHelper svcAddrHelper = new OCOM_ServiceAddressControllerHelper();
            Quote Qttoupdate1 = svcAddrHelper.UpdateAddressAndQuote(Qttoupdate.id, smbCareAddr, SAData);      
                      
            try
            {
                Qttoupdate1.Maximum_Speed__c = '';
                update Qttoupdate1;
                Pagereference pref1= new pagereference('/'+Qttoupdate1.id);                         
                return pref1;               
            }                      
            catch(exception e)
            {
                return null;
            }         
   
      } 
      
             
        public PageReference doDeviceDetection() {

        PageReference result = null;

        Boolean isMobileDevice = false;
        String userAgent = ApexPages.currentPage().getHeaders().get('USER-AGENT');

        if( (userAgent.contains('iPhone')) || (userAgent.contains('iPad')) || (userAgent.contains('Android')) ) {
            isMobileDevice = true;
        }

        if( isMobileDevice ) {
           result = stdCtrl.view();
           result.getParameters().put('sfdc.override', '1');

        }

        return result;   
    }  
    public void showPanel()
    {showOrhide=true;}
     
}