/* 2015-12-12[Roderick de Vera]
 * Helper class which invokes the Web Service Callout for Product Technical Availability Check
 * Updated By Aditya Jamwal 31 May,2017  BB-209 
 */
public class OrdrTechnicalAvailabilityWsCallout {
    public static boolean TechAvailabilityCall;
    public static TpFulfillmentResourceOrderV3.ResourceOrder checkProductTechnicalAvailability(Map<String, Object> inputMap) {
        TpFulfillmentResourceOrderV3.ResourceOrder resourceOrder = new TpFulfillmentResourceOrderV3.ResourceOrder();
        try { 
            OrdrWsCheckGenericTechnicalAvailability.CheckGenericTechnicalAvailabilitySOAPPort servicePort = new OrdrWsCheckGenericTechnicalAvailability.CheckGenericTechnicalAvailabilitySOAPPort(); 
            servicePort = prepareCallout(servicePort);          
            resourceOrder = servicePort.checkGenericTechnicalAvailability(prepareTechnicalAvailabilityPayload(inputMap));      
        } 
        catch (CalloutException e) {

            OrdrUtilities.auditLogs('OrdrTechnicalAvailabilityWsCallout.checkProductTechnicalAvailability', 
                                    'NetCracker', 
                                    'CheckGenericTechnicalAvailability', 
                                    null, 
                                    null, 
                                    true, 
                                    e.getMessage(), 
                                    e.getStackTraceString());
            
            throw new OrdrExceptions.TechnicalAvailabilityServiceException(e.getMessage(), e);
        }
        return resourceOrder;
    }
    
    public static TpFulfillmentResourceOrderV3.ResourceOrder AsynCheckProductTechnicalAvailability(System.Continuation continuation, Map<String, Object> inputMap) {
        TpFulfillmentResourceOrderV3.ResourceOrder resourceOrder = new TpFulfillmentResourceOrderV3.ResourceOrder();
        OrdrWsCheckGenericTechnicalAvailability.TpFulfillmentResourceOrderV3Future responseResult;
        try { 
            OrdrWsCheckGenericTechnicalAvailability.AsynCheckGenericTechnicalAvailabilitySOAPPort servicePort = new OrdrWsCheckGenericTechnicalAvailability.AsynCheckGenericTechnicalAvailabilitySOAPPort(); 
            servicePort = asynPrepareCallout(servicePort);          
            responseResult = servicePort.beginCheckGenericTechnicalAvailability(continuation, prepareTechnicalAvailabilityPayload(inputMap));      
        } 
        catch (CalloutException e) {

            OrdrUtilities.auditLogs('OrdrTechnicalAvailabilityWsCallout.checkProductTechnicalAvailability', 
                                    'NetCracker', 
                                    'CheckGenericTechnicalAvailability', 
                                    null, 
                                    null, 
                                    true, 
                                    e.getMessage(), 
                                    e.getStackTraceString());
            
            throw new OrdrExceptions.TechnicalAvailabilityServiceException(e.getMessage(), e);
        }
        return resourceOrder;
    }
    
    public static TpFulfillmentResourceOrderV3.ResourceOrder checkTnPortability(String tnType, Map<String, Object> inputMap) {
        TpFulfillmentResourceOrderV3.ResourceOrder resourceOrder = new TpFulfillmentResourceOrderV3.ResourceOrder();
        try { 
            OrdrWsCheckGenericTechnicalAvailability.CheckGenericTechnicalAvailabilitySOAPPort servicePort = new OrdrWsCheckGenericTechnicalAvailability.CheckGenericTechnicalAvailabilitySOAPPort(); 
            servicePort = prepareCallout(servicePort);          
            resourceOrder = servicePort.checkGenericTechnicalAvailability(prepareTnPortabilityPayload(tnType, inputMap));      
        } 
        catch (CalloutException e) {
            
            OrdrUtilities.auditLogs('OrdrTechnicalAvailabilityWsCallout.checkTnPortability', 
                                    'NetCracker', 
                                    'CheckGenericTechnicalAvailability', 
                                    null, 
                                    null, 
                                    true, 
                                    e.getMessage(), 
                                    e.getStackTraceString());
            
            throw new OrdrExceptions.TechnicalAvailabilityServiceException(e.getMessage(), e);
        }
        return resourceOrder;
    }
    
    private static OrdrWsCheckGenericTechnicalAvailability.CheckGenericTechnicalAvailabilitySOAPPort 
        prepareCallout(OrdrWsCheckGenericTechnicalAvailability.CheckGenericTechnicalAvailabilitySOAPPort servicePort) {
            Ordering_WS__c technicalAvailabilityEndpoint = Ordering_WS__c.getValues('TechnicalAvailabilityEndpoint');           
            servicePort.endpoint_x = technicalAvailabilityEndpoint.value__c;
          //  'https://webservice1.preprd.teluslabs.net/RMO/ResourceMgmt/CheckGenericTechnicalAvailabilitySvc_v2_2_vs1_KIDC_IT03';//
            // Set SFDC Webservice call timeout
            servicePort.timeout_x = OrdrConstants.WS_TIMEOUT_MS;    
            
            if (servicePort.endpoint_x.startsWith('https://xmlgwy')) {
                Ordering_WS__c wsUserName =  Ordering_WS__c.getValues(OrdrConstants.CREDENTIAL_USERNAME);
                Ordering_WS__c wsPassword =  Ordering_WS__c.getValues(OrdrConstants.CREDENTIAL_PASSWORD); 
                String credentials = wsUserName.value__c + ':' + wsPAssword.value__c;
                String encodedUserNameAndPassword = EncodingUtil.base64Encode(Blob.valueOf(credentials));   
                
                servicePort.inputHttpHeaders_x = new Map<String, String>();
                servicePort.inputHttpHeaders_x.put('Authorization', 'Basic ' + encodedUserNameAndPassword);
            } else {
                servicePort.clientCertName_x = OrdrConstants.SDF_CERT_NAME;
            }
            
            return servicePort;
    } 
    
    //************************** Added by Aditya Jamwal BB-209
        public static OrdrWsCheckGenericTechnicalAvailability.AsynCheckGenericTechnicalAvailabilitySOAPPort 
            asynPrepareCallout(OrdrWsCheckGenericTechnicalAvailability.AsynCheckGenericTechnicalAvailabilitySOAPPort servicePort) {
                Ordering_WS__c technicalAvailabilityEndpoint = Ordering_WS__c.getValues('TechnicalAvailabilityEndpoint');           
                servicePort.endpoint_x = technicalAvailabilityEndpoint.value__c;
                // Set SFDC Webservice call timeout
                servicePort.timeout_x = OrdrConstants.WS_TIMEOUT_MS;    
                
                if (servicePort.endpoint_x.startsWith('https://xmlgwy')) {
                    Ordering_WS__c wsUserName =  Ordering_WS__c.getValues(OrdrConstants.CREDENTIAL_USERNAME);
                    Ordering_WS__c wsPassword =  Ordering_WS__c.getValues(OrdrConstants.CREDENTIAL_PASSWORD); 
                    String credentials = wsUserName.value__c + ':' + wsPAssword.value__c;
                    String encodedUserNameAndPassword = EncodingUtil.base64Encode(Blob.valueOf(credentials));   
                    
                    servicePort.inputHttpHeaders_x = new Map<String, String>();
                    servicePort.inputHttpHeaders_x.put('Authorization', 'Basic ' + encodedUserNameAndPassword);
                } else {
                    servicePort.clientCertName_x = OrdrConstants.SDF_CERT_NAME;
                }
                
                return servicePort;
        }
    
    //**************************
   //Updated to public by Aditya Jamwal BB-209
    public static TpFulfillmentResourceOrderV3.ResourceOrder prepareTechnicalAvailabilityPayload(Map<String, Object> inputMap) {
        TpFulfillmentResourceOrderV3.ResourceOrder resourceOrder = new TpFulfillmentResourceOrderV3.ResourceOrder();
        List<TpCommonBaseV3.CharacteristicValue> characteristicValues = new List<TpCommonBaseV3.CharacteristicValue>();
        List<TpFulfillmentResourceOrderV3.ResourceOrderItem> resourceOrderItems = new List<TpFulfillmentResourceOrderV3.ResourceOrderItem>();
        
        /*create resourceOrder/Specification section
        */
        TpCommonBaseV3.EntitySpecification specification = new TpCommonBaseV3.EntitySpecification();
        specification.Name = OrdrConstants.SPECIFICATION_NAME;
        specification.Type_x = OrdrConstants.SPECIFICATION_TYPE_AVAILABILITY;        
        specification.Category = OrdrConstants.SPECIFICATION_CATEGORY_LOGICAL_RESOURCE;
        resourceOrder.Specification = specification;
        
        /*create resourceOrder/CharacteristicValue/Characteristic/Name section
        */
        characteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.USER_ID, OrdrUtilities.getCurrentUserTelusId()));
        resourceOrder.CharacteristicValue = characteristicValues;
        
        /*create resourceOrder/ResourceOrderItem/Address/CharacteristicValue/Characteristic/Name section
        */
        TpFulfillmentResourceOrderV3.ResourceOrderItem resourceOrderItem = new TpFulfillmentResourceOrderV3.ResourceOrderItem();
        TpCommonUrbanPropertyAddressV3.UrbanPropertyAddress address = OrdrUtilities.constructAddress(inputMap);
        resourceOrderItem.Address = address;

        //create an empty Resource tag as per CGTA IA
        resourceOrderItem.Resource = new TpCommonBusinessInteractionV3.BusinessInteractionEntity();
        resourceOrderItems.add(resourceOrderItem);     
        
        resourceOrder.ResourceOrderItem = resourceOrderItems;
            
        
        return resourceOrder;
    }
    
    private static TpFulfillmentResourceOrderV3.ResourceOrder prepareTnPortabilityPayload(String tnType, Map<String, Object> inputMap) {
        TpFulfillmentResourceOrderV3.ResourceOrder resourceOrder = new TpFulfillmentResourceOrderV3.ResourceOrder();
        List<TpCommonBaseV3.CharacteristicValue> characteristicValues = new List<TpCommonBaseV3.CharacteristicValue>();
        List<TpFulfillmentResourceOrderV3.ResourceOrderItem> resourceOrderItems = new List<TpFulfillmentResourceOrderV3.ResourceOrderItem>();
        
        /*create resourceOrder/Specification section
        */
        TpCommonBaseV3.EntitySpecification specification = new TpCommonBaseV3.EntitySpecification();
        specification.Type_x = OrdrConstants.SPECIFICATION_TYPE_PORTABILITY;
        specification.Category = OrdrConstants.SPECIFICATION_CATEGORY_LOGICAL_RESOURCE;
        resourceOrder.Specification = specification;
        
        /*create resourceOrder/CharacteristicValue/Characteristic/Name section
        */
        characteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.USER_ID, OrdrUtilities.getCurrentUserTelusId()));
        DateTime ts = System.now();
        characteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.REQUEST_DATE, ts.format('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'')));
        resourceOrder.CharacteristicValue = characteristicValues;
        
        TpFulfillmentResourceOrderV3.ResourceOrderItem resourceOrderItem = new TpFulfillmentResourceOrderV3.ResourceOrderItem();
        
        if (tnType == OrdrConstants.WIRELINE) {
            /*create resourceOrder/ResourceOrderItem/Address/CharacteristicValue/Characteristic/Name section
            */
            TpCommonUrbanPropertyAddressV3.UrbanPropertyAddress address = constructAddress(inputMap);
            resourceOrderItem.Address = address;
        }

        //create resourceOrder/ResourceOrderItem/Resource/CharacteristicValue/Characteristic/Name section
        TpCommonBusinessInteractionV3.BusinessInteractionEntity resource = new TpCommonBusinessInteractionV3.BusinessInteractionEntity();
        characteristicValues = new List<TpCommonBaseV3.CharacteristicValue>();
        characteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.TN, (String)inputMap.get(OrdrConstants.TN)));
        if (tnType == OrdrConstants.WIRELESS) {
            characteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.PRODUCT_TYPE, (String)inputMap.get(OrdrConstants.PRODUCT_TYPE)));
            characteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.BRAND, (String)inputMap.get(OrdrConstants.BRAND)));
        }
        resource.CharacteristicValue = characteristicValues;
        resourceOrderItem.Resource = resource;

        resourceOrderItems.add(resourceOrderItem);     
        resourceOrder.ResourceOrderItem = resourceOrderItems;
            
        
        return resourceOrder;
    }    

    /*build address section of the Wireline TN Portability payload
     */
    public static TpCommonUrbanPropertyAddressV3.UrbanPropertyAddress constructAddress(Map<String, Object> inputMap) {
        TpCommonUrbanPropertyAddressV3.UrbanPropertyAddress address = new TpCommonUrbanPropertyAddressV3.UrbanPropertyAddress();  
        List<TpCommonBaseV3.CharacteristicValue> characteristicValues = new List<TpCommonBaseV3.CharacteristicValue>();
        Map<String, String> mandatoryFields = new Map<String, String>();
        mandatoryFields.put(OrdrConstants.FMS_ID, OrdrConstants.FMS_ID);
        mandatoryFields.put(OrdrConstants.ADDR_PROVINCE_CODE, OrdrConstants.ADDR_PROVINCE_CODE);
        mandatoryFields.put(OrdrConstants.ADDR_MUNICIPALITY, OrdrConstants.ADDR_MUNICIPALITY);
        mandatoryFields.put(OrdrConstants.ADDR_STREET_NAME, OrdrConstants.ADDR_STREET_NAME);
        mandatoryFields.put(OrdrConstants.ADDR_STREET_NUMBER, OrdrConstants.ADDR_STREET_NUMBER);
        
        //FMS Id is mandatory as per IA
        if (inputMap.containsKey(OrdrConstants.FMS_ID) && inputMap.get(OrdrConstants.FMS_ID) != null) {
            address.Id = (String)inputMap.get(OrdrConstants.FMS_ID);
            mandatoryFields.remove(OrdrConstants.FMS_ID);
        } 
        //Province Code is mandatory
        if (inputMap.containsKey(OrdrConstants.ADDR_PROVINCE_CODE) && inputMap.get(OrdrConstants.ADDR_PROVINCE_CODE) != null) {
            characteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.ADDR_PROVINCE_CODE, (String)inputMap.get(OrdrConstants.ADDR_PROVINCE_CODE)));
            mandatoryFields.remove(OrdrConstants.ADDR_PROVINCE_CODE);
        } 
        //Municipality is mandatory
        if (inputMap.containsKey(OrdrConstants.ADDR_MUNICIPALITY) && inputMap.get(OrdrConstants.ADDR_MUNICIPALITY) != null) {
            characteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.ADDR_MUNICIPALITY, (String)inputMap.get(OrdrConstants.ADDR_MUNICIPALITY)));
            mandatoryFields.remove(OrdrConstants.ADDR_MUNICIPALITY);
        } 
        //Street Name is mandatory
        if (inputMap.containsKey(OrdrConstants.ADDR_STREET_NAME) && inputMap.get(OrdrConstants.ADDR_STREET_NAME) != null) {
            characteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.ADDR_STREET_NAME, (String)inputMap.get(OrdrConstants.ADDR_STREET_NAME)));
            mandatoryFields.remove(OrdrConstants.ADDR_STREET_NAME);
        } 
        //Street Number is mandatory 
        if (inputMap.containsKey(OrdrConstants.ADDR_STREET_NUMBER) && inputMap.get(OrdrConstants.ADDR_STREET_NUMBER) != null) {
            characteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.ADDR_STREET_NUMBER, (String)inputMap.get(OrdrConstants.ADDR_STREET_NUMBER)));
            mandatoryFields.remove(OrdrConstants.ADDR_STREET_NUMBER);
        } 

        
        if (mandatoryFields.size() > 0) {
            String requiredFields = JSON.serialize(mandatoryFields.values());
            System.debug('requiredFields:' + requiredFields);
            String msg = Label.DFLT0004;
            msg = msg.replace('{0}', 'Wireline TN Portability');
            msg = msg.replace('{1}', requiredFields);
            throw new OrdrExceptions.InvalidParameterException(msg);                
        }
        
        address.CharacteristicValue = characteristicValues;
        return address;
         
    }
    
    
}