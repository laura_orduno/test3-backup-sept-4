/*******************
Author: Adharsh
Class Name: OCOM_CreateContractStep
Description: This class to used to create and update a Contract. It is called from the OS - Create Contract.
It also sends validation information to the OS like whether a contract document, work request exists for a particular contract.
********************/

global with sharing class OCOM_CreateContractStep implements vlocity_cmt.VlocityOpenInterface { 

    public Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {

        if(methodName == 'getContractDetails') {
        System.debug('////inputMap'+inputMap);  
            if(inputMap.ContainsKey('TypeofContract')){
                //Id contractRequestID 
                Map<string,object> ContractDetail = (Map<string,object>)inputMap.get('CreateTheContract');
                //system.debug('cpTest'+inputMap.get('CreateTheContract'));
                Map<String,Object> contractTypeNode = (Map<String,Object>) inputMap.get('TypeofContract');
                //System.debug('///contractTypeNode'+contractTypeNode);
                Id contractRequestID  = (Id)contractTypeNode.get('ContractRequestIdFromURL');
                //System.debug('///contractRequestID'+contractRequestID);
                Map<String,Object> response = generateOrUpdateContract(contractRequestID,ContractDetail);
                //System.debug('//////response'+response);
                if (response != null) 
                    outMap.put('createContractResponse', response);
                    //Added by Jaya since the WorkRequest class also puts in the response under this node.
                    outMap.put('WorkRequestDetails',response.get('WorkRequestDetails'));
                    Map<String, Object> workRequestParam = new Map<String, Object>();
                    Map<String,Object> contractNode = (Map<String,Object>)outMap.get('createContractResponse');
                    //System.debug('/////contractNode'+contractNode);
                    Contract contractObjNode = (Contract)contractNode.get('contractObj');
                    //System.debug('/////contractObjNode'+contractObjNode);
                    workRequestParam.put('ContextId', inputMap.get('ContextId'));
                    //System.debug('/////contextId'+workRequestParam.get('ContextId'));
                    workRequestParam.put('ContractRequestID',contractTypeNode.get('ContractRequestIdFromURL'));
                    //System.debug('/////ContractRequestID'+workRequestParam.get('ContractRequestID'));
                    workRequestParam.put('ContractId',contractObjNode.id);
                   // System.debug('/////ContractId'+workRequestParam.get('ContractId'));
                    outMap.put('workRequestParam',workRequestParam);
                
                System.debug('debugging '+JSON.serializePretty(outMap));
            }
        } else {
            return false;
        }
        return true;
    }

    public static Map<String,Object> UpdateContractStatusWithEnvelope(List<contract> lstContract){
        Boolean isCallContinuation = false;
        String amendContractResponse = '';
         Map<String,Object> outMap = new Map<String,Object>();
         //Added line for Test Coverage
          try{
            /*if(vlocity_cmt.FlowStaticMap.FlowMap.Containskey('forContractTestCoverageNoEnvelopException')){
                            throw new fotTestCoverageException('FAILURE');
                        } */
            if(lstContract!= null && lstContract.size()>0){
              
                    for(Contract cntrct: lstContract){
                        String contractStatus = cntrct.Status;
                       
                    
                     if(String.IsNotBlank(contractStatus) && (contractStatus.equalsIgnoreCase('In Progress') || contractStatus.equalsIgnoreCase('Customer Accepted'))){
                           /* if(Test.isRunningTest())
                            amendContractResponse = SetValueFromTestMethod;
                            else*/
                         //if(!Test.isRunningTest()){
                                 System.debug('//enteredddd');
                                amendContractResponse = OCOM_DocuSignUtilClass.doCancel_vlocity(cntrct);
                         //}    
                            if(amendContractResponse == Label.SUCCESS){
                            system.debug('Response is:::' + amendContractResponse  );
                                //isCallContinuation=true;
                                //outMap.put('isCallContinuation',isCallContinuation);
                                outMap.put('amendContractResponse',amendContractResponse);
                               
                                break;
                            }else{
                                
                                //isCallContinuation=false;
                                //outMap.put('isCallContinuation',isCallContinuation);
                                outMap.put('amendContractResponse',amendContractResponse);
                                break;
                            }

                            
                        }
                        
                    }
                 }   
                    System.debug('@@lstContract'+lstContract);
            }catch(exception e){
                System.debug(LoggingLevel.ERROR, 'Exception is '+e);
                System.debug(LoggingLevel.ERROR, 'Exception stack trace '+e.getStackTraceString());
                isCallContinuation=false;
                outMap.put('isCallContinuation',isCallContinuation);
                outMap.put('amendContractResponse',e.getMessage());
                return outMap;
                //return outMap.put('sendOrderResponse',e.getStackTraceString());
            }
        return outMap;
    }

    /*This method creates a new Contract record or updates an existing Contract. Returns validation information on work request, contract attachment to OS */ 
    public static Map<String,Object> generateOrUpdateContract(Id contractRequestID,Map<string,object>ContractDetail) {
        Map<String,Object> responseMap = new Map<String,Object>();
        Set<String> contractEndStatusSet = new Set<String> {'Cancelled Request', 'Client Declined', 'Deactivated', 'Acceptance Expired','Terminated'};
        String contractNumber = '';
        //List<contract>UpdateContra
        String contractId;
        Boolean isSuccess =false;
        List<contract> contractToupdate = new List<contract>();
        list<Contracts__c> contractRequestToUpdate = new List<Contracts__c>();
        List<Contract>  lstExistingContracts = new List<Contract>();
        Map<Id,Contract> exisitingContractMap = new Map<Id,Contract>();
        Map<String,Id> contractStatusToIdMap  = new Map<String,Id>();
        Map<Id,Work_Request__c> contractWRMap = new Map<Id,Work_Request__c>();
        Boolean workRequestExists = false;
        //Added by Jaya
        Boolean contractDocExists = false;
        Boolean closedWorkRequestExists = false;
        List<contract> contractWithEnvelope = new List<contract>();
        //ToDO:: Change the Contract RecordType to a new one.
        Id ContractRecordTypeId = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Vlocity Contracts').getRecordTypeId();        
        System.debug('ContractRecordTypeId____'+ContractRecordTypeId);
        try {
            if( contractRequestID != null) {
                //showContractSection = true;
                
                //Change by Jaya - added vlocity_cmt__ActiveContractVersionId__c fields
                //Change by Jaya - added Status__c to work request query.
                for(Contract contrct: [Select (select term__c from vlocity_cmt__ContractLineItems__r),Id, TERM_OF_CONTRACT__c,vlocity_cmt__OriginalContractId__r.TERM_OF_CONTRACT__c,vlocity_cmt__OriginalContractId__r.Total_Contract_Value__c,Total_Contract_Value__c,ContractRevoked__c, vlocity_cmt__ActiveContractVersionId__r.AttachmentUrl__c, Status,BA_Contract_Status__c,Name,No_Amend__c,LastModifiedDate,LastModifiedBy.Name,
                                                        ContractNumber,vlocity_cmt__OrderId__c,vlocity_cmt__OrderId__r.Status,Comments__c,
                                       (Select Id, Status__c, Name, Owner.Name, Owner.Id, LastModifiedDate, Comments__c from Work_Requests__r ORDER BY LastModifiedDate DESC)
                                                        From Contract where Complex_Fulfillment_Contract__c =:contractRequestID 
                                                        and Status NOT IN :contractEndStatusSet Order By LastModifiedDate Desc] ){
                    exisitingContractMap.put(contrct.Id,contrct);
                    lstExistingContracts.add(contrct);
                    contractStatusToIdMap.put(contrct.status, contrct.id);
                    /* if (contrct.Work_Requests__c.size()>0)
                     {
                         workRequestExists=true;
                     }
                   boolean isfirstWRChecked =false;                                     
                     for(Work_Request__c wr: contrct.Work_Requests__r) {                                       
                            contractWRMap.put(wr.id,wr);
                            workRequestExists = true;
                            
                            //Added by Jaya
                            if(wr != null && isfirstWRChecked != true)
                            {
                                isfirstWRChecked=true;
                                if(wr.Status__c == 'Closed')
                                {
                                    closedWorkRequestExists = true;
                                }
                                responseMap.put('WorkRequestDetails',contrct.Work_Requests__r);
                            }
                       }*/

                }
            
              //Logic for Generating New Contract
                    System.debug('Existing Contract Information>>' + lstExistingContracts + ' lstExistingContracts.size()>>>' + lstExistingContracts.size());
                if(lstExistingContracts == null || lstExistingContracts.size() ==0 ) {
                    System.debug('Contract needs to be created***');
                    System.debug('contractRequestID='+contractRequestID);
                    if(Test.isRunningTest()){
                        List<Contract> contractList=[select id from contract];
                        if(!contractList.isEmpty()){
                            contractId=contractList.get(0).id;                            
                        } else { return null;}
                        
                    } else {
                       contractId = vlocity_cmt.ContractServiceResource.createContractWithTemplate(contractRequestID); 
                    }
                        

                        vlocity_cmt__ContractManagementConfigurationSetup__c needDoc  = vlocity_cmt__ContractManagementConfigurationSetup__c.getValues('CreateDocument_'+contractId);  
                        if(needDoc != null){
                            delete needDoc;
                        }

                        system.debug('ContractID___::'+contractId);
                        if(contractId != null && contractId != ''){
                            List<String> contractIds = new List<String>{contractId};
                            
                            //Change by Jaya - Added field vlocity_cmt__ActiveContractVersionId__c
                            contractToUpdate = [Select TERM_OF_CONTRACT__c,vlocity_cmt__OriginalContractId__r.TERM_OF_CONTRACT__c,vlocity_cmt__OriginalContractId__r.Total_Contract_Value__c,Total_Contract_Value__c,Id,vlocity_cmt__ActiveContractVersionId__r.AttachmentUrl__c,Status,BA_Contract_Status__c,Name,No_Amend__c,LastModifiedDate,LastModifiedBy.Name,
                                                Complex_Fulfillment_Contract__c,Comments__c,
                                                ContractNumber,vlocity_cmt__OrderId__r.Status,vlocity_cmt__OrderId__c
                                                From Contract where Id =: contractId limit 1];
                                              
                            isSuccess = true;
                            if(contractToUpdate != null && contractToUpdate.size() > 0) {
                                contract c = contractToUpdate[0];
                               // c.Comments__c = (string)ContractDetail.get('Comment');
                                //update c;
                                Contracts__c contractReq = new Contracts__c(id=contractToUpdate[0].Complex_Fulfillment_Contract__c, SFDC_Contract__c=contractToUpdate[0].id );
                                contractRequestToUpdate.add(contractReq);
                                update contractRequestToUpdate;
                                
                           
                                if(c.vlocity_cmt__ActiveContractVersionId__r.AttachmentUrl__c != null)
                                    contractDocExists = true;
                                else if(c.vlocity_cmt__ActiveContractVersionId__c == null)
                                    contractDocExists = false;
                            }
                            // Aysnch Call to Generate Contract Snapshot.
                             OCOM_ContractDetails.createContractInstanceAttributes(contractToUpdate[0].id);
    
                            String contractStatus = 'Draft';
                            
                            responseMap.put('response',Label.OCOM_Contract_Created);
                            responseMap.put('contractStatus',contractStatus);
                            responseMap.put('isSuccess',isSuccess);
                            responseMap.put('contractObj',contractToUpdate[0]);
                            responseMap.put('AttachmentRequired',contractDocExists);  
                            
                            if(contractToUpdate!=null && !contractToUpdate.isEmpty()){
                                        Contract contractInst=contractToUpdate.get(0);
                                Boolean tlcFlag=false;
                                if(String.isNotBlank(contractInst.vlocity_cmt__OrderId__c)){
                                 	tlcFlag=isTLCFlag(contractInst.vlocity_cmt__OrderId__c);
                                }
                                        if(tlcFlag||(contractInst.TERM_OF_CONTRACT__c!=null && contractInst.vlocity_cmt__OriginalContractId__r.TERM_OF_CONTRACT__c!=null
                                           && contractInst.TERM_OF_CONTRACT__c!=contractInst.vlocity_cmt__OriginalContractId__r.TERM_OF_CONTRACT__c)){
                                               responseMap.put('TlcMsgFlag','true');
                                           } else if(contractInst.Total_Contract_Value__c!=null && contractInst.vlocity_cmt__OriginalContractId__r.Total_Contract_Value__c!=null
                                                     && contractInst.Total_Contract_Value__c!=contractInst.vlocity_cmt__OriginalContractId__r.Total_Contract_Value__c) {
                                                         responseMap.put('TlcMsgFlag','true'); 
                                                     } else {
                                                         responseMap.put('TlcMsgFlag','false');
                                                     }
                                    }
                            //responseMap.put('workRequestExists',workRequestExists);
                            //Added by Jaya
                            //responseMap.put('closedWorkRequestExists',closedWorkRequestExists);

                        }
                    }
                    //Logic for Updating the Contract 
                    //Jaya - TBD add logic for checking if the existing contracts have an attachment. 
                    //There might be multiple contracts associated with the same Contract Request so do we check every contract for an attachment?
                    else if (lstExistingContracts != null && lstExistingContracts.size()> 0 ){
                        Contract updateContract = new Contract();
                        String response;
                        Map<String,Object> outMap = new Map<String,Object>();
                        
                       /* Commenting out as not Required for Beta
                        //String contractStatus = updateContract.Status;
                        for(Contract con:lstExistingContracts)
                        {
                            if(con.Status != null)
                            {
                                if(con.Status == 'In Progress' || con.Status == 'Customer Accepted')
                                {
                                    contractWithEnvelope.add(con);
                                }
                            }
                        }
                        
                        if(contractWithEnvelope != null && contractWithEnvelope.size() > 0)
                        {
                            outMap = OCOM_ContractUtil.UpdateContractStatusWithEnvelope(contractWithEnvelope);
                            system.debug('***OutMap'+outMap);
                        }*/
                        
                                
                        if(contractStatusToIdMap.ContainsKey('Draft')){
                            updateContract =  exisitingContractMap.get(contractStatusToIdMap.get('Draft'));
                            System.debug('Update Contract Clis...');
                            //Adharsh: Commenting out Summer 17 bug workaround
                            // List<vlocity_cmt__ContractManagementConfigurationSetup__c> cmcList = new List<vlocity_cmt__ContractManagementConfigurationSetup__c>();
                            //  System.debug('Update Contract____ '+updateContract.id);

                            //vlocity_cmt__ContractManagementConfigurationSetup__c needDoc  = vlocity_cmt__ContractManagementConfigurationSetup__c.getValues('CreateDocument_'+updateContract.id);
                            //System.debug('needDoc__ '+needDoc);

                            //if(needDoc !=null) {
                            //        cmcList.add(needDoc);
                            //     }

                            //    if(cmcList != null && cmcList.size() > 0 && !cmcList.isEmpty()){
                            //        delete cmcList;
                            //    }
                            //Adharsh Comment End
                            String result = vlocity_cmt.ContractServiceResource.updateContract(contractRequestID);
                            if(result!= null && result.equalsIgnoreCase(Label.vlocity_cmt.ContractUpdateSuccess)){

                               
                                

                                response = 'Success';
                                
                                // Aysnch Call to Generate Contract Snapshot.
                                    OCOM_ContractDetails.createContractInstanceAttributes(updateContract.id);
                                
                                } else{
                                        response = result;
                                }
                            
                            System.debug('Result__::' +result + '__Resoponse__::'+response);
                           
                        }else{
                            
                            updateContract =  lstExistingContracts[0];
                            System.debug('Cannot Update Contract Clis...Contract Status ::' + updateContract.Status);
                            response = 'Success';
                        }
                        
                        if(response == 'Success' ){
                            
                                system.debug('ContractID___::'+updateContract.id);
                                responseMap.put('response','ExisitingContract');
                                responseMap.put('contractStatus',updateContract.Status);
                                responseMap.put('isSuccess',true);
                                responseMap.put('contractObj',updateContract);
                                //responseMap.put('workRequestExists',workRequestExists);
                                //Added by Jaya
                                //if(updateContract.vlocity_cmt__ActiveContractVersionId__r.AttachmentUrl__c != null && updateContract.vlocity_cmt__ActiveContractVersionId__r.AttachmentUrl__c != '')
                                //responseMap.put('closedWorkRequestExists',closedWorkRequestExists);
                                
                                if(updateContract.ContractRevoked__c == true)
                                    responseMap.put('ContractRevoked',true);
                                else
                                    responseMap.put('ContractRevoked',false);
                                
                                if(updateContract.vlocity_cmt__ActiveContractVersionId__r.AttachmentUrl__c != null)
                                {
                                    contractDocExists = true;
                                    responseMap.put('AttachmentRequired',contractDocExists);
                                }
                                else if(updateContract.vlocity_cmt__ActiveContractVersionId__r.AttachmentUrl__c == null)
                                {
                                    contractDocExists = false;
                                    responseMap.put('AttachmentRequired',contractDocExists);
                                }
                                  Boolean tlcFlag=false;
                            tlcFlag =isTLCFlag(updateContract.vlocity_cmt__OrderId__c);
                             /*   if(String.isNotBlank(updateContract.vlocity_cmt__OrderId__c)){
                                    for(Orderitem oiInst:[select contract_action__c from orderitem where order.parentid__c=:updateContract.vlocity_cmt__OrderId__c]){
                                        if(String.isNotBlank(oiInst.contract_action__c)){
                                            tlcFlag=true;
                                        }
                                    }
                                    
                                }*/
                                      
                            if(tlcFlag||(updateContract.TERM_OF_CONTRACT__c!=null && updateContract.vlocity_cmt__OriginalContractId__r.TERM_OF_CONTRACT__c!=null
                               && updateContract.TERM_OF_CONTRACT__c!=updateContract.vlocity_cmt__OriginalContractId__r.TERM_OF_CONTRACT__c)){
                                   responseMap.put('TlcMsgFlag','true');
                               } else if(updateContract.Total_Contract_Value__c!=null && updateContract.vlocity_cmt__OriginalContractId__r.Total_Contract_Value__c!=null
                                         && updateContract.Total_Contract_Value__c!=updateContract.vlocity_cmt__OriginalContractId__r.Total_Contract_Value__c) {
                                             responseMap.put('TlcMsgFlag','true'); 
                                         } else {
                                             responseMap.put('TlcMsgFlag','false');
                                         }
                                       
                          
                                    
                                system.debug('***'+responseMap);
                        }
                        else {
                                system.debug('ContractID-update Failed___::'+updateContract.id);
                                responseMap.put('response',response);
                                responseMap.put('contractStatus',updateContract.Status);
                                responseMap.put('isSuccess',false);
                                responseMap.put('contractObj',updateContract);
                                //responseMap.put('workRequestExists',workRequestExists);
                                //Added by Jaya
                               // responseMap.put('closedWorkRequestExists',closedWorkRequestExists);
                               if(updateContract.ContractRevoked__c == true)
                                    responseMap.put('ContractRevoked',true);
                                else
                                    responseMap.put('ContractRevoked',false);
                                //Added by Jaya
                                if(updateContract.vlocity_cmt__ActiveContractVersionId__r.AttachmentUrl__c != null)
                                {
                                    contractDocExists = true;
                                    responseMap.put('AttachmentRequired',true);
                                }
                                else if(updateContract.vlocity_cmt__ActiveContractVersionId__r.AttachmentUrl__c == null)
                                {
                                    contractDocExists = false;
                                    responseMap.put('AttachmentRequired',false);
                                  
                                }
                             Boolean tlcFlag=false;
                                
                            if(String.isNotBlank(updateContract.vlocity_cmt__OrderId__c)){
                                 	tlcFlag=isTLCFlag(updateContract.vlocity_cmt__OrderId__c);
                                }
                               if(tlcFlag||(updateContract.TERM_OF_CONTRACT__c!=null && updateContract.vlocity_cmt__OriginalContractId__r.TERM_OF_CONTRACT__c!=null
                               && updateContract.TERM_OF_CONTRACT__c!=updateContract.vlocity_cmt__OriginalContractId__r.TERM_OF_CONTRACT__c)){
                                   responseMap.put('TlcMsgFlag','true');
                               } else if(updateContract.Total_Contract_Value__c!=null && updateContract.vlocity_cmt__OriginalContractId__r.Total_Contract_Value__c!=null
                                         && updateContract.Total_Contract_Value__c!=updateContract.vlocity_cmt__OriginalContractId__r.Total_Contract_Value__c) {
                                             responseMap.put('TlcMsgFlag','true'); 
                                         } else {
                                             responseMap.put('TlcMsgFlag','false');
                                         }
                            }                           
                              
                    }  
            }
        }catch(exception e){
        
            System.debug(LoggingLevel.ERROR, 'Exception is '+e);
            System.debug(LoggingLevel.ERROR, 'Exception stack trace '+e.getStackTraceString());
            String messageDisplay = e.getMessage();
            isSuccess = false;
             
            responseMap.put('response',messageDisplay);
            responseMap.put('isSuccess',isSuccess);
            responseMap.put('contractObj',null);
            return responseMap;
        }
       
        return responseMap;
    }    
    
    public static Boolean isTLCFlag(String orderId){
        //return true;
        Boolean tlcFlag=false;
        if(String.isNotBlank(orderId)){
            Map<String,Integer> orderIdTermMap=new Map<String,Integer>();
            Map<String,String> orderIdStatusMap=new Map<String,String>();
            Boolean isOneOrderCancelled=false;
            for(Orderitem oiInst:[select contract_action__c,term__c,order.status,orderid from orderitem where order.parentid__c=:orderId]){
                if(String.isNotBlank(oiInst.contract_action__c)){
                    tlcFlag=true;
                }
                if(String.isNotBlank(oiInst.Term__c)){
                    String termStr=oiInst.Term__c;
                    Integer termInt=0;
                    try{
                        termInt=Integer.valueOf(termStr);
                    }catch(Exception e){
                        
                    }
                    
                    if(termInt>0){
                        orderIdTermMap.put(oiInst.orderid,termInt);  
                    }
                    
                }
                if(oiInst.Order.status=='Cancelled'){
                    isOneOrderCancelled=true;
                }
                orderIdStatusMap.put(oiInst.orderid,oiInst.Order.Status);
                
            }
            if(isOneOrderCancelled){
                Integer term=0;
                Integer cancelledOrderTerm=0;
                for(String keyStr:orderIdTermMap.keySet()){
                    Integer termTemp=orderIdTermMap.get(keyStr);
                    if(orderIdStatusMap.get(keyStr)!=null && 'Cancelled'==orderIdStatusMap.get(keyStr)){
                        if(orderIdTermMap.get(keyStr)!=null && orderIdTermMap.get(keyStr)>cancelledOrderTerm){
                            cancelledOrderTerm=orderIdTermMap.get(keyStr);
                        }
                    } else {
                        if(orderIdTermMap.get(keyStr)!=null && orderIdTermMap.get(keyStr)>term  ){
                            term=orderIdTermMap.get(keyStr);
                        }
                    }
                }
                if(cancelledOrderTerm>term){
                    tlcFlag=true;
                }
            }
        }
        return tlcFlag;
    }
    
 
}