/********************************************************************************************
* Class Name : HTTPSOAPEnvCallout
* Purpose    : The Purpose of this class is to make external http callouts.     
* 
* Modification History 
*-------------------------------------------------------------------------------------------
* Author                        Date                Version             Remarks
*-------------------------------------------------------------------------------------------
* Tech Mahindra(Balasaheb)          Apr/04/2016         1.0                 Initial Creation
* 
*
**********************************************************************************************/

public with sharing class ESRHTTPSOAPEnvCallout{  
    
    public string HttpCallout(string reqXML,string uri,string httpMethod,string actionHeader,string uid, string password)
    {
        try{           
            
            //check Request XML is blank or Empty
            if(String.isBlank(reqXML)) 
                throw new NullAgrumentException('Request XML cannot be blank or empty');
            //Check HTTP URI is banlk or empty
            if(String.isBlank(uri))
                throw new NullAgrumentException('Request URI cannot be blank or empty');
            
            HTTPRequest httpReq = new HTTPRequest();            
            Http http = new Http();            
            httpReq.SetEndPoint(uri);            
            httpReq.SetMethod(httpMethod);            
            httpReq.SetTimeOut(120000);             
            //Set Header Action               
            httpReq.SetHeader('SOAPAction',actionHeader);            
            //Set Authorization header for Service Authenticaiton    
            Blob headerValue = Blob.valueOf(uid+ ':' + password);
            String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
            httpReq.setHeader('Authorization', authorizationHeader);
            httpReq.setHeader('Content-Type', 'text/xml');
            httpReq.SetBody(reqXML);                
            //debug Request   
            system.debug(httpReq);
            //Invoke External Service  
            string respXML = '';
           // if(Test.isRunningTest()) {
                //respXML = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body><createServiceResponse xmlns="http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/ElectronicServiceRequestServiceRequestResponse_v1"><createServiceResult xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><contextData i:nil="true"/><dateTimeStamp>0001-01-01T00:00:00</dateTimeStamp><errorCode>0</errorCode><messageType>Inserted Success</messageType><transactionId i:nil="true"/><serviceResponseId>0</serviceResponseId></createServiceResult></createServiceResponse></s:Body></s:Envelope>'; 
           // } else{
                HttpResponse httpResp = http.Send(httpReq);
                //Debug Response           
                respXML = httpResp.getBody();
           // }
            
            system.debug('********** resp: '+respXML);
            return respXML;
        }
        catch(CalloutException ex)
        {
            /*Webservice_Integration_Error_Log__c errorLog = new Webservice_Integration_Error_Log__c();
            errorLog.Apex_Class_and_Method__c = 'HTTPSOAPEnvCallout.HttpCallout()';
            errorLog.Error_Code_and_Message__c = ex.getMessage();
            errorLog.External_System_Name__c = 'ESR';
            system.debug(ex.getMessage());
            insert errorLog;
            return ex.getMessage();*/
            throw ex;
        }
        
        catch(Exception e){            
            /*system.debug(e.getMessage());
            return e.getMessage();*/
            throw e;
        }
        
    }    
    public class NullAgrumentException Extends Exception{}
    
    
}