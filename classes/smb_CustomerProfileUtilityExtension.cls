public with sharing class smb_CustomerProfileUtilityExtension {

	private final Account account;
	
	public smb_CustomerProfileUtilityExtension(ApexPages.StandardController stdController) {
        this.account = (Account)stdController.getRecord();
    }
	
	public PageReference createCustomerInCP() {
		try {
			smb_CreateCustomerTriggerCalloutHelper.performCustomerServiceCall_Synchronous(new List<Id> {account.Id});
		}
		catch (Exception ex) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Fatal, ex.getMessage(), ex.getStackTraceString()));
			System.debug('------------ DC ------------');
			System.debug(ex);
			return null;
		}
		
		PageReference ref = Page.smb_CustomerProfileUtility;
		ref.setRedirect(true);
		ref.getParameters().put('id', account.id);
		return ref;
	}
}