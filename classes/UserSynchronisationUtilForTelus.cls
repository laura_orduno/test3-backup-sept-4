/**
* This class offers functionnality to support the SFDC to CPQ User synchronisation for Telus.
* We will not be using the out of the box user synchronisation mechanism as it requires Basic Authentification by SFDC CPQ Managed package when calling CPQ. 
* Basic Authentification is not currently supported by the CPQ Managed Package.
**/
public class UserSynchronisationUtilForTelus{
    public static void resetUserSyncInCPQFlag(User[] usersModified) {
        for (User userModified: usersModified) {
            if (userModified.CPQ_User_Access__c == true) {
                userModified.scpq__CurrentInCPQSystem__c = false;
            }
        }
    }
}