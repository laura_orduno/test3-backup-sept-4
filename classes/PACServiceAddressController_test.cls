/* Developed By: Sukhdeep Singh (IBM)
*  Created Date: 27-Oct-2015
* This is a test class to provide coverage to PACServiceAddressComponentController
* Modified - 06 Feb 2016 - Sandip
*/

@isTest(SeeAllData=true)
private class PACServiceAddressController_test {

    static testMethod void testServiceAddressController() {
        test.startTest(); 
        PACServiceAddressComponentController obj =  new PACServiceAddressComponentController();
        obj.useAsAttri = true;
        obj.idx = null;
        //obj.eventId = null;
        obj.Init = null;
        obj.config = null;
        obj.isUsedInVFInd = false;
        obj.pacConfigDetails=null;
        obj.onSelectElementId = 'chkId';
        obj.onCheckElementId = 'tstData';
        PACServiceAddressComponentController.apiKeyErrorMessage = 'Message';
        
        ServiceAddressData saData = new ServiceAddressData();
        saData.address = 'PO Box 226';
        saData.city = 'Behchoko';
        saData.country = 'Canada';
        saData.postalCode = 'X0E 0Y0';
        saData.province = 'NT';
        saData.fmsId = '5823362';

        saData.searchString = '';
        saData.searchCriteria = '';
        saData.addrStatus = '';

        saData.utm = '';
        saData.gpsCordinate = '';
        saData.state  = '';
        saData.numb = '';
        saData.type = '';
        saData.subdivision = '';
        saData.meridian = '';
        saData.township = '';
        saData.section = '';
        saData.quarter = '';
        saData.plan = '';
        saData.block = '';
        saData.lot = '';
        saData.googleMapLink = '';
        saData.streetDirection = '';
        saData.postalCode = '';
        saData.province = '';
        saData.canadaProvince = '';
        saData.street = '';
        saData.streetNumber = '';
        saData.streetName = '';
        saData.suiteNumber = '';
        saData.buildingNumber = '';
        saData.vector = '';
        saData.aptNumber = '';
        saData.clliCode = '';
        saData.coid = '';
        saData.localRoutingNumber = '';
        saData.ratingNpaNxx = '';
        saData.switchNumber = '';
        saData.terminalNumber = '';
        saData.npa = '';
        saData.lowestNxx = '';

        saData.isFmsIdRequired = false;
        saData.isAdressRequired = false;
        saData.isCountryRequired = false;
        saData.isUtmRequired = false;
        saData.isGpsCordinateRequired = false;
        saData.isStateRequired = false;
        saData.isNumbRequired = false;
        saData.isTypeRequired = false;
        saData.isSubdivisionRequired = false;
        saData.isMeridianRequired = false;
        saData.isRangeRequired = false;
        saData.isTownshipRequired = false;
        saData.isSectionRequired = false;
        saData.isSectionRequired = false;
        saData.isQuarterRequired = false;
        saData.isPlanRequired = false;
        saData.isBlockRequired = false;
        saData.isLotRequired = false;
        saData.isGoogleMapLinkRequired = false;
        saData.isStreetDirectionRequired = false;
        saData.isPostalCodeRequired = false;
        saData.isProvinceRequired = false;
        saData.isStreetRequired = false;
        saData.isStreetNumberRequired = false;
        saData.isStreetNameRequired = false;
        saData.isAptNumberRequired  = false;
        saData.isManualCapture   = false;
        saData.isSearchStringRequired   = false;




        
        obj.cData = saData;
        //string pacConfigName = obj.getInit();
        string pacConfigName1 = obj.getConfig();
       // obj.getCountries();
       // obj.getUSStates();
       // obj.getCanadaProvince();
        PACServiceAddressComponentController.getPostalKey ();
        
        obj.getServiceAddressDetails();
   
        obj.getServiceAddressDetails();
        obj.FMSCheckStatus = System.Label.PACNotValidStatus;
        system.debug('obj.FMSCheckStatus ===> '+obj.FMSCheckStatus);
        obj.isFmsCheckInd = true;
        PACServiceAddressComponentController.getPostalKey();
        //SRS_ASFValidationReqResponse.ServiceAddress respData = new SRS_ASFValidationReqResponse.ServiceAddress();
        SRS_ServiceAddressCommonAddressTypes_v2.address respData = new SRS_ServiceAddressCommonAddressTypes_v2.address();
        obj.assignFMSDetails(respData);
       

         test.stopTest();

    }

  static testMethod void testServiceAddressController1() {
        test.startTest(); 
        PACServiceAddressComponentController obj =  new PACServiceAddressComponentController();
        obj.connectPostalAddress = false;
        obj.isSACG = false;
        obj.sacgMessage = '';
        obj.isValidateThruSACG = false;
        
        ServiceAddressData saData = new ServiceAddressData();
        saData.address = '1004 TORONTO ST';
        saData.city = 'SMITHERS';
        saData.country = 'CAN';
        saData.postalCode = 'V0J2N0';
        saData.province = 'BC';
        saData.fmsId = '5823362';
        
        obj.cData = saData;
        
        obj.isCheckAddress = true;
        Test.setMock(WebServiceMock.class, new SRS_ASFServiceMockTest1());  
        obj.CheckAddress();
        test.stopTest();
    }
  
 public class SRS_ASFServiceMockTest1 implements WebServiceMock {
      public void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {

     //  if(soapAction == 'searchServiceAddress'){
        SRS_ServiceAddressMgmtRequestRes.searchServiceAddressResponse_element respSearch = new SRS_ServiceAddressMgmtRequestRes.searchServiceAddressResponse_element();
        //SRS_ServiceAddressMgmtRequestRes.getServiceAddressDetails_element respSearch = new SRS_ServiceAddressMgmtRequestRes.getServiceAddressDetails_element();
        SRS_ServiceAddressMgmtRequestRes.LikeMunicipalityItem testMunicipal =  new SRS_ServiceAddressMgmtRequestRes.LikeMunicipalityItem();
        SRS_ServiceAddressMgmtRequestRes.LikeStreetItem testStreet = new SRS_ServiceAddressMgmtRequestRes.LikeStreetItem();
        list<SRS_ServiceAddressMgmtRequestRes.LikeStreetItem> lstStreet = new list<SRS_ServiceAddressMgmtRequestRes.LikeStreetItem>();
        SRS_ServiceAddressMgmtRequestRes.LikeHouseItem testHouse = new SRS_ServiceAddressMgmtRequestRes.LikeHouseItem();
        list<SRS_ServiceAddressMgmtRequestRes.LikeHouseItem> lstHouse = new list<SRS_ServiceAddressMgmtRequestRes.LikeHouseItem>();
        SRS_ServiceAddressMgmtRequestRes.LikeApartmentItem testApartment = new SRS_ServiceAddressMgmtRequestRes.LikeApartmentItem();
        list<SRS_ServiceAddressMgmtRequestRes.LikeApartmentItem> lstApartment = new list<SRS_ServiceAddressMgmtRequestRes.LikeApartmentItem>();
        SRS_ServiceAddressMgmtRequestRes.ServiceAddress address = new SRS_ServiceAddressMgmtRequestRes.ServiceAddress();
        
        SRS_ServiceAddressMgmtRequestRes.ServiceAddress respaddress  = new SRS_ServiceAddressMgmtRequestRes.ServiceAddress();  
        SRS_ServiceAddressMgmtRequestRes.LikeStreetList respLikeStreet = new SRS_ServiceAddressMgmtRequestRes.LikeStreetList();
        SRS_ServiceAddressMgmtRequestRes.likeMunicipalityList respLikeMunicipality = new SRS_ServiceAddressMgmtRequestRes.likeMunicipalityList();
        SRS_ServiceAddressMgmtRequestRes.likeHouseList respLikeHouse = new SRS_ServiceAddressMgmtRequestRes.likeHouseList();
        SRS_ServiceAddressMgmtRequestRes.likeApartmentList respLikeApartment = new SRS_ServiceAddressMgmtRequestRes.likeApartmentList();
        response.put('response_x', respSearch);

        //}
    } 
    } 
    
}