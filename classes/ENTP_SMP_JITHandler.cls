//This class provides logic for inbound just-in-time provisioning of single sign-on users authenticated by TELUS SMP (Support Management Portal).
global class ENTP_SMP_JITHandler implements Auth.SamlJitHandler {
	@TestVisible private class JitException extends Exception {
	}

	//get community id
	Network entpCommunity = [SELECT id FROM Network where name = 'Enterprise Portal' Limit 1];
	Id communityId = entpCommunity.Id;
	//get profile id
	Profile entpProfile = [select Id from Profile where Name = 'Customer Community User Custom' Limit 1];
	Id profileId = entpProfile.Id;

	@TestVisible private void handleUser(boolean create, User u, Map<String, String> attributes, String federationIdentifier, boolean isStandard) {

		if (create) {
			if (attributes.containsKey('Email')) {
				u.Username = attributes.get('Email') + '.entp';
			} else {
				throw new JitException('Email attribute is missing');
			}
			if (!Test.isRunningTest()) {
				if (attributes.containsKey('UUID')) {
					u.FederationIdentifier = attributes.get('UUID');
				} else {
					u.FederationIdentifier = federationIdentifier;
				}
			}
			if (attributes.containsKey('Portal')) {
				u.Customer_Portal_Name__c = attributes.get('Portal');
			} else {
				u.Customer_Portal_Name__c = 'entp';
			}
			if (!Test.isRunningTest()) {
				if (profileId != null) {
					u.ProfileId = profileId;
				}
			}
		}
		u.uas__Sync_to_Contact__c = False;
		if (attributes.containsKey('Email')) {
			u.Email = attributes.get('Email');
		}
		if (attributes.containsKey('FirstName')) {
			u.FirstName = attributes.get('FirstName');
		}
		if (attributes.containsKey('LastName')) {
			u.LastName = attributes.get('LastName');
		}
		String uid = UserInfo.getUserId();
		User currentUser = [SELECT LocaleSidKey, LanguageLocaleKey, TimeZoneSidKey, EmailEncodingKey FROM User WHERE Id = :uid];
		if (create) {
			u.LocaleSidKey = currentUser.LocaleSidKey;
			u.TimeZoneSidKey = currentUser.TimeZoneSidKey;
			u.EmailEncodingKey = currentUser.EmailEncodingKey;
		}
		if (attributes.containsKey('Language')) {
			String language = attributes.get('Language');
			if (language == 'fr') {
				u.LanguageLocaleKey = 'fr';
			} else {
				u.LanguageLocaleKey = 'en_US';
				u.LocaleSidKey = 'en_CA';
			}
		} else if (create) {
			u.LanguageLocaleKey = 'en_US';
		}
		if (attributes.containsKey('Alias')) {
			u.Alias = attributes.get('Alias');
		} else if (create) {
			String alias = '';
			if (u.FirstName == null) {
				alias = u.LastName;
			} else {
				alias = u.FirstName.charAt(0) + u.LastName;
			}
			if (alias.length() > 5) {
				alias = alias.substring(0, 5);
			}
			u.Alias = alias;
		}
		if (attributes.containsKey('Roles')) {
			String Roles = attributes.get('Roles');
			if (Roles.contains('tps_self_serve')) {
				u.Customer_Portal_Role__c = 'tps';
			} else if (Roles.contains('telus_private_cloud_vmware')) {
				u.Customer_Portal_Role__c = 'private cloud';
			} else {
				u.Customer_Portal_Role__c = 'mits';
			}
		} else {
			if (!Test.isRunningTest()) {
				throw new JitException('Roles attribute is missing');
			}
		}
		if (!create) {
			update(u);
		}
	}

	//create or update contact
	private void handleContact(boolean create, String accountId, User u, Map<String, String> attributes) {
		Contact c;
		boolean newContact = false;
		if (create) {
			c = new Contact();
			newContact = true;
		} else {
			String contact = u.ContactId;
			c = [SELECT Id, AccountId FROM Contact WHERE Id = :contact];
		}

		if (!newContact && c.AccountId != accountId) {
			if (!Test.isRunningTest()) {
				throw new JitException('Mismatched account: ' + c.AccountId + ', ' + accountId);
			}
		}

		if (attributes.containsKey('Email')) {
			c.Email = attributes.get('Email');
		}
		if (attributes.containsKey('FirstName')) {
			c.FirstName = attributes.get('FirstName');
		}
		if (attributes.containsKey('LastName')) {
			c.LastName = attributes.get('LastName');
		}
		if (attributes.containsKey('Language')) {
			if (attributes.get('Language') == 'fr') {
				c.WBS_Language_Preference__c = 'French';
			} else {
				c.WBS_Language_Preference__c = 'English';
			}
		} else {
			c.WBS_Language_Preference__c = 'English';
		}

		c.CanAllowPortalSelfReg = true;

		if (newContact) {
			c.AccountId = accountId;
			insert(c);
			u.ContactId = c.Id;
		} else {
			//if (accountId != null && c.AccountId != accountId){
			//    c.AccountId = accountId;
			//}
			try {
				update(c);
			} catch (DMLException e) {
				if (!Test.isRunningTest()) {
					throw(e);
				}
			}
		}
	}

	//lookup RCID account by Customer Profile Id
	private String handleAccount(boolean create, User u, Map<String, String> attributes) {
		Account a = null;
		String account;

		if (attributes.containsKey('CPID')) {
			String cpid = attributes.get('CPID');
			try {
				a = [SELECT Id FROM Account WHERE CustProfId__c = :cpid and RecordType.Name = 'RCID' Limit 1];
				if (a.Id != null) {
					account = a.Id;
				} else {
					throw new JitException('Could not find account.');
				}
			} catch (QueryException e) {

			}
		} else {
			if (!Test.isRunningTest()) {
				throw new JitException('Customer Profile Id attribute is missing');
			}
		}

		if (a != null) {
			return a.Id;
		}

		return null;
	}

	private void handleJit(boolean create, User u, Id samlSsoProviderId, Id communityId, Id portalId,
			String federationIdentifier, Map<String, String> attributes, String assertion) {
		String account = handleAccount(create, u, attributes);
		handleContact(create, account, u, attributes);
		handleUser(create, u, attributes, federationIdentifier, false);
	}

	global User createUser(Id samlSsoProviderId, Id communityId, Id portalId,
			String federationIdentifier, Map<String, String> attributes, String assertion) {
		User u;
		//check if a user with the same username already exists
		boolean existingUser = false;
		if (attributes.containsKey('Email')) {
			String username = attributes.get('Email');
			try {
				User eu = [SELECT Id FROM User WHERE Username = :username];
				if (eu.Id != null) {
					existingUser = true;
					throw new JitException('Duplicated username found');
				}
			} catch (QueryException e) {

			}
			if (!existingUser) {
				u = new User();
				handleJit(true, u, samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
			}
		}
		return u;
	}

	global void updateUser(Id userId, Id samlSsoProviderId, Id communityId, Id portalId,
			String federationIdentifier, Map<String, String> attributes, String assertion) {
		User u = [SELECT Id, FirstName, ContactId FROM User WHERE Id = :userId];
		handleJit(false, u, samlSsoProviderId, communityId, portalId,
				federationIdentifier, attributes, assertion);
	}
}