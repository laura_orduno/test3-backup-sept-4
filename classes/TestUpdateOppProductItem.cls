@isTest
private class TestUpdateOppProductItem {
    static testmethod void testCodeCoverage(){
    
          // created Account -Start
             Account acc = new Account(
                 Name= 'TheTestUpdateOppProductItem Account',
                 BillingState = 'BC',
                 Phone = '604-232-5424'
             );
             insert acc;
             System.debug('Account='+acc);             
          //created Account -End
         
          //Created a User - Start
             //Selected a profile Partner User             
             Profile pPowerPartner =[select id from Profile limit 1];             
             System.debug('pPowerPartner ='+pPowerPartner );         
             User u = new User(
                 FirstName = 'FTestName',
                 LastName = 'LTestName',
                 Alias = 'kiuN',
                 Email = 'te234st@abc.com',
                 Username = 'pfgfdartner@abc.com',
      phone = '6049969449',
                 CommunityNickname = 'tbvji',
                 emailencodingkey = 'UTF-8',
                 languagelocalekey = 'en_US',
                 localesidkey = 'en_US',                 
                 timezonesidkey = 'America/Los_Angeles',             
                 profileId = pPowerPartner.Id
             );
             insert u;             
          //Created a User - End
         
          //Create Opportunity-Start             
             Datetime dateTimetemp = System.now() + 5;
             Date dateTemp = Date.newInstance(dateTimetemp.year(),dateTimetemp.month(),dateTimetemp.day());
             Opportunity opp = new Opportunity (
                 accountId = acc.Id,
                 Name = 'Testing Opportunity for Owner', 
                 CloseDate = dateTemp,                  
                 OwnerId = u.id, 
                 StageName = 'I have a Potential Opportunity (20%)'
             );        
             insert opp ;             
          //Create Opportunity-End
         
          //Create Product -Start
             Product2 prodObj = new Product2(
                 Name = 'Test -Product',
                 IsActive = true,
                 Product_Family_Code__c = 'TEST11'
             );
             insert prodObj ; 
         //Create Product -End
         
         //Create Opportunity  Product Item -Start        
          Opp_Product_Item__c opiObj = new Opp_Product_Item__c(
             Product__c =  prodObj.Id,
             Opportunity__c = opp.Id            
          );
          insert opiObj ;
         //Create Opportunit Product Item -End
         
         Opportunity oppObj = [select StageName from Opportunity where Id = :opp.Id]; 
         if(oppObj != null){
            oppObj.StageName = 'Value';
            /*
			* Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
			* START
			*/
				oppObj.Stage_update_time__c=System.Now();
			/*
			* Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
			* END
			*/
            
            update  oppObj;
         }
    }
}