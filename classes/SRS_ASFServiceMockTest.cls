global class SRS_ASFServiceMockTest implements WebServiceMock {
      global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {

       if(soapAction == 'searchServiceAddress'){
            SRS_ASFPing.pingResponse_element pingResp = new SRS_ASFPing.pingResponse_element();
            SRS_ASFPing.ping_element pingEle = new SRS_ASFPing.ping_element();
            SRS_ASFValidationExcpt.PolicyExc polExc = new SRS_ASFValidationExcpt.PolicyExc();
            SRS_ASFValidationExcpt.ServiceExc srvcExc = new SRS_ASFValidationExcpt.ServiceExc();    
            SRS_ASFValidationReqResponse.searchServiceAddressResponse_element responseElement = new SRS_ASFValidationReqResponse.searchServiceAddressResponse_element();
            SRS_ASFValidationReqResponse.LikeMunicipalityItem testMunicipal =  new SRS_ASFValidationReqResponse.LikeMunicipalityItem();
            testMunicipal.municipalityName = 'TestMunicipal';
            testMunicipal.provinceStateCode = 'TA';
            list<SRS_ASFValidationReqResponse.LikeMunicipalityItem> lstMunic = new list<SRS_ASFValidationReqResponse.LikeMunicipalityItem>();
            lstMunic.add(testMunicipal);
            SRS_ASFValidationReqResponse.LikeMunicipalityList testMunicipalcollection = new SRS_ASFValidationReqResponse.LikeMunicipalityList();
            testMunicipalcollection.startIndex = '0';
            testMunicipalcollection.likeMunicipalityItem = lstMunic;
            /////////////////////////////////////////////
            SRS_ASFValidationReqResponse.LikeStreetItem testStreet = new SRS_ASFValidationReqResponse.LikeStreetItem();
            list<SRS_ASFValidationReqResponse.LikeStreetItem> lstStreet = new list<SRS_ASFValidationReqResponse.LikeStreetItem>();
            lstStreet.add(testStreet);
            SRS_ASFValidationReqResponse.LikeStreetList testStreetcollection = new SRS_ASFValidationReqResponse.LikeStreetList();
            testStreetcollection.startIndex = '0';
            testStreetcollection.LikeStreetItem = lstStreet;
            testStreet.street='TestStreet';
            testStreet.vector='TestVector';
            testStreet.firstHouseNumber='1234';
            testStreet.lastHousenumber='1234';
            testStreet.treatmentCode='09865';
            /////////////////////////////////////////////
            SRS_ASFValidationReqResponse.LikeHouseItem testHouse = new SRS_ASFValidationReqResponse.LikeHouseItem();
            list<SRS_ASFValidationReqResponse.LikeHouseItem> lstHouse = new list<SRS_ASFValidationReqResponse.LikeHouseItem>();
            lstHouse.add(testHouse);
            SRS_ASFValidationReqResponse.LikeHouseList testHousecollection = new SRS_ASFValidationReqResponse.LikeHouseList();
            testHousecollection.startIndex = '0';
            testHousecollection.LikeHouseItem = lstHouse;
            testHouse.houseNumber='509';
            testHouse.houseSuffix='1203';
            /////////////////////////////////////////////
            SRS_ASFValidationReqResponse.LikeApartmentItem testApartment = new SRS_ASFValidationReqResponse.LikeApartmentItem();
            list<SRS_ASFValidationReqResponse.LikeApartmentItem> lstApartment = new list<SRS_ASFValidationReqResponse.LikeApartmentItem>();
            lstApartment.add(testApartment);
            SRS_ASFValidationReqResponse.LikeApartmentList testApartmentcollection = new SRS_ASFValidationReqResponse.LikeApartmentList();
            testApartmentcollection.startIndex = '0';
            testApartmentcollection.LikeApartmentItem = lstApartment;
            testApartment.unitNumber='567';
            /////////////////////////////////////////////
            SRS_ASFValidationReqResponse.ServiceAddress address = new SRS_ASFValidationReqResponse.ServiceAddress();
            address.addressId='01234';
            address.municipalityName = 'TestMunicipal';
            address.provinceStateCode = 'TA';
            //SRS_ASFValidationReqResponse.ServiceAddress address1 = new SRS_ASFValidationReqResponse.ServiceAddress();
            address.streetName='TestStreet';
            address.vector='TestVector';            
            //SRS_ASFValidationReqResponse.ServiceAddress address2 = new SRS_ASFValidationReqResponse.ServiceAddress();         
            address.streetNumber='509';
            address.streetNumberSuffix='1203';
            //SRS_ASFValidationReqResponse.ServiceAddress address3 = new SRS_ASFValidationReqResponse.ServiceAddress();
            address.unitNumber='567';
            
            testMunicipalcollection.address = address; 
            testApartmentcollection.address = address;      
            testHousecollection.address = address;
            testStreetcollection.address = address;
            responseElement.likeMunicipalityList = testMunicipalcollection;
            responseElement.likeStreetList = testStreetcollection;
            responseElement.likeHouseList = testHousecollection;
            responseElement.likeApartmentList = testApartmentcollection;
            response.put('response_x', responseElement);
       } 
       if(soapAction == 'getServiceAddressDetails'){           
            SRS_ASFValidationReqResponse.getServiceAddressDetailsResponse_element getResponseElement = new SRS_ASFValidationReqResponse.getServiceAddressDetailsResponse_element();
       }       
      
   }

}