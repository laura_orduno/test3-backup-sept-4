@isTest
private class vlocity_addon_MasterSummaryChrgsDataTest {

     static testmethod void MasterSummaryChrgsDataTest(){
        TestDataHelper.testContractLineItemListCreation_ConE();
        Test.startTest();
     Id Id1 = null;
     
    Map<String,Object> outMap=new Map<String,Object>();
        Apexpages.currentPage().getparameters().put('id',TestDataHelper.testContractObj.id);
        vlocity_addon_MasterSummaryChrgsData objvlocity_MasterSummaryChrgsData = new vlocity_addon_MasterSummaryChrgsData();
        boolean blnSuccess=objvlocity_MasterSummaryChrgsData.invokeMethod('buildDocumentSectionContent',new Map<String,Object>{'contextObjId' => TestDataHelper.testContractObj.id},outMap,new Map<String,Object>());
        system.assertEquals(outMap!=null && outMap.size() > 0, true);
    	system.assertEquals(blnSuccess, true);
     
     
    
    Test.stopTest();
    }

    
    
}