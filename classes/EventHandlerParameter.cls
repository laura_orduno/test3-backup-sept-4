/*****************************************************
    Name: EventHandlerParameter
    Usage: This class is used to define the parameter sent to Event Handler class
    Author – Sandip Chaudhari (IBM)
    Revision History -
    Created By/Date - Sandip Chaudhari / 04 July 2016
    
    Updated By/Date/Reason for Change - 
    
******************************************************/
global class EventHandlerParameter{
    global EventHandlerParameter(){}
    global External_Event__c eventObject;
    //global String templateName;
    global Map<String , Object> dataMap;
    global String subject;
    global String fromaddress;
    global List<String> cc;
    global List<String> bcc;
    global ID targetId;
    global ID whatId;
    global String toAddresses;
    global List<ID> targetIds;
    global String plainBody;
    global String htmlBody;
    global String accountNumber;
    global String currentDate;
    global String toName;
    global String serviceLocation;
    global String rcid;
    global Id orgWideEmailAddress;
    //global String errorMsg;
}