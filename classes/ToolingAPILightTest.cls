@IsTest
private class ToolingAPILightTest
{
   
    
    @IsTest 
    public static void testQueryCustomObjects(){
		Test.setMock(HttpCalloutMock.class, new ToolingAPILightMock(testObjectResponse, 200));
		ToolingAPILight toolingAPI = new ToolingAPILight();
		List<ToolingAPILight.CustomObject> result  = (List<ToolingAPILight.CustomObject>)
			//toolingAPI.query('Select Id, DeveloperName, NamespacePrefix From CustomObject Where DeveloperName = \'Test\'').records;
           toolingAPI.query('Select Id, DeveloperName, NamespacePrefix From CustomObject Where DeveloperName = \'Billing_Adjustment\'').records;
		System.assertEquals(result.size(), 1);
		System.assertEquals(result[0].DeveloperName, 'Test');
	}
    
    @IsTest 
    public static void testCallOUtError(){
		Test.setMock(HttpCalloutMock.class, new ToolingAPILightErrorMock(testObjectResponse, 200));
		ToolingAPILight toolingAPI = new ToolingAPILight();
        try{
		List<ToolingAPILight.CustomObject> result  = (List<ToolingAPILight.CustomObject>)
		toolingAPI.query('Select Id, DeveloperName, NamespacePrefix From CustomObject Where DeveloperName = \'Billing_Adjustment\'').records;
        }catch(Exception e){
            
        }    
	}
 	
    @IsTest 
    public static void testQueryCustomFields(){
		Test.setMock(HttpCalloutMock.class, new ToolingAPILightMock(testFieldResponse, 200));
		ToolingAPILight toolingAPI = new ToolingAPILight();
		List<ToolingAPILight.CustomField> result = (List<ToolingAPILight.CustomField>)
			toolingAPI.query('Select Id, DeveloperName, NamespacePrefix, TableEnumOrId From CustomField Where TableEnumOrId = \'01IG00000021cXoMAI\'').records;
		System.assertEquals(result.size(), 1);
		System.assertEquals(result[0].DeveloperName, 'A_Number');
	}
 	
    @IsTest 
    public static void testCoverTypes(){
   		ToolingAPILight.CustomField cf = new ToolingAPILight.CustomField();
		ToolingAPILight.CustomObject co = new ToolingAPILight.CustomObject();
		ToolingAPILight.QueryResult qr =  new ToolingAPILight.QueryResult();
		ToolingAPILight.User_x  u = new ToolingAPILight.User_x();
        JSONGenerator jsonGen =  JSON.createGenerator(true);
        cf.serialize(jsonGen);
        co.serialize(jsonGen);
        u.serialize(jsonGen);
	}
    
    
     @IsTest 
    public static void testToolingAPILightException(){
        new ToolingAPILight.ToolingAPILightException(new List<ToolingAPILight.ErrorResponse>{new ToolingAPILight.ErrorResponse()});
    }

	public class ToolingAPILightMock implements HttpCalloutMock {
		private String testResponse;
		private Integer testStatusCode;

		public ToolingAPILightMock(String testResponse, Integer testStatusCode){
			this.testResponse = testResponse;
			this.testStatusCode = testStatusCode;
		}

		public HTTPResponse respond(HTTPRequest req) {
          	HttpResponse res = new HttpResponse();
			res.setBody(testResponse);
			res.setStatusCode(testStatusCode);
			return res;
		}
	}
    
    public class ToolingAPILightErrorMock implements HttpCalloutMock {
		private String testResponse;
		private Integer testStatusCode;

		public ToolingAPILightErrorMock(String testResponse, Integer testStatusCode){
			this.testResponse = testResponse;
			this.testStatusCode = testStatusCode;
		}

		public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
			res.setBody(testErrorResponse);
			res.setStatusCode(400);
			return res;
            
       }
  	}

    public class CustcalloutException extends Exception
    {
        
    }
    
	private static String testObjectResponse =
		'{' +
		  '"size" : 1,' +
		  '"totalSize" : 1,' +
		  '"done" : true,' +
		  '"records" : [ {' +
		    '"attributes" : {' +
		      '"type" : "CustomObject",' +
		      '"url" : "/services/data/v29.0/tooling/sobjects/CustomObject/01IG00000021cXoMAI"' +
		    '},' +
		    '"Id" : "01IG00000021cXoMAI",' +
		    '"DeveloperName" : "Test"' +
		  '} ],' +
		  '"queryLocator" : null,' +
		  '"entityTypeName" : "CustomEntityDefinition"' +
		'}';

	private static String testFieldResponse =
		'{' +
		  '"size" : 1,' +
		  '"totalSize" : 1,' +
		  '"done" : true,' +
		  '"queryLocator" : null,' +
		  '"records" : [ {' +
		    '"attributes" : {' +
		      '"type" : "CustomField",' +
		      '"url" : "/services/data/v29.0/tooling/sobjects/CustomField/00NG0000009Y0I9MAK"' +
		    '},' +
		    '"DeveloperName" : "A_Number",' +
		    '"Id" : "00NG0000009Y0I9MAK",' +
		    '"FullName" : "01IG00000021cXo.A_Number__c",' +
		    '"TableEnumOrId" : "01IG00000021cXoMAI",' +
		    '"NamespacePrefix" : null' +
		  '} ],' +
		  '"entityTypeName" : "CustomFieldDefinition"' +
		'}';

	private static String testErrorResponse =
		'[{' +
		  '"errorCode" : "INVALID_FIELD",' +
		  '"message" : "message"' +
		'}]';

	private static String testApexClassMemberQueryResponse = '{'
    	+ '"size": 1,' 
    	+ '"totalSize": 1,'
    	+ '"done": true,'
    	+ '"records": [{'
        + '"attributes": {'
        + '    "type": "ApexClassMember",'
        + '    "url": "/services/data/v28.0/tooling/sobjects/ApexClassMember/400G00000005IaoIAE"'
        + '},'
        + '"Id": "400G00000005IaoIAE",'
        + '"Body": "body",'
        + '"Content": "content",'
        + '"ContentEntityId": "01pG0000003ZjfTIAS",'
        + '"LastSyncDate": "2014-01-28T14:51:03.000+0000",'
        + '"Metadata": {'
        + '    "apiVersion": 28.0,'
        + '    "packageVersions": null,'
        + '    "status": "Active",'
        + '    "module": null,'
        + '    "urls": null,'
        + '    "fullName": null'
        + '},'
        + '"MetadataContainerId": "1drG0000000EKF0IAO",'
        + '"SymbolTable": {'
        + '    "tableDeclaration": {'
        + '        "modifiers": [],'
        + '        "name": "ContactExt",'
        + '        "location": {'
        + '            "column": 27,'
        + '            "line": 1'
        + '        },'
        + '        "type": "ContactExt",'
        + '        "references": []'
        + '    },'
        + '    "variables": [{'
        + '        "modifiers": [],'
        + '        "name": "stdController",'
        + '        "location": {'
        + '            "column": 52,'
        + '            "line": 9'
        + '        },'
        + '        "type": "StandardController",'
        + '        "references": [{'
        + '            "column": 30,'
        + '            "line": 10'
        + '        }, {'
        + '            "column": 35,'
        + '            "line": 11'
        + '        }]'
        + '    }],'
        + '    "externalReferences": [],'
        + '    "innerClasses": [],'
        + '    "name": "ContactExt",'
        + '    "constructors": [{'
        + '        "parameters": [{'
        + '            "name": "stdController",'
        + '            "type": "StandardController"'
        + '        }],'
        + '        "visibility": "PUBLIC",'
        + '        "modifiers": [],'
        + '        "name": "ContactExt",'
        + '        "location": {'
        + '            "column": 12,'
        + '            "line": 9'
        + '        },'
        + '        "type": null,'
        + '        "references": []'
        + '    }],'
        + '    "key": "01pG0000003ZjfT",'
        + '    "methods": [{'
        + '        "returnType": "PageReference",'
        + '        "parameters": [],'
        + '        "visibility": "PUBLIC",'
        + '        "modifiers": [],'
        + '        "name": "saveContact",'
        + '        "location": {'
        + '            "column": 26,'
        + '            "line": 14'
        + '        },'
        + '        "type": null,'
        + '        "references": []'
        + '    }, {'
        + '        "returnType": "PageReference",'
        + '        "parameters": [],'
        + '        "visibility": "PUBLIC",'
        + '        "modifiers": [],'
        + '        "name": "cancelChanges",'
        + '        "location": {'
        + '            "column": 26,'
        + '            "line": 19'
        + '        },'
        + '        "type": null,'
        + '        "references": []'
        + '    }],'
        + '    "properties": [{'
        + '        "visibility": "PRIVATE",'
        + '        "modifiers": [],'
        + '        "name": "myContact",'
        + '        "location": {'
        + '            "column": 18,'
        + '            "line": 3'
        + '        },'
        + '        "type": "Contact",'
        + '        "references": [{'
        + '            "column": 14,'
        + '            "line": 11'
        + '        }, {'
        + '            "column": 16,'
        + '            "line": 15'
        + '        }]'
        + '    }, {'
        + '        "visibility": "PRIVATE",'
        + '        "modifiers": [],'
        + '        "name": "stdController",'
        + '        "location": {'
        + '            "column": 42,'
        + '            "line": 4'
        + '        },'
        + '        "type": "StandardController",'
        + '        "references": [{'
        + '            "column": 14,'
        + '            "line": 10'
        + '        }]'
        + '    }],'
        + '    "id": "01pG0000003ZjfT",'
        + '    "namespace": "timesheet"'
        + '}'
        + '}],'
        + '"queryLocator": null,'
        + '"entityTypeName": "ApexClassMember"'
        + '}';
}