global class MyController {
    public String textdata = '';
    public static String hiddenPhoneNumberValue {get;set;}
    //public static String hiddenPhoneNumberValue = '';
    
    
    @RemoteAction
    global static String findNameFromANI(String ANI)
    {
        system.debug('*** findNameFromANI for '+ANI);       
        try{
            List<Contact> objects = [SELECT Id, Name, Phone FROM Contact WHERE Phone = :ANI OR MobilePhone = :ANI];
            if (!objects.isEmpty()){
              for (Contact obj : objects){
                    system.debug('*** findNameFromANI Name = '+ obj.Name);
                    return obj.Name;
              } 
            }                
            return 'not found'; 
        }
        catch(QueryException e){
            return 'not found'; 
        }        
    }   
    
    
    @RemoteAction
    global static String findObjectType(String ID)
    {
        system.debug('*** findObjectType for '+ID);
        String accountPrefix = Schema.SObjectType.Account.getKeyPrefix();
        String casePrefix = Schema.SObjectType.Case.getKeyPrefix();
        String contactPrefix = Schema.SObjectType.Contact.getKeyPrefix();
        String opportunityPrefix = Schema.SObjectType.Opportunity.getKeyPrefix();
        String taskPrefix = Schema.SObjectType.Task.getKeyPrefix();     
        
        String prefix = ID.substring(0, 3);
        system.debug('*** prexix = '+prefix);
        
        //if(prefix == '003')
        if(prefix == contactPrefix) 
            return 'Contact';
            
        //if(prefix == '006')   
        if(prefix == opportunityPrefix)
            return 'Opportunity';
                        
        //if(prefix == '500')
        if(prefix == casePrefix)        
            return 'Case';
            
        //if(prefix == '00T')
        if(prefix ==    taskPrefix)
            return 'Task';  
                        
            
        //if(prefix == '001')
        if(prefix == accountPrefix)
        {
            try{
                //BKK Account acc = [SELECT Id FROM Account WHERE RecordType.IsPersonType=True AND Id = :ID];
                Account acc = [SELECT Id FROM Account WHERE Id = :ID];              
            }
            catch(QueryException e){
                return 'Account';   
            }
            return 'BusinessAccount';
        }
                
        return 'unknown';
    } 
    
    
    @RemoteAction
    global static String findName(String ID,String NameType)
    {
        //expecting full, first or last for NameType
        system.debug('*** findName - '+NameType +' - for '+ID);
        String namePrefix = ID.substring(0, 3);
        String name = '';
        
        /*
        if(namePrefix == '001') //account
        {
              Account acc = [SELECT Id, Name, FirstName, LastName, Phone FROM Account WHERE Id = :ID];
            Account acc = [SELECT Id, Name, Phone FROM Account WHERE Id = :ID];

            name = acc.Name;
            system.debug('*** Account Name 1 = '+ name);
            Integer i = name.indexOf(' ');

            
            if(NameType == 'full')
                return name;
            if(NameType == 'last')
            {
                // BKK return acc.LastName;
                String tempName = name.substring(i+1,name.length);
                return tempName;
            }
            if(NameType == 'first')
            {
                // BKK return acc.FirstName;
                String tempName = name.substring(0,i-1);                
                return tempName;
            }
        }
        */
        
        if(namePrefix == '003') //Contact
        {
                Contact con = [SELECT Id,Name,Phone FROM Contact WHERE Id = :ID];
                name = con.Name;
            Integer i = name.indexOf(' ');    
            if(NameType == 'full')
                return name;
            if(NameType == 'last')
            {
                String tempName = name.substring(i+1,name.length());
                return tempName;
            }
            if(NameType == 'first')
            {
                String tempName = name.substring(0,i);                
                return tempName;
            }               

        }
        
        if(namePrefix == '500')
        {
            Case my_case = [SELECT AccountId FROM Case WHERE Id = :ID];
            system.debug('*** findName Account for Case = '+my_case.AccountId);
            ID = my_case.AccountId;
        }

        // BKK Account obj = [SELECT Id, Name, FirstName, LastName, Phone FROM Account WHERE Id = :ID];
        Account obj = [SELECT Id, Name, Phone FROM Account WHERE Id = :ID];
        
        if(obj != null)
        {       
            // BKK system.debug('*** Account Name = '+ obj.Name +', '+ obj.LastName + ', '+obj.FirstName);
            if(NameType == 'full')
                return obj.Name;
            if(NameType == 'last')
                // BKK return obj.LastName;
                return obj.Name;                
            if(NameType == 'first')
                // BKK return obj.FirstName;
                return obj.Name;
                
        }
        return name;
    }
    
    @RemoteAction
    global static String findGenesysId(String ID)
    {
        String idPrefix = ID.substring(0, 3);
        system.debug('*** findGenesysId');
        /* BKK
        if(idPrefix == '500')
        {
            Case tmpCase;
            try{
            tmpCase = [select Id, Subject,CaseNumber,GenesysId__c from Case where  Id = :ID];
            }
            catch(QueryException e){
                return '';
            }
            return tmpCase.GenesysId__c;
        }
        */
        return '';
    }
    
    @RemoteAction
    global static String findPhone(String ID)
    {
        system.debug('*** findPhone');
        String phonePrefix = ID.substring(0, 3);
        String phone = '';
        
        if(phonePrefix == '003') //contact
        {
            Contact con = [SELECT Id,Name,Phone FROM Contact WHERE Id = :ID];
            phone = con.Phone;
            system.debug('*** Contact Phone = '+ phone);
            return phone;
        }
        
        if(phonePrefix == '001') //account
        {
// BKK          Account acc = [SELECT Id, Name, FirstName, LastName, Phone FROM Account WHERE Id = :ID];
                        Account acc = [SELECT Id, Name,  Phone FROM Account WHERE Id = :ID];

            phone = acc.Phone;
            system.debug('*** Account Phone 1 = '+ phone);          
            return phone;
        }
        if(phonePrefix == '500')
        {
            Case my_case = [SELECT AccountId FROM Case WHERE Id = :ID];
            system.debug('*** findPhone Account for Case = '+my_case.AccountId);
            ID = my_case.AccountId;
        }
        //Contact con = [SELECT Id,Name,Phone FROM Contact WHERE Id = :ID];
        //phone = con.Phone;
        // BKK Account obj = [SELECT Id, Name, FirstName, LastName, Phone FROM Account WHERE Id = :ID];     
        Account obj = [SELECT Id, Name, Phone FROM Account WHERE Id = :ID];     
        
        phone = obj.Phone;      
        system.debug('*** Account Phone 2 = '+ phone);
        return phone;   
    }   
    
    @RemoteAction
    global static String findSubject(String ID)
    {
        system.debug('*** findSubject for ID '+ID);
        String sPrefix = ID.substring(0, 3);
        if(sPrefix == '500')
        {
            Case my_case = [SELECT Subject FROM Case WHERE Id = :ID];
            system.debug('*** findSubject Subject for Case = '+my_case.Subject);
            return my_case.Subject;         
        }
        return 'not found'; 
    }
    
    @RemoteAction
    global static String findCaseNum(String ID)
    {
        system.debug('*** findCaseNum for ID '+ID);
        String cPrefix = ID.substring(0, 3);
        if(cPrefix == '500')
        {
            Case my_case = [SELECT CaseNumber FROM Case WHERE Id = :ID];
            system.debug('*** findCaseNum CaseNumber for Case = '+my_case.CaseNumber);
            return my_case.CaseNumber;          
        }
        return 'not found'; 
    }       
    
    @RemoteAction
    global static String findIdOfCaseNumber(String ANI,String caseNum)
    {
        system.debug('*** findIdOfCaseNumber for '+caseNum);
        String result = 'not found';
        Case my_case;
        try{
            my_case = [SELECT Id,AccountId FROM Case WHERE CaseNumber = :caseNum ];
        }
        catch(QueryException e){
            return result;
        }

        try{
            Account my_account = [SELECT Phone FROM Account WHERE Phone = :ANI];
        }
        catch(QueryException e){
            return result;
        }   

        result = my_case.Id;

        return result;

    }
    

    
    /// TM: creates an Task (Activity) record for the account with number ANI with linked Genesys ID    
    @RemoteAction global static String createTask(String ani, String subject, String description, String disposition, Integer callTime, String genesysId){
        system.debug('*** createActivity');
        system.debug('*** ani = '+ ani);
        system.debug('*** subject = '+ subject);
        system.debug('*** description = '+ description);
        system.debug('*** genesysId = '+ genesysId);
        system.debug('*** disposition = '+disposition);
        String result = 'fail';
        
        // find the account this call was related too
// BKK      List<Account> accounts = [SELECT Id, Name, FirstName, Phone FROM Account WHERE RecordType.SobjectType = 'Account' AND RecordType.IsPersonType=True AND Phone = :ani];       
        List<Contact> contacts= [SELECT AccountId, Id, Name, Phone FROM Contact WHERE Phone = :ani];
        if (!contacts.isEmpty()){
            for (Contact acc : contacts){
                system.debug('*** Account Name = '+ acc.Name);
                Task t = new Task (WhatId = acc.AccountId,
                        WhoId = acc.Id,
                        CallType  = 'Inbound',
                        Type = 'Call',
                        Status = 'Completed',
                        Subject = subject,
                        Description = description,
                        Disposition__c = disposition,
                        CallDurationInSeconds = callTime,
                        GenesysId__c = genesysId
                        );
                insert t;
                system.debug('*** Task id = '  +t.Id);
                //result = 'success';
                result = t.Id;
                return result;
            }   
        }   
        return 'not found';
    }
    
    
    @RemoteAction
    global static String redirectToPersonAccount(String ANI){
        system.debug('*** redirectToPersonAccount');
        try{
// BKK          List<Account> accounts = [SELECT Id, Name, FirstName, Phone FROM Account WHERE RecordType.SobjectType = 'Account' AND RecordType.IsPersonType=True AND Phone = :ANI];
            List<Account> accounts = [SELECT Id, Name, Phone FROM Account WHERE Phone = :ANI];

            if (!accounts.isEmpty()){
                Integer listSize = accounts.size();
                if(listSize > 1){
                    return 'multiple found';
                }
                for (Account a : accounts){
                    system.debug('*** PA Id = ' + a.Id);
                    system.debug('*** PA Name = '+ a.Name);
                    system.debug('*** PA Phone = '+ a.Phone);
                    return a.Id;
                }
            }
        }
        catch(QueryException e){
                return 'not found'; 
        }

        return 'not found';
    }


    @RemoteAction
    global static String redirectToBusinessAccount(String ANI){
        system.debug('*** redirectToBusinessAccount ANI = '+ANI);
        try{
// BKK          List<Account> accounts = [SELECT Id, Name, FirstName, Phone FROM Account WHERE RecordType.SobjectType = 'Account' AND RecordType.IsPersonType=True AND Phone = :ANI];
            List<Contact> contacts= [SELECT Id, Name, Phone, MobilePhone FROM Contact WHERE Phone = :ANI OR MobilePhone = :ANI];

            if (!contacts.isEmpty()){
                Integer listSize = contacts.size();
                if(listSize > 1){
                    return 'multiple found';
                }
                for (Contact a : contacts){
                    system.debug('*** Contact Id = ' + a.Id);
                    //system.debug('*** PA FirstName = '+ a.FirstName);
                    system.debug('*** Contact Name = '+ a.Name);
                    system.debug('*** Contact Phone = '+ a.Phone);
                    system.debug('*** Contact MobilePhone = '+a.MobilePhone);
                    return a.Id;
                }
            }
        }
        catch(QueryException e){
                return 'not found'; 
        }

        return 'not found';
    }





    
    public String getTextData() {return textdata;}
    public void setTextData(String newData){textdata = newData;}
    
    //public static String gethiddenPhoneNumberValue() {return hiddenPhoneNumberValue;}
    //public static void sethiddenPhoneNumberValue(String newData){hiddenPhoneNumberValue = newData;}
    
    
    

}