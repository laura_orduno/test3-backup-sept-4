public without sharing class QuoteDetailComponentController {
    public Id quoteId {get; set;}
    
    /* Traction Change for Case TC11294 */
    public Boolean dealerUser {get{return QuotePortalUtils.isUserDealer();}}
    /* End Traction Change for Case TC11294 */
    
    public QuoteLineVO[] getLines() {
        QuoteDetailController qdc = new QuotedetailController(quoteId);
        return qdc.products;
    }
    
}