//Generated by FuseIT WSDL2Apex (http://www.fuseit.com/Solutions/SFDC-Explorer/Help-WSDL-Parser.aspx)
//Methods Included: createBusinessCreditProfile, getBusinessCreditProfileSummary, addBusinessPartner, addIndividualPartner, removePartner, createIndividualCustomerWithCreditWorthiness, replaceBusinessProprietor, updateBusinessCreditProfile, updateIndivdualCreditProfile, searchIndivdualCreditProfile, searchBusinessCreditProfile, attachCreditReport, voidCreditReport, getCreditReportList, getCreditReport, ping
// Primary Port Class Name: BusinessCreditProfileMgmtService_v1_0_SOAP	
public class CreditProfileService {
    public class BusinessCreditProfileMgmtService_v1_0_SOAP {
        
        public String endpoint_x; 
        
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/ping_v1','CreditProfileServicePing','http://telus.com/wsdl/CMO/OrderMgmt/BusinessCreditProfileMgmtService_1','CreditProfileService','http://xmlschema.tmi.telus.com/xsd/common/exceptions/Exceptions_v1_0','CreditProfileServiceExceptions','http://xmlschema.tmi.telus.com/xsd/Customer/Customer/BusinessCreditCommonTypes_v1','CreditProfileServiceTypes','http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditProfileMgmtServiceRequestResponse_v1','CreditProfileServiceRequestResponse'};
            
            public BusinessCreditProfileMgmtService_v1_0_SOAP()
        {
            if ( !Test.isRunningTest() )
            {
                endpoint_x = SMBCare_WebServices__c.getInstance('CreditProfileEndpoint').value__c;
            }
        }
        
        public void addBusinessPartner(Long masterPartnerBusinessCustomerId,Long partnerBusinessCustomerId,CreditProfileServiceTypes.CreditAuditInfo auditInfo) {
            CreditProfileServiceRequestResponse.addBusinessPartner_element request_x = new CreditProfileServiceRequestResponse.addBusinessPartner_element();
            CreditProfileServiceRequestResponse.addBusinessPartnerResponse_element response_x;
            request_x.masterPartnerBusinessCustomerId = masterPartnerBusinessCustomerId;
            request_x.partnerBusinessCustomerId = partnerBusinessCustomerId;
            request_x.auditInfo = auditInfo;
            Map<String, CreditProfileServiceRequestResponse.addBusinessPartnerResponse_element> response_map_x = new Map<String, CreditProfileServiceRequestResponse.addBusinessPartnerResponse_element>();
            response_map_x.put('response_x', response_x);
            //if ( !Test.isRunningTest() )
                WebServiceCallout.invoke(
                    this,
                    request_x,
                    response_map_x,
                    new String[]{endpoint_x,
                        'addBusinessPartner',
                        'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditProfileMgmtServiceRequestResponse_v1',
                        'addBusinessPartner',
                        'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditProfileMgmtServiceRequestResponse_v1',
                        'addBusinessPartnerResponse',
                        'CreditProfileServiceRequestResponse.addBusinessPartnerResponse_element'}
                );
            response_x = response_map_x.get('response_x');
        }
        
        public void addIndividualPartner(Long masterBusinessCustomerId,Long partnerIndividualCustomerId,CreditProfileServiceTypes.CreditAuditInfo auditInfo) {
            CreditProfileServiceRequestResponse.addIndividualPartner_element request_x = new CreditProfileServiceRequestResponse.addIndividualPartner_element();
            CreditProfileServiceRequestResponse.addIndividualPartnerResponse_element response_x;
            request_x.masterBusinessCustomerId = masterBusinessCustomerId;
            request_x.partnerIndividualCustomerId = partnerIndividualCustomerId;
            request_x.auditInfo = auditInfo;
            Map<String, CreditProfileServiceRequestResponse.addIndividualPartnerResponse_element> response_map_x = new Map<String, CreditProfileServiceRequestResponse.addIndividualPartnerResponse_element>();
            response_map_x.put('response_x', response_x);
            //if ( !Test.isRunningTest() )
                WebServiceCallout.invoke(
                    this,
                    request_x,
                    response_map_x,
                    new String[]{endpoint_x,
                        'addIndividualPartner',
                        'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditProfileMgmtServiceRequestResponse_v1',
                        'addIndividualPartner',
                        'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditProfileMgmtServiceRequestResponse_v1',
                        'addIndividualPartnerResponse',
                        'CreditProfileServiceRequestResponse.addIndividualPartnerResponse_element'}
                );
            response_x = response_map_x.get('response_x');
        }
        
        public String attachCreditReport(Long businessCustomerId,CreditProfileServiceTypes.BusinessCreditReportRequest businessCreditReportRequest,CreditProfileServiceTypes.CreditAuditInfo auditInfo) {
            CreditProfileServiceRequestResponse.attachCreditReport_element request_x = new CreditProfileServiceRequestResponse.attachCreditReport_element();
            CreditProfileServiceRequestResponse.attachCreditReportResponse_element response_x;
            request_x.businessCustomerId = businessCustomerId;
            request_x.businessCreditReportRequest = businessCreditReportRequest;
            request_x.auditInfo = auditInfo;
            Map<String, CreditProfileServiceRequestResponse.attachCreditReportResponse_element> response_map_x = new Map<String, CreditProfileServiceRequestResponse.attachCreditReportResponse_element>();
            response_map_x.put('response_x', response_x);
            //if ( !Test.isRunningTest() )
                WebServiceCallout.invoke(
                    this,
                    request_x,
                    response_map_x,
                    new String[]{endpoint_x,
                        'attachCreditReport',
                        'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditProfileMgmtServiceRequestResponse_v1',
                        'attachCreditReport',
                        'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditProfileMgmtServiceRequestResponse_v1',
                        'attachCreditReportResponse',
                        'CreditProfileServiceRequestResponse.attachCreditReportResponse_element'}
                );
            response_x = response_map_x.get('response_x');
            String creditReportId;
            if(response_x!=null) creditReportId = response_x.creditReportId;
            return creditReportId;
        }
        
        public Long createBusinessCreditProfile(
            Long businessCustomerId
            ,CreditProfileServiceTypes.BusinessCreditProfileBase newBusinessCreditProfile
            ,Long proprietorshipIndividualCustomerId
            ,CreditProfileServiceTypes.CreditAuditInfo auditInfo) {
                CreditProfileServiceRequestResponse.createBusinessCreditProfile_element request_x = new CreditProfileServiceRequestResponse.createBusinessCreditProfile_element();
                CreditProfileServiceRequestResponse.createBusinessCreditProfileResponse_element response_x;
                request_x.businessCustomerId = businessCustomerId;
                request_x.newBusinessCreditProfile = newBusinessCreditProfile;
                request_x.proprietorshipIndividualCustomerId = proprietorshipIndividualCustomerId;
                request_x.auditInfo = auditInfo;
                Map<String, CreditProfileServiceRequestResponse.createBusinessCreditProfileResponse_element> response_map_x = new Map<String, CreditProfileServiceRequestResponse.createBusinessCreditProfileResponse_element>();
                response_map_x.put('response_x', response_x);
                //if ( !Test.isRunningTest() )
                    WebServiceCallout.invoke(
                        this,
                        request_x,
                        response_map_x,
                        new String[]{endpoint_x,
                            'createBusinessCreditProfile',
                            'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditProfileMgmtServiceRequestResponse_v1',
                            'createBusinessCreditProfile',
                            'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditProfileMgmtServiceRequestResponse_v1',
                            'createBusinessCreditProfileResponse',
                            'CreditProfileServiceRequestResponse.createBusinessCreditProfileResponse_element'}
                    );
                response_x = response_map_x.get('response_x');
                Long businessCreditProfileId = 0L;
                if(response_x!=null) businessCreditProfileId = response_x.businessCreditProfileId;
                return businessCreditProfileId;
            }
        
        public Long createIndividualCustomerWithCreditWorthiness(CreditProfileServiceTypes.IndividualCustomerProfile individualCustomer,CreditProfileServiceTypes.IndividualCreditProfileBase individualCreditProfile,CreditProfileServiceTypes.CreditAuditInfo auditInfo) {
            CreditProfileServiceRequestResponse.createIndividualCustomerWithCreditWorthiness_element request_x = new CreditProfileServiceRequestResponse.createIndividualCustomerWithCreditWorthiness_element();
            CreditProfileServiceRequestResponse.createIndividualCustomerWithCreditWorthinessResponse_element response_x;
            request_x.individualCustomer = individualCustomer;
            request_x.individualCreditProfile = individualCreditProfile;
            request_x.auditInfo = auditInfo;
            Map<String, CreditProfileServiceRequestResponse.createIndividualCustomerWithCreditWorthinessResponse_element> response_map_x = new Map<String, CreditProfileServiceRequestResponse.createIndividualCustomerWithCreditWorthinessResponse_element>();
            response_map_x.put('response_x', response_x);
            //if ( !Test.isRunningTest() )
                WebServiceCallout.invoke(
                    this,
                    request_x,
                    response_map_x,
                    new String[]{endpoint_x,
                        'createIndividualCustomerWithCreditWorthiness',
                        'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditProfileMgmtServiceRequestResponse_v1',
                        'createIndividualCustomerWithCreditWorthiness',
                        'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditProfileMgmtServiceRequestResponse_v1',
                        'createIndividualCustomerWithCreditWorthinessResponse',
                        'CreditProfileServiceRequestResponse.createIndividualCustomerWithCreditWorthinessResponse_element'}
                );
            response_x = response_map_x.get('response_x');
            Long individualCustomerId = 0L;
            if(response_x!=null) individualCustomerId = response_x.individualCustomerId;
            return individualCustomerId;
        }
        
        public CreditProfileServiceRequestResponse.getBusinessCreditProfileSummaryResponse_businessCreditProfileSummaryResult_element 
            getBusinessCreditProfileSummary(
                Long businessCustomerId
                ,CreditProfileServiceTypes.CreditAuditInfo auditInfo) {
                    CreditProfileServiceRequestResponse.getBusinessCreditProfileSummary_element request_x = new CreditProfileServiceRequestResponse.getBusinessCreditProfileSummary_element();
                    CreditProfileServiceRequestResponse.getBusinessCreditProfileSummaryResponse_element response_x;
                    request_x.businessCustomerId = businessCustomerId;
                    request_x.auditInfo = auditInfo;
                    Map<String, CreditProfileServiceRequestResponse.getBusinessCreditProfileSummaryResponse_element> response_map_x = new Map<String, CreditProfileServiceRequestResponse.getBusinessCreditProfileSummaryResponse_element>();
                    response_map_x.put('response_x', response_x);
                    WebServiceCallout.invoke(
                        this,
                        request_x,
                        response_map_x,
                        new String[]{endpoint_x,
                            'getBusinessCreditProfileSummary',
                            'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditProfileMgmtServiceRequestResponse_v1',
                            'getBusinessCreditProfileSummary',
                            'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditProfileMgmtServiceRequestResponse_v1',
                            'getBusinessCreditProfileSummaryResponse',
                            'CreditProfileServiceRequestResponse.getBusinessCreditProfileSummaryResponse_element'}
                    );
                    response_x = response_map_x.get('response_x');
                    return response_x.businessCreditProfileSummaryResult;  
                }
        
        public CreditProfileServiceTypes.BusinessCreditReportFullDetails getCreditReport(Long creditReportId) {
            CreditProfileServiceRequestResponse.getCreditReport_element request_x = new CreditProfileServiceRequestResponse.getCreditReport_element();
            CreditProfileServiceRequestResponse.getCreditReportResponse_element response_x;
            request_x.creditReportId = creditReportId;
            Map<String, CreditProfileServiceRequestResponse.getCreditReportResponse_element> response_map_x = new Map<String, CreditProfileServiceRequestResponse.getCreditReportResponse_element>();
            response_map_x.put('response_x', response_x);
                WebServiceCallout.invoke(
                    this,
                    request_x,
                    response_map_x,
                    new String[]{endpoint_x,
                        'getCreditReport',
                        'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditProfileMgmtServiceRequestResponse_v1',
                        'getCreditReport',
                        'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditProfileMgmtServiceRequestResponse_v1',
                        'getCreditReportResponse',
                        'CreditProfileServiceRequestResponse.getCreditReportResponse_element'}
                );
                response_x = response_map_x.get('response_x');
                return response_x.businessCreditReportFullDetails;  
        }
        
        public CreditProfileServiceTypes.BusinessCreditReportMetaData[] getCreditReportList(Long businessCustomerId) {
            CreditProfileServiceRequestResponse.getCreditReportList_element request_x = new CreditProfileServiceRequestResponse.getCreditReportList_element();
            CreditProfileServiceRequestResponse.getCreditReportListResponse_element response_x;
            request_x.businessCustomerId = businessCustomerId;
            Map<String, CreditProfileServiceRequestResponse.getCreditReportListResponse_element> response_map_x = new Map<String, CreditProfileServiceRequestResponse.getCreditReportListResponse_element>();
            response_map_x.put('response_x', response_x);
                WebServiceCallout.invoke(
                    this,
                    request_x,
                    response_map_x,
                    new String[]{endpoint_x,
                        'getCreditReportList',
                        'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditProfileMgmtServiceRequestResponse_v1',
                        'getCreditReportList',
                        'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditProfileMgmtServiceRequestResponse_v1',
                        'getCreditReportListResponse',
                        'CreditProfileServiceRequestResponse.getCreditReportListResponse_element'}
                );
                response_x = response_map_x.get('response_x');
                return response_x.businessCreditReportMetaDataList;  
        }
        
        public String ping() {
            System.debug('... dyy ... calling ping() ...');
            CreditProfileServicePing.ping_element request_x = new CreditProfileServicePing.ping_element();
            CreditProfileServicePing.pingResponse_element response_x;
            Map<String, CreditProfileServicePing.pingResponse_element> response_map_x = new Map<String, CreditProfileServicePing.pingResponse_element>();
            response_map_x.put('response_x', response_x);
            //if ( !Test.isRunningTest() )
                WebServiceCallout.invoke(
                    this,
                    request_x,
                    response_map_x,
                    new String[]{endpoint_x,
                        'ping',
                        'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/ping_v1',
                        'ping',
                        'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/ping_v1',
                        'pingResponse',
                        'CreditProfileServicePing.pingResponse_element'}
                );
            response_x = response_map_x.get('response_x');
            String version;
            if(response_x!=null) version = response_x.version;
            return version;
        }
        
        public void removePartner(Long masterBusinessCustomerId,Long partnerCustomerId,CreditProfileServiceTypes.CreditAuditInfo auditInfo) {
            CreditProfileServiceRequestResponse.removePartner_element request_x = new CreditProfileServiceRequestResponse.removePartner_element();
            CreditProfileServiceRequestResponse.removePartnerResponse_element response_x;
            request_x.masterBusinessCustomerId = masterBusinessCustomerId;
            request_x.partnerCustomerId = partnerCustomerId;
            request_x.auditInfo = auditInfo;
            Map<String, CreditProfileServiceRequestResponse.removePartnerResponse_element> response_map_x = new Map<String, CreditProfileServiceRequestResponse.removePartnerResponse_element>();
            response_map_x.put('response_x', response_x);
                WebServiceCallout.invoke(
                    this,
                    request_x,
                    response_map_x,
                    new String[]{endpoint_x,
                        'removePartner',
                        'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditProfileMgmtServiceRequestResponse_v1',
                        'removePartner',
                        'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditProfileMgmtServiceRequestResponse_v1',
                        'removePartnerResponse',
                        'CreditProfileServiceRequestResponse.removePartnerResponse_element'}
                );
            response_x = response_map_x.get('response_x');
        }
        
        public void replaceBusinessProprietor(Long masterBusinessCustomerId,Long proprietorIndividualCustomerId,CreditProfileServiceTypes.CreditAuditInfo auditInfo) {
            CreditProfileServiceRequestResponse.replaceBusinessProprietor_element request_x = new CreditProfileServiceRequestResponse.replaceBusinessProprietor_element();
            CreditProfileServiceRequestResponse.replaceBusinessProprietorResponse_element response_x;
            request_x.masterBusinessCustomerId = masterBusinessCustomerId;
            request_x.proprietorIndividualCustomerId = proprietorIndividualCustomerId;
            request_x.auditInfo = auditInfo;
            Map<String, CreditProfileServiceRequestResponse.replaceBusinessProprietorResponse_element> response_map_x = new Map<String, CreditProfileServiceRequestResponse.replaceBusinessProprietorResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
                this,
                request_x,
                response_map_x,
                new String[]{endpoint_x,
                    'replaceBusinessProprietor',
                    'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditProfileMgmtServiceRequestResponse_v1',
                    'replaceBusinessProprietor',
                    'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditProfileMgmtServiceRequestResponse_v1',
                    'replaceBusinessProprietorResponse',
                    'CreditProfileServiceRequestResponse.replaceBusinessProprietorResponse_element'}
            );
            response_x = response_map_x.get('response_x');
        }
        
        public CreditProfileServiceRequestResponse.searchBusinessCreditProfileResponse_businessProfileSearchResultList_element[] 
            searchBusinessCreditProfile(String registrationIncorporationNum,String secondaryDunsNum,CreditProfileServiceTypes.CreditAuditInfo auditInfo) {
                CreditProfileServiceRequestResponse.searchBusinessCreditProfile_element request_x = new CreditProfileServiceRequestResponse.searchBusinessCreditProfile_element();
                CreditProfileServiceRequestResponse.searchBusinessCreditProfileResponse_element response_x;
                request_x.registrationIncorporationNum = registrationIncorporationNum;
                request_x.secondaryDunsNum = secondaryDunsNum;
                request_x.auditInfo = auditInfo;
                Map<String, CreditProfileServiceRequestResponse.searchBusinessCreditProfileResponse_element> response_map_x = new Map<String, CreditProfileServiceRequestResponse.searchBusinessCreditProfileResponse_element>();
                response_map_x.put('response_x', response_x);
                WebServiceCallout.invoke(
                    this,
                    request_x,
                    response_map_x,
                    new String[]{endpoint_x,
                        'searchBusinessCreditProfile',
                        'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditProfileMgmtServiceRequestResponse_v1',
                        'searchBusinessCreditProfile',
                        'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditProfileMgmtServiceRequestResponse_v1',
                        'searchBusinessCreditProfileResponse',
                        'CreditProfileServiceRequestResponse.searchBusinessCreditProfileResponse_element'}
                );
                response_x = response_map_x.get('response_x');
                return response_x.businessProfileSearchResultList;   
            }
        
        public CreditProfileServiceTypes.IndividualProfile[] searchIndivdualCreditProfile(
            CreditProfileServiceTypes.IndividualCreditIdentification creditIdentification
            ,CreditProfileServiceTypes.CreditAuditInfo auditInfo) {
                CreditProfileServiceRequestResponse.searchIndivdualCreditProfile_element request_x = new CreditProfileServiceRequestResponse.searchIndivdualCreditProfile_element();
                CreditProfileServiceRequestResponse.searchIndivdualCreditProfileResponse_element response_x;
                request_x.creditIdentification = creditIdentification;
                request_x.auditInfo = auditInfo;
                Map<String, CreditProfileServiceRequestResponse.searchIndivdualCreditProfileResponse_element> response_map_x = new Map<String, CreditProfileServiceRequestResponse.searchIndivdualCreditProfileResponse_element>();
                response_map_x.put('response_x', response_x);
                WebServiceCallout.invoke(
                    this,
                    request_x,
                    response_map_x,
                    new String[]{endpoint_x,
                        'searchIndivdualCreditProfile',
                        'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditProfileMgmtServiceRequestResponse_v1',
                        'searchIndivdualCreditProfile',
                        'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditProfileMgmtServiceRequestResponse_v1',
                        'searchIndivdualCreditProfileResponse',
                        'CreditProfileServiceRequestResponse.searchIndivdualCreditProfileResponse_element'}
                );
                response_x = response_map_x.get('response_x');
                return response_x.individualProfileSearchResultList;
            }
        
        public void updateBusinessCreditProfile(
            Long businessCustomerId
            ,CreditProfileServiceTypes.BusinessCreditProfileBase updatedBusinessCreditProfile
            ,Long proprietorshipIndividualCustomerId
            ,CreditProfileServiceTypes.CreditAuditInfo auditInfo
        ) {
            CreditProfileServiceRequestResponse.updateBusinessCreditProfile_element request_x = new CreditProfileServiceRequestResponse.updateBusinessCreditProfile_element();
            CreditProfileServiceRequestResponse.updateBusinessCreditProfileResponse_element response_x;
            request_x.businessCustomerId = businessCustomerId;
            request_x.updatedBusinessCreditProfile = updatedBusinessCreditProfile;
            request_x.proprietorshipIndividualCustomerId = proprietorshipIndividualCustomerId;
            request_x.auditInfo = auditInfo;
            Map<String, CreditProfileServiceRequestResponse.updateBusinessCreditProfileResponse_element> response_map_x = new Map<String, CreditProfileServiceRequestResponse.updateBusinessCreditProfileResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
                this,
                request_x,
                response_map_x,
                new String[]{endpoint_x,
                    'updateBusinessCreditProfile',
                    'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditProfileMgmtServiceRequestResponse_v1',
                    'updateBusinessCreditProfile',
                    'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditProfileMgmtServiceRequestResponse_v1',
                    'updateBusinessCreditProfileResponse',
                    'CreditProfileServiceRequestResponse.updateBusinessCreditProfileResponse_element'}
            );
            response_x = response_map_x.get('response_x');
        }
        
        public void updateIndivdualCreditProfile(
            Long individualCustomerId
            ,CreditProfileServiceTypes.IndividualCreditProfile individualCreditProfile
            ,CreditProfileServiceTypes.CreditAuditInfo auditInfo
        ) {
            CreditProfileServiceRequestResponse.updateIndivdualCreditProfile_element request_x = new CreditProfileServiceRequestResponse.updateIndivdualCreditProfile_element();
            CreditProfileServiceRequestResponse.updateIndivdualCreditProfileResponse_element response_x;
            request_x.individualCustomerId = individualCustomerId;
            request_x.individualCreditProfile = individualCreditProfile;
            request_x.auditInfo = auditInfo;
            Map<String, CreditProfileServiceRequestResponse.updateIndivdualCreditProfileResponse_element> response_map_x = new Map<String, CreditProfileServiceRequestResponse.updateIndivdualCreditProfileResponse_element>();
            response_map_x.put('response_x', response_x); 
            WebServiceCallout.invoke(
                this,
                request_x,
                response_map_x,
                new String[]{endpoint_x,
                    'updateIndivdualCreditProfile',
                    'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditProfileMgmtServiceRequestResponse_v1',
                    'updateIndivdualCreditProfile',
                    'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditProfileMgmtServiceRequestResponse_v1',
                    'updateIndivdualCreditProfileResponse',
                    'CreditProfileServiceRequestResponse.updateIndivdualCreditProfileResponse_element'}
            );
            response_x = response_map_x.get('response_x');
        }
        
        public void voidCreditReport(Long creditReportId,CreditProfileServiceTypes.CreditAuditInfo auditInfo) {
            CreditProfileServiceRequestResponse.voidCreditReport_element request_x = new CreditProfileServiceRequestResponse.voidCreditReport_element();
            CreditProfileServiceRequestResponse.voidCreditReportResponse_element response_x;
            request_x.creditReportId = creditReportId;
            request_x.auditInfo = auditInfo;
            Map<String, CreditProfileServiceRequestResponse.voidCreditReportResponse_element> response_map_x = new Map<String, CreditProfileServiceRequestResponse.voidCreditReportResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
                this,
                request_x,
                response_map_x,
                new String[]{endpoint_x,
                    'voidCreditReport',
                    'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditProfileMgmtServiceRequestResponse_v1',
                    'voidCreditReport',
                    'http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/BusinessCreditProfileMgmtServiceRequestResponse_v1',
                    'voidCreditReportResponse',
                    'CreditProfileServiceRequestResponse.voidCreditReportResponse_element'}
            );
            response_x = response_map_x.get('response_x');
        }
    }
}