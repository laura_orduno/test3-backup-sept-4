/**
 * Helper class for constructing SOQL query strings.
 * 
 * @author Max Rudman
 * @since 7/22/2009
 */
public class QueryBuilder implements Expression {
    private SelectClause selectClause;
    private FromClause fromClause;
    private WhereClause whereClause;
    private OrderByClause orderByClause;
    private GroupByClause groupByClause;
    private LimitClause limitClause;
    
    /**
     * Default constructor.
     */
    public QueryBuilder(Schema.SObjectType stype) {
        this(stype.getDescribe().getName());
    }
    
    /**
     * Default constructor.
     */
    public QueryBuilder(String objectName) {
        selectClause = new SelectClause();
        fromClause = new FromClause(objectName);
    }
    
    /**
     * Returns SELECT clause.
     */
    public SelectClause getSelectClause() {
        return selectClause;
    }
    
    /**
     * Returns FROM clause.
     */
    public FromClause getFromClause() {
        return fromClause;
    }
    
    /**
     * Returns WHERE clause.
     */
    public WhereClause getWhereClause() {
        if (whereClause == null) {
            whereClause = new WhereClause();
        }
        return whereClause;
    }
    
    /**
     * Returns ORDER BY clause.
     */
    public OrderByClause getOrderByClause() {
        if (orderByClause == null) {
            orderByClause = new OrderByClause();
        }
        return orderByClause;
    }
    
    /**
     * Returns LIMIT clause.
     */
    public LimitClause getLimitClause() {
        if (limitClause == null) {
            limitClause = new LimitClause();
        }
        return limitClause;
    }
    
    /**
     * Returns GROUP BY clause.
     */
    public GroupByClause getGroupByClause() {
        if (groupByClause == null) {
            groupByClause = new GroupByClause();
        }
        return groupByClause;
    }
    
    /**
     * Generates SOQL string.
     */
    public String buildSOQL() {
        String soql = selectClause.buildSOQL();
        soql += (' ' + fromClause.buildSOQL());
        if (whereClause != null) {
            soql += (' ' + whereClause.buildSOQL());
        }
        if (orderByClause != null) {
            soql += (' ' + orderByClause.buildSOQL());
        }
        if (limitClause != null) {
            soql += (' ' + limitClause.buildSOQL());
        }
        if (groupByClause != null) {
            soql += (' ' + groupByClause.buildSOQL());
        }
        return soql.trim();
    }
    
    /**
     * Factory method to create EQUALS operator expression.
     */
    public Expression eq(String field, Object value) {
        return new OperatorExpression(field, EQUALS, value);
    }
    
    /**
     * Factory method to create EQUALS operator expression.
     */
    public Expression eq(Schema.SObjectField field, Object value) {
        return eq(String.valueOf(field), value);
    }
    
    /**
     * Factory method to create NOT EQUALS operator expression.
     */
    public Expression neq(String field, Object value) {
        return new OperatorExpression(field, NOT_EQUALS, value);
    }
    
    /**
     * Factory method to create NOT EQUALS operator expression.
     */
    public Expression neq(Schema.SObjectField field, Object value) {
        return neq(String.valueOf(field), value);
    }
    
    /**
     * Factory method to create GREATER THAN operator expression.
     */
    public Expression gt(String field, Object value) {
        return new OperatorExpression(field, GREATER_THAN, value);
    }
    
    /**
     * Factory method to create GREATER THAN operator expression.
     */
    public Expression gt(Schema.SObjectField field, Object value) {
        return gt(String.valueOf(field), value);
    }
    
    /**
     * Factory method to create GREATER EQUALS operator expression.
     */
    public Expression gte(String field, Object value) {
        return new OperatorExpression(field, GREATER_EQUALS, value);
    }
    
    /**
     * Factory method to create GREATER EQUALS operator expression.
     */
    public Expression gte(Schema.SObjectField field, Object value) {
        return gte(String.valueOf(field), value);
    }
    
    /**
     * Factory method to create LESS THAN operator expression.
     */
    public Expression lt(String field, Object value) {
        return new OperatorExpression(field, LESS_THAN, value);
    }
    
    /**
     * Factory method to create LESS THAN operator expression.
     */
    public Expression lt(Schema.SObjectField field, Object value) {
        return lt(String.valueOf(field), value);
    }
    
    /**
     * Factory method to create LESS EQUALS operator expression.
     */
    public Expression lte(String field, Object value) {
        return new OperatorExpression(field, LESS_EQUALS, value);
    }
    
    /**
     * Factory method to create LESS EQUALS operator expression.
     */
    public Expression lte(Schema.SObjectField field, Object value) {
        return lte(String.valueOf(field), value);
    }
    
    /**
     * Factory method to create LIKE operator expression.
     */
    public Expression lke(String field, Object value) {
        return new OperatorExpression(field, LKE, value);
    }
    
    /**
     * Factory method to create LIKE operator expression.
     */
    public Expression lke(Schema.SObjectField field, Object value) {
        return lke(String.valueOf(field), value);
    }
    
    /**
     * Factory method to create AND expression.
     */
    public AndExpression land() {
        return new AndExpression();
    }
    
    /**
     * Factory method to create OR expression.
     */
    public OrExpression lor() {
        return new OrExpression();
    }
    
    /**
     * Factory method to create IN expression.
     */
    public InExpression inExpr(Schema.SObjectField field, Set<Id> values) {
        return new InExpression(field.getDescribe().getName(), values);
    }
    
    /**
     * Factory method to create IN expression.
     */
    public InExpression inExpr(String field, Set<Id> values) {
        return new InExpression(field, values);
    }
    
    /**
     * Factory method to create IN expression.
     */
    public InExpression inExpr(Schema.SObjectField field, Set<String> values) {
        return new InExpression(field.getDescribe().getName(), values);
    }
    
    /**
     * Factory method to create IN expression.
     */
    public InExpression inExpr(String field, Set<String> values) {
        return new InExpression(field, values);
    }
    
    /**
     * Factory method to create IN expression.
     */
    public InExpression inExpr(Schema.SObjectField field, Set<Decimal> values) {
        return new InExpression(field.getDescribe().getName(), values);
    }
    
    /**
     * Factory method to create IN expression.
     */
    public InExpression inExpr(String field, Set<Decimal> values) {
        return new InExpression(field, values);
    }
    
    /**
     * Factory method to create IN expression.
     */
    public InExpression inExpr(Schema.SObjectField field, Expression subQuery) {
        return new InExpression(field.getDescribe().getName(), subQuery);
    }
    
    /**
     * Factory method to create IN expression.
     */
    public InExpression inExpr(String field, Expression subQuery) {
        return new InExpression(field, subQuery);
    }
    
    /**
     * Factory method to create INCLUDES operator expression.
     */
    public Expression includes(String field, Set<String> values) {
        return new IncludesExpression(field, values);
    }
    
    /**
     * Factory method to create EXCLUDES operator expression.
     */
    public Expression includes(Schema.SObjectField field, Set<String> values) {
        return includes(String.valueOf(field), values);
    }
    
    /**
     * Factory method to create INCLUDES operator expression.
     */
    public Expression excludes(String field, Set<String> values) {
        return new ExcludesExpression(field, values);
    }
    
    /**
     * Factory method to create EXCLUDES operator expression.
     */
    public Expression excludes(Schema.SObjectField field, Set<String> values) {
        return excludes(String.valueOf(field), values);
    }
    
    /**
     * Joins specified array of Expression values using specified separator. 
     */
    private static String join(List<Expression> values, String separator) {
        // Handle null values
        if ((values == null) || (values.size() == 0)) {
            return '';
        }
        if (separator == null) {
            separator = '';
        }
        
        String result = '';
        for (Expression value : values) {
            if (value == null) {
                continue;
            }
            String fragment = value.buildSOQL();
            if (value instanceof QueryBuilder) {
                // This is a sub-qury. Enclose in paranthesis
                fragment = '(' + fragment + ')';
            }
            result += (fragment + separator);
        }
        // Chop off trailing separator
        result = result.substring(0,result.length() - separator.length());
        return result;
    }
    
    
    /**
     * Contract for a class capabale of generating SOQL expressions.
     */
    public interface Expression {
        /**
         * Generates SOQL fragment for this expression.
         */
        String buildSOQL();
    }
    
    /**
     * Expression implementation that supports nested expressions.
     */
    public abstract class ComplexExpression implements Expression {
        private List<Expression> expressions = new List<Expression>();
        
        /**
         * Adds specified expression to the list of expressions maintained by this clause.
         */
        public ComplexExpression addExpression(Expression expr) {
            if (expr != null) {
                expressions.add(expr);
            }
            return this;
        }
        
        /**
         * Returns the list of expressions accumulated by this clause.
         */
        public List<Expression> getExpressions() {
            return expressions;
        }
    }
    
    /**
     * Expression implementation that knows how to build SELECT clause.
     */
    public class SelectClause extends ComplexExpression {
        private Set<String> existing = new Set<String>();
        
        /**
         * Convenience method to add a field.
         */
        public void addField(Schema.SObjectField field) {
            addField(field.getDescribe().getName());
        }
        
        /**
         * Convenience method to add a field.
         */
        public void addField(String field) {
            if (!existing.contains(field.toLowerCase())) {
                addExpression(new FieldExpression(field));
                existing.add(field.toLowerCase());
            }
        }
        
        /**
         * Convenience method to add a field.
         */
        public void addField(Schema.SObjectField relationship, Schema.SObjectField field) {
            // SteelBrick CPQ version 23 removed SBQQ__Opportunity__c Master-detail field and
            // replaced it with SBQQ__Opportunity2__c Lookup field.
            // In order to maintain backward compatability we need to reference legacy SBQQ__Opportunity__c field
            // such that dependency on specific version of SBCPQ (v14.x) can be created.
            // Otherwise, SFDC looks at the most recent version of the package which doesn't have this field.
            Schema.SObjectField oppField = SBQQ__Quote__c.SBQQ__Opportunity__c;
            addField(relationship.getDescribe().getRelationshipName(), field.getDescribe().getName());
        }
        
        /**
         * Convenience method to add a field.
         */
        public void addField(Schema.SObjectField relationship, String fieldName) {
            addField(relationship.getDescribe().getRelationshipName(), fieldName);
        }
        
        /**
         * Convenience method to add a field.
         */
        public void addField(Schema.SObjectField r1, Schema.SObjectField r2, Schema.SObjectField field) {
            addField(r1.getDescribe().getRelationshipName() + '.' + r2.getDescribe().getRelationshipName(), field.getDescribe().getName());
        }
        
        /**
         * Convenience method to add a field.
         */
        public void addField(String relationship, String field) {
            FieldExpression expr = new FieldExpression(relationship, field);
            if (!existing.contains(expr.buildSOQL().toLowerCase())) {
                addExpression(expr);
                existing.add(expr.buildSOQL().toLowerCase());
            }
        }
        
        /**
         * Convenience method to add several fields expressed as Schema.SObjectField
         */
        public void addFields(Schema.SObjectField[] fields) {
            for (Schema.SObjectField field : fields) {
                addField(field);
            }
        }
        
        /**
         * Convenience method to add several fields expressed as String
         */
        public void addFields(String[] fields) {
            for (String field : fields) {
                addField(field);
            }
        }
        
        /**
         * Convenience method to add several fields expressed as String
         */
        public void addFields(Schema.FieldSetMember[] members) {
            for (Schema.FieldSetMember member : members) {
                addField(member.getFieldPath());
            }
        }
        
        /**
         * Generates SOQL for the WHERE clause.
         */
        public String buildSOQL() {
            return 'SELECT ' + QueryBuilder.join(getExpressions(), ',');
        }
    }
    
    /**
     * Expression implementation that knows how to build FROM clause.
     */
    public class FromClause implements Expression {
        private String objectName;
        
        /**
         * Default constructor;
         */
        public FromClause(String objectName) {
            this.objectName = objectName;
        }
        
        /**
         * Getter for 'objectName' property.
         */
        public String getObjectName() {
            return objectName;
        }
        
        /**
         * Setter for 'objectName' property.
         */
        public void setObjectName(String objectName) {
            this.objectName = objectName;
        }
        
        /**
         * Generates SOQL for the WHERE clause.
         */
        public String buildSOQL() {
            return 'FROM ' + objectName;
        }
    }
    
    /**
     * Expression implementation that knows how to build WHERE clause.
     */
    public class WhereClause extends ComplexExpression  {
        /**
         * Generates SOQL for the WHERE clause.
         */
        public String buildSOQL() {
            if (getExpressions().size() == 0) {
                return '';
            }
            
            return 'WHERE ' + QueryBuilder.join(getExpressions(), ' AND ');
        }
    }
    
    /**
     * Expression implementation that knows how to build ORDER BY clause.
     */
    public class OrderByClause extends ComplexExpression  {
        
        /**
         * Adds ascending sort on specified field.
         */
        public void addAscending(String field) {
            addExpression(new OrderByExpression(field, false));
        }
        
        /**
         * Adds ascending sort on specified field.
         */
        public void addAscending(Schema.SObjectField field) {
            addAscending(String.valueOf(field));
        }
        
        /**
         * Adds descending sort on specified field.
         */
        public void addDescending(String field) {
            addExpression(new OrderByExpression(field, true));
        }
        
        /**
         * Adds descending sort on specified field.
         */
        public void addDescending(Schema.SObjectField field) {
            addDescending(String.valueOf(field));
        }
        
        /**
         * Generates SOQL for the ORDER BY clause.
         */
        public String buildSOQL() {
            if (getExpressions().size() == 0) {
                return '';
            }
            
            return 'ORDER BY ' + QueryBuilder.join(getExpressions(), ',');
        }
    }
    
    /**
     * Clause implementation that knows how to build LIMIT clause.
     */
    public class LimitClause implements Expression  {
        private Integer value;
        
        /**
         * Getter for 'value' property.
         */
        public Integer getValue() {
            return value;
        }
        
        /**
         * Setter for 'value' property.
         */
        public void setValue(Integer value) {
            this.value = value;
        }
        
        public String buildSOQL() {
            if (value == null) {
                return '';
            }
            return 'LIMIT ' + value;
        }
    }
    
    /**
     * Expression implementation that knows how to build SELECT clause.
     */
    public class GroupByClause extends ComplexExpression {
        private Set<String> existing = new Set<String>();
        
        /**
         * Convenience method to add a field.
         */
        public void addField(Schema.SObjectField field) {
            addField(field.getDescribe().getName());
        }
        
        /**
         * Convenience method to add a field.
         */
        public void addField(String field) {
            if (!existing.contains(field.toLowerCase())) {
                addExpression(new FieldExpression(field));
                existing.add(field.toLowerCase());
            }
        }
        
        /**
         * Convenience method to add several fields expressed as Schema.SObjectField
         */
        public void addFields(Schema.SObjectField[] fields) {
            for (Schema.SObjectField field : fields) {
                addField(field);
            }
        }
        
        /**
         * Convenience method to add several fields expressed as String
         */
        public void addFields(String[] fields) {
            for (String field : fields) {
                addField(field);
            }
        }
        
        /**
         * Generates SOQL for the WHERE clause.
         */
        public String buildSOQL() {
            return 'GROUP BY ' + QueryBuilder.join(getExpressions(), ',');
        }
    }
    
    /**
     * Expression implementation that knows how to deal with "order by" expressions.
     */
    public class OrderByExpression implements Expression {
        private String field;
        private Boolean descending;

        /**
         * Default constructor.
         */
        public OrderByExpression(String field, Boolean descending) {
            this.field = field;
            this.descending = descending;
            if (this.descending == null) {
                this.descending = false;
            }
        }
        
        /**
         * Generates SOQL fragment for "Order By" expression.
         */
        public String buildSOQL() {
            if (descending) {
                return field + ' DESC';
            } else {
                return field + ' ASC';
            }
        }
    }
    
    /**
     * Expression implementation that knows how to deal with logical AND expressions.
     */
    public class AndExpression extends ComplexExpression {
        /**
         * Generates SOQL fragment.
         */
        public String buildSOQL() {
            return '(' + QueryBuilder.join(getExpressions(), ' AND ') + ')';
        }
    }
    
    /**
     * Expression implementation that knows how to deal with logical OR expressions.
     */
    public class OrExpression extends ComplexExpression {
        /**
         * Generates SOQL fragment.
         */
        public String buildSOQL() {
            return '(' + QueryBuilder.join(getExpressions(), ' OR ') + ')';
        }
    }
    
    /**
     * Expression implementation that knows how to deal with operator expressions.
     */
    public class OperatorExpression implements Expression {
        private String field;
        private String operator;
        private Object value;
        
        /**
         * Default constructor.
         */
        public OperatorExpression(String field, String operator, Object value) {
            this.field = field;
            this.operator = operator;
            this.value = value;
        }
        
        /**
         * Generates SOQL fragment.
         */
        public String buildSOQL() {
            Object escapedValue = value;
            if (value != null) {
                if (value instanceof String) {
                    escapedValue = '\'' + String.escapeSingleQuotes((String)value) + '\'';
                } else if (value instanceof Date) {
                    // Chop off time portion
                    escapedValue = String.valueOf(value).split(' ')[0];
                }
            }
            return field + ' ' + operator + ' ' + escapedValue;
        }
    }
    
    /**
     * Simple Expression implementation that constructs a field path.
     */
    public class FieldExpression implements Expression {
        private String field;
        private String relationship;
        
        /**
         * Default constructor.
         */
        public FieldExpression(String relationship, String field) {
            this.field = field;
            this.relationship = relationship;
        }
        
        public FieldExpression(String field) {
            this(null,field);
        }
        
        /**
         * Generates SOQL fragment.
         */
        public String buildSOQL() {
            if (relationship != null) {
                return relationship + '.' + field;
            } else {
                return field;
            }
        }
    }
    
    /**
     * Simple expression implementation that wraps a value.
     */
    public class ValueExpression implements Expression {
        private String soql;

        public ValueExpression(Id id) {
            soql = '\'' + id + '\'';
        }
        
        public ValueExpression(String value) {
            soql = '\'' + String.escapeSingleQuotes(value) + '\'';
        }
        
        public ValueExpression(Decimal value) {
            soql = String.valueOf(value);
        }
        
        /**
         * Generates SOQL fragment.
         */
        public String buildSOQL() {
            return soql;
        }
    }
    
    /**
     * Expression implementation that knows how to deal with IN operator.
     */
    public class InExpression implements Expression {
        private String soql;
        
        public InExpression(String field, Set<Id> values) {
            List<Expression> exprs = new List<Expression>();
            for (Id value : values) {
                exprs.add(new ValueExpression(value));
            }
            soql = field + ' IN (' + QueryBuilder.join(exprs, ',') + ')';
        }
        
        public InExpression(String field, Set<String> values) {
            List<Expression> exprs = new List<Expression>();
            for (String value : values) {
                exprs.add(new ValueExpression(value));
            }
            soql = field + ' IN (' + QueryBuilder.join(exprs, ',') + ')';
        }
        
        public InExpression(String field, Set<Decimal> values) {
            List<Expression> exprs = new List<Expression>();
            for (Decimal value : values) {
                exprs.add(new ValueExpression(value));
            }
            soql = field + ' IN (' + QueryBuilder.join(exprs, ',') + ')';
        }
        
        public InExpression(String field, Expression subQuery) {
            soql = field + ' IN (' + subQuery.buildSOQL() + ')';
        }
        
        /**
         * Generates SOQL fragment.
         */
        public String buildSOQL() {
            return soql;
        }
    }
    
    /**
     * Expression implementation that knows how to deal with INCLUDES operator.
     */
    public class IncludesExpression implements Expression {
        private String soql;
        
        public IncludesExpression(String field, Set<String> values) {
            List<Expression> exprs = new List<Expression>();
            for (String value : values) {
                exprs.add(new ValueExpression(value));
            }
            soql = field + ' INCLUDES (' + QueryBuilder.join(exprs, ',') + ')';
        }
        
        /**
         * Generates SOQL fragment.
         */
        public String buildSOQL() {
            return soql;
        }
    }
    
    /**
     * Expression implementation that knows how to deal with EXCLUDES operator.
     */
    public class ExcludesExpression implements Expression {
        private String soql;
        
        public ExcludesExpression(String field, Set<String> values) {
            List<Expression> exprs = new List<Expression>();
            for (String value : values) {
                exprs.add(new ValueExpression(value));
            }
            soql = field + ' EXCLUDES (' + QueryBuilder.join(exprs, ',') + ')';
        }
        
        /**
         * Generates SOQL fragment.
         */
        public String buildSOQL() {
            return soql;
        }
    }
    
    public static final String EQUALS = '=';
    public static final String NOT_EQUALS = '!=';
    public static final String GREATER_THAN = '>';
    public static final String GREATER_EQUALS = '>=';
    public static final String LESS_THAN = '<';
    public static final String LESS_EQUALS = '<=';
    public static final String LKE = 'LIKE';
}