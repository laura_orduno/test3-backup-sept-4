/**
 * Calculates summary variables
 */
public class SummaryVariableCalculator {
	private List<SBQQ__SummaryVariable__c> variables;
	private Map<Id,Decimal> results;
	
	public SummaryVariableCalculator(List<SBQQ__SummaryVariable__c> variables) {
		this.variables = variables;
		results = new Map<Id,Decimal>();
	}
	
	public void calculate(SObject quote, List<SObject> records) {
		if (records.isEmpty()) {
			return;
		}
		
		Schema.SObjectType targetType = records[0].getSObjectType();
		for (SBQQ__SummaryVariable__c var : variables) {
			Decimal result = calculateVariable(var, quote, records);
			results.put(var.Id, result);
		}
	}
	
	public Decimal getResult(Id variableId) {
		return results.get(variableId);
	}
	
	private Decimal calculateVariable(SBQQ__SummaryVariable__c var, SObject quote, List<SObject> records) {
		Decimal cnt = 0;
		Decimal sum = 0;
		Decimal min = 0;
		Decimal max = 0;
		
		if (var.SBQQ__TargetObject__c == null) {
			var.SBQQ__TargetObject__c = 'Quote Line';
		}
		Schema.SObjectField aggregateField = AliasedMetaDataUtils.getField(var.SBQQ__TargetObject__c, var.SBQQ__AggregateField__c);
		
		Operators.Logical op = Operators.getInstance(var.SBQQ__Operator__c);
		if ((op == null) && (var.SBQQ__FilterField__c != null)) {
			throw new SummaryVariableException('Invalid operator: ' + var.SBQQ__Operator__c + ' [' + var.Id + ']');
		}
		
		Boolean recordSetEmpty = true;
		for (SObject rec : records) {
			if (rec.getSObjectType() == AliasedMetaDataUtils.getType(var.SBQQ__TargetObject__c)) {
				recordSetEmpty = false;
				if (var.SBQQ__FilterField__c != null) {
					Schema.SObjectField field = AliasedMetaDataUtils.getField(rec.getSObjectType(), var.SBQQ__FilterField__c);
					if ((field != null) && !op.evaluate(rec.get(field), var.SBQQ__FilterValue__c)) {
						continue;
					}
					if ((var.SBQQ__ConstraintField__c != null) && !isConstraintMet(var, quote, rec)) {
						continue;
					}
				}
				
				Object aggregateValue = rec.get(aggregateField);
				if (aggregateValue != null) {
					if (!var.SBQQ__AggregateFunction__c.equalsIgnoreCase('Count')) {
						try {
							Decimal aggregateDecimal = (Decimal)aggregateValue;
							sum += aggregateDecimal;
							min = Math.min(min, aggregateDecimal);
							max = Math.max(max, aggregateDecimal);
						} catch (TypeException te) {
						}
					}
					cnt++;
				}
			}
		}
		
		if (recordSetEmpty) {
			return null;
		}
		
		if (var.SBQQ__AggregateFunction__c == null) {
			throw new SummaryVariableException('Missing aggregate function. [' + var.Id + ']');
		} else if (var.SBQQ__AggregateFunction__c.equalsIgnoreCase('Count')) {
			return cnt;
		} else if (var.SBQQ__AggregateFunction__c.equalsIgnoreCase('Sum')) {
			return sum;
		} else if (var.SBQQ__AggregateFunction__c.equalsIgnoreCase('Average')) {
			return (cnt > 0) ? sum / cnt : 0;
		} else if (var.SBQQ__AggregateFunction__c.equalsIgnoreCase('Min')) {
			return min;
		} else if (var.SBQQ__AggregateFunction__c.equalsIgnoreCase('Max')) {
			return max;
		} else {
			throw new SummaryVariableException('Unspported aggregate function: ' + var.SBQQ__AggregateFunction__c + '[' + var.Id + ']');
		}
	}
	
	private Boolean isConstraintMet(SBQQ__SummaryVariable__c var, SObject quote, SObject rec) {
		Schema.SObjectField recField = AliasedMetaDataUtils.getField(rec.getSObjectType(), var.SBQQ__ConstraintField__c);
		Schema.SObjectField quoteField = AliasedMetaDataUtils.getField(SBQQ__Quote__c.sObjectType, var.SBQQ__ConstraintField__c);
		if ((recField != null) && (quoteField != null)) {
			return Operators.getInstance('equals').evaluate(rec.get(recField), quote.get(quoteField));
		}
		return false;
	}
	
	public Set<String> getReferencedFields() {
		Set<String> fields = new Set<String>();
		for (SBQQ__SummaryVariable__c var : variables) {
			if (var.SBQQ__AggregateField__c != null) {
				fields.add(var.SBQQ__AggregateField__c.toLowerCase());
			}
			if (var.SBQQ__FilterField__c != null) {
				fields.add(var.SBQQ__FilterField__c.toLowerCase());
			}
		}
		return fields;
	}
	
	public class SummaryVariableException extends Exception {}
}