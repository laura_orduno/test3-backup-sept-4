/*
###########################################################################
# Last Modified by......: Anthony Michael
# Last Modified Date....: 08-Jan-2016
# Description...........: changed smb_ContractManagementSvcTypes_v2 to cmsv3_ContractManagementSvcTypes_v3 
############################################################################
#############################################################################################################
# Last Modified by......: Nitin Saxena
# Last Modified Date....: 14-Jun-2016
# Description...........: changed smb_ContractManagementSvcTypes_v3 to cms3_1ContractManagementSvcTypes_v3.1.
  The essence of the change was to accommodate a parameter type change (long to string) for 
  the findContractAmendmentsByAssociateNum() operation on the CMS web service endpoint.  A new operation was introduced on CMS V3, 
  findContractAmendmentsByAssociateNumber(), in order to accommodate this version change. 
  Due to SOA Governance policies, the web service endpoint was not to be versioned.  But instead, adding two new operations that 
  referenced an updated XSD version of 3.1 was implemented by the BTO CMS Development team.  As a result, we are still 
  naming this service within SFDC as 3.1 to show that there was a change to the service endpoint contract. 
  Technically, adding operations should result in a new end point version.  But, BTO decided against this in order to meet
  the TQ project timelines.  This is why you may see the endpoint URL pointing to version 3.0 of the web service, 
    even though it is actually version 3.1 due to the addition of two new operations
##################################################################################################################################


*/
global class smb_Amendments implements Comparable {

    
    global String contractId;    
    global String contractNum{set;get;}
    global String contractAssociateNum{set;get;}
    global String contractAmendmentNum{set;get;}
    global String contractAmendmentNumber{set;get;}
    global String signedDate{set;get;}
    global String registeredDate{set;get;}
    global string effectiveDate{set;get;}
    global String expiryDate{set;get;}
    global String contractType{set;get;}
    global String contractStatus{set;get;}
    global string contractTerm{set;get;}   
    global string signator{set;get;}
     
    global Integer CompareTo(Object compareTo) {
        smb_Amendments contractToCompare = (smb_Amendments)compareTo;

        if (contractAmendmentNumber == null) 
            return 1;
        if (contractToCompare == null || contractToCompare.contractAmendmentNumber == null)    
            return -1;
        if (contractAmendmentNumber > contractToCompare.contractAmendmentNumber)
            return 1;
        if (contractAmendmentNumber == contractToCompare.contractAmendmentNumber)
            return 0;
        return -1;
    }
    
    global smb_Amendments() {}

    // changed the below method from smb_ContractManagementSvcTypes_v2 to smb_ContractManagementSvcTypes_v3 
    public smb_Amendments (cms3_1ContractManagementSvcTypes_v3.ContractAmendment Amendment) {
   // public smb_Amendments (cmsv3_ContractManagementSvcTypes_v3.ContractAmendment Amendment) {
        this.contractId = Amendment.contractId;
        this.contractAmendmentNumber = zeroFill(Amendment.contractAmendmentNumber);
        if(Amendment.signedDate<>null && Amendment.signedDate<>'')
        this.signedDate = formatDateData(Amendment.signedDate);
        //this.signedDate = parseDateAndFormat(Amendment.signedDate);
        if(Amendment.registeredDate<>null && Amendment.registeredDate<>'')
        this.registeredDate = formatDateData(Amendment.registeredDate);
        //this.registeredDate = parseDateAndFormat(Amendment.registeredDate);
        if(Amendment.effectiveDate<>null && Amendment.effectiveDate<>'')
        this.effectiveDate = formatDateData(Amendment.effectiveDate);  
       // this.effectiveDate = parseDateAndFormat(Amendment.effectiveDate);
       if(Amendment.expiryDate<>null && Amendment.expiryDate<>'')
        this.expiryDate = formatDateData(Amendment.expiryDate);  
      //  this.expiryDate = parseDateAndFormat(Amendment.expiryDate);
    this.contractType = Amendment.contractType;
        this.contractStatus = Amendment.contractStatus;
        this.contractTerm = (Amendment.contractTermUnit == 'Day'
                            ?Amendment.contractTerm + '(D)'
                            :Amendment.contractTerm);
        if (null != Amendment.customerSignor)
        {
            system.debug('------Amendment.customerSignor.firstName-----'+Amendment.customerSignor.firstName);
            system.debug('------Amendment.customerSignor.firstName-----'+Amendment.customerSignor.firstName != null ? Amendment.customerSignor.firstName:'');
        this.signator =  (Amendment.customerSignor.title != null ? Amendment.customerSignor.title+' ':' ')
                        
                        + (Amendment.customerSignor.firstName != null ? Amendment.customerSignor.firstName+' ':' ')
                        +  (Amendment.customerSignor.lastName != null ? Amendment.customerSignor.lastName:'');
        }
    }
  
  @TestVisible  private Date parseDate(string dateValue) {
        if (dateValue == null) return null;
        
        return  ((Datetime)json.deserialize(dateValue, Date.class)).date();
    }
    
 @TestVisible   private String parseDateAndFormat(string dateValue) {
        if (dateValue == null) return null;
        
        return smb_DateUtility.formatDate(parseDate(dateValue));
    }
 @TestVisible   private String parseDateAndFormat(Date dateValue) {
        if (dateValue == null) return null;
        
        return smb_DateUtility.formatDate(dateValue); 
    }
    
 @TestVisible   private Decimal parseDecimal(string decimalValue) {
        if (decimalValue == null) return null;
        
        return Decimal.valueOf(decimalValue);
    }
 @TestVisible   private string zeroFill(string value) {
        if (Value == null) return '000';
        
        if(value.length()==1)
            return '00'+value;
        else if (value.length()==2)
        return '0'+value;
        else if (value == '')
        return '000';
        else
        return value;
    }

    public static string formatDateData(string strDate){
   string strDat=strDate;
    strDat=strDat.left(10);
String[] strLLL=strDat.split('-');
datetime myDate= Datetime.newInstance(integer.valueof(strLLL[0]),integer.valueof(strLLL[1]),integer.valueof(strLLL[2]));
    string monthName='';
    String finalDate='';
    integer month=myDate.date().month();
    integer intdate=myDate.date().day();
    integer year=myDate.date().year();
    if(month==1)
        monthName='Jan';
    if(month==2)
        monthName='Feb';
    if(month==3)
        monthName='Mar';
    if(month==4)
        monthName='Apr';
    if(month==5)
        monthName='May';
    if(month==6)
        monthName='Jun';
    if(month==7)
        monthName='Jul';
    if(month==8)
        monthName='Aug';
    if(month==9)
        monthName='Sep';
    if(month==10)
        monthName='Oct';
    if(month==11)
        monthName='Nov';
    if(month==12)
        monthName='Dec';
    finalDate= monthName+' '+string.valueOf(intdate)+','+ string.valueOf(year);
    return finalDate;
}
}