@isTest
public with sharing class AbstractDAOTest extends AbstractDAO {
	
	public void saveChildrenExtern(Schema.SObjectField parentField, List<SObject> children) {
		saveChildren(parentField, children);
	}
	
	public void addCustomFieldsExtern(QueryBuilder query, Schema.SObjectType type) {
		addCustomFields(query, type);
	}
	
	private static testMethod void testSaveChildren() {
		Account a = new Account(name = 'test');		
		insert a;
		
		Contact c1 = new Contact(lastname = 'lname1', accountId = a.id);
		Contact c2 = new Contact(lastname = 'lname2', accountId = a.id);
		
		List<Contact> contacts = new List<Contact>{ c1, c2 };
		insert contacts;
		
		contacts.add(new Contact(lastname = 'lname3', accountId = a.id));
		
		AbstractDAOTest dao = new AbstractDAOTest();
		
		dao.saveChildrenExtern(Contact.accountId, contacts);
		
		Integer contactCount = [SELECT count() FROM Contact WHERE AccountId = :a.id];
		
		system.assertEquals(3, contactCount);
	}
	
	private static testMethod void testSaveChildrenEmpty() {
		AbstractDAOTest dao = new AbstractDAOTest();
		
		dao.saveChildrenExtern(null, new List<SObject>());
	}
	
	private static testMethod void testAddCustomFieldsExt() {	
		AbstractDAOTest dao = new AbstractDAOTest();
		
		QueryBuilder q = new QueryBuilder(Account.getSObjectType());
		
		dao.addCustomFieldsExtern(q, Account.getSObjectType());
	}
}