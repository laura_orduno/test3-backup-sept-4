@isTest(SeeAllData=false)
public class OC_ProductFeasibilityImplTest {
    private static void createData(){
        System.runAs(new User(Id = Userinfo.getUserId())) {
            OrdrTestDataFactory.createOrderingWSCustomSettings('ProductEligibilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/RMO/ResourceMgmt/CheckGenericTechnicalAvailabilitySvc_v2_2_vs1');
            
         
        }
    }
    @isTest
    private static void testCheckProductFeasibility0() {
        createData();
        List<SMBCare_Address__c> addresses = OrdrTestDataFactory.createAddresses();
        Id serviceAddressId = addresses[0].Id;
       // OrdrTestDataFactory.createOrderingWSCustomSettings('TechnicalAvailabilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/RMO/ResourceMgmt/CheckGenericTechnicalAvailabilitySvc_v2_2_vs1');
         String orderId=OrdrTestDataFactory.singleMethodForOrderId();   
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('orderId',orderId);
        TpFulfillmentCustomerOrderV3.CustomerOrder co = OrdrProductFeasibilityWsCallout.performCustomerOrderFeasibility(inputMap);
        Test.stopTest();
    }
    @isTest
    private static void testCheckProductFeasibility1() {
        createData();
        List<SMBCare_Address__c> addresses = OrdrTestDataFactory.createAddresses();
        Id serviceAddressId = addresses[0].Id;
       // OrdrTestDataFactory.createOrderingWSCustomSettings('TechnicalAvailabilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/RMO/ResourceMgmt/CheckGenericTechnicalAvailabilitySvc_v2_2_vs1');
         String orderId=OrdrTestDataFactory.singleMethodForOrderId(); 
        List<OrderItem> oiList=[select id,vlocity_cmt__ParentItemId__c from orderitem where orderid=:orderId];
        String parentId=null;
        for(OrderItem oiInstance:oiList){
            if(String.isBlank(parentId)){
                parentId=oiInstance.id;
            } else {
                oiInstance.vlocity_cmt__ParentItemId__c=parentId;
            }
        }
        update oiList;
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('orderId',orderId);
        TpFulfillmentCustomerOrderV3.CustomerOrder co = OrdrProductFeasibilityWsCallout.performCustomerOrderFeasibility(inputMap);
        Test.stopTest();
    }

        @isTest
    private static void testCheckProductFeasibility2() {
        createData();
        List<SMBCare_Address__c> addresses = OrdrTestDataFactory.createAddresses();
        Id serviceAddressId = addresses[0].Id;
       // OrdrTestDataFactory.createOrderingWSCustomSettings('TechnicalAvailabilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/RMO/ResourceMgmt/CheckGenericTechnicalAvailabilitySvc_v2_2_vs1');
         String orderId=OrdrTestDataFactory.singleMethodForOrderId(); 
        List<OrderItem> oiList=[select id,vlocity_cmt__ParentItemId__c from orderitem where orderid=:orderId];
        String parentId=null;
        for(OrderItem oiInstance:oiList){
            if(String.isBlank(parentId)){
                parentId=oiInstance.id;
            } else {
                oiInstance.vlocity_cmt__ParentItemId__c=parentId;
            }
        }
        update oiList;
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('orderId',orderId);
        TpFulfillmentCustomerOrderV3.CustomerOrder co = OrdrProductFeasibilityWsCallout.performCustomerOrderFeasibility(inputMap);
        Test.stopTest();
    }
    private static testMethod void testCheckProductFeasibility() {
        /*
        List<SMBCare_Address__c> addresses = OrdrTestDataFactory.createAddresses();
        Id serviceAddressId = addresses[0].Id;
        OrdrTestDataFactory.createOrderingWSCustomSettings('TechnicalAvailabilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/RMO/ResourceMgmt/CheckGenericTechnicalAvailabilitySvc_v2_2_vs1');
            
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        Map<String, Object> inputMap = new Map<String, Object>();
        TpFulfillmentCustomerOrderV3.CustomerOrder co = OrdrProductFeasibilityWsCallout.performCustomerOrderFeasibility(inputMap);
        Test.stopTest();
*/
        OrdrTestDataFactory.createOrderingWSCustomSettings('ProductEligibilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/RMO/ResourceMgmt/checkGenericTechnicalAvailability');

        OC_ProductFeasibilityImpl componentFeasibility = new OC_ProductFeasibilityImpl();
        Map<String, Object> input = new Map<String, Object>();
        Map<String, Object> output = new Map<String, Object>();
        Map<String, Object> options = new Map<String, Object>();
        
        //List<Order> orders = OrdrTestDataFactory.createOrdersWithServiceAddress();
        //input.put('orderId', orders[0].Id);
        input.put(OrdrConstants.ADDR_ISFIFA,'Yes');
        List<Opportunity> opps = OrdrTestDataFactory.createOpprtunities(1);
        //List<Quote> quotes = OrdrTestDataFactory.createQuotesWithServiceAddress();
        List<SMBCare_Address__c> addresses = OrdrTestDataFactory.createAddresses();
        Id pricebookId = Test.getStandardPricebookId();

        List<Order> orders = new List<Order>();
        for(Integer j=0; j<addresses.size(); j++) {    
            orders.add(new Order(Name='OR-0000'+j, AccountId=opps[0].AccountId, OpportunityId=opps[0].Id, 
                                 //vlocity_cmt__QuoteId__c=quotes[0].Id,
                                 Service_Address__c=addresses[j].Id,
                                 EffectiveDate=System.today(), status='Draft', orderMgmtId__c = '12345678'+j,
                                 Pricebook2Id=priceBookId));
        }
        insert orders;
        input.put('orderId', orders[0].Id);

        /* 
        for(Order ord : orders) {
            input.put('orderId',ord.Id);
        }
        Product2 product = new Product2(name = 'Product A', productcode = 'CODE A', isActive = true, ocom_shippable__c='Yes', Sellable__c=true, orderMgmtId__c='987654321');
        insert product;        
        PriceBookEntry pbe = new PriceBookEntry(product2id = product.id, unitprice = 1,  isactive = true, pricebook2id = pricebookId);
        insert pbe;

        for(Integer j=0; j<addresses.size(); j++) {
            OrderItem orderItem = new OrderItem();
            orderItem.OrderId = orders[j].id;
            orderItem.PricebookEntryId = pbe.id;
            orderItem.Quantity = 1;
            orderItem.UnitPrice = 100;
            insert orderItem;        
        }
 */
        
        List<vlocity_cmt.ProductWrapper> productDefinitions = new List<vlocity_cmt.ProductWrapper>();
        vlocity_cmt.ProductWrapper productW = new vlocity_cmt.ProductWrapper();
        productW.productId='01t40000004JwWyAAK';
        productW.name = 'Office Internet Basic';
        productW.JSONAttribute = '{"TELUSCHAR":[{"name":"Office Internet Basic","attributedisplayname__c":"Download speed","attributeRunTimeInfo":{"values":[{"displayText":"25 Mbps"}]}}]}';
        productDefinitions.add(productW);
        //input.put('productDefinition', productDefinitions);
        
        vlocity_cmt__ProductChildItem__c pci = new vlocity_cmt__ProductChildItem__c(vlocity_cmt__ChildLineNumber__c='001', vlocity_cmt__ChildProductId__c='01t40000004JwWyAAK', OCOMOverrideType__c='Override Child');
        insert pci;  
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        //TpFulfillmentCustomerOrderV3.CustomerOrder co = OrdrProductFeasibilityWsCallout.performCustomerOrderFeasibility(input);
        try{
        Boolean returnVal = componentFeasibility.invokeMethod('checkProductFeasibility', input, output, options);
       
        }catch(Exception ex){}
        //System.assert(returnVal);
        Test.stopTest();
    }
}
    /*
    @isTest
    private static void testCheckProductTechnicalAvailability_Exception() {
        
        List<SMBCare_Address__c> addresses = OrdrTestDataFactory.createAddresses();
        Id serviceAddressId = addresses[0].Id;
        OrdrTestDataFactory.createOrderingWSCustomSettings('TechnicalAvailabilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/RMO/ResourceMgmt/CheckGenericTechnicalAvailabilitySvc_v2_2_vs1');
            
        Test.startTest();
        try {
            Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl(false));
            // Call the method that invokes a callout
            List<OrdrTechnicalAvailabilityManager.ProductCategory> categories = OrdrTechnicalAvailabilityManager.checkProductTechnicalAvailability(serviceAddressId);
            System.assert(false, 'Exception expected');
        }
        catch(Exception e) {
            System.debug(e.getTypeName() + ': ' + e.getMessage());
        }
        Test.stopTest();
        
    }    
    
    @isTest
    private static void testCheckWirelineTnPortability() {
        
        List<SMBCare_Address__c> addresses = OrdrTestDataFactory.createAddresses();
        Id serviceAddressId = addresses[0].Id;
        OrdrTestDataFactory.createOrderingWSCustomSettings('TechnicalAvailabilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/RMO/ResourceMgmt/CheckGenericTechnicalAvailabilitySvc_v2_2_vs1');
            
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        // Call the method that invokes a callout
        OrdrTechnicalAvailabilityManager.TnPortability tnPortability = OrdrTechnicalAvailabilityManager.checkWirelineTnPortability(serviceAddressId, '6046956229');
        // Verify that a fake result is returned
        System.assertEquals('true', tnPortability.portableIndicator);
        Test.stopTest();
        
    }  
    

    @isTest
    private static void testCheckWirelineTnPortability_Exception() {
        
        List<SMBCare_Address__c> addresses = OrdrTestDataFactory.createAddresses();
        Id serviceAddressId = addresses[0].Id;
        OrdrTestDataFactory.createOrderingWSCustomSettings('TechnicalAvailabilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/RMO/ResourceMgmt/CheckGenericTechnicalAvailabilitySvc_v2_2_vs1');
            
        Test.startTest();
        try {
            Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl(false));
            // Call the method that invokes a callout
            OrdrTechnicalAvailabilityManager.TnPortability tnPortability = OrdrTechnicalAvailabilityManager.checkWirelineTnPortability(serviceAddressId, '6046956229');
            System.assert(false, 'Exception expected');
        }
        catch(Exception e) {
            System.debug(e.getTypeName() + ': ' + e.getMessage());
        }
        Test.stopTest();
        
    }      
    
    @isTest
    private static void testCheckWirelessTnPortability() {

        OrdrTestDataFactory.createOrderingWSCustomSettings('TechnicalAvailabilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/RMO/ResourceMgmt/CheckGenericTechnicalAvailabilitySvc_v2_2_vs1');
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        // Call the method that invokes a callout
        OrdrTechnicalAvailabilityManager.TnPortability tnPortability = OrdrTechnicalAvailabilityManager.checkWirelessTnPortability('6046956229', 'EXT_2H', '1');
        // Verify that a fake result is returned
        System.assertEquals('true', tnPortability.portableIndicator);
        Test.stopTest();
        
    }   

    
    @isTest
    private static void testCheckWirelessTnPortability_Exception() {

        OrdrTestDataFactory.createOrderingWSCustomSettings('TechnicalAvailabilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/RMO/ResourceMgmt/CheckGenericTechnicalAvailabilitySvc_v2_2_vs1');
        
        Test.startTest();
        try {
            Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl(false));
            // Call the method that invokes a callout
            OrdrTechnicalAvailabilityManager.TnPortability tnPortability = OrdrTechnicalAvailabilityManager.checkWirelessTnPortability('6046956229', 'EXT_2H', '1');
            System.assert(false, 'Exception expected');
        }
        catch(Exception e) {
            System.debug(e.getTypeName() + ': ' + e.getMessage());
        }
        Test.stopTest();
        
    }   
    
    
    @isTest
    private static void testGetServicePath() {

        List<SMBCare_Address__c> addresses = OrdrTestDataFactory.createAddresses();
        Id serviceAddressId = addresses[0].Id;
        OrdrTestDataFactory.createOrderingWSCustomSettings('ClearanceCheckEndpoint', 'https://xmlgwy-pt1.telus.com:9030/pt01/RMO/InventoryMgmt/FindResource_v2_0_vs1_PT');
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        // Call the method that invokes a callout
        List<OrdrTechnicalAvailabilityManager.ServicePath> paths = OrdrTechnicalAvailabilityManager.getServicePath(serviceAddressId);
        // Verify that a fake result is returned
        System.assertEquals(3, paths.size());
        Test.stopTest();
        
    } 
    // Added by Arvind 06/27 to increase Test Coverage for the existing classes updated by Aditya as part of Buy-1..
    @isTest
    static void OrdrWsCheckGenericTechnicalAvailability_Test1(){
    Test.startTest();
    OrdrWsCheckGenericTechnicalAvailability OrdrWsChekTech= new OrdrWsCheckGenericTechnicalAvailability();
    OrdrWsCheckGenericTechnicalAvailability.checkGenericTechnicalAvailabilityFault1_element FaultElement1 = new  OrdrWsCheckGenericTechnicalAvailability.checkGenericTechnicalAvailabilityFault1_element();
    OrdrWsCheckGenericTechnicalAvailability.checkGenericTechnicalAvailabilityFault2_element FaultElement2 = new  OrdrWsCheckGenericTechnicalAvailability.checkGenericTechnicalAvailabilityFault2_element();
    test.stopTest();
    }
    
    @isTest
    static void AsynCheckGenericTechnicalAvailabilitySOAPPort_Test1(){
    Test.startTest();
    OrdrWsCheckGenericTechnicalAvailability OrdrWsChekTech= new OrdrWsCheckGenericTechnicalAvailability();
    OrdrWsCheckGenericTechnicalAvailability.AsynCheckGenericTechnicalAvailabilitySOAPPort AsyncPort= new  OrdrWsCheckGenericTechnicalAvailability.AsynCheckGenericTechnicalAvailabilitySOAPPort();
    TpFulfillmentResourceOrderV3.ResourceOrder RsOrder= new TpFulfillmentResourceOrderV3.ResourceOrder();
    Continuation Cont = new Continuation(60);
    AsyncPort.beginCheckGenericTechnicalAvailability(Cont, RsOrder);
    Test.stopTest();
    }
    
    @isTest
    static void OrdrTechAvailWsCalloutAsynCheckProductTechnicalAvailability_Test1(){
    OrdrTestDataFactory.createOrderingWSCustomSettings('TechnicalAvailabilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/RMO/ResourceMgmt/CheckGenericTechnicalAvailabilitySvc_v2_2_vs1');
    Test.startTest();
    OrdrTechnicalAvailabilityWsCallout OrdrTechWs= new OrdrTechnicalAvailabilityWsCallout();
    OrdrWsCheckGenericTechnicalAvailability.AsynCheckGenericTechnicalAvailabilitySOAPPort AsyncPort= new  OrdrWsCheckGenericTechnicalAvailability.AsynCheckGenericTechnicalAvailabilitySOAPPort();
    TpFulfillmentResourceOrderV3.ResourceOrder RsOrder= new TpFulfillmentResourceOrderV3.ResourceOrder();
    Continuation Cont = new Continuation(60);
    OrdrTechnicalAvailabilityWsCallout.asynPrepareCallout(AsyncPort);
    Test.stopTest();
    }
*/