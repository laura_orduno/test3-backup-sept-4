/**
* A Class to handle the updating of Cases from Comments.
* @parm Dane Peterson
* @date 2017-12-7
**/

public without sharing class trac_UpdateCaseFromCaseComment {

	@TestVisible private static final String PROFILE_CUSTOMER_COMMUNITY = 'customer community user';

	static Boolean ranOnceCanceled = false;
	static Boolean ranOnceUpdated = false;

	/**
	* Updates cases associated to case comments with the most recent comment body/date/submitter for that cases.
	* @author Dane Peterson, Traction on Demand
	* @date 2017-12-7
	* @parm List of Case Comments
	**/
	public static void execute(List<CaseComment> records) {

		// Form a list of Ids so we can then query for the case comments to get additional informationation.
		List<Id> recordIds = new List<Id>();
		for (CaseComment r : records) {
			recordIds.add(r.Id);
		}

		// We have to re-query records in ordered to get CreadtedBy Information.
		List<CaseComment> newRecords = [SELECT CreatedBy.Name, CreatedBy.FirstName, CreatedBy.LastName, CreatedById, ParentId, CommentBody, CreatedBy.Email, IsPublished, CreatedDate FROM CaseComment WHERE Id in:recordIds];

		// Checks to confirm case comments meet specified crieteria and return a Map<CaseId, CaseComment>
		Map<Id, CaseComment> cc = meetsCriteria(newRecords);

		// If CC is empty (which it should not be) we can return
		if (cc.isEmpty()) {
			return;
		}

		// To hold the Cases we want to update
		List<Case> toUpdate = new List<Case>();

		// We need to get the cases we are going to update.
		Set<Id> caseIds = cc.keySet();
		Map<Id, Case> caseMap = new Map<Id, Case>([
				SELECT Id, Origin, Owner.Name, Owner.profile.Name, CreatedById, CreatedBy.Name, CreatedBy.Email, CreatedBy.Profile.Name, Last_Case_Comment_Date__c, Last_Case_Comment__c, Last_Case_Comment_Submitter__c, RecordTypeId
				FROM Case
				WHERE Id IN :caseIds
		]);

		// Start to loop through each Case. Note we use a map<Id, Case> because we can only have at most 1 NEWEST case comment on a case.
		for (Id cId : caseIds) {
			if (caseMap.containsKey(cId)) {
				Case tempC = caseMap.get(cId);
				CaseComment tempCC = cc.get(cId);

				tempC.Last_Case_Comment__c = tempCC.CommentBody;
				tempC.Last_Case_Comment_Submitter__c = tempCC.CreatedBy.Name;
				tempC.Last_Case_Comment_Submitted_By__c = tempCC.CreatedById;
				tempC.Last_Case_Comment_Date__c = tempCC.CreatedDate.date();
				tempC.Last_Case_Comment_Public__C = tempCC.IsPublished;

				// VITAL CARE + ENTP + MBR CASE COMMENT FILTERING
				String caseCreatorProfileName = (tempC.createdBy != null && tempC.createdBy.profile != null)
						? tempC.createdBy.profile.name.toLowerCase() : '';

				if (caseCreatorProfileName.contains('customer community user')) {
					if (tempCC.CommentBody != null) {
						if (!tempCC.CommentBody.contains(Label.MBRCancelReason) && !tempCC.CommentBody.contains(Label.MBREmailCommentAuthor)) {

							// comment is not a cancellation, and comment did not come in via e-mail
							tempC.Last_Case_Comment_Submitted_By__c = tempCC.CreatedById;
							tempC.Last_Case_Comment_Submitter__c = tempCC.CreatedBy.Name != null ? tempCC.CreatedBy.Name : 'Unknown';
							tempC.Last_Case_Comment__c = tempCC.CommentBody;

						} else if (tempCC.CommentBody.contains(Label.MBREmailCommentAuthor)) {
							String commenterInfo = tempCC.CommentBody.substringBefore(Label.MBREmailCommentAuthor).trim();
							if (commenterInfo.contains(' - ')) {
								// check if submitter is the case creator
								String submitterEmail = commenterInfo.substringBefore(' - ').toLowerCase();
								if (submitterEmail == tempC.CreatedBy.Email.toLowerCase()) {
									tempC.Last_Case_Comment_Submitted_By__c = tempC.CreatedById;
								}
								tempC.Last_Case_Comment_Submitter__c = commenterInfo.substringAfter(' - ');
								tempC.Last_Case_Comment__c = tempCC.CommentBody.substringAfter(Label.MBREmailCommentAuthor);
							}
						}
					}
				}

				toUpdate.add(tempC);
			}

		}

		update toUpdate;
	}

	/**
	* Determines if a Case Comment has a cooresponding ParentId.
	* @author Dane Peterson, Traction on Demand
	* @date 2017-12-7
	* @param records: List of Case Comment.
	* @return: Map of cooresponding Case Ids of the Case Comment. 
	**/
	public static Map<Id, CaseComment> meetsCriteria(List<CaseComment> records) {
		Map<Id, CaseComment> cc = new Map<Id, CaseComment>();
		for (CaseComment c : records) {
			if (c.ParentId != null) {
				if (!cc.containsKey(c.ParentId) || (cc.get(c.ParentId).CreatedDate < c.CreatedDate)) {
					cc.put(c.ParentId, c);
				}
			}
		}
		return cc;
	}
}