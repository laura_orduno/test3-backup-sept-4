/**
* @Author Santosh Rath
*/
public class ContractRequestTriggerHandler {
    private static Map<id,schema.recordtypeinfo> contractRecordTypeMap;
    private static Map<String,Id> contracReplacetRecordTypeMap; 
    private static Map<id,RecordType> recordTypeMap;
    private static Map<id,Opportunity> oppidObjMap; 
    public ContractRequestTriggerHandler(){
        if(Trigger.isexecuting){
            if(contractRecordTypeMap==null){
                contractRecordTypeMap=Contracts__c.sobjecttype.getdescribe().getrecordtypeinfosbyid();
            }
            if(contracReplacetRecordTypeMap==null){
               contracReplacetRecordTypeMap=RecordTypeUtil.GetRecordTypeIdsByDeveloperName(Contract_Replacements__c.SObjectType); 
            }
            if(recordTypeMap==null){
              recordTypeMap=new Map<id,RecordType>([SELECT Id, Name, DeveloperName, SobjectType FROM RecordType where SobjectType ='Contracts__c']);  
            }
             
            if(Trigger.isbefore){
                //beforeInit();
                if(Trigger.isinsert){
                    beforeInsert();
                }
                if(Trigger.isupdate){
                    beforeUpdate();
                }
               
            }else if(Trigger.isafter){
                Set<Id> orderIdSet=new Set<Id>();
                afterInit(orderIdSet);
                if(Trigger.isinsert){
                   // afterInsert(orderIdSet);
                }
                if(Trigger.isupdate){
                    afterUpdate();
                }                
            }
        }
    }
    /* update may 2018 - w order 2.0 contract request gets billing address via pac or opp - trigger no longer needed
    
    void beforeInit(){
        List<Contracts__c> contractReqList=Trigger.new;        
        for(Contracts__c newContractReq: contractReqList){ 
            if(String.isBlank(newContractReq.Contract_Billing_City__c)){
                newContractReq.Contract_Billing_City__c=newContractReq.Billing_City__c;
            }
            if(String.isBlank(newContractReq.Contract_Billing_Country__c)){
                newContractReq.Contract_Billing_Country__c=newContractReq.Billing_Country__c;
            }
            if(String.isBlank(newContractReq.Contract_Billing_Province__c)){
                newContractReq.Contract_Billing_Province__c=newContractReq.Billing_Province__c;
            }
            if(String.isBlank(newContractReq.Contract_Billing_Street__c)){
                newContractReq.Contract_Billing_Street__c=newContractReq.Billing_Street__c;
            }
            if(String.isBlank(newContractReq.Contract_Billing_Province__c)){
                newContractReq.Contract_Billing_Province__c=newContractReq.Billing_Province__c;
            }           
        }
    }*/
    void afterInit(Set<Id> orderIdSet){
        set<id> setSRIds=new set<id>();
        List<Contracts__c> contractReqList=(Trigger.isDelete?Trigger.old:Trigger.New);
        for(Contracts__c objCon:contractReqList){
            if(objCon.Contract__c!=null){
                setSRIds.add(objCon.Contract__c);
            }
             if(String.isNotBlank(objCon.order__c)){
              orderIdSet.add(objCon.order__c);
          }     
        }
        updateTrackForComplexOrder(setSRIds);
    }
    void beforeInsert(){
       List<String> opportunityIdList=new List<String>();
        List<Contracts__c> contractReqList=Trigger.new;
        for(Contracts__c newContractReq: contractReqList){            
             if(String.isNotBlank(newContractReq.RecordTypeId) && (contractRecordTypeMap.get(newContractReq.recordtypeid).getname().equalsignorecase('eContract'))){
                opportunityIdList.add(newContractReq.Opportunity__c);
            }
            if(String.isNotBlank(newContractReq.RecordTypeId) && (contractRecordTypeMap.get(newContractReq.recordtypeid).getname().equalsignorecase('CPQ Contract'))){
                opportunityIdList.add(newContractReq.Opportunity__c);
            }
            newContractReq.Record_Type_Name__c=recordTypeMap.get(newContractReq.recordtypeid).DeveloperName;
        }
        updateAccountOnContractRequest(opportunityIdList);
    }
   /* void afterInsert(Set<Id> orderIdSet){
        List<Contracts__c> contractReqList=Trigger.new;
        List<Contract_Replacements__c> contractReplacementList=new List<Contract_Replacements__c>();
        //List<OrderItem> oiList=[select contract_request__c,Contract_Line_Item__c from orderitem where contract_request__c in :Trigger.newMap.keySet() and Contract_Action__c='Change_Replace'];
        List<OrderItem> oiList=[select orderid,isdeleted,contract_request__c,Contract_Line_Item__c,Contract_Action__c  from orderitem where OrderId in :orderIdSet all rows];
        // Map<id,List<id>> contractRequestCliMap=new Map<Id,List<Id>>();
        Map<id,List<id>> contractRequestCliMap=null;
        Map<Id,Map<id,List<id>>> orderIdContractRequestCliMap=new Map<Id,Map<id,List<id>>>();
        List<Id> cliList=null;
        for(OrderItem oiInstance:oiList){
            if((oiInstance.isDeleted && String.isBlank(oiInstance.Contract_Line_Item__c)) || (String.isBlank(oiInstance.contract_request__c))){
                continue;
            }
            
            if(oiInstance.IsDeleted || (!oiInstance.IsDeleted && ('Change_Replace'.equalsignorecase(oiInstance.Contract_Action__c)))){
                contractRequestCliMap=orderIdContractRequestCliMap.get(oiInstance.orderid);
                if(contractRequestCliMap==null){
                    contractRequestCliMap=new Map<Id,List<Id>>();
                    cliList=new List<Id>();
                    cliList.add(oiInstance.Contract_Line_Item__c);
                    contractRequestCliMap.put(oiInstance.contract_request__c,cliList);
                    orderIdContractRequestCliMap.put(oiInstance.orderid,contractRequestCliMap);
                } else {
                    cliList=contractRequestCliMap.get(oiInstance.contract_request__c);
                    if(cliList==null){
                        cliList=new List<Id>();
                        cliList.add(oiInstance.Contract_Line_Item__c);
                        contractRequestCliMap.put(oiInstance.contract_request__c,cliList);
                        orderIdContractRequestCliMap.put(oiInstance.orderid,contractRequestCliMap);
                    }else {
                        cliList.add(oiInstance.Contract_Line_Item__c);
                        contractRequestCliMap.put(oiInstance.contract_request__c,cliList);
                        orderIdContractRequestCliMap.put(oiInstance.orderid,contractRequestCliMap);
                    }               
                } 
            }           
        }
        for(Contracts__c newContractReq: contractReqList){
            if(String.isNotBlank(newContractReq.RecordTypeId) && (contractRecordTypeMap.get(newContractReq.recordtypeid).getname().equalsignorecase('CPQ Contract')) && 
               'Replacement'.equalsignorecase(newContractReq.Type_Of_Contract__c)){
                   if(contractRequestCliMap.get(newContractReq.id)!=null){
                       for(Id cliId:contractRequestCliMap.get(newContractReq.id)){
                       Contract_Replacements__c contractReplacement=new Contract_Replacements__c(Account__c=newContractReq.Account__c,Contract_Line_Item__c=cliId,Contract_Request_ID__c=newContractReq.id,recordTypeId=contracReplacetRecordTypeMap.get('CPQ Contract Replacement'));
                       contractReplacementList.add(contractReplacement);
                        }   
                   }
                               
               }
        }
        if(contractReplacementList.size()>0){
            insert contractReplacementList;
        }
    }*/
    void beforeUpdate(){
        List<String> opportunityIdList=new List<String>();
        List<Contracts__c> contractReqList=Trigger.new;
        for(Contracts__c newContractReq:contractReqList){            
            Contracts__c oldContractReq=(Contracts__c)Trigger.oldMap.get(newContractReq.id);
            if(String.isNotBlank(newContractReq.RecordType.Name) && (contractRecordTypeMap.get(newContractReq.recordtypeid).getname().equalsignorecase('eContract')) && 
               (newContractReq.Opportunity__c!=oldContractReq.Opportunity__c)){
                opportunityIdList.add(newContractReq.Opportunity__c);                
            }
            if(String.isNotBlank(newContractReq.RecordType.Name) && (contractRecordTypeMap.get(newContractReq.recordtypeid).getname().equalsignorecase('CPQ Contract')) && 
               (newContractReq.Opportunity__c!=oldContractReq.Opportunity__c)){
                opportunityIdList.add(newContractReq.Opportunity__c);                
            }
        }
        if(opportunityIdList.size()>0){
        updateAccountOnContractRequest(opportunityIdList);
        }
    }
    void afterUpdate(){}
    
    void updateTrackForComplexOrder(Set<id> setSRIds){
        if(setSRIds.size()>0){
            List<Service_Request__c> lstSR = new List<Service_Request__c>();
            for(Id srId : setSRIds){           
                Service_Request__c sr = new Service_Request__c(Id = srId,Track__c=system.now());
                lstSR.add(sr);
            }
            if(lstSR.size()>0)
                update lstSR;
        }
    }
    void updateAccountOnContractRequest(List<String> opportunityIdList){
        if(oppidObjMap==null || oppidObjMap.isEmpty()){
             oppidObjMap=new Map<id,Opportunity>([select id,AccountId from  Opportunity where id in:opportunityIdList]);
        }
       
        List<Contracts__c> contractReqList=Trigger.new;
        for(Contracts__c newContractReq:contractReqList){
            if(String.isNotBlank(newContractReq.Opportunity__c) && oppidObjMap.get(newContractReq.Opportunity__c)!=null){
                newContractReq.Account__c=oppidObjMap.get(newContractReq.Opportunity__c).AccountId;
            }
            
        }               
    }
}