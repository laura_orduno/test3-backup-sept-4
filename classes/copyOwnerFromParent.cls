public class copyOwnerFromParent{
    
    public static void afterUpdate(List<Account> oldAccounts, List<Account> newAccounts)
    {    
    	// get record type map
		Map<ID, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Account.getRecordTypeInfosById();
        
        // build corresponding id set and maps to be passed to helper method for updating child accounts
        Set<Id> parentIds = new Set<Id>();
        Map<Id, Id> parentOwnerMap = new Map<Id, Id>();
        
        // loop through account lists to determine whether the relevent fields have been changed, build id sets accordingly
        for(Integer i = 0;i < oldAccounts.size();i++)
        {
            String recordTypeName = rtMap.get(newAccounts[i].recordTypeId).getName();
            
            if(recordTypeName == 'CBUCID' && oldAccounts[i].Sub_Segment_Code__c != newAccounts[i].Sub_Segment_Code__c )
            {
                parentIds.add(newAccounts[i].id);
            }
            else if( (recordTypeName == 'RCID' || recordTypeName == 'BAN' || recordTypeName == 'CAN') && oldAccounts[i].ownerId != newAccounts[i].ownerId )
//            if( recordTypeName == 'RCID' && oldAccounts[i].ownerId != newAccounts[i].ownerId )
            {
                parentIds.add(newAccounts[i].id);
            }
            else if( (recordTypeName == 'RCID' || recordTypeName == 'BAN' || recordTypeName == 'CAN') && (newAccounts[i].parentId != null && oldAccounts[i].parentId != newAccounts[i].parentId ) ) 
            {                
                parentIds.add(newAccounts[i].parentId);
            }
        }
        
        if(!parentIds.isEmpty())
        {
            Map<Id, Account> parentMap = new Map<Id, Account>([SELECT id, recordTypeId, owner.id, sub_segment_code__c FROM Account WHERE id IN :parentIds AND owner.isActive = true]); 
                                
            // list used to perform update to db
            List<Account> updateList = new List<Account>();
            List<Account> childAccountsPendingUpdate = copyOwnerFromParentAccount(parentIds, parentMap);
                    
            // add to update list
            updateList.addAll(childAccountsPendingUpdate);
            // only perform update to db if list has entries, otherwise bypass to prevent the trigger from perpetually firing 
            if(updateList.size() > 0)
            {
                update updateList;
            } 
        }    
    }
        
    public static void beforeInsert(List<Account> accounts)
    {
        // get record type map
		Map<ID, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Account.getRecordTypeInfosById();
        
        Set<Id> parentIds = new Set<Id>();
        
        for(Account acct : accounts)
        {	
            if(rtMap.get(acct.recordTypeId).getName() == 'CBUCID')
            {
          		continue;
            }

            parentIds.add(acct.parentId);
        }
        
        if(!parentIds.isEmpty())
        {
            Map<Id, Account> parentIdMap = new Map<Id, Account>([SELECT id, recordTypeId, owner.id, sub_segment_code__c FROM Account WHERE id IN :parentIds AND owner.isActive = true]); 
    
            for(Account acct : accounts)
            {
            	String recordTypeName = rtMap.get(acct.recordTypeId).getName();
                
                if(parentIdMap.containsKey(acct.parentId))
                {
                    Account parent = parentIdMap.get(acct.parentId);
                        
                    if(recordTypeName == 'RCID')
                    {
//System.debug(acct.name + ', subsegment: ' + acct.sub_segment_code__c + ' -> ' + parent.sub_segment_code__c);                        
                    	acct.sub_segment_code__c = parent.sub_segment_code__c;
                    }
                    else
                    {
                    	acct.ownerId = parent.owner.id;
                    }    
                }
            }        
        }    
    }
        
    public static List<Account> findChildAccounts(Set<id> acids)
    {
        List<Account> accounts = [SELECT name, recordTypeId, parent.id, owner.id, sub_segment_code__c FROM Account WHERE parent.id IN :acids]; 

        return accounts;
    }
            
    public static List<Account> copyOwnerFromParentAccount(Set<Id> idSet, Map<Id, Account> parentMap)
    {        
        // get record type map
		Map<ID, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Account.getRecordTypeInfosById();
        
        List<Account> updatedAccounts = new List<Account>();
        
        List<Account> childAccounts = findChildAccounts(idSet);
        
        for(Account acct : childAccounts)
        {
            String recordTypeName = rtMap.get(acct.recordTypeId).getName();
            
            if(parentMap.containsKey(acct.parentId))
            {
                Account parent = parentMap.get(acct.parentId);
                    
                if(recordTypeName == 'RCID')
                {
//System.debug(acct.name + ', subsegment: ' + acct.sub_segment_code__c + ' -> ' + parent.sub_segment_code__c);                        
                    acct.sub_segment_code__c = parent.sub_segment_code__c;
                }
                else
                {
                	acct.ownerId = parent.owner.id;
                }
                
                // add entry to returning list 
                updatedAccounts.add(acct);                
            }
        }        
                
        return updatedAccounts;
    }               
}