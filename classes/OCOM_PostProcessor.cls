global with sharing class OCOM_PostProcessor implements vlocity_cmt.VlocityOpenInterface {
    

    private final String CLASS_NAME = 'OCOM_PostProcessor';


    global Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {
        Boolean success = true;
        string TargetExternalIdKeyName = OCOM_vlocityCustomUtilcls.PriceMatrixTargetExternalId;
        //system.debug('TargetExternalIdKeyName'+TargetExternalIdKeyName);



        try {
            if(methodName == 'calculate') {  

        //##################################################################
         //VlocityLogger.e(CLASS_NAME+'-'+ methodName);
        //##################################################################



                  //system.debug(LoggingLevel.ERROR, ' ---- OCOMPostProcessor inputMap ' + inputMap);
                  //system.debug(LoggingLevel.ERROR, ' ---- OCOMPostProcessor outMap ' + outMap);
                  //system.debug(LoggingLevel.ERROR, ' ---- OCOMPostProcessor options ' + options);
                  ////system.debug(LoggingLevel.ERROR, ' ---- OCOMPostProcessor FlowStaticMap.flowMap ' + vlocity_cmt.FlowStaticMap.flowMap);

                  SObject parent = (SObject)vlocity_cmt.FlowStaticMap.flowMap.get('parent');
                  String parentObjName;
                  String parentId;
                  if (parent != null) {
                      parentId = String.valueOf(parent.Id);
                      parentObjName = parent.getSObjectType().getDescribe().getName();
                  }
                
                  if (String.isBlank(parentId) || String.isBlank(parentObjName)) {
                      //system.debug(LoggingLevel.ERROR, ' ---- OCOMPostProcessor parentId or parentObjName is blank.');
                      return true;
                  }
                  String outputBundle = getConfigurationValue('OCOMPostProc'+parentObjName+'DRBundle');
                  //system.debug(LoggingLevel.ERROR, ' ---- OCOMPostProcessor outputBundle ' + outputBundle);

                  if (String.isBlank(outputBundle)) {
                      //system.debug(LoggingLevel.ERROR, ' ---- OCOMPostProcessor outputBundle is blank.');
                      return true;
                  }
                  
                  List<Object> outputList = (List<Object>)outMap.get('output');
                  List<Object> objectList;
                 //system.debug(LoggingLevel.ERROR, ' ---- OCOMPostProcessor outputList ' + outputList);
                  if (outputList != null && !outputList.isEmpty()) {
                      vlocity_cmt.PricingCalculationService.CalculationProcedureResults calcProcResults = (vlocity_cmt.PricingCalculationService.CalculationProcedureResults)outputList[0];
                      objectList = calcProcResults.calculationResults;                      
                  }

                 system.debug(LoggingLevel.ERROR, ' ---- OCOMPostProcessor calculation results ' + JSON.serializePretty(objectList));
         Map<String, Object> savedIdMap = new Map<String, Object>();
                  if (objectList != null && !objectList.isEmpty()) {
                      // Map<String, Decimal> mrcMap = new Map<String, Decimal>();
                      // Map<String, Decimal> nrcMap = new Map<String, Decimal>();
                      List<Object> saveList = new List<Object>();        
                      Map<String, Object> dataSet = (Map<String, Object>)inputMap.get('data');
                      //system.debug('***dataSet '+JSON.serializePretty(dataSet));                       
                      if (dataSet != null && !dataSet.isEmpty()) {
                        // map input data row to external id. This assumes external id is unique across all line items.
                        Map<String, Map<String, Object>> externalIdToDataRowMap = new Map<String, Map<String, Object>>(); 
                        Map<String, Object> IdToDataRowMap = new Map<String, Object>();                       
                        for (String key : dataSet.keySet()) {
                            Map<String, Object> dataRow = (Map<String, Object>)dataSet.get(key);
                            String mapKey = (String)dataRow.get('Reference External Id') + '_$$_' + (String)dataRow.get('ItemID');
                            if (!externalIdToDataRowMap.containsKey(mapKey)) {
                                externalIdToDataRowMap.put(mapKey, dataRow);
                            }
                        }
                       // system.debug('***externalIdToDataRowMap '+JSON.serializePretty(externalIdToDataRowMap));
                         
                        map<string,map<string,Decimal>> extIdToCurrencyMap = new  map<string,map<string,Decimal>>();
                        for (Object obj : objectList) {
                            // This is a row in the calculation procedure output
                            Map<String, Object> row = (Map<String, Object>)obj;

                            // Get the External Id from the output row-- 'OCOMTextPriceMatrix__TargetExternalId'
                          String outputExternalId ;
                          if(row.ContainsKey(TargetExternalIdKeyName) ) 
                            outputExternalId = (String)row.get(TargetExternalIdKeyName);
                            String childItemId = (String)row.get('ID');
                            //system.debug('***childID '+childItemId);
                            Map<String, Object> childDataRow = (Map<String, Object>)dataSet.get(childItemId);
                            //system.debug('***childDataRow '+childDataRow);
                            String childLineNum;
                            if (childDataRow != null) {
                                childLineNum = (String)childDataRow.get('Line Number');
                            }
                           // system.debug('***childLineNum '+childLineNum + externalIdToDataRowMap.size() );
                           // system.debug('.KeyexternalIdToDataRowMapSet' + externalIdToDataRowMap.keyset() + 'outputExternalId' + outputExternalId);
                            Map<String, Object> dataSetRow;
                            if(outputExternalId != null ){
                            for (String key : externalIdToDataRowMap.keySet()) {
                              
                                if (key.startsWith(outputExternalId)) {
                                    
                                    dataSetRow = (Map<String, Object>)externalIdToDataRowMap.get(key);
                                    
                                    if (childLineNum.startsWith((String)dataSetRow.get('Line Number'))) {
                                        break;
                                    }
                                }
                            }
                          }
                            
                            //system.debug('***dataSetRow '+dataSetRow);

                               
                                                        
                            if (dataSetRow != null) {
                               Map<String, Object> objToSaveMap = new Map<String, Object>();
                                String itemID = (String)dataSetRow.get('ItemID');  

                              if(dataSetRow.containsKey('Quantity') && dataSetRow.get('Quantity') != null) {
                                Decimal Qty = Decimal.valueof((String)dataSetRow.get('Quantity'));
                                objToSaveMap.put('QTY', Qty);
                              }

                                
                                //this gets the existing MRC and NRC Values
                               /* if(dataSetRow.containsKey('Input MRC') && dataSetRow.get('Input MRC') != null)
                                Decimal inputmrc = Decimal.valueOf((Double)dataSetRow.get('Input MRC')).setScale(2, RoundingMode.HALF_UP);    
                                if(dataSetRow.containsKey('Input NRC') && dataSetRow.get('Input NRC') != null)
                                Decimal inputnrc = Decimal.valueOf((Double)dataSetRow.get('Input NRC')).setScale(2, RoundingMode.HALF_UP);    
                                */
                                //system.debug(LoggingLevel.ERROR,'inputmrc: ' + inputmrc);
                                //system.debug(LoggingLevel.ERROR,'inputnrc: ' + inputnrc);

                                objToSaveMap.put('ID', itemID);

                                //THIS SECTION ADDS UP THE MRC AND NRC
                                //Modified the code to Replace the MRC and NRC
                                Decimal mrc;
                                Decimal nrc;
                               
                                if(row.containsKey('MRC') && row.get('MRC') != null)
                                mrc =  Decimal.valueOf((Double)row.get('MRC')).setScale(2, RoundingMode.HALF_UP);
                                if(row.containsKey('NRC') && row.get('NRC') != null)
                                nrc =  Decimal.valueOf((Double)row.get('NRC')).setScale(2, RoundingMode.HALF_UP);
                                
                                string childItemSize = (childLineNum.split('\\.')).Size() > 0 ? string.Valueof((childLineNum.split('\\.')).Size()): '0';
                                string orderID =  (String)row.get('ID');
                                if(!extIdToCurrencyMap.containsKey(((String)row.get(TargetExternalIdKeyName))+orderID)){
                                    extIdToCurrencyMap.put((String)row.get(TargetExternalIdKeyName)+orderID, new map<string,decimal>{'MRC' => mrc,'NRC'=> nrc});

                                } else if(extIdToCurrencyMap.containsKey(((String)row.get(TargetExternalIdKeyName))+orderID)) {
                                   
                                   mrc += extIdToCurrencyMap.get(((String)row.get(TargetExternalIdKeyName))+orderID).get('MRC');
                                   nrc += extIdToCurrencyMap.get(((String)row.get(TargetExternalIdKeyName))+orderID).get('NRC');
                                   extIdToCurrencyMap.get(((String)row.get(TargetExternalIdKeyName))+orderID).put('MRC', mrc);
                                   extIdToCurrencyMap.get(((String)row.get(TargetExternalIdKeyName))+orderID).put('NRC', nrc);
                                    //system.debug('MRC ___'+mrc);
                                    //system.debug('NRC ___'+nrc);

                                }
                               // system.debug('extIdToCurrencyMap____' +extIdToCurrencyMap);
                                
                                objToSaveMap.put('MRC', mrc);
                               // system.debug(LoggingLevel.ERROR,'mrc_afterelse: ' + mrc);                 
                                
                                
                                objToSaveMap.put('NRC', nrc);
                                
                               // system.debug(LoggingLevel.ERROR,'nrc_afterelse: ' + nrc);
                                
                                // save the rest of the fields from the output row
                                for (String key : row.keySet()) {
                                    if (!objToSaveMap.containsKey(key)) {
                                        objToSaveMap.put(key, row.get(key));
                                    }
                                }
                                saveList.add(objToSaveMap);
                            }                          
                        }
                       
                        String duplicateOli = 'x';
                        for(Object savedItems : saveList){
                            String currentOlid = (String)((Map<String,Object>)savedItems).get('ID');
                            Decimal currentMrc = (Decimal)((Map<String,Object>)savedItems).get('MRC');
                            Decimal currentNrc = (Decimal)((Map<String,Object>)savedItems).get('NRC');
                            if(!savedIdMap.containsKey(currentOlid)){
                                savedIdMap.put(currentOlid,savedItems);
                            }
                            else if(savedIdMap.containsKey(currentOlid)){
                                  Decimal dupMRC = (Decimal)((Map<String,Object>)savedIdMap.get(currentOlid)).get('MRC');
                                  Decimal dupNRC = (Decimal)((Map<String,Object>)savedIdMap.get(currentOlid)).get('NRC');
                                  ((Map<String,Object>)savedIdMap.get(currentOlid)).put('MRC',dupMRC+currentMrc);
                                  ((Map<String,Object>)savedIdMap.get(currentOlid)).put('NRC',dupNRC+currentNrc);
                              }
                          }

                        /*for(Object savedItemsObj : savedIdMap.Values()){
                            Map<String,Object>savedItems = (Map<String,Object>) savedItemsObj;
                            String currentOlid = (String)(savedItems).get('ID');
                            Decimal itemQty = ((Decimal)(savedItems).get('QTY') != null ) ? (Decimal)(savedItems).get('QTY')  : 1;
                            Decimal currentMrc = (Decimal)(savedItems).get('MRC') * itemQty;
                            Decimal currentNrc = (Decimal)(savedItems).get('NRC') * itemQty;
                            savedItems.put('MRC',currentMrc);
                            savedItems.put('NRC',currentNrc);
                              
                          }*/
                      }
                  } 
                   //system.debug(LoggingLevel.ERROR, ' ---- OCOMPostProcessor objects to save ' + JSON.serializePretty(savedIdMap));
                        
                 // Vlocity Dec 27
                 // Removing DR post instead calling DML directly to udpate
                  updateOLIs(savedIdMap);
              }
          } catch(Exception e){
              system.debug(LoggingLevel.ERROR, 'Exception is '+e);
              system.debug(LoggingLevel.ERROR, 'Exception stack trace '+e.getStackTraceString());
        //##################################################################
         //VlocityLogger.x(CLASS_NAME+'-'+ methodName);
        //##################################################################

              success=false;
              throw e;
         }

        //##################################################################
         //VlocityLogger.x(CLASS_NAME+'-'+ methodName);
        //##################################################################


        return success;        
    }
    /**
       Update OLIs 
       Vlocity Dec 27 2016

    */
     @TestVisible 
    private void updateOLIs(Map<String, Object> savedIdMap){
      
        //################################################
        // construct list of OLIs that changed price by matrix 
        //################################################

        List<SObject> itemList =  (list<SObject>)vlocity_cmt.FlowStaticMap.flowMap.get('itemList');
        Map<Id,OrderItem> tempOliToUpdate = new Map<Id,OrderItem>();
        // original implementation of SetManual and SetOverrideToZero use list 
        List<Id> oliIds = new List<Id>();
        // go through itemList 
        for(Sobject s: itemList){
          // check if item is OrderItem
          if(s.getSObjectType().getDescribe().getName()=='OrderItem'){
              OrderItem oi = (OrderItem)s;
              oliIds.add(oi.Id);
              // compare NRC and MRC if different add to oliToUpdate list
              if(savedIdMap != null && savedIdMap.containsKey(oi.Id)){
                
                 if(((Decimal)((Map<String,Object>)savedIdMap.get(oi.Id)).get('NRC') != oi.vlocity_cmt__OneTimeTotal__c) ||
                 ((Decimal)((Map<String,Object>)savedIdMap.get(oi.Id)).get('MRC') != oi.vlocity_cmt__RecurringTotal__c)){
                    Decimal qty = (Decimal)((Map<String,Object>)savedIdMap.get(oi.Id)).get('QTY');
                      Decimal onetimeCharge = (Decimal)((Map<String,Object>)savedIdMap.get(oi.Id)).get('NRC');
                      Decimal recCharge = (Decimal)((Map<String,Object>)savedIdMap.get(oi.Id)).get('MRC');
                     
                      Map<String,Object> outMap = new Map<String,Object>();
                      outMap = getCalculatedPrice(oi, qty, onetimeCharge,recCharge);
                     
                     if(outMap != null  && outMap.size()>0 && !outMap.isEmpty() ){
                        Decimal onetimeTotal = (Decimal)outMap.get('oneTimeTotal');
                        Decimal recTotal = (Decimal)outMap.get('recTotal');
                        oi.vlocity_cmt__OneTimeTotal__c = onetimeTotal;
                        oi.vlocity_cmt__RecurringTotal__c = recTotal;
                        tempOliToUpdate.put(oi.id, oi);
                     }
                     
                 }
              }
          }

        }

        //################################################
        // call setOverRidePricesToZero and Manual Override 
        //################################################

        List<OrderItem> otherOLIs = new List<OrderItem> ();
        if(oliIds.size()>0){
          Map<Id,SOBJECT> sobjectIdToSobject = new Map<Id,SOBJECT> ([select Id
                                                                      ,PricebookEntry.product2.ProductCode
                                                                      ,PricebookEntry.product2.Name
                                                                      ,PriceBookEntry.product2.Id

                                                                      ,vlocity_cmt__Product2Id__c
                                                                      ,vlocity_cmt__ParentItemId__c
                                                                      ,vlocity_cmt__JSONAttribute__c
                                                                      ,vlocity_cmt__OneTimeTotal__c
                                                                      ,vlocity_cmt__EffectiveOneTimeTotal__c
                                                                      ,vlocity_cmt__RecurringTotal__c
                                                                      ,vlocity_cmt__EffectiveRecurringTotal__c
                                                                      ,vlocity_cmt__RootItemId__c
                                                                      ,vlocity_cmt__OneTimeCharge__c
                                                                      ,vlocity_cmt__RecurringCharge__c
// TO JAYA 

                                                                      ,vlocity_cmt__ProvisioningStatus__c
                                                                      ,Quantity
                                                                      ,vlocity_cmt__Product2Id__r.VLAdditionalConfigData__c
                                                                      from OrderItem where Id in :oliIds]);

          //################################################
          // JAYA HERE you need to call static MACD_PricingHelper.DisconnectPricing
          // Basically the method is returning OLIs with Provisioning Status = Disconnect
          // If Disconnect happening. We can assume all OLIs are Diconnect OLIs. 
          //################################################
          Map<Id,OrderItem> disconnectMap = new  Map<Id,OrderItem> ();
          disconnectMap = MACD_PricingHelper.DisconnectPricing(sobjectIdToSobject, oliIds );
        
          if(disconnectMap.size()==0){
            otherOLIs = OCOM_PricingRulesHelper.getListToUpdate(sobjectIdToSobject, oliIds, tempOliToUpdate);
          }else{
            otherOLIs = disconnectMap.values();
          }

          system.debug(LoggingLevel.ERROR, ' ---- otherOLIs ' + otherOLIs);
        }

      
     Map<Id,OrderItem> updateOLiMap = new Map<Id,OrderItem>();
        if(otherOLIs.size()>0){
         
        //if(vlocity_cmt.FlowStaticMap.flowMap.Containskey('AddPricing')  && !(Boolean)vlocity_cmt.FlowStaticMap.flowMap.get('AddPricing') )      
            update otherOLIs;
            
             for(orderItem oli : otherOLIs) {
                  updateOLiMap.put(oli.id,oli);
                   }
         
        }
        Set<Id> oliIDSet = new Set<Id>();

       if(null!= updateOLiMap && updateOLiMap.size()>0 && itemList != null && !itemList.isEmpty() && itemList.size() > 0){
            for(Sobject sObj: itemList){
          // check if item is OrderItem
              if(sObj.getSObjectType().getDescribe().getName()=='OrderItem'){
                  OrderItem Oli = (OrderItem)sObj;
                    if( updateOLiMap.containsKey(Oli.id)){
                   
                        if(updateOLiMap.get(Oli.id).vlocity_cmt__RecurringTotal__c != null )
                            Oli.vlocity_cmt__RecurringTotal__c =  updateOLiMap.get(Oli.id).vlocity_cmt__RecurringTotal__c ;
                         
                        if(updateOLiMap.get(Oli.id).vlocity_cmt__OneTimeTotal__c != null )
                             Oli.vlocity_cmt__OneTimeTotal__c  = updateOLiMap.get(Oli.id).vlocity_cmt__OneTimeTotal__c ;

                       /* system.debug('Oli ID_____' +Oli.id );
                        System.debug('Oli Monthly price_____' +Oli.vlocity_cmt__RecurringTotal__c);
                         System.debug('Oli One time price_____' +Oli.vlocity_cmt__OneTimeTotal__c);*/
                  }
               }
            }
          }
    }

    public Map<String,Object> getCalculatedPrice(OrderItem oli, Decimal Qty, Decimal oneTimeCharge, Decimal recCharge){
                    Map<String, Object> outMap = new Map<String,Object>();  

                      Decimal otManDiscount = (Decimal) oli.vlocity_cmt__OneTimeManualDiscount__c;
                      Decimal recManDiscount = (Decimal) oli.vlocity_cmt__RecurringManualDiscount__c;

                      Qty = (Qty != null) ? Qty : 1;        
                      oneTimeCharge = (oneTimeCharge != null) ? oneTimeCharge : Decimal.valueOf(100000000); // Default price is expensive.
                      recCharge = (recCharge != null) ? recCharge : 0.0;

                        otManDiscount = (otManDiscount != null) ? otManDiscount : 0.0;
                      recManDiscount = (recManDiscount != null) ? recManDiscount : 0.0;

                      Decimal oneTimeComputePrice = oneTimeCharge - oneTimeCharge * otManDiscount/100;
                      Decimal oneTimeTotal = onetimeComputePrice * Qty;
      
                      Decimal recComputePrice = recCharge - recCharge * recManDiscount/100; 
                      Decimal recTotal = recComputePrice * Qty;

                      outMap.put('recTotal',recTotal);
                      outMap.put('oneTimeTotal',oneTimeTotal);
            return outMap;
    }

    private String getConfigurationValue(String setupName) {

        
        vlocity_cmt__CpqConfigurationSetup__c cpqSetup = vlocity_cmt__CpqConfigurationSetup__c.getInstance(setupName); 
        String retval = null;
        if (cpqSetup != null 
              && cpqSetup.vlocity_cmt__SetupValue__c != null
              && cpqSetup.vlocity_cmt__SetupValue__c.length() > 0) {
            retval = cpqSetup.vlocity_cmt__SetupValue__c;          
        }  
        return retval;
    }    
}