/*****************************************************
    Name: SendInfoForContactOrLead
    Usage: This class send mail for contact or lead to update their information.
    Author – Clear Task
    Date – 04/02/2011    
    Revision History
******************************************************/
public with sharing class  SendInfoForContactOrLead{
 /*
  * Variable in SendInfoForContactOrLead Class
  */ 
   // used for contact Id getting from Link of EMail
   private String contactId = ApexPages.currentPage().getParameters().get('contactId');
   // used for lead Id getting from Link of EMail
   private String leadId = ApexPages.currentPage().getParameters().get('leadId');
   // used for GUID getting from Link of EMail
   private String guid = ApexPages.currentPage().getParameters().get('uid');
   //updateMsg  method is for show the user information is update or not 
   public String updateMsg  {get; set;}
   //showValidateMsg method for validate message
   public String showValidateMsg{get; set;}
   //show Lead information when Lead record is to be update
   private boolean renderLeadBlock = false;
   //show Contact information when Contactd record is to be update
   private boolean renderContactBlock = false;
   //assigned the Contact field 
   public Contact updateContact {get; set;}
   //assigned the Lead field
   public Lead updateLead {get; set;}
   //show the status that record is update or not
   public Boolean saved {get; private set;}   
   //updateMsg  method is for show the user information is update or not 
   private String styleClassName;
   
   private boolean errorFirstName = false;

   
    
 /*
  * Constructure of SendInfoForContactOrLead Class
  */
   public SendInfoForContactOrLead() { 
       errorFirstName = true;
       styleClassName = 'borderMsgClearStyle';   
       
       if(leadId != null && leadId != '') {
            showLeadStatus();
        } else if(contactId != null && contactId != '') {
            showContactStatus();
        } else {           
            styleClassName = 'borderMsgStyle blueLine';  
            showValidateMsg = System.Label.lead_Id_Or_Contact_Id_does_not_exists;
                    
            renderLeadBlock = false;
            renderContactBlock = false;
            saved = false;
        }
   }
 /*
  * getGuid method for return the current assigned guid value
  */   
   public String getGuid() {
    return this.guid;
   }
   public boolean getErrorFirstName() {
    return errorFirstName;
   }

 /*
  * showLeadStatus method for record for Lead ID is exist or not
  */
  public void showLeadStatus(){ 
         List<Lead> leads = StayInTouchUtil.getLeadList(leadId);        
         if(leads != null && leads.size() != 0) { 
             this.updateLead = leads[0].clone(false, false);
             if(!isLeadConverted(updateLead.IsConverted)){
               if(isValidGuid(updateLead.guid__c)) {
                   renderLeadBlock = true;
               }
             }           
         }
         else {
            this.styleClassName = 'borderMsgStyle blueLine';
            showValidateMsg = System.Label.sorry_Lead_Id_does_not_exists;
         }
   }
 /*
  * showContactStatus method for record for Contact ID is exist or not
  */   
  public void showContactStatus(){
     List<Contact> contacts = StayInTouchUtil.getContactList(contactId);
     if(contacts != null && contacts.size() != 0) {
        updateContact = contacts[0].clone(false,false);
        if(isValidGuid(updateContact.guid__c)) {
                   renderContactBlock = true;
                 
        }
     } else {
        this.styleClassName = 'borderMsgStyle blueLine';
        showValidateMsg = System.Label.sorry_Contact_does_not_exists;
     }
   }
 /*
  * getRenderLeadBlock method for return flag true or false to show the Lead Information block
  */
   public boolean getRenderLeadBlock(){
     return renderLeadBlock;
   }
 /*
  * getRenderContactBlock method for return flag true or false to show the Contact Information block
  */
   public boolean getRenderContactBlock(){
     return renderContactBlock;
   }
   public String getStyleClassName(){
     return styleClassName;
   }
 /*
  * doSubmit method for update the information for Lead or Contact
  */
   public void doSubmit(){    
        if(contactId != null && contactId != '') {
            updateContact();
        }
        else if(leadId != null && leadId != '') {  
            styleClassName = 'borderMsgClearStyle'; 
            saveLead();
        }
  } 
  
 /*
  * updateContact method for update the information for Contact
  */
   public void updateContact(){
     string strFisrtName = updateContact.FirstName;
     errorFirstName = isMandatoryFieldEmpty(strFisrtName);    
     
     if(errorFirstName) {
       List<Contact> contactList = StayInTouchUtil.getContactList(contactId);
       if(contactList.size() == 0) {  
                showValidateMsg = System.Label.sorry_Contact_does_not_exists;
                return;
       }        
	    Contact c = contactList[0];

	    if(isValidGuid(c.GUID__C)) {
	        c.MailingPostalCode =updateContact.mailingPostalCode ;
	        c.MailingCountry = updateContact.mailingCountry ;
	        c.MailingState = updateContact.mailingState; 
	        c.MailingStreet = updateContact.mailingStreet;
	        c.MailingCity = updateContact.mailingCity ;
	        c.FirstName = updateContact.firstName ;
	        c.LastName = updateContact.lastName;
	        c.Email = updateContact.email ;
	        c.Phone = updateContact.phone;
	        c.GUID__c = ''; 
	        c.Last_Stay_in_touch_Date__c=System.now();
	        try {
	           StayInTouchUtil.updateCall(c);               
	           this.styleClassName = 'borderMsgStyle blueLine';
	           this.updateMsg = System.Label.information_has_been_updated_successfully;
	           saved = true;
	        } catch(DMLException e) {              
	           this.styleClassName = 'borderMsgStyle blueLine';
	           this.updateMsg = System.Label.error_in_saving_data;
	           saved = true;
	        }
	   		}
     }
   }
 /*
  * updateLead method for update the information for Lead 
  */
   public void updateLead(){    
     string strFisrtName = updateLead.FirstName;
     errorFirstName = isMandatoryFieldEmpty(strFisrtName);

    if(errorFirstName){
        List<Lead> leads = StayInTouchUtil.getLeadList(leadId);
        if(leads.size() == 0){
             this.styleClassName = 'borderMsgStyle blueLine';
             showValidateMsg = System.Label.sorry_Lead_Id_does_not_exists;
             return;
        }
        Lead currentLead = leads[0];
        if(isValidGuid(currentlead.GUID__C) && !isLeadConverted(currentLead.IsConverted)) {
            currentLead.FirstName = updateLead.FirstName;
            currentLead.LastName = updateLead.LastName;
            currentLead.Email = updateLead.Email;
            currentLead.Phone = updateLead.Phone;
            currentLead.State = updateLead.State;
            currentLead.Country = updateLead.Country;
            currentLead.PostalCode = updateLead.PostalCode;
            currentlead.City = updateLead.City;
            currentlead.Street = updateLead.Street;                      
            currentLead.GUID__c = ''; 
            currentLead.Last_Stay_in_touch_Date__c = System.now(); 
            try {            
                //update currentLead;
                StayInTouchUtil.updateCall(currentLead);                
                this.styleClassName = 'borderMsgStyle blueLine';
                this.updateMsg = System.Label.information_has_been_updated_successfully;
                saved = true;
            } catch(DMLException e) {               
                this.styleClassName = 'borderMsgStyle blueLine';
                this.updateMsg = System.Label.error_in_saving_data;
                saved = true;
            }
        }
      } 
   }
   public void saveLead(){    
        List<Lead> leads = StayInTouchUtil.getLeadList(leadId);
        if(leads.size() == 0){ 
            this.styleClassName = 'borderMsgStyle blueLine';
            showValidateMsg = System.Label.sorry_Lead_Id_does_not_exists;
            return;
        }
        Lead currentLead = leads[0];        
        if(isValidGuid(currentlead.GUID__C) && !isLeadConverted(currentLead.IsConverted)) {
            currentLead.FirstName = updateLead.FirstName;
            currentLead.LastName = updateLead.LastName;
            currentLead.Email = updateLead.Email;
            currentLead.Phone = updateLead.Phone;
            currentLead.State = updateLead.State;
            currentLead.Country = updateLead.Country;
            currentLead.PostalCode = updateLead.PostalCode;
            currentlead.City = updateLead.City;
            currentlead.Street = updateLead.Street;                      
            currentlead.GUID__c = ''; 
            currentlead.Last_Stay_in_touch_Date__c = System.now(); 
            try {            
                StayInTouchUtil.updateCall(currentlead);
                this.updateMsg = System.Label.information_has_been_updated_successfully;
                saved = true;
                this.styleClassName = 'borderMsgStyle blueLine';
            } catch(DMLException e) {                
                this.styleClassName = 'borderMsgStyle blueLine';
                this.updateMsg = System.Label.error_in_saving_data;
                saved = true;
            }
        }
   }
 
 /*
  * isValidGuid method return true or false after matching the GUID of EMail and GUID in DB stored
  */   
   public boolean isValidGuid(String dbGuid) {
         if((dbGuid!= null) && (dbGuid).trim() != '') { 
            if(dbGuid.equals(guid)) {
               return true;
            } else {                    
               this.styleClassName = 'borderMsgStyle blueLine';           
               showValidateMsg = System.Label.sorry_Your_GUID_does_not_match;
               return false;
            }
         } else {
                this.styleClassName = 'borderMsgStyle blueLine';                
                showValidateMsg = System.Label.sorry_This_link_is_already_used_once_and_can_not_be_used_again;
                return false;
         }
   }
   
 /*
  * isLeadConverted method return true if LEad has been converted into Opprtunity otherwise return false
  */     
   public boolean isLeadConverted(boolean isConverted) {          
          if(isConverted) {            
            this.styleClassName = 'borderMsgStyle blueLine';
            showValidateMsg = System.Label.lead_has_already_been_Converted;           
          }
          return isConverted;
   }
 /*
  * isMandatoryFieldEmpty method return true if string is not null  or empty
  */   
   public boolean isMandatoryFieldEmpty(string txtValue) {       
       if(txtValue != null && txtValue.trim() != '') {
         return true;
       } else {
         return false;
       }
   }

}