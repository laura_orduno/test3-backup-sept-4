@isTest(seealldata=false)
private class voc_processRecordsTest {
	/*
    static testMethod void insertRecords() {
      
        List<VOC_Survey_Type__c> voctypes = new List<VOC_Survey_Type__c>();
        VOC_Survey_Type__c voctype1 = new VOC_Survey_Type__c(Requires_Fielded__c = false, Survey_URL__c = 'test', Type__c= 1);
        vocTypes.add(voctype1);
        VOC_Survey_Type__c voctype2 = new VOC_Survey_Type__c(Requires_Fielded__c = true, Survey_URL__c = 'test', Type__c= 2);        
        vocTypes.add(voctype2);
        insert vocTypes;
        
        List<Account> accountsToInsert = new List<Account>();
        Account cbucidacct = new Account(Name='cbucid',recordtypeid=getAccountRecordTypeIdByName('CBUCID'));
        accountsToInsert.add(cbucidacct);
        Account rcidacct = new Account(Name='rcid',recordtypeid=getAccountRecordTypeIdByName('RCID'),parentid = cbucidacct.id);
        accountsToInsert.add(rcidacct);
        Account canacct = new Account(Name='can',recordtypeid=getAccountRecordTypeIdByName('CAN'),parentid = rcidacct.id);
        accountsToInsert.add(canacct);
        Account canacct2 = new Account(Name='can2',recordtypeid=getAccountRecordTypeIdByName('CAN'),parentid = rcidacct.id);
        accountsToInsert.add(canacct2);
        Account canacct3 = new Account(Name='can3',recordtypeid=getAccountRecordTypeIdByName('CAN'),parentid = rcidacct.id);
        accountsToInsert.add(canacct3);
        Account canacct4 = new Account(Name='can3',recordtypeid=getAccountRecordTypeIdByName('CAN'),parentid = canacct3.id);
        accountsToInsert.add(canacct4);
        Account rcidacct2 = new Account(Name='dummy',recordtypeid=getAccountRecordTypeIdByName('RCID'));
        accountsToInsert.add(rcidacct2);        
        insert accountsToInsert;

        VOC_Dummy_Account__c vocCStemp = VOC_Dummy_Account__c.getvalues('dummy');
        if(vocCStemp ==null) {
            VOC_Dummy_Account__c vocCS = new VOC_Dummy_Account__c();
            vocCS.name = 'dummy';
            vocCS.Account_Id__c = rcidacct2.id;
            insert vocCS;
        } else {
            vocCStemp.Account_Id__c = rcidacct2.id;
            update vocCStemp;
        }
                
        List<Contact> cons = new List<Contact>();
        Contact rcidcon1 = new Contact(LastName='last',accountid=rcidacct.id,firstname='first',email='test@test.com');
        Contact rcidcon2 = new Contact(LastName='last',accountid=rcidacct.id);
        Contact rcidcon3 = new Contact(LastName='last',accountid=rcidacct.id,firstname='first');                
        Contact cancon1 = new Contact(LastName='last1',accountid=canacct.id,firstname='first1',email='test@test.com');
        Contact cancon2 = new Contact(LastName='last2',accountid=canacct2.id);
        Contact cancon3 = new Contact(LastName='last3',accountid=canacct3.id,firstname='first3');
        Contact cancon4 = new Contact(LastName='last4',accountid=canacct4.id,firstname='first4',email='test4@test.com');
        cons.add(rcidcon1);
        cons.add(rcidcon2);
        cons.add(rcidcon3);
        cons.add(cancon1);
        cons.add(cancon2);
        cons.add(cancon3);
        cons.add(cancon4);
        insert cons;
        
        List<VOC_Survey__c> vocs = new List<VOC_Survey__c>();
        VOC_Survey__c voc1 = new VOC_Survey__c(
                                    Account_No__c='test',
                                    Agent_ID__c='T876522',
                                    Business_Name__c='test',
                                    Case_Owner__c='test',
                                    Case_Subject__c='test',
                                    Customer_Email__c='test@test.com',
                                    Customer_First_Name__c='test',
                                    Customer_Last_Name__c='test',
                                    Customer_Phone__c='4555555555',
                                    Custom_ID__c='testtest1',
                                    Interaction_Date__c=Date.today(),
                                    Location__c='test',
                                    RCID__c='1',
                                    Reference__c='test',
                                    Ticket__c='test',
                                    Survey_Type__c=1
                                );
        vocs.add(voc1);
        VOC_Survey__c voc2 = new VOC_Survey__c(
                                    Account_No__c='test',
                                    Agent_ID__c='T876522',
                                    Business_Name__c='test',
                                    Case_Owner__c='test',
                                    Case_Subject__c='test',
                                    Customer_Email__c='test@test.com',
                                    Customer_First_Name__c='first3',
                                    Customer_Last_Name__c='last3',
                                    Customer_Phone__c='4555555555',
                                    Custom_ID__c='testtest2',
                                    Interaction_Date__c=Date.today(),
                                    Location__c='test',
                                    RCID__c='1',
                                    Reference__c='test',
                                    Ticket__c='test',
                                    Survey_Type__c=1,
                                    update_manager_emails__c=true,
                                    manager_callback__c=true
                                );
        vocs.add(voc2);
        VOC_Survey__c voc3 = new VOC_Survey__c(
                                    Account_No__c='test',
                                    Agent_ID__c='T876522',
                                    Business_Name__c='test',
                                    Case_Owner__c='test',
                                    Case_Subject__c='test',
                                    Customer_Email__c='test4@test.com',
                                    Customer_First_Name__c='first4',
                                    Customer_Last_Name__c='last4',
                                    Customer_Phone__c='4555555555',
                                    Custom_ID__c='testtest992',
                                    Interaction_Date__c=Date.today(),
                                    Location__c='test',
                                    RCID__c='122323',
                                    Reference__c='test',
                                    Ticket__c='test',
                                    Survey_Type__c=1,
                                    update_manager_emails__c=true,
                                    manager_callback__c=true
                                );
        vocs.add(voc3);
        test.starttest();
        insert vocs;
        test.stoptest();
        
    }*/
    static testMethod void insertVolumeRecords() {
      
        List<VOC_Survey_Type__c> vocTypes = new List<VOC_Survey_Type__c>();
        List<voc_survey_emails__c> surveyEmails = new List<voc_survey_emails__c>();
        map<integer,integer> surveyTypeIndexMap=new map<integer,integer>();
        integer maxSurveTypes=5;
        for(integer i=0;i<maxSurveTypes;i++){
            VOC_Survey_Type__c voctype = new VOC_Survey_Type__c(Requires_Fielded__c = false, Survey_URL__c = 'test', Type__c=i+1);
            vocTypes.add(voctype);
            surveyTypeIndexMap.put(integer.valueof(voctype.type__c),i);
            voc_survey_emails__c surveyEmail = new voc_survey_emails__c(name=''+(i+1));
            surveyEmails.add(surveyEmail);
        }
        insert vocTypes;
        for(voc_survey_emails__c surveyEmail:surveyEmails){
            system.debug('Contains:'+surveyTypeIndexMap.containskey(integer.valueof(surveyEmail.name))+', name:'+integer.valueof(surveyEmail.name));
            voc_survey_type__c surveyType=vocTypes.get(surveyTypeIndexMap.get(integer.valueof(surveyEmail.name)));
            surveyEmail.survey_type_id__c=surveyType.id;
        }
        insert surveyEmails;
        
        List<Account> accountsToInsert = new List<Account>();
        Account cbucidacct = new Account(Name='cbucid',recordtypeid=getAccountRecordTypeIdByName('CBUCID'));
        accountsToInsert.add(cbucidacct);
        Account rcidacct = new Account(Name='rcid',recordtypeid=getAccountRecordTypeIdByName('RCID'),parentid = cbucidacct.id);
        accountsToInsert.add(rcidacct);
        Account rcidacct2 = new Account(Name='dummy',recordtypeid=getAccountRecordTypeIdByName('RCID'));
        accountsToInsert.add(rcidacct2);        
        insert accountsToInsert;

        VOC_Dummy_Account__c vocCStemp = VOC_Dummy_Account__c.getvalues('dummy');
        if(vocCStemp ==null) {
            VOC_Dummy_Account__c vocCS = new VOC_Dummy_Account__c();
            vocCS.name = 'dummy';
            vocCS.Account_Id__c = rcidacct2.id;
            insert vocCS;
        } else {
            vocCStemp.Account_Id__c = rcidacct2.id;
            update vocCStemp;
        }
                
        List<Contact> cons = new List<Contact>();
        List<VOC_Survey__c> vocs = new List<VOC_Survey__c>();
        integer maxContacts=120;
        integer counter=1;
        for(integer i=111;i<maxContacts;i++){
            cons.add(new Contact(lastname='last'+i,accountid=rcidacct.id,firstname='first'+i,email='test'+i+'@test.com'));
            for(voc_survey_type__c surveyType:vocTypes){
                vocs.add(new VOC_Survey__c(
                    Account_No__c='test',
                    Agent_ID__c='T876522',
                    Business_Name__c='test',
                    Case_Owner__c='test',
                    Case_Subject__c='test',
                    Customer_Email__c='test'+i+'@test.com',
                    Customer_First_Name__c='first'+i,
                    Customer_Last_Name__c='last'+i,
                    Customer_Phone__c='4555555555',
                    Custom_ID__c='testtest9'+(i+counter),
                    Interaction_Date__c=Date.today(),
                    Location__c='test',
                    manager_ids__c=userinfo.getuserid(),
                    RCID__c='122323',
                    Reference__c='test',
                    Ticket__c='test',
                    Survey_Type__c=surveyType.type__c,
                    update_manager_emails__c=true,
                    manager_callback__c=true
                ));
                counter++;
            }
        }
        insert cons;
        cons.clear();
        VOC_Survey_Load__c load=new VOC_Survey_Load__c();
        test.starttest();
        insert vocs;
        insert load;
        test.stoptest();
        vocs.clear();
    }
    /*
    
    static testMethod void insertRecordsError() {
      
        List<VOC_Survey_Type__c> voctypes = new List<VOC_Survey_Type__c>();
        VOC_Survey_Type__c voctype1 = new VOC_Survey_Type__c(Requires_Fielded__c = false, Survey_URL__c = 'test', Type__c= 1);
        vocTypes.add(voctype1);
        VOC_Survey_Type__c voctype2 = new VOC_Survey_Type__c(Requires_Fielded__c = true, Survey_URL__c = 'test', Type__c= 2);        
        vocTypes.add(voctype2);
        insert vocTypes;
        
        List<Account> accountsToInsert = new List<Account>();
        Account rcidacct = new Account(Name='rcid',recordtypeid=getAccountRecordTypeIdByName('RCID'));
        accountsToInsert.add(rcidacct);
        Account canacct = new Account(Name='can',recordtypeid=getAccountRecordTypeIdByName('CAN'),parentid = rcidacct.id);
        accountsToInsert.add(canacct);
        Account rcidacct2 = new Account(Name='dummy',recordtypeid=getAccountRecordTypeIdByName('RCID'));
        accountsToInsert.add(rcidacct2);        
        insert accountsToInsert;

        VOC_Dummy_Account__c vocCStemp = VOC_Dummy_Account__c.getvalues('dummy');
        if(vocCStemp ==null) {
            VOC_Dummy_Account__c vocCS = new VOC_Dummy_Account__c();
            vocCS.name = 'dummy';
            vocCS.Account_Id__c = rcidacct2.id;
            insert vocCS;
        } else {
            vocCStemp.Account_Id__c = rcidacct2.id;
            update vocCStemp;
        }
        
        List<Contact> cons = new List<Contact>();
        Contact rcidcon1 = new Contact(LastName='last',accountid=rcidacct.id,firstname='first',email='test@test.com');
        Contact rcidcon2 = new Contact(LastName='last',accountid=rcidacct.id);
        Contact rcidcon3 = new Contact(LastName='last',accountid=rcidacct.id,firstname='first');                
        Contact cancon1 = new Contact(LastName='last1',accountid=rcidacct.id,firstname='first1',email='test@test.com');
        Contact cancon2 = new Contact(LastName='last2',accountid=rcidacct.id);
        Contact cancon3 = new Contact(LastName='last3',accountid=rcidacct.id,firstname='first3');
        cons.add(rcidcon1);
        cons.add(rcidcon2);
        cons.add(rcidcon3);
        cons.add(cancon1);
        cons.add(cancon2);
        cons.add(cancon3);
        insert cons;
        
        List<VOC_Survey__c> vocs = new List<VOC_Survey__c>();
        VOC_Survey__c voc3 = new VOC_Survey__c(
                                    Account_No__c='test',
                                    Agent_ID__c='T876522',
                                    Business_Name__c='test',
                                    Case_Owner__c='test',
                                    Case_Subject__c='test',
                                    Customer_Email__c='test@test.com',
                                    Customer_First_Name__c='first3',
                                    Customer_Last_Name__c='last3',
                                    Customer_Phone__c='4555555555',
                                    Custom_ID__c='testtest3',
                                    Interaction_Date__c=Date.today(),
                                    Location__c='test',
                                    RCID__c='1',
                                    Reference__c='test',
                                    Ticket__c='test'
                                );
        vocs.add(voc3);
        try{
            insert vocs;
        } catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('Make sure it exists first') ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        }
    }
    
    static testMethod void insertRecordsNoRcid() {
      
        List<VOC_Survey_Type__c> voctypes = new List<VOC_Survey_Type__c>();
        VOC_Survey_Type__c voctype1 = new VOC_Survey_Type__c(Requires_Fielded__c = false, Survey_URL__c = 'test', Type__c= 1);
        vocTypes.add(voctype1);
        VOC_Survey_Type__c voctype2 = new VOC_Survey_Type__c(Requires_Fielded__c = true, Survey_URL__c = 'test', Type__c= 2);        
        vocTypes.add(voctype2);
        insert vocTypes;
        
        List<Account> accountsToInsert = new List<Account>();
        Account rcidacct = new Account(Name='rcid',recordtypeid=getAccountRecordTypeIdByName('RCID'));
        accountsToInsert.add(rcidacct);
        Account canacct = new Account(Name='can',recordtypeid=getAccountRecordTypeIdByName('CAN'),parentid = rcidacct.id);
        accountsToInsert.add(canacct);
        Account rcidacct2 = new Account(Name='dummy',recordtypeid=getAccountRecordTypeIdByName('RCID'));
        accountsToInsert.add(rcidacct2);        
        insert accountsToInsert;

        VOC_Dummy_Account__c vocCStemp = VOC_Dummy_Account__c.getvalues('dummy');
        if(vocCStemp ==null) {
            VOC_Dummy_Account__c vocCS = new VOC_Dummy_Account__c();
            vocCS.name = 'dummy';
            vocCS.Account_Id__c = rcidacct2.id;
            insert vocCS;
        } else {
            vocCStemp.Account_Id__c = rcidacct2.id;
            update vocCStemp;
        }
        
        List<Contact> cons = new List<Contact>();
        Contact rcidcon1 = new Contact(LastName='last',accountid=rcidacct.id,firstname='first',email='test@test.com');
        Contact rcidcon2 = new Contact(LastName='last',accountid=rcidacct.id);
        Contact rcidcon3 = new Contact(LastName='last',accountid=rcidacct.id,firstname='first');                
        Contact cancon1 = new Contact(LastName='last1',accountid=rcidacct.id,firstname='first1',email='test@test.com');
        Contact cancon2 = new Contact(LastName='last2',accountid=rcidacct.id);
        Contact cancon3 = new Contact(LastName='last3',accountid=rcidacct.id,firstname='first3');
        cons.add(rcidcon1);
        cons.add(rcidcon2);
        cons.add(rcidcon3);
        cons.add(cancon1);
        cons.add(cancon2);
        cons.add(cancon3);
        insert cons;
        
        List<VOC_Survey__c> vocs = new List<VOC_Survey__c>();
        VOC_Survey__c voc3 = new VOC_Survey__c(
                                    Account_No__c='test',
                                    Agent_ID__c='T876522',
                                    Business_Name__c='test',
                                    Case_Owner__c='test',
                                    Case_Subject__c='test',
                                    Customer_Email__c='test@test.com',
                                    Customer_First_Name__c='first3',
                                    Customer_Last_Name__c='last3',
                                    Customer_Phone__c='4555555555',
                                    Custom_ID__c='testtest3',
                                    Interaction_Date__c=Date.today(),
                                    Location__c='test',
                                    Reference__c='test',
                                    Ticket__c='test'
                                );
        vocs.add(voc3);
        try{
            insert vocs;
        } catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('Make sure it exists first') ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        }
    }
    
    static testMethod void insertRecordsNoEmail() {
      
        List<VOC_Survey_Type__c> voctypes = new List<VOC_Survey_Type__c>();
        VOC_Survey_Type__c voctype1 = new VOC_Survey_Type__c(Requires_Fielded__c = false, Survey_URL__c = 'test', Type__c= 1);
        vocTypes.add(voctype1);
        VOC_Survey_Type__c voctype2 = new VOC_Survey_Type__c(Requires_Fielded__c = true, Survey_URL__c = 'test', Type__c= 2);        
        vocTypes.add(voctype2);
        insert vocTypes;
        
        List<Account> accountsToInsert = new List<Account>();
        Account rcidacct = new Account(Name='rcid',recordtypeid=getAccountRecordTypeIdByName('RCID'));
        accountsToInsert.add(rcidacct);
        Account canacct = new Account(Name='can',recordtypeid=getAccountRecordTypeIdByName('CAN'),parentid = rcidacct.id);
        accountsToInsert.add(canacct);
        Account rcidacct2 = new Account(Name='dummy',recordtypeid=getAccountRecordTypeIdByName('RCID'));
        accountsToInsert.add(rcidacct2);        
        insert accountsToInsert;

        VOC_Dummy_Account__c vocCStemp = VOC_Dummy_Account__c.getvalues('dummy');
        if(vocCStemp ==null) {
            VOC_Dummy_Account__c vocCS = new VOC_Dummy_Account__c();
            vocCS.name = 'dummy';
            vocCS.Account_Id__c = rcidacct2.id;
            insert vocCS;
        } else {
            vocCStemp.Account_Id__c = rcidacct2.id;
            update vocCStemp;
        }
        
        List<Contact> cons = new List<Contact>();
        Contact rcidcon1 = new Contact(LastName='last',accountid=rcidacct.id,firstname='first',email='test@test.com');
        Contact rcidcon2 = new Contact(LastName='last',accountid=rcidacct.id);
        Contact rcidcon3 = new Contact(LastName='last',accountid=rcidacct.id,firstname='first');                
        Contact cancon1 = new Contact(LastName='last1',accountid=rcidacct.id,firstname='first1',email='test@test.com');
        Contact cancon2 = new Contact(LastName='last2',accountid=rcidacct.id);
        Contact cancon3 = new Contact(LastName='last3',accountid=rcidacct.id,firstname='first3');
        cons.add(rcidcon1);
        cons.add(rcidcon2);
        cons.add(rcidcon3);
        cons.add(cancon1);
        cons.add(cancon2);
        cons.add(cancon3);
        insert cons;
        
        List<VOC_Survey__c> vocs = new List<VOC_Survey__c>();
        VOC_Survey__c voc3 = new VOC_Survey__c(
                                    Account_No__c='test',
                                    Agent_ID__c='T876522',
                                    Business_Name__c='test',
                                    Case_Owner__c='test',
                                    Case_Subject__c='test',
                                    Customer_First_Name__c='first3',
                                    Customer_Last_Name__c='last3',
                                    Customer_Phone__c='4555555555',
                                    Custom_ID__c='testtest3',
                                    Interaction_Date__c=Date.today(),
                                    Location__c='test',
                                    Reference__c='test',
                                    Ticket__c='test'
                                );
        vocs.add(voc3);
        try{
            insert vocs;
        } catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('Make sure it exists first') ? true : false;
            //System.AssertEquals(expectedExceptionThrown, true);
        }
    }
    
    static testMethod void insertRecordsNewEmail() {
      
        List<VOC_Survey_Type__c> voctypes = new List<VOC_Survey_Type__c>();
        VOC_Survey_Type__c voctype1 = new VOC_Survey_Type__c(Requires_Fielded__c = false, Survey_URL__c = 'test', Type__c= 1);
        vocTypes.add(voctype1);
        VOC_Survey_Type__c voctype2 = new VOC_Survey_Type__c(Requires_Fielded__c = true, Survey_URL__c = 'test', Type__c= 2);        
        vocTypes.add(voctype2);
        insert vocTypes;
        
        List<Account> accountsToInsert = new List<Account>();
        Account rcidacct = new Account(Name='rcid',recordtypeid=getAccountRecordTypeIdByName('RCID'));
        accountsToInsert.add(rcidacct);
        Account canacct = new Account(Name='can',recordtypeid=getAccountRecordTypeIdByName('CAN'),parentid = rcidacct.id);
        accountsToInsert.add(canacct);
        Account rcidacct2 = new Account(Name='dummy',recordtypeid=getAccountRecordTypeIdByName('RCID'));
        accountsToInsert.add(rcidacct2);        
        insert accountsToInsert;

        VOC_Dummy_Account__c vocCStemp = VOC_Dummy_Account__c.getvalues('dummy');
        if(vocCStemp ==null) {
            VOC_Dummy_Account__c vocCS = new VOC_Dummy_Account__c();
            vocCS.name = 'dummy';
            vocCS.Account_Id__c = rcidacct2.id;
            insert vocCS;
        } else {
            vocCStemp.Account_Id__c = rcidacct2.id;
            update vocCStemp;
        }
        
        List<Contact> cons = new List<Contact>();
        Contact rcidcon1 = new Contact(LastName='last',accountid=rcidacct.id,firstname='first',email='test@test.com');
        Contact rcidcon2 = new Contact(LastName='last',accountid=rcidacct.id);
        Contact rcidcon3 = new Contact(LastName='last',accountid=rcidacct.id,firstname='first');                
        Contact cancon1 = new Contact(LastName='last1',accountid=rcidacct.id,firstname='first1',email='test@test.com');
        Contact cancon2 = new Contact(LastName='last2',accountid=rcidacct.id);
        Contact cancon3 = new Contact(LastName='last3',accountid=rcidacct.id,firstname='first3');
        cons.add(rcidcon1);
        cons.add(rcidcon2);
        cons.add(rcidcon3);
        cons.add(cancon1);
        cons.add(cancon2);
        cons.add(cancon3);
        insert cons;
        
        List<VOC_Survey__c> vocs = new List<VOC_Survey__c>();
        VOC_Survey__c voc1 = new VOC_Survey__c(
                                    Account_No__c='test',
                                    Agent_ID__c='T876522',
                                    Business_Name__c='test',
                                    Case_Owner__c='test',
                                    Case_Subject__c='test',
                                    Customer_Email__c='testadsf@test.com',
                                    Customer_First_Name__c='test',
                                    Customer_Last_Name__c='test',
                                    Customer_Phone__c='4555555555',
                                    Custom_ID__c='testtest1',
                                    Interaction_Date__c=Date.today(),
                                    Location__c='test',
                                    RCID__c='1',
                                    Reference__c='test',
                                    Ticket__c='test',
                                    Survey_Type__c=1
                                );
        vocs.add(voc1);
        VOC_Survey__c voc2 = new VOC_Survey__c(
                                    Account_No__c='test',
                                    Agent_ID__c='T876522',
                                    Business_Name__c='test',
                                    Case_Owner__c='test',
                                    Case_Subject__c='test',
                                    Customer_Email__c='tesasdft@test.com',
                                    Customer_First_Name__c='first3',
                                    Customer_Last_Name__c='last3',
                                    Customer_Phone__c='4555555555',
                                    Custom_ID__c='testtest2',
                                    Interaction_Date__c=Date.today(),
                                    Location__c='test',
                                    RCID__c='1',
                                    Reference__c='test',
                                    Ticket__c='test',
                                    Survey_Type__c=1,
                                    update_manager_emails__c=true,
                                    manager_callback__c=true
                                );
        vocs.add(voc2);
        insert vocs;
        
    }
    
    static testMethod void insertRecordsDummy() {
      
        List<VOC_Survey_Type__c> voctypes = new List<VOC_Survey_Type__c>();
        VOC_Survey_Type__c voctype1 = new VOC_Survey_Type__c(Requires_Fielded__c = false, Survey_URL__c = 'test', Type__c= 1);
        vocTypes.add(voctype1);
        VOC_Survey_Type__c voctype2 = new VOC_Survey_Type__c(Requires_Fielded__c = true, Survey_URL__c = 'test', Type__c= 2);        
        vocTypes.add(voctype2);
        insert vocTypes;
        
        List<Account> accountsToInsert = new List<Account>();
        Account rcidacct = new Account(Name='rcid',recordtypeid=getAccountRecordTypeIdByName('RCID'));
        accountsToInsert.add(rcidacct);
        Account canacct = new Account(Name='can',recordtypeid=getAccountRecordTypeIdByName('CAN'),parentid = rcidacct.id);
        accountsToInsert.add(canacct);
        Account rcidacct2 = new Account(Name='dummy',recordtypeid=getAccountRecordTypeIdByName('RCID'));
        accountsToInsert.add(rcidacct2);        
        insert accountsToInsert;

        VOC_Dummy_Account__c vocCStemp = VOC_Dummy_Account__c.getvalues('dummy');
        if(vocCStemp ==null) {
            VOC_Dummy_Account__c vocCS = new VOC_Dummy_Account__c();
            vocCS.name = 'dummy';
            vocCS.Account_Id__c = rcidacct2.id;
            insert vocCS;
        } else {
            vocCStemp.Account_Id__c = rcidacct2.id;
            update vocCStemp;
        }
        
        List<Contact> cons = new List<Contact>();
        Contact rcidcon1 = new Contact(LastName='last',accountid=rcidacct.id,firstname='first',email='test@test.com');
        Contact rcidcon2 = new Contact(LastName='last',accountid=rcidacct.id);
        Contact rcidcon3 = new Contact(LastName='last',accountid=rcidacct.id,firstname='first');                
        Contact cancon1 = new Contact(LastName='last1',accountid=rcidacct.id,firstname='first1',email='test@test.com');
        Contact cancon2 = new Contact(LastName='last2',accountid=rcidacct.id);
        Contact cancon3 = new Contact(LastName='last3',accountid=rcidacct.id,firstname='first3');
        cons.add(rcidcon1);
        cons.add(rcidcon2);
        cons.add(rcidcon3);
        cons.add(cancon1);
        cons.add(cancon2);
        cons.add(cancon3);
        insert cons;
        
        List<VOC_Survey__c> vocs = new List<VOC_Survey__c>();
        VOC_Survey__c voc1 = new VOC_Survey__c(
                                    Account_No__c='test',
                                    Agent_ID__c='T876522',
                                    Business_Name__c='test',
                                    Case_Owner__c='test',
                                    Case_Subject__c='test',
                                    Customer_Email__c='tes234tadsf@test.com',
                                    Customer_First_Name__c='test',
                                    Customer_Last_Name__c='test',
                                    Customer_Phone__c='4555555555',
                                    Custom_ID__c='testtest1',
                                    Interaction_Date__c=Date.today(),
                                    Location__c='test',
                                    RCID__c='2342341',
                                    Reference__c='test',
                                    Ticket__c='test',
                                    Survey_Type__c=1
                                );
        vocs.add(voc1);
        VOC_Survey__c voc2 = new VOC_Survey__c(
                                    Account_No__c='test',
                                    Agent_ID__c='T876522',
                                    Business_Name__c='test',
                                    Case_Owner__c='test',
                                    Case_Subject__c='test',
                                    Customer_Email__c='tes234asdft@test.com',
                                    Customer_First_Name__c='first3',
                                    Customer_Last_Name__c='last3',
                                    Customer_Phone__c='4555555555',
                                    Interaction_Date__c=Date.today(),
                                    Location__c='test',
                                    RCID__c='1234234',
                                    Custom_ID__c='testtest1343',
                                    Reference__c='test',
                                    Ticket__c='test',
                                    Survey_Type__c=1,
                                    update_manager_emails__c=true,
                                    manager_callback__c=true
                                );
        vocs.add(voc2);
        insert vocs;
        
    }
    
    static testMethod void insertRecordsUpdateManagerNoAgent() {
      
        List<VOC_Survey_Type__c> voctypes = new List<VOC_Survey_Type__c>();
        VOC_Survey_Type__c voctype1 = new VOC_Survey_Type__c(Requires_Fielded__c = false, Survey_URL__c = 'test', Type__c= 1);
        vocTypes.add(voctype1);
        VOC_Survey_Type__c voctype2 = new VOC_Survey_Type__c(Requires_Fielded__c = true, Survey_URL__c = 'test', Type__c= 2);        
        vocTypes.add(voctype2);
        insert vocTypes;
        
        List<Account> accountsToInsert = new List<Account>();
        Account rcidacct = new Account(Name='rcid',recordtypeid=getAccountRecordTypeIdByName('RCID'));
        accountsToInsert.add(rcidacct);
        Account canacct = new Account(Name='can',recordtypeid=getAccountRecordTypeIdByName('CAN'),parentid = rcidacct.id);
        accountsToInsert.add(canacct);
        Account rcidacct2 = new Account(Name='dummy',recordtypeid=getAccountRecordTypeIdByName('RCID'));
        accountsToInsert.add(rcidacct2);        
        insert accountsToInsert;

        VOC_Dummy_Account__c vocCStemp = VOC_Dummy_Account__c.getvalues('dummy');
        if(vocCStemp ==null) {
            VOC_Dummy_Account__c vocCS = new VOC_Dummy_Account__c();
            vocCS.name = 'dummy';
            vocCS.Account_Id__c = rcidacct2.id;
            insert vocCS;
        } else {
            vocCStemp.Account_Id__c = rcidacct2.id;
            update vocCStemp;
        }
        
        List<Contact> cons = new List<Contact>();
        Contact rcidcon1 = new Contact(LastName='last',accountid=rcidacct.id,firstname='first',email='test@test.com');
        Contact rcidcon2 = new Contact(LastName='last',accountid=rcidacct.id);
        Contact rcidcon3 = new Contact(LastName='last',accountid=rcidacct.id,firstname='first');                
        Contact cancon1 = new Contact(LastName='last1',accountid=rcidacct.id,firstname='first1',email='test@test.com');
        Contact cancon2 = new Contact(LastName='last2',accountid=rcidacct.id);
        Contact cancon3 = new Contact(LastName='last3',accountid=rcidacct.id,firstname='first3');
        cons.add(rcidcon1);
        cons.add(rcidcon2);
        cons.add(rcidcon3);
        cons.add(cancon1);
        cons.add(cancon2);
        cons.add(cancon3);
        insert cons;
        
        List<VOC_Survey__c> vocs = new List<VOC_Survey__c>();
        VOC_Survey__c voc1 = new VOC_Survey__c(
                                    Account_No__c='test',
                                    Business_Name__c='test',
                                    Case_Owner__c='test',
                                    Case_Subject__c='test',
                                    Customer_Email__c='tes234tadsf@test.com',
                                    Customer_First_Name__c='test',
                                    Customer_Last_Name__c='test',
                                    Customer_Phone__c='4555555555',
                                    Interaction_Date__c=Date.today(),
                                    Location__c='test',
                                    Agent_ID__c='T34576522',
                                    Custom_ID__c='testt3es2t1',                                    
                                    RCID__c='2342341',
                                    Reference__c='test',
                                    Ticket__c='test',
                                    Survey_Type__c=1,
                                    update_manager_emails__c=true,
                                    manager_callback__c=true
                                );
        vocs.add(voc1);
        VOC_Survey__c voc2 = new VOC_Survey__c(
                                    Account_No__c='test',
                                    Business_Name__c='test',
                                    Case_Owner__c='test',
                                    Case_Subject__c='test',
                                    Customer_Email__c='tes234asdft@test.com',
                                    Customer_First_Name__c='first3',
                                    Customer_Last_Name__c='last3',
                                    Customer_Phone__c='4555555555',
                                    Interaction_Date__c=Date.today(),
                                    Location__c='test',
                                    RCID__c='1234234',
                                    Custom_ID__c='testtest2342341',                                    
                                    Agent_ID__c='T34576522',
                                    Reference__c='test',
                                    Ticket__c='test',
                                    Survey_Type__c=1,
                                    update_manager_emails__c=true,
                                    manager_callback__c=true
                                );
        vocs.add(voc2);
        insert vocs;
        
    }
	*/
    private static id getAccountRecordTypeIdByName(String name) {
        Schema.DescribeSObjectResult atoken = Schema.SObjectType.Account;
        Map<String, RecordTypeInfo> recordTypeInfoByName = atoken.getRecordTypeInfosByName();
        return recordTypeInfoByName.get(name).getRecordTypeId();
    }

}