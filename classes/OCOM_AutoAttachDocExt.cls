public with sharing class OCOM_AutoAttachDocExt {
  public id versionId {get;set;}
 
    string updateParam = '';
    public id contractID{get;set;}
    public static vlocity_cmt__ContractVersion__c activeContVersion {get;set;}
    public list<vlocity_cmt__ContractVersion__c> conVersion = new list<vlocity_cmt__ContractVersion__c>();
    public boolean isUpdate { get;set;}
    public string templatePicker = '';
    public String errorMessage{get;set;}
    
    public boolean isInConsole {
     get{
        Map<String, String> parametersMap = ApexPages.currentPage().getParameters();   
        for(String key : parametersMap.keySet()){
            if(key.trim().equalsIgnoreCase('isInConsole')){
                String value=parametersMap.get(key);
                if(value.trim().equals('true')){
                   return true;
                }
                else if(value.trim().equals('true')){
                    return false;
                }
            }
        }

        return false;
     }
     set;
   }
   
    public OCOM_AutoAttachDocExt(vlocity_cmt.ContractDocumentCreationController con){ 
        contractID = ApexPages.currentPage().getParameters().get('id');
        updateParam = ApexPages.currentPage().getParameters().get('isUpdate');
        templatePicker = ApexPages.currentPage().getParameters().get('templatePicker');

        if(updateParam != null && updateParam.trim().equalsIgnoreCase('true'))
            isUpdate = true;
        else 
            isUpdate = false;
            
        system.debug('isUpdate'+ isUpdate);
       // setStatResPath(statResName);
        
    }
    
    public void attachDoc(){
            //Create new Version if the request is update
             List<vlocity_cmt__ContractSection__c> oldList = new List<vlocity_cmt__ContractSection__c>();
             List<vlocity_cmt__ContractVersion__c> oldTemplate = new List<vlocity_cmt__ContractVersion__c>();
             List<attachment> oldAttachmentList = new List<attachment>();
         try{
            if(isUpdate == true && contractID != null){
                versionId = vlocity_cmt.ContractServiceResource.createNewContractVersionDocument(contractID);
                System.debug('New Version ID:' + versionId);
            }else {
                // Attach a Template to the esixting Version and then attach a Doc.
                for (vlocity_cmt__ContractVersion__c conVersions: [Select ID,vlocity_cmt__ContractId__r.Contract_Type__c ,vlocity_cmt__ContractId__r.Agreement_Type__c,          vlocity_cmt__DocumentTemplateId__c,
                            (select id from vlocity_cmt__Contract_Sections__r) , (select id from attachments)
                            from vlocity_cmt__ContractVersion__c 
                            where vlocity_cmt__ContractId__c = :contractID 
                              and vlocity_cmt__Status__c = 'Active' ]){
                                        oldList.addAll(conVersions.vlocity_cmt__Contract_Sections__r);
                                        oldTemplate.add(conVersions); 
                                        oldAttachmentList.addAll(conVersions.attachments);
                } 

                if(oldList.size() > 0 && !oldList.isEmpty() ){
                    delete oldList;
                }
                 if(oldAttachmentList.size() > 0 && !oldAttachmentList.isEmpty() ){
                    delete oldAttachmentList;
                }

                if( oldTemplate.size() >0 &&  !oldTemplate.isEmpty()){
                        oldTemplate[0].vlocity_cmt__DocumentTemplateId__c=null;
                        
                        update oldTemplate[0];
                        activeContVersion = oldTemplate[0];
                    }    

                /*if(conVersion.size()>0 && !conVersion.isEmpty() ) {
                    activeContVersion = conVersion[0];
                     versionId = conVersion[0].id;
                    System.debug('Old Version ID:' + versionId);*/
                   
                    if(activeContVersion != null){
                        versionId = activeContVersion.id;
                        // get Template name based on Contract Type 
                         if(templatePicker != null && templatePicker != '' ){
                            Map<String, String> objNameMap = new Map<String, String> ();
                            Id documentTemplateId = getTemplateId(templatePicker, 'Contract');
                           // Attach template to the Contract Version
                        vlocity_cmt.ContractDocumentDisplayController.createContractSections(documentTemplateId, activeContVersion.id); 
                        } 
                        else if(templatePicker == ''){
                          system.debug('>>>>>>> NO Template Name______ ' );
                          String message = Label.vlocity_cmt.PDF_NoTemplateId;
                           errorMessage = message;
                           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,Label.vlocity_cmt.PDF_NoTemplateId));
                          throw new NoTemplateIdException(message);

                        }                   
                    }              
                }
             }catch(exception e){
                System.debug(LoggingLevel.ERROR, 'Exception is '+e);
                System.debug(LoggingLevel.ERROR, 'Exception stack trace '+e.getStackTraceString());
                 errorMessage = e.getMessage()  ;
            }
                  
        }
    
    // Get Template name fromt the custom settings
   /* @testvisible
    private String getDocumentTemplateName (String ContractType){
         
         Set_Doc_Template__c docTemplate =  Set_Doc_Template__c.getInstance(ContractType);
    
         if(docTemplate!=null){
            String documentTemplateName = docTemplate.DocumentTemplateName__c; 
            System.debug('Template Name::' + documentTemplateName);
            return documentTemplateName;
        }
        return null;
    }*/
    
   //get active template ID for the contract 
       @testvisible
  private Id getTemplateId(String documentTemplateName, String objTypeName){
        
        List<vlocity_cmt__DocumentTemplate__c> templateIds = [Select Id from vlocity_cmt__DocumentTemplate__c 
                                                              where Name=:documentTemplateName And vlocity_cmt__IsActive__c=true 
                                                              AND vlocity_cmt__ApplicableTypes__c INCLUDES (:objTypeName)];
        
        if(templateIds !=null && templateIds.size()>0){
            return templateIds[0].Id;
        }
        else{
            String message = Label.vlocity_cmt.PDF_NoTemplateId;
            throw new NoTemplateIdException(message);
        }
        return null;    
    }
    
  
 /*String message = Label.vlocity_cmt.PDF_NoTemplateId;
throw new NoTemplateIdException(message);
*/    
public class NoTemplateIdException extends Exception{}    
}