/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class OC_CreateOrderController_Test {
  
  static testMethod void createSolutionOrder_UnitTest() {
        // TO DO: implement unit test
        //Data Setup           
        Id recSerTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        Id recRCIDTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId();        
          
        Account RCIDAcc = new Account(name='RCID Account',recordtypeid=recRCIDTypeId, RCID_Name__c='RCID-99887721');
        Account SerAcc = new Account(name='Service Account',recordtypeid=recSerTypeId,parentid=RCIDacc.id);          
          
        List<Account> AccList= new List<Account>();          
        acclist.add(RCIDAcc);
        acclist.add(SerAcc );
        insert AccList;
        Test.startTest();
        OC_CreateOrderController.createSolutionOrder(RCIDAcc.Id,'New Services',null);
        OC_CreateOrderController.createSolutionOrder(null,null,null);
        Test.stopTest();
        
  }
  
  static testMethod void createCustomerOrder_UnitTest() {
        // TO DO: implement unit test
        //Data Setup           
        Id recSerTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        Id recRCIDTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId();        
          
        Account RCIDAcc = new Account(name='RCID Account',recordtypeid=recRCIDTypeId, RCID_Name__c='RCID-99887721');
        Account SerAcc = new Account(name='Service Account',recordtypeid=recSerTypeId,parentid=RCIDacc.id);          
          
        List<Account> AccList= new List<Account>();          
        acclist.add(RCIDAcc);
        acclist.add(SerAcc );
        insert AccList;
        
        smbcare_address__c address= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N3',FMS_Address_ID__c='987650',account__c=RCIDAcc.id); 
        smbcare_address__c address3= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N3',FMS_Address_ID__c='987652',account__c=RCIDAcc.id,service_account_Id__c=SerAcc.id); 
        address3.Block__c = 'Block 1';
        address3.Building_Number__c = 'Building 1';
        address3.Street_Number__c = '25';
        address3.Street_Name__c = ' York Street';
        
        smbcare_address__c address2= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N5',FMS_Address_ID__c='987651',account__c=RCIDacc.id); 
        List<smbcare_address__c> smbCarelist= new List<smbcare_address__c>(); 
        smbCarelist.add(address);
        smbCarelist.add(address3);
        insert smbCarelist;
        
        ServiceAddressData SData = new ServiceAddressData();
        SData.isManualCapture=true;
        SData.fmsId='987652';
        
        Contact Cont = new Contact (FirstName ='FirstName', LastName='LastName', AccountId=seracc.Id, Email='Abc@abc.com');
        insert Cont;
        
        Contract Ct = new Contract(AccountId = RCIDacc.Id, Status = 'Draft',StartDate = Date.Today(), ContractTerm = 12);
        Insert Ct;
        
        Id standardPriceBookId = Test.getStandardPriceBookId();
        
        SMBCare_Address__c smbCareAddr = new SMBCare_Address__c(Account__c=RCIDacc.Id, Postal_Code__c='M1T3N3', Street_Number__c='Street 1', Street_Name__c='Street Name 1', Province__c='ONT', Suite_Number__c='1001', COID__c='COID 1', Building_Number__c='Building 1', Country__c='CAN', FMS_Address_ID__c='FMSID 121');
        insert smbCareAddr;
        
        Opportunity Opp= new Opportunity(Name='Test Opp',StageName='Discovery',CloseDate=System.today().addDays(5));
        insert Opp; 
        
        Quote Qt= new Quote(Name='Test Quote11',opportunityid=opp.id);
        insert Qt;
        
        Order Od = new Order(Name = 'ORD-000001', Shipping_Address__c='2010 100 AV NW EDMONTON AB T5A3M1', Service_Address_Text__c='2010 100 AV NW EDMONTON AB T5A3M1' , FMS_Address_ID__c='FMSID 121', ShippingStreet='2010 100 AV', ShippingCity='EDMONTON', BAN__c='Ban 21' , PriceBook2Id =standardPriceBookId  , AccountId = RCIDacc.Id, EffectiveDate = Date.Today(), ContractId = Ct.Id, Status = 'Draft', vlocity_cmt__QuoteId__c=Qt.Id);
        Od.Service_Address__c = address3.Id;
        Insert Od;
        
        Order Od1 = new Order(Name = 'ORD-000002', Shipping_Address__c='2010 100 AV NW EDMONTON AB T5A3M1', Service_Address_Text__c='2010 100 AV NW EDMONTON AB T5A3M1' , FMS_Address_ID__c='FMSID 121', ShippingStreet='2010 100 AV', ShippingCity='EDMONTON', BAN__c='Ban 21' , PriceBook2Id =standardPriceBookId  , AccountId = RCIDacc.Id, EffectiveDate = Date.Today(), ContractId = Ct.Id, Status = 'Draft', vlocity_cmt__QuoteId__c=Qt.Id);
        Od1.Service_Address__c = address3.Id;
        Od1.parentId__c = Od.Id;
        Insert Od1; 
        
         List<VlocityCPQUtil.OrderWrapper> CustOrdrWrappList = new List<VlocityCPQUtil.OrderWrapper>();
         VlocityCPQUtil.OrderWrapper OrdrWrapp = new VlocityCPQUtil.OrderWrapper();
   OrdrWrapp.serviceAddressId = address.Id;
         OrdrWrapp.SolutionOrderId = Od.Id;
         OrdrWrapp.orderId = Od.Id;
         OrdrWrapp.moveServiceAddressId = address.Id;
         OrdrWrapp.orderType = 'Move';
         CustOrdrWrappList.add(OrdrWrapp);
         
        Test.startTest();
        OC_CreateOrderController.customerSolutionId = Od.Id;
        OC_CreateOrderController.createCustomerOrders(null, RCIDAcc.Id);
        OC_CreateOrderController.createCustomerOrders(CustOrdrWrappList, RCIDAcc.Id);       
        OC_CreateOrderController.updateOrders(CustOrdrWrappList);
        OC_CreateOrderController.checkServiceLocationsAndCreateSolutionOrder(RCIDAcc.Id);
        
        VlocityCPQUtil vlObj = new VlocityCPQUtil();
        Map<string,Object> input = new Map<string,Object>();
        Map<string,Object> output = new Map<string,Object>();
        Map<string,Object> options = new Map<string,Object>();
        input.put('customerSolutionId',Od.Id);
        vlObj.invokeMethod('getCustomerOrdersLocation',input,output,options);
        OrdrWrapp.SolutionOrderId = null;
        OC_CreateOrderController.createCustomerOrders(CustOrdrWrappList, RCIDAcc.Id);
        OrdrWrapp.orderId = null;
        OC_CreateOrderController.updateOrders(CustOrdrWrappList);

        Test.stopTest();
        
  }
  
  static testMethod void createCustomerOrderAndCustomerOrders_UnitTest() {
        // TO DO: implement unit test
        //Data Setup           
        Id recSerTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        Id recRCIDTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId();        
          
        Account RCIDAcc = new Account(name='RCID Account',recordtypeid=recRCIDTypeId, RCID_Name__c='RCID-99887721');
        Account SerAcc = new Account(name='Service Account',recordtypeid=recSerTypeId,parentid=RCIDacc.id);          
          
        List<Account> AccList= new List<Account>();          
        acclist.add(RCIDAcc);
        acclist.add(SerAcc );
        insert AccList;
        
        smbcare_address__c address= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N3',FMS_Address_ID__c='987650',account__c=RCIDAcc.id); 
        smbcare_address__c address3= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N3',FMS_Address_ID__c='987652',account__c=RCIDAcc.id,service_account_Id__c=SerAcc.id); 
        address3.Block__c = 'Block 1';
        address3.Building_Number__c = 'Building 1';
        address3.Street_Number__c = '25';
        address3.Street_Name__c = ' York Street';
        
        smbcare_address__c address2= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N5',FMS_Address_ID__c='987651',account__c=RCIDacc.id); 
        List<smbcare_address__c> smbCarelist= new List<smbcare_address__c>(); 
        smbCarelist.add(address);
        smbCarelist.add(address3);
        insert smbCarelist;
        
        ServiceAddressData SData = new ServiceAddressData();
        SData.isManualCapture=true;
        SData.fmsId='987652';
        
        Contact Cont = new Contact (FirstName ='FirstName', LastName='LastName', AccountId=seracc.Id, Email='Abc@abc.com');
        insert Cont;
        
        Contract Ct = new Contract(AccountId = RCIDacc.Id, Status = 'Draft',StartDate = Date.Today(), ContractTerm = 12);
        Insert Ct;
        
        Id standardPriceBookId = Test.getStandardPriceBookId();
        
        SMBCare_Address__c smbCareAddr = new SMBCare_Address__c(Account__c=RCIDacc.Id, Postal_Code__c='M1T3N3', Street_Number__c='Street 1', Street_Name__c='Street Name 1', Province__c='ONT', Suite_Number__c='1001', COID__c='COID 1', Building_Number__c='Building 1', Country__c='CAN', FMS_Address_ID__c='FMSID 121');
        insert smbCareAddr;
        
        Opportunity Opp= new Opportunity(Name='Test Opp',StageName='Discovery',CloseDate=System.today().addDays(5));
        insert Opp; 
        
        Quote Qt= new Quote(Name='Test Quote11',opportunityid=opp.id);
        insert Qt;
        
        Order Od = new Order(Name = 'ORD-000001', Shipping_Address__c='2010 100 AV NW EDMONTON AB T5A3M1', Service_Address_Text__c='2010 100 AV NW EDMONTON AB T5A3M1' , FMS_Address_ID__c='FMSID 121', ShippingStreet='2010 100 AV', ShippingCity='EDMONTON', BAN__c='Ban 21' , PriceBook2Id =standardPriceBookId  , AccountId = RCIDacc.Id, EffectiveDate = Date.Today(), ContractId = Ct.Id, Status = 'Draft', vlocity_cmt__QuoteId__c=Qt.Id);
        Od.Service_Address__c = address3.Id;
        Insert Od;
        
        
         List<VlocityCPQUtil.OrderWrapper> CustOrdrWrappList = new List<VlocityCPQUtil.OrderWrapper>();
         VlocityCPQUtil.OrderWrapper OrdrWrapp = new VlocityCPQUtil.OrderWrapper();
     OrdrWrapp.serviceAddressId = address.Id;
         OrdrWrapp.SolutionOrderId = Od.Id;
         CustOrdrWrappList.add(OrdrWrapp);
         
        Test.startTest();
        OC_CreateOrderController.createSolutionOrderAndCustomerOrders(CustOrdrWrappList, RCIDAcc.Id,'Move');
        OC_CreateOrderController.createSolutionOrderAndCustomerOrders(null, null,'Move');
        Test.stopTest();
        
  }
  
  static testMethod void deleteOrders_UnitTest() {
        // TO DO: implement unit test
        //Data Setup           
        Id recSerTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        Id recRCIDTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId();        
          
        Account RCIDAcc = new Account(name='RCID Account',recordtypeid=recRCIDTypeId, RCID_Name__c='RCID-99887721');
        Account SerAcc = new Account(name='Service Account',recordtypeid=recSerTypeId,parentid=RCIDacc.id);          
          
        List<Account> AccList= new List<Account>();          
        acclist.add(RCIDAcc);
        acclist.add(SerAcc );
        insert AccList;
        
        smbcare_address__c address= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N3',FMS_Address_ID__c='987650',account__c=RCIDAcc.id); 
        smbcare_address__c address3= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N3',FMS_Address_ID__c='987652',account__c=RCIDAcc.id,service_account_Id__c=SerAcc.id); 
        address3.Block__c = 'Block 1';
        address3.Building_Number__c = 'Building 1';
        address3.Street_Number__c = '25';
        address3.Street_Name__c = ' York Street';
        
        smbcare_address__c address2= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N5',FMS_Address_ID__c='987651',account__c=RCIDacc.id); 
        List<smbcare_address__c> smbCarelist= new List<smbcare_address__c>(); 
        smbCarelist.add(address);
        smbCarelist.add(address3);
        insert smbCarelist;
        
        ServiceAddressData SData = new ServiceAddressData();
        SData.isManualCapture=true;
        SData.fmsId='987652';
        
        Contact Cont = new Contact (FirstName ='FirstName', LastName='LastName', AccountId=seracc.Id, Email='Abc@abc.com');
        insert Cont;
        
        Contract Ct = new Contract(AccountId = RCIDacc.Id, Status = 'Draft',StartDate = Date.Today(), ContractTerm = 12);
        Insert Ct;
        
        Id standardPriceBookId = Test.getStandardPriceBookId();
        
        SMBCare_Address__c smbCareAddr = new SMBCare_Address__c(Account__c=RCIDacc.Id, Postal_Code__c='M1T3N3', Street_Number__c='Street 1', Street_Name__c='Street Name 1', Province__c='ONT', Suite_Number__c='1001', COID__c='COID 1', Building_Number__c='Building 1', Country__c='CAN', FMS_Address_ID__c='FMSID 121');
        insert smbCareAddr;
        
        Opportunity Opp= new Opportunity(Name='Test Opp',StageName='Discovery',CloseDate=System.today().addDays(5));
        insert Opp; 
        
        Quote Qt= new Quote(Name='Test Quote11',opportunityid=opp.id);
        insert Qt;

        Credit_Assessment__c aCAR = new Credit_Assessment__c(CAR_Status__c='Pending');
        insert aCAR;

        Order Od = new Order(Name = 'ORD-000001', Shipping_Address__c='2010 100 AV NW EDMONTON AB T5A3M1', Service_Address_Text__c='2010 100 AV NW EDMONTON AB T5A3M1' , FMS_Address_ID__c='FMSID 121', ShippingStreet='2010 100 AV', ShippingCity='EDMONTON', BAN__c='Ban 21' , PriceBook2Id =standardPriceBookId  , AccountId = RCIDacc.Id, EffectiveDate = Date.Today(), ContractId = Ct.Id, Status = 'Draft', vlocity_cmt__QuoteId__c=Qt.Id);
        Od.Service_Address__c = address3.Id;
        Od.Credit_Assessment__c=aCAR.id;
        Insert Od;
        
        Order Od1 = new Order(Name = 'ORD-000002', Shipping_Address__c='2011 100 AV NW EDMONTON AB M1T3N#', Service_Address_Text__c='2011 100 AV NW EDMONTON AB T5A3M1' , FMS_Address_ID__c='FMSID 131', ShippingStreet='2011 100 AV', ShippingCity='EDMONTON', BAN__c='Ban 21' , PriceBook2Id =standardPriceBookId  , AccountId = RCIDacc.Id, EffectiveDate = Date.Today(), ContractId = Ct.Id, Status = 'Draft', vlocity_cmt__QuoteId__c=Qt.Id);
        Od1.Service_Address__c = address3.Id;
        Insert Od1;
        
        Product2 Pd = new Product2(Name='Porduct 1',isActive=true);
        insert Pd;

       
        PricebookEntry Pbe = new PricebookEntry(Pricebook2Id=standardPriceBookId, Product2Id=pd.Id, UnitPrice=99, isActive=true);
        insert Pbe;

        OrderItem ordPd = new OrderItem(PriceBookEntryId=Pbe.Id, OrderId=Od.Id, Quantity=1, UnitPrice=99);
        insert ordPd;
        
         List<String> OrderIdSet = new List<String>{Od.Id, Od1.Id};
         test.startTest();
        OC_CreateOrderController.cancelOrders(OrderIdSet); 
        
        Order Od2 = new Order(Name = 'ORD-000003', Shipping_Address__c='2011 100 AV NW EDMONTON AB M1T3N#', Service_Address_Text__c='2011 100 AV NW EDMONTON AB T5A3M1' , FMS_Address_ID__c='FMSID 131', ShippingStreet='2011 100 AV', ShippingCity='EDMONTON', BAN__c='Ban 21' , PriceBook2Id =standardPriceBookId  , AccountId = RCIDacc.Id, EffectiveDate = Date.Today(), ContractId = Ct.Id, Status = 'Draft', vlocity_cmt__QuoteId__c=Qt.Id);
        Od2.Service_Address__c = address3.Id;
        Insert Od2;
        List<String> OrderIdSet2 = new List<String>{Od2.Id};
        system.debug('OrderIdSet2 '+OrderIdSet2 );
        OC_CreateOrderController.deleteOrders(OrderIdSet2);  
        test.stopTest();         
        
  }

}