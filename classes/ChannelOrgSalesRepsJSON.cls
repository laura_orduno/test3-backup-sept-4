public class ChannelOrgSalesRepsJSON {
    public String firstName {get;set;} 
    public String lastName {get;set;} 
    public String salesRepPin {get;set;} 
    public Object salesRepCategoryKeys {get;set;} 
    
    public static void consumeObject(JSONParser parser) {
        Integer depth = 0;
        do {
            JSONToken curr = parser.getCurrentToken();
            if (curr == JSONToken.START_OBJECT || 
                curr == JSONToken.START_ARRAY) {
                    depth++;
                } else if (curr == JSONToken.END_OBJECT ||
                           curr == JSONToken.END_ARRAY) {
                               depth--;
                           }
        } while (depth > 0 && parser.nextToken() != null);
    }
    
    
    
    
    public ChannelOrgSalesRepsJSON(JSONParser parser) {
        while (parser.nextToken() != JSONToken.END_OBJECT) {
            if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                String text = parser.getText();
                if (parser.nextToken() != JSONToken.VALUE_NULL) {
                    if (text == 'firstName') {
                        firstName = parser.getText();
                    } else if (text == 'lastName') {
                        lastName = parser.getText();
                    } else if (text == 'salesRepPin') {
                        salesRepPin = parser.getText();
                    } else if (text == 'salesRepCategoryKeys') {
                        //salesRepCategoryKeys = new Object(parser);
                        salesRepCategoryKeys = parser.getText();
                    } else {
                        System.debug(LoggingLevel.WARN, 'ChannelOrgSalesRepsJSON consuming unrecognized property: '+text);
                        consumeObject(parser);
                    }
                }
            }
        }
    }
    
    
    
    public static ChannelOrgSalesRepsJSON parse(String json) {
        return new ChannelOrgSalesRepsJSON(System.JSON.createParser(json));
    }
}