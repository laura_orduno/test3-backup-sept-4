/**
 * Various utility methods.
 * 
 * @author Max Rudman, Prishan Fernando
 * @since 7/11/2009
 */
public class Util {
	/**
	 * Tests specified new and old records for changes in specified set of fields.
	 * 
	 * @return <code>true</code> if changed; <code>false</code> otherwise.
	 */
	public static Boolean hasChanges(Set<String> fieldNames, SObject oldRecord, SObject newRecord) {
		if (oldRecord == null) {
			return true;
		}
		Boolean changed = false;
		for (String field : fieldNames) {
			changed = (oldRecord.get(field) != newRecord.get(field));
			if (changed) break;
		}
		return changed;
	}
	
	/**
	 * Joins specified array of string values using specified separator. Optionally, 
	 */
	public static String join(List<String> values, String separator, String enclosedBy) {
		// Handle null values
		if ((values == null) || (values.size() == 0)) {
			return '';
		}
		if (separator == null) {
			separator = '';
		}
		if (enclosedBy == null) {
			enclosedBy = '';
		}
		
		String result = '';
		for (String value : values) {
			if (value == null) {
				continue;
			}
			result += (enclosedBy + value + enclosedBy + separator);
		}
		// Chop off trailing separator
		result = result.substring(0,result.length() - separator.length());
		return result;
	}
	
	/**
	 * Joins specified array of string values using specified separator. Optionally, 
	 */
	public static String join(Set<String> values, String separator, String enclosedBy) {
		return join(new List<String>(values),separator,enclosedBy);
	}
	
	/**
	 * Tests if specified value is null or an empty string.
	 */
	public static Boolean isBlank(String value) {
		return (value == null) || (value.trim().length() == 0);
	}
    
    /**
	 * Return List of Pick list values for the given object and the picklist filed name
	 */
    public static List<String> getPickListValues(Sobject object_name, String field_name)
    {
        List<String> pList = new List<String>();
        Schema.sObjectType sobject_type = object_name.getSObjectType(); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
        List<Schema.PicklistEntry> pick_list_values = field_map.get(field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
        for (Schema.PicklistEntry ple : pick_list_values){//for all values in the picklist list
            pList.add(ple.getLabel()); //add the value and label to our final list
        }  
        return pList;
    }

    public static void emailSystemError(database.saveresult[] results,string subject,boolean printSuccess){
        boolean hasErrors=false;
        string message=Util.getErrorEmailHeader()+'\n'+Util.printGovernorLimits()+'\nErrors:';
        for(database.saveresult result:results){
            message+='\n\nRecord ID:'+result.getid()+'\n';
            if(printSuccess&&result.issuccess()){
                message+='Successfully Insert/Update';
            }else{
                if(!result.issuccess()){
                    hasErrors=true;
                    for(database.error error:result.geterrors()){
                        message+='Status Code:'+error.getstatuscode()+':'+error.getmessage()+'\nFields:'+error.getfields();
                    }
                }
            }
        }
        if(hasErrors||printSuccess){
            Util.emailSystemError(subject,message);
        }
    }
    public static string getErrorEmailHeader(){
        string message='Current User:'+userinfo.getname()+'\nUser ID:'+userinfo.getuserid()+'\nProfile ID:'+userinfo.getprofileid()+'\nOrder ID:'+userinfo.getorganizationid()+'\nOrg Name:'+userinfo.getorganizationname()+'\n';
        return message;
    }
    public static string getTriggerContext(){
        string message='';
        if(test.isrunningtest()||trigger.isexecuting){
            if(test.isrunningtest()||trigger.size>0){
                schema.describesobjectresult result=(test.isrunningtest()?account.sobjecttype.getdescribe():trigger.new.get(0).getsobjecttype().getdescribe());
                message+='Trigger '+(test.isrunningtest()||trigger.isbefore?'Before':'After')+' '+(test.isrunningtest()||trigger.isinsert?'Insert':(trigger.isupdate?'Update':'Delete'))+' for Object:'+result.getlabel()+' ('+result.getname()+')\n';
            }
        }
        return message;
    }
    public static void emailSystemError(exception e){
        string message=Util.getErrorEmailHeader();
        message+=Util.getTriggerContext();
        message+='\n'+Util.printGovernorLimits()+'\n'+
            'Error Message:'+e.getmessage()+'\nStack Trace:'+e.getstacktracestring();
        Util.emailSystemError(e.getmessage(),message);
    }
    public static void emailTriggerThreshold(){
        string subject='Governor Limits Reached Threshold: ';
        string message=Util.getErrorEmailHeader();
        message+=Util.getTriggerContext();
        message+='\n'+Util.printGovernorLimits();
        Util.emailSystemError(subject,message);
    }
    public static void emailSystemError(string subject,string message){
        try{
            list<groupmember> devTeamMembers=[select userorgroupid from groupmember where group.developername='crm_dev_team' limit 100];
            set<id> memberIds=new set<id>();
            for(groupmember devTeamMember:devTeamMembers){
                memberIds.add(devTeamMember.userorgroupid);
            }
            list<user> devTeamUsers=[select email from user where id=:memberIds and isactive=true limit 100];
            list<string> emailAddresses=new list<string>();
            for(user devTeamUser:devTeamUsers){
                emailAddresses.add(devTeamUser.email);
            }
            if(!emailAddresses.isempty()){
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setToAddresses(emailAddresses);   
                mail.setSubject(subject);
                mail.setplaintextbody(message);
                if(!test.isrunningtest()){
                    messaging.sendemail(new messaging.singleemailmessage[]{mail});
                }
            }
        }catch(exception e){
            system.debug(e.getmessage());
        }
    }
    public static void logWebservice(webservice_integration_error_log__c log){
        if(log!=null){
            try{
                insert log;
            }catch(exception e){
                Util.emailSystemError(e);
            }
        }
    }
    public static void logWebserviceAsync(webservice_integration_error_log__c log){
        AsyncLogger logger=new AsyncLogger(new list<webservice_integration_error_log__c>{log});
        system.enqueuejob(logger);
    }
    public static void logWebserviceAsync(list<webservice_integration_error_log__c> logs){
        AsyncLogger logger=new AsyncLogger(logs);
        system.enqueuejob(logger);
    }
    public static string printGovernorLimits(){
        string printout='CPU Time:'+limits.getcputime()+'/'+limits.getlimitcputime()+'\n'+
            'SOQL:'+limits.getqueries()+'/'+limits.getlimitqueries()+'\n'+
            'SOSL:'+limits.getsoslqueries()+'/'+limits.getlimitsoslqueries()+'\n'+
            'Aggregate Queries:'+limits.getaggregatequeries()+'/'+limits.getlimitaggregatequeries()+'\n'+
            'Query Rows:'+limits.getqueryRows()+'/'+limits.getlimitqueryrows()+'\n'+
            'DML:'+limits.getdmlstatements()+'/'+limits.getlimitdmlstatements()+'\n'+
            'DML Rows:'+limits.getdmlrows()+'/'+limits.getlimitdmlrows()+'\n'+
            'Callouts:'+limits.getcallouts()+'/'+limits.getlimitcallouts()+'\n'+
            'Emails:'+limits.getemailinvocations()+'/'+limits.getlimitemailinvocations()+'\n'+
            'Futures:'+limits.getfuturecalls()+'/'+limits.getlimitfuturecalls()+'\n'+
            'Heap:'+limits.getheapsize()+'/'+limits.getlimitheapsize()+'\n'+
            'Queueable Jobs:'+limits.getqueueablejobs()+'/'+limits.getlimitqueueablejobs()+'\n';
        return printout;
    }
	
	/**Start New Util Methods added By Prishan on July 07 2015**/
    
    /**
     * Helper Method to get Record Type Id by Name
	 * @param String Object API Name
	 * @param String RecordType Name
	 * @return Id Id of the record type.
	 */
	public static id getRecordTypeIdByName(String object_name, String recordTypeName){
       	return getObjectTypeDescribe(object_name).getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
	}
	/**
     * Helper Method to get the Record Type Id for the Given Record Type Name Defined under given sObject instance.
     * @param String objectInstance :Instance of the object 
     * @param String recordTypeName : Record Type Name.
     * @return Id RecordTypeId 
     * Usage: for(Account a : trigger.New){
    			Id recTypeid = Util.getRecordTypeIdByName(a,'ABC');
               }  
     */
    public static id getRecordTypeIdByName(Sobject objectInstance, String recordTypeName){
        return objectInstance.getSObjectType().getDescribe().getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
    }
    
    /**
     * Helper Method to get the Record Type Name by Id
     * @param String objectName :API Name of the Sobject Example: Account, Product, Test__c
     * @param String recordTypeName : Record Type Name.
     * @return String Name of the record type 
     * 
     */
    public static String getRecordTypeNameById(String object_name, String recordTypeId){
       return getObjectTypeDescribe(object_name).getRecordTypeInfosById().get(recordTypeId).getName(); 
	}
    
    /**
     * Helper Method to get the Record Type Nmae for the Given Record Type Id Defined under given sObject instance.
     * @param String objectInstance :Instance of the object 
     * @param String recordTypeName : Record Type Name.
     * @return Id RecordTypeId 
     * Usage: for(Account a : trigger.New){
    			Id recTypeid = Util.getRecordTypeNameById(a,'ABC');
               }  
     */
    public static String getRecordTypeNameById(Sobject objectInstance, String recordTypeId){
       return objectInstance.getSObjectType().getDescribe().getRecordTypeInfosById().get(recordTypeId).getName(); 
	}
    
    /**
     * Helper method to Buitl Dynamic Query for the given sObject and FieldsSet Name
     * @param String objectName :API Name of the Sobject Example: Account, Product, Test__c
     * @param String fieldSetName : Fieldset name(API name) which avilabe under provied Sobject 
	 * @param List<String> filter : items under where clause join by 'And' Example: {'id=:varId'}
     * @return String SOQL query string
	 * Usage :  
	 *    String q = Util.queryBuilder('Product2', 'Rate_Show', new List<String>{'RecordType.Name= :recname'}); 
     */ 
    public static String queryBuilder(String objectName, String fieldSetName, List<String> filter){
        String query = 'select ';
        List<String> fieldList = getFieldSetFields(objectName, fieldSetName);
        return queryBuilder(objectName,fieldList,filter);
	}
    /**
     * Helper method to Buitl Dynamic Query for the given sObject and List of Fields
     * @param String objectName :API Name of the Sobject Example: Account, Product, Test__c
     * @param List<String> fields : List of Field name to be included in the query 
	 * @param List<String> filter : items under where clause join by 'And' Example: {'id=:varId'}
     * @return String SOQL query string
	 * Usage :  
     * String q = Util.queryBuilder('Product2', new List<String>{'id','Name'}, new List<String>{'RecordType.Name= :recname'}); 
     */
    public static String queryBuilder(String objectName, List<String> fields, List<String> filter){
        return queryBuilder(objectName,fields,filter,null);
	}
    
    /**
     * Helper method to Buitl Dynamic Query for the given sObject and List of Fields
     * @param String objectName :API Name of the Sobject Example: Account, Product, Test__c
     * @param List<String> fields : List of Field name to be included in the query 
	 * @param List<String> filter : items under where clause join by 'And' Example: {'id=:varId'}
	 * @param List<String> orbderBy Example: {'Name ASC'}
     * @return String SOQL query string
	 * Usage :  
     * String q = Util.queryBuilder('Product2', new List<String>{'id','Name'}, new List<String>{'RecordType.Name= :recname'},new List<String>{Name}); 
     */
    public static String queryBuilder(String objectName, List<String> fields, List<String> filter,List<string> orderBy){
        String query = 'SELECT ';
        query += String.join(fields,',');
        query +=' FROM ' + objectName;
        if(filter!=null && filter.size()>0){
           query +=' WHERE ' + String.join(filter,' and '); 
    	}
        if(orderBy!=null && orderBy.size()>0){
            query +=' ORDER BY ' + String.join(orderBy,','); 
        }
       return query;
	}
    /**
     * Helper Method to get Fields Defined under given sObject and Field Set Name.
     * @param String objectName :API Name of the Sobject Example: Account, Product, Test__c
     * @param String fieldSetName : Field set name(API name) which avilabe under provied Sobject
     * @return List<String> return list of FieldNames  
     */
    public static List<String> getFieldSetFields(String object_name,String fsetName){
      	List<Schema.FieldSetMember> fieldset  = getFieldSetFieldsObjs(object_name,fsetName);
    	List<String> items = new List<String>();
        for(Schema.FieldSetMember f : fieldset) {
           	items.add(f.getFieldPath());    
        }
        return items;
	}
    /**
     * Helper Method to get Fields Objects Defined under given sObject and Field Set Name.
     * @param String objectName :API Name of the Sobject Example: Account, Product, Test__c
     * @param String fieldSetName : Field set name(API name) which avilabe under provied Sobject
     * @return List<Schema.FieldSetMember> return list of FieldObjects 
     */
    public static List<Schema.FieldSetMember> getFieldSetFieldsObjs(String object_name,String fsetName){
       return getObjectTypeDescribe(object_name).FieldSets.getMap().get(fsetName).getFields();
	}
    
    /**
     * Helper Method to get RecordTypes As Select Options for a given sObject
     * @param String objectName :API Name of the Sobject Example: Account, Product, Test__c
     * @return  List<SelectOption> return list of recodtypes  
     */
    public static List<SelectOption> getRecordTypesAsSelectOptions(String objectName){
        List<SelectOption> rtList = new List<SelectOption>();
        for(Schema.RecordTypeInfo rtypInfo : getObjectTypeDescribe(objectName).getRecordTypeInfosByName().values()){
           if(rtypInfo.getName()=='Master')
              continue;
           rtList.add(new SelectOption(rtypInfo.getRecordTypeId(),rtypInfo.getName()));
        }
        rtList.sort();
        return rtList;
    }
    
    /**
     * Helper Method to get the Index(position) of the sobject on a List compare by sobject Id 
     * @param List<sObject> Object List
     * @param sObject sObj : Sobject to search for.
     * @return Integer retrun the poistion of the list. if not found return -1 
     */
    public static Integer indexOf(List<sObject> items, sObject sObj){
       integer index =0;
        for(SObject item : items){
            if(item.id == sObj.id){
                return index;
            }
          index++;  
       }
       return -1;
    }
    
    public static Schema.DescribeSObjectResult getObjectTypeDescribe(String object_name){
        Type t = Type.forName(object_name);
        return ((Sobject)t.newInstance()).getSObjectType().getDescribe();
    }
     /**
     * Helper Method to get the updatable Field for the sobjectect
     * @param String objectname
     * @return List<String> list of updatable field Names 
     */
    public static List<String> getUpdatableFields(String object_name){
       Map<String, Schema.SObjectField> fieldMap = getObjectTypeDescribe(object_name).fields.getMap();
       List<String> updatableFields = new List<String>(); 
        for(Schema.SObjectField f : fieldMap.values()) {
            String fieldName = f.getDescribe().Name;
            if( f.getDescribe().isUpdateable() && !(fieldName=='OwnerId'))
				updatableFields.add(f.getDescribe().Name);
        }
        return updatableFields;
    }
    
    /**
     * Helper Method to get the Field Describer for a given object and field name
     * @param String objectname
     * @param String FieldName
     * @return List<String> list of updatable field Names 
     */
    public static Schema.DescribeFieldResult getFieldDescribe(String objectname,String fieldName){
       Map<String, Schema.SObjectField> fieldMap = getObjectTypeDescribe(objectname).fields.getMap();
       return fieldMap.get(fieldName).getDescribe();
    }
    
    /**
     * Reference design copied from internet.  This is without handling newline within a single
     * field.
     */
    public static List<List<String>> parseCSV(String contents,Boolean skipHeaders) {
        List<List<String>> allFields = new List<List<String>>();
        
        try{            
            // replace instances where a double quote begins a field containing a comma
            // in this case you get a double quote followed by a doubled double quote
            // do this for beginning and end of a field
System.debug('Util.parseCSV: A: ' + contents);
            contents = contents.replaceAll(',"""',',"DBLQT').replaceall('""",','DBLQT",');
            // now replace all remaining double quotes - we do this so that we can reconstruct
            // fields with commas inside assuming they begin and end with a double quote
System.debug('Util.parseCSV: B');
            contents = contents.replaceAll('""','DBLQT');
            // we are not attempting to handle fields with a newline inside of them
            // so, split on newline to get the spreadsheet rows
System.debug('Util.parseCSV: C');
            List<String> lines = new List<String>();
            lines = contents.split('\r\n');
System.debug('Util.parseCSV: B');
            Integer num = 0;
            for(String line:lines) {
                System.debug('Util.parseCSV: line: ' + line);
                // check for blank CSV lines (only commas)
                if (line.replaceAll(',','').trim().length() == 0) break;
                
                List<String> fields = line.split(',');
                List<String> cleanFields = new List<String>();
                String compositeField;
                Boolean makeCompositeField = false;
                for(String field:fields) {
                    if (field.startsWith('"') && field.endsWith('"')) {
                        cleanFields.add(field.replaceAll('DBLQT','"'));
                    } else if (field.startsWith('"')) {
                        makeCompositeField = true;
                        compositeField = field;
                    } else if (field.endsWith('"')) {
                        compositeField += ',' + field;
                        cleanFields.add(compositeField.replaceAll('DBLQT','"'));
                        makeCompositeField = false;
                    } else if (makeCompositeField) {
                        compositeField +=  ',' + field;
                    } else {
                        cleanFields.add(field.replaceAll('DBLQT','"'));
                    }
                }
                
                allFields.add(cleanFields);
            }
            if (skipHeaders) allFields.remove(0);
        }
        catch(StringException sEx){
            throw sEx;                        
        }
        catch(Exception ex){
        	throw ex;    
        }

        return allFields;		
    }
}