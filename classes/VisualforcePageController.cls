global with sharing abstract class VisualforcePageController {
    public String getPreferredLanguage(){
        String lang = ApexPages.currentPage().getParameters().get('lang');
        if(lang == null){
            lang = 'en_US';
        }
		return lang;        
    }
}