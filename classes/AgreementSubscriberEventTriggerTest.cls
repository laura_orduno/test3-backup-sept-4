@IsTest
public class AgreementSubscriberEventTriggerTest {
    
    @isTest
    public static void testSubsriberEventTrigger(){
        
        Test.startTest();
        Account rcid1 = new Account(Name='RCID ACC');
        Account rcid2 = new Account(Name='RCID ACC');
        insert rcid1 ;
        insert rcid2 ;
        Account ban =  new Account(Name='BAN ACC');
        
        ban.ParentId = rcid1.id;
        ban.CAN__c = '123456';
        
        insert ban;
        
        Apttus__APTS_Agreement__c ag = new Apttus__APTS_Agreement__c(Name='TestAGR', Apttus__Status__c ='Activated' );
        ag.Apttus__Account__c = rcid1.id;
        insert ag;
        
        Subsidiary_Account__c subAcc = new Subsidiary_Account__c(Account__c = rcid2.id , Agreement__c=ag.id);
            
        Id ChurnRecTypeIdFixed = Util.getRecordTypeIdByName('Agreement_Churn_Allotment__c', 'Fixed Amount For Contract Term');
        Id ChurnRecTypeIdPeriod = Util.getRecordTypeIdByName('Agreement_Churn_Allotment__c', 'Specified Amount For a Specific Period');
        
      	Agreement_Churn_Allotment__c churnRec1 = new Agreement_Churn_Allotment__c(Allowed_Churn_Units__c = 10 , Agreement__c = ag.id, RecordTypeId= ChurnRecTypeIdFixed) ;
        Agreement_Churn_Allotment__c churnRec2 = new Agreement_Churn_Allotment__c(Allowed_Churn_Units__c = 10 , Agreement__c = ag.id,
                     Start_Date__c = Date.newInstance(2016, 1, 1),End_Date__c =   Date.newInstance(2016, 5, 30), RecordTypeId= ChurnRecTypeIdPeriod );
        
        
      	Id hardwareRecTypeIdFixed = Util.getRecordTypeIdByName('Agreement_Hardware_Allotment__c', 'Fixed Amount For Contract Term');
        Id hardwareRecTypeIdPeriod = Util.getRecordTypeIdByName('Agreement_Hardware_Allotment__c', 'Specified Amount For a Specific Period');
        Agreement_Hardware_Allotment__c hardwareRec1 = new Agreement_Hardware_Allotment__c(Allowed_Hardware_Units__c = 30 , Agreement__c = ag.id, RecordTypeId = hardwareRecTypeIdFixed);
        Agreement_Hardware_Allotment__c hardwareRec2 = new Agreement_Hardware_Allotment__c(Allowed_Hardware_Units__c = 30 , Agreement__c = ag.id,
                     Start_Date__c = Date.newInstance(2016, 1, 1),End_Date__c =   Date.newInstance(2016, 5, 30),RecordTypeId= hardwareRecTypeIdPeriod);
        List<SObject> toInsert = new List<SObject>();
        toInsert.add(churnRec1);toInsert.add(churnRec2);
        toInsert.add(hardwareRec1);toInsert.add(hardwareRec2);
        toInsert.add(subAcc);
        insert toInsert; 
        
        toInsert.clear();
        
        String [] eventStatuses = new String[] {'A','C','C','R','S'};
        for(String st :eventStatuses){
            toInsert.add(new Agreement_Subscriber_Event__c(BAN__c='123456', Subscriber_Status__c=st, Effective_Date__c= Date.today()));   
        } 
        insert toInsert;
    	Test.stopTest();
    
    }

    
    
    
}