@isTest
public class OrdrRateGroupInfoWsCalloutTest {
 
    @isTest
    private static void testPing() {
        
        OrdrTestDataFactory.createOrderingWSCustomSettings('RateGroupInfoEndpoint', 'https://xmlgwy-pt1.telus.com:9030/pt01/CMO/BillingAccountMgmt/RateGroupInfoService_v1_0_1_vs1');
            
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsAsfMockImpl());
        // Call the method that invokes a callout
        String version = OrdrRateGroupInfoWsCallout.ping();
        // Verify that a fake result is returned
        System.assertEquals(version, '1.0');
        Test.stopTest();
    }

    @isTest
    private static void testPing_Exception() {
        
        OrdrTestDataFactory.createOrderingWSCustomSettings('RateGroupInfoEndpoint', 'https://xmlgwy-pt1.telus.com:9030/pt01/CMO/BillingAccountMgmt/RateGroupInfoService_v1_0_1_vs1');
            
        Test.startTest();
        try {
            Test.setMock(WebServiceMock.class, new OrdrWsAsfMockImpl(false));
            // Call the method that invokes a callout
            String version = OrdrRateGroupInfoWsCallout.ping();
            System.assert(false, 'Exception expected');
        } catch(Exception e) {
            System.debug(e.getTypeName() + ': ' + e.getMessage());
        }
        Test.stopTest();

    }
    
    @isTest
    private static void testGetRateGroupAndForborneInformationByNpaNxx() {

        OrdrTestDataFactory.createOrderingWSCustomSettings('RateGroupInfoEndpoint', 'https://xmlgwy-pt1.telus.com:9030/pt01/CMO/BillingAccountMgmt/RateGroupInfoService_v1_0_1_vs1');
            
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsAsfMockImpl());
        // Call the method that invokes a callout
        RateGroupInfo_ResourceRes.RateGroupAndForborneAttribute attribute = OrdrRateGroupInfoWsCallout.getRateGroupAndForborneInformationByNpaNxx('604904');
        // Verify that a fake result is returned
        System.assertEquals(attribute.rateGroupAttribute.rateGroupHistoryList.rateGroupList[0].rateBand, 'B');
        Test.stopTest();
    }    
    
    @isTest
    private static void testGetRateGroupAndForborneInformationByNpaNxx_Exception() {

        OrdrTestDataFactory.createOrderingWSCustomSettings('RateGroupInfoEndpoint', 'https://xmlgwy-pt1.telus.com:9030/pt01/CMO/BillingAccountMgmt/RateGroupInfoService_v1_0_1_vs1');
        
        Test.startTest();
        try {
            Test.setMock(WebServiceMock.class, new OrdrWsAsfMockImpl(false));
            // Call the method that invokes a callout
            RateGroupInfo_ResourceRes.RateGroupAndForborneAttribute attribute = OrdrRateGroupInfoWsCallout.getRateGroupAndForborneInformationByNpaNxx('604904');
            System.assert(false, 'Exception expected');
        } catch(Exception e) {
            System.debug(e.getTypeName() + ': ' + e.getMessage());
        }
        Test.stopTest();

    }     
    
    @isTest
    private static void testGetRateGroupInfoByNpaNxx() {
        
        OrdrTestDataFactory.createOrderingWSCustomSettings('RateGroupInfoEndpoint', 'https://xmlgwy-pt1.telus.com:9030/pt01/CMO/BillingAccountMgmt/RateGroupInfoService_v1_0_1_vs1');
            
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsAsfMockImpl());
        // Call the method that invokes a callout
        RateGroupInfo_ResourceRes.RateGroupAttribute attribute = OrdrRateGroupInfoWsCallout.getRateGroupInfoByNpaNxx('604904');
        // Verify that a fake result is returned
        System.assertEquals(attribute.rateGroupHistoryList.rateGroupList[0].rateBand, 'B');
        Test.stopTest();
    }    

    @isTest
    private static void testGetRateGroupInfoByNpaNxx_Exception() {
        
        OrdrTestDataFactory.createOrderingWSCustomSettings('RateGroupInfoEndpoint', 'https://xmlgwy-pt1.telus.com:9030/pt01/CMO/BillingAccountMgmt/RateGroupInfoService_v1_0_1_vs1');
            
        Test.startTest();
        try {
            Test.setMock(WebServiceMock.class, new OrdrWsAsfMockImpl(false));
            // Call the method that invokes a callout
            RateGroupInfo_ResourceRes.RateGroupAttribute attribute = OrdrRateGroupInfoWsCallout.getRateGroupInfoByNpaNxx('604904');
            System.assert(false, 'Exception expected');
        } catch(Exception e) {
            System.debug(e.getTypeName() + ': ' + e.getMessage());
        }
        Test.stopTest();
    }     
}