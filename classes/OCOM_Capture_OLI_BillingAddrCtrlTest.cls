@isTest
public class OCOM_Capture_OLI_BillingAddrCtrlTest 
{
    public static Order ordObj;
    public static String orderId;
    public static Account RCIDacc;
    public static Account billingAcc;
    public static orderItem  ordrItem0;
    
    @testSetup
    static void testDataSetup() 
    {
        Id recBillingTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Billing').getRecordTypeId();
        Id recRCIDTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId();
        
        String hashString = 'X' + String.valueOf(Datetime.now().formatGMT('HH:mm:ss.SSS'));
        
        RCIDacc = new Account(name='AccName' ,recordtypeid=recRCIDTypeId, CustProfId__c=hashString, CBUCID_Parent_Id__c=hashString);
        insert RCIDacc;
        //billingAcc = new Account(name='Draft-00000 Billing Account 00196', BAN_CAN__c='102-testBAN00196', recordtypeid=recBillingTypeId,Billing_System__c = '102',Billingstreet = 'Test Street',BillingCity = 'Test city',BillingState = 'Test state',BillingPostalCode = '1m1 1k1',BillingCountry = 'CANADA');
        //insert billingAcc;
       
        //BillingAccountNamePrefix__c tst = new BillingAccountNamePrefix__c(name='Draft-00000');
        //insert tst;
        SMBCare_Address__c a = new SMBCare_Address__c(Account__c=RCIDacc.Id, Service_Account_Id__c = RCIDacc.Id,
                                                      address_type__c= OrdrConstants.ADDRESS_TYPE_STANDARD, Suite_Number__c='10T2', 
                                                      Street_Number__c='3777', Street_Name__c='Kingsway', 
                                                      Street_Type_Code__c='ST', City__c='Burnaby', Province__c='BC', 
                                                      Postal_Code__c='V5H 3Z7', FMS_Address_ID__c='1919282', SRS_Service_Location__c='3243244');
        insert a;
        
        Product2 prod = new Product2(Name = 'Laptop X200', 
        Family = 'Hardware', IncludeQuoteContract__c = true);
        insert prod;
        
        // Get standard price book ID.
        // This is available irrespective of the state of SeeAllData.
        Id pricebookId = Test.getStandardPricebookId();
        
        // 1. Insert a price book entry for the standard price book.
        // Standard price book entries require the standard price book ID we got earlier.
        PricebookEntry standardPrice = new PricebookEntry(
        Pricebook2Id = pricebookId, Product2Id = prod.Id,
        UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        // Create a custom price book
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        // 2. Insert a price book entry with a custom price.
        PricebookEntry customPrice = new PricebookEntry(
        Pricebook2Id = customPB.Id, Product2Id = prod.Id,
        UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        ordObj = new order(Ban__c='draft',Billing_Notes_Remarks__c = 'Test Notes', Reason_Code__c='34aas',General_Notes_Remarks__c='123123',Pricebook2Id=customPB.id, accountId=RCIDacc.id,effectivedate=system.today(),status='Draft');   
        insert ordObj;
        orderId = ordObj.Id;
        
        ordrItem0 = new orderItem();
        ordrItem0.OrderId=ordObj.id;
        ordrItem0.PricebookEntryId=customPrice.id;
        ordrItem0.Quantity=1;
        ordrItem0.UnitPrice=100.00;
        ordrItem0.vlocity_cmt__RecurringTotal__c=0.00;
        ordrItem0.vlocity_cmt__OneTimeTotal__c=100.00;
        ordrItem0.vlocity_cmt__LineNumber__c='0001';
        ordrItem0.vlocity_cmt__ProvisioningStatus__c=null;
        ordrItem0.orderMgmtId__c='123456780';
        ordrItem0.vlocity_cmt__ServiceAccountId__c = RCIDAcc.Id;
        ordrItem0.vlocity_cmt__ProvisioningStatus__c = 'New';
        insert ordrItem0;
        
    }
    //--getOLIWrapperToBillingAddressMap()
    //--search()
    //-runSearch()
    //-performSearch()
    //--SaveBillingAccount()
    //--SaveBillingOnSelect()
    //--UpdateBillingForMACD()
    //
    @isTest
    private static void testGetServiceAddressSummary(){
         testDataSetup();
        //if (ordObj != null) 
        if (String.isNotBlank(orderId))
        {
            String accountId=[select accountId from order where id=:orderId ].accountId;
            OCOM_Capture_OLI_BillingAddrController cntrl=new OCOM_Capture_OLI_BillingAddrController();
            cntrl.getServiceAddressSummary(accountId);
            
        }
    }    
    @isTest
    private static void getOLIWrapperToBillingAddressMapTest() 
    {
        //Order ordObj = [SELECT Id FROM Order WHERE orderMgmtId__c='1234567890' LIMIT 1];
        //String orderId= OrdrTestDataFactory.orderIdWithVoiceProducts();
        testDataSetup();
        //if (ordObj != null) 
        if (String.isNotBlank(orderId))
        {
            Test.startTest();
            OCOM_Capture_OLI_BillingAddrController billingAddrCtrl = new OCOM_Capture_OLI_BillingAddrController();
            
            billingAddrCtrl.OderId = (orderId);
            ordrItem0.vlocity_cmt__ServiceAccountId__c = RCIDAcc.Id;
            update ordrItem0;
            //billingAddrCtrl.OderId = (String.valueOf(ordObj.id));
            billingAddrCtrl.getOLIWrapperToBillingAddressMap();
            boolean isDisable = billingAddrCtrl.disableAddrBtn;
            billingAddrCtrl.OLIWrapperToBillingAddressMap = null;
            isDisable = billingAddrCtrl.disableAddrBtn;
            
            Test.stopTest();
            //system.assert(billingAddrCtrl.isCarRequired, true);
        }
    }
    
    @isTest
    private static void searchTest() 
    {
        //String orderId= OrdrTestDataFactory.orderIdWithVoiceProducts();
        //Order ordObj = [SELECT Id FROM Order WHERE orderMgmtId__c='1234567890' LIMIT 1];
        testDataSetup();
        if (ordObj != null) 
        {
            Test.startTest();
            OCOM_Capture_OLI_BillingAddrController billingAddrCtrl = new OCOM_Capture_OLI_BillingAddrController();
            billingAddrCtrl.OderId = (String.valueOf(ordObj.id));
            billingAddrCtrl.searchString = '';
            billingAddrCtrl.search();
            Test.stopTest();
            //system.assert(billingAddrCtrl.isCarRequired, true);
        }
    }
    
    @isTest
    private static void SaveBillingAccountTest() 
    {
        testDataSetup();
        //String orderId= OrdrTestDataFactory.orderIdWithVoiceProducts();
        //Order ordObj = [SELECT Id,accountId FROM Order WHERE orderMgmtId__c='1234567890' LIMIT 1];
        String recordTypeID = [select id FROM RecordType where SobjectType='Contact' limit 1].id;
        Contact testContact = new Contact(LastName='TestContact1212',Phone = '9876543281' ,
                                          Email = 'TestMail1@telus.com',
                                          HomePhone = '6476486478',
                                          MobilePhone = '6476486478');
        testContact.FirstName='FirstNam121e';
        testContact.AccountId = ordObj.AccountId;
        
        insert testContact;
        
        ordObj.CustomerAuthorizedById = testContact.Id;
        update ordObj;
        OrdrTestDataFactory.createOrderingWSCustomSettings('BillingAccountCreationEndpoint', 'http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/billing/CreateBillingAccount/createBillingAccount');
        List<BillingAccountNamePrefix__c> billingAccountNamePrefixList = BillingAccountNamePrefix__c.getall().values();
        insert billingAccountNamePrefixList;
        if (ordObj != null) 
        {
            List<OrderItem> oiList=[select id,vlocity_cmt__ParentItemId__c,order.accountid from orderitem where orderid=:ordObj.id];
            for(OrderItem oiInt:oiList){
                oiInt.vlocity_cmt__ParentItemId__c=null;
                oiInt.vlocity_cmt__ServiceAccountId__c=oiInt.order.accountid;
            }
            update oiList;
            Test.startTest();
            Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
            OCOM_Capture_OLI_BillingAddrController billingAddrCtrl = new OCOM_Capture_OLI_BillingAddrController();
            billingAddrCtrl.OderId = (String.valueOf(ordObj.id));
            billingAddrCtrl.getOLIWrapperToBillingAddressMap();
            AddressData saData = new AddressData();
            saData.addressLine1 = 'PO Box 226';
            saData.city = 'Behchoko';
            saData.country = 'CAN';
            saData.postalCode = 'X0E 0Y0';
            saData.province = 'NT';
    
            saData.addressLine2 = '';
            saData.addressLine3 = '';

            saData.company = '';
            saData.subBuilding = '';
            saData.buildingNumber = '';
            saData.buildingName = '';
            saData.secondaryStreet = '';
            saData.street = '';
            saData.block = '';
            saData.addressLine4 = '';
            saData.addressLine5 = '';
            saData.provinceName = '';
            saData.provinceCode = '';
            saData.countryIso2 = '';
            saData.countryIso3 = '';
            saData.streetName = 'NT';
            saData.streetType = 'NT';
            saData.isManualCapture = false;
          
            saData.billingSystem ='102';
            billingAddrCtrl.PAData = saData;
            billingAddrCtrl.SaveBillingAccount();
            billingAddrCtrl.PAData.isManualCapture = true;
            billingAddrCtrl.PAData.billingSystem = '119';
            billingAddrCtrl.buildBillingAccountCreationRequest(billingAddrCtrl.PAData, ordObj);
            Test.stopTest();
            //system.assert(billingAddrCtrl.isCarRequired, true);
        }
    }
    
    @isTest
    private static void SaveBillingOnSelectTest() 
    {
        testDataSetup();
        //Account billAcc = [Select id from Account where BillingCity='TestCity' LIMIT 1];
        
       // ordObj.Billing_Address__c = 'Test Address';
      
        
        if (ordObj != null && RCIDacc != null) 
        {
            Test.startTest();
            OCOM_Capture_OLI_BillingAddrController billingAddrCtrl = new OCOM_Capture_OLI_BillingAddrController();
            billingAddrCtrl.OderId = (String.valueOf(ordObj.id));
            set<Id> BillAddrUpdateSet = new set<id>();
            BillAddrUpdateSet.add(ordrItem0.ID);
            
            billingAddrCtrl.selectedAccId = (String.valueOf(RCIDacc.id));
            //billingAddrCtrl.billingNotes = 'Test data';
            billingAddrCtrl.getOLIWrapperToBillingAddressMap();
            billingAddrCtrl.SaveBillingOnSelect();
            Test.stopTest();
            //system.assert(billingAddrCtrl.isCarRequired, true);
        }
    }
    
    @isTest
    private static void UpdateBillingForMACDTest() 
    {
        testDataSetup();
        if (ordObj != null) 
        {
            Test.startTest();
            OCOM_Capture_OLI_BillingAddrController billingAddrCtrl = new OCOM_Capture_OLI_BillingAddrController();
            billingAddrCtrl.OderId = (String.valueOf(ordObj.id));
            billingAddrCtrl.selectedOliBillAcc= '111-1234';
            billingAddrCtrl.billingNotes = 'Test data';
            billingAddrCtrl.getOLIWrapperToBillingAddressMap();
            billingAddrCtrl.UpdateBillingForMACD();
            billingAddrCtrl.UpdateNotes();
            Test.stopTest();
            //system.assert(billingAddrCtrl.isCarRequired, true);
        }
    }
    @isTest
    private static void Exceptionhandling() 
    {
        testDataSetup();
        if (ordObj != null) 
        {
            Test.startTest();
            OCOM_Capture_OLI_BillingAddrController billingAddrCtrl = new OCOM_Capture_OLI_BillingAddrController();
            billingAddrCtrl.OderId = (String.valueOf(ordObj.id));
            billingAddrCtrl.selectedOliBillAcc= null;
            billingAddrCtrl.getOLIWrapperToBillingAddressMap();
            billingAddrCtrl.UpdateBillingForMACD();
            billingAddrCtrl.UpdateNotes();
            Test.stopTest();
            //system.assert(billingAddrCtrl.isCarRequired, true);
        }
    }
}