/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 * 
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=True)
private class smb_QuoteToPdfStatusController_Test {

    static testMethod void smb_QuoteToPdfStatusController() {
        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'New Provide/Upgrade Order';
        testOpp_1.Printed_Total_MRC_Bundling_Bonus__c = '2.13';
        testOpp_1.Printed_Voice_Positive_Service_Charges__c = '2.13';
        testOpp_1.Printed_Voice_Negative_Service_Charges__c = '2.13';
        testOpp_1.Send_Quote_Contact__c = testOpp_1.Contract_Signor__c;
        testOpp_1.Send_Quote_Additional_Email__c = 'example@astadia.com';
        testOpp_1.Bundle_Type__c = 'Business Select';
        insert testOpp_1;
        OpportunityContactRole opprole = new OpportunityContactRole(opportunityID = testOpp_1.Id,contactId = testOpp_1.Contract_Signor__c,isPrimary = true);
        insert opprole;
        system.debug(opprole);
        Test.startTest();
        smb_test_utility.createPQLI('PQL_Test_Data',testOpp_1.Id);
        PageReference pageref = Page.SMB_WO_SelectAvailableDate;
        Test.setCurrentPage(pageref);        
        pageref.getparameters().put('src', 'C');
        pageref.getparameters().put('PTabId', 'TestId');
        ApexPages.StandardController sc_1 = new ApexPages.standardController(testOpp_1);
        
        smb_QuoteToPdfStatusController quoteToPdfStatus = new smb_QuoteToPdfStatusController(sc_1);
        quoteToPdfStatus.GeneratePDF();
        quoteToPdfStatus.ReturnOrder();
        Test.stopTest();
    }
    static testMethod void smb_QuoteToPdfController() {
        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'New Provide/Upgrade Order';
        testOpp_1.Printed_Total_MRC_Bundling_Bonus__c = '2.13';
        testOpp_1.Printed_Voice_Positive_Service_Charges__c = '2.13';
        testOpp_1.Printed_Voice_Negative_Service_Charges__c = '2.13';
        testOpp_1.Send_Quote_Contact__c = testOpp_1.Contract_Signor__c;
        testOpp_1.StageName = 'Quote Ordered';
        insert testOpp_1;
        OpportunityContactRole opprole = new OpportunityContactRole(opportunityID = testOpp_1.Id,contactId = testOpp_1.Contract_Signor__c,isPrimary = true);
        insert opprole;
        system.debug(opprole);
        //smb_test_utility.createPQLI('PQL_Test_Data',testOpp_1.Id);
        
        PageReference pageref = Page.SMB_WO_SelectAvailableDate;
        Test.setCurrentPage(pageref);        
        pageref.getparameters().put('src', 'C');
        pageref.getparameters().put('PTabId', 'TestId');
        Test.startTest();
        pageref.getparameters().put('id', testOpp_1.Id);
        smb_QuoteToPdfController quoteToPDF = new smb_QuoteToPdfController();
        system.debug('---::quoteToPDF::----' + quoteToPDF.errorMsg);
        Test.stopTest();
    }
}