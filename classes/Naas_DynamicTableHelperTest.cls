@isTest
public class Naas_DynamicTableHelperTest {
    private static Id createData(){  
        Id contractReqRecId = Contracts__c.SObjectType.getDescribe().getRecordTypeInfosByName().get('eContract').getRecordTypeId();
        Id contractRecId = Contract.SObjectType.getDescribe().getRecordTypeInfosByName().get('Corporate Wireless').getRecordTypeId();
        Id vlContractRecId = Contract.SObjectType.getDescribe().getRecordTypeInfosByName().get('Vlocity Contracts').getRecordTypeId();
        Id baContractRecId = Contract.SObjectType.getDescribe().getRecordTypeInfosByName().get('Business Anywhere').getRecordTypeId();
        
        
        Id contractLineRecordId= vlocity_cmt__ContractLineItem__c.SObjectType.getDescribe().getRecordTypeInfosByName().get('eContract Line Item').getRecordTypeId();
        Id rcidAccntRecordTypeId=Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('RCID').getRecordTypeId();
        Account acct = new Account(Name='testacct', Phone='9222022768', RecordTypeId=rcidAccntRecordTypeId);
        insert acct;
        Contact testContactObj = new Contact(firstname = 'TestConFirstName', LastName = 'TestConLastname', accountId = acct.Id , 
                                             Phone = '123456' , fax = '1234567', Email = 'test@gmail.com');
        insert testContactObj;
        Opportunity opp = new Opportunity(AccountId=acct.id, Name='testopp', CloseDate=system.today(),Probability=0,StageName='test stage');
        insert opp;  
        system.debug('queries line 19 -- '+Limits.getQueries());
              
        Contracts__c ConReq= new Contracts__c(RecordTypeId=contractReqRecId,Contract_Type__c='New',Opportunity__c=opp.id );
        insert ConReq;  
        
        Contract testContractObj = new Contract(Complex_Fulfillment_Contract__c=ConReq.id,Status='Draft',Contract_Type__c='Amendment',
                                                AccountId=acct.id,ContractTerm=12,StartDate=system.today(),
                                                Customer_Signor__c=testContactObj.id,TELUS_Signor__c=userinfo.getuserid(), Agreement_Type__c = 'Amendment',Effective_Date__c=Datetime.now());
        insert testContractObj;
        system.debug('queries line 22 -- '+Limits.getQueries());
        
        Product2 testProductObj = new Product2(Name = 'Laptop X200', 
                                               Family = 'Hardware' ,vlocity_cmt__TrackAsAgreement__c = TRUE, Sellable__c = true);
        insert testProductObj;
        List<vlocity_cmt__ContractLineItem__c> testContractLineItemList =new List<vlocity_cmt__ContractLineItem__c>();
        vlocity_cmt__ContractLineItem__c cli1 =  new vlocity_cmt__ContractLineItem__c(vlocity_cmt__RecurringTotal__c=9,TELUS_Printed_Product_Name__c = testProductObj.Name,vlocity_cmt__Product2Id__c=testProductObj.id,vlocity_cmt__ContractId__c=testContractObj.id,vlocity_cmt__OneTimeTotal__c=12,name='test0',vlocity_cmt__OneTimeCharge__c=2,vlocity_cmt__Quantity__c=12,vlocity_cmt__LineNumber__c = '0001');
        testContractLineItemList.add(cli1);
        
        vlocity_cmt__ContractLineItem__c cli2 = new vlocity_cmt__ContractLineItem__c(vlocity_cmt__RecurringTotal__c=6,vlocity_cmt__Product2Id__c=testProductObj.id,vlocity_cmt__ContractId__c=testContractObj.id,vlocity_cmt__OneTimeTotal__c=6,name='test1',vlocity_cmt__OneTimeCharge__c=3,vlocity_cmt__Quantity__c=12,TELUS_Printed_Product_Name__c = testProductObj.Name,vlocity_cmt__LineNumber__c = '0001.0001');
        
        testContractLineItemList.add(cli2);
        vlocity_cmt__ContractLineItem__c cli3  = new vlocity_cmt__ContractLineItem__c(vlocity_cmt__RecurringTotal__c=3,vlocity_cmt__Product2Id__c=testProductObj.id,vlocity_cmt__ContractId__c=testContractObj.id,vlocity_cmt__OneTimeTotal__c=5,name='test2',vlocity_cmt__OneTimeCharge__c=4,vlocity_cmt__Quantity__c=12,TELUS_Printed_Product_Name__c = testProductObj.Name,vlocity_cmt__LineNumber__c = '0002');
        testContractLineItemList.add(cli3);
        
        vlocity_cmt__ContractLineItem__c cli4 =  new vlocity_cmt__ContractLineItem__c(vlocity_cmt__RecurringTotal__c=3,vlocity_cmt__Product2Id__c=testProductObj.id,vlocity_cmt__ContractId__c=testContractObj.id,vlocity_cmt__OneTimeTotal__c=5,name='test3',vlocity_cmt__OneTimeCharge__c=4,vlocity_cmt__Quantity__c=12,TELUS_Printed_Product_Name__c = testProductObj.Name,vlocity_cmt__LineNumber__c = '0002.0001');
        
        testContractLineItemList.add(cli4);
        insert  testContractLineItemList;
        system.debug('queries line 47 -- '+Limits.getQueries());
        
        Contract testContractObj1 = new Contract(Complex_Fulfillment_Contract__c=ConReq.id,Status='Draft',Contract_Type__c='Amendment',
                                                 AccountId=acct.id,ContractTerm=12,StartDate=system.today(),
                                                 Customer_Signor__c=testContactObj.id,TELUS_Signor__c=userinfo.getuserid(), Agreement_Type__c = 'Amendment',Effective_Date__c=Datetime.now());
        insert testContractObj1;
        
        system.debug('queries line 54 -- '+Limits.getQueries());
        //testContractObj.status='Draft';
        update testContractObj;

        system.debug('queries line 58 -- '+Limits.getQueries());
        Test.startTest();
        testContractObj.InitialContract__c=String.valueOf(testContractObj1.id).substring(0,7);       
        testContractObj.status='Contract Registered';
        testContractObj.vlocity_cmt__OriginalContractId__c=testContractObj1.id;
        update testContractObj;
        system.debug('queries line 64 -- '+Limits.getQueries());
        testContractObj.recordtypeid=contractRecId;
        testContractObj.InitialContract__c=null;
        testContractObj.Contract_Type__c=null;
        update testContractObj;
        system.debug('queries line 69 -- '+Limits.getQueries());
        
        testContractObj.recordtypeid=vlContractRecId;
        update testContractObj;
        system.debug('queries line 73 -- '+Limits.getQueries());
        
        testContractObj.recordtypeid=baContractRecId;
        update testContractObj;
        system.debug('queries line 77 -- '+Limits.getQueries());
        Test.stopTest();
        return testContractObj.id;
        
    }
    @isTest
    private static void summaryChargesQuery( ){
        Id contractId=createData();
        //Test.startTest();
        Naas_DynamicTableHelper.summaryChargesQuery(contractId);
        //Test.stopTest();
    }
    @isTest
    private static void masterSummaryChargesQuery(){
        Id contractId=createData();
        //Test.startTest();
        Naas_DynamicTableHelper.masterSummaryChargesQuery(contractId);
       // Test.stopTest();
    }
    @isTest
    private static void getSummaryChargesData1(){
        Id contractId=createData();
        //Test.startTest();
        Naas_DynamicTableHelper.getSummaryChargesData(contractId,'Current CLI');
       // Test.stopTest();
    }
    @isTest
    private static void getSummaryChargesData2(){
        Id contractId=createData();
        //Test.startTest();
        Naas_DynamicTableHelper.getSummaryChargesData(contractId,'Master CLI');
        //Test.stopTest();
    }
    @isTest
    private static void getServiceChargesData(){
        Id contractId=createData();
        //Test.startTest();
        Naas_DynamicTableHelper.getServiceChargesData(contractId);
        //Test.stopTest();
    }
    @isTest
    private static void padSpace(){
        Test.startTest();
        Naas_DynamicTableHelper.padSpace(2);
        Test.stopTest();
    }
    @isTest
    private static void buildDocumentSectionContent(){
        Map<String,Object> inputMap=new Map<String,Object>();
        Map<String,Object> outMap=new Map<String,Object>();
        Map<String,Object> options=new Map<String,Object>();
        inputMap.put('contextObjName','Contract');
            Id contractId=createData();
        inputMap.put('contextObjId',contractId);
        List<vlocity_cmt__ContractLineItem__c> contractLiList=[select id from vlocity_cmt__ContractLineItem__c where vlocity_cmt__ContractId__c=:contractId];
        inputMap.put('items',contractLiList);
        //Test.startTest();
        Naas_DynamicTableHelper.buildDocumentSectionContent (inputMap,  outMap, options);               
        //Test.stopTest();
    }
    private static void createHTMLTable(){}
    private static void prepareData(){}
    private static void getGroupByData(){}
    private static void insertDataCells(){}
    private static void addDataCells(){}
    
}