@isTest
public with sharing class activateAgreement1Test {
    private static final string Agmt_Status_Fully_Signed='Fully Signed';
     private static final string Agmt_StatusCategory_In_Signatures='In Signatures';
     static testMethod void testMethod10() {
         //create account
        Account account = new Account(Name = 'test Accout for Comply Custom API 1');
        insert account;
        
        //create contact
        Contact cont = new Contact (LastName='Tiger', Email='Scott@example.com');
        insert cont;
        
        //create agreement
        Apttus__APTS_Agreement__c agreeement = new Apttus__APTS_Agreement__c(
                                                    Name = 'test Agreement for Comply Custom API 1', 
                                                    Apttus__Account__c = account.Id, 
                                                    Apttus__Status_Category__c = 'Request', 
                                                    Apttus__Status__c = 'Request', 
                                                    Apttus__Contract_Start_Date__c = date.today(), 
                                                    Apttus__Contract_End_Date__c = date.today().addDays(365), 
                                                    Apts_Envelope_Id__c = '8fd115ee-43c2-480a-bcd3-008910ec4988', 
                                                    Printed_Agreement_Type__c ='BusinessAnywhere',
                                                    APTS_Email_Address_Customer_Auth_Contact__c = 'abc@apttus.com',
                                                    Apttus__Term_Months__c = 12, 
                                                    APTS_Customer_Signing_Authority__c = 'abc',Contract_Signor__c=cont.id);

        insert agreeement;
        
        dsfs__DocuSign_Status__c dsStatus = new dsfs__DocuSign_Status__c();
        dsStatus.Apttus_DocuSign__Apttus_Agreement_ID__c = agreeement.id;
        dsStatus.Recipient_Signed_on_Paper__c = 'yes';
        //insert dsStatus;
        
        //attachment
        Blob blobBody = Blob.valueOf('Unit Test Attachment Body');
        Attachment attachment = new Attachment(Name = 'test Attachment for Comply Custom API 1.doc', Body = blobBody, ParentId = agreeement.Id);
        insert attachment;
        
        Blob blobBody1 = Blob.valueOf('Unit Test Attachment Body');
        Attachment attachment1 = new Attachment(Name = 'test Attachment for Comply Custom API 2_Signed.doc', Body = blobBody1, ParentId = agreeement.Id);
        //insert attachment1;
        
        System.debug('********************InserttedAgreementid'+agreeement.Id);
        System.debug('********************InserttedAgreementstatus'+agreeement.Apttus__Status__c);
        List<Apttus__APTS_Agreement__c> AgreementUpdate= new List<Apttus__APTS_Agreement__c>();
        AgreementUpdate = [SELECT Id, Apttus__Status__c, Apttus__Status_Category__c FROM Apttus__APTS_Agreement__c WHERE Id =:agreeement.Id];
        AgreementUpdate[0].Apttus__Status_Category__c = Agmt_StatusCategory_In_Signatures;
        AgreementUpdate[0].Apttus__Status__c = 'Fully Signed'; 
        System.debug('********************UpdatedAgreementid'+AgreementUpdate[0].Id);
        ApttusTriggerHelper.setAlreadyModified1();
        update AgreementUpdate;
        System.debug('********************UpdatedAgreementstatus'+AgreementUpdate[0].Apttus__Status__c);
        //test.stopTest();        
     }
}