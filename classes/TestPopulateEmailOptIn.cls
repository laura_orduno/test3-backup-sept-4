@isTest
private class TestPopulateEmailOptIn{
    static testmethod void testPopulateEmailOptIn(){   
       
        //Created a Contact 
        Contact con = new Contact(
        LastName='Test Contact',
        Email='test@technologies.com'
        );
        insert con;
        
        //Created A new Lead
        Lead l = new Lead(
        Company='Test',
        LastName='Test Lead',
        Email_Opt_In__c=true,
        Email='test@technologies.com'
        );
        insert l;
        
        
        Contact updatedCon=[select Email_Opt_In__c from Contact where id=:con.id];       
        System.assertEquals(l.Email_Opt_In__c,updatedCon.Email_Opt_In__c);
        
        Lead leadForUpdate=[select Email_Opt_In__c from Lead where id=:l.id];  
        leadForUpdate.Email_Opt_In__c=false;
        update leadForUpdate;
        
        Contact updatedWithFalseCon=[select Email_Opt_In__c from Contact where id=:con.id];       
        System.assertEquals(leadForUpdate.Email_Opt_In__c,updatedWithFalseCon.Email_Opt_In__c);

    }
    
}