/**
 * trac_UserLangMgmt.cls - Customer Central Language Group Management
 * @description Updates group membership based on user language selections in combination with custom settings entries
 *
 * @author Grant Adamson, Traction on Demand
 * @date 2013-07-29
 */

public with sharing class trac_UserLangMgmt {
	private Map<Id, UserRole> roleMap; // map of all Partner UserRole ids to UserRole sobjects
	private Map<String, Id> roleNameToIdMap; // reverse map of UserRole names to ids, from roleMap
	private Map<String, Map<String, Id>> languageGroupMap; // Map of generic group name to Language (EN/FR) to Group Id
	private Map<Id, Set<String>> roleToGroupNamesMap; // Map of UserRole id to Set of Generic Group Names
	private List<GroupMember> membershipsToInsert;
	private Map<Id, Set<Id>> membershipsToDeleteMap;
	
	private List<UserRole> listOfRoles;


	public trac_UserLangMgmt() {
		roleMap = loadRoleMap();
		roleNameToIdMap = buildRoleNameToIdMap(roleMap);
		languageGroupMap = loadLanguageGroupMap();
		roleToGroupNamesMap = loadRoleToGroupNamesMap(roleNameToIdMap);
		membershipsToInsert = new List<GroupMember>();
		membershipsToDeleteMap = new Map<Id, Set<Id>>();
		listOfRoles = [SELECT Id, Name, ParentRoleId FROM UserRole WHERE PortalType = 'Partner'];
	}


	private static Map<Id, UserRole> loadRoleMap() {
		return new Map<Id, UserRole>([
			SELECT Name, ParentRoleId
			FROM UserRole
			WHERE PortalType = 'Partner'
		]);
	}
	
	
	private static Map<String, Id> buildRoleNameToIdMap(Map<Id, UserRole> roleMap) {
		Map<String, Id> roleNameToIdMap = new Map<String, Id>();
		for(Id key : roleMap.keySet()) {
			//here make sure there is no doubles!
			if(!roleNameToIdMap.containsKey(roleMap.get(key).name)) {
				roleNameToIdMap.put(roleMap.get(key).name, key);
				System.debug(roleMap.get(key).name + '      ' + key);
			
			}
			
		}
		
		return roleNameToIdMap;
		
	}
	
	private static Map<String, Map<String, Id>> loadLanguageGroupMap() {
		List<Language_Groups__c> languageGroups = [
			SELECT name, English__c, French__c
			FROM Language_Groups__c
		];
		
		Set<String> groupNames = new Set<String>();
		for(Language_Groups__c lg : languageGroups) {
			groupNames.add(lg.english__c);
			groupNames.add(lg.french__c);
		}
		
		Map<String, Id> groupNameToIdMap = new Map<String, Id>();
		
		List<Group> groupObjects = [
			SELECT name
			FROM Group
			WHERE name IN :groupNames
		];
		
		for(Group grp : groupObjects) {
			groupNameToIdMap.put(grp.name, grp.id);
		}
		
		Map<String, Map<String, Id>> languageGroupMap = new Map<String,Map<String, Id>>();
		for(Language_Groups__c lg : languageGroups) {
			if(!languageGroupMap.containsKey(lg.name)) {
				languageGroupMap.put(lg.name, new Map<String,Id>());
			}
			
			languageGroupMap.get(lg.name).put('en', groupNameToIdMap.get(lg.english__c));
			languageGroupMap.get(lg.name).put('fr', groupNameToIdMap.get(lg.french__c));
		}
		
		return languageGroupMap;
	}
	
	
	private static Map<Id, Set<String>> loadRoleToGroupNamesMap(Map<String, Id> roleNameToIdMap) {
		Map<Id, Set<String>> roleToGroupNamesMap = new Map<Id, Set<String>>();
		
		List<UserRole_Groups__c> urgs = [SELECT UserRole__c, Group__c FROM UserRole_Groups__c];
		for(UserRole_Groups__c urg : urgs) {
			System.debug('>>>>>>>>>>>>>>>> ' + urg.UserRole__c + ' <<<<<<<<<<<<<<<<<<');
			System.debug('<<<<<<<<<<<<<<<< ' + urg.Group__c + ' >>>>>>>>>>>>>>>>>>');
			if(!roleToGroupNamesMap.containsKey(roleNameToIdMap.get(urg.UserRole__c) )) {
				roleToGroupNamesMap.put(roleNameToIdMap.get(urg.UserRole__c), new Set<String>());
				System.debug('PLEASE WORK???? ' + urg.UserRole__c);
				System.debug('USERROLE ' + roleNameToIdMap.get(urg.UserRole__c));
				
			}
			System.debug('GROUP GROUP ' + urg.group__c);
			
			roleToGroupNamesMap.get(roleNameToIdMap.get(urg.UserRole__c)).add(urg.group__c);
						
		}
		return roleToGroupNamesMap;
	}
	
	
	// replace with trigger context vars to integrate
	public void updateUserLangGroups(List<User> newUsers, List<User> oldUsers) {
	//public void updateUserLangGroups() {
		User u;
		Id matchedRoleId;
		
		//List<User> allUsers = [SELECT Id, UserRoleId, LanguageLocaleKey FROM User WHERE UserRoleId IN :listOfRoles];
		
		System.debug('BEFORE FOR LOOP');	
			
		for(Integer i = 0; i < newUsers.size(); i++) {
		//for(Integer i = 0; i < allUsers.size(); i++) {
			System.debug('AFTER FOR LOOP');
			u = newUsers[i];
			//u = allUsers[i];
			if(u.LanguageLocaleKey != oldUsers[i].languageLocaleKey) {
				
				for(Id ur : roleNameToIdMap.values()) {
					System.debug(ur);
				}
				
				matchedRoleId = findMatchingRole(u.UserRoleId);
								
				System.debug('MATCHED ROLL'     + matchedRoleId);
				
				if(matchedRoleId != null) {
					addMemberships(u, matchedRoleId);
					removeMemberships(u, matchedRoleId);
				} 				
			} 
			
		}
		System.debug('AFTER AFTER FOR LOOP');
		if(!membershipsToInsert.isEmpty()) {
			insert membershipsToInsert;
			membershipsToInsert.clear();
		}
		
		if(!membershipsToDeleteMap.isEmpty()) {
			processMembershipsToDelete();
			membershipsToDeleteMap.clear();
		}
	}
	
	
	/*
	public void insertUserLangGroups(List<User> newUsers) { 
		User u;
		Id matchedRoleId;
				
		for(Integer i = 0; i < newUsers.size(); i++) {
			
			u = newUsers[i];
			if(u.LanguageLocaleKey == 'fr') {
				matchedRoleId = findMatchingRole(u.UserRoleId); 
			}
			addMemberships(u, matchedRoleId);
			removeMemberships(u, matchedRoleId);
		}
		
		if(!membershipsToInsert.isEmpty()) {
			insert membershipsToInsert;
			membershipsToInsert.clear();
		}
		
		if(!membershipsToDeleteMap.isEmpty()) {
			processMembershipsToDelete();
			membershipsToDeleteMap.clear();
		}
	}
	*/
		
	private Id findMatchingRole(Id userRoleId) {
		// found a match, return it
		if(roleToGroupNamesMap.containsKey(userRoleId)) {
		
			return userRoleId;
		}
		// reached then end of the role tree, either no parent, or parent is not a Partner role and thus not in the map
		if(!roleMap.containsKey(userRoleId) || roleMap.get(userRoleId).parentRoleId == null) {
			return null;
		}
		
		// keep looking
		return findMatchingRole(roleMap.get(userRoleId).parentRoleId);
	}
	
		
	private void addMemberships(User u, Id matchedRoleId) {
		for(String groupName : roleToGroupNamesMap.get(matchedRoleId)) {
			System.debug('THE GROUP ' + groupName);
				membershipsToInsert.add(
				new GroupMember(
					GroupId = languageGroupMap.get(groupName).get(u.languageLocaleKey.left(2)),
					UserOrGroupId = u.id
				)
			);
			
			
		}
	}
	
	private void removeMemberships(User u, Id matchedRoleId) {
		for(String groupName : roleToGroupNamesMap.get(matchedRoleId)) {
			System.debug('ROLETOGROUPNAMESMAP ' + roleToGroupNamesMap.get(matchedRoleId) + ' GROUP222 ' + groupName);
			if(!membershipsToDeleteMap.containsKey(u.id)) {
				membershipsToDeleteMap.put(u.id, new Set<Id>());
			}
			membershipsToDeleteMap.get(u.id).add(
				languageGroupMap.get(groupName).get(u.languageLocaleKey.left(2) == 'en' ? 'fr' : 'en')
			);
			
		}
	}
	
	
	private void processMembershipsToDelete() {
		List<GroupMember> membershipsToDelete = new List<GroupMember>();
		
		List<GroupMember> members = [
			SELECT GroupId, UserOrGroupId, Group.Name
			FROM GroupMember
			WHERE UserOrGroupId IN :membershipsToDeleteMap.keySet()
			ORDER BY UserOrGroupId
		];
		
		for(GroupMember gm : members) {
			
			System.debug('GROUPMEMBER TO DELETE2 ' + gm.UserOrGroupId + ' GROUPID GROUPID ' + gm.GroupId + ' GROUPNAME ' + gm.Group.Name);
			System.debug('THIS ONE HAS TO BE DELETED DAMMIT ' + membershipsToDeleteMap.get(gm.UserOrGroupId));
			if(membershipsToDeleteMap.get(gm.UserOrGroupId).contains(gm.GroupId)) {
				System.debug('GROUPMEMBER TO DELETE ' + gm);
				membershipsToDelete.add(gm);
			}
		}
		
		if(!membershipsToDelete.isEmpty()) {
			delete membershipsToDelete;
		}
	}
}