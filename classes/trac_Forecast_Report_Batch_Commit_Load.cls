global class trac_Forecast_Report_Batch_Commit_Load implements Database.Batchable<sObject>, Database.Stateful {

    global final String Query;
    global final Set<Id> Reg_Users;
    global final Set<Id> Ent_Users;
    
    global trac_Forecast_Report_Batch_Commit_Load(String sQuery, Set<Id> setReg_Users, Set<Id> setEnt_Users) {
        Query = sQuery; 
        Reg_Users = setReg_Users;
        Ent_Users = setEnt_Users;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(Query);
    } 
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        
        List<Forecast_Report__c> myRs = [SELECT Id, OwnerId, Period__c, User__r.Name, Commit_Load__c, Actual_Load__c 
                                            FROM Forecast_Report__c WHERE Commit_Load__c > 0 OR Amount_TYPE__c IN ('Final Commit Loads', 'Final Commit Loads - Acct Based')];
                                                                                                                            
        List<Opportunity> myOpps = scope;

        for(Forecast_Report__c myR :myRs) {
            
            if(myR.Actual_Load__c == null)
                myR.Actual_Load__c = 0;
                
            for(Opportunity myO :myOpps) {
                                    
                //Opportunity Based
                if(myR.Period__c == myO.Close_Period__c
                    && Reg_Users.contains(myO.OwnerId)) {
                
                    if(myR.OwnerId == myO.OwnerId)
                        myR.Actual_Load__c = myR.Actual_Load__c + myO.Forecast_Report_Mobility__c;
                    else if(myR.User__r.Name == myO.Owner.Manager_1__c)
                        myR.Actual_Load__c = myR.Actual_Load__c + myO.Forecast_Report_Mobility__c;
                    else if(myR.User__r.Name == myO.Owner.Manager_2__c)
                        myR.Actual_Load__c = myR.Actual_Load__c + myO.Forecast_Report_Mobility__c;
                    else if(myR.User__r.Name == myO.Owner.Manager_3__c)
                        myR.Actual_Load__c = myR.Actual_Load__c + myO.Forecast_Report_Mobility__c;
                    else if(myR.User__r.Name == myO.Owner.Manager_4__c)
                        myR.Actual_Load__c = myR.Actual_Load__c + myO.Forecast_Report_Mobility__c;
                }

                //Account Based
                else if(myR.Period__c == myO.Close_Period__c
                    && Ent_Users.contains(myO.OwnerId)) {
                
                    if(myR.OwnerId == myO.Account.OwnerId)
                        myR.Actual_Load__c = myR.Actual_Load__c + myO.Forecast_Report_Mobility__c;
                    else if(myR.User__r.Name == myO.Account.Owner.Manager_1__c)
                        myR.Actual_Load__c = myR.Actual_Load__c + myO.Forecast_Report_Mobility__c;
                    else if(myR.User__r.Name == myO.Account.Owner.Manager_2__c)
                        myR.Actual_Load__c = myR.Actual_Load__c + myO.Forecast_Report_Mobility__c;
                    else if(myR.User__r.Name == myO.Account.Owner.Manager_3__c)
                        myR.Actual_Load__c = myR.Actual_Load__c + myO.Forecast_Report_Mobility__c;
                    else if(myR.User__r.Name == myO.Account.Owner.Manager_4__c)
                        myR.Actual_Load__c = myR.Actual_Load__c + myO.Forecast_Report_Mobility__c;
                }
            }           
        }
        
        update myRs;
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
}