@isTest(seealldata=true)
public with sharing class NCProvisioningDetailsExtTest {
	static testMethod void testCPQ() {
		Opportunitylineitem cpqOrderLineitem=[select opportunityid from Opportunitylineitem where opportunity.type='New Provide/Upgrade Order' and opportunity.stagename='Order Request Completed' order by createddate desc limit 1];
		test.starttest();
		pagereference cpqPage=system.currentPageReference();
		cpqPage.getparameters().put('id',cpqOrderLineitem.opportunityid);
		//test.invokePage(cpqPage);
		NCProvisioningDetailsExt cpqController=new NCProvisioningDetailsExt();
		cpqController.getCPQLineItems();
		test.stoptest();
	}
	static testMethod void testAOO() {
		Opportunitylineitem aooOrderLineitem=[select opportunityid from Opportunitylineitem where opportunity.type='All Other Orders' and opportunity.stagename='Order Request Completed' order by createddate desc limit 1];
		test.starttest();
		pagereference aooPage=system.currentPageReference();
		aooPage.getparameters().put('id',aooOrderLineitem.opportunityid);
		//test.invokePage(cpqPage);
		NCProvisioningDetailsExt aooController=new NCProvisioningDetailsExt();
		aooController.getAOOLineItems();
		test.stoptest();
	}
}