/**
 * S2C_smb_newQuoteFromAccountCtlrExt
 * @description Addtional methods to be executed seperated from the original class.
 * @author Thomas Tran, Traction on Demand
 * @serialDate  06-29-2015
 */
public with sharing class S2C_smb_newQuoteFromAccountCtlrExt {

	/**
	 * launchWizard
	 * @description Launches the wizard with required parameters.
	 * @author Thomas Tran, Traction on Demand
	 * @date 06-29-2015
	 */
	public static PageReference launchWizard(Integer oppAmount, Account account, String contactId, String oppType, String oppStageName, String oppName){
		PageReference refPage = Page.S2C_MoveWizard;

		//Opportunity Values
		refPage.getParameters().put('OPP_AMOUNT', String.valueOf(oppAmount));
		refPage.getParameters().put('OPP_ACCOUNT_ID', account.Id);
		refPage.getParameters().put('OPP_TYPE', oppType);
		refPage.getParameters().put('OPP_STAGE_NAME', oppStageName);
		refPage.getParameters().put('WIZARD_SECTION', 'From');

		if(!String.isEmpty(oppName)){
			refPage.getParameters().put('OPP_NAME', oppName + ' - MOVE');
		} else{
			refPage.getParameters().put('OPP_NAME', account.Name + ' - COMPLEX ORDER - ' + System.now() + ' - MOVE');
		}

		//Contact Values
		refPage.getParameters().put('CONTACT_ID', contactId);

		return refPage;
	}
}