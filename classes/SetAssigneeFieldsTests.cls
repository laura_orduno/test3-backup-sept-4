/**
 * Test suite for CopyUserFieldstoAssignee trigger
 * 
 * @author Max Rudman
 * @since 4/5/2009
 */
 
@isTest
public class SetAssigneeFieldsTests {
    testMethod static void testByUser() {
        User anotherAdmin = [SELECT Id FROM User WHERE Id != :UserInfo.getUserId() AND Profile.Id = '00e30000000fHqU' AND IsActive = true LIMIT 1];
        
        User user = [SELECT Id, Tier__c, User_Vertical__c FROM User WHERE Id = :UserInfo.getUserId()];
        
        ADRA__AllocationRule__c rule = new ADRA__AllocationRule__c(Name='Test');
        insert rule;
        
        ADRA__Assignee__c a1 = new ADRA__Assignee__c();
        a1.ADRA__Rule__c = rule.Id;
        a1.ADRA__User__c = user.Id;
        insert a1;
        
        //System.runAs(anotherAdmin) {
            user.Tier__c = 'Tier B';
            user.User_Vertical__c = 'Automotive;Construction';
            update user;
        //}
        
        a1 = [SELECT Assignee_Tier__c, Assignee_Vertical__c FROM ADRA__Assignee__c WHERE Id = :a1.Id];
        System.assertEquals('Tier B', a1.Assignee_Tier__c);
        System.assertEquals('Automotive,Construction', a1.Assignee_Vertical__c);
    }
    
    testMethod static void testByAssignee() {
        User anotherAdmin = [SELECT Id FROM User WHERE Id != :UserInfo.getUserId() AND Profile.Id = '00e30000000fHqU' AND IsActive = true LIMIT 1];
        
        User user = [SELECT Id, Tier__c, User_Vertical__c FROM User WHERE Id = :UserInfo.getUserId()];
        
        System.runAs(anotherAdmin) {
            user.Tier__c = 'Tier B';
            user.User_Vertical__c = 'Automotive;Construction';
            update user;
        }
        
        ADRA__AllocationRule__c rule = new ADRA__AllocationRule__c(Name='Test');
        insert rule;
        
        ADRA__Assignee__c a1 = new ADRA__Assignee__c();
        a1.ADRA__Rule__c = rule.Id;
        a1.ADRA__User__c = user.Id;
        insert a1;
        
        a1 = [SELECT Assignee_Tier__c, Assignee_Vertical__c FROM ADRA__Assignee__c WHERE Id = :a1.Id];
        System.assertEquals('Tier B', a1.Assignee_Tier__c);
        System.assertEquals('Automotive,Construction', a1.Assignee_Vertical__c);
    }

    testMethod static void testNewAssigneeCount() {
    
        List<User> users = [Select Id from User Where Profile.Name Like '%System Admin TEAM%' and IsActive = true Limit 3];
        ADRA__AllocationRule__c rule = new ADRA__AllocationRule__c(Name='Test', ADRA__Allow_Load_Balancing_Across_Rules__c = true);
        insert rule;
        ADRA__Assignee__c a1 = new ADRA__Assignee__c();
        a1.ADRA__Rule__c = rule.Id;
        a1.ADRA__User__c = users[0].Id;
        insert a1;
    
        ADRA__Assignee__c a2 = new ADRA__Assignee__c();
        a2.ADRA__Rule__c = rule.Id;
        a2.ADRA__User__c = users[1].Id;
        insert a2;
    
        a1.ADRA__Allocate_Count__c = 5;
        a2.ADRA__Allocate_Count__c = 4;
    
        update new List<ADRA__Assignee__c> {a1, a2};
    
        ADRA__Assignee__c a3 = new ADRA__Assignee__c();
        a3.ADRA__Rule__c = rule.Id;
        a3.ADRA__User__c = users[2].Id;
    
        Test.startTest();
    
        insert a3;
    
        Test.stopTest();
    
        a3 = [Select Id, ADRA__Allocate_Count__c from ADRA__Assignee__c where Id = :a3.Id];
        System.assertEquals(4, a3.ADRA__Allocate_Count__c);
    
    }
}