@isTest(SeeAllData=true)
public class TestForShowSearchDetails6{
    public static testMethod void testMethodForCodeCoverage(){       
                 
       List<AtoZ_Assignment__c> atmList=new List<AtoZ_Assignment__c>();
                        atmList=[Select a.Network_Drawing__c, a.Link__c, a.Comment__c, a.SvcMgr_Activity__c,
                a.SvcMgr_ProductSuite__c, a.Id, a.Tel__c, a.Email__c, a.AtoZ_Field__c, a.AtoZ_Contact__r.Id,
                a.AtoZ_Contact__r.Full_Name__c,a.AtoZ_Contact__r.Email__c ,a.AtoZ_Contact__r.Phone__c,
                a.AtoZ_Contact__r.External_Email__c
                        From AtoZ_Assignment__c a  where id='a0G40000002HXAEEA4' limit 1000];          
       
       List<AtoZ_Assignment__c> atmList2=new List<AtoZ_Assignment__c>();
                        atmList2=[Select a.Network_Drawing__c, a.Link__c, a.Comment__c, a.SvcMgr_Activity__c,
                a.SvcMgr_ProductSuite__c, a.Id, a.Tel__c, a.Email__c, a.AtoZ_Field__c, a.AtoZ_Contact__r.Id,
                a.AtoZ_Contact__r.Full_Name__c,a.AtoZ_Contact__r.Email__c ,a.AtoZ_Contact__r.Phone__c,
                a.AtoZ_Contact__r.External_Email__c
                        From AtoZ_Assignment__c a  where atoz_field__c='Billing_EnhData' limit 1];
       for(AtoZ_Assignment__c atest:atmList2){
           atest.AtoZ_Field__c='SvcSpec_VITILCare';
       }
       update atmList2;                 
                        
       List<AtoZ_Assignment__c> atmList3=new List<AtoZ_Assignment__c>();
                        atmList3=[Select a.Network_Drawing__c, a.Link__c, a.Comment__c, a.SvcMgr_Activity__c,
                a.SvcMgr_ProductSuite__c, a.Id, a.Tel__c, a.Email__c, a.AtoZ_Field__c, a.AtoZ_Contact__r.Id,
                a.AtoZ_Contact__r.Full_Name__c,a.AtoZ_Contact__r.Email__c ,a.AtoZ_Contact__r.Phone__c,
                a.AtoZ_Contact__r.External_Email__c
                        From AtoZ_Assignment__c a  where atoz_field__c='SvcSpec_NCSC' limit 1];                 

       List<AtoZ_Assignment__c> atmList4=new List<AtoZ_Assignment__c>();
                        atmList4=[Select a.Network_Drawing__c, a.Link__c, a.Comment__c, a.SvcMgr_Activity__c,
                a.SvcMgr_ProductSuite__c, a.Id, a.Tel__c, a.Email__c, a.AtoZ_Field__c, a.AtoZ_Contact__r.Id,
                a.AtoZ_Contact__r.Full_Name__c,a.AtoZ_Contact__r.Email__c ,a.AtoZ_Contact__r.Phone__c,
                a.AtoZ_Contact__r.External_Email__c
                        From AtoZ_Assignment__c a  where atoz_field__c='SvcSpec_NCSC2' limit 1];      

       List<AtoZ_Assignment__c> atmList5=new List<AtoZ_Assignment__c>();
                        atmList5=[Select a.Network_Drawing__c, a.Link__c, a.Comment__c, a.SvcMgr_Activity__c,
                a.SvcMgr_ProductSuite__c, a.Id, a.Tel__c, a.Email__c, a.AtoZ_Field__c, a.AtoZ_Contact__r.Id,
                a.AtoZ_Contact__r.Full_Name__c,a.AtoZ_Contact__r.Email__c ,a.AtoZ_Contact__r.Phone__c,
                a.AtoZ_Contact__r.External_Email__c
                        From AtoZ_Assignment__c a  where atoz_field__c='CrSvcs_Bankruptcy' limit 1];    
       
       Map<String, AtoZ_Assignment__c> fieldAssignmentMap = new Map<String, AtoZ_Assignment__c>();
       fieldAssignmentMap.put('SvcMgr_Activity__c',atmList.get(0));
       fieldAssignmentMap.put('SvcMgr_ProductSuite__c',atmList.get(0));
       fieldAssignmentMap.put('Network_Drawing__c',atmList.get(0));
       fieldAssignmentMap.put('SvcSpec_VITILCare',atmList2.get(0));
       fieldAssignmentMap.put('SvcSpec_NCSC',atmList3.get(0));
       fieldAssignmentMap.put('SvcSpec_NCSC2',atmList4.get(0));
       fieldAssignmentMap.put('CrSvcs_Bankruptcy',atmList5.get(0));
                 
       ShowDetailForAssignmentProv obj=new ShowDetailForAssignmentProv();          
       
       Map<String, String> assignClientServiceCredit2=obj.assignClientServiceCreditValueNew (fieldAssignmentMap);  
              
    }

}