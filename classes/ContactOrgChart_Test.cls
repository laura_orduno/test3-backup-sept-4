@isTest(SeeAllData=false)
public with sharing class ContactOrgChart_Test {
	
	public static testMethod void test_getAllContacts() {
		Account a = new Account();
		a.Name = 'Telus';
		a.Phone = '1-800-888-9999';
		Insert a;

		Contact c0 = new Contact();
		c0.AccountId = a.Id;
		c0.FirstName = 'abc';
		c0.LastName = 'def';
		c0.ReportsToId = null;

		Contact c1 = new Contact();
		c1.AccountId = a.Id;
		c1.FirstName = 'Masoud';
		c1.LastName = 'Az';
		c1.ReportsToId = c0.Id;
		Insert c1;

		Contact c2 = new Contact();
		c2.AccountId = a.Id;	
		c2.FirstName = 'Dan';
		c2.LastName = 'D';
		Insert c2;

		Contact c3 = new Contact();
		c3.AccountId = a.Id;	
		c3.FirstName = 'Mr';
		c3.LastName = 'A';
		c3.ReportsToId = c2.Id;
		c3.Title = 'Title';
		Insert c3;

		Contact c5 = new Contact();
		c5.AccountId = a.Id;
		c5.FirstName = 'ghi';
		c5.LastName = 'jkl';
		c5.ReportsToId = null;
		Insert c5;


		ESP__c esp = new ESP__c();
		esp.Account__c = a.Id;
		esp.Name = 'TELUS ESP';
		Insert esp;

		ESP_User_Contact_Roles__c e1 = new ESP_User_Contact_Roles__c();
		e1.Contact__C  = c1.Id;
		e1.Background_Comment__c = 'background c';
		e1.ESP__c = esp.Id;
		e1.Primary_Owner_of_Relationship__c = UserInfo.getUserId();
		e1.User__c = UserInfo.getUserId();


		ESP_User_Contact_Roles__c e2 = new ESP_User_Contact_Roles__c();
		e2.Contact__C  = c2.Id;
		e2.Background_Comment__c = 'background c';
		e2.ESP__c = esp.Id;
		e2.Primary_Owner_of_Relationship__c = UserInfo.getUserId();
		e2.User__c = UserInfo.getUserId();


		ESP_User_Contact_Roles__c e3 = new ESP_User_Contact_Roles__c();
		e3.Contact__C  = c3.Id;
		e3.Background_Comment__c = 'background c';
		e3.ESP__c = esp.Id;
		e3.Primary_Owner_of_Relationship__c = UserInfo.getUserId();
		e3.User__c = UserInfo.getUserId();		

		ESP_User_Contact_Roles__c e4 = new ESP_User_Contact_Roles__c();
		e4.Contact__C  = c5.Id;
		e4.Background_Comment__c = 'background c';
		e4.ESP__c = esp.Id;
		e4.Primary_Owner_of_Relationship__c = UserInfo.getUserId();
		e4.User__c = UserInfo.getUserId();

		List<ESP_User_Contact_Roles__c> l = new List<ESP_User_Contact_Roles__c>();
		l.add(e1);
		l.add(e2);
		l.add(e3);
		l.add(e4);
		Insert l;

		ESP_Contact_Attribute__c at = new ESP_Contact_Attribute__c();
		at.ESP_User_Contact_Role__c = e1.Id;
		at.Adaptability_To_Change__c = 'V';
		at.Coverage__c = 'MC';
		Insert at;

		Test.startTest();
		ContactOrgChart.getAllContacts(a.Id,esp.Id);
		Test.stopTest();
	}

	public static testMethod void test_replace(){
		Contact c = new Contact();
		c.FirstName = 'M';
		c.LastName ='A';
		c.Title = 'blah';
		Insert c;
		String inString = '{!title}' + ' is cool';
		Test.startTest();
		ContactOrgChart.replace(inString,'title',c);
		Test.stopTest();
	}

	public static testMethod void test_isNullEmpty(){
		String s1 = null;
		String s2 = 'test';
		Test.startTest();
		ContactOrgChart.IsNullEmpty(s1);
		ContactOrgChart.IsNullEmpty(s2);
		Test.stopTest();
	}

	public static testMethod void test_updateMoreLinkDataContenty(){
		Account a = new Account();
		a.Name = 'Telus';
		a.Phone = '1-800-888-9999';
		Insert a;

		Contact c0 = new Contact();
		c0.AccountId = a.Id;
		c0.FirstName = 'abc';
		c0.LastName = 'def';
		c0.ReportsToId = null;

		Contact c1 = new Contact();
		c1.AccountId = a.Id;
		c1.FirstName = 'Masoud';
		c1.LastName = 'Az';
		c1.ReportsToId = c0.Id;
		Insert c1;

		Contact c2 = new Contact();
		c2.AccountId = a.Id;	
		c2.FirstName = 'Dan';
		c2.LastName = 'D';
		Insert c2;

		Contact c3 = new Contact();
		c3.AccountId = a.Id;	
		c3.FirstName = 'Mr';
		c3.LastName = 'A';
		c3.ReportsToId = c2.Id;
		c3.Title = 'Title';
		Insert c3;

		Contact c5 = new Contact();
		c5.AccountId = a.Id;
		c5.FirstName = 'ghi';
		c5.LastName = 'jkl';
		c5.ReportsToId = null;
		Insert c5;


		ESP__c esp = new ESP__c();
		esp.Account__c = a.Id;
		esp.Name = 'TELUS ESP';
		Insert esp;

		ESP_User_Contact_Roles__c e1 = new ESP_User_Contact_Roles__c();
		e1.Contact__C  = c1.Id;
		e1.Background_Comment__c = 'background c';
		e1.ESP__c = esp.Id;
		e1.Primary_Owner_of_Relationship__c = UserInfo.getUserId();
		e1.User__c = UserInfo.getUserId();


		ESP_User_Contact_Roles__c e2 = new ESP_User_Contact_Roles__c();
		e2.Contact__C  = c2.Id;
		e2.Background_Comment__c = 'background c';
		e2.ESP__c = esp.Id;
		e2.Primary_Owner_of_Relationship__c = UserInfo.getUserId();
		e2.User__c = UserInfo.getUserId();


		ESP_User_Contact_Roles__c e3 = new ESP_User_Contact_Roles__c();
		e3.Contact__C  = c3.Id;
		e3.Background_Comment__c = 'background c';
		e3.ESP__c = esp.Id;
		e3.Primary_Owner_of_Relationship__c = UserInfo.getUserId();
		e3.User__c = UserInfo.getUserId();		

		ESP_User_Contact_Roles__c e4 = new ESP_User_Contact_Roles__c();
		e4.Contact__C  = c5.Id;
		e4.Background_Comment__c = 'background c';
		e4.ESP__c = esp.Id;
		e4.Primary_Owner_of_Relationship__c = UserInfo.getUserId();
		e4.User__c = UserInfo.getUserId();

		List<ESP_User_Contact_Roles__c> l = new List<ESP_User_Contact_Roles__c>();
		l.add(e1);
		l.add(e2);
		l.add(e3);
		l.add(e4);
		Insert l;

		ESP_Contact_Attribute__c at = new ESP_Contact_Attribute__c();
		at.ESP_User_Contact_Role__c = e1.Id;
		at.Adaptability_To_Change__c = 'V';
		at.Coverage__c = 'MC';
		Insert at;

		Test.startTest();
		ContactOrgChart.updateMoreLinkDataContent(esp.Id,c1.Id);
		ContactOrgChart.getRoleTypeMenuItemsRemote(c1,esp.Id,false,at);
		ContactOrgChart.ContactHierarchy ch0 = new ContactOrgChart.ContactHierarchy();
		ContactOrgChart.ContactHierarchy ch1 
				= new ContactOrgChart.ContactHierarchy(e1.Id,c5,at,esp.Id,'','','comments',true,'1');
		ch1.getId();
		ch1.getReportsToId();
		ch1.getHasReports();
		ch1.getContactBody();			
		Test.stopTest();
	}
}