@isTest(SeeAllData = True)
public class OCOM_NewQuoteButtonFromAccInterim_Test{

   public static testMethod void testWithNoContacts() {
         Pagereference pref = Page.smb_NewQuoteButtonFromAccountInterim;
         test.setCurrentPage(pref);
        OCOM_NewQuoteButtonFromAccountInterim tno = new OCOM_NewQuoteButtonFromAccountInterim();
        Account a = new Account(Name = 'New test account');
        insert a;
        ApexPages.currentPage().getParameters().clear();
        ApexPages.currentPage().getParameters().put('aid', a.id);
        tno.onLoad();
        tno.clear();
        tno.showQuoteB();
        
        //tno.opptyCreation();
       
        system.assertEquals(true, tno.showNewContactForm);
        system.assertEquals(0, tno.contactsCount);
        system.assertEquals(null, tno.toggleNewContactForm());
        system.assertEquals(false, tno.showNewContactForm);
    }
       
    public static testMethod void testAddContact() {
        Pagereference pref = Page.smb_NewQuoteButtonFromAccountInterim;
        test.setCurrentPage(pref);
        OCOM_NewQuoteButtonFromAccountInterim tno1 = new OCOM_NewQuoteButtonFromAccountInterim();
        Account a = new Account(Name = 'This is a test account');
        insert a;
        ApexPages.currentPage().getParameters().clear();
        ApexPages.currentPage().getParameters().put('aid', a.id);
                      tno1.onLoad();
        system.assertEquals(true, tno1.showNewContactForm);
        system.assertEquals(0, tno1.contactsCount);
        
        tno1.new_contact.FirstName = 'Roger';
        tno1.new_contact.LastName = 'Miller';
        tno1.new_contact.Title = 'Developer';
        tno1.new_contact.Phone = '4545646789';
        tno1.new_contact.Email = 'roger@in.ibm.com';
        tno1.new_contact.accountid=a.id;
        System.debug('####### tno1.new_contact'+ tno1.new_contact); 
        insert tno1.new_contact; 
        tno1.selectedContactId=tno1.new_contact.id;
        System.debug('#######'+tno1.new_contact.id);
        tno1.onSelectContact();
        tno1.createNewContact();
       
        tno1.onSearch();
        tno1.toggleNewContactForm();
        tno1.toggleNewContactForm();
        
                
        system.assertNotEquals(0, tno1.contactsCount);
        system.assertEquals(tno1.contacts[0].Id, tno1.mostRecentlyCreatedContactId);
         tno1.clear();
    }
           
         
          Id standardPriceBookId = Test.getStandardPriceBookId();
     
            
   public static testMethod void testAddNegative() {
        Pagereference pref = Page.smb_NewQuoteButtonFromAccountInterim;
        test.setCurrentPage(pref);
        OCOM_NewQuoteButtonFromAccountInterim tno1 = new OCOM_NewQuoteButtonFromAccountInterim();
        
        ApexPages.currentPage().getParameters().clear();
       // ApexPages.currentPage().getParameters().put('aid', a.id);
         tno1.onLoad();
              
         tno1.clear();
    }
    
 }