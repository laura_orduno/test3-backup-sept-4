global with sharing class OC_ProductFeasibilityImpl implements vlocity_cmt.VlocityOpenInterface {
    
    public class FeasibilityInfo {
        public String offeringKey; //orderMgmtId
        public String lineItemExternalId; //SFDC OLI Id
        public String locationKey; //locationId
        public String underlyingConnectivityId;
        public String offeringId;
        public String serviceDesignation; //Regular Consumer, Public Wi-fi
        public PricebookEntry pbe;
    }

    public Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {

        if(methodName == 'checkProductFeasibility') {
           if(inputMap.Containskey('orderId')){
                checkProductFeasibility((Id)inputMap.get('orderId'), outMap);
           }
        } else {
            return false;
        }
        return true;
    }
    
    private void checkProductFeasibility(Id orderId, Map<String,Object> outMap) {
        String feasibility = 'true';
        String reason = '';
        List<String> offerKeys = new List<String>();
        Map<String, FeasibilityInfo> feasibilityInfoMap = new Map<String, FeasibilityInfo>(); 

        //Feasibility WS callout
        Order order = null;
        List<Order> orderObjList=[select Service_Address__r.Id, account.CustProfId__c from order where Id = :orderId LIMIT 1];
        if(orderObjList!=null && !orderObjList.isEmpty()){
            order=orderObjList.get(0);
        }
        if(order==null){
            return;
        }
        Id serviceAddressId = order.Service_Address__r.Id;
        Map<String, String> inputMap = OrdrUtilities.constructServiceAddress(serviceAddressId);
        inputMap.put('orderId', orderId);
        inputMap.put(OrdrConstants.CUSTOMER_PROFILE_ID, order.account.CustProfId__c);
        if (inputMap.containsKey(OrdrConstants.ADDR_ISFIFA) && inputMap.get(OrdrConstants.ADDR_ISFIFA) == 'Yes') {
            TpFulfillmentCustomerOrderV3.CustomerOrder customerOrder = OrdrProductFeasibilityWsCallout.performCustomerOrderFeasibility(inputMap);
            
            //parse WS response
            if (customerOrder != null) {
                for(TpCommonBaseV3.CharacteristicValue charVal : customerOrder.CharacteristicValue) {
                    if (charVal.Value != null) {
                        if (charVal.Characteristic.Name == 'feasible') feasibility = charVal.Value[0];
                        if (charVal.Characteristic.Name == 'reason') reason = charVal.Value[0];
                    }
                }
            
                if (customerOrder.ProductOrderItem != null) {
                    for(TpFulfillmentProductOrderV3.ProductOrderItem productOrderItem : customerOrder.ProductOrderItem) {
                        FeasibilityInfo feasibilityInfo = new FeasibilityInfo();
                        for(TpCommonBaseV3.CharacteristicValue charVal : productOrderItem.CharacteristicValue) {
                            if (charVal.Value != null) {
                                if (charVal.Characteristic.Name == 'offeringKey') {
                                    offerKeys.add(charVal.Value[0]);
                                    feasibilityInfo.offeringKey = charVal.Value[0];
                                }
                                if (charVal.Characteristic.Name == 'lineItemExternalId') feasibilityInfo.lineItemExternalId = charVal.Value[0];
                                if (charVal.Characteristic.Name == 'locationKey') feasibilityInfo.locationKey = charVal.Value[0];
                                if (charVal.Characteristic.Name == 'underlyingConnectivityId') feasibilityInfo.underlyingConnectivityId = charVal.Value[0];
                                if (charVal.Characteristic.Name == 'offeringId') feasibilityInfo.offeringId = charVal.Value[0];
                                if (charVal.Characteristic.Name == 'serviceDesignation') feasibilityInfo.serviceDesignation = charVal.Value[0];
                            }
                        }
                        feasibilityInfoMap.put(feasibilityInfo.offeringKey, feasibilityInfo);
                    }
                }
            }
        }
        
        Map<Id, PriceBookEntry> pbeMap = new Map<Id, PriceBookEntry>([SELECT Id, Name, product2.orderMgmtId__c from PricebookEntry where orderMgmtId__c in :offerKeys]);
        List<FeasibilityInfo> feasibilityInfoList = new List<FeasibilityInfo>();
        for (String key : pbeMap.keySet()) {
            PriceBookEntry pbe = pbeMap.get(key);
            FeasibilityInfo feasibilityInfo = feasibilityInfoMap.get(pbe.product2.orderMgmtId__c);
            feasibilityInfo.pbe = pbe;
            feasibilityInfoList.add(feasibilityInfo);
        }
        
        
        outMap.put('feasibility', feasibility.equalsIgnoreCase('true'));
        outMap.put('reason', reason);
        outMap.put('feasibilityInfoList', feasibilityInfoList);
        
    }
    
}