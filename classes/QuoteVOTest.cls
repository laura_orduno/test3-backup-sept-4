@isTest
private class QuoteVOTest {

    private static testMethod void testQuoteVO() {
        /*
    	Account a = new Account(name = 'test');
    	insert a;
    	
    	Web_Account__c wa = new Web_Account__c(Company_Legal_Name__c = 'Company Legal Name');
    	insert wa;
    	
    	Contact c = new Contact(
    		AccountId = a.id,
    		FirstName = 'first',
    		LastName = 'last',
				Phone = '123-456-7890',
				Email = 'contact@example.com',
				Title = 'title',
				Role_c__c = 'testrole'
    	);
    	insert c;
    	
    	Opportunity o = new Opportunity(
    		name = 'test opp',
    		AccountId = a.id,
    		Web_Account__c = wa.id,
    		closedate = Date.today(),
    		stagename = 'test'
    	);
    	insert o;
    	
    	Address__c addr = new Address__c(
    		Special_Location__c = 'special',
				Street__c = 'street',
				City__c = 'city',
				Rate_Band__c = 'F',
				State_Province__c = 'BC',
				Postal_Code__c = 'postalcode'
    	);
    	insert addr;
    	
    	Id PROFILEID = [SELECT id FROM Profile WHERE name='Standard User' LIMIT 1].id;
    	
    	User u = new User(
    		username = Datetime.now().getTime() + '@TRACTIONSM.COM',
	      email = 'test@example.com',
	      title = 'test',
	      firstname = 'first',
	      lastname = 'last',
	      alias = 'test',
	      TimezoneSIDKey = 'America/Los_Angeles',
	      LocaleSIDKey = 'en_US',
	      EmailEncodingKey = 'UTF-8',
	      ProfileId = PROFILEID,
	      LanguageLocaleKey = 'en_US',
    		Team_TELUS_ID__c = 'telusid',
				Sales_Rep_ID__c = 'repid',
				CPMS_NCCS_Dealer_Code__c = 'drcode'
    	);
    	insert u;
    	
    	SBQQ__Quote__c quote = new SBQQ__Quote__c(
				SBQQ__Account__c = a.id,
				//Credit_Check_Requested__c = ,
				Agreement_Url__c = 'www.example.com/agreement', 
				//Agreement_Sent_Date__c = ,
				//Agreement_Expiration_Date__c = ,
				//Account_Name__c = ,
				//SBQQ__BillingStreet__c = ,
				//SBQQ__BillingState__c = ,
				//Account_Phone__c = ,
				//Group__c = ,
				SBQQ__PrimaryContact__c = c.id,
				Install_Location__c = addr.id,
				Province__c = 'BC',
				Rate_Band__c = 'F',
				//Service_Amount__c = ,
				//Device_Amount__c = ,
				//Credit_Amount__c = ,
				//Feature_Amount__c = ,
				//Monthly_Amount__c = ,
				SBQQ__SalesRep__c = u.id,
				//New_Site_Enabled__c = ,
				//Site_URL__c = ,
				//Channel__c = ,
				//Channel_Outlet_ID__c = ,
				//Is_Addon__c = ,
				//Source_Quote__c
				//Source_Quote__r.Name
				//agreement_accepted__c = ,
				Expiry_Date__c = Date.newInstance(2014,01,01),
				//SBQQ__Status__c = 'teststatus',
				//Preferred_Installation_Date__c = ,
				//Preferred_Installation_Time__c = ,
				//Assigned_Account_Order__c = ,
				//Dealer_record_Dealer_code__c = ,
				//Client_In_Store__c = ,
				//Order_Complete__c = ,
				//Special_Instructions__c = ,
				//Unserviceable_Items__c = ,
				//SBQQ__BillingName__c = ,
				SBQQ__Opportunity__c = o.id,
				SBQQ__ShippingStreet__c = 'shipstreet',
				SBQQ__ShippingCity__c = 'shipcity',
				SBQQ__ShippingState__c = 'shipstate',
				SBQQ__ShippingPostalCode__c = 'shipcode',
				//SBQQ__Type__c = ,
				SBQQ__SubscriptionTerm__c = 3,
				SBQQ__CustomerDiscount__c = 5,
				SBQQ__PartnerDiscount__c = 10,
				SBQQ__MarkupRate__c = 4,
				SBQQ__StartDate__c = Date.newInstance(2014,1,1),
				SBQQ__EndDate__c = Date.newInstance(2015,1,1)
    	);
    	insert quote;
    	
    	QuoteVO vo = new QuoteVO(quote);
    	
    	system.assertEquals(null,vo.pid);
    	system.assertEquals(null,vo.lid);
    	
    	system.assertEquals(false, vo.hasLineItems);
    	system.assertEquals(Date.newInstance(2014,01,01), vo.expirationDate);
    	system.assertEquals('01/01/2014', vo.expirationDateFormatted);
    	vo.status = 'teststatus';
    	system.assertEquals('teststatus', vo.status);
    	
    	system.assertEquals('', vo.humboldtNum);
    	
    	system.assertEquals(null, vo.preferredInstallDate);
    	vo.preferredInstallDate = '';
    	system.assertEquals(null, vo.preferredInstallDate);
    	vo.preferredInstallDate = '1/1/2014';
    	system.assertEquals('01/01/2014', vo.preferredInstallDate);
    	
    	system.assertEquals(null, vo.preferredInstallTime);
    	vo.preferredInstallTime = '';
    	system.assertEquals(null, vo.preferredInstallTime);
    	vo.preferredInstallTime = '09:00';
    	system.assertEquals('09:00', vo.preferredInstallTime);
    	
    	vo.assignedAccountOrder = 'assigned';
    	system.assertEquals('assigned', vo.assignedAccountOrder);
    	
    	vo.dealerRecordDealerCode = 'dlcode';
    	system.assertEquals('dlcode', vo.dealerRecordDealerCode);
    	
    	system.assertEquals(false, vo.clientInStore);
    	vo.clientInStore = true;
    	system.assert(vo.clientInStore);
    	
    	vo.orderComplete = true;
    	system.assert(vo.orderComplete);
    	
    	vo.specialInstructions = 'instructions';
    	system.assertEquals('instructions', vo.specialInstructions);
    	
    	vo.unserviceableItems = 'unserviceable';
    	system.assertEquals('unserviceable', vo.unserviceableItems);
    	
    	system.assertEquals('shipstreet',vo.shippingStreet);
    	system.assertEquals('shipcity',vo.shippingCity);
    	system.assertEquals('shipstate',vo.shippingProvince);
    	system.assertEquals('shipcode',vo.shippingPostalCode);
    	
    	SBQQ__QuoteLineGroup__c[] groups = new List<SBQQ__QuoteLineGroup__c>{ new SBQQ__QuoteLineGroup__c()};
    	vo.setGroups(groups);
    	
    	Product2 p = new Product2(name = 'testProduct');
    	insert p;
    	
    	SBQQ__QuoteLine__c[] lines = new List<SBQQ__QuoteLine__c>{ new SBQQ__QuoteLine__c(SBQQ__Quote__c = quote.id, SBQQ__Product__c = p.id)};
    	insert lines;
    	
    	vo.setLines(lines);
    	system.assertEquals(1,vo.lineItems.size());
    	system.assertEquals(1,vo.getLineRecords().size());
    	
    	system.assertEquals(quote.id,vo.getId());
    	
    	vo.renumber();
    	
    	system.assertEquals(false,vo.isCloneable);
    	
    	QuoteVo clone = vo.deepClone();
    	
    	QuoteVO.LineItem byLineResult = vo.getItemByLine(lines[0]);
    	
    	system.assertEquals(null, vo.getItemByLine(null));
    	
    	QuoteVO.LineItem byProductResult = vo.getItemByProductId(p.id);
    	
    	system.assertEquals(null, vo.getItemByProductId(null));
    	
    	system.assertEquals(null, vo.getGroupById(null));
    	
    	system.assert(vo.isFullQuote());
    	
    	vo.refreshDiscountSchedules();
    	vo.refreshProducts();
    	
    	system.assertEquals(false, vo.isGrouped());
    	
    	system.assertEquals(false, vo.isPrimary());
    	
    	system.assertEquals(false, vo.isRenewal());
    	
    	vo.clearGroups();
    	
    	vo.addLineItem(new SBQQ__QuoteLine__c(SBQQ__Quote__c = quote.id, SBQQ__Product__c = p.id));
    	system.assertEquals(2,vo.lineItems.size());
			
			system.assertEquals(quote.id, vo.quoteId);
			system.assertEquals(null, vo.quoteName);
			
			vo.quoteType = 'test';
			system.assertEquals('test', vo.quoteType);
			
			system.assertEquals(a.id, vo.accountId);
			Account newA = new Account(name = 'new account');
			insert newA;
			vo.accountId = newA.id;
			system.assertEquals(newA.id, vo.accountId);
			
			system.assertEquals(3, vo.subscriptionTerm);
			vo.subscriptionTerm = 2;
			system.assertEquals(2, vo.subscriptionTerm);
			
			system.assertEquals(5, vo.additionalDiscountRate);
			vo.additionalDiscountRate = 4;
			system.assertEquals(4, vo.additionalDiscountRate);
			
			system.assertEquals(10, vo.partnerDiscountRate);
			vo.partnerDiscountRate = 9;
			system.assertEquals(9, vo.partnerDiscountRate);
			
			system.assertEquals(4, vo.markupRate);
			vo.markupRate = 3;
			system.assertEquals(3, vo.markupRate);
			
			system.assertEquals(Date.newInstance(2014,1,1), vo.startDate);
			vo.startDate = Date.newInstance(2014,1,2);
			system.assertEquals(Date.newInstance(2014,1,2), vo.startDate);
			
			system.assertEquals(Date.newInstance(2015,1,1), vo.endDate);
			vo.endDate = Date.newInstance(2015,1,2);
			system.assertEquals(Date.newInstance(2015,1,2), vo.endDate);

			system.assertEquals(o.id, vo.opportunityId);
*/
    }
}