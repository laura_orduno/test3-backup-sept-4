@isTest
private class QuoteDetailControllerTests {
    private static Web_Account__c account;
    private static SBQQ__Quote__c quote;
    
    testMethod static void testOnClone() {
        setUp();
        update new SBQQ__Quote__c(Id = quote.Id, SBQQ__Status__c = 'Client Declined');
        
        
        ApexPages.currentPage().getParameters().put('qid', quote.Id);
        QuoteDetailController target = new QuoteDetailController();
        PageReference pref = target.onClone();
        System.assertEquals(null,pref); // should fail to clone without additional info
        
        // add missing info
        AdditionalInformation__c info = new AdditionalInformation__c(quote__c = quote.id);
        insert info;
        
        target = new QuoteDetailController();
        target.getCanClone();
        target.getCanRequestClone();
        target.onRequestClone();
        pref = target.onClone();
        //System.assertNotEquals(null,pref,'Errors:' + ApexPages.getMessages());
    }
    testMethod static void testOnCloneRequest() {
        setUp();
        
        ApexPages.currentPage().getParameters().put('qid', quote.Id);
        QuoteDetailController target = new QuoteDetailController();
        
        // add missing info
        AdditionalInformation__c info = new AdditionalInformation__c(quote__c = quote.id);
        insert info;
        
        target = new QuoteDetailController();
        target.getCanClone();
        target.getCanRequestClone();
        target.onRequestClone();
    }
    
    testMethod static void testOnEmail() {
        setUp();
        
        ApexPages.currentPage().getParameters().put('qid', quote.Id);
        QuoteDetailController target = new QuoteDetailController();
        PageReference pref = target.onEmail();
        System.assertNotEquals(null,pref);
    }
    
    testMethod static void testOnPrint() {
        setUp();
        
        ApexPages.currentPage().getParameters().put('qid', quote.Id);
        QuoteDetailController target = new QuoteDetailController();
        PageReference pref = target.onPrint();
        System.assertNotEquals(null,pref);
    }
    
    testMethod static void testOnSendAgreement() {
        setUp();
        
        ApexPages.currentPage().getParameters().put('qid', quote.Id);
        QuoteDetailController target = new QuoteDetailController();
        PageReference pref = target.onSendAgreement();
        System.assertEquals(null,pref); // should fail without required info
        
        // need additional account and contact details
        fillAcctAndContact();
        
        target = new QuoteDetailController();
        pref = target.onSendAgreement();
        // System.assertNotEquals(null,pref,'Errors:' + ApexPages.getMessages());
    }
    
    testMethod static void testOnCheckCredit() {
        setUp();
        
        ApexPages.currentPage().getParameters().put('qid', quote.Id);
        QuoteDetailController target = new QuoteDetailController();
        target.getDetailPageURL();
        target.getSummaryPageURL();
        PageReference pref = target.onCheckCredit();
        System.assertEquals(null,pref); // should fail if additional info has not yet been entered
        
        // need additional account and contact details
        fillAcctAndContact();
        
        target = new QuoteDetailController();
        pref = target.onCheckCredit();
        // System.assertNotEquals(null,pref,'Errors:' + ApexPages.getMessages());
    }
    
    testMethod static void testOnCancel() {
        setUp();
        ApexPages.currentPage().getParameters().put('retURL', Page.QuoteSummary.getURL());
        ApexPages.currentPage().getParameters().put('qid', quote.Id);
        QuoteDetailController target = new QuoteDetailController();
        target.onCancel();
    }
    
    testMethod static void testOnLoad() {
        setUp();
        ApexPages.currentPage().getParameters().put('retURL', Page.QuoteSummary.getURL());
        ApexPages.currentPage().getParameters().put('qid', quote.Id);
        QuoteDetailController target = new QuoteDetailController();
        target.onLoad();
    }
    
    testMethod static void testQuoteRefresh() {
        setUp();
        ApexPages.currentPage().getParameters().put('retURL', Page.QuoteSummary.getURL());
        ApexPages.currentPage().getParameters().put('qid', quote.Id);
        QuoteDetailController target = new QuoteDetailController();
        System.assertEquals('', target.getQuoteRefresh());
    }
    
    testMethod static void testOnSave() {
        setUp();
        
        Quote_Group__c grp = new Quote_Group__c();
        insert grp;
        
        quote.Group__c = grp.Id;
        update quote;
        
        SBQQ__Quote__c quote2 = new SBQQ__Quote__c(SBQQ__Opportunity__c=quote.SBQQ__Opportunity__c,Group__c=grp.Id);
        quote2.Rate_Band__c = 'F';
        quote2.Province__c = 'BC';
        insert quote2;
        
        ApexPages.currentPage().getParameters().put('qid', quote.Id);
        ApexPages.currentPage().getParameters().put('retURL', '/home/home.jsp');
        QuoteDetailController target = new QuoteDetailController();
        
        target.products[0].getConfigurationFields();
        target.quote.status = 'Test';
        PageReference pref = target.onSave();
        System.assertNotEquals(null,pref, 'Errors:' + ApexPages.getMessages());
        
        target.quote.status = 'Customer Accepted Agreement, Pending Dealer Input';
        pref = target.onSave();
        System.assertNotEquals(null,pref, 'Errors:' + ApexPages.getMessages());
    }
    
    // Traction R5rq3 - GA - 2012-03-13
    testMethod static void testEditableStatus() {
        setUp();
        
        quote.sbqq__status__c = 'badValue';
        update quote;
        
        ApexPages.currentPage().getParameters().put('qid', quote.Id);
        ApexPages.currentPage().getParameters().put('retURL', '/home/home.jsp');
        QuoteDetailController target = new QuoteDetailController();
        
        system.assert(!target.getCanEdit(),'Quote with non-editable status should return false for canEdit');
        
        quote.sbqq__status__c = 'draft';
        update quote;
        
        target = new QuoteDetailController();
        
        system.assert(target.getCanEdit(),'Quote with editable status should return true for canEdit');
        
        quote.Agreement_Accepted__c = true;
        update quote;
        
        target = new QuoteDetailController();
        
        system.assert(!target.getCanEdit(),'Quote with editable status but signed agreement should return false for canEdit');
    }
    
    testMethod static void testCloneableStatus() {
        System.runAs(new User(Id = Userinfo.getUserId())) {
            update new User(Id = userInfo.getuserId(), Channel__c = 'BACC');
        }
        setUp();
        
        quote.sbqq__status__c = 'badValue';
        update quote;
        
        ApexPages.currentPage().getParameters().put('qid', quote.Id);
        ApexPages.currentPage().getParameters().put('retURL', '/home/home.jsp');
        QuoteDetailController target = new QuoteDetailController();
        
        system.assert(!target.getCanClone(),'Quote with non-cloneable status should return false for canClone');
        
        quote.sbqq__status__c = 'Held for Customer';
        update quote;
        
        target = new QuoteDetailController();
        
        system.assert(!target.getCanClone(),'Quote with cloneable status but no agreement should return false for canClone');
        
        quote.Agreement_Accepted__c = true;
        update quote;
        
        target = new QuoteDetailController();
        
        system.assert(target.getCanClone(),'Quote with cloneable status and agreement should return true for canClone');
        
        target.getUserCanIssueCloneRequests();
    }
    
    private static testMethod void testGeneralSettersAndGetters() {
        setUp();
        
        ApexPages.currentPage().getParameters().put('qid', quote.Id);
        QuoteDetailController target = new QuoteDetailController();
        
        target.isCreditCheckDisabled();
        target.isSendAgreementDisabled();
        target.isSendEmailDisabled();
        
        // test alternate constructor
        target = new QuoteDetailController(quote.id);
        
        // this setter does nothing, but has been left in for compatibility with legacy VF pages
        target.setSummaryPageURL('into the void');
    }
    
    private static testMethod void testExceptionHandling() {
        QuoteDetailController target = new QuoteDetailController();
        
        target.isSendAgreementDisabled();
        target.isCreditCheckDisabled();
        target.onSave();
        target.onSendAgreement();
        target.onCheckCredit();
        target.getCanEdit();
        target.getCanEditProducts();
        
        
    }
    
    private static void setUp() {
        account = new Web_Account__c(Name = 'UnitTest');
        insert account;
        
        Address__c installLocation = new Address__c(
            Web_Account__c = account.Id,
            Special_Location__c = 'Test',
            Rate_Band__c = 'F',
            State_Province__c = 'BC'
        );
        insert installLocation;
        
        Quote_Group__c qg = new Quote_Group__c();
        insert qg;
        
        Product2 p = new Product2(Name='Test', SBQQ__ConfigurationFields__c = 'Service_Term__c');
        insert p;
        
        Opportunity opp = new Opportunity(
            Name='Test',
            Web_Account__c = account.Id,
            CloseDate = Date.today(),
            StageName = 'Prospecting'
        );
        insert opp;
        
        quote = new SBQQ__Quote__c(
            SBQQ__Opportunity__c=opp.Id,
            Rate_Band__c = 'F',
            Province__c = 'BC',
            Install_Location__c = installLocation.Id,
            Group__c = qg.id
        );
        insert quote;
        
        SBQQ__QuoteLine__c line = new SBQQ__QuoteLine__c(
            SBQQ__Quote__c = quote.Id,
            SBQQ__Bundle__c = true,
            SBQQ__Product__c = p.Id
        );
        insert line;
        
        PageReference pref = ApexPages.currentPage();
        pref.getParameters().put('ms', '1');
    }
    
    // Traction R5
    private static void fillAcctAndContact() {
        account.Type_of_customer__c = 'Test';
        account.BAN_type__c = 'Test';
        account.Billing_Address__c = '1234 Street';
        account.Billing_City__c = 'Vancouver';
        account.Billing_Province__c = 'AB';
        account.Billing_Postal_Code__c = 'A1A 1A1';
        account.Shipping_Address__c = account.Billing_Address__c;
        account.Shipping_City__c = account.Billing_City__c;
        account.Shipping_Province__c = account.Billing_Province__c;
        account.Shipping_Postal_Code__c = account.Billing_Postal_Code__c;
        update account;
        
                // need a contact in order to have a credit check submitted!
        Contact c = new Contact(Web_Account__c = account.id,
                                FirstName = 'unit',
                                Lastname = 'tester',
                                email = 'not@example.com',
                                phone = '1234567890',
                                title = 'Tester',
                                title_category__c = 'IT Dept',
                                language_preference__c = 'English',
                                Department_Category__c = 'IT',
                                Role_c__c = 'Purchaser',
                                MailingStreet = '1234 Street',
                                MailingCity = 'Vancouver',
                                MailingState = 'AB',
                                MailingPostalCode = 'A1A 1A1');
        insert c;
    }
}