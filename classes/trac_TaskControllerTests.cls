@isTest
private class trac_TaskControllerTests {
	

	@isTest 
	private static ID createTasks() {
		Case c = new Case();
		insert c;
		Task t0 = new Task();
		t0.ActivityDate = Date.today();
		t0.whatId = c.id;
		insert t0;
		Task t1 = new Task();
		t1.ActivityDate = Date.today().addDays(1);
		insert t1;
		Task t2 = new Task();
		t2.ActivityDate = Date.today().addDays(-1);
		insert t2;
		Task t3 = new Task();
		t3.ActivityDate = Date.today().addDays(30);
		insert t3;
		return t0.ID;

	}

	testmethod static void testFetchTasksNoFilter(){
		createTasks();
		trac_TaskController tc = new trac_TaskController();
		//System.assertEquals(tc.tasks.size(), 4);
	}
	testmethod static void testFetchTasksToday(){
		createTasks();
		trac_TaskController tc = new trac_TaskController();
		tc.tasklistmode = '1';
		tc.fetchTasks();
		//System.assertEquals(1, tc.tasks.size());
	}
	testmethod static void testFetchTasksTomorrow(){
		createTasks();
		trac_TaskController tc = new trac_TaskController();
		tc.tasklistmode = '3';
		tc.fetchTasks();
		//System.assertEquals(1, tc.tasks.size());
	}
	testmethod static void testFetchTasksThisMonth(){
		createTasks();
		trac_TaskController tc = new trac_TaskController();
		tc.tasklistmode = '6';
		tc.fetchTasks();

		// If this test runs on the first or last day of the month, there will only be two tasks that fall in this month
		if(Date.today().addDays(1).month() != Date.today().month() || Date.today().addDays(-1).month() != Date.today().month()) {
			//System.assertEquals(2, tc.tasks.size());	 
		}else{
			//System.assertEquals(3, tc.tasks.size());	
		}
	}

	testmethod static void testCloseTask(){
		trac_TaskController tc = new trac_TaskController();
		tc.taskid = createTasks();
		PageReference pg = tc.closeTask();
		//System.assertNotEquals(null, pg);
	}

	testmethod static void testCloseTaskWithoutTaskID(){
		trac_TaskController tc = new trac_TaskController();
		PageReference pg = tc.closeTask();
		//System.assertEquals(null, pg);
	}

	testmethod static void testChangeFilter() {
		createTasks();
		PageReference pageRef = Page.trac_TasksPage;
		Map<String, String> params = pageRef.getParameters();
		params.put('trac_tasklistmode_num','1');
		params.put('trac_tasklistoffset_num', '0');
        Test.setCurrentPage(pageRef);
        trac_TaskController tc = new trac_TaskController();
        tc.changeFilter();
        //System.assertEquals(1, tc.tasks.size());
	}
}