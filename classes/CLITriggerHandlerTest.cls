@isTest
private class CLITriggerHandlerTest {
    private static Contracts__c ConReq;
    private static Account acct;
    private static Contact testContactObj;
        private static List<vlocity_cmt__ContractLineItem__c> createData(){  
            Id contractReqRecId = Contracts__c.SObjectType.getDescribe().getRecordTypeInfosByName().get('eContract').getRecordTypeId();
            Id contractRecId = Contract.SObjectType.getDescribe().getRecordTypeInfosByName().get('Corporate Wireless').getRecordTypeId();
            Id contractLineRecordId= vlocity_cmt__ContractLineItem__c.SObjectType.getDescribe().getRecordTypeInfosByName().get('eContract Line Item').getRecordTypeId();
            Id rcidAccntRecordTypeId=Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('RCID').getRecordTypeId();
             acct = new Account(Name='testacct', Phone='9222022768', RecordTypeId=rcidAccntRecordTypeId);
            insert acct;
             testContactObj = new Contact(firstname = 'TestConFirstName', LastName = 'TestConLastname', accountId = acct.Id , 
                                                 Phone = '123456' , fax = '1234567', Email = 'test@gmail.com');
            insert testContactObj;
            Opportunity opp = new Opportunity(AccountId=acct.id, Name='testopp', CloseDate=system.today(),Probability=0,StageName='test stage');
            insert opp;        
             ConReq= new Contracts__c(RecordTypeId=contractReqRecId,Contract_Type__c='New',Opportunity__c=opp.id );
            insert ConReq;   
            Contract testContractObj = new Contract(Complex_Fulfillment_Contract__c=ConReq.id,Status='Draft',Contract_Type__c='Amendment',
                                                    AccountId=acct.id,ContractTerm=12,StartDate=system.today(),
                                                    Customer_Signor__c=testContactObj.id,TELUS_Signor__c=userinfo.getuserid(), Agreement_Type__c = 'Amendment');
            insert testContractObj;
             vlocity_cmt__Catalog__c cat = new vlocity_cmt__Catalog__c();
        cat.Name = 'eContract';
        insert cat;
            Product2 testProductObj = new Product2(Name = 'Laptop X200', 
                                                   Family = 'Hardware' ,vlocity_cmt__TrackAsAgreement__c = TRUE, Sellable__c = true);
            insert testProductObj;
            vlocity_cmt__CatalogProductRelationship__c  cr=new vlocity_cmt__CatalogProductRelationship__c();
        cr.vlocity_cmt__Product2Id__c=testProductObj.id;
        cr.vlocity_cmt__CatalogId__c=cat.id;
        insert cr;
            List<vlocity_cmt__ContractLineItem__c> testContractLineItemList =new List<vlocity_cmt__ContractLineItem__c>();
            vlocity_cmt__ContractLineItem__c cli1 =  new vlocity_cmt__ContractLineItem__c(vlocity_cmt__RecurringTotal__c=9,TELUS_Printed_Product_Name__c = testProductObj.Name,vlocity_cmt__Product2Id__c=testProductObj.id,vlocity_cmt__ContractId__c=testContractObj.id,vlocity_cmt__OneTimeTotal__c=12,name='test0',vlocity_cmt__OneTimeCharge__c=2,vlocity_cmt__Quantity__c=12,vlocity_cmt__LineNumber__c = '0001');
            testContractLineItemList.add(cli1);
            
            vlocity_cmt__ContractLineItem__c cli2 = new vlocity_cmt__ContractLineItem__c(vlocity_cmt__RecurringTotal__c=6,vlocity_cmt__Product2Id__c=testProductObj.id,vlocity_cmt__ContractId__c=testContractObj.id,vlocity_cmt__OneTimeTotal__c=6,name='test1',vlocity_cmt__OneTimeCharge__c=3,vlocity_cmt__Quantity__c=12,TELUS_Printed_Product_Name__c = testProductObj.Name,vlocity_cmt__LineNumber__c = '0001.0001');
            
            testContractLineItemList.add(cli2);
            vlocity_cmt__ContractLineItem__c cli3  = new vlocity_cmt__ContractLineItem__c(vlocity_cmt__RecurringTotal__c=3,vlocity_cmt__Product2Id__c=testProductObj.id,vlocity_cmt__ContractId__c=testContractObj.id,vlocity_cmt__OneTimeTotal__c=5,name='test2',vlocity_cmt__OneTimeCharge__c=4,vlocity_cmt__Quantity__c=12,TELUS_Printed_Product_Name__c = testProductObj.Name,vlocity_cmt__LineNumber__c = '0002');
            testContractLineItemList.add(cli3);
            
            vlocity_cmt__ContractLineItem__c cli4 =  new vlocity_cmt__ContractLineItem__c(vlocity_cmt__RecurringTotal__c=3,vlocity_cmt__Product2Id__c=testProductObj.id,vlocity_cmt__ContractId__c=testContractObj.id,vlocity_cmt__OneTimeTotal__c=5,name='test3',vlocity_cmt__OneTimeCharge__c=4,vlocity_cmt__Quantity__c=12,TELUS_Printed_Product_Name__c = testProductObj.Name,vlocity_cmt__LineNumber__c = '0002.0001');
            
            testContractLineItemList.add(cli4);
            insert  testContractLineItemList;
						return testContractLineItemList;
        }
    @isTest
    private static void createContractLineItem1() { 
	   Test.startTest();
        createData();
	  Test.stopTest();
    }
	@isTest
    private static void createContractLineItem2() { 
	String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
       
		Test.startTest();
		  List<vlocity_cmt__ContractLineItem__c> cliList= createData();
		 List<Order> oList=[select id,(select id from orderitems) from order where parentid__c=:orderId];
		 String refid1='';
		 String refid2='';
		 String refid3='';
		 String refid4='';
		 
		 Order childOrdObj=oList.get(0);
		 for(OrderItem oiInst:childOrdObj.orderitems){
			 if(String.isBlank(refid1)){
				 refid1=oiInst.id;
			 }
			 else if(String.isBlank(refid2)){
				 refid2=oiInst.id;
			 }
			 else if(String.isBlank(refid3)){
				 refid3=oiInst.id;
			 }
			 else if(String.isBlank(refid4)){
				 refid4=oiInst.id;
			 }
		 }
		 cliList.get(0).OrderLineItemId__c=refid1;
		 cliList.get(1).OrderLineItemId__c=refid2;
		 cliList.get(2).OrderLineItemId__c=refid3;
		 cliList.get(3).OrderLineItemId__c=refid4;
		 CLITriggerHandler.contractRecTypes=null;
		 update cliList;
	  Test.stopTest();
       // createData();
    }
    @isTest
    private static void createContractLineItem3(){
        List<vlocity_cmt__ContractLineItem__c> cliList= createData();
		Test.startTest();
        Contract testContractObj1 = new Contract(Complex_Fulfillment_Contract__c=ConReq.id,Status='Draft',Contract_Type__c='Amendment',
                                                AccountId=acct.id,ContractTerm=12,StartDate=system.today(),
                                                Customer_Signor__c=testContactObj.id,TELUS_Signor__c=userinfo.getuserid(), Agreement_Type__c = 'Amendment',Effective_Date__c=Datetime.now());
            insert testContractObj1;
			Contract testContractObj2 = new Contract(Complex_Fulfillment_Contract__c=ConReq.id,Status='Draft',Contract_Type__c='Amendment',
                                                AccountId=acct.id,ContractTerm=12,StartDate=system.today(),
                                                Customer_Signor__c=testContactObj.id,TELUS_Signor__c=userinfo.getuserid(), Agreement_Type__c = 'Amendment',Effective_Date__c=Datetime.now());
            insert testContractObj2;
            List<Id> contractIds=new List<Id>();
			for(vlocity_cmt__ContractLineItem__c cli:cliList){
                cli.OrderLineItemId__c='12345';
				cli.Replaced_by_Contract__c=testContractObj1.id;
				cli.Original_Contract__c=testContractObj2.id;
				//cli.Effective_Date__c=System.today();
				cli.term__c='60';
                contractIds.add(cli.vlocity_cmt__ContractId__c);
            }
            Contract contractObj=[select id,Effective_Date__c from contract where id in :contractIds limit 1];
            contractObj.Effective_Date__c=System.today();
            update contractObj;
            update cliList;
			vlocity_cmt__ContractLineItem__c cliObj=cliList.get(0);
			cliObj.Replaced_by_Contract__c=testContractObj1.id;
			update cliObj;
        Test.stopTest();

    }
	@isTest
	private static void finalizeContractReplacementLineItems(){
		Test.startTest();
		Set<Id> contractIdSet=new Set<Id>();
		List<vlocity_cmt__ContractLineItem__c> cliList= createData();
		Contract con=[select id,status from contract where id=:cliList.get(0).vlocity_cmt__ContractId__c];
		con.status='Contract Registered';
		contractIdSet.add(con.id);
		update con;
		CliTriggerHandler handle=new CliTriggerHandler();
		handle.finalizeContractReplacementLineItems(contractIdSet);
		Test.stopTest();
	}
	/*@isTest
	private static void setContractStatusTerminated(){
		Test.startTest();
		Set<Id> contractIdSet=new Set<Id>();
		List<vlocity_cmt__ContractLineItem__c> cliList= createData();
		Contract con=[select id,status from contract where id=:cliList.get(0).vlocity_cmt__ContractId__c];
		con.status='Contract Registered';
		contractIdSet.add(con.id);
		update con;
		CliTriggerHandler handle=new CliTriggerHandler();
		handle.setContractStatusTerminated(contractIdSet);
		Test.stopTest();
	} */
    
    @isTest
    private static void testDebug(){
        
         List<vlocity_cmt__ContractLineItem__c> cliList= createData();
         Contract testContractObj1 = new Contract(Complex_Fulfillment_Contract__c=ConReq.id,Status='Draft',Contract_Type__c='Amendment',
                                                AccountId=acct.id,ContractTerm=12,StartDate=system.today(),
                                                Customer_Signor__c=testContactObj.id,TELUS_Signor__c=userinfo.getuserid(), Agreement_Type__c = 'Amendment',Effective_Date__c=Datetime.now());
            insert testContractObj1;
			
		Test.startTest();
       
            List<Id> contractIds=new List<Id>();
			for(vlocity_cmt__ContractLineItem__c cli:cliList){
                cli.OrderLineItemId__c='12345';
				cli.Replaced_by_Contract__c=testContractObj1.id;
				//cli.Original_Contract__c=testContractObj2.id;
				//cli.Effective_Date__c=System.today();
				cli.term__c='60';
                contractIds.add(cli.vlocity_cmt__ContractId__c);
            }
            //Contract contractObj=[select id,Effective_Date__c from contract where id in :contractIds limit 1];
            //contractObj.Effective_Date__c=System.today();
            //update contractObj;
            update cliList;
           for(vlocity_cmt__ContractLineItem__c cli:cliList){
                System.debug('cli.Replaced_by_Contract__c='+cli.Replaced_by_Contract__c);
               System.debug('cli.term__c='+cli.term__c);
            }
        Test.stopTest();
    }
    
	
}