global class OCOM_CancelOrderController {
    
    // --------------------------------------------
    // Page Getters, Setters & General Declarations
    // --------------------------------------------
    
    public String PageTitle{get;set;}
    public String RedirectPage{get;set;}
    public Order objOrder      {get; set;}
    public Boolean reSumbitOrder {get;set;}
    public boolean isCancelCompleted{get;set;}
    public string profileName{get;set;}
    public string parentTabId{get;set;}
    public integer mockTestVariable=0;
    public String previousOrderStatus{get;set;}
    public string errorLevel{get;set;}
    public string messageName{get;set;}
    public Boolean isValidContractExists{get;set;}
    public Boolean isParentOrder{get;set;}
    public String parentOrderId{get;set;}
    public String childOrderId{get;set;}
    public String RCID{get;set;}
    
    global OCOM_CancelOrderController(ApexPages.StandardController objController) {
        reSumbitOrder= false;
        isParentOrder=false;
        // Determine whether the page references an Id
        Id thisId = ApexPages.currentPage().getParameters().get('id');
        parentTabId=ApexPages.currentPage().getParameters().get('parentTabID');  

        if(String.isNotBlank(thisId)){
            List<Order> orderList =[Select id,accountId,parentId__c from order where parentId__c =:thisId ];
            if(null != orderList && orderList.size()>0){
                isParentOrder = true;
                parentOrderId = orderList[0].parentId__c;
                childOrderId = orderList[0].Id;
                RCID = orderList[0].accountId;
            }

        }
        
        
        // Instantiate the object this page maintains
        
        this.objOrder = (Order)objController.getRecord();
        //Defect 9661 added o.ordermgmtid__c below
        objOrder = [Select o.orderMgmtId_Status__c,o.ordermgmtid__c,o.OrderNumber,o.Order_Number__c,o.Credit_Assessment__c,o.Credit_Assessment__r.CAR_Status__c,o.vlocity_cmt__AccountId__c,
                    o.RecordTypeId, o.Name,o.Id,o.Reason_Code__c,o.General_Notes_Remarks__c,status, 
                    (SELECT Id,ContractNumber,vlocity_cmt__OrderId__c,Status From vlocity_cmt__Contracts__r 
                     where Status NOT IN ('Cancelled Request', 'Cancelled','Client Declined', 'Deactivated', 'Acceptance Expired','Terminated')) 
                    from Order o where o.Id=:objOrder.Id limit 1];
        PageTitle = 'Cancel Order: ' + objOrder.OrderNumber;
        // if(objOrder.General_Notes_Remarks__c !='' && !Test.isRunningTest())
        objOrder.General_Notes_Remarks__c='';
        if(objOrder != null ) {
            List<Contract> contractList = objOrder.vlocity_cmt__Contracts__r;
            if(contractList != null && contractList.size() > 0 && !contractList.isEmpty()){
                isValidContractExists = true;
            }
            else isValidContractExists = false;
        }
        
        
        objOrder.Change_Type__c='Cancel Entire Order';
        
        isCancelCompleted=false;
        user currentUser=[select profile.name from user where id=:userinfo.getuserid()];
        this.profileName=currentUser.profile.name;
    } 
    
    public void DoOk(){
        RedirectPage='';
        isCancelCompleted=false;
        
        if(Test.isRunningTest()){
            objOrder.General_Notes_Remarks__c='Test Remark';
        }
        if(String.isBlank(objOrder.Reason_Code__c) || String.isBlank(objOrder.General_Notes_Remarks__c) ){
            //RedirectPage='Error:One or More fields have been left Blank';
            apexpages.addmessage(new apexpages.message(apexpages.severity.error,'Reason Code and General Notes Remarks cannot be blank.'));
        }else {
            if(!isParentOrder){
                cancelOrder();
            }else if(String.isNotBlank(objOrder.Id)){
                Map<id,Order> childOrdersMap = new Map<id,Order>([Select id,OrderNumber,sub_order_number__c from Order where parentId__c =:objOrder.Id]);
                VlocityCPQUtil vlcCPQUtil = new VlocityCPQUtil();
                Map<String, Object> inputMap = new Map<String, Object>();
                Map<String, Object> outputMap = new Map<String, Object>();
                inputMap.put('cancelOrderIds', (new List<String>{objOrder.Id}));
                inputMap.put('reasonCode',objOrder.Reason_Code__c);
                inputMap.put('generalNotesRemark',objOrder.General_Notes_Remarks__c);
                vlcCPQUtil.invokeMethod('cancelOrders', inputMap, outputMap, null);
                if(null != outputMap && null != outputMap.get('cancelOrdersResult')){
                     Map<String,VlocityCPQUtil.OrderWrapper>  cancelOrderResultMap =  ( Map<String,VlocityCPQUtil.OrderWrapper>)outputMap.get('cancelOrdersResult');
                     if(null != cancelOrderResultMap && cancelOrderResultMap.size() >0){
                         for(VlocityCPQUtil.OrderWrapper OrderWrapper :cancelOrderResultMap.values() ){
                             if(OrderWrapper.isError){
                                String childOrderNumber = OrderWrapper.orderId;
                                if(null != childOrdersMap && null != childOrdersMap.get(OrderWrapper.orderId)){
                                    childOrderNumber = (childOrdersMap.get(OrderWrapper.orderId)).sub_order_number__c;
                                }                               
                               apexpages.addmessage(new apexpages.message(apexpages.severity.error,'  '+childOrderNumber+'  '+  OrderWrapper.message));  
                             }
                         }
                     }
                }else if(null != outputMap && null != outputMap.get('error')){
                    apexpages.addmessage(new apexpages.message(apexpages.severity.error,'  ' +  outputMap.get('error')));
                }
            }
            
            if(!apexpages.hasmessages()){
                Boolean result=DoCancel(objOrder.id,objOrder.Reason_Code__c,objOrder.General_Notes_Remarks__c);
                if(result)
                { 
                    //apexpages.addmessage(new apexpages.message(apexpages.severity.confirm,'Your Order has been cancelled successfully.  Please click the Close button.'));
                    isCancelCompleted=true;
                    system.debug('Is cancel Completed' + isCancelCompleted);
                }
                
            }        
        }
        // return null;
    }
    
    public void showMessage() {
        if(errorLevel == 'CONFIRM') 
            ApexPages.addMessage(new ApexPages.Message(apexpages.severity.confirm, messageName));
    }
    
    public pagereference invalidForm(){
        apexpages.addmessage(new apexpages.message(apexpages.severity.error,' Reason Code, and Notes cannot be blank.'));
        return null;
    }
    public PageReference CancelButton()
    { 
        PageReference pageRef = new PageReference('/apex/OCOM_SubmitOrder?id='+objOrder.id);
        pageRef.setRedirect(true);
        return pageRef;
    }
    @TestVisible
    void cancelOrder(){     
        System.debug('cancelOrder, previousOrderStatus: ' + previousOrderStatus);
        Boolean IsCancelFail= false;
        Boolean isOrderActivated=false;
        List<apexpages.message> messages = new List<apexpages.message>();        
        
        //else if(previousOpportunityStage.StartsWith('Order Request') && !previousOpportunityStage.equalsIgnoreCase('Order Request Completed')){
        //TS TODO in the if statement below, order can be cancelled if in 'Delivery' status, as long as there are no assets in 'Complete' status yet
        //Defect 9661 Begin
         Boolean sendOrderToNC=previousOrderStatus == 'In Progress' || previousOrderStatus == 'Completed' || previousOrderStatus == 'Error' || previousOrderStatus == 'Initiating' || previousOrderStatus == 'Activated' || previousOrderStatus == 'Delivery' || previousOrderStatus  == 'Submitted';
        if(!sendOrderToNC){
            sendOrderToNC=previousOrderStatus  == 'Not Submitted' || String.isNotBlank(objOrder.ordermgmtid__c);
        }       
        //Defect 9661 End
        if (sendOrderToNC) { //Changed the condition to a boolean variable for Defect 9661
            isOrderActivated=true;
            //        if(objOrder.orderMgmtId__c != null){             
            //String isSuccess = SMB_WebserviceHelper.sendWebserviceRequest(SMBCare_WebServices__c.getValues('SMB_CancelOrderEndpoint').value__c,Label.SMB_CancelOrderXML, objOrder.Order_Requests__r[0].NC_Order_ID__c,SMBCare_WebServices__c.getValues('SMB_CancelOrderSoapAction').value__c,'Cancel Order'); 
            map<string,schema.recordtypeinfo> recordTypeInfoMap=webservice_integration_error_log__c.sobjecttype.getdescribe().getrecordtypeinfosbyname();
            Webservice_Integration_Error_Log__c log=new Webservice_Integration_Error_Log__c(apex_class_and_method__c='OCOM_CancelOrderController.cancelOrder',external_system_name__c='NC',webservice_name__c='Cancel Order',object_name__c='ORDER',sfdc_record_id__c=objOrder.id);
            
            try{
            
              CancelNCOrder(objOrder.id,log); 
                
            }catch(CancelNCOrderException cnoe){
                system.debug(cnoe.getmessage());
                log.recordtypeid=recordTypeInfoMap.get('Failure').getrecordtypeid();
                log.error_code_and_message__c=cnoe.getmessage();
                log.stack_trace__c=cnoe.getstacktracestring();
                apexpages.addmessage(new apexpages.message(apexpages.severity.error,cnoe.getmessage()));
                IsCancelFail =true;               
            }finally{                            
                if(IsCancelFail){
                    try{
                        log.error_code_and_message__c=(string.isnotblank(log.error_code_and_message__c)&&log.error_code_and_message__c.length()>32767?log.error_code_and_message__c.substring(0,32767):log.error_code_and_message__c);
                        log.stack_trace__c=(string.isnotblank(log.stack_trace__c)&&log.stack_trace__c.length()>32767?log.stack_trace__c.substring(0,32767):log.stack_trace__c);
                        if(string.isnotblank(log.error_code_and_message__c) && (log.error_code_and_message__c.indexOf('IO Exception:')!=-1 || log.error_code_and_message__c.indexOf('Read timed out')!=-1)){
							flagTimeoutWithOrderObj(objOrder);
						}
                        insert log;
                        if (Test.isRunningTest())
                        {
                            //Test code to throw exception
                            IsCancelFail=false;
                            messages.add(new apexpages.message(apexpages.severity.error,'For Test coverage'));                                 
                            Webservice_Integration_Error_Log__c toThrowException = new Webservice_Integration_Error_Log__c();
                            update toThrowException;
                            
                        }
                    }catch(exception ex){
                        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                        List<String> lstEmail = new List<String>(); //use single custom setting for all data
                        SMBCare_WebServices__c wsEmailAddress = SMBCare_WebServices__c.getValues('EmailAddress');
                        string htmlBody = '<b>OCOM_CancelOrderController.cancelOrder</b><br/>'+
                            '<b>------------------------------------------------------</b><br/>'+
                            '<b>===========================================================</b><br/>'+
                            '<b>Unable to insert NC Cancellation Log for Order:'+objOrder.Id+'</b><br/><br/>'+
                            ex.getmessage()+'<br/><br/>Stack Trace:<br/>'+ex.getstacktracestring()+'<br/>'+
                            '<b> ---------------------------------------------</b><br/>';
                        string eMail='andy.leung@telus.com';
                        if (String.isNotBlank(wsEmailAddress.Value__c)){
                            eMail= wsEmailAddress.Value__c;
                        }
                        mail.setToAddresses(eMail.split(',', 0));
                        mail.setSenderDisplayName('noreply@salesforce.com');
                        mail.setReplyTo('noreply@salesforce.com');
                        mail.setSubject('Unable to insert NC Cancellation Log for Order');
                        mail.setHtmlBody(htmlBody);
                        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                        
                    }
                }
            }
            
        }
        system.debug('IsCancelFail'+IsCancelFail);          
        //WFM booking cancellation
        if((isOrderActivated && !IsCancelFail) ||(!isOrderActivated)){
            try{
                //SMB_WebServiceHelper.OCOM_CancelWorkOrder(objOrder.id); 
                unreserveUsedTns(objOrder.id);
                OCOM_WFM_DueDate_Mgmt_Helper.cancelWFMBookingByOrderId(objOrder.id);
                
                
                if (Test.isRunningTest()) {
                    //Test code to throw exception
                    messages.add(new apexpages.message(apexpages.severity.error,'For Test coverage'));
                }
            }
            catch(exception e)
            {
                System.debug(e.getmessage());
                // apexpages.addmessage(new apexpages.message(apexpages.severity.error,'Please contact Adminstrator,WorkOrder cancellation is failed due to : '+e.getmessage()));
            }
        }
        if(!messages.isempty()){
            for(apexpages.message message:messages){
                if(!Test.isRunningTest()) { apexpages.addmessage(message);
                                          }
            }
            
        }
        
    }
    @TestVisible
    private void flagTimeoutWithOrderObj(Order ordObj){
        if(ordObj!=null){
            ordObj.orderMgmtId_Status__c=OrdrConstants.ORDR_CANCEL_TIMEOUT_ERROR;//'ordCancelTimeOutError';
            update ordObj;
        }
    }
    @TestVisible
    void cancelNCOrder(String orderID, webservice_integration_error_log__c log){
        
        OrdrCancelOrderManager cancelOrder= new OrdrCancelOrderManager();
        TpCommonBaseV3.CharacteristicValue[] CharacteristicValue;
        TpFulfillmentCustomerOrderV3.CustomerOrder cancelOrderResponse;
        list<string> Value = new list<string>();
        try{
            if(Test.isRunningTest())
            {  
                TpFulfillmentCustomerOrderV3.CustomerOrder cus = new TpFulfillmentCustomerOrderV3.CustomerOrder();
                List<TpCommonBaseV3.CharacteristicValue> CharacteristicValue1 = new List<TpCommonBaseV3.CharacteristicValue>();
                TpCommonBaseV3.CharacteristicValue cvalue= new TpCommonBaseV3.CharacteristicValue();
                List<string> str= new List<string>();
                str.add('str');cvalue.value=str;
                CharacteristicValue1.add(cvalue);
                cus.CharacteristicValue=CharacteristicValue1;
                if(mockTestVariable==0)
                {  cus.ErrorCode='error';
                 cus.ErrorDescription='errordesc';
                }else if(mockTestVariable==1){  cus.ErrorCode='';
                                              cus.ErrorDescription='';
                                             }
                cancelOrderResponse =cus;
                if(mockTestVariable==3){cancelOrderResponse =  OrdrCancelOrderManager.cancelOrder(orderID);}   
            } else{
                //TODO check if the order has passed PONR state
				Boolean isOrderCancellable=isCancellable(orderID);
                if(!isOrderCancellable){
                    throw new CancelNCOrderException('Your order has passed PONR state.Hence it cannot be cancelled');   
                }
                cancelOrderResponse =  OrdrCancelOrderManager.cancelOrder(orderID);      
                if(null!=cancelOrderResponse && null !=cancelOrderResponse.CharacteristicValue && cancelOrderResponse.CharacteristicValue.size()>0 && null != cancelOrderResponse.CharacteristicValue.size())
                { 
                    log.error_code_and_message__c+='\n\nRequest:\n\n'+cancelOrderResponse.ErrorCode+'\n\nResponse:\n\n'+cancelOrderResponse.ErrorDescription;
                    if(String.isBlank(cancelOrderResponse.ErrorCode))
                    { 
                        CharacteristicValue =cancelOrderResponse.CharacteristicValue;
                        if(null!=CharacteristicValue[0] && CharacteristicValue[0].value.size()>0){
                            Value = CharacteristicValue[0].value;
                            system.debug('response'+value);
                        } 
                        
                    } 
                    else
                    {
                        String response='EXCEPTION:-';              
                        response=+' '+cancelOrderResponse.ErrorCode+' : '+cancelOrderResponse.ErrorDescription;            
                        response=response+' : Please contact Administrator.'; 
                        system.debug('@@@'+response);
                        //if(cancelOrderResponse.ErrorCode.containsignorecase('110')){
                        throw new CancelNCOrderException(response);          
                        
                    }
                }   
            }
        }catch(calloutexception ce){
            throw new CancelNCOrderException('NetCracker Order Manager did not respond.  Salesforce received: '+ ce.getmessage());                
        }catch(exception e){ 
            throw new CancelNCOrderException('Please contact Adminstrator, Order didn\'t get cancelled due to mentioned Error:'+ e.getmessage());
        }        
        
        
    }
    @TestVisible
    private static Boolean isCancellable(String orderId){
        Boolean isOrderCancellable=true;
        List<OrderItem> orderItemRecs = [Select Id, orderMgmtId_Status__c, order.orderMgmtId_Status__c  FROM OrderItem WHERE OrderId = :orderId];
        if(orderItemRecs != null && orderItemRecs.size()>0){
            for(OrderItem orderRec : orderItemRecs){               
                if((String.isNotBlank(orderRec.orderMgmtId_Status__c) && OrdrConstants.OI_PONR_STATUS.equalsIgnoreCase(orderRec.orderMgmtId_Status__c)) || (String.isNotBlank(orderRec.orderMgmtId_Status__c) && OrdrConstants.OI_COMPLETED_STATUS.equalsIgnoreCase(orderRec.orderMgmtId_Status__c)) || (String.isNotBlank(orderRec.order.orderMgmtId_Status__c ) && OrdrConstants.NC_ORDR_PONR_STATUS.equalsIgnoreCase(orderRec.order.orderMgmtId_Status__c)) || (String.isNotBlank(orderRec.order.orderMgmtId_Status__c ) && OrdrConstants.NC_ORDR_PROCESSED_STATUS.equalsIgnoreCase(orderRec.order.orderMgmtId_Status__c))){
                       isOrderCancellable=false; 
                   } 		
                
            }
        }
        return isOrderCancellable;
    }
    
    public boolean DoCancel(string orderId, string reasonCode, string notes)
    {       
        Order ord = [Select change_type__c, reason_code__c,Status, General_Notes_Remarks__c,Credit_Assessment__r.CAR_Result__c,Credit_Assessment__r.CAR_Status__c,Credit_Assessment__c,Credit_Assessment__r.CAR_Code__c from Order where Id=:orderId];
        ord.Change_Type__c = 'Cancel Entire Order';
        ord.Reason_Code__c = reasonCode;
        ord.General_Notes_Remarks__c = notes;
        ord.Status ='Cancelled';
        ord.Activities__c='N/A';
        try{update ord;}catch(exception e){ apexpages.addmessage(new apexpages.message(apexpages.severity.error,'Please contact System Admin' + e.getMessage()));return false;}
        system.debug('inside car cancellation 1'+ord);
        //CAR Cancellation
        string carNo = ord.Credit_Assessment__c;
        string carStatus;
        string carResult;    
        if(string.isNotBlank(carNo)){
            carStatus =ord.Credit_Assessment__r.CAR_Status__c;
            carResult =ord.Credit_Assessment__r.CAR_Result__c;
        }
        string orderStatus =ord.Status;
        // Thomas QC-10813 Start
        //if(string.isnotblank(carNo) && string.isnotblank(orderStatus) && orderStatus=='Cancelled' && carStatus !='Cancelled' && carStatus !='Completed' && carResult!='Approved'){
        if(string.isnotblank(carNo) && string.isnotblank(orderStatus) && orderStatus=='Cancelled' && carStatus !='Cancelled'){
        // Thomas QC-10813 End
            system.debug('inside car cancellation 1 '+ord);
            Credit_Assessment__c CAR= new Credit_Assessment__c();
            CAR.id=carNo;
            CAR.CAR_Status__c='Cancelled';     
            CAR.CAR_Code__c='Cancel order received: TELUS Initiated'; //o.Reason_Code__c;
            // need to add field for reason to cancel           
            try{update CAR; }catch(exception e){apexpages.addmessage(new apexpages.message(apexpages.severity.error,'Please contact System Admin' + e.getMessage()));return false;}
        } 
        return true;
    }
    
    @remoteaction
    global static String vfrPrepareOrderCancellationRequest(string orderId, string reasonCode, string notes)
    {       
        Order ord = [Select change_type__c, reason_code__c,Status, General_Notes_Remarks__c,Credit_Assessment__r.CAR_Result__c,Credit_Assessment__r.CAR_Status__c,Credit_Assessment__c,Credit_Assessment__r.CAR_Code__c from Order where Id=:orderId];
        ord.Change_Type__c = 'Cancel Entire Order';
        ord.Reason_Code__c = reasonCode;
        ord.General_Notes_Remarks__c = notes;
        String previousStatus = ord.Status;
        /*system.debug('previousStatus '+previousStatus );
if(string.isNotblank(previousStatus) && ('Activated'== previousStatus || 'Delivery'==previousStatus || 'Submitted'== previousStatus )){
List<Asset> assetswithOrder=[select id,status from Asset where vlocity_cmt__OrderId__c=:orderId and status IN('Shipped','Installed')];
if(null != assetswithOrder && assetswithOrder.size()>0) {
apexpages.addmessage(new apexpages.message(apexpages.severity.error,'You cannot cancel this order.It has assets those are shipped or installed.'));
return 'has assets';
}
return previousStatus;
}else{
return previousStatus;
}*/
        return previousStatus;
    }
    
    @future(callout=true)
    public static void unreserveUsedTns(String orderId){
        List<String> tnList=OrdrUtilities.getUsedTns(orderId);
        OrdrTnReservationImpl.unreserveUsedTns(orderId,tnList);      
        
    }  
    @remoteaction
    public static string cancelEnvelope(String  OrderId){
        map<string,orderItem> LinenumberTOCliIDMap = new map<string,orderItem>();
        String errorMsgs; 
        boolean isRemoteCall = false;
        string callSuccessfulMessage = '';
        string sErrorMessage = '';
        try{ 
            //String  OrderId = (string)inputMap.get('ContextId');
            Order objOrder = new Order(id=OrderId);
            system.debug('Order Id_____:' + objOrder);
            //Iterate trough Order Items to see if any of them are Amended.--- Check if we need to use a Specific value for AmendStatus
            if(OrderId != null){
                map<String,object> outMap = OCOM_ContractUtil.AmendOrder(objOrder,'Cancel');
                sErrorMessage=(string)outMap.get('amendContractResponse');
                isRemoteCall=(boolean)outMap.get('isCallContinuation');
                string envelopeId = (string)outMap.get('envelopId'); 
                System.debug('OutMap--->' + outMap);
            }
            errorMsgs = sErrorMessage;
            system.debug('****isRemoteCall*****'+isRemoteCall + '***sErrorMessage****'+sErrorMessage);
        }catch(exception e){
            String errorMessage=Label.OCOM_ExceptionMessagePrefix+e.getMessage();
            System.debug(LoggingLevel.ERROR, 'Exception is '+e);
            System.debug(LoggingLevel.ERROR, 'Exception stack trace '+e.getStackTraceString());
            
            return errorMsgs = errorMessage;
            // outPutMap.put('error',errorMessage);
        }
        return errorMsgs;
        
    }   
    
}