public class CreditSearchIdentityController
{
    //Parameters from a user search
    public CreditInfoSearchParam searchParam {set; get;}

    public List<DisplayCreditInfo> matchingCreditInfoList {set; get;}

    public boolean isShowMatchedCreditInfo {set; get;}

    /**
     * Constructor
     */
	public CreditSearchIdentityController(ApexPages.StandardController stdController)
    {
        searchParam = new CreditInfoSearchParam();

        isShowMatchedCreditInfo = false;
    }
    
    public void search()
    {
        conditionSearchParam();

        matchingCreditInfoList = buildDisplayCreditInfoList(queryCreditProfile(searchParam));

        matchingCreditInfoList.addAll(buildDisplayCreditInfoList(queryCreditAssessment(searchParam)));

        isShowMatchedCreditInfo = true;
    }
    
    private List<Credit_Profile__c> queryCreditProfile(CreditInfoSearchParam param)
    {
        if ( !param.isEmpty() )
        {
            String query =  buildQueryStringForCreditInfoByIdentifiers(param, 'Credit_Profile__c');

            System.debug('Querying Credit Profile: ' + query);

            return Database.query(query);
        }
        else
        {
            return new List<Credit_Profile__c>();
        }
    }

    private List<Credit_Assessment__c> queryCreditAssessment(CreditInfoSearchParam param)
    {
        if ( !param.isEmpty() )
        {
            String query =  buildQueryStringForCreditInfoByIdentifiers(param,'Credit_Assessment__c');

            System.debug('Querying Credit Profile: ' + query);

            return Database.query(query);
        }
        else
        {
            return new List<Credit_Assessment__c>();
        }
    }

    private String buildQueryStringForCreditInfoByIdentifiers(CreditInfoSearchParam param, String objectApiName)
    {
        String query =  'Select Id, First_Name__c, Middle_Name__c, Last_Name__c, Legal_Name_On_File__c, RCID__c, ' +
                        'Street_Address_Residential__c, City_Residential__c, Province_State_Residential__c, Postal_Code_Residential__c, Country_Residential__c, ' +
                        'Birth_Date__c, SIN__c, Driver_s_License__c, Drivers_License_Province__c, Provincial_ID__c, Provincial_ID_Province__c, Passport__c, Passport_Country__c, Health_Card_No__c';

        String fromObject = 'From ' + objectApiName;

        String whereCondition = '';

        if (String.isNotBlank(param.sin))
        {
            whereCondition += 'SIN_Hash__c = ' + '\'' + CreditCrypto.hash(param.sin) + '\' ';
        }

        if (String.isNotBlank(param.driversLicenseNum))
        {
            whereCondition += 'OR ( Driver_s_License_Hash__c = ' + '\'' + CreditCrypto.hash(param.driversLicenseNum) + '\' ';

            if (String.isNotBlank(param.driversLicenseProvince))
            {
                whereCondition += 'AND Drivers_License_Province__c = ' + '\'' + String.escapeSingleQuotes(param.driversLicenseProvince) + '\' ';
            }

            whereCondition += ')';
        }

        if (String.isNotBlank(param.provincialIdNum))
        {
            whereCondition += 'OR ( Provincial_ID_Hash__c = ' + '\'' + CreditCrypto.hash(param.provincialIdNum) + '\' ';

            if (String.isNotBlank(param.provincialIdProvince))
            {
                whereCondition += 'AND Provincial_ID_Province__c = ' + '\'' + String.escapeSingleQuotes(param.provincialIdProvince) + '\' ';
            }

            whereCondition += ')';
        }

        if (String.isNotBlank(param.passportNum))
        {
            whereCondition += 'OR ( Passport_Hash__c = ' + '\'' + CreditCrypto.hash(param.passportNum) + '\' ';

            if (String.isNotBlank(param.passportCountry))
            {
                whereCondition += 'AND Passport_Country__c = ' + '\'' + String.escapeSingleQuotes(param.passportCountry) + '\' ';
            }

            whereCondition += ')';
        }

        if (String.isNotBlank(param.healthCardNum))
        {
            whereCondition += 'OR Health_Card_No_Hash__c = ' + '\'' + CreditCrypto.hash(param.healthCardNum) + '\' ';
        }

        whereCondition = whereCondition.removeStart('OR');

        query += ' ' + fromObject + ' Where ' + whereCondition;

        return query;
    }

    private List<DisplayCreditInfo> buildDisplayCreditInfoList(List<Credit_Profile__c> creditProfiles)
    {
        List<DisplayCreditInfo> displayCreditProfiles = new List<DisplayCreditInfo>();

        for ( Credit_Profile__c profile : creditProfiles )
        {
            displayCreditProfiles.add(buildDisplayCreditInfo(profile));
        }

        return displayCreditProfiles;
    }

    private DisplayCreditInfo buildDisplayCreditInfo(Credit_Profile__c creditProfile)
    {
        DisplayCreditInfo profile = new DisplayCreditInfo();

        profile.id = creditProfile.id;
        profile.firstName = creditProfile.First_Name__c;
        profile.middleName = creditProfile.Middle_Name__c;
        profile.lastName = creditProfile.Last_Name__c;
        profile.name = buildIndividualCustomerDisplayName(creditProfile.First_Name__c, creditProfile.Middle_Name__c, creditProfile.Last_Name__c);
        profile.legalName = creditProfile.Legal_Name_On_File__c;
        profile.rcid = creditProfile.RCID__c;
        profile.addressLine = creditProfile.Street_Address_Residential__c;
        profile.city = creditProfile.City_Residential__c;
        profile.provinceState = creditProfile.Province_State_Residential__c;
        profile.postalCode = creditProfile.Postal_Code_Residential__c;
        profile.country = creditProfile.Country_Residential__c;
        profile.sin = creditProfile.SIN__c;
        profile.birthdate = creditProfile.Birth_Date__c;
        profile.driversLicenseNum = creditProfile.Driver_s_License__c;
        profile.driversLicenseProvince = creditProfile.Drivers_License_province__c;
        profile.provincialIdNum = creditProfile.Provincial_ID__c;
        profile.provincialIdProvince = creditProfile.Provincial_ID_Province__c;
        profile.passportNum = creditProfile.Passport__c;
        profile.passportCountry = creditProfile.Passport_Country__c;
        profile.healthCardNum = creditProfile.Health_Card_No__c;
        profile.type = System.Label.Credit_Profile;

        return profile;
    }

    private List<DisplayCreditInfo> buildDisplayCreditInfoList(List<Credit_Assessment__c> cars)
    {
        List<DisplayCreditInfo> displayCreditProfiles = new List<DisplayCreditInfo>();

        for ( Credit_Assessment__c car : cars )
        {
            displayCreditProfiles.add(buildDisplayCreditInfo(car));
        }

        return displayCreditProfiles;
    }

    private DisplayCreditInfo buildDisplayCreditInfo(Credit_Assessment__c car)
    {
        DisplayCreditInfo profile = new DisplayCreditInfo();

        profile.id = car.id;
        profile.firstName = car.First_Name__c;
        profile.middleName = car.Middle_Name__c;
        profile.lastName = car.Last_Name__c;
        profile.name = buildIndividualCustomerDisplayName(car.First_Name__c, car.Middle_Name__c, car.Last_Name__c);
        profile.legalName = car.Legal_Name_On_File__c;
        profile.rcid = car.RCID__c;
        profile.addressLine = car.Street_Address_Residential__c;
        profile.city = car.City_Residential__c;
        profile.provinceState = car.Province_State_Residential__c;
        profile.postalCode = car.Postal_Code_Residential__c;
        profile.country = car.Country_Residential__c;
        profile.sin = car.SIN__c;
        profile.birthdate = car.Birth_Date__c;
        profile.driversLicenseNum = car.Driver_s_License__c;
        profile.driversLicenseProvince = car.Drivers_License_province__c;
        profile.provincialIdNum = car.Provincial_ID__c;
        profile.provincialIdProvince = car.Provincial_ID_Province__c;
        profile.passportNum = car.Passport__c;
        profile.passportCountry = car.Passport_Country__c;
        profile.healthCardNum = car.Health_Card_No__c;
        profile.type = System.Label.creditAssessment;

        return profile;
    }

    private String buildIndividualCustomerDisplayName(String firstName, String middleName, String lastName)
    {
        String fName = String.isNotBlank(firstName) ? firstName : '';
        String mName = String.isNotBlank(middleName) ? ' ' + middleName + ' ' : ' ';
        String lName = String.isNotBlank(lastName) ? lastName : '';

        String concatenatedName = fName + mName + lName;

        return concatenatedName.trim();
    }

    private void conditionSearchParam()
    {
        searchParam.driversLicenseProvince = convertToProvinceName(searchParam.driversLicenseProvince);
        searchParam.provincialIdProvince = convertToProvinceName(searchParam.provincialIdProvince);
        searchParam.passportCountry = convertToCountryName(searchParam.passportCountry);
    }

    private static String convertToProvinceName(String provinceCode)
    {
        String provinceValue = provinceCode;

        String provinceName = CreditReferenceCode.getJurisdictionProvinceName(provinceCode);

        if ( String.isNotBlank(provinceName) )
        {
            provinceValue = provinceName;
        }

        return provinceValue;
    }

    private static String convertToCountryName(String countryCode)
    {
        String countryValue = countryCode;

        String countryName = CreditReferenceCode.getJurisdictionCountryName(countryCode);

        if ( String.isNotBlank(countryName) )
        {
            countryValue = countryName;
        }

        return countryValue;
    }

    public class CreditInfoSearchParam
    {
        public String sin {set; get;}
        public String driversLicenseNum {set; get;}
        public String driversLicenseProvince {set; get;}
        public String provincialIdNum {set; get;}
        public String provincialIdProvince {set; get;}
        public String passportNum {set; get;}
        public String passportCountry {set; get;}
        public String healthCardNum {set; get;}

        public boolean isEmpty()
        {
            return String.isBlank(sin) && String.isBlank(driversLicenseNum) && String.isBlank(provincialIdNum) && String.isBlank(passportNum) && String.isBlank(healthCardNum);
        }
    }

    public class DisplayCreditInfo
    {
        public String id {set; get;}
        public String name {set; get;}
        public String firstName {set; get;}
        public String middleName {set; get;}
        public String lastName {set; get;}
        public String legalName {set; get;}
        public String rcid {set; get;}
        public String addressLine {set; get;}
        public String city {set; get;}
        public String provinceState {set; get;}
        public String postalCode {set; get;}
        public String country {set; get;}
        public Date birthdate {set; get;}
        public String sin {set; get;}
        public String driversLicenseNum {set; get;}
        public String driversLicenseProvince {set; get;}
        public String provincialIdNum {set; get;}
        public String provincialIdProvince {set; get;}
        public String passportNum {set; get;}
        public String passportCountry {set; get;}
        public String healthCardNum {set; get;}
        public String type {set; get;}
    }
}