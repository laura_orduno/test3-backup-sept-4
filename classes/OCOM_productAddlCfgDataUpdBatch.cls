global class OCOM_productAddlCfgDataUpdBatch implements Database.Batchable<sObject> {
    Final static String DEVICEATTRNAME  = 'Operating System Type';
    Final static String RATEPLANATTRNAME = 'Compatible Operating System Type';
    Final static String LINETYPE = 'Line Type';
    Final static String CHARGEABLE = 'Chargeable';
     String query;
     //Added By Sandip for catalog automation 01/19
     public Boolean executeNextJob = false;
     //End
     
    global OCOM_productAddlCfgDataUpdBatch() {
        
    }
    //Added By Sandip for catalog automation 01/19
    global OCOM_productAddlCfgDataUpdBatch(Boolean isExecuteNextJob) {
        executeNextJob = isExecuteNextJob;
    }
    //End
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        set<String> prodSpecNameList = new Set<String>{'Wireless Device Product Component','Wireless Rate Plan Product Component','TELUS Office Phone Product'};
		set<String> relationShipTypeNameList = new Set<String>{'Promotion'};
 
        list<string> fieldsList = new list<string>{'id','Name','Sellable__c','vlocity_cmt__TrackAsAgreement__c', 'ProductCode', 'ProductSpecification__r.Name','vlocity_cmt__JSONAttribute__c','VLAdditionalConfigData__c','RelationshipType__c'};
        //set<id> product2Ids = new set<id>{'01t22000000HWFXAA4','01t40000004X0uOAAS','01t22000000HWFYAA4','01t40000004X6uIAAS'};
        query = 'SELECT ';
        query += String.join(fieldsList,',');
        query +=' FROM ' + 'product2';
        query +=' WHERE ProductSpecification__r.Name IN :prodSpecNameList' ;
         query +=' OR RelationshipType__c IN :relationShipTypeNameList'; //and id IN :product2Ids

        system.debug('Query :: ' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        Map< String, Object> inputMap = new Map<String, Object>();  
        Map<String, Object> outputMap = new Map<String, Object>();
        Map<Id,Product2> productOutputMap = new Map<Id,Product2>();
        List<product2> udpateOSTypeProdList = new List<product2>();
        system.debug('SCOPE _____Size_______########' + scope.size());

        if(scope.size() > 0 && !scope.isEmpty()){

            Map<id,sobject> allProductMap = new  Map<id,sobject>(scope);
            inputmap.put('product2Ids', (allProductMap.keyset() ));
            inputmap.put('product2MapResult', allProductMap);
            outputMap = setOSTypeOnProdHeader(inputMap,outputMap);
        
            if(outputMap.containskey('productToUpdateMap')){
                productOutputMap = (Map<id,Product2>)outputMap.get('productToUpdateMap');
            }
            System.debug('productOutputMap_Keys_______########' + productOutputMap.keySet());

            for(product2 prodObj: (list<product2>)scope){
                if(productOutputMap != null && productOutputMap.containskey(prodObj.id)){
                   // prodObj.vlocity_cmt__TrackAsAgreement__c = true;
                    udpateOSTypeProdList.add(prodObj);
                }
            }
        }
        System.debug('udpateOSTypeProdList- Size_________########' + udpateOSTypeProdList.size());
            if(udpateOSTypeProdList.size() > 0 && !udpateOSTypeProdList.isEmpty() ){
                update udpateOSTypeProdList;
            }   

    }
    
        global void finish(Database.BatchableContext BC) { 
           //Commented By Sandip for catalog automation 01/19
           /* AsyncApexJob a =    [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, ExtendedStatus,
                                 TotalJobItems, CreatedBy.Email FROM AsyncApexJob WHERE Id = :BC.getJobId()];
            
            // Send an email to the Apex job's submitter
            // notifying of job completion.
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            
            String[] toAddresses = new String[] {a.CreatedBy.Email};
            mail.setToAddresses(toAddresses);
            mail.setSubject('Product update job Status: ' + a.Status);
            mail.setPlainTextBody('The batch Apex job processed ' + a.TotalJobItems + ' batches with '+ a.NumberOfErrors + ' failures.'+ ' Error Details: ' + a.ExtendedStatus);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); */  
            //Comment End
            //Added By Sandip for catalog automation 01/19
            // End
            
             // Initiate next batch job
            ExternalAuditLogHelper.ExternalAuditLogCreateParameter paramObj = new  ExternalAuditLogHelper.ExternalAuditLogCreateParameter();
            paramObj.emailAddress = '';
            paramObj.jobName = ExternalAuditLogHelper.OCOM_PROD_ADDLCFG_BATCH_JOB;
            //if(isError){
            //    paramObj.result = ExternalAuditLogHelper.OCOM_ETL_JOB_ERROR;
            //}else{
                paramObj.result = ExternalAuditLogHelper.OCOM_ETL_JOB_SUCCESS;
            //}
            paramObj.resultDescription = '';
            paramObj.batchId = bc.getJobId();
            paramObj.emailSubject = System.Label.OCOM_productAddlCfgDataUpdBatch + ' ';
            paramObj.executeNextJob = executeNextJob;
            ExternalAuditLogHelper.createAuditLog (paramObj);
        }
 @TestVisible
    private static Map<string,Object> setOSTypeOnProdHeader(Map<String, Object> inputMap , Map<String, Object> outMap){
      
      //Adharsh: Attribute Assignemnt Logic begin
        Set<String> attrnameSet = new Set<String>{DEVICEATTRNAME,RATEPLANATTRNAME,LINETYPE,CHARGEABLE};
        Map<Id,String> prodIDtoVLAddlConfigMap = new Map<Id,String>(); 
        prodIDtoVLAddlConfigMap = getAttrAssignmnt(attrnameSet);
      //Adharsh: Attribute Assignemnt Logic end  
        
        set<id> porductIdSet = new set<id>();
        Map<id,sObject> prodIDToSobjectMap  = new  Map<id,sObject>();
        Map<Id,Product2> productToUpdateMap = new Map<Id, Product2>(); 
       if(inputMap != null && inputMap.containsKey('product2Ids')){
            porductIdSet.addall((set<id>)inputMap.get('product2Ids'));
        }
         if(inputMap != null && inputMap.containsKey('product2MapResult')){
            prodIDToSobjectMap = (Map<id,sObject>)inputMap.get('product2MapResult');
        }
        //map<id,boolean> contractableProductMap = new map<id,boolean>();
        for(id prodID:prodIDToSobjectMap.keySet() ){
            Product2 prod = (Product2)prodIDToSobjectMap.get(prodID);
            
            String prodSpecName =  prod.ProductSpecification__r.Name; 
            //Adharsh: Attribute Assignemnt Logic begin
            if(prodIDtoVLAddlConfigMap != null && prodIDtoVLAddlConfigMap.size() > 0 && !prodIDtoVLAddlConfigMap.isEmpty()){ 
                // Identify Device and Rate Plan products which needs opersting system to be stamped in header.
                if(prodSpecName != null && (prodSpecName.containsIgnoreCase('Wireless Device') ||  prodSpecName.containsIgnoreCase('Wireless Rate Plan'))){
                    //Product2 prdOSTypeUpdate = getOSTypeFromJSONAttr(prod, DEVICEATTRNAME);
                    if(prodIDtoVLAddlConfigMap.containskey(prodID) && prodIDtoVLAddlConfigMap.get(prodID) != null &&prodIDtoVLAddlConfigMap.get(prodID) != ''){
                            prod.VLAdditionalConfigData__c = prodIDtoVLAddlConfigMap.get(prodID);
                            productToUpdateMap.put(prod.id, prod);
                    }
                }
                // Identify Waive products which needs to be flagged as Waive to be stamped in header.  
                else if(prod.Name != null && prod.Name.startsWithIgnoreCase('Waive') && prod.RelationshipType__c != null && prod.RelationshipType__c.equalsIgnoreCase('Promotion')){
     
                    Map<String,String> waivetypeMap = new Map<String,String>{'Waive' => 'WAIVE PROMO'};
                        prod.VLAdditionalConfigData__c = JSON.serialize(waivetypeMap);
                    Product2 waiveProdUpdate = prod;
                    productToUpdateMap.put(waiveProdUpdate.id, waiveProdUpdate);
                    }
                // Identify Overline Offers which needs the LineType attributes to be stamped to header.      
                else if(prodIDtoVLAddlConfigMap.containskey(prodID) && (prodIDtoVLAddlConfigMap.get(prodID) != null || prodIDtoVLAddlConfigMap.get(prodID) != '') ){
                       prod.VLAdditionalConfigData__c = prodIDtoVLAddlConfigMap.get(prodID);
                       productToUpdateMap.put(prod.id, prod);
                    }     
                }

            }
            //Adharsh: Attribute Assignemnt Logic end 

        //Adharsh : Commenting out JSON Reference Logic Start    
           /* if(prod.vlocity_cmt__JSONAttribute__c != null &&prodSpecName != null && prodSpecName.containsIgnoreCase('Wireless Device') ){
                Product2 prdOSTypeUpdate = getOSTypeFromJSONAttr(prod, DEVICEATTRNAME);
                if(prdOSTypeUpdate != null ){
                        productToUpdateMap.put(prdOSTypeUpdate.id, prdOSTypeUpdate);
                }
            }
            else if(prod.vlocity_cmt__JSONAttribute__c != null &&prodSpecName != null && prodSpecName.containsIgnoreCase('Wireless Rate Plan') ){
                //productToUpdateMap = getOSTypeFromJSONAttr(prod, DEVICEATTRNAME);
                Product2 prdOSTypeUpdate = getOSTypeFromJSONAttr(prod, RATEPLANATTRNAME);
                if(prdOSTypeUpdate != null ){
                        productToUpdateMap.put(prdOSTypeUpdate.id, prdOSTypeUpdate);
                }
            }
          else if(prod.vlocity_cmt__JSONAttribute__c != null ){
            //productToUpdateMap = getOSTypeFromJSONAttr(prod, DEVICEATTRNAME);
            Product2 prdOverlineUpdate = getOSTypeFromJSONAttr(prod, LINETYPE);
            if(prdOverlineUpdate != null ){
                productToUpdateMap.put(prdOverlineUpdate.id, prdOverlineUpdate);
            }
          }
          else if(prod.Name != null && prod.Name.startsWithIgnoreCase('Waive') && prod.RelationshipType__c != null && prod.RelationshipType__c.equalsIgnoreCase('Promotion')){
                //productToUpdateMap = getOSTypeFromJSONAttr(prod, DEVICEATTRNAME);
                Map<String,String> waivetypeMap = new Map<String,String>{'Waive' => 'WAIVE PROMO'};
                    prod.VLAdditionalConfigData__c = JSON.serialize(waivetypeMap);
                Product2 waiveProdUpdate = prod;
                productToUpdateMap.put(waiveProdUpdate.id, waiveProdUpdate);
            }
        } */
        //Adharsh:: JSON Reference Comment End

        System.debug('productToUpdateMap_____####' + productToUpdateMap.size());
        if( productToUpdateMap.size() > 0 && !productToUpdateMap.isEmpty() ) {     
                    outMap.put('productToUpdateMap',productToUpdateMap);
                }
        return outMap;
    }

/* Adharsh: Attribute Assignment will drive the value for VLAdditionalConfigData__c. Dont need this method 
    public static Product2 getOSTypeFromJSONAttr( Product2 currentProd, String attrName){
           //system.debug('______LINETYPE' +attrName);
            Map<Id,Product2> productUpdateMap = new Map<Id,Product2>();
            Product2 productToUpdate = new Product2();
                if(currentProd != null && currentProd.vlocity_cmt__JSONAttribute__c != null ){
                        Map<String,OCOM_VlocityJSONParseUtil.JSONWrapper>  attributeMap = OCOM_VlocityJSONParseUtil.getAttributesFromJSON(currentProd.vlocity_cmt__JSONAttribute__c);

                        if(null != attributeMap && attributeMap.size()>0 && attributeMap.containskey(attrName)){
                                    OCOM_VlocityJSONParseUtil.JSONWrapper attrNameValue = attributeMap.get(attrName);
                                        String OSType = attrNameValue.value;
                                       // system.debug('OSType_____' + OSType );
                                        if(OSType != null && OSType != ''){
                                        map<String,String> OStypeMap = new Map<String,String>{attrName => OSType};
                                        currentProd.VLAdditionalConfigData__c = JSON.serialize(OStypeMap);
                                            //currentProd.ProductCode = OSType;
                                            //productUpdateMap.put(currentProd.id,currentProd);
                                            productToUpdate = currentProd;
                                        }                         
                            }
                }
                return productToUpdate;
    }  
    */

// Adharsh: Adding logic to query Attribut Assignment based on Attribute list. 
      public static Map <Id, String> getAttrAssignmnt(Set<String> attrNameSet)  {
        Map<Id,String> prodIdToVLAddlConfigData = new Map<Id,String>();
      
        for (vlocity_cmt__AttributeAssignment__c attrAssignment :[Select Id,name,vlocity_cmt__ObjectId__c,vlocity_cmt__ValueInNumber__c,vlocity_cmt__Value__c   
                                                              from vlocity_cmt__AttributeAssignment__c where 
                                                               Name in :attrNameSet ]){
                    if(attrAssignment.vlocity_cmt__ObjectId__c != null && attrAssignment.vlocity_cmt__Value__c != null &&attrAssignment.vlocity_cmt__Value__c != '') {
                         Map<String,String> OStypeMap = new Map<String,String>{attrAssignment.name => attrAssignment.vlocity_cmt__Value__c};
                         String vladditionalConfigData = JSON.serialize(OStypeMap);
                         prodIdToVLAddlConfigData.put(attrAssignment.vlocity_cmt__ObjectId__c, vladditionalConfigData);

                    }
        }
        return prodIdToVLAddlConfigData;
    }                                                            
                
    
}