/*
###########################################################################
# File..................: MyException
# Version...............: 1
# Created by............: Vinay Sharma
# Created Date..........: 06/03/2014
# Last Modified by......: Vinay Sharma
# Last Modified Date....: 06/03/2014
# Description...........: This class is used for throws custom Exception
#
# Copyright (c) 2000-2012. Astadia, Inc. All Rights Reserved.
#
# Created by the Astadia, Inc. Modification must retain the above copyright notice. 
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any commercial purpose, without fee, and without a written
# agreement from Astadia, Inc., is hereby forbidden. Any modification to source
# code, must include this paragraph and copyright.
#
# Permission is not granted to anyone to use this software for commercial uses.
#
# Contact address: 2839 Paces Ferry Road, Suite 350, Atlanta, GA 30339
# Company URL : http://www.astadia.com
###########################################################################
*/
public class MyException extends Exception {}