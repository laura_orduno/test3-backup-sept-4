/* ----------------------------------------------------------------------------------------
------------This is to provide test data for al test classes------------------------------
------------------------------------------------------------------------------------------*/
@isTest(SeeAllData=false)
public  class FOBO_Test_Util {
    
    public static Account createAccount() {
        Account acc = new Account(Name='Testing Software', BillingCountry = 'US', BillingState = 'IL');
        insert acc;
        return acc;
    }
    public static Contact createContact(){
        Account acc = createAccount();
        Contact c = new Contact(FirstName = 'Test firstName', LastName = 'Test Last Name', Email = 'test@test.com', Phone = '123456789', Title = 'Developer', AccountId = acc.Id);
        insert c;
        return c;
    }
    public static Contact createContactFromAccount(Id Acc){
        //Account acc = createAccount();
        Contact c = new Contact(FirstName = 'Test firstName', LastName = 'Test Last Name', Email = 'test@test.com', Phone = '123456789', Title = 'Developer', AccountId = acc);
        insert c;
        return c;
    }
    public static Opportunity createOpportunity(){
        Account acc = createAccount();
        Opportunity Oppt = new Opportunity();
        Oppt.AccountId = acc.Id;
        Oppt.Name = 'Opportunity for ' + acc.Name;
        Oppt.CloseDate = Date.today();
        Oppt.StageName = 'I have a Potential Opportunity (20%)';
        Oppt.Order_Type__c = 'All Other Orders' ;
        insert Oppt;
        return Oppt;
    }
    /*public static Order_Request__c createOrderRequest(){
        
        Opportunity Oppt = createOpportunity();
        Order_Request__c rq = new Order_Request__c();
        Contact c = createContact();
        rq.Opportunity__c = Oppt.Id;
        rq.Account__c = Oppt.AccountId;
       // rq.Primary_Contact__c = c.Id;
        insert rq;
        return rq;
    }*/
    public static FOBOChecklist__c createFobocheckList(){
        Opportunity rq = createOpportunity();
        FOBOChecklist__c fcheck = new FOBOChecklist__c();
        fcheck.Opportunity__c= rq.Id;
        insert fcheck;
        return fcheck;
    }
    public static SMBCare_Address__c createserviceAddress(){
        Account acc = createAccount();
        SMBCare_Address__c sr = new SMBCare_Address__c();
        sr.Account__c = acc.Id;
        sr.Unit_Number__c = '100';
        sr.Street__c = '25';
        sr.Street_Address__c = 'york st';
        sr.City__c = 'Toronto';
        sr.State__c = 'ON';
        sr.Country__c = 'Canada';
        insert sr;
        return sr;
        
    }
    public static SMBCare_Address__c createserviceAddressFroAcc(Id Acc){
        //Account acc = createAccount();
        SMBCare_Address__c sr = new SMBCare_Address__c();
        sr.Account__c = acc;
        sr.Unit_Number__c = '100';
        sr.Street__c = '25';
        sr.Street_Address__c = 'york st';
        sr.City__c = 'Toronto';
        sr.State__c = 'ON';
        sr.Country__c = 'Canada';
        insert sr;
        return sr;
        
    }
}