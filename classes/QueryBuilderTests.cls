/**
 * Unit test suite for QueryBuilder class.
 * 
 * @author Max Rudman
 * @since 7/22/2009
 */
@isTest
private class QueryBuilderTests {
	testMethod static void testSimple() {
		QueryBuilder target = new QueryBuilder('Account');
		target.getSelectClause().addField('Id');
		target.getSelectClause().addField('Name');
		
		System.assertEquals('SELECT Id,Name FROM Account', target.buildSOQL());
	}
	
	testMethod static void testWithSimpleWhere() {
		QueryBuilder target = new QueryBuilder(Account.sObjectType);
		target.getSelectClause().addField(Account.Id);
		target.getSelectClause().addField(Account.Name);
		target.getWhereClause().addExpression(target.lke(Account.Name,'Test%'));
		target.getWhereClause().addExpression(target.includes(Account.Name,new Set<String>{'AA;BB'}));
		target.getWhereClause().addExpression(target.excludes(Account.Name, new Set<String>{'XX'}));
		
		System.assertEquals('SELECT Id,Name FROM Account WHERE Name LIKE \'Test%\' AND Name INCLUDES (\'AA;BB\') AND Name EXCLUDES (\'XX\')', target.buildSOQL());
	}
	
	testMethod static void testWithComplexWhere() {
		QueryBuilder target = new QueryBuilder('Account');
		target.getSelectClause().addField('Id');
		target.getSelectClause().addField('Name');
		target.getWhereClause().addExpression(
				target.land().addExpression(target.lke('Name','Test%')).
				addExpression(target.lor().addExpression(target.lt('EmployeeCount',100)).addExpression(target.gt('EmployeeCount',2000))
				)
		);
		
		System.assertEquals('SELECT Id,Name FROM Account WHERE (Name LIKE \'Test%\' AND (EmployeeCount < 100 OR EmployeeCount > 2000))', target.buildSOQL());
	}
	
	testMethod static void testFull() {
		QueryBuilder target = new QueryBuilder('Account');
		target.getSelectClause().addField('Id');
		target.getSelectClause().addField('Name');
		target.getWhereClause().addExpression(target.lke(Account.Name,'Test%'));
		target.getOrderByClause().addDescending('Name');
		target.getLimitClause().setValue(5);
		
		System.assertEquals('SELECT Id,Name FROM Account WHERE Name LIKE \'Test%\' ORDER BY Name DESC LIMIT 5', target.buildSOQL());
	}
	
	testMethod static void testGroupBy() {
		QueryBuilder target = new QueryBuilder('Account');
		target.getSelectClause().addField(Account.Name);
		target.getSelectClause().addField('COUNT(Id)');
		target.getGroupByClause().addField(Account.Name);
		
		System.assertEquals('SELECT Name,COUNT(Id) FROM Account GROUP BY Name', target.buildSOQL());
	}
}