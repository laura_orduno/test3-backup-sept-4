@isTest
private class ChannelPortalCacheMgmtWSCalloutTest {
    private static void createData(){
        System.runAs(new User(Id = Userinfo.getUserId())) {
            OrdrTestDataFactory.createOrderingWSCustomSettings('ChannelPortalCacheMgmtEndPoint', 'https://webservice1.preprd.teluslabs.net/it02/SPO/PartnerMgmt/ChannelPortalCacheMgmtSvc/ChannelPortalCacheMgmtSvc_v1_0_vs1');
      
        }
    }
    @isTest
    private static void testGetCacheList() {  
        createData();
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        ChannelPortalCacheMgmtSrvReqRes.GetCacheListResponseType response=ChannelPortalCacheMgmtWSCallout.getCacheList('123456');
        ChannelPortalCacheMgmtSrvReqRes.MapItem[] customCacheList=response.customCacheList;
            for(ChannelPortalCacheMgmtSrvReqRes.MapItem mapItemObj:customCacheList){
                System.debug('key='+mapItemObj.key);
                System.debug('value='+mapItemObj.value);
                if(String.isNotBlank(mapItemObj.key) && 'channelOrgSalesReps_JSON'.equalsIgnoreCase(mapItemObj.key)){
                    List<ChannelOrgSalesRepsJSONExplicit> objList = ChannelOrgSalesRepsJSONExplicit.parse(mapItemObj.value);
                    for(ChannelOrgSalesRepsJSONExplicit obj:objList){
                        System.debug(obj.firstName);
                         System.debug(obj.lastName);
                         System.debug(obj.salesRepPin);
                         System.debug(obj.salesRepCategoryKeys);
                    }
                }
                if(String.isNotBlank(mapItemObj.key) && 'channelOrgOutlets_JSON'.equalsIgnoreCase(mapItemObj.key)){
                    List<ChannelOrgOutletsJSONExplicit> objList = ChannelOrgOutletsJSONExplicit.parse(mapItemObj.value);
                    for(ChannelOrgOutletsJSONExplicit obj:objList){
                        System.debug(obj.currentChannelOutletID);
                         System.debug(obj.currentChannelOutletDescription);
                         System.debug(obj.currentOutletCategoryKeys);                         
                    }
                }
            }
        
        Test.stopTest();        
    }
    @isTest
    private static void auxiliaryTest(){
        Test.startTest(); 
        ChannelPortalCacheMgmt obj1=new ChannelPortalCacheMgmt();
        ChannelPortalCacheMgmtBaseExceptionsV3.MessageList obj2=new ChannelPortalCacheMgmtBaseExceptionsV3.MessageList();
        ChannelPortalCacheMgmtBaseExceptionsV3.FaultExceptionDetailsType obj3=new ChannelPortalCacheMgmtBaseExceptionsV3.FaultExceptionDetailsType();
        ChannelPortalCacheMgmtBaseExceptionsV3.Message obj4=new ChannelPortalCacheMgmtBaseExceptionsV3.Message();
        ChannelPortalCacheMgmtCommonTypesV9.OrganizationName ob21=new ChannelPortalCacheMgmtCommonTypesV9.OrganizationName();
        ChannelPortalCacheMgmtCommonTypesV9.Description ob22=new ChannelPortalCacheMgmtCommonTypesV9.Description();
        ChannelPortalCacheMgmtCommonTypesV9.Message ob23=new ChannelPortalCacheMgmtCommonTypesV9.Message();
        ChannelPortalCacheMgmtCommonTypesV9.HealthCard ob24=new ChannelPortalCacheMgmtCommonTypesV9.HealthCard();
        ChannelPortalCacheMgmtCommonTypesV9.PartyName ob25=new ChannelPortalCacheMgmtCommonTypesV9.PartyName();
        ChannelPortalCacheMgmtCommonTypesV9.Name ob26=new ChannelPortalCacheMgmtCommonTypesV9.Name();
        ChannelPortalCacheMgmtCommonTypesV9.MultilingualDescriptionList ob27=new ChannelPortalCacheMgmtCommonTypesV9.MultilingualDescriptionList();
        ChannelPortalCacheMgmtCommonTypesV9.IndividualName ob28=new ChannelPortalCacheMgmtCommonTypesV9.IndividualName();
        ChannelPortalCacheMgmtCommonTypesV9.MarketSegment ob29=new ChannelPortalCacheMgmtCommonTypesV9.MarketSegment();
        ChannelPortalCacheMgmtCommonTypesV9.Passport ob221=new ChannelPortalCacheMgmtCommonTypesV9.Passport();
        ChannelPortalCacheMgmtCommonTypesV9.DriversLicense ob22223=new ChannelPortalCacheMgmtCommonTypesV9.DriversLicense();
        ChannelPortalCacheMgmtCommonTypesV9.CodeDescText ob22425=new ChannelPortalCacheMgmtCommonTypesV9.CodeDescText();
        ChannelPortalCacheMgmtCommonTypesV9.MessageType ob226=new ChannelPortalCacheMgmtCommonTypesV9.MessageType();
        ChannelPortalCacheMgmtCommonTypesV9.AuditInfo ob227=new ChannelPortalCacheMgmtCommonTypesV9.AuditInfo();
        ChannelPortalCacheMgmtCommonTypesV9.UnknownName ob228=new ChannelPortalCacheMgmtCommonTypesV9.UnknownName();
        ChannelPortalCacheMgmtCommonTypesV9.Rate ob229=new ChannelPortalCacheMgmtCommonTypesV9.Rate();
        ChannelPortalCacheMgmtCommonTypesV9.Quantity ob231=new ChannelPortalCacheMgmtCommonTypesV9.Quantity();
        ChannelPortalCacheMgmtCommonTypesV9.BankAccount ob232=new ChannelPortalCacheMgmtCommonTypesV9.BankAccount();
        ChannelPortalCacheMgmtCommonTypesV9.Duration ob233=new ChannelPortalCacheMgmtCommonTypesV9.Duration();
        ChannelPortalCacheMgmtCommonTypesV9.ResponseMessage ob234=new ChannelPortalCacheMgmtCommonTypesV9.ResponseMessage();
        ChannelPortalCacheMgmtCommonTypesV9.MonetaryAmt ob235=new ChannelPortalCacheMgmtCommonTypesV9.MonetaryAmt();
        ChannelPortalCacheMgmtCommonTypesV9.MultilingualNameList ob236=new ChannelPortalCacheMgmtCommonTypesV9.MultilingualNameList();
        ChannelPortalCacheMgmtCommonTypesV9.CreditCard ob237=new ChannelPortalCacheMgmtCommonTypesV9.CreditCard();
        ChannelPortalCacheMgmtCommonTypesV9.MultilingualCodeDescTextList ob238=new ChannelPortalCacheMgmtCommonTypesV9.MultilingualCodeDescTextList();
        ChannelPortalCacheMgmtCommonTypesV9.brandType ob239=new ChannelPortalCacheMgmtCommonTypesV9.brandType();
        ChannelPortalCacheMgmtCommonTypesV9.ProvincialIdCard ob240=new ChannelPortalCacheMgmtCommonTypesV9.ProvincialIdCard();
        ChannelPortalCacheMgmtCommonTypesV9.ResponseStatus ob241=new ChannelPortalCacheMgmtCommonTypesV9.ResponseStatus();
        ChannelPortalCacheMgmtCommonTypesV9.MultilingualCodeDescriptionList ob242=new ChannelPortalCacheMgmtCommonTypesV9.MultilingualCodeDescriptionList();
        ChannelPortalCacheMgmtPingV1.ping_element ob40=new ChannelPortalCacheMgmtPingV1.ping_element();
        ChannelPortalCacheMgmtPingV1.pingResponse_element ob41=new ChannelPortalCacheMgmtPingV1.pingResponse_element();
        ChannelPortalCacheMgmtSrvReqRes.SaveCacheListType obj51=new ChannelPortalCacheMgmtSrvReqRes.SaveCacheListType();
		ChannelPortalCacheMgmtSrvReqRes.SaveCacheListResponseType  obj52=new ChannelPortalCacheMgmtSrvReqRes.SaveCacheListResponseType();
        Test.stopTest();
    }
}