@isTest
private class QuoteCustomConfigurationControllerTests {
    
    private static Web_Account__c account;
    private static Contact contact;
    private static SBQQ__Quote__c quote;
    private static AdditionalInformation__c addl;
    private static SBQQ__QuoteLine__c line;
    
    static testMethod void test() {
        setUp();
        
        ApexPages.currentPage().getParameters().put('selId', 'a:b');
        ApexPages.currentPage().getParameters().put('quoteLineId', line.Id);
        ApexPages.currentPage().getParameters().put('category', 'Wireless');
        ApexPages.currentPage().getParameters().put('qty', '1');
        ApexPages.currentPage().getParameters().put('quoteId', quote.Id);
        QuoteCustomConfigurationController controller = new QuoteCustomConfigurationController();
        controller.qty = 3;
        controller.buildTermPricesString();
        controller.isDealerUser();
        
        controller.initWLS();
        controller.initWLN();
        controller.initExisting();
        controller.initUC();
        controller.initCB();
        controller.initSH();
        controller.initIF();
        controller.initTollFree();
        controller.initExtraWLN();
                
        controller.onSave();
        controller.removeIndex = 0;
        controller.removeAdditionalData();
        QuoteCustomConfigurationController.linkUnassignedAdditionalInformation(quote.Id);
        QuoteCustomConfigurationController.appendAdditionalInfo('foo', 'bar', 'foobar');
    }
    testMethod static void testPasswordChecker() {
        // Too short. Passwords must be between 6 and 8 characters long and contain letters and digits.
        System.assertEquals(false, QuoteCustomConfigurationController.isValidPassword('test'));
        
        // Too long.
        System.assertEquals(false, QuoteCustomConfigurationController.isValidPassword('qwertyuiop'));
        
        // No digits.
        System.assertEquals(false, QuoteCustomConfigurationController.isValidPassword('testing'));
        
        // No letters.
        System.assertEquals(false, QuoteCustomConfigurationController.isValidPassword('1234567'));
        
        // Correct format
        System.assertEquals(true, QuoteCustomConfigurationController.isValidPassword('test1234'));
        
        // Correct format
        System.assertEquals(true, QuoteCustomConfigurationController.isValidPassword('te123t'));
    }
    
    private static void setUp() {
        account = new Web_Account__c(Name='UnitTest', phone__c = '6049969449');
        insert account;
        
        contact = new Contact(LastName='Test',FirstName='Max',Web_Account__c=account.Id, phone='6049969449');
        insert contact;
        
        Opportunity opp = new Opportunity(Name='Test',Web_Account__c=account.Id,CloseDate=System.today(),StageName='Prospecting');
        insert opp;
        
        quote = new SBQQ__Quote__c(SBQQ__Opportunity__c=opp.Id);
        quote.Rate_Band__c = 'F';
        quote.Province__c = 'BC';
        insert quote;

        line = new SBQQ__QuoteLine__c(SBQQ__Quote__c=quote.Id,SBQQ__Bundle__c=true);
        insert line;
        
        addl = new AdditionalInformation__c(Quote__c=quote.Id, Quote_Line__c=line.Id);
        insert addl;
    }
}