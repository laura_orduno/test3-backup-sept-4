/**
 * S2C_OrderCreation
 *
 * @description Clones existing opportunity to a different record type
 *              of order.
 *
 * @author Thomas Tran, Traction on Demand
 * @date 01-29-2015
 * @lastmodifiedby Thomas Tran, Traction on Demand
 * @lastmodifiedbydate 02-02-2015
 * @storynumber LP ID: 19901617
 */
global without sharing class S2C_OrderCreation {
	private static final String ORDER_RECORD_DEVELOPER_NAME              = 'SRS_Order_Request';
	private static final String PREQUAL_RECORD_DEVELOPER_NAME            = 'Prequal';
	private static final String COMPLEX_ORDER_TYPE                       = 'Complex Order';
	private static final String ORIGINATED_STAGE_NAME                    = 'Originated';
	private static final String PROVIDE_INSTALL_DEVELOPER_NAME           = 'Provide';
	private static final String DISCONNECT_DEVELOPER_NAME                = 'Disconnect';
	private static final String PROVIDE_INSTALL_DDL_VALUE                = 'Provide/Install';
	private static final String PREQUAL_DDL_VALUE                        = 'Prequal';
	private static final String CHANGE_DDL_VALUE                         = 'Change';
	private static final String DISCONNECT_DDL_VALUE                     = 'Disconnect';
	private static final String MOVE_DDL_VALUE                           = 'Move';
	private static final String RECORDS_ONLY_DDL_VALUE                   = 'Records Only';
	private static Map<String, Id> developerNameToRecordTypeId           = new Map<String, Id>();
	private static Map<Id, OpportunityProductItemInfo> opiIdToOpiInfoMap = new Map<Id, OpportunityProductItemInfo>();

	/**
	 * Generate order opportunity from sales opportunity.
	 * @param  Id
	 * @return String
	 * 
	 * @author Thomas Tran, Traction on Demand
	 * @date 01-29-2015
	 */
	webservice static String createOrder(Id opportunityId, String opiJSON){
		opiIdToOpiInfoMap = createOPIInfoMap(opiJSON);

		Opportunity newOrderOpportunity;
		String oppURL= '';
		List<String> developerNameList = new List<String>{ORDER_RECORD_DEVELOPER_NAME};

		for(S2C_Service_Request_Creation_Order_Types__c currentTypeTemp : [SELECT Developer_Name__c FROM S2C_Service_Request_Creation_Order_Types__c]){
			developerNameList.add(currentTypeTemp.Developer_Name__c);
		}

		Opportunity salesOpportunity = [
			SELECT Id, AccountId, OwnerId, Name, Credit_Assesment__c, Order_Opportunity__c, (SELECT Product_Family__c FROM Opportunity_Product_Items__r)
			FROM Opportunity
			WHERE Id = :opportunityId
		];
		
		for(RecordType currentRecordTypeInfoTemp : [SELECT Id, DeveloperName
													FROM RecordType
													WHERE DeveloperName IN :developerNameList]){
			developerNameToRecordTypeId.put(currentRecordTypeInfoTemp.DeveloperName, currentRecordTypeInfoTemp.Id);
		}

		SYstem.debug('RT ID :::: ' + developerNameToRecordTypeId);

		if(salesOpportunity != null){
			newOrderOpportunity = cloneOpportunity(salesOpportunity);
		}

		if(newOrderOpportunity != null){
			oppURL = '/' + newOrderOpportunity.Id;
			salesOpportunity.Order_Opportunity__c = newOrderOpportunity.Id;

			update salesOpportunity;
		}

		return oppURL;
	}

	/**
	 * Clones the sales opportunity to the order opportunity
	 * @param  Opportunity
	 * @return Opportunity
	 *
	 * @author Thomas Tran, Traction on Demand
	 * @date 01-29-2015
	 */
	private static Opportunity cloneOpportunity(Opportunity salesOpportunity){
		Opportunity newOrderOpportunity;
		Id primaryContactRoleId;

		List<OpportunityContactRole> primaryContactList = [
			SELECT ContactId
			FROM OpportunityContactRole
			WHERE OpportunityId = :salesOpportunity.Id AND IsPrimary = true
		];

		if(primaryContactList.size() > 0){
			primaryContactRoleId = primaryContactList[0].ContactId;
		}

		newOrderOpportunity = new Opportunity(
			AccountId = salesOpportunity.AccountId,
			//OwnerId = salesOpportunity.OwnerId,
			Name = salesOpportunity.Name,
			Primary_Order_Contact__c = primaryContactRoleId,
			Type = COMPLEX_ORDER_TYPE, 
			RecordTypeId = developerNameToRecordTypeId.get(ORDER_RECORD_DEVELOPER_NAME),
			StageName = ORIGINATED_STAGE_NAME,
			CloseDate = Date.today(),
			Sales_Opportunity_Id__c = String.valueOf(salesOpportunity.Id),
//			Sales_Opportunity__c = String.valueOf(salesOpportunity.Id),
			Credit_Assesment__c = salesOpportunity.Credit_Assesment__c
		);

		insert newOrderOpportunity;

		System.debug('trac_debug >>>> ' + newOrderOpportunity);
		System.debug('trac_debug >>>> ' + salesOpportunity.Opportunity_Product_Items__r);
		createServiceRequest(newOrderOpportunity, salesOpportunity.Opportunity_Product_Items__r);

		return newOrderOpportunity;
	}

	/**
	 * Clones the sales opportunity service request onto the order opportunity
	 * @param  Opportunity
	 * @return 
	 *
	 * @author Thomas Tran, Traction on Demand
	 * @date 01-30-2015
	 */
	private static void createServiceRequest(Opportunity orderOpportunity, List<Opp_Product_Item__c> productItems){
		Service_Request__c newServiceRequest;
		Service_Request__c disconnectServiceRequest;
		Service_Request__c provideInstallServiceRequest;
		List<ServiceRequestPair> pairList = new List<ServiceRequestPair>();
		List<Service_Request__c> serviceRequestToInsert = new List<Service_Request__c>();
		List<RecordType> refNumberRecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'SRS_Service_Request_Related_Order__c' AND DeveloperName = 'Reference_Number'];
		Id referenceNumberRecordTypeId = refNumberRecordTypeId[0].Id;
		OpportunityProductItemInfo currentOPIInfo;

		for(Opp_Product_Item__c currentOppProductItemTemp : productItems){
			currentOPIInfo = opiIdToOpiInfoMap.get(currentOppProductItemTemp.Id);

			if(currentOPIInfo.createSR){
				if(MOVE_DDL_VALUE.equals(currentOPIInfo.orderType)){
					ServiceRequestPair currentPair = new ServiceRequestPair(orderOpportunity, currentOPIInfo, developerNameToRecordTypeId.get(DISCONNECT_DEVELOPER_NAME), developerNameToRecordTypeId.get(PROVIDE_INSTALL_DEVELOPER_NAME));
					pairList.add(currentPair);
					serviceRequestToInsert.add(currentPair.disconnectSR);
					serviceRequestToInsert.add(currentPair.provideInstallSR);

				} else{
					newServiceRequest = new Service_Request__c(
						Account_Name__c = orderOpportunity.AccountId,
						Opportunity__c = orderOpportunity.Id,
						Target_Date__c = currentOPIInfo.dateValue,
						PrimaryContact__c = orderOpportunity.Primary_Order_Contact__c,
						Opportunity_Product_Item__c = currentOPIInfo.opiId,
						RecordTypeId = developerNameToRecordTypeId.get(currentOPIInfo.orderType),
						Target_Date_Type__c = 'Best Effort'
					);

                    if(!currentOPIInfo.orderType.equals(DISCONNECT_DDL_VALUE)){
						newServiceRequest.Contract_Length__c = Double.valueOf(currentOPIInfo.contractLength);
					} else{
						newServiceRequest.Contract_Length__c = 0;
					}

					if(currentOPIInfo.srsProductId != null){
						newServiceRequest.SRS_PODS_Product__c = currentOPIInfo.srsProductId;
					}

					serviceRequestToInsert.add(newServiceRequest);
				}
			}
		}

		if(!serviceRequestToInsert.isEmpty()){
			insert serviceRequestToInsert;

			if(!pairList.isEmpty()){
				List<SRS_Service_Request_Related_Order__c> srRelatedOrderList = new List<SRS_Service_Request_Related_Order__c>();

				for(ServiceRequestPair currentSRPTemp : pairList){
					srRelatedOrderList.addAll(currentSRPTemp.createSRLink(referenceNumberRecordTypeId));
				}

				if(!srRelatedOrderList.isEmpty()){
					insert srRelatedOrderList;
				}
			}
            
            // create a corresponding onsite contact for each newly inserted service request
            list<id> serviceRequestToInsertIds=new list<id>();
            for(service_request__c sr:serviceRequestToInsert){
                serviceRequestToInsertIds.add(sr.id);
            }
            ServiceRequestContactCallback.onServiceRequestsCreated(serviceRequestToInsertIds);
            BillabeChargeCallback.onServiceRequestsCreated(serviceRequestToInsertIds);
		}
	}

	/**
	 * Creates a map based on the opp product item information gathered from the page.
	 * @param  String
	 * @return Map<Id, OpportunityProductItemInfo>
	 *
	 * @author Thomas Tran, Traction on Demand
	 * @date 02-17-2015
	 */
	private static Map<Id, OpportunityProductItemInfo> createOPIInfoMap(String opiJson){
		List<OpportunityProductItemInfo> opiInfoList = (List<OpportunityProductItemInfo>)JSON.deserialize(opiJson, List<OpportunityProductItemInfo>.class);
		Map<Id, OpportunityProductItemInfo> opiInfoIdToOpiInfoMap = new Map<Id, OpportunityProductItemInfo>();

		for(OpportunityProductItemInfo currentOpportunityProductItemInfoTemp : opiInfoList){
			opiInfoIdToOpiInfoMap.put(currentOpportunityProductItemInfoTemp.opiId, currentOpportunityProductItemInfoTemp);
		}

		return opiInfoIdToOpiInfoMap;
	}

	/**
	 *	OpportunityProductItemInfo
	 * 
	 * @description Object to help group the data gathered on SRS Product selection screen.
	 *
	 * @author Thomas Tran, Traction on Demand
	 * @date 02-17-2015
	 */
	public class OpportunityProductItemInfo{
		public Id opiId {get; set;}
		public String opiRecName {get; set;}
		public String opiName {get; set;}
		public String orderType {get; set;}
		public Id srsProductId {get; set;}
		public String contractLength {get; set;} 
		public Date dateValue {get; set;}
		public Boolean createSR {get; set;}

		public OpportunityProductItemInfo(Id opiId, String opiRecName, String opiName, String orderType, String contractLength, Date dateValue, String createSR){
			this.opiId = opiId;
			this.opiRecName = opiRecName;
			this.opiName = opiName;
			this.orderType = orderType;
			this.srsProductId = null;
			this.contractLength = contractLength;
			this.dateValue = dateValue;
			this.createSr = Boolean.valueOf(createSR);
		}

		public OpportunityProductItemInfo(Id opiId, String opiRecName, String opiName, String orderType, Id srsProductId, String contractLength, Date dateValue, String createSR){
			this.opiId = opiId;
			this.opiRecName = opiRecName;
			this.opiName = opiName;
			this.orderType = orderType;
			this.srsProductId = srsProductId;
			this.contractLength = contractLength;
			this.dateValue = dateValue;
			this.createSr = Boolean.valueOf(createSR);
		}
	}

	/**
	 * ServiceRequestPair
	 * @description Inner class to help pair the servicve request that was created for Move.
	 * @author Thomas Tran, Traction on Demand
	 * @date 07-07-2015
	 */
	public class ServiceRequestPair{
		public Service_Request__c disconnectSR {get; set;}
		public Service_Request__c provideInstallSR {get; set;}

		/**
		 * ServiceRequestPair Constructor
		 * @description Creates the disconnect and provide install service request.
		 * @author Thomas Tran, Traction on Demand
		 * @date 07-07-2015
		 */
		public ServiceRequestPair(Opportunity orderOpportunity, OpportunityProductItemInfo currentOPIInfo, Id disconnectRT, Id provideInstallRT){
			disconnectSR = new Service_Request__c(
				Account_Name__c = orderOpportunity.AccountId,
				Opportunity__c = orderOpportunity.Id,
				Target_Date__c = currentOPIInfo.dateValue,
				PrimaryContact__c = orderOpportunity.Primary_Order_Contact__c,
				Opportunity_Product_Item__c = currentOPIInfo.opiId,
				RecordTypeId = disconnectRT,
				Contract_Length__c = 0,
				Description__c = orderOpportunity.Name + ' - Move',
				Target_Date_Type__c = 'Best Effort'
			);

			provideInstallSR = new Service_Request__c(
				Account_Name__c = orderOpportunity.AccountId,
				Opportunity__c = orderOpportunity.Id,
				Target_Date__c = currentOPIInfo.dateValue,
				PrimaryContact__c = orderOpportunity.Primary_Order_Contact__c,
				Opportunity_Product_Item__c = currentOPIInfo.opiId,
				RecordTypeId = provideInstallRT,
				Contract_Length__c = Double.valueOf(currentOPIInfo.contractLength),
				Description__c = orderOpportunity.Name + ' - Move',
				Target_Date_Type__c = 'Best Effort'
			);

			if(currentOPIInfo.srsProductId != null){
				disconnectSR.SRS_PODS_Product__c = currentOPIInfo.srsProductId;
				provideInstallSR.SRS_PODS_Product__c = currentOPIInfo.srsProductId;
			}
		}

		/**
		 * createSRLink
		 * @description Creates the disconnect and provide install service request.
		 * @author Thomas Tran, Traction on Demand
		 * @date 07-07-2015
		 */
		public List<SRS_Service_Request_Related_Order__c> createSRLink(Id referenceNumberRecordTypeId){
			SRS_Service_Request_Related_Order__c disconnectSRToProvideInstallSR;
			SRS_Service_Request_Related_Order__c provideInstallSRTodisconnectSR;
			List<SRS_Service_Request_Related_Order__c> linksToInsert = new List<SRS_Service_Request_Related_Order__c>();
			Service_Request__c outSRName = [SELECT Name FROM Service_Request__c WHERE Id = :disconnectSR.Id];
			Service_Request__c inSRName = [SELECT Name FROM Service_Request__c WHERE Id = :provideInstallSR.Id];

			disconnectSRToProvideInstallSR = new SRS_Service_Request_Related_Order__c();
			disconnectSRToProvideInstallSR.RecordTypeId = referenceNumberRecordTypeId;
			disconnectSRToProvideInstallSR.Name = outSRName.Name;
			disconnectSRToProvideInstallSR.Reference_Type__c = 'Related Serv Req';
			disconnectSRToProvideInstallSR.RO_Service_Request__c = provideInstallSR.Id;
			disconnectSRToProvideInstallSR.Comments__c = 'Disconnect – MOVE';

			linksToInsert.add(disconnectSRToProvideInstallSR);

			provideInstallSRTodisconnectSR = new SRS_Service_Request_Related_Order__c();
			provideInstallSRTodisconnectSR.RecordTypeId = referenceNumberRecordTypeId;
			provideInstallSRTodisconnectSR.Name = inSRName.Name;
			provideInstallSRTodisconnectSR.Reference_Type__c = 'Related Serv Req';
			provideInstallSRTodisconnectSR.RO_Service_Request__c = disconnectSR.Id;
			provideInstallSRTodisconnectSR.Comments__c = 'Provide/Install – MOVE';

			linksToInsert.add(provideInstallSRTodisconnectSR);

			return linksToInsert;

		}
	}

}