global class smb_SVOCControllerTC {
      // Changes as per TT Aggregation
        public transient list<TicketVO.TicketData> lstTickets {get;set;}
        TicketVO.ServiceRequestAndResponseParameter reqResObj = new TicketVO.ServiceRequestAndResponseParameter();
        public List<TicketVO.TicketData> matchedInternalCases {get; set;}
      // End
      public Account acct {get; set;}
      public List<Case> matchedCases {get; set;}
          public String tID {get;set;}    
          public String RCID {get;set;}
    
          public String pageInstanceId  {
          get {
                  if (pageInstanceId == null) {
                          pageInstanceId = smb_GuidUtil.NewGuid();
                  }
                  return pageInstanceId;
          }
          private set;
      }                   
    
      public Boolean AllowCreationOfRelatedRecords {
            get {
                if (acct == null) return false;
                
                if (acct.RecordType == null) return false;
                
                if (acct.RecordType.DeveloperName == 'RCID' || acct.RecordType.DeveloperName == 'New_Prospect_Account') {
                    return true;
                }
                
                return false;
            }
        }
          
        global transient List<Id> AllAccountIdsInHierarchy {get;set;}
        
        public PageReference init() {
                // TT Aggregation changes
                lstTickets = new list<TicketVO.TicketData>();
                // end           
                TicketVO ttDetailObj = new TicketVO();
                Id accountId = ApexPages.currentPage().getParameters().get('id');
                
                acct = getAccount(accountId);
                
                allAccountIdsInHierarchy = smb_AccountUtility.getAccountsInHierarchy(accountId, acct.ParentId);
                
                tID = [select Team_TELUS_ID__c from User where id = :Userinfo.getUserId()].Team_TELUS_ID__c;    
                //added Origin to matahcedCases by Rob 10/19/2017
                matchedCases = [SELECT Id, CaseNumber, toLabel(RecordType.Name), toLabel(Type), Subject, Origin, toLabel(Status), toLabel(Priority), CreatedDate, Escalation__c, 
                                MilestoneStatus   
                                FROM Case 
                                WHERE (AccountId IN :allAccountIdsInHierarchy OR (Master_Account_Name__c =:accountid AND (RecordType.DeveloperName = 'TBO_Request' OR RecordType.DeveloperName = 'Legal_Name_Correction')))    // Updated filter criteria as part of TBO project                                              
                                ORDER BY smb_PrioritySortOrder__c DESC, CreatedDate DESC, Status DESC limit 1000];
                // TT Aggregation Changes
                matchedInternalCases = new List<TicketVO.TicketData>();
                if(matchedCases != null && matchedCases.size()>0){
                    for(Case caseObj: matchedCases){
                        TicketVO.TicketData ttObj = new TicketVO.TicketData();
                        ttObj.TELUSTicketId = caseObj.CaseNumber;
                        ttObj.RelatedTicketId = caseObj.Id;
                        ttObj.sourceTypeCode = caseObj.RecordType.Name;
                        ttObj.comments = caseObj.Subject;
                        ttObj.CaseOrigin = caseObj.Origin;
                        ttObj.ticketTypeCode = caseObj.Type;
                        ttObj.statusCode = caseObj.Status;
                        ttObj.priorityCode = caseObj.Priority;
                        try{
                            if(caseObj.CreatedDate != null){
                                String mon = smb_DateUtility.getMonthName(caseObj.CreatedDate.month());
                                ttObj.createDateStr = mon + ' ' + caseObj.CreatedDate.day()+ ', ' + caseObj.CreatedDate.year();
                            }
                        }catch(Exception ex){}
                        ttObj.escalation = caseObj.Escalation__c;
                        ttObj.milestoneStatus = caseObj.MilestoneStatus;
                        matchedInternalCases.add(ttObj); 
                    }
                }                
                // End
                return null;
        }
 
        private static Account getAccount(Id accountId) {
            if (accountId == null) return null;
            
            return [SELECT  Id, ParentId, Name, RCID__c, RecordTypeId, RecordType.DeveloperName
                    FROM Account 
                    WHERE Id =: accountId limit 1000];
        }

        @RemoteAction
        global static List<smb_TTODS_troubleticket.TroubleTicket> getTroubleTickets(string tID, string RCID){ 
                if (string.isBlank(tID) || string.isBlank(RCID)) return null;

                smb_TTODS_CalloutHelper smbCallOut = new smb_TTODS_CalloutHelper();
                list<smb_TTODS_troubleticket.TroubleTicket> lstTT = new list<smb_TTODS_troubleticket.TroubleTicket>();  //TID: T1234567  RCID: 0003206700 
                lstTT  = smbCallOut.getTicketsByQueryCall(TID ,RCID );
                 if(lstTT != null){
                 for(smb_TTODS_troubleticket.TroubleTicket ttods:lstTT){
                        ttods.comments = abbreviate(ttods.comments, 45);
                 }
                }               
                return lstTT;           
        }
        
        // Sandip - TT Aggegation Changes - Added on 24 March 2016
        // This is call back method for implementation of continuation for trouble tickets
        public Object processTTAggregationResponse(){
            try{
                system.debug('@@@@In response');
                lstTickets = new list<TicketVO.TicketData>();
                smb_Ticket_Mapping_Helper.doMapping(reqResObj);
                list<TicketVO.TicketData> lstTT = reqResObj.lstTickets; // get the aggregate list of tickets
                Integer troubleTicketCount = lstTT.size();
                System.debug('##@@@ In response troubleTicketCount ' + troubleTicketCount);  
                Integer recCounter = 0;
                // for loop to break the loop once limit reached
                if(troubleTicketCount != 0){
                    lstTT.sort(); // sort the list
                    for(TicketVO.TicketData ttods:lstTT){
                        //if(recCounter >= reqResObj.recordLimits) break; // break the loop if record limit is reached
                        recCounter ++;
                        ttods.comments = abbreviate(ttods.comments, 45);
                        lstTickets.add(ttods);                      
                    }
                }
            }catch(Exception ex){
                 smb_Ticket_Aggregator.logError('smb_SVOCController.getAggregateTickets', '' , ex.getmessage(), ex.getstacktracestring());
            }
            return null;
        }
        public Continuation getAggregateTickets(){
            Continuation con = new Continuation(TicketVO.TIMEOUT_INT_SECS);
            try{
                smb_Ticket_Aggregator ttObj = new smb_Ticket_Aggregator();
                // set the input parameter which need pass to aggrgate method
                reqResObj.TIDValue = tID;
                reqResObj.RCIDValue = acct.RCID__c;
                reqResObj.accountId = acct.Id;
                reqResObj.callBackMethodName = 'processTTAggregationResponse'; // Specify callback method name, this method should be present in VF controller
                reqResObj.recordLimits = Integer.valueOf(Apexpages.currentPage().getParameters().get('recLimit'));
                reqResObj.upToXLatestModified = Integer.valueOf(Apexpages.currentPage().getParameters().get('upToXLatestModified'));
                // Invoke method to get aggrigate tickets
                con = ttObj.getTroubleTickets(reqResObj);
            }catch(Exception ex){
                 smb_Ticket_Aggregator.logError('smb_SVOCController.getAggregateTickets', '' , ex.getmessage(), ex.getstacktracestring());
            }
            // Return continuation object to VF page so that callback method will get invoked after all callout executed
            return con;
        }
       // End 

        private static string abbreviate(string text, integer length) {
            if (string.isBlank(text)) return '';
            return text.abbreviate(length);
        }   

        public String baseUrl {
        get {
            // Changes as per in Mydomain imlplementation
        	//return 'https://' + URL.getSalesforceBaseUrl().getHost().substring(2,6) + '.salesforce.com';
        	String inst = URLMethodUtility.getInstanceName();
        	return 'https://' + inst + '.salesforce.com';
        }
    }
}