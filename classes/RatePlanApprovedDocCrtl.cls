/**
 * Controller Class for the Visualforce RatePlanApprovedDocPage 
 * Prints out Different Types of Rate Plan and a Grid Format.
 * This class provides mapping between Plan Section names and Field Set name. Also provieds the order of 
 * the sections display in the document.
 * 
 * @Author :Prishan Fernando
 * @Created on : Sept 25 2015
 */ 


public class RatePlanApprovedDocCrtl {
    
    public Set<String> sectionOrder {get;set;}

    public Map<String,List<Rate_Plan_Item__c>> ratePlansSubByType {get;set;}
    
    public Map<String,DealSupportRatePlanGrid__c> fieldsSetNamesMap {get;set;}
    
    private Static String ObjectName ='Rate_Plan_Item__c';
    
    private Static String FieldSetName ='Grid_Summary_Fields';

    private Static String SummadyFieldSetName ='AprvlDocFields';
    
    public Offer_House_demand__c dealSupport {get;set;}
    
    public Integer numberOfChurnsAlot {get;set;}
    
    public Integer numberOfHardwareAlot {get;set;}
    
    public RatePlanApprovedDocCrtl(ApexPages.StandardController ctrl){
        try{
            sectionOrder = new Set<String>();
            initFieldSetMapping();         
          	dealSupport = (Offer_House_demand__c) ctrl.getRecord();
            id recId =dealSupport.id;
          	numberOfChurnsAlot = dealSupport.Agreement_Churn_Allotments__r.size(); 
            numberOfHardwareAlot = dealSupport.Agreement_hardware_Allotments__r.size();
            String query =  Util.queryBuilder('Offer_House_demand__c',SummadyFieldSetName, new List<String>{'id =:recId'});
        	dealSupport =(Offer_House_demand__c) Database.query(query);
            initData(dealSupport.id);
            initshowSections();
        }catch(ConfigurationErrorException e) {
            System.debug(LoggingLevel.ERROR,'ConfigurationErrorException ');
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL,e.getMessage());
            ApexPages.addMessage(msg);
        }    
    }
    
    private void initFieldSetMapping(){
        fieldsSetNamesMap = new Map<String,DealSupportRatePlanGrid__c>();
        List<DealSupportRatePlanGrid__c>  planFieldSetMapping =  [Select Name, Field_Set_Name__c,Type__c,Display_Order__c 
                                                                 from  DealSupportRatePlanGrid__c ORDER BY Display_Order__c];
        if(planFieldSetMapping==null || planFieldSetMapping.isEmpty()){
            throw new ConfigurationErrorException('Custom Setting DealSupportRatePlanGrid has not been setup. Please Ask your admin to check the Custom Setting Data for Plan Type and FieldSet mappings');
        }
        validateFieldSetNames(planFieldSetMapping);
        for(DealSupportRatePlanGrid__c cusSetting : planFieldSetMapping){
            fieldsSetNamesMap.put(cusSetting.Name, cusSetting);
            sectionOrder.add(cusSetting.Name);
        }
    }
    
    private void initData(String dealSupport){
        	Id dealid = dealSupport;
            List<String> queryFields = Util.getFieldSetFields(ObjectName, FieldSetName);
            String query =  Util.queryBuilder(ObjectName,queryFields, new List<String>{'Deal_Support__c =:dealid'} ,
                                              new List<String>{'RP_Monthly_Plan_Rate__c'});
            List<Rate_Plan_Item__c> ratePlanItems = (List<Rate_Plan_Item__c>) Database.query(query);
            ratePlansSubByType = new Map<String,List<Rate_Plan_Item__c>>();
        	
            for(Rate_Plan_Item__c rp : ratePlanItems){
                String planType = rp.Plan_Type__c;
                if(ratePlansSubByType.containsKey(planType)){
                    formatTandC(rp);
                    formatIncludedFeatures(rp);
                    ratePlansSubByType.get(planType).add(rp);
                }else{
                  List<Rate_Plan_Item__c> plans = new List<Rate_Plan_Item__c>();
                  formatTandC(rp);
                  formatIncludedFeatures(rp);
                  plans.add(rp);
                  ratePlansSubByType.put(planType, plans);
                }
            }
       
    }
    
    private void formatTandC(Rate_Plan_Item__c rp){
        String tandc = rp.RP_Terms_and_Conditions__c;
        if(tandc!=null){
        	rp.RP_Terms_and_Conditions__c= tandc.replaceAll('\n','<br/>');
        }
    }
    
    private void formatIncludedFeatures(Rate_Plan_Item__c rp){
        String features = rp.RP_Included_Features__c;
        if(features!=null){
        	rp.Included_Features_All__c = features.replaceAll(';','<br/>') + rp.RP_Included_Features_Other__c;
        }
    }
    
    private void initshowSections(){
        for(String secKeys :sectionOrder){
            if(!ratePlansSubByType.containsKey(secKeys)){
                sectionOrder.remove(secKeys);
            }
        }
        
    }
    
    private void validateFieldSetNames(List<DealSupportRatePlanGrid__c> custsetting){
        Set<String> avilablefSames= Util.getObjectTypeDescribe(ObjectName).FieldSets.getMap().keySet();
        List<String> notDefined = new List<String>();
        for(DealSupportRatePlanGrid__c s :custsetting){
            if(!(avilablefSames.contains(s.Field_Set_Name__c.toLowerCase()))){
                notDefined.add(s.Field_Set_Name__c);
            }
        }
        if(!notDefined.isEmpty()){
            throw new ConfigurationErrorException('Field Sets Not create for Object Rate_Plan_Item__c :' + String.join(notDefined, ' , '));
        }
    }
    
   public class ConfigurationErrorException extends Exception {
      
   }
               

}