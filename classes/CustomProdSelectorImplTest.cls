@isTest
private class CustomProdSelectorImplTest {

    @isTest 
    private static void testInvokeMethod1() {
        Test.startTest();

        generateData();
        String methodName = 'getAllProducts';
        Map<String, Object> input = new Map<String, Object>();
        Map<String, Object> output = new Map<String, Object>();
        input.put('Pricebook2Id', Test.getStandardPricebookId());
        input.put('includeAttributes',true);
        List<String>  fieldsToReturn = new List<String>();
        fieldsToReturn.add('Product2Id');
        fieldsToReturn.add('UnitPrice');
        fieldsToReturn.add('Product2.Name');
        input.put('fieldsToReturn',fieldsToReturn);
        input.put('sortBy','Name');
        input.put('pageSize',6);
        List<vlocity_cmt.CpqQueryObject> queries = new List<vlocity_cmt.CpqQueryObject>();
        vlocity_cmt.CpqQueryObject constQuery1 = new vlocity_cmt.CpqQueryObject(vlocity_cmt.CpqQueryObject.QUERY_TYPE.IN_OP,Test.getStandardPricebookId(),'Pricebook2Id');
        queries.add(constQuery1);
        input.put('Queries', queries);
        CustomProductSelectorImplementation sImpl = new CustomProductSelectorImplementation();
        Boolean o = sImpl.invokeMethod(methodName, input, output, null);  
         Test.stopTest();
    }
    
    
        @isTest 
    private static void testInvokeMethod2() {
      Test.startTest();

        generateData();
        String methodName = 'getAllProducts';
        Map<String, Object> input = new Map<String, Object>();
        Map<String, Object> output = new Map<String, Object>();
        input.put('Pricebook2Id', Test.getStandardPricebookId());
        input.put('includeAttributes',true);
        List<String>  fieldsToReturn = new List<String>();
        fieldsToReturn.add('Product2Id');
        fieldsToReturn.add('UnitPrice');
        fieldsToReturn.add('Product2.Name');
        input.put('fieldsToReturn',fieldsToReturn);
        input.put('sortBy','NonRecurringCost');
        input.put('pageSize',6);
        List<vlocity_cmt.CpqQueryObject> queries = new List<vlocity_cmt.CpqQueryObject>();
        vlocity_cmt.CpqQueryObject constQuery1 = new vlocity_cmt.CpqQueryObject(vlocity_cmt.CpqQueryObject.QUERY_TYPE.EQUAL_TO_OP,Test.getStandardPricebookId(),'Pricebook2Id',vlocity_cmt.CpqQueryObject.QUERY_LOGICAL_OPERATOR.OR_OPP);
        queries.add(constQuery1);
        input.put('Queries', queries);
        CustomProductSelectorImplementation sImpl = new CustomProductSelectorImplementation();
        Boolean o = sImpl.invokeMethod(methodName, input, output, null);
        Test.stopTest();
    }


          @isTest 
    private static void testInvokeMethod3() {
      Test.startTest();
        generateData();
        String methodName = 'getAllProducts';
        Map<String, Object> input = new Map<String, Object>();
        Map<String, Object> output = new Map<String, Object>();
        input.put('Pricebook2Id', Test.getStandardPricebookId());
        input.put('includeAttributes',true);
        List<String>  fieldsToReturn = new List<String>();
        fieldsToReturn.add('Product2Id');
        fieldsToReturn.add('UnitPrice');
        fieldsToReturn.add('Product2.Name');
        input.put('fieldsToReturn',fieldsToReturn);
        input.put('sortBy','RecurringCost');
        input.put('pageSize',6);
        List<vlocity_cmt.CpqQueryObject> queries = new List<vlocity_cmt.CpqQueryObject>();
        vlocity_cmt.CpqQueryObject constQuery1 = new vlocity_cmt.CpqQueryObject(vlocity_cmt.CpqQueryObject.QUERY_TYPE.EQUAL_TO_OP,Test.getStandardPricebookId(),'Pricebook2Id',vlocity_cmt.CpqQueryObject.QUERY_LOGICAL_OPERATOR.AND_OPP);
        queries.add(constQuery1);
        input.put('Queries', queries);
        //input.put('Queries', null);



        CustomProductSelectorImplementation sImpl = new CustomProductSelectorImplementation();
        Boolean o = sImpl.invokeMethod(methodName, input, output, null);
        Test.stopTest();
    }
    
           @isTest 
    private static void testInvokeMethod4() {
      Test.startTest();

        generateData();
        String methodName = 'getAllProducts';
        Map<String, Object> input = new Map<String, Object>();
        Map<String, Object> output = new Map<String, Object>();
        input.put('Pricebook2Id', Test.getStandardPricebookId());
        input.put('includeAttributes',true);
        List<String>  fieldsToReturn = new List<String>();
        fieldsToReturn.add('Product2Id');
        fieldsToReturn.add('UnitPrice');
        fieldsToReturn.add('Product2.Name');
        input.put('fieldsToReturn',fieldsToReturn);
        input.put('sortBy','NonRecurringCost');
        input.put('pageSize',6);
        List<vlocity_cmt.CpqQueryObject> queries = new List<vlocity_cmt.CpqQueryObject>();
        vlocity_cmt.CpqQueryObject constQuery1 = new vlocity_cmt.CpqQueryObject(vlocity_cmt.CpqQueryObject.QUERY_TYPE.IN_OP,Test.getStandardPricebookId(),'Pricebook2Id',vlocity_cmt.CpqQueryObject.QUERY_LOGICAL_OPERATOR.OR_OPP);
        queries.add(constQuery1);
        input.put('Queries', queries);
        CustomProductSelectorImplementation sImpl = new CustomProductSelectorImplementation();
        Boolean o = sImpl.invokeMethod(methodName, input, output, null);
        Test.stopTest();
    }
    
    
     @isTest 
    private static void testInvokeMethod5() {
      Test.startTest();

        generateData();
        String methodName = 'getProducts';
        Map<String, Object> input = new Map<String, Object>();
        Map<String, Object> output = new Map<String, Object>();
        input.put('Pricebook2Id', Test.getStandardPricebookId());
        input.put('includeAttributes',true);
        List<String>  fieldsToReturn = new List<String>();
        fieldsToReturn.add('Product2Id');
        fieldsToReturn.add('UnitPrice');
        fieldsToReturn.add('Product2.Name');
        input.put('fieldsToReturn',fieldsToReturn);
        input.put('sortBy','NonRecurringCost');
        input.put('pageSize',6);
        List<vlocity_cmt.CpqQueryObject> queries = new List<vlocity_cmt.CpqQueryObject>();
        vlocity_cmt.CpqQueryObject constQuery1 = new vlocity_cmt.CpqQueryObject(vlocity_cmt.CpqQueryObject.QUERY_TYPE.IN_OP,Test.getStandardPricebookId(),'Pricebook2Id',vlocity_cmt.CpqQueryObject.QUERY_LOGICAL_OPERATOR.OR_OPP);
        queries.add(constQuery1);
        input.put('Queries', queries);
        //input.put('Queries', null);



        CustomProductSelectorImplementation sImpl = new CustomProductSelectorImplementation();
        Boolean o = sImpl.invokeMethod(methodName, input, output, null);
        Test.stopTest();
    }
    
    private static void generateData() {

        String invalidJSON = '{"TELUSCHAR":[{"$$AttributeDefinitionStart$$":null,"objectid__c":"01t220000003x14AAA","attributeid__c":null,"attributecategoryid__c":"a7Q2200000000UBEAY","categorycode__c":"TELUSCHAR","categoryname__c":"Characteristics","attributeuniquecode__c":null,"attributeconfigurable__c":false,"attributedisplaysequence__c":"4","attributefilterable__c":false,"isactive__c":false,"attributedisplayname__c":null,"displaysequence__c":null,"formatmask__c":null,"hasrule__c":false,"isconfigurable__c":true,"ishidden__c":true,"valueinnumber__c":null,"objecttype__c":"Product2","querycode__c":null,"isreadonly__c":true,"isquerydriven__c":false,"querylabel__c":null,"id":"a7P22000000VSudEAG","isrequired__c":false,"rulemessage__c":null,"uidisplaytype__c":"Dropdown","value__c":null,"valuedatatype__c":"Picklist","valuedescription__c":null,"attributeRunTimeInfo":{"dataType":"Picklist","uiDisplayType":"Dropdown","values":[{"value":"British Columbia","displayText":"British Columbia","id":"9145124756913834806"},{"value":"Alberta","displayText":"Alberta","id":"9145124756913834804"}],"default":[]},"$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"objectid__c":"01t220000003x14AAA","attributeid__c":"a7R220000004CUaEAM","attributecategoryid__c":"a7Q2200000000UBEAY","categorycode__c":"TELUSCHAR","categoryname__c":"Characteristics","attributeuniquecode__c":"ATTRIBUTE-096","attributeconfigurable__c":true,"attributedisplaysequence__c":"1","attributefilterable__c":false,"isactive__c":true,"attributedisplayname__c":"Offering ID","displaysequence__c":null,"formatmask__c":null,"hasrule__c":false,"isconfigurable__c":true,"ishidden__c":true,"valueinnumber__c":null,"objecttype__c":"Product2","querycode__c":null,"isreadonly__c":true,"isquerydriven__c":false,"querylabel__c":null,"id":"a7P22000000VSuUEAW","isrequired__c":true,"rulemessage__c":null,"uidisplaytype__c":"Text","value__c":"OP-TollRestriction","valuedatatype__c":"Text","valuedescription__c":null,"attributeRunTimeInfo":{"dataType":"Text","uiDisplayType":"Text","default":"OP-TollRestriction"},"$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"objectid__c":"01t220000003x14AAA","attributeid__c":"a7R220000004CXQEA2","attributecategoryid__c":"a7Q2200000000UBEAY","categorycode__c":"TELUSCHAR","categoryname__c":"Characteristics","attributeuniquecode__c":"ATTRIBUTE-155","attributeconfigurable__c":true,"attributedisplaysequence__c":"3","attributefilterable__c":false,"isactive__c":true,"attributedisplayname__c":"Line Item External ID","displaysequence__c":null,"formatmask__c":null,"hasrule__c":false,"isconfigurable__c":true,"ishidden__c":true,"valueinnumber__c":null,"objecttype__c":"Product2","querycode__c":null,"isreadonly__c":true,"isquerydriven__c":false,"querylabel__c":null,"id":"a7P22000000VSuWEAW","isrequired__c":false,"rulemessage__c":null,"uidisplaytype__c":"Text","value__c":null,"valuedatatype__c":"Text","valuedescription__c":null,"attributeRunTimeInfo":{"dataType":"Text","uiDisplayType":"Text"},"$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"objectid__c":"01t220000003x14AAA","attributeid__c":"a7R220000004CYEEA2","attributecategoryid__c":"a7Q2200000000UBEAY","categorycode__c":"TELUSCHAR","categoryname__c":"Characteristics","attributeuniquecode__c":"ATTRIBUTE-165","attributeconfigurable__c":true,"attributedisplaysequence__c":"2","attributefilterable__c":false,"isactive__c":true,"attributedisplayname__c":"Move Cross-Reference","displaysequence__c":null,"formatmask__c":null,"hasrule__c":false,"isconfigurable__c":true,"ishidden__c":true,"valueinnumber__c":null,"objecttype__c":"Product2","querycode__c":null,"isreadonly__c":true,"isquerydriven__c":false,"querylabel__c":null,"id":"a7P22000000VSuVEAW","isrequired__c":false,"rulemessage__c":null,"uidisplaytype__c":"Text","value__c":null,"valuedatatype__c":"Text","valuedescription__c":null,"attributeRunTimeInfo":{"dataType":"Text","uiDisplayType":"Text"},"$$AttributeDefinitionEnd$$":null}]}';
        String validJSON = '{"cat1":[{"$$AttributeDefinitionStart$$":null,"objectid__c":"01t28000001sUmtAAE","attributeid__c":"a0A2800000C5UbJEAV","attributecategoryid__c":"a092800000EMbjEAAT","categorycode__c":"cat1","categoryname__c":"Category1","attributeuniquecode__c":"ATTRIBUTE-014","attributeconfigurable__c":true,"attributedisplaysequence__c":"1","attributefilterable__c":true,"isactive__c":true,"attributedisplayname__c":"Attr1","displaysequence__c":"3","formatmask__c":null,"hasrule__c":false,"isconfigurable__c":false,"ishidden__c":false,"valueinnumber__c":null,"objecttype__c":"Product2","querycode__c":null,"isreadonly__c":false,"isquerydriven__c":false,"querylabel__c":null,"id":"a082800000aa4fCAAQ","isrequired__c":true,"rulemessage__c":null,"uidisplaytype__c":"","value__c":null,"valuedatatype__c":"Currency","valuedescription__c":null,"attributeRunTimeInfo":{"dataType":"Currency"},"$$AttributeDefinitionEnd$$":null}]}';
        String errorSyntaxJSON = '{"cat1":[{"$$AttributeDefinitionStart$$":null,"objectid__c":"01t28000001sUmtAAE","attributeid__c":"a0A2800000C5UbJEAV","attributecategoryid__c":"a092800000EMbjEAAT","categorycode__c":"cat1","categoryname__c":"Category1","attributeuniquecode__c":"ATTRIBUTE-014","attributeconfigurable__c":true,"attributedisplaysequence__c":"1","attributefilterable__c":true,"isactive__c":true,"attributedisplayname__c":"Attr1","displaysequence__c":"3","formatmask__c":null,"hasrule__c":false,"isconfigurable__c":false,"ishidden__c":false,"valueinnumber__c":null,"objecttype__c":"Product2","querycode__c":null,"isreadonly__c":false,"isquerydriven__c":false,"querylabel__c":null,"id":"a082800000aa4fCAAQ","isrequired__c":true,"rulemessage__c":null,"uidisplaytype__c":"","value__c":null,"valuedatatype__c"-"Currency","valuedescription__c":null,"attributeRunTimeInfo":{"dataType":"Currency"},"$$AttributeDefinitionEnd$$":null}]}"';

        Product2 product1 = new Product2();    
        product1.Name = 'Test Product 1';
        product1.vlocity_cmt__JSONAttribute__c = invalidJSON;
        insert product1;

        Product2 product2 = new Product2();    
        product2.Name = 'Test Product 2';
        product2.vlocity_cmt__JSONAttribute__c = validJSON;
        insert product2;

        Product2 product3 = new Product2();    
        product3.Name = 'Test Product 3';
        product3.vlocity_cmt__JSONAttribute__c = errorSyntaxJSON;
        insert product3;

        Product2 product4 = new Product2();    
        product4.Name = 'Test Product 3';
        insert product4;
        
        PricebookEntry priceBookEntryNew1 = new PricebookEntry();
        priceBookEntryNew1.PriceBook2Id = Test.getStandardPricebookId();
        priceBookEntryNew1.Product2Id = product1.Id;
        priceBookEntryNew1.UnitPrice = 11.00;
        priceBookEntryNew1.UseStandardPrice = false;
        priceBookEntryNew1.isactive = true;
        insert priceBookEntryNew1;

        PricebookEntry priceBookEntryNew2 = new PricebookEntry();
        priceBookEntryNew2.PriceBook2Id = Test.getStandardPricebookId();
        priceBookEntryNew2.Product2Id = product2.Id;
        priceBookEntryNew2.UnitPrice = 11.00;
        priceBookEntryNew2.UseStandardPrice = false;
        priceBookEntryNew2.isactive = true;
        insert priceBookEntryNew2;

        PricebookEntry priceBookEntryNew3 = new PricebookEntry();
        priceBookEntryNew3.PriceBook2Id = Test.getStandardPricebookId();
        priceBookEntryNew3.Product2Id = product3.Id;
        priceBookEntryNew3.UnitPrice = 11.00;
        priceBookEntryNew3.UseStandardPrice = false;
        priceBookEntryNew3.isactive = true;
        insert priceBookEntryNew3;

        PricebookEntry priceBookEntryNew4 = new PricebookEntry();
        priceBookEntryNew4.PriceBook2Id = Test.getStandardPricebookId();
        priceBookEntryNew4.Product2Id = product4.Id;
        priceBookEntryNew4.UnitPrice = 11.00;
        priceBookEntryNew4.UseStandardPrice = false;
        priceBookEntryNew4.isactive = true;
        insert priceBookEntryNew4;
    }   
}