public class ContactVO {
	public Contact record {get; private set;}
	public String id {get{return record.Id;}}
	
	public String name {
		get{return record.Name;}
	}
	public String firstName {
		get{return record.FirstName;}
		set{record.FirstName = value;}
	}
	public String lastName {
		get{return record.LastName;}
		set{record.LastName = value;}
	}
	public String title {
		get{return record.Title;}
		set{record.Title = value;}
	}
	public String role {
		get{return record.Role_c__c;}
		set{record.Role_c__c = value;}
	}
	public String department {
		get{return record.Department_Category__c;}
		set{record.Department_Category__c = value;}
	}
	public String titleCategory {
		get{return record.Title_Category__c;}
		set{record.Title_Category__c = value;}
	}
	public String language {
		get{return record.Language_Preference__c;}
		set{record.Language_Preference__c = value;}
	}
	public String contactMethod {
		get{return record.Preferred_Contact_Method__c;}
		set{record.Preferred_Contact_Method__c = value;}
	}
	public String phone {
		get{return record.Phone;}
		set{record.Phone = value;}
	}
	public String otherPhone {
		get{return record.OtherPhone;}
		set{record.OtherPhone = value;}
	}
	public String fax {
		get{return record.Fax;}
		set{record.Fax = value;}
	}
	public String mobilePhone {
		get{return record.MobilePhone;}
		set{record.MobilePhone = value;}
	}
	public String email {
		get{return record.Email;}
		set{record.Email = value;}
	}
	public String mailingStreet {
		get{return record.MailingStreet;}
		set{record.MailingStreet = value;}
	}
	public String mailingCity {
		get{return record.MailingCity;}
		set{record.MailingCity = value;}
	}
	public String mailingProvince {
		get{return record.MailingState;}
		set{record.MailingState = value;}
	}
	public String mailingPostalCode {
		get{return record.MailingPostalCode;}
		set{record.MailingPostalCode = value;}
	}
	public String mailingCountry {
		get{return record.MailingCountry;}
		set{record.MailingCountry = value;}
	}
	public Boolean optIn {
		get{return record.Email_Opt_In__c;}
		set{record.Email_Opt_In__c = value;}
	}
	
	public ContactVO(Contact record) {
		this.record = record;
	}
}