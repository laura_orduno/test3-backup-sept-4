public with sharing class trac_Utilities {

  // Helper Method: creates a query string for all fields in the Offer House Object
  // Returns the string
  public static String createObjectQuery(String theObject) {
    
    // Iterate through fields to select
    String theQuery = 'SELECT ' + String.join(trac_Utilities.getObjectFields(theObject), ', ');
    theQuery += ' FROM ' + theObject;
    
    return theQuery;
  }
  
  // Helper Method: gets an object's Fields
  public static List<String> getObjectFields(String theObject) {
    
    List<String> fieldNames = new List<String>();
    
    // Get describe for the object
    Schema.sObjectType sobject_type = Schema.getGlobalDescribe().get(theObject);
    Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe();
    
    // Get field map for the object
    Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap();
    for (Schema.SObjectField fieldObj : field_map.values()) {
      
      // Store each field name
      Schema.DescribeFieldResult field = fieldObj.getDescribe();
      if (field.isAccessible()) fieldNames.add(field.getName());
    }
    
    return fieldNames;
  }
  
  // Helper method: returns true if the current user can access
  // the object's field
  public static Boolean isFieldAccessible(String theObject, String theField) {
    
    // Get describe for the object
    Schema.sObjectType sobject_type = Schema.getGlobalDescribe().get(theObject);
    Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe();
    
    // Get field map for the object
    Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap();
    
    // Get the specified field
    if (field_map.containsKey(theField)) {
        Schema.DescribeFieldResult field = field_map.get(theField).getDescribe();
        return field.isAccessible();
    }
    return false;
  }
  
  // Helper method: returns true if the current user can access
  // the object's field
  public static Boolean isFieldEditable(String theObject, String theField) {
    
    // Get describe for the object
    Schema.sObjectType sobject_type = Schema.getGlobalDescribe().get(theObject);
    Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe();
    
    // Get field map for the object
    Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap();
    
    // Get the specified field
    if (field_map.containsKey(theField)) {
        Schema.DescribeFieldResult field = field_map.get(theField).getDescribe();
        return field.isUpdateable();
    }
    return false;
  }
  
  
  public static String addField(String input, String newField) {
    // replace with regex
    return input.replace('SELECT ', 'SELECT ' + newField + ', ');
  }
  
  public static String removeField(String input, String newField) {
    // replace with regex
    if (input.contains(' ' + newField + ',')) input = input.replace(' ' + newField + ',', '');
    if (input.contains(', ' + newField + ' ')) input = input.replace(', ' + newField + ' ', '');
    return input;
  }
  
}