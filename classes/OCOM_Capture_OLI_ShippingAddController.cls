/********************************************************************************************************************************
Class Name:     OCOM_Capture_OLI_ShippingAddController 
Purpose:        This class is a controller class for OCOM_Capture_OLI_ShippingAddress component      
Created by:     Aditya Jamwal      26-August-2016  
Modified by:    Aditya Jamwal      13-Feb-2017      Defect# 10518 : Contact Name, Contact Number and Contact Email not displayed for WLS devices.
													Defect# 10516 : Contact Number and Email not visible in NC for TELUS SIM Card.
*********************************************************************************************************************************
*/
public class OCOM_Capture_OLI_ShippingAddController {
    public String oderId {get; set;}
    public boolean isAddedAddressValidated {get;set;}
    public Map<String,String> orderStatusMap{get;set;} // added as per RTA-647 RTA-965
    public String unSelectedOLIIDs {get;set;}    //Samir - Fix BSBD_RTA-553 extra check added to avoid ignoring last element  
    public boolean getisValMaploaded(){
        if(null !=canadaPostValCompMap && ((Map<string,string>)canadaPostValCompMap).size()>0){
            return true;
        }
        else return false;
    }    
    
    public Object canadaPostValCompMap;
    
    public void setcanadaPostValCompMap(Object argId){
        canadaPostValCompMap = argId;    
    } 
    
    public Object getcanadaPostValCompMap(){system.debug('!!canadaPostValCompMap '+canadaPostValCompMap);
                                            Map<String,String> gg = new Map<String,String>();
                                            system.debug('@!'+ canadaPostValCompMap);
                                            return ((Map<String,String>)canadaPostValCompMap);
                                           }
    
    public String contactName {get; set;}
    //Defect# 10516,10518 Start - Added by Aditya Jamwal 13, Feb 2017
    public String contactEmail {get; set;}
    public String contactNumber {get; set;}
    //Defect# 10516,10518 End - Added by Aditya Jamwal 13, Feb 2017
    public List<OLIWrapper> OLIWrapperList {get;set;}
    public Map<String,List<OLIWrapper>> OLIWrapperToServiceAddressMap;
    public Map<String,List<OLIWrapper>> getOLIWrapperToServiceAddressMap(){
        system.debug('debugging !!!OLIWrapperToServiceAddressMap 0 '+ OLIWrapperToServiceAddressMap );
        OLIWrapperToServiceAddressMap = new Map<String,List<OLIWrapper>>(); 
        OLIWrapperList = new List<OLIWrapper>();  
        Map<String,List<OLIWrapper>> returnMap=init(OderId);
        system.debug('debugging !!!returnMap 0 '+ returnMap );
        return returnMap; 
    }
    public AddressData PAData  { get; set; }
    public OCOM_Capture_OLI_ShippingAddController(){
        system.debug('%%######%% START: OCOM_Capture_OLI_ShippingAddController:OCOM_Capture_OLI_ShippingAddController  %%######%% ');
        system.debug('OCOM_Capture_OLI_ShippingAddController constructor oderId(' + canadaPostValCompMap + ')');          
        PAData  = new AddressData();
        unSelectedOLIIDs = '';
        // isShippableOffersPresent=true;
        // system.debug('%%######%% END: OCOM_Capture_OLI_ShippingAddController:OCOM_Capture_OLI_ShippingAddController  %%######%% ');
        
    }
    private Map<String,List<OLIWrapper>> init(String orderId){ 
        system.debug('%%######%% START: OCOM_Capture_OLI_ShippingAddController:init  %%######%% ');
        system.debug('OCOM_Capture_OLI_ShippingAddController constructor oderId 1 (' + canadaPostValCompMap + ')');          
        Map<string,OrderItem>offerIDToOfferMap = new Map<string,OrderItem>();
        List<OLIWrapper> OLIWrapperList2 = new  List<OLIWrapper>();
        boolean noShippingAddressFound = false;
        Map<String,List<OLIWrapper>>  initOLIWrapperToServiceAddrMap = new Map<String,List<OLIWrapper>>();
        List<OrderItem> ordrItemRecs =[select id,order.status,Shipping_Contact_Email__c,Shipping_Contact_Number__c,Shipping_Address_Contact__c,pricebookentry.product2.OCOM_Shippable__c,Shipping_Address__c,
                                       vlocity_cmt__RootItemId__c,order.ServiceAddressSummary__c,order.Account.Name,order.OwnerId,pricebookentry.name,
                                       pricebookentry.product2.OCOM_Bookable__c,pricebookentry.product2.ProductSpecification__r.orderMgmtId__c,
                                       orderId,vlocity_cmt__JSONAttribute__c, vlocity_cmt__JSONNode__c,order.Service_Address__r.FMS_Address_ID__c,
                                       order.Service_Address__r.Street__c,order.Service_Address__r.Building_Number__c,order.Service_Address__r.City__c,
                                       order.Service_Address__r.Province__c,order.Service_Address__r.Postal_Code__c,order.Service_Address__r.COID__c,
                                       order.Service_Address__r.Country__c from OrderItem where orderId =:orderId OR order.ParentId__c =:orderId ];
        //OR order.ParentId__c =:orderId
        orderStatusMap = new Map<String,String>(); // added as per RTA-647 and RTA-965
        for(OrderItem o:ordrItemRecs){
            system.debug('!!!OrderItem '+o.pricebookentry.name);
            offerIDToOfferMap.put(o.id,o);
            Boolean isShippableCharInJSON = false;
            Map<String,OCOM_VlocityJSONParseUtil.JSONWrapper> attributesMap = OCOM_VlocityJSONParseUtil.getAttributesFromJSON(o.vlocity_cmt__JSONAttribute__c);
            if(null != attributesMap && null!= OrdrConstants.CHAR_SHIPPING_REQUIRED && null != attributesMap.get(OrdrConstants.CHAR_SHIPPING_REQUIRED) && String.isNotBlank((attributesMap.get(OrdrConstants.CHAR_SHIPPING_REQUIRED)).value)
               && (attributesMap.get(OrdrConstants.CHAR_SHIPPING_REQUIRED)).value.equalsIgnoreCase(OrdrConstants.CHAR_VALUE_YES)){
                   isShippableCharInJSON= true;
               }
            if(o.pricebookentry.product2.OCOM_Shippable__c == 'Yes' || isShippableCharInJSON){
                OLIWrapper OLIWrapper = new OLIWrapper(o);
                if(String.isNotBlank(o.vlocity_cmt__JSONAttribute__c)){
                    OLIWrapper.deviceName = o.pricebookentry.Name;// (String) readOLICharacterstic(o.vlocity_cmt__JSONAttribute__c,'Device Name','Read',null);
                    OLIWrapper.subcriberName ='';
                    if(null != OrdrConstants.CHAR_SUBSCRIBER_FIRST_NAME && null != attributesMap.get(OrdrConstants.CHAR_SUBSCRIBER_FIRST_NAME)){OLIWrapper.subcriberName = attributesMap.get(OrdrConstants.CHAR_SUBSCRIBER_FIRST_NAME).value;} 
                    if(null != OrdrConstants.CHAR_SUBSCRIBER_LAST_NAME && null != attributesMap.get(OrdrConstants.CHAR_SUBSCRIBER_LAST_NAME)){OLIWrapper.subcriberName += ' '+ attributesMap.get(OrdrConstants.CHAR_SUBSCRIBER_LAST_NAME).value;}
                    
                    //((String) readOLICharacterstic(o.vlocity_cmt__JSONAttribute__c,'Subscriber First Name','Read',null))+' '
                    //+((String) readOLICharacterstic(o.vlocity_cmt__JSONAttribute__c,'Subscriber Last Name','Read',null) );
                    system.debug('OLIWrapper.subcriberName '+OLIWrapper.subcriberName);
                }
                if(!noShippingAddressFound){isAddedAddressValidated = true;}
                if(String.isBlank(o.Shipping_Address__c)){isAddedAddressValidated = false; noShippingAddressFound =true; }
                OLIWrapper.contactName= o.Shipping_Address_Contact__c; 
                OLIWrapper.contactNumber= o.Shipping_Contact_Number__c; 
                OLIWrapper.contactEmail= o.Shipping_Contact_Email__c; 
                OLIWrapperList.add(OLIWrapper);
                OLIWrapperList2 = new  List<OLIWrapper>();
                if(null != initOLIWrapperToServiceAddrMap.get(o.order.ServiceAddressSummary__c)){
                OLIWrapperList2 =  initOLIWrapperToServiceAddrMap.get(o.order.ServiceAddressSummary__c);
                }
                OLIWrapperList2.add(OLIWrapper);
                initOLIWrapperToServiceAddrMap.put(o.order.ServiceAddressSummary__c,OLIWrapperList2);
                orderStatusMap.put(o.order.ServiceAddressSummary__c,o.order.status); // added as per RTA-647 and RTA-965
            }  
            
        }
        if(null !=OLIWrapperList && OLIWrapperList.size()>0){
            for(OLIWrapper oWrap:OLIWrapperList){
                if(offerIDToOfferMap.containsKey(oWrap.oli.vlocity_cmt__RootItemId__c)){
                    oWrap.OfferName=(offerIDToOfferMap.get(oWrap.oli.vlocity_cmt__RootItemId__c)).pricebookentry.name;   
                }                
            }
        }
        system.debug('%%######%% END: OCOM_Capture_OLI_ShippingAddController:init  %%######%% ');
        return initOLIWrapperToServiceAddrMap;
    }
    
    public static Object readOLICharacterstic(String JsonChar,string charKeyName,String operationType,Map<String,String> charactersticValueMap){
        system.debug('%%######%% START: OCOM_Capture_OLI_ShippingAddController:readOLICharacterstic  %%######%% ');
        system.debug('!!! JsonChar '+JsonChar);
        try{ 
            if(String.isNotBlank(JsonChar)){
                Map<String, Object> attributes = (Map<String, Object>) JSON.deserializeUntyped(JsonChar);
                system.debug('JsonChar Map '+attributes);
                for (String key : attributes.keySet()) {
                    for (Object attribute : (List<Object>) attributes.get(key)) {
                        Map<String, Object> attributeMap = (Map<String, Object>) attribute;
                        String attributeName = (String) attributeMap.get('attributedisplayname__c');
                        if(String.IsNotBlank(attributeName) && attributeName.toUpperCase() == charKeyName.toUpperCase()){
                            String value = (String) attributeMap.get('value__c');
                            Map<String, Object> runTimeInfoMap = (Map<String, Object>) attributeMap.get('attributeRunTimeInfo');
                            system.debug('!!!! JsonChar attributeMap '+attributeMap);
                            if(String.isBlank(value)) {
                                value = (String) runTimeInfoMap.get('value'); 
                            }
                            system.debug('!!!! JsonChar value 0 '+value);                            
                            //String value = (String) attributeMap.get('value__c');                  
                            if(null != charactersticValueMap && charactersticValueMap.containsKey(attributeName) ){
                                if(operationType=='Update'){
                                    if(runTimeInfoMap.containsKey('value'))
                                    {runTimeInfoMap.put('value',charactersticValueMap.get(attributeName));}
                                    if(attributeMap.containskey('value__c'))  
                                    {attributeMap.put('value__c',charactersticValueMap.get(attributeName));}
                                }else{
                                    system.debug('!!!! JsonChar value 1 '+value+' '+operationType+' '+charactersticValueMap );
                                    system.debug('%%######%% END: OCOM_Capture_OLI_ShippingAddController:readOLICharacterstic  %%######%% ');
                                    return value;
                                } 
                                
                            }                   
                        }
                    }
                }
                if(operationType=='Update'){
                    system.debug('!!!! JsonChar value attributes '+attributes);
                    system.debug('%%######%% END: OCOM_Capture_OLI_ShippingAddController:readOLICharacterstic  %%######%% ');
                    return  JSON.serialize(attributes);}
                system.debug('!!!! JsonChar value 3');
                system.debug('%%######%% END: OCOM_Capture_OLI_ShippingAddController:readOLICharacterstic  %%######%% ');
                return ' ';
            }else{
                system.debug('%%######%% END: OCOM_Capture_OLI_ShippingAddController:readOLICharacterstic  %%######%% ');
                return JsonChar;
            }
            
        }catch(exception e){
            system.debug('!!!!! exception try 0 '+e);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,Label.OCOM_ExceptionMessagePrefix+e.getMessage()));
            system.debug('%%######%% END: OCOM_Capture_OLI_ShippingAddController:readOLICharacterstic  %%######%% ');
            return JsonChar; 
            
        }
    }
    Public Pagereference SaveShipping()
    {
        Boolean isJSONUpdateRequired = false;
        system.debug('%%######%% START: OCOM_Capture_OLI_ShippingAddController:SaveShipping  %%######%% ');
        String sAddr = ' '+(String)PAData.addressLine1+ ' ' +(String)PAData.city+ ' ' +(String)PAData.province+ ' '+(String)PAData.postalCode+' ' +(String)PAData.country;
        List <OrderItem> selectedOLIs=New List<OrderItem>();
        Map<String,String> charactersticValueMap = new  Map<String,String>();
        if(String.isNotBlank(sAddr) && String.isNotBlank(PAData.addressLine1) 
                                    && null !=OrdrConstants.CHAR_SHIPPING_ADDRESS_CHARACTERISTIC){charactersticValueMap.put(OrdrConstants.CHAR_SHIPPING_ADDRESS_CHARACTERISTIC,sAddr);}
        if(String.isNotBlank(sAddr) && String.isNotBlank(PAData.addressLine1) 
                                    && null !=OrdrConstants.CHAR_SHIPPING_ADDRESS){charactersticValueMap.put(OrdrConstants.CHAR_SHIPPING_ADDRESS,sAddr);}
        if(String.isNotBlank(contactName) && null !=OrdrConstants.CHAR_SHIPPING_CONTACT_NAME){charactersticValueMap.put(OrdrConstants.CHAR_SHIPPING_CONTACT_NAME,contactName);} 
        if(String.isNotBlank(PAData.city) && null !=OrdrConstants.CHAR_CITY_OF_USE){charactersticValueMap.put(OrdrConstants.CHAR_CITY_OF_USE,PAData.city);}
        if(String.isNotBlank(PAData.province) && null !=OrdrConstants.CHAR_PROVINCE_OF_USE){charactersticValueMap.put(OrdrConstants.CHAR_PROVINCE_OF_USE,PAData.province);}
        //Defect# 10516,10518 Start - Added by Aditya Jamwal 13, Feb 2017
        if(String.isNotBlank(contactNumber) && null !=OrdrConstants.CHAR_SHIPPING_CONTACT_NUMBER){charactersticValueMap.put(OrdrConstants.CHAR_SHIPPING_CONTACT_NUMBER,contactNumber);}
        if(String.isNotBlank(contactEmail)  && null !=OrdrConstants.CHAR_SHIPPING_CONTACT_EMAIL){charactersticValueMap.put(OrdrConstants.CHAR_SHIPPING_CONTACT_EMAIL,contactEmail);}
        //Defect# 10516,10518 End - Added by Aditya Jamwal 13, Feb 2017
        
        system.debug('!!@@charactersticValueMap '+ charactersticValueMap );
        system.debug('!!! OLIWrapperList'+ OLIWrapperList);
        system.debug('!!! unSelectedOLIIDs'+ unSelectedOLIIDs);
        if(OLIWrapperList.size()>0){
            //  for(List<OLIWrapper> olList :OLIWrapperToServiceAddressMap.values())
            for(OLIWrapper ol :OLIWrapperList){
                system.debug('!!! vlocity_cmt__JSONAttribute__c '+ol.oli.vlocity_cmt__JSONAttribute__c);
                String JsonChar =ol.oli.vlocity_cmt__JSONAttribute__c;
                if(ol.isSelected){
        	    //BSBD_RTA-553 - this is an workaround solution to fix the issue that always last element selection from client is ignored on controller 
        	    if(String.isNotBlank(unSelectedOLIIDs) && unSelectedOLIIDs.contains(ol.oli.id)){
        	 	system.debug('OCOM_Capture_OLI_BillingAddrController::SaveBillingAccount ignoring unselected oli id '+ol.oli.id);
        	 	continue;
        	    }  
                    //set Shipping Address
                    if(String.isNotBlank(sAddr) && String.isNotBlank(PAData.addressLine1)){
                        ol.oli.Shipping_Address__c =sAddr;
                        isJSONUpdateRequired = true;
                    }
                    //set Shipping Contact Name
                    if(String.isNotBlank(contactName)){
                        ol.oli.Shipping_Address_Contact__c=ol.contactName=contactName;
                        system.debug('!!!! contactName'+contactName);
                        isJSONUpdateRequired = true;
                    }
                    //Defect# 10516,10518 Start - Added by Aditya Jamwal 15, Feb 2017
                    //set Shipping Contact Email
                    if(String.isNotBlank(contactEmail)){
                        ol.oli.Shipping_Contact_Email__c=ol.contactEmail=contactEmail;
                        system.debug('!!!! contactEmail'+contactEmail);
                        isJSONUpdateRequired = true;
                    }
                    //set Shipping Contact Number
                    if(String.isNotBlank(contactNumber)){
                        ol.oli.Shipping_Contact_Number__c=ol.contactNumber=contactNumber;
                        system.debug('!!!! contactNumber'+contactNumber);
                        isJSONUpdateRequired = true;
                    }
                    //Defect# 10516,10518 End   - Added by Aditya Jamwal 15, Feb 2017
                    
                    if(String.isNotBlank(ol.oli.vlocity_cmt__JSONAttribute__c) && isJSONUpdateRequired){
                        JsonChar = OCOM_VlocityJSONParseUtil.setAttributesInJSON(ol.oli.vlocity_cmt__JSONAttribute__c,charactersticValueMap);
                        ol.oli.vlocity_cmt__JSONAttribute__c= JsonChar;
                    }
                    
                    system.debug('!!!!debug  '+ol.oli.Shipping_Address__c ); 
                    if(isJSONUpdateRequired){
                       selectedOLIs.add(ol.oli); 
                    }                       
                }
            }
            PAData.addressLine1 = ' ';
            PAData.city = ' ' ;
            PAData.province = ' ';
            PAData.postalCode =' ' ;
            PAData.country ='';
            //Samir - Fix BSBD_RTA-553 extra check added to avoid ignoring last element  
            unSelectedOLIIDs = '';
        }
        try{
            update selectedOLIs;  
            //  OLIWrapperList.clear();           
        }catch(exception e)
        {
            system.debug('!!!!! exception try '+e);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,Label.OCOM_ExceptionMessagePrefix+e.getMessage()));
            system.debug('%%######%% END: OCOM_Capture_OLI_ShippingAddController:SaveShipping  %%######%% ');
            return null;
        }   
        system.debug('%%######%% END: OCOM_Capture_OLI_ShippingAddController:SaveShipping  %%######%% ');
        return null;
    } 
    
    Public class OLIWrapper{
        Public boolean isSelected {get;set;}
        Public String deviceName {get;set;}
        Public String OfferName {get;set;}
        Public String subcriberName {get;set;}
        public OrderItem oli {get;set;}
        public String contactName  {get;set;}
        //Defect# 10516,10518 Start - Added by Aditya Jamwal 13, Feb 2017
        public String contactNumber  {get;set;}
        public String contactEmail  {get;set;}
        //Defect# 10516,10518 Start - Added by Aditya Jamwal 13, Feb 2017
        public OLIWrapper(orderItem o){
        	//Samir BSBD_RTA-553 - fixing issue to pre-select checkboxs on page refresh. 
            this.isSelected= true;
            this.oli=o;
        }
    }
}