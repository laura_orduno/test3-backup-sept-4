/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 * Name - GlobysBillAnalyzerHistoryChart_Test
 * 1. Created By - Sandip - 17 Aug 2016
 * 2. Modified By 
 */
 
@isTest(SeeAllData=false)
public class ExternalEventFramework_Test{

    public static void createEEConfig(String eventName, String handlerName, Boolean isAsync){
        External_Event_Config__c obj1 = new External_Event_Config__c (Name = eventName, Event_Handler_Name__c = handlerName , Is_Asynchronous_Execution__c = isAsync);
        Database.insert(obj1 , false);
    }
    
    public static void createEETemplateConfig(String templateId, String templateName){
        External_Event_Notification_Templates__c obj1 = new External_Event_Notification_Templates__c (Name = templateId, Email_Template_Name__c = templateName);
        Database.insert(obj1 , false);
    }
    
    public static account createRCIDAccount(){
        Account cbucid = new Account(
            Name = 'Test',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CBUCID').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5'
        );
        insert cbucid;
        Account rcid = new Account(
            Name = 'RCID Test',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            RCID__c = '987654_xx',
            parentId = cbucid.id
        );
        insert rcid; 
        return rcid;
    }
    
    public static Account createBillingAccount(Id parentAcc){
        Account canID = new Account(
            Name = 'RCID Test',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CAN').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            BAN_CAN__c = '119-987654test_xx',
            CAN__c = '987654test_xx',
            parentId = parentAcc
        );
        insert canID;
        return canID; 
    }
    
    public static void createContact(Id accId){
        Contact conId = new Contact(FirstName='test',LastName='test',
                                    Email='aaa@xxx.com', HSIA_Data_Usage_Email_Notification__c = true,
                                    AccountId=accId);
        insert conId;                           
    }
    
    public static void createAsset(Id accId){
        Asset assetObj = new Asset(Name='testasset', Phone__c = '(647) 718-9999',
                                    AccountId=accId);
        insert assetObj;                           
    }
    
    public static void createEmailTemplate(String templateName){
        System.runAs ( new User(Id = UserInfo.getUserId()) ) {
            EmailTemplate validEmailTemplate = new EmailTemplate();
            validEmailTemplate.isActive = true;
            validEmailTemplate.Name = templateName;
            validEmailTemplate.DeveloperName = templateName;
            validEmailTemplate.TemplateType = 'text';
            validEmailTemplate.FolderId = UserInfo.getUserId();
            validEmailTemplate.Subject = 'Your Subject Here';
            insert validEmailTemplate;
        }
    }
    
    public static testMethod void testEEFrameworkAsync(){
        createEmailTemplate('UBB_OU_Notification_75_EN');
        createEEConfig('UBBThresholdBreach', 'HSIAThresholdNotifyEventHandlerImpl', true);
        createEETemplateConfig('UBBThresholdBreach75English', 'UBB_OU_Notification_75_EN');
        Account accObj = createRCIDAccount();
        createBillingAccount(accObj.Id);
        createContact(accObj.Id);
        String jsonInput = '{"ServiceID":"1001020835","ECID":"0000","BillingAccountNumber":"987654test_xx","BillCycle":"4","Quota":"200 GB","SubscriberAddress":"809  HIGH FOREST PL BC LANGFORD  CAN","ThresholdPercentage":"75","ActualUsage":"465.81GB","HSIAPricePlan":"High Speed Extreme (V3)","ThresholdTimestamp":"November 5, 1994, 8:15:30 am UTC","BilllingSystemId":"119"}';
        ExternalEventRequestResponseParameter.RequestEventData eventData = new ExternalEventRequestResponseParameter.RequestEventData();
        eventData.eventDetails = jsonInput;
        eventData.eventName = 'UBBThresholdBreach';
        test.startTest();
        SFDCExternalEventService_v1_0.createEvent(eventData);
        List<ExternalEventRequestResponseParameter.ResponseEventData> responseData;
        ExternalEventService_Helper.logException('test', 'test', 'test', 'test', 'test', responseData);
        responseData = new List<ExternalEventRequestResponseParameter.ResponseEventData>();
        ExternalEventRequestResponseParameter.ResponseEventData responseObject = new ExternalEventRequestResponseParameter.ResponseEventData();
        responseData.add(responseObject);
        ExternalEventService_Helper.logException('test', 'test', 'test', 'test', 'test', responseData);
        test.stopTest();
    }
    
    public static testMethod void testEEFrameworkEvents(){
        createEmailTemplate('UBB_OU_Notification_75_EN');
        createEEConfig('UBBThresholdBreach', 'HSIAThresholdNotifyEventHandlerImpl', true);
        createEETemplateConfig('UBBThresholdBreach75English', 'UBB_OU_Notification_75_EN');
        Account accObj = createRCIDAccount();
        createBillingAccount(accObj.Id);
        createContact(accObj.Id);
        String jsonInput = '{"ServiceID":"1001020835","ECID":"0000","BillingAccountNumber":"987654test_xx","BillCycle":"4","Quota":"200 GB","SubscriberAddress":"809  HIGH FOREST PL BC LANGFORD  CAN","ThresholdPercentage":"75","ActualUsage":"465.81GB","HSIAPricePlan":"High Speed Extreme (V3)","ThresholdTimestamp":"November 5, 1994, 8:15:30 am UTC","BilllingSystemId":"119"}';
        test.startTest();
        ExternalEventRequestResponseParameter.RequestEventData eventData = new ExternalEventRequestResponseParameter.RequestEventData();
        eventData.eventDetails = jsonInput;
        eventData.eventName = 'UBBThresholdBreach';
        ExternalEventRequestResponseParameter.RequestEventData eventData1 = new ExternalEventRequestResponseParameter.RequestEventData();
        eventData1.eventDetails = jsonInput;
        eventData1.eventName = 'UBBThresholdBreach';
        List<ExternalEventRequestResponseParameter.RequestEventData> eventList = new List<ExternalEventRequestResponseParameter.RequestEventData>();
        eventList.add(eventData);
        eventList.add(eventData1);
        SFDCExternalEventService_v1_0.createEvents(eventList);
        test.stopTest();
    }
    
    public static testMethod void testEEFrameworkSync(){
        createEmailTemplate('UBB_OU_Notification_75_EN');
        createEEConfig('UBBThresholdBreach', 'HSIAThresholdNotifyEventHandlerImpl', false);
        createEETemplateConfig('UBBThresholdBreach75English', 'UBB_OU_Notification_75_EN');
        Account accObj = createRCIDAccount();
        createBillingAccount(accObj.Id);
        createContact(accObj.Id);
        String jsonInput = '{"ServiceID":"1001020835","ECID":"0000","BillingAccountNumber":"987654test_xx","BillCycle":"4","Quota":"200 GB","SubscriberAddress":"809  HIGH FOREST PL BC LANGFORD  CAN","ThresholdPercentage":"75","ActualUsage":"465.81GB","HSIAPricePlan":"High Speed Extreme (V3)","ThresholdTimestamp":"November 5, 1994, 8:15:30 am UTC","BilllingSystemId":"119"}';
        ExternalEventRequestResponseParameter.RequestEventData eventData = new ExternalEventRequestResponseParameter.RequestEventData();
        eventData.eventDetails = jsonInput;
        eventData.eventName = 'UBBThresholdBreach';
        test.startTest();
        SFDCExternalEventService_v1_0.createEvent(eventData);
        test.stopTest();
    }
    public static testMethod void testEEFrameworkNotvalidAccount(){
        createEmailTemplate('UBB_OU_Notification_75_EN');
        createEEConfig('UBBThresholdBreach', 'HSIAThresholdNotifyEventHandlerImpl', false);
        createEETemplateConfig('UBBThresholdBreach75English', 'UBB_OU_Notification_75_EN');
        Account accObj = createRCIDAccount();
        createBillingAccount(accObj.Id);
        createContact(accObj.Id);
        String jsonInput = '{"ServiceID":"1001020835","ECID":"0000","BillingAccountNumber":"987654test_xx","BillCycle":"4","Quota":"200 GB","SubscriberAddress":"809  HIGH FOREST PL BC LANGFORD  CAN","ThresholdPercentage":"75","ActualUsage":"465.81GB","HSIAPricePlan":"High Speed Extreme (V3)","ThresholdTimestamp":"November 5, 1994, 8:15:30 am UTC","BilllingSystemId":"2016"}';
        ExternalEventRequestResponseParameter.RequestEventData eventData = new ExternalEventRequestResponseParameter.RequestEventData();
        eventData.eventDetails = jsonInput;
        eventData.eventName = 'UBBThresholdBreach';
        test.startTest();
        SFDCExternalEventService_v1_0.createEvent(eventData);
        test.stopTest();
    }
    public static testMethod void testEEFrameworkNotvalidTemplate(){
        createEmailTemplate('UBB_OU_Notification_100_ENFNTEst');
        createEEConfig('UBBThresholdBreach', 'HSIAThresholdNotifyEventHandlerImpl', false);
        createEETemplateConfig('UBBThresholdBreach75English', 'UBB_OU_Notification_75_EN');
        Account accObj = createRCIDAccount();
        createBillingAccount(accObj.Id);
        createContact(accObj.Id);
        String jsonInput = '{"ServiceID":"1001020835","ECID":"0000","BillingAccountNumber":"987654test_xx","BillCycle":"4","Quota":"200 GB","SubscriberAddress":"809  HIGH FOREST PL BC LANGFORD  CAN","ThresholdPercentage":"75","ActualUsage":"465.81GB","HSIAPricePlan":"High Speed Extreme (V3)","ThresholdTimestamp":"November 5, 1994, 8:15:30 am UTC","BilllingSystemId":"119"}';
        ExternalEventRequestResponseParameter.RequestEventData eventData = new ExternalEventRequestResponseParameter.RequestEventData();
        eventData.eventDetails = jsonInput;
        eventData.eventName = 'UBBThresholdBreach';
        test.startTest();
        SFDCExternalEventService_v1_0.createEvent(eventData);
        test.stopTest();
    }
    public static testMethod void testEEFrameworkThersoldAbove100(){
        createEmailTemplate('UBB_OU_Notification_100_EN');
        createEEConfig('UBBThresholdBreach', 'HSIAThresholdNotifyEventHandlerImpl', false);
        createEETemplateConfig('UBBThresholdBreach100English', 'UBB_OU_Notification_100_EN');
        Account accObj = createRCIDAccount();
        createBillingAccount(accObj.Id);
        createContact(accObj.Id);
        String jsonInput = '{"ServiceID":"1001020835","ECID":"0000","BillingAccountNumber":"987654test_xx","BillCycle":"4","Quota":"200 GB","SubscriberAddress":"809  HIGH FOREST PL BC LANGFORD  CAN","ThresholdPercentage":"101","ActualUsage":"465.81GB","HSIAPricePlan":"High Speed Extreme (V3)","ThresholdTimestamp":"November 5, 1994, 8:15:30 am UTC","BilllingSystemId":"119"}';
        ExternalEventRequestResponseParameter.RequestEventData eventData = new ExternalEventRequestResponseParameter.RequestEventData();
        eventData.eventDetails = jsonInput;
        eventData.eventName = 'UBBThresholdBreach';
        test.startTest();
        SFDCExternalEventService_v1_0.createEvent(eventData);
        test.stopTest();
    }
    public static testMethod void testEEFrameworAsset(){
        createEmailTemplate('UBB_OU_Notification_75_EN');
        createEEConfig('UBBThresholdBreach', 'HSIAThresholdNotifyEventHandlerImpl', false);
        createEETemplateConfig('UBBThresholdBreach75English', 'UBB_OU_Notification_75_EN');
        Account accObj = createRCIDAccount();
        Account banAcc = createBillingAccount(accObj.Id);
        createContact(accObj.Id);
        createAsset(banAcc.Id);
        String jsonInput = '{"ServiceID":"6477189999","ECID":"0000","BillingAccountNumber":"","BillCycle":"4","Quota":"200 GB","SubscriberAddress":"809  HIGH FOREST PL BC LANGFORD  CAN","ThresholdPercentage":"75","ActualUsage":"465.81GB","HSIAPricePlan":"High Speed Extreme (V3)","ThresholdTimestamp":"November 5, 1994, 8:15:30 am UTC","BilllingSystemId":"119"}';
        ExternalEventRequestResponseParameter.RequestEventData eventData = new ExternalEventRequestResponseParameter.RequestEventData();
        eventData.eventDetails = jsonInput;
        eventData.eventName = 'UBBThresholdBreach';
        test.startTest();
        SFDCExternalEventService_v1_0.createEvent(eventData);
        test.stopTest();
    }
    public static testMethod void testEEFrameworkInvalidJSON(){
        createEmailTemplate('UBB_OU_Notification_75_EN');
        createEEConfig('UBBThresholdBreach', 'HSIAThresholdNotifyEventHandlerImpl', true);
        createEETemplateConfig('UBBThresholdBreach75English', 'UBB_OU_Notification_75_EN');
        String jsonInput = '{"ServiceID":"","ECID":"","BillingAccountNumber":"","BillCycle":"","Quota":"","SubscriberAddress":"","ThresholdPercentage":"","ActualUsage":"","HSIAPricePlan":"","ThresholdTimestamp":"","BilllingSystemId":""}';
        ExternalEventRequestResponseParameter.RequestEventData eventData = new ExternalEventRequestResponseParameter.RequestEventData();
        eventData.eventDetails = jsonInput;
        eventData.eventName = 'UBBThresholdBreach';
        test.startTest();
        SFDCExternalEventService_v1_0.createEvent(eventData);
        //test logerror
         EventHandler_Helper.logError('HSIAThresholdNotifyEventHandlerImpl.processEvent', 'EE101' ,'test', 'test', 'Mediation');
        //end
        test.stopTest();
    }
    
    public static testMethod void testEEFrameworkInvalidJSON1(){
        createEmailTemplate('UBB_OU_Notification_75_EN');
        createEEConfig('UBBThresholdBreach', 'HSIAThresholdNotifyEventHandlerImpl', true);
        createEETemplateConfig('UBBThresholdBreach75English', 'UBB_OU_Notification_75_EN');
        ExternalEventRequestResponseParameter.RequestEventData eventData = new ExternalEventRequestResponseParameter.RequestEventData();
        eventData.eventDetails = '';
        eventData.eventName = '';
        test.startTest();
        SFDCExternalEventService_v1_0.createEvent(eventData);
        test.stopTest();
    }
    
    public static testMethod void testEEFrameworkInvalidJSON3(){
        createEmailTemplate('UBB_OU_Notification_75_EN');
        createEEConfig('UBBThresholdBreach', 'HSIAThresholdNotifyEventHandlerImpl', true);
        createEETemplateConfig('UBBThresholdBreach75English', 'UBB_OU_Notification_75_EN');
        ExternalEventRequestResponseParameter.RequestEventData eventData = new ExternalEventRequestResponseParameter.RequestEventData();
        eventData.eventDetails = '{"ServiceID":1001020835","ECID":"0000"}';
        eventData.eventName = 'UBBThresholdBreach';
        test.startTest();
        SFDCExternalEventService_v1_0.createEvent(eventData);
        // test for invalid clas name
        ExternalEventManager_Helper.createHandlerInstance('testclassname');
        // end test
        test.stopTest();
    }
    
    public static testMethod void testEEFrameworkInvalidClassName(){
        createEmailTemplate('UBB_OU_Notification_75_EN');
        createEEConfig('UBBThresholdBreach', 'HSIAThresholdNotifyEventHandlerImpl_test', true);
        createEETemplateConfig('UBBThresholdBreach75English', 'UBB_OU_Notification_75_EN');
        ExternalEventRequestResponseParameter.RequestEventData eventData = new ExternalEventRequestResponseParameter.RequestEventData();
        eventData.eventDetails = '{"ServiceID":1001020835","ECID":"0000"}';
        eventData.eventName = 'UBBThresholdBreach';
        test.startTest();
        SFDCExternalEventService_v1_0.createEvent(eventData);
        test.stopTest();
    }
    
    public static testMethod void testEEFrameworkRest1(){
        createEEConfig('UBBThresholdBreach', 'HSIAThresholdNotifyEventHandlerImpl', true);
        ExternalEventRequestResponseParameter.RequestEventData eventData = new ExternalEventRequestResponseParameter.RequestEventData();
        eventData.eventDetails = '';
        eventData.eventName = '';
        test.startTest();
        SFDCExternalEventRSService_v1_0.createEvent(eventData);
        test.stopTest();
    }
    
    public static testMethod void testEEFrameworkRest2(){
        createEEConfig('UBBThresholdBreach', 'HSIAThresholdNotifyEventHandlerImpl', true);
        ExternalEventRequestResponseParameter.RequestEventData eventData = new ExternalEventRequestResponseParameter.RequestEventData();
        eventData.eventDetails = '';
        eventData.eventName = '';
        List<ExternalEventRequestResponseParameter.RequestEventData> eventList = new List<ExternalEventRequestResponseParameter.RequestEventData>();
        eventList.add(eventData);
        test.startTest();
        SFDCExternalEventsRSService_v1_0.createEvents(eventList);
        test.stopTest();
    }
    
    /*public static testMethod void testReqResClass(){
        test.startTest();
        ExternalEventRequestResponseParameter Obj1 = new ExternalEventRequestResponseParameter();
        ExternalEventRequestResponseParameter.RequestEventData eventData = new ExternalEventRequestResponseParameter.RequestEventData();
        eventData.eventName = 'test';
        eventData.eventDetails = 'test';
        ExternalEventRequestResponseParameter.EventError errObj = new ExternalEventRequestResponseParameter.EventError();
        errObj.errorCode = 'test';
        errObj.errorDescription = 'test';
        ExternalEventRequestResponseParameter.ResponseEventData eventData1 = new ExternalEventRequestResponseParameter.ResponseEventData();
        eventData1.eventId = 'test';
        eventData1.error = new List<ExternalEventRequestResponseParameter.EventError>();
        eventData1.error.add(errObj);
        test.stopTest();
    }*/
}