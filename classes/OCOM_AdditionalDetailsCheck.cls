/********************************************************************************************************************************
Class Name:     OCOM_AdditionalDetailsCheck 
Purpose:        This class is a utility class to execute validate rules on additional detail step.      
Test class Name : OCOM_AdditionalDetailsCheck_test
Created by:     Niran Kanagan          
Modified by:    Aditya Jamwal      16-Feb-2016      Defect# 10518 : Contact Name, Contact Number and Contact Email not displayed for WLS devices.
                                                    Defect# 10516 : Contact Number and Email not visible in NC for TELUS SIM Card.
*********************************************************************************************************************************
*/
global with sharing class OCOM_AdditionalDetailsCheck implements vlocity_cmt.VlocityOpenInterface2 {
    public String OderId;
    public String errorMsgs; 
    public boolean isRemoteCall = false;
    public string sErrorMessage = '';
    public Map<String, String> errors = new Map<String, String>();
    global OCOM_AdditionalDetailsCheck(){}
    
    global Boolean invokeMethod(String methodName, Map<String, Object> input, Map<String, Object> output, Map<String, Object> options) {
        OderId = (String)input.get('ContextId');
        System.debug('Validate Additional Details $$ Order ID $$ ===========> '+OderId);
        if (methodName.indexOf('ValidateAndAmend')!=-1) {
            validateCreditAssessment(input, output, options);
            validateBillingAddress(input, output, options);
            validateShippingAddress(input, output, options);
            //validateContract(input, output, options);
            //ADHARSH: UNCOMMENTING OUT CONTRACT RELATED CODE FOR FULL LAUNCH
            if(errors == null || errors.size() == 0){
                amendContracts(input,output,options);
            }
            //ADHARSH: END
        }
        if(errors != null && errors.size()>0)
        {
            output.put('vlcValidationErrors', errors);
        }
        return true;
    }
    @TestVisible
    private void validateContract(Map<String, Object> inputMap, Map<String, Object> outputMap, Map<String, Object> options){
        System.debug('validateContract is called');
        Map<String,Id> contracReplacetRecordTypeMap = RecordTypeUtil.GetRecordTypeIdsByDeveloperName(Contract_Replacements__c.SObjectType);
        Map<String, Boolean> contractRulesMap = new Map<String, Boolean>();
        for (Contract_Rule__mdt rule : [select Id, DeveloperName, Is_Contract_Required_on_MTM__c from Contract_Rule__mdt where Contract_Record_Type__c = 'Contract']) {
            contractRulesMap.put(rule.DeveloperName, rule.Is_Contract_Required_on_MTM__c);
        }
        
        Map<String, vlocity_cmt__CatalogProductRelationship__c> contractableProductsMap = new Map<String, vlocity_cmt__CatalogProductRelationship__c>();
        for (vlocity_cmt__CatalogProductRelationship__c cpr : [Select id, vlocity_cmt__Product2Id__c, vlocity_cmt__CatalogId__r.Name From vlocity_cmt__CatalogProductRelationship__c where vlocity_cmt__CatalogId__r.Name in :contractRulesMap.keySet()]) {
            contractableProductsMap.put(cpr.vlocity_cmt__Product2Id__c, cpr);
        }
        
        
        
        List<OrderItem> orderItems = [SELECT Id, vlocity_cmt__RootItemId__c,contract_request__c, vlocity_cmt__Product2Id__c FROM OrderItem 
                                      WHERE OrderId = :OderId  ORDER BY vlocity_cmt__LineNumber__c ];
        
        for(OrderItem oi:orderItems){
            if(contractableProductsMap.containsKey(oi.vlocity_cmt__Product2Id__c) && String.isBlank(oi.contract_request__c)){
                errors.put('UIErrorContracts', 'Contract requests are missing');
                return;
            }
        }
        
    }
    private List<OrderItem> getOffers(string ordrId){
        List<OrderItem> items = [Select Id, OrderId,pricebookentry.product2.OCOM_Shippable__c,Shipping_Address__c,Billing_Address__c,vlocity_cmt__BillingAccountId__c,Billing_Account__c,OfferName__c, order.ServiceAddressSummary__c,vlocity_cmt__ParentItemId__c,Order.status, 
                                 Pricebookentry.Product2.Family,
                                 PriceBookEntry.product2.Sellable__c,
                                 Pricebookentry.Product2.vlocity_cmt__TrackAsAgreement__c,
                                 Pricebookentry.Product2.Name,amendStatus__c,
                                 PriceBookEntry.product2.IncludeQuoteContract__c,
                                 vlocity_cmt__LineNumber__c,Shipping_Address_Contact__c,
                                 //Start    Aditya Jamwal      16-Feb-2016      Defect# 10516,10518
                                 Shipping_Contact_Number__c,
                                 Shipping_Contact_Email__c,
                                 //End    Aditya Jamwal      16-Feb-2016      Defect# 10516,10518
                                 vlocity_cmt__JSONAttribute__c
                                 from OrderItem where orderId=:ordrId];
        
        return items;
    }
    
    private void validateCreditAssessment(Map<String, Object> inputMap, Map<String, Object> outputMap, Map<String, Object> options){
        Map<String, Object> creditOutputMap = new Map<String, Object>();
        OCOM_CreditAssessment_Helper.checkCredit(OderId, creditOutputMap);
        system.debug('OCOM_AdditionalDetailsCheck::validateCreditAssessment() creditOutputMap (' + creditOutputMap + ')');
        String creditFailureMessage = (String)creditOutputMap.get('validationFailureMessage');
        //if (String.isNotBlank(creditFailureMessage)) {
        //addErrorMsg(creditFailureMessage);
        //errors.put('UIErrorCredit', 'Credit Check is missing.');
        //}
        // Thomas OCOM-1233 Start
        if (creditOutputMap.get('noCreditProfile')==true) {
            errors.put('UIErrorCredit', system.Label.OCOM_Credit_Profile_Is_Missing);
        } else if (creditOutputMap.get('isCarRequired')==true && creditOutputMap.get('currentCARId')==null) {
            errors.put('UIErrorCredit', system.Label.OCOM_Credit_Check_Is_Missing);
        } else if (creditOutputMap.get('moreThan10PercentChange')==true) {
            errors.put('UIErrorCredit', system.Label.OCOM_CAR_Amend_Required);
        }
        // Thomas OCOM-1233 End
    }
    
   // Start : Added by Aditya Jamwal to validate if shipping details are captured on additional detail step.
    private void validateShippingAddress(Map<String, Object> inputMap, Map<String, Object> outputMap, Map<String, Object> options){
        List<OrderItem> items = getOffers(OderId);
        boolean missing = false;
        if(items !=null && items.size()>0){
            for(orderItem o:items){
                Boolean isShippableCharInJSON = false;
                Map<String,OCOM_VlocityJSONParseUtil.JSONWrapper> attributesMap = OCOM_VlocityJSONParseUtil.getAttributesFromJSON(o.vlocity_cmt__JSONAttribute__c);
                if(null != attributesMap && null!= OrdrConstants.CHAR_SHIPPING_REQUIRED && null != attributesMap.get(OrdrConstants.CHAR_SHIPPING_REQUIRED) && String.isNotBlank((attributesMap.get(OrdrConstants.CHAR_SHIPPING_REQUIRED)).value)
                   && (attributesMap.get(OrdrConstants.CHAR_SHIPPING_REQUIRED)).value.equalsIgnoreCase('Yes')){
                       isShippableCharInJSON= true;
                   }
                if((o.pricebookentry.product2.OCOM_Shippable__c == 'Yes' || isShippableCharInJSON) 
                   && (String.isBlank(o.Shipping_Address__c) || String.isBlank(o.Shipping_Address_Contact__c)
                       //Start    Aditya Jamwal      16-Feb-2016      Defect# 10516,10518
                       || String.isBlank(o.Shipping_Contact_Email__c) || String.isBlank(o.Shipping_Contact_Number__c)) ){
                       //End    Aditya Jamwal      16-Feb-2016      Defect# 10516,10518    
                       missing = true;
                   }
            }
        }
        if(missing)
            errors.put('UIErrorShipping', Label.OCOM_OLIShippingAddressValidationmsg);
    }    
   //End: Added by Aditya Jamwal to validate if shipping details are captured on additional detail step.
   
    private void validateBillingAddress(Map<String, Object> inputMap, Map<String, Object> outputMap, Map<String, Object> options) {
        // List<OrderItem> items = [Select Id, OrderId, Billing_Address__c,vlocity_cmt__BillingAccountId__c,Billing_Account__c, OCOMProductName__c, order.ServiceAddressSummary__c from OrderItem where orderId=:OderId and vlocity_cmt__ParentItemId__c = null];
        System.debug('Validate Additional Details $$ Order ID $$ ===========> '+OderId);
        List<OrderItem> items = getOffers(OderId);
        boolean missing = false;
        if (items !=null && items.size()>0) {
            for (OrderItem item :items) {
                if(String.isBlank(item.Billing_Account__c) && 
                   String.isBlank(item.vlocity_cmt__ParentItemId__c) && 
                   item.PriceBookEntry.product2.IncludeQuoteContract__c == true) 
                {
                    //addErrorMsg('Billing Address required for '+ item.OfferName__c);
                    missing = true;
                }
            }
        }
        if(missing)
            errors.put('UIErrorBilling', Label.OCOM_Billing_Addresses_are_missing);
        //Map<String, Object> myErrorMap = new Map<String, Object>();
        //myErrorMap.put('ValidationErrorDisplay', 'BillingAddress Test Error'); // additionalDetails
        //outputMap.put('vlcValidationErrors',myErrorMap);
        //outputMap.put('error', myErrorMap);
    }
    
    //ADHARSH: UNCOMMENTING OUT CONTRACT RELATED CODE FOR FULL LAUNCH
    
    public void amendContracts(Map<String, Object> inputMap, Map<String, Object> outputMap, Map<String, Object> options) {
        map<string,orderItem> LinenumberTOCliIDMap = new map<string,orderItem>();
        
        try { 
            String  OrderId = (string)inputMap.get('ContextId');
            Order objOrder = new Order(id=OrderId);
            boolean isItemsAmended = false;
            system.debug('Order _____:' + objOrder);
            
            //Iterate through Order Items to see if any of them are Amended.--- Check if we need to use a Specific value for AmendStatus
            list<OrderItem> orderItemsList = getOffers(OrderId);
            if (orderItemsList.size() > 0 && !orderItemsList.isEmpty()) {
                for (orderItem oli:orderItemsList) {
                    string lnumber = string.valueof(oli.vlocity_cmt__LineNumber__c) != null? string.valueof(oli.vlocity_cmt__LineNumber__c):'';
                    string prarentLineNumber  = OCOM_ContractUtil.getRootlineNumber(lnumber);   
                    if(prarentLineNumber != null && LinenumberTOCliIDMap.containsKey(prarentLineNumber) && oli.amendStatus__c != null){
                        isItemsAmended = true;
                        break; 
                    } else if(!LinenumberTOCliIDMap.containsKey(prarentLineNumber) && oli.PriceBookEntry.product2.Sellable__c == true &&  oli.Pricebookentry.Product2.vlocity_cmt__TrackAsAgreement__c == true ){
                        if( oli.amendStatus__c != null){
                            isItemsAmended = true;
                            system.debug('isItemsAmended_________:'+isItemsAmended + '__Item Name__'+ oli.Pricebookentry.Product2.Name);
                            break; 
                        } else{
                            LinenumberTOCliIDMap.put(prarentlinenumber,oli);
                            system.debug('LinenumberTOCliIDMap_________:'+LinenumberTOCliIDMap);
                        }
                    }
                }
            }
            if (isItemsAmended == false) {
                sErrorMessage = Label.SUCCESS;
            } else if(isItemsAmended == true) {
                map<String,object> outMap = OCOM_ContractUtil.AmendOrder(objOrder,'Amend');
                sErrorMessage=(string)outMap.get('amendContractResponse');
                // isRemoteCall=(boolean)outMap.get('isCallContinuation');
                System.debug('OutMap--->' + outMap);
            }
            if (sErrorMessage != Label.SUCCESS) {  
                errorMsgs = sErrorMessage;
                errors.put('UIErrorContracts', errorMsgs);
            }
            system.debug( '***sErrorMessage****'+sErrorMessage);
        } catch(exception e) {
            String errorMessage=Label.OCOM_ExceptionMessagePrefix+e.getMessage();
            System.debug(LoggingLevel.ERROR, 'Exception is '+e);
            System.debug(LoggingLevel.ERROR, 'Exception stack trace '+e.getStackTraceString());
            errorMsgs = errorMessage;
            errors.put('UIErrorContracts', errorMsgs);
            // outPutMap.put('error',errorMessage);
        }
    }
    //ADHARSH:COMMENT END
    /*
    private void addErrorMsg(String msg) {
    if(String.isBlank(errorMsgs))
    errorMsgs = msg;
    else
    errorMsgs = errorMsgs + '\n' + msg;
    }
    */
}