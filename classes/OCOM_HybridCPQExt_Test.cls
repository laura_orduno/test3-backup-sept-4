@isTest
public class OCOM_HybridCPQExt_Test{
    
    @IsTest
    private static void testInit(){
        
        Schema.DescribeSObjectResult d = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
        Schema.RecordTypeInfo rtByName =  rtMapByName.get('CAN');
        Id recTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Billing').getRecordTypeId();
        Id recSerTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        Id recRCIDTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId();
        
        Account RCIDacc = new Account(name='debs',recordtypeid=recRCIDTypeId);
        Account acc = new Account(name='Billing account', BAN_CAN__c='draft', recordtypeid=recTypeId);
        Account serAcc = new Account(name='123 street', recordtypeid=recSerTypeId, parentid=RCIDacc.id);
        
        
          
          
        List<Account> acclist= new List<Account>();          
        acclist.add(RCIDacc);
        acclist.add(acc);
        acclist.add(serAcc);
        
        insert acclist;
          
        SMBCare_Address__c address = new SMBCare_Address__c(account__c=serAcc.id); 
         
        Product2 prod = new Product2(Name = 'Laptop X200', Family = 'Hardware');
        insert prod;

        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod.Id, UnitPrice = 10000, IsActive = true);
        insert standardPrice;

        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;

        PricebookEntry customPrice = new PricebookEntry(Pricebook2Id = customPB.Id, Product2Id = prod.Id, UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        Contact c= new Contact(FirstName='abc', LastName='xyz');
        insert c;
        
        List< Credit_Assessment__c> CARList= new List<Credit_Assessment__c>();
        Credit_Assessment__c CAR= new Credit_Assessment__c(CAR_Status__c='test', CAR_Result__c='Denied');
        CARList.add(CAR);
        Credit_Assessment__c CAR2= new Credit_Assessment__c(CAR_Status__c='test',CAR_Result__c='Denied');
        CARList.add(CAR2);
        insert CARList;
         
        Order ord = new Order(Credit_Assessment__c=car.id,Ban__c='draft',Reason_Code__c='34aas',General_Notes_Remarks__c='123123', accountId=serAcc.id, effectivedate=system.today(), status='not submitted', Pricebook2Id=customPB.id, CustomerAuthorizedById=c.id, service_address__c=address.id);   
        Order ord2 = new Order(Credit_Assessment__c=car2.id,Ban__c='draft',Reason_Code__c='34aas',General_Notes_Remarks__c='123', accountId=acc.id, effectivedate=system.today(), status='Draft', Pricebook2Id=customPB.id, CustomerAuthorizedById=c.id, service_address__c=address.id);   
        List<Order> ordlist = new List<Order>();   
        ordList.add(ord);ordList.add(ord2);
        insert ordList; 
          
        OrderItem OLI2 = new OrderItem(UnitPrice=12, orderId= ord2.Id, orderMgmt_BPI_Id__c='9871379163517', vlocity_cmt__BillingAccountId__c=acc.id, vlocity_cmt__ServiceAccountId__c=serAcc.id, Quantity=2, PricebookEntryId=customPrice.id);
        OrderItem  OLI = new OrderItem(UnitPrice=12, orderId= ord.Id, vlocity_cmt__AssetReferenceId__c=OLI2.id, vlocity_cmt__BillingAccountId__c=acc.id, vlocity_cmt__ServiceAccountId__c=serAcc.id, Quantity=2, PricebookEntryId=customPrice.id);
          
        List<OrderItem>OLIlist= new List<OrderItem>();   
        OLIList.add(OLI2);
        OLIList.add(OLI);
        insert OLIList;
        
        
        
        asset ast= new asset(name='test', accountID=RCIDacc.id, vlocity_cmt__OrderId__c=ord2.id, status='Shipped');
        insert ast; 
       
        PageReference pageRef = Page.OCOM_HybridCPQ;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', ord.Id);
        ApexPages.currentPage().getParameters().put('src', 'os');
        
        //vlocity_cmt.CardCanvasController controller = new vlocity_cmt.CardCanvasController();
        //Updated by Arvind on 05/23/2017 to fix the validation issues..
        Object controller;
        OCOM_HybridCPQExt myPageExt = new OCOM_HybridCPQExt(controller);
        
        
        
        Test.startTest();
        PageReference p = myPageExt.init();
        
        //Added by danish on 27/03/2017 to cover UpdatePrivisioningStatusOnDisconnect method passing an order item Id as Parameter
        String IdOfLineItem = OLI.Id;
        OCOM_HybridCPQExt.UpdatePrivisioningStatusOnDisconnect(IdOfLineItem);  
        System.debug(myPageExt.isPartner);
        Test.stopTest();
                        
        List<ApexPages.Message> messages = ApexPages.getMessages();
        
        System.assertEquals(0, messages.size());
        
        Order theOrder = [SELECT Id, Service_Address__c, Service_Address__r.Id, Service_Address__r.Service_Account_Id__c, Service_Address__r.Service_Account_Id__r.Id, Service_Address__r.Service_Account_Id__r.ParentId, CustomerAuthorizedBy.Id FROM Order WHERE Id=: ord.Id];

        System.assertEquals(ApexPages.currentPage().getParameters().get('contactId'), theOrder .CustomerAuthorizedById);
        System.assertEquals(ApexPages.currentPage().getParameters().get('orderId'), theOrder.Id);
        System.assertEquals(ApexPages.currentPage().getParameters().get('smbCareAddrId'), theOrder.Service_Address__r.Id);
        System.assertEquals(ApexPages.currentPage().getParameters().get('ServiceAcctId'), theOrder.Service_Address__r.Service_Account_Id__r.Id);
        System.assertEquals(ApexPages.currentPage().getParameters().get('ParentAccId'), theOrder.Service_Address__r.Service_Account_Id__r.ParentId);
    }
}