/******************************************************************************
# File..................: AccountDetailController
# Version...............: 1
# Created by............: Sandip Chaudhari (IBM)
# Description...........: Class is to Create, Validate, Save Service Address for opportunity product. 
                          This class is main controller for detail page. Created as a part of PAC R2
******************************************************************************/
Public class AccountDetailController{
    public AddressData PAData  { get; set; }
    public String fullAddress { get; set; }
    Account accObj;
    public String isError{ get; set; }
    // Constructor to get required details like opportunity product item record, prepare full address, instantiate objects, mapping
    public AccountDetailController(ApexPages.StandardController controller){
        isError = 'onLoad';
        PAData = new AddressData();
        PAData.isAddressLine1Required = true;
        PAData.isProvinceRequired = true;
        PAData.isCityRequired = true;
       // address = new Address__c();
        accObj = (Account)controller.getRecord();
        accObj = [SELECT Id, BillingStreet, BillingCity, BillingState,
                        BillingCountry, BillingPostalCode, Billing_Address_Status__c
                        FROM Account
                        WHERE Id=: accObj.Id];
                        
        //Concatnate address to show on page
        if(accObj != null){
            fullAddress = '  ';
            if(accObj.BillingStreet != null && accObj.BillingStreet != 'null'){
                fullAddress = fullAddress  + ' ' + accObj.BillingStreet;
            }
            if(accObj.BillingCity != null && accObj.BillingCity != 'null'){
                fullAddress = fullAddress  + ' ' + accObj.BillingCity;
            }
            if(accObj.BillingState != null && accObj.BillingState != 'null'){
                fullAddress = fullAddress  + ' ' + accObj.BillingState;
            }
            if(accObj.BillingCountry != null && accObj.BillingCountry != 'null'){
                fullAddress = fullAddress  + ' ' + accObj.BillingCountry;
            }
            if(accObj.BillingPostalCode != null && accObj.BillingPostalCode != 'null'){
                fullAddress = fullAddress  + ' ' + accObj.BillingPostalCode;
            }
            
            PAData.searchString = fullAddress;
            PAData.addressLine1 = accObj.BillingStreet;
            PAData.city = accObj.BillingCity;
            PAData.province = accObj.BillingState;
            PAData.canadaProvince = accObj.BillingState;
            PAData.usState = accObj.BillingState;
            PAData.country = accObj.BillingCountry;
            PAData.postalCode = accObj.BillingPostalCode;
            
            if(accObj.Billing_Address_Status__c == System.Label.PACInValidCaptureNewAddrStatus){
                PAData.isManualCapture = true;
            }else{
                PAData.isManualCapture = false;
            }
        } 
    }
    
     /*
    * Created by :  Sandip Chaudhari
    * Name - updateBillingAddress 
    * Description - This method is map the PAC address values with the existing address fields
    * and update the object.
    */
    public pageReference updateBillingAddress (){
        mapAddress();
        try{
            isError = 'inSave';
            update accObj;
        }catch(Exception ex){
            isError = 'InError';
            ApexPages.addMessages(ex) ;
        }
        //PageReference redirectPage = new PageReference('/apex/AccountDetail?id='+ accObj.Id +'&sfdc.override=1');
        //redirectPage.setRedirect(true);
        //return redirectPage;
        return null;
    }
    
    /**
    * Name - mapAddress
    * Description - Map Billing address with existing respective fields
    * Param - 
    * Return type - void
    **/
    public void mapAddress(){
        system.debug('#### City' + PAData.city);
        system.debug('#### postalCode' + PAData.postalCode);
        accObj.BillingStreet = PAData.addressLine1;
        accObj.BillingCity = PAData.city;
        accObj.BillingState = PAData.province;//PAData.provinceCode;
        accObj.BillingCountry = PAData.country;
        accObj.BillingPostalCode = PAData.postalCode;
        accObj.Billing_Address_Status__c = PAData.captureNewAddrStatus;
    }
    
}