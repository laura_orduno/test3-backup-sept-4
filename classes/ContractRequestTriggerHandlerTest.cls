@isTest
private class ContractRequestTriggerHandlerTest {
    private static void createData(){  
        Id contractReqRecId = Contracts__c.SObjectType.getDescribe().getRecordTypeInfosByName().get('eContract').getRecordTypeId();
        Id contractRecId = Contract.SObjectType.getDescribe().getRecordTypeInfosByName().get('Corporate Wireless').getRecordTypeId();
        Id contractLineRecordId= vlocity_cmt__ContractLineItem__c.SObjectType.getDescribe().getRecordTypeInfosByName().get('eContract Line Item').getRecordTypeId();
        Id rcidAccntRecordTypeId=Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('RCID').getRecordTypeId();
        Account acct = new Account(Name='testacct', Phone='9222022768', RecordTypeId=rcidAccntRecordTypeId);
        insert acct;
        Contact testContactObj = new Contact(firstname = 'TestConFirstName', LastName = 'TestConLastname', accountId = acct.Id , 
                                             Phone = '123456' , fax = '1234567', Email = 'test@gmail.com');
        insert testContactObj;
        Opportunity opp = new Opportunity(AccountId=acct.id, Name='testopp', CloseDate=system.today(),StageName='test stage');
        insert opp;        
        Contracts__c ConReq= new Contracts__c(RecordTypeId=contractReqRecId,Contract_Type__c='New',Opportunity__c=opp.id );
        insert ConReq;   
        Contract testContractObj = new Contract(Complex_Fulfillment_Contract__c=ConReq.id,Status='Draft',Contract_Type__c='Amendment',
                                                AccountId=acct.id,ContractTerm=12,StartDate=system.today(),
                                                Customer_Signor__c=testContactObj.id,TELUS_Signor__c=userinfo.getuserid(), Agreement_Type__c = 'Amendment');
        insert testContractObj;
        
        Product2 testProductObj = new Product2(Name = 'Laptop X200', 
                                               Family = 'Hardware' ,vlocity_cmt__TrackAsAgreement__c = TRUE, Sellable__c = true);
        insert testProductObj;
        List<vlocity_cmt__ContractLineItem__c> testContractLineItemList =new List<vlocity_cmt__ContractLineItem__c>();
        vlocity_cmt__ContractLineItem__c cli1 =  new vlocity_cmt__ContractLineItem__c(vlocity_cmt__RecurringTotal__c=9,TELUS_Printed_Product_Name__c = testProductObj.Name,vlocity_cmt__Product2Id__c=testProductObj.id,vlocity_cmt__ContractId__c=testContractObj.id,vlocity_cmt__OneTimeTotal__c=12,name='test0',vlocity_cmt__OneTimeCharge__c=2,vlocity_cmt__Quantity__c=12,vlocity_cmt__LineNumber__c = '0001');
        testContractLineItemList.add(cli1);
        
        vlocity_cmt__ContractLineItem__c cli2 = new vlocity_cmt__ContractLineItem__c(vlocity_cmt__RecurringTotal__c=6,vlocity_cmt__Product2Id__c=testProductObj.id,vlocity_cmt__ContractId__c=testContractObj.id,vlocity_cmt__OneTimeTotal__c=6,name='test1',vlocity_cmt__OneTimeCharge__c=3,vlocity_cmt__Quantity__c=12,TELUS_Printed_Product_Name__c = testProductObj.Name,vlocity_cmt__LineNumber__c = '0001.0001');
        
        testContractLineItemList.add(cli2);
        vlocity_cmt__ContractLineItem__c cli3  = new vlocity_cmt__ContractLineItem__c(vlocity_cmt__RecurringTotal__c=3,vlocity_cmt__Product2Id__c=testProductObj.id,vlocity_cmt__ContractId__c=testContractObj.id,vlocity_cmt__OneTimeTotal__c=5,name='test2',vlocity_cmt__OneTimeCharge__c=4,vlocity_cmt__Quantity__c=12,TELUS_Printed_Product_Name__c = testProductObj.Name,vlocity_cmt__LineNumber__c = '0002');
        testContractLineItemList.add(cli3);
        
        vlocity_cmt__ContractLineItem__c cli4 =  new vlocity_cmt__ContractLineItem__c(vlocity_cmt__RecurringTotal__c=3,vlocity_cmt__Product2Id__c=testProductObj.id,vlocity_cmt__ContractId__c=testContractObj.id,vlocity_cmt__OneTimeTotal__c=5,name='test3',vlocity_cmt__OneTimeCharge__c=4,vlocity_cmt__Quantity__c=12,TELUS_Printed_Product_Name__c = testProductObj.Name,vlocity_cmt__LineNumber__c = '0002.0001');
        
        testContractLineItemList.add(cli4);
        insert  testContractLineItemList;
    }
    @isTest
    private static void createContractRequest() { 
        Test.startTest();
        createData();
        Test.stopTest();
    }
}