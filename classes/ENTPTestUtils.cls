@IsTest
public with sharing class ENTPTestUtils {

	public static final String testUserPassword = 'Traction123..';

	public static void createENTPCustomSetting() {
		ENTP_Customer_Interface_Settings__c cis = new ENTP_Customer_Interface_Settings__c();
		cis.Manage_Requests_List_Limit__c = 10;
		cis.Overview_Case_Limit__c = 5;

		insert cis;
	}

	public static void createCaseRootCauseCronicSetting() {
		Case_RootCause_Cronic__c rc = new Case_RootCause_Cronic__c();
		rc.name = 'Billing';
		rc.Picklist_Strings__c = 'Billing';

		insert rc;
	}

	public static void createSMBCareWebServicesCustomSetting() {
		List<SMBCare_WebServices__c> lst = new List<SMBCare_WebServices__c>();

		SMBCare_WebServices__c cis = new SMBCare_WebServices__c();
		cis.name = 'username';
		cis.value__c = 'APP_SFDC';
		insert cis;

		cis = new SMBCare_WebServices__c();
		cis.name = 'username_pc';
		cis.value__c = 'APP_PRIVCLOUD';
		insert cis;

		cis = new SMBCare_WebServices__c();
		cis.name = 'password';
		cis.value__c = 'soaorgid';
		insert cis;

		cis = new SMBCare_WebServices__c();
		cis.name = 'TTODS_EndPoint';
		cis.value__c = 'https://xmlgwy-pt1.telus.com:9030/pt01/SMO/ProblemMgmt/TroubleTicketODSQueryService_v1_5_vs0';
		insert cis;

		cis = new SMBCare_WebServices__c();
		cis.name = 'TTODS_Endpoint_Search_Lynx';
		cis.value__c = 'https://xmlgwy-pt1.telus.com:9030/pt01/SMO/ProblemMgmt/TroubleTicketODSQueryService_v1_5_vs0';
		insert cis;

		cis = new SMBCare_WebServices__c();
		cis.name = 'TTODS_Endpoint_Update_Lynx';
		cis.value__c = 'https://xmlgwy-pt1.telus.com:9030/pt01/SMO/ProblemMgmt/LynxTroubleTicketMgmtService_v1_0_vs0';
		insert cis;

		cis = new SMBCare_WebServices__c();
		cis.name = 'TTODS_Endpoint_Create_Lynx';
		cis.value__c = 'https://xmlgwy-pt1.telus.com:9030/pt01/SMO/ProblemMgmt/LynxTroubleTicketMgmtService_v1_0_vs0';
		insert cis;

		cis = new SMBCare_WebServices__c();
		cis.name = 'ENDPOINT_Remedy_View';
		cis.value__c = 'https://xmlgwy-pt1.telus.com:9030/dv/SMO/ProblemMgmt/ExternalTicketViewService_v1_0_vs0';
		insert cis;

	}

	public static List<CCIGetHelp__c> createHelpQuestions() {
		List<CCIGetHelp__c> questions = new List<CCIGetHelp__c>();

		questions.add(new CCIGetHelp__c(
				Portal__c = 'ENTP',
				Category__c = 'General Questions',
				Question__c = 'how do we test this controller?',
				Published__c = true,
				Answer__c = 'super easy, like one, two, three'
		)
		);

		questions.add(new CCIGetHelp__c(
				Portal__c = 'ENTP',
				Category__c = 'Profile and Settings',
				Question__c = 'how do we test controller?',
				Published__c = true,
				Answer__c = 'super easy'
		)
		);

		insert questions;

		return questions;
	}

	public static List<Case> createENTPCases(Integer numToInsert, Id accountId) {
		return createENTPCases(numToInsert, accountId, true);
	}

	public static List<Case> createENTPCases(Integer numToInsert, Id accountId, Boolean doInsert) {
		trac_TriggerHandlerBase.blockTrigger = true;
		List<Case> cases = new List<Case>();
		for (Integer i = 0; i < numToInsert; i++) {

			Case theCase = new Case();
			theCase.Account__c = accountId;
			theCase.Subject = 'TEST SUBJECT';
			theCase.Description = 'Condition: In Service';
			theCase.Status = 'New';
			theCase.Origin = 'ENTP';
			theCase.Root_Cause__c = 'Billing';
			theCase.Request_Type__c = 'Other';
			theCase.Priority = 'Medium';
			theCase.Lynx_Ticket_Number__c = '1111111111';
			theCase.My_Business_Requests_Type__c = 'Mainstream';
			theCase.NotifyCollaboratorString__c = 'testtest@unit-test.com;testtest2@unit-test.com';
			theCase.ENTP_Condition__c = 'In service';
			theCase.ENTP_AccessDay__c = 'Tuesday';
			theCase.Customer_Phone_contact__c = '5555555555';
			theCase.Customer_First_Name_contact__c = 'John';
			theCase.Project_number__c = '1234567';
			theCase.Contact_Phone_Number2__c = '5555555555';
			// theCase.contact.LastName = 'Doe';
			cases.add(theCase);
		}
		if (doInsert) {
			insert cases;
		}
		System.debug('2014-11-15 MBRTestUtils.createENTPCases(): inserted cases: ' + cases);
		trac_TriggerHandlerBase.blockTrigger = false;
		return cases;
	}
	public static EmailMessage createEmail(Id pId, Boolean incom, String emailBody) {
		EmailMessage em = new EmailMessage(ParentId = pId,
				Incoming = incom,
				TextBody = emailBody + '\n write above this line [ref:_001]',
				Subject = '[ref:_001]',
				FromAddress = 'rzhenissova@tractionondemand.com',
				ToAddress = 'mybusinessrequest@telus.com'
		);
		System.debug('2014-11-15 MBRTestUtils.createEmail(): returning EmailMessage em: ' + em);
		return em;
	}

	public static List<Attachment> createAttachment(Id pId, Integer numToInsert, String ext, DateTime ModDateTime) {
		trac_TriggerHandlerBase.blockTrigger = true;
		List<Attachment> attList = new List<Attachment>{
		};
		for (Integer i = 0; i < numToInsert; i++) {
			Attachment att = new Attachment(parentId = pId,
					Name = 'test attachment.' + ext,
					Body = Blob.valueOf('Unit Test Attachment Body ' + i),
					LastModifiedDate = ModDateTime,
					CreatedDate = ModDateTime);
			attList.add(att);
		}
		insert attList;
		trac_TriggerHandlerBase.blockTrigger = false;
		return attList;
	}

	public static void createComment(Id caseId, boolean emailComment, Boolean isPublished) {
		trac_TriggerHandlerBase.blockTrigger = true;
		if (emailComment == true) {
			CaseComment comm = new CaseComment(
					ParentId = caseId,
					CommentBody = 'testEmail@testEmail.com - :' + Label.MBREmailCommentAuthor + ' - test comment',
					isPublished = isPublished);
			insert comm;
		} else {
			CaseComment comm = new CaseComment(
					ParentId = caseId,
					CommentBody = 'test comment',
					isPublished = isPublished);
			insert comm;
		}
		trac_TriggerHandlerBase.blockTrigger = false;
	}
	public static List<ExternalToInternal__c> createExternalToInternalCustomSettings() {
		List<ExternalToInternal__c> eis = new List<ExternalToInternal__c>{
				new ExternalToInternal__c(Name = 'request1', External__c = 'Billing inquiry', Identifier__c = 'onlineStatus', Internal__c = 'SMB Care Billing'),
				new ExternalToInternal__c(Name = 'request2', External__c = 'Account review', Identifier__c = 'onlineRequestTypeToCaseRT', Internal__c = 'Generic'),
				new ExternalToInternal__c(Name = 'request3', External__c = 'Generic', Identifier__c = 'onlineStatus', Internal__c = 'Generic'),
				new ExternalToInternal__c(Name = 'request4', External__c = 'Test', Identifier__c = 'onlineRequestTypeToCaseRT', Internal__c = 'Generic'),
				new ExternalToInternal__c(Name = 'request5', External__c = 'Dummy', Identifier__c = 'onlineCaseRTToRequestType', Internal__c = 'Generic'),
				new ExternalToInternal__c(Name = 'test', Identifier__c = 'onlineRequestTypeToCaseRT', Internal__c = 'SMB Care Product', External__c = 'New product/service - add-on order'),
				new ExternalToInternal__c(Name = 'test1', Identifier__c = 'onlineRequestTypeToCaseRT', Internal__c = 'SMB Care Product', External__c = 'New product/service - hardware/accessory order'),
				new ExternalToInternal__c(Name = 'test2', Identifier__c = 'onlineRequestTypeToCaseRT', Internal__c = 'SMB Care Product', External__c = 'New product/service - renewal order'),
				new ExternalToInternal__c(Name = 'test3', Identifier__c = 'onlineRequestTypeToCaseRT', Internal__c = 'SMB Care Product', External__c = 'Change service'),
				new ExternalToInternal__c(Name = 'test4', Identifier__c = 'onlineRequestTypeToCaseRT', Internal__c = 'SMB Care Product', External__c = 'Transfer of business ownership'),
				new ExternalToInternal__c(Name = 'test5', Identifier__c = 'onlineRequestTypeToCaseRT', Internal__c = 'SMB Care Product', External__c = 'Account Update - update authorized person'),
				new ExternalToInternal__c(Name = 'test6', Identifier__c = 'onlineRequestTypeToCaseRT', Internal__c = 'SMB Care Product', External__c = 'Billing inquiry'),

				new ExternalToInternal__c(Name = 'ENTPTest', Identifier__c = 'onlineRequestTypeENTP', Internal__c = 'ENT Care Assure', External__c = 'Enhanced - Standard (i.e. Ethernet 10 Mg, 100Mg, WAN ADSL, etc.)'),
				new ExternalToInternal__c(Name = 'ENTPTest1', Identifier__c = 'onlineRequestTypeENTP', Internal__c = 'ENT Care Assure', External__c = 'L1L (Leased Loops, Unbundled Loops) (i.e. No Dial Tone)'),
				new ExternalToInternal__c(Name = 'ENTPTest2', Identifier__c = 'onlineRequestTypeENTP', Internal__c = 'ENT Care Assure', External__c = 'Mainstream/Data (i.e. T1, T3, DS0, etc)'),
				new ExternalToInternal__c(Name = 'ENTPTest3', Identifier__c = 'onlineRequestTypeENTP', Internal__c = 'ENT Care Assure', External__c = 'Voice - PRI'),
				new ExternalToInternal__c(Name = 'ENTPTest4', Identifier__c = 'onlineRequestTypeENTP', Internal__c = 'ENT Care Assure', External__c = '	Voice - Phone Number (i.e. 10 digit)')
		};

		insert eis;
		return eis;
	}

}