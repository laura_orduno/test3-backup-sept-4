public without sharing class QuoteConfigFieldSetModel {
    public Integer index {get; set;}
    public AdditionalInformation__c additionalInformation {get; set;}
    public List<ConfigFieldViewModel> configFieldViewModelList {get; set;}
    private String productCategory;
    private Product2 product;
    
	public QuoteConfigFieldSetModel(Integer index, AdditionalInformation__c additionalInformation, List<QuoteConfigFieldVO> fieldViewModelList, String productCategory, String quoteStatus, Product2 product) {
		this.product = product;
		this.productCategory = productCategory;
    	this.index = index;
    	this.additionalInformation = additionalInformation;
    	this.configFieldViewModelList = buildViewModelList(fieldViewModelList, quoteStatus);
	}
	
	private List<ConfigFieldViewModel> buildViewModelList(List<QuoteConfigFieldVO> configFields, String quoteStatus) {
		System.assert(configFields != null, 'configFields argument is null');
    	List<ConfigFieldViewModel> viewModelList = new List<ConfigFieldViewModel>();
		String phoneNumberSetup;
		if('Existing Device'.equalsIgnoreCase(productCategory)) {
			phoneNumberSetup = additionalInformation.Phone_Number_Setup_QED__c;
		} else {
			phoneNumberSetup = additionalInformation.Phone_Number_Setup__c; 
		}
		Boolean isPorting = 'Porting'.equalsIgnoreCase(phoneNumberSetup) ;
		Boolean isMaintainWC = 'Maintain - with change'.equalsIgnoreCase(phoneNumberSetup) ;
        Boolean isMaintainNC = 'Maintain - no change'.equalsIgnoreCase(phoneNumberSetup) ;
        Boolean isMaintain = 'Maintain'.equalsIgnoreCase(phoneNumberSetup) ;
         if (isMaintainWC){isMaintain=true;}if (isMaintainNC){isMaintain=true;}
		Boolean isNewNumber = 'New Number'.equalsIgnoreCase(phoneNumberSetup) ;
    	Integer quoteStatusOrdinal = QuoteCustomConfigurationService.getRequiredOrdinalByStatus(quoteStatus);
    	Integer fieldRequiredStageOrdinal;
    	for(QuoteConfigFieldVO quoteConfigFieldVO : configFields){
			ConfigFieldViewModel viewModel = new ConfigFieldViewModel(quoteConfigFieldVO);
			fieldRequiredStageOrdinal = QuoteCustomConfigurationService.getRequiredOrdinalByStatus(quoteConfigFieldVO.getRequiredStage());
			viewModel.required = fieldRequiredStageOrdinal <= quoteStatusOrdinal;
			if('Wireless'.equalsIgnoreCase(productCategory)) {
				
				if (viewModel.quoteConfigFieldVO.fieldName == 'First_Name__c'||
                       viewModel.quoteConfigFieldVO.fieldName == 'Last_Name__c' ||
                       viewModel.quoteConfigFieldVO.fieldName == 'City_of_Use__c' ||
                       viewModel.quoteConfigFieldVO.fieldName == 'Province__c' ||
                       viewModel.quoteConfigFieldVO.fieldName == 'ESN_IMEI__c') {
                           viewModel.visible = !isMaintainNC;
                    }
				
				if(('Old_Service_Provider__c'.equalsIgnoreCase(quoteConfigFieldVO.fieldName) ||
					'Porting__c'.equalsIgnoreCase(quoteConfigFieldVO.fieldName) ||
					'Old_Service_Provider_Account_Number__c'.equalsIgnoreCase(quoteConfigFieldVO.fieldName))) {
					viewModel.visible = isPorting;						
				} else if ('Mobile__c'.equalsIgnoreCase(quoteConfigFieldVO.fieldName)) {
					viewModel.visible = isMaintain;
				} else if ('SIM__c'.equalsIgnoreCase(quoteConfigFieldVO.fieldName) && !isMaintainNC) {
					viewModel.visible = isHSPA();
				}
			} else if('Office Phone'.equalsIgnoreCase(productCategory) && isMaintainNC == false) {
				
				if(viewModel.quoteConfigFieldVO.fieldName == 'Directory_Listing__c' ||
                       viewModel.quoteConfigFieldVO.fieldName == 'Directory_Listing_Same_As_Business_Name__c' ||
                       viewModel.quoteConfigFieldVO.fieldName == 'Directory_Listing_Display__c' ||
                       viewModel.quoteConfigFieldVO.fieldName == 'Destination_Number__c') {
                        viewModel.visible = !isMaintainNC;
                    }
                    
				if(('Street__c'.equalsIgnoreCase(quoteConfigFieldVO.fieldName) ||
					'City__c'.equalsIgnoreCase(quoteConfigFieldVO.fieldName) ||
					'Province__c'.equalsIgnoreCase(quoteConfigFieldVO.fieldName) ||
					'Postal_Code__c'.equalsIgnoreCase(quoteConfigFieldVO.fieldName) ||
					'Porting__c'.equalsIgnoreCase(quoteConfigFieldVO.fieldName))) {
					viewModel.visible = isPorting;
				} else if ('Billing_Telephone_Number_BTN__c'.equalsIgnoreCase(quoteConfigFieldVO.fieldName)) {
					viewModel.visible = isMaintain;
				}
			} else if('Office Phone'.equalsIgnoreCase(productCategory) && isMaintainNC == true) {
                if ('Billing_Telephone_Number_BTN__c'.equalsIgnoreCase(quoteConfigFieldVO.fieldName)) {
                    viewModel.visible = isMaintain;
                }
            } else if('Existing Device'.equalsIgnoreCase(productCategory)) {
				if(('City_of_Use__c'.equalsIgnoreCase(quoteConfigFieldVO.fieldName) ||
					'Province__c'.equalsIgnoreCase(quoteConfigFieldVO.fieldName))) {
					viewModel.visible = isNewNumber;
				} else if('Mobile__c'.equalsIgnoreCase(quoteConfigFieldVO.fieldName)) {
					viewModel.visible = isMaintain;
				}
			}
			viewModelList.add(viewModel);				        		
    	}
    	return viewModelList;
    }
    
    public Boolean isHSPA() {
    	return (product!= null && 'HSPA'.equals(product.Network__c)) || additionalInformation.Network_Type__c == 'HSPA';
    }
    
	public class ConfigFieldViewModel {
		public QuoteConfigFieldVO quoteConfigFieldVO {get; set;}
		public Boolean valid {get; set;}
		public Boolean visible {get; set;}
		public Boolean required {get; set;}
		
		public ConfigFieldViewModel(QuoteConfigFieldVO quoteConfigFieldVO) {
			this.quoteConfigFieldVO = quoteConfigFieldVO;
			this.valid = true;
			this.visible = quoteConfigFieldVO.initVisible;
		}
	} 
}