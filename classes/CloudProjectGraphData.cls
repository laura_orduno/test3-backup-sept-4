public class CloudProjectGraphData{
    public String projectId;
    public String projectName;
    public Decimal allocationAmount;
    // used to generate allocation percentage
    public Decimal maxAllocationAmount;
    public Decimal allocationPercentage;
    public String startDate;
    public String releaseDate;
        
    public CloudProjectGraphData(Id projectId, String projectName, Decimal allocationAmount, Decimal maxAllocationAmount, Decimal allocationPercentage, String startDate, String releaseDate)
    {
        this.projectId = String.valueOf(projectId);
    	this.projectName = projectName;
    	this.allocationAmount = allocationAmount;
    	this.maxAllocationAmount = maxAllocationAmount;
    	this.allocationPercentage = allocationPercentage;
    	this.startDate = startDate;
    	this.releaseDate = releaseDate;
    }           
}