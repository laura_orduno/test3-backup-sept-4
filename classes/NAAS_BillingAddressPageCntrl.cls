public class NAAS_BillingAddressPageCntrl {
public Order objOrder{get;set;}
public string orderId{get;set;}
public list<string> filter ;
public boolean arecontractable{get;set;}
public static Map<String,String> canadaPostValMap {get;set;}
public static boolean isCPValdationComplete = false;
public static boolean isShippableProductPresent{get;set;}
public NAAS_BillingAddressPageCntrl(){
    system.debug('%%######%% START: NAAS_BillingAddressPageCntrl:NAAS_BillingAddressPageCntrl  %%######%% ');
    objOrder=new Order();    
    orderId=ApexPages.currentPage().getparameters().get('id');
    objOrder = getOrderDetail();
    arecontractable=false;
    isShippableProductPresent=false;
    system.debug('%%######%% END: NAAS_BillingAddressPageCntrl:NAAS_BillingAddressPageCntrl %%######%% ');
}
//Start : Added by Aditya Jamwal (IBM) to validate Shipping Address from Canada Post....  
public void doShippingAddrValidation(){   
    system.debug('%%######%% START: NAAS_BillingAddressPageCntrl:doShippingAddrValidation  %%######%% ');
    If(!isCPValdationComplete){  
    canadaPostValMap  = new Map<String,String>();
    system.debug('!!!Shipping Address '+orderId);
    List<OrderItem> ordrItemRecs;
    List<OrderItem> updateOrdrItemRecs = new List<OrderItem> ();
    try{
    ordrItemRecs =[select id,order.CustomerAuthorizedBy.Name,Shipping_Address_Contact__c,pricebookentry.product2.OCOM_Shippable__c,Shipping_Address__c,order.Service_Address__r.city__c,order.Service_Address__r.province__c,order.ServiceAddressSummary__c,orderId,vlocity_cmt__JSONAttribute__c,order.Service_Address__r.Street__c,order.Service_Address__r.Building_Number__c,order.Service_Address__r.Postal_Code__c from OrderItem where orderId =:orderId];
    }catch(exception ex){}    
     system.debug('!!!Shipping Address ordrItemRecs '+ordrItemRecs);
    NAAS_ShippingAddrCPValdationUtility.serviceAddressData saData = new NAAS_ShippingAddrCPValdationUtility.serviceAddressData();        
    if(null !=ordrItemRecs  &&ordrItemRecs.size()>0){
    String status=' ';
    Map<String,String> charactersticValueMap = new  Map<String,String>();
     if(null!=ordrItemRecs[0].order.Service_Address__c){         
            charactersticValueMap.put('Shipping Address Characteristic',ordrItemRecs[0].order.ServiceAddressSummary__c);
            charactersticValueMap.put('Shipping Address',ordrItemRecs[0].order.ServiceAddressSummary__c);
            charactersticValueMap.put('City of use',ordrItemRecs[0].order.Service_Address__r.city__c);
            charactersticValueMap.put('Province of use',ordrItemRecs[0].order.Service_Address__r.province__c);
         saData.postalCode=ordrItemRecs[0].order.Service_Address__r.Postal_Code__c ; saData.streetName=ordrItemRecs[0].order.Service_Address__r.Street__c; saData.buildingNumber=ordrItemRecs[0].order.Service_Address__r.Building_Number__c;
       try{ 
         status= NAAS_ShippingAddrCPValdationUtility.validateAddress(saData);
          }catch(exception ex){}  
     }  if(Test.isRunningTest()){status='valid'; }
        boolean isContactNeeded=false;boolean isShippingAddressNeeded=false;
       for(orderItem oli :ordrItemRecs ){
           
             if(oli.pricebookentry.product2.OCOM_Shippable__c == 'Yes'){
               isShippableProductPresent=true;
             system.debug('!!!!oli'+status+' '+oli.Shipping_Address__c);   
              if(String.isBlank(oli.Shipping_Address__c) && status=='valid'){
                 
                 oli.Shipping_Address__c=oli.order.ServiceAddressSummary__c; 
                 String JsonChar =oli.vlocity_cmt__JSONAttribute__c;    
                  system.debug('!!!!debug before charactersticValueMap'+charactersticValueMap); 
                 for(string key :charactersticValueMap.keySet()){
                     //  system.debug('!!!!debug before charactersticValueMap'+charactersticValueMap); 
                       JsonChar=(String) OCOM_Capture_OLI_ShippingAddController.readOLICharacterstic(JsonChar,key,'Update',charactersticValueMap);
                       system.debug('!!!!debug After JSON '+JsonChar); 
                     }
                 oli.vlocity_cmt__JSONAttribute__c= JsonChar;                
                isShippingAddressNeeded =true; 
             }
              if(null!= oli.order.CustomerAuthorizedBy && String.isNotBlank(oli.order.CustomerAuthorizedBy.Name) && String.isBlank(oli.Shipping_Address_Contact__c)){
               oli.Shipping_Address_Contact__c = oli.order.CustomerAuthorizedBy.Name;
               isContactNeeded=true;
             } 
                 if(isShippingAddressNeeded || isContactNeeded){ updateOrdrItemRecs.add(oli); }  
                 isShippingAddressNeeded = isContactNeeded =false;
           }             
             canadaPostValMap.put(oli.order.ServiceAddressSummary__c,status);
        } 
       update updateOrdrItemRecs;
    } 
   } 
    system.debug(' canadaPostValMap '+canadaPostValMap);
    system.debug('%%######%% END: NAAS_BillingAddressPageCntrl:doShippingAddrValidation  %%######%% ');    
}

//End: Added by Aditya Jamwal (IBM) to validate Shipping Address from Canada Post....    

 public Order getOrderDetail(){
     system.debug('%%######%% START: NAAS_BillingAddressPageCntrl:getOrderDetail  %%######%% ');
     try {
    if(filter ==null && orderId != null){
            //System.debug('Filter: ' + filter + '; rel: ' + relFieldName + '; recId: ' + orderId);
            
            filter = new List<String>{'id' + '=\''+ orderId +'\'' };
        }
        System.debug('Filter: ' + filter + '; rel: ' + 'id' + '; recId: ' + orderId);
    List<string> fieldNames = new List<string>{'id','ContactEmail__c',
                                            'ContactName__c',
                                            'ContactPhone__c','ContactTitle__c','DeliveryMethod__c',
                                            'Service_Address_Text__c','Account.Id','AccountId',
                                            'CustomerAuthorizedByID','CustomerAuthorizedBy.Name','CustomerSignedBy__c',
                                            'CustomerAuthorizedBy.Email','CustomerAuthorizedBy.Phone','CustomerAuthorizedBy.Title',
                                             'CustomerSignedBy__r.Name','CustomerSignedBy__r.Email','CustomerSignedBy__r.Phone','CustomerSignedBy__r.Title'};
        System.debug('getting query');
        String query = Util.queryBuilder('Order', fieldNames, filter);
        System.debug(LoggingLevel.Error,'query :' + query);
        list<order> order = Database.query(query);
    if(order.size() > 0 && !order.isEmpty() )
        objOrder = order[0];
     }catch(Exception anException){throw anException;return null;}
     system.debug('%%######%% END: NAAS_BillingAddressPageCntrl:getOrderDetail  %%######%% ');
     return objOrder;
}

}