public class VITILcareCaseTriggerHelper {

    public static void afterUpdate(Map<Id, Case> oldMap, Map<Id, Case> newMap){
		System.debug('VITILcareCaseTriggerHelper.afterUpdate');

		Set<Id> vitilCareClosedCaseIds = new Set<Id>();
        
        for(Id caseId : newMap.keyset()){
            Case caseNew = newMap.get(caseId);
            Case caseOld = oldMap.get(caseId);
            
            System.debug('caseNew: ' + caseNew.origin + ', ' + caseNew.status + ', ' + caseNew.Lynx_Ticket_Number__c);     
            System.debug('caseOld: ' + caseOld.origin + ', ' + caseOld.status + ', ' + caseOld.Lynx_Ticket_Number__c);     
            
            Boolean isNewCase = caseNew.status == 'new';
            Boolean isClosedCase = caseNew.status == 'closed' && caseNew.status != caseOld.status;
            System.debug('isClosedCase: ' + isClosedCase);     
            Boolean isVITILcareOrigin = caseNew.Origin == 'VITILcare';
            Boolean lynxTicketNumbersNotNull = caseNew.Lynx_Ticket_Number__c != null && caseNew.Lynx_Ticket_Number__c.length() > 0;
            Boolean lynxTicketNumberAcquired = caseOld.Lynx_Ticket_Number__c != caseNew.Lynx_Ticket_Number__c;
            
            if(isNewCase && isVITILcareOrigin && lynxTicketNumbersNotNull && lynxTicketNumberAcquired){
	            System.debug('LYNX TICKET NUMBER ACQUIRED: sendEmailToCollaborators');            

                VITILcare_Case_Collaborator_Settings__c collabSettings = VITILcare_Case_Collaborator_Settings__c.getOrgDefaults();
                System.debug('Collab collabSettings: ' + collabSettings);
                Id emailTemplateId = collabSettings.New_Case_Template_Id__c;
System.debug('emailTemplateId: ' + emailTemplateId);
                
                sendEmailNotificationToCollaborators(caseNew, emailTemplateId);
            }
            else{
	            System.debug('CRITERIA FAILED: NO sendEmailToCollaborators, Origin(' + isVITILcareOrigin + '), NOTNULL(' + lynxTicketNumbersNotNull + '), ACQUIRED(' + lynxTicketNumberAcquired + ')');            
            }
            
            if(isClosedCase && isVITILcareOrigin){
                vitilCareClosedCaseIds.add(caseId);
            } 
           /*  if(isClosedCase && caseNew.Origin == 'ENTP'){
                vitilCareClosedCaseIds.add(caseId);
            }  */  
        }  
        
        if(!vitilCareClosedCaseIds.isEmpty()){
        	processClosedCases(vitilCareClosedCaseIds);            
        }
    }
    
    public static void sendEmailNotificationToCollaborators(Case c, Id emailTemplateId){
        // send notification to collaborators
        System.debug('sendEmailNotificationToCollaborators, ' + c.NotifyCollaboratorString__c + ', ' + emailTemplateId);
        
        if (c.NotifyCollaboratorString__c != null && c.NotifyCollaboratorString__c.length() > 0) {
            List<String> collabEmails = c.NotifyCollaboratorString__c.split(';', 0);
System.debug('sendEmailNotificationToCollaborators, collabEmails: ' + collabEmails);
            if (collabEmails.size() > 0) {
                if (emailTemplateId != null) {
                    Id fromEmailId = null;
                    List<OrgWideEmailAddress> fromEmails = [select Id from OrgWideEmailAddress where DisplayName = 'VITILcare'];
System.debug('sendEmailNotificationToCollaborators, fromEmails: ' + collabEmails);
                    if (fromEmails.size() > 0) {
                        fromEmailId = fromEmails[0].Id;
                    }
                    try {
System.debug('sendEmailNotificationToCollaborators, SEND ');
                        List<Messaging.SendEmailResult> mailResults = VITILcareUtils.sendTemplatedCaseEmails(collabEmails, fromEmailId, emailTemplateId, c.Id);
        System.debug('sendEmailNotificationToCollaborators, mailResults: ' + mailResults);
                    } catch (Exception e) {
                        System.debug('Collab caught VITILcareUtils.sendTemplatedCaseEmails() exception: ' + e);
                    }
                }
            }
        }
    } 

    public static void processClosedCases(Set<Id> caseIds){
System.debug('processClosedCases: '  + caseIds);        
        // get all case comments that exists for id set	
        List<Case> cases = [SELECT id, origin, NotifyCollaboratorString__c FROM Case WHERE id IN :caseIds];
                
        List<EmailTemplate> emailTemplates = [SELECT Id from EmailTemplate where name = 'VITILcare case is closed collaborators'];
        EmailTemplate tmpl = emailTemplates.size()>0 ? emailTemplates[0] : null;
        
System.debug('processClosedCases: '  + cases);        
        for (Case c : cases) {
            if (c.Origin == 'VITILcare') {
				sendEmailNotificationToCollaborators(c, tmpl.id);
    		}
          /*  if (c.Origin == 'ENTP') {
				sendEmailNotificationToCollaborators(c, tmpl.id);
    		}*/
        }
    }    
    
}