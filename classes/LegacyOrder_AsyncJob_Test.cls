@isTest(SeeAllData=false) 
public class LegacyOrder_AsyncJob_Test {
    
    public enum OrderFields {LegacyOrderID,Sequence,CRIS3HoldCode,BillingSystem}
    
    @testSetup static void setupOrders()
    {
        list<map<OrderFields,string> > holdCodesList = new list<map<OrderFields,string> >();
        
        map<OrderFields,string> order1 = new map<OrderFields,string> {OrderFields.LegacyOrderID => '1000001', OrderFields.Sequence => '1', OrderFields.CRIS3HoldCode => 'TRG', OrderFields.BillingSystem => 'CRIS3-BC'};
        holdCodesList.add(order1);
        map<OrderFields,string> order2 = new map<OrderFields,string> {OrderFields.LegacyOrderID => '1000002', OrderFields.Sequence => '1', OrderFields.CRIS3HoldCode => 'TRG,ACC', OrderFields.BillingSystem => 'CRIS3-BC'};
        holdCodesList.add(order2);
        map<OrderFields,string> order3 = new map<OrderFields,string> {OrderFields.LegacyOrderID => '1000003', OrderFields.Sequence => '1', OrderFields.CRIS3HoldCode => 'CML', OrderFields.BillingSystem => 'CRIS3-BC'};
        holdCodesList.add(order3);
        map<OrderFields,string> order4 = new map<OrderFields,string> {OrderFields.LegacyOrderID => '1000004', OrderFields.Sequence => '1', OrderFields.CRIS3HoldCode => 'ACC', OrderFields.BillingSystem => ''};
        holdCodesList.add(order4);
        map<OrderFields,string> order5 = new map<OrderFields,string> {OrderFields.LegacyOrderID => '1000005', OrderFields.Sequence => '1', OrderFields.CRIS3HoldCode => 'SUB,TRG', OrderFields.BillingSystem => ' '};
        holdCodesList.add(order5);
        
        List<OrderStatusUpdates_ComplexTypes.LineItem> orderObjList = GenerateOrderStatusUpdates(holdCodesList);
        
        Test.startTest();
        LegacyOrder_AsyncJob orderTest = new LegacyOrder_AsyncJob(UserInfo.getSessionId(), orderObjList);
        orderTest.execute(null);
        Test.stopTest();
    }

    static testMethod void testOrderUpdates()
    {
        list<map<OrderFields,string> > holdCodesList = new list<map<OrderFields,string> >();
        
        map<OrderFields,string> order1 = new map<OrderFields,string> {OrderFields.LegacyOrderID => '1000001', OrderFields.Sequence => '1', OrderFields.CRIS3HoldCode => '', OrderFields.BillingSystem => 'CRIS3-BC'};
        holdCodesList.add(order1);
        map<OrderFields,string> order2 = new map<OrderFields,string> {OrderFields.LegacyOrderID => '1000002', OrderFields.Sequence => '1', OrderFields.CRIS3HoldCode => 'TRG,ACC', OrderFields.BillingSystem => 'CRIS3-BC'};
        holdCodesList.add(order2);
        map<OrderFields,string> order3 = new map<OrderFields,string> {OrderFields.LegacyOrderID => '1000002', OrderFields.Sequence => '1', OrderFields.CRIS3HoldCode => 'CML', OrderFields.BillingSystem => 'CRIS3-BC'};
        holdCodesList.add(order3);
        map<OrderFields,string> order4 = new map<OrderFields,string> {OrderFields.LegacyOrderID => '1000003', OrderFields.Sequence => '1', OrderFields.CRIS3HoldCode => 'ACC', OrderFields.BillingSystem => 'CRIS3-BC'};
        holdCodesList.add(order4);
        map<OrderFields,string> order5 = new map<OrderFields,string> {OrderFields.LegacyOrderID => '1000004', OrderFields.CRIS3HoldCode => 'SUB,TRG', OrderFields.BillingSystem => 'CRIS3-BC'};
        holdCodesList.add(order5);
        map<OrderFields,string> order6 = new map<OrderFields,string> {OrderFields.LegacyOrderID => '1000004', OrderFields.CRIS3HoldCode => 'TRG', OrderFields.BillingSystem => 'CRIS3-BC'};
        holdCodesList.add(order6);
        map<OrderFields,string> order7 = new map<OrderFields,string> {OrderFields.LegacyOrderID => '1000005', OrderFields.Sequence => '1', OrderFields.CRIS3HoldCode => 'ADR,TRG', OrderFields.BillingSystem => 'CRIS3-BC'};
        holdCodesList.add(order7);
        map<OrderFields,string> order8 = new map<OrderFields,string> {OrderFields.LegacyOrderID => '1000006', OrderFields.Sequence => '1', OrderFields.CRIS3HoldCode => 'CML,TRG', OrderFields.BillingSystem => 'CRIS3-BC'};
        holdCodesList.add(order8);
        map<OrderFields,string> order9 = new map<OrderFields,string> {OrderFields.LegacyOrderID => '1000006', OrderFields.Sequence => '2', OrderFields.CRIS3HoldCode => 'SUB,TRG', OrderFields.BillingSystem => 'CRIS3-BC'};
        holdCodesList.add(order9);
        
        List<OrderStatusUpdates_ComplexTypes.LineItem> orderObjList = GenerateOrderStatusUpdates(holdCodesList);
        
        Test.startTest();
        LegacyOrder_AsyncJob orderTest = new LegacyOrder_AsyncJob(UserInfo.getSessionId(), orderObjList);
        orderTest.execute(null);
        Test.stopTest();
    }

    static public List<OrderStatusUpdates_ComplexTypes.LineItem> GenerateOrderStatusUpdates(list<map<OrderFields,string> > holdCodesList)
    {
        List<OrderStatusUpdates_ComplexTypes.LineItem> orderObjList = new List<OrderStatusUpdates_ComplexTypes.LineItem>();

        if(holdCodesList.isEmpty())
            return orderObjList;
        
        for(map<OrderFields,string> orderMap : holdCodesList) {
            
            OrderStatusUpdates_ComplexTypes.LineItem orderObj = new OrderStatusUpdates_ComplexTypes.LineItem();
            
            for(OrderFields orderFld : orderMap.keySet()) {
                
                string orderVal = ordermap.get(orderFld);
                
                if(orderFld == OrderFields.LegacyOrderID)
                    orderObj.LegacyOrderID = orderVal;
                else if(orderFld == OrderFields.Sequence)
                    orderObj.Sequence = orderVal;
                else if(orderFld == OrderFields.CRIS3HoldCode)
                    orderObj.CRIS3HoldCode = orderVal;
                else if(orderFld == OrderFields.BillingSystem)
                    orderObj.BillingSystem = orderVal;
            }
            
            // populate the rest of the fields
            orderObj.CustomerName = 'Customer'+orderObj.LegacyOrderID;
            //orderObj.BillingSystem = 'CRIS3-BC';
            orderObj.IssueDate = string.valueOf(system.today());
            orderObj.CRIS3OrderStatus = 'Open';
            orderObj.RCID = '123456';
            orderObj.BillingAddress = '3777 Kingsway';
            orderObj.BTN = '6045551212';
            orderObj.ServiceAddress = '3777 Kingsway';
            orderObj.ServiceCity = 'Burnaby';
            orderObj.ServiceProvince = 'BC';
            orderObj.WorkInstallType = 'W';
            OrderStatusUpdates_ComplexTypes.DueDate dueDate = new OrderStatusUpdates_ComplexTypes.DueDate();
            dueDate.DDScheduleDate = date.today();
            orderObj.DDate = dueDate;
            
            orderObjList.add(orderObj);
        }
        return orderObjList;
    }
}