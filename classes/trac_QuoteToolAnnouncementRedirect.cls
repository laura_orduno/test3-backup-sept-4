public class trac_QuoteToolAnnouncementRedirect {
    Public pageReference doRedirect() {
        return new PageReference('/' + Schema.sObjectType.Quote_Tool_Announcement__c.getKeyPrefix());
    }
    static testMethod void testRedirect() {
    	trac_QuoteToolAnnouncementRedirect tQTAR = new trac_QuoteToolAnnouncementRedirect();
    	String prefix = Schema.sObjectType.Quote_Tool_Announcement__c.getKeyPrefix();
    	System.assertEquals(tQTAR.doRedirect().getUrl(), '/' + prefix);
    }
}