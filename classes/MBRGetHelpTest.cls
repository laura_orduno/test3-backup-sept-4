/*
* @author - Rauza Zhennisova, Christian Wico : Traction on Demand
* @description - Unit test for MBRGetHelpController.cls
*/

@isTest(SeeAllData=false)
private class MBRGetHelpTest {
	
	static CCIGetHelp__c help {get;set;}
	static CCIGetHelp__c help2 {get;set;}
	static CCIGetHelp__c VITILcareHelp {get;set;}
	static CCIGetHelp__c VITILcareHelp2 {get;set;}

	static {
		help = new CCIGetHelp__c(
								Portal__c = 'My Business Request',
								Category__c = 'General Questions',
								Question__c = 'how do we test this controller?',
								Published__c = true,
								Answer__c = 'super easy, like one, two, three');
		help2 = new CCIGetHelp__c(
								Portal__c = 'My Business Request',
								Category__c = 'Profile and Settings',
								Question__c = 'how do we test controller?',
								Published__c = true,
								Answer__c = 'super easy');
		/* VITILcare portal */
		VITILcareHelp = new CCIGetHelp__c(
								Portal__c = 'VITILcare',
								Category__c = 'General Questions',
								Question__c = 'how do we test this controller?',
								Published__c = true,
								Answer__c = 'super easy, like one, two, three');
		VITILcareHelp2 = new CCIGetHelp__c(
								Portal__c = 'VITILcare',
								Category__c = 'Profile and Settings',
								Question__c = 'how do we test controller?',
								Published__c = true,
								Answer__c = 'super easy');
        
		insert new List<CCIGetHelp__c> {help, help2, VITILcareHelp, VITILcareHelp2};
	}


	@isTest static void getHelpTest() {
		MBRGetHelpController helpCtrl = new MBRGetHelpController();

		List<String> catList =  helpCtrl.getCategories();
		System.assert(catList.size()>0);

		List<CCIGetHelp__c> questList =  helpCtrl.getQuestions();
		System.debug('222222222' + questList);
		System.assertEquals(questList.size(), 2);
	}

	@isTest static void testByPage() {
		
		// test constructor
		PageReference pref = Page.MBRGetHelp;
		pref.getParameters().put('q', String.valueOf(help.Id));
		Test.setCurrentPage(pref);

		MBRGetHelpController ctlr = new MBRGetHelpController();		
		System.assert(ctlr.questionId == String.valueOf(help.Id));
		System.assert(ctlr.getQuestions().size() == 1);

		
		// test selectedQuestion
		PageReference pref2 = Page.MBRGetHelp;
		pref2.getParameters().put('questionNumber', String.valueOf(help.Id));
		Test.setCurrentPage(pref2);

		MBRGetHelpController ctlr2 = new MBRGetHelpController();		
		System.assert(ctlr2.selectedQuestion.Id == help.Id);
	}
    
    
    /* VITILcare */
	@isTest static void VITILcare_getHelpTest() {
		VITILcareGetHelpController helpCtrl = new VITILcareGetHelpController();

		List<String> catList =  helpCtrl.getCategories();
		System.assert(catList.size()>0);

		List<CCIGetHelp__c> questList =  helpCtrl.getQuestions();
		System.debug('222222222' + questList);
		System.assertEquals(questList.size(), 2);
	}

	@isTest static void VITILcare_testByPage() {
		
		// test constructor
		PageReference pref = Page.VITILcareGetHelp;
		pref.getParameters().put('q', String.valueOf(help.Id));
		Test.setCurrentPage(pref);

		VITILcareGetHelpController ctlr = new VITILcareGetHelpController();		
		System.assert(ctlr.questionId == String.valueOf(help.Id));
		System.assert(ctlr.getQuestions().size() == 1);

		
		// test selectedQuestion
		PageReference pref2 = Page.VITILcareGetHelp;
		pref2.getParameters().put('questionNumber', String.valueOf(help.Id));
		Test.setCurrentPage(pref2);

		VITILcareGetHelpController ctlr2 = new VITILcareGetHelpController();		
		System.assert(ctlr2.selectedQuestion.Id == help.Id);
	}
}