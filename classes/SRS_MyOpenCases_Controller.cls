public with sharing class SRS_MyOpenCases_Controller {
  
    public list<case> lstCases{get{
        return sortList(lstCases);
    } set;}
    
    public String sortBy { get; set; }
    public String sortDir { get; set; }
    
    public  SRS_MyOpenCases_Controller(){
        
/*      
        //CF: commented out and changed 2/11        
        lstCases = [select id,CaseNumber,RecTypeName__c,Account.Name,
                    Priority,Status,Subject,type,createddate,
                    Escalation_History__c 
                    from case 
                    where ownerid =: Userinfo.getUserId() and status != 'Closed' order by createddate ASC];
*/
        //CF: Change query to use boolean rather than status for closed cases
        lstCases = [select id,CaseNumber,RecTypeName__c, Account.Name,
                    Priority,Status,Subject,type,createddate,
                    Escalation_History__c 
                    from case 
                    where ownerid =: Userinfo.getUserId() and isClosed != true order by createddate ASC];
        
         system.debug('@@@@lstCases : '+lstCases);
     }
    
    
    public PageReference empty() { return null; }
    
    public List<SObject> sortList(List<SObject> cleanList){
        
        system.debug('@@@@@cleanList : '+cleanList);
        system.debug('@@@@@sortBy : '+sortBy);
        
        if (sortBy == null) { return cleanList; }
        
        List<SObject> resultList = new List<SObject>();
        Map<Object, List<SObject>> objectMap = new Map<Object, List<SObject>>();
        
        for (SObject item : cleanList) {
          system.debug('@@@@@item:'+item);
          
          if (objectMap.get(item.get(sortBy)) == null) {
            objectMap.put(item.get(sortBy), new List<SObject>());
          }
          objectMap.get(item.get(sortBy)).add(item);
        }
        
        List<Object> keys = new List<Object>(objectMap.keySet());
        keys.sort();
        
        for(Object key : keys) {
          resultList.addAll(objectMap.get(key));
        }
        
        cleanList.clear();
        
        if (sortDir == 'ASC') {
          for (SObject item : resultList) {
            cleanList.add(item);
          }
        } else {
          for (Integer i = resultList.size()-1; i >= 0; i--) {
            cleanList.add(resultList[i]);
          }
        }
        
        return cleanList;
  }
  
}