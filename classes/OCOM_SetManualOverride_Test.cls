@isTest(seeAllData=false)
private class OCOM_SetManualOverride_Test{
    
     @isTest
    private static void preproc_test1() {
    
      Product2 testProduct = new Product2(Name = 'Test Product 1', orderMgmtId__c = '9143863905013553517');
      testProduct.vlocity_cmt__JSONAttribute__c = '{"GEN_DESC": [{"$$AttributeDefinitionStart$$": null, "objectid__c": "01t22000000Po9oAAC", "attributeid__c": "a7R220000000HAqEAM", "attributecategoryid__c": "a7Q2200000000UBEAY", "categorycode__c": "TELUSCHAR", "categoryname__c": "Characteristics", "attributeuniquecode__c": "ATTRIBUTE-406", "attributeconfigurable__c": true, "attributedisplaysequence__c": "13", "attributefilterable__c": false, "isactive__c": true, "attributedisplayname__c": "One-time Charges, NRC", "displaysequence__c": null, "formatmask__c": null, "hasrule__c": false, "isconfigurable__c": true, "ishidden__c": false, "valueinnumber__c": null, "objecttype__c": "Product2", "querycode__c": null, "isreadonly__c": false, "isquerydriven__c": false, "querylabel__c": null, "id": "a7P220000008l1fEAA", "isrequired__c": false, "rulemessage__c": null, "uidisplaytype__c": "Text", "value__c": null, "valuedatatype__c": "Text", "valuedescription__c": null, "attributecloneable__c": true, "customconfiguitemplate__c": null }, {"$$AttributeDefinitionStart$$": null, "objectid__c": "01t22000000Po9oAAC", "attributeid__c": "a7R220000000HAqEAM", "attributecategoryid__c": "a7Q2200000000UBEAY", "categorycode__c": "TELUSCHAR", "categoryname__c": "Characteristics", "attributeuniquecode__c": "ATTRIBUTE-406", "attributeconfigurable__c": true, "attributedisplaysequence__c": "13", "attributefilterable__c": false, "isactive__c": true, "attributedisplayname__c": "Recurring Charges, MRC", "displaysequence__c": null, "formatmask__c": null, "hasrule__c": false, "isconfigurable__c": true, "ishidden__c": false, "valueinnumber__c": null, "objecttype__c": "Product2", "querycode__c": null, "isreadonly__c": false, "isquerydriven__c": false, "querylabel__c": null, "id": "a7P220000008l1fEAA", "isrequired__c": false, "rulemessage__c": null, "uidisplaytype__c": "Text", "value__c": null, "valuedatatype__c": "Text", "valuedescription__c": null, "attributecloneable__c": true, "customconfiguitemplate__c": null }] }';
      insert testProduct;
      // Create a Pricebooks
      Pricebook2 standardBook =  new Pricebook2(Id=Test.getStandardPricebookId(), Name = 'StandardPricebook', IsActive = true);
      
      Pricebook2 testPricebook = new Pricebook2(Name = 'TestPricebook', IsActive = true);
      insert testPricebook;
      
      // Create Pricebook entries
      PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = standardBook.Id,
          Product2Id = testProduct.Id, UnitPrice = 10, IsActive = true, UseStandardPrice = false);
      insert standardPrice;
      
      PricebookEntry testPricebookEntryPlain = new PricebookEntry(Pricebook2Id = testPricebook.Id,
          Product2Id = testProduct.Id, UnitPrice = 11, IsActive = true, UseStandardPrice = false);
      insert testPricebookEntryPlain;
     
      // Create Account
      Account testAccount = new Account();
      testAccount.Name = 'TestAccount';
      //testAccount.SLA__c = 'Platinum';
      insert testAccount;

      // Create Order
      Order objorder = new Order(Name='Test Order',EffectiveDate=System.today(),status='Draft',AccountId = testAccount.Id, Pricebook2Id=testPricebook.Id);
      insert objorder;

      // Create Order Items
       OrderItem orderItem= new OrderItem(OrderId=objOrder.Id, PricebookEntryId = testPricebookEntryPlain.Id,
       vlocity_cmt__LineNumber__c = '0001', Quantity = 3.0, UnitPrice = 270000.0, vlocity_cmt__ProvisioningStatus__c = 'New');
      orderItem.vlocity_cmt__JSONAttribute__c = '{"GEN_DESC": [{"$$AttributeDefinitionStart$$": null, "objectid__c": "01t22000000Po9oAAC", "attributeid__c": "a7R220000000HAqEAM", "attributecategoryid__c": "a7Q2200000000UBEAY", "categorycode__c": "TELUSCHAR", "categoryname__c": "Characteristics", "attributeuniquecode__c": "ATTRIBUTE-406", "attributeconfigurable__c": true, "attributedisplaysequence__c": "13", "attributefilterable__c": false, "isactive__c": true, "attributedisplayname__c": "One-time Charges, NRC", "displaysequence__c": null, "formatmask__c": null, "hasrule__c": false, "isconfigurable__c": true, "ishidden__c": false, "valueinnumber__c": null, "objecttype__c": "Product2", "querycode__c": null, "isreadonly__c": false, "isquerydriven__c": false, "querylabel__c": null, "id": "a7P220000008l1fEAA", "isrequired__c": false, "rulemessage__c": null, "uidisplaytype__c": "Text", "value__c": null, "valuedatatype__c": "Text", "valuedescription__c": null, "attributecloneable__c": true, "customconfiguitemplate__c": null, "attributeRunTimeInfo": {"dataType": "Text", "uiDisplayType": "Text", "value": "320"}, "$$AttributeDefinitionEnd$$": null }, {"$$AttributeDefinitionStart$$": null, "objectid__c": "01t22000000Po9oAAC", "attributeid__c": "a7R220000000HAqEAM", "attributecategoryid__c": "a7Q2200000000UBEAY", "categorycode__c": "TELUSCHAR", "categoryname__c": "Characteristics", "attributeuniquecode__c": "ATTRIBUTE-406", "attributeconfigurable__c": true, "attributedisplaysequence__c": "13", "attributefilterable__c": false, "isactive__c": true, "attributedisplayname__c": "Recurring Charges, MRC", "displaysequence__c": null, "formatmask__c": null, "hasrule__c": false, "isconfigurable__c": true, "ishidden__c": false, "valueinnumber__c": null, "objecttype__c": "Product2", "querycode__c": null, "isreadonly__c": false, "isquerydriven__c": false, "querylabel__c": null, "id": "a7P220000008l1fEAA", "isrequired__c": false, "rulemessage__c": null, "uidisplaytype__c": "Text", "value__c": null, "valuedatatype__c": "Text", "valuedescription__c": null, "attributecloneable__c": true, "customconfiguitemplate__c": null, "attributeRunTimeInfo": {"dataType": "Text", "uiDisplayType": "Text", "value": "320"}, "$$AttributeDefinitionEnd$$": null }  ] }';
    insert orderItem;
    
      Map<String,Object> inputMap = new Map<String, Object>();
       Map<Id, Sobject> sobjectIdToSobject = new Map<Id,Sobject>();
       sobjectIdToSobject.put(orderItem.Id,orderItem);
       Map<String, Object> filterEvaluatorOutput = new Map<String,Object>();
        filterEvaluatorOutput.put('sobjectIdToSobject',sobjectIdToSobject);
         Set<Id> qualifiedObjectIds = new Set<Id>();
         qualifiedObjectIds.add(orderItem.id);
         inputMap.put('qualifiedObjectIds',qualifiedObjectIds);
         inputMap.put('filterEvaluatorOutput',filterEvaluatorOutput);
       String methodName = 'priceItems';
       String methodName1 = 'testpriceItems';
       Map<String,Object> outMap = new Map<String, Object>();
       Map<String,Object> options = new Map<String, Object>();
       OCOM_SetManualOverride setManualOverride = new OCOM_SetManualOverride();
       
       test.startTest();
          setManualOverride.invokeMethod(methodName, inputMap, outMap, options);
          setManualOverride.invokeMethod(methodName1, inputMap, outMap, options);
        test.stopTest();   
  }  
}