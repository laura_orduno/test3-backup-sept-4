@isTest
public class OCOM_TNRControllerTest 
{

    @testSetup
    static void testDataSetup() 
    {
    insert new OCOM_TNR_Settings__c(Line_Designation__c = 'Line Designation', 
                                    Line_Type__c = 'Line Type',
                                    Offering_ID__c = 'Offering ID',
                                    Overline__c = 'Overline',
                                    Phone_Number__c = 'Phone Number',
                                    Pilot__c = 'Pilot', 
                                    Single_Line__c = 'Single Line',
                                    Smart_Ring__c = 'SmartRing',
                                    Telephone_Number__c = 'Telephone Number',
                                    Telephone_Number_Setup__c = 'Telephone Number Setup',
                                    Toll_Free_Number__c='Toll-Free Number',
                                   	Wireless_Device_Type__c = 'Wireless Device Type',
                                   	Responsible_Provisioning_Group__c = 'Responsible Provisioning Group',
                                   	WLN_Provisioning_Group__c = '9145615376313186289',
                                   	WLS_Provisioning_Group__c = '9145615376313186291',
                                   	LDIT_Provisioning_Group__c = '9145615376313186293');
        
    }
    

    

    
    @isTest
    private static void autoAssignTNTest()
    {
        test.startTest();
        String orderId= OrdrTestDataFactory.orderIdWithVoiceProducts();
        String attributeid = 'a7R220000004CZQEA2';
        List<OrderItem> items = [select id from OrderItem where OrderId =:orderId and Product_display_name__c = 'Toll-Free'];
        String itemId = items.get(0).id;
        
        OCOM_TNRController tnr = new OCOM_TNRController(Id.valueOf(orderId), Id.valueOf(itemId), Id.valueOf(attributeid), 'undefined');
        tnr.init();
        tnr.autoSelectTN();
        test.stopTest();
    }
    
    @isTest
    private static void autoAssignTNTest2()
    {
        test.startTest();
        String orderId= OrdrTestDataFactory.orderIdWithVoiceProducts();
        String attributeid = 'a7R220000004CZQEA2';
        List<OrderItem> items = [select id from OrderItem where OrderId =:orderId and Product_display_name__c = 'Change Phone'];
        String itemId = items.get(0).id;
        String provId = '9145615376313186289';
        
        OCOM_TNRController tnr = new OCOM_TNRController(Id.valueOf(orderId), Id.valueOf(itemId), Id.valueOf(attributeid), provId);
        tnr.init();
        tnr.autoSelectTN();
        test.stopTest();
    }
    
    @isTest
    private static void autoAssignTNTest3()
    {
        test.startTest();
        String orderId= OrdrTestDataFactory.orderIdWithVoiceProducts();
        String attributeid = 'a7R220000004CZQEA2';
        List<OrderItem> items = [select id from OrderItem where OrderId =:orderId and Product_display_name__c = 'Change Phone'];
        String itemId = items.get(0).id;
        String provId = '9145615376313186291';
        
        OCOM_TNRController tnr = new OCOM_TNRController(Id.valueOf(orderId), Id.valueOf(itemId), Id.valueOf(attributeid), provId);
        tnr.init();
        tnr.autoSelectTN();
        test.stopTest();
    }
    
    @isTest
    private static void autoAssignTNTest4()
    {
        test.startTest();
        String orderId= OrdrTestDataFactory.orderIdWithVoiceProducts();
        String attributeid = 'a7R220000004CZQEA2';
        List<OrderItem> items = [select id from OrderItem where OrderId =:orderId and Product_display_name__c = 'Change Phone'];
        String itemId = items.get(0).id;
        String provId = '9145615376313186293';
        
        OCOM_TNRController tnr = new OCOM_TNRController(Id.valueOf(orderId), Id.valueOf(itemId), Id.valueOf(attributeid), provId);
        tnr.init();
        tnr.autoSelectTN();
        test.stopTest();
    }
    
    @isTest
    private static void initTest()
    {
         test.startTest();
        String orderId= OrdrTestDataFactory.orderIdWithVoiceProducts();
        String attributeid = 'a7R220000004CZQEA2';
        List<OrderItem> items = [select id from OrderItem where OrderId =:orderId and Product_display_name__c = 'Business Select Office Phone - Single Line'];
        String itemId = items.get(0).id;
        
        Test.setCurrentPageReference(new PageReference('Page.myPage'));
        System.currentPageReference().getParameters().put('orderId', Id.valueOf(orderId));
        System.currentPageReference().getParameters().put('lineItemId', Id.valueOf(itemId));
        System.currentPageReference().getParameters().put('attributeId', Id.valueOf(attributeid));
        System.currentPageReference().getParameters().put('provisionigGroup', 'undefined');

        OCOM_TNRController tnr = new OCOM_TNRController();
        tnr.init();
        tnr.getShowAssignedTNs();
        tnr.getAssignedTNs();
        tnr.getShowUnAssignedTNs();
        tnr.getUnAssignedTNsModel();
        tnr.getShowUnAssignedTNsError();
        tnr.getShowSearchBar();
        //tnr.getCoid();
        //tnr.getSelectedNpaNxxVal();
        tnr.getNpaNxxOptions();
        tnr.getNpaNxxOptions();
        tnr.getNpaOptions();
        //tnr.getSelectedNxxVal();
        //tnr.getSelectedLast4Val();
        tnr.getShowSearchRes();
        //tnr.getSearchKey();
        tnr.getTelPhoneNosModelList();
        tnr.getShowNoSpareTN();
        tnr.getShowTNSearchNoNumber();
        tnr.getShowTNSearchError();
         test.stopTest();
    }
    
    @isTest
    private static void tnSearchTest()
    {
         test.startTest();
        String orderId= OrdrTestDataFactory.orderIdWithVoiceProducts();
        String attributeid = 'a7R220000004CZQEA2';
        List<OrderItem> items = [select id from OrderItem where OrderId =:orderId and Product_display_name__c = 'Business Select Office Phone - Single Line'];
        String itemId = items.get(0).id;
        
        Test.setCurrentPageReference(new PageReference('Page.myPage'));
        System.currentPageReference().getParameters().put('orderId', Id.valueOf(orderId));
        System.currentPageReference().getParameters().put('lineItemId', Id.valueOf(itemId));
        System.currentPageReference().getParameters().put('attributeId', Id.valueOf(attributeid));
        System.currentPageReference().getParameters().put('provisionigGroup', 'undefined');

        OCOM_TNRController tnr = new OCOM_TNRController();
        tnr.init();
        tnr.selectedNxxVal = '';
        tnr.doTNSearch();
        
        tnr.selectedNpaVal = '780';
        tnr.selectedNxxVal = '371';
        tnr.selectedNpaNxxVal = '780-371';
        tnr.selectedLast4Val = '1975';
        tnr.doTNSearch();
        tnr.setOrderControllerId(Id.valueOf(orderId));
        tnr.getOrderControllerId();
        tnr.doTNSearch();
        tnr.doNpa();
        tnr.doNpaNxx();
        tnr.doNextPage();
        
        tnr.selectedLast4Val = '1';
        tnr.doTNSearch();
        tnr.selectedLast4Val = '19';
        tnr.doTNSearch();
        tnr.selectedLast4Val = '197';
        tnr.doTNSearch();
         test.stopTest();
    }
    
    @isTest
    private static void tnSelectReserveAssignTest()
    {     test.startTest();
        String orderId= OrdrTestDataFactory.orderIdWithVoiceProducts();
        String attributeid = 'a7R220000004CZQEA2';
        List<OrderItem> items = [select id from OrderItem where OrderId =:orderId and Product_display_name__c = 'Business Select Office Phone - Single Line'];
        String itemId = items.get(0).id;
        
        Test.setCurrentPageReference(new PageReference('Page.myPage'));
        System.currentPageReference().getParameters().put('orderId', Id.valueOf(orderId));
        System.currentPageReference().getParameters().put('lineItemId', Id.valueOf(itemId));
        System.currentPageReference().getParameters().put('attributeId', Id.valueOf(attributeid));
     	System.currentPageReference().getParameters().put('provisionigGroup', 'undefined');
     

        OCOM_TNRController tnr = new OCOM_TNRController();
        
        tnr.selectedNpaVal = '780';
        tnr.selectedNxxVal = '371';
        tnr.selectedNpaNxxVal = '780-371';
        tnr.init();
        
        System.currentPageReference().getParameters().put('selectedLeftPh', '780 371 1975');
        System.currentPageReference().getParameters().put('selectedRightPh', '780 371 1976');
        tnr.selectLeftTn();
        tnr.selectRightTn();
        tnr.doReserveOrAssign();
        tnr.init();
        System.currentPageReference().getParameters().put('selectedLeftPh', '780 371 1973');
        System.currentPageReference().getParameters().put('selectedRightPh', '780 371 1974');
        tnr.selectLeftTn();
        tnr.selectRightTn();
        tnr.doReserveOrAssign();
        
        System.currentPageReference().getParameters().put('SelectedUnAssignedTN', '780 371 1976');
        tnr.doAssign();
        tnr.doNoSpareTN();
        tnr.releaseTN();
         test.stopTest();
    }
     @isTest
    private static void handelErrorTest()
    {
         test.startTest();
        OCOM_TNRController tnr = new OCOM_TNRController();
        
        OrdrGenericException genEx1 = new OrdrGenericException();
        genEx1.errorCode=OrdrErrorCodes.TNR_UNEXPECTED_ERROR;
        genEx1.setMessage('test OrdrGenericException');
        tnr.handelError(genEx1);
        
        OrdrGenericException genEx2 = new OrdrGenericException();
        genEx2.errorCode=OrdrErrorCodes.TNT_GENERAL_CONNECTIVITY_ERROR;
        genEx2.setMessage('test OrdrGenericException');
        tnr.handelError(genEx2);
        
        OrdrServicesException svcEx1 = new OrdrServicesException();
        svcEx1.setMessage('NO SPARES FOR NXX');
        //svcEx1.errorCode=OrdrErrorCodes.TNR_NO_TN_AVAILABLE_FOR_SELECTED_NPANXX;
        tnr.handelError(svcEx1);
        
        OrdrServicesException svcEx2 = new OrdrServicesException();
        svcEx2.setMessage('TELEPHONE LINE NUMBER REQUESTED IS NOT SPARE');
        //svcEx2.errorCode=OrdrErrorCodes.TNR_RESERVATION_FAILED;
        tnr.handelError(svcEx2);
        
        OrdrServicesException svcEx3 = new OrdrServicesException();
        svcEx3.setMessage('HELD-FOR-DISPLAY');
        //svcEx3.errorCode=OrdrErrorCodes.TNR_INVALID_NXX_FOR_SELECTED_NPANXX;
        tnr.handelError(svcEx3);
        
        OrdrServicesException svcEx4 = new OrdrServicesException();
        svcEx4.setMessage('NXX DOES NOT EXIST FOR SPECIFIED COID');
        //svcEx4.errorCode=OrdrErrorCodes.TNR_UNEXPECTED_ERROR;
        tnr.handelError(svcEx4);
        
        OrdrServicesException svcEx5 = new OrdrServicesException();
        svcEx5.setMessage('Real TN was not reserved');
        //svcEx5.errorCode=OrdrErrorCodes.DEFAULT_UNEXPECTED_ERROR;
        tnr.handelError(svcEx5);
        
        OrdrServicesException svcEx6 = new OrdrServicesException();
        svcEx6.setMessage('test OrdrServicesException');
        //svcEx6.errorCode=OrdrErrorCodes.TNT_GENERAL_CONNECTIVITY_ERROR;
        tnr.handelError(svcEx6);
        
        MyException ex = new MyException();
        ex.setMessage('Test Exception');
        tnr.handelError(ex);
        test.stopTest();
    }
    
    public class MyException extends Exception {}
}