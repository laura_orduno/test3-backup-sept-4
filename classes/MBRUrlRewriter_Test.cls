@isTest
private class MBRUrlRewriter_Test {
	
	@isTest static void test() {
		MBRUrlRewriter cur = new MBRUrlRewriter();

		System.assert(cur.urlMap.size() > 0);

		// test mapRequestUrl method
		System.assert(cur.mapRequestUrl(new PageReference('/overview')).getUrl().contains('mbroverview'));
		System.assert(cur.mapRequestUrl(new PageReference('http:///www.google.com')) == null);

		// test generateUrlFor method
		List<PageReference> prefs = new List<PageReference> {Page.MBROverview};
		System.debug(prefs[0]);
		System.assert(cur.generateUrlFor(prefs)[0].getUrl().contains('/overview'));
	}
	
	
}