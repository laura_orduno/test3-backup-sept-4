/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class OCOM_NotificationController_Test 
{

    static testMethod void addNotification() 
    {
        OCOM_NotificationController notf = new OCOM_NotificationController();
        String res1 = notf.notificationJSON;
        System.AssertEquals(true, String.isBlank(res1));
        notf.addNotification('notif1', 'Info', 'Test Msg');
        notf.generateJSON();
        String res2 = notf.notificationJSON;
        System.AssertEquals(false, String.isBlank(res2));
        
    }
    
    static testMethod void getMessages_Quote()
    {
    	Opportunity o = new Opportunity();
        o.Name = 'test Opportunity for expiration';
        o.StageName = 'NewStage';
        o.CloseDate = Date.today().addDays(1);    
        insert o;
        
    	Quote newQuote = new Quote();
    	newQuote.OpportunityId = o.Id;
        newQuote.name = 'test quote for expiration';
    	newQuote.Maximum_Speed__c = '24 Mbps';
    	newQuote.vlocity_cmt__ValidationMessage__c = 'Test msg';
    	insert newQuote;
    	
    	PageReference pageRef = new PageReference('/apex/quote?id='+newQuote.id);
        Test.setCurrentPage(pageRef);
        
        ApexPages.currentPage().getParameters().put('id', newQuote.id);
        
        OCOM_NotificationController notf = new OCOM_NotificationController();
        notf.getMessages();
        
    }
    
    static testMethod void getMessages_Order()
    {
    	Account acc = new Account();
        acc.name = 'test Acc';
    	insert acc;
    	
    	Order newOrder = new Order();
        newOrder.EffectiveDate = Date.today();
        newOrder.Status = 'Not Submitted';
    	newOrder.name = 'test order';
    	newOrder.AccountId = acc.id;
    	newOrder.Maximum_Speed__c = '24 Mbps';
    	newOrder.vlocity_cmt__ValidationMessage__c = 'Test msg';
    	insert newOrder;
    	
    	PageReference pageRef = new PageReference('/apex/Order?id='+newOrder.id);
        Test.setCurrentPage(pageRef);
        
        ApexPages.currentPage().getParameters().put('id', newOrder.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(newOrder);
        OCOM_NotificationController notf = new OCOM_NotificationController(sc);
        notf.getMessages();
        
    }
}