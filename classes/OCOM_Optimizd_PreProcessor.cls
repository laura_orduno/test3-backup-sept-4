global with sharing class OCOM_Optimizd_PreProcessor implements vlocity_cmt.VlocityOpenInterface  {
  private Boolean useDisplayText = false;
  String sRateBand = '';
  String sRateSubBand = '';
  String sForborne = '';
  String sMarket = '';
  String sMarketDesc = '';

  map<string, list<CalcMatrixdataWrapper>> cmrExtIDtoWrapperMap =  new map<string, list<CalcMatrixdataWrapper>>();


  global Boolean invokeMethod(String methodName, Map<String, Object> inputMap, Map<String, Object> outMap, Map<String, Object> options) {
    Boolean success = true;

    List<Object> detailNodeList = new List<Object>();


    //OCOM_vlocityCustomUtilcls.PriceMatrixTargetExternalId = 'OCOMTextPriceMatrix__TargetExternalId';
    OCOM_vlocityCustomUtilcls.PriceMatrixTargetExternalId = System.Label.OCOMTextPriceMatrix_TargetExternalID;
    system.debug(LoggingLevel.ERROR, ' ---- OCOMPreProcessor PriceMatrixTargetExternalId ' + System.Label.OCOMTextPriceMatrix_TargetExternalID);
    try {
      if (methodName == 'calculate') {
        List<SObject> itemList =  (list<SObject>)vlocity_cmt.FlowStaticMap.flowMap.get('itemList');
        Set<Id> orderItemIdSet = new Set<Id>();
        List<Object> outputList = new List<Object>();
        List<Sobject> itemSobjList = new List<Sobject>();

        if (inputMap.containsKey('leanPricingCall') ) {
          List<Object> itemIDList  = (List<Object>)inputMap.get('leanPricingCall');

          if ( itemIDList != null && itemIDList.size() > 0 && !itemIDList.isEmpty() ) {
            for (object itemIdobj : itemIDList ) {
              // system.debug('itemIdobj____'+itemIdobj);
              system.debug('itemIdobj____' + (itemIdobj instanceof String ));
              id ItemID = id.valueof((String)itemIdobj);
              orderItemIdSet.add(ItemID);
            }

          }
          // vlocity_cmt.FlowStaticMap.flowMap.put('leaningPricingItemList',orderItemIdSet);


        } else if (itemList != null && itemList.size() > 0 && !itemList.isEmpty()) {
          for (SObject s : itemList) {

            OrderItem orderlineItems = (OrderItem) s;
            orderItemIdSet.add(orderlineItems.Id);
          }
        }
        List<Order> currentOrderList = new List<Order>();
        if (orderItemIdSet != null && orderItemIdSet.size() > 0 && !orderItemIdSet.isEmpty()) {
          for (OrderItem ordItem : [Select ID,
                                    PricebookEntry.UnitPrice,
                                    PricebookEntry.vlocity_cmt__RecurringPrice__c,
                                    PricebookEntry.product2.ProductCode,
                                    PricebookEntry.product2.Name,
                                    PricebookEntry.product2.Id,
                                    PricebookEntry.Product2.orderMgmtId__c ,
                                    vlocity_cmt__Product2Id__c,
                                    vlocity_cmt__ParentItemId__c,
                                    Quantity, OfferName__c, OrderId,
                                    vlocity_cmt__JSONAttribute__c,
                                    vlocity_cmt__LineNumber__c,
                                    vlocity_cmt__OneTimeTotal__c,
                                    vlocity_cmt__RecurringTotal__c,
                                    vlocity_cmt__EffectiveOneTimeTotal__c,
                                    vlocity_cmt__EffectiveRecurringTotal__c,
                                    vlocity_cmt__RootItemId__c,
                                    Order.RateBand__c,
                                    Order.Rate_Sub_Band__c,
                                    Order.Market__c, Order.Forbone__c,
                                    Order.Forbearance__c,
                                    vlocity_cmt__Product2Id__r.VLAdditionalConfigData__c,
                                    vlocity_cmt__OneTimeManualDiscount__c,
                                    vlocity_cmt__RecurringManualDiscount__c
                                    from OrderItem
                                    where Id = : OrderItemIdSet]) {
            itemSobjList.add(ordItem);
            system.debug(LoggingLevel.ERROR, ' ---- OCOMPreProcessor itemLists = ' + ordItem.Id);
            //Map<String,String>idMap = new Map<String,String>{'ID' => ordItem.Id};
            Map<String, Object> detailNodeDetails = new Map<String, String>();
            detailNodeDetails.put('Reference Product Name', ordItem.OfferName__c );
            detailNodeDetails.put('Order Id', ordItem.OrderId);
            detailNodeDetails.put('Reference External Id', ordItem.PricebookEntry.Product2.orderMgmtId__c);
            detailNodeDetails.put('Quantity', String.valueOf(ordItem.Quantity));
            detailNodeDetails.put('Item Attr', ordItem.vlocity_cmt__JSONAttribute__c);
            detailNodeDetails.put('Line Number', ordItem.vlocity_cmt__LineNumber__c);
            detailNodeDetails.put('Input NRC', String.valueOf(ordItem.vlocity_cmt__OneTimeTotal__c));
            detailNodeDetails.put('Input MRC', String.valueOf(ordItem.vlocity_cmt__RecurringTotal__c));

            Map<String, Object> detailNodeMap = new Map<String, Object>();
            detailNodeMap.put('ID', ordItem.Id);
            detailNodeMap.put('Detail', detailNodeDetails);
            detailNodeList.add(detailNodeMap);
            if (currentOrderList.size() == 0 || currentOrderList.isEmpty() ) {
              sRateSubBand = ordItem.Order.Rate_Sub_Band__c;
              sRateBand =   ordItem.Order.RateBand__c;
              sForborne = '';
              if (ordItem.Order.Forbearance__c != null)
                sForborne = ordItem.Order.Forbearance__c;
              /*if(currentOrder[0].Forbone__c  == true)
                  {
                  sForborne = 'No';}
                  else{
                  sForborne = 'Yes';
               }*/
              sMarket = ordItem.Order.Market__c  != null ? ordItem.Order.Market__c  : '';
              if (String.isNotBlank(ordItem.Order.Market__c)) {
                Country_Province_Codes__c FullProvValue = Country_Province_Codes__c.getValues(ordItem.Order.Market__c);
                sMarketDesc = ordItem.Order.Market__c;
                if (null != FullProvValue) {
                  sMarketDesc  = FullProvValue.Description__c;
                }
              }
              Order currentOrder = new Order(id = ordItem.orderId );
              currentOrderList.add(currentOrder);
            }
          }
          vlocity_cmt.FlowStaticMap.flowMap.put('leaningPricingItemList', itemSobjList);
        }

        //System.debug('detailNodeMap_____::' + JSON.serializePretty(detailNodeList));

        string CalcMatrixName = system.label.OCOM_Text_Price_Matrix;
        //getCalcMatrixdata('OCOM Text Price Matrix');
        // Query the Calculation Matrix Data for Constracting Row Map and Comparision
        getCalcMatrixdata(CalcMatrixName);

        Map<String, Object> data = new Map<String, Object>();

        if (detailNodeList != null && detailNodeList.size() > 0 && !detailNodeList.isEmpty()) {
          Object drResult = detailNodeList;
          if (drResult instanceof List<Object>) {
            List<Object> resultList = (List<Object>)drResult;
            for (Object obj : resultList) {
              Map<String, Object> result = (Map<String, Object>)obj;
              processData(result, data);
            }
            inputMap.put('data', data);
          } else if (drResult instanceof Map<String, Object>) {
            //////system.debug('InstanceOf Map');
            Map<String, Object> result = (Map<String, Object>)drResult;
            processData(result, data);
            inputMap.put('data', data);
          }
          // Iterate to find the Node to Update.
          Map<String, Object> dataNodeToUpdate = new Map<String, Object>();

          integer i = 0;
          for (object dataObj : data.values()) {
            if (dataObj instanceof Map<String, Object>) {
              Map<String, Object> rowMap = (Map<String, Object>) dataObj;
              if (rowMap != null && (rowMap.ContainsKey('Matrix MRC') || rowMap.ContainsKey('Matrix NRC')) && (rowMap.get('Matrix MRC') != null || rowMap.get('Matrix NRC') != null)) {
                if (rowMap.containsKey('Matrix MRC') )
                  rowMap.put('MRC', rowMap.get('Matrix MRC'));
                if (rowMap.containsKey('Matrix NRC') )
                  rowMap.put('NRC', rowMap.get('Matrix NRC'));
                if (rowMap.containsKey('Reference External Id') )
                  rowMap.put(System.Label.OCOMTextPriceMatrix_TargetExternalID, rowMap.get('Reference External Id'));
                if (rowMap.containsKey('Reference Product Name') )
                  rowMap.put(System.Label.OCOMTextMultiplePriceMatrix_TargetProductName, rowMap.get('Reference Product Name'));

                rowMap.put('ID', 'Row' + i);

                dataNodeToUpdate.put('Row' + i, dataObj);
                outputList.add(dataObj);
              }

            }
            i++;
          }
          system.debug(LoggingLevel.ERROR, ' ---- OCOMPreProcessor data input to procedure222\n ' +  outputList);
        }

        // if(outputList != null && !outputList.isEmpty() && outputList.size()>0 ){
        outMap.put('output', outputList);
        Map<Id, OrderItem> oliToUpdMap = new Map<Id, OrderItem>();

        // Call POST PROCESSOR to update the pricing
        OCOM_Optimizd_PostProcessor postproc = new OCOM_Optimizd_PostProcessor();
        postproc.invokeMethod(methodName, inputMap, outMap, options);

        // Return he computed list to UI for display 
        outMap.put('outputPriceList', outMap.get('OliToUpdtePriceMap'));
        system.debug('outMap____' + outMap);

      }


    } catch (Exception e) {
      system.debug(LoggingLevel.ERROR, 'Exception is ' + e);
      system.debug(LoggingLevel.ERROR, 'Exception stack trace ' + e.getStackTraceString());

      //##################################################################
      //VlocityLogger.x(CLASS_NAME+'-'+ methodName + ':Exception e='+ e+' : ');
      //##################################################################

      success = false;
      throw e;
    }




    return success;
  }


  //in ProcessData will process each line item (XLI) and will create a processDetail (line 103)
  @testVisible
  private void processData(Map<String, Object> result,  Map<String, Object> data) {

    //##################################################################
    //VlocityLogger.e(CLASS_NAME+'-processData');
    //##################################################################
    //////system.debug('result'+result);
    //////system.debug('data'+data);

    String rowId = (String)result.get('ID');

    String rowKey = 'Row' + data.size();
    Map<String, Object> row = new Map<String, Object>();
    data.put(rowKey, row);
    row.put('ItemID', rowId);

    Object detailNode = result.get('Detail');
    if (detailNode instanceof List<Object>) {
      List<Object> detailList = (List<Object>)detailNode;
      for (Object obj : detailList) {
        Map<String, Object> detail = (Map<String, Object>)obj;
        processDetail(detail, row, data);
      }
    } else if (detailNode instanceof Map<String, Object>) {
      Map<String, Object> detail = (Map<String, Object>)result.get('Detail');
      processDetail(detail, row, data);
    }

    //##################################################################
    //VlocityLogger.x(CLASS_NAME+'-processData');
    //##################################################################


  }
  @testVisible
  private void processDetail(Map<String, Object> detail, Map<String, Object> row, Map<String, Object> data) {

    //##################################################################
    //VlocityLogger.e(CLASS_NAME+'-processDetail');
    //##################################################################

    //System.debug('Detail forOLI' + JSON.serialize(detail));
    if (detail != null) {
      for (String key : detail.keySet()) {
        if (key != 'Item Attr') {
          row.put(key, detail.get(key));
        }
      }
      // Check for the attribute JSON string
      if (detail.containsKey('Item Attr')) {
        Object itemAttrsJSON = detail.get('Item Attr');
        if (itemAttrsJSON instanceof String) {
          getAttributes((String)itemAttrsJSON, row, data);
        }
      }
    }



    //##################################################################
    //VlocityLogger.x(CLASS_NAME+'-processDetail');
    //##################################################################

  }

  /**
  * Extracts all attributes and values from the attribute JSON and adds them to the matrix row
  */
  @TestVisible
  private void getAttributes(String prodAttrJSON, Map<String, Object> row, Map<String, Object> data) {


    //##################################################################
    //VlocityLogger.e(CLASS_NAME+'-getAttributes');
    //##################################################################


    //////system.debug(LoggingLevel.ERROR,'prodAttrJSON ' + prodAttrJSON);
    if (String.isBlank(prodAttrJSON)) {
      return;
    }
    Map<String, Object> rowMap = row;
    Object jsonAttr = JSON.deserializeUntyped(prodAttrJSON);

    if (jsonAttr instanceof List<Object>) {
      //////system.debug(LoggingLevel.ERROR,'jsonAttr is a List<Object>');
      return;
    } else if (jsonAttr == null) {
      //////system.debug(LoggingLevel.ERROR,'jsonAttr is null');
      return;
    }

    Map<String, Object> attrMap = (Map<String, Object>)jsonAttr;
    Boolean bFirst = true;
    for (String key : attrMap.keySet()) {
      //////system.debug(LoggingLevel.ERROR,'key ' + key);
      Object attrs = attrMap.get(key);
      if (attrs instanceof List<Object>) {
        //////system.debug(LoggingLevel.ERROR,'attrs ' + attrs);
        integer searchCounter = 0;
        string rowNodeTochange = '';
        integer position = 0;
        map<integer, string> attrPosition = new map<integer, string> ();
        list<CalcMatrixdataWrapper> charactersticNameWrapperList = new list<CalcMatrixdataWrapper>();
        Map<string, string> charactersticNameValueMap = new map<string, string>();
        string attrNamedelimited = '';
        string attrValuedelimited = '';
        List<Object> attrList = (List<Object>)attrs;
        //////system.debug('attrList'+attrList);
        for (Object attr : attrList) {


          if (!bFirst) {
            rowMap = rowMap.clone();
            data.put('Row' + data.size(), rowMap);
          } else {
            bFirst = false;
          }
          Map<String, Object> attrInfo = (Map<String, Object>)attr;

          String attrName = (String)attrInfo.get('attributedisplayname__c');

          Object propertyValue;

          try {
            if (attrName != null && attrName.equalsIgnoreCase('Rate Sub-Band') ) {
              rowMap.put('Characteristic Name', attrName);
              rowMap.put('Characteristic Value', sRateSubBand);
              propertyValue = sRateSubBand;

              //continue;
            } else if (attrName != null && attrName.equalsIgnoreCase('Rate Band') ) {
              rowMap.put('Characteristic Name', attrName);
              rowMap.put('Characteristic Value', sRateBand);
              propertyValue = sRateBand;
              //continue;
            } else if (attrName != null && attrName.equalsIgnoreCase('Forborne') ) {
              rowMap.put('Characteristic Name', attrName);
              rowMap.put('Characteristic Value', sForborne);
              //system.debug('>>>>>>Forborne Value' + attrName + '____' + sForborne);
              propertyValue = sForborne;
              //continue;
            } else if (attrName != null && attrName.equalsIgnoreCase('Market') ) {
              rowMap.put('Characteristic Name', attrName);
              rowMap.put('Characteristic Value', sMarket);
              //system.debug('>>>>>>Market Value' + attrName + '::____' + sMarket);
              propertyValue = sMarket;
              //continue;
            } else if (attrName != null && attrName.equalsIgnoreCase('Chargeable')) {

              if (attrInfo.ContainsKey('value__c')) {
                rowMap.put('Characteristic Name', attrName);
                rowMap.put('Characteristic Value', String.valueof(attrInfo.get('value__c')) );
                propertyValue = String.valueof(attrInfo.get('value__c'));
              }
              /*
               else if(attrInfo.ContainsKey('value__c') && String.valueof(attrInfo.get('value__c')).equalsIgnoreCase('No')){
                  rowMap.put('Characteristic Name', attrName);
                  rowMap.put('Characteristic Value', 'Yes' );
               }*/
              //continue;
            }

            else {
              rowMap.put('Characteristic Name', attrName);
              Map<String, Object> attributeRunTimeInfo =  (Map<String, Object>)attrInfo.get('attributeRunTimeInfo');

              if (attributeRunTimeInfo == null) {
                //////system.debug('attributeRunTimeInfo is null for ' + attrInfo);
                continue;
              }

              String dataType = (String)attributeRunTimeInfo.get('dataType');

              if (dataType == null) {
                //////system.debug(LoggingLevel.ERROR,'Unable to find the dataType for attribute name ' + attrName);
                continue;
              }

              //get initial value
              propertyValue = attributeRunTimeInfo.get('value');
              if (propertyValue == null) {
                propertyValue = attributeRunTimeInfo.get('default');
              }



              String normDataType = dataType.toLowerCase();
              if (normDataType == 'multi picklist') {
                propertyValue = attributeRunTimeInfo.get('selectedItems');
                if (propertyValue == null || ((List<Object>) propertyValue).isEmpty()) {

                  List<Object> values = (List<Object>)attributeRunTimeInfo.get('default');
                  String valuesDelimited = '';
                  for (Integer index = 0; index < values.size(); index ++) {
                    Map<String, Object> value = (Map<String, Object>)values[index];
                    valuesDelimited += getPropertyValue(value) + ';';
                    //////system.debug('propertyValue 1 is:' + propertyValue);
                  }
                  if (valuesDelimited.length() > 0) {
                    propertyValue = valuesDelimited.subString(0, valuesDelimited.length() - 1);
                    rowMap.put('Characteristic Value', propertyValue);

                    // charactersticNameValueMap.put(attrName,(string)propertyValue);
                  }
                } else {
                  //////system.debug('propertyValue 2 is:' + propertyValue);
                  List<Object> values = (List<Object>)propertyValue;
                  String valuesDelimited = '';
                  for (Integer index = 0; index < values.size(); index ++) {
                    Map<String, Object> value = (Map<String, Object>)values[index];
                    valuesDelimited += getPropertyValue(value) + ';';
                  }
                  if (valuesDelimited.length() > 0) {
                    propertyValue = valuesDelimited.subString(0, valuesDelimited.length() - 1);
                    rowMap.put('Characteristic Value', propertyValue);
                    //charactersticNameValueMap.put(attrName,(string)propertyValue);
                  }
                }
              } else if (normDataType == 'picklist') {

                propertyValue = attributeRunTimeInfo.get('selectedItem');
                if (propertyValue == null) {
                  //////system.debug('propertyValue 3 is:' + propertyValue);
                  List<Object> values = (List<Object>)attributeRunTimeInfo.get('default');
                  if (values != null && !values.isEmpty()) {
                    String valuesDelimited = null;
                    Map<String, Object> value = (Map<String, Object>)values[0];
                    valuesDelimited = getPropertyValue(value);
                    propertyValue = valuesDelimited;
                    rowMap.put('Characteristic Value', propertyValue);

                  }
                } else {
                  //////system.debug('propertyValue 4 is:' + propertyValue);
                  if (propertyValue instanceof Map<String, Object>) {
                    Map<String, Object> value = (Map<String, Object>)propertyValue;
                    propertyValue = getPropertyValue(value);
                  } else if (propertyValue instanceof String) {
                    propertyValue = getPropertyValue2(attributeRunTimeInfo.get('values'), (String)propertyValue);
                    //////system.debug('getPropertyValue2 returned ' + propertyValue);
                  }
                  rowMap.put('Characteristic Value', propertyValue);
                }
              } else {
                rowMap.put('Characteristic Value', propertyValue);
              }
            }
            //System.debug('rowMap_Testing'+rowMap);
            if (rowMap.containsKey('Reference External Id') && cmrExtIDtoWrapperMap.containsKey((string) rowMap.get('Reference External Id'))) {
              charactersticNameWrapperList = cmrExtIDtoWrapperMap.get((string) rowMap.get('Reference External Id'));
              //////system.debug('cmWrapper++++'+charactersticNameWrapperList);
              for (CalcMatrixdataWrapper cmWrapper : charactersticNameWrapperList) {
                if (cmWrapper.charactersticNameOrderMap.containsKey(attrName) && propertyValue != null) {
                  // cmWrapper.searchCount++;
                  //cmWrapper.runTimeattrPosition.put(cmWrapper.charactersticNameOrderMap.get(attrName), new CharactersticNameValue(attrName, string.valueof( propertyValue)));
                  if (cmWrapper.itemIDToSearchCount.containsKey((string) rowMap.get('ItemID'))) {
                    if (propertyValue != null && cmWrapper.matrixcharNameValueMap != null && cmWrapper.matrixcharNameValueMap.containskey(attrName) && String.valueof(propertyValue).equalsIgnoreCase(cmWrapper.matrixcharNameValueMap.get(attrName))) {
                      Integer searchCount = cmWrapper.itemIDToSearchCount.get((string) rowMap.get('ItemID')) + 1;
                      //////system.debug('ItemID::::' +(string) rowMap.get('ItemID') +'-----Search Count _____' + searchCount );
                      cmWrapper.itemIDToSearchCount.put((string) rowMap.get('ItemID'), searchCount);
                    }
                  } else if (propertyValue != null && cmWrapper.matrixcharNameValueMap != null && cmWrapper.matrixcharNameValueMap.containskey(attrName) && String.valueof(propertyValue).equalsIgnoreCase(cmWrapper.matrixcharNameValueMap.get(attrName))  ) {
                    cmWrapper.itemIDToSearchCount.put((string) rowMap.get('ItemID'), 1);
                  }
                  if (cmWrapper.ItemrRunTimeattrPositionMap.containsKey((string) rowMap.get('ItemID'))) {
                    cmWrapper.ItemrRunTimeattrPositionMap.get((string) rowMap.get('ItemID')).put(cmWrapper.charactersticNameOrderMap.get(attrName), new CharactersticNameValue(attrName, string.valueof( propertyValue)));
                    //system.debug('>>>>111' + (string) rowMap.get('ItemID'));
                    //system.debug('cmWrapper*&*&&*___' + cmWrapper);
                  } else {
                    cmWrapper.runTimeattrPosition.put(cmWrapper.charactersticNameOrderMap.get(attrName), new CharactersticNameValue(attrName, string.valueof( propertyValue)));
                    cmWrapper.ItemrRunTimeattrPositionMap.put((string) rowMap.get('ItemID'), cmWrapper.runTimeattrPosition);
                  }
                }
              }
            }
          } catch (Exception e) {
            system.debug(LoggingLevel.ERROR, 'Exception:::::' + e);
            system.debug(LoggingLevel.ERROR, 'Exception stack trace:::::' + e.getStackTraceString());
            throw e;
          }
        } // end for loop
        //system.debug('rowMap__________' + JSON.serializePretty(rowMap));
        getDelimitedRowNode(rowMap, data);


      } // end if
     
    }

    //##################################################################
    //VlocityLogger.x(CLASS_NAME+'-getAttributes');
    //##################################################################

  }

  private String getPropertyValue2(Object valuesList, String propertyValue) {

    //##################################################################
    //VlocityLogger.e(CLASS_NAME+'-getPropertyValue2');
    //##################################################################


    //////system.debug('valuesList ' + valuesList);
    if (valuesList instanceof List<Object>) {
      List<Object> objList = (List<Object>)valuesList;
      String internalVal, displayVal;
      for (Object obj : objList) {
        if (obj instanceof Map<String, Object>) {
          Map<String, Object> valueMap = (Map<String, Object>)obj;
          internalVal = String.valueOf(valueMap.get('value'));
          displayVal = String.valueOf(valueMap.get('displayText'));
          if (internalVal == propertyValue || displayVal == propertyValue) {
            if (this.useDisplayText) {
              return displayVal;
            } else {
              if (String.isBlank(internalVal))
                return displayVal;
              else
                return internalVal;
            }
          }
        }
      }
    }


    //##################################################################
    //VlocityLogger.x(CLASS_NAME+'-getPropertyValue2');
    //##################################################################


    return propertyValue;
  }

  private String getPropertyValue(Map<String, Object> valueMap) {

    //##################################################################
    //VlocityLogger.e(CLASS_NAME+'-getPropertyValue');
    //##################################################################


    if (this.useDisplayText) {
      return String.valueOf(valueMap.get('displayText'));
    }
    String propertyValue = String.valueOf(valueMap.get('value'));
    if (String.isBlank(propertyValue)) {
      propertyValue = String.valueOf(valueMap.get('displayText'));
    }

    //##################################################################
    //VlocityLogger.x(CLASS_NAME+'-getPropertyValue');
    //##################################################################


    return propertyValue;
  }

  /*private String getConfigurationValue(String setupName) {

      //##################################################################
       //VlocityLogger.e(CLASS_NAME+'-getConfigurationValue');
      //##################################################################


      vlocity_cmt__CpqConfigurationSetup__c cpqSetup = vlocity_cmt__CpqConfigurationSetup__c.getInstance(setupName);
      String retval = null;
      if (cpqSetup != null
          && cpqSetup.vlocity_cmt__SetupValue__c != null
          && cpqSetup.vlocity_cmt__SetupValue__c.length() > 0) {
              retval = cpqSetup.vlocity_cmt__SetupValue__c;
          }

      //##################################################################
       //VlocityLogger.x(CLASS_NAME+'-getConfigurationValue');
      //##################################################################


      return retval;
  } */

  // Query CalculationMarix data to get the characterstic name, value for future comparision.
  private void getCalcMatrixdata(string CalCulationMatrixName ) {

    //##################################################################
    //VlocityLogger.e(CLASS_NAME+'-getCalcMatrixdata');
    //##################################################################
    //String calcMatrixCacheKey = 'local.orderingCache.preprocMatrixWrapperData';
    /* if(!Test.isRunningTest() && Cache.Org.contains(calcMatrixCacheKey)) {
      System.debug('getting Matrix data from platform cache....');
          cmrExtIDtoWrapperMap = (map<string, list<CalcMatrixdataWrapper>>) Cache.Org.get(calcMatrixCacheKey);
      }
      else { */
    System.debug('querying Matrix data ....');
    for ( vlocity_cmt__CalculationMatrixRow__c cmr : [select id, name, vlocity_cmt__InputData__c, vlocity_cmt__OutputData__c
          from vlocity_cmt__CalculationMatrixRow__c where
          vlocity_cmt__CalculationMatrixVersionId__r.vlocity_cmt__CalculationMatrixId__r.name = :CalCulationMatrixName AND
              vlocity_cmt__CalculationMatrixVersionId__r.vlocity_cmt__IsEnabled__c = true AND name != 'Header' ] ) {
      //system.debug('outputdata______' + cmr.vlocity_cmt__OutputData__c);
      object inputdata = JSON.deserializeUntyped(cmr.vlocity_cmt__InputData__c);
      object outputdata = JSON.deserializeUntyped(cmr.vlocity_cmt__OutputData__c);
      String MRC ;
      String NRC;
      if (outputdata instanceof Map<String, Object> ) {
        Map<string, Object> outputDataMap = ( Map<string, Object>)outputdata;
        if (outputDataMap != null && outputDataMap.ContainsKey('MRC') && outputDataMap.ContainsKey('NRC')) {
          MRC = String.valueOf(outputDataMap.get('MRC'));
          NRC = String.valueOf(outputDataMap.get('NRC'));
        }
      }

      if (inputdata instanceof Map<String, Object> ) {
        Map<string, Object> inputDataMap = ( Map<string, Object>)inputdata;

        if (inputDataMap.ContainsKey('Reference External Id') && inputDataMap.ContainsKey('Characteristic Name') ) {
          string externalId = string.valueof(inputDataMap.get('Reference External Id'));
          string charactersticName = string.valueof(inputDataMap.get('Characteristic Name'));
          string charactersticValue = string.valueof(inputDataMap.get('Characteristic Value'));
          Set<string> charNameSet = new set<string>();
          List<string> charValueList = new List<string>();
          boolean addRecordtoWrapper = true;
          charNameSet.addAll(string.valueof(inputDataMap.get('Characteristic Name')).split(';'));
          charValueList.addAll(string.valueof(inputDataMap.get('Characteristic Value')).split(';'));
          if (cmrExtIDtoWrapperMap.containsKey(externalId) ) {

            cmrExtIDtoWrapperMap.get(externalId).add(new CalcMatrixdataWrapper(charactersticName, charNameSet, charValueList, 0, externalID, MRC, NRC));
          } else {
            list<CalcMatrixdataWrapper> calcMatrixDataList = new list<CalcMatrixdataWrapper>();
            calcMatrixDataList.add(new CalcMatrixdataWrapper(charactersticName, charNameSet, charValueList, 0, externalID, MRC, NRC));
            cmrExtIDtoWrapperMap.put(externalId, calcMatrixDataList);
          }
        }

      }
    }
    //if (!Test.isRunningTest())
      //Cache.Org.put(calcMatrixCacheKey, cmrExtIDtoWrapperMap);
    //}
    // system.debug('cmrExtIDtoWrapperMap-Size::' + JSON.serializePretty(cmrExtIDtoWrapperMap));
    //##################################################################
    //VlocityLogger.x(CLASS_NAME+'-getCalcMatrixdata');
    //##################################################################

  }


  @TestVisible
  private void getDelimitedRowNode(Map<String, Object> rowMap, Map<String, Object> data) {

    //##################################################################
    //VlocityLogger.e(CLASS_NAME+'-getDelimitedRowNode');
    //##################################################################


    String attrName ;
    String attrValue ;
    list<CalcMatrixdataWrapper> charactersticNameWrapperList = new list<CalcMatrixdataWrapper>();
    Map<string, string> charactersticNameValueMap = new map<string, string>();
    string attrNamedelimited = '';
    string attrValuedelimited = '';
    String externalID = (string) rowMap.get('Reference External Id');
    String oliId = (string) rowMap.get('ItemID');

    system.debug('oliId__' + oliId);

    if (cmrExtIDtoWrapperMap.containsKey(externalID)) {
      charactersticNameWrapperList = cmrExtIDtoWrapperMap.get(externalID);
      for (CalcMatrixdataWrapper cmWrapper : charactersticNameWrapperList) {

        //system.debug('cmWrapper___' + Json.SerializePretty(cmWrapper));
        if (integer.valueOf(cmWrapper.itemIDToSearchCount.get(oliId)) == cmWrapper.charactersticNameOrderMap.size() && cmWrapper.charactersticNameOrderMap.size() > 1) {
          //list < integer > attrNameposition = new list < integer > (cmWrapper.runTimeattrPosition.keyset());
          List < integer > attrNameposition = New List<Integer> (cmWrapper.ItemrRunTimeattrPositionMap.get(oliId).keySet());

          attrNameposition.sort();
          List < string > attrNameOrderedListName = new List < string > ();
          List < string > attrNameOrderedListValue = new List < string > ();
          for (integer i : attrNameposition) {
            attrNameOrderedListName.add(cmWrapper.runTimeattrPosition.get(i).name);
            attrNameOrderedListValue.add(cmWrapper.runTimeattrPosition.get(i).value);
          }
          rowMap = rowMap.clone();
          // system.debug('rowMap Before delimited::' + rowMap);

          rowMap.put('Characteristic Name', string.join(attrNameOrderedListName, ';'));
          rowMap.put('Characteristic Value', string.join(attrNameOrderedListValue, ';'));
          rowMap.put('Matrix MRC', cmWrapper.MRC);
          rowMap.put('Matrix NRC', cmWrapper.NRC);
          //system.debug('rowMap After delimited::' + JSON.serializePretty(rowMap));
          data.put('Row' + (data.size()), rowMap);
        } else if (  cmWrapper.charactersticNameOrderMap.size() == 1 && cmWrapper.ItemrRunTimeattrPositionMap != null && cmWrapper.ItemrRunTimeattrPositionMap.size() >= 1 && cmWrapper.ItemrRunTimeattrPositionMap.containskey(oliId)) {
          Map<Integer, OCOM_Optimizd_Preprocessor.CharactersticNameValue> runtimeMap =   (Map<Integer, OCOM_Optimizd_Preprocessor.CharactersticNameValue>) (cmWrapper.ItemrRunTimeattrPositionMap.get(oliId));

          if (runtimeMap != null && runtimeMap.containsKey(1)) {
            OCOM_Optimizd_Preprocessor.CharactersticNameValue  runTimeAttrNamevalueMap = (OCOM_Optimizd_Preprocessor.CharactersticNameValue)runtimeMap.get(1);
            attrValue = runTimeAttrNamevalueMap.value;
            attrName = runTimeAttrNamevalueMap.name;
            system.debug('attrName___' + attrName);
            system.debug('attrValue___' + attrValue);
            system.debug('cmWrapper.matrixcharNameValueMap' + cmWrapper.matrixcharNameValueMap);
            //list < integer > attrNameposition = new list < integer > (cmWrapper.runTimeattrPosition.keyset());
            if (attrName != null && attrValue != null && cmWrapper.matrixcharNameValueMap != null &&  cmWrapper.matrixcharNameValueMap.Containskey(attrName) && attrValue.equalsIgnoreCase(cmWrapper.matrixcharNameValueMap.get(attrName))) {
              rowMap.put('Matrix MRC', cmWrapper.MRC);
              rowMap.put('Matrix NRC', cmWrapper.NRC);
            }
          }
          //system.debug('rowMap After delimited::' + JSON.serializePretty(rowMap));
          //data.put('Row'+(data.size()),rowMap);
        }
      }
    }

    //##################################################################
    //VlocityLogger.x(CLASS_NAME+'-getDelimitedRowNode');
    //##################################################################


  }


  private class CalcMatrixdataWrapper {
    string charactersticName {get; set;}
    set<string> charactersticNameSet = new set<string>();
    set<string> charactersticValueSet = new set<string>();
    map<string, string> matrixcharNameValueMap = new Map<String, String>();
    map<string, string> runTimecharNameValueMap {set; get;}
    map<string, integer>charactersticNameOrderMap = new map<string, integer>();
    map<integer, CharactersticNameValue> runTimeattrPosition = new map<integer, CharactersticNameValue>();
    integer searchCount {get; set;}
    integer i = 1;
    string externalID = '';
    Map<String, Integer> itemIDToSearchCount = new Map<String, Integer>();
    Map<String, Map<integer, CharactersticNameValue> > ItemrRunTimeattrPositionMap =  new Map<String, Map<integer, CharactersticNameValue> >();
    Map<String, Map<String, String>> extIDToMRCNRCMap = new Map<String, Map<String, String>>();
    String MRC ;
    String NRC ;

    public CalcMatrixdataWrapper(string charactersticName,  Set<string> charactersticNameSet, List<string> charactersticValueList, integer searchCount , String externalID , String MRC, String NRC) {
      this.charactersticName = charactersticName;
      this.charactersticNameSet = charactersticNameSet;
      this.charactersticValueSet.addall(charactersticValueList);
      //this.runTimecharNameValueMap = charactersticNameValueMap;
      this.searchCount = searchCount;
      this.externalID = externalID;
      this.MRC = MRC;
      this.NRC = NRC;
      integer j = 0;
      for (string cName :  charactersticNameSet) {
        charactersticNameOrderMap.put(cName, i);

        if (j < charactersticValueList.size())
          matrixcharNameValueMap.put(cName, charactersticValueList[j] );
        i++;
        j++;
      }
    }
  }
  public class CharactersticNameValue {
    public string name;
    public string value;
    public CharactersticNameValue(string name, string value) {
      this.name = name;
      this.value = value;
    }
  }
}