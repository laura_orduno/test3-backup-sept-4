public class OCOM_AgreementDetails {
    public String PAData { get; set; }
    public order objOrder{get;set;}
    public boolean pickerchecked{get;set;}
    public string templatePicker{get;set;}
    public string orId{get;set;}
    public string accountID {get;set;}
    public boolean isSelected{get;set;}
    map<string,object> inputMap=new map<string,object>();
    map<string,object> outputMap=new map<string,object>();
    public list<string> filter ;
    public contractDetailsWrapper objcontractableWrapper;
    public static boolean  blncontractable{get;set;}
    public String searchString {get; set;}
    public List<Contact>conSearchResult {get;set;}
    public string ContId {get;set;}
    public Contact new_contact {get; set;}
    public string errorDsiplay {get;set;}
    public OCOM_AgreementDetails(){
        blncontractable=false;
        new_contact=new Contact();
        orId=ApexPages.currentPage().getParameters().get('id');
        isSelected = true;
    }
    private void getOrderProduct(map<string,object> inputmap,map<string,object> outputMap){
        map<Id,Id> mapOLIWithProductIds=new map<Id,Id>();
        list<OrderItem> lstOrderItem =new list<OrderItem>();
        Boolean areContractable = false;
        system.debug('>>OrderId::'+ orId);
        // 29 Nov 2017 - RTA 497 - Sandip - added condition for parentId__c in where clause
        for(OrderItem OrdrItem : [Select Id,PriceBookEntry.Product2.Id ,PriceBookEntry.product2.Sellable__c,
                                  Pricebookentry.Product2.vlocity_cmt__TrackAsAgreement__c
                                  FROM OrderItem WHERE (OrderId = :orId OR Order.ParentId__c =: orId) and
                                  PriceBookEntry.product2.Sellable__c =true
                                  ORDER BY vlocity_cmt__LineNumber__c]){
                                      if(OrdrItem.Pricebookentry.Product2.vlocity_cmt__TrackAsAgreement__c == true  &&
                                         OrdrItem.PriceBookEntry.product2.Sellable__c == true && areContractable != true)
                                          areContractable = true;
                                      if( areContractable == true){
                                          templatePicker= OCOM_ContractUtil.getTemplateName();
                                          outputMap.put('areContractable',areContractable);
                                          outputMap.put('templatePicker',templatePicker);
                                      }
                                      system.debug('Contractable Check___ ' + areContractable + 'Track' + OrdrItem.Pricebookentry.Product2.vlocity_cmt__TrackAsAgreement__c );
                                  }
        outputMap.put('OrderItem',lstOrderItem);
        system.debug('>>lstOrderItem' + lstOrderItem);
        inputmap.put('LiToProducts',mapOLIWithProductIds);
    }
    public class contractDetailsWrapper{
        public boolean iscontractable{get;set;}
        public string templatePicker{get;set;}
        public string contactName{get;set;}
        public string contactEmail{get;set;}
        public string contactPhone{get;set;}
        public string contactTitle{get;set;}
        public string deliveryMethod{get;set;}
        public order orders{get;set;}
        public contractDetailsWrapper(){
            this.iscontractable=false;
        }
    }
    public contractDetailsWrapper getcontractableOrderCheck(){
        if(blncontractable == true){
            return objcontractableWrapper;
        }
        if(objcontractableWrapper == null)
            objcontractableWrapper=new   contractDetailsWrapper();
        boolean iscontractable=false;
        map<string,object> inputMap=new map<string,object>{'orderId' => orId};
            map<string,object> outputMap=new map<string,object>();
        getOrderProduct(inputMap,outputMap);
        if(outputMap.containsKey('areContractable')){
            blncontractable=true;
            objcontractableWrapper.iscontractable=(boolean)outputMap.get('areContractable');
        }
        if(outputMap.containsKey('templatePicker')){
            objcontractableWrapper.templatePicker=(string)outputMap.get('templatePicker');
        }
        return objcontractableWrapper;
    }
    public list<contractDetailsWrapper> getOrderDetail(){
        list<contractDetailsWrapper> orderDetailWrapper = new list<contractDetailsWrapper>();
        System.debug('***objOrder in Constructor*****'+objOrder);
        if(objOrder != null && objOrder.AccountId != null ){
            accountID = string.valueof(objOrder.AccountId);
        }
        if(objOrder != null && objOrder.CustomerSignedBy__c == null )
        {
            system.debug('Getting CustomerAutrizedBy Details' + objOrder.CustomerAuthorizedBy.Name);
            objcontractableWrapper = new contractDetailsWrapper();
            objcontractableWrapper.contactName = objOrder.CustomerAuthorizedBy.Name;
            objcontractableWrapper.contactEmail = objOrder.CustomerAuthorizedBy.Email;
            objcontractableWrapper.contactPhone = objOrder.CustomerAuthorizedBy.Phone;
            objcontractableWrapper.contactTitle = objOrder.CustomerAuthorizedBy.Title;
            objcontractableWrapper.deliveryMethod = objOrder.DeliveryMethod__c;
            objcontractableWrapper.orders = objOrder;
            orderDetailWrapper.add(objcontractableWrapper);
        }
        else if(objOrder != null && objOrder.CustomerSignedBy__c != null )
        {
            system.debug('Getting CustomerSignedBy Details' + objOrder.CustomerSignedBy__r.Name);
            objcontractableWrapper = new contractDetailsWrapper();
            objcontractableWrapper.contactName = objOrder.CustomerSignedBy__r.Name;
            objcontractableWrapper.contactEmail = objOrder.CustomerSignedBy__r.Email;
            objcontractableWrapper.contactPhone = objOrder.CustomerSignedBy__r.Phone;
            objcontractableWrapper.contactTitle = objOrder.CustomerSignedBy__r.Title;
            objcontractableWrapper.deliveryMethod = objOrder.DeliveryMethod__c;
            objcontractableWrapper.orders = objOrder;
            orderDetailWrapper.add(objcontractableWrapper);
        }
        return orderDetailWrapper;
    }
     public list<Contact> loadContacts()
        {
            System.debug('***AccountID*****'+accountID );
            list<Contact>contacts = [Select Name, Title, Phone, Email, Active__c,LastModifiedDate  From Contact Where AccountId = :accountID Order By Active__c DESC, CreatedDate DESC limit 50];
            system.debug('@@Contacts'+contacts);
            return contacts;
        }
    
     public list<Contact> getSearchResult()
          {
            conSearchResult = new list<Contact>();
            try {
                if (searchString == null || searchString == '') 
                { 
                    conSearchResult = loadContacts(); 
                    return conSearchResult; 
                }
                searchString = '%' + searchString + '%';
                system.debug('Search String' +searchString );
                conSearchResult =[select id, name, email, phone, title, Active__c,LastModifiedDate 
                                            from Contact where AccountId = :accountID 
                                            and (Name Like :searchString OR email Like :searchString OR phone Like :searchString) 
                                            limit 50];
 
            } catch (Exception e) {
              return null;
             }
             return conSearchResult;
          }
    
     public PageReference onSelectContact() {
      return null;
    }
    
    
     public PageReference UpdateOrder(){
        System.debug('Selected Contact' + ContId + '___' +orId);

             
              if( orId != null){
                  
                  // Update Order
                  Order orToUpdate  = new Order(id= orId);  
                  if(ContId !=null)
                      orToUpdate.CustomerSignedBy__c = ContId;
                  //getOrderDetail();
                  System.debug('Order Contact'+ orToUpdate.CustomerSignedBy__c);
                  orToUpdate.DeliveryMethod__c=objOrder.DeliveryMethod__c;
                  update orToUpdate;
              }
          		 
            objcontractableWrapper=new contractDetailsWrapper();
            Pagereference Pr= new pagereference('/apex/OCOM_AdditionalDetails?id='+objOrder.id);
            Pr.setRedirect(true);                         
            return pr;
            //return null;
      }
    
    public Pagereference createNewContact() {
        system.debug('New Contact '+new_contact);
            
        if(new_contact != null){
             new_contact.AccountId = accountID;
      if(new_contact.FirstName == '' || new_contact.FirstName == null){
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Please enter Contact First Name'));
      }
 
      else if(new_contact.LastName == '' || new_contact.FirstName == null){
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please enter Contact Last Name'));
      }
 
      else if(new_contact.phone == '' || new_contact.phone == null){
       ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please enter Contact phone'));
      }
 
      else if(new_contact.Email == '' || new_contact.Email == null){
       ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Please enter Contact Email'));
      }
 
     else if(new_contact.Language_Preference__c == '' || new_contact.Language_Preference__c == null){
       ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Please enter Lanuguage Preference'));
      }
      else
            insert new_contact;
        }
                
             	
			
        if(new_contact !=null  && new_contact.id != null){
			 system.debug('New ContactName '+new_contact.name + new_contact.id);
            
  				  contID = new_contact.id;
                  System.debug('Selected Contact' + ContId);
                  Order orToUpdate  = new Order(id= orId);
                  orToUpdate.CustomerSignedBy__c = ContId;
                  
                  update orToUpdate;
                  System.debug('Order Contact'+ orToUpdate.CustomerAuthorizedById);
   
                //loadContacts();
            }
           Pagereference Pr= new pagereference('/apex/OCOM_AdditionalDetails?id='+objOrder.id);
            Pr.setRedirect(true); 
            return Pr;
         }
}