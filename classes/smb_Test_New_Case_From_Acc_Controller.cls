/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class smb_Test_New_Case_From_Acc_Controller {

    static testMethod void newCaseFromAccountControllerTest() {
    	
       //PageReference ref = new PageReference('/apex/smb_New_Case_From_Account'); 
       PageReference ref = Page.smb_New_Case_From_Account;
       Test.setCurrentPage(ref);     	
    	
    	
	   RecordType recordType = [Select r.Id, r.DeveloperName From RecordType r where r.SobjectType = 'Account' and r.DeveloperName='RCID' limit 1];
	        
	   Account acc1 = new Account(Name='Testing Software', BillingCountry = 'US', BillingState = 'IL', recordTypeId=recordType.Id , No_Chronic_Incidents__c =10.0);
	   insert acc1;
	   
	   Contact cont = new Contact(Lastname= 'lastname', Account = acc1);
	   insert cont;
	   ApexPages.currentPage().getParameters().put('aid', acc1.id); 
	   
	   smb_New_Case_From_Account_Controller smb = new smb_New_Case_From_Account_Controller();
	   smb.new_contact =  cont; 
	   //smb.new_contact = [Select FirstName, LastName, Name, Email, Title, Phone From Contact Where Id = :cont.Id];
	   smb.selectedContactId = cont.id;
	   smb.searchTerm = 'abc';
	   smb.onLoad();
	   smb.onSearch();
	   smb.throwExceptionMsg('message','warning');
	   ref = smb.onSelectContact();
	   ref = smb.createNewContact();
	   smb.addETP('e');
	   smb.clearExceptionArea();
	   smb.clear();
	   smb.throwCustomExceptionMsg('msgTitle1', 'msgSummary1', true, 'msgDetail1', 1, 'msgSeverity1');
	   smb.toggleNewContactForm();

	   
    }   
    
    
    static testMethod void newCaseFromAccountControllerTest2() {
    	
       //PageReference ref = new PageReference('/apex/smb_New_Case_From_Account'); 
       PageReference ref = Page.smb_New_Case_From_Account;
       Test.setCurrentPage(ref);     	
    	
    	
	   RecordType recordType = [Select r.Id, r.DeveloperName From RecordType r where r.SobjectType = 'Account' and r.DeveloperName='RCID' limit 1];
	        
	   Account acc1 = new Account(Name='Testing Software', BillingCountry = 'US', BillingState = 'IL', recordTypeId=recordType.Id , No_Chronic_Incidents__c =10.0);
	   insert acc1;
	   
	   Contact cont = new Contact(Lastname= 'lastname', Account = acc1);
	   insert cont;
	   ApexPages.currentPage().getParameters().put('aid', acc1.id); 
	   
	   smb_New_Case_From_Account_Controller smb = new smb_New_Case_From_Account_Controller();
	    
	   smb.new_contact = [Select FirstName, LastName, Name, Email, Title, Phone From Contact Where Id = :cont.Id];
	   smb.selectedCaseRecordType = 'CHOOSEAGAIN';
	   smb.searchTerm = 'abc';
	   smb.onLoad();
	   smb.onSearch();
	   
	   ref = smb.onSelectContact();
	   ref = smb.createNewContact();
	   smb.addETP('e');
	   smb.clearExceptionArea();
	   smb.clear();
	   smb.throwCustomExceptionMsg('msgTitle1', 'msgSummary1', true, 'msgDetail1', 1, 'msgSeverity1');
	   smb.toggleNewContactForm();
	   smb.throwExceptionMsg('message','confirm');

	   
    }      
}