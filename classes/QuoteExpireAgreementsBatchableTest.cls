@isTest
private class QuoteExpireAgreementsBatchableTest {
	private static testMethod void testQuoteExpireAgreementsBatchable() {
    	Agreement__c agreement = new Agreement__c(
    		Type__c = 'test',
    		Status__c = QuoteAgreementStatus.PENDING,
    		Expiry_Date__c = Date.today()
    	);
    	insert agreement;
    	
    	Test.startTest();
    	
    	Database.executeBatch(new QuoteExpireAgreementsBatchable(), 9);
    	
    	Test.stopTest();
    	
    	agreement = [SELECT Status__c FROM Agreement__c WHERE Id = :agreement.id];
    	
    	system.assertEquals(QuoteAgreementStatus.EXPIRED, agreement.status__c);
    }
}