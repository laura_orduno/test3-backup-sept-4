/**
 * Unit tests for <code>QuoteCalculator</code> class.
 * 
 * @author Max Rudman
 * @since 10/28/2010
 */
@isTest
private class QuoteCalculatorTests {
	private static QuoteVO vo;
	private static Product2 product;
	private static Product2 subscription;
	
	testMethod static void testComponentQuantity() {
		setUp();
		
		// Add two more lines
		vo.addLineItem(new SBQQ__QuoteLine__c(SBQQ__Product__c=product.Id));
		vo.addLineItem(new SBQQ__QuoteLine__c(SBQQ__Product__c=product.Id));
		
		// Make 1st line a component of 2nd
		vo.lineItems[0].requiredBy = vo.lineItems[1].line;
		vo.lineItems[1].addComponent(vo.lineItems[0]);
		vo.lineItems[0].bundledQuantity = 3;
		vo.lineItems[0].lineNumber = 1;
		// Make 2nd line a component of 3rd
		vo.lineItems[1].requiredBy = vo.lineItems[2].line;
		vo.lineItems[2].addComponent(vo.lineItems[1]);
		vo.lineItems[1].bundledQuantity = 2;
		vo.lineItems[1].lineNumber = 2;
		
		vo.lineItems[2].quantity = 10;
		vo.lineItems[2].lineNumber = 3;
		
		QuoteCalculator target = new QuoteCalculator(new List<QuoteVO>{vo});
		target.calculate();
		System.assertEquals(3 * 2 * 10, vo.lineItems[0].quantity);
		System.assertEquals(2 * 10, vo.lineItems[1].quantity);
	}
	
	testMethod static void testComponentRollup() {
		setUp();
		
		// Add two more lines
		vo.addLineItem(new SBQQ__QuoteLine__c(SBQQ__Product__c=product.Id,SBQQ__Quantity__c=1));
		vo.addLineItem(new SBQQ__QuoteLine__c(SBQQ__Product__c=product.Id,SBQQ__Quantity__c=1));
		
		vo.lineItems[0].lineNumber = 1;
		vo.lineItems[0].listPrice = 100;
		vo.lineItems[0].bundle = true;
		
		// Make 2nd line a component of 1st
		vo.lineItems[1].requiredBy = vo.lineItems[0].line;
		vo.lineItems[1].listPrice = 200;
		vo.lineItems[1].lineNumber = 2;
		vo.lineItems[1].bundledQuantity = 1;
		vo.lineItems[1].bundle = true;
		
		// Make 3rd line a component of 2nd
		vo.lineItems[2].requiredBy = vo.lineItems[1].line;
		vo.lineItems[2].listPrice = 300;
		vo.lineItems[2].lineNumber = 3;
		vo.lineItems[2].bundledQuantity = 1;
		
		QuoteCalculator target = new QuoteCalculator(new List<QuoteVO>{vo});
		target.calculate();
		System.assertEquals(300, vo.lineItems[1].componentTotal);
		System.assertEquals(300 + 200, vo.lineItems[0].componentTotal);
	}
	
	testMethod static void testBundledQuantity() {
		setUp();
		
		vo.addLineItem(new SBQQ__QuoteLine__c(SBQQ__Product__c=product.Id,SBQQ__Quantity__c=1,SBQQ__BundledQuantity__c=15));
		vo.addLineItem(new SBQQ__QuoteLine__c(SBQQ__Product__c=product.Id,SBQQ__Quantity__c=1,SBQQ__BundledQuantity__c=12));
		vo.lineItems[0].addComponent(vo.lineItems[2]);
		vo.lineItems[2].requiredBy = vo.lineItems[0].line;
		
		vo.lineItems[0].quantity = 4;
		
		QuoteCalculator target = new QuoteCalculator(new List<QuoteVO>{vo});
		target.calculate();
		System.assertEquals(4, vo.lineItems[0].quantity);
		System.assertEquals(15, vo.lineItems[1].quantity, 'Line: ' + vo.lineItems[1].line);
		System.assertEquals(48, vo.lineItems[2].quantity, 'Line: ' + vo.lineItems[2].line);
	}
	
	private static void setUp() {
		product = new Product2(Name='Test',IsActive=true);
		subscription = new Product2(Name='Test',IsActive=true);
		insert new Product2[]{product,subscription};
		
		vo = new QuoteVO(new SBQQ__Quote__c());
		if (UserInfo.isMultiCurrencyOrganization()) {
			vo.record.put('CurrencyIsoCode', UserInfo.getDefaultCurrency());
		}
		vo.addLineItem(new SBQQ__QuoteLine__c(SBQQ__Product__c=product.Id,SBQQ__Quantity__c=1));
	}
}