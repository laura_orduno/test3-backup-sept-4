/*
    *******************************************************************************************************************************
    Class Name:     OC_ValidateCustomerOrderTest 
    Purpose:        Test code for OC_ValidateCustomerOrder class        
    Created by:     Sandip Chaudhari   02-Aug-2017        
    Modified by:    
    *******************************************************************************************************************************
*/

@isTest
private class OC_OrderStatusHelperTest {
    /*private static Order createMasterOrder(String status, Account accObj){
        Order masterOrder=new Order();
        masterOrder.AccountId=accObj.Id;
        masterOrder.CurrencyIsoCode='CAD';
        masterOrder.EffectiveDate=Date.today();
        masterOrder.Name='MasterOrder';
        masterOrder.Status=status;
        masterOrder.orderMgmtId_Status__c = status;
        masterOrder.Pricebook2Id =  customPriceBook.ID;
        return masterOrder;
    }*/
   /* @isTest
    private static void statusMapping1() {
        Account accObj = new Account(Name='TestAccount', Account_Status__c = 'Active');
        insert accObj;
        
        Product2 product4 = new Product2(Name='Collect Call Deny ',ProductCode='PROD-004', Description='This is a test JSON Product 1 Generator',isActive = true,orderMgmtId__c='9143863905013553513' ,OCOM_Bookable__c='Yes',Sellable__c = true);
       product4.vlocity_cmt__JSONAttribute__c  = '{"TELUSCHAR": [{"$$AttributeDefinitionEnd$$": null, "attributeRunTimeInfo": {"selectedItem":{"value":"Overline","displayText":"Overline","id":"9145085938413814481"}",values": [{"value": "Overline", "displayText": "Overline", "id": "9145085938413814481"}], "default": [{"value": "Overline", "displayText": "Overline", "id": "9145085938413814481"}], "uiDisplayType": "Dropdown", "dataType": "Picklist"}, "categorydisplaysequence__c": 1, "customconfiguitemplate__c": null, "attributecloneable__c": true, "valuedescription__c": null, "valuedatatype__c": "Picklist", "value__c": "Overline", "uidisplaytype__c": "Dropdown", "rulemessage__c": null, "isrequired__c": true, "id": "a7P220000008lnIEAQ", "querylabel__c": null, "isquerydriven__c": false, "isreadonly__c": true, "querycode__c": null, "objecttype__c": "Product2", "valueinnumber__c": null, "ishidden__c": true, "isconfigurable__c": true, "hasrule__c": false, "formatmask__c": null, "displaysequence__c": null, "attributedisplayname__c": "Line Type", "isactive__c": true, "attributefilterable__c": false, "attributedisplaysequence__c": "2", "attributeconfigurable__c": true, "attributeuniquecode__c": "ATTRIBUTE-549", "categoryname__c": "Characteristics", "categorycode__c": "TELUSCHAR", "attributecategoryid__c": "a7Q220000009vghEAA", "attributeid__c": "a7R220000000HqcEAE", "objectid__c": "01t22000000UXFUAA4", "$$AttributeDefinitionStart$$": null }] }';
        product4.IncludeQuoteContract__c= true;
       insert product4;

        
        Pricebook2 standardBook =  new Pricebook2(Id = Test.getStandardPricebookId(), Name = 'TestPricebook', IsActive = true); 
         PricebookEntry sbe4 = new PricebookEntry(Pricebook2Id = standardBook.Id,Product2Id = product4.Id, UnitPrice = 2, Vlocity_cmt__RecurringPrice__c = 17, IsActive = true, UseStandardPrice = false);
        insert sbe4;
        Pricebook2 customPriceBook =  new Pricebook2( Name = 'CustomPricebook', IsActive = true);
        insert customPriceBook;        
        PricebookEntry pbe4 = new PricebookEntry(Pricebook2Id = customPriceBook.Id,Product2Id = product4.Id, UnitPrice = 2, Vlocity_cmt__RecurringPrice__c = 17, IsActive = true, UseStandardPrice = false);
        insert pbe4;
        
        Order masterOrder=new Order();
        masterOrder.AccountId=accObj.Id;
        masterOrder.CurrencyIsoCode='CAD';
        masterOrder.EffectiveDate=Date.today();
        masterOrder.Name='MasterOrder';
        masterOrder.Status='Not Submitted';
        masterOrder.orderMgmtId_Status__c = 'Not Submitted';
        masterOrder.Pricebook2Id =  customPriceBook.ID;
        insert masterOrder;
        
        List<Order> childOrderList = new List<Order>();
        Order childOrder1=new Order();
        childOrder1.AccountId=accObj.Id;
        childOrder1.ParentId__c=masterOrder.Id;
        childOrder1.CurrencyIsoCode='CAD';
        childOrder1.EffectiveDate=Date.today();
        childOrder1.Name='ChildOrder1';
        childOrder1.Status='Not Submitted';
        childOrder1.Pricebook2Id =  customPriceBook.ID;
        //childOrderList.add(childOrder1);
        
        Order childOrder2=new Order();
        childOrder2.AccountId=accObj.Id;
        childOrder2.ParentId__c=masterOrder.Id;
        childOrder2.CurrencyIsoCode='CAD';
        childOrder2.EffectiveDate=Date.today();
        childOrder2.Name='ChildOrder2';
        childOrder2.Status='Not Submitted';
        childOrder2.Pricebook2Id =  customPriceBook.ID;
        //childOrderList.add(childOrder2);
        
        Test.StartTest();
        insert childOrder1;
        OrderItem  orderItem1 = new OrderItem(OrderId=childOrder1.Id, PricebookEntryId = pbe4.Id,vlocity_cmt__Product2Id__c=product4.Id, vlocity_cmt__LineNumber__c = '0001', Quantity = 1, UnitPrice = 10, vlocity_cmt__ProvisioningStatus__c = 'New');
        orderItem1.vlocity_cmt__JSONAttribute__c  = '{"TELUSCHAR": [{"$$AttributeDefinitionEnd$$": null, "attributeRunTimeInfo": {"selectedItem": {"value": "Overline1", "displayText": "Overline1", "id": "9145085938413814481"}, ",values": [{"value": "Overline", "displayText": "Overline", "id": "9145085938413814481"}], "default": [{"value": "Overline", "displayText": "Overline", "id": "9145085938413814481"}], "uiDisplayType": "Dropdown", "dataType": "Picklist"}, "categorydisplaysequence__c": 1, "customconfiguitemplate__c": null, "attributecloneable__c": true, "valuedescription__c": null, "valuedatatype__c": "Picklist", "value__c": "Overline", "uidisplaytype__c": "Dropdown", "rulemessage__c": null, "isrequired__c": true, "id": "a7P220000008lnIEAQ", "querylabel__c": null, "isquerydriven__c": false, "isreadonly__c": true, "querycode__c": null, "objecttype__c": "Product2", "valueinnumber__c": null, "ishidden__c": true, "isconfigurable__c": true, "hasrule__c": false, "formatmask__c": null, "displaysequence__c": null, "attributedisplayname__c": "Line Type", "isactive__c": true, "attributefilterable__c": false, "attributedisplaysequence__c": "2", "attributeconfigurable__c": true, "attributeuniquecode__c": "ATTRIBUTE-549", "categoryname__c": "Characteristics", "categorycode__c": "TELUSCHAR", "attributecategoryid__c": "a7Q220000009vghEAA", "attributeid__c": "a7R220000000HqcEAE", "objectid__c": "01t22000000UXFUAA4", "$$AttributeDefinitionStart$$": null }] }';
        insert orderItem1;
        childOrder1.orderMgmtId_Status__c=OrdrConstants.NC_ORDR_PROCESSING_STATUS;
        update childOrder1;
        
        Map<Id, Map<Id, List<OrderItem>>> mapId = new Map<Id, Map<Id, List<OrderItem>>>();
        Map<Id, List<OrderItem>> map1 = new Map<Id, List<OrderItem>>();
        List<OrderItem> ordItm = new List<OrderItem>();
        ordItm.add(orderItem1);
        map1.put(childOrderList[0].Id, ordItm);
        mapId.put(masterOrder.Id, map1);
        OC_OrderStatusHelper.getChildOrderStatus(mapId);
        OC_OrderStatusHelper.getDecomposedOrders(mapId);
        OC_OrderStatusHelper.getContractableOrderItems(mapId);
        Test.StopTest();
    }*/
    
    @isTest
    private static void updateOrderNCStatus() {
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User us = new User(Alias = 'standt', Email='applicationTocp1@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Application TOCP', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='applicationTocp1@testorg.com');
        System.runAs (us) {
        Account accObj = new Account(Name='TestAccount', Account_Status__c = 'Active');
        insert accObj;
        
        Product2 product4 = new Product2(Name='Collect Call Deny ',ProductCode='PROD-004', Description='This is a test JSON Product 1 Generator',isActive = true,orderMgmtId__c='9143863905013553513' ,OCOM_Bookable__c='Yes',Sellable__c = true);
       product4.vlocity_cmt__JSONAttribute__c  = '{"TELUSCHAR": [{"$$AttributeDefinitionEnd$$": null, "attributeRunTimeInfo": {"selectedItem":{"value":"Overline","displayText":"Overline","id":"9145085938413814481"}",values": [{"value": "Overline", "displayText": "Overline", "id": "9145085938413814481"}], "default": [{"value": "Overline", "displayText": "Overline", "id": "9145085938413814481"}], "uiDisplayType": "Dropdown", "dataType": "Picklist"}, "categorydisplaysequence__c": 1, "customconfiguitemplate__c": null, "attributecloneable__c": true, "valuedescription__c": null, "valuedatatype__c": "Picklist", "value__c": "Overline", "uidisplaytype__c": "Dropdown", "rulemessage__c": null, "isrequired__c": true, "id": "a7P220000008lnIEAQ", "querylabel__c": null, "isquerydriven__c": false, "isreadonly__c": true, "querycode__c": null, "objecttype__c": "Product2", "valueinnumber__c": null, "ishidden__c": true, "isconfigurable__c": true, "hasrule__c": false, "formatmask__c": null, "displaysequence__c": null, "attributedisplayname__c": "Line Type", "isactive__c": true, "attributefilterable__c": false, "attributedisplaysequence__c": "2", "attributeconfigurable__c": true, "attributeuniquecode__c": "ATTRIBUTE-549", "categoryname__c": "Characteristics", "categorycode__c": "TELUSCHAR", "attributecategoryid__c": "a7Q220000009vghEAA", "attributeid__c": "a7R220000000HqcEAE", "objectid__c": "01t22000000UXFUAA4", "$$AttributeDefinitionStart$$": null }] }';
        product4.IncludeQuoteContract__c= true;
       insert product4;

        
        Pricebook2 standardBook =  new Pricebook2(Id = Test.getStandardPricebookId(), Name = 'TestPricebook', IsActive = true); 
         PricebookEntry sbe4 = new PricebookEntry(Pricebook2Id = standardBook.Id,Product2Id = product4.Id, UnitPrice = 2, Vlocity_cmt__RecurringPrice__c = 17, IsActive = true, UseStandardPrice = false);
        insert sbe4;
        Pricebook2 customPriceBook =  new Pricebook2( Name = 'CustomPricebook', IsActive = true);
        insert customPriceBook;        
        PricebookEntry pbe4 = new PricebookEntry(Pricebook2Id = customPriceBook.Id,Product2Id = product4.Id, UnitPrice = 2, Vlocity_cmt__RecurringPrice__c = 17, IsActive = true, UseStandardPrice = false);
        insert pbe4;
        
        Order masterOrder=new Order();
        masterOrder.AccountId=accObj.Id;
        masterOrder.CurrencyIsoCode='CAD';
        masterOrder.EffectiveDate=Date.today();
        masterOrder.Name='MasterOrder';
        masterOrder.Status='Not Submitted';
        masterOrder.orderMgmtId_Status__c = 'Not Submitted';
        masterOrder.Pricebook2Id =  customPriceBook.ID;
        insert masterOrder;
        
        List<Order> childOrderList = new List<Order>();
        Order childOrder1=new Order();
        childOrder1.AccountId=accObj.Id;
        childOrder1.ParentId__c=masterOrder.Id;
        childOrder1.CurrencyIsoCode='CAD';
        childOrder1.EffectiveDate=Date.today();
        childOrder1.Name='ChildOrder1';
        childOrder1.Status='Not Submitted';
        childOrder1.Pricebook2Id =  customPriceBook.ID;
        childOrderList.add(childOrder1);
        
        Order childOrder2=new Order();
        childOrder2.AccountId=accObj.Id;
        childOrder2.ParentId__c=masterOrder.Id;
        childOrder2.CurrencyIsoCode='CAD';
        childOrder2.EffectiveDate=Date.today();
        childOrder2.Name='ChildOrder2';
        childOrder2.Status='Not Submitted';
        childOrder2.Pricebook2Id =  customPriceBook.ID;
        childOrderList.add(childOrder2);
        insert childOrderList;
            
        OrderItem  orderItem1 = new OrderItem(OrderId=childOrder1.Id, PricebookEntryId = pbe4.Id,vlocity_cmt__Product2Id__c=product4.Id,
                      vlocity_cmt__LineNumber__c = '0001', Quantity = 1, UnitPrice = 10, vlocity_cmt__ProvisioningStatus__c = 'New');
        orderItem1.vlocity_cmt__JSONAttribute__c  = '{"TELUSCHAR": [{"$$AttributeDefinitionEnd$$": null, "attributeRunTimeInfo": {"selectedItem": {"value": "Overline1", "displayText": "Overline1", "id": "9145085938413814481"}, ",values": [{"value": "Overline", "displayText": "Overline", "id": "9145085938413814481"}], "default": [{"value": "Overline", "displayText": "Overline", "id": "9145085938413814481"}], "uiDisplayType": "Dropdown", "dataType": "Picklist"}, "categorydisplaysequence__c": 1, "customconfiguitemplate__c": null, "attributecloneable__c": true, "valuedescription__c": null, "valuedatatype__c": "Picklist", "value__c": "Overline", "uidisplaytype__c": "Dropdown", "rulemessage__c": null, "isrequired__c": true, "id": "a7P220000008lnIEAQ", "querylabel__c": null, "isquerydriven__c": false, "isreadonly__c": true, "querycode__c": null, "objecttype__c": "Product2", "valueinnumber__c": null, "ishidden__c": true, "isconfigurable__c": true, "hasrule__c": false, "formatmask__c": null, "displaysequence__c": null, "attributedisplayname__c": "Line Type", "isactive__c": true, "attributefilterable__c": false, "attributedisplaysequence__c": "2", "attributeconfigurable__c": true, "attributeuniquecode__c": "ATTRIBUTE-549", "categoryname__c": "Characteristics", "categorycode__c": "TELUSCHAR", "attributecategoryid__c": "a7Q220000009vghEAA", "attributeid__c": "a7R220000000HqcEAE", "objectid__c": "01t22000000UXFUAA4", "$$AttributeDefinitionStart$$": null }] }';
        insert orderItem1;
            
        Test.StartTest();
        childOrder1.status='In Progress';
        childOrder1.orderMgmtId_Status__c=OrdrConstants.NC_ORDR_PROCESSING_STATUS;
        update childOrder1;
        Map<Id,Order> oldOrderMap = new Map<Id,Order>();
        oldOrderMap.put(childOrder1.Id,childOrder1);
        oldOrderMap.put(childOrder2.Id,childOrder2);
            
		OC_OrderStatusHelper.updateOrderStatus(true,false,true, false, childOrderList, oldOrderMap);
        Map<Id, Map<Id, List<OrderItem>>> mapId = new Map<Id, Map<Id, List<OrderItem>>>();
        Map<Id, List<OrderItem>> map1 = new Map<Id, List<OrderItem>>();
        List<OrderItem> ordItm = new List<OrderItem>();
        ordItm.add(orderItem1);
        map1.put(childOrderList[0].Id, ordItm);
        mapId.put(masterOrder.Id, map1);
        OC_OrderStatusHelper.getChildOrderStatus(mapId);
        OC_OrderStatusHelper.getDecomposedOrders(mapId);
        OC_OrderStatusHelper.getContractableOrderItems(mapId);
        Test.StopTest();
   
        }
        
    } 
    @isTest
    private static void method1(){
        //Test.startTest();
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        Test.startTest();
        List<Order> childList=[select id,ordermgmtid__c from order where parentid__c=:orderId];
        for(Order ord:childList){
            ord.ordermgmtid__c='12345';
        }
        update childList;
        Map<Id, Map<Id, List<OrderItem>>> masterOrderMap=new  Map<Id, Map<Id, List<OrderItem>>>();
        Map<Id, List<OrderItem>> childOrdMap=new Map<Id, List<OrderItem>>();
        List<OrderItem> oiList=[select id,orderMgmtId_Status__c,orderid,Term__c from orderitem where order.parentid__c=:orderId];
        for(OrderItem oi:oiList){
            oi.orderMgmtId_Status__c='Processing';
            oi.Term__c='24 months';
            
        }
        update oiList;
        
        oiList=[select id,orderMgmtId_Status__c,orderid,Order.ParentId__c,Term__c from orderitem where order.parentid__c=:orderId];
        /* for(OrderItem oi:oiList){
             System.debug('debugging Term='+oi.Term__c);
             if(childOrdMap.containsKey(oi.orderid)){
                 List<OrderItem> fetchList=childOrdMap.get(oi.orderid);
                 fetchList.add(oi);
                 childOrdMap.put(oi.orderid,fetchList);
             } else {
                 List<OrderItem> nnewList=new List<OrderItem>();
                  nnewList.add(oi);            
                 childOrdMap.put(oi.orderid,nnewList);                 
             }            
            
        }*/
        childOrdMap.put(oiList.get(0).orderid,oiList);
        masterOrderMap.put(orderId,childOrdMap);
        OC_OrderStatusHelper.getDecomposedOrders(masterOrderMap);
        OC_OrderStatusHelper.MasterStatusOrder ob1=new OC_OrderStatusHelper.MasterStatusOrder();
        OC_OrderStatusHelper.MasterStatusOrder ob2=new OC_OrderStatusHelper.MasterStatusOrder('test',1);
        ob2.compareto(ob1);
        Test.stopTest();
    }
    

}