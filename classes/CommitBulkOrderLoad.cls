// Commit all Bulk Order Load Records into Service Request and creating Service
// Address if required.  This will only commit the ones that are either not
// committed or commited fail.  This job will NOT submit Service Request.
public class CommitBulkOrderLoad implements queueable,database.allowscallouts{
    id bulkOrderLoadId{get;set;}
    string sessionId{get;set;}
    list<id> bulkOrderLoadRecordIds{get;set;}
    list<bulk_order_load_record__c> bulkOrderLoadRecordList{get;set;}
    list<service_request__c> serviceRequests{get;set;}
    integer commitPerJob{get;set;}
    //TODO implement maximum retry variable
    void init(string sessionId){
        commitPerJob=integer.valueof(label.Commit_Job_Size);
        this.sessionId=sessionId;
    }
    public CommitBulkOrderLoad(string sessionId,id bulkOrderLoadId){
        this.bulkOrderLoadId=bulkOrderLoadId;
        init(sessionId);
    }
    public CommitBulkOrderLoad(string sessionId,list<id> bulkOrderLoadRecordIds){
        this.bulkOrderLoadRecordIds=bulkOrderLoadRecordIds;
        init(sessionId);
    }
    public CommitBulkOrderLoad(list<bulk_order_load_record__c> bulkOrderLoadRecordList){
        this.bulkOrderLoadRecordList=bulkOrderLoadRecordList;
        init('');
    }
    //TODO Currently it's assumed that one job can create all SRs but if later
    //the total number of SRs to be created will have a risk of hitting
    //governor limit, it requires callout for a job distributor.
    //
    // It rolls back if anything failed so no phantom record created
    public void execute(queueablecontext context){
System.debug('Bulkload: CommitBulkOrderLoad: ' + bulkOrderLoadRecordList);        
        string query='';
        list<schema.fieldsetmember> srCreationQueryFieldSetMembers=schema.sobjecttype.bulk_order_load_record__c.fieldsets.getmap().get('SR_Creation_Query').getfields();
        list<string> fieldnames=new list<string>();
        for(schema.fieldsetmember member:srCreationQueryFieldSetMembers){
            fieldnames.add(member.getfieldpath());
        }
        /*if(bulkOrderLoadRecordIds!=null&&!bulkOrderLoadRecordIds.isempty()){
            query='select '+string.join(fieldnames,',')+',bulk_order_load_file__r.opportunity__c,bulk_order_load_file__r.opportunity__r.accountid from bulk_order_load_record__c where id=:bulkOrderLoadRecordIds and status__c=\'Ready to Create SR\' limit 50000';
        }else */
        if((bulkOrderLoadRecordList==null||bulkOrderLoadRecordList.isempty())&&string.isnotblank(bulkOrderLoadId)){
            query='select '+string.join(fieldnames,',')+',bulk_order_load_file__r.opportunity__c,bulk_order_load_file__r.opportunity__r.accountid from bulk_order_load_record__c where bulk_order_load_file__c=:bulkOrderLoadId and status__c=\''+label.Ready_to_Create_SR+'\' limit 50000';
            bulkOrderLoadRecordList=(list<bulk_order_load_record__c>)database.query(query);
        }
        if(bulkOrderLoadRecordList!=null&&!bulkOrderLoadRecordList.isempty()){
            list<bulk_order_load_record__c> partialBulkOrderLoadRecordList=new list<bulk_order_load_record__c>();
            integer index=0;
            for(bulk_order_load_record__c bulkOrderLoadRecord:bulkOrderLoadRecordList){
                if(index<commitPerJob){
                    partialBulkOrderLoadRecordList.add(bulkOrderLoadRecord);
                    index++;
                }else{
                    break;
                }
            }
            boolean isProcessed=processNext(partialBulkOrderLoadRecordList);
            if(isProcessed){
                integer numPartial=partialBulkOrderLoadRecordList.size();
                for(integer i=0;i<numPartial;i++){
                    bulkOrderLoadRecordList.remove(0);
                }
            }
            //TODO make sure a limited retry machanism is in place before submitting another job other it will chain forever
            if(!bulkOrderLoadRecordList.isempty()){
                system.enqueuejob(new CommitBulkOrderLoad(bulkOrderLoadRecordList));
            }
        }
    }
    boolean processNext(list<bulk_order_load_record__c> bulkOrderLoadRecords){
System.debug('Bulkload: processNext: ' + bulkOrderLoadRecords);        
        boolean isProcessed=false;
        integer numBulkOrderLoadRecords=bulkOrderLoadRecords.size();
        Savepoint sp = Database.setSavepoint();
        id accountId=null;
        DataMappingUtil dataMappingUtil=new DataMappingUtil();
        list<service_request__c> newServiceRequests=new list<service_request__c>();
        list<smbcare_address__c> upsertSMBCareAddresses=new list<smbcare_address__c>();
        list<srs_service_address__c> newServiceAddresses=new list<srs_service_address__c>();
        list<service_request_contact__c> newServiceRequestContacts=new list<service_request_contact__c>();
        map<string,integer> fmsIdAddressIdMap=new map<string,integer>();
        map<id,string> contactNameMap=new map<id,string>();
        map<id,string> contactEmailMap=new map<id,string>();
        map<id,string> contactPhoneMap=new map<id,string>();
        if(numBulkOrderLoadRecords>0){
            accountId=bulkOrderLoadRecords[0].bulk_order_load_file__r.opportunity__r.accountid;
        }
        for(integer i=0;i<numBulkOrderLoadRecords;i++){
            if(string.isnotblank(bulkOrderLoadRecords[i].fms_address_id__c)){
                fmsIdAddressIdMap.put(bulkOrderLoadRecords[i].fms_address_id__c,i);
            }
            if(string.isnotblank(bulkOrderLoadRecords[i].contact_name__c)){
                contactNameMap.put(bulkOrderLoadRecords[i].id,bulkOrderLoadRecords[i].contact_name__c);
            }
            if(string.isnotblank(bulkOrderLoadRecords[i].contact_email__c)){
                contactEmailMap.put(bulkOrderLoadRecords[i].id,bulkOrderLoadRecords[i].contact_email__c);
            }
            if(string.isnotblank(bulkOrderLoadRecords[i].contact_phone__c)){
                contactPhoneMap.put(bulkOrderLoadRecords[i].id,bulkOrderLoadRecords[i].contact_phone__c);
            }
        }
        string errorMessage='';
        try{
            list<smbcare_address__c> smbcareAddresses=[select id,Address_Type__c,Suite_Number__c,City__c,Status__c,
                                                       Street_Number__c,Province__c,Street_Name__c,Postal_Code__c,Street_Direction__c,
                                                       Google_Map_Link__c,Lot__c,Block__c,Plan__c,SRS_Quarter__c,Section__c,Township__c,
                                                       Range__c,Meridian__c,Subdivision_ID__c,SRS_Type__c,Number__c,Address__c,state__c,SRS_State__c,
                                                       GPS_Coordinates__c,UTM__c,Country__c from smbcare_address__c where fms_address_id__c=:fmsIdAddressIdMap.keyset() and recordtype.name=:label.Address limit 50000];
            map<string,integer> smbcareAddressIndexMap=new map<string,integer>();
            integer numSMBCareAddresses=smbcareAddresses.size();
            for(integer i=0;i<numSMBCareAddresses;i++){
                smbcareAddressIndexMap.put(smbcareAddresses[i].fms_address_id__c,i);
            }
            list<contact> contacts=[select id,firstname,lastname,email,phone from contact where accountid=:accountId and name=:contactNameMap.values() limit 50000];
            smbcare_address__c smbcareAddress=null;
            for(bulk_order_load_record__c bulkOrderLoadRecord:bulkOrderLoadRecords){
                if(string.isnotblank(bulkOrderLoadRecord.fms_address_id__c)){
                    if(smbcareAddressIndexMap.containskey(bulkOrderLoadRecord.fms_address_id__c)){
                        smbcareAddress=smbcareAddresses.get(smbcareAddressIndexMap.get(bulkOrderLoadRecord.fms_address_id__c));
                    }else{
                        smbcareAddress=new smbcare_address__c();
                    }
                }else{
                    smbcareAddress=new smbcare_address__c();
                }
                smbcareAddress=new smbcare_address__c();

                smbcareAddress.account__c=accountId;
                smbcareAddress=dataMappingUtil.mapBulkOrderLoadToSMBCareAddress(bulkOrderLoadRecord,smbcareAddress);
                upsertSMBCareAddresses.add(smbcareAddress);
                service_request__c serviceRequest=dataMappingUtil.createServiceRequest(bulkOrderLoadRecord);
                newServiceRequests.add(serviceRequest);
            }
            insert newServiceRequests;
            upsert upsertSMBCareAddresses;
            integer numServiceRequests=newServiceRequests.size();
            for(integer i=0;i<numServiceRequests;i++){
                bulkOrderLoadRecords[i].put('Service_Request__c',newServiceRequests[i].id);
                bulkOrderLoadRecords[i].put('Status__c',label.SR_Created);
                srs_service_address__c newServiceAddress=dataMappingUtil.createServiceAddress(bulkOrderLoadRecords[i],newServiceRequests[i],upsertSMBCareAddresses[i]);
                newServiceAddresses.add(newServiceAddress);
                service_request_contact__c serviceRequestContact=dataMappingUtil.createServiceRequestContact(bulkOrderLoadRecords[i],newServiceRequests[i],contacts);
                newServiceRequestContacts.add(serviceRequestContact);
            }
System.debug('Bulkload: newServiceAddresses: ' + newServiceAddresses);        
            insert newServiceAddresses;
            insert newServiceRequestContacts;
            update bulkOrderLoadRecords;
        }catch(queryexception qe){
            database.rollback(sp);
            errorMessage=qe.getmessage();
            // No SMBCare Address Found, email system error for now
        }catch(dmlexception dmle){
            database.rollback(sp);
            errorMessage=dmle.getmessage();
            // Wait until next update from PAC is okay but must email CRM Ops to investigate this issue
        }catch(exception e){
            database.rollback(sp);
            errorMessage=e.getmessage();
        }finally{
            //TODO implement retry machanism to make it retry at least couple times before calling an exist
            if(string.isnotblank(errorMessage)){
                for(bulk_order_load_record__c bulkOrderLoadRecord:bulkOrderLoadRecords){
    				bulkOrderLoadRecord.put('System_Message__c', errorMessage);
    				bulkOrderLoadRecord.put('Status__c',label.SR_Creation_Failed);
                    bulkOrderLoadRecord.put('Service_Request__c',null);
                    //TODO Temporary fix, this should be removed to use a better retry machanism
                    isProcessed=true;
                }
                try{
                    update bulkOrderLoadRecords;
                }catch(exception updateEx){
                    Util.emailSystemError(updateEx);
                }
            }else{
                isProcessed=true;
            }
        }
        return isProcessed;
    }/*
    void requestDistribution(){
        system.debug('session id:'+sessionId);
        // Pass one big call instead of multiple, this will minimize the redundant network consumption
        string body='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bul="http://soap.sforce.com/schemas/class/BulkLoadMaster">'+
            '<soapenv:Header><bul:SessionHeader><bul:sessionId>'+sessionId+'</bul:sessionId></bul:SessionHeader></soapenv:Header><soapenv:Body>';
        body+='<bul:distributeCommit>';
        for(id bulkOrderLoadRecordId:this.bulkOrderLoadRecordIds){
            body+='<bul:bulkOrderLoadRecordIds>'+bulkOrderLoadRecordId+'</bul:bulkOrderLoadRecordIds>';
        }
        body+='</bul:distributeCommit></soapenv:Body></soapenv:Envelope>';
        http http=new http();
        httprequest req=new httprequest();
        req.setendpoint(url.getsalesforcebaseurl().getprotocol()+'://'+url.getsalesforcebaseurl().gethost()+'/services/Soap/class/BulkLoadMaster');
        req.setmethod('POST');
        req.setheader('Content-Type','text/xml');
        req.setheader('SOAPAction','""');
        req.setBody(body);
        httpresponse res=http.send(req);
        system.debug('Total records queried:'+bulkOrderLoadRecordIds.size()+', Total SOQL Used:'+limits.getQueries()+', total callouts:'+limits.getCallouts());
        system.debug('response:'+res.getBody());
    }*/
    class DataMappingUtil{
        list<schema.fieldsetmember> commitBulkOrderAddressMembers{get;set;}
        list<schema.fieldsetmember> commitAddressBulkOrderMembers{get;set;}
        list<schema.fieldsetmember> commitServiceRequestBulkOrderMembers{get;set;}
        list<schema.fieldsetmember> commitBulkOrderServiceRequestMembers{get;set;}
        list<schema.fieldsetmember> commitServiceAddressBulkOrderMembers{get;set;}
        list<schema.fieldsetmember> commitBulkOrderServiceAddressMembers{get;set;}
        map<string,schema.recordtypeinfo> serviceRequestRecordTypeMap{get;set;}
        integer numCommitServiceRequestBulkOrderMembers{get;set;}
        integer numCommitAddressBulkOrderMembers{get;set;}
        integer numCommitServiceAddressBulkOrderMembers{get;set;}
        public DataMappingUtil(){
            map<string,schema.fieldset> bulkOrderLoadRecordFieldsetMap=schema.sobjecttype.bulk_order_load_record__c.fieldsets.getmap();
            commitBulkOrderAddressMembers=bulkOrderLoadRecordFieldsetMap.get('Commit_SMBCare_Address').getfields();
            commitBulkOrderServiceRequestMembers=bulkOrderLoadRecordFieldsetMap.get('Commit_Service_Request').getfields();
            commitBulkOrderServiceAddressMembers=bulkOrderLoadRecordFieldsetMap.get('Commit_SRS_Service_Address').getfields();
            commitServiceRequestBulkOrderMembers=schema.sobjecttype.service_request__c.fieldsets.getmap().get('Commit_Bulk_Order_Load_Records').getfields();
            commitAddressBulkOrderMembers=schema.sobjecttype.smbcare_address__c.fieldsets.getmap().get('Commit_Bulk_Order_Load_Records').getfields();
            commitServiceAddressBulkOrderMembers=schema.sobjecttype.srs_service_address__c.fieldsets.getmap().get('Commit_Bulk_Order_Load_Records').getfields();
            serviceRequestRecordTypeMap=schema.sobjecttype.service_request__c.getrecordtypeinfosbyname();
            numCommitServiceRequestBulkOrderMembers=commitServiceRequestBulkOrderMembers.size();
            numCommitAddressBulkOrderMembers=commitAddressBulkOrderMembers.size();
            numCommitServiceAddressBulkOrderMembers=commitServiceAddressBulkOrderMembers.size();
        }
        public service_request__c createServiceRequest(bulk_order_load_record__c bulkOrderLoadRecord){
            string serviceRequestFieldPath='';
            service_request__c newServiceRequest=new service_request__c(opportunity__c=bulkOrderLoadRecord.bulk_order_load_file__r.opportunity__c);
            newServiceRequest.recordtypeid=serviceRequestRecordTypeMap.get(bulkOrderLoadRecord.order_type__c).getrecordtypeid();
            newServiceRequest.account_name__c=bulkOrderLoadRecord.bulk_order_load_file__r.opportunity__r.accountid;
            for(integer i=0;i<numCommitServiceRequestBulkOrderMembers;i++){
                serviceRequestFieldPath=commitServiceRequestBulkOrderMembers[i].getfieldpath();
                if(commitServiceRequestBulkOrderMembers[i].getrequired()){
                    schema.fieldsetmember commitBulkOrderServiceRequestMember=commitBulkOrderServiceRequestMembers.get(i);
                    newServiceRequest.put(serviceRequestFieldPath,bulkOrderLoadRecord.get(commitBulkOrderServiceRequestMember.getfieldpath()));
                }
            }
            return newServiceRequest;
        }
        public smbcare_address__c mapBulkOrderLoadToSMBCareAddress(bulk_order_load_record__c bulkOrderLoadRecord,smbcare_address__c smbcareAddress){
            string addressFieldPath='';
            for(integer i=0;i<numCommitAddressBulkOrderMembers;i++){
                addressFieldPath=commitAddressBulkOrderMembers[i].getfieldpath();
                if(commitAddressBulkOrderMembers[i].getrequired()){
                    schema.fieldsetmember commitBulkOrderServiceRequestMember=commitBulkOrderAddressMembers.get(i);
                    smbcareAddress.put(addressFieldPath,bulkOrderLoadRecord.get(commitBulkOrderServiceRequestMember.getfieldpath()));
                }
            }
            return smbcareAddress;
        }
        //TODO create data mapping
        public srs_service_address__c createServiceAddress(bulk_order_load_record__c bulkOrderLoadRecord,service_request__c newServiceRequest,smbcare_address__c address){ 
            srs_service_address__c newServiceAddress=new srs_service_address__c(service_request__c=newServiceRequest.id,address__c=address.id);
            string serviceAddressFieldPath='';
            for(integer i=0;i<numCommitServiceAddressBulkOrderMembers;i++){
                serviceAddressFieldPath=commitServiceAddressBulkOrderMembers[i].getfieldpath();
                if(commitServiceAddressBulkOrderMembers[i].getrequired()){
                    schema.fieldsetmember commitBulkOrderServiceAddressmember=commitBulkOrderServiceAddressMembers.get(i);
                    newServiceAddress.put(serviceAddressFieldPath,bulkOrderLoadRecord.get(commitBulkOrderServiceAddressmember.getfieldpath()));
                }
            }
            return newServiceAddress;
        }
        //TODO See questions in TSD
        public service_request_contact__c createServiceRequestContact(bulk_order_load_record__c bulkOrderLoadRecord,service_request__c newServiceRequest,list<contact> contacts){
            service_request_contact__c newServiceRequestContact=new service_request_contact__c(service_request__c=newServiceRequest.id,
                                                                                               non_crm_contact_name__c=bulkOrderLoadRecord.contact_name__c,
                                                                                              non_crm_contact_email__c=bulkOrderLoadRecord.contact_email__c,
                                                                                              non_crm_contact_phone__c=bulkOrderLoadRecord.contact_phone__c,
                                                                                              contact_type__c=label.Customer_Onsite);
            return newServiceRequestContact;
        }
    }
}