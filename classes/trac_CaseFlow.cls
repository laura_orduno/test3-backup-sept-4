/**
*trac_CaseTriggerDispatcher
*@description Class that extends Dispatcher class and will send a Case, along with any case comments and related devices, to 
*             Ring Central via salesforce to salesforce.
*@authoer David Barrera
*@Date 8/28/14
@Date  8/12/15 Kelly
*/
global class trac_CaseFlow implements Process.Plugin {
    
    global Process.PluginResult invoke(Process.PluginRequest request){

        ID aCaseId = (Id) request.inputParameters.get('caseId');
        Case aCase = [
            SELECT Id
            FROM Case
            WHERE Id = :aCaseId 
        ];
        if(aCase != null){
            sendCase(aCase);
        }
        Map<String,Object> result = new Map<String,Object>(); 
        return new Process.PluginResult(result);

    }

    global Process.PluginDescribeResult describe(){
        Process.PluginDescribeResult result = new Process.PluginDescribeResult(); 
        result.Name = 'flowS2Splugin';        
        result.inputParameters = new 
           List<Process.PluginDescribeResult.InputParameter>{ 
               new Process.PluginDescribeResult.InputParameter('caseId', 
               Process.PluginDescribeResult.ParameterType.Id, true) 
            }; 
        result.outputParameters = new 
           List<Process.PluginDescribeResult.OutputParameter>{ }; 
        return result; 
    }

    public void sendCase(Case aCase) {

        Set<Id> caseWithPNRC = new Set<Id>();

        List<PartnerNetworkRecordConnection> pnrcList = new List<PartnerNetworkRecordConnection>();
        List<Device__c> localDevices = new List<Device__c>();
        List<Vendor_Communication__c> vCtoCaseComment = new List<Vendor_Communication__c>();
        //added attachment 20150805
        List<Attachment> caseAttachments = new List<Attachment>();
        Map<Id,String> relatedrecords = new Map<Id,String>();

        pnrcList =[ SELECT id,status,LocalRecordId,StartDate,PartnerRecordId
                    FROM PartnerNetworkRecordConnection
                    WHERE LocalRecordId = :aCase.Id
                    AND Status != 'Inactive'];
       
       for(PartnerNetworkRecordConnection pnrc: pnrcList){
            caseWithPNRC.add(pnrc.LocalRecordId);
        }
        if ( !caseWithPNRC.contains(aCase.Id)){

            localDevices = [
                SELECT VoIP_Case__c, Id,ConnectionReceivedId, Parent_Id_At_Source__c ,OwnerId
                FROM Device__c 
                WHERE VoIP_Case__c = :aCase.Id
                AND VoIP_Case__c <> null
                AND ConnectionReceivedId = null 
            ];

            vCtoCaseComment = [
                SELECT Id,Case__c,OwnerId
                FROM Vendor_Communication__c            
                WHERE Case__c <> null
                AND Case__c = :aCase.ID
            ];
        
			//added query attachments from case
			caseAttachments = [
                SELECT Id,ParentId, Name, Body, ContentType, Description 
                FROM Attachment 
                WHERE ParentId = :aCase.ID
            ];
            
             for(Vendor_Communication__c ven : vCtoCaseComment){
                if(relatedrecords.containsKey(ven.Case__c)){

                    String tmp = relatedrecords.get(ven.Case__c);
                    tmp = tmp + ',Vendor_Communication__c';
                    relatedrecords.put(ven.Case__c, tmp);
                }
                else{
                    relatedrecords.put(ven.Case__c, 'Vendor_Communication__c');
                }
            }

            for(Device__c dev:localDevices){

                if(relatedrecords.containsKey(dev.VoIP_Case__c)){
                    String tmp = relatedrecords.get(dev.VoIP_Case__c);
                    tmp = tmp + ',Device__c';
                    relatedrecords.put(dev.VoIP_Case__c, tmp);
                }
                else{
                    relatedrecords.put(dev.VoIP_Case__c, 'Device__c');
                }
            }
            
            //added attachments from case to relatedrecords
            for(Attachment att:caseAttachments){

                if(relatedrecords.containsKey(att.ParentId)){
                    String tmp = relatedrecords.get(att.ParentId);
                    tmp = tmp + ',Attachment';
                    relatedrecords.put(att.ParentId, tmp);                    
                }
                else{
                    relatedrecords.put(att.ParentId, 'Attachment');
                }
            }

            
            System.Debug('Callig sendToRingCentral with related records: ' + relatedrecords);
            List<Case> localCase = new List<Case>();
            localCase.add(aCase);
            trac_S2S_connection_Helper.sendToRingCentral(localCase , null, relatedrecords, 'RingCentral',true,true,true);  
            trac_CaseTriggerDispatcher.getVendorCaseId(localCase);     
        }               
    }
}