public without sharing class OrderItem_PAC_Address_Controller {
public boolean showOrhide {get;set;}

public AddressData PAData  { get; set; }
public AddressData PAData1  { get; set; }
ApexPages.StandardController stdCtrl;
Id OliId { get; set; }
Id Oid { get; set; }
public string searchString{get;set;} 
public List<Account> results{get;set;} // search results
public String selectedAccId{get;set;}
Order ord;
//public Order order {get; set;}
//public ID orderId {get;set;}   
      
   public OrderItem_PAC_Address_Controller(ApexPages.StandardController controller){
       stdCtrl = controller;
        //this.order = (Order)stdCtrl.getRecord();
        //orderId = order.Id;
        PAData = new AddressData();
        PAData1 = new AddressData();
        showOrhide=false;
        //PAData.isCityRequired = true;
        //PAData1.isCityRequired = true;
        Oid =  ApexPages.currentPage().getParameters().get('id');
        OliId =  ApexPages.currentPage().getParameters().get('contextId');
        system.debug('@@@@ID'+OliId);
        results = performSearch('');
   }
   
    Public Pagereference SaveShippingOli()
    {
    // OliId =  ApexPages.currentPage().getParameters().get('contextId');
    system.debug('#### Review Cart Order Line Item ID####'+OliId);
    
    String oiSAddr = ' '+(String)PAData.addressLine1;
    oiSAddr = oiSAddr +' '+(String)PAData.city; 
    oiSAddr = oiSAddr +' '+(String)PAData.provinceName;
    oiSAddr = oiSAddr +' '+(String)PAData.postalCode; 
    oiSAddr = oiSAddr +' '+(String)PAData.country;
    system.debug('#### Review Cart Order Line Item ID 222####'+OliId);
    OrderItem oisupdate=[select id, OrderId, Shipping_Address__c,Shipping_Street__c, Shipping_City__c, Shipping_State__c, Shipping_Zip__c,Shipping_Country__c from OrderItem where id=:OliId]; //PriceBookEntry.Product2.Name
   // String PrdtNam = oisupdate.PriceBookEntry.Product2.Name;
   if(PAData.street!=null ||PAData.street!='')
       oisupdate.Shipping_Street__c=PAData.street;    
   else 
       oisupdate.Shipping_Street__c=PAData.addressLine1;   
    oisupdate.Shipping_City__c=PAData.city;
    if(PAData.state!=null ||PAData.state!='')
        oisupdate.Shipping_State__c=PAData.state;  
    else
        oisupdate.Shipping_State__c=PAData.provinceName;  
    oisupdate.Shipping_Zip__c=PAData.postalCode;
    oisupdate.Shipping_Country__c=PAData.country;
  //oisupdate.Shipping_same_as_Service__c=False; 
  //  If(PrdtNam=='Smart Hub')
  //  {   showPanel();
        if(oiSAddr.length() > 8)
        oisupdate.Shipping_Address__c=oiSAddr;
        try{
            update oisupdate;       
            system.debug('@@@@india Shipping'+oisupdate);
            Pagereference pref= new pagereference('/apex/OrderItems_PAC_Address?id='+oisupdate.id);                         
            return pref; 
        }
        catch(exception e)
        {
            return null;
        } 
   /*  }
     else 
     {
         return null;  
     }    */
  } 
  
  
   Public Pagereference SaveBillingOli()
    {
        String oiBAddr = ' '+(String)PAData1.addressLine1+ ' ' +(String)PAData1.city+ ' ' +(String)PAData1.provinceName+ ' '+(String)PAData1.postalCode+' ' +(String)PAData1.country;
        OrderItem oibupdate=[select id, OrderId, Billing_Address__c from OrderItem where id=:OliId];      
        /*oibupdate.BillingCity=PAData1.city;
        oibupdate.BillingState=PAData1.state;
        oibupdate.BillingPostalCode=PAData1.postalCode;
        oibupdate.BillingCountry=PAData1.country; */
        //oupdate.Shipping_same_as_Service__c=False; 
        oibupdate.Billing_Address__c=oiBAddr; 
        System.debug('oibupdate####'+oibupdate);
        try{
            update oibupdate;       
            Pagereference pref1= new pagereference('/apex/OrderItem_PAC_Account?id='+oibupdate.id);                         
            return pref1; 
            }
        catch(exception e)
        {
            return null;
        } 
    } 
    
    
    Public Pagereference SaveBillingOlionSelect()
    {
        OrderItem OrderItemToUpdate;
        OrderItemToUpdate=[select id, OrderId, Billing_Account__c, Billing_Address__c from OrderItem where id=:OliId];
        List<Account> accountList = [SELECT Id, Billing_System__c, Billingstreet, BAN_CAN__c, Name FROM Account WHERE id=:selectedAccId];
        OrderItemToUpdate.Billing_Address__c=accountList[0].Billingstreet;
        //OrderItemToUpdate.Billing_Account__c = accountList[0].Billing_System__c;
        OrderItemToUpdate.Billing_Account__c=accountList[0].BAN_CAN__c;
        try{
        System.debug('OrderItemToUpdate###'+OrderItemToUpdate);
            update OrderItemToUpdate;       
            Pagereference pref1= new pagereference('/apex/OrderItem_PAC_Account?id='+OrderItemToUpdate.id);                         
            return pref1; 
            }
            
        catch(exception e)
        {
            return null;
        } 
      }   
    
    
      
   private void runSearch() {
    // TODO prepare query string for complex serarches & prevent injections
    results= performSearch(searchString);               
  } 

  private List<Account> performSearch(string searchString) {
  
       OrderItem oibupdate=[select id, Order.AccountId ,Billing_Account__c, Billing_Address__c from OrderItem where id=:OliId];  
       Id AccId =   oibupdate.Order.AccountId;
       List<Account> accountList = Database.query('SELECT Id, Account.Ban_Can__c, Account.Billing_System__c,Account.Billingstreet,Account.name FROM Account WHERE (Name Like : searchString OR Name Like \'%'+searchString +'%\') and parentid=:AccId and (Billing_Account_Active_Indicator__c=\'Y\' or Billing_Account_Active_Indicator__c=\'\')');        
       return accountList;  

  }
  
    // used by the visualforce page to send the link to the right dom element
  public string getFormTag() {
    return System.currentPageReference().getParameters().get('frm');
  }
  
    // used by the visualforce page to send the link to the right dom element for the text box
  public string getTextBox() {
    return System.currentPageReference().getParameters().get('txt');
  }
   // performs the keyword search
  public PageReference search() {
    runSearch();
    return null;
  }
  
          
}