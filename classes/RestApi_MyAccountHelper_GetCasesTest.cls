@isTest
public class RestApi_MyAccountHelper_GetCasesTest {

	private static User u;
	private static Account a;
	private static Account parentAcc;
	private static Case c;
	private static Account porAcc;
	private static Contact_Linked_Billing_Account__c ba;
	private static ExternalToInternal__c ei;
	private static Segment__c segment;
	private static RestApi_MyAccountHelper_GetCases getCaseObj;
	private static RestApi_MyAccountHelper_GetCases.RequestUser ru;
	private static List<String> filters = new List<String>{
			RestApi_MyAccountHelper_TestUtils.EXTERNALSTATUS
	};
	private static List<String> billingAccounts = new List<String>{
			RestApi_MyAccountHelper_TestUtils.BILLINGACCOUNTNUMBER
	};
	private static RestApi_MyAccountHelper_GetCases.GetCasesREsponse resp;
	private static RestApi_MyAccountHelper.Response intresp;

	static {
		trac_TriggerHandlerBase.blockTrigger = true;
		User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
		System.RunAs(thisUser) {
			ei = RestApi_MyAccountHelper_TestUtils.createEtoI();
			insert ei;
		}

		segment = RestApi_MyAccountHelper_TestUtils.createSegment();
		insert segment;

		parentAcc = new Account(
				Name = 'Account Inc.',
				sub_Segment_Code__c = segment.id,
				RecordTypeId = RestApi_MyAccountHelper_Utility.RT_RCID);
		insert parentAcc;

		a = new Account(Name = 'Account Inc.',
				ParentId = parentAcc.Id,
				BAN_CAN__c = RestApi_MyAccountHelper_TestUtils.BILLINGACCOUNTNUMBER,
				RecordTypeId = PortalConstants.ACCOUNT_RECORDTYPE_BAN);
		insert a;

		porAcc = new Account(Name = 'Portal Account Inc.', sub_Segment_Code__c = segment.Id);
		insert porAcc;

		u = RestApi_MyAccountHelper_TestUtils.createPortalUser(porAcc);
		insert u;

		c = RestApi_MyAccountHelper_TestUtils.createCase(a, u);
		c.Subject = 'test';
		insert c;

		ba = new Contact_Linked_Billing_Account__c(Billing_Account__c = a.id, Contact__C = u.ContactId);
		insert ba;

		ru = RestApi_MyAccountHelper_TestUtils.createRequestUser();
		trac_TriggerHandlerBase.blockTrigger = false;
	}

	// Proper Test class with that executes as needed
	@isTest
	private static void testCompleteRequest() {
		ru.uuid = RestApi_MyAccountHelper_TestUtils.UUID;
		createRequest(ru);

		Test.startTest();

		getCaseObj = new RestApi_MyAccountHelper_GetCases(RestContext.request.requestBody.toString());
		resp = getCaseObj.response;

		Test.stopTest();

	}

	// Proper Test class with that executes as needed
	@isTest
	private static void testCompleteRequestNoUUID() {

		ru.uuid = RestApi_MyAccountHelper_TestUtils.UUIDNOT;
		createRequest(ru);

		Test.startTest();

		getCaseObj = new RestApi_MyAccountHelper_GetCases(RestContext.request.requestBody.toString());
		resp = getCaseObj.response;

		Test.stopTest();

	}

	// Proper Test class with that executes as needed
	@isTest
	private static void testCompleteRequestNullSBS() {
		porAcc.Sub_Segment_Code__c = null;
		update porAcc;

		ru.uuid = RestApi_MyAccountHelper_TestUtils.UUID;
		createRequest(ru);

		Test.startTest();

		getCaseObj = new RestApi_MyAccountHelper_GetCases(RestContext.request.requestBody.toString());
		resp = getCaseObj.response;

		Test.stopTest();

	}

	// Proper Test class with that executes as needed
	@isTest
	private static void testCompleteRequestNoUUIDNullSBS() {
		parentAcc.Sub_Segment_Code__c = null;
		parentAcc.RecordTypeId = RestApi_MyAccountHelper_GetCases.RT_RCID;
		update parentAcc;

		ru.uuid = RestApi_MyAccountHelper_TestUtils.UUIDNOT;
		createRequest(ru);

		Test.startTest();

		getCaseObj = new RestApi_MyAccountHelper_GetCases(RestContext.request.requestBody.toString());
		resp = getCaseObj.response;

		Test.stopTest();

	}

	// Proper Test class with that executes as needed
	@isTest
	private static void testCompleteRequestNotSBS() {
		segment.cust_Segment_A__c = 'notSBS';
		update segment;

		ru.uuid = RestApi_MyAccountHelper_TestUtils.UUID;
		createRequest(ru);

		Test.startTest();

		getCaseObj = new RestApi_MyAccountHelper_GetCases(RestContext.request.requestBody.toString());
		resp = getCaseObj.response;

		Test.stopTest();

	}

	// Proper Test class with that executes as needed
	@isTest
	private static void testCompleteRequestNoUUIDNotSBS() {
		segment.cust_Segment_A__c = 'notSBS';
		update segment;

		ru.uuid = RestApi_MyAccountHelper_TestUtils.UUIDNOT;
		createRequest(ru);

		Test.startTest();

		getCaseObj = new RestApi_MyAccountHelper_GetCases(RestContext.request.requestBody.toString());
		resp = getCaseObj.response;

		Test.stopTest();

	}

	// Proper Test class with that executes as needed
	@isTest
	private static void testNullUUIDRequest() {

		ru.uuid = null;
		createRequest(ru);

		Test.startTest();

		try {
			getCaseObj = new RestApi_MyAccountHelper_GetCases(RestContext.request.requestBody.toString());
			resp = getCaseObj.response;
			System.assert(false, 'Exepected an xException to be thrown');
		} catch (xException ex) {
			System.assert(true);
		} catch (Exception e) {
			System.assert(false, 'Exepected an xException to be thrown');
		}

		Test.stopTest();

	}

	// Proper Test class with that executes as needed
	@isTest
	private static void testNoUserRequest() {

		ru.uuid = RestApi_MyAccountHelper_TestUtils.UUIDNOT;
		createRequest(null);

		Test.startTest();

		try {
			getCaseObj = new RestApi_MyAccountHelper_GetCases(RestContext.request.requestBody.toString());
			resp = getCaseObj.response;
			System.assert(false, 'Exepected an xException to be thrown');
		} catch (xException ex) {
			System.assert(true);
		} catch (Exception e) {
			System.assert(false, 'Exepected an xException to be thrown');
		}

		Test.stopTest();

	}

	@IsTest
	private static void testSetFailureVariables() {
		ru.uuid = RestApi_MyAccountHelper_TestUtils.UUID;
		createRequest(ru);

		Test.startTest();

		getCaseObj = new RestApi_MyAccountHelper_GetCases(RestContext.request.requestBody.toString());
		resp = getCaseObj.response;
		resp.setFailureVariables();

		Test.stopTest();
	}

	@IsTest
	private static void testSetErrorVariables() {
		ru.uuid = RestApi_MyAccountHelper_TestUtils.UUID;
		createRequest(ru);

		Test.startTest();

		getCaseObj = new RestApi_MyAccountHelper_GetCases(RestContext.request.requestBody.toString());
		resp = getCaseObj.response;
		resp.setErrorVariables();

		Test.stopTest();
	}

	private static void createRequest(RestApi_MyAccountHelper_GetCases.RequestUser ru) {
		RestApi_MyAccountHelper_GetCases.GetCasesRequestObject caseRequest = new RestApi_MyAccountHelper_GetCases.GetCasesRequestObject();
		// Add billing account and other
		caseRequest.user = ru;
		caseRequest.billing = billingAccounts;
		caseRequest.filters = filters;
		caseRequest.type = RestApi_MyAccountHelper_TestUtils.REQUEST_TYPE_GET_CASES;
		caseRequest.accounts = new List<RestApi_MyAccountHelper_GetCases.AccountWrapper>{
				new RestApi_MyAccountHelper_GetCases.AccountWrapper(billingAccounts[0], 'C', 'E') // EPP Billing account type
		};
		RestRequest request = new RestRequest();
		request.requestUri = '/services/apexrest/RestApi_MyAccount/';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(JSON.serialize(caseRequest));
		RestContext.request = request;
	}
}