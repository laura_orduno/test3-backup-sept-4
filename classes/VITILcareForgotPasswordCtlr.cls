public class VITILcareForgotPasswordCtlr extends VisualforcePageController{
    public String username {get; set;}   
    public String usernameError {get;set;}
    public String resetComplete {get;set;}
    
    public PageReference forgotPassword() {
        resetComplete = '';
        usernameError = '';
        List<user> cons = [select id from user where UserType = 'CspLitePortal' and username = :username and isactive=true];
        if(cons.size()<1) {
            usernameError = label.mbrInvalidUsername +' | ' +label.VITILcareInvalidUsernameFR;
            return null;
        }
        
        boolean success = Site.forgotPassword(username);
        
        if (success) {
            resetComplete = label.VITILcarePasswordResetComplete+' | ' +label.VITILcarePasswordResetCompFR;
        }
        else {
            usernameError = label.mbrInvalidUsername+' | ' +label.VITILcareInvalidUsernameFR;
        }
        return null;
    }
    
}