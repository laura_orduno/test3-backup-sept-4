public class ProductOptionSelectionModel {
	private static Map<String,List<String>> CONFIG_FIELD_NAME_CACHE = new Map<String,List<String>>();
	
	public Id productId {get; private set;}
	public QuoteVO.LineItem lineItem {get; set;}
	public SObject line {get{return (lineItem != null) ? lineItem.line : null;}}
	public Integer lineItemKey {get{return (lineItem != null) ? lineItem.key : null;}}
	public ProductOptionSelectionModel[] featureSelections {get; private set;}
	public Boolean customValidated {get; set;}
	public transient String[] validationMessages {get; private set;}
	
	public transient ProductModel product {get; set;}
	public transient ProductFeatureModel feature {get; set;}
	public String activeFeatureKey {get; set;}
	
	// Properties that drive the layout of option selection pages
	// They must be part of view state rather than calculated off related
	// ProductModel because on postback the model is null.
	public Boolean customValidatorExists {get; private set;}
	public Boolean optionSelectionAdd {get; private set;}
	public Boolean optionSelectionClick {get; private set;}
	public Boolean optionLayoutSections {get; private set;}
	public Boolean optionLayoutTabs {get; private set;}
	public Boolean optionLayoutWizard {get; private set;}
	public Boolean configurationFormExists {get; private set;}
	public Boolean configurationFormOnly {get; private set;}
	public Boolean actionColumnDisplayed {get{return isActionColumnDisplayed();}}
	
	//Add'l info configuration
	public Boolean customConfigurationRequired{get; private set;}
	public String customConfigurationPage{get; private set;} 
	public Boolean hasCustomConfigurationPage{get; private set;}
	
	private Map<String,Selection> selectionsById;
	private List<Selection> orderedSelections;
	private SBQQ__ProductOption__c configurationData;
	private String featureKey;
	private Boolean initialized = false;
	private String configurationFields;
	// Indicates this product is being re-configured
	private Boolean reconfigured = false;
	//private transient Map<Id,List<Selection>> selectionsByProductId;
	
	public ProductOptionSelectionModel(ProductModel product) {
		this(product.id);
		this.product = product;
		initState();
		initOptions();
		initFeatureSelections();
		initialized = true;
	}
	
	public ProductOptionSelectionModel(ProductModel product, ProductFeatureModel feature, List<Selection> selections) {
		this(product.id);
		this.product = product;
		this.feature = feature;
		featureKey = feature.getKey();
		initState();
		initialized = true;
		if (selections != null) {
			for (Selection sel : selections) {
				addSelection(sel);
			}
		}
	}
	
	public ProductOptionSelectionModel(QuoteVO.LineItem lineItem) {
		this(lineItem.productId);
		this.lineItem = lineItem;
		reconfigured = true;
	}
	
	public ProductOptionSelectionModel(Id productId) {
		System.assert(productId != null, 'Product ID must not be null');
		selectionsById = new Map<String,Selection>();
		orderedSelections = new List<Selection>();
		this.productId = productId;
		customValidated = true;
	}
	
	public Integer getFeatureCount() {
		return featureSelections.size();
	}
	
	public String getQuantityFieldName() {
		return SBQQ__ProductOption__c.SBQQ__Quantity__c.getDescribe().getName();
	}
	
	public String getPriceFieldName() {
		return SBQQ__ProductOption__c.SBQQ__UnitPrice__c.getDescribe().getName();
	}
	
	public List<String> getConfigurationFieldNames() {
		if (!CONFIG_FIELD_NAME_CACHE.containsKey(configurationFields)) {
			List<String> names = new List<String>();
			for (ConfigurationField field : getConfigurationFields()) {
				names.add(field.name);
			}
			CONFIG_FIELD_NAME_CACHE.put(configurationFields, names);
		}
		return CONFIG_FIELD_NAME_CACHE.get(configurationFields);
	}
	
	public List<ConfigurationField> getConfigurationFields() {
		return getConfigurationFields(configurationFields);
	}
	
	public static List<String> getConfigurationFieldNames(String configurationFields) {
		if (!CONFIG_FIELD_NAME_CACHE.containsKey(configurationFields)) {
			List<String> names = new List<String>();
			for (ConfigurationField field : getConfigurationFields(configurationFields)) {
				names.add(field.name);
			}
			CONFIG_FIELD_NAME_CACHE.put(configurationFields, names);
		}
		return CONFIG_FIELD_NAME_CACHE.get(configurationFields);
	}
	
	public static List<ConfigurationField> getConfigurationFields(String configurationFields) {
		List<ConfigurationField> result = new List<ConfigurationField>();
		if (StringUtils.isBlank(configurationFields)) {
			return result;
		}
		for (String fieldName : configurationFields.split(',')) {
			String fname = fieldName.trim();
			String[] parts = fname.split('\\+');
			fname = parts[0];
			String options = null;
			if (parts.size() > 1) {
				options = parts[1];
			}
			if ((MetaDataUtils.getField(SBQQ__ProductOption__c.sObjectType, fname) != null) || Test.isRunningTest()) {
				result.add(new ConfigurationField(fname, options));
			}
		}
		return result;
	}
	
	public Boolean isActionColumnDisplayed() {
		Boolean displayed = !optionSelectionClick;
		for (Selection sel : selectionsById.values()) {
			if (sel.option != null) {
				displayed = displayed || sel.option.configurable;
			}
		}
		return displayed;
	}
	
	public void restoreProduct(ProductModel product) {
		restoreProduct(product, false);
	}
	
	public void restoreProduct(ProductModel product, Boolean quick) {
		this.product = product;
		this.product.setSelections(this);
		if (!initialized) {
			initState();
			if (!quick) {
				initOptions();
				initFeatureSelections();
			}
		} 
		
		// Selections exist; we just need to reconnect to options.
		for (Selection selection : selectionsById.values()) {
			ProductOptionModel option = product.getOptionById(selection.optionId);
			if (option != null) {
				// Option may or may not be found depending on how the product model was loaded
				// For instance, on save only selected options are loaded so unselected
				// selections will not have a matching option.
				selection.restoreOption(option);
			}
		}
		
		if (!quick) {
			// Feature selections exist; we just need to reconnect to features
			for (ProductOptionSelectionModel fselections : featureSelections) {
				fselections.feature = product.getFeatureByKey(fselections.featureKey);
			}
		}
	}
	
	public Boolean validate() {
		validationMessages = new List<String>();
		Boolean valid = customValidated;
		for (ProductOptionSelectionModel sels : featureSelections) {
			valid = valid && sels.validateFeature();
		}
		
		if (!valid) {
			validationMessages.add('Incorrect product selections in some or all categories. Look for messages in affected option category for details.');
			return valid;
		}
		
		Boolean configValid = true;
		for (ProductOptionSelectionModel.Selection sel : getSelections()) {
			if (sel.selected) {
				configValid = configValid && sel.configured;
				if (!sel.isMinQuantityMet()) {
					validationMessages.add(String.format('You have not met minimum quantity ({0}) for {1}.', new String[]{String.valueOf(sel.option.vo.record.SBQQ__MinQuantity__c),sel.option.vo.record.SBQQ__ProductName__c}));
					valid = false;
				}
				if (!sel.isMaxQuantityMet()) {
					validationMessages.add(String.format('You exceeded the maximum allowed quantity ({0}) for {1}', new String[]{String.valueOf(sel.option.vo.record.SBQQ__MaxQuantity__c),sel.option.vo.record.SBQQ__ProductName__c}));
					valid = false;
				}
			}
		}
		if (!valid || !configValid) {
			if (!configValid) {
				validationMessages.add('Some or all options require further configuration. Look for information icon next to options that must be configured');
			}
			return false;
		}
		
		if (product.getVO().constraints != null) {
			for (ProductVO.Constraint constr : product.getVO().constraints) {
				if (constr.isDependency()) {
					if (isOptionSelected(constr.getConstrainedOptionId()) && 
							!isOptionSelected(constr.getConstrainingOptionId())) {
						ProductOptionModel constrained = product.getOptionById(constr.getConstrainedOptionId());
						ProductOptionModel constraining = product.getOptionById(constr.getConstrainingOptionId());
						validationMessages = new String[]{constrained.productName + ' requires ' + constraining.productName};
						valid = false;
						return valid;
					}
				} else if (constr.isExclusion()) {
					if (isOptionSelected(constr.getConstrainedOptionId()) && 
							isOptionSelected(constr.getConstrainingOptionId())) {
						ProductOptionModel constrained = product.getOptionById(constr.getConstrainedOptionId());
						ProductOptionModel constraining = product.getOptionById(constr.getConstrainingOptionId());
						validationMessages = new String[]{constrained.productName + ' is mutually exclusive with ' + constraining.productName};
						valid = false;
						return valid;
					}
				}
			}
		}
		
		ConfigurationRuleValidator validator = new ConfigurationRuleValidator(productId, getSelectedEditedOptions());
		validationMessages = validator.validate();
		return valid && validationMessages.isEmpty();
	}
	
	public Boolean isMinOptionCountMet() {
		if ((feature == null) || (feature.vo == null)) {
			return true;
		}
		return (getSelectedSelectionCount() >= feature.vo.record.SBQQ__MinOptionCount__c);
	}
	
	public Boolean isMaxOptionCountMet() {
		if ((feature != null) && (feature.vo != null) && (feature.vo.record.SBQQ__MaxOptionCount__c != null)) {
			return (getSelectedSelectionCount() <= feature.vo.record.SBQQ__MaxOptionCount__c);
		}
		return true;
	}
	
	private Boolean validateFeature() {
		Boolean valid = true;
		validationMessages = new List<String>();
		
		if (!isMinOptionCountMet()) {
			validationMessages.add('Too few options selected');
			valid = false;
		} else if (!isMaxOptionCountMet()) {
			validationMessages.add('Too many options selected');
			valid = false;
		}
		return valid;
	}
	
	public List<SBQQ__ProductOption__c> getSelectedEditedOptions() {
		return getEditedOptions(true);
	}
	
	public List<SBQQ__ProductOption__c> getAllEditedOptions() {
		return getEditedOptions(false);
	}	
	
	public List<SBQQ__ProductOption__c> getEditedOptions(Boolean selectedOnly) {
		List<SBQQ__ProductOption__c> result = new List<SBQQ__ProductOption__c>();
		for (ProductOptionSelectionModel.Selection sel : getSelections()) {
			ProductOptionModel option = product.getOptionById(sel.optionId);
			if ((!selectedOnly || sel.selected) && (option != null)) {
				result.add(option.mergeWithSelection(sel.editableOption));
			}
		}
		
		// Add custom config data just in case some rules key off it
		if (configurationData != null) {
			result.add(configurationData);
		}
		
		return result;
	}
	
	/**
	 * Returns dummy ProductOption record that holds custom configuration fields
	 * Upon completion of configuration the values will be written out 
	 * to Quote Line generated by this product
	 */
	public SBQQ__ProductOption__c getConfigurationData() {
		if (configurationData == null) {
			configurationData = new SBQQ__ProductOption__c();
			if (line != null) {
				QuoteUtils.mapCustomFields(configurationData, line);
			}
		}
		return configurationData;
	}
	
	public String toDebug() {
		return toDebug(true);
	}
	
	public String toDebug(Boolean nested) {
		String result = '{PID=' + productId + ',SELECTIONS=[';
		for (Selection sel : selectionsById.values()) {
			result += '{SELECTED=' + sel.selected + ',';
			result += 'ID=' + sel.id + ',';
			result += 'OID=' + sel.optionId;
			if (nested && (sel.nestedSelections != null)) {
				result += ',NESTED=' + sel.nestedSelections.toDebug();
			}
			result += '},';
		}
		result += ']}';
		return result;
	}
	
	public String getSelectedOptionIdsEncoded() {
		List<Id> selectedOptionIdsList = new List<Id>();
		selectedOptionIdsList.addAll(getSelectedOptionIds());
		return StringUtils.join(selectedOptionIdsList, ',', '');
	}
	
	/**
	 * Returns a set of unique IDs of selected options.
	 */
	public Set<Id> getSelectedOptionIds() {
		return getSelectedOptionIds(false);
	}
	
	public Set<Id> getSelectedOptionIds(Boolean nested) {
		return getSelectedOptionIds(this, nested);
	}
	
	private Set<Id> getSelectedOptionIds(ProductOptionSelectionModel model, Boolean nested) {
		Set<Id> selectedOptionIds = new Set<Id>();
		for (Selection selection : model.selectionsById.values()) {
			if (selection.selected) {
				selectedOptionIds.add(selection.optionId);
				if (nested && (selection.nestedSelections != null)) {
					selectedOptionIds.addAll(selection.nestedSelections.getSelectedOptionIds(nested));
				}
			}
		}
		return selectedOptionIds;
	}
	
	public Set<Id> getSelectedProductIds() {
		Set<Id> result = new Set<Id>();
		for (Selection selection : selectionsById.values()) {
			if (selection.selected) {
				result.add(selection.optionalProductId);
			}
		}
		return result;
	}
	
	public Integer getSelectedSelectionCount() {
		Integer cnt = 0;
		for (Selection sel : selectionsById.values()) {
			if (sel.selected == true) {
				cnt++;
			}
		}
		return cnt;
	}
	
	public Integer getNestedSelectedSelectionCount() {
		Integer cnt = 0;
		for (Selection sel : selectionsById.values()) {
			if (sel.selected == true) {
				cnt++;
				if (sel.nestedSelections != null) {
					cnt += sel.nestedSelections.getNestedSelectedSelectionCount();
				}
			}
		}
		return cnt;
	}
	
	public Set<Id> getOptionIds() {
		Set<Id> optionIds = new Set<Id>();
		for (Selection selection : selectionsById.values()) {
			optionIds.add(selection.optionId);
		}
		return optionIds;
	}
	
	public List<Selection> getSelections() {
		return orderedSelections;
	}
	
	public List<Selection> getDisplayedSelections() {
		return orderedSelections;
	}
	
	public Boolean isOptionSelected(ProductOptionModel option) {
		return getSelectedOptionIds().contains(option.id);
	}
	
	public Boolean isOptionSelected(Id optionId) {
		return getSelectedOptionIds().contains(optionId);
	}
	
	public Selection addSelection(ProductOptionModel option) {
		Selection selection = new Selection(this, option);
		selection.selected = true;
		addSelection(selection);
		selection.configured = !option.configurationRequired;
		if (featureSelections != null) {
			for (ProductOptionSelectionModel fsels : featureSelections) {
				if (fsels.featureKey == option.feature.getKey()) {
					fsels.addSelection(selection);
				}
			}
		}
		return selection;
	}
	
	public Selection addSelection(QuoteVO.LineItem item) {
		Selection selection = new Selection(this, item);
		selection.selected = true;
		addSelection(selection);
		return selection;
	}
	
	public Selection addSelection(Selection selection) {
		selectionsById.put(selection.id, selection);
		orderedSelections.add(selection);
		return selection;
	}
	
	public List<Selection> addOption(ProductOptionModel option) {
		List<Selection> result = new List<Selection>();
		Boolean found = false;
		for (Selection selection : selectionsById.values()) {
			if (selection.optionId == option.id) {
				selection.restoreOption(option);
				result.add(selection);
				found = true;
			}
		}
		if (!found) {
			Selection sel = new Selection(this, option);
			if (sel.listPrice != null) {
				// Don't add options without list price
				sel.configured = !option.configurationRequired;
				addSelection(sel);
				result.add(sel);
			}
		}
		return result;
	}
	
	/**
	 * Removes all selections for specified option.
	 */
	public void removeSelections(ProductOptionModel option) {
		Id optionId = option.Id;
		Set<String> selIds = new Set<String>();
		for (Selection selection : selectionsById.values()) {
			if (selection.optionId == optionId) {
				selIds.add(selection.id);
			}
		}
		
		for (String id : selIds) {
			removeSelection(id);
		}
	}
	
	/**
	 * Removed selection identified by specified ID.
	 */
	public void removeSelection(String selectionId) {
		selectionsById.remove(selectionId);
		initFeatureSelections();
		for (Integer i=0;i<orderedSelections.size();i++) {
			if (orderedSelections[i].id == selectionId) {
				orderedSelections.remove(i);
				return;
			}
		}
	}
	
	public Selection[] getSelections(ProductFeatureModel feature) {
		List<Selection> result = new List<Selection>();
		for (ProductOptionModel option : feature.options) {
			for (Selection sel : selectionsById.values()) {
				if (sel.optionId == option.id) {
					sel.restoreOption(option);
					result.add(sel);
				}
			}
		}
		return result;
	}
	
	public Selection findSelectionById(String id) {
		if (selectionsById.containsKey(id)) {
			return selectionsById.get(id);
		} else {
			for (Selection sel : selectionsById.values()) {
				if (sel.hasNested()) {
					Selection found = sel.getNestedSelections().findSelectionById(id);
					if (found != null) {
						return found;
					}
				}
			}
		}
		return null;
	}
	
	public Selection[] findSelectionsByProductIds(Set<Id> productIds) {
		Selection[] result = new Selection[0];
		for (Selection sel : selectionsById.values()) {
			if (productIds.contains(sel.optionalProductId)) {
				result.add(sel);
			}
			result.addAll(sel.getNestedSelections().findSelectionsByProductIds(productIds));
		}
		return result;
	}
	
	public void generateProvisioningSummary(Map<String,AdditionalInformation__c[]> provisioningMap) {
		for (Selection sel : selectionsById.values()) {
			if ((sel.selected) && (sel.option.vo.record.Provisioning_Summary_Fields__c != null)) {
				AdditionalInformation__c[] ais = provisioningMap.get(sel.lineId);
				if (ais == null) {
					ais = provisioningMap.get(sel.id);
				}
				if (ais != null) {
					sel.provisioningSummary = '';
					for (AdditionalInformation__c ai : ais) {
						for (String fname : sel.option.vo.record.Provisioning_Summary_Fields__c.split(',')) {
							fname = fname.trim();
							Object value = ai.get(fname);
							if (value != null) {
								sel.provisioningSummary = sel.provisioningSummary + MetaDataUtils.getField(AdditionalInformation__c.sObjectType, fname).getDescribe().getLabel() + ': ' + value + '\n'; 
							} 
						}
						sel.provisioningSummary += '\n';
					}
				}
			}
		}
	}
	
	private void initState() {
		configurationFields = product.getVO().record.SBQQ__ConfigurationFields__c;
		customValidatorExists = !StringUtils.isBlank(product.getVO().record.SBQQ__ConfigurationValidator__c);
		optionSelectionAdd = product.getVO().record.SBQQ__OptionSelectionMethod__c == 'Add';
		optionSelectionClick = (product.getVO().record.SBQQ__OptionSelectionMethod__c == null) || (product.getVO().record.SBQQ__OptionSelectionMethod__c == 'Click');
		optionLayoutSections = (product.getVO().record.SBQQ__OptionLayout__c == null) || (product.getVO().record.SBQQ__OptionLayout__c == 'Sections');
		optionLayoutTabs = (product.getVO().record.SBQQ__OptionLayout__c == 'Tabs');
		optionLayoutWizard = (product.getVO().record.SBQQ__OptionLayout__c == 'Wizard');
		configurationFormExists = product.getVO().hasConfigurationFields();
		configurationFormOnly = configurationFormExists && product.getVO().options.isEmpty();
		customConfigurationRequired = product.getVO().record.SBQQ__CustomConfigurationRequired__c;
		customConfigurationPage = product.getVO().record.SBQQ__CustomConfigurationPage__c;
		hasCustomConfigurationPage = !StringUtils.isBlank(customConfigurationPage);
		ProductFeatureModel defaultFeature = product.getDefaultFeature();
		if (defaultFeature != null) {
			activeFeatureKey = defaultFeature.getKey();
		}
	}
	
	/**
	 * Returns selections in this model grouped by feature.
	 */
	private void initFeatureSelections() {
		featureSelections = new List<ProductOptionSelectionModel>();
		for (ProductFeatureModel feature : product.features) {
			featureSelections.add(new ProductOptionSelectionModel(product, feature, getSelections(feature)));
		}
	}
	
	private void initOptions() {
		for (ProductOptionModel option : product.getAllOptions()) {
			if (optionSelectionClick || option.required) {
				addOption(option);
			}
		}
	}
	
	public class Selection {
		// Public properties
		public Id optionId {get; private set;}
		public Id optionalProductId {get; private set;}
		public Boolean selected {get; set;}
		public String id {get; private set;}
		public QuoteVO.LineItem lineItem {get; set;}
		
		public SBQQ__ProductOption__c editableOption {get; private set;}
		public Boolean configured {get; set;}
		public ProductOptionSelectionModel parent {get; private set;}
		public Boolean required {get; private set;}
		public Boolean priceEditable {get; private set;}
		public Boolean quantityEditable {get; private set;}
		public Boolean existing {get; private set;}
		public Decimal listPrice {get; set;}
		public Map<String,String> externalConfigurationData {get; private set;}
		
		public transient ProductOptionModel option {get; private set;}
		public transient String provisioningSummary {get; private set;}
		
		// Convenience properties
		public ProductModel configuredProduct {get{return option.configuredProduct;}}
		public ProductModel optionalProduct {get{return option.optionalProduct;}}
		public String lineId {get{return (line == null) ? null : line.Id;}}
		public SObject line {get{return (lineItem != null) ? lineItem.line : null;}}
		public Integer lineItemKey {get{return (lineItem != null) ? lineItem.key : null;}}
		
		
		private ProductOptionSelectionModel nestedSelections;
		
		private Selection(ProductOptionSelectionModel parent, Id optionId) {
			System.assert(optionId != null, 'optionId must not be null');
			this.parent = parent;
			this.optionId = optionId;
			id = optionId + ':' + (Math.random() * 1000000).longValue();
			editableOption = new SBQQ__ProductOption__c();
			configured = true;
			existing = false;
		}
		
		public Selection(ProductOptionSelectionModel parent, QuoteVO.LineItem lineItem) {
			this(parent, lineItem.productOptionId);
			optionalProductId = lineItem.productId;
			QuoteUtils.setCurrencyCode(editableOption, QuoteUtils.getCurrencyCode(lineItem.line));
			this.lineItem = lineItem;
			this.existing = lineItem.isExistingProduct();
			this.priceEditable = lineItem.isPriceEditable();
			nestedSelections = new ProductOptionSelectionModel(lineItem);
			if (lineItem.product.SBQQ__ConfigurationFields__c != null) {
				QuoteUtils.mapCustomFields(editableOption, line, ProductOptionSelectionModel.getConfigurationFieldNames(lineItem.product.SBQQ__ConfigurationFields__c));
			}
			listPrice = lineItem.listPrice;
			Decimal bqty = lineItem.bundledQuantity;
			editableOption.SBQQ__Quantity__c = (bqty != null) ? bqty : lineItem.quantity;
			editableOption.SBQQ__Discount__c = lineItem.optionDiscountRate;
			editableOption.SBQQ__DiscountAmount__c = lineItem.optionDiscountAmount;
			editableOption.SBQQ__UnitPrice__c = listPrice;
			if (lineItem.isPricingMethodCustom()) {
				editableOption.SBQQ__UnitPrice__c = lineItem.customerPrice;
			}
			if (lineItem.configurationRequired == true) {
				configured = false;
			}
			calculate();
		}
		
		public Selection(ProductOptionSelectionModel parent, ProductOptionModel option) {
			this(parent, option.vo.record.id);
			optionalProductId = option.optionalProductId;
			QuoteUtils.setCurrencyCode(editableOption, QuoteUtils.getCurrencyCode(option.vo.record));
			listPrice = option.vo.record.SBQQ__UnitPrice__c;
			restoreOption(option);
			nestedSelections = new ProductOptionSelectionModel(option.optionalProductId);
			selected = (!parent.reconfigured && option.vo.record.SBQQ__Selected__c) || (option.required);
			editableOption.SBQQ__Discount__c = option.vo.record.SBQQ__Discount__c;
			editableOption.SBQQ__DiscountAmount__c = option.vo.record.SBQQ__DiscountAmount__c;
			editableOption.SBQQ__Quantity__c = option.vo.record.SBQQ__Quantity__c;
			if (editableOption.SBQQ__Quantity__c == null) {
				editableOption.SBQQ__Quantity__c = 1;
				if (option.vo.record.SBQQ__MinQuantity__c != null) {
					editableOption.SBQQ__Quantity__c = option.vo.record.SBQQ__MinQuantity__c;
				}
				
			}
			editableOption.SBQQ__UnitPrice__c = option.vo.record.SBQQ__UnitPrice__c;
			if (editableOption.SBQQ__UnitPrice__c == null) {
				editableOption.SBQQ__UnitPrice__c = listPrice;
			}
			
			externalConfigurationData = option.externalConfigurationData;
			
			calculate();
		}
		
		public ProductOptionSelectionModel getNestedSelections() {
			return nestedSelections;
		}
		
		public Boolean hasNested() {
			return (nestedSelections != null);
		}
		
		public void restoreOption(ProductOptionModel option) {
			this.option = option;
			System.assert(option != null, 'Attempt to set null option');
			this.required = option.required;
			this.priceEditable = !existing && option.priceEditable;
			this.quantityEditable = !existing && option.quantityEditable;
			if ((option.optionalProduct != null) && (listPrice == null)) {
				listPrice = option.optionalProduct.getVO().getDefaultListPrice();
			}
			editableOption.SBQQ__OptionalSKU__c = option.optionalProductId;
			calculate();
		}
		
		public void calculate() {
			if (!priceEditable) {
				if (editableOption.SBQQ__Discount__c != null) {
					editableOption.SBQQ__UnitPrice__c = QuoteUtils.multiply(listPrice, QuoteUtils.divide(editableOption.SBQQ__Discount__c, 100.0));
				} else if (editableOption.SBQQ__DiscountAmount__c != null) {
					editableOption.SBQQ__UnitPrice__c = QuoteUtils.subtract(listPrice, editableOption.SBQQ__DiscountAmount__c);
				} else if (editableOption.SBQQ__UnitPrice__c == null) {
					editableOption.SBQQ__UnitPrice__c = listPrice;
				}
			}
		
		}
		
		public Boolean isMinQuantityMet() {
			if ((option != null) && (option.vo.record.SBQQ__MinQuantity__c != null)) {
				return (editableOption.SBQQ__Quantity__c != null) && (editableOption.SBQQ__Quantity__c >= option.vo.record.SBQQ__MinQuantity__c);
			}
			return true;
		}
		
		public Boolean isMaxQuantityMet() {
			if ((option != null) && (option.vo.record.SBQQ__MaxQuantity__c != null)) {
				return (editableOption.SBQQ__Quantity__c != null) && (editableOption.SBQQ__Quantity__c <= option.vo.record.SBQQ__MaxQuantity__c);
			}
			return true;
		}
	}
	
	public class ConfigurationField {
		public String name {get; private set;}
		public Boolean required {get; private set;}
		
		public ConfigurationField(String name, String options) {
			this.name = name;
			this.required = false;
			if (!StringUtils.isBlank(options)) {
				if (options.equalsIgnoreCase('r')) {
					this.required = true;
				}
			}
		}
	}
}