@isTest
private class OCOM_Capture_CreditCheckControllerTest {
    @testSetup
    static void testDataSetup() {
        String orderId= OrdrTestDataFactory.singleMethodForOrderId();
        Order ordObj=[select Account.Credit_Profile__c,AccountId, Credit_Check_Not_Required__c,RequestedDate__C,status,Rush__c,Ban__c,Shipping_Address__c FROM Order where id=:orderId];
        ordObj.status='Not Submitted';
        ordObj.Rush__c=true;
        ordObj.Ban__c='12345';
        ordObj.Shipping_Address__c='test address';
        ordObj.Credit_Check_Not_Required__c=false;
        Date day1 = Date.today();
        Date todayPlus = day1.addDays(4);
        ordObj.RequestedDate__C=todayPlus;
        update ordObj;
        
        Account anAccount = [select Account_Status__c from Account where id=:ordObj.AccountId limit 1];
        anAccount.Account_Status__c = 'ACTIVE';
        update anAccount;
        
        Credit_Assessment__c aCAR = new Credit_Assessment__c(CAR_Status__c='Pending');
        insert aCAR;
        
        Credit_Profile__c creditProfile=new Credit_Profile__c(name='CP-RCID', credit_reference_TL__c='123456', account__c=ordObj.accountid, RecordTypeId = Schema.SObjectType.Credit_Profile__c.getRecordTypeInfosByName().get('Business').getRecordTypeId(), Legal_Entity_Type__c='Incorporated (INC)');
        insert creditProfile;
        
        OrderItem anOrderItem=[select vlocity_cmt__RecurringTotal__c from OrderItem where OrderId=:orderId limit 1];
        anOrderItem.vlocity_cmt__RecurringTotal__c=400.00;
        update anOrderItem;
       
        ordObj.Account.Credit_Profile__c =creditProfile.id;
        ordObj.Credit_Assessment__c=aCAR.id;
        update ordObj;
	}
    
    @isTest
	private static void creditCheckTest1() {
        Order ordObj = [SELECT Id FROM Order WHERE orderMgmtId__c='1234567890' LIMIT 1];
        if (ordObj != null) {
            ApexPages.currentPage().getParameters().put('parentOrderId',ordObj.Id);
            Test.startTest();
            OCOM_Capture_CreditCheckController creditCheckController = new OCOM_Capture_CreditCheckController();
            creditCheckController.setOId(String.valueOf(ordObj.id));
            creditCheckController.invokeCreditAssessment();
            Test.stopTest();
            system.assert(creditCheckController.isCarRequired, true);
        }
    }
}