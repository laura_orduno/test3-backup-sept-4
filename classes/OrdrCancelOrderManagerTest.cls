@isTest
public class OrdrCancelOrderManagerTest 
{
    /*
    @isTest
    private static void testCancelOrderWithOrderNotFound()
    {
        //leave the DML outside the Test.startTest();
       
        List<Order> orders = OrdrTestDataFactory.createOrdersWithServiceAddress();
        Test.startTest();
        
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        
        //Call the method that invokes a callout      
     
        Id anId = orders[0].Id;  
        System.debug('testCancelOrderWithorderNotFound an order id=' + anId);
        TpFulfillmentCustomerOrderV3.CustomerOrder customerOrder = OrdrCancelOrderManager.cancelOrder(Id.valueOf('801e0000001PT'));
        TpCommonBaseV3.CharacteristicValue[]  characteristicValues =  customerOrder.CharacteristicValue;
                
        System.assertNotEquals(null, characteristicValues, 'characteristicValues is an array and it should not be null');
       
        for( TpCommonBaseV3.CharacteristicValue characteristicValue : characteristicValues)
        {
            List<String> values = characteristicValue.Value;
           
            System.assertNotEquals(null, values, 'characteristicValue.Value is a list and it should not be null');
            
            for(String result: values)
            {
                  // Verify that a fake result is returned     
                 System.assertEquals(OrdrCancelOrderManager.SUCCESS_SYMBOL, result);
            }                
        }      
      
        
        Test.stopTest();
        
    }    
   
    @isTest
    private static void testCancelOrderWithoutOrderId() 
    {
        List<Order> orders = OrdrTestDataFactory.createOrdersWithServiceAddress();
        Test.startTest();
        
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        
        //Call the method that invokes a callout      
     
        Id anId = orders[0].Id;  
        System.debug('testCancelOrderWithoutOrderId an order id=' +anId);
        TpFulfillmentCustomerOrderV3.CustomerOrder customerOrder = OrdrCancelOrderManager.cancelOrder(null);
      
       // System.AssertException;
        System.assert(customerOrder==null);
       // TpCommonBaseV3.CharacteristicValue[]  characteristicValues =  customerOrder.CharacteristicValue;
       Test.stopTest();
    }
    */
    @testSetup static void testData() {
        OrdrTestDataFactory.createOrderingWSCustomSettings('CancelOrderEndpoint','http://testUrl');
    }

    @isTest
    private static void testCancelOrder() 
    {
        //leave the DML outside the Test.startTest();
       
        List<Order> orders = OrdrTestDataFactory.createOrdersWithServiceAddress();
        Test.startTest();
        
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        
        //Call the method that invokes a callout      
     
        Id anId = orders[0].Id;  
        System.debug('testCancelOrder an order id=' + anId);
        TpFulfillmentCustomerOrderV3.CustomerOrder customerOrder = OrdrCancelOrderManager.cancelOrder(anId);
        TpCommonBaseV3.CharacteristicValue[]  characteristicValues =  customerOrder.CharacteristicValue;
                
        System.assertNotEquals(null, characteristicValues, 'characteristicValues is an array and it should not be null');
       
        for( TpCommonBaseV3.CharacteristicValue characteristicValue : characteristicValues)
        {
            List<String> values = characteristicValue.Value;
           
            System.assertNotEquals(null, values, 'characteristicValue.Value is a list and it should not be null');
            
            for(String result: values)
            {
                  // Verify that a fake result is returned     
                 System.assertEquals(OrdrCancelOrderManager.SUCCESS_SYMBOL, result);
            }                
        }      
      	OrdrCancelOrderManager obj = new OrdrCancelOrderManager();
        OrdrCancelOrderManager obj1 = new OrdrCancelOrderManager(anId);
        System.enqueueJob(obj);
        ID jobID = System.enqueueJob(obj1);
        Test.stopTest();
        
    }

}