@isTest
private class QuoteProductSearchControllerTests {
    testMethod static void testSearch() {
        QuoteProductSearchController target = new QuoteProductSearchController();
        
        target.province = 'AB';
        target.rateBand = 'A-D';
        
        target.onSearch();
    }
    
    testMethod static void testContinue() {
        QuoteProductSearchController target = new QuoteProductSearchController();
        
        target.province = 'AB';
        target.rateBand = 'A-D';
        
        target.onContinue();
        target.getProductCount();
    }
    
    testMethod static void testLocation() {
        
        
        
        Web_Account__c account = new Web_Account__c(Name = 'Test', Billing_Address__c = 'Test', Billing_City__c = 'Surrey', Billing_Province__c = 'Calgary', Billing_Postal_Code__c = 'V4N 0Z5', Shipping_Address__c = '123 Main St', Shipping_City__c = 'Calgary', Shipping_Province__c = 'AB', Shipping_Postal_Code__c = 'V4N 0Z5');
        insert account;
        Address__c loc = new Address__c(Rate_Band__c = 'A-D', State_Province__c = 'AB', Web_Account__c = account.id);
        insert loc;
        
        ApexPages.currentPage().getParameters().put('addon', '1');
        ApexPages.currentPage().getParameters().put('aid', account.id);
        
        QuoteProductSearchController target = new QuoteProductSearchController();
        target.locationId = loc.id;
        target.province = 'AB';
        target.rateBand = 'A-D';
        
        target.onContinue();
        target.getProductCount();
    }
    
    /* public class ProductWrapper {
        private PriceBookEntry entry;
        public String id {get{return entry.Product2.Id;}}
        public String name {get{return entry.Product2.Name;}}
        public String description {get{
            if (entry.Product2.Description != null) {
                return entry.Product2.Description.replaceAll('\n','<br/>');
            }
            return null;
        }}
        public String specifications {get{return entry.Product2.SBQQ__Specifications__c;}}
       
        public ProductWrapper(PriceBookEntry entry) {
            this.entry = entry;
        }
    }*/
}