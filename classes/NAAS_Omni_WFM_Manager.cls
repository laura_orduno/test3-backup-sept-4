/* This class is used for WFM's Omniscript Backend Logic .
Author : Accenture 
Developer: Arpit Goyal
Created Date: 17-October-2016
*/

global with sharing  class NAAS_Omni_WFM_Manager implements vlocity_cmt.VlocityOpenInterface2{
    
    public Boolean invokeMethod(String methodName, Map<String, Object> input, Map<String, Object> output, Map<String, Object> options)  {
        
        
        if (methodName.equals('isShippableProductPresent')){
             isShippableProductPresent(input, output, options);
             System.debug('MEthod called>>>>>>>>isShippableProductPresent');
             return true;
        }
        if (methodName.equals('setIsOnsiteEditable')){
             setIsOnsiteEditable(input, output, options);
             return true;
        }
        
        if (methodName.equals('setIsOnsiteEditableClose')){
             setIsOnsiteEditableClose(input, output, options);
             return true;
        }
        
        if (methodName.equals('getContactDetails')){
             getContactDetails(input, output, options);
             return true;
        }
        return false;
    }
    
     
    
    /*******************************************
     * @author : Arpit Goyal
     * Release / Sprint : Sprint 9
     * params : Map<String, Object> input, Map<String, Object> output, Map<String, Object> options
     * Description : This method fetches conatct record from order's RCID account and sets into om niscript json
    /******************************************/
    
    public void getContactDetails(Map<String, Object> input, Map<String, Object> output, Map<String, Object> options){
        String orderId = (String)input.get('ContextId');
        Map<String, Object> defaultContactInfo = new Map<String, Object>();
        List<Order> fetchedOrder = [Select id, CustomerAuthorizedBy.Name, ContactEmail__c, ContactPhone__c from Order where id =:orderId];
        system.debug('fetchedOrder ----->' + fetchedOrder);
        //system.assertEquals(fetchedOrder.size() , 100);
        if(fetchedOrder  != null){
            defaultContactInfo.put('defaultContactName', fetchedOrder[0].CustomerAuthorizedBy.Name);            
            defaultContactInfo.put('defaultContactEmail', fetchedOrder[0].ContactEmail__c );
            //defaultContactInfo.put('defaultContactEmail', fetchedOrder[0].ContactPhone__c );  
            system.debug('outPutMap, getContactDetails() --->' + output);
            output.put('defaultContactInfo',defaultContactInfo);
            system.debug('outPutMap, getContactDetails() filled --->' + output);
        }
    }
    
    
    
    
    
    //21-Sept-2016 Arpit : Sprint : 5 : This method resets the value of isOnsiteContactEditable value in the NAAS order capture omniscript
    public void setIsOnsiteEditable(Map<String, Object> inputMap, Map<String, Object> outputMap, Map<String, Object> options) {
        String isOnsiteContactEditable = 'open' ;
        outPutMap.put('closeOnsiteBlock', isOnsiteContactEditable);
        
        
    }
    //21-Sept-2016 Arpit : Sprint : 5 : This method resets the value of isOnsiteContactEditable value in the NAAS order capture omniscript
    public void setIsOnsiteEditableClose(Map<String, Object> inputMap, Map<String, Object> outputMap, Map<String, Object> options) {
        String isOnsiteContactEditable = 'close' ;
        outPutMap.put('closeOnsiteBlock', isOnsiteContactEditable ); 
        system.debug('outPutMap --->' + outPutMap);
    }
    
    //27-dec-2016  Danish Sprint 11:This method checks if there are any Shippable Products in the order. 
    public void isShippableProductPresent(Map<String, Object> input, Map<String, Object> output, Map<String, Object> options){
        Integer numberOfShippableProducts = 0;
        Boolean isShippableProductPresent =false;
        String orderId = (String)input.get('ContextId');
        Map<String, Object> ShippableInfo= new Map<String, Object>();
        try{
        List<OrderItem> orderList = [Select id,pricebookentry.product2.OCOM_Shippable__c  from OrderItem where orderId =:orderId];
        IF(orderList.size()>0 && orderList != null ){
          For(OrderItem o: orderList){
            if(o.pricebookentry.product2.OCOM_Shippable__c == 'Yes'){
                    numberOfShippableProducts++;    
                }
          }
        }  
        if(numberOfShippableProducts > 0){
             isShippableProductPresent=true;
         }
        
        if(isShippableProductPresent==true){
           ShippableInfo.put('isShippableProductPresent', isShippableProductPresent);            
            }
        if(isShippableProductPresent==false){
            ShippableInfo.put('isShippableProductPresent', isShippableProductPresent);
           }
        output.put('ShippableInfo',ShippableInfo);
        system.debug('outPutMap, isShippableProductPresent()' + output);
    }     
    catch(exception e){
            String errorMessage=Label.OCOM_ExceptionMessagePrefix+e.getMessage();
            output.put('error',errorMessage);
        }
    }           
    
    
}