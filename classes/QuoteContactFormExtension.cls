/*
    Quote Contact Form Extension
    QuoteContactFormExtension.cls
    Part of the TELUS QuoteQuickly Portal
    
    Author: Alex Miller @ Traction <amiller@tractionondemand.com>
    Created: March 8, 2012
    
    Since: Release 5
    Initial Definition: R5rq11
    
    Allows you to control the 'requireness' of fields on the Contact Form component.
    This is used in conjunction with validation in QuoteDetailController.cls to meet R5rq11

    Page Parameters:
     - Name -        - Type -        - Description -                         - Since -
    stage           String          Quote stage as in SBQQ__Status__c on    March 5, 2012
                                     SBQQ__Quote__c
    qid             String          Quote ID of the current quote           March 5, 2012

    Properties:
     - Method -          - Returns -     - Description -                     - Since -
    rDraft              Boolean         True when quote is at or beyond     March 5, 2012
                                         the draft stage
    rCreditCheck        Boolean         True when quote is at or beyond     March 5, 2012
                                         the credit check stage
    rAgreement          Boolean         True when quote is at or beyond     April 20, 2012
                                         the agreement stage
    isChannelInbound    Boolean         True when user's channel is set     April 23, 2012
                                         to "Inbound"
    isChannelOutbound   Boolean         True when user's channel is set     April 23, 2012
                                         to "Outbound"
*/
public without sharing class QuoteContactFormExtension extends QuoteComponentController {
    
    public ContactVO cvo {get; set;}
    
    private SBQQ__Quote__c quote;
    public Boolean rAgreement {get; private set;}
    public Boolean rCreditCheck {get; private set;}
    public Boolean rDraft {get; private set;}
    public Boolean isChannelInbound {get{ return QuotePortalUtils.getChannel().equalsIgnoreCase('inbound'); }}
    public Boolean isChannelOutbound {get{ return QuotePortalUtils.getChannel().equalsIgnoreCase('outbound'); }}
    
    public static final String REQUIRED_STAGE_CREDIT_CHECK = 'Sent for Credit Check'; 
    public static final String REQUIRED_STAGE_AGREEMENT = 'Sent Agreement, waiting acceptance';
    public static final String REQUIRED_STAGE_CUSTOMER_ACCEPTED = 'Customer Accepted - Pending Dealer input';
    public static final String REQUIRED_STAGE_DRAFT = 'Draft';
    private static final Map<String, Integer> requiredOrdinalByStatus = new Map<String, Integer>();
    static {
        Schema.DescribeFieldResult quoteStatusField = SBQQ__Quote__c.SBQQ__Status__c.getDescribe();
        List<Schema.PicklistEntry> quoteStatusList = quoteStatusField.getPicklistValues();
        Integer ordinal = 1;
        for(Schema.PicklistEntry ple : quoteStatusList) {
            requiredOrdinalByStatus.put(ple.getValue(), ordinal);
            ordinal++;
        }
    }
    
    public QuoteContactFormExtension() {
        if (ApexPages.currentPage() != null) {
            String stage = ApexPages.currentPage().getParameters().get('stage');
            String qid = ApexPages.currentPage().getParameters().get('qid');
            rDraft = true;
            rCreditCheck = false;
            rAgreement = false;
            if (qid != null) {
                quote = QuoteUtilities.loadQuote( qid );
                if (quote != null && quote.id != null) {
                    if (requiredOrdinalByStatus.get(quote.SBQQ__Status__c) >= requiredOrdinalByStatus.get(REQUIRED_STAGE_CREDIT_CHECK)) {
                        rCreditCheck = true;
                    }
                    if (requiredOrdinalByStatus.get(quote.SBQQ__Status__c) >= requiredOrdinalByStatus.get(REQUIRED_STAGE_AGREEMENT)) {
                        rAgreement = true;
                    }
                }
            }
            if (stage != null) {
                if (requiredOrdinalByStatus.get(stage) >= requiredOrdinalByStatus.get(REQUIRED_STAGE_CREDIT_CHECK)) {
                    rCreditCheck = true;
                }
                if (requiredOrdinalByStatus.get(stage) >= requiredOrdinalByStatus.get(REQUIRED_STAGE_AGREEMENT)) {
                    rAgreement = true;
                }
            }
        }
    }
    
    public static Integer getRequiredOrdinalByStatus(String quoteStatus){
        return requiredOrdinalByStatus.get(quoteStatus);
    }
    
    public string errorDefaultMessage {get; set;}
    public string errorDefaultMessagePicklist {get; set;}
    
    /* Draft! */
    public string errorFirstName {get; set;}
    public string errorLastName {get; set;}
    public string errorEmail {get; set;}
    public string errorPhone {get; set;}
    
    /* Credit Check */
    public string errorTitle {get; set;}
    public string errorTitleCategory {get; set;}
    public string errorDepartment {get; set;}
    public string errorLanguage {get; set;}
    public string errorRole {get; set;}
    public string errorMailingStreet {get; set;}
    public string errorMailingCity {get; set;}
    public string errorMailingProvince {get; set;}
    public string errorMailingPostalCode {get; set;}
    
    public void clearErrors() {
        /* Draft! */
        errorFirstName  = '';
        errorLastName  = '';
        errorEmail  = '';
        errorPhone  = '';
        
        /* Credit Check */
        errorTitle  = '';
        errorTitleCategory  = '';
        errorDepartment  = '';
        errorLanguage  = '';
        errorRole  = '';
        errorMailingStreet  = '';
        errorMailingCity  = '';
        errorMailingProvince  = '';
        errorMailingPostalCode  = '';
    }
    
    public boolean doValidation() {
        clearErrors();
        errorDefaultMessage = '<strong>Error:</strong> You must enter a value.';
        errorDefaultMessagePicklist = '<strong>Error:</strong> You must choose a value.';
        if (cvo == null) {
            errorFirstName = 'not inited or no cvo';
            return false;
        }
        system.debug(cvo.record);
        system.debug(rDraft);
        system.debug(rCreditCheck);
        boolean error = false;
        if (rDraft) {
            if (cvo.record.FirstName == null || cvo.record.FirstName == '') {
                errorFirstName = errorDefaultMessage;
                error = true;
            }
            if (cvo.record.LastName == null || cvo.record.LastName == '') {
                errorLastName = errorDefaultMessage;
                error = true;
            }
            if (cvo.record.Phone == null || cvo.record.Phone == '') {
                errorPhone = errorDefaultMessage;
                error = true;
            }
            if (cvo.record.Email == null || cvo.record.Email == '') {
                errorEmail = errorDefaultMessage;
                error = true;
            }
            
        }
        if (rCreditCheck) {
            if (cvo.record.Title == null || cvo.record.Title == '') {
                errorTitle = errorDefaultMessage;
                error = true;
            }
            if (cvo.record.Title_Category__c == null || cvo.record.Title_Category__c == '') {
                errorTitleCategory = errorDefaultMessage;
                error = true;
            }
            if (cvo.record.Department_Category__c == null || cvo.record.Department_Category__c == '') {
                errorDepartment = errorDefaultMessage;
                error = true;
            }
            if (cvo.record.Language_Preference__c == null || cvo.record.Language_Preference__c == '') {
                errorLanguage = errorDefaultMessage;
                error = true;
            }
            if (cvo.record.Role_c__c == null || cvo.record.Role_c__c == '') {
                errorRole = errorDefaultMessage;
                error = true;
            }
            if (cvo.record.MailingStreet == null || cvo.record.MailingStreet == '') {
                errorMailingStreet = errorDefaultMessage;
                error = true;
            }
            if (cvo.record.MailingCity == null || cvo.record.MailingCity == '') {
                errorMailingCity = errorDefaultMessage;
                error = true;
            }
            if (cvo.record.MailingState == null || cvo.record.MailingState == '') {
                errorMailingProvince = errorDefaultMessage;
                error = true;
            }
            if (cvo.record.MailingPostalCode == null || cvo.record.MailingPostalCode == '') {
                errorMailingPostalCode = errorDefaultMessage;
                error = true;
            }
        }
        if (rAgreement) {
            // Nothing here yet
        }
        if (error) {
            // Output a page message
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 
                'You must complete all required fields for the before you can continue.'));
            return false;
        }
        return true;
    }
    private static testMethod void testQuoteContactFormExt() {
        Opportunity opp = new Opportunity(Name='Test',CloseDate=System.today(),StageName='Prospecting');
        insert opp;
        
        SBQQ__Quote__c quote = new SBQQ__Quote__c(SBQQ__Opportunity__c=opp.Id);
        insert quote;
        
        ApexPages.currentPage().getParameters().put('qid', quote.Id);
        QuoteContactFormExtension ext = new QuoteContactFormExtension();
        
        system.assertEquals(true, ext.rDraft);
        system.assertEquals(false, ext.rCreditCheck);
        system.assertEquals(false, ext.rAgreement);
        
        ApexPages.currentPage().getParameters().put('stage', REQUIRED_STAGE_CREDIT_CHECK); // 'faking' the stage via URL
        ext = new QuoteContactFormExtension();
        
        system.assertEquals(true, ext.rDraft);
        system.assertEquals(true, ext.rCreditCheck);
        system.assertEquals(false, ext.rAgreement);
        
        ApexPages.currentPage().getParameters().put('stage', REQUIRED_STAGE_AGREEMENT); // 'faking' the stage via URL
        ext = new QuoteContactFormExtension();
        
        system.assertEquals(true, ext.rDraft);
        system.assertEquals(true, ext.rCreditCheck);
        system.assertEquals(true, ext.rAgreement);
        
        ApexPages.currentPage().getParameters().remove('stage');
        quote.SBQQ__Status__c = REQUIRED_STAGE_DRAFT;
        update quote;
        
        ext = new QuoteContactFormExtension();
        
        system.assertEquals(true, ext.rDraft);
        system.assertEquals(false, ext.rCreditCheck);
        system.assertEquals(false, ext.rAgreement);
        
        quote.SBQQ__Status__c = REQUIRED_STAGE_CREDIT_CHECK;
        update quote;
        
        ext = new QuoteContactFormExtension();
        
        system.assertEquals(true, ext.rDraft);
        system.assertEquals(true, ext.rCreditCheck);
        system.assertEquals(false, ext.rAgreement);
        
        quote.SBQQ__Status__c = REQUIRED_STAGE_AGREEMENT;
        update quote;
        
        ext = new QuoteContactFormExtension();
        
        system.assertEquals(true, ext.rDraft);
        system.assertEquals(true, ext.rCreditCheck);
        system.assertEquals(true, ext.rAgreement);
        
        // Let's make a contact with some information...
        Contact c = new Contact(
        FirstName = '',
        LastName = '',
        Phone = '',
        Email = '',
        Title = '',
        Title_Category__c = '',
        Department_Category__c = '',
        Language_Preference__c = '',
        Role_c__c = '',
        MailingStreet = '',
        MailingCity = '',
        MailingState = '',
        MailingPostalCode = '');
        ContactVO cvo = new ContactVO(c);
        ext.cvo = cvo;
        ext.doValidation();
    }
}