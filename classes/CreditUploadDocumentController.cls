public with sharing class CreditUploadDocumentController {
    
    public Boolean previewOnly {get;set;}
    public Boolean uploadOnly {get;set;}
    public String credit_profile_id{get;set;}
    public String car_id{get;set;}
    public String checklist_id{get;set;}
    public String showText{get;set;}
    public String doc_Id{get;set;}
    public String crr_Id{get;set;}
    private Boolean insert_CRR{get;set;}
    public String doctype{get;set;}
    public String recordType{get;set;}
    public String subTab{get;set;}
    public String titleString{get;set;}    
    private String reportName{get;set;}    
    
    private String externalCarId{get;set;}
    
    public final Credit_Related_Info__c CRI_Object{get;set;}
    public Long businessCustomerId{get;set;}
    
    public Blob renderasPdf{get;set;}
    
    public Document document {
        get {
            if (document == null)
                document = new Document();
            return document;
        }
        set;
    }
    
    public CreditUploadDocumentController(ApexPages.StandardController controller) {
        previewOnly=false;
        uploadOnly=false;
        insert_CRR=true;
        doc_Id=ApexPages.currentPage().getParameters().get('doc_ID');
        
        crr_Id=ApexPages.currentPage().getParameters().get('crr_ID');
        System.debug('... dyy ... CreditUploadDocumentController.crr_Id = ' + crr_Id);
        
        car_id=ApexPages.currentPage().getParameters().get('car_ID');
        System.debug('... dyy ... CreditUploadDocumentController.car_id = ' + car_id);
        
        credit_profile_id=ApexPages.currentPage().getParameters().get('cp_ID');
        System.debug('... dyy ... CreditUploadDocumentController.credit_profile_id = ' + credit_profile_id);
        
        checklist_id=ApexPages.currentPage().getParameters().get('chk_ID');    
        doctype=ApexPages.currentPage().getParameters().get('doctype'); 
        recordtype=ApexPages.currentPage().getParameters().get('RecordType'); 
        subtab=ApexPages.currentPage().getParameters().get('subtab'); 
        if (doctype=='trade') {
            titleString=Label.New_Trade_reference;
        } else if (doctype=='external') {
            titleString=Label.New_Agency_Report;
            recordtype=Schema.SObjectType.Credit_Related_Info__c.getRecordTypeInfosByName().get(Label.External_Agency_Report).getRecordTypeId();
        } else if (doctype=='otherSecure') {
            titleString=Label.Add_Secure_Document;
            recordtype=Schema.SObjectType.Credit_Related_Info__c.getRecordTypeInfosByName().get(Label.Other_Secure_Documents).getRecordTypeId();                    
        } else if (doctype=='public') {
            titleString=Label.New_Public_Report;
            recordtype=Schema.SObjectType.Credit_Related_Info__c.getRecordTypeInfosByName().get(Label.Public_Reports).getRecordTypeId();
        } else if (doctype=='financial') {
            titleString=Label.New_Financial_Report;
            recordtype=Schema.SObjectType.Credit_Related_Info__c.getRecordTypeInfosByName().get(Label.Financial_Reports).getRecordTypeId();
        } else if (doctype=='other') {
            titleString=Label.New_Misc_Other_Internet_Search;
            recordtype=Schema.SObjectType.Credit_Related_Info__c.getRecordTypeInfosByName().get(Label.Misc_Other_Internet_Searches).getRecordTypeId();
        } else {
            titleString='';
        }
        
        if (doctype==null) {
            previewOnly=true;
        } else {
            uploadOnly=true;
        }        
        
        if (crr_id ==null){
            this.CRI_Object = (Credit_Related_Info__c)controller.getRecord();
            crr_Id = this.CRI_Object.id;
            System.debug('... dyy ... this.CRI_Object.id = ' + crr_Id);
            
        } else {
            Try {
                if (crr_id!=null)
                {
                    this.CRI_Object=[select id, document_id__c from credit_related_info__c where id=: crr_id];                
                    insert_CRR = false;
                }
            } catch (exception e) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error retrieving Document Information: ' + e.getMessage()));                
            }
            
        }
        this.CRI_Object.RecordTypeId=recordType;
        
        
        if (doc_ID!=null) {
            previewOnly=true;
        }
        
    }
    
    public PageReference upload() {
        if (document.body==null && doctype!='trade' && doctype!='public')  {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.Credit_Document_Empty)); 
            return null;
        }else if (document.body==null &&(doctype=='trade'||doctype=='public')){
            updateCreditRelatedInfo('');
            return redirectToPreviousPage();
        }
        
        document.AuthorId = UserInfo.getUserId();
        document.IsInternalUseOnly=true;
        document.ispublic=false;    
        reportName=document.name;
        
        System.debug('... dyy ... credit_profile_id:' + credit_profile_id);
        System.debug('... dyy ... car_id:' + car_id);
        System.debug('... dyy ... checklist_id:' + checklist_id);
        
        System.debug('... dyy ... document.name = ' + document.name);
        
        
        try {
            if (credit_profile_id !=null) {
                Credit_Profile__c cp = [select id, Account__r.custprofid__c 
                                        from Credit_Profile__c 
                                        where id = :credit_profile_id];
                businessCustomerId = Long.valueof(cp.Account__r.custprofid__c);
            } else if (car_id != null) {
                Credit_Assessment__c car= [select id, CAR_Account__r.custprofid__c from Credit_Assessment__c where id = :car_id];
                businessCustomerId = Long.valueof(car.CAR_Account__r.custprofid__c);
            } else if (checklist_id != null) {
                Credit_Checklist__c[] chk = [select id
                                             , Credit_Assessment_Ref_No__r.CAR_Account__r.custprofid__c
                                             , Credit_Assessment_Ref_No__r.Name
                                             , Credit_Assessment_Ref_No__r.Credit_Profile__c 
                                             from Credit_Checklist__c 
                                             where id = :checklist_id
                                            ];
                processChecklist(chk);
                /*
                if (chk.size()>0) {
                    businessCustomerId = Long.valueof(chk[0].Credit_Assessment_Ref_No__r.CAR_Account__r.custprofid__c);
                    credit_profile_id=chk[0].Credit_Assessment_Ref_No__r.Credit_Profile__c;
                    externalCarId = chk[0].Credit_Assessment_Ref_No__r.Name;
                } */                      
            }
            if (businessCustomerID == null) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.Credit_Error_Uploading_File + ': Business Customer ID is null'));            
                document.body = null; // clears the viewstate
                document = new Document();
                return null;                
            }
            
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.Credit_Error_Uploading_File + ': ' + e.getMessage()));            
            System.debug('... dyy ... Error in looking up IDs');
            document.body = null; // clears the viewstate
            document = new Document();
            return null;
        }
        
        try {    
            document.FolderId = getFolderId();            
            System.debug('... dyy ... CreditAssessmentCallout.attachCreditReport ... ');
            
            String recordId;
            
            
            if(isAssessmentReport()){
                if ( !Test.isRunningTest() )
                    recordId= CreditAssessmentCallout.attachCreditReportWithCarId(
                        externalCarId
                        ,  businessCustomerId
                        ,  document
                        ,  CreditReferenceCode.getReportTypeCd(CRI_Object.Document_Report_Type__c) 
                        ,  CreditReferenceCode.getReportSourceCd(CRI_Object.Document_report_source__c) );
                
            }else if(isProfileReport()){
                if ( !Test.isRunningTest() )
                    recordId= CreditProfileCalloutUpdate.attachCreditReport(
                        businessCustomerId
                        ,  document
                        ,  CreditReferenceCode.getReportTypeCd(CRI_Object.Document_Report_Type__c) 
                        ,  CreditReferenceCode.getReportSourceCd(CRI_Object.Document_report_source__c) );
                
            }else{
                recordId = saveAttachment();
            }            
            
            System.debug('... dyy ... recordId = ' + recordId);
            System.debug('... dyy ... ReportType = ' + CRI_Object.Document_Report_Type__c);
            System.debug('... dyy ... ReportSource = ' + CRI_Object.Document_report_source__c);              
            if (recordId != null) {
                updateCreditRelatedInfo(recordID);
            } else {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.Credit_Error_Uploading_File));            
                document.body = null; // clears the viewstate
                document = new Document();
                return null;                
            }
        } catch (DMLException e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.Credit_Error_Uploading_File + ': ' + e.getMessage()));
            return null;
        } finally {
            document.body = null; // clears the viewstate
            document = new Document();
        }
        
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,Label.Credit_File_Uploaded_Successfully));
        /*
        PageReference p = null; 
        If (checklist_id != null) {
            p = new PageReference('/'+checklist_id);                                                
        } else {
            p = new PageReference('/'+credit_profile_id);                                    
        }
        
        p.setRedirect(true);
        return p;*/
        return redirectToPreviousPage();
    }
    
    
    
    
    private PageReference redirectToPreviousPage() {
        PageReference p = null; 
        If (checklist_id != null) {
            p = new PageReference('/'+checklist_id);                                                
        } else {
            p = new PageReference('/'+credit_profile_id);                                    
        }        
        p.setRedirect(true);
        return p;
    }
    
    
    public ID getFolderId(){    
        ID folderId;
        Folder[] folders =[select id from Folder where developername = 'Credit_Related_Documents'];
        if(folders!=null
           &&folders.size()!=0) {
               folderId = document.FolderId = folders[0].id;
           } else {
               folderId =document.FolderId = UserInfo.getUserId(); // put it in running user's folder                   
           }
        return folderId;
    }
    
    public PageReference previewPDF(){
        //system.debug(CRI_Object.Document_ID__c + '2nd time');
        
        system.debug('... dyy ... PreviewPDF: crr_id = ' + crr_id);
        if (!previewOnly) {
            return null;
        }
        
        Long reportId;
        System.debug('Document id' + CRI_Object.Document_ID__c);
        if (CRI_Object.Document_ID__c != null) {  
            if(isToViewViaBTCredit(CRI_Object.Document_ID__c)){
                reportID = Long.valueOf(CRI_Object.Document_ID__c);
                return previewPDFviaBTCredit(reportId);
            }else{
                return previewPDFviaSFDC(CRI_Object.Document_ID__c);
            }
        } else {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'Upload a document first before viewing it'));            
            return null;                                 
        }
        
    }   
    
    private Boolean isToViewViaBTCredit(String reportID){
        Boolean isToViewViaBTCredit = false;
        try{
            Long.valueOf(CRI_Object.Document_ID__c);
            isToViewViaBTCredit = true;
        }catch(Exception e){
            System.debug('... dyy ... report is a sfdc attachment ...');
        }
        return isToViewViaBTCredit;
    }
    
    
    
    @TestVisible 
    private PageReference previewPDFviaSFDC(String attachmentId){
        String url = '/servlet/servlet.FileDownload?file=' + attachmentId;
        return new PageReference(url);   
    }
    
    
    private PageReference previewPDFviaBTCredit(Long reportId){
        System.debug('previewing document');
        if ( !Test.isRunningTest() ){
            if(isAssessmentReport())
                document = CreditAssessmentCallout.getCreditReport(reportId);
            else
                document = CreditProfileCalloutUpdate.getCreditReport(reportId);
        }
        
        
        document.FolderId = getFolderId();
        PageReference p = null;
        try {            
            insert document;
            CRI_Object.Document_Internal_ID__c = document.id;
            update CRI_Object;
            
            System.debug('... dyy ... document authorid = ' + UserInfo.getUserID());
            System.debug('... dyy ... document folderid = ' + UserInfo.getUserID());
            p = new PageReference('/servlet/servlet.FileDownload?file='+document.id);                        
        } catch (DMLException e) {
            return null;
        } finally {
            System.debug('... dyy ... document id= ' + document.id);
            document.body = null; // clears the viewstate
            document = new Document();
        }
        p.setRedirect(true);
        return p;        
        
    } 
    
    
    public void updateCreditRelatedInfo(String DocumentID){ 
        //Insert the corresponding record into the Credit Related Info object 
        //If a CRR (Credit Related Record) ID has been passed, then that record should be updated instead
        try {
            CRI_Object.Credit_Assessment__c=car_id;
            CRI_Object.report_name__c=reportName;
            if (doctype=='external') {
                CRI_Object.Credit_Checklist_External_Agency__c =checklist_id;
                CRI_Object.credit_profile_External_Report__c=credit_profile_id;
            } else if (doctype=='otherSecure') {
                CRI_Object.Credit_Checklist_Secure_Document__c =checklist_id;
                CRI_Object.credit_profile_Other_Report__c=credit_profile_id;
            } else if (doctype=='public') {
                CRI_Object.Credit_Checklist_Public_Report__c =checklist_id;
            } else if (doctype=='trade') {
                CRI_Object.Credit_Checklist__c =checklist_id;
            } else if (doctype=='financial') {
                CRI_Object.Credit_Checklist_Financial_Report__c =checklist_id;
            } else {
                CRI_Object.Credit_Checklist_Misc_Other__c =checklist_id;
            }
            
            CRI_Object.document_id__c=DocumentID;
            
            if (!insert_CRR) {              
                update CRI_Object;
            } else {                
                insert CRI_Object;
                insert_CRR = false;
            }
            
            System.debug('... dyy ... CRI_Object.id = ' + CRI_Object.id);
            
        } catch (exception e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.Credit_Error_Uploading_File + ': ' + e.getMessage()));            
        }
    }
    
    /*----------------------------------------------------------*/

    
    @TestVisible 
    private void processChecklist(Credit_Checklist__c[] checklist){    
        if (checklist.size()>0) {
            businessCustomerId = Long.valueof(checklist[0].Credit_Assessment_Ref_No__r.CAR_Account__r.custprofid__c);
            credit_profile_id=checklist[0].Credit_Assessment_Ref_No__r.Credit_Profile__c;
            externalCarId = checklist[0].Credit_Assessment_Ref_No__r.Name;
        } 
    }
    
    @TestVisible 
    private String saveAttachment(){          
        String recordId;
        Attachment attachment = new Attachment();        
        updateCreditRelatedInfo(recordId);
        crr_Id = CRI_Object.id;
        attachment.ParentId = crr_Id;
        attachment.Name = document.name;
        attachment.Body = document.body;
        insert attachment;
        if(attachment!=null &&attachment.id!=null) recordId = attachment.id;  
        return recordId;
    }
    
    @TestVisible 
    private Boolean isAssessmentReport(){    
        Boolean isAssessmentReport =false;
            //= 'financial'.equalsIgnoreCase(doctype);
        return isAssessmentReport;
    }
    
    @TestVisible 
    private Boolean isProfileReport(){   
        Boolean isProfileReport = 'otherSecure'.equalsIgnoreCase(doctype)
            || 'external'.equalsIgnoreCase(doctype);
        return isProfileReport;
    }  
    
}