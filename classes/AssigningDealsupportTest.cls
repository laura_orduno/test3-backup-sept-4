@isTest
public class AssigningDealsupportTest {
    
    
    private static AssigningDealsupportController dealsupportController; 
    
    private static void initDealsupport(){      
        dealsupportController = new AssigningDealsupportController();
        dealsupportController.selectedTeamMember= getUserId();          
        Test.setCurrentPageReference(new PageReference('dyy test page'));         
        System.currentPageReference().getParameters().put('ids', getDealsupportIds());           
        String retUrl = '/' + SObjectType.Offer_House_Demand__c.keyPrefix;   
        System.currentPageReference().getParameters().put('retUrl', retUrl);
    } 

    
    private static ID getUserId(){
        User user = [select id from User where Alias='dyuan' limit 1];
        return user.Id;
    }      
    
    private static String getDealsupportIds(){
        map<string,schema.recordtypeinfo> recordTypeMap=offer_house_demand__c.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        List<Offer_House_Demand__c> ohList = new List<Offer_House_Demand__c>();  
        for(integer i=0;i<11;i++) {
            Offer_House_Demand__c oh = new Offer_House_Demand__c();
            oh.Deal_Support_Team__c = 'WLS';
            if(i==0) oh.Status__c = 'Submitted';
            if(i==1) oh.Status__c = 'Resubmitted';
            if(i==2) oh.Status__c = 'Open';
            if(i==3) oh.Status__c = 'Create Contract';
            if(i==4) oh.Deal_Support_Team__c = null;
            if(i==5) oh.Deal_Support_Team__c = 'NA';
            oh.recordtypeid=recordTypeMap.get('WLN Deal Support').getrecordtypeid();
            oh.type_of_contract__c='CCA';
            oh.signature_required__c='Yes';
            ohList.add(oh);
        }
        insert ohList;
        Offer_House_Demand__c[] offerHouses = [select id from Offer_House_Demand__c where id in :ohList]; 
        String idString = '';   
        for(Offer_House_Demand__c theOfferHouse:offerHouses){
            idString += theOfferHouse.Id + ',';   
        }
        return idString;
    } 
    /*--------------------------------------------------*/
    
    static{initDealsupport();}
    
    @isTest
    public static void getTeamMemberList() {   
        Test.startTest();
        dealsupportController.getTeamMemberList();        
        System.debug(LoggingLevel.INFO,'getTeamMemberList  SOQL Usage : ' +  Limits.getQueries());
        Test.StopTest();
    }
    @isTest
    public  static void assignToMe() {     
        Test.startTest();
        dealsupportController.assignToMe();
         
        System.currentPageReference().getParameters().put('retUrl', null);
        System.currentPageReference().getParameters().put('ids', null);         
        dealsupportController.selectedTeamMember = null;          
        dealsupportController.assignToMe();      
        
        System.debug(LoggingLevel.INFO,'assignToMe SOQL Usage : ' +  Limits.getQueries());
        Test.StopTest();
    }    
    
    @isTest
    public static void managerAssign() {     
        Test.startTest();
        //dealsupportController.managerAssign();
        
        System.currentPageReference().getParameters().put('retUrl', null);
        System.currentPageReference().getParameters().put('ids', null);         
        dealsupportController.selectedTeamMember = null;          
        //dealsupportController.managerAssign();
        
        System.debug(LoggingLevel.INFO,'managerAssign SOQL Usage : ' +  Limits.getQueries());
        Test.StopTest();
    }

}