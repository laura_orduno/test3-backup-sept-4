@isTest
private class QuoteLocationControllerTests {
	private static Web_Account__c account;
	private static Address__c location;
	
	testMethod static void testOnSave() {
		setUp();
		
		ApexPages.currentPage().getParameters().put('id', location.Id);
		ApexPages.currentPage().getParameters().put('retURL', '/home/home.jsp');
		QuoteLocationController target = new QuoteLocationController();
		PageReference pref = target.onSave();
		System.assert(pref != null);
	}
	
	testMethod static void testListing() {
		setUp();
		
		ApexPages.currentPage().getParameters().put('aid', account.Id);
		ApexPages.currentPage().getParameters().put('retURL', '/home/home.jsp');
		QuoteLocationController target = new QuoteLocationController();
		System.assertEquals(1, target.getAddresses().size());
	}
	
	private static void setUp() {
		account = new Web_Account__c(Name='UnitTest');
		insert account;
		
		location = new Address__c(Web_Account__c=account.Id,Special_Location__c='Test');
		location.Rate_Band__c = 'F';
		location.State_Province__c = 'BC';
		insert location;
	}
}