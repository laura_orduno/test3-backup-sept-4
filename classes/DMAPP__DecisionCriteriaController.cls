/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class DecisionCriteriaController extends DMAPP.TASBaseController {
    @RemoteAction
    global static String createDecisionCriteria(String opportunityId, String criterion, String ourPosition) {
        return null;
    }
    @RemoteAction
    global static void deleteDecisionCriteria(String opportunityId) {

    }
    @RemoteAction
    global static Object getDecisionCriteria(String shadowOppId) {
        return null;
    }
    @RemoteAction
    global static String updateDecisionCriteria(String criterionId, String opportunityId, String criterion, String ourPosition) {
        return null;
    }
    @RemoteAction
    global static DMAPP__DM_Decision_Criterion_Score__c updateDecisionCriteriaScore(String rankId, String importance) {
        return null;
    }
    @RemoteAction
    global static void updatePoliticalMapContact(String id, Boolean keyPlayer) {

    }
}
