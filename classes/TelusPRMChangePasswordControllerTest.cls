@isTest
private class TelusPRMChangePasswordControllerTest {
	private static testMethod void testChangePasswordController() {
        // Instantiate a new controller with all parameters in the page
        TelusPRMChangePasswordController controller = new TelusPRMChangePasswordController();
        controller.oldPassword = '123456';
        controller.newPassword = 'qwerty1'; 
        controller.verifyNewPassword = 'qwerty1';                
        
        controller.changePassword();
    }
}