/** Class:OC_OrderPdfHelper
    This Class is to Save and Send Order Pdf.
 **/
public class OC_OrderPdfHelper 
{
    public boolean displayModal {get; set;}
    public Order o {get;set;}
    public ID oId {get;set;}
    public String CCEmails{get;set;}
    public String emailAddress {get;set;}
    //public Contact tempCont{get;set;}
    public String pdfType{get;set;}
    //private QuoteDocument doc;
    
    //Arpit : 19-May-2017 : Sprint-3 English-French toggleing
    //Added for setting choosen language
    public String choosenLanguage{get ; set;}
    public transient Attachment attachment;
    public String ErrorMsg {get;set;}
    public String SuccessMsg {get;set;}
    /** Method:OCOM_SaveAndSendOrderPdfHelper(ID orderId)
    This is a paramterized constructor.
    Inputs Args:Id.
    Output Args:None
    **/
    public OC_OrderPdfHelper(ID orderId) //, String PdfDocType
    {
      //  SuccessMsg = 'sucess';
      //  ErrorMsg = 'error';
       // system.debug('OCOM_SaveAndSendOrderPdfHelper::OCOM_SaveAndSenderOrderPdfHelper order id('+orderId+')');
        this.oId = orderId;
        o = [select Id, Name, Status, OrderNumber,ordermgmtid__c,CustomerAuthorizedById,CustomerAuthorizedBy.Email from Order where Id = :oId];
       // system.debug('OCOM_SaveAndSendOrderPdfHelper::OCOM_SaveAndSenderOrderPdfHelper o('+o+')');
        //this.pdfType = PdfDocType;
        emailAddress = o.CustomerAuthorizedBy.Email;
        
    }
    /** Method:OCOM_SaveAndSendOrderPdfHelper(ID orderId, String lang)
    Arpit : 19-May-2017 : Sprint-3 English-French toggleing
    paramterized constructor for English-french toggeling
    Inputs Args:Id,String
    Output Args:None
    **/
    public OC_OrderPdfHelper(ID orderId, String lang) //, String PdfDocType
    {
      //  SuccessMsg = 'sucess';
      //  ErrorMsg = 'error';
        //system.debug('OCOM_SaveAndSendOrderPdfHelper::OCOM_SaveAndSenderOrderPdfHelper order id('+orderId+')');
        this.oId = orderId;
        o = [select Id, Name, Status, OrderNumber,ordermgmtid__c,CustomerAuthorizedById,CustomerAuthorizedBy.Email from Order where Id = :oId];
       // system.debug('OCOM_SaveAndSendOrderPdfHelper::OCOM_SaveAndSenderOrderPdfHelper o('+o+')');
        //this.pdfType = PdfDocType;
        emailAddress = o.CustomerAuthorizedBy.Email;
        //Arpit : 19-May-2017 : Sprint-3 English-French toggleing
        choosenLanguage = lang;
        if( o.Status.equals(Label.Submitted_Label) ||  o.Status.equals(Label.In_Progress_Label) || o.Status.equals(Label.Activated_Label) || o.Status.equals(Label.Pending_Label) || String.isNotBlank(o.ordermgmtid__c)){      
            pdfType =Label.Confirmation_Label;
        }
        else{
            pdfType = Label.Review_Label;
        }
        //system.debug('OCOM_SaveAndSendOrderPdfHelper::choosenLanguage--->' + choosenLanguage );
    }
    /** Method:SavePDF()
    This method is to Save PDF.
    Inputs Args:None
    Output Args:None
    **/ 
    public void SavePDF() 
    {
        
       // system.debug('OCOM_SaveAndSendOrderPdfHelper::SavePDF() starts');
        //if(!Test.isRunningTest() && !validate()){
        //    PdfNotCreatedAlert = 'Please Complete Product Comfiguration before sending Quote';
        //    return; 
        //}
         resetMessages();
        String pdfFRtype;         //17 July 2017 - QC14754 - Sandip
        //if(String.isBlank(pdfType)){
           //system.debug('!@!order '+o);
            if( o.Status.equals(Label.Submitted_Label) ||  o.Status.equals(Label.In_Progress_Label) || o.Status.equals(Label.Activated_Label) || o.Status.equals(Label.Pending_Label) || String.isNotBlank(o.ordermgmtid__c)){      
                //pdfType =Label.Confirmation_Label;
                pdfFRtype = Label.Confirmation_fr_Label; //17 July 2017 - QC14754 - Sandip
            }
            else{
                //pdfType = Label.Review_Label;
                pdfFRtype = Label.Review_fr_Label; //17 July 2017 - QC14754 - Sandip
            }
        //}
        String seatchStr = 'OR%-'+pdfType+'-V%';
        String searchFrStr = 'OR%-'+pdfFRtype +'-V%'; //17 July 2017 - QC14754 - Sandip
        //17 July 2017 - QC14754 - Sandip
         List<Attachment> lisAtt = [select Name 
                                    from Attachment 
                                    where parentId = :o.id and (name like :seatchStr OR name like :searchFrStr)
                                    order by CreatedDate desc];
        // End - QC14754
       // system.debug('OCOM_SaveAndSendOrderPdfHelper::SavePDF() lisAtt('+lisAtt+')');
        Integer versionNum = 1;
        if(lisAtt.size() >0){
            String n = lisAtt[0].name;
            String vNumString = n.substring(n.length()-5, n.length()-4);
            try{
                Integer vNum = Integer.valueOf(vNumString);
                if(vNum !=null){
                    versionNum = vNum + 1;
                   // system.debug('OCOM_SaveAndSendOrderPdfHelper::SavePDF() versionNum('+versionNum+')');
                }
            } catch(Exception e){}
            
        }

        PageReference pr = Page.OC_OrderPdf;
        
        pr.getParameters().put('id', o.id);
        //Arpit: 24-May-2017 : Buy1 - Sprint-3EN-FR Toggleing of PDF
        //Setting pageref on the basis of choosen language
         pr.getParameters().put('lang', choosenLanguage );
         pr.getParameters().put('pdfType', pdfType);
        
        // system.debug('OCOM_SaveAndSendOrderPdfHelper::SavePDF() page reference('+pr+')');
      
         Blob cont;
         if(!Test.isRunningTest()){
            //system.debug('OCOM_SaveAndSendOrderPdfHelper::SavePDF() getContentAsPDF() start');
            try {
                 cont = pr.getContentAsPDF();
            } catch (exception  anException) {//system.debug('OCOM_SaveAndSendOrderPdfHelper::SavePDF() exception('+anException+')');
            }
          //  system.debug('OCOM_SaveAndSendOrderPdfHelper::SavePDF() getContentAsPDF() end');
         }else {
            cont =Blob.valueOf('FOR UNIT TEST');
         }
         
         //17 July 2017 - QC14754 - Sandip
         String fileName = pdfType;
         if(choosenLanguage == Label.fr_Label){
             fileName = pdfFRtype;
         }  
         attachment = new Attachment(parentId = o.id, name=o.OrderNumber+'-'+fileName+'-V'+versionNum + '.pdf', body = cont);
         // End - QC14754
        // system.debug('OCOM_SaveAndSendOrderPdfHelper::SavePDF() attachment('+attachment+')');

         try{
            insert  attachment;
           // system.debug('OCOM_SaveAndSendOrderPdfHelper::SavePDF() successfully inserted attachment');
            //q.Is_PDF_Generated__c = true;
            //update  q;
         }catch (DmlException de){ attachment = null;
         // system.debug('OCOM_SaveAndSendOrderPdfHelper::SavePDF() dmlException('+de+')'); 
         }         
         //SuccessMsg='New version of Quote PDF was successfully saved.';
        
       
    } 

    
    /**
    Method:sendEmail()
    This method is to send Email.
    Inputs Args:None
    Output Args:None
    **/ 
    public void sendEmail()
    {
       // system.debug('OCOM_SaveAndSendOrderPdfHelper::sendEmail() starts');
       // system.debug('OCOM_SaveAndSendOrderPdfHelper, sendPDF()::choosenLanguage--->' + choosenLanguage );
        resetMessages();
        //Arpit : 08-April-2017 : Buy1 Sprint 1 : French Support
        //Aleterd query to add CustomerAuthorizedBy.Language_Preference__c
        o = [select Id, Name, Status, OrderNumber,ordermgmtid__c,CustomerAuthorizedById,CustomerAuthorizedBy.Email , CustomerAuthorizedBy.Language_Preference__c  from Order where Id = :oId];
       system.debug('o--------->>>>>>>>>>>'+o.CustomerAuthorizedBy.Language_Preference__c + o.CustomerAuthorizedById);
        //at order submit page, email address field is blank for PDF
        //if(pdfType != Label.Review_Label && String.isEmpty(emailAddress))
        //    return;
        
        displayModal = false;
        Id emailTemplateId;
        string emailTemplateName;
        map<string,SMB_QuoteEmailTemplates__c> emailTemplates = SMB_QuoteEmailTemplates__c.getAll();
        if(choosenLanguage == null){ 
            if(o.CustomerAuthorizedBy.Language_Preference__c != null  && o.CustomerAuthorizedBy.Language_Preference__c == Label.English_Label ){
                emailTemplateName = emailTemplates.get('Default Quote PDF').TemplateName__c;
                choosenLanguage = Label.en_US_Label;
            }else if(o.CustomerAuthorizedBy.Language_Preference__c != null  && o.CustomerAuthorizedBy.Language_Preference__c == Label.French_Label){
                emailTemplateName = emailTemplates.get('Default Quote PDF (French)').TemplateName__c;
                choosenLanguage = Label.fr_Label;  
            }else{
                emailTemplateName = emailTemplates.get('Default Quote PDF').TemplateName__c;
                choosenLanguage = Label.en_US_Label;
            }
        }else{
            if(choosenLanguage != Label.fr_Label){
                emailTemplateName = emailTemplates.get('Default Quote PDF').TemplateName__c;
            }else{
                emailTemplateName = emailTemplates.get('Default Quote PDF (French)').TemplateName__c;  
            }
        }
                    
        SavePDF(); 
        if(o.CustomerAuthorizedById== null)
            errorMsg = Label.OCOM_Error_sending_email_Order_Contact_is_not_specified;
        else if(attachment == null)
            errorMsg = Label.OCOM_Error_sending_email_Unable_to_create_the_PDF_attachment;

        if(String.IsBlank(errorMsg))
        {
            //String oppBundleType; 
            try
            {
                if(String.IsBlank(emailTemplateName))
                {
                    errorMsg = Label.OCOM_Missing_Email_Template;
                    return;
                }
                
                EmailTemplate emlTemp ;
                if(!Test.isRunningTest()){
                    emlTemp= [Select Id,Subject,body From EmailTemplate e WHERE DeveloperName = :emailTemplateName LIMIT 1].get(0);
                    //system.debug('OCOM_SaveAndSendOrderPdfHelper::sendEmail() emlTemp('+emlTemp+')');
                    emailTemplateId = Id.ValueOf(emlTemp.Id);
                }
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                List<String> lstEmail = new List<String>();
                List<String> lstCCEmail = new List<String>();
                if(String.isNotEmpty(emailAddress)){
                    lstEmail.add(emailAddress);
                }else {
                    errorMsg = Label.OCOM_Email_Address_is_not_specified;
                    return; 
                }
                
                //system.debug('OCOM_SaveAndSendOrderPdfHelper::sendEmail() CCEmails('+CCEmails+')');
                if(String.IsNotEmpty(CCEmails)){
                    lstCCEmail = CCEmails.split(';');
                    if(lstCCEmail.size()>0){
                        mail.setCcAddresses( lstCCEmail);
                    }
                }
                
                mail.setToAddresses(lstEmail);
                mail.setSenderDisplayName(Label.noreply_Label);
                mail.setReplyTo(Label.noreply_Label);
                mail.setTargetObjectId(o.CustomerAuthorizedById);
                mail.setTreatTargetObjectAsRecipient(false);
                //mail.setWhatId(q.OpportunityId);
                
                if(!Test.isRunningTest()){
                    mail.setTemplateId(emailTemplateId);
                }
              //  system.debug('OCOM_SaveAndSendOrderPdfHelper::sendEmail() mail('+mail+')');
                
                Messaging.EmailFileAttachment pdfAttc = new Messaging.EmailFileAttachment();
                pdfAttc.setContentType(Label.application_pdf_Label);
                pdfAttc.setFileName(attachment.name);
                pdfAttc.setBody(attachment.body);
                // set to inline 
                pdfAttc.setInline(true);
               // system.debug('OCOM_SaveAndSendOrderPdfHelper::sendEmail() pdfAttc('+pdfAttc+')');
                
                mail.setFileAttachments(new Messaging.EmailFileAttachment[]{pdfAttc});
                mail.saveAsActivity = false;                               
               // system.debug('OCOM_SaveAndSendOrderPdfHelper::sendEmail() mail('+mail+')');
                if(!Test.isRunningTest()){
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                }
                
                // Set Quote Status to Sent
                //q.status = 'Sent';
                o.Activities__c = Label.Sent_to_Label+lstEmail[0]+Label.on + date.today().format();
                //system.debug('OCOM_SaveAndSendOrderPdfHelper::sendEmail() o('+o+')');
                try{
                    update o;
                   // system.debug('OCOM_SaveAndSendOrderPdfHelper::sendEmail() order updated');
                }catch (DMLException de){errorMsg = Label.OCOM_Not_able_create_an_Activity_record_in_the_Order;
                    //system.debug('OCOM_SaveAndSendOrderPdfHelper::sendEmail() order DMLException('+de+')');
                }
                // create Task as Email
                Task t = new Task (WhatId = o.id, WhoId = o.CustomerAuthorizedById, status=Label.Completed_Label, ActivityDate = Date.Today());//WhoId = o.CustomerAuthorizedById,
                if(!Test.isRunningTest()){
                    t.subject = Label.Email_Label+emlTemp.Subject;
                    
                    if(String.IsNotEmpty(CCEmails)){
                        t.Description = Label.CC_Label+ CCEmails;
                    }
                    //t.Description += '\r\n' +emlTemp.body;
                }
               // system.debug('OCOM_SaveAndSendOrderPdfHelper::sendEmail() t('+t+')');
                try{
                    insert t;
                    //system.debug('OCOM_SaveAndSendOrderPdfHelper::sendEmail() task inserted');
                }catch (DMLException e){/* String msg=''; for (Integer i = 0; i < e.getNumDml(); i++) {msg = msg + e.getDmlMessage(i);}*/errorMsg = Label.OCOM_Error_creating_task+' ' +e.getMessage();
                   // system.debug('OCOM_SaveAndSendOrderPdfHelper::sendEmail() task DMLException('+e+')');
                }
            }catch(Exception e){
                errorMsg = Label.OCOM_Error_in_sending_email +' ' + e.getMessage();
               // system.debug('OCOM_SaveAndSendOrderPdfHelper::sendEmail() Exception('+e+')');
                return;}
            SuccessMsg=Label.OCOM_The_email_was_successfully_sent;
           // system.debug('OCOM_SaveAndSendOrderPdfHelper::sendEmail() end');
        }
                
    }
    
    
   
    /**
    Method:resetMessages()
    This method is to reset Messages.
    Inputs Args:None
    Output Args:None
    **/ 
    private void resetMessages()
    {
       // system.debug('OCOM_SaveAndSendOrderPdfHelper::resetMessages()');
        ErrorMsg=null;
        SuccessMsg = null;
    }
    
    /**
    Method:openModal()
    This method is to open Modal
    Inputs Args:None
    Output Args:None
    **/ 
    public void openModal() 
    {           
        displayModal= true;               
       // system.debug('OCOM_SaveAndSendOrderPdfHelper : choosenLanguage after clicnking on Send Email button----->' + choosenLanguage);
    }  
      
     /**
    Method:hideModal()
    This method is to hide Modal
    Inputs Args:None
    Output Args:None
    **/  
    public void hideModal() 
    {                
        displayModal = false;    
    }

}