global class trac_Forecast_Report_Batch_Insert implements Database.Batchable<Forecast_Report__c>, Database.Stateful {
	
	global final List<Forecast_Report__c> myFs;
	
	global trac_Forecast_Report_Batch_Insert(List<Forecast_Report__c> insReport) {
	
		myFs = insReport;	
	}
	
	global Iterable<Forecast_Report__c> start(Database.batchableContext info){
        // just instantiate the new iterable here and return
        return myFs;
    }
    
    global void execute(Database.BatchableContext BC, List<Forecast_Report__c> scope){
    	
    	insert scope;
    }
    
    global void finish(Database.BatchableContext ctx){
    	
    }    	
}