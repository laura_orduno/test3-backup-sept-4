@isTest
private class TestForSendInfoForContactOrLead{
    static testMethod void testForCodeCoverage(){
       String guid = '50021de2-8ea3-455f-8a1c-a6e3214dc78f';       
       boolean flag = true, assFlag = false; 
       String notValidGuid = 'aas';
       SendInfoForContactOrLead objN=new SendInfoForContactOrLead(); 
       objN.isValidGuid(notValidGuid );    
       System.currentPagereference().getParameters().put('contactId','0034000000kATK4' );
       System.currentPagereference().getParameters().put('leadId',null );
       System.currentPagereference().getParameters().put('uid','dd67cf26-46bb-41e3-b458-e4b6a16aa1e8');   
       SendInfoForContactOrLead obj=new SendInfoForContactOrLead();
       obj.getGuid();
       obj.getErrorFirstName();
       obj.showLeadStatus();
       obj.showContactStatus();
       obj.getRenderLeadBlock();
       obj.getRenderContactBlock();
       obj.doSubmit();
       obj.updateContact();
       //obj.updateLead();       
       obj.isValidGuid(guid);
       //obj.isLeadConverted(flag);
       obj.isMandatoryFieldEmpty(guid);
       System.currentPagereference().getParameters().put('contactId', null );
       System.currentPagereference().getParameters().put('uid', 'f9cfab51-4a4f-403b-952f-4d84d3864756');   
       System.currentPagereference().getParameters().put('leadId', '00Q4000000V6XgTEAV' );
       SendInfoForContactOrLead objL=new SendInfoForContactOrLead();
       objL.getGuid();
       objL.showLeadStatus();
       objL.showContactStatus();
       objL.getRenderLeadBlock();
       objL.getRenderContactBlock();
       objL.doSubmit();
       //objL.updateContact();
       objL.updateLead(); 
       objL.saveLead();
       guid='efe04c8b-3a9a-405f-91ff-d49baad36751';         
       objL.isValidGuid(guid);
       objL.isLeadConverted(flag);
       assFlag = objL.isMandatoryFieldEmpty(guid);
       System.assertEquals(true,assFlag );
    }   
}