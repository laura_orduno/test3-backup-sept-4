public class vlocity_addon_AutoAttachDocCntrl_CP {
    
    public string versionId {get;set;}
    //  public string nameSpacePrefix {get;set;} 
    //public string documentName {get;set;} 
    // List<Attachment> attachments{get;set;}
    string updateParam = '';
    public id contractID{get;set;}
    public static vlocity_cmt__ContractVersion__c activeContVersion {get;set;}
    public list<vlocity_cmt__ContractVersion__c> conVersion = new list<vlocity_cmt__ContractVersion__c>();
    public boolean isUpdate { get;set;}
    
    
    
    public boolean isInConsole {
        get{
            Map<String, String> parametersMap = ApexPages.currentPage().getParameters();   
            for(String key : parametersMap.keySet()){
                if(key.trim().equalsIgnoreCase('isInConsole')){
                    String value=parametersMap.get(key);
                    if(value.trim().equals('true')){
                        return true;
                    }
                    
                    else if(value.trim().equals('false')){
                        return false;
                    }
                    
                }
            }
            
            return false;
        }
        set;
    }
    
    public vlocity_addon_AutoAttachDocCntrl_CP(vlocity_cmt.ContractDocumentCreationController con){ 
        contractID = ApexPages.currentPage().getParameters().get('id');
        updateParam = ApexPages.currentPage().getParameters().get('isUpdate');
        if(updateParam != null && updateParam.trim().equalsIgnoreCase('true'))
            isUpdate = true;
        else 
            isUpdate = false;
        
        system.debug('isUpdate'+ isUpdate);
        
    }
    
    public void attachDoc(){
        List<Contract> contractNewList = new List<Contract> ();
        //Create new Version if the request is update
        system.debug('isInConsole::'+isInConsole);
        versionId = '';
        try{
            if(contractID != null) {
                try{
                    //Call Method to clone related objects 
                    contractNewList=ContractCreateRelatedObj.createRelatedObject(isUpdate,contractID);     
                    //Call Method to Update Contract Line Item Record Type
                    ContractCreateRelatedObj.updateConLineItem(contractID);
              
                }catch(exception e){
                    throw (e);
                }
            }
        if(isUpdate == true && contractID != null && contractNewList.size() > 0 && contractNewList[0].status == 'Draft' ){    
            versionId = vlocity_cmt.ContractServiceResource.createNewContractVersionDocument(contractID);
           List<vlocity_cmt__ContractSection__c> oldList = new List<vlocity_cmt__ContractSection__c>();
                List<vlocity_cmt__ContractVersion__c> oldTemplate = new List<vlocity_cmt__ContractVersion__c>();
                for (vlocity_cmt__ContractVersion__c conVersions: [Select Id,vlocity_cmt__ContractId__r.Contract_Type__c, vlocity_cmt__DocumentTemplateId__c,
                                                                   (select id from vlocity_cmt__Contract_Sections__r) 
                                                                   from vlocity_cmt__ContractVersion__c 
                                                                   where Id = :versionId limit 1]){
                                                                       oldList.addAll(conVersions.vlocity_cmt__Contract_Sections__r);
                                                                       oldTemplate.add(conVersions);                                           
                                                                   }

                if( oldTemplate.size() >0 &&  !oldTemplate.isEmpty()){
                    oldTemplate[0].vlocity_cmt__DocumentTemplateId__c=null;
                    delete oldList;
                    update oldTemplate[0];
                    activeContVersion = oldTemplate[0];
                }
                System.debug('New Version ID:' + versionId);
        }else if(contractID != null && contractNewList.size() > 0 && contractNewList[0].status == 'Draft') {
            // Attach a Template to the esixting Version and then attach a Doc.
            conVersion = [Select ID,vlocity_cmt__ContractId__r.Contract_Type__c   from vlocity_cmt__ContractVersion__c where vlocity_cmt__ContractId__c = :contractID 
                          and vlocity_cmt__Status__c = 'Active' ]; 
            if(conVersion.size()>0 && !conVersion.isEmpty() ) {
                activeContVersion = conVersion[0];
                versionId = conVersion[0].id;
                System.debug('Version ID from Managed Package:' + versionId);
            }
        }
                if(activeContVersion != null && activeContVersion.vlocity_cmt__ContractId__r.Contract_Type__c != null   ){
                    String documentTemplateName = '';
                    // get Template name based on Contract Type
                    documentTemplateName = getDocumentTemplateName (activeContVersion.vlocity_cmt__ContractId__r.Contract_Type__c);
                    if(documentTemplateName != null){
                        Map<String, String> objNameMap = new Map<String, String> ();
                        Id documentTemplateId = getTemplateId(documentTemplateName, 'Contract');
                        // Attach template to the Contract Version
                        vlocity_cmt.ContractDocumentDisplayController.createContractSections(documentTemplateId, activeContVersion.id); 
                    }                    
                }
            }catch(exception e){
                System.debug(LoggingLevel.ERROR, 'Exception is '+e);
                System.debug(LoggingLevel.ERROR, 'Exception stack trace '+e.getStackTraceString());
            }
       }

    // Get Template name fromt the custom settings
    private String getDocumentTemplateName (String ContractType){
        
        Set_Doc_Template__c docTemplate =  Set_Doc_Template__c.getInstance(ContractType);
        
        if(docTemplate!=null){
            String documentTemplateName = docTemplate.DocumentTemplateName__c; 
            return documentTemplateName;
        }
        return null;
    }
    
    //get active template ID for the contract 
    private Id getTemplateId(String documentTemplateName, String objTypeName){
        
        List<vlocity_cmt__DocumentTemplate__c> templateIds = [Select Id from vlocity_cmt__DocumentTemplate__c 
                                                              where Name=:documentTemplateName And vlocity_cmt__IsActive__c=true 
                                                              AND vlocity_cmt__ApplicableTypes__c INCLUDES (:objTypeName)];
        
        if(templateIds !=null && templateIds.size()>0){
            return templateIds[0].Id;
        }
        else{
            String message = Label.vlocity_cmt.PDF_NoTemplateId;
            throw new NoTemplateIdException(message);
        }
        return null;    
    }
    
    
    
    
    
    
    //Main Method
    

    
    
    
    
    
    
    public class NoTemplateIdException extends Exception{}    
}