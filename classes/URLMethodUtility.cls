/**
* Determines the true API hostname for a Salesforce org using the Identity API.
* eg 'https://pod.salesforce.com' (most orgs)
* eg 'https://custom.my.salesforce.com' (my domain)
* eg 'https://custom--dev.pod.my.salesforce.com' (sandbox orgs)
*/

global class URLMethodUtility{
    
    webservice static string getExternalForm(){
        String externalFormUrl = Url.getSalesforceBaseURL().toExternalForm();
        return externalFormUrl;
    }
   
    webservice static URL getBaseUrl(){
        URL baseUrl = Url.getSalesforceBaseURL();
        return baseUrl;
    }
    
    webservice static string getHost(){
        String host = Url.getSalesforceBaseURL().getHost();
        return host;
    }
    
    webservice static string getInstanceName(){
        String instanceName = [select InstanceName from Organization limit 1].InstanceName;
        return instanceName;
    }
    
      
    /* please use this method very carefully, might need to change for production instance */
    webservice static string getDomainName(){
        String domainName;
        String host = System.URL.getSalesforceBaseUrl().getHost();
        //String host = 'telus.cs19.my.salesforce.com/home/home.jsp';
        List<String> parts = host.replace('-api','').split('--');
        if (parts.size() == 3) domainName = parts[0]+'--'+parts[1];
        else if (parts.size() == 2){
             Integer index = parts[1].indexOf('.');
             domainName = parts[0] + '--' +parts[1].subString(0,index);
        }else if(parts.size() == 1){
            Integer index = parts[0].indexOf('.');
            domainName = parts[0].subString(0,index);
        }else domainName = null;
        
        return domainName;
    }
}