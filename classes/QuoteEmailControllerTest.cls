@isTest
private class QuoteEmailControllerTest {
    @isTest(SeeAllData=true)
    private static void testQuoteEmailController() {
        SBQQ__Quote__c quote = [Select Id, SBQQ__Opportunity__r.Web_Account__c From SBQQ__Quote__c Where Number_of_quote_lines__c > 6 and Completed__c = true Order By CreatedDate DESC Limit 1];
        system.assertnotequals(null, quote);
        PageReference pref = new PageReference('/'+quote.id);
        pref.setRedirect(true);
        ApexPages.currentPage().getParameters().put('retURL', '/' + quote.id);
        ApexPages.currentPage().getParameters().put('qids', quote.id + ','); 
        ApexPages.currentPage().getParameters().put('aid', quote.SBQQ__Opportunity__r.Web_Account__c);
        
        QuoteEmailController qec = new QuoteEmailController();
        System.assertNotEquals(null, qec.onCancel());
        System.assertNotEquals(null, qec.onContinue());
        Contact c = null;
        for (QuoteEmailController.ContactSelector cs : qec.contacts) {
            cs.selected = true;
            cs.contact.record.Email = 'test@example.com';
            c = cs.contact.record;
        }
        if (c != null)
            update c;
            qec.onSend();
    }
    
    testMethod static void testContactSelector() {
        ContactVO cvo = new ContactVO(new Contact());
        QuoteEmailController.ContactSelector cs = new QuoteEmailController.ContactSelector(cvo);
        System.assertEquals(cvo, cs.contact);
        System.assertEquals(false, cs.selected);
        cs.selected = true;
        System.assertEquals(true, cs.selected);
    }
}