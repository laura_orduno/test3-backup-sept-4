public without sharing class QuoteBundleCommitmentClient {
    public class QuoteBundleCommitmentClientException extends Exception {}
    
    Id quoteId {get; set;}
    
    SBQQ__Quote__c quote {get; set;}
    Web_Account__c account {get; set;}
    Address__c installlocation {get; set;}
    Contact contact {get; set;}
    SBQQ__QuoteLine__c[] quotelines {get; set;}
    Map<String, Integer> offerCount {get; set;}
    
    svc_BundleCommitmentCommonType_v1.CustomerInfo customer {get; set;}
    svc_BundleCommitmentCommonType_v1.WirelessOfferReference[] offerRefList {get; set;}
    String bundleName {get; set;}
    public Boolean success {get; private set;}
    svc_BundleCommitmentExtSvcRequestRespons.CreateBundleCommitmentResponse response {get; set;}
    
    public QuoteBundleCommitmentClient(Id qid) {
        quoteId = qid;
    }
    
    public void send() {
    try {
        // Load quote
        loadQuote();
        // Load web account
        loadAccount();
        // Load install location
        loadInstallLocation();
        // Load contact info
        loadContact();
        // Load quote lines
        loadQuoteLines();
        // Accumulate offers
        buildOfferCount();
        
        // Quit if no offers found
        if (offerCount.size() == 0) {
            return;
        }
        
        // Create wrapped objects
        customer = buildCustomer();
        offerRefList = buildOfferReferences();
        // Send request
        success = sendRequest();
        // Update quote record if successful
        updateQuote();
    } catch (Exception e) {
       e.setMessage(e.getMessage().left(254));
        QuoteUtilities.logNow(e, false);
    }
    }
    
    void loadQuote() {
        quote = [
            Select Name, Install_Location__c, SBQQ__Opportunity__r.Web_Account__c, SBQQ__PrimaryContact__c,
                     SBQQ__Opportunity__r.Web_Account__r.Id, SBQQ__Opportunity__r.Web_Account__r.BAN__c,
                     SBQQ__Opportunity__r.Web_Account__r.Name, SBQQ__Opportunity__r.Web_Account__r.Company_Legal_Name__c,
                     SBQQ__PrimaryContact__r.FirstName, SBQQ__PrimaryContact__r.LastName
         From SBQQ__Quote__c
         Where Id = :quoteId
       ];
    }
    
    void loadAccount() {
        account = quote.SBQQ__Opportunity__r.Web_Account__r;
    }
    
    void loadInstallLocation() {
        installlocation = QuoteUtilities.loadAddressById(quote.Install_Location__c);
    }
    
    void loadContact() {
        contact = quote.SBQQ__PrimaryContact__r;
    }
    
    void loadQuoteLines() {
        quotelines = [
            Select SBQQ__Quote__c, SBQQ__Product__r.RWMS_Bundle_Name__c, SBQQ__Product__c, SBQQ__Product__r.Name,
                         SBQQ__Product__r.Wireless_Offer_Code__c, SBQQ__Quantity__c
            From SBQQ__QuoteLine__c
            Where SBQQ__Quote__c = :quote.Id
       ];
    }
    
    void buildOfferCount() {
        offerCount = new Map<String, Integer>();
        for (SBQQ__QuoteLine__c line : quotelines) {
            if (line.SBQQ__Product__r.Name.contains('Bundle')) {
                bundleName = line.SBQQ__Product__r.RWMS_Bundle_Name__c;
            }
            if (line.SBQQ__Product__r == null || line.SBQQ__Product__r.Wireless_Offer_Code__c == null) {
                continue;
            }
            incrementOffer(line.SBQQ__Product__r.Wireless_Offer_Code__c, Integer.valueOf(line.SBQQ__Quantity__c));
        }
    }
    void incrementOffer(String wirelessOfferCode, Integer quantity) {
        if (!offerCount.containsKey(wirelessOfferCode)) {
            offerCount.put(wirelessOfferCode, 0);
        }
        Integer count = offerCount.get(wirelessOfferCode);
        if (quantity == null) {
            quantity = 0;
        }
        count += quantity;
        offerCount.put(wirelessOfferCode, count);
    }
    
    svc_BundleCommitmentCommonType_v1.CustomerInfo buildCustomer() {
        svc_BundleCommitmentCommonType_v1.CustomerInfo customerInfo = new svc_BundleCommitmentCommonType_v1.CustomerInfo();
        customerInfo.companyName = account.Name;
        customerInfo.customer = new svc_EnterpriseCommonName_v1.IndividualName();
        customerInfo.customer.firstName = contact.FirstName;
        customerInfo.customer.lastName = contact.LastName;

         //Suite_Unit__c, Street__c, Street_Direction__c, Street_Type__c, City__c, Postal_Code__c, State_Province__c, Country__c 
        customerInfo.address = new svc_TelusCommonAddressTypes_v5.Address();
        customerInfo.address.streetName = installlocation.Street__c;
        customerInfo.address.streetNumber = installlocation.Street_Number__c;
        customerInfo.address.unitName = installlocation.Suite_Unit__c;
        customerInfo.address.municipalityName = installlocation.City__c;
        customerInfo.address.provinceStateCode = installlocation.State_Province__c;
        customerInfo.address.postalZipCode = installlocation.Postal_Code__c;
        customerInfo.address.countryCode = installlocation.Country__c;
        
        return customerInfo;
    }
    
    svc_BundleCommitmentCommonType_v1.WirelessOfferReference[] buildOfferReferences() {
        svc_BundleCommitmentCommonType_v1.WirelessOfferReference[] refs = new List<svc_BundleCommitmentCommonType_v1.WirelessOfferReference>();
        for (String code : offerCount.keySet()) {
            svc_BundleCommitmentCommonType_v1.WirelessOfferReference wor = new svc_BundleCommitmentCommonType_v1.WirelessOfferReference();
            wor.wirelessOfferCode = code;
            wor.originalOffersAvailableNum = offerCount.get(code);
            refs.add(wor);
        }
        return refs;
    }
    
    boolean sendRequest() {
            Quote_Portal__c qp = Quote_Portal__c.getInstance();
            if (qp == null || !qp.Send_To_RWMS__c || qp.RWMS_Endpoint__c == null || qp.RWMS_Credentials__c == null) {
                return false;
            }
            svc_BundleCommitmentExtSvc_1.BundleCommitmentExtSvcPort port = new svc_BundleCommitmentExtSvc_1.BundleCommitmentExtSvcPort();
            port.endpoint_x = qp.RWMS_Endpoint__c;
            //port.endpoint_x = 'http://requestb.in/1auyxf51';
            port.lenientParsing_x = true;
            
            Blob headerValue = Blob.valueOf(qp.RWMS_Credentials__c);   
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                    
            port.inputHttpHeaders_x = new Map<String,String>();
            port.inputHttpHeaders_x.put('Authorization', authorizationHeader);
            
            if(!Test.isRunningTest()) {
                response = port.createBundleCommitment(quote.Name, bundleName, account.BAN__c, customer, offerRefList);
                if (response.errorCode != null && response.errorCode != '0') {
                    throw new QuoteBundleCommitmentClientException('Quote #: ' +quote.Name+ ' Code: ' + response.errorCode + '; Message Type: ' + response.messageType + '; Context Data: ' + response.contextData);
                }
                system.debug(response);
            }
        return (response != null && response.errorCode != null && response.errorCode == '0');
    }
    
    void updateQuote() {
        quote.RWMS_Called__c = (success != null && success);
        quote.RWMS_Callout_Timestamp__c = Datetime.now();
        update quote;
    }
    
    @future(callout=true)
    public static void sendCommitmentMessage(Id qid) {
        QuoteBundleCommitmentClient qbcc = new QuoteBundleCommitmentClient(qid);
        qbcc.send();
    }
    
}