@isTest
public class CalculateBusinessHoursAgesTest {
    public static testMethod void testBusinessHoursBucketer() {
        List<Stop_Status__c> stopStatus = new List<Stop_Status__c>();
        stopStatus.add( new Stop_Status__c(Name = 'Waiting on Telus', Waiting_For__c = 'Telus') );
        stopStatus.add( new Stop_Status__c(Name = 'Waiting on Customer', Waiting_For__c = 'Customer') ); 
        stopStatus.add( new Stop_Status__c(Name = 'Waiting on Vendor', Waiting_For__c = 'Vendor') ); 
        insert stopStatus;

        RecordType rt = [select Id, DeveloperName from RecordType where SobjectType = 'Case' and DeveloperName = 'Generic' limit 1];
        
        Case c = new Case();
        c.RecordTypeId = rt.Id;
        c.Status = 'Waiting on Vendor';
        c.Last_Status_Change__c = System.Now();
        insert c;

        c.Status = 'Waiting on Telus';
        update c;

        c.Status = 'Waiting on Customer';
        update c;

        c.Status = 'Waiting on Telus';
        update c;

        c.Status = 'Waiting on Vendor';
        update c;

		Case updatedCase = [select Time_With_Customer__c,Time_With_Support__c,Case_Age_In_Business_Hours__c,Time_With_Vendor__c from Case where Id=:c.Id];
		System.assert(updatedCase.Time_With_Customer__c!=null);
        System.assert(updatedCase.Time_With_Support__c!=null);
        System.assert(updatedCase.Time_With_Vendor__c!=null);
        System.assert(updatedCase.Case_Age_In_Business_Hours__c==null);

        c.Status = 'Closed';
        update c;

        updatedCase = [select Time_With_Customer__c,Time_With_Support__c,Case_Age_In_Business_Hours__c,Time_With_Vendor__c from Case where Id=:c.Id];
/*****
        System.assert(updatedCase.Time_With_Customer__c!=null);
        System.assert(updatedCase.Time_With_Support__c!=null);
        System.assert(updatedCase.Time_With_Vendor__c!=null);
        System.assert(updatedCase.Case_Age_In_Business_Hours__c!=null);
*/
    }
}