/**
 * trac_UserLangMgmtTest.cls - Customer Central Language Group Management
 * @description Tests for trac_UserLangMgmt class
 *
 * @author Grant Adamson, Traction on Demand
 * @date 2013-07-29
 */

@isTest(SeeAllData=true) // required for portal role hierarchy/accounts
private class trac_UserLangMgmtTest {
    private static final Id PROFILE_ID = [SELECT Id FROM Profile WHERE Name = 'Telus Partner User 2' LIMIT 1].Id;
    //private static final Id USER_ROLE_ID = [SELECT Id FROM UserRole WHERE Name = 'Dealer Tom Harris AB Partner User' LIMIT 1].Id;
    private static final Id USER_ROLE_ID = [SELECT Id FROM UserRole WHERE Name = 'Dealer - APEX BC Partner User' LIMIT 1].Id;
    //private static final Id OWNER_ACCOUNT_ID = [SELECT Id FROM Account WHERE Name = 'Dealer Tom Harris AB' LIMIT 1].id;
    private static final Id OWNER_ACCOUNT_ID = [SELECT Id FROM Account WHERE Name = 'Dealer - APEX BC' LIMIT 1].id;
    //private static final Id OWNER_ACCOUNT_ID2 = [SELECT Id FROM Account WHERE Name = 'Dealer Tom Harris BC' LIMIT 1].id;
    
    
    private static testMethod void testLanguageChange() {     
        
        User testUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(testUser){
          
            Group enGroup = trac_TestUtils.getGroup('Test - EN', 'Regular'); //new Group(name = 'Test - EN', Type = 'Regular');
            Group frGroup = trac_TestUtils.getGroup('Test - FR', 'Regular');
            List<Group> groupList = new List<Group>() ;//Group[]{ enGroup, frGroup };
            groupList.add(enGroup);
            groupList.add(frGroup);
            insert groupList; 
            
            Language_Groups__c lg = trac_TestUtils.createLanguageGroup('Test'); 
            
            UserRole_Groups__c urg = trac_TestUtils.createUserRoleGroups('test');
            
            Contact c = new Contact(lastname = 'Testing', accountId = OWNER_ACCOUNT_ID);
            insert c;
            
            Contact c2 = new Contact(lastname = 'Testing', accountId = OWNER_ACCOUNT_ID);
            insert c2;
            
            User u = new User(
                alias = 'u1',
                email='u1@testorg.com',
                emailencodingkey='UTF-8',
                lastname='Testing',
                languagelocalekey='en_US',
                localesidkey='en_US',
                profileid = PROFILE_ID,
                userroleid = USER_ROLE_ID,
                contactId = c.id,
                country='United States',
                CommunityNickname = 'u1',
                timezonesidkey='America/Los_Angeles',
                username='u1111@testorg.com'
            );
            insert u;
    
            User frUser = u.clone(true, false, false, false); // preserve id
            frUser.LanguageLocaleKey = 'fr';
            frUser.LocaleSidKey = 'fr_CA';
            
            update frUser;
        
            trac_UserLangMgmt langMgmt = new trac_UserLangMgmt();
        
        
            // change user language to french
            langMgmt.updateUserLangGroups(new User[]{ frUser }, new User[]{ u });
            //langMgmt.updateUserLangGroups();
            
            List<GroupMember> groupMembers = [
                SELECT GroupId, UserOrGroupId
                FROM GroupMember
                WHERE GroupId = :frGroup.id
                OR GroupId = :enGroup.id
            ];
            
            for(GroupMember gm : groupMembers) {
                if(frGroup.id == gm.groupId) {
                    system.assertEquals(frGroup.id, gm.groupId, 'GroupMember entry should reference the French group');
                    if(u.id == gm.UserOrGroupId) {
                        system.assertEquals(u.id, gm.UserOrGroupId, 'GroupMember entry should reference the correct user');
                    }
                }
                
            }
            //system.assertEquals(1, groupMembers.size());
            //system.assertEquals(frGroup.id, groupMembers[0].groupId, 'GroupMember entry should reference the French group');
            //system.assertEquals(u.id, groupMembers[0].UserOrGroupId, 'GroupMember entry should reference the correct user');
            
            // flip back to english
            langMgmt.updateUserLangGroups(new User[]{ u }, new User[]{ frUser });
            //langMgmt.updateUserLangGroups();
            groupMembers = [
                SELECT GroupId, UserOrGroupId
                FROM GroupMember
                WHERE GroupId = :frGroup.id
                   OR GroupId = :enGroup.id
            ];
            //system.assertEquals(1, groupMembers.size());
            
            for(GroupMember gm : groupMembers) {
                if(enGroup.id == gm.groupId) {
                    system.assertEquals(enGroup.id, gm.groupId, 'GroupMember entry should reference the French group');
                    if(u.id == gm.UserOrGroupId) {
                        system.assertEquals(u.id, gm.UserOrGroupId, 'GroupMember entry should reference the correct user');
                    }
                }
                
            }
            
            //system.assertEquals(enGroup.id, groupMembers[0].groupId, 'GroupMember entry should reference the English group');
            //system.assertEquals(u.id, groupMembers[0].UserOrGroupId, 'GroupMember entry should reference the correct user');
            
            
        }
    }
        
    
        
        

}