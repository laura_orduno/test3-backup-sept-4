global with sharing class InitiateLeadUpdateBatchService implements Messaging.InboundEmailHandler {

    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        String userIdsAsString = email.plainTextBody;
        invoke(userIdsAsString);
        return result;
    }

	public void invoke(String userIdsAsString) {	//Messaging.InboundEmail
		System.debug('---Before userIdsAsString = ' + userIdsAsString);
		if (userIdsAsString != null && userIdsAsString.indexOf('^') != -1 && userIdsAsString.indexOf('$') != -1) {
			userIdsAsString = userIdsAsString.substring(userIdsAsString.indexOf('$') + 1, userIdsAsString.indexOf('^'));
		}
		System.debug('---After userIdsAsString = ' + userIdsAsString);
		Set<id> userIds = new Set<Id>();
		for(String s : userIdsAsString.split(',')){
			userIds.add((Id) s);
		}
        List<User> users = new List<User>();
		//invoke lead update batch
		SetLeadManagerBatch obj = new SetLeadManagerBatch(); 
		obj.query = 'select id, manager_1__c, manager_2__c, manager_3__c, manager_4__c, ownerid from Lead where isConverted = false and ownerid in '+ RoleHierarchy.buildInExpression(userIds);
        Database.executeBatch(obj);
		
	}  

	static testmethod void testEmailHandler(){
		// Create a new email and envelope object
	   Messaging.InboundEmail email = new Messaging.InboundEmail();
	   Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
	 
	// Create the plainTextBody and fromAddres for the test
	    email.plainTextBody = userinfo.getuserid();
	    //email.fromAddress ='rmencke@salesforce.com';
	 
		InitiateLeadUpdateBatchService obj = new InitiateLeadUpdateBatchService();
		obj.handleInboundEmail(email, env);
	} 
}