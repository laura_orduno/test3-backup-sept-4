public virtual with sharing class VITILcareAuthCheckCtlr extends VisualforcePageController{
         
        public String SFDCAccountId {get; set;}
        public String UserLangLocale {get; set;}
     Public String CPRole {get; set;}
           public string userLanguage {get; set;}
     public PageReference initCheck() {
        if (Userinfo.getUserType().equals('Guest')) {
            String startUrl = ApexPages.currentPage().getUrl();
            PageReference pageRef = Page.VITILcareLogin;
            //pageRef.getParameters().put('startUrl', startUrl.replace('/apex/',''));
             pageRef.getParameters().put('startUrl', startUrl.replace('/',''));
            pageRef.setRedirect(true);
            return pageRef;
        }
 getLanguageLocaleKey();
         system.debug('UserLanguage: ' + userLanguage);
        String userAccountId;
         String langLocale;
           
         //String accountIDNum;
        //if(UserUtil.CurrentUser.AccountId!=null) userAccountId = (String)UserUtil.CurrentUser.AccountId;
        // UserUtil is extremely inefficient; better just to get account ID straight from database
        List<User> users = [SELECT Id, AccountId, LanguageLocaleKey, Customer_Portal_Role__c FROM User WHERE Id = :UserInfo.getUserId()];
        if (users.size() > 0) {
            userAccountId = users[0].AccountId;
            langLocale = users[0].LanguageLocaleKey;
            SFDCAccountId = userAccountId;
            UserLangLocale = langLocale;
            CPRole = users[0].Customer_Portal_Role__c;
        }
        if (userAccountId == null || userAccountId == '' || userAccountId.substring(0, 3) != '001') { 
            PageReference pageRef = Page.MBRNonPortalUsersError;
            pageRef.setRedirect(true);
            return pageRef;
            
        }
        return null;
    }
    public String getLanguageLocaleKey() {
        //string userLanguage;
    if(userLanguage == null) {
        userLanguage = [SELECT LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()].LanguageLocaleKey;
    }
    return userLanguage;
}
    public string getTypeofCust() {
         String userAccountId;
         userAccountId = [SELECT AccountId FROM User WHERE Id = :UserInfo.getUserId()].AccountId;

    /*if(userLanguage == null) {
        userLanguage = [SELECT LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()].LanguageLocaleKey;
    }*/
         //String lang1 = ApexPages.currentPage().getParameters().get('lang');
         string Chat_Assign_group;
        
        List<account> Accounts = [SELECT Strategic__c, ID from Account where ID =:userAccountId];
            if (Accounts[0].Strategic__c == true) {
      
        if(userLanguage == 'en_US'){
           
            Chat_Assign_group = '111'; //English/CCL NCSC
        }
        if(userLanguage == 'fr'){
            
            Chat_Assign_group = '211'; //French/CCL NCSC
            
        }
      
                 } else
                 {
                   if(userLanguage == 'en_US'){
           
            Chat_Assign_group = '110'; //English NCSC
        }
        if(userLanguage == 'fr'){
            
            Chat_Assign_group = '210'; //French NCSC
            
        }   
                 }
         return Chat_Assign_group;
    }
}