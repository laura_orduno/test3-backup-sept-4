global class ContractSubscriberEventSchedulable implements Schedulable {
    global void execute(SchedulableContext sc) {
        Database.executeBatch(new ContractSubscriberEventBatch());
    }
}