global class smb_nullAccountField implements Database.Batchable<sObject>
{
	global final String query;
	
	global smb_nullAccountField(String q)
	{
		query = q;
    }

	/*
    * The start method is called at the beginning of a batch Apex job. Use the start method to collect the records or objects to be passed to the interface method execute. 
    * This method returns either a Database.QueryLocator object or an iterable that contains the records or objects being passed into the job.
    */ 
   	global Database.QueryLocator start(Database.BatchableContext BC)
   	{
      	return Database.getQueryLocator(query);
   	}

	/*
    *   The execute method is called for each batch of records passed to the method. Use this method to do all required processing for each chunk of data.
    *   @praram:- A reference to the Database.BatchableContext object.
    *   @param :- A list of sObjects, such as List<sObject>, or a list of parameterized types. If you are using a Database.QueryLocator, the returned list should be used.
    */
   	global void execute(Database.BatchableContext BC, List<sObject> scope)
   	{
   		List<Account> updatedAccts = new List<Account>();
   		for (Account acct : (Account[]) scope)
   		{
   			acct.CustProfId__c = null;
   			updatedAccts.add(acct);
   		}
   		
   		if (updatedAccts.size() > 0)
   		{
   			update updatedAccts;
   		}
   	}

	 /*
     * The finish method is called after all batches are processed.
     */
    global void finish(Database.BatchableContext BC)
    {

    }
}