/********************************************************************************************
* Class Name : ESRInboundService
* Purpose    : This is webservice class which extends Salesforce APIs to ESR applicaton
*    
* 
* Modification History 
*-------------------------------------------------------------------------------------------
* Author                         Date                Version             Remarks
*-------------------------------------------------------------------------------------------
* Tech Mahindra(Balasaheb)       May/06/2016          1.0                 Initial Creation
* 
*
**********************************************************************************************/
global class ESRInboundService {

    public static Id errorLogFailureRecordTypeId{get;set;}
    public static Id errorLogSuccessRecordTypeId{get;set;}
    
    
    static{
        /*Code added by Swapnil Kamble[Tech M] for capturing Error and success logs in the 'Webservice Integration Error Log' object 
         *Date: 7 July 2016
         */
        errorLogFailureRecordTypeId =Schema.SObjectType.Webservice_Integration_Error_Log__c.getRecordTypeInfosByName().get('Failure') != NULL ?Schema.SObjectType.Webservice_Integration_Error_Log__c.getRecordTypeInfosByName().get('Failure').getRecordTypeId() : null;
        
        errorLogSuccessRecordTypeId =Schema.SObjectType.Webservice_Integration_Error_Log__c.getRecordTypeInfosByName().get('Success') != NULL ?Schema.SObjectType.Webservice_Integration_Error_Log__c.getRecordTypeInfosByName().get('Success').getRecordTypeId() : null;
    }
    
    /**
updateRequest(ESRServiceRequest esrRequest) -
**/
    webservice static ESRInboundServiceResponse updateRequest(ESRServiceRequest esrRequest){
        return  updateSFServiceRequest(esrRequest);
    }
    
    /**
updateSFServiceRequest(ESRServiceRequest esrRequest) - 
**/
    public static ESRInboundServiceResponse updateSFServiceRequest(ESRServiceRequest esrRequest)
    {
        try
        {
            List<SRS_PODS_Answer__c> templateAnswersLst = new List<SRS_PODS_Answer__c>();
            List<SRS_PODS_Answer__c> updateTemplateAnsLst = new List<SRS_PODS_Answer__c>();           
            List<Service_Request__c> lstSR;
            
            system.debug('$$$$ esrRequest'+esrRequest);
            //pull Service Request record list for the given id
            if(esrRequest != null && esrRequest.salesForceId != null){
                lstSR = [SELECT Id, 
                         Name, 
                         Opportunity_Product_Item__c, 
                         SRS_PODS_Product__c, 
                         Opportunity__r.Sales_Opportunity_Id__c,
                         (SELECT Id, ESR_Id__c, Version_Status__c FROM Enhanced_Tracking__r WHERE Version_Status__c =:'In Progress')
                         FROM Service_Request__c 
                         WHERE name =: esrRequest.salesForceId
                         AND (Service_Request_Status__c != 'Cancelled' OR Service_Request_Status__c != 'Completed')];
                System.debug('$$$$ lstSR = '+lstSR);
                
                //Null check for record presence in Salesforce
                if(lstSR != null && lstSR.size() > 0 && String.isNotBlank(lstSR[0].SRS_PODS_Product__c)){
                    //Get all the questions saved for service request Edit Template 
                    templateAnswersLst  =   [SELECT Id, 
                                             SRS_PODS_Products__r.QUESTION_EN__c, 
                                             SRS_PODS_Answer__c, SRS_PODS_Products__r.CHILD__c, 
                                             SRS_PODS_Products__r.CHILD_OF__c, 
                                             SRS_PODS_Products__r.SYSTEM_GENERATED_QUESTION_ID__c
                                             FROM SRS_PODS_Answer__c 
                                             WHERE Service_Request__c =: lstSR[0].Id 
                                             AND SRS_PODS_Answer__c != ''
                                             AND SRS_PODS_Products__r.SRS_PODS_Product_Master__c =: lstSR[0].SRS_PODS_Product__c];
                    
                    system.debug('$$$$ templateAnswersLst = '+templateAnswersLst);
                    
                    //get all the questions for proudct
                    List<SRS_PODS_Template__c> templateQuestionsList =[SELECT  VALID_ANSWER__c,                                                   
                                                                       SYSTEM_GENERATED_QUESTION_ID__c,                                                    
                                                                       QUESTION_EN__c,                                                   
                                                                       CHILD__c, 
                                                                       CHILD_OF__c, 
                                                                       ANSWER_ID__c
                                                                       FROM SRS_PODS_Template__c 
                                                                       WHERE  SRS_PODS_Product_Master__c =:lstSR[0].SRS_PODS_Product__c];
                    
                    //Create map with parent key and list of childs
                    MAP<double,Set<double>> parentChildMAP = new MAP<double,Set<double>>(); 
                    //Store list of all child in set 
                    Set<double> setAllChilds = new Set<double>(); 
                    for(SRS_PODS_Template__c prodTempalte:templateQuestionsList){
                        if(prodTempalte.Child__c != null){
                            //chek for duplicate childs
                            if(!setAllChilds.contains(prodTempalte.Child__c)){
                                if(parentChildMAP.containsKey(prodTempalte.SYSTEM_GENERATED_QUESTION_ID__c)){
                                    parentChildMAP.get(prodTempalte.SYSTEM_GENERATED_QUESTION_ID__c).add(prodTempalte.Child__c);
                                }
                                else{
                                    parentChildMAP.put(prodTempalte.SYSTEM_GENERATED_QUESTION_ID__c, new Set<double>{prodTempalte.Child__c});
                                }
                            }
                            setAllChilds.add(prodTempalte.Child__c);
                        }            
                    }
                    
                    //iterate for non parent and non child questions
                    for(SRS_PODS_Template__c prodTempalte:templateQuestionsList){
                        if(!parentChildMAP.containsKey(prodTempalte.SYSTEM_GENERATED_QUESTION_ID__c) && !setAllChilds.contains(prodTempalte.SYSTEM_GENERATED_QUESTION_ID__c)){
                            parentChildMAP.put(prodTempalte.SYSTEM_GENERATED_QUESTION_ID__c,new Set<double>());
                        }
                    }        
                    system.debug('$$$$parentChildMAP  '+parentChildMAP);
                    
                    //create map for Tempalte Questions and answers saved against Service Request
                    MAP<double,SRS_PODS_Answer__c> srTmpltQAMap = new MAP<double,SRS_PODS_Answer__c>();
                    if(templateAnswersLst != null && templateAnswersLst.size() > 0){
                        for(SRS_PODS_Answer__c sfAnswer : templateAnswersLst){                            
                            if(string.isNotBlank(sfAnswer.SRS_PODS_Products__r.QUESTION_EN__c)){
                                srTmpltQAMap.put(sfAnswer.SRS_PODS_Products__r.SYSTEM_GENERATED_QUESTION_ID__c,sfAnswer);
                            }
                        }
                        system.debug('$$$$ srTmpltQAMap = '+srTmpltQAMap);                       
                        
                        //Map with question key and anser object for final update 
                        MAP<string,SRS_PODS_Answer__c> srUpdateTmpltQAMAP = new MAP<string,SRS_PODS_Answer__c>();
                        for(double qId:parentChildMAP.keySet()){
                            if(srTmpltQAMap.containsKey(qId)){
                                srUpdateTmpltQAMAP.put(srTmpltQAMap.get(qId).SRS_PODS_Products__r.QUESTION_EN__c,srTmpltQAMap.get(qId));                              
                                //iterate for childs which are as List of Child Ids against parent key in parentChildMAP
                                for(double childId:parentChildMAP.get(qId)){
                                    if(srTmpltQAMap.containsKey(childId)){
                                        //Support level is not child question in ESR and should handled as independent question.
                                        if(srTmpltQAMap.get(childId).SRS_PODS_Products__r.QUESTION_EN__c.equalsIgnoreCase('Support Level')){
                                            srUpdateTmpltQAMAP.put(srTmpltQAMap.get(childId).SRS_PODS_Products__r.QUESTION_EN__c,srTmpltQAMap.get(childId));   
                                        }else{
                                            srUpdateTmpltQAMAP.put(srTmpltQAMap.get(childId).SRS_PODS_Products__r.QUESTION_EN__c+'|'+srTmpltQAMap.get(qId).SRS_PODS_Products__r.QUESTION_EN__c,srTmpltQAMap.get(childId));                              
                                        }
                                    }
                                }
                            }                            
                        }
                        System.debug('$$$$ srUpdateTmpltQAMAP = '+ srUpdateTmpltQAMAP);
                        
                        //Load existing Answer and Questions in Map for input Comparison with 
                        if(esrRequest.template != null && esrRequest.template.size() > 0){
                            for(ProdTemplate esrAnswer : esrRequest.template){  
                                system.debug('$$$$ esrAnswer.questionText = '+esrAnswer.questionText);
                                if(srUpdateTmpltQAMAP.containsKey(esrAnswer.questionText)){
                                    srUpdateTmpltQAMAP.get(esrAnswer.questionText).SRS_PODS_Answer__c = esrAnswer.answerText;
                                }
                            }
                        }
                        
                        //Null check for Question and Anwsers set
                        if(srUpdateTmpltQAMAP != null && !srUpdateTmpltQAMAP.isEmpty()){  //CH01 - Added null check
                            updateTemplateAnsLst = srUpdateTmpltQAMAP.values();
                        }
                        System.debug('$$$$ updateTemplateAnsLst = '+updateTemplateAnsLst);
                    }
                    
                    Service_Request__c SRToUpdate = lstSR[0];                   
                    
                    if(esrRequest.customerStatusCd != null){
                        SRToUpdate.Billing_Account_New__c = esrRequest.customerStatusCd;
                    }
                    if(esrRequest.requestForProposalTxt != null){
                        SRToUpdate.Is_RFP__c = esrRequest.requestForProposalTxt;
                    }
                    if(esrRequest.budgetaryFirmTxt != null){
                        if(!esrRequest.budgetaryFirmTxt.equalsIgnoreCase('Sold')){   
                            SRToUpdate.ESR_Opportunity_Type__c = esrRequest.budgetaryFirmTxt;                           
                        }                       
                    }                    
                    if(esrRequest.requestDueDt != null){
                        SRToUpdate.Target_Date__c = esrRequest.requestDueDt; 
                    }
                    if(string.isNotBlank(esrRequest.requestStatusCd)){
                        //Complete Mark service request when status changes to sold
                        if(esrRequest.requestStatusCd.equalsIgnoreCase('SOLD: AUTOQUOTE') || esrRequest.requestStatusCd.equalsIgnoreCase('SOLD: VSE')){                      
                            srToUpdate.Service_Request_Status__c='Completed';
                        }
                    }
                    System.debug('$$$$ SRToUpdate = '+SRToUpdate);
                    
                    //Create Notes for Service Request
                    List<Note> lstNote = new List<Note>();                    
                    System.debug('$$$$ esrRequest.notes = '+esrRequest.notes);
                    if(esrRequest.notes != null){
                        for(esrLog log : esrRequest.notes){
                            Note objNote = new Note();
                            system.debug('$$$$ log'+log);
                            if(string.isNotBlank(log.logTxt)){
                                objNote.body = log.logTxt;
                                objNote.title = log.logTxt.length() > 10?log.logTxt.substring(0,10):log.logTxt;  
                                objNote.ParentId = SRToUpdate.Id;
                                lstNote.add(objNote); 
                            }
                        }
                    }
                    
                    update SRToUpdate;                    
                    //update Opportunity Product Item                    
                    Opp_Product_Item__c optyProdItem = new Opp_Product_Item__c(); 
                    system.debug('$$$$ optyProdItem'+ lstSR[0].Opportunity_Product_Item__c);
                    if(lstSR[0].Opportunity_Product_Item__c != null){ 
                        //Null check for hardware margin   
                        optyProdItem.id = lstSR[0].Opportunity_Product_Item__c;
                        system.debug('$$$$ esrRequest.hardwareMarginTxt '+esrRequest.opportunityValueNum );                   
                        if(string.isNotBlank(esrRequest.opportunityValueNum)){
                            //update new Opp_Product_Item__c(Id = lstSR[0].Opportunity_Product_Item__c, Margin__c = decimal.valueOf(esrRequest.opportunityValueNum));
                            optyProdItem.Total_Contract_Value__c = decimal.valueOf(esrRequest.opportunityValueNum);
                            system.debug('$$$$ Product Item'+ optyProdItem);
                        }
                        if(string.isNotBlank(esrRequest.hardwareMarginTxt)){
                            //update new Opp_Product_Item__c(Id = lstSR[0].Opportunity_Product_Item__c, Margin__c = decimal.valueOf(esrRequest.opportunityValueNum));
                            optyProdItem.Margin__c = decimal.valueOf(esrRequest.hardwareMarginTxt);
                            system.debug('$$$$ Product Item'+ optyProdItem);
                        }
                        
                        update  optyProdItem;
                        system.debug('$$$$ updated Product Item'+ optyProdItem);
                        //Assing version Opportunity Type                           
                    }
                    //update latest version 
                    //update SF version if version details are received from ESR                    
                    system.debug('$$$$ Enhanced_Tracking' +lstSR[0].Enhanced_Tracking__r);                   
                    if(!lstSR[0].Enhanced_Tracking__r.isEmpty()){
                        Enhanced_Tracking__c sfVersion = new Enhanced_Tracking__c(); 
                        sfVersion = lstSR[0].Enhanced_Tracking__r[0];                            
                        system.debug('$$$$ sfVersion'+sfVersion);
                        try
                        {
                            if(String.isNotBlank(esrRequest.budgetaryFirmTxt)){
                                sfVersion.ESR_Opportunity_Type__c = esrRequest.budgetaryFirmTxt;                                    
                            }
                            if(String.isNotBlank(esrRequest.opportunityValueNum)){
                                sfVersion.Opportunity_Value__c  = esrRequest.opportunityValueNum;
                            }
                            if(String.isNotBlank(esrRequest.requestStatusCd)){
                                if(esrRequest.requestStatusCd.equalsIgnoreCase('CLOSED: AUTOQUOTE') || esrRequest.requestStatusCd.equalsIgnoreCase('CLOSED: VSE')){
                                    sfVersion.Version_Status__c ='Completed';
                                }
                                if(esrRequest.requestStatusCd.equalsIgnoreCase('CANCELLED : VSE')){
                                    sfVersion.Version_Status__c ='Cancelled';
                                }
                            }
                            system.debug('$$$$ Updated Version '+sfVersion);
                            update sfVersion;
                            system.debug('$$$$ Updated Version '+sfVersion);
                        }
                        catch(Exception ex){}
                    }
                    //update product template
                    if(updateTemplateAnsLst != null && updateTemplateAnsLst.size() > 0){
                        update updateTemplateAnsLst;
                    }                    
                    //create Notes
                    if(lstNote != null && lstNote.size() > 0){
                        insert lstNote;
                    }
                    
                }
                else{
                    
                    throw new DmlException('No Record Found for Given Salesforce Id');
                }
                
            }else{
                throw new DmlException('Record cannot be retrived for given id or Service Request is Cancelled or Completed.');
                
            }
            //if(service.Parent_Service_Request__c == null){
            //throw new Exception('Parent service is missing for Version');
            // }
            return new ESRInboundService.ESRInboundServiceResponse('Record updated Successfully','');
        }
             
        catch(Exception ex)                                                           
        {
             system.debug('$$$$ Error'+ex);
            Webservice_Integration_Error_Log__c errorLog = new Webservice_Integration_Error_Log__c();
            errorLog.Apex_Class_and_Method__c = 'ESRInboundService.updateSFServiceRequest()';
            errorLog.Error_Code_and_Message__c = ex.getMessage() + ' ' + ex.getLineNumber() + ' ' + ex.getStackTraceString();
            errorLog.External_System_Name__c = 'ESR';
            errorLog.SFDC_Record_Id__c = esrRequest != null ? esrRequest.salesForceId : '';
            /*Code added by Swapnil Kamble[Tech M] for capturing Error and success logs in the 'Webservice Integration Error Log' object 
             *Date: 7 July 2016
             */
            errorLog.RecordTypeId = errorLogFailureRecordTypeId;
            errorLog.Stack_Trace__c = ex.getStackTraceString();
            insert errorLog;
            Util.emailSystemError(ex);
            throw ex;
        }
    }
    
    /**
creatVersion(ESRVersion esrVersion) - 
**/
    webservice static ESRInboundServiceResponse creatVersion(ESRVersion esrVersion){
        try{
            system.debug('****** creatVersion **** esrVersion = '+esrVersion);            
            Enhanced_Tracking__c sfVersion = new Enhanced_Tracking__c();
            Service_Request__c srToUpdate = new Service_Request__c();
            //Search Parent Record
            List<Service_Request__c> lstParentService = new List<Service_Request__c>();
            List<Enhanced_Tracking__c> lstCompletedVersions = new List<Enhanced_Tracking__c>();
            //retrive Version record based on ESR ID if version is already created.
            if(esrVersion != null && String.isNotBlank(esrVersion.esrId)){
                //retrieve service request details for ESR ID
                lstParentService = [SELECT Id, Name,Opportunity_Product_Item__c, 
                                    SRS_PODS_Product__c, 
                                    Opportunity__r.Sales_Opportunity_Id__c,
                                    (SELECT Id, ESR_Id__c, Version_Status__c FROM Enhanced_Tracking__r)
                                    FROM Service_Request__c
                                    WHERE Name =: esrVersion.salesForceId
                                    AND (Service_Request_Status__c != 'Cancelled' OR Service_Request_Status__c != 'Completed')];
                System.debug('$$$$ lstParentService = '+lstParentService);
                 System.debug('$$$$ Enhanced_Tracking__r = '+lstParentService[0].Enhanced_Tracking__r);
                
                if(!lstParentService.isEmpty()){
                    for(Enhanced_Tracking__c oldVer :lstParentService[0].Enhanced_Tracking__r){
                        if(oldVer.ESR_Id__c != esrVersion.esrId){
                            oldVer.Version_Status__c = 'Completed';
                            lstCompletedVersions.add(oldVer);
                        }else if(oldVer.ESR_Id__c == esrVersion.esrId){
                            //sfVersion = oldVer;
                           throw new InboundException('Version with specified ESR ID is already created.'); 
                        }                   
                    }
                    //Set parent id for version
                    sfVersion.Service_Request__c = lstParentService[0].id;
                    srToUpdate = lstParentService[0];
                }else{
                    throw new DmlException('No Parent Service Request record found or Service Request is Cancelled or Completed.');
                }
                System.debug('$$$$ lstCompletedVersions = '+lstCompletedVersions);
                
            }else{
                throw new InboundException('Object is NULL or ESR Id is empty');
            }
            
            //set Parent Record esrId
            if(String.isNotBlank(esrVersion.esrId)){
                sfVersion.ESR_Id__c = esrVersion.esrId;
            }
            
            if(String.isNotBlank(esrVersion.VersionId)){
                sfVersion.Version_ID__c = esrVersion.VersionId;
            }
            if(String.isNotBlank(esrVersion.description)){
                sfVersion.Version_Description__c =  esrVersion.description;
            }
            if(String.isNotBlank(esrVersion.VersionTitle)){
                sfVersion.Version_Title__c = esrVersion.VersionTitle;             
            }
            if(String.isNotBlank(esrVersion.systemType)){
                sfVersion.System_Type__c = esrVersion.systemType;
                //srToUpdate - Not maintained @ Service request level
            }
            if(String.isNotBlank(esrVersion.orderType)){
                sfVersion.Order_Type__c = esrVersion.orderType;
                //srToUpdate - handled in Template code for update Service request 
            }
            if(String.isNotBlank(esrVersion.OpportunityType)){
                sfVersion.ESR_Opportunity_Type__c = esrVersion.OpportunityType;
                if(esrVersion.OpportunityType.equalsIgnoreCase('Sold')){
                    //when Opprotunity tpye is sold ESR then change opportunity to Complete and no change on Opportunity type
                    esrVersion.requestStatusCd ='SOLD: AUTOQUOTE';                    
                }
                else{
                    srToUpdate.ESR_Opportunity_Type__c = esrVersion.OpportunityType;
                }
            }
            if(String.isNotBlank(esrVersion.opportunityValueNum)){
                sfVersion.Opportunity_Value__c = esrVersion.opportunityValueNum;               
            }
            System.debug('$$$$ esrVersion.requestStatusCd = '+esrVersion.requestStatusCd);
            
            //set status for version with status for service request if status is sold.
            if(String.isNotBlank(esrVersion.requestStatusCd)){
                if(esrVersion.requestStatusCd.equalsIgnoreCase('CLOSED: AUTOQUOTE') || esrVersion.requestStatusCd.equalsIgnoreCase('CLOSED: VSE')){
                    sfVersion.Version_Status__c ='Completed';
                }
                if(esrVersion.requestStatusCd.equalsIgnoreCase('CANCELLED : VSE')){
                    sfVersion.Version_Status__c ='Cancelled';
                }
                if(esrVersion.requestStatusCd.equalsIgnoreCase('SOLD: AUTOQUOTE') || esrVersion.requestStatusCd.equalsIgnoreCase('SOLD: VSE')){
                    sfVersion.Version_Status__c ='Completed';
                    srToUpdate.Service_Request_Status__c='Completed';
                }
            }  
            
            //get the ESR Record Type
            //RecordType recordType = [Select Id, Name from RecordType Where SobjectType ='Enhanced_Tracking__c' and Name = 'ESR-Enhanced Tracking' limit 1];
            
            Id recordType =Schema.SObjectType.Enhanced_Tracking__c.getRecordTypeInfosByName().get('ESR-Enhanced Tracking').getRecordTypeId();
            sfVersion.RecordTypeId = recordType;
            
            try{
                upsert sfVersion;
                //update service request 
                update srToUpdate;
                system.debug('$$$$ srToUpdate'+srToUpdate);
                if(!lstCompletedVersions.isEmpty() && lstCompletedVersions.size() > 0)
                    update lstCompletedVersions;
            }
            catch(DMLException ex){
                System.debug('The following exception has occurred while creating Version record : ' + ex.getMessage() + ' ' + ex.getLineNumber() + ' ' + ex.getStackTraceString());
                throw ex;
            } 
            
            system.debug('$$$$sfVersion = '+sfVersion);
            
            //update opportunity value for SR
            Opp_Product_Item__c optyProdItem;                   
            if(srToUpdate.Opportunity_Product_Item__c != null){ 
                //Null check for hardware margin
                if(esrVersion.opportunityValueNum != null){
                    try
                    {
                        system.debug('$$$$ Opportunity Valyue '+esrVersion.opportunityValueNum );
                        Opp_Product_Item__c opItm = new Opp_Product_Item__c();
                        opItm.id =srToUpdate.Opportunity_Product_Item__c;
                        opItm.Total_Contract_Value__c = decimal.valueOf(esrVersion.opportunityValueNum);
                        update opItm;
                        // update new Opp_Product_Item__c(Id = srToUpdate.Opportunity_Product_Item__c, Total_Contract_Value__c = decimal.valueOf(esrVersion.opportunityValueNum));
                        system.debug('$$$$  Opportunity Valyue '+opItm);
                    }
                    catch(DMLException ex){
                        System.debug('The following exception has occurred while creating Version record : ' + ex.getMessage() + ' ' + ex.getLineNumber() + ' ' + ex.getStackTraceString());
                        throw ex;
                    } 
                }
            }
            
            //Create note on Service request 
            List<Note> lstNotes = new List<Note>();
            if(esrVersion.notes !=null){
                for(esrLog log : esrVersion.notes){
                    Note note = new Note();
                    system.debug('$$$$ log'+log);
                    if(String.isNotBlank(log.logTxt)){
                        note.body=log.logTxt;
                        note.title= log.logTxt.length() > 10?log.logTxt.substring(0,10):log.logTxt;                        
                        note.ParentId=lstParentService[0].Id;
                        lstNotes.add(note); 
                    }
                }
            }
            system.debug('$$$$ lstNotes = '+lstNotes);
            
            
            //updateSFServiceRequest(esrVersion.parentService);
            if(lstNotes !=null && lstNotes.size() > 0){
                insert lstNotes;
            }
            system.debug('$$$$ finished execution');
            return new ESRInboundService.ESRInboundServiceResponse('Version Record Created Sucesfully',sfVersion.id);            
        }
       
        Catch(Exception ex){
            
            system.debug('$$$$ Error'+ex);
            Webservice_Integration_Error_Log__c errorLog = new Webservice_Integration_Error_Log__c();
            errorLog.Apex_Class_and_Method__c = 'ESRInboundService.creatVersion()';
            errorLog.Error_Code_and_Message__c = ex.getMessage() + ' ' + ex.getLineNumber() + ' ' + ex.getStackTraceString();
            errorLog.External_System_Name__c = 'ESR';
            errorLog.SFDC_Record_Id__c = esrVersion != null ? esrVersion.salesForceId : '';
            /*Code added by Swapnil Kamble[Tech M] for capturing Error and success logs in the 'Webservice Integration Error Log' object 
             *Date: 7 July 2016
             */
            errorLog.RecordTypeId = errorLogFailureRecordTypeId;
            errorLog.Stack_Trace__c = ex.getStackTraceString();
            insert errorLog;
            Util.emailSystemError(ex);
            throw ex;
        }  
    }
    
    /**
CreateAttachmet(EsrAttachment attachment) -
**/
    webservice static ESRInboundServiceResponse CreateAttachmet(EsrAttachment attachment){
        try{
            //System.debug('**** CreateAttachmet **** attachment = '+attachment);
            System.debug('$$$$ HEAP getLimitHeapSize-1'+limits.getLimitHeapSize());
            System.debug('$$$$ HEAP getHeapSize-1'+limits.getHeapSize());
            Attachment objAttach = new  Attachment();   
            
            //Search for Parent record and set Parentid for attachment
            List<Service_Request__c> lstParentService = new List<Service_Request__c>();
            lstParentService = [SELECT Id, Name
                                FROM Service_Request__c 
                                WHERE name =: attachment.ParentId];
            if(lstParentService.size() > 0){
                objAttach.ParentId = lstParentService[0].id;
            }              
            else{
                throw new DmlException('No Parent Record Found');
            }            
            //objAttach.Body = Blob.Valueof(attachment.fileTxt);
            objAttach.Body  = EncodingUtil.base64Decode(attachment.fileTxt);
            //objAttach.ParentId = attachment.ParentId;
            objAttach.ContentType = attachment.attachmentTypeCd;
            objAttach.name = attachment.attachmentPathTxt;
            insert objAttach;
            System.debug('$$$$ HEAP getLimitHeapSize-2'+limits.getLimitHeapSize());
            System.debug('$$$$ HEAP getHeapSize-2'+limits.getHeapSize());
            return new ESRInboundService.ESRInboundServiceResponse('Attacment Record Created Sucesfully',objAttach.Id); 
           
        }
        
        catch(Exception ex){
             system.debug('$$$$ Error'+ex);
            Webservice_Integration_Error_Log__c errorLog = new Webservice_Integration_Error_Log__c();
            errorLog.Apex_Class_and_Method__c = 'ESRInboundService.CreateAttachmet()';
            errorLog.Error_Code_and_Message__c = ex.getMessage() + ' ' + ex.getLineNumber() + ' ' + ex.getStackTraceString();
            errorLog.External_System_Name__c = 'ESR';
            errorLog.SFDC_Record_Id__c = attachment != null ? attachment.ParentId : '';
            /*Code added by Swapnil Kamble[Tech M] for capturing Error and success logs in the 'Webservice Integration Error Log' object 
             *Date: 7 July 2016
             */
            errorLog.RecordTypeId = errorLogFailureRecordTypeId;
            errorLog.Stack_Trace__c = ex.getStackTraceString();
            insert errorLog;
            Util.emailSystemError(ex);
            throw ex;
        }
    } 
    
    /**************************************************************************************************************************/
    /******************************************* WRAPPER CLASS SECTION ********************************************************/
    /**************************************************************************************************************************/
    
    /**
EsrAttachment() - EsrAttachment Wrapper class
**/
    global class EsrAttachment{
        webservice string ParentId {get;set;}        
        webservice string title {get;set;}
        webservice string fileTxt {get;set;}
        webservice string attachmentPathTxt {get;set;}
        webservice string attachmentTypeCd {get;set;}      
       
    }
    
    /**
EsrLog() - EsrLog Wrapper Class
**/
    global class EsrLog{
        webservice string eventText {get;set;}
        webservice string logTxt {get;set;}
    }
    
    /**
ESRInboundServiceResponse() - ESRInboundServiceResponse Wrapper class
**/
    global class ESRInboundServiceResponse
    {
        global ESRInboundServiceResponse(string respMessage, string txnId){
            responseMessage = respMessage;
            transactionId = txnId;
        }
        webservice string responseMessage{get;set;}
        webservice string transactionId{get;set;}         
        
    }
    
    /**
ProdTemplate() - ProdTemplate Wrapper class
**/
    global class ProdTemplate{        
        webservice string questionText{get;set;}
        webservice string answerText{get;set;}         
    }
    
    /**
ESRServiceRequest() - ESRServiceRequest Wrapper class
**/
    global class ESRServiceRequest{
        webservice boolean customerStatusCd {get;set;}
        webservice string requestForProposalTxt {get;set;}
        webservice string  budgetaryFirmTxt {get;set;}
        webservice date requestDueDt {get;set;}
        webservice string salesQuoteRequestCd {get;set;}
        webservice string projectCd {get;set;}
        webservice string opportunityValueNum {get;set;}
        webservice string hardwareMarginTxt {get;set;}
        webservice List<EsrLog> notes {get;set;}
        webservice string requestStatusCd {get;set;}
        webservice string salesForceId {get;set;}
        webservice List<ProdTemplate> template {get;set;}
        webservice string productCategoryTxt {get;set;}
        webservice string productName{get;set;}
        webservice string esrId{get;set;}
    }
    
    /**
ESRVersion() - ESRVersion Wrapper class
**/
    global class ESRVersion{
        webservice string versionId {get;set;}
        webservice string esrId {get;set;}
        webservice string VersionTitle{get;set;}
        webservice string description{get;set;}
        webservice string orderType{get;set;}
        webservice string systemType{get;set;}
        webservice string opportunityValueNum{get;set;}
        Webservice String OpportunityType{get;set;}
        webservice List<EsrLog> notes{get;set;}
        webservice string salesForceId{get;set;}
        webservice string requestStatusCd{get;set;}
        
    }
    
    global class InboundException extends Exception{
        /*InboundException(String exceptionMessage){
this.exceptionMessage = exceptionMessage;
}*/
        webservice String exceptionMessage {get;set;}
    }
}