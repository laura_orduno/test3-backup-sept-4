/*
 * Base class extended by all the trac_Repair*Ctlr classes
 * Contains basic functionality to load case record, indicate callout status, and present manual links
 * Part of the SMB Repairs project
 *
 * @author: Grant Adamson, Traction On Demand 
 */

public virtual without sharing class trac_RepairCaseCtlr {
	private static final String CASE_PREFIX = Case.sObjectType.getDescribe().getKeyPrefix();
	
	public String redirectURL {get; private set;}
	public Boolean complete {get; private set;}
	public Boolean success {get; private set;}
	public Boolean closed {get; private set;}
	public Case theCase {get; private set;}
	
	
	public trac_RepairCaseCtlr() {
		complete = false;
		success = false;
		closed = false;
		Id caseId = getCaseId();
		theCase = loadCase(caseId);
		redirectURL = buildRedirectUrl();
		if(theCase.isClosed) {
			trac_PageUtils.addError(Label.REPAIR_CLOSED_NO_REOPEN);
			closed = true;
			complete = true;
			success = false;
		}
	}
	
	
	private static Id getCaseId() {
		if(trac_PageUtils.isParamNull('id')) {
			throw new RepairCaseException(Label.NO_CASE_ID);
		}
		
		String caseIdString = trac_PageUtils.getParam('id');
		
		if(!caseIdString.toLowerCase().startsWith(CASE_PREFIX)) {
			throw new RepairCaseException(Label.INVALID_CASE_ID);
		}
		
		Id caseId;
		
		try {
			caseId = (Id)caseIdString;
		} catch (Exception e) {
			throw new RepairCaseException(Label.INVALID_CASE_ID);
		}
		
		return caseId; 
	}
	
	
	private static Case loadCase(Id caseId) {
		/*
		List<Case> cases = [
			SELECT    
				CaseNumber,
				isClosed,
				Repair__c,
				Repair_Validated__c,
				Repair_In_Progress__c,
				Accessory_SKU__c,
				repair_type__c,
				Customer_Title_client__c,
				Customer_First_Name_client__c,
				Customer_Last_Name_client__c,
				ESN_MEID_IMEI__c,
				Mobile_number__c,
				Model_SKU__c,
				Customer_Title_contact__c,
				Customer_First_Name_contact__c,
				Customer_Last_Name_contact__c,
				Customer_Phone_contact__c,
				Customer_Email_contact__c,
				Address_shipping__c,
				City_shipping__c,
				Province_shipping__c,
				Postal_Code_shipping__c,
				Business_Name__c,
				Address_account__c,
				City_account__c,
				Province_account__c,
				Postal_Code_account__c,
				Physical_Damage__c,
				Issue_System__c,
				Defects_behaviour__c,
				Descrip_of_Defect_Troubleshooting_Perf__c,
				Is_there_any_Cosmetic_Damage__c,
				Comments_create_repair__c,
				Estimated_Repair_Cost_Up_To__c,
				Client_Contacted__c,
				Shipping_Required__c,
				Use_Alternate_Address__c,
				Issue_System_sfdc_only__c
			FROM Case
			WHERE Id = :caseId
		];
		*/
		List<Case> cases = database.query('SELECT CaseNumber,isClosed,Repair__c,Repair_Validated__c,Repair_In_Progress__c,Accessory_SKU__c,repair_type__c,Customer_Title_client__c,Customer_First_Name_client__c,Customer_Last_Name_client__c,ESN_MEID_IMEI__c,Mobile_number__c,Model_SKU__c,Customer_Title_contact__c,Customer_First_Name_contact__c,Customer_Last_Name_contact__c,Customer_Phone_contact__c,Customer_Email_contact__c,Address_shipping__c,City_shipping__c,Province_shipping__c,Postal_Code_shipping__c,Business_Name__c,Address_account__c,City_account__c,Province_account__c,Postal_Code_account__c,Physical_Damage__c,Issue_System__c,Defects_behaviour__c,Descrip_of_Defect_Troubleshooting_Perf__c,Is_there_any_Cosmetic_Damage__c,Comments_create_repair__c,Estimated_Repair_Cost_Up_To__c,Client_Contacted__c,Shipping_Required__c,Use_Alternate_Address__c,Issue_System_sfdc_only__c FROM Case WHERE Id =\''+caseId+'\'');
		if(cases.size() > 0) {
			return cases[0];
		} else {
			throw new RepairCaseException(Label.CASE_NOT_FOUND);
		}
	}
	
	
	private String buildRedirectUrl() {
		string repairCreateLinkString='';
		try{
			repair_ws_endpoints__c repairCreateLink=repair_ws_endpoints__c.getinstance('RepairCreateLink');
			repairCreateLinkString=repairCreateLink.endpoint__c;
		}catch(exception e){
			repairCreateLinkString='https://securesso-pt168.tsl.telus.com/srdcare/IntSSOLogin.action?goto=CreateRepair.action';
		}
		String url = 
			repairCreateLinkString +
			'&caseID=' + (theCase.id != null ? theCase.id : '') + 
			'&caseNO=' + (theCase.CaseNumber != null ? theCase.CaseNumber : '') +
			'&languageCode=' + getLangCode() +
			'&repairTypeCd=' + theCase.Repair_Type__c;
		
		if(theCase.Repair_Type__c == 'Device' || theCase.Repair_Type__c == 'SIM') {
			url += '&mobileNum=' + (theCase.Mobile_number__c != null ? theCase.Mobile_number__c : '') +
						 '&itemESN=' + (theCase.ESN_MEID_IMEI__c != null ? theCase.ESN_MEID_IMEI__c : '');
		
		} else if (theCase.repair_type__c == 'Accessory') {
			url += '&itemSKU=' + (theCase.Accessory_SKU__c != null ? theCase.Accessory_SKU__c : '');
		}					 
		
		return url;
	}
	
	
	private static String getLangCode() {
		String temp = UserInfo.getLanguage();
		
		// en_CA -> EN
		return temp.substring(0,2).toUpperCase();
	}
	
	
	public class RepairCaseException extends Exception {}
	
}