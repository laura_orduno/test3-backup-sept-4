@isTest
public class OrdrProductFeasibilityWsCalloutTest {
@isTest
    private static void getLineItemAction(){
        String orderId= OrdrTestDataFactory.singleMethodForOrderId();
       
        
        Test.startTest();
        Order ordObj=[select id,type from order where id=:orderId];
        ordObj.type='New Services';
        update ordObj;
         List<OrderItem> oiListOut=[select UpgradeDowngrade__c,orderMgmt_BPI_Id__c,id,amendStatus__c,order.type,vlocity_cmt__ProvisioningStatus__c,quantity
                               from orderitem where orderid=:orderId];
        for(OrderItem oiInst:oiListOut){
            oiInst.UpgradeDowngrade__c=true;
            oiInst.amendStatus__c='Add';
            oiInst.vlocity_cmt__ProvisioningStatus__c='Add';
            oiInst.orderMgmt_BPI_Id__c='123456';
        }
        update oiListOut;
        OrderItem oiInst=oiListOut.get(0);
        Map<String,List<OrderItem>> upgradeDowngradeBpiIdMap=new Map<String,List<OrderItem>>();
        
         for (OrderItem oli : oiListOut) {
            if(oli.UpgradeDowngrade__c){
                if(String.isNotBlank(oli.orderMgmt_BPI_Id__c)){
                    if(upgradeDowngradeBpiIdMap.keySet().contains(oli.orderMgmt_BPI_Id__c)){
                        List<OrderItem> oiList=upgradeDowngradeBpiIdMap.get(oli.orderMgmt_BPI_Id__c);
                         oiList.add(oli);
                        upgradeDowngradeBpiIdMap.put(oli.orderMgmt_BPI_Id__c,oiList);
                    } else{
                        List<OrderItem> oiList=new List<OrderItem>();
                        oiList.add(oli);
                        upgradeDowngradeBpiIdMap.put(oli.orderMgmt_BPI_Id__c,oiList);
                    }
                    
                }
                
            }
        }
        OrdrProductFeasibilityWsCallout.getLineItemAction(oiInst,upgradeDowngradeBpiIdMap); 
        ordObj.type='Change';
        update ordObj;
        oiListOut=[select UpgradeDowngrade__c,orderMgmt_BPI_Id__c,id,amendStatus__c,order.type,vlocity_cmt__ProvisioningStatus__c,quantity
                               from orderitem where orderid=:orderId];
        for(OrderItem oiInst1:oiListOut){
            oiInst1.UpgradeDowngrade__c=true;
            oiInst1.amendStatus__c='Change';
            oiInst1.vlocity_cmt__ProvisioningStatus__c='Change';
        }
        update oiListOut;
        oiInst=oiListOut.get(0);
        OrdrProductFeasibilityWsCallout.getLineItemAction(oiInst,upgradeDowngradeBpiIdMap); 
        
        ordObj.type='Change';
        update ordObj;
        oiListOut=[select UpgradeDowngrade__c,orderMgmt_BPI_Id__c,id,amendStatus__c,order.type,vlocity_cmt__ProvisioningStatus__c,quantity
                               from orderitem where orderid=:orderId];
        for(OrderItem oiInst1:oiListOut){
            oiInst1.UpgradeDowngrade__c=false;
            oiInst1.amendStatus__c='Change';
            oiInst1.vlocity_cmt__ProvisioningStatus__c='Change';
        }
        update oiListOut;
        oiInst=oiListOut.get(0);
        OrdrProductFeasibilityWsCallout.getLineItemAction(oiInst,upgradeDowngradeBpiIdMap); 
        
        ordObj.type='Amend';
        update ordObj;
        oiListOut=[select UpgradeDowngrade__c,orderMgmt_BPI_Id__c,id,amendStatus__c,order.type,vlocity_cmt__ProvisioningStatus__c,quantity
                               from orderitem where orderid=:orderId];
        Integer i=0;
        for(OrderItem oiInst1:oiListOut){
            oiInst1.UpgradeDowngrade__c=true;
            if(i==0){
                oiInst1.amendStatus__c='Change';
            oiInst1.vlocity_cmt__ProvisioningStatus__c='Change';
            }
             else if(i==0){
                oiInst1.amendStatus__c='Change';
            oiInst1.vlocity_cmt__ProvisioningStatus__c='Change';
            }
            else if(i==0){
                oiInst1.amendStatus__c='Deleted';
            oiInst1.vlocity_cmt__ProvisioningStatus__c='Deleted';
                
            } else {
                oiInst1.amendStatus__c='Add';
            oiInst1.vlocity_cmt__ProvisioningStatus__c='Add';
            }
            
        }
        update oiListOut;
        oiInst=oiListOut.get(0);
        OrdrProductFeasibilityWsCallout.getLineItemAction(oiInst,upgradeDowngradeBpiIdMap); 
        
        ordObj.type='Move';
        update ordObj;
        oiListOut=[select UpgradeDowngrade__c,orderMgmt_BPI_Id__c,id,amendStatus__c,order.type,vlocity_cmt__ProvisioningStatus__c,quantity
                               from orderitem where orderid=:orderId];
        oiInst=oiListOut.get(0);
        OrdrProductFeasibilityWsCallout.getLineItemAction(oiInst,upgradeDowngradeBpiIdMap); 
        Test.stopTest();
    }
    private static void createData(){
        System.runAs(new User(Id = Userinfo.getUserId())) {
            OrdrTestDataFactory.createOrderingWSCustomSettings('ProductEligibilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/RMO/ResourceMgmt/CheckGenericTechnicalAvailabilitySvc_v2_2_vs1');
            
         
        }
    }
    @isTest
    private static void testCheckProductFeasibility0() {
        createData();
        List<SMBCare_Address__c> addresses = OrdrTestDataFactory.createAddresses();
        Id serviceAddressId = addresses[0].Id;
       // OrdrTestDataFactory.createOrderingWSCustomSettings('TechnicalAvailabilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/RMO/ResourceMgmt/CheckGenericTechnicalAvailabilitySvc_v2_2_vs1');
         String orderId=OrdrTestDataFactory.singleMethodForOrderId();   
         List<OrderItem> oiListOut=[select UpgradeDowngrade__c,orderMgmt_BPI_Id__c,id,amendStatus__c,order.type,vlocity_cmt__ProvisioningStatus__c,quantity
                               from orderitem where orderid=:orderId];
        Integer i=0;
        for(OrderItem oiInst1:oiListOut){
            oiInst1.UpgradeDowngrade__c=true;
            oiInst1.amendStatus__c='Add';
            oiInst1.vlocity_cmt__ProvisioningStatus__c='Add';
            oiInst1.orderMgmt_BPI_Id__c='123456';
            if(i==0){
                oiInst1.amendStatus__c='Change';
            oiInst1.vlocity_cmt__ProvisioningStatus__c='Change';
            }
             else if(i==0){
                oiInst1.amendStatus__c='Change';
            oiInst1.vlocity_cmt__ProvisioningStatus__c='Change';
            }
            else if(i==0){
                oiInst1.amendStatus__c='Deleted';
            oiInst1.vlocity_cmt__ProvisioningStatus__c='Deleted';
                
            } else {
                oiInst1.amendStatus__c='Add';
            oiInst1.vlocity_cmt__ProvisioningStatus__c='Add';
            }
        }
        update oiListOut;
        
               oiListOut=[select UpgradeDowngrade__c,orderMgmt_BPI_Id__c,id,amendStatus__c,order.type,vlocity_cmt__ProvisioningStatus__c,quantity
                               from orderitem where orderid=:orderId];
       
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('orderId',orderId);
        TpFulfillmentCustomerOrderV3.CustomerOrder co = OrdrProductFeasibilityWsCallout.performCustomerOrderFeasibility(inputMap);
        Test.stopTest();
    }
    @isTest
    private static void testCheckProductFeasibility1() {
        createData();
        List<SMBCare_Address__c> addresses = OrdrTestDataFactory.createAddresses();
        Id serviceAddressId = addresses[0].Id;
       // OrdrTestDataFactory.createOrderingWSCustomSettings('TechnicalAvailabilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/RMO/ResourceMgmt/CheckGenericTechnicalAvailabilitySvc_v2_2_vs1');
         String orderId=OrdrTestDataFactory.singleMethodForOrderId(); 
        List<OrderItem> oiList=[select id,vlocity_cmt__ParentItemId__c from orderitem where orderid=:orderId];
        String parentId=null;
        for(OrderItem oiInstance:oiList){
            if(String.isBlank(parentId)){
                parentId=oiInstance.id;
            } else {
                oiInstance.vlocity_cmt__ParentItemId__c=parentId;
            }
        }
        update oiList;
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('orderId',orderId);
        TpFulfillmentCustomerOrderV3.CustomerOrder co = OrdrProductFeasibilityWsCallout.performCustomerOrderFeasibility(inputMap);
        Test.stopTest();
    }

        @isTest
    private static void testCheckProductFeasibility2() {
        createData();
        List<SMBCare_Address__c> addresses = OrdrTestDataFactory.createAddresses();
        Id serviceAddressId = addresses[0].Id;
       // OrdrTestDataFactory.createOrderingWSCustomSettings('TechnicalAvailabilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/RMO/ResourceMgmt/CheckGenericTechnicalAvailabilitySvc_v2_2_vs1');
         String orderId=OrdrTestDataFactory.singleMethodForOrderId(); 
        List<OrderItem> oiList=[select id,vlocity_cmt__ParentItemId__c from orderitem where orderid=:orderId];
        String parentId=null;
        for(OrderItem oiInstance:oiList){
            if(String.isBlank(parentId)){
                parentId=oiInstance.id;
            } else {
                oiInstance.vlocity_cmt__ParentItemId__c=parentId;
            }
        }
        update oiList;
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('orderId',orderId);
        TpFulfillmentCustomerOrderV3.CustomerOrder co = OrdrProductFeasibilityWsCallout.performCustomerOrderFeasibility(inputMap);
        Test.stopTest();
    }
    private static testMethod void testCheckProductFeasibility() {
      
        OrdrTestDataFactory.createOrderingWSCustomSettings('ProductEligibilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/RMO/ResourceMgmt/checkGenericTechnicalAvailability');

        OC_ProductFeasibilityImpl componentFeasibility = new OC_ProductFeasibilityImpl();
        Map<String, Object> input = new Map<String, Object>();
        Map<String, Object> output = new Map<String, Object>();
        Map<String, Object> options = new Map<String, Object>();
        
        //List<Order> orders = OrdrTestDataFactory.createOrdersWithServiceAddress();
        //input.put('orderId', orders[0].Id);
        input.put(OrdrConstants.ADDR_ISFIFA,'Yes');
        List<Opportunity> opps = OrdrTestDataFactory.createOpprtunities(1);
        //List<Quote> quotes = OrdrTestDataFactory.createQuotesWithServiceAddress();
        List<SMBCare_Address__c> addresses = OrdrTestDataFactory.createAddresses();
        Id pricebookId = Test.getStandardPricebookId();

        List<Order> orders = new List<Order>();
        for(Integer j=0; j<addresses.size(); j++) {    
            orders.add(new Order(Name='OR-0000'+j, AccountId=opps[0].AccountId, OpportunityId=opps[0].Id, 
                                 //vlocity_cmt__QuoteId__c=quotes[0].Id,
                                 Service_Address__c=addresses[j].Id,
                                 EffectiveDate=System.today(), status='Draft', orderMgmtId__c = '12345678'+j,
                                 Pricebook2Id=priceBookId));
        }
        insert orders;
        input.put('orderId', orders[0].Id);

        
        List<vlocity_cmt.ProductWrapper> productDefinitions = new List<vlocity_cmt.ProductWrapper>();
        vlocity_cmt.ProductWrapper productW = new vlocity_cmt.ProductWrapper();
        productW.productId='01t40000004JwWyAAK';
        productW.name = 'Office Internet Basic';
        productW.JSONAttribute = '{"TELUSCHAR":[{"name":"Office Internet Basic","attributedisplayname__c":"Download speed","attributeRunTimeInfo":{"values":[{"displayText":"25 Mbps"}]}}]}';
        productDefinitions.add(productW);
        //input.put('productDefinition', productDefinitions);
        
        vlocity_cmt__ProductChildItem__c pci = new vlocity_cmt__ProductChildItem__c(vlocity_cmt__ChildLineNumber__c='001', vlocity_cmt__ChildProductId__c='01t40000004JwWyAAK', OCOMOverrideType__c='Override Child');
        insert pci;  
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        //TpFulfillmentCustomerOrderV3.CustomerOrder co = OrdrProductFeasibilityWsCallout.performCustomerOrderFeasibility(input);
        try{
        Boolean returnVal = componentFeasibility.invokeMethod('checkProductFeasibility', input, output, options);
       
        }catch(Exception ex){}
        //System.assert(returnVal);
        Test.stopTest();
    }
}