/*
    ###########################################################################
    # File..................: TBOshowCaseAccountPdfExt
    # Version...............: 1
    # Created by............: Umesh Atry (TechM)
    # Created Date..........: 18/03/2016 
    # Last Modified by......: Umesh Atry (TechM)
    # Last Modified Date....: 21/04/2016
    # Description...........: Class is used to make a PDf according to Account Search Result from TBO form.
    #
    ############################################################################
    */

public with sharing class TBOshowCaseAccountPdfExt {
    public ApexPages.StandardController controller;
    public Case caseRecord {get;set;}
    public string mnc {get;set;}
    public Id caseID {get;set;}   
    public string accSearchType { get;set;} 
    public string AccSearchNumber { get;set;}
    public Map < string, List < Account >> AccountCallsMap { get;set;}
    public Map < id, SMBCare_Address__c > getAddressMap { get; set; }
    public List < asset > getAssetlist { get; set; }
    public Map < string, List < asset >> AssestMap {get;set; }
    public Map < string, Integer > AssestSizeMap {  get; set;}
    public map < id, account > finalAccList {  get; set;}
    public List < pdfData > PdfDetailsList {  get;set; }
    public static list < account > listaccResult { get;set; }
    public set < id > caseAccSet {  get; set;   }
    public set < String > addressString {  get; set; }

    public class pdfData {
        public string address { get;set;}
        public string can { get; set; }
        public string btn {  get;set; }
        public Integer wtn { get; set; }

        public pdfData() {}

    }
    public TBOshowCaseAccountPdfExt(ApexPages.StandardController controller) {
        this.controller = controller;
        this.caseRecord = (Case) controller.getRecord();
        system.debug('Case Record Details = ' + this.caseRecord);
        mnc = ApexPages.currentPage().getParameters().get('mnc');
        accSearchType = ApexPages.currentPage().getParameters().get('accSearchType');
        AccSearchNumber = ApexPages.currentPage().getParameters().get('AccSearchNumber');
        CaseId = ApexPages.currentPage().getParameters().get('id');
        if (caseId != null) {
            Case caser = [select id, AccountId from
                case where id = :
                    caseID
            ];
        }
        system.debug('CaseId=' + CaseId);

        system.debug('accSearchType=' + accSearchType);
        system.debug('AccSearchNumber=' + AccSearchNumber);
        system.debug('mnc=' + mnc);

        if (mnc == 'searchAllAtRCID') {
            callSearchAllAtRCID();
        }
        if (mnc == 'searchAccountsFromCBN') {
            searchAccountsFromCBN();
        }
        if (mnc == 'searchAndAddSingleAccount') {
            searchAndAddSingleAccount();
        }
        if (mnc == 'demo') {
            callMe();
        }
    }

    //holds the list f columns to be used in query
    public string getAcccolumns = 'SELECT Id, Name, parentId, RecordType.DeveloperName, Billing_System__c,CBUCID__c,BAN__C, RCID__c, CustProfId__c,CBUCID_RCID__c, BCAN__c,Billing_System_ID__c,BTN__c, CAN__c, BAN_CAN__c, Billing_Account_Active_Indicator__c, CAN_Level__c ,BCAN_Pilot_Org_Indicator__c from Account where';

    public void callMe() {
        string getAcccolumns = 'SELECT Id, Name, parentId, RecordType.DeveloperName, Billing_System__c,CBUCID__c,BAN__C, RCID__c, CustProfId__c,CBUCID_RCID__c, BCAN__c,Billing_System_ID__c,BTN__c, CAN__c, BAN_CAN__c, Billing_Account_Active_Indicator__c, CAN_Level__c ,BCAN_Pilot_Org_Indicator__c from Account';
        List < Account > acc = database.query('' + getAcccolumns + '');
        finalAccList = new map < id, Account > (acc);
    }

    public set < id > searchCaseAccSet() {
            set < id > caset = new set < id > ();
            if (caseRecord != null) {
                list < case_account__c > calist = new list < case_account__c > (
                    [Select id, name, case__c, account__c from case_account__c where case__C =: caseRecord.id limit 10000]);
                if (calist != null) {
                    for (case_account__c ca: calist) caset.add(ca.account__c);
                }
            }
            if (caset != null && caset.size() > 0)
                return caset;
            else
                return null;
        }
        // Method to search and return a details of a searched account
    public Account searchCurrentAcc(String searchNumber) {
        try {
            Account Acc = database.query('' + getAcccolumns + ' ' + accSearchType + '__c =\'' + SearchNumber + '\' AND  Inactive__c=false AND Billing_Account_Active_Indicator__c =\'Y\' limit 1');           
            if (Acc != null)
                return Acc;
            else {

                return null;
            }
        } catch (Exception e) {

            return null;
        }
    }




    public boolean SearchAndValidateParentRCID(String AccSearchNumber) {
        Account CurrAcc = searchCurrentAcc(AccSearchNumber);
        Case caser = [select id, AccountId from
            case where id = :
                caseID
        ];
        system.debug('>>>> CurrAcc Acc:' + CurrAcc);
        if (CurrAcc != null && CurrAcc.BCAN__c != null) {
            Account highBillAcc = [SELECT Id, Name, parentId, RecordType.DeveloperName, Billing_System__c,
                CBUCID__c, RCID__c, CustProfId__c, CBUCID_RCID__c, Billing_System_ID__c, BTN__c, CAN__c, BAN_CAN__c, Billing_Account_Active_Indicator__c, CAN_Level__c, BCAN__c, BCAN_Pilot_Org_Indicator__c
                from Account where BAN_CAN__c =: CurrAcc.BCAN__c limit 1
            ];
            system.debug('>>>> Hightest Billing Acc:' + highBillAcc.parentid);
            system.debug('>>>> Hightest Billing Acc BAN_CAN__c:' + highBillAcc.BAN_CAN__c);
            if (highBillAcc != null) {
                Account RCIDParent = [SELECT Id, Name, parentId, RecordType.DeveloperName, Billing_System__c,
                    CBUCID__c, RCID__c, CustProfId__c, CBUCID_RCID__c, Billing_System_ID__c, BTN__c, CAN__c, BAN_CAN__c, Billing_Account_Active_Indicator__c, CAN_Level__c, BCAN__c, BCAN_Pilot_Org_Indicator__c
                    from Account where id =: highBillAcc.parentid limit 1
                ];
                system.debug('>>>> RCIDParent Acc:' + RCIDParent.RCID__c);
                system.debug('>>>> RCIDParent name:' + RCIDParent.name);
                system.debug('>>>> caseRecord.Outgoing_Account_Name:' + caser.AccountID);

                if (RCIDParent != null) {
                    if (caser.AccountID != null) {
                        system.debug('>>>> RCIDParent detail:' + RCIDParent);
                        if (RCIDParent.id == caser.AccountID) {

                            return true;
                        }
                    }
                }




                return false;
            }
        }
        return false;
    }

    public void callSearchAllAtRCID() {
        boolean isSearchValid;
        Case caser = [select id, AccountId from
            case where id = :
                caseID
        ];

        if (AccSearchNumber.length() <= 0 && caser.AccountID != null)
            isSearchValid = true;
        else
            isSearchValid = SearchAndValidateParentRCID(AccSearchNumber);
        if (isSearchValid) {
            map < id, Account > RCIDchilds = new map < id, Account > ([SELECT Id, Name, parentId, RecordType.DeveloperName, Billing_System__c,
                CBUCID__c, RCID__c, CustProfId__c, CBUCID_RCID__c, Billing_System_ID__c, BTN__c, CAN__c, BAN_CAN__c, Billing_Account_Active_Indicator__c, CAN_Level__c, BCAN__c, BCAN_Pilot_Org_Indicator__c
                from Account where parentid =: caser.AccountID AND Inactive__c = false AND Billing_Account_Active_Indicator__c = 'Y'
                limit 50000
            ]);

            system.debug('>>>> RCIDchilds map size: ' + RCIDchilds.size());
            set < String > BCANset = new Set < String > ();
            map < string, list < Account >> BCANvsinnerChilds = new map < string, list < Account >> ();
            list < Account > allAccList = new list < account > ();
            for (Account acc: RCIDchilds.values()) {
                system.debug('>>> RCID child Acc: ' + acc);
                allAccList.add(acc);
            }

            for (Account acc: RCIDchilds.values()) {
                if (acc.BCAN__C != null)
                    BCANset.add(acc.BCAN__C);
                else
                    BCANset.add('NO_BCAN -' + acc.id);
            }
            if (BCANset.size() > 0) {
                for (String s: BCANset) {
                    BCANvsinnerChilds.put(s, new list < Account > {});
                }
            }

            system.debug('>>>> BCANvsInnerchilds map before innershilds query: ' + BCANvsinnerChilds);
            list < Account > RCIDinnerChildlist = [SELECT Id, Name, parentId, RecordType.DeveloperName, Billing_System__c,
                CBUCID__c, RCID__c, CustProfId__c, CBUCID_RCID__c, Billing_System_ID__c, BTN__c, CAN__c, BAN_CAN__c, Billing_Account_Active_Indicator__c, CAN_Level__c, BCAN__c, BCAN_Pilot_Org_Indicator__c
                from Account where BCAN__C in: BCANset AND Inactive__c = false AND Billing_Account_Active_Indicator__c = 'Y'
                AND id NOT in: RCIDchilds.keyset() limit 50000
            ];
            system.debug('>>>> RCID inner childslist count ' + RCIDinnerChildlist.size());
            for (Account acc: RCIDinnerChildlist) {
                if (BCANset.contains(acc.BCAN__C)) {
                    if (BCANvsinnerChilds.get(acc.BCAN__C) != null)
                        BCANvsinnerChilds.get(acc.BCAN__C).add(acc);
                    else
                        BCANvsinnerChilds.put(acc.BCAN__C, new list < account > {
                            acc
                        });
                }
            }

            system.debug('>>>> RCID inner childs map count ' + BCANvsinnerChilds.size());

            if (BCANvsinnerChilds.size() > 0) {
                for (String s: BCANvsinnerChilds.keyset()) {
                    if (BCANvsinnerChilds.get(s).size() > 0)
                        allAcclist.addall(BCANvsinnerChilds.get(s));
                }             

            }
            listaccResult = new list < account > ();
            listaccResult.addall(allAcclist);
            system.debug('>>>>listaccResult from RCID before ca check: ' + listaccResult.size());

            CaseAccSet = new set < id > ();
            CaseAccSet = searchCaseAccSet();
            if (CaseAccSet != null)
                system.debug('>>>> CA set size: ' + CaseAccSet.size());

            map < id, account > newresultAccmap = new map < id, account > (listaccResult);
            listaccResult.clear();
            listaccresult.addall(newresultAccmap.values());

            system.debug('>>>>listaccResult from RCID after ca check: ' + listaccResult.size());
            finalAccList = new map < id, account > (listaccResult);
        }
    }
    public void searchAccountsFromCBN() {

            /************/
            list < Account > accListFromCBN = New list < Account > ();
            Account CBNAcc = new account();
            boolean isSearchValid;

            isSearchValid = SearchAndValidateParentRCID(AccSearchNumber);

            if (isSearchValid) {
                Account currAcc = searchCurrentAcc(AccSearchNumber);

                if (currAcc != null && currAcc.BCAN__c != null && currAcc.BCAN__c.substringAfter('-') != currAcc.CAN__c) {


                    accListFromCBN = [SELECT Id, Name, parentId, RecordType.DeveloperName, Billing_System__c, CBUCID__c, RCID__c, CustProfId__c, CBUCID_RCID__c, Billing_System_ID__c, BTN__c, CAN__c, BAN_CAN__c, Billing_Account_Active_Indicator__c, CAN_Level__c, BCAN_Pilot_Org_Indicator__c
                        from Account where BCAN__c =: currAcc.BCAN__c AND Inactive__c = false AND Billing_Account_Active_Indicator__c = 'Y'
                        limit 50000
                    ];

                    if (accListFromCBN.size() > 0) {

                        //total=accListFromCBN.size();
                        listaccResult = new list < account > ();
                        listaccResult.addall(accListFromCBN);
                        system.debug('>>>>listaccResult from CBN before ca check: ' + listaccResult.size());

                        CaseAccSet = new set < id > ();
                        CaseAccSet = searchCaseAccSet();
                        if (CaseAccSet != null)
                            system.debug('>>>> CA set size: ' + CaseAccSet.size());

                        map < id, account > newresultAccmap = new map < id, account > (listaccResult);
                        listaccResult.clear();
                        listaccresult.addall(newresultAccmap.values());
                        finalAccList = new map < id, account > (listaccResult);
                    }
                }
            } 
        }
       
    public void searchAndAddSingleAccount() {
            boolean isSearchValid;
            isSearchValid = SearchAndValidateParentRCID(AccSearchNumber);
            if (isSearchValid) {

                Account resultAcc = searchCurrentAcc(AccSearchNumber);
                if (resultAcc != null) {

                    listaccResult = New list < account > ();
                    listaccResult.add(resultAcc);

                    CaseAccSet = new set < id > ();
                    CaseAccSet = searchCaseAccSet();
                    if (CaseAccSet != null)
                        system.debug('>>>> CA set size: ' + CaseAccSet.size());

                    map < id, account > newresultAccmap = new map < id, account > (listaccResult);
                    listaccResult.clear();
                    listaccresult.addall(newresultAccmap.values());
                    finalAccList = new map < id, account > (listaccResult);                   
                }
            }
        }
       
    public pagereference CallPdf() {
        string dbquery = '' + getAcccolumns + ' limit 2028';
        system.debug('dbquery  = ' + dbquery);
       
        if (finalAccList != NULL) {
            string AddessDetails = '';
            system.debug('InSide showAccountsDetailsInPDf');

            system.debug('>>>Acc count:' + finalAccList.size());
            AccountCallsMap = new Map < string, List < Account >> ();
            getAddressMap = new Map < id, SMBCare_Address__c > ([select id, Street_Address__c, Street_Number__c, Street_Name__c, State__c, City__c, Account__c, Postal_Code__c from SMBCare_Address__c where account__c =: finalAccList.keyset()]);
            system.debug('>>>Acc count:' + getAddressMap.size());
            List < SMBCare_Address__c > allAddressList = getAddressMap.values();
            Set < Id > AddressList = new Set < Id > ();
            PdfDetailsList = new List < pdfData > ();
            for (SMBCare_Address__c smbc: getAddressMap.values()) {
                AddressList.add(smbc.account__c);
                AddessDetails += 'Services At : ';
                 AddessDetails += (smbc.Street_Address__c == null) ? '-' : smbc.Street_Address__c + ' ';
                AddessDetails += (smbc.City__c == null) ? '-' : smbc.City__c + ' ';
                AddessDetails += (smbc.State__c == null) ? '-' : smbc.State__c + ' ';
                AddessDetails += (smbc.Postal_Code__c == null) ? '-' : smbc.Postal_Code__c + ' ';
               
                system.debug('AddessDetails [to be added] = ' + AddessDetails);


                List < Account > tobeadd = new List < Account > ();

                if (AccountCallsMap.containskey(string.valueof(AddessDetails))) {
                    tobeadd = AccountCallsMap.get(string.valueof(AddessDetails));
                   
                    if (finalAccList.containskey(string.valueof(smbc.account__c))) {
                        tobeadd.add(finalAccList.get(smbc.Account__c));
                        AccountCallsMap.put(string.valueof(AddessDetails), tobeadd);
                    }
                } else {
                    system.debug('Check Accounts IDs' + smbc.Account__c);
                    if (finalAccList.containskey(string.valueof(smbc.account__c))) {
                        tobeadd.add(finalAccList.get(smbc.Account__c));
                        AccountCallsMap.put(string.valueof(AddessDetails), tobeadd);
                    }
                }

                AddessDetails = '';
            }


            List < Account > tobeaddnoAddr = new List < Account > ();
            integer count = 0;
            for (Account acc: finalAccList.values()) {
                if (!AddressList.contains(acc.id)) {
                    tobeaddnoAddr.add(acc);
                }
            }
            if (tobeaddnoAddr.size() != 0) {
                AccountCallsMap.put('no-address', tobeaddnoAddr);
            }
            //system.debug('Count :'+count);
            system.debug('call Maps Size :' + AccountCallsMap.size());
            system.debug('callMaps Details :' + AccountCallsMap);

            getAssetlist = new list < asset > ([select id, name, Type__c, accountId from Asset where accountId =: finalAccList.keyset()]);
            AssestMap = new Map < string, List < asset >> ();
            system.debug('==================');

            If(AccountCallsMap != null) {
                for (asset ass: getAssetlist) {
                    system.debug('ass.account = ' + ass.accountID);
                    List < asset > tobeadd = new List < asset > ();
                    if (AssestMap.containskey(ass.accountID)) {
                        tobeAdd = AssestMap.get(ass.accountID);
                        tobeAdd.add(ass);
                        AssestMap.put(ass.accountID, tobeAdd);
                    } else {
                        tobeAdd.add(ass);
                        AssestMap.put(ass.accountID, tobeAdd);
                    }
                }
            }

            AssestSizeMap = new Map < string, Integer > ();
            for (ID acc: assestMAp.keySet()) {
                List < asset > getAsset = AssestMap.get(acc);
                system.debug('Umesh >>>> acc = ' + acc + 'asses size = ' + getAsset.size());
                AssestSizeMap.put(acc, getAsset.size());
                getAsset.clear();

            }
            addressString = new Set < string > ();
            for (String sNew: AccountCallsMap.keySet()) {
                List < Account > aa = AccountCallsMap.get(sNew);
                for (Account acc: aa) {
                    pdfData pdfd = new pdfData();
                    if (!addressString.contains(sNew)) {
                        addressString.add(sNew);
                        pdfd.address = sNew;
                    }
                    pdfd.btn = acc.BTN__c;
                    pdfd.can = acc.CAN__c;
                    if ((AssestSizeMap).containskey(acc.ID)) {
                        pdfd.wtn = AssestSizeMap.get(acc.ID);                       
                    } else {
                        pdfd.wtn = 0;
                    }
                    PdfDetailsList.add(pdfd);
                }

            }
            system.debug('AssestMap size = ' + AssestMap.size());
            system.debug('AssestMap  = ' + AssestMap);
            system.debug('PdfDetailsList  = ' + PdfDetailsList.size());
        }
        return null;
    }
}