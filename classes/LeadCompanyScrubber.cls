/**
 * Utility class that scrubs Lead Company field in provided Lead records
 * according to configured rules.
 * 
 * @author Max Rudman
 * @since 6/28/2010
 */
public with sharing class LeadCompanyScrubber {
    private List<ScrubRule__c> rules;
    
    public LeadCompanyScrubber() {
        rules = [SELECT Type__c, Value__c FROM ScrubRule__c WHERE Active__c = true];
    }
    
    /**
     * Scrubs specified leads according to loaded rules.
     */
    public void scrub(List<Lead> leads) {
        if (rules.isEmpty()) {
            // Nothing to do.
            return;
        }
        
        for (Lead lead : leads) {
            // Default Scrubbed Company to Company.
            lead.ScrubbedCompany__c = lead.Company;
            for (ScrubRule__c rule : rules) {
                scrub(rule, lead);
            }
        }
    }
    
    private void scrub(ScrubRule__c rule, Lead lead) {
        String value = rule.Value__c;
        if ((value == null) || (lead.ScrubbedCompany__c == null)) {
            return;
        }
        
        value = value.replaceAll('\\.','\\.?');
        
        Pattern p;
        if (rule.Type__c == 'Prefix') {
            p = Pattern.compile('^' + value);
        } else if (rule.Type__c == 'Suffix') {
            p = Pattern.compile(value + '$');
        }
        
        String company = lead.ScrubbedCompany__c;
        Matcher m = p.matcher(company);
        lead.ScrubbedCompany__c = m.replaceAll('').trim();
    }
}