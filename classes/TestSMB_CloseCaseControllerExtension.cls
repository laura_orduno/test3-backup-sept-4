@istest(seealldata=false)
private class TestSMB_CloseCaseControllerExtension{
    @testsetup
    static void setupTestData(){
        map<string,schema.recordtypeinfo> accountRecordTypeInfosMap=account.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        account rcidAccount=new account(name='Test RCID',language_preference__c='English',recordtypeid=accountRecordTypeInfosMap.get('RCID').getrecordtypeid());
        insert rcidAccount;
        contact newContact=new contact(firstname='Test',lastname='TestLastname',email='test.email@testemail.com',accountid=rcidAccount.id,active__c=true);
        insert newContact;
        user escalationUser=[select id from user where care_type__c like '%Escalation%' and isactive=true limit 1];
        case careBillingCase=new case(recordtypeid=case.sobjecttype.getdescribe().getrecordtypeinfosbyname().get('SMB Care Billing').getrecordtypeid(),escalated_case__c=true,escalation_reason__c='Testing only',ownerid=escalationUser.id,accountid=rcidAccount.id,contactid=newContact.id,type='Billing Address Update',subject='Test Care Billing',region__c='ON',description='Test Care Billing');
        insert careBillingCase;
        case careBillingClosedCase=new case(recordtypeid=case.sobjecttype.getdescribe().getrecordtypeinfosbyname().get('SMB Care Billing').getrecordtypeid(),root_cause__c='Billing',root_cause_detail__c='Billing',root_cause_sublist__c='Billing agent behavior, incl. lack of ownership',escalated_case__c=true,escalation_reason__c='Testing only',ownerid=escalationUser.id,accountid=rcidAccount.id,contactid=newContact.id,type='Billing Address Update',subject='Test Care Billing Closed',region__c='ON',description='Test Care Billing',status='Closed');
        insert careBillingClosedCase;
        list<close_case_field_set_assignment__c> fieldSettings=new list<close_case_field_set_assignment__c>();
        fieldSettings.add(new close_case_field_set_assignment__c(name='Default',body_set__c='SMB_Close_Case_Body_Fields',header_set__c='SMB_Close_Case_Header_Fields',default__c=true));
        fieldSettings.add(new close_case_field_set_assignment__c(name='SMB_Escalation',header_set__c='SMB_Escalation_Close_Case_Body_Fields',body_set__c='SMB_Escalation_Close_Case_Header_Fields',default__c=false));
        fieldSettings.add(new close_case_field_set_assignment__c(name='SMB_Escalation_Wireless',header_set__c='SMB_Escalation_Close_Case_Header_Fields',body_set__c='SMB_Escalation_Close_Case_Body_Fields',default__c=false));
        fieldSettings.add(new close_case_field_set_assignment__c(name='SMB_Escalation_Wireline_SalesCare',header_set__c='SMB_Escalation_Close_Case_Header_Fields',body_set__c='SMB_Escalation_Close_Case_Body_Fields',default__c=false));
        insert fieldSettings;
    }
    /**
     * First assign to Escalation queue, then same or another user picking it up and create first task.
     */
    @istest
    static void testCloseEscalationCase(){
        case careBillingCase=[select id,ownerid,escalation_reason__c,escalated_case__c,contactid,isclosed,escalation_queue_to_first_callback__c,closed_by__c,createdbyid,createddate from case where subject='Test Care Billing' limit 1];
        user escalationUser=[select id from user where care_type__c like '%Escalation%' and isactive=true limit 1];
        date caseCreationDate=date.newinstance(2016,5,2);
        time taskCreationTime=time.newinstance(10,5,5,000);
        task firstCallbackTask=new task(recordtypeid=task.sobjecttype.getdescribe().getrecordtypeinfosbyname().get('BCX General Task').getrecordtypeid(),whatid=careBillingCase.id,whoid=careBillingCase.contactid,ownerid=careBillingCase.ownerid,priority='Medium',status='Closed',type='Call',activitydate=caseCreationDate,smb_call_type__c='IB Call - Customer Initiated',subject='First Callback');
        insert firstCallbackTask;
        test.setcreateddate(firstCallbackTask.id,datetime.newinstance(caseCreationDate,taskCreationTime).adddays(1));
        test.starttest();
        pagereference currentPage=page.smb_closecase;
        currentPage.getparameters().put('Id',careBillingCase.id);
        test.setcurrentpage(currentPage);
        apexpages.standardcontroller controller=new apexpages.standardcontroller(careBillingCase);
        smb_CloseCaseControllerExtension ext=new smb_CloseCaseControllerExtension(controller);
        test.stoptest();
    }
    /**
     * No task found Query Exception
     */
    @istest
    static void testNoTaskFoundException(){
        case careBillingCase=[select id,ownerid,escalation_reason__c,escalated_case__c,contactid,isclosed,escalation_queue_to_first_callback__c,closed_by__c,createdbyid,createddate from case where subject='Test Care Billing' limit 1];
        test.starttest();
        pagereference currentPage=page.smb_closecase;
        currentPage.getparameters().put('Id',careBillingCase.id);
        test.setcurrentpage(currentPage);
        apexpages.standardcontroller controller=new apexpages.standardcontroller(careBillingCase);
        smb_CloseCaseControllerExtension ext=new smb_CloseCaseControllerExtension(controller);
        test.stoptest();
    }
    /**
     * Case is already closed.
     */
    @istest
    static void testCaseIsClosedException(){
        case careBillingClosedCase=[select id,ownerid,escalation_reason__c,escalated_case__c,contactid,isclosed,escalation_queue_to_first_callback__c,closed_by__c,createdbyid,createddate,root_cause__c,root_cause_detail__c,root_cause_sublist__c from case where subject='Test Care Billing Closed' limit 1];
        test.starttest();
        pagereference currentPage=page.smb_closecase;
        currentPage.getparameters().put('Id',careBillingClosedCase.id);
        test.setcurrentpage(currentPage);
        apexpages.standardcontroller controller=new apexpages.standardcontroller(careBillingClosedCase);
        smb_CloseCaseControllerExtension ext=new smb_CloseCaseControllerExtension(controller);
        test.stoptest();
    }
}