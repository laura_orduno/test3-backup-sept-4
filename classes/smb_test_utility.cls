/*
###########################################################################
# File..................: smb_test_utility
# Version...............: 1
# Created by............: Vinay Sharma
# Created Date..........: 19-Feb-2013
# Last Modified by......: 
# Last Modified Date....: 
# Description...........: This class contains the methods used to Creating a dummy data for Test classes.
# Change Log:    
############################################################################
 */
public class smb_test_utility {
    public static map<String,RecordType> recordTypeMap;
    
    private static void fillRecordTypeMap(){
         recordTypeMap = new map<String,RecordType>();
         for(RecordType rt: [select id,Name,developerName,SobjectType from RecordType]){
            recordTypeMap.put(rt.developerName+'_'+rt.SobjectType,rt);
         }        
    }
    public static string getRecordTypeId(String DeveloperName, String ObjectAPIName){
        system.debug('In recordType ');
        string recordTypeId = Null;
        if(recordTypeMap == Null){
           fillRecordTypeMap();
        }
        if(DeveloperName <> Null && DeveloperName <> '' && ObjectAPIName <> Null && ObjectAPIName <> ''){
            if(recordTypeMap <> Null && recordTypeMap.containsKey(DeveloperName+'_'+ObjectAPIName)){
                recordTypeId = (recordTypeMap.get(DeveloperName+'_'+ObjectAPIName)).Id;
            }
        
        }
        return recordTypeId;
    }
    public static Account createAccount(String recordType_DeveloperName, boolean doInsert) {
        String recordTypeID = getRecordTypeId(recordType_DeveloperName, 'Account');
        Account testAccount = new Account(Name='AstadiaTestAccount');
        if(recordTypeID <> Null && recordTypeID <> ''){
            testAccount.RecordTypeId = recordTypeID; 
        }
        if(doInsert){
            insert testAccount;
        }
        return testAccount;
    }
    public static User createUser(String profId, boolean doInsert) {
        User u = new User(
                 FirstName = 'FTestName',
                 LastName = 'LTestName',
                 Alias = 'nmbj',
                 Email = 'ghkl@abc.com',
                 phone = '6049969449',
                 Username = 'hjkltner@abc.com',
                 CommunityNickname = 'ouio',
                 emailencodingkey = 'UTF-8',
                 languagelocalekey = 'en_US',
                 localesidkey = 'en_US',                 
                 timezonesidkey = 'America/Los_Angeles',             
                 profileId = profId
             );
        if(doInsert){
            insert u;
        }
        return u;
    }
    public static Contact createContact(String recordType_DeveloperName, String AccountID, boolean doInsert){
        String recordTypeID = getRecordTypeId(recordType_DeveloperName, 'Contact');
        Contact testContact = new Contact(LastName='AstadiaTestContact',Phone = '9876543281' ,
                        Email = 'abc@gmail.com', 
                        HomePhone = '9876543281', 
                        MobilePhone = '9876543281');
        testContact.AccountId = AccountId <> Null && AccountId <> ''?AccountId:createAccount(Null,true).Id;
        if(recordTypeID <> Null && recordTypeID <> ''){
            testContact.RecordTypeId = recordTypeID; 
        }
        if(doInsert){
            insert testContact;
        }
        return testContact;
    }
    public static Opportunity createOpportunity(String recordType_DeveloperName, String AccountId, String ContactId, boolean doInsert){
        String recordTypeID = getRecordTypeId(recordType_DeveloperName, 'Opportunity');
        Opportunity testOpportunity = new Opportunity(Name='AstadiaTestOpportunity',StageName='Quote Accepted',CloseDate=system.Today());
        if(recordTypeID <> Null && recordTypeID <> ''){
            testOpportunity.RecordTypeId = recordTypeID; 
        }
        
        testOpportunity.AccountId = AccountId <> Null && AccountId <> '' ? AccountId : createAccount(Null,true).Id;
        ContactId = ContactId <> Null && ContactId <> '' ? ContactId : createContact(Null,testOpportunity.AccountId,true).Id;
        testOpportunity.Contract_Signor__c = ContactId ;
        
        if(doInsert){
            insert testOpportunity;
        }
        return testOpportunity;
    }
    public static void createPQLI(String fileName,String oppId) {
        SMB_GenericDataLoader genericLoad = new SMB_GenericDataLoader();
        list<scpq__PrimaryQuoteLine__c> lstRecord = genericLoad.makeData('scpq__PrimaryQuoteLine__c',fileName);        
        for(scpq__PrimaryQuoteLine__c pqli:lstRecord){
            pqli.scpq__OpportunityId__c = oppId;
        }
        insert lstRecord;   
    }
    
    public static void createProduct() {
        Product2 product = new Product2(name='AstadiaTestProduct',Sterling_Item_ID__c='p1');
        insert product;
        
        Pricebook2 pricebook = [select id from Pricebook2 where isStandard=true];
        
        PricebookEntry pbe = new PricebookEntry(pricebook2id = pricebook.id, product2id = product.id,unitprice=1.0,UseStandardPrice=true,isActive=true);
        insert pbe;
        // TO DO: implement unit test
    }
    public static Apttus__APTS_Agreement__c createAgreement(String recordType_DeveloperName, boolean doInsert){
        Apttus__APTS_Agreement__c agreement = new Apttus__APTS_Agreement__c();
        
        if(doInsert){
            insert agreement;
        }
        return agreement;
    }
    public static Apttus__AgreementLineItem__c createAgreementLineItem(String recordType_DeveloperName, String AgreementId, boolean doInsert){
        AgreementId = AgreementId <> Null?AgreementId:createAgreement(Null,true).Id;
        Apttus__AgreementLineItem__c agreeLineItem = new Apttus__AgreementLineItem__c(Apttus__AgreementId__c = AgreementId);
        
        if(doInsert){
            insert agreeLineItem;
        }
        return agreeLineItem;
    }
    public static Order_Request__c createOrderRequest(String recordType_DeveloperName, String OpportunityId, boolean doInsert){
        OpportunityId = OpportunityId<>Null ?OpportunityId : createOpportunity(Null,Null,Null,true).Id; 
        Order_Request__c ORC = new Order_Request__c(Opportunity__c = OpportunityId, NC_Order_ID__c ='9137412239213520216', CurrencyIsoCode='USD', Order__c ='New');
        
        if(doInsert){
            insert ORC;
        }
        return ORC;
    }
    public static Work_Order__c createWorkOrder(String recordType_DeveloperName, boolean doInsert){
        Work_Order__c workOrder = new Work_Order__c(Job_Type__c = 'Test' ,Install_Type__c = 'Test');
        if(doInsert){
            insert workOrder;
        }
        return workOrder;
    }
    public static scpq__SciQuote__c createQuote(String recordType_DeveloperName, String opportunityId, boolean doInsert){
        scpq__SciQuote__c quote = new scpq__SciQuote__c(scpq__OpportunityId__c = opportunityId, scpq__Primary__c = true,scpq__OrderHeaderKey__c = '123', scpq__SciLastModifiedDate__c=system.today(),scpq__Status__c = 'Quote');
        if(doInsert){
            insert quote;
        }
        return quote;
    }
    public static void createServiceRequest(String OpportunityId){
        OpportunityId = OpportunityId<>Null ?OpportunityId : createOpportunity(Null,Null,Null,true).Id; 
        list<Order_Request__c> list_OrderRequest= new list<Order_Request__c>();
        list_OrderRequest.add(new  Order_Request__c(NC_Order_ID__c = 'CustTestId1', Opportunity__c=OpportunityId));
        list_OrderRequest.add(new  Order_Request__c(NC_Order_ID__c = 'CustTestId2', Opportunity__c=OpportunityId));
        list_OrderRequest.add(new  Order_Request__c(NC_Order_ID__c = 'CustTestId3', Opportunity__c=OpportunityId));
        list_OrderRequest.add(new  Order_Request__c(NC_Order_ID__c = 'CustTestId4', Opportunity__c=OpportunityId));
        list_OrderRequest.add(new  Order_Request__c(NC_Order_ID__c = 'CustTestId5', Opportunity__c=OpportunityId));
        list_OrderRequest.add(new  Order_Request__c(NC_Order_ID__c = 'CustTestId6', Opportunity__c=OpportunityId));
        list_OrderRequest.add(new  Order_Request__c(NC_Order_ID__c = 'CustTestId7', Opportunity__c=OpportunityId));
        insert list_OrderRequest;
        list<Service_Request__c> lstSerRequests = new list<Service_Request__c>();
        lstSerRequests.add(new Service_Request__c(Opportunity__c=OpportunityId, Service_Address__c='Dummy Service Address', Scheduled_Due_Date__c=Datetime.now(),Order_Label__c='test Order', External_Order_ID__c='ExtTestId1', Customer_Order_ID__c='CustTestId1', CurrencyIsoCode='CAD', Status__c='Order Entry is Completed', Product_ID__c='Test1'));
        lstSerRequests.add(new Service_Request__c(Opportunity__c=OpportunityId, Service_Address__c='Dummy Service Address', Scheduled_Due_Date__c=Datetime.now(),Order_Label__c='test Order', External_Order_ID__c='ExtTestId2', Customer_Order_ID__c='CustTestId2', CurrencyIsoCode='CAD', Status__c='Order Received', Product_ID__c='Test2'));
        lstSerRequests.add(new Service_Request__c(Opportunity__c=OpportunityId, Service_Address__c='Dummy Service Address', Scheduled_Due_Date__c=Datetime.now(),Order_Label__c='test Order', External_Order_ID__c='ExtTestId2', Customer_Order_ID__c='CustTestId3', CurrencyIsoCode='CAD', Status__c='Provisioning is Completed', Product_ID__c='Test3'));
        lstSerRequests.add(new Service_Request__c(Opportunity__c=OpportunityId, Service_Address__c='Dummy Service Address', Scheduled_Due_Date__c=Datetime.now(),Order_Label__c='test Order', External_Order_ID__c='ExtTestId2', Customer_Order_ID__c='CustTestId4', CurrencyIsoCode='CAD', Status__c='Critical Fulfillment Error', Product_ID__c='Test4'));
        lstSerRequests.add(new Service_Request__c(Opportunity__c=OpportunityId, Service_Address__c='Dummy Service Address', Scheduled_Due_Date__c=Datetime.now(),Order_Label__c='test Order', External_Order_ID__c='ExtTestId2', Customer_Order_ID__c='CustTestId5', CurrencyIsoCode='CAD', Status__c='Order Details Missing', Product_ID__c='Test5'));
        lstSerRequests.add(new Service_Request__c(Opportunity__c=OpportunityId, Service_Address__c='Dummy Service Address', Scheduled_Due_Date__c=Datetime.now(),Order_Label__c='test Order', External_Order_ID__c='ExtTestId2', Customer_Order_ID__c='CustTestId6', CurrencyIsoCode='CAD', Status__c='BO Hold', Product_ID__c='Test6'));
        lstSerRequests.add(new Service_Request__c(Opportunity__c=OpportunityId, Service_Address__c='Dummy Service Address', Scheduled_Due_Date__c=Datetime.now(),Order_Label__c='test Order', External_Order_ID__c='ExtTestId2', Customer_Order_ID__c='CustTestId7', CurrencyIsoCode='CAD', Status__c='Suspended', Product_ID__c='Test7'));
        insert lstSerRequests;
    }
    public static void createCustomSettingData(){
        
        list<SMBCare_WebServices__c> listCustomSetting =  new list<SMBCare_WebServices__c>();
        listCustomSetting.add(new SMBCare_WebServices__c(Name='SMB_SubmitOrder EndPoint',value__c='abc'));
        listCustomSetting.add(new SMBCare_WebServices__c(Name='BookDueDate_EndPoint',value__c='https://partnerservices-pt.telus.com:443/SOA/RMO/ProcessMgmt/FieldWorkAssignmentMgmtService_v2_0_vs1_vs0'));
        listCustomSetting.add(new SMBCare_WebServices__c(Name='username',value__c='uname'));
        listCustomSetting.add(new SMBCare_WebServices__c(Name='password',value__c='pwd'));
        listCustomSetting.add(new SMBCare_WebServices__c(Name='SMB_LegacyOrderEndpoint',value__c='pwd'));
        listCustomSetting.add(new SMBCare_WebServices__c(Name='SMB_SuspendOrderEndpoint',value__c='pwd'));
        listCustomSetting.add(new SMBCare_WebServices__c(Name='SMB_SuspendOrderSoapAction',value__c='pwd'));
        listCustomSetting.add(new SMBCare_WebServices__c(Name='SMB_CancelOrderEndpoint',value__c='pwd'));
        listCustomSetting.add(new SMBCare_WebServices__c(Name='SMB_CancelOrderSoapAction',value__c='pwd'));
        listCustomSetting.add(new SMBCare_WebServices__c(Name='Contract_EndPoint_v3',value__c='https://xmlgwy-pt1.telus.com:9030/pt01/CMO/ContractMgmt/ContractMgmtSvc_v3_0_vs0'));
        listCustomSetting.add(new SMBCare_WebServices__c(Name='TTODS_EndPoint',value__c='https://xmlgwy-pt1.telus.com:9030/st01/SMO/ProblemMgmt/TroubleTicketODSQueryService_v1_5_vs0'));
        listCustomSetting.add(new SMBCare_WebServices__c(Name='SOA6_TTODS_EndPoint',value__c='https://xmlgwy-pt1.telus.com:9030/st01/SMO/ProblemMgmt/TroubleTicketODSQueryService_v1_5_vs0'));
        listCustomSetting.add(new SMBCare_WebServices__c(Name='SMB_BundleRegistration',value__c='EndPoint'));
        listCustomSetting.add(new SMBCare_WebServices__c(Name='SearchDueDate_EndPoint',value__c='EndPoint'));
        listCustomSetting.add(new SMBCare_WebServices__c(Name='SMB_ResumeOrderEndpoint',value__c='https://partnerservices-pt.telus.com:443/SOA/CMO/OrderMgmt/ResumeCustomerOrder_v2_1_vs0_vs0'));
        listCustomSetting.add(new SMBCare_WebServices__c(Name='SMB_ResumeOrderSoapAction',value__c='http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/fulfillment/ResumeCustomerOrder/resumeCustomerOrder'));
        Database.insert(listCustomSetting,false);
        list<SMB_DebugWebServices__c>  listCustomSettingDebugWebServices = new list<SMB_DebugWebServices__c>();
        listCustomSettingDebugWebServices.add(new SMB_DebugWebServices__c(Name = UserInfo.getUserName(),SubmitOrder__c = true,EndPoint__c = 'Test'));
        Database.insert(listCustomSettingDebugWebServices,false);
    } 
}