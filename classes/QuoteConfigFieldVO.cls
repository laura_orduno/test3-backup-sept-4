public without sharing class QuoteConfigFieldVO {
	public String fieldName {get; set;}
	public String name {get; set;}
	public Decimal displayOrder {get; set;}
	public String dealerRequiredStage {get; set;}
	public String internalRequiredStage {get; set;}
	public String messageBelow {get; set;}
	public String errorMessage {get; set;}
	public String cssStyle {get; set;}
	public Boolean initVisible {get; set;}

	public QuoteConfigFieldVO(Schema.SObjectField field, Decimal displayOrder, String requiredStage, Boolean initVisible, String cssStyle, String errorMessage) {
		this(field, displayOrder, requiredStage, requiredStage, initVisible, cssStyle, errorMessage);
	}

	public QuoteConfigFieldVO(Schema.SObjectField field, Decimal displayOrder, String dealerRequiredStage, String internalRequiredStage, Boolean initVisible, String cssStyle, String errorMessage) {
		this.fieldName = field.getDescribe().getName();
		this.name = field.getDescribe().getLabel();
		this.displayOrder = displayOrder;
		this.dealerRequiredStage = dealerRequiredStage;
		this.internalRequiredStage = internalRequiredStage;
		this.initVisible = initVisible;
		this.messageBelow = field.getDescribe().getInlineHelpText();
		this.cssStyle = cssStyle;
		this.errorMessage = errorMessage;
	}
	
	public String getRequiredStage() {
		String stage;
		if(QuotePortalUtils.isUserDealer()) {
			stage = dealerRequiredStage;
		} else {
			stage = internalRequiredStage;
		}
		return stage;
	}
}