public without sharing class GenerateQuoteDocumentController {
	public QuoteVO[] quotes {get; private set;}
	public SBQQ__QuoteTemplate__c template {get; private set;}
	public Boolean hasQuote {get{return !quotes.isEmpty();}}
	
	public GenerateQuoteDocumentController(ApexPages.StandardController stdController) {
		Id quoteId = stdController.getId();
		loadQuotes(new List<Id>{quoteId});
	}
	
	public GenerateQuoteDocumentController() {
		String qids = ApexPages.currentPage().getParameters().get('qids');
		Id templateId = ApexPages.currentPage().getParameters().get('tid');
		List<String> quoteIds = new List<String>(qids.split(','));
		loadQuotes(quoteIds);
		loadTemplate(templateId);
	}
	
	private void loadQuotes(List<String> quoteIds) {
		quotes = new List<QuoteVO>();
		for (SBQQ__Quote__c quote : QuoteUtilities.loadQuotes(quoteIds)) {
			QuoteVO qvo = new QuoteVO(quote);
			qvo.bundleView = new QuoteBundleView(quote.Id);
			quotes.add(qvo);
		}
	}
	
	private void loadTemplate(Id templateId) {
		template = [SELECT Name, SBQQ__TopMargin__c, SBQQ__LeftMargin__c,
		            	SBQQ__RightMargin__c, SBQQ__BottomMargin__c,
		            	SBQQ__PageWidth__c, SBQQ__PageHeight__c,
		            	SBQQ__PageOrientation__c, SBQQ__TermsConditions__c, Terms_URL__c
		            FROM SBQQ__QuoteTemplate__c WHERE Id = :templateId];
	}
	
	public String getTermsLinkTag() {
		return '<basic-link external-destination="url(\''
            + template.Terms_URL__c + '\')" text-decoration="underline" color="blue">' + template.Terms_URL__c + '</basic-link>';
	}
	
}