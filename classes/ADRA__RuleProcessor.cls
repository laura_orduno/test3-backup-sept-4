/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global virtual class RuleProcessor {
global class MatchResult {
    global MatchResult(SObject record) {

    }
    global MatchResult(SObject record, ADRA.AllocationRule baseRule, ADRA.AllocationRule extensionRule) {

    }
}
}
