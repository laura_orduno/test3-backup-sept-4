/*
Test class for Submit and Cancel Order
 */
@isTest
private class OCOM_SubmitAndCancelOrder_test {

      
    static testMethod void testmethodforTpInventoryResourceConfigV3() {
    	TpInventoryResourceConfigV3  commonBase = new TpInventoryResourceConfigV3();
    	TpInventoryResourceConfigV3.LogicalResource v1= new TpInventoryResourceConfigV3.LogicalResource();
        TpInventoryResourceConfigV3.ManagedEntity v2  = new TpInventoryResourceConfigV3.ManagedEntity();
        TpInventoryResourceConfigV3.PhysicalResource v3  = new TpInventoryResourceConfigV3.PhysicalResource();
        TpInventoryResourceConfigV3.Resource v4= new TpInventoryResourceConfigV3.Resource();
        TpInventoryResourceConfigV3.SupplierPartnerProduct v5 = new TpInventoryResourceConfigV3.SupplierPartnerProduct();
    }
    
   static testMethod void testmethodforTpInventoryServiceConfigExtV3() {
    	TpInventoryServiceConfigExtV3  commonBase = new TpInventoryServiceConfigExtV3();
    	TpInventoryServiceConfigExtV3.CustomerFacingServiceExtensions v1= new TpInventoryServiceConfigExtV3.CustomerFacingServiceExtensions();
        TpInventoryServiceConfigExtV3.ResourceFacingServiceExtensions v2  = new TpInventoryServiceConfigExtV3.ResourceFacingServiceExtensions();
        TpInventoryServiceConfigExtV3.ServiceExtensions v3  = new TpInventoryServiceConfigExtV3.ServiceExtensions();
   } 
   
    static testMethod void testmethodforTpInventoryServiceConfigV3() {
    	TpInventoryServiceConfigV3  commonBase = new TpInventoryServiceConfigV3();
    	TpInventoryServiceConfigV3.CustomerFacingService v1= new TpInventoryServiceConfigV3.CustomerFacingService();
        TpInventoryServiceConfigV3.ResourceFacingService v2  = new TpInventoryServiceConfigV3.ResourceFacingService();
        TpInventoryServiceConfigV3.Service v3  = new TpInventoryServiceConfigV3.Service();
   } 
    
   static testMethod void testmethodforTpTemplatesBusinessInteractionExtV3() {

    	TpTemplatesBusinessInteractionExtV3  commonBase = new TpTemplatesBusinessInteractionExtV3();
    	TpTemplatesBusinessInteractionExtV3.BandwidthProfileLogicalResource v1= new TpTemplatesBusinessInteractionExtV3.BandwidthProfileLogicalResource();
        TpTemplatesBusinessInteractionExtV3.BasicMobileVoiceProduct v2  = new TpTemplatesBusinessInteractionExtV3.BasicMobileVoiceProduct();
        TpTemplatesBusinessInteractionExtV3.BasicPhysicalResource v3  = new TpTemplatesBusinessInteractionExtV3.BasicPhysicalResource();
        TpTemplatesBusinessInteractionExtV3.BasicPortPhysicalResource v4= new TpTemplatesBusinessInteractionExtV3.BasicPortPhysicalResource();
        TpTemplatesBusinessInteractionExtV3.BasicResidentialVoIPProduct v5 = new TpTemplatesBusinessInteractionExtV3.BasicResidentialVoIPProduct();
        TpTemplatesBusinessInteractionExtV3.BasicVoIPCustomerFacingService v6 = new TpTemplatesBusinessInteractionExtV3.BasicVoIPCustomerFacingService();
        TpTemplatesBusinessInteractionExtV3.BusinessInternetProduct v7 = new TpTemplatesBusinessInteractionExtV3.BusinessInternetProduct();
        TpTemplatesBusinessInteractionExtV3.BusinessIPTVRealTimeChargingResourceFacingService v8 = new TpTemplatesBusinessInteractionExtV3.BusinessIPTVRealTimeChargingResourceFacingService();
        TpTemplatesBusinessInteractionExtV3.CDMAResourceFacingService v9= new TpTemplatesBusinessInteractionExtV3.CDMAResourceFacingService();
		
        TpTemplatesBusinessInteractionExtV3.CustomizeTVPackageProduct v10= new TpTemplatesBusinessInteractionExtV3.CustomizeTVPackageProduct();
        TpTemplatesBusinessInteractionExtV3.ChannelListingLogicalResource v12  = new TpTemplatesBusinessInteractionExtV3.ChannelListingLogicalResource();
        TpTemplatesBusinessInteractionExtV3.ChannelPackageProfileLogicalResource v13  = new TpTemplatesBusinessInteractionExtV3.ChannelPackageProfileLogicalResource();
        TpTemplatesBusinessInteractionExtV3.ChannelTypeListingLogicalResource v14= new TpTemplatesBusinessInteractionExtV3.ChannelTypeListingLogicalResource();
        TpTemplatesBusinessInteractionExtV3.ConnectivityLinkProduct v15 = new TpTemplatesBusinessInteractionExtV3.ConnectivityLinkProduct();
        TpTemplatesBusinessInteractionExtV3.ContentFileLogicalResource v16 = new TpTemplatesBusinessInteractionExtV3.ContentFileLogicalResource();
        TpTemplatesBusinessInteractionExtV3.ContentObjectLogicalResource v17 = new TpTemplatesBusinessInteractionExtV3.ContentObjectLogicalResource();
        TpTemplatesBusinessInteractionExtV3.CustomerFacingServiceChoice v18 = new TpTemplatesBusinessInteractionExtV3.CustomerFacingServiceChoice();
        TpTemplatesBusinessInteractionExtV3.CustomizedTVPackageProductOffering v19= new TpTemplatesBusinessInteractionExtV3.CustomizedTVPackageProductOffering();
		
		TpTemplatesBusinessInteractionExtV3.DiskSpaceLogicalResource v21= new TpTemplatesBusinessInteractionExtV3.DiskSpaceLogicalResource();
        TpTemplatesBusinessInteractionExtV3.DomainNameMailboxCustomerFacingService v22  = new TpTemplatesBusinessInteractionExtV3.DomainNameMailboxCustomerFacingService();
        TpTemplatesBusinessInteractionExtV3.DomainNameMediationProduct v23  = new TpTemplatesBusinessInteractionExtV3.DomainNameMediationProduct();
        TpTemplatesBusinessInteractionExtV3.DomainNameMediationSupplierPartnerProduct v24= new TpTemplatesBusinessInteractionExtV3.DomainNameMediationSupplierPartnerProduct();
        TpTemplatesBusinessInteractionExtV3.DomainNameRegistrationCustomerFacingService v25 = new TpTemplatesBusinessInteractionExtV3.DomainNameRegistrationCustomerFacingService();
        TpTemplatesBusinessInteractionExtV3.DomainNameRegistrationProduct v26 = new TpTemplatesBusinessInteractionExtV3.DomainNameRegistrationProduct();
        TpTemplatesBusinessInteractionExtV3.DomainNameRegistrationResourceFacingService v27 = new TpTemplatesBusinessInteractionExtV3.DomainNameRegistrationResourceFacingService();
        TpTemplatesBusinessInteractionExtV3.DomainNameRegistrationSupplierPartnerProduct v28 = new TpTemplatesBusinessInteractionExtV3.DomainNameRegistrationSupplierPartnerProduct();
        TpTemplatesBusinessInteractionExtV3.DropPortPhysicalResource v29= new TpTemplatesBusinessInteractionExtV3.DropPortPhysicalResource();
        
		TpTemplatesBusinessInteractionExtV3.DSLAMPortLogicalResource v31= new TpTemplatesBusinessInteractionExtV3.DSLAMPortLogicalResource();
        TpTemplatesBusinessInteractionExtV3.EDGEResourceFacingService v32  = new TpTemplatesBusinessInteractionExtV3.EDGEResourceFacingService();
        TpTemplatesBusinessInteractionExtV3.EgressProfileLogicalResource v33  = new TpTemplatesBusinessInteractionExtV3.EgressProfileLogicalResource();
        TpTemplatesBusinessInteractionExtV3.eMailMailboxCustomerFacingService v34= new TpTemplatesBusinessInteractionExtV3.eMailMailboxCustomerFacingService();
        TpTemplatesBusinessInteractionExtV3.eMailMailboxProduct v35 = new TpTemplatesBusinessInteractionExtV3.eMailMailboxProduct();
        TpTemplatesBusinessInteractionExtV3.EthernetVLANSubinterfaceLogicalResource v36 = new TpTemplatesBusinessInteractionExtV3.EthernetVLANSubinterfaceLogicalResource();
        TpTemplatesBusinessInteractionExtV3.ExampleVirtualCashProduct v37 = new TpTemplatesBusinessInteractionExtV3.ExampleVirtualCashProduct();
        TpTemplatesBusinessInteractionExtV3.ExistingPhoneNumberPortProduct v38 = new TpTemplatesBusinessInteractionExtV3.ExistingPhoneNumberPortProduct();
        TpTemplatesBusinessInteractionExtV3.FixedDataConnectivityResourceFacingService v39= new TpTemplatesBusinessInteractionExtV3.FixedDataConnectivityResourceFacingService();		
		
		TpTemplatesBusinessInteractionExtV3.FixedVideoConnectivityResourceFacingService v41= new TpTemplatesBusinessInteractionExtV3.FixedVideoConnectivityResourceFacingService();
        TpTemplatesBusinessInteractionExtV3.FixedVoIPConnectivityResourceFacingService v42  = new TpTemplatesBusinessInteractionExtV3.FixedVoIPConnectivityResourceFacingService();
        TpTemplatesBusinessInteractionExtV3.GbEPortPhysicalResource v43  = new TpTemplatesBusinessInteractionExtV3.GbEPortPhysicalResource();
        TpTemplatesBusinessInteractionExtV3.GigEStackedVLANResourceFacingService v44= new TpTemplatesBusinessInteractionExtV3.GigEStackedVLANResourceFacingService();
        TpTemplatesBusinessInteractionExtV3.GigEVLANResourceFacingService v45 = new TpTemplatesBusinessInteractionExtV3.GigEVLANResourceFacingService();
        TpTemplatesBusinessInteractionExtV3.GPRSResourceFacingService v46 = new TpTemplatesBusinessInteractionExtV3.GPRSResourceFacingService();
        TpTemplatesBusinessInteractionExtV3.GSM2GNetworkLogicalResource v47 = new TpTemplatesBusinessInteractionExtV3.GSM2GNetworkLogicalResource();
        TpTemplatesBusinessInteractionExtV3.GSM2GResourceFacingService v48 = new TpTemplatesBusinessInteractionExtV3.GSM2GResourceFacingService();
        TpTemplatesBusinessInteractionExtV3.GSM3GNetworkLogicalResource v49= new TpTemplatesBusinessInteractionExtV3.GSM3GNetworkLogicalResource();
		
		TpTemplatesBusinessInteractionExtV3.GSM3GResourceFacingService v51= new TpTemplatesBusinessInteractionExtV3.GSM3GResourceFacingService();
        TpTemplatesBusinessInteractionExtV3.GSMResourceFacingService v52  = new TpTemplatesBusinessInteractionExtV3.GSMResourceFacingService();
        TpTemplatesBusinessInteractionExtV3.HDIPTVReceiverLogicalResource v53  = new TpTemplatesBusinessInteractionExtV3.HDIPTVReceiverLogicalResource();
        TpTemplatesBusinessInteractionExtV3.HDTVChannelLogicalResource v54= new TpTemplatesBusinessInteractionExtV3.HDTVChannelLogicalResource();
        TpTemplatesBusinessInteractionExtV3.IMAPeMailResourceFacingService v55 = new TpTemplatesBusinessInteractionExtV3.IMAPeMailResourceFacingService();
        TpTemplatesBusinessInteractionExtV3.IMAPortPhysicalResource v56 = new TpTemplatesBusinessInteractionExtV3.IMAPortPhysicalResource();
        TpTemplatesBusinessInteractionExtV3.IMSVoiceMailResourceFacingService v57 = new TpTemplatesBusinessInteractionExtV3.IMSVoiceMailResourceFacingService();
        TpTemplatesBusinessInteractionExtV3.IMSVoIPResourceFacingService v58 = new TpTemplatesBusinessInteractionExtV3.IMSVoIPResourceFacingService();
        TpTemplatesBusinessInteractionExtV3.IngressProfileLogicalResource v59= new TpTemplatesBusinessInteractionExtV3.IngressProfileLogicalResource();
		
		TpTemplatesBusinessInteractionExtV3.InternetAccessResourceFacingService v61= new TpTemplatesBusinessInteractionExtV3.InternetAccessResourceFacingService();
        TpTemplatesBusinessInteractionExtV3.IPTVResourceFacingService v62  = new TpTemplatesBusinessInteractionExtV3.IPTVResourceFacingService();
        TpTemplatesBusinessInteractionExtV3.LogicalResourceChoice v63  = new TpTemplatesBusinessInteractionExtV3.LogicalResourceChoice();
        TpTemplatesBusinessInteractionExtV3.ManufacturerModel1EthernetSwitch10GbEPortPhysicalResource v64= new TpTemplatesBusinessInteractionExtV3.ManufacturerModel1EthernetSwitch10GbEPortPhysicalResource();
        TpTemplatesBusinessInteractionExtV3.ManufacturerModel1EthernetSwitchGbEPortPhysicalResource v65 = new TpTemplatesBusinessInteractionExtV3.ManufacturerModel1EthernetSwitchGbEPortPhysicalResource();
        TpTemplatesBusinessInteractionExtV3.ManufacturerModel1EthernetSwitchPhysicalResource v66 = new TpTemplatesBusinessInteractionExtV3.ManufacturerModel1EthernetSwitchPhysicalResource();
        TpTemplatesBusinessInteractionExtV3.MasterCatalogLogicalResource v67 = new TpTemplatesBusinessInteractionExtV3.MasterCatalogLogicalResource();
        TpTemplatesBusinessInteractionExtV3.MediaroomResourceFacingService v68 = new TpTemplatesBusinessInteractionExtV3.MediaroomResourceFacingService();
        TpTemplatesBusinessInteractionExtV3.MobileInternet25ProductOffering v69= new TpTemplatesBusinessInteractionExtV3.MobileInternet25ProductOffering();
		
		TpTemplatesBusinessInteractionExtV3.MobileInternet50ProductOffering v71= new TpTemplatesBusinessInteractionExtV3.MobileInternet50ProductOffering();
        TpTemplatesBusinessInteractionExtV3.MobileInternetCustomerFacingService v72  = new TpTemplatesBusinessInteractionExtV3.MobileInternetCustomerFacingService();
        TpTemplatesBusinessInteractionExtV3.MobileInternetProduct v73  = new TpTemplatesBusinessInteractionExtV3.MobileInternetProduct();
        TpTemplatesBusinessInteractionExtV3.MobileVoiceAndInternetProductOffering v74= new TpTemplatesBusinessInteractionExtV3.MobileVoiceAndInternetProductOffering();
        TpTemplatesBusinessInteractionExtV3.MobileVoiceCustomerFacingService v75 = new TpTemplatesBusinessInteractionExtV3.MobileVoiceCustomerFacingService();
        TpTemplatesBusinessInteractionExtV3.MovieCategoryCatalogLogicalResource v76 = new TpTemplatesBusinessInteractionExtV3.MovieCategoryCatalogLogicalResource();
        TpTemplatesBusinessInteractionExtV3.MovieGenreCatalogLogicalResource v77 = new TpTemplatesBusinessInteractionExtV3.MovieGenreCatalogLogicalResource();
        TpTemplatesBusinessInteractionExtV3.MusicGenreCatalogLogicalResource v78 = new TpTemplatesBusinessInteractionExtV3.MusicGenreCatalogLogicalResource();
        TpTemplatesBusinessInteractionExtV3.NetworkToNetworkInterfaceLogicalResource v79= new TpTemplatesBusinessInteractionExtV3.NetworkToNetworkInterfaceLogicalResource();
		
		TpTemplatesBusinessInteractionExtV3.ResidentialVoIPProductOffering v81= new TpTemplatesBusinessInteractionExtV3.ResidentialVoIPProductOffering();
        TpTemplatesBusinessInteractionExtV3.ResourceFacingServiceChoice v82  = new TpTemplatesBusinessInteractionExtV3.ResourceFacingServiceChoice();
        TpTemplatesBusinessInteractionExtV3.SDTVChannelLogicalResource v83  = new TpTemplatesBusinessInteractionExtV3.SDTVChannelLogicalResource();
        TpTemplatesBusinessInteractionExtV3.SIMCardPhysicalResource v84= new TpTemplatesBusinessInteractionExtV3.SIMCardPhysicalResource();
        TpTemplatesBusinessInteractionExtV3.SiteInternetServiceCustomerFacingService v85 = new TpTemplatesBusinessInteractionExtV3.SiteInternetServiceCustomerFacingService();
        TpTemplatesBusinessInteractionExtV3.StandardIPTVReceiverLogicalResource v86 = new TpTemplatesBusinessInteractionExtV3.StandardIPTVReceiverLogicalResource();
        TpTemplatesBusinessInteractionExtV3.SupplierPartnerProductChoice v87 = new TpTemplatesBusinessInteractionExtV3.SupplierPartnerProductChoice();
        TpTemplatesBusinessInteractionExtV3.TDMAResourceFacingService v88 = new TpTemplatesBusinessInteractionExtV3.TDMAResourceFacingService();
        TpTemplatesBusinessInteractionExtV3.TextMessagingCustomerFacingService v89= new TpTemplatesBusinessInteractionExtV3.TextMessagingCustomerFacingService();
		
		
		TpTemplatesBusinessInteractionExtV3.ThemePackLogicalResource v91= new TpTemplatesBusinessInteractionExtV3.ThemePackLogicalResource();
        TpTemplatesBusinessInteractionExtV3.TopPicksProduct v92  = new TpTemplatesBusinessInteractionExtV3.TopPicksProduct();
        TpTemplatesBusinessInteractionExtV3.TopPicksProductOffering v93  = new TpTemplatesBusinessInteractionExtV3.TopPicksProductOffering();
        TpTemplatesBusinessInteractionExtV3.TVChannelCatalogLogicalResource v94= new TpTemplatesBusinessInteractionExtV3.TVChannelCatalogLogicalResource();
        TpTemplatesBusinessInteractionExtV3.TVChannelProfileLogicalResource v95 = new TpTemplatesBusinessInteractionExtV3.TVChannelProfileLogicalResource();
        TpTemplatesBusinessInteractionExtV3.TVCustomerFacingService v96 = new TpTemplatesBusinessInteractionExtV3.TVCustomerFacingService();
        TpTemplatesBusinessInteractionExtV3.TVProgrammingCatalogLogicalResource v97 = new TpTemplatesBusinessInteractionExtV3.TVProgrammingCatalogLogicalResource();
        TpTemplatesBusinessInteractionExtV3.UICCCardPhysicalResource v98 = new TpTemplatesBusinessInteractionExtV3.UICCCardPhysicalResource();
        TpTemplatesBusinessInteractionExtV3.UMTSResourceFacingService v99= new TpTemplatesBusinessInteractionExtV3.UMTSResourceFacingService();
		
		TpTemplatesBusinessInteractionExtV3.UnixWebsiteResourceFacingService v101= new TpTemplatesBusinessInteractionExtV3.UnixWebsiteResourceFacingService();
        TpTemplatesBusinessInteractionExtV3.VirtualCashCustomerFacingService v102  = new TpTemplatesBusinessInteractionExtV3.VirtualCashCustomerFacingService();
        TpTemplatesBusinessInteractionExtV3.VoiceAndText100ProductOffering v103  = new TpTemplatesBusinessInteractionExtV3.VoiceAndText100ProductOffering();
        TpTemplatesBusinessInteractionExtV3.VoiceAndText50ProductOffering v104= new TpTemplatesBusinessInteractionExtV3.VoiceAndText50ProductOffering();
        TpTemplatesBusinessInteractionExtV3.POPeMailResourceFacingService v105 = new TpTemplatesBusinessInteractionExtV3.POPeMailResourceFacingService();
        TpTemplatesBusinessInteractionExtV3.VoiceAndText75ProductOffering v106 = new TpTemplatesBusinessInteractionExtV3.VoiceAndText75ProductOffering();
        TpTemplatesBusinessInteractionExtV3.ProductOfferingChoice v107 = new TpTemplatesBusinessInteractionExtV3.ProductOfferingChoice();
        TpTemplatesBusinessInteractionExtV3.VoiceIPServiceResourceFacingService v108 = new TpTemplatesBusinessInteractionExtV3.VoiceIPServiceResourceFacingService();
        TpTemplatesBusinessInteractionExtV3.VoiceMailBoxProduct v109= new TpTemplatesBusinessInteractionExtV3.VoiceMailBoxProduct();
			
		TpTemplatesBusinessInteractionExtV3.VoiceProfileLogicalResource v111= new TpTemplatesBusinessInteractionExtV3.VoiceProfileLogicalResource();
        TpTemplatesBusinessInteractionExtV3.WANProfileLogicalResource v112  = new TpTemplatesBusinessInteractionExtV3.WANProfileLogicalResource();
        TpTemplatesBusinessInteractionExtV3.WebsiteCustomerFacingService v113  = new TpTemplatesBusinessInteractionExtV3.WebsiteCustomerFacingService();
        TpTemplatesBusinessInteractionExtV3.WebsiteProduct v114= new TpTemplatesBusinessInteractionExtV3.WebsiteProduct();
        TpTemplatesBusinessInteractionExtV3.WindowsWebsiteResourceFacingService v115 = new TpTemplatesBusinessInteractionExtV3.WindowsWebsiteResourceFacingService();
	} 

	static testMethod void testmethodforTpCommonBaseExtV3() {
    	TpCommonBaseExtV3  commonBase = new TpCommonBaseExtV3();
    	TpCommonBaseExtV3.CharacteristicExtensions v1 = new TpCommonBaseExtV3.CharacteristicExtensions();
        TpCommonBaseExtV3.CharacteristicValueExtensions v2  = new TpCommonBaseExtV3.CharacteristicValueExtensions();
        TpCommonBaseExtV3.DependencyExtensions v3 = new TpCommonBaseExtV3.DependencyExtensions();
        TpCommonBaseExtV3.EntityExtensions v4= new TpCommonBaseExtV3.EntityExtensions();
        TpCommonBaseExtV3.EntitySpecificationExtensions v5= new TpCommonBaseExtV3.EntitySpecificationExtensions();
        TpCommonBaseExtV3.EntityWithSpecificationExtensions v6 = new TpCommonBaseExtV3.EntityWithSpecificationExtensions();
        TpCommonBaseExtV3.IbmTelecomObjectExtensions v7= new TpCommonBaseExtV3.IbmTelecomObjectExtensions();
        
    }
    
    
    
    static testMethod void testmethodforTpCommonBaseV3() {
    	TpCommonBaseV3  commonBase = new TpCommonBaseV3();
    	TpCommonBaseV3.Characteristic v1= new TpCommonBaseV3.Characteristic();
        TpCommonBaseV3.CharacteristicValue v2 = new TpCommonBaseV3.CharacteristicValue();
        TpCommonBaseV3.EntitySpecification v3 = new TpCommonBaseV3.EntitySpecification();
        TpCommonBaseV3.EntityWithSpecification v4= new TpCommonBaseV3.EntityWithSpecification();
        
    }
    
    static testMethod void testmethodforTpCommonBusinessInteractionExtV3() {
    	TpCommonBusinessInteractionExtV3  commonBase = new TpCommonBusinessInteractionExtV3();
    	TpCommonBusinessInteractionExtV3.BusinessEventExtensions v1= new TpCommonBusinessInteractionExtV3.BusinessEventExtensions();
        TpCommonBusinessInteractionExtV3.BusinessInteractionEntityChoice v2  = new TpCommonBusinessInteractionExtV3.BusinessInteractionEntityChoice();
        TpCommonBusinessInteractionExtV3.BusinessInteractionEntityExtensions v3  = new TpCommonBusinessInteractionExtV3.BusinessInteractionEntityExtensions();
        TpCommonBusinessInteractionExtV3.BusinessInteractionExtensions v4= new TpCommonBusinessInteractionExtV3.BusinessInteractionExtensions();
        TpCommonBusinessInteractionExtV3.BusinessInteractionItemExtensions v5 = new TpCommonBusinessInteractionExtV3.BusinessInteractionItemExtensions();
        TpCommonBusinessInteractionExtV3.RequestExtensions v6 = new TpCommonBusinessInteractionExtV3.RequestExtensions();
        TpCommonBusinessInteractionExtV3.SiteExtensions v7 = new TpCommonBusinessInteractionExtV3.SiteExtensions();
        TpCommonBusinessInteractionExtV3.StatusNotificationExtensions v8 = new TpCommonBusinessInteractionExtV3.StatusNotificationExtensions();
        TpCommonBusinessInteractionExtV3.StatusNotificationItemExtensions v9= new TpCommonBusinessInteractionExtV3.StatusNotificationItemExtensions();
        
    }
    
    static testMethod void testmethodforTpCommonBusinessInteractionV3() {
    	TpCommonBusinessInteractionV3  commonBase = new TpCommonBusinessInteractionV3();
    	TpCommonBusinessInteractionV3.Assignment v1= new TpCommonBusinessInteractionV3.Assignment();
        TpCommonBusinessInteractionV3.BusinessEvent v2  = new TpCommonBusinessInteractionV3.BusinessEvent();
        TpCommonBusinessInteractionV3.BusinessInteraction v3  = new TpCommonBusinessInteractionV3.BusinessInteraction();
        TpCommonBusinessInteractionV3.BusinessInteractionEntity v4= new TpCommonBusinessInteractionV3.BusinessInteractionEntity();
        TpCommonBusinessInteractionV3.BusinessInteractionItem v5 = new TpCommonBusinessInteractionV3.BusinessInteractionItem();
        TpCommonBusinessInteractionV3.BusinessTimePeriod v6 = new TpCommonBusinessInteractionV3.BusinessTimePeriod();
        TpCommonBusinessInteractionV3.Request v7 = new TpCommonBusinessInteractionV3.Request();
        TpCommonBusinessInteractionV3.StatusNotification v8 = new TpCommonBusinessInteractionV3.StatusNotification();
        TpCommonBusinessInteractionV3.StatusNotificationItem v9= new TpCommonBusinessInteractionV3.StatusNotificationItem();
        
    }
    
     static testMethod void testmethodforTpCommonMessage() {
    	TpCommonMessage  commonBase = new TpCommonMessage();
    	TpCommonMessage.Exception_x v1= new TpCommonMessage.Exception_x();
        TpCommonMessage.ExceptionMessage v2  = new TpCommonMessage.ExceptionMessage();
        TpCommonMessage.IbmTelecomMessage v3  = new TpCommonMessage.IbmTelecomMessage();
        TpCommonMessage.LookupRequestMessage v4= new TpCommonMessage.LookupRequestMessage();
        TpCommonMessage.TelecomException_x v5 = new TpCommonMessage.TelecomException_x();
    }
    
    static testMethod void testmethodforTpCommonMtosiV3() {
    	TpCommonMtosiV3  commonBase = new TpCommonMtosiV3();
    	TpCommonMtosiV3.ActivityStatus_T v1= new TpCommonMtosiV3.ActivityStatus_T();
        TpCommonMtosiV3.ActivityStatusEnum_T v2  = new TpCommonMtosiV3.ActivityStatusEnum_T();
        TpCommonMtosiV3.ContextEntry v3  = new TpCommonMtosiV3.ContextEntry();
        TpCommonMtosiV3.ExecutionContext v4= new TpCommonMtosiV3.ExecutionContext();
        TpCommonMtosiV3.ExecutionContextType v5 = new TpCommonMtosiV3.ExecutionContextType();
        TpCommonMtosiV3.Header_T v6= new TpCommonMtosiV3.Header_T();
        TpCommonMtosiV3.PropertyType v7 = new TpCommonMtosiV3.PropertyType();
        TpCommonMtosiV3.PropertyType_property_element v8 = new TpCommonMtosiV3.PropertyType_property_element();
    }
    
     static testMethod void testmethodforTpCommonPartyExtV3() {
    	TpCommonPartyExtV3  commonBase = new TpCommonPartyExtV3();
    	TpCommonPartyExtV3.ContactMediumExtensions v1= new TpCommonPartyExtV3.ContactMediumExtensions();
        TpCommonPartyExtV3.ContactPersonExtensions v2  = new TpCommonPartyExtV3.ContactPersonExtensions();
        TpCommonPartyExtV3.PartyRoleExtensions v3  = new TpCommonPartyExtV3.PartyRoleExtensions();
         TpCommonPartyExtV3.PartyExtensions v4=new TpCommonPartyExtV3.PartyExtensions();
    }
    
     static testMethod void testmethodforTpCommonPartyV3() {
    	TpCommonPartyV3  commonBase = new TpCommonPartyV3();
    	TpCommonPartyV3.ContactMedium v1= new TpCommonPartyV3.ContactMedium();
        TpCommonPartyV3.ContactPerson v2  = new TpCommonPartyV3.ContactPerson();
        TpCommonPartyV3.Party v3  = new TpCommonPartyV3.Party();
        TpCommonPartyV3.PartyRole v4  = new TpCommonPartyV3.PartyRole();
    }
    
    static testMethod void testmethodforTpCommonPlaceExtV3() {
    	TpCommonPlaceExtV3  commonBase = new TpCommonPlaceExtV3();
    	TpCommonPlaceExtV3.GeographicAddressExtensions v1= new TpCommonPlaceExtV3.GeographicAddressExtensions();
        TpCommonPlaceExtV3.UrbanPropertyAddressExtensions v2  = new TpCommonPlaceExtV3.UrbanPropertyAddressExtensions();
        
    }
    
     static testMethod void testmethodforTpCommonPlaceV3() {
    	TpCommonPlaceV3  commonBase = new TpCommonPlaceV3();
    	TpCommonPlaceV3.AbstractGeographicAddress v1= new TpCommonPlaceV3.AbstractGeographicAddress();
        TpCommonPlaceV3.GeographicAddress v2  = new TpCommonPlaceV3.GeographicAddress();
        TpCommonPlaceV3.GeographicPlace v3  = new TpCommonPlaceV3.GeographicPlace();
        TpCommonPlaceV3.Place v4= new TpCommonPlaceV3.Place();
        TpCommonPlaceV3.Site v5 = new TpCommonPlaceV3.Site();
    }
    
    static testMethod void testmethodforTpCommonUrbanPropertyAddressV3() {
    	TpCommonUrbanPropertyAddressV3  commonBase = new TpCommonUrbanPropertyAddressV3();
    	TpCommonUrbanPropertyAddressV3.UrbanPropertyAddress v1= new TpCommonUrbanPropertyAddressV3.UrbanPropertyAddress();
    }
    
    static testMethod void testmethodforTpCustomerOrderMessage() {
    	TpCustomerOrderMessage  commonBase = new TpCustomerOrderMessage();
    	TpCustomerOrderMessage.CustomerOrderMessage v1= new TpCustomerOrderMessage.CustomerOrderMessage();
    }
    
    static testMethod void testmethodforTpFulfillmentCustomerOrderExtV3() {
    	TpFulfillmentCustomerOrderExtV3  commonBase = new TpFulfillmentCustomerOrderExtV3();
    	TpFulfillmentCustomerOrderExtV3.AgreementItemOrderItemExtensions v1= new TpFulfillmentCustomerOrderExtV3.AgreementItemOrderItemExtensions();
        TpFulfillmentCustomerOrderExtV3.AgreementOrderItemExtensions v2  = new TpFulfillmentCustomerOrderExtV3.AgreementOrderItemExtensions();
        TpFulfillmentCustomerOrderExtV3.BankAccountOrderItemExtensions v3  = new TpFulfillmentCustomerOrderExtV3.BankAccountOrderItemExtensions();
        TpFulfillmentCustomerOrderExtV3.CustomerAccountOrderItemExtensions v4= new TpFulfillmentCustomerOrderExtV3.CustomerAccountOrderItemExtensions();
        TpFulfillmentCustomerOrderExtV3.CustomerOrderExtensions v5 = new TpFulfillmentCustomerOrderExtV3.CustomerOrderExtensions();
    }
    
    static testMethod void testmethodforTpFulfillmentCustomerOrderV3() {
    	TpFulfillmentCustomerOrderV3  commonBase = new TpFulfillmentCustomerOrderV3();
    	TpFulfillmentCustomerOrderV3.AgreementItemOrderItem v1= new TpFulfillmentCustomerOrderV3.AgreementItemOrderItem();
        TpFulfillmentCustomerOrderV3.AgreementOrderItem v2  = new TpFulfillmentCustomerOrderV3.AgreementOrderItem();
        TpFulfillmentCustomerOrderV3.BankAccountOrderItem v3  = new TpFulfillmentCustomerOrderV3.BankAccountOrderItem();
        TpFulfillmentCustomerOrderV3.CustomerAccountOrderItem v4= new TpFulfillmentCustomerOrderV3.CustomerAccountOrderItem();
        TpFulfillmentCustomerOrderV3.CustomerOrder v5 = new TpFulfillmentCustomerOrderV3.CustomerOrder();
    }
    
    static testMethod void testmethodforTpFulfillmentProductOrderExtV3() {
    	TpFulfillmentProductOrderExtV3  commonBase = new TpFulfillmentProductOrderExtV3();
    	TpFulfillmentProductOrderExtV3.ProductOrderExtensions v1= new TpFulfillmentProductOrderExtV3.ProductOrderExtensions();
        TpFulfillmentProductOrderExtV3.ProductOrderItemExtensions v2  = new TpFulfillmentProductOrderExtV3.ProductOrderItemExtensions();
        TpFulfillmentProductOrderExtV3.SiteOrderItemExtensions v3  = new TpFulfillmentProductOrderExtV3.SiteOrderItemExtensions();
    }
    
    static testMethod void testmethodforTpFulfillmentProductOrderV3() {
    	TpFulfillmentProductOrderV3  commonBase = new TpFulfillmentProductOrderV3();
    	TpFulfillmentProductOrderV3.ProductOrderItem v1= new TpFulfillmentProductOrderV3.ProductOrderItem();
        TpFulfillmentProductOrderV3.SiteOrderItem v2  = new TpFulfillmentProductOrderV3.SiteOrderItem();
    }
    
    static testMethod void testmethodforTpInventoryCustomerConfigExtV3() {
    	TpInventoryCustomerConfigExtV3  commonBase = new TpInventoryCustomerConfigExtV3();
    	TpInventoryCustomerConfigExtV3.AgreementExtensions v1= new TpInventoryCustomerConfigExtV3.AgreementExtensions();
        TpInventoryCustomerConfigExtV3.AgreementItemExtensions v2  = new TpInventoryCustomerConfigExtV3.AgreementItemExtensions();
        TpInventoryCustomerConfigExtV3.CustomerAccountExtensions v3  = new TpInventoryCustomerConfigExtV3.CustomerAccountExtensions();
        TpInventoryCustomerConfigExtV3.CustomerExtensions v4 = new TpInventoryCustomerConfigExtV3.CustomerExtensions();
 
    }
    
     static testMethod void testmethodforTpInventoryCustomerConfigV3() {
    	TpInventoryCustomerConfigV3  commonBase = new TpInventoryCustomerConfigV3();
    	TpInventoryCustomerConfigV3.Agreement v1= new TpInventoryCustomerConfigV3.Agreement();
        TpInventoryCustomerConfigV3.AgreementItem v2  = new TpInventoryCustomerConfigV3.AgreementItem();
        TpInventoryCustomerConfigV3.CustomerAccount v3  = new TpInventoryCustomerConfigV3.CustomerAccount();
        TpInventoryCustomerConfigV3.CustomerConfiguration v4 = new TpInventoryCustomerConfigV3.CustomerConfiguration();
 
    }
    
    static testMethod void testmethodforTpInventoryCustomerV3() {
    	TpInventoryCustomerV3  commonBase = new TpInventoryCustomerV3();
    	TpInventoryCustomerV3.BankAccount v1= new TpInventoryCustomerV3.BankAccount();
        TpInventoryCustomerV3.BaseAgreement v2  = new TpInventoryCustomerV3.BaseAgreement();
        TpInventoryCustomerV3.BaseAgreementItem v3  = new TpInventoryCustomerV3.BaseAgreementItem();
        TpInventoryCustomerV3.BaseCustomerAccount v4= new TpInventoryCustomerV3.BaseCustomerAccount();
        TpInventoryCustomerV3.Customer v5 = new TpInventoryCustomerV3.Customer();
    }
    
    static testMethod void testmethodforTpInventoryProductConfigExtV3() {
    	TpInventoryProductConfigExtV3  commonBase = new TpInventoryProductConfigExtV3();
    	TpInventoryProductConfigExtV3.ProductExtensions v1= new TpInventoryProductConfigExtV3.ProductExtensions();
     }
     
    static testMethod void testmethodforTpInventoryProductConfigV3() {
    	TpInventoryProductConfigV3  commonBase = new TpInventoryProductConfigV3();
    	TpInventoryProductConfigV3.OrderedProduct v1= new TpInventoryProductConfigV3.OrderedProduct();
        TpInventoryProductConfigV3.Product v2  = new TpInventoryProductConfigV3.Product();
      }
      
     
    static testMethod void testmethodforTpInventoryResourceConfigExtV3() {
    	TpInventoryResourceConfigExtV3  commonBase = new TpInventoryResourceConfigExtV3();
    	TpInventoryResourceConfigExtV3.LogicalResourceExtensions v1= new TpInventoryResourceConfigExtV3.LogicalResourceExtensions();
        TpInventoryResourceConfigExtV3.PhysicalResourceExtensions v2  = new TpInventoryResourceConfigExtV3.PhysicalResourceExtensions();
        TpInventoryResourceConfigExtV3.ResourceExtensions v3  = new TpInventoryResourceConfigExtV3.ResourceExtensions();
        TpInventoryResourceConfigExtV3.SupplierPartnerProductExtensions v4 = new TpInventoryResourceConfigExtV3.SupplierPartnerProductExtensions();
 
    } 

	static testMethod void testmethodforTpTemplatesContactMediumExtV3() {
    	TpTemplatesContactMediumExtV3  commonBase = new TpTemplatesContactMediumExtV3();
    	TpTemplatesContactMediumExtV3.ContactMediumChoice v1= new TpTemplatesContactMediumExtV3.ContactMediumChoice();
        TpTemplatesContactMediumExtV3.USBillingAddressContactMedium v2  = new TpTemplatesContactMediumExtV3.USBillingAddressContactMedium();
    } 

	static testMethod void testmethodforTpTemplatesPartyExtV3() {
    	TpTemplatesPartyExtV3  commonBase = new TpTemplatesPartyExtV3();
    	TpTemplatesPartyExtV3.CorporationParty v1= new TpTemplatesPartyExtV3.CorporationParty();
        TpTemplatesPartyExtV3.DepartmentParty v2  = new TpTemplatesPartyExtV3.DepartmentParty();
        TpTemplatesPartyExtV3.EnterpriseParty v3  = new TpTemplatesPartyExtV3.EnterpriseParty();
        TpTemplatesPartyExtV3.GovernmentAgencyParty v4 = new TpTemplatesPartyExtV3.GovernmentAgencyParty();
		TpTemplatesPartyExtV3.HouseholdParty v5  = new TpTemplatesPartyExtV3.HouseholdParty();
        TpTemplatesPartyExtV3.PersonParty v6 = new TpTemplatesPartyExtV3.PersonParty();
        TpTemplatesPartyExtV3.PartyChoice v7 = new TpTemplatesPartyExtV3.PartyChoice();
        
 
    } 

	static testMethod void testmethodforTpTemplatesPartyRoleExtV3() {
    	TpTemplatesPartyRoleExtV3  commonBase = new TpTemplatesPartyRoleExtV3();
    	TpTemplatesPartyRoleExtV3.AdministrativeContactPartyRole v1= new TpTemplatesPartyRoleExtV3.AdministrativeContactPartyRole();
        TpTemplatesPartyRoleExtV3.ArchitectPartyRole v2  = new TpTemplatesPartyRoleExtV3.ArchitectPartyRole();
        TpTemplatesPartyRoleExtV3.BusinessAnalystPartyRole v3  = new TpTemplatesPartyRoleExtV3.BusinessAnalystPartyRole()
			;
        TpTemplatesPartyRoleExtV3.BusinessModelerPartyRole v4= new TpTemplatesPartyRoleExtV3.BusinessModelerPartyRole();
        TpTemplatesPartyRoleExtV3.DomainNameRegistrarPartyRole v5 = new TpTemplatesPartyRoleExtV3.DomainNameRegistrarPartyRole();
        TpTemplatesPartyRoleExtV3.EmployeePartyRole v6 = new TpTemplatesPartyRoleExtV3.EmployeePartyRole();
        TpTemplatesPartyRoleExtV3.EndUserPartyRole v7 = new TpTemplatesPartyRoleExtV3.EndUserPartyRole();
        TpTemplatesPartyRoleExtV3.FamilyMemberPartyRole v8 = new TpTemplatesPartyRoleExtV3.FamilyMemberPartyRole();
        TpTemplatesPartyRoleExtV3.HouseholdMemberPartyRole v9= new TpTemplatesPartyRoleExtV3.HouseholdMemberPartyRole();

        TpTemplatesPartyRoleExtV3.MainContactPartyRole v11= new TpTemplatesPartyRoleExtV3.MainContactPartyRole();
        TpTemplatesPartyRoleExtV3.NextOfKinPartyRole v12  = new TpTemplatesPartyRoleExtV3.NextOfKinPartyRole();
        TpTemplatesPartyRoleExtV3.NumberPortabilityAdministratorPartyRole v13  = new TpTemplatesPartyRoleExtV3.NumberPortabilityAdministratorPartyRole()
			;
        TpTemplatesPartyRoleExtV3.PartyRoleChoice v14= new TpTemplatesPartyRoleExtV3.PartyRoleChoice();
        TpTemplatesPartyRoleExtV3.QualityAssuranceRepresentativePartyRole v15 = new TpTemplatesPartyRoleExtV3.QualityAssuranceRepresentativePartyRole();
        TpTemplatesPartyRoleExtV3.SubscriberPartyRole v16 = new TpTemplatesPartyRoleExtV3.SubscriberPartyRole();
        TpTemplatesPartyRoleExtV3.TechnicalContactPartyRole v17 = new TpTemplatesPartyRoleExtV3.TechnicalContactPartyRole();
        TpTemplatesPartyRoleExtV3.UserConceptualModelerPartyRole v18 = new TpTemplatesPartyRoleExtV3.UserConceptualModelerPartyRole();
    }

	static testMethod void testmethodforTpTemplatesPhoneV3() {
    	TpTemplatesPhoneV3  commonBase = new TpTemplatesPhoneV3();
    	TpTemplatesPhoneV3.Phone v1= new TpTemplatesPhoneV3.Phone();
	}

	static testMethod void testmethodforTpTemplatesSetTopBoxV3() {
    	TpTemplatesSetTopBoxV3  commonBase = new TpTemplatesSetTopBoxV3();
    	TpTemplatesSetTopBoxV3.SetTopBox v1= new TpTemplatesSetTopBoxV3.SetTopBox();
	}

}