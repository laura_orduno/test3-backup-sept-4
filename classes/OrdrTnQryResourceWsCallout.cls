/**
 * @author Santosh Rath
 * 
 **/
public class OrdrTnQryResourceWsCallout {
public static OrdrTnResQryResConfMsg.ResourceConfigurationMessage queryResource(TpCommonMtosiV3.Header_T header, List<TpCommonBaseV3.EntityWithSpecification> sourceCriteria, List<TpCommonBaseV3.EntityWithSpecification> targetCriteria, TpInventoryResourceConfigV3.LogicalResource logResource, TpInventoryResourceConfigV3.PhysicalResource phyResource,Boolean isTollFree) {
OrdrTnResQryResConfMsg.ResourceConfigurationMessage request_x ;
       OrdrTnResQryResConfMsg.ResourceConfigurationMessage respConfigurationMessage;
        try {
              System.debug('The name of the TNR WebService called is queryservice');
                 OrdrTnResQryInvOperation.QueryResourceSOAPBinding binding = new OrdrTnResQryInvOperation.QueryResourceSOAPBinding();
                binding = prepareCallout(binding,isTollFree); 
                if(!OrdrUtilities.useAuditFlag){
                    respConfigurationMessage = binding.queryResource(header,sourceCriteria,targetCriteria);
                }
        } catch (CalloutException ce) {
            System.debug(ce.getStackTraceString());
            System.debug('CalloutException occurred while invoking binding.queryResource' +ce);
            System.debug(ce.getmessage());
            String msg=ce.getmessage();
            if(String.isNotBlank(msg) && (msg.indexOf(OrdrConstants.SDF_INTERNAL_SERVER_ERR)!=-1)){
                msg=msg+OrdrConstants.SDF_SUPPORT_MSG;
            }
            OrdrUtilities.auditLogs('OrdrTnQryResourceWsCallout.queryResource','TOCP','queryResource',null,null,true,false,msg,ce.getStackTraceString());
            OrdrServicesException ose=new OrdrServicesException(ce.getmessage(),ce);
            throw ose;           
        } catch (Exception e) {
            System.debug(e.getStackTraceString());
            System.debug('Exception occurred while invoking binding.queryResource' +e);
            String msg=e.getmessage();
            if(String.isNotBlank(msg) && (msg.indexOf(OrdrConstants.SDF_INTERNAL_SERVER_ERR)!=-1)){
                msg=msg+OrdrConstants.SDF_SUPPORT_MSG;
            }
            OrdrUtilities.auditLogs('OrdrTnQryResourceWsCallout.queryResource','TOCP','queryResource',null,null,false,true,msg,e.getStackTraceString());
            System.debug(e.getmessage());
            OrdrServicesException ose=new OrdrServicesException(e.getmessage(),e);
            throw ose;
        }
        return respConfigurationMessage;
        
    }
 @TestVisible  private static OrdrTnResQryInvOperation.QueryResourceSOAPBinding prepareCallout(OrdrTnResQryInvOperation.QueryResourceSOAPBinding binding,Boolean isTollFree) {
        Ordering_WS__c queryResourceWirelineEndpoint = Ordering_WS__c.getValues('QueryResourceWirelineEndpoint');  //ReserveResourceTollfreeEndpoint
        if(isTollFree){
			queryResourceWirelineEndpoint=Ordering_WS__c.getValues('QueryResourceTollfreeEndpoint'); 
		}
        binding.endpoint_x = queryResourceWirelineEndpoint.value__c;
        System.debug('QueryResourceWirelineEndpoint: '+ queryResourceWirelineEndpoint.value__c);
      
       // servicePort.clientCertName_x = OrdrConstants.SDF_CERT_NAME;          
        // Set SFDC Webservice call timeout
        binding.timeout_x = OrdrConstants.WS_TIMEOUT_MS;
        //binding.timeout_x = 30000;
        if (binding.endpoint_x.startsWith(OrdrConstants.ENDPOINT_XML_GATEWAY_SYMBOL)) {
                Ordering_WS__c wsUserName = Ordering_WS__c.getValues(OrdrConstants.CREDENTIAL_USERNAME);
                Ordering_WS__c wsPassword =  Ordering_WS__c.getValues(OrdrConstants.CREDENTIAL_PASSWORD);  
                if (String.isNotBlank(wsUserName.value__c) && String.isNotBlank(wsPassword.value__c)) {
                        String credentials = 'APP_CPQ' + ':' + wsPAssword.value__c;
                        String encodedUserNameAndPassword = EncodingUtil.base64Encode(Blob.valueOf(credentials));
                        binding.inputHttpHeaders_x = new Map<String, String>();
                        binding.inputHttpHeaders_x.put(OrdrConstants.AUTHORIZATION, OrdrConstants.BASIC + encodedUserNameAndPassword);
                    }   
                }
          else {
                binding.clientCertName_x = OrdrConstants.SDF_CERT_NAME;
            }
        return binding;
    } 
    
        
    
    public static OrdrTnNpaNxx queryResourceForCoid(OrdrTnServiceAddress serviceAddress, String userId, String npa, String alternateNxx, OrdrTnReservationType reservationType) {
       
        
        System.debug('-- QueryResourceImpl.queryResourceForCoid - processing starts at '+System.now() );

        //Holder<HeaderT> header = new Holder<HeaderT>();
        TpCommonMtosiV3.Header_T header = null;
        List<TpCommonBaseV3.EntityWithSpecification> sourceCriteria = null;
        List<TpCommonBaseV3.EntityWithSpecification> targetCriteria = null;
        
        TpInventoryResourceConfigV3.LogicalResource logResource=new TpInventoryResourceConfigV3.LogicalResource();
        
        TpInventoryResourceConfigV3.PhysicalResource phyResource=new  TpInventoryResourceConfigV3.PhysicalResource();
        OrdrTnNpaNxx resultingOrdrTnNpaNxx= null;
        String servingCoid = null;
        String switchNumber = null;
        boolean isServingCoidNativeToServiceAddress = false;
        OrdrTnResQryResConfMsg.ResourceConfigurationMessage resourceConfigMsg=null;

        if (reservationType != null) {
            if (String.valueOf(reservationType).equals(String.valueOf(OrdrTnReservationType.WLN))) {
                //Preparing the WS call to TOCP to retrieve from FMS through ASF the COID and Switch Number associated with a given alternate OrdrTnNpaNxx
                sourceCriteria = generateTocpSourceCriteriaForWirelineDarkDslAndRealTnSoapRequest(serviceAddress, userId, npa, alternateNxx);
            }
            else if (String.valueOf(reservationType).equals(String.valueOf(OrdrTnReservationType.TOLLFREE))) {
                //This functionality is not supported/required by SMS - therefore, no outbound WS call needed
            }

            if (sourceCriteria != null) {
                //String tocpReqStr = SoaUtil.ObjectToXml(sourceCriteria);
                String tocpReqStr = null;
                System.debug( tocpReqStr);

                // Get the binding to the WebService
                //binding = getQueryResourceBinding(reservationType);

                // Invoke the TOCP - QueryResource web service
                try {
                    System.debug( 'Invoking ASF InventoryService.getCOIDByOrdrTnNpaNxxthrough TOCP QueryResource');
                      resourceConfigMsg=queryResource(header, sourceCriteria, targetCriteria, logResource, phyResource,false);
                } 
                catch (Exception e) {
                    System.debug(e.getStackTraceString());
                    throw e;
                }

                // Process the TOCP response

                if ((resourceConfigMsg != null)) {
                logResource=resourceConfigMsg.logicalResource.get(0);
                }
                if ((logResource != null) ) {
                    Map <String, String> hm = convertTocpV2CharacteristicValuesToMap (logResource.characteristicValue);
                    //Map <String, String> hm = null;
                    if (hm.containsKey('coid')) {
                        servingCoid = hm.get('coid');
                    }
                    List<TpInventoryResourceConfigV3.PhysicalResource> listRp = logResource.PhysicalResource;
                    if ((listRp != null) && (listRp.size() > 0)) {
                        for (Integer i=0; i<listRp.size(); i++) {
                            if (listRp.get(i) != null) {
                                TpInventoryResourceConfigV3.PhysicalResource res = listRp.get(i);
                                if ((res != null) && (res.CharacteristicValue != null)) {
                                    Map <String, String> hm2 = convertTocpV2CharacteristicValuesToMap (res.characteristicValue);
                                    if (hm2.containsKey('switchNumber')) {
                                        switchNumber = hm2.get('switchNumber');
                                    }
                                }
                            }
                        }
                    }
                }
                if (String.isNotBlank(servingCoid)) {
                    if (servingCoid.trim().equalsIgnoreCase(serviceAddress.getCOID())) {
                        isServingCoidNativeToServiceAddress = true;
                    }
                    resultingOrdrTnNpaNxx= new OrdrTnNpaNxx(npa, alternateNxx, servingCoid, switchNumber, false, isServingCoidNativeToServiceAddress);
                }
            }
        }
        return resultingOrdrTnNpaNxx;
    }
  
    
  
  
    public static Boolean queryResourceForTollFreeStatus(OrdrTnServiceAddress serviceAddress, String userId, String tn, OrdrTnReservationType reservationType) {
        System.debug('-- QueryResourceImpl.queryResourceForTollFreeStatus - processing starts at ' +System.now());

        TpCommonMtosiV3.Header_T header = new TpCommonMtosiV3.Header_T();
        List<TpCommonBaseV3.EntityWithSpecification> sourceCriteria = null;
        List<TpCommonBaseV3.EntityWithSpecification> targetCriteria = null;
        
        TpInventoryResourceConfigV3.LogicalResource logResource = new TpInventoryResourceConfigV3.LogicalResource();
        
        TpInventoryResourceConfigV3.PhysicalResource phyResource = new TpInventoryResourceConfigV3.PhysicalResource();

        String tollFreeNumber = null;
        String effectiveDate = null;
        String respOrg = null;
        String lastActiveDate = null;
        String telephoneNumberStatus = null;
        String notes = null;
        boolean isReservedStatus = false;
        boolean isReservedByTelusAndSfdcCpq = false;
		OrdrTnResQryResConfMsg.ResourceConfigurationMessage resourceConfigMsg=null;

        if (reservationType != null) {
            if (String.valueOf(reservationType).equals(String.valueOf(OrdrTnReservationType.WLN))) {
                //This functionality is not supported/required by FMS - therefore, no outbound WS call needed
            }
            else if (String.valueOf(reservationType).equals(String.valueOf(OrdrTnReservationType.TOLLFREE))) {
                //Preparing the WS call to TOCP to retrieve from SMS the status associated with a given Toll Free telephone number
                sourceCriteria = generateTocpSourceCriteriaForTollFreeSoapRequest(serviceAddress, userId, tn);
                targetCriteria = generateTocpTargetCriteriaForTollFreeSoapRequest(serviceAddress, userId, tn);
            }

            if (sourceCriteria != null) {
                // Invoke the TOCP - QueryResource web service
                try {
                    System.debug( 'Invoking SMS queryResourceForTollFreeStatus through TOCP QueryResource');
                    resourceConfigMsg=queryResource(header, sourceCriteria, targetCriteria, logResource, phyResource,true);
                }
                catch (Exception e) {
                    System.debug(e.getStackTraceString());
                    throw e;
                }
               // Process the TOCP response

                if ((resourceConfigMsg != null)) {
					logResource=resourceConfigMsg.logicalResource.get(0);
                }
                // Process the TOCP response
                if ((logResource != null) && (logResource.CharacteristicValue != null)) {
                    Map <String, String> hm = convertTocpV2CharacteristicValuesToMap (logResource.CharacteristicValue);
                    if (hm.containsKey('tollFreeNumber')) {
                        //The reserved Toll Free telephone number
                        tollFreeNumber = hm.get('tollFreeNumber');
                    }
                    if (hm.containsKey('effectiveDate')) {
                        //For searchOption = telephoneNumberStatus, the date in which the number was given the current status. Not applicable if status is SPARE
                        effectiveDate = hm.get('effectiveDate');
                    }
                    if (hm.containsKey('respOrg')) {
                        //For searchOption = telephoneNumberStatus, the RESP ORG of the controlling organization. Returned only if field is available
                        respOrg = hm.get('respOrg');
                    }
                    if (hm.containsKey('lastActiveDate')) {
                        //Will be returned if number is spare or has a status of reserved and the RESP ORG is the control RESP ORG of the telephone number
                        lastActiveDate = hm.get('lastActiveDate');
                    }
                    if (hm.containsKey('telephoneNumberStatus')) {
                        //SPARE, UNAVAIL, RESERVE, ASSIGNE, SUSPEND, WORKING, DISCONN, TRANSIT
                        telephoneNumberStatus = hm.get('telephoneNumberStatus');
                        if (String.isNotBlank(telephoneNumberStatus)) {
                            if (telephoneNumberStatus.trim().equalsIgnoreCase('RESERVE')) {
                                isReservedStatus = true;
                            }
                            else {
                                System.debug('QueryResourceImpl.queryResourceForTollFreeStatus - status of '+tn+' is ' +telephoneNumberStatus);         
                            }
                        }
                    }
                    if (hm.containsKey('notes')) {
                        //Note that 'notes' is being set by Salesforce in the ReserveResource request to the value of 'SFDCTN' - to differentiate between SFDC/Vlocity versus Rebiller systems
                        notes = hm.get('notes');
                        if (String.isNotBlank(notes) && (notes.trim().equalsIgnoreCase('SFDCTN'))) {
                            isReservedByTelusAndSfdcCpq = true;
                        }
                    }
                }
                if (String.isNotBlank(tollFreeNumber) &&  (tollFreeNumber.trim().equalsIgnoreCase(checkNullInput(tn)))) {
                    if (!isReservedStatus) {
                        isReservedByTelusAndSfdcCpq = false;
                    }
                }
                else {
                    isReservedByTelusAndSfdcCpq = false;
                }
            }
        }
        return isReservedByTelusAndSfdcCpq;
        
    }
@TestVisible   private static List<TpCommonBaseV3.EntityWithSpecification> generateTocpSourceCriteriaForWirelineDarkDslAndRealTnSoapRequest(OrdrTnServiceAddress serviceAddress, String userId, String npa, String alternateNxx) {
        List<TpCommonBaseV3.EntityWithSpecification> lews = new List<TpCommonBaseV3.EntityWithSpecification>();
        TpCommonBaseV3.EntityWithSpecification sourceCriteria = new TpCommonBaseV3.EntityWithSpecification();
        sourceCriteria.Specification=generateTocpV2EntitySpecificationForSoapRequest('Wireline', 'Telephone Number', 'Logical Resource');
        List<TpCommonBaseV3.CharacteristicValue> charValList=new List<TpCommonBaseV3.CharacteristicValue>();
        charValList.add(generateTocpV2CharacteristicValueForSoapRequest('userId', 'SFDCTN', true));
        charValList.add(generateTocpV2CharacteristicValueForSoapRequest('npa', checkNullInput(npa), false));
        charValList.add(generateTocpV2CharacteristicValueForSoapRequest('nxx', checkNullInput(alternateNxx), false));
        
        sourceCriteria.CharacteristicValue=charValList;
        lews.add(sourceCriteria);

        for(TpCommonBaseV3.EntityWithSpecification a : lews){
            List<TpCommonBaseV3.CharacteristicValue>  bList=a.CharacteristicValue;
            for(TpCommonBaseV3.CharacteristicValue b:bList){
                TpCommonBaseV3.Characteristic c=b.Characteristic;
                 System.debug('characterstics name is '+c.name);
                 List<String> dList=b.value;
                 for(String d:dList){
                    System.debug('charactersticsvale  is '+d);
                 }
            }
        }
        return lews;
    }
  
 @TestVisible  private static List<TpCommonBaseV3.EntityWithSpecification> generateTocpSourceCriteriaForTollFreeSoapRequest(OrdrTnServiceAddress serviceAddress, String userId, String tn) {
        //Order action reference number - digit alphanumeric correlation id used by SMS - must begin with a capital 'O'
        String id = String.valueOf(System.currentTimeMillis()).substring(4,13);
        // Total length of ID passed to SMS should be exactly 10 : 9 digits number prefixed by 'O'
        List<TpCommonBaseV3.EntityWithSpecification> lews = new List<TpCommonBaseV3.EntityWithSpecification>();
        TpCommonBaseV3.EntityWithSpecification sourceCriteria = new TpCommonBaseV3.EntityWithSpecification();
        sourceCriteria.Specification=generateTocpV2EntitySpecificationForSoapRequest('', 'Toll Free Telephone Number', 'Logical Resource');
        sourceCriteria.Id=('O'+id);
       

        List<TpCommonBaseV3.CharacteristicValue> charValList=new List<TpCommonBaseV3.CharacteristicValue>();
        charValList.add(generateTocpV2CharacteristicValueForSoapRequest('respOrg', checkNullInput('webService.queryResource.request.tollfree.respOrg'), true));
        charValList.add(generateTocpV2CharacteristicValueForSoapRequest('tollFreeNumber', checkNullInput(tn), false));
        
        sourceCriteria.CharacteristicValue=charValList;
        lews.add(sourceCriteria);
        return lews;
    }
 
  @TestVisible  private static List<TpCommonBaseV3.EntityWithSpecification> generateTocpTargetCriteriaForTollFreeSoapRequest(OrdrTnServiceAddress serviceAddress, String userId, String tn) {
        List<TpCommonBaseV3.EntityWithSpecification> lews = new List<TpCommonBaseV3.EntityWithSpecification>();
        TpCommonBaseV3.EntityWithSpecification targetCriteria = new TpCommonBaseV3.EntityWithSpecification();
      List<TpCommonBaseV3.CharacteristicValue> charValList=new List<TpCommonBaseV3.CharacteristicValue>();
      	charValList.add(generateTocpV2CharacteristicValueForSoapRequest('searchOption', 'telephoneNumber', true));
        
       targetCriteria.CharacteristicValue=charValList;
        lews.add(targetCriteria);
        return lews;
    }
  
  @TestVisible  private static String checkNullInput(String sInput) {
        String sOutput = '';
        sOutput = (String.isBlank(sInput) ? sOutput : sInput);  
        System.debug('sOutput is '+sOutput);
        return sOutput;
    }


    


    public static Map <String, String> convertTocpV2CharacteristicValuesToMap (List<TpCommonBaseV3.CharacteristicValue> listCharVal) {
        Map <String, String> hm = new Map <String, String>();
        for(Integer z=0;(listCharVal!=null && z<listCharVal.size());z++) {
            TpCommonBaseV3.CharacteristicValue cv = listCharVal.get(z);
            if ((cv != null) && (cv.Characteristic != null)) {
                TpCommonBaseV3.Characteristic c = cv.Characteristic;
                String cName  = c.Name;
                if (String.isNotBlank(cName)) {
                    String cValue = null;
                    if ((cv.Value != null) && (cv.Value.size()>0)) {
                        cValue = cv.Value.get(0);
                        if (!hm.containsKey(cName)) {
                            hm.put(cName, cValue);
                        }
                    }
                }
            }
        }
        return hm;
    }


    public static TpCommonBaseV3.EntitySpecification generateTocpV2EntitySpecificationForSoapRequest(
            String specificationName, String specificationType, String specificationCategory) {
        // Setting service specifications
        TpCommonBaseV3.EntitySpecification entitySpec = new TpCommonBaseV3.EntitySpecification();
        entitySpec.Name=checkNullInput(specificationName);
        entitySpec.Type_x=checkNullInput(specificationType);
        entitySpec.Category=checkNullInput(specificationCategory);
        return entitySpec;
    }

    

    public static TpCommonBaseV3.CharacteristicValue generateTocpV2CharacteristicValueForSoapRequest(
            String nameOfCharacteristic, String valueOfCharacteristic, boolean firstElement) {

        TpCommonBaseV3.CharacteristicValue charValueOne = null;
        if (String.isNotBlank(nameOfCharacteristic)) {
            charValueOne = new TpCommonBaseV3.CharacteristicValue();
            TpCommonBaseV3.Characteristic charOne = new TpCommonBaseV3.Characteristic();
            charOne.Name=nameOfCharacteristic;           
            charValueOne.Characteristic=charOne;
            
            List<String> charValList=new List<String>();
                charValList.add(checkNullInput(valueOfCharacteristic));     
            charValueOne.Value=charValList;
            
        }
        return charValueOne;
    }

 }