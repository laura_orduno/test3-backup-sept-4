/**
 *  MBROverviewCtlrTest.cls
 *
 *  @description
 *		Test Class for the My Business Requests Overview Controller. Copied By Dan, Developed By Many.
 *
 *	@date - 03/01/2014
 *	@author - Dan Reich: Traction on Demand
 *  @author - Christian Wico: cwico@tractionondemand.com
 *  @author - Alex Kong: akong@tractionondemand.com
 */
@isTest
private class MBROverviewCtlrTest {
	
	static Case mbrTestCase {get;set;}
	static Case vitilCareTestCase {get;set;}

	static {
		insert new Customer_Interface_Settings__c( 
			Manage_Requests_List_Limit__c = 10,
			Overview_Case_Limit__c = 5,
			Case_Origin_Value__c = 'My Business Requests'
		);
        
        /* VITILcare portal */
		insert new VITILcare_Customer_Interface_Settings__c( 
			Manage_Requests_List_Limit__c = 10,
			Overview_Case_Limit__c = 5,
			Case_Origin_Value__c = 'VITILcare'
		);

        List<Case> cases = new List<Case>();
		Case mbrCase = new Case(Subject = 'Test Subject', Origin = 'My Business Requests', My_Business_Requests_Type__c = 'Dummy', RecordTypeId = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId);
		cases.add(mbrCase);
		Case vitilCareCase = new Case(Subject = 'Test Subject', Origin = 'VITILcare', My_Business_Requests_Type__c = 'Dummy', RecordTypeId = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId);
		cases.add(vitilCareCase);
        
        insert cases;
        
		mbrTestCase = [SELECT Id, Subject, RecordTypeId, Contact.FirstName, Contact.LastName, My_Business_Requests_Type__c, CaseNumber, Status, LastModifiedDate FROM Case WHERE Id = :mbrCase.Id LIMIT 1];
		vitilCareTestCase = [SELECT Id, Subject, RecordTypeId, Contact.FirstName, Contact.LastName, My_Business_Requests_Type__c, CaseNumber, Status, LastModifiedDate FROM Case WHERE Id = :vitilCareCase.Id LIMIT 1];

		MBRTestUtils.createExternalToInternalCustomSettings();
		MBRTestUtils.createParentToChildCustomSettings();
	}

	@isTest 
	static void testOverviewPage() {
		MBROverviewCtlr overviewContr = new MBROverviewCtlr();
		overviewContr.initCheck();

		Test.startTest();

		//test search functionality
		Case c = [SELECT caseNumber from Case Where Id = :mbrTestCase.Id LIMIT 1];
		//overviewContr.q = 'Test';
		overviewContr.searchCase();
		System.assertEquals(1, overviewContr.totalCases);

        Id [] fixedSearchResults = new Id[]{c.Id};
        Test.setFixedSearchResults(fixedSearchResults);

		overviewContr.q = 'Test';
		List<Case> cases = overviewContr.executeSearch(1,1,true);
		System.assertEquals(1, cases.size());

		Test.stopTest();
		/*
		PageReference pageRef = overviewContr.searchCase();
		System.assertEquals(pageRef, null);

		overviewContr.q=c.caseNumber;
		pageRef = overviewContr.searchCase();
		System.assertNotEquals(null,pageRef);
		System.assertEquals(pageRef.getUrl(), '/apex/mbrcasedetail?caseNumber='+overviewContr.q);

		//Verify the pageRef parameters as well
	    Map<String,String> pageParameters = pageRef.getParameters();
	    System.assertEquals(1,pageParameters.values().size());
	    System.assertEquals(overviewContr.q, pageParameters.get('caseNumber'));
	    */
	}

	@isTest static void testGuestUser() {
		MBROverviewCtlr overviewContr = new MBROverviewCtlr();
	}

	@isTest static void testGetCaseDetails() {
		MBROverviewCtlr ctlr = new MBROverviewCtlr();
		ctlr.caseNumber = mbrTestCase.CaseNumber;
		System.assert(ctlr.getCaseDetails().getUrl().containsIgnoreCase('detail'));
	}

	@isTest static void testGetPageOfCases() {
		MBROverviewCtlr ctlr = new MBROverviewCtlr();
		System.assertEquals(ctlr.getPageOfCases(0,100).size(), 1);

		// test with filter
		ctlr.categoryFilter = 'Test';
		//ctlr.statusFilter = 'request4';
		System.assertEquals(ctlr.getPageOfCases(0,100).size(), 0);
	}

	@isTest static void testCreateNewCase() {
		MBROverviewCtlr ctlr = new MBROverviewCtlr();
		System.assert(ctlr.createNewCase().getUrl().containsIgnoreCase('Case'));
	}

	@isTest static void testOtherMethods() {
		MBROverviewCtlr ctlr = new MBROverviewCtlr();

		// paging
		ctlr.totalPages = 20;

		
		System.assert(ctlr.pageNumbers.size() > 0);
		System.assert(ctlr.allListings.size() > 0);
		System.assert(ctlr.refreshGrid() == null);
		System.assert(ctlr.Previous() == null);
		System.assert(ctlr.Next() == null);
		System.assert(ctlr.getDisablePrevious() == true);
		System.assert(ctlr.getDisableNext() == false);

		ctlr.totalPages = 1;
		ctlr.selectedPage = 4;
		System.assert(ctlr.pageNumbers.size() > 0);

		// filters
		System.assert(ctlr.getCategoryFilterOptions().size() > 0);
		System.assert(ctlr.getStatusFilterOptions().size() > 0);

		// internal class test
		
		MBROverviewCtlr.CaseListing cl = new MBROverviewCtlr.CaseListing(mbrTestCase);
		System.assert(cl.getTimeDiff(DateTime.now().addMinutes(-60)).containsIgnoreCase('1 hour ago'));
		System.assert(cl.getTimeDiff(DateTime.now().addHours(-20)).containsIgnoreCase('hours ago'));
		System.assert(cl.getTimeDiff(DateTime.now().addDays(-20)).containsIgnoreCase('days ago'));
		System.assert(cl.getTimeDiff(DateTime.now().addDays(-31)).containsIgnoreCase('over 1 month'));
		System.debug(cl.getTimeDiff(DateTime.now().addYears(2)));
		//System.assert(cl.getTimeDiff(DateTime.now().addYears(2)).containsIgnoreCase('months ago'));
	}


	/* VITILcare portal */
    
	@isTest 
	static void VITILcare_testOverviewPage() {
		VITILcareOverviewCtlr overviewContr = new VITILcareOverviewCtlr();
		overviewContr.initCheck();

		Test.startTest();

		//test search functionality
		Case c = [SELECT caseNumber from Case Where Id = :vitilCareTestCase.Id LIMIT 1];
		//overviewContr.q = 'Test';
		overviewContr.searchCase();
		System.assertEquals(1, overviewContr.totalCases);

        Id [] fixedSearchResults = new Id[]{c.Id};
        Test.setFixedSearchResults(fixedSearchResults);

		overviewContr.q = 'Test';
		List<Case> cases = overviewContr.executeSearch(1,1,true);
		System.assertEquals(1, cases.size());

		Test.stopTest();
		/*
		PageReference pageRef = overviewContr.searchCase();
		System.assertEquals(pageRef, null);

		overviewContr.q=c.caseNumber;
		pageRef = overviewContr.searchCase();
		System.assertNotEquals(null,pageRef);
		System.assertEquals(pageRef.getUrl(), '/apex/mbrcasedetail?caseNumber='+overviewContr.q);

		//Verify the pageRef parameters as well
	    Map<String,String> pageParameters = pageRef.getParameters();
	    System.assertEquals(1,pageParameters.values().size());
	    System.assertEquals(overviewContr.q, pageParameters.get('caseNumber'));
	    */
	}

	@isTest static void VITILcare_testGuestUser() {
		VITILcareOverviewCtlr overviewContr = new VITILcareOverviewCtlr();
	}

	@isTest static void VITILcare_testGetCaseDetails() {
		VITILcareOverviewCtlr ctlr = new VITILcareOverviewCtlr();
		ctlr.caseNumber = vitilCareTestCase.CaseNumber;
		System.assert(ctlr.getCaseDetails().getUrl().containsIgnoreCase('detail'));
	}

	@isTest static void VITILcare_testGetPageOfCases() {
		VITILcareOverviewCtlr ctlr = new VITILcareOverviewCtlr();
		System.assertEquals(ctlr.getPageOfCases(0,100).size(), 1);

		// test with filter
		ctlr.categoryFilter = 'Test';
		//ctlr.statusFilter = 'request4';
		System.assertEquals(ctlr.getPageOfCases(0,100).size(), 0);
	}

	@isTest static void VITILcare_testCreateNewCase() {
		VITILcareOverviewCtlr ctlr = new VITILcareOverviewCtlr();
		System.assert(ctlr.createNewCase().getUrl().containsIgnoreCase('Case'));
	}

	@isTest static void VITILcare_testOtherMethods() {
		VITILcareOverviewCtlr ctlr = new VITILcareOverviewCtlr();

		// paging
		ctlr.totalPages = 20;

		
		System.assert(ctlr.pageNumbers.size() > 0);
		System.assert(ctlr.allListings.size() > 0);
		System.assert(ctlr.refreshGrid() == null);
		System.assert(ctlr.Previous() == null);
		System.assert(ctlr.Next() == null);
		System.assert(ctlr.getDisablePrevious() == true);
		System.assert(ctlr.getDisableNext() == false);

		ctlr.totalPages = 1;
		ctlr.selectedPage = 4;
		System.assert(ctlr.pageNumbers.size() > 0);

		// filters
		System.assert(ctlr.getCategoryFilterOptions().size() > 0);
		System.assert(ctlr.getStatusFilterOptions().size() > 0);

		// internal class test
		
		MBROverviewCtlr.CaseListing cl = new MBROverviewCtlr.CaseListing(vitilCareTestCase);
		System.assert(cl.getTimeDiff(DateTime.now().addMinutes(-60)).containsIgnoreCase('1 hour ago'));
		System.assert(cl.getTimeDiff(DateTime.now().addHours(-20)).containsIgnoreCase('hours ago'));
		System.assert(cl.getTimeDiff(DateTime.now().addDays(-20)).containsIgnoreCase('days ago'));
		System.assert(cl.getTimeDiff(DateTime.now().addDays(-31)).containsIgnoreCase('over 1 month'));
		System.debug(cl.getTimeDiff(DateTime.now().addYears(2)));
		//System.assert(cl.getTimeDiff(DateTime.now().addYears(2)).containsIgnoreCase('months ago'));
	}
    


}