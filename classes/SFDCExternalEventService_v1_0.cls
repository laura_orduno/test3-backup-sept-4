/*****************************************************
    Name: ExternalEventService_WS_v1
    Usage: This class used to expose web service to external systems to create single or multiple events in salesforce per call
    Author – Sandip Chaudhari (IBM)
    Revision History -
    Created By/Date - Sandip Chaudhari / 04 July 2016
    
    Updated By/Date/Reason for Change - 
    
******************************************************/
global class SFDCExternalEventService_v1_0{
  //create single events in salesforce per call 
    webservice static ExternalEventRequestResponseParameter.ResponseEventData createEvent(ExternalEventRequestResponseParameter.RequestEventData eventData){
       List<ExternalEventRequestResponseParameter.RequestEventData> requestList = new List<ExternalEventRequestResponseParameter.RequestEventData>();
       requestList.add(eventData);
       List<ExternalEventRequestResponseParameter.ResponseEventData> responseData =  ExternalEventService_Helper.processEvent(requestList);
       return responseData[0];
       /*if(responseData != null && responseData.size() > 0){
           return responseData[0];
       }else{
           return null;
       }*/
    }
    //create multiple events in salesforce per call
    webservice static List<ExternalEventRequestResponseParameter.ResponseEventData> createEvents(List<ExternalEventRequestResponseParameter.RequestEventData> eventsData){
       List<ExternalEventRequestResponseParameter.ResponseEventData> responseData =  ExternalEventService_Helper.processEvent(eventsData);
       return responseData;
    }
}