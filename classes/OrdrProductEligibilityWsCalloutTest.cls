@isTest
private class OrdrProductEligibilityWsCalloutTest {
	@isTest
    private static void testPerformCustomerOrderFeasibility_StandardAddress() {
		Map<String, Object> inputMap = new Map<String, Object>(); 
        inputMap.put(OrdrConstants.FMS_ID, '010293');
        inputMap.put(OrdrConstants.LOCATION_ID, '38475');
        inputMap.put(OrdrConstants.ADDRESS_TYPE, OrdrConstants.ADDRESS_TYPE_STANDARD);
        inputMap.put(OrdrConstants.ADDR_PROVINCE_CODE, 'BC');
        inputMap.put(OrdrConstants.ADDR_MUNICIPALITY, 'Burnaby');
        inputMap.put(OrdrConstants.ADDR_STREET_TYPE, 'ST');
        inputMap.put(OrdrConstants.ADDR_STREET_NAME, 'Kingsway');
        inputMap.put(OrdrConstants.ADDR_STREET_NUMBER, '3777');
        inputMap.put(OrdrConstants.ADDR_UNIT_NUMBER, '10T2');
        inputMap.put(OrdrConstants.ADDR_POSTAL_CODE, 'V5R 3Z7');
        inputMap.put(OrdrConstants.ADDR_STREET_DIRECTION, 'E');
        inputMap.put(OrdrConstants.IS_ILEC, 'Y');
        
        OrdrTestDataFactory.createOrderingWSCustomSettings('ProductEligibilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/CMO/OrderMgmt/PerformCustomerOrderFeasibility_v2_0_vs0');
        
        Test.startTest();
      	Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        // Call the method that invokes a callout
        TpFulfillmentCustomerOrderV3.CustomerOrder response = OrdrProductEligibilityWsCallout.performCustomerOrderFeasibility(inputMap);
        // Verify that a fake result is returned
        System.assertEquals(OrdrConstants.SPECIFICATION_TYPE_AVAILABILITY, response.Specification.Type_x);
        Test.stopTest();
        
    }
    
	@isTest
    private static void testPerformCustomerOrderFeasibility_LotBlockPlanAddress() {
		Map<String, Object> inputMap = new Map<String, String>(); 
        inputMap.put(OrdrConstants.FMS_ID, '010293');
        inputMap.put(OrdrConstants.LOCATION_ID, '38475');
        inputMap.put(OrdrConstants.ADDRESS_TYPE, OrdrConstants.ADDRESS_TYPE_LOT_BLOCK_PLAN);
        inputMap.put(OrdrConstants.ADDR_PROVINCE_CODE, 'AB');
        inputMap.put(OrdrConstants.ADDR_LOT, '&12A');
        inputMap.put(OrdrConstants.ADDR_BLOCK, '2234');
        inputMap.put(OrdrConstants.ADDR_PLAN, '1113456');
        inputMap.put(OrdrConstants.ADDR_MUNICIPALITY, 'Calgary');
        inputMap.put(OrdrConstants.IS_ILEC, 'Y');

        OrdrTestDataFactory.createOrderingWSCustomSettings('ProductEligibilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/CMO/OrderMgmt/PerformCustomerOrderFeasibility_v2_0_vs0');
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        // Call the method that invokes a callout
        TpFulfillmentCustomerOrderV3.CustomerOrder response = OrdrProductEligibilityWsCallout.performCustomerOrderFeasibility(inputMap);
        // Verify that a fake result is returned
        System.assertEquals(OrdrConstants.SPECIFICATION_TYPE_AVAILABILITY, response.Specification.Type_x);
        Test.stopTest();
        
    }    

	@isTest
    private static void testPerformCustomerOrderFeasibility_LegalLandDescriptionAddress() {
		Map<String, Object> inputMap = new Map<String, String>(); 
        inputMap.put(OrdrConstants.FMS_ID, '010293');
        inputMap.put(OrdrConstants.LOCATION_ID, '38475');
        inputMap.put(OrdrConstants.ADDRESS_TYPE, OrdrConstants.ADDRESS_TYPE_LEGAL_LAND_DESC);
        inputMap.put(OrdrConstants.ADDR_PROVINCE_CODE, 'AB');
        inputMap.put(OrdrConstants.ADDR_QUARTER, 'SW');
        inputMap.put(OrdrConstants.ADDR_SECTION, '24');
        inputMap.put(OrdrConstants.ADDR_TOWNSHIP, '12');
        inputMap.put(OrdrConstants.ADDR_RANGE, '20');
        inputMap.put(OrdrConstants.ADDR_MERIDIAN, 'W4');
        inputMap.put(OrdrConstants.IS_ILEC, 'Y');
        
        OrdrTestDataFactory.createOrderingWSCustomSettings('ProductEligibilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/CMO/OrderMgmt/PerformCustomerOrderFeasibility_v2_0_vs0');
        
        Test.startTest();     
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        // Call the method that invokes a callout
        TpFulfillmentCustomerOrderV3.CustomerOrder response = OrdrProductEligibilityWsCallout.performCustomerOrderFeasibility(inputMap);
        // Verify that a fake result is returned
        System.assertEquals(OrdrConstants.SPECIFICATION_TYPE_AVAILABILITY, response.Specification.Type_x);
        Test.stopTest();
        
    }

	@isTest
    private static void testPerformCustomerOrderFeasibility_TownshipRangeRoadHigwayAddress() {
		Map<String, Object> inputMap = new Map<String, String>(); 
        inputMap.put(OrdrConstants.FMS_ID, '010293');
        inputMap.put(OrdrConstants.LOCATION_ID, '38475');
        inputMap.put(OrdrConstants.ADDRESS_TYPE, OrdrConstants.ADDRESS_TYPE_TOWN_RANGE_ROAD_HWY);
        inputMap.put(OrdrConstants.ADDR_PROVINCE_CODE, 'BC');
        inputMap.put(OrdrConstants.ADDR_STREET_TYPE, 'HWY');
        inputMap.put(OrdrConstants.ADDR_STREET_NAME, 'Highway 16');
        inputMap.put(OrdrConstants.ADDR_UNIT_NUMBER, '14387');
        inputMap.put(OrdrConstants.IS_ILEC, 'Y');
        
        OrdrTestDataFactory.createOrderingWSCustomSettings('ProductEligibilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/CMO/OrderMgmt/PerformCustomerOrderFeasibility_v2_0_vs0');
        
        Test.startTest();        
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        // Call the method that invokes a callout
        TpFulfillmentCustomerOrderV3.CustomerOrder response = OrdrProductEligibilityWsCallout.performCustomerOrderFeasibility(inputMap);
        // Verify that a fake result is returned
        System.assertEquals(OrdrConstants.SPECIFICATION_TYPE_AVAILABILITY, response.Specification.Type_x);
        Test.stopTest();
        
    }
    
	@isTest
    private static void testPerformCustomerOrderFeasibility_MissingMandatoryFields() {
		Map<String, Object> inputMap = new Map<String, String>(); 
        inputMap.put(OrdrConstants.ADDRESS_TYPE, OrdrConstants.ADDRESS_TYPE_STANDARD);
        
        OrdrTestDataFactory.createOrderingWSCustomSettings('ProductEligibilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/CMO/OrderMgmt/PerformCustomerOrderFeasibility_v2_0_vs0');
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        try {
            // Call the method that invokes a callout
            TpFulfillmentCustomerOrderV3.CustomerOrder response = OrdrProductEligibilityWsCallout.performCustomerOrderFeasibility(inputMap);
            //System.assert(false, 'Exception expected');
        }
        catch(Exception e) {
            System.debug(e.getTypeName() + ': ' + e.getMessage());
        }
        Test.stopTest();
        
    } 
    
	@isTest
    private static void testPerformCustomerOrderFeasibility_Exception() {
		Map<String, Object> inputMap = new Map<String, Object>(); 
        inputMap.put(OrdrConstants.FMS_ID, '010293');
        inputMap.put(OrdrConstants.LOCATION_ID, '38475');
        inputMap.put(OrdrConstants.ADDRESS_TYPE, OrdrConstants.ADDRESS_TYPE_STANDARD);
        inputMap.put(OrdrConstants.ADDR_PROVINCE_CODE, 'BC');
        inputMap.put(OrdrConstants.ADDR_MUNICIPALITY, 'Burnaby');
        inputMap.put(OrdrConstants.ADDR_STREET_TYPE, 'ST');
        inputMap.put(OrdrConstants.ADDR_STREET_NAME, 'Kingsway');
        inputMap.put(OrdrConstants.ADDR_STREET_NUMBER, '3777');
        inputMap.put(OrdrConstants.ADDR_UNIT_NUMBER, '10T2');
        inputMap.put(OrdrConstants.ADDR_POSTAL_CODE, 'V5R 3Z7');
        inputMap.put(OrdrConstants.ADDR_STREET_DIRECTION, 'E');
        inputMap.put(OrdrConstants.IS_ILEC, 'Y');
        
        OrdrTestDataFactory.createOrderingWSCustomSettings('ProductEligibilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/CMO/OrderMgmt/PerformCustomerOrderFeasibility_v2_0_vs0');
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl(false));
        try {
            // Call the method that invokes a callout
            TpFulfillmentCustomerOrderV3.CustomerOrder response = OrdrProductEligibilityWsCallout.performCustomerOrderFeasibility(inputMap);
            System.assert(false, 'Exception expected');
        }
        catch(Exception e) {
            System.debug(e.getTypeName() + ': ' + e.getMessage());
        }
        Test.stopTest();
        
    }        
    
}