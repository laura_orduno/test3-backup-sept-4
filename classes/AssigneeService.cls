public class AssigneeService {
    /**
     * Method sets the allocation count for all the new assigness that are part of the rules
     * based on the maximum allocation count of the other assignees in those rules
     * and it will only do when the "Allow_Load_Balancing_Across_Rules__c" is set true.
     * @param ruleIds 
     */
    public static void assignAllocationCount(Map<Id, List<ADRA__Assignee__c>> ruleIdToAssignees) {
        System.debug('Rule IDs----->:::'+ ruleIdToAssignees);
        // Retrieving all the allocation rules and all the assigness that are part of these allocation rules based on the rules ids that are passed in.
        List<ADRA__AllocationRule__c> allocationRules = [SELECT Id, ADRA__Allow_Load_Balancing_Across_Rules__c,
                                                          (select id, ADRA__Allocate_Count__c from ADRA__assignees__r) 
                                                   FROM ADRA__AllocationRule__c 
                                                   WHERE Id IN :ruleIdToAssignees.keySet()];
        if (allocationRules != null && allocationRules.size() > 0) {
            List<ADRA__Assignee__c> modifiedAssignees = new List<ADRA__Assignee__c>();
            for (ADRA__AllocationRule__c allocationRule : allocationRules) {
                if(allocationRule.ADRA__Allow_Load_Balancing_Across_Rules__c){
                    Decimal maxCount = 0;
                    for(ADRA__Assignee__c assignee : allocationRule.ADRA__Assignees__r) {
                        if(assignee.ADRA__Allocate_Count__c > maxCount)
                            maxCount = assignee.ADRA__Allocate_Count__c;
                    }
                    for(ADRA__Assignee__c assignee : ruleIdToAssignees.get(allocationRule.Id)) {
                        assignee.ADRA__Allocate_Count__c = (maxCount <=0 )? 0 :(maxCount - 1);
                        System.debug('-------'+assignee);
                    }
                }
            }
        }
    }
}