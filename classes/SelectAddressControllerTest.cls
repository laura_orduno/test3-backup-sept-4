@isTest(SeeAllData=true)
private class SelectAddressControllerTest {
	testMethod static void unit() {
		Account acc = TestUtil.createAccount();
		Opportunity opp = TestUtil.createOpportunity(acc.Id);
		Opportunity_Solution__c solution = TestUtil.createOpportunitySolution(opp.Id);
		List<Opp_Product_Item__c> items = TestUtil.createPLIList(opp.Id);

		Address__c a1 = new Address__c(City__c = 'SFO', Account__c = acc.id);
		insert a1;
        PageReference pageRef = Page.SelectAddress;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('oppId', opp.id);

		SelectAddressController c = new SelectAddressController();
		//no resultt
		Address__c searchAddress = new Address__c(City__c = 'LAX');
		c.address = searchAddress;
		c.searchAddress();
		
		
		searchAddress = new Address__c(City__c = 'SFO');
		c.address = searchAddress;
		c.searchAddress();
		
		//returns null
		c.selectAddress();
		
		c.selectedAddress = '0';
		c.selectAddress();
		c.copyAddress();
		c.searchDuplicateAddress();
		c.enterNewAddress();
		c.addAddress();
		c.cancel();

		//with pli
		ApexPages.currentPage().getParameters().put('pliId', items.get(0).id);
		c = new SelectAddressController();
		searchAddress = new Address__c(City__c = 'SFO');
		c.address = searchAddress;
		c.searchAddress();
		c.selectedAddress = '0';
		c.selectAddress();
		c.copyAddress();
		c.addAddress();

		//std methods
		c.cancel();
	}
}