/**
 * Test suite for <code>SObjectListOptionModel</code> class.
 * 
 * @author Max Rudman
 * @since 9/10/2009
 */
@isTest
private class SObjectListOptionModelTests {
	testMethod static void test() {
		Account a1 = new Account(Name='Test1');
		Account a2 = new Account();
		List<Account> accounts = new List<Account>{a1,a2};
		
		SObjectListOptionModel target = new SObjectListOptionModel(accounts);
		System.assertEquals(2, target.getOptions().size());
		System.assertequals('', target.getOptions().get(0).getValue());
		System.assertequals(a1.Name, target.getOptions().get(0).getLabel());
		System.assertequals('', target.getOptions().get(1).getValue());
		System.assertequals('', target.getOptions().get(1).getLabel());
	}
}