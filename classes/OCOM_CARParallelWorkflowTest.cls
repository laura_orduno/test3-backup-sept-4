@isTest(SeeAllData=false)
private class OCOM_CARParallelWorkflowTest{
    private static void createData(){
        System.runAs(new User(Id = Userinfo.getUserId())) {
            OrdrTestDataFactory.createOrderingWSCustomSettings('ReserveResourceTollfreeEndpoint', 'https://webservice1.preprd.teluslabs.net/RMO/ResourceMgmt/ReserveResource_v2_0_vs1_DV');
            OrdrTestDataFactory.createOrderingWSCustomSettings('ReserveResourceWirelineEndpoint', 'https://webservice1.preprd.teluslabs.net/RMO/ResourceMgmt/ReserveResource_v2_0_vs1_PT');
            OrdrTestDataFactory.createOrderingWSCustomSettings('QueryResourceWirelineEndpoint', 'https://webservice1.preprd.teluslabs.net/RMO/InventoryMgmt/QueryResource_v2_0_vs1_DV');
            OrdrTestDataFactory.createOrderingWSCustomSettings('QueryResourceTollfreeEndpoint', 'https://webservice1.preprd.teluslabs.net/RMO/InventoryMgmt/QueryResource_v2_0_vs1_DV');
            OrdrTestDataFactory.createOrderingWSCustomSettings('FindResourceWirelineEndpoint', 'https://webservice1.preprd.teluslabs.net/RMO/InventoryMgmt/FindResource_v2_0_vs1_PT');
            OrdrTestDataFactory.createOrderingWSCustomSettings('FindResourceTollfreeRespOrg', 'https://webservice1.preprd.teluslabs.net/RMO/InventoryMgmt/FindResource_v2_0_vs1_PT');
            OrdrTestDataFactory.createOrderingWSCustomSettings('RecoverResourceWirelineEndpoint', 'https://webservice1.preprd.teluslabs.net/RMO/ResourceMgmt/RecoverResource_v2_0_vs1_PT');
            OrdrTestDataFactory.createOrderingWSCustomSettings('RecoverResourceTollfreeEndpoint', 'https://webservice1.preprd.teluslabs.net/RMO/ResourceMgmt/RecoverResource_v2_0_vs1_DV');
            OrdrTestDataFactory.createOrderingWSCustomSettings('AmendCustomerOrderEndpoint', 'https://webservice1.preprd.teluslabs.net/CMO/OrderMgmt/AmendCustomerOrder_v2_0_vs0');
            OrdrTestDataFactory.createOrderingWSCustomSettings('SubmitCustomerOrderEndpoint', 'https://webservice1.preprd.teluslabs.net/CMO/OrderMgmt/SubmitCustomerOrder_v2_1_vs1');
         
        }
    }
    @isTest
    private static void CARParallelWorkflowTest1() {
        createData();
        String orderId= OrdrTestDataFactory.singleMethodForOrderId();
        Order ordObj=[select Account.Credit_Profile__c,AccountId, Credit_Check_Not_Required__c,RequestedDate__C,status,Rush__c,Ban__c,Shipping_Address__c FROM Order where id=:orderId];
        ordObj.status='Processing';
        ordObj.Rush__c=true;
        ordObj.Ban__c='12345';
        ordObj.Shipping_Address__c='test address';
        ordObj.Credit_Check_Not_Required__c=false;
        Date day1 = Date.today();
        Date todayPlus = day1.addDays(4);
        ordObj.RequestedDate__C=todayPlus;
        update ordObj;
        
        Account anAccount = [select Account_Status__c from Account where id=:ordObj.AccountId limit 1];
        anAccount.Account_Status__c = 'ACTIVE';
        update anAccount;
        
        Credit_Assessment__c aCAR = new Credit_Assessment__c(CAR_Status__c='Queued');
        insert aCAR;
        
        Credit_Profile__c creditProfile=new Credit_Profile__c(name='CP-RCID', credit_reference_TL__c='123456', account__c=ordObj.accountid, RecordTypeId = Schema.SObjectType.Credit_Profile__c.getRecordTypeInfosByName().get('Business').getRecordTypeId(), Legal_Entity_Type__c='Incorporated (INC)');
        insert creditProfile;
        
        OrderItem anOrderItem=[select vlocity_cmt__RecurringTotal__c from OrderItem where OrderId=:orderId limit 1];
        anOrderItem.vlocity_cmt__RecurringTotal__c=400.00;
        update anOrderItem;
       
        ordObj.Account.Credit_Profile__c =creditProfile.id;
        ordObj.Credit_Assessment__c=aCAR.id;
        update ordObj;
        try{
            List<OCOM_CAR_Status__c> lstStatus = new List<OCOM_CAR_Status__c>();
            OCOM_CAR_Status__c carstatus = new OCOM_CAR_Status__c(Category__c='From', Name='Queued');
            OCOM_CAR_Status__c carstatus1 = new OCOM_CAR_Status__c(Category__c='To', Name='Completed');
            OCOM_CAR_Status__c carstatus2 = new OCOM_CAR_Status__c(Category__c='Result', Name='Approved');
            lstStatus.add(carstatus);
            lstStatus.add(carstatus1);
            lstStatus.add(carstatus2);
            Database.Insert(lstStatus,false);
            OCOM_CAR_Order_Status__c obj1 = new OCOM_CAR_Order_Status__c(Name='Processing', Category__c='Order Status');
            insert obj1;
        }catch(Exception ex){}
        aCAR.CAR_Status__c='Completed';
        aCAR.CAR_Result__c='Approved';
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        try{
            update aCar;
        }catch(Exception ex){}
        Test.stopTest();
    }
}