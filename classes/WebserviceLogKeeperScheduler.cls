global class WebserviceLogKeeperScheduler implements schedulable{
    global string query{get;set;}
    global string cronExp{get;set;}
    global string name{get;set;}
    global WebserviceLogKeeperScheduler(){
        name='Webservice Log Keeper';
        cronExp='0 0 0 ? * SUN';
        query='select id from webservice_integration_error_log__c where createddate<last_90_days';
    }
    global void execute(schedulablecontext sc){
        WebserviceLogKeeper keeper=new WebserviceLogKeeper(query);
        database.executebatch(keeper);
    }
}