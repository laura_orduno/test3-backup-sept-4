/*****************************************************
    Name: DeleteMeFromSalesProdTeam
    Usage: This class use to Delete Opprotunity Sales Team Member 
    Author – Clear Task
    Date – 04/04/2011
    Revision History
******************************************************/
public without sharing class DeleteMeFromSalesProdTeam{
  // used for opprotunity Id after click on 'Delete Me From Sales/Prod Team' Button in Opprotunity 
    private String oppId = ApexPages.currentPage().getParameters().get('oppId');
  //show the message 
    public boolean flag{get; set;} 
    //show the account team block 
    public boolean optFlag{get; set;} 
  //show the message 
    public String strMsg{get; set;}  
   //show the message 
    public Set<Id> roleIds{get; set;}    
   //account team wrapper
    public List<OpportunityTeamWrapper> opportunityTeamWrapperList{get;set;}
    public Map<Id, User> userObjMap ;
 /*
  * init method for to Delete Opprotunity Sales Team Member 
  */
    public void init(){
        userObjMap = new Map<Id, User>(); 
        flag = false;
        optFlag = false; 
        if(oppId != null && oppId != ''){
            Boolean flagManager = true;
            Id userId = UserInfo.getUserId();
            Id userRoleId = UserInfo.getUserRoleId();
            System.debug('userId =' +userId + 'userRoleId =' + userRoleId );
            List<OpportunityTeamMember> salesTeamList = new List<OpportunityTeamMember>();            
            salesTeamList = [Select o.TeamMemberRole, o.User.UserRoleId, o.UserId, o.OpportunityId, o.Opportunity.OwnerId, o.Id From OpportunityTeamMember o where o.OpportunityId=:oppId and o.UserId = :userId ];   
            List<UserRole> userRoleList = [ Select u.ParentRoleId, u.Name, u.Id From UserRole u where u.Id = :userRoleId ];
            if(salesTeamList != null && salesTeamList.size() == 0 && userRoleList != null && userRoleList.size()>0 && (userRoleList[0].Name).contains('Manager')){
                salesTeamList = [Select o.TeamMemberRole, o.User.UserRoleId, o.UserId, o.OpportunityId, o.Opportunity.OwnerId, o.Id From OpportunityTeamMember o where o.OpportunityId=:oppId ];       
                flagManager = false;
            }
            roleIds = new Set<Id>(); 
            if(salesTeamList != null && salesTeamList.size()>0){
                if( userRoleList != null && userRoleList.size()>0 && (userRoleList[0].Name).contains('Manager')){
                    //userRoleId = salesTeamList[0].User.UserRoleId ;                  
                    roleIds.add(userRoleId);
                    Map<Id, Set<UserRole>> roleHierarchyChildMap = new Map<Id, Set<UserRole>>();
                    List<UserRole> roleList = [ Select u.ParentRoleId, u.Name, u.Id From UserRole u];
                    
                    if(roleList != null && roleList.size()>0){
                        
                        for(UserRole ur :roleList){
                            if(ur.ParentRoleId != null){
                                if(roleHierarchyChildMap != null && roleHierarchyChildMap.containsKey(ur.ParentRoleId)){
                                    Set<UserRole> tempUserRoleList = roleHierarchyChildMap.get(ur.ParentRoleId);
                                    tempUserRoleList.add(ur);   
                                }else{
                                    Set<UserRole> tempUserRoleList = new Set<UserRole>();
                                    tempUserRoleList.add(ur);
                                    roleHierarchyChildMap.put(ur.ParentRoleId, tempUserRoleList);   
                                }   
                            }
                        }
                                
                        System.debug('roleHierarchyChildMap=' + roleHierarchyChildMap.get(userRoleId));
                        findChildHierarchyByMap(userRoleId, roleHierarchyChildMap);
                        System.debug('roleIds--Size=' + roleIds.size());
                        
                        if(roleIds != null && roleIds.size()>0){
                            
                            Map<Id, User> usersObjMap = new Map<Id, User>([ Select u.Id, u.Name, u.Username, u.UserRoleId,  u.system_admin_manager__c, 
                                            u.Temp_Manager_4__c, u.Temp_Manager_3__c, u.Temp_Manager_2__c, u.Temp_Manager_1__c, u.Sales_Manager__c, 
                                            u.Manager_4__c, u.Manager_3__c, u.Manager_2__c, u.Manager_1__c, u.ManagerId  From User u where u.UserRoleId in :roleIds ]);     
                            
                            /* find user in role hierarchy -start*/
                            Map<String, User> currentUserMap = new Map<String, User>();
                            User currentUser = usersObjMap.get(userId);
                            currentUserMap.put(currentUser.Name, currentUser);                            
                            userObjMap = findUsersInRoleHierarchy(usersObjMap, currentUserMap);
                            if(userObjMap != null)
                                userObjMap.put(currentUser.id, currentUser); 
                            /* find user in role hierarchy -end*/  
                        }                       
                    }                       
              }
           // if(salesTeamList != null && salesTeamList.size()>0){
                if(userRoleList != null && userRoleList.size()>0 && (userRoleList[0].Name).contains('Manager') ){
                    if(userObjMap != null && userObjMap.size()>0){                                
                                List<OpportunityTeamMember> allSalesTeamList = [Select o.TeamMemberRole, o.User.UserRoleId, o.UserId, o.OpportunityId, o.Opportunity.OwnerId, o.Id From OpportunityTeamMember o where o.OpportunityId=:oppId ];         
                                if(allSalesTeamList != null && allSalesTeamList.size()>0){
                                    flag = false;
                                    optFlag = true;
                                    strMsg = '';
                                    opportunityTeamWrapperList = new  List<OpportunityTeamWrapper>();
                                    if(flagManager){
                                        OpportunityTeamWrapper optMngrObj = new OpportunityTeamWrapper(salesTeamList[0]);                                    
                                        opportunityTeamWrapperList.add(optMngrObj);
                                    }
                                    for(OpportunityTeamMember at :allSalesTeamList){
                                        if(at.UserId != userId && userObjMap.containsKey(at.UserId)){
                                            OpportunityTeamWrapper atwObj = new OpportunityTeamWrapper(at); 
                                            opportunityTeamWrapperList.add(atwObj);                                         
                                        }   
                                    }
                                    System.debug('accountTeamWrapperList=' + opportunityTeamWrapperList);
                                    if(opportunityTeamWrapperList != null && opportunityTeamWrapperList.size()== 1 && flagManager == true){
                                         try{
                                            delete salesTeamList;                    
                                            flag = true;
                                            optFlag = false;
                                         }catch(DMLException e){
                                            flag = false;    
                                         }     
                                    }else if(opportunityTeamWrapperList != null && opportunityTeamWrapperList.size()== 0 && flagManager == false){
                                        flag = false;
                                        strMsg = '';
                                        optFlag = false;    
                                    }
                                }
                     }  
                }else{//in case of User is not Manager
                    try{
                        delete salesTeamList;                    
                        flag = true;
                        optFlag = false;
                    }catch(DMLException e){
                        flag = false;    
                    } 
                }               
            //}
            //opprotunity Item - start
            List<Opp_Product_Item__c> oppProdItemList = [Select o.Opportunity__c, o.Name, o.Id, (Select Id, Name, Member__c, Member__r.UserRoleId From Product_Sales_Team__r ) From Opp_Product_Item__c o where o.Opportunity__c = :oppId ];
            if(flag == true){                  
                if(oppProdItemList != null && oppProdItemList.size()>0){
                    List<Product_Sales_Team__c> prodSalesTeamList = new List<Product_Sales_Team__c>();
                    for(Opp_Product_Item__c opi :oppProdItemList){
                        if(opi.Product_Sales_Team__r != null && (opi.Product_Sales_Team__r).size()>0){
                             prodSalesTeamList.addAll(opi.Product_Sales_Team__r);                        
                        }
                    }
                    if(prodSalesTeamList != null && prodSalesTeamList.size()>0){
                        List<Product_Sales_Team__c> deleteProdSalesTeamList = new List<Product_Sales_Team__c>();
                        for(Product_Sales_Team__c pst :prodSalesTeamList){
                            if((pst.Member__c != null && userId != null) && (pst.Member__c == userId)){
                                 deleteProdSalesTeamList.add(pst);                        
                            }
                        }
                        if(deleteProdSalesTeamList != null && deleteProdSalesTeamList.size()>0){                        
                                try{
                                    delete deleteProdSalesTeamList;                    
                                    flag = true;
                                    optFlag = false;
                                }catch(DMLException e){
                                    flag = false;    
                                }       
                        }    
                    }
                }         
             }else if(salesTeamList != null && salesTeamList.size()>0 && oppProdItemList != null && oppProdItemList.size()>0){
                flag = false; 
             }
            
           } else{
               flag = false;
               strMsg = '';
           }
        }
    }
    public void deleteOpportunityTeamMember(){
        if(opportunityTeamWrapperList != null && opportunityTeamWrapperList.size()>0){
            Map<Id, OpportunityTeamMember> deleteForOppItemMap = new Map<Id, OpportunityTeamMember>();
            List<OpportunityTeamMember> deleteoptTeamList = new List<OpportunityTeamMember>();
            for(OpportunityTeamWrapper atwObj :opportunityTeamWrapperList){
                if(atwObj.checked != null && atwObj.checked == true){
                    deleteoptTeamList.add(atwObj.oppTeam); 
                    deleteForOppItemMap.put(atwObj.oppTeam.UserId, atwObj.oppTeam); 
                }   
            }
            System.debug('deleteActTeamList=' + deleteoptTeamList);
            if(deleteoptTeamList != null && deleteoptTeamList.size()>0){                
                  try{
                            delete deleteoptTeamList;
                            flag = true;
                            optFlag = false;
                            List<Opp_Product_Item__c> oppProdItemList = [Select o.Opportunity__c, o.Name, o.Id, (Select Id, Name, Member__c, Member__r.UserRoleId From Product_Sales_Team__r ) From Opp_Product_Item__c o where o.Opportunity__c = :oppId ];   
                            if(oppProdItemList != null && oppProdItemList.size()>0){
                                List<Product_Sales_Team__c> prodSalesTeamList = new List<Product_Sales_Team__c>();
                                for(Opp_Product_Item__c opi :oppProdItemList){
                                    if(opi.Product_Sales_Team__r != null && (opi.Product_Sales_Team__r).size()>0){
                                         prodSalesTeamList.addAll(opi.Product_Sales_Team__r);                        
                                    }
                                }
                                if(prodSalesTeamList != null && prodSalesTeamList.size()>0){
                                    List<Product_Sales_Team__c> deleteProdSalesTeamList = new List<Product_Sales_Team__c>();
                                    for(Product_Sales_Team__c pst :prodSalesTeamList){
                                        if((pst.Member__c != null && deleteForOppItemMap != null) && (deleteForOppItemMap.containsKey(pst.Member__c)) ){
                                             deleteProdSalesTeamList.add(pst);                        
                                        }
                                    }
                                    if(deleteProdSalesTeamList != null && deleteProdSalesTeamList.size()>0){                        
                                            try{
                                                delete deleteProdSalesTeamList;                    
                                                flag = true;
                                                optFlag = false;
                                            }catch(DMLException e){
                                                flag = false;    
                                            }       
                                    }    
                                }
                            } 
                  }catch(DMLException e){
                            flag = false;
                            strMsg = 'You do not have sufficient access to perform this action.';    
                  } 
            }   
        }   
    }
    
    public Map<Id, User> findUsersInRoleHierarchy(Map<Id, User> userObjMap, Map<String, User> tempUserInRoleHierarchyMap){
        Map<Id, User> usersForManager = new Map<Id, User>();
        if(tempUserInRoleHierarchyMap != null && tempUserInRoleHierarchyMap.size()>0 && userObjMap != null && userObjMap.size()>0){
            for(Id i :userObjMap.keySet()){
                User u = userObjMap.get(i);
                if( !(tempUserInRoleHierarchyMap.containsKey(i)) ){
                    if( (u.Temp_Manager_4__c != null && tempUserInRoleHierarchyMap.containsKey(u.Temp_Manager_4__c)) ||
                        (u.Temp_Manager_3__c != null && tempUserInRoleHierarchyMap.containsKey(u.Temp_Manager_3__c)) ||
                        (u.Temp_Manager_2__c != null && tempUserInRoleHierarchyMap.containsKey(u.Temp_Manager_2__c)) ||
                        (u.Temp_Manager_1__c != null && tempUserInRoleHierarchyMap.containsKey(u.Temp_Manager_1__c)) ||
                        (u.Manager_4__c != null && tempUserInRoleHierarchyMap.containsKey(u.Manager_4__c)) ||
                        (u.Manager_3__c != null && tempUserInRoleHierarchyMap.containsKey(u.Manager_3__c)) ||
                        (u.Manager_2__c != null && tempUserInRoleHierarchyMap.containsKey(u.Manager_2__c)) ||
                        (u.Manager_1__c != null && tempUserInRoleHierarchyMap.containsKey(u.Manager_1__c)) 
                    ){
                        usersForManager.put(i, u); 
                        tempUserInRoleHierarchyMap.put(u.Name, u) ;                             
                    }
                }
            }               
        }   
        System.debug('tempUserInRoleHierarchyMap=' + usersForManager);
        return  usersForManager;
    }
    
    public void findChildHierarchyByMap(ID parentRoleId, Map<Id, Set<UserRole>> roleHierarchyChildMap ){
        Set<UserRole> tempChildRoleIdOfParent = new Set<UserRole>();
        if(roleHierarchyChildMap != null && roleHierarchyChildMap.size()>0 && parentRoleId != null){
            if(roleHierarchyChildMap.containsKey(parentRoleId)){
                Set<UserRole> tempUserRoleList = roleHierarchyChildMap.get(parentRoleId);
                tempChildRoleIdOfParent.addAll(tempUserRoleList);
                if(tempChildRoleIdOfParent != null && tempChildRoleIdOfParent.size()>0){
                    System.debug('parentRoleId=' + parentRoleId + ' tempChildRoleIdOfParent='+ tempChildRoleIdOfParent);
                    for(UserRole ur :tempChildRoleIdOfParent){
                        roleIds.add(ur.Id);
                        findChildHierarchyByMap( ur.Id, roleHierarchyChildMap );        
                    }   
                }                           
            }
            
        }
    }
 /*
  * cancel  method for return to previous page while its from Opprotunity Sales Team Member 
  */
  public PageReference cancel(){
      //if(oppId != null){
          return new PageReference('/006/o');//+ oppId
      //}
      return null;
  }  
}