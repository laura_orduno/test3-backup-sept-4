/*****************************************************
    Name: ExternalEventService_Helper
    Usage: This class used define common methods used in service
    Author – Sandip Chaudhari (IBM)
    Revision History -
    Created By/Date - Sandip Chaudhari / 04 July 2016
    
    Updated By/Date/Reason for Change - 
    
******************************************************/
global class ExternalEventService_Helper{
    /*
    * Name - processEvent
    * Description - This method validate event details and create record in salesforce
    * Param - List<ExternalEventRequestResponseParameter.RequestEventData>
    * Return Type - List<ExternalEventRequestResponseParameter.ResponseEventData>
    */
    global static List<ExternalEventRequestResponseParameter.ResponseEventData> processEvent(List<ExternalEventRequestResponseParameter.RequestEventData> eventData){
           
           List<ExternalEventRequestResponseParameter.ResponseEventData> responseData = new List<ExternalEventRequestResponseParameter.ResponseEventData>();
           try{
               //List<Id> eventIdsForAsyncCall = new List<Id>();
               //List<External_Event__c> eventsForSyncCall = new List<External_Event__c>();
               List<External_Event__c> eventList = new List<External_Event__c>();
               for(ExternalEventRequestResponseParameter.RequestEventData requestObject: eventData){
                   //validate event details for mandatory data for elements presetn in JSON
                   List<ExternalEventRequestResponseParameter.EventError> errorList = validateData(requestObject.eventName, requestObject.eventDetails);
                   
                   //Prepare Error object and success objects
                   if (errorList != null && errorList.size()>0){
                       ExternalEventRequestResponseParameter.ResponseEventData responseObject = new ExternalEventRequestResponseParameter.ResponseEventData();
                       responseObject.error = errorList;
                       responseData.add(responseObject);
                   }else{
                       External_Event__c eventObject = new External_Event__c();
                       eventObject.Event_Name__c = requestObject.eventName;
                       eventObject.Event_Details__c = requestObject.eventDetails;
                       eventList.add(eventObject);
                       ExternalEventRequestResponseParameter.ResponseEventData responseObject = new ExternalEventRequestResponseParameter.ResponseEventData();
                       responseObject.eventId = 'TEMPID';
                       responseData.add(responseObject);
                   }
               }
               if(eventList != null && eventList.size() > 0){
                   
                   Database.SaveResult[] srList = Database.insert(eventList, false);
                   
                   List<Id> eventIds = new List<Id>();
                   for (External_Event__c eventObject: eventList) {
                       eventIds.add(eventObject.Id);
                   }
                   List<External_Event__c> insertedList = [SELECT 
                                                          Event_Name__c, Event_Details__c, Event_Process_Error__c, Event_Id__c
                                                        FROM External_Event__c
                                                        WHERE Id IN: eventIds];
                    if(insertedList != null){
                        // update the sucess objects in response data
                        for (External_Event__c eventObject: insertedList) {
                           for (ExternalEventRequestResponseParameter.ResponseEventData responseObject: responseData) {
                               if(responseObject.eventId == 'TEMPID'){
                                   responseObject.eventId = eventObject.Event_Id__c;
                                   break;
                               }
                           }
                        }
                        ExternalEventManager.handleEvent(insertedList, responseData);
                    }
               }
               system.debug('@@@ responseData return' + responseData);
           }catch(LimitException ex){
                /*system.debug('@@@ Exception 1' + ex.getmessage() + ' - ' + ex.getstacktracestring());
                String messageToService = ex.getmessage();
                String errorCode = 'EE-ERR-015';*/
                logException('ExternalEventService_Helper.processEvent', ex.getmessage(), ex.getstacktracestring(), System.Label.ExternalEventLimitException, 'EE-ERR-015', responseData);
           }catch(DMLException ex){
                /*system.debug('@@@ Exception 2' + ex.getmessage() + ' - ' + ex.getstacktracestring());
                String messageToService = ex.getmessage();
                String errorCode = 'EE-ERR-016';*/
                logException('ExternalEventService_Helper.processEvent', ex.getmessage(), ex.getstacktracestring(),  System.Label.ExternalEventDMLException, 'EE-ERR-016', responseData);
           }catch(Exception ex){
                /*system.debug('@@@ Exception 3' + ex.getmessage() + ' - ' + ex.getstacktracestring());
                String messageToService = ex.getmessage();
                String errorCode = 'EE-ERR-017';*/
                logException('ExternalEventService_Helper.processEvent', ex.getmessage(), ex.getstacktracestring(), System.Label.ExternalEventException, 'EE-ERR-017', responseData);
           }
           return responseData;
    }                                 
    

    /*
    * Name - validateData
    * Description - validate event details for mandatory data for elements presetn in JSON
    * Param - String eventName, String eventDetails
    * Return Type - List<ExternalEventRequestResponseParameter.EventError>
    */  
    global static List<ExternalEventRequestResponseParameter.EventError> validateData(String eventName, String eventDetails){
        List<ExternalEventRequestResponseParameter.EventError> errorList = new List<ExternalEventRequestResponseParameter.EventError>();
        Boolean isBlankEventName = string.isBlank(eventName)?true:false;
        Boolean isBlankeventDetails = string.isBlank(eventDetails)?true:false;
        if(isBlankEventName){
            ExternalEventRequestResponseParameter.EventError errorObject1 = new ExternalEventRequestResponseParameter.EventError();
            errorObject1.errorCode = 'EE-ERR-001';
            errorObject1.errorDescription = system.label.ExternalEventBlankEventNameMessage;
            errorList.add(errorObject1);
        }
       if (isBlankeventDetails){
            ExternalEventRequestResponseParameter.EventError errorObject2 = new ExternalEventRequestResponseParameter.EventError();
            errorObject2.errorCode = 'EE-ERR-002';
            errorObject2.errorDescription = system.label.ExternalEventBlankEventDetailMessage;
            errorList.add(errorObject2);
       }else{
            ExternalEventManager.validateEventDetails(eventName, eventDetails, errorList);
       }
     
        return errorList;           
    }
    
    /*
    * Name - logException
    * Description - log exception if there any before executing event handler
    */
    global static void logException(String methodName, String exceptionMsg, String exceptionStack, String messageToService, String errorCode, List<ExternalEventRequestResponseParameter.ResponseEventData> responseData){
        EventHandler_Helper.logError(methodName, '',exceptionMsg, exceptionStack, 'Mediation');
        List<ExternalEventRequestResponseParameter.EventError> errorList = new List<ExternalEventRequestResponseParameter.EventError>();
        ExternalEventRequestResponseParameter.EventError errorObject = new ExternalEventRequestResponseParameter.EventError();
        errorObject.errorCode = errorCode;
        errorObject.errorDescription = messageToService;
        errorList.add(errorObject);
        //responseData = new List<ExternalEventRequestResponseParameter.ResponseEventData>();
        ExternalEventRequestResponseParameter.ResponseEventData responseObject = new ExternalEventRequestResponseParameter.ResponseEventData();
        if(responseData != null && responseData.size()>0){
             responseObject = responseData[0];
             responseObject.eventId = '';
        }else{
             responseData = new List<ExternalEventRequestResponseParameter.ResponseEventData>();
             responseObject = new ExternalEventRequestResponseParameter.ResponseEventData();
        }
        responseObject.error = errorList;
        responseData.add(responseObject);
    }
    
}