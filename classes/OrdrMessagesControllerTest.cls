/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest //(SeeAllData=true)
private class OrdrMessagesControllerTest {
    @isTest
    private static void testOrdrMessagesController() {
        String orderId= OrdrTestDataFactory.singleMethodForOrderId();
        List<Order> orderRecordList = [Select id,orderMgmtId__c,orderMgmtId_Status__c, status,vlocity_cmt__ValidationMessage__c,vlocity_cmt__ValidationStatus__c,vlocity_cmt__ValidationDate__c From order Where Id = :orderId];
        Order orderInst=orderRecordList.get(0);
        ApexPages.StandardController sc = new ApexPages.StandardController(orderInst);
        OrdrMessagesController ordrMsgController=new OrdrMessagesController(sc);
        ApexPages.currentPage().getParameters().put('id',orderId);
        String msgKey=ApexPages.currentPage().getParameters().get('id')+'msg';
        //String ncOrderSubmitStatus=String.valueOf(Cache.Session.get(msgKey));
        ordrMsgController.msgKeyVal='Fail';
        ordrMsgController.readMessage();
        
        ordrMsgController.msgKeyVal='Warn';
        ordrMsgController.readMessage();
         ordrMsgController.msgKeyVal='InProgress';
        ordrMsgController.readMessage();
        ordrMsgController.msgKeyVal='Success';        
        ordrMsgController.readMessage();
        
        ordrMsgController.msgKeyVal=' ';        
        ordrMsgController.readMessage();
        
        ApexPages.StandardController sc1 = new ApexPages.StandardController(orderInst);
        OrdrMessagesController ordrMsgController1=new OrdrMessagesController(sc1);
        
        orderInst.orderMgmtId_Status__c='Submitting';
        orderInst.vlocity_cmt__ValidationStatus__c='SubmitFailed';
        orderInst.orderMgmtId__c='121452345625645';
        orderInst.status='Not Submitted';
        update orderInst;
        ordrMsgController1.readMessage();
        
        ApexPages.StandardController sc2 = new ApexPages.StandardController(orderInst);
        OrdrMessagesController ordrMsgController2=new OrdrMessagesController(sc2);
        orderInst.orderMgmtId_Status__c='Submitting';
        orderInst.vlocity_cmt__ValidationStatus__c='InProgress';
        orderInst.orderMgmtId__c='121452345625646';
        orderInst.status='Not Submitted';
        update orderInst;
        ordrMsgController2.readMessage();
        
        
        ApexPages.StandardController sc3 = new ApexPages.StandardController(orderInst);
        OrdrMessagesController ordrMsgController3=new OrdrMessagesController(sc3);
        orderInst.orderMgmtId_Status__c='Submitting';
        orderInst.vlocity_cmt__ValidationStatus__c='Success';
        orderInst.orderMgmtId__c='121452345625647';
        orderInst.status='Not Submitted';
        update orderInst;
        ordrMsgController3.readMessage();
        
        
        ordrMsgController.getOrderId(); 
        ordrMsgController.setInputParam('Test Message');
        ordrMsgController.getInputParam();
    }
}