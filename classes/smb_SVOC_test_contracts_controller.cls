/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * 
 * This is a testing class for smb_SVOC_contracts_controller
 * 
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 * 
 * Test coverage for smb_SVOC_contracts_controller is 92% on Jan 5th, 2017
 */
@isTest(seeAllData=false)
private class smb_SVOC_test_contracts_controller {

    static testMethod void mySmb_SVOC_contracts_controller() {
        // TO DO: implement unit test
       PageReference ref = new PageReference('/apex/smb_SVOC_Contracts'); 
       Test.setCurrentPage(ref); 
    	
	   RecordType recordType = [Select r.Id, r.DeveloperName From RecordType r where r.SobjectType = 'Account' and r.DeveloperName='RCID' limit 1];
	        
	   Account acc1 = new Account(Name='Testing Software',CustProfId__c= '31722403', BillingCountry = 'US', BillingState = 'IL', recordTypeId=recordType.Id , No_Chronic_Incidents__c =10.0);
	   insert acc1;
	   
	   Contact cont = new Contact(Lastname= 'lastname', Account = acc1);
	   insert cont;
	   
	   ApexPages.currentPage().getParameters().put('accountId', acc1.id); 
	   ApexPages.currentPage().getParameters().put('conNum', '20172623'); 
	   ApexPages.currentPage().getParameters().put('conAssoNum', '002');
	   ApexPages.currentPage().getParameters().put('cAmndNum', '002');
	   ApexPages.currentPage().getParameters().put('cNum', '20172623-002');
	   
	   /*
	   list<long> cnum = new list<long>();
	   cnum.add(200908754);
	   cnum.add(200909875);
	   */
	   

	   //ApexPages.currentPage().getParameters().put('cNum', cnum); 	   
	   
	   
	   smb_SVOC_contracts_controller smb = new smb_SVOC_contracts_controller(); 
	   smb.mapANumberandAddressandDetails = new Map <String,Map<String,List<String>>>();
	   //Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
	   ref = smb.init(); 
	   ref = smb.getAmendmentDetails() ;

	   //smb.findContracts('ytr') ;
	   
	   //findAmendments(String conNum, Integer conAssoNum, String conAmendNum)
	   smb.findAmendments('10', 5, '8');
	   smb.getServiceDetails();
	   //smb_SVOC_contracts_controller.formatphonenumber('123456789');
	   //smb_SVOC_contracts_controller.formatphonenum('787654232');
	   
	   //ref = smb.getAmendmentDetails();
	   
	   
    }
    
   

    @isTest
    private static void get_amendment_details_humbolt()
    {
        ApexPages.currentPage().getParameters().put('cType', 'humbolt');
        ApexPages.currentPage().getParameters().put('cNum', '20138114-000-000');
        ApexPages.currentPage().getParameters().put('cAssoNum', '0');
        ApexPages.currentPage().getParameters().put('contractId', 'contractId_test');
        smb_SVOC_contracts_controller smb = new smb_SVOC_contracts_controller();

        PageReference pageReference = smb.getAmendmentDetails();
        
        System.assert(pageReference ==null, 'pageReference should be null');
    }

    @isTest
    private static void get_amendment_details_velocity()
    {
        ApexPages.currentPage().getParameters().put('cType', 'vlocity');
        ApexPages.currentPage().getParameters().put('contractId', '0');

        smb_SVOC_contracts_controller smb = new smb_SVOC_contracts_controller();

        PageReference pageReference = smb.getAmendmentDetails();
    }

    @isTest
    private static void access_class_properties()
    {
        smb_SVOC_contracts_controller smb = new smb_SVOC_contracts_controller();
        String value = smb.HumboldtUrl;
        value = smb.companyLegalName;
        Integer numericValue = smb.rowCounter;
    }

    @isTest
    private static void formatphonenumber()
    {
      String resultPhone =  smb_SVOC_contracts_controller.formatphonenumber('12345678901');          
      System.debug(' for 12345678901 result Phone=' + resultPhone);      
      System.assertEquals('12345678901', resultPhone, 'should be 12345678901');        
      resultPhone =  smb_SVOC_contracts_controller.formatphonenumber('6042029865');          
      System.debug('for 6042029865 result Phone=' + resultPhone);       
      System.assertEquals('(604) 202-9865', resultPhone, 'should be (604) 202-9865');  
    }

    @isTest
    private static void zeroFill()
    {
        smb_SVOC_contracts_controller smb = new smb_SVOC_contracts_controller();
        smb.zeroFill('A');
        smb.zeroFill('AB');
        smb.zeroFill('ABC');
    }     

    @isTest
	private static void wrapCls_Instance()
    {
        smb_SVOC_contracts_controller.WrapCls wrapClsWithConstructor = new smb_SVOC_contracts_controller.WrapCls('', new List<String>());
    }
    
     @isTest
    private static void translateEntoFr()
    {  
        smb_SVOC_contracts_controller smb = new smb_SVOC_contracts_controller();
        String dateStr = 'Apr 12, 2012';       
        String frDateStr = smb.translateEntoFr( dateStr);
        System.assertEquals('Avril 12, 2012', frDateStr, 'frDateStr does not match the expected value as Avril');
        dateStr = 'Aug 12, 2012';
        frDateStr = smb.translateEntoFr(dateStr);
        System.assertEquals('Août 12, 2012', frDateStr, 'frDateStr does not match the expected value as Août 12, 2012');      
    }
    
    @isTest
    private static void  obtainActiveContractStatuses()
    {
         smb_SVOC_contracts_controller smb = new smb_SVOC_contracts_controller();
         insert  new Contract_Status__c(Status_to_be_displayed__c = 'Activated', Name = 'Status_to_be_displayed');
         Set<String> statuses = smb.obtainActiveContractStatuses();
         System.debug('statuses size=' + statuses.size());
         System.assertEquals(1, statuses.size());
         for(String status: statuses)
         {
             System.debug('status=' + status);
         }
        
         System.assert(statuses.contains('Status_to_be_displayed'));
    }
    
    @isTest
    private static void dateValueConversion()
    {
          smb_SVOC_contracts_controller smb = new smb_SVOC_contracts_controller();
          smb.userLanguage = 'fr';
          List<smb_contract> lstContracts = new List<smb_contract> ();
          cms3_1ContractManagementSvcTypes_v3.Contract contract =  new cms3_1ContractManagementSvcTypes_v3.Contract();
          contract.signedDate = '2000-11-01';
          contract.registeredDate = '2000-11-01';
          contract.expiryDate = '2000-11-01';
          contract.effectiveDate = '2000-11-01';          
            
          smb_Contract smbContract = new smb_Contract(contract);
          lstContracts.add(smbContract);
          smb.lstContracts = lstContracts;
          smb.dateValueConversion();
        
    }
    
    @isTest
    private static void findAmendments()
    {     
       smb_SVOC_contracts_controller smb = new smb_SVOC_contracts_controller();      
       smb.mapANumberandAddressandDetails = new Map <String,Map<String,List<String>>>();
	   smb.findAmendments('20000270', 0, '000');        
       smb.findAmendments('20046536', 5, '000');
       smb.findAmendments('20046536', 0, '000'); 
       smb.findAmendments('20035846', 0, '000'); 
       smb.findAmendments('20036049', 0, '000'); 
       
        
      // smb.findAmendments('nonexisting', 0, '000');
    }
    
    @isTest
    private static void getServiceDetails()
    {
          smb_SVOC_contracts_controller smb = new smb_SVOC_contracts_controller();    
          ApexPages.currentPage().getParameters().put('cAmndNum', '12345');
          smb.mapANumberandAddressandDetails = new Map <String, Map<String,List<String>>>();        
          Map<String, List<String>> addresses = new Map<String, List<String>>();
          List<String> myList = new List<String>();
          myList.add('123456 ABC Ave');
          addresses.put('key@#$', myList);
          smb.mapANumberandAddressandDetails.put('12345', addresses);
          smb.getServiceDetails();
    }
  
    /*
    @isTest
    private static void generateContractNumKey()
    {
        cms3_1ContractManagementSvcTypes_v3.Contract contract = new cms3_1ContractManagementSvcTypes_v3.Contract();
        contract.contractNum='123';
        contract.contractAssociateNum = 'abc';
        contract.contractAmendmentNumber='mnl';
        smb_SVOC_contracts_controller smb = new smb_SVOC_contracts_controller(); 
        String key = smb.generateContractNumKey(contract);
        System.assertEquals('123-abc-mnl', key, 'key value should be 123-abc-mnl');           
    }
    */
    @isTest
    private static void findContracts()
    {
        
        smb_SVOC_contracts_controller smb = new smb_SVOC_contracts_controller();        
        smb.lstContracts = new List<smb_contract>();
        smb.findContracts('35059399',  Id.valueOf('0015400000AWc6XAAT'), null);
      
    }
  
}