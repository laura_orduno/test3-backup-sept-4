@isTest
private class TelusPRMSiteRegisterControllerTest {
    private static testMethod void testRegistration() {
        TelusPRMSiteRegisterController controller = new TelusPRMSiteRegisterController();
        controller.username = 'test@force.com';
        controller.email = 'test@force.com';
        controller.communityNickname = 'test';
        // registerUser will always return null when the page isn't accessed as a guest user
        System.assert(controller.registerUser() == null);    
        
        controller.password = 'abcd1234';
        controller.confirmPassword = 'abcd123';
        System.assert(controller.registerUser() == null);  
    }
}