/*
    *******************************************************************************************************************************
    Class Name:     OC_Util 
    Purpose:        OC_Util         
    Created by:     Sandip Chaudhari
    *******************************************************************************************************************************
*/
 public class OC_Util{
     public OC_Util(){}
     private static final Map<string, List<string>> monthNames = new Map<string, List<string>>{'en' => new List<string> {'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'},
                                                                                              'fr' => new List<string> {'janv', 'févr', 'mars', 'Avril', 'mai', 'juin', 'juil', 'août', 'sept', 'oct', 'nov', 'déc'}};
                                                                                              
     public Static String formatNumber (Decimal numberValue, String currentLanguage){
         if(numberValue != null){
            string regex = '(\\d)(?=(\\d{3})+(?!\\d))';
            Pattern objPt = Pattern.compile('(\\d)(?=(\\d{3})+(?!\\d))');
            Matcher regexMatcher = objPt.matcher(String.valueOf(numberValue));
            String formattedCur;
            if(regexMatcher.find()) {
                formattedCur = String.valueOf(numberValue).replaceAll(regex, '$1,');
            }
            if(formattedCur==null){formattedCur=String.valueOf(numberValue);} // return original number if regex return null value, it returns null in case number is less than 1000
            if(currentLanguage != Label.en_US_Label){
                formattedCur = formattedCur.replace(',', ' ');
                formattedCur = formattedCur.replace('.', ',');
                formattedCur = formattedCur + ' $';
            }else{
                formattedCur = + '$ ' + formattedCur;
            }
            return formattedCur;
        }else{
            if(currentLanguage != Label.en_US_Label){
                return '0.00 $';
            }else{
                return '$ 0.00';
            }
        }
    }
    
    public static String dateTranslation(DateTime dt, String currentLanguage){
        String strDate='';
        try{
            if(dt != null){
                string mon = getMonthName(dt.month(), currentLanguage);
                if (UserInfo.getLocale().startsWith('fr')) {
                     strDate = dt.day()+ ' ' + mon + ', ' + dt.year();
                }else{
                     strDate = dt.formatGMT('MMM dd, yyyy');
                }
            }
        }catch(Exception ex){}
        system.debug('@@@ strDate:' + strDate);
        return strDate;
    }
    
    public static string getMonthName(integer monthNumber, String currentLanguage) {
        if (1 > monthNumber || monthNumber > 12) {
            //todo: throw an error
            return null;
        }
        
        if (currentLanguage != Label.en_US_Label) {
            return monthNames.get('fr').get(monthNumber - 1);
        } 
        else {
            return monthNames.get('en').get(monthNumber - 1);
        }
        
         
    }
}