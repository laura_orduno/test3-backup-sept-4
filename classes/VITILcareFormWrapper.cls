public with sharing class VITILcareFormWrapper {
public VITILcareFormWrapper() {
        authPerson = new AuthorizedPerson();
        getTicketInfo = new GetTicketInfoAct();
        FeedbackInfo = new FeedbackInfoAct();    
        legalNameChange = new LegalNameChange();
        ownerTransfer = new TransferOwnership();
        moveServices = new MoveServices();
        userServiceChange = getListOfUsers();
        addOnUsers = getAddOnUsers();
        renewalUsers = getRenewalUsers();
        hardwares = getHardwares();
        payment = new PaymentInfo();
        additionalInfo = '';
        selectedValue = '';
        billing = new BillingInquiry();
        pickupAddressInfo = new PickupAddressInfo();
        contactChangeInfo = new contactChangeInfoAct();

        // populate default number of returnItemInfos
        returnItemInfos = new List<ReturnItemInfo>();
        for (Integer i = 0; i < 5; i++) returnItemInfos.add(new ReturnItemInfo());
    }
    public List<ServiceUser> userServiceChange {get; set;}
    public List<AddOnUser> addOnUsers {get; set;}
    public List<RenewalUser> renewalUsers {get; set;}
    public List<ReturnItemInfo> returnItemInfos {get; set;}
    public List<Hardware> hardwares {get; set;}
    public string selectedValue {get; set;}
    public AuthorizedPerson authPerson {get; set;}
    public GetTicketInfoAct getTicketInfo {get; set;}
    public TransferOwnership ownerTransfer {get; set;}
    public LegalNameChange legalNameChange {get;set;}
    public MoveServices moveServices {get;set;}
    public String additionalInfo {get; set;}
    public PaymentInfo payment {get; set;}
    public PickupAddressInfo pickupAddressInfo {get;set;}
    public BillingInquiry billing {get; set; }
    public FeedbackInfoAct FeedbackInfo {get; set;}
    public contactChangeInfoAct contactChangeInfo {get; set;}
    
    
    public class AuthorizedPerson {
        public String name {get; set;}
        public String  phoneNumber {get; set;}
        public String email {get; set;}
        public String additionalNote {get; set;}

        public AuthorizedPerson(){
            name='';
            phoneNumber='';
            email='';
            additionalNote='';
        }
    }
        public class FeedbackInfoAct {
            public String FeedbackType{get; set;}
            public String additionalNote {get; set;}

        public FeedbackInfoAct(){
             FeedbackType='';
             additionalNote='';
        }
    }
    
    public class contactChangeInfoAct {
             public String Firstname {get; set;}
         public String Lastname {get; set;}
        public String  phoneNumber {get; set;}
        public String email {get; set;}
         public String requestPortal {get; set;}
              public String prefLanguage{get; set;}
            public String additionalNote {get; set;}

        public contactChangeInfoAct(){
             Firstname='';
             Lastname='';
            phoneNumber='';
            email='';
            requestPortal='';
             prefLanguage='';
             additionalNote='';
        }
    }
    public class GetTicketInfoAct {
        public String subject {get; set;}
        public String projectNum {get; set;}
        public String phoneNumber {get; set;}
        public String siteAddress {get; set;}
        public String additionalNote {get; set;}
        public String ReportedBy {get; set;}
        public String ReportedByCompany {get; set;}
        public String ReportedByPhNum {get; set;}
        public String ReportedByEm {get; set;}
        public String OnsiteContactName {get; set;}
        public String OnsiteContactNumber {get; set;}
        public String SiteCompanyName {get; set;}
        public String AccessHours {get; set;}
        public String CustomerTicketNum {get; set;}
        public String OngoingCalls {get; set;}
        public String SetModelColor {get; set;}
        public String SetLocation {get; set;}
        public String AllSetsAffected {get; set;}
        public String PowerIssue {get; set;}
         public String ConditionTypes {get; set;}
         
          
        public GetTicketInfoAct(){
            subject='';
            projectNum='';
            phoneNumber='';
            siteAddress='';
            additionalNote='';
            ReportedBy = '';
            ReportedByCompany='';
             ReportedByPhNum='';
            ReportedByEm = '';
            OnsiteContactName = '';
             OnsiteContactNumber = '';
            SiteCompanyName = '';
            AccessHours = '';
            CustomerTicketNum = '';
            OngoingCalls = '';
            SetModelColor = '';
            SetLocation = '';
            AllSetsAffected = '';
            PowerIssue = '';
            ConditionTypes = '';
 
        }
    }

    public class TransferOwnership {
        public String name {get; set;}
        public String  phoneNumber {get; set;}
        public String email {get; set;}
        public String transferNumbers {get; set;}
        public String companyName {get; set;}
        public String accountOwner {get; set;}
        public String transferDate {get; set;}

        public TransferOwnership(){
            name='';
            phoneNumber='';
            email='';
            transferNumbers='';
            companyName='';
            accountOwner='';
            transferDate ='';
        }
    }

    public class LegalNameChange {
        public String serviceTypes {get; set;}
        public String newLegalName {get; set;}
        public String operatingName {get; set;}
        public String incorporationNumber {get; set;}
        public String incorporationDate {get; set;}
        public String billingAddress {get; set;}
        public String directorName {get; set;}
        public String directorPhone {get; set;}
        public String directorEmail {get; set;}
        public String authorizedName {get; set;}
        public String authorizedPhone {get; set;}
        public String authorizedEmail {get; set;}
        public String additionalNote {get; set;}

        public LegalNameChange(){
            serviceTypes='';
            newLegalName='';
            operatingName='';
            incorporationNumber='';
            incorporationDate='';
            billingAddress='';
            directorName='';
            directorPhone='';
            directorEmail='';
            authorizedName='';
            authorizedPhone='';
            authorizedEmail='';
            additionalNote='';
        }
    }

    public class MoveServices {
        public String moveDate {get; set;}
        public String movingFrom {get; set;}
        public String movingTo {get; set;}
        public String billingSameAsNew {get; set;}
        public String billingAddress {get; set;}
        public String existingPhoneSystem {get; set;}
        public String existingInterconnector {get; set;}
        public String moveAllServices {get; set;}
        public String movePhoneNumbers {get; set;}
        public String locationFullyWired {get; set;}
        public String contactName {get; set;}
        public String contactPhone {get; set;}
        public String contactEmail {get; set;}
        public String additionalNote {get; set;}

        public MoveServices(){
            moveDate='';
            movingFrom='';
            movingTo='';
            billingSameAsNew='';
            billingAddress='';
            existingPhoneSystem='';
            existingInterconnector='';
            moveAllServices='';
            movePhoneNumbers='';
            locationFullyWired='';
            contactName='';
            contactPhone='';
            contactEmail='';
            additionalNote='';
        }
    }

    public class PaymentInfo{
        public String selectedPaymentType {get; set;}
        public String shippingAddress {get; set;}
        public String shippingName {get; set;}
        public String shippingPhone {get; set;}

        public PaymentInfo(){
            selectedPaymentType='';
            shippingAddress='';
            shippingName='';
            shippingPhone='';
        }
    }

    public class ReturnItemInfo {
        public String returnDeviceOrAccessory {get;set;}
        public String subscriberNumber {get;set;}
        public String newDeviceOrAccessory {get;set;}
        public String additionalInfo {get;set;}
    }

    public class PickupAddressInfo {
        public String address {get;set;}
        public String contactName {get;set;}
        public String contactPhone {get;set;}
    }


    public class ServiceUser{
        public String mobileNo {get; set;}
        public String ratePlan {get; set;}
        public String newService {get; set;}
        public String addInfo {get; set;}

        public ServiceUser(){
            mobileNo ='';
            ratePlan ='';
            newService ='';
            addInfo ='';
        }
    }


    public class RenewalUser{

        public String mobileNo {get; set;}
        public String keepExsiting {get; set;}
        public String accessories {get; set;}
        public String addInfo {get; set;}
        public String device {get; set;}

        public RenewalUser(){
            mobileNo = '';
            device = '';
            keepExsiting = '';
            accessories = '';
            addInfo = '';
        }
    }

    public class AddOnUser{
        public String existingNumber {get; set;}
        public String currentProvider {get; set;}
        public String selectedProvince {get; set;}
        public String selectedCity {get; set;}
        public String existingDevice {get; set;}
        public String existingSIM {get; set;}
        public String newDevice {get; set;}
        public String accessories {get; set;}
        public String addInfo {get; set;}
        public String isTransfer {get; set;}
        public String isOwnDevice {get; set;}

        public AddOnUser(){
            existingNumber = '';
            currentProvider = '';
            selectedProvince = '';
            selectedCity = '';
            existingDevice = '';
            existingSIM = '';
            newDevice = '';
            accessories = '';
            addInfo = '';
            isTransfer = '';
            isOwnDevice = '';
        }
    }

    public class BillingInquiry{
        public String invoiceDate {get; set;}
        public String amount {get; set;}
        public String accNumber {get; set;}
        public String phoneNumbers {get; set;}

        public BillingInquiry(){
            invoiceDate = '';
            amount = '';
            accNumber = '';
            phoneNumbers = '';
        }
    }

    public class Hardware{
        public String accessories {get; set;}
        public String addInfo {get; set;}

        public Hardware(){
            accessories = '';
            addInfo = '';
        }
    }

    public List<ServiceUser> getListOfUsers(){
        List<ServiceUser> serviceUsers = new List<ServiceUser>();
        for(Integer i=0; i<5; i++){
            serviceUsers.add(new ServiceUser());
        }
        return serviceUsers;
    }

    public List<AddOnUser> getAddOnUsers(){
        List<AddOnUser> addOnUsers = new List<AddOnUser>();
        for(Integer i=0; i<5; i++){
            addOnUsers.add(new AddOnUser());
        }
        return addOnUsers;
    }
    
    public List<RenewalUser> getRenewalUsers(){
        List<RenewalUser> renewalUsers = new List<RenewalUser>();
        for(Integer i=0; i<5; i++){
            renewalUsers.add(new RenewalUser());
        }
        return renewalUsers;
    }

    public List<Hardware> getHardwares(){
        List<Hardware> hardwares = new List<Hardware>();
        for(Integer i=0; i<5; i++){
            hardwares.add(new Hardware());
        }
        return hardwares;
    }


    public Boolean isEmptyAddOnUser(AddOnUser aUser){
        return (String.isBlank(aUser.existingNumber) && String.isBlank(aUser.currentProvider) && String.isBlank(aUser.selectedProvince) && String.isBlank(aUser.selectedCity) && String.isBlank(aUser.existingDevice) && String.isBlank(aUser.existingSIM) && String.isBlank(aUser.newDevice) && String.isBlank(aUser.accessories) && String.isBlank(aUser.addInfo) && String.isBlank(aUser.isTransfer) && String.isBlank(aUser.isOwnDevice));
    }

    public Boolean isEmptyHardware(Hardware hw){
        return String.isBlank(hw.accessories) && String.isBlank(hw.addInfo);
    }

    public List<SelectOption> getPaymentMethods(){
        List<SelectOption> paymentMethods = new List<SelectOption>{new SelectOption('', '-Please Select-')};
        paymentMethods.add(new SelectOption(Label.MBRChargeAirtime, Label.MBRChargeAirtime));
        paymentMethods.add(new SelectOption(Label.MBRChargeExisting, Label.MBRChargeExisting));
        paymentMethods.add(new SelectOption('Other', 'Other'));
        return paymentMethods;
    }

    public List<SelectOption> getDeviceTypes(){
        List<SelectOption> deviceTypes = new List<SelectOption>{new SelectOption('', '-Please Select-')};
        deviceTypes.add(new SelectOption('Smartphone/Blackberry 10+', 'Smartphone/Blackberry 10+'));
        deviceTypes.add(new SelectOption('Blackberry Bold, Curve & Torch', 'Blackberry Bold, Curve & Torch'));
        deviceTypes.add(new SelectOption('Mobile Phone', 'Mobile Phone'));
        deviceTypes.add(new SelectOption('Mobile Internet Device', 'Mobile Internet Device'));
        return deviceTypes;
    }

     public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('English','English')); 
        options.add(new SelectOption('French','French')); 
        return options; 
    }   
    public List<SelectOption> getProvinces(){
        List<SelectOption> provinces = new List<SelectOption>();
        provinces.add(new SelectOption('', '-Select Province-'));
        provinces.add(new SelectOption('AB', 'Alberta'));
        provinces.add(new SelectOption('BC', 'British Columbia'));
        provinces.add(new SelectOption('MB', 'Manitoba'));
        provinces.add(new SelectOption('NB', 'New Brunswick'));
        provinces.add(new SelectOption('NF', 'Newfoundland'));
        provinces.add(new SelectOption('NT', 'Northwest Territories'));
        provinces.add(new SelectOption('NS', 'Nova Scotia'));
        provinces.add(new SelectOption('ON', 'Ontario'));
        provinces.add(new SelectOption('PEI', 'Prince Edward Island'));
        provinces.add(new SelectOption('QC', 'Quebec'));
        provinces.add(new SelectOption('SK', 'Saskatchewan'));
        provinces.add(new SelectOption('YT', 'Yukon'));
        return provinces;
    }
    
    public String getOutgoingCallField(){
        return 'true';
    }
    
    public void setOutgoingCallField(){
    }
    
    public void setAllSetsAffectedField(){
    }
    
    public void setPowerIssueField(){
    }
     public void setFeedbackTypeField(){
    }
}