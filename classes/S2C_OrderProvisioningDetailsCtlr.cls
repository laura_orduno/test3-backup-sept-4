/**
 * S2C_OrderProvisioningDetailsCtlr
 * @description Consolidate data that is required to be display regarding
 *              the service request.
 *
 * @author Thomas Tran, Merisha Shim, Traction on Demand
 * @date 03-06-2015
 * @lastmodifiedby Thomas Tran
 * @datelastmodified 03-06-2015
 */
public with sharing class S2C_OrderProvisioningDetailsCtlr {
	private static final String ORDER_OPPORTUNITY_ID = 'orderOppId';
	private static final String SERVICE_REQUEST_ID = 'serviceRequestId';

	public Id orderOppId {get; set;}
	public Id salesOppId {get; set;}
	public Id serviceOrderId {get; set;}

	public String orderOppIdField { get; set; }
	public String orderOppPrimaryOrderContact { get; set; }
	public String orderOppPrimaryOrderContactEmail { get; set; }
	public String orderOppAccountRCID { get; set; }
	public String orderOppAccountCorrectLegalName { get; set; }
	public String orderOppAccountOwnerName { get; set; }
	public String orderOppAccountDeliquency { get; set; }
	public String orderOppAccountLegalNameChecked { get; set; }
	public String orderOppCreditPortalTier { get; set; }
	
	public String orderOppCreditAssessment { get; set; }
	public String orderOppBidReference { get; set; }

	public S2C_OrderProvisioningDetailsCtlr() {
		this.orderOppId = ApexPages.currentPage().getParameters().get(ORDER_OPPORTUNITY_ID);
		this.serviceOrderId = ApexPages.currentPage().getParameters().get(SERVICE_REQUEST_ID);


		List<Service_Request__c> serviceRequestList = [
			SELECT Opportunity_Product_Item__r.Opportunity__c
			FROM Service_Request__c
			WHERE Id = :serviceOrderId
		];
		
		if(!serviceRequestList.isEmpty()){
			this.salesOppId = serviceRequestList[0].Opportunity_Product_Item__r.Opportunity__c;
		} else{
			this.salesOppId = null;
		}

		setOpportunityOrderInformation();
	}

	/**
	 * setOpportunityOrderInformation
	 * @description Set the Order Opportunity fields
	 *
	 * @author Merisha Shim, Traction on Demand
	 * @date 03-02-2015
	 */
	 private void setOpportunityOrderInformation() {
	 	if(orderOppId != null){
		 	Opportunity o = [SELECT Order_ID__c, Primary_Order_Contact__r.Name, Primary_Order_Contact__r.Email, Account.RCID__c, Account.Correct_Legal_Name__c, Account.Owner.Name,
		 						Account.Delinquency_Flag__c, Account.Legal_Name_Checked__c, Account.Credit_Portal_Tier__c, Credit_Assesment__c, 
		 						 Account.CreditRef_Num__c, Credit_Assessment__r.Name,Credit_Assessment__c
							 FROM Opportunity
							 WHERE Id = :orderOppId];


			
			orderOppIdField = o.Order_ID__c;
			orderOppPrimaryOrderContact = o.Primary_Order_Contact__r.Name;
			orderOppPrimaryOrderContactEmail = o.Primary_Order_Contact__r.Email;
			orderOppAccountRCID = o.Account.RCID__c;
			orderOppAccountCorrectLegalName = o.Account.Correct_Legal_Name__c;
			orderOppAccountOwnerName = o.Account.Owner.Name;
			orderOppAccountDeliquency = o.Account.Delinquency_Flag__c;
			orderOppAccountLegalNameChecked = o.Account.Legal_Name_Checked__c;
			orderOppCreditPortalTier = o.Account.Credit_Portal_Tier__c;
			if(o.Credit_Assessment__c != null){
				orderOppCreditAssessment = o.Credit_Assessment__r.Name;
			} else{
				if(String.isNotBlank(o.Credit_Assesment__c)){
					orderOppCreditAssessment = o.Credit_Assesment__c;
				} else{
					orderOppCreditAssessment = o.Account.CreditRef_Num__c;
				}
			}

			if(salesOppId != null) {
				//get the Sales Opportunity's Bid Reference
				orderOppBidReference = [SELECT Bid_Reference__c FROM Opportunity WHERE Id =: salesOppId].Bid_Reference__c;
			}
		}
	 }

	 /**
	 * getSRSPodsTemplate
	 * @description Returns a list of SRS_PODS_Template__c.
	 *
	 * @author Thomas Tran, Traction on Demand
	 * @date 03-09-2015
	 */
	public List<QuestionAnswer> getSRSPodsTemplateInfo(){
		List<QuestionAnswer> qaList = new List<QuestionAnswer>();
		Map<String, String> qaMap = new Map<String, String>();

		for(SRS_PODS_Answer__c currentsrsPodsTemplateTemp : [SELECT SRS_PODS_Products__r.QUESTION_EN__c, SRS_PODS_Answer__c
																FROM SRS_PODS_Answer__c
																WHERE Service_Request__c =: serviceOrderId AND SRS_PODS_Products__c != null]) {

			if(!qaMap.containsKey(currentsrsPodsTemplateTemp.SRS_PODS_Products__r.QUESTION_EN__c)) {
				qaMap.put(currentsrsPodsTemplateTemp.SRS_PODS_Products__r.QUESTION_EN__c, currentsrsPodsTemplateTemp.SRS_PODS_Answer__c);
			} else {
				if(qaMap.get(currentsrsPodsTemplateTemp.SRS_PODS_Products__r.QUESTION_EN__c) == null && currentsrsPodsTemplateTemp.SRS_PODS_Answer__c != null) {
					qaMap.put(currentsrsPodsTemplateTemp.SRS_PODS_Products__r.QUESTION_EN__c, currentsrsPodsTemplateTemp.SRS_PODS_Answer__c);
				}
			}
		}

		for(String s : qaMap.keyset()) {
			QuestionAnswer qa = new QuestionAnswer(s, qaMap.get(s));
			qaList.add(qa);
		}

		return qaList;
	}

	/**
	 * getWorkRequest
	 * @description Returns a list of Contracts__c.
	 *
	 * @author Thomas Tran, Traction on Demand
	 * @date 03-09-2015
	 */
	public List<Work_Request__c> getWorkRequestInfo(){
		if(serviceOrderId != null){
			return [
				SELECT Type__c, Description__c, Owner.Name, LBCO_Calculator__c,  Contract_Status_Type__c, Pending_Reason__c, Status__c
				FROM Work_Request__c
				WHERE Service_Request__c = :serviceOrderId
			];
		} else{
			return new List<Work_Request__c>();
		}
	}

	/**
	 * getContractInfo
	 * @description Returns the Contract Lookup Details on the Service Request.
	 *
	 * @author Merisha Shim, Traction on Demand
	 * @date 03-12-2015
	 */
	public List<Contracts__c> getContractInfo() {
		if(serviceOrderId != null){
			return [
				SELECT Contract_Type__c, Contract_Number__c, Contract_Term_Months__c, Contract_Remarks__c
				FROM Contracts__c
				WHERE Contract__c = :serviceOrderId
			];
		} else{
			return new List<Contracts__c>();
		}
	}

	/**
	 * getServiceRequestInfo
	 * @description Returns a list of Service_Request__c.
	 *
	 * @author Thomas Tran, Traction on Demand
	 * @date 03-02-2015
	 */
	public List<Service_Request__c> getServiceRequestInfo(){
        List<Service_Request__c> srInfo = 
		[
            // SRS_Service_Request_Charge__r.Charge_Type__c
			SELECT Name, Service_Request_Status_Formula__c, RecordType.Name, SRS_Service_Address__r.Postal_Code__c, Details_Of_Service__c, Description__c, BTN__c, SRS_PODS_Product__r.Name, Contract_Length_Months__c,Contract_Length__c, Opportunity_Product_Item__r.Existing_Contract_Nbr__c, Target_Date__c, Service_Identifier_Type__c, Existing_Service_Id__c, Billing_Account_New__c, Billing_Account__r.BillingStreet, Billing_Account__r.BillingCity, Billing_Account__r.BillingState, Billing_Account__r.BillingPostalCode, Billing_Account__r.BillingCountry, Billing_Address__c,Target_Date_Type__c,
			(SELECT Full_Address__c, City__c, Province__c, Address__r.Postal_Code__c, NPA_NXX__c, RecordType.Name
				FROM Service_Address__r), 
			(SELECT Charge_Type__c, Amount__c, Location__c, Approved_By_TELUS__r.Name, Approved_Date__c, Pre_approved__c, Charge_Method__c, EBAM__c, Charge_Remarks__c, Status__c 
				FROM Billable_Charges__r
				WHERE Charge_Type__c = 'Monthly Recurring' 
				OR Charge_Type__c = 'Activation/Non-Recurring'
				OR Charge_Type__c = 'Usage'
				OR Charge_Type__c = 'Access Construction'),
			(SELECT Contact__r.Name, Contact_Type__c, Contact__r.Email, Phone__c, Mobile__c  
				FROM Service_Request_Contacts__r)   
			FROM Service_Request__c
			WHERE Id = :serviceOrderId
		];
        
        // format long text so that line breaks display on visualforce page
        for(Service_Request__c sr : srInfo){
            if(sr.Details_Of_Service__c != null){
            	sr.Details_Of_Service__c = sr.Details_Of_Service__c.replace('\n', '</br>');
            }    
        }
        
        return srInfo;
	}

	/**
	 * QuestionAnswer
	 * @description Inner classes used to pair the questions and answers together.
	 *
	 * @author Thomas Tran, Traction on Demand
	 * @date 03-02-2015
	 */	
	public class QuestionAnswer{
		public String question {get; set;}
		public String answer {get; set;}

		public QuestionAnswer(String question, String answer){
			this.question = question;
			this.answer = answer;
		}
	}
}