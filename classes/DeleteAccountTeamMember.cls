/*****************************************************
    Name: DeleteAccountTeamMember
    Usage: This class use to Delete Account Team Member 
    Author – Clear Task
    Date – 04/04/2011
    Revision History
******************************************************/
public without sharing class DeleteAccountTeamMember{
  // used for account Id after click on 'Delete Me From Account Team' Button in Lead
    private String actId = ApexPages.currentPage().getParameters().get('actId');
  //show the message 
    public boolean flag{get; set;} 
    //show the account team block 
    public boolean actFlag{get; set;} 
  //show the message 
    public String strMsg{get; set;}  
   //show the message 
    public Set<Id> roleIds{get; set;}    
   //account team wrapper
    public List<AccountTeamWrapper> accountTeamWrapperList{get;set;}
   //deleted member
   public Integer deletedMember{get;set;}
 /*
  * init method for to Delete Account Team Member 
  */
    public void init(){
        flag = false; 
        actFlag = false;
        strMsg = '';
        if(actId != null && actId != ''){
            Boolean flagManager = true;
            Id userId = UserInfo.getUserId();
            Id userRoleId = UserInfo.getUserRoleId();
            System.debug('userId =' +userId + 'userRoleId =' + userRoleId );
            List<AccountTeamMember> actTeamList = new List<AccountTeamMember>();
            actTeamList = [Select a.User.UserRoleId, a.UserId, a.Id, a.AccountId, a.Account.OwnerId, a.TeamMemberRole  From AccountTeamMember a where  a.AccountId = :actId and a.UserId = :userId ];   
            List<UserRole> userRoleList = [ Select u.ParentRoleId, u.Name, u.Id From UserRole u where u.Id = :userRoleId ]; 
            if(actTeamList != null && actTeamList.size()== 0 &&  userRoleList != null && userRoleList.size()>0 && (userRoleList[0].Name).contains('Manager')){  
                actTeamList = [Select a.User.UserRoleId, a.UserId, a.Id, a.AccountId, a.Account.OwnerId, a.TeamMemberRole  From AccountTeamMember a where  a.AccountId = :actId ]; 
                flagManager = false;      
            }
            if(actTeamList != null && actTeamList.size()>0){                
                roleIds = new Set<Id>();
                if(userRoleList != null && userRoleList.size()>0 && (userRoleList[0].Name).contains('Manager')){
                    
                    //Id userRoleId = actTeamList[0].User.UserRoleId ;
                    roleIds.add(userRoleId);
                    Map<Id, Set<UserRole>> roleHierarchyChildMap = new Map<Id, Set<UserRole>>();
                    List<UserRole> roleList = [ Select u.ParentRoleId, u.Name, u.Id From UserRole u];
                    
                    if(roleList != null && roleList.size()>0){
                        
                        for(UserRole ur :roleList){
                            if(ur.ParentRoleId != null){
                                if(roleHierarchyChildMap != null && roleHierarchyChildMap.containsKey(ur.ParentRoleId)){
                                    Set<UserRole> tempUserRoleList = roleHierarchyChildMap.get(ur.ParentRoleId);
                                    tempUserRoleList.add(ur);   
                                }else{
                                    Set<UserRole> tempUserRoleList = new Set<UserRole>();
                                    tempUserRoleList.add(ur);
                                    roleHierarchyChildMap.put(ur.ParentRoleId, tempUserRoleList);   
                                }   
                            }
                        }
                                
                        System.debug('roleHierarchyChildMap=' + roleHierarchyChildMap.get(userRoleId));
                        findChildHierarchyByMap(userRoleId, roleHierarchyChildMap);
                        System.debug('roleIds--Size=' + roleIds.size());
                        
                        if(roleIds != null && roleIds.size()>0){
                            
                            Map<Id, User> usersObjMap = new Map<Id, User>([ Select u.Id, u.Name, u.Username, u.UserRoleId,  u.system_admin_manager__c, 
                                            u.Temp_Manager_4__c, u.Temp_Manager_3__c, u.Temp_Manager_2__c, u.Temp_Manager_1__c, u.Sales_Manager__c, 
                                            u.Manager_4__c, u.Manager_3__c, u.Manager_2__c, u.Manager_1__c, u.ManagerId  From User u where u.UserRoleId in :roleIds ]);     
                            
                            /* find user in role hierarchy -start*/
                            Map<String, User> currentUserMap = new Map<String, User>();
                            User currentUser = usersObjMap.get(userId);
                            currentUserMap.put(currentUser.Name, currentUser);
                            Map<Id, User> userObjMap = new Map<Id, User>(); 
                            userObjMap = findUsersInRoleHierarchy(usersObjMap, currentUserMap); 
                            /* find user in role hierarchy -end*/  
                                                     
                            if(userObjMap != null && userObjMap.size()>0){
                                userObjMap.put(currentUser.id, currentUser);
                                List<AccountTeamMember> allActTeamList = [Select a.UserId, a.Id, a.AccountId, a.Account.OwnerId, a.TeamMemberRole  From AccountTeamMember a where  a.AccountId = :actId ];      
                                if(allActTeamList != null && allActTeamList.size()>0){
                                    flag = false;
                                    actFlag = true;
                                    strMsg = '';
                                    accountTeamWrapperList = new  List<AccountTeamWrapper>();
                                    
                                    if(flagManager){
                                        AccountTeamWrapper atwMngrObj = new AccountTeamWrapper(actTeamList[0]);
                                        accountTeamWrapperList.add(atwMngrObj);
                                    }
                                    
                                    for(AccountTeamMember at :allActTeamList){
                                        if(at.UserId != userId && userObjMap.containsKey(at.UserId)){
                                            AccountTeamWrapper atwObj = new AccountTeamWrapper(at); 
                                            accountTeamWrapperList.add(atwObj);                                         
                                        }   
                                    }
                                    System.debug('accountTeamWrapperList=' + accountTeamWrapperList);
                                    
                                    if(accountTeamWrapperList != null && accountTeamWrapperList.size()== 1 && flagManager == true){
                                         try{
                                            delete actTeamList ;                    
                                            flag = true;
                                            actFlag = false;
                                         }catch(DMLException e){
                                            flag = false;    
                                         }     
                                    }else if(accountTeamWrapperList != null && accountTeamWrapperList.size()== 0 && flagManager == false){
                                        flag = false;
                                        strMsg = '';
                                        actFlag = false;    
                                    }
                                }
                            }
                            
                        }                       
                    }                   
                }else{
                    //in case user is not manager
                    try{
                        delete actTeamList;
                        flag = true;
                        actFlag = false;
                    }catch(DMLException e){
                        flag = false;
                        strMsg = 'You do not have sufficient access to perform this action.';    
                    } 
                }               
            }else{
                flag = false;
                strMsg = '';
            }
        }
    }
    
    public Map<Id, User> findUsersInRoleHierarchy(Map<Id, User> userObjMap, Map<String, User> tempUserInRoleHierarchyMap){
        Map<Id, User> usersForManager = new Map<Id, User>();
        if(tempUserInRoleHierarchyMap != null && tempUserInRoleHierarchyMap.size()>0 && userObjMap != null && userObjMap.size()>0){
            for(Id i :userObjMap.keySet()){
                User u = userObjMap.get(i);
                if( !(tempUserInRoleHierarchyMap.containsKey(i)) ){
                    if( (u.Temp_Manager_4__c != null && tempUserInRoleHierarchyMap.containsKey(u.Temp_Manager_4__c)) ||
                        (u.Temp_Manager_3__c != null && tempUserInRoleHierarchyMap.containsKey(u.Temp_Manager_3__c)) ||
                        (u.Temp_Manager_2__c != null && tempUserInRoleHierarchyMap.containsKey(u.Temp_Manager_2__c)) ||
                        (u.Temp_Manager_1__c != null && tempUserInRoleHierarchyMap.containsKey(u.Temp_Manager_1__c)) ||
                        (u.Manager_4__c != null && tempUserInRoleHierarchyMap.containsKey(u.Manager_4__c)) ||
                        (u.Manager_3__c != null && tempUserInRoleHierarchyMap.containsKey(u.Manager_3__c)) ||
                        (u.Manager_2__c != null && tempUserInRoleHierarchyMap.containsKey(u.Manager_2__c)) ||
                        (u.Manager_1__c != null && tempUserInRoleHierarchyMap.containsKey(u.Manager_1__c)) 
                    ){
                        usersForManager.put(i, u); 
                        tempUserInRoleHierarchyMap.put(u.Name, u) ;                             
                    }
                }
            }               
        }   
        System.debug('tempUserInRoleHierarchyMap=' + usersForManager);
        return  usersForManager;
    }
    
    public void findChildHierarchyByMap(ID parentRoleId, Map<Id, Set<UserRole>> roleHierarchyChildMap ){
        Set<UserRole> tempChildRoleIdOfParent = new Set<UserRole>();
        if(roleHierarchyChildMap != null && roleHierarchyChildMap.size()>0 && parentRoleId != null){
            if(roleHierarchyChildMap.containsKey(parentRoleId)){
                Set<UserRole> tempUserRoleList = roleHierarchyChildMap.get(parentRoleId);
                tempChildRoleIdOfParent.addAll(tempUserRoleList);
                if(tempChildRoleIdOfParent != null && tempChildRoleIdOfParent.size()>0){
                    System.debug('parentRoleId=' + parentRoleId + ' tempChildRoleIdOfParent='+ tempChildRoleIdOfParent);
                    for(UserRole ur :tempChildRoleIdOfParent){
                        roleIds.add(ur.Id);
                        findChildHierarchyByMap( ur.Id, roleHierarchyChildMap );        
                    }   
                }                           
            }
            
        }
    }
    
    public void deleteAccountTeamMember(){
        if(accountTeamWrapperList != null && accountTeamWrapperList.size()>0){
            List<AccountTeamMember> deleteActTeamList = new List<AccountTeamMember>();
            for(AccountTeamWrapper atwObj :accountTeamWrapperList){
                if(atwObj.checked != null && atwObj.checked == true){
                    deleteActTeamList.add(atwObj.accountTeam);  
                }   
            }
            System.debug('deleteActTeamList=' + deleteActTeamList);
            if(deleteActTeamList != null && deleteActTeamList.size()>0){                
                  try{
                            delete deleteActTeamList;
                            flag = true;
                            actFlag = false;
                  }catch(DMLException e){
                            flag = false;
                            strMsg = 'You do not have sufficient access to perform this action.';    
                  } 
            }   
        }   
    }
   
 /*
  * cancel  method for return to previous page while its from Account
  */
  public PageReference cancel(){
      if(actId != null){
          return new PageReference('/001/o' );
      }
      return null;
  }
  
  /*public void findChildHierarchy(ID parentRoleId, List<UserRole> roleList ){
        Set<Id> tempChildRoleIdOfParent = new Set<Id>();
        if(roleList != null && parentRoleId != null){
            for(UserRole ur :roleList){
                if(ur.ParentRoleId == parentRoleId){
                    tempChildRoleIdOfParent.add(ur.Id);
                    roleIds.add(ur.Id); 
                }       
            }
            if(tempChildRoleIdOfParent != null && tempChildRoleIdOfParent.size()>0){
                System.debug('parentRoleId=' + parentRoleId + ' tempChildRoleIdOfParent='+ tempChildRoleIdOfParent);
                for(Id i :tempChildRoleIdOfParent){
                    findChildHierarchy( i, roleList );      
                }   
            }
        }
    }*/
      
}