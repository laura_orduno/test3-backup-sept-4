global class ServiceRequestContactCallback{
    @future
    webservice static void onServiceRequestsCreated(list<id> serviceRequestIds){
        if(serviceRequestIds!=null&&!serviceRequestIds.isempty()){
            list<service_request__c> serviceRequests=[select id,name,PrimaryContact__c,Required_To_Hand_Off_To_Care_checklist__c from service_request__c where id=:serviceRequestIds];
            if(!serviceRequests.isempty()){
                List<Service_Request_Contact__c> lstSerRequestContacts = new List<Service_Request_Contact__c>(); 
                for(Service_Request__c SR : serviceRequests){
                    if(SR.PrimaryContact__c != null){
                        SR.Required_To_Hand_Off_To_Care_checklist__c='';
                        Service_Request_Contact__c serRequestContact = new Service_Request_Contact__c();    
                        
                        serRequestContact.Service_Request__c = SR.id;
                        serRequestContact.contact__c = SR.PrimaryContact__c;
                        //serRequestContact.Contact_Type__c = 'Customer Onsite';
                        //Updated by PRiyanka Dhomne US - 002 
                        serRequestContact.Contact_Type__c = system.label.SRS_ContactTypeRequestingCustomer;///'Requesting Customer';
                        
                        lstSerRequestContacts.add(serRequestContact);
                    }
                }
                
                if(lstSerRequestContacts.size() > 0){
                    try{
                        insert lstSerRequestContacts;
                        update serviceRequests;
                    }
                    catch(DmlException e){
                        throw e;
                    }
                } 
            }
        }
    }
}