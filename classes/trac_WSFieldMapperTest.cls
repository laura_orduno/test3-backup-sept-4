/*
 * Tests for trac_WSFieldMapper
 * Part of the SMB Repairs project
 *
 * @author: Grant Adamson, Traction On Demand
 
Change Log:
July 8, 2014 - Andy Leung
-  Updated query to dynamic query
 */

@isTest
private class trac_WSFieldMapperTest {
	
	public static testMethod void testTracRepairResultToCase() {
		Case c = new Case(subject = 'test');
		insert c;
		
		trac_RepairWS.RepairRequest req = new trac_RepairWS.RepairRequest();
		req.caseId = c.id;
		req.repairId = 111111111111111112L;
		req.clientName.title = 'Mr.';
		req.clientName.firstName = 'Tim';
		req.clientName.middleName = 'Tiberius';
		req.clientName.lastName = 'Tester';
		req.defectiveSerialNumber = trac_RepairTestUtils.REPAIR_ESN_SUCCESS;
		req.mobileNumber = '6044567890';
		req.defectiveSKU = 'sku';
		req.contactName.title = 'Mrs.';
		req.contactName.firstName = 'Bob';
		req.contactName.middleName = 'Herbert';
		req.contactName.lastName = 'Smith';
		req.contactPhoneNumber = '7782223344';
		req.contactEmailAddress = 'contact@example.com';
		req.clientShippingAddress.streetName = 'shippingStreetName';
		req.clientShippingAddress.municipalityName = 'shippingMuniName';
		req.clientShippingAddress.provinceStateCode = 'BC';
		req.clientShippingAddress.postalZipCode = 'shipzip';
		req.clientBusinessName = 'bizname';
		req.address.streetName = 'streetName';
		req.address.municipalityName = 'muniName';
		req.address.provinceStateCode = 'AB';
		req.address.postalZipCode = 'zipcode';
		//req.physicalDamageInd = true;
		req.issue = 'it broke';
		req.defectsBehaviour = 'screen don\'t work right';
		req.descriptionAndTroubleshooting = 'turned it on and off';
		req.cosmeticDamageInd = true;
		req.descriptionOfCosmeticDamage = 'scuff marks';
		req.estimatedRepairCost = 250;
		req.languageCD = 'EN';
		
		trac_WSFieldMapper.tracRepairResultToCase(req, c);
		
		// record should update without error
		update c;
		
		/*c = [
			SELECT repair__c, Customer_Title_client__c, Customer_First_Name_client__c, Customer_Middle_Name_client__c,
						 Customer_Last_Name_client__c, ESN_MEID_IMEI__c, Mobile_number__c, Model_SKU__c, Customer_Title_contact__c,
						 Customer_First_Name_contact__c, Customer_Middle_Name_contact__c, Customer_Last_Name_contact__c,
						 Customer_Phone_contact__c, Customer_Email_contact__c, Address_shipping__c, City_shipping__c, Province_shipping__c,
						 Postal_Code_shipping__c, Business_Name__c, Address_account__c, City_account__c, Province_account__c,
						 Postal_Code_account__c, Physical_Damage__c, Issue_System__c, Defects_behaviour__c,
						 Descrip_of_Defect_Troubleshooting_Perf__c, Is_there_any_Cosmetic_Damage__c, Comments_create_repair__c,
						 Estimated_Repair_Cost_Up_To__c
			FROM Case
			WHERE id = :c.id
		];*/
		c=(Case)database.query('SELECT repair__c, Customer_Title_client__c, Customer_First_Name_client__c, Customer_Middle_Name_client__c,Customer_Last_Name_client__c, ESN_MEID_IMEI__c, Mobile_number__c, Model_SKU__c, Customer_Title_contact__c,Customer_First_Name_contact__c, Customer_Middle_Name_contact__c, Customer_Last_Name_contact__c,Customer_Phone_contact__c, Customer_Email_contact__c, Address_shipping__c, City_shipping__c, Province_shipping__c,Postal_Code_shipping__c, Business_Name__c, Address_account__c, City_account__c, Province_account__c,Postal_Code_account__c, Physical_Damage__c, Issue_System__c, Defects_behaviour__c,Descrip_of_Defect_Troubleshooting_Perf__c, Is_there_any_Cosmetic_Damage__c, Comments_create_repair__c,Estimated_Repair_Cost_Up_To__c FROM Case WHERE id =\''+c.id+'\'');	 
	 	//string physicalDamage=(string)c.get('Physical_Damage__c');
	 	system.assertEquals(req.repairId + '', c.Repair__c);
		system.assertEquals(req.clientName.title, c.Customer_Title_client__c);
		system.assertEquals(req.clientName.firstName, c.Customer_First_Name_client__c);
		system.assertEquals(req.clientName.middleName, c.Customer_Middle_Name_client__c);
		system.assertEquals(req.clientName.lastName, c.Customer_Last_Name_client__c);
		system.assertEquals(req.defectiveSerialNumber, c.ESN_MEID_IMEI__c);
		system.assertEquals(req.mobileNumber, c.Mobile_number__c);
		system.assertEquals(req.defectiveSKU, c.Model_SKU__c);
		system.assertEquals(req.contactName.title,c.Customer_Title_contact__c);
		system.assertEquals(req.contactName.firstName, c.Customer_First_Name_contact__c);
		system.assertEquals(req.contactName.middleName, c.Customer_Middle_Name_contact__c);
		system.assertEquals(req.contactName.lastName, c.Customer_Last_Name_contact__c);
		system.assertEquals(req.contactPhoneNumber, c.Customer_Phone_contact__c);
		system.assertEquals(req.contactEmailAddress, c.Customer_Email_contact__c);
		system.assertEquals(req.clientShippingAddress.streetName, c.Address_shipping__c);
		system.assertEquals(req.clientShippingAddress.municipalityName, c.City_shipping__c);
		system.assertEquals(req.clientShippingAddress.provinceStateCode, c.Province_shipping__c);
		system.assertEquals(req.clientShippingAddress.postalZipCode, c.Postal_Code_shipping__c);
		system.assertEquals(req.clientBusinessName, c.Business_Name__c);
		//system.assertEquals(req.physicalDamageInd, (string.isnotblank(physicalDamage)&&physicalDamage.equalsignorecase('yes')?true:false));
		system.assertEquals(req.issue, c.Issue_System__c);
		system.assertEquals(req.defectsBehaviour, c.Defects_behaviour__c);
		system.assertEquals(req.descriptionAndTroubleshooting, c.Descrip_of_Defect_Troubleshooting_Perf__c);
		system.assertEquals(req.cosmeticDamageInd, c.Is_there_any_Cosmetic_Damage__c);
		system.assertEquals(req.descriptionOfCosmeticDamage, c.Comments_create_repair__c);
		system.assertEquals(req.estimatedRepairCost, c.Estimated_Repair_Cost_Up_To__c);
	}
	
	
	public static testMethod void testTracRepairResultToCaseAccessory() {
		Case c = new Case(subject = 'test');
		c.Customer_Email_contact__c = 'original@example.com';
		c.Repair_Type__c = trac_WSFieldMapper.ACCESSORY_REPAIR_TYPE;
		c.Accessory_SKU__c = 'test';
		c.accessory_purchase_date__c = Date.today();
		insert c;
		
		trac_RepairWS.RepairRequest req = new trac_RepairWS.RepairRequest();
		req.caseId = c.id;
		req.repairId = 111111111111111112L;
		req.clientName.title = 'Mr.';
		req.clientName.firstName = 'Tim';
		req.clientName.middleName = 'Tiberius';
		req.clientName.lastName = 'Tester';
		req.defectiveSKU = 'sku';
		req.contactName.title = 'Mrs.';
		req.contactName.firstName = 'Bob';
		req.contactName.middleName = 'Herbert';
		req.contactName.lastName = 'Smith';
		req.contactPhoneNumber = '7782223344';
		req.contactEmailAddress = 'contact@example.com';
		req.clientShippingAddress.streetName = 'shippingStreetName';
		req.clientShippingAddress.municipalityName = 'shippingMuniName';
		req.clientShippingAddress.provinceStateCode = 'BC';
		req.clientShippingAddress.postalZipCode = 'shipzip';
		req.clientBusinessName = 'bizname';
		req.address.streetName = 'streetName';
		req.address.municipalityName = 'muniName';
		req.address.provinceStateCode = 'AB';
		req.address.postalZipCode = 'zipcode';
		//req.physicalDamageInd = true;
		req.issue = 'it broke';
		req.defectsBehaviour = 'screen don\'t work right';
		req.descriptionAndTroubleshooting = 'turned it on and off';
		req.cosmeticDamageInd = true;
		req.descriptionOfCosmeticDamage = 'scuff marks';
		req.estimatedRepairCost = 250;
		req.languageCD = 'EN';
		
		trac_WSFieldMapper.tracRepairResultToCase(req, c);
		
		// record should update without error
		update c;
		
		/*c = [
			SELECT repair__c, Customer_Title_client__c, Customer_First_Name_client__c, Customer_Middle_Name_client__c,
						 Customer_Last_Name_client__c, ESN_MEID_IMEI__c, Mobile_number__c, Model_SKU__c, Customer_Title_contact__c,
						 Customer_First_Name_contact__c, Customer_Middle_Name_contact__c, Customer_Last_Name_contact__c,
						 Customer_Phone_contact__c, Customer_Email_contact__c, Address_shipping__c, City_shipping__c, Province_shipping__c,
						 Postal_Code_shipping__c, Business_Name__c, Address_account__c, City_account__c, Province_account__c,
						 Postal_Code_account__c, Physical_Damage__c, Issue_System__c, Defects_behaviour__c,
						 Descrip_of_Defect_Troubleshooting_Perf__c, Is_there_any_Cosmetic_Damage__c, Comments_create_repair__c,
						 Estimated_Repair_Cost_Up_To__c
			FROM Case
			WHERE id = :c.id
		];*/
		c=(Case)database.query('SELECT repair__c, Customer_Title_client__c, Customer_First_Name_client__c, Customer_Middle_Name_client__c,Customer_Last_Name_client__c, ESN_MEID_IMEI__c, Mobile_number__c, Model_SKU__c, Customer_Title_contact__c,Customer_First_Name_contact__c, Customer_Middle_Name_contact__c, Customer_Last_Name_contact__c,Customer_Phone_contact__c, Customer_Email_contact__c, Address_shipping__c, City_shipping__c, Province_shipping__c,Postal_Code_shipping__c, Business_Name__c, Address_account__c, City_account__c, Province_account__c,Postal_Code_account__c, Physical_Damage__c, Issue_System__c, Defects_behaviour__c,Descrip_of_Defect_Troubleshooting_Perf__c, Is_there_any_Cosmetic_Damage__c, Comments_create_repair__c,Estimated_Repair_Cost_Up_To__c FROM Case WHERE id =\''+c.id+'\'');	 
	 	//string physicalDamage=(string)c.get('Physical_Damage__c');
	 	system.assertEquals(req.repairId + '', c.Repair__c);
		system.assertEquals(req.clientName.title, c.Customer_Title_client__c);
		system.assertEquals(req.clientName.firstName, c.Customer_First_Name_client__c);
		system.assertEquals(req.clientName.middleName, c.Customer_Middle_Name_client__c);
		system.assertEquals(req.clientName.lastName, c.Customer_Last_Name_client__c);
		system.assertEquals(req.defectiveSerialNumber, c.ESN_MEID_IMEI__c);
		system.assertEquals(req.mobileNumber, c.Mobile_number__c);
		system.assertEquals(req.defectiveSKU, c.Model_SKU__c);
		system.assertEquals(req.contactName.title,c.Customer_Title_contact__c);
		system.assertEquals(req.contactName.firstName, c.Customer_First_Name_contact__c);
		system.assertEquals(req.contactName.middleName, c.Customer_Middle_Name_contact__c);
		system.assertEquals(req.contactName.lastName, c.Customer_Last_Name_contact__c);
		system.assertEquals(req.contactPhoneNumber, c.Customer_Phone_contact__c);
		system.assertEquals('original@example.com', c.Customer_Email_contact__c);
		system.assertEquals(req.clientShippingAddress.streetName, c.Address_shipping__c);
		system.assertEquals(req.clientShippingAddress.municipalityName, c.City_shipping__c);
		system.assertEquals(req.clientShippingAddress.provinceStateCode, c.Province_shipping__c);
		system.assertEquals(req.clientShippingAddress.postalZipCode, c.Postal_Code_shipping__c);
		system.assertEquals(req.clientBusinessName, c.Business_Name__c);
		//system.assertEquals(req.physicalDamageInd, (string.isnotblank(physicalDamage)&&physicalDamage.equalsignorecase('yes')?true:false));
		system.assertEquals(req.issue, c.Issue_System__c);
		system.assertEquals(req.defectsBehaviour, c.Defects_behaviour__c);
		system.assertEquals(req.descriptionAndTroubleshooting, c.Descrip_of_Defect_Troubleshooting_Perf__c);
		system.assertEquals(req.cosmeticDamageInd, c.Is_there_any_Cosmetic_Damage__c);
		system.assertEquals(req.descriptionOfCosmeticDamage, c.Comments_create_repair__c);
		system.assertEquals(req.estimatedRepairCost, c.Estimated_Repair_Cost_Up_To__c);
	}
	
	
	private static testMethod void testValidateResultToCase() {
		Case c = new Case(
			subject = 'test',
			repair_type__c = 'Device',
			ESN_MEID_IMEI__c = 'agdfkhadfhkgdaf',
			Mobile_number__c = '9991234567'
		);
		insert c;
		
		trac_WSFieldMapper.validateResultToCase(trac_svcPseudoMocks.validateSuccess(),c);
		
		// record should update without error
		update c;
		
		c = [
			SELECT repair_validated__c, Model_SKU__c, DOA_Expiry_Date__c, Warranty_Status__c, Warranty_Expiry_Date__c, Product__c,
						 Customer_First_Name_client__c, Customer_Last_Name_client__c, Business_Name__c, Address_account__c, City_account__c,
						 Province_account__c, Postal_Code_account__c
			FROM Case
			WHERE Id = :c.id
		];
		
		system.assertEquals(true,c.repair_validated__c);
		system.assertEquals('PPCDM8300', c.Model_SKU__c);
		system.assertEquals('In warranty', c.Warranty_Status__c);
		system.assertEquals('DAVID', c.Customer_First_Name_client__c);
		system.assertEquals('FOLLACK', c.Customer_Last_Name_client__c);
		system.assertEquals('DAVID FOLLACK', c.Business_Name__c);
		system.assertEquals('Standard Battery LG TM520', c.Product__c);
		system.assertEquals(Date.newInstance(2002,11,1), c.DOA_Expiry_Date__c);
		system.assertEquals('1234 Test Street Suite 500', c.Address_account__c);
	}
	
	
	private static testMethod void testValidateResultToCaseAccessory() {
		Case c = new Case(
			subject = 'test',
			repair_type__c = 'Accessory',
			accessory_sku__c = 'kjdghkdf',
			accessory_purchase_date__c = Date.today()
		);
		insert c;
		
		trac_WSFieldMapper.validateResultToCase(trac_svcPseudoMocks.validateSuccess(),c);
		
		// record should update without error
		update c;
		
		c = [
			SELECT repair_validated__c, Model_SKU__c, DOA_Expiry_Date__c, Warranty_Status__c, Warranty_Expiry_Date__c, Product__c,
						 Customer_First_Name_client__c, Customer_Last_Name_client__c, Business_Name__c, Address_account__c, City_account__c,
						 Province_account__c, Postal_Code_account__c
			FROM Case
			WHERE Id = :c.id
		];
		
		system.assertEquals(true,c.repair_validated__c);
		system.assertEquals('PPCDM8300', c.Model_SKU__c);
		system.assertEquals('In warranty', c.Warranty_Status__c);
		system.assertEquals('DAVID', c.Customer_First_Name_client__c);
		system.assertEquals('FOLLACK', c.Customer_Last_Name_client__c);
		system.assertEquals('DAVID FOLLACK', c.Business_Name__c);
		system.assertEquals('Standard Battery LG TM520', c.Product__c);
		system.assertEquals(null, c.DOA_Expiry_Date__c);
	}
	
	
	private static testMethod void testGetRepairResultToCase() {
		Case c = new Case(
			subject = 'test',
			repair_type__c = trac_WSFieldMapper.DEVICE_REPAIR_TYPE,
			mobile_number__c = '1234567890',
			ESN_MEID_IMEI__c = 'a6a6a6a6a6a6a6a',
			customer_email_contact__c = 'original@example.com'
		);
		insert c;
		
		trac_WSFieldMapper.getRepairResultToCase(trac_svcPseudoMocks.getRepairSuccess(c.id),c);
		
		system.assertEquals('x152524@telus.com', c.Customer_Email_contact__c);
		system.assertEquals(false, c.Repair_Case_Id_Mismatch__c);
		system.assertEquals(Date.newInstance(2012,09,26), c.DOA_Expiry_Date__c);
		
		// record should update without error
		update c;
	}
	
	
	private static testMethod void testGetRepairResultToCaseClosed() {
		Case c = new Case(
			subject = 'test',
			repair_type__c = trac_WSFieldMapper.DEVICE_REPAIR_TYPE,
			mobile_number__c = '1234567890',
			ESN_MEID_IMEI__c = 'a6a6a6a6a6a6a6a',
			customer_email_contact__c = 'original@example.com'
		);
		insert c;
		
		trac_WSFieldMapper.getRepairResultToCase(trac_svcPseudoMocks.getRepairSuccessClosed(c.id),c);
		
		system.assertEquals(false, c.repair_in_progress__c);
		update c;
	}
	
	
	private static testMethod void testGetRepairResultToCaseCancelled() {
		Case c = new Case(
			subject = 'test',
			repair_type__c = trac_WSFieldMapper.DEVICE_REPAIR_TYPE,
			mobile_number__c = '1234567890',
			ESN_MEID_IMEI__c = 'a6a6a6a6a6a6a6a',
			customer_email_contact__c = 'original@example.com'
		);
		insert c;
		
		trac_WSFieldMapper.getRepairResultToCase(trac_svcPseudoMocks.getRepairSuccessCancelled(c.id),c);
		
		system.assertEquals(false, c.repair_in_progress__c);
		system.assertEquals('Cancelled', c.status);
		update c;
	}
	
	
	private static testMethod void testGetRepairResultToCaseAccessory() {
		Case c = new Case(
			subject = 'test',
			repair_type__c = trac_WSFieldMapper.ACCESSORY_REPAIR_TYPE,
			accessory_sku__c = '12345',
			accessory_purchase_date__c = Date.today(),
			customer_email_contact__c = 'original@example.com'
		);
		insert c;
		
				
		rpm_EquipmentRepairInformationSvcRequest.getRepairResponse_element resp = trac_svcPseudoMocks.getRepairSuccess(c.id);
		resp.repair.subscriberNumber = null; // accessories cannot have mobile numbers
		
		trac_WSFieldMapper.getRepairResultToCase(resp,c);
		
		// accessory response contains blank email, so don't overwrite
		system.assertEquals('original@example.com', c.Customer_Email_contact__c);
		system.assertEquals(false, c.Repair_Case_Id_Mismatch__c);
		system.assertEquals(null, c.DOA_Expiry_Date__c);
		
		// record should update without error
		update c;
	}
	
	
	private static testMethod void testGetRepairResultToCaseIdInvalid() {
		Case c = new Case(subject = 'test', repair__c = trac_RepairTestUtils.REPAIR_ID_NO_MATCH);
		insert c;
		
		trac_WSFieldMapper.getRepairResultToCase(trac_svcPseudoMocks.getRepairSuccess('invalidid'),c);
		
		system.assertEquals(true,c.Repair_Case_Id_Mismatch__c);
	}
	
	
	private static testMethod void testGetRepairResultToCaseIdMismatch() {
		Case c = new Case(subject = 'test', repair__c = trac_RepairTestUtils.REPAIR_ID_NO_MATCH);
		insert c;
		
		Case c2 = new Case();
		insert c2;
		
		trac_WSFieldMapper.getRepairResultToCase(trac_svcPseudoMocks.getRepairSuccess(c2.id),c);
		
		system.assertEquals(true,c.Repair_Case_Id_Mismatch__c);
	}
	
	
	private static testMethod void testRepairStatusByIdToMap() {
		rpm_EquipmentRepairInformationSvcRequest.RepairStatusList rsl =
			new rpm_EquipmentRepairInformationSvcRequest.RepairStatusList();
		
		rpm_EquipmentRepairInformationSvcRequest.RepairStatusById status1 = 
			new rpm_EquipmentRepairInformationSvcRequest.RepairStatusById();
		status1.repairId = '1';
		status1.repairStatusId = 'ABD_REPAIR_PENDING';
		
		rpm_EquipmentRepairInformationSvcRequest.RepairStatusById status2 = 
			new rpm_EquipmentRepairInformationSvcRequest.RepairStatusById();
		status2.repairId = '2';
		status2.repairStatusId = 'ABD_REPAIR_CLOSED';
		
		rsl.repairStatusById = new List<rpm_EquipmentRepairInformationSvcRequest.RepairStatusById>{status1, status2};
		
		Map<String,String> statusMap = trac_WSFieldMapper.repairStatusByIdToMap(rsl);
		
		system.assertEquals('ABD_REPAIR_PENDING',statusMap.get('1'));
		system.assertEquals('ABD_REPAIR_CLOSED',statusMap.get('2'));
	}
	
	
	private static testMethod void testIdListToRepairIdList() {
		rpm_EquipmentRepairInformationSvcRequest.RepairIdList repIdList =
			trac_WSFieldMapper.idSetToRepairIdList(new Set<String>{'test1','test2'});
		
		system.assertEquals('test1',repIdList.RepairId[0]);
		system.assertEquals('test2',repIdList.RepairId[1]);
	}
	
}