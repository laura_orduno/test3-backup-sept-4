/**
	@author Vlocity
	@version Apr 21 2016
*/
@isTest
private class vlocity_addon_BA_CliMRCTableTest {
	
		@isTest static void test_buildDocumentSectionContent() {
			//setup stub: create a contract 
			
			TestDataHelper.testContractCreation();
            TestDataHelper.testContractLineItemListCreation_BA();
            vlocity_cmt__ContractLineItem__c v1 =  new vlocity_cmt__ContractLineItem__c(vlocity_cmt__Product2Id__c=TestDataHelper.testProductObj.id,TELUS_Printed_Product_Name__c = TestDataHelper.testProductObj.Name,TELUS_Printed_MRC__c = '$10.0',
                                                                                        vlocity_cmt__ContractId__c=TestDataHelper.testContractObj.id,TELUS_Printed_Indent_Level__c = 0,TELUS_Business_Tool__c = false,
                                                                                        TELUS_Printed_Document_Display_in_MRC__c = true,
                                                                                        name='Name123',vlocity_cmt__Quantity__c=12);
            insert v1;
            vlocity_cmt__ContractLineItem__c v2 =  new vlocity_cmt__ContractLineItem__c(vlocity_cmt__Product2Id__c=TestDataHelper.testProductObj.id,TELUS_Printed_Product_Name__c = TestDataHelper.testProductObj.Name,TELUS_Printed_MRC__c = '$10.0',
                                                                                        vlocity_cmt__ContractId__c=TestDataHelper.testContractObj.id,TELUS_Printed_Indent_Level__c = 1,TELUS_Business_Tool__c = false,
                                                                                        Parent_Contract_Line_Item__c = v1.id,
                                                                                        TELUS_Printed_Document_Display_in_MRC__c = true,
                                                                                        name='Name123',vlocity_cmt__Quantity__c=12);

			insert v2;
            vlocity_cmt__ContractLineItem__c v3 =  new vlocity_cmt__ContractLineItem__c(vlocity_cmt__Product2Id__c=TestDataHelper.testProductObj.id,TELUS_Printed_Product_Name__c = TestDataHelper.testProductObj.Name,TELUS_Printed_MRC__c = '$10.0',
                                                                                        vlocity_cmt__ContractId__c=TestDataHelper.testContractObj.id,TELUS_Printed_Indent_Level__c = 1,TELUS_Business_Tool__c = true,
                                                                                        
                                                                                        TELUS_Printed_Document_Display_in_MRC__c = true,
                                                                                        name='Name123',vlocity_cmt__Quantity__c=12);

			insert v3;
			Map<String,Object> inputMap = new Map<String,Object>();

			// send contractId in inputMap
			inputMap.put('contextObjId', TestDataHelper.testContractObj.id);

			Map<String,Object> outMap  = new Map<String,Object>();
			Map<String,Object> options = new Map<String,Object>();

			vlocity_cmt.VlocityOpenInterface intF = new  vlocity_addon_BA_CliMRCTable ();

			// this must be true
			system.assert(intF.invokeMethod('buildDocumentSectionContent',inputMap, outMap, options));

		}
}