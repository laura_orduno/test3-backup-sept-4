/**
 * An apex page controller that exposes the site login functionality
 */
global with sharing class VITILcareLoginCtlr extends VisualforcePageController{
    global transient String usernameError {get;set;}
    global transient String loginError {get;set;}
    global String username{get;set;}
    global String password{get;set;}
    public transient String[] errors {get; private set;}
    
    global VITILcareLoginCtlr () {
        usernameError = '';
        loginError = '';
    }
    
    global PageReference login(){
        String EMAIL_PATTERN = '^[_A-Za-z0-9-&\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$';
        usernameError = '';
        loginError = '';    
        if(!Pattern.matches(EMAIL_PATTERN, username)) {
        //if(!trac_PageUtils.isValidEmail(username)) {
            usernameError = label.mbrInvalidEmail;
            system.debug('usernameError: '+usernameError);
            return null;
        }
            
        User loginUser = null;
        try{
  			loginUser = [SELECT id, Customer_Portal_Name__c, LocaleSidKey, LanguageLocaleKey FROM USER WHERE username = :username];            
        }
        catch(QueryException e){
        	loginUser = null;
            System.debug('ERROR: Login user not found (' + username + ')');
        }
        catch(DMLException e){
        	loginUser = null;
            System.debug('ERROR: Login user not found (' + username + ')');
        }
        
        errors = new String[]{};
            
        if(loginUser != null){
            if(loginUser.Customer_Portal_Name__c != null && !loginUser.Customer_Portal_Name__c.containsIgnoreCase('vitilcare')){
                usernameError = Label.VITILcareWrongPortal;
                return null;
            }
        }
        else{
            errors.add('ERROR: Login user not found (' + username + ')');            
        }
       
        String startUrl = System.currentPageReference().getParameters().get('startURL');
        
        PageReference result =  Site.login(username, password, startUrl!=null? startUrl: '/VITILcareOverview');
                
        if (!ApexPages.getMessages().isEmpty()) {
            for (ApexPages.Message msg : ApexPages.getMessages()) {
                errors.add(msg.getDetail());
            }
        }
        
        if(errors.size() > 0){
            loginError = errors[0];
            system.debug('loginError: '+loginError);
            return null;
        }
        else{
            
            System.debug('Language (' + loginUser.LanguageLocaleKey + ')');
            try {
                loginUser.LanguageLocaleKey = 'en_US';
                
                System.debug('UPDATE: loginUser to ' + loginUser);
                
                update loginUser;               
            }
            catch(Exception e){
                // throw e;
                
                webservice_integration_error_log__c errorLog = new webservice_integration_error_log__c(
                    Webservice_Name__c = 'VITILcare', 
                    apex_class_and_method__c = 'VITILcareLoginCtlr',
                    error_code_and_message__c = e.getMessage()
                );
                insert errorLog;
            
                
                System.debug('ERROR: Update to Language not completed');
            }
/*****            
*/            

        	return result;
        }    
    }

}