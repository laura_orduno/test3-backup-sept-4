public class ContractDocumentDownloadCtrlExt{
  public Sobject currentObj {get;set;} 
  private  ApexPages.StandardController controller;
  public Id attachmentId {get;set;}
  public Boolean showLink {get;set;}

  public ContractDocumentDownloadCtrlExt(ApexPages.StandardController stdController){
    controller = stdController;
    currentObj = stdController.getRecord();
    vlocity_cmt__ContractVersion__c contractDoc = [Select Id From vlocity_cmt__ContractVersion__c where vlocity_cmt__ContractId__c =: currentObj.Id and vlocity_cmt__Status__c = 'Active' Limit 1 ];
    if(contractDoc!= null){
      Attachment att =[SELECT Name,Id, parentId, Body, ContentType FROM Attachment where parentId = :contractDoc.Id LIMIT 1];
      if(att != null){
        attachmentId = att.id;
        showLink = true;
        //String attchmentName  = att.Name;
       //attchmentName = attchmentName.replace('.docx', '.pdf');
        //Apexpages.currentPage().getHeaders().put('content-disposition', 'attachment;  filename=' +attchmentName);
      }
    }    
  }
}