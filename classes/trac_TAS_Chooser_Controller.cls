public with sharing class trac_TAS_Chooser_Controller {

  public Boolean hasAccess {get; set;}
  public Boolean isFrench {get; set;}
  
  public ID theOppId {get; set;}
  public ID theAccountId {get; set;}
  public ID longRecordType {get; set;}
  public ID shortRecordType {get; set;}
  
  public String theOppQuery {get; set;}
  public String theTASQuery {get; set;}
  public String debugMessage {get; set;}
  
  public List<SelectOption> tasOptions {get; set;}
  public List<SelectOption> tasRecordTypeOptions {get; set;}
  public String selectedTAS {get; set;}
  public String selectedTASRecordType {get; set;}
  
  public Opportunity theOpp {get; set;}
  public List<Target_Account_Selling__c> theTASes {get; set;}
  public TAS_Settings__c theSettings {get; set;}
  
  public class TASexception extends exception {}
  
  public trac_TAS_Chooser_Controller() {
    
    debugMessage = '';
    hasAccess = false;
    initLanguage();
    
    if (initCustomSettings()) initRecordTypes();
    
    if (initOpp()) {
        
        theAccountId = theOpp.AccountId; // Set the account id
        
        initCustomSettings();
         
        initTAS();
        initTASOptions();
    }
  }
  
  public void initLanguage() {
    isFrench = !(UserInfo.getLanguage() == 'en_US');
  }
  
  public Boolean initOpp() {
    
    theOppId = ApexPages.currentPage().getParameters().get('OPPID');
    
	  List<UserRecordAccess> listUserRecordAccess = [SELECT RecordId, HasReadAccess, HasEditAccess, HasDeleteAccess 
	                                                 FROM UserRecordAccess 
	                                                 WHERE UserId=:UserInfo.getUserId() 
	                                                  AND RecordId =: theOppId];
	  if (!listUserRecordAccess.isEmpty()) hasAccess = listUserRecordAccess[0].HasEditAccess;
    
    // Load the Opp
    if (hasAccess) {
	    theOppQuery = trac_Utilities.createObjectQuery('Opportunity');
	    List<Opportunity> tempOpps = database.query(theOppQuery + ' WHERE Id =:theOppId');
	    if (!tempOpps.isEmpty()) {
	      theOpp = tempOpps[0];
	      return true;
	    }
    }
    return false;
    
  }
  
  public void initTAS() {
    // Get existing tas for the opp's account, if any
//      theTASQuery = trac_Utilities.createObjectQuery('Target_Account_Selling__c');
      theTASQuery = 'SELECT id, name FROM Target_Account_Selling__c';
      theTASes = database.query(theTASQuery + ' WHERE Account__c = :theAccountId');
  }
  
    
  public void initTASOptions() {
      tasOptions = new List<SelectOption>();
      tasOptions.add(new SelectOption('Select', 'Select'));
      for (Target_Account_Selling__c tas : theTASes) {
        tasOptions.add(new SelectOption(tas.Id, tas.Name));
        debugMessage += ' Name: ' + tas.Name; 
      }
      selectedTAS = 'Select';
  }
  
  public Boolean initCustomSettings() {
    if (theSettings == null) theSettings = TAS_Settings__c.getOrgDefaults();
    return (theSettings != null);
  }
  
  public void initRecordTypes() {
    
    tasRecordTypeOptions = new List<SelectOption>();
    tasRecordTypeOptions.add(new SelectOption('Select', 'Select'));
    
    List<RecordType> temp = [SELECT Id, SObjectType, Name 
                             FROM RecordType 
                             WHERE SObjectType = 'Target_Account_Selling__c'];
    
    for (RecordType rt : temp) {
        if (rt.Name == theSettings.Long_Record_Type__c) {
        	longRecordType = rt.Id;
        	if (!isFrench) tasRecordTypeOptions.add(new SelectOption(rt.Id, rt.Name));
        	else tasRecordTypeOptions.add(new SelectOption(rt.Id, 'Le plan TAS INTÉGRAL')); 
        }
        if (rt.Name == theSettings.Short_Record_Type__c) {
        	shortRecordType = rt.Id;
          if (!isFrench) tasRecordTypeOptions.add(new SelectOption(rt.Id, rt.Name));
          else tasRecordTypeOptions.add(new SelectOption(rt.Id, 'Le plan TAS abrégé'));
        }
        selectedTASRecordType = 'Select';
    }
  }
  
  public PageReference doChooseTAS() {
    
    try {
        PageReference newPage = Page.trac_TAS;
        newPage.getParameters().put('ID', selectedTAS);
        newPage.getParameters().put('OPPID', theOpp.Id);
        
        if (selectedTAS != 'Select') {
            for (Target_Account_Selling__c tas : theTASes) {
                if (tas.Id == selectedTAS) {
                    theOpp.Target_Account_Selling_Plan__c = tas.Id;
                    update theOpp;
                }
            }
            
            newPage.setRedirect(true);
            return newPage;
        }
    } catch (Exception e) {
        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
        return null;
    }
    return null;
  }
  
  public PageReference doCreateTAS() {
    try {
      if (selectedTASRecordType != 'Select') {
              PageReference newPage = Page.trac_TAS;
              newPage.getParameters().put('OPPID', theOpp.Id);
              newPage.getParameters().put('RECORDTYPEID', 
                          (selectedTASRecordType == longRecordType)? longRecordType : shortRecordType);
        
            newPage.setRedirect(true);
            return newPage;
      } else {
      	if (!isFrench) throw new TASException('Please select a TAS plan type');
      	else throw new TASException('S\'il vous plaît choisir un type de plan TAS.');
      }
    } catch (Exception e) {
      ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
      return null;
    }
    return null;
    
  }
  
}