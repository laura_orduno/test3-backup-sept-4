/*
 * Additional Test for StringUtils
 * Coverage of StringUtils by its companion test class, StringUtilsTest, is quite poor. This class adds more tests to improve coverage.
 *
 * @Author TractionOnDemand - GA - 2013
 */

@isTest
public class StringUtilsTest_trac {
	
	private static testMethod void testIndexOfAny() {
		Integer i = StringUtils.indexOfAny('test', 'ze');
		
		system.assertEquals(1,i);
	}
	
	private static testMethod void testIndexOfAnyNull() {
		String nullString;
		Integer i = StringUtils.indexOfAny(null, nullString);
		
		system.assertEquals(-1,i);
	}
	
	private static testMethod void testIndexOfAnyList() {
		Integer i = StringUtils.indexOfAny('test', new String[]{'z', 'e'});
		
		system.assertEquals(1,i);
	}
	
	private static testMethod void testIndexOfAnyListNullInList() {
		Integer i = StringUtils.indexOfAny('test', new String[]{null, 'e'});
		
		system.assertEquals(1,i);
	}
	
	private static testMethod void testIndexOfAnyListNull() {
		List<String> nullList;
		Integer i = StringUtils.indexOfAny(null, nullList);
		
		system.assertEquals(-1, i);
	}
	
	private static testMethod void testLastIndexofWithStartPos() {
		Integer i = StringUtils.lastIndexOf('test', 't', 2);
		
		system.assertEquals(0, i);
	}
	
	private static testMethod void testLastIndexOfNull() {
		Integer i = StringUtils.lastIndexOf(null, null);
		
		system.assertEquals(-1,i);
	}
	
	private static testMethod void testLastIndexOfAnyList() {
		Integer i = StringUtils.lastIndexOfAny('test', new String[]{'z', 't'});
		
		system.assertEquals(3,i);
	}
	
	private static testMethod void testLastIndexOfAnyListNullInList() {
		Integer i = StringUtils.lastIndexOfAny('test', new String[]{null, 't'});
		
		system.assertEquals(3,i);
	}
	
	private static testMethod void testLastIndexOfAnyListNull() {
		List<String> nullList;
		Integer i = StringUtils.lastIndexOfAny(null, nullList);
		
		system.assertEquals(-1,i);
	}
	
	private static testMethod void testLeft() {
		String result = StringUtils.left('Test', 3);
		
		system.assertEquals('Tes', result);
	}
	
	private static testMethod void testLeftBeyondStringLength() {
		String result = StringUtils.left('Test', 2000);
		
		system.assertEquals('Test', result);
	}
	
	private static testMethod void testLeftNull() {
		String result = StringUtils.left(null, 500);
		
		system.assertEquals(null, result);
	}
	
	private static testMethod void testLeftNegativeLength() {
		String result = StringUtils.left('Test', -1);
		
		system.assertEquals('', result);
	}
	
	private static testMethod void testMid() {
		String result = StringUtils.mid('Test', 1, 2);
		
		system.assertEquals('es', result);
	}
	
	private static testMethod void testMidStartPosOnly() {
		String result = StringUtils.mid('Test', 1, 5);
		
		system.assertEquals('est', result);
	}
	
	private static testMethod void testMidNull() {
		String result = StringUtils.mid(null, 1, 2);
		
		system.assertEquals(null, result);
	}
	
	private static testMethod void testMidPosGreaterThanStringLength() {
		String result = StringUtils.mid('Test', 5000, 2);
		
		system.assertEquals('', result);
	}
	
	private static testMethod void testMidPosNegative() {
		String result = StringUtils.mid('Test', -1, 2);
		
		system.assertEquals('Te', result);
	}
	
	private static testMethod void testOverlay() {
		String result = StringUtils.overlay('LongString', 'Test', 1, 5);
		
		system.assertEquals('LTesttring', result);
	}
	
	private static testMethod void testOverlaySourceNull() {
		String result = StringUtils.overlay(null, 'Test', 1, 5);
		
		system.assertEquals(null, result);
	}
	
	private static testMethod void testOverlayNull() {
		String result = StringUtils.overlay('LongString', null, 1, 5);
		
		system.assertEquals('Ltring', result);
	}
	
	private static testMethod void testOverlayNegativePositions() {
		String result = StringUtils.overlay('LongString', 'Test', -1, 4);
		
		system.assertEquals('TestString', result);
		
		result = StringUtils.overlay('LongString', 'Test', 1, -1);
		
		system.assertEquals('TestongString', result);
	}
	
	private static testMethod void testStartIndexGreaterThanMid() {
		String result = StringUtils.overlay('LongString', 'Test', 500, 4);
		
		system.assertEquals('LongTest', result);
	}
	
	private static testMethod void testEndIndexGreaterThanMid() {
		String result = StringUtils.overlay('LongString', 'Test', 1, 500);
		
		system.assertEquals('LTest', result);
	}
	
	private static testMethod void testRemove() {
		String result = StringUtils.remove('TestString','Test');
		
		system.assertEquals('String', result);
	}
	
	private static testMethod void testRemoveNull() {
		String result = StringUtils.remove(null, null);
		
		system.assertEquals(null, result);
	}
	
	private static testMethod void testRemoveStartEmptyString() {
		String result = StringUtils.removeStart('Test', '');
		
		system.assertEquals('Test', result);
	}
	
	private static testMethod void testRemoveStartRemoveStringLongerThanExisting() {
		String result = StringUtils.removeStart('Test', 'Test');
		
		system.assertEquals('', result);
	}
	
	private static testMethod void testRemoveStart() {
		String result = StringUtils.removeStart('TestingString', 'Test');
		
		system.assertEquals('ingString', result);
	}
	
	private static testMethod void testRemoveStartNoMatch() {
		String result = StringUtils.removeStart('Test', 'NoMatch');
		
		system.assertEquals('Test', result);
	}
	
	private static testMethod void testRemoveStartIgnoreCase() {
		String result = StringUtils.removeStartIgnoreCase('TestString', 'TEST');
		
		system.assertEquals('String', result);
	}
	
	private static testMethod void testRemoveStartIgnoreCaseNoMatch() {
		String result = StringUtils.removeStartIgnoreCase('TestString', 'NOMATCH');
		
		system.assertEquals('TestString', result);
	}
	
	private static testMethod void testRemoveStartIgnoreCaseEmptyString() {
		String result = StringUtils.removeStartIgnoreCase('TestString', '');
		
		system.assertEquals('TestString', result);
	}
	
	private static testMethod void testRemoveStartIgnoreCaseEmpty() {
		String result = StringUtils.removeStartIgnoreCase('', '');
		
		system.assertEquals('', result);
	}
	
	private static testMethod void testRemoveEnd() {
		String result = StringUtils.removeEnd('TestString', 'String');
		
		system.assertEquals('Test', result);
	}
	
	private static testMethod void testRemoveEndNoMatch() {
		String result = StringUtils.removeEnd('TestString', 'None');
		
		system.assertEquals('TestString', result);
	}
	
	private static testMethod void testRemoveEndEmptyString() {
		String result = StringUtils.removeEnd('', 'None');
		
		system.assertEquals('', result);
	}
	
	private static testMethod void testRemoveEndSameSizeStrings() {
		String result = StringUtils.removeEnd('TestString', 'TestString');
		
		system.assertEquals('', result);
	}
	
	private static testMethod void testRemoveEndIgnoreCase() {
		String result = StringUtils.removeEndIgnoreCase('TestString', 'STRING');
		
		system.assertEquals('Test', result);
	}
	
	private static testMethod void testRemoveEndIgnoreCaseNoMatch() {
		String result = StringUtils.removeEndIgnoreCase('TestString', 'None');
		
		system.assertEquals('TestString', result);
	}
	
	private static testMethod void testRemoveEndIgnoreCaseEmptyString() {
		String result = StringUtils.removeEndIgnoreCase('', 'None');
		
		system.assertEquals('', result);
	}
	
	private static testMethod void testRemoveEndIgnoreCaseSameSizeStrings() {
		String result = StringUtils.removeEndIgnoreCase('TestString', 'TestString');
		
		system.assertEquals('', result);
	}
	
	private static testMethod void testRepeat() {
		String result = StringUtils.repeat('Test', 3);
		
		system.assertEquals('TestTestTest', result);
	}
	
	private static testMethod void testRepeatSingle() {
		String result = StringUtils.repeat('Test', 1);
		
		system.assertEquals('Test', result);
	}
	
	private static testMethod void testRepeatNull() {
		String result = StringUtils.repeat(null, 3);
		
		system.assertEquals(null, result);
	}
	
	private static testMethod void testRepeatZero() {
		String result = StringUtils.repeat('Test', 0);
		
		system.assertEquals('', result);
	}
	
	private static testMethod void testRepeatSingleChar() {
		String result = StringUtils.repeat('t', 5);
		
		system.assertEquals('ttttt', result);
	}
	
	private static testMethod void testReplace() {
		String result = StringUtils.replace('TestString','Test', 'New');
		
		system.assertEquals('NewString', result);
	}
	
	private static testMethod void testReplaceNullReplacement() {
		String result = StringUtils.replace('TestString','Test', null);
		
		system.assertEquals('TestString', result);
	}
	
	private static testMethod void testReplaceNoMatch() {
		String result = StringUtils.replace('TestString','nomatch', 'New');
		
		system.assertEquals('TestString', result);
	}
	
	private static testMethod void testReplaceOnce() {
		String result = StringUtils.replaceOnce('TestStringTest','Test', 'New');
		
		system.assertEquals('NewStringTest', result);
	}
	
	private static testMethod void testReplaceEach() {
		String result = StringUtils.replaceEach('ThisIsATestString', new String[]{'This', 'Test'}, new String[]{'What', 'New'});
		
		system.assertEquals('WhatIsANewString', result);
	}
	
	private static testMethod void testReplaceEachEmptyLists() {
		String result = StringUtils.replaceEach('ThisIsATestString', new String[]{}, new String[]{});
		
		system.assertEquals('ThisIsATestString', result);
	}
	
	private static testMethod void testReplaceEachMismatchedListSize() {
		Boolean caughtException = false;
		
		try {
			String result = StringUtils.replaceEach('ThisIsATestString', new String[]{'This', 'Test'}, new String[]{'What'});
		} catch (Exception e) {
			caughtException = true;
			system.assertEquals('Search and Replace array lengths don\'t match: 2 vs 1', e.getMessage());
		}
		
		system.assert(caughtException);
	}
	
	private static testMethod void testReplaceEachInvalidTTL() {
		Boolean caughtException = false;
			
		try {
		String result =
			StringUtils.replaceEach('ThisIsATestStringThisString', new String[]{'This', 'Test'}, new String[]{'What', 'New'}, true, -1);
		} catch (Exception e) {
			caughtException = true;
			system.assertEquals('TimeToLive of -1 is less than 0: ThisIsATestStringThisString', e.getMessage());
		}
		
		system.assert(caughtException);
	}
	
	private static testMethod void testReplaceEachRepeatedly() {
		String result =
			StringUtils.replaceEachRepeatedly('ThisIsATestStringThisString', new String[]{'This', 'Test'}, new String[]{'What', 'New'}, true);
		
		system.assertEquals('WhatIsANewStringWhatString', result);
	}
	
	private static testMethod void testReverse() {
		String result = StringUtils.reverse('Test');
		
		system.assertEquals('tseT', result);
	}

	private static testMethod void testReverseNull() {
		String result = StringUtils.reverse(null);
		
		system.assertEquals(null, result);
	}
	
	private static testMethod void testRight() {
		String result = StringUtils.right('Test', 3);
		
		system.assertEquals('est', result);
	}
	
	private static testMethod void testRightBeyondStringLength() {
		String result = StringUtils.right('Test', 2000);
		
		system.assertEquals('Test', result);
	}
	
	private static testMethod void testRightNull() {
		String result = StringUtils.right(null, 3);
		
		system.assertEquals(null, result);
	}
	
	private static testMethod void testRightNegativeLength() {
		String result = StringUtils.right('Test', -1);
		
		system.assertEquals('', result);
	}
	
	private static testMethod void testRegionMatches() {
		Boolean result = Stringutils.regionMatches('ThisTestString', 4, 'TestStringTwo', 0, 10);
		
		system.assert(result);
	}
	
	private static testMethod void testRegionMatchesIgnoreCase() {
		Boolean result = Stringutils.regionMatchesIgnoreCase('ThisTestString', 4, 'TestSTRINGTwo', 0, 10);
		
		system.assert(result);
	}
	
	private static testMethod void testRegionMatchesFalse() {
		Boolean result = Stringutils.regionMatches('ThisTestString', 4, 'TestStrungTwo', 0, 10);
		
		system.assertEquals(false,result);
	}
	
	private static testMethod void testSplitByCharacterType() {
		String[] results = Stringutils.splitByCharacterType('Aa1!');
		
		system.assertEquals(4,results.size());
	}
	
	private static testMethod void testSplitByCharacterTypeNull() {
		String[] results = Stringutils.splitByCharacterType(null);
		
		system.assertEquals(null,results);
	}
	
	private static testMethod void testSplitByCharacterTypeEmpty() {
		String[] results = Stringutils.splitByCharacterType('');
		
		system.assertEquals(0,results.size());
	}
	
	private static testMethod void testSplitByCharacterTypeCamelCase() {
		String[] results = Stringutils.splitByCharacterTypeCamelCase('MyTestString');
		
		system.assertEquals(3,results.size());
	}
	
	private static testMethod void testSplitByCharacterTypeCamelCaseSequential() {
		String[] results = Stringutils.splitByCharacterTypeCamelCase('ZMyTestString');
		
		system.assertEquals(4, results.size());
	}
	
	private static testMethod void testStrip() {
		String result = StringUtils.strip('  Test  ');
		
		system.assertEquals('Test', result);
	}
	
	private static testMethod void testStripNull() {
		String result = StringUtils.strip(null);
		
		system.assertEquals(null, result);
	}
	
	private static testMethod void testStripEmpty() {
		String result = StringUtils.strip('');
		
		system.assertEquals('', result);
	}
	
	private static testMethod void testStripToNull() {
		String result = StringUtils.stripToNull('  ');
		
		system.assertEquals(null, result);
	}
	
	private static testMethod void testStripToNullNullInput() {
		String result = StringUtils.stripToNull(null);
		
		system.assertEquals(null, result);
	}
	
	private static testMethod void testStripToEmpty() {
		String result = StringUtils.stripToEmpty(null);
		
		system.assertEquals('', result);
	}
	
	private static testMethod void testStripStart() {
		String result = StringUtils.stripStart('  Test  ', ' ');
		
		system.assertEquals('Test  ', result);
	}
	
	private static testMethod void testStripStartEmptyCharsToStrip() {
		String result = StringUtils.stripStart('  Test  ', '');
		
		system.assertEquals('  Test  ', result);
	}
	
	private static testMethod void testStripStartNull() {
		String result = StringUtils.stripStart(null, ' ');
		
		system.assertEquals(null, result);
	}
	
	private static testMethod void testStripEnd() {
		String result = StringUtils.stripEnd('  Test  ', ' ');
		
		system.assertEquals('  Test', result);
	}
	
	private static testMethod void testStripEndEmptyCharsToStrip() {
		String result = StringUtils.stripEnd('  Test  ', '');
		
		system.assertEquals('  Test  ', result);
	}
	
	private static testMethod void testStripEndNull() {
		String result = StringUtils.stripEnd(null, ' ');
		
		system.assertEquals(null, result);
	}
	
	private static testMethod void testStripAll() {
		String[] results = StringUtils.stripAll(new String[]{' Test ', ' Test2 '});
		
		system.assertEquals(2, results.size());
		system.assertEquals('Test', results[0]);
		system.assertEquals('Test2', results[1]);
	}
	
	private static testMethod void testStripAllNull() {
		String[] results = StringUtils.stripAll(null);
		
		system.assertEquals(null, results);
	}
	
	private static testMethod void testSubstringNegativeEndPos() {
		String result = StringUtils.substring('Test', 2, -1);
		
		system.assertEquals('s', result);
	}
	
	private static testMethod void testSubstringNull() {
		String result = StringUtils.substring(null, 2, -1);
		
		system.assertEquals(null, result);
	}
	
	private static testMethod void testSubstringBefore() {
		String result = StringUtils.substringBefore('ThisIsATest', 'A');
		
		system.assertEquals('ThisIs', result);
	}
	
	private static testMethod void testSubstringBeforeNoMatch() {
		String result = StringUtils.substringBefore('ThisIsATest', 'X');
		
		system.assertEquals('ThisIsATest', result);
	}
	
	private static testMethod void testSubstringBeforeNull() {
		String result = StringUtils.substringBefore(null, 'A');
		
		system.assertEquals(null, result);
	}
	
	private static testMethod void testSubstringBeforeEmptySeparator() {
		String result = StringUtils.substringBefore('ThisIsATest', '');
		
		system.assertEquals('', result);
	}
	
	private static testMethod void testSubstringAfter() {
		String result = StringUtils.substringAfter('ThisIsATest', 'A');
		
		system.assertEquals('Test', result);
	}
	
	private static testMethod void testSubstringAfterNoMatch() {
		String result = StringUtils.substringAfter('ThisIsATest', 'X');
		
		system.assertEquals('', result);
	}
	
	private static testMethod void testSubstringAfterNull() {
		String result = StringUtils.substringAfter(null, 'A');
		
		system.assertEquals(null, result);
	}
	
	private static testMethod void testSubstringAfterEmptySeparator() {
		String result = StringUtils.substringAfter('ThisIsATest', '');
		
		system.assertEquals('ThisIsATest', result);
	}
	
	private static testMethod void testSubstringAfterNullSeparator() {
		String result = StringUtils.substringAfter('ThisIsATest', null);
		
		system.assertEquals('', result);
	}
	
	private static testMethod void testSubstringBeforeLast() {
		String result = StringUtils.substringBeforeLast('ThisIsATestA', 'A');
		
		system.assertEquals('ThisIsATest', result);
	}
	
	private static testMethod void testSubstringBeforeLastNoMatch() {
		String result = StringUtils.substringBeforeLast('ThisIsATest', 'X');
		
		system.assertEquals('ThisIsATest', result);
	}
	
	private static testMethod void testSubstringBeforeLastNull() {
		String result = StringUtils.substringBeforeLast(null, 'A');
		
		system.assertEquals(null, result);
	}
	
	private static testMethod void testSubstringBeforeLastEmptySeparator() {
		String result = StringUtils.substringBeforeLast('ThisIsATest', '');
		
		system.assertEquals('ThisIsATest', result);
	}
	
	private static testMethod void testSubstringAfterLast() {
		String result = StringUtils.substringAfterLast('ThisIsATestATest', 'A');
		
		system.assertEquals('Test', result);
	}
	
	private static testMethod void testSubstringAfterLastNoMatch() {
		String result = StringUtils.substringAfterLast('ThisIsATest', 'X');
		
		system.assertEquals('', result);
	}
	
	private static testMethod void testSubstringAfterLastNull() {
		String result = StringUtils.substringAfterLast(null, 'A');
		
		system.assertEquals(null, result);
	}
	
	private static testMethod void testSubstringAfterLastEmptySeparator() {
		String result = StringUtils.substringAfterLast('ThisIsATest', '');
		
		system.assertEquals('', result);
	}
	
	private static testMethod void testSubstringBetween() {
		String result = StringUtils.substringBetween('ThisIsATest', 'This', 'Test');
		
		system.assertEquals('IsA', result);
	}
	
	private static testMethod void testSubstringBetweenNull() {
		String result = StringUtils.substringBetween(null, 'This', 'Test');
		
		system.assertEquals(null, result);
	}
	
	private static testMethod void testSubstringBetweenStartNoMatch() {
		String result = StringUtils.substringBetween('ThisIsATest', 'X', 'Test');
		
		system.assertEquals(null, result);
	}
	
	private static testMethod void testSubstringBetweenSingleSpecifier() {
		String result = StringUtils.substringBetween('AxTestStringxTest', 'x');
		
		system.assertEquals('TestString', result);
	}
	
	private static testMethod void testSubstringsBetween() {
		String[] results = StringUtils.substringsBetween('This$IsATest^String$More^Content', '$', '^');
		
		system.assertEquals(2, results.size());
		system.assertEquals('IsATest', results[0]);
		system.assertEquals('More', results[1]);
	}
	
	private static testMethod void testSubstringsBetweenNoMatch() {
		String[] results = StringUtils.substringsBetween('ThisIsATestStringMoreContent', '$', '^');
		
		system.assertEquals(null, results);
	}
	
	private static testMethod void testSubstringsBetweenEmpty() {
		String[] results = StringUtils.substringsBetween('', '$', '^');
		
		system.assertEquals(0, results.size());
	}
	
	private static testMethod void testSubstringsBetweenNull() {
		String[] results = StringUtils.substringsBetween(null, '$', '^');
		
		system.assertEquals(null, results);
	}
	
	private static testMethod void testTrimAll() {
		String[] results = StringUtils.trimAll(new String[]{ ' test1 ', ' test2 ' });
		
		system.assertEquals(2, results.size());
		system.assertEquals('test1', results[0]);
		system.assertEquals('test2', results[1]);
	}
	
	private static testMethod void testTrimAllEmptyList() {
		String[] results = StringUtils.trimAll(new List<String>());
		
		system.assertEquals(0, results.size());
	}
	
	private static testMethod void testTrimAllNull() {
		List<String> nullList;
		String[] results = StringUtils.trimAll(nullList);
		
		system.assertEquals(null, results);
	}
	
	private static testMethod void testTrimAllSet() {
		Set<String> results = StringUtils.trimAll(new Set<String>{ ' test1 ', ' test2 ' });
		
		system.assertEquals(2, results.size());
		system.assert(results.contains('test1'));
		system.assert(results.contains('test2'));
	}
	
	private static testMethod void testTrimAllSetEmptyList() {
		Set<String> results = StringUtils.trimAll(new Set<String>());
		
		system.assertEquals(0, results.size());
	}
	
	private static testMethod void testTrimAllSetNull() {
		Set<String> nullSet;
		Set<String> results = StringUtils.trimAll(nullSet);
		
		system.assertEquals(null, results);
	}
	
	private static testMethod void testTrimToNull() {
		String result = StringUtils.trimToNull('  ');
		
		system.assertEquals(null, result);
	}
	
	private static testMethod void testTrimToEmpty() {
		String result = StringUtils.trimToEmpty(null);
		
		system.assertEquals('', result);
	}
	
	private static testMethod void testEnsureStringStartsEndsWithChar() {
		String result = StringUtils.ensureStringStartsEndsWithChar('Test', '!');
		
		system.assertEquals('!Test!', result);
	}
	
	private static testMethod void testEnsureStringStartsEndsWithCharNull() {
		String result = StringUtils.ensureStringStartsEndsWithChar(null, '!');
		
		system.assertEquals(null, result);
	}
	
	private static testMethod void testToCharArray() {
		String[] results = StringUtils.toCharArray('Test');
		
		system.assertEquals(4, results.size());
	}
	
	private static testMethod void testToCharArrayNull() {
		String[] results = StringUtils.toCharArray(null);
		
		system.assertEquals(0, results.size());
	}
	
	private static testMethod void testTrimLower() {
		String result = StringUtils.trimLower(' Test ');
		
		system.assertEquals('test', result);
	}
	
	private static testMethod void testStripMarkup() {
		String result = StringUtils.stripMarkup('<tag>Test</tag>');
		
		system.assertEquals('Test', result);
	}
	
	private static testMethod void testStripMarkupNull() {
		String result = StringUtils.stripMarkup(null);
		
		system.assertEquals(null, result);
	}
	
	private static testMethod void testJoinArrayNull() {
		String result = StringUtils.joinArray(null);
		
		system.assertEquals(null, result);
	}
	
	private static testMethod void testSplitAndTrim() {
		String[] results = StringUtils.splitAndTrim('Test1, Test2', ',');
		
		system.assertEquals(2, results.size());
		system.assertEquals('Test1', results[0]);
		system.assertEquals('Test2', results[1]);
	}
	
	private static testMethod void testSplitAndTrimNoMatch() {
		String[] results = StringUtils.splitAndTrim('Test1, Test2', '!');
		
		system.assertEquals(1, results.size());
		system.assertEquals('Test1, Test2', results[0]);
	}
	
	private static testMethod void testStartsWithNull() {
		Boolean result = StringUtils.startsWith(null, 'x');
		
		system.assertEquals(false, result);
	}
	
	private static testMethod void testStartsWithCompareGreaterThanString() {
		Boolean result = StringUtils.startsWith('Test', 'xxxxxxx');
		
		system.assertEquals(false, result);
	}
	
	private static testMethod void testJoinNull() {
		String result = StringUtils.join(null, ',', null);
		
		system.assertEquals('', result);
	}
	
	private static testMethod void testJoinNullSeparator() {
		String result = StringUtils.join(new String[]{'test1','test2'}, null, null);
		
		system.assertEquals('test1test2', result);
	}
	
	private static testMethod void testJoinNullInList() {
		String result = StringUtils.join(new String[]{'test1',null,'test2'}, ',', null);
		
		system.assertEquals('test1,test2', result);
	}
	
}