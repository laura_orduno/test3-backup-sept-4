@isTest
public class CreditProfileCreateProxyTest {
    
    @isTest
    public static void invocablemethod() {  
        
        Test.startTest();     
        
        CreditUnitTestHelper.createTestuser();         
        User testUser = CreditUnitTestHelper.getTestuser(); 
        System.runAs(testUser){  
            
            Test.setCurrentPageReference(new PageReference('dyy test page'));         
            System.currentPageReference().getParameters().put('ids', null);            
            
            Credit_Profile__c[] cps = new List<Credit_Profile__c>();
            
            for(Integer i=0;i<1;i++){
                Credit_Profile__c cp = CreditUnitTestHelper.getCreditProfile();  
                cps.add(cp);
            }  
            
            String[] profileIds = new List<String>();
            for (Credit_Profile__c cp:cps ){
                profileIds.add(cp.id);
            }            
            
            
            CreditProfileCreateProxy.invocablemethod(profileIds);
            
        }
        
        Test.StopTest();    
    }
}