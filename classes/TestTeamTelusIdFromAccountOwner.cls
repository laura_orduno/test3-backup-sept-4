@isTest
private class TestTeamTelusIdFromAccountOwner{
    
     static testmethod void testCodeCoverage(){
         // created Account -Start
            Account acc = new Account(
                 Name= 'Test123'
            );
            insert acc;             
            Account actObj = [select OwnerId, Team_TELUS_ID__c from Account where Id = :acc.Id];
            System.debug('actObj ='+actObj );
            if(actObj != null){
                User u= [select Team_TELUS_ID__c from User where Id = :actObj.OwnerId];
                System.assertEquals(u.Team_TELUS_ID__c, actObj.Team_TELUS_ID__c);
            } 
     }
     
}