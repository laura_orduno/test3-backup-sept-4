public with sharing class TelusProductCharacterstics {

    public TELUSCHAR[] TELUSCHAR;
    public class TELUSCHAR {
        public String SegmentValue; //Yes
        public boolean Filterable;
        public String Code;
        public String Name;
        public String hashKey;    //01F
        public String AttributeDefinitionEnd;
        public attributeRunTimeInfo attributeRunTimeInfo;
        public String valuec;   //Selected
        public String valueinnumberc;
        public String valuedescriptionc;
        public String valuedatatypec;   //Checkbox
        public String uidisplaytypec;
        public String startstoryc;
        public String rulemessagec;
        public boolean removedflagc;
        public String removedatec;
        public String querylabelc;
        public String querycodec;
        public String objecttypec;  //Product2
        public String objectlinkc;  //<a href="/01te0000002vnNHAAY" target="_self">ATTRIBUTE-015</a>
        public String objectidc;    //01te0000002vnNHAAY
        public boolean isrequiredc;
        public boolean isquerydrivenc;
        public boolean isconfigurablec;
        public boolean isactivec;
        public boolean hasrulec;
        public String formatmaskc;
        public String endstoryc;
        public String displaysequencec; //1
        public String datac;
        public String categorynamec;    //Characteristics
        public String categorycodec;    //TELUSCHAR
        public String attributeuniquecodec;
        public String attributeidc;
        public boolean attributefilterablec;
        public String attributedisplaysequencec;    //8
        public String attributedisplaynamec;
        public boolean attributeconfigurablec;
        public String attributecategoryidc; //a6Be0000000CdPhEAK
        public String  adddatec;
        public String id;   //a6Ae00000004kSqEAI
       // public AttributeDefinitionStart $$AttributeDefinitionStart$$;
    }

    public class attributeRunTimeInfo {
        public String value;
        public defaultValue defaultValue;
        public String dataType; //Checkbox
        public selectedItem selectedItem;
        
      // public values values;
      //  public defaultValues[] defaultValue;
       
    }
   
   public class selectedItem {
        public Integer id;  //1
        public String displayText;  //Dynamic
        public String value;    //
    }
   /* public class values {
     public values[] values;
        public Integer id;  //1
        public String displayText;  //Dynamic
        public String value;    //
         public override String toString(){
          return this.value;
        }
        public values (String value){
          this.value=value;
        }
         public values (Object value){
          this.value=String.valueOf(value);
        }

    }*/
    public class defaultValue {
        defaultValue[] defaultValue;
        public Integer id;  //1
        public String displayText;  //Dynamic
        public String value;    //
        /*public override String toString(){
          return this.value;
        }
        public defaultValue(String value){
          this.value=value;
        }*/
    }

    public static TelusProductCharacterstics  parse(String json){
        return (TelusProductCharacterstics ) System.JSON.deserialize(json, TelusProductCharacterstics.class);
    }    

  

}