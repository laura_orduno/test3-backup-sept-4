@isTest(SeeAllData=false) 
public class OCOM_OutOrderFlowPACTest {

    static testMethod void testOutOrderFlow() {
        Account acctObj = new Account(name='OCOMTestAccount');
        acctObj.Account_Open_Since__c = date.today();
        acctObj.Billing_Account_ID__c = '1234567890';
        acctObj.recordtypeid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId();
        insert acctObj;
        
        Account acctSrvObj = new Account(name='OCOMTestSrvAccount');
        acctSrvObj.recordtypeid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        insert acctSrvObj;

        SMBCare_Address__c smbCareAddr = new SMBCare_Address__c();
        smbCareAddr.Suite_Number__c = '10';
        smbCareAddr.Building_Number__c = '3777';
        smbCareAddr.Street__c = 'KINGSWAY';
        smbCareAddr.City__c = 'BURNABY';
        smbCareAddr.Street_Direction_Code__c = 'N';
        smbCareAddr.Province__c = 'BC';
        smbCareAddr.Country__c = 'CAN';
        smbCareAddr.Postal_Code__c = 'V5H3Z7';
        smbCareAddr.Service_Account_Id__c = acctSrvObj.id;
        smbCareAddr.Account__c = acctObj.id;
        smbCareAddr.Status__c = 'Valid';
        insert smbCareAddr;
        
        PageReference pageRef = Page.OCOM_OutOrderFlowPAC;
        pageRef.getParameters().put('smbCareAddrId',string.valueof(smbCareAddr.id));
        Test.setCurrentPage(pageRef);

        ServiceAddressData origSAData = new ServiceAddressData();
        origSAData.suiteNumber = '10';
        origSAData.buildingNumber = '3777';
        origSAData.street = 'KINGSWAY';
        origSAData.streetDirectionCode = 'N';
        origSAData.city = 'BURNABY';
        origSAData.province = 'BC';
        origSAData.country = 'CAN';
        origSAData.postalCode = 'V5H3Z7';
        origSAData.searchString = '10 3777 KINGSWAY BURNABY BC V5H3Z7';
        origSAData.captureNewAddrStatus = 'Valid';

        OCOM_OutOrderFlowPACController ooCtrl = new OCOM_OutOrderFlowPACController();
        ooCtrl.init();

        ooCtrl.SAData = origSAData;
        ooCtrl.getAccountList();
        system.assert(ooCtrl.AccountList.size() > 0,'ooCtrl.AccountList.size()='+ooCtrl.AccountList.size());
        
        ooCtrl.smbCareAddrId = '';
        ooCtrl.LoadAccounts();
        ooCtrl.updateSAData();
        ooCtrl.smbCareAddrId = smbCareAddr.id;

        ooCtrl.errorMsg = 'none';
        ooCtrl.showErrorMsg = false;
        ooCtrl.ProcessChange();
        ooCtrl.getOrderPathValues();
        ooCtrl.getExistingServicesValues();
        ooCtrl.getcustomerLocationValues();
        ooCtrl.getTypeOfOrderValues();
        
        ooCtrl.smbCareAddrId = smbCareAddr.id;
        string pageURL = ooCtrl.RedirectToNextPage().getURL();
        ooCtrl.orderPath = 'Complex';
        pageURL = ooCtrl.RedirectToNextPage().getURL();
        ooCtrl.smbCareAddrId = '';
        pageURL = ooCtrl.RedirectToNextPage().getURL();

        //ooCtrl.SAData = origSAData;
        //ooCtrl.smbCareAddrId = smbCareAddr.id;
        //ooCtrl.SAData.isManualCapture = true;
        //ooCtrl.CreateServiceAccount();
    }
}