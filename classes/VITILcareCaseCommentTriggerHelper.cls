public class VITILcareCaseCommentTriggerHelper {
    @future    
    public static void processCaseComments(Set<Id> caseCommentIds){
System.debug('VITILcareCaseCommentTriggerHelper: processCaseComments: ' + caseCommentIds);
        
        try{
            // get all case comments that exists for id set	
            List<CaseComment> caseComments = [SELECT id, parentId, isPublished, CreatedById, CommentBody FROM CaseComment WHERE id IN :caseCommentIds];
                       
            // update
System.debug('CALLING updateLastCaseComment');
            updateLastCaseComment(caseComments);
            // email collaborators	
System.debug('CALLING sendEmailNotificationToCollaborators');
            sendEmailNotificationToCollaborators(caseComments);            
        }
        catch(QueryException e){
System.debug('VITILcareCaseCommentTriggerHelper: QueryException' + e);
            throw e;
        }
    }

    public static void updateLastCaseComment(List<CaseComment> caseComments){
        Map<Id, List<CaseComment>> childCaseComments = new Map<Id, List<CaseComment>>();        
        Set<Id> commentSubmitterUserIds = new Set<Id>();
        List<CaseComment> childComments = null;
	        
        // build child case comment map
        for(CaseComment cc : caseComments){        
            if(!childCaseComments.containsKey(cc.ParentId)){
                childComments = new List<CaseComment>();
            }
            else{
                childComments = childCaseComments.get(cc.ParentId);
            }
            
            if(!commentSubmitterUserIds.contains(cc.CreatedById)){
                commentSubmitterUserIds.add(cc.CreatedById);
            }
            
            if(cc.isPublished){        
                childComments.add(cc);
                childCaseComments.put(cc.ParentId, childComments);        
            }    
        }
        
        Map<Id, User> commentSubmitters = new Map<Id, User>([SELECT id, name, profile.name FROM User WHERE id in :commentSubmitterUserIds]);
        
        for(Id submitterKey : commentSubmitters.keyset()) {
            User commentSubmitter = commentSubmitters.get(submitterKey);
        }
               
        List<Case> caseUpdateList = new List<Case>();
        List<Case> cases = 
            [
                SELECT id, owner.id, owner.name, owner.profile.name, createdById, createdBy.name, createdBy.email, createdBy.profile.name, Last_Case_Comment__c, Last_Case_Comment_Submitter__c 
                FROM Case WHERE id IN :childCaseComments.keyset()
            ];
        
        for(Case c : cases)
        {
            String caseCreatorProfileName = (c.createdBy != null && c.createdBy.profile != null) ? c.createdBy.profile.name.toLowerCase() : '';
                    
            if(caseCreatorProfileName.contains('customer community user'))
            {
                for( CaseComment cc : childCaseComments.get(c.id) ) 
                {
                    if(!cc.CommentBody.contains(Label.MBRCancelReason) && !cc.CommentBody.contains(Label.MBREmailCommentAuthor))
                    {
                        // comment is not a cancellation, and comment did not come in via e-mail
                        User commentSubmitter = commentSubmitters.get(cc.CreatedById);
                    
                        c.Last_Case_Comment_Submitted_By__c = cc.CreatedById;
                        c.Last_Case_Comment_Submitter__c = commentSubmitter != null ? commentSubmitter.name : 'Unknown';
                        c.Last_Case_Comment__c = cc.CommentBody;
    //System.debug('Last_Case_Comment_Submitted_By__c: ' + c.Last_Case_Comment_Submitted_By__c);        
    //System.debug('Last_Case_Comment_Submitter__c: ' + c.Last_Case_Comment_Submitter__c);        
    //System.debug('Last_Case_Comment__c: ' + c.Last_Case_Comment_Submitter__c);        
                    } else if (cc.CommentBody.contains(Label.MBREmailCommentAuthor)) {
                        String commenterInfo = cc.CommentBody.substringBefore(Label.MBREmailCommentAuthor).trim();
                        if (commenterInfo.contains(' - ')) {
                            // check if submitter is the case creator
                            String submitterEmail = commenterInfo.substringBefore(' - ').toLowerCase();
                            if (submitterEmail == c.CreatedBy.Email.toLowerCase()) {
                                c.Last_Case_Comment_Submitted_By__c = c.CreatedById;
                            }
                            c.Last_Case_Comment_Submitter__c = commenterInfo.substringAfter(' - ');
                            c.Last_Case_Comment__c = cc.CommentBody.substringAfter(Label.MBREmailCommentAuthor);
                        }
                    }
                }
            }    
            
            caseUpdateList.add(c);
        }
        
        try
        {
            update caseUpdateList;
        }
        catch(DmlException e)
        {
System.debug('VITILcareCaseCommentTriggerHelper.updateLastCaseComment: DmlException' + e);
            throw e;
        }
    }
    
    public static void sendEmailNotificationToCollaborators(List<CaseComment> caseComments){
System.debug('sendEmailNotificationToCollaborators: ' + caseComments);

        // do we need to send email notifications to external collaborators?
        List<EmailTemplate> emailTemplates = [SELECT Id from EmailTemplate where name = 'VITILcare case comment collaborator HTML'];
        EmailTemplate tmpl = emailTemplates.size()>0 ? emailTemplates[0] : null;
        
System.debug('emailTemplates: ' + emailTemplates);
        
        VITILcare_Case_Collaborator_Settings__c collabSettings = VITILcare_Case_Collaborator_Settings__c.getOrgDefaults();
        System.debug('Collab collabSettings: ' + collabSettings);
    
        Id fromEmailId = null;
        List<OrgWideEmailAddress> fromEmails = [select Id from OrgWideEmailAddress where DisplayName = 'VITILcare'];
System.debug('fromEmails: ' + fromEmails);

        if (fromEmails.size() > 0) {
            fromEmailId = fromEmails[0].Id;
        }

        List<Id> caseIds = new List<Id>();
        for (CaseComment cc: caseComments) {
            caseIds.add(cc.ParentId);
        }
        System.debug('caseIds: ' + caseIds);

        Map<Id, Case> cases = new Map<Id, Case>([SELECT Id, NotifyCollaboratorString__c, Origin, Owner.Id, CreatedBy.Id, CreatedBy.Email, Last_Case_Comment_Submitted_By__c, Last_Case_Comment_Submitted_By__r.Id FROM Case WHERE Id IN :caseIds]);
        System.debug('cases: ' + cases);
    
        for (CaseComment cc : caseComments) {
            System.debug('cc.ParentId: ' + cc.ParentId);
            Case parentCase = cases.get(cc.ParentId);
            System.debug('parentCase: ' + parentCase);
            if (parentCase.Origin == 'VITILcare' && tmpl != null) {
                System.debug('ORIGIN: ' + parentCase.Origin + ', cc.CreatedById: ' + cc.CreatedById);
                
                System.debug('parentCase.Last_Case_Comment_Submitted_By__c: ' + parentCase.Last_Case_Comment_Submitted_By__c);
                System.debug('parentCase.Last_Case_Comment_Submitted_By__r.Id: ' + parentCase.Last_Case_Comment_Submitted_By__r.Id);
                System.debug('cc.CommentBody: ' + cc.CommentBody);
                System.debug('starts with: ' + parentCase.CreatedBy.Email);

                if ( (parentCase.Last_Case_Comment_Submitted_By__c != null && parentCase.Last_Case_Comment_Submitted_By__r.Id == cc.CreatedById) 
                    || (cc.CommentBody.startsWith(parentCase.CreatedBy.Email))
                ) {
System.debug('parentCase.NotifyCollaboratorString__c: ' + parentCase.NotifyCollaboratorString__c);
                    // case comment was not created by a collaborator; send out collaborator notifications!
                    if (parentCase.NotifyCollaboratorString__c != null && parentCase.NotifyCollaboratorString__c.length() > 0) {
                        List<String> collabEmails = parentCase.NotifyCollaboratorString__c.split(';', 0);
                        if (collabEmails.size() > 0) {
                            try {
System.debug('VITILcareUtils.sendTemplatedCaseEmails: ' + collabEmails + ', ' + fromEmailId + ', ' + tmpl.Id  + ', ' + parentCase.Id);
                                List<Messaging.SendEmailResult> mailResults = VITILcareUtils.sendTemplatedCaseEmails(collabEmails, fromEmailId, tmpl.Id, parentCase.Id);
                            } catch (Exception e) {
		                        System.debug('Collab caught VITILcareUtils.sendTemplatedCaseEmails() exception: ' + e);
                            }
                        }
                    }
                }
            }
        }      
    }
}