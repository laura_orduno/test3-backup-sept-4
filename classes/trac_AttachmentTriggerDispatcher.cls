public with sharing class trac_AttachmentTriggerDispatcher extends Dispatcher{

    public List<Attachment> newList {get; set;}
    public List<Attachment> oldList {get; set;}
    public Map<Id, Attachment> newMap {get; set;}
    public Map<Id, Attachment> oldMap {get; set;}
    public static boolean firstRun = true;


    public trac_AttachmentTriggerDispatcher () {
        super();
    }

    public override void init() {

        newList = (trigger.new != null) ? (List<Attachment>)trigger.new : new List<Attachment>();
        oldList = (trigger.old != null) ? (List<Attachment>)trigger.old : new List<Attachment>();
        newMap = (trigger.newMap != null) ? (Map<Id, Attachment>)trigger.newMap : new Map<Id, Attachment>();
        oldMap = (trigger.oldMap != null) ? (Map<Id, Attachment>)trigger.oldMap : new Map<Id, Attachment>();

    }

    public override void execute(){
    	//functions to be used. 
    	if( limitExecutionEvent(TriggerEvent.BEFOREINSERT)  || limitExecutionEvent(TriggerEvent.BEFOREUPDATE) ){
    		//getParentIdAtSource();
            parentAttachmentToCase();
            
    	}	
        if( limitExecutionEvent(TriggerEvent.AFTERINSERT)  || limitExecutionEvent(TriggerEvent.AFTERUPDATE) ){
            sendAttachment();
        }   
    }

    public void parentAttachmentToCase(){
        parentAttachmentToCase(newList);
    }

    public void sendAttachment(){
        sendAttachment(newList);
    }
	
    public static void sendAttachment(List<Attachment> newAttachments){
    	System.debug('trac_AttachmentTriggerDispatcher: Total Number of SOQL Queries allowed in this Apex code context: ' +  Limits.getLimitQueries());

        if(Limits.getLimitQueries() >= 100){
	    	System.debug('trac_AttachmentTriggerDispatcher: abort since SOQL limmit will be hit.');
            
            return;
        }
        
     	Set<Id> caseIds = new Set<Id>();
        List<PartnerNetworkRecordConnection> pnrcList = new List<PartnerNetworkRecordConnection>();
        Map<Id,Id> parentRecordIdMap = new Map<id,Id>();
    	List<Attachment> localAttachments = new List<Attachment>();       
        List<Attachment> attachmentWithVendor = new List<Attachment>();        
		Map<Id, List<Attachment>> caseToAttachment = new Map<Id, List<Attachment>>();
        for(Attachment att :newAttachments){    
            caseIds.add(att.ParentId);
            if(caseToAttachment.containsKey(att.ParentId))
            {
                caseToAttachment.get(att.ParentId).add(att);
            }
            else 
                caseToAttachment.put(att.ParentId, new List<Attachment>{att});
        } 

        if(caseIds.size() > 0){
            pnrcList =[ Select id,status,LocalRecordId,StartDate,PartnerRecordId
                        From PartnerNetworkRecordConnection
                        Where LocalRecordId in :caseIds
                        AND Status <> 'Inactive'
                        ];
        }

        for(PartnerNetworkRecordConnection pnrc:pnrcList){
            if(caseToAttachment.containsKey(pnrc.LocalRecordId)){
                List<Attachment> tmpAtt = caseToAttachment.get(pnrc.LocalRecordId);                
                attachmentWithVendor.addAll(tmpAtt);
            }
        }

    	for(Attachment att :attachmentWithVendor){        	
    		if (att.ConnectionReceivedId == null ){    			
    			localAttachments.add(att);
                //added parentrecordIdMap
				parentRecordIdMap.put(att.id,att.ParentId);
    		}
    	}

    	if(localAttachments.size() > 0 ){
    		trac_S2S_connection_Helper.sendToRingCentral(localAttachments, parentRecordIdMap, null, 'RingCentral',false,false,false);
    	}
    }
    
    /**
    *@author: Kelly Lu 
    *@description: When an Attachment comes in from the RingCentral Org parent the Attachment to the case associated with the ID in parent id at source.
    **/
    public static void parentAttachmentToCase(List<Attachment> attachments){
		Set<String> parentCaseIdSet = new Set<String>();
        List<Attachment> incomingAttachments = new List<Attachment>();        
        Map<Id,Case> parentCases = new Map<Id,Case>();
        Map<Id,String> attachmentToParentCaseId = new Map<Id,String>();

        for(Attachment att: attachments){
            if( (att.ConnectionReceivedId != null || Test.isRunningTest() ) && String.isNotBlank(att.ParentId)){                
				parentCaseIdSet.add(String.valueOf(att.ParentId));
                incomingAttachments.add(att);                
            }
        }
		
        
        //added for attachment parentId to connected to PartnerRecordId
        Map<Id,Id> partnerToLocalCaseMap = new Map<Id,Id>();
		for (PartnerNetworkRecordConnection connectionCase : [SELECT Id, Status, ConnectionId, LocalRecordId 
															 FROM PartnerNetworkRecordConnection
															 WHERE PartnerRecordId IN :parentCaseIdSet]){
			
			partnerToLocalCaseMap.put(connectionCase.PartnerRecordId,connectionCase.LocalRecordId);
		}
        
        for(Attachment att : incomingAttachments){
			if(att.ParentId != NULL && partnerToLocalCaseMap != NULL){
				if(partnerToLocalCaseMap.containsKey(att.ParentId) && partnerToLocalCaseMap.get(att.ParentId) != NULL){
					att.ParentId = partnerToLocalCaseMap.get(att.ParentId);
				}
			}			
		}
    }  
}