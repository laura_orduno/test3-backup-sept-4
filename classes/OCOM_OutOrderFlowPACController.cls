/*******************************************************************************
     * Project      : OCOM/US-1149
     * Helper       : OCOM_OutOrderFlowPACController
     * Test Class   : OCOM_OutOrderFlowPACController_Test
     * Description  : Apex class for OCOM_OutOrderFlowPAC VF page that allows Agents to capture Address outside OrderFlow
     * Author       : Francisco Hong
     * Date         : August-08-2016
     *
     * Modification Log:
     * ------------------------------------------------------------------------------
     * Ver      Date                Modified By           Description
     * ------------------------------------------------------------------------------
     * 1.0     
     
     *******************************************************************************/

public without sharing class OCOM_OutOrderFlowPACController {
    public ServiceAddressData SAData {get;set;}
    public String smbCareAddrId {get;set;}
    //public Account serviceAccount {get;set;}
    //public String existingServices {get;set;}
    public String typeOfOrder {get;set;}
    public String orderPath {get;set;}
    public String customerLocation {get;set;}
    public String errorMsg {get;set;}
    public Boolean showErrorMsg {get;set;}
    public List<AccountSummary> AccountList {get;set;}
    public String addressOrigin{get;set;}
    
    //Added by Arvind as part of display the additional fields receiving via ServiceAddressMgmtService V1.1 (US-OCOM-1101)
    
    public String dropFacilityCode { get; set; }
    public String dropTypeCode { get; set; }
    public String dropLength { get; set; }
    public String dropExceptionCode { get; set; }
    public String currentTransportTypeCode { get; set; }
    public String futureTransportTypeCode { get; set; }
    public String futurePlantReadyDate { get; set; }
    public String futureTransportRemarkTypeCode { get; set; }
    public String gponBuildTypeCode { get; set; }
    public String provisioningSystemCode { get; set; }
    public String ratingNpaNxx { get; set; }
    public String COID { get; set; }
    
    public class AccountSummary {
        public Id smbCareAddressId {get;set;}
        public String name {get;set;}
        public String billingName {get;set;}
        public String billingAccountId {get;set;}
        public String dateOpened {get;set;}
        public String serviceAccountId {get;set;}
        public string accountId {get;set;}
        
        public AccountSummary(SMBCare_Address__c aObj){
            if(aObj.id != null)
                smbCareAddressId = string.valueof(aObj.id);
            if(string.isNotBlank(aObj.Account__r.name))
                name = aObj.Account__r.name;
            if(string.isNotBlank(aObj.Account__r.Billing_Account_Name__c))
                billingName = aObj.Account__r.Billing_Account_Name__c;
            if(string.isNotBlank(aObj.Account__r.Billing_Account_ID__c))
                billingAccountId = aObj.Account__r.Billing_Account_ID__c;
            if(aObj.Account__r.Account_Open_Since__c != null)
                dateOpened = aObj.Account__r.Account_Open_Since__c.format();
            if(string.isNotBlank(aObj.Service_Account_Id__c))
                serviceAccountId = aObj.Service_Account_Id__c;
            if(aObj.Account__c != null)
                accountId = string.valueof(aObj.Account__c);
        }
    }
    
    public OCOM_OutOrderFlowPACController(){
        SAData = new ServiceAddressData();        
        AccountList = new List<AccountSummary>();
        customerLocation = 'Current';
        typeOfOrder = 'New Services';
        orderPath = 'Simple';
        addressOrigin = 'outsideflow';
        smbCareAddrId = System.currentPageReference().getParameters().get('smbCareAddrId');
    }

    public List<AccountSummary> getAccountList(){
        if(AccountList.size() == 0)
            LoadAccounts();
        return AccountList;
    }
    
    public void ProcessChange(){
        system.debug('ProcessChange:smbCareAddrId='+smbCareAddrId);
    }

    private SMBCare_Address__c findAccount(string smbCareAddressId){
        SMBCare_Address__c smbCareAddr;
        Id SMBCare_AddressId;
        boolean idValid = false;
        
        try{
            SMBCare_AddressId = id.valueOf(smbCareAddressId);
            idValid = true;
        }
        catch(exception e){
            system.debug('findAccount:Id INVALID: smbCareAddressId=' + smbCareAddressId);
        }
        
        if(smbCareAddressId == 'newaccount' || !idValid){
            return null; 
        }

        try{
            smbCareAddr = [Select Service_Account_Id__c,id, Account__c, FMS_Address_ID__c, Status__c,Postal_Code__c , Street_Number__c, city__c , Street__c, Street_Name__c, Street_Direction_Code__c , Suite_Number__c , Province__c, COID__c , Building_Number__c , Country__c, State__c, Civic_Number__c, Street_Type_Code__c , Location_Id__c , Address__c , Address_Type__c ,  LocalRoutingNumber__c , NPA_NXX__c , SwitchNumber__c , TerminalNumber__c, Service_Account_Id__r.RecordTypeId from SMBCare_address__c where Id=:SMBCare_AddressId limit 1];
        }
        catch(exception e){
            system.debug('findAccount:Query returned no rows for smbCareAddressId=' + smbCareAddressId);
        }

        if(smbCareAddr == null) {
            system.debug('findAccount:could not find smbCareAddressId='+smbCareAddressId);
        }

        return smbCareAddr;
    }
    
    public void LoadAccounts(){
        List<SMBCare_Address__c> smbCareAddrList;

        system.debug('LoadAccounts:AccountList.size()='+AccountList.size());
        system.debug('LoadAccounts:smbCareAddrId='+smbCareAddrId);

//        if(!SAData.captureNewAddrStatus.equalsIgnoreCase('Valid') || string.isBlank(SAData.searchString))
        if(string.isBlank(SAData.searchString)){
            system.debug('LoadAccounts:SAData.searchString is blank');
            return;
        }
        
        AccountList.clear();
        
        // Load all the accounts with the inputted address
        string queryStr = 'SELECT id, Service_Account_Id__c, Account__c, FMS_Address_ID__c, Status__c,Postal_Code__c, Street_Number__c, city__c , Street__c, Street_Name__c, Street_Direction_Code__c, Suite_Number__c, Province__c, COID__c , Building_Number__c , Country__c,  State__c, Civic_Number__c, Street_Type_Code__c, Location_Id__c, Address__c , Address_Type__c, LocalRoutingNumber__c, NPA_NXX__c , SwitchNumber__c, TerminalNumber__c, Service_Account_Id__r.RecordTypeId, Account__r.name, Account__r.Account_Open_Since__c, Account__r.Billing_Account_Name__c, Account__r.Billing_Account_ID__c FROM SMBCare_address__c ';
        string whereStr = GetWhereClause();
        if(string.isNotBlank(whereStr))
            queryStr += whereStr;
        else
            queryStr = '';
        
        system.debug('LoadAccounts:queryStr = ' + queryStr);

        try {
            if(string.isNotBlank(queryStr)){
                smbCareAddrList = database.query(queryStr + ' limit 20');
            }
        }
        catch(queryexception qe) {
            errorMsg=qe.getmessage();
        }

        if(smbCareAddrList == null) {
            system.debug('LoadAccounts:smbCareAddrList = null');
            return;
        }
        
        system.debug('LoadAccounts:smbCareAddrList.size() = ' + smbCareAddrList.size());

        // Format Account summary fields and store in list - only adding unique accounts
        set<id> uniqueAccts = new set<id>();
        for(SMBCare_Address__c aItm : smbCareAddrList){
            if(uniqueAccts.add(aItm.Account__c) == true){
                AccountList.add(new AccountSummary(aItm));
                system.debug('LoadAccounts:AccountName=' + aItm.Account__r.name + ': serviceAccountId=' + aItm.Service_Account_Id__c + ': accountId=' + aItm.Account__c);
            }
        }
        
        if(string.isBlank(smbCareAddrId) || smbCareAddrId == 'newaccount') {
            for(AccountSummary itm : AccountList) {
                if(string.isNotBlank(itm.accountId)){
                    smbCareAddrId = itm.smbCareAddressId;
                    break;
                }
            }
            if(string.isBlank(smbCareAddrId))
                smbCareAddrId = 'newaccount';

            system.debug('LoadAccounts:Set new value for AccountList.smbCareAddrId='+smbCareAddrId);
        }

        system.debug('LoadAccounts:After function:AccountList.size()='+AccountList.size());
        system.debug('LoadAccounts: AccountList('+AccountList+')');
        
        return;
    }
    
    // Thomas QC-7727/OCOM-1042 Start
    public PageReference updateSAData() {
        System.debug('OCOM_OutOrderFlowPACController::updateSAData() start');
        dropFacilityCode = SAData.dropFacilityCode;
        dropTypeCode = SAData.dropTypeCode;
        dropLength = SAData.dropLength;
        dropExceptionCode = SAData.dropExceptionCode;
        currentTransportTypeCode = SAData.currentTransportTypeCode;
        futureTransportTypeCode = SAData.futureTransportTypeCode;
        futurePlantReadyDate = SAData.futurePlantReadyDate;
        futureTransportRemarkTypeCode = SAData.futureTransportRemarkTypeCode;
        gponBuildTypeCode = SAData.gponBuildTypeCode;
        provisioningSystemCode = SAData.provisioningSystemCode;
        ratingNpaNxx = SAData.ratingNpaNxx;
        COID = SAData.COID;
        System.debug('OCOM_OutOrderFlowPACController::updateSAData() end');
        
        return null;
    }
    // Thomas QC-7727/OCOM-1042 End

    public List<SelectOption> getOrderPathValues() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Simple','Simple'));
        options.add(new SelectOption('Complex','Complex'));
        return options;
    }

    public List<SelectOption> getExistingServicesValues() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Yes','Yes'));
        options.add(new SelectOption('No','No'));
        return options;
    }   
    
    public List<SelectOption> getcustomerLocationValues() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Current','Current'));
        options.add(new SelectOption('New','New'));
        return options;
    } 
    
    public List<SelectOption> getTypeOfOrderValues() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('New Services','New Services'));
        options.add(new SelectOption('Move in address','Move in address'));
         return options;
    }

    public string GetWhereClause() {
        string queryStr = ' ';
        string andStr = ' ';
        
        if(string.isNotBlank(SAData.suiteNumber)){
            andStr = string.isNotBlank(queryStr) ? ' AND ' : ' ';
            queryStr += andStr + 'Suite_Number__c=\'' + SAData.suiteNumber + '\'';
        }
        if(string.isNotBlank(SAData.buildingNumber)){
            andStr = string.isNotBlank(queryStr) ? ' AND ' : ' ';
            queryStr += andStr + 'Building_Number__c=\'' + SAData.buildingNumber + '\'';
        }
        //if(string.isNotBlank(SAData.streetNumber)){
        //    andStr = string.isNotBlank(queryStr) ? ' AND ' : ' ';
        //    queryStr += andStr + 'Street_Number__c=\'' + SAData.streetNumber + '\'';
        //}
        if(string.isNotBlank(SAData.street)){
            andStr = string.isNotBlank(queryStr) ? ' AND ' : ' ';
            queryStr += andStr + 'Street__c=\'' + SAData.street + '\'';
        }
        //if(string.isNotBlank(SAData.streetName)){
        //    andStr = string.isNotBlank(queryStr) ? ' AND ' : ' ';
        //    queryStr += andStr + 'Street_Name__c=\'' + SAData.streetName + '\'';
        //}
        if(string.isNotBlank(SAData.streetDirectionCode)){
            andStr = string.isNotBlank(queryStr) ? ' AND ' : ' ';
            queryStr += andStr + 'Street_Direction_Code__c=\'' + SAData.streetDirectionCode + '\'';
        }
        if(string.isNotBlank(SAData.city)){
            andStr = string.isNotBlank(queryStr) ? ' AND ' : ' ';
            queryStr += andStr + 'city__c=\'' + SAData.city + '\'';
        }
        if(string.isNotBlank(SAData.province)){
            andStr = string.isNotBlank(queryStr) ? ' AND ' : ' ';
            queryStr += andStr + 'Province__c=\'' + SAData.province + '\'';
        }
        if(string.isNotBlank(SAData.country)){
            andStr = string.isNotBlank(queryStr) ? ' AND ' : ' ';
            queryStr += andStr + 'Country__c=\'' + SAData.country + '\'';
        }
        if(string.isNotBlank(SAData.postalCode)){
            andStr = string.isNotBlank(queryStr) ? ' AND ' : ' ';
            queryStr += andStr + 'Postal_Code__c=\'' + SAData.postalCode + '\'';
        }
        
        if(string.isNotBlank(queryStr))
            queryStr = ' where ' + queryStr;
        
        return queryStr;
    }
    
    public string GetAddressSearchString(SMBCare_Address__c smbCareAddr) {
        if(smbCareAddr == null)
            return null;
        
        string searchStr = ' ';

        if(smbCareAddr.Status__c.equalsIgnoreCase('valid')){
            if(string.isNotBlank(smbCareAddr.Suite_Number__c)){
                searchStr += smbCareAddr.Suite_Number__c + ' ';
            }
            if(string.isNotBlank(smbCareAddr.Building_Number__c)){
                searchStr += smbCareAddr.Building_Number__c + ' ';
            }
        }
        if(string.isNotBlank(smbCareAddr.Street__c)){
            searchStr += smbCareAddr.Street__c + ' ';
        }
        if(string.isNotBlank(smbCareAddr.city__c)){
            searchStr += smbCareAddr.city__c + ' ';
        }
        if(string.isNotBlank(smbCareAddr.Province__c)){
            searchStr += smbCareAddr.Province__c + ' ';
        }
        if(string.isNotBlank(smbCareAddr.Postal_Code__c)){
            searchStr += smbCareAddr.Postal_Code__c;
        }

        return searchStr;
    }
    
    public PageReference init(){
        SMBCare_Address__c smbCareAddr = findAccount(smbCareAddrId);
        string addressStr = GetAddressSearchString(smbCareAddr);
        SAData = new ServiceAddressData();
        AccountList = new List<AccountSummary>();

        if(string.isNotBlank(addressStr)) {
            SAData.onLoadSearchAddress = addressStr;
            SAData.searchString = addressStr;
        }  
        return null;
    }
    
    public PageReference RedirectToNextPage() {
        
        SMBCare_Address__c smbCareAddr = findAccount(smbCareAddrId);
        if(smbCareAddr == null){
            system.debug('RedirectToNextPage:could not find smbCareAddrId='+smbCareAddrId);
            smbCareAddrId = 'newaccount';
            orderPath = 'Simple';
        }
        
        if (orderPath == 'Complex')
            return new PageReference('/apex/smb_newQuoteFromAccount?aid='+smbCareAddr.Account__c+'&OrderPath='+orderPath);
        
        else if(smbCareAddrId == 'newaccount')
            return new PageReference('/apex/smb_NewAccount');
        
        else
            return new PageReference('/apex/OCOM_InOrderFlowContact?id=' +smbCareAddr.Service_Account_Id__c + '&OrderType=' +typeOfOrder+ 
                                     '&ParentAccId=' + smbCareAddr.Account__c + '&smbCareAddrId=' + smbCareAddrId + '&addressOrigin=outsideflow');
    }
}