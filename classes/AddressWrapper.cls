public with sharing class AddressWrapper {

	public SObject sObj {get; private set;}
	public String SObjectName {get; private set;}
	public String key {get; private set;}
	
	public AddressWrapper(SObject sObj, String SObjectName, String key) {
		this.sObj = sObj;
		this.sObjectName = sObjectName;
		this.key = key;
	}
	
	public String getStreet() {
		return getObjectValue('Street__c');	
	}
	
	public String getCity() {
		return getObjectValue('City__c');	
	}
	
	public String getState() {
		return getObjectValue('State_Province__c');	
	}
	
	public String getCountry() {
		return getObjectValue('Country__c');	
	}
	
	public String getPostalCode() {
		return getObjectValue('Postal_Code__c');	
	}
	
	public String getBuildingName() {
		return getObjectValue('Building_Name__c');	
	}

	public String getFloor() {
		return getObjectValue('Floor__c');	
	}

	public String getSuiteUnit() {
		return getObjectValue('Suite_Unit__c');	
	}

	public String getStreetType() {
		return getObjectValue('Street_Type__c');	
	}

	public String getStreetDirection() {
		return getObjectValue('Street_Direction__c');	
	}

	public String getObjectValue(String fieldName) {
		System.debug('sobj = ' + sobj);
		System.debug('fieldName = ' + fieldName);
		if (sobj == null) return '';
		Map<String, String> objMap = SearchAddressUtil.OBJECT_MAP.get(sObjectName);
		String mappedFieldName = (objMap == null || objMap.get(fieldName) == null) ? fieldName : objMap.get(fieldName);
		System.debug('mappedFieldName = ' + mappedFieldName);
		if ( Util.isBlank(mappedFieldName) ) {
			return '';
		}
		if (mappedFieldName.indexOf('.') != -1) {
			String name = mappedFieldName.substring(0, mappedFieldName.indexOf('.'));
			String fName = mappedFieldName.substring(mappedFieldName.indexOf('.') + 1);
			Sobject obj1 = sobj.getSObject(name);
			if (obj1 == null) {
				return '';
			}
			return (String) obj1.get(fname);
			//return '';
		} 
		return (String) sobj.get(mappedFieldName);
		
	}
	
	public String getObjectDisplay() {
		return SearchAddressUtil.OBJECT_DISPLAY_MAP.get(sObjectName);
	}
	
	public Id getRecordId() {
		return sobj.Id;
	}
	
	public String getRecordName() {
		return getObjectValue('Name');
	}
	
	public Boolean isEmpty() {
		return ( Util.isBlank(getStreet()) && Util.isBlank(getCity()) && Util.isBlank(getState()) && 
		Util.isBlank(getPostalCode()) && Util.isBlank(getCountry()) && Util.isBlank(getBuildingName()) && 
		Util.isBlank(getFloor()) && Util.isBlank(getSuiteUnit()) && Util.isBlank(getStreetType()) &&
		Util.isBlank(getStreetDirection()) );
	}

}