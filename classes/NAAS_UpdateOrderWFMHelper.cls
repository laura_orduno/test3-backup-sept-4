/***********************************************************
 * @author : Arpit Goyal
 * Date : 16-Dec-2016
 * Sprint : MVP release - Sprint 10
 * Purpose : Class contains actions after WFM booking and Work order update
************************************************************/
public class NAAS_UpdateOrderWFMHelper {

    public void updateServiceEntryObj(List<Work_Order__c> woList){
        system.debug('Inside OCOM_UpdateOrderWFMHelper.afterReservationActions()');
        system.debug('incoming woList ----->' + woList);
        List<Id> oliListFromWorkOrders = new List<Id>() ; 
        for(Work_Order__c woObj : woList){
            
            if(woObj.Status__c.equalsIgnoreCase('Reserved')){
                Id oliIdOnWOObject = woObj.OpportunityLineItem__c ; 
                oliListFromWorkOrders.add(oliIdOnWOObject) ;
            }
             
        }
        
        system.debug('oliListFromWorkOrders ----->' + oliListFromWorkOrders);    
        Map<Id, OrderItem> oliMap = new Map<Id, OrderItem>([Select Id, Service_Address__c, isWFMReserved__c from OrderItem where Id IN : oliListFromWorkOrders ]);
        system.debug('oliMap that contains OLIs from workOrder----->' + oliMap);    
        Set<ServiceAddressOrderEntry__c> serviceOrderEntrySet =  new Set<ServiceAddressOrderEntry__c>() ;
        //Added today
        //Map<Id, ServiceAddressOrderEntry__c> serviceEntryMap = new  Map<Id, ServiceAddressOrderEntry__c>();
        Set<Id> setOfServiceAddressId = new Set<Id>();
        
        for(OrderItem oliObj : [Select Id, Service_Address__c, isWFMReserved__c from OrderItem where  id in: oliListFromWorkOrders ]){
            setofServiceAddressId.add(oliObj.Service_Address__c);
        }
        Map<Id, ServiceAddressOrderEntry__c> serviceEntryMap = new  Map<Id, ServiceAddressOrderEntry__c>([Select Id, AlreadyBookedProductsCount__c, BookableProductsCount__c 
                                                                                                           from ServiceAddressOrderEntry__c 
                                                                                                           where Address__c IN : setofServiceAddressId]);
        //Ends today
        /*for(OrderItem oliObj : oliMap.values()){
                serviceOrderEntrySet.add([Select Id, AlreadyBookedProductsCount__c, BookableProductsCount__c 
                                           from ServiceAddressOrderEntry__c 
                                           where Address__c =: oliObj.Service_Address__c and Order__c =:  oliObj.OrderId Limit 1]);
            }*/
            system.debug('serviceEntryMap ----->' + serviceEntryMap );   //This will contain only unique values
            for(ServiceAddressOrderEntry__c saoeObj : serviceEntryMap.values()){
                Integer bookedProductsCount = 0 ;
                if(saoeObj.AlreadyBookedProductsCount__c == 0){
                    saoeObj.AlreadyBookedProductsCount__c = 1  ;
                }else{
                    saoeObj.AlreadyBookedProductsCount__c ++ ;
                    if(saoeObj.AlreadyBookedProductsCount__c > saoeObj.BookableProductsCount__c ) 
                    { break ; }
                }
                    
                if(saoeObj.AlreadyBookedProductsCount__c == saoeObj.BookableProductsCount__c ){
                    saoeObj.TechnicianBookingStatus__c  = 'Reserved' ;
                    
                }
                update saoeObj;
            }
    }
   /********************************
     Created by : Arpit Goyal
     Date : 14-Dec-2016
     Release / Sprint :  MVP Release - Sprint 11
     params : List<OrderItem> oliList
     returnType : void
     Purpose : This method sets BookableProductsCount__c  count on service entry object  when OLi is added on Order
    ********************************/ 
    public void setBookableProductCount(List<OrderItem> oliList){
        system.debug('incoming oliList ---->' + oliList);
        system.debug('incoming oliList size ---->' + oliList.size());
        Integer bookableCountOnOrderItem = 0  ;
        List<ServiceAddressOrderEntry__c> toBeUpdatedList = new List<ServiceAddressOrderEntry__c>();
        set<id> seiJuncId = new set<id>();
        for(OrderItem oliObj : oliList){ 
            Id orderId = oliObj.OrderId ;
            if(oliObj.isBookable__c){
                List<ServiceAddressOrderEntry__c> junctionObjList = [Select ID, Address__c, BookableProductsCount__c from ServiceAddressOrderEntry__c where Address__c =: oliObj.Service_Address__c AND Order__c =: orderId  ] ;
                if(junctionObjList != null && junctionObjList.size() != 0 ){
                    if(junctionObjList[0].BookableProductsCount__c ==  null || junctionObjList[0].BookableProductsCount__c ==  0){
                        junctionObjList[0].BookableProductsCount__c = 1 ;
                    }else{
                        junctionObjList[0].BookableProductsCount__c ++ ;  
                    }
                        
                    system.debug('for ' + oliObj.Id + ' isBookable__c is ' + oliObj.isBookable__c + ' and its BookableProductsCount__c is ' + bookableCountOnOrderItem);
                    system.debug('incoming junctionObjList to update ---->' + junctionObjList);
                    //toBeUpdatedList.add(junctionObjList[0]); commented by prashant
                    if(!seiJuncId.contains(junctionObjList[0].id)){// added by prashant
                        toBeUpdatedList.add(junctionObjList[0]);
                    }
                   // added by prashant- to fix error of duplicate key while adding products in select and configure - on 8 Feb 2017
                   seiJuncId.add(junctionObjList[0].id);
                }
                
            }
        }
        update toBeUpdatedList ;
        
    }
    
    /********************************
     Created by : Arpit Goyal
     Date : 23-Dec-2016
     Release / Sprint :  MVP Release - Sprint 11
     params : List<OrderItem> oliList
     returnType : void
     Purpose : This method decreases BookableProductsCount__c  count omn service entry object  when OLi is removed from Order
    ********************************/   
  public void decreaseBookableProductCount(List<OrderItem> oliList){
        if(oliList != null){
               
            system.debug('incoming oliList ---->' + oliList);
            system.debug('incoming oliList size ---->' + oliList.size());
            Integer bookableCountOnOrderItem = 0  ;
            List<ServiceAddressOrderEntry__c> toBeUpdatedList = new List<ServiceAddressOrderEntry__c>();
            set<id> seiJuncId = new set<id>();
            for(OrderItem oliObj : oliList){ 
                Id orderId = oliObj.OrderId ;
                if(oliObj.isBookable__c){
                    List<ServiceAddressOrderEntry__c> junctionObjList = [Select ID, Address__c, BookableProductsCount__c from ServiceAddressOrderEntry__c where Address__c =: oliObj.Service_Address__c AND Order__c =: orderId  ] ;
                       if(junctionObjList != null && junctionObjList.size()>0){     
                            if(junctionObjList[0].BookableProductsCount__c == 0){
                                break ;
                            }else if(junctionObjList != null && junctionObjList.size() != 0 ){
                                   junctionObjList[0].BookableProductsCount__c -- ;  
                            }
                            system.debug('incoming junctionObjList to update ---->' + junctionObjList);
                            if(!seiJuncId.contains(junctionObjList[0].id)){// added by prashant
                                toBeUpdatedList.add(junctionObjList[0]);
                            }
                           // added by prashant- to fix error of duplicate key while adding products in select and configure
                           seiJuncId.add(junctionObjList[0].id);
                       }
                }
            }
            update toBeUpdatedList ;
        }
    }
}