@isTest
private class telus_SMETProjectMemberUpdateTest {

    static testMethod void insertTest() {
    	
        SMET_Project__c p = new SMET_Project__c(name = 'test', Impact__c = 5, Complexity__c = 5, Project_Prime__c = UserInfo.getUserId());
        insert p;
                        
        List<Cloud_Enablement_Project_Team__c> ms = [SELECT Cloud_Enablement_Project__c, Team_Member__c FROM Cloud_Enablement_Project_Team__c where Cloud_Enablement_Project__c = :p.id];
        System.assertEquals(ms.size(),1); // confirm 1 member
        
    }
    
    static testMethod void updateTest() {
    	
        SMET_Project__c p = new SMET_Project__c(name = 'test', Impact__c = 5, Complexity__c = 5, Project_Prime__c = UserInfo.getUserId());
        insert p;
                        
        List<Cloud_Enablement_Project_Team__c> ms = [SELECT Cloud_Enablement_Project__c, Team_Member__c FROM Cloud_Enablement_Project_Team__c where Cloud_Enablement_Project__c = :p.id];
        System.assertEquals(ms.size(),1); // confirm 1 member
        
        delete ms;
        
		//List<Cloud_Enablement_Project_Team__c> ms1 = [SELECT Cloud_Enablement_Project__c, Team_Member__c FROM Cloud_Enablement_Project_Team__c where Cloud_Enablement_Project__c = :p.id];
        //System.assertEquals(ms1.size(),0); // confirm 0 member
        
        p.Impact__c = 4;
        update p;
        
        ms = [SELECT Cloud_Enablement_Project__c, Team_Member__c FROM Cloud_Enablement_Project_Team__c where Cloud_Enablement_Project__c = :p.id];
        System.assertEquals(ms.size(),1); // confirm 1 member
    }
    
}