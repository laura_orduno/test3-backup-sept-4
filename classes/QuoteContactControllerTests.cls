@isTest
private class QuoteContactControllerTests {
    private static Web_Account__c account;
    private static Contact contact;
    private static SBQQ__Quote__c quote;
    
    testMethod static void testConstructor() {
        setUp();
        
        ApexPages.currentPage().getParameters().put('qid', quote.Id);
        ApexPages.currentPage().getParameters().put('aid', account.Id);
        ApexPages.currentPage().getParameters().put('stage', QuoteStatus.DRAFT);
        QuoteContactController target = new QuoteContactController();
        System.assertEquals(true, target.rDraft);
        System.assertEquals(false, target.rCreditCheck);
        System.assertEquals(false, target.rAgreement);
        
        ApexPages.currentPage().getParameters().put('stage', QuoteStatus.SENT_FOR_CREDIT_CHECK);
        target = new QuoteContactController();
        System.assertEquals(true, target.rDraft);
        System.assertEquals(true, target.rCreditCheck);
        System.assertEquals(false, target.rAgreement);
        
        ApexPages.currentPage().getParameters().put('stage', QuoteStatus.SENT_AGREEMENT);
        target = new QuoteContactController();
        System.assertEquals(true, target.rDraft);
        System.assertEquals(true, target.rCreditCheck);
        System.assertEquals(true, target.rAgreement);
        
        ApexPages.currentPage().getParameters().remove('stage');
        
        target = new QuoteContactController();
        System.assertEquals(true, target.rDraft);
        System.assertEquals(false, target.rCreditcheck);
        System.assertEquals(false, target.rAgreement);
        
        quote.SBQQ__Status__c = QuoteStatus.SENT_FOR_CREDIT_CHECK;
        update quote;
        target = new QuoteContactController();
        System.assertEquals(true, target.rDraft);
        System.assertEquals(true, target.rCreditCheck);
        System.assertEquals(false, target.rAgreement);
        
        quote.SBQQ__Status__c = QuoteStatus.SENT_AGREEMENT;
        update quote;
        target = new QuoteContactController();
        System.assertEquals(true, target.rDraft);
        System.assertEquals(true, target.rCreditCheck);
        System.assertEquals(true, target.rAgreement);
    }
    
    testMethod static void testCancel() {
        setUp();
        ApexPages.currentPage().getParameters().put('qid', quote.Id);
        ApexPages.currentPage().getParameters().put('aid', account.Id);
        ApexPages.currentPage().getParameters().put('retURL', '/' + quote.Id);
        ApexPages.currentPage().getParameters().put('stage', QuoteStatus.SENT_FOR_CREDIT_CHECK);
        QuoteContactController target = new QuoteContactController();
        target.onCancel();
    }
    
    testMethod static void testSaveAndContinue() {
        setUp();
        
        ApexPages.currentPage().getParameters().put('qid', quote.Id);
        ApexPages.currentPage().getParameters().put('aid', account.Id);
        QuoteContactController target = new QuoteContactController();
        target.selectedContact.lastName = 'Test';
        target.selectedContact.phone = '6049969449';
        PageReference pref = target.onSaveAndContinue();
        System.assert(pref != null, 'Errors:' + ApexPages.getMessages());
        System.assert(target.getPreviousPageURL() != null);
    }
    
    testMethod static void testSelectAndContinue() {
        setUp();
        
        ApexPages.currentPage().getParameters().put('qid', quote.Id);
        ApexPages.currentPage().getParameters().put('aid', account.Id);
        QuoteContactController target = new QuoteContactController();
        System.assertEquals(1, target.getContacts().size());
        target.selectedContactId = contact.Id;
        PageReference pref = target.onSelectAndContinue();
        System.assert(pref != null, 'Errors:' + ApexPages.getMessages());
    }
    
    testMethod static void testSave() {
        setUp();
        
        ApexPages.currentPage().getParameters().put('id', contact.Id);
        ApexPages.currentPage().getParameters().put('retURL', '/home/home.jsp');
        QuoteContactController target = new QuoteContactController();
        PageReference pref = target.onSave();
        System.assert(pref != null, 'Errors:' + ApexPages.getMessages());
    }
    
    testMethod static void testAddContact() {
        setUp();
        
        ApexPages.currentPage().getParameters().put('qid', quote.Id);
        ApexPages.currentPage().getParameters().put('aid', account.Id);
        ApexPages.currentPage().getParameters().put('addon', '1');
        QuoteContactController target = new QuoteContactController();
        target.selectedContact.lastName = 'Test';
        target.selectedContact.phone = '6049969449';
        PageReference pref = target.onAddContact();
        System.assertEquals(0, ApexPages.getMessages().size(), 'Errors:' + ApexPages.getMessages());
        System.assert(target.getPreviousPageURL() != null);
    }
    
    testMethod static void testDelete() {
        setUp();
        
        ApexPages.currentPage().getParameters().put('id', contact.Id);
        ApexPages.currentPage().getParameters().put('retURL', '/home/home.jsp');
        QuoteContactController target = new QuoteContactController();
        PageReference pref = target.onDelete();
        System.assert(pref != null, 'Errors:' + ApexPages.getMessages());
    }
    
    private static void setUp() {
        account = new Web_Account__c(Name='UnitTest', Phone__c='6049969449');
        insert account;
        
        contact = new Contact(LastName='Test',FirstName='Max',Web_Account__c=account.Id, Phone='6049969449');
        insert contact;
        
        Opportunity opp = new Opportunity(Name='Test',Web_Account__c=account.Id,CloseDate=System.today(),StageName='Prospecting');
        insert opp;
        
        quote = new SBQQ__Quote__c(SBQQ__Opportunity__c=opp.Id);
        quote.Rate_Band__c = 'F';
        quote.Province__c = 'BC';
        insert quote;
        
        SBQQ__QuoteLine__c line = new SBQQ__QuoteLine__c(SBQQ__Quote__c=quote.Id,SBQQ__Bundle__c=true);
        insert line;
    }
}