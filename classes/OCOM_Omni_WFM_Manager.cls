/* This class is used for WFM's Omniscript Backend Logic .
Project: OCOM (WP3) 
Developer: Aditya Jamwal (IBM)
Created Date: 10-June-2016
*/

global with sharing  class OCOM_Omni_WFM_Manager implements vlocity_cmt.VlocityOpenInterface2{
    
    public Boolean invokeMethod(String methodName, Map<String, Object> input, Map<String, Object> output, Map<String, Object> options)  {
        if (methodName.indexOf('SetRequestedDatePadding')!=-1) {
            SetRequestedDatePadding(input, output, options);
            getContactDetails(input,output,options);
        }
        
        if (methodName.indexOf('SetLatestContactInFo')!=-1) {
            SetLatestContactInFo(input, output, options);
        }
        
        if (methodName.indexOf('SetRequestedDateToOffers')!=-1) {
            SetRequestedDateToOffers(input, output, options);
        }
        return false;
    }
    
     private void SetRequestedDateToOffers(Map<String, Object> input, Map<String, Object> output, Map<String, Object> options){
        try{
            String OderId = (String)input.get('ContextId');
            Map<String, Object> installationDateMap = (Map<String, Object>)input.get('InstallDateRange');
            system.debug('!!installationDateMap '+installationDateMap);
            if(null != installationDateMap && null != installationDateMap.get('installDate')){
               String requestedDate = (String) installationDateMap.get('installDate');
                if(String.isNotBlank(requestedDate)){
                   requestedDate = requestedDate.substringBefore('T');
                }
            List<OrderItem> OliList = [Select id,vlocity_cmt__JSONAttribute__c from orderItem where orderId =:OderId];
            List<OrderItem> updateOLIList = new List<OrderItem>();
             Map<String,String> AttributeWithValueMap = new  Map<String,String>();
            system.debug('!!! intallDateDetailsText '+input);
             AttributeWithValueMap.put('Requested Due Date',requestedDate);
            if(String.isNotBlank(OderId) && null != OliList && OliList.size()>0 && null != AttributeWithValueMap && AttributeWithValueMap.size()>0){
                for(OrderItem ol: OliList){
                   String newJSON = OCOM_VlocityJSONParseUtil.setAttributesInJSON(ol.vlocity_cmt__JSONAttribute__c,AttributeWithValueMap);
                    if(String.isNotBlank(newJSON) && newJSON != ol.vlocity_cmt__JSONAttribute__c){
                        ol.vlocity_cmt__JSONAttribute__c =newJSON ;
                        updateOLIList.add(ol);
                         system.debug('!!!! newJSON  '+ newJSON );
                    }
                }
            }
            if(null != updateOLIList && updateOLIList.size()>0){
                update updateOLIList;
            }
            system.debug('!!!! '+ updateOLIList); 
            }
         if(Test.isRunningTest()){  integer i=4/0;}   
        }catch(exception e){
            String errorMessage=Label.OCOM_ExceptionMessagePrefix+e.getMessage();
            output.put('error',errorMessage);
        }
    }     

    private void getContactDetails(Map<String, Object> input, Map<String, Object> output, Map<String, Object> options){
        try{ 
            String OderId = (String)input.get('ContextId');
            Map<String, Object> onsiteContactInfo = new Map<String, Object>(); 
            Map<String, Object> orderContactInfo = new Map<String, Object>();
            List<Order> Odr =[Select id,CustomerAuthorizedBy.Name,ContactEmail__c,ContactPhone__c,captureLatestOnsiteContact__c from order where id=:OderId];
            
            /* 12/5/2017 Sid: Send Order Contact Details */
            if (null!=Odr && Odr.size()>0 && null != Odr[0].CustomerAuthorizedBy) {
                orderContactInfo.put('onsiteContact',Odr[0].CustomerAuthorizedBy.Name);
                orderContactInfo.put('onsiteContactMoveOut',Odr[0].CustomerAuthorizedBy.Name);
                if(String.isNotBlank(Odr[0].ContactEmail__c)){
                    orderContactInfo.put('onsiteEmail',Odr[0].ContactEmail__c);   
                }else{ 
                    orderContactInfo.put('onsiteEmail',' ');
                } 
                if(String.isNotBlank(Odr[0].ContactEmail__c)){
                    orderContactInfo.put('onsiteEmailMoveOut',Odr[0].ContactEmail__c);   
                }else{ 
                    orderContactInfo.put('onsiteEmailMoveOut',' ');
                }                               
                orderContactInfo.put('onsitePhone',Odr[0].ContactPhone__c);
                orderContactInfo.put('onsitePhoneMoveOut',Odr[0].ContactPhone__c);
                orderContactInfo.put('sameAsOrderContact',true);
                output.put('orderContactInfo', orderContactInfo);
            }
            
            if(null!=Odr && Odr.size()>0 && String.isNotBlank(Odr[0].captureLatestOnsiteContact__c)){
                    ContactWrapper deserializedCont = (ContactWrapper)JSON.deserialize(Odr[0].captureLatestOnsiteContact__c,ContactWrapper.class); 
                    onsiteContactInfo.put('onsiteContact', deserializedCont.onsiteContact);
                    onsiteContactInfo.put('onsiteEmail', deserializedCont.onsiteEmail);
                    onsiteContactInfo.put('onsitePhone', deserializedCont.onsitePhone);  
                    onsiteContactInfo.put('installRemarks', deserializedCont.installRemarks);
                    onsiteContactInfo.put('onsiteContactMoveOut', deserializedCont.onsiteContactMoveOut);
                    onsiteContactInfo.put('onsiteEmailMoveOut', deserializedCont.onsiteEmailMoveOut);
                    onsiteContactInfo.put('onsitePhoneMoveOut', deserializedCont.onsitePhoneMoveOut);  
                    onsiteContactInfo.put('installRemarksMoveOut', deserializedCont.installRemarksMoveOut);
                    onsiteContactInfo.put('sameAsOrderContact', deserializedCont.sameAsOrderContact);                        
                    output.put('onsiteContactInfo',onsiteContactInfo);
                    return;
                }
            else if(null!=Odr && Odr.size()>0 && String.isBlank(Odr[0].captureLatestOnsiteContact__c)){
                
                onsiteContactInfo.put('onsiteContact','');
                onsiteContactInfo.put('onsiteContactMoveOut','');
                onsiteContactInfo.put('onsiteEmail','');
                onsiteContactInfo.put('onsiteEmailMoveOut','');                              
                onsiteContactInfo.put('onsitePhone','');
                onsiteContactInfo.put('onsitePhoneMoveOut','');
                onsiteContactInfo.put('installRemarks','');
                onsiteContactInfo.put('installRemarksMoveOut',''); 
                onsiteContactInfo.put('sameAsOrderContact', false);   
                output.put('onsiteContactInfo',onsiteContactInfo);
            }           
          if(Test.isRunningTest()){  integer i=4/0;}  
        }catch(exception e){
            String errorMessage=Label.OCOM_ExceptionMessagePrefix+e.getMessage();
            output.put('error',errorMessage);
        } 
    }
    private void SetLatestContactInFo(Map<String, Object> input, Map<String, Object> output, Map<String, Object> options){
        try{
            String OderId = (String)input.get('ContextId');
            Map<String, Object> onsiteContactInfo = (Map<String, Object>) input.get('onsiteContactInfo');
            if(onsiteContactInfo==null){
                return;
            }
            System.debug(onsiteContactInfo);
            
            ContactWrapper cont= new ContactWrapper(((String)onsiteContactInfo.get('onsiteContact')),
                                                    ((String)onsiteContactInfo.get('onsiteEmail')),
                                                    ((String)onsiteContactInfo.get('onsitePhone')),
                                                    ((String)onsiteContactInfo.get('installRemarks')),
                                                    ((String)onsiteContactInfo.get('onsiteContactMoveOut')),
                                                    ((String)onsiteContactInfo.get('onsiteEmailMoveOut')),
                                                    ((String)onsiteContactInfo.get('onsitePhoneMoveOut')),
                                                    ((String)onsiteContactInfo.get('installRemarksMoveOut')),
                                                    ((Boolean)onsiteContactInfo.get('sameAsOrderContact')));
            String JSONString = JSON.serialize(cont); 
            if(String.isNotBlank(OderId) && String.isNotBlank(JSONString)){
                Order ord= new Order(id = OderId,captureLatestOnsiteContact__c=JSONString);
                update ord;
            }
          if(Test.isRunningTest()){  integer i=4/0;}
        }catch(exception e){
            System.debug(e.getStackTraceString());
            String errorMessage=Label.OCOM_ExceptionMessagePrefix+e.getMessage();
            output.put('error',errorMessage);
        }
    }     
    
    
    private void SetRequestedDatePadding(Map<String, Object> inputMap, Map<String, Object> outputMap, Map<String, Object> options) {
        //string currentUrl =  ApexPages.currentPage().getUrl();
        //Cache.Session.put('CurrentUrl', currentUrl);  
        //System.debug('WFM URL_____' + currentUrl);

        integer datePaddingMap = (integer)inputMap.get('datePaddingValue');
        Date InstallDate;
        system.debug(' datePaddingValue datePaddingMap ' +datePaddingMap);
        Integer WFMRequestedDatePadding;
        try{ 
            list<user> lstUsr = [select id, CPQ_Super_User_Access__c from User where id = :Userinfo.getUserId()];
            
            if(lstUsr != null && lstUsr.size()>0)
            {  
                if (lstUsr[0].CPQ_Super_User_Access__c){
                    WFMRequestedDatePadding=0;
                    system.debug(' in super user.');
                }                  
                else {
                    OCOM_WFM_Parameters__c wfmParam= OCOM_WFM_Parameters__c.getValues('WFMRequestedDatePadding');
                    if(null!=wfmParam){
                        WFMRequestedDatePadding=Integer.valueOf(wfmParam.Parameter_value__c);
                        system.debug(' not in super user.' +WFMRequestedDatePadding);
                    }
                }
                outPutMap.put('datePaddingValue',WFMRequestedDatePadding);
                if(Test.isRunningTest()){  integer i=4/0;}
            }
        }catch(exception e){
            String errorMessage=Label.OCOM_ExceptionMessagePrefix+e.getMessage();
            outPutMap.put('error',errorMessage);
        }
        
    }
    
    public Class ContactWrapper{
        String onsiteContact;
        String onsiteEmail;
        String onsitePhone;
        String installRemarks;
        String onsiteContactMoveOut;
        String onsiteEmailMoveOut;
        String onsitePhoneMoveOut;
        String installRemarksMoveOut;
        Boolean sameAsOrderContact;        
        
        public ContactWrapper(String onsiteContact,String onsiteEmail,String onsitePhone,String installRemarks,String onsiteContactMoveOut,String onsiteEmailMoveOut,String onsitePhoneMoveOut,String installRemarksMoveOut,Boolean sameAsOrderContact){
            this.onsiteContact = onsiteContact;
            this.onsiteEmail = onsiteEmail;  
            this.onsitePhone = onsitePhone;
            this.installRemarks =installRemarks;
            this.onsiteContactMoveOut = onsiteContactMoveOut;
            this.onsiteEmailMoveOut = onsiteEmailMoveOut;  
            this.onsitePhoneMoveOut = onsitePhoneMoveOut;
            this.installRemarksMoveOut = installRemarksMoveOut;  
            this.sameAsOrderContact = sameAsOrderContact;            
        }
    }
    
}