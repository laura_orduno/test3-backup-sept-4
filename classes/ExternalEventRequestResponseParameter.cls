/*****************************************************
    Name: ExternalEventRequestResponseParameter
    Usage: This class to used to define require parameters used for web service or rest call
    Author – Sandip Chaudhari (IBM)
    Revision History -
    Created By/Date - Sandip Chaudhari / 04 July 2016
    
    Updated By/Date/Reason for Change - 
    
******************************************************/
global class ExternalEventRequestResponseParameter{
    
    // Parametere used in request xml to service
    global class RequestEventData{
      webservice String eventName;
      webservice String eventDetails;
      global RequestEventData(){
      }
    }
  
    // Parametere to send in response xml of service
    global class ResponseEventData{
      webservice String eventId;
      webservice List<EventError> error;
      global ResponseEventData(){
      }
    }
    
    // Error Details parametes used to send in response xml
    global class EventError{
        webservice String errorCode;
        webservice String errorDescription;
        global EventError(){
        }
    }
}