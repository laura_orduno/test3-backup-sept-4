@isTest
public class OpportunityAttachOrderTest {
    @isTest
    private static void getOrders(){
        List<Opportunity> oppList=OrdrTestDataFactory.createOpprtunities(1);
        Opportunity opObj=oppList.get(0);
        ApexPages.StandardController sc = new ApexPages.standardController(opObj);
        OpportunityAttachOrder cntrl=new OpportunityAttachOrder(sc);
        Test.startTest();
        cntrl.getOrders();
        Test.stopTest();
    }
    @isTest
    private static void attachOrder(){
        List<Opportunity> oppList=OrdrTestDataFactory.createOpprtunities(1);
        Opportunity opObj=oppList.get(0);
        ApexPages.StandardController sc = new ApexPages.standardController(opObj);
        Test.startTest();
        String ordrId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        String jsonStr='[{"opiId":null,"opiName":null,"orderType":null,"srsProductId":null,"contractLength":null,"dateValue":null,"createSR":null,"recordId":"'+ordrId+'"}]';
        OpportunityAttachOrder.attachOrder(opObj.id,jsonStr);
        Test.stopTest();
    }
    @isTest
    private static void extractOrdIdList(){
        String jsonStr='[{"opiId":null,"opiName":null,"orderType":null,"srsProductId":null,"contractLength":null,"dateValue":null,"createSR":null,"recordId":"80123000000CuaPAAS"}]';
        Test.startTest();
        OpportunityAttachOrder.extractOrdIdList(jsonStr);
        Test.stopTest();
    }
    @isTest
    private static void newOrder(){
        List<Opportunity> oppList=OrdrTestDataFactory.createOpprtunities(1);
        Opportunity opObj=oppList.get(0);
        ApexPages.StandardController sc = new ApexPages.standardController(opObj);
        Test.startTest();
        OpportunityAttachOrder obj=new OpportunityAttachOrder(sc);
        obj.newOrder();
        Test.stopTest();
    }
    
}