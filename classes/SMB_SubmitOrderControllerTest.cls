/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SMB_SubmitOrderControllerTest {

    static testMethod void TestCase_1() {
        PageReference pageref = Page.SMB_OrderSuccess;
        Test.setCurrentPage(pageref);
        Test.startTest();
        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'All Other Orders';
        insert testOpp_1;
        pageref.getparameters().put('id', testOpp_1.Id);
        pageref.getparameters().put('operation', 'submit');
        
        ApexPages.StandardController sc_1 = new ApexPages.standardController(testOpp_1);
        SMB_SubmitOrderController contToTest_1 = new SMB_SubmitOrderController(sc_1);
        
        contToTest_1.SubmitOrder();
        
        Test.stopTest();
    }
    
    static testMethod void TestCase_2() {
        PageReference pageref = Page.SMB_OrderSuccess;
        Test.setCurrentPage(pageref);
        
        Test.startTest();
           TestDataHelper.testAccountCreation();
        TestDataHelper.testContactCreation();
        
        
        Opportunity testOpp_2   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_2.Printed_Agreement_Type__c = 'Business Anywhere Plus';
        testOpp_2.type = 'New Provide/Upgrade Order';
        insert testOpp_2;
        pageref.getparameters().put('id', testOpp_2.Id);
        pageref.getparameters().put('operation', 'submit');
        
       Apttus__APTS_Agreement__c agreement = new Apttus__APTS_Agreement__c(
                                                    Name = 'test Agreement for Comply Custom API 1', 
                                                    Apttus__Account__c = TestDataHelper.testAccountObj.Id, 
                                                    Apttus__Status_Category__c = 'Request', 
                                                    Apttus__Status__c = 'Request', 
                                                    Apttus__Contract_Start_Date__c = date.today(), 
                                                    Apttus__Contract_End_Date__c = date.today().addDays(365), 
                                                    Apts_Envelope_Id__c = '8fd115ee-43c2-480a-bcd3-008910ec4988', 
                                                    Printed_Agreement_Type__c ='BusinessAnywhere',
                                                    APTS_Email_Address_Customer_Auth_Contact__c = 'abc@apttus.com',
                                                    Apttus__Term_Months__c = 12, 
                                                    APTS_Customer_Signing_Authority__c = 'abc',Contract_Signor__c=TestDataHelper.testContactObj.id);

        insert agreement;
        
        ApexPages.StandardController sc_2 = new ApexPages.standardController(testOpp_2);
        SMB_SubmitOrderController contToTest_2 = new SMB_SubmitOrderController(sc_2);
        contToTest_2.selectFlow = 'Apptus';
        contToTest_2.SubmitOrder();
        contToTest_2.sendDocumentToApttus();
        Test.stopTest();
    }
    
    static testMethod void TestCase_3() {
        
        PageReference pageref = Page.SMB_OrderSuccess;
        Test.setCurrentPage(pageref);
        
        Test.startTest();

        pageref.getparameters().put('operation', 'submit');

        SMB_SubmitOrderController contToTest_3 = new SMB_SubmitOrderController();
        
        contToTest_3.SubmitOrder();
        Test.stopTest();
    }
    
    static testMethod void TestCase_4() {
        PageReference pageref = Page.SMB_OrderSuccess;
        Test.setCurrentPage(pageref);
        
        Test.startTest();
        Opportunity testOpp_4   = smb_test_utility.createOpportunity('SMB_Care_Amend_Order_Opportunity', Null, Null, false);
        testOpp_4.StageName = 'Contract On Hold (Customer)';
        /*
        * Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
        * START
        */
            testOpp_4.Stage_update_time__c=System.Now();
        /*
        * Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
        * END
        */
        testOpp_4.Type = 'All Other Orders';
        insert testOpp_4;
        //smb_test_utility.createPQLI('PQL_Test_Data',testOpp_4.Id);
        //smb_test_utility.createPQLI('PQL_Test_Data_Modified',testOpp_4.Id);
        pageref.getparameters().put('id', testOpp_4.Id);
        pageref.getparameters().put('operation', 'submit');
        
        ApexPages.StandardController sc_4 = new ApexPages.standardController(testOpp_4);
        SMB_SubmitOrderController contToTest_4 = new SMB_SubmitOrderController(sc_4);
        
        contToTest_4.SubmitOrder();
        Test.stopTest();
    }
    
    static testMethod void TestCase_5() {
        PageReference pageref = Page.SMB_OrderSuccess;
        Test.setCurrentPage(pageref);
        
        Test.startTest();
        Opportunity testOpp_5   = smb_test_utility.createOpportunity('SMB_Care_Amend_Order_Opportunity', Null, Null, false);
        testOpp_5.StageName = 'Order Request New';
        /*
        * Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
        * START
        */
            testOpp_5.Stage_update_time__c=System.Now();
        /*
        * Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
        * END
        */
        testOpp_5.Amend_Stage__c = 'Order Request New';
        testOpp_5.Type = 'New Provide/Upgrade Order';
        testOpp_5.Printed_Agreement_Type__c = 'Business Anywhere';
        testOpp_5.is_Wireless_Products_Added_or_Changed__c = true;
        testOpp_5.Agreement_Required__c = 'Yes';
        insert testOpp_5;
        
        //smb_test_utility.createPQLI('PQL_Test_Data',testOpp_5.Id);
        
        pageref.getparameters().put('id', testOpp_5.Id);
        pageref.getparameters().put('operation', 'submit');
        
        Order_Request__c ORC = new Order_Request__c(Opportunity__c = testOpp_5.id, NC_Order_ID__c ='9137412239213520216', CurrencyIsoCode='USD', Order__c ='New');
        insert ORC;

        testOpp_5 = [Select Id,Printed_Agreement_Type__c, Amend_Stage__c, Type,is_Wireless_Products_Added_or_Changed__c,StageName,RecordTypeId,RecordType.DeveloperName,Contract_Signor__r.FirstName, Contract_Signor__r.LastName, 
        (Select Id, Order__c, NC_Order_ID__c from Order_Requests__r where Order__c NOT IN ('Completed','Cancelled'))
        From Opportunity where Id =:testOpp_5.Id ];
        
        smb_test_utility.createCustomSettingData();
        
        system.debug('-->> testOpp_5' + testOpp_5);
        system.debug('-->> testOpp_5' + testOpp_5.order_requests__r[0]);
        
        ApexPages.StandardController sc_5 = new ApexPages.standardController(testOpp_5);
        SMB_SubmitOrderController contToTest_5 = new SMB_SubmitOrderController(sc_5);
        contToTest_5.selectFlow = 'Apttus';
        contToTest_5.submitAmendedOrder();
        Test.stopTest();
    }
    
    static testMethod void TestCase_6() {
        PageReference pageref = Page.SMB_OrderSuccess;
        Test.setCurrentPage(pageref);
        
        Test.startTest();
        Opportunity testOpp_6   = smb_test_utility.createOpportunity('SMB_Care_Amend_Order_Opportunity', Null, Null, false);
        testOpp_6.StageName = 'Order Request New';
        /*
        * Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
        * START
        */
            testOpp_6.Stage_update_time__c=System.Now();
        /*
        * Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
        * END
        */
        testOpp_6.Type = 'All Other Orders';
        testOpp_6.Amend_Stage__c = 'Order Request New';
        insert testOpp_6;
        
        //smb_test_utility.createPQLI('PQL_Test_Data',testOpp_6.Id);
        //smb_test_utility.createPQLI('PQL_Test_Data_Modified',testOpp_6.Id);
        pageref.getparameters().put('id', testOpp_6.Id);
        pageref.getparameters().put('operation', 'submit');
        
        Order_Request__c ORC = new Order_Request__c(Opportunity__c = testOpp_6.id, NC_Order_ID__c ='9137412239213520216', CurrencyIsoCode='USD', Order__c ='New');
        insert ORC;

        testOpp_6 = [Select Id, 
        (Select Id, Order__c, NC_Order_ID__c from Order_Requests__r where Order__c NOT IN ('Completed','Cancelled'))
        From Opportunity where Id =:testOpp_6.Id ];
        
        smb_test_utility.createCustomSettingData();
        
        ApexPages.StandardController sc_6 = new ApexPages.standardController(testOpp_6);
        SMB_SubmitOrderController contToTest_6 = new SMB_SubmitOrderController(sc_6);
        
        contToTest_6.submitAmendedOrder();
        Test.stopTest();
    }
    
   
     static testMethod void TestCase_7() {
        PageReference pageref = Page.SMB_OrderSuccess;
        Test.setCurrentPage(pageref);
        Test.startTest();
        
        Opportunity testOpp_8   = smb_test_utility.createOpportunity('SMB_Care_Amend_Order_Opportunity', Null, Null, false);
        insert testOpp_8;
        
        Opportunity testOpp_7   = smb_test_utility.createOpportunity('SMB_Care_Amend_Order_Opportunity', testOpp_8.AccountId, Null, false);
        testOpp_7.StageName = 'Order Request New';
        testOpp_7.Type = 'All Other Orders';
        testOpp_7.Amend_Stage__c = 'Order Request New';
        testOpp_7.Provide_Order__c = true;
        testOpp_7.Related_Orders__c = testOpp_8.Id;
        //testOpp_7.AccountId=testOpp_8.AccountId;
        testOpp_7.StageName = 'Test Stage';
        insert testOpp_7;
        
        ApexPages.StandardController sc_7 = new ApexPages.standardController(testOpp_7);
        SMB_SubmitOrderController contToTest_7 = new SMB_SubmitOrderController(sc_7);
        contToTest_7.objOpportunity = testOpp_7;
 
        contToTest_7.submitAmendedOrder();
 
        //smb_test_utility.createPQLI('PQL_Test_Data',testOpp_7.Id);
        //smb_test_utility.createPQLI('PQL_Test_Data_Modified',testOpp_7.Id);
 
        pageref.getparameters().put('id', testOpp_7.Id);
 
        pageref.getparameters().put('operation', 'cancelSMBOpp');
        
        Order_Request__c ORC = new Order_Request__c(Opportunity__c = testOpp_7.id, NC_Order_ID__c ='9137412239213520216', CurrencyIsoCode='USD', Order__c ='New');
        insert ORC;
 
        testOpp_7 = [Select Id, 
        (Select Id, Order__c, NC_Order_ID__c from Order_Requests__r where Order__c NOT IN ('Completed','Cancelled'))
        From Opportunity where Id =:testOpp_7.Id ];
        
        smb_test_utility.createCustomSettingData();
        
        contToTest_7.operation = 'cancelSMBOpp';
        contToTest_7.loadOrderSuccess(); //for cancel operation
        
        contToTest_7.operation = 'submit';
        contToTest_7.loadOrderSuccess(); //for submit operation
        
        contToTest_7.RenderSuccess = true;
        contToTest_7.operation = 'cancelSMBOpp';
        contToTest_7.loadOrderSuccess(); 
        
        Id recordTypeIdStr = smb_test_utility.getRecordTypeId('SMB_Care_Opportunity', 'Opportunity');
        testOpp_7.RecordTypeId = recordTypeIdStr;
        testOpp_7.Agreement_Required__c = 'Yes';
 
 
 
        testOpp_7.Printed_Agreement_Type__c = 'Business Anywhere Plus';
        update testOpp_7;
        
        contToTest_7.objOpportunity = testOpp_7;
        contToTest_7.operation = 'submit';
 
 
        contToTest_7.SubmitOrder();
        
        Apttus__APTS_Agreement__c agreeRec = smb_test_utility.createAgreement('', false);
        agreeRec.Apttus__Related_Opportunity__c = contToTest_7.objOpportunity.Id;
        agreeRec.Apttus__Status__c = 'Request';
        insert agreeRec;
        
        contToTest_7.isEnabled = true;
 
 
        Test.stopTest();
        contToTest_7.sendDocumentToApttus();
    }
    
      static testMethod void TestCase_10() {
        PageReference pageref = Page.SMB_OrderSuccess;
        Test.setCurrentPage(pageref);
        Test.startTest();
        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'New Provide/Upgrade Order';
        insert testOpp_1;
     
        
        pageref.getparameters().put('id', testOpp_1.Id);
        pageref.getparameters().put('operation', 'cancelSMBOpp');
              

        
        Opportunity testOpp_8   = smb_test_utility.createOpportunity('SMB_Care_Amend_Order_Opportunity', Null, Null, false);
        insert testOpp_8;
         Opportunity testOpp_7   = smb_test_utility.createOpportunity('SMB_Care_Amend_Order_Opportunity', testOpp_8.AccountId, Null, false);
        testOpp_7.StageName = 'Order Request New';
        testOpp_7.Type = 'All Other Orders';
        testOpp_7.Amend_Stage__c = 'Order Request New';
        testOpp_7.Provide_Order__c = true;
        testOpp_7.Related_Orders__c = testOpp_8.Id;
        //testOpp_7.AccountId=testOpp_8.AccountId;
        //testOpp_7.StageName = 'Test Stage';
         insert testOpp_7;

        ApexPages.StandardController sc_7 = new ApexPages.standardController(testOpp_7);
        SMB_SubmitOrderController contToTest_7 = new SMB_SubmitOrderController(sc_7);
        contToTest_7.objOpportunity = testOpp_7;
        contToTest_7.selectFlow = 'vlocity';
        contToTest_7.submitAmendedOrder();
        
        
        
        Test.stopTest();
    }
    
       
    static testMethod void TestCase_11() {
        PageReference pageref = Page.SMB_OrderSuccess;
        Test.setCurrentPage(pageref);
        Test.startTest();
            TestDataHelper.testAccountCreation();
        TestDataHelper.testContactCreation();
        
        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'New Provide/Upgrade Order';
        insert testOpp_1;
        
      
        
        Contract testContract_1  = new Contract(Contract_Type__c='Amendment',AccountId=TestDataHelper.testAccountObj.Id,ContractTerm=12,StartDate=system.today(),vlocity_cmt__OpportunityId__c = testOpp_1.Id);
        insert testContract_1;

        Blob b = Blob.valueOf('Test Data');  
        Attachment attachment = new Attachment();  
        attachment.ParentId = testContract_1.Id;  
        attachment.Name = 'Test Attachment for Parent';  
        attachment.Body = b;  
        insert(attachment);  
    
        vlocity_cmt__ContractVersion__c testContractVersion = new vlocity_cmt__ContractVersion__c(Name='Test Version 1', vlocity_cmt__Status__c = 'Active', vlocity_cmt__ContractId__c = testContract_1.Id);
        insert testContractVersion;
        
        pageref.getparameters().put('id', testOpp_1.Id);
        pageref.getparameters().put('operation', 'cancelSMBOpp');
        
       
        ApexPages.StandardController sc_1 = new ApexPages.standardController(testOpp_1);
        SMB_SubmitOrderController contToTest_1 = new SMB_SubmitOrderController(sc_1);
       
     
        contToTest_1.generateAgreementDocument_vlocity();
    
        
        Test.stopTest();
    }
    
         static testMethod void TestCase_12() {
             
                       Test.startTest();

       User userRep = TestDataHelper.createUser( 'Sales Manager / Director / MD' , 'standt1','vlocityTestUser@testuser.com','tes1','vlocityTestUser@testuser.com');
                System.RunAs(userRep){ 
                    
                       PageReference pageref = Page.SMB_OrderSuccess;
        Test.setCurrentPage(pageref);
         TestDataHelper.testAccountCreation();
        TestDataHelper.testContactCreation();
        
        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'New Provide/Upgrade Order';
        insert testOpp_1;
        
      
        
        Contract testContract_1  = new Contract(Contract_Type__c='Amendment',AccountId=TestDataHelper.testAccountObj.Id,ContractTerm=12,StartDate=system.today(),vlocity_cmt__OpportunityId__c = testOpp_1.Id);
        insert testContract_1;

        Blob b = Blob.valueOf('Test Data');  
        Attachment attachment = new Attachment();  
        attachment.ParentId = testContract_1.Id;  
        attachment.Name = 'Test Attachment for Parent';  
        attachment.Body = b;  
        insert(attachment);  
    
        vlocity_cmt__ContractVersion__c testContractVersion = new vlocity_cmt__ContractVersion__c(Name='Test Version 1', vlocity_cmt__Status__c = 'Active', vlocity_cmt__ContractId__c = testContract_1.Id);
        insert testContractVersion;
      
        pageref.getparameters().put('id', testOpp_1.Id);
        pageref.getparameters().put('operation', 'cancelSMBOpp');
        
       
        ApexPages.StandardController sc_1 = new ApexPages.standardController(testOpp_1);
        SMB_SubmitOrderController contToTest_1 = new SMB_SubmitOrderController(sc_1);
             
             SMB_SubmitOrderController.sendAgreementDocument_vlocity(testContract_1.id, attachment.id, testContractVersion.id);
                    
                }
             
             
             
     
       
        Test.stopTest();
    }
    
      static testMethod void TestCase_13() {
        PageReference pageref = Page.SMB_OrderSuccess;
        Test.setCurrentPage(pageref);
        Test.startTest();
        TestDataHelper.testAccountCreation();
        TestDataHelper.testContactCreation();
        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'New Provide/Upgrade Order';
        insert testOpp_1;
        
      
        
  Apttus__APTS_Agreement__c agreement = new Apttus__APTS_Agreement__c(
                                                    Name = 'test Agreement for Comply Custom API 1', 
                                                    Apttus__Account__c = TestDataHelper.testAccountObj.Id, 
                                                    Apttus__Status_Category__c = 'Request', 
                                                    Apttus__Status__c = 'Request', 
                                                    Apttus__Contract_Start_Date__c = date.today(), 
                                                    Apttus__Contract_End_Date__c = date.today().addDays(365), 
                                                    Apts_Envelope_Id__c = '8fd115ee-43c2-480a-bcd3-008910ec4988', 
                                                    Printed_Agreement_Type__c ='BusinessAnywhere',
                                                    APTS_Email_Address_Customer_Auth_Contact__c = 'abc@apttus.com',
                                                    Apttus__Term_Months__c = 12, 
                                                    APTS_Customer_Signing_Authority__c = 'abc',Contract_Signor__c=TestDataHelper.testContactObj.id);

        insert agreement;
        

        Blob b = Blob.valueOf('Test Data');  
        Attachment attachment = new Attachment();  
        attachment.ParentId = agreement.Id;  
        attachment.Name = 'Test Attachment for Parent';  
        attachment.Body = b;  
        insert(attachment);  
    
        
        pageref.getparameters().put('id', testOpp_1.Id);
        pageref.getparameters().put('operation', 'cancelSMBOpp');
        
       
        ApexPages.StandardController sc_1 = new ApexPages.standardController(testOpp_1);
        SMB_SubmitOrderController contToTest_1 = new SMB_SubmitOrderController(sc_1);
             
             SMB_SubmitOrderController.sendAgreementDocument(agreement.Id, attachment.id);
       
        Test.stopTest();
    }
    
 @isTest(SeeAllData= true)
 static void TestCase_New1() {


PageReference pageref = Page.SMB_OrderSuccess;
        Test.setCurrentPage(pageref);
        Test.startTest();
        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'All Other Orders';
        insert testOpp_1;
 
        Opportunity testOpp_2   = smb_test_utility.createOpportunity('SMB_Care_Amend_Order_Opportunity', testOpp_1.AccountId, Null, false);
        testOpp_2.StageName = 'Order Request New';
        testOpp_2.Type = 'All Other Orders';
        testOpp_2.Provide_Order__c = true;
        testOpp_2.Related_Orders__c = testOpp_1.Id;
        testOpp_2.StageName = 'Test Stage';
        insert testOpp_2;
        smb_test_utility.createOrderRequest('Order_Request',testOpp_2.Id, true);


        pageref.getparameters().put('id', testOpp_2.Id);
        pageref.getparameters().put('operation', 'cancelSMBOpp');
        
        ApexPages.StandardController sc_1 = new ApexPages.standardController(testOpp_2);
        SMB_SubmitOrderController contToTest_1 = new SMB_SubmitOrderController(sc_1);
        
        contToTest_1.cancelSMBOrder();
             Test.stopTest();




}



static testmethod void TestCase_New2() {


PageReference pageref = Page.SMB_OrderSuccess;
        Test.setCurrentPage(pageref);
        Test.startTest();
        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'All Other Orders';
        insert testOpp_1;
         
          pageref.getparameters().put('id', testOpp_1.AccountId);
        pageref.getparameters().put('operation', 'cancelSMBOpp');
    
        ApexPages.StandardController sc_1 = new ApexPages.standardController(testOpp_1);
        SMB_SubmitOrderController contToTest_1 = new SMB_SubmitOrderController(sc_1);
        
        contToTest_1.cancelSMBOrder();

        contToTest_1.runActionPoller();
        contToTest_1.showSystemErrorMessage();

        
        Test.stopTest();



}

static testmethod void TestCase_New3() {


  PageReference pageref = Page.SMB_OrderSuccess;
        Test.setCurrentPage(pageref);
        Test.startTest();
        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'All Other Orders';
         testOpp_1.Amend_Stage__c = 'Order Request New';
        testOpp_1.Type = 'New Provide/Upgrade Order';
        testOpp_1.Printed_Agreement_Type__c = 'Business Anywhere';
        testOpp_1.is_Wireless_Products_Added_or_Changed__c = true;
        testOpp_1.Agreement_Required__c = 'Yes';
        insert testOpp_1;
    
    
        pageref.getparameters().put('id', testOpp_1.Id);
        pageref.getparameters().put('operation', 'cancelSMBOpp');
        
        ApexPages.StandardController sc_1 = new ApexPages.standardController(testOpp_1);
        SMB_SubmitOrderController contToTest_1 = new SMB_SubmitOrderController(sc_1);
        contToTest_1.selectFlow = 'vlocity';
        contToTest_1.SubmitOrder();
        
        Test.stopTest();


}



static testmethod void TestCase_New4() {


        Test.startTest();
    
      User userRep = TestDataHelper.createUser( 'Sales Manager / Director / MD' , 'standt1','vlocityTestUser@testuser.com','tes1','vlocityTestUser@testuser.com');
                System.RunAs(userRep){ 
                      PageReference pageref = Page.SMB_OrderSuccess;

                            Test.setCurrentPage(pageref);

                    
        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'All Other Orders';
        insert testOpp_1;
        pageref.getparameters().put('id', testOpp_1.Id);
        pageref.getparameters().put('operation', 'cancelSMBOpp');
        
        ApexPages.StandardController sc_1 = new ApexPages.standardController(testOpp_1);
        SMB_SubmitOrderController contToTest_1 = new SMB_SubmitOrderController(sc_1);
        
        SMB_SubmitOrderController.getCPQQuoteCancellationHistory(String.valueof(testOpp_1.Id));
        contToTest_1.SubmitOrder();
                    
                }
        Test.stopTest();


}


static testmethod void TestCase_New5() {


  PageReference pageref = Page.SMB_OrderSuccess;
        Test.setCurrentPage(pageref);
        Test.startTest();
        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'All Other Orders';
        insert testOpp_1;
        pageref.getparameters().put('id', testOpp_1.Id);
        pageref.getparameters().put('operation', 'cancelSMBOpp');
        
        ApexPages.StandardController sc_1 = new ApexPages.standardController(testOpp_1);
        SMB_SubmitOrderController contToTest_1 = new SMB_SubmitOrderController(sc_1);
        
        SMB_SubmitOrderController.logCPQCancellation(String.valueof(testOpp_1.Id), '"status":200' , 'Test');
        contToTest_1.CancelSMBCareOrder();
        Test.stopTest();


}


}