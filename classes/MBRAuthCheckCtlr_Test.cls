/**
* @author Christian Wico - cwico@tractionondemand.com
* @description Unit Test for MBRAuthCheckCtlr.cls
*/
@isTest(SeeAllData=false)
private class MBRAuthCheckCtlr_Test {
	
	@isTest static void testNonPortal() {
		MBRAuthCheckCtlr ctlr = new MBRAuthCheckCtlr();
		System.assert(ctlr.initCheck().getUrl().containsIgnoreCase('NonPortal'));
	}

	@isTest static void testGuestUser() {
        System.runAs(MBRTestUtils.createGuestUser()) {
        	Test.setCurrentPage(Page.MBROverview);
            MBRAuthCheckCtlr ctlr = new MBRAuthCheckCtlr();
            System.debug(ctlr.initCheck().getUrl());
			System.assert(ctlr.initCheck().getUrl().containsIgnoreCase('login'));
        }

	}
	
	@isTest static void testValidUser() {
		System.runAs(MBRTestUtils.createPortalUser('')) {
        	Test.setCurrentPage(Page.MBROverview);
            MBRAuthCheckCtlr ctlr = new MBRAuthCheckCtlr();
			System.assert(ctlr.initCheck() == null);
        }
	}

    /* VITILcare portal	*/
	@isTest static void VITILcare_testNonPortal() {
		VITILcareAuthCheckCtlr ctlr = new VITILcareAuthCheckCtlr();
		System.assert(ctlr.initCheck().getUrl().containsIgnoreCase('NonPortal'));
	}

	@isTest static void VITILcare_testGuestUser() {
        System.runAs(MBRTestUtils.createGuestUser()) {
        	Test.setCurrentPage(Page.VITILcareOverview);
            VITILcareAuthCheckCtlr ctlr = new VITILcareAuthCheckCtlr();
            System.debug(ctlr.initCheck().getUrl());
			System.assert(ctlr.initCheck().getUrl().containsIgnoreCase('login'));
        }

	}
	
	@isTest static void VITILcare_testValidUser() {
		System.runAs(MBRTestUtils.createPortalUser('VITILcare')) {
        	Test.setCurrentPage(Page.VITILcareOverview);
            VITILcareAuthCheckCtlr ctlr = new VITILcareAuthCheckCtlr();
			System.assert(ctlr.initCheck() == null);
        }
	}
}