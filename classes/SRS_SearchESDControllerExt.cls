public with sharing class SRS_SearchESDControllerExt {
	public SRS_SearchESDControllerExt() {
		
	}

	public static void createServiceAddress(SRS_SearchESDController.Wrapper esdWrapper, Id serviceId, Id acctId, String demarcationLocation, String endCustomerName, String npaNXX,
		String location, String cli, String existingWorkingTN, String siteReady, String buildingName, String newBuidling, String floor, String cabinet, String rack, String shelf, String postalCode){
			SMBCare_Address__c smbCareAddress = new SMBCare_Address__c(
				Account__c = acctId,
				Street_Name__c = esdWrapper.service_location,
				City__c = esdWrapper.city,
				Province__c = esdWrapper.pr_st,
				Postal_Code__c =  postalCode
			);

			SYstem.debug('Address :::: ' + smbCareAddress);

			insert smbCareAddress;

			SRS_Service_Address__c newServiceAddress = new SRS_Service_Address__c(
				Service_Request__c = serviceId,
				Address__c = smbCareAddress.Id,
				Demarcation_Location__c = demarcationLocation,
				NPA_NXX__c = npaNXX,
				Floor__c = floor,
				Location__c = (String.isNotBlank(location)) ? location : 'Location A',
				New_Building__c = newBuidling,
				Rack__c = rack,
				Shelf__c = shelf,
				Site_Ready__c = siteReady,
				CLLI__c = cli,
				Cabinet__c = cabinet,
				Building_Name__c = buildingName,
				Existing_Working_TN__c = existingWorkingTN,
				End_Customer_Name__c = endCustomerName
			);

			SYstem.debug('S ADdress :::: ' + newServiceAddress);

			insert newServiceAddress;
	}

	public static Id retrieveProductId(String productName){
		List<SRS_PODS_Product__c> existingProductList;
		Id productId = null;

		if(String.isNotBlank(productName)){
			existingProductList = [SELECT Id FROM SRS_PODS_Product__c WHERE Name = :productName LIMIT 1];

			if(!existingProductList.isEmpty()){
				productId = existingProductList[0].Id;
			}
		}

		return productId;
	}
}