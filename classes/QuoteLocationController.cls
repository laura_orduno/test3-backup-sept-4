public class QuoteLocationController {
    public Address__c address {get; private set;}
    public Id accountId {get; set;}
    public Boolean dealerUser {get{return QuotePortalUtils.isUserDealer();}}
    
    private List<Address__c> addresses;
    public Integer addressCount {get; private set;}
    private String returnURL;
    
    public QuoteLocationController() {
        Id id = ApexPages.currentPage().getParameters().get('id');
        if (id != null) {
            address = QuoteUtilities.loadAddressById((Id)id);
        } else {
            accountId = ApexPages.currentPage().getParameters().get('aid');
            address = new Address__c(Web_Account__c=accountId,RecordTypeId='012400000005WIh');
        }
        returnURL = ApexPages.currentPage().getParameters().get('retURL');
    }
    
    public List<Address__c> getAddresses() {
        if (accountId != null) {
            addresses = QuoteUtilities.loadAddressesByAccountId(accountId);
            addressCount = addresses.size();
        }
        return addresses;
    }
    
    public PageReference onSave() {
        try {
            upsert address;
        } catch (Exception e) {
            ApexPages.addMessages(e);
            return null;
        }
        return new PageReference(returnURL);
    }
    
    public PageReference onCancel() {
        return new PageReference(returnURL);
    }
}