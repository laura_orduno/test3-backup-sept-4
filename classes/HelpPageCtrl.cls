public class HelpPageCtrl {
    
    public PageReference redirect(){
        String passedParam1 = Apexpages.currentPage().getParameters().get('objectName');
        Help_Pages__c fieldMap =null;
        if(!Test.isrunningtest()){
        	fieldMap = Help_Pages__c.getValues(passedParam1);    
        }else{
            fieldMap = new Help_Pages__c(Name='Name', Help_URL__c='/abs');
        }
        PageReference pr = new PageReference(fieldMap.Help_URL__c);
        pr.setRedirect(true);
        return pr;
    }
}