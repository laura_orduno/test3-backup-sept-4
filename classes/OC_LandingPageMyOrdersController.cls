/*
    *******************************************************************************************************************************
    Class Name:     OC_LandingPageMyOrders
    Purpose:        BSBD-RTA-407
                    1) As a Sales rep, ability to view all orders that I have created on My order list view.
                    2) As a Sales rep, ability to view all orders created on my behalf on My order list view (based on Sales Rep field)    
    Test Class Name : OC_LandingPageMyOrdersTest
    Created by:      Sandip Chaudhari        06-Nov-2017     
  
    *******************************************************************************************************************************
*/
global without sharing class OC_LandingPageMyOrdersController{
     global OC_LandingPageMyOrdersController(){
         String keyStr = apexpages.currentpage().getparameters().get('key');
         System.debug('keyStr='+keyStr);
     }
     
     @RemoteAction
     global static List<OrderDetails> getMyOrders(){         
         List<OrderDetails> orderDetails = new List<OrderDetails>();
         List<Order> myOrders = new List<Order>();
         Id currentUserId = UserInfo.getUserId();
         List<Id> salesRepIds = DealerPortalUtil.getDealerSalesRepIdsFromCache();
         User userObj = [SELECT Id, Name, profile.Name, UserRole.Name FROM User WHERE Id=:userInfo.getUserId() LIMIT 1];
		 String roleName = userObj.UserRole.Name;
         System.debug('@@@ roleName getMyOrder' + roleName);
         System.debug('@@@ SalesRepList' + salesRepIds);
         Set<String> dealeradminPricipalRoleSet = DealerPortalUtil.getDealerAdminPricipalRoles();
         System.debug('@@@ dealeradminPricipalRoleSet ' + dealeradminPricipalRoleSet);
         if(dealeradminPricipalRoleSet.contains(roleName)){
             System.debug('@@@ In Dealer Principal');
             myOrders = [SELECT Id, Master_Order_Status__c, AccountID,Account.Name, Credit_Assessment__c, Credit_Assessment__r.Name, Status, OrderNumber, OwnerId, Owner.FirstName, Owner.LastName, 
                         Sales_Representative__r.FirstName,
                         Sales_Representative__r.LastName,Condition__c,
                         CreatedDate, vlocity_cmt__DueDate__c, Credit_Assessment_Status__c, LastModifiedDate,
                         ParentId__c
                         FROM ORDER
                         WHERE 
                         //ParentId__c = null AND commented as per BSBD_RTA-1191 to get child orders for Sales Rep
                         (ownerId =: currentUserId OR Sales_Representative__c =:currentUserId OR ownerId IN:salesRepIds OR Sales_Representative__c IN: salesRepIds)
                         ORDER BY LastModifiedDate DESC Limit 10000];
         }else{
             System.debug('@@@ In Sales Rep currentUserId ' + currentUserId);
             // Query to get parent order using salesrep as SalesRep exists on child level only
             List<Order> tmpOrderList = [Select Id, ParentId__c from Order Where Sales_Representative__c=: currentUserId];
             List<Id> tmpParentIdList = new List<Id>();
             if(tmpOrderList != null){
                 for(Order tmpOrder: tmpOrderList){
                     tmpParentIdList.add(tmpOrder.ParentId__c);
                 }
             }
             // End to get parent order logic and use the same in below one
             myOrders = [SELECT Id, Master_Order_Status__c, AccountID,Account.Name, Credit_Assessment__c, Credit_Assessment__r.Name, Status, OrderNumber, OwnerId, Owner.FirstName, Owner.LastName, 
                         Sales_Representative__r.FirstName,
                         Sales_Representative__r.LastName,Condition__c,
                         CreatedDate, vlocity_cmt__DueDate__c, Credit_Assessment_Status__c, LastModifiedDate,
                         ParentId__c
                         FROM ORDER
                         WHERE 
                         //ParentId__c = null AND commented as per BSBD_RTA-1191 to get child orders for Sales Rep
                         (ownerId =: currentUserId OR Sales_Representative__c =:currentUserId OR Id IN:tmpParentIdList)
                         ORDER BY LastModifiedDate DESC Limit 10000];
         }
         
          if(myOrders != null && myOrders.size()>0){
             Map<Id, Set<String>> salesRepMap = new Map<Id, Set<String>>(); // added as per BSBD_RTA-1191 to get child orders for Sales Rep
              // added as per BSBD_RTA-1191 to get child orders for Sales Rep
              List<Order> myMasterOrders = new List<Order>();
              for(Order orderObj: myOrders){
                 if(orderObj.ParentId__c != null){
                     Set<String> tmpSet = new Set<String>();
                     if(salesRepMap.containsKey(orderObj.Id)){
                         tmpSet = salesRepMap.get(orderObj.Id);
                     }
                     tmpSet.add(orderObj.Sales_Representative__r.FirstName + ' ' + orderObj.Sales_Representative__r.LastName);
                     salesRepMap.put(orderObj.ParentId__c, tmpSet);
                 }else{
                     myMasterOrders.add(orderObj);
                 }
              }
              // End BSBD_RTA-1191 to get child orders for Sales Rep
              for(Order orderObj: myMasterOrders){
                 OrderDetails orderDtlObj = new OrderDetails();
                 orderDtlObj.orderId = orderObj.Id;
                 orderDtlObj.accountId = orderObj.AccountID;
                 orderDtlObj.accountName = orderObj.Account.Name;
                 orderDtlObj.carId = orderObj.Credit_Assessment__c;
                 orderDtlObj.carName = orderObj.Credit_Assessment__r.Name;
                 orderDtlObj.carStatus = orderObj.Credit_Assessment_Status__c;
                 //orderDtlObj.status = orderObj.status;
                 orderDtlObj.status = orderObj.Master_Order_Status__c; // BSBD_RTA-1143
                 orderDtlObj.orderNumber = orderObj.orderNumber;
                 orderDtlObj.ownerId = orderObj.ownerId;
                 orderDtlObj.ownerFirstName = orderObj.Owner.FirstName;
                 orderDtlObj.ownerLastName = orderObj.Owner.LastName;
                 // added as per BSBD_RTA-1191 to get child orders for Sales Rep
                 System.debug('@@@ salesRepMap for ' + orderObj.Id + ' is ' + salesRepMap);
                 if(salesRepMap.get(orderObj.Id) != null){
                     Set<String> salesRepSet = salesRepMap.get(orderObj.Id);
                     String saleRepString = '';
                     if(salesRepSet != null && salesRepSet.size()>0){
                         for(String s:salesRepSet) {
                             saleRepString += (saleRepString==''?'':',')+s;
                         }
                     }
                     orderDtlObj.allChildOrderSaleRep = saleRepString;
                     System.debug('@@@ allChildOrderSaleRep ' + orderDtlObj.allChildOrderSaleRep);
                 }
                 if(String.isBlank(orderDtlObj.allChildOrderSaleRep)){
                     orderDtlObj.saleRepFirstName = orderObj.Sales_Representative__r.FirstName;
                 	 orderDtlObj.saleRepLastName = orderObj.Sales_Representative__r.LastName;
                     orderDtlObj.allChildOrderSaleRep = orderDtlObj.saleRepFirstName + ' ' + orderDtlObj.saleRepLastName;
                 }
                 //End BSBD_RTA-1191 to get child orders for Sales Rep
                 orderDtlObj.condition = orderObj.Condition__c;
                 orderDtlObj.createdDate = orderObj.CreatedDate;
                 orderDtlObj.dueDate = orderObj.vlocity_cmt__DueDate__c;
                 orderDtlObj.lastModifiedDate= orderObj.LastModifiedDate;
                 orderDetails.add(orderDtlObj);
             }
         }
         return orderDetails;
     }
    
      global Class OrderDetails{
         public Id orderId{get;set;}
         public String accountId{get;set;}
         public string accountName{get;set;}
         public String carId{get;set;}
         public string carName{get;set;}
         public string carStatus{get;set;}
         public string status{get;set;}
         public string orderNumber{get;set;}
         public string ownerId{get;set;}
         public string ownerFirstName{get;set;}
         public string ownerLastName{get;set;}
         public string saleRepFirstName{get;set;}
         public string saleRepLastName{get;set;}
         public string allChildOrderSaleRep{get;set;} //added as per BSBD_RTA-1191 to get child orders for Sales Rep
         public string condition{get;set;}
         public DateTime createdDate{get;set;}
         public DateTime dueDate{get;set;}
         public DateTime lastModifiedDate{get;set;}
     }
}