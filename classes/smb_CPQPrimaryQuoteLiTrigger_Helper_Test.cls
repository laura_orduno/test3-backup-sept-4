@isTest(seeAllData = true)
private class smb_CPQPrimaryQuoteLiTrigger_Helper_Test {

    static testMethod void Test_PQLI() {
        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'All Other Orders';
        insert testOpp_1;
        OpportunityContactRole opprole = new OpportunityContactRole(opportunityID = testOpp_1.Id,contactId = testOpp_1.Contract_Signor__c,isPrimary = true);
        insert opprole;
        system.debug(opprole);
        Test.startTest();
        smb_test_utility.createPQLI('PQL_Test_Data',testOpp_1.Id);
        list<scpq__PrimaryQuoteLine__c> listPqli = [Select id from scpq__PrimaryQuoteLine__c where scpq__OpportunityId__c =:testOpp_1.Id];
        delete listPqli;
        //undelete listPqli;
        Test.stopTest();
    }
    static testMethod void Test_PQL2() {
        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'New Provide/Upgrade Order';
        insert testOpp_1;
        OpportunityContactRole opprole = new OpportunityContactRole(opportunityID = testOpp_1.Id,contactId = testOpp_1.Contract_Signor__c,isPrimary = true);
        insert opprole;
        system.debug(opprole);
        smb_test_utility.createPQLI('NewPQL',testOpp_1.Id);
        Test.startTest();
        Work_Order__c workOrder = new Work_Order__c(Opportunity__c=testOpp_1.id,Job_Type__c = 'Test' ,Install_Type__c = 'Fieldwork', Status__c = 'Reserved', Product_Name__c = 'Business Anywhere Plus Office Internet',
        						 Scheduled_Datetime__c = DateTime.now() , Time_Slot__c = 'Test Slot', Unlinked_Work_Order__c = false ,WFM_Number__c = 'Test Number', Scheduled_Due_Date_Location__c = 'Test Location', Permanent_Comments__c = 'test test');
        insert workOrder;
        Work_Order__c workOrder2 = new Work_Order__c(Opportunity__c=testOpp_1.id,Job_Type__c = 'Test' ,Install_Type__c = 'Fieldwork', Status__c = 'Reserved', Product_Name__c = 'New ADSL No Local Voice Line',
        						 Scheduled_Datetime__c = DateTime.now() , Time_Slot__c = 'Test Slot', Unlinked_Work_Order__c = false ,WFM_Number__c = 'Test Number', Scheduled_Due_Date_Location__c = 'Test Location', Permanent_Comments__c = 'test test');
        insert workOrder2;
        
        list<OpportunityLineItem> oliList = [Select id, Work_Order__c from OpportunityLineItem where OpportunityId = :testOpp_1.Id AND Item_ID__c IN ('BSNSSNYWHRFFCNTRNT-TL-I','NWDSLNLCLVCLN-TL-I') Order by LineNo__c];
        if(oliList != null && oliList.size() > 1)
        {
        	oliList[0].Work_Order__c = workOrder.Id;
        	oliList[1].Work_Order__c = workOrder2.Id;
        	update oliList;
        }
        list<scpq__PrimaryQuoteLine__c> listPqli = [Select id from scpq__PrimaryQuoteLine__c where scpq__OpportunityId__c =:testOpp_1.Id];
        delete listPqli;
        smb_test_utility.createPQLI('NewPQL',testOpp_1.Id);
        //list<scpq__PrimaryQuoteLine__c> listPqli = [Select id from scpq__PrimaryQuoteLine__c where scpq__OpportunityId__c =:testOpp_1.Id];
        //delete listPqli;
        //undelete listPqli;
        Test.stopTest();
    }
}