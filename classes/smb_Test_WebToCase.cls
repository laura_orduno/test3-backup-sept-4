/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class smb_Test_WebToCase 
{
    
    static testMethod void webToCaseMultipleContactsTest()
    {
        set<Id> accIdSet = new set<Id>();
        /*
        for(RecordType rt : [SELECT Id, Name,DeveloperName from RecordType WHERE SobjectType = 'Account'])
        {
            accRTMap.put(rt.Name, rt);
        }
*/
        map<string,schema.recordtypeinfo> accRTMap=Schema.sobjecttype.Account.getRecordTypeInfosByName();
        
        id caseRTId = [SELECT Id FROM RecordType WHERE SobjectType = 'Case' and Name ='SMB Care L&R Web Form' LIMIT 1].get(0).id;
        
        // build Test Data

        list<Account> accList = new List<Account>();
        list<Account> Lev1List = new List<Account>();
        
        Account rcidAccount3 = createTestAccount('RCID 3','(010) 111 0001',accRTMap.get('RCID'),null,false,'0000001113',false);
        accList.add(rcidAccount3);
        Lev1List.add(rcidAccount3);
        
        insert Lev1List;
        
        list<Contact> contactList = new list<Contact>();
        for(integer i=0;i<1500;i++)
        {
            contactList.add(createTestContact('RCID' + i,'1234' + i,rcidAccount3.Id,'RCID' + i + '@test.com.test',null,false));
        }
        insert contactList;
        //insert accList;
        
        for(Account acc : accList)
        {
            accIdSet.add(acc.id);
        }
        List<Id> lstId = new list<Id>();
        
        for(Contact c: contactList)
        {
            lstId.add(c.id);
        }
        
        list<Case> caseList = new list<Case>();
          
        
        
        caseList.add(createTestWebCase('0000001113','Telephone Number:0101110011','DR PK','555','pkpk@test.com.test',false,caseRTId));
       
        insert caseList;
        
        Test.startTest();
        
        
        // Coverage for Controller
        //------------------------
        
        
       
        lstId.AddAll(accIdSet);
        
        Test.setFixedSearchResults(lstId);
        
         Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(caseList.get(0));
        smb_AssignAccountToWebCaseExtension wcController = new smb_AssignAccountToWebCaseExtension(controller);
        
        
        ApexPages.currentPage().getParameters().put('selectedAccountId', rcidAccount3.id);
        wcController.selectAccount();
        test.stoptest();
		system.debug('limits after webToCaseMultipleContactsTest:'+ limits.getAggregateQueries());
    }
    
    
    static testMethod void webToCaseSearchTest()
    {
        set<Id> accIdSet = new set<Id>();
        /*
        for(RecordType rt : [SELECT Id, Name,DeveloperName from RecordType WHERE SobjectType = 'Account'])
        {
            accRTMap.put(rt.Name, rt);
        }
        */
        map<string,schema.recordtypeinfo> accRTMap=Schema.sobjecttype.Account.getRecordTypeInfosByName();

        id caseRTId = [SELECT Id FROM RecordType WHERE SobjectType = 'Case' and Name ='SMB Care L&R Web Form' LIMIT 1].get(0).id;
        
        // build Test Data

        list<Account> accList = new List<Account>();
        list<Account> Lev1List = new List<Account>();
        
        Account rcidAccount3 = createTestAccount('RCID 3','(010) 111 0001',accRTMap.get('RCID'),null,false,'0000001113',false);
        accList.add(rcidAccount3);
        Lev1List.add(rcidAccount3);
        Account rcidAccount2 = createTestAccount('RCID 2','(010) 111 0011',accRTMap.get('RCID'),null,false,'0000001114',false);
        accList.add(rcidAccount2);
        Lev1List.add(rcidAccount2);

        
        insert Lev1List;
        
        list<Asset> assetList = new list<Asset>();
        assetList.add(createTestAsset(rcidAccount2.Id,'(000) 000 1113',true));
                
        //insert accList;
        
        for(Account acc : accList)
        {
            accIdSet.add(acc.id);
        }

        list<Case> caseList = new list<Case>();
          
        
        // No Email - No RCID 
        caseList.add(createTestWebCase('',':0101110011','DR PK','555','pkpk@test.com.test',false,caseRTId));
        caseList.add(createTestWebCase('',':0101110011','PK','','',false,caseRTId));
        
        
        insert caseList;
        
        Test.startTest();
        
        
        // Coverage for Controller
        //------------------------
        
        
        List<Id> lstId = new list<Id>();
        lstId.AddAll(accIdSet);
        lstId.add(assetList.get(0).id);
        
        Test.setFixedSearchResults(lstId);
        
         Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(caseList.get(0));
        smb_AssignAccountToWebCaseExtension wcController = new smb_AssignAccountToWebCaseExtension(controller);
        list<SelectOption> caseRTTypes = wcController.caseRecordTypes;
        
        
        wcController.searchText = '(010) 111 0011';
        wcController.findAccounts();
        
        wcController.searchText = '0000001113';
        wcController.findAccounts();
        ApexPages.currentPage().getParameters().put('selectedAccountId', wcController.accountResults.get(0).id);
        wcController.selectAccount();
        wcController.showContactForm();
        wcController.newContact.Language_Preference__c = 'English';
        //wcController.newContact.email = 'pk@test.com.test';
        wcController.createNewContact();
        wcController.searchText = 'DR PK';
        lstId.Add(wcController.newContact.id);
        Test.setFixedSearchResults(lstId);
        
        wcController.findContacts();
        ApexPages.currentPage().getParameters().put('selectedContactId', wcController.newContact.id);
        wcController.selectContact();
        
        
        
        controller = new Apexpages.Standardcontroller(caseList.get(1));
        wcController = new smb_AssignAccountToWebCaseExtension(controller);
                
        ApexPages.currentPage().getParameters().put('selectedAccountId', rcidAccount3.id);
        wcController.selectAccount();wcController.selectAccount();
        wcController.showContactForm();
        wcController.newContact.Language_Preference__c = 'English';
        //wcController.newContact.email = 'pk@test.com.test';
        wcController.createNewContact();
        wcController.searchText = 'PK';
        lstId.Add(wcController.newContact.id);
        Test.setFixedSearchResults(lstId);
        wcController.findContacts();
        wcController.selectedAccountId = null;

        ApexPages.currentPage().getParameters().put('selectedContactId', wcController.newContact.id);
        wcController.selectContact();
        
        
        Test.stopTest();
		system.debug('limits after webToCaseSearchTest:'+ limits.getAggregateQueries());
    }
    
    
    static testMethod void webToCaseControllerTest()
    {
		system.debug('limits before webToCaseControllerTest:'+ limits.getAggregateQueries());
        set<Id> accIdSet = new set<Id>();
        /*map<string,RecordType> accRTMap= new map<String, RecordType>();
        
        for(RecordType rt : [SELECT Id, Name,DeveloperName from RecordType WHERE SobjectType = 'Account'])
        {
            accRTMap.put(rt.Name, rt);
        }*/
        map<string,schema.recordtypeinfo> accRTMap=Schema.sobjecttype.Account.getRecordTypeInfosByName();
        
//        Case testCase = new Case();
//        testCase.recordTypeId = ;
        
        id caseRTId = [SELECT Id FROM RecordType WHERE SobjectType = 'Case' and Name ='SMB Care L&R Web Form' LIMIT 1].get(0).id;
        
        // build Test Data
        /*
            cbucAccount
                - rcidAccount
                    - canAccount
                        - canAccount1
                        - canAccount3
                            - canAccount4
                    - banAccount
                - rcidAccount2
            canAccount2
            banAccount2
            rcidAccount3
            banAccount4
                - banAccount5
                    - banAccount6
                        -banAccount7
        */
        list<Account> accList = new List<Account>();
        list<Account> Lev1List = new List<Account>();
        
        Profile p = [SELECT id FROM Profile where Name = 'System Integration' LIMIT 1].get(0);
        User u = new User(Alias = 'integt2', Email='integrationuser2@testorg.com', 
              EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
              LocaleSidKey='en_US', ProfileId = p.Id, 
              TimeZoneSidKey='America/Los_Angeles', UserName='integrationuser2@testorg.com');
        
        insert u;
              
        Profile p2 = [SELECT id FROM Profile where Name = 'System Administrator' LIMIT 1].get(0);
        User u2 = new User(Alias = 'testpk2', Email='testpkstandarduser2@testorg.com', 
              EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
              LocaleSidKey='en_US', ProfileId = p2.Id, 
              TimeZoneSidKey='America/Los_Angeles', UserName='testpkstandarduser2@testorg.com');
        
        insert u2; 
         
        System.RunAs(u)
        {

            Account cbucAccount = createTestAccount('CBUC','(000) 111 0001',accRTMap.get('CBUCID'),null,false,'0000000001',false);
            accList.add(cbucAccount);
            Lev1List.add(cbucAccount);
            Account canAccount2 = createTestAccount('CAN 2','(000) 111 0002',accRTMap.get('CAN'),null,false,'00444',false);
            accList.add(canAccount2);
            Lev1List.add(canAccount2);
            Account banAccount2 = createTestAccount('BAN 2','(000) 111 0003',accRTMap.get('BAN'),null,true,'004',false);
            accList.add(banAccount2);
            Lev1List.add(banAccount2);
            Account rcidAccount3 = createTestAccount('RCID 3','(010) 111 0001',accRTMap.get('RCID'),null,false,'0000001113',false);
            accList.add(rcidAccount3);
            Lev1List.add(rcidAccount3);
            Account banAccount4 = createTestAccount('BAN 4','(011) 222 0001',accRTMap.get('BAN'),null,false,'005',false);
            accList.add(banAccount4);
            Lev1List.add(banAccount4);
            
            insert Lev1List;
            Lev1List = new List<Account>();
            
            Account rcidAccount = createTestAccount('RCID','(000) 111 1001',accRTMap.get('RCID'),cbucAccount.Id,false,'0000001111',false);
            accList.add(rcidAccount);
            Lev1List.add(rcidAccount);
            Account rcidAccount2 = createTestAccount('RCID 2','(010) 111 0001',accRTMap.get('RCID'),cbucAccount.Id,false,'0000001112',false);
            accList.add(rcidAccount2);
            Lev1List.add(rcidAccount2);
            Account banAccount5 = createTestAccount('BAN 5','(011) 222 0011',accRTMap.get('BAN'),banAccount4.Id,false,'0055',false);
            accList.add(banAccount5);
            Lev1List.add(banAccount5);
            
            insert Lev1List;
            Lev1List = new List<Account>();
            
            
            Account canAccount = createTestAccount('CAN','(000) 111 2101',accRTMap.get('CAN'),rcidAccount.Id,false,'1-001',false);
            accList.add(canAccount);
            Lev1List.add(canAccount);
            Account banAccount = createTestAccount('BAN','(001) 333 0001',accRTMap.get('BAN'),rcidAccount.Id,false,'0044',false);
            accList.add(banAccount);
            Lev1List.add(banAccount);
            Account banAccount6 = createTestAccount('BAN 6','(011) 222 0201',accRTMap.get('BAN'),banAccount5.Id,false,'00555',false);
            accList.add(banAccount6);
            Lev1List.add(banAccount6);
            
            insert Lev1List;
            Lev1List = new List<Account>();
            
            Account canAccount1 = createTestAccount('CAN 1','(000) 111 2001',accRTMap.get('CAN'),canAccount.Id,false,'002',false);
            accList.add(canAccount1);
            Lev1List.add(canAccount1);
            Account canAccount3 = createTestAccount('CAN 3','(000) 222 0001',accRTMap.get('CAN'),canAccount.Id,true,'003',false);
            accList.add(canAccount3);
            Lev1List.add(canAccount3);
            Account banAccount7 = createTestAccount('BAN 7','(011) 122 2001',accRTMap.get('BAN'),banAccount6.Id,false,'0055-5',false);
            accList.add(banAccount7);
            Lev1List.add(banAccount7);
            
            insert Lev1List;
            Lev1List = new List<Account>();
            
            Account canAccount4 = createTestAccount('CAN 4','(001) 333 0001',accRTMap.get('CAN'),canAccount3.Id,true,'00471',false);
            accList.add(canAccount4);
            Lev1List.add(canAccount4);
           
            insert Lev1List;
            
            System.runAs(u2)
            {
        
                //insert accList;
                
                for(Account acc : accList)
                {
                    accIdSet.add(acc.id);
                }
                
                list<Web_Account__c> webAccList = new List<Web_Account__c>();
                for(integer i=0; i<3; i++)
                {
                    webAccList.add(createWebAccount('Test ' + i,false));
                }
                insert webAccList;
                
                System.debug('### rcidAccount ' + rcidAccount ); 
                
                list<Contact> contactList = new List<Contact>();
                
                contactList.add(createTestContact('CBUC','(222) 222 0000',cbucAccount.Id,'cbuc@test.com.test',null,false));
                contactList.add(createTestContact('CBUC2','(222) 222 0011',cbucAccount.Id,'cbuc2@test.com.test',null,false));
                contactList.add(createTestContact('RCID','(222) 222 1100',rcidAccount.Id,'rcid@test.com.test',null,false));
                contactList.add(createTestContact('RCID2','(222) 222 0001',rcidAccount.Id,'rcid2@test.com.test',null,false));
                contactList.add(createTestContact('RCID3','(222) 222 3333',rcidAccount.Id,'rcid3@test.com.test',null,false));
                contactList.add(createTestContact('CAN','(222) 222 0002',canAccount.Id,'can@test.com.test',null,false));
                contactList.add(createTestContact('CAN2','(222) 222 0033',canAccount.Id,'can2@test.com.test',null,false));
                contactList.add(createTestContact('CAN3 1','(222) 222 0000',canAccount3.Id,'can3_1@test.com.test',null,false));
                contactList.add(createTestContact('BAN','(222) 222 0000',banAccount.Id,'ban@test.com.test',null,false));
                contactList.add(createTestContact('BAN2','(222) 333 0000',banAccount.Id,'ban2@test.com.test',null,false));
                contactList.add(createTestContact('RCID 2','(222) 111 0000',rcidAccount2.Id,'rcid2@test.com.test',null,false));
                contactList.add(createTestContact('CAN 2','(222) 222 1111',canAccount2.Id,'can_2@test.com.test',null,false));
                contactList.add(createTestContact('BAN 2','(222) 222 4444',banAccount2.Id,'ban_2@test.com.test',null,false));
                contactList.add(createTestContact('CAN4 1','(222) 222 1122',canAccount2.Id,'can4_1@test.com.test',null,false));
                contactList.add(createTestContact('BAN7 1','(222) 222 2222',banAccount7.Id,'ban7_1@test.com.test',null,false));
                contactList.add(createTestContact('BAN6 1','(222) 222 2222',banAccount6.Id,'ban6_1@test.com.test',null,false));
                contactList.add(createTestContact('BAN5 1','(222) 222 2222',banAccount6.Id,'ban5_1@test.com.test',null,false));
                contactList.add(createTestContact('RCID4 Web Account','(222) 222 3333',rcidAccount.Id,'rcid3@test.com.test',webAccList.get(0).id,false));
                
                database.insert(contactList);
                
                
                set<Id> conIdSet = new Set<Id>();
                
                for(Contact con : contactList)
                {
                    conIdSet.add(con.id);
                }
                
                list<Case> caseList = new list<Case>();
                // Exact Match Email - Non RCID
                caseList.add(createTestWebCase('0000001112','Telephone Number : (222) 222 1122','BAN 2','(222) 222 1122','ban2@test.com.test',false,caseRTId));
                
                // Exact Match Email - No RCID - Consumer True
                caseList.add(createTestWebCase('1112','BAN : 004','BAN 2','(222) 222 1122','ban_2@test.com.test',false,caseRTId));
                
                // Exact Match Email - No RCID - Consumer False
                caseList.add(createTestWebCase('1112','CAN : 004','BAN 2','(222) 222 1122','can_2@test.com.test',false,caseRTId));
                
                
                // No Email - No RCID 
                caseList.add(createTestWebCase('','CAN : 00444','DR PK','555',null,false,caseRTId));
              
                list<Asset> assetList = new list<Asset>();
                assetList.add(createTestAsset(canAccount1.Id,'2222221122',true));
                
                insert caseList;
                //Test.startTest();
                
                //Test.stopTest();
                
                Test.startTest();
                
                
                // Coverage for Controller
                //------------------------
                
                List<Id> lstId = new list<Id>();
                lstId.AddAll(accIdSet);
                lstId.AddAll(conIdSet);
                lstId.Add(assetList.get(0).Id);
                
                Test.setFixedSearchResults(lstId);
                
                Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(caseList.get(0));
                smb_AssignAccountToWebCaseExtension wcController = new smb_AssignAccountToWebCaseExtension(controller);
                ApexPages.currentPage().getParameters().put('selectedAccountId', wcController.accountResults.get(0).id);
                wcController.selectAccount();
                ApexPages.currentPage().getParameters().put('selectedContactId', wcController.contactResults.get(0).id);
                wcController.selectContact();
                wcController.selectedRecordTypeId = 'CHOOSEAGAIN';
                wcController.saveCase();
                wcController.reset();
                
                controller = new Apexpages.Standardcontroller(caseList.get(1));
                wcController = new smb_AssignAccountToWebCaseExtension(controller);
                ApexPages.currentPage().getParameters().put('selectedAccountId', String.ValueOf(banAccount2.Id));
                wcController.selectAccount();
                ApexPages.currentPage().getParameters().put('selectedContactId', wcController.contactResults.get(0).id);
                wcController.selectContact();
        
                
                controller = new Apexpages.Standardcontroller(caseList.get(2));
                wcController = new smb_AssignAccountToWebCaseExtension(controller);
                ApexPages.currentPage().getParameters().put('selectedAccountId', canAccount2.Id);
                wcController.selectAccount();
                ApexPages.currentPage().getParameters().put('selectedContactId', wcController.contactResults.get(0).id);
                wcController.selectContact();
                
                //------------------------
                
                Test.stopTest();
            }
        }
		system.debug('limits after webToCaseControllerTest:'+ limits.getAggregateQueries());
    }
   
    static testMethod void webToCaseTest()
    {
		system.debug('limits before webToCaseTest:'+ limits.getAggregateQueries());
        //list<Account> accountsList = new List<Account>();
        //SobjectType = 'Case' and Name ='SMB Care L&R Web Form'
        set<Id> accIdSet = new set<Id>();
        /*
        map<string,RecordType> accRTMap= new map<String, RecordType>();
        for(RecordType rt : [SELECT Id, Name,DeveloperName from RecordType WHERE SobjectType = 'Account'])
        {
            accRTMap.put(rt.Name, rt);
        }
        */
        map<string,schema.recordtypeinfo> accRTMap=Schema.sobjecttype.Account.getRecordTypeInfosByName();
        
        id caseRTId = [SELECT Id FROM RecordType WHERE SobjectType = 'Case' and Name ='SMB Care L&R Web Form' LIMIT 1].get(0).id;
        
        // build Test Data
        /*
            cbucAccount
                - rcidAccount
                    - canAccount
                        - canAccount1
                        - canAccount3
                            - canAccount4
                    - banAccount
                - rcidAccount2
            canAccount2
            banAccount2
            rcidAccount3
            banAccount4
                - banAccount5
                    - banAccount6
                        -banAccount7
        */
        list<Account> accList = new List<Account>();
        list<Account> Lev1List = new List<Account>();
        
        
        Profile p = [SELECT id FROM Profile where Name = 'System Integration' LIMIT 1].get(0);
        User u = new User(Alias = 'integt1', Email='integrationuser1@testorg.com', 
              EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
              LocaleSidKey='en_US', ProfileId = p.Id, 
              TimeZoneSidKey='America/Los_Angeles', UserName='integrationuser1@testorg.com');
        
        insert u;
              
        Profile p2 = [SELECT id FROM Profile where Name = 'System Administrator' LIMIT 1].get(0);
        User u2 = new User(Alias = 'testpk1', Email='testpkstandarduser1@testorg.com', 
              EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
              LocaleSidKey='en_US', ProfileId = p2.Id, 
              TimeZoneSidKey='America/Los_Angeles', UserName='testpkstandarduser1@testorg.com');
        
        insert u2; 
         
        System.RunAs(u)
        {
        
            Account cbucAccount = createTestAccount('CBUC','(000) 111 0001',accRTMap.get('CBUCID'),null,false,'0000000001',false);
            accList.add(cbucAccount);
            Lev1List.add(cbucAccount);
            Account canAccount2 = createTestAccount('CAN 2','(000) 111 0002',accRTMap.get('CAN'),null,false,'00444',false);
            accList.add(canAccount2);
            Lev1List.add(canAccount2);
            Account banAccount2 = createTestAccount('BAN 2','(000) 111 0003',accRTMap.get('BAN'),null,true,'004',false);
            accList.add(banAccount2);
            Lev1List.add(banAccount2);
            Account rcidAccount3 = createTestAccount('RCID 3','(010) 111 0001',accRTMap.get('RCID'),null,false,'0000001113',false);
            accList.add(rcidAccount3);
            Lev1List.add(rcidAccount3);
            Account banAccount4 = createTestAccount('BAN 4','(011) 222 0001',accRTMap.get('BAN'),null,false,'005',false);
            accList.add(banAccount4);
            Lev1List.add(banAccount4);
            
            insert Lev1List;
            Lev1List = new List<Account>();
            
            Account rcidAccount = createTestAccount('RCID','(000) 111 1001',accRTMap.get('RCID'),cbucAccount.Id,false,'0000001111',false);
            accList.add(rcidAccount);
            Lev1List.add(rcidAccount);
            Account rcidAccount2 = createTestAccount('RCID 2','(010) 111 0001',accRTMap.get('RCID'),cbucAccount.Id,false,'0000001112',false);
            accList.add(rcidAccount2);
            Lev1List.add(rcidAccount2);
            Account banAccount5 = createTestAccount('BAN 5','(011) 222 0011',accRTMap.get('BAN'),banAccount4.Id,false,'0055',false);
            accList.add(banAccount5);
            Lev1List.add(banAccount5);
            
            insert Lev1List;
            Lev1List = new List<Account>();
            
            
            Account canAccount = createTestAccount('CAN','(000) 111 2101',accRTMap.get('CAN'),rcidAccount.Id,false,'001',false);
            accList.add(canAccount);
            Lev1List.add(canAccount);
            Account banAccount = createTestAccount('BAN','(001) 333 0001',accRTMap.get('BAN'),rcidAccount.Id,false,'0044',false);
            accList.add(banAccount);
            Lev1List.add(banAccount);
            Account banAccount6 = createTestAccount('BAN 6','(011) 222 0201',accRTMap.get('BAN'),banAccount5.Id,false,'00555',false);
            accList.add(banAccount6);
            Lev1List.add(banAccount6);
            
            insert Lev1List;
            Lev1List = new List<Account>();
            
            Account canAccount1 = createTestAccount('CAN 1','(000) 111 2001',accRTMap.get('CAN'),canAccount.Id,false,'002',false);
            accList.add(canAccount1);
            Lev1List.add(canAccount1);
            Account canAccount3 = createTestAccount('CAN 3','(000) 222 0001',accRTMap.get('CAN'),canAccount.Id,true,'003',false);
            accList.add(canAccount3);
            Lev1List.add(canAccount3);
            Account banAccount7 = createTestAccount('BAN 7','(011) 122 2001',accRTMap.get('BAN'),banAccount6.Id,false,'0055-5',false);
            accList.add(banAccount7);
            Lev1List.add(banAccount7);
            
            insert Lev1List;
            Lev1List = new List<Account>();
            
            Account canAccount4 = createTestAccount('CAN 4','(001) 333 0001',accRTMap.get('CAN'),canAccount3.Id,true,'00472',false);
            accList.add(canAccount4);
            Lev1List.add(canAccount4);
           
            insert Lev1List;
            
            System.runAs(u2)
            {
        
                //insert accList;
                
                for(Account acc : accList)
                {
                    accIdSet.add(acc.id);
                }
                
                list<Web_Account__c> webAccList = new List<Web_Account__c>();
                for(integer i=0; i<3; i++)
                {
                    webAccList.add(createWebAccount('Test ' + i,false));
                }
                insert webAccList;
                
                System.debug('### rcidAccount ' + rcidAccount ); 
                
                list<Contact> contactList = new List<Contact>();
                
                contactList.add(createTestContact('CBUC','(222) 222 0000',cbucAccount.Id,'cbuc@test.com.test',null,false));
                contactList.add(createTestContact('CBUC2','(222) 222 0011',cbucAccount.Id,'cbuc2@test.com.test',null,false));
                contactList.add(createTestContact('RCID','(222) 222 1100',rcidAccount.Id,'rcid@test.com.test',null,false));
                contactList.add(createTestContact('RCID2','(222) 222 0001',rcidAccount.Id,'rcid2@test.com.test',null,false));
                contactList.add(createTestContact('RCID3','(222) 222 3333',rcidAccount.Id,'rcid3@test.com.test',null,false));
                contactList.add(createTestContact('CAN','(222) 222 0002',canAccount.Id,'can@test.com.test',null,false));
                contactList.add(createTestContact('CAN2','(222) 222 0033',canAccount.Id,'can2@test.com.test',null,false));
                contactList.add(createTestContact('CAN3 1','(222) 222 0000',canAccount3.Id,'can3_1@test.com.test',null,false));
                contactList.add(createTestContact('BAN','(222) 222 0000',banAccount.Id,'ban@test.com.test',null,false));
                contactList.add(createTestContact('BAN2','(222) 333 0000',banAccount.Id,'ban2@test.com.test',null,false));
                contactList.add(createTestContact('RCID 2','(222) 111 0000',rcidAccount2.Id,'rcid2@test.com.test',null,false));
                contactList.add(createTestContact('CAN 2','(222) 222 1111',canAccount2.Id,'can_2@test.com.test',null,false));
                contactList.add(createTestContact('BAN 2','(222) 222 4444',banAccount2.Id,'ban_2@test.com.test',null,false));
                contactList.add(createTestContact('CAN4 1','(222) 222 1122',canAccount2.Id,'can4_1@test.com.test',null,false));
                contactList.add(createTestContact('BAN7 1','(222) 222 2222',banAccount7.Id,'ban7_1@test.com.test',null,false));
                contactList.add(createTestContact('BAN6 1','(222) 222 2222',banAccount6.Id,'ban6_1@test.com.test',null,false));
                contactList.add(createTestContact('BAN5 1','(222) 222 2222',banAccount6.Id,'ban5_1@test.com.test',null,false));
                contactList.add(createTestContact('RCID4 Web Account','(222) 222 3333',rcidAccount.Id,'rcid3@test.com.test',webAccList.get(0).id,false));
                
                database.insert(contactList);
                
                set<Id> conIdSet = new Set<Id>();
                
                for(Contact con : contactList)
                {
                    conIdSet.add(con.id);
                }
                
                list<Case> caseList = new list<Case>();
                // Exact Match Email - Non RCID
                caseList.add(createTestWebCase('0000001112','(222) 222 1122','BAN 2','(222) 222 1122','ban2@test.com.test',false,caseRTId));
                // Exact Match Email - RCID
                caseList.add(createTestWebCase('1112','Tel : (222) 222 1122','BAN 2','(222) 222 1122','rcid@test.com.test',false,caseRTId));
                
                // Exact Match Email - CBUC
                caseList.add(createTestWebCase('1112','Tel : (222) 222 1122','BAN 2','(222) 222 1122','cbuc@test.com.test',false,caseRTId));
                
                // Exact Match Email - No RCID - Consumer True
                caseList.add(createTestWebCase('1112','BAN : 004','BAN 2','(222) 222 1122','ban_2@test.com.test',false,caseRTId));
                
                // Exact Match Email - No RCID - Consumer False
                caseList.add(createTestWebCase('1112','CAN : 004','BAN 2','(222) 222 1122','can_2@test.com.test',false,caseRTId));
                
                // Multiple Email - invalid RCID
                caseList.add(createTestWebCase('1144','CAN : 004','BAN 2','(222) 222 1122','rcid2@test.com.test',false,caseRTId));
                
                // Multiple Email - Valid RCID - No exact match
                caseList.add(createTestWebCase('1111','CAN : 004','BAN 2','(222) 222 1122','rcid2@test.com.test',false,caseRTId));
                
                // No Email - Valid RCID - 0 Contact
                caseList.add(createTestWebCase('1113','CAN : 004','BAN 2','(222) 222 1122','rcid2@test.com.test',false,caseRTId));
                
                // No Email - Valid RCID - Exact Match
                caseList.add(createTestWebCase('1111','CAN : 004','Test RCID','222.222.1100',null,false,caseRTId));
                
                // No Email - Valid RCID - Exact only Name Match
                caseList.add(createTestWebCase('1111','CAN : 004','Test RCID','222.222.1101',null,false,caseRTId));
                
                // No Email - Valid RCID - Exact only Number Match
                caseList.add(createTestWebCase('1111','CAN : 00444','RCID','222.222.1100',null,false,caseRTId));
                
                // No Email - Valid RCID - Semi Name Match
                caseList.add(createTestWebCase('1111','CAN : 00444','Test RCI','333.222.1101',null,false,caseRTId));
                
                // No Email - Valid RCID - Semi Number Match
                caseList.add(createTestWebCase('1111','CAN : 00444','PK','222.222',null,false,caseRTId));
                
                // No Email - Valid RCID - No Match Contacts
                caseList.add(createTestWebCase('0000001112','CAN : 00444','PK','555',null,false,caseRTId));
                
                // No Email - No RCID 
                caseList.add(createTestWebCase('','CAN : 00444','PK','555',null,false,caseRTId));
                
                // No Data
                caseList.add(createTestWebCase(null,null,null,null,null,false,caseRTId));
                
                // Duplicate Email but 1 id a Web Account
                caseList.add(createTestWebCase('1144','CAN : 004','BAN 2','(222) 222 1122','rcid3@test.com.test',false,caseRTId));
                
                // Multiple Traverse
                caseList.add(createTestWebCase('1144','CAN : 004','BAN 2','(222) 222 1122','ban7_1@test.com.test',false,caseRTId));
                caseList.add(createTestWebCase('1144','BAN : 00555','BAN 2','(222) 222 1122','ban6_1@test.com.test',false,caseRTId));
                caseList.add(createTestWebCase('1144','BAN : 00555','BAN 3','(222) 222 1122','ban5_1@test.com.test',false,caseRTId));
                
                
                
                Test.startTest();
                // Coverage for Trigger
                //---------------------
                insert caseList;
                //---------------------
                    
                
                Test.stopTest();
            }
        }
		system.debug('limits after webToCaseTest:'+ limits.getAggregateQueries());
        
                
    }
    
    public static Case createTestWebCase(string webRCID,string banCanTel, string contactName, string contactPhone, string contactEmail, boolean doInsert, id recId)
    {
        Case cas = new Case();
        cas.Web_form_RCID__c = webRCID;
        cas.BAN_Nbr__c = banCanTel;
        cas.SuppliedName = contactName;
        cas.SuppliedPhone = contactPhone;
        cas.SuppliedEmail = contactEmail;
        cas.Priority = 'Medium';
        cas.Status = 'New';
        cas.Origin = 'L&R Web Form';
        cas.RecordTypeId = recId;
        cas.SuppliedCompany = 'Test Account';
        cas.Product_Affected__c = 'Voice';
        cas.Type = 'Other';
        cas.Subject = 'Test Case ** Please Ignore';
        cas.Description = 'Test Description';
        
        if(doInsert)
            insert cas;
        return cas;
    }
    
    public static Account createTestAccount(string aName,string aPhone, Schema.RecordTypeInfo aRecType, id aParentId, boolean aConsumerLegacy,string aRCID, boolean doInsert )
    {
        Account acc = new Account();
        acc.Name = 'Test ' + aName;
        acc.Phone = aPhone;
        acc.Language_Preference__c = 'English';
        acc.RecordTypeId = aRecType.getrecordtypeid();
        acc.ParentId = aParentId;
        acc.Consumer_Legacy_client__c = aConsumerLegacy;
        if(aRecType.getname() == 'CBUCID')
            acc.CBUCID_Parent_Id__c = aRcid;
        else if(aRecType.getname() == 'RCID')
            acc.RCID__c = aRcid;
        else if(aRecType.getname() == 'BAN')
        {
            acc.BAN__c = aRcid;
            acc.BAN_CAN__c = aRcid;
            acc.Account_Classification_ETL__c = 'Wireless';
        }
        else if(aRecType.getname() == 'CAN')
        {
            acc.CAN__c = aRcid;
            acc.BAN_CAN__c = aRcid;
            acc.Account_Classification_ETL__c = 'FFH';
        } 
        if(doInsert)
            insert acc;
        return acc;
    }
    
    public static Contact createTestContact(string cName,string cPhone, id cAccId, string cEmail,id webAccount, boolean doInsert)
    {
        Contact con = new Contact();
        con.FirstName = 'Test ';
        con.LastName = cName;
        con.Phone = cPhone;
        con.Language_Preference__c = 'English';
        con.AccountId = cAccId;
        con.email = cEmail;
        con.Web_Account__c = webAccount;
        if(doInsert)
            insert con;
        return con;
    }
    
    public static Web_Account__c createWebAccount (string webName, boolean doInsert)
    {
        Web_Account__c webAcc = new Web_Account__c();
        webAcc.Type_of_Customer__c = 'New';
        webAcc.Account_Classification__c = 'Wireless';
        webAcc.Current_Customer_Status__c = 'Wireline Customer';
        webAcc.Name = webName;
        webAcc.Company_Legal_Name__c = 'Test Company';
        webAcc.Billing_Address__c ='Test Billing Address';
        webAcc.Billing_City__c ='Test Billing City';
        webAcc.Billing_Province__c = 'AB';
        webAcc.Billing_Postal_Code__c = 'A0A 0A0';
        webAcc.BAN_Type__c ='Business Regular';
        webAcc.Shipping_Address__c = 'Test Shipping Address';
        webAcc.Shipping_City__c = 'Test Shipping City';
        webAcc.Shipping_Province__c = 'AB';
        webAcc.Shipping_Postal_Code__c = 'A0A 0A0';
        if(doInsert)
            insert webAcc;
        return webAcc;
        
    }
    
    public static Asset createTestAsset (id aAccount,string aPhone, boolean doInsert)
    {
        Asset assetObj = new Asset();
        assetObj.AccountId = aAccount;
        assetObj.Phone__c = aPhone;
        assetObj.Name = aPhone;
        assetObj.Billing_System_ID__c = '100';
        assetObj.MCM_External_ID__c = '100-123456-' + aPhone;
        if(doInsert)
            insert assetObj;
        return assetObj;
        
    }
    
    
    
}