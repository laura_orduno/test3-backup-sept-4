/*
###########################################################################
# File..................: smb_WebCaseAfterClass
# Version...............: 26
# Created by............: Puneet Khosla
# Created Date..........: 07-Nov-2013
# Last Modified by......: 
# Last Modified Date....: 
# Description...........: Only to be run for Web to Case : Helper Class for trigger (smb_WebCaseAfter)
###########################################################################
*/
public class smb_WebCaseAfterClass 
{
    public static map<string,Contact> foundContacts;
    public static set<id> accountIdSet;
    public static list<Case> webCaseList;
    
    /*
    * @author       : Puneet Khosla
    * @method Name  : chooseContact
    * @description  : This method selects the best Match Contact
    * @return       : void : N/A 
    * @param        : caseList : List of the all cases 
   */
    public static void chooseContact(list<Case> caseList)
    {
        //-------------------- Variables --------------------
        set<string> emailAddresses = new set<string>();
        set<string> rcidAccountsSet = new set<string>();
        
        webCaseList = new list<Case>();
        id lrRecordTypeId = id.valueOf([Select Id From RecordType where SobjectType = 'Case' and Name ='SMB Care L&R Web Form' LIMIT 1].get(0).id);
                
        for(Case cs : caseList)
        {
            // Extract only Web Cases
            if(cs.recordTypeId == lrRecordTypeId  && cs.Origin == 'L&R Web Form')
            {
                if(cs.ContactId == null)
                {
                    //Populate the email Addresses Set to find exact match
                    if(String.isNotBlank(cs.SuppliedEmail))
                        emailAddresses.add(cs.SuppliedEmail);
                }
                if(cs.AccountId == null)
                {
                    if(String.IsNotBlank(cs.Web_form_RCID__c))
                    {
                        string enteredRCID = cs.Web_form_RCID__c;
                        enteredRCID = enteredRCID.replaceAll(' ','').leftPad(10);
                        enteredRCID = enteredRCID.replaceAll(' ', '0');
                        cs.Web_form_RCID__c = enteredRCID;
                        rcidAccountsSet.add(enteredRCID);
                    }                       
                }
                string formattedDescription = cs.Description;
                formattedDescription = formattedDescription.replaceAll('<br>','\r\n');
                
                cs.Description = formattedDescription;
                // Create the web Cases List
                webCaseList.add(cs);
            }
        }
        // Only to Run for Web To Case
        if(webCaseList.size() > 0)
        {
            map<string,set<Contact>> rcidContactMap = new Map<string,set<Contact>>();
            // Get the mapping data for email and RCID Accounts
            buildContactsWithEmail(emailAddresses);
            rcidContactMap = buildAccountsWithContacts(rcidAccountsSet);
            emailAddresses = null;
            
            map<id,id> childRcidMapping = new map<id,id>();
            list<Contact> tempContactList = new list<Contact>();
            
            // Get the RCID Account matching to the account Id
            if(accountIdSet !=null && accountIdSet.size() > 0)
                childRcidMapping = smb_AccountUtility.getRcidAccountId(accountIdSet);
            
            for(Case c : webCaseList)
            {
                
                if(c.ContactId == null || c.ContactId == '')
                {
                    if(foundContacts.containsKey(c.SuppliedEmail))
                    {                   
                        Contact con = foundContacts.get(c.SuppliedEmail);
                        System.debug('### con.AccountId : ' + con.AccountId);
                        if(con.AccountId != null)
                        {
                            System.debug('### con.Account.RecordType.DeveloperName ' + con.Account.RecordType.DeveloperName);
                            if (con.Account.RecordType.DeveloperName != 'RCID')
                            {
                                System.debug('### NonRCID con.accountid ' + con.accountid);
                                if(childRcidMapping.containsKey(con.accountid))
                                {
                                    System.debug('### Mapped RCID con.accountid ' + con.accountid);
                                    c.AccountId = childRcidMapping.get(con.accountid);
                                    c.ContactId = con.id;
                                    if (con.Account.RecordType.DeveloperName == 'BAN')
                                        c.Related_BAN_Account__c = con.accountid;
                                }
                                else
                                    if((con.Account.RecordType.DeveloperName == 'BAN' || con.Account.RecordType.DeveloperName == 'CAN') && (con.Account.Consumer_Legacy_client__c ==false) )
                                    {
                                    }
                                    else
                                    {
                                        System.debug('### Consumer con.accountid ' + con.accountid);
                                        c.ContactId = con.id;
                                        c.accountId = con.AccountId;
                                        if (con.Account.RecordType.DeveloperName == 'BAN')
                                            c.Related_BAN_Account__c = con.accountid;
                                    }
                            }
                            else
                            {
                                System.debug('### RCID con.accountid ' + con.accountid);
                                c.AccountId = con.accountid;
                                c.ContactId = con.id;
                            }
                            System.debug('### c.AccountId ' + c.AccountId); 
                        }
                    }
                    else
                    {
                        if(String.isNotBlank(c.Web_form_RCID__c))
                        {
                            if(rcidContactMap.containsKey(c.Web_form_RCID__c))
                            {
                                tempContactList = new list<Contact>();
                                tempContactList.addAll(rcidContactMap.get(c.Web_form_RCID__c));
                                Contact bestMatchContact = (findBestMatchContact(tempContactList,c.SuppliedName,c.SuppliedPhone,true)).get(0);
                                if(bestMatchContact!= null)
                                {
                                    c.AccountId = bestMatchContact.accountid;
                                    c.ContactId = bestMatchContact.id;
                                }
                            }
                        }
                    }
                }       
            }
        }
    }
    
    /*
    * @author       : Puneet Khosla
    * @method Name  : runAssignmentRule
    * @description  : This method is used to Run the Assignment Rule
    * @return       : void : N/A 
    * @param        : caseList : List of the all cases 
   */
    public static void runAssignmentRule(list<Case> caseList)
    {
        webCaseList = new list<Case>();
        
// ***** OLD IMPLEMENTATION        id lrRecordTypeId = id.valueOf([Select Id From RecordType where SobjectType = 'Case' and Name ='SMB Care L&R Web Form' LIMIT 1].get(0).id);
        id lrRecordTypeId = null;
        
        for(Case cs : caseList)
        {
            if(cs.Origin == 'L&R Web Form'){
                if(lrRecordTypeId == null){
			        lrRecordTypeId = id.valueOf([Select Id From RecordType where SobjectType = 'Case' and Name ='SMB Care L&R Web Form' LIMIT 1].get(0).id);
                }
                
                if(cs.recordTypeId == lrRecordTypeId){
	                webCaseList.add(cs);                                
                } 
            }

            
/***** OLD IMPLEMENTATION            
            if(cs.recordTypeId == lrRecordTypeId && cs.Origin == 'L&R Web Form')
                webCaseList.add(cs);                
*/
        }
        
        if(webCaseList.size() > 0)
        {   
            AssignmentRule AR = new AssignmentRule();
            AR = [select id from AssignmentRule where SobjectType = 'Case' and Active = true limit 1];
        
            Database.DMLOptions dmlOpts = new Database.DMLOptions();
            dmlOpts.assignmentRuleHeader.assignmentRuleId= AR.id;
            
            list<Case> listToUpdate = new list<Case>();
            for(Case c : webCaseList)
            {
                Case c1= new Case(id=c.id);
                c1.setOptions(dmlOpts);
                listToUpdate.add(c1);
            }
            if(listToUpdate.size() > 0)
                database.update(listToUpdate,false);
        }
    }
    
    /*
    * @author       : Puneet Khosla
    * @method Name  : buildContactsWithEmail
    * @description  : This method is used to build a map of email address and Contact
    * @return       : void : N/A 
    * @param        : emailAdd : Set of the all email addresses in web to case 
   */
    public static void buildContactsWithEmail (set<string> emailAdd)
    {
        foundContacts = new map<string,Contact>();
        accountIdSet = new set<id>();
        set<string> emailAddressSet = new set<string>();
        
        list<Contact> contactList = new list<Contact>();
        // Get List of Contacts with matching email address
        try
        {
            contactList = [SELECT id, accountid, email,Account.RecordType.DeveloperName, Account.Consumer_Legacy_client__c, Name, Phone,Web_Account__c from Contact WHERE email in :emailAdd AND Active__c = true AND (Web_Account__c = null OR Web_Account__c = '') LIMIT 3000];
        }
        catch (Exception e)
        {
            contactList = new list<Contact>();
        }
        
        for(Contact c : contactList)
        {
            if(emailAddressSet.contains(c.email))
            {
                // If multiple Contacts found with same email address, then no automatch
                if(foundContacts.containsKey(c.email))
                {
                    foundContacts.remove(c.email);
                }
            }
            else
            {
                emailAddressSet.add(c.email);
                foundContacts.put(c.email,c);
                accountIdSet.add(c.AccountId);
            }
        }
        System.debug('### foundContacts : ' + foundContacts);
        emailAddressSet = null;
    }
    
    /*
    * @author       : Puneet Khosla
    * @method Name  : buildAccountsWithContacts
    * @description  : This method is used to build a map of RCID Account with the Contacts
    * @return       : map<string,set<Contact>> : Map of all RCID accounts with Contacts 
    * @param        : rcidId : Set of the all RCID ids in web to case 
   */
    
    public static map<string,set<Contact>> buildAccountsWithContacts(set<String> rcidId)
    {
        System.debug('### Reached buildAccountsWithContacts');
        
        list<Contact> conList = [SELECT id, accountId,Name,Phone, Email,Account.RCID__c,Web_Account__c FROM Contact where account.RCID__c in :rcidId AND Active__c = true AND (Web_Account__c = null OR Web_Account__c = '') LIMIT 3000];
        map<string,set<Contact>> rcidContactMap = new Map<string,set<Contact>>();
        
        for(Contact c : conList)
        {
            if(rcidContactMap.containsKey(c.Account.RCID__c))
            {
                set<Contact> tSet = rcidContactMap.get(c.Account.RCID__c);
                tSet.add(c);
                rcidContactMap.put(c.Account.RCID__c,tSet); 
                tSet = null;
            }
            else
                rcidContactMap.put(c.Account.RCID__c,new set<Contact>{c});
        }
        System.debug('### rcidContactMap : ' + rcidContactMap);
        return rcidContactMap;
    }
    
    /*
    * @author       : Puneet Khosla
    * @method Name  : findBestMatchContact
    * @description  : This method is used to return list of matching contacts
    * @return       : list<Contact> : List of matched contacts with best matches on the top 
    * @param        : contactList : Set of the all Contacts
    *                 referenceContactName : Name to be matched with
    *                 referenceContactNumber : Number to be matched with
    *                 bestMatchOnly : Whether we just want the best matched Contact         
   */
   
    public static list<Contact> findBestMatchContact (list<Contact> contactList,string referenceContactName, string referenceContactNumber, boolean bestMatchOnly)
    {
        
        Contact bestMatchContact;
        
        set<Contact> bestMatchSet = new set<Contact>();
        set<Contact> goodMatchSet = new set<Contact>();
        set<Contact> matchSet = new set<Contact>();
        set<Contact> noMatchSet = new set<Contact>();
        list<Contact> finalList = new list<Contact>();
        
        integer listSize = contactList.size();
        integer pointer = 0;
        boolean bestContactFound = false;
        boolean nameContactFound = false;
        boolean numberContactFound = false;
        boolean anyMatchContactFound = false;
        
        // If Single Contact, return
        if(listSize == 1)
        {
            finalList.add(contactList.get(pointer));
            bestContactFound = true;
            anyMatchContactFound = true;
            return finalList;
        }
        
        string suppliedName = referenceContactName;
        string suppliedPhone = referenceContactNumber.replaceAll('[^0-9]','');
        
        // If bestMatchOnly is true, then loop exits when bestContactFound : true
        while(!(bestContactFound && bestMatchOnly) && pointer < listSize)
        {
            
            string contactName = contactList.get(pointer).Name;
            string contactNumber = contactList.get(pointer).Phone;
            if(String.IsNotBlank(contactNumber))
                contactNumber = contactNumber.replaceAll('[^0-9]','');
            
            // If exact Name and number found
            if(suppliedName.equalsIgnoreCase(contactName) && suppliedPhone.equalsIgnoreCase(contactNumber))
            {
                bestMatchSet.add(contactList.get(pointer));
                bestContactFound = true;
            }
            // If only Name matches
            else if(suppliedName.equalsIgnoreCase(contactName))
            {
                goodMatchSet.add(contactList.get(pointer));
                nameContactFound = true;
                anyMatchContactFound = true;
            }
            // If only Number matches
            else if(suppliedPhone.equalsIgnoreCase(contactNumber))
            {
                goodMatchSet.add(contactList.get(pointer));
                numberContactFound = true;
                anyMatchContactFound=true;
            }
            // Match partial Name
            else if(contactName.containsIgnoreCase(suppliedName))
            {
                matchSet.add(contactList.get(pointer));
                anyMatchContactFound = true;
            }
            // Match partial Number
            else if(String.IsNotBlank(contactNumber) && contactNumber.containsIgnoreCase(suppliedPhone))
            {
                matchSet.add(contactList.get(pointer));
                anyMatchContactFound = true;
            }
            else
                noMatchSet.add(contactList.get(pointer));
            pointer = pointer +1;   
        }
        // Create Final list
        
        for(Contact con : bestMatchSet)
            finalList.add(con);
        bestMatchSet = null;
        for(Contact con : goodMatchSet)
            finalList.add(con);
        goodMatchSet = null;
        for(Contact con : matchSet)
            finalList.add(con);
        matchSet = null;
        for(Contact con : noMatchSet)
            finalList.add(con);
        noMatchSet = null;
        return finalList;
        
    }
}