global class scheduledBatch_VOCSurveySuppression implements Schedulable {
   global void execute(SchedulableContext sc) {
       batch_VOCSurveySuppression bc = new batch_VOCSurveySuppression();        
       Database.executeBatch(bc);
   }
}