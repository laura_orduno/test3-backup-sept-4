/*
###########################################################################
# File..................: smb_AgreementLineItemTrigger_Helper
# Version...............: 1
# Created by............: Puneet Khosla
# Created Date..........: 19-Dec-2013
# Last Modified by......: 
# Last Modified Date....: 
# Description...........: This Helper class contains the methods used for Agreement Line Item Trigger.
# Change Log:    
# 1. Puneet Khosla - Printed Quotes and Agreement Data Mapping       
############################################################################
 */
public class smb_AgreementLineItemTrigger_Helper
{
    public static void updateALI(List<Apttus__AgreementLineItem__c> aliList)
    {          
       
        Set<Id> setAgreementIds = new Set<Id>();
        // Create a set of Unique Agreements
        for(Apttus__AgreementLineItem__c agreementLI: aliList)
        {
            setAgreementIds.add(agreementLI.Apttus__AgreementId__c); 
        }

        List<Apttus__AgreementLineItem__c> alUpdateList = new List<Apttus__AgreementLineItem__c>();       

        Map<Id,Apttus__APTS_Agreement__c> agreementMap = new Map<Id,Apttus__APTS_Agreement__c>([Select ag.Name, ag.Id, (Select Id, Apttus__AgreementId__c, LineNo__c, ParentLineNo__c From Apttus__AgreementLineItems__r) From Apttus__APTS_Agreement__c ag where Id IN : setAgreementIds]);
        
        // Start looking up for agreement
        for(Apttus__APTS_Agreement__c agreement : agreementMap.values())
        {              
            Map<String,Apttus__AgreementLineItem__c> objALI = new Map<String,Apttus__AgreementLineItem__c>(); 
            // Map the line number and agreement
            for(Apttus__AgreementLineItem__c al : agreement.Apttus__AgreementLineItems__r)
            {        
                objALI.put(String.valueOf(al.LineNo__c),al);       
            }
            // This is again traversed as we first need to build the map of entire list   
            for(Apttus__AgreementLineItem__c al : agreement.Apttus__AgreementLineItems__r)
            {
                if(al.ParentLineNo__c != null && objALI.get(String.valueOf(Integer.valueOf(al.ParentLineNo__c))) != null)
                {
                    Apttus__AgreementLineItem__c currentALI = new Apttus__AgreementLineItem__c(Id=al.Id);
                    currentALI.APTS_LineItem_Parent_Line_Item__c = objALI.get(String.valueOf(Integer.valueOf(al.ParentLineNo__c))).Id;                                                                                                                         
                    alUpdateList.add(currentALI);                                           
                }       
            }   
        }
        update alUpdateList;
    } 
}