/**
 * @author Steve Doucette, Traction on Demand 
 * @date 2018-06-15
 * @description Schedulable class that dynamically purges records from the org based on custom metadata
 */
public with sharing class PurgerScheduler implements Schedulable {
	@TestVisible private static final String JOB_NAME_PREFIX = 'PurgerScheduler';

	private static final DateTime NOW = DateTime.now();

	private static final Integer MINUTES_IN_DAY = 1440;

	private static Integer nextRunInMinutes = MINUTES_IN_DAY; // Default to 1 day

	public void execute(SchedulableContext sc) {
		try {
			for (Purger_Setting__mdt ps : [
					SELECT Id, DeveloperName, MasterLabel, Criteria__c, Exclusion_Field__c, Frequency__c, Last_Run__c, Retention_Time__c
					FROM Purger_Setting__mdt
					WHERE Active__c = TRUE
			]) {

				// Use the Purger Setting with lowest frequency to determine when the next run will be
				if (ps.Frequency__c > 0 && ps.Frequency__c < nextRunInMinutes) {
					nextRunInMinutes = Integer.valueOf(ps.Frequency__c);
				}

				// Create batch job for this Purger Setting
				if (ps.Last_Run__c == null || ps.Last_Run__c <= NOW.addMinutes(Integer.valueOf(-ps.Frequency__c))) {
					Database.executeBatch(new PurgerBatchJob(ps));
				}
			}
		} catch (Exception e) {
			PurgerUtils.log(e);
		} finally {
			scheduleNextRun(sc.getTriggerId());
		}
	}

	// Abort current job (if it still exists) and schedule new job by X minutes
	// (where X is determined by the interval value of custom metadata type)
	private static void scheduleNextRun(Id thisJobId) {
		// Abort current job (if it still exists)
		try {
			System.abortJob(thisJobId);
		} catch (Exception e) {
			// Do Nothing
		}

		// Start a new schedule
		Type t = Type.forName(JOB_NAME_PREFIX);
		Schedulable s = (Schedulable) t.newInstance();
		DateTime nextRun = NOW.addMinutes(nextRunInMinutes);
		System.schedule(JOB_NAME_PREFIX + '_' + System.currentTimeMillis(), '0 ' + nextRun.minute() + ' ' + nextRun.hour()
				+ ' ' + nextRun.day() + ' ' + nextRun.month() + ' ? ' + nextRun.year(), s);
	}
}