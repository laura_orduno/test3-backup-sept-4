@isTest
public class ENTPOverviewCtlr_Test {
	@isTest(SeeAllData=false) static void overviewController() {

		User u = MBRTestUtils.createENTPPortalUser('test-user-1', false, 'en_US');
		ENTPTestUtils.createENTPCustomSetting();

		insert new List<SMBCare_WebServices__c>{
				new SMBCare_WebServices__c(Name = 'TTODS_Endpoint_Search_Lynx', Value__c = 'Test_Endpoint'),
				new SMBCare_WebServices__c(Name = 'username', Value__c = 'testUserName'),
				new SMBCare_WebServices__c(Name = 'password', Value__c = 'testPassword')
		};

		System.runAs(u) {

			Case ENTPCase = new Case(Subject = 'Test Subject', Origin = 'ENTP', My_Business_Requests_Type__c = 'Other', Lynx_Ticket_Number__c = '5555555555', RecordTypeId = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId);
			insert ENTPCase;
			Case ENTPTestCase = [SELECT Id, Subject, RecordTypeId, Contact.FirstName, Lynx_Ticket_Number__c, Contact.LastName, My_Business_Requests_Type__c, CaseNumber, Status, LastModifiedDate FROM Case WHERE Id = :ENTPCase.Id LIMIT 1];

			ENTPOverviewCtlr1 overviewContr = new ENTPOverviewCtlr1();
			overviewContr.initCheck();

			Test.startTest();
			String CustRole = 'private cloud';
			//overviewContr.CustRole1 = CustRole;
			overviewContr.statusFilter = 'My tickets';
			String SFDCAccountId = 'test';
			System.assertEquals(SFDCAccountId, 'test');
			//overviewContr.CatListPull1='test';
			overviewContr.AddFuncPull = 'Test';
			overviewContr.AddFuncPull1 = 'Test';
			overviewContr.lynxTicketNo = '11111111';
			overviewContr.refNo = '123345566';
			overviewContr.telusID = '70208';
			overviewContr.RCID = '111111';

			//System.assertEquals( CatListPull1, 'test');
			//test search functionality
			Case c = [SELECT caseNumber from Case Where Id = :ENTPTestCase.Id LIMIT 1];
			//overviewContr.q = 'Test';
			overviewContr.searchCase();
			//System.assertEquals(1, overviewContr.totalCases);

			Id [] fixedSearchResults = new Id[]{
					c.Id
			};
			Test.setFixedSearchResults(fixedSearchResults);

			overviewContr.q = '11111111';
			List<Case> cases = overviewContr.executeSearch(1, 1, true);
			//System.assertEquals(1, cases.size());

			overviewContr.callMethod();
			//test search functionality
			Case cN = [SELECT caseNumber from Case Where Id = :ENTPTestCase.Id LIMIT 1];
			//overviewContr.q = 'Test';
			overviewContr.searchCase();
			//System.assertEquals(1, overviewContr.totalCases);

			Id [] fixedSearchResultsN = new Id[]{
					cN.Id
			};
			Test.setFixedSearchResults(fixedSearchResultsN);

			overviewContr.q = '5555555556';
			List<Case> casesN = overviewContr.executeSearch(1, 1, true);
			//System.assertEquals(1, cases.size());

		}
		System.runAs(u) {

			Case ENTPCase = new Case(Subject = 'Test Subject', Origin = 'ENTP', My_Business_Requests_Type__c = 'Other', Lynx_Ticket_Number__c = '5555555555', RecordTypeId = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId);
			insert ENTPCase;
			Case ENTPTestCase = [SELECT Id, Subject, RecordTypeId, Contact.FirstName, Lynx_Ticket_Number__c, Contact.LastName, My_Business_Requests_Type__c, CaseNumber, Status, LastModifiedDate FROM Case WHERE Id = :ENTPCase.Id LIMIT 1];

			ENTPOverviewCtlr1 overviewContr1 = new ENTPOverviewCtlr1();
			overviewContr1.initCheck();

			String CustRole = 'tps';
			String CustRole1 = CustRole;
			String SFDCAccountId = 'test';
			System.assertEquals(SFDCAccountId, 'test');
			//overviewContr1.CatListPull1='test';
			overviewContr1.AddFuncPull = 'Test';
			overviewContr1.AddFuncPull1 = 'Test';
			overviewContr1.lynxTicketNo = '11111111';
			overviewContr1.refNo = '123345566';
			overviewContr1.telusID = '70208';
			overviewContr1.RCID = '111111';
			//System.assertEquals( CatListPull1, 'test');
			//test search functionality
			Case c = [SELECT caseNumber from Case Where Id = :ENTPTestCase.Id LIMIT 1];
			//overviewContr.q = 'Test';
			overviewContr1.searchCase();
			//System.assertEquals(1, overviewContr.totalCases);

			Id [] fixedSearchResults = new Id[]{
					c.Id
			};
			Test.setFixedSearchResults(fixedSearchResults);

			overviewContr1.q = 'Test';
			List<Case> cases = overviewContr1.executeSearch(1, 1, true);
			//System.assertEquals(1, cases.size());

			overviewContr1.callMethod();
			//test search functionality
			Case cN = [SELECT caseNumber from Case Where Id = :ENTPTestCase.Id LIMIT 1];
			//overviewContr.q = 'Test';
			overviewContr1.searchCase();
			//System.assertEquals(1, overviewContr.totalCases);

			Id [] fixedSearchResultsN = new Id[]{
					cN.Id
			};
			Test.setFixedSearchResults(fixedSearchResultsN);

			overviewContr1.q = '5555555556';
			List<Case> casesN = overviewContr1.executeSearch(1, 1, true);
			//System.assertEquals(1, cases.size());

			Test.stopTest();
		}
	}

	@isTest(SeeAllData=false) static void overviewControllerFR() {

		User uFr = MBRTestUtils.createENTPPortalUser('test-user-1', false, 'fr');
		ENTPTestUtils.createENTPCustomSetting();

		insert new List<SMBCare_WebServices__c>{
				new SMBCare_WebServices__c(Name = 'TTODS_Endpoint_Search_Lynx', Value__c = 'Test_Endpoint'),
				new SMBCare_WebServices__c(Name = 'username', Value__c = 'testUserName'),
				new SMBCare_WebServices__c(Name = 'password', Value__c = 'testPassword')
		};

		System.runAs(uFr) {

			Case ENTPCase = new Case(Subject = 'Test Subject', Origin = 'ENTP', My_Business_Requests_Type__c = 'Other', Lynx_Ticket_Number__c = '5555555555');
			insert ENTPCase;
			Case ENTPTestCase = [SELECT Id, Subject, RecordTypeId, Contact.FirstName, Lynx_Ticket_Number__c, Contact.LastName, My_Business_Requests_Type__c, CaseNumber, Status, LastModifiedDate FROM Case WHERE Id = :ENTPCase.Id LIMIT 1];

			ENTPOverviewCtlr1 overviewContr = new ENTPOverviewCtlr1();
			overviewContr.initCheck();

			Test.startTest();
			String CustRole = 'private cloud';
			//overviewContr.CustRole1 = CustRole;
			overviewContr.statusFilter = 'My tickets';
			String SFDCAccountId = 'test';
			System.assertEquals(SFDCAccountId, 'test');
			//overviewContr.CatListPull1='test';
			overviewContr.AddFuncPull = 'Test';
			overviewContr.AddFuncPull1 = 'Test';
			overviewContr.lynxTicketNo = '11111111';
			overviewContr.refNo = '123345566';
			overviewContr.telusID = '70208';
			overviewContr.RCID = '111111';

			//System.assertEquals( CatListPull1, 'test');
			//test search functionality
			Case c = [SELECT caseNumber from Case Where Id = :ENTPTestCase.Id LIMIT 1];
			//overviewContr.q = 'Test';
			overviewContr.searchCase();
			//System.assertEquals(1, overviewContr.totalCases);

			Id [] fixedSearchResults = new Id[]{
					c.Id
			};
			Test.setFixedSearchResults(fixedSearchResults);

			overviewContr.q = '11111111';
			List<Case> cases = overviewContr.executeSearch(1, 1, true);
			//System.assertEquals(1, cases.size());

			overviewContr.callMethod();
			//test search functionality
			Case cN = [SELECT caseNumber from Case Where Id = :ENTPTestCase.Id LIMIT 1];
			//overviewContr.q = 'Test';
			overviewContr.searchCase();
			//System.assertEquals(1, overviewContr.totalCases);

			Id [] fixedSearchResultsN = new Id[]{
					cN.Id
			};
			Test.setFixedSearchResults(fixedSearchResultsN);

			overviewContr.q = '5555555556';
			List<Case> casesN = overviewContr.executeSearch(1, 1, true);
			//System.assertEquals(1, cases.size());

		}
		System.runAs(uFr) {

			Case ENTPCase = new Case(Subject = 'Test Subject', Origin = 'ENTP', My_Business_Requests_Type__c = 'Other', Lynx_Ticket_Number__c = '5555555555');
			insert ENTPCase;
			Case ENTPTestCase = [SELECT Id, Subject, RecordTypeId, Contact.FirstName, Lynx_Ticket_Number__c, Contact.LastName, My_Business_Requests_Type__c, CaseNumber, Status, LastModifiedDate FROM Case WHERE Id = :ENTPCase.Id LIMIT 1];

			ENTPOverviewCtlr1 overviewContr1 = new ENTPOverviewCtlr1();
			overviewContr1.initCheck();

			String CustRole = 'tps';
			String CustRole1 = CustRole;
			String SFDCAccountId = 'test';
			System.assertEquals(SFDCAccountId, 'test');
			//overviewContr1.CatListPull1='test';
			overviewContr1.AddFuncPull = 'Test';
			overviewContr1.AddFuncPull1 = 'Test';
			overviewContr1.lynxTicketNo = '11111111';
			overviewContr1.refNo = '123345566';
			overviewContr1.telusID = '70208';
			overviewContr1.RCID = '111111';
			//System.assertEquals( CatListPull1, 'test');
			//test search functionality
			Case c = [SELECT caseNumber from Case Where Id = :ENTPTestCase.Id LIMIT 1];
			//overviewContr.q = 'Test';
			overviewContr1.searchCase();
			//System.assertEquals(1, overviewContr.totalCases);

			Id [] fixedSearchResults = new Id[]{
					c.Id
			};
			Test.setFixedSearchResults(fixedSearchResults);

			overviewContr1.q = 'Test';
			List<Case> cases = overviewContr1.executeSearch(1, 1, true);
			//System.assertEquals(1, cases.size());

			overviewContr1.callMethod();
			//test search functionality
			Case cN = [SELECT caseNumber from Case Where Id = :ENTPTestCase.Id LIMIT 1];
			//overviewContr.q = 'Test';
			overviewContr1.searchCase();
			//System.assertEquals(1, overviewContr.totalCases);

			Id [] fixedSearchResultsN = new Id[]{
					cN.Id
			};
			Test.setFixedSearchResults(fixedSearchResultsN);

			overviewContr1.q = '5555555556';
			List<Case> casesN = overviewContr1.executeSearch(1, 1, true);
			//System.assertEquals(1, cases.size());

			Test.stopTest();
		}
	}

	@isTest(SeeAllData=true)
	static void testGetCaseDetails() {
		Case ENTPCase = new Case(Subject = 'Test Subject', Origin = 'ENTP', My_Business_Requests_Type__c = 'Other', Lynx_Ticket_Number__c = '5555555555', RecordTypeId = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId);
		insert ENTPCase;
		Case ENTPTestCase = [SELECT Id, Subject, RecordTypeId, Contact.FirstName, Lynx_Ticket_Number__c, Contact.LastName, My_Business_Requests_Type__c, CaseNumber, Status, LastModifiedDate FROM Case WHERE Id = :ENTPCase.Id LIMIT 1];

		ENTPOverviewCtlr1 ctlr = new ENTPOverviewCtlr1();
		ctlr.caseNumber = ENTPTestCase.CaseNumber;
		System.assert(ctlr.getCaseDetails().getUrl().containsIgnoreCase('detail'));
		ctlr.categoryType = 'Test';
		ENTPOverviewCtlr1 ctlr2 = new ENTPOverviewCtlr1();
		ctlr2.caseNumber = ENTPTestCase.CaseNumber;
		System.assert(ctlr2.getCaseDetails().getUrl().containsIgnoreCase('detail'));
	}
	@isTest(SeeAllData=false)
	static void testGetdoFilterUpdate() {
		ENTPOverviewCtlr1 ctlr = new ENTPOverviewCtlr1();
		PageReference pref = ctlr.doFilterUpdate();
	}

	@isTest(SeeAllData=false)
	static void testOtherMethods() {
		ENTPTestUtils.createSMBCareWebServicesCustomSetting();
		Integer selectedPage = 0;
		List<Case> cases1 = new List<Case>();
		Case ENTPCase1 = new Case(Subject = 'Test Subject', Origin = 'ENTP', My_Business_Requests_Type__c = 'Other', RecordTypeId = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId);
		cases1.add(ENTPCase1);

		insert cases1;

		case ENTPTestCase = [SELECT Id, Subject, RecordTypeId, Contact.FirstName, Lynx_Ticket_Number__c, Contact.LastName, My_Business_Requests_Type__c, CaseNumber, Status, LastModifiedDate FROM Case WHERE Id = :ENTPCase1.Id LIMIT 1];
		// ENTPOverviewCtlr1.CaseListing cl = new ENTPOverviewCtlr1.CaseListing(ENTPTestCase);

		ENTP_Customer_Interface_Settings__c ENTPcis = new ENTP_Customer_Interface_Settings__c(Manage_Requests_List_Limit__c = null,
				Overview_Case_Limit__c = null);
		insert ENTPcis;
		ENTP_Customer_Interface_Settings__c cis = ENTP_Customer_Interface_Settings__c.getInstance();
		system.debug('CIS:=' + cis);
		/*   if(cis != null){

				   delete cis;


			}
	  System.assert(cis == null);*/
		ENTPOverviewCtlr1 ctlr = new ENTPOverviewCtlr1();
		ctlr.categoryType = null;
		//ctlr.createNewCase();
		// paging

		ctlr.totalPages = 20;
		ctlr.selectedPage = 5;
		ctlr.searchOverview();
		ctlr.resetSearch();
		ctlr.abbreviate('Testing', 4);
		System.assert(ctlr.pageNumbers.size() > 0);
		// System.assert(cl.allListings.size() > 0);
		System.assert(ctlr.refreshGrid() == null);
		ctlr.Previous();
		ctlr.Next();
		System.assert(ctlr.c == null);
		System.assert(ctlr.pageError == null);
		// System.assert(ctlr.Previous());
		// System.assert(ctlr.Next());
		System.assert(ctlr.getDisablePrevious() == false);
		System.assert(ctlr.getDisableNext() == false);

		ctlr.totalPages = 1;
		ctlr.selectedPage = 4;
		System.assert(ctlr.pageNumbers.size() > 0);
		System.assert(ctlr.TotalPages > 0);

		// filters
		System.assert(ctlr.getCategoryFilterOptions().size() > 0);
		System.assert(ctlr.getStatusFilterOptions().size() > 0);

		// internal class test

		//   System.assert(cl.getTimeDiff(DateTime.now().addMinutes(-60)).containsIgnoreCase('1 hour ago'));
		//  System.assert(cl.getTimeDiff(DateTime.now().addHours(-20)).containsIgnoreCase('hours ago'));
		//System.assert(cl.getTimeDiff(DateTime.now().addHours(-40)).containsIgnoreCase('hours ago'));
		// System.assert(cl.getTimeDiff(DateTime.now().addDays(-20)).containsIgnoreCase('days ago'));
		//  System.assert(cl.getTimeDiff(DateTime.now().addDays(-31)).containsIgnoreCase('over 1 month'));
		//  System.debug(cl.getTimeDiff(DateTime.now().addYears(2)));
		//System.assert(cl.getTimeDiff(DateTime.now().addYears(2)).containsIgnoreCase('months ago'));
		Test.startTest();
		User u = MBRTestUtils.createENTPPortalUser('test-user-1', false, 'en_US');
		System.runAs(u) {
			String userAccountId = [SELECT AccountId FROM User WHERE Id = :UserInfo.getUserId()].AccountId;
			// system.assert(String.isNotEmpty(userAccountId));
			List<account> Accounts = [SELECT Strategic__c, ID from Account where ID = :userAccountId];
			ctlr.getLanguageLocaleKey();
			String userLanguage = [SELECT LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()].LanguageLocaleKey;
			// system.assert(String.isNotEmpty(userLanguage));
			ctlr.getSearchError();
			// account a = new account(name = 'test acct');
			//insert a;

			List<Case> testCases = ENTPTestUtils.createENTPCases(2, userAccountId);

			List<Case> caseList1 = [SELECT createdby.firstname, createdby.lastname, Lynx_Ticket_Number__c, NotifyCustomer__c, NotifyCollaboratorString__c, OwnerId, Resolution_Details__c, My_Business_Requests_Type__c, ParentId, isClosed, Contact.FirstName, Contact.LastName, id,contact.name,subject,description,lastmodifieddate,case_resolution__c,createddate, caseNumber, recordType.name, account.name, type, status, priority, isEscalated, Escalation_History__c, (select Active__c,Collaborator_Email__c, Collaborator_Name__c, Role__c from Collaborator__r order by createddate desc), (select createdby.firstname, createdby.lastname,Id,Name,Description,ParentId,CreatedDate,LastModifiedDate, ContentType from Attachments order by LastModifiedDate asc), (select createdby.firstname, createdby.lastname,Id,CommentBody,ParentId,CreatedDate,LastModifiedDate from casecomments where IsPublished = true order by CreatedDate asc), (select id, toaddress,fromaddress,fromname,ccaddress,messagedate,subject,htmlbody,textbody,hasattachment from emailmessages) FROM Case WHERE Id = :testCases[0].Id];
			Case cs1 = caseList1[0];
			ctlr.LynxTicketSearchRET(cs1.Lynx_Ticket_Number__c);
			// ctlr.PinNumRet();
			//ctlr.GetLynxTicketInfo(cs1.id);
			Test.stopTest();
		}

	}

/* @isTest
static void valueFromReader(){
ENTPOverviewCtlr1 ctlr = new ENTPOverviewCtlr1();
     XmlStreamReader reader = null;
ctlr.getValueFromReader(reader,'test',false);
} */
	//ENTPTestUtils.createSMBCareWebServicesCustomSetting();
	// List<Case> cases1 = new List<Case>();

	//  Case ENTPCase1 = new Case(Subject = 'Test Subject', Origin = 'ENTP', My_Business_Requests_Type__c = 'Other', RecordTypeId = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId);
	//  cases1.add(ENTPCase1);

	//  insert cases1;

/***** CAUSING ERROR: Class.ENTPOverviewCtlr1.resetRequestView: line 393  */
	//case ENTPTestCase1 = [SELECT Id, Subject, RecordTypeId, Contact.FirstName, Lynx_Ticket_Number__c, Contact.LastName, My_Business_Requests_Type__c, CaseNumber, Status, LastModifiedDate FROM Case WHERE Id = :ENTPCase1.Id LIMIT 1];
	@isTest(SeeAllData=false) static void testFrMethods() {
		User f = MBRTestUtils.createENTPPortalUser('test-user-2', false, 'fr');
		ENTPTestUtils.createENTPCustomSetting();
		ENTPTestUtils.createSMBCareWebServicesCustomSetting();

		System.runAs(f) {
			//Case ENTPCase3 = new Case(Subject = 'Test Subject', Origin = 'ENTP', My_Business_Requests_Type__c = 'Other', Lynx_Ticket_Number__c = '5555555555', RecordTypeId = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId);
			//insert ENTPCase3;
			String userAccountId = [SELECT AccountId FROM User WHERE Id = :UserInfo.getUserId()].AccountId;
			system.assert(String.isNotEmpty(userAccountId));
			List<Case> FrCase = MBRTestUtils.createENTPCases(1, userAccountId);
			Case ENTPTestCase3 = [
					SELECT Id, Subject, RecordTypeId, Contact.FirstName, Lynx_Ticket_Number__c, Contact.LastName,
							My_Business_Requests_Type__c, CaseNumber, Status, LastModifiedDate, Trouble_Ticket__c
					FROM Case
					WHERE Id = :FrCase[0].Id
					LIMIT 1
			];
			ENTPOverviewCtlr1 ctlr = new ENTPOverviewCtlr1();

			ctlr.initCheck();
			Test.startTest();

			List<account> Accounts = [SELECT Strategic__c, ID from Account where ID = :userAccountId];
			ctlr.getLanguageLocaleKey();
			String userLanguage = [SELECT LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()].LanguageLocaleKey;
			system.debug('UserLanguage-TEST:' + userLanguage);
			system.assert(String.isNotEmpty(userLanguage));
			String CustRole = 'private cloud';
			String CustRole1 = CustRole;
			system.debug('CustRole1-TEST:' + CustRole1);
			String SFDCAccountId = 'test';
			System.assertEquals(SFDCAccountId, 'test');
			String CatListPull1 = 'test';
			System.assertEquals(CatListPull1, 'test');
			// List<ENTP_portal_additional_functions__c> ENTPPortalMap = [Select Name,Customer_Portal_Role__c,Additional_Function__c from ENTP_portal_additional_functions__c where Customer_Portal_Role__c =: CustRole];
			//system.assert(String.isNotEmpty(ENTPPortalMap[0].Additional_Function__c));
			String AddFuncPull1 = 'test';
			//System.assertEquals( AddFuncPull1, 'test');
			string CatListPull = 'test';
			//   String CatListPull1='test';
			System.assertEquals(CatListPull1, 'test');
			// String AddFuncPull1='test';
			System.assertEquals(AddFuncPull1, 'test');
			//System.assertEquals( CatListPull, CatListPull);
			List<User> users = [SELECT Id, AccountId, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()];
			system.assert(String.isNotEmpty(users.get(0).AccountId));

			Portal_OverviewCtlr.CaseListing cl = new Portal_OverviewCtlr.CaseListing(ENTPTestCase3, 'ENTP');
			cl.getTimeDiff(DateTime.now().addMinutes(-60));
			cl.getTimeDiff(DateTime.now().addHours(-20));
			cl.getTimeDiff(DateTime.now().addHours(-40));
			cl.getTimeDiff(DateTime.now().addDays(-20));
			cl.getTimeDiff(DateTime.now().addDays(-31));
			cl.getTimeDiff(DateTime.now().addYears(2));
			Test.stopTest();
		}

	}

	@isTest(SeeAllData=false) static void testMethods() {
		User u = MBRTestUtils.createENTPPortalUser('test-user-2', false, 'en_US');
		ENTPTestUtils.createENTPCustomSetting();
		ENTPTestUtils.createSMBCareWebServicesCustomSetting();

		System.runAs(u) {
			//Case ENTPCase3 = new Case(Subject = 'Test Subject', Origin = 'ENTP', My_Business_Requests_Type__c = 'Other', Lynx_Ticket_Number__c = '5555555555', RecordTypeId = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId);
			//insert ENTPCase3;
			String userAccountId = [SELECT AccountId FROM User WHERE Id = :UserInfo.getUserId()].AccountId;
			system.assert(String.isNotEmpty(userAccountId));
			List<Case> FrCase = MBRTestUtils.createENTPCases(1, userAccountId);
			Case ENTPTestCase3 = [
					SELECT Id, Subject, RecordTypeId, Contact.FirstName, Lynx_Ticket_Number__c, Contact.LastName,
							My_Business_Requests_Type__c, CaseNumber, Status, LastModifiedDate, Trouble_Ticket__c
					FROM Case
					WHERE Id = :FrCase[0].Id
					LIMIT 1
			];
			ENTPOverviewCtlr1 ctlr = new ENTPOverviewCtlr1();

			ctlr.initCheck();
			Test.startTest();

			List<account> Accounts = [SELECT Strategic__c, ID from Account where ID = :userAccountId];
			ctlr.getLanguageLocaleKey();
			String userLanguage = [SELECT LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()].LanguageLocaleKey;
			system.debug('UserLanguage-TEST:' + userLanguage);
			system.assert(String.isNotEmpty(userLanguage));
			String CustRole = 'private cloud';
			String CustRole1 = CustRole;
			system.debug('CustRole1-TEST:' + CustRole1);
			String SFDCAccountId = 'test';
			System.assertEquals(SFDCAccountId, 'test');
			String CatListPull1 = 'test';
			System.assertEquals(CatListPull1, 'test');
			// List<ENTP_portal_additional_functions__c> ENTPPortalMap = [Select Name,Customer_Portal_Role__c,Additional_Function__c from ENTP_portal_additional_functions__c where Customer_Portal_Role__c =: CustRole];
			//system.assert(String.isNotEmpty(ENTPPortalMap[0].Additional_Function__c));
			String AddFuncPull1 = 'test';
			//System.assertEquals( AddFuncPull1, 'test');
			string CatListPull = 'test';
			//   String CatListPull1='test';
			System.assertEquals(CatListPull1, 'test');
			// String AddFuncPull1='test';
			System.assertEquals(AddFuncPull1, 'test');
			//System.assertEquals( CatListPull, CatListPull);
			List<User> users = [SELECT Id, AccountId, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()];
			system.assert(String.isNotEmpty(users.get(0).AccountId));

			Portal_OverviewCtlr.CaseListing cl = new Portal_OverviewCtlr.CaseListing(ENTPTestCase3, 'ENTP');
			cl.getTimeDiff(DateTime.now().addMinutes(-60));
			cl.getTimeDiff(DateTime.now().addHours(-20));
			cl.getTimeDiff(DateTime.now().addHours(-40));
			cl.getTimeDiff(DateTime.now().addDays(-20));
			cl.getTimeDiff(DateTime.now().addDays(-31));
			cl.getTimeDiff(DateTime.now().addYears(2));
			Test.stopTest();
		}

	}

	@isTest
	public static void testMisc() {
		ENTPOverviewCtlr1 controller = new ENTPOverviewCtlr1();
		controller.getPageOfCases(1, 1);
		controller.pageSize = 1;
		//List<Portal_OverviewCtlr.CaseListing> caseListings = controller.allListings;
	}
}