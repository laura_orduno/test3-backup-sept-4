/**
* Description of the purpose of the class:
This class provides methods to save service address and invoke the integration points to load the service address extended attributes. Also class also checks if a service address is already registered for that customer account. If it exists, it will update the service address details with the new values. Otherwise, it will create a new service address.
Before the service address is saved, it will invoke a web service call to FMS to get the additional address fields those are missing from Autonomy.
It will also handle the manual override scenarios where the user selects the service address types and fills in other relevant details and save the address.

* Author: Aditya Jamwal (IBM) / Sandip Chaudhari / Sukhdeep Singh
* Date: 01-oct-2015
* Project/Release: Predictive Address Capture
* Change Control Information:
* Date Who Reason for the change
* ###Sandip - 25 Jan 2016 - PAC R2 Changes
* 30 March 2017 - Defect # 11914 - Sandip - To show error message from webservice
* 27 Sept 2017 - Defect # 16420- Sandip - hide temporary job phone to support NaaS flow. Depricate NAAS_PACServiceAddressComponent.component
*/
global class PACServiceAddressComponentController{
 
    public String idx {get;set;}
    public ServiceAddressData cData {get;set;}
    public String PACconfigName {get;set;}
    public Boolean useAsAttri {get;set;}
    public String FMSCheckStatus {get;set;}
    public PACConfig__c pacConfigDetails {get;set;}
    public PACConfig__c pacPAConfigDetails {get;set;}
    public string Init{set;}
    public string config{set;}
    public Boolean isNewAddressCapture{get;set;}
    public String PACPAconfigName{get;set;}
    public static String keyConfig {get;set;}
    public String pacJSconfigName {get;set;}
    public Boolean isUsedInVFInd {get;set;}
    public Boolean isFmsCheckInd {get;set;}
    public String captureNewLabel{get;set;}
    public String onSelectElementId{get;set;} // Change event get fire after selecting address from drop down
    public String onCheckElementId{get;set;} // Change event get fire after check 'Capture New Address' checkbox
    public static String apiKeyErrorMessage{get;set;}
    public List<selectOption> countryList {get;set;}
    public String selectedLanguages {get;set;}
    public Boolean connectPostalAddress {get;set;}
    public List<selectOption> usStateList {get;set;}
    public List<selectOption> canadaProvinceList {get;set;}
    public Boolean isSACG {get;set;}
    public String sacgMessage {get;set;}
    public Boolean isValidateThruSACG{get;set;}
    public PACUtility pacUtil = new PACUtility();
    
    // PAC R2 Changes
    public String inValidStatus{get;set;}
    public String validStatus{get;set;}
    public Boolean isCheckAddress{get;set;}
    public Static boolean ServiceAddCall = False;
   
      // public String selectedProvience {get;set;}
    
  /*  public string getInit(){
      pacConfigDetails = PACConfig__c.getInstance(pacConfigName);
      return pacConfigName;
    }*/
    
    
    public String elementIdOnAddressReady{get;set;}
    // Defect # 11914 - 30 March 2017 - Sandip - To show error message from webservice
    public String errorMessage{get;set;}
    // End
    public boolean isHideTempJobPhone{get;set;} // 27 Sept 2017 - Defect # 16420- Sandip - hide temporary job phone to support NaaS flow.
    public PACServiceAddressComponentController()  
      { 
        system.debug('In constructor ###' + captureNewLabel ); 
        pacConfigDetails = new PACConfig__c();
        isNewAddressCapture = false;
        countryList = new List<SelectOption>();
        usStateList = new List<SelectOption>();
        canadaProvinceList = new List<SelectOption>();
        pacUtil.getCountryStateList();
        countryList = pacUtil.countryList;
        usStateList = pacUtil.usStateList;
        canadaProvinceList = pacUtil.canadaProvinceList;
        //getCountries();
        //getCanadaProvince();
        //getUSstates();
        //getCountryStateList();
        selectedLanguages = UserInfo.getLanguage();
        if(selectedLanguages != null && selectedLanguages.length() > 2){
           selectedLanguages = selectedLanguages.substring(0,2); 
        }
       // if(isFmsCheckInd==true){
            if(isCheckAddress==true) // Introduced new parameter in PAC R2.
            {
             checkAddress();
            }
       // }
      }
  
   public String getConfig(){
        // PAC R2 Changes
        inValidStatus = System.Label.PACInValidCaptureNewAddrStatus;
        validStatus = System.Label.PACValidCaptureNewAddrStatus;
        // PAC R2 Changes
        
        keyConfig = PACPAconfigName;
        pacConfigDetails = PACConfig__c.getInstance(pacConfigName); // Fetch configuration for service autonomy
        system.debug('@@@keyConfig ' + keyConfig);
        if(PACPAconfigName == null){
            PACPAconfigName = 'billing';  // Fetch configuration for Canada Post. This hard coded value will be used if user not passing configuration name from outer context.
        }
        pacPAConfigDetails = PACConfig__c.getInstance(PACPAconfigName);
        List<ExternalPACJSName__c> allData = ExternalPACJSName__c.getAll().values();
        if(cData!=null && cData.country == null){
            cData.country = 'CAN';
        }
        if(allData != null){
            for (ExternalPACJSName__c obj: allData){
                if (obj.Is_Active__c){
                    pacJSconfigName = obj.Name;
                    break;
                }
            }
       }
       if(captureNewLabel == null){
            captureNewLabel = System.Label.PACCaptureNewAddress;
       }
       cData.isSACG = false;
       system.debug('In Config ###' + captureNewLabel );
       return pacConfigName;
    }
  
 
   /*
    * getServiceAddressDetails - This method is used to get additional details from the FMS system
    * Parameter -
    * Return Type - void
    */

    public void getServiceAddressDetails(){
        system.debug('getFMSdetails');
        system.debug('SUKHDEEP PACServiceAddressComponentController.getServiceAddressDetails  cData  ===>  '+cData);
        String addressId,regionId;
        
        try{
            // 30 March 2017 - Sandip - To show error message from webservice
            errorMessage = '';
            // End
            
            SMBCare_WebServices__c cs = SMBCare_WebServices__c.getInstance('username');
            String userId = cs.value__c;
            system.debug('username@@@' + userId);

            if(null != cData){  
                // TC03359357
                //  
                // 
                if(String.isBlank(cData.fmsId)){
                    // set
                    cData.isASFDown = true;    
                    logError('PACServiceAddressComponentController.getServiceAddressDetails', '', 'FMS ID is blank.', '');
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'FMS ID is blank.');
                    ApexPages.addMessage(msg);                     
                }
                // 
                
                
                cData.isASFDown = false; //31 March 2017 - Defect # 11733 - Added by Sandip
                //cData.fmsId = '1895080';
                //cData.province = 'AB';
                system.debug('addressId fmsId '+cData.fmsId);
                system.debug('addressId'+cData.province);
                addressId=cData.fmsId;
                regionId=cData.province;

                /* Update by Arvind :-
                Replaces SRS_ASFService to SRS_ServiceAddressManagementService_1, ServiceAddressManagementServicePort to ServiceAddressManagementService_v1_1_SOAP
                and SRS_ASFValidationReqResponse to SRS_ServiceAddressMgmtRequestRes (To call new Version "ServiceAddressService 1.1" of classes)..
                */
                /*
                SRS_ASFService.ServiceAddressManagementServicePort asfObj = new SRS_ASFService.ServiceAddressManagementServicePort();  
                SRS_ASFValidationReqResponse.ServiceAddress asfaddress  = new SRS_ASFValidationReqResponse.ServiceAddress();     
                SRS_ASFValidationReqResponse.getServiceAddressDetailsResponse_element respDetails = new SRS_ASFValidationReqResponse.getServiceAddressDetailsResponse_element();
                */

                SRS_ServiceAddressManagementService_1.ServiceAddressManagementService_v1_1_SOAP asfObj = new SRS_ServiceAddressManagementService_1.ServiceAddressManagementService_v1_1_SOAP();  
                SRS_ServiceAddressMgmtRequestRes.ServiceAddress asfaddress  = new SRS_ServiceAddressMgmtRequestRes.ServiceAddress();
                SRS_ServiceAddressMgmtRequestRes.LikeMunicipalityItem LikeMunciplity = new SRS_ServiceAddressMgmtRequestRes.LikeMunicipalityItem ();
                SRS_ServiceAddressMgmtRequestRes.getServiceAddressDetails_element respDetails = new SRS_ServiceAddressMgmtRequestRes.getServiceAddressDetails_element();
                //Added by Arvind as it was moved under Comman Types Class with new Service
                SRS_ServiceAddressCommonAddressTypes_v2.Address asfCommanaddress  = new SRS_ServiceAddressCommonAddressTypes_v2.Address();
                
                asfCommanaddress.addressId = addressId;
                asfCommanaddress.provinceStateCode = regionId;                
                system.debug('asfCommanaddress ===> ' + asfCommanaddress);
                SRS_ServiceAddressMgmtRequestRes.getServiceAddressDetails_element requestElem=new SRS_ServiceAddressMgmtRequestRes.getServiceAddressDetails_element();
                requestElem.address=asfCommanaddress;
                SRS_ServiceAddressMgmtRequestRes.ServiceAddress saRequestElem=new SRS_ServiceAddressMgmtRequestRes.ServiceAddress();
                //saRequestElem.address=requestElem;                
                respDetails = asfObj.getServiceAddressDetails(userId ,system.now(),requestElem);
                system.debug('respDetails ===> ' + respDetails);
                //Added by Arvind##                
                 // Added by Arvind##
                SRS_ServiceAddressCommonAddressTypes_v2.ResourceLocationAddressComponent respResourceLocationAddressComponent = new SRS_ServiceAddressCommonAddressTypes_v2.ResourceLocationAddressComponent();
                 assignFMSDetails(respDetails.address);
                 PACServiceAddressComponentController.ServiceAddCall = True;
                system.debug('@@FMSResult'+respDetails.address);
            }
        }catch(Exception ex){system.debug('IN EXCEPTION !!!');
            system.debug('@@IN EXCEPTION '+ex);
            cData.isASFDown = true; //31 March 2017 - Defect # 11733 - Added by Sandip   
            // Defect # 11914 - 30 March 2017 - Sandip - To show error message from webservice
            errorMessage = Label.PAC_ASFServiceDownMessage;
            logError('PACServiceAddressComponentController.getServiceAddressDetails','',ex.getMessage(),ex.getstacktracestring());
            // End                          
            ApexPages.addMessages(ex); 
        }
        
    }
    
    // Defect # 11914 - 30 March 2017 - Sandip - To show error message from webservice
    public static void logError(String classAndMethodName, String recId, String errorCodeAndMsg, String stackTrace){
        webservice_integration_error_log__c errorLog = new webservice_integration_error_log__c
                                                    (apex_class_and_method__c=classAndMethodName,
                                                     external_system_name__c='ASF',
                                                     webservice_name__c='ASF',
                                                     object_name__c='PAC Component',
                                                     sfdc_record_id__c=recId,
                                                     Error_Code_and_Message__c = errorCodeAndMsg,
                                                     Stack_Trace__c = stackTrace
                                                     );
        insert errorLog;
    }
    // End
    
    /* Added by Sandip
    * This method is used to check address if valid or invalid if isCheckAddress = true.
    * Method will get invoke only if isCheckAddress = true, This will be set from outer context and pas it to component as a paraemeter
    * return 'Valid 'if address is valid oterwise return 'Not Valid';
    */
    
    public void checkAddress(){
        if(isCheckAddress==true){
        try{
            SMBCare_WebServices__c cs = SMBCare_WebServices__c.getInstance('username');
            String userId = cs.value__c;
            system.debug('username@@@' + userId);
            system.debug('SUKHDEEP getServiceAddressDetails.checkAddress  cData  ===>  '+cData);
            FMSCheckStatus = System.Label.PACNotValidStatus;
            
            /* Update by Arvind :-
            Replaces SRS_ASFService to SRS_ServiceAddressManagementService_1, ServiceAddressManagementServicePort to ServiceAddressManagementService_v1_1_SOAP
            and SRS_ASFValidationReqResponse to SRS_ServiceAddressMgmtRequestRes (To call new Version "ServiceAddressService 1.1" of classes)..
            */
            /*
            SRS_ASFService.ServiceAddressManagementServicePort asfObj = new SRS_ASFService.ServiceAddressManagementServicePort();  
            SRS_ASFValidationReqResponse.ServiceAddress address  = new SRS_ASFValidationReqResponse.ServiceAddress();
            SRS_ASFValidationReqResponse.searchServiceAddressResponse_element respSearch = new SRS_ASFValidationReqResponse.searchServiceAddressResponse_element();
            */          
            
            SRS_ServiceAddressManagementService_1.ServiceAddressManagementService_v1_1_SOAP asfObj = new SRS_ServiceAddressManagementService_1.ServiceAddressManagementService_v1_1_SOAP();  
           
            SRS_ServiceAddressMgmtRequestRes.ServiceAddress address  = new SRS_ServiceAddressMgmtRequestRes.ServiceAddress();
            SRS_ServiceAddressCommonAddressTypes_v2.Address addressTyp  = new SRS_ServiceAddressCommonAddressTypes_v2.Address();
            
            SRS_ServiceAddressMgmtRequestRes.searchServiceAddressResponse_element respSearch = new SRS_ServiceAddressMgmtRequestRes.searchServiceAddressResponse_element();
            
            addressTyp.countryCode = 'CAN';
            addressTyp.municipalityName = cData.city;
            addressTyp.provinceStateCode = cData.province;
         //   address.streetName = cData.street;
            addressTyp.postalZipCode = cData.postalCode;       
            addressTyp.unitNumber = cData.suiteNumber;
            //Added by Arvind 
            SRS_ServiceAddressCommonAddressTypes_v2.ResourceLocationAddressComponent LocAddComp = new SRS_ServiceAddressCommonAddressTypes_v2.ResourceLocationAddressComponent();
            LocAddComp.vector = cData.streetDirection;
            //address.vector = cData.streetDirection; 
            
            respSearch = asfObj.searchServiceAddress(userId ,system.now(),address,10,'','','','');
            // Update by Arvind replacse SRS_ASFValidationReqResponse to SRS_ServiceAddressMgmtRequestRes to call new version of Service..
            SRS_ServiceAddressMgmtRequestRes.ServiceAddress respaddress  = new SRS_ServiceAddressMgmtRequestRes.ServiceAddress();  
                    
            SRS_ServiceAddressMgmtRequestRes.LikeStreetList respLikeStreet = new SRS_ServiceAddressMgmtRequestRes.LikeStreetList();
            SRS_ServiceAddressMgmtRequestRes.likeMunicipalityList respLikeMunicipality = new SRS_ServiceAddressMgmtRequestRes.likeMunicipalityList();
            SRS_ServiceAddressMgmtRequestRes.likeHouseList respLikeHouse = new SRS_ServiceAddressMgmtRequestRes.likeHouseList();
            SRS_ServiceAddressMgmtRequestRes.likeApartmentList respLikeApartment = new SRS_ServiceAddressMgmtRequestRes.likeApartmentList();
            
            // Added by Arvind##
            SRS_ServiceAddressCommonAddressTypes_v2.ResourceLocationAddressComponent respResourceLocationAddressComponent = new SRS_ServiceAddressCommonAddressTypes_v2.ResourceLocationAddressComponent();
            SRS_ServiceAddressCommonAddressTypes_v2.CivicAddress CivicResponse = new SRS_ServiceAddressCommonAddressTypes_v2.CivicAddress();
            SRS_ServiceAddressCommonAddressTypes_v2.RuralAddress RuralResponse = new SRS_ServiceAddressCommonAddressTypes_v2.RuralAddress();
            SRS_ServiceAddressCommonAddressTypes_v2.StationSquareAddress StationResponse = new SRS_ServiceAddressCommonAddressTypes_v2.StationSquareAddress();
            SRS_ServiceAddressCommonAddressTypes_v2.MilitaryAddress MilitaryResponse = new SRS_ServiceAddressCommonAddressTypes_v2.MilitaryAddress();
            SRS_ServiceAddressCommonAddressTypes_v2.Address AddressResponse = new SRS_ServiceAddressCommonAddressTypes_v2.Address();
            
            respLikeStreet = respSearch.likeStreetList;
            respLikeMunicipality = respSearch.likeMunicipalityList;
            respLikeHouse = respSearch.likeHouseList;
            respLikeApartment = respSearch.likeApartmentList;
            respaddress = respSearch.address; 
                       
            // If exact match found, updates Status to Valid        
            if((respAddress!=NULL)&&(respLikeStreet==NULL)&&(respLikeMunicipality==NULL)&&(respLikeApartment==NULL)&&(respLikeHouse==NULL)){
                FMSCheckStatus = System.Label.PACValidStatus;
                cData.addrStatus = FMSCheckStatus;
               // assignFMSDetails(respaddress, respResourceLocationAddressComponent, CivicResponse, RuralResponse,StationResponse,MilitaryResponse,AddressResponse);
                system.debug('@@FMSResult'+respaddress);
            }else{
                FMSCheckStatus = System.Label.PACNotValidStatus;
                cData.addrStatus = FMSCheckStatus;
            }
        }catch(Exception ex){system.debug('IN EXCEPTION !!!');
               ApexPages.addMessages(ex); 
        }        
           // return FMSCheckStatus;
       }
    }
    
    /* Added by Sandip
    * assignFMSDetails - Mapping data from FMS to ServiceAddressData instance
    * Parameter - SRS_ASFValidationReqResponse.ServiceAddress
    * return type - void
    */
    //Method Updated by Arvind as all variables were not present with RS_ServiceAddressMgmtRequestRes.ServiceAddress..
    // public void assignFMSDetails(SRS_ServiceAddressMgmtRequestRes.ServiceAddress respData, SRS_ServiceAddressCommonAddressTypes_v2.ResourceLocationAddressComponent ResourceLocationAddress, SRS_ServiceAddressCommonAddressTypes_v2.CivicAddress CivicResponse, SRS_ServiceAddressCommonAddressTypes_v2.RuralAddress RuralResponse, SRS_ServiceAddressCommonAddressTypes_v2.StationSquareAddress StationResponse   ,SRS_ServiceAddressCommonAddressTypes_v2.MilitaryAddress MilitaryResponse,SRS_ServiceAddressCommonAddressTypes_v2.Address AddressResponse){
     public void assignFMSDetails(SRS_ServiceAddressCommonAddressTypes_v2.Address respData){
      system.debug('@@@respData.CLLICode' + respData.CLLICode);
      cData.clliCode = respData.CLLICode;
      system.debug('@@@cData.clliCode' + cData.clliCode);
        cData.coid     = respData.COID;
        cData.localRoutingNumber = respData.localRoutingNumber;
        cData.ratingNpaNxx = respData.ratingNpaNxx;
        // 14 June 2016 - TC0366151 - Production defect
        //cData.npa = respData.npa;
        //cData.lowestNxx = respData.lowestNxx;
        cData.npa = string.isBlank(respData.npa)?'':respData.npa;
        cData.lowestNxx = string.isBlank(respData.lowestNxx)?'':respData.lowestNxx;
        // End
        cData.switchNumber = respData.switchNumber;
        cData.terminalNumber = respData.terminalNumber;
        
        //Added in PAC 2 changes
        cData.civicNumber = respData.civicNumber;
        cData.civicNumberSuffix  = respData.civicNumberSuffix;
        cData.canadaPostBuildName = respData.canadaPostBuildName;
        cData.canadaPostLocnName = respData.canadaPostLocnName; 
        cData.canadaPostRecordType = respData.canadaPostRecordType;
        cData.careOf = respData.careOf;
        cData.ruralRouteNumber = respData.ruralRouteNumber;
        cData.ruralRouteTypeCode = respData.ruralRouteTypeCode;
        cData.stationName = respData.stationName;
        cData.stationQualifier = respData.stationQualifier;
        cData.stationTypeCode = respData.stationTypeCode;
        cData.fleetMailOfficeName = respData.fleetMailOfficeName;
        cData.hmcsName = respData.hmcsName;
        cData.xCoordinate = respData.xCoordinate;
        cData.yCoordinate = respData.yCoordinate;
        cData.ILS = respData.ILS;
        cData.vector = respData.vector;
        cData.addressId = respData.addressId;
        cData.addressTypeCode = respData.addressTypeCode;
        cData.additionalAddressInformation = respData.additionalAddressInformation;
        cData.renderedAddress = respData.renderedAddress;
        cData.addressAssignmentId = respData.addressAssignmentId;
        cData.addressAssignmentSubTypeCode = respData.addressAssignmentSubTypeCode;
        cData.addressAssignmentTypeCode = respData.addressAssignmentTypeCode;
        cData.addressMatchingStatusCode = respData.addressMatchingStatusCode;
        cData.addressSearchText = respData.addressSearchText;
        cData.countryCode = respData.countryCode;
        cData.emailAddressText = respData.emailAddressText;
        cData.externalAddressId = respData.externalAddressId;
        cData.externalAddressSourceId = respData.externalAddressSourceId;
        cData.externalServiceAddressId = respData.externalServiceAddressId;
        cData.mailingTypeCode = respData.mailingTypeCode;
        cData.municipalityName = respData.municipalityName;
        cData.postOfficeBoxNumber = respData.postOfficeBoxNumber;
        cData.postalZipCode = respData.postalZipCode;
        cData.provinceStateCode = respData.provinceStateCode;
        cData.relateAddressAssignmentId = respData.relateAddressAssignmentId;
        cData.streetDirectionCode = respData.streetDirectionCode;
        cData.streetName_fms = respData.streetName;
        cData.streetNumber_fms = respData.streetNumber;
        cData.streetTypeCode = respData.streetTypeCode;
        cData.unitNumber = respData.unitNumber;
        cData.validateAddressIndicator = respData.validateAddressIndicator;
        cData.unitTypeCode = respData.unitTypeCode;
        cData.municipalityClli = respData.municipalityClli;
        cData.prewireDate = respData.prewireDate;
        cData.enterPhone = respData.enterPhone;
        cData.jurisdiction = respData.jurisdiction;
        cData.baseRateAreaCode = respData.baseRateAreaCode;
        cData.baseRateAreaMileage = respData.baseRateAreaMileage;
        cData.transmissionZone = respData.transmissionZone;
        cData.nnxCodeSpecial = respData.nnxCodeSpecial;
        cData.rateCenter = respData.rateCenter;
        cData.commentLine1 = respData.commentLine1;
        cData.commentLine2 = respData.commentLine2;
        cData.switchType = respData.switchType;
        cData.switchId = respData.switchId;
        cData.terminalCode = respData.terminalCode;
        cData.serviceCount = respData.serviceCount;
        cData.serviceAddressIndividualLineServiceCode = respData.serviceAddressIndividualLineServiceCode;
        cData.portabilityCode = respData.portabilityCode;
        cData.rateCentreRemarks = respData.rateCentreRemarks;
        cData.blockAddressInd = respData.blockAddressInd;
        cData.legalLandDescription = respData.legalLandDescription;
        cData.streetNumberSuffix = respData.streetNumberSuffix;
        
        //Added by Arvind as part of ServiceAddressManagement Service V1.1 (US-OCOM-1101)
        
        cData.dropFacilityCode = respData.dropFacilityCode;
        cData.dropTypeCode = respData.dropTypeCode;
        cData.dropLength = respData.dropLength;
        cData.dropExceptionCode = respData.dropExceptionCode;
        cData.currentTransportTypeCode = respData.currentTransportTypeCode;
        cData.futureTransportTypeCode = respData.futureTransportTypeCode;
        cData.futurePlantReadyDate = respData.futurePlantReadyDate;
        cData.futureTransportRemarkTypeCode = respData.futureTransportRemarkTypeCode;
        system.debug('cData cData '+cData);
        // Addition End
        
         //Added by Arvind to populate gponBuildTypeCode/provisioningSystemCode.
         
         cData.gponBuildTypeCode = respData.gponBuildTypeCode;
         cData.provisioningSystemCode = respData.provisioningSystemCode;
         cData.futureASfCall = True;
         
         //
        
      system.debug('@@FMSResult'+cData.clliCode+' @ 1'+cData.coid+'@2'+cData.localRoutingNumber+'@3'+cData.ratingNpaNxx+'@4'+cData.switchNumber
       +'@5'+cData.terminalNumber);
        
     }
    
    
    
    /* 
    * getPostalKey - This method is invoked remotely on load event of component. 
    *            Used get API key for Canada Post
    * Parameter - 
    * Return Type - String
    */
    @RemoteAction
    global static String getPostalKey(){
        PACConfig__c configdtls = PACConfig__c.getInstance(keyConfig);
        if(configdtls != null && configdtls.API_Key__c != null){
            return configdtls.API_Key__c;
        }else{
            apiKeyErrorMessage = System.Label.PACAPIKeyErrorMessage;
            return null;
        }
    } 
    
    //BSBD_RTA-479 - Added by Sandip to make autonomy call for partner user (outside TELUS VPN)
    @RemoteAction
    global static String invokePAC(String queryString){
       PACConfig__c configdtls = PACConfig__c.getInstance('service'); // Fetch configuration for service autonomy
        String telusCertificate = configdtls.Telus_Certificate__c;

        System.debug ('@@@telusCertificate ' + telusCertificate);
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        
        //String queryString = 'https://webservice1.preprd.teluslabs.net/rest/v1/AutoAddressCompletion/searchAddress/?a=query&databasematch=dt1&spellcheck=true&totalresults=true&sort=TELUS.LPDS.STREET_NAME%3Aalphabetical%20TELUS.LPDS.HOUSE_NUMBER%3Aalphabetical%20TELUS.LPDS.FMS.SUITESORTING%3Aalphabetical%20TELUS.LPDS.FMS.APARTMENT%3Aalphabetical&Synonym=true&text=1616%201%20ST%20NE%20CALGARY%20AB&maxResults=20.0';
        req.setEndpoint(queryString);
        req.setHeader('Content-Type', 'text/xml');
        req.setClientCertificateName(telusCertificate);  
        req.setMethod('GET');
        
        HttpResponse res = new HttpResponse();
        res = h.send(req);
        String responseStr = res.getBody();       
        System.debug('HttpResponse  Response ---> '+ responseStr);
        return responseStr;
    } 

}