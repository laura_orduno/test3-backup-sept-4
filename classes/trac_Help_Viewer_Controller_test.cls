@isTest
private class trac_Help_Viewer_Controller_test {
    static testMethod void HVtest() {
        trac_Help_Viewer_Controller thv = new trac_Help_Viewer_Controller();
        System.assertEquals(thv.getDisplayHomepage(), true);
        System.assertEquals(thv.getDisplaySearchResults(), false);
        System.assertEquals(thv.getDisplaySingleRecord(), false);
        System.assertEquals(thv.getWasSourceId(), false);
    
        Account myAcc = new Account();
        myAcc.Name = 'Test';
        myAcc.RCID__c = 'TEST';
        insert myAcc;
    
        Opportunity o = new Opportunity(Name = 'Test', Account = myAcc, StageName='Open', CloseDate = Date.today());
        Insert o;
        
        
        Help_Asset__c ha = new Help_Asset__c(Object__c = 'Opportunity', Details__c = 'Details', Summary__c = 'Summary', Title__c = 'Title', PDF_Id__c = 'PDF ID');
        insert ha;
        
        if (ha.Id != null) {
            ApexPages.currentPage().getParameters().put('id', o.Id);
            trac_Help_Viewer_Controller thv1 = new trac_Help_Viewer_Controller();
            thv1.getDisplaySingleRecord();
            thv1.getDisplaySearchReturnedNothing();
            thv1.getDisplaySearchResults();
            thv1.getMyRequests();
            thv1.getFeaturedVideos();
            thv1.getSidebarHTML();
            thv1.getPrefixForSMETRequest();
            System.assertEquals(thv1.getWasSourceId(), true);
            List<SelectOption> rts = thv1.getSMETRts();
            if (rts.size() > 0) {
                thv1.smetCategory = rts[0].getValue();
                thv1.smetDescription = 'Test Description';
                thv1.smetSubject = 'Test Subject';
                thv1.createRequest();
            }
            System.assertEquals(thv1.getDisplayHomePage(), false);
        }
        ApexPages.currentPage().getParameters().put('q', 'Stage');
        trac_Help_Viewer_Controller thv2 = new trac_Help_Viewer_Controller();
        ApexPages.currentPage().getParameters().put('haid', ha.Id);
        trac_Help_Viewer_Controller thv3 = new trac_Help_Viewer_Controller();
    
    }
}