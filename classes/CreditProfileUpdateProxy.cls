global class CreditProfileUpdateProxy {
    
    @invocablemethod
    global static void invocablemethod(List<ID> profileIds){ 
        System.debug('... dyy ... profileIds' + profileIds);
        if(profileIds!=null &&profileIds.size()!=0)
            for (ID profileId:profileIds){    
                CreditProfileCalloutUpdate.updateCreditProfile(profileId);
            }
    }
    
}