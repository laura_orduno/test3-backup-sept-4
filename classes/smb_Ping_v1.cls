//Generated by wsdl2apex

public class smb_Ping_v1 {
    public class pingResponse_element {
        public String version;
        private String[] version_type_info = new String[]{'pingResponse','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/ping_v1','true','false'};
        private String[] field_order_type_info = new String[]{'version'};
    }
    public class ping_element {
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/ping_v1','false','false'};
        private String[] field_order_type_info = new String[]{};
    }
}