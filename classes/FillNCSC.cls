/*
    To start this batch:
    Id batchInstanceId = Database.executeBatch(new FillNCSC(), 1000);
*/
global class FillNCSC implements Database.Batchable<sObject>, Database.AllowsCallouts {
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'Select Id From AtoZ_Assignment__c Where AtoZ_Field__c = \'SvcSpec_NCSC\'';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext bc, List<sObject> scope) {
        //for (sObject s : scope) {
        //}
        update scope;
    }
    global void finish(Database.BatchableContext bc) {
        // cleanup, email alert, stats, etc
    }

}