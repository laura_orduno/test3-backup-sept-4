@isTest
private class TestMigrateDetailsController {

    static testMethod void myUnitTest() {
    	Test.startTest();
    	
    	Account acc = new Account();
    	acc.Name = 'TestAcc';
    	insert acc;
    	
        Contract contract = new Contract(); 
        contract.AccountId = acc.id;
        contract.Status = 'Draft';
        insert contract;
        
		Offer_House_Demand__c oldDealSupport = new Offer_House_Demand__c();
        map<string,schema.recordtypeinfo> recordTypeMap=offer_house_demand__c.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        oldDealSupport.recordtypeid=recordTypeMap.get('WLN Deal Support').getrecordtypeid();
        oldDealSupport.type_of_contract__c='CCA';
        oldDealSupport.signature_required__c='Yes';
        oldDealSupport.Contract__c = contract.id;
        insert oldDealSupport;        
        
		createRatePlanObjects(oldDealSupport, false);
		
        String expr = MigrateDetailsController.getAllFields('Rate_Plan_Item__c').get('Rate_Plan_Item__c');
        String queryOld = 'SELECT id, Name_TPL__c, Parent_Rate_Plan_Item__r.Name_TPL__c, ' + expr + ' FROM Rate_Plan_Item__c WHERE Deal_Support__c = ' + '\'' + oldDealSupport.id + '\'';        
        List<Rate_Plan_Item__c> queryOldRatePlanItems = Database.query(queryOld);
        List<Rate_Plan_Item__c> forInsertList = new List<Rate_Plan_Item__c>();   
             
        for(Rate_Plan_Item__c oldItem : queryOldRatePlanItems) {
			Rate_Plan_Item__c rpi = oldItem;
			rpi.Parent_Rate_Plan_Item__c = rpi.id;
			rpi.id = null;
			rpi.Deal_Support__c = null;
			forInsertList.add(rpi);        			
        }
        insert forInsertList;    
            
		Offer_House_Demand__c newDealSupport = new Offer_House_Demand__c();
        newDealSupport.recordtypeid=recordTypeMap.get('WLN Deal Support').getrecordtypeid();
        newDealSupport.type_of_contract__c='CCA';
        newDealSupport.signature_required__c='Yes';
        newDealSupport.Existing_Contract__c = contract.id;
        insert newDealSupport;   
                  
        createRatePlanObjects(newDealSupport, true);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(newDealSupport);
        MigrateDetailsController migrateDetails = new MigrateDetailsController(sc);
        MigrateDetailsController.goMigrateDetails(oldDealSupport.id, newDealSupport.id);
        
		queryOld = 'SELECT id, Name_TPL__c, Parent_Rate_Plan_Item__r.Name_TPL__c, ' + expr + ', (SELECT Name_TPL__c, ' + expr + ' FROM Rate_Plans_Items__r)' + ' FROM Rate_Plan_Item__c WHERE Deal_Support__c = ' + '\'' + oldDealSupport.id + '\'';
        queryOldRatePlanItems = Database.query(queryOld);
        String queryNew = 'SELECT id, Name_TPL__c, Parent_Rate_Plan_Item__r.Name_TPL__c, ' + expr + ', (SELECT Name_TPL__c, ' + expr + ' FROM Rate_Plans_Items__r)' + ' FROM Rate_Plan_Item__c WHERE Deal_Support__c = ' + '\'' + newDealSupport.id + '\'';
        List<Rate_Plan_Item__c> queryNewRatePlanItems = Database.query(queryNew);
        
        Map<String,Rate_Plan_Item__c> mapChildRPold = new Map<String,Rate_Plan_Item__c>();
        for(Rate_Plan_Item__c oldItem : queryOldRatePlanItems) {
        	for(Rate_Plan_Item__c subOldItem : oldItem.Rate_Plans_Items__r) {
        		mapChildRPold.put(subOldItem.Name_TPL__c, subOldItem);
        	}
        }
        
        Map<String,Rate_Plan_Item__c> mapChildRPnew = new Map<String,Rate_Plan_Item__c>();
        for(Rate_Plan_Item__c newItem : queryNewRatePlanItems) {        	
        	for(Rate_Plan_Item__c subNewItem : newItem.Rate_Plans_Items__r) {
        		mapChildRPnew.put(subNewItem.Name_TPL__c, subNewItem);
        	}
        }   
             
        for(String key : mapChildRPold.keySet()) {
        	System.assert(mapChildRPnew.keySet().contains(key));
        }        
        Test.stopTest();

    }
    private static void createRatePlanObjects(Offer_House_Demand__c testObj, Boolean newDs) {
        List<Rate_Plan_Item__c> ratePlanItems = new LIst<Rate_Plan_Item__c>();
         List<Product2> prds = createProducts(newDs);
        for(Product2 p: prds){
            ratePlanItems.add(new Rate_Plan_Item__c(Product__c=p.id, Deal_Support__c=testObj.id));
        }
        insert ratePlanItems;
    }
    
    private static List<Product2> createProducts(Boolean newDs) {
        String[] productPlanTypes = new String[]{'Data Plan','Voice Plan','Data Plan'};
        List<Product2> toInsert = new List<Product2>();
	    Integer i=10;
	    for(String typ : productPlanTypes){
	    	if(newDs == true) {
			    toInsert.add(new Product2(Name='RP'+typ+i,RP_Plan_Type__c =typ,RP_SOC_Code__c='XCVVA'+typ+i));
			    i++;
	    	} else {
			    toInsert.add(new Product2(Name='RP'+typ+i,RP_Plan_Type__c =typ,RP_SOC_Code__c='XVVVA'+typ+i));
			    i++;	    		
	    	}
	    }
        insert toInsert;
        return toInsert;
    }       
}