@isTest
public class Naas_ServiceChargesTable_Test {
	public TestMethod static void testInvokeMethod() {
    	String methodName = 'buildDocumentSectionContent';
        Map<String,Object> inputMap = new Map<String,Object>();
        Map<String,Object> outMap =  new Map<String,Object>();
        Map<String,Object> options = new Map<String,Object>();
        Naas_ServiceChargesTable obj = new Naas_ServiceChargesTable();
        obj.invokeMethod(methodName,inputMap,outMap,options);
        Naas_ServicesSummaryTable obj1 = new Naas_ServicesSummaryTable();
        obj1.invokeMethod(methodName,inputMap,outMap,options);
    }
}