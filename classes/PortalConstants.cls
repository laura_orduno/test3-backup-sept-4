public with sharing class PortalConstants {

	public static final String PROFILE_CUSTOMER_COMMUNITY = 'Customer Community User Custom';
	public static final String PROFILE_SYSTEM_ADMINISTRATOR = 'System Administrator';

	public static final String VITILTEMPLATEID = [SELECT Metadata_Value__c from Apex_Org_Default__mdt where DeveloperName = 'VITILCareCaseCommentEmailTemplateId' LIMIT 1].Metadata_Value__c;
	public static final String VITILNEWTEMPLATEID = [SELECT Metadata_Value__c from Apex_Org_Default__mdt where DeveloperName = 'VITILcareNewEmailTemplateId' LIMIT 1].Metadata_Value__c;
	public static final String VITILCANCELTEMPLATEID = [SELECT Metadata_Value__c from Apex_Org_Default__mdt where DeveloperName = 'VITILcareCancelEmailTemplateId' LIMIT 1].Metadata_Value__c;
	public static final String VITILFROMEMAILID = [select Metadata_Value__c from Apex_Org_Default__mdt where DeveloperName = 'VITILCareOrgWideEmailId' LIMIT 1].Metadata_Value__c;

	public static final String ENTPUPDATETEMPLATEID = [SELECT Metadata_Value__c from Apex_Org_Default__mdt where DeveloperName = 'ENTPCaseCommentEmailTemplateId' LIMIT 1].Metadata_Value__c;
	public static final String ENTPNEWTEMPLATEID = [SELECT Metadata_Value__c from Apex_Org_Default__mdt where DeveloperName = 'ENTPNewEmailTemplateId' LIMIT 1].Metadata_Value__c;
	public static final String ENTPCANCELTEMPLATEID = [SELECT Metadata_Value__c from Apex_Org_Default__mdt where DeveloperName = 'ENTPCanceledEmailTemplateId' LIMIT 1].Metadata_Value__c;
	public static final String ENTPFROMEMAILID = [select Metadata_Value__c from Apex_Org_Default__mdt where DeveloperName = 'ENTPOrgWideEmailId' LIMIT 1].Metadata_Value__c;

	public static final String MBRNEWTEMPLATEID = [SELECT Metadata_Value__c from Apex_Org_Default__mdt where DeveloperName = 'MBRCaseCreateEmailTemplateId' LIMIT 1].Metadata_Value__c;
	public static final String MBRUPDATETEMPLATEID = [SELECT Metadata_Value__c from Apex_Org_Default__mdt where DeveloperName = 'MBRCaseCommentEmailTemplateId' LIMIT 1].Metadata_Value__c;
	public static final String MBRCANCELTEMPLATEID = [SELECT Metadata_Value__c from Apex_Org_Default__mdt where DeveloperName = 'MBRClosedEmailTemplate' LIMIT 1].Metadata_Value__c;
	public static final String MBRFROMEMAILID = [select Metadata_Value__c from Apex_Org_Default__mdt where DeveloperName = 'MBROrgWideEmailAddressId' LIMIT 1].Metadata_Value__c;

	public static final string MYACCOUNTFROMEMAILID = [select Metadata_Value__c from Apex_Org_Default__mdt where DeveloperName = 'MyAccountOrgWideEmailId' LIMIT 1].Metadata_Value__c;
	public static final String MYACCOUNTUPDATETEMPLATEID = [SELECT Metadata_Value__c from Apex_Org_Default__mdt where DeveloperName = 'MyAccountCaseCommentEmailTemplateId' LIMIT 1].Metadata_Value__c;
	public static final String MYACCOUNTCANCELTEMPLATEID = [select Metadata_Value__c from Apex_Org_Default__mdt where DeveloperName = 'MyAccountCancelEmailTemplateId' LIMIT 1].Metadata_Value__c;
	public static final String MYACCOUNTNEWTEMPLATEID = [select Metadata_Value__c from Apex_Org_Default__mdt where DeveloperName = 'MyAccountCaseCreateEmailTemplateId' LIMIT 1].Metadata_Value__c;

	public static final String MYACCOUNT_ORIGIN = 'My Account';
	public static final string VITIL_ORIGIN = 'VITILcare';
	public static final String ENTP_ORIGIN = 'ENTP';
	public static final String MBR_ORIGIN = 'My Business Requests';
	public static final String MITS_ORIGIN = 'MITS';
	public static final String PC_ORIGIN = 'Private Cloud';

	public static final String CASESTATUS_CLOSED = 'Closed';

	public static final String ACCOUNT_RT_DEV_NAME_NEW_PROSPECT_ACCOUNT = 'New_Prospect_Account';
	public static final String ACCOUNT_RT_DEV_NAME_NEW_ACCOUNT = 'New_Account';

	public static final String ACCOUNT_TYPE_RCID = 'RCID';
	public static final String ACCOUNT_TYPE_CBUCID = 'CBUCID';
	public static final String ACCOUNT_TYPE_BAN = 'BAN';
	public static final String ACCOUNT_TYPE_CAN = 'CAN';

	public static final String MASR_ACCOUNT_TYPE = 'C';
	public static final String MASR_ACCOUNT_SUBTYPE = 'E';

	public static final String ACCOUNT_RECORDTYPE_RCID = Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByName().get('RCID').getRecordTypeId();
	public static final String ACCOUNT_RECORDTYPE_CBUCID = Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByName().get('CBUCID').getRecordTypeId();
	public static final String ACCOUNT_RECORDTYPE_BAN = Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByName().get('BAN').getRecordTypeId();
	public static final String ACCOUNT_RECORDTYPE_CAN = Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByName().get('CAN').getRecordTypeId();

	public static final String ACCOUNT_STATUS_OBSOLETE = 'Obsolete';
	public static final String ACCOUNT_STATUS_TERMINATED = 'Terminated';

	public static final String OPPORTUNITY_RT_SMB_CARE = 'SMB Care Opportunity';
	public static final String OPPORTUNITY_RT_SMB_CARE_AMEND = 'SMB Care Amend Order Opportunity';
	public static final String OPPORTUNITY_RT_SMB_CARE_LOCKED = 'SMB Care Locked Order Opportunity';
	public static final String OPPORTUNITY_RT_SRS_ORDER_REQUEST = 'SRS Order Request';

	public static final String OPPORTUNITY_STAGENAME_ORDER_REQUEST_COMPLETED = 'Order Request Completed';
	public static final String OPPORTUNITY_STAGENAME_ORDER_REQUEST_CANCELLED = 'Order Request Cancelled';
	public static final String OPPORTUNITY_STAGENAME_QUOTE_CANCELLED = 'Quote Cancelled';
	public static final String OPPORTUNITY_STAGENAME_QUOTE_EXPIRED = 'Quote Expired';
	public static final String OPPORTUNITY_STAGENAME_CONTRACT_ACCEPTANCE_EXPIRED = 'Contract Acceptance Expired';

	public static final String ASSET_TYPE_WTN = 'WTN';
	public static final String ASSET_TYPE_CIR = 'CIR';
	public static final String ASSET_TYPE_TEL = 'TEL';
	public static final String ASSET_TYPE_DOM = 'DOM';
}