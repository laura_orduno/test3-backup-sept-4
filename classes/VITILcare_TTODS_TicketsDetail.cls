global with sharing class VITILcare_TTODS_TicketsDetail {

    public Account acct {get; set;}
    public List<Case> matchedCases {get; set;}
    public String tID {get;set;}    
    public String RCID {get;set;}
        
    public String ticketComments{get;set;}//Added By CF
    public String ticketNumber{get;set;}//Added By Bharat
    public String ticketStatus{get;set;}//Added By Bharat
    public String ticketType{get;set;}//Added By Bharat
    public String ticketAssignedTo{get;set;}//Added By Bharat
    public String ticketDescription{get;set;}//Added By Bharat
    public String ticketPriority{get;set;}//Added By Bharat
    public DateTime ticketCreatedDate{get;set;}//Added By Bharat
    public DateTime ticketClosedDate{get;set;}//Added By Bharat
    public String ticketCustomerContact{get;set;}//Added By Bharat
    public String ticketCreatedBy{get;set;}//Added By Bharat
    //public List<VITILcare_TTODS_troubleticket.TroubleTicketActivity> activityList {get;set;} //Added By Bharat 
    public List<TroubleTicketActivityWrapper> activityList{get; set;}
    
    public String pageInstanceId  {
        get {
                if (pageInstanceId == null) {
//                        pageInstanceId = VITILcare_GuidUtil.NewGuid();
                        pageInstanceId = smb_GuidUtil.NewGuid();
                }
                return pageInstanceId;
        }
        private set;
    }           
    
    public String ticketID;
     
    global transient List<Id> AllAccountIdsInHierarchy {get;set;}
 

    public PageReference init() {
        
        id accountId = ApexPages.currentPage().getParameters().get('id');            
        ticketID = ApexPages.currentPage().getParameters().get('ticketID');
        
        system.debug('***** Id: ' + accountId);
        system.debug('***** ticketID: ' + ticketID);
        
        acct = getAccount(accountId);
        
//        allAccountIdsInHierarchy = VITILcare_AccountUtility.getAccountsInHierarchy(accountId, acct.ParentId);
        allAccountIdsInHierarchy = smb_AccountUtility.getAccountsInHierarchy(accountId, acct.ParentId);
        
        tID = [select Team_TELUS_ID__c from User where id = :Userinfo.getUserId()].Team_TELUS_ID__c;    
        
        matchedCases = [SELECT Id, CaseNumber, Status, Priority, Subject, CreatedDate, RecordType.Name 
                                        FROM Case 
                                        WHERE AccountId IN :allAccountIdsInHierarchy 
                                        AND IsClosed = False 
                                        ORDER BY CreatedDate DESC];
        try{
        getTroubleTicketDetail(tId,acct.RCID__c);
        }catch(CalloutException e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Web Service Exception occured, Please try again'));         
        }
        return null;        
    }

    private static Account getAccount(Id accountId) {
    if (accountId == null) return null;
    
    return [SELECT  Id, ParentId, Name, RCID__c
            FROM Account 
            WHERE Id =: accountId];
    }

    
    /* Method introduced by Bharat */
    public void getTroubleTicketDetail(string tID, string RCID){                
        
        VITILcare_TTODS_CalloutHelper smbCallOut = new VITILcare_TTODS_CalloutHelper();
        list<smb_TTODS_troubleticket.TroubleTicket> lstTT = new list<smb_TTODS_troubleticket.TroubleTicket>();  //TID: T1234567  RCID: 0003206700  
        lstTT  = smbCallOut.getTicketsByQueryCall(TID ,RCID );
                
        activityList = new List<TroubleTicketActivityWrapper>();
        
//      for(VITILcare_TTODS_troubleticket.TroubleTicket ttods:lstTT){           
        for(smb_TTODS_troubleticket.TroubleTicket ttods:lstTT){         
            if(ttods.TELUSTicketId == ticketID){
                ticketComments = ttods.comments;
                ticketNumber = ttods.TELUSTicketId; 
                ticketStatus = ttods.statusCode;
                ticketType = ttods.ticketTypeCode;
                ticketAssignedTo = ttods.assignToWorkgroupName;
                ticketDescription = ttods.description;
                ticketPriority = ttods.priorityCode;
                ticketCreatedDate = ttods.createDate;
                ticketClosedDate = ttods.closeDate;             
                if(ttods.contact != null && ttods.contact.get(0) != null && ttods.contact.get(0).telecommunicationsaddress != null && ttods.contact.get(0).telecommunicationsaddress.get(0) != null)
                {
                    ticketCustomerContact = ttods.contact.get(0).fullName+' / '+formatphonenum(ttods.contact.get(0).telecommunicationsaddress.get(0).telephoneNumber);
                }
                ticketCreatedBy = ttods.createdByName;              
                if(ttods.troubleTicketActivity != null){
//               for(VITILcare_TTODS_troubleticket.TroubleTicketActivity ttact: ttods.troubleTicketActivity){                                       
                 for(smb_TTODS_troubleticket.TroubleTicketActivity ttact: ttods.troubleTicketActivity){                                     
                    activityList.add(new TroubleTicketActivityWrapper(ttact));
                 }  
                }
                                                     
            }                                                                           
        }                       
    }
    
    private static string abbreviate(string text, integer length) {
        if (string.isBlank(text)) return '';
        
        return text.abbreviate(length);
    }   
    public String formatphonenum (String s) {
     s = '(' + s.substring(0, 3) + ') ' + s.substring(3, 6) + '-' + s.substring(6);
     return s;
    }
    public String baseUrl {
        get {
            // Changes as per in Mydomain imlplementation
            //return 'https://' + URL.getSalesforceBaseUrl().getHost().substring(2,6) + '.salesforce.com';
            String inst = URLMethodUtility.getInstanceName();
            return 'https://' + inst + '.salesforce.com';
        }
    }
    public class TroubleTicketActivityWrapper {
        public String activityTypeCode{get;set;}        
        public String TELUSActivityId {get;set;}        
        public String assignToWorkgroup {get;set;}
        public String activityComments {get;set;}
        public String resolutionCode1 {get;set;}
        public String resolutionCode2 {get;set;}
        public String resolutionCode3 {get;set;}        
        public DateTime actualCompleteDateTime {get;set;}        
        public String completedFlag {get;set;}        
        public DateTime actvitiyLastUpdateTimeTTODS {get;set;}
//        public TroubleTicketActivityWrapper(VITILcare_TTODS_troubleticket.TroubleTicketActivity activity){
        public TroubleTicketActivityWrapper(smb_TTODS_troubleticket.TroubleTicketActivity activity){
            this.activityTypeCode = activity.activityTypeCode;        
            this.TELUSActivityId = activity.TELUSActivityId;        
            this.assignToWorkgroup = activity.assignToWorkgroup;
            this.activityComments = activity.activityComments;
            this.resolutionCode1 = activity.resolutionCode1;
            this.resolutionCode2 = activity.resolutionCode2;
            this.resolutionCode3 = activity.resolutionCode3;        
            this.actualCompleteDateTime = activity.actualCompleteDateTime;      
            this.completedFlag = activity.completedFlag;        
            this.actvitiyLastUpdateTimeTTODS = activity.actvitiyLastUpdateTimeTTODS;    
        }
                
    }    
}