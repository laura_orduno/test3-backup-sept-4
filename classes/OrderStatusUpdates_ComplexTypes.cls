/*
###########################################################################
# File..................: OrderStatusUpdates_ComplexTypes
# Version...............: 1
# Created by............: Aditya Jamwal (IBM)
# Created Date..........: 15-Sept-2014 
# Last Modified by......: Rahul Badal (IBM)
# Last Modified Date....: 22-July-2015
# Modification:           Added additional fields @59-68
# Description...........: Class is declare datatype for CRDB- SF integration,CRDB will push information in below declared datatype format.
############################################################################
*/

global class OrderStatusUpdates_ComplexTypes {
    
    global class Response {
        webservice LineItem     Lt; 
        webservice FaultResponse FaultCode;
        webservice SuccessResponse SuccessCode;
    }
    
    global class FaultResponse {
        webservice String Message;
    }
    
    global class SuccessResponse {
        webservice String Message;
    }
    
    global class OrderStatusUpdate {
        
        webservice List<LineItem> LineItemList;
        webservice String OrderId;
        
    }
    
    /*global class LineItems {
        webservice List<LineItem> LineItemList;
    }*/
    
    global class LineItem {
        // our variables here
            webservice string LegacyOrderID;
            webservice String Sequence;
            webservice String IssueDate;
            webservice String BillingSystem;
            webservice string FoxOrderID;
            webservice string FoxJEOPCode;
            webservice string CRDBJEOPCode;
            webservice string CRIS3HoldCode;
            webservice string CRIS3OrderStatus;
            webservice DesignAssessmentCompleteDate DACDate;
            webservice DueDate DDate;
            webservice EngineeringReadyDate ERDate;
            webservice PlantReadyDate PRDate;
            webservice PlantTestDate PTDate;
            webservice ReadyToDate RTDate; 
            // Additional Fields added per CR 
            webservice string BillingAddress;
            webservice string BTN;
            webservice string CustomerName;
            webservice string OriginatorID;
            webservice string RCID;
            webservice string RCIDName;
            webservice string ServiceAddress;
            webservice string ServiceCity;
            webservice string ServiceProvince;
            webservice string WorkInstallType;
    }
    
     global Class DesignAssessmentCompleteDate
    {
      webservice Date DACScheduleDate;
      webservice Date DACActualDate;
    }

    global Class DueDate
        {
          webservice Date DDScheduleDate;
          webservice Date DDActualDate;
        }
        
    global Class EngineeringReadyDate
        {
          webservice Date ERDScheduleDate;
          webservice Date ERDActualDate;
        }
    global Class PlantReadyDate
        {
          webservice Date PRDScheduleDate;
          webservice Date PRDActualDate;
        }
    
    global Class PlantTestDate
        {
          webservice Date PTDScheduleDate;
          webservice Date PTDActualDate;
        }   
        
    global Class ReadyToDate
        {
          webservice Date RTIScheduleDate;
          webservice Date RTIActualDate;
        }
    
}