/**
 * To initiate the SR bulk load CSV Parsing, validation, SR conversion, and SR Submissions.
 */
global class BulkLoadMaster{
    /**
     * Validate Bulk Order Load file before mapping each record to the Bulk Order Load CSV.
     * If particular row of record has parse error, there will still be a Bulk Order Load Record
     * to be created to reference that and shows user the parse error.
     * 
     * Note that this only locks the current Bulk Order Load recoerd by changing the status to
     * In Progress and submits an async job to avoid long UI wait time.  Async Job will then
     * process CSV and update each Bulk Order Load Record data and status.
     */
    webservice static void validateBulkOrderLoad(id bulkOrderLoadId){
        bulk_order_load_file__c bulkOrderLoad=[select id,submission_status__c,total_records_in_template__c,validation_start_time__c from bulk_order_load_file__c where id=:bulkOrderLoadId and submission_status__c not in ('Validation In Progress','Complete') for update];
        bulkOrderLoad.submission_status__c='Validation_In_Progress';
        attachment bulkOrderLoadFileAttachment=[select id,body from attachment where parentid=:bulkOrderLoadId order by createddate desc limit 1];

        
        
System.debug('bulkOrderLoadFileAttachment.body: ' + bulkOrderLoadFileAttachment.body);
        list<list<string>> bulkOrderLoadRecords=BulkLoadUtils.parseCSV(bulkOrderLoadFileAttachment.body.tostring(),true);
        //TODO Update Bulk Order Load File # of records update.
        bulkOrderLoad.total_records_in_template__c=bulkOrderLoadRecords.size();
        bulkOrderLoad.validation_start_time__c=datetime.now();
        update bulkOrderLoad;
        //TODO Submit an async job to parse CSV and create bulk order load records
        system.debug('Enqueuing for first dispatch...with session id:'+userinfo.getsessionid());
        system.enqueuejob(new BulkOrderLoadRecordConverter(userinfo.getsessionid(),bulkOrderLoadId));
    }
    // Tested with 1200 records per job in first Bulk Order Load project in Jan 2017.  Will divert at least
    // 1200 records per job to process up to 50 x 1200 = 60k records
    webservice static void bulkOrderLoadRecordsDistributor(id bulkOrderLoadId,list<BulkOrderLoadRecordRow> bulkOrderLoadRecordRows){
        integer numBulkOrderLoadRecords=bulkOrderLoadRecordRows.size();
        integer numPerJob=numBulkOrderLoadRecords/limits.getLimitQueueableJobs();
        integer remainder=(numBulkOrderLoadRecords>limits.getLimitQueueableJobs()?math.mod(numBulkOrderLoadRecords,limits.getLimitQueueableJobs()):0);
        if(numPerJob<1){
            numPerJob=1;
        }
        if(remainder>0){
            numPerJob+=1;
        }
        system.debug('session id:'+userinfo.getsessionid()+',bulkOrderLoadId:'+bulkOrderLoadId+', numPerJob:'+numPerJob+', num records:'+numBulkOrderLoadRecords+', getLimitQueueableJobs:'+limits.getLimitQueueableJobs());
        list<BulkOrderLoadRecordRow> queueableRecords=new list<BulkOrderLoadRecordRow>();
        for(BulkOrderLoadRecordRow bulkOrderLoadRecordRow:bulkOrderLoadRecordRows){
            queueableRecords.add(bulkOrderLoadRecordRow);
            if(queueableRecords.size()>=numPerJob){
                system.debug('Session ID:'+userinfo.getsessionid()+', In Distributor Queueable Size:'+queueableRecords.size());
                system.enqueuejob(new BulkOrderLoadRecordConverter(userinfo.getsessionid(),bulkOrderLoadId,queueableRecords));
                queueableRecords.clear();
            }
        }
        if(!queueableRecords.isempty()){
            system.debug('Session ID:'+userinfo.getsessionid()+',Enquaued another job, queueable size:'+queueableRecords.size());
            system.enqueuejob(new BulkOrderLoadRecordConverter(userinfo.getsessionid(),bulkOrderLoadId,queueableRecords));
            queueableRecords.clear();
        }
    }
    // Commit all Bulk Order Load Records associated to this bulkOrderlOadId
    // into Service Requests
    webservice static void commitBulkOrderLoad(id bulkOrderLoadId){
        bulk_order_load_file__c bulkOrderLoad=[select id,submission_status__c,sr_creation_start_time__c from bulk_order_load_file__c where id=:bulkOrderLoadId and submission_status__c not in ('Validation In Progress') limit 50000];
        bulkOrderLoad.submission_status__c='SR Creation in Progress';
        bulkOrderLoad.sr_creation_start_time__c=datetime.now();
        update bulkOrderLoad;
        //TODO Submit an async job to parse CSV and create bulk order load records
        system.enqueuejob(new CommitBulkOrderLoad(userinfo.getsessionid(),bulkOrderLoadId));
    }

/***** Travis: APPEARS NOT TO BE USED    
    webservice static void distributeCommit(list<id> bulkOrderLoadRecordIds){
        integer numBulkOrderLoadRecords=bulkOrderLoadRecordIds.size();
        integer numPerJob=numBulkOrderLoadRecords/limits.getLimitQueueableJobs();
        integer remainder=(numBulkOrderLoadRecords>limits.getLimitQueueableJobs()?math.mod(numBulkOrderLoadRecords,limits.getLimitQueueableJobs()):0);
        if(numPerJob<1){
            numPerJob=1;
        }
        if(remainder>0){
            numPerJob+=1;
        }
        system.debug('session id:'+userinfo.getsessionid()+', numPerJob:'+numPerJob+', num records:'+numBulkOrderLoadRecords+', getLimitQueueableJobs:'+limits.getLimitQueueableJobs());
        list<id> queueableRecords=new list<id>();
        for(id bulkOrderLoadRecordId:bulkOrderLoadRecordIds){
            queueableRecords.add(bulkOrderLoadRecordId);
            if(queueableRecords.size()>=numPerJob){
                system.debug('Session ID:'+userinfo.getsessionid()+', In Distributor Queueable Size:'+queueableRecords.size());
                system.enqueuejob(new CommitBulkOrderLoad(userinfo.getsessionid(),queueableRecords));
                queueableRecords.clear();
            }
        }
        if(!queueableRecords.isempty()){
            system.debug('Session ID:'+userinfo.getsessionid()+',Enquaued another job, queueable size:'+queueableRecords.size());
            system.enqueuejob(new CommitBulkOrderLoad(userinfo.getsessionid(),queueableRecords));
            queueableRecords.clear();
        }
    }
*/
    
    // Submit all to fox
    webservice static void submitBulkOrderLoad(id opportunityId){
System.debug('BulkLoad: submitBulkOrderLoad(id opportunityId): ' + opportunityId);        
        list<id> serviceRequestIds=new list<id>();
        list<service_request__c> serviceRequests=database.query('select id,Fox_Submission_In_Progress__c,reason_to_send_code__c,recordtype.name from service_request__c where opportunity__c=:opportunityId and service_request_status__c=\'Ready to Submit\' and Fox_Submission_In_Progress__c=false limit 50000 for update');
System.debug('BulkLoad: submitBulkOrderLoad, SRs: ' + serviceRequests);        
        for(service_request__c serviceRequest:serviceRequests){
            serviceRequest.put('Fox_Submission_In_Progress__c',true);
            serviceRequest.reason_to_send_code__c='\n  Bulk Submission';
            serviceRequestIds.add(serviceRequest.id);
        }
        update serviceRequests;
        //TODO Submit an async job to parse CSV and create bulk order load records
        SubmitBulkOrderLoad sbol = new SubmitBulkOrderLoad(userinfo.getsessionid(), opportunityId, serviceRequestIds);
        if(Test.isRunningTest()){
            // test code coverage scenario
            sbol.submitPerJob = 1;
            if(serviceRequests.size() > 0){
            	sbol.testSR = serviceRequests[0];                
            }
System.debug('BulkLoad: submitBulkOrderLoad: Test.isRunningTest(), sbol.submitPerJob: ' + sbol.submitPerJob);        
        }
        system.enqueuejob(sbol);
    }

    webservice static void distributeSubmitToFox(list<id> serviceRequestIds){
System.debug('BulkLoad: distributeSubmitToFox');
        Id opportunityID = null;
        integer numRecords=serviceRequestIds.size();
        integer numPerJob=numRecords/limits.getLimitQueueableJobs();
        integer remainder=(numRecords>limits.getLimitQueueableJobs()?math.mod(numRecords,limits.getLimitQueueableJobs()):0);
        if(numPerJob<1){
            numPerJob=1;
        }
        if(remainder>0){
            numPerJob+=1;
        }
// OLD        system.debug('session id:'+userinfo.getsessionid()+', numPerJob:'+numPerJob+', num records:'+numRecords+', getLimitQueueableJobs:'+limits.getLimitQueueableJobs());
        system.debug('BulkLoad: ' + userinfo.getsessionid() + + ' opportunityId, ' + opportunityId + ', numPerJob:'+numPerJob+', num records:'+numRecords+', getLimitQueueableJobs:'+limits.getLimitQueueableJobs());
        list<id> queueableRecords=new list<id>();
        for(id serviceRequestId:serviceRequestIds){
            queueableRecords.add(serviceRequestId);
            if(queueableRecords.size()>=numPerJob){
                system.debug('Session ID:'+userinfo.getsessionid()+', In Distributor Queueable Size:'+queueableRecords.size());
// OLD                system.enqueuejob(new SubmitBulkOrderLoad(userinfo.getsessionid(),queueableRecords));
                system.enqueuejob(new SubmitBulkOrderLoad(userinfo.getsessionid(), opportunityID, queueableRecords));
                queueableRecords.clear();
            }
        }
        if(!queueableRecords.isempty()){
            system.debug('Session ID:'+userinfo.getsessionid()+',Enquaued another job, queueable size:'+queueableRecords.size());
// OLD            system.enqueuejob(new SubmitBulkOrderLoad(userinfo.getsessionid(),queueableRecords));
            system.enqueuejob(new SubmitBulkOrderLoad(userinfo.getsessionid(), opportunityID, queueableRecords));
            queueableRecords.clear();
        }
    }
    /**
     * Equally distribute to number of jobs that are allowed in this org based on current governor limits.
     */
    webservice static list<BulkOrderLoadRecordRow> testParameters(){
        list<BulkOrderLoadRecordRow> bulkOrderLoadRecordRows=new list<BulkOrderLoadRecordRow>();
        for(integer i=0;i<15;i++){
            BulkOrderLoadRecordRow bulkOrderLoadRecordRow=new BulkOrderLoadRecordRow();
            for(integer j=0;j<15;j++){
                bulkOrderLoadRecordRow.columns.add('column:'+j);
            }
            bulkOrderLoadRecordRow.rowNumber=i;
            bulkOrderLoadRecordRows.add(bulkOrderLoadRecordRow);
        }
        return bulkOrderLoadRecordRows;
    }
    global class BulkOrderLoadRecordRow {
        webservice list<string> columns{get;set;}
        webservice integer rowNumber{get;set;}
        global BulkOrderLoadRecordRow(){
            columns=new list<string>();
        }
        global BulkOrderLoadRecordRow(list<string> columns,integer rowNumber){
            this.columns=columns;
            this.rowNumber=rowNumber;
        }
    }
}