/**
 * Determine how many jobs are current running and submitted to estimate how busy the
 * queue is, then add nodes to branch out total jobs.  This class will add up to 100 nodes.
 * 
 * Not Busy
 * total SR = node x branch x job x submission
 * 8000 = 2 x 100 x 50 (max) x 1
 * 
 * 8000 = 1 x 80 x 50 x 2
 * 
 * 3777 = 1 x 38 x 50 x 2 = 3800
 * 
 * Total Branch = Total SR / (Total Job x Total Submission Per Job) + (Remainder of Total SR / (Total Job x Total Submission Per Job) > 0 ? 1 : 0);
 * 
 * Total Node = Total SR % (Total Branch x Total Job x Total Submission Per Job) < 1 ? 1 : (Total SR / Total Branch x Total Job x Total Submission Per Job) + 1;
 * 
 * Example: 5739
 * submission per job: 5
 * Calculate: Total Job x Total Submission per Job = 50 x 1 = 50
 * Total Branch: 5739 / 50 > 100 = 100
 * Total Node: 5739 / (100 x 50) > 0 = 1 + (Remainder of 5739 / (100 x 50) > 0 ? 1 : 0);
 * 
 * Exapmle: 13579 with max branch at 100
 * Total Node: 13579 / (100 x 50) > 0 = 2 + (Remainder of 13579 / (100 x 50) > 0 ? 1 : 0) = 3, 3 x 100 x 50 x 1;
 * 
 * Exapmle: 13579 with max branch at 80
 * Total Node: 13579 / (80 x 50) > 0 = 2 + (Remainder of 13579 / (80 x 50) > 0 ? 1 : 0) = 4, 4 x 80 x 50 x 1;
 * 
 * Exmaple: 13579 with max branch at 50 and max node at 100
 * Total Iteration: 13579 / (100 x 50)
 */
public class BulkLoadRoot implements queueable,database.allowscallouts{
    id orderId{get;set;}
    integer maxBranch{get;set;}
    integer maxJob{get;set;}
    integer submissionPerJob{get;set;}
    public BulkLoadRoot(id orderId){
        this.orderId=orderId;
        maxBranch=50;
        maxJob=50;
        submissionPerJob=1;
    }
    public void execute(queueablecontext context){
        //TODO Make query selective
        list<service_request__c> serviceRequests=[select id from service_request__c where opportunity__c=:orderId limit 50000];
        integer numSR=serviceRequests.size();
        if(numSR>0){
            // Assuming allowing callout is 100
            if(numSR>limits.getlimitcallouts()){
                // Call x100 addNode and evenly distribute total SR Ids
                integer limitCallouts=limits.getlimitcallouts();
                integer srPerCallout=numSR/limitCallouts;
                integer counter=0;
                list<id> serviceIds=new list<id>();
                for(service_request__c serviceRequest:serviceRequests){
                    serviceIds.add(serviceRequest.id);
                    if(serviceIds.size()>=srPerCallout){
                        // TODO add Node
                        serviceIds.clear();
                    }
                }
                if(!serviceIds.isempty()){
                    // TODO add Node
                    serviceIds.clear();
                }
            }
            /*
            integer maxJobSubmissions=submissionPerJob*maxJob;
            if(numSR<maxJobSubmissions){
                // Submit 1 node, 1 branch
            }else{
                integer estimatedBranch=numSR/maxJobSubmissions;
                integer estimatedExtraBranch=(math.mod(numSR,maxJobSubmissions)>0?1:0);
                integer maxBranchSubmission=maxBranch*maxJobSubmissions;
                integer totalIteration=(math.mod(numSR,maxBranchSubmission)>0?numSR/maxBranchSubmission+1:numSR/maxBranchSubmission);
                if(estimatedBranch>maxBranch){
                    integer estimatedNode=numSR/maxBranchSubmission;
                    integer estimatedExtraNode=(math.mod(numSR,maxBranchSubmission)>0?1:0);
                    if(estimatedNode<=1){
                        // Submit 1 node
                    }else{
                        // Submit estimatedNode + estimatedExtraNode
                    }
                }else{
                    // Submit 1 node, estimated branch + remainder
                }
            }*/
        }else{
            // Update Opportunity with appropriate status
        }
    }
}