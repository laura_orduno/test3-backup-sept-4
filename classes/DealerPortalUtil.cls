/* This class provides Integration APIs for Customer Search with account number or phone number.
   Developer: Aditya Jamwal (IBM)
   Created Date: 02-Nov-2017
*/
global with sharing class DealerPortalUtil implements vlocity_cmt.VlocityOpenInterface2{
    global static String RCID_RECORDTYPE_NAME = 'RCID';
    global static String CAN_RECORDTYPE_NAME = 'CAN';
    global static String CBUCID_RECORDTYPE_NAME = 'CBUCID';
    global static String BAN_RECORDTYPE_NAME = 'BAN';
    
    global Object invokeMethod(String methodName, Map<String, Object> input, Map<String, Object> output, Map<String, Object> options)  {
        system.debug('INput '+ input);
      try{
        if (methodName.indexOf('getAccountsandContactsWithPhoneNumber') != -1) {
            getAccountsandContactsWithPhoneNumber(input,output,options);
        }
        
        if (methodName.indexOf('getAccountsandContactsWithAccountNumber') != -1) {
            getAccountsandContactsWithAccountNumber(input,output,options);
        }
        }catch(exception ex){
            system.debug('!@Error '+ex.getStackTraceString());
            output.put('error',ex.getMessage()); 
        }
        
        return null;
    }    
    
    private void getAccountsandContactsWithAccountNumber(Map<String, Object> input, Map<String, Object> output, Map<String, Object> options){
        system.debug('@#Input '+ input);
        Map<Id,AccountWrapper> accountWrapperMap = new Map<Id,AccountWrapper>();
        Set<String> accountIdSet = new Set<String>();
        //Set<String> notFoundAccountIdSet = new Set<String>();
        Set<String> CAN_BAN_AccountIdSet = new Set<String>();
        String accountNumber = (String) input.get('accountNumber');
        String RCIDRecordTypeId =  getRecordTypeId(RCID_RECORDTYPE_NAME);
        String BANRecordTypeId =  getRecordTypeId(BAN_RECORDTYPE_NAME);
        String CANRecordTypeId =  getRecordTypeId(CAN_RECORDTYPE_NAME);
        if(String.isNotBlank(accountNumber)){
            accountNumber = removeSpecialCharacters(accountNumber);
            system.debug('@#Input '+ accountNumber);
            /*for(Account acc : [Select Id,CAN__c,recordTypeId,ParentId,Parent.recordtypeId                          
                               From Account where CAN__c != NULL limit 50000]){
               if(acc.CAN__c.startsWithIgnoreCase(accountNumber)){                   
                     system.debug('@#CAN '+ acc.CAN__c +' RCIDRecordTypeId '+RCIDRecordTypeId);
                    if(String.isNotBlank(RCIDRecordTypeId)){
                        if(String.isNotBlank(acc.recordtypeId) && RCIDRecordTypeId.equalsIgnoreCase(acc.recordtypeId)){
                             accountIdSet.add(acc.Id); 
                             continue;
                        }else {
                             notFoundAccountIdSet.add(acc.Id); 
                             continue;
                        }
                    }                    
                }                  
            }
			if(NULL != notFoundAccountIdSet && notFoundAccountIdSet.size() > 0){
               Map<String,String> childIdToRCIDMap = new Map<String,String>();
               Map<String,String> notFoundParentToChildIdMap = new Map<String,String>();
               getParentAccountIdOfSpecificRecordType(notFoundAccountIdSet, RCIDRecordTypeId , childIdToRCIDMap, notFoundParentToChildIdMap);
                if(NULL != childIdToRCIDMap && childIdToRCIDMap.size()>0 ){
                    accountIdSet.addAll(childIdToRCIDMap.values());
                } 
            }
			*/
            
            for(Account acc : [Select Id,CAN__c,recordTypeId,ParentId,Parent.recordtypeId                          
                               From Account where (RecordTypeId=: BANRecordTypeId OR RecordTypeId=: CANRecordTypeId) 
                               AND CAN__c != NULL
                               AND CAN__c=: accountNumber 
                               LIMIT 50000]){
                CAN_BAN_AccountIdSet.add(acc.Id);   
            }
            if(NULL != CAN_BAN_AccountIdSet && CAN_BAN_AccountIdSet.size() > 0){
               Map<String,String> childIdToRCIDMap = new Map<String,String>();
               Map<String,String> notFoundParentToChildIdMap = new Map<String,String>();
               getParentAccountIdOfSpecificRecordType(CAN_BAN_AccountIdSet, RCIDRecordTypeId , childIdToRCIDMap, notFoundParentToChildIdMap);
                if(NULL != childIdToRCIDMap && childIdToRCIDMap.size()>0 ){
                    accountIdSet.addAll(childIdToRCIDMap.values());
                } 
            }
            
            system.debug('!!@@accountIdSet '+ accountIdSet);
            
            List<Contact>contactList = [Select name,accountId,account.Name,HomePhone,MobilePhone,OtherPhone,Phone,AssistantPhone,Direct_Line__c from Contact where accountId = : accountIdSet limit 50000];
            
            if(null != contactList){
                system.debug('@#contactList size '+ contactList);
                for(Contact cont : contactList){
                    AccountWrapper accWrapper = new AccountWrapper();
                    if(null != accountWrapperMap && null != accountWrapperMap.get(cont.AccountId)){
                        accWrapper = accountWrapperMap.get(cont.AccountId);
                        accWrapper.contactList.add(cont);
                        accountWrapperMap.put(cont.AccountId,accWrapper);
                    }else if(null != accountWrapperMap && null == accountWrapperMap.get(cont.AccountId)){
                        accWrapper.account = new Account(id = cont.AccountId, Name = cont.account.Name);
                        accWrapper.contactList = new List<Contact>();
                        accWrapper.contactList.add(cont);
                        accountWrapperMap.put(cont.AccountId,accWrapper);
                    }
                }
            }           
            
        }else{ output.put('error','Please provide the account number to initate search.');}
        output.put('accountWrapperList',accountWrapperMap.values());
     }
     
    
     private void getAccountsandContactsWithPhoneNumber(Map<String, Object> input, Map<String, Object> output, Map<String, Object> options){
            system.debug('@#Input '+ input);
            Map<Id,AccountWrapper> accountWrapperMap = new Map<Id,AccountWrapper>();
            Set<String> accountIdSet = new Set<String>();         
       		//Set<String> notFoundAccountIdSet = new Set<String>();
       		Set<String> CAN_BAN_AccountIdSet = new Set<String>();
            String phoneNumber = (String) input.get('phoneNumber');
            String RCIDRecordTypeId =  getRecordTypeId(RCID_RECORDTYPE_NAME);
         	String BANRecordTypeId =  getRecordTypeId(BAN_RECORDTYPE_NAME);
        	String CANRecordTypeId =  getRecordTypeId(CAN_RECORDTYPE_NAME);
            if(String.isNotBlank(phoneNumber)){
                phoneNumber = removeSpecialCharacters(phoneNumber);
                system.debug('@#Input '+ phoneNumber);
                /*for(Account acc : [Select id,Name,BTN__c,parentId,parent.Name,recordtypeId from Account where BTN__c != NULL limit 50000]){
                    if(acc.BTN__c.startsWithIgnoreCase(phoneNumber)){                        
                     //   system.debug('@#parentId '+ acc.Name);
                        if(String.isNotBlank(RCIDRecordTypeId)){
                        if(String.isNotBlank(acc.recordtypeId) && RCIDRecordTypeId.equalsIgnoreCase(acc.recordtypeId)){
                             accountIdSet.add(acc.Id); 
                             continue;
                        }else {
                             notFoundAccountIdSet.add(acc.Id); 
                             continue;
                        }
                    } 
                  }                    
                }
                if(NULL != notFoundAccountIdSet && notFoundAccountIdSet.size() > 0){
                    Map<String,String> childIdToRCIDMap = new Map<String,String>();
                    Map<String,String> notFoundParentToChildIdMap = new Map<String,String>();
                    getParentAccountIdOfSpecificRecordType(notFoundAccountIdSet, RCIDRecordTypeId , childIdToRCIDMap, notFoundParentToChildIdMap);
                    if(NULL != childIdToRCIDMap && childIdToRCIDMap.size()>0 ){
                        accountIdSet.addAll(childIdToRCIDMap.values());
                    } 
                }
                */
                for(Account acc : [Select id,Name,BTN__c,parentId,parent.Name,recordtypeId FROM Account 
                                   WHERE (RecordTypeId=: BANRecordTypeId OR RecordTypeId=: CANRecordTypeId)
                                   AND BTN__c!= NULL
                                   AND BTN__c=: phoneNumber
                                   LIMIT 50000]){
                    CAN_BAN_AccountIdSet.add(acc.Id);                    
                }
                if(NULL != CAN_BAN_AccountIdSet && CAN_BAN_AccountIdSet.size() > 0){
                    Map<String,String> childIdToRCIDMap = new Map<String,String>();
                    Map<String,String> notFoundParentToChildIdMap = new Map<String,String>();
                    getParentAccountIdOfSpecificRecordType(CAN_BAN_AccountIdSet, RCIDRecordTypeId , childIdToRCIDMap, notFoundParentToChildIdMap);
                    if(NULL != childIdToRCIDMap && childIdToRCIDMap.size()>0 ){
                        accountIdSet.addAll(childIdToRCIDMap.values());
                    } 
                }
                String formattedPhoneNumber = formatContactNumber(phoneNumber);
                // added wild character for searching phone number - BSBD_RTA-1350
                String formattedPhTmp = formattedPhoneNumber +'%'; 
                String originalPhTmp = phoneNumber +'%'; 
                List<Contact>contactList = [SELECT name,accountId,account.Name,HomePhone,MobilePhone,
                                            OtherPhone,Phone,Fax,AssistantPhone,Direct_Line__c 
                                            FROM Contact 
                                            WHERE Phone Like : formattedPhoneNumber OR Phone Like : originalPhTmp
                                            OR HomePhone=: formattedPhoneNumber OR HomePhone=: phoneNumber
                                            OR MobilePhone=: formattedPhoneNumber OR MobilePhone=: phoneNumber
                                            OR OtherPhone=: formattedPhoneNumber OR OtherPhone=: phoneNumber
                                            OR Fax=: formattedPhoneNumber OR Fax=: phoneNumber
                                            OR AssistantPhone=: formattedPhoneNumber OR AssistantPhone=: phoneNumber
                                            OR Direct_Line__c=: formattedPhoneNumber OR Direct_Line__c=: phoneNumber
                                            limit 50000];
                if(null != contactList){
                	for(Contact cont : contactList){
                        if(cont.AccountId != null){
                            accountIdSet.add(cont.AccountId);
                        }
                    }
                }
                // get all contact for RCIDs
                List<Contact> allRCIDContactList = [Select name,accountId,account.Name,HomePhone,MobilePhone,OtherPhone,Phone,
                							Fax,AssistantPhone,Direct_Line__c 
                                            from Contact 
                                            Where accountId In: accountIdSet
                                            limit 50000];
                //where phone =: phoneNumber
                if(null != allRCIDContactList){
                    //system.debug('@#contactList size '+ contactList.size()+' '+phoneNumber);
                    for(Contact cont : allRCIDContactList){
                        AccountWrapper accWrapper = new AccountWrapper();
                        //system.debug('@#contactList size '+ String.isNotBlank(cont.Phone) );
                        /*String contNumber = removeSpecialCharacters(cont.Phone);
                        String HomePhone = removeSpecialCharacters(cont.HomePhone);
                        String MobilePhone =removeSpecialCharacters(cont.MobilePhone) ;
                        String OtherPhone = removeSpecialCharacters(cont.OtherPhone);
                        String Fax = removeSpecialCharacters(cont.Fax);
                        String AssistantPhone= removeSpecialCharacters(cont.AssistantPhone);
                        String DirectLine = removeSpecialCharacters(cont.Direct_Line__c);
                        if( accountIdSet.contains(cont.AccountId) || (String.isNotBlank(contNumber) && contNumber.startsWithIgnoreCase(phoneNumber) )
                                                                      || (String.isNotBlank(HomePhone) && contNumber.startsWithIgnoreCase(phoneNumber) )
                                                                      || (String.isNotBlank(MobilePhone) && contNumber.startsWithIgnoreCase(phoneNumber) )
                                                                      || (String.isNotBlank(OtherPhone) && contNumber.startsWithIgnoreCase(phoneNumber) )
                                                                      || (String.isNotBlank(Fax) && contNumber.startsWithIgnoreCase(phoneNumber) )
                                                                      || (String.isNotBlank(AssistantPhone) && contNumber.startsWithIgnoreCase(phoneNumber) )
                                                                      || (String.isNotBlank(DirectLine) && contNumber.startsWithIgnoreCase(phoneNumber) )                            
                            
                        ){*/
                         // system.debug(' !@ matches'+phoneNumber.equalsIgnoreCase(cont.Phone.replaceAll('[\\D]','')) );
                        if(null != accountWrapperMap && null != accountWrapperMap.get(cont.AccountId)){
                            accWrapper = accountWrapperMap.get(cont.AccountId);
                            accWrapper.contactList.add(cont);
                            accountWrapperMap.put(cont.AccountId,accWrapper);
                        }else if(null != accountWrapperMap && null == accountWrapperMap.get(cont.AccountId)){
                            accWrapper.account = new Account(id = cont.AccountId, Name = cont.account.Name);
                            accWrapper.contactList = new List<Contact>();
                            accWrapper.contactList.add(cont);
                            accountWrapperMap.put(cont.AccountId,accWrapper);
                        }
                        // use case : sibling contacts matches with inputted phone number.So, all siblings contacts will be added automatically.
                        /*}else  if(null != accountWrapperMap && null != accountWrapperMap.get(cont.AccountId)){
                                accWrapper = accountWrapperMap.get(cont.AccountId);
                                accWrapper.contactList.add(cont);
                                accountWrapperMap.put(cont.AccountId,accWrapper);
                        }*/
                    }                
                }
            }else{ output.put('error','Please provide the phone number to initate search.');}
            output.put('accountWrapperList',accountWrapperMap.values());
            
        }
    
     // This method is created to get RCID account for a child account (CAN/BAN) by looking uptill immediate RCID parent.
    @TestVisible
    private static Map<String,String> getParentAccountIdOfSpecificRecordType(Set<String> childAccountIds, String parentRecordId , Map<String,String> childIdToRCIDMap, Map<String,String> notFoundParentToChildIdMap){
        
        Set<String> stillNotFoundRCIDChildIds = new Set<String>();  
        
        if(null != childAccountIds && childAccountIds.size() > 0){
            
            for(Account acc : [Select id,name,parent.Name,ParentId,Parent.recordtypeId, // level 1 parent
                               		  Parent.ParentId,Parent.Parent.recordtypeId,  // level 2 parent
                               		  Parent.Parent.ParentId,Parent.Parent.Parent.recordtypeId, // level 3 parent
                               		  Parent.Parent.Parent.ParentId,Parent.Parent.Parent.Parent.recordtypeId, // level 4 parent
                                      Parent.Parent.Parent.Parent.ParentId,Parent.Parent.Parent.Parent.Parent.recordtypeId  // level 5 parent                                 
                               From Account where id = :childAccountIds]){
                                   
                if(String.isNotBlank(acc.parentid) && String.isNotBlank(acc.parent.recordtypeId)){
                    system.debug('account ' +acc.name);
                    String originalChildId;
                    if(null != notFoundParentToChildIdMap && null != notFoundParentToChildIdMap.get(acc.id)){
                        originalChildId = notFoundParentToChildIdMap.get(acc.id);
                         system.debug('orginal child ' +acc.name +' '+originalChildId);
                    }
                    if(parentRecordId.equalsIgnoreCase(acc.parent.recordtypeId)){  
                       
                        if(String.isNotBlank(originalChildId)){ childIdToRCIDMap.put(originalChildId,acc.parentId);} 
                        else{childIdToRCIDMap.put(acc.id,acc.parentId);}
                        system.debug('account 1' +acc.parentId);
                        
                        
                    }else if(String.isNotBlank(acc.Parent.Parent.recordtypeId) && parentRecordId.equalsIgnoreCase(acc.Parent.Parent.recordtypeId)){
                            
                        if(String.isNotBlank(originalChildId)){ childIdToRCIDMap.put(originalChildId,acc.Parent.Parentid);} 
                        else{childIdToRCIDMap.put(acc.id,acc.Parent.Parentid);}
                         system.debug('account 2 ' +acc.Parent.Parentid);
                        
                    }else if(String.isNotBlank(acc.Parent.Parent.Parent.recordtypeId) && parentRecordId.equalsIgnoreCase(acc.Parent.Parent.Parent.recordtypeId)){
                        
                         if(String.isNotBlank(originalChildId)){ childIdToRCIDMap.put(originalChildId,acc.Parent.Parent.Parentid);} 
                         else{childIdToRCIDMap.put(acc.id,acc.Parent.Parent.Parentid);}
                         system.debug('account 3 ' +acc.Parent.Parent.Parentid);
                        
                    }else if(String.isNotBlank(acc.Parent.Parent.Parent.Parent.recordtypeId) && parentRecordId.equalsIgnoreCase(acc.Parent.Parent.Parent.Parent.recordtypeId)){
                        
                         if(String.isNotBlank(originalChildId)){ childIdToRCIDMap.put(originalChildId,acc.Parent.Parent.Parent.Parentid);} 
                         else{childIdToRCIDMap.put(acc.id,acc.Parent.Parent.Parent.Parentid);}
                         system.debug('account4 ' +acc.Parent.Parent.Parent.Parentid +' \n '+ childIdToRCIDMap);
                        
                    }else if(String.isNotBlank(acc.Parent.Parent.Parent.Parent.Parent.recordtypeId) && parentRecordId.equalsIgnoreCase(acc.Parent.Parent.Parent.Parent.Parent.recordtypeId)){
                       
                        if(String.isNotBlank(originalChildId)){ childIdToRCIDMap.put(originalChildId,acc.Parent.Parent.Parent.Parent.Parentid);} 
                        else{childIdToRCIDMap.put(acc.id,acc.Parent.Parent.Parent.Parent.Parentid); }
                         system.debug('account 5 ' +acc.Parent.Parent.Parent.Parent.Parentid);
                        
                    }else if(String.isNotBlank(acc.Parent.Parent.Parent.Parent.ParentId)){
                        system.debug('Parent not found ' +acc.Parent.Parent.Parent.Parent.ParentId);
                       stillNotFoundRCIDChildIds.add(acc.Parent.Parent.Parent.Parent.ParentId);
                       notFoundParentToChildIdMap.put(acc.Parent.Parent.Parent.Parent.ParentId,acc.id);  
                       if(String.isNotBlank(originalChildId)){
                        notFoundParentToChildIdMap.put(acc.Parent.Parent.Parent.Parent.ParentId,originalChildId);      
                       }
                       
                    }else{
                        system.debug('Parent not found set to null ' +acc.name);
                       childIdToRCIDMap.put(acc.id,NULL); // No Parent account of a particular recordType is found.
                    }
               }
            }
            
            if(null != stillNotFoundRCIDChildIds && stillNotFoundRCIDChildIds.size() > 0){
                 system.debug('recursice call '+stillNotFoundRCIDChildIds);
              getParentAccountIdOfSpecificRecordType(stillNotFoundRCIDChildIds, parentRecordId, childIdToRCIDMap,notFoundParentToChildIdMap);  
            }else{
                 system.debug(' not recursice call '+childIdToRCIDMap);
               return childIdToRCIDMap;
            }            
        }        
        return null;
     }
    
     @TestVisible
     private String removeSpecialCharacters(String str){        
        Pattern nonAlphanumeric = Pattern.compile('[^a-zA-Z0-9]');
        if(String.isNotBlank(str)){
            Matcher matcher = nonAlphanumeric.matcher(str);
            str = matcher.replaceAll('');  
        }        
        return str;
     }
    
    @TestVisible
    private String formatContactNumber(String str){        
        String phoneDigits = '';
        if (str.length() == 10) {
    	  phoneDigits =	'(' + str.substring(0,3) + ') ' + str.substring(3,6) + '-' + str.substring(6,10);
        }else{
            phoneDigits = str; 
        }
        return phoneDigits;
     }
    
     private static String getRecordTypeId(String recordTypeName){
        
        String recordTypeId;
        
        if(String.isNotBlank(recordTypeName)){
            
            Schema.DescribeSObjectResult resSchema = Account.sObjectType.getDescribe();
            //getting all Recordtype  Account
            Map<String,Schema.RecordTypeInfo> recordTypeInfo = resSchema.getRecordTypeInfosByName(); 
            //Getting Business Record Type Id
            if(null != recordTypeInfo && null != recordTypeInfo.get(recordTypeName)){
                recordTypeId = recordTypeInfo.get(recordTypeName).getRecordTypeId();
            }            
        }
        return recordTypeId;
        
    }
    
    // Sandip - Method added as per defect BSBD_RTA-799
    public static list<Id> getDealerSalesRepIdsFromCache(){
        List<Id> salesRepIds = new List<Id>();
        try{
            Set<String> salesRepPin = new Set<String>();
            //load outlets and salesreps from  ChannelPortalCacheMgmtData 
            String federationId = [SELECT FederationIdentifier FROM User WHERE Id = :UserInfo.getUserId()].FederationIdentifier;    
            String cmsKey = 'local.orderingCache.KEY' + federationId;
            System.debug('getDealerSalesRepIdsFromCache cmskey=' + cmsKey);
            Object ob1 = Cache.Org.get(cmsKey);
            System.debug('DATA FROM LoginController '+JSON.serialize(ob1));
            ChannelPortalCacheMgmtData dataObj=(ChannelPortalCacheMgmtData)Cache.Org.get(cmsKey);
            String channelOrgCachedSalesreps = null;
            if(dataObj!=null){
                List<ChannelOrgSalesRepsJSONExplicit> channelOrgSalesRepJsonObjList=dataObj.channelOrgSalesRepJsonObjList;
                if(channelOrgSalesRepJsonObjList != null){
                    for(ChannelOrgSalesRepsJSONExplicit obj:channelOrgSalesRepJsonObjList){
                        if(obj.salesRepPin != null){
                        	salesRepPin.add(obj.salesRepPin);
                        }
                    }
                }
                System.debug('@@@ salesRepPin' + salesRepPin);
            }
            if(salesRepPin.size()>0){
                List<User> userList = [SELECT Id FROM USER WHERE Sales_Rep_ID__c <> null AND Sales_Rep_ID__c IN: salesRepPin];
                System.debug('@@@ userList' + userList);
                if(userList != null && userList.size()>0){
                    for(User u:userList){
                        salesRepIds.add(u.Id);
                    }
                }
            }
        }catch(Exception ex){}
        return salesRepIds;
    }
    
    // Sandip - Method added 16 Apr 2018
    public static Set<String> getDealerAdminPricipalRoles(){
        Set<String> dealeradminPricipalRoleSet = new Set<String>();
        List<DealerAdminPrincipalRoleNames__mdt> dealerAdminPrincipalRoleList = [Select MasterLabel, QualifiedApiName From DealerAdminPrincipalRoleNames__mdt];
        if(dealerAdminPrincipalRoleList != null && dealerAdminPrincipalRoleList.size()>0){
            for(DealerAdminPrincipalRoleNames__mdt dObj: dealerAdminPrincipalRoleList){
                dealeradminPricipalRoleSet.add(dObj.MasterLabel);
            }
        }
        return dealeradminPricipalRoleSet;
    }
        
        global Class AccountWrapper{
            global Account account;
            global List<Contact> contactList;
            
        }
        
    }