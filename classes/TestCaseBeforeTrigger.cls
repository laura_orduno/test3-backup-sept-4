@isTest
private class TestCaseBeforeTrigger {

    static testMethod void myUnitTest() {
       Case theCase = TestUtil.createCases(1, null, true)[0];

       List<QueueSobject> qs = [Select Queue.Type, Queue.Name, Queue.Id, QueueId 
       			  From QueueSobject
       			  Where Queue.Type='Queue'  and SobjectType = 'Case' Limit 1];
   	   theCase.OwnerId = qs[0].Queue.Id;
       
       Test.startTest();
	   	   update theCase;
       Test.stopTest();
       theCase = [Select Id, Queue_Name__c From Case where Id = :theCase.Id ];
       System.assertEquals(qs[0].Queue.Name, theCase.Queue_Name__c);
    }
}