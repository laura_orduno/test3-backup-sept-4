//Generated by wsdl2apex

public class AsyncRateGroupInfo_SrvCmoBillinga {
    public class getRateGroupAndForborneInformationByNpaNxxResponse_elementFuture extends System.WebServiceCalloutFuture {
        public RateGroupInfo_ResourceRes.RateGroupAndForborneAttribute getValue() {
            RateGroupInfo_SrvCmoBillinga.getRateGroupAndForborneInformationByNpaNxxResponse_element response = (RateGroupInfo_SrvCmoBillinga.getRateGroupAndForborneInformationByNpaNxxResponse_element)System.WebServiceCallout.endInvoke(this);
            return response.rateGroupAndForborneAttribute;
        }
    }
    public class getRateGroupInfoByNpaNxxResponse_elementFuture extends System.WebServiceCalloutFuture {
        public RateGroupInfo_ResourceRes.RateGroupAttribute getValue() {
            RateGroupInfo_SrvCmoBillinga.getRateGroupInfoByNpaNxxResponse_element response = (RateGroupInfo_SrvCmoBillinga.getRateGroupInfoByNpaNxxResponse_element)System.WebServiceCallout.endInvoke(this);
            return response.rateGroupAttribute;
        }
    }
}