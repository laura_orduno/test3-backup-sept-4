/**
    Unit Test for  OCOM_QuotePdfNewWrapper 
    @author Vlocity
    @version 02/08/2016
	
*/

@isTest
private class OCOM_QuotePdfNewWrapperContExtentionTest {
	
	public OCOM_QuotePdfNewWrapperContExtentionTest(){
		setup();
	}
	private  Contact testCont; 
	private  Account testAcct; 
	private  Opportunity testOpp; 
	private  Quote testQuote; 
	private  OCOM_QuotePdfNewWrapperContExtention cont;
	//private  EmailTemplate eTemp; 

	private void setup(){

			//eTemp = new EmailTemplate (developerName = 'Default',  TemplateType= 'Text', Name = 'Default'); 
   //         insert eTemp; 
   			testAcct = new Account (name='testAccount');
   			insert testAcct; 
   			testCont = new Contact (lastName='testContact', AccountId = testAcct.id); 
   			insert testCont;
   		    testOpp = new Opportunity(name='testOpp',StageName='Discovery',CloseDate=System.today().addDays(1), AccountId = testAcct.id);
	   		insert testOpp; 
	    	testQuote= new Quote(Name='testQuote',ContactId = testCont.id, OpportunityId=testOpp.id);
			insert testQuote;

			Test.setCurrentPage(Page.OCOM_QuotePdfNewWrapper);
			ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(testQuote);
        	ApexPages.currentPage().getParameters().put('Id',testQuote.id);
        	cont = new OCOM_QuotePdfNewWrapperContExtention(sc);
	}

	@isTest static void test_SavePDF() {
		OCOM_QuotePdfNewWrapperContExtentionTest tes = new OCOM_QuotePdfNewWrapperContExtentionTest();
		tes.cont.SavePDF();
        // Call save PDF this will save to attachment 
        Test.startTest();
        	tes.cont.SavePDF();
        Test.stopTest();
        // Check if the attachmant is saved
        List<Attachment> atts = [select id from Attachment where parentId = :tes.testQuote.id];
        System.assert(atts.size()==2);

	}
	
	@isTest static void test_SendEmail() {
		 OCOM_QuotePdfNewWrapperContExtentionTest tes = new OCOM_QuotePdfNewWrapperContExtentionTest();
         // save a template to SMB_QuoteEmailTemplates__c
         SMB_QuoteEmailTemplates__c smbTemp = new SMB_QuoteEmailTemplates__c(Name = 'Default',TemplateName__c = 'Default');
         insert smbTemp; 
         // send before attachement is created. 
         tes.cont.SendEmail();
         system.assertEquals('Error sending email: PDF has not generated yet.', tes.cont.ErrorMsg);

         // create and save pdf to attachment 
         tes.cont.savePDF();
         
         tes.cont.CCEmails = 'tes1@gmail.com;tes2@gmail.com';
         Test.startTest();
        	tes.cont.SendEmail();
         Test.stopTest();

         // Check Success Message 
         //System.assertEquals('The email was successfully sent.', tes.cont.ErrorMsg);
         System.assertEquals('The email was successfully sent.', tes.cont.SuccessMsg);
         tes.cont.openModal();
         System.assert(tes.cont.displayModal);
         tes.cont.hideModal();
         System.assert(!tes.cont.displayModal);

	}
	
}