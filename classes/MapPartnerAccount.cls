public with sharing class MapPartnerAccount {
    public static void populateFieldOnObj(List<sObject> objList){ 
    
      //create set of ownerid
        Set<Id> ownerId = new Set<Id> ();
         for(sObject led:objList){
             ownerId.add((Id) led.get('OwnerId'));
        }
        System.debug('##########OwnerId!!!!!'+ownerId);
   
      //create map of user    
        Map<Id,User> mapOwner = new Map<Id,User>([select u.UserType, u.Contact.AccountId from User u where id in :ownerId]);
        System.debug('######mapOwner!!!!!'+mapOwner);
    
        for(sObject o : objList){
            User usr =mapOwner.get((Id)o.get('OwnerId'));
            if(usr != null && usr.UserType.equalsIgnoreCase('PowerPartner')){
                o.put('Partner_Account__c',usr.Contact.AccountId) ;
                System.debug('######PartnerAccount!!!'+usr.Contact.AccountId); 
            } else{
                o.put('Partner_Account__c',null) ;
            }
        }
    }

}