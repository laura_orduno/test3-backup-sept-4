global with sharing class VlProductAvailabilityImpl implements vlocity_cmt.GlobalInterfaces.ProductAvailabilityInterface {
        
    public List<SObject> getAvailableProducts(SObject s, List<PriceBookEntry> searchResult) {   
    
        System.Debug('#### Vloicty ####: getAvailableProducts 1 =' + Limits.getHeapSize());
        system.debug('searchResult -- '+JSON.serializePretty(searchResult));
        
        Id objectId = (Id) s.get('Id');
        
        Map<String, OrdrProductEligibilityManager.Offer> availableOffers = OrdrProductEligibilityManager.getAvailableOffers(objectId);
        
        System.Debug('#### Vloicty ####: getAvailableProducts 2 =' + Limits.getHeapSize());
        
        String[] availableOfferIds = OrdrProductEligibilityManager.getAvailableOfferIds(availableOffers);
        
        system.debug('availableOfferIds -- '+JSON.serializePretty(availableOfferIds));
        
        System.Debug('#### Vloicty ####: getAvailableProducts 3 =' + Limits.getHeapSize());
        System.Debug('#### Vloicty ####: availableOfferIds.size()='+availableOfferIds.size());
        
        OrdrProductEligibilityManager.validateCartLineItems(objectId, availableOfferIds);
        
        System.Debug('#### Vloicty ####: getAvailableProducts 4 =' + Limits.getHeapSize());
        
        Set<Id> searchResultIds = new Set<Id>();
        if (searchResult != null) {
            searchResultIds = (new Map<Id, PriceBookEntry>(searchResult)).keySet();
        } 
        
        List<SObject> sObjectList = filterPriceBookEntry(objectId, availableOfferIds, searchResultIds);
        
        System.Debug('#### Vloicty ####: getAvailableProducts 5 =' + Limits.getHeapSize());
        System.Debug('#### Vloicty ####: sObjectList.size()='+sObjectList.size());             
        System.Debug('#### Vloicty ####: getAvailableProducts 6 =' + Limits.getHeapSize());
        System.Debug('#### sObjectList=' + sObjectList);

        if(sObjectList.size() > 0 || Test.isRunningTest()){
            
            return sObjectList;
            
        } else {
            throw new OrdrExceptions.ResultNotFoundException(Label.PCOF0001);
        }
    }
    
    private List<SObject> filterPriceBookEntry(Id objectId, String[] availableOfferIds, Set<Id> searchResultIds) {
        
        String priceBookId = OrdrProductEligibilityManager.retrievePriceBookId(objectId);
        system.debug('Eligibility Check Price Book Id: ' + priceBookId);
        
        system.debug('searchResultIds -- '+JSON.serializePretty(searchResultIds));
        system.debug('availableOfferIds filterPBE -- '+JSON.serializePretty(availableOfferIds));
        
        //String query = 'SELECT ' + OrdrUtilities.commaSeparatedFields('PricebookEntry', new List<String>{'Product2'});
        String query = 'SELECT Id, Pricebook2Id, Product2Id, ProductCode, Product2.Description, UnitPrice, Name, Product2.Name, Product2.vlocity_cmt__JSONAttribute__c, IsActive';
        query += ' FROM PricebookEntry';    
        query += ' WHERE Pricebook2Id = :priceBookId';
        query += ' AND Id in :searchResultIds';
        query += ' AND Product2.sellable__c = true';
        query += ' AND Product2.isActive = true';
        query += ' AND Product2.orderMgmtId__c in :availableOfferIds';

        List<SObject> sObjList = database.query(query);
        
        system.debug('sObjList -- '+JSON.serializePretty(sObjList));

        return sObjList;
    }
    

    
}