global class RestApi_MyAccountHelper_CreateComment {    

	public static final string EMAIL2CASE_PROFILE = RestApi_MyAccountHelper_Utility.EMAIL2CASE_PROFILE;
    public static final string INTEGRATION_PROFILE = RestApi_MyAccountHelper_Utility.INTEGRATION_PROFILE;

	// GetCasesRequestObject object classes     
    public class RequestUser{
        public String uuid = '';
    }

    public class RequestComment{
        public string body = '';
    	public string parentId = '';
    }

    
    public class CreateCommentRequestObject extends RestApi_MyAccountHelper.RequestObject{
        public RequestUser user;
        public RequestComment comment;
    }
    
    // We may have to add this if we need to provide further context around the CaseComment
	public class CaseCommentResponse{
		public Boolean agent = false;
		public CaseComment caseComment = null;
    }
    
    // CreateCommentResponse	
	// derived child class to act as the JSON response
    global class CreateCommentResponse extends RestApi_MyAccountHelper.Response{
        public CaseCommentResponse caseCommentCreated = null;

        public CreateCommentResponse(CreateCommentRequestObject request){
	        super(request);
            
            if(request.user == null){
                throw new xException('User object for request not defined');                
            }    
            // check if uuid specified
            if(request.user.uuid == null){
                throw new xException('Field "uuid" in user object for request not defined');                
            }

            // check if case information was submitted
            if(request.comment == null){
                 throw new xException('No comment information was submitted for creation');   
            }

            if(request.comment.parentId == null){
                throw new xException('No comment parentId was submitted for creation');
            }

            if(request.comment.body == null){
                throw new xException('No comment body was submitted for creation');
            }

            // CONFIRM THAT DATA LIVES WITHIN SALESFORCE
            // get user by uuid ( refers to the field "federationIdentifier" in the Salesforce User object)		
            User u = RestApi_MyAccountHelper_Utility.getUser(request.user.uuid);

            // if user not found
            if( u == null ){
                throw new xException('User not found for uuid: ' + request.user.uuid);                
            }

			if (!u.Contact.MASR_Enabled__c) {
				throw new xException('User is not MASR Enabled.');
			}

            Case c = RestApi_MyAccountHelper_Utility.getCaseDetails( request.comment.parentId );
            if( c == null ){
                throw new xException( 'Case not found for Case Id: ' + request.comment.parentId );                
            }

            CaseComment newCaseComment = new CaseComment();
            newCaseComment.ParentId = request.comment.parentId;
            newCaseComment.CommentBody = u.name + ': ' + request.comment.body;
            newCaseComment.IsPublished = true;


            try{
				insert newCaseComment;    
            	caseCommentCreated = getCaseComment(newCaseComment.Id);

            	//send email to collaborators
            }
            catch(DMLException e){
                throw new xException(e.getMessage());                
            }
        
        }    

	    private CaseCommentResponse getCaseComment(Id caseCommentId){
	        try{                
	            CaseComment comment = [
	            	SELECT 	Id, ParentId, Commentbody, CreatedDate, CreatedBy.ProfileId, IsPublished 
	                FROM 	CaseComment 
	                WHERE 	IsPublished = true 
	                AND 	Id = :caseCommentId
	                LIMIT 1
	            ];

	            CaseCommentResponse cr = new CaseCommentResponse();
	            cr.agent = comment.CreatedBy.ProfileId != INTEGRATION_PROFILE && comment.CreatedBy.ProfileId != EMAIL2CASE_PROFILE;
	            cr.caseComment = comment;           

	            return cr;
	        }
	        catch(QueryException e){
	            // cannot throw execption if null as that is valid.
	            //throw new xException('Case: ' + e.getMessage());

	            return null;  
	        }

	        catch(Exception e){
	            throw new xException('Case Comments: ' + e.getMessage());                            
	        }
	    }
    } 


    // object to be returned as JSON response	
    public CreateCommentResponse response = null;
    
    public RestApi_MyAccountHelper_CreateComment(String requestString){
        // convert request to object 
        CreateCommentRequestObject CreateCommentRequest = (CreateCommentRequestObject)JSON.deserialize(requestString, CreateCommentRequestObject.class);                    
        response = new CreateCommentResponse(CreateCommentRequest);                    
    }        
}