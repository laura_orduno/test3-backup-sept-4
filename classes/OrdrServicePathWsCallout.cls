/* 2015-12-12[Roderick de Vera]
 * Helper class which invokes the Web Service Callout for Clearance Check
 */
public class OrdrServicePathWsCallout {
    
    public static OrdrTnResourceInfoRetrievalConfiguration.ResourceConfigurationCollectionMessage findResource(Map<String, Object> inputMap) {
	   
        OrdrTnResourceInfoRetrievalConfiguration.ResourceConfigurationCollectionMessage resourceConfig;
    
    	TpCommonMessage.LookupRequestMessage lookupRequest = preparePayload(inputMap);
		TpCommonBaseV3.EntityWithSpecification[] sourceCriteria = lookupRequest.sourceCriteria;
		TpCommonBaseV3.EntityWithSpecification[] targetCriteria = lookupRequest.targetCriteria;
        try {
            OrdrTnResourceInfoRetrievalFindResource.FindResourceSOAPBinding servicePort = new OrdrTnResourceInfoRetrievalFindResource.FindResourceSOAPBinding();
            servicePort = prepareCallout(servicePort); 
            
            resourceConfig = servicePort.findResource(sourceCriteria, targetCriteria);
           
        } 
        catch (CalloutException e) {

            OrdrUtilities.auditLogs('OrdrServicePathWsCallout.findResource', 
                                    'ASF', 
                                    'ClearanceCheckService', 
                                    null, 
                                    null, 
                                    true, 
                                    e.getMessage(), 
                                    e.getStackTraceString());
            
            throw new OrdrExceptions.ServicePathServiceException(e.getMessage(), e);        

        }
        return resourceConfig;
    }    
    
    private static OrdrTnResourceInfoRetrievalFindResource.FindResourceSOAPBinding 
        prepareCallout(OrdrTnResourceInfoRetrievalFindResource.FindResourceSOAPBinding servicePort) {
            Ordering_WS__c findResourceWirelineEndpoint = Ordering_WS__c.getValues('ClearanceCheckEndpoint');           
            servicePort.endpoint_x = findResourceWirelineEndpoint.value__c;
            // Set SFDC Webservice call timeout
            servicePort.timeout_x = OrdrConstants.WS_TIMEOUT_MS;    
            
            if (servicePort.endpoint_x.startsWith('https://xmlgwy')) {
                Ordering_WS__c wsUserName =  Ordering_WS__c.getValues(OrdrConstants.CREDENTIAL_USERNAME);
                Ordering_WS__c wsPassword =  Ordering_WS__c.getValues(OrdrConstants.CREDENTIAL_PASSWORD); 
                String credentials = wsUserName.value__c + ':' + wsPAssword.value__c;
                String encodedUserNameAndPassword = EncodingUtil.base64Encode(Blob.valueOf(credentials));   
                
                servicePort.inputHttpHeaders_x = new Map<String, String>();
                servicePort.inputHttpHeaders_x.put('Authorization', 'Basic ' + encodedUserNameAndPassword);
            } else {
                servicePort.clientCertName_x = OrdrConstants.SDF_CERT_NAME;
            }
            
            return servicePort;
    } 
    
    private static TpCommonMessage.LookupRequestMessage preparePayload(Map<String, Object> inputMap) {
        
        TpCommonMessage.LookupRequestMessage lookupRequest = new TpCommonMessage.LookupRequestMessage();
        TpCommonBaseV3.EntityWithSpecification sourceCriteria = new TpCommonBaseV3.EntityWithSpecification();
        
        Map<String, String> mandatoryFields = new Map<String, String>();
        mandatoryFields.put(OrdrConstants.FMS_ID, OrdrConstants.FMS_ID);
        mandatoryFields.put(OrdrConstants.ADDR_MUNICIPALITY, OrdrConstants.ADDR_MUNICIPALITY);
        mandatoryFields.put(OrdrConstants.ADDR_PROVINCE, OrdrConstants.ADDR_PROVINCE);
        mandatoryFields.put(OrdrConstants.COID, OrdrConstants.COID);
        
        /*create sourceCriteria/Specification section
        */
        TpCommonBaseV3.EntitySpecification specification = new TpCommonBaseV3.EntitySpecification();
        specification.Name = 'ASF';
        specification.Type_x = 'Access Path';
        specification.Category = 'Physical Resource';
        sourceCriteria.Specification = specification;
        
        /*create sourceCriteria/CharacteristicValue/Characteristic/Name section
        */
        List<TpCommonBaseV3.CharacteristicValue> characteristicValues = new List<TpCommonBaseV3.CharacteristicValue>();
        if (inputMap.containsKey(OrdrConstants.FMS_ID) && inputMap.get(OrdrConstants.FMS_ID) != null) {
            characteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.FMS_ID, (String)inputMap.get(OrdrConstants.FMS_ID)));
            mandatoryFields.remove(OrdrConstants.FMS_ID);
        } 
        if (inputMap.containsKey(OrdrConstants.ADDR_MUNICIPALITY) && inputMap.get(OrdrConstants.ADDR_MUNICIPALITY) != null) {
            characteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.ADDR_MUNICIPALITY, (String)inputMap.get(OrdrConstants.ADDR_MUNICIPALITY)));
            mandatoryFields.remove(OrdrConstants.ADDR_MUNICIPALITY);
        } 
        if (inputMap.containsKey(OrdrConstants.ADDR_PROVINCE_CODE) && inputMap.get(OrdrConstants.ADDR_PROVINCE_CODE) != null) {
            characteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.ADDR_PROVINCE, (String)inputMap.get(OrdrConstants.ADDR_PROVINCE_CODE)));
            mandatoryFields.remove(OrdrConstants.ADDR_PROVINCE);
        } 
        if (inputMap.containsKey(OrdrConstants.COID) && inputMap.get(OrdrConstants.COID) != null) {
            characteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.COID, (String)inputMap.get(OrdrConstants.COID)));
            mandatoryFields.remove(OrdrConstants.COID);
        }         
        characteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.USER_ID, OrdrUtilities.getCurrentUserTelusId()));
        
        if (mandatoryFields.size() > 0) {
            String requiredFields = JSON.serialize(mandatoryFields.values());
            System.debug('requiredFields:' + requiredFields);
            String msg = Label.DFLT0004;
            msg = msg.replace('{0}', 'Clearance Check');
            msg = msg.replace('{1}', requiredFields);
            throw new OrdrExceptions.InvalidParameterException(msg);            
        }
        
        sourceCriteria.CharacteristicValue = characteristicValues;
        lookupRequest.SourceCriteria = new List<TpCommonBaseV3.EntityWithSpecification>();
        lookupRequest.SourceCriteria.add(sourceCriteria);
        
        return lookupRequest;
    }
    
}