@isTest
private class test_CalculateTFScoreMax100{
    
    public static testMethod void test1()
    {
        InsertNewTelusforceCase(); 
        UpdateHasAVPDeclaredThisAsAPriority('No');       
    }
    
    private static void InsertEmptyTelusforceCase() { 
        SMET_Request__c tfCase = new SMET_Request__c();
        
        tfCase.Subject__c = 'Test entry';
        tfCase.Description__c = 'Test entry';        
        
        insert tfCase;        
    }             
    
    public static void InsertNewTelusforceCase() { 
        SMET_Request__c tfCase = new SMET_Request__c();
        
        tfCase.Subject__c = 'Test entry';
        tfCase.Description__c = 'Test entry';
    
        tfCase.Has_a_VP_declared_this_as_a_priority__c = 'Yes';
        tfCase.Enterprise__c = true;
        tfCase.Small_Business__c = true;
        tfCase.Medium_Business__c = true;
        tfCase.TQ__c = true;
        tfCase.THFS__c = true;
        tfCase.Telus_International__c = true;
        tfCase.Dealer_Channel__c = true;
        
        
        insert tfCase;        
    }             
    
    private static void UpdateHasAVPDeclaredThisAsAPriority(String isPrority) { 
        SMET_Request__c[] tfCases = [SELECT id, Has_a_VP_declared_this_as_a_priority__c FROM SMET_Request__c WHERE name = 'TFC_01'] ;        
        
        for(SMET_Request__c tfCase : tfCases)
        {
            if(tfCase != null)
            {
                tfCase.Has_a_VP_declared_this_as_a_priority__c = isPrority;
                
                update tfCase;
            }
        }
    }             
}