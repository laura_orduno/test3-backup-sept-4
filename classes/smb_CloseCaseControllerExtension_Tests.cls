@isTest(seeAllData=true)
private class smb_CloseCaseControllerExtension_Tests {

	private static testMethod void testController() {
		
		Case caseRecord = new Case (
			
		);
		
		insert caseRecord;
		
		PageReference pageRef = Page.smb_CloseCase;
		pageRef.getParameters().put('Id', caseRecord.Id);
		
        Test.setCurrentPage(pageRef);
		
		ApexPages.StandardController controller = new ApexPages.StandardController(caseRecord);
		
		smb_CloseCaseControllerExtension extension = new smb_CloseCaseControllerExtension(controller);
		
		System.assert(extension.CaseClosed == false);
		
		System.assert(extension.headerFieldNames != null);
		
		System.assert(extension.bodyFieldNames != null);
		
		System.assert(extension.headerFields != null);
		
		System.assert(extension.bodyFields != null);
		
	}

}