/*****************************************************
    Name: EmailNotificationForCustomStatusTool
    Usage: This class sends out notification for the AZ Cust status Tool object updates
    Author – Clear Task
    Date – 12/27/2010    
    Revision History
******************************************************/

public class EmailNotificationForCustomStatusTool{
    /*
     *  GREEN Distribution group by CBUCID 
     */
    public static void emailNotificationForGreenDistribution(
    	Map<String, AtoZ_CustStatusTool__c> setCbucid,
    	Map<String, String> previousColorStatus,
    	Set<String> mapModiName
    ){
    	try{ 
        List<AtoZ_Assignment__c> listEmailToEmployee = [
        	SELECT CBUCID__R.CBUCID__c, CBUCID__R.Name, CBUCID__R.CBU_Name__c, CBUCID__R.CBU_CD__c,
        				 CBUCID__R.Sub_Segment_Description__c, CBUCID__R.Sub_Segment_Code__r.Name, CBUCID__R.Alt_Segment__c, Id,
        				 AtoZ_Field__c, AtoZ_Contact__r.Id, AtoZ_Contact__r.Full_Name__c, AtoZ_Contact__r.Email__c,
        				 AtoZ_Contact__r.Manager_Email__c, AtoZ_Contact__r.Director_Email__c
        	FROM AtoZ_Assignment__c
        	WHERE CBUCID__R.CBUCID__c in :setCbucid.keySet()
        	order by CBUCID__R.CBUCID__c
        	limit 1000
        ];
        
        //for WLN/WLS User Name
        Map<String, Map<String, String>> mapWlnWlsDetailForGreen = new Map<String, Map<String, String>>();
        //Email ID for Employee
        Map<String, Set<String>> mapEmailListForGreen = new Map<String, Set<String>>();
        Set<String> emailListForGreen = new Set<String>();
        //Additional Employee
        Set<String> emailListForAddEmpYellowOrRed = new Set<String>();

        String tempCbucid= '';
        
        if(listEmailToEmployee.size()>0) {
        	tempCbucid=listEmailToEmployee[0].CBUCID__R.CBUCID__c;                
        }
        
        Integer sizeList = 1;    
        for(AtoZ_Assignment__c a: listEmailToEmployee) {
	        if(!tempCbucid.equals(a.CBUCID__R.CBUCID__c)) {
		        mapEmailListForGreen.put(a.CBUCID__R.CBUCID__c, emailListForGreen);
		        emailListForGreen = new Set<String>();
		        tempCbucid = a.CBUCID__R.CBUCID__c;
	        }
	        
	        if(a.AtoZ_Contact__r.Email__c != null && a.AtoZ_Contact__r.Email__c != ''){
	        	emailListForGreen.add(a.AtoZ_Contact__r.Email__c);                                               
	        }
	        
	        if(listEmailToEmployee.size() == sizeList){
	        	mapEmailListForGreen.put(a.CBUCID__R.CBUCID__c, emailListForGreen); 
	        }
	        
	        sizeList++;             
        }   
        

        List<Sales_Assignment__c> listEmailToWlsAndWlnMgrDir = [
        	SELECT s.Account__r.cbucid__c, s.Id, s.Role__c, s.User__r.firstname, s.User__r.lastname, s.User__r.Email,
        				 s.User__r.UserRole.Name
        	FROM Sales_Assignment__c s
        	WHERE s.Account__r.cbucid__c in : setCbucid.keySet()
        	order by s.Account__r.cbucid__c
        	limit 1000
        ];
        
        mapWlnWlsDetailForGreen = findWlsWlnUserName(listEmailToWlsAndWlnMgrDir, mapWlnWlsDetailForGreen, setCbucid);
                
        //Send Email  Notification     
        if(mapEmailListForGreen.size()>0){
        	sendEmail(mapEmailListForGreen, previousColorStatus, setCbucid, mapWlnWlsDetailForGreen, mapModiName,
          emailListForAddEmpYellowOrRed);
        }
      } catch(Exception e){}
    }

	/*
	 *  YELLOW or RED Distribution group by CBUCID 
	 */ 
	public static void emailNotificationForYellowOrRedDistribution(
   	Map<String, AtoZ_CustStatusTool__c> setCbucid,
   	Map<String, String> previousColorStatus,
   	Set<String> mapModiName,
   	String status
  ){
      try{
        //for Email List
        Map<String, Set<String>> mapEmailListForYellowOrRed=new Map<String, Set<String>>();
        //WLN Account Prime  , WLN Director , WLS Sales Rep , WLS Director , WLS Manager -Name
        Map<String, Map<String, String>> mapWlnWlsDetailForYellowOrRed=new Map<String, Map<String, String>>();
        //Query for WLN Account Prime  , WLN Director , WLS Sales Rep , WLS Director , WLS Manager
       
        List<Sales_Assignment__c> listEmailToWlsAndWlnMgrDir=[
        	SELECT Account__r.cbucid__c, Role__c, User__r.firstname, User__r.lastname, User__r.Email, User__r.UserRole.Name
        	FROM Sales_Assignment__c
        	WHERE Account__r.cbucid__c in :setCbucid.keySet()
        	order by Account__r.cbucid__c limit 1000
        ];
        
        // Employee List
        List<AtoZ_Assignment__c> listEmailToEmployee = new List<AtoZ_Assignment__c>();
        
        listEmailToEmployee = [
	        SELECT  CBUCID__R.CBUCID__c,  CBUCID__R.Name, CBUCID__R.CBU_Name__c, CBUCID__R.CBU_CD__c,
	        				CBUCID__R.Sub_Segment_Description__c, CBUCID__R.Sub_Segment_Code__r.Name, CBUCID__R.Alt_Segment__c,
	        				AtoZ_Field__c, AtoZ_Contact__r.Id, AtoZ_Contact__r.Full_Name__c, AtoZ_Contact__r.Email__c,
	        				AtoZ_Contact__r.Manager_Email__c, AtoZ_Contact__r.Director_Email__c
	        FROM AtoZ_Assignment__c
	        WHERE CBUCID__R.CBUCID__c in :setCbucid.keySet()
	        order by CBUCID__R.CBUCID__c
	        limit 1000
        ];
        
        //Additional Employee
        Set<String> emailListForAddEmpYellowOrRed = new Set<String>();  
        List<Employee__c> listEmailToAdditionalEmps = new List<Employee__c>();
        
        if(status.equals('Red')){
	        mapEmailListForYellowOrRed = findWlsWlnUserEmailFromUser(listEmailToWlsAndWlnMgrDir, mapEmailListForYellowOrRed, 'Red');
	        
	        listEmailToAdditionalEmps = [
	        	SELECT Email__c, CST_Email_Red__c
	        	FROM Employee__c
	        	WHERE CST_Email_Red__c = true
	        ];
	                    
        } else if(status.equals('Yellow')) {
	        mapEmailListForYellowOrRed = findWlsWlnUserEmailFromUser(listEmailToWlsAndWlnMgrDir, mapEmailListForYellowOrRed, 'Yellow');
	        
	        listEmailToAdditionalEmps = [
	        	SELECT Email__c, CST_Email_Yellow__c
	        	FROM Employee__c
	        	WHERE CST_Email_Yellow__c = true
	        ];                
        }
        
        //WLN/WLS Manager and Director
        mapEmailListForYellowOrRed = findWlsWlnUserEmailFromEmployee(listEmailToEmployee, mapEmailListForYellowOrRed);
       
        Set<String> emailListForYellowOrRed = new Set<String>();     
        String tempCbucid= '';
        
        if(listEmailToEmployee.size()>0) {
	        tempCbucid = listEmailToEmployee[0].CBUCID__R.CBUCID__c; 
	        emailListForYellowOrRed = mapEmailListForYellowOrRed.get(tempCbucid);          
        }
        
        Integer sizeList = 1;       
        
        for(AtoZ_Assignment__c a: listEmailToEmployee){
	        if(!tempCbucid.equals(a.CBUCID__R.CBUCID__c)) {                    
            mapEmailListForYellowOrRed.remove(tempCbucid);               
            mapEmailListForYellowOrRed.put(tempCbucid,emailListForYellowOrRed);
            emailListForYellowOrRed = new Set<String>();  
            tempCbucid = a.CBUCID__R.CBUCID__c;
	        }
	        
	        if(a.AtoZ_Contact__r.Email__c != null && a.AtoZ_Contact__r.Email__c != '') {
	        	emailListForYellowOrRed.add(a.AtoZ_Contact__r.Email__c);                                               
	        }
	        
	        if(listEmailToEmployee.size() == sizeList) {
	        	mapEmailListForYellowOrRed.remove(tempCbucid);  
	          mapEmailListForYellowOrRed.put(tempCbucid,emailListForYellowOrRed); 
	        }
	        
	        sizeList++;           
        }

        for(Employee__c a : listEmailToAdditionalEmps) {                
	        if(a.Email__c != null && a.Email__c != '') {
	        	emailListForAddEmpYellowOrRed.add(a.Email__c);                                               
	        }                          
        }      

        mapWlnWlsDetailForYellowOrRed = findWlsWlnUserName(listEmailToWlsAndWlnMgrDir, mapWlnWlsDetailForYellowOrRed, setCbucid); 
        
        //Send Email  Notification     
        if(mapEmailListForYellowOrRed.size() > 0 || emailListForAddEmpYellowOrRed.size() > 0) {
        	sendEmail(mapEmailListForYellowOrRed, previousColorStatus, setCbucid, mapWlnWlsDetailForYellowOrRed, mapModiName,
        		emailListForAddEmpYellowOrRed);
        }
        
      } catch(Exception e) {}
    }
    
    /*
     *  Method for Sending Email group by CBUCID
     */
    public static void sendEmail(
    	Map<String, Set<String>> mapEmailList,
    	Map<String, String> previousColorStatus,
    	Map<String, AtoZ_CustStatusTool__c> mapCustomTool,
    	Map<String, Map<String, String>> mapWlnWlsDetail,
    	Set<String> mapModiName,
    	Set<String> emailListForAddEmpYellowOrRed
    ){
        Map<String,String> modifiedUserName = new Map<String,String>();
        List<User> modifiedUser = [
        	SELECT firstname, lastname
        	FROM user
        	WHERE id in:mapModiName
        ];
        
        for(User u: modifiedUser){
        	modifiedUserName.put(u.id, u.firstname + ' ' + u.lastname);
        }
        
        List<Cust_Business_Unit_Cust_ID__c> listCBUCID = [
        	SELECT CBUCID__c, Name, CBU_Name__c, CBU_CD__c, Sub_Segment_Description__c, Sub_Segment_Code__r.Name, Alt_Segment__c
        	FROM Cust_Business_Unit_Cust_ID__c
        	WHERE CBUCID__c in:mapCustomTool.keySet()
        	order by CBUCID__c
        	limit 1000
        ];
              
        Map<String, Cust_Business_Unit_Cust_ID__c> mapCbucidDetail = new Map<String, Cust_Business_Unit_Cust_ID__c>();
        
        for(Cust_Business_Unit_Cust_ID__c c : listCBUCID) {
        	mapCbucidDetail.put(c.CBUCID__c, c);
        }
        
        for(String cbucid : mapCustomTool.keySet()){            
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            Cust_Business_Unit_Cust_ID__c cbucidDetail = mapCbucidDetail.get(cbucid);
            AtoZ_CustStatusTool__c custStatus = mapCustomTool.get(cbucid);
            Map<String, String> wlnWlsDetail = mapWlnWlsDetail.get(cbucid);
            String strSubject =
            	' Customer Status Change: ' + custStatus.Status__c + ' / ' + cbucidDetail.Name + ' - ' + custStatus.Service_Type__c;
            Set<String> toEmailList = new Set<String>();
            toEmailList = mapEmailList.get(cbucid);
            
            if(emailListForAddEmpYellowOrRed!= null && emailListForAddEmpYellowOrRed.size() > 0){
            	if(toEmailList == null) {
            		toEmailList = new Set<String>();
            	}
              toEmailList.addAll(emailListForAddEmpYellowOrRed ); 
            }            

            toEmailList.add('rmehta@cleartask.com');            
            toEmailList.add('aseo@cleartask.com');            
            toEmailList.add('Allan.Roszmann@telus.com'); 
                      
            String strMsgBody = System.Label.HeaderMsg1
                                +'\n'+ System.Label.HeaderMsg2
                                +'\n\n'+'-------------------------------------------------------------------------------------------------------------------------'
                                +'\n'+ System.Label.CBUName +' ' + cbucidDetail.Name 
                                +'\n'  +'           '+ System.Label.CBU +' '+  cbucidDetail.CBUCID__c         
                                +'\n\n' + System.Label.CBUChannel +' '+ cbucidDetail.CBU_Name__c +'/'+ cbucidDetail.Sub_Segment_Description__c
                                +'\n' + '                       '+cbucidDetail.CBU_CD__c +'/'+ cbucidDetail.Sub_Segment_Code__r.Name
                                +'\n'+'-------------------------------------------------------------------------------------------------------------------------'
                                + '\n\n' 
                                +'\n'  +System.Label.Status+'                 '+ custStatus.Status__c + '       ('+ previousColorStatus.get(cbucid)+ ')'
                                +'\n'  +System.Label.WirelineWrls+' '+ custStatus.Service_Type__c
                                +'\n'  +System.Label.DateOfChange+'    '+ custStatus.LastModifiedDate
                                +'\n'  +System.Label.ModifiedBy+'          '+ modifiedUserName.get(custStatus.LastModifiedById)
                                +'\n' 
                                +'\n' +System.Label.WirelineCSM+'               '+wlnWlsDetail.get('DIR')
                                +'\n' +System.Label.WirelessCSM+'              '+wlnWlsDetail.get('WLS_DIR')
                                +'\n' +System.Label.WirelineAccountPrime+' '+wlnWlsDetail.get('ACCTPRIME')  
                                +'\n' +System.Label.WirelessSalesRep+'      '+wlnWlsDetail.get('WLS_SR')  
                                +'\n\n'
                                +'\n' +System.Label.Notes
                                +'\n' + custStatus.Notes__c
                                +'\n\n'+System.Label.ActionPlan
                                +'\n' + custStatus.Action_Plan__c
                                +'\n\n'
                                +'\n'+'============================================================='
                                +'\n'+System.Label.FooterMsg1
                                +'\n'+System.Label.FooterMsg2
                                +'\n'+'=============================================================';
                                       
            email.setPlainTextBody(strMsgBody);
            email.setSaveAsActivity(false);
            email.setSubject(strSubject);
            
            List<String> toMail = new List<String>();
            for(String s: toEmailList){
            	toMail.add(s);
            }          
            
            email.setToAddresses(toMail);
            Messaging.SendEmailResult[] sendResult;
            
            try {
            	sendResult = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
            } catch(Exception e) {}
        }
    }
    
    /*
     *  Method for WLN Account Prime  , WLN Director , WLS Sales Rep , WLS Director , WLS Manager Name 
     */   
    public static Map<String, Map<String, String>> findWlsWlnUserName(
    	List<Sales_Assignment__c> listEmailToWlsAndWlnMgrDir,
    	Map<String, Map<String, String>> mapWlnWlsDetailForGreen,
    	Map<String, AtoZ_CustStatusTool__c> mapCustomTool
    ){
        String tempCbucid= '';
        
        if(listEmailToWlsAndWlnMgrDir.size() > 0) {
	        tempCbucid = listEmailToWlsAndWlnMgrDir[0].Account__r.CBUCID__c;                
	        Map<String, String> mapWlnWlsByCbucid = new Map<String, String>();
	        mapWlnWlsByCbucid.put('ACCTPRIME', '');
	        mapWlnWlsByCbucid.put('DIR', '');
	        mapWlnWlsByCbucid.put('WLS_SR', '');
	        mapWlnWlsByCbucid.put('WLS_MGR', '');
	        mapWlnWlsByCbucid.put('WLS_DIR', '');
                    
	        Integer index = 1;
	        Boolean flag = true;       
	        for(Sales_Assignment__c a : listEmailToWlsAndWlnMgrDir) {
		        if(!tempCbucid.equals(a.Account__r.cbucid__c)) {
	            mapWlnWlsDetailForGreen.put(tempCbucid, mapWlnWlsByCbucid);
	            mapWlnWlsByCbucid = new Map<String, String> ();                        
	            mapWlnWlsByCbucid.put('ACCTPRIME', '');
	            mapWlnWlsByCbucid.put('DIR', '');
	            mapWlnWlsByCbucid.put('WLS_SR', '');
	            mapWlnWlsByCbucid.put('WLS_MGR', '');
	            mapWlnWlsByCbucid.put('WLS_DIR', '');
	            tempCbucid = a.Account__r.cbucid__c;
		        }
		                          
            if(mapWlnWlsByCbucid.containsKey(a.Role__c) && mapWlnWlsByCbucid.get(a.Role__c) == '' &&
            	 a.User__r.firstName != null && a.User__r.firstName != '') {
	            mapWlnWlsByCbucid.remove(a.Role__c);
	            mapWlnWlsByCbucid.put(a.Role__c, a.User__r.firstName + ' ' + a.User__r.lastName);    
            }
                             
            if(listEmailToWlsAndWlnMgrDir.size() == index){
            	mapWlnWlsDetailForGreen.put(tempCbucid, mapWlnWlsByCbucid);
            }
            
            index++;                                   
	        }                
        } else {
	        for(String cbucid: mapCustomTool.keySet()) {
		        if(!mapWlnWlsDetailForGreen.containsKey(cbucid)) {
	            Map<String, String> mapWlnWlsByCbucid = new Map<String, String>();
	            mapWlnWlsByCbucid.put('ACCTPRIME', '');
	            mapWlnWlsByCbucid.put('DIR', '');
	            mapWlnWlsByCbucid.put('WLS_SR', '');
	            mapWlnWlsByCbucid.put('WLS_MGR', '');
	            mapWlnWlsByCbucid.put('WLS_DIR', '');
	            mapWlnWlsDetailForGreen.put(cbucid, mapWlnWlsByCbucid);
		        } 
	        }
        }      
        
      	return mapWlnWlsDetailForGreen;
    }
    
    /*
     *  Method for EMail List for WLN Account Prime  , WLN Director , WLS Sales Rep , WLS Director , WLS Manager 
     */ 
    public static Map<String, Set<String>> findWlsWlnUserEmailFromUser(
    	List<Sales_Assignment__c> listEmailToWlsAndWlnMgrDir,
    	Map<String, Set<String>> mapEmailListForYellowOrRed,
    	String strColor
    ){
        String tempCbucid = '';
        Set<String> emailListForYellowOrRed = new Set<String>();   
        
        if(listEmailToWlsAndWlnMgrDir.size() > 0) {
	        tempCbucid = listEmailToWlsAndWlnMgrDir[0].Account__r.CBUCID__c; 
	        
	        if(mapEmailListForYellowOrRed.containsKey(tempCbucid)) {
	        	emailListForYellowOrRed = mapEmailListForYellowOrRed.get(tempCbucid);
	        } else {
	          emailListForYellowOrRed = new Set<String>();  
	          mapEmailListForYellowOrRed.put(tempCbucid, emailListForYellowOrRed);                     
	        }
	                 
	        Map<String, String> mapWlnWlsByCbucid = new Map<String, String>();
	        mapWlnWlsByCbucid.put('ACCTPRIME', '');
	        mapWlnWlsByCbucid.put('DIR', '');
	        mapWlnWlsByCbucid.put('WLS_SR', '');
	        mapWlnWlsByCbucid.put('WLS_MGR', '');
	        mapWlnWlsByCbucid.put('WLS_DIR', '');
	        
	        if(strColor.equals('Red')){
	        	mapWlnWlsByCbucid.put('MD', '');
	        }
	         
	        Integer index = 1;
	        Boolean flag = true;       
	        
	        for(Sales_Assignment__c a:listEmailToWlsAndWlnMgrDir) {
		        if(!tempCbucid.equals(a.Account__r.cbucid__c)) {
	            mapEmailListForYellowOrRed.remove(tempCbucid);
	            mapEmailListForYellowOrRed.put(tempCbucid, emailListForYellowOrRed);        
	            
	            if(mapEmailListForYellowOrRed.containsKey(a.Account__r.cbucid__c)) {
	            	emailListForYellowOrRed = mapEmailListForYellowOrRed.get(a.Account__r.cbucid__c);
	            } else {                          
	            	emailListForYellowOrRed = new Set<String>();                       
	            }
	            
	            mapWlnWlsByCbucid = new Map<String, String>();                        
	            mapWlnWlsByCbucid.put('ACCTPRIME', '');
	            mapWlnWlsByCbucid.put('DIR', '');
	            mapWlnWlsByCbucid.put('WLS_SR', '');
	            mapWlnWlsByCbucid.put('WLS_MGR', '');
	            mapWlnWlsByCbucid.put('WLS_DIR', '');
	            
	            if(strColor.equals('Red')){
	            	mapWlnWlsByCbucid.put('MD', '');
	            }  
	            
	            tempCbucid = a.Account__r.cbucid__c; 
		        }
		                       
		        if(mapWlnWlsByCbucid.containsKey(a.Role__c) && mapWlnWlsByCbucid.get(a.Role__c) == '' &&
					  	 a.User__r.Email != null && a.User__r.Email != ''){
					  	emailListForYellowOrRed.add(a.User__r.Email);                                                            
		        }                   
		        
		        if(listEmailToWlsAndWlnMgrDir.size() == index){
		        	mapEmailListForYellowOrRed.put(tempCbucid, emailListForYellowOrRed);
		        }
		        
		        index++;                                     
	        }                
        }   
             
        return mapEmailListForYellowOrRed;
    }
    
    /*
     *  Method for EMail List for WLN Account Prime  , WLN Director , WLS Sales Rep , WLS Director , WLS Manager 
     */ 
    public static Map<String, Set<String>> findWlsWlnUserEmailFromEmployee(
    	List<AtoZ_Assignment__c> listEmailToWlsAndWlnMgrDir,
    	Map<String,Set<String>> mapEmailListForYellowOrRed
    ){
        String tempCbucid= '';
        Set<String> emailListForYellowOrRed=new Set<String>();   
        
        if(listEmailToWlsAndWlnMgrDir.size() > 0) {
	        tempCbucid = listEmailToWlsAndWlnMgrDir[0].CBUCID__R.CBUCID__c;
	        
	        if(mapEmailListForYellowOrRed.containsKey(tempCbucid)) {
	        	emailListForYellowOrRed = mapEmailListForYellowOrRed.get(tempCbucid);
	        } else {
			      emailListForYellowOrRed = new Set<String>();
			      mapEmailListForYellowOrRed.put(tempCbucid, emailListForYellowOrRed);                       
	        }
	                        
	        Map<String, String> mapWlnWlsByCbucid = new Map<String, String>();
	        mapWlnWlsByCbucid.put('WLN_SM', '');
	        mapWlnWlsByCbucid.put('WLN_Mgr', '');
	        mapWlnWlsByCbucid.put('WLN_Dir', '');                            
	        
	        Integer index = 1;
	        Boolean flag = true;       
	        
	        for(AtoZ_Assignment__c a:listEmailToWlsAndWlnMgrDir) {
		        if(!tempCbucid.equals(a.CBUCID__R.CBUCID__c)) { 
	            mapEmailListForYellowOrRed.remove(tempCbucid);
	            mapEmailListForYellowOrRed.put(tempCbucid, emailListForYellowOrRed);       
	            
	            if(mapEmailListForYellowOrRed.containsKey(a.CBUCID__R.CBUCID__c)) {
	            	emailListForYellowOrRed = mapEmailListForYellowOrRed.get(a.CBUCID__R.CBUCID__c);
	            } else {                          
	            	emailListForYellowOrRed = new Set<String>();                       
	            }     
	            
	            mapWlnWlsByCbucid = new Map<String, String>();
	            mapWlnWlsByCbucid.put('WLN_SM', '');
	            mapWlnWlsByCbucid.put('WLN_Mgr', '');
	            mapWlnWlsByCbucid.put('WLN_Dir', '');                        
	            tempCbucid = a.CBUCID__R.CBUCID__c;                         
		        } 
		                         
            if((a.AtoZ_Field__c).equals('SvcSpec_SvcMgr')) {
	            if( a.AtoZ_Contact__r.Manager_Email__c != null && a.AtoZ_Contact__r.Manager_Email__c != '') {
	            	emailListForYellowOrRed.add(a.AtoZ_Contact__r.Manager_Email__c);                                
	            }
	            
	            if( a.AtoZ_Contact__r.Director_Email__c != null && a.AtoZ_Contact__r.Director_Email__c != '') {
	            	emailListForYellowOrRed.add(a.AtoZ_Contact__r.Director_Email__c);                                
	            }                             
            }
                                                
            if(listEmailToWlsAndWlnMgrDir.size() == index) {
            	mapEmailListForYellowOrRed.put(tempCbucid, emailListForYellowOrRed); 
            }
            
            index++;                                     
	        }                
        }        
        
        return mapEmailListForYellowOrRed;
    }
    
}