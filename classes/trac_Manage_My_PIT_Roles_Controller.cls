public with sharing class trac_Manage_My_PIT_Roles_Controller {
    public class PITRException extends Exception {}

    public Boolean loaded {get; private set;}
    public Opportunity opportunity {get; private set;}
    public OppLineRoleVO[] lines {get; set;}
    public Boolean getHasLines() {
        return (lines != null && lines.size() > 0);
    }
    public Integer linesCount {get {if(lines == null){return 0;} return lines.size();}}
    public SelectOption[] roleOptions {get; set;}


    public void onLoad() {
        loaded = false;
        try {
            Id oid = (Id) ApexPages.currentPage().getParameters().get('oid');
            
            List<Opportunity> oppTemp = [Select Name, Owner.Name From Opportunity Where Id = :oid Limit 1];
            
            if (oppTemp.size() == 0) {
            	throw new PITRException('Unable to load opportunity');
            }
            
            opportunity = oppTemp[0];

            loadLines();
            loaded = true;
            
        } catch (Exception e) {
        	QuoteUtilities.log(e); 
        	clear();
        }
    }
    
    private void loadLines() {
        Map<Id, Opp_Product_Item__c> oppProductItemMap = new Map<Id, Opp_Product_Item__c>([Select Id, Product__r.Name, BPC_1__c, BPC_2__c, BPC_3__c, Opportunity_Solution__r.Name From Opp_Product_Item__c Where Opportunity__c = :opportunity.Id]);
        if (oppProductItemMap == null || oppProductItemMap.size() < 1) { clear(); return; }

        List<Product_Sales_Team__c> listMembers = [Select Team_Role__c, Product__c, Member__c From Product_Sales_Team__c Where Product__c In :oppProductItemMap.keySet() And Member__c = :UserInfo.getUserId()];
        
        Map<Id, Product_Sales_Team__c> productToMember = new Map<Id, Product_Sales_Team__c>();
        for (Product_Sales_Team__c pstm : listMembers) {
            productToMember.put(pstm.Product__c, pstm);
        }
        lines = new List<OppLineRoleVO>();
        for (Opp_Product_Item__c opi : oppProductItemMap.values()) {
            lines.add(new OppLineRoleVO(opi, productToMember.get(opi.Id)));
        }
    }
    
    public PageReference saveRoles() {
        Savepoint sp = Database.setSavepoint();
        Boolean fatal = false;
        try {
            if (opportunity == null) {
                clear(); throw new PITRException('An error occured. Please return to the opportunity and try again.');
            }
            if (lines == null || lines.size() == 0) { clear(); loadLines(); }
            List<Product_Sales_Team__c> rolesToDelete = new List<Product_Sales_Team__c>();
            List<Product_Sales_Team__c> rolesToUpdate = new List<Product_Sales_Team__c>();
            List<Product_Sales_Team__c> rolesToInsert = new List<Product_Sales_Team__c>();
            for (OppLineRoleVO olr : lines) {
                Boolean mustHaveRole = false;
                if (olr.addThis != null && olr.addThis == true) { mustHaveRole = true; }
                if (olr.getHasRole()) { mustHaveRole = false; }
                if (mustHaveRole && (olr.pit.Team_Role__c == null || olr.pit.Team_Role__c == '')) {
                    addMTP('You must select a role for ' + olr.productName);
                    return null;
                }
                if (olr.startRole == null || olr.startRole == '') {
                    if (olr.pit.Team_Role__c != null && olr.pit.Team_Role__c != '') {
                        // New role
                        rolesToInsert.add(olr.pit);
                    } else { /* Both were blank, do nothing. */ }
                } else {
                    if (olr.pit.Team_Role__c == null || olr.pit.Team_Role__c == '') {
                        // Role was blanked
                        rolesToDelete.add(olr.pit);
                    } else {
                        // Role was changed
                        rolesToUpdate.add(olr.pit);
                    }
                }
            }
            if (rolesToDelete != null && rolesToDelete.size() > 0) {
                delete rolesToDelete;
                addMTP('Deleted ' + rolesToDelete.size() + ' roles.');
            }
            if (rolesToUpdate != null && rolesToUpdate.size() > 0) {
                update rolesToUpdate;
                addMTP('Updated ' + rolesToUpdate.size() + ' roles.');
            }
            if (rolesToInsert != null && rolesToInsert.size() > 0) {
                insert rolesToInsert;
                addMTP('Created ' + rolesToInsert.size() + ' roles.');
            }
            loadLines();
        } catch (Exception e) { fatal = true; Database.rollback(sp); if (e.getMessage().contains('INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY')) { addETP('You don\'t have permission to join this opportunity. Please contact the opportunity owner for permission.'); } else {QuoteUtilities.log(e);} }
        return (fatal) ? null : backToOpp();
    }
    
    public PageReference backToOpp() {
        if (opportunity == null) { return null; }
        PageReference pr = new PageReference('/' + opportunity.Id);
        pr.setRedirect(true);
        return pr;
    }
    
    private void clear() {
        opportunity = null;
        lines = null;
    }
    
    private void addETP(String e) {
        ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, e));
    }
    
    private void addMTP(String m) {
        ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Info, m));
    }
    
    
    public class OppLineRoleVO {
        public Boolean addThis {get; set;}
        public String productName {get; private set;}
        public String bpcOne {get; private set;}
        public String bpcTwo {get; private set;}
        public String bpcThree {get; private set;}
        
        public Opp_Product_Item__c opi {get; set;}
        
        public Product_Sales_Team__c pit {get; set;}
        public String startRole {get; set;}
        
        public String solution {get; private set;}
        
        public boolean getHasRole() {
            return (startRole != null && startRole != '' );
        }
        
        public OppLineRoleVO(Opp_Product_Item__c opix, Product_Sales_Team__c pitx) {
            if (opix == null) { throw new PITRException('Opp product item cannot be null'); }
            productName = opix.Product__r.Name;
            bpcOne = opix.BPC_1__c;
            bpcTwo = opix.BPC_2__c;
            bpcThree = opix.BPC_3__c;
            opi = opix;
            pit = pitx;
            
            if (getHasRole()) {
                addThis = true;
            }
            
            if (pit == null) {
                pit = new Product_Sales_Team__c(Member__c = UserInfo.getUserId(), Product__c = opix.Id);
            } else {
                startRole = pitx.Team_Role__c;
            }
            
            solution = opix.Opportunity_Solution__r.Name;
        }
    }

}