/**
 * Helper class that calculates quotes.
 * 
 * @author Max Rudman
 * @since 9/4/2010
 */
public class QuoteCalculator {
	private List<QuoteVO> quotes;
	
	public List<SObject> changedLines {get; private set;}
	public List<SBQQ__QuoteLineGroup__c> changedGroups {get; private set;} 
	
	/**
	 * Default constructor.
	 */
	public QuoteCalculator(List<QuoteVO> quotes) {
		this.quotes = quotes;
		changedLines = new List<SObject>();
		changedGroups = new List<SBQQ__QuoteLineGroup__c>();
	}
	
	public QuoteCalculator(QuoteVO quote) {
		this(new List<QuoteVO>{quote});
	}	
	

	public void calculate() {
		for (QuoteVO quote : quotes) {
			calculate(quote);
		}
	}
	
	public void calculate(QuoteVO quote) {
		// Calculate quantities (components, batches, etc.)
		calculateQuantities(quote);
		
		evaluatePriceRules(quote);
		
		// Reset totals
		quote.listProductTotal = 0;
		quote.customerProductTotal = 0;
		quote.netProductTotal = 0;
		quote.regularProductTotal = 0;
		quote.netTotal = 0;
		for (QuoteVO.LineItemGroup itemGroup : quote.lineItemGroups) {
			itemGroup.listProductTotal = 0;
			itemGroup.customerProductTotal = 0;
			itemGroup.netProductTotal = 0;
			itemGroup.regularProductTotal = 0;
			itemGroup.lineGroup.SBQQ__ListTotal__c = 0;
			itemGroup.lineGroup.SBQQ__CustomerTotal__c = 0;
			itemGroup.lineGroup.SBQQ__NetTotal__c = 0;
			if (itemGroup.isExisting()) {
				changedGroups.add(itemGroup.lineGroup);
			}
		}
		
		// Calculate non-dynamic subscription line items
		List<QuoteVO.LineItem> dynamicSubItems = new List<QuoteVO.LineItem>();
		for (QuoteVO.LineItem item : quote.lineItems) {
			if (item.isExisting()) {
				changedLines.add(item.line);
			}
			if (!item.isDynamicSubscription()) {
				LineCalculator lcalc = new LineCalculator(this, item);
				lcalc.calculate();
				if (!item.isOptional()) {
					if (!item.isExcludedFromMaintenance()) {
						quote.listProductTotal += item.listProductTotal;
						quote.customerProductTotal += item.customerProductTotal;
						quote.netProductTotal += item.netProductTotal;
						quote.regularProductTotal += item.regularProductTotal;
					}
					quote.netTotal += item.netTotal;
				}
				if (item.parentGroup != null) {
					if (!item.isOptional() || (item.parentGroup.lineGroup.SBQQ__Optional__c == true)) {
						if (!item.isExcludedFromMaintenance()) {
							item.parentGroup.listProductTotal += item.listProductTotal;
							item.parentGroup.customerProductTotal += item.customerProductTotal;
							item.parentGroup.netProductTotal += item.netProductTotal;
							item.parentGroup.regularProductTotal += item.regularProductTotal;
						}
						item.parentGroup.lineGroup.SBQQ__ListTotal__c += item.listTotal;
						item.parentGroup.lineGroup.SBQQ__CustomerTotal__c += item.customerTotal;
						item.parentGroup.lineGroup.SBQQ__NetTotal__c += item.netTotal;
					}
				}
			} else {
				dynamicSubItems.add(item);
			}
		}
		
		rollupComponents(quote);
	}
	
    private Decimal calculateQuantity(QuoteVO.LineItem item) {
    	Decimal originalQty = item.bundledQuantity;
    	if (originalQty != null) {
    		Decimal parentQty = ((item.parentItem != null) && !item.isDynamicSubscription()) ? calculateQuantity(item.parentItem) : 1;
    		Decimal qty = originalQty;
    		return QuoteUtils.multiply(qty, parentQty);
    	}
    	return item.quantity;
    }
	
	private void evaluatePriceRules(QuoteVO quote) {
		if (quote.quoteId != null) {
			// Quote ID is required for PriceRuleEvaluator.
			// Should only be null in some unit tests
			List<SObject> lines = new List<SObject>();
			for (QuoteVO.LineItem item : quote.lineItems) {
				lines.add(item.line);
			}
			
			//QuoteLinePriceRuleEvaluator evaluator = new QuoteLinePriceRuleEvaluator(quote.quoteId, lines);
			//evaluator.evaluate();
		}
	}
	
	private void rollupComponents(QuoteVO quote) {
		// Reset component totals
		for (QuoteVO.LineItem item : quote.lineItems) {
			item.componentTotal = (item.isPackage()) ? 0 : null;
			item.componentListTotal = (item.isPackage()) ? 0 : null;
			item.componentCost = (item.isPackage()) ? 0 : null;
		}
		
		for (QuoteVO.LineItem item : quote.lineItems) {
			if (item.isPackage()) {
				rollupComponents(quote, item.line);
			}
		}
	}
	
	private void rollupComponents(QuoteVO quote, SObject line) {
		Decimal netTotal = 0;
		Decimal listTotal = 0;
		Decimal cost = 0;
		
		for (QuoteVO.LineItem item : quote.lineItems) {
			if (item.requiredBy == line) {
				if (!item.isOptional() || (item.parentGroup != null) && (item.parentGroup.lineGroup.SBQQ__Optional__c)) {
					netTotal += item.netTotal;
					listTotal += item.listTotal;
					//cost += QuoteUtils.multiplyAsZero(item.quantity, item.unitCost);
				}
				if (item.isPackage()) {
					rollupComponents(quote, item.line);
					netTotal += item.componentTotal;
					listTotal += item.componentListTotal;
					//cost +=  item.componentCost;
				}
			}
		}
		
		AliasedMetaDataUtils.setFieldValue(line, SBQQ__QuoteLine__c.SBQQ__ComponentTotal__c, netTotal);
		AliasedMetaDataUtils.setFieldValue(line, SBQQ__QuoteLine__c.SBQQ__ComponentListTotal__c, listTotal);
		AliasedMetaDataUtils.setFieldValue(line, SBQQ__QuoteLine__c.SBQQ__ComponentCost__c, cost);
    }
	
	private void calculateQuantities(QuoteVO qvo) {
    	for (QuoteVO.LineItem item : qvo.lineItems) {
    		item.quantity = calculateQuantity(item);
			if (item.quantity != null) {
				item.quantity = item.quantity.setScale(0);
			}
    	}
    }
    
    private SObject[] getAllLineRecords() {
    	SObject[] lines = new SObject[0];
    	for (QuoteVO quote : quotes) {
    		lines.addAll(quote.getLineRecords());
    	}
    	return lines;
    }
    
	private class LineCalculator {
		private QuoteCalculator parent;
        private QuoteVO.LineItem item;
        private Decimal proratedRenewalPrice;
        
        public LineCalculator(QuoteCalculator parent, 
        		QuoteVO.LineItem item) {
        	this.parent = parent;
            this.item = item;
            item.listProductTotal = 0;
            item.customerProductTotal = 0;
            item.netProductTotal = 0;
            item.regularProductTotal = 0;
        }
        
        public void calculate() {
        	// Compute Renewal flag; only line items in Renewal quote, that are not subscription and reference renewed asset are "renewal"
            item.renewal = item.parentQuote.isRenewal() && !item.isSubscription() && (item.renewedAssetId != null);
            
            calculateSpecialPrice();
            applyProration();
            applyCustomerAndVolumeDiscount();
            
            item.customerTotal = 0;
            item.netTotal = 0;
            item.listTotal = 0;
            item.regularTotal = 0;
            item.grossProfitAmount = null;
            if (!item.isRenewal() && !item.isExistingProduct()) {
            	if (item.isPricingMethodBlock()) {
            		item.customerTotal = QuoteUtils.multiplyAsZero(1, item.customerPrice).setScale(2, System.RoundingMode.HALF_UP);
            		item.netTotal = QuoteUtils.multiplyAsZero(1, item.netPrice).setScale(2, System.RoundingMode.HALF_UP);
            		item.regularTotal = QuoteUtils.multiplyAsZero(1, item.regularPrice).setScale(2, System.RoundingMode.HALF_UP);
            		item.listTotal = QuoteUtils.multiplyAsZero(1, item.listPrice).setScale(2, System.RoundingMode.HALF_UP);
            	} else {
            		item.customerTotal = QuoteUtils.multiplyAsZero(item.quantity, item.customerPrice).setScale(2, System.RoundingMode.HALF_UP);
            		item.netTotal = QuoteUtils.multiplyAsZero(item.quantity, item.netPrice).setScale(2, System.RoundingMode.HALF_UP);
            		item.regularTotal = QuoteUtils.multiplyAsZero(item.quantity, item.regularPrice).setScale(2, System.RoundingMode.HALF_UP);
            		item.listTotal = QuoteUtils.multiplyAsZero(item.quantity, item.listPrice).setScale(2, System.RoundingMode.HALF_UP);
            	}
            }
            if (!item.isRenewal() && !item.isBundled() && !item.isExistingProduct()) {
            	item.listTotal = (item.isPricingMethodBlock()) ? QuoteUtils.multiplyAsZero(1, item.listPrice) : QuoteUtils.multiplyAsZero(item.quantity, item.listPrice);
            }
            if ((!item.isSubscription() || item.isIncludedInMaintenance()) && !item.isBundled() && !item.isExistingProduct()) {
            	item.listProductTotal = item.isPricingMethodBlock() ? ((item.listPrice != null) ? item.listPrice : 0) : QuoteUtils.multiplyAsZero(item.quantity, item.listPrice);
            	item.customerProductTotal = item.isPricingMethodBlock() ? ((item.customerPrice != null) ? item.customerPrice : 0) : QuoteUtils.multiplyAsZero(item.quantity, item.customerPrice);
            	item.netProductTotal = item.isPricingMethodBlock() ? ((item.netPrice != null) ? item.netPrice : 0) : QuoteUtils.multiplyAsZero(item.quantity, item.netPrice);
            	item.regularProductTotal = item.isPricingMethodBlock() ? ((item.regularPrice != null) ? item.regularPrice :0) : QuoteUtils.multiplyAsZero(item.quantity, item.regularPrice);
            }
        }
        
        /**
         * Applies volume discount set on this line.
         */
        private void calculateSpecialPrice() {
        	if (item.isBundled()) {
        		item.specialPrice = 0;
        		return;
        	}
            
            
        	if ((item.contractedPriceId != null) || item.isSpecialPriceRenewal()) {
                // When Contracted Price is not blank Special Price is calculated on add
                // When Special Price Type is Renewal the Special Price is set on add
                // In neither case should it be overwritten.
                return;
            }
        	
            // Default Special Price to List Price.
            item.specialPrice = item.listPrice;
            
            // Apply Option Discount on top, if any
            if (item.specialPrice != null) {
            	if (item.optionDiscountRate != null) {
            		item.specialPrice = item.specialPrice * (1 - (item.optionDiscountRate / 100));
            	} else if (item.optionDiscountAmount != null) {
            		item.specialPrice = item.specialPrice - item.optionDiscountAmount;
            	}
            }
            
        }
        
        private void applyCustomerAndVolumeDiscount() {
        	// Use prorated price as the base
        	Decimal price = item.proratedPrice;
        	item.regularPrice = price;
        	
        	if (item.isPricingMethodCustom()) {
        		// With Custom pricing method customer price is entered directly
        		return;
        	}
        	
        	item.regularPrice = price;
        	
        	if ((item.renewalPrice != null) && !item.isDynamicSubscription()) {
            	item.additionalDiscountAmount = QuoteUtils.subtract(item.regularPrice, proratedRenewalPrice);
            }
        	
            Decimal discount = 0;
            if (item.isDiscountable()) {
            	if (item.additionalDiscountAmount != null) {
                    discount = item.additionalDiscountAmount;
                } else {
                    Decimal rate = item.additionalDiscountRate;
                    discount = (rate == null) ? 0 : QuoteUtils.multiply(price, (rate.doubleValue() / 100.0));
                }
            }
            item.customerPrice = QuoteUtils.subtract(price, discount);
            if (item.customerPrice != null) {
            	item.customerPrice = item.customerPrice.setScale(2, System.RoundingMode.HALF_UP);
            }
            item.netPrice = item.customerPrice;
        }
        
        
        /**
         * Applies proration to this line item.
         */
        private void applyProration() {
            item.prorateMultiplier = 1;
            
            item.proratedPrice = QuoteUtils.multiply(item.specialPrice, item.prorateMultiplier);
            if (item.proratedPrice != null) {
            	item.proratedPrice = item.proratedPrice.setScale(2, System.RoundingMode.HALF_UP);
            }
            item.proratedListPrice = QuoteUtils.multiply(item.listPrice, item.prorateMultiplier);
            if (item.proratedListPrice != null) {
            	item.proratedListPrice = item.proratedListPrice.setScale(2, System.RoundingMode.HALF_UP);
            }
            if (item.renewalPrice != null) {
            	proratedRenewalPrice = item.renewalPrice * item.prorateMultiplier;
            	proratedRenewalPrice = proratedRenewalPrice.setScale(2, System.RoundingMode.HALF_UP);
            }
        }
    }
    
    public class CalculatorException extends Exception {}
}