public class voc_processRecordsUtil implements queueable{
    list<voc_survey__c> surveyHasManagerIdsOrEmails{get;set;}
    public voc_processRecordsUtil(list<voc_survey__c> surveyHasManagerIdsOrEmails){
        this.surveyHasManagerIdsOrEmails=surveyHasManagerIdsOrEmails;
    }
    public void runStandalone(){
        List<Task> externalTasks = new List<Task>();
        List<Task> internalTasks = new List<Task>();
        List<VOC_Task_for_Email_Alert__c> vocTasks = new List<VOC_Task_for_Email_Alert__c>();
        Set<Id> taskIds = new Set<Id>();
        Schema.RecordTypeInfo recordTypeInfo=task.sobjecttype.getdescribe().getRecordTypeInfosByName().get('VOC Manager Callback Task');
        if(recordTypeInfo!=null){
            id taskRecordTypeId=recordTypeInfo.getRecordTypeId();
            list<string> managerEmails=new list<string>();
            list<string> managerIds=new list<string>();
            if(!surveyHasManagerIdsOrEmails.isempty()){
                for (voc_survey__c voc:surveyHasManagerIdsOrEmails) {
                    if(voc.Manager_IDs__c!=null) managerIds = voc.Manager_IDs__c.split(',', -2);
                    if(voc.manager_emails__c!=null) managerEmails = voc.manager_emails__c.split(',', -2);
                    for(String managerId : managerIds){
                        internalTasks.add(new Task(
                            ActivityDate = Date.today().addDays(5),
                            Subject = 'VOC Manager Callback Request',
                            Description = 'Customer Comments:'+voc.Customer_Callback_Comments__c,
                            WhoId = voc.Contact_ID__c,
                            WhatId = voc.Account__c,
                            RecordTypeId = taskRecordTypeId,
                            OwnerId = managerId,
                            VOC_Survey_ID__c = voc.id,                                    
                            VOC_Survey__c = voc.Custom_ID__c,
                            Status='Not Started'));
                        vocTasks.add(new VOC_Task_for_Email_Alert__c(
                            Agent_ID__c = voc.Agent_ID__c,
                            Business_Name__c = voc.Business_Name__c,
                            Case_Owner__c = voc.Case_Owner__c,
                            Case_Subject__c = voc.Case_Subject__c,
                            Customer_Callback_Comments__c = voc.Customer_Callback_Comments__c,
                            Customer_Callback_Email__c = voc.Customer_Callback_Email__c,
                            Customer_Callback_First_Name__c = voc.Customer_Callback_First_Name__c,
                            Customer_Callback_Last_Name__c = voc.Customer_Callback_Last_Name__c,
                            Customer_Callback_Phone_Number__c = voc.Customer_Callback_Phone_Number__c,
                            Customer_Email__c = voc.Customer_Email__c,
                            Customer_First_Name__c = voc.Customer_First_Name__c,
                            Customer_Last_Name__c = voc.Customer_Last_Name__c,
                            Customer_Phone__c = voc.Customer_Phone__c,
                            Custom_ID__c = voc.Custom_ID__c,
                            Interaction_Date__c = voc.Interaction_Date__c,
                            Location__c = voc.Location__c,
                            Manager_ID__c = managerId,
                            Preferred_Time_for_Contact__c = voc.Preferred_Time_for_Contact__c,
                            Reference__c = voc.Reference__c,
                            Ticket__c = voc.Ticket__c));
                    }
                    if(managerIds.size()==0){
                        for(String email : managerEmails){
                            externalTasks.add(new Task(
                                External_Assignee__c = email,
                                ActivityDate = Date.today().addDays(5),
                                Subject = 'VOC Manager Callback Request',
                                Description = 'Customer Comments:'+voc.Customer_Callback_Comments__c,
                                WhoId = voc.Contact_ID__c,
                                WhatId = voc.Account__c,
                                RecordTypeId = taskRecordTypeId,
                                OwnerId = voc.OwnerId,
                                VOC_Survey_ID__c = voc.id,
                                VOC_Survey__c = voc.Custom_ID__c,
                                Status='Not Started'));
                            vocTasks.add(new VOC_Task_for_Email_Alert__c(
                                Agent_ID__c = voc.Agent_ID__c,
                                Business_Name__c = voc.Business_Name__c,
                                Case_Owner__c = voc.Case_Owner__c,
                                Case_Subject__c = voc.Case_Subject__c,
                                Customer_Callback_Comments__c = voc.Customer_Callback_Comments__c,
                                Customer_Callback_Email__c = voc.Customer_Callback_Email__c,
                                Customer_Callback_First_Name__c = voc.Customer_Callback_First_Name__c,
                                Customer_Callback_Last_Name__c = voc.Customer_Callback_Last_Name__c,
                                Customer_Callback_Phone_Number__c = voc.Customer_Callback_Phone_Number__c,
                                Customer_Email__c = voc.Customer_Email__c,
                                Customer_First_Name__c = voc.Customer_First_Name__c,
                                Customer_Last_Name__c = voc.Customer_Last_Name__c,
                                Customer_Phone__c = voc.Customer_Phone__c,
                                Custom_ID__c = voc.Custom_ID__c,
                                Interaction_Date__c = voc.Interaction_Date__c,
                                Location__c = voc.Location__c,
                                Manager_Email__c = email,
                                Preferred_Time_for_Contact__c = voc.Preferred_Time_for_Contact__c,
                                Reference__c = voc.Reference__c,
                                Ticket__c = voc.Ticket__c));
                        }
                    }
                }
                insert internalTasks;
                insert externalTasks;
                
                ExternalUserPortal__c eups = ExternalUserPortal__c.getInstance();
                if(!Test.isRunningTest()) {
                    System.assert(eups != null, 'Failed to load critical portal settings');
                    System.assert(eups.HMAC_Digest_Key__c != null, 'Failed to load critical digest settings');
                }
                
                for(Task insertedTask : internalTasks){
                    system.debug('internalTask task id:'+insertedTask.id);
                    system.debug('internalTask task token:'+insertedTask.Token__c);
                    for(VOC_Task_for_Email_Alert__c vocT : vocTasks){
                        if(vocT.Custom_ID__c == insertedTask.VOC_Survey__c){
                            vocT.Task_ID__c = insertedTask.id;
                            vocT.External_Link__c = eups.Base_URL__c + '/' + insertedTask.Id;
                        }
                    }
                }
                
                for(Task insertedTask : externalTasks){
                    taskIds.add(insertedTask.Id);
                }
                externalTasks = [SELECT Id, VOC_Survey__c, Token__c FROM Task WHERE Id IN :taskIds];
                
                for(Task insertedTask : externalTasks){
                    system.debug('externalTask task id:'+insertedTask.id);
                    system.debug('externalTask task token:'+insertedTask.Token__c);
                    for(VOC_Task_for_Email_Alert__c vocT : vocTasks){
                        if(vocT.Custom_ID__c == insertedTask.VOC_Survey__c){
                            vocT.Task_ID__c = insertedTask.id;
                            vocT.Token__c = insertedTask.Token__c; //only external needs token
                            vocT.External_Link__c = eups.Portal_URL__c + '/ExternalServicePortalTaskInformation?oid=' + insertedTask.Id + '&tok=' + EncodingUtil.urlEncode(insertedTask.Token__c, 'UTF-8');
                        }
                    }
                }
                
                insert vocTasks;
            }
        }
    }
    public void execute(queueablecontext qc){
        runStandalone();
    }
}