public with sharing class AccountDetailBriefController {
    
    public String acctId {get; set;}
    
   
    public Account acct {
        get{
            
            if (acctId != null) {
                acct = [select id
                        ,Account_Close_Day__c
                        ,BillingCity
                        ,BillingCountry
                        ,BillingPostalCode
                        ,BillingState
                        ,BillingStreet
                        ,Chronic__c
                        ,Churn_Threat_Flag__c
                        ,Description
                        ,Industry
                        ,Legacy_Client__c
                        ,Operating_Name_DBA__c
                        ,Paternal_RCID_Support_PIN__c
                        ,Phone
                        ,Strategic__c
                        ,Wireline_PIN__c
                        from Account
                        where id = :acctId
                       ];
            }
            return acct;
        }
        
        set;
    }
}