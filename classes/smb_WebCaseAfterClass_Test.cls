/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData = true)
public class smb_WebCaseAfterClass_Test {
	static testMethod void Test_smb_WebCaseAfterClass() {
		smb_test_utility.createCustomSettingData();
        Account accObj = smb_test_utility.createAccount('', false);
        accObj.RecordTypeId = smb_test_utility.getRecordTypeId('RCID', 'Account');
        accObj.RCID__c = 'Test RCId';
        insert accObj;
        
        Contact contObj = smb_test_utility.createContact('', accObj.Id, false);
        contObj.Active__c = true;
        contObj.email = 'a@a.com';
        contObj.Phone = '1234[^0-9]56789';
        insert contObj;
        
        Contact contObj1 = smb_test_utility.createContact('', accObj.Id, false);
        contObj1.Active__c = true;
        contObj1.email = 'a@a.com';
        contObj1.Phone = '1234[^0-9]56789';
        insert contObj1;
        
        Opportunity testOpp = smb_test_utility.createOpportunity('SMB_Care_Opportunity', accObj.Id, contObj.Id, true);
		Case caseObj = new Case(Opportunity__c = testOpp.Id, SMB_Case_Service_Add_Key__c = 'test key', Origin = 'L&R Web Form', 
								Web_form_RCID__c = 'Test Id', Description = 'Test <br> Desc');
		
		caseObj.RecordTypeId = smb_test_utility.getRecordTypeId('SMB_Care_L_R_Web_Form', 'Case');
		insert caseObj;
		
		smb_WebCaseAfterClass.chooseContact(new List<Case>{caseObj});
		smb_WebCaseAfterClass.runAssignmentRule(new List<Case>{caseObj});
		smb_WebCaseAfterClass.buildContactsWithEmail(new Set<String>{contObj.email});
		smb_WebCaseAfterClass.buildAccountsWithContacts(new Set<String>{accObj.RCID__c});
		//smb_WebCaseAfterClass.findBestMatchContact(new List<Contact>{contObj,contObj1}, contObj.Name, contObj.Phone, false);
		smb_WebCaseAfterClass.findBestMatchContact(new List<Contact>{contObj,contObj1}, 'Test Name', '1234[^0-9]56789', false);
	}
}