@isTest
public class P3MSIntegrationTest {
    
    @isTest(seeAllData=true)
    static void testIntegration() {
        
        Id PRICEBOOKID = [SELECT Id FROM Pricebook2 WHERE isActive = true and isstandard = true LIMIT 1].id;
        
        Product2 p = new Product2(name = 'Test Product', productcode = 'TESTCODE', isActive = true);
        insert p;

        PricebookEntry pbe = new PricebookEntry(product2id = p.id, unitprice = 1, usestandardprice = false, isactive = true, pricebook2id = PRICEBOOKID);
        insert pbe;
        
        P3MS_Master_Product__c pmp = new P3MS_Master_Product__c(
            Base_Price_Amt__c = 3,
            Effective_Start_Ts__c = Date.today(),
            Eol_Date__c = Date.today().addDays(3),
            Product_Cd__c = 'TESTCODE',
            Product_Desc__c = 'TESTDESC',
            Product_Nm__c = 'Test Product',
            Term_Credit_12__c = 0.50,
            Term_Credit_24__c = 1.00,
            Term_Credit_36__c = 2.00
        );
        
        insert pmp;
        
        P3MS_Master_Product__c pmpx = new P3MS_Master_Product__c(
            Base_Price_Amt__c = 3,
            Effective_Start_Ts__c = Date.today(),
            Eol_Date__c = Date.today().addDays(3),
            Product_Cd__c = 'TESTC',
            Product_Desc__c = 'TESTDESC',
            Product_Nm__c = 'Test Product',
            Term_Credit_12__c = 0.50,
            Term_Credit_24__c = 1.00,
            Term_Credit_36__c = 2.00
        );
        
        insert pmpx;
        
        
        
        System.assertEquals([Select Name From Product2 Where Id = :p.id].name, pmp.Product_Nm__c);
    }
}