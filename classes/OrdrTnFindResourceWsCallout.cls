/*
 *  @author Santosh Rath
 * This implementation class ... (used for Wireline real TN as well as Toll Free Number)
  * TOCP is the intermediate integration layer between the system (northbound) and the ASF and SMS systems (southbound) for BSE Telephone Number Reservation (TNR) functionality.
 * Telephone Number Reservation Functionality
 * TOCP TNR interfaces support the following Wireline products: Single Line (SL), Multi Line (ML), Dark High Speed and Toll Free (TF)
 * 
 * For SL, ML and Dark High Speed, the TOCP Contract is mapped to the following ASF API
 * FindResource
 * - Get native NPANXX by COID and location from FMS
 * - Request spare real wireline telephone number(s) from FMS (numerical vanity search supported)
 * 
 * For Toll Free (TF), the TOCP Contract is mapped to the following SMS API
 * FindResource 
 * - Request toll free number list from SMS (alphanumeric vanity search supported)
 * 
 *
 */
public class OrdrTnFindResourceWsCallout { 

 public static OrdrTnResourceInfoRetrievalConfiguration.ResourceConfigurationCollectionMessage findResource(TpCommonMessage.LookupRequestMessage parameters,Boolean isTollFree) {
       System.debug('Test   '+parameters.sourceCriteria);
                   
        OrdrTnResourceInfoRetrievalConfiguration.ResourceConfigurationCollectionMessage respConfigurationCollectionMessage;
        TpCommonBaseV3.EntityWithSpecification[] sourceCriteria=parameters.sourceCriteria;
        TpCommonBaseV3.EntityWithSpecification[] targetCriteria=parameters.targetCriteria;
        try {
                System.debug('The name of the TNR WebService called is findResource');
                 OrdrTnResourceInfoRetrievalFindResource.FindResourceSOAPBinding binding = new OrdrTnResourceInfoRetrievalFindResource.FindResourceSOAPBinding();
                binding = prepareCallout(binding,isTollFree); 
                if(!OrdrUtilities.useAuditFlag){
                    respConfigurationCollectionMessage = binding.findResource(sourceCriteria, targetCriteria);
                }
                System.debug('FindResource Callout response is printed below');
                //JSON.serializePretty(respConfigurationCollectionMessage);
                System.debug(respConfigurationCollectionMessage);
                System.debug('FindResource Callout response Ended');
            
        } 
        catch (CalloutException ce) {
            System.debug(ce.getStackTraceString());
            System.debug('CalloutException occurred while invoking binding.findResource' +ce);
            System.debug(ce.getmessage());
            String msg=ce.getmessage();
            if(String.isNotBlank(msg) && (msg.indexOf(OrdrConstants.SDF_INTERNAL_SERVER_ERR)!=-1)){
                msg=msg+OrdrConstants.SDF_SUPPORT_MSG;
            }
            OrdrUtilities.auditLogs('OrdrTnFindResourceWsCallout.findResource','TOCP','findResource',null,null,true,false,msg,ce.getStackTraceString());
            OrdrServicesException ose=new OrdrServicesException(ce.getmessage(),ce);
            throw ose;
        } catch (Exception e) {
            System.debug(e.getStackTraceString());
            System.debug('Exception occurred while invoking binding.findResource' +e);
            System.debug(e.getmessage());
            String msg=e.getmessage();
            if(String.isNotBlank(msg) && (msg.indexOf(OrdrConstants.SDF_INTERNAL_SERVER_ERR)!=-1)){
                msg=msg+OrdrConstants.SDF_SUPPORT_MSG;
            }
            OrdrUtilities.auditLogs('OrdrTnFindResourceWsCallout.findResource','TOCP','findResource',null,null,false,true,msg,e.getStackTraceString());
            OrdrServicesException ose=new OrdrServicesException(e.getmessage(),e);
            throw ose;
        }
        return respConfigurationCollectionMessage;
    }
    private static OrdrTnResourceInfoRetrievalFindResource.FindResourceSOAPBinding prepareCallout(OrdrTnResourceInfoRetrievalFindResource.FindResourceSOAPBinding binding,Boolean isTollFree) {
        Ordering_WS__c findResourceWirelineEndpoint = Ordering_WS__c.getValues('FindResourceWirelineEndpoint');  //ReserveResourceTollfreeEndpoint
        if(isTollFree){
            findResourceWirelineEndpoint = Ordering_WS__c.getValues('FindResourceTollfreeRespOrg');
        }
        
        binding.endpoint_x = findResourceWirelineEndpoint.value__c;
        System.debug('FindResourceWirelineEndpoint: '+ findResourceWirelineEndpoint.value__c.length());
        System.debug('FindResourceWirelineEndpoint: '+ findResourceWirelineEndpoint.value__c.trim().length());
        binding.timeout_x = OrdrConstants.WS_TIMEOUT_MS;
        //binding.timeout_x = 60000;
         if (binding.endpoint_x.startsWith(OrdrConstants.ENDPOINT_XML_GATEWAY_SYMBOL)) {
                Ordering_WS__c wsUserName = Ordering_WS__c.getValues(OrdrConstants.CREDENTIAL_USERNAME);
                Ordering_WS__c wsPassword =  Ordering_WS__c.getValues(OrdrConstants.CREDENTIAL_PASSWORD);  
                if (String.isNotBlank(wsUserName.value__c) && String.isNotBlank(wsPassword.value__c)) {
                        String credentials = 'APP_CPQ' + ':' + wsPAssword.value__c;
                        String encodedUserNameAndPassword = EncodingUtil.base64Encode(Blob.valueOf(credentials));
                        binding.inputHttpHeaders_x = new Map<String, String>();
                        binding.inputHttpHeaders_x.put(OrdrConstants.AUTHORIZATION, OrdrConstants.BASIC + encodedUserNameAndPassword);
                    }   
                }
          else {
                System.debug('SDF Certificate name : '+ OrdrConstants.SDF_CERT_NAME);
                binding.clientCertName_x = OrdrConstants.SDF_CERT_NAME;
            }
        return binding;
    } 

    /*
     * Used for Real TN and Toll Free number reservation using the TOCP FindResource Business Contract
 * For SL, ML and Dark High Speed, the TOCP Contract is mapped to the following ASF API
 * FindResource
 * - Get native NPANXX by COID and location from FMS
*
     * @param  
     * @return 
     * 
     */     
   
    public static List<OrdrTnNpaNxx> findResourceForNpaNxxList(OrdrTnServiceAddress serviceAddress, String userId , OrdrTnReservationType reservationType) {
        
        OrdrTnResourceInfoRetrievalConfiguration.ResourceConfigurationCollectionMessage tocpResp = null;
        //Object tocpResp = null;
        System.debug('-- FindResourceImpl.findResourceForNpaNxxList - processing starts at '+System.now() );

       TpCommonMessage.LookupRequestMessage parameters = null;
       // Object parameters = null;
        List<OrdrTnNpaNxx> resultingList = new List<OrdrTnNpaNxx>();

        if (reservationType != null) {
            if (reservationType.equals(OrdrTnReservationType.WLN)) {
                //Preparing the WS call to TOCP to retrieve from FMS through ASF the list of native OrdrTnNpaNxxlist associated with the SA's COID
                parameters = generateTocpLookupRequestMessageForWirelineDarkDslAndRealTnSoapRequest(serviceAddress, null, null, false, userId, false);
            }
            else if (reservationType.equals(OrdrTnReservationType.TOLLFREE)) {
                //This functionality is not supported/required by SMS - therefore, no outbound WS call needed
                //parameters = generateTocpLookupRequestMessageForWirelineDarkDslAndRealTnSoapRequest(serviceAddress, null, null, false, userId, false);
            }
            
            if (parameters != null) {
                System.debug( parameters);
                
                // Invoke the TOCP - FindResource web service
                try {                   
                    tocpResp = findResource(parameters,reservationType.equals(OrdrTnReservationType.TOLLFREE));
                } catch (Exception e) {
                    System.debug('Exception occurred while executing findResource(parameters)'+e);
                    throw e;
                }

                // Process the TOCP response
                if (tocpResp != null) {
                        List<TpInventoryResourceConfigV3.LogicalResource> listLRes = tocpResp.logicalResource;
                        System.debug('Findresponse size is '+listLRes.size());
                                    if ((listLRes != null) && (listLRes.size() > 0)) {
                                        for (integer i=0; i<listLRes.size(); i++) {
                                            if ((listLRes.get(i) != null) && (listLRes.get(i).CharacteristicValue != null)) {
                                                TpInventoryResourceConfigV3.LogicalResource logicalResponse=listLRes.get(i);
                                                if(logicalResponse!=null){

                                                List<TpInventoryResourceConfigV3.LogicalResource> componentLogicalResourceList=logicalResponse.ComponentLogicalResource;
                                                    if(componentLogicalResourceList!=null){
                                                                for(TpInventoryResourceConfigV3.LogicalResource componentLogicalResource:componentLogicalResourceList){
                                                
                                                Map <String, String> hm = convertTocpV2CharacteristicValuesToMap (componentLogicalResource.CharacteristicValue);
                                                String statusCode = null;
                                                String npa = null;
                                                String nxx = null;
                                                String switchNumber = null;
                                                if (hm.containsKey('statusCode')) {
                                                    statusCode = hm.get('statusCode');
                                                }   
                                                if (hm.containsKey('npa')) {
                                                    npa = hm.get('npa');
                                                }   
                                                if (hm.containsKey('nxx')) {
                                                    nxx = hm.get('nxx');
                                                }   
                                                if (hm.containsKey('switchNumber')) {
                                                    switchNumber = hm.get('switchNumber');
                                                }
                                                if ((statusCode != null) && (statusCode.equals('A'))) {
                                                    if(String.isNotBlank(serviceAddress.getServingCOID())){
                                                        resultingList.add(new OrdrTnNpaNxx(npa, nxx, serviceAddress.getServingCOID(), switchNumber,false,false));
                                                    } else {
                                                        resultingList.add(new OrdrTnNpaNxx(npa, nxx, serviceAddress.getCOID(), switchNumber,false,true));
                                                    }
                                                    
                                                }
                                                else
                                                {
                                                    System.debug('Filtering the following OrdrTnNpaNxx'+npa+'-'+nxx+' in FindResourceImpl.findResourceForNpaNxxList as its status is '+statusCode);
                                                }
                                                }
                                                    }
                                        
                                                }

                                            }
                                        }
                                    }
                                }
            }
        }
        return resultingList;
    }
 
    /**
     * Used for Real TN and Toll Free number reservation using the TOCP FindResource Business Contract
     *  * For SL, ML and Dark High Speed, the TOCP Contract is mapped to the following ASF API
 * FindResource
 * - Request spare real wireline telephone number(s) from FMS (numerical vanity search supported)
 * 
 * For Toll Free (TF), the TOCP Contract is mapped to the following SMS API
 * FindResource 
 * - Request toll free number list from SMS (alphanumeric vanity search supported)
     * @param  
     * @return 
     *  
     */     
    
    public static List<OrdrTnLineNumber> findResourceForSpareTnList(OrdrTnServiceAddress serviceAddress, OrdrTnNpaNxx selectedNpaNxx, String lineNumberPrefix, Boolean isSearchForExactMatch, String userId, OrdrTnReservationType reservationType) {
        OrdrTnResourceInfoRetrievalConfiguration.ResourceConfigurationCollectionMessage tocpResp = null;
        System.debug('-- FindResourceImpl.findResourceForSpareTnList - processing starts at '+System.now() );

        TpCommonMessage.LookupRequestMessage parameters = null;
        //Object parameters = null;
        List<OrdrTnLineNumber> resultingList = new List<OrdrTnLineNumber>();

        if (reservationType != null) {
            if (reservationType.equals(OrdrTnReservationType.WLN)) {
                //Preparing the WS call to TOCP to retrieve from FMS through ASF, a list of spare TNs associated with a given OrdrTnNpaNxx
                parameters = generateTocpLookupRequestMessageForWirelineDarkDslAndRealTnSoapRequest(serviceAddress, selectedNpaNxx, lineNumberPrefix, isSearchForExactMatch, userId, true);
            }
            else if (reservationType.equals(OrdrTnReservationType.TOLLFREE)) {
                //Preparing the WS call to TOCP to retrieve from SMS, a list of spare Toll Free Numbers associated with a given OrdrTnNpaNxx
                parameters = generateTocpLookupRequestMessageForTollFreeSoapRequest(serviceAddress, selectedNpaNxx, lineNumberPrefix, isSearchForExactMatch, userId);
            }

            if (parameters != null) {
                //String tocpReqStr = SoaUtil.ObjectToXml(parameters);
                String tocpReqStr = null;
                System.debug( tocpReqStr);

                
                // Invoke the TOCP - FindResource web service
                try {
                    if (reservationType.equals(OrdrTnReservationType.TOLLFREE)) {
                        System.debug( 'Invoking SMS requestOrdrTn through TOCP FindResource');
                        tocpResp = findResource(parameters,true);
                    }
                    else {
                        System.debug( 'Invoking ASF InventoryService.requestOrdrTn through TOCP FindResource');
                        tocpResp = findResource(parameters,false);
                    }
                    
                    //tocpResp = null;
                }
                catch (Exception e) {
                    System.debug(e.getStackTraceString());
                    throw e;
                }

                // Process the TOCP response
                if (tocpResp != null) {
                    List<TpInventoryResourceConfigV3.LogicalResource> listRes = tocpResp.logicalResource;
                    if ((listRes != null) && (listRes.size() > 0)) {
                        for (TpInventoryResourceConfigV3.LogicalResource lr : listRes) {
                            
                                if (lr != null) {
                                    List<TpInventoryResourceConfigV3.LogicalResource> listLRes = lr.ComponentLogicalResource;
                                    if ((listLRes != null) && (listLRes.size() > 0)) {
                                        for (Integer i=0; i<listLRes.size(); i++) {
                                            if ((listLRes.get(i) != null) && (listLRes.get(i).CharacteristicValue != null)) {
                                                Map <String, String> hm = convertTocpV2CharacteristicValuesToMap (listLRes.get(i).CharacteristicValue);
                                                String npa = null;
                                                String nxx = null;
                                                String line = null;
                                                String tollFreeNumber = null;
                                                String releaseDate = null;
                                                String switchNumber=null;
                                                if (hm.containsKey('npa')) {
                                                    npa = hm.get('npa');
                                                }   
                                                if (hm.containsKey('nxx')) {
                                                    nxx = hm.get('nxx');
                                                }   
                                                if (hm.containsKey('line')) {
                                                    line = hm.get('line');
                                                }   
                                                if (hm.containsKey('tollFreeNumber')) {
                                                    tollFreeNumber = hm.get('tollFreeNumber');
                                                }
                                                if (hm.containsKey('releaseDate')) {
                                                    releaseDate = hm.get('releaseDate');
                                                }
                                                if (hm.containsKey('switchNumber')) {
                                                    switchNumber = hm.get('switchNumber');
                                                }

                                                if (String.isNotBlank(tollFreeNumber)) {
                                                    OrdrTnLineNumber lineNoObj=new OrdrTnLineNumber(tollFreeNumber, OrdrTnReservationType.TOLLFREE, OrdrTnLineNumberStatus.AVAILABLE); 
                                                    if(String.isNotBlank(switchNumber)){
                                                        lineNoObj.setSwitchNumber(switchNumber);
                                                    }                                                   
                                                    resultingList.add(lineNoObj);
                                                }
                                                else if (String.isNotBlank(npa) && String.isNotBlank(nxx) && String.isNotBlank(line)) {
                                                     OrdrTnLineNumber lineNoObj=new OrdrTnLineNumber(npa, nxx, line, releaseDate, OrdrTnReservationType.WLN);
                                                    if(String.isNotBlank(switchNumber)){
                                                        lineNoObj.setSwitchNumber(switchNumber);
                                                    }
                                                    resultingList.add(lineNoObj);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
            }
        }
        return resultingList;
    }
    

    
    private static TpCommonMessage.LookupRequestMessage generateTocpLookupRequestMessageForWirelineDarkDslAndRealTnSoapRequest(OrdrTnServiceAddress serviceAddress, OrdrTnNpaNxx selectedNpaNxx, String lineNumberPrefix, Boolean isSearchForExactMatch, String userId, boolean isSearchForTn){
        System.debug( 'Generating TN lookup request message for COID: ' + serviceAddress.getCOID());

        TpCommonMessage.LookupRequestMessage lrm = new TpCommonMessage.LookupRequestMessage();
            TpCommonBaseV3.EntityWithSpecification sourceCriteria = new TpCommonBaseV3.EntityWithSpecification();
            
            sourceCriteria.Specification=generateTocpV2EntitySpecificationForSoapRequest('Wireline', 'Telephone Number', 'Logical Resource');
            List<TpCommonBaseV3.CharacteristicValue> charValList=new List<TpCommonBaseV3.CharacteristicValue>();
            charValList.add(generateTocpV2CharacteristicValueForSoapRequest('userId', 'SFDCTN', true));
            
            if (isSearchForTn) {              
                String switchNumber = OrdrConstants.SWITCH_CODE;
                if (selectedNpaNxx!= null && String.isNotBlank(selectedNpaNxx.getSwitchNumber())){
                    switchNumber = selectedNpaNxx.getSwitchNumber();
                }
                System.debug('LookupRequestMessage method switchNumber='+switchNumber);
                String coidToUse = serviceAddress.getCOID();
                if (selectedNpaNxx!= null && String.isNotBlank(selectedNpaNxx.getServingCoid())){
                    coidToUse = selectedNpaNxx.getServingCoid();
                }
                System.debug('LookupRequestMessage method coidToUse='+coidToUse);
                                
                charValList.add(generateTocpV2CharacteristicValueForSoapRequest('coid', checkNullInput(coidToUse), false));
                
                charValList.add(generateTocpV2CharacteristicValueForSoapRequest('switchNumber', checkNullInput(switchNumber), false));
                
                charValList.add(generateTocpV2CharacteristicValueForSoapRequest('npa', checkNullInput(selectedNpaNxx.getNpa()), false));
                
                charValList.add(generateTocpV2CharacteristicValueForSoapRequest('nxx', checkNullInput(selectedNpaNxx.getNxx()), false));
                
                charValList.add(generateTocpV2CharacteristicValueForSoapRequest('line', checkNullInput(lineNumberPrefix), false));
                
                charValList.add(generateTocpV2CharacteristicValueForSoapRequest('releaseDate', '', false));
                sourceCriteria.CharacteristicValue=charValList;
            }
            else {
                
                charValList.add(generateTocpV2CharacteristicValueForSoapRequest('coid', checkNullInput(serviceAddress.getCOID()), false));
                charValList.add(generateTocpV2CharacteristicValueForSoapRequest('province', checkNullInput(serviceAddress.getProvinceStateCode()), false));
                charValList.add(generateTocpV2CharacteristicValueForSoapRequest('city', checkNullInput(serviceAddress.getMunicipalityName()), false));
                charValList.add(generateTocpV2CharacteristicValueForSoapRequest('rateCenter', checkNullInput(serviceAddress.getRateCenter()), false));
                charValList.add(generateTocpV2CharacteristicValueForSoapRequest('switchNumber', checkNullInput(serviceAddress.getSwitchNumber()), false));
                charValList.add(generateTocpV2CharacteristicValueForSoapRequest('switchType', checkNullInput(serviceAddress.getSwitchType()), false));
                
                sourceCriteria.CharacteristicValue=charValList;

                System.debug( 'Extracting TN Lookup Params -- COID: ' + serviceAddress.getCOID() + ', Rate Center:' + serviceAddress.getRateCenter() + ', Switch Number:' + serviceAddress.getSwitchNumber() + ', Switch Type: ' + serviceAddress.getSwitchType());
            }

            List<TpCommonBaseV3.EntityWithSpecification> sourceCriteriaList1=new List<TpCommonBaseV3.EntityWithSpecification>();
            sourceCriteriaList1.add(sourceCriteria);

            lrm.SourceCriteria= sourceCriteriaList1;
        
            TpCommonBaseV3.EntityWithSpecification targetCriteria = new TpCommonBaseV3.EntityWithSpecification();
            List<TpCommonBaseV3.CharacteristicValue> charValList2=new List<TpCommonBaseV3.CharacteristicValue>();
            charValList2.add(generateTocpV2CharacteristicValueForSoapRequest('searchOption', (isSearchForTn ? 'findTelephoneNumbers' : 'findNpaNxxCoData') , true));
           
            /*if (isSearchForTn) {
                charValList2.add(generateTocpV2CharacteristicValueForSoapRequest('exactMatch', (isSearchForExactMatch ? 'true' : 'false') , false));
                
            }*/

             targetCriteria.CharacteristicValue=charValList2;
            List<TpCommonBaseV3.EntityWithSpecification> entitySpecList=new List<TpCommonBaseV3.EntityWithSpecification>();
            entitySpecList.add(targetCriteria);
            lrm.TargetCriteria= entitySpecList;
        
        return lrm;
        
    }
        
    private static TpCommonMessage.LookupRequestMessage generateTocpLookupRequestMessageForTollFreeSoapRequest(OrdrTnServiceAddress serviceAddress, OrdrTnNpaNxx selectedNpaNxx, String lineNumberPrefix, Boolean isSearchForExactMatch, String userId) {
        System.debug('Inside generateTocpLookupRequestMessageForTollFreeSoapRequest method');
        TpCommonMessage.LookupRequestMessage lrm = new TpCommonMessage.LookupRequestMessage();
    
            String tollFreeNumber = null;
            String lineNumberPrefixToUse = checkNullInput(lineNumberPrefix).trim();
            String npaToUse = '';
            String nxxToUse = '';
            String consecutiveFlag = null;
            if (selectedNpaNxx!= null) {
                npaToUse = checkNullInput(selectedNpaNxx.getNpa()).trim();
                nxxToUse = checkNullInput(selectedNpaNxx.getNxx()).trim();
            }
            while (npaToUse.length() < 3)
            {
                npaToUse = npaToUse + '*';
            }
            while (nxxToUse.length() < 3)
            {
                nxxToUse = nxxToUse + '*';
            }           
            while (lineNumberPrefixToUse.length() < 4)
            {
                lineNumberPrefixToUse = lineNumberPrefixToUse + '*';
            }
            tollFreeNumber = npaToUse+nxxToUse+lineNumberPrefixToUse;
            if (tollFreeNumber.substring(9).equalsIgnoreCase('*')) {
                consecutiveFlag = 'Y';
            }
            else {
                consecutiveFlag = 'N';
            }
            String id = String.valueOf(System.currentTimeMillis()).substring(4,13);
            TpCommonBaseV3.EntityWithSpecification sourceCriteria = new TpCommonBaseV3.EntityWithSpecification();
            // Total length of ID passed to SMS should be exactly 10 : 9 digits number prefixed by 'O'
            sourceCriteria.Id='O'+id;
            sourceCriteria.Specification=generateTocpV2EntitySpecificationForSoapRequest('', 'Toll Free Telephone Number', 'Logical Resource');
            //sourceCriteria.CharacteristicValue=generateTocpV2CharacteristicValueForSoapRequest('respOrg', checkNullInput(SoaUtil.getPropertyValue('webService.findResource.request.tollfree.respOrg')), true);

             Ordering_WS__c findResourceTollfreeRespOrg = Ordering_WS__c.getValues('FindResourceTollfreeRespOrg');  //ReserveResourceTollfreeEndpoint
             //binding.endpoint_x = findResourceTollfreeRespOrg.value__c;
              System.debug('Test '+tollFreeNumber);
             List<TpCommonBaseV3.CharacteristicValue> charValList2=new List<TpCommonBaseV3.CharacteristicValue>();
             charValList2.add(generateTocpV2CharacteristicValueForSoapRequest('respOrg', 'EUS01', true));
             charValList2.add(generateTocpV2CharacteristicValueForSoapRequest('tollFreeNumber', checkNullInput(tollFreeNumber).toUpperCase(), false));
             charValList2.add(generateTocpV2CharacteristicValueForSoapRequest('consecutiveFlag', checkNullInput(consecutiveFlag), false));
            sourceCriteria.CharacteristicValue=charValList2;
            //For vanity telephone numbers, make sure all alphabetic characters are capitalized - otherwise TOCP/SMS returns an error such as 1004 - Invalid Vanity Number
            
            List<TpCommonBaseV3.EntityWithSpecification> sourceCriteriaList=new List<TpCommonBaseV3.EntityWithSpecification>(); 
            sourceCriteriaList.add(sourceCriteria);
            lrm.SourceCriteria=sourceCriteriaList;
        
        return lrm;
    }
    
  
    

    

    public static TpCommonBaseV3.CharacteristicValue generateTocpV2CharacteristicValueForSoapRequest(
            String nameOfCharacteristic, String valueOfCharacteristic, boolean firstElement) {

        TpCommonBaseV3.CharacteristicValue charValueOne = null;
        if (String.isNotBlank(nameOfCharacteristic)) {
            charValueOne = new TpCommonBaseV3.CharacteristicValue();
            TpCommonBaseV3.Characteristic charOne = new TpCommonBaseV3.Characteristic();
            charOne.Name=nameOfCharacteristic;           
            charValueOne.Characteristic=charOne;
            
            List<String> charValList=new List<String>();
                charValList.add(checkNullInput(valueOfCharacteristic));     
            charValueOne.Value=charValList;
            
        }
        return charValueOne;
    }

    private static String checkNullInput(String sInput) {
        String sOutput = '';
        sOutput = (String.isBlank(sInput) ? sOutput : sInput);  
        return sOutput;
    }

    public static Map <String, String> convertTocpV2CharacteristicValuesToMap (List<TpCommonBaseV3.CharacteristicValue> listCharVal) {
        Map <String, String> hm = new Map <String, String>();
        for(TpCommonBaseV3.CharacteristicValue cv:listCharVal){
            if ((cv != null) && (cv.Characteristic != null)) {
                TpCommonBaseV3.Characteristic c = cv.Characteristic;
                String cName  = c.Name;
                if (String.isNotBlank(cName)) {
                    String cValue = null;
                    if ((cv.Value != null) && (cv.Value.size()>0)) {
                        cValue = cv.Value.get(0);
                        if (!hm.containsKey(cName)) {
                            hm.put(cName, cValue);
                        }
                    }
                }
            }
        }

        System.debug('Charcterstic map is '+hm);
        return hm;
    }
        public static TpCommonBaseV3.EntitySpecification generateTocpV2EntitySpecificationForSoapRequest(
            String specificationName, String specificationType, String specificationCategory) {
        // Setting service specifications
        TpCommonBaseV3.EntitySpecification entitySpec = new TpCommonBaseV3.EntitySpecification();
        entitySpec.Name=checkNullInput(specificationName);
        entitySpec.Type_x=checkNullInput(specificationType);
        entitySpec.Category=checkNullInput(specificationCategory);
        return entitySpec;
    }

}