public without sharing class OCOM_ServiceAddressControllerHelper 
{
    
     Public Order UpdateAddressAndOrder(Id orderId, SMBCare_Address__c smbCareAddr, ServiceAddressData SAData)
    {
        System.debug('SAData Update :'+SAData);
        List<SMBCare_Address__c> SMBCareList = New List<SMBCare_Address__c>();
        Id ServiceRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();

        Order OrdToUpdate=[select id, AccountId, Service_Address__c,Service_Address__r.id,account.id,Service_Address_Text__c, Status, Maximum_Speed__c, CreatedBy.Name,FMS_Address_ID__c, vlocity_cmt__QuoteId__c from Order where id=:orderId];
        
          //*********************************************************//
            
             if(SAData.isManualCapture){
                smbCareAddr.Address__c =SAData.address;
                smbCareAddr.city__c = SAData.city;
  
                smbCareAddr.Province__c = SAData.province;
                smbCareAddr.Postal_Code__c= SAData.postalCode;
                smbCareAddr.Country__c=SAData.country; 
                
               OrdToUpdate.Service_Address_Text__c = smbCareAddr.Address__c + ' ' + smbCareAddr.City__c + ' ' + smbCareAddr.Province__c + ' ' + smbCareAddr.Postal_Code__c + ' ' + smbCareAddr.Country__c;
                
              }else{
             OrdToUpdate.Service_Address_Text__c= SAData.searchString;}

            if(smbCareAddr.Status__c == 'Valid'){
             OrdToUpdate.Activities__c='Service Address confirmed';
            }else{
             OrdToUpdate.Activities__c='Pending Service Address confirmation';}

          boolean serviceAccountFound=false;
   
          SMBCareList = [select Service_Account_Id__c,id, Account__c, FMS_Address_ID__c, Status__c,Postal_Code__c , Street_Number__c, city__c , Street__c, Street_Name__c, Street_Direction_Code__c , Suite_Number__c , Province__c, COID__c , Building_Number__c , Country__c,  State__c, Civic_Number__c, Street_Type_Code__c , Location_Id__c , Address__c , Address_Type__c ,  LocalRoutingNumber__c , NPA_NXX__c , SwitchNumber__c , TerminalNumber__c, Service_Account_Id__r.RecordTypeId  from SMBCare_Address__c where Account__c =:OrdToUpdate.accountid and  FMS_Address_ID__c=:SAData.fmsId];
          
          System.debug('SMBCareList ###'+SMBCareList);
          
         
        // Service_address__c field will be copied by Field Mapper from existing Quote to Order. 
        if(SMBCareList!=null) {
            
                      
            for(SMBCare_Address__c SmbCare: SMBCareList){         
              if(String.isNotBlank(SmbCare.Service_Account_Id__c) && SmbCare.Service_Account_Id__r.RecordTypeId.equals(ServiceRecordType) && SmbCare.Account__c ==OrdToUpdate.AccountId){
                  serviceAccountFound=true;
                  System.debug('serviceAccountFound###'+serviceAccountFound);
                  OrdToUpdate.Service_address__c=SmbCare.id;
                  UpdateServiceAccountAssociation(OrdToUpdate.id ,SmbCare.Service_Account_Id__c);
                  updateServiceAddress(SmbCare, SAData, SmbCare.id);
                  return null;
              }  
          }
        }
          
          if(!serviceAccountFound && SMBCareList.size()>0){
              List<SMBCare_Address__c> toUpdateList=new List<SMBCare_Address__c>();
              for(SMBCare_Address__c SmbCare: SMBCareList){
                  if(String.isBlank(SmbCare.Service_Account_Id__c) && SmbCare.Account__c ==OrdToUpdate.AccountId){
                      Account ac = CreateServiceAcc(OrdToUpdate,SAData);
                      SmbCare.Service_Account_Id__c=ac.id;  
                    toUpdateList.add(SmbCare);  
                   break;   
                  }
              }
              if(toUpdateList.size()>0){                  
                  update toUpdateList;  
              }         
          }
  
          if(SMBCareList==null || SMBCareList.size()==0){
               System.debug('No record found###')  ; 
               Account ac = CreateServiceAcc(OrdToUpdate,SAData);
               OrdToUpdate.FMS_Address_ID__c=SAData.fmsId; 
               id smbCareAddrId =createServiceAddress(smbCareAddr, SAData,OrdToUpdate.accountId, ac.Id);
               OrdToUpdate.Service_Address__c=smbCareAddrId;
          }
         
     return  OrdToUpdate;
 }
 //********************************************************************//        
    
    public Quote UpdateAddressAndQuote(Id quoteId, SMBCare_Address__c smbCareAddr, ServiceAddressData SAData)
    {
        System.debug('UpdateAddressAndQuote   called');
       
        Quote Qttoupdate1= new Quote();
        Id smcCareAddressId;
        List<SMBCare_Address__c> existingAddress;
        List<quote> QttoupdateList=[select id,Service_Address__c,Service_Address__r.id, FMS_Address_ID__c, accountId, Status, Activities__c, ExpirationDate, CreatedBy.Name from quote where id=:quoteId];
        if(null!= QttoupdateList &&QttoupdateList.size()>0)
          Qttoupdate1=QttoupdateList[0];
        //we should remove any service account association up on address change. at order creation time, service account association will be propergated to quote
        Qttoupdate1.ServiceAccountId__c =  null;
        if(SAData.fmsId != null && SAData.fmsId.trim().length() > 0) {
            system.debug('OCOM_ServiceAddressControllerHelper::UpdateAddressAndQuote() SAData.fmsId: ' + SAData.fmsId);
            existingAddress=[select id, Postal_Code__c , Street_Number__c, city__c , Street__c, Street_Name__c, Street_Direction_Code__c , Suite_Number__c , Province__c, COID__c , Building_Number__c , Country__c, FMS_Address_ID__c, State__c, Civic_Number__c, Street_Type_Code__c , Location_Id__c , Address__c , Address_Type__c , Status__c , LocalRoutingNumber__c , NPA_NXX__c , SwitchNumber__c , TerminalNumber__c  from SMBCare_Address__c where FMS_Address_ID__c=:SAData.fmsId and  Account__c =:Qttoupdate1.accountId limit 1]; //Address__c, Block__c, Street__c, City__c, Province__c, Postal_Code__c, Country__c
            system.debug('OCOM_ServiceAddressControllerHelper::UpdateAddressAndQuote() existingAddress: ' + existingAddress);
        }
      
        if(existingAddress == null || !(existingAddress.size()>0))
        {
            system.debug('###-in Save if');

            smcCareAddressId = createServiceAddress(smbCareAddr, SAData, Qttoupdate1.accountId,null); 
            Qttoupdate1.Service_Address__c=smbCareAddr.id;
            //Added to populate FMS Id on Quote..
            system.debug(' Before Update SMB care '+smbCareAddr);
            Qttoupdate1.FMS_Address_ID__c=smbCareAddr.FMS_Address_ID__c;
            system.debug(' After Update SMB care '+smbCareAddr);
           if(SAData.isManualCapture)
                Qttoupdate1.Service_Address_Text__c = smbCareAddr.Address__c + ' ' + smbCareAddr.City__c + ' ' + smbCareAddr.Province__c + ' ' + smbCareAddr.Postal_Code__c + ' ' + smbCareAddr.Country__c;
           else
                Qttoupdate1.Service_Address_Text__c= SAData.searchString;
        }
        else
        {
            
            Qttoupdate1.Service_Address__c=existingAddress[0].id;
            //Added to populate FMS Id on Quote..
            Qttoupdate1.FMS_Address_ID__c=existingAddress[0].FMS_Address_ID__c;
             system.debug(' After Update SMB care else '+existingAddress[0]);
            if(SAData.isManualCapture)
                Qttoupdate1.Service_Address_Text__c = SAData.Address + ' ' + SAData.City + ' ' + SAData.Province + ' ' + SAData.PostalCode + ' ' + SAData.Country;
           else
                Qttoupdate1.Service_Address_Text__c= SAData.searchString;
            //TODO: move in to updateServiceAddress
           if(String.isBlank(existingAddress[0].Street_Type_Code__c)){
                smbCareAddr.Street_Type_Code__c = 'Other';
            }
            
            smcCareAddressId = updateServiceAddress(existingAddress[0], SAData, existingAddress[0].id);
            
        }
           
        String actDraft = 'Created by '+ qttoupdate1.CreatedBy.Name + ';\r\n Pending:  Address validation; \r\n Action: Confirm Address;';
        String actInProgress;
        
        Date today = Date.today();
        Date ExpDate = null;
        OCOM_Quote_Settings__c qs = OCOM_Quote_Settings__c.getOrgDefaults();
        system.debug('Quote Settings: '+ qs);
        if(qs!=null && qs.Days_to_expiry__c > 0 )
        {
            ExpDate = today.addDays((Integer)qs.Days_to_expiry__c);
        }
        List<SMBCare_Address__c> smbCareAddrStatus = new List<SMBCare_Address__c>();
       
        smbCareAddrStatus = [select Status__c from SMBCare_Address__c where id=:smcCareAddressId];
        
        if(smbCareAddrStatus[0].Status__c== 'Valid' && (Qttoupdate1.Status == null || Qttoupdate1.Status == 'Draft'))
        {    
            Qttoupdate1.Status= 'In Progress';
            if(Qttoupdate1.ExpirationDate == null && ExpDate != null)
                Qttoupdate1.ExpirationDate = ExpDate;
            actInProgress = 'Created by '+ qttoupdate1.CreatedBy.Name + ';\r\n Address confirmed;\r\n Prices valid until '+ Qttoupdate1.ExpirationDate.format();
            Qttoupdate1.Activities__c=actInProgress;
        }
        else if(smbCareAddrStatus[0].Status__c == 'Valid' && Qttoupdate1.Status != 'Draft')
        {
            actInProgress = 'Created by '+ qttoupdate1.CreatedBy.Name + ';\r\n Address confirmed;\r\n Prices valid until '+ Qttoupdate1.ExpirationDate.format();
            Qttoupdate1.Activities__c=actInProgress;
        }
        else
        {    system.debug('****** Status: ' + smbCareAddrStatus); 
            Qttoupdate1.Status= 'Draft'; 
            Qttoupdate1.Activities__c = actDraft;
            Qttoupdate1.ExpirationDate = null;
        }
        
        return Qttoupdate1;
    }
    
    
   
    
     /* Method to Associate a Service Account based on the service address that was updated on Order */
    
    
       
    Public Void CreateServiceAccount(Order ord, SMBCare_Address__c smbCareAddr, ServiceAddressData SAData)
    {

        List<SMBCare_Address__c> SMBCareList = New List<SMBCare_Address__c>();
        Id ServiceRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        
          //*********************************************************//
          boolean serviceAccountFound=false;
         SMBCareList = [select Service_Account_Id__c,id, Account__c, FMS_Address_ID__c,Service_Account_Id__r.RecordTypeId  from SMBCare_Address__c where id =:Ord.Service_Address__c];
          
        // Service_address__c field will be copied by Field Mapper from existing Quote to Order.  
          for(SMBCare_Address__c SmbCare: SMBCareList){         
              if(String.isNotBlank(SmbCare.Service_Account_Id__c) && SmbCare.Service_Account_Id__r.RecordTypeId.equals(ServiceRecordType) && SmbCare.Account__c ==Ord.AccountId){
                serviceAccountFound=true;
                UpdateServiceAccountAssociation(ord.id, SmbCare.Service_Account_Id__c);
                return;
              }  
          }
          if(!serviceAccountFound && SMBCareList.size()>0){
              List<SMBCare_Address__c> toUpdateList=new List<SMBCare_Address__c>();
              for(SMBCare_Address__c SmbCare: SMBCareList){
                  if(String.isBlank(SmbCare.Service_Account_Id__c) && SmbCare.Account__c ==Ord.AccountId){
                      Account ac = CreateServiceAcc(Ord,SAData);
                      SmbCare.Service_Account_Id__c=ac.id;  
                    toUpdateList.add(SmbCare);            
                  }
              }
              if(toUpdateList.size()>0){                  
                  update toUpdateList;  
              }         
          }
  
          if(SMBCareList==null || SMBCareList.size()==0){
               Account ac = CreateServiceAcc(Ord,SAData);
               SAData.fmsId = Ord.FMS_Address_ID__c ; 
               id smbCareAddr1 =createServiceAddress(smbCareAddr, SAData, ord.AccountId,ac.Id);
               Ord.Service_Address__c=smbCareAddr1;
          }

 
     Update Ord;
 }
    Public Account CreateServiceAcc(Order ord,ServiceAddressData SAData)
    {
        Id ServiceRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
       String FmsID=ord.FMS_Address_ID__c;
        if(null!= SAData && string.isNotBlank(SAData.fmsId)){
            FmsID=SAData.fmsId;           
        }
        Account Acc = New Account(Name=ord.Service_Address_Text__c+' '+FmsID, RecordTypeId=ServiceRecordType, ParentId=Ord.AccountId);
        try
        {
        Insert Acc;
        }
        catch(exception e)
        {
         return null;
        }
        UpdateServiceAccountAssociation(ord.id, Acc.id);
        return Acc;
    }
    
    /* Service Account creation end */
    
    
    public Void UpdateServiceAccountAssociation(Id odrId, ID svcAccountId)
    {
        Order ord = [Select id, vlocity_cmt__QuoteId__c,vlocity_cmt__QuoteId__r.OpportunityId from Order where id=:odrId];
        ord.ServiceAccountId__c = svcAccountId;
        update ord;
        
        List<work_order__c> wrkOrds = new List<work_order__c>();
        wrkOrds = [Select id, ServiceAccountId__c from work_order__c where order__c =:odrId];
        if(wrkOrds != null && wrkOrds.size() > 0)
        {
            for(work_order__c wrkOrd:wrkOrds)
                wrkOrd.ServiceAccountId__c = svcAccountId;
            update wrkOrds;
        }
        //get the Quote for this order and associate it with the service account
        if(String.IsNotBlank(ord.vlocity_cmt__QuoteId__c))
        {
            Quote quo = [Select id, ServiceAccountId__c, OpportunityId from Quote where id=:ord.vlocity_cmt__QuoteId__c];
            if(String.isBlank(quo.ServiceAccountId__c))
            {
                quo.ServiceAccountId__c = svcAccountId;
                update quo;
            }
            //get the Opportunity for this order and associate it with the service account
            if(String.IsNotBlank(quo.OpportunityId))
            {
                Opportunity opp = [select id, ServiceAccountId__c from Opportunity where id=:quo.OpportunityId];
                if(String.isBlank(opp.ServiceAccountId__c))
                {
                    opp.ServiceAccountId__c = svcAccountId;
                    update opp;
                }
            }
        } 
    } 
    
    public Id createServiceAddress(SMBCare_Address__c smbCareAddr, ServiceAddressData SAData, Id accountId ,Id serviceAccountId)
    {
        smbCareAddr.Postal_Code__c = SAData.postalCode;
        //Added as per BSBDTR-710 - Billing Fallout for NaaS Non-ILEC Orders
        if(String.isNotBlank(SAData.streetNumber)){
            smbCareAddr.Street_Number__c = SAData.streetNumber;
        }else{
      	  smbCareAddr.Street_Number__c= SAData.buildingNumber;
        }
      //..Added by Aditya Jamwal (IBM) as apert of OCOM-1214 
        if(String.isNotBlank(SAData.streetTypeSuffix)){
         smbCareAddr.StreetType__c =SAData.streetTypeSuffix;                     
        }else if (String.isNotBlank(SAData.streetTypePrefix)) {
            smbCareAddr.StreetType__c = SAData.streetTypePrefix;
       }else{
            smbCareAddr.StreetType__c = 'Other';
       }
        if(String.isNotBlank(SAData.municipalityNameLPDS)){
            smbCareAddr.Municipality_Name__c =SAData.municipalityNameLPDS;                     
        }
        //....End
        //Defect-9741 Begin
          if(String.isNotBlank(SAData.rateCenter)){ 
            smbCareAddr.Rate_Center__c =SAData.rateCenter;                     
        }
        //Defect-9741 End
        //Added by Arvind for the new fields receiving via ServiceAddress Management Service V1.1 (US-1101)
        
       if(String.isNotBlank(SAData.dropFacilityCode)){ 
         smbCareAddr.Drop_FacilityCode__c =SAData.dropFacilityCode;                     
        }

     if(String.isNotBlank(SAData.dropTypeCode)){ 
         smbCareAddr.Drop_TypeCode__c =SAData.dropTypeCode;                     
        }

     if(String.isNotBlank(SAData.dropLength)){ 
         smbCareAddr.Drop_Length__c =SAData.dropLength;                     
        }

     if(String.isNotBlank(SAData.dropExceptionCode)){ 
         smbCareAddr.Drop_ExceptionCode__c =SAData.dropExceptionCode;                     
        }

     if(String.isNotBlank(SAData.currentTransportTypeCode)){ 
         smbCareAddr.Current_TransportTypeCode__c =SAData.currentTransportTypeCode;                     
        }

     if(String.isNotBlank(SAData.futureTransportTypeCode)){ 
         smbCareAddr.Future_TransportTypeCode__c =SAData.futureTransportTypeCode;                     
        }

     if(String.isNotBlank(SAData.futurePlantReadyDate)){ 
         smbCareAddr.Future_PlantReadyDate__c =SAData.futurePlantReadyDate;                     
        }

     if(String.isNotBlank(SAData.futureTransportRemarkTypeCode )){ 
         smbCareAddr.Future_TransportRemarkTypeCode__c =SAData.futureTransportRemarkTypeCode;                     
        }

     if(String.isNotBlank(SAData.gponBuildTypeCode)){ 
         smbCareAddr.GPON_BuildTypeCode__c =SAData.gponBuildTypeCode;                     
        }

     if(String.isNotBlank(SAData.provisioningSystemCode)){ 
         smbCareAddr.Provisioning_SystemCode__c =SAData.provisioningSystemCode;                     
        }
            
        //Addition End 
        
        smbCareAddr.city__c = SAData.city;
        
      /*    //[Roderick] 2016-03-03: Interim fix to map abbreviated city to the long name
            //smbCareAddr.city__c = SAData.city;
            if (municipality__c.getValues(SAData.city) == null) {
                
            } else { 
                smbCareAddr.city__c = municipality__c.getValues(SAData.city).decode__c;   
            } 
      */  
        smbCareAddr.Street__c= SAData.street;
        //Fixing it as the street name is not coming in full from lpds. Storing it from FMS
         //smbCareAddr.Street_Name__c= SAData.streetName;
        
        if(string.isNotBlank(SAData.streetName_fms)){
            smbCareAddr.Street_Name__c=SAData.streetName_fms;}
        else if(string.isNotBlank(SAData.streetName)){
            smbCareAddr.Street_Name__c=SAData.streetName;}
        else if(string.isNotBlank(SAData.street)){
            smbCareAddr.Street_Name__c=SAData.street;}
        
        smbCareAddr.Street_Direction_Code__c = SAData.streetDirection;
        smbCareAddr.Suite_Number__c = SAData.suiteNumber;
        smbCareAddr.Province__c=   SAData.province;
        smbCareAddr.COID__c = SAData.coid;
        smbCareAddr.Building_Number__c = SAData.buildingNumber;
         System.debug('smbCareAddr.coid__c###'+smbCareAddr.COID__c);
        smbCareAddr.Country__c= SAData.country;
        smbCareAddr.FMS_Address_ID__c= SAData.fmsId;
        smbCareAddr.State__c= SAData.state;
        smbCareAddr.Civic_Number__c= SAData.civicNumber;
         
        //[Santosh] Populating streetTypeCode as 'Other'
        //smbCareAddr.Street_Type_Code__c = SAData.streetTypeCode;
        if(String.isBlank(SAData.streetTypeCode)){
            smbCareAddr.Street_Type_Code__c = 'Other';
        } else {
            smbCareAddr.Street_Type_Code__c = SAData.streetTypeCode;
        }
        smbCareAddr.Location_Id__c = SAData.locationId;
        smbCareAddr.is_ILEC__c = SAData.isILEC;
        
        smbCareAddr.Address__c = SAData.address;
        //smbCareAddr.Status__c = SAData.status;
        smbCareAddr.Address_Type__c = 'Standard'; // Service Address Type defaults to Standard.
        
        if (String.isBlank(SAData.fmsId) && SAData.isILEC)
            smbCareAddr.Status__c ='Invalid';
        else
            smbCareAddr.Status__c ='Valid';
        
        smbCareAddr.LocalRoutingNumber__c = SAData.localRoutingNumber;
        smbCareAddr.NPA_NXX__c = SAData.ratingNpaNxx;
        smbCareAddr.SwitchNumber__c = SAData.switchNumber;
        smbCareAddr.TerminalNumber__c = SAData.terminalNumber;
        smbCareAddr.coid__c = SAData.coid;
        smbCareAddr.CLLI__c = SAData.clliCode;
        smbCareAddr.FMS_Address_ID__c= SAData.fmsId;
        smbCareAddr.State__c= SAData.state;
        
        
        
        
        smbCareAddr.Account__c = accountId;
        if(null!=serviceAccountId){
            smbCareAddr.Service_Account_Id__c = serviceAccountId;
        }
        system.debug('##### B4 Insert: '+smbCareAddr);
        Insert smbCareAddr;
        system.debug('##### After Insert: '+smbCareAddr);
        return smbCareAddr.id;
       // Upsert smbCareAddr;
    }
    
     public Id updateServiceAddress(SMBCare_Address__c smbCareAddr, ServiceAddressData SAData,String idStr)
    {
        
        system.debug('OCOM_ServiceAddressControllerHelper::updateServiceAddress() smbCareAddr: ' + smbCareAddr);
        system.debug('OCOM_ServiceAddressControllerHelper::updateServiceAddress() SAData: ' + SAData);
        system.debug('OCOM_ServiceAddressControllerHelper::updateServiceAddress() idStr: ' + idStr);
                
        if(String.isBlank(idStr)){
            return null;
        }
        system.debug('##### updateServiceAddress in ID: '+idStr);
        smbCareAddr.Postal_Code__c = SAData.postalCode;
        //Added as per BSBDTR-710 - Billing Fallout for NaaS Non-ILEC Orders
        if(String.isNotBlank(SAData.streetNumber)){
            smbCareAddr.Street_Number__c = SAData.streetNumber;
        }else{
      	  smbCareAddr.Street_Number__c= SAData.buildingNumber;
        }
       //..Added by Aditya Jamwal (IBM) as apert of OCOM-1214 
        
         if(String.isNotBlank(SAData.streetTypeSuffix)){
              system.debug('##### 11111 ' + SAData.streetTypeSuffix);
            smbCareAddr.StreetType__c =SAData.streetTypeSuffix;                     
        }else if (String.isNotBlank(SAData.streetTypePrefix)) {
              system.debug('##### 22222 '+ SAData.streetTypePrefix);
            smbCareAddr.StreetType__c = SAData.streetTypePrefix;
        }else{
              system.debug('##### 33333 ');
             smbCareAddr.StreetType__c = 'Other';
       }
        if(String.isNotBlank(SAData.municipalityNameLPDS)){
            smbCareAddr.Municipality_Name__c =SAData.municipalityNameLPDS;                     
        }
      //..End  
        //Defect-9741 Begin
          if(String.isNotBlank(SAData.rateCenter)){ 
            smbCareAddr.Rate_Center__c =SAData.rateCenter;                     
        }
        //Defect-9741 End

    //Added by Arvind for the new fields receiving via ServiceAddress Management Service V1.1 (US-1101)
        
       if(String.isNotBlank(SAData.dropFacilityCode)){ 
         smbCareAddr.Drop_FacilityCode__c =SAData.dropFacilityCode;                     
        }

     if(String.isNotBlank(SAData.dropTypeCode)){ 
         smbCareAddr.Drop_TypeCode__c =SAData.dropTypeCode;                     
        }

     if(String.isNotBlank(SAData.dropLength)){ 
         smbCareAddr.Drop_Length__c =SAData.dropLength;                     
        }

     if(String.isNotBlank(SAData.dropExceptionCode)){ 
         smbCareAddr.Drop_ExceptionCode__c =SAData.dropExceptionCode;                     
        }

     if(String.isNotBlank(SAData.currentTransportTypeCode)){ 
         smbCareAddr.Current_TransportTypeCode__c =SAData.currentTransportTypeCode;                     
        }

     if(String.isNotBlank(SAData.futureTransportTypeCode)){ 
         smbCareAddr.Future_TransportTypeCode__c =SAData.futureTransportTypeCode;                     
        }

     if(String.isNotBlank(SAData.futurePlantReadyDate)){ 
         smbCareAddr.Future_PlantReadyDate__c =SAData.futurePlantReadyDate;                     
        }

     if(String.isNotBlank(SAData.futureTransportRemarkTypeCode )){ 
         smbCareAddr.Future_TransportRemarkTypeCode__c =SAData.futureTransportRemarkTypeCode;                     
        }

     if(String.isNotBlank(SAData.gponBuildTypeCode)){ 
         smbCareAddr.GPON_BuildTypeCode__c =SAData.gponBuildTypeCode;                     
        }

     if(String.isNotBlank(SAData.provisioningSystemCode)){ 
         smbCareAddr.Provisioning_SystemCode__c =SAData.provisioningSystemCode;                     
        }
            
        //Addition End 

        smbCareAddr.city__c = SAData.city;
 
        smbCareAddr.Street__c= SAData.street;
        //Fixing it as the street name is not coming in full from lpds. Storing it from FMS
         //smbCareAddr.Street_Name__c= SAData.streetName;
       
       if(string.isNotBlank(SAData.streetName_fms)){
            smbCareAddr.Street_Name__c=SAData.streetName_fms;}
        else if(string.isNotBlank(SAData.streetName)){
            smbCareAddr.Street_Name__c=SAData.streetName;}
        else if(string.isNotBlank(SAData.street)){
            smbCareAddr.Street_Name__c=SAData.street;}
        
        smbCareAddr.Street_Direction_Code__c = SAData.streetDirection;
        smbCareAddr.Suite_Number__c = SAData.suiteNumber;
        smbCareAddr.Province__c=   SAData.province;
        smbCareAddr.COID__c = SAData.coid;
        smbCareAddr.Building_Number__c = SAData.buildingNumber;
         System.debug('smbCareAddr.coid__c###'+smbCareAddr.COID__c);
        smbCareAddr.Country__c= SAData.country;
        smbCareAddr.FMS_Address_ID__c= SAData.fmsId;
        smbCareAddr.State__c= SAData.state;
        smbCareAddr.Civic_Number__c= SAData.civicNumber;
         
        //[Santosh] Populating streetTypeCode as 'Other'
        //smbCareAddr.Street_Type_Code__c = SAData.streetTypeCode;
        if(String.isBlank(SAData.streetTypeCode)){
            smbCareAddr.Street_Type_Code__c = 'Other';
        } else {
            smbCareAddr.Street_Type_Code__c = SAData.streetTypeCode;
        }
        smbCareAddr.Location_Id__c = SAData.locationId;
        smbCareAddr.is_ILEC__c = SAData.isILEC;
        
        smbCareAddr.Address__c = SAData.address;
        //smbCareAddr.Status__c = SAData.status;
        smbCareAddr.Address_Type__c = 'Standard'; // Service Address Type defaults to Standard.
        
        if (String.isBlank(SAData.fmsId) && SAData.isILEC)
            smbCareAddr.Status__c ='Invalid';
        else
            smbCareAddr.Status__c ='Valid';
        
        smbCareAddr.LocalRoutingNumber__c = SAData.localRoutingNumber;
        
        system.debug('');
        smbCareAddr.NPA_NXX__c = SAData.ratingNpaNxx;
        smbCareAddr.SwitchNumber__c = SAData.switchNumber;
        smbCareAddr.TerminalNumber__c = SAData.terminalNumber;
        smbCareAddr.coid__c = SAData.coid;
        smbCareAddr.CLLI__c = SAData.clliCode;
        smbCareAddr.FMS_Address_ID__c= SAData.fmsId;
        smbCareAddr.State__c= SAData.state;
        smbCareAddr.id=idStr;
        system.debug('#####'+smbCareAddr);

        update smbCareAddr;
        return smbCareAddr.Id;
    }
}