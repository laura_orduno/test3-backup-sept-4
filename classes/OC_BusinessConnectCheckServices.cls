/*******************************************************************************

     * Description  : Apex class for Standalone Check Services (Business Connect) features
     * Author       : Andrew Fee
     * Date         : Oct-2017
     *
     * Modification Log:
     * ------------------------------------------------------------------------------
     * Ver      Date                Modified By           Description
     * ------------------------------------------------------------------------------
     * 1.0     
     
     *******************************************************************************/

public class OC_BusinessConnectCheckServices {

	private final String SORTBYCITY = Label.OD_City;// 'City';
    private final String SORTBYNPA = Label.OC_Area_Code;//'Area Code';

    private final String AB = 'Alberta';
    private final String BC = 'British Columbia';
    private final String MB = 'Manitoba';
    private final String NB = 'New Brunswick';
    private final String NL = 'Newfoundland and Labrador';
    private final String NWT = 'Northwest Territories';
    private final String NS = 'Nova Scotia';
    private final String NU = 'Nunavut';
    private final String ONT = 'Ontario';
    private final String PEI = 'Prince Edward Island';
    private final String QC = 'Quebec';
    private final String SK = 'Saskatchewan';
    private final String YT = 'Yukon';


	public String portabilityCheckMessage {get;set;}
    public Boolean portabilityCheckFailure {get;set;}
    public Boolean portabilityCheckReturned {get;set;}

    public String availabilityCheckMessage {get;set;}
    public Boolean availabilityCheckReturned {get;set;}

    public Integer availableNumbers {get;set;}

    public String region {get;set;}
    public List<SelectOption> regions {get;set;}

    public String rateCenter {get;set;}
    public List<SelectOption> rateCenters {get;set;}

    public String sortingOption {get;set;}
    public List<SelectOption> options {get;set;}

    //for DEMO purposes only. we will get this list from RingCentral Integration
    public List<String> bcRateCenters = new List<String> {'Greenwood - 236',
'Midway - 236',
'Naramata - 236',
'Okanagan Falls - 236',
'Oliver - 236',
'Rock Creek - 236',
'Summerland - 236',
'Ashcroft - 236',
'Aspen Park - 236',
'Avola - 236',
'Barriere - 236',
'Cache Creek - 236',
'Clearwater - 236',
'Lillooet - 236',
'Little Fort - 236',
'Logan Lake - 236',
'Lytton - 236',
'Spences Bridge - 236',
'Vavenby - 236',
'Westwold - 236',
'Victoria - 236',
'Aldergrove - 236',
'Boston Bar - 236',
'Britannia Beach - 236',
'Newton - 236',
'West Vancouver - 236'};

    //constructor
    public OC_BusinessConnectCheckServices() {
        this.portabilityCheckFailure = null;
        this.portabilityCheckReturned = false;

        this.availableNumbers = 0;
		this.availabilityCheckReturned = false;

        this.options = getSortingOptions();
        this.sortingOption = SORTBYCITY;  //default radio button selection

        this.regions = getRegions();
        
        this.rateCenters = new List<SelectOption>();
        updateRateCenters();
    }

    public List<SelectOption> getRegions() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption(AB,AB));
        options.add(new SelectOption(BC,BC));
        options.add(new SelectOption(MB,MB));
        options.add(new SelectOption(NB,NB));
        options.add(new SelectOption(NL,NL));
        options.add(new SelectOption(NWT,NWT));
        options.add(new SelectOption(NS,NS));
        options.add(new SelectOption(NU,NU));
        options.add(new SelectOption(ONT,ONT));
        options.add(new SelectOption(PEI,PEI));
        options.add(new SelectOption(QC,QC));
        options.add(new SelectOption(SK,SK));
        options.add(new SelectOption(YT,YT));
        return options;
    }

    public List<SelectOption> getSortingOptions() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption(SORTBYCITY,SORTBYCITY));
        options.add(new SelectOption(SORTBYNPA,SORTBYNPA));
        return options;
    }

    public void initBusinessConnectPortability() {
        String phoneNumber = Apexpages.currentPage().getParameters().get('phoneNumber');
        
        //TODO: implement portability check and return true/false

        this.portabilityCheckFailure = false;
        this.portabilityCheckReturned = true;
    }

    public void initAvailabilityCheck() {
        String selectedRegion = Apexpages.currentPage().getParameters().get('region');
        String selectedRateCenter = Apexpages.currentPage().getParameters().get('rateCenter');

        //TODO: implement availability check and return number of TN's

        system.debug('initAvailabilityCheck: ' + selectedRegion);
        system.debug('initAvailabilityCheck: ' + selectedRateCenter);

        this.availableNumbers = 99;
        this.availabilityCheckReturned = true;
    }

    //TODO: the follow method will be refactored to call out to 
    //remote service to retrieve (and cache) the list of rate centers for the region
    public void updateRateCenters() {
        this.rateCenters.clear();

        if (String.isBlank(region)) {
            return;
        }
        if (region == AB) {    
        }
        if (region == BC) { 
            //for DEMO purposes only
            bcRateCenters.sort();
            for (String rateCenter : bcRateCenters) {
                this.rateCenters.add(new SelectOption(rateCenter, rateCenter)); 
            }  
        }
        if (region == MB) {
        }
        if (region == NB) {
        }
    }

}