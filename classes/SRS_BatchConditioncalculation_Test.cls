/* Developed By: Hari Neelalaka, IBM
*  Created Date: 18-Dec-2014
*/
@isTest

private Class SRS_BatchConditioncalculation_Test{
    
    static testMethod void testBatchConditioncalculation(){ 
        // creating an account and using assertion for validation
        Account acc = new Account();
        acc.Name = 'test';
        insert acc;
        system.assertEquals(acc.Name, 'test');
        // creating a service request referencing an account created above
        List<Service_Request__c> lstSR=new List<Service_Request__c>();
        Service_Request__c sr = new Service_Request__c();
        sr.Account_Name__c = acc.Id;
        sr.Service_Request_Status__c = 'In Progress';
        RecordType rt = [select id,Name from RecordType where SobjectType='Service_Request__c' and Name='Change' Limit 1];
        sr.recordTypeId=rt.id;
        sr.Condition_Color__c='green';
        sr.FMOEnabled__c=true;
        lstSR.add(sr);
        lstSR.add(new Service_Request__c(Account_Name__c=acc.id,FMOEnabled__c=true));
        insert lstSR;             
        String q = '';
        SRS_BatchConditioncalculation batchObj = new SRS_BatchConditioncalculation(q);
        batchObj.JobFailed='exception';
        batchObj.countFailure=4;
        SMBCare_WebServices__c cs2 = new SMBCare_WebServices__c(); //Custom Setting for password Field
        cs2.Name = 'EmailAddress';
        cs2.Value__c = 'test@test.com';
        insert cs2; 
        // Modify the query here        
        Test.startTest();
        ID batchprocessid = Database.executeBatch(batchObj,1);
        Test.stopTest(); 
    }
}