/**
 * Supports the trac_LnR_Task Detail Visualforce page that displays fields from the Supplemental Activity Data record
 * on the Task (Remark) page layout. The Supplemental Activity Data record is used to store additional Loyalty & Retention
 * fields, as a workaround to the field limit of the Task object.
 *
 * @author Matt Freedman, Traction on Demand
 * @date 2017-01-07
 */
public with sharing class trac_LnR_Task_Detail_Ctrl {

	private final Task taskRecord;
    public Supplemental_Activity_Data__c taskExtendRecord { get; private set; }

    /**
     * Retrieve the Task record from the standard controller and load the Supplemental Activity Data
     * record (if there is one) associated to this Task.
     */
    public trac_LnR_Task_Detail_Ctrl(ApexPages.StandardController stdController) {
        this.taskRecord = (Task)stdController.getRecord();

        getSupplementalRecord();
    }

    /**
     * Retrieve the Supplemental Activity Data record that is associated to this Task record.
     */
    private void getSupplementalRecord() {
        Task t = [SELECT Supplemental_Activity_Data__c FROM Task WHERE Id = :this.taskRecord.Id];

        if (!String.isBlank(t.Supplemental_Activity_Data__c)) {
            this.taskExtendRecord = [SELECT
                                        Callback_Number__c,
                                        Client_Tenure__c,
                                        Comp_port_credit__c,
                                        Comp_rate_plan_cost__c,
                                        Current_TLC__c,
                                        DB_Waived__c,
                                        Dealer_Code__c,
                                        Device_Model__c,
                                        Inbox__c,
                                        Month_Remaining__c,
                                        Service_Phone_Number_Worked_On__c,
                                        Plan_Name_SOC__c,
                                        Plan_Offered_SOC__c,
                                        Rateplan_Change_Fee__c,
                                        Mid_Contract_RPO_Reason__c,
                                        DB_Exception_reason__c,
                                        Renewal_Credit_Amount__c,
                                        Sales_Rep_Code__c,
                                        SFDC_Case_Number__c
                                    FROM Supplemental_Activity_Data__c
                                    WHERE Id = :t.Supplemental_Activity_Data__c];
        }
    }

}