/**
  @author Vlocity Implements GetItemsListInterface
  @version April 27 2016
*/


global with sharing class GetServiceRequestItems implements vlocity_cmt.VlocityOpenInterface{
    private static string CONTRACTOBJ = 'Contracts__c';
    private static string DEALSUBPPORTOBJ = 'Offer_House_Demand__c';
    
  public Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {
  Boolean success = true;
   
  if(methodName == 'getItemsList')
  { 
    getItemsList(inputMap, outMap, options); 
  }
   
  return success; 
  }
   
    private void getItemsList (Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options){
        String objectName;  
        Id contextId = (Id) inputMap.get('contextObjId');
        Schema.SObjectType contextObjType =  Id.valueOf(contextId).getSObjectType();
	   	String contextObjName = contextObjType.getDescribe().getName();
        system.debug('contextObjName::'+ contextObjName);
        List<SObject> items = new List<SObject> ();
        
        if(CONTRACTOBJ.equalsignoreCase(contextObjName)){
            items = [Select Id, Name from Service_Request__c  where Contract__c =:contextId];
        }
        else if(DEALSUBPPORTOBJ.equalsignoreCase(contextObjName)) {
            items = [Select Id, Name from Rate_Plan_Item__c where Deal_Support__c =:contextId];
        }       
        outMap.put('itemsList', items);
               
    } 
}