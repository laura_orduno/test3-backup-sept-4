/* Developed By: Hari Neelalaka, IBM
*  Created Date: 22-Dec-2014
 Updated By: HAdhir Parchure, Tech Mahindra
 Updated Date: 10-Oct-2015
*/
@isTest

private Class SRS_MyOpenSolutions_Controller_Test{
    
    static testMethod void testSRS_MyOpenSolutions_Controller(){ 
        // creating an account
        Account acc = new Account();
        acc.Name = 'test';
        insert acc;
        // creating a service request referencing an account created above
        Service_Request__c sr = new Service_Request__c();
        sr.Account_Name__c = acc.Id;
        insert sr;
        // Insert new record in "Opportunity" object
        Opportunity opp = new Opportunity(AccountId=acc.Id,Name='test',CloseDate=system.today(),StageName='Win Reason');
        Database.insert(opp);
        
        // Instantiate "SRS_MyOpenSolutions_Controller"
        
        SRS_MyOpenSolutions_Controller objCtrl=new SRS_MyOpenSolutions_Controller();
        objCtrl.showRelatedList = true;
        objCtrl.SRId ='abc';
        
        objCtrl.getSortDirection();
        objCtrl.setSortDirection('test');
        objCtrl.sortExpression='LastModifiedDate';
        objCtrl.getsortDirection();
        objCtrl.sortExpression='DD_Scheduled__c';
        objCtrl.getsortDirection();
        objCtrl.sortExpression='';
        objCtrl.getsortDirection();
        objCtrl.SortRelatedListwithColumn();
        objCtrl.getSolutionsList();
        
        SRS_MyOpenSolutions_Controller.Solution objSol=new SRS_MyOpenSolutions_Controller.Solution();
        
        List<SRS_MyOpenSolutions_Controller.Solution> solutionL = new List<SRS_MyOpenSolutions_Controller.Solution>();
        
        //................Sorting by City.................... 
        
        SRS_MyOpenSolutions_Controller.SORT_FIELD = 'city';
        SRS_MyOpenSolutions_Controller.SORT_DIR = 'ASC';
        SRS_MyOpenSolutions_Controller.Solution objSolution=new SRS_MyOpenSolutions_Controller.Solution();
        objSolution.Opp=opp;
        SolutionL.add(objSolution);
        SolutionL.sort();
        
        //................Sorting by Last Modified Date....................
         
        SRS_MyOpenSolutions_Controller.SORT_FIELD = 'LastModifiedDate';
        SRS_MyOpenSolutions_Controller.Solution objSolution1=new SRS_MyOpenSolutions_Controller.Solution();
        objSolution1.Opp=opp;
        SolutionL.add(objSolution1);
        SolutionL.sort();
        
        //................Sorting by DD_Scheduled__c....................
         
    /*    SRS_MyOpenSolutions_Controller.SORT_FIELD = 'DD_Scheduled__c';
        SRS_MyOpenSolutions_Controller.Solution objSolution2=new SRS_MyOpenSolutions_Controller.Solution();
        objSolution2.Opp=opp;
        SolutionL.add(objSolution2);
        SolutionL.sort();
    */    
        //................Sorting by account.Name....................
         
        SRS_MyOpenSolutions_Controller.SORT_FIELD = 'account.Name';
        SRS_MyOpenSolutions_Controller.Solution objSolution3=new SRS_MyOpenSolutions_Controller.Solution();
        objSolution3.Opp=opp;
        SolutionL.add(objSolution3);
        SolutionL.sort(); 
        
        //................Sorting by Condition_Sort_Index__c....................
         
        SRS_MyOpenSolutions_Controller.SORT_FIELD = 'Condition_Sort_Index__c';
        SRS_MyOpenSolutions_Controller.Solution objSolution4=new SRS_MyOpenSolutions_Controller.Solution();
        objSolution4.Opp=opp;
        SolutionL.add(objSolution4);
        SolutionL.sort(); 
     
        //................Sorting by Requested_Date__c....................
         
        SRS_MyOpenSolutions_Controller.SORT_FIELD = 'Requested_Date__c';
        SRS_MyOpenSolutions_Controller.Solution objSolution5=new SRS_MyOpenSolutions_Controller.Solution();
        objSolution5.Opp=opp;
        SolutionL.add(objSolution5);
        SolutionL.sort();
        
        //.....Added by Adhir to cover the sorting of other colums as apart of User story service Request sorting & filtering (BR801647)
        //................Sorting by Order_ID__c....................
         
        SRS_MyOpenSolutions_Controller.SORT_FIELD = 'Solution.Opp.Order_ID__c';
        SRS_MyOpenSolutions_Controller.Solution objSolution6=new SRS_MyOpenSolutions_Controller.Solution();
        objSolution6.Opp=opp;
        SolutionL.add(objSolution6);
        SolutionL.sort();
        
        //................Sorting by Opportunity name....................
         
        SRS_MyOpenSolutions_Controller.SORT_FIELD = 'Solution.Opp.name';
        SRS_MyOpenSolutions_Controller.Solution objSolution7=new SRS_MyOpenSolutions_Controller.Solution();
        objSolution7.Opp=opp;
        SolutionL.add(objSolution7);
        SolutionL.sort();
        
        //................Sorting by Status....................
         
        SRS_MyOpenSolutions_Controller.SORT_FIELD = 'Solution.Opp.Status__c';
        SRS_MyOpenSolutions_Controller.Solution objSolution8=new SRS_MyOpenSolutions_Controller.Solution();
        objSolution8.Opp=opp;
        SolutionL.add(objSolution8);
        SolutionL.sort();
        
        //................Sorting by Opportunity Type....................
         
        SRS_MyOpenSolutions_Controller.SORT_FIELD = 'Solution.Opp.Type';
        SRS_MyOpenSolutions_Controller.Solution objSolution9=new SRS_MyOpenSolutions_Controller.Solution();
        objSolution9.Opp=opp;
        SolutionL.add(objSolution9);
        SolutionL.sort();
        
        //................Sorting by Product Type....................
         
        SRS_MyOpenSolutions_Controller.SORT_FIELD = 'Solution.Opp.Product_Type__c';
        SRS_MyOpenSolutions_Controller.Solution objSolution10=new SRS_MyOpenSolutions_Controller.Solution();
        objSolution10.Opp=opp;
        SolutionL.add(objSolution10);
        SolutionL.sort();
        
        
        //................Sorting by Created Date....................
         
        SRS_MyOpenSolutions_Controller.SORT_FIELD = 'solution.oppCrDate';
        SRS_MyOpenSolutions_Controller.Solution objSolution11=new SRS_MyOpenSolutions_Controller.Solution();
        objSolution11.Opp=opp;
        //objSolution11.oppCrDate = Date.newinstanceopp.createdDate;
        SolutionL.add(objSolution11);
        SolutionL.sort();   
        
        
                
    }
}