global class QuoteExceptionHandler {
    @future
    public static void storeLogFuture(String token, String type, String message, String linenumber, String stacktrace, String userId, String quoteId, String url) {
        storeLog(token, type, message, linenumber, stacktrace, userId, quoteId, url);
    }
    
    public static void storeLog(String token, String type, String message, String linenumber, String stacktrace, String userId, String quoteId, String url) {   
        Quote_Portal_Exception__c qpec = new Quote_Portal_Exception__c();
        qpec.Token__c = token;
        qpec.Type__c = type;
        qpec.Message__c = message;
        qpec.Line_Number__c = linenumber;
        qpec.Stack_Trace__c = stacktrace;
        qpec.URL__c = url;
        try {
            qpec.User__c = (Id)userId; }
        catch (Exception e) {}
        try {
            qpec.Quote__c = (Id)quoteId; }
        catch (exception e) {}
        
        system.debug('\n\nStoring Exception:\n');
        insert qpec;
        system.debug(qpec);
    }
}