/*
    *******************************************************************************************************************************
    Class Name:    OCOM_HybridCPQExt 
    Purpose:       Extends the vlocity managed class vlocity_cmt.CardCanvasController with the init mehtod to 
                   allow navigation from any Omniscript related page in OCOM Order Capture to Select & Configure page        
    Created by:    Wael Elkhawass    21-Aug-2016    OCOM-1207     No previous version
    Modified by:      
    *******************************************************************************************************************************
*/

global class OCOM_HybridCPQExt {

   // public vlocity_cmt.CardCanvasController controllerHybridcpqRef;
    
    // fhong - outside order flow origination
    public Boolean isPartner{
        get{
            String sitePrefix=Site.getPathPrefix();
            system.debug('Site.prefix:'+sitePrefix);
            if(String.isNotBlank(sitePrefix) && sitePrefix.contains('Partner')){
                return true;
            }
            return false;
        }
    }
    public String addressOrigin{get;set;}
    Public Order theOrder{get;set;}
    public OCOM_HybridCPQExt(Object  controller) {
            
     //   this.controllerHybridcpqRef=controller;
    }
    
    public PageReference init() {
                
        String src = ApexPages.currentPage().getParameters().get('src')==null?'':ApexPages.currentPage().getParameters().get('src');
        String rcidStr=ApexPages.currentPage().getParameters().get('rcid');
        
        String original_param_order_id = ApexPages.currentPage().getParameters().get('id')==null?'':ApexPages.currentPage().getParameters().get('id');
        String orderSolutionId= ApexPages.currentPage().getParameters().get('parentOrderId')==null?'':ApexPages.currentPage().getParameters().get('parentOrderId');
        if(String.isNotBlank(orderSolutionId) && String.isNotBlank(original_param_order_id)){
            List<Order> ordList = [SELECT accountid,vlocity_cmt__accountid__c,Id, Service_Address__c, Service_Address__r.Id, Service_Address__r.Service_Account_Id__c,
                    Service_Address__r.Service_Account_Id__r.Id, Service_Address__r.Service_Account_Id__r.ParentId, CustomerAuthorizedBy.Id 
                    FROM Order WHERE Id=: original_param_order_id and parentid__c=:orderSolutionId];
            if(!ordList.isEmpty()){
               theOrder=ordList.get(0);
            }
            
        } else{
            if(String.isNotBlank(orderSolutionId)) {
                List<Order> ordList = [SELECT accountid,vlocity_cmt__accountid__c,Id, Service_Address__c, Service_Address__r.Id, Service_Address__r.Service_Account_Id__c,
                    Service_Address__r.Service_Account_Id__r.Id, Service_Address__r.Service_Account_Id__r.ParentId, CustomerAuthorizedBy.Id 
                    FROM Order WHERE parentid__c=:orderSolutionId limit 1 ];
                if(!ordList.isEmpty()){
                    theOrder=ordList.get(0);
                }
            }
            
            if(String.isNotBlank(original_param_order_id)) {
                List<Order> ordList = [SELECT accountid,vlocity_cmt__accountid__c,Id, Service_Address__c, Service_Address__r.Id, Service_Address__r.Service_Account_Id__c,
                    Service_Address__r.Service_Account_Id__r.Id, Service_Address__r.Service_Account_Id__r.ParentId, CustomerAuthorizedBy.Id 
                    FROM Order WHERE Id=: original_param_order_id ];
                if(!ordList.isEmpty()){
                    theOrder=ordList.get(0);
                }
            }
        }
        
        Boolean hasActiveAsset;
        
        //danish added this on 16/03/2017 for ACD functionality 
        system.debug('original_param_order_id >>>>'+original_param_order_id );
        If(original_param_order_id != null  && !(String.isBlank(original_param_order_id))){
            if(System.currentPageReference().getParameters().get('smbCareAddrId') == null && System.currentPageReference().getParameters().get('contactId') == null){
                    OCOM_ACD_ServiceAddresshelper AcdServiceAddressClass = new OCOM_ACD_ServiceAddresshelper();
                    hasActiveAsset = AcdServiceAddressClass.CheckIfAnylineItemsAreActiveAsset(original_param_order_id);  
            }       
        }
        
        
        if(String.isBlank(rcidStr)){
            rcidStr=theOrder.accountid;
            if(String.isBlank(rcidStr)){
                rcidStr=theOrder.vlocity_cmt__accountid__c;
            }
            ApexPages.currentPage().getParameters().put('rcid',rcidStr);
        }
        
         
        
        // fhong - outside order flow origination
        addressOrigin = System.currentPageReference().getParameters().get('addressOrigin');

        if(src=='os'){ // if the link to hypercpq page is coming from any of the susequent pages managed by OmniScript    
            
            ApexPages.currentPage().getParameters().put('contactId', theOrder.CustomerAuthorizedBy.Id);
            ApexPages.currentPage().getParameters().put('smbCareAddrId', theOrder.Service_Address__r.Id);
            ApexPages.currentPage().getParameters().put('ServiceAcctId', theOrder.Service_Address__r.Service_Account_Id__r.Id);
            ApexPages.currentPage().getParameters().put('ParentAccId', theOrder.Service_Address__r.Service_Account_Id__r.ParentId);
            ApexPages.currentPage().getParameters().put('orderId', original_param_order_id);
        } 
        
        return null; 
    }
        
    @RemoteAction
    Global static void UpdatePrivisioningStatusOnDisconnect(string OrderItemId) {
    
        List<OrderItem> oi = new List<OrderItem>();
        
        oi = [Select id,vlocity_cmt__ProvisioningStatus__c,Notes_Remarks__c from OrderItem where id =:OrderItemId];
        if(oi.size()>0) {
            oi[0].vlocity_cmt__ProvisioningStatus__c = 'Disconnected';
            oi[0].Notes_Remarks__c = 'notes';
        }
        
        update oi;
        
    } 
    

}