@isTest
private class OCOM_ContractUtil_Test {

public static void insertFieldMapper(){

        	String nameSpaceprefix = 'vlocity_cmt__';
		 Vlocity_cmt__CustomFieldMap__c customFieldMap1 = new Vlocity_cmt__CustomFieldMap__c(	Name='OrderItem>ContractLineItem__c0',
																	Vlocity_cmt__SourceFieldName__c = 'ListPrice',
																	Vlocity_cmt__DestinationFieldName__c =nameSpaceprefix+'UnitPrice__c',
																	Vlocity_cmt__DestinationSObjectType__c=nameSpaceprefix+'ContractLineItem__c',
																	vlocity_cmt__SourceFieldType__c='currency',																	
																	Vlocity_cmt__SourceSObjectType__c='OrderItem');
         Vlocity_cmt__CustomFieldMap__c customFieldMap2 = new Vlocity_cmt__CustomFieldMap__c(	Name='OrderItem>ContractLineItem__c1',
																	Vlocity_cmt__SourceFieldName__c = 'Quantity',
																	Vlocity_cmt__DestinationFieldName__c =nameSpaceprefix+'Quantity__c',
																	Vlocity_cmt__DestinationSObjectType__c=nameSpaceprefix+'ContractLineItem__c',
																	Vlocity_cmt__SourceFieldType__c='double',
																	Vlocity_cmt__SourceSObjectType__c='OrderItem');
		 Vlocity_cmt__CustomFieldMap__c customFieldMap3 = new Vlocity_cmt__CustomFieldMap__c(	Name='OrderItem>ContractLineItem__c2',
																	Vlocity_cmt__SourceFieldName__c = 'vlocity_cmt__LineNumber__c',
																	Vlocity_cmt__DestinationFieldName__c = 'vlocity_cmt__LineNumber__c',
																	Vlocity_cmt__SourceFieldType__c='string',
																	Vlocity_cmt__DestinationSObjectType__c=nameSpaceprefix+'ContractLineItem__c',
																	Vlocity_cmt__SourceSObjectType__c='OrderItem');
		 Vlocity_cmt__CustomFieldMap__c customFieldMap4 = new Vlocity_cmt__CustomFieldMap__c(	Name='OrderItem>ContractLineItem__c3',
																	Vlocity_cmt__SourceFieldName__c = 'PricebookEntryId',
																	Vlocity_cmt__DestinationFieldName__c =nameSpaceprefix+'PricebookEntryId__c',
																	Vlocity_cmt__SourceFieldSObjectType__c='PricebookEntry',
																	Vlocity_cmt__SourceFieldType__c='reference',
																	Vlocity_cmt__DestinationSObjectType__c=nameSpaceprefix+'ContractLineItem__c',
																	Vlocity_cmt__SourceSObjectType__c='OrderItem');	
		  Vlocity_cmt__CustomFieldMap__c customFieldMap5 = new Vlocity_cmt__CustomFieldMap__c(Name='OrderItem>ContractLineItem__c4',
																	Vlocity_cmt__SourceFieldName__c = 'vlocity_cmt__RecurringTotal__c',
																	Vlocity_cmt__DestinationFieldName__c ='vlocity_cmt__RecurringTotal__c',
																	
																	Vlocity_cmt__SourceFieldType__c='currency',
																	Vlocity_cmt__DestinationSObjectType__c=nameSpaceprefix+'ContractLineItem__c',
																	Vlocity_cmt__SourceSObjectType__c='OrderItem');	
		 Vlocity_cmt__CustomFieldMap__c customFieldMap6 = new Vlocity_cmt__CustomFieldMap__c(Name='Order>Contract0',
																	Vlocity_cmt__SourceFieldName__c = 'AccountId',
																	Vlocity_cmt__DestinationFieldName__c ='AccountId',
																	Vlocity_cmt__DestinationSObjectType__c='Contract',
																	Vlocity_cmt__SourceFieldType__c='reference',
																	Vlocity_cmt__SourceSObjectType__c='Order');
		 Vlocity_cmt__CustomFieldMap__c customFieldMap7 = new Vlocity_cmt__CustomFieldMap__c(Name='Order>Contract1',
																	Vlocity_cmt__SourceFieldName__c = 'Id',
																	Vlocity_cmt__DestinationFieldName__c =nameSpaceprefix+'OrderId__c',
																	Vlocity_cmt__DestinationSObjectType__c='Contract',
																	Vlocity_cmt__SourceFieldType__c='reference',
																		Vlocity_cmt__SourceSObjectType__c='Order');
		List<Vlocity_cmt__CustomFieldMap__c> listCustomFieldMaps = new List<Vlocity_cmt__CustomFieldMap__c> ();
		listCustomFieldMaps.add(customFieldMap1);
		listCustomFieldMaps.add(customFieldMap2);
		listCustomFieldMaps.add(customFieldMap3);
		listCustomFieldMaps.add(customFieldMap4);
		listCustomFieldMaps.add(customFieldMap5);
		listCustomFieldMaps.add(customFieldMap6);
		listCustomFieldMaps.add(customFieldMap7);
		insert listCustomFieldMaps;
	}


@isTest static void test_CreateNewContractException() {
		Map<String,Object> TestinputMap = new Map<String,Object>();
		Map<String,Object> TestoutMap = new Map<String,Object>();
		String orderId= OrdrTestDataFactory.singleMethodForOrderId();
        Order ordObj=[select Account.Credit_Profile__c,AccountId, Credit_Check_Not_Required__c,RequestedDate__C,status,Rush__c,Ban__c,Shipping_Address__c,orderMgmtId__c FROM Order where id=:orderId];
        ordObj.orderMgmtId__c='OS1234567890';
        ordObj.status='Not Submitted';
        ordObj.Rush__c=true;
        ordObj.Ban__c='12345';
        ordObj.Shipping_Address__c='test address';
        ordObj.Credit_Check_Not_Required__c=false;
        Date day1 = Date.today();
        Date todayPlus = day1.addDays(4);
        ordObj.RequestedDate__C=todayPlus;
        update ordObj;
        //insertFieldMapper();

        system.debug('Account ID______' + ordobj.AccountId);
        Test.startTest();
        TestinputMap.put('orderId',ordObj.id);
		//TestinputMap.put('isSuccess',true);
		TestinputMap.put('creditStatus','Approved');
		TestinputMap.put('areContractable',True);
    	 TestinputMap.put('isMoveOutOrder',false);
		OCOM_ContractUtil.createNewContract(TestinputMap,TestoutMap);
		Boolean isSuccess = (Boolean)TestoutMap.get('isSuccess');
		System.assertEquals(false, isSuccess);	
		Test.stopTest();
	}

	@isTest static void test_CreateNewContractCreditCheckPenging() {
		Map<String,Object> TestinputMap = new Map<String,Object>();
		Map<String,Object> TestoutMap = new Map<String,Object>();
		String orderId= OrdrTestDataFactory.singleMethodForOrderId();
        Order ordObj=[select Account.Credit_Profile__c,AccountId, Credit_Check_Not_Required__c,RequestedDate__C,status,Rush__c,Ban__c,Shipping_Address__c,orderMgmtId__c FROM Order where id=:orderId];
        ordObj.orderMgmtId__c='OS1234567890';
        ordObj.status='Not Submitted';
        ordObj.Rush__c=true;
        ordObj.Ban__c='12345';
        ordObj.Shipping_Address__c='test address';
        ordObj.Credit_Check_Not_Required__c=false;
        Date day1 = Date.today();
        Date todayPlus = day1.addDays(4);
        ordObj.RequestedDate__C=todayPlus;
        update ordObj;
        //insertFieldMapper();
        
        system.debug('Account ID______' + ordobj.AccountId);
        Test.startTest();
        TestinputMap.put('orderId',ordObj.id);
		//TestinputMap.put('isSuccess',true);
		TestinputMap.put('creditStatus','Pending');
        TestinputMap.put('areContractable',True);
        TestinputMap.put('isMoveOutOrder',false);
		//TestinputMap.put('areContractable',True);
		OCOM_ContractUtil.createNewContract(TestinputMap,TestoutMap);

		Boolean isSuccess = (Boolean)TestoutMap.get('isSuccess');
        String contractStatus = (String)TestoutMap.get('contractStatus'); 
		System.assertEquals(true, isSuccess);	
		System.assertEquals(Label.OCOM_Contract_Credit_Check_Pending, contractStatus);
		Test.stopTest();
	}
	
	@isTest 
    private static void test_CreateNewContract() {

		Map<String,Object> TestinputMap = new Map<String,Object>();
		Map<String,Object> TestoutMap = new Map<String,Object>();
	
		
		String orderId= OrdrTestDataFactory.singleMethodForOrderId();
        Order ordObj=[select Account.Credit_Profile__c,AccountId, Credit_Check_Not_Required__c,RequestedDate__C,status,Rush__c,Ban__c,Shipping_Address__c,orderMgmtId__c FROM Order where id=:orderId];
        ordObj.orderMgmtId__c='OS1234567890';
        ordObj.status='Not Submitted';
        ordObj.Rush__c=true;
        ordObj.Ban__c='12345';
        ordObj.Shipping_Address__c='test address';
        ordObj.Credit_Check_Not_Required__c=false;
        Date day1 = Date.today();
        Date todayPlus = day1.addDays(4);
        ordObj.RequestedDate__C=todayPlus;
        update ordObj;
        insertFieldMapper();

        system.debug('Account ID______' + ordobj.AccountId);
        Test.startTest();
        TestinputMap.put('orderId',ordObj.id);
		//TestinputMap.put('isSuccess',true);
		TestinputMap.put('creditStatus','Approved');
		TestinputMap.put('areContractable',True);
        TestinputMap.put('isMoveOutOrder',false);
		OCOM_ContractUtil.createNewContract(TestinputMap,TestoutMap);
		Boolean isSuccess = (Boolean)TestoutMap.get('isSuccess');
		String contractID = (String)TestoutMap.get('contractID');
		String contractNumber = (String)TestoutMap.get('contractNumber');
		//System.assertEquals(true, isSuccess);
		//System.assertNotEquals(null,contractID);
		//System.assertEquals(Label.OCOM_Contract_Created,contractNumber);
		Test.stopTest();
	}
	
	@isTest static void test_existingContractTest() {

		Map<String,Object> TestinputMap = new Map<String,Object>();
		Map<String,Object> TestoutMap = new Map<String,Object>();
		TestDataHelper.testContractCreation1();
		String orderId= OrdrTestDataFactory.singleMethodForOrderId();
        Order ordObj=[select Account.Credit_Profile__c,AccountId, Credit_Check_Not_Required__c,RequestedDate__C,status,Rush__c,Ban__c,Shipping_Address__c,orderMgmtId__c FROM Order where id=:orderId];
        ordObj.orderMgmtId__c='OS1234567890';
        ordObj.status='Not Submitted';
        ordObj.Rush__c=true;
        ordObj.Ban__c='12345';
        ordObj.Shipping_Address__c='test address';
        ordObj.Credit_Check_Not_Required__c=false;
        Date day1 = Date.today();
        Date todayPlus = day1.addDays(4);
        ordObj.RequestedDate__C=todayPlus;
        update ordObj;
        insertFieldMapper();

        TestDataHelper.testContractObj.vlocity_cmt__OrderId__c = ordObj.id;
        update TestDataHelper.testContractObj;

         Test.startTest();
        TestinputMap.put('orderId',ordObj.id);
		//TestinputMap.put('isSuccess',true);
		TestinputMap.put('creditStatus','Approved');
		TestinputMap.put('areContractable',True);
          TestinputMap.put('isMoveOutOrder',false);
		OCOM_ContractUtil.createNewContract(TestinputMap,TestoutMap);
		Boolean isSuccess = (Boolean)TestoutMap.get('isSuccess');
		String contractID = (String)TestoutMap.get('contractID');
		String contractStatus = (String)TestoutMap.get('contractStatus');
		String contractNumber = (String)TestoutMap.get('contractNumber');
		System.assertEquals(true, isSuccess);
		System.assertNotEquals(null,contractID);
		System.assertNotEquals(null,contractNumber);
		Test.stopTest();

	}

	@isTest static void test_ContractSentTest() {

		Map<String,Object> TestinputMap = new Map<String,Object>();
		Map<String,Object> TestoutMap = new Map<String,Object>();
		TestDataHelper.testContractCreation1();
		String orderId= OrdrTestDataFactory.singleMethodForOrderId();
        Order ordObj=[select Account.Credit_Profile__c,AccountId, Credit_Check_Not_Required__c,RequestedDate__C,status,Rush__c,Ban__c,Shipping_Address__c,orderMgmtId__c FROM Order where id=:orderId];
        ordObj.orderMgmtId__c='OS1234567890';
        ordObj.status='Not Submitted';
        ordObj.Rush__c=true;
        ordObj.Ban__c='12345';
        ordObj.Shipping_Address__c='test address';
        ordObj.Credit_Check_Not_Required__c=false;
        Date day1 = Date.today();
        Date todayPlus = day1.addDays(4);
        ordObj.RequestedDate__C=todayPlus;
        update ordObj;
        insertFieldMapper();

        TestDataHelper.testContractObj.vlocity_cmt__OrderId__c = ordObj.id;
        TestDataHelper.testContractObj.Status='Contract Sent';
        update TestDataHelper.testContractObj;

        Test.startTest();
        TestinputMap.put('orderId',ordObj.id);
		//TestinputMap.put('isSuccess',true);
		TestinputMap.put('creditStatus','Approved');
		TestinputMap.put('areContractable',True);
          TestinputMap.put('isMoveOutOrder',false);
		OCOM_ContractUtil.createNewContract(TestinputMap,TestoutMap);
		Boolean isSuccess = (Boolean)TestoutMap.get('isSuccess');
		String contractID = (String)TestoutMap.get('contractID');
		String contractStatus = (String)TestoutMap.get('contractStatus');
		String contractNumber = (String)TestoutMap.get('contractNumber');
		System.assertEquals(true, isSuccess);
		System.assertNotEquals(null,contractID);
		System.assertNotEquals(null,contractNumber);
		Test.stopTest();
	}

	@isTest static void test_AmendContractsWithEnvelope() {

		Map<String,Object> TestinputMap = new Map<String,Object>();
		Map<String,Object> TestoutMap = new Map<String,Object>();
		TestDataHelper.testContractCreation1();
		String orderId= OrdrTestDataFactory.singleMethodForOrderId();
        Order ordObj=[select Account.Credit_Profile__c,AccountId, Credit_Check_Not_Required__c,RequestedDate__C,status,Rush__c,Ban__c,Shipping_Address__c,orderMgmtId__c FROM Order where id=:orderId];
        ordObj.orderMgmtId__c='OS1234567890';
        ordObj.status='Not Submitted';
        ordObj.Rush__c=true;
        ordObj.Ban__c='12345';
        ordObj.Shipping_Address__c='test address';
        ordObj.Credit_Check_Not_Required__c=false;
        Date day1 = Date.today();
        Date todayPlus = day1.addDays(4);
        ordObj.RequestedDate__C=todayPlus;
        update ordObj;
        insertFieldMapper();

        TestDataHelper.testContractObj.vlocity_cmt__OrderId__c = ordObj.id;
        TestDataHelper.testContractObj.Status='Contract Sent';
        update TestDataHelper.testContractObj;

        Test.startTest();
        TestinputMap.put('orderId',ordObj.id);
		//TestinputMap.put('isSuccess',true);
		TestinputMap.put('creditStatus','Approved');
		TestinputMap.put('areContractable',True);
          TestinputMap.put('isMoveOutOrder',false);
		OCOM_ContractUtil.createNewContract(TestinputMap,TestoutMap);
		Boolean isSuccess = (Boolean)TestoutMap.get('isSuccess');
		String contractID = (String)TestoutMap.get('contractID');
		String contractStatus = (String)TestoutMap.get('contractStatus');
		String contractNumber = (String)TestoutMap.get('contractNumber');
		System.assertEquals(true, isSuccess);
		System.assertNotEquals(null,contractID);
		System.assertNotEquals(null,contractNumber);

		OCOM_ContractUtil.SetValueFromTestMethod = 'SUCCESS';	

		TestDataHelper.testContractObj.Status='Contract Sent';
        update TestDataHelper.testContractObj;

		//OCOM_ContractUtil.contractableOrderCheck(TestinputMap,TestoutMap);
		Map<String,Object> output = OCOM_ContractUtil.AmendOrder(ordObj,'Amend');
		System.assertEquals('SUCCESS',(String)output.get('amendContractResponse')); 
		Test.stopTest(); 
	}

	@isTest static void test_AmendContractsWithOutEnvelope() {

		Map<String,Object> TestinputMap = new Map<String,Object>();
		Map<String,Object> TestoutMap = new Map<String,Object>();
		TestDataHelper.testContractCreation1();
		String orderId= OrdrTestDataFactory.singleMethodForOrderId();
        Order ordObj=[select Account.Credit_Profile__c,AccountId, Credit_Check_Not_Required__c,RequestedDate__C,status,Rush__c,Ban__c,Shipping_Address__c,orderMgmtId__c FROM Order where id=:orderId];
        ordObj.orderMgmtId__c='OS1234567890';
        ordObj.status='Not Submitted';
        ordObj.Rush__c=true;
        ordObj.Ban__c='12345';
        ordObj.Shipping_Address__c='test address';
        ordObj.Credit_Check_Not_Required__c=false;
        Date day1 = Date.today();
        Date todayPlus = day1.addDays(4);
        ordObj.RequestedDate__C=todayPlus;
        update ordObj;
        insertFieldMapper();

        TestDataHelper.testContractObj.vlocity_cmt__OrderId__c = ordObj.id;
        TestDataHelper.testContractObj.Status='Contract Sent';
        update TestDataHelper.testContractObj;

        TestinputMap.put('orderId',ordObj.id);
		//TestinputMap.put('isSuccess',true);
		TestinputMap.put('creditStatus','Approved');
		TestinputMap.put('areContractable',True);
          TestinputMap.put('isMoveOutOrder',false);
		OCOM_ContractUtil.createNewContract(TestinputMap,TestoutMap);
		Boolean isSuccess = (Boolean)TestoutMap.get('isSuccess');
		String contractID = (String)TestoutMap.get('contractID');
		String contractStatus = (String)TestoutMap.get('contractStatus');
		String contractNumber = (String)TestoutMap.get('contractNumber');
		System.assertEquals(true, isSuccess);
		System.assertNotEquals(null,contractID);
		System.assertNotEquals(null,contractNumber);

		OCOM_ContractUtil.SetValueFromTestMethod = 'SUCCESS';	

		TestDataHelper.testContractObj.Status='Draft';
		TestDataHelper.testContractObj.No_Amend__c= false;
        update TestDataHelper.testContractObj;
        Test.startTest();
		//OCOM_ContractUtil.contractableOrderCheck(TestinputMap,TestoutMap);
		Map<String,Object> output = OCOM_ContractUtil.AmendOrder(ordObj,'Amend');
		System.assertEquals('SUCCESS',(String)output.get('amendContractResponse')); 
		Test.stopTest(); 
	}
	
	@isTest static void test_CancelContractsWithOutEnvelope() {

		Map<String,Object> TestinputMap = new Map<String,Object>();
		Map<String,Object> TestoutMap = new Map<String,Object>();
		TestDataHelper.testContractCreation1();
		String orderId= OrdrTestDataFactory.singleMethodForOrderId();
        Order ordObj=[select Account.Credit_Profile__c,AccountId, Credit_Check_Not_Required__c,RequestedDate__C,status,Rush__c,Ban__c,Shipping_Address__c,orderMgmtId__c FROM Order where id=:orderId];
        ordObj.orderMgmtId__c='OS1234567890';
        ordObj.status='Not Submitted';
        ordObj.Rush__c=true;
        ordObj.Ban__c='12345';
        ordObj.Shipping_Address__c='test address';
        ordObj.Credit_Check_Not_Required__c=false;
        Date day1 = Date.today();
        Date todayPlus = day1.addDays(4);
        ordObj.RequestedDate__C=todayPlus;
        update ordObj;
        insertFieldMapper();

        TestDataHelper.testContractObj.vlocity_cmt__OrderId__c = ordObj.id;
        TestDataHelper.testContractObj.Status='Contract Sent';
        update TestDataHelper.testContractObj;

        TestinputMap.put('orderId',ordObj.id);
		//TestinputMap.put('isSuccess',true);
		TestinputMap.put('creditStatus','Approved');
		TestinputMap.put('areContractable',True);
          TestinputMap.put('isMoveOutOrder',false);
		OCOM_ContractUtil.createNewContract(TestinputMap,TestoutMap);
		Boolean isSuccess = (Boolean)TestoutMap.get('isSuccess');
		String contractID = (String)TestoutMap.get('contractID');
		String contractStatus = (String)TestoutMap.get('contractStatus');
		String contractNumber = (String)TestoutMap.get('contractNumber');
		System.assertEquals(true, isSuccess);
		System.assertNotEquals(null,contractID);
		System.assertNotEquals(null,contractNumber);

		OCOM_ContractUtil.SetValueFromTestMethod = 'SUCCESS';	

		TestDataHelper.testContractObj.Status='Draft';
		TestDataHelper.testContractObj.No_Amend__c= true;
        update TestDataHelper.testContractObj;
        Test.startTest();
		//OCOM_ContractUtil.contractableOrderCheck(TestinputMap,TestoutMap);
		Map<String,Object> output = OCOM_ContractUtil.AmendOrder(ordObj,'Cancel');
		OCOM_ContractUtil.getRootLineNumber('0001.0005');
		System.assertEquals('SUCCESS',(String)output.get('amendContractResponse'));
		Test.stopTest(); 

	}

@isTest static void test_AmendContractsException() {

		Map<String,Object> TestinputMap = new Map<String,Object>();
		Map<String,Object> TestoutMap = new Map<String,Object>();
		TestDataHelper.testContractCreation1();
			

		TestDataHelper.testContractObj.Status='Draft';
		TestDataHelper.testContractObj.No_Amend__c= true;
        update TestDataHelper.testContractObj;
        list<contract> templst = new list<contract>();
        Test.startTest();
        vlocity_cmt.FlowStaticMap.FlowMap.put('forContractTestCoverageNoEnvelopException',true);
		templst.add(TestDataHelper.testContractObj);
		Map<String,Object> output  = OCOM_ContractUtil.updateContractStatusNoEnvelope(templst);
		
		Map<String,Object> output2 = OCOM_ContractUtil.UpdateContractStatusWithEnvelope(templst);

		System.assertEquals('FAILURE',(String)output.get('amendContractResponse')); 
		System.assertEquals('FAILURE',(String)output2.get('amendContractResponse')); 
		Test.stopTest();
	}


	@isTest static void test_documentTemplateTest() {
		Set_Doc_Template__c temp = new Set_Doc_Template__c();
		temp.name='OCOM Business Anywhere';
		temp.DocumentTemplateName__c ='OCOM Business Anywhere';
		insert temp;
		String templateName = OCOM_ContractUtil.getTemplateName();
		System.assertEquals('OCOM Business Anywhere',templateName);
	}

	@isTest
    private static void getContractableOfferList(){
        String orderId=OrdrTestDataFactory.singleMethodForOrderId();
        List<OrderItem> oiList=[select id,vlocity_cmt__ParentItemId__c,vlocity_cmt__JSONAttribute__c from orderitem where orderid=:orderId];
        String parentId=null;
        for(OrderItem oiInstance:oiList){
            if(String.isBlank(parentId)){
                parentId=oiInstance.id;
            } else {
                oiInstance.vlocity_cmt__ParentItemId__c=parentId;
            }
        }
        update oiList;
        Map<id,List<OrderItem>> ordertoOLIMap=new Map<id,List<OrderItem>>();
        ordertoOLIMap.put(orderId,oiList);
        Map<String,Object> inputMap=new Map<String,Object>();
        inputMap.put('ordertoOLIMap',ordertoOLIMap);
        Test.startTest();
        OCOM_ContractUtil.getContractableOfferList(inputMap);
        Test.stopTest();
    }
	
}