/**
 * An apex page controller that supports self registration of users in communities that allow self registration
 */
public without sharing class MBRCommunitiesSelfRegController {

    public String contactId {get; set;}
    public String token {get; set;}
    public String community {get; set;}
    public String LanguageType {get;set;}
   //public String prefLanguage{get; set;}
    public String roleType {get; set;}
    public transient boolean genericError {get; set;}
    public transient Integer genericMessagesCount {get; set;}
    public transient List<String> genericMessages {get; set;}
    public transient String recoverPW {get; set;}
    public transient String LanguageErr {get; set;}
    public transient String invalidNickname {get; set;}
    public transient String userNameTaken {get; set;}
    public transient String weakPassword {get; set;}
    public transient String passwordNotMatch {get; set;}
    public String firstName {get; set;}
    public String lastName {get; set;}
    public transient String[] errors {get; private set;}
    public String firstLastName {
        get {
            system.debug('firstLastName : '+firstName+' '+lastName);
            return firstName+' '+lastName;
        }
        set;
    }
    public String contactAccountName {
        get {
            system.debug('contactAccountName : '+contactAccountName );
            return contactAccountName;
        } 
        set;
    }
    public String email {get; set;}
    public String accountNumber {get; set;}
    public String pin {get; set;}
    public String password {get; set {password = value == null ? value : value.trim(); } }
    public String confirmPassword {get; set { confirmPassword = value == null ? value : value.trim(); } }
    public String communityNickname {get; set { communityNickname = value == null ? value : value.trim(); } }
    private String masterPin;
    private String supportPin;
    private MBRAccountUtil acctUtil;
    private Contact contactInScope = null;
    
    public MBRCommunitiesSelfRegController() {
        try {
            recoverPW='';
            contactId = ApexPages.CurrentPage().getParameters().get('id');
            token = ApexPages.CurrentPage().getParameters().get('key');
            community = ApexPages.CurrentPage().getParameters().get('community');
            roleType = ApexPages.CurrentPage().getParameters().get('role');
            System.debug('contactId: ' + contactId);
            if(!Userinfo.getUserType().equals('Guest'))
            {
                recoverPW = label.mbrLogOutFirst;
            }
        } catch (Exception e) {
            System.debug( e );
        }    
    }
    
    public PageReference init() {  
System.debug('CONFIRM REG: ' + contactId);        
//        if(false) {
        if(contactId != null && contactid != '') {
            List<contact> cons = [select Authenticated__c, account.Wireline_PIN__c, account.Wireless_PIN__c, rcid__c, account.name, accountid, firstname, lastname, email, token__c from contact where id = :contactId and CanAllowPortalSelfReg = true limit 1];
System.debug('cons: ' + cons);        
            if(cons.size()<1 || token != cons[0].token__c) {
//            if(false) {
                system.debug('urlToken: '+token);
                system.debug('cons.size(): '+cons.size());
                if(cons.size()>0) {
                    system.debug('contact token: '+cons[0].token__c);
                }
                recoverPW = label.mbrInvalidIdToken;
                return null;
            }
            else {
                contactInScope = cons[0];
            }
        }
        else {
            return new PageReference( '/CustomLogin'); 
            //return new PageReference( '/VITILcareOverview'); 
        }

        List<User> userCheck = [Select UserType from User where ContactId = :contactId and UserType = 'CspLitePortal' and IsActive = true];
        System.debug('userCheck: ' + userCheck); 
        if(userCheck.size()>0){
            system.debug('usercheck usertype:'+userCheck[0].UserType);
            if(community == 'vitilcare'){
            	recoverPW = label.VITILcareAlreadyRegistered;
    		}
            else{
            	recoverPW = label.mbrAlreadyRegistered;
            }
            system.debug('null:userchecksize');
            return null;        
        }
        
        contactAccountName = contactInScope.account.name;
        masterPin = contactInScope.account.Wireline_PIN__c;
        supportPin = contactInScope.account.Wireless_PIN__c;
       // acctUtil = new MBRAccountUtil(contactInScope.rcid__c);
        
        firstName = contactInScope.firstName;
        lastName = contactInScope.lastName;
        email = contactInScope.email;
        return null;
    }
    
    private boolean isValidPassword() {
        return password == confirmPassword;
    }

    /*
    private boolean isMasterPin() {
        return pin == masterPin;
    }
    
    private boolean isSupportPin() {
        return pin == supportPin;
    }
    
    private boolean validAccountNumber() {
        Set<String> acctnums = acctUtil.getAccountNumbers();
        for(String acc : acctnums) {
            system.debug(acc);
        }
        return acctnums.contains(accountNumber.toLowerCase().trim());
    }
    */

    public PageReference registerUser() {
        LanguageErr = '';
         if(community == 'vitilcare'){
        if(LanguageType == '' || LanguageType == null){
            LanguageErr = 'Please choose a preferred language';
            //ApexPages.Message msg = new ApexPages.message(ApexPages.severity.ERROR,'Please at least one value');
          // ApexPages.Message msg = new ApexPages.message(ApexPages.severity.ERROR,'Please choose a preferred language');
          //  ApexPages.addMessage(msg);
         
            return null; 
        }
        } 
        System.debug('entered registerUser');
        genericError = true;
        genericMessages = new List<String>();
        genericMessagesCount = 0;
        userNameTaken = '';
        invalidNickname = '';
        weakPassword = '';
        passwordNotMatch = '';
        recoverPW = '';
        /*if(isMasterPin() || isSupportPin() || validAccountNumber()){
            contactInScope.Authenticated__c = true;
        } else {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Info, 'Invalid PIN or Account Number provided.');
            ApexPages.addMessage(msg);
            return null;
        }*/
        // it's okay if password is null - we'll send the user a random password in that case

        String userName = email;
        // check if username is taken
        List<User> users = [SELECT Id, Username FROM User WHERE UserType = 'CSPLitePortal' AND Username = :userName LIMIT 1];
        if (users.size() > 0) {
            userNameTaken = Label.MBRUserNameTaken;
            genericError = false;
            //genericMessages.add( Label.MBRUserNameTaken );
            //genericMessagesCount++;
            system.debug('userNameTaken: '+userNameTaken);
            return null;
        }

        if(!MBRChangePasswordCtlr.isStrongPassword(password)) {
            weakPassword = label.mbrWeakPasswordError;
            genericError = false;
            system.debug('weakPassword: '+weakPassword);
            system.debug('isStrongPassword: '+password+':'+confirmPassword);
            return null;
        }

        if (!isValidPassword()) {
            passwordNotMatch = label.mbrPasswordDoNotMatchError;
            genericError = false;            
            system.debug('isvalidpassword: '+password+':'+confirmPassword);
            return null;
        }    

        String roleEnum = null; // To be filled in by customer.
        String accountId = contactInScope.accountid; // To be filled in by customer.
        
        

        User u = new User();
        u.ContactId = contactInScope.id;
        u.Username = userName;
        u.Email = email;
        u.FirstName = firstName;
        u.LastName = lastName;
         u.LanguageLocaleKey = LanguageType;
        if(roleType != '' && roleType != null){
            u.Customer_Portal_Role__c = roleType;
        }
        //u.CommunityNickname = communityNickname;
        u.CommunityNickname = u.ContactId + '.' + DateTime.now().getTime();
        if(!String.isBlank(community)){
			u.Customer_Portal_Name__c = community;            
        }
        else{
			u.Customer_Portal_Name__c = 'mbr';                        
        }
    
        System.debug('u: ' + u);
        
        String userId;
        try {
            userId = Site.createPortalUser(u, accountId, password);
            System.debug('userId: ' + userId);
        } catch (Exception e) {
            System.debug('Site.createPortalUser exception: ' + e.getMessage());
        }

        if (!ApexPages.getMessages().isEmpty()) {
            // Hack to get around a weird problem where pageMessages component
            // does not render messages generated by Site.changePassword method.
            errors = new String[]{};
            for (ApexPages.Message msg : ApexPages.getMessages()) {
                errors.add(msg.getDetail());
            }
        }
        if(errors!=null){
            /*
            if(errors[0].contains('That nickname already exists. Enter a unique nickname.')){
                invalidNickname = label.mbrNicknameInUse;
                genericError = false;
            }
            */
            return null;
        }

        update contactInScope;
        
        if (userId != null) { 
            if (password != null && password.length() > 1) {
                system.debug('site.login');
                if(community == 'vitilcare'){
                	return Site.login(userName, password, '/VITILcareOverview');
                }
                else{
                	return Site.login(userName, password, '/overview');                    
                }
            }
            else {
                PageReference page = System.Page.MBRCommunitiesSelfRegConfirm;          
                
                if(community == 'vitilcare'){
                    page = System.Page.VITILcareCommunitiesSelfRegConfirm;          
                }
                
                page.setRedirect(true);
                return page;
            }
        }
        system.debug('userId:'+userId);
        return null;
    }
 public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>(); 
     options.add(new SelectOption('en_US',label.VitilcarePortalAccess_EnglishLabel)); 
        options.add(new SelectOption('fr',label.VITILcarePortalAccess_FrenchLabel)); 
        return options; 
    }
                   
    public String getLanguageType() {
        return LanguageType;
    }
                    
    public void setLanguageType(String LanguageType) { this.LanguageType = LanguageType; }
    

}