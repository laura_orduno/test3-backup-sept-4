@isTest
private class NAAS_SLA_button_extensionTest {
    
    @isTest(SeeAllData=true) 
    // test scenario where  account is missing
    private static void SLAbuttonTest() {
        Schema.DescribeSObjectResult d = Schema.SObjectType.Account; 
          Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
          Schema.RecordTypeInfo rtByName =  rtMapByName.get('CAN');
          system.debug(' id'+rtByName.getRecordTypeId());
          Id recTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Billing').getRecordTypeId();
          Id recSerTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
          Id recRCIDTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId();
        
        
          Account RCIDacc = new Account(name='debs',recordtypeid=recRCIDTypeId);
          Account acc = new Account(name='Billing account',BAN_CAN__c='draft',recordtypeid=recTypeId,Billing_System_ID__c = '101');
          Account RCIDacc2 = new Account(name='debs',recordtypeid=recRCIDTypeId);
          
          Account seracc = new Account(name='123 street',recordtypeid=recSerTypeId,parentid=RCIDacc.id);
          
          
          List<Account> acclist= new List<Account>();          
          acclist.add(RCIDacc);
          acclist.add(acc);
          acclist.add(seracc );
          insert acclist;
          
          
          vlocity_cmt__Catalog__c testCatalog = new vlocity_cmt__Catalog__c();
          testCatalog.Name = 'NAAS';
          insert testCatalog;
          
         /*smbcare_address__c address= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N5',FMS_Address_ID__c='987651',account__c=seracc.id,service_account_id__c=seracc.id); 
         insert address;
          */
           // First, set up test price book entries.
        // Insert a test product.
        Product2 prod = new Product2(Name = 'Test NaaS product', 
            Family = 'NaaS',Catalog__c = testCatalog.Id);
        insert prod;
         System.assertEquals(prod.name, 'Test NaaS product');
        // Get standard price book ID.
        // This is available irrespective of the state of SeeAllData.
        Id pricebookId = Test.getStandardPricebookId();

        // 1. Insert a price book entry for the standard price book.
        // Standard price book entries require the standard price book ID we got earlier.
        Asset testassetRec = new Asset(name='Asset1',Product2Id =prod.Id,accountId=RCIDacc.id,vlocity_cmt__BillingAccountId__c=acc.Id,orderMgmtId__c='10156456494');
        insert testassetRec;
        
        ApexPages.Standardcontroller stdCtrl = new ApexPages.Standardcontroller(RCIDacc);
        NAAS_SLA_button_extension slabuttonclass= new NAAS_SLA_button_extension(stdCtrl);
        slabuttonclass.renderSlabutton();
        
           
        
    }
    @isTest(SeeAllData=true) 
    // test scenario where  account is missing
    private static void SLAbuttonTest2() {
    Schema.DescribeSObjectResult d = Schema.SObjectType.Account; 
          Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
          Schema.RecordTypeInfo rtByName =  rtMapByName.get('CAN');
          system.debug(' id'+rtByName.getRecordTypeId());
          Id recTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Billing').getRecordTypeId();
          Id recSerTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
          Id recRCIDTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId();
        
        
          Account RCIDacc = new Account(name='debs',recordtypeid=recRCIDTypeId);
          Account acc = new Account(name='Billing account',BAN_CAN__c='draft',recordtypeid=recTypeId,Billing_System_ID__c = '101');
          Account RCIDacc2 = new Account(name='debs',recordtypeid=recRCIDTypeId);
          
          Account seracc = new Account(name='123 street',recordtypeid=recSerTypeId,parentid=RCIDacc.id);
          
          
          List<Account> acclist= new List<Account>();          
          acclist.add(RCIDacc);
          acclist.add(acc);
          acclist.add(seracc );
          insert acclist;
          
          
          vlocity_cmt__Catalog__c testCatalog = new vlocity_cmt__Catalog__c();
          testCatalog.Name = 'Voice';
          insert testCatalog;
          
         /*smbcare_address__c address= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N5',FMS_Address_ID__c='987651',account__c=seracc.id,service_account_id__c=seracc.id); 
         insert address;
          */
           // First, set up test price book entries.
        // Insert a test product.
        Product2 prod = new Product2(Name = 'Test NaaS product', 
            Family = 'NaaS',Catalog__c = testCatalog.Id);
        insert prod;
         System.assertEquals(prod.name, 'Test NaaS product');
        // Get standard price book ID.
        // This is available irrespective of the state of SeeAllData.
        Id pricebookId = Test.getStandardPricebookId();

        // 1. Insert a price book entry for the standard price book.
        // Standard price book entries require the standard price book ID we got earlier.
        Asset testassetRec = new Asset(name='Asset1',Product2Id =prod.Id,accountId=RCIDacc.id,vlocity_cmt__BillingAccountId__c=acc.Id,orderMgmtId__c='10156456494');
        insert testassetRec;
        
        ApexPages.Standardcontroller stdCtrl = new ApexPages.Standardcontroller(RCIDacc);
        NAAS_SLA_button_extension slabuttonclass= new NAAS_SLA_button_extension(stdCtrl);
        slabuttonclass.renderSlabutton();
        
             
        
    }
}