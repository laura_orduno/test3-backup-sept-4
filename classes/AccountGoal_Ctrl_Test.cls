/**
* @author       Masoud Azampour
* @date         2013-12-10
* @Description  Tests AccountGoal_Ctrl
*/
@isTest(SeeAllData=false)
public with sharing class AccountGoal_Ctrl_Test {


	public static testMethod void testMyController(){
		Account a = new Account();
		a.Name = 'Telus';
		a.Phone = '1-800-888-9999';
		Insert a;

		Contact c1 = new Contact();
		c1.AccountId = a.Id;
		c1.FirstName = 'Masoud';
		c1.LastName = 'Az';
		Insert c1;

		Contact c2 = new Contact();
		c2.AccountId = a.Id;	
		c2.FirstName = 'Dan';
		c2.LastName = 'D';
		Insert c2;


		ESP__c esp = new ESP__c();
		esp.Account__c = a.Id;
		esp.Name = 'TELUS ESP';
		Insert esp;



		PageReference pageRef = Page.AccountGoal;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('espId', esp.Id);

        AccountGoal_Ctrl ctrl = new AccountGoal_Ctrl();
        ctrl.init();

        Goal__c g = new Goal__c();
        g.Account_Goal__c = 'My Goal';
        g.ESP__c = esp.Id;
        Insert g;
        

        List<Objectives__c> objsToInsert = new List<Objectives__c>();
        Objectives__c o1 = new Objectives__c();
        o1.Goal__c = g.Id;
        o1.Objective__c = 'O1';
        objsToInsert.add(o1);

        Objectives__c o2 = new Objectives__c();
        o2.Goal__c = g.Id;
        o2.Objective__c = 'O2';
        objsToInsert.add(o2);

        Insert objsToInsert;

        List<Task> tasksToInsert = new List<Task>();
        Task t1 = new Task();
        t1.Status = 'Not Started';
        t1.Action_Item_custom__c = 't1 item';
        t1.Objective_Action_Resource__c = 't1 res';
        t1.OwnerId = UserInfo.getUserId();
        t1.ActivityDate = Date.newInstance(2014, 4, 21);
        t1.WhatId = o1.Id;
        tasksToInsert.add(t1);
        
        Task t2 = new Task();
        t2.Status = 'In Progress';
        t2.Action_Item_custom__c = 't2 item';
        t2.Objective_Action_Resource__c = 't2 res';
        t2.OwnerId = UserInfo.getUserId();
        t2.ActivityDate = Date.newInstance(2014, 5, 21);
        t2.WhatId = o1.Id;     
        tasksToInsert.add(t2);   
        Insert tasksToInsert;

        ctrl.init();

        ctrl.saveGoal();
        ctrl.cancelGoal();
        
        ctrl.objectiveId = o1.Id;
        ctrl.saveObjective();
        ctrl.newObjGoalId = g.Id;
        ctrl.addNewObjective();

        ctrl.sortBy = 'Status';
        ctrl.sortDirection = 'desc';
        ctrl.getObjectivesAndTasks();

        ctrl.sortBy = 'OwnerName';
        ctrl.sortDirection = 'desc';
        ctrl.getObjectivesAndTasks();

        ctrl.sortBy = 'Date';
        ctrl.sortDirection = 'desc';
        ctrl.getObjectivesAndTasks();

        ctrl.taskId = t1.Id;
        ctrl.saveTask();
        ctrl.newTaskObjectiveId = o2.Id;
        ctrl.addNewTask();
        ctrl.taskId = t1.Id;
        ctrl.deleteTask();

        ctrl.objectiveId = o1.Id;
        ctrl.deleteObjective();
        ctrl.deleteGoal();


	}
	
}