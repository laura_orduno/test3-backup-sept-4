@isTest(seeAllData = true)
private class CloudProjectPortalSignoffInfo_Test {

	testMethod static void testCloudProjectPortalSignoffInfo1() {
        insertTestExternalUserPortalSettings();
        
        SMET_Project__c a = new SMET_Project__c(Name = 'TestAccount');
        insert a;
        
        SMET_Requirement__c c2 = new SMET_Requirement__c(
        	Related_SMET_Project__c = a.id,
        	Name = 'Miller',
        	Order__c = 1
        );
        insert c2;
        
        Cloud_Project_Sign_Off__c t = new Cloud_Project_Sign_Off__c(
        	Asignee_Name__c = 'test',
        	Assignee_Email__c = 'test@test.com',
        	Cloud_Project_Key_Deliverable__c = c2.id,
        	status__c = 'test'
        );
        insert t;
        t = [SELECT token__c, Id from Cloud_Project_Sign_Off__c where id=:t.id];
                
        Note nt = new Note(
        	parentid = t.id,
        	title = 'test@test.com'
        );
        insert nt;
        
        PageReference pageRef = Page.CloudProjectPortalSignOffInformation;
        Test.setCurrentPage(pageRef);
        system.debug('t.id:'+t.id);
        system.debug('t.token:'+t.token__c);
        ApexPages.currentPage().getParameters().put('oid', t.id);
        ApexPages.currentPage().getParameters().put('tok', t.token__c);
        test.starttest();
        CloudProjectPortalSignoffInfo c = new CloudProjectPortalSignoffInfo();
      
        c.onLoad();
        c.getAttachmentList();
        c.getNoteList();
        c.getNewNote();
        c.addComment();
        c.getSubject();
        c.getRelatedTo();
        c.getRole();
        c.getRelatedToName();
        c.getRelatedToEmail();
        c.getStatus();
        c.getCreatedBy();
        c.onComplete();
		test.stoptest();        
    }
    
    @isTest
    private static void insertTestExternalUserPortalSettings() {
    	// if org already contains custom settings, remove for test
        delete [SELECT Id from CloudProjectPortal__c];
    	
    	CloudProjectPortal__c eups = new CloudProjectPortal__c(
    		Base_URL__c = 'http://login.salesforce.com',
    		HMAC_Digest_Key__c = '/eYPmJ0eXTb+8R/X7YE9Skv6Ly8tfChkR0gdWt9ngXg=',
    		Portal_URL__c = 'http://telus.force.com/externalservice'
    	);
    	
    	insert eups;
    }

}