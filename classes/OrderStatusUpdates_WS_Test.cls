/* Developed By: Aditya Jamwal, IBM
* Created Date: 14-Jan-2015
*/
@isTest

private  class OrderStatusUpdates_WS_Test {
    
     static testMethod void OrderStatusUpdates_WS_Test() {
         
         Test.startTest();
         SRS_Test_Data_Utility  testDate= new SRS_Test_Data_Utility();
         testDate.CreateDataFor_OrderStatusUpdates_WS_Test();
         OrderStatusUpdates_WS CRDBpush = new OrderStatusUpdates_WS();
         OrderStatusUpdates_WS.GenerateSuccess();
         OrderStatusUpdates_WS.GenerateError('There is an error.');
         OrderStatusUpdates_ComplexTypes.OrderStatusUpdate Order = new OrderStatusUpdates_ComplexTypes.OrderStatusUpdate();
         OrderStatusUpdates_ComplexTypes.LineItem Li = new OrderStatusUpdates_ComplexTypes.LineItem();
		     OrderStatusUpdates_ComplexTypes.DesignAssessmentCompleteDate dac= new OrderStatusUpdates_ComplexTypes.DesignAssessmentCompleteDate();
            dac.DACScheduleDate= date.valueof('2014-12-28');
            dac.DACActualDate=null;
            OrderStatusUpdates_ComplexTypes.DueDate dd = new OrderStatusUpdates_ComplexTypes.DueDate();
            dd.DDScheduleDate=date.valueof('2014-03-04');
            dd.DDActualDate=null;
            OrderStatusUpdates_ComplexTypes.EngineeringReadyDate erd = new OrderStatusUpdates_ComplexTypes.EngineeringReadyDate();
            erd.ERDScheduleDate=date.valueof('2014-03-04');
            erd.ERDActualDate=null;
            OrderStatusUpdates_ComplexTypes.PlantReadyDate prd = new OrderStatusUpdates_ComplexTypes.PlantReadyDate();
            prd.PRDScheduleDate=date.valueof('2014-03-04');
            prd.PRDActualDate=null;
            OrderStatusUpdates_ComplexTypes.PlantTestDate ptd = new OrderStatusUpdates_ComplexTypes.PlantTestDate();
            ptd.PTDScheduleDate=date.valueof('2014-03-04');
            ptd.PTDActualDate=null;
            OrderStatusUpdates_ComplexTypes.ReadyToDate rtd = new OrderStatusUpdates_ComplexTypes.ReadyToDate();
            rtd.RTIScheduleDate=date.valueof('2014-03-04');
            rtd.RTIActualDate=null;
            Li.LegacyOrderID='I250352SEED';
            Li.Sequence='02';
            Li.IssueDate=String.valueof(system.today());
            Li.BillingSystem='CRIS3-BC';
            Li.FoxOrderID='12312345';
            Li.FoxJEOPCode='hrss';
            Li.CRDBJEOPCode='';
            Li.CRIS3HoldCode='';
            Li.CRIS3OrderStatus='';
			Li.DACDate=dac;
            Li.DDate=dd;
            Li.ERDate=erd;
            Li.PRDate=prd;
            Li.PTDate=ptd;
            Li.RTDate=rtd;			
            List<OrderStatusUpdates_ComplexTypes.LineItem> lolist = new List<OrderStatusUpdates_ComplexTypes.LineItem>();
            lolist.add(Li);
            Order.LineItemList =lolist;
          OrderStatusUpdates_WS.UpdateOrderStatus(Order);
          OrderStatusUpdates_ComplexTypes.LineItem L2 = new OrderStatusUpdates_ComplexTypes.LineItem();
		     OrderStatusUpdates_ComplexTypes.DesignAssessmentCompleteDate dac2= new OrderStatusUpdates_ComplexTypes.DesignAssessmentCompleteDate();
            dac2.DACScheduleDate= date.valueof('2014-12-28');
            dac2.DACActualDate=null;
            OrderStatusUpdates_ComplexTypes.DueDate dd2 = new OrderStatusUpdates_ComplexTypes.DueDate();
            dd2.DDScheduleDate=date.valueof('2014-03-04');
            dd2.DDActualDate=null;
            OrderStatusUpdates_ComplexTypes.EngineeringReadyDate erd2 = new OrderStatusUpdates_ComplexTypes.EngineeringReadyDate();
            erd2.ERDScheduleDate=date.valueof('2014-03-04');
            erd2.ERDActualDate=null;
            OrderStatusUpdates_ComplexTypes.PlantReadyDate prd2 = new OrderStatusUpdates_ComplexTypes.PlantReadyDate();
            prd2.PRDScheduleDate=date.valueof('2014-03-04');
            prd2.PRDActualDate=null;
            OrderStatusUpdates_ComplexTypes.PlantTestDate ptd2 = new OrderStatusUpdates_ComplexTypes.PlantTestDate();
            ptd2.PTDScheduleDate=date.valueof('2014-03-04');
            ptd2.PTDActualDate=null;
            OrderStatusUpdates_ComplexTypes.ReadyToDate rtd2 = new OrderStatusUpdates_ComplexTypes.ReadyToDate();
            rtd2.RTIScheduleDate=date.valueof('2014-03-04');
            rtd2.RTIActualDate=null;
            L2.LegacyOrderID='I250352SaED';
            L2.Sequence='02';
            L2.IssueDate=String.valueof(system.today());
            L2.BillingSystem='CRIS3-BC';
            L2.FoxOrderID='12312345';
            L2.FoxJEOPCode='hrss';
            L2.CRDBJEOPCode='';
            L2.CRIS3HoldCode='';
            L2.CRIS3OrderStatus='';
			L2.DACDate=dac2;
            L2.DDate=dd2;
            L2.ERDate=erd2;
            L2.PRDate=prd2;
            L2.PTDate=ptd2;
            L2.RTDate=rtd2;			
            List<OrderStatusUpdates_ComplexTypes.LineItem> lolist2 = new List<OrderStatusUpdates_ComplexTypes.LineItem>();
            lolist2.add(L2);
           Order.LineItemList =lolist2;
           OrderStatusUpdates_WS.UpdateOrderStatus(Order);
         Test.stopTest();
     }

}