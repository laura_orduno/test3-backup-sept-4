/*
 * General utilities relating to handling Apex Pages
 *
 * @author: Grant Adamson, Traction On Demand 
 */

public without sharing class trac_PageUtils {
	private static final String EMAIL_PATTERN = 
		'^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@'
		+ '[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$';
	
	
	public static Boolean isParamNull(String paramName) {
		return ApexPages.currentPage().getParameters().get(paramName) == null;
	}
	
	public static Boolean isParamNotNull(String paramName) {
  	return !isParamNull(paramName);
  }
    
	public static String getParam(String paramName) {
		return ApexPages.currentPage().getParameters().get(paramName);
	}
	
	public static void setParam(String paramName, String paramValue) {
		ApexPages.currentPage().getParameters().put(paramName,paramValue);
	}
	
	public static Boolean isValidEmail(String email) {
		return (email != null ? Pattern.matches(EMAIL_PATTERN, email) : false);
	}
	
	public static void addError(String msg) {
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,msg));
	}
	
	public static void addWarning(String msg) {
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,msg));
	}
	
	public static void addInfo(String msg) {
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,msg));
	}
	
}