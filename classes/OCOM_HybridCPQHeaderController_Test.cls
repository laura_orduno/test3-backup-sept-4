@isTest
public class OCOM_HybridCPQHeaderController_Test {

//PageReference opptyPage = new PageReference('/apex/ocom_test_hybridcpq');
   // Test.setCurrentPage(opptyPage);
 
   public static testmethod void testHybridCPQHeaderController() {
       System.debug('MethodOne>>>>>');
        Account acc = new Account(Name='Test Account');
        insert acc;
       
        
        Account acc1 = [select name , Id from Account where name='TEST ACCOUNT' limit 1];
        SMBCare_Address__c address = new SMBCare_Address__c(Account__c=acc1.Id,Street_Address__c='adasd',City__c='asdasd', FMS_Address_ID__c='sfsdf',Status__c= 'Valid',isTVAvailable__c=true,Forbone__c=true,Maximum_Speed__c='50'); 
        insert address;
       
       Product2 product4 = new Product2(Name='Collect Call Deny ',ProductCode='PROD-004', Description='This is a test JSON Product 1 Generator',isActive = true,orderMgmtId__c='9143863905013553513' ,OCOM_Bookable__c='Yes',Sellable__c = true);
       product4.vlocity_cmt__JSONAttribute__c  = '{"TELUSCHAR": [{"$$AttributeDefinitionEnd$$": null, "attributeRunTimeInfo": {"selectedItem":{"value":"Overline","displayText":"Overline","id":"9145085938413814481"}",values": [{"value": "Overline", "displayText": "Overline", "id": "9145085938413814481"}], "default": [{"value": "Overline", "displayText": "Overline", "id": "9145085938413814481"}], "uiDisplayType": "Dropdown", "dataType": "Picklist"}, "categorydisplaysequence__c": 1, "customconfiguitemplate__c": null, "attributecloneable__c": true, "valuedescription__c": null, "valuedatatype__c": "Picklist", "value__c": "Overline", "uidisplaytype__c": "Dropdown", "rulemessage__c": null, "isrequired__c": true, "id": "a7P220000008lnIEAQ", "querylabel__c": null, "isquerydriven__c": false, "isreadonly__c": true, "querycode__c": null, "objecttype__c": "Product2", "valueinnumber__c": null, "ishidden__c": true, "isconfigurable__c": true, "hasrule__c": false, "formatmask__c": null, "displaysequence__c": null, "attributedisplayname__c": "Line Type", "isactive__c": true, "attributefilterable__c": false, "attributedisplaysequence__c": "2", "attributeconfigurable__c": true, "attributeuniquecode__c": "ATTRIBUTE-549", "categoryname__c": "Characteristics", "categorycode__c": "TELUSCHAR", "attributecategoryid__c": "a7Q220000009vghEAA", "attributeid__c": "a7R220000000HqcEAE", "objectid__c": "01t22000000UXFUAA4", "$$AttributeDefinitionStart$$": null }] }';
        product4.IncludeQuoteContract__c= true;
       insert product4;

        
        Pricebook2 standardBook =  new Pricebook2(Id = Test.getStandardPricebookId(), Name = 'TestPricebook', IsActive = true); 
         PricebookEntry sbe4 = new PricebookEntry(Pricebook2Id = standardBook.Id,Product2Id = product4.Id, UnitPrice = 2, Vlocity_cmt__RecurringPrice__c = 17, IsActive = true, UseStandardPrice = false);
        insert sbe4;
        Pricebook2 customPriceBook =  new Pricebook2( Name = 'CustomPricebook', IsActive = true);
        insert customPriceBook;        
        PricebookEntry pbe4 = new PricebookEntry(Pricebook2Id = customPriceBook.Id,Product2Id = product4.Id, UnitPrice = 2, Vlocity_cmt__RecurringPrice__c = 17, IsActive = true, UseStandardPrice = false);
        insert pbe4;
        
        Order ord = new Order(AccountId = acc1.Id, Status='Draft',EffectiveDate=System.Today(),service_address__c=address.id,Type = 'Move',Service_Address_Text__c = address.AddressText__c,Pricebook2Id =  customPriceBook.ID );
        ord.orderMgmtId_Status__c =  OrdrConstants.OI_COMPLETED_STATUS;
       insert ord;
        
        OrderItem  orderItem1 = new OrderItem(OrderId=Ord.Id, PricebookEntryId = pbe4.Id,vlocity_cmt__Product2Id__c=product4.Id, vlocity_cmt__LineNumber__c = '0001', Quantity = 1, UnitPrice = 10, vlocity_cmt__ProvisioningStatus__c = 'New');
        orderItem1.vlocity_cmt__JSONAttribute__c  = '{"TELUSCHAR": [{"$$AttributeDefinitionEnd$$": null, "attributeRunTimeInfo": {"selectedItem": {"value": "Overline1", "displayText": "Overline1", "id": "9145085938413814481"}, ",values": [{"value": "Overline", "displayText": "Overline", "id": "9145085938413814481"}], "default": [{"value": "Overline", "displayText": "Overline", "id": "9145085938413814481"}], "uiDisplayType": "Dropdown", "dataType": "Picklist"}, "categorydisplaysequence__c": 1, "customconfiguitemplate__c": null, "attributecloneable__c": true, "valuedescription__c": null, "valuedatatype__c": "Picklist", "value__c": "Overline", "uidisplaytype__c": "Dropdown", "rulemessage__c": null, "isrequired__c": true, "id": "a7P220000008lnIEAQ", "querylabel__c": null, "isquerydriven__c": false, "isreadonly__c": true, "querycode__c": null, "objecttype__c": "Product2", "valueinnumber__c": null, "ishidden__c": true, "isconfigurable__c": true, "hasrule__c": false, "formatmask__c": null, "displaysequence__c": null, "attributedisplayname__c": "Line Type", "isactive__c": true, "attributefilterable__c": false, "attributedisplaysequence__c": "2", "attributeconfigurable__c": true, "attributeuniquecode__c": "ATTRIBUTE-549", "categoryname__c": "Characteristics", "categorycode__c": "TELUSCHAR", "attributecategoryid__c": "a7Q220000009vghEAA", "attributeid__c": "a7R220000000HqcEAE", "objectid__c": "01t22000000UXFUAA4", "$$AttributeDefinitionStart$$": null }] }';
        insert orderItem1;
        
        Order order = [Select Id from Order where AccountId = :acc1.Id]; 
        Test.setCurrentPageReference(new PageReference('Page.ocom_test_hybridcpq')); 
        System.currentPageReference().getParameters().put('id', ord.Id);
       // System.currentPageReference().getParameters().put('smbCareAddrId', address.Id);
        OCOM_HybridCPQHeaderController Ctrl = new OCOM_HybridCPQHeaderController();
       // OCOM_HybridCPQHeaderController.callRemoteMethodForDefaultlanguage(ord.id);
        string a = Ctrl.genericAccountName;
       // System.assertEquals(a,'TEST ACCOUNT');
        string b = Ctrl.genericAddress;
        string c = Ctrl.orderNumber;
        Boolean displayComponents = Ctrl.displayComponents;
       Boolean displayViewPdfBtn = Ctrl.displayViewPdfBtn;
        String type = Ctrl.Type;
        Boolean srvcAddrssNotValid = Ctrl.srvcAddrssNotValid;
        String tvAvailabilityInfo = Ctrl.tvAvailabilityInfo;
        String forborneInfo = Ctrl.forborneInfo;
        String MaxSpeed = Ctrl.MaxSpeed;
        Boolean maxSpdCopper = Ctrl.maxSpdCopper;
       string moveoutAddress = Ctrl.moveOutAddress;
       Boolean isILEC = Ctrl.isILEC;
        Ctrl.cancelOrder();
       
        /*System.assertEquals(false, Ctrl.srvcAddrssNotValid);
        System.assertEquals('TV Not Available', Ctrl.tvAvailabilityInfo);
        System.assertEquals('Non-Forborne', Ctrl.forborneInfo);
        System.assertEquals('', Ctrl.MaxSpeed);
        System.assertEquals(true, Ctrl.maxSpdCopper);*/
        
   }
   
    public static testmethod void tesnegativetHybridCPQHeaderController() {
        System.debug('MethodTwo>>>>');
        Account acc = new Account(name='Test Account');
        insert acc;
        
        Account acc1 = [select name , Id from Account where name='Test Account' limit 1];
        SMBCare_Address__c address = new SMBCare_Address__c(Account__c=acc1.Id,Street_Address__c='adasd',City__c='asdasd', FMS_Address_ID__c='sfsdf',Status__c= 'Valid',isTVAvailable__c=true,Forbone__c=true,Maximum_Speed__c='50'); 
        insert address;
        
        contact cont= new contact(firstname='abc',lastname='xyz',Language_Preference__c = 'English');
        insert cont;
             
        Product2 product4 = new Product2(Name='Collect Call Deny ',ProductCode='PROD-004', Description='This is a test JSON Product 1 Generator',isActive = true,orderMgmtId__c='9143863905013553513' ,OCOM_Bookable__c='Yes',Sellable__c = true);
        product4.vlocity_cmt__JSONAttribute__c  = '{"TELUSCHAR": [{"$$AttributeDefinitionEnd$$": null, "attributeRunTimeInfo": {"selectedItem":{"value":"Overline","displayText":"Overline","id":"9145085938413814481"}",values": [{"value": "Overline", "displayText": "Overline", "id": "9145085938413814481"}], "default": [{"value": "Overline", "displayText": "Overline", "id": "9145085938413814481"}], "uiDisplayType": "Dropdown", "dataType": "Picklist"}, "categorydisplaysequence__c": 1, "customconfiguitemplate__c": null, "attributecloneable__c": true, "valuedescription__c": null, "valuedatatype__c": "Picklist", "value__c": "Overline", "uidisplaytype__c": "Dropdown", "rulemessage__c": null, "isrequired__c": true, "id": "a7P220000008lnIEAQ", "querylabel__c": null, "isquerydriven__c": false, "isreadonly__c": true, "querycode__c": null, "objecttype__c": "Product2", "valueinnumber__c": null, "ishidden__c": true, "isconfigurable__c": true, "hasrule__c": false, "formatmask__c": null, "displaysequence__c": null, "attributedisplayname__c": "Line Type", "isactive__c": true, "attributefilterable__c": false, "attributedisplaysequence__c": "2", "attributeconfigurable__c": true, "attributeuniquecode__c": "ATTRIBUTE-549", "categoryname__c": "Characteristics", "categorycode__c": "TELUSCHAR", "attributecategoryid__c": "a7Q220000009vghEAA", "attributeid__c": "a7R220000000HqcEAE", "objectid__c": "01t22000000UXFUAA4", "$$AttributeDefinitionStart$$": null }] }';
        insert product4;
 
        
        Pricebook2 standardBook =  new Pricebook2(Id = Test.getStandardPricebookId(), Name = 'TestPricebook', IsActive = true); 
         PricebookEntry sbe4 = new PricebookEntry(Pricebook2Id = standardBook.Id,Product2Id = product4.Id, UnitPrice = 2, Vlocity_cmt__RecurringPrice__c = 17, IsActive = true, UseStandardPrice = false);
        insert sbe4;
        Pricebook2 customPriceBook =  new Pricebook2( Name = 'CustomPricebook', IsActive = true);
        insert customPriceBook;        
        PricebookEntry pbe4 = new PricebookEntry(Pricebook2Id = customPriceBook.Id,Product2Id = product4.Id, UnitPrice = 2, Vlocity_cmt__RecurringPrice__c = 17, IsActive = true, UseStandardPrice = false);
        insert pbe4;
        Order ord = new Order(AccountId = acc1.Id, Status='Draft',EffectiveDate=System.Today(),service_address__c=address.id,Pricebook2Id =  customPriceBook.ID);
        ord.CustomerAuthorizedByID = cont.id;
        ord.CustomerAuthorizedBy = cont;
        ord.CustomerAuthorizedBy.Language_Preference__c = 'English';
        insert ord;
        
        OrderItem  orderItem1 = new OrderItem(OrderId=Ord.Id, PricebookEntryId = pbe4.Id,vlocity_cmt__Product2Id__c=product4.Id, vlocity_cmt__LineNumber__c = '0001', Quantity = 1, UnitPrice = 10, vlocity_cmt__ProvisioningStatus__c = 'New');
        orderItem1.vlocity_cmt__JSONAttribute__c  = '{"TELUSCHAR": [{"$$AttributeDefinitionEnd$$": null, "attributeRunTimeInfo": {"selectedItem": {"value": "Overline1", "displayText": "Overline1", "id": "9145085938413814481"}, ",values": [{"value": "Overline", "displayText": "Overline", "id": "9145085938413814481"}], "default": [{"value": "Overline", "displayText": "Overline", "id": "9145085938413814481"}], "uiDisplayType": "Dropdown", "dataType": "Picklist"}, "categorydisplaysequence__c": 1, "customconfiguitemplate__c": null, "attributecloneable__c": true, "valuedescription__c": null, "valuedatatype__c": "Picklist", "value__c": "Overline", "uidisplaytype__c": "Dropdown", "rulemessage__c": null, "isrequired__c": true, "id": "a7P220000008lnIEAQ", "querylabel__c": null, "isquerydriven__c": false, "isreadonly__c": true, "querycode__c": null, "objecttype__c": "Product2", "valueinnumber__c": null, "ishidden__c": true, "isconfigurable__c": true, "hasrule__c": false, "formatmask__c": null, "displaysequence__c": null, "attributedisplayname__c": "Line Type", "isactive__c": true, "attributefilterable__c": false, "attributedisplaysequence__c": "2", "attributeconfigurable__c": true, "attributeuniquecode__c": "ATTRIBUTE-549", "categoryname__c": "Characteristics", "categorycode__c": "TELUSCHAR", "attributecategoryid__c": "a7Q220000009vghEAA", "attributeid__c": "a7R220000000HqcEAE", "objectid__c": "01t22000000UXFUAA4", "$$AttributeDefinitionStart$$": null }] }';
        insert orderItem1;

        
       
        Order order = [Select Id from Order where AccountId = :acc1.Id]; 
       
        Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.ocom_test_hybridcpq')); 
        //System.currentPageReference().getParameters().put('id', ord.Id);
        System.currentPageReference().getParameters().put('smbCareAddrId', address.Id);
        OCOM_HybridCPQHeaderController Ctrl = new OCOM_HybridCPQHeaderController();
        string a = Ctrl.genericAccountName;
        string b = Ctrl.genericAddress;
        string moveoutAddress = Ctrl.moveOutAddress;
        Boolean isILEC = Ctrl.isILEC;
        String type = Ctrl.Type;
        Boolean displayComponents = Ctrl.displayComponents;
        Boolean displayViewPdfBtn = Ctrl.displayViewPdfBtn;
        Boolean srvcAddrssNotValid = Ctrl.srvcAddrssNotValid;
        String tvAvailabilityInfo = Ctrl.tvAvailabilityInfo;
        String forborneInfo = Ctrl.forborneInfo;
        String MaxSpeed = Ctrl.MaxSpeed;
        Boolean maxSpdCopper = Ctrl.maxSpdCopper;
        ctrl.cancelOrder();
       OCOM_HybridCPQHeaderController.callRemoteMethodForDefaultlanguage(ord.Id);
       String lang = OCOM_HybridCPQHeaderController.lang;
       OCOM_HybridCPQHeaderController.setChoosenLanguageOnOrder(order.Id ,lang);
       string tst1 = ctrl.workRequestRecordTypeId;
       string tst2 = ctrl.accountWorkRequestJson;
       string tst3 = ctrl.opportunityWorkRequestJson;
       string tst4 = ctrl.orderWorkRequestJson;
       Test.StopTest();
       
     /*  System.assertEquals(null, Ctrl.srvcAddrssNotValid);
        System.assertEquals(null, Ctrl.tvAvailabilityInfo);
        System.assertEquals(null, Ctrl.forborneInfo);
        System.assertEquals(null, Ctrl.MaxSpeed);
        System.assertEquals(null, Ctrl.maxSpdCopper);
        System.assertEquals(null, Ctrl.displayViewPdfBtn);*/
         
   }

}