global with sharing class AddressValidationUtility {
	
	//Validate Methods
    //Validate Methods
    //Validate Methods

    global static ValidateAddressResponse validate(Address address) {
	
		// initialize callout
		EnterpriseAddressValidationService_CP webserv = new EnterpriseAddressValidationService_CP();
		EnterpriseAddressValidationService_CP.EnterpriseAddressValidationServicePort servPort = new EnterpriseAddressValidationService_CP.EnterpriseAddressValidationServicePort();
		CustomerCommon_CP.Address requestValidateAddress = new CustomerCommon_CP.Address();
		
		// set basic authentication parameters
		String authString = SMBCare_WebServices__c.getInstance('username').Value__c + ':' + SMBCare_WebServices__c.getInstance('password').Value__c;
		String encodedAuthString = EncodingUtil.base64Encode(Blob.valueOf(authString));
		servPort.inputHttpHeaders_x = new Map<String, String>();
		servPort.inputHttpHeaders_x.put('Authorization','Basic ' + encodedAuthString);
		
		// set web service parameters
		String lineLayout = SMBCare_WebServices__c.getInstance('line_layout').Value__c;
		String addrLayout = SMBCare_WebServices__c.getInstance('address_layout').Value__c;
		
		requestValidateAddress.renderedAddress = new String[] {address.addressLine1};
		requestValidateAddress.municipalityName = address.city;
		requestValidateAddress.provinceStateCode = address.provState;
		requestValidateAddress.postalZipCode = address.postalCode;
    	requestValidateAddress.countryCode = 'CAN';
    	
    	System.debug('*****address: ' + requestValidateAddress);
    	System.debug('*****lineLayout: ' + lineLayout);
    	System.debug('*****addrLayout: ' + addrLayout);
    	
    	EnterpriseAddressValidationServiceReq_CP.verificationResultType response;
    	
    	try
    	{    	
    		response = servPort.verifyCanadianPostalAddress(lineLayout, requestValidateAddress, 'VERBOSE', addrLayout);
    	}
    	catch (CalloutException ex)
    	{
    		System.debug('*****CalloutException: ' + ex);
    		
    		throw new AddressValidationException('The Canada Post validation service is unavailable. Please try again later.');
    		
    	}
    	
    	System.debug('*****response: ' + response);
    	
    	return processResponse(response);
    }

	/* This method processes the response from the Canada Post web service callout
     *
    */
    private static ValidateAddressResponse processResponse(EnterpriseAddressValidationServiceReq_CP.verificationResultType response)
    {
    	if (response == null)
    	{
    		throw new AddressValidationException('Response is null.');
    	}
    	
    	ValidateAddressResponse result = new ValidateAddressResponse();
    	
    	if (!response.validAddressInd)
    	{		
			// get any error messages
	    	List<EnterpriseAddressValidationServiceReq_CP.verificationErrorType> errors = response.verificationErrors;
	    	
	    	for (EnterpriseAddressValidationServiceReq_CP.verificationErrorType error : errors)
	    	{
	    		result.errorMessages.add(error.errDesc);
	    	}
    	}
    	
    	// get results states
    	List<EnterpriseAddressValidationServiceReq_CP.verificationResultStateType> resStates = response.verificationResultStates;    	
    	for (EnterpriseAddressValidationServiceReq_CP.verificationResultStateType resState : resStates)
    	{    	
    		result.infoMessages.add(resState.desc_x);
    	}
    	
    	for (EnterpriseAddressValidationServiceReq_CP.verificationResultStateType errorState : response.verificationResultStates) {
    		if (errorState.state == 'C' || errorState.state == 'V') {
    			break; // found correctable address
    		}
    		
    		//couldn't find a valid or correctable address
    		return result;
    	}
    	
    	List<CustomerCommon_CP.Address> addrs = response.matchingAddresses;
    	
    	
    	if (addrs != null && addrs.size() > 0)
    	{
    		integer index = 0;
	    	for (CustomerCommon_CP.Address addr : addrs)
	    	{
	    		Address returnedAddr = createAddressFromValidationResult(addr);
	    		returnedAddr.counter = index++;
	    		result.returnedAddresses.add(returnedAddr);
	    	}
    	}
    	
    	return result;    	
    }
    
    global class Address
	{
		public string seladdrType {get; set;}
		public string seladdrForm {get; set;}
		public String addressLine1 {get; set;}
		public String addressLine2 {get; set;}
		public String addressLine3 {get; set;}
		public String addressConcat
		{
			get
			{
				String addrLine = addressLine1;
				
				if (addressLine2 != null && addressLine2 != '')
				{
					addrLine = addrLine + '\n' + addressLine2;
				}
				
				if (addressLine3 != null && addressLine3 != '')
				{
					addrLine = addrLine + '\n' + addressLine3;
				}
				
				return addrLine;
			}
			set;
		}
		public String city {get; set;}
		public String provState {get; set;}
		public String country {get; set;}
		public String postalCode {get; set;}
		public Integer counter {get; set;}
		
		public override String toString() {
			string results = '';
			
			results = appendIfNotBlank(results, '\n', addressLine1);
			results = appendIfNotBlank(results, '\n', addressLine2);
			results = appendIfNotBlank(results, '\n', addressLine3);
			results = appendIfNotBlank(results, '\n', city);
			results = appendIfNotBlank(results, ', ', provState);
			results = appendIfNotBlank(results, '  ', postalCode);
			results = appendIfNotBlank(results, '\n', country);
			
			return results;
		}
		
		private string appendIfNotBlank(string originalText, string separater, string appendage) {
			if (string.isBlank(appendage)) return originalText;
			
			if (string.isBlank(originalText)) return appendage;
			
			return originalText + separater + appendage;
		}
	}


	private static Address createAddressFromValidationResult(CustomerCommon_CP.Address address) {
    	
    	Address returnedAddr = new Address();
	    		
		returnedAddr.addressLine1 = address.renderedAddress[0];
		returnedAddr.city = address.municipalityName;
		returnedAddr.provState = address.provinceStateCode;
		returnedAddr.country = address.countryCode;
		if (address.postalZipCode != null && !address.postalZipCode.trim().contains(' '))
		{
			returnedAddr.postalCode = address.postalZipCode.left(3) + ' ' + address.postalZipCode.mid(3, address.postalZipCode.trim().length());
		}
		else
		{
			returnedAddr.postalCode = address.postalZipCode;
		}
    	
    	return returnedAddr;
    }
    
    public static Address createAddressFromAccount(Account account, string addressType) {
    	Address address = new Address();
    	
    	address.seladdrType = addressType;
    	
    	List<string> streetAddressLines = new List<string>();
    	
    	if (addressType == 'Billing')
    	{
    		if (string.isNotBlank(account.BillingStreet))
    		{
	    		streetAddressLines.addAll(account.BillingStreet.split('\n'));
    		}
    		address.city = account.BillingCity;
    		address.provState = account.BillingState;
    		address.postalCode = account.BillingPostalCode;
    		address.country = account.BillingCountry;
    	}
    	else if (addressType == 'Shipping')
    	{
    		if (string.isNotBlank(account.ShippingStreet))
    		{
    			streetAddressLines.addAll(account.ShippingStreet.split('\n'));
    		}
    		address.city = account.ShippingCity;
    		address.provState = account.ShippingState;
    		address.postalCode = account.ShippingPostalCode;
    		address.country = account.ShippingCountry;    		
    	}
    	
    	if (streetAddressLines.size() > 0) {
	    	address.addressLine1 = streetAddressLines[0];
    	}
		if (streetAddressLines.size() > 1) {
			address.addressLine2 = streetAddressLines[1];
		}
		if (streetAddressLines.size() > 2) {
			address.addressLine3 = streetAddressLines[2];
		}
    	
    	// default address form
    	if (string.isBlank(address.seladdrForm))
    	{
    		address.seladdrForm = 'Standard';
    	}
    	
    	// default country to Canada if blank
    	if (string.isBlank(address.country))
    	{
    		address.country = 'CAN';
    	}
    	
    	return address;
    }
    
    global class ValidateAddressResponse {
    	global List<string> infoMessages {get;set;}
    	global List<string> errorMessages {get;set;}
    	global List<Address> returnedAddresses {get;set;}	
    
    	global ValidateAddressResponse() {
    		infoMessages = new List<string>();
    		errorMessages = new List<string>();
    		returnedAddresses = new List<Address>();
    	}
    }
}