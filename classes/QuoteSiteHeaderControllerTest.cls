@isTest
private class QuoteSiteHeaderControllerTest {
    private static testMethod void testStandardCtor() {
        SBQQ__Quote__c quote = insertTestQuote();
            
            ApexPages.currentPage().getParameters().put('qid', quote.id);
            
            QuoteSiteHeaderController ctlr = new QuoteSiteHeaderController();
            
            system.assert(ctlr.quoteLoaded);
        system.assertEquals(quote.id,ctlr.quote.id);
    }
    
    private static testMethod void testStandardCtorNoIdInUrl() {
        QuoteSiteHeaderController ctlr = new QuoteSiteHeaderController();
        
        system.assertEquals(false,ctlr.quoteLoaded);
        system.assertEquals(null,ctlr.quote);
    }
    
    private static testMethod void testCtorWithIdParam() {
        SBQQ__Quote__c quote = insertTestQuote();
        
        QuoteSiteHeaderController ctlr = new QuoteSiteHeaderController(quote.id);
            
            system.assert(ctlr.quoteLoaded);
        system.assertEquals(quote.id,ctlr.quote.id);
    }
    
    /* Unable to test this scenario, existing code generates StringException Invalid id
    private static testMethod void testCtorWithIdParamNull() {
        Id empty;
        QuoteSiteHeaderController ctlr = new QuoteSiteHeaderController(empty);
        
        system.assertEquals(false,ctlr.quoteLoaded);
        system.assertEquals(null,ctlr.quote);
    }
    */
    
    private static testMethod void testUserGreeting() {
        QuoteSiteHeaderController ctlr = new QuoteSiteHeaderController();
        
        system.assert(ctlr.UserGreeting.contains(UserInfo.getName()));
    }
    
    private static testMethod void testGetQuoteName() {
        SBQQ__Quote__c quote = insertTestQuote();
        
        Product2 p = new Product2(Name='Test');
        insert p;
        
        SBQQ__QuoteLine__c line = new SBQQ__QuoteLine__c(SBQQ__Quote__c = quote.id,SBQQ__Product__c=p.Id);
        insert line;
        
        final String QUOTE_NAME = [SELECT name FROM SBQQ__Quote__c WHERE Id = :quote.id].name;
        
        QuoteSiteHeaderController ctlr = new QuoteSiteHeaderController(quote.id);
        
        system.assertEquals(QUOTE_NAME,ctlr.getQuoteName());
    }
    
    private static testMethod void testGetQuoteNameNoQuoteLines() {
        SBQQ__Quote__c quote = insertTestQuote();
        
        QuoteSiteHeaderController ctlr = new QuoteSiteHeaderController(quote.id);
        
        system.assertEquals(null,ctlr.getQuoteName());
    }
    
    // TEST HELPERS
    
    @isTest
    private static SBQQ__Quote__c insertTestQuote() {
        Account a = new Account(name = 'test');
            insert a;
            
            Opportunity o = new Opportunity(name = 'test', accountid = a.id, stagename = 'test', closedate = Date.today());
            insert o;
            
            SBQQ__Quote__c quote = new SBQQ__Quote__c(SBQQ__Opportunity__c = o.id);
            insert quote;
            
            return quote;
    }
}