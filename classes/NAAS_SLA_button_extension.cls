public with sharing class NAAS_SLA_button_extension {
    public Account currentAccount;
    private ApexPages.StandardController stdController;
    Public Boolean ShowBtn  {get;set;}
    Public String BCAN {get;set;}
    public String CAN {get;set;}
    Public String url {get;set;}
    public NAAS_SLA_button_extension(ApexPages.StandardController stdController) {
        this.currentAccount= (Account)stdController.getRecord();
        this.stdController = stdController;
      }
   public void renderSlabutton() {
      try{
        ShowBtn = false;
        List<Asset> listOfAsset = new List<Asset>();
        listOfAsset = [Select Id,AccountId,Product2.Catalog__r.Name,vlocity_cmt__BillingAccountId__r.BCAN__c,vlocity_cmt__BillingAccountId__r.CAN__c,vlocity_cmt__BillingAccountId__r.Billing_System_ID__c,vlocity_cmt__BillingAccountId__c From Asset Where (Product2.Catalog__r.Name = 'NaaS' AND  Account.Id =: currentAccount.Id) AND (vlocity_cmt__BillingAccountId__r.Billing_System_ID__c = '101' OR vlocity_cmt__BillingAccountId__r.Billing_System_ID__c = '102' ) Limit 2];
        If(listOfAsset.size() > 0){
            ShowBtn = true; 
            BCAN = listOfAsset[0].vlocity_cmt__BillingAccountId__r.BCAN__c;
            CAN  = listOfAsset[0].vlocity_cmt__BillingAccountId__r.CAN__c;
            
            SLA_NaaS_URL__c domainObj = SLA_NaaS_URL__c.getValues('DomainName1') ; 
           
            if(domainObj != null ){
                url = domainObj.DomainName__c ;
                url ='https://' + url  +'/business/myaccount/products/naas/service-levels/agent';  
                system.debug('url ---------->'+ url );
                
            }
            
        }
        else{
            ShowBtn = false;
        }
    }
    catch(exception e){
    apexpages.addmessages(e);
}
 
 
 }
  
 }