global with sharing class CreateContractDocument implements vlocity_cmt.VlocityOpenInterface{
      public Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {
        Boolean success = true;
        if(methodName == 'createWordDoc')
        { 
                createWordDoc(inputMap, outMap, options); 
        }
        else if(methodName == 'getDocxTemplate')
        { 
                getDocxTemplate(inputMap, outMap, options); 
        }
        
        else if(methodName == 'attachDocx')
        { 
                attachDocx(inputMap, outMap, options); 
        }
       
          return success; 
   }
        
    private void createWordDoc (Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options){        
        System.debug(' In createWordDoc, inputMap is '+inputMap);
        String nameSpaceprefix = 'vlocity_cmt__';
        //create document
        String relsXml=(String) inputMap.get('relXml');
        System.debug(' relsXml is '+relsXml);
        //Map<String, Object> contractResponseMap =(Map<String, Object>) inputMap.get('createContractResponse');
        Map<String,Object> conObjMap = (Map<String, Object>)inputMap.get('contractObj');
        String conId = (String)conObjMap.get('Id');
        String versionId =getContractVersionDetails(conId,false, '');
        inputMap.put('contractVersionId',versionId);

         outMap.put('doc', vlocity_cmt.ContractDocumentCreationController.getContractVersionDocument(versionId, relsXml));
         outMap.put('versionId', versionId);
         system.debug('Doc______' + outMap.get('versionId'));
        /*if(inputMap.get('PickTemplate') !=null){
              Map<String, Object> docTemplate =(Map<String, Object>) inputMap.get('PickTemplate');
              if(docTemplate !=null){
                  List<Object> templateObj = (List<Object>) docTemplate.get('DocumentTemplate');
                  
                  if(templateObj !=null && templateObj.size()>0){
                      Map<String, Object> template = (Map<String, Object>) templateObj[0];
                      Id templateId = (Id) template.get('Id');
                      Id contractVersionId = (Id) inputMap.get('DRId_'+nameSpaceprefix+'ContractVersion__c');
                      if(contractVersionId==null){
                          Id contractId = (Id) inputMap.get('DRId_Contract');
                          List<vlocity_cmt__ContractVersion__c> versionList = [Select Id from vlocity_cmt__ContractVersion__c where vlocity_cmt__ContractId__c=:contractId and vlocity_cmt__Status__c='Active'];
                          if(versionList !=null && versionList.size()>0){
                            contractVersionId = versionList[0].Id;                      
                          }
                      }
                  
                      System.debug('Template Id is '+templateId +' version Id is '+contractVersionId);
                      vlocity_cmt.ContractDocumentDisplayController.createContractSections(templateId, contractVersionId);              
                      outMap.put('doc', vlocity_cmt.ContractDocumentCreationController.getContractVersionDocument(contractVersionId, relsXml));
                  }
             }
        }*/
    }

   private String getContractVersionDetails(String contractId, Boolean isUpdate,String templatePicker ){

    //Create new Version if the request is update
             List<vlocity_cmt__ContractSection__c> oldList = new List<vlocity_cmt__ContractSection__c>();
             List<vlocity_cmt__ContractVersion__c> oldConVersion = new List<vlocity_cmt__ContractVersion__c>();
             List<attachment> oldAttachmentList = new List<attachment>();
             Id contractTemplateId ;
             String versionID;
         try{
            if(contractId != null && contractId != ''){
                // Attach a Template to the esixting Version and then attach a Doc.
                for (vlocity_cmt__ContractVersion__c conVersions: [Select ID,vlocity_cmt__ContractId__r.Contract_Type__c ,AttachmentId__c,
                                                                   vlocity_cmt__ContractId__r.Agreement_Type__c, vlocity_cmt__DocumentTemplateId__c,
                                                                    (select id from vlocity_cmt__Contract_Sections__r) , (select id from attachments)
                                                                    from vlocity_cmt__ContractVersion__c 
                                                                    where vlocity_cmt__ContractId__c = :contractID 
                                                                      and vlocity_cmt__Status__c = 'Active' and vlocity_cmt__ContractId__r.Status = 'Draft']){
                                      if(conVersions.vlocity_cmt__Contract_Sections__r != null)
                                          oldList.addAll(conVersions.vlocity_cmt__Contract_Sections__r);
                                      if(conVersions != null ) 
                                          oldConVersion.add(conVersions); 
                                      if(conVersions.attachments != null && conVersions.attachments.size() > 0 && !conVersions.attachments.isEmpty())
                                          oldAttachmentList.addAll(conVersions.attachments);
                                         
                } 

                if( oldConVersion.size() >0 &&  !oldConVersion.isEmpty() && oldConVersion[0].AttachmentId__c != '' && oldConVersion[0].AttachmentId__c != null){
                        //oldConVersion[0].vlocity_cmt__DocumentTemplateId__c=null;
                        contractTemplateId = oldConVersion[0].vlocity_cmt__DocumentTemplateId__c;
                        //update oldConVersion[0];
                        versionId = vlocity_cmt.ContractServiceResource.createNewContractVersionDocument(contractID);
                        System.debug('New Version ID:' + versionId);    
                    }
                else if(oldConVersion[0].AttachmentId__c == null || oldConVersion[0].AttachmentId__c == ''){
                     versionId = oldConVersion[0].id;
                }

                   

                /*if(conVersion.size()>0 && !conVersion.isEmpty() ) {
                    activeContVersion = conVersion[0];
                     versionId = conVersion[0].id;
                    System.debug('Old Version ID:' + versionId);*/
                   
                              
                }
             }catch(exception e){
                System.debug(LoggingLevel.ERROR, 'Exception is '+e);
                System.debug(LoggingLevel.ERROR, 'Exception stack trace '+e.getStackTraceString());
                 String errorMessage = e.getMessage()  ;
                 return errorMessage;
            }
             return  versionID; 
        
   }



    
    private void getDocxTemplate (Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options){        
               
        outMap.put('relXml',vlocity_cmt.ContractDocumentCreationController.getDocxTemplate());
    }
    
    private void attachDocx (Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options){        
        System.debug(' In attachDocx, inputMap is '+inputMap);
        String contractVersionId;
         Map<String, Object> contractResponseMap =(Map<String, Object>) inputMap.get('createContractResponse');
        Map<String,Object> conObjMap = (Map<String, Object>)contractResponseMap.get('contractObj');
        String conId = (String)conObjMap.get('Id');
        contractVersionId = (String)inputMap.get('contractVersionId');
        if(contractVersionId == null){
              //Id contractId = (Id) inputMap.get('DRId_Contract');
              List<vlocity_cmt__ContractVersion__c> versionList = [Select Id from vlocity_cmt__ContractVersion__c where vlocity_cmt__ContractId__c=:conId and vlocity_cmt__Status__c='Active'];
              if(versionList !=null && versionList.size()>0){
                 contractVersionId = versionList[0].Id;                     
              }
        }
        
        String data = (String) inputMap.get('contractData');
        vlocity_cmt.ContractDocumentCreationController.saveDocx(contractVersionId, data);
    }
    
}