/*------------------------
 @Author: TELUS internal Team. 
 @Business Requirement: This is to support 
 @Dependent VF Page:User see this page afetr selecting Complex/simple Order type. 
 
--------------------------*/
public class Util_Class_for_Complex_Order {
    
     public static List<SelectOption> findPickListVlaues(String obj, String picklistName){
            map<String,Schema.SObjectType> gd = Schema.getGlobalDescribe();
            Schema.sObjectType sobject_type = gd.get(obj);

            Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe();

            Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap();
          
            List<Schema.PicklistEntry> pick_list_values = field_map.get(picklistName).getDescribe().getPickListValues();

            List<selectOption> options = new List<selectOption>();

          for (Schema.PicklistEntry a : pick_list_values) {
                      options.add(new selectOption(a.getLabel(), a.getValue()));
          }
      return options;
     }
     
     //This is to read any filedset and return field meta data. 
     public static List<Schema.FieldSetMember> readFieldSet(String fieldSetName, String ObjectName)
    {
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(ObjectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
    
        //system.debug('====>' + DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName));
    
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
    
        //List<Schema.FieldSetMember> fieldSetMemberList =  fieldSetObj.getFields();
        //system.debug('fieldSetMemberList ====>' + fieldSetMemberList);  
        return fieldSetObj.getFields(); 
    }

}