/******************************************************************************
# File..................: OpportunityDetailController
# Version...............: 1
# Created by............: Sandip Chaudhari (IBM)
# Description...........: Class is to Create, Validate, Save Billing Address for opportunity. 
                          This class is main controller for detail page. Created as a part of PAC R2
******************************************************************************/
Public class OpportunityDetailController{
    public AddressData PAData  { get; set; }
    public String fullAddress { get; set; }
    public String isError{ get; set; }
    Opportunity oppObj;
    
    // Constructor to get required details like opportunity product item record, prepare full address, instantiate objects, mapping
    public OpportunityDetailController(ApexPages.StandardController controller){
        isError = 'onLoad';
        PAData = new AddressData();
        PAData.isAddressLine1Required = true;
        PAData.isProvinceRequired = true;
        PAData.isCityRequired = true;
       // address = new Address__c();
        oppObj = (Opportunity)controller.getRecord();
        oppObj = [SELECT Id, Building_Name__c, Suite_Unit__c, Street__c,
                        Street_Direction__c, accountId, City__c, State_Province__c,
                        Postal_Code__c, Country__c, Contract_Billing_Address_Status__c
                        FROM Opportunity
                        WHERE Id=: oppObj.Id];
        if(oppObj != null){
            fullAddress = '  ';
            /*if(oppObj.Building_Name__c != null && oppObj.Building_Name__c != 'null'){
                fullAddress = fullAddress  + ' ' + oppObj.Building_Name__c;
            }
            if(oppObj.Suite_Unit__c != null && oppObj.Suite_Unit__c != 'null'){
                fullAddress = fullAddress  + ' ' + oppObj.Suite_Unit__c;
            }*/
            if(oppObj.Street__c != null && oppObj.Street__c != 'null'){
                fullAddress = fullAddress  + ' ' + oppObj.Street__c;
            }
            if(oppObj.Street_Direction__c != null && oppObj.Street_Direction__c != 'null'){
                fullAddress = fullAddress  + ' ' + oppObj.Street_Direction__c;
            }
            if(oppObj.City__c != null && oppObj.City__c != 'null'){
                fullAddress = fullAddress  + ' ' + oppObj.City__c;
            }
            if(oppObj.State_Province__c != null && oppObj.State_Province__c != 'null'){
                fullAddress = fullAddress  + ' ' + oppObj.State_Province__c;
            }
            if(oppObj.Postal_Code__c != null && oppObj.Postal_Code__c != 'null'){
                fullAddress = fullAddress  + ' ' + oppObj.Postal_Code__c;
            }
            if(oppObj.Country__c != null && oppObj.Country__c != 'null'){
                fullAddress = fullAddress  + ' ' + oppObj.Country__c;
            }
            
            PAData.searchString = fullAddress;
            PAData.addressLine1 = oppObj.Street__c;
            PAData.city = oppObj.City__c;
            PAData.province = oppObj.State_Province__c;
            PAData.canadaProvince = oppObj.State_Province__c;
            PAData.usState = oppObj.State_Province__c;
            PAData.country = oppObj.Country__c;
            PAData.postalCode = oppObj.Postal_Code__c;
            
            if(oppObj.Contract_Billing_Address_Status__c == System.Label.PACInValidCaptureNewAddrStatus){
                PAData.isManualCapture = true;
            }else{
                PAData.isManualCapture = false;
            }
        }else{
            PAData.country='Canada';
        }
    }
    
    /*
    * Created by :  Sandip Chaudhari
    * Name - updateContractBillingAddress 
    * Description - This method is map the PAC address values with the existing address object
    * and update the object.
    */
    public pageReference updateContractBillingAddress (){
        try{
            isError = 'inSave';
            mapAddress();
            update oppObj;
        }catch(Exception ex){
            isError = 'InError';
            ApexPages.addMessages(ex) ;
        }
        //    PageReference redirectPage = new PageReference('/'+ oppObj.Id);
        //redirectPage.setRedirect(true);
        //return redirectPage;
       return null;
    }
    
  /*  public boolean checkDuplicate(){
         boolean isDupe = false;
         SearchAddressUtil searchUtil = new SearchAddressUtil(oppObj.AccountId, MAX_NO_OF_RECORDS);
         List<AddressWrapper> addressList = searchUtil.getAddresses(address);
         if(addressList != null){
             isDupe = (addressList.size() > 0);
         }
         return isDupe;
    } */
    
    /**
    * Name - mapAddress
    * Description - Map Billing address with respective address fields
    * Param - 
    * Return type - void
    **/
    public void mapAddress(){
        system.debug('#### City' + PAData.city);
        system.debug('#### postalCode' + PAData.postalCode);
        oppObj.Building_Name__c = PAData.buildingNumber;
       //oppProductObj.Floor__c = PAData1.
        //oppObj.Suite_Unit__c = PAData1.suiteNumber;
        oppObj.Street__c = PAData.addressLine1;
        //oppProductObj.Street_Type__c = PAData1.
        //oppObj.Street_Direction__c = PAData1.streetDirection;
        oppObj.City__c = PAData.city;
        oppObj.State_Province__c = PAData.province;
        oppObj.Postal_Code__c = PAData.postalCode;
        oppObj.Country__c = PAData.country;
        oppObj.Contract_Billing_Address_Status__c = PAData.captureNewAddrStatus;
    }
    
}