public with sharing class CustomAccountLookupController {
  public Account account {get;set;} // new account to create
 // public List<Account> results{get;set;} 
  public List<Contact> results{get;set;} 
  public string searchString{get;set;} // search keyword
  
  public CustomAccountLookupController() {
    //account = new Account();
    // get the current search string
    searchString = System.currentPageReference().getParameters().get('lksrch');
    //runSearch();
    runSearchPhone();
    system.debug('*** CustomAccountLookupController search for '+searchString); 
    searchString = ''; 
  }
   
  // performs the keyword search
  public PageReference search() {
    system.debug('*** search');
    runSearch();
    return null;
  }
  
  // prepare the query and issue the search command
  private void runSearch() {
    system.debug('*** runSearch');
    // TODO prepare query string for complex serarches & prevent injections
    results = performSearch(searchString); 
    system.debug('*** runSearch results = '+results);              
  } 
  
  // run the search and return the records found. 
//  private List<Account> performSearch(string searchString) {
    private List<Contact> performSearch(string searchString) {
    system.debug('*** performSearch for '+searchString);
    //String soql = 'select id, name from account';
    String soql = 'select id, name from contact';
    if(searchString != '' && searchString != null)
      soql = soql +  ' where name LIKE \'%' + searchString +'%\'';
      //soql = soql +  ' where phone LIKE \'%' + searchString +'%\'';      
    soql = soql + ' limit 25';
    System.debug(soql);
    return database.query(soql); 

  }
  
  // prepare the query and issue the search command
  private void runSearchPhone() {
    system.debug('*** runSearchPhone');
     results = performSearchPhone(searchString); 
    system.debug('*** runSearchPhone results = '+results);              
  } 

  
// run the search and return the records found. 
    private List<Contact> performSearchPhone(string searchString) {
    system.debug('*** performSearchPhone for '+searchString);
    String soql = 'select Contact.id, Contact.name, Contact.Account.Name, Contact.Phone from contact';
    if(searchString != '' && searchString != null)
      //soql = soql +  ' where phone LIKE \'%' + searchString +'%\'';
      soql = soql + ' where phone LIKE \'%' + searchString +'%\' '+ ' OR MobilePhone LIKE \'%' + searchString +'%\' ';
    soql = soql + ' limit 25';
    System.debug('*** performSearchPhone soql = ' +soql);
    return database.query(soql); 

  }
  
  
  
  // save the new account record
  public PageReference saveAccount() {
    insert account;
    // reset the account
    account = new Account();
    return null;
  }
  
  // used by the visualforce page to send the link to the right dom element
  public string getFormTag() {
    return System.currentPageReference().getParameters().get('frm');
  }
    
  // used by the visualforce page to send the link to the right dom element for the text box
  public string getTextBox() {
    return System.currentPageReference().getParameters().get('txt');
  }
 
}