/* 2015-04-05[Roderick de Vera]
 * Helper class which invokes the Web Service Callout for Retrieve Product Configuration
 */
public class OrdrRetrieveProductConfigurationCallout {
    
    public static TpInventoryProductConfigV3.Product retrieveProductConfigurationByCustomerAccount(String customerProfileId) {
        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put(OrdrConstants.CUSTOMER_PROFILE_ID, customerProfileId);
        return retrieveProductConfiguration(inputMap);
    }
    
    public static TpInventoryProductConfigV3.Product retrieveProductConfigurationByServiceAccount(String customerProfileId, String locationId) {
        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put(OrdrConstants.CUSTOMER_PROFILE_ID, customerProfileId);
        inputMap.put(OrdrConstants.LOCATION_ID, locationId);
        return retrieveProductConfiguration(inputMap);
    }
    
    public static TpInventoryProductConfigV3.Product retrieveProductConfigurationByProductInstance(String productInstanceId) {
        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put(OrdrConstants.PRODUCT_INSTANCE_ID, productInstanceId);
        return retrieveProductConfiguration(inputMap);
    }    
    
    private static TpInventoryProductConfigV3.Product retrieveProductConfiguration(Map<String, Object> inputMap) {
        TpInventoryProductConfigV3.Product productConfiguration = new TpInventoryProductConfigV3.Product();
        try { 
            OrdrWsRetrieveProductConfiguration.RetrieveProductConfigurationSOAPPort servicePort = new OrdrWsRetrieveProductConfiguration.RetrieveProductConfigurationSOAPPort(); 
            servicePort = prepareCallout(servicePort);          
            productConfiguration = servicePort.retrieveProductConfiguration(preparePayload(inputMap)); 
        } 
        catch (CalloutException e) {
            //todo: log web service exception
            throw e;
        }
        return productConfiguration;
    }
    
    private static OrdrWsRetrieveProductConfiguration.RetrieveProductConfigurationSOAPPort 
        prepareCallout(OrdrWsRetrieveProductConfiguration.RetrieveProductConfigurationSOAPPort servicePort) {
            if (!Test.isRunningTest( )) {
                Ordering_WS__c retrieveProductConfigEndpoint = Ordering_WS__c.getValues('RetrieveProductConfigEndpoint');           
                servicePort.endpoint_x = retrieveProductConfigEndpoint.value__c;
                // Set SFDC Webservice call timeout
                servicePort.timeout_x = OrdrConstants.WS_TIMEOUT_MS;    

                if (servicePort.endpoint_x.startsWith(OrdrConstants.ENDPOINT_XML_GATEWAY_SYMBOL)) {
                    Ordering_WS__c wsUserName =  Ordering_WS__c.getValues(OrdrConstants.CREDENTIAL_USERNAME);
                    Ordering_WS__c wsPassword =  Ordering_WS__c.getValues(OrdrConstants.CREDENTIAL_PASSWORD); 
                    String credentials = wsUserName.value__c + ':' + wsPAssword.value__c;
                    String encodedUserNameAndPassword = EncodingUtil.base64Encode(Blob.valueOf(credentials));   
                    
                    servicePort.inputHttpHeaders_x = new Map<String, String>();
                    servicePort.inputHttpHeaders_x.put(OrdrConstants.AUTHORIZATION, OrdrConstants.BASIC + encodedUserNameAndPassword);
                } else {
                    servicePort.clientCertName_x = OrdrConstants.SDF_CERT_NAME;
                }
            }
            
            return servicePort;
    } 
    
    private static TpInventoryProductConfigV3.Product preparePayload(Map<String, Object> inputMap) {
        TpInventoryProductConfigV3.Product productConfigurationRequest = new TpInventoryProductConfigV3.Product();
        
        /*create productConfiguration/Specification section
        */
        TpCommonBaseV3.EntitySpecification specification = new TpCommonBaseV3.EntitySpecification();
        specification.Name = OrdrConstants.SPECIFICATION_NAME;
        specification.Type_x = OrdrConstants.SPECIFICATION_TYPE_PRODUCT_CONFIG;
        specification.Category = OrdrConstants.SPECIFICATION_CATEGORY_PRODUCT_CONFIG;
        productConfigurationRequest.Specification = specification;        
        
        /*create productConfiguration/CustomerAccountId section
        */            
        if (inputMap.containsKey(OrdrConstants.CUSTOMER_PROFILE_ID) && inputMap.get(OrdrConstants.CUSTOMER_PROFILE_ID) != null) {
	        TpCommonBaseV3.EntityWithSpecification customerAccountId = new TpCommonBaseV3.EntityWithSpecification();
            customerAccountId.Id = (String) inputMap.get(OrdrConstants.CUSTOMER_PROFILE_ID);
	        productConfigurationRequest.CustomerAccountId = customerAccountId;
        }
        
        /*create productConfiguration/CharacteristicValue/Characteristic/Name section
        */            
        List<TpCommonBaseV3.CharacteristicValue> characteristicValues = new List<TpCommonBaseV3.CharacteristicValue>();
        if (inputMap.containsKey(OrdrConstants.LOCATION_ID) && inputMap.get(OrdrConstants.LOCATION_ID) != null) {
            characteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.LOCATION_ID, new List<String>{(String)inputMap.get(OrdrConstants.LOCATION_ID)}));
        }         
        if (inputMap.containsKey(OrdrConstants.PRODUCT_INSTANCE_ID) && inputMap.get(OrdrConstants.PRODUCT_INSTANCE_ID) != null) {
            characteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.PRODUCT_INSTANCE_ID, new List<String>{(String)inputMap.get(OrdrConstants.PRODUCT_INSTANCE_ID)}));
        }
        characteristicValues.add(OrdrUtilities.constructCharacteristicValue('instanceType', new List<String>{OrdrConstants.INSTANCE_TYPE}));        
        productConfigurationRequest.CharacteristicValue = characteristicValues;
        
        return productConfigurationRequest;
    }    

}