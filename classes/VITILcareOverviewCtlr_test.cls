@isTest
public class VITILcareOverviewCtlr_test {
	static Case vitilCareTestCase {get;set;}
	static {
	insert new VITILcare_Customer_Interface_Settings__c( 
			Manage_Requests_List_Limit__c = 10,
			Overview_Case_Limit__c = 5,
			Case_Origin_Value__c = 'VITILcare'
		);

        List<Case> cases = new List<Case>();
Case vitilCareCase = new Case(Subject = 'Test Subject', Origin = 'VITILcare', My_Business_Requests_Type__c = 'Dummy', RecordTypeId = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId);
		cases.add(vitilCareCase);
		 insert cases;
		 vitilCareTestCase = [SELECT Id, Subject, RecordTypeId, Contact.FirstName, Contact.LastName, My_Business_Requests_Type__c, CaseNumber, Status, LastModifiedDate FROM Case WHERE Id = :vitilCareCase.Id LIMIT 1];
}
@isTest(SeeAllData=false) static void overviewController() {
	VITILcareOverviewCtlr overviewContr = new VITILcareOverviewCtlr();
		overviewContr.initCheck();

		Test.startTest();

		//test search functionality
		Case c = [SELECT caseNumber from Case Where Id = :vitilCareTestCase.Id LIMIT 1];
		//overviewContr.q = 'Test';
		overviewContr.searchCase();
		System.assertEquals(1, overviewContr.totalCases);

        Id [] fixedSearchResults = new Id[]{c.Id};
        Test.setFixedSearchResults(fixedSearchResults);

		overviewContr.q = 'Test';
		List<Case> cases = overviewContr.executeSearch(1,1,true);
		System.assertEquals(1, cases.size());
		overviewContr.searchOverview();
		Test.stopTest();
		

	}
	

	@isTest static void VITILcare_testGuestUser() {
		VITILcareOverviewCtlr overviewContr = new VITILcareOverviewCtlr();
	}

	@isTest static void VITILcare_testGetCaseDetails() {
        Case VITILcareCase = new Case(Subject = 'Test Subject', Origin = 'ENTP', My_Business_Requests_Type__c = 'Other', Lynx_Ticket_Number__c = '5555555555', RecordTypeId = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId);
		insert VITILcareCase;
		Case VITILcareTestCase = [SELECT Id, Subject, RecordTypeId, Contact.FirstName, Lynx_Ticket_Number__c, Contact.LastName, My_Business_Requests_Type__c, CaseNumber, Status, LastModifiedDate FROM Case WHERE Id = :VITILcareCase.Id LIMIT 1];

		VITILcareOverviewCtlr ctlr = new VITILcareOverviewCtlr();
		ctlr.caseNumber = VITILcareTestCase.CaseNumber;
		System.assert(ctlr.getCaseDetails().getUrl().containsIgnoreCase('detail'));
	}

	@isTest static void VITILcare_testGetPageOfCases() {
		VITILcareOverviewCtlr ctlr = new VITILcareOverviewCtlr();
		
		ctlr.categoryFilter = 'Test';
		
	}

	@isTest static void VITILcare_testCreateNewCase() {
		VITILcareOverviewCtlr ctlr = new VITILcareOverviewCtlr();
		System.assert(ctlr.createNewCase().getUrl().containsIgnoreCase('Case'));
	}

	@isTest static void VITILcare_testOtherMethods() {
        
StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('ENTP_LynxTicketSearchXMLResult');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type','text/xml;charset=UTF-8');
		Test.setMock(HttpCalloutMock.class, mock);

		ENTPTestUtils.createSMBCareWebServicesCustomSetting();
		Integer selectedPage = 0;
		List<Case> cases1 = new List<Case>();
		Case VITILcareCase1 = new Case(Subject = 'Test Subject', Origin = 'ENTP', My_Business_Requests_Type__c = 'Other', RecordTypeId = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId);
		cases1.add(VITILcareCase1);

		insert cases1;

		case VITILcareTestCase = [SELECT Id, Subject, RecordTypeId, Contact.FirstName, Lynx_Ticket_Number__c, Contact.LastName, My_Business_Requests_Type__c, CaseNumber, Status, LastModifiedDate FROM Case WHERE Id = :VITILcareCase1.Id LIMIT 1];
		// ENTPOverviewCtlr1.CaseListing cl = new ENTPOverviewCtlr1.CaseListing(ENTPTestCase);

		ENTP_Customer_Interface_Settings__c ENTPcis = new ENTP_Customer_Interface_Settings__c(Manage_Requests_List_Limit__c = null,
				Overview_Case_Limit__c = null);
		insert ENTPcis;
		ENTP_Customer_Interface_Settings__c cis = ENTP_Customer_Interface_Settings__c.getInstance();
		system.debug('CIS:=' + cis);
		/*   if(cis != null){

				   delete cis;


			}
	  System.assert(cis == null);*/
		VITILcareOverviewCtlr ctlr = new VITILcareOverviewCtlr();

				ctlr.categoryType = null;
		ctlr.createNewCase();
		// paging

		ctlr.totalPages = 20;
		ctlr.selectedPage = 5;
		//ctlr.searchOverview();
		ctlr.resetSearch();
		//ctlr.abbreviate('Testing', 4);
		System.assert(ctlr.pageNumbers.size() > 0);
		// System.assert(cl.allListings.size() > 0);
		System.assert(ctlr.refreshGrid() == null);
		ctlr.Previous();
		ctlr.Next();
		System.assert(ctlr.c == null);
		System.assert(ctlr.pageError == null);
		// System.assert(ctlr.Previous());
		// System.assert(ctlr.Next());
		//System.assert(ctlr.getDisablePrevious() == false);
		System.assert(ctlr.getDisableNext() == false);

		ctlr.totalPages = 1;
		ctlr.selectedPage = 4;
		System.assert(ctlr.pageNumbers.size() > 0);
		System.assert(ctlr.TotalPages > 0);

		// filters
		System.assert(ctlr.getCategoryFilterOptions().size() > 0);
		System.assert(ctlr.getStatusFilterOptions().size() > 0);
		
		Test.startTest();
		User u = MBRTestUtils.createENTPPortalUser('test-user-1', false, 'en_US');
		System.runAs(u) {
			String userAccountId = [SELECT AccountId FROM User WHERE Id = :UserInfo.getUserId()].AccountId;
			// system.assert(String.isNotEmpty(userAccountId));
			List<account> Accounts = [SELECT Strategic__c, ID from Account where ID = :userAccountId];
			ctlr.getLanguageLocaleKey();
			String userLanguage = [SELECT LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()].LanguageLocaleKey;
			// system.assert(String.isNotEmpty(userLanguage));
			ctlr.getSearchError();
			// account a = new account(name = 'test acct');
			//insert a;

			List<Case> testCases = ENTPTestUtils.createENTPCases(2, userAccountId);

			List<Case> caseList1 = [SELECT createdby.firstname, createdby.lastname, Lynx_Ticket_Number__c, NotifyCustomer__c, NotifyCollaboratorString__c, OwnerId, Resolution_Details__c, My_Business_Requests_Type__c, ParentId, isClosed, Contact.FirstName, Contact.LastName, id,contact.name,subject,description,lastmodifieddate,case_resolution__c,createddate, caseNumber, recordType.name, account.name, type, status, priority, isEscalated, Escalation_History__c, (select Active__c,Collaborator_Email__c, Collaborator_Name__c, Role__c from Collaborator__r order by createddate desc), (select createdby.firstname, createdby.lastname,Id,Name,Description,ParentId,CreatedDate,LastModifiedDate, ContentType from Attachments order by LastModifiedDate asc), (select createdby.firstname, createdby.lastname,Id,CommentBody,ParentId,CreatedDate,LastModifiedDate from casecomments where IsPublished = true order by CreatedDate asc), (select id, toaddress,fromaddress,fromname,ccaddress,messagedate,subject,htmlbody,textbody,hasattachment from emailmessages) FROM Case WHERE Id = :testCases[0].Id];
			Case cs1 = caseList1[0];
			ctlr.LynxTicketSearchRET(cs1.Lynx_Ticket_Number__c);
			// ctlr.PinNumRet();
			//ctlr.GetLynxTicketInfo(cs1.id);
			Test.stopTest();
		}

	}


	@isTest(SeeAllData=false) static void testFrMethods() {
		User f = MBRTestUtils.createENTPPortalUser('test-user-2', false, 'fr');
		ENTPTestUtils.createENTPCustomSetting();
		ENTPTestUtils.createSMBCareWebServicesCustomSetting();

		System.runAs(f) {
			//Case ENTPCase3 = new Case(Subject = 'Test Subject', Origin = 'ENTP', My_Business_Requests_Type__c = 'Other', Lynx_Ticket_Number__c = '5555555555', RecordTypeId = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId);
			//insert ENTPCase3;
			String userAccountId = [SELECT AccountId FROM User WHERE Id = :UserInfo.getUserId()].AccountId;
			system.assert(String.isNotEmpty(userAccountId));
			List<Case> FrCase = MBRTestUtils.createENTPCases(1, userAccountId);
			Case ENTPTestCase3 = [SELECT Id, Subject, RecordTypeId, Contact.FirstName, Lynx_Ticket_Number__c, Contact.LastName, My_Business_Requests_Type__c, CaseNumber, Status, LastModifiedDate FROM Case WHERE Id = :FrCase[0].Id LIMIT 1];
			ENTPOverviewCtlr1 ctlr = new ENTPOverviewCtlr1();

			ctlr.initCheck();
			Test.startTest();

			List<account> Accounts = [SELECT Strategic__c, ID from Account where ID = :userAccountId];
			ctlr.getLanguageLocaleKey();
			String userLanguage = [SELECT LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()].LanguageLocaleKey;
			system.debug('UserLanguage-TEST:' + userLanguage);
			system.assert(String.isNotEmpty(userLanguage));
			String CustRole = 'private cloud';
			String CustRole1 = CustRole;
			system.debug('CustRole1-TEST:' + CustRole1);
			String SFDCAccountId = 'test';
			System.assertEquals(SFDCAccountId, 'test');
			String CatListPull1 = 'test';
			System.assertEquals(CatListPull1, 'test');
			// List<ENTP_portal_additional_functions__c> ENTPPortalMap = [Select Name,Customer_Portal_Role__c,Additional_Function__c from ENTP_portal_additional_functions__c where Customer_Portal_Role__c =: CustRole];
			//system.assert(String.isNotEmpty(ENTPPortalMap[0].Additional_Function__c));
			String AddFuncPull1 = 'test';
			//System.assertEquals( AddFuncPull1, 'test');
			string CatListPull = 'test';
			//   String CatListPull1='test';
			System.assertEquals(CatListPull1, 'test');
			// String AddFuncPull1='test';
			System.assertEquals(AddFuncPull1, 'test');
			//System.assertEquals( CatListPull, CatListPull);
			List<User> users = [SELECT Id, AccountId, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()];
			system.assert(String.isNotEmpty(users.get(0).AccountId));


			Test.stopTest();
		}

	}

}