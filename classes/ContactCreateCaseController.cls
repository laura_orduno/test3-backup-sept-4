global class ContactCreateCaseController  {

    private final Contact cont;
    
    public string Subject {get;set;}
    public string Status {get;set;}
    public string Description {get;set;}
    public Id NewCaseId {get;set;}
    
    @remoteAction
    global static Id SaveCaseRemote(Id contId, String Subject, String Status, string Description){
        
        System.Debug('*****      ContactCreateCaseController::SaveCaseRemote');
        System.Debug('*****      ContactCreateCaseController::contId='+contId );
        Contact acctId = [SELECT AccountId FROM Contact WHERE id= :contId];
        Id NewCaseId = CaseModel.CreateCase(acctId.AccountId, contId,Subject, Status, Description );
        return NewCaseId;
        
    }
    
    
    public ContactCreateCaseController(ApexPages.StandardController stdController) {
        this.cont = (Contact)stdController.getRecord();
    }
    
    public PageReference saveCase() {
        System.Debug('*****      ContactCreateCaseController::saveCase');
        
        //NewCaseId = CaseModel.CreateCase(cont.Id,Subject, Status, Description );
        return null;
    }
   
    public string getLabel() {
        return ('12345');
    }
}