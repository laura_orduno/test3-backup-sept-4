//Generated by wsdl2apex

public class AsyncSmb_TTODS_troubleticket {
    public class TroubleTicketListFuture extends System.WebServiceCalloutFuture {
        public smb_TTODS_troubleticket.TroubleTicket[] getValue() {
            smb_TTODS_troubleticket.TroubleTicketList response = (smb_TTODS_troubleticket.TroubleTicketList)System.WebServiceCallout.endInvoke(this);
            return response.TroubleTicket;
        }
    }
}