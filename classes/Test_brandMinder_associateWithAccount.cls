@isTest
private class Test_brandMinder_associateWithAccount {

    static testMethod void myUnitTest() {
       Account acct = TestUtil.createAccount();
       
       Schema.DescribeSObjectResult acc = Schema.SObjectType.Account; 
       Map<String,Schema.RecordTypeInfo> acctRtMapByName = acc.getRecordTypeInfosByName();
       Schema.RecordTypeInfo acctRtByName =  acctRtMapByName.get('RCID');
       
       acct.recordtypeid = acctRtByName.getRecordTypeId();
       acct.RCID__c = '100';
       
       //Added by Arvind on 01/12/2016 to by pass the new validation rule added on Account
       
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'testUser', Email='standardAccountuser@testorg.com.ocom', 
              EmailEncodingKey='UTF-8', LastName='Testing123', LanguageLocaleKey='en_US', 
              LocaleSidKey='en_US', ProfileId = p.Id, 
              TimeZoneSidKey='America/Los_Angeles', UserName='standarduser88@testing.com.ocom', IsActive = true, Can_Update_Account_Name__c=True);    
        insert u;  
        System.runas(u)
        {  
        update acct;
        }
        //Addition End
       Case theCase = TestUtil.createCases(1, acct.id, false)[0];

       Schema.DescribeSObjectResult acc2 = Schema.SObjectType.Case; 
       Map<String,Schema.RecordTypeInfo> rtMapByName = acc2.getRecordTypeInfosByName();
       Schema.RecordTypeInfo rtByName =  rtMapByName.get('SMB Care Technical Support');
       
       theCase.recordtypeid = rtByName.getRecordTypeId();
       theCase.brandminder__c = true;
       theCase.proactive_case__c = true;
       theCase.rcid_for_account_lookup__c = '100';
       
       Test.startTest();
          insert theCase;
       Test.stopTest();

    }

    static testMethod void myUnitTestNoRcid() {
       Account acct = TestUtil.createAccount();
       
       Schema.DescribeSObjectResult d = Schema.SObjectType.Account; 
       Map<String,Schema.RecordTypeInfo> acctRtMapByName = d.getRecordTypeInfosByName();
       Schema.RecordTypeInfo acctRtByName =  acctRtMapByName.get('RCID');
       
       acct.recordtypeid = acctRtByName.getRecordTypeId();
       acct.RCID__c = '100';
       //Added by Arvind on 01/12/2016 to by pass the new validation rule added on Account
       
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'testUser', Email='standardAccountuser1@testorg.com.ocom', 
              EmailEncodingKey='UTF-8', LastName='Testing123', LanguageLocaleKey='en_US', 
              LocaleSidKey='en_US', ProfileId = p.Id, 
              TimeZoneSidKey='America/Los_Angeles', UserName='standarduser77@testing.com.ocom', IsActive = true, Can_Update_Account_Name__c=True);    
        insert u;  
        System.runas(u)
        {  
        update acct;
        }
        
        // Addition End
       Case theCase = TestUtil.createCases(1, acct.id, false)[0];

       Schema.DescribeSObjectResult d2 = Schema.SObjectType.Case; 
       Map<String,Schema.RecordTypeInfo> rtMapByName = d2.getRecordTypeInfosByName();
       Schema.RecordTypeInfo rtByName =  rtMapByName.get('SMB Care Technical Support');
       
       theCase.recordtypeid = rtByName.getRecordTypeId();
       theCase.brandminder__c = true;
       theCase.proactive_case__c = true;
       //theCase.rcid_for_account_lookup__c = '100';
       
       Test.startTest();
          try{
            insert theCase;
          } catch (Exception e){
            System.assertEquals(e.getMessage().contains('RCID is required'),true);
          }
       Test.stopTest();

    }

}