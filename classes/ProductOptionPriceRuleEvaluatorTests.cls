@isTest
private class ProductOptionPriceRuleEvaluatorTests {
    private static SBQQ__Quote__c quote;
    
    static testMethod void testEvaluate() {
        setUp();
        
        Product2 p1 = new Product2(ProductCode='TP1', Name='Test Product 1',Family='Test',IsActive=true);
        insert p1;
        Product2 p2 = new Product2(ProductCode='TP2', Name='Test Product 2',Family='Test',IsActive=true);
        insert p2;
        
        SBQQ__PriceRule__c priceRule = new SBQQ__PriceRule__c(SBQQ__TargetObject__c='Product Option',SBQQ__ConditionsMet__c ='All',SBQQ__Active__c=true);
        insert priceRule;
        
        ProductOptionPriceRuleEvaluator evaluator = new ProductOptionPriceRuleEvaluator(quote.Id, new SBQQ__ProductOption__c[] {});
        evaluator.evaluate();               
        
        SBQQ__PriceAction__c priceAction = new SBQQ__PriceAction__c(SBQQ__Value__c = '150', SBQQ__Rule__c = priceRule.Id);
        priceAction.SBQQ__Field__c = 'Unit Price';
        insert priceAction;
        
        SBQQ__PriceCondition__c condition1 = new SBQQ__PriceCondition__c(SBQQ__Value__c = 'Test Code', SBQQ__Rule__c = priceRule.Id
        , SBQQ__Operator__c = 'equals', SBQQ__Object__c = 'Product Option');
        condition1.SBQQ__Field__c = String.valueOf(SBQQ__ProductOption__c.SBQQ__ComponentCode__c);
        insert condition1;
        
        evaluator = new ProductOptionPriceRuleEvaluator(quote.Id, new SBQQ__ProductOption__c[] {});
        evaluator.evaluate();               
        
        SBQQ__PriceCondition__c condition2 = new SBQQ__PriceCondition__c(SBQQ__Value__c = 'Test', SBQQ__Rule__c = priceRule.Id
        , SBQQ__Operator__c = 'equals', SBQQ__Object__c = 'Quote');
        condition2.SBQQ__Field__c = String.valueOf(SBQQ__Quote__c.SBQQ__BillingCity__c);
        insert condition2;                      
        
        SBQQ__ProductOption__c option1 = new SBQQ__ProductOption__c(SBQQ__UnitPrice__c = 10.0, SBQQ__Selected__c = false, SBQQ__Required__c = false
            , SBQQ__Quantity__c = 1.0, SBQQ__OptionalSKU__c = p2.Id, SBQQ__Number__c = 1.0
            , SBQQ__Discount__c = 10.0, SBQQ__Bundled__c = False, SBQQ__ComponentCode__c='Test Code');
            
        evaluator = new ProductOptionPriceRuleEvaluator(quote.Id, new SBQQ__ProductOption__c[] {option1});
        evaluator.evaluate();       
        
        System.assertEquals(150, option1.SBQQ__UnitPrice__c);
        
        SBQQ__ProductOption__c option2 = new SBQQ__ProductOption__c(SBQQ__UnitPrice__c = 20.0, SBQQ__Selected__c = false, SBQQ__Required__c = false
            , SBQQ__Quantity__c = 2.0, SBQQ__OptionalSKU__c = p2.Id, SBQQ__Number__c = 2.0
            , SBQQ__Discount__c = 20.0, SBQQ__Bundled__c = False, SBQQ__ComponentCode__c='Test Code 2');  
            
        evaluator = new ProductOptionPriceRuleEvaluator(quote.Id, new SBQQ__ProductOption__c[] {option2});
        evaluator.evaluate();               
        
        priceRule.SBQQ__ConditionsMet__c = 'Any';
        update priceRule;
        
        evaluator = new ProductOptionPriceRuleEvaluator(quote.Id, new SBQQ__ProductOption__c[] {option2});
        evaluator.evaluate();       
        
        System.assertEquals(150, option1.SBQQ__UnitPrice__c); 
        
        SBQQ__PriceCondition__c condition3 = new SBQQ__PriceCondition__c(SBQQ__Value__c = '2', SBQQ__Rule__c = priceRule.Id
        , SBQQ__Operator__c = 'equals', SBQQ__Object__c = 'Quote Line');
        condition3.SBQQ__Field__c = String.valueOf(SBQQ__QuoteLine__c.SBQQ__Quantity__c);
        insert condition3;      
        
        option1.SBQQ__UnitPrice__c = 0;
        evaluator = new ProductOptionPriceRuleEvaluator(quote.Id, new SBQQ__ProductOption__c[] {option1});
        evaluator.evaluate();   
        System.assertNotEquals(150, option1.SBQQ__UnitPrice__c);
    }   
    
    private static void setUp() {
        Opportunity opp = new Opportunity(Name='Test', StageName='Prospecting', CloseDate=System.today());
        insert opp;
        
        quote = new SBQQ__Quote__c(SBQQ__Opportunity__c= opp.Id, SBQQ__BillingCity__c = 'Test');
        insert quote;
    }

}