/**
 * Created by dpeterson with Traction on Demand on 1/15/2018.
 *
 * This is a refactor of brandMinder_associateWithAccount.Trigger
 */

public with sharing class brandMinder_associateWithAccount {
    private static string RTNAME = 'SMB Care Technical Support';

    public static void execute(List<Case> newCases){
        // get record type map
        Map<ID, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Case.getRecordTypeInfosById();
        // map to reference rcid to Account
        Map<String,Id> rcidToAccount = new Map<String,Id>();
        // set of RCIDs to be passed to SOQL query
        Set<String> rcids = new Set<String>();

        for(Case c : newCases)
        {
            String recordTypeName = c.recordTypeId != null ? rtMap.get(c.recordTypeId).getName() : 'unknown';

            if(recordTypeName == RTNAME && c.brandminder__c == true && c.proactive_case__c == true)
            {
                if (c.rcid_for_account_lookup__c != null) {
                    rcids.add(c.rcid_for_account_lookup__c);
                } else {
                    c.addError('RCID is required!');
                }
            }
        }

        if(!rcids.isEmpty())
        {
            for(Account acct : [SELECT Id, rcid__c FROM Account WHERE rcid__c IN :rcids])
            {
                rcidToAccount.put(acct.RCID__c,acct.Id);
            }
        }

        for(Case c : newCases) {
            if(rcidToAccount.containsKey(c.rcid_for_account_lookup__c)){
                if(c.AccountId == null) c.AccountId = rcidToAccount.get(c.rcid_for_account_lookup__c);
            }
        }
    }
}