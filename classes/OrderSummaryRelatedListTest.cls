@isTest
public class OrderSummaryRelatedListTest {
    private static void setupData(String orderId){
        Note aNote1 = new Note();
        aNote1.title = 'title';
        aNote1.parentId = orderId;
        insert aNote1;
        
        String bodyString1 = 'body';
        Blob bodyBlob1 = Blob.valueof(bodyString1);

        Attachment anAttachment1 = new Attachment();
        anAttachment1.name = 'name';
        anAttachment1.parentId = orderId;
        anAttachment1.body = bodyBlob1;
       // anAttachment1.deleteNoteAttachmentType='Note';
        insert anAttachment1;
        List<Task> tasksToInsert = new List<Task>();
        Task t1 = new Task();
        t1.Status = 'Not Started';
        t1.Action_Item_custom__c = 't1 item';
        t1.Objective_Action_Resource__c = 't1 res';
        t1.OwnerId = UserInfo.getUserId();
        t1.ActivityDate = Date.newInstance(2014, 4, 21);
        t1.WhatId = orderId;
        t1.subject='test';
        t1.description = 'testdesc';
        //t1.deleteTaskEventType=true;
        tasksToInsert.add(t1);
        
        Task t2 = new Task();
        t2.Status = 'In Progress';
        t2.Action_Item_custom__c = 't2 item';
        t2.Objective_Action_Resource__c = 't2 res';
        t2.OwnerId = UserInfo.getUserId();
        t2.ActivityDate = Date.newInstance(2014, 5, 21);
        t2.WhatId = orderId; 
         t2.subject='test';
        t2.description = 'testdesc11';
        //t2.deleteTaskEventType=false;
        tasksToInsert.add(t2);   
        Insert tasksToInsert;
        
        Cloud_Enablement_Team_Member__c orderManager = new Cloud_Enablement_Team_Member__c(Team_Name__c='Order Manager',Order__c=orderId,Team_Member__c=UserInfo.getUserId());
        insert orderManager;
        
        Order ordObj=[select accountid from order where id=:orderId];
         List<work_request__c> lstWRs=new List<work_request__c>();
        work_request__c wr = new work_request__c();
        wr.Account__c = ordObj.accountid;
        wr.order__C=orderId;
        wr.Status__c='In Progress'; 
        wr.Transfer_to_Queue__c = 'Contract/SRT AB';
        wr.ownerid=Userinfo.getUserId();
        lstWRs.add(wr);
        lstWRs.add(new work_request__c(Account__c=ordObj.accountid,order__C=orderId,Transfer_to_Queue__c = 'Contract/SRT AB',Status__c='In Progress',ownerid=Userinfo.getUserId()));
        insert lstWRs;
    }
    @isTest
    private static void getOrderTeamMembers() {
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        PageReference pageRef = Page.OC_OrderSummaryMultiSite;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('parentOrderId', orderId );
        Order ordObj=[select id from Order where id=:orderId];
        Order childObj=[select id from order where parentid__c=:ordObj.id];
        ApexPages.StandardController sc = new ApexPages.standardController(childObj);
        
        Test.startTest();
        OrderSummaryRelatedList cntrl=new OrderSummaryRelatedList(sc);
        cntrl.deleteNoteAttachmentType='Note';
        cntrl.deleteTaskEventType='true';
        setupData(childObj.id);
        setupData(orderId);
        OrdrTestDataFactory.associateContractWithOrder(childObj.id);
        cntrl.getOrderTeamMembers();
        Test.stopTest();
    }
    @isTest
    private static void getNotesAttachments0() {
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        PageReference pageRef = Page.OC_OrderSummaryMultiSite;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('parentOrderId', orderId );
        Order ordObj=[select id from Order where id=:orderId];
        Order childObj=[select id from order where parentid__c=:ordObj.id];
        ApexPages.StandardController sc = new ApexPages.standardController(childObj);
        Test.startTest();
        OrderSummaryRelatedList cntrl=new OrderSummaryRelatedList(sc);
        cntrl.deleteNoteAttachmentType='Note';
        cntrl.deleteTaskEventType='false';
        
        setupData(childObj.id);
        setupData(orderId);
        OrdrTestDataFactory.associateContractWithOrder(childObj.id);
       cntrl.order=childObj;
        OrderSummaryRelatedList.noteQueryListFromContext=null;
        cntrl.getNotesAttachments();
        Test.stopTest();
    }
	@isTest
    private static void getNotesAttachments1() {
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        PageReference pageRef = Page.OC_OrderSummaryMultiSite;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('parentOrderId', orderId );
        Order ordObj=[select id from Order where id=:orderId];
        Order childObj=[select id from order where parentid__c=:ordObj.id];
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.standardController(ordObj);
        OrderSummaryRelatedList cntrl=new OrderSummaryRelatedList(sc);
        cntrl.deleteNoteAttachmentType='Note';
        cntrl.deleteTaskEventType='false';
        
        setupData(childObj.id);
        setupData(orderId);
        cntrl.order=ordObj;
        OrderSummaryRelatedList.noteQueryListFromContext=null;
        OrdrTestDataFactory.associateContractWithOrder(childObj.id);
        cntrl.getNotesAttachments();
        Test.stopTest();
    }
    @isTest
    private static void getTasksEvents0() {
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        PageReference pageRef = Page.OC_OrderSummaryMultiSite;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('parentOrderId', orderId );
        Order ordObj=[select id from Order where id=:orderId];
        Order childObj=[select id from order where parentid__c=:ordObj.id];
        ApexPages.StandardController sc = new ApexPages.standardController(childObj);
        Test.startTest();
        OrderSummaryRelatedList cntrl=new OrderSummaryRelatedList(sc);
        cntrl.deleteNoteAttachmentType='Note';
        cntrl.deleteTaskEventType='true';
        
        setupData(childObj.id);
        setupData(orderId);
        OrdrTestDataFactory.associateContractWithOrder(childObj.id);
        cntrl.getTasksEvents();
        Test.stopTest();
    }
	   @isTest
    private static void getTasksEvents1() {
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        PageReference pageRef = Page.OC_OrderSummaryMultiSite;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('parentOrderId', orderId );
        Order ordObj=[select id from Order where id=:orderId];
        Order childObj=[select id from order where parentid__c=:ordObj.id];
        ApexPages.StandardController sc = new ApexPages.standardController(ordObj);
        Test.startTest();
        OrderSummaryRelatedList cntrl=new OrderSummaryRelatedList(sc);
        cntrl.deleteNoteAttachmentType='Note';
        cntrl.deleteTaskEventType='true';
        
        setupData(childObj.id);
        setupData(orderId);
        OrdrTestDataFactory.associateContractWithOrder(childObj.id);
        cntrl.getTasksEvents();
        Test.stopTest();
    }
    @isTest
    private static void getContracts() {
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        PageReference pageRef = Page.OC_OrderSummaryMultiSite;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('parentOrderId', orderId );
        Order ordObj=[select id from Order where id=:orderId];
        Order childObj=[select id from order where parentid__c=:ordObj.id];
        ApexPages.StandardController sc = new ApexPages.standardController(childObj);
        Test.startTest();
        OrderSummaryRelatedList cntrl=new OrderSummaryRelatedList(sc);
        cntrl.deleteNoteAttachmentType='Note';
        cntrl.deleteTaskEventType='false';
        
        setupData(childObj.id);
        setupData(orderId);
        OrdrTestDataFactory.associateContractWithOrder(childObj.id);
        cntrl.getContracts();
        Test.stopTest();
    }
    @isTest
    private static void getWorkOrders() {
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        PageReference pageRef = Page.OC_OrderSummaryMultiSite;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('parentOrderId', orderId );
        Order ordObj=[select id from Order where id=:orderId];
        Order childObj=[select id from order where parentid__c=:ordObj.id];
        ApexPages.StandardController sc = new ApexPages.standardController(childObj);
         Test.startTest();
        OrderSummaryRelatedList cntrl=new OrderSummaryRelatedList(sc);
        cntrl.deleteNoteAttachmentType='Note';
        cntrl.deleteTaskEventType='true';
       
        setupData(childObj.id);
        setupData(orderId);
        OrdrTestDataFactory.associateContractWithOrder(childObj.id);
        cntrl.getWorkOrders();
        Test.stopTest();
    }
    @isTest
    private static void getCases() {
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        PageReference pageRef = Page.OC_OrderSummaryMultiSite;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('parentOrderId', orderId );
        Order ordObj=[select id from Order where id=:orderId];
        Order childObj=[select id from order where parentid__c=:ordObj.id];
        ApexPages.StandardController sc = new ApexPages.standardController(childObj);
        Test.startTest();
        OrderSummaryRelatedList cntrl=new OrderSummaryRelatedList(sc);
        cntrl.deleteNoteAttachmentType='Note';
        cntrl.deleteTaskEventType='false';
        
        setupData(childObj.id);
        setupData(orderId);
        OrdrTestDataFactory.associateContractWithOrder(childObj.id);
        cntrl.getCases();
        Test.stopTest();
    }
    @isTest
    private static void getNewOrderLineItems() {
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        PageReference pageRef = Page.OC_OrderSummaryMultiSite;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('parentOrderId', orderId );
        Order ordObj=[select id from Order where id=:orderId];
        Order childObj=[select id from order where parentid__c=:ordObj.id];
        ApexPages.StandardController sc = new ApexPages.standardController(childObj);
        Test.startTest();
        OrderSummaryRelatedList cntrl=new OrderSummaryRelatedList(sc);
        cntrl.deleteNoteAttachmentType='Note';
        cntrl.deleteTaskEventType='true';
        
        setupData(childObj.id);
        setupData(orderId);
        OrdrTestDataFactory.associateContractWithOrder(childObj.id);
        cntrl.getNewOrderLineItems();
        Test.stopTest();
    }
    @isTest
    private static void deleteOrderTeamMember() {
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        PageReference pageRef = Page.OC_OrderSummaryMultiSite;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('parentOrderId', orderId );
        Order ordObj=[select id from Order where id=:orderId];
        Order childObj=[select id from order where parentid__c=:ordObj.id];
        ApexPages.StandardController sc = new ApexPages.standardController(childObj);
        Test.startTest();
        OrderSummaryRelatedList cntrl=new OrderSummaryRelatedList(sc);
        cntrl.deleteNoteAttachmentType='Note';
        cntrl.deleteTaskEventType='false';
        
        setupData(childObj.id);
        setupData(orderId);
        OrdrTestDataFactory.associateContractWithOrder(childObj.id);
        cntrl.deleteTeamMemberId='';
        cntrl.deleteOrderTeamMember();
        Test.stopTest();
    }
    @isTest
    private static void loadRelatedList(){
         String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        PageReference pageRef = Page.OC_OrderSummaryMultiSite;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('parentOrderId', orderId );
        Order ordObj=[select id from Order where id=:orderId];
        Order childObj=[select id from order where parentid__c=:ordObj.id];
        ApexPages.StandardController sc = new ApexPages.standardController(childObj);
         Test.startTest();
        OrderSummaryRelatedList cntrl=new OrderSummaryRelatedList(sc);
        cntrl.deleteNoteAttachmentType='Note';
        cntrl.deleteTaskEventType='true';
       
        setupData(childObj.id);
        setupData(orderId);
        OrdrTestDataFactory.associateContractWithOrder(childObj.id);
        cntrl.loadRelatedList();        
        Test.stopTest();
    }
    @isTest
    private static void deleteOrderLineItem(){
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        PageReference pageRef = Page.OC_OrderSummaryMultiSite;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('parentOrderId', orderId );
        Order ordObj=[select id from Order where id=:orderId];
        Order childObj=[select id from order where parentid__c=:ordObj.id];
        ApexPages.StandardController sc = new ApexPages.standardController(childObj);
        Test.startTest();
        OrderSummaryRelatedList cntrl=new OrderSummaryRelatedList(sc);
        cntrl.deleteNoteAttachmentType='Note';
        cntrl.deleteTaskEventType='true';
        
        setupData(childObj.id);
        setupData(orderId);
        OrdrTestDataFactory.associateContractWithOrder(childObj.id);
        cntrl.deleteOrderLineItemId='';
        cntrl.deleteOrderLineItem();        
        Test.stopTest();
    }
    @isTest
    private static void deleteContract(){
           String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        PageReference pageRef = Page.OC_OrderSummaryMultiSite;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('parentOrderId', orderId );
        Order ordObj=[select id from Order where id=:orderId];
        Order childObj=[select id from order where parentid__c=:ordObj.id];
        ApexPages.StandardController sc = new ApexPages.standardController(childObj);
        
        Test.startTest();
        setupData(childObj.id);
        setupData(orderId);
        OrderSummaryRelatedList cntrl=new OrderSummaryRelatedList(sc);
        cntrl.deleteNoteAttachmentType='Note';
        cntrl.deleteTaskEventType='false';
        OrdrTestDataFactory.associateContractWithOrder(childObj.id);
		String contractId=[select id from contract where vlocity_cmt__OrderId__c=:childObj.id].id;
        cntrl.deleteContractId=contractId;
        cntrl.deleteContract();        
        Test.stopTest();
    }
    @isTest
    private static void deleteWorkOrder(){
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        PageReference pageRef = Page.OC_OrderSummaryMultiSite;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('parentOrderId', orderId );
        Order ordObj=[select id from Order where id=:orderId];
        Order childObj=[select id from order where parentid__c=:ordObj.id];
        ApexPages.StandardController sc = new ApexPages.standardController(childObj);
        
        Test.startTest();
        OrderSummaryRelatedList cntrl=new OrderSummaryRelatedList(sc);
        cntrl.deleteNoteAttachmentType='Note';
        cntrl.deleteTaskEventType='true';
        setupData(childObj.id);
        setupData(orderId);
        OrdrTestDataFactory.associateContractWithOrder(childObj.id);
        cntrl.deleteWorkOrderId='';
        cntrl.deleteWorkOrder();        
        Test.stopTest();
    }
    @isTest
    private static void deleteTaskEvent(){
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        PageReference pageRef = Page.OC_OrderSummaryMultiSite;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('parentOrderId', orderId );
        Order ordObj=[select id from Order where id=:orderId];
        Order childObj=[select id from order where parentid__c=:ordObj.id];
        ApexPages.StandardController sc = new ApexPages.standardController(childObj);
        Test.startTest();
        OrderSummaryRelatedList cntrl=new OrderSummaryRelatedList(sc);
        cntrl.deleteNoteAttachmentType='Note';
        cntrl.deleteTaskEventType='true';
        
        setupData(childObj.id);
        setupData(orderId);
        OrdrTestDataFactory.associateContractWithOrder(childObj.id);
		List<Task> taskList=[select id from task where WhatId=:childObj.id];
		String taskId=taskList.get(0).id;
        cntrl.deleteTaskEventId=taskId;
        Event testEvent = new Event(WhatId = childObj.id, DurationInMinutes=10, subject = 'Meeting', ActivityDateTime=date.valueOf('2017-08-08'));
        insert testEvent;
        cntrl.deleteTaskEvent();        
        Test.stopTest();
    }
    @isTest
    private static void deleteNoteAttachment() {
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        PageReference pageRef = Page.OC_OrderSummaryMultiSite;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('parentOrderId', orderId );
        Order ordObj=[select id from Order where id=:orderId];
        Order childObj=[select id from order where parentid__c=:ordObj.id];
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.standardController(childObj);
        OrderSummaryRelatedList cntrl=new OrderSummaryRelatedList(sc);
        cntrl.deleteNoteAttachmentType='Note';
        cntrl.deleteTaskEventType='true';
        
        setupData(childObj.id);
        setupData(orderId);
        OrdrTestDataFactory.associateContractWithOrder(childObj.id);
		List<attachment> attachList=[select id from attachment where parentId=:childObj.id];
		String attchId=attachList.get(0).id;
        cntrl.deleteNoteAttachmentId=attchId;
        cntrl.deleteNoteAttachment();        
        Test.stopTest();
    }
	@isTest 
	private static void auxiliaryTest(){
		 String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        PageReference pageRef = Page.OC_OrderSummaryMultiSite;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('parentOrderId', orderId );
        Order ordObj=[select id from Order where id=:orderId];
        Order childObj=[select id from order where parentid__c=:ordObj.id];
        ApexPages.StandardController sc = new ApexPages.standardController(childObj);
        Test.startTest();
        OrderSummaryRelatedList cntrl=new OrderSummaryRelatedList(sc);
		
		OrdrTestDataFactory.associateContractWithOrder(orderId);
		setupData(childObj.id);
        setupData(orderId);
		System.debug(cntrl.shortOrderId);
		System.debug(cntrl.shortSalesRepresentativeId);
		System.debug(cntrl.hasNotesAttachments);
		System.debug(cntrl.hasNewOrderLineItems);
		System.debug(cntrl.hasTasksEvents);
		System.debug(cntrl.hasOpenTasksEvents);
		System.debug(cntrl.hasTasksEventsHistory);
		System.debug(cntrl.hasWorkRequests);
		System.debug(cntrl.hasWorkOrders);
		System.debug(cntrl.hasCases);			
		System.debug(cntrl.hasContract);
		
		
		Test.stopTest();
	}
}