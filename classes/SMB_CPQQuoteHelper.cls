public class SMB_CPQQuoteHelper {
    public static void createAgreement(string oppId){
        //SMB_CPQQuoteHelper.createApttusAgreement(oppId);
    	//SMB_CPQQuoteHelper.createVlocityAgreement(oppId);
    }

    public static id createAgreement_new(string oppId){

        id contractID = SMB_CPQQuoteHelper.createVlocityAgreement(oppId);
        return contractID;
    }

    //This method creates Agreements for QuoteLines in Accepted Status
    /*public static void createApttusAgreement(String OppId){
        string positiveServiceCharges = '$0.00';
        string negativeServiceCharges = '$0.00'; 
        Opportunity objOpportunity = [Select o.Name,o.OwnerId, o.Order_ID__c,o.Delivery_Method__c,o.Id,o.Primary_Order_Contact__r.Email, o.Primary_Order_Contact__r.Name, 
                                      o.Primary_Order_Contact__c, AccountId,Printed_Agreement_Type__c,Printed_Customer_Rep_Email__c,Printed_Customer_Rep__c,
                                      Printed_Total_MRC__c,Printed_Total_NRC__c,Printed_Service_Address__c, Contract_Signor__r.Name, Contract_Signor__r.Email,
                                      Contract_Signor__c,Printed_Total_MRC_Bundling_Bonus__c, Printed_Voice_Positive_Service_Charges__c, Printed_Voice_Negative_Service_Charges__c,
                                      (Select Id, OpportunityId, Agreement__c,  PricebookEntry.Product2.Id,
                                       Item_ID__c, Service_Address_Id__c, Unit_Price__c, Quantity, Service_Civic_Number__c, 
                                       Service_Street__c, Service_City__c, Province__c, Service_State__c, Service_Country__c,
                                       Service_Postal_Code__c, Printed_Product_Name__c, Business_Tool__c,
                                       Printed_MRC__c,Printed_NRC__c,Printed_Display_Order__c,Printed_Indent_Level__c,
                                       Printed_Child_Product_Name_Derived_MRC__c,Printed_Child_Quantity_Derived_MRC__c, Printed_Child_Amount_Derived_MRC__c,
                                       Printed_Child_Product_Name_Derived_NRC__c,Printed_Child_Quantity_Derived_NRC__c, Printed_Child_Amount_Derived_NRC__c,
                                       Printed_Document_Display_in_MRC__c,Printed_Document_Display_in_NRC__c,
                                       ParentLineNo__c,LineNo__c,
                                       Order_Attribute__c,Printed_Suppression_Flag__c,Element_Type__c
                                       From OpportunityLineItems where Item_ID__c != null)
                                      From Opportunity o 
                                      where o.Id =: OppId limit 1];  
        
        system.debug('Opportunity  is : '+objOpportunity);
        
        if(objOpportunity != null && objOpportunity.OpportunityLineItems != null && objOpportunity.OpportunityLineItems.size()>0 && objOpportunity.Printed_Agreement_Type__c != null && objOpportunity.Printed_Agreement_Type__c != ''){
            
            //Finding Existing Agreements       
            list<Apttus__APTS_Agreement__c>  lstExistingAgreements = [Select a.Id, a.Apttus__Related_Opportunity__c,Apttus__Status__c
                                                                      From Apttus__APTS_Agreement__c a where a.Apttus__Related_Opportunity__c =: OppId 
                                                                      and Apttus__Status__c NOT IN ('Cancelled Request', 'Cancelled')];
            
            if(lstExistingAgreements != null && lstExistingAgreements.size() > 0){
                system.debug('-----Existing  Aggrement_List---'+lstExistingAgreements);  
                for(Apttus__APTS_Agreement__c agr :  lstExistingAgreements){                       
                    agr.Apttus__Status__c = 'Cancelled Request';                                      
                }  
                
                update  lstExistingAgreements;
            }
            
            //Creating agreement that needs to be inserted                       
            
            Apttus__APTS_Agreement__c objAgreement = new Apttus__APTS_Agreement__c();
            //obj.Name='Agreement for '+objOpportunity.Order_ID__c;
            objAgreement.Name = 'Agreement for ' + objOpportunity.Order_ID__c;
            objAgreement.Apttus__Status__c='Request';
            objAgreement.Apttus__Account__c = objOpportunity.AccountId;
            objAgreement.Contract_Signor__c = objOpportunity.Contract_Signor__c;
            objAgreement.Apttus__Primary_Contact__c =  objOpportunity.Primary_Order_Contact__c;                         
            objAgreement.Apttus__Related_Opportunity__c = objOpportunity.Id;
            objAgreement.APTS_Delivery_Method__c = objOpportunity.Delivery_Method__c;
            objAgreement.APTS_Originator_ID__c = objOpportunity.OwnerId;
            objAgreement.APTS_Email_Address_Customer_Auth_Contact__c =  objOpportunity.Contract_Signor__r.Email;
            objAgreement.APTS_Customer_Signing_Authority__c = objOpportunity.Contract_Signor__r.Name;
            objAgreement.Order_Number__c = objOpportunity.Order_ID__c;
            // Code Changes by Puneet - Start
            //objAgreement.Printed_Total_MRC_Bundling_Bonus__c = objOpportunity.Printed_Total_MRC_Bundling_Bonus__c;
            //objAgreement.Printed_Voice_Positive_Service_Charges__c = objOpportunity.Printed_Voice_Positive_Service_Charges__c;
            //objAgreement.Printed_Voice_Negative_Service_Charges__c = objOpportunity.Printed_Voice_Negative_Service_Charges__c;
            positiveServiceCharges = objOpportunity.Printed_Voice_Positive_Service_Charges__c;
            negativeServiceCharges = objOpportunity.Printed_Voice_Negative_Service_Charges__c;
            objAgreement.Printed_Agreement_Type__c = objOpportunity.Printed_Agreement_Type__c;
            objAgreement.Printed_Customer_Rep__c = objOpportunity.Printed_Customer_Rep__c;
            objAgreement.Printed_Customer_Rep_Email__c = objOpportunity.Printed_Customer_Rep_Email__c;
            objAgreement.Printed_Service_Address__c = objOpportunity.Printed_Service_Address__c;
            if(objOpportunity.Printed_Total_MRC__c != null)
                objAgreement.Printed_Total_MRC__c = SMB_Helper.formattedAmount('$' + String.ValueOf(objOpportunity.Printed_Total_MRC__c));
            if(objOpportunity.Printed_Total_NRC__c != null)
                objAgreement.Printed_Total_NRC__c = SMB_Helper.formattedAmount('$' + String.ValueOf(objOpportunity.Printed_Total_NRC__c));
            // objAgreement.Apttus__Contract_End_Date__c=System.today()+7;
            objAgreement.APTS_Agreement_Delivery_Date__c=System.today();  //CF changed from System.today()+7
            
            try{
                insert objAgreement; 
                System.debug(objAgreement.name);
            }catch(Exception e1){
                System.debug('ERROR:' + e1); 
            }
            
            if(objAgreement!= null && objAgreement.Id != null){
                List<OpportunityLineItem> oliForUpdate = new List<OpportunityLineItem>();
                List<Apttus__AgreementLineItem__c> lstAgreementLines = new List<Apttus__AgreementLineItem__c>();
                integer maxDisplay = 0;
                for(OpportunityLineItem oli : objOpportunity.OpportunityLineItems){
                    oli.Agreement__c = objAgreement.Id;
                    oliForUpdate.add(oli);
                    // Code Changes by Puneet Start
                    
                    if(!oli.Printed_Suppression_Flag__c)
                    {
                        Apttus__AgreementLineItem__c objAgreementLine = new Apttus__AgreementLineItem__c();
                        objAgreementLine.Apttus__AgreementId__c = objAgreement.Id;
                        objAgreementLine.Apttus__ProductId__c = oli.PricebookEntry.Product2.Id;
                        objAgreementLine.Apttus__Quantity__c = oli.Quantity;
                        if(oli.Unit_Price__c != null)
                            objAgreementLine.Apttus__ListPrice__c = Decimal.valueOf(oli.Unit_Price__c);
                        
                        //objAgreementLine.Printed_Child_Amount_Derived_MRC__c = oli.Printed_Child_Amount_Derived_MRC__c;
                        objAgreementLine.Printed_Child_Product_Name_Derived_MRC__c = oli.Printed_Child_Product_Name_Derived_MRC__c;
                        //objAgreementLine.Printed_Child_Quantity_Derived_MRC__c = Integer.ValueOf(oli.Printed_Child_Quantity_Derived_MRC__c);
                        //objAgreementLine.Printed_Child_Amount_Derived_NRC__c = oli.Printed_Child_Amount_Derived_NRC__c;
                        objAgreementLine.Printed_Child_Product_Name_Derived_NRC__c = oli.Printed_Child_Product_Name_Derived_NRC__c;
                        //objAgreementLine.Printed_Child_Quantity_Derived_NRC__c = Integer.ValueOf(oli.Printed_Child_Quantity_Derived_NRC__c);
                        objAgreementLine.Printed_Document_Display_in_MRC__c = oli.Printed_Document_Display_in_MRC__c;
                        objAgreementLine.Printed_Document_Display_in_NRC__c = oli.Printed_Document_Display_in_NRC__c;
                        objAgreementLine.Printed_Display_Order__c = oli.Printed_Display_Order__c;
                        objAgreementLine.Printed_Indent_Level__c = oli.Printed_Indent_Level__c;
                        objAgreementLine.Printed_MRC__c = oli.Printed_MRC__c;
                        objAgreementLine.Printed_NRC__c = oli.Printed_NRC__c;
                        objAgreementLine.Printed_Product_Name__c = oli.Printed_Product_Name__c;
                        objAgreementLine.Printed_Suppression_Flag__c = oli.Printed_Suppression_Flag__c;
                        objAgreementLine.Business_Tool__c = oli.Business_Tool__c;
                        objAgreementLine.Order_Attribute__c = oli.Order_Attribute__c;
                        objAgreementLine.Printed_Element_Type__c = oli.Element_Type__c;
                        
                        if(oli.ParentLineNo__c != null)
                            objAgreementLine.ParentLineNo__c = Integer.ValueOf(oli.ParentLineNo__c);
                        
                        if(maxDisplay < Integer.valueOf(oli.Printed_Display_Order__c))
                            maxDisplay = Integer.valueOf(oli.Printed_Display_Order__c);
                        
                        objAgreementLine.LineNo__c = oli.LineNo__c;
                        
                        // Code Changes by Puneet End
                        lstAgreementLines.add(objAgreementLine);
                    }
                }
                if(String.isNotBlank(positiveServiceCharges) && positiveServiceCharges <> '$0.00')
                {
                    maxDisplay += 1;
                    Apttus__AgreementLineItem__c objAgreementLine = new Apttus__AgreementLineItem__c();
                    objAgreementLine.Apttus__AgreementId__c = objAgreement.Id;
                    objAgreementLine.Apttus__Quantity__c = 1;
                    objAgreementLine.Printed_Document_Display_in_MRC__c = false;
                    objAgreementLine.Printed_Document_Display_in_NRC__c = true;
                    objAgreementLine.Printed_Display_Order__c = maxDisplay;
                    objAgreementLine.Printed_Indent_Level__c = 0;
                    objAgreementLine.Printed_MRC__c = '$0.00';
                    objAgreementLine.Printed_NRC__c = positiveServiceCharges;
                    objAgreementLine.Printed_Product_Name__c = 'Service Installation Charges';      
                    lstAgreementLines.add(objAgreementLine);
                }
                if(String.isNotBlank(negativeServiceCharges) &&  negativeServiceCharges<> '$0.00')
                {
                    maxDisplay += 1;
                    Apttus__AgreementLineItem__c objAgreementLine = new Apttus__AgreementLineItem__c();
                    objAgreementLine.Apttus__AgreementId__c = objAgreement.Id;
                    objAgreementLine.Apttus__Quantity__c = 1;
                    objAgreementLine.Printed_Document_Display_in_MRC__c = false;
                    objAgreementLine.Printed_Document_Display_in_NRC__c = true;
                    objAgreementLine.Printed_Display_Order__c = maxDisplay;
                    objAgreementLine.Printed_Indent_Level__c = 0;
                    objAgreementLine.Printed_MRC__c = '$0.00';
                    objAgreementLine.Printed_NRC__c = negativeServiceCharges;
                    objAgreementLine.Printed_Product_Name__c = 'Service Installation Charges Waived';   
                    lstAgreementLines.add(objAgreementLine);
                }
                try{
                    if(oliForUpdate.size()>0){
                        update oliForUpdate;
                    }        
                }catch(Exception e){
                    System.debug('ERROR:' + e); 
                }           
                try{
                    
                    if(lstAgreementLines != null){
                        insert lstAgreementLines;
                    }
                }catch(Exception e){
                    System.debug('ERROR:' + e); 
                }                                                                                               
            }
        }
    }*/
    public static id createVlocityAgreement(id OppId){
        string positiveServiceCharges = '$0.00';
        string negativeServiceCharges = '$0.00'; 
        Contract objAgreement = new Contract();
        Opportunity objOpportunity = [Select o.Name,o.OwnerId, o.Order_ID__c,o.Delivery_Method__c,o.Id,o.Primary_Order_Contact__r.Email, o.Primary_Order_Contact__r.Name, o.Primary_Order_Contact__c, AccountId,Printed_Agreement_Type__c,Printed_Customer_Rep_Email__c,Printed_Customer_Rep__c,
                                      Printed_Total_MRC__c,Printed_Total_NRC__c,Printed_Service_Address__c, Contract_Signor__r.Name, Contract_Signor__r.Email,
                                      Contract_Signor__c,Printed_Total_MRC_Bundling_Bonus__c, Printed_Voice_Positive_Service_Charges__c, Printed_Voice_Negative_Service_Charges__c,
                                      (Select Id, OpportunityId,  PricebookEntry.Product2.Id,Contract__c,
                                       Item_ID__c, Service_Address_Id__c, Unit_Price__c, Quantity, Service_Civic_Number__c, 
                                       Service_Street__c, Service_City__c, Province__c, Service_State__c, Service_Country__c,
                                       Service_Postal_Code__c, Printed_Product_Name__c, Business_Tool__c,
                                       Printed_MRC__c,Printed_NRC__c,Printed_Display_Order__c,Printed_Indent_Level__c,
                                       Printed_Child_Product_Name_Derived_MRC__c,Printed_Child_Quantity_Derived_MRC__c, Printed_Child_Amount_Derived_MRC__c,
                                       Printed_Child_Product_Name_Derived_NRC__c,Printed_Child_Quantity_Derived_NRC__c, Printed_Child_Amount_Derived_NRC__c,
                                       Printed_Document_Display_in_MRC__c,Printed_Document_Display_in_NRC__c,
                                       ParentLineNo__c,LineNo__c,
                                       Order_Attribute__c,Printed_Suppression_Flag__c,Element_Type__c
                                       From OpportunityLineItems where Item_ID__c != null)
                                      From Opportunity o 
                                      where o.Id =: OppId limit 1];  
        
        system.debug('Opportunity  is : '+objOpportunity);
        
        /*if(objOpportunity != null && objOpportunity.OpportunityLineItems != null && objOpportunity.OpportunityLineItems.size()>0 && objOpportunity.Printed_Agreement_Type__c != null && objOpportunity.Printed_Agreement_Type__c != ''){
            
            //Finding Existing Agreements       
             list<Apttus__APTS_Agreement__c>  lstExistingAgreements = [Select a.Id, a.Apttus__Related_Opportunity__c,Apttus__Status__c,Apttus__Status_Category__c
                                                                      From Apttus__APTS_Agreement__c a where a.Apttus__Related_Opportunity__c =: OppId 
                                                                      and Apttus__Status__c  IN ('Activated')];
            
            if(lstExistingAgreements != null && lstExistingAgreements.size() > 0){
                system.debug('-----Existing  Aggrement_List---'+lstExistingAgreements);  
                for(Apttus__APTS_Agreement__c agr :  lstExistingAgreements){                       
                    agr.Apttus__Status_Category__c = 'Cancelled'; 
                    agr.Apttus__Status__c = 'Cancelled Request';                }  
                
                update  lstExistingAgreements;
            }
        }*/
        
        if(objOpportunity != null && objOpportunity.OpportunityLineItems != null && objOpportunity.OpportunityLineItems.size()>0 && objOpportunity.Printed_Agreement_Type__c != null && objOpportunity.Printed_Agreement_Type__c != ''){
            
            //Finding Existing Agreements  
            
            /*
				Added to include Vlocity specific objects
			*/
            
            list<Contract>  lstExistingContracts = [Select a.Id, a.vlocity_cmt__OpportunityId__c,Status,BA_Contract_Status__c
                                                    From Contract a where a.vlocity_cmt__OpportunityId__c =: OppId 
                                                    and Status NOT IN ('Cancelled Request', 'Cancelled','Client Declined', 'Deactivated', 'Acceptance Expired')];
            
            if(lstExistingContracts != null && lstExistingContracts.size() > 0){
                system.debug('-----Existing  Aggrement_List---'+lstExistingContracts);  
                for(Contract agr :  lstExistingContracts){                       
                    //agr.BA_Contract_Status__c = 'Cancelled Request';    
                    if(agr.Status == 'Contract Registered' ){
                        agr.Status = 'Deactivated';   
                    }
                    else {
                        agr.Status = 'Cancelled';   
                    }
                    
                }  
                
                update  lstExistingContracts;
            }
            
            
            //Creating agreement that needs to be inserted                       
            
            // Added for Vlocity specific contract object mapping
            
            //Contract objAgreement = new Contract();
            Id ContractRecordTypeId = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Business Anywhere').getRecordTypeId();
            Id CliRecordTypeId = Schema.SObjectType.vlocity_cmt__ContractLineItem__c.getRecordTypeInfosByName().get('Contract Line Item').getRecordTypeId();

            objAgreement.Name = 'Agreement for ' + objOpportunity.Order_ID__c;
            //objAgreement.BA_Contract_Status__c = 'Request';
            objAgreement.status = 'Draft';
            objAgreement.AccountId = objOpportunity.AccountId;
            objAgreement.CustomerSignedId = objOpportunity.Contract_Signor__c;                       
            objAgreement.vlocity_cmt__OpportunityId__c = objOpportunity.Id;
            objAgreement.delivery_method__c = objOpportunity.Delivery_Method__c;
            objAgreement.OwnerId = objOpportunity.OwnerId;
            objAgreement.order_id__c = objOpportunity.Order_ID__c;
            positiveServiceCharges = objOpportunity.Printed_Voice_Positive_Service_Charges__c;
            negativeServiceCharges = objOpportunity.Printed_Voice_Negative_Service_Charges__c;
            objAgreement.RecordTypeId = ContractRecordTypeId;
            objAgreement.Agreement_Type__c = objOpportunity.Printed_Agreement_Type__c;
            objAgreement.Printed_Customer_Rep__c = objOpportunity.Printed_Customer_Rep__c;
            objAgreement.Printed_Customer_Rep_Email__c = objOpportunity.Printed_Customer_Rep_Email__c;
            objAgreement.Printed_contract_Address__c = objOpportunity.Printed_Service_Address__c;
            
            if(objOpportunity.Printed_Total_MRC__c != null)
                objAgreement.Printed_Total_MRC__c  = SMB_Helper.formattedAmount('$' + String.ValueOf(objOpportunity.Printed_Total_MRC__c));
            if(objOpportunity.Printed_Total_NRC__c != null)
                objAgreement.Printed_Total_NRC__c  = SMB_Helper.formattedAmount('$' + String.ValueOf(objOpportunity.Printed_Total_NRC__c));
            objAgreement.Contract_Delivery_Date__c=System.today();
            System.debug('Contract Details::' + objAgreement);
            
            try{
                insert objAgreement; 
                System.debug(objAgreement.name);
            }catch(Exception e1){
                System.debug('ERROR:' + e1); 
            }
            if(objAgreement!= null && objAgreement.Id != null){
                /*
* Updating Opportunity Line Item with Agreement Ids
*/      
                List<OpportunityLineItem> oliForUpdate = new List<OpportunityLineItem>();
                List<vlocity_cmt__contractlineitem__c> lstAgreementLines = new List<vlocity_cmt__contractlineitem__c>();
                
                integer maxDisplay = 0;
                for(OpportunityLineItem oli : objOpportunity.OpportunityLineItems){
                    oli.Contract__c = objAgreement.Id;
                    oliForUpdate.add(oli);
                    
                    
                    // Creating Vlocity Agreement Line Items          
                    
                    if(!oli.Printed_Suppression_Flag__c)
                    {
                        
                        vlocity_cmt__contractlineitem__c objAgreementLine = new vlocity_cmt__contractlineitem__c();
                        objAgreementLine.RecordTypeId = CliRecordTypeId;
                        objAgreementLine.vlocity_cmt__ContractId__c = objAgreement.Id; // For Master-Detail relationship
                        objAgreementLine.OpportunityId__c = objOpportunity.Id;
                        objAgreementLine.vlocity_cmt__Product2Id__c = oli.PricebookEntry.Product2.Id;
                        objAgreementLine.vlocity_cmt__Quantity__c  = oli.Quantity;
                        if(oli.Unit_Price__c != null)
                            objAgreementLine.vlocity_cmt__UnitPrice__c = Decimal.valueOf(oli.Unit_Price__c);
                        objAgreementLine.TELUS_Printed_MRC__c = oli.Printed_MRC__c;
                        objAgreementLine.TELUS_Printed_NRC__c = oli.Printed_NRC__c;
                        //system.debug('Contract Line Items' + objAgreementLine);
                        objAgreementLine.TELUS_Printed_Document_Display_in_MRC__c = oli.Printed_Document_Display_in_MRC__c;
                        objAgreementLine.TELUS_Printed_Document_Display_in_NRC__c = oli.Printed_Document_Display_in_NRC__c;
                        objAgreementLine.TELUS_Printed_Display_Order__c = oli.Printed_Display_Order__c;
                        objAgreementLine.TELUS_Printed_Indent_Level__c = oli.Printed_Indent_Level__c;
                        objAgreementLine.TELUS_Printed_Product_Name__c = oli.Printed_Product_Name__c;
                        objAgreementLine.TELUS_Printed_Suppression_Flag__c = oli.Printed_Suppression_Flag__c;
                        objAgreementLine.TELUS_Printed_Element_Type__c = oli.Element_Type__c;
                        objAgreementLine.TELUS_Business_Tool__c = oli.Business_Tool__c;
                                   
						//objAgreementLine.Printed_Child_Product_Name_Derived_MRC__c = oli.Printed_Child_Product_Name_Derived_MRC__c;
						objAgreementLine.Printed_Child_Product_Name_Derived_NRC__c = oli.Printed_Child_Product_Name_Derived_NRC__c;

if(maxDisplay < Integer.valueOf(oli.Printed_Display_Order__c))
maxDisplay = Integer.valueOf(oli.Printed_Display_Order__c);

                        if(oli.ParentLineNo__c != null){
                            objAgreementLine.parent_line__c=oli.ParentLineNo__c;
                        }
                        objAgreementLine.Order_Attribute__c=oli.Order_Attribute__c;
                        
                        objAgreementLine.vlocity_cmt__LineNumber__c = string.valueof(oli.LineNo__c);
                        
                        lstAgreementLines.add(objAgreementLine);
                    }
                }
                if(String.isNotBlank(positiveServiceCharges) && positiveServiceCharges <> '$0.00')
                {
                    maxDisplay += 1;
                    vlocity_cmt__contractlineitem__c objAgreementLine = new vlocity_cmt__contractlineitem__c();
                    
                    objAgreementLine.vlocity_cmt__ContractId__c = objAgreement.Id;
                    objAgreementLine.vlocity_cmt__Quantity__c = 1;
                    objAgreementLine.TELUS_Printed_Document_Display_in_MRC__c  = false;
                    objAgreementLine.TELUS_Printed_Document_Display_in_NRC__c  = true;
                    objAgreementLine.TELUS_Printed_Display_Order__c  = maxDisplay;
                    objAgreementLine.TELUS_Printed_Indent_Level__c  = 0;
                    objAgreementLine.TELUS_Printed_MRC__c = '$0.00';
                    objAgreementLine.TELUS_Printed_NRC__c = positiveServiceCharges;
                    objAgreementLine.TELUS_Printed_Product_Name__c  = 'Service Installation Charges'; 
                    
                    lstAgreementLines.add(objAgreementLine);
                }
                if(String.isNotBlank(negativeServiceCharges) &&  negativeServiceCharges<> '$0.00')
                {
                    maxDisplay += 1;
                    vlocity_cmt__contractlineitem__c objAgreementLine = new vlocity_cmt__contractlineitem__c();
                    
                    objAgreementLine.vlocity_cmt__ContractId__c = objAgreement.Id;
                    objAgreementLine.vlocity_cmt__Quantity__c = 1;
                    objAgreementLine.TELUS_Printed_Document_Display_in_MRC__c  = false;
                    objAgreementLine.TELUS_Printed_Document_Display_in_NRC__c  = true;
                    objAgreementLine.TELUS_Printed_Display_Order__c = maxDisplay;
                    objAgreementLine.TELUS_Printed_Indent_Level__c  = 0;
                    objAgreementLine.TELUS_Printed_MRC__c = '$0.00';
                    objAgreementLine.TELUS_Printed_NRC__c = negativeServiceCharges;
                    objAgreementLine.TELUS_Printed_Product_Name__c = 'Service Installation Charges Waived';   
                    
                    lstAgreementLines.add(objAgreementLine);
                }
                try{
                    if(oliForUpdate.size()>0){
                        update oliForUpdate;
                    }        
                }catch(Exception e){
                    util.emailsystemerror(e);
                }           
                try{
                    
                    if(lstAgreementLines != null){
                        insert lstAgreementLines;
                        map<string,id> lineItemIdMap=new map<string,id>();
                        for(vlocity_cmt__contractlineitem__c contractLineItem:lstAgreementLines){
                            if(string.isnotblank(contractLineItem.vlocity_cmt__linenumber__c)){
                                lineItemIdMap.put(contractLineItem.vlocity_cmt__linenumber__c,contractLineItem.id);
                                System.debug('lineItemIdMap'+ lineItemIdMap);
                            }
                        }
                        for(vlocity_cmt__contractlineitem__c contractLineItem:lstAgreementLines){
                            if(string.isnotblank(contractLineItem.parent_line__c)){
                                contractLineItem.parent_contract_line_item__c=lineItemIdMap.get(string.valueof(integer.valueof(contractLineItem.parent_line__c)));
                            	System.debug('contractLineItem.parent_contract_line_item__c'+ contractLineItem.parent_contract_line_item__c);
                            }
                        }
                        update lstAgreementLines;
                    }
                }catch(Exception e){
                    util.emailsystemerror(e);
                }                                                                                               
            }
        }
        return objAgreement.Id;
    }
    
    public static void UpdateContractsToCancelled(String oppId){
/*
        list<Apttus__APTS_Agreement__c> lstAgreement = [Select Id, Apttus__Status_Category__c from Apttus__APTS_Agreement__c where Apttus__Related_Opportunity__c =: oppId and Apttus__Status_Category__c <> 'Cancelled'];
        if(lstAgreement!= null && lstAgreement.size()>0){
            for(Apttus__APTS_Agreement__c aa: lstAgreement)
            {
                aa.Apttus__Status_Category__c = 'Cancelled';
            }
            
            update lstAgreement;
        }*/
    }
    
    
    public static void UpdateContractsToCancelled_vlocity(String oppId){
        list<Contract>  lstExistingAgreements = [Select a.Id, a.vlocity_cmt__OpportunityId__c,Status From Contract a where a.vlocity_cmt__OpportunityId__c=:OppId and Status NOT IN ('Cancelled Request', 'Cancelled','Client Declined', 'Deactivated', 'Acceptance Expired')];
        for(contract agreement:lstExistingAgreements){
            if(agreement.Status == 'Contract Registered' ){
                agreement.Status = 'Deactivated';   
            }
            else {
                agreement.Status = 'Cancelled';   
            }
        }
        try{
            update lstExistingAgreements; 
        }Catch (Exception e){
            throw(e);
        }
    }
}