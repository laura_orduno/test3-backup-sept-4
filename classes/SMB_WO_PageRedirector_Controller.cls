public class SMB_WO_PageRedirector_Controller 
{
    public string originalTabId{get;set;}
    public string url{get;set;}
    public string oppName {get;set;}
    
    public SMB_WO_PageRedirector_Controller()
    {
        string foboId = ApexPages.currentPage().getParameters().get('FOBOOppId');
        //url = '/' + ApexPages.currentPage().getParameters().get('FOBOOppId') + '?nonce=' + ApexPages.currentPage().getParameters().get('nonce') + '&sfdcIFrameOrigin=' + ApexPages.currentPage().getParameters().get('sfdcIFrameOrigin');
        url = '/' + foboId;
        originalTabId= ApexPages.currentPage().getParameters().get('pTabId');
        list<Opportunity> oppList = [Select id, name from Opportunity where Id = :foboId];
        if(oppList == null || oppList.size()<=0)
        {
            oppName = 'New Order';
        }
        else
        {
            oppName = oppList.get(0).Name;
        }              
    }
    
}