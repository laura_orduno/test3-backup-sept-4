@isTest
private class trac_RCID_CBUCID_NCSC_IndicatorTest {

    static testMethod void testTrigger() {
    // code coverage only
       Account r = new Account(name = 'test');
       insert r;
       
       Segment__c seg = new Segment__c(
            Name = 'test',
            CBU_Abbreviated_Name__c = 'abrv_name',
            CBU_Code__c = 'aaa',
            CBU_Description__c = 'desc',
            CBU_Name__c = 'cbuName',
            CSH_Managing_Director__c = 'director',
            CSH_Segment_Name__c = 'csh_segment_name',
            CSH_Sub_Segment_Description__c = 'sub_seg_desc',
            Channel_Region__c = 'na',
            ILEC_Flag__c = 'flag',
            Marketing_Managing_Director__c = 'marketing_director',
            Marketing_Segment_Name__c = 'marketing_seg',
            Marketing_Sub_Segment_Description__c = 'marketing_sub_seg_desc',
            Marketing_Sub_Segment_Short_Description__c = 'marketing_sub_seg_short_desc'
        );
        insert seg;
        
        Cust_Business_Unit_Cust_ID__c c = new Cust_Business_Unit_Cust_ID__c(
            name = 'test',
            CBUCID__c = 'aaaaaaaaaa',
            CBU_Name__c = 'cbu_name',
            CBU_CD__c = 'aaa',
            Alt_Segment__c = 'test_alt_seg',
            Sub_Segment_Code__c = seg.id
        );
        insert c;
        
        AtoZ_CustStatusTool__c aToZ = new AtoZ_CustStatusTool__c(CBUCID__c = c.CBUCID__c);
        insert aToZ;
        
        Employee__c e = new Employee__c(
            Full_Name__c = 'test',
            Email__c = 'employee@example.com',
            Manager_Email__c = 'manager@example.com',
            Director_Email__c = 'director@example.com',
            Employee_Id__c = '12345',
            TMODS_TEAM_MEMBER_ID__c = 98765
        );
        insert e;
        
        AtoZ_Assignment__c assign = new AtoZ_Assignment__c(
        CBUCID__c = c.id,
        RCID__c = r.id,
        AtoZ_Field__c = 'test_az_field',
        AtoZ_Contact__c = e.Id,
        Is_NCSC__c = true
      );
        insert assign; 
        
      delete assign;
    }
}