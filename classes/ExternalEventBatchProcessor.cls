/*****************************************************
    Name: ExternalEventBatchProcessor
    Usage: This is batch class to process events asyncronousely
    Author – Sandip Chaudhari (IBM)
    Revision History -
    Created By/Date - Sandip Chaudhari / 04 July 2016
    
    Updated By/Date/Reason for Change - 
*/    
global class ExternalEventBatchProcessor implements Database.Batchable<External_Event__c>{
    global List<External_Event__c> eventObjects = new List<External_Event__c>();
    global List<ExternalEventRequestResponseParameter.ResponseEventData> responseData = new List<ExternalEventRequestResponseParameter.ResponseEventData>();
    
    //get all eventObjects
    global ExternalEventBatchProcessor(List<External_Event__c> eventObjs,  List<ExternalEventRequestResponseParameter.ResponseEventData> responseDataP){
        eventObjects.addAll(eventObjs);
        responseData.addAll(responseDataP);
    }
    
    //start events processing
    global Iterable<External_Event__c> start(Database.BatchableContext BC){
        return eventObjects;
    }
    
    //execute events
    global void execute(Database.BatchableContext BC, List<External_Event__c> eventObjects){
        Map<String, List<External_Event__c>> eventMap = new Map<String, List<External_Event__c>>();
        //Create map for each event to bulkify the code 
        system.debug('@@@ eventObjects ' + eventObjects);
        if(eventObjects != null){
            for(External_Event__c eventObject: eventObjects){
                try{
                    List<External_Event__c> eventList;
                    if(eventMap.get(eventObject.Event_Name__c) != null){
                        eventList =  eventMap.get(eventObject.Event_Name__c);
                    }else{
                        eventList =  new List<External_Event__c>();
                    }
                    eventList.add(eventObject);
                    eventMap.put(eventObject.Event_Name__c, eventList);
                }catch (Exception ex){
                    eventObject.Event_Process_Error__c = ex.getMessage() + ' - ' +  ex.getstacktracestring(); 
                }
            }
        }
        system.debug('@@@ eventMap ' + eventMap);
        //Create instance to invoke event handler and pass list of same type of events
        if(eventMap != null && eventMap.size()>0){
            for(String eventName: eventMap.keySet()){
                EventHandlerInterface classObject = ExternalEventManager_Helper.createHandlerInstance(eventName);
                if(classObject != null){
                    if(eventMap.get(eventName) != null){
                        classObject.processEvent(eventMap.get(eventName), responseData);
                    }
                }else{
                    if(eventMap.get(eventName) != null){
                        List<External_Event__c> errorEvents = eventMap.get(eventName);
                        if(errorEvents != null){
                            for(External_Event__c eventObject: errorEvents){
                                eventObject.Event_Process_Error__c = system.label.ExternalEventControllerNameMissing; 
                            }
                        }
                    }
                }
            }
        }
    
        update eventObjects;
    
    }
     
    global void finish(Database.BatchableContext BC){
        
    }
     
}