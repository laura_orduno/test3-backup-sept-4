public without sharing class trac_TAS_Unrestricted {

  public static void deleteTheTAS(ID theTASId,
                                  List<Id> theActivitiesIds,
                                  List<Id> theMilestonesIds,
                                  List<Id> theBusinessPartnersIds,
                                  List<Id> theCompetitorsIds,
                                  List<Id> theDecisionCriteriaIds,
                                  List<Id> theRelationshipStrategiesIds,
                                  List<Id> theTASContactsIds) {
  	
  	// get the tas to delete
    Database.delete(theActivitiesIds);
    Database.delete(theMilestonesIds);
    Database.delete(theBusinessPartnersIds);
    Database.delete(theCompetitorsIds);
    Database.delete(theDecisionCriteriaIds);
    Database.delete(theRelationshipStrategiesIds);
    Database.delete(theTASContactsIds);
    
    Database.delete(theTASId);
  	
  }
}