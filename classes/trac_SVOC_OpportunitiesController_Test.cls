@isTest
public with sharing class trac_SVOC_OpportunitiesController_Test {
   static testMethod void standardExecutionTest() {
        Account acc = smb_test_utility.createAccount('New_Account', True);
        PageReference pageref = Page.trac_SVOC_Opportunities;
        Test.setCurrentPage(pageref);
        pageref.getparameters().put('id', acc.Id);
        trac_SVOC_Opportunities_Controller oc = new trac_SVOC_Opportunities_Controller();
        String baseUrl = oc.baseUrl;
        oc.init();
    }
    static testMethod void missingIdTest() {
        
        PageReference pageref = Page.trac_SVOC_Opportunities;
        Test.setCurrentPage(pageref);
        
        trac_SVOC_Opportunities_Controller oc = new trac_SVOC_Opportunities_Controller();
        String testId = oc.pageInstanceId;
        System.assertNotEquals(testId, '');
        
    }

    static testMethod void testAllowCreationOfRelatedRecords() {
        Account acc = smb_test_utility.createAccount('New_Account', True);
        RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName='RCID'];
        acc.RecordTypeId = rt.Id;
        //Added by Arvind on 01/12/2016 to by pass the new validation rule added on Account
       
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'testUser', Email='standardAccountuser@testorg.com.ocom', 
              EmailEncodingKey='UTF-8', LastName='Testing123', LanguageLocaleKey='en_US', 
              LocaleSidKey='en_US', ProfileId = p.Id, 
              TimeZoneSidKey='America/Los_Angeles', UserName='standarduser66@testing.com.ocom', IsActive = true, Can_Update_Account_Name__c=True);    
        insert u;  
        System.runas(u)
        {
        update acc;
        }
        PageReference pageref = Page.trac_SVOC_Opportunities;
        Test.setCurrentPage(pageref);
        pageref.getparameters().put('id', acc.Id);
        trac_SVOC_Opportunities_Controller oc = new trac_SVOC_Opportunities_Controller();
        oc.init();  
        Boolean allowCreation = oc.AllowCreationOfRelatedRecords;
        System.assertEquals(allowCreation, true);
    }
}