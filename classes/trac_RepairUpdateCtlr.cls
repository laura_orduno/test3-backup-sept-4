/*
 * Controller for the RepairUpdate page
 * Part of the SMB Repairs project
 *
 * @author: Grant Adamson, Traction On Demand 
 */

public without sharing class trac_RepairUpdateCtlr extends trac_RepairCaseCtlr{
	
	public trac_RepairUpdateCtlr() {
		super();
	}
	

	public void doUpdateRepair() {
		if(theCase.repair__c != null) {
			trac_RepairCalloutResponse resp = trac_RepairCallout.getRepair(theCase);
			
			if(resp.success) {
				if(theCase.Repair_Case_Id_Mismatch__c) {
					trac_PageUtils.addError(Label.CASE_ID_MISMATCH);
				} else {
					success = true;
					update theCase;
				}
			} else {
				trac_PageUtils.addError(resp.messageBody);
			}
		} else {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, Label.REPAIR_ID_REQUIRED));
		}
		
		complete = true;
	}
	
}