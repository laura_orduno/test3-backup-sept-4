global class MBRCheckCaseStatus implements Schedulable {
	global void execute(SchedulableContext sc) {
		MBRNewCaseCtlr.updateCasePriorityToUrgent();
	}
}