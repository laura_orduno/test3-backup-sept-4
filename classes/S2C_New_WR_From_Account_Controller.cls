/*
	Created by Tech Mahindra Ltd.    
	Developed b y:  Yogesh Narad,  Priyanka Dhomne
	New WR From Account Controller
	Used for implementing the business logic associated with the Contact Selection Page and its functionalities
    S2C_New_WR_From_Account_Controller.cls
    Part of the TELUS Usability II Projects    
   
*/
public with sharing class S2C_New_WR_From_Account_Controller {
    public class NewWRException extends Exception {} 
    
    public Boolean loaded {get; private set;}
    public Account account {get;set;}
    public Contact[] contacts {get; private set;}
    public String oppName {get;set;}
    public Integer contactsCount {get {if(contacts == null){return 0;} return contacts.size();}}
    
    /* Input Variables */
    public String searchTerm {get; set;}
    public Id selectedContactId {get; set;}
    
    public Boolean showNewContactForm {get; private set;}
    public Contact new_contact {get; set;}
    public Work_Request__c new_wr{get; set;}
    public Id mostRecentlyCreatedContactId {get; private set;}

    public S2C_New_WR_From_Account_Controller() {
        RecordType rt = [SELECT Id, Name, DeveloperName FROM RecordType WHERE SObjectType = 'Work_Request__c' AND DeveloperName = 'Complex_Provisioning_Handoff'];
    	new_wr= new Work_Request__c();
        new_wr.recordtypeId = rt.Id;
    }
    
    public void onLoad() {
        Savepoint sp = database.setSavepoint();
        try {
            loaded = false;
            showNewContactForm = false;
            if (ApexPages.currentPage() == null || ApexPages.currentPage().getParameters() == null) { clear(); return; }
            String accountId = ApexPages.currentPage().getParameters().get('aid');
            if (accountId == null || accountId == '' || accountId.substring(0, 3) != '001') { clear(); return; }
            Id xid = (Id) accountId;
            account = [Select Name, Owner.Name,ParentId From Account Where Id = :xid Limit 1];
            if (account == null) { clear(); throw new NewWRException('Unable to load account'); return; }

            loadContacts();
            if (contacts == null || contacts.size() == 0) { showNewContactForm = true; }
            
            new_contact = new Contact(AccountId = account.Id);
            system.debug('new_contact'+new_contact);
            loaded = true;
        } catch (Exception e) { 
            //system.debug('Caught Exception: ' + ex.getMessage() + ' on line ' + ex.getLineNumber() + '\n stack:' + ex.getStackTraceString());
            clear(); Database.rollback(sp); QuoteUtilities.log(e); 
        }
    }
    
    private void loadContacts() {
        Set<Id> associatedIds = new Set<Id>();
        List<Id> queryIds = new List<Id>() ;
        List<Id> recTypeId = new List<Id>();
        Map<Id,Account> associatedAccounts = new Map<Id,Account>();

        recTypeId.add( Schema.SObjectType.Account.RecordTypeInfosByName.get('CAN').RecordTypeId );
        recTypeId.add( Schema.SObjectType.Account.RecordTypeInfosByName.get('BAN').RecordTypeId );

        if(account != null){
            queryIds.add(account.Id); 
        }
        if( account != null && account.ParentId != null ){
            queryIds.add( account.ParentId );
        }

        //Grab the associated accounts ( the CBUCID Account of the RCID, and all the BAN and CANs from this account) .
        if( queryIds.size() > 0 ){
            associatedAccounts = new Map<Id,Account>( [SELECT Id,ParentId,Name 
                                                        FROM Account 
                                                        Where ParentId in :queryIds
                                                        OR Id in :queryIds
                                                        OR ( ParentId = :account.Id 
                                                            AND RecordTypeId in :recTypeId) limit 48000
                                                          ]);
        }
       
        if(associatedAccounts.size() > 0 ){
            associatedIds = associatedAccounts.keySet();
            contacts = [SELECT Name, Title, Email, Active__c ,Account.RecordTypeId
                            FROM Contact 
                            WHERE AccountId IN :associatedIds
                            ORDER BY Active__c DESC, Account.RecordType.Name DESC,CreatedDate DESC 
                            LIMIT 1000
            ]; 
        }
    }
    
    public PageReference toggleNewContactForm() {
        try {
            showNewContactForm = !showNewContactForm;
        } catch (Exception e) { clear(); QuoteUtilities.log(e); }
        return null;
    }
    
    public void onSearch() {
        try {
            if (searchTerm == null || searchTerm == '') { loadContacts(); return; }
            List<List<sObject>> searchResult = [FIND :searchTerm IN ALL FIELDS RETURNING Contact (name, email, title, Active__c,Account.RecordTypeId Where AccountId =: account.Id) limit 50];
            contacts = ((List<Contact>)searchResult[0]);
        } catch (Exception e) { QuoteUtilities.log(e); } return;
    }
    
    
    public PageReference createNewContact() {
            ApexPages.standardController sc = new ApexPages.standardController(new_contact);
            sc.save();
            if (sc.getId() == null) { return null; }
            new_contact = [Select FirstName, LastName, Name, Email, Title, Phone From Contact Where Id = :sc.getId()];
            
            if (new_contact.Id != null) {
                showNewContactForm = false;
                mostRecentlyCreatedContactId = new_contact.id;
                new_contact = new Contact(AccountId = account.id);
                loadContacts();
            }
            return null;
    }
    
     public PageReference  getSelected(){   
        selectedContactId = ApexPages.currentPage().getParameters().get('contactId');  
        return null;
        }
    
    public PageReference onSelectContact() { 
        Contact selectedcont = new Contact();
        if(selectedContactId == null){
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please Select Contact'));
          return null;
        }else{
            if(selectedContactId != null){
               selectedcont=[select id, name from contact where id=:selectedContactId];
            }
           // RecordType rt = [SELECT Id, Name, DeveloperName FROM RecordType WHERE SObjectType = 'Work_Request__c' AND DeveloperName = 'Complex_Provisioning_Handoff'];
        //new_wr.recordtypeId = rt.Id;
        new_wr.Opportunity__c =(createShellOrder(account.id,selectedcont.id)).Id; 
        Try{
        insert new_wr;
        }
        catch(exception e){
        }    
        Id devRecordTypeId = Schema.SObjectType.Work_Request__c.getRecordTypeInfosByName().get('Complex Provisioning Handoff').getRecordTypeId();     
        Schema.DescribeSObjectResult r = Work_Request__c.sObjectType.getDescribe();
        String keyPrefix = r.getKeyPrefix();
       PageReference pageRef = new PageReference('/'+new_wr.id);
       //PageReference pageRef = new PageReference('/'+keyPrefix+'/e?RecordType='+devRecordTypeId+'&CF00N40000002vihV='+account.name+'&CF00N40000002vihV_lkid='+account.id+'&CF00N40000002vihX='+selectedcont.name+'&CF00N40000002vihX_lkid='+selectedContactId+'&retURL=/'+account.id);
        
        pageRef.setRedirect(true);      
         return pageRef; 
        }      
        return null;      
    }
    
    private void clear() {
        account = null;
        contacts = null;
        new_contact = null;
    }
   
   public Opportunity createShellOrder(Id accId,Id contid){
        RecordType recType = [Select Id From RecordType Where SobjectType = 'Opportunity' And Name = 'SRS Order Request']; 
        Account acc = [select id, Name from Account where Id =: accId limit 1];   
            Opportunity ord_Req_Opp = new Opportunity();
            ord_Req_Opp.Amount = 0;
            ord_Req_Opp.CloseDate = system.today();
            ord_Req_Opp.AccountId = accId;
            ord_Req_Opp.Primary_Order_Contact__c =contid; 
            ord_Req_Opp.Name = acc.Name + '- COMPLEX ORDER - ' +  System.now();
            ord_Req_Opp.StageName = 'Originated';
            ord_Req_Opp.Status__c='Originated';
            ord_Req_Opp.RecordTypeId = recType.Id;
            
            try{
                insert ord_Req_Opp;                
            }catch(Exception e){  system.debug('##########'+e.getCause());}   
            
        return ord_Req_Opp;
    }
   
}