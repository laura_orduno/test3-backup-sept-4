/**
 * Contraller backing configurationSummaty component. The component generates
 * summary of options chosen for the configurable product passed into the component.
 * 
 * @author Max Rudman
 * @since 4/8/2011
 */
public class ConfigurationSummaryController {
	// Input properties
	public ProductOptionSelectionModel selections {get; 
		set {
			options = null;
			summaryGroups = null;
			selections = value;
			if ((selections != null) && (Selections.product != null)) {
				rootOption = new SBQQ__ProductOption__c(SBQQ__UnitPrice__c=selections.product.getVO().getDefaultListPrice());
			}
		}
	}
	public Boolean includeBundled {get; set;}
	public Boolean includeNested {get; set;}
	public String groupField {get; set;}
	public String totalFilterField {get; set;}
	public String totalFilterValue {get; set;}
	public String totalFormatPattern {get; set;}
	public SBQQ__ProductOption__c rootOption {get; private set;}
	
	// Output properties
	private transient List<SBQQ__ProductOption__c> options;
	private transient SummaryGroup[] summaryGroups;
	
	public ConfigurationSummaryController() {
		includeNested = true;
		includeBundled = false;
	}
	
	public SummaryGroup[] getSummaryGroups() {
		if (summaryGroups == null) {
			try {
				summaryGroups = new List<SummaryGroup>();
				if (options == null) {
					options = computeOptionSet();
				}
				Map<String,List<SBQQ__ProductOption__c>> groupMap = groupOptions(options);
				for (String name : groupMap.keySet()) {
					SummaryGroup grp = new SummaryGroup(name);
					for (SBQQ__ProductOption__c option : groupMap.get(name)) {
						grp.options.add(new Option(option));
					}
					summaryGroups.add(grp);
				}
			} catch (Exception e) {
				ApexPages.addMessages(e);
			}
		}
		return summaryGroups;
	}
	
	public Decimal getTotal() {
		try {
			if (options == null) {
				options = computeOptionSet();
			}
			return totalOptions(filterOptions(options));
		} catch (Exception e) {
			ApexPages.addMessages(e);
		}
		return 0;
	}
	
	private List<SBQQ__ProductOption__c> computeOptionSet() {
		List<SBQQ__ProductOption__c> result = new List<SBQQ__ProductOption__c>();
		if (selections != null) {
			if (includeNested) {
				// Build the set of all options selected in all configured products.
				Set<Id> optionIds = selections.getSelectedOptionIds(true);
				if (optionIds.isEmpty()) {
					// Nothing to do.
					return result;
				}
				
				// Now deep load products and selected options 
				List<ProductVO> vos = 
					ProductService.getInstance().findProductsWithSelectedOptionsAndPrices(
						new Set<Id>{selections.productId}, optionIds, null, null).values();
				
				ProductModel product = new ProductModel(vos[0]);
				collectOptions(1, result, product, selections);
			} else {
				collectOptions(1, result, selections.product, selections);
			}
			
		}
		return result;
	}
	
	private void collectOptions(Decimal parentQty, List<SBQQ__ProductOption__c> result, ProductModel product, ProductOptionSelectionModel selections) {
		for (ProductOptionSelectionModel.Selection selection : selections.getSelections()) {
			if (selection.selected) {
				ProductOptionModel option = product.getOptionById(selection.optionId);
				SBQQ__ProductOption__c opt = option.mergeWithSelection(selection.editableOption);
				opt.SBQQ__Quantity__c = opt.SBQQ__Quantity__c * parentQty;
				if (!option.vo.record.SBQQ__Bundled__c || (option.vo.record.SBQQ__Bundled__c && includeBundled)) {
					result.add(opt);
				}
				if (option.nested) {
					collectOptions(opt.SBQQ__Quantity__c, result, option.optionalProduct, selection.getNestedSelections());
				}
			}
		}
	}
	
	private Map<String,List<SBQQ__ProductOption__c>> groupOptions(List<SBQQ__ProductOption__c> options) {
		Schema.SObjectField grpField = MetaDataUtils.getField(SBQQ__ProductOption__c.sObjectType, groupField);
		if (grpField == null) {
			return new Map<String,List<SBQQ__ProductOption__c>>{''=>options};
		}
		return (Map<String,List<SBQQ__ProductOption__c>>)QuoteUtils.groupByField(options, grpField);
	}
	
	private List<SBQQ__ProductOption__c> filterOptions(List<SBQQ__ProductOption__c> options) {
		Schema.SObjectField tfield = MetaDataUtils.getField(SBQQ__ProductOption__c.sObjectType, totalFilterField);
		if (tfield == null) {
			return options;
		}
		List<SBQQ__ProductOption__c> result = new List<SBQQ__ProductOption__c>();
		for (SBQQ__ProductOption__c option : options) {
			Object value = option.get(tfield);
			if (value == totalFilterValue) {
				result.add(option);
			}
		}
		return result;
	}
	
	private Decimal totalOptions(List<SBQQ__ProductOption__c> options) {
		Decimal total = ((selections == null) || (Selections.product == null)) ? 0 : selections.product.getVO().getDefaultListPrice();
		if (total == null) {
			total = 0;
		}
		for (SBQQ__ProductOption__c option : options) {
			Decimal qty = (option.SBQQ__Quantity__c == null) ? 1 : option.SBQQ__Quantity__c;
			total += (qty * (option.SBQQ__UnitPrice__c != null ? option.SBQQ__UnitPrice__c : 0));
		}
		return total;
	}
	
	public Integer getTotalProductCount() {
		// Add one to count the root level product
		return 1 + selections.getNestedSelectedSelectionCount();
	}
	
	public class SummaryGroup {
		public String title {get; set;}
		public Option[] options {get; set;}
		
		public SummaryGroup(String title) {
			this.title = title;
			options = new List<Option>();
		}
	}
	
	public class Option {
		public SBQQ__ProductOption__c target {get; set;}
		
		public Option(SBQQ__ProductOption__c target) {
			this.target = target;
		}
		
		public Decimal getTotalPrice() {
			return target.SBQQ__Quantity__c * target.SBQQ__UnitPrice__c;
		}
	}
}