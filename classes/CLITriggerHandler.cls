public class CLITriggerHandler {
    public static Map<id,RecordType> recordTypeMap;
    //Map<String, Boolean> contractRulesMap;
    public static Map<String, vlocity_cmt__CatalogProductRelationship__c> contractableProductsMap;
    // Map<id,RecordType> product2RecordTypeMap;
    public static Map<Id,Contract> contractIdContractMap;
    public static List<Order> allOrdersObj;
    public static Map<id,Contract> contractIdObjMap;
    public static Map<Id,Product2> prod2Map;
    public static Map<Id,Contract> originalContractIdmap;
    public static Map<String,Id> contractRecTypes;
    public static Set<Id> amendedContractsSet;
    public CLITriggerHandler(){
        if(trigger.isexecuting){
            if(recordTypeMap==null){
                recordTypeMap=new Map<id,RecordType>([SELECT Id, Name, DeveloperName, SobjectType FROM RecordType where SobjectType ='vlocity_cmt__ContractLineItem__c']);
            }
            
            //product2RecordTypeMap=new Map<id,RecordType>([SELECT Id, Name, DeveloperName, SobjectType FROM RecordType where SobjectType ='Product2']);
            if(trigger.isbefore){
                beforeInit();
                if(trigger.isinsert){
                    beforeInsert();
                }else if(trigger.isupdate){
                    beforeUpdate();
                }
            }else if(trigger.isafter){
                afterInit();
                if(trigger.isinsert){
                    afterInsert();
                }else if(trigger.isupdate){
                    afterUpdate();
                }
            }
        }
    }
    /**
* Shared initializations in all BEFORE events.
*/
    void beforeInit(){
       /*  contractRulesMap = new Map<String, Boolean>();
        for (Contract_Rule__mdt rule : [select Id, DeveloperName, Is_Contract_Required_on_MTM__c from Contract_Rule__mdt where Contract_Record_Type__c in( 'Contract')]) {
            contractRulesMap.put(rule.DeveloperName, rule.Is_Contract_Required_on_MTM__c);
        }*/
        
        //Map<String,Id> contracReplacetRecordTypeMap; 
    
        //contracReplacetRecordTypeMap=RecordTypeUtil.GetRecordTypeIdsByDeveloperName(Contract_Replacements__c.SObjectType);
        if(contractableProductsMap==null){
            Map<String, Boolean> contractRulesMap = new Map<String, Boolean>();
            for (Contract_Rule__mdt rule : [select Id, DeveloperName, Is_Contract_Required_on_MTM__c from Contract_Rule__mdt where Contract_Record_Type__c = 'Contract']) {
                contractRulesMap.put(rule.DeveloperName, rule.Is_Contract_Required_on_MTM__c);
            }
            
            contractableProductsMap = new Map<String, vlocity_cmt__CatalogProductRelationship__c>();
            for (vlocity_cmt__CatalogProductRelationship__c cpr : [Select id, vlocity_cmt__Product2Id__c, vlocity_cmt__CatalogId__r.Name From vlocity_cmt__CatalogProductRelationship__c where vlocity_cmt__CatalogId__r.Name in :contractRulesMap.keySet()]) {
                
                contractableProductsMap.put(cpr.vlocity_cmt__Product2Id__c, cpr);
            } 
        }
        
        
        
        if(contractIdContractMap==null){
            Set<Id> contractIdSet=new Set<Id>();
            for(sobject record:trigger.new){
                vlocity_cmt__ContractLineItem__c newCli=(vlocity_cmt__ContractLineItem__c)record;
                if(String.isNotBlank(newCli.vlocity_cmt__ContractId__c) && !contractIdSet.contains(newCli.vlocity_cmt__ContractId__c)){
                    contractIdSet.add(newCli.vlocity_cmt__ContractId__c);
                }  
            }
            contractIdContractMap=new Map<Id,Contract>([select id,Effective_Date__c from contract where id in:contractIdSet]);
        }
        
    }
    /**
* Shared initializations in all AFTER events.
*/
    void afterInit(){}
    void beforeInsert(){
        Set<Id> setOfOlis=new Set<Id>();
        Set<Id> contractIdSet=new Set<Id>();
        //Set<Id> originalContractIdSet=new Set<Id>();
        if(contractRecTypes==null){
            contractRecTypes = RecordTypeUtil.GetRecordTypeIdsByDeveloperName(vlocity_cmt__ContractLineItem__c.SObjectType);
        }
         
        for(sobject record:trigger.new){                
            vlocity_cmt__ContractLineItem__c newCli=(vlocity_cmt__ContractLineItem__c)record;
            //RTA-648 - added condition of OrderLineItemId__c for order2.0 Contract Line record type
            if(String.isNotBlank(newCli.OrderLineItemId__c)){
               setOfOlis.add(newCli.OrderLineItemId__c); 
            }
            
            if(String.isNotBlank(newCli.OrderLineItemId__c) && contractRecTypes!=null && contractRecTypes.containsKey('Contract_Line_Item')){
                newCli.RecordTypeId = contractRecTypes.get('Contract_Line_Item');
            }else if(String.isNotBlank(newCli.Service_Request__c) && contractRecTypes!=null && contractRecTypes.containsKey('eContract_Line_Item') ){
                newCli.RecordTypeId = contractRecTypes.get('eContract_Line_Item');
            }else if(String.isNotBlank(newCli.OrderLineItemId__c) && contractRecTypes!=null && contractRecTypes.containsKey('Contract_Line_Item')){
                newCli.RecordTypeId = contractRecTypes.get('Contract_Line_Item');
            }else if(String.isBlank(newCli.OrderLineItemId__c) && String.isNotBlank(newCli.Rate_Plan_Item__c) && contractRecTypes!=null && contractRecTypes.containsKey('Corporate_Wireless_Line_Item')){
                newCli.RecordTypeId = contractRecTypes.get('Corporate_Wireless_Line_Item');  
            }
            if(String.isNotBlank(newCli.vlocity_cmt__ContractId__c) && !contractIdSet.contains(newCli.vlocity_cmt__ContractId__c)){
                contractIdSet.add(newCli.vlocity_cmt__ContractId__c);
            } 
            
           /* if(String.isNotBlank(newCli.Original_Contract__c) && !originalContractIdSet.contains(newCli.Original_Contract__c)){
                originalContractIdSet.add(newCli.Original_Contract__c);
            }*/
            if(newCli.Term_Date__c==null && contractIdContractMap!=null && contractIdContractMap.containsKey(newCli.vlocity_cmt__ContractId__c)){
                Datetime dT=contractIdContractMap.get(newCli.vlocity_cmt__ContractId__c).Effective_Date__c;				
                 if(dT!=null){
                	newCli.Term_Date__c=date.newinstance(dT.year(), dT.month(), dT.day());
                 }
            }
            if( String.isNotBlank(newCli.term__c)){
                if(String.isBlank(String.valueOf(newCli.Contract_Term__c)) || newCli.Contract_Term__c!=Integer.valueOf(newCli.term__c.trim() )){
                   newCli.Contract_Term__c=Integer.valueOf(newCli.term__c.trim()); 
                }                
            }
        }
        
        List<Contract> contractList=[SELECT Id, (select id,OrderLineItemId__c from vlocity_cmt__ContractLineItems__r)
                                     from contract where id in :contractIdSet];
        for(Contract conObj:contractList){
            if(conObj.vlocity_cmt__ContractLineItems__r!=null){
                for(vlocity_cmt__ContractLineItem__c conLineObj:conObj.vlocity_cmt__ContractLineItems__r){
                    if(!setOfOlis.contains(conLineObj.OrderLineItemId__c)){
                        setOfOlis.add(conLineObj.OrderLineItemId__c);
                    }
                } 
            }
        }
        updateMRCNRC(setOfOlis);
        updateContractLineItemProperties(contractIdSet);
        //calculateTLC(product2IdSet);
        //setContractStatusTerminated(originalContractIdSet);
    }
    void updateMRCNRC(Set<Id> setOfOlis){
        if(setOfOlis==null || setOfOlis.isEmpty()){
            return;            
        }
        //  List<OrderItem> allOiList=[select id,vlocity_cmt__RootItemId__c,vlocity_cmt__RecurringTotal__c,vlocity_cmt__OneTimeTotal__c from orderitem 
        //                            where id in :setOfOlis];
        if(allOrdersObj==null){
            allOrdersObj=[select id,(select orderid,id,vlocity_cmt__ParentItemId__c,vlocity_cmt__RootItemId__c,
                                             vlocity_cmt__RecurringTotal__c,
                                             vlocity_cmt__OneTimeTotal__c from orderitems) from order where
                                  id in (select orderid from orderitem where id in :setOfOlis)];        
        }
        
        Map<Id,Set<Id>> ordIdOiIdMapArg=new Map<Id,Set<Id>>();
        Map<Id,Set<Id>> ordIdOiIdMapAll=new Map<Id,Set<Id>>();
        Map<Id,Id> oiIdRootIdMap=new Map<Id,Id>();
        Map<Id,Id> oiIdParentIdMap=new Map<Id,Id>();
        Map<Id,OrderItem> oiIdOrderItemMap=new Map<Id,OrderItem>();
        for(Order ordInstance:allOrdersObj){
            if(ordInstance.orderitems!=null){
                for(OrderItem oiInstance:ordInstance.orderitems){
                    oiIdRootIdMap.put(oiInstance.id,oiInstance.vlocity_cmt__RootItemId__c);
                    oiIdParentIdMap.put(oiInstance.id,oiInstance.vlocity_cmt__ParentItemId__c);
                    oiIdOrderItemMap.put(oiInstance.id,oiInstance);
                    if(setOfOlis.contains(oiInstance.id)){
                        if(ordIdOiIdMapArg.containsKey(oiInstance.orderid)){
                            Set<Id> oiIdSet=ordIdOiIdMapArg.get(oiInstance.orderid);
                            oiIdSet.add(oiInstance.id);
                            ordIdOiIdMapArg.put(oiInstance.orderid,oiIdSet);
                        } else {
                            Set<Id> oiIdSet=new Set<Id>();
                            oiIdSet.add(oiInstance.id);
                            ordIdOiIdMapArg.put(oiInstance.orderid,oiIdSet);
                        }
                    }
                    if(ordIdOiIdMapAll.containsKey(oiInstance.orderid)){
                        Set<Id> oiIdSet=ordIdOiIdMapAll.get(oiInstance.orderid);
                        oiIdSet.add(oiInstance.id);
                        ordIdOiIdMapAll.put(oiInstance.orderid,oiIdSet);
                    } else {
                        Set<Id> oiIdSet=new Set<Id>();
                        oiIdSet.add(oiInstance.id);
                        ordIdOiIdMapAll.put(oiInstance.orderid,oiIdSet);
                    }
                }
            }
            
        }
        Map<String,Double> rootItemMissingMrcPriceMap=new Map<String,Double>();
        Map<String,Double> rootItemMissingNrcPriceMap=new Map<String,Double>();
        for(Id argOrdId:ordIdOiIdMapArg.keySet()){
            Set<Id> argOiIdSet=ordIdOiIdMapArg.get(argOrdId);
            Set<Id> allOiIdSet=ordIdOiIdMapAll.get(argOrdId);
            Set<Id> allMissingOiIdSet=new Set<Id>();
            
            if(allOiIdSet!=null && !allOiIdSet.isEmpty()){
                allMissingOiIdSet.addAll(allOiIdSet);
                allMissingOiIdSet.removeAll(argOiIdSet);
            }
            if(allMissingOiIdSet!=null && !allMissingOiIdSet.isEmpty()){
                for(Id missingOiIdInstance:allMissingOiIdSet){
                    Id rootId=oiIdRootIdMap.get(missingOiIdInstance);
                    Double missingMRC=oiIdOrderItemMap.get(missingOiIdInstance).vlocity_cmt__RecurringTotal__c;
                    Double missingNRC=oiIdOrderItemMap.get(missingOiIdInstance).vlocity_cmt__OneTimeTotal__c;
                    if(rootItemMissingMrcPriceMap.containsKey(rootId)){
                        Double storedMRC=rootItemMissingMrcPriceMap.get(rootId);
                        storedMRC=storedMRC+missingMRC;
                        rootItemMissingMrcPriceMap.put(rootId,storedMRC);
                    } else {
                        rootItemMissingMrcPriceMap.put(rootId,missingMRC);
                    }  
                    
                    if(rootItemMissingNrcPriceMap.containsKey(rootId)){
                        Double storedNRC=rootItemMissingNrcPriceMap.get(rootId);
                        storedNRC=storedNRC+missingNRC;
                        rootItemMissingNrcPriceMap.put(rootId,storedNRC);
                    } else {
                        rootItemMissingNrcPriceMap.put(rootId,missingNRC);
                    }
                }
            }
            
            
            for(sobject record:trigger.new){
                vlocity_cmt__ContractLineItem__c newCli=(vlocity_cmt__ContractLineItem__c)record;
                String oilId=newCli.OrderLineItemId__c;
                if(rootItemMissingMrcPriceMap.containsKey(oilId)){
                    newCli.vlocity_cmt__RecurringTotal__c=newCli.vlocity_cmt__RecurringTotal__c+rootItemMissingMrcPriceMap.get(oilId);
                }
                if(rootItemMissingNrcPriceMap.containsKey(oilId)){                
                    newCli.vlocity_cmt__OneTimeTotal__c =newCli.vlocity_cmt__OneTimeTotal__c+rootItemMissingNrcPriceMap.get(oilId);
                }
            }
            
        }
        
    }
    @TestVisible
     void finalizeContractReplacementLineItems(Set<Id> replacedcontractIdsList){
         List<Contract> conList=[select status from contract where id in :replacedcontractIdsList];
         Set<String> registeredId=new Set<String>();
         for(Contract conOb:conList){
             if(String.isNotBlank(conOb.status) && 'Contract Registered'.equalsIgnoreCase(conOb.status)){
                 if(String.valueOf(conOb.id).length()>15){
                     registeredId.add(String.valueOf(conOb.id).substring(0,15));
                 } else{
                     registeredId.add(conOb.id);
                 }                 
             }
         }
         if(trigger.new!=null){  
             for(sobject record:trigger.new){
                 vlocity_cmt__ContractLineItem__c newCli=(vlocity_cmt__ContractLineItem__c)record;
                 
                 if(String.isNotBlank(newCli.Replaced_by_Contract__c) ){
                     String newId=newCli.Replaced_by_Contract__c;
                     if(String.isNotBlank(newId) && newId.length()>15){
                         newId=newId.substring(0,15);
                     } 
                     if(registeredId.contains(newId)){
                         if(String.isBlank(newCli.Requirement_for_Termination__c)){
                             newCli.vlocity_cmt__Status__c='Inactive';
                         }
                     }
                 }
             }
         }
    }
    void beforeUpdate(){
        
        Set<Id> contractIdSet=new Set<Id>();
        Set<Id> setOfOlis=new Set<Id>();
        Set<Id> originalContractIdSet=new Set<Id>();
        Set<Id> toReplaceContractList=new Set<Id>();
       // Map<String,Id> contractRecTypes = RecordTypeUtil.GetRecordTypeIdsByDeveloperName(vlocity_cmt__ContractLineItem__c.SObjectType);
        if(contractRecTypes==null){
            contractRecTypes = RecordTypeUtil.GetRecordTypeIdsByDeveloperName(vlocity_cmt__ContractLineItem__c.SObjectType);
        }
        for(sobject record:trigger.new){
            vlocity_cmt__ContractLineItem__c newCli=(vlocity_cmt__ContractLineItem__c)record;
            System.debug('inside triggerhelper newCli.OrderLineItemId__c='+newCli.OrderLineItemId__c);
            System.debug('inside triggerhelper newCli.Replaced_by_Contract__c='+newCli.Replaced_by_Contract__c+' String.isNotBlank(newCli.Replaced_by_Contract__c)='+String.isNotBlank(newCli.Replaced_by_Contract__c));
            if(String.isNotBlank(newCli.vlocity_cmt__ContractId__c) && !contractIdSet.contains(newCli.vlocity_cmt__ContractId__c)){
                contractIdSet.add(newCli.vlocity_cmt__ContractId__c);
            }  
            if(String.isNotBlank(newCli.OrderLineItemId__c) && (newCli.OrderLineItemId__c instanceof Id) ){
               setOfOlis.add(newCli.OrderLineItemId__c); 
            }
            if(String.isNotBlank(newCli.Replaced_by_Contract__c)  ){
                String newVal=newCli.Replaced_by_Contract__c;
                toReplaceContractList.add(newVal);
                /*String oldVal=null;
                    if(Trigger.oldMap.get(newCli.id)!=null && 
                       String.isNotBlank(Trigger.oldMap.get(newCli.id).Replaced_by_Contract__c)){
                          oldVal= Trigger.oldMap.get(newCli.id).Replaced_by_Contract__c;
                       }
                if(String.isBlank(oldVal) || !newVal.equalsIgnoreCase(oldVal)){
                    toReplaceContractList.add(newVal);
                }*/
            }
            if(String.isNotBlank(newCli.Original_Contract__c) && String.isNotBlank(newCli.vlocity_cmt__ContractId__c) && newCli.vlocity_cmt__ContractId__c!=newCli.Original_Contract__c 
               && !originalContractIdSet.contains(newCli.Original_Contract__c)){
                originalContractIdSet.add(newCli.Original_Contract__c);
            }
            if(newCli.Term_Date__c==null && contractIdContractMap!=null && contractIdContractMap.containsKey(newCli.vlocity_cmt__ContractId__c)){
                Datetime dT=contractIdContractMap.get(newCli.vlocity_cmt__ContractId__c).Effective_Date__c;				
                 if(dT!=null){
                	newCli.Term_Date__c=date.newinstance(dT.year(), dT.month(), dT.day());
                 }
            }
           
            if( String.isNotBlank(newCli.term__c)){
                if(String.isBlank(String.valueOf(newCli.Contract_Term__c)) || newCli.Contract_Term__c!=Integer.valueOf(newCli.term__c.trim() )){
                   newCli.Contract_Term__c=Integer.valueOf(newCli.term__c.trim()); 
                }                
            }
        }
        
        
        updateContractLineItemProperties(contractIdSet);
        List<Contract> contractList=[SELECT Id, (select id,OrderLineItemId__c from vlocity_cmt__ContractLineItems__r)
                                     from contract where id in :contractIdSet];
        for(Contract conObj:contractList){
            if(conObj.vlocity_cmt__ContractLineItems__r!=null){
                for(vlocity_cmt__ContractLineItem__c conLineObj:conObj.vlocity_cmt__ContractLineItems__r){
                    if(!setOfOlis.contains(conLineObj.OrderLineItemId__c)){
                        setOfOlis.add(conLineObj.OrderLineItemId__c);
                    }
                } 
            }
        }
        updateMRCNRC(setOfOlis);
        //calculateTLC(product2IdSet);
        checkTLCCalculationRequired();
       // setContractStatusTerminated(originalContractIdSet);
       if(!toReplaceContractList.isEmpty()){
            finalizeContractReplacementLineItems(toReplaceContractList);
        }
    }
    void afterInsert(){
		Map<Id,Id> oiIdClidMap=new Map<Id,Id>();
		Set<Id> originalContractIdSet=new Set<Id>();
		for(sobject record:trigger.new){
            vlocity_cmt__ContractLineItem__c newCli=(vlocity_cmt__ContractLineItem__c)record;
			if(String.isNotBlank(newCli.OrderLineItemId__c)){
				oiIdClidMap.put(newCli.OrderLineItemId__c,record.id);
			}
			if(String.isNotBlank(newCli.vlocity_cmt__ContractId__c) && String.isNotBlank(newCli.Original_Contract__c) 
               && newCli.vlocity_cmt__ContractId__c!=newCli.Original_Contract__c && !originalContractIdSet.contains(newCli.Original_Contract__c)){
                originalContractIdSet.add(newCli.Original_Contract__c);
            }
		}
		//setContractStatusTerminated(originalContractIdSet);
		List<OrderItem> oiRecsList=[select id,Contract_Line_Item__c from orderItem where id in :oiIdClidMap.keySet()];
        if(oiRecsList!=null){
           for(OrderItem oiRec:oiRecsList){
                oiRec.Contract_Line_Item__c= oiIdClidMap.get(oiRec.id);              
            } 
            if(oiRecsList.size()>0){
                update oiRecsList;
            }
            
        }
            
            
	}
    void afterUpdate(){
       /* Set<Id> originalContractIdSet=new Set<Id>();
        for(sobject record:trigger.new){                
            vlocity_cmt__ContractLineItem__c newCli=(vlocity_cmt__ContractLineItem__c)record;
            if(String.isNotBlank(newCli.Original_Contract__c) && 
               String.isNotBlank(newCli.vlocity_cmt__ContractId__c) && newCli.vlocity_cmt__ContractId__c!=newCli.Original_Contract__c 
               && !originalContractIdSet.contains(newCli.Original_Contract__c)){
                   originalContractIdSet.add(newCli.Original_Contract__c);
               }
        }
        setContractStatusTerminated(originalContractIdSet);*/
    }
    void updateContractLineItemProperties(Set<Id> contractIdSet){
        if(contractIdSet==null || contractIdSet.size()==0){
            return;
        }
        /*if(amendedContractsSet==null){
            amendedContractsSet=new Set<Id>();
            amendedContractsSet.add(contractIdSet);
        }
        if(!amendedContractsSet.containsAll(contractIdSet)){
            List<Contract> amendedContractsList=[select id,contractnumber,(select id,Original_Contract__c from vlocity_cmt__ContractLineItems__r) from contract where  vlocity_cmt__OriginalContractId__c  in :amendedContractsSet];
            
        }*/
        if(contractIdObjMap==null || !contractIdObjMap.keySet().containsAll(contractIdSet)){
            contractIdObjMap=new Map<Id,Contract>([select id,status,accountid,vlocity_cmt__OriginalContractId__c,Effective_Date__c 
                                                   from contract where id in :contractIdSet]);
        }
        
        for(sobject record:trigger.new){
            vlocity_cmt__ContractLineItem__c newCli=(vlocity_cmt__ContractLineItem__c)record;
			
            if(newCli.Term_Date__c==null){
                if(contractIdObjMap!=null && contractIdObjMap.containsKey(newCli.vlocity_cmt__ContractId__c)){
                    Datetime dT=contractIdObjMap.get(newCli.vlocity_cmt__ContractId__c).Effective_Date__c;				
                    if(dT!=null){
                        newCli.Term_Date__c=date.newinstance(dT.year(), dT.month(), dT.day());
                    }
                }
            }
			
            if(contractIdObjMap!=null && contractIdObjMap.containsKey(newCli.vlocity_cmt__ContractId__c)){
                newCli.Account__c=contractIdObjMap.get(newCli.vlocity_cmt__ContractId__c).accountid;
            }
             
            if(contractIdObjMap!=null && contractIdObjMap.containsKey(newCli.vlocity_cmt__ContractId__c) 
               && String.isNotBlank(contractIdObjMap.get(newCli.vlocity_cmt__ContractId__c).vlocity_cmt__OriginalContractId__c)){
                    newCli.Original_Contract__c=contractIdObjMap.get(newCli.vlocity_cmt__ContractId__c).vlocity_cmt__OriginalContractId__c;
            } else{
                newCli.Original_Contract__c=newCli.vlocity_cmt__ContractId__c;
            }
           /* if(String.isNotBlank(newCli.Contract_Type__c) && 'New'.equalsIgnoreCase(newCli.Contract_Type__c)){
                
            } else {                
                
            }*/
            if(('Terminated'.equalsIgnoreCase(newCli.Service_Status__c)) &&
               (newCli.Terminate_Date__c==null) && recordTypeMap.get(newCli.recordtypeid)!=null &&
               ('eContract_Line_Item'.equalsIgnoreCase(recordTypeMap.get(newCli.recordtypeid).DeveloperName) || 'Contract_Line_Item'.equalsIgnoreCase(recordTypeMap.get(newCli.recordtypeid).DeveloperName))){
                   newCli.Terminate_Date__c=newCli.Effective_Take_Out_Date__c;
               } else if(String.isNotBlank(newCli.Requirement_for_TLC_Calculation__c)  && recordTypeMap.get(newCli.recordtypeid)!=null &&
                         ('eContract_Line_Item'.equalsIgnoreCase(recordTypeMap.get(newCli.recordtypeid).DeveloperName) || 'Contract_Line_Item'.equalsIgnoreCase(recordTypeMap.get(newCli.recordtypeid).DeveloperName))){
                   newCli.BAR_Required__c='TBD';
               } else if(('No'.equalsIgnorecase(newCli.Was_Contract_Replaced__c)) && (newCli.Effective_Take_Out_Date__c!=null && newCli.Expiry_Date__c!=null && newCli.Effective_Take_Out_Date__c<newCli.Expiry_Date__c) && recordTypeMap.get(newCli.recordtypeid)!=null
                         && ('eContract_Line_Item'.equalsIgnoreCase(recordTypeMap.get(newCli.recordtypeid).DeveloperName) || 'Contract_Line_Item'.equalsIgnoreCase(recordTypeMap.get(newCli.recordtypeid).DeveloperName))){
                   newCli.BAR_Required__c='Yes';
               } else if((newCli.New_TCV__c<newCli.TCV_Remaining__c) && ('Yes'.equalsIgnoreCase(newCli.Was_Contract_Replaced__c)) && recordTypeMap.get(newCli.recordtypeid)!=null &&
                         ('eContract_Line_Item'.equalsIgnoreCase(recordTypeMap.get(newCli.recordtypeid).DeveloperName) || 'Contract_Line_Item'.equalsIgnoreCase(recordTypeMap.get(newCli.recordtypeid).DeveloperName))) {
                   newCli.BAR_Required__c='Yes';
               } else {
                   if(recordTypeMap.get(newCli.recordtypeid)!=null && ('eContract_Line_Item'.equalsIgnoreCase(recordTypeMap.get(newCli.recordtypeid).DeveloperName) || 'Contract_Line_Item'.equalsIgnoreCase(recordTypeMap.get(newCli.recordtypeid).DeveloperName))){
                       newCli.BAR_Required__c='No';
                   }
                   
               }
        }
    } 
    void calculateTLC(Set<Id> product2IdSet,Set<Id> cliIdSet){
        if(prod2Map==null){
           prod2Map=new Map<Id,Product2>([select id,Name,RecordType.DeveloperName from product2 where id in :product2IdSet]); 
        }
        
        for(sobject record:trigger.new){
            vlocity_cmt__ContractLineItem__c newCli=(vlocity_cmt__ContractLineItem__c)record;
            if(cliIdSet!=null && cliIdSet.contains(newCli.id)){
                if(('Yes'.equalsIgnoreCase(newCli.Was_Contract_Replaced__c)) && (newCli.New_TCV__c==null) ){
                    newCli.TLC_Amount2__c=null;
                } else if ((newCli.New_TCV__c!=null && newCli.TCV_Remaining__c!=null && newCli.New_TCV__c>=newCli.TCV_Remaining__c) && ('Yes'.equalsIgnoreCase(newCli.Was_Contract_Replaced__c))){
                    newCli.TLC_Amount2__c=0;
                } else if(('No'.equalsIgnoreCase(newCli.Was_Contract_Replaced__c)) && (newCli.Effective_Take_Out_Date__c!=null && newCli.Expiry_Date__c!=null && newCli.Effective_Take_Out_Date__c>=newCli.Expiry_Date__c)){
                    newCli.TLC_Amount2__c=0;
                } else if((prod2Map!=null && prod2Map.get(newCli.vlocity_cmt__Product2Id__c)!=null && 'DID'.equalsIgnoreCase(prod2Map.get(newCli.vlocity_cmt__Product2Id__c).Name) ) && (prod2Map!=null && prod2Map.get(newCli.vlocity_cmt__Product2Id__c)!=null && 'Contractible_Service'.equalsIgnoreCase(prod2Map.get(newCli.vlocity_cmt__Product2Id__c).RecordType.DeveloperName)) ){
					if(newCli.Months_Remaining__c!=null && newCli.vlocity_cmt__RecurringTotal__c!=null){
						newCli.TLC_Amount2__c=0.25*newCli.Months_Remaining__c * newCli.vlocity_cmt__RecurringTotal__c ;  
					}
                              
                } else if((prod2Map!=null && prod2Map.get(newCli.vlocity_cmt__Product2Id__c)!=null && 'DID'!=prod2Map.get(newCli.vlocity_cmt__Product2Id__c).Name && 'Business Long Distance and Toll-free'!=prod2Map.get(newCli.vlocity_cmt__Product2Id__c).Name) && (prod2Map!=null && prod2Map.get(newCli.vlocity_cmt__Product2Id__c)!=null && 'Contractible_Service'.equalsIgnoreCase(prod2Map.get(newCli.vlocity_cmt__Product2Id__c).RecordType.DeveloperName) && ((newCli.Contract_Term__c - newCli.Months_Remaining__c) > 24)) ){
					if(newCli.Months_Remaining__c!=null && newCli.vlocity_cmt__RecurringTotal__c!=null){
                    newCli.TLC_Amount2__c=(0.5*newCli.Months_Remaining__c * newCli.vlocity_cmt__RecurringTotal__c); 
					}
                } else if( (prod2Map!=null && prod2Map.get(newCli.vlocity_cmt__Product2Id__c)!=null &&'DID'!=prod2Map.get(newCli.vlocity_cmt__Product2Id__c).Name && 'Business Long Distance and Toll-free'!=prod2Map.get(newCli.vlocity_cmt__Product2Id__c).Name) && (prod2Map!=null && prod2Map.get(newCli.vlocity_cmt__Product2Id__c)!=null &&'Contractible_Service'.equalsIgnoreCase(prod2Map.get(newCli.vlocity_cmt__Product2Id__c).RecordType.DeveloperName) && ( (newCli.Contract_Term__c - newCli.Months_Remaining__c)  <=24 ) )){
					if(newCli.Months_Remaining__c!=null && newCli.vlocity_cmt__RecurringTotal__c!=null){
                    newCli.TLC_Amount2__c=(1*newCli.Months_Remaining__c * newCli.vlocity_cmt__RecurringTotal__c );
					}
                } else if(prod2Map!=null && prod2Map.get(newCli.vlocity_cmt__Product2Id__c)!=null && 
                          String.isNotBlank(prod2Map.get(newCli.vlocity_cmt__Product2Id__c).Name) &&
                          'Business Long Distance and Toll-free'.equalsIgnoreCase(prod2Map.get(newCli.vlocity_cmt__Product2Id__c).Name.trim()) 
                          && ( String.isNotBlank(prod2Map.get(newCli.vlocity_cmt__Product2Id__c).RecordType.DeveloperName) && 
                              'Contractible_Service'.equalsIgnoreCase(prod2Map.get(newCli.vlocity_cmt__Product2Id__c).RecordType.DeveloperName.trim()))){ //BLD to be done
                    if(newCli.Months_Remaining__c!=null && newCli.vlocity_cmt__RecurringTotal__c!=null){
					newCli.TLC_Amount2__c=(0.5*newCli.Months_Remaining__c * newCli.vlocity_cmt__RecurringTotal__c) ;
					}
                } else if( (newCli.Contract_Term__c!=null && newCli.Months_Remaining__c!=null && (newCli.Contract_Term__c - newCli.Months_Remaining__c )> 24)){
							  if(newCli.Months_Remaining__c!=null && newCli.vlocity_cmt__RecurringTotal__c!=null){
                    newCli.TLC_Amount2__c=(0.5*newCli.Months_Remaining__c * newCli.vlocity_cmt__RecurringTotal__c); 
							  }
                }else if((newCli.Contract_Term__c!=null && newCli.Months_Remaining__c!=null && (newCli.Contract_Term__c - newCli.Months_Remaining__c <= 24))){
							  if(newCli.Months_Remaining__c!=null && newCli.vlocity_cmt__RecurringTotal__c!=null){
                    newCli.TLC_Amount2__c=(1*newCli.Months_Remaining__c * newCli.vlocity_cmt__RecurringTotal__c );
							  }
                }                
                else {
                    newCli.TLC_Amount2__c=0;
                }
            }
        }
    }
  /*  @TestVisible
    void setContractStatusTerminated(Set<Id> originalContractIdSet){
		if(originalContractIdSet==null || originalContractIdSet.size()==0){
			return;
		}
        if(originalContractIdmap==null){
            originalContractIdmap=new Map<Id,Contract>([select id,recordType.DeveloperName,status,
                     (select id,Original_Contract__c,vlocity_cmt__Status__c,vlocity_cmt__ContractId__c from vlocity_cmt__ContractLineItems__r) from Contract where id in :originalContractIdSet]);
        }
        
        Map<Id,Integer> originalContractIdCntMap=new Map<Id,Integer>();
        for(sobject record:trigger.new){
            vlocity_cmt__ContractLineItem__c newCli=(vlocity_cmt__ContractLineItem__c)record;
            if(String.isBlank(newCli.Original_Contract__c) || 
			(originalContractIdmap!=null && originalContractIdmap.get(newCli.Original_Contract__c)==null) ||
               (originalContractIdmap!=null && originalContractIdmap.containsKey(newCli.Original_Contract__c) && !'Contract'.equalsIgnoreCase(originalContractIdmap.get(newCli.Original_Contract__c).recordType.DeveloperName))){
                   continue;
               }
                        
            if(originalContractIdCntMap.get(newCli.Original_Contract__c)==null){
                originalContractIdCntMap.put(newCli.Original_Contract__c,0);
            }
            if((String.isNotBlank(newCli.vlocity_cmt__Status__c) && 'Active'.equalsIgnoreCase(newCli.vlocity_cmt__Status__c)) && (originalContractIdmap!=null && originalContractIdmap.containsKey(newCli.Original_Contract__c) &&'Contract'.equalsIgnoreCase(originalContractIdmap.get(newCli.Original_Contract__c).recordType.DeveloperName))){
                //String OriginalContractId=newCli.Original_Contract__c;
				
                Integer recordCount=originalContractIdCntMap.get(newCli.Original_Contract__c);
                recordCount=recordCount+1;
                originalContractIdCntMap.put(newCli.Original_Contract__c,recordCount);
            }
        }
        
        for(Id contractKy:originalContractIdmap.keySet()){
            Contract fetchedContractObj=originalContractIdmap.get(contractKy);
            if(fetchedContractObj!=null){
                for(vlocity_cmt__ContractLineItem__c fetchedLineObj:fetchedContractObj.vlocity_cmt__ContractLineItems__r){
                    if((String.isNotBlank(fetchedLineObj.vlocity_cmt__Status__c) && 'Active'.equalsIgnoreCase(fetchedLineObj.vlocity_cmt__Status__c))){
                        
                        if(originalContractIdCntMap.get(fetchedLineObj.vlocity_cmt__ContractId__c)==null){
                            originalContractIdCntMap.put(fetchedLineObj.vlocity_cmt__ContractId__c,0);
                        } else {
                            Integer recordCount=originalContractIdCntMap.get(fetchedLineObj.vlocity_cmt__ContractId__c);
                            recordCount=recordCount+1;
                            originalContractIdCntMap.put(fetchedLineObj.vlocity_cmt__ContractId__c,recordCount);
                        }
                    } 
                }
            }
        }
        
        List<Contract> contractListToTerminate=new List<Contract>();
		//[select id,status from Contract where id in: originalContractIdCntMap.keySet()];
        Boolean isDirty=false;
        for(Id contractToTerminateId:originalContractIdmap.keySet()){
            Contract contractToterminate= originalContractIdmap.get(contractToTerminateId);
            if(originalContractIdCntMap==null || !originalContractIdCntMap.containsKey(contractToterminate.id)){
                continue;
            }
            if(originalContractIdCntMap!=null && originalContractIdCntMap.get(contractToterminate.id)!=null && originalContractIdCntMap.get(contractToterminate.id)==0){
                contractToterminate.Status='Terminated';
                isDirty=true;
                contractListToTerminate.add(contractToterminate);
            }
        }
        if(isDirty){
            update contractListToTerminate;
        }
    }*/
    
    void checkTLCCalculationRequired(){
       // Map<String,Id> contractRecTypes = RecordTypeUtil.GetRecordTypeIdsByDeveloperName(vlocity_cmt__ContractLineItem__c.SObjectType);
        if(contractRecTypes==null){
            contractRecTypes = RecordTypeUtil.GetRecordTypeIdsByDeveloperName(vlocity_cmt__ContractLineItem__c.SObjectType);
        }
        Set<Id> product2IdSet=new Set<Id>();
        Set<Id> cliIdSet=new Set<Id>();
        for(sobject record:trigger.new){
            vlocity_cmt__ContractLineItem__c newCli=(vlocity_cmt__ContractLineItem__c)record;
            vlocity_cmt__ContractLineItem__c oldCli=(vlocity_cmt__ContractLineItem__c)Trigger.oldMap.get(newCli.id);
            if(!(
                (newCli.Total_Contract_Value__c!=oldCli.Total_Contract_Value__c) || 
                (newCli.New_TCV__c!=oldCli.New_TCV__c) || 
                (newCli.Effective_Take_Out_Date__c!=oldCli.Effective_Take_Out_Date__c) || 
                (newCli.Contract_Term__c!=oldCli.Contract_Term__c) || 
                (newCli.Term_Date__c!=oldCli.Term_Date__c) || 
                (newCli.vlocity_cmt__Product2Id__c!=oldCli.vlocity_cmt__Product2Id__c) || 
                (newCli.Was_Contract_Replaced__c!=oldCli.Was_Contract_Replaced__c)
            )){
                continue;
            }   
                if(contractRecTypes!=null && newCli.RecordTypeId==contractRecTypes.get('eContract_Line_Item')){
                    if(!product2IdSet.contains(newCli.vlocity_cmt__Product2Id__c)){
                        product2IdSet.add(newCli.vlocity_cmt__Product2Id__c);
                    }
                    cliIdSet.add(newCli.id);
                } 
                if(contractRecTypes!=null && newCli.RecordTypeId==contractRecTypes.get('Contract_Line_Item')){
                    //if(String.isNotBlank(newCli.vlocity_cmt__Product2Id__c) && contractableProductsMap!=null && 
                      // contractableProductsMap.containsKey(newCli.vlocity_cmt__Product2Id__c)){
                        if(!product2IdSet.contains(newCli.vlocity_cmt__Product2Id__c)){
                            product2IdSet.add(newCli.vlocity_cmt__Product2Id__c);
                        }
                        cliIdSet.add(newCli.id);
                   // }
                }                
          
        }
        calculateTLC(product2IdSet,cliIdSet);
    }
    

}