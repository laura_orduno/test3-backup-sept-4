/**
 * The Class TNReservationData.
 */
public class OrdrTnReservationData {
	
	/** The opportunity id. */
	private String opportunityId;
	
	/** The order id. */
	private String orderId;
	
	/** The order item id. */
	private String orderItemId;
	
	/** The customer id. */
	private String customerId;
	
	/** The switch number. */
	private String switchNumber;
	
	/** The extn co id. */
	private String extnCoId;
	
	/** The extn service address id. */
	private String extnServiceAddressId;
	
	/** The prov. */
	private String prov;
	
	/** The tns. */
	private String tns;
	
	/** The status. */
	private String status;
	
	/** The is in use. */
	private String isInUse;
	
	/** The is primary quote. */
	private String isPrimaryQuote;
	
	/** The user id. */
	private String userId;
	
	/** The product type. */
	private String productType;
	
	/** The non native co id. */
	private String nonNativeCoId;
	
	/** The reservation type. */
	private String reservationType;
	
	/** The extn fms service address id. */
	private String extnFmsServiceAddressId;

	private String npa;

	private String nxx;
	
	private String lineNumber;

	private String lineNumberPrefix;
    
    private String salesforceAddressId;
    
    public String getSalesforceAddressId() {
        return salesforceAddressId;
    }
    public void setSalesforceAddressId(String salesforceAddressId) {
        this.salesforceAddressId = salesforceAddressId;
    }

	public String getLineNumberPrefix(){
		return lineNumberPrefix;
	}

	public void setLineNumberPrefix(String lineNumberPrefix){
		this.lineNumberPrefix=lineNumberPrefix;
	}

	/**
	 * Gets the non native co id.
	 *
	 * @return the non native co id
	 */
	public String getNonNativeCoId() {
		return nonNativeCoId;
	}

	/**
	 * Sets the non native co id.
	 *
	 * @param nonNativeCoId the new non native co id
	 */
	public void setNonNativeCoId(String nonNativeCoId) {
		this.nonNativeCoId = nonNativeCoId;
	}

	/**
	 * Gets the reservation type.
	 *
	 * @return the reservation type
	 */
	public String getReservationType() {
		return reservationType;
	}

	/**
	 * Sets the reservation type.
	 *
	 * @param reservationType the new reservation type
	 */
	public void setReservationType(String reservationType) {
		this.reservationType = reservationType;
	}

	
	/**
	 * Gets the opportunity key.
	 *
	 * @return the opportunity key
	 */
	public String getOpportunityId() {
		return opportunityId;
	}
	
	/**
	 * Sets the opportunity key.
	 *
	 * @param opportunityKey the new opportunity key
	 */
	public void setOpportunityId(String opportunityId) {
		this.opportunityId = opportunityId;
	}
	
	/**
	 * Gets the order header key.
	 *
	 * @return the order header key
	 */
	public String getOrderId() {
		return orderId;
	}
	
	/**
	 * Sets the order header key.
	 *
	 * @param orderHeaderKey the new order header key
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	
	/**
	 * Gets the bom config key.
	 *
	 * @return the bom config key
	 */
	public String getOrderItemId() {
		return orderItemId;
	}
	
	/**
	 * Sets the bom config key.
	 *
	 * @param bomConfigKey the new bom config key
	 */
	public void setOrderItemId(String orderItemId) {
		this.orderItemId = orderItemId;
	}
	
	/**
	 * Gets the customer id.
	 *
	 * @return the customer id
	 */
	public String getCustomerId() {
		return customerId;
	}
	
	/**
	 * Sets the customer id.
	 *
	 * @param customerId the new customer id
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	
	/**
	 * Gets the switch number.
	 *
	 * @return the switch number
	 */
	public String getSwitchNumber() {
		return switchNumber;
	}
	
	/**
	 * Sets the switch number.
	 *
	 * @param switchNumber the new switch number
	 */
	public void setSwitchNumber(String switchNumber) {
		this.switchNumber = switchNumber;
	}
	
	/**
	 * Gets the extn co id.
	 *
	 * @return the extn co id
	 */
	public String getExtnCoId() {
		return extnCoId;
	}
	
	/**
	 * Sets the extn co id.
	 *
	 * @param extnCoId the new extn co id
	 */
	public void setExtnCoId(String extnCoId) {
		this.extnCoId = extnCoId;
	}
	
	/**
	 * Gets the extn service address id.
	 *
	 * @return the extn service address id
	 */
	public String getExtnServiceAddressId() {
		return extnServiceAddressId;
	}
	
	/**
	 * Sets the extn service address id.
	 *
	 * @param extnServiceAddressId the new extn service address id
	 */
	public void setExtnServiceAddressId(String extnServiceAddressId) {
		this.extnServiceAddressId = extnServiceAddressId;
	}
	
	/**
	 * Gets the prov.
	 *
	 * @return the prov
	 */
	public String getProv() {
		return prov;
	}
	
	/**
	 * Sets the prov.
	 *
	 * @param prov the new prov
	 */
	public void setProv(String prov) {
		this.prov = prov;
	}
	
	/**
	 * Gets the tns.
	 *
	 * @return the tns
	 */
	public String getTns() {
		return tns;
	}
	
	/**
	 * Sets the tns.
	 *
	 * @param tns the new tns
	 */
	public void setTns(String tns) {
		this.tns = tns;
	}
	
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	
	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
	/**
	 * Gets the checks if is in use.
	 *
	 * @return the checks if is in use
	 */
	public String getIsInUse() {
		return isInUse;
	}
	
	/**
	 * Sets the checks if is in use.
	 *
	 * @param isInUse the new checks if is in use
	 */
	public void setIsInUse(String isInUse) {
		this.isInUse = isInUse;
	}
	
	/**
	 * Gets the checks if is primary quote.
	 *
	 * @return the checks if is primary quote
	 */
	public String getIsPrimaryQuote() {
		return isPrimaryQuote;
	}
	
	/**
	 * Sets the checks if is primary quote.
	 *
	 * @param isPrimaryQuote the new checks if is primary quote
	 */
	public void setIsPrimaryQuote(String isPrimaryQuote) {
		this.isPrimaryQuote = isPrimaryQuote;
	}

	/**
	 * Gets the product type.
	 *
	 * @return the product type
	 */
	public String getProductType() {
		return productType;
	}

	/**
	 * Sets the product type.
	 *
	 * @param productType the new product type
	 */
	public void setProductType(String productType) {
		this.productType = productType;
	}

	/**
	 * Gets the extn fms service address id.
	 *
	 * @return the extn fms service address id
	 */
	public String getExtnFmsServiceAddressId() {
		return extnFmsServiceAddressId;
	}

	/**
	 * Sets the extn fms service address id.
	 *
	 * @param extnFmsServiceAddressId the new extn fms service address id
	 */
	public void setExtnFmsServiceAddressId(String extnFmsServiceAddressId) {
		this.extnFmsServiceAddressId = extnFmsServiceAddressId;
	}

	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * Sets the user id.
	 *
	 * @param userId the new user id
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getNpa() {
		return npa;
	}
	public void setNpa(String npa) {
		this.npa = npa;
	}

	public String getNxx() {
		return nxx;
	}
	public void setNxx(String nxx) {
		this.nxx = nxx;
	}
	/**
	 * @return the lineNumber
	 */
	public String getLineNumber() {
		return lineNumber;
	}

	/**
	 * @param lineNumber the lineNumber to set
	 */
	public void setLineNumber(String lineNumber) {
		this.lineNumber = lineNumber;
	}
}