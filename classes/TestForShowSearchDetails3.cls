@isTest(SeeAllData=true)
public class TestForShowSearchDetails3{
    public static testMethod void testAtmList2(){       
       String cbucid='0002229208';
       String rcid='0001362414';
       String strProv='PQ';
       Integer newPageIndex=1;
       PageReference objPage;
       System.currentPagereference().getParameters().put('cbucid','0002229208');
       System.currentPagereference().getParameters().put('cbucidnumber','0002229208');
       System.currentPagereference().getParameters().put('strCbucid','0002229208');
       System.currentPagereference().getParameters().put('rcid','0001362414');
       System.currentPagereference().getParameters().put('rcidnumber','0001362414');       
       System.currentPagereference().getParameters().put('banOrBcan','0001362414');       
       System.currentPagereference().getParameters().put('strProv','PQ');   
       System.currentPagereference().getParameters().put('strAlpa','PQ');   
       System.currentPagereference().getParameters().put('strIndex','0.0');   
       
       ShowDetailForAssignmentProv obj=new ShowDetailForAssignmentProv();
       obj.strProv='PQ';
       
       obj.Rcidnumber='0001362414';
       obj.cbucidnumber='0002229208';
     
       obj.customerName='LES EDITIONS CEC';
       obj.cbucidName='QUEBECOR MEDIA INC';
       obj.banOrBcan='LES EDITIONS CEC INC';
       
       obj.selectedTabForSearch='rcidnumber';
       obj.txtForSearch='0001362414';
       obj.searchRcidnumber();
       
       List<Account> aList=new List<Account>();
                        aList=[Select a.rcid__c From Account a
                        where a.RCID__c='0001362414' limit 1000];
       for(Account atest:aList){
           atest.rcid__c='234234234';
       }
       update aList;
       
       List<AccountTeamMember> atmList=new List<AccountTeamMember>();
                        atmList=[Select a.UserId,a.user.name, a.user.firstname, a.user.lastname, a.user.email, a.user.phone,
                        a.TeamMemberRole, a.AccountId From AccountTeamMember a
                        where a.user.isactive=true and a.Account.rcid__c='0001362414' limit 1000];
       for(AccountTeamMember atest:atmList){
           atest.TeamMemberRole='Bus Sales Analyst';
       }
       update atmList;
       obj.getAccountDetail();
       obj.showCustomerDetailWithStatus();
              

    }
    
}