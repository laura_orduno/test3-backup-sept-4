@isTest
private class QuoteProvisionRequestControllerTest {
    testMethod static void test() {
        Web_Account__c account = new Web_Account__c(Name='UnitTest');
        insert account;
        
        Opportunity opp = new Opportunity(Name='Test',Web_Account__c=account.Id,CloseDate=System.today(),StageName='Prospecting');
        insert opp;
        
        SBQQ__Quote__c quote = new SBQQ__Quote__c(SBQQ__Opportunity__c=opp.Id);
        quote.Rate_Band__c = 'F';
        quote.Province__c = 'BC';
        insert quote;
        
        ApexPages.currentPage().getParameters().put('qid', quote.Id);
        QuoteProvisionRequestController target = new QuoteProvisionRequestController();
        System.assert(target.quote != null);
    }
}