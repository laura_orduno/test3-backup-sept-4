public without sharing class ENTPAccounTContactUpdate {
    
   public static void UpdateAccCont(String PINNum, String COMID, string BUID)
    {
       try{
                     user g = [Select AccountId, ContactId from User where id =:UserInfo.getUserId()];
            Account AccountToUPdate = [Select Remedy_Business_Unit_Id__c,Remedy_Company_Id__c from Account where Id =: g.AccountId ];
            Contact ContacttoUpdate = [Select Remedy_PIN__c from Contact where Id =: g.ContactId];
                    System.debug('RemedyAddPIN->NewPIN, PIN(' + PINNum + ')');
                     System.debug('RemedyAddCOMID->NewCOMID, COMID(' + COMID + ')');
                    System.debug('RemedyAddBUID->NewBUID, BUID(' + BUID + ')');
                    
                    AccountToUPdate.Remedy_Business_Unit_Id__c = BUID;
                    AccountToUPdate.Remedy_Company_Id__c = COMID;
                    ContacttoUpdate.Remedy_PIN__c = PINNum;
                    
                    Update AccountToUPdate;
                    Update ContacttoUpdate;
                    
                }
                catch(DmlException e) {
                    System.debug('RemedyAddPIN->TicketEvent, exception: ' + e.getMessage());
                } 
    }

}