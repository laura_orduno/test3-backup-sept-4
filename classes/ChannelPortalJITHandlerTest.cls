@isTest
public class ChannelPortalJITHandlerTest {
    private static void createData(){
        System.runAs(new User(Id = Userinfo.getUserId())) {
            OrdrTestDataFactory.createOrderingWSCustomSettings('ChannelPortalCacheMgmtEndPoint', 'https://webservice1.preprd.teluslabs.net/RMO/ResourceMgmt/ReserveResource_v2_0_vs1_DV');
            Account a = new Account(Name='TestAccount', CustProfId__c='123456',Channel_Org_Code__c='testV');
            insert a;
            Contact testContact = new Contact(FirstName='TestContact',LastName='TestContact',Phone = '9876543281' ,
                                              Email = 'TestMail@telus.com', 
                                              HomePhone = '6476486478', 
                                              MobilePhone = '6476486478',Title='Dealer Principal');
            testContact.accountid=a.id;
            insert testContact;
             User u = new User(
                ProfileId = [SELECT Id FROM Profile WHERE Name = 'Customer Community User Custom'].Id,
                LastName = 'TestContact',
                FirstName= 'TestContact',
                Email = 'TestMail@telus.com', 
                Username = 'TestMail@telus.com', 
                CompanyName = 'TEST',
                Title = 'title',
                Alias = 'alias',
                TimeZoneSidKey = 'America/Los_Angeles',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                ContactId = testContact.Id
            );
            insert u;
            User userObj=[SELECT UserRoleId,Id, ContactId, FederationIdentifier FROM User WHERE id=:Userinfo.getUserId()];
            userObj.FederationIdentifier=Userinfo.getUserId();
            //userObj.ContactId=testContact.id;
            
            UserRole role = new UserRole(name = 'Test Manager');    
            insert role;
            
            userObj.UserRoleId=role.id;
            update userObj;
            
        }
    }
    @isTest
    private static void createUserTest(){
        createData();
        Test.startTest();
        ChannelPortalJITHandler obj=new ChannelPortalJITHandler();
        Id samlSsoProviderId=null;
        Id communityId=null;
        Id portalId=null; 
        String federationIdentifier='test';
        Map<String, String> attributes=new  Map<String, String>();
        attributes.put('SSOuid','test');
        attributes.put('firstName','test');
        attributes.put('lastName','test');
        attributes.put( 'eMail','test@test.com');
        attributes.put('alias','test');
        attributes.put('language','English');
        String assertion=null;
        obj.createUser(samlSsoProviderId,communityId,portalId,federationIdentifier,attributes,assertion);
        Test.stopTest();
    }
    @isTest
    private static void updateUserTest(){
        createData();
        Test.startTest();
        ChannelPortalJITHandler obj=new ChannelPortalJITHandler();
        Id samlSsoProviderId=null;
        Id communityId=null;
        Id portalId=null; 
        String federationIdentifier='test';
        Map<String, String> attributes=new  Map<String, String>();
        String assertion=null;
        attributes.put('SSOuid','test');
        attributes.put('firstName','test');
        attributes.put('lastName','test');
        attributes.put( 'eMail','test@test.com');
        attributes.put('alias','test');
        attributes.put('language','English');
        User u=[select id,ContactId from user where lastname='TestContact' limit 1];
        obj.updateUser(u.id,samlSsoProviderId,communityId,portalId,federationIdentifier,attributes,assertion);
        Test.stopTest();
    }
    @isTest
    private static void handleContactTest(){
        createData();
        Test.startTest();
        ChannelPortalJITHandler obj=new ChannelPortalJITHandler();
        Id samlSsoProviderId=null;
        Id communityId=null;
        Id portalId=null; 
        String federationIdentifier='test';
        Map<String, String> attributes=new  Map<String, String>();
        String assertion=null;
        attributes.put('SSOuid','test');
        attributes.put('firstName','test');
        
        //attributes.put( 'eMail','test@test.com');
        attributes.put('alias','test');
        attributes.put('language','fr');
		String accountId=[select id from account limit 1].id;
		User u=[select id,ContactId from user where lastname='TestContact' limit 1];
        obj.handleContact(false, accountId, u, attributes, 'test');
        Test.stopTest();
    }
}