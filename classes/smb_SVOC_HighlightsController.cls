global with sharing class smb_SVOC_HighlightsController {
    
    public Boolean fromConsole {get;set;}
    
    public Id accountId {get; 
        set {
            accountId = value;
            this.AccountRecord = getAccountRecordFromId(accountId);
            this.VOCRecord = getVOCRecordFromAccountId(accountId);
            this.proactiveCaseRecord = getProactiveCaseRecordFromAccountId(accountId);            
            System.debug('setting id to ' + accountId);
            System.debug(this.AccountRecord);
            System.debug(this.VOCRecord);
            System.debug(this.VOCRecord.SVOC_Status_Icon__c);
        }
    }
    
    public Account AccountRecord {get;private set;}
    public VOC_Survey__c VOCRecord {get;private set;}
    public Case proactiveCaseRecord {get;private set;}
    
    public String AccountNumberLabel {
        get {
            if (AccountRecord == null) return null;
            return smb_AccountUtility.getAccountNumberLabel(AccountRecord.RecordType.DeveloperName);
        }
    }
    
    @RemoteAction
    global static string GetAccountNumberLabelFromAccountRecordType(string recordTypeDeveloperName) {
        return smb_AccountUtility.getAccountNumberLabel(recordTypeDeveloperName);
    } 
    
    public String AccountNumber {
        get {
            if (accountRecord == null) return null;
            return smb_AccountUtility.getAccountNumber(accountRecord);
        }
    }
    
    public String marketingSubSegment{
        get {
            try{
System.debug('marketingSubSegment: ' + AccountRecord);                
                if (AccountRecord == null) return null;
    
                Id parentId = accountRecord.parentId; 			
                String recordTypeName = accountRecord.recordType.DeveloperName;
                String marketingSubSegmentDescription = accountRecord.Marketing_Sub_Segment_Description__c;
    
                Integer maxParentCount = 5;
                
                while(recordTypeName != 'RCID' && recordTypeName != 'CBUCID' && maxParentCount-- > 0){
                    Account parentAccountRecord = getAccountRecordFromId(parentId);
                    
                    parentId = parentAccountRecord.parentId; 			
                    recordTypeName = parentAccountRecord.recordType.DeveloperName;
                    marketingSubSegmentDescription = parentAccountRecord.Marketing_Sub_Segment_Description__c;
                }
                
                return marketingSubSegmentDescription;                
            }
            catch(QueryException e){
            	return '';    
            }
            catch(Exception e){
            	return '';                    
            }
        }
    }
    
    private static string checkForNull (string text, string defaultText, string appendTextIfNotNull) {
        if (text == null) return defaultText;
        return text + appendTextIfNotNull;
    }
    
    public String BillingAddress {
        get {
            if (AccountRecord == null) return null;
            return checkForNull(AccountRecord.BillingStreet, '', '<br />') + checkForNull(AccountRecord.BillingCity, '', ', ') + checkForNull(AccountRecord.BillingState, '', ', ') + checkForNull(AccountRecord.BillingPostalCode, '', '');
        }
    }
    
    
    
    private static Account getAccountRecordFromId(Id accountId) {
        
        if (accountId == null) {
             return null;
        }
        
        return [SELECT  Id, Name, RecordTypeId, RecordType.DeveloperName, RecordType.Name, CBU_Name__c, parentId, 
                        BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry,
                        Owner.Name, tolabel(Customer_Segment__c), Registration_Incorporation_Number__c,
                        tolabel(Account_Status__c), Type_of_Business__c, Account_Open_Since__c, Wireline_PIN__c,
                        Wireless_PIN__c, Paternal_RCID_Master_PIN__c, Paternal_RCID_Support_PIN__c, Paternal_RCID_PIN_Declined__c, smb_Strategic_Customer_Icon_Name__c, smb_Fraud_Risk_Icon_Name__c,
                        smb_Churn_Threat_Icon_Name__c, smb_Chronic_Icon_Name__c,CBUCID_RCID__c,CAN__c,PTN__c,
                        Sub_Segment_Code__c, Sub_Segment_Code__r.Marketing_Sub_Segment_Short_Description__c, tolabel(Legal_Entity_Type__c), Marketing_Sub_Segment_Description__c,Support_Model__c
                FROM Account
                WHERE Id = :accountId];
    }
    
    private static VOC_Survey__c getVOCRecordFromAccountId(Id accountId) {
        
        if (accountId == null) {
             return null;
        }
        
        List<VOC_Survey__c> tempList = [SELECT  Id, Name, SVOC_Status_Icon__c
                FROM VOC_Survey__c
                WHERE Account__c = :accountId and SVOC_Status_Icon__c != NULL
                ORDER BY Received_Date__c DESC Limit 1];
        
        if(tempList.size()>0) return tempList[0];
        return null;
    }
    
    private static Case getProactiveCaseRecordFromAccountId(Id accountId) {
        
        if (accountId == null) {
             return null;
        }
        
        List<Case> tempList = [SELECT  Id, Subject
                FROM Case
                WHERE Accountid = :accountId and Proactive_Case__c = true and Status != 'Closed'
                ORDER BY createddate DESC Limit 1];
        
        if(tempList.size()>0){
            return tempList[0];
        }
        return null;
    }
}