@isTest
public class CustomGetProductListImplementation_Test {
    static testmethod void invokeMethodPositiveTest(){
        Test.startTest();
        CustomGetProductListImplementation getProductListImpl = new CustomGetProductListImplementation();
        String methodName = 'getProductList';
        Map<String,Object> inputMap = new  Map<String,Object>();
       Map<String,Object> outMap   = new Map<String,Object>();
       Map<String,Object> options  = new Map<String,Object>();
        //inputMap.put('contextId', TestDataHelper.testContractObj.Id);
        inputMap.put('contextId', TestDataHelper.testContractObj);
       //Boolean result  = validateTelSignorObj.invokeMethod(methodName,inputMap, outMap, options);
        system.assertEquals(getProductListImpl.invokeMethod(methodName,inputMap, outMap, options) , true);
		Test.stopTest();
    } 

}