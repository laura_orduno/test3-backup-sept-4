global with sharing class OC_UpgradeDowngrade implements vlocity_cmt.VlocityOpenInterface 
{
    global Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) 
    {
        if(methodName == 'getProductRelationship')
        {
            String productId = (String)inputMap.get('ProductId');
            Map<String,Object> response = getProductRelationship(productId);
            outMap.put('productRelationShipList', response);
            System.debug('>>>InvokeMethodResponse'+ outMap);
        }
        else if(methodName == 'setUpgradeDowngradeFlagOnOLI')
        {
            String orderItemId = (String)inputMap.get('oliId');
            OrderItem updateOI = setUpgradeDowngradeFlagOnOLI(orderItemId);
            if(updateOI != null)
                outMap.put('UpgradeDowngradeFlagOnOLI',true);
            else
                outMap.put('UpgradeDowngradeFlagOnOLI',false);
        }
        return true;
    }
    
    public static OrderItem setUpgradeDowngradeFlagOnOLI(String oliId)
    {
        List<OrderItem> orderItem = new List<OrderItem>();
        Boolean result;
        orderItem = [select Id, UpgradeDowngrade__c from OrderItem where Id = :oliId];
        orderItem[0].UpgradeDowngrade__c = true;
        Database.SaveResult[] srList = Database.Update(orderItem,false);
        for(Database.SaveResult sr:srList)
        {
            if(sr.isSuccess())
            {
                result = true;
            }
            else 
            {
        // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Order Item fields that affected this error: ' + err.getFields());
                }
                result = false;
            }
        }
        if(result == true)
            return orderItem[0];
        else
            return null;
    }
    public static List<OrderItem> updateOliList = new List<OrderItem>();
    public static List<OrderItem> createOliList = new List<OrderItem>();
    
    public static List<OrderItem> updateOrCreateOLI(String destRootLineNum, Map<Id,List<OrderItem>> sourceOffers, Map<Id,List<OrderItem>> destinationOffers, Map<Id,OrderItem> sourceRootOLI, Map<Id,OrderItem> destRootOLI)
    {              
        String rootItemIdSource;
        String rootItemIdDest;       
        
        //Map of product Id and line number
        Map<String,String> sourceProduct2IdLinenumber = new Map<String,String>();
        Map<String,OrderItem> destOliIdOliMap = new Map<String,OrderItem>();
        String rootDestLineNumber;
        System.debug('***destRootLineNum'+destRootLineNum);
        
        for(Id rootId:sourceOffers.keySet())
        {
          rootItemIdSource = rootId;
          break;
        }
        system.debug('***rootItemIdSource'+rootItemIdSource);

        for(Id rootId:destinationOffers.keySet())
        {
          rootItemIdDest = rootId;         
          break;
        }
        system.debug('***rootItemIdDest'+rootItemIdDest);
        sourceProduct2IdLinenumber.put(sourceRootOLI.get(rootItemIdSource).vlocity_cmt__Product2Id__c,sourceRootOLI.get(rootItemIdSource).vlocity_cmt__LineNumber__c);
        for(OrderItem sourceOI : sourceOffers.get(rootItemIdSource))
        {
            sourceProduct2IdLinenumber.put(String.valueOf(sourceOI.vlocity_cmt__Product2Id__c),sourceOI.vlocity_cmt__LineNumber__c);           
        }
        
        
        system.debug('***sourceProduct2IdLinenumber'+sourceProduct2IdLinenumber);
        
        for(OrderItem destOI : destinationOffers.get(rootItemIdDest))
        {
            destOliIdOliMap.put(String.valueOf(destOI.vlocity_cmt__Product2Id__c),destOI);
        }
        //destOliIdOliMap.put(destRootOLI.get(rootItemIdDest).vlocity_cmt__Product2Id__c,destRootOLI.get(rootItemIdDest));
        system.debug('***destOliIdOliMap'+destOliIdOliMap);    
        for(OrderItem oItemSource:sourceOffers.get(rootItemIdSource))
        {           
            if(destOliIdOliMap.get(oItemSource.vlocity_cmt__Product2Id__c) != null)
            {
                //OrderItem oi = ;
                system.debug('***oiSourceJSON'+oItemSource.vlocity_cmt__JSONAttribute__c);
                system.debug('***oiDestJSONBeforeUpdate'+destOliIdOliMap.get(oItemSource.vlocity_cmt__Product2Id__c).vlocity_cmt__JSONAttribute__c);              
                destOliIdOliMap.get(oItemSource.vlocity_cmt__Product2Id__c).vlocity_cmt__JSONAttribute__c = oItemSource.vlocity_cmt__JSONAttribute__c;
                destOliIdOliMap.get(oItemSource.vlocity_cmt__Product2Id__c).orderMgmt_BPI_Id__c = oItemSource.orderMgmt_BPI_Id__c;
                system.debug('***oiDestJSONAfterUpdate'+destOliIdOliMap.get(oItemSource.vlocity_cmt__Product2Id__c).vlocity_cmt__JSONAttribute__c);
                updateOliList.add(destOliIdOliMap.get(oItemSource.vlocity_cmt__Product2Id__c));
            }
            else
            {
                //createOliMap.put(oItemSource.Id,oItemSource);
                createOliList.add(oItemSource);
            }
        }
        
        OrderItem oiRoot = new OrderItem(Id=rootItemIdDest, vlocity_cmt__ProvisioningStatus__c = 'Changed', orderMgmt_BPI_Id__c=sourceRootOLI.get(rootItemIdSource).orderMgmt_BPI_Id__c, vlocity_cmt__JSONAttribute__c=sourceRootOLI.get(rootItemIdSource).vlocity_cmt__JSONAttribute__c, vlocity_cmt__InCartQuantityMap__c = sourceRootOLI.get(rootItemIdSource).vlocity_cmt__InCartQuantityMap__c, vlocity_cmt__AssetId__c = sourceRootOLI.get(rootItemIdSource).vlocity_cmt__AssetId__c, vlocity_cmt__AssetReferenceId__c = sourceRootOLI.get(rootItemIdSource).vlocity_cmt__AssetReferenceId__c);
        updateOliList.add(oiRoot);
        //populateUpdateCreateOLIList(sourceOffers,rootItemIdSource,rootItemIdDest,destOliIdOliMap,sourceRootOLI,destRootOLI);  
                  system.debug('***IncartMap'+sourceRootOLI.get(rootItemIdSource).vlocity_cmt__InCartQuantityMap__c);   
                  system.debug('***IncartMapId'+sourceRootOLI.get(rootItemIdSource).Id);         
        system.debug('***updateOliList'+updateOliList);
        system.debug('***createOLIList'+createOliList);
        createOLI(createOliList, rootItemIdDest, destinationOffers, destRootLineNum, destRootOLI, sourceProduct2IdLinenumber);
        //deleteSourceOffer(sourceOffers,rootItemIdSource);
        return updateOliList;
    }
    
    
    
    public static void deleteSourceOffer(Map<Id,List<OrderItem>> sourceOfferTBD,String sourceRootItemId)
    {
        List<OrderItem> childOITBD = new List<OrderItem>();
        childOITBD = sourceOfferTBD.get(sourceRootItemId);
        delete childOITBD;       
        
        OrderItem rootItemTBD = new OrderItem(Id=sourceRootItemId);
        delete rootItemTBD;               
    }
    
    public static List<OrderItem> createOLI(List<OrderItem> orderItems, String rootItemId, Map<Id,List<OrderItem>> rootIdOrderItemList, String rootLineNumber, Map<Id,OrderItem> destinationRootOLI, Map<String,String> product2IdLinenumber)
    {

        List<OrderItem> childItemsOfDest = rootIdOrderItemList.get(rootItemId);       
        List<OrderItem> orderItemsToCreate = new List<OrderItem>();
        Integer lengthOfChildItemsOfDest = childItemsOfDest.size();
        System.debug('***lengthOfChildItemsOfDest'+lengthOfChildItemsOfDest);       
        String lineNumber;
        String productHierarchyPath;
        List<String> splitProductHierarchyPath = new List<String>();
        List<String> splitLineNumber = new List<String>();
        String newProductHierarchyPath;
        String sourceChildLineNumber;
        String newLineNumber;
        
        lineNumber = childItemsOfDest[lengthOfChildItemsOfDest-1].vlocity_cmt__LineNumber__c;
        
        system.debug('***lastchild'+childItemsOfDest[lengthOfChildItemsOfDest-1]);
        system.debug('***linenumber'+lineNumber);
        try {
               
                    for(OrderItem oi:orderItems)
                    {
                        if(oi.vlocity_cmt__RelationshipType__c != 'Promotion'){
                          //lineNumber = incrementLineNum(lineNumber);
                          sourceChildLineNumber = product2IdLinenumber.get(oi.vlocity_cmt__Product2Id__c);
                            system.debug('sourceChildLineNumber>>>' +sourceChildLineNumber);
                            if(sourceChildLineNumber != null || sourceChildLineNumber != ''){
                                splitLineNumber = sourceChildLineNumber.split('\\.');
                                if(splitLineNumber.size() > 0 && !splitLineNumber.isEmpty()){
                                    splitLineNumber.remove(0);
                                }
                                
                            }
                         // splitLineNumber = sourceChildLineNumber.split('.');
                          
                          newLineNumber = rootLineNumber+'.'+String.join(splitLineNumber,'.');
                              system.debug('newLineNumber>>>' +newLineNumber);
                          System.debug('***oi.vlocity_cmt__ProductHierarchyPath__c'+oi.vlocity_cmt__ProductHierarchyPath__c);
                          splitProductHierarchyPath = oi.vlocity_cmt__ProductHierarchyPath__c.split('<');
                          splitProductHierarchyPath.remove(0);
                          System.debug('***oi.vlocity_cmt__ProductHierarchyPath__c after remove'+splitProductHierarchyPath);
                          newProductHierarchyPath = destinationRootOLI.get(rootItemId).vlocity_cmt__Product2Id__c+'<'+String.join(splitProductHierarchyPath,'<');
                          system.debug('***newProductHierarchyPath '+newProductHierarchyPath);
                          /*OrderItem newOI = new OrderItem(vlocity_cmt__LineNumber__c = lineNumber, vlocity_cmt__JSONAttribute__c = oi.vlocity_cmt__JSONAttribute__c, orderMgmt_BPI_Id__c = oi.orderMgmt_BPI_Id__c, OrderId = oi.OrderId, Quantity = oi.Quantity, PricebookEntryId = oi.PricebookEntryId, UnitPrice = oi.UnitPrice, vlocity_cmt__SerialNumber__c = oi.vlocity_cmt__SerialNumber__c, vlocity_cmt__RecurringUOM__c = oi.vlocity_cmt__RecurringUOM__c, vlocity_cmt__RecurringManualDiscount__c = oi.vlocity_cmt__RecurringManualDiscount__c, vlocity_cmt__ProvisioningStatus__c = oi.vlocity_cmt__ProvisioningStatus__c, vlocity_cmt__RecurringDiscountPrice__c = oi.vlocity_cmt__RecurringDiscountPrice__c, EndDate = oi.EndDate, vlocity_cmt__BillingAccountId__c = oi.vlocity_cmt__BillingAccountId__c, vlocity_cmt__DisconnectDate__c = oi.vlocity_cmt__DisconnectDate__c, vlocity_cmt__ConnectDate__c = oi.vlocity_cmt__ConnectDate__c, vlocity_cmt__ActivationName__c = oi.vlocity_cmt__ActivationName__c, vlocity_cmt__OneTimeManualDiscount__c = oi.vlocity_cmt__OneTimeManualDiscount__c, ServiceDate = oi.ServiceDate, vlocity_cmt__OneTimeDiscountPrice__c = oi.vlocity_cmt__OneTimeDiscountPrice__c, vlocity_cmt__OneTimeCalculatedPrice__c = oi.vlocity_cmt__OneTimeCalculatedPrice__c,
                          vlocity_cmt__PremisesId__c = oi.vlocity_cmt__PremisesId__c, vlocity_cmt__OneTimeCharge__c = oi.vlocity_cmt__OneTimeCharge__c, vlocity_cmt__AssetId__c = oi.vlocity_cmt__AssetId__c, vlocity_cmt__AssetReferenceId__c = oi.vlocity_cmt__AssetReferenceId__c, vlocity_cmt__EffectiveQuantity__c = oi.vlocity_cmt__EffectiveQuantity__c, vlocity_cmt__EffectiveRecurringTotal__c = oi.vlocity_cmt__EffectiveRecurringTotal__c, vlocity_cmt__EffectiveOneTimeTotal__c = oi.vlocity_cmt__EffectiveOneTimeTotal__c, vlocity_cmt__ServiceAccountId__c = oi.vlocity_cmt__ServiceAccountId__c,
                          vlocity_cmt__RecurringTotal__c = oi.vlocity_cmt__RecurringTotal__c, vlocity_cmt__OneTimeTotal__c = oi.vlocity_cmt__OneTimeTotal__c, vlocity_cmt__ProductHierarchyPath__c = newProductHierarchyPath);*/                  
                          //OrderItem newOI = new OrderItem(vlocity_cmt__RecurringCharge__c = oi.vlocity_cmt__RecurringCharge__c, vlocity_cmt__LineNumber__c = newLineNumber, vlocity_cmt__JSONAttribute__c = oi.vlocity_cmt__JSONAttribute__c, orderMgmt_BPI_Id__c = oi.orderMgmt_BPI_Id__c, OrderId = oi.OrderId, Quantity = oi.Quantity, PricebookEntryId = oi.PricebookEntryId, UnitPrice = oi.UnitPrice, vlocity_cmt__SerialNumber__c = oi.vlocity_cmt__SerialNumber__c, vlocity_cmt__RecurringUOM__c = oi.vlocity_cmt__RecurringUOM__c, vlocity_cmt__RecurringManualDiscount__c = oi.vlocity_cmt__RecurringManualDiscount__c, vlocity_cmt__ProvisioningStatus__c = 'Changed', vlocity_cmt__RecurringDiscountPrice__c = oi.vlocity_cmt__RecurringDiscountPrice__c, EndDate = oi.EndDate, vlocity_cmt__BillingAccountId__c = oi.vlocity_cmt__BillingAccountId__c, vlocity_cmt__DisconnectDate__c = oi.vlocity_cmt__DisconnectDate__c, vlocity_cmt__ConnectDate__c = oi.vlocity_cmt__ConnectDate__c, vlocity_cmt__ActivationName__c = oi.vlocity_cmt__ActivationName__c, vlocity_cmt__OneTimeManualDiscount__c = oi.vlocity_cmt__OneTimeManualDiscount__c, ServiceDate = oi.ServiceDate, vlocity_cmt__OneTimeDiscountPrice__c = oi.vlocity_cmt__OneTimeDiscountPrice__c, vlocity_cmt__OneTimeCalculatedPrice__c = oi.vlocity_cmt__OneTimeCalculatedPrice__c,
                          OrderItem newOI = new OrderItem(vlocity_cmt__RecurringCharge__c = oi.vlocity_cmt__RecurringCharge__c, vlocity_cmt__LineNumber__c = newLineNumber, vlocity_cmt__JSONAttribute__c = oi.vlocity_cmt__JSONAttribute__c, orderMgmt_BPI_Id__c = oi.orderMgmt_BPI_Id__c, OrderId = oi.OrderId, Quantity = oi.Quantity, PricebookEntryId = oi.PricebookEntryId, UnitPrice = oi.UnitPrice, vlocity_cmt__SerialNumber__c = oi.vlocity_cmt__SerialNumber__c, vlocity_cmt__RecurringUOM__c = oi.vlocity_cmt__RecurringUOM__c, vlocity_cmt__RecurringManualDiscount__c = oi.vlocity_cmt__RecurringManualDiscount__c, vlocity_cmt__ProvisioningStatus__c = '-', vlocity_cmt__RecurringDiscountPrice__c = oi.vlocity_cmt__RecurringDiscountPrice__c, EndDate = oi.EndDate, vlocity_cmt__BillingAccountId__c = oi.vlocity_cmt__BillingAccountId__c, vlocity_cmt__DisconnectDate__c = oi.vlocity_cmt__DisconnectDate__c, vlocity_cmt__ConnectDate__c = oi.vlocity_cmt__ConnectDate__c, vlocity_cmt__ActivationName__c = oi.vlocity_cmt__ActivationName__c, vlocity_cmt__OneTimeManualDiscount__c = oi.vlocity_cmt__OneTimeManualDiscount__c, ServiceDate = oi.ServiceDate, vlocity_cmt__OneTimeDiscountPrice__c = oi.vlocity_cmt__OneTimeDiscountPrice__c, vlocity_cmt__OneTimeCalculatedPrice__c = oi.vlocity_cmt__OneTimeCalculatedPrice__c,
                          vlocity_cmt__PremisesId__c = oi.vlocity_cmt__PremisesId__c, vlocity_cmt__OneTimeCharge__c = oi.vlocity_cmt__OneTimeCharge__c, vlocity_cmt__AssetReferenceId__c = oi.vlocity_cmt__AssetReferenceId__c, vlocity_cmt__EffectiveQuantity__c = oi.vlocity_cmt__EffectiveQuantity__c, vlocity_cmt__AssetId__c = oi.vlocity_cmt__AssetId__c,vlocity_cmt__EffectiveRecurringTotal__c = oi.vlocity_cmt__EffectiveRecurringTotal__c, vlocity_cmt__EffectiveOneTimeTotal__c = oi.vlocity_cmt__EffectiveOneTimeTotal__c, vlocity_cmt__ServiceAccountId__c = oi.vlocity_cmt__ServiceAccountId__c, 
                          vlocity_cmt__RecurringTotal__c = oi.vlocity_cmt__RecurringTotal__c, vlocity_cmt__OneTimeTotal__c = oi.vlocity_cmt__OneTimeTotal__c, vlocity_cmt__ProductHierarchyPath__c = newProductHierarchyPath, vlocity_cmt__InCartQuantityMap__c = oi.vlocity_cmt__InCartQuantityMap__c, vlocity_cmt__CpqMessageData__c = null);
                          orderItemsToCreate.add(newOI);
                        }
                        system.debug('***orderItemsToCreate'+orderItemsToCreate);
                        if(orderItemsToCreate != null && orderItemsToCreate.size()>0)
                        {
                           Database.SaveResult[] srList = Database.insert(orderItemsToCreate,false);
                           system.debug('***srList'+srList);
                        }
                }
        } catch(exception e){
            System.debug(LoggingLevel.ERROR, 'Exception is '+e);
            System.debug(LoggingLevel.ERROR, 'Exception stack trace '+e.getStackTraceString());
            String messageDisplay = e.getMessage();
            return null;
            }
        return orderItemsToCreate;
    }
    public static Integer levelLength = 4;
    public static String padLeft(Integer i) 
    {
        String str = String.valueOf(i);
        while (str.length() < levelLength) {
            str = '0' + str;
        }
        return str;
    }    
    
    public static String incrementLineNum(String lineNum) 
    {
        if (lineNum== null || String.isEmpty(lineNum ) )
            return padLeft(1) ; // 1st root 0001
        String retval = '';
        String[] lns = lineNum.split('\\.');
        String lastLN = lns[lns.size()-1];
        Integer ln = Integer.valueOf(lastLN);
        ln ++;
        lns[lns.size()-1] = padLeft(ln);
        retval = String.join(lns,'.');
        return retval;
    }
    
    //This method returns a map of the relationship type and a list of Product Relationships.
    public static Map<String,Object> getProductRelationship(String prodId)
    {       
        List<vlocity_cmt__ProductRelationship__c> objTempList = new List<vlocity_cmt__ProductRelationship__c>();
        List<Product2> relatedProdList = new List<Product2>();
        Map<String,List<Product2>> relTypeProductRelationships = new Map<String,List<Product2>>();              
        
        for(vlocity_cmt__ProductRelationship__c prodRel:[SELECT Id, Name, vlocity_cmt__RelationshipType__c,
                            vlocity_cmt__Product2Id__c,vlocity_cmt__Product2Id__r.Name,
                            vlocity_cmt__RelatedProductId__c,vlocity_cmt__RelatedProductId__r.Name
                            FROM vlocity_cmt__ProductRelationship__c
                            WHERE vlocity_cmt__Product2Id__c = :prodId])
            {
                //objTempList = new List<vlocity_cmt__ProductRelationship__c>();
                if(prodRel.vlocity_cmt__RelationshipType__c == 'Upgrade')
                {
                     Product2 relatedUpgradeProd = new Product2(Id = prodRel.vlocity_cmt__RelatedProductId__c, Name = prodRel.vlocity_cmt__RelatedProductId__r.Name );
                   if(relTypeProductRelationships.containsKey('Upgrade'))
                        {
                            relTypeProductRelationships.get('Upgrade').add(relatedUpgradeProd);
                            
                        }
                    else {
                        relTypeProductRelationships.put('Upgrade',new List<Product2>{relatedUpgradeProd});
                    }
                   // relatedProdList.add(relatedUpgradeProd);                  
                    //relTypeProductRelationships.put('Upgrade',relatedProdList);
                    
                }
                else if(prodRel.vlocity_cmt__RelationshipType__c == 'Downgrade') 
                {    

                    Product2 relatedDowngradeProd = new Product2(Id = prodRel.vlocity_cmt__RelatedProductId__c, Name = prodRel.vlocity_cmt__RelatedProductId__r.Name );          
                    if(relTypeProductRelationships.containsKey('Downgrade'))
                    {
                         relTypeProductRelationships.get('Downgrade').add(relatedDowngradeProd);
                        //system.debug('***relatedProdList___:: '+relTypeProductRelationships);
                        //relatedProdList = relTypeProductRelationships.remove('Downgrade');
                        //system.debug('***objTempList '+objTempList);
                    }
                    else {
                        relTypeProductRelationships.put('Downgrade',new List<Product2>{relatedDowngradeProd});
                    }
                    //relatedProdList.add(relatedDowngradeProd);                       
                    //relTypeProductRelationships.put('Downgrade',relatedProdList);                   
                }              
            }            
         
        System.debug('Result_____' + relTypeProductRelationships);                    
        return relTypeProductRelationships;
    }
}