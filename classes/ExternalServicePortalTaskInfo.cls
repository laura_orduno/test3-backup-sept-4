public without sharing class ExternalServicePortalTaskInfo {
    public class ExternalServicePortalException extends Exception {}

    public Boolean loaded {get; private set;}
    public Boolean canComplete {get; private set;}

    private Boolean isTask {get; set;}
    private Task task {get; set;}
    private User user {get; set;}
    private Contact contact {get; set;}
    private Lead lead {get; set;}
    
    private Account account {get; set;}
    private Opportunity opportunity {get; set;}
    
    private Event event {get; set;}

    public ExternalServicePortalTaskInfo() {
        
    }
    public void onLoad() {
        Savepoint sp = database.setSavepoint();
        try {
            loaded = false;
            if (ApexPages.currentPage() == null || ApexPages.currentPage().getParameters() == null) { clear(); return; }
            String oid = ApexPages.currentPage().getParameters().get('oid');
            String tok = ApexPages.currentPage().getParameters().get('tok');
            if (tok == null || oid == null /*|| oid.length() != 18 */|| oid.substring(0, 3) != '00T') { clear(); return; }
            Id xid = (Id) oid;
            task = [Select Subject, Status, Priority, Description, IsClosed, WhoId, WhatId, ActivityDate, OwnerId, Token__c, External_Assignee__c, CreatedBy.Name From Task Where Id = :xid Limit 1];
            if (task == null) { clear(); throw new ExternalServicePortalException('Unable to load task'); return; }
            if (task.Token__c != tok) { clear(); throw new ExternalServicePortalException('Unable to load task.'); return; }
            user = [Select Name, Email From User Where Id = :task.OwnerId Limit 1];
            if (user == null) { clear(); throw new ExternalServicePortalException('Task loading failed.'); return; }
            if (task.WhoId != null) {
                if (String.valueOf(task.WhoId).substring(0, 3) == '003') {
                    // Contact!
                    contact = [Select Name, Phone, Email From Contact Where Id = :task.WhoId];
                } else if (String.valueOf(task.WhoId).substring(0, 3) == '00Q') {
                    // Lead!
                    lead = [Select Name, Phone, Email From Lead Where Id = :task.WhoId];
                }
            }
            
            if (task.WhatId != null) {
                if (String.valueOf(task.WhatId).substring(0, 3) == '001') {
                    account = [Select Name From Account Where Id = :task.WhatId Limit 1];
                } else if (String.valueOf(task.WhatId).substring(0, 3) == '006') {
                    opportunity = [Select Name From Opportunity Where Id = :task.WhatId Limit 1];
                }
            }
            
            canComplete = (task.IsClosed == null || task.IsClosed == false);   
            loaded = true;
        } catch (Exception e) { Database.rollback(sp); QuoteUtilities.log(e); }
    }
    
    public PageReference onComplete() {
        try {
            if (task == null) { throw new ExternalServicePortalException('No task loaded'); return null;}
            task.Status = 'Completed';
            if (task.SMB_Call_Type__c == null || task.SMB_Call_Type__c == '') { task.SMB_Call_Type__c = 'Not Applicable'; }
            if (task.Call_Outcome__c == null || task.Call_Outcome__c == '') { task.Call_Outcome__c = 'Not Applicable'; }
            update task;
            PageReference pr = ApexPages.currentPage();
            pr.setRedirect(true);
            return pr;
        } catch (Exception e) { QuoteUtilities.log(e); } return null;
    }
    
    public string getSubject() {
        if (task == null) {return '';}
        String subject = '';
        if (task.Subject != null) {
            subject += task.Subject;
        }
        String rt = getRelatedToName();
        if (rt != null && rt != '') {
            if (subject == '') {
                subject = 'Regarding ' + rt;
            }
        }
        return subject;
    }
    
    public string getRelatedTo() {
        if (task == null) {return '';}
        String subject = '';
        if (opportunity != null) {
            subject = opportunity.Name;
        } else if (account != null) {
            subject = account.Name;
        }
        return subject;
    }
    
    public string getRelatedToName() {
        if (task == null) {return '';}
        String subject = '';
        if (contact != null) {
            subject = contact.Name;
        } else if (lead != null) {
            subject = lead.Name;
        }
        return subject;
    }
    public string getStatus() {
        if (task == null) {return '';}
        return task.Status;
    }
    public string getDescription() {
        if (task == null) {return '';}
        return task.Description;
    }
    public string getPriority() {
        if (task == null) {return '';}
        return task.Priority;
    }
    public string getDueDate() {
        if (task == null) {return '';}
        return (task.ActivityDate == null) ? '' : task.ActivityDate.format();
    }
    
    public string getRelatedToPhone() {
        if (task == null) {return '';}
        String subject = '';
        if (contact != null) {
            subject = contact.Phone;
        } else if (lead != null) {
            subject = lead.Phone;
        }
        return subject;
    }
    
    public string getRelatedToEmail() {
        if (task == null) {return '';}
        String subject = '';
        if (contact != null) {
            subject = contact.Email;
        } else if (lead != null) {
            subject = lead.Email;
        }
        return subject;
    }
    
    public string getCreatedBy() {
        if (task == null) { return ''; }
        return task.CreatedBy.Name;
    }
    
    private void clear() {
        isTask = null;
        task = null;
        user = null;
        event = null;
    }
    
    private void addETP(String e) {
        ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, e));
    }
}