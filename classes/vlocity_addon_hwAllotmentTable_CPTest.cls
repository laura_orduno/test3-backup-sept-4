@isTest
public class vlocity_addon_hwAllotmentTable_CPTest {
    
        static testMethod void testMethodOne(){
            
 TestDataHelper.testContractWithHardwareChrunCreation('Hardware Allotment');
        vlocity_cmt__ContractLineItem__c line1 = new vlocity_cmt__ContractLineItem__c(Name='line1', vlocity_cmt__Status__c='Active', vlocity_cmt__Quantity__c=1, vlocity_cmt__OneTimeTotal__c=1000, vlocity_cmt__ContractId__c=TestDataHelper.testContractObj.Id);
        vlocity_cmt__ContractLineItem__c line2 = new vlocity_cmt__ContractLineItem__c(Name='line2', vlocity_cmt__Status__c='Active', vlocity_cmt__Quantity__c=1, vlocity_cmt__OneTimeTotal__c=2000, vlocity_cmt__ContractId__c=TestDataHelper.testContractObj.Id);
        vlocity_cmt__ContractLineItem__c line3 = new vlocity_cmt__ContractLineItem__c(Name='line3', vlocity_cmt__Status__c='Inactive', vlocity_cmt__Quantity__c=1, vlocity_cmt__OneTimeTotal__c=3000, vlocity_cmt__ContractId__c=TestDataHelper.testContractObj.Id);
        vlocity_cmt__ContractLineItem__c line4 = new vlocity_cmt__ContractLineItem__c(Name='line4', vlocity_cmt__Status__c='Inactive', vlocity_cmt__Quantity__c=1, vlocity_cmt__OneTimeTotal__c=3000, vlocity_cmt__ContractId__c=TestDataHelper.testContractObj.Id);
        
        List<vlocity_cmt__ContractLineItem__c> lineList = new List<vlocity_cmt__ContractLineItem__c> ();
        lineList.add(line1);
        lineList.add(line2);
        lineList.add(line3);
        lineList.add(line4);
        insert lineList;
            
            Map<String,Object> inputMap = new   Map<String,Object>(); 
               inputMap.put('contextObjId', TestDataHelper.testContractObj.Id);
               inputMap.put('contextObjName', 'Contract');          
               inputMap.put('items', lineList);
          inputMap.put('documentFontStyle', 'Aria, times, serif');
            
                            Map<String,Object> outMap =  new   Map<String,Object>();
                Map<String,Object> options = new   Map<String,Object>();
                
            vlocity_addon_hwAllotmentTable_CP classObj = new vlocity_addon_hwAllotmentTable_CP();
            classObj.invokeMethod('buildDocumentSectionContent',inputMap,outMap, Options);

        }

}