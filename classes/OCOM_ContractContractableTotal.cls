public class OCOM_ContractContractableTotal
{
    @InvocableMethod  
    public static void caluclateMRCNRC(List<Id> contractIds)
    {
        list<contract> contractlistToUpdate = new list<contract>();
        contractlistToUpdate = setContractTotalMRCNRC(contractIds);
        if(contractlistToUpdate.size()>0 && !contractlistToUpdate.isEmpty() )
                    update  contractlistToUpdate;

    }

    public static List<Contract> setContractTotalMRCNRC(List<Id> contractIds){
        Decimal TotalMRC = 0.00;
        Decimal TotalNRC = 0.00;
        Map<Id,String> conIDtoNameMap = new Map<Id,String>();
        List<contract> contractlistToUpdate = new List<contract>();
        try {
        map<string,vlocity_cmt__ContractLineItem__c> LinenumberTOCliIDMap = new map<string,vlocity_cmt__ContractLineItem__c>();

        map<id,contract> contractMap = new map<id,contract>();
        //list<cliWrapperClass> cliWrapperClassList = new list<cliWrapperClass>();
        for(vlocity_cmt__ContractLineItem__c cli: [select id,name, vlocity_cmt__LineNumber__c, 
                                                    vlocity_cmt__ContractId__c,  vlocity_cmt__ContractId__r.name,   
                                                    vlocity_cmt__Product2Id__r.vlocity_cmt__TrackAsAgreement__c,vlocity_cmt__Product2Id__r.Sellable__c,
                                                    vlocity_cmt__RecurringCharge__c,vlocity_cmt__OneTimeCharge__c,vlocity_cmt__RecurringTotal__c,vlocity_cmt__OneTimeTotal__c, vlocity_cmt__ContractId__r.Company_Name__c, vlocity_cmt__ContractId__r.CreatedDate

                                                    from vlocity_cmt__ContractLineItem__c
                                                    where vlocity_cmt__ContractId__c  = :contractIds
                                                    order by vlocity_cmt__ContractId__c,vlocity_cmt__LineNumber__c])
                {
                    //Clear the variables for a new contract set iteration
                   // system.debug('Contract Name' + cli.vlocity_cmt__ContractId__r.name);
                    if(!contractMap.containsKey(cli.vlocity_cmt__ContractId__c)){
                        TotalMRC = 0.00;
                        TotalNRC = 0.00;
                        LinenumberTOCliIDMap.Clear();
                        
                    }
                    string lnumber = string.valueof(cli.vlocity_cmt__LineNumber__c) != null? string.valueof(cli.vlocity_cmt__LineNumber__c):'';
                    string prarentLineNumber  = OCOM_ContractUtil.getRootlineNumber(lnumber);   
                    if(prarentLineNumber != null && LinenumberTOCliIDMap.containsKey(prarentLineNumber)){
                         Decimal mrc = cli.vlocity_cmt__RecurringTotal__c  != null ? cli.vlocity_cmt__RecurringTotal__c.setScale(2) : 0.00;
                        TotalMRC += mrc;
                         Decimal nrc = cli.vlocity_cmt__OneTimeTotal__c  != null ? cli.vlocity_cmt__OneTimeTotal__c.setScale(2) : 0.00;
                        TotalNRC += cli.vlocity_cmt__OneTimeTotal__c.setScale(2);
                    }
                        
                     else if(!LinenumberTOCliIDMap.containsKey(prarentLineNumber) && cli.vlocity_cmt__Product2Id__r.Sellable__c == true && cli.vlocity_cmt__Product2Id__r.vlocity_cmt__TrackAsAgreement__c == true){
                        LinenumberTOCliIDMap.put(prarentlinenumber,cli);
                        Decimal mrc = cli.vlocity_cmt__RecurringTotal__c  != null ? cli.vlocity_cmt__RecurringTotal__c.setScale(2) : 0.00;
                        TotalMRC += mrc;
                        Decimal nrc = cli.vlocity_cmt__OneTimeTotal__c  != null ? cli.vlocity_cmt__OneTimeTotal__c.setScale(2) : 0.00;
                        TotalNRC += nrc;
                       // system.debug('LinenumberTOCliIDMap_________:'+LinenumberTOCliIDMap);
                    }
                      if(conIDtoNameMap !=null && !conIDtoNameMap.containsKey(cli.vlocity_cmt__ContractId__c)){
                           String companyName =  cli.vlocity_cmt__ContractId__r.Company_Name__c != null ? cli.vlocity_cmt__ContractId__r.Company_Name__c.left(42): '';
                           Date createdDate = date.valueof(cli.vlocity_cmt__ContractId__r.CreatedDate);
                           String contractCreateDate = String.valueOf(createdDate);
                           System.debug('Contract Name ____:' + companyName + '-' +'AGR BA ' + contractCreateDate);
                           String contractName = companyName + '-' +'AGR BA ' + contractCreateDate;
                           conIDtoNameMap.put(cli.vlocity_cmt__ContractId__c, contractName);
                        }

                         contract con = new contract(id= cli.vlocity_cmt__ContractId__c);
                         con.Contractable_Monthly_Total__c = TotalMRC;
                         con.Contractable_OneTime_Total__c = TotalNRC;
                         if(conIDtoNameMap != null && conIDtoNameMap.containsKey(cli.vlocity_cmt__ContractId__c)){
                            con.Name = conIDtoNameMap.get(cli.vlocity_cmt__ContractId__c);
                         }
                         contractMap.put(cli.vlocity_cmt__ContractId__c, con);

                      
                       //system.debug('Contract Map______' + contractMap);
                    
                }

                system.debug('TotalNRC________' + TotalNRC);
                System.debug('TotalMRC________' + TotalMRC);
                
                if(contractMap.size() > 0 && !contractMap.isEmpty() )
                    contractlistToUpdate.addall(contractMap.values());

               } catch(exception e) {
                        System.debug(LoggingLevel.ERROR, 'Exception is '+e);
                        System.debug(LoggingLevel.ERROR, 'Exception stack trace '+e.getStackTraceString());
                        throw e;
                        return null;
               }

            //if(contractlistToUpdate.size()>0 && !contractlistToUpdate.isEmpty() )
                    return  contractlistToUpdate;
    }

    

   
}