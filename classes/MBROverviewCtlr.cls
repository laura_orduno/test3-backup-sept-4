/**
 *  MBROverviewCtlr.cls
 *
 *  @description
 *		The Controller for the Overview and Manage Requests Page.  Copied to a new file and cleaned by Dan.  Authored By Rauza, Christian, Jimmy and Alex as well.
 *
 *	@date - 03/01/2014
 *	@author - Dan Reich: Traction on Demand
 */
public with sharing class MBROverviewCtlr {
	// used for custom exception messages
    public transient String pageError {get; set;}
    public String categoryType {get; set;}
	public String q {get;set;}
    public Case c {get;set;}
    public String caseNumber {get; set;}
    public Boolean morePages {get; set;}
     public String mbrCustomer {get;set;}
    public Map<String, String> extStatusIntStatusMap {get;set;}
    public Map<String, String> intStatusExtStatusMap {get;set;}

    public String categoryFilter {get; set;}
    public String statusFilter {get; set;}

    private String caseOriginValue {get;set;}
    private List<Case> allCases;
	public Integer totalCases; //used to show user the total size of the list
	private Integer pageSize;
	public Integer totalPages {get; set;}
    public Integer selectedPage {get;set;}

	public List<CaseListing> topListings {get; set;}

	private Customer_Interface_Settings__c cis;
    
    public Boolean restrictTechSupport {get; set;}

	public MBROverviewCtlr() {
		cis = Customer_Interface_Settings__c.getInstance();
		if( cis == null ) {
			cis = new Customer_Interface_Settings__c(
				Manage_Requests_List_Limit__c = 10,
				Overview_Case_Limit__c = 5
			);
		}
        List<User> getMBRCustomer = [select id from User where Customer_portal_name__c = 'My Account' and email = :UserInfo.getUserEmail()]; 
        if(getMBRCustomer.size() > 0){
        mbrCustomer = getMBRCustomer[0].id;
    }
		pageSize = Integer.valueOf(cis.Manage_Requests_List_Limit__c); //sets the page size or number of rows
        String searchCriteria = ApexPages.currentPage().getParameters().get('search');
        q = '';
        if (String.isEmpty(q) && String.isNotEmpty(searchCriteria)) {
            q = searchCriteria;
        }

        // find the Case Origin string
        this.caseOriginValue = 'CCI Self-Serve';
        if(cis.Case_Origin_Value__c != null) {
            this.caseOriginValue = cis.Case_Origin_Value__c;
        }

        System.debug('caseOriginValue: ' + caseOriginValue);

		resetRequestView();
		categoryFilter = '';
		statusFilter = '';
        restrictTechSupport = MBRUtils.restrictTechSupport();

        List<ExternalToInternal__c> etis = ExternalToInternal__c.getAll().values();
        this.extStatusIntStatusMap = new Map<String, String>();
        this.intStatusExtStatusMap = new Map<String, String>();
        for (ExternalToInternal__c eti : etis) {
            if(eti.Identifier__c.equals('onlineStatus')) {
                this.extStatusIntStatusMap.put(eti.External__c, eti.Internal__c);
                this.intStatusExtStatusMap.put(eti.Internal__c, eti.External__c);
            }
        }
        //System.debug('this.intStatusExtStatusMap: ' + intStatusExtStatusMap);

        // make sure all case statuses have a valid mapping
        if (allCases.size() > 0) {
            for (Case c : allCases) {
                if (this.intStatusExtStatusMap.get(c.Status) == null) {
                    this.intStatusExtStatusMap.put(c.Status, 'Unknown');
                }
            }
        }
	}


    public void searchCase() {
        resetRequestView();
   // 	String searchCase = '%' + q + '%';
   // 	 List<Case> result = [SELECT Id, CaseNumber, Web_to_case_Type__c, Subject, Status, LastModifiedDate, My_Business_Requests_Type__c FROM Case WHERE CaseNumber LIKE :searchCase OR Subject LIKE :searchCase];
   //     if (result.size() ==1 ){
   //     	c = result[0];
	  //  }
	  //  else{ 
	  //  	allCases = result;
			//totalCases = allCases.size();
			//totalPages = getTotalPages();
			//selectedPage = 0;
	  //  }
    }

    public PageReference resetSearch() {
        q = '';
        resetRequestView();
        return null;
    }

    public PageReference searchOverview(){
    	PageReference pageRef = Page.MBRManageRequests;
    	pageRef.getParameters().put('search', q);
    	return pageRef;
    }
    
	public PageReference initCheck() {
        getRecentCases();
	    return null;
    }

	public PageReference createNewCase(){
		//categoryType = 'CCICatRequest11';
		PageReference pageRef = Page.MBRNewCase;
		if(categoryType != null){
			
			pageRef.getParameters().put('category', categoryType);
		}
        pageRef.setRedirect(true);

		return pageRef;
	} 

	public void getRecentCases(){
		Integer caseLimit = Integer.valueOf(cis.Overview_Case_Limit__c);
		List<Case> topCases = [SELECT RecordTypeId, Subject, LastModifiedDate, caseNumber, Status, My_Business_Requests_Type__c, Contact.FirstName, Contact.LastName
								FROM Case where Origin = 'My Business Requests'
								ORDER BY LastModifiedDate DESC
								LIMIT :caseLimit];
		topListings = new List<CaseListing>{};
		for(Case c:topCases){
			topListings.add(new CaseListing(c));
		}
	}

	public List<Case> getPageOfCases( Integer counter, Integer size ) {
        List<Case> listCases = new List<Case>();
        /*
        if (allCases == null) {
            // allCases should never be null... but just in case.
            resetRequestView();
        }
        */

        Boolean recount = false;
        listCases = executeSearch( counter, size, recount );
        
        /*
        } else {
            // allCases not set, need to execute our own search here
            Set<Case> tmpCases = new Set<Case>();
            String searchCriteria = ApexPages.currentPage().getParameters().get('search');
            if (String.isEmpty(searchCriteria) || searchCriteria.isNumeric()) {
            	String queryString = 'SELECT CaseNumber, Status, RecordTypeId, Subject, LastModifiedDate, My_Business_Requests_Type__c FROM Case';
            	String whereClause = '';
            	System.debug('aaaaaaaaaaaaaa '+searchCriteria);
            	if( String.isNotEmpty( categoryFilter ) ) {
            		whereClause += ' WHERE My_Business_Requests_Type__c IN ' + MBRUtils.getRequestTypeFilterForCategory( categoryFilter );
            	}
            	if( String.isNotEmpty( statusFilter ) ) {
            		if( String.isNotEmpty( whereClause ) ) {
            			whereClause += ' AND';
            		} else {
            			whereClause += ' WHERE';
            		}
            		whereClause += ' Status IN ' + MBRUtils.getInternalStatusFilterByExternal( statusFilter );
            	}
            	if(String.isNotEmpty(searchCriteria)){
            		q = searchCriteria;
            		resetRequestView();
            		ApexPages.currentPage().getParameters().put('search', '');
            	}
            	if(String.isNotEmpty(q) && q.isNumeric()){
            		if( String.isNotEmpty( whereClause ) ) {
            			whereClause += ' AND';
            		} else {
            			whereClause += ' WHERE';
            		}
            		String queryCase = '\'%' + q + '%\'';
            		 whereClause += ' CaseNumber LIKE ' + queryCase;
            	}
            	if( String.isNotEmpty( whereClause ) ) {
            		queryString += whereClause;
            	}
            	System.debug('qqqqqqqqqqqq' + q);
            	queryString += ' ORDER BY LastModifiedDate DESC';
            	queryString += ' LIMIT ' + size;
            	queryString += ' OFFSET ' + counter;

                List<Case> soqlCases = (List<Case>) database.query( queryString );
                if (soqlCases.size() > 0) {
                    tmpCases.addAll(soqlCases);
                }
            }

            if (String.isNotEmpty(searchCriteria)) {
                // now do a general begins-with sosl search
                q = searchCriteria;
                String qWild = q + '*';
                List<List<Case>> soslResults = [FIND :qWild IN ALL FIELDS RETURNING Case (CaseNumber, Status, RecordTypeId, Subject, LastModifiedDate, My_Business_Requests_Type__c)];
                List<Case> soslCases = (List<Case>)soslResults[0];

                // now merge the results
                if (soslCases.size() > 0) {
                    tmpCases.addAll(soslCases);
                }
            }

            // now sort the results
            if (tmpCases.size() > 0) {
                List<CaseWrapper> caseWrappers = new List<CaseWrapper>();
                for (Case cs : tmpCases) {
                    caseWrappers.add( new CaseWrapper(cs) );
                }
                caseWrappers.sort();

                // now stash the sorted cases into listCases
                for(CaseWrapper cw : caseWrappers) {
                    listCases.add(cw.mainCase);
                }
            }
        }
        */

    	return listCases;
	}



    public PageReference doFilterUpdate() {
    	// Need to run two queries so page numbers are updated....  not ideal.  Should do a 'count' instead.
    	resetRequestView();
    	return null;
    }

	public PageReference getCaseDetails(){
		//categoryType = 'CCICatRequest11';
		PageReference pageRef = Page.MBRCaseDetail;
		if(caseNumber != null){
			
			pageRef.getParameters().put('caseNumber', caseNumber);
		}
        pageRef.setRedirect(true);

		return pageRef;
	} 

	public List<CaseListing> allListings {
        get {
	        if( selectedPage == null || selectedPage <= 0 ){
	        	selectedPage = 1;
	        }
		    Integer counter = pageSize * (selectedPage - 1);
		    
		    try { 
	        	allListings = new List<CaseListing>{};
	        	//we have to catch query exceptions in case the list is greater than 2000 rows
                List<Case> cases = getPageOfCases( counter, pageSize ); 
	         	
				for( Case c : cases ){
					allListings.add( new CaseListing( c ) );
				}
                return allListings;
	        
	        } catch (QueryException e) {                            
	                ApexPages.addMessages(e);                   
	                return null;
	        }
	    }
        set;
    }
    
    public List<Integer> pageNumbers {
    	get{	        
	        Integer lowerLimit = 1;
	        if( selectedPage >= 4 && totalPages > 4 ) {
	        	Integer nearestEvenNumber = selectedPage - Math.mod( selectedPage, 2 );
	        	lowerLimit = (totalPages - nearestEvenNumber) >= 3 ? nearestEvenNumber - 1 : totalPages - 3; 
	        } 

	        Integer upperLimit = ( lowerLimit + 3 >= totalPages ) ? totalPages : lowerLimit + 3;

	        List<Integer> pageNumbers = new List<Integer>();
	        for( Integer i = lowerLimit; i <= upperLimit; i++ ) {
	        	pageNumbers.add( i );
	        }

	        if( upperLimit < totalPages ) {
	        	morePages = true;
	        } else {
	        	morePages = false;
	        }
	        return pageNumbers;
    	} 
    	set;
	}  

    public List<SelectOption> getCategoryFilterOptions() {
    	List<SelectOption> options = new List<SelectOption>();
        Boolean restrictTechSupport = MBRUtils.restrictTechSupport();
    	options.add( new SelectOption( '', label.mbrFilterAllCategories ) );
    	for( String category : MBRUtils.getCategoryRequestTypes().keySet() ) {
            if (!category.equals('Tech support') || !restrictTechSupport) {
                options.add( new SelectOption( category, category ) );
            }
    	}
    	return options;
    } 

    public List<SelectOption> getStatusFilterOptions() {
    	List<SelectOption> options = new List<SelectOption>();
    	options.add( new SelectOption( '', label.mbrFilterAllStatuses ) );
    	for( String status : MBRUtils.getExternalToInternalStatuses().keySet() ) {
    		options.add( new SelectOption( status, status ) );
    	}
    	return options;
    } 
    
    public PageReference refreshGrid() { //user clicked a page number  
        return null;
    }
    
    public PageReference Previous() { //user clicked previous button
    	selectedPage--;
        return null;
    }

    public PageReference Next() { //user clicked next button
    	selectedPage++;
        return null;
    }
    
    public Boolean getDisablePrevious() { //this will disable the previous and beginning buttons
        return selectedPage <= 1;
    }

    public Boolean getDisableNext() { //this will disable the next and end buttons
    	return selectedPage >= totalPages;
    }

    private Integer getTotalPages() {
		Integer totalPages = totalCases / pageSize;
		if( math.mod(totalCases, pageSize) > 0 ) {
			totalPages += 1;
		}
		return totalPages;
    }

	private void resetRequestView() {
        Boolean recount  = true;
        allCases = executeSearch( 0, pageSize, recount );
        totalCases = allCases.size();
        totalPages = getTotalPages();
        selectedPage = 0;
    }

    public List<Case> executeSearch( Integer counter, Integer size, Boolean recount ) {
        Set<Case> tmpCases = new Set<Case>();
        List<Case> soqlCases = new List<Case>();
        List<Case> soslCases = new List<Case>();
        List<Case> retval = new List<Case>();
        String soslCasesIdString = '';

        /*
        String searchCriteria = ApexPages.currentPage().getParameters().get('search');
        if (String.isEmpty(q) && String.isNotEmpty(searchCriteria)) {
            q = searchCriteria;
        }
        */

        if (String.isNotEmpty(q) && q.length() > 1) {
            // now do a general begins-with sosl search
            String qWild = q + '*';
            String whereClause = ' WHERE Origin = \'' + String.escapeSingleQuotes(this.caseOriginValue) + '\' ';
            String whereJoiner = ' AND ';
            if( String.isNotEmpty( categoryFilter ) ) {
                whereClause += whereJoiner + 'My_Business_Requests_Type__c IN ' + MBRUtils.getRequestTypeFilterForCategory( categoryFilter );
                //whereJoiner = ' AND ';
            }
            if( String.isNotEmpty( statusFilter ) ) {
                whereClause += whereJoiner + ' Status IN ' + MBRUtils.getInternalStatusFilterByExternal( statusFilter );
                //whereJoiner = ' AND ';
            }
            String orderByClause = ' ORDER BY LastModifiedDate DESC ';
            String limitOffsetClause = ' LIMIT ' + size + ' OFFSET ' + counter;
            String allSoslQuery = 'FIND \'' + String.escapeSingleQuotes(qWild) +'\' IN ALL FIELDS RETURNING Case (Id, CaseNumber, Subject, Description ' + whereClause + orderByClause + ')';
            System.debug('akong: executeSearch: allSoslQuery: ' + allSoslQuery);
            try {
                List<List<Case>> allSoslResults = search.query(allSoslQuery);
                soslCases = (List<Case>)allSoslResults[0];
            } catch (Exception e) {
                System.debug('akong: executeSearch: sosl search exception: ' + e);
            }
            System.debug('akong: executeSearch: soslCases: ' + soslCases);
            // build the string of case IDs to use in the main soql query
            if (soslCases.size() > 0) {
                for(Case soslCase : soslCases) {
                    soslCasesIdString += (String.isNotEmpty(soslCasesIdString)?', ':'') + '\''+soslCase.Id+'\'';
                }
                soslCasesIdString = '(' + soslCasesIdString + ')';
            } else if (!q.isNumeric()) {
                // search was a non-numeric keyword search, and we got no hits... time to bail!
                return soslCases;
            }
        }

        System.debug('akong: executeSearch: soslCases.size(): ' + soslCases.size());

    	// time to build the main soql query
        String queryString = 'SELECT CaseNumber, Status, RecordTypeId, Subject, LastModifiedDate, My_Business_Requests_Type__c, Contact.FirstName, Contact.LastName FROM Case';
    	String whereClause = ' WHERE Origin = \'' + String.escapeSingleQuotes(this.caseOriginValue) + '\' ';
        String whereJoiner = ' AND ';
    	if( String.isNotEmpty( categoryFilter ) ) {
    		whereClause += whereJoiner + ' My_Business_Requests_Type__c IN ' + MBRUtils.getRequestTypeFilterForCategory( categoryFilter );
            //whereJoiner = ' AND ';
    	}
    	if( String.isNotEmpty( statusFilter ) ) {
    		whereClause += whereJoiner + ' Status IN ' + MBRUtils.getInternalStatusFilterByExternal( statusFilter );
            //whereJoiner = ' AND ';
    	}
    	if(String.isNotEmpty(q) && q.isNumeric()) {
    		// case number search
            String queryCase = '\'%' + q + '%\'';
    		whereClause += whereJoiner + ' (CaseNumber LIKE ' + queryCase;
            if (soslCases.size() > 0) {
                // fold in list of case IDs from sosl search
                whereClause += ' OR Id IN ' + soslCasesIdString;
            }
            whereClause += ')';
            whereJoiner = ' AND ';
    	} else if (soslCases.size() > 0) {
            // we have a list of case IDs from the sosl search
            whereClause += whereJoiner + ' Id IN ' + soslCasesIdString;
            whereJoiner = ' AND ';
        }
    	if( String.isNotEmpty( whereClause ) ) {
    		queryString += whereClause;
    	}
    	String orderByClause = ' ORDER BY LastModifiedDate DESC';
        String limitOffsetClause = ' LIMIT ' + size + ' OFFSET ' + counter;
        System.debug('akong: executeSearch: queryString: ' + queryString);
        System.debug('akong: executeSearch: orderByClause: ' + orderByClause);
        System.debug('akong: executeSearch: limitOffsetClause: ' + limitOffsetClause);
    	if (recount) {
            soqlCases = (List<Case>) database.query( queryString + orderByClause );
        } else {
            soqlCases = (List<Case>) database.query( queryString + orderByClause + limitOffsetClause );
        }

        // catchall logic if we searched for non-numeric keyword and got nothing from keyword search
        if (String.isNotEmpty(q) && !q.isNumeric() && soslCases.size() == 0) {
            soqlCases.clear();
        }

        System.debug('akong: executeSearch: soqlCases: ' + soqlCases);
        retval.addAll( soqlCases );

        if (retval.size() == 1 && retval[0].CaseNumber == q){
            c = retval[0]; // immediate redirect to case details
	    }

        return retval;
	}

	public class CaseListing{
		public String status {get; set;}
		public String statusIcon {get; set;}
		public String refNo {get; set;}
		public String type {get; set;}
		public String subject {get; set;}
		public String timeDiff {get; set;}
        public String CreatedBy {get; set;}

		public CaseListing(Case tmpCase){
			this.refNo = tmpCase.caseNumber;
			this.type = getRecordType(tmpCase);
			this.subject = tmpCase.Subject;
			this.status = tmpCase.status;
                this.CreatedBy = tmpCase.Contact.FirstName + ' ' + tmpCase.Contact.LastName;
			this.statusIcon = 'status-'+tmpCase.status;
			this.timeDiff = getTimeDiff(tmpCase.LastModifiedDate);
		}

		public String getRecordType(Case tmpCase){
			String internalName = Schema.SObjectType.Case.getRecordTypeInfosById().get(tmpCase.RecordTypeId).getName();
	        List<ExternalToInternal__c> etis = ExternalToInternal__c.getAll().values();
	        String typeName = tmpCase.My_Business_Requests_Type__c;
	        if(internalName != null){
		        for (ExternalToInternal__c eti : etis) {
		            if(eti.Identifier__c.equals('onlineCaseRTToRequestType') && eti.Internal__c.equals(internalName)) {
		                typeName = eti.External__c;
		            }
		        }
		    }
		    if(typeName == '' || typeName == null){
		    	typeName = Label.MBRUnknownRecordType;
		    }
	        return typeName;
	    }

		public String getTimeDiff(DateTime modifDate){
			String dateDif = '';
			DateTime todayDate  = DateTime.now();

			if(modifDate!=null){
				Integer hourDif = Integer.valueOf((todayDate.getTime() - modifDate.getTime())/(1000*60*60));
				Integer hoursInMonth = 24*30;

				if(hourDif==0){
					dateDif = 'now';
				}
				else if(hourDif == 1){
					dateDif = '1 hour ago';
				}
				else if(hourDif<24){
					dateDif = hourDif + ' hours ago';
				}
				else if(hourDif < 48){
					dateDif = '1 day ago';
				}
				else if(hourDif < hoursInMonth){
					dateDif = hourDif/24 + ' days ago';
				}
				else if(hourDif < hoursInMonth*2){
					dateDif = 'over 1 month';
				}
				else{
					dateDif = 'over ' + hourDif/hoursInMonth + 'months ago';
				}
			}
			return dateDif;
		}
	}
    /*
    public class CaseWrapper implements Comparable {

        public Case mainCase;
        
        // Constructor
        public CaseWrapper(Case cs) {
            mainCase = cs;
        }
        
        // Compare cases based on LastModifiedDate
        public Integer compareTo(Object compareTo) {
            // Cast argument to CaseWrapper
            CaseWrapper compareToMainCase = (CaseWrapper)compareTo;
            
            // The return value of 0 indicates that both elements are equal.
            long dt1 = mainCase.LastModifiedDate.getTime() / 1000;
            long dt2 = compareToMainCase.mainCase.LastModifiedDate.getTime() / 1000;
            Integer returnValue = 0;
            if (dt1 < dt2) {
                // Set return value to a positive value.
                returnValue = 1;
            } else if (dt1 > dt2) {
                // Set return value to a negative value.
                returnValue = -1;
            }
            
            return returnValue;       
        }
    }
    */

}