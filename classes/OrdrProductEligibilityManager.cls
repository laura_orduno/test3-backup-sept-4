/*******
 * [Roderick de Vera]
 * Utility class to manage Offer Eligibility
 *******/
public class OrdrProductEligibilityManager {
    //public Static boolean UpdateDownloadSpeed=True;
    
    /*******
     *  inner classes to support simplification of eligibility structure
     *******/
    public class Offer {
        public String key;
        public String name;
        public String availability;
        public String specification;
        public Map<String, List<Value>> characteristics;
        public Map<String, Offer> childOffers;
    }
    
    public class Value {
        public String key;
        public String label;
        public String availability;
        public Integer ordinalSequence;
    }
    
    /*******
     *  inner class to support simplification of rate group and forbearance info structure
     *******/
    public class RateGroupInfo {
        public String rateGroupAttribute;
        public String rateBand;
        public String rateSubBand;
        public Boolean isForborne;
    }
    
    /*******
     * utility method to retrieve the fielded service address associated for a given object (e.g. Order, Quote, Address, Account)
     *******/
    private static Map<String, String> retrieveServiceAddress(Id objectId) {
        
        Id serviceAddressId = getServiceAddressId(objectId);
        return OrdrUtilities.constructServiceAddress(serviceAddressId);
        
    }

     /*******
     * utility method to retrieve the service address Id associated for a given object (e.g. Order, Quote, Address, Account)
     *******/
    private static Id getServiceAddressId(Id objectId) {
        
        String objectType = objectId.getSObjectType().getDescribe().getName();
        Id serviceAddressId;
        
        if (objectType == 'Quote') {
            Quote quote = [select Service_Address__r.Id from quote where Id = :objectId LIMIT 1];
            serviceAddressId = quote.Service_Address__r.Id;
        }
        else if (objectType == 'Order') {
            Order order = [select Service_Address__r.Id from order where Id = :objectId LIMIT 1];
            serviceAddressId = order.Service_Address__r.Id;
        }
        else if (objectType == 'Account') {
            SMBCare_Address__c address = [select Id from SMBCare_Address__c where Service_Account_Id__c = :objectId LIMIT 1];
            serviceAddressId = address.Id;
        } 
        else if (objectType == 'SMBCare_Address__c') {
            serviceAddressId = objectId;
        }
        
        system.debug('serviceAddressId=' + objectType + ':' +serviceAddressId);

        return serviceAddressId;
        
    }
    
     /*******
     * utility method to retrieve the price book Id associated for a given object (e.g. Order, Quote)
     *******/
    public static Id retrievePriceBookId(Id objectId) {
        
        String objectType = objectId.getSObjectType().getDescribe().getName();
        Id priceBookId;
        
        if (objectType == 'Quote') {
            Quote quote = [select pricebook2Id from quote where Id = :objectId LIMIT 1];
            priceBookId = quote.pricebook2Id;
        }
        else if (objectType == 'Order') {
            Order order = [select pricebook2Id from order where Id = :objectId LIMIT 1];
            priceBookId = order.pricebook2Id;
        }
        
        System.Debug('priceBookId='+ priceBookId );

        return priceBookId;
        
    }

     /*******
     * function to transform the Offer Eligibility response from the WS callout into a map of Offer class hierarchy
     *******/
    private static Map<String, Offer> constructAvailableOffers(TpFulfillmentCustomerOrderV3.CustomerOrder customerOrderResponse) {
        Map<String, Offer> availableOffers = new Map<String, Offer>();

        for(TpFulfillmentProductOrderV3.ProductOrderItem productOrderItem : customerOrderResponse.ProductOrderItem) {
            Offer offer = decomposeProductOffer(productOrderItem);
            availableOffers.put(offer.key, offer);
        }
        return availableOffers;
    }
    
     /*******
     * recursive function to map the Offer Eligibility response from the WS callout into a map of Offer class hierarchy
     *******/    
    private static Offer decomposeProductOffer(TpFulfillmentProductOrderV3.ProductOrderItem productOrderItem) {
        
        Offer offer = new Offer();
        for(TpCommonBaseV3.CharacteristicValue charVal : productOrderItem.CharacteristicValue) {
            System.debug(charVal.Characteristic.Name + '=' + charVal.Value[0]);
            if (charVal.Characteristic.Name=='offeringKey' || charVal.Characteristic.Name=='categoryKey') offer.key = charVal.Value[0];
            if (charVal.Characteristic.Name == 'name') offer.name = charVal.Value[0];
            if (charVal.Characteristic.Name=='available') offer.availability = charVal.Value[0];
            
            if (charVal.Characteristic.Name=='availableOffersList' || charVal.Characteristic.Name=='limitedAvailableOffersList') {
                if (offer.childOffers == null) {
                    offer.childOffers = new Map<String, Offer>();
                }
                for(String key : charVal.Value[0].split(',')) {
                    Offer childOfferCategory = new Offer();
                    childOfferCategory.key = key;
                    childOfferCategory.availability = (charVal.Characteristic.Name=='limitedAvailableOffersList') ? 'limited' : 'available';
                    offer.childOffers.put(childOfferCategory.key, childOfferCategory); 
                }
            }
        }
        
        if (productOrderItem.ComponentProductOrderItem != null) {
            Map<String, List<Value>> characteristicMap = new Map<String, List<Value>>();
            
            for(TpFulfillmentProductOrderV3.ProductOrderItem component : productOrderItem.ComponentProductOrderItem) {
                TpCommonBaseV3.EntitySpecification specification = component.Specification;
                if (specification != null) {
                    if (specification.Type_x == 'Child Offer' || specification.Type_x == 'Child Offer Category') {
                        if (offer.childOffers == null) {
                            offer.childOffers = new Map<String, Offer>();
                        }
                        Offer childOffer = decomposeProductOffer(component);
                        childOffer.specification = specification.Type_x;
                        offer.childOffers.put(childOffer.key, childOffer);
                    }
                    else if (specification.Type_x == 'Offer Characteristic') {
                        List<Value> values = new List<Value>();
                        Value value = new Value();
                        for(TpCommonBaseV3.CharacteristicValue charVal : component.CharacteristicValue) {
                            System.debug(charVal.Characteristic.Name + '=' + charVal.Value[0]);
                            if (charVal.Characteristic.Name == 'value') value.key = charVal.Value[0];
                            if (charVal.Characteristic.Name == 'label') value.label = charVal.Value[0];
                            if (charVal.Characteristic.Name == 'available') value.availability = charVal.Value[0];
                            if (charVal.Characteristic.Name == 'orderNumber') value.ordinalSequence = integer.ValueOf(charVal.Value[0]);  
                        }
                        if (characteristicMap.containsKey(specification.Name)) {
                            characteristicMap.get(specification.Name).add(value);           
                        }
                        else {
                            values.add(value);
                            system.debug('offer.Name = ' + offer.Name);
                            system.debug('specification.Name = ' + specification.Name);
                            system.debug('Values____' + values);
                            characteristicMap.put(specification.Name, values);
                        }
                    }
                }
            }
            offer.characteristics = characteristicMap;
        }
        
        return offer;
    }
 
     /*******
     * function to get all the available offers for a given service location associated to an object (e.g. Order,  Quote)
     * the available offers will be stored in the platform Org cache so that subsequent call to get the available offers will just be retrieve from the cache
     *******/    
    public static Map<String, Offer> getAvailableOffers(Id objectId) {
        
        TpFulfillmentCustomerOrderV3.CustomerOrder customerOrderResponse;
        Map<String, Offer> availableOffers = new Map<String, Offer>();
        Map<String, String> addressInfoMap = new Map<String, String>();
        
        addressInfoMap = retrieveServiceAddress(objectId);

        // retrieve/store Eligibility results from/to platform cache
        if (addressInfoMap != null && !addressInfoMap.isEmpty()) {
            
            String availableOffersKey = 'local.orderingCache.availableOffers';
            if (addressInfoMap.containsKey(OrdrConstants.FMS_ID) && addressInfoMap.get(OrdrConstants.FMS_ID) != null) {
                availableOffersKey += addressInfoMap.get(OrdrConstants.FMS_ID);                 
            } else if (addressInfoMap.containsKey(OrdrConstants.IS_ILEC) && addressInfoMap.get(OrdrConstants.IS_ILEC) == 'Yes') {
                //Invalid address (e.g. SACG)
                return availableOffers;
            } else {
                availableOffersKey += addressInfoMap.get(OrdrConstants.LOCATION_ID); //non-ilec
            }

            if(!Test.isRunningTest() && Cache.Org.contains(availableOffersKey)) {
                availableOffers = (Map<String, Offer>) Cache.Org.get(availableOffersKey);
            } else {  
                customerOrderResponse = OrdrProductEligibilityWsCallout.performCustomerOrderFeasibility(addressInfoMap); 
                availableOffers = constructAvailableOffers(customerOrderResponse);
                if (!Test.isRunningTest()) Cache.Org.put(availableOffersKey, availableOffers);                       
            }
                
        }
        else {
            throw new OrdrExceptions.InvalidParameterException(Label.DFLT0002.replace('{0}', 'Product Eligibility'));
        }
        
        return availableOffers;
                
    }
    
     /*******
     * utility function to retrieve a map of available children and grandchildren of a given offer
     *******/    
    public static Map<Id, PriceBookEntry> getAvailableChildOffers(Id objectId, String offerExternalId) {
        return getAvailableChildOffers(objectId, new List<String>{offerExternalId});
    }
    

     /*******
     * utility function to retrieve a map of available children and grandchildren of a given list of offers
     *******/    
    public static Map<Id, PriceBookEntry> getAvailableChildOffers(Id objectId, List<String> offerExternalIds) {
    
        String METHOD_NAME ='OrdrProductEligibilityManager_getAvailableChildOffers';   
        //##################################################################
        // VlocityLogger.e(METHOD_NAME);
        //##################################################################
 
        Map<Id, PriceBookEntry> pbes = new Map<Id, PriceBookEntry>();
		String[] availableOffersAndChildren = new List<String>();
        
        Map<String, Offer> offers = getAvailableOffers(objectId);
        
        for (String offerExternalId : offerExternalIds) {
            Offer offer = offers.get(offerExternalId);
            if (offer != null) {
                Map<String, Offer> parentOffer = new Map<String, Offer>();
                parentOffer.put(offer.key, offer);
                String[] availableChildOffers =  getAvailableOfferIds(parentOffer);
                availableOffersAndChildren.addAll(availableChildOffers);
            }
        }
        
        if (availableOffersAndChildren.size() > 0) {
            String priceBookId = retrievePriceBookId(objectId);
            pbes = new Map<Id, PriceBookEntry>([SELECT Id, Pricebook2Id, Product2Id, Product2.OrderMgmtId__c 
                                                FROM PriceBookEntry 
                                                WHERE Pricebook2Id = :priceBookId
                                                AND Product2.OrderMgmtId__c in :availableOffersAndChildren]);
        }

        //##################################################################
        // VlocityLogger.x(METHOD_NAME);
        //##################################################################
    
        return pbes;
    }
    
     /*******
     * utility function to retrieve a map of available characteristics of a given offer
     *******/     
    public static Map<String, String> getOfferAvailableCharacteristics(Id objectId, String offerExternalId) {
        return getOfferAvailableCharacteristics(objectId, new List<String>{offerExternalId});
    }
    
     /*******
     * utility function to retrieve a map of available characteristics of a given offer
     *******/     
    public static Map<String, String> getOfferAvailableCharacteristics(Id objectId, List<String> offerExternalIds) {
        Map<String, String> characteristicsMap = new Map<String, String>();
        Map<String, Offer> offers = getAvailableOffers(objectId);
        
        for (String offerExternalId : offerExternalIds) {
            Offer offer = offers.get(offerExternalId);
            if (offer != null && offer.characteristics != null) {
               // system.debug('offer.key=' + offer.key);
               // system.debug('offer.name=' + offer.name);
                characteristicsMap.putAll(flattenCharacteristicsHierarchy(offer));
            }
        }
   
        return characteristicsMap;
    }
    
    private static Map<String, String> flattenCharacteristicsHierarchy(Offer offer) {
        Map<String, String> characteristicsMap = new Map<String, String>();
        //System.debug('Offer___'+ JSON.serializePretty(offer));
        
        for (String characteristicName : offer.characteristics.keySet()) {
            for (Value charVal : offer.characteristics.get(characteristicName)) {
                system.debug('characteristicName: ' + characteristicName);
                if (charVal.availability.equalsIgnoreCase('available')) {
                    String key = offer.key + ':' + characteristicName;
                    characteristicsMap.put(key, charVal.key);
                    system.debug('getOfferAvailableCharacteristics: ' + key + '=' + charVal.key);
                }
            }
        }
        
        if (offer.childOffers != null) {
            for (String childOfferKey : offer.childOffers.keySet()) {
                Offer childOffer = offer.childOffers.get(childOfferKey);
                if (childOffer != null && childOffer.characteristics != null) {
                    characteristicsMap.putAll(flattenCharacteristicsHierarchy(childOffer));
                }
            }
        }
        
        return characteristicsMap;
    }
    
     /*******
     * utility function to retrieve a list of available offer Ids
     *******/     
    public static List<String> getAvailableOfferIds(Map<String, Offer> offers){
        List<String> availableOfferIds = new List<String>();

        for(String key : offers.keySet()) {
            Offer offer = offers.get(key);
            if(offer.availability.equalsIgnoreCase('available') 
               || offer.availability.equalsIgnoreCase('limited') 
               || offer.availability.equalsIgnoreCase('unknown')) {
                   
                   //Roderick: temporary solution for 2-play bundle until NC implements the long-term fix                       
                   if (String.isNotBlank(offer.specification)
                       && offer.specification.equalsIgnoreCase('Child Offer')
                       && offer.availability.equalsIgnoreCase('limited') 
                       && (offer.key == '9143210930013949423' || offer.key == '9145986681013643123')) {
    					//do nothing: temporary fix for 2-play bundle
    					system.debug('2-Play child offer key:' + offer.key);
    					system.debug('2-Play child offer name:' + offer.name);
    					system.debug('2-Play child availability:' + offer.availability);
                   } else {
                       availableOfferIds.add(offer.key);
                       if (offer.childOffers != null) {
                           availableOfferIds.addAll(getAvailableOfferIds(offer.childOffers)); 
                       }
                   }
                   /* this blocked is replaced temporarily by the above code
                   availableOfferIds.add(offer.key);
                   if (offer.childOffers != null) {
                       availableOfferIds.addAll(getAvailableOfferIds(offer.childOffers)); 
                   }
				    */
               }           
        }
        return availableOfferIds;   
    } 
    
    
     /*******
     * function to revalidate the cart line items when the service address of the Quote/Order changed
     *******/ 
    public static void validateCartLineItems(Id objectId, String[] availableOfferIds) {

        if (hasServiceAddressChanged(objectId)) {
            String objectType = objectId.getSObjectType().getDescribe().getName();
            
            if (objectType == 'Quote') {
                List<QuoteLineItem> quoteItems = [select Id, vlocity_cmt__ProvisioningStatus__c
                                                  from QuoteLineItem where QuoteId = :objectid 
                                                  and Product2.OrderMgmtId__c not in :availableOfferIds
                                                  and vlocity_cmt__ProvisioningStatus__c != 'Deleted'];
                Quote quote = [select Id, vlocity_cmt__ValidationMessage__c from Quote where Id = :objectId LIMIT 1];
                if (quoteItems.size() > 0) {
                    for (QuoteLineItem quoteItem : quoteItems) {
                        quoteItem.vlocity_cmt__ProvisioningStatus__c = 'Not Available';
                    }  
                    update quoteItems;
                    
                    quote.vlocity_cmt__ValidationMessage__c = 'Please ‘Review Cart’ and delete any items marked ‘Not Available’ to complete the order. ';
                } else {
                    quote.vlocity_cmt__ValidationMessage__c = '';
                }
                update quote;
                
            } else if (objectType == 'Order') {
                List<OrderItem> orderItems = [select Id, vlocity_cmt__ProvisioningStatus__c 
                                              from OrderItem where OrderId = :objectid 
                                              and vlocity_cmt__Product2Id__r.OrderMgmtId__c not in :availableOfferIds
                                              and vlocity_cmt__ProvisioningStatus__c != 'Deleted'];
                Order order = [select Id, vlocity_cmt__ValidationMessage__c from Order where Id = :objectId LIMIT 1];
                if (orderItems.size() > 0) {
                    for (OrderItem orderItem : orderItems) {
                        orderItem.vlocity_cmt__ProvisioningStatus__c = 'Not Available';
                    }
                    update orderItems;
                    
                    order.vlocity_cmt__ValidationMessage__c = 'Please ‘Review Cart’ and delete any items marked ‘Not Available’ to complete the order. ';
                } else {
                    order.vlocity_cmt__ValidationMessage__c = '';
                }
                update order;
            } 
        }
    }
    
     /*******
     * utility function to determine if the service address of the Quote/Order changed
     *******/     
    private static Boolean hasServiceAddressChanged(Id objectId) {
        
        Id serviceAddressId = getServiceAddressId(objectId);
        
        String orderAddressKey = 'local.orderingCache.quoteOrder';
        orderAddressKey += objectId;                 
        if(Cache.Session.contains(orderAddressKey)) {
            if (serviceAddressId != (Id) Cache.Session.get(orderAddressKey)) return true;
        }  
        Cache.Session.put(orderAddressKey, serviceAddressId);
        
        //this line is added for better test coverage
        if (Test.isRunningTest( )) return true;
           
        return false;
    }

    
     /*******
     * function to retrieve a Rate Group and Forbearance information of a given service address
     * the information are stored in SMBCare_Address object
     *******/     
    public static RateGroupInfo retrieveRateGroupInfo(Id serviceAddressId) {
        
        //Id serviceAddressId = getServiceAddressId(objectId);
        SMBCare_Address__c address = [select Id, npa_nxx__c, rate_group__c, rate_band__c, rate_sub_band__c, forbone__c from SMBCare_Address__c where Id = :serviceAddressId LIMIT 1];
        
        if (String.isNotEmpty(address.npa_nxx__c)) {
            RateGroupInfo rateGroupInfo = new RateGroupInfo();
            
            RateGroupInfo_ResourceRes.RateGroupAndForborneAttribute attribute = OrdrRateGroupInfoWsCallout.getRateGroupAndForborneInformationByNpaNxx(address.npa_nxx__c);
            RateGroupInfo_ResourceRes.RateGroup rateGroup = attribute.rateGroupAttribute.rateGroupHistoryList.rateGroupList[0];
            
            rateGroupInfo.rateGroupAttribute = rateGroup.rateGroupAttribute;
            rateGroupInfo.rateBand = rateGroup.rateBand;
            rateGroupInfo.rateSubBand = rateGroup.rateSubBand;
            rateGroupInfo.isForborne = attribute.exchangeForborneStatus.exchangeForborneStatusInd;
            
            address.rate_group__c = rateGroupInfo.rateGroupAttribute;
            address.rate_band__c = rateGroupInfo.rateBand;
            address.rate_sub_band__c = rateGroupInfo.rateSubBand;
            address.forbone__c = rateGroupInfo.isForborne;
            
            update address;
            
            return rateGroupInfo;
            
        } else {
            String msg = Label.DFLT0004;
            msg = msg.replace('{0}', 'Rate Group and Forbearance Info');
            msg = msg.replace('{1}', '["npa/nxx"]');
            throw new OrdrExceptions.InvalidParameterException(msg);    
        }
    
    }
    
     /*******
     * asynchronous wrapper function to retrieve Rate Group info
     *******/     
    @future(Callout=True)
    public static void saveRateGroupInfo(Id serviceAddressId){
        retrieveRateGroupInfo(serviceAddressId);
    }
    
     /*******
     * asynchronous wrapper function to cache Offer Eligibility
     *******/         
    @future(Callout=True)
    public static void cacheAvailableOffers(Id serviceAddressId){
        Map<String, Offer> availableOffers = getAvailableOffers(serviceAddressId);
    }
    

    /******** Deprecated: 10/20/2016        
    public static void persistMaximumSpeed(Id objectId, String maxSpeed) {
        
        String objectType = objectId.getSObjectType().getDescribe().getName();
        
        if (objectType == 'Quote') {
            Quote quote = [select maximum_speed__c from quote where Id = :objectId LIMIT 1];
            quote.maximum_speed__c = maxSpeed;
            update quote;
        }
        else if (objectType == 'Order') {
            Order order = [select maximum_speed__c from order where Id = :objectId LIMIT 1];
            order.maximum_speed__c = maxSpeed;
            update order;
        }
        else if (objectType == 'Account') {
            SMBCare_Address__c address = [select maximum_speed__c from SMBCare_Address__c where Service_Account_Id__c = :objectId LIMIT 1];
            address.maximum_speed__c = maxSpeed;
            update address;
        }
       else if (objectType == 'SMBCare_Address__c') {
            if(UpdateDownloadSpeed)
                {
                UpdateDownloadSpeed=false;
                SMBCare_Address__c address = [select maximum_speed__c from SMBCare_Address__c where Id = :objectId LIMIT 1];
                address.maximum_speed__c = maxSpeed;
                System.debug('maxSpeed###'+maxSpeed);
                update address;
                }
                
        }

        system.debug('maxSpeed=' + objectType + ':' +maxSpeed);
        
    }
    ***********/ 
    
    
    /******** Deprecated: 10/20/2016
    public static String getMaximumSpeed(Map<String, Offer> availableOffers) {
        String maxSpeed = '';
        
        for(String key : availableOffers.keySet()) {
            Offer offer = availableOffers.get(key);
            if (offer.characteristics!=null && offer.characteristics.containsKey('Download speed')) {
                for (Value value : offer.characteristics.get('Download speed')) {              
                    if(value.availability == 'available' || value.availability == 'limited'){              
                        maxSpeed = value.label + (value.availability == 'limited' ? ' (Limited)' : '');                                          
                        return maxSpeed;
                    }      
                }  
            }
        }
        return maxSpeed;
    }  
    ********/ 
           
}