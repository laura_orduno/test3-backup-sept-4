global with sharing class RevokeSignatureManual_CP implements vlocity_cmt.VlocityOpenInterface{
    
    public Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {
        Boolean success = true;
        try{
            if(methodName == 'revokeContract') {             
                 revokeContract(inputMap, outMap, options);                
            }
          }catch(Exception e){
                    
                    success=false;
         }

        return success;        
    }
    
     private static void revokeContract(Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options){
         
         list<string> errorMessages = new list<string>();
         if(inputMap.containsKey('contextId'))
         {
             Id contextId = (id)inputMap.get('contextId');
           	 Contract conObj = new Contract (id = contextId);
             conObj.Status = 'Draft';
             update conObj;
     
         }
    }

}