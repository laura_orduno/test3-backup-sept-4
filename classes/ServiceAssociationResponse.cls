public class ServiceAssociationResponse {
    public string userProfileId{get;set;}
    public string userProfileStatusTxt{get;set;}
    public string firstNameTxt{get;set;}
    public string lastNameTxt{get;set;}
    public string clientTypeTxt{get;set;}
    public string clientRoleTypeTxt{get;set;}
    public string accountTypeCd{get;set;}
    public string accountSubTypeCd{get;set;}
    public Status status{get;set;}
    public ServiceAssociationResponse(){
        status=new Status();
    }
    public class Status{
        public string statusCd{get;set;}
        public string statusSubCd{get;set;}
        public string statusTxt{get;set;}
        public string systemErrorTimestamp{get;set;}
        public string systemErrorCd{get;set;}
        public string systemErrorTxt{get;set;}
    }
    public static ServiceAssociationResponse parse(httpresponse response){
        ServiceAssociationResponse saResponse;
        jsonparser parser=json.createparser(response.getbody());
        parser.nexttoken();
        while(parser.nexttoken()!=null){
            if(parser.getcurrenttoken()==jsontoken.start_object){
                saResponse=(ServiceAssociationResponse)parser.readvalueas(ServiceAssociationResponse.class);
                break;
            }
        }
        return saResponse;
    }
}