/*
 * NAAS_WS_CustomerAssetSummaryList
 *
 * REST API Service that returns summary of all assets associated with a provided Billing Account Number and Type.
 *
 * Revision History:
 * 2016/10/17 - Robert Cho - Initial creation based on v1.0 API specs, which use RCID as parameter for REST service.
 * 2017/02/22 - Jeffrey Ko - Modified Apex REST service to match the latest v1.3 API specs.  The revised service
 *                           will now take a list of parameters of Billing Account # and Billing Account Type (BAN/CAN).
 *                           The pattern will be 
 *                           https://instance.salesforce.com/services/apexrest/Customer/Assets/Summaries?account=101-123456789,102-1324567
 *                           The account (BCAN) value consists of the source ID and the Billing Account Number.
 * 
 */


@RestResource(urlMapping='/Customer/Assets/Summaries/*')

global with sharing class NAAS_WS_CustomerAssetSummaryList
{
    @HttpGet
    global static void getAssets()
    {              
        system.debug( '*** NAAS_WS_CustomerAssetSummaryList ... doGet() ... START' );
        
        //initialize constants        
        final String jsonAssetNameLabel = 'assetName';
        final String jsonBpiIdLabel = 'bpiIdentifier';
        final String jsonProductNameLabel = 'productName';
        final String jsonBillingAccountLabel = 'billingAccountNumber';
        final String jsonServiceAccountLabel = 'service';
        final String jsonProductCategoryLabel = 'productCategory';
        final String rcidText = 'RCID';
        final String logFailType = 'Failure';
        final String logSuccessType = 'Success';
        final String URL_MAPPING_PATH = '/Customer/Assets/Summaries/';
        
                        
        //initialize the product category mapping custom settings into a local map for performance reasons
        /*
        Map<String, String> productCategoryMap = new Map<String, String>();
        List<ProductCategoryMapping__c> productCategoryMappingList = ProductCategoryMapping__c.getall().values();
        for (ProductCategoryMapping__c currentProductCategoryMapping: productCategoryMappingList)
        {
            productCategoryMap.put(currentProductCategoryMapping.Product_External_ID__c, currentProductCategoryMapping.Product_Category__c);
        }*/
           
        //initialize JSON generator and start a block
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        
        //initialize the status variables to start with the OK status
        Integer statusCode = 200;
        String subCode;
        String statusTxt = 'OK';                 
        
        //initialize the request and response
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;           

        res.addHeader('Content-Type', 'application/json');

        //initialize functional variables
        //String rcidInputValue = '';
        String billingAcctNumberInputValue = '';
        String[] billingAcctNumberArray = null;
        
        //Account rcidAccount;
        List<Asset> customerAssets;    
        
        try
        {                                        
            
            //Get the parameters from the URI ... 
            
            system.debug( 'req.requestURI = ' + req.requestURI );
            system.debug( 'req.resourcePath = ' + req.resourcePath);
            system.debug( 'req.httpMethod = ' + req.httpMethod );

            billingAcctNumberInputValue = RestContext.request.params.get('account');
            system.debug( 'billingAcctNumberInputValue  = ' + billingAcctNumberInputValue  );

            if ( billingAcctNumberInputValue != null )
            {
                billingAcctNumberInputValue = billingAcctNumberInputValue.trim();
                billingAcctNumberArray = billingAcctNumberInputValue.split(',');
                
                system.debug( 'billingAcctNumberArray= ' + billingAcctNumberArray);
                
            }
            
            
            //rcidInputValue = req.params.get(rcidText);
            
            // Check if parameters exist (and not equal to empty string)
            if ( billingAcctNumberArray != null && billingAcctNumberArray.size() > 0 )
            {                                   
            
                // Before we get started, we need to make sure the input parameter matches ONLY
                // those defined in the custom metatype config mapping!!!!
                Map<String, String> billingSystemIdMap = getBillingSystemIdMap();
                if ( isBcanNumberInSystemId(billingSystemIdMap , billingAcctNumberArray  ) )
                {
            
                    // Billing Account Number is unique and can return 1 object only
                    String billAccountQuery = 'SELECT Id ' + 
                                              'FROM Account ' + 
                                              'WHERE BCAN__c IN :billingAcctNumberArray';
                    
                    system.debug( 'billAccountQuery (SOQL) = ' + billAccountQuery );
                                      
                    List<Account> billingAccounts = Database.query(billAccountQuery);
                    system.debug( 'billingAccounts(from Query) = ' + billingAccounts);
                    
                    
                                      
                    system.debug( 'billingAccounts.size() = ' + billingAccounts.size() );
                    
                    
                    // Found 1 Billing Account!
                    if (billingAccounts != null && billingAccounts.size() > 0)
                    {
                        system.debug( 'billingAccounts not null ... query for assets ');
                    
                        //retreive all assets associated with the rcid account
                        customerAssets = [SELECT Name, orderMgmt_BPI_Id__c, Product2.Name, 
                                                 vlocity_cmt__BillingAccountId__c,
                                                 vlocity_cmt__BillingAccountId__r.Name, 
                                                 vlocity_cmt__ServiceAccountID__r.Name, 
                                                 Product2.Catalog__c,
                                                 Product2.Catalog__r.Name,
                                                 Product2.ProductSpecification__r.orderMgmtId__c,
                                                 Account.BCAN__c
                                          FROM Asset 
                                          WHERE AccountId IN :billingAccounts];
                        
                        
                        system.debug( 'customerAssets = ' + customerAssets );
                        
                        //Start writing the JSON Asset list                                       
                        gen.writeFieldName('CustomerAssetSummaryList');
                        gen.writeStartArray();
                         
                        //Build the JSON for each asset. If any of the fields are missing, will omit the whole field:value combination
                        for (Asset currentAsset : customerAssets)
                        {                     
                            system.debug( 'currentAsset  = ' + currentAsset );
                           
                            gen.writeStartObject();
                                   
                            //assetName
                            if (currentAsset.Name != null)
                            {
                                system.debug( 'currentAsset.Name  = ' + currentAsset.Name );
                            
                                gen.writeStringField(jsonAssetNameLabel, currentAsset.Name);
                            }

                            // BIlling Account Number
                            if (currentAsset.Account.BCAN__c != null)
                            {
                                gen.writeStringField(jsonBillingAccountLabel, currentAsset.Account.BCAN__c.trim());
                            }

                            
                            //bpiIdentifer
                            if (currentAsset.orderMgmt_BPI_Id__c != null)
                            {
                                gen.writeNumberField(jsonBpiIdLabel, Integer.valueOf(currentAsset.orderMgmt_BPI_Id__c.trim()));
                            }
                            
                            //productName
                            if (currentAsset.Product2.Name != null)
                            {
                                gen.writeStringField(jsonProductNameLabel, currentAsset.Product2.Name);
                            }
                            
                            //billing
                            /*
                            if (currentAsset.vlocity_cmt__BillingAccountId__r.Name != null)
                                gen.writeStringField(jsonBillingAccountLabel, currentAsset.vlocity_cmt__BillingAccountId__r.Name);
                            */
                            
                            
                            //service
                            if (currentAsset.vlocity_cmt__ServiceAccountID__r.Name != null)
                            {
                                gen.writeStringField(jsonServiceAccountLabel, currentAsset.vlocity_cmt__ServiceAccountID__r.Name);    
                            }
                            
                            //productCategory
                            if (currentAsset.Product2.Catalog__c != null)
                            {
                                gen.writeStringField(jsonProductCategoryLabel, currentAsset.Product2.Catalog__r.Name);
                            }
                            
                            /*
                            
                            if (currentAsset.Product2.ProductSpecification__r.orderMgmtId__c != null)
                            {
                                //Use the Product Category Map containing the Product Category Mapping custom setting 
                                // to get the category based on the product specification and output to the JSON
                                String productCategory = productCategoryMap.get(currentAsset.Product2.ProductSpecification__r.orderMgmtId__c);                           
                                if (productCategory != null)
                                {
                                    gen.writeStringField(jsonProductCategoryLabel, productCategory);
                                }    
                                    
                            }*/
                            
                            
                            gen.writeEndObject();                                                    
                        }   //end for loop to cycle assets
                        gen.writeEndArray();
                    }  //end rcid null check
                    
                    
                    // if billingAccount is NULL
                    else
                    {
                        system.debug('ERROR STATUS CODE 400 ---> Cannot find Billing Account (billingAccount==null or EMPTY) ' );
                        statusCode = 400;
                        subCode = 'ANF';
                        statusTxt = 'Cannot find Billing account: ' + billingAcctNumberInputValue ; 
                        createWebserviceLog(logFailType, statusTxt, '', '' );                    
                    }
                 
                 
                 
                 } // END if ( isBcanNumberInSystemId(billingSystemIdMap , billingAcctNumberArray  )
                 else
                 {
                    system.debug('ERROR STATUS CODE 400 ---> Requested Billing Account Number contains an invalid System ID ' );          
                    //missing parameters
                    statusCode = 400;
                    subCode = 'ITP';
                    statusTxt = 'Requested Billing Account Number contains an invalid System ID - Only these Billing System IDs are allowed: ' + billingSystemIdMap ;                      
                 }
                 
                 
                 
            }   //END IF if ( billingAcctNumberArray != null && billingAcctNumberArray.size() > 0 )
            
            
            // ELSE if either billingAcctTypeInputValue OR billingAcctNumberInputValue is NULL
            else 
            {                     
                system.debug('ERROR STATUS CODE 400 ---> Request is missing input parameter ... ' );          
                //missing parameters
                statusCode = 400;
                subCode = 'MSP';
                statusTxt = 'Request is missing input parameter';                      
            }                        
        }   
        catch (Exception e)
        {
            system.debug('ERROR STATUS CODE 500 ---> General Error ...  ' + e.getMessage() );
        
            //catch any execution errors
            statusCode = 500;
            statusTxt = 'General Error: '+ e.getMessage(); 
            createWebServiceLog(logFailType, statusTxt, e.getStackTraceString(), '');
        }
        
        //build JSON for the status portion of the response        
        gen.writeFieldName('status');
        gen.writeStartObject();
        gen.writeNumberField('statusCd', statusCode);
        
        if (subCode != null)
        {       
            gen.writeStringField('SubCode', subCode);
        }
        
        if (statusTxt != null)
        {
            gen.writeStringField('statusTxt', statusTxt);
        }
        
        gen.writeEndObject();        

        //write output
        String outputResult = gen.getAsString();
               
        res.responseBody = Blob.ValueOf(outputResult);
                
        //Logging for success
        if (statusCode == 200)
        {         
            createWebserviceLog(logSuccessType, '', '', outputResult);  
        }
    }    
    
    
    /* 
     * Returns true if the billing account number contains a value that matches the Billing System ID
     */
    @TestVisible
    private static boolean isBcanNumberInSystemId(Map<String,String> systemIdMap, String[] bcanNumberArray)
    {
        boolean isBcanNumberInSystemId = false;
        system.debug('isBcanNumberInSystemId ... START ... ');
        system.debug('systemIdMap=' + systemIdMap);
        system.debug('bcanNumberArray=' + bcanNumberArray);
    
    
        for ( String bcanNumber : bcanNumberArray )
        {
            system.debug('bcanNumber =' + bcanNumber );
        
            // First, lets split the BCAN into 2 by "-".
            String[] tempArray = bcanNumber.split('-');
            
            String systemId = tempArray[0];
            
            system.debug('systemId =' + systemId );
            
            // See if this systemID exists in the map
            
            if ( systemId != null )
            {
                system.debug('systemId is NOT blank!' );
                system.debug('systemIdMap.get(' + systemId + ') =' + systemIdMap.get( systemId ));
            
                if ( systemIdMap.get( systemId ) != null )
                {
                    system.debug('SETTING the flag to true!!!!' );
                    isBcanNumberInSystemId = true;
                }
                else
                {
                    system.debug('One of the parameters contain a bad system ID!' );
                    isBcanNumberInSystemId = false;
                    break; // at least one bad one is found; break
                }
            }
            
        }
        
        
        system.debug('isBcanNumberInSystemId (RETURNING) =' + isBcanNumberInSystemId );
        
        return isBcanNumberInSystemId ;
    
    }
    
    
    
    /*
     * Gets the Custom Metatype data to determine Billing System ID's to query against
     */
    @TestVisible
    private static Map<String, String> getBillingSystemIdMap()
    {
        system.debug('getBillingSystemIdMap() ... START');        
    
        Map<String, String> tempMap = new Map<String, String>();
    
        NAAS_WS_CustomerAssetSummary_Config__mdt[] configMappings 
            = [SELECT MasterLabel, value__c 
               FROM NAAS_WS_CustomerAssetSummary_Config__mdt 
               WHERE MasterLabel = 'Billing_System_IDs' ];
            
        String tempString = null;   
        
        if ( configMappings != null && configMappings.size() == 1 )
        {
            for (NAAS_WS_CustomerAssetSummary_Config__mdt configMapping : configMappings) 
            {
                System.debug(configMapping.MasterLabel + ' : ' + configMapping.value__c );
                
                tempString = configMapping.value__c;
            }
            
            system.debug('getBillingSystemIdMap() ... tempString =' + tempString );        
            
            if ( tempString != null )
            {
                String[] tempStrArray = tempString.split(',');
                for ( String tempStr: tempStrArray )
                {
                    tempMap.put(tempStr, tempStr);
                }
            }
        }
        
        system.debug('getBillingSystemIdMap() ... tempMap =' + tempMap );        
        
        return tempMap;
    }



    
    /*
     * Creates a Web Service log
     */
    @TestVisible
    private static void createWebserviceLog(String resultType, String exceptionMsg, String stackTraceMsg, String payload)
    {
        //initialize Webservice Logging
        Webservice_Integration_Error_Log__c errorLog = new Webservice_Integration_Error_Log__c();
        errorLog.Apex_Class_and_Method__c = 'NAAS_WS_CustomeryAssetSummaryList.getAssets()';
        errorLog.Webservice_Name__c = 'CustomerAssetSummaryList';
        errorLog.External_System_Name__c = 'Telus.com';
        
        //Check lengths of inputs
        if (exceptionMsg.length() > 32768)
        {
            exceptionMsg = exceptionMsg.substring(0, 32767);
        }
        
        if (stackTraceMsg.length() > 32768)
        {
            stackTraceMsg = stackTraceMsg.substring(0, 32767);
        }
        
        if (payload.length() > 32768)
        {
            payload = payload.substring(0, 32767);
        }
        
        errorLog.Request_Payload__c = payload;
        
        // on fail
        if (resultType == 'Failure')
        {
            errorLog.Error_Code_and_Message__c = exceptionMsg;            
            errorLog.Stack_Trace__c = stackTraceMsg;
            errorLog.Status__c = 'Failed';
            errorLog.RecordTypeId = [Select Id, Name from RecordType where Name =: 'Failure' limit 1].Id;
        }        
        // on success
        else if (resultType == 'Success')
        {
            errorLog.Status__c = 'Passed';
            errorLog.RecordTypeId = [Select Id, Name from RecordType where Name =: 'Success' limit 1].Id;
        }
        
        insert errorLog;
    }
    
}