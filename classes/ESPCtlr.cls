/**
* @author Christian Wico - cwico@tractionondemand.com
* @description ESP.page controller extension
*/
public with sharing class ESPCtlr {

        public Account acc {get; set;}
        public ESP__c esp {get; set;}
        public List<Solution_Offering__c> solutions {get; private set;}
        public List<String> cultures {get; private set;}
        public List<String> lors {get; private set;}
        public List<String> coverages {get; private set;}
    public List<String> presences {get; private set;}
    

        public ESPCtlr() {
        init();
    }

        public ESPCtlr(ApexPages.StandardController stdController) {
        init();
    }

    public void init() {
        esp = [SELECT Id, Account__c FROM ESP__c WHERE Id =:ApexPages.currentPage().getParameters().get('id') LIMIT 1];

        if (acc == null) acc = [SELECT Id, Name FROM Account WHERE Id=:esp.Account__c LIMIT 1];

        cultures = new List<String>();
        lors = new List<String>();
        coverages = new List<String>();
        presences = new List<String>();

        /*

        // get picklist values for Culture__c field
        Schema.DescribeFieldResult fieldResult = Department__c.Culture__c.getDescribe();
        for (Schema.PicklistEntry f: fieldResult.getPicklistValues()) cultures.add(f.getValue());

        // get picklist values for Level_of_Relationship__c
        fieldResult = Department__c.Level_of_Relationship__c.getDescribe();
        for (Schema.PicklistEntry f: fieldResult.getPicklistValues()) lors.add(f.getValue());

        // get picklist values for Coverage_Strategy__c
        fieldResult = Department__c.Coverage_Strategy__c.getDescribe();
        for (Schema.PicklistEntry f: fieldResult.getPicklistValues()) coverages.add(f.getValue());
        */

        // get picklist values for Product Presence
        Schema.DescribeFieldResult fieldResult = Product_Presence__c.Product_Presence__c.getDescribe();
        
        for (Schema.PicklistEntry f: fieldResult.getPicklistValues()) presences.add(f.getValue());

        solutions = [SELECT Id, Name FROM Solution_Offering__c ORDER BY Id LIMIT 200];
    }

    public String getSolutionJSON() {return JSON.serialize(solutions);}

    public PageReference clearMap() {
        delete [SELECT Id FROM Account_Map_Data__c WHERE ESP__r.Account__c =:acc.Id LIMIT 2000];
        delete [SELECT Id FROM Product_Presence__c WHERE ESP__r.Account__c =:acc.Id LIMIT 2000];
        
        List<Department__c> ds = [SELECT Id FROM Department__c WHERE Account__c =:acc.Id LIMIT 200];
        for (Department__c d: ds) d.Enable_ESP__c = false;
        update ds;

        return null;
    }
    
}