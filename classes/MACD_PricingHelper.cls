/**
    Vlocity March 2017
*/
global with sharing class MACD_PricingHelper {
    

//#########################################
// MACD Disconnect Pricing  
//#########################################

    global static Map<Id,OrderItem>   DisconnectPricing(Map<Id, Sobject>  sobjectIdToSobject, List<Id> qualifiedObjectIds){
        
        Map <Id, OrderItem> mapToUpdates = new Map <Id, OrderItem> ();
        System.debug('MACD sobjectIdToSobject--'+sobjectIdToSobject);
        System.debug('qualifiedObjectIds--'+qualifiedObjectIds);

        for (orderItem oi: (List<OrderItem>)sobjectIdToSobject.values()){
            System.debug('OLI in for loop--'+oi);
            if(oi.vlocity_cmt__ProvisioningStatus__c == 'Disconnected'){
                  oi.vlocity_cmt__OneTimeTotal__c = 0;
                   oi.vlocity_cmt__RecurringTotal__c = 0;
                    oi.vlocity_cmt__RecurringCharge__c = 0;
                     oi.vlocity_cmt__EffectiveOneTimeTotal__c = 0;
                      oi.vlocity_cmt__EffectiveRecurringTotal__c = 0;
                mapToUpdates.put(oi.id, oi);
            }

        }

        return mapToUpdates;         
    }
}