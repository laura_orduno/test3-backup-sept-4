/*****************************************************
    Name: MaskNonReservableTelNumbeBatchJob
    Usage: This batch job will format the 'vlocity_cmt__FormatMask__c' in format '(999) 999 9999'
            TO RUN THIS batch job FROM ANONYMOUS MODE  paste this code :
            use below code to run next job
            MaskNonReservableTelNumbeBatchJob jobObject = new MaskNonReservableTelNumbeBatchJob(true);
            Id batchJobId = Database.executeBatch(jobObject,2000);
            
            To Restrict next job, use below code
            MaskNonReservableTelNumbeBatchJob jobObject = new MaskNonReservableTelNumbeBatchJob();
            Id batchJobId = Database.executeBatch(jobObject,2000);
    Author – Sandip Chaudhari (IBM)
    Revision History -
    Created By/Date - Sandip Chaudhari / 08 Dec 2016
    
    Updated By/Date/Reason for Change - 
    -> Defect # 10747 - 11 Apr 2017 - Sandip - Added code to update vlocity_cmt__CustomConfigUiTemplate__c = 'ocom-cpq-input-custom-template-limit'
	-> 1 Feb 2018 - Sandip as per BSBD - RTA - PI 3 / BSBDRP3-208
    
******************************************************/
public class MaskNonReservableTelNumbeBatchJob implements Database.Batchable<sObject>{
    public String query = 'SELECT Id, Name, vlocity_cmt__AttributeName__c, orderMgmtId__c, Reservable__c, vlocity_cmt__ObjectId__c, ' +
                        ' vlocity_cmt__FormatMask__c, vlocity_cmt__ValidValuesData__c, vlocity_cmt__CustomConfigUiTemplate__c ' + 
                        ' FROM vlocity_cmt__AttributeAssignment__c ' +
                        ' WHERE (Reservable__c <> \'Yes\' ' +
                        ' AND vlocity_cmt__AttributeName__c LIKE \'%Phone%\' )' +
                        ' OR (vlocity_cmt__AttributeName__c = \'City of use\') ' +
                        ' OR (vlocity_cmt__AttributeName__c = \'Toll-Free Terminating Number\') ' + 
                        ' OR (vlocity_cmt__AttributeName__c = \'Name to be Displayed\') ' +
    					' OR (vlocity_cmt__AttributeName__c = \'Previous Service Provider\') ' + 
                        ' OR (vlocity_cmt__AttributeName__c = \'Previous Account Number\') ' +
    					' OR (vlocity_cmt__AttributeName__c = \'Listing Name\') ';
    public String exceptionStr = '';
    public Boolean isError = false;
    public Boolean executeNextJob = false;
    // Constructor to get query from outside
    public MaskNonReservableTelNumbeBatchJob(String queryp){
        query = queryp;
    }
    
    public MaskNonReservableTelNumbeBatchJob(Boolean isExecuteNextJob){
        executeNextJob = isExecuteNextJob;
    }
    
    // Constructor to get query from outside and decide whether to execute next job or not
    public MaskNonReservableTelNumbeBatchJob(String queryp, Boolean isExecuteNextJob){
        query = queryp;
        executeNextJob = isExecuteNextJob;
    }
    // Constructor with default query
    public MaskNonReservableTelNumbeBatchJob(){
       
    }
    
    // Start the batch job
    public Database.QueryLocator start(Database.BatchableContext BC){
       // Access initialState here
       List<AtrributeTemplateMapping__mdt> attrTemplateMapping = [SELECT MasterLabel, Label, Template_Name__c FROM AtrributeTemplateMapping__mdt];
       String filterStr = ''; 
       if(attrTemplateMapping != null){
            for(AtrributeTemplateMapping__mdt attrMapObj : attrTemplateMapping){
                filterStr = filterStr + ' OR vlocity_cmt__AttributeName__c = \'' + attrMapObj.Label + '\'';
            }
           query = 'SELECT Id, Name, vlocity_cmt__AttributeName__c, orderMgmtId__c, Reservable__c, vlocity_cmt__ObjectId__c, ' +
                        ' vlocity_cmt__FormatMask__c, vlocity_cmt__ValidValuesData__c, vlocity_cmt__CustomConfigUiTemplate__c ' + 
                        ' FROM vlocity_cmt__AttributeAssignment__c ' +
                        ' WHERE (Reservable__c <> \'Yes\' ' +
                        ' AND vlocity_cmt__AttributeName__c LIKE \'%Phone%\' )' +
                        ' OR (vlocity_cmt__AttributeName__c = \'City of use\') ' +
                        ' OR (vlocity_cmt__AttributeName__c = \'Toll-Free Terminating Number\') ' + filterStr ;
       }
       system.debug('@@@ Query' + query);
       return Database.getQueryLocator(query);
    }
    
    // Execute the batch job
    public void execute(Database.BatchableContext BC, List<sObject> scope) {
        // Access initialState here
       Map<String, String> AttrMappingLabels = new Map<String, String>();
       List<AtrributeTemplateMapping__mdt> attrTemplateMapping = [SELECT MasterLabel, Label, Template_Name__c FROM AtrributeTemplateMapping__mdt];
       if(attrTemplateMapping != null){
            for(AtrributeTemplateMapping__mdt attrMapObj : attrTemplateMapping){
                AttrMappingLabels.put(attrMapObj.Label, attrMapObj.Template_Name__c);
            }
       }
        system.debug('@@@ AttrMappingLabels' + AttrMappingLabels);
        try{
            List<vlocity_cmt__AttributeAssignment__c> lstUpdate = new List<vlocity_cmt__AttributeAssignment__c>();
            Map<Id, Product2>  prodMap = new Map<ID, Product2>([SELECT Name FROM Product2 WHERE NAME = 'Name Display' Limit 1000]);
        
            // mask 'vlocity_cmt__FormatMask__c' in format '(999) 999 9999'
            for(Sobject sObj : scope){
                Boolean isRecModified = false;
                vlocity_cmt__AttributeAssignment__c objAtt = (vlocity_cmt__AttributeAssignment__c)sObj;
                if(String.isBlank(objAtt.vlocity_cmt__ValidValuesData__c) && objAtt.Reservable__c != 'Yes' 
                    && objAtt.vlocity_cmt__AttributeName__c != null && ((objAtt.vlocity_cmt__AttributeName__c).touppercase()).contains('PHONE')){
                    
                    objAtt.vlocity_cmt__FormatMask__c = '(999) 999 9999';
                    isRecModified = true;
                }
                if(objAtt.vlocity_cmt__AttributeName__c != null && (objAtt.vlocity_cmt__AttributeName__c).touppercase() == 'TOLL-FREE TERMINATING NUMBER'){
                    objAtt.vlocity_cmt__FormatMask__c = '(999) 999 9999';
                    isRecModified  = true;
                }
                
                if(objAtt.vlocity_cmt__AttributeName__c != null && (objAtt.vlocity_cmt__AttributeName__c).touppercase() == 'CITY OF USE'){
                    objAtt.vlocity_cmt__CustomConfigUiTemplate__c = 'ocom-cpq-input-custom-template-select';
                    isRecModified = true;
                }
                //Defect # 10747 - 11 Apr 2017 - Sandip - Added code to update vlocity_cmt__CustomConfigUiTemplate__c = 'ocom-cpq-input-custom-template-limit'
                if(objAtt.vlocity_cmt__AttributeName__c != null && (objAtt.vlocity_cmt__AttributeName__c).touppercase() == 'NAME TO BE DISPLAYED'){
                    if(prodMap != null){
                        if(prodMap.containsKey(objAtt.vlocity_cmt__ObjectId__c)){
                            objAtt.vlocity_cmt__CustomConfigUiTemplate__c = 'ocom-cpq-input-custom-template-limit';
                            isRecModified = true;
                        }
                    }
                }
                // end Defect # 10747
				
                //Added reservable falg == yes and inner conditions as per BSBD - RTA - PI 3 / BSBDRP3-208
                if(objAtt.Reservable__c == 'Yes'){
                    system.debug('@@@ AttrMappingLabels' + objAtt.vlocity_cmt__AttributeName__c);
                    system.debug('@@@ AttrMappingLabels' + AttrMappingLabels.get(objAtt.vlocity_cmt__AttributeName__c));
                    if(AttrMappingLabels.get(objAtt.vlocity_cmt__AttributeName__c) != null){
                        objAtt.vlocity_cmt__CustomConfigUiTemplate__c = AttrMappingLabels.get(objAtt.vlocity_cmt__AttributeName__c);
                        isRecModified = true;
                    }
                    /*if(objAtt.vlocity_cmt__AttributeName__c != null && (objAtt.vlocity_cmt__AttributeName__c).touppercase() == 'NAME TO BE DISPLAYED'){
                    	objAtt.vlocity_cmt__CustomConfigUiTemplate__c = 'ocom-cpq-input-custom-template-default';
                        isRecModified = true;
                    }
                    if(objAtt.vlocity_cmt__AttributeName__c != null && (objAtt.vlocity_cmt__AttributeName__c).touppercase() == 'PREVIOUS ACCOUNT NUMBER'){
                    	objAtt.vlocity_cmt__CustomConfigUiTemplate__c = 'ocom-cpq-input-custom-template-disable';
                        isRecModified = true;
                    }
                    if(objAtt.vlocity_cmt__AttributeName__c != null && (objAtt.vlocity_cmt__AttributeName__c).touppercase() == 'PREVIOUS SERVICE PROVIDER'){
                    	objAtt.vlocity_cmt__CustomConfigUiTemplate__c = 'ocom-cpq-input-custom-template-disable';
                        isRecModified = true;
                    } 
                    if(objAtt.vlocity_cmt__AttributeName__c != null && (objAtt.vlocity_cmt__AttributeName__c).touppercase() == 'LISTING NAME'){
                    	objAtt.vlocity_cmt__CustomConfigUiTemplate__c = 'ocom-cpq-input-custom-template-default';
                        isRecModified = true;
                    } */
                }
                
                if(isRecModified){
                    lstUpdate.add(objAtt);
                }
            }
            if(lstUpdate.size()>0){
                update lstUpdate;
            }
        }catch (Exception ex){
            isError = true;
            exceptionStr = ex.getMessage();
        }
    }
    
    public void finish(Database.BatchableContext BC) {
           
          // Initiate next batch job
            ExternalAuditLogHelper.ExternalAuditLogCreateParameter paramObj = new  ExternalAuditLogHelper.ExternalAuditLogCreateParameter();
            paramObj.emailAddress = '';
            paramObj.jobName = ExternalAuditLogHelper.OCOM_MASK_NONRERVABLE_TEL_NUM_BATCH_JOB;
            if(isError){
                paramObj.result = ExternalAuditLogHelper.OCOM_ETL_JOB_ERROR;
            }else{
                paramObj.result = ExternalAuditLogHelper.OCOM_ETL_JOB_SUCCESS;
            }
            paramObj.resultDescription = exceptionStr;
            paramObj.batchId = bc.getJobId();
            paramObj.emailSubject = System.Label.MaskNonReservableEmailSubject + ' ';
            paramObj.executeNextJob = executeNextJob;
            ExternalAuditLogHelper.createAuditLog (paramObj);
    }
}