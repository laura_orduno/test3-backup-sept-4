/**
 * Test class for S2C_ContractDetailsCtlr
 *
 * @author Thomas Tran, Merisha Shim, Traction on Demand
 * @date 03-15-2015
 */

@isTest(seeAllData=false)
private class S2C_ContractDetailsCtlrTest {
	
	@isTest static void getAllData() {
		System.runAs(new User(Id = UserInfo.getUserId())){
			insert S2C_TestUtility.createOrderType();
			Account newAccount = S2C_TestUtility.createAccount('Test Account');
			insert newAccount;

			Opportunity newOpportunity = S2C_TestUtility.createOpportunity(newAccount.Id, 'Test Opportunity');
			newOpportunity.CloseDate = Date.today();
			newOpportunity.StageName = S2C_TestUtility.ORIGINATED_STAGE_NAME;
			insert newOpportunity;

			Product2 newProduct2 = S2C_TestUtility.createProduct('Test Product', 'Test');
			insert newProduct2;

			Opp_Product_Item__c newOppProductItem = S2C_TestUtility.createOpportunityProductItem(newProduct2.Id, newOpportunity.Id);
			insert newOppProductItem;

			SRS_PODS_Product__c newSRSPodsProduct = S2C_TestUtility.createSRSPODSProduct(newProduct2.Product_Family__c);
			insert newSRSPodsProduct;

			//S2C_OrderCreation.OpportunityProductItemInfo newOpiItem = new S2C_OrderCreation.OpportunityProductItemInfo(newOppProductItem.Id, newOppProductItem.Name, 'Prequal', newSRSPodsProduct.Id, '12', Date.valueOf('2015-01-01'));
			S2C_OrderCreation.OpportunityProductItemInfo newOpiItem = new S2C_OrderCreation.OpportunityProductItemInfo(newOppProductItem.Id, newOppProductItem.Name, 'Prequal', 'Prequal', '12', Date.valueOf('2015-01-01'),'True');
			List<S2C_OrderCreation.OpportunityProductItemInfo> opiInfoList = new List<S2C_OrderCreation.OpportunityProductItemInfo>();
			opiInfoList.add(newOpiItem);

			String resultString = S2C_OrderCreation.createOrder(newOpportunity.Id, JSON.serialize(opiInfoList));

			Opportunity orderOpp = S2C_TestUtility.getOrderOpportunity(newOpportunity.Id);

			Contracts__c contract = new Contracts__c(Opportunity__c = orderOpp.Id,Type_of_Contract__c = 'New');

			insert contract;

			PageReference testPageRef = Page.S2C_ContractDetails;
			testPageRef.getParameters().put('orderOppId', orderOpp.Id);
			testPageRef.getParameters().put('salesOppId', orderOpp.Sales_Opportunity_Id__c);
			testPageRef.getParameters().put('contractId', contract.Id);
			Test.setCurrentPage(testPageRef);

			Test.startTest();

			S2C_ContractDetailsCtlr ctlr = new S2C_ContractDetailsCtlr();

			
			List<Service_Request__c> serviceRequestResults = ctlr.getServiceRequestInfo();
			List<Opp_Product_Item__c> oppProductItemResults = ctlr.getOpportunityProductItemInfo();
			List<Work_Request__c> workRequestResults = ctlr.getWorkRequestInfo();

			Test.stopTest();

			System.assertEquals(0, serviceRequestResults.size());
			System.assertEquals(1, oppProductItemResults.size());
			System.assertEquals(0, workRequestResults.size());
		}
	}

	
}