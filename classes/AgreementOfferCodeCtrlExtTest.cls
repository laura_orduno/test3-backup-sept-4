@isTest
public class AgreementOfferCodeCtrlExtTest {
    
    @isTest
    public static void testAgreementOfferEditandPropagate(){
        Test.startTest(); 
        Apttus__APTS_Agreement__c agreement = new Apttus__APTS_Agreement__c();
        insert agreement;
        Offer_House_Demand__c  ds = new Offer_House_Demand__c();
        ds.Agreement_Id_Apttus__c = agreement.id;
        map<string,schema.recordtypeinfo> recordTypeMap=offer_house_demand__c.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        ds.recordtypeid=recordTypeMap.get('WLN Deal Support').getrecordtypeid();
        ds.type_of_contract__c='CCA';
        ds.signature_required__c='Yes';
        insert ds;   
        Subsidiary_Account__c subAcc = new  Subsidiary_Account__c(agreement__c=agreement.id, deal_support__c=ds.id);
        insert subAcc; 
        ApexPages.currentPage().getParameters().put('retURL','/'+ds.id);  
        Agreement_Offer_Code__c offerCode = new  Agreement_Offer_Code__c(deal_support__c =ds.id);
        ApexPages.currentPage().getParameters().put('retURL','/'+ds.id);   
        ApexPages.StandardController sc = new ApexPages.standardController(offerCode);
        AgreementOfferCodeCtrlExt  ctrl = new AgreementOfferCodeCtrlExt(sc) ;
        ctrl.cancel(); 
        ctrl.save();
        ctrl.edit();
        ctrl.showPropergateList();  
        ctrl.propagate();
        ctrl.displayItems.get(0).selected = true;  
        ctrl.propagate();  
        ctrl.saveNew();
        boolean sp =  ctrl.showPropergate;
        Test.stopTest();   
    }
    
    @isTest
    public static void testAgreementOfferCodeEdit1(){
        Apttus__APTS_Agreement__c agreement = new Apttus__APTS_Agreement__c();
        insert agreement;
        Offer_House_Demand__c  ds = new Offer_House_Demand__c();
        ds.Agreement_Id_Apttus__c = agreement.id;
        map<string,schema.recordtypeinfo> recordTypeMap=offer_house_demand__c.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        ds.recordtypeid=recordTypeMap.get('WLN Deal Support').getrecordtypeid();
        ds.type_of_contract__c='CCA';
        ds.signature_required__c='Yes';
        insert ds;
        Subsidiary_Account__c subAcc = new  Subsidiary_Account__c(agreement__c=agreement.id, deal_support__c=ds.id);
        insert subAcc;   
        Agreement_Offer_Code__c offerCode = new  Agreement_Offer_Code__c(Subsidiary_Account__c =subAcc.id);
        insert offerCode; 
        ApexPages.currentPage().getParameters().put('retURL','/'+subAcc.id);  
        ApexPages.StandardController sc = new ApexPages.standardController(offerCode);
        AgreementOfferCodeCtrlExt  ctrl = new AgreementOfferCodeCtrlExt(sc);   
        ctrl.edit();
        ctrl.save();  
    }
    
    @isTest
    public static void testAgreementOfferCodeEdit2(){
        Apttus__APTS_Agreement__c agreement = new Apttus__APTS_Agreement__c();
        insert agreement;
        map<string,schema.recordtypeinfo> recordTypeMap=offer_house_demand__c.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        Offer_House_Demand__c  ds = new Offer_House_Demand__c();
        ds.Agreement_Id_Apttus__c = agreement.id;
        ds.recordtypeid=recordTypeMap.get('WLN Deal Support').getrecordtypeid();
        ds.type_of_contract__c='CCA';
        ds.signature_required__c='Yes';
        insert ds;   
        Agreement_Offer_Code__c offerCode = new  Agreement_Offer_Code__c(deal_support__c =ds.id, agreement__c=agreement.id);
        insert offerCode; 
        ApexPages.currentPage().getParameters().put('retURL','/'+agreement.id);  
        ApexPages.StandardController sc = new ApexPages.standardController(offerCode);
        AgreementOfferCodeCtrlExt  ctrl = new AgreementOfferCodeCtrlExt(sc);   
        ctrl.edit();
        ctrl.save();  
    }
    
    @isTest
    public static void testAgreementOfferCodeErrorLinkField(){
        Subsidiary_Account__c subAcc = new  Subsidiary_Account__c();
        insert subAcc; 
        ApexPages.currentPage().getParameters().put('retURL','/'+subAcc.id);  
        Agreement_Offer_Code__c offerCode = new  Agreement_Offer_Code__c();
        ApexPages.StandardController sc = new ApexPages.standardController(offerCode);
        AgreementOfferCodeCtrlExt ctrl = new AgreementOfferCodeCtrlExt(sc);
    }
}