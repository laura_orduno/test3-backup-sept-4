/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class smb_test_CloneClosedCaseExtension {

    static testMethod void myUnitTest() {


		   PageReference ref = new PageReference('/apex/smb_CloneClosedCase'); 
		   
		   RecordType recordType = [Select r.Id, r.DeveloperName From RecordType r where r.SobjectType = 'Account' and r.DeveloperName='RCID' limit 1];
		        
		   Account acc1 = new Account(Name='Testing Software', BillingCountry = 'US', BillingState = 'IL', recordTypeId=recordType.Id , No_Chronic_Incidents__c =10.0);
		   insert acc1;
		   
		   Case theCase = new Case();
           theCase.Account__c = acc1.id;
           theCase.Subject = 'TEST SUBJECT';
           theCase.Description = 'TEST';
		   theCase.Status = 'Closed';
		   theCase.Request_Type__c = 'Other';
		   theCase.Priority = 'Medium';
		   theCase.Root_Cause__c = 'Unknown';
		   //theCase.IsClosed = false;
		   insert theCase;
		   
		   
		   ApexPages.Standardcontroller std = new ApexPages.Standardcontroller(theCase);
		   
	       smb_CloneClosedCaseExtension smb = new smb_CloneClosedCaseExtension(std);	
	       
	       ref = smb.init();
	       ref = smb.returnToCase();
	       smb.throwExceptionMsg('message', 'warning');
	       smb.throwExceptionMsg('message', 'testing');	   
		   


    }
}