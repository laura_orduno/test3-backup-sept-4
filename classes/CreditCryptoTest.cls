@isTest
public class CreditCryptoTest
{
	@isTest
    public static void hash()
    {
		String driversLicenseNum = 'DL123456';
		String hash = CreditCrypto.hash(driversLicenseNum);

        System.assertEquals('ehdB4vhkbkTVRzFsMlIrdlj2cg1qeISPF7VcC5Ag7YW/wX2H3L47+ELbx72U+Y+L9DAV+2AsOUKhi2omWKhJ/w==', hash);
    }
}