/**    
    *trac_CaseFlowTest
    *@description Test class that tests the sending of a case through the process builder.
    *@authoer David Barrera
    *@Date 9/3/14
	 @Date modified on 08/12/2015 by kelly
    */

@isTest                      
private class trac_CaseFlowTest {
    @isTest
     /* November 14 2014 - commenting out test class to allow for successful deployment.  Flow must be in place in production and activated before test classes can be passed and run successfully.
      */
    static void flowSendCaseTest() {
       trac_CaseFlow plugin = new trac_CaseFlow();
        Map<String,Object> inputParams = new Map<String,Object>();

        List<Case> testCases = new List<Case>();        
        Case testCase1 = new Case(Subject = 'Test Subject 1', Priority = 'Medium');

        testCases.add(testCase1);       
        
        insert testCases;

        for(Case aCase: testCases){
            aCase.Vendor_Transfer_Type__c ='Warm Transfer';
            aCase.Type ='VoIP Assure';
            acase.Transfer_to_Queue__c = 'RingCentral Tier 2';
            aCase.BAN_Nbr__c='1234';
        }

        List<CaseComment> testCaseComments = new List<CaseComment>();
        CaseComment testCaseComment1 = new CaseComment(CommentBody = 'Test number 1 for sure ',
                                                        ParentId = testCase1.Id
        );
        
        testCaseComments.add(testCaseComment1);
        
        insert testCaseComments;

        List<Device__c> testDevices = new List<Device__c>();
        testDevices.add( new Device__C(Serial_Number__c='123',VoIP_Case__c = testCase1.Id));

        insert testDevices; 
        
        //added by kelly for adding case attachment testing
        List<Attachment> testAttachments = new List<Attachment>();
        testAttachments.add( new Attachment(
			parentId = String.valueOf(testCase1.Id),
            body = Blob.valueOf( 'this is an attachment test' ),
			name = 'fake attachment'
		));
		
		insert testAttachments;
        
        inputParams.put('caseId', testCases[0].Id );

        Process.PluginRequest request = new Process.PluginRequest(inputParams);           
        Test.startTest();
        plugin.invoke(request);
        Test.stopTest();
        List<PartnerNetworkRecordConnection> casePnrcList = getPNRCList(testCases);
        Map<Id, Id> caseIdToPNRC = new Map<Id, Id>(); 
        //System.Debug('This is the size of the pnrc: ' + casePnrcList.size());
        for (PartnerNetworkRecordConnection pnrc: casePnrcList){
            //System.Debug('Case ID of the local record in the pnrcList: ' + pnrc.LocalRecordId);   
            caseIdToPNRC.put(pnrc.LocalRecordId, pnrc.PartnerRecordId); 
        }

        //System.assert(caseIdToPNRC.containsKey(testCases.get(0).Id));
        plugin.describe();

    } 
    
    @isTest
    static void flowSendCaseTestOnlyDevice() {
      
        trac_CaseFlow plugin = new trac_CaseFlow();
        Map<String,Object> inputParams = new Map<String,Object>();

        List<Case> testCases = new List<Case>();        
        Case testCase1 = new Case(Subject = 'Test Subject 1', Priority = 'Medium');

        testCases.add(testCase1);       
        
        insert testCases;

        for(Case aCase: testCases){
            aCase.Vendor_Transfer_Type__c ='Warm Transfer';
            aCase.Type ='VoIP Assure';
            acase.Transfer_to_Queue__c = 'RingCentral Tier 2';
            aCase.BAN_Nbr__c='1234';
        }


        List<Device__c> testDevices = new List<Device__c>();
        testDevices.add( new Device__C(Serial_Number__c='123',VoIP_Case__c = testCase1.Id));

        insert testDevices;  
        
        //added attachment
        List<Attachment> testAttachments = new List<Attachment>();
        testAttachments.add( new Attachment(
			parentId = String.valueOf(testCase1.Id),
            body = Blob.valueOf( 'this is an attachment test' ),
			name = 'fake attachment'
		));
		
		insert testAttachments;
        
        
        inputParams.put('caseId', testCases[0].Id );

        Process.PluginRequest request = new Process.PluginRequest(inputParams);           
        Test.startTest();
        plugin.invoke(request);
        Test.stopTest();
        List<PartnerNetworkRecordConnection> casePnrcList = getPNRCList(testCases);
        Map<Id, Id> caseIdToPNRC = new Map<Id, Id>(); 
        //System.Debug('This is the size of the pnrc: ' + casePnrcList.size());
        for (PartnerNetworkRecordConnection pnrc: casePnrcList){
            //System.Debug('Case ID of the local record in the pnrcList: ' + pnrc.LocalRecordId);   
            caseIdToPNRC.put(pnrc.LocalRecordId, pnrc.PartnerRecordId); 
        }

        System.assert(caseIdToPNRC.containsKey(testCases.get(0).Id));       
    } 

    public static List<PartnerNetworkRecordConnection> getPNRCList(List<SObject> objList){
        List<PartnerNetworkRecordConnection> pnrcList = new List<PartnerNetworkRecordConnection>();
        Set<String> objIds = new Set<String>();
        System.Debug('---- This is the objList: ' + objList);
        for(SObject obj: objList){
            objIds.add(obj.Id);
        }
        System.Debug('---- This is the objList size: ' + objIds.size() );
        System.Debug('---- This is the objIds: ' + objIds );
        if(objIds.size() > 0){
            pnrcList =[ 
                Select id,status,LocalRecordId,StartDate,PartnerRecordId
                From PartnerNetworkRecordConnection
                Where LocalRecordId in :objIds
            ];      
        }   
        System.Debug('---this ist he pnrcList: ' + pnrcList);
        return pnrcList;
    }

    
}