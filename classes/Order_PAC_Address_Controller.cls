public without sharing class Order_PAC_Address_Controller {
public boolean showOrhide {get;set;}
public AddressData PAData  { get; set; }
public AddressData PAData1  { get; set; }
public AddressData BAData  { get; set; }
public ServiceAddressData SAData  { get; set; }
public SMBCare_Address__c smbCareAddr {get; set;} 
public AddressData cData{ get; set; }
ApexPages.StandardController stdCtrl; 
Public Order order;
Public Id Oid { get; set; }
Public Id iId { get; set; }
public string searchString{get;set;} // search keyword
public List<Account> results{get;set;} // search results
public Account account {get; set;}
public id currentId;
public String myString {get; set;}
public String selectedAccId{get;set;}
public String NPA_NXX {get;set;}
Public String coid {get;set;}
Account accou;
    
    public Integer testCount {get;set;}


  public Order_PAC_Address_Controller(ApexPages.StandardController controller){
      testCount = 3; 
      stdCtrl = controller;
       //Added to pass fields that were not present in VF...
       if(!Test.isRunningTest()) {
       controller.addFields(new List<String> { 'Service_Address__c','type','Update_Billing_Address__c', 'FMS_Address_ID__c', 'AccountId', 'ShippingStreet','ShippingCity','Service_Address__r.Street__c', 'Service_Address__r.City__c', 'Ban__c', 'Service_Address__r.NPA_NXX__c', 'Service_Address__r.coid__c'});
       }  
       order = (Order ) controller.getRecord();
       PAData  = new AddressData();
       PAData1  = new AddressData(); 
       BAData   = new AddressData();     
       showOrhide=false;
       //PAData.isCityRequired = true;
       NPA_NXX = Order.Service_Address__r.NPA_NXX__c;   
       coid = Order.Service_Address__r.coid__c;
      
       PAData.searchString= Order.Shipping_Address1__c;  
       PAData.onLoadSearchAddress= Order.Shipping_Address1__c;  
       //PAData.searchString= Order.ShippingStreet;
       //PAData1.isCityRequired = true;
       Oid =  ApexPages.currentPage().getParameters().get('id');
       List <OrderItem> item  = [SELECT Id FROM OrderItem where orderId =:Oid Limit 1];
           if(item !=null && item.size()>0)
               iId = item.get(0).id;
       system.debug('Order_PAC_Address_Controller Constructor Oid(' + Oid + ') iId (' + iId + ')');
       //SaveShipping();

       //Service Address: start
       SAData  = new ServiceAddressData();
       //SAData.isPostalCodeRequired = true;
       SAData.isSearchStringRequired = true;
       SAData.searchString = Order.Service_Address_Text__c;
       SAData.onLoadSearchAddress= Order.Service_Address_Text__c;
        
       BAData.searchString = Order.Update_Billing_Address__c;
       BAData.onLoadSearchAddress= Order.Update_Billing_Address__c; 
       BAData.isSearchStringRequired = true;
       BAData.isCityRequired = true; 
       BAData.isAddressLine1Required=true;
       BAData.isProvinceRequired= true;
       BAData.isPostalCodeRequired = true;
       smbCareAddr = new SMBCare_Address__c();
       smbCareAddr.Address_Type__c = 'Standard'; // Service Address Type defaults to Standard.
       smbCareAddr.Status__c='Valid';
       smbCareAddr.recordtypeid = Schema.SObjectType.SMBCare_Address__c.getRecordTypeInfosByName().get('Address').getRecordTypeId();
       //Service address: End
       
       results = performSearch('');
   }

    public Pagereference countUp()
    {
        if(testCount > 0)
            testCount = testCount+1;
        else
            testCount = 1;
        system.debug(' In countUp ###: '+testCount);
        return null;
    }
    public Pagereference SaveService()
    {
        
        OCOM_ServiceAddressControllerHelper svcAddrHelper = new OCOM_ServiceAddressControllerHelper();
        Order Odrupdate1 = svcAddrHelper.UpdateAddressAndOrder(Oid, smbCareAddr, SAData); 
        NPA_NXX = SAData.ratingNpaNxx;
        coid = SAData.coid;
        try{  
            update Odrupdate1;
            Pagereference pref= new pagereference('/'+Odrupdate1.id);                         
            return pref; 
        }
        catch(exception e)
        {
            return null;
        }        
    } 
    
   Public Pagereference SaveShipping()
    {  
       
    //String sAddr = ' '+(String)PAData.street+ ' ' +(String)PAData.city+ ' ' +(String)PAData.state+ ' '+(String)PAData.postalCode+' ' +(String)PAData.country;
    String sAddr = ' '+(String)PAData.addressLine1+ ' ' +(String)PAData.city+ ' ' +(String)PAData.province+ ' '+(String)PAData.postalCode+' ' +(String)PAData.country;
    Order osupdate=[select id, AccountId, Service_Address__c, Shipping_Address__c,ShippingStreet, ShippingCity,ShippingState,ShippingPostalCode,ShippingCountry from Order where id=:Oid];
    List <OrderItem> linesShip=[select id, OrderId, Shipping_Address__c from OrderItem where OrderId=:Oid];
    List <OrderItem> oiSUpt=New List<OrderItem>();
    osupdate.ShippingStreet=PAData.addressLine1;
    osupdate.ShippingCity=PAData.city;
    osupdate.ShippingState=PAData.provinceName;
    osupdate.ShippingPostalCode=PAData.postalCode;
    osupdate.ShippingCountry=PAData.country;
    osupdate.Shipping_same_as_Service__c=False;
    if(sAddr.length() > 8)
    osupdate.Shipping_Address__c=sAddr;
    for(OrderItem ois : linesShip) 
    { ois.Shipping_Address__c =sAddr;
      oiSUpt.add(ois);    
    }
    try{
    update osupdate;
    update oiSUpt;       
    Pagereference pref= new pagereference('/apex/Order_PAC_Address?id='+osupdate.id);
    pref.setRedirect(true);                            
    return pref; 
    }
    catch(exception e)
    {
        return null;
    } 
  } 
  
  // Start Added by Aditya Jamwal as per WP2 User story OCOM-498
     Public Pagereference UpdateBillingForMACD()
    {
    String sAddr = ' '+(String)BAData.addressLine1+ ' ' +(String)BAData.city+ ' ' +(String)BAData.province+ ' '+(String)BAData.postalCode+' ' +(String)BAData.country;
   
    if(sAddr.length() > 8 && null != Oid){
    
    Order osupdate= new Order(id=Oid,Update_Billing_Address__c=sAddr);
        try{
        update osupdate;   
        Pagereference pref= new pagereference('/apex/Order_PAC_Address?id='+osupdate.id);
        pref.setRedirect(true);                            
        return pref; 
        }
        catch(exception e)
        {
            return null;
        }
    }else{
    return null;
    }
  } 
  // End Added by Aditya Jamwal as per WP2 User story OCOM-498

// Not using this method anymore as we don't need to create a SMB Care address record..
    /* Public Pagereference SaveBilling()
    {
         //String bAddr = ' '+(String)PAData1.street+ ' ' +(String)PAData1.city+ ' ' +(String)PAData1.state+ ' '+(String)PAData1.postalCode+' ' +(String)PAData1.country;
         Order Odrupdate1=[select id,Service_Address__c,account.id, Status, CreatedBy.Name from Order where id=:Oid];
        List<SMBCare_Address__c> existingAddress=[select id from SMBCare_Address__c where  Account__c =:Odrupdate1.account.id limit 1];
         if(!(existingAddress.size()>0))
        {
           system.debug('ifcondtion');
            smbCareAddr.Postal_Code__c = PAData1.postalCode;
            smbCareAddr.Block__c= PAData1.buildingNumber;
            smbCareAddr.city__c= PAData1.city;
            smbCareAddr.Street__c= PAData1.street;
            smbCareAddr.Province__c=   PAData1.province;
            smbCareAddr.Country__c= PAData1.country;       
           
            smbCareAddr.recordtypeid = Schema.SObjectType.SMBCare_Address__c.getRecordTypeInfosByName().get('Billing').getRecordTypeId();  
                   
            smbCareAddr.Account__c =Odrupdate1.account.id;
            insert smbCareAddr; 
            
        }
        else
        {
         smbCareAddr = new SMBCare_Address__c();
         smbCareAddr.Account__c = Odrupdate1.account.id;        
         insert smbCareAddr;
         
        } 
        
        List<SMBCare_Address__c> insertedaccont=[select id,name from SMBCare_Address__c where  Account__c =:Odrupdate1.account.id ORDER BY Name DESC limit 1]; 
        String bAddr = ' '+(String)PAData1.addressLine1+ ' ' +(String)PAData1.city+ ' ' +(String)PAData1.province+ ' '+(String)PAData1.postalCode+' ' +(String)PAData1.country;
        Order obupdate=[select id, AccountId,name, BAN__c,Billing_Address__c, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry from Order where id=:Oid];
        List <OrderItem> linesBill=[Select Id, OrderId, Billing_Address__c from OrderItem where OrderId=:Oid Limit 30];
        List <OrderItem> oiBUpt=New List<OrderItem> ();
        obupdate.BillingStreet=PAData1.addressLine1;
        obupdate.BillingCity=PAData1.city;
        obupdate.BillingState=PAData1.province;
        obupdate.BillingPostalCode=PAData1.postalCode;
        obupdate.BillingCountry=PAData1.country;
        obupdate.BAN__c='Draft  ' + insertedaccont[0].name;    
        obupdate.Billing_Address__c=bAddr;
        
        for(OrderItem oib : linesBill) 
        { oib.Billing_Address__c=bAddr;
          oib.Billing_Account__c=obupdate.BAN__c;
          oib.vlocity_cmt__BillingAccountId__c=Odrupdate1.account.id;
          oiBUpt.add(oib);    
        }
        try{
            update obupdate;      
            update oiBUpt; 
            Pagereference Pr= new pagereference('/apex/Order_PAC_Address?id='+obupdate.id);
            Pr.setRedirect(true);                         
            return Pr; 
            }
        catch(exception e)
        {
            return null;
        } 
    }
    */
    // Method written to create Billing Account
    Public Pagereference SaveBillingAccount()
    {
         Set<Id> setBillingAccIds = new set<id>(); // Set of ServiceAccIds
         List<Account> ListBillingAcc= new List<Account>(); // List of Billing Account
         Id BillingRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Billing').getRecordTypeId();
         List <OrderItem> oiBUpt=New List<OrderItem> ();
         
         Order OderUpd= [select id, AccountId, Name, BAN__c, Billing_Address__c, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry from Order where id=:Oid];
         //String BillingAddr = ' '+(String)PAData1.street+ ' ' +(String)PAData1.city+ ' ' +(String)PAData1.state+ ' '+(String)PAData1.postalCode+' ' +(String)PAData1.country;
         String BillingAddr = ' '+(String)PAData1.addressLine1+ ' ' +(String)PAData1.city+ ' ' +(String)PAData1.province+ ' '+(String)PAData1.postalCode+' ' +(String)PAData1.country;
         
         ListBillingAcc = [select id from Account where RecordTypeId=:BillingRecordType and ParentId =:order.AccountId and BillingStreet =:PAData1.street and BillingCity =:PAData1.city and BillingState =:PAData1.state and BillingPostalCode =: PAData1.postalCode and BillingCountry =: PAData1.country];
         
         
             String CurrentCount;
             List<Account> AccLst = new List<Account>();
             AccLst = [select id, name from Account where RecordTypeId=:BillingRecordType order by createddate desc limit 1];
             if(AccLst .size() > 0 )
             {
             String AccName = AccLst [0].Name;
             string[] Splitted  = AccName.split('-');
             CurrentCount = '00000'+ String.valueof(Integer.valueof(Splitted[1]) + 1);
             }
             else CurrentCount = '000001';
             
            Account Acc = new account();
            //Commented by Arvind as part of defect id #54552
            //if(ListBillingAcc.size() == 0)
            //{
            Acc = new Account(ParentId = order.AccountId, RecordTypeId =BillingRecordType, Name='Draft-'+ CurrentCount, Ban__c='Draft-'+ CurrentCount, Ban_Can__c='Draft-'+ CurrentCount, BillingStreet =PAData1.street,  BillingCity =PAData1.city, BillingState =PAData1.state, BillingPostalCode=PAData1.postalCode, BillingCountry = PAData1.country);
            Insert Acc;
            OderUpd.BAN__c=Acc.name;
            //Commented by Arvind as part of defect id #54552
            /*}
            else
            {
            OderUpd.BAN__c=ListBillingAcc[0].name;
            }*/
            
         OderUpd.BillingStreet=PAData1.addressLine1;
         OderUpd.BillingCity=PAData1.city;
         OderUpd.BillingState=PAData1.province;
         OderUpd.BillingPostalCode=PAData1.postalCode;
         OderUpd.BillingCountry=PAData1.country;
         OderUpd.Billing_Address__c=BillingAddr;
              
        for(OrderItem oib :[Select Id, OrderId, Billing_Address__c,vlocity_cmt__BillingAccountId__c,Billing_Account__c from OrderItem where OrderId=:Oid]) 
        { 
          
         if(String.isBlank(oib.Billing_Account__c)){
          oib.vlocity_cmt__BillingAccountId__c=Acc.id;
          oib.Billing_Address__c=BillingAddr;
          oib.Billing_Account__c=OderUpd.BAN__c;
          }
          oiBUpt.add(oib); 
          system.debug('Inside save' +oib);   
        }
        
        try{
            update OderUpd;  
            if(oiBUpt.size() > 0)
            update oiBUpt; 
            Pagereference Pr= new pagereference('/apex/Order_PAC_Address?id='+OderUpd.id);
            Pr.setRedirect(true);                         
            return Pr; 
            }
        catch(exception e)
        {
            return null;
        } 

    }
   
    
    //Addition End
          
    
    // Method written by Arvind to update the Billing Address on Order/OrderLineItem when Agent selecting an existing Account during Billing Address update..
    Public Pagereference SaveBillingOnSelect()
    {
      
        List <OrderItem> OLineItemUpd=New List<OrderItem> ();
        Order OrderToUpdate;
       
        OrderToUpdate=[select id, AccountId, name, BAN__c,Billing_Address__c, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry from Order where id=:Oid];
        List<Account> accountList = [SELECT Id, Ban__c, Ban_Can__c, Billing_System__c, Billingstreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, Name FROM Account WHERE id=:selectedAccId];
        //OrderToUpdate.Billing_Address__c=accountList[0].Billingstreet+' '+accountList[0].BillingCity+' '+accountList[0].BillingState+' '+accountList[0].BillingPostalCode;
        String BillAddr='';
        if(accountList[0].Billingstreet != null)
        BillAddr=BillAddr+' '+accountList[0].Billingstreet;
        if(accountList[0].BillingCity != null)
        BillAddr=BillAddr+' '+accountList[0].BillingCity;
        if(accountList[0].BillingState != null)
        BillAddr=BillAddr+' '+accountList[0].BillingState;
        if(accountList[0].BillingPostalCode != null)
        BillAddr=BillAddr+' '+accountList[0].BillingPostalCode;
        if(accountList[0].BillingCountry!= null)
        BillAddr=BillAddr+' '+accountList[0].BillingCountry;
        
        OrderToUpdate.Billing_Address__c=BillAddr;
        OrderToUpdate.Ban__c=accountList[0].BAN_CAN__c;
        //OrderToUpdate.Ban__c=accountList[0].Name;

        for(OrderItem OlItem : [Select Id, OrderId, Billing_Address__c,vlocity_cmt__BillingAccountId__c,Billing_Account__c from OrderItem where OrderId=:Oid]){   
         if(String.isBlank(OlItem.Billing_Account__c)){
               OlItem.vlocity_cmt__BillingAccountId__c=accountList[0].id;
               OlItem.Billing_Address__c=BillAddr;
               OlItem.Billing_Account__c=OrderToUpdate.BAN__c;
               OLineItemUpd.add(OlItem);           
               system.debug('Inside onselect' +OlItem);
          }            
                
         }           

         try{
              System.debug('OrderToUpdate###'+OrderToUpdate);   
              update OrderToUpdate;
              if(OLineItemUpd.size()>0)              
               update OLineItemUpd;
              Pagereference Pref= new pagereference('/apex/Order_PAC_Address?id='+OrderToUpdate.id);
              Pref.setRedirect(true);                         
            return Pref; 
            }
            catch(exception e)
            {
                return null;
            } 
    
    }    
    
    // Method to update the BlnPopup field.
    
    public pagereference UpdateBlnPopup()
    {
        System.debug(' UpdateBlnPopup method called');
        Order OrderToUpdate;
        OrderToUpdate=[select id, Status, BlnPopup__c, Shipping_Address__c, Service_Address_Text__c, vlocity_cmt__QuoteId__c from Order where id=:Oid];
        List<Quote> Quot = [select id, ContactId, Service_Address_Text__c, Contact.Name from Quote where id=:OrderToUpdate.vlocity_cmt__QuoteId__c];
        
        if(OrderToUpdate.BlnPopup__c == True && null!= Quot && Quot.size()>0 && OrderToUpdate.Status!='Activated')
        {
        system.debug('OrderToUpdate.BlnPopup__c'+OrderToUpdate.BlnPopup__c);
        OCOM_ServiceAddressControllerHelper svcAddrHelper = new OCOM_ServiceAddressControllerHelper();
        svcAddrHelper.CreateServiceAccount(order, smbCareAddr, SAData); 
        OrderToUpdate.ShipToContactId=Quot[0].ContactId;
        OrderToUpdate.Shipping_Address_Contact__c=Quot[0].Contact.Name;
        //OrderToUpdate.Shipping_Address__c=Quot[0].Service_Address_Text__c;
        OrderToUpdate.BlnPopup__c=False;
        update OrderToUpdate;    
        }
        else if(OrderToUpdate.BlnPopup__c == True && OrderToUpdate.vlocity_cmt__QuoteId__c==null)
              {
                    OrderToUpdate.BlnPopup__c=False;
                    update OrderToUpdate;   
              }
     /*   try{
         system.debug('Order_PAC_Address_Controller action Start');
           initCache(Oid); 
        }catch(Exception ex){
           System.debug(ex) ;
        }*/
        
        return null;
    }
    

            
        
/*public void initCache(String orderId){
        System.debug(' initWidget method called');
        if(orderId!=null){
            List<Order> orderObjList = [select id, Service_Address__c,FMS_Address_ID__c, Service_Address__r.Street__c, 
                                        Service_Address__r.City__c, Service_Address__r.NPA_NXX__c, Service_Address__r.coid__c,
                                        Service_Address__r.FMS_Address_ID__c
                                        from Order where id =:orderId limit 1];
            //NPA_NXX = orderObj.Service_Address__r.NPA_NXX__c; 
            Order orderObj=null;
            if(orderObjList!=null && orderObjList.size()>0){
                orderObj=orderObjList.get(0);
            }
            if(orderObj!=null){
                coid = orderObj.Service_Address__r.coid__c;
                SMBCare_Address__c smbcareAddress = [Select id from SMBCare_Address__c where id=:orderObj.Service_Address__c];
              
                
                OrdrTnServiceAddress resultSAObj=OrdrTnReservationImpl.getNpaNxxListTnList(orderObj.id,null,smbcareAddress, UserInfo.getUserId(), OrdrTnReservationType.WLN,null,null );
               
               OrdrTnReservationImpl.saveNpaNxxLineListInCacheOnLoad(orderObj.id,null,smbcareAddress, String.valueOf(UserInfo.getUserId()),OrdrTnReservationType.WLN ,resultSAObj,null);                
           
               
            }
        }
}*/
        



    /*
    Public Pagereference RemoveBilling()
    {
        
        //String bAddr = ' '+(String)PAData1.street+ ' ' +(String)PAData1.city+ ' ' +(String)PAData1.state+ ' '+(String)PAData1.postalCode+' ' +(String)PAData1.country;
        String bAddr = ' '+(String)PAData1.addressLine1+ ' ' +(String)PAData1.city+ ' ' +(String)PAData1.province+ ' '+(String)PAData1.postalCode+' ' +(String)PAData1.country;
        Order obupdate=[select id, AccountId, Billing_Address__c, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry,Billing_Address2__c from Order where id=:Oid];
        List <OrderItem> linesBill=[Select Id, OrderId, Billing_Address__c,Billing_Address2__c from OrderItem where OrderId=:Oid Limit 30];
        List <OrderItem> oiBUpt=New List<OrderItem> ();
        obupdate.BillingStreet=PAData1.addressLine1;
        obupdate.BillingCity=PAData1.city;
        obupdate.BillingState=PAData1.province;
        obupdate.BillingPostalCode=PAData1.postalCode;
        obupdate.BillingCountry=PAData1.country;
        //oupdate.Shipping_same_as_Service__c=False;       
        obupdate.Billing_Address__c=bAddr;
        obupdate.Billing_Address2__c='';
        for(OrderItem oib : linesBill) 
        { oib.Billing_Address2__c='';
          oiBUpt.add(oib);    
        }
        try{
            update obupdate;  
            update oiBUpt; 
            Pagereference pref1= new pagereference('/apex/Order_PAC_Address?id='+obupdate.id);
            pref1.setRedirect(true);                         
            return pref1; 
            }
        catch(exception e)
        {
            return null;
        }
 } */
 
  // prepare the query and issue the search command
  private void runSearch() {
    // TODO prepare query string for complex serarches & prevent injections
    results= performSearch(searchString);
                 
  } 

  // run the search and return the records found. 
  private List<Account> performSearch(string searchString) {
  
    Id Billing = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Billing').getRecordTypeId();
    Id BAN = Schema.SObjectType.Account.getRecordTypeInfosByName().get('BAN').getRecordTypeId();
    Id CAN = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CAN').getRecordTypeId();
    Set<Id> AccRecordTypeId = new Set<ID>{Billing,BAN,CAN};

    Order Odrupdate2=[select id,name,account.id,account.name, Status from Order where id=:Oid limit 1];
    id aid=Odrupdate2.accountid;
      //query accounts by merging the variable name inside the query string
      List<Account> accountList = Database.query('SELECT Id,Account.Billing_System__c,Account.Billingstreet, Account.BillingCity, Account.BillingState, Account.BillingPostalCode, Account.BillingCountry, Account.name , Account.BAN_CAN__c FROM Account WHERE (Name Like : searchString OR Name Like \'%'+searchString +'%\') and RecordTypeId in:AccRecordTypeId  and parentid=:aid and (Billing_Account_Active_Indicator__c=\'Y\' or Billing_Account_Active_Indicator__c=\'\') order by CreatedDate desc limit 1000');        
  
    return accountList; 

  }
    // used by the visualforce page to send the link to the right dom element
  public string getFormTag() {
    return System.currentPageReference().getParameters().get('frm');
  }
  
    // used by the visualforce page to send the link to the right dom element for the text box
  public string getTextBox() {
    return System.currentPageReference().getParameters().get('txt');
  }
   // performs the keyword search
  public PageReference search() {
    runSearch();
    return null;
  }

/*
public class displayAccount {   
       public Account acct {get; set;}
        public List<SMBCare_Address__c> serviceAddresses {get;set;}
        public string parentRCID {get;set;}
        public string rcidAccountName {get;set;}        
        public string cbucidAccountName {get;set;}
        public string parentCBUCID {get;set;}
        public String businessName {get; set;}
        public String billingAddress {get;set;}
        public String serviceAddress {get;set;}
        public Id activityAccountId {get;set;}
        public Id activityCBUCIDAccountId {get;set;}        
        public String telephoneNumber {get;set;}
        public Boolean accountActive {get;set;}
        public Boolean accountAccess {get;set;}
        public String accountStatus {get;set;}   
        public String customRecordType {get;set;}      
        
        
        
        public displayAccount(Account account) {
            this.acct = account;

            this.accountAccess = true;
            this.businessName = account.Name;
            
            this.accountActive = (account.Billing_Account_Active_Indicator__c != 'N')&&(account.Inactive__c == false);
            if(this.accountActive == true){
                this.accountStatus = 'Active';
            } else {
                this.accountStatus = 'Inactive';
            }    
            this.telephoneNumber = account.BTN__c;
            
            if (account.Addresses1__r.size() > 0) {
                SMBCare_Address__c serviceAddress = account.Addresses1__r.get(0);
              
            }
        }
 }
 public List<account> getdisplayAccounts() {
        
        Order Odrupdate2=[select id,name,account.id,account.name, Status from Order where id=:Oid];        
        account acc=[select id,name from account where id=:Odrupdate2.account.id and account.name=:Odrupdate2.account.name];        
      // List<Account> ac=[select name,RCID__c,Billingstreet,Billing_System__c from account where  limit 10];  
      accou=[select Account.id,Account.parentid,Account.Billing_System__c,Account.Billingstreet,Account.name from account where parentid=:acc.id and Billing_Account_Active_Indicator__c='Y' limit 100];
       return accou;
        } */
    
   
  /*  private Map<Id, displayAccount> displayAccountsMappedToAccountId;
    
    public boolean displaySearchResults {get;set;}
    public Integer inactiveAccountCount {get;set;}
    public Integer rcidCount {get;set;}
    public Integer cbucidCount {get;set;}
    public Integer canCount {get;set;}
    public Integer banCount {get;set;}                
    public Integer restCount {get;set;}    
    

    public List<displayOpportunity> displayOpp {get; set;}

    public class displayOpportunity {
        public Opportunity Opp1{get; set;}
        public String OppName {get; set;}
        public String accountName {get; set;}
        public Id activityAccountId {get;set;}

     

        public displayOpportunity(Opportunity Opp1) {
            this.Opp1 = Opp1;
            this.OppName = Opp1.Name;

            if (Opp1.AccountId != null) {
                this.accountName = Opp1.Account.Name; 
            }
        }
    } */
 
 }