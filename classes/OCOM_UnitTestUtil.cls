/**
	
	Unit Test Util Class creating
	Asset, Order, Order Line Items 

    @version 04/12/2016
    @author  Vlocity

*/

public with sharing class OCOM_UnitTestUtil {


//	Create Account 
	
public static List<Account> getTestAccounts (String NameStartWith,Integer howMany, Id RecordTypeId){
	List<Account> lis = new List<Account>();
	for(Integer i=0; i< howMany; i++){
		lis.add(new Account(Name = NameStartWith+i, RecordTypeId = RecordTypeId));
	}
	insert lis; 
 	return lis; 
}

public static List<Contact> getTestContacts (String NameStartWith, Integer howMany, Id acctId){
	List<Contact> lis = new List<Contact>();
	for(Integer i=0; i< howMany; i++){
		lis.add(new Contact(LastName = NameStartWith+i, AccountId= acctId ));
	}
	insert lis; 
 	return lis; 
}

// Create Pricebook Entry

public static List<Product2>  lisP;
public static List<PricebookEntry>  lisCustPBE;
public static Pricebook2 custPB; 

public static void getTestPriceBookEntrys (String NameStartWith,Integer howMany ){
	
	//creae list of products
    lisP= new List<Product2>();
    lisP.add(new Product2(Name = NameStartWith , Family='Test Family Changed', Location_Dependency__c = 'None'));

	for(Integer i=1; i< howMany; i++){
		lisP.add(new Product2(Name = NameStartWith+i , Family='Test Family', Location_Dependency__c = 'Dependent'));
	}
    insert lisP; 
 	
 	Id pricebookId = Test.getStandardPricebookId();
    
    // create pricebook entry for standard pricebook
 	List<PricebookEntry>  lisStandardPBE= new List<PricebookEntry>();
       
	for(Integer i=0; i< howMany; i++){
		lisStandardPBE.add(new PricebookEntry(
        			Pricebook2Id = pricebookId, Product2Id = lisP[i].id,
            		UnitPrice = 100, IsActive = true));
	}
	insert lisStandardPBE;

	// create custom pricebook
	custPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
	insert custPB;

	// create pricebook entry for a custom pricebook
 	lisCustPBE= new List<PricebookEntry>();
       
	for(Integer i=0; i< howMany; i++){
		lisCustPBE.add(new PricebookEntry(
        			Pricebook2Id = custPB.Id, Product2Id = lisP[i].id,
            		UnitPrice = 200, IsActive = true));
	}
	insert lisCustPBE;
}

// Create Order 
public static Order testOrder; 
public static Smbcare_address__c testAddress;
public static void getTestOrder(Id acctId, Id serviceAcctId){
		testAddress= new smbcare_address__c( Unit_Number__c='1', Street_Address__c='444 Berry ST', City__c='San Francisco',State__c='CA', Postal_Code__c='94158',FMS_Address_ID__c='987651',account__c=serviceAcctId); 
        insert testAddress;
	    testOrder = new order(Ban__c='draft',accountId=acctId,effectivedate=system.today(),status='not submitted',Pricebook2Id=custPB.id,service_address__c=testAddress.id);   
        insert testOrder; 
}

// Create Order Products
public static List<OrderItem> testOrderProducts; 
public static void  getTestOrderProducts (Integer howMany, Id acctId, Id serviceAcctId){
	getTestPriceBookEntrys('TestProd', howMany);

	if (testOrder == null){
		getTestOrder(acctId, serviceAcctId);
	}
	
	testOrderProducts= new List<OrderItem> ();
	testOrderProducts.add(new OrderItem(orderId = testOrder.Id, pricebookentryId = lisCustPBE[0].Id, Quantity = 1,UnitPrice =400, Description = 'Test OLI', vlocity_cmt__ProvisioningStatus__c = 'New', vlocity_cmt__LineNumber__c = '0001'));	 
	insert testOrderProducts;
	for(Integer i=1; i< lisCustPBE.size(); i++){
		 testOrderProducts.add(new OrderItem(orderId = testOrder.Id, pricebookentryId = lisCustPBE[i].Id, Quantity = 1,UnitPrice =400, Description = 'Test OLI', vlocity_cmt__ProvisioningStatus__c = 'New', vlocity_cmt__LineNumber__c = '0001.000'+i, vlocity_cmt__RootItemId__c=testOrderProducts[0].Id));	 
	}
	upsert testOrderProducts;	
}

// Create Assets 
public static List<Asset> testAssets; 
public static void getTestAssets(Integer howMany, Id acctId, Id serviceAcctId){
		
		getTestOrderProducts (howMany, acctId, serviceAcctId);
		testAssets = new List<Asset>();
		
		for(Integer i=0; i< testOrderProducts.size(); i++){
			testAssets.add(new Asset(name= 'TestAssets'+i,accountId=acctId,vlocity_cmt__AssetReferenceId__c=String.valueOf(testOrderProducts[i].Id),vlocity_cmt__BillingAccountId__c=null,orderMgmtId__c=null,vlocity_cmt__OrderId__c=testOrder.Id,vlocity_cmt__OrderProductId__c=testOrderProducts[i].Id,vlocity_cmt__ServiceAccountId__c=serviceAcctId)); 
        }
        insert testAssets;
}


}