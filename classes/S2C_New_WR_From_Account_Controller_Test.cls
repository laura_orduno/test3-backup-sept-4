/* Created by Tech Mahindra Ltd. 

Test Class for S2C_New_WR_From_Account_Controller  */

@isTest
public class S2C_New_WR_From_Account_Controller_Test {
    
    @testsetup
    static void createRecords(){
        RecordType rt = [select Id from RecordType where Name = 'RCID' and SobjectType = 'Account' limit 1];        
        RecordType recordType = [select Id from RecordType where Name = 'Complex Provisioning Handoff' and SobjectType = 'Work_Request__c' limit 1];
        
        Account act =new Account();
        act.Name= 'Test Account';
        act.Phone='1234567890';        
        act.RecordTypeId = rt.Id;
        insert act;
                        
        Contact new_contact=new Contact();
        new_contact.AccountId =act.Id;
        new_contact.FirstName = 'Test Fname';
        new_contact.LastName='TestLName';
        new_contact.Email='TestEmail@Test.com';
        new_contact.Title='Test ContatcTitle';
        new_contact.Phone='44444444';
        new_contact.Language_Preference__c='English';
        insert new_contact;
        
        Work_Request__c workRequestObj = new Work_Request__c();
        workRequestObj.Description__c = 'Test Work Request';
        workRequestObj.Product_Category__c = 'Data'; 
        workRequestObj.Account__c = act.Id;
        insert workRequestObj; 
        
    }
    Static TestMethod Void createWorkRequestTestMethod(){
        
        Account acc = [Select Id,Name,ParentId from Account where Name =: 'Test Account'];
        Contact con = [Select id,Email from Contact Where email =: 'TestEmail@Test.com'];
       
        S2C_New_WR_From_Account_Controller wrFromAccountController = new S2C_New_WR_From_Account_Controller();
        wrFromAccountController.selectedContactId = con.Id;
        wrFromAccountController.searchTerm = '';
        wrFromAccountController.onSearch();
        wrFromAccountController.searchTerm = 'Test Fname';
        wrFromAccountController.oppName='test';
        
        ApexPages.currentPage().getParameters().put('aid', acc.Id);
        
        wrFromAccountController.onLoad();
        wrFromAccountController.onSearch();
        wrFromAccountController.onSelectContact();
        wrFromAccountController.getSelected();  
        wrFromAccountController.toggleNewContactForm();
        
        system.assert(wrFromAccountController.new_wr != null);
    }
    
     Static TestMethod Void createContactTestMethod(){
        Account acc = [Select Id,Name,ParentId from Account where Name =: 'Test Account'];
       
        Contact con=new Contact();
        con.AccountId =acc.Id;
        con.FirstName = 'Test Fname1';
        con.LastName='Test John';
        con.Email='TestEmail123@Test.com';
        con.Title='Test ContatcTitle1';
        con.Phone='44444444';
               
        
        S2C_New_WR_From_Account_Controller wrFromAccountController = new S2C_New_WR_From_Account_Controller();  
        wrFromAccountController.onLoad(); 
        wrFromAccountController.new_contact = con;
        wrFromAccountController.account = acc;
        wrFromAccountController.selectedContactId = NULL;
        wrFromAccountController.onSelectContact();
        
         
        ApexPages.StandardController sc = new ApexPages.StandardController(wrFromAccountController.new_contact);                 
        wrFromAccountController.createNewContact();
        
        List<Contact> conInsertList = [select id from Contact where LastName =: 'Test John'];
        system.assert(conInsertList.size() > 0);
          
    }
    
    }