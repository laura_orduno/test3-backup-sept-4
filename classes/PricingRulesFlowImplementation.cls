/***
* @group Implementations
*
* @description strongly typed interface: PricingInterface <br>
* <1>Intro and Purpose: Prices the line items in the object (Opty, Quote, Order) <br>
* <2>Triggering the Interface: Called as part of every CRUD operation on line items<br>
* <3>Other implementations: <br>
* - PricingRulesFlowImplementation<br>
**/
global with sharing class PricingRulesFlowImplementation implements vlocity_cmt.GlobalInterfaces.PricingInterface {
    global Flow.Interview.PricingRulesFlow myFlow {get; set;}

    
/**
* @param parent (SObject) : parent ie Order, Quote or Oppty
* @param itemList (List&SObject&gt) :  list of line items
* @return void
* @description Perform line item pricing based on flow
*/

    global void priceLineItems(SObject parent, List<SObject> itemList) {
        if (itemList == null || itemList.isEmpty()) {
            // nothing to price
            return;
        }
        
        vlocity_cmt.FlowStaticMap.flowMap.put('parent',parent);
        vlocity_cmt.FlowStaticMap.flowMap.put('itemList',itemList); 
        
        Map<String, Object> myMap = new Map<String, Object>();
        
        if(parent instanceof Opportunity)MyMap.put('parentType', 'Opportunity');
        if(parent instanceof Order)MyMap.put('parentType', 'Order');
        if(parent instanceof Quote)MyMap.put('parentType', 'Quote');

        myFlow = new Flow.Interview.PricingRulesFlow(myMap);
        
        try {
            myFlow.start(); 
        } catch (System.FlowException e){
            System.debug(LoggingLevel.ERROR, e.getMessage());
            System.debug(LoggingLevel.ERROR, e.getLineNumber());
            System.debug(LoggingLevel.ERROR, e.getStackTraceString());
        }
        
        
    }
}