@isTest
public class OrdrExceptionsTest {
    @isTest
    private static void testM(){
        Test.startTest();
        Exception ex0 =  new OrdrExceptions.QueryResourceException();
        Exception ex1 = new OrdrExceptions.BillingAccountCreationServiceException(); 
        Exception ex2 = new OrdrExceptions.ServicePathServiceException(); 
        Exception ex3 = new OrdrExceptions.RateGroupInfoServiceException(); 
        Exception ex4 = new OrdrExceptions.ProductFeasibilityException(); 
        Exception ex5 = new OrdrExceptions.ProductEligibilityException(); 
        Exception ex6 = new OrdrExceptions.TechnicalAvailabilityServiceException();   
        
        ex0.getMessage();
        ex1.getMessage(); 
        ex2.getMessage(); 
        ex3.getMessage();
        ex4.getMessage(); 
        ex5.getMessage();
        ex6.getMessage();
        Test.stopTest();
    }
    @isTest
    private static void wfmDataType(){
        WFMFWAssignmentSvcReqResV3.UpdateWorkOrderResponse ob1=new WFMFWAssignmentSvcReqResV3.UpdateWorkOrderResponse();
        WFMFWAssignmentSvcReqResV3.GetWorkOrder ob2=new WFMFWAssignmentSvcReqResV3.GetWorkOrder();
        WFMFWAssignmentSvcReqResV3.SearchWorkOrderList ob3=new WFMFWAssignmentSvcReqResV3.SearchWorkOrderList();
        WFMFWAssignmentSvcReqResV3.GetWorkOrderResponse ob4=new WFMFWAssignmentSvcReqResV3.GetWorkOrderResponse();
        WFMFWAssignmentSvcReqResV3.CancelWorkOrderResponse ob5=new WFMFWAssignmentSvcReqResV3.CancelWorkOrderResponse();
        WFMFWAssignmentSvcReqResV3.SearchWorkOrderListResponse ob6=new WFMFWAssignmentSvcReqResV3.SearchWorkOrderListResponse();
        WFMFWAssignmentSvcReqResV3.CancelWorkOrder ob7=new WFMFWAssignmentSvcReqResV3.CancelWorkOrder();
        WFMFWAssignmentSvcReqResV3.CreateWorkOrder ob8=new WFMFWAssignmentSvcReqResV3.CreateWorkOrder();
        WFMFWAssignmentSvcReqResV3.CreateWorkOrderResponse ob9=new WFMFWAssignmentSvcReqResV3.CreateWorkOrderResponse();
        WFMFWAssignmentSvcReqResV3.UpdateWorkOrder ob10=new WFMFWAssignmentSvcReqResV3.UpdateWorkOrder();
        WFMAssignmentExceptionV3.Message ob11=new WFMAssignmentExceptionV3.Message();
        WFMAssignmentExceptionV3.FaultExceptionDetailsType ob12=new WFMAssignmentExceptionV3.FaultExceptionDetailsType();
        WFMAssignmentExceptionV3.MessageList ob13=new WFMAssignmentExceptionV3.MessageList();
        
        WFMEnterpriseCommonTypesV3.OrganizationName ob31=new WFMEnterpriseCommonTypesV3.OrganizationName();
        WFMEnterpriseCommonTypesV3.Description ob32=new WFMEnterpriseCommonTypesV3.Description();
        WFMEnterpriseCommonTypesV3.Message ob33=new WFMEnterpriseCommonTypesV3.Message();
        WFMEnterpriseCommonTypesV3.HealthCard ob34=new WFMEnterpriseCommonTypesV3.HealthCard();
        WFMEnterpriseCommonTypesV3.PartyName ob35=new WFMEnterpriseCommonTypesV3.PartyName();
        WFMEnterpriseCommonTypesV3.Name ob36=new WFMEnterpriseCommonTypesV3.Name();
        WFMEnterpriseCommonTypesV3.MultilingualDescriptionList ob37=new WFMEnterpriseCommonTypesV3.MultilingualDescriptionList();
        WFMEnterpriseCommonTypesV3.IndividualName ob38=new WFMEnterpriseCommonTypesV3.IndividualName();
        WFMEnterpriseCommonTypesV3.MarketSegment ob39=new WFMEnterpriseCommonTypesV3.MarketSegment();
        WFMEnterpriseCommonTypesV3.Passport ob40=new WFMEnterpriseCommonTypesV3.Passport();
        WFMEnterpriseCommonTypesV3.DriversLicense ob41=new WFMEnterpriseCommonTypesV3.DriversLicense();
        WFMEnterpriseCommonTypesV3.CodeDescText ob42=new WFMEnterpriseCommonTypesV3.CodeDescText();
        WFMEnterpriseCommonTypesV3.MessageType ob43=new WFMEnterpriseCommonTypesV3.MessageType();
        WFMEnterpriseCommonTypesV3.AuditInfo ob44=new WFMEnterpriseCommonTypesV3.AuditInfo();
        WFMEnterpriseCommonTypesV3.UnknownName ob45=new WFMEnterpriseCommonTypesV3.UnknownName();
        WFMEnterpriseCommonTypesV3.Rate ob46=new WFMEnterpriseCommonTypesV3.Rate();
        WFMEnterpriseCommonTypesV3.Quantity ob47=new WFMEnterpriseCommonTypesV3.Quantity();
        WFMEnterpriseCommonTypesV3.BankAccount ob48=new WFMEnterpriseCommonTypesV3.BankAccount();
        WFMEnterpriseCommonTypesV3.Duration ob49=new WFMEnterpriseCommonTypesV3.Duration();
        WFMEnterpriseCommonTypesV3.ResponseMessage ob50=new WFMEnterpriseCommonTypesV3.ResponseMessage();
        WFMEnterpriseCommonTypesV3.MonetaryAmt ob51=new WFMEnterpriseCommonTypesV3.MonetaryAmt();
        WFMEnterpriseCommonTypesV3.MultilingualNameList ob52=new WFMEnterpriseCommonTypesV3.MultilingualNameList();
        WFMEnterpriseCommonTypesV3.CreditCard ob53=new WFMEnterpriseCommonTypesV3.CreditCard();
        WFMEnterpriseCommonTypesV3.MultilingualCodeDescTextList ob54=new WFMEnterpriseCommonTypesV3.MultilingualCodeDescTextList();
        WFMEnterpriseCommonTypesV3.brandType ob55=new WFMEnterpriseCommonTypesV3.brandType();
        WFMEnterpriseCommonTypesV3.ProvincialIdCard ob56=new WFMEnterpriseCommonTypesV3.ProvincialIdCard();
        WFMEnterpriseCommonTypesV3.ResponseStatus ob57=new WFMEnterpriseCommonTypesV3.ResponseStatus();
        WFMEnterpriseCommonTypesV3.MultilingualCodeDescriptionList ob58=new WFMEnterpriseCommonTypesV3.MultilingualCodeDescriptionList();
        
        WFMFWAppointmentSvcReqResV3.SearchAvailableAppointmentList ob581=new WFMFWAppointmentSvcReqResV3.SearchAvailableAppointmentList();
        WFMFWAppointmentSvcReqResV3.SearchAvailableAppointmentListResponse ob59=new WFMFWAppointmentSvcReqResV3.SearchAvailableAppointmentListResponse();
        
        WFMFWAppointmentSvcReqResV3Async.SearchAvailableAppointmentListResponseFuture ob60=new WFMFWAppointmentSvcReqResV3Async.SearchAvailableAppointmentListResponseFuture();
        
        WFMFWAppointmentV3WS.FieldWorkAppointmentServicePort ob61=new WFMFWAppointmentV3WS.FieldWorkAppointmentServicePort();
        WFMFWAppointmentV3WSAsync.FieldWorkAppointmentServicePortAsync ob62=new WFMFWAppointmentV3WSAsync.FieldWorkAppointmentServicePortAsync();
        
        WFMFWAssignmentSvcReqResV3Async.SearchWorkOrderListResponseFuture ob63=new WFMFWAssignmentSvcReqResV3Async.SearchWorkOrderListResponseFuture();
        WFMFWAssignmentSvcReqResV3Async.UpdateWorkOrderResponseFuture ob64=new WFMFWAssignmentSvcReqResV3Async.UpdateWorkOrderResponseFuture();
        WFMFWAssignmentSvcReqResV3Async.GetWorkOrderResponseFuture ob65=new WFMFWAssignmentSvcReqResV3Async.GetWorkOrderResponseFuture();
        WFMFWAssignmentSvcReqResV3Async.CancelWorkOrderResponseFuture ob66=new WFMFWAssignmentSvcReqResV3Async.CancelWorkOrderResponseFuture();
        WFMFWAssignmentSvcReqResV3Async.CreateWorkOrderResponseFuture ob661=new WFMFWAssignmentSvcReqResV3Async.CreateWorkOrderResponseFuture();
        
        WFMFWAssignmentV3WS.FieldWorkAssignmentMgmtServicePort ob67=new WFMFWAssignmentV3WS.FieldWorkAssignmentMgmtServicePort();
        
        WFMFWAssignmentV3WSAsync.FieldWorkAssignmentMgmtServicePortAsync ob68=new WFMFWAssignmentV3WSAsync.FieldWorkAssignmentMgmtServicePortAsync();
        
        WFMFWExceptionV3.Message ob69=new WFMFWExceptionV3.Message();
        WFMFWExceptionV3.FaultExceptionDetailsType ob70=new WFMFWExceptionV3.FaultExceptionDetailsType();
        WFMFWExceptionV3.MessageList ob71=new WFMFWExceptionV3.MessageList();
        
        WFMOrderTypesV3.TypedAddress ob72=new WFMOrderTypesV3.TypedAddress();
        
        WFMOrderTypesV3.TypedAddress ob101=new WFMOrderTypesV3.TypedAddress();
        WFMOrderTypesV3.FmsAddress ob102=new WFMOrderTypesV3.FmsAddress();
        WFMOrderTypesV3.ComponentList ob103=new WFMOrderTypesV3.ComponentList();
        WFMOrderTypesV3.JobIdent ob104=new WFMOrderTypesV3.JobIdent();
        WFMOrderTypesV3.WorkOrderUpdateNotification ob105=new WFMOrderTypesV3.WorkOrderUpdateNotification();
        WFMOrderTypesV3.CivicAddress ob106=new WFMOrderTypesV3.CivicAddress();
        WFMOrderTypesV3.WorkOrder ob107=new WFMOrderTypesV3.WorkOrder();
        WFMOrderTypesV3.RelationshipParameter ob108=new WFMOrderTypesV3.RelationshipParameter();
        WFMOrderTypesV3.TeamWorkerList ob109=new WFMOrderTypesV3.TeamWorkerList();
        WFMOrderTypesV3.RegionHierarchy ob110=new WFMOrderTypesV3.RegionHierarchy();
        WFMOrderTypesV3.LocationAddress ob111=new WFMOrderTypesV3.LocationAddress();
        WFMOrderTypesV3.JobList ob112=new WFMOrderTypesV3.JobList();
        WFMOrderTypesV3.InputHeader ob113=new WFMOrderTypesV3.InputHeader();
        WFMOrderTypesV3.Component ob114=new WFMOrderTypesV3.Component();
        WFMOrderTypesV3.Job ob115=new WFMOrderTypesV3.Job();
        WFMOrderTypesV3.RequiredSkillList ob116=new WFMOrderTypesV3.RequiredSkillList();
        WFMOrderTypesV3.WorkOrderIdent ob117=new WFMOrderTypesV3.WorkOrderIdent();
        WFMOrderTypesV3.LocationCodeList ob118=new WFMOrderTypesV3.LocationCodeList();
        WFMOrderTypesV3.TypeCode ob119=new WFMOrderTypesV3.TypeCode();
        WFMOrderTypesV3.TeamWorkerSkill ob120=new WFMOrderTypesV3.TeamWorkerSkill();
        WFMOrderTypesV3.LocationCode ob121=new WFMOrderTypesV3.LocationCode();
        WFMOrderTypesV3.GeographicCoordinateAddress ob122=new WFMOrderTypesV3.GeographicCoordinateAddress();
        WFMOrderTypesV3.WorkOrderRelationship ob123=new WFMOrderTypesV3.WorkOrderRelationship();
        WFMOrderTypesV3.RelationshipParameterList ob124=new WFMOrderTypesV3.RelationshipParameterList();
        WFMOrderTypesV3.SerialNumberList ob125=new WFMOrderTypesV3.SerialNumberList();
        WFMOrderTypesV3.TypedLocationAddress ob126=new WFMOrderTypesV3.TypedLocationAddress();
        WFMOrderTypesV3.Contact ob127=new WFMOrderTypesV3.Contact();
        WFMOrderTypesV3.ComponentLocation ob128=new WFMOrderTypesV3.ComponentLocation();
        WFMOrderTypesV3.CustomerPremiseEquipmentList ob129=new WFMOrderTypesV3.CustomerPremiseEquipmentList();
        WFMOrderTypesV3.AssignmentExecution ob130=new WFMOrderTypesV3.AssignmentExecution();
        WFMOrderTypesV3.AvailableAppointment ob131=new WFMOrderTypesV3.AvailableAppointment();
        WFMOrderTypesV3.EventList ob132=new WFMOrderTypesV3.EventList();
        WFMOrderTypesV3.BooleanValueReferenceType ob133=new WFMOrderTypesV3.BooleanValueReferenceType();
        WFMOrderTypesV3.JobAssignment ob134=new WFMOrderTypesV3.JobAssignment();
        WFMOrderTypesV3.CustomerPremiseEquipment ob135=new WFMOrderTypesV3.CustomerPremiseEquipment();
        WFMOrderTypesV3.TypeCodeList ob136=new WFMOrderTypesV3.TypeCodeList();
        WFMOrderTypesV3.ReferenceCode ob137=new WFMOrderTypesV3.ReferenceCode();
        WFMOrderTypesV3.WorkOrderRelationshipList ob138=new WFMOrderTypesV3.WorkOrderRelationshipList();
        WFMOrderTypesV3.WorkOrderMap ob139=new WFMOrderTypesV3.WorkOrderMap();
        WFMOrderTypesV3.Location ob140=new WFMOrderTypesV3.Location();
        
    }
}