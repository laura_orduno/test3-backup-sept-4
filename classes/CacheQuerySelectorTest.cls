/**
 * Created by dpeterson on 1/25/2018.
 */

@IsTest
public with sharing class CacheQuerySelectorTest {

    @isTest public static void getCaseStatusTest() {
        Set<String> setStatus = CacheQuerySelector.getClosedCaseStatuses();

        List<CaseStatus> listStatus = [Select MasterLabel From CaseStatus where IsClosed = true];

        for (CaseStatus s : listStatus) {
            System.assert(setStatus.contains(s.MasterLabel), 'Case Status should be contained');
        }

        Set<String> setStatus2 = CacheQuerySelector.getClosedCaseStatuses();
        for (CaseStatus s : listStatus) {
            System.assert(setStatus2.contains(s.MasterLabel), 'Case Status should be contained in a requery');
        }
    }
}