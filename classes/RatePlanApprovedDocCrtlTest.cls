@isTest
public class RatePlanApprovedDocCrtlTest {
  
    @isTest(seeallData=true)
    public static void testRatePlanApprovedDocCrtl(){
        Test.startTest(); 
        
        Offer_House_Demand__c testObj = new Offer_House_Demand__c();
        map<string,schema.recordtypeinfo> recordTypeMap=offer_house_demand__c.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        testObj.recordtypeid=recordTypeMap.get('WLN Deal Support').getrecordtypeid();
        testObj.type_of_contract__c='CCA';
        testObj.signature_required__c='Yes';
        insert testObj;
        createRatePlanObjects(testObj);
        ApexPages.StandardController sc = new ApexPages.StandardController(testObj);
        RatePlanApprovedDocCrtl ctrl = new RatePlanApprovedDocCrtl(sc);  
        System.assertNotEquals(null,ctrl.sectionOrder);
        System.debug(LoggingLevel.ERROR, 'ratePlansSubByType'+ctrl.ratePlansSubByType);
        //System.assertNotEquals(null,ctrl.ratePlansSubByType);
        //System.assertNotEquals(null,ctrl.fieldsSetNamesMap);
        Test.stopTest();
        
    }
    
    @isTest()
    public static void testRatePlanApprovedDocCrtlError(){
        Test.startTest(); 
        Offer_House_Demand__c testObj = new Offer_House_Demand__c();
        map<string,schema.recordtypeinfo> recordTypeMap=offer_house_demand__c.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        testObj.recordtypeid=recordTypeMap.get('WLN Deal Support').getrecordtypeid();
        testObj.type_of_contract__c='CCA';
        testObj.signature_required__c='Yes';
        insert testObj;
        createRatePlanObjects(testObj);
        ApexPages.StandardController sc = new ApexPages.StandardController(testObj);
        RatePlanApprovedDocCrtl ctrl = new RatePlanApprovedDocCrtl(sc);  
        Test.stopTest();
        
    }
    
    
    private static void createRatePlanObjects(Offer_House_Demand__c testObj ){
        List<Rate_Plan_Item__c> ratePlanItems = new LIst<Rate_Plan_Item__c>();
         List<Product2> prds = createProducts();
        for(Product2 p: prds){
            ratePlanItems.add(new Rate_Plan_Item__c(Product__c=p.id, Deal_Support__c=testObj.id));
        }
        insert ratePlanItems;
    }
    
    private static List<Product2> createProducts(){
        String[] productPlanTypes = new String[]{'Data Plan','Voice Plan','Data Plan'};
        List<Product2> toInsert = new List<Product2>();
        Integer i=100;
        for(String typ : productPlanTypes){
            toInsert.add(new Product2(Name='RP'+typ+i,RP_Plan_Type__c =typ,RP_SOC_Code__c='XCVVA'+typ+i));
            i++;
        }
        insert toInsert;
        return toInsert;
    }
    
    private static void createCustomConfigSettings(){
        List<DealSupportRatePlanGrid__c>  planFieldSetMapping = new List<DealSupportRatePlanGrid__c>();
        planFieldSetMapping.add(new DealSupportRatePlanGrid__c(Name='Data Features',Field_Set_Name__c='Grid_Data_Features',Type__c='Rate Plan Add On',Display_Order__c=1));
        planFieldSetMapping.add(new DealSupportRatePlanGrid__c(Name='Data Plan',Field_Set_Name__c='Grid_Data_Plan',Type__c='Rate Plan Add On',Display_Order__c=2));
        planFieldSetMapping.add(new DealSupportRatePlanGrid__c(Name='Roaming Add On',Field_Set_Name__c='Grid_Roaming_Add_On',Type__c='Rate Plan Add On	',Display_Order__c=3));
        planFieldSetMapping.add(new DealSupportRatePlanGrid__c(Name='Roaming Pay Per Use',Field_Set_Name__c='Grid_Roaming_Pay_Per_Use',Type__c='Rate Plan Add On',Display_Order__c=4));
        planFieldSetMapping.add(new DealSupportRatePlanGrid__c(Name='Text Features',Field_Set_Name__c='Grid_Text_Features',Type__c='Rate Plan',Display_Order__c=5));
        planFieldSetMapping.add(new DealSupportRatePlanGrid__c(Name='Voice and Data Plan',Field_Set_Name__c='Grid_Voice_and_Data_Plan',Type__c='Rate Plan',Display_Order__c=6));
        planFieldSetMapping.add(new DealSupportRatePlanGrid__c(Name='Voice Features',Field_Set_Name__c='Grid_Voice_Features',Type__c='Rate Plan',Display_Order__c=7));
        planFieldSetMapping.add(new DealSupportRatePlanGrid__c(Name='Voice Plan',Field_Set_Name__c='Grid_Voice_Features',Type__c='Rate Plan',Display_Order__c=8));
        insert planFieldSetMapping;
    }
}