@isTest(SeeAllData=false)
private class ENTP_SMPJITHandler_test {
	@isTest(SeeAllData=false) static void ENTP_SMP_JITHandler() {
         Account a = new Account(Name = 'Unit Test Account');
          a.Remedy_Business_Unit_Id__c = 'PCLOUD';
            a.Remedy_Company_Id__c = 'TCS';         
        a.strategic__c = false;
        insert a;

        Contact c = new Contact(Email = 'test-user-1@unit-test.com', LastName = 'Test', AccountId = a.Id);
        c.Remedy_PIN__c = '70208';
         insert c; 
          Profile p = [SELECT Id FROM Profile WHERE UserType='CSPLitePortal' LIMIT 1]; 
         User u = new User(Alias = 'TestUser', Email='test-user-1@unit-test.com', ContactId=c.Id, 
                            EmailEncodingKey='UTF-8', FirstName='Unit', LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', CommunityNickname='test-user-1UNTST', /*UserRoleId = portalRole.Id,*/ ProfileId=p.Id, 
                          	TimeZoneSidKey='America/Los_Angeles', 
                            UserName='test-user-1@unit-test.com');

        u.Customer_Portal_Role__c = 'private cloud';
        
        insert u;             
                
        System.runAs(u) {
            ENTP_SMP_JITHandler ctlr = new ENTP_SMP_JITHandler();
            ctlr.handleUser(true, u, new Map<String, String>{'Email' => 'test@test.com', 'UUID'=>'1234', 'Portal' => 'entp', 'FirstName'=>'Test', 'LastName'=>'Tester', 'Language'=>'en_US', 'Roles'=>'tps_self_serve'}, 'Testing', true);
            ctlr.updateUser(u.id, u.id, u.id, u.id, '', new Map<String, String>{'CPID' => '1234'}, '');
             ctlr.createUser(u.id, u.id, u.id,'Testing', new Map<String, String>{'Email' => 'test@test.com', 'UUID'=>'1234', 'Portal' => 'entp', 'FirstName'=>'Test', 'LastName'=>'Tester', 'Language'=>'en_US', 'Roles'=>'tps_self_serve'}, 'Test');
        }
    }
	
}