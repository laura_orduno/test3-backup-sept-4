global class OpportunityCallback {
    webservice static void onWorkOrderDueDateChange(id orderId){
        try{
            opportunity opp=[select id,stagename from opportunity where id=:orderId];
            list<Order_Request__c> lstOR = [Select Id, Order__c, NC_Order_ID__c,Operation_Failed_Status__c from Order_Request__c where NC_Order_ID__c<>null and Opportunity__c=:orderId and Order__c NOT IN ('Completed','Cancelled')];
            if (opp.stagename.equalsignorecase('Order Request New')||opp.stagename.equalsignorecase('Order Request On Hold (Customer)')||opp.stagename.equalsignorecase('Order Request On Hold (TELUS)')||opp.stagename.equalsignorecase('Order Request On Hold (Sales)')){
                if(lstOR.size()>0){
                    String isSuccess = SMB_WebserviceHelper.sendWebserviceRequest(SMBCare_WebServices__c.getValues('SMB_SuspendOrderEndpoint').value__c,Label.SMB_SuspendOrderXML, lstOR[0].NC_Order_ID__c,SMBCare_WebServices__c.getValues('SMB_SuspendOrderSoapAction').value__c,'Suspend Order');
                }
                smb_OrderSubmit_Callout_Helper.SubmitOrderRequest(orderId);
            }else if(opp.stagename.equalsignorecase('Order Request In Progress')||opp.stagename.equalsignorecase('Order Request Delivery')||opp.stagename.equalsignorecase('Order Request Non-Doable')||opp.stagename.equalsignorecase('Order Request Needs More Info')){
                id FOBOOppId=SMB_Helper.createFOBOOrderAndRelatedOLIForDueDateChange(orderId);
                smb_OrderSubmit_Callout_Helper.SubmitOrderRequest(FOBOOppId);
            }
        }catch(queryexception qe){
            // No action required currently if no Opportunity found.
        }
    }
}