@isTest(seeAllData=true)
private class SetLeadManagerBatchTest1 {
	static testmethod void testBatch(){
   		Set<Id> userIds = new Set<Id> {Userinfo.getUserId()};
   		Lead l = new Lead(phone = '2342341233', company = 'test company', lastname = 'leadlastname', ownerid = Userinfo.getUserId());
		insert l;
   		Test.startTest();
   		SetLeadManagerBatch obj = new SetLeadManagerBatch(); 
		obj.query = 'select id, manager_1__c, manager_2__c, manager_3__c, manager_4__c, ownerid from Lead where isConverted = false and ownerid in '+ RoleHierarchy.buildInExpression(userIds);
        Database.executeBatch(obj);
        Test.stopTest();
   }
}