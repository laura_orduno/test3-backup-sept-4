public without sharing class QuoteSendAgreementController {
    // New in R5.1:
    public class AgreementException extends Exception {}
    
    public List<ContactVO> contacts {get; private set;}
    public Id selectedContactId {get; set;}
    public Boolean sent {get; private set;}
    public String contactEmail {get; set;}
    public String agreementType {get; set;}
    public Boolean eeUser {get{return QuotePortalUtils.isExperienceExcellenceUser();}}
    public Boolean recomboDisabled {get; private set;}
    
    private String returnURL;
    private String quoteIds;
    private String templateId;
    private String groupId;
    
    public QuoteSendAgreementController() {
        agreementType = 'recombo';
        
        System.assert(Quote_Portal__c.getInstance() != null, 'Quote Portal settings are missing');
        
        returnURL = ApexPages.currentPage().getParameters().get('retURL');
        quoteIds = ApexPages.currentPage().getParameters().get('qids');
        groupId = ApexPages.currentPage().getParameters().get('gid');
        templateId = ApexPages.currentPage().getParameters().get('tid');
        if (templateId == null) {
            templateId = Quote_Portal__c.getInstance().Quote_Template_Id__c;
        }
        Id accountId = ApexPages.currentPage().getParameters().get('aid');
        
        this.contacts = new List<ContactVO>();
        for (Contact contact :
            [SELECT Id, FirstName, LastName, Title, Role_c__c, Phone, Email FROM Contact
             WHERE Web_Account__c = :accountId]) {
            contacts.add(new ContactVO(contact));
        }
        
        Id createdById;
        if (groupId != null) {
            Quote_Group__c grp = [SELECT CreatedBy.ContactId FROM Quote_Group__c WHERE Id = :groupId];
            createdById = grp.CreatedBy.ContactId;
        } else {
            SBQQ__Quote__c quote = [SELECT CreatedBy.ContactId FROM SBQQ__Quote__c WHERE Id = :quoteIds];
            createdById = quote.CreatedBy.ContactId;
        }
        
        if (eeUser) {
            contacts.add(new ContactVO([SELECT Id, FirstName, LastName, Title, Role_c__c, Phone, Email FROM Contact WHERE Id = :createdById]));
        }
        
        sent = false;
    }
    
    private void addErrorMessage(String message) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, message));
    }
    
    public PageReference onSend() {
        Savepoint sp = null;
        try {
            Contact c = new Contact(Email=contactEmail);
            if (StringUtils.isBlank(selectedContactId)) {
                return null;
            }
            c = [SELECT Email, FirstName, LastName FROM Contact WHERE Id = :selectedContactId];
            contactEmail = c.Email;
            
            if (quoteIds == null) { throw new AgreementException('Missing target quote IDs'); }
            
            if (agreementType == 'recombo') {
                if (c.Id == null) { addErrorMessage('Contact selection is required to generate agreement'); return null;
                }
                if (c.Email == null) { addErrorMessage('Contact email is required to generate agreement.'); return null;
                }
                if (c.FirstName == null) { addErrorMessage('Contact first name is required to generate agreement'); return null;
                }
                if (c.LastName == null) { addErrorMessage('Contact last name is required to generate agreement'); return null;
                }
            }
            
            String prettyType = '';
            if (agreementType == 'recombo') {
                prettyType = 'Recombo';
            } else {
                prettyType = 'Wet Ink';
            }
            Agreement__c agreementRecord = new Agreement__c(Type__c = prettyType);
            agreementRecord.Recipient__c = c.Id;
            
            Document doc = generateDocument();
            
            String waybillId = null;
            
            Boolean sendEmail = false;
            if (agreementType == 'recombo') {
                Quote_Portal__c qp = Quote_Portal__c.getInstance();
                RecomboClient client = new RecomboClient(qp.Recombo_Endpoint__c, qp.Recombo_Username__c, qp.Recombo_Password__c);
                String docId = (groupId != null) ? groupId : quoteIds;
                client.document = new Recombo.Document(docId,doc.Name,doc.ContentType,doc.Body);
                client.emailInfo = new Recombo.EmailInfo();
                client.persons.add(new Recombo.Person(c.Email,c.FirstName,c.LastName));     
                client.sendDocument();
                waybillId = client.waybillId;
                
                agreementRecord.Waybill_ID__c = client.waybillId;
                agreementRecord.URL__c = client.documentURL;
            } else {
                sendEmail = true;
            }
            
            agreementRecord.Sent_Date__c = DateTime.now();
            agreementRecord.Status__c = QuoteAgreementStatus.PENDING;
            if (groupId != null) {
                agreementRecord.Quote_Group__c = groupId;
            } else {
                agreementRecord.Quote__c = quoteIds;
            }
            
            Boolean expirationResult = (groupId == null) ? QuoteAgreementUtilities.expireOldQuoteAgreements(quoteIds, QuoteAgreementStatus.REPLACED, waybillId) : QuoteAgreementUtilities.expireOldQuoteGroupAgreements(groupId, QuoteAgreementStatus.REPLACED, waybillId);
            
            // Stupid restrictions!
            sp = Database.setSavepoint();
            
            if (sendEmail) {
                QuoteDocumentEmailer emailer = new QuoteDocumentEmailer(doc.body);
                emailer.email('BA Quote', new Set<Id>{(Id)selectedContactId});
            }
            
            insert agreementRecord;
            
            // New in R5.1:
            // If this is a printed or "wet ink" agreement, store the PDF body as an attachment to the agreement record for future use.
            if (agreementType == 'wetInk') {
                Attachment a = new Attachment(Name = doc.Name, Body = doc.Body, ContentType = doc.ContentType, ParentId = agreementRecord.Id);
                insert a;
                agreementRecord.Attachment_ID__c = a.Id;
                update agreementRecord;
            }
            
            List<SBQQ__Quote__c> batchn = new List<SBQQ__Quote__c>();
            for (String id : quoteIds.split(',')) {
                SBQQ__Quote__c quote = new SBQQ__Quote__c(Id = id, SBQQ__PrimaryContact__c = c.Id);
                quote.SBQQ__PrimaryContact__c = c.Id;
                quote.SBQQ__Status__c = QuoteStatus.SENT_AGREEMENT;
                quote.Provisioning_Requested__c = false;
                quote.Agreement_Accepted__c = false;
                batchn.add(quote);
            }
            update batchn;
            
            if (groupId != null) {
                Quote_Group__c grpn = new Quote_Group__c(Id = groupId, Customer_Contact__c = c.Id);
                grpn.Customer_Contact__c = c.Id;
                grpn.Status__c = QuoteStatus.SENT_AGREEMENT;
                update grpn;
            }
            
            sent = true; } catch (Exception e) { if (sp != null) {Database.rollback(sp);} QuoteUtilities.log(e); }
            return null;
    }
    
    private Document generateDocument() {
        SBQQ__QuoteTemplate__c template = [SELECT SBQQ__LogoDocumentId__c FROM SBQQ__QuoteTemplate__c WHERE Id = :templateId];
        QuoteDocumentGenerator generator = new QuoteDocumentGenerator(template);
        generator.recombo = (agreementType == 'recombo');
        generator.signatureBlock = (agreementType == 'wetInk');
        generator.contactEmail = contactEmail;
        
        Document doc = new Document();
        doc.Name = 'BA_Quote.pdf';
        doc.ContentType = 'application/pdf';
        doc.body = Test.isRunningTest() ? Blob.valueOf('test') : generator.generate(quoteIds);
        
        return doc;
    }
    
    public PageReference onContinue() {
        PageReference pref = new PageReference(returnURL);
        pref.setRedirect(true);
        return pref;
    }
    
}