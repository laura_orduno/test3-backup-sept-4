public with sharing class AjaxResController{
    
    public JsonObject json {get;set;} 
    public void doEmployeeSearch(){
        Map<string,string> params = ApexPages.currentPage().getParameters();
        List<Employee__c> listUser= new List<Employee__c>();
        listUser= getEmployeeRecords(params.get('q')); 
        json = new JsonObject();
        List<JSONObject.value> values = new List<JSONObject.value>();
                for (Employee__c a: listUser){                   
                    JSONObject cjson = new JSONObject();                    
                    cjson.putOpt('Name', new JSONObject.value(a.Full_Name__c));
                    cjson.putOpt('Email', new JSONObject.value(a.Email__c));
                    cjson.putOpt('Phone', new JSONObject.value(a.Phone__c));
                    cjson.putOpt('KanaEmail', new JSONObject.value(a.External_Email__c));
                    values.add(new JSONObject.value(cjson));
                    json.putOpt('Users', new JSONObject.value(values));
                }
    }
    public void doUserSearch(){
        Map<string,string> params = ApexPages.currentPage().getParameters();
        List<User> listUser= new List<User>();
        listUser= getUserRecords(params.get('q')); 
        json = new JsonObject();
        List<JSONObject.value> values = new List<JSONObject.value>();
                for (User u: listUser){                   
                    JSONObject cjson = new JSONObject();                    
                    cjson.putOpt('Name', new JSONObject.value(u.firstname+' '+u.lastname));
                    if(u.EmployeeId__c != null && (((u.EmployeeId__c).equals('00999990')) || ((u.EmployeeId__c).equals('00999991')) || ((u.EmployeeId__c).equals('00999992')) || ((u.EmployeeId__c).equals('00999993')) || ((u.EmployeeId__c).equals('00999994')) || ((u.EmployeeId__c).equals('00999995')) || ((u.EmployeeId__c).equals('00999996')) || ((u.EmployeeId__c).equals('00999997')) || ((u.EmployeeId__c).equals('00999998')) || ((u.EmployeeId__c).equals('00999999'))))
                        cjson.putOpt('Email', new JSONObject.value(''));
                    else
                         cjson.putOpt('Email', new JSONObject.value(u.Email));
                    cjson.putOpt('Phone', new JSONObject.value(u.Phone));
                    values.add(new JSONObject.value(cjson));
                    json.putOpt('Users', new JSONObject.value(values));
                }
    }
    //Show Popup Viewed on Login
    public void doShowPopupView(){
        String userId = UserInfo.getUserId();
        String userName = UserInfo.getUserName();
        Map<string,string> params = ApexPages.currentPage().getParameters();
        List<User> listUser= new List<User>();
        System.debug('#######userID='+userId + ' ##userName='+ userName  );
        listUser= [Select Id, UserName, Pop_Up_Viewed__c From User where id=: userId limit 1];
        json = new JsonObject();
        List<JSONObject.value> values= new List<JSONObject.value>();
                for (User a: listUser){                   
                    JSONObject cjson = new JSONObject();                  
                    cjson.putOpt('PopupView', new JSONObject.value(a.Pop_Up_Viewed__c));   
                    cjson.putOpt('ID', new JSONObject.value(a.id));   
                    cjson.putOpt('UserName', new JSONObject.value(a.UserName));                    
                    values.add(new JSONObject.value(cjson));
                    json.putOpt('Users', new JSONObject.value(values));
                }

    }
    //Update Popup Viewed after show the Popup
    public void doUpdatePopupView(){
        String userId = UserInfo.getUserId();
        String userName = UserInfo.getUserName();       
        System.debug('#######userID='+userId + ' ##userName='+ userName  );
        User activeUser= [Select Pop_Up_Viewed__c From User where id=: userId limit 1];
        activeUser.Pop_Up_Viewed__c=true;
        try{
            update activeUser;
        }catch(Exception e){}
        List<User> listUser= new List<User>();
        System.debug('#######userID='+userId + ' ##userName='+ userName  );
        listUser= [Select Id, UserName, Pop_Up_Viewed__c From User where id=: userId limit 1];
        json = new JsonObject();
        List<JSONObject.value> values= new List<JSONObject.value>();
                for (User a: listUser){                   
                    JSONObject cjson = new JSONObject();                  
                    cjson.putOpt('PopupView', new JSONObject.value(a.Pop_Up_Viewed__c));   
                    cjson.putOpt('ID', new JSONObject.value(a.id));   
                    cjson.putOpt('UserName', new JSONObject.value(a.UserName));                    
                    values.add(new JSONObject.value(cjson));
                    json.putOpt('Users', new JSONObject.value(values));
                }
    }
    // Does the SOQL query, using Ajax request data
    public List<Employee__c> getEmployeeRecords(String userName) {
         List<Employee__c> records = new List<Employee__c>();
         if (userName!= null && userName.trim().length() > 0){
             // Protect from SOQL injection 
              records = [select Full_Name__c,  Email__c, Phone__c, External_Email__c  from  Employee__c where id =:userName];
               
          }  
          return records;     
   }
   public List<User> getUserRecords(String userName) {
         List<User> records = new List<User>();
         if (userName!= null && userName.trim().length() > 0){
             // Protect from SOQL injection 
              records = [select FirstName, LastName,  Email, Phone, EmployeeId__c from  User where id =:userName ];               
          }  
          return records;     
   }
     // Returns the JSON result string
    public String getResult() {
        return json.ValuetoString();
    }
}