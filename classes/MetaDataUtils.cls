/**
 * Utility class for dealing with Apex metadata APIs. The implementation takes care to cache
 * results of governor limited methods such as "fields" and "getPicklistValues" so the calling code
 * is freed from such concerns.
 * 
 *  @author Max Rudman
 *  @since 8/29/2009
 */
public class MetaDataUtils {
	private static final Map<String,List<Schema.SObjectField>> FIELDS_CACHE = new Map<String,List<Schema.SObjectField>>();
	private static final Map<String,List<Schema.PicklistEntry>> PICKLIST_VALUES_CACHE = new Map<String,List<Schema.PicklistEntry>>();
	
	/**
	 * Returns names of fields in object identified by specified name.
	 * 
	 * @param objectName API name of the object.
	 */
	public static List<String> getFieldNames(String objectName) {
		List<Schema.SObjectField> fields = getFields(objectName);
		List<String> result = new List<String>();
		for (Schema.SObjectField field : fields) {
			result.add(field.getDescribe().getName());
		}
		return result;
	}
	
	/**
	 * Returns names of fields in object identified by SObject token.
	 * 
	 * @param objectName API name of the object.
	 */
	public static List<String> getFieldNames(Schema.SObjectType soType) {
		List<Schema.SObjectField> fields = getFields(soType);
		List<String> result = new List<String>();
		for (Schema.SObjectField field : fields) {
			result.add(field.getDescribe().getName());
		}
		return result;
	}
	
	/**
	 * Returns field schema for specified object name. This method takes care to cache results 
	 * of "fields" calls in order to avoid exceeding governor limits.
	 * 
	 * @param objectName API name of the object.
	 */
	public static List<Schema.SObjectField> getFields(String objectName) {
		Schema.SObjectType soType = getType(objectName);
		return getFields(soType);
	}
	
	/**
	 * Returns field schema for specified object token. This method takes care to cache results 
	 * of "fields" calls in order to avoid exceeding governor limits.
	 * 
	 * @param objectName Object token.
	 */
	public static List<Schema.SObjectField> getFields(Schema.SObjectType soType) {
		String key = String.valueOf(soType);
		List<Schema.SObjectField> fields = FIELDS_CACHE.get(key);
		if (fields == null) {
			fields = soType.getDescribe().fields.getMap().values();
			FIELDS_CACHE.put(key, fields);
		}
		return fields;
	}
	
	public static Schema.SObjectType getType(String objectName) {
		System.assert(objectName != null);
		String objectNameLC = objectName.toLowerCase();
		Schema.SObjectType soType = Schema.getGlobalDescribe().get(objectNameLC);
		System.assert(soType != null, 'Unknown object: ' + objectName + '; Global Describe Keys: ' + Schema.getGlobalDescribe().keySet());
		return soType;
	}
	
	/**
	 * Returns field with specified name in object with specified name.
	 * 
	 * @param objectName Name of object that should be searched for specified field.
	 * @param fieldName Name of field.
	 */
	public static Schema.SObjectField getField(String objectName, String fieldName) {
		Schema.SObjectType soType = getType(objectName);
		return getField(soType, fieldName);
	}
	
	/**
	 * Returns field with specified name in specified object type.
	 * 
	 * @param objectName Name of object that should be searched for specified field.
	 * @param fieldName Name of field.
	 */
	public static Schema.SObjectField getField(Schema.SObjectType objectType, String fieldName) {
		for (Schema.SObjectField field : getFields(objectType)) {
			if (field.getDescribe().getName().equalsIgnoreCase(fieldName)) {
				return field;
			}
		}
		return null;
	}
	
	public static List<Schema.PicklistEntry> getPicklistValues(String objectName, String fieldName) {
		Schema.SObjectField field = getField(objectName, fieldName);
		if (field == null) {
			return null;
		}
		return getPicklistValues(getType(objectName), field);
	}
	
	public static List<Schema.PicklistEntry> getPicklistValues(Schema.SObjectType soType, Schema.SObjectField field) {
		String key = soType + '.' + field;
		if (PICKLIST_VALUES_CACHE.containsKey(key)) {
			return PICKLIST_VALUES_CACHE.get(key);
		}
		
		PICKLIST_VALUES_CACHE.put(key, field.getDescribe().getPicklistValues());
		return PICKLIST_VALUES_CACHE.get(key);
	}
	
	/**
	 * Attampts to find the child relationship for specified lookup field in specifid target object.
	 * 
	 * @param target SObject type that contains chaild relationship (eg: Contacts in Account)
	 * @param lookupField Lookup field in child object that points to target (eg: AccountId in Contact)
	 */
	public static Schema.ChildRelationship findRelationship(Schema.sObjectType target, Schema.SObjectField lookupField) {
        for (Schema.ChildRelationship rel : target.getDescribe().getChildRelationships()) {
            if (rel.getField() == lookupField) {
                return rel;
            }
        }
        return null;
    }
}