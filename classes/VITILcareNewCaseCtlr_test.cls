@isTest(SeeAllData=false)
public class VITILcareNewCaseCtlr_test {
    /*****    
     static User u {get;set;}

    static {
        u = MBRTestUtils.createPortalUser('');
        MBRTestUtils.createExternalToInternalCustomSettings();
        MBRTestUtils.createParentToChildCustomSettings();
        MBRTestUtils.createTypeToFieldsCustomSettings();

    }


    @isTest public static void doNothing(){
    aaaWorkAroundClass ctlr = new aaaWorkAroundClass();
       ctlr.doNothing();
    }*/

    
    @isTest public static void testOther() {
        // just for coverage
        ENTPNewCaseCtlr oldCtlr = new ENTPNewCaseCtlr();
        
        VITILcareNewCaseCtlr ctlr = new VITILcareNewCaseCtlr();
 ctlr.getRecordType('Test');
        System.assert(ctlr.getCategories().size() > 0);
        System.assert(ctlr.getRequestTypes().size() > 0);        
        System.assert(ctlr.createNewOnSubmit  == false);
        ctlr.createNewOnSubmit = true;
        System.assert(ctlr.createNewOnSubmit == true);
        ctlr.clear();
        ctlr.requestType = 'onlineStatus';
//        System.assert(ctlr.getRecordType('Dummy') == 'Generic');
        Attachment a  = ctlr.attachment;

        
        VITILcareNewCaseCtlr ctlr2 = new VITILcareNewCaseCtlr();

        System.assert(ctlr2.getCategories().size() > 0);
        System.assert(ctlr2.getRequestTypes().size() > 0);        
        System.assert(ctlr2.createNewOnSubmit  == false);
        ctlr2.createNewOnSubmit = true;
        System.assert(ctlr2.createNewOnSubmit == true);
        ctlr2.clear();
        ctlr2.requestType = 'onlineStatus';
       
    }

    @isTest public static  void testResetValues() {
        MBRTestUtils.createParentToChildCustomSettings();
        MBRTestUtils.createExternalToInternalCustomSettings();

        PageReference pref = Page.ENTPNewCase;
        pref.getParameters().put('category', 'CCICatRequest1');
        Test.setCurrentPage(pref);

        VITILcareNewCaseCtlr ctlr = new VITILcareNewCaseCtlr();
        // reset values
        ctlr.caseSubmitted = true;
        ctlr.resetValues();
        System.assert(ctlr.selectedCategory == Label.MBRSelectCategory);

        ctlr.caseSubmitted = false;
       
//        pref.getParameters().put('category', null);
        ctlr.resetValues();
       // System.assert(ctlr.selectedCategory == 'Mainstream/Data(i.e T1, T3, DS0, etc)');
    }

    @isTest public static void testInitCheck() {
        User u = MBRTestUtils.createPortalUser('');        
        System.runAs(u) {
            VITILcareNewCaseCtlr ctlr = new VITILcareNewCaseCtlr();
            System.assert(ctlr.initCheck() == null);
        }
    }
    
    @isTest public static void testCreateNewCase() {
        MBRTestUtils.createExternalToInternalCustomSettings();
//        MBRTestUtils.createParentToChildCustomSettings();
//        MBRTestUtils.createTypeToFieldsCustomSettings();

        //System.runAs(u) { // sharing issue with this context

            PageReference pref = Page.ENTPNewCase;
            pref.getParameters().put('category', 'CCICatRequest1');
            Test.setCurrentPage(pref);
        
            VITILcareNewCaseCtlr ctlr = new VITILcareNewCaseCtlr();
            
            ctlr.attachment.Name='Unit Test Attachment';
            ctlr.attachment.body=Blob.valueOf('Unit Test Attachment Body');

//System.debug('66: ctlr.createNewCase() + ' ctlr.createNewCase());        
//            System.assert(ctlr.createNewCase()ctlr.createNewCase() == null);

            ctlr.requestType = label.mbrSelectCategoryFirst;   
//            System.assert(ctlr.createNewCase() == null);
            
            ctlr.selectedCategory = 'Test';
            ctlr.requestType = label.mbrSelectCategoryFirst;  
//            System.assert(ctlr.createNewCase() == null); 
            
            ctlr.requestType = label.mbrSelectType;  
//            System.assert(ctlr.createNewCase() == null);


            ctlr.requestType = 'Voice - PRI';  
            ctlr.createNewCase();            //System.assert(ctlr.createNewCase() == null);

            ctlr.caseToInsert.recordtypeid = null;   
//            System.assert(ctlr.createNewCase() == null);

            ctlr.requestType = 'Mainstream/Data (i.e. T1, T3, DS0, etc)';
            ctlr.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId;   
            ctlr.createNewCase();
        //}
    }

    @isTest public static void testCreateParentCase() {
        MBRTestUtils.createExternalToInternalCustomSettings();

        VITILcareNewCaseCtlr ctlr = new VITILcareNewCaseCtlr();


        ctlr.selectedCategory = 'Voice - PRI';
        ctlr.requestType = 'Voice - PRI';  
        ctlr.caseToInsert.Description = 'Test Description';
        ctlr.caseToInsert.Subject = 'Test Subject';
        ctlr.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId;   
        ctlr.createNewCase();

        List<Case> parentCases = [SELECT Id, CaseNumber, My_Business_Requests_Type__c FROM Case ORDER BY CreatedDate DESC];
        Case parentCase = null;
        if (parentCases.size() > 0) {
            parentCase = parentCases[0];
            parentCase.status = 'Closed';
            update parentCase;
        }

 
System.debug('parentCase: ' + parentCase);        
        // set pageReference and querystring params
        PageReference submitPageRef = Page.ENTPNewCase;
        Test.setCurrentPage(submitPageRef);
       ApexPages.currentPage().getParameters().put('reopen', parentCase.CaseNumber);

        // instantiate a second controller for the child insert
       	VITILcareNewCaseCtlr ctlr2 = new VITILcareNewCaseCtlr();
        ctlr2.selectedCategory = 'Voice - PRI';
        ctlr2.requestType = 'Voice - PRI';
         ctlr2.caseToInsert.Description = 'Test Description';
        ctlr2.caseToInsert.Subject = 'Test Subject Child';
        ctlr2.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId;   
        ctlr2.createNewCase();
            
        Case childCase = [SELECT Id, Parent.Id, ParentId, CaseNumber FROM Case WHERE ParentId != null ORDER BY CreatedDate DESC LIMIT 1][0];
        System.assertEquals(parentCase.Id, childCase.Parent.Id);


    }

    @isTest
    public static void testPopulateDescrip(){
        MBRTestUtils.createExternalToInternalCustomSettings();
         Test.startTest();
        User u = MBRTestUtils.createVITILcarePortalUser('test-user-1', false, 'en_US');        
//        System.runAs(u) {
            VITILcareNewCaseCtlr ctlr = new VITILcareNewCaseCtlr();
            ctlr.requestType = 'Mainstream/Data (i.e. T1, T3, DS0, etc)';
            ctlr.selectedCategory = 'Enterprise';
            ctlr.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId; 
    
            ctlr.formWrapper.getTicketInfo.subject = 'Test';
            ctlr.formWrapper.getTicketInfo.projectNum = 'Test 123';
            ctlr.formWrapper.getTicketInfo.siteCompanyName = 'Test';
            ctlr.formWrapper.getTicketInfo.siteAddress = 'Test';
            ctlr.formWrapper.getTicketInfo.reportedBy = 'Test';
            ctlr.formWrapper.getTicketInfo.reportedByCompany = 'Test';
            ctlr.formWrapper.getTicketInfo.reportedByPhNum = 'Test';
            ctlr.formWrapper.getTicketInfo.reportedByEm = 'Test';
            ctlr.formWrapper.getTicketInfo.onsiteContactName = 'Test';
            ctlr.formWrapper.getTicketInfo.onsiteContactNumber = 'Test';
            ctlr.formWrapper.getTicketInfo.accessHours = 'Test';
           // ctlr.formWrapper.getTicketInfo.csID = 'Test';
           // ctlr.formWrapper.getTicketInfo.phNum = 'Test';
           // ctlr.formWrapper.getTicketInfo.PRINum = 'Test';
          //  ctlr.formWrapper.getTicketInfo.circuitNum = 'Test';
            ctlr.formWrapper.getTicketInfo.phoneNumber = 'Test';
            ctlr.formWrapper.getTicketInfo.customerTicketNum = 'Test';
           // ctlr.formWrapper.getTicketInfo.hazSafIssue = 'Test';
            ctlr.formWrapper.getTicketInfo.ongoingCalls = 'Test';
            ctlr.formWrapper.getTicketInfo.setModelColor = 'Test';
            ctlr.formWrapper.getTicketInfo.setLocation = 'Test';
            ctlr.formWrapper.getTicketInfo.allSetsAffected = 'Test';
           // ctlr.formWrapper.getTicketInfo.VoiceIssueType[] = 'Test, test2';
            ctlr.formWrapper.getTicketInfo.powerIssue = 'Test';
           /* ctlr.formWrapper.getTicketInfo.secEmail = 'Test';
            ctlr.formWrapper.getTicketInfo.intrusiveTest = 'Test';
            ctlr.formWrapper.getTicketInfo.noIntrusiveTestText = 'Test';
            ctlr.formWrapper.getTicketInfo.powerToTelusEquip = 'Test';*/
            
          /*  ctlr.formWrapper.getTicketInfo.telusEquipConn = 'Test';
            ctlr.formWrapper.getTicketInfo.cirUsedFor = 'Test';
            ctlr.formWrapper.getTicketInfo.howManyUsers = 'Test';
            ctlr.formWrapper.getTicketInfo.isBackupThere = 'Test';*/
            
           // ctlr.formWrapper.getTicketInfo.typeOfCircuit = 'Test';
            ctlr.formWrapper.getTicketInfo.conditionTypes = 'Test';
           /* ctlr.formWrapper.getTicketInfo.outageSig = 'Test';
            ctlr.formWrapper.getTicketInfo.sitesImpacted = 'Test';
            ctlr.formWrapper.getTicketInfo.serviceEverWorked = 'Test';*/
            
          /*  ctlr.formWrapper.getTicketInfo.setsAffected = 'Test';
            ctlr.formWrapper.getTicketInfo.jacksAffected = 'Test';
            ctlr.formWrapper.getTicketInfo.testResults = 'Test';
            ctlr.formWrapper.getTicketInfo.assocPhoneNumber = 'Test';
            ctlr.formWrapper.getTicketInfo.POI = 'Test';
            ctlr.formWrapper.getTicketInfo.centralOffice = 'Test';
            ctlr.formWrapper.getTicketInfo.orderNum = 'Test';
            ctlr.formWrapper.getTicketInfo.schdDateOfInstall = 'Test';
            ctlr.formWrapper.getTicketInfo.spId = 'Test';*/
            
           // ctlr.formWrapper.getTicketInfo.failedProvisioning = 'Test';
            
            ctlr.formWrapper.getTicketInfo.additionalNote = 'More information';
    
            ctlr.createNewCase();
        
        //	ctlr.userLanguage = 'fr';
            ctlr.createNewCase();
        
            List<Case> cases = [SELECT Id, Description, Subject FROM Case];
    //        System.assertEquals(cases.size(), 1);
    //        System.assert(cases[0].Description.contains('This user has his/her own device'));
    //        System.assert(cases[0].Description.contains(ctlr.formWrapper.additionalInfo));
    //        System.assert(cases[0].Subject.equalsIgnoreCase(ctlr.requestType));
//        }
Test.stopTest();       
    }
        
 @isTest
    public static void testPopulateDescripFr(){
        MBRTestUtils.createExternalToInternalCustomSettings();
        Test.startTest();
        User u = MBRTestUtils.createVITILcarePortalUser('test-user-1', false, 'fr');        
        System.runAs(u) {
            VITILcareNewCaseCtlr ctlr = new VITILcareNewCaseCtlr();
            ctlr.requestType = 'Mainstream/Data (i.e. T1, T3, DS0, etc)';
            ctlr.selectedCategory = 'Enterprise';
           // ctlr.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId; 
    
            ctlr.formWrapper.getTicketInfo.subject = 'Test';
            ctlr.formWrapper.getTicketInfo.projectNum = 'Test 123';
            ctlr.formWrapper.getTicketInfo.siteCompanyName = 'Test';
            ctlr.formWrapper.getTicketInfo.siteAddress = 'Test';
            ctlr.formWrapper.getTicketInfo.reportedBy = 'Test';
            ctlr.formWrapper.getTicketInfo.reportedByCompany = 'Test';
            ctlr.formWrapper.getTicketInfo.reportedByPhNum = 'Test';
            ctlr.formWrapper.getTicketInfo.reportedByEm = 'Test';
            ctlr.formWrapper.getTicketInfo.onsiteContactName = 'Test';
            ctlr.formWrapper.getTicketInfo.onsiteContactNumber = 'Test';
            ctlr.formWrapper.getTicketInfo.accessHours = 'Test';
            ctlr.formWrapper.getTicketInfo.phoneNumber = 'Test';
            ctlr.formWrapper.getTicketInfo.customerTicketNum = 'Test';
            ctlr.formWrapper.getTicketInfo.ongoingCalls = 'Test';
            ctlr.formWrapper.getTicketInfo.setModelColor = 'Test';
            ctlr.formWrapper.getTicketInfo.setLocation = 'Test';
            ctlr.formWrapper.getTicketInfo.allSetsAffected = 'Test';
            ctlr.formWrapper.getTicketInfo.powerIssue = 'Test';
          
            ctlr.formWrapper.getTicketInfo.conditionTypes = 'Test';
          
            
            ctlr.formWrapper.getTicketInfo.additionalNote = 'More information';
   
            ctlr.createNewCase();
        
        //	ctlr.userLanguage = 'fr';
           // ctlr.createNewCase();
        
            List<Case> cases = [SELECT Id, Description, Subject FROM Case];
    //        System.assertEquals(cases.size(), 1);
    //        System.assert(cases[0].Description.contains('This user has his/her own device'));
    //        System.assert(cases[0].Description.contains(ctlr.formWrapper.additionalInfo));
    //        System.assert(cases[0].Subject.equalsIgnoreCase(ctlr.requestType));
       }
  Test.stopTest();
    }
    @isTest public static void testUpdateCasePriorityToUrgent() {
        
        VITILcareNewCaseCtlr ctlr = new VITILcareNewCaseCtlr();

        String originValue = 'ENTPTest';
      
        List<Case> cases = new List<Case>();
        cases.add( new Case(Subject='4', Status = 'New', Priority = 'Medium', Origin = originValue) ); // subject will be used for day of week
        cases.add( new Case(Subject='5', Status = 'New', Priority = 'Medium', Origin = originValue) );
        cases.add( new Case(Subject='6', Status = 'New', Priority = 'Medium', Origin = originValue) );
        cases.add( new Case(Subject='0', Status = 'New', Priority = 'Medium', Origin = originValue) );
        cases.add( new Case(Subject='0', Status = 'New', Priority = 'Urgent', Origin = originValue) );
        cases.add( new Case(Subject='0', Status = 'In Progress', Priority = 'Medium', Origin = originValue) );

        System.debug('akong: cases A: ' + cases);

        try {
            insert cases;
        } catch (Exception e) {
            System.debug('akong: case insert exception: ' + e);
        }

        Test.startTest();
        VITILcareNewCaseCtlr.updateCasePriorityToUrgent();
        Test.stopTest();

        List<Case> urgentCases = [SELECT Id, Priority FROM Case WHERE Priority = 'Urgent'];
       System.assertEquals(1, urgentCases.size());
    }



}