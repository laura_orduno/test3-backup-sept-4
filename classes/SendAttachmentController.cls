public class SendAttachmentController{
    public Boolean flag{get;set;}    
    public Attachment attachmentInCase{get;set;}
    private String caseId=ApexPages.currentPage().getParameters().get('caseId');     
    public Attachment attachmentObj{get;set;}
     
    public SendAttachmentController(){
        attachmentObj= new Attachment();
        flag=false;        
    }
  //insert attachment  
    public void upload(){ 
        try{
            attachmentObj.parentid = caseId;
            insert attachmentObj;
            System.debug('###########After Insert');
            attachmentInCase=[Select Name, BodyLength From Attachment where (parentid=:caseId and CreatedDate=:System.now()) LIMIT 1];
            System.debug('##########'+attachmentInCase);
            flag=true;
            attachmentObj= new Attachment();
        }catch(Exception e){
            System.debug('###########In Catch');
            flag=false;
        } 
    }
    public PageReference cancel(){
        PageReference casePage = new PageReference('/' +caseId); 
        casePage.setRedirect(true);
        return casePage;
    }    
}