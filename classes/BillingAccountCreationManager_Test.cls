@isTest
public class BillingAccountCreationManager_Test {
  @isTest
    private static void testBillingAccountCreation() {
        
       OrdrTestDataFactory.createOrderingWSCustomSettings('BillingAccountCreationEndpoint', 'http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/billing/CreateBillingAccount/createBillingAccount');
         Account billingAccount = new Account(Name='U123TestBilling');
         insert billingAccount;
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        // Call the method that invokes a callout
         BillingAccountCreationManager.BillingAccountCreationRequest customerAccount = prepareDummyData();
         BillingAccountCreationManager.BillingAccountCreationResponse response = BillingAccountCreationManager.createBillingAccount(customerAccount);         
         BillingAccountCreationManager.createBillingAccountAndSave(customerAccount,billingAccount.Id);
         BillingAccountCreationManager.createBillingAccountFuture( JSon.serialize(customerAccount) ,billingAccount.Id);
         BillingAccountCreationManager.createBillingAccountFuture(customerAccount,billingAccount.Id);
        
        BillingAccCreationCustomerOrderMessage.CustomerOrderCollectionMessage b1 = new BillingAccCreationCustomerOrderMessage.CustomerOrderCollectionMessage();
        BillingAccCreationCustomerOrderMessage.CustomerAccountOrderItemMessage b2 = new BillingAccCreationCustomerOrderMessage.CustomerAccountOrderItemMessage();
        TypesSchemaCreateBillingAccount b = new TypesSchemaCreateBillingAccount();
   //     system.debug('response '+response); ,
        // Verify that a fake result is returned
       //  System.assertEquals(2, categories.size());
        Test.stopTest();
        
    }
    
      @isTest
    private static void testBillingAccount_fail() {
        
       OrdrTestDataFactory.createOrderingWSCustomSettings('BillingAccountCreationEndpoint', 'http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/billing/CreateBillingAccount/createBillingAccount');
         Account billingAccount = new Account(Name='U123TestBilling');
         insert billingAccount;
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl(false));
        // Call the method that invokes a callout
         BillingAccountCreationManager.BillingAccountCreationRequest customerAccount = prepareDummyData();
        try{
         BillingAccountCreationManager.BillingAccountCreationResponse response = BillingAccountCreationManager.createBillingAccount(customerAccount); 
        }catch(exception ex){
           
        }
        exception ext21 =  new OrdrExceptions.QueryResourceException();
        exception ext22 = new OrdrExceptions.BillingAccountCreationServiceException(); 
        exception ext23 = new OrdrExceptions.ServicePathServiceException(); 
        exception ext24 = new OrdrExceptions.RateGroupInfoServiceException(); 
        exception ext25 = new OrdrExceptions.ProductFeasibilityException(); 
        exception ext26 = new OrdrExceptions.ProductEligibilityException(); 
        exception ext27 = new OrdrExceptions.TechnicalAvailabilityServiceException();   
            
        ext21.getMessage();
        ext22.getMessage(); 
        ext23.getMessage(); 
        ext24.getMessage();
        ext25.getMessage(); 
        ext26.getMessage();
        ext27.getMessage();
     
        Test.stopTest();
        
    }
    
   @isTest
   private static void testBillingAccountCreation_Exception() {
        
       OrdrTestDataFactory.createOrderingWSCustomSettings('BillingAccountCreationEndpoint', 'http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/billing/CreateBillingAccount/createBillingAccount');
            
        Test.startTest();
        try {
            Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl('OrdrGenericException'));
            // Call the method that invokes a callout
            BillingAccountCreationManager.BillingAccountCreationResponse response = BillingAccountCreationManager.createBillingAccount(prepareDummyData());
            
         //   System.assert(false, 'Exception expected');
        }
        catch(Exception e) {
            System.debug(e.getTypeName() + ': ' + e.getMessage());
        }
        Test.stopTest();
        
   }  
   
     @isTest
   private static void testBillingAccountCreation_Exception_Missing_Mandatory_Parameters() {
       
       Test.startTest();
        try {
           BillingAccountCreationManager.BillingAccountCreationRequest customerAccount = new BillingAccountCreationManager.BillingAccountCreationRequest();
           customerAccount.billingSystemId='119';
           BillingAccountCreationWSCallout.validateForMandatoryValues(customerAccount);           
        }
        catch(Exception e) {
            System.debug(e.getTypeName() + ': ' + e.getMessage());
        }
        Test.stopTest();
        
   }  
     
 public static BillingAccountCreationManager.BillingAccountCreationRequest prepareDummyData(){
        
        BillingAccountCreationManager.BillingAccountCreationRequest customerAccount = new BillingAccountCreationManager.BillingAccountCreationRequest();
        OrdrErrorCodes OrdrErrorCodes = new OrdrErrorCodes();
        OrdrExceptions OrdrExceptions = new OrdrExceptions();
        List<BillingAccountCreationManager.ContactPerson> cpList = new List<BillingAccountCreationManager.ContactPerson>();
        
        BillingAccountCreationManager.ContactPerson cp  = new BillingAccountCreationManager.ContactPerson();
        cp.contactFirstName = 'Aditya';
        cp.contactLastName  = 'Jamwal';
        cp.contactEmail = 'test@g1.com';
        cp.contactPhoneNumber = '0987654321';
        cpList.add(cp);
        customerAccount.contactPersonList = cpList;
        
        BillingAccountCreationManager.BillingAddress ba = new BillingAccountCreationManager.BillingAddress();
        ba.houseNumber = '1234';
        ba.streetName = 'abc';
        ba.streetType = 'court';
        ba.city = 'Scarborough';
        ba.province = 'ON';
        ba.postalCode = 'M1T 3N5';
        customerAccount.billingAddress = ba;
        
        customerAccount.businessLegalName = 'legal';
        customerAccount.creditClass = '3';
        customerAccount.customerBusinessUnitCustomerId = '098765';
        customerAccount.customerProfileID = '76622';
        customerAccount.regionalCustomerId = '12322';
        customerAccount.taxProvince ='ON';
        customerAccount.displayLanguage = 'EN';
     	customerAccount.billingSystemId ='102';
        customerAccount.coreAccountPrefix ='102';
        customerAccount.businessDomain ='102';
        customerAccount.businessSubDomain ='102';
        customerAccount.responsiblegroup ='102';
        customerAccount.provincialTaxAppliesInd ='102';
        customerAccount.billingPeriodNumber ='102';
        customerAccount.federalTaxAppliesInd ='102';
        customerAccount.startDate = System.today();
        customerAccount.serviceTypeCode ='102';
        customerAccount.serviceRequested ='102';
        customerAccount.contractLengthInMonths ='102';
     	customerAccount.typeOfService = new List<String>{'102'};
     
     
     
        
        return customerAccount;
        
    }    
}