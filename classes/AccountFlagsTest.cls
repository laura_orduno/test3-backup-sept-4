@isTest(SeeAllData=true) 
private class AccountFlagsTest {

    static testMethod void testAccountIcons()
    {
        // setup new account data
        Account acct = new Account();
        acct.Name = 'test';
        acct.no_chronic_incidents__c = 2;
        acct.fraud__c = true;
        acct.strategic__c = true;
        acct.dntc__c = true;
        acct.churn_threat_flag__c = true;
        insert acct;

        Case caseRec = new Case();
        caseRec.accountId = acct.id;
        caseRec.Proactive_Case__c = true;
        insert caseRec;
                        
        PageReference pageRef = Page.AccountFlagsDetail;
        pageRef.getParameters().put('Id',acct.Id);
        Test.setCurrentPage(pageRef);      
        ApexPages.Standardcontroller stdcon = new ApexPages.Standardcontroller(acct);
        
        AccountFlagsHelper acctHelper = new AccountFlagsHelper(stdcon);

        string tmpStr;
        string queriedCaseId;
        
        queriedCaseId = acctHelper.getProactiveCaseIdForAccount(acct.Id,tmpStr);
        string caseId = string.valueOf(caseRec.Id);
        System.assertEquals(queriedCaseId,caseId);

        smb_IconCMSController cmsCtlr = new smb_IconCMSController();
        cmsCtlr.objId = acct.Id;
        cmsCtlr.objAPIName = 'Account';
        Component.Apex.OutputPanel outPanel = cmsCtlr.getThePanel();

        smb_IconCMSController cmsCtlr2 = new smb_IconCMSController();
        cmsCtlr2.objId = acct.Id;
        cmsCtlr2.objAPIName = 'Account';
        cmsCtlr2.iconClick = 'window.open(\'http://example.net/file_{!language}\');';
        Component.Apex.OutputPanel outPanel2 = cmsCtlr2.getThePanel();
         
        // test invalid data
        // 
        id badId = null;
        queriedCaseId = acctHelper.getProactiveCaseIdForAccount(badId, tmpStr);
        System.assertEquals(queriedCaseId,'');
    }

    static testMethod void testAccountIconsVOCSurvey(){
        
        // Cannot create a new VOC_Survey due to permissions
        // Instead look for an existing one

        List<VOC_Survey__c> vocList = [select id, account__r.id, SVOC_Status_Icon__c 
                                       from VOC_Survey__c
                                       where SVOC_Status_Icon__c like '%Icon' limit 1];

        if(vocList.size() == 0 || vocList[0].account__r.id == null) {
            return;
        }
        
        List<Account> acctList = [select id, name 
                                       from Account
                                       where id = :vocList[0].account__r.id limit 1];

        if(acctList.size() == 0 || acctList[0].id == null) {
            return;
        }

        PageReference pageRef = Page.AccountFlagsDetail;
        pageRef.getParameters().put('Id',acctList[0].Id);
        Test.setCurrentPage(pageRef);      
        ApexPages.Standardcontroller stdcon = new ApexPages.Standardcontroller(acctList[0]);
        
        AccountFlagsHelper acctHelper = new AccountFlagsHelper(stdcon);
        string tmpStr;
        string queriedVocId = acctHelper.getVOCSurveyIdFromAccount(vocList[0].Id,tmpStr);
        string vocId = string.valueOf(vocList[0].Id);
        
        smb_IconCMSController cmsCtlr = new smb_IconCMSController();
        cmsCtlr.objId = vocList[0].Id;
        cmsCtlr.objAPIName = 'VOC_Survey__c';
        Component.Apex.OutputPanel outPanel = cmsCtlr.getThePanel();

        smb_IconCMSController cmsCtlr2 = new smb_IconCMSController();
        cmsCtlr2.objId = vocList[0].Id;
        cmsCtlr2.objAPIName = 'VOC_Survey__c';
        cmsCtlr2.renderIcon = false;
        Component.Apex.OutputPanel outPanel2 = cmsCtlr2.getThePanel();

        smb_IconCMSController cmsCtlr3 = new smb_IconCMSController();
        cmsCtlr3.objId = acctList[0].Id; // Pass in the wrong ID to trigger a queryexception
        cmsCtlr3.objAPIName = 'VOC_Survey__c';
        Component.Apex.OutputPanel outPanel3 = cmsCtlr3.getThePanel();

        // test invalid data
        // 
        id badId = null;
        queriedVocId = acctHelper.getVOCSurveyIdFromAccount(badId, tmpStr);
        System.assertEquals(queriedVocId,'');
    }
}