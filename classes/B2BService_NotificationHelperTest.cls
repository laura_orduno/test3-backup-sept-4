/**
 * B2BService_NotificationHelperTest - Portal Refactoring
 * @description Tests for B2BService_NotificationHelper
 * @author Dane Peterson, Traction on Demand
 * @date 2017-01-03
 */

@isTest
private class B2BService_NotificationHelperTest {
    private static final String CASE_COMMENT = 'Value used in all tests';
    private static final String CHANGED_CASE_COMMENT = 'This is a changed comment';
    private static final String EMAIL = System.now().millisecond() + 'test@test.com';
    private static final String PORTAL_FIRST_NAME = 'FirstName';
    private static final String PORTAL_LAST_NAME = 'LastName';
    private static final String LYNXTICKETNUMBER = '12345';
    private static final String CASE_COMMENT_FROM_EMAIL = EMAIL + ' - ' + PORTAL_FIRST_NAME + ' ' + PORTAL_LAST_NAME + ' ' + Label.MBREmailCommentAuthor + CASE_COMMENT;

    private static Case c;
    private static CaseComment cc;

    static {
    	RestApi_MyAccountHelper_TestUtils.createMBRCustomSetting();
    }

    @isTest
    private static void testCaseInsertUpdateDelete(){
        User u = B2BService_NotificationHelperTest.createUser(false);
        Case result = B2BService_NotificationHelperTest.insertCase(PortalConstants.MYACCOUNT_ORIGIN, u, CASE_COMMENT);
        update result;
        delete result;
    }

    @isTest
    private static void MyAccountExecuteUpdated() {

    	Test.startTest();
        trac_TriggerHandlerBase.blockTrigger = true;
        User u = B2BService_NotificationHelperTest.createUser(false);
        Case result = B2BService_NotificationHelperTest.insertCase(PortalConstants.MYACCOUNT_ORIGIN, u, CASE_COMMENT);

        B2BService_NotificationHelper.executeUpdated(new Set<Id>{result.Id});

        trac_TriggerHandlerBase.blockTrigger = false;
        Test.stopTest();

    }

    @isTest
    private static void MyAccountExecuteCanceled() {
    	Test.startTest();
        trac_TriggerHandlerBase.blockTrigger = true;
        User u = B2BService_NotificationHelperTest.createUser(false);
        Case result = B2BService_NotificationHelperTest.insertCase(PortalConstants.MYACCOUNT_ORIGIN, u, CASE_COMMENT);
        result.Status = PortalConstants.CASESTATUS_CLOSED; 
        update result;
        B2BService_NotificationHelper.executeCanceled(new Set<Id>{result.Id});
    	Integer invocations = Limits.getEmailInvocations();

        trac_TriggerHandlerBase.blockTrigger = false;
        Test.stopTest();

        //System.assertEquals(1, invocations, 'An email should be sent');
    }

    @isTest
    private static void MBRExecuteUpdated() {
    	Test.startTest();
        trac_TriggerHandlerBase.blockTrigger = true;
        User u = B2BService_NotificationHelperTest.createUser(false);
        Case result = B2BService_NotificationHelperTest.insertCase(PortalConstants.MBR_ORIGIN, u, CASE_COMMENT);

        B2BService_NotificationHelper.executeUpdated(new Set<Id>{result.Id});

        trac_TriggerHandlerBase.blockTrigger = false;
        Test.stopTest();

        //System.assertEquals(1, invocations, 'An email should be sent');
    }

    @isTest
    private static void MBRExecuteCanceled() {
    	Test.startTest();
        trac_TriggerHandlerBase.blockTrigger = true;
        User u = B2BService_NotificationHelperTest.createUser(false);
        Case result = B2BService_NotificationHelperTest.insertCase(PortalConstants.MBR_ORIGIN, u, CASE_COMMENT);
        result.Status = PortalConstants.CASESTATUS_CLOSED; 
        update result;
        B2BService_NotificationHelper.executeCanceled(new Set<Id>{result.Id});

        trac_TriggerHandlerBase.blockTrigger = false;
        Test.stopTest();

        //System.assertEquals(1, invocations, 'An email should be sent');
    }

    @isTest
    private static void VIILExecuteUpdated() {
    	Test.startTest();
        trac_TriggerHandlerBase.blockTrigger = true;
        User u = B2BService_NotificationHelperTest.createUser(false);
        Case result = B2BService_NotificationHelperTest.insertCase(PortalConstants.VITIL_ORIGIN, u, CASE_COMMENT);

        B2BService_NotificationHelper.executeUpdated(new Set<Id>{result.Id});

        trac_TriggerHandlerBase.blockTrigger = false;
        Test.stopTest();

        //System.assertEquals(1, invocations, 'An email should be sent');
    }


    @isTest
    private static void ENTPExecuteUpdated() {
    	Test.startTest();
        trac_TriggerHandlerBase.blockTrigger = true;
        User u = B2BService_NotificationHelperTest.createUser(false);
        Case result = B2BService_NotificationHelperTest.insertCase(PortalConstants.ENTP_ORIGIN, u, CASE_COMMENT);

        B2BService_NotificationHelper.executeUpdated(new Set<Id>{result.Id});

        trac_TriggerHandlerBase.blockTrigger = false;
        Test.stopTest();

        //System.assertEquals(1, invocations, 'An email should be sent');
    }

     @isTest
    private static void processCommentsMYACCOUNTCANCELED() {
    	Test.startTest();
        trac_TriggerHandlerBase.blockTrigger = true;
        User u = B2BService_NotificationHelperTest.createUser(false);
        Case result = B2BService_NotificationHelperTest.insertCase(PortalConstants.MYACCOUNT_ORIGIN, u, CASE_COMMENT);
        Case cloned = result.clone(true,true);
        result.Status = PortalConstants.CASESTATUS_CLOSED;
        update result;

         Case c = [SELECT Id, Status, Origin, NotifyCollaboratorString__c, isClosedOnCreate FROM Case WHERE Id =:result.Id];
         Map<Id, Case> oldMap = new Map<Id,Case>{cloned.Id => cloned};
         B2BService_NotificationHelper.processComments(new List<Case>{c}, oldMap);


        trac_TriggerHandlerBase.blockTrigger = false;
        Test.stopTest();

        //System.assertEquals(1, invocations, 'An email should be sent');
    }

    @isTest
    private static void processCommentsMBRCANCELED() {
    	Test.startTest();
        trac_TriggerHandlerBase.blockTrigger = true;
        User u = B2BService_NotificationHelperTest.createUser(false);
        Case result = B2BService_NotificationHelperTest.insertCase(PortalConstants.MBR_ORIGIN, u, CASE_COMMENT);
        Case cloned = result.clone(true,true);
        result.Status = PortalConstants.CASESTATUS_CLOSED;
        update result;

        Case c = [SELECT Id, Status, Origin, NotifyCollaboratorString__c, isClosedOnCreate FROM Case WHERE Id =:result.Id];
        Map<Id, Case> oldMap = new Map<Id,Case>{cloned.Id => cloned};
        B2BService_NotificationHelper.processComments(new List<Case>{c}, oldMap);

        trac_TriggerHandlerBase.blockTrigger = false;
        Test.stopTest();

        //System.assertEquals(1, invocations, 'An email should be sent');
    }

    @isTest
    private static void processCommentsENTPCANCELED() {
    	Test.startTest();
        trac_TriggerHandlerBase.blockTrigger = true;
        User u = B2BService_NotificationHelperTest.createUser(false);
        Case result = B2BService_NotificationHelperTest.insertCase(PortalConstants.ENTP_ORIGIN, u, CASE_COMMENT);
        Case cloned = result.clone(true,true);
        result.Status = PortalConstants.CASESTATUS_CLOSED;
        update result;

        Case c = [SELECT Id, Status, Origin, NotifyCollaboratorString__c, isClosedOnCreate FROM Case WHERE Id =:result.Id];
        Map<Id, Case> oldMap = new Map<Id,Case>{cloned.Id => cloned};
        B2BService_NotificationHelper.processComments(new List<Case>{c}, oldMap);

        trac_TriggerHandlerBase.blockTrigger = false;
        Test.stopTest();

        //System.assertEquals(1, invocations, 'An email should be sent');
    }

    @isTest
    private static void processCommentsVITILCANCELED() {
        Test.startTest();
        trac_TriggerHandlerBase.blockTrigger = true;
        User u = B2BService_NotificationHelperTest.createUser(false);
        Case result = B2BService_NotificationHelperTest.insertCase(PortalConstants.VITIL_ORIGIN, u, CASE_COMMENT);
        Case cloned = result.clone(true,true);
        result.Status = PortalConstants.CASESTATUS_CLOSED;
        update result;

        Case c = [SELECT Id, Status, Origin, NotifyCollaboratorString__c, isClosedOnCreate FROM Case WHERE Id =:result.Id];
        Map<Id, Case> oldMap = new Map<Id,Case>{cloned.Id => cloned};
        B2BService_NotificationHelper.processComments(new List<Case>{c}, oldMap);

        trac_TriggerHandlerBase.blockTrigger = false;
        Test.stopTest();

        //System.assertEquals(1, invocations, 'An email should be sent');
    }

     @isTest
    private static void processCommentsMBRUPDATE() {
    	Test.startTest();
        trac_TriggerHandlerBase.blockTrigger = true;
        User u = B2BService_NotificationHelperTest.createUser(false);
        Case result = B2BService_NotificationHelperTest.insertCase(PortalConstants.MBR_ORIGIN, u, CASE_COMMENT);
        Case cloned = result.clone(true,true);
        result.Last_Case_Comment__c = CHANGED_CASE_COMMENT;

        Map<Id, Case> oldMap = new Map<Id,Case>{cloned.Id => cloned};
        B2BService_NotificationHelper.processComments(new List<Case>{result}, oldMap);

        trac_TriggerHandlerBase.blockTrigger = false;
        Test.stopTest();

        //System.assertEquals(1, invocations, 'An email should be sent');
    }

     @isTest
    private static void processCommentsMYACCOUNTUPDATE() {
    	Test.startTest();
        trac_TriggerHandlerBase.blockTrigger = true;
        User u = B2BService_NotificationHelperTest.createUser(false);
        Case result = B2BService_NotificationHelperTest.insertCase(PortalConstants.MYACCOUNT_ORIGIN, u, CASE_COMMENT);
        Case cloned = result.clone(true,true);
        result.Last_Case_Comment__c = CHANGED_CASE_COMMENT;

        Map<Id, Case> oldMap = new Map<Id,Case>{cloned.Id => cloned};
        B2BService_NotificationHelper.processComments(new List<Case>{result}, oldMap);

        trac_TriggerHandlerBase.blockTrigger = false;
        Test.stopTest();

        //System.assertEquals(1, invocations, 'An email should be sent');
    }

    @isTest
    private static void processCommentsVITILUPDATE() {
    	Test.startTest();
        trac_TriggerHandlerBase.blockTrigger = true;
        User u = B2BService_NotificationHelperTest.createUser(false);
        Case result = B2BService_NotificationHelperTest.insertCase(PortalConstants.VITIL_ORIGIN, u, CASE_COMMENT);
        Case cloned = result.clone(true,true);
        result.Last_Case_Comment__c = CHANGED_CASE_COMMENT;

        Map<Id, Case> oldMap = new Map<Id,Case>{cloned.Id => cloned};
        B2BService_NotificationHelper.processComments(new List<Case>{result}, oldMap);

        trac_TriggerHandlerBase.blockTrigger = false;
        Test.stopTest();

        //System.assertEquals(1, invocations, 'An email should be sent');
    }

    @isTest
    private static void processCommentsENTPUPDATE() {
    	Test.startTest();
        trac_TriggerHandlerBase.blockTrigger = true;
        User u = B2BService_NotificationHelperTest.createUser(false);
        Case result = B2BService_NotificationHelperTest.insertCase(PortalConstants.ENTP_ORIGIN, u, CASE_COMMENT);
        Case cloned = result.clone(true,true);
        result.Last_Case_Comment__c = CHANGED_CASE_COMMENT;

        Map<Id, Case> oldMap = new Map<Id,Case>{cloned.Id => cloned};
        B2BService_NotificationHelper.processComments(new List<Case>{result}, oldMap);

        trac_TriggerHandlerBase.blockTrigger = false;
        Test.stopTest();

        //System.assertEquals(1, invocations, 'An email should be sent');
    }

    // TO-DO: Modifiy and Complete

    @isTest
    private static void processCommentsENTPNEW() {
        Test.startTest();
        trac_TriggerHandlerBase.blockTrigger = true;
        User u = B2BService_NotificationHelperTest.createUser(false);
        Case result = B2BService_NotificationHelperTest.insertCase(PortalConstants.ENTP_ORIGIN, u, CASE_COMMENT);
        Case cloned = result.clone(true,true);
        result.Lynx_Ticket_Number__c = LYNXTICKETNUMBER;

        Map<Id, Case> oldMap = new Map<Id,Case>{cloned.Id => cloned};
        B2BService_NotificationHelper.processComments(new List<Case>{result}, oldMap);

        trac_TriggerHandlerBase.blockTrigger = false;
        Test.stopTest();
    }

    // TO-DO: Modifiy and Complete

    @isTest
    private static void processCommentsVITILNEW() {
        Test.startTest();
        trac_TriggerHandlerBase.blockTrigger = true;
        User u = B2BService_NotificationHelperTest.createUser(false);
        Case result = B2BService_NotificationHelperTest.insertCase(PortalConstants.VITIL_ORIGIN, u, CASE_COMMENT);
        Case cloned = result.clone(true,true);
        result.Lynx_Ticket_Number__c = LYNXTICKETNUMBER;

        Map<Id, Case> oldMap = new Map<Id,Case>{cloned.Id => cloned};
        B2BService_NotificationHelper.processComments(new List<Case>{result}, oldMap);

        trac_TriggerHandlerBase.blockTrigger = false;
        Test.stopTest();
    }

    @isTest
    private static void recordMunipulationTest() {
        User u = B2BService_NotificationHelperTest.createUser(false);
        Case result = B2BService_NotificationHelperTest.insertCase(PortalConstants.VITIL_ORIGIN, u, CASE_COMMENT);
        update result;
        delete result;
        undelete result;
    }



    private static User createUser(Boolean portalUser){
        User u; 

        if(portalUser){
            Account a = new Account(Name = 'Test Account');
            insert a;

            Contact contact1 = new Contact(
                FirstName = PORTAL_FIRST_NAME,
                Lastname = PORTAL_LAST_NAME,
                AccountId = a.Id,
                Email = EMAIL
            );
            insert contact1; 

            u = new User(
                ProfileId = [SELECT Id FROM Profile WHERE Name = 'Customer Community User Custom'].Id,
                LastName = PORTAL_LAST_NAME,
                FirstName= PORTAL_FIRST_NAME,
                Email = EMAIL,
                Username = EMAIL,
                CompanyName = 'TEST',
                Title = 'title',
                Alias = 'alias',
                TimeZoneSidKey = 'America/Los_Angeles',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                ContactId = contact1.Id
            );
            // TODO CREATE PORTAL USER SET U = PORTAL USER
        } else {
            u = [SELECT Id, Name FROM User Where Id =: UserInfo.getUserId()];
        }

        return u;

    }

    private static Case insertCase(String origin, User u, String comment){

        // This needs to be set to run the trigger for MyAccount
        System.runAs(u) {
            c = new Case(Subject = 'Test Class', Origin = origin, NotifyCollaboratorString__c = 'dpeterson@tractionondemand.com', Last_Case_Comment__c = CASE_COMMENT, Last_Case_Comment_Submitter__c = u.Name, Last_Case_Comment_Submitted_By__c = u.id, Last_Case_Comment_Public__c = true, Last_Case_Comment_Date__c = Date.today());
			insert c;            
        }

        Case result = [SELECT Last_Case_Comment__c, Last_Case_Comment_Submitter__c, Last_Case_Comment_Submitted_By__c, Last_Case_Comment_Date__c, Last_Case_Comment_Public__c, NotifyCollaboratorString__c FROM Case WHERE Id = :c.Id];
        return result;
    }

}