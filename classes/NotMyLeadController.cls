/*****************************************************
    Name: NotMyLeadController 

    Usage: This class use to change owner name of Lead to Allocation Catcher Queue 

    Author – Clear Task

    Date – 11/15/2010
    
    Revision History
******************************************************/
public class NotMyLeadController {
  // used for lead Id after click on 'Not My Lead' Button in Lead
    private String leadId = ApexPages.currentPage().getParameters().get('leadId');
  //Allocation Catcher Queue
  public static final String ALLOCATION_CATCHER_QUEUE = 'Allocation Catcher Reassignment Queue';
  //show the message 
    public boolean flag{get; set;}
  // assign Lead vallue
    private Lead leadObj;  
 /*
  * NotMyLeadController  constructure for find the record of Lead
  */
    public NotMyLeadController (){ 
        if(leadId != null && leadId != ''){       
            leadObj = [ select ID, OwnerId from Lead where ID=:leadId ];
        }
        flag = false; 
    }
 /*
  * init method for assign Allocation Catcher Queue to Lead Owner
  */
    public void init(){
        Group qObj = [Select g.Name, g.Id From Group g where g.Name= :ALLOCATION_CATCHER_QUEUE]; 
        if(qObj  != null && leadObj != null){
            leadObj.OwnerId  = qObj.Id;
            update leadObj;
            flag = true;
        }
    }
 /*
  * cancel  method for return to previous page while its from Lead
  */
  public PageReference cancel(){
      if(leadObj!= null){
          return new PageReference('/'+leadObj.Id);
      }
      return null;
  }  
}