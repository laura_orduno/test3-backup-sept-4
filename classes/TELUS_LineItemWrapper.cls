public with sharing class TELUS_LineItemWrapper implements Comparable{
    public SObject sObjectItem;
    public static String sortBy;
    public Object objItem;
    public TELUS_LineItemWrapper(SObject item){
        sObjectItem = item;
    }
    
    public TELUS_LineItemWrapper(Object item, String sortProp){
        objItem = item;
        sortBy = sortProp;
    }

    public Integer compareTo(Object compareTo){
        TELUS_LineItemWrapper compareToLine = (TELUS_LineItemWrapper)compareTo;
        Integer returnValue = 0;
        if(sObjectItem != null && compareToLine.sObjectItem != null) {
            if(sortBy ==  null || sortBy == 'Descending'){
                if(String.ValueOf(sObjectItem.get('vlocity_cmt__LineNumber__c')) > String.ValueOf(compareToLine.sObjectItem.get('vlocity_cmt__LineNumber__c'))){
                    returnValue = -1;
                
                } else if (String.ValueOf(sObjectItem.get('vlocity_cmt__LineNumber__c')) < String.ValueOf(compareToLine.sObjectItem.get('vlocity_cmt__LineNumber__c'))){
                    returnValue = 1;
                    
                }
            
            } else if(sortBy == 'Ascending'){
                if(String.ValueOf(sObjectItem.get('vlocity_cmt__LineNumber__c')) > String.ValueOf(compareToLine.sObjectItem.get('vlocity_cmt__LineNumber__c'))){
                    returnValue = 1;
                
                } else if (String.ValueOf(sObjectItem.get('vlocity_cmt__LineNumber__c')) < String.ValueOf(compareToLine.sObjectItem.get('vlocity_cmt__LineNumber__c'))){
                    returnValue = -1;
                    
                }
            
            
            }
        }
        /*else{
            system.debug('objItem'+objItem);
            system.debug('compareToLine.objItem'+compareToLine.objItem);
            if(objItem != null && compareToLine.objItem != null) {
                
                Map<String, Object> sObjMap = (Map<String, Object>)objItem;
                Map<String, Object> sObjMapCompareTo = (Map<String, Object>)compareToLine.objItem;
                if(sortBy ==  null || sortBy == 'Descending'){
                    if(String.ValueOf(sObjMap.get('vlocity_cmt__LineNumber__c')) > String.ValueOf(sObjMapCompareTo.get('vlocity_cmt__LineNumber__c'))){
                        returnValue = -1;
                    
                    } else if (String.ValueOf(sObjMap.get('vlocity_cmt__LineNumber__c')) < String.ValueOf(sObjMapCompareTo.get('vlocity_cmt__LineNumber__c'))){
                        returnValue = 1;
                        
                    }
                
                } else if(sortBy == 'Ascending'){
                    if(String.ValueOf(sObjMap.get('vlocity_cmt__LineNumber__c')) > String.ValueOf(sObjMapCompareTo.get('vlocity_cmt__LineNumber__c'))){
                        returnValue = 1;
                    
                    } else if (String.ValueOf(sObjMap.get('vlocity_cmt__LineNumber__c')) < String.ValueOf(sObjMapCompareTo.get('vlocity_cmt__LineNumber__c'))){
                        returnValue = -1;
                        
                    }
                
                
                }
            }
        }*/

        return returnValue;
    }
    
}