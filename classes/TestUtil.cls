public class TestUtil {

    public static Account createAccount() {
        Account acc = new Account(Name='Testing Software', BillingCountry = 'US', BillingState = 'IL');
        insert acc;
        return acc;
    }

    public static Opportunity createOpportunity(Id accountId) {
        Opportunity opp = new Opportunity(Name = 'Test Opp', AccountId = accountId, Type = 'New Customer', 
                                        CloseDate = Date.today(), StageName = 'Prospecting');
        insert opp;
        return opp;
        
    }

    public static List<Opp_Product_Item__c> createPLIList(Id oppId) {
        Pricebook2 pb = [select id, name from pricebook2 where isstandard = true limit 1];
        List<Product2> products = new List<Product2>();
        for(Integer i=0;i<5;i++){
            Product2 p = new Product2(Name='Product'+i, product_family_Code__c = 'Code'+i, 
                IsActive = true, BPC_1__c = 'BPC1' + i, BPC_2__c = 'BPC3' + i, BPC_3__c = 'BPC3' + i);
            products.add(p);
        }
        insert products;

        List<PricebookEntry> entries = new List<PricebookEntry>();
        for(Product2 p : products){
            PricebookEntry pbe = new PricebookEntry(pricebook2id = pb.id, product2id = p.id,unitprice = 100,usestandardprice = false, isactive = true);
            entries.add(pbe);
        }
        insert entries;     
        List<Opp_Product_Item__c> items = new List<Opp_Product_Item__c>();
        for(PricebookEntry p : entries){
            Opp_Product_Item__c item = new Opp_Product_Item__c(Opportunity__c = oppId, product__c = p.product2id, 
                quantity__c = 2);
            items.add(item);
        }
        insert items;
        return items;       
    }

    public static Opportunity_Solution__c createOpportunitySolution(Id oppId) {
        Opportunity_Solution__c solution = new Opportunity_Solution__c(Opportunity__c = oppId, Description__c = 'Testing', Name = 'Test');
        insert solution;
        return solution;
    }
    
    public static List<Case> createCases(Integer numToInsert, Id accountId, Boolean doInsert ) {
        List<Case> cases = new List<Case>();
        for( Integer i = 0; i < numToInsert; i++ ) {
            Case theCase = new Case();
            theCase.Account__c = accountId;
            theCase.Queue_Name__c ='ASR Ontario';
            theCase.Subject = 'TEST SUBJECT';
            theCase.Description = 'TEST';
            theCase.Status = 'New';
            theCase.Request_Type__c = 'Other';
            theCase.Priority = 'Medium';
            cases.add( theCase ); 
        }
        if( doInsert )
            insert cases;
        return cases;
    }
    
}