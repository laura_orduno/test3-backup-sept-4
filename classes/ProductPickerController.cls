/******************************************************************************************************
Controller class for product picker.
Has functionality for - 
1. Looking up products based on the hierarchy
2. Choosing Opportunity Product Item Record Type

Author - Rohit Mehta
Updated by - Tom Muzyka
Created Date - Sep 2009
Update Date  - May 2010

******************************************************************************************************/

public with sharing class ProductPickerController {

    public Id opportunityId;
    public Id solutionId;
    public Id pliId;
    public Id opiRecTypeId;
    public Opportunity_Solution__c oppSolution {get; set;}
    public Opp_Product_Item__c pli {get;set;}
    public ProductSearchComponent component {get; set;}
    public boolean flagSearchAll{get; set;} 
    public String bpc1 {get; set;}
    public String bpc2 {get; set;}
    public String bpc3 {get; set;}
    
    public List<SelectOption> bpc1Options {get; set;}
    public List<SelectOption> bpc2Options {get; set;}
    public List<SelectOption> bpc3Options {get; set;}
    
    public List<Product2> productList {get; set;}
    
    public String selectedProduct {get; set;}
    public String selectedProductName {get; set;}
    public Decimal selectedProductLeadTime {get; set;}
    
    public String pageMessage {get; private set;}
    
    public static final Integer MIN_LENGTH_REQD = 2;
    public static final String FLD_OPI_SOLUTION = '00N40000001zp1U';
    public static final String FLD_OPI_OPPORTUNITY = '00N40000001zp16';
    public static final String FLD_OPI_PRODUCT = '00N40000001zp1Z';
    public static final String CODE_OPI = 'a0L';
    
    /*
     * Pagination Variable -Start   
     */
        private List<Product2>  pageProductList;    
        private Integer pageNumber;    
        private Integer pageSize;    
        private Integer totalPageNumber;
        //return current pagenumber
        public Integer getPageNumber(){
            return pageNumber;
        }
        //retulr partner list to show by pagesize
        public List<Product2> getPageProductList(){
            return pageProductList;
        }
        //return pagesize
        public Integer getPageSize(){
           return pageSize;
        }
        
        /*
         *  return total page 
         */
        public Integer getTotalPageNumber(){
            if (totalPageNumber == 0 && productList !=null){    
            totalPageNumber = productList .size() / pageSize;    
            Integer mod = productList .size() - (totalPageNumber * pageSize);    
            if (mod > 0)    
                totalPageNumber++;    
            }    
            return totalPageNumber;
        }

    /*
    * Pagination Variable -End
    */ 
    
    public ProductPickerController() {
        solutionId = System.currentPageReference().getParameters().get('solutionId');
        pliId = Util.isBlank(System.currentPageReference().getParameters().get('pliId')) ? null : System.currentPageReference().getParameters().get('pliId');

        //pliId = (System.currentPageReference().getParameters().get('pliId').equals('')) ? null : System.currentPageReference().getParameters().get('pliId');
        
        component = new ProductSearchComponent();
        init();
         //Pagination -Start
             pageNumber = 0;    
             totalPageNumber = 0;    
             pageSize = 30;
        //Pagination - Ends  
        flagSearchAll=false; 
    }
    
    private void init() {
        oppSolution = [Select Name, Description__c, Opportunity__c, Opportunity_Close_Date__c, Opportunity__r.Name, Opportunity__r.RecordTypeId from Opportunity_Solution__c where Id = :solutionId];
        if (pliId == null)  { //its a new product
            pli = null; //new Opp_Product_Item__c(Opportunity__c = oppSolution.Opportunity__c, Opportunity_Solution__c = solutionId);
        } else { //edit mode
            pli = [Select Opportunity__c from Opp_Product_Item__c where Id = :pliId];
        }
        reset();
    }
    
    private void reset() {
        bpc1Options = new List<SelectOption>();
        bpc2Options = new List<SelectOption>();
        bpc3Options = new List<SelectOption>();
        bpc1 = null;
        bpc2 = null;
        bpc3 = null;
        pageMessage = null;
        
        productList = new List<Product2>();
    }
 /*
  * Pagination-Start
  */
      public PageReference firstBtnClick() {
        bindData(1);    
        return null;
      }
      
      public PageReference previousBtnClick() {
        bindData(pageNumber - 1);    
        return null;
      }
      
      public PageReference nextBtnClick() {
        bindData(pageNumber + 1);    
        return null;
      }
      
      public PageReference lastBtnClick() {
        pageNumber =totalPageNumber -1;
        bindData(totalPageNumber );    
        return null;
      }
      
      // bindData method is used for show the partner record as per current pagesize
      
      private void bindData(Integer newPageIndex){
        try{    
            if (productList== null){
                System.debug('########hits='+ productList);    
                return;
            }
            pageProductList= new List<Product2> ();    
            Transient Integer counter = 0;    
            Transient Integer min = 0;    
            Transient Integer max = 0;    
            if (newPageIndex > pageNumber){    
                min = pageNumber * pageSize;    
                max = newPageIndex * pageSize;                
            }else{    
                max = newPageIndex * pageSize;    
                min = max - pageSize;                 
            }   
            for(Product2 p : productList){    
                counter++;    
                if (counter > min && counter <= max)    
                pageProductList.add(p);    
            }    
            pageNumber = newPageIndex;        
            if (pageProductList== null || pageProductList.size() <= 0) {    
            }
        }    
        catch(Exception ex){         
        }
      }
  /*
  * Pagination-End
  */ 
    public Pagereference save() {
        if (pli != null) {
            pli.Product__c = selectedProduct;
            update pli;
            return new Pagereference('/' + pli.Id);
        }

        return new Pagereference('/' + CODE_OPI + '/e?CF' + FLD_OPI_SOLUTION + '=' + oppSolution.name + 
            '&CF' + FLD_OPI_SOLUTION + '_lkid=' + solutionId + 'CF' + FLD_OPI_OPPORTUNITY + '=' + 
            oppSolution.Opportunity__r.Name + 'CF' + FLD_OPI_OPPORTUNITY + '_lkid=' + oppSolution.Opportunity__c +
            'CF' + FLD_OPI_PRODUCT + '=' + selectedProductName + 'CF' + FLD_OPI_PRODUCT + '_lkid=' + selectedProduct +   
            '&retURL=%2F' + solutionId);
    }
    
    public Pagereference cancel() {
        return new Pagereference('/' + solutionId);
    }
    
    public Pagereference addProduct() {
        return new Pagereference('/' + solutionId);
    }

    public List<Product2> getSearchResults() {
        productList = component.searchProducts();
        return productList;
    }
    public void doRenderBlock(){
        productList.clear();
        if (component.searchOption.equals('Search All') ) {
            flagSearchAll=true; 
        }else{
            flagSearchAll=false; 
        }
    }
    public Pagereference selectProduct() {
        try {
            if (pli != null) {
                pli.Product__c = selectedProduct;
                update pli;
                return new Pagereference('/' + pli.Id);
            }
        } catch (Exception e) {
// *****            pageMessage = e.getMessage();
            return null;
        }
        
        if (oppSolution.Opportunity__r.RecordTypeId == '01240000000DnnN') {
            opiRecTypeId = '01240000000DnnM';
        } else {
            opiRecTypeId = '01240000000DnnP';
        }
        System.debug('oppSolution.name = ' + oppSolution.name);
        System.debug('oppSolution.Opportunity__r.Name = ' + oppSolution.Opportunity__r.Name);
        System.debug('selectedProductName = ' + selectedProductName);
        Decimal temp_sbsd = 0;
        String temp_lead = String.valueOf(selectedProductLeadTime);
        if(temp_lead == '0' || temp_lead == null) temp_sbsd = 150;
        else temp_sbsd = selectedProductLeadTime;
        //return new Pagereference('/apex/CreatePLI?solutionId=' + solutionId + '&p2Id=' + selectedProduct);
         return new Pagereference('/' + CODE_OPI + '/e?CF' + FLD_OPI_SOLUTION + '=' + EncodingUtil.urlEncode(oppSolution.name, 'UTF-8') + 
                '&CF' + FLD_OPI_SOLUTION + '_lkid=' + solutionId + '&CF' + FLD_OPI_OPPORTUNITY + '=' + 
                EncodingUtil.urlEncode(oppSolution.Opportunity__r.Name, 'UTF-8') + '&CF' + FLD_OPI_OPPORTUNITY + '_lkid=' + oppSolution.Opportunity__c +
                '&CF' + FLD_OPI_PRODUCT + '=' + EncodingUtil.urlEncode(selectedProductName, 'UTF-8') + '&CF' + FLD_OPI_PRODUCT + '_lkid=' + selectedProduct +   
                '&retURL=%2F' + solutionId + '&RecordType=' + opiRecTypeID + '&ent=Opportunity_Product_Item__c' + '&00N40000001zp1D=' + (oppSolution.Opportunity_Close_Date__c+Integer.valueOf(temp_sbsd)).format());
     
    }

    public Pagereference searchProducts() {
        reset();
        if (component == null) {
            component = new ProductSearchComponent();
        }
       /* if (component.searchOption.equals('Search All') ) {
            flagSearchAll=true;
            component.searchText='';
        } 
        if (!component.searchOption.equals('Search All') ) {
            flagSearchAll=false;            
        } */
        if (Util.isBlank(component.searchText)) {
            pageMessage = 'Please enter a search criteria.';
            return null;
        } else if (component.searchText.length() < MIN_LENGTH_REQD) {
            pageMessage = 'The search criteria should be at least ' + MIN_LENGTH_REQD + ' characters';
            return null;
        }
        
        productList = component.searchProducts();
        if (component.searchOption != null ) {
            if (component.searchOption.equals('BPC_1__c')) {
                System.debug('Set Options for bpc1');
                setOptions(productList, bpc1Options, 'BPC1');
            } else if (component.searchOption.equals('BPC_2__c')) {
                System.debug('Set Options for bpc2');
                setOptions(productList, bpc2Options, 'BPC2');
            }  else if (component.searchOption.equals('BPC_3__c')) {
                System.debug('Set Options for bpc3');
                setOptions(productList, bpc3Options, 'BPC3');
            } else if (component.searchOption.equals('Search All')) {
                System.debug('Set Options for bpc3');
                setOptions(productList, bpc1Options, 'BPC1');
            }          
        }
        totalPageNumber = 0;    
        bindData(1); 
        if (productList.isEmpty()) {
            pageMessage = 'No products found for the search criteria.';
        }
        return null;
    }
    
    public Pagereference bpc3Selected() {
        if (Util.isBlank(bpc3)) {
            return bpc2Selected();
        }
        String query = 'Select ' + Util.join(ProductSearchComponent.FIELDS_TO_RETRIEVE,',',null) + ' from Product2 where BPC_3__c = :bpc3 And IsActive = true';
        if (bpc2 != null && (!bpc2.equals(''))) {
            query += ' and BPC_2__c = :bpc2';
        }
        if (bpc1 != null && (!bpc1.equals(''))) {
            query += ' and BPC_1__c = :bpc1';
        }
        //productList = Database.query('Select Id, Name, BPC_1__c, BPC_2__c, BPC_3__c, Family from Product2 where BPC_3__c = :bpc3 and BPC_2__c = :bpc2 and BPC_1__c = :bpc1' );
        productList = Database.query(query + ProductSearchComponent.ORDER_BY);
        totalPageNumber = 0;    
        bindData(1); 
        return null;
    }
    
    public Pagereference bpc2Selected() {
        if (Util.isBlank(bpc2)) {
            return bpc1Selected();
        }
        bpc3Options = new List<SelectOption>();
        bpc3Options.add(new SelectOption('', '-Select-'));
        String query = 'Select ' + Util.join(ProductSearchComponent.FIELDS_TO_RETRIEVE,',',null) + ' from Product2 where BPC_2__c = :bpc2 And IsActive = true';
        if (bpc1 != null && (!bpc1.equals(''))) {
            query += ' and BPC_1__c = :bpc1';
        }
        //productList = Database.query('Select Id, Name, BPC_1__c, BPC_2__c, BPC_3__c, Family from Product2 where BPC_2__c = :bpc2 and BPC_1__c = :bpc1' );
        productList = Database.query(query + ProductSearchComponent.ORDER_BY);
        Set<String> bpc3Set = new Set<String>();
        for(Product2 p : productList) {
            bpc3Set.add(p.BPC_3__c);
        }
        for (String s : bpc3Set) {
            bpc3Options.add(new SelectOption(s, s));
        }
        totalPageNumber = 0;    
        bindData(1); 
        return null;
    }
    
    public Pagereference bpc1Selected() {
        if (Util.isBlank(bpc1)) {
            return searchProducts();
        }
        bpc2Options = new List<SelectOption>();
        bpc2Options.add(new SelectOption('', '-Select-'));
        productList = Database.query('Select ' + Util.join(ProductSearchComponent.FIELDS_TO_RETRIEVE,',',null) + ' from Product2 where BPC_1__c = :bpc1 And IsActive = true' + ProductSearchComponent.ORDER_BY);
        Set<String> bpc2Set = new Set<String>();
        for(Product2 p : productList) {
            bpc2Set.add(p.BPC_2__c);
        }
        for (String s : bpc2Set) {
            bpc2Options.add(new SelectOption(s, s));
        }
        /*if (bpc2Options.size() > 0) {
            bpc2 = bpc2Options.get(0).getValue();
            return bpc2Selected();
        }*/
        totalPageNumber = 0;    
        bindData(1); 
        return null;
    }
    
    public List<SelectOption> getSearchOptions() {
        List<SelectOption> searchOptions = new List<SelectOption>();
        //searchOptions.add(new SelectOption('', '--Select--'));
        searchOptions.add(new SelectOption('Search All',  'Search All'));
        searchOptions.add(new SelectOption('BPC_1__c', 'BPC1'));
        searchOptions.add(new SelectOption('BPC_2__c', 'BPC2'));
        searchOptions.add(new SelectOption('BPC_3__c', 'BPC3'));
        searchOptions.add(new SelectOption('PRODUCT_FAMILY__c',  'EPP5'));
        //add Search All in PickList
       
        return searchOptions;
    }
    
    private void setOptions(List<Product2> productList, List<SelectOption> selectOptionList, String bpcIdentifier) {
        selectOptionList.clear();
        selectOptionList.add(new SelectOption('', '-Select-'));
        Set<String> bpcSet = new Set<String>();
        for (Product2 p : productList) {
            if (bpcIdentifier.equals('BPC1')) {
                bpcSet.add(p.BPC_1__c);
            } else if (bpcIdentifier.equals('BPC2')) {
                bpcSet.add(p.BPC_2__c);
            } else if (bpcIdentifier.equals('BPC3')) {
                bpcSet.add(p.BPC_3__c);
            }
        }
        for (String s : bpcSet) {
            selectOptionList.add(new SelectOption(s, s));
        }
        
/*      if (selectOptionList.size() > 0) {
            if (bpcIdentifier.equals('BPC1')) {
                bpc1 = selectOptionList.get(0).getValue();
                bpc1Selected();
            } else if (bpcIdentifier.equals('BPC2')) {
                bpc2 = selectOptionList.get(0).getValue();
                bpc2Selected();
            } else if (bpcIdentifier.equals('BPC3')) {
                bpc3 = selectOptionList.get(0).getValue();
                bpc3Selected();
            }
        }*/
        System.debug('bpcSet = ' + bpcSet);
        System.debug('bpc1Options = ' + bpc1Options.size());
    }
    
}