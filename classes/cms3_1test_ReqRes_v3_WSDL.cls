/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class cms3_1test_ReqRes_v3_WSDL {

static testMethod void cms3_1ContractManagementSvcReqRes_v3() {
       cms3_1ContractManagementSvcReqRes_v3 smb21 = new cms3_1ContractManagementSvcReqRes_v3();
       cms3_1ContractManagementSvcReqRes_v3.calculateTerminationCharges_element a1 = new cms3_1ContractManagementSvcReqRes_v3.calculateTerminationCharges_element();
       cms3_1ContractManagementSvcReqRes_v3.calculateTerminationChargesResponse_element a2 = new cms3_1ContractManagementSvcReqRes_v3.calculateTerminationChargesResponse_element();
       cms3_1ContractManagementSvcReqRes_v3.cancelContractSubmission_element a3 = new cms3_1ContractManagementSvcReqRes_v3.cancelContractSubmission_element();
       cms3_1ContractManagementSvcReqRes_v3.cancelContractSubmissionResponse_element a4 = new cms3_1ContractManagementSvcReqRes_v3.cancelContractSubmissionResponse_element();
       cms3_1ContractManagementSvcReqRes_v3.ContractData a5 = new cms3_1ContractManagementSvcReqRes_v3.ContractData();
       cms3_1ContractManagementSvcReqRes_v3.createContractSubmission_element a6 = new cms3_1ContractManagementSvcReqRes_v3.createContractSubmission_element();
       cms3_1ContractManagementSvcReqRes_v3.createContractSubmissionResponse_element a7 = new cms3_1ContractManagementSvcReqRes_v3.createContractSubmissionResponse_element();
       cms3_1ContractManagementSvcReqRes_v3.findContractAmendmentsByAssociateNum_element a8 = new cms3_1ContractManagementSvcReqRes_v3.findContractAmendmentsByAssociateNum_element();
       cms3_1ContractManagementSvcReqRes_v3.findContractAmendmentsByAssociateNumResponse_element a9 = new cms3_1ContractManagementSvcReqRes_v3.findContractAmendmentsByAssociateNumResponse_element();
       cms3_1ContractManagementSvcReqRes_v3.findContractAmendmentsByAssociateNumber_element a32 = new cms3_1ContractManagementSvcReqRes_v3.findContractAmendmentsByAssociateNumber_element();
       cms3_1ContractManagementSvcReqRes_v3.findContractAmendmentsByAssociateNumberResponse_element a33 = new cms3_1ContractManagementSvcReqRes_v3.findContractAmendmentsByAssociateNumberResponse_element();
       cms3_1ContractManagementSvcReqRes_v3.findContractData_element a10 = new cms3_1ContractManagementSvcReqRes_v3.findContractData_element();
       cms3_1ContractManagementSvcReqRes_v3.findContractDataResponse_element a11 = new cms3_1ContractManagementSvcReqRes_v3.findContractDataResponse_element();
       cms3_1ContractManagementSvcReqRes_v3.findContractsByAgentId_element a12 = new cms3_1ContractManagementSvcReqRes_v3.findContractsByAgentId_element();
       cms3_1ContractManagementSvcReqRes_v3.findContractsByAgentIdResponse_element a13 = new cms3_1ContractManagementSvcReqRes_v3.findContractsByAgentIdResponse_element();
       cms3_1ContractManagementSvcReqRes_v3.findContractsByCustomerId_element a14 = new cms3_1ContractManagementSvcReqRes_v3.findContractsByCustomerId_element();
       cms3_1ContractManagementSvcReqRes_v3.findContractsByCustomerIdResponse_element a15 = new cms3_1ContractManagementSvcReqRes_v3.findContractsByCustomerIdResponse_element();
       cms3_1ContractManagementSvcReqRes_v3.getContract_element a16 = new cms3_1ContractManagementSvcReqRes_v3.getContract_element();
       cms3_1ContractManagementSvcReqRes_v3.getContractDocument_element a17 = new cms3_1ContractManagementSvcReqRes_v3.getContractDocument_element();
       cms3_1ContractManagementSvcReqRes_v3.getContractDocumentMetaData_element a18 = new cms3_1ContractManagementSvcReqRes_v3.getContractDocumentMetaData_element();
       cms3_1ContractManagementSvcReqRes_v3.getContractDocumentMetaDataResponse_element a19 = new cms3_1ContractManagementSvcReqRes_v3.getContractDocumentMetaDataResponse_element();
       cms3_1ContractManagementSvcReqRes_v3.getContractDocumentResponse_element a20 = new cms3_1ContractManagementSvcReqRes_v3.getContractDocumentResponse_element();
       cms3_1ContractManagementSvcReqRes_v3.getContractResponse_element a21 = new cms3_1ContractManagementSvcReqRes_v3.getContractResponse_element();
       cms3_1ContractManagementSvcReqRes_v3.replaceContractSubmission_element a22 = new cms3_1ContractManagementSvcReqRes_v3.replaceContractSubmission_element();
       cms3_1ContractManagementSvcReqRes_v3.replaceContractSubmissionResponse_element a23 = new cms3_1ContractManagementSvcReqRes_v3.replaceContractSubmissionResponse_element();
       cms3_1ContractManagementSvcReqRes_v3.TerminationChargesInformation a24 = new cms3_1ContractManagementSvcReqRes_v3.TerminationChargesInformation();
       cms3_1ContractManagementSvcReqRes_v3.TerminationChargesParameter a25 = new cms3_1ContractManagementSvcReqRes_v3.TerminationChargesParameter();
       cms3_1ContractManagementSvcReqRes_v3.triggerResendContract_element a26 = new cms3_1ContractManagementSvcReqRes_v3.triggerResendContract_element();
       cms3_1ContractManagementSvcReqRes_v3.triggerResendContractResponse_element a27 = new cms3_1ContractManagementSvcReqRes_v3.triggerResendContractResponse_element();
       cms3_1ContractManagementSvcReqRes_v3.updateContractSubmission_element a28 = new cms3_1ContractManagementSvcReqRes_v3.updateContractSubmission_element();
       cms3_1ContractManagementSvcReqRes_v3.updateContractSubmissionResponse_element a29 = new cms3_1ContractManagementSvcReqRes_v3.updateContractSubmissionResponse_element();
       cms3_1ContractManagementSvcReqRes_v3.verifyValidReplacement_element a30  = new cms3_1ContractManagementSvcReqRes_v3.verifyValidReplacement_element();
       cms3_1ContractManagementSvcReqRes_v3.verifyValidReplacementResponse_element a31 = new cms3_1ContractManagementSvcReqRes_v3.verifyValidReplacementResponse_element();
            
            }
            }