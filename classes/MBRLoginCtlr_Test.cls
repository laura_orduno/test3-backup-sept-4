/**
* @author: Christian Wico - cwico@tractionondemand.com
* @description: Unit Test for MBRLoginCtlr.cls
*/
@isTest(SeeAllData=false)
private class MBRLoginCtlr_Test {
	
	@isTest static void testLoginSuccess() {
		Test.setCurrentPage(Page.MBRLogin);
		User u = MBRTestUtils.createPortalUser('');
		MBRLoginCtlr ctlr = new MBRLoginCtlr();
		ctlr.username = u.username;
		ctlr.password = MBRTestUtils.testUserPassword;
		PageReference result = ctlr.login(); // salesforce does not assert Site.login
	}

	@isTest static void testLoginFail() {
		MBRLoginCtlr ctlr = new MBRLoginCtlr();
		ctlr.username = 'thisisnotavalidemail';
		System.assert(ctlr.login() == null);
	}

	@isTest static void testErrorMassages() {
		Test.setCurrentPage(Page.MBRLogin);
		MBRLoginCtlr ctlr = new MBRLoginCtlr();
		ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: Invalid Input.');
        ApexPages.addMessage(myMsg);		
		ctlr.username = 'test@testing.com';
		ctlr.password = 'aaaaaaaaaa';
		ctlr.login();
		System.assert(ctlr.errors.size() > 0);
	}

	@isTest static void testLoginBadUsername() {
		MBRLoginCtlr ctlr = new MBRLoginCtlr();
		ctlr.username = 'fail@example.com';
		PageReference ref = ctlr.login();
		System.debug(ref);
		System.debug('Username Error: ' + ctlr.usernameError);
		System.assert(ctlr.usernameError != null);
	}
    
    
    
    /* VITILcare portal*/
    
	@isTest static void VITILcare_testLoginSuccess() {
System.debug('VITILcare_testLoginSuccess???');        
		Test.setCurrentPage(Page.VITILcareLogin);
		User u = MBRTestUtils.createPortalUser('VITILcare');
		VITILcareLoginCtlr ctlr = new VITILcareLoginCtlr();
		ctlr.username = u.username;
System.debug('VITILcare_testLoginSuccess: ctlr.username: ' + ctlr.username);        
		ctlr.password = MBRTestUtils.testUserPassword;
		PageReference result = ctlr.login(); // salesforce does not assert Site.login
	}

	@isTest static void VITILcare_testLoginFail() {
		VITILcareLoginCtlr ctlr = new VITILcareLoginCtlr();
		ctlr.username = 'thisisnotavalidemail';
		System.assert(ctlr.login() == null);
	}

	@isTest static void VITILcare_testErrorMassages() {
		Test.setCurrentPage(Page.VITILcareLogin);
		User u = MBRTestUtils.createPortalUser('VITILcare');
		VITILcareLoginCtlr ctlr = new VITILcareLoginCtlr();
		ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: Invalid Input.');
        ApexPages.addMessage(myMsg);		
		ctlr.username = 'test@testing.com';
		ctlr.password = 'aaaaaaaaaa';
		ctlr.login();
		System.assert(ctlr.errors.size() > 0);
	}

	@isTest static void VITILcare_testLoginBadUsername() {
		VITILcareLoginCtlr ctlr = new VITILcareLoginCtlr();
		User u = MBRTestUtils.createPortalUser('VITILcare');
		ctlr.username = 'fail@example.com';
		PageReference ref = ctlr.login();
		System.debug(ref);
		System.debug('Username Error: ' + ctlr.usernameError);
		System.assert(ctlr.usernameError != null);
	}
}