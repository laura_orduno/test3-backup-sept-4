public class CloudProjectData{
	public Id projectId;
    public String projectName;
    public String projectType;
    public String isEvolution;
    public String milestone;
    public String startDate;
    public String endDate; 
}