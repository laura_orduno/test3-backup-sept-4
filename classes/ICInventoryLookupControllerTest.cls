@isTest
private class ICInventoryLookupControllerTest{

    @isTest
    static void testInitialLookupMainPage(){    
        createICPicUpDeviceItem('Internet Stick', 'Vancouver','BC');
        createICPicUpDeviceItem('PCS Device', 'Vancouver','BC');
        createICCourierDeviceItem('SmartHub', 'Barnaby','BC');
        createICCourierDeviceItem('Internet Stick', 'Barnaby','BC');
        createICInventoryFaxAccountItem();
     
        RecordType recordTypeDevice = [select Id, Name from RecordType where SObjectType = 'IC_Solution__c' and Name = 'Loaner Device'];
        PageReference pageRef = Page.ICInventoryLookupPage;
        Test.setCurrentPageReference(pageRef);
        ApexPages.currentPage().getParameters().put('rt',recordTypeDevice.id);
        ApexPages.currentPage().getParameters().put('frm','textForm');
        ApexPages.currentPage().getParameters().put('txt','textFiledId');
       
        ICInventoryLookupController controllerDevice = new ICInventoryLookupController();
        System.assertEquals(4,controllerDevice.results.size(),'Testing Result');
        System.assertEquals(3,controllerDevice.getCityList().size(),'Testing City List');
        System.assertEquals(4,controllerDevice.getDeviceTypes().size(),'Testing DeviceTypes');
        System.assertEquals(3,controllerDevice.getDeliveryMethods().size(),'Delivery Methods');
        System.assertEquals(true,controllerDevice.getIsDevice(),'Testing IsDevice');
        
        System.assertEquals('textForm',controllerDevice.getFormTag(),'Testing FormTag');
        System.assertEquals('textFiledId',controllerDevice.getTextBox(),'Testing TextBox');
      
        RecordType recordTypeFax = [select Id, Name from RecordType where SObjectType = 'IC_Solution__c' and Name = 'Internet Fax'];
        ApexPages.currentPage().getParameters().put('rt',recordTypeFax.id);
        ICInventoryLookupController controllerFax = new ICInventoryLookupController();
        System.assertEquals(1,controllerFax.results.size());
        System.assertEquals(false,controllerFax.getIsDevice());
        
        
     
        
    }
    
    @isTest
    static void testDeliveryMethodFilter (){    
        createICPicUpDeviceItem('Internet Stick', 'Vancouver','BC');
        createICPicUpDeviceItem('PCS Device', 'Vancouver','BC');
        createICPicUpDeviceItem('Internet Stick', 'Maple Ridge','BC');

        createICCourierDeviceItem('SmartHub', 'Barnaby','BC');
        createICCourierDeviceItem('Internet Stick', 'Barnaby','BC');
        
        RecordType recordTypeDevice = [select Id, Name from RecordType where SObjectType = 'IC_Solution__c' and Name = 'Loaner Device'];
        PageReference pageRef = Page.ICInventoryLookupPage;
        Test.setCurrentPageReference(pageRef);
        ApexPages.currentPage().getParameters().put('rt',recordTypeDevice.id);
        
        
       
        ICInventoryLookupController controllerDevice = new ICInventoryLookupController();
        controllerDevice.deliveryMethod ='Customer Pick-up';
        controllerDevice.Search();
        System.assertEquals(3,controllerDevice.results.size(), 'Testing Customer Pickup');
     
        controllerDevice.deliveryMethod ='Courier to Customer';
        controllerDevice.Search();
        System.assertEquals(2,controllerDevice.results.size());
        
    }
    
    @isTest
    static void testFilterFunction(){    
         createICPicUpDeviceItem('Internet Stick', 'Vancouver','BC');
        createICPicUpDeviceItem('PCS Device', 'Vancouver','BC');
        createICPicUpDeviceItem('Internet Stick', 'Calgery','AB');

        createICCourierDeviceItem('SmartHub', 'Barnaby','BC');
        createICCourierDeviceItem('Internet Stick', 'Barnaby','BC');
        createICCourierDeviceItem('SmartHub', 'Edmonton','AB');
        
        RecordType recordTypeDevice = [select Id, Name from RecordType where SObjectType = 'IC_Solution__c' and Name = 'Loaner Device'];
        PageReference pageRef = Page.ICInventoryLookupPage;
        Test.setCurrentPageReference(pageRef);
        ApexPages.currentPage().getParameters().put('rt',recordTypeDevice.id);
        
        ICInventoryLookupController controller = new ICInventoryLookupController();
        controller.searchCity='Vancouver';
        controller.Search();
        System.assertEquals(2,controller.results.size(), 'Testing Results For City Vancouver');
        controller.searchCity='Barnaby';
        controller.Search();
        System.assertEquals(2,controller.results.size(), 'Testing Results For City Barnaby');
        System.assertEquals(5,controller.getCityList().size(), 'Testing CityList');
        
        controller.searchCity='';
        
        controller.searchDeviceType = 'SmartHub';
        controller.Search();
        System.assertEquals(2,controller.results.size(), 'Testing Results For DeviceType  SmartHub');
       
        controller.searchDeviceType = 'Internet Stick';
        controller.Search();
        System.assertEquals(3,controller.results.size(), 'Testing Results For  DeviceType Internet Stick');
        
        controller.searchDeviceType = 'PCS Device';
        controller.Search();
        System.assertEquals(1,controller.results.size(), 'Testing Results For DeviceType PCS Device');
        
    }
    
    
     @isTest
     static void testDeliveryMethodsDropDown(){
             
        RecordType recordTypeDevice = [select Id, Name from RecordType where SObjectType = 'IC_Solution__c' and Name = 'Loaner Device'];
        PageReference pageRef = Page.ICInventoryLookupPage;
        Test.setCurrentPageReference(pageRef);
        ApexPages.currentPage().getParameters().put('rt',recordTypeDevice.id);
        ApexPages.currentPage().getParameters().put('frm','textForm');
        ApexPages.currentPage().getParameters().put('txt','textFiledId');
       
        ICInventoryLookupController controller = new ICInventoryLookupController();
         System.debug('controller.getDeliveryMethods() :' + controller.getDeliveryMethods());
             System.assertEquals(3, controller.getDeliveryMethods().size());
             SelectOption option1 = controller.getDeliveryMethods().get(0);
             SelectOption option2 = controller.getDeliveryMethods().get(1);   
             SelectOption option3 = controller.getDeliveryMethods().get(2);   
             System.assertEquals('All', option1.getLabel());
             System.assertEquals('Customer Pick-up',    option2.getLabel());
             System.assertEquals('Courier to Customer', option3.getLabel());
        }
    
    public static IC_Inventory__c createICPicUpDeviceItem(String deviceType,String city, String prov){
        RecordType recordType = [select Id, Name from RecordType where SObjectType = 'IC_Inventory__c' and Name = 'Device' Limit 1];
        IC_Device_Provider__c dealer = createDealerProvider(city,prov);
        System.debug('Createing PicUpDeviceIte');
        System.debug('Dealer : '+ dealer);
        IC_Inventory__c item = new IC_Inventory__c();
        
        item.RecordTypeId=recordType.Id;
        item.Device_Type__c = deviceType;
        item.Device_Provider__c=dealer.id;
        item.STN__c ='454546546';
        //item.ESN__c ='4546546';
        insert item;
        System.debug('PicUpDeviceItem After Insert');
        System.debug('PicUpDeviceIte : ' + item);
        return item;
    }
    
    public static IC_Inventory__c createICCourierDeviceItem(String deviceType,String city, String prov){
        RecordType recordType = [select Id, Name from RecordType where SObjectType = 'IC_Inventory__c' and Name = 'Device' Limit 1];
        IC_Device_Provider__c dealer = createWarHouseProvider(city,prov);
        System.debug('Createing CourierDeviceItem');
        System.debug('Dealer :'+dealer);
        IC_Inventory__c item = new IC_Inventory__c();
        item.RecordTypeId=recordType.Id;
        item.Device_Type__c = deviceType;
        item.Device_Provider__c=dealer.id;
        item.STN__c ='454546546';
        //item.ESN__c ='4546546';
        insert item;
        System.debug('CourierDeviceItem After Insert');
        System.debug('CourierDeviceItem: ' + item);
        return item;
    }

    
    public static IC_Inventory__c createICInventoryFaxAccountItem(){
        RecordType recordType = [select Id, Name from RecordType where SObjectType = 'IC_Inventory__c' and Name = 'Internet Fax Account' Limit 1];
        IC_Inventory__c item = new IC_Inventory__c();
        item.RecordTypeId=recordType.Id;
        item.Fax_Number__c='7787789874';
        insert item;
        return item;
    }
    
    public static IC_Device_Provider__c createDealerProvider(String city, String prov) { 
        IC_Device_Provider__c provider = new IC_Device_Provider__c(Name = 'Test Account-Dealer');
        provider.ICS_Delivery_Method__c = 'Customer Pick-up';
        provider.City__c=city;
        provider.Province__c=prov;
        provider.ICS_Notification_Email__c = 'prishan.fernando@telus.com,mark.heimburger@telus.com';
        provider.Active__c= true;
        insert provider;
        return provider;
    }
    
     public static IC_Device_Provider__c createWarHouseProvider(String city, String prov) { 
        IC_Device_Provider__c provider = new IC_Device_Provider__c(Name = 'Test Account-WareHouse');
        provider.ICS_Delivery_Method__c = 'Courier to Customer';
        provider.City__c=city;
        provider.Province__c=prov;
        provider.ICS_Notification_Email__c = 'prishan.fernando@telus.com,mark.heimburger@telus.com';
        provider.Active__c= true;
        insert provider;
        return provider;
    }
    
}