/*
    *******************************************************************************************************************************
    Class Name:     OCOM_TaskEvent
    Purpose:        Object to accommodate tasks or events        
    Created by:     Thomas Su             30-Dec-2016     QC-9301       No previous version
    Modified by:    
    *******************************************************************************************************************************
*/

public class OCOM_TaskEvent implements Comparable {
    public String id {get;set;}
    public String subject {get;set;}
    public Id whoId {get;set;}
    public String whoName {get;set;}
    public Boolean isTask {get;set;}
    public DateTime createdDate {get;set;}
    public String dueDate {get;set;}
    public String status {get;set;}
    public String priority {get;set;}
    public Id ownerId {get;set;}
    public String ownerName {get;set;}
    public String lastModifiedDate {get;set;}
    public String summary {get;set;}
    
    public Integer compareTo(Object compareTo) {
        OCOM_TaskEvent that = (OCOM_TaskEvent) compareTo;
        if(this.createdDate < that.createdDate)
            return 1;
        else if(this.createdDate > that.createdDate)
            return -1;
        else
            return 0;
    }
}