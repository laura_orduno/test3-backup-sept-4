@IsTest
public class vlocity_addon_DynamicTableHelper_Test {
    static testmethod void ServiceChargesTableTest(){
        TestDataHelper.testContractLineItemListCreation_ConE();
        Test.startTest();
        Map<String,Object> outMap=new Map<String,Object>();
        Apexpages.currentPage().getparameters().put('id',TestDataHelper.testContractObj.id);
        vlocity_addon_ServiceChargesTable objvlocity_addon_ServiceChargesTable=new vlocity_addon_ServiceChargesTable();
        boolean blnSuccess=objvlocity_addon_ServiceChargesTable.invokeMethod('buildDocumentSectionContent',new Map<String,Object>{'contextObjId' => TestDataHelper.testContractObj.id},outMap,new Map<String,Object>());
        system.assertEquals(outMap!=null && outMap.size() > 0, true);
        system.assertEquals(blnSuccess, true);
        Test.stopTest();
    }
    
    static testmethod void ServiceSummaryTableTest(){
        TestDataHelper.testContractLineItemListCreation_ConE();
        Test.startTest();
        Map<String,Object> outMap=new Map<String,Object>();
        Apexpages.currentPage().getparameters().put('id',TestDataHelper.testContractObj.id);
        vlocity_addon_ServicesSummaryTable objvlocity_addon_ServicesSummaryTable=new vlocity_addon_ServicesSummaryTable();
        boolean blnSuccess=objvlocity_addon_ServicesSummaryTable.invokeMethod('buildDocumentSectionContent',new Map<String,Object>{'contextObjId' => TestDataHelper.testContractObj.id},outMap,new Map<String,Object>());
        system.assertEquals(outMap!=null && outMap.size() > 0, true);
        system.assertEquals(blnSuccess, true);
        Test.stopTest();
    }
    static testmethod void SummaryChargesTableTest(){
        TestDataHelper.testContractLineItemListCreation_ConE();
        Test.startTest();
        Map<String,Object> outMap=new Map<String,Object>();
        Apexpages.currentPage().getparameters().put('id',TestDataHelper.testContractObj.id);
        vlocity_addon_SummaryChargesTable objvlocity_addon_SummaryChargesTable=new vlocity_addon_SummaryChargesTable();
        boolean blnSuccess=objvlocity_addon_SummaryChargesTable.invokeMethod('buildDocumentSectionContent',new Map<String,Object>{'contextObjId' => TestDataHelper.testContractObj.id},outMap,new Map<String,Object>());
        system.assertEquals(outMap!=null && outMap.size() > 0, true);
        system.assertEquals(blnSuccess, true);
        Test.stopTest();
    }
   /* static testmethod void MasterContractSummaryChargesDataTest(){
        
         TestDataHelper.testContractLineItemListCreation();
         Test.startTest();
       // vlocity_addon_DynamicTableHelper c1 = new vlocity_addon_DynamicTableHelper();
         map<string,object> s2 = new map<string,object>();
         String s1 = TestDataHelper.testContractObj.id ;
        TestDataHelper.testContractLineItemListCreation();
         vlocity_cmt__ContractLineItem__c v2 =  new vlocity_cmt__ContractLineItem__c(vlocity_cmt__RecurringTotal__c=9,vlocity_cmt__Product2Id__c=TestDataHelper.testProductObj.id,vlocity_cmt__ContractId__c=TestDataHelper.testContractObj.id,Monthly_Variable_Total__c=10,vlocity_cmt__OneTimeTotal__c=12,name='Name123',vlocity_cmt__OneTimeCharge__c=2,vlocity_cmt__Quantity__c=12);
         insert v2;
        
         vlocity_cmt__ContractLineItem__c v1 =  new vlocity_cmt__ContractLineItem__c(Replaced_Contract_Line__c = v2.id ,vlocity_cmt__RecurringTotal__c=9,vlocity_cmt__Product2Id__c=TestDataHelper.testProductObj.id,vlocity_cmt__ContractId__c=TestDataHelper.testContractObj.id,Monthly_Variable_Total__c=10,vlocity_cmt__OneTimeTotal__c=12,name='Name123',vlocity_cmt__OneTimeCharge__c=2,vlocity_cmt__Quantity__c=12);
         insert v1;
         s2 = vlocity_addon_DynamicTableHelper.getServiceChargesData(s1);
     
         system.assertEquals(s2.size()>0, true);
         Test.stopTest();
         }*/
    static testmethod void masterSummaryChargesQueryTest(){
        
         TestDataHelper.testContractLineItemListCreation_ConE();
         Test.startTest();
       // vlocity_addon_DynamicTableHelper c1 = new vlocity_addon_DynamicTableHelper();
         list<AggregateResult> s2 = new list<AggregateResult>();
         Id s1 = TestDataHelper.testContractObj.id ;
         s2 = vlocity_addon_DynamicTableHelper.masterSummaryChargesQuery(s1);
         Test.stopTest();
        
    }
    
    static testmethod void getRateSheetDataTest(){
        
         TestDataHelper.testContractLineItemListCreation_ConE();
         vlocity_cmt__ContractLineItem__c v2 =  new vlocity_cmt__ContractLineItem__c(vlocity_cmt__RecurringTotal__c=9,vlocity_cmt__Product2Id__c=TestDataHelper.testProductObj.id,vlocity_cmt__ContractId__c=TestDataHelper.testContractObj.id,Monthly_Variable_Total__c=10,vlocity_cmt__OneTimeTotal__c=12,name='Name123',vlocity_cmt__OneTimeCharge__c=2,vlocity_cmt__Quantity__c=12);
         insert v2;
        
         vlocity_cmt__ContractLineItem__c v1 =  new vlocity_cmt__ContractLineItem__c(Replaced_Contract_Line__c = v2.id ,vlocity_cmt__RecurringTotal__c=9,vlocity_cmt__Product2Id__c=TestDataHelper.testProductObj.id,vlocity_cmt__ContractId__c=TestDataHelper.testContractObj.id,Monthly_Variable_Total__c=10,vlocity_cmt__OneTimeTotal__c=12,name='Name123',vlocity_cmt__OneTimeCharge__c=2,vlocity_cmt__Quantity__c=12);
         insert v1; 
         Test.startTest();
       // vlocity_addon_DynamicTableHelper c1 = new vlocity_addon_DynamicTableHelper();
       
         map<string,object> s2 = new map<string,object>();
         String s1 = TestDataHelper.testContractObj.id ;
         s2 = vlocity_addon_DynamicTableHelper.getRateSheetData(s1);
         system.assertEquals(s2.size()>0, true);
         Test.stopTest();
        
    }
     static testmethod void getRateSheetDataTest2(){
        
         TestDataHelper.testReplacedContractLineItemListCreation();
         vlocity_cmt__ContractLineItem__c v2 =  new vlocity_cmt__ContractLineItem__c(vlocity_cmt__RecurringTotal__c=9,vlocity_cmt__Product2Id__c=TestDataHelper.testProductObj.id,vlocity_cmt__ContractId__c=TestDataHelper.testContractObj.id,Monthly_Variable_Total__c=10,vlocity_cmt__OneTimeTotal__c=12,name='Name123',vlocity_cmt__OneTimeCharge__c=2,vlocity_cmt__Quantity__c=12);
         insert v2;
        
         vlocity_cmt__ContractLineItem__c v1 =  new vlocity_cmt__ContractLineItem__c(Replaced_Contract_Line__c = v2.id ,vlocity_cmt__RecurringTotal__c=9,vlocity_cmt__Product2Id__c=TestDataHelper.testProductObj.id,vlocity_cmt__ContractId__c=TestDataHelper.testContractObj.id,Monthly_Variable_Total__c=10,vlocity_cmt__OneTimeTotal__c=12,name='Name123',vlocity_cmt__OneTimeCharge__c=2,vlocity_cmt__Quantity__c=12);
         insert v1; 
         Test.startTest();
       // vlocity_addon_DynamicTableHelper c1 = new vlocity_addon_DynamicTableHelper();
       
         map<string,object> s2 = new map<string,object>();
         String s1 = TestDataHelper.testContractObj.id ;
         s2 = vlocity_addon_DynamicTableHelper.getRateSheetData(s1);
         system.assertEquals(s2.size()>0, true);
         Test.stopTest();
     }
    static testmethod void getSummaryChargesDataTest(){
        TestDataHelper.testContractLineItemListWithProductCreation();
        Test.startTest();
        vlocity_cmt__ContractLineItem__c v2 =  new vlocity_cmt__ContractLineItem__c(vlocity_cmt__RecurringTotal__c=9,vlocity_cmt__Product2Id__c=TestDataHelper.testProductObj.id,vlocity_cmt__ContractId__c=TestDataHelper.testContractObj.id,Monthly_Variable_Total__c=10,vlocity_cmt__OneTimeTotal__c=12,name='Name123',vlocity_cmt__OneTimeCharge__c=2,vlocity_cmt__Quantity__c=12);
    	insert v2;
        vlocity_cmt__ContractLineItem__c v1 =  new vlocity_cmt__ContractLineItem__c(Replaced_Contract_Line__c = v2.id ,vlocity_cmt__RecurringTotal__c=9,vlocity_cmt__Product2Id__c=TestDataHelper.testProductObj.id,vlocity_cmt__ContractId__c=TestDataHelper.testContractObj.id,Monthly_Variable_Total__c=10,vlocity_cmt__OneTimeTotal__c=12,name='Name123',vlocity_cmt__OneTimeCharge__c=2,vlocity_cmt__Quantity__c=12);
        insert v1;
        map<string,object> s2 = new map<string,object>();
        Id s1 = TestDataHelper.testContractObj.id ;
        s2 = vlocity_addon_DynamicTableHelper.getSummaryChargesData_aa(s1);
        Test.stopTest();
    }
    
}