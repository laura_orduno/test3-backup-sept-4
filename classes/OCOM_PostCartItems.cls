/**

OCOM_PostCartItems used in OCOM_CustomCpqAppHandlerHookImpl

Executed when Order Product records are added to Cart

- Availability check for Child Offers (Formattig JSON results)
- Exlude flags for Exclude Rule Voice Features 


*/

public without sharing class OCOM_PostCartItems {
    
public static Map<String,List<String>> prodToRelatedProdMap = new Map<String,List<String>>();
 public static Map<String,List<String>> relatedProdToProdMap = new Map<String,List<String>>();
 public static Map<Id,Map<String,List<String>>> requiresProdRelIdMap = new Map<Id,Map<String,List<String>>>();
 public static integer nodeCounter = 0;
 public static Map<String,integer> grandChildNodeCounter = new Map<String,integer>();
 public  Map<String,Object> indexToOrderIDMap = new Map<String,Object>();
 public static Boolean isSuperUser = false;
 public set<String> excludeWirelssProductSet = new set<String>{'telus wireless product','wireless rate plan product component','wireless device product component','sim card product component'};
 public static Map<Id,Id> overlineProd2OliId = new Map<Id,Id>();
 public static Map<Id,Map<id,priceBookEntry>> topOfferToAvailablePbeMap =  new Map<Id,Map<id,priceBookEntry>> ();
 public static String linetemHirerachy = '';
 public static Integer getChildItemsiterationCount = 0;
    
    public  void genericMethod(Map<String,Object> input, Map<String,Object> output, String methodName){
        
         Map<id,Map<String,String>> oliIdtoAttrsUpdateMap = new Map<id,Map<String,String>>();
        System.debug('******* Limit getHeapSize()-1- --' + Limits.getHeapSize());
        System.debug('******* Limit getLimitHeapSize() 1--- ' +Limits.getLimitHeapSize());
       
        //##################################################################
        // Order Id 

        String OrderId = String.ValueOf(input.get('cartId'));
            
        //##################################################################
        // TELUS Connectivity 


            String OrderItemId = String.ValueOf(input.get('id'));
            string topOfferProdId = '';
            String NCID = '';
            try{
            Map<String, OrderItem> topOfferMap = new Map<String, OrderItem>();
            if(topOfferMap!= null && topOfferMap.size() == 0){
                topOfferMap = getTopoffer(OrderId);
            }
          /****Adharsh: Commenting out AutoAdd Telus Connectivity as this is handled by OOB Rules *****************/
            /*Map<String,Object> inputItemInfoMap = new Map<String,Object> {'topOfferMap' => topOfferMap};
              OCOM_AddTelusConnectivityHelper.allowOfficeInternet = false;
              if(methodName.equalsIgnoreCase('postCartsItems.postInvoke') && !Test.isRunningTest())
                OCOM_AddTelusConnectivityHelper.addTelusConnectivity(OrderId, OrderItemId,inputItemInfoMap,true);
            */
                //Arpit : 4-Jan-2017 - Sprint 12 : Added for Checkout readiness
                system.debug('Inside postCartsItems_GetCartsItems_PostInvoke');
                system.debug('OrderItemId ----> '+ OrderItemId);                
                system.debug('!!SOQL Called before NAAS checkoutReadinessOnOliAddition consumed '+ Limits.getQueries() +' Available' +Limits.getLimitQueries());
                //System.debug('Debugging output='+JSON.serialize(output));
               
                //if(!Test.isRunningTest())
                OCOM_CustomEligibilityHookHelper.checkoutReadinessOnOliAddition(output, (Id)orderItemId );
                system.debug('!!SOQL Called after NAAS checkoutReadinessOnOliAddition consumed '+ Limits.getQueries() +' Available' +Limits.getLimitQueries());
            
            System.debug('******* Limit getHeapSize() --2' + Limits.getHeapSize());
            System.debug('******* Limit getLimitHeapSize() -- 2' +Limits.getLimitHeapSize());   

             if(OrdrUtilities.isSuperUser()) {
                isSuperUser = true;
            }
    
            // Get Exclude Replationship Logic from product Relationship Table

            if((prodToRelatedProdMap.size() == 0 && relatedProdToProdMap.size() == 0 )){
                Map<String,Object> outMap = OCOM_CustomHookUtil.getProductReplationShip();
                prodToRelatedProdMap = (Map<String,List<String>>)outMap.get('prodToRelatedProdMap');
                relatedProdToProdMap = (Map<String,List<String>>)outMap.get('relatedProdToProdMap');
                requiresProdRelIdMap =  (Map<Id,Map<String,List<String>>>)outMap.get('requiresProdRelsMap');
            }

            
        if(topOfferMap.size() > 0 && !topOfferMap.isEmpty() ){
            
    // ************* Implementing Eligibity change that accepts list of NCIDS **********************
            Set<String> ncidSet = new Set<String>();
            //Generate set of Top offer NCIDs
            for(orderItem topOffer: topOfferMap.values()){
                ncidSet.add(topOffer.ProductExternalId__c);
            }
            //Check Custom setting before calling Eligibity
             String callProductEligibilityManager = OCOM_CustomHookUtil.getConfigurationValue('OCOM_HookAvailaibilityTrigger');
            Map<id,priceBookEntry> availablePriceBookEntryMap = new Map<Id, priceBookEntry> ();// { '01u220000010lOnAAI' => new pricebookEntry(id = '01u220000010lOnAAI') }; 
           
           //Added for test Coverage
            if(Test.isRunningTest() && vlocity_cmt.FlowStaticMap.FlowMap.ContainsKey('test.availablePriceBookEntryMap')){
                availablePriceBookEntryMap = (Map<Id,priceBookEntry>)vlocity_cmt.FlowStaticMap.FlowMap.get('test.availablePriceBookEntryMap');
            }

             if((oliIdtoAttrsUpdateMap == null || oliIdtoAttrsUpdateMap.Size() == 0 || oliIdtoAttrsUpdateMap.isEmpty()) && vlocity_cmt.FlowStaticMap.FlowMap.ContainsKey('updateoliAttrs.updateNamedisplay')){
                oliIdtoAttrsUpdateMap = ( Map<Id,Map<String,String>>)vlocity_cmt.FlowStaticMap.FlowMap.get('updateoliAttrs.updateNamedisplay');
             }
            
             /* Commenting Out as this is handled in Checkout
            // Start: Added by Adharsh for Autopopulate MRC NRC attributes on UI
             if((oliIdtoAttrsUpdateMap == null || oliIdtoAttrsUpdateMap.Size() == 0 || oliIdtoAttrsUpdateMap.isEmpty()) && vlocity_cmt.FlowStaticMap.FlowMap.ContainsKey('updateoliAttrs.updateManualMRCNRC')){
                oliIdtoAttrsUpdateMap = ( Map<Id,Map<String,String>>)vlocity_cmt.FlowStaticMap.FlowMap.get('updateoliAttrs.updateManualMRCNRC');
             }
             //End */
             // Get the Available pbe list from the OrdrProductEligibilityManager class if the Custom Setting is ON
                if(!Test.isRunningTest() && callProductEligibilityManager != null && callProductEligibilityManager.equalsIgnoreCase('ON') ){
                    
                    //System.debug('Calling Eligibility Manager...');
                    availablePriceBookEntryMap = OrdrProductEligibilityManager.getAvailableChildOffers(OrderId, new List<String>(ncidSet));
                    system.debug('availablePriceBookEntryMap -- '+availablePriceBookEntryMap);
                   // COMMENTING AS THE CURRENT LOGIC CALLS ELIGIBILITY ONLY ONCE PER REQUEST
                    /*if(topOfferToAvailablePbeMap != null && topOfferToAvailablePbeMap.containsKey(topOffer.id)){
                        availablePriceBookEntryMap =topOfferToAvailablePbeMap.get(topOffer.id); 
                    }
                    else 
                    
                    if(topOfferToAvailablePbeMap == null || !topOfferToAvailablePbeMap.containsKey(topOffer.id)){

                         System.debug('Calling Eligibility Manager...');
                        availablePriceBookEntryMap = OrdrProductEligibilityManager.getAvailableChildOffers(OrderId, new List<String>(ncidSet));
                        //topOfferToAvailablePbeMap.put(topOffer.id,availablePriceBookEntryMap);
                       
                    } */
                     //COMMENT ENDS
                }
               
             if (availablePriceBookEntryMap != null && availablePriceBookEntryMap.size() >0 )
                 System.debug('availablePriceBookEntryIDS_____'+availablePriceBookEntryMap.Keyset());
            

        // ITERATE OVER TOP OFFERS TO REMOVE AVAILABILITY NODES FROM THE JSON RESPONSE AND TO SET EXCLUDE FLAG
            for(orderItem topOffer: topOfferMap.values()){
               
                NCID = topOffer.ProductExternalId__c;
                //System.debug('NCID' + NCID + 'OrderId' + OrderId);
                vlocity_cmt.JSONResult results = (vlocity_cmt.JSONResult)output.get(vlocity_cmt.JSONResult.JSON_KEY_RESULT);
                
                if(null !=  results && null != results.records ){
                    nodeCounter = 0;
                    grandChildNodeCounter.clear();
                    indexToOrderIDMap.clear();
                    integer topOfferCount = 0;
                    List<vlocity_cmt.JSONRecord> childRecordsList = (List<vlocity_cmt.JSONRecord>)results.records;
                    topOfferCount = getTopofferNode(childRecordsList,topOffer);
                    //System.debug('topOfferCount222_______' + topOfferCount);
                    if(childRecordsList.size() > topOfferCount){
                        vlocity_cmt.JSONRecord childRecords =  childRecordsList[topOfferCount];
                        Map<String,vlocity_cmt.JSONResult>chiprodMap  =  (Map<String,vlocity_cmt.JSONResult>)childRecords.nameResult;
                        // Start: Added by Adharsh for Exclude/Requires  Rules on TopOffer
         
                          if(prodToRelatedProdMap != null && prodToRelatedProdMap.ContainsKey(topOffer.vlocity_cmt__Product2Id__r.id) ){
                                  childRecords.fields.put('excludeFlag',String.join(prodToRelatedProdMap.get(topOffer.vlocity_cmt__Product2Id__r.id),','));
                              }
                          else if(relatedProdToProdMap != null && relatedProdToProdMap.ContainsKey(topOffer.vlocity_cmt__Product2Id__r.id) ){
                                  childRecords.fields.put('excludeFlag',String.join(relatedProdToProdMap.get(topOffer.vlocity_cmt__Product2Id__r.id),','));
                            }
                            if(requiresProdRelIdMap.ContainsKey(topOffer.vlocity_cmt__Product2Id__r.id) ){
                                    childRecords.fields.put('requireFlag',requiresProdRelIdMap.get(topOffer.vlocity_cmt__Product2Id__r.id));
                                }
 
                          //End: Top offer Exclude/Requires Logic End
                            if(oliIdtoAttrsUpdateMap != null && oliIdtoAttrsUpdateMap.size()>0 && !oliIdtoAttrsUpdateMap.isEmpty()){
                                    updateOliAttrsJSONValues(childRecords,oliIdtoAttrsUpdateMap);
                                }
                               // Iterate over the PCI Node of JSONResult to strip off the non Available products 
                               getEligibleProductsJSON(chiprodMap, availablePriceBookEntryMap,OrderId,topOffer);

                               // Iterate over the grand childs for check PCIs Availaibility
                               getChildItemsAvailaiblity(chiprodMap,topOffer, availablePriceBookEntryMap);
                                getChildItemsiterationCount = 0 ;
                                indexToOrderIDMap.clear();

                               // Iterate over the ProductGroup node to strip off PCIs and run Requires and Exclude on Line Items
                               getProductGroupAvailaiblity(chiprodMap,topOffer, availablePriceBookEntryMap);

                              // Iterate over the Key Item Node of the JSONResult to Stamp the Exclude flag
                               setExcludeFlagOnLineItems(chiprodMap, topOffer, availablePriceBookEntryMap);
                         
                          }
                      }
            }
        }
    }catch(exception ex){
            System.debug(LoggingLevel.ERROR, 'Exception is '+ex);
            System.debug(LoggingLevel.ERROR, 'Exception stack trace '+ex.getStackTraceString());
            throw ex;
            //return false;     
        }

    }

    public  void getEligibleProductsJSON(Map<String,vlocity_cmt.JSONResult>chiprodMap, Map<id,priceBookEntry> availablePriceBookEntryMap , String OrderID,OrderItem topOffer ) {
        try {
       
       // Logic to Generate Eligible PCIs 
       // Here we gerenate a MAP of non available PCIs and delete them from the JSON Node
            if(chiprodMap.size() > 0 && !chiprodMap.isEmpty()){
                indexToOrderIDMap =  getChildhierarchyAvailaiblity(chiprodMap,'0',availablePriceBookEntryMap,topOffer);   
               
                List<String> unReqNodesList = new List<String>();
                for(String indexToRemove: indexToOrderIDMap.keySet() ){
                        unReqNodesList.add(indexToRemove);
                    }
                unReqNodesList.sort();
                List<Integer> finalList = new List<Integer>();
                for(String index: unReqNodesList)
                {
                    List<String> indexes = index.split('\\.'); 
                    integer indexsize = indexes.size();
                    integer indexToRemove = indexes.size()>0? integer.valueof(indexes[indexsize - 1]): null;
                    finalList.add(indexToRemove);
                }
               
                if(finalList != null && finalList.size() > 0 && !finalList.isEmpty() ){
                    system.debug('>>>>> Delete entry ' + finalList);
                    finalList.sort();
                    for(Integer i = finalList.size()-1; i>=0;i--){      
                        integer indexToRemove = finalList.get(i);
                         system.debug('>>>>> indextoremove' + indexToRemove);
                        if(indexToRemove != null){
                            deleteNonAvailableJSONNode(chiprodMap,indexToRemove,0 );
                        }
                    }
                }
            }
            system.debug('Exitingg......' );
        }catch(exception e){
            System.debug(LoggingLevel.ERROR, 'Exception is '+e);
                 System.debug(LoggingLevel.ERROR, 'Exception stack trace '+e.getStackTraceString());
        }   
    }

    
    public  integer getTopofferNode( List<vlocity_cmt.JSONRecord> childRecordsList,OrderItem topOffer) {
        integer topOfferCount = 0;
        if(childRecordsList != null && childRecordsList.size()>0){
            
            for(vlocity_cmt.JSONRecord cRecords: childRecordsList ){
                    Map<String,Object> fields = (Map<String,Object>)cRecords.fields;
                    if(fields.Containskey('PricebookEntry') ){
                        PricebookEntry pbe = (PricebookEntry)fields.get('PricebookEntry');
                        if(pbe.Id == topOffer.PricebookEntryId){
                            break;
                        }
                        topOfferCount++;
                    }else if(!fields.Containskey('PricebookEntry'))
                        break;
                }
            }
        return topOfferCount;

    }

  public static Map<String, OrderItem> getTopoffer(String OrderId) {
    Map<String, OrderItem> topOfferMap = new Map<String, OrderItem>();
    if (OrderId != null && OrderId != '') {

      for (OrderItem ori : [SELECT id, vlocity_cmt__Product2Id__r.orderMgmtId__c, vlocity_cmt__Product2Id__r.id, vlocity_cmt__Product2Id__r.Name, vlocity_cmt__Product2Id__r.VLAdditionalConfigData__c,
      vlocity_cmt__Product2Id__r.ProductSpecification__r.Name , ProductExternalId__c, Running_Count__c, OrderID,
      vlocity_cmt__Product2Id__c, PricebookEntryId, vlocity_cmt__JSONAttribute__c, vlocity_cmt__LineNumber__c
      from orderItem where OrderId = :OrderId ] ) {
        Map<String, Object> vlConfigData  = new  Map<String, Object>();
        if (ori.vlocity_cmt__Product2Id__r.VLAdditionalConfigData__c != null)
          vlConfigData = (Map<String, Object>)JSON.deserializeUntyped(ori.vlocity_cmt__Product2Id__r.VLAdditionalConfigData__c);
        if (ori.vlocity_cmt__LineNumber__c != null && ! String.valueOf(ori.vlocity_cmt__LineNumber__c).Contains('.')) 
        {
          topOfferMap.put(ori.id, ori);
        }

        if (vlConfigData != null && vlConfigData.size() > 0 && !vlConfigData.isEmpty() && vlConfigData.containsKey('Line Type') && String.valueof(vlConfigData.get('Line Type')).equalsIgnoreCase('Overline')) 
        {
          //topOfferMap.put(ori.vlocity_cmt__Product2Id__c, ori);
          overlineProd2OliId.put(ori.vlocity_cmt__Product2Id__r.id, ori.id);
        }
      }
    }
    return topOfferMap;
  }

//*** This Method iterates over the PCIs to identify the Non available PCI and generates a Map of the Same *****//
public  Map<String,object> getChildhierarchyAvailaiblity(Map<String,vlocity_cmt.JSONResult>chiprodMap, String parentIndex, Map<id,priceBookEntry> availablePriceBookEntryMap, OrderItem topOffer){
   
            System.Debug('OutsideLooppp------->'+parentIndex );    
            if(chiprodMap.containskey(vlocity_cmt.JSONResult.JSON_KEY_CHILDPRODUCTS_RESULT)){
                vlocity_cmt.JSONResult childProductResult = (vlocity_cmt.JSONResult)chiprodMap.get(vlocity_cmt.JSONResult.JSON_KEY_CHILDPRODUCTS_RESULT);
                List<vlocity_cmt.JSONRecord> childOffers = childProductResult.records;
                
                Set<String> indexesToRemove = new Set<String>();
                    integer i = 0;  
                for(vlocity_cmt.JSONRecord oli: childOffers){

                    String index = parentIndex + '.'+ String.valueof(i);
                  if(oli.fields.containskey('Id')) {
                    Map<String, Object> pbeNameMap = (Map<String,Object>)oli.fields.get('Name');
                    String pbeName = (String)pbeNameMap.get('value');

                   system.debug('Olids___ ' + pbeName);
                   Map<String,Object> priceBookEntryIdField = (Map<String,Object>)oli.fields.get('Id');
                   String priceBookEntryIdValue = (String)priceBookEntryIdField.get('value');
                   Product2 product2Details = (Product2) oli.fields.get('Product2');
                    String productSpec = product2Details.ProductSpecification__r.Name != null ?  product2Details.ProductSpecification__r.Name : '';
                    String prodRelationShipType = product2Details.RelationshipType__c != null ? product2Details.RelationshipType__c  : '';
                   
                    Boolean isSuperUserPromos = false;
                    if(product2Details.OCOM_Promotion_Applies_To__c != null)
                       isSuperUserPromos = string.Valueof(product2Details.OCOM_Promotion_Applies_To__c).equalsIgnoreCase('Super User' )? true: false;
                    //system.debug('superUserPromos______________' + isSuperUserPromos + '------'+prodRelationShipType + '________' + product2Details.Name + 'ID::' + priceBookEntryIdValue);
                    if(availablePriceBookEntryMap.size() > 0 && !availablePriceBookEntryMap.isEmpty() && !availablePriceBookEntryMap.containsKey(priceBookEntryIdValue)){
                            if( excludeWirelssProductSet.contains(productSpec.toLowerCase())){
                                i++;
                                continue;
                            }
                            else if(prodRelationShipType.equalsIgnoreCase('Promotion') && !isSuperUserPromos){
                                system.debug('Continue Promosss');
                                i++;
                                continue;
                            }
                            else if( isSuperUserPromos && isSuperUser) {
                                i++;
                                continue;
                            }
                            else{ 
                             String checkParentNode = index.subString(0,index.length()-2);
                             if(!indexToOrderIDMap.containskey(checkParentNode))
                                indexToOrderIDMap.put(index,new pricebookEntry(id= priceBookEntryIdValue));
                               
                            }  
                           }
                            if(oli.fields.Containskey('Product2Id')){
                                Map<String,Object> product2IDField = (Map<String,Object>)oli.fields.get('Product2Id');
                                String prod2IdValue = (String)product2IDField.get('value');
                                // Start:Exclude Flag Stamp Logic on PCIs
                                    if(prodToRelatedProdMap.ContainsKey(prod2IdValue) ){
                                                system.debug('Exclude flagg....' + prod2IdValue);
                                                oli.fields.put('excludeFlag',String.join(prodToRelatedProdMap.get(prod2IdValue),','));
                                            }
                                    else if(relatedProdToProdMap.ContainsKey(prod2IdValue) ){
                                       oli.fields.put('excludeFlag',String.join(relatedProdToProdMap.get(prod2IdValue),','));
                                       system.debug('Exclude flag added for product::' + prod2IdValue);
                                    } 
                                //End of Exclude Logic
                                //Start: Requires Rule Logic on PCIs
                                if(requiresProdRelIdMap.ContainsKey(prod2IdValue) ){
                                                system.debug('Require flagg....' + requiresProdRelIdMap.get(prod2IdValue));
                                                oli.fields.put('requireFlag',requiresProdRelIdMap.get(prod2IdValue));
                                            }
                                // End: Requires Rule Logic        
                            }   
                    
                        Map<String,vlocity_cmt.JSONResult> childObjdetails = (Map<String,vlocity_cmt.JSONResult>)oli.nameResult;
                  
                    }
                      i++;
                  }
              }
    return indexToOrderIDMap;
}


// ***** This Method Iterates over the KeyItems( OLI ) Nodes and generates a map of Nodes to Delete **************//


public void getChildItemsAvailaiblity(Map<String,vlocity_cmt.JSONResult>chiprodMap , OrderItem topOffer, Map<id,priceBookEntry> availablePriceBookEntryMap ){

//System.debug(' Child Line Items  Availability Check...');
    if(chiprodMap != null && chiprodMap.containskey(vlocity_cmt.JSONResult.JSON_KEY_LINEITEMS_RESULT)){
            if(getChildItemsiterationCount == 0 )
                indexToOrderIDMap.clear();
            vlocity_cmt.JSONResult childProductResult = (vlocity_cmt.JSONResult)chiprodMap.get(vlocity_cmt.JSONResult.JSON_KEY_LINEITEMS_RESULT);
            List<vlocity_cmt.JSONRecord> childOffers = childProductResult.records;
           // system.debug('Lineitems.....' + childOffers);
           linetemHirerachy = linetemHirerachy +'--->' + String.Valueof(getChildItemsiterationCount);
           system.debug('linetemHirerachy_____' +linetemHirerachy);
            for(vlocity_cmt.JSONRecord oli: childOffers){
                // Check for Overline and run the Avaliability Check logic for the same 
                                        
                if(oli.fields.Containskey('Product2Id')){
                    nodeCounter = 0;
                    grandChildNodeCounter.clear();
                    indexToOrderIDMap.clear();
                    String prod2IdValue = (String)oli.fields.get('Product2Id');
                    //system.debug('overline pci Ids_____' + prod2IdValue);   
                        Map<String,vlocity_cmt.JSONResult>overlineChiprodMap  =  (Map<String,vlocity_cmt.JSONResult>)oli.nameResult;
                       
                        String OrderId = topOffer.OrderId;
                        getEligibleProductsJSON(overlineChiprodMap, availablePriceBookEntryMap,OrderId,topOffer);
                        system.debug('Oveline pci stripped off product22....');
                    }

                     Map<String,vlocity_cmt.JSONResult> childObjdetails = (Map<String,vlocity_cmt.JSONResult>)oli.nameResult;
                     getChildItemsiterationCount++;
                    // Iterate over grand child to set the Exclude Flag....
                     getChildItemsAvailaiblity(childObjdetails, topOffer, availablePriceBookEntryMap);         
                }
            }
     }


//*** This Method iterates over the Product Group Node to identify the Non available PCIs aand generates a Map of the Same *****//

// ***** This Method Iterates over the KeyItems(OLI)  Nodes and generates a map of Nodes to Delete **************//


public void getProductGroupAvailaiblity(Map<String,vlocity_cmt.JSONResult>chiprodMap , OrderItem topOffer, Map<id,priceBookEntry> availablePriceBookEntryMap ){

//System.debug(' Child Line Items  Availability Check...');
    if(chiprodMap != null && chiprodMap.containskey(vlocity_cmt.JSONResult.JSON_KEY_PRODUCTGROUPS_RESULT)){
            if(getChildItemsiterationCount == 0 )
                indexToOrderIDMap.clear();
            vlocity_cmt.JSONResult childProductResult = (vlocity_cmt.JSONResult)chiprodMap.get(vlocity_cmt.JSONResult.JSON_KEY_PRODUCTGROUPS_RESULT);
            List<vlocity_cmt.JSONRecord> childOffers = childProductResult.records;
           // system.debug('Lineitems.....' + childOffers);
           linetemHirerachy = linetemHirerachy +'--->' + String.Valueof(getChildItemsiterationCount);
           system.debug('linetemHirerachy_____' +linetemHirerachy);
            for(vlocity_cmt.JSONRecord rec: childOffers){
                // Check for Overline and run the Avaliability Check logic for the same 
                                        
                if(rec.fields.containskey('Id')) {
                    Map<String, Object> pbeNameMap = (Map<String,Object>)rec.fields.get('Name');
                    String pbeName = (String)pbeNameMap.get('value');
                    System.debug('pbeName_+++____'+pbeName);

                   Map<String,Object> priceBookEntryIdField = (Map<String,Object>)rec.fields.get('Id');
                   String priceBookEntryIdValue = (String)priceBookEntryIdField.get('value');
                   Product2 product2Details = (Product2) rec.fields.get('Product2');
                    String productSpec = product2Details.ProductSpecification__r.Name != null ?  product2Details.ProductSpecification__r.Name : '';
                    String prodRelationShipType = product2Details.RelationshipType__c != null ? product2Details.RelationshipType__c  : '';
                   
                    nodeCounter = 0;
                    grandChildNodeCounter.clear();
                    indexToOrderIDMap.clear();
                    //system.debug('overline pci Ids_____' + overlineProd2OliId.keySet());   
                        Map<String,vlocity_cmt.JSONResult>prodGroupPCIMap  =  (Map<String,vlocity_cmt.JSONResult>)rec.nameResult;
                       
                        String OrderId = topOffer.OrderId;
                        getEligibleProductsJSON(prodGroupPCIMap, availablePriceBookEntryMap,OrderId,topOffer);
                        //system.debug('Oveline pci stripped off product22....' );
                    }

                     Map<String,vlocity_cmt.JSONResult> childObjdetails = (Map<String,vlocity_cmt.JSONResult>)rec.nameResult;
                     getChildItemsiterationCount++;
                    // Iterate over grand child to set the Exclude Flag....
                     getChildItemsAvailaiblity(childObjdetails, topOffer, availablePriceBookEntryMap);         
                }
            }
     }




// ***** This Method Iterates over the JSONResult and Removes the Non available nodes **************//

public  void   deleteNonAvailableJSONNode (Map<String,vlocity_cmt.JSONResult>chiprodMap, Integer parentIndex, integer childNodeCounter){
        System.Debug('Deeleting Node------->'+parentIndex ); 
        String nodeTypeToIterate = vlocity_cmt.JSONResult.JSON_KEY_CHILDPRODUCTS_RESULT;
        
     //Delete the node if there are no grand children.
        if(parentIndex != null && chiprodMap != null && chiprodMap.containskey(nodeTypeToIterate) ){
                 vlocity_cmt.JSONResult childRecMap = (vlocity_cmt.JSONResult)chiprodMap.get(nodeTypeToIterate);
                List<vlocity_cmt.JSONRecord> childOffers = childRecMap.records;
                system.debug('Lineitems.....' + childOffers.size());
                Integer StringrootIndex = parentIndex; //Integer.valueof(indexesToTraverse[0]);
              if(childOffers != null && childOffers.size() >= StringrootIndex){
                if( childNodeCounter == 0){
                    StringrootIndex = StringrootIndex - nodeCounter;
                }
                else if(childNodeCounter > 0){
                    StringrootIndex = StringrootIndex - (childNodeCounter -1);
                }
               // System.debug('Removing the Node>>>>>>>'+ StringrootIndex);
                childOffers.remove(StringrootIndex);
              }
              
            }
        }

// ********* Method to Iterate over the Key LineItems and Set the Exclude Flag and Requires FLag **********************//

 public void setExcludeFlagOnLineItems(Map<String,vlocity_cmt.JSONResult>chiprodMap , OrderItem topOffer, Map<id,priceBookEntry> availablePriceBookEntryMap ){

    if(chiprodMap != null && chiprodMap.containskey(vlocity_cmt.JSONResult.JSON_KEY_LINEITEMS_RESULT)){
            vlocity_cmt.JSONResult childProductResult = (vlocity_cmt.JSONResult)chiprodMap.get(vlocity_cmt.JSONResult.JSON_KEY_LINEITEMS_RESULT);
            List<vlocity_cmt.JSONRecord> childOffers = childProductResult.records;
           // system.debug('Lineitems.....' + childOffers.size());

            for(vlocity_cmt.JSONRecord oli: childOffers){
                if(oli.fields.Containskey('Product2Id')){
                    String prod2IdValue = (String)oli.fields.get('Product2Id');                    
                        if(prodToRelatedProdMap != null && prodToRelatedProdMap.ContainsKey(prod2IdValue) ){
                                //system.debug('Exclude flag added for product22::' + prodToRelatedProdMap.get(prod2IdValue));
                                oli.fields.put('excludeFlag',String.join(prodToRelatedProdMap.get(prod2IdValue),','));
                            }
                        else if(relatedProdToProdMap != null && relatedProdToProdMap.ContainsKey(prod2IdValue) ){
                                //system.debug('Exclude flag added for product33::' + relatedProdToProdMap.get(prod2IdValue));
                                oli.fields.put('excludeFlag',String.join(relatedProdToProdMap.get(prod2IdValue),','));
                        }
                        //Start: Requires Rule Logic on PCIs
                        if(requiresProdRelIdMap.ContainsKey(prod2IdValue) ){
                                                system.debug('Require flagg....' + requiresProdRelIdMap.get(prod2IdValue));
                                                oli.fields.put('requireFlag',requiresProdRelIdMap.get(prod2IdValue));
                          }
                        // End: Requires Rule Logic  
                    }
                // Check for Overline and run the Avaliability Check logic for the same 
                                        
                if(oli.fields.Containskey('Product2Id')){
                    nodeCounter = 0;
                    grandChildNodeCounter.clear();
                    indexToOrderIDMap.clear();
                    String prod2IdValue = (String)oli.fields.get('Product2Id');
                   // system.debug('overline pci Ids_____' + overlineProd2OliId.keySet());
                    if(overlineProd2OliId != null && overlineProd2OliId.ContainsKey(prod2IdValue) ){
                        
                        Map<String,vlocity_cmt.JSONResult>overlineChiprodMap  =  (Map<String,vlocity_cmt.JSONResult>)oli.nameResult;
                     
                        String OrderId = topOffer.OrderId;
                       // getEligibleProductsJSON(overlineChiprodMap, availablePriceBookEntryMap,OrderId,topOffer);
                        system.debug('Oveline pci stripped off product22....');
                    }
                }

                     Map<String,vlocity_cmt.JSONResult> childObjdetails = (Map<String,vlocity_cmt.JSONResult>)oli.nameResult;
                    // Iterate over grand child to set the Exclude Flag....
                     setExcludeFlagOnLineItems(childObjdetails, topOffer, availablePriceBookEntryMap);         
                }
            }
     }

// ***************Method to Iterate over the Attribute Categories Result Node to set the required JSON Attrs Runtime value.***********
// Note these changes were only be reflected in UI and not on Oli JSON. For Oli JSON changes refer to OCOM_UpdateOliAttributes class

public static void updateOliAttrsJSONValues (vlocity_cmt.JSONRecord record, Map<Id,Map<String,String>> oliIdtoAttrsUpdateMap) {
        //Map<Id,Map<String,String>> oliIdtoAttrsUpdateMap = new Map<Id,Map<String,string>>();
        String currentOliId ;
         if(record.fields.containskey('Id')) {
                  vlocity_cmt.JSONField oliIdfield = (vlocity_cmt.JSONField )record.fields.get('Id');
                    currentOliId = (String)oliIdfield.value;
               }
        if(oliIdtoAttrsUpdateMap != null && currentOliId != null && oliIdtoAttrsUpdateMap.containsKey(currentOliId) && record.nameResult.get(vlocity_cmt.JSONResult.JSON_KEY_ATTRIBUTE_CATEGORIES_RESULT) != null){
            Map<String,String> attrsValuetoUpdateMap = oliIdtoAttrsUpdateMap.get(currentOliId);
            system.debug('attrsValuetoUpdateMap__' +attrsValuetoUpdateMap);
            vlocity_cmt.JSONResult result = record.nameResult.get(vlocity_cmt.JSONResult.JSON_KEY_ATTRIBUTE_CATEGORIES_RESULT);
            for (Integer index = result.records.size(); index > 0; index--){
                vlocity_cmt.JSONRecord jsonAttrsRecNode = result.records[index - 1 ];
                if(jsonAttrsRecNode != null && jsonAttrsRecNode.nameResult.containskey(vlocity_cmt.JSONResult.JSON_KEY_ATTRIBUTES_RESULT) && jsonAttrsRecNode.nameResult.get(vlocity_cmt.JSONResult.JSON_KEY_ATTRIBUTES_RESULT) != null){
                    vlocity_cmt.JSONResult jsonProdAttrnodeResult = jsonAttrsRecNode.nameResult.get(vlocity_cmt.JSONResult.JSON_KEY_ATTRIBUTES_RESULT);
                    for (Integer attrIndex = jsonProdAttrnodeResult.records.size(); attrIndex > 0; attrIndex--){
                        vlocity_cmt.JSONRecord jsonAttrsWrapper = jsonProdAttrnodeResult.records[attrIndex - 1 ];
                        vlocity_cmt.JSONAttribute jsonAttrsValues = (vlocity_cmt.JSONAttribute)jsonAttrsWrapper;
                        if(attrsValuetoUpdateMap != null && attrsValuetoUpdateMap.containsKey(jsonAttrsValues.label)){
                            String attrValueToupdate = attrsValuetoUpdateMap.get(jsonAttrsValues.label);
                            system.debug('Attr Name ---> ' + jsonAttrsValues.label);
                            system.debug('Attr Value ---> ' + attrValueToupdate);
                            vlocity_cmt.JSONAttribute.JSONUserValues  jsonuserVal = jsonAttrsValues.userValues;
                            vlocity_cmt.JSONAttribute.JSONSingleUserValue jsonSingleuserVal = (vlocity_cmt.JSONAttribute.JSONSingleUserValue)jsonuserVal;
                            jsonSingleuserVal.value = attrValueToupdate;
                            vlocity_cmt.JSONAttribute.JSONValue jsonvalue = jsonAttrsValues.values[0];
                           jsonvalue.value = attrValueToupdate;

                        }
                    }
                }

            }

        }
        else if (record.nameResult.get(vlocity_cmt.JSONResult.JSON_KEY_LINEITEMS_RESULT) != null) {
            vlocity_cmt.JSONResult result = record.nameResult.get(vlocity_cmt.JSONResult.JSON_KEY_LINEITEMS_RESULT);
            system.debug('Iterate over child lineitems...');
            for (Integer index = result.records.size(); index > 0; index--) {
                  system.debug('Iterate over child lineitems...');
                vlocity_cmt.JSONRecord childRecords = result.records[index - 1 ];
                updateOliAttrsJSONValues(childRecords, oliIdtoAttrsUpdateMap);
            }
    
        }


    }

}