/**
 * An apex page controller that exposes the site login functionality
 */
public class TelusPRMSiteLoginController {
    public String username {get; set;}
    public String password {get; set;}
    public String errorMsg {get; set;}
    public Boolean errFlag = false;
    public String english {get; set;}
    private Map<String, String> efMap = new Map<String, String>{    
    		'Enter a value in the User Name field.' => 'Veuillez entrer votre nom d’utilisateur.',
    		'Enter a value in the Password field.' => 'Veuillez entrer votre mot de passe.',
    		'Your login attempt has failed. Make sure the username and password are correct.' => 'Votre tentative de connexion a échoué. Le nom d’utilisateur ou le mot de passe que vous avez saisi est incorrect.'    		
    };

	public Boolean getErrFlag(){
		return errFlag;
	}

    public PageReference login() {
        String startUrl = System.currentPageReference().getParameters().get('startURL');
        System.debug('Start url = ' + startURL);
        String prefix = Site.getPrefix();
        //System.debug(efMap);
        if(prefix != null){
            startURL = (startURL == null) ? prefix + '/home/home.jsp' : startURL;
        }else{
            startURL = (startURL == null) ? '/home/home.jsp' : startURL;
        }

       errorMsg = '';
        PageReference p = Site.login(username, password, startUrl);
        Boolean isEnglish = english.equals('true');
        if ( p == null) {
            if (ApexPages.hasMessages()) {
            		errFlag = true;            		
            		ApexPages.Message[] msg = ApexPages.getMessages();
                    for (ApexPages.Message m : msg) {
                        if (efMAp.get(m.getDetail()) != null) {
                            errorMsg += efMAp.get(m.getDetail());
                        }                        
                    }
            }

        } 
        return p;
    }
    
   	public PageReference loginEn() {
        String startUrl = System.currentPageReference().getParameters().get('startURL');
        System.debug('Start url = ' + startURL);
        String prefix = Site.getPrefix();
        if(prefix != null){
            startURL = (startURL == null) ? prefix + '/home/home.jsp' : startURL;
        }else{
            startURL = (startURL == null) ? '/home/home.jsp' : startURL;
        }
        PageReference p = Site.login(username, password, startUrl);
        return p;       
    }
    
    public TelusPRMSiteLoginController () {
    	english = 'true';
    }
       
}