public class OCOM_PAC_MACDContactExt {

    //public OCOM_PAC_MACD_Controller controllerContRef;
    //Updated by Arvind as part of US-1138
    public OCOM_InOrderFlowContactController controllerContRef;
    
    /* Start Capture contact Code */
    public OCOM_PAC_MACD_Controller MacdcontrollerRef;
    public Boolean loaded {get; private set;}
    public String searchTerm {get; set;}
    public Contact[] contacts {get; private set;}
    public Contact new_contact {get; set;}
    public Integer contactsCount {get {if(contacts == null){return 0;} return contacts.size();}}
    public Boolean showNewContactForm {get; private set;}
    public static String selectedContactId{get;set;}
    public Id mostRecentlyCreatedContactId {get; private set;}
    public String errorMessage {get;set;}
    public Account account {get; set;}
    /* End Capture contact Code */

    public OCOM_PAC_MACDContactExt(OCOM_InOrderFlowContactController controller) {
        this.controllerContRef=controller;
    }
    
    public OCOM_PAC_MACDContactExt(OCOM_PAC_MACD_Controller controller) {
        this.MacdcontrollerRef=controller;
    }
    
    
    /* Start Capture contact Code */
    private void clear() {
        account = null;
        contacts= null;
    }

    // OnLoad method invoked on click of Move & VF page load.
    public void onLoad() {

        loaded = false;
        showNewContactForm = false;
            
        // Added by Wael - Start OCOM-1207
        String src = ApexPages.currentPage().getParameters().get('src')==null?'':ApexPages.currentPage().getParameters().get('src');
        String orderId = ApexPages.currentPage().getParameters().get('OrderId'); 
        // Wael - End OCOM-1207
            
        /*string currentUrl =  ApexPages.currentPage().getUrl();
        system.debug('OrderID___' + orderId +'_URL______'+ currentUrl);
        if(orderId != null && orderId != ''){
            Cache.Session.put(orderId, currentUrl);    
            
        }*/
            
            
        if(src!='os'){     // Added by Wael - Start OCOM-1207, the body inside the if already existed before the changes. Wael added the else.
            try {
                if (ApexPages.currentPage() == null || ApexPages.currentPage().getParameters() == null){
                    clear();
                }
                String aid = ApexPages.currentPage().getParameters().get('id');
                if (aid == null || aid == '' || aid.substring(0, 3) != '001') {
                    clear();
                }
                Id xid = (Id) aid;
                account = [Select Name, Owner.Name,parentid From Account Where Id = :xid Limit 1];
                if (account == null){
                    clear();
                }
                loadContacts();
                if (contacts == null || contacts.size() == 0) { showNewContactForm = true; }
                new_contact = new Contact(AccountId = account.parentId);
            }
            catch (Exception e){
                clear();
            }
            loaded = true;
            // Added by Wael - Start OCOM-1207    
        } else {
            try {
                Order theOrder = [SELECT Id, Service_Address__c, Service_Address__r.Id, Service_Address__r.Service_Account_Id__c, Service_Address__r.Service_Account_Id__r.Id, Service_Address__r.Service_Account_Id__r.ParentId, CustomerAuthorizedBy.Id FROM Order WHERE Id=: orderId];
                
                if (ApexPages.currentPage() == null || ApexPages.currentPage().getParameters() == null){
                    clear();
                }
                String aid = ApexPages.currentPage().getParameters().get('id');
                if (aid == null || aid == '' || aid.substring(0, 3) != '001') {
                    clear();
                }
                    
                Id xid = theOrder.Service_Address__r.Service_Account_Id__c;
                ApexPages.currentPage().getParameters().put('contactId', theOrder.CustomerAuthorizedBy.Id);
                ApexPages.currentPage().getParameters().put('smbCareAddrId', theOrder.Service_Address__r.Id);
                ApexPages.currentPage().getParameters().put('ServiceAcctId', theOrder.Service_Address__r.Service_Account_Id__r.Id);
                ApexPages.currentPage().getParameters().put('ParentAccId', theOrder.Service_Address__r.Service_Account_Id__r.ParentId);
                
                account = [Select Name, Owner.Name,parentid From Account Where Id = :xid Limit 1];
                if (account == null){
                    clear();
                }
                
                loadContacts();
                if (contacts == null || contacts.size() == 0) { showNewContactForm = true; }
                new_contact = new Contact(AccountId = account.parentId);
            }
            catch (Exception e){
                clear();
            }
            loaded = true;
        }
        // Wael - End OCOM-1207
    }
    
    private void loadContacts(){
        system.debug('@@account.Id'+account.parentId);
        contacts = [Select Name, Title, Phone, Email, Active__c,LastModifiedDate  From Contact Where AccountId = :account.parentId Order By Active__c DESC, CreatedDate DESC limit 50];
        system.debug('@@Contacts'+contacts);
    }
    
    public PageReference onSelectContact() {
        errorMessage = '';
        //flagErrorContact = false;

         if (selectedContactId == null ){
             //  throw new NewQuoteException('You must select a valid contact to create a quote.');
               return null;
         }

         return null;
    }

    public PageReference cancelNewContact(){
        // new_contact = new Contact(AccountId = account.parentId,FirstName='firstname',LastName='lastname',Phone='0123456789',Title='Mr',Email='test@test.com',Language_Preference__c='English');
        showNewContactForm = !showNewContactForm;
        system.debug('Inside contact '+showNewContactForm );

        return null;
    }
    
    public PageReference toggleNewContactForm(){
        new_contact = new Contact(AccountId = account.parentId);
        showNewContactForm = !showNewContactForm;
        system.debug('Inside contact '+showNewContactForm );

        return null;
    }

    public PageReference createNewContact() {
        system.debug('>>>>>> 0 ' + new_contact);
        ApexPages.standardController sc = new ApexPages.standardController(new_contact);
        sc.save();

        system.debug('>>>>>> '+new_contact.name +' sc.getId() '+sc.getId());
        if(sc.getId() == null) {
            system.debug('>>>>>> 1 '+sc);
            return null;
        }
        
        List<Contact> contact_list = [Select FirstName, LastName, Name, Email, Title, Phone From Contact Where Id = :sc.getId()];
        if(contact_list.size()>0){
        	new_contact = contact_list.get(0);
        }	
        
        if (new_contact.Id != null){
            system.debug('>>>>>> 2 '+new_contact);
            showNewContactForm = false;
            mostRecentlyCreatedContactId = new_contact.id;
            new_contact = new Contact(AccountId = account.parentId);
            loadContacts();
        }
        return null;
    }

    public void onSearch(){
        try {
            if (searchTerm == null || searchTerm == '') { 
                loadContacts(); 
                return; 
            }
            searchTerm = '%' + searchTerm + '%';
            selectedContactId='';
            // List<List<sObject>> searchResult = [FIND :searchTerm IN All FIELDS RETURNING Contact (name, email, phone, title, Active__c,LastModifiedDate   Where AccountId = :account.parentId) limit 50];
            List<Contact>searchResult =[select id, name, email, phone, title, Active__c,LastModifiedDate from Contact where AccountId =:account.parentId and (Name Like :searchTerm OR email Like :searchTerm OR phone Like :searchTerm) limit 50];
            // contacts = ((List<Contact>)searchResult[0]);
            contacts = searchResult;
        } catch (Exception e) {
        
        } 
        return;
    }

    /* End Capture contact Code */
}