/*
    *******************************************************************************************************************************
    Class Name:     OC_OrderPdfWrapperController
    Purpose:        Controller for Saving and Sending Order PDF       
    Test Class Name: OC_OrderPdfWrapperControllerTest
    Created by:    Sandip ,  27/10/2017
    *******************************************************************************************************************************
*/
public with sharing class OC_OrderPdfWrapperController 
{

    public Order o {get;set;}
    public static String choosenLanguage {get ; set ;}
    
    public OC_OrderPdfHelper pdfHelper {get;set;}
    
    /**
    Method: OC_OrderPdfWrapper(ApexPages.StandardController stdController) 
    This method is parameterized constructor which is initializing the current Order object
    and fetching records from Order object where Id is equal to current order Id 
    Inputs Args: ApexPages.StandardController
    Output Args: none
    **/
    public OC_OrderPdfWrapperController(ApexPages.StandardController stdController) 
    {
        this.o = (Order)stdController.getRecord();
        choosenLanguage = ApexPages.currentPage().getParameters().get('lang');
        if(o.id!=null)
            o = [select Id, Name, Status, OrderNumber, CustomerAuthorizedById,CustomerAuthorizedBy.Email  from Order where Id = :o.Id];
        pdfHelper = new OC_OrderPdfHelper(o.id, choosenLanguage ); 

    }
    
    //Added new parameterized constructor
    /**
    Requirement      : French-English Toggleing of PDF and mailers
    args             : ApexPages.StandardController, String
    Purpose          : Constructor of class
    **/
    /*public OC_OrderPdfWrapperController(ApexPages.StandardController stdController, String choosenLang) 
    {
        this.o = (Order)stdController.getRecord();
        if(choosenLang == null) choosenLanguage = ApexPages.currentPage().getParameters().get('lang');
        if(o.id!=null)
            o = [select Id, Name, Status, OrderNumber, CustomerAuthorizedById,CustomerAuthorizedBy.Email  from Order where Id = :o.Id];
        if(choosenLang != null || choosenLanguage != null){            
             pdfHelper = new OC_OrderPdfHelper(o.id, choosenLang); 
        }

    }*/


    /*
    Requirement      : French-English Toggleing of PDF and mailers
    args             : null
    Purpose          : Calls helper class for sending mail logic 
    ***********************/
    //public void callSendEmail(){
    //    pdfHelper.sendEmailWithLang(choosenLanguage);
    //}

}