/*
    ###########################################################################
    # File..................: TBOWorkRequestEmailComponent
    # Version...............: 1
    # Created by............: Umesh Atry (TechM)
    # Created Date..........: 17/02/2016 
    # Last Modified by......: Umesh Atry (TechM)
    # Last Modified Date....: 18/02/2016
    # Description...........: Class is to populate Case Account records based on selected work request.
    #
    ############################################################################
    */

public class TBOWorkRequestEmailComponent{
         public Id wrID {get;set;}
       // public string error {get;set;}    
        
        public TBOWorkRequestEmailComponent(){
            wrID  = Apexpages.currentPage().getParameters().get('Id');
        }   
    
        public Work_Request__c workRequest {get;set;}
       
        public List<Case_Account__c> CaseAcc {
        get{
         if(wrID!= null){
          workRequest = [Select id,Service_Type__c,case__r.casenumber from Work_Request__c  where id =:wrID];
                    system.debug('workRequest '+workRequest );
                    List<Case_Account__c> CaseAcc1 = new List<Case_Account__c>();
                    String picks = workRequest.Service_Type__c;                  
    
                    system.debug('picks =='+picks);
                    CaseAcc1 = [select id,Name,Acc_Current__c,CAN__c,BTN__c,Billing_System__c,Account__r.Name,Service_Type__c,case__r.Casenumber from Case_Account__c                    
                    where case__r.CaseNumber =:workRequest.case__r.casenumber and Service_Type__c INCLUDES (:picks) ] ;
                    
                    
                    system.debug(CaseAcc1.size());
                    return CaseAcc1;
                    }else return null;
                    }set;
        }
       
            
}