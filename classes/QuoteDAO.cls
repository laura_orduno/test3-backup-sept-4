/**
 * Data Access Object (DAO) helper class for loading Quote and related objects.
 * 
 * @author Max Rudman
 * @since 8/7/2010
 */
public class QuoteDAO extends AbstractDAO {
	public static final String DEFAULT_SALES_REP_ID_FIELD = 'DefaultSalesRepId__c';
	public static final String DEFAULT_GROUP_NAME_FIELD = 'DefaultGroupName__c';
	
    private static List<Schema.SObjectField> MINIMAL_QUOTE_FIELDS = new List<Schema.SObjectField>{
        SBQQ__Quote__c.Id, SBQQ__Quote__c.Name, SBQQ__Quote__c.SBQQ__Type__c, 
        SBQQ__Quote__c.SBQQ__CustomerDiscount__c, SBQQ__Quote__c.SBQQ__MarkupRate__c, SBQQ__Quote__c.SBQQ__Account__c,
        SBQQ__Quote__c.SBQQ__PartnerDiscount__c, SBQQ__Quote__c.SBQQ__SubscriptionTerm__c, SBQQ__Quote__c.SBQQ__ExpirationDate__c,
        SBQQ__Quote__c.SBQQ__StartDate__c, SBQQ__Quote__c.SBQQ__EndDate__c, SBQQ__Quote__c.SBQQ__MasterContract__c, SBQQ__Quote__c.SBQQ__QuoteProcessId__c
    };
    
    private static List<Schema.SObjectField> MINIMAL_GROUP_FIELDS = new List<Schema.SObjectField>{
        SBQQ__QuoteLineGroup__c.Name, SBQQ__QuoteLineGroup__c.SBQQ__Description__c, SBQQ__QuoteLineGroup__c.SBQQ__Number__c,
        SBQQ__QuoteLineGroup__c.SBQQ__ListTotal__c, SBQQ__QuoteLineGroup__c.SBQQ__NetTotal__c, 
        SBQQ__QuoteLineGroup__c.SBQQ__CustomerTotal__c, SBQQ__QuoteLineGroup__c.SBQQ__SubscriptionTerm__c,
        SBQQ__QuoteLineGroup__c.SBQQ__Quote__c, SBQQ__QuoteLineGroup__c.SBQQ__AdditionalDiscountRate__c,
        SBQQ__QuoteLineGroup__c.SBQQ__MarkupRate__c, SBQQ__QuoteLineGroup__c.SBQQ__StartDate__c,
        SBQQ__QuoteLineGroup__c.SBQQ__EndDate__c, SBQQ__QuoteLineGroup__c.SBQQ__Optional__c
    };
    
    private Schema.SObjectType quoteType;
    private Schema.SObjectType quoteLineType;
    
    public Boolean loadGroups {get; set;}
    public Boolean loadLines {get; set;}
    public List<Schema.SObjectField> quoteFields {get; set;}
    public List<Schema.SObjectField> groupFields {get; set;}
    public List<Schema.SObjectField> lineFields {get; set;}
    
    /**
     * Default constructor.
     */
    public QuoteDAO() {
    	loadGroups = true;
    	loadLines = true;
    	quoteFields = MINIMAL_QUOTE_FIELDS;
    	groupFields = MINIMAL_GROUP_FIELDS;
    }
    
    public QuoteVO loadByKey(String key) {
        QueryBuilder builder = createQuoteQuery();
        builder.getSelectClause().addExpression(createGroupSubQuery());
        builder.getWhereClause().addExpression(builder.eq(SBQQ__Quote__c.SBQQ__Key__c, key));
        List<QuoteVO> vos = loadInternal(builder);
        return vos.isEmpty() ? null : vos[0];
    }
    
    public List<QuoteVO> loadByIds(Set<Id> ids) {
    	if ((ids == null) || ids.isEmpty()) {
    		return new List<QuoteVO>();
    	}
        determineObjectType(new List<Id>(ids).get(0));
        QueryBuilder builder = createQuoteQuery();
        if (loadGroups && (quoteType == SBQQ__Quote__c.sObjectType)) {
            builder.getSelectClause().addExpression(createGroupSubQuery());
        }
        builder.getWhereClause().addExpression(builder.inExpr(SBQQ__Quote__c.Id, ids));
        return loadInternal(builder);
    }
    
    private List<QuoteVO> loadInternal(QueryBuilder builder) {
        List<SObject> quotes = Database.query(builder.buildSOQL());
        Map<Id,QuoteVO> vosById = new Map<Id,QuoteVO>();
        Set<Id> quoteIds = new Set<Id>();
        for (SObject quote : quotes) {
            QuoteVO vo = new QuoteVO((SBQQ__Quote__c)quote);
            if (loadGroups && (quoteType == SBQQ__Quote__c.sObjectType)) {
                SBQQ__Quote__c q = (SBQQ__Quote__c)quote;
                vo.setGroups( q.SBQQ__LineItemGroups__r);
            }
            vosById.put(quote.Id, vo);
            quoteIds.add(quote.Id);
        }
        
        if (quoteIds.isEmpty()) {
        	throw new LoadException('Unable to find quote. The record may have been deleted');
        }
        
        if (loadLines) {
	        Map<Id,QuoteVO.LineItem> linesById = new Map<Id,QuoteVO.LineItem>();
	        for (SObject line : loadLines(quoteIds)) {
	            QuoteVO qvo = vosById.get((Id)AliasedMetaDataUtils.getFieldValue(line, SBQQ__QuoteLine__c.SBQQ__Quote__c));
	            if (qvo != null) {
	                QuoteVO.LineItem item = qvo.addLineItem(line);
	                linesById.put(item.lineId, item);
	            }
	        }
	        
	        // Point component line items to the package line
	        for (QuoteVO vo : vosById.values()) {
	            for (QuoteVO.LineItem item : vo.lineItems) {
	                if (item.isComponent()) {
	                    QuoteVO.LineItem parentItem = linesById.get(item.requiredById);
	                    item.requiredBy = parentItem.line;
	                    parentItem.addComponent(item);
	                    // Force Package (Bundle__c) flag on referenced line just in case
	                    item.requiredBy.put(String.valueOf(SBQQ__QuoteLine__c.SBQQ__Bundle__c), true);
	                }
	            }
	        }
        }
        return vosById.values();
    }
    
    public void save(QuoteVO vo) {
    	save(vo, true);
    }
    
    public void save(QuoteVO vo, Boolean saveQuote) {
        quoteType = vo.record.getSObjectType();
        if (quoteType == SBQQ__Quote__c.sObjectType) {
            quoteLineType = SBQQ__QuoteLine__c.sObjectType;
        } else if (quoteType == SBQQ__WebQuote__c.sObjectType) {
            quoteLineType = SBQQ__WebQuoteLine__c.sObjectType;
        } else {
            System.assert(false, 'Unrecognized quote type: ' + quoteType);
        }
        
        if (saveQuote) {
        	Database.upsert(vo.record);
        }
        
        Map<Id,SBQQ__QuoteLineGroup__c> existingGroups = 
            new Map<Id,SBQQ__QuoteLineGroup__c>([SELECT Id FROM SBQQ__QuoteLineGroup__c WHERE SBQQ__Quote__c = :vo.getId()]);
        Map<Id,SObject> existingLines = loadExistingLines(vo.quoteId);
            
        
        Set<Id> deletedGroupIds = new Set<Id>(existingGroups.keySet());
        deletedGroupIds.removeAll(vo.getGroupIds());
        Set<Id> deletedLineIds = new Set<Id>(existingLines.keySet());
        deletedLineIds.removeAll(vo.getLineIds());
        
        List<SBQQ__QuoteLineGroup__c> groups = new List<SBQQ__QuoteLineGroup__c>();
        for (QuoteVO.LineItemGroup lig : vo.lineItemGroups) {
            if (lig.lineGroup.SBQQ__Quote__c == null) {
                lig.lineGroup.SBQQ__Quote__c = vo.quoteId;
            }
            groups.add(lig.lineGroup);
        }
        upsert groups;
        
        saveRootLines(vo);
        
        Database.delete(new List<Id>(deletedGroupIds));
        Database.delete(new List<Id>(deletedLineIds));
    }
    
    private Map<Id,SObject> loadExistingLines(Id quoteId) {
    	QueryBuilder qb = new QueryBuilder(quoteLineType);
    	qb.getSelectClause().addField(SBQQ__QuoteLine__c.Id);
    	qb.getWhereClause().addExpression(qb.eq(SBQQ__QuoteLine__c.SBQQ__Quote__c, quoteId));
    	
    	Map<Id,SObject> existing = new Map<Id,SObject>();
    	for (SObject line : Database.query(qb.buildSOQL())) {
    		existing.put(line.Id, line);
    	}
    	return existing;
    }
    
    private void saveRootLines(QuoteVO vo) {
    	List<SObject> linesInsert = new List<SObject>();
        List<SObject> linesUpdate = new List<SObject>();
        List<QuoteVO.LineItem> items = new List<QuoteVO.LineItem>();
        for (QuoteVO.LineItem item : vo.lineItems) {
            if (item.quoteId == null) {
                item.quoteId = vo.quoteId;
            }
            if (item.parentGroup != null) {
                item.line.put(SBQQ__QuoteLine__c.SBQQ__Group__c, item.parentGroup.lineGroup.Id);
            }
            if (!item.isComponent()) {
                items.add(item);
                if (item.line.Id == null) {
                	linesInsert.add(item.line);
                } else {
                	linesUpdate.add(item.line);
                }
            }
        }
        if (!linesInsert.isEmpty()) {
            insert linesInsert;
        }
        if (!linesUpdate.isEmpty()) {
            update linesUpdate;
        }
        
        saveComponents(items);
    }
    
    private void saveComponents(List<QuoteVO.LineItem> parentItems) {
        List<SObject> linesInsert = new List<SObject>();
        List<SObject> linesUpdate = new List<SObject>();
        List<QuoteVO.LineItem> items = new List<QuoteVO.LineItem>();
        for (QuoteVO.LineItem parentItem : parentItems) {
            for (QuoteVO.LineItem component : parentItem.getComponents()) {
                if (component.quoteId== null) {
                    component.quoteId = parentItem.parentQuote.quoteId;
                }
                if (component.parentGroup != null) {
                    component.line.put(SBQQ__QuoteLine__c.SBQQ__Group__c, component.parentGroup.lineGroup.Id);
                }
                component.requiredBy = parentItem.line;
                component.requiredById = parentItem.lineId;
                if (component.line.Id == null) {
                	linesInsert.add(component.line);
                } else {
                	linesUpdate.add(component.line);
                }
                if (component.hasComponents()) {
                    items.add(component);
                }
            }
        }
        if (!linesInsert.isEmpty()) {
            insert linesInsert;
        }
        if (!linesUpdate.isEmpty()) {
            update linesUpdate;
        }
        
        if (!items.isEmpty()) {
            saveComponents(items);
        }
    }
    
    /* private void saveDependentLines(QuoteVO vo, List<SObject> parentLines) {
        Map<Id,SObject> parentsById = new Map<Id,SObject>();
        for (SObject parent : parentLines) {
            parentsById.put(parent.Id, parent);
        }
        
        List<SObject> childLines = new List<SObject>();
        List<SObject> childLinesInsert = new List<SObject>();
        List<SObject> childLinesUpdate = new List<SObject>();
        for (QuoteVO.LineItem item : vo.lineItems) {
            for (SObject parent : parentLines) {
                // We only want the lines directly required by parent lines AND unsaved parents
                if ((item.requiredBy == parent) && (item.requiredById == null)) {
                    if (item.quoteId == null) {
                        item.quoteId = vo.quoteId;
                    }
                    if (item.parentGroup != null) {
                        item.line.put(QuoteLine__c.Group__c, item.parentGroup.lineGroup.Id);
                    }
                    item.requiredById = item.requiredBy.Id;
                    item.requiredBy = null;
                    childLines.add(item.line);
                    if (item.line.Id == null) {
                    	childLinesInsert.add(item.line);
                    } else {
                    	childLinesUpdate.add(item.line);
                    }
                }
            }
        }
        if (!childLines.isEmpty()) {
            insert childLinesInsert;
            update childLinesUpdate;
            saveDependentLines(vo, childLines);
        }
    } */
    
    private List<SObject> loadLines(Set<Id> quoteIds) {
        QueryBuilder builder = createLineQuery();
        builder.getWhereClause().addExpression(builder.inExpr(SBQQ__QuoteLine__c.SBQQ__Quote__c, quoteIds));
        return Database.query(builder.buildSOQL());
    }
    
    private QueryBuilder createQuoteQuery() {
        if (quoteFields == null) {
            return createQuoteQuery(MetaDataUtils.getFields(quoteType));
        } else {
            return createQuoteQuery(MINIMAL_QUOTE_FIELDS);
        }
    }
    
    private QueryBuilder createQuoteQuery(List<Schema.SObjectField> fields) {
        QueryBuilder qb = new QueryBuilder(quoteType);
        if (quoteType == SBQQ__Quote__c.sObjectType) {
            qb.getSelectClause().addField(SBQQ__Quote__c.SBQQ__Primary__c);
            qb.getSelectClause().addField(SBQQ__Quote__c.SBQQ__LineItemsGrouped__c);
        } else if (quoteType == SBQQ__WebQuote__c.sObjectType) {
            qb.getSelectClause().addField(SBQQ__WebQuote__c.SBQQ__PricebookId__c);
        }
        for (Schema.SObjectField field : fields) {
            qb.getSelectClause().addField(field);
        }
        if (UserInfo.isMultiCurrencyOrganization()) {
            qb.getSelectClause().addField('CurrencyIsoCode');
        }
        qb.getSelectClause().addField(SBQQ__Quote__c.SBQQ__Opportunity__c,Opportunity.Id);
        qb.getSelectClause().addField(SBQQ__Quote__c.SBQQ__Opportunity__c,Opportunity.Pricebook2Id);
        qb.getSelectClause().addField(SBQQ__Quote__c.SBQQ__Opportunity__c,Opportunity.AccountId);
        Schema.SObjectField defaultGroupNameField = MetaDataUtils.getField(quoteType, QuoteDAO.DEFAULT_GROUP_NAME_FIELD);
        if (defaultGroupNameField != null) {
        	qb.getSelectClause().addField(defaultGroupNameField);
        }
        return qb;
    }
    
    private QueryBuilder createGroupSubQuery() {
        if (groupFields == null) {
            return createGroupSubQuery(MetaDataUtils.getFields(SBQQ__QuoteLineGroup__c.sObjectType));
        } else {
            return createGroupSubQuery(MINIMAL_GROUP_FIELDS);
        }
    }
    
    private QueryBuilder createGroupSubQuery(List<Schema.SObjectField> fields) {
        QueryBuilder qb = new QueryBuilder(MetaDataUtils.findRelationship(SBQQ__Quote__c.sObjectType, SBQQ__QuoteLineGroup__c.SBQQ__Quote__c).getRelationshipName());
        for (Schema.SObjectField field : fields) {
            qb.getSelectClause().addField(field);
        }
        qb.getOrderByClause().addAscending(SBQQ__QuoteLineGroup__c.SBQQ__Number__c);
        qb.getOrderByClause().addAscending(SBQQ__QuoteLineGroup__c.Id);
        return qb;
    }
    
    private QueryBuilder createLineQuery() {
        return createLineQuery(MetaDataUtils.getFields(quoteLineType));
    }
            
    private QueryBuilder createLineQuery(List<Schema.SObjectField> fields) {
        Map<String,Schema.SObjectField> customLookupsByName = new Map<String,Schema.SObjectField>();
        
        QueryBuilder qb = new QueryBuilder(quoteLineType);
        qb.getSelectClause().addField(SBQQ__QuoteLine__c.SBQQ__Product__c, Product2.Name);
        qb.getSelectClause().addField(SBQQ__QuoteLine__c.SBQQ__Product__c, Product2.ProductCode);
        qb.getSelectClause().addField(SBQQ__QuoteLine__c.SBQQ__Product__c, Product2.SBQQ__ConfigurationFields__c);
        qb.getSelectClause().addField(SBQQ__QuoteLine__c.SBQQ__Product__c, Product2.SBQQ__AssetConversion__c);
        qb.getSelectClause().addField(SBQQ__QuoteLine__c.SBQQ__Product__c, Product2.SBQQ__ExcludeFromOpportunity__c);
        qb.getSelectClause().addField(SBQQ__QuoteLine__c.SBQQ__Product__c, Product2.SBQQ__ReconfigurationDisabled__c);
        qb.getSelectClause().addField(SBQQ__QuoteLine__c.SBQQ__Product__c, Product2.SBQQ__DescriptionLocked__c);
        qb.getSelectClause().addField(SBQQ__QuoteLine__c.SBQQ__Product__c, Product2.SBQQ__ConfigurationEvent__c);
        qb.getSelectClause().addField(SBQQ__QuoteLine__c.SBQQ__Product__c, Product2.SBQQ__ExcludeFromMaintenance__c);
        qb.getSelectClause().addField(SBQQ__QuoteLine__c.SBQQ__Product__c, Product2.SBQQ__IncludeInMaintenance__c);
        // Querying RequiredBy__r relationship because QuoteVO.LineItem.isComponent method
        // attempts to access it
        qb.getSelectClause().addField(SBQQ__QuoteLine__c.SBQQ__RequiredBy__c, SBQQ__QuoteLine__c.Name);
        for (Schema.SObjectField field : fields) {
            qb.getSelectClause().addField(field);
            if (field.getDescribe().getType() == Schema.DisplayType.Reference && 
                    customLookupsByName.containsKey(field.getDescribe().getName())) {
                qb.getSelectClause().addField(field.getDescribe().getRelationshipName(), 'Name');
            }
        }
        qb.getOrderByClause().addAscending(SBQQ__QuoteLine__c.SBQQ__Number__c);
        qb.getOrderByClause().addAscending(SBQQ__QuoteLine__c.Id);
        return qb;
    }
    
    private void determineObjectType(Id id) {
        System.assert(id != null, 'Quote ID must not be null');
        if (String.valueOf(id).substring(0,3) == SBQQ__Quote__c.sObjectType.getDescribe().getKeyPrefix()) {
            quoteType = SBQQ__Quote__c.sObjectType;
            quoteLineType = SBQQ__QuoteLine__c.sObjectType;
        } else if (String.valueOf(id).substring(0,3) == SBQQ__WebQuote__c.sObjectType.getDescribe().getKeyPrefix()) {
            quoteType = SBQQ__WebQuote__c.sObjectType;
            quoteLineType = SBQQ__WebQuoteLine__c.sObjectType;
        } else {
            System.assert(false, 'Unsupported quote object: ' + id);
        }
    }
}