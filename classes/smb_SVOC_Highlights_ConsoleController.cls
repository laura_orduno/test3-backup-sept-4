public with sharing class smb_SVOC_Highlights_ConsoleController {

	public Id AccountId {get; set;}
	
	public smb_SVOC_Highlights_ConsoleController() {
		initialise();
	}
	
	public void initialise() {
			
		Id incomingId = ApexPages.currentPage().getParameters().get('Id'); 
		
		System.debug('***::***::***::***::***::***::***');
		System.debug(incomingId);
		
		if (incomingId == null) return;
		
		String objectAPIName = incomingId.getSObjectType().getDescribe().getName();
		
		System.debug(objectAPIName);
		
		if (objectAPIName == 'Case') {
			AccountId = getAccountIdFromCase(incomingId);
		} 
		
		else if (ObjectAPIName == 'Contact') {
			AccountId = getAccountIdFromContact(incomingId);
		} 
		
		else if (ObjectAPIName == 'Task') {
			AccountId = getAccountIdFromTask(incomingId);
		}
		
		system.debug(AccountId);
	}
	
	private static Id getAccountIdFromCase(Id CaseId) {
		List<Case> cases = [SELECT AccountId FROM Case WHERE Id =: CaseId LIMIT 1];
		
		if (cases.size() > 0)
			return cases.get(0).AccountId;
		
		return null;
	}
	
	private static Id getAccountIdFromContact(Id ContactId) {
		List<Contact> contacts = [SELECT AccountId FROM Contact WHERE Id = :ContactId LIMIT 1];
		
		if (contacts.size() > 0)
			return contacts.get(0).AccountId;
		
		return null;
	}
	
	private static Id getAccountIdFromTask(Id TaskId) {
		List<Task> tasks = [SELECT WhatId, WhoId, AccountId FROM Task WHERE Id = :TaskId LIMIT 1];
		
		if (tasks.size() == 0)
			return null;
		
		Task t = tasks.get(0);
		
		if (t.AccountId != null) return t.AccountId;
		
		Id searchId = t.WhatId;
		
		if (searchId == null)
			searchId = t.WhoId;
		
		if (searchId == null)
			return null;
			
		String objectType = searchId.getSObjectType().getDescribe().getName();
		
		if (objectType == 'Account') {return searchId;
		}
		else if (objectType == 'Case') {return getAccountIdFromCase(searchId);
		} 
		else if (objectType == 'Contact') {return getAccountIdFromContact(searchId);
		}
		
		return null;
	}

}