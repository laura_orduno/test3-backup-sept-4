/*
Title: Cloud Projects - Calendar Blocking View
Description: Queries active cloud projects for display in a gantt chart.
Author: Hans Vedo, hvedo@tractionondemand.com
2013-03-28: Updated to include the Id in the SELECT statement, supporting direct project links.
2013-03-13: Test added
2013-02-25: Created
*/

global with sharing class trac_CloneAccountPlan {
	
	public trac_CloneAccountPlan() {
		
	}

	webService static String retrieveExistingAccountPlans(Id accountId) {
		Map<String, Map<Id, String>> result = new Map<String, Map<Id, String>>();
		result.put('currentAccountPlan', new Map<Id, String>());
		result.put('plansInHierarchy', new Map<Id, String>());

		List<ESP__c> currentAccountPlan = [SELECT Id, Name, RecordType.Name FROM ESP__c WHERE Account__c = :accountId ];
		
		
		if(currentAccountPlan.size() > 0) {
			ESP__c plan = currentAccountPlan.get(0);
			result.get('currentAccountPlan').put(plan.Id, plan.Name);
			return JSON.serialize(result);
		}
		
		Account currentAccount = [SELECT Id, RecordTypeId, RecordType.Name, ParentId FROM Account WHERE Id=:accountId];
		Id parentAccountId;
		if(currentAccount.RecordType.Name == 'CBUCID') {
			parentAccountId = currentAccount.Id;
		} else {
			parentAccountId = currentAccount.ParentId;
		}

		Set<Id> accountIds = (new Map<Id, Account>([SELECT Id FROM Account WHERE ParentId=:parentAccountId AND RecordType.Name='RCID'])).keySet().clone();
		accountIds.add(parentAccountId);

		for(ESP__c plan : [SELECT Id, Name, RecordType.Name, Account__r.RecordType.Name, Account__r.RCID__c, Account__r.CBUCID_Parent_Id__c FROM ESP__c WHERE Account__c IN :accountIds AND Account__c != :accountId]) {
			if(plan.Account__r.RecordType.Name=='RCID') {
				result.get('plansInHierarchy').put(plan.Id, plan.Account__r.RCID__c + ' - ' + plan.Account__r.RecordType.Name + ' - ' + plan.Name);
			} else {
				result.get('plansInHierarchy').put(plan.Id, plan.Account__r.CBUCID_Parent_Id__c + ' - ' + plan.Account__r.RecordType.Name + ' - ' + plan.Name);
			}
		}
		return JSON.serialize(result);
	}

	webService static String cloneAccountPlanService(Id targetAccountId, Id accountPlanId) {
		Map<String, String> result = new Map<String, String>();
		result.put('newAccountPlanId', cloneAccountPlan(targetAccountId, accountPlanId) );
		return JSON.serialize(result);
	}

	public static List<Id> getIdsFromObjectList(List<SObject> objs) {
		List<Id> ids = new List<Id>();
		for(SObject obj : objs) ids.add(obj.Id);
		return ids;
	}

	public static Id cloneAccountPlan(Id targetAccountId, Id accountPlanId) {
		ESP__c originalAccountPlan = [SELECT Id, Name, Account__c, Account__r.Name FROM ESP__C WHERE Id=:accountPlanId];
		Account newAccount = [SELECT Name FROM Account WHERE Id=:targetAccountId];
		ESP__c newAccountPlan = (ESP__c)cloneObject(accountPlanId, 'ESP__c');
		newAccountPlan.Account__c = targetAccountId;
		newAccountPlan.Name = newAccount.Name + ' Account Plan - Cloned from ' + originalAccountPlan.Account__r.Name;

		if(newAccountPlan.Name.length() > 80) newAccountPlan.Name = newAccountPlan.Name.substring(0,80);
		insert newAccountPlan;

		List<Product_Presence__c> oldProductPresences = [SELECT Id, ESP__c FROM Product_Presence__c WHERE ESP__c = :accountPlanId];
		Map<Id, SObject> newProductPresences = cloneObjects(getIdsFromObjectList(oldProductPresences), 'Product_Presence__c');
		for(Product_Presence__c oldProductPresence : oldProductPresences) {
			Product_Presence__c newProductPresence = (Product_Presence__c)newProductPresences.get(oldProductPresence.Id);
			newProductPresence.ESP__c = newAccountPlan.Id;
		}
		insert newProductPresences.values();

		List<Department__c> oldDepartments = [SELECT Id, Account__c FROM Department__c WHERE Account__c = :originalAccountPlan.Account__c];
		Map<Id, SObject> newDepartments = cloneObjects(getIdsFromObjectList(oldDepartments), 'Department__c');
		for(Department__c oldDepartment : oldDepartments) {
			Department__c newDepartment = (Department__c)newDepartments.get(oldDepartment.Id);
			newDepartment.Account__c = targetAccountId;
		}
		insert newDepartments.values();
 		
 		List<Account_Map_Data__c> oldAccountMapData = [SELECT Id, ESP__c, Department__c FROM Account_Map_Data__c WHERE ESP__c = :accountPlanId];
		Map<Id, SObject> newAccountMapData = cloneObjects(getIdsFromObjectList(oldAccountMapData), 'Account_Map_Data__c');
		for(Account_Map_Data__c oldAccountMapDataItem : oldAccountMapData) {
			Account_Map_Data__c newAccountMapDataItem = (Account_Map_Data__c)newAccountMapData.get(oldAccountMapDataItem.Id);
			newAccountMapDataItem.ESP__c = newAccountPlan.Id;
			newAccountMapDataItem.Department__c = newDepartments.get(oldAccountMapDataItem.Department__c).Id;
		}
		insert newAccountMapData.values();

		List<Account_Plan_Financial__c> oldAccountPlanFinancials = [SELECT Id, Account_Plan__c FROM Account_Plan_Financial__c WHERE Account_Plan__c = :accountPlanId];
		Map<Id, SObject> newAccountPlanFinancials = cloneObjects(getIdsFromObjectList(oldAccountPlanFinancials), 'Account_Plan_Financial__c');
		for(Account_Plan_Financial__c oldAccountPlanFinancial : oldAccountPlanFinancials) {
			Account_Plan_Financial__c newAccountPlanFinancial = (Account_Plan_Financial__c)newAccountPlanFinancials.get(oldAccountPlanFinancial.Id);
			newAccountPlanFinancial.Account_Plan__c = newAccountPlan.Id;
		}
		insert newAccountPlanFinancials.values();

		Map<Id, ESP_User_Contact_Roles__c> oldLinkedContacts = new Map<Id, ESP_User_Contact_Roles__c>([SELECT Id, Contact__r.ReportsToId, ESP__c FROM ESP_User_Contact_Roles__c WHERE ESP__c = :accountPlanId]);
		Map<Id, SObject> newLinkedContacts = cloneObjects(getIdsFromObjectList(oldLinkedContacts.values()), 'ESP_User_Contact_Roles__c');
		List<Id> reportsToList = new List<Id>();
		List<Id> existingContactsInRoles = new List<Id>();

		for(Id oldLinkedContactId : oldLinkedContacts.keySet()) {
			existingContactsInRoles.add(oldLinkedContacts.get(oldLinkedContactId).Contact__c);
			reportsToList.add(oldLinkedContacts.get(oldLinkedContactId).Contact__r.ReportsToId);
			ESP_User_Contact_Roles__c newLinkedContact = (ESP_User_Contact_Roles__c)newLinkedContacts.get(oldLinkedContactId);
			newLinkedContact.ESP__c = newAccountPlan.Id;
		}
		insert newLinkedContacts.values();

		List<Contact> contactsOutsideRoles = [Select Id From Contact Where Contact.AccountId = :originalAccountPlan.Account__c AND Id in :reportsToList AND Id NOT in :existingContactsInRoles];
		List<ESP_User_Contact_Roles__c> newContactRoles = new List<ESP_User_Contact_Roles__c>();
		for(Contact c : contactsOutsideRoles) {
			ESP_User_Contact_Roles__c user_contact_role = new ESP_User_Contact_Roles__c();
			user_contact_role.Contact__c = c.Id;
			user_contact_role.ESP__c = newAccountPlan.Id;
			newContactRoles.add(user_contact_role);
		}
		insert newContactRoles;

		List<ESP_Contact_Attribute__c> oldContactAttributes = [SELECT Id, ESP_User_Contact_Role__c FROM ESP_Contact_Attribute__c WHERE ESP_User_Contact_Role__c IN :oldLinkedContacts.keySet()];
		Map<Id, SOBject> newContactAttributes = cloneObjects(getIdsFromObjectList(oldContactAttributes), 'ESP_Contact_Attribute__c');
		for(ESP_Contact_Attribute__c oldContactAttribute : oldContactAttributes) {
			ESP_Contact_Attribute__c newContactAttribute = (ESP_Contact_Attribute__c)newContactAttributes.get(oldContactAttribute.Id);
			newContactAttribute.ESP_User_Contact_Role__c = newLinkedContacts.get(oldContactAttribute.ESP_User_Contact_Role__c).Id;
		}
		insert newContactAttributes.values();

		List<Goal__c> originalGoals = [SELECT Id FROM Goal__c WHERE ESP__c = :accountPlanId];
		List<Id> originalGoalIds = new List<Id>();
		for(Goal__c goal : originalGoals) originalGoalIds.add(goal.Id);
		
		Map<Id, Sobject> newGoals = (Map<Id, Sobject>) cloneObjects(originalGoalIds, 'Goal__c');
		for(Goal__c goal : (List<Goal__c>)newGoals.Values()) {
			goal.ESP__c = newAccountPlan.Id;
		}
		insert newGoals.values();
		
		if(originalGoalIds.size() > 0) {

			Map<Id, Objectives__c> originalObjectives = new Map<Id, Objectives__c>([SELECT Id, Goal__c FROM Objectives__c WHERE Goal__c IN :originalGoalIds]);
			Map<Id, SOBject> newObjectives = cloneObjects(getIdsFromObjectList(originalObjectives.values()), 'Objectives__c');
			
			for(Id originalObjectiveId : originalObjectives.keySet()) {
				Objectives__c newObjective = (Objectives__c)newObjectives.get(originalObjectiveId);
				newObjective.Goal__c = newGoals.get(originalObjectives.get(originalObjectiveId).Goal__c).Id;
			}
			insert newObjectives.values();

		}
		return newAccountPlan.Id;

		/*
		Map<Id, Task> originalTasks = new Map<Id, Task>([SELECT Id, WhatId FROM Task WHERE WhatId IN :originalObjectives.keySet()]);
		List<Task> newTasks = new List<Task>();
		for(Id originalTaskId : originalTasks.keySet()) {
			Task newTask = (Task)cloneObject(originalTaskId, 'Task');
			newTask.WhatId = newObjectives.get( originalTasks.get(originalTaskId).WhatId ).Id;
			newTasks.add(newTask);
		}
		insert newTasks;*/

	}

	private static Map<Id, SObject> cloneObjects(List<Id> originalObjectIds, String objectType) {
		Map<Id, SObject> newObjects = new Map<Id, SObject>();
		if(originalObjectIds.size() ==0) return newObjects;
		String[] types = new String[]{objectType};
		Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
		
		Map<String, SObjectField> fieldList = results.get(0).fields.getMap();
		System.debug(String.join((List<String>)originalObjectIds, '\',\''));
		List<SObject> originalObjs = Database.query('SELECT ' + String.join(new List<String>(fieldList.keySet()), ',') + ' FROM ' + objectType + ' WHERE Id IN (\'' + String.join((List<String>)originalObjectIds, '\',\'') + '\')');
		
		for(SObject originalObj : originalObjs) {
			newObjects.put(originalObj.Id, originalObj.clone(false, true, false, false));
		}
		return newObjects;
	}

	private static SObject cloneObject(Id originalObjId, String objectType) {
		String[] types = new String[]{objectType};
		Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
		
		Map<String, SObjectField> fieldList = results.get(0).fields.getMap();
		List<SObject> originalObj = Database.query('SELECT ' + String.join(new List<String>(fieldList.keySet()), ',') + ' FROM ' + objectType + ' WHERE Id=\'' + originalObjId + '\'');
		Sobject tmp = originalObj.get(0).clone(false, true, false, false);
		return tmp;
	}

}