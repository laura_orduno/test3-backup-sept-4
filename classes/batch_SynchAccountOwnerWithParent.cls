global class batch_SynchAccountOwnerWithParent implements Database.Batchable<sObject>
{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {  
        String recordLimit = '';
        
        for(batch_SyncParentChildAccountFields_Query__c q : batch_SyncParentChildAccountFields_Query__c.getAll().values())
        {
            recordLimit = q.record_limit__c;
            
            break;
        }
        
        System.debug('recordLimit??: ' + recordLimit);  
        
        String query = 
            ' SELECT ' +
                ' ownerId, ' +                                                          // set of account fields to be potentially updated 
                ' Account_Owner_Synched_With_Parent__c, ' +                             // (BAN/CAN) formula field that checks if the account's paternal RCID's ownerId is the same (True/False)            
                ' Paternal_RCID_Owner_Id__c ' +                                         // (BAN/CAN) formula field that returns the ownerId value of the account's paternal RCID
            ' FROM ' + 
                ' Account ' + 
            ' WHERE ' +             
                ' parentId != null ' + 
                ' AND ' + 
                ' ( ' +
                    ' ( Account_Owner_Synched_With_Parent__c = False AND Paternal_RCID_Owner_Id__c != null ) ' +
                ' ) ' + recordLimit;
        
        System.debug('query??: ' + query);  
        
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Account> scope)
    { 
        List<Account> updateList = new List<Account>();

        for(Account a : scope)
        {                           
            Boolean addToUpdateList = false;
            
            if(!a.Account_Owner_Synched_With_Parent__c)
            {
                a.ownerId = a.Paternal_RCID_Owner_Id__c;
                addToUpdateList = true;                                    
            }
                                      
            if(addToUpdateList)
            {
                updateList.add(a);
            }
        }
        
        if(updateList.size() > 0)
        {
            update updateList;
        }        
    }
        
    global void finish(Database.BatchableContext BC)
    {
    }
}