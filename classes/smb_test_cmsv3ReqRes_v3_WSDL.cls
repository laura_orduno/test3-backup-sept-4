/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class smb_test_cmsv3ReqRes_v3_WSDL {

static testMethod void cmsv3_ContractManagementSvcReqRes_v3() {
    cmsv3_ContractManagementSvcReqRes_v3 smb21 = new cmsv3_ContractManagementSvcReqRes_v3();
        cmsv3_ContractManagementSvcReqRes_v3.calculateTerminationCharges_element a1 = new cmsv3_ContractManagementSvcReqRes_v3.calculateTerminationCharges_element();
        cmsv3_ContractManagementSvcReqRes_v3.calculateTerminationChargesResponse_element a2 = new cmsv3_ContractManagementSvcReqRes_v3.calculateTerminationChargesResponse_element();
        cmsv3_ContractManagementSvcReqRes_v3.cancelContractSubmission_element a3 = new cmsv3_ContractManagementSvcReqRes_v3.cancelContractSubmission_element();
        cmsv3_ContractManagementSvcReqRes_v3.cancelContractSubmissionResponse_element a4 = new cmsv3_ContractManagementSvcReqRes_v3.cancelContractSubmissionResponse_element();
        cmsv3_ContractManagementSvcReqRes_v3.ContractData a5 = new cmsv3_ContractManagementSvcReqRes_v3.ContractData();
        cmsv3_ContractManagementSvcReqRes_v3.createContractSubmission_element a6 = new cmsv3_ContractManagementSvcReqRes_v3.createContractSubmission_element();
        cmsv3_ContractManagementSvcReqRes_v3.createContractSubmissionResponse_element a7 = new cmsv3_ContractManagementSvcReqRes_v3.createContractSubmissionResponse_element();
        cmsv3_ContractManagementSvcReqRes_v3.findContractAmendmentsByAssociateNum_element a8 = new cmsv3_ContractManagementSvcReqRes_v3.findContractAmendmentsByAssociateNum_element();
        cmsv3_ContractManagementSvcReqRes_v3.findContractAmendmentsByAssociateNumResponse_element a9 = new cmsv3_ContractManagementSvcReqRes_v3.findContractAmendmentsByAssociateNumResponse_element();
        cmsv3_ContractManagementSvcReqRes_v3.findContractData_element a10 = new cmsv3_ContractManagementSvcReqRes_v3.findContractData_element();
        cmsv3_ContractManagementSvcReqRes_v3.findContractDataResponse_element a11 = new cmsv3_ContractManagementSvcReqRes_v3.findContractDataResponse_element();
        cmsv3_ContractManagementSvcReqRes_v3.findContractsByAgentId_element a12 = new cmsv3_ContractManagementSvcReqRes_v3.findContractsByAgentId_element();
        cmsv3_ContractManagementSvcReqRes_v3.findContractsByAgentIdResponse_element a13 = new cmsv3_ContractManagementSvcReqRes_v3.findContractsByAgentIdResponse_element();
        cmsv3_ContractManagementSvcReqRes_v3.findContractsByCustomerId_element a14 = new cmsv3_ContractManagementSvcReqRes_v3.findContractsByCustomerId_element();
        cmsv3_ContractManagementSvcReqRes_v3.findContractsByCustomerIdResponse_element a15 = new cmsv3_ContractManagementSvcReqRes_v3.findContractsByCustomerIdResponse_element();
        cmsv3_ContractManagementSvcReqRes_v3.getContract_element a16 = new cmsv3_ContractManagementSvcReqRes_v3.getContract_element();
        cmsv3_ContractManagementSvcReqRes_v3.getContractDocument_element a17 = new cmsv3_ContractManagementSvcReqRes_v3.getContractDocument_element();
        cmsv3_ContractManagementSvcReqRes_v3.getContractDocumentMetaData_element a18 = new cmsv3_ContractManagementSvcReqRes_v3.getContractDocumentMetaData_element();
        cmsv3_ContractManagementSvcReqRes_v3.getContractDocumentMetaDataResponse_element a19 = new cmsv3_ContractManagementSvcReqRes_v3.getContractDocumentMetaDataResponse_element();
        cmsv3_ContractManagementSvcReqRes_v3.getContractDocumentResponse_element a20 = new cmsv3_ContractManagementSvcReqRes_v3.getContractDocumentResponse_element();
        cmsv3_ContractManagementSvcReqRes_v3.getContractResponse_element a21 = new cmsv3_ContractManagementSvcReqRes_v3.getContractResponse_element();
        cmsv3_ContractManagementSvcReqRes_v3.replaceContractSubmission_element a22 = new cmsv3_ContractManagementSvcReqRes_v3.replaceContractSubmission_element();
        cmsv3_ContractManagementSvcReqRes_v3.replaceContractSubmissionResponse_element a23 = new cmsv3_ContractManagementSvcReqRes_v3.replaceContractSubmissionResponse_element();
        cmsv3_ContractManagementSvcReqRes_v3.TerminationChargesInformation a24 = new cmsv3_ContractManagementSvcReqRes_v3.TerminationChargesInformation();
        cmsv3_ContractManagementSvcReqRes_v3.TerminationChargesParameter a25 = new cmsv3_ContractManagementSvcReqRes_v3.TerminationChargesParameter();
        cmsv3_ContractManagementSvcReqRes_v3.triggerResendContract_element a26 = new cmsv3_ContractManagementSvcReqRes_v3.triggerResendContract_element();
        cmsv3_ContractManagementSvcReqRes_v3.triggerResendContractResponse_element a27 = new cmsv3_ContractManagementSvcReqRes_v3.triggerResendContractResponse_element();
        cmsv3_ContractManagementSvcReqRes_v3.updateContractSubmission_element a28 = new cmsv3_ContractManagementSvcReqRes_v3.updateContractSubmission_element();
        cmsv3_ContractManagementSvcReqRes_v3.updateContractSubmissionResponse_element a29 = new cmsv3_ContractManagementSvcReqRes_v3.updateContractSubmissionResponse_element();
        cmsv3_ContractManagementSvcReqRes_v3.verifyValidReplacement_element a30  = new cmsv3_ContractManagementSvcReqRes_v3.verifyValidReplacement_element();
        cmsv3_ContractManagementSvcReqRes_v3.verifyValidReplacementResponse_element a31 = new cmsv3_ContractManagementSvcReqRes_v3.verifyValidReplacementResponse_element();
            
            }
            }