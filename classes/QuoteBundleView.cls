/*
    Quote Bundle View
    QuoteBundleView.cls
    Part of the TELUS QuoteQuickly Portal
    
    Author: Alex Miller @ Traction <amiller@tractionondemand.com>
    Created: March 10, 2012
    
    Since: Release 5
    Initial Definition: R5rq4
    
    Known Issues:
    1. This is a model and a view - time constraints required this
    2. Sorting the quote lines doesn't work

    New Field Audit:
     - Object -          - API Name -            - Label -           - Type -                    - Since -
    
*/
public without sharing class QuoteBundleView {    
    public string status {get; private set;}
    
    public id quoteId {get; private set;}
    public SBQQ__Quote__c quote {get; private set;}
    public Map<Id, SBQQ__QuoteLine__c> lines {get; private set;}
    public Map<Id, SBQQ__QuoteLine__c> linesToProcess {get; private set;}
    public Map<Id, AdditionalInformation__c> additionalInformations {get; private set;}
    public Map<id, Id> deviceToService {get; private set;}
    
    public String agreementExpiryDateFormatted { get { return Date.today().addDays(7).format(); }}
    
    public SBQQ__QuoteLine__c bundleLine {get; private set;}
    public String Name {get{
        if (bundleLine == null) { return ''; }
        String name = bundleLine.SBQQ__Product__r.Name;
        String nameTag = '';
        if (name.toLowerCase().contains('plus')) {
            nameTag = '1 Office Phone, 1 Internet Line and 2 Wireless Devices';
        } else {
            nameTag = '1 Office Phone and 2 Wireless Devices';
        }
        return bundleLine.SBQQ__Product__r.Name + ' - ' + nameTag;
    }}
    public Boolean isPlus {get{
        if (bundleLine == null || bundleLine.SBQQ__Product__r == null || bundleLine.SBQQ__Product__r.Name == null) { return false; }
        return bundleLine.SBQQ__Product__r.Name.toLowerCase().contains('plus');
    }}
    public Decimal BundleOnlyMonthlyTotal {get{ return (bundleLine==null) ? 0.00 : bundleLine.SBQQ__NetPrice__c;}}
    
    private Map<Id, coreLine> coreLines {get; set;}
    public coreLine[] getCoreLines() {
        coreLine[] cls = coreLines.values();
        sortCore(cls, 'asc');
        return cls;
    }
    
    public Integer CoreLinesCount {get{if (coreLines != null) {return coreLines.size();} return 0;}}
    public Decimal CoreMonthlyTotal {get{
        if (coreLines == null) { return 0.00; }
        Decimal total = 0.00;
        for (coreLine cl : coreLines.values()) {
            total += cl.line.SBQQ__NetTotal__c;
            for (featureLine fl : cl.features.values()) {
                total += fl.line.SBQQ__NetTotal__c;
            }
        }
        return total;
    }}
    private Map<Id, addlLine> addlLines {get; set;}
    public addlLine[] getAddlLines() {
        addlLine[] als = addlLines.values();
        sortAddl(als, lines);
        return als;
    }
    public Integer AddlLinesCount {get{if (addlLines != null) {return addlLines.size();} return 0;}}
    public Decimal AddlMonthlyTotal {get{
        if (addlLines == null) { return 0.00; }
        Decimal total = 0.00;
        for (addlLine al : addlLines.values()) {
            total += al.line.SBQQ__NetTotal__c;
            for (featureLine fl : al.features.values()) {
                total += fl.line.SBQQ__NetTotal__c;
            }
        }
        return total;
    }}
    private Map<Id, deviceLine> deviceLines {get; set;}
    public deviceLine[] getDeviceLines() {
        return deviceLines.values();
    }
    public Integer DeviceLinesCount {get{if (deviceLines != null) {return deviceLines.size();} return 0;}}
    public Decimal DeviceOneTimeTotal {get{
        if (deviceLines == null) { return 0.00; }
        Decimal total = 0.00;
        for (deviceLine dl : deviceLines.values()) {
            total += dl.line.SBQQ__NetTotal__c;
        }
        return total;
    }}
    public Decimal DeviceAirtimeCreditTotal {get{
        Decimal total = 0.00;
        for (deviceLine dl : deviceLines.values()) {
            if (dl.getAirtimeCredit() != null) {
                total += dl.getAirtimeCredit();
            }
        }
        return total;
    }}
    public Map<Id, String> lineToList {get; private set;}
    public Map<Id, Id[]> lineToAdditionalInformations {get; private set;}
    
    public Decimal CoreAndBundleMonthlyTotal {get{
        return CoreMonthlyTotal + BundleOnlyMonthlyTotal;
    }}
    public Decimal MonthlyTotal {get{
        return CoreMonthlyTotal + AddlMonthlyTotal + BundleOnlyMonthlyTotal;
    }}
    
    public boolean init() {
        lines = new Map<Id, SBQQ__QuoteLine__c>();
        linesToProcess = new Map<Id, SBQQ__QuoteLine__c>();
        coreLines = new Map<Id, coreLine>();
        addlLines = new Map<Id, addlLine>();
        deviceLines = new Map<Id, deviceLine>();
        lineToList = new Map<Id, String>();
        lineToAdditionalInformations = new Map<Id, Id[] >();
        deviceToService = new Map<Id, Id>();
        
        if (quoteId == null) {
            // Return error that the quote Id is empty.
            system.debug('bundle::loadQuote: no id provided. Call failed.');
            status += 'Failed to load quote - no id. ';
            return false;
        }
        quote = QuoteUtilities.loadQuote(quoteId);
        if (quote == null || quote.Id == null) {
            system.debug('bundle::init: Unable to load quote.'); return false; }
        lines = QuoteUtilities.loadQuoteLinesMap(quote.Id);
        if (lines == null || lines.size() == 0) {
            system.debug('bundle::init: Unable to load quote lines.'); return false; }
        additionalInformations = QuoteUtilities.loadAdditionalInformationMap(quote.Id);
        if (additionalInformations == null || additionalInformations.size() == 0) {
            system.debug('bundle::init: Unable to load quote additional information records.');
        }
        return true;
    }
    public QuoteBundleView(Id qid) {
        quoteId = qid;
        if (!init()) { return; }
        if (!parse()) { return; }
    }
    public QuoteBundleView(SBQQ__Quote__c q) {
        quoteId = q.id;
        if (!init()) { return; }
        if (!parse()) { return; }
    }
    
    public boolean parse() {
        if (quote == null || quote.Id == null) { system.debug('bundle::parse: No quote loaded.'); return false; }
        if (lines == null || lines.size() == 0) { system.debug('bundle::parse: No quote lines loaded.'); return false; }
        
        // Time to parse quote lines into a magical quote template!
        linesToProcess.clear();
        for (SBQQ__QuoteLine__c line : lines.values()) {
            // Copy the quote lines into a processing map
            linesToProcess.put(line.id, line);
        }
        // Map quote lines to additional information records
        lineToAdditionalInformations.clear();
        if (additionalInformations != null) {
            for (AdditionalInformation__c ai : additionalInformations.values()) {
                if (ai != null) {
                    if (lineToAdditionalInformations.containsKey(ai.Quote_Line__c)) {
                        lineToAdditionalInformations.get(ai.Quote_Line__c).add(ai.Id);
                    } else {
                        Id[] xid = new List<Id>();
                        xid.add(ai.id);
                        lineToAdditionalInformations.put(ai.Quote_Line__c, xid);
                    }
                }
            }
        }
        
        // Find the core bundle line
        bundleLine = QuoteUtilities.findBundleLine(linesToProcess);
        if (bundleLine == null || bundleLine.Id == null) {
            // The core bundle line could not be found!
            system.debug('bundle::parse: Could not find core bundle line.');
            return false;
        } else {
            linesToProcess.remove(bundleLine.Id);   
        } system.debug('bundle::parse: Core bundle line found:'); system.debug(bundleLine);
        
        // Find the device lines
        if (!parseDeviceLines()) {
            // No device lines found.
            system.debug('bundle::parse: Could not find any device lines.');
        } system.debug('bundle::parse: Found ' + deviceLines.size() + ' device lines.');
        
        // Find the service lines
        if (!parseServiceLines()) {
            // No service lines found.
            system.debug('bundle::parse: Could not find any service lines.');
        } system.debug('bundle::parse: Found ' + coreLines.size() + ' core service lines and ' + addlLines.size() + ' additional service lines.');
        
        // Find the feature lines
        Integer ff = parseFeatureLines();
        if (ff == 0) {
            // No feature lines found.
            system.debug('bundle::parse: Could not find any feature lines.');
        } system.debug('bundle::parse: Found ' + ff + ' feature lines.');
        
        // Find the airtime credits
        parseCreditsLines();
        
        return true;
    }
    
    
    private boolean parseDeviceLines() {
        // Find the device lines
        // Clear the existing device lines
        deviceLines.clear();
        // If there's no lines to process, return false
        if (linesToProcess.size() == 0) { return false; }
        Boolean devicesFound = false;
        for (SBQQ__QuoteLine__c line : linesToProcess.values()) {
            if (line.Category__c == 'Devices') {
                // If this is a device line...
                // Store it, and take it out of the processing lineup
                AdditionalInformation__c[] xai = new List<AdditionalInformation__c>();
                if (lineToAdditionalInformations.get( line.Id ) != null) {
                    for (Id i : lineToAdditionalInformations.get( line.Id )) {
                        xai.add(additionalInformations.get(i));
                    }
                }
                deviceLines.put(line.Id, new deviceLine(line, xai));
                // Map this id to the correct list
                lineToList.put(line.Id, 'devices');
                devicesFound = true;
                linesToProcess.remove(line.Id);
            }
        } return devicesFound; }
        
    private boolean parseServiceLines() {
        // Find the service lines
        // Clear the existing service lines
        coreLines.clear();
        addlLines.clear();
        // If there's no lines to process, return false
        if (linesToProcess.size() == 0) { return false; }
        for (SBQQ__QuoteLine__c line : linesToProcess.values()) {
            if (line.Category__c == 'Services') {
                // If this is a service line...
                // Store it, and take it out of the processing lineup
                
                Boolean isCore = false;
                if (line.SBQQ__ProductOption__r.SBQQ__Feature__r.Name == 'Core') {
                    isCore = true;
                }
                if (!isCore) {
                    SBQQ__QuoteLine__c reqBy = lines.get(line.SBQQ__RequiredBy__c);
                    if (reqby != null && reqby.SBQQ__ProductOption__r.SBQQ__Feature__r.Name == 'Core') { // || line.SBQQ__NetTotal__c == null || line.SBQQ__NetTotal__c == 0) {
                        isCore = true;
                    }
                }
                
                Boolean placed = false;
                // If this is a service related to a device, find the device's addl info record and associate it
                if (lines.get(line.SBQQ__RequiredBy__c) != null && lines.get(line.SBQQ__RequiredBy__c).Category__c == 'Devices') {
                    system.debug('putting device ' + lines.get(line.SBQQ__RequiredBy__c).name + ' into device to service pointing to ' + line.name);
                    deviceToService.put(lines.get(line.SBQQ__RequiredBy__c).id, line.id);
                    
                    AdditionalInformation__c[] xai = new List<AdditionalInformation__c>();
                    if (lineToAdditionalInformations.get( line.SBQQ__RequiredBy__c ) != null) {
                        for (Id i : lineToAdditionalInformations.get( line.SBQQ__RequiredBy__c )) {
                            xai.add(additionalInformations.get(i));
                        }
                    }
                    
                    if (isCore) {
                        coreLines.put(line.Id, new coreLine(line, xai));
                    } else {
                        addlLines.put(line.Id, new addlLine(line, xai));
                    }
                    placed = true;
                } else if (lines.get(line.SBQQ__RequiredBy__c) != null && lines.get( line.SBQQ__RequiredBy__c ).Category__c == 'Services' && lines.get( line.SBQQ__RequiredBy__c ).SBQQ__OptionLevel__c != null) {
                    // This is a nested service line. Keep in in processing.
                    system.debug('I think this is a nested service line.');
                    system.debug(line);
                    system.debug(lines.get(line.sbqq__requiredby__c));
                } else {
                    if (additionalInformations == null) {
                        if (isCore) {
                            coreLines.put(line.Id, new coreLine(line, null ));
                        } else {
                            addlLines.put(line.Id, new addlLine(line, null ));
                        }
                    } else {
                        AdditionalInformation__c[] xai = new List<AdditionalInformation__c>();
                        if (lineToAdditionalInformations.get( line.Id ) != null) {
                            for (Id i : lineToAdditionalInformations.get( line.Id )) {
                                xai.add(additionalInformations.get(i));
                            }
                        }
                        if (isCore) {
                            coreLines.put(line.Id, new coreLine(line, xai));
                        } else {
                            addlLines.put(line.Id, new addlLine(line, xai));
                        }
                    }
                    placed = true;
                }
                if (placed) {
                    // Map this id to the correct list
                    if (isCore) {
                        system.debug('line to list... putting ' + line.id + ' ' + line.name + ' into core');
                        lineToList.put(line.Id, 'core');
                    } else {
                        system.debug('line to list... putting ' + line.id + ' ' + line.name + ' into addl');
                        lineToList.put(line.Id, 'addl');
                    }
                    linesToProcess.remove(line.Id);
                }
            }
        } return (coreLines.size() == 0) ? false : true; }
        
    private integer parseFeatureLines() {
        // Find the features lines
        // If there's no lines to process, return 0
        if (linesToProcess.size() == 0) { return 0; }
        system.debug('features lines to process ' + linestoprocess.size());
        Integer featuresFound = 0;
        for (SBQQ__QuoteLine__c line : linesToProcess.values()) {
            if (line.Category__c == 'Features') {
                // This is a feature line...
                
                // Find out where it belongs:
                String whereToPutThis = lineToList.get(line.SBQQ__RequiredBy__c);
                if (whereToPutThis != null && whereToPutThis == 'devices') {
                    system.debug('line ' + line.name + 's req by was a device so its been repointed to ' + lineToList.get( deviceToService.get(line.SBQQ__RequiredBy__c) ));
                    whereToPutThis = lineToList.get( deviceToService.get(line.SBQQ__RequiredBy__c) );
                    if (whereToPutThis == 'core') {
                        system.debug('line ' + line.name + ' is going into core');
                        coreLines.get( deviceToService.get(line.SBQQ__RequiredBy__c) ).addFeature(new featureLine(line));
                    } else if (whereToPutThis == 'addl') {
                        system.debug('line ' + line.name + ' is going into addl');
                        addlLines.get( deviceToService.get(line.SBQQ__RequiredBy__c) ).addFeature(new featureLine(line));
                    } else {
                        continue;
                    }
                } else {
                    if (whereToPutThis == 'core') {
                        system.debug('line ' + line.name + ' is going into core');
                        coreLines.get(line.SBQQ__RequiredBy__c).addFeature(new featureLine(line));
                    } else if (whereToPutThis == 'addl') {
                        system.debug('line ' + line.name + ' is going into addl');
                        addlLines.get(line.SBQQ__RequiredBy__c).addFeature(new featureLine(line));
                    } else {
                        continue;
                    }
                }
                featuresFound++;
                linesToProcess.remove(line.Id);
            } else if (lines.get(line.SBQQ__RequiredBy__c).Category__c == 'Services') {
                // Nested service
                String whereToPutThis = lineToList.get(line.SBQQ__RequiredBy__c);
                if (whereToPutThis == 'core') {
                    system.debug('line ' + line.name + ' is going into core');
                    coreLines.get( line.SBQQ__RequiredBy__c ).addFeature(new featureLine(line));
                } else if (whereToPutThis == 'addl') {
                    system.debug('line ' + line.name + ' is going into addl');
                    addlLines.get( line.SBQQ__RequiredBy__c ).addFeature(new featureLine(line));
                } else {
                    continue;
                }
                featuresFound++;
            }
        } return featuresFound; }
        
    private integer parseCreditsLines() {
        // Find the features lines
        // If there's no lines to process, return 0
        if (linesToProcess.size() == 0) { return 0; }
        Integer creditsFound = 0;
        for (SBQQ__QuoteLine__c line : linesToProcess.values()) {
            if (line.Category__c == 'Credits') {
                // This is a credits line, so we have to look up two levels.
                deviceLines.get(line.SBQQ__RequiredBy__c).creditLines.put(line.id, line);
                creditsFound++;
                linesToProcess.remove(line.Id);
            }
        } return creditsFound; }
    
    
    /* Nested Classes */
    public virtual class stdLine {
        /* Setup */
        public stdLine() { init(); }
        public stdLine(SBQQ__QuoteLine__c l, AdditionalInformation__c[] ai) { init(); line = l; addlInfos = ai;}
        public SBQQ__QuoteLine__c line {get; set;}
        public AdditionalInformation__c[] addlInfos {get; set;}
        private Map<Id, featureLine> features {get; set;}
        /* End Setup */
        public id Id { get { return line.Id; } }
        public string ProductName { get { return line.SBQQ__Product__r.Customer_Friendly_Name__c; } }//RD change for customer friendly name 13 feb 2014
        public Integer Quantity { get { return (Integer)line.SBQQ__Quantity__c; } }
        
        public void init() {
            features = new Map<Id, featureLine>();
        }
        
        public string getUserString() {
            // Used to display the directory listing and telephone number for a line.
            if (addlInfos == null) { return ''; }
            String us = '';
            for (AdditionalInformation__c ai : addlInfos) {
                String u = getUserStringIndividual(ai);
                if (u != '') {
                    
                    us = us + '<block>' + SFDCEncoder.SFDC_HTMLENCODE(u) + '</block>';
                }
            }
            return us;
        }
        private string getUserStringIndividual(AdditionalInformation__c addlInfo) {
            String dl = null;
            String tn = null;
            if (addlInfo == null) { return ''; }
            // Find the directory listing / owner name
            if (addlInfo.First_Name__c != null && addlInfo.Last_Name__c != null) {
                dl = addlInfo.First_Name__c + ' ' + addlInfo.Last_Name__c;
            } else if (addlInfo.First_Name__c != null) {
                dl = addlInfo.First_Name__c;
            } else if (addlInfo.Last_Name__c != null) {
                dl = addlInfo.Last_Name__c;
            } else if (addlInfo.Directory_Listing__c != null) {
                dl = addlInfo.Directory_Listing__c;
            }
            
            if (addlInfo.Mobile__c != null) {
                tn = addlInfo.Mobile__c;
            } else if (addlInfo.Billing_Telephone_Number_BTN__c != null) {
                tn = addlInfo.Billing_Telephone_Number_BTN__c;
            } else if (addlInfo.Destination_Number__c != null) {
                tn = addlInfo.Destination_Number__c;
            }
            else if (addlInfo.Porting__c != null) {
                tn = addlInfo.Porting__c;
            }
            
            if (tn != null && dl != null) {
                return dl + '  ' + tn; 
            } else if (tn != null) {
                return tn;
            } else if (dl != null) {
                return dl;
            }
            return '';
        }
        
        public string getTotal() {
            if (line.SBQQ__NetTotal__c != 0 || line.Category__c != 'Services') {
                return '$' + line.SBQQ__NetTotal__c;
            } else {
                return 'INCLUDED';
            }
        }
        
        public featureLine[] getFeatures() {
            return features.values();
        }
        
        public void addFeature(featureLine f) {
            features.put(f.Id, f);
        }
        
        public void removeFeature(featureLine f) {
            features.remove(f.Id);
        }
        
        public void removeFeature(Id fid) {
            features.remove(fid);
        }
    }
    
    public class coreLine extends stdLine {
        public coreLine(SBQQ__QuoteLine__c l, AdditionalInformation__c[] ai) { line = l; addlInfos = ai; }
    }
    
    public class addlLine extends stdLine {
        public addlLine(SBQQ__QuoteLine__c l, AdditionalInformation__c[] ai) { line = l; addlInfos = ai; }
    }
    public class deviceLine extends stdLine {
        public deviceLine(SBQQ__QuoteLine__c l, AdditionalInformation__c[] ai) { creditLines = new Map<Id, SBQQ__QuoteLine__c>(); line = l; addlInfos = ai; }
        public Map<Id, SBQQ__QuoteLine__c> creditLines {get; set;}
        public string getAction() {
            if (addlInfos == null) {return '';}
            for (AdditionalInformation__c ai : addlInfos) {
                if (ai.Phone_Number_Setup__c != null && ai.Phone_Number_Setup__c != '') { return ai.Phone_Number_Setup__c; }
                if (ai.Phone_Number_Setup_QED__c != null && ai.Phone_Number_Setup_QED__c != '') { return ai.Phone_Number_Setup_QED__c; }
            }
            return '';
        }
        public string getTerm() {
            if (addlInfos == null) {return '';}
            for (AdditionalInformation__c ai : addlInfos) {
                if (ai.Service_Term__c != null && ai.Service_Term__c != '') { return ai.Service_Term__c; }
                if (ai.Service_Term_QED__c != null && ai.Service_Term_QED__c != '') { return ai.Service_Term_QED__c; }
            }
            return ''; 
        }
        public decimal getNoTermPrice() {
            // Return the "no term price" of the product
            if (line.SBQQ__Product__r.MTM__c == null) { return null; }
            return line.SBQQ__Product__r.MTM__c;
        }
        public decimal getTermDiscount() {
            // Return the no term price minus the customer price
            if (line.SBQQ__Product__r.MTM__c == null) { return null; }
            if (line.SBQQ__NetTotal__c == null) { return line.SBQQ__Product__r.MTM__c; }
            return line.SBQQ__Product__r.MTM__c - line.SBQQ__NetTotal__c;
        }
        public decimal getAirtimeCredit() {
            decimal credit = 0;
            for (SBQQ__QuoteLine__c ql : creditLines.values()) {
                credit += ql.SBQQ__NetTotal__c;
            }
            return (credit == 0) ? null : credit;
        }
        
    }
    public class featureLine {//Need to add customer friendly name here
        public SBQQ__QuoteLine__c line {get; set;}
        public featureLine(SBQQ__QuoteLine__c l) {line = l;}
        public id Id { get { return line.id; } }
        public string ProductName { get { return line.SBQQ__Product__r.Customer_Friendly_Name__c; } }
        public Integer Quantity { get { return (Integer)line.SBQQ__Quantity__c; } }
        public decimal Total { get { return line.SBQQ__NetTotal__c; } }
    }
    
     public static void sortCore(List<coreLine> items, String order){
       List<coreLine> resultList = new List<coreLine>();
   
        //Create a map that can be used for sorting 
       Map<object, List<coreLine>> objectMap = new Map<object, List<coreLine>>();
           
       for(coreLine ob : items){
                if(objectMap.get(ob.line.SBQQ__Number__c) == null){
                    objectMap.put(ob.line.SBQQ__Number__c, new List<coreLine>()); 
                }
                objectMap.get(ob.line.SBQQ__Number__c).add(ob);
        }       
        //Sort the keys
        List<object> keys = new List<object>(objectMap.keySet());
        keys.sort();
       
        for(object key : keys){ 
            resultList.addAll(objectMap.get(key)); 
        }
       
        //Apply the sorted values to the source list
        items.clear();
        if(order.toLowerCase() == 'asc'){
            for(coreLine ob : resultList){
                items.add(ob); 
            }
        }else if(order.toLowerCase() == 'desc'){
            for(integer i = resultList.size()-1; i >= 0; i--){
                items.add(resultList[i]);  
            }
        }
    }
    
    public static void sortAddl(List<addlLine> items, Map<Id, SBQQ__QuoteLine__c> lines){
       List<addlLine> resultList = new List<addlLine>();
   
        //Create a map that can be used for sorting 
       Map<object, List<addlLine>> objectMap = new Map<object, List<addlLine>>();
       objectMap.put('Extras - Phone', new List<addlLine>());
       objectMap.put('Extras - Internet', new List<addlLine>());
       objectMap.put('Extras - Wireless', new List<addlLine>());
       objectMap.put('Business Tools', new List<addlLine>());
           
       for(addlLine ob : items){
            if (lines.get( ob.line.SBQQ__RequiredBy__c ) != null &&
                lines.get( ob.line.SBQQ__RequiredBy__c ).Category__c == 'Devices' ) {
                    
                if(objectMap.get( lines.get( ob.line.SBQQ__RequiredBy__c ).SBQQ__ProductOption__r.SBQQ__Feature__r.Name ) == null){
                    objectMap.put( lines.get( ob.line.SBQQ__RequiredBy__c ).SBQQ__ProductOption__r.SBQQ__Feature__r.Name , new List<addlLine>()); 
                }
                objectMap.get( lines.get( ob.line.SBQQ__RequiredBy__c ).SBQQ__ProductOption__r.SBQQ__Feature__r.Name ).add(ob);
            } else {
                if(objectMap.get( ob.line.SBQQ__ProductOption__r.SBQQ__Feature__r.Name ) == null){
                    objectMap.put( ob.line.SBQQ__ProductOption__r.SBQQ__Feature__r.Name , new List<addlLine>()); 
                }
                objectMap.get( ob.line.SBQQ__ProductOption__r.SBQQ__Feature__r.Name ).add(ob);
            }
        }       
        //Sort the keys
        List<object> keys = new List<object>(objectMap.keySet());
       
        items.clear();
        
        // This is done manually because for some reason 'Business Tools' kept bubbling up above 'Extras - Wireless'
        resultList.addAll(objectMap.get('Extras - Phone')); 
        items.addAll(objectMap.get('Extras - Phone'));
        resultList.addAll(objectMap.get('Extras - Internet')); 
        items.addAll(objectMap.get('Extras - Internet'));
        resultList.addAll(objectMap.get('Extras - Wireless')); 
        items.addAll(objectMap.get('Extras - Wireless'));
        resultList.addAll(objectMap.get('Business Tools')); 
        items.addAll(objectMap.get('Business Tools'));
        
        for(object key : keys){
            if (key == 'Extras - Phone' || key == 'Extras - Internet' || key == 'Extras - Wireless' || key == 'Business Tools') {
                continue;
            }
            system.debug('Loading ' + key+ ' lines');
            system.debug(objectMap.get(key));
            resultList.addAll(objectMap.get(key)); 
            items.addAll(objectMap.get(key));
        }
    }
}