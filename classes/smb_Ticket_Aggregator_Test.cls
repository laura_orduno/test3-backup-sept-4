/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 * Name - smb_Ticket_Aggregator_Test - This is common test class and some of common methods and variables being invoked in other 
                                       test classes of TT Aggregation project
 * 1. Created By - Sandip - 07 March 2016
 * 2. Modified By 
 */

@isTest(SeeAllData=false)
public class smb_Ticket_Aggregator_Test{
    public Static String srmMockBody = '<S:Envelope xmlns:env=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:S=\"http://schemas.xmlsoap.org/soap/envelope/\">' +
                        '<S:Body><ns0:searchTroubleTicketsByRegionalCustomerIdResponse ' +
                        'xmlns:ns0="http://xmlschema.tmi.telus.com/srv/SMO/ProblemMgmt/TQTroubleTicketSvcRequestResponse_v1" ' +
                        'xmlns:ns7="http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v9">' +
                        '<ns7:dateTimeStamp>2016-04-04T14:01:51.819-04:00</ns7:dateTimeStamp>'+
                        '<ns7:transactionId>33363561-d8da-469d-979f-08bb27315461</ns7:transactionId>'+
                        '<troubleTicketList><number>TERM-20160309-0002</number><statusCode>Open</statusCode>'+
                        '<customerAddress><streetName>null</streetName><cityName>null</cityName><districtName>null</districtName></customerAddress>'+
                        '<ownerTeamMember><teamMemberId>0</teamMemberId><ns9:systemEmployeeId>MGENDRE</ns9:systemEmployeeId><name>'+
                        '<ns9:givenName>MATHIEU</ns9:givenName><ns9:familyName>GENDREAU</ns9:familyName>'+
                        '<displayName>MATHIEU GENDREAU</displayName></name><workGroupName>GROUPE ISS RIMOUSKI</workGroupName></ownerTeamMember>'+
                        '<createdByTeamMember><teamMemberId>0</teamMemberId><ns9:systemEmployeeId>T859475</ns9:systemEmployeeId><name>'+
                        '<ns9:givenName>JEFFREY</ns9:givenName><ns9:familyName>LEFEBVRE</ns9:familyName>'+
                        '<displayName>JEFFREY LEFEBVRE</displayName></name><workGroupName>GROUPE ISS RIMOUSKI</workGroupName></createdByTeamMember>'+
                        '<statusCodeDescriptionList><ns7:description>' +
                        '<ns7:locale>fr</ns7:locale><ns7:descriptionText/></ns7:description></statusCodeDescriptionList><finalServiceCode>R_ORES_APP</finalServiceCode>'+
                        '<finalServiceCodeDescriptionList><ns7:description><ns7:locale>fr</ns7:locale>'+
                        '<ns7:descriptionText>REPARATION CLIENT OR EN-SERVICE APPAREIL TEL.</ns7:descriptionText>'+
                        '</ns7:description></finalServiceCodeDescriptionList><agencyCode>TERMINAUX</agencyCode><priorityCode>106</priorityCode>'+
                        '<priorityCodeDescriptionList><ns7:description><ns7:locale>fr</ns7:locale><ns7:descriptionText>OR &lt; 48 HRES</ns7:descriptionText>'+
                        '</ns7:description></priorityCodeDescriptionList><creationDateTime>20151112 104838</creationDateTime>'+
                        '<statusDateTime>2016-04-04T14:01:51.819-04:00</statusDateTime><customerName>VILLE DE GASPÉ</customerName><callCardModeCode>A</callCardModeCode>'+
                        '<systemOriginId>DCI</systemOriginId><customerPhoneNumber>(418)368-8543</customerPhoneNumber><customerTypeText>OR (AFFAIRE - CLTS MAJEURS)</customerTypeText>'+
                        '<customerAccountNumber>5004101884</customerAccountNumber><dispatchLevelCode>S206GASP</dispatchLevelCode>'+
                        '<dispositionCodeList><dispositionCode>te</dispositionCode><dispositionCodeName>nm</dispositionCodeName>'+
                        '<dispositionDateTime>20151112 104838</dispositionDateTime></dispositionCodeList>'+
                        '<activityList><activityCode>CREATE</activityCode>'+
                        '<statusCode>0</statusCode><statusDateTime>20160309 165753</statusDateTime><userId>7010</userId></activityList><commentList>'+
                        '<commentText>% TRBLE Beau comme un coeur</commentText><commentDateTime>20151112 104838</commentDateTime><userId>GTURCO2</userId>'+
                        '<commentCode>REMARKS</commentCode></commentList><address><civicNumber>4</civicNumber><streetName>ST-PATRICK</streetName>'+
                        '<streetTypeCode>AV</streetTypeCode><cityName>GASPE</cityName><cityAbbreviationText>GASPE</cityAbbreviationText>'+
                        '<completeAddressInformationText>4 AV ST-PATRICK</completeAddressInformationText><serviceAddressId>000001</serviceAddressId>'+
                        '</address></troubleTicketList></ns0:searchTroubleTicketsByRegionalCustomerIdResponse></S:Body></S:Envelope>';
   
    public Static String ttodsMockBody = '<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" '+
                          'xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">' +
                          '<soapenv:Body><TroubleTicketList xmlns=\"http://www.telus.com/schema/servicestatus/troubleticket\">' +
                          '<TroubleTicket xsi:type="TroubleTicket"><TELUSTicketId>1000119421</TELUSTicketId><description>N/A</description>'+
                          '<sourceTypeCode>CUSTOMER</sourceTypeCode><ticketTypeCode>Ticket</ticketTypeCode><severityCode>5 - MEDIUM</severityCode>'+
                          '<priorityCode>In Service</priorityCode><statusCode>Open</statusCode><subStatusCode>Resolved</subStatusCode>'+
                          '<customerDueDate>2013-06-05T00:00:00.000Z</customerDueDate><createDate>2013-05-24T07:13:19.000Z</createDate>'+
                          '<modifyDateTime>2015-06-15T23:49:40.000Z</modifyDateTime><createdByName>T833470</createdByName>'+
                          '<assignToWorkgroupName>Voice - Business Test</assignToWorkgroupName><comments>N/A</comments>'+
                          '<troubleTicketActivity><TELUSTroubleTicketId>1000119421</TELUSTroubleTicketId><activityTypeCode>Appointment</activityTypeCode>'+
                          '<TELUSActivityId>1-1PLE3BH</TELUSActivityId><statusCode>Assigned</statusCode><createdBy>SADMIN</createdBy>'+
                          '<resolutionCondition>In Service</resolutionCondition><workforceRequired>Installation Repair</workforceRequired>'+
                          '<workgroupTime>0</workgroupTime><totalActivityTime>0</totalActivityTime><CWCTotalTimeHours>0</CWCTotalTimeHours>'+
                          '<CWCTotalTimeMinutes>0</CWCTotalTimeMinutes><CWCEquipmentCost>0</CWCEquipmentCost><activityOwner>SADMIN</activityOwner>'+
                          '<activityComments>WFM Appointment assigned by </activityComments><notifyCustomerFlag>Y</notifyCustomerFlag>'+
                          '<plannedStartDateTime>2013-06-04T15:00:00.000Z</plannedStartDateTime>'+
                          '<plannedCompleteDateTime>2013-06-05T00:00:00.000Z</plannedCompleteDateTime>'+
                          '<activityCreateDateTime>2013-06-04T21:00:13.000Z</activityCreateDateTime><activityIdTypeCode>SIEBEL</activityIdTypeCode>'+
                          '<completedFlag>N</completedFlag><modifiedDateTime>2013-06-04T21:00:13.000Z</modifiedDateTime>'+
                          '<actvitiyLastUpdateTimeTTODS>2013-06-04T21:00:14.000Z</actvitiyLastUpdateTimeTTODS></troubleTicketActivity>'+
                          '<lastUpdateTimeTTODS>2015-11-26T20:04:59.000Z</lastUpdateTimeTTODS></TroubleTicket>'+
                          '</TroubleTicketList></soapenv:Body></soapenv:Envelope>';
        
    public class AsyncSoapServiceMockImpl implements WebServiceMock { 
        public void doInvoke( 
        Object stub, 
        Object request, 
        Map<String, Object> response, 
        String endpoint, 
        String soapAction, 
        String requestName, 
        String responseNS, 
        String responseName, 
        String responseType) { 
        // do nothing 
        } 
    } 
    public static Account createAccount(){
        RecordType recordType = [Select r.Id, r.DeveloperName From RecordType r where r.SobjectType = 'Account' and r.DeveloperName='RCID' limit 1];
        Account acc = new Account(
                 recordTypeId=recordType.Id,
                 Name= 'Test123',
                 BillingState = 'AB',
                 RCID__c = '0001058404_zzz',
                 BAN_CAN__c = '0001058404_zzz',
                 Phone = '123-120-1201'
         );
        insert acc;
        try{
            SMBCare_WebServices__c obj = new SMBCare_WebServices__c();
            obj.Name = 'SRM_EndPoint';
            obj.value__c = 'https://xmlgwy-pt1.telus.com:9030/dv01/SMO/ProblemMgmt/TQTroubleTicketSvc-1_2/TQTroubleTicketSvc_v1_2_vs0';
            insert obj;
        }catch (Exception ex){}
        try{
            SMBCare_WebServices__c obj3 = new SMBCare_WebServices__c();
            obj3.Name = 'TTODS_EndPoint';
            obj3.value__c = 'https://xmlgwy-pt1.telus.com:9030/pt01/SMO/ProblemMgmt/TroubleTicketODSQueryService_v1_5_vs0';
            insert obj3;
        }catch (Exception ex){}
            
        try{
            SMBCare_WebServices__c obj1 = new SMBCare_WebServices__c();
            obj1.Name = 'username';
            obj1.value__c = 'test';
            SMBCare_WebServices__c obj2 = new SMBCare_WebServices__c();
            obj2.Name = 'password';
            obj2.value__c = 'test';
            insert obj1;
            insert obj2;
        }catch (Exception ex){}
        try{
            TT_Aggrgation_Status_Mapping__c obj4 = new TT_Aggrgation_Status_Mapping__c();
            obj4.name = 'Open';
            obj4.value__c= 'Open';
            obj4.Sort_Priority__c = 1;
        }catch(Exception ex){}
        return acc;     
    }
    
    public static void putParameter(PageReference pageRef, String accId, String tktId, String src, String lmt, String xlmt){
        pageRef.getParameters().put('id', accId);
        pageRef.getParameters().put('ticketID', tktId);
        pageRef.getParameters().put('source', src);
        pageRef.getParameters().put('recLimit', lmt);
        pageRef.getParameters().put('upToXLatestModified', xlmt);
    }
    
    public static testmethod void testGetTickets() {
        createAccount();
        Test.setMock(WebServiceMock.class, new AsyncSoapServiceMockImpl());
        Test.startTest();
        smb_Ticket_Aggregator ttObj = new smb_Ticket_Aggregator();
        smb_Ticket_Aggregator.logError('testGetTickets','testId','testMsg','testStack');
        TicketVO.ServiceRequestAndResponseParameter reqResObj = new TicketVO.ServiceRequestAndResponseParameter();
        // set the input parameter which need pass to aggrgate method
        reqResObj.TIDValue = 'T1234567';
        reqResObj.RCIDValue = '0001058404';
        reqResObj.callBackMethodName = 'processTTAggregationResponse_test'; // Specify callback method name, this method should be present in VF controller
        reqResObj.recordLimits = 200;
        reqResObj.upToXLatestModified = 200;
        Continuation conti = ttObj.getTroubleTickets(reqResObj);
        Map<String, HttpRequest> requests = conti.getRequests();
        system.debug('@@@TEst Request ' + requests);
        system.debug('@@@TEst Request ' + requests.size());
        system.assert(requests.size() == 2); //19 May 2016 - Sandip - Removed commentted line for deploying SRM changes
        //TTODS
        HttpResponse response = new HttpResponse();
        String mockBody = ttodsMockBody;
        response.setBody(mockBody); 
        Test.setContinuationResponse('Continuation-1', response);
        //SRM
        HttpResponse response1 = new HttpResponse();
        String mockBody1 = srmMockBody;
        response1.setBody(mockBody1); 
        Test.setContinuationResponse('Continuation-2', response1);
        
        Object result = Test.invokeContinuationMethod(ttObj, conti); 
        Test.stopTest(); 
    }
    public static testmethod void testGetIndividualTickets() {
        createAccount();
        Test.setMock(WebServiceMock.class, new AsyncSoapServiceMockImpl());
        Test.startTest();
        smb_Ticket_Aggregator ttObj = new smb_Ticket_Aggregator();
        smb_Ticket_Aggregator.logError('testClass','testId','testMsg','testStack');
        TicketVO.ServiceRequestAndResponseParameter reqResObj = new TicketVO.ServiceRequestAndResponseParameter();
        // set the input parameter which need pass to aggrgate method
        reqResObj.callBackMethodName = 'processTTAggregationResponse_test'; // Specify callback method name, this method should be present in VF controller
        reqResObj.recordLimits = 200;
        reqResObj.upToXLatestModified = 200;
        reqResObj.ticketIdToSearch = 'TERM-20160309-0002';
        reqResObj.source = TicketVO.SRC_SRM;
        Continuation conti = ttObj.getTTDetailBySource(reqResObj);
        Map<String, HttpRequest> requests = conti.getRequests();
        system.debug('@@@TEst Request ' + requests);
        system.debug('@@@TEst Request ' + requests.size());
        // 19 May 2016 - Sandip - Removed commentted line for deploying SRM changes
        system.assert(requests.size() == 1);
        //SRM
        HttpResponse response1 = new HttpResponse();
        String mockBody1 = srmMockBody;
        response1.setBody(mockBody1); 
        Test.setContinuationResponse('Continuation-1', response1);
        Object result = Test.invokeContinuationMethod(ttObj, conti);
        Test.stopTest(); 
    } 
    public static testMethod void testOtherWSDLClassesPart1(){
        createAccount();
        Test.startTest();
        SRM1_2_TQTroubleTicketTypes_v1.Comment testObj1 = new SRM1_2_TQTroubleTicketTypes_v1.Comment();
        SRM1_2_TQTroubleTicketTypes_v1.TeamMemberBasicDetails testObj2 = new SRM1_2_TQTroubleTicketTypes_v1.TeamMemberBasicDetails();
        SRM1_2_TQTroubleTicketTypes_v1.ServiceCode testObj3 = new SRM1_2_TQTroubleTicketTypes_v1.ServiceCode();
        SRM1_2_TQTroubleTicketTypes_v1.Address testObj4 = new SRM1_2_TQTroubleTicketTypes_v1.Address();
        SRM1_2_TQTroubleTicketTypes_v1.Activity testObj5 = new SRM1_2_TQTroubleTicketTypes_v1.Activity();
        SRM1_2_TQTroubleTicketTypes_v1.TroubleTicket testObj6 = new SRM1_2_TQTroubleTicketTypes_v1.TroubleTicket();
        SRM1_2_TQTroubleTicketTypes_v1.DispositionCode testObj7 = new SRM1_2_TQTroubleTicketTypes_v1.DispositionCode();
        
        SRM1_2_Exceptions_v3.Message testObj8 = new SRM1_2_Exceptions_v3.Message();
        SRM1_2_Exceptions_v3.FaultExceptionDetailsType testObj9 = new SRM1_2_Exceptions_v3.FaultExceptionDetailsType();
        SRM1_2_Exceptions_v3.MessageList testObj10 = new SRM1_2_Exceptions_v3.MessageList();
        
        SRM1_2_HumanResourcesCommon_v2.financialReleaseLimitCode_element testObj11 = new SRM1_2_HumanResourcesCommon_v2.financialReleaseLimitCode_element();
        SRM1_2_HumanResourcesCommon_v2.PersonnelSubAreaDetails testObj12 = new SRM1_2_HumanResourcesCommon_v2.PersonnelSubAreaDetails();
        SRM1_2_HumanResourcesCommon_v2.PositionDetails testObj13 = new SRM1_2_HumanResourcesCommon_v2.PositionDetails();
        SRM1_2_HumanResourcesCommon_v2.WorkStation_element testObj14 = new SRM1_2_HumanResourcesCommon_v2.WorkStation_element();
        SRM1_2_HumanResourcesCommon_v2.Name testObj15 = new SRM1_2_HumanResourcesCommon_v2.Name();
        SRM1_2_HumanResourcesCommon_v2.positionCostCentreRelationshipCode_element testObj16 = new SRM1_2_HumanResourcesCommon_v2.positionCostCentreRelationshipCode_element();
        SRM1_2_HumanResourcesCommon_v2.WorkLocationDetails testObj17 = new SRM1_2_HumanResourcesCommon_v2.WorkLocationDetails();
        SRM1_2_HumanResourcesCommon_v2.CorporateJobCategoryDetails testObj18 = new SRM1_2_HumanResourcesCommon_v2.CorporateJobCategoryDetails();
        SRM1_2_HumanResourcesCommon_v2.corporateJobCategoryRelationshipCode_element testObj19 = new SRM1_2_HumanResourcesCommon_v2.corporateJobCategoryRelationshipCode_element();
        SRM1_2_HumanResourcesCommon_v2.Job testObj20 = new SRM1_2_HumanResourcesCommon_v2.Job();
        SRM1_2_HumanResourcesCommon_v2.OrganizationalAssignmentDetails testObj21 = new SRM1_2_HumanResourcesCommon_v2.OrganizationalAssignmentDetails();
        SRM1_2_HumanResourcesCommon_v2.organizationManagedByPosRelationshipCode_element testObj22 = new SRM1_2_HumanResourcesCommon_v2.organizationManagedByPosRelationshipCode_element();
        SRM1_2_HumanResourcesCommon_v2.PersonnelSubAreaDetails testObj23 = new SRM1_2_HumanResourcesCommon_v2.PersonnelSubAreaDetails();        
        SRM1_2_HumanResourcesCommon_v2.organizationParentCode_element testObj24 = new SRM1_2_HumanResourcesCommon_v2.organizationParentCode_element();        
        SRM1_2_HumanResourcesCommon_v2.Position testObj25 = new SRM1_2_HumanResourcesCommon_v2.Position();        
        SRM1_2_HumanResourcesCommon_v2.ContactMethodDetails testObj26 = new SRM1_2_HumanResourcesCommon_v2.ContactMethodDetails();        
        SRM1_2_HumanResourcesCommon_v2.TeamMemberBusinessDetails testObj27 = new SRM1_2_HumanResourcesCommon_v2.TeamMemberBusinessDetails();        
        SRM1_2_HumanResourcesCommon_v2.TeamMemberOrganizationDetails testObj28 = new SRM1_2_HumanResourcesCommon_v2.TeamMemberOrganizationDetails();        
        SRM1_2_HumanResourcesCommon_v2.FieldDetail testObj29 = new SRM1_2_HumanResourcesCommon_v2.FieldDetail();        
        SRM1_2_HumanResourcesCommon_v2.WorkAddressDetails testObj30 = new SRM1_2_HumanResourcesCommon_v2.WorkAddressDetails();        
        SRM1_2_HumanResourcesCommon_v2.positionToPositionCategoryRelationshipCode_element testObj31 = new SRM1_2_HumanResourcesCommon_v2.positionToPositionCategoryRelationshipCode_element();        
        SRM1_2_HumanResourcesCommon_v2.TeamMemberPersonalDetails testObj32 = new SRM1_2_HumanResourcesCommon_v2.TeamMemberPersonalDetails();        
        SRM1_2_HumanResourcesCommon_v2.positionToJobRelationshipCode_element testObj33 = new SRM1_2_HumanResourcesCommon_v2.positionToJobRelationshipCode_element();        
        SRM1_2_HumanResourcesCommon_v2.positionSupportedByPositionRelationshipCode_element testObj34 = new SRM1_2_HumanResourcesCommon_v2.positionSupportedByPositionRelationshipCode_element();        
        SRM1_2_HumanResourcesCommon_v2.CompanyAreaDetails testObj35 = new SRM1_2_HumanResourcesCommon_v2.CompanyAreaDetails();        
        SRM1_2_HumanResourcesCommon_v2.TeamMemberData testObj36 = new SRM1_2_HumanResourcesCommon_v2.TeamMemberData();        
        SRM1_2_HumanResourcesCommon_v2.Organization testObj37 = new SRM1_2_HumanResourcesCommon_v2.Organization();        
        SRM1_2_HumanResourcesCommon_v2.LoginTypeDetails testObj38 = new SRM1_2_HumanResourcesCommon_v2.LoginTypeDetails();        
        SRM1_2_HumanResourcesCommon_v2.OrganizationalUnitDetails testObj39 = new SRM1_2_HumanResourcesCommon_v2.OrganizationalUnitDetails();        
        SRM1_2_HumanResourcesCommon_v2.FinancialReleaseLimitDetails testObj40 = new SRM1_2_HumanResourcesCommon_v2.FinancialReleaseLimitDetails();        
        SRM1_2_HumanResourcesCommon_v2.BaseFields testObj41 = new SRM1_2_HumanResourcesCommon_v2.BaseFields();        
        SRM1_2_HumanResourcesCommon_v2.JobDetails testObj42 = new SRM1_2_HumanResourcesCommon_v2.JobDetails();        
        SRM1_2_HumanResourcesCommon_v2.TeamMemberBasicDetails testObj43 = new SRM1_2_HumanResourcesCommon_v2.TeamMemberBasicDetails();        
        SRM1_2_HumanResourcesCommon_v2.FunctionalGroupDetails testObj44 = new SRM1_2_HumanResourcesCommon_v2.FunctionalGroupDetails();        
        SRM1_2_HumanResourcesCommon_v2.preferredName_element testObj45 = new SRM1_2_HumanResourcesCommon_v2.preferredName_element();        
        SRM1_2_HumanResourcesCommon_v2.Value_element testObj46 = new SRM1_2_HumanResourcesCommon_v2.Value_element();        
        SRM1_2_HumanResourcesCommon_v2.CostCentre testObj47 = new SRM1_2_HumanResourcesCommon_v2.CostCentre();        
        SRM1_2_HumanResourcesCommon_v2.clliCode_element testObj48 = new SRM1_2_HumanResourcesCommon_v2.clliCode_element();        
        SRM1_2_HumanResourcesCommon_v2.TeamMemberDetails testObj49 = new SRM1_2_HumanResourcesCommon_v2.TeamMemberDetails();        
        SRM1_2_HumanResourcesCommon_v2.legacyId_element testObj50 = new SRM1_2_HumanResourcesCommon_v2.legacyId_element();        
        SRM1_2_HumanResourcesCommon_v2.CostCentreDetails testObj51 = new SRM1_2_HumanResourcesCommon_v2.CostCentreDetails();
        SRM1_2_HumanResourcesCommon_v2.PositionCategoryDetails testObj52 = new SRM1_2_HumanResourcesCommon_v2.PositionCategoryDetails();                
        SRM1_2_HumanResourcesCommon_v2.positionBelongsToOrgRelationshipCode_element testObj53 = new SRM1_2_HumanResourcesCommon_v2.positionBelongsToOrgRelationshipCode_element();                
        SRM1_2_HumanResourcesCommon_v2.LoginDetails testObj54 = new SRM1_2_HumanResourcesCommon_v2.LoginDetails();                
        SRM1_2_HumanResourcesCommon_v2.EmploymentStatusTypeDetails testObj55 = new SRM1_2_HumanResourcesCommon_v2.EmploymentStatusTypeDetails();                
        SRM1_2_HumanResourcesCommon_v2.financialReleaseLimitRelationshipCode_element testObj56 = new SRM1_2_HumanResourcesCommon_v2.financialReleaseLimitRelationshipCode_element();                
        SRM1_2_HumanResourcesCommon_v2.TeamMemberPositionDetails testObj57 = new SRM1_2_HumanResourcesCommon_v2.TeamMemberPositionDetails();                
        SRM1_2_HumanResourcesCommon_v2.BargainingUnitDetails testObj58 = new SRM1_2_HumanResourcesCommon_v2.BargainingUnitDetails();                
        SRM1_2_HumanResourcesCommon_v2.EmploymentTypeDetails testObj59 = new SRM1_2_HumanResourcesCommon_v2.EmploymentTypeDetails(); 
        SRM1_2_HumanResourcesCommon_v2.PersonnelAreaDetails testObj60 = new SRM1_2_HumanResourcesCommon_v2.PersonnelAreaDetails();
        SRM1_2_HumanResourcesCommon_v2.Floor_element testObj61 = new SRM1_2_HumanResourcesCommon_v2.Floor_element();
        
        SRM1_2_ping_v2.ping_element testObj62 = new SRM1_2_ping_v2.ping_element();
        SRM1_2_ping_v2.pingResponse_element testObj63 = new SRM1_2_ping_v2.pingResponse_element();        
        SRM1_2_ping_v2.NameValuePair testObj64 = new SRM1_2_ping_v2.NameValuePair();        
        SRM1_2_ping_v2.StatsInfo testObj65 = new SRM1_2_ping_v2.StatsInfo();        
        SRM1_2_ping_v2.DependencyStatus testObj66 = new SRM1_2_ping_v2.DependencyStatus();        
        SRM1_2_ping_v2.DependencyStatusList testObj67 = new SRM1_2_ping_v2.DependencyStatusList();
        SRM1_2_ping_v2.BuildInfo testObj68 = new SRM1_2_ping_v2.BuildInfo();
        SRM1_2_ping_v2.PingStats testObj69 = new SRM1_2_ping_v2.PingStats();
        SRM1_2_ping_v2.OperationStats testObj70 = new SRM1_2_ping_v2.OperationStats();  
        SRM1_2_ping_v2.CurrentStatsWindow testObj71 = new SRM1_2_ping_v2.CurrentStatsWindow();        
        SRM1_2_ping_v2.PingStatsExtension testObj72 = new SRM1_2_ping_v2.PingStatsExtension();        
        
        SRM1_2_TQCustomerBillingTypes_v1.BillingAddress testObj73 = new SRM1_2_TQCustomerBillingTypes_v1.BillingAddress();
        SRM1_2_TQCustomerBillingTypes_v1.Payment testObj74 = new SRM1_2_TQCustomerBillingTypes_v1.Payment();
        SRM1_2_TQCustomerBillingTypes_v1.BillingSummary testObj75 = new SRM1_2_TQCustomerBillingTypes_v1.BillingSummary();
        SRM1_2_TQCustomerBillingTypes_v1.HistoricalUsageStatistics testObj76 = new SRM1_2_TQCustomerBillingTypes_v1.HistoricalUsageStatistics();
        
        SRM1_2_TQCustomerTypes_v1.ServiceAddress testObj77 = new SRM1_2_TQCustomerTypes_v1.ServiceAddress();
        SRM1_2_TQCustomerTypes_v1.Address testObj78 = new SRM1_2_TQCustomerTypes_v1.Address();
        SRM1_2_TQCustomerTypes_v1.Customer testObj79 = new SRM1_2_TQCustomerTypes_v1.Customer();
        SRM1_2_TQCustomerTypes_v1.MobilePackage testObj80 = new SRM1_2_TQCustomerTypes_v1.MobilePackage();
        SRM1_2_TQCustomerTypes_v1.CustomerProfile testObj81 = new SRM1_2_TQCustomerTypes_v1.CustomerProfile();
        SRM1_2_TQCustomerTypes_v1.ParameterCode testObj82 = new SRM1_2_TQCustomerTypes_v1.ParameterCode();
        SRM1_2_TQCustomerTypes_v1.Contact testObj83 = new SRM1_2_TQCustomerTypes_v1.Contact();
        SRM1_2_TQCustomerTypes_v1.CustomerSummary testObj84 = new SRM1_2_TQCustomerTypes_v1.CustomerSummary();
        SRM1_2_TQCustomerTypes_v1.CustomerTelephoneNumber testObj85 = new SRM1_2_TQCustomerTypes_v1.CustomerTelephoneNumber();
        SRM1_2_TQCustomerTypes_v1.MobileAccount testObj86 = new SRM1_2_TQCustomerTypes_v1.MobileAccount();
        SRM1_2_TQCustomerTypes_v1.BusinessCustomer testObj87 = new SRM1_2_TQCustomerTypes_v1.BusinessCustomer();
        SRM1_2_TQCustomerTypes_v1.CustomerBillingAddress testObj88 = new SRM1_2_TQCustomerTypes_v1.CustomerBillingAddress();
        SRM1_2_TQCustomerTypes_v1.HeadBusinessCustomer testObj89 = new SRM1_2_TQCustomerTypes_v1.HeadBusinessCustomer();
        SRM1_2_TQCustomerTypes_v1.CustomerCriteria testObj90 = new SRM1_2_TQCustomerTypes_v1.CustomerCriteria();
        
        SRM1_2_TQTroubleTicketSvcReqRes_v1.searchTroubleTicketsByTicketNumberResponse_element testObj91 =
                                    new SRM1_2_TQTroubleTicketSvcReqRes_v1.searchTroubleTicketsByTicketNumberResponse_element();
        SRM1_2_TQTroubleTicketSvcReqRes_v1.createTicket_element testObj92 =
                                    new SRM1_2_TQTroubleTicketSvcReqRes_v1.createTicket_element();
        SRM1_2_TQTroubleTicketSvcReqRes_v1.searchTroubleTicketsByRegionalCustomerId_element testObj93 =
                                    new SRM1_2_TQTroubleTicketSvcReqRes_v1.searchTroubleTicketsByRegionalCustomerId_element();
        SRM1_2_TQTroubleTicketSvcReqRes_v1.createTicketResponse_element testObj94 =
                                    new SRM1_2_TQTroubleTicketSvcReqRes_v1.createTicketResponse_element();
        SRM1_2_TQTroubleTicketSvcReqRes_v1.createTicketForTQCustomer_element testObj95 =
                                    new SRM1_2_TQTroubleTicketSvcReqRes_v1.createTicketForTQCustomer_element();
        SRM1_2_TQTroubleTicketSvcReqRes_v1.searchTroubleTicketsByRegionalCustomerIdResponse_element testObj96 =
                                    new SRM1_2_TQTroubleTicketSvcReqRes_v1.searchTroubleTicketsByRegionalCustomerIdResponse_element();
        SRM1_2_TQTroubleTicketSvcReqRes_v1.searchTroubleTicketsLiveResponse_element testObj97 =
                                    new SRM1_2_TQTroubleTicketSvcReqRes_v1.searchTroubleTicketsLiveResponse_element();
        SRM1_2_TQTroubleTicketSvcReqRes_v1.searchTroubleTicketsLive_element testObj98 =
                                    new SRM1_2_TQTroubleTicketSvcReqRes_v1.searchTroubleTicketsLive_element();
        SRM1_2_TQTroubleTicketSvcReqRes_v1.createTicketSpecForTQCustomer_element testObj99 =
                                    new SRM1_2_TQTroubleTicketSvcReqRes_v1.createTicketSpecForTQCustomer_element();
        SRM1_2_TQTroubleTicketSvcReqRes_v1.createTicketForTQCustomerResponse_element testObj100 =
                                    new SRM1_2_TQTroubleTicketSvcReqRes_v1.createTicketForTQCustomerResponse_element();
        SRM1_2_TQTroubleTicketSvcReqRes_v1.searchTroubleTicketsResponse_element testObj101 =
                                    new SRM1_2_TQTroubleTicketSvcReqRes_v1.searchTroubleTicketsResponse_element();
        SRM1_2_TQTroubleTicketSvcReqRes_v1.searchTroubleTicketsByTicketNumber_element testObj102 =
                                    new SRM1_2_TQTroubleTicketSvcReqRes_v1.searchTroubleTicketsByTicketNumber_element();
        SRM1_2_TQTroubleTicketSvcReqRes_v1.searchTroubleTickets_element testObj103 =
                                    new SRM1_2_TQTroubleTicketSvcReqRes_v1.searchTroubleTickets_element();
        SRM1_2_TQTroubleTicketSvcReqRes_v1.createTicketSpecForTQCustomerResponse_element testObj104 =
                                    new SRM1_2_TQTroubleTicketSvcReqRes_v1.createTicketSpecForTQCustomerResponse_element();
        
        SRM1_2_EnterpriseCommonTypes_v1.CodeDescTextList testObj105 = new SRM1_2_EnterpriseCommonTypes_v1.CodeDescTextList();
        SRM1_2_EnterpriseCommonTypes_v1.CodeDescText testObj106 = new SRM1_2_EnterpriseCommonTypes_v1.CodeDescText();
        SRM1_2_EnterpriseCommonTypes_v1.MessageType testObj107 = new SRM1_2_EnterpriseCommonTypes_v1.MessageType();
        
        SRM1_2_EnterpriseCommonTypes_v9.OrganizationName testObj108 = new SRM1_2_EnterpriseCommonTypes_v9.OrganizationName();  
        SRM1_2_EnterpriseCommonTypes_v9.Description testObj109 = new SRM1_2_EnterpriseCommonTypes_v9.Description();
        SRM1_2_EnterpriseCommonTypes_v9.Message testObj110 = new SRM1_2_EnterpriseCommonTypes_v9.Message();
        SRM1_2_EnterpriseCommonTypes_v9.HealthCard testObj111 = new SRM1_2_EnterpriseCommonTypes_v9.HealthCard();
        SRM1_2_EnterpriseCommonTypes_v9.PartyName testObj112 = new SRM1_2_EnterpriseCommonTypes_v9.PartyName();
        SRM1_2_EnterpriseCommonTypes_v9.Name testObj113 = new SRM1_2_EnterpriseCommonTypes_v9.Name();
        SRM1_2_EnterpriseCommonTypes_v9.MultilingualDescriptionList testObj114 = new SRM1_2_EnterpriseCommonTypes_v9.MultilingualDescriptionList();
        SRM1_2_EnterpriseCommonTypes_v9.IndividualName testObj115 = new SRM1_2_EnterpriseCommonTypes_v9.IndividualName();
        SRM1_2_EnterpriseCommonTypes_v9.MarketSegment testObj116 = new SRM1_2_EnterpriseCommonTypes_v9.MarketSegment();
        SRM1_2_EnterpriseCommonTypes_v9.Passport testObj117 = new SRM1_2_EnterpriseCommonTypes_v9.Passport();
        SRM1_2_EnterpriseCommonTypes_v9.DriversLicense testObj118 = new SRM1_2_EnterpriseCommonTypes_v9.DriversLicense();
        SRM1_2_EnterpriseCommonTypes_v9.CodeDescText testObj119 = new SRM1_2_EnterpriseCommonTypes_v9.CodeDescText();
        SRM1_2_EnterpriseCommonTypes_v9.MessageType testObj120 = new SRM1_2_EnterpriseCommonTypes_v9.MessageType();
        SRM1_2_EnterpriseCommonTypes_v9.AuditInfo testObj121 = new SRM1_2_EnterpriseCommonTypes_v9.AuditInfo();
        SRM1_2_EnterpriseCommonTypes_v9.UnknownName testObj122 = new SRM1_2_EnterpriseCommonTypes_v9.UnknownName();
        SRM1_2_EnterpriseCommonTypes_v9.Rate testObj123 = new SRM1_2_EnterpriseCommonTypes_v9.Rate();
        SRM1_2_EnterpriseCommonTypes_v9.Quantity testObj124 = new SRM1_2_EnterpriseCommonTypes_v9.Quantity();
        SRM1_2_EnterpriseCommonTypes_v9.BankAccount testObj125 = new SRM1_2_EnterpriseCommonTypes_v9.BankAccount();
        SRM1_2_EnterpriseCommonTypes_v9.Duration testObj126 = new SRM1_2_EnterpriseCommonTypes_v9.Duration();
        SRM1_2_EnterpriseCommonTypes_v9.ResponseMessage testObj127 = new SRM1_2_EnterpriseCommonTypes_v9.ResponseMessage();
        SRM1_2_EnterpriseCommonTypes_v9.MonetaryAmt testObj128 = new SRM1_2_EnterpriseCommonTypes_v9.MonetaryAmt();
        SRM1_2_EnterpriseCommonTypes_v9.MultilingualNameList testObj129 = new SRM1_2_EnterpriseCommonTypes_v9.MultilingualNameList();
        SRM1_2_EnterpriseCommonTypes_v9.CreditCard testObj130 = new SRM1_2_EnterpriseCommonTypes_v9.CreditCard();
        SRM1_2_EnterpriseCommonTypes_v9.MultilingualCodeDescTextList testObj131 = new SRM1_2_EnterpriseCommonTypes_v9.MultilingualCodeDescTextList();
        SRM1_2_EnterpriseCommonTypes_v9.brandType testObj132 = new SRM1_2_EnterpriseCommonTypes_v9.brandType();
        SRM1_2_EnterpriseCommonTypes_v9.ProvincialIdCard testObj133 = new SRM1_2_EnterpriseCommonTypes_v9.ProvincialIdCard();
        SRM1_2_EnterpriseCommonTypes_v9.ResponseStatus testObj134 = new SRM1_2_EnterpriseCommonTypes_v9.ResponseStatus();
        SRM1_2_EnterpriseCommonTypes_v9.MultilingualCodeDescriptionList testObj135 = new SRM1_2_EnterpriseCommonTypes_v9.MultilingualCodeDescriptionList();
                               
        Test.stopTest(); 
    }
    public static testMethod void testOtherWSDLClassesPart2(){
        createAccount();
        Test.startTest();
        SRM1_2_TQTroubleTicketSvc_1.TQTroubleTicketSvcPort testObj = new SRM1_2_TQTroubleTicketSvc_1.TQTroubleTicketSvcPort();
        AsyncSRM1_2_TQTroubleTicketSvc_1.AsyncTQTroubleTicketSvcPort testObj1 = new AsyncSRM1_2_TQTroubleTicketSvc_1.AsyncTQTroubleTicketSvcPort();
        Continuation cont = new Continuation(1);
        try{testObj.createTicketSpecForTQCustomer('test1','test2','test3','test4','test5','test6','test7','test8');}catch(Exception ex){}
        try{testObj.searchTroubleTickets('test1', new SRM1_2_TQBaseCriteriaTypes_v1.RangeCriteria());}catch(Exception ex){}
        try{testObj.createTicketForTQCustomer('test1','test2','test3','test4','test5','test6','test7','test8');}catch(Exception ex){}
        try{testObj.ping('test1', false);}catch(Exception ex){}
        try{testObj.searchTroubleTicketsByTicketNumber('test1');}catch(Exception ex){}
        try{testObj.searchTroubleTicketsLive('test1', new SRM1_2_TQBaseCriteriaTypes_v1.RangeCriteria());}catch(Exception ex){}
        try{testObj.searchTroubleTicketsByRegionalCustomerId('test1', new SRM1_2_TQBaseCriteriaTypes_v1.RangeCriteria());}catch(Exception ex){}
        try{testObj1.beginCreateTicketSpecForTQCustomer(cont,'test1','test2','test3','test4','test5','test6','test7','test8');}catch(Exception ex){}
        try{testObj1.beginSearchTroubleTickets(cont,'test1', new SRM1_2_TQBaseCriteriaTypes_v1.RangeCriteria());}catch(Exception ex){}
        try{testObj1.beginCreateTicketForTQCustomer(cont,'test1','test2','test3','test4','test5','test6','test7','test8');}catch(Exception ex){}
        try{testObj1.beginPing(cont,'test1', false);}catch(Exception ex){}
        try{testObj1.beginSearchTroubleTicketsByTicketNumber(cont,'test1');}catch(Exception ex){}
        try{testObj1.beginSearchTroubleTicketsLive(cont,'test1', new SRM1_2_TQBaseCriteriaTypes_v1.RangeCriteria());}catch(Exception ex){}
        try{
            AsyncSRM1_2_TQTroubleTicketSvcReqRes_v1.createTicketResponse_elementFuture testObj3 = new AsyncSRM1_2_TQTroubleTicketSvcReqRes_v1.createTicketResponse_elementFuture();
            testObj3.getValue();
        }catch(Exception ex){}
        try{
            AsyncSRM1_2_TQTroubleTicketSvcReqRes_v1.createTicketSpecForTQCustomerResponse_elementFuture testObj4 = new AsyncSRM1_2_TQTroubleTicketSvcReqRes_v1.createTicketSpecForTQCustomerResponse_elementFuture();
            testObj4.getValue();
        }catch(Exception ex){}
        try{
            AsyncSRM1_2_TQTroubleTicketSvcReqRes_v1.searchTroubleTicketsResponse_elementFuture testObj5 = new AsyncSRM1_2_TQTroubleTicketSvcReqRes_v1.searchTroubleTicketsResponse_elementFuture();
            testObj5.getValue();
        }catch(Exception ex){}
        try{
            AsyncSRM1_2_TQTroubleTicketSvcReqRes_v1.createTicketForTQCustomerResponse_elementFuture testObj6 = new AsyncSRM1_2_TQTroubleTicketSvcReqRes_v1.createTicketForTQCustomerResponse_elementFuture();
            testObj6.getValue();
        }catch(Exception ex){}
        try{
            AsyncSRM1_2_TQTroubleTicketSvcReqRes_v1.searchTroubleTicketsByTicketNumberResponse_elementFuture testObj7 = new AsyncSRM1_2_TQTroubleTicketSvcReqRes_v1.searchTroubleTicketsByTicketNumberResponse_elementFuture();
            testObj7.getValue();
        }catch(Exception ex){}
        try{
            AsyncSRM1_2_TQTroubleTicketSvcReqRes_v1.searchTroubleTicketsLiveResponse_elementFuture testObj8 = new AsyncSRM1_2_TQTroubleTicketSvcReqRes_v1.searchTroubleTicketsLiveResponse_elementFuture();
            testObj8.getValue();
        }catch(Exception ex){}
        try{
            AsyncSRM1_2_TQTroubleTicketSvcReqRes_v1.searchTroubleTicketsByRegionalCustomerIdResponse_elementFuture testObj8 = new AsyncSRM1_2_TQTroubleTicketSvcReqRes_v1.searchTroubleTicketsByRegionalCustomerIdResponse_elementFuture();
            testObj8.getValue();
        }catch(Exception ex){}
        
        Asyncsmb_TTODS_queryticketservice.AsyncQueryTicketBinding_v1_5 testObj9 = new Asyncsmb_TTODS_queryticketservice.AsyncQueryTicketBinding_v1_5();
        try{testObj9.beginGetTicketsByQuery(cont,'test','test');}catch(Exception ex){}
        AsyncSmb_TTODS_troubleticket.TroubleTicketListFuture testObj10 = new AsyncSmb_TTODS_troubleticket.TroubleTicketListFuture();
        try{testObj10.getValue();}catch(Exception ex){}
             
        Test.stopTest();
        
    } 
}