@isTest
private class VITILcare_LynxCreate_Test {
	@isTest(SeeAllData=false) static void ENTPNewCaseTriggerHelperVitil() {
        trac_TriggerHandlerBase.blockTrigger = true;
        test.startTest();
      ENTPTestUtils.createSMBCareWebServicesCustomSetting();
        //User u = MBRTestUtils.createENTPPortalUser('test-user-1', false, 'en_US'); 
        Account a = new Account(Name = 'Unit Test Account');
          a.Remedy_Business_Unit_Id__c = 'TCI';
            a.Remedy_Company_Id__c = 'TCI';         
        a.strategic__c = false;
        insert a;

        Contact c = new Contact(Email = 'test-user-1@unit-test.com', LastName = 'Test', AccountId = a.Id);
        c.Remedy_PIN__c = '70208';
         insert c; 
          Profile p = [SELECT Id FROM Profile WHERE UserType='CSPLitePortal' LIMIT 1]; 
         User u = new User(Alias = 'TestUser', Email='test-user-1@unit-test.com', ContactId=c.Id, 
                            EmailEncodingKey='UTF-8', FirstName='Unit', LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', CommunityNickname='test-user-1UNTST', /*UserRoleId = portalRole.Id*/ProfileId=p.Id,
                       	TimeZoneSidKey='America/Los_Angeles', 
                            UserName='test-user-1@unit-test.com');

        u.Customer_Portal_Role__c = null; 
        u.Customer_Portal_Name__c = 'vitilcare';
        
        insert u;        
        System.runAs(u) {
             ENTPTestUtils.createENTPCases(1,a.id);
             Case parentCase = [SELECT Id, ContactId, CaseNumber, My_Business_Requests_Type__c, LastModifiedDate, AccountId FROM Case ORDER BY CreatedDate DESC LIMIT 1];
            System.debug('AccountID=:'+a.id);
            ENTPNewCaseTriggerHelper.SendLynxVitil(parentCase.id);
                      
            
        // ENTPNewCaseTriggerHelper.SendRemedyCreate(parentCase.id);
        }
        test.stopTest();
        trac_TriggerHandlerBase.blockTrigger = false;
    }	
	
}