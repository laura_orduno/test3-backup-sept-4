public class ProcessBuilderAction {
    
    /**
     * Explanation: This method will be invoked from the Process Builder when the Salesforce admin
     * does not want to execute any actions.
     * 
     * Once this idea is implemented https://success.salesforce.com/ideaView?id=08730000000DqGRAA0
     * this code will no longer be needed
     * 
     * June 28, 2017/Allan Roszmann - Created for Voc 'Closed Case Survey' Process Builder steps,
     * but this class can be used for any other Process Builder step requiring 'no action' when
     * filter criteria is met.
     * 
     **/
    
    @InvocableMethod
    public static void DoNothing() {}

}