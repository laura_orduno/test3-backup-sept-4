@isTest
private class VlProductAvailabilityImplTest {
    @isTest
    private static void testGetEligibleProductsFromOrder_Standard() {
        VlProductAvailabilityImpl productAvailability = new VlProductAvailabilityImpl();
 
        List<Order> orders = OrdrTestDataFactory.createOrdersWithServiceAddress();
        OrdrTestDataFactory.createOrderingWSCustomSettings('ProductEligibilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/CMO/OrderMgmt/PerformCustomerOrderFeasibility_v2_0_vs0');
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        //orders[0] has Standard address
        List<SObject> sObjList = productAvailability.getAvailableProducts(orders[0], new List<PriceBookEntry>());
        Test.stopTest();
    } 

    @isTest
    private static void testInvokeMethodFromQuote_LotBlockPlan() {
        VlProductAvailabilityImpl productAvailability = new VlProductAvailabilityImpl();
 
        List<Quote> quotes = OrdrTestDataFactory.createQuotesWithServiceAddress();
        OrdrTestDataFactory.createOrderingWSCustomSettings('ProductEligibilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/CMO/OrderMgmt/PerformCustomerOrderFeasibility_v2_0_vs0');
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        //quotes[1] has LotBlockPlan address
        List<SObject> sObjList = productAvailability.getAvailableProducts(quotes[1], null);
        Test.stopTest();
    }    
    
    @isTest
    private static void testInvokeMethodFromOrder_LegalLandDesc() {
        VlProductAvailabilityImpl productAvailability = new VlProductAvailabilityImpl();
 
        List<Order> orders = OrdrTestDataFactory.createOrdersWithServiceAddress();
        OrdrTestDataFactory.createOrderingWSCustomSettings('ProductEligibilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/CMO/OrderMgmt/PerformCustomerOrderFeasibility_v2_0_vs0');
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        //orders[2] has LegalLandDesc address
        List<SObject> sObjList = productAvailability.getAvailableProducts(orders[2], null);
        Test.stopTest();
    }
    
    @isTest
    private static void testInvokeMethodFromQuote_TownshipRangeRoadHwy() {
        VlProductAvailabilityImpl productAvailability = new VlProductAvailabilityImpl();
 
        List<Quote> quotes = OrdrTestDataFactory.createQuotesWithServiceAddress();
        OrdrTestDataFactory.createOrderingWSCustomSettings('ProductEligibilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/CMO/OrderMgmt/PerformCustomerOrderFeasibility_v2_0_vs0');
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        //quotes[3] has TownshipRangeRoadHwy address
        List<SObject> sObjList = productAvailability.getAvailableProducts(quotes[3], null);
        Test.stopTest();
    }  
    
	@isTest
    private static void testInvokeMethod_Exception() {
		VlProductAvailabilityImpl productAvailability = new VlProductAvailabilityImpl(); 
        OrdrTestDataFactory.createOrderingWSCustomSettings('ProductEligibilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/CMO/OrderMgmt/PerformCustomerOrderFeasibility_v2_0_vs0');
		
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        try {
            List<SObject> sObjList = productAvailability.getAvailableProducts(null, null);
            System.assert(false, 'Exception expected');
        }
        catch(Exception e) {
            System.debug(e.getTypeName() + ': ' + e.getMessage());
        }
        Test.stopTest();
        
    }     

	@isTest
    private static void testSaveRateGroupInfo() {
		VlProductAvailabilityImpl productAvailability = new VlProductAvailabilityImpl(); 
        List<SMBCare_Address__c> addresses = OrdrTestDataFactory.createAddresses();
        Id serviceAddressId = addresses[0].Id;

        OrdrTestDataFactory.createOrderingWSCustomSettings('RateGroupInfoEndpoint', 'https://xmlgwy-pt1.telus.com:9030/pt01/CMO/BillingAccountMgmt/RateGroupInfoService_v1_0_1_vs1');
		
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsAsfMockImpl());
        OrdrProductEligibilityManager.saveRateGroupInfo(serviceAddressId);
        Test.stopTest();
        
    } 

	@isTest
    private static void testRetrieveRateGroupInfo_Exception() {
		VlProductAvailabilityImpl productAvailability = new VlProductAvailabilityImpl(); 
        List<SMBCare_Address__c> addresses = OrdrTestDataFactory.createAddresses();
        addresses[0].npa_nxx__c = null;
        update addresses[0];
        Id serviceAddressId = addresses[0].Id;  //null npa_nxx

        OrdrTestDataFactory.createOrderingWSCustomSettings('RateGroupInfoEndpoint', 'https://xmlgwy-pt1.telus.com:9030/pt01/CMO/BillingAccountMgmt/RateGroupInfoService_v1_0_1_vs1');
		
        Test.startTest();
        try {
            Test.setMock(WebServiceMock.class, new OrdrWsAsfMockImpl());
            OrdrProductEligibilityManager.retrieveRateGroupInfo(serviceAddressId);
            System.assert(false, 'Exception expected');
        }
        catch(Exception e) {
            System.debug(e.getTypeName() + ': ' + e.getMessage());
        }        
        Test.stopTest();
        
    } 
    
	@isTest
    private static void testCacheAvailableOffers() {
		VlProductAvailabilityImpl productAvailability = new VlProductAvailabilityImpl(); 
        List<SMBCare_Address__c> addresses = OrdrTestDataFactory.createAddresses();
        Id serviceAddressId = addresses[0].Id;

        OrdrTestDataFactory.createOrderingWSCustomSettings('ProductEligibilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/CMO/OrderMgmt/PerformCustomerOrderFeasibility_v2_0_vs0');
		
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        OrdrProductEligibilityManager.cacheAvailableOffers(serviceAddressId);
        Test.stopTest();
        
    }  
    
	@isTest
    private static void testGetAvailableChildOffers() {
		VlProductAvailabilityImpl productAvailability = new VlProductAvailabilityImpl(); 
        List<SMBCare_Address__c> addresses = OrdrTestDataFactory.createAddresses();
        Id serviceAddressId = addresses[0].Id;

        OrdrTestDataFactory.createOrderingWSCustomSettings('ProductEligibilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/CMO/OrderMgmt/PerformCustomerOrderFeasibility_v2_0_vs0');
		
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        OrdrProductEligibilityManager.getAvailableChildOffers(serviceAddressId, '9143210725313949412');
        Test.stopTest();
        
    }   
    
	@isTest
    private static void testGetOfferAvailableCharacteristics() {
		VlProductAvailabilityImpl productAvailability = new VlProductAvailabilityImpl(); 
        List<SMBCare_Address__c> addresses = OrdrTestDataFactory.createAddresses();
        Id serviceAddressId = addresses[0].Id;

        OrdrTestDataFactory.createOrderingWSCustomSettings('ProductEligibilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/CMO/OrderMgmt/PerformCustomerOrderFeasibility_v2_0_vs0');
		
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        OrdrProductEligibilityManager.getOfferAvailableCharacteristics(serviceAddressId, '9143210725313949412');
        Test.stopTest();
        
    }     
}