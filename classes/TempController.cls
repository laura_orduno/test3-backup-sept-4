public with sharing class TempController {
    //Agreement Lookup
    public dsfs__DocuSign_Status__c docuSignStatus {get; set;} 
    private String agreementDocumentId;
    public Boolean isError {get; set;}
    public String waitCursorStatus {get; set;}
    //public Id agreementId {public get; public set;}
    //public Apttus__APTS_Agreement__c agreement{get; set;} 
    
    //constructor
    public TempController() {
        docuSignStatus = new dsfs__DocuSign_Status__c();
        isError = false;
        waitCursorStatus = 'Please Wait ... ';
    }
    
    //Generate
    public void doGenerate() {
        waitCursorStatus += 'Sending ...';
        if(docuSignStatus.Apttus_DocuSign__Apttus_Agreement_ID__c == null) {
            isError = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Please select an Agreement.'));
            return;
        }
        
        try {                        
            //call generate api
            agreementDocumentId = AptsComplyCustomAPI.doGenerate(docuSignStatus.Apttus_DocuSign__Apttus_Agreement_ID__c);
        } catch(Exception ex) {
            isError = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, ex.getMessage()));
            return;
        }
    }
    
    //send
    public void doSend() {
        if(docuSignStatus.Apttus_DocuSign__Apttus_Agreement_ID__c == null) {
            isError = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Please select an Agreement.'));
            return;
        }
        
        if(agreementDocumentId == null) {
            isError = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Agreement Document not found.'));
            return;
        }
        
        try {
            //call send api
            String sendResponse = AptsComplyCustomAPI.doSend(docuSignStatus.Apttus_DocuSign__Apttus_Agreement_ID__c, agreementDocumentId);
            isError = true;
            if(sendResponse.equalsIgnoreCase('Agreement Document Sent Successfully.')) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, sendResponse));
            } else {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, sendResponse));
            }            
            return;
        } catch(Exception ex) {
            isError = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, ex.getMessage()));
            return;
        }
    }
    
    //void envelope
    public void CancelAgreement() {
        if(docuSignStatus.Apttus_DocuSign__Apttus_Agreement_ID__c == null) {
            isError = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Please select an Agreement.'));
            return;
        }
        
        //void envelope api call
        try {
            String cancelResponse = AptsComplyCustomAPI.doCancel(docuSignStatus.Apttus_DocuSign__Apttus_Agreement_ID__c);           
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, cancelResponse));
            return;
        } catch(Exception ex) {
            isError = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, ex.getMessage()));
            return;
        }
        
        //update agreement status
        Apttus__Apts_Agreement__c agmt = new Apttus__Apts_Agreement__c(id=docuSignStatus.Apttus_DocuSign__Apttus_Agreement_ID__c);
        agmt.Apttus__Status_Category__c = 'Cancelled';
        agmt.Apttus__Status__c = '';
        update agmt;
    }
    
    //Cancel
    public PageReference Cancel() {
        return new PageReference('/home/home.jsp');
    }
    
    /*public void doGenerateAndSend() {
        System.debug('doGenerateAndSend: AgreementId:'+agreementId);
        AptsComplyCustomAPI.doGenerateAndSend(agreementId);
    }
    
    public void doCancel() {
        System.debug('doCancel: AgreementId:'+agreementId);
        AptsComplyCustomAPI.doCancel(agreementId);
    }*/
}