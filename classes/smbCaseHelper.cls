/**
 * Created by rdraper on 1/16/2018.
 */

public without sharing class smbCaseHelper {

    @TestVisible final static String LEGAL_NAME_CORRECTION_CASE_RECORD_TYPE = 'Legal (RCID only) Name Correction';
    @TestVisible final static String LEGAL_NAME_UPDATE_CASE_RECORD_TYPE = 'Legal Name Update';
    @TestVisible final static String TBO_CASE_RECORD_TYPE = 'TBO Request';

    final static String LEGAL_NAME_CORRECTION_CASE_SUBJECT = 'Legal Name Correction';
    @TestVisible final static String SMB_CARE_ENTITLEMENT_ID = [SELECT Metadata_Value__c FROM Apex_Org_Default__mdt WHERE DeveloperName = 'SMBCareEntitlementId' LIMIT 1].Metadata_Value__c;
    final static String LEGAL_NAME_QUEUE_ID = [SELECT Metadata_Value__c FROM Apex_Org_Default__mdt WHERE DeveloperName = 'SMBDefaultLegalNameCorrectionQueueId' LIMIT 1].Metadata_Value__c;
    final static String GROUP_PREFIX = '00G';
    final static String USER_PREFIX = '005';
    Id legalNameCorrectionQueue = LEGAL_NAME_QUEUE_ID;
    @TestVisible private static Map<Id, Schema.RecordtypeInfo> recordTypeInfoMap = Case.SObjectType.getDescribe().getRecordTypeInfosById();

    Set<String> SALES_COMPENSATION_RT_NAMES = new Set<String>{
            'sales compensation inquiries', 'sales compensation inquiries (partner solutions)'
    };
    Set<String> SALES_COMPENSATION_EXCLUDE_PROFILE_NAMES
            = new Set<String>{
                    'sales compensation'
                    , 'system administrator'
                    , 'system admin team'
                    , 'system admin team no pw expiry'
                    , 'system integration'
                    , 'system vendor'
                    , 'cloud enablement team'
            };

    private List<Case> triggerNewList;
    private Map<Id, Case> triggerNewMap;
    private Map<Id, Case> triggerOldMap;

    private Set<Id> userIds = new Set<Id>();
    private Set<Id> groupIds = new Set<Id>();
    private Map<Id, User> userMap = new Map<Id, User>();
    private Map<Id, Group> groupMap = new Map<Id, Group>();
    static Set<Id> updatedCaseIdWithMembers = new Set<Id>();

    public smbCaseHelper(List<Case> triggerNew, Map<Id, Case> triggerNewMap, Map<Id, Case> triggerOldMap) {
        this.triggerNewList = triggerNew;
        this.triggerNewMap = triggerNewMap;
        this.triggerOldMap = triggerOldMap;
    }

    public void execute() {
        if (Trigger.isBefore) {
            setOwnerFields();
            extractOwnerIds();
            setQueueName();
            setDeptAndRegion();
            smb_CaseBeforeClass.establishStartEndTimes(triggerNewList, Trigger.isUpdate, Trigger.isInsert, triggerOldMap);
            smb_CaseBeforeClass.syncParentCaseOwnerField(triggerNewList, Trigger.isUpdate, Trigger.isInsert, triggerOldMap);
            setSMBCareFields();
            processTBORecordTypeCases();
        }
        if(Trigger.isAfter){
            smb_CaseAfterClass.flagDeactivationOnAccount(triggerNewList, Trigger.isUpdate, Trigger.isInsert, triggerOldMap);
            smb_CaseAfterClass.establishTrackingRecords(triggerNewList, Trigger.isUpdate, Trigger.isInsert, triggerOldMap);
            updateAccountLegalName();
            insertCaseTeamMembers();
            insertTasks();
        }
    }

    private void updateAccountLegalName(){
        if (Trigger.isInsert) {
            List<Account> accountsWithUpdatedLegalName = new List<Account>();
            for (Case c : triggerNewList) {
                if (recordTypeInfoMap.containsKey(c.RecordTypeId)
                        &&  (recordTypeInfoMap.get(c.RecordTypeId).getName().equalsIgnoreCase(LEGAL_NAME_CORRECTION_CASE_RECORD_TYPE)
                                ||  recordTypeInfoMap.get(c.RecordTypeId).getName().equalsIgnoreCase(LEGAL_NAME_UPDATE_CASE_RECORD_TYPE))) {

                    if (String.isNotBlank(c.Updated_Legal_Name__c)) {
                        if (String.isNotBlank(c.AccountId)) {
                            accountsWithUpdatedLegalName.add (
                                new Account(
                                    Id = c.AccountId,
                                    Correct_Legal_Name__c = c.Updated_Legal_Name__c
                                )
                            );
                        }
                    }

                }
            }
            if (!accountsWithUpdatedLegalName.isEmpty()) {
                Database.SaveResult[] results = Database.update(accountsWithUpdatedLegalName, false);
                Util.emailSystemError(results, 'smb_CaseBefore - Account Correct Legal Name Update Failure', false);
            }
        }
    }

    private void insertCaseTeamMembers(){
        /* Editor: Adhir Parchure
           Date: 14-08-2015
           Project: BCX Callidus Integration
           Use case: Read-only access for Creator of Case (SIS 1088)
           Description: After case assignment rule execution Case submitter loses an access to the case,
               below code adds case submitter to a case team with Read only access.
        */
        List<CaseTeamMember> caseTeamMemberList = new List<CaseTeamMember>();

        CaseTeamRole caseRole;
        Boolean isSalesEnquiry = false;
        if (Trigger.isInsert && Trigger.isAfter) {
            for (Case newCase : triggerNewList) {
                if (newCase.RecTypeName__c.equalsIgnoreCase('Sales Compensation Inquiries')
                            ||  newCase.RecTypeName__c.equalsIgnoreCase('Sales Compensation Inquiries (Partner Solutions)')
                        &&  isSalesEnquiry == false) {
                    isSalesEnquiry = true;
                    break;//just determine if we need to continue with the logic
                }
            }
        }

        if (isSalesEnquiry) {
            if(caseRole == null) {
                caseRole = [SELECT Id FROM CaseTeamRole WHERE Name LIKE 'Sales Compensation Inquiry creator%' LIMIT 1];
            }

            if (Limits.getQueries() >= 100) {
                System.debug('smb_CaseAfter: abort since SOQL limmit will be hit.');

                throw new QueryException();
            }

            if (Trigger.isInsert && Trigger.isAfter) {

                for (Case newCase : triggerNewList) {
                    if (!String.isBlank(newCase.RecTypeName__c)
                            && !String.isBlank(newCase.createdbyid)
                            && !updatedCaseIdWithMembers.contains(newCase.id)
                            && caseRole != null
                            && (newCase.RecTypeName__c.equalsIgnoreCase('Sales Compensation Inquiries')
                                    ||  newCase.RecTypeName__c.equalsIgnoreCase('Sales Compensation Inquiries (Partner Solutions)'))) {
                        // Add a case submitter to the case team
                        caseTeamMemberList.add(
                            new CaseTeamMember(
                                ParentId = newCase.Id,
                                MemberId = newCase.CreatedById,
                                TeamRoleId = caseRole.Id
                            )
                        );
                        updatedCaseIdWithMembers.add(newCase.id);

                    }

                }

                if (!caseTeamMemberList.isEmpty()) {
                    insert caseTeamMemberList;
                }
            }
        }
    }

    private void insertTasks(){
        // START >> Added by Adhir - TBO User story<< add "Resolution Details" to ase comments when case is closed.
        if (Trigger.isUpdate && Trigger.isAfter) {

            List<Task> taskInsertList = new List<Task>();
            for (Case closedCase : triggerNewList) {

                if (recordTypeInfoMap.containsKey(closedCase.RecordTypeId)
                        &&  recordTypeInfoMap.containsKey(closedCase.RecordTypeId)
                        &&  (recordTypeInfoMap.get(closedCase.RecordTypeId).getName().equalsIgnoreCase(TBO_CASE_RECORD_TYPE)
                                ||  recordTypeInfoMap.get(closedCase.RecordTypeId).getName().equalsIgnoreCase(LEGAL_NAME_UPDATE_CASE_RECORD_TYPE))
                        &&  (closedCase.status.equalsIgnoreCase('Cancelled')
                                ||  closedCase.status.equalsIgnoreCase('Closed'))
                        &&  String.isNotBlank(closedCase.Resolution_Details__c)
                        &&  !String.valueOf(closedCase.OwnerId).startsWith('00G')) {

                    taskInsertList.add(
                        new Task(
                            OwnerId = closedCase.OwnerId
                            ,Subject = 'Closed case Resolution details - ' + closedCase.Resolution_Details__c
                            ,Status = 'Completed'
                            ,ActivityDate = System.today()
                            ,Priority = 'Normal'
                            ,WhatId = closedCase.Id
                        )
                    );

                }
            }

            if (!taskInsertList.isEmpty()) {
                Database.insert(taskInsertList);
            }

        }
    }


    private void setOwnerFields() {
        for (Case theCase : triggerNewList) {

            //added by Scott Fawcett
            String ownerId2 = theCase.OwnerId;

            if (Trigger.isInsert && ownerId2.startsWith(USER_PREFIX)) {
                theCase.Current_Owner__c = theCase.OwnerId;
            } else if (Trigger.isUpdate) {
                if (ownerId2.startsWith(USER_PREFIX)) {
                    theCase.Current_Owner__c = theCase.OwnerId;
                } else if (ownerId2.startsWith(GROUP_PREFIX)) {
                    theCase.Current_Owner__c = null;
                    String prevOwnerId = triggerOldMap.get(theCase.Id).OwnerId;
                    if (triggerOldMap.get(theCase.Id).OwnerId != theCase.OwnerId && prevOwnerId.startsWith(USER_PREFIX)) {
                        theCase.Previous_Owner__c = triggerOldMap.get(theCase.Id).OwnerId;
                    }
                }
            }
        }
    }

    private void setQueueName() {

        if (Limits.getQueries() >= 100) {
            System.debug('smb_CaseBefore: abort since SOQL limmit will be hit.');

            throw new QueryException();
        }

        if (!groupIds.isEmpty()) {
            groupMap = new Map<Id, Group>([SELECT Id, Name, Type FROM Group WHERE Id IN :groupIds AND Type = 'Queue']);
            for (Case c : triggerNewList) {
                Group g = groupMap.get(c.OwnerId);
                if (g != null)
                    c.Queue_Name__c = g.Name;
            }
        }

    }

    private void setDeptAndRegion() {

        // added for SMB Evolution - 2/26/2013 by Scott Fawcett
        if (Limits.getQueries() >= 100) {
            System.debug('smb_CaseBefore: abort since SOQL limmit will be hit.');

            throw new QueryException();
        }

        if (!userIds.isEmpty()) {
            userMap = new Map<Id, User>([SELECT Care_Type__c, ProfileId, Profile.Name, state,ManagerId, Manager.Email, Channel_Callidus__c FROM User WHERE Id IN :userIds]);

            if (Limits.getQueries() >= 100) {
                System.debug('smb_CaseBefore: abort since SOQL limmit will be hit.');

                throw new QueryException();
            }

            /*
            Purpose/Description: SIS-937. Auto-Population Sales Compensation Inquiry records with the Submitter Department and Region from their User Detail
            Update by Author: Rajan Tatuskar
            Updated Date: 11-Aug-2015
            Business: BT Sales Compensation Team
            */

            if (Trigger.isInsert) {
                if (!userMap.isEmpty()) {

                    for (Case theCase : triggerNewList) {
                        if (userMap.containsKey(theCase.OwnerId)) {
                            User currentUser = userMap.get(theCase.OwnerId);

                            if (String.isNotBlank(theCase.RecTypeName__c) &&
                                    SALES_COMPENSATION_RT_NAMES.contains(theCase.RecTypeName__c.toLowerCase()) &&
                                    !SALES_COMPENSATION_EXCLUDE_PROFILE_NAMES.contains(currentUser.Profile.Name.toLowerCase())) {

                                /* Added by Adhir
                                    User story: the Dept. & Region Accessibility (SIS1090) start */

                                if (String.isNotBlank(theCase.Dept__c) || String.isNotBlank(theCase.Region__c)) {
                                    theCase.addError(Label.Dept_and_region_blank);
                                }

                                // User story: the Dept. & Region Accessibility (SIS1090) end

                                if (String.isNotBlank(currentUser.Channel_Callidus__c))
                                    theCase.Dept__c = currentUser.Channel_Callidus__c;

                                if (String.isNotBlank(currentUser.state))
                                    theCase.Region__c = currentUser.state;

                            }
                        }

                    }
                }
            }
            // END of SIS-937 US.
        }
    }

    private void setSMBCareFields() {

        Set<Id> SMBCaseRTs = new Set<Id>();

        for (Schema.RecordTypeInfo r : Case.sobjecttype.getDescribe().getRecordTypeInfosById().values()) {
            // add every found RT except "SMBEV_General_Inquiry"
            if (r.getName() != 'SMB Care General Inquiry')
                SMBCaseRTs.add(r.getRecordTypeId());
        }

        for (Case c : triggerNewList) {
            //stamp Case with the Owners User Profile or Queue Name
            if (String.valueOf(c.OwnerId).startsWith(GROUP_PREFIX))
                c.Case_Owner_Profile_Name__c = groupMap.get(c.OwnerId).Name;

            if (String.valueOf(c.OwnerId).startsWith(USER_PREFIX))
                c.Case_Owner_Profile_Name__c = userMap.get(c.OwnerId).Profile.Name;
        }

        if (Trigger.isInsert) {
            for (Case c : triggerNewList) {

                if (SMB_CARE_ENTITLEMENT_ID != null) {

                    // for SMB Evolution Record Types, stamp the Entitlement Id on the Case
                    if (SMBCaseRTs.contains(c.RecordTypeId))
                        c.EntitlementId = SMB_CARE_ENTITLEMENT_ID;

                }

                if (recordTypeInfoMap.containsKey(c.RecordTypeId)
                        &&  recordTypeInfoMap.get(c.RecordTypeId).getName().equalsIgnoreCase(LEGAL_NAME_CORRECTION_CASE_RECORD_TYPE)) {
                    if (string.isBlank(c.Subject)) {
                        c.Subject = LEGAL_NAME_CORRECTION_CASE_SUBJECT;
                    }
                    // Commented as part of TBO project, as LNU will follow TBO queue assignment this is no longer required. - Adhir[18-05-2016]
                    if (legalNameCorrectionQueue != null) {
                        c.OwnerId = legalNameCorrectionQueue;
                    }
                }
            }
        }
        if (Trigger.isUpdate) {

            // this portion of the code stamps updates to milestones
            Set<Id> firstResponseIds = new Set<Id>(); // related to SMB Care First Response Time
            Set<Id> resolutionIds = new Set<Id>(); // related to SMB Care Resolution Time
            //Set<Id> escalationIds = new Set<Id>(); // related to SMB Care Escalations
            Set<Id> totalCaseIds = new Set<Id>();

            for (Case c2 : triggerNewList) {

                // for SMB Care First Response Time
                if (c2.Status == 'In Progress'
                        &&  triggerOldMap.get(c2.Id).Status != 'In Progress'
                        &&  SMBCaseRTs.contains(c2.RecordTypeId)) {

                    firstResponseIds.add(c2.Id);
                    if (!totalCaseIds.contains(c2.Id))
                        totalCaseIds.add(c2.Id);

                }

                // for SMB Care Resolution Time
                if ((   c2.Status == 'Closed'
                                || c2.Status == 'Cancelled')
                            &&  !c2.IsClosed
                            &&  SMBCaseRTs.contains(c2.RecordTypeId)) {

                    resolutionIds.add(c2.Id);
                    if (!totalCaseIds.contains(c2.Id))
                        totalCaseIds.add(c2.Id);

                    // also need to tie out the first response if it wasn't previously done
                    if (!firstResponseIds.contains(c2.Id))
                        firstResponseIds.add(c2.Id);

                    // need to also stamp Escalation_End__c field if it isn't already
                    if (c2.Escalated_Case__c && c2.Escalation_End__c == null)
                        c2.Escalation_End__c = System.now();

                }

                if (!totalCaseIds.isEmpty()) {
                    List<CaseMilestone> CMs = [
                            SELECT CompletionDate, Id, CaseId, MilestoneType.Name
                            FROM CaseMilestone
                            WHERE CaseId IN :totalCaseIds AND CompletionDate = null AND
                            (MilestoneType.Name = 'SMB Care First Response Time' OR
                            MilestoneType.Name = 'SMB Care Resolution Time')
                    ];

                    if (!CMs.isEmpty()) {
                        for (CaseMilestone cm : CMs) {
                            if (firstResponseIds.contains(cm.CaseId) && cm.MilestoneType.Name == 'SMB Care First Response Time')
                                cm.CompletionDate = System.now();
                            if (resolutionIds.contains(cm.CaseId) && cm.MilestoneType.Name == 'SMB Care Resolution Time')
                                cm.CompletionDate = System.now();
                        }

                        update CMs;
                    }
                }
            }
        }
        //system.debug('##### EXITING smb_CaseBefore Trigger');
    }

    private void processTBORecordTypeCases() {
        /*******
       ********    Added by Rajan - This section is for TBO user story
       ********/

        if (Trigger.isInsert || Trigger.isUpdate) {
            Set<Id> accountIds = new Set<Id>();
            Set<Id> contactIds = new Set<Id>();

            for (Case caseObj : triggerNewList) {

                /***for US#23 ***/
                /*we need to update useremail with owners manager's email address*/
                /*US-16-LNC, this warranty defect(User Story 16 LNC) updated by Umesh (29June2016)
                added record type condition in below statement */
                if (recordTypeInfoMap.containsKey(caseObj.RecordTypeId)
                        && (recordTypeInfoMap.get(caseObj.RecordTypeId).getName().equalsIgnoreCase(TBO_CASE_RECORD_TYPE)
                                ||  recordTypeInfoMap.get(caseObj.RecordTypeId).getName().equalsIgnoreCase(LEGAL_NAME_UPDATE_CASE_RECORD_TYPE))
                        && userMap.containsKey(caseObj.OwnerId)) {

                    caseObj.User_Email__c = userMap.get(caseObj.OwnerId).Manager.Email;

                }

                // could easily be a validation rule *refactor later
                if (recordTypeInfoMap.containsKey(caseObj.RecordTypeId)
                        && (recordTypeInfoMap.get(caseObj.RecordTypeId).getName().equalsIgnoreCase(TBO_CASE_RECORD_TYPE)
                                ||  recordTypeInfoMap.get(caseObj.RecordTypeId).getName().equalsIgnoreCase(LEGAL_NAME_UPDATE_CASE_RECORD_TYPE))
                        && caseObj.Status.equalsIgnoreCase('Draft')
                        && CaseObj.AccountId == null
                        && caseObj.Master_Account_Name__c == null) {

                    caseObj.addError(Label.TBO_OutgoingORIncomingCustomerMandatory);

                }

                // could easily be a validation rule *refactor later
                if (recordTypeInfoMap.containsKey(caseObj.RecordTypeId)
                        && recordTypeInfoMap.get(caseObj.RecordTypeId).getName().equalsIgnoreCase(TBO_CASE_RECORD_TYPE)
                        && (caseObj.Master_Account_Name__c == null
                                ||  caseObj.Contact_phone_number__c == null)) {

                    if (Trigger.isUpdate
                            && (caseObj.Status.equalsIgnoreCase('In Progress')
                                    ||  caseObj.Status.equalsIgnoreCase('Completed')
                                    ||  caseObj.Status.equalsIgnoreCase('Closed'))
                            && (caseObj.Master_Account_Name__c == null
                                    ||  caseObj.Contact_phone_number__c == null)
                            && caseObj.Status_Cancel_Case__c == null) {

                        caseObj.addError(Label.TBO_IncomingAccountContactMandatory);

                    }
                }

            }


            for (Case caseObj : triggerNewList) {
                if (recordTypeInfoMap.containsKey(caseObj.RecordTypeId)
                        && (recordTypeInfoMap.get(caseObj.RecordTypeId).getName().equalsIgnoreCase(TBO_CASE_RECORD_TYPE)
                                ||  (recordTypeInfoMap.get(caseObj.RecordTypeId).getName().equalsIgnoreCase(LEGAL_NAME_UPDATE_CASE_RECORD_TYPE)))) {

                    if (Trigger.isUpdate) {

                        /*Check status changed then we will send the notification escalation messages(US#23).*/
                        if ((caseObj.Status != triggerOldMap.get(caseObj.Id).Status)
                                || (caseObj.TBO_In_Progress_Status__c == 'Waiting for internal-credit'
                                        &&  triggerOldMap.get(caseObj.Id).TBO_In_Progress_Status__c != 'Waiting for internal-credit')) {

                            caseObj.CheckStatusDate__c = System.now();

                        }

                        // Updated the correct status name - Adhir [17-05-2016]
                        if (caseObj.Status_Cancel_Case__c == 'Cancelled'
                                &&  caseObj.status != 'Draft'
                                &&  caseObj.status != 'In Progress'
                                &&  caseObj.status != 'New'
                                &&  caseObj.status != 'Completed'
                                &&  caseObj.status != 'Assigned') {

                            caseObj.status = caseObj.Status_Cancel_Case__c;
                            caseObj.TBO_In_Progress_Status__c = ''; //need to update this value if in progress case is cancelled - umesh - 18-may-2016

                        }
                    }

                    //commented as part of US #140 -Adhir[21-04-2016]
                    /*if(caseObj.Outgoing_Account_Name__c==null)
                        caseObj.AccountId=null;*/

                    /*if(caseObj.Outgoing_Account_Name__c!=null && Trigger.isInsert)
                        caseObj.AccountId=caseObj.Outgoing_Account_Name__c;*/

                    //modified as part of US #140 -Adhir[21-04-2016]

                    if (caseObj.Accountid != null) {
                        accountIds.add(caseObj.AccountId);
                    }
                    if (caseObj.Master_Account_Name__c != null) {
                        //system.debug('caseObj.Master_Account_Name__c&&&&&UMESH&&&&& =>>'+caseObj.Master_Account_Name__c);
                        accountIds.add(caseObj.Master_Account_Name__c);
                        //system.debug('account Id is added');
                    }
                    if (caseObj.Contact_phone_number__c != null) {
                        contactIds.add(caseObj.Contact_phone_number__c);
                    }
                }

            }

            Map<Id,Account> accountEntries;
            Map<Id,Contact> contactEntries;
            if (!accountIds.isEmpty()) {
                accountEntries = new Map<Id, Account>([
                        SELECT Id,Name,CBU_Name__c,DNTC__c,Credit_Profile__c,Marketing_Sub_Segment_Description__c,Parent.Correct_Legal_Name__c,
                                Parent.RCID__c,Parent.RCID_Name__c,Parent.CBUCID_formula__c,Parent.CBU_Code_RCID__c,Parent.Sub_Segment_Code__r.Name,Parent.Parent.Name,
                                BCAN__c,Billing_System__c
                        FROM Account
                        WHERE Id IN :accountIds
                ]);
            }
            if (!contactIds.isEmpty()) {
                contactEntries = new Map<Id, Contact>([SELECT Id,Email FROM Contact WHERE Id in :contactIds]);
            }
            for (Case caseObj : triggerNewList) {
                if (recordTypeInfoMap.containsKey(caseObj.RecordTypeId)
                        && (recordTypeInfoMap.get(caseObj.RecordTypeId).getName().equalsIgnoreCase(TBO_CASE_RECORD_TYPE)
                                ||  (recordTypeInfoMap.get(caseObj.RecordTypeId).getName().equalsIgnoreCase(LEGAL_NAME_UPDATE_CASE_RECORD_TYPE)))) {
                    //  Modified as part of US #140 - Adhir[21-04-2016]
                    if (caseObj.AccountId != null) {
                        caseObj.Business_Unit_Outgoing__c = accountEntries.get(caseObj.AccountId).CBU_Name__c;
                        caseObj.Subsegment_Outgoing__c = accountEntries.get(caseObj.AccountId).Marketing_Sub_Segment_Description__c;
                    } else {
                        caseObj.Business_Unit_Outgoing__c = null;
                        caseObj.Subsegment_Outgoing__c = null;
                    }
                    if (caseObj.Master_Account_Name__c != null) {
                        caseObj.Business_Unit_Incoming__c = accountEntries.get(caseObj.Master_Account_Name__c).CBU_Name__c;
                        /*US#53 we need to update DNTC flag due to unavailablity of Direct access Incoming Customer Fileds.*/
                        caseObj.Incoming_Customer_DNTC__c = accountEntries.get(caseObj.Master_Account_Name__c).DNTC__c;
                        /*US71 where we nedd to show incoming customer account name into ECH Queue.*/
                        caseObj.Incoming_Customer_Account__c = accountEntries.get(caseObj.Master_Account_Name__c).Name;
                        //system.debug('caseObj.Incoming_Customer_DNTC__c  $$$$$$$UMESH########'+caseObj.Incoming_Customer_DNTC__c);
                        caseObj.Subsegment_Incoming__c = accountEntries.get(caseObj.Master_Account_Name__c).Marketing_Sub_Segment_Description__c;

                        /* ECH Reporting US #93 - These fields are populated for ECH report.*/

                    } else {
                        caseObj.Business_Unit_Incoming__c = null;
                        caseObj.Subsegment_Incoming__c = null;
                    }

                    if (caseObj.Contact_phone_number__c != null) {
                        caseObj.Incoming_Contact_Email__c = contactEntries.get(caseObj.Contact_phone_number__c).Email;
                    }
                }
                /*TBO US#82: Credit profile autopopulation for Incoming customer*/
                if (Trigger.isUpdate
                        &&  recordTypeInfoMap.containsKey(caseObj.RecordTypeId)
                        &&  recordTypeInfoMap.get(caseObj.RecordTypeId).getName().equalsIgnoreCase(TBO_CASE_RECORD_TYPE)
                        &&  caseObj.Master_Account_Name__c != null
                        &&  (caseObj.Credit_Profile__c == null
                                ||  caseObj.Master_Account_Name__c != triggerOldMap.get(caseObj.Id).Master_Account_Name__c)) {

                    if (accountEntries.get(caseObj.Master_Account_Name__c).Credit_Profile__c != null) {
                        caseObj.Credit_Profile__c = accountEntries.get(caseObj.Master_Account_Name__c).Credit_Profile__c;
                    } else {
                        caseObj.Credit_Profile__c = null;
                    }

                }

                if (Trigger.isInsert
                        &&  recordTypeInfoMap.containsKey(caseObj.RecordTypeId)
                        &&  recordTypeInfoMap.get(caseObj.RecordTypeId).getName().equalsIgnoreCase(TBO_CASE_RECORD_TYPE)
                        &&  caseObj.Master_Account_Name__c != null
                        &&  caseObj.Credit_Profile__c == null
                        &&  accountEntries.get(caseObj.Master_Account_Name__c).Credit_Profile__c != null) {

                    caseObj.Credit_Profile__c = accountEntries.get(caseObj.Master_Account_Name__c).Credit_Profile__c;
                }
                /* end US#82*/
                /*TBO US#133: Credit profile autopopulation for Legal name Update*/
                if (Trigger.isUpdate && recordTypeInfoMap.containsKey(caseObj.RecordTypeId)
                        &&  recordTypeInfoMap.get(caseObj.RecordTypeId).getName().equalsIgnoreCase(LEGAL_NAME_UPDATE_CASE_RECORD_TYPE)
                        &&  caseObj.AccountId != null
                        &&  (caseObj.Credit_Profile__c == null
                                ||  caseObj.AccountId != triggerOldMap.get(caseObj.Id).AccountId)) {

                    if (accountEntries.get(caseObj.AccountId).Credit_Profile__c != null) {
                        caseObj.Credit_Profile__c = accountEntries.get(caseObj.AccountId).Credit_Profile__c;
                    } else {
                        caseObj.Credit_Profile__c = null;
                    }
                }

                if (Trigger.isInsert
                        &&  recordTypeInfoMap.containsKey(caseObj.RecordTypeId)
                        &&  recordTypeInfoMap.get(caseObj.RecordTypeId).getName().equalsIgnoreCase(LEGAL_NAME_UPDATE_CASE_RECORD_TYPE)
                        &&  caseObj.AccountId != null
                        &&  caseObj.Credit_Profile__c == null
                        &&  accountEntries.get(caseObj.AccountId).Credit_Profile__c != null) {

                    caseObj.Credit_Profile__c = accountEntries.get(caseObj.AccountId).Credit_Profile__c;

                }

                /* end US#133*/
            }
        }
    }


    private void extractOwnerIds() {
        for (Case c : triggerNewList) {

            if (String.valueOf(c.OwnerId).startsWith(USER_PREFIX)) {
                userIds.add(c.OwnerId);
            } else if (String.valueOf(c.OwnerId).startsWith(GROUP_PREFIX)) {
                groupIds.add(c.OwnerId);
            }

        }
    }

}