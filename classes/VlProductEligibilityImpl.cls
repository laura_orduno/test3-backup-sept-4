global with sharing class VlProductEligibilityImpl implements vlocity_cmt.GlobalInterfaces.ProductEligibilityInterface {
        
    public List<SObject> getEligibleProducts(SObject s, List<PriceBookEntry> retList) {   
        
        try {

            Id objectId = (Id) s.get('Id');
         
            Map<String, OrdrProductEligibilityManager.Offer> availableOffers = OrdrProductEligibilityManager.getAvailableOffers(objectId);
            
            //OrdrProductEligibilityManager.persistMaximumSpeed(objectId, getMaximumSpeed(availableOffers));
            
            String[] availableOfferIds = OrdrProductEligibilityManager.getAvailableOfferIds(availableOffers);
            
            OrdrProductEligibilityManager.validateCartLineItems(objectId, availableOfferIds);
            
            List<SObject> sObjectList = filterPriceBookEntry(objectId, availableOfferIds);
            //throw new OrdrExceptions.ProductEligibilityException('test error ' + availableOfferIds + ' ' + JSON.serialize(availableOffers));

             if(sObjectList.size() > 0){
                   return sObjectList;
             } else {
                    String msg = 'There are no products eligible for this location.';
                    throw new OrdrExceptions.ProductEligibilityException(msg);
             }

          
             
            
        } catch (Exception e) {
            /*String msg = e.getMessage();
            msg += e.getStackTraceString().startsWith('()') ? '' : ': ' + e.getStackTraceString();
            throw new OrdrExceptions.ProductEligibilityException(msg);
            */
            return null; 
        }

    }
    
    private List<SObject> filterPriceBookEntry(Id objectId, String[] availableOfferIds) {
        //Vlocity Testing

        String priceBookId = OrdrProductEligibilityManager.retrievePriceBookId(objectId);
                                                                                                      
        String query = 'SELECT ' + OrdrUtilities.commaSeparatedFields('PricebookEntry', new List<String>{'Product2'});
        query += ' FROM PricebookEntry';    
        query += ' WHERE Pricebook2Id = :priceBookId';
        query += ' AND Product2.sellable__c = true';
        query += ' AND Product2.orderMgmtId__c in :availableOfferIds';
        System.debug(query);
        
        system.debug('availableOfferIds -- '+availableOfferIds);
                              
        List<SObject> sObjList = database.query(query);

        system.debug('sObjList -- '+JSON.serializePretty(sObjList));
        
        return sObjList;
    }
    
    private String getMaximumSpeed(Map<String, OrdrProductEligibilityManager.Offer> availableOffers) {
        String maxSpeed = '';
       for(String key : availableOffers.keySet()) {
            OrdrProductEligibilityManager.Offer offer = availableOffers.get(key);
            if (offer!=null && offer.characteristics!=null && offer.characteristics.containsKey('Download speed')) {
                for (OrdrProductEligibilityManager.Value value : offer.characteristics.get('Download speed')) {              
                    if(value.availability == 'available' || value.availability == 'limited'){              
                        maxSpeed = value.label + (value.availability == 'limited' ? ' (Limited)' : '');                                          
                        return maxSpeed;
                    }      
                }  
            }
        }
        return maxSpeed;
    }
    
}