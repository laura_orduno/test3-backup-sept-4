public class NAAS_AdditionalDetailsCntrl{
public Order objOrder{get;set;}
public string orderId{get;set;}
public list<string> filter ;
public boolean arecontractable{get;set;}
public static Map<String,String> canadaPostValMap {get;set;}
public static boolean isCPValdationComplete = false;
public static boolean isShippableProductPresent{get;set;}
Public static boolean enableNextbtn{get;set;} 
Public Map<String,String> OrdrItemRecsStatus{get;set;} 

public NAAS_AdditionalDetailsCntrl(){
    system.debug('%%######%% START: OCOM_AdditionalDetailsCntrl:OCOM_AdditionalDetailsCntrl  %%######%% ');
    objOrder=new Order();    
    orderId=ApexPages.currentPage().getparameters().get('id');
    objOrder = getOrderDetail();
    //arecontractable=false;
    arecontractable=true;//Arpit added
    isShippableProductPresent=false;
    system.debug('%%######%% END: OCOM_AdditionalDetailsCntrl:OCOM_AdditionalDetailsCntrl %%######%% ');
}
//Start : Added by Aditya Jamwal (IBM) to validate Shipping Address from Canada Post....  

    public void doShippingAddrValidation(){   
    system.debug('%%######%% START: OCOM_AdditionalDetailsCntrl:doShippingAddrValidation  %%######%% ');
    Map<id,string> MapOlitoStatus = new Map<id,string>();
    Map<Id,Map<string,String>> MapOLIcharactersticValueMap = new  Map<Id,Map<string,String>>();
    If(!isCPValdationComplete){  
    canadaPostValMap  = new Map<String,String>();
    system.debug('!!!Shipping Address '+orderId);
    List<OrderItem> ordrItemRecs;
    List<OrderItem> updateOrdrItemRecs = new List<OrderItem> ();
    try{
                ordrItemRecs =[select id,order.CustomerAuthorizedBy.Name,Shipping_Address_Contact__c,pricebookentry.product2.OCOM_Shippable__c,Service_Address_Full_Name__c
                               ,order.CustomerAuthorizedBy.Email,Shipping_Address__c,order.Service_Address__r.city__c,order.Service_Address__r.province__c
                               ,order.ServiceAddressSummary__c,order.CustomerAuthorizedBy.Phone,Shipping_Contact_Email__c,Shipping_Contact_Number__c
                               ,orderId,vlocity_cmt__JSONAttribute__c,order.Service_Address__r.Street__c,order.Service_Address__r.Building_Number__c
                               ,order.Service_Address__r.Postal_Code__c,Service_Address__r.city__c,Service_Address__r.Postal_Code__c,Service_Address__r.province__c,Service_Address__r.Street__c,Service_Address__r.Building_Number__c from OrderItem where orderId =:orderId];
    }catch(exception ex){
        system.debug('!!!Shipping Address '+ex);
    }    
     system.debug('!!!Shipping Address ordrItemRecs '+ordrItemRecs);
    OCOM_ShippingAddrCPValdationUtility.serviceAddressData saData = new OCOM_ShippingAddrCPValdationUtility.serviceAddressData();        
    if(null !=ordrItemRecs  &&ordrItemRecs.size()>0){
    String status=' ';
    Map<String,String> charactersticValueMap = new  Map<String,String>();
    for(orderItem oli :ordrItemRecs ){
        charactersticValueMap = new  Map<String,String>();
         if(null!=oli.order.Service_Address__c){         
                charactersticValueMap.put('Shipping Address Characteristic',oli.order.ServiceAddressSummary__c);
                charactersticValueMap.put('Shipping Address',oli.order.ServiceAddressSummary__c);
                charactersticValueMap.put('City of use',oli.Service_Address__r.city__c);
                charactersticValueMap.put('Province of use',oli.Service_Address__r.province__c);
             saData.postalCode=oli.Service_Address__r.Postal_Code__c ; saData.streetName=oli.Service_Address__r.Street__c; saData.buildingNumber=oli.Service_Address__r.Building_Number__c;
               system.debug('saData>>>>>>'+saData);
               MapOLIcharactersticValueMap.put(oli.id,charactersticValueMap);
           try{
              status = ''; 
             status= OCOM_ShippingAddrCPValdationUtility.validateAddress(saData);
             system.debug('!!!status>>> '+status+'   :::oli');
             MapOlitoStatus.put(oli.id,status);
              }catch(exception ex){ system.debug('!!!Shipping Address '+ex); }                 
             }  
     }
     if(Test.isRunningTest()){status='valid'; }
        boolean isContactNeeded=false;boolean isShippingAddressNeeded=false;
       for(orderItem oli :ordrItemRecs ){
           
             if(oli.pricebookentry.product2.OCOM_Shippable__c == 'Yes'){
               isShippableProductPresent=true;
             system.debug('!!!!oli'+status+' '+oli.Shipping_Address__c);   
              if(String.isBlank(oli.Shipping_Address__c) && status=='valid'){
                 
                 oli.Shipping_Address__c=oli.order.ServiceAddressSummary__c; 
                 String JsonChar =oli.vlocity_cmt__JSONAttribute__c;    
                  system.debug('!!!!debug before charactersticValueMap'+charactersticValueMap); 
                 for(string key : MapOLIcharactersticValueMap.get(oli.id).keySet()){
                     //  system.debug('!!!!debug before charactersticValueMap'+charactersticValueMap); 
                       JsonChar=(String) OCOM_Capture_OLI_ShippingAddController.readOLICharacterstic(JsonChar,key,'Update',MapOLIcharactersticValueMap.get(oli.id));
                       system.debug('!!!!debug After JSON '+JsonChar); 
                     }
                 oli.vlocity_cmt__JSONAttribute__c= JsonChar;                
                isShippingAddressNeeded =true; 
             }
              if(null!= oli.order.CustomerAuthorizedBy && String.isNotBlank(oli.order.CustomerAuthorizedBy.Name) && String.isBlank(oli.Shipping_Address_Contact__c)){
               oli.Shipping_Address_Contact__c = oli.order.CustomerAuthorizedBy.Name;
           //Start    Danish Hamdani 16-MAY-2017      Defect# 13111
                            oli.Shipping_Contact_Email__c = oli.order.CustomerAuthorizedBy.Email;
                            oli.Shipping_Contact_Number__c = oli.order.CustomerAuthorizedBy.Phone;
                            if(null !=OrdrConstants.CHAR_SHIPPING_CONTACT_EMAIL){charactersticValueMap.put(OrdrConstants.CHAR_SHIPPING_CONTACT_EMAIL,oli.order.CustomerAuthorizedBy.Email);}                       
                            if(null !=OrdrConstants.CHAR_SHIPPING_CONTACT_NUMBER){charactersticValueMap.put(OrdrConstants.CHAR_SHIPPING_CONTACT_NUMBER,oli.order.CustomerAuthorizedBy.Phone);}                       
               isContactNeeded=true;
             } 
                 if(isShippingAddressNeeded || isContactNeeded){ updateOrdrItemRecs.add(oli); }  
                 isShippingAddressNeeded = isContactNeeded =false;
           }             
             canadaPostValMap.put(oli.Service_Address_Full_Name__c,MapOlitoStatus.get(oli.id));
             //canadaPostValMap.put(oli.order.ServiceAddressSummary__c,'Invalid');
        } 
       update updateOrdrItemRecs;
      } 
   } 
    system.debug(' canadaPostValMap '+canadaPostValMap);
    system.debug('%%######%% END: OCOM_AdditionalDetailsCntrl:doShippingAddrValidation  %%######%% ');    
    OrdrItemRecsStatus = canadaPostValMap;
}
 public Order getOrderDetail(){
     system.debug('%%######%% START: OCOM_AdditionalDetailsCntrl:getOrderDetail  %%######%% ');
     try {
    if(filter ==null && orderId != null){
            //System.debug('Filter: ' + filter + '; rel: ' + relFieldName + '; recId: ' + orderId);
            
            filter = new List<String>{'id' + '=\''+ orderId +'\'' };
        }
        System.debug('Filter: ' + filter + '; rel: ' + 'id' + '; recId: ' + orderId);
    List<string> fieldNames = new List<string>{'id','ContactEmail__c',
                                            'ContactName__c',
                                            'ContactPhone__c','ContactTitle__c','DeliveryMethod__c',
                                            'Service_Address_Text__c','Account.Id','AccountId',
                                            'CustomerAuthorizedByID','CustomerAuthorizedBy.Name','CustomerSignedBy__c',
                                            'CustomerAuthorizedBy.Email','CustomerAuthorizedBy.Phone','CustomerAuthorizedBy.Title',
                                             'CustomerSignedBy__r.Name','CustomerSignedBy__r.Email','CustomerSignedBy__r.Phone','CustomerSignedBy__r.Title'};
        System.debug('getting query');
        String query = Util.queryBuilder('Order', fieldNames, filter);
        System.debug(LoggingLevel.Error,'query :' + query);
        list<order> order = Database.query(query);
    if(order.size() > 0 && !order.isEmpty() )
        objOrder = order[0];
     }catch(Exception anException){ system.debug('OCOM_AdditionalDetails::GetOrderDetails() exception(' + anException + ')'); throw anException; return null;  }     
     system.debug('%%######%% END: OCOM_AdditionalDetailsCntrl:getOrderDetail  %%######%% ');
     return objOrder;
}

}