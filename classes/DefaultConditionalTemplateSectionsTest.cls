@isTest
public class DefaultConditionalTemplateSectionsTest {
    
    static testMethod void testMethodOne(){
        system.debug('Test Method 1___');
        TestDataHelper.testAccountCreation();
        TestDataHelper.testContractCreationwithDealSupport();
        TestDataHelper.testvlocityDocTemplateteCreation();
        TestDataHelper.testContractWithContractLineItemListCreation('Voice Plan');
	
        
        vlocity_cmt__EntityFilter__c entityFilter = new vlocity_cmt__EntityFilter__c(Name='entity filter', vlocity_cmt__FilterOnObjectName__c='vlocity_cmt__ContractLineItem__c'  );
        insert entityFilter;
        
        vlocity_cmt__DocumentTemplateSection__c objDocTemplateSection=new vlocity_cmt__DocumentTemplateSection__c(vlocity_cmt__DocumentTemplateId__c=TestDataHelper.testvlocityDocTemplateObj.Id,name='test');
        insert objDocTemplateSection;
        
        vlocity_cmt__DocumentTemplateSectionCondition__c objDocTemplateSectionCondition = new vlocity_cmt__DocumentTemplateSectionCondition__c(Name = 'Test',vlocity_cmt__DocumentTemplateSectionId__c =objDocTemplateSection.Id, vlocity_cmt__Product2Id__c = TestDataHelper.testProductObj2.Id, vlocity_cmt__EntityFilterId__c = entityFilter.id, vlocity_cmt__ApplicableItemType__c = 'ContractLineItem__c');
        insert objDocTemplateSectionCondition;
        
        objDocTemplateSection = [Select Id, vlocity_cmt__CountOfSectionConditions__c From vlocity_cmt__DocumentTemplateSection__c where Id = :objDocTemplateSection.Id ];

        Contract_Section_Filter__c objContactMapping=new Contract_Section_Filter__c(name = 'Test Section', Field_API_Name__c='Offer_House_Demand__r.Loyalty_Credit__c',Filter_Value__c='12',Filter_Operator__c='=',Section_Name__c='test');
        insert objContactMapping;
        
        
        Id ContractRecordTypeId = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Corporate Wireless').getRecordTypeId();
        
        
        contract objcontract=new contract(Offer_House_Demand__c=TestDataHelper.testDealSupport.id,name='Test',Services__c='Voice',recordTypeID=ContractRecordTypeId,Hardware_Refresh_Type__c='Test',Contract_Type__c='Amendment', AccountId= TestDataHelper.testAccountObj.id); //Fulfillment_Type__c='Test'
        insert objcontract;
        
        vlocity_cmt__ContractLineItem__c  objCLI=new vlocity_cmt__ContractLineItem__c (vlocity_cmt__ContractId__c  =objcontract.id,name='test',Renewal_Credit__c='12',Hardware_Discounts__c='12' , vlocity_cmt__Product2Id__c  = TestDataHelper.testProductObj2.id);
        insert objCLI;
        
        Contract_Template_Section_Mapping__c objContractTemplateSectionMapping = new Contract_Template_Section_Mapping__c(name='Voice',Template_Section_Name__c='test');
        insert objContractTemplateSectionMapping;
        
        
        Map<String,Object> inputMap=new Map<String,Object>();
        inputMap.put('allSections',new Map<Id, vlocity_cmt__DocumentTemplateSection__c>{objDocTemplateSection.id=>objDocTemplateSection});
        inputMap.put('contextId',objcontract.id);
      
        inputMap.put('items',TestDataHelper.testContractLineItemList);
        
        
        Map<String,Object> outMap =  new   Map<String,Object>();
        Map<String,Object> options = new   Map<String,Object>();
        Test.startTest();
        DefaultConditionalTemplateSections.getTempSectionsBasedonProdcts(inputMap,outMap, Options);
        
        Test.stopTest();
        
        
        
        
        
    }
    
    static testMethod void testMethodTwo(){
        
        system.debug('Test Method 2....');
        TestDataHelper.testAccountCreation();
        TestDataHelper.testContractCreationwithDealSupport();
        TestDataHelper.testvlocityDocTemplateteCreation();
        TestDataHelper.testContractWithContractLineItemListCreation('Voice Plan');

        
        vlocity_cmt__EntityFilter__c entityFilter = new vlocity_cmt__EntityFilter__c(Name='entity filter', vlocity_cmt__FilterOnObjectName__c='vlocity_cmt__ContractLineItem__c'  );
        insert entityFilter;
        
        vlocity_cmt__DocumentTemplateSection__c objDocTemplateSection=new vlocity_cmt__DocumentTemplateSection__c(vlocity_cmt__DocumentTemplateId__c=TestDataHelper.testvlocityDocTemplateObj.Id,name='test');
        insert objDocTemplateSection;
        
        vlocity_cmt__DocumentTemplateSectionCondition__c objDocTemplateSectionCondition = new vlocity_cmt__DocumentTemplateSectionCondition__c(Name = 'Test',vlocity_cmt__DocumentTemplateSectionId__c =objDocTemplateSection.Id, vlocity_cmt__Product2Id__c = TestDataHelper.testProductObj2.Id,  vlocity_cmt__EntityFilterId__c = entityFilter.id, vlocity_cmt__ApplicableItemType__c = 'ContractLineItem__c');
        insert objDocTemplateSectionCondition;
        
        objDocTemplateSection = [Select Id, vlocity_cmt__CountOfSectionConditions__c From vlocity_cmt__DocumentTemplateSection__c where Id = :objDocTemplateSection.Id ];

        Contract_Section_Filter__c objContactMapping=new Contract_Section_Filter__c(name = 'Test Section', Field_API_Name__c='Offer_House_Demand__r.Loyalty_Credit__c',Filter_Value__c='12',Filter_Operator__c='=',Section_Name__c='test');
        insert objContactMapping;
        
        
        Id ContractRecordTypeId = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Corporate Wireless').getRecordTypeId();
        
        
        contract objcontract=new contract(Offer_House_Demand__c=TestDataHelper.testDealSupport.id,name='Test',Services__c='Voice',recordTypeID=ContractRecordTypeId,Hardware_Refresh_Type__c='Test',Contract_Type__c='Amendment', AccountId= TestDataHelper.testAccountObj.id); //Fulfillment_Type__c='Test'
        insert objcontract;
        
        vlocity_cmt__ContractLineItem__c  objCLI=new vlocity_cmt__ContractLineItem__c (vlocity_cmt__ContractId__c  =objcontract.id,name='test',Renewal_Credit__c='12',Hardware_Discounts__c='12' , vlocity_cmt__Product2Id__c  = TestDataHelper.testProductObj2.id);
        insert objCLI;
        
        Contract_Template_Section_Mapping__c objContractTemplateSectionMapping = new Contract_Template_Section_Mapping__c(name='Voice',Template_Section_Name__c='test');
        insert objContractTemplateSectionMapping;
        
        
        Map<String,Object> inputMap=new Map<String,Object>();
        inputMap.put('allSections',new Map<Id, vlocity_cmt__DocumentTemplateSection__c>{objDocTemplateSection.id=>objDocTemplateSection});
        inputMap.put('contextId',objcontract.id);
  //products and pricebook and pricebookentry
        List<Product2> productList = new List<Product2>();
        List<String> productNames = new List<String>();
        Pricebook2 standardBook =  new Pricebook2(Id=Test.getStandardPricebookId(), Name = 'TestPricebook', IsActive = true);
        //Insert 10 Products
        for (Integer i=1;i<=10;i++)
        {
            String name = 'Product'+i;
            Product2 product = new Product2(Name=name, Description='This is a Description'+i);
            productList.add(product);
            productNames.add(name); 
        }
        insert productList;
        //Create a Pricebook
        Pricebook2 testPricebook = new Pricebook2(Name = 'TestPricebook', IsActive = true);
        insert testPricebook;
        //Associate Products with a Pricebook entry
        List<PricebookEntry> stdPriceBookEntryList = new List<PricebookEntry>();
        productList = new List<Product2>();
        productList = [Select Id,Name from Product2 where Name IN :productNames];
        Integer j=1;
        for (Product2 product: productList)
        {           
            PricebookEntry standardPricebookEntry = new PricebookEntry(Pricebook2Id = standardBook.Id,
            Product2Id = product.Id, UnitPrice = j*2, vlocity_cmt__RecurringPrice__c = j*17, IsActive = true, UseStandardPrice = false);
            stdPriceBookEntryList.add(standardPricebookEntry);
            j++;
        }
        insert stdPriceBookEntryList;
                                                                                                                                  
        //account
        Account testAccount = new Account();
        testAccount.Name = 'Test Account';
        insert testAccount;
        
        //Insert Opportunity and Line Item
         Opportunity testOpp = new Opportunity(Name ='testOpp',
                                                AccountId = testAccount.Id,
                                                StageName = 'test stage',
                                                CloseDate = Date.today(),
                                                Pricebook2Id = testPricebook.Id);
                                                
        insert testOpp;
        
        Quote testQuote = new Quote(Name='TestQuote',Pricebook2Id = standardBook.Id, OpportunityId=testOpp.Id );
        insert testQuote;
        List<QuoteLineItem> quoteItemList = new List<QuoteLineItem>();
        Integer i=1;
        for (PricebookEntry wrapper: stdPriceBookEntryList)
        {
            QuoteLineItem qItem = new QuoteLineItem(QuoteId=testQuote.Id, PricebookEntryId = wrapper.Id, Quantity = 1, vlocity_cmt__OneTimeTotal__c = 100, vlocity_cmt__RecurringTotal__c=200, UnitPrice=100);
            quoteItemList.add(qItem);
            i++;
            if (i>5)
                break;
        }
        insert quoteItemList;
        inputMap.put('items',quoteItemList);
        
        
        Map<String,Object> outMap =  new   Map<String,Object>();
        Map<String,Object> options = new   Map<String,Object>();
        Test.startTest();
        DefaultConditionalTemplateSections.getTempSectionsBasedonProdcts(inputMap,outMap, Options);
        
        Test.stopTest();
        
        
        
        
        
    }
    
    static testMethod void testMethodThree(){
        
        System.debug('Test Method 3...');
        TestDataHelper.testAccountCreation();
        TestDataHelper.testContractCreationwithDealSupport();
        TestDataHelper.testvlocityDocTemplateteCreation();
        TestDataHelper.testContractWithContractLineItemListCreation('Voice Plan');

        vlocity_cmt__EntityFilter__c entityFilter = new vlocity_cmt__EntityFilter__c(Name='entity filter', vlocity_cmt__FilterOnObjectName__c='vlocity_cmt__ContractLineItem__c'  );
        insert entityFilter;
            
        vlocity_cmt__DocumentTemplateSection__c objDocTemplateSection=new vlocity_cmt__DocumentTemplateSection__c(vlocity_cmt__DocumentTemplateId__c=TestDataHelper.testvlocityDocTemplateObj.Id,name='test');
        insert objDocTemplateSection;
        
        vlocity_cmt__DocumentTemplateSectionCondition__c objDocTemplateSectionCondition = new vlocity_cmt__DocumentTemplateSectionCondition__c(Name = 'Test',vlocity_cmt__DocumentTemplateSectionId__c =objDocTemplateSection.Id, vlocity_cmt__Product2Id__c = TestDataHelper.testProductObj2.Id, vlocity_cmt__EntityFilterId__c = entityFilter.id, vlocity_cmt__ApplicableItemType__c = 'ContractLineItem__c' );
        insert objDocTemplateSectionCondition;
        

        Contract_Section_Filter__c objContactMapping=new Contract_Section_Filter__c(name = 'Test Section', Field_API_Name__c='Offer_House_Demand__r.Loyalty_Credit__c',Filter_Value__c='12',Filter_Operator__c='=',Section_Name__c='test');
        insert objContactMapping;
        
        
        Id ContractRecordTypeId = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Corporate Wireless').getRecordTypeId();
        
        
        contract objcontract=new contract(Offer_House_Demand__c=TestDataHelper.testDealSupport.id,name='Test',Services__c='Voice',recordTypeID=ContractRecordTypeId,Hardware_Refresh_Type__c='Test',Contract_Type__c='Amendment', AccountId= TestDataHelper.testAccountObj.id); //Fulfillment_Type__c='Test'
        insert objcontract;
        
        vlocity_cmt__ContractLineItem__c  objCLI=new vlocity_cmt__ContractLineItem__c (vlocity_cmt__ContractId__c  =objcontract.id,name='test',Renewal_Credit__c='12',Hardware_Discounts__c='12' , vlocity_cmt__Product2Id__c  = TestDataHelper.testProductObj2.id);
        insert objCLI;
        
        Contract_Template_Section_Mapping__c objContractTemplateSectionMapping = new Contract_Template_Section_Mapping__c(name='Voice',Template_Section_Name__c='test');
        insert objContractTemplateSectionMapping;
        
        
        Map<String,Object> inputMap=new Map<String,Object>();
        inputMap.put('allSections',new Map<Id, vlocity_cmt__DocumentTemplateSection__c>{objContractTemplateSectionMapping.id=>objDocTemplateSection});
        inputMap.put('contextId',objcontract.id);
      
        inputMap.put('items',TestDataHelper.testContractLineItemList);
        
        
        Map<String,Object> outMap =  new   Map<String,Object>();
        Map<String,Object> options = new   Map<String,Object>();
        Test.startTest();
        DefaultConditionalTemplateSections.getTempSectionsBasedonProdcts(inputMap,outMap, Options);
        DefaultConditionalTemplateSections.returnConditionalSections(new Map<Id, vlocity_cmt__DocumentTemplateSection__c>{objContractTemplateSectionMapping.id=>objDocTemplateSection});
        Test.stopTest();
     
    }
    
     static testMethod void testMethodFour(){
        system.debug('Test Method 4___');
        TestDataHelper.testAccountCreation();
        TestDataHelper.testContractCreationwithDealSupport();
        TestDataHelper.testvlocityDocTemplateteCreation();
        TestDataHelper.testContractWithContractLineItemListCreation('Voice Plan');
	
        
        vlocity_cmt__EntityFilter__c entityFilter = new vlocity_cmt__EntityFilter__c(Name='entity filter', vlocity_cmt__FilterOnObjectName__c='vlocity_cmt__ContractLineItem__c'  );
        insert entityFilter;
        
        vlocity_cmt__DocumentTemplateSection__c objDocTemplateSection=new vlocity_cmt__DocumentTemplateSection__c(vlocity_cmt__DocumentTemplateId__c=TestDataHelper.testvlocityDocTemplateObj.Id,name='test');
        insert objDocTemplateSection;
        
        vlocity_cmt__DocumentTemplateSectionCondition__c objDocTemplateSectionCondition = new vlocity_cmt__DocumentTemplateSectionCondition__c(Name = 'Test',vlocity_cmt__DocumentTemplateSectionId__c =objDocTemplateSection.Id, vlocity_cmt__Product2Id__c = TestDataHelper.testProductObj2.Id );
        insert objDocTemplateSectionCondition;
        
        objDocTemplateSection = [Select Id, vlocity_cmt__CountOfSectionConditions__c From vlocity_cmt__DocumentTemplateSection__c where Id = :objDocTemplateSection.Id ];

        Contract_Section_Filter__c objContactMapping=new Contract_Section_Filter__c(name = 'Test Section', Field_API_Name__c='Offer_House_Demand__r.Loyalty_Credit__c',Filter_Value__c='12',Filter_Operator__c='=',Section_Name__c='test');
        insert objContactMapping;
        
        
        Id ContractRecordTypeId = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Corporate Wireless').getRecordTypeId();
        
        
        contract objcontract=new contract(Offer_House_Demand__c=TestDataHelper.testDealSupport.id,name='Test',Services__c='Voice',recordTypeID=ContractRecordTypeId,Hardware_Refresh_Type__c='Test',Contract_Type__c='Amendment', AccountId= TestDataHelper.testAccountObj.id); //Fulfillment_Type__c='Test'
        insert objcontract;
        
        vlocity_cmt__ContractLineItem__c  objCLI=new vlocity_cmt__ContractLineItem__c (vlocity_cmt__ContractId__c  =objcontract.id,name='test',Renewal_Credit__c='12',Hardware_Discounts__c='12' , vlocity_cmt__Product2Id__c  = TestDataHelper.testProductObj2.id);
        insert objCLI;
        
        Contract_Template_Section_Mapping__c objContractTemplateSectionMapping = new Contract_Template_Section_Mapping__c(name='Voice',Template_Section_Name__c='test');
        insert objContractTemplateSectionMapping;
        
        
        Map<String,Object> inputMap=new Map<String,Object>();
        inputMap.put('allSections',new Map<Id, vlocity_cmt__DocumentTemplateSection__c>{objDocTemplateSection.id=>objDocTemplateSection});
        inputMap.put('contextId',objcontract.id);
      
        inputMap.put('items',TestDataHelper.testContractLineItemList);
        
        
        Map<String,Object> outMap =  new   Map<String,Object>();
        Map<String,Object> options = new   Map<String,Object>();
        Test.startTest();
        DefaultConditionalTemplateSections.getTempSectionsBasedonProdcts(inputMap,outMap, Options);
        
        Test.stopTest(); 
        
    }
    
     static testMethod void testMethodFive(){
        system.debug('Test Method 5___');
        TestDataHelper.testAccountCreation();
        TestDataHelper.testContractCreationwithDealSupport();
        TestDataHelper.testvlocityDocTemplateteCreation();
        TestDataHelper.testContractWithContractLineItemListCreation('Voice Plan');
	
        
        vlocity_cmt__EntityFilter__c entityFilter = new vlocity_cmt__EntityFilter__c(Name='entity filter', vlocity_cmt__FilterOnObjectName__c='vlocity_cmt__ContractLineItem__c'  );
        insert entityFilter;
        
        vlocity_cmt__DocumentTemplateSection__c objDocTemplateSection=new vlocity_cmt__DocumentTemplateSection__c(vlocity_cmt__DocumentTemplateId__c=TestDataHelper.testvlocityDocTemplateObj.Id,name='test');
        insert objDocTemplateSection;
        
        vlocity_cmt__DocumentTemplateSectionCondition__c objDocTemplateSectionCondition = new vlocity_cmt__DocumentTemplateSectionCondition__c(Name = 'Test',vlocity_cmt__DocumentTemplateSectionId__c =objDocTemplateSection.Id, vlocity_cmt__Product2Id__c = TestDataHelper.testProductObj2.Id, vlocity_cmt__EntityFilterId__c = entityFilter.id, vlocity_cmt__ApplicableItemType__c = 'ContractLineItem__c');
        insert objDocTemplateSectionCondition;
        
        objDocTemplateSection = [Select Id, vlocity_cmt__CountOfSectionConditions__c From vlocity_cmt__DocumentTemplateSection__c where Id = :objDocTemplateSection.Id ];

        Contract_Section_Filter__c objContactMapping=new Contract_Section_Filter__c(name = 'Test Section', Field_API_Name__c='Offer_House_Demand__r.Loyalty_Credit__c',Filter_Value__c='12',Filter_Operator__c='=',Section_Name__c='test');
        insert objContactMapping;
        
        
        Id ContractRecordTypeId = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Corporate Wireless').getRecordTypeId();
        
        
        contract objcontract=new contract(Offer_House_Demand__c=TestDataHelper.testDealSupport.id,name='Test',Services__c='Voice',recordTypeID=ContractRecordTypeId,Hardware_Refresh_Type__c='Test',Contract_Type__c='Amendment', AccountId= TestDataHelper.testAccountObj.id); //Fulfillment_Type__c='Test'
        insert objcontract;
        
        vlocity_cmt__ContractLineItem__c  objCLI=new vlocity_cmt__ContractLineItem__c (vlocity_cmt__ContractId__c  =objcontract.id,name='test',Renewal_Credit__c='12',Hardware_Discounts__c='12' , vlocity_cmt__Product2Id__c  = TestDataHelper.testProductObj2.id);
        insert objCLI;
        
        Contract_Template_Section_Mapping__c objContractTemplateSectionMapping = new Contract_Template_Section_Mapping__c(name='Voice',Template_Section_Name__c='test');
        insert objContractTemplateSectionMapping;
        
        
        Map<String,Object> inputMap=new Map<String,Object>();
        inputMap.put('allSections',new Map<Id, vlocity_cmt__DocumentTemplateSection__c>{objDocTemplateSection.id=>objDocTemplateSection});
        inputMap.put('contextId',objcontract.id);
      
        //inputMap.put('items',TestDataHelper.testContractLineItemList);
        
        
        Map<String,Object> outMap =  new   Map<String,Object>();
        Map<String,Object> options = new   Map<String,Object>();
        Test.startTest();
        DefaultConditionalTemplateSections.getTempSectionsBasedonProdcts(inputMap,outMap, Options);
        
        Test.stopTest();
        
        
        
        
        
    }
    
    
}