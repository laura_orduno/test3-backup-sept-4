/**
 * S2C_SearchCSIDBTNCtlr_Test
 * @description Test class for S2C_SearchCSIDBTNCtlr
 * @author Thomas Tran, Traction on Demand
 * @date 06-17-2015
 */
@isTest(seeAllData=false)
private class S2C_SearchCSIDBTNCtlr_Test {
	private static final String INSTALLPROVIDE = 'Provide/Install';
	private static final String CHANGE         = 'Change';
	private static final String DISCONNECT     = 'Disconnect';
	
	@isTest static void queryBTNCANInformation_BTN() {
		Service_Request__c newSR = createServiceRequest(INSTALLPROVIDE);
		insert newSR;

		setupPageParam(newSR, INSTALLPROVIDE);

		Account newAccount = S2C_TestUtility.createAccount('Account Test 0001');
		newAccount.BTN__c = '123456789';
		newAccount.Operating_Name_DBA__c = 'Test';
		newAccount.Correct_Legal_Name__c = 'Test';
		newAccount.Billing_Account_Active_Indicator__c = 'Y';
		insert newAccount;

		SMBCare_Address__c newSMBCareAddress = new SMBCare_Address__c(
			Account__c = newAccount.Id,
			Unit_Number__c = '123',
			Street_Address__c = 'test st',
			City__c = 'testcouver',
			State__c = 'test',
			Postal_Code__c = 't1e23s4'
		);
		insert newSMBCareAddress;

		S2C_SearchCSIDBTNCtlr ctlr = new S2C_SearchCSIDBTNCtlr();
		ctlr.srAcctId = newAccount.Id;
		ctlr.searchValue = 'BTN';
		ctlr.btnSearchOption = 'true';
		ctlr.criteriaValue = '123456789';

		Test.startTest();

		ctlr.queryBTNCANInformation();

		Test.stopTest();

		System.assert(!ctlr.btnCanResults.isEmpty());

	}

	@isTest static void associateToSR_Disconnect_Update() {
		Service_Request__c disconnectSR = createServiceRequest(DISCONNECT);
		insert disconnectSR;

		Service_Request__c provideSR = createServiceRequest(INSTALLPROVIDE);
		insert provideSR;

		pairSRs(provideSR, disconnectSR);

		setupPageParam(disconnectSR, DISCONNECT);

		Account newAccount = S2C_TestUtility.createAccount('Account Test 0001');
		newAccount.BTN__c = '123456789';
		newAccount.Operating_Name_DBA__c = 'Test';
		newAccount.Correct_Legal_Name__c = 'Test';
		newAccount.Billing_Account_Active_Indicator__c = 'Y';
		insert newAccount;

		SMBCare_Address__c newSMBCareAddress = new SMBCare_Address__c(
			Account__c = newAccount.Id,
			Unit_Number__c = '123',
			Street_Address__c = 'test st',
			City__c = 'testcouver',
			State__c = 'BC',
			Postal_Code__c = 't1e23s4',
			Province__c = 'BC'
		);
		insert newSMBCareAddress;

		S2C_SearchCSIDBTNCtlr ctlr = new S2C_SearchCSIDBTNCtlr();
		ctlr.srAcctId = newAccount.Id;
		ctlr.demarcationLocation = 'Test';
		ctlr.endCustomerName = 'Test';
		ctlr.npaNXX = 'Test';
		ctlr.location = 'Test';
		ctlr.cli = 'Test';
		ctlr.existingWorkingTN = 'Test';
		ctlr.siteReady = 'Test';
		ctlr.buildingName = 'Test';
		ctlr.newBuidling = 'Test';
		ctlr.floor = 'Test';
		ctlr.cabinet = 'Test';
		ctlr.rack = 'Test';
		ctlr.shelf = 'Test';


		ctlr.btnSearchOption = 'true';
		ctlr.criteriaValue = '123456789';

		ctlr.queryBTNCANInformation();
		ctlr.btnCanResults[0].isChecked = true;

		Test.startTest();

		ctlr.associateToSR();

		Test.stopTest();
	}

	@isTest static void queryBTNCANInformation_CAN() {
		Service_Request__c newSR = createServiceRequest(INSTALLPROVIDE);
		insert newSR;

		setupPageParam(newSR, INSTALLPROVIDE);

		Account newAccount = S2C_TestUtility.createAccount('Account Test 0001');
		newAccount.CAN__c = '123456789';
		newAccount.Operating_Name_DBA__c = 'Test';
		newAccount.Correct_Legal_Name__c = 'Test';
		newAccount.Billing_Account_Active_Indicator__c = 'Y';
		insert newAccount;

		SMBCare_Address__c newSMBCareAddress = new SMBCare_Address__c(
			Account__c = newAccount.Id,
			Unit_Number__c = '123',
			Street_Address__c = 'test st',
			City__c = 'testcouver',
			State__c = 'test',
			Postal_Code__c = 't1e23s4'
		);
		insert newSMBCareAddress;

		S2C_SearchCSIDBTNCtlr ctlr = new S2C_SearchCSIDBTNCtlr();
		ctlr.srAcctId = newAccount.Id;
		ctlr.btnSearchOption = 'false';
		ctlr.criteriaValue = '123456789';

		Test.startTest();

		ctlr.queryBTNCANInformation();

		Test.stopTest();

		System.assert(!ctlr.btnCanResults.isEmpty());

	}
	
	@isTest static void resetSearchResults() {
		Service_Request__c newSR = createServiceRequest(INSTALLPROVIDE);
		insert newSR;

		setupPageParam(newSR, INSTALLPROVIDE);

		S2C_SearchCSIDBTNCtlr ctlr = new S2C_SearchCSIDBTNCtlr();

		Test.startTest();

		ctlr.resetSearchResults();
		List<SelectOption> resultList = ctlr.getSearchValues();

		Test.stopTest();

		System.assert(ctlr.btnCanResults.isEmpty());
		System.assert(!resultList.isEmpty());
	}

	@isTest static void associateToSR_Install() {
		Service_Request__c newSR = createServiceRequest(INSTALLPROVIDE);
		insert newSR;

		setupPageParam(newSR, INSTALLPROVIDE);

		Account newAccount = S2C_TestUtility.createAccount('Account Test 0001');
		newAccount.BTN__c = '123456789';
		newAccount.Operating_Name_DBA__c = 'Test';
		newAccount.Correct_Legal_Name__c = 'Test';
		newAccount.Billing_Account_Active_Indicator__c = 'Y';
		insert newAccount;

		SMBCare_Address__c newSMBCareAddress = new SMBCare_Address__c(
			Account__c = newAccount.Id,
			Unit_Number__c = '123',
			Street_Address__c = 'test st',
			City__c = 'testcouver',
			State__c = 'BC',
			Postal_Code__c = 't1e23s4',
			Province__c = 'BC'
		);
		insert newSMBCareAddress;

		S2C_SearchCSIDBTNCtlr ctlr = new S2C_SearchCSIDBTNCtlr();
		ctlr.srAcctId = newAccount.Id;
		ctlr.btnSearchOption = 'true';
		ctlr.criteriaValue = '123456789';

		ctlr.queryBTNCANInformation();
		ctlr.btnCanResults[0].isChecked = true;

		Test.startTest();

		ctlr.associateToSR();

		Test.stopTest();
	}

	@isTest static void associateToSR_Change() {
		Service_Request__c newSR = createServiceRequest(INSTALLPROVIDE);
		insert newSR;

		setupPageParam(newSR, CHANGE);

		Account newAccount = S2C_TestUtility.createAccount('Account Test 0001');
		newAccount.BTN__c = '123456789';
		newAccount.Operating_Name_DBA__c = 'Test';
		newAccount.Correct_Legal_Name__c = 'Test';
		newAccount.Billing_Account_Active_Indicator__c = 'Y';
		insert newAccount;

		SMBCare_Address__c newSMBCareAddress = new SMBCare_Address__c(
			Account__c = newAccount.Id,
			Unit_Number__c = '123',
			Street_Address__c = 'test st',
			City__c = 'testcouver',
			State__c = 'BC',
			Postal_Code__c = 't1e23s4',
			Province__c = 'BC'
		);
		insert newSMBCareAddress;

		S2C_SearchCSIDBTNCtlr ctlr = new S2C_SearchCSIDBTNCtlr();
		ctlr.srAcctId = newAccount.Id;
		ctlr.demarcationLocation = 'Test';
		ctlr.endCustomerName = 'Test';
		ctlr.npaNXX = 'Test';
		ctlr.location = 'Test';
		ctlr.cli = 'Test';
		ctlr.existingWorkingTN = 'Test';
		ctlr.siteReady = 'Test';
		ctlr.buildingName = 'Test';
		ctlr.newBuidling = 'Test';
		ctlr.floor = 'Test';
		ctlr.cabinet = 'Test';
		ctlr.rack = 'Test';
		ctlr.shelf = 'Test';


		ctlr.btnSearchOption = 'true';
		ctlr.criteriaValue = '123456789';

		ctlr.queryBTNCANInformation();
		ctlr.btnCanResults[0].isChecked = true;

		Test.startTest();

		ctlr.associateToSR();

		Test.stopTest();
	}

	@isTest static void associateToSR_Disconnect() {
		Service_Request__c newSR = createServiceRequest(INSTALLPROVIDE);
		insert newSR;

		setupPageParam(newSR, DISCONNECT);

		Account newAccount = S2C_TestUtility.createAccount('Account Test 0001');
		newAccount.CAN__c = '123456789';
		newAccount.Operating_Name_DBA__c = 'Test';
		newAccount.Correct_Legal_Name__c = 'Test';
		newAccount.Billing_Account_Active_Indicator__c = 'Y';
		insert newAccount;

		SMBCare_Address__c newSMBCareAddress = new SMBCare_Address__c(
			Account__c = newAccount.Id,
			Unit_Number__c = '123',
			Street_Address__c = 'test st',
			City__c = 'testcouver',
			State__c = 'BC',
			Postal_Code__c = 't1e23s4',
			Province__c = 'BC'
		);
		insert newSMBCareAddress;

		S2C_SearchCSIDBTNCtlr ctlr = new S2C_SearchCSIDBTNCtlr();
		ctlr.srAcctId = newAccount.Id;
		ctlr.btnSearchOption = 'false';
		ctlr.criteriaValue = '123456789';

		ctlr.queryBTNCANInformation();
		ctlr.btnCanResults[0].isChecked = true;

		Test.startTest();

		ctlr.associateToSR();

		Test.stopTest();
	}

	@isTest static void associateToSR_Disconnect_No_Care_Address() {
		Service_Request__c newSR = createServiceRequest(INSTALLPROVIDE);
		insert newSR;

		setupPageParam(newSR, DISCONNECT);

		Account newAccount = S2C_TestUtility.createAccount('Account Test 0001');
		newAccount.CAN__c = '123456789';
		newAccount.Operating_Name_DBA__c = 'Test';
		newAccount.Correct_Legal_Name__c = 'Test';
		newAccount.Billing_Account_Active_Indicator__c = 'Y';
		insert newAccount;

		S2C_SearchCSIDBTNCtlr ctlr = new S2C_SearchCSIDBTNCtlr();
		ctlr.srAcctId = newAccount.Id;
		ctlr.btnSearchOption = 'false';
		ctlr.criteriaValue = '123456789';

		ctlr.queryBTNCANInformation();
		ctlr.btnCanResults[0].isChecked = true;

		Test.startTest();

		ctlr.associateToSR();

		Test.stopTest();
	}


	@isTest static void associateToSR_Disconnect_DML_FAILURE() {
		Service_Request__c newSR = createServiceRequest(INSTALLPROVIDE);
		insert newSR;

		setupPageParam(newSR, DISCONNECT);

		Account newAccount = S2C_TestUtility.createAccount('Account Test 0001');
		newAccount.CAN__c = '123456789';
		newAccount.Operating_Name_DBA__c = 'Test';
		newAccount.Correct_Legal_Name__c = 'Test';
		newAccount.Billing_Account_Active_Indicator__c = 'Y';
		insert newAccount;

		SMBCare_Address__c newSMBCareAddress = new SMBCare_Address__c(
			Account__c = newAccount.Id,
			Unit_Number__c = '123',
			Street_Address__c = 'test st',
			City__c = 'testcouver',
			State__c = 'BC',
			Postal_Code__c = 't1e23s4'
		);
		insert newSMBCareAddress;

		S2C_SearchCSIDBTNCtlr ctlr = new S2C_SearchCSIDBTNCtlr();
		ctlr.srAcctId = newAccount.Id;
		ctlr.btnSearchOption = 'false';
		ctlr.criteriaValue = '123456789';

		ctlr.queryBTNCANInformation();
		ctlr.btnCanResults[0].isChecked = true;

		Test.startTest();

		try {
    		ctlr.associateToSR();
		} catch (Exception e) {
    		System.assert(false, 'Exception ' + e);
    	}
		

		Test.stopTest();
	}

	@isTest static void associateToSR_Disconnect_DML_NO_SELECTION() {
		Service_Request__c newSR = createServiceRequest(INSTALLPROVIDE);
		insert newSR;

		setupPageParam(newSR, DISCONNECT);

		Account newAccount = S2C_TestUtility.createAccount('Account Test 0001');
		newAccount.CAN__c = '123456789';
		newAccount.Operating_Name_DBA__c = 'Test';
		newAccount.Correct_Legal_Name__c = 'Test';
		newAccount.Billing_Account_Active_Indicator__c = 'Y';
		insert newAccount;

		SMBCare_Address__c newSMBCareAddress = new SMBCare_Address__c(
			Account__c = newAccount.Id,
			Unit_Number__c = '123',
			Street_Address__c = 'test st',
			City__c = 'testcouver',
			State__c = 'BC',
			Postal_Code__c = 't1e23s4'
		);
		insert newSMBCareAddress;

		S2C_SearchCSIDBTNCtlr ctlr = new S2C_SearchCSIDBTNCtlr();
		ctlr.srAcctId = newAccount.Id;
		ctlr.btnSearchOption = 'false';
		ctlr.criteriaValue = '123456789';

		ctlr.queryBTNCANInformation();

		Test.startTest();

		try {
    		ctlr.associateToSR();
		} catch (Exception e) {
    		System.assert(false, 'Exception ' + e);
    	}
		

		Test.stopTest();
	}

	@isTest static void execute_page_methods_setup() {
		Service_Request__c newSR = createServiceRequest(INSTALLPROVIDE);
		insert newSR;

		setupPageParam(newSR, INSTALLPROVIDE);

		Account newAccount = S2C_TestUtility.createAccount('Account Test 0001');
		newAccount.BTN__c = '123456789';
		newAccount.Operating_Name_DBA__c = 'Test';
		newAccount.Correct_Legal_Name__c = 'Test';
		newAccount.Billing_Account_Active_Indicator__c = 'Y';
		insert newAccount;

		SMBCare_Address__c newSMBCareAddress = new SMBCare_Address__c(
			Account__c = newAccount.Id,
			Unit_Number__c = '123',
			Street_Address__c = 'test st',
			City__c = 'testcouver',
			State__c = 'test',
			Postal_Code__c = 't1e23s4'
		);
		insert newSMBCareAddress;

		S2C_SearchCSIDBTNCtlr ctlr = new S2C_SearchCSIDBTNCtlr();
		ctlr.srAcctId = newAccount.Id;
		ctlr.checkBoxStatus = 'true';
		ctlr.hasServiceLocationStatus = 'true';
		ctlr.checkboxCheck();
		List<SelectOption> siteReadyValues = ctlr.getSiteReadyValues();
		List<SelectOption> newBuildingVaules = ctlr.getNewBuildingValues();
		List<SelectOption> locationValues = ctlr.getLocationValues();
	}

	private static void setupPageParam(Service_Request__c currentServiceRequest, String srName){
		PageReference testPageRef = Page.S2C_SearchCSIDBTN;
		Test.setCurrentPage(testPageRef);
		ApexPages.currentPage().getParameters().put('SRID', currentServiceRequest.Id);
		ApexPages.currentPage().getParameters().put('SRRTNAME', srName);
	}

	private static Service_Request__c createServiceRequest(String recordTypeName){
		Id rtId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();

		Service_Request__c newServiceRequest = new Service_Request__c(
			RecordTypeID = rtId
		);

		return newServiceRequest;
	}

	private static void pairSRs(Service_Request__c provideSR, Service_Request__c disconnectSR){
		List<SRS_Service_Request_Related_Order__c> srroToInsert = new List<SRS_Service_Request_Related_Order__c>();
		String referenceNumberRecordTypeId = Schema.SObjectType.SRS_Service_Request_Related_Order__c.getRecordTypeInfosByName().get('Reference Number').getRecordTypeId();
		Set<Id> ids = new Set<Id>();
		ids.add(provideSR.Id);
		ids.add(disconnectSR.Id);
		Map<Id, Service_Request__c> srMap = new Map<Id, Service_Request__c>([SELECT Name FROM Service_Request__c WHERE Id IN :ids]);

		srroToInsert.add(
			new SRS_Service_Request_Related_Order__c(
				RecordTypeId = referenceNumberRecordTypeId,
				Name = srMap.get(provideSR.Id).Name,
				Reference_Type__c = 'Related Serv Req',
				RO_Service_Request__c = disconnectSR.Id,
				Comments__c = 'Test'
			)
		);

		srroToInsert.add(
			new SRS_Service_Request_Related_Order__c(
				RecordTypeId = referenceNumberRecordTypeId,
				Name = srMap.get(disconnectSR.Id).Name,
				Reference_Type__c = 'Related Serv Req',
				RO_Service_Request__c = provideSR.Id,
				Comments__c = 'Test'
			)
		);

		insert srroToInsert;
	}
	
}