@isTest
private class NAAS_PAC_MACDContactExt_Test{
    
    testMethod static void CreateContacttest() {       
  
        
           Schema.DescribeSObjectResult d = Schema.SObjectType.Account; 
          Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();      
          Id recSerTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
          Id recRCIDTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId();       
          
          Account RCIDacc = new Account(name='debs',recordtypeid=recRCIDTypeId);
          Account seracc = new Account(name='123 street',recordtypeid=recSerTypeId,parentid=RCIDacc.id);        
          
          List<Account> acclist= new List<Account>();          
          acclist.add(RCIDacc);
          acclist.add(seracc );
          insert acclist;
         system.assertEquals(acclist.size(), 2); 
          
         smbcare_address__c address= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N5',FMS_Address_ID__c='987651',account__c=seracc.id,Service_Account_Id__c=seracc.Id); 
         insert address;
        ServiceAddressData SDate = new ServiceAddressData();
        //PageReference pageref = Page.NAAS_PAC_MACD;
      //  Test.setCurrentPage(pageref);        
       // pageref.getparameters().put('id', seracc.Id);
        
        NAAS_InOrderFlowContactController contactcon= new NAAS_InOrderFlowContactController();
        
        NAAS_PAC_MACDContactExt testCon1 = new NAAS_PAC_MACDContactExt(contactcon);
          
        NAAS_PAC_MACD_Controller con = new NAAS_PAC_MACD_Controller();
        NAAS_PAC_MACDContactExt testCon2 = new NAAS_PAC_MACDContactExt(con);
          
        Contact conTest = new Contact(LastName='testLastName',FirstName='testFirstName',email='a@gmail.com',phone='1234567890');   
        insert conTest;
        
        Test.setCurrentPageReference(new PageReference('Page.NAAS_PAC_MACD')); 
        System.currentPageReference().getParameters().put('id', '004132121212121');        
        Order newOrder = new Order(Name='Test',Service_Address__c = address.Id, AccountId = seracc.Id,Status = 'Draft',EffectiveDate = System.Today() );
        //newOrder.CustomerAuthorizedBy.Id=conTest.Id;
        Insert newOrder;
        testCon2.new_contact=conTest;
        testCon2.onSelectContact();        
        testCon2.onLoad();        
        System.currentPageReference().getParameters().put('id',seracc.id);
        testCon2.onLoad();
        testCon2.toggleNewContactForm();
                
        System.currentPageReference().getParameters().put('id',conTest.id);
        try{
        testCon2.createNewContact();
        testCon2.CancelNewContact();
        }catch(exception ex){}
         testCon2.searchTerm='test';
         testCon2.onSearch(); 
    
         NAAS_PAC_MACDContactExt.selectedContactId=conTest.id;
         testCon2.onSelectContact();
         System.currentPageReference().getParameters().put('src','os');
         
         System.currentPageReference().getParameters().put('id','');
         System.currentPageReference().getParameters().put('OrderId',newOrder.Id);
         
         testCon2.onLoad();
        
        
        //}catch(exception ex){
         
       // }  
            
    }

   }