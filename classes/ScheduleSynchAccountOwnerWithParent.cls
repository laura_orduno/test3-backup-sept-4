global class ScheduleSynchAccountOwnerWithParent implements Schedulable{
    global void execute(SchedulableContext sc) {
        batch_SynchAccountOwnerWithParent b = new batch_SynchAccountOwnerWithParent();
        database.executeBatch(b);
    }
}