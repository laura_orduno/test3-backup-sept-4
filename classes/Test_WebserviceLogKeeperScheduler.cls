@istest
public class Test_WebserviceLogKeeperScheduler {
	@istest
    static void testKeeper(){
        webservice_integration_error_log__c deletingRecord=new webservice_integration_error_log__c(name='Test Deleting Record',apex_class_and_method__c='Test Deleting Class and Method',error_code_and_message__c='Test Deleting Message',webservice_name__c='Test Deleting Webservice Name');
        webservice_integration_error_log__c notDeletingRecord=new webservice_integration_error_log__c(name='Test Not Deleting Record',apex_class_and_method__c='Test Not Deleting Class and Method',error_code_and_message__c='Test Not Deleting Message',webservice_name__c='Test Not Deleting Webservice Name');
        insert deletingRecord;
        insert notDeletingRecord;
        string query='select id from webservice_integration_error_log__c where name=\'Test Deleting Record\'';
        test.starttest();
        WebserviceLogKeeperScheduler scheduler=new WebserviceLogKeeperScheduler();
        scheduler.query=query;
        system.schedule(scheduler.name,scheduler.cronExp,scheduler);
        test.stoptest();
    }
}