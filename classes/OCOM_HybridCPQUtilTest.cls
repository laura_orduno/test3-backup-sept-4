/**
     Vlocity
     Version July 5 2016
   Unit Test Class for OCOM_HybridCPQUtil
*/

@isTest
public class OCOM_HybridCPQUtilTest {
//vlocity_cmt.VlocityOpenInterface vlInterface;
    
   

    public void OCOM_HybridCPQUtilTest1(){  
    }

       public static testmethod void test_CreateOCOMOrder() {
        Account rcidAcct;
    Account servAcct;
     Account parentAcct;
     List<Account> servAccts;
     List<Contact> lisContacts;
        Id recTypeIdService=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
            Id recTypeIdRCID=Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId();
            System.assertNotEquals(recTypeIdService,'');
            System.assertNotEquals(recTypeIdService,null);
            rcidAcct = OCOM_UnitTestUtil.getTestAccounts('RCID', 1, recTypeIdRCID)[0];
            parentAcct =  OCOM_UnitTestUtil.getTestAccounts('RCID', 1, recTypeIdRCID)[0];
            rcidAcct.parentId=parentAcct.Id;
            update rcidAcct;
            servAcct = OCOM_UnitTestUtil.getTestAccounts('Service ', 1, recTypeIdService)[0];
            servAcct.ParentId = rcidAcct.Id;
            update servAcct;
            
            lisContacts = OCOM_UnitTestUtil.getTestContacts('Test Contact', 1, servAcct.id);
        OCOM_HybridCPQUtil tes = new OCOM_HybridCPQUtil();
         
        Map<String, Object> options = new Map<String, Object>();
        Map<String, Object> input = new Map<String, Object>();
        Map<String, Object> output = new Map<String, Object>();
        input.put('ParentAccId',rcidAcct.ParentId);
        input.put('ServiceAcctId',servAcct.Id);
        input.put('ContactId',lisContacts[0].Id);
        input.put('Type','New Services');
        tes.invokeMethod('createOcomOrder',input, output, options);
        smbcare_address__c testAddress= new smbcare_address__c( Unit_Number__c='1', Street_Address__c='444 Berry ST', City__c='San Francisco',State__c='CA', Postal_Code__c='94158',FMS_Address_ID__c='987651',account__c=servAcct.Id); 
            testAddress.Service_Account_Id__c = servAcct.Id;
        insert testAddress;
        tes.invokeMethod('createOcomOrder',input, output, options);
        //Id orderId = (Id)output.get('OrderId')
        

    }

}