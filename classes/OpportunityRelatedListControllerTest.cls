@isTest
public class OpportunityRelatedListControllerTest {
    @isTest
    private static void sendRedirect() {
        List<Opportunity> oppList=OrdrTestDataFactory.createOpprtunities(1);
        Opportunity opObj=oppList.get(0);
        PageReference pageRef = Page.OpportunityOverridePage;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', opObj.id );
        ApexPages.StandardController sc = new ApexPages.standardController(opObj);
        OpportunityRelatedListController cntrl=new OpportunityRelatedListController(sc);
        Test.startTest();
        cntrl.sendRedirect();
        Test.stopTest();
    }
    @isTest
    private static void checkNewOrder() {
        List<Opportunity> oppList=OrdrTestDataFactory.createOpprtunities(1);
        Opportunity opObj=oppList.get(0);
        PageReference pageRef = Page.OpportunityOverridePage;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', opObj.id );
        ApexPages.StandardController sc = new ApexPages.standardController(opObj);
        OpportunityRelatedListController cntrl=new OpportunityRelatedListController(sc);
        Test.startTest();
         Product2 p = new Product2(Name='Test', isActive=true);
        insert p;
        Opp_Product_Item__c opi = new Opp_Product_Item__c();
        opi.Opportunity__c = opObj.id;
        opi.Product__c = p.id;
        insert opi;
        cntrl.checkNewOrder();
        Test.stopTest();
    }
    @isTest
    private static void attachOrder(){
        List<Opportunity> oppList=OrdrTestDataFactory.createOpprtunities(1);
        Opportunity opObj=oppList.get(0);
        PageReference pageRef = Page.OpportunityOverridePage;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', opObj.id );
        ApexPages.StandardController sc = new ApexPages.standardController(opObj);
        OpportunityRelatedListController cntrl=new OpportunityRelatedListController(sc);
        Test.startTest();
        cntrl.attachOrder();
        Test.stopTest();
    }
    @isTest
    private static void deattachOrder(){
        List<Opportunity> oppList=OrdrTestDataFactory.createOpprtunities(1);
        Opportunity opObj=oppList.get(0);
        PageReference pageRef = Page.OpportunityOverridePage;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', opObj.id );
        Test.startTest();
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        ApexPages.StandardController sc = new ApexPages.standardController(opObj);
        OpportunityRelatedListController cntrl=new OpportunityRelatedListController(sc);
        
        cntrl.ordIdStr=orderId;
        cntrl.deattachOrder();
        Test.stopTest();
    }
    @isTest
    private static void extractOrdIdList(){
        String orderId1=OrdrTestDataFactory.singleMethodForOrderId();
         
        Test.startTest();
        //String orderId2=OrdrTestDataFactory.singleMethodForOrderId();
        String jsonStr='[{"recordId":"'+orderId1+'"}]';
        OpportunityRelatedListController.extractOrdIdList(jsonStr);
        Test.stopTest();
        
    }
    /*public OpportunityOrderInfo(Id recordId){

public class OpportunityRelatedListController {
public class OpportunityOrderInfo{*/
}