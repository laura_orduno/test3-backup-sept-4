public class OpportunityRelatedListController {
    public PageReference sendRedirect() {
        String oppId = ApexPages.currentPage().getParameters().get('id');
        PageReference pageRef = new PageReference('/'+oppId+'?arg=1');
        pageRef.setRedirect(true);
        System.debug('Redirected');
        return pageRef;
    }
    String opportunityId;
    public String primaryContactId {get;set;}
    public String accountName {get;set;}
    public Opportunity oppObj {get;set;}
    public List<Order> ordObjList {get;set;}
    public List<Opportunity> ordOppObjList {get;set;}
    public String oppIdStr{get;set;}
    public String ordIdStr{get;set;}
    public String newOrderPath{get;set;}
    public String attachOrderResult{get;set;}
    public String deattachOrderResult{get;set;}
    public String opiJSON{get;set;}
    public Integer relatedListSize{get {
        List<Order> orderObjList=[select OrderNumber,Condition__c,Status,RequestedDate__c,Owner.Name,CreatedDate,Opportunity.name from order where opportunityId=:opportunityId];
        return orderObjList.size()+1;
    }
                                   set;}
    public OpportunityRelatedListController(ApexPages.StandardController controller){
        opportunityId=controller.getRecord().id;
        oppObj=[select ownerid,AccountId,account.name,name,Order_Opportunity__c from opportunity where id=:opportunityId];
        accountName=oppObj.account.name;
        ordObjList=[select id,OrderNumber,Condition__c,Status,RequestedDate__c,Owner.Name,CreatedDate,Opportunity.name from order where opportunityId=:opportunityId];
        relatedListSize=ordObjList.size()+1;
        if(String.isNotBlank(oppObj.Order_Opportunity__c)){
            ordOppObjList=[SELECT id,ownerid,Order_ID__c,Condition__c,Status__c,Requested_Due_Date__c,Owner_Name__c,CreatedDate FROM OPPORTUNITY WHERE ID=:oppObj.Order_Opportunity__c];
        }
        List<OpportunityContactRole>  oppContactRoleList=[SELECT Id, OpportunityId, ContactId, Role, IsPrimary FROM OpportunityContactRole where OpportunityId=:opportunityId and IsPrimary=true];
        
        if(oppContactRoleList!=null && oppContactRoleList.size()>0){
            primaryContactId=oppContactRoleList.get(0).ContactId;
        }
        //opp=(Opportunity)controller.getRecord();
        //opp=[select AccountId,name from Opportunity where id=:opp.id];
    }
    public PageReference checkNewOrder() {
        System.debug('Inside checkNewOrder method');
        try {
            if(String.isBlank(oppIdStr)){
                oppIdStr=ApexPages.currentPage().getParameters().get('id');
            }
            List<Opp_Product_Item__c> oppProdList =[SELECT id,Product__c FROM Opp_Product_Item__c where  Opportunity__c=:oppIdStr order by createddate desc];
            System.debug('oppProdList.size='+ oppProdList.size());
            List<Opportunity> oppOrdList=[select Order_Opportunity__c from opportunity where id=:oppIdStr];
            if(oppOrdList.size()>0){
                String oppOrdIdStr=oppOrdList.get(0).Order_Opportunity__c;
                List<Service_Request__c> srObjList=[ SELECT id,Opportunity__c, Product_Name__c, Opportunity_Product_Item__c FROM Service_Request__c where  Opportunity__c=:oppOrdIdStr];
                System.debug('srObjList.size='+ srObjList.size());
                Set<String> oppSolProdStrSet=new Set<String>();
                Map<String,String> oppProdItemIdProd2IdMap=new Map<String,String>();
                for(Opp_Product_Item__c obj:oppProdList){
                    //String prdStr=obj.Product__c;
                    String prdStr=obj.id;
                    if(String.isNotBlank(prdStr)){
                        if(prdStr.length()>15){
                            prdStr=prdStr.substring(0, 15);
                        }
                        oppSolProdStrSet.add(prdStr);
                    }
                }
                Set<String> srProdStrSet=new Set<String>();
                for(Service_Request__c srsObj:srObjList){
                    String prdStr=srsObj.Opportunity_Product_Item__c;
                    if(String.isNotBlank(prdStr)){
                        if(prdStr.length()>15){
                            prdStr=prdStr.substring(0, 15);
                        }
                        srProdStrSet.add(prdStr);
                    }
                }
                oppSolProdStrSet.removeAll(srProdStrSet);
                Set<String> prod2Idlist=new Set<String>();
                for(Opp_Product_Item__c obj:oppProdList){
                    String prdStr=obj.id;
                    if(String.isNotBlank(prdStr)){
                        if(prdStr.length()>15){
                            prdStr=prdStr.substring(0, 15);
                        }
                    }
                    if(oppSolProdStrSet.contains(prdStr)){
                        prod2Idlist.add(obj.Product__c);
                    }
                }
                System.debug('oppSolProdStrSet='+ oppSolProdStrSet);
                System.debug('srProdStrSet='+ srProdStrSet);
                System.debug('prod2Idlist='+ prod2Idlist);
                if(prod2Idlist.size()>0){
                    List<B2B_EPC_Products_Family__mdt>  b2bProdFamilyList=[SELECT Id, Product_Family__c, B2B_Flow__c FROM B2B_EPC_Products_Family__mdt];
                    List<Product2> prodList=[select ordermgmtid__c,Product_Family__c from product2 where id in:prod2Idlist];
                    Boolean isB2b=false;
                    Boolean isComplex=false;
                    for(Product2 prodObj:prodList){
                        if(String.isNotBlank(prodObj.ordermgmtid__c)){
                            isB2b=true;
                        }
                        for(B2B_EPC_Products_Family__mdt prodFamilyObj:b2bProdFamilyList){
                            if(String.isNotBlank(prodFamilyObj.Product_Family__c) && String.isNotBlank(prodObj.Product_Family__c) && prodFamilyObj.Product_Family__c== prodObj.Product_Family__c){
                                if(prodFamilyObj.B2B_Flow__c){
                                    isB2b=true;
                                }
                            }
                        }
                        if(String.isBlank(prodObj.ordermgmtid__c)){
                            isComplex=true;
                        }
                    }
                    if(isB2b){
                        newOrderPath='B2B';
                    }
                    if(isComplex){
                        newOrderPath='Complex';
                    }
                    if(isB2b && isComplex){
                        newOrderPath='Both';
                    }
                }
            }
        } catch(Exception e) {
            system.debug(e);
            //throw e;
        }
        return null  ;
    }
    public PageReference attachOrder(){
        if(String.isBlank(oppIdStr)){
            oppIdStr=ApexPages.currentPage().getParameters().get('id');
        }
        System.debug(' '+opiJSON);
        List<String> ordIdList = extractOrdIdList(opiJSON);
        System.debug(' '+ordIdList);
        List<Order> ordList=[select vlocity_cmt__OpportunityId__c,opportunityid from order where id in: ordIdList];
        for(Order ordObj:ordList){
            ordObj.vlocity_cmt__OpportunityId__c=oppIdStr;
            ordObj.opportunityid=oppIdStr;
        }
        Database.SaveResult[] saveResultlist=Database.update(ordList, true);
        for (Database.SaveResult sr : saveResultlist) {
            if (sr.isSuccess()) {
                continue;
            }
            attachOrderResult= 'false';
        }
        attachOrderResult= 'true';
        List<Order> orderObjList=[select OrderNumber,Condition__c,Status,RequestedDate__c,Owner.Name,CreatedDate,Opportunity.name from order where opportunityId=:opportunityId];
        relatedListSize= orderObjList.size()+1;
        attachOrderResult='{"attachOrderResult":"'+attachOrderResult+'","relatedListSize":"'+relatedListSize+'"}';
        return null;
    }
    public PageReference deattachOrder(){
        if(String.isBlank(ordIdStr)){
            return null;
        }
        if(String.isBlank(oppIdStr)){
            oppIdStr=ApexPages.currentPage().getParameters().get('id');
        }
        List<Order> ordList=[select vlocity_cmt__OpportunityId__c,opportunityid from order where id =:ordIdStr];
        for(Order ordObj:ordList){
            ordObj.vlocity_cmt__OpportunityId__c=null;
            ordObj.opportunityid=null;
        }
        Database.SaveResult[] saveResultlist=Database.update(ordList, true);
        for (Database.SaveResult sr : saveResultlist) {
            if (sr.isSuccess()) {
                continue;
            }
            deattachOrderResult= 'false';
        }
        if(deattachOrderResult!='false'){
            deattachOrderResult= 'true';
        }
        List<Order> orderObjList=[select OrderNumber,Condition__c,Status,RequestedDate__c,Owner.Name,CreatedDate,Opportunity.name from order where opportunityId=:opportunityId];
        relatedListSize= orderObjList.size()+1;
        attachOrderResult='{"deattachOrderResult":"'+deattachOrderResult+'","relatedListSize":"'+relatedListSize+'"}';
        return null;
    }
    @TestVisible
    private static List<String> extractOrdIdList(String opiJson){
        List<String> ordIdList=new List<String>();
        if(String.isBlank(opiJson)){
            return ordIdList;
        }
        List<OpportunityOrderInfo> opiInfoList = (List<OpportunityOrderInfo>)JSON.deserialize(opiJson, List<OpportunityOrderInfo>.class);
        Map<Id, OpportunityOrderInfo> opportunityOrderInfoMap = new Map<Id, OpportunityOrderInfo>();
        for(OpportunityOrderInfo currentOpportunityOrderInfo : opiInfoList){
            //opportunityOrderInfoMap.put(currentOpportunityOrderInfo.recordId, currentOpportunityOrderInfo);
            ordIdList.add(currentOpportunityOrderInfo.recordId);
        }
        return ordIdList;
    }
    public class OpportunityOrderInfo{
        public String recordId {get; set;}
        public OpportunityOrderInfo(String recordId){
            this.recordId = recordId;
        }
    }
}