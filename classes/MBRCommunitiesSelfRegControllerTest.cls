/**
 * An apex page controller that supports self registration of users in communities that allow self registration
 */
/*@IsTest public with sharing class MBRCommunitiesSelfRegControllerTest {
    @IsTest(SeeAllData=true) 
    public static void testMBRCommunitiesSelfRegController() {
        MBRCommunitiesSelfRegController controller = new MBRCommunitiesSelfRegController();
        controller.firstName = 'FirstName';
        controller.lastName = 'LastName';
        controller.email = 'test@force.com';
        controller.communityNickname = 'test';
        
        // registerUser will always return null when the page isn't accessed as a guest user
        System.assert(controller.registerUser() == null);    
        
        controller.password = 'abcd1234';
        controller.confirmPassword = 'abcd123';
        System.assert(controller.registerUser() == null);  
    }    
}*/

@istest(SeeAllData=false)
public with sharing class MBRCommunitiesSelfRegControllerTest {

    @isTest(SeeAllData=false)
    public static void testMBRSelfRegController() {

        User guestUser = MBRTestUtils.createGuestUser();
        Account cbucidAcct = new Account(Name = 'CBUCID Account', CBUCID_Parent_Id__c = 'CBUCID_123');
        insert cbucidAcct;

        Account a = new Account(Name = 'Unit Test Account', parentId = cbucidAcct.Id, rcid__c = 'rcid_123');
        insert a;
        System.debug('a: ' + a);

        // Fields: 'Name', 'FirstName', 'LastName', 'Email', 'AccountId', 'CanAllowPortalSelfReg'
        Contact c = new Contact(Phone = '6045551212', Email = 'test@unit-test.com', FirstName = 'Testy', LastName = 'Tester', AccountId = a.Id, CanAllowPortalSelfReg = true);
        insert c;
        // requery
        c = [SELECT Id, Phone, Email, FirstName, LastName, AccountId, Token__c, Token_Encoded__c FROM Contact WHERE Id = :c.Id];
        System.debug('c: ' + c);

        PageReference pageRef = Page.MBRConfirmRegister;
        pageRef.getParameters().put('id', c.Id);
        pageRef.getParameters().put('key', c.Token__c);

        System.runAs(guestUser) {

            Test.setCurrentPage(pageRef);

            Test.startTest();

            MBRCommunitiesSelfRegController ctlr = new MBRCommunitiesSelfRegController();

            ctlr.init();
ctlr.accountNumber = '123456';
            ctlr.pin = 'test';
            ctlr.communityNickname = 'test';
            ctlr.password = 'abcABC.123';
            ctlr.community = 'vitilcare';
            ctlr.confirmPassword = ctlr.password;

            String firstLastName = ctlr.firstLastName;
            String contactAccountName = ctlr.contactAccountName;

            PageReference finalPageRef = ctlr.registerUser();
	
            // strong password	
//            ctlr.password = '!abcABC.123';
//            PageReference finalPageRef = ctlr.registerUser();

            Test.stopTest();

            User createdUser = [SELECT Id, ContactId, Contact.Id FROM User ORDER BY CreatedDate DESC limit 1];
            System.debug('createdUser: ' + createdUser);

            System.assertEquals(0, ctlr.errors==null?0:ctlr.errors.size());
        }
    }

    @isTest(SeeAllData=false)
    public static void testMBRSelfRegUsernameTaken() {

        User existingUser = MBRTestUtils.createPortalUser('');
        System.debug('existingUser: ' + existingUser);

        //List<User> users = [SELECT Id, Username FROM User WHERE UserType = 'CSPLitePortal' AND Username = :existingUser.Username LIMIT 1];
        //System.debug('users: ' + users);

        User guestUser = MBRTestUtils.createGuestUser();

        Account cbucidAcct = new Account(Name = 'CBUCID Account', CBUCID_Parent_Id__c = 'CBUCID_123');
        insert cbucidAcct;

        Account a = new Account(Name = 'Unit Test Account', parentId = cbucidAcct.Id, rcid__c = 'rcid_123');
        insert a;
        System.debug('a: ' + a);

        // Fields: 'Name', 'FirstName', 'LastName', 'Email', 'AccountId', 'CanAllowPortalSelfReg'
        // IMPORTANT: contact email needs to be 'test-user@unit-test.com' to collide with existing user's username
        Contact c = new Contact(Phone = '6045551212', Email = 'test-user@unit-test.com', FirstName = 'Testy', LastName = 'Tester', AccountId = a.Id, CanAllowPortalSelfReg = true);
        insert c;
        // requery
        c = [SELECT Id, Phone, Email, FirstName, LastName, AccountId, Token__c, Token_Encoded__c FROM Contact WHERE Id = :c.Id];
        System.debug('c: ' + c);

        PageReference pageRef = Page.MBRConfirmRegister;
        pageRef.getParameters().put('id', c.Id);
        pageRef.getParameters().put('key', c.Token__c);

        System.runAs(guestUser) {

            Test.setCurrentPage(pageRef);

            Test.startTest();

            MBRCommunitiesSelfRegController ctlr = new MBRCommunitiesSelfRegController();

            ctlr.init();
ctlr.community = 'mbr';
            ctlr.password = 'abcABC.123';
            ctlr.confirmPassword = ctlr.password;

            System.debug('ctlr.email: ' + ctlr.email);
            ctlr.LanguageType = 'en_US';
            PageReference finalPageRef = ctlr.registerUser();

            System.debug('ctlr.errors: ' + ctlr.errors);

            Test.stopTest();

            System.assertEquals( ctlr.userNameTaken, Label.MBRUserNameTaken );
        }
    }

    @isTest(SeeAllData=false)
    public static void testMBRSelfRegWeakPassword() {

        User existingUser = MBRTestUtils.createPortalUser('');
        System.debug('existingUser: ' + existingUser);

        //List<User> users = [SELECT Id, Username FROM User WHERE UserType = 'CSPLitePortal' AND Username = :existingUser.Username LIMIT 1];
        //System.debug('users: ' + users);

        User guestUser = MBRTestUtils.createGuestUser();

        Account cbucidAcct = new Account(Name = 'CBUCID Account', CBUCID_Parent_Id__c = 'CBUCID_123');
        insert cbucidAcct;

        Account a = new Account(Name = 'Unit Test Account', parentId = cbucidAcct.Id, rcid__c = 'rcid_123');
        insert a;
        System.debug('a: ' + a);

        // Fields: 'Name', 'FirstName', 'LastName', 'Email', 'AccountId', 'CanAllowPortalSelfReg'
        Contact c = new Contact(Phone = '6045551212', Email = 'test@unit-test.com', FirstName = 'Testy', LastName = 'Tester', AccountId = a.Id, CanAllowPortalSelfReg = true);
        insert c;
        // requery
        c = [SELECT Id, Phone, Email, FirstName, LastName, AccountId, Token__c, Token_Encoded__c FROM Contact WHERE Id = :c.Id];
        System.debug('c: ' + c);

        PageReference pageRef = Page.MBRConfirmRegister;
        pageRef.getParameters().put('id', c.Id);
        pageRef.getParameters().put('key', c.Token__c);

        System.runAs(guestUser) {

            Test.setCurrentPage(pageRef);

            Test.startTest();

            MBRCommunitiesSelfRegController ctlr = new MBRCommunitiesSelfRegController();

         ctlr.init();

            ctlr.password = 'password';
            ctlr.confirmPassword = ctlr.password;

            System.debug('ctlr.email: ' + ctlr.email);
            ctlr.LanguageType = 'en_US';
            PageReference finalPageRef = ctlr.registerUser();
//ctlr.registerUser();
            System.debug('ctlr.errors: ' + ctlr.errors);
  System.debug('ctlr.weakPassword: ' + ctlr.weakPassword);
           Test.stopTest();

            System.assertEquals( ctlr.weakPassword, Label.MBRWeakPasswordError );
        }
    }

    @isTest(SeeAllData=false)
    public static void testMBRSelfRegPasswordNoMatch() {

        User existingUser = MBRTestUtils.createPortalUser('');
        System.debug('existingUser: ' + existingUser);

        //List<User> users = [SELECT Id, Username FROM User WHERE UserType = 'CSPLitePortal' AND Username = :existingUser.Username LIMIT 1];
        //System.debug('users: ' + users);

        User guestUser = MBRTestUtils.createGuestUser();

        Account cbucidAcct = new Account(Name = 'CBUCID Account', CBUCID_Parent_Id__c = 'CBUCID_123');
        insert cbucidAcct;

        Account a = new Account(Name = 'Unit Test Account', parentId = cbucidAcct.Id, rcid__c = 'rcid_123');
        insert a;
        System.debug('a: ' + a);

        // Fields: 'Name', 'FirstName', 'LastName', 'Email', 'AccountId', 'CanAllowPortalSelfReg'
        Contact c = new Contact(Phone = '6045551212', Email = 'test@unit-test.com', FirstName = 'Testy', LastName = 'Tester', AccountId = a.Id, CanAllowPortalSelfReg = true);
        insert c;
        // requery
        c = [SELECT Id, Phone, Email, FirstName, LastName, AccountId, Token__c, Token_Encoded__c FROM Contact WHERE Id = :c.Id];
        System.debug('c: ' + c);

        PageReference pageRef = Page.MBRConfirmRegister;
        pageRef.getParameters().put('id', c.Id);
        pageRef.getParameters().put('key', c.Token__c);

        System.runAs(guestUser) {

            Test.setCurrentPage(pageRef);

            Test.startTest();

            MBRCommunitiesSelfRegController ctlr = new MBRCommunitiesSelfRegController();

            ctlr.init();

            ctlr.password = 'abcABC.123';
            ctlr.confirmPassword = 'abcABC.456';
ctlr.LanguageType = 'en_US';
            System.debug('ctlr.email: ' + ctlr.email);
            PageReference finalPageRef = ctlr.registerUser();
        ctlr.genericError = true;
        ctlr.genericMessages = new List<String>();
        ctlr.genericMessagesCount = 0;
        ctlr.userNameTaken = '';
        ctlr.invalidNickname = '';
        ctlr.weakPassword = '';
        ctlr.passwordNotMatch = '';
        ctlr.recoverPW = '';  
            System.debug('ctlr.errors: ' + ctlr.errors);

            Test.stopTest();

            //System.assertEquals( ctlr.passwordNotMatch , Label.MBRPasswordDoNotMatchError );
        }
    }
    
    @isTest(SeeAllData=false)
    public static void testMBRSelfRegSuccess() {

        User existingUser = MBRTestUtils.createPortalUser('');
        System.debug('existingUser: ' + existingUser);

        //List<User> users = [SELECT Id, Username FROM User WHERE UserType = 'CSPLitePortal' AND Username = :existingUser.Username LIMIT 1];
        //System.debug('users: ' + users);

        User guestUser = MBRTestUtils.createGuestUser();

        Account cbucidAcct = new Account(Name = 'CBUCID Account', CBUCID_Parent_Id__c = 'CBUCID_123');
        insert cbucidAcct;

        Account a = new Account(Name = 'Unit Test Account', parentId = cbucidAcct.Id, rcid__c = 'rcid_123');
        insert a;
        System.debug('a: ' + a);

        // Fields: 'Name', 'FirstName', 'LastName', 'Email', 'AccountId', 'CanAllowPortalSelfReg'
        Contact c = new Contact(Phone = '6045551212', Email = 'test@unit-test.com', FirstName = 'Testy', LastName = 'Tester', AccountId = a.Id, CanAllowPortalSelfReg = true);
        insert c;
        // requery
        c = [SELECT Id, Phone, Email, FirstName, LastName, AccountId, Token__c, Token_Encoded__c FROM Contact WHERE Id = :c.Id];
        System.debug('c: ' + c);

        PageReference pageRef = Page.MBRConfirmRegister;
        pageRef.getParameters().put('id', c.Id);
        pageRef.getParameters().put('key', c.Token__c);

        System.runAs(guestUser) {

            Test.setCurrentPage(pageRef);

            Test.startTest();

            MBRCommunitiesSelfRegController ctlr = new MBRCommunitiesSelfRegController();

            ctlr.init();

            ctlr.password = 'abcABC.123';
            ctlr.confirmPassword = ctlr.password;
ctlr.LanguageType = 'en_US';
            System.debug('ctlr.email: ' + ctlr.email);
            PageReference finalPageRef = ctlr.registerUser();
        ctlr.genericError = true;
        ctlr.genericMessages = new List<String>();
        ctlr.genericMessagesCount = 0;
        ctlr.userNameTaken = '';
        ctlr.invalidNickname = '';
        ctlr.weakPassword = '';
        ctlr.passwordNotMatch = '';
        ctlr.recoverPW = '';  
            System.debug('ctlr.errors: ' + ctlr.errors);

            Test.stopTest();

            //System.assertEquals( ctlr.passwordNotMatch , Label.MBRPasswordDoNotMatchError );
        }
    }
    

    @isTest(SeeAllData=false)
    public static void testMBRSelfRegControllerTokenBad() {

        Account a = new Account(Name = 'Unit Test Account');
        insert a;
        System.debug('a: ' + a);

        // Fields: 'Name', 'FirstName', 'LastName', 'Email', 'AccountId', 'CanAllowPortalSelfReg'
        Contact c = new Contact(Phone = '6045551212', Email = 'test@unit-test.com', FirstName = 'Testy', LastName = 'Tester', AccountId = a.Id, CanAllowPortalSelfReg = true);
        insert c;
        System.debug('c: ' + c);

        PageReference pageRef = Page.MBRConfirmRegister;
        pageRef.getParameters().put('id', c.Id);
        pageRef.getParameters().put('key', 'asdfasdfasdfasfdas');

        Test.setCurrentPage(pageRef);

        Test.startTest();

        MBRCommunitiesSelfRegController ctlr = new MBRCommunitiesSelfRegController();

        ctlr.init();

        Test.stopTest();

        System.assertEquals( ctlr.recoverPW , Label.MBRInvalidIdToken );
    }

    
    
/*
    static testMethod void test(){
        account acct = new account();
        acct.CBUCID_Parent_Id__c = '111';
        acct.name = 'test';
        insert acct;
        account acct2 = new account();
        acct2.name = 'test';
        acct2.parentid = acct.id;
        acct2.rCID__c = '222';
        acct2.Wireline_PIN__c = '2222';
        acct2.Wireless_PIN__c = '2222';
        insert acct2;
        contact con = new contact();
        con.email = 'test@test.com';
        con.CanAllowPortalSelfReg = true;
        con.accountid = acct2.id;
        con.lastname = 'test';
        insert con;
    
        MBRCommunitiesSelfRegController csrc = new MBRCommunitiesSelfRegController();
        Test.setcurrentpage(Page.MBRCommunitiesSelfReg);
        
        //csrc.agreeToTC = true;
        csrc.firstName = 'test';
        csrc.lastName = 'test';
        csrc.email = 'test@test.com';
        csrc.pin = '2222';
        csrc.accountNumber = '222';
        csrc.password = 'test';
        csrc.confirmPassword = 'test';
        csrc.communityNickname = 'test';
        
        PageReference pageRef = csrc.registerUser();
        System.assertEquals(null,null);
    }
    
    static testMethod void test2(){
        account acct = new account();
        acct.CBUCID_Parent_Id__c = '111';
        acct.name = 'test';
        insert acct;
        account acct2 = new account();
        acct2.name = 'test';
        acct2.parentid = acct.id;
        acct2.rCID__c = '222';
        acct2.Wireline_PIN__c = '2222';
        acct2.Wireless_PIN__c = '2222';
        insert acct2;
        contact con = new contact();
        con.email = 'test@test.com';
        con.CanAllowPortalSelfReg = true;
        con.accountid = acct2.id;
        con.lastname = 'test';
        insert con;
    
        MBRCommunitiesSelfRegController csrc = new MBRCommunitiesSelfRegController();
        Test.setcurrentpage(Page.MBRCommunitiesSelfReg);
        
        //csrc.agreeToTC = true;
        csrc.firstName = 'test';
        csrc.lastName = 'test';
        csrc.email = 'test@test.com';
        csrc.pin = '2222';
        csrc.accountNumber = '222';
        csrc.communityNickname = 'test';
        
        PageReference pageRef = csrc.registerUser();
        System.assertEquals(pageRef.getURL(),Page.MBRCommunitiesSelfRegConfirm.getURL());
    }*/
}