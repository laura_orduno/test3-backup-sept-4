/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global virtual class AllocationService {
    global static ADRA.AllocationService getInstance(String objectName) {
        return null;
    }
    global List<SObject> resolveRecords(Set<Id> recordIds, Set<String> extraFields) {
        return null;
    }
}
