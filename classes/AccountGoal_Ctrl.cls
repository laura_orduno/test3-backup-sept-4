global class AccountGoal_Ctrl {

    public ESP__c esp {get; private set;}
    public String espId {get; private set;}
    
    public Goal__c goal {get; set;}
    public String goalId {set; get;}
    
    public Boolean showBtnAddObjective {get; private set;}
    
    
    public String objectiveId {set; get;}
    public String taskId {set;get;}

    public Objectives__c newObjective {set; get;}
    public String newObjGoalId {set; get;}
    
    public Task newTask {set;get;}
    public String newTaskObjectiveId {set; get;}


    private List<Objectives__c> objectivesList ;
    private List<Task> tasksList;
    private List<ObjectiveAndItsTasks> objectivesAndTasksList;

    public String goalSavedMsg {get ;private set;}

    public String sortBy {get;set;}
    public String sortDirection {get; set;}
    public String sortForObjectiveId {get;set;}

    public Integer counter {get ; private set;}

    public String emptyString {get {return ' ';} }

    public AccountGoal_Ctrl() {


    }

    public void init(){
        if (sortBy == null){
            sortBy = 'Id';
        }
        
        if (sortDirection == null){
            sortDirection = 'asc';
        }
       
       //System.debug('sortBy ' + sortBy + ' sortDirection ' + sortDirection);
        counter = 0;

        espId =  ApexPages.currentPage().getParameters().get('espId');
        newTask = new Task();
        newObjective = new Objectives__c();
        goalSavedMsg = 'Saved';     
        List<ESP__c> currentESP = [Select Id,Name
                                   From ESP__c
                                   Where Id = :espId LIMIT 1]; //because one ESP has one goal exactly
        if (currentESP !=null && currentESP.size() >0){
            esp = currentESP[0];
            

            //goalId = 'a3PM00000005nT1MAI';

            goal = new Goal__c( ESP__c = espId );
            List<Goal__c > goals = [SELECT Id,Name,Account_Goal__c 
                                    FROM Goal__c 
                                    WHERE ESP__c =: espId LIMIT 1];
            
            if (goals != null && goals.size()>0){
                goal = goals[0];
                goalId = goal.Id; 
                showBtnAddObjective = true;              
            }
            else{
                goalId = '000000000000000000'; //New goal
                showBtnAddObjective = false;
            }

            //getObjectivesAndTasks();

            getObjectivesAndTasks();
        
        }
        else{
            return;
        }

    }

    /*public List<Objectives__c> getObjectivesAndTasks(){
        //if (objectivesList == null){
            //--method 1
            String selectStmt = ' SELECT Objectives__c.Name,Id, Goal__c,Objective__c, Objective_Strategy__c,Type_of_Objective__c,Unit_or_Resource__c,' +
                               '    (SELECT Id,WhatId,Status,Subject,Type,CallType,ActivityDate,Description,OwnerId,Owner.Name,Priority,Action_Item_custom__c, Objective_Action_Resource__c From ' + 
                                    ' Tasks Order By ' + sortBy + ' ' + sortDirection + ')' +
                               ' FROM Objectives__C ' +
                               ' Where Goal__c =:goalId ' +
                               ' Order By Id CreatedDate';
            objectivesList = Database.query(selectStmt);

            //--method 2
            /*objectivesList = [SELECT Objectives__c.Name,Id, Goal__c,Objective__c, Objective_Strategy__c,Type_of_Objective__c,Unit_or_Resource__c,
                    (SELECT Id,WhatId,Status,Subject,Type,CallType,ActivityDate,Description,OwnerId,Priority, Objective_Action_Resource__c From Tasks Order By Id) 
                  FROM Objectives__C
                  Where Goal__c =:goalId
                  Order BY Id];*/



        //}

    
     /*   return objectivesList;
    }*/

    public List<ObjectiveAndItsTasks> getObjectivesAndTasks(){
        Map<Id, Objectives__c> objsMap = new Map<Id, Objectives__c>( 
                                   [SELECT Name, Id, Goal__c,Objective__c, Objective_Strategy_Values__c, Objective_Strategy__c,Type_of_Objective__c,Unit_or_Resource__c, CreatedDate,Show_Objective_in_PPT__c
                                   FROM Objectives__C
                                   Where Goal__c =:goalId ]);

        List<Task> tasks = new List<Task> ([SELECT Id,WhatId,Status,Subject,Type,CallType,ActivityDate,Description,OwnerId,Owner.Name,Priority, Objective_Action_Resource__c,Action_Item_custom__c 
                                                    ,Senior_Leadership_Ask__c,Show_Task_in_PPT__c
                                                     From Task 
                                                     Where WhatId IN :objsMap.keySet() Order By ID]);

        Map<String, List<Task>> mapObjToTasks = new Map<String, List<Task>>();
        for(Task t:tasks){
            if (!mapObjToTasks.containsKey(t.WhatId)){ //if key does not exist, initialize it
                mapObjToTasks.put(t.WhatId, new List<Task>());
            }
            mapObjToTasks.get(t.WhatId).add(t);
        }

        List<ObjectiveAndItsTasks> otList = new List<ObjectiveAndItsTasks>();
        ObjectiveAndItsTasks ot ;
        for(Objectives__c o:objsMap.values()){

            if (mapObjToTasks.get(o.Id) != null){
                List<Task> tempLst = mapObjToTasks.get(o.Id);
                //TODO: Sort the list here
                List<TaskSorter> taskSortedList = new List<TaskSorter>();
                for(Task t:tempLst){
                    taskSortedList.add(new TaskSorter(t,sortBy,sortDirection));                    
                }
                taskSortedList.sort();

                ot = new ObjectiveAndItsTasks(o,taskSortedList,true);
            }else{
                ot = new ObjectiveAndItsTasks(o,new List<TaskSorter>(),false);
            }

            otList.add(ot);
        }
       // System.debug('sortBy ' + sortBy);
       // System.debug('otList ' + otList);
        objectivesAndTasksList = otList;
        objectivesAndTasksList.sort();
        
        return objectivesAndTasksList;
    }

    public class ObjectiveAndItsTasks implements Comparable{
        public Objectives__c obj {get; private set;}
        public List<TaskSorter> tasks {get; private set;}
        public Boolean hasTasks {get; private set;}

        public ObjectiveAndItsTasks(Objectives__c o,List<TaskSorter> tList,Boolean hasTasks){
            obj = o;
            tasks = tList;
            this.hasTasks = hasTasks;
        }

        public Integer compareTo( Object compareTo ) {
            try{
                ObjectiveAndItsTasks that = (ObjectiveAndItsTasks) compareTo;
                return (this.obj.CreatedDate.getTime() - that.obj.CreatedDate.getTime()).intValue();
            } catch( Exception e ) {
                return -1;
            }
        }
    }

    /************* Account Goal *****************/
    public PageReference saveGoal() {
        if (goalId == '000000000000000000'){
            goalSavedMsg = 'A new Goal is created. Now you can add objectives to this goal';
        }
        else{
            goalSavedMsg = 'Goal is Saved';
        }

        upsert goal;
        goalId = goal.Id;
        showBtnAddObjective = true;
        PageReference pageRef = new PageReference(ApexPages.currentPage().getUrl());
        //pageRef.setRedirect(true);
        //return pageRef;
        counter = counter + 1;
        return null;

    }

    public PageReference cancelGoal(){
        espId =  ApexPages.currentPage().getParameters().get('espId');
        String url = '/' + espId;
        PageReference pageRef = new PageReference(url);
        pageRef.setRedirect(true);
        return pageRef;
    }

    public PageReference deleteGoal() {
        espId =  ApexPages.currentPage().getParameters().get('espId');
        String url = '/' + espId;
        
        List<Goal__c > goalsToDelete = [SELECT Id
                                        FROM Goal__c 
                                        WHERE ESP__c =: espId LIMIT 1];
        List<Id> goalIds = new List<Id>();
        for(Goal__c g:goalsToDelete){
            goalIds.add(g.Id);
        }

        List<Objectives__c> objectivesToDelete = [Select Id
                                                 From Objectives__c
                                                 Where Goal__c in:goalIds];
        List<Id> objectiveIds = new List<Id>();
        for(Objectives__c o:objectivesToDelete){
            objectiveIds.add(o.Id);
        }
        List<Task> tasksToDelete = [Select Id
                                    From Task
                                    Where WhatId in :objectiveIds];
        
        if (tasksToDelete !=null && tasksToDelete.size()>0){
            Delete tasksToDelete;
        }

        if (objectivesToDelete !=null && objectivesToDelete.size()>0){
            Delete objectivesToDelete;
        }

        if (goalsToDelete !=null && goalsToDelete.size()>0){
            Delete goalsToDelete;
        }

        PageReference pageRef = new PageReference(url);
        pageRef.setRedirect(true);
        return pageRef;
    }
    /*********************************************/

    /***** Objectives ****************************/
    public void saveObjective(){
        //System.debug('save objectiveId: ' + objectiveId);
        List<Objectives__c> objsToUpdate = new List<Objectives__c>();       
        if (objectiveId !=null && objectivesAndTasksList !=null ){            
            for(ObjectiveAndItsTasks ot:objectivesAndTasksList){
                if ( String.valueOf( ot.obj.Id ) == objectiveId){
                    objsToUpdate.add( ot.obj);
                }            
            }
            if (objsToUpdate.size()>0) 
            {
                Update objsToUpdate;
            }
        }
        objectiveId = null;
    }

    public void deleteObjective(){
        //System.debug('del objectiveId: ' + objectiveId);
        List<Objectives__c> objsToDelete = new List<Objectives__c>();   
        List<Id> objIds = new List<Id>();
        if (objectiveId !=null){
            for(ObjectiveAndItsTasks ot:objectivesAndTasksList){
                if ( String.valueOf( ot.obj.Id  ) == objectiveId){
                    objsToDelete.add(ot.obj);
                    objIds.add(ot.obj.Id);
                }
            }

            List<Task> t = [Select Id From Task Where WhatId in :objIds];
            if (t !=null && t.size()>0){ //Delete the tasks associated with the objectives first
                Delete t;
            }


            if (objsToDelete.size()>0){
                Delete objsToDelete;
            }
        }
        objectiveId = null;     
    }

    public void addNewObjective(){
        newObjective.Goal__c = newObjGoalId;
        Insert newObjective;
        newObjective = new Objectives__c(); 
    }
    /*******************************************/

    /*************** Task *********************/
    public void saveTask(){
        List<Task> tasksToUpdate = new List<Task>();
        if (taskId !=null){
            tasksList = new List<Task>();

            //Get all the Tasks related to the objectives
            for(ObjectiveAndItsTasks ot:objectivesAndTasksList){
                for(TaskSorter ts:ot.tasks){
                   tasksList.add(ts.tsk);
                }
            }

            //Check if this task is in the list
            for(Task t:tasksList){
                if ( String.valueOf( t.Id ) == taskId){
                    tasksToUpdate.add(t);
                }
            }

            if (tasksToUpdate.size()>0){
                Update tasksToUpdate;
            }

        }
        taskId = null;
    }

    public void deleteTask(){
        List<Task> tasksToDelete = new List<Task>();
        if (taskId !=null){
            tasksList = new List<Task>();

            //Get all the Tasks related to the objectives
            for(ObjectiveAndItsTasks ot:objectivesAndTasksList){
                for(TaskSorter ts:ot.tasks){
                   tasksList.add(ts.tsk);
                }
            }

            //Check if this task is in the list
            for(Task t:tasksList){
                if ( String.valueOf( t.Id ) == taskId){
                    tasksToDelete.add(t);
                }
            }
            if (tasksToDelete.size()>0){
                Delete tasksToDelete;
            }

        }
        taskId = null;
    }


    public void addNewTask(){       
        newTask.WhatId = newTaskObjectiveId;
        if (newTask.OwnerId ==null || newTask.Owner.Name == null || newTask.Owner.Name.trim().length() ==0){
            /*System.debug('Owner is required');
            newTask.OwnerId.addError('Owner is required');
            newTask = new Task();
            return;*/
        }
        Insert newTask;
        newTask = new Task();
    }
    /*******************************/

    global class TaskSorter implements Comparable{

        public Task tsk {get; private set;}
        public String sortBy {get; private set;}
        public String sortDirection {get; private set;}

        public TaskSorter(Task tsk,String sortCol,String sortDir){            
            this.tsk = tsk;
            sortBy = sortCol;
            this.sortDirection = sortDir;
        }
        global Integer compareTo(Object compareTo) {
            TaskSorter sortIt = (TaskSorter)compareTo;
            Integer returnValue = 0;

            if (sortBy == 'Id' || sortBy == null){
                if (String.valueOf(this.tsk.Id).toLowerCase() > String.valueOf(sortIt.tsk.Id).toLowerCase()){
                    returnValue = 1;
                }else if (String.valueOf(this.tsk.Id).toLowerCase() < String.valueOf(sortIt.tsk.Id).toLowerCase()){
                    returnValue = -1;
                }
            }
            else if (sortBy == 'Status'){
                if (this.tsk.Status.toLowerCase() > sortIt.tsk.Status.toLowerCase()){
                    returnValue = 1;
                }else if (this.tsk.Status.toLowerCase() < sortIt.tsk.Status.toLowerCase()){
                    returnValue = -1;
                }
            }
            else if (sortBy == 'OwnerName'){
                if (this.tsk.Owner.Name == null && sortIt.tsk.Owner.Name == null){
                    return 0;
                }

                if (this.tsk.Owner.Name !=null && sortIt.tsk.Owner.Name ==null){
                    return 1;
                }
                if (this.tsk.Owner.Name == null && sortIt.tsk.Owner.Name !=null){
                    return -1;
                }

                if (this.tsk.Owner.Name.toLowerCase() > sortIt.tsk.Owner.Name.toLowerCase()){
                    returnValue = 1;
                }else if (this.tsk.Owner.Name.toLowerCase() < sortIt.tsk.Owner.Name.toLowerCase()){
                    returnValue = -1;
                }
            } 
            else if (sortBy == 'Date'){
                if (this.tsk.ActivityDate > sortIt.tsk.ActivityDate){
                    returnValue = 1;
                }else if (this.tsk.ActivityDate < sortIt.tsk.ActivityDate){
                    returnValue = -1;
                }
            }             
            
            if (sortDirection == 'desc'){
                returnValue = returnValue * -1;
            }
            return returnValue;           
        }
    }

}