/*******************************************************************************
     * Project      : OCOM
     * Helper       : OrderItem_PAC_Address_Controller
     * Test Class   : OrderItem_PAC_Address_Controller_Test
     * Description  : Test class for OrderItem_PAC_Address_Controller(Apex class written by Arvind to Update OrderItem Addresses.)
     * Author       : Arvind Yadav
     * Date         : February-10-2016
     *
     * Modification Log:
     * ------------------------------------------------------------------------------
     * Ver      Date                Modified By           Description
     * ------------------------------------------------------------------------------
     * 1.0     
     
     *******************************************************************************/
@isTest
private class OrderItem_PAC_Address_Controller_Test {

    static testMethod void OrderItemUnitTest() {
        // TO DO: implement unit test
       Test.StartTest();
        Opportunity Opp= new Opportunity(Name='Test Opp',StageName='Discovery',CloseDate=System.today().addDays(5));
        insert Opp; 
        
        Quote Qt= new Quote(Name='Test Quote11',opportunityid=opp.id);
        insert Qt;

        Account AcctRCID = new Account(Name='Test Account', RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId());
        insert AcctRCID;
        
        Account Acct = new Account(Name='Test Account', ParentId=AcctRCID.Id);
        insert Acct;
        
        Id BussRecordTypeId= Schema.SObjectType.Account.getRecordTypeInfosByName().get('Billing').getRecordTypeId();
        Account AcctBusiness = new Account(ParentId=AcctRCID.Id,Name='Test BusineAccount', Ban__c='Ban 1', Can__c= 'Can 1', Ban_Can__c='BanCan1234', Billingstreet='Street1', RecordTypeId=BussRecordTypeId, Billing_Account_Active_Indicator__c='Y');
        insert AcctBusiness ;

        Contract Ct = new Contract(AccountId = Acct.Id, Status = 'Draft',StartDate = Date.Today(), ContractTerm = 12);
        Insert Ct;
        
        Id standardPriceBookId = Test.getStandardPriceBookId();
        
        Order Od = new Order(Name = 'ORD-000001', PriceBook2Id =standardPriceBookId  , AccountId = Acct.Id, EffectiveDate = Date.Today(), ContractId = Ct.Id, Status = 'Draft', vlocity_cmt__QuoteId__c=Qt.Id);
        Insert Od;
        
        Product2 Pd = new Product2(Name='Porduct 1',isActive=true);
        insert Pd;

       
        PricebookEntry Pbe = new PricebookEntry(Pricebook2Id=standardPriceBookId, Product2Id=pd.Id, UnitPrice=99, isActive=true);
        insert Pbe;

        OrderItem ordPd = new OrderItem(PriceBookEntryId=Pbe.Id, OrderId=Od.Id, Quantity=1, UnitPrice=99);
        insert ordPd;
        
        
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(Od);
        ApexPages.currentPage().getParameters().put('contextId',ordPd.id);
        ApexPages.currentPage().getParameters().put('selectedAccId',AcctBusiness.id);
       
        
       OrderItem_PAC_Address_Controller OrderItemController = new OrderItem_PAC_Address_Controller(sc);
       OrderItemController.selectedAccId = AcctBusiness.id;
       OrderItemController.SaveShippingOli();
       OrderItemController.SaveBillingOli();
       OrderItemController.getFormTag();
       OrderItemController.getTextBox(); 
       OrderItemController.Search();
       OrderItemController.SaveBillingOlionSelect();
        
            
       Test.StopTest();
    }
}