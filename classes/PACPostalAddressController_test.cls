/* Developed By: Sandip Chaudhari (IBM)
*  Created Date: 02-Nov-2015
* This is a test class to provide coverage to PACPostalAddressComponentController
*/

@isTest(SeeAllData=true)
private class PACPostalAddressController_test {

    static testMethod void runPositiveTestCases() {
        BAC_TQ_Picklist_Values__c bacentry = new BAC_TQ_Picklist_Values__c(name = 'value1',
                                                                           Related_Domain__c='01',Code__c='01',isDomain__c =true,isResponsilbeGroup__c =true,
                                                                           isSubDomain__c=true,Value__c='value');
        insert bacentry;
        test.startTest();
        PACPostalAddressController obj =  new PACPostalAddressController();
        obj.useAsAttri = true;
        obj.idx = null;
        //obj.eventId = null;
        obj.Init = null;
        obj.config = null;
        obj.isUsedInVFInd = false;
        
        obj.pacSAConfigDetails=null;
        obj.onSelectElementId = 'chkId';
        obj.onCheckElementId = 'tstData';
        PACPostalAddressController.apiKeyErrorMessage = 'Message';
        
        AddressData saData = new AddressData();
        saData.addressLine1 = 'PO Box 226';
        saData.city = 'Behchoko';
        saData.country = 'CAN';
        saData.postalCode = 'X0E 0Y0';
        saData.province = 'NT';

        saData.addressLine2 = '';
        saData.addressLine3 = '';
        saData.organization = '';
        saData.domesticId = '';
        saData.language = '';
        saData.languageAlternatives = '';
        saData.department = '';
        saData.company = '';
        saData.subBuilding = '';
        saData.buildingNumber = '';
        saData.buildingName = '';
        saData.secondaryStreet = '';
        saData.street = '';
        saData.block = '';
        saData.neighbourhood = '';
        saData.district = '';
        saData.addressLine4 = '';
        saData.addressLine5 = '';
        saData.adminAreaName = '';
        saData.adminAreaCode = '';
        saData.provinceName = '';
        saData.provinceCode = '';
        saData.countryIso2 = '';
        saData.countryIso3 = '';
        saData.countryIsoNumber = '';
        saData.sortingNumber1 = '';
        saData.sortingNumber2 = '';
        saData.barcode = '';
        saData.pOBoxNumber = '';
        saData.PAlabel = '';
        saData.PAType = '';
        saData.PADataLevel = '';

        saData.isSearchStringRequired = false;
        saData.isAddressLine1Required = false;
        saData.isAddressLine2Required = false;
        saData.isAddressLine3Required = false;
        saData.isCityRequired = false;
        saData.isStateRequired = false;

        saData.isZipRequired = false;
        saData.isPostalCodeRequired = false;
        saData.isProvinceRequired = false;
        saData.isCountryRequired = false;
        saData.isOrganizationRequired = false;
        saData.isDomesticIdRequired  = false;
        saData.isSubBuildingRequired = false;
        saData.isLanguageRequired  = false;
        saData.isLanguageAlternativesRequired  = false;
        saData.isDepartmentRequired  = false;
        saData.isCompanyRequired  = false;

        saData.isSubBuildingRequired = false;
        saData.isBuildingNumberRequired = false;
        saData.isBuildingNameRequired = false;
        saData.isSecondaryStreetRequired = false;
        saData.isStreetRequired = false;
        saData.isBlockRequired = false;
        saData.isNeighbourhoodRequired = false;
        saData.isDistrictRequired = false;
        saData.isAddressLine4Required = false;
        saData.isAddressLine5Required = false;
        saData.isAdminAreaNameRequired = false;
        saData.isAdminAreaCodeRequired = false;
        saData.isProvinceNameRequired = false;
        saData.isProvinceCodeRequired = false;
        saData.isCountryIso2Required = false;
        saData.isCountryIso3Required = false;
        saData.isCountryIsoNumberRequired = false;
        saData.isSortingNumber1Required = false;
        saData.isSortingNumber2Required = false;
        saData.isBarcodeRequired = false;
        saData.isPOBoxNumberRequired = false;
        saData.isPALabelRequired = false;
        saData.isPATypeRequired = false;
        saData.isPADataLevelRequired = false;
      	saData.domainCode ='012';
        obj.cData = saData;
        Boolean sa = obj.retainBillingAddress;
    //    List<String> subDomainPickList = obj.subDomainPickList;
        String billingDtls = obj.billingDtls;
        Order orderobj = obj.orderobj;
        String ordercontactlanguage = obj.ordercontactlanguage;
        obj.populateTQPicklistValues();
        obj.getbillingLangList();
        List<selectOption> subDomainPickList = obj.getsubDomainPickList();
        
        //string pacConfigName = obj.getInit();
        string pacConfigName1 = obj.getConfig();
        //obj.getCountries();
        //obj.getUSStates();
        //obj.getCanadaProvince();
        obj.syncViewState();
        PACPostalAddressController.getPCKey('billing');
        //List<selectOption> tst = obj.isoCountryCodes;
        //obj.checkAddress();
        //obj.getServiceAddressDetails();
         test.stopTest();
    }
}