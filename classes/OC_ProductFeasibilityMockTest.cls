@isTest
private class OC_ProductFeasibilityMockTest {

    private static Id ORDER_ID;

    private static void setup() {
        Id pricebookId = Test.getStandardPricebookId();

        vlocity_cmt__Catalog__c catalog = new vlocity_cmt__Catalog__c();
        catalog.Name = 'Internet';
        insert catalog;

        //Connectivity Product
        Product2 connProd = new Product2(Name = 'TELUS Connectivity', productcode = 'CONN', isActive = true, ocom_shippable__c='Yes', Sellable__c=true, orderMgmtId__c='887654321');
        connProd.Catalog__c = catalog.Id;
        connProd.VLAdditionalConfigData__C = '{"Offering ID":"FIFA-Connectivity"}';
        insert connProd;   
        PriceBookEntry connPbe = new PriceBookEntry(product2id = connProd.Id, unitprice = 1, isActive = true, pricebook2id = pricebookId);
        insert connPbe;

        //FIFA Internet Product
        Product2 fifaProduct = new Product2(Name = 'Fibre Internet 250', productcode = 'FIFA', isActive = true, ocom_shippable__c='Yes', Sellable__c=true, orderMgmtId__c='987654321');
        fifaProduct.Catalog__c = catalog.Id;
        fifaProduct.VLAdditionalConfigData__C = '{"Offering ID":"FIFA-INET-25"}';
        insert fifaProduct;        
        PriceBookEntry fifaPbe = new PriceBookEntry(product2id = fifaProduct.Id, unitprice = 1, isActive = true, pricebook2id = pricebookId);
        insert fifaPbe;

        //Public Wifi Product
        Product2 wifiProduct = new Product2(Name = 'TELUS Public Wi-Fi', productcode = 'WIFI', isActive = true, ocom_shippable__c='Yes', Sellable__c=true, orderMgmtId__c='787654321');
        wifiProduct.Catalog__c = catalog.Id;
        wifiProduct.VLAdditionalConfigData__C = '{"Offering ID":"FIFA-P-WIFI"}';
        insert wifiProduct;        
        
        PriceBookEntry wifiPbe = new PriceBookEntry(product2id = wifiProduct.Id, unitprice = 1, isActive = true, pricebook2id = pricebookId);
        insert wifiPbe;


        List<Opportunity> opps = OrdrTestDataFactory.createOpprtunities(1);

        //Note that the address must have 'Feasibility' in the street name.
        SMBCare_Address__c address = new SMBCare_Address__c(Account__c=opps[0].AccountId,
                                          address_type__c= OrdrConstants.ADDRESS_TYPE_STANDARD, Suite_Number__c='1',
                                          Street_Number__c='1', Street_Name__c='Feasibility',
                                          Street_Type_Code__c='ST', City__c='Feasibility', Municipality_Name__c='Feasibility', Province__c='BC', 
                                          Postal_Code__c='V5H 3Z7', FMS_Address_ID__c='1919282', Location_Id__c='3243244',
                                          COID__C='CBRV', npa_nxx__c='604432', is_ILEC__c=true, isFIFA__c=true);
        insert address;

        //Insert new Order
        Order order = new Order(
            AccountId=opps[0].AccountId, 
            OpportunityId=opps[0].Id, 
            Service_Address__c=address.Id, 
            EffectiveDate=System.today(), 
            status='Draft', 
            orderMgmtId__c = '123456781', 
            Pricebook2Id=Test.getStandardPricebookId()
        );
        insert order;
        ORDER_ID = order.Id;

        //Insert OLIs
        OrderItem ordrItem = new OrderItem();
        ordrItem.OrderId = order.Id;
        ordrItem.Product_display_name__c = 'Fibre Internet 250';
        ordrItem.Product2 = fifaProduct;
        ordrItem.PricebookEntryId = fifaPbe.Id;
        ordrItem.Quantity = 1;
        ordrItem.UnitPrice = 200.00;
        ordrItem.vlocity_cmt__LineNumber__c='0003';
        ordrItem.orderMgmtId__c='987654321';
        ordrItem.vlocity_cmt__ProvisioningStatus__c=null;
        ordrItem.vlocity_cmt__OneTimeTotal__c = 0.0;
        insert ordrItem;

        OrderItem ordrItem2 = new OrderItem();
        ordrItem2.OrderId = order.Id;
        ordrItem2.Product_display_name__c = 'TELUS Public Wi-Fi';
        ordrItem2.Product2 = wifiProduct;
        ordrItem2.PricebookEntryId = wifiPbe.Id;
        ordrItem2.Quantity = 1;
        ordrItem2.UnitPrice = 10.00;
        ordrItem2.vlocity_cmt__LineNumber__c='0002';
        ordrItem2.orderMgmtId__c='787654321';
        ordrItem2.vlocity_cmt__ProvisioningStatus__c=null;
        ordrItem2.vlocity_cmt__OneTimeTotal__c = 0.0;
        insert ordrItem2;
    }


    //test of mock feasibility service - address 1
    @isTest static void feasibilityTest1() {
        setup();
        Map<String, Object> input = new Map<String, Object>();
        Map<String, Object> output = new Map<String, Object>();
        Map<String, Object> options = new Map<String, Object>();

        input.put('orderId', ORDER_ID);

        test.startTest();

        OC_ProductFeasibilityMock componentFeasibility = new OC_ProductFeasibilityMock();

        Boolean returnVal = componentFeasibility.invokeMethod('checkProductFeasibility', input, output, options);
        System.assert(returnVal);
        
        Boolean feasibilityResult = Boolean.valueOf(output.get('feasibility'));
        System.assert(feasibilityResult);

        test.stopTest();
    }

    //test of mock feasibility service - address 2
    @isTest static void feasibilityTest2() {
        setup();
        Map<String, Object> input = new Map<String, Object>();
        Map<String, Object> output = new Map<String, Object>();
        Map<String, Object> options = new Map<String, Object>();

        List<Opportunity> opps2 = OrdrTestDataFactory.createOpprtunities(1);

        //Note that the address must have 'Feasibility' in the street name.
        SMBCare_Address__c address2 = new SMBCare_Address__c(Account__c=opps2[0].AccountId,
                                          address_type__c= OrdrConstants.ADDRESS_TYPE_STANDARD, Suite_Number__c='2',
                                          Street_Number__c='2', Street_Name__c='Feasibility',
                                          Street_Type_Code__c='ST', City__c='Feasibility', Municipality_Name__c='Feasibility', Province__c='BC', 
                                          Postal_Code__c='V5H 3Z7', FMS_Address_ID__c='1919282', Location_Id__c='3243244',
                                          COID__C='CBRV', npa_nxx__c='604432', is_ILEC__c=true, isFIFA__c=true);
        insert address2;

        Order order = [SELECT AccountId, OpportunityId, Service_Address__c FROM Order WHERE Id =:ORDER_ID LIMIT 1];

        order.AccountId=opps2[0].AccountId;
        order.OpportunityId=opps2[0].Id;
        order.Service_Address__c=address2.Id; 
        update order;
        input.put('orderId', ORDER_ID);

        test.startTest();

        OC_ProductFeasibilityMock componentFeasibility = new OC_ProductFeasibilityMock();
        Boolean returnVal = componentFeasibility.invokeMethod('checkProductFeasibility', input, output, options);
        System.assert(returnVal);
        
        Boolean feasibilityResult = Boolean.valueOf(output.get('feasibility'));
        System.assert(feasibilityResult);

        test.stopTest();
    }

    //test of mock feasibility service - address 3
    @isTest static void feasibilityTest3() {
        setup();
        Map<String, Object> input = new Map<String, Object>();
        Map<String, Object> output = new Map<String, Object>();
        Map<String, Object> options = new Map<String, Object>();

        List<Opportunity> opps3 = OrdrTestDataFactory.createOpprtunities(1);

        //Note that the address must have 'Feasibility' in the street name.
        SMBCare_Address__c address3 = new SMBCare_Address__c(Account__c=opps3[0].AccountId,
                                          address_type__c= OrdrConstants.ADDRESS_TYPE_STANDARD, Suite_Number__c='3',
                                          Street_Number__c='3', Street_Name__c='Feasibility',
                                          Street_Type_Code__c='ST', City__c='Feasibility', Municipality_Name__c='Feasibility', Province__c='BC', 
                                          Postal_Code__c='V5H 3Z7', FMS_Address_ID__c='1919282', Location_Id__c='3243244',
                                          COID__C='CBRV', npa_nxx__c='604432', is_ILEC__c=true, isFIFA__c=true);
        insert address3;

        Order order = [SELECT AccountId, OpportunityId, Service_Address__c FROM Order WHERE Id =:ORDER_ID LIMIT 1];

        order.AccountId=opps3[0].AccountId;
        order.OpportunityId=opps3[0].Id;
        order.Service_Address__c=address3.Id; 
        update order;
        input.put('orderId', ORDER_ID);

        test.startTest();

        OC_ProductFeasibilityMock componentFeasibility = new OC_ProductFeasibilityMock();
        Boolean returnVal = componentFeasibility.invokeMethod('checkProductFeasibility', input, output, options);
        System.assert(returnVal);
        
        Boolean feasibilityResult = Boolean.valueOf(output.get('feasibility'));
        System.assert(feasibilityResult);

        test.stopTest();
    }

    //test of mock feasibility service - address 4
    @isTest static void feasibilityTest4() {
        setup();
        Map<String, Object> input = new Map<String, Object>();
        Map<String, Object> output = new Map<String, Object>();
        Map<String, Object> options = new Map<String, Object>();

        List<Opportunity> opps4 = OrdrTestDataFactory.createOpprtunities(1);

        //Note that the address must have 'Feasibility' in the street name.
        SMBCare_Address__c address4 = new SMBCare_Address__c(Account__c=opps4[0].AccountId,
                                          address_type__c= OrdrConstants.ADDRESS_TYPE_STANDARD, Suite_Number__c='4',
                                          Street_Number__c='4', Street_Name__c='Feasibility',
                                          Street_Type_Code__c='ST', City__c='Feasibility', Municipality_Name__c='Feasibility', Province__c='BC', 
                                          Postal_Code__c='V5H 3Z7', FMS_Address_ID__c='1919282', Location_Id__c='3243244',
                                          COID__C='CBRV', npa_nxx__c='604432', is_ILEC__c=true, isFIFA__c=true);
        insert address4;

        Order order = [SELECT AccountId, OpportunityId, Service_Address__c FROM Order WHERE Id =:ORDER_ID LIMIT 1];

        order.AccountId=opps4[0].AccountId;
        order.OpportunityId=opps4[0].Id;
        order.Service_Address__c=address4.Id; 
        update order;
        input.put('orderId', ORDER_ID);

        test.startTest();

        OC_ProductFeasibilityMock componentFeasibility = new OC_ProductFeasibilityMock();
        Boolean returnVal = componentFeasibility.invokeMethod('checkProductFeasibility', input, output, options);
        System.assert(returnVal);
        
        Boolean feasibilityResult = Boolean.valueOf(output.get('feasibility'));
        System.assert(feasibilityResult);

        test.stopTest();
    }

    //test of mock feasibility service - address 5
    @isTest static void feasibilityTest5() {
        setup();
        Map<String, Object> input = new Map<String, Object>();
        Map<String, Object> output = new Map<String, Object>();
        Map<String, Object> options = new Map<String, Object>();

        List<Opportunity> opps5 = OrdrTestDataFactory.createOpprtunities(1);

        //Note that the address must have 'Feasibility' in the street name.
        SMBCare_Address__c address5 = new SMBCare_Address__c(Account__c=opps5[0].AccountId,
                                          address_type__c= OrdrConstants.ADDRESS_TYPE_STANDARD, Suite_Number__c='5',
                                          Street_Number__c='5', Street_Name__c='Feasibility',
                                          Street_Type_Code__c='ST', City__c='Feasibility', Municipality_Name__c='Feasibility', Province__c='BC', 
                                          Postal_Code__c='V5H 3Z7', FMS_Address_ID__c='1919282', Location_Id__c='3243244',
                                          COID__C='CBRV', npa_nxx__c='604432', is_ILEC__c=true, isFIFA__c=true);
        insert address5;

        Order order = [SELECT AccountId, OpportunityId, Service_Address__c FROM Order WHERE Id =:ORDER_ID LIMIT 1];

        order.AccountId=opps5[0].AccountId;
        order.OpportunityId=opps5[0].Id;
        order.Service_Address__c=address5.Id; 
        update order;
        input.put('orderId', ORDER_ID);

        test.startTest();

        OC_ProductFeasibilityMock componentFeasibility = new OC_ProductFeasibilityMock();
        Boolean returnVal = componentFeasibility.invokeMethod('checkProductFeasibility', input, output, options);
        System.assert(returnVal);
        
        Boolean feasibilityResult = Boolean.valueOf(output.get('feasibility'));
        System.assert(!feasibilityResult);  //expecting this result to be false

        test.stopTest();
    }

    //test of mock feasibility service - address 6
    @isTest static void feasibilityTest6() {
        setup();
        Map<String, Object> input = new Map<String, Object>();
        Map<String, Object> output = new Map<String, Object>();
        Map<String, Object> options = new Map<String, Object>();

        List<Opportunity> opps6 = OrdrTestDataFactory.createOpprtunities(1);

        //Note that the address must have 'Feasibility' in the street name.
        SMBCare_Address__c address6 = new SMBCare_Address__c(Account__c=opps6[0].AccountId,
                                          address_type__c= OrdrConstants.ADDRESS_TYPE_STANDARD, Suite_Number__c='6',
                                          Street_Number__c='6', Street_Name__c='Feasibility',
                                          Street_Type_Code__c='ST', City__c='Feasibility', Municipality_Name__c='Feasibility', Province__c='BC', 
                                          Postal_Code__c='V5H 3Z7', FMS_Address_ID__c='1919282', Location_Id__c='3243244',
                                          COID__C='CBRV', npa_nxx__c='604432', is_ILEC__c=true, isFIFA__c=true);
        insert address6;

        Order order = [SELECT AccountId, OpportunityId, Service_Address__c FROM Order WHERE Id =:ORDER_ID LIMIT 1];

        order.AccountId=opps6[0].AccountId;
        order.OpportunityId=opps6[0].Id;
        order.Service_Address__c=address6.Id; 
        update order;
        input.put('orderId', ORDER_ID);

        test.startTest();

        OC_ProductFeasibilityMock componentFeasibility = new OC_ProductFeasibilityMock();
        Boolean returnVal = componentFeasibility.invokeMethod('checkProductFeasibility', input, output, options);
        System.assert(returnVal);
        
        Boolean feasibilityResult = Boolean.valueOf(output.get('feasibility'));
        System.assert(!feasibilityResult);  //expecting this result to be false

        test.stopTest();
    }

    //test of mock feasibility service - address 7
    @isTest static void feasibilityTest7() {
        setup();
        Map<String, Object> input = new Map<String, Object>();
        Map<String, Object> output = new Map<String, Object>();
        Map<String, Object> options = new Map<String, Object>();

        List<Opportunity> opps7 = OrdrTestDataFactory.createOpprtunities(1);

        //Note that the address must have 'Feasibility' in the street name.
        SMBCare_Address__c address7 = new SMBCare_Address__c(Account__c=opps7[0].AccountId,
                                          address_type__c= OrdrConstants.ADDRESS_TYPE_STANDARD, Suite_Number__c='7',
                                          Street_Number__c='7', Street_Name__c='Feasibility',
                                          Street_Type_Code__c='ST', City__c='Feasibility', Municipality_Name__c='Feasibility', Province__c='BC', 
                                          Postal_Code__c='V5H 3Z7', FMS_Address_ID__c='1919282', Location_Id__c='3243244',
                                          COID__C='CBRV', npa_nxx__c='604432', is_ILEC__c=true, isFIFA__c=true);
        insert address7;

        Order order = [SELECT AccountId, OpportunityId, Service_Address__c FROM Order WHERE Id =:ORDER_ID LIMIT 1];

        order.AccountId=opps7[0].AccountId;
        order.OpportunityId=opps7[0].Id;
        order.Service_Address__c=address7.Id; 
        update order;
        input.put('orderId', ORDER_ID);

        test.startTest();

        OC_ProductFeasibilityMock componentFeasibility = new OC_ProductFeasibilityMock();
        Boolean returnVal = componentFeasibility.invokeMethod('checkProductFeasibility', input, output, options);
        System.assert(returnVal);
        
        Boolean feasibilityResult = Boolean.valueOf(output.get('feasibility'));
        System.assert(!feasibilityResult);  //expecting this result to be false

        test.stopTest();
    }
}