public class GenericFilterListViewCtrl extends GenericListViewCompCtrl {
 
    public static final Integer pageSize = 15;
    
    // Parameters Id
    public String sourceId {get;set;} //Id of the source object whcih master side Example Deal has many Rate Plans , rate Plan has many addons
    
    // Parameters Object Names
    public String targetObj {get;set;}
    
    // Parameters FiledNames
    public String linkField {get;set;}
   
    // Parameters Filter
    public String dataFilter {get;set;}

    
    public List<Schema.FieldSetMember> displayFieldset {get;set;} 
    public List<Schema.FieldSetMember> FilterFieldset {get;set;}
    
    public String displayFieldSetName {get;set;}
    
    public String filterFieldsetName {get;set;}
    
    public sObject targetObjInstance {get;set;} 
    
    public GenericFilterListViewCtrl (){
         try{
            init();
        }catch(RequiredParameterException e){
            String errmsg= 'Required parameters sourceId, sfld, tfld, tarobj, junobj, dfsn can not be empty. This is a Developer Error!';
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL,errmsg);
            ApexPages.addMessage(msg);
           
        } 
    }
    
    private void init(){
        displayFieldset = new List<Schema.FieldSetMember>();
        FilterFieldset =  new List<Schema.FieldSetMember>();
        //Required Parameters  
        sourceId = ApexPages.currentPage().getParameters().get('sourceId');
        linkField = ApexPages.currentPage().getParameters().get('lnkfld');
        targetObj = ApexPages.currentPage().getParameters().get('trgobj');
        
        displayFieldSetName =  ApexPages.currentPage().getParameters().get('dfsn');
        if(sourceId==null || targetObj ==null || linkField == null || displayFieldSetName==null){
            String msg = 'sourceId: ' + sourceId +  ' targetObj: '+ targetObj+ ' linkField: ' + linkField +' displayFieldSetName: ' +displayFieldSetName;
            System.debug(LoggingLevel.ERROR, msg);
                throw new RequiredParameterException();
        }
        ObjName = targetObj;
        ColFields =   displayFieldSetName;
       
        //Optional parameters
        filterFieldsetName =  ApexPages.currentPage().getParameters().get('ffsn');
        dataFilter = ApexPages.currentPage().getParameters().get('datafilter');
        
        filterFieldsetName = filterFieldsetName == null?displayFieldSetName:filterFieldsetName;
        targetObjInstance  = (Sobject)Type.forName(targetObj).newInstance();
        displayFieldset = Util.getFieldSetFieldsObjs(targetObj, displayFieldSetName);
        FilterFieldset = Util.getFieldSetFieldsObjs(targetObj, filterFieldsetName);
        if(dataFilter!=null && dataFilter !='' ){
          String[] parts = dataFilter.replaceAll('\'', '').split('='); 
            if(parts.size() > 1) {
            	targetObjInstance.put(parts[0], parts[1]);
            }
        }
        if(filter ==null){
            filter = new List<String>{dataFilter};
        }else{
          filter.add(dataFilter); 
        }
        filter.add(linkField + '=\'' + sourceId + '\'');
    }
    public PageReference doFilter(){
        Set<String> queryFieldsSet = new Set<String>();
        List<String> queryFields = new List<String>();
        List<String>  localFilter = new List<String>();
        localFilter.add(linkField + '=\'' + sourceId + '\'');
        queryFieldsSet.addAll(Util.getFieldSetFields(targetObj, displayFieldSetName));
        queryFieldsSet.addAll(new LIst<string>{'Name', 'Id'});
        queryFields.addAll(queryFieldsSet);
        for(Schema.FieldSetMember f :FilterFieldset){
            Object filterPlanType =(Object) targetObjInstance.get(f.getFieldPath());
            if(filterPlanType != null && 'PICKLIST'.equals(String.valueOf(f.getType()))){
               	localFilter.add(f.getFieldPath() +' =\'' + filterPlanType+ '\'');
            }else if(filterPlanType != null && 'STRING'.equals(String.valueOf(f.getType()))){
                localFilter.add(f.getFieldPath() +' Like \'%' + filterPlanType+ '%\'');
            }else if(filterPlanType!=null ){
                localFilter.add(f.getFieldPath() +' =' + filterPlanType);
            }    
        }
        String query = Util.queryBuilder(targetObj,queryFields ,localFilter);
        paginator = new GenericPagination(query,pageSize);
        filter.clear();
        filter.addAll(localFilter);
	    return null;
    }
   
   public class RequiredParameterException extends Exception {
   }
}