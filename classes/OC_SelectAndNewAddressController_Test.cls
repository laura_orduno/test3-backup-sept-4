/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class OC_SelectAndNewAddressController_Test {
  
  static testMethod void OC_SelectAndNewAddressController_UnitTest() {
        // TO DO: implement unit test
        //Data Setup           
        Id recSerTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        Id recRCIDTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId();        
          
        Account RCIDacc = new Account(name='RCID Account',recordtypeid=recRCIDTypeId);
        Account seracc = new Account(name='Service Account',recordtypeid=recSerTypeId,parentid=RCIDacc.id);          
          
        List<Account> acclist= new List<Account>();          
        acclist.add(RCIDacc);
        acclist.add(seracc );
        insert acclist;
          
        smbcare_address__c address= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N3',FMS_Address_ID__c='987650',account__c=RCIDacc.id); 
        smbcare_address__c address3= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N3',FMS_Address_ID__c='987652',account__c=RCIDacc.id,service_account_Id__c=seracc.id); 
        address3.Block__c = 'Block 1';
        address3.Building_Number__c = 'Building 1';
        address3.Street_Number__c = '25';
        address3.Street_Name__c = ' York Street';
        
        smbcare_address__c address2= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N5',FMS_Address_ID__c='987651',account__c=RCIDacc.id); 
        List<smbcare_address__c> smbCarelist= new List<smbcare_address__c>(); 
        smbCarelist.add(address);
        smbCarelist.add(address3);
        insert smbCarelist;
        
        ServiceAddressData SData = new ServiceAddressData();
        SData.isManualCapture=true;
        SData.fmsId='987652';
        
        Contact Cont = new Contact (FirstName ='FirstName', LastName='LastName', AccountId=seracc.Id, Email='Abc@abc.com');
        insert Cont;
        
        Contract Ct = new Contract(AccountId = RCIDacc.Id, Status = 'Draft',StartDate = Date.Today(), ContractTerm = 12);
        Insert Ct;
        
        Id standardPriceBookId = Test.getStandardPriceBookId();
        
        SMBCare_Address__c smbCareAddr = new SMBCare_Address__c(Account__c=RCIDacc.Id, Postal_Code__c='M1T3N3', Street_Number__c='Street 1', Street_Name__c='Street Name 1', Province__c='ONT', Suite_Number__c='1001', COID__c='COID 1', Building_Number__c='Building 1', Country__c='CAN', FMS_Address_ID__c='FMSID 121');
        insert smbCareAddr;
        
        Opportunity Opp= new Opportunity(Name='Test Opp',StageName='Discovery',CloseDate=System.today().addDays(5));
        insert Opp; 
        
        Quote Qt= new Quote(Name='Test Quote11',opportunityid=opp.id);
        insert Qt;
        
        Order Od = new Order(Name = 'ORD-000001', Shipping_Address__c='2010 100 AV NW EDMONTON AB T5A3M1', Service_Address_Text__c='2010 100 AV NW EDMONTON AB T5A3M1' , FMS_Address_ID__c='FMSID 121', ShippingStreet='2010 100 AV', ShippingCity='EDMONTON', BAN__c='Ban 21' , PriceBook2Id =standardPriceBookId  , AccountId = RCIDacc.Id, EffectiveDate = Date.Today(), ContractId = Ct.Id, Status = 'Draft', vlocity_cmt__QuoteId__c=Qt.Id);
        Od.Service_Address__c = address3.Id;
        Insert Od;
        
        Product2 Pd = new Product2(Name='Porduct 1',isActive=true);
        insert Pd;

       
        PricebookEntry Pbe = new PricebookEntry(Pricebook2Id=standardPriceBookId, Product2Id=pd.Id, UnitPrice=99, isActive=true);
        insert Pbe;

        OrderItem ordPd = new OrderItem(PriceBookEntryId=Pbe.Id, OrderId=Od.Id, Quantity=1, UnitPrice=99);
        insert ordPd;
        
       // Case Cse = new Case(AccountId=seracc.Id, ContactId=Cont.Id, Description='Test Description', Status='New');
    //Insert Cse;
    
    List<Country_Province_Codes__c> cntPrvnList = new List<Country_Province_Codes__c>();
    
    Country_Province_Codes__c CntryProv = new Country_Province_Codes__c(Name='AA', Type__c='American State', is_ILEC__c=false,Description__c='Armed Forces Americas');
    
    Country_Province_Codes__c CntryProv1 = new Country_Province_Codes__c(Name='  AK', Type__c='American State', is_ILEC__c=false,Description__c='Alaska');
    
    cntPrvnList.add(CntryProv);
    cntPrvnList.add(CntryProv1);
    
    Insert cntPrvnList;    
                
        Test.startTest();
        PageReference PageRef = Page.OC_SelectAddress;
        Test.setCurrentPage(PageRef);  
        PageRef.getparameters().put('rcid', RCIDacc.Id);
        PageRef.getparameters().put('addressType', 'MOVE_OUT');
        PageRef.getparameters().put('parentOrderId', Od.Id);
        
        OC_SelectAndNewAddressController ScNewController =  new  OC_SelectAndNewAddressController();
        ScNewController.serviceAccount = seracc;
        
        ScNewController.CreateServiceAccount();
        ScNewController.RedirectToSelectAddressPage();
        ScNewController.RedirectToContactPage();
        ScNewController.RedirectToNewAddressPage();
        //
        OrdrTestDataFactory.createOrderingWSCustomSettings('ProductEligibilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/CMO/OrderMgmt/PerformCustomerOrderFeasibility_v2_0_vs0');
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        //TpFulfillmentCustomerOrderV3.CustomerOrder response = OrdrProductEligibilityWsCallout.performCustomerOrderFeasibility(inputMap);
        //
        ScNewController.SAData = new ServiceAddressData();
        ScNewController.SAData.province='AB';
        ScNewController.SAData.isManualCapture = True;
        ScNewController.processManualAddress();
        ScNewController.moveOutSAData = new ServiceAddressData();
        ScNewController.smbCareAddrId = address.Id;
        ScNewController.dropExceptionCode = '';
        ScNewController.isASFDown = false;
        ScNewController.blnGTACall = false;
        ScNewController.showErrorMsg = false;
        ScNewController.displayPageComp = true;
        OC_SelectAndNewAddressController.isILEC('AB');
        Test.StopTest();   
        
             
    }

}