global class smb_SVOC_OrdersController 
{
    public Account acct {get; set;}
    public transient List<Opportunity> matchedOpportunity {get; set;}
    public transient List<OCOM_QuoteOrderVO> matchedQuoteOrders {get;set;}
    public transient List<Service_Request__c> matchedServReq {get;set;}    
    public String RCID {get;set;}   

    public transient list<smb_orders_trackingtypes_v4.OrderRequest> lstOrders {get;set;}
    public transient list<SMB_WebserviceHelper.orderWrapper> orderList {get;set;}
    
    public String pageInstanceId  {
        get {
            if (pageInstanceId == null) {
                pageInstanceId = smb_GuidUtil.NewGuid();
            }
            return pageInstanceId;
        }
        private set;
    }
    

    public Boolean AllowCreationWithNewQuoteButton {
    
        get { List<Profile> PROFIL = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
               String MyProflieName = PROFIL[0].Name;
                      
            if (acct == null) return false;
            system.debug('-----Profile Name ----------'+PROFIL[0].Name);
            if (acct.RecordType == null) return false;
            
            if (acct.RecordType.DeveloperName == 'RCID' || acct.RecordType.DeveloperName == 'New_Prospect_Account' ) 
            // &&  CONTAINS(MyProflieName, "Vlocity")) //PROFIL[0].Id =='00ee0000000E0mvAAC' || PROFIL[0].Id== '00ee0000000E0ZcAAK'))
            {   if (MyProflieName.contains('Vlocity') || MyProflieName == 'System Admin TEAM' ||  MyProflieName == 'System Vendor')           
               { 
                   if(acct.Account_Status__c == 'Obsolete' || acct.Account_Status__c == 'Terminated'){
                       return false;
                   }
                   else{ return true;}
                }
            }            
            return false;
        }
    }
    
    public Boolean AllowCreationWithNewOrderInterimButton {
        get {
            if (acct == null) return false;
            
            if (acct.RecordType == null) return false;
            
            // Thomas QC-8321/QC-8367 Start
            integer numMembers=0;
            try{
                numMembers=[select count() from groupmember where group.developername='OCOM_Pilot_Users' and userorgroupid=:userinfo.getuserid()];                
            }catch(exception e){
            }
            if (userinfo.isCurrentUserLicensed('vlocity_cmt') && numMembers>0) return false;
            // Thomas QC-8321/QC-8367 End
            
            if (acct.RecordType.DeveloperName == 'RCID' || acct.RecordType.DeveloperName == 'New_Prospect_Account') {
                if(acct.Account_Status__c == 'Obsolete' || acct.Account_Status__c == 'Terminated'){
                    return false;
                }else{
                    return true;
                }
            }            
            return false;
        }
    }

    
     public Boolean AllowCreationWithNewOrderButton {
        get {
            if (acct == null) return false;
            
            if (acct.RecordType == null) return false;
            
            // Thomas QC-8321/QC-8367 Start
            integer numMembers=0;
            try{
                numMembers=[select count() from groupmember where group.developername='OCOM_Pilot_Users' and userorgroupid=:userinfo.getuserid()];                
            }catch(exception e){
            }
            if (userinfo.isCurrentUserLicensed('vlocity_cmt')==false || numMembers<=0) return false;
            // Thomas QC-8321/QC-8367 End
            
            if (acct.RecordType.DeveloperName == 'RCID' || acct.RecordType.DeveloperName == 'New_Prospect_Account') {
                if(acct.Account_Status__c == 'Obsolete' || acct.Account_Status__c == 'Terminated'){
                    return false;
                }else{
                    return true;
                }
            }            
            return false;
        }
    }
    
    public Boolean AllowCreationOfRelatedRecords {
        get {
            if (acct == null) return false;
            
            if (acct.RecordType == null) return false;
            
            if (acct.RecordType.DeveloperName == 'RCID' || acct.RecordType.DeveloperName == 'New_Prospect_Account') {
                if(acct.Account_Status__c == 'Obsolete' || acct.Account_Status__c == 'Terminated'){
                    return false;
                }else{
                    return true;
                }
            }            
            return false;
        }
    }
              
    global transient List<Id> AllAccountIdsInHierarchy {get;set;}
        
        public PageReference init() 
        {           
            Id accountId = ApexPages.currentPage().getParameters().get('id');
            matchedQuoteOrders = new List<OCOM_QuoteOrderVO>();
            acct = getAccount(accountId);
            
            allAccountIdsInHierarchy = smb_AccountUtility.getAccountsInHierarchy(accountId, acct.ParentId);               
            
            // Sandip - 08 June 2016 - Translation Phase II - Added Status__c, Condition_Color__c in query
            matchedOpportunity = [select Id, Name, Owner.Name, Total_Contract_Renewal__c, StageName, CloseDate, CreatedDate, Order_ID__c, Type, 
                                  Requested_Due_Date__c, Condition__c, Service_Address__c, Status__c, Condition_Color__c   
                from Opportunity 
                where 
                (recordtype.name='SMB Care Opportunity' and Order_ID__c != null and accountid =:accountId) or
                (recordtype.name='SMB Care Amend Order Opportunity' and Order_ID__c != null and accountid =:accountId) or
                (recordtype.name='SMB Care Locked Order Opportunity' and Order_ID__c != null and accountid =:accountId) or
                (recordtype.name='SRS Order Request' and Order_ID__c != null and accountid =:accountId)
                order by Order_ID__c DESC limit 1000];
                //system.debug('matchedOppt: '+matchedOpportunity);

                 List<Order> matchedOrders = [select parentid__r.ordernumber,id, status, name, owner.name, OrderNumber,Sub_Order_Number__c,recordtype.developername, 
                                              order_number__c, CreatedDate, Type, Service_Address_Text__c, RequestedDate__c,Service_Address_Full_Name__c
                                              from Order where AccountId=:accountId 
                                              order by CreatedDate DESC ];
                                              
            List<Quote> quoteList = [Select q.id, q.status, q.QuoteNumber, q.Name, q.CreatedDate, q.vlocity_cmt__Type__c, Service_Address_Text__c, q.Opportunity.owner.name
                                 From Quote q 
                                 where q.OpportunityId in (select o.id 
                                                           from Opportunity o 
                                                           where o.Record_Type_Name__c = 'Vlocity_User' and o.accountid =:accountId)
                                order by q.CreatedDate DESC ];
            
            if(matchedOpportunity != null && matchedOpportunity.size() > 0)
            {
                for(Opportunity opp :matchedOpportunity)
                {
                    OCOM_QuoteOrderVO svOrder = new OCOM_QuoteOrderVO();
                    svOrder.Condition = opp.Condition__c;
                    svOrder.OrderId = opp.Order_ID__c; 
                    svOrder.Id = opp.Id;
                    svOrder.OppDes = opp.Name;
                    svOrder.Name = opp.Name;
                    svOrder.OrderType = opp.Type;
                    svOrder.Status = opp.StageName;
                    svOrder.CreatedDate = opp.CreatedDate;
                    
                    // Sandip - 08 June 2016 - Translation Phase II
                    svOrder.strCreatedDate = SMB_WebserviceHelper.getDateFormat(opp.CreatedDate);
                    //end
                    
                    svOrder.OwnerFullName = opp.Owner.Name;
                    svOrder.RequestedDueDate = opp.Requested_Due_Date__c;
                    
                    // Sandip - 08 June 2016 - Translation Phase II
                    svOrder.strRequestedDueDate = SMB_WebserviceHelper.getDateFormat(opp.Requested_Due_Date__c);
                    //End
                    
                    svOrder.ServiceAddress = opp.Service_Address__c;
                    
                    matchedQuoteOrders.add(svOrder);   
                    
                }
            }
            //Quotes lstquotes
        
            if(quoteList != null && quoteList.size() > 0)
            {
                for(quote qte :quoteList)
                {
                    OCOM_QuoteOrderVO svOrder = new OCOM_QuoteOrderVO();
                    svOrder.Condition = '';
                    svOrder.OrderId = qte.QuoteNumber; 
                    svOrder.Id = qte.Id;
                    //svOrder.OppDes = 'Order Capture Pilot';
                    //Change done for US1231
                    // svOrder.OppDes = 'Vlocity Order Type';
                    svOrder.Name = 'Quote';
                    svOrder.OrderType = qte.vlocity_cmt__Type__c;
                    svOrder.RecordType = 'vlocity_quote';
                    svOrder.ProductType = '';
                    svOrder.Status = qte.status;
                    svOrder.CreatedDate = qte.CreatedDate;
                    // Sandip - 08 June 2016 - Translation Phase II
                    svOrder.strCreatedDate = SMB_WebserviceHelper.getDateFormat(qte.CreatedDate);
                    //end
                    svOrder.OwnerFullName = qte.Opportunity.Owner.name;
                    //svOrder.RequestedDueDate = '';
                    svOrder.ServiceAddress = qte.Service_Address_Text__c;
                    
                    matchedQuoteOrders.add(svOrder);   
                    
                }
            }
            
            if(matchedOrders != null && matchedOrders.size() > 0)
            {
                Map<String,String> childOrderMastOrderMap=new Map<String,String>();
                Map<String,Integer> masterOrderMastOrderMap=new Map<String,Integer>();
                Integer cnt=0;
                Map<String,String> subOrderAddressMap=new Map<String,String>();
                for(Order odr :matchedOrders){
                    if(String.isNotBlank(odr.recordtype.developername) && 'Customer_Order'.equalsIgnoreCase(odr.recordtype.developername)){
                        //svOrder.OrderId = odr.Sub_Order_Number__c; 
                        if(String.isNotBlank(odr.parentid__r.ordernumber)){
                            if(masterOrderMastOrderMap.keySet().contains(odr.parentid__r.ordernumber)){
                                Integer newCnt=masterOrderMastOrderMap.get(odr.parentid__r.ordernumber);
                                newCnt++;
                                masterOrderMastOrderMap.put(odr.parentid__r.ordernumber,newCnt);
                            } else{
                                cnt++;
                                masterOrderMastOrderMap.put(odr.parentid__r.ordernumber,cnt);
                            }
                            
                            if(String.isNotBlank(odr.Service_Address_Full_Name__c)){
                                subOrderAddressMap.put(odr.parentid__r.ordernumber,odr.Service_Address_Full_Name__c);
                            } else{
                                subOrderAddressMap.put(odr.parentid__r.ordernumber,odr.Service_Address_Text__c);                                   
                            }
                        }
                    }
                    
                }
                
                for(Order odr :matchedOrders){
                    if(String.isNotBlank(odr.recordtype.developername) && 'Customer_Order'.equalsIgnoreCase(odr.recordtype.developername)){
                        if(masterOrderMastOrderMap!=null && masterOrderMastOrderMap.containsKey(odr.parentid__r.ordernumber) 
                           && masterOrderMastOrderMap.get(odr.parentid__r.ordernumber)==1){
                            continue;
                        }
                    }
                    OCOM_QuoteOrderVO svOrder = new OCOM_QuoteOrderVO();
                    svOrder.Condition = '';
                    //Sub_Order_Number__c,recordtype.developername,
                    if(String.isNotBlank(odr.recordtype.developername) && 'Customer_Order'.equalsIgnoreCase(odr.recordtype.developername)){
                        if(String.isNotBlank(odr.Sub_Order_Number__c)){
                          svOrder.OrderId = odr.Sub_Order_Number__c;   
                        } else {
                           svOrder.OrderId = odr.OrderNumber;  
                        }
                        
                    } else {
                    svOrder.OrderId = odr.OrderNumber; 
                    }
                    //svOrder.OppDes = 'Order Capture Pilot';
                     //Change done for US1231
                     svOrder.OppDes = odr.Type;
                    svOrder.Id = odr.Id;
                    svOrder.Name = 'Order';
                    svOrder.OrderType = odr.Type;
                    svOrder.RecordType = 'vlocity_order';
                    svOrder.ProductType = '';
                    svOrder.Status = odr.status;
                    svOrder.CreatedDate = odr.CreatedDate;
                    
                    // Sandip - 08 June 2016 - Translation Phase II
                    svOrder.strCreatedDate = SMB_WebserviceHelper.getDateFormat(odr.CreatedDate);
                    // End
                    
                    svOrder.OwnerFullName = odr.Owner.name;
                    svOrder.RequestedDueDate = odr.RequestedDate__c;
                    
                    // Sandip - 08 June 2016 - Translation Phase II
                    svOrder.strRequestedDueDate = SMB_WebserviceHelper.getDateFormat(odr.RequestedDate__c);
                    // End
                    
                    if(String.isNotBlank(odr.recordtype.developername) && 'Customer_Solution'.equalsIgnoreCase(odr.recordtype.developername)){
                        if(masterOrderMastOrderMap!=null && String.isNotBlank(odr.ordernumber) &&
                           masterOrderMastOrderMap.get(odr.ordernumber)!=null &&
                           masterOrderMastOrderMap.get(odr.ordernumber)>1){
                               svOrder.ServiceAddress='';
                           }
                        if(masterOrderMastOrderMap!=null && String.isNotBlank(odr.ordernumber) &&
                           masterOrderMastOrderMap.get(odr.ordernumber)!=null &&
                           masterOrderMastOrderMap.get(odr.ordernumber)==1){
                               svOrder.ServiceAddress=subOrderAddressMap.get(odr.ordernumber);
                              // svOrder.
                           }
                        
                    } else {
                        if(String.isNotBlank(odr.Service_Address_Full_Name__c)){
                            svOrder.ServiceAddress = odr.Service_Address_Full_Name__c;
                        } else{
                    svOrder.ServiceAddress = odr.Service_Address_Text__c;
                        }
                    }
                    
                    matchedQuoteOrders.add(svOrder);   
                    
                }
            }
            
            if (matchedQuoteOrders != null)
                matchedQuoteOrders.sort();
            integer quotesTotal = matchedQuoteOrders.size();
            if(quotesTotal > 1000)
            {
                for(Integer i = 1000; i < quotesTotal; i++)
                    matchedQuoteOrders.remove(1000);
            }
            
            matchedServReq = [select id, Legacy_Order_ID__c, Opportunity__r.Order_ID__c from Service_Request__c where Opportunity__c IN: matchedOpportunity];

            try{
                getLegacyOrders(acct.RCID__c, '500');          
            }catch(CalloutException e){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Web Service Exception occured, Please try again'));         
            }
                 
            return null;
        }
 
        public list<SMB_WebserviceHelper.orderWrapper> getLegacyOrders(string RCID, string toRow){ 
            smb_OrderTracking_CalloutHelper smbCallOut = new smb_OrderTracking_CalloutHelper();
            lstOrders = new list<smb_orders_trackingtypes_v4.OrderRequest>();               
            lstOrders = smbCallOut.getOrdersByRCID(RCID, toRow);  
            if(lstOrders == null) return null;          
    
            orderList = new list<SMB_WebserviceHelper.orderWrapper>();
    
            for(smb_orders_trackingtypes_v4.OrderRequest lstOR:lstOrders){                                                      
                orderList.add(new SMB_WebserviceHelper.orderWrapper(lstOR));
            }                                   
            
            list<String> lstLegacyID = new list<String>();
            //Sandip - 08 June 2016 - Translation Phase II
            Map<String, Order_Status_Translation__c> orderStatusMap = Order_Status_Translation__c.getAll();
            //End
            for(SMB_WebserviceHelper.orderWrapper legacyOW:orderList){
                for(Service_Request__c sr:matchedServReq){
                    //Sandip - 08 June 2016 - Translation Phase II
                    if(UserInfo.getLocale().startsWith('fr') && legacyOW.orderStatus != null){
                        Order_Status_Translation__c objStatus = orderStatusMap.get(legacyOW.orderStatus);
                        if(objStatus != null && objStatus.French_Translation_Value__c != null){
                            legacyOW.orderStatus = objStatus.French_Translation_Value__c;
                        }
                    }
                    try{
                         system.debug('@@@ test date: ' + legacyOW.createDate);
                         legacyOW.strOrderDueDate = SMB_WebserviceHelper.getDateFormat(legacyOW.OrderDueDate);
                         legacyOW.strCustReqDate = SMB_WebserviceHelper.getDateFormat(legacyOW.custReqDate);
                         legacyOW.strCreateDate = SMB_WebserviceHelper.getDateFormat(legacyOW.createDate);
                         legacyOW.strCompletionDate = SMB_WebserviceHelper.getDateFormat(legacyOW.completionDate);
                    }catch(Exception ex){
                        system.debug('###lstLegacyID dates: '+ legacyOW.OrderDueDate + ' ' + legacyOW.custReqDate);
                        system.debug('###lstLegacyID dates: '+ legacyOW.createDate + ' ' + legacyOW.completionDate);
                    }
                    //End
                    if(sr.Legacy_Order_ID__c != null){
                        lstLegacyID = sr.Legacy_Order_ID__c.split(',');
                        //system.debug('***lstLegacyID: '+lstLegacyID);
                        //system.debug('***lstLegacyID.size: '+lstLegacyID.size());
                        for(integer i=0;i < lstLegacyID.size(); i++ ){
                            //system.debug('***lstLegacyID field: '+ lstLegacyID[i]);
                            //system.debug('***legacyOW.orderID: '+legacyOW.orderID);
                            //create concatenated field to compare results from service request
                            String LOID_Seq = legacyOW.orderID + '-' + legacyOW.seqNbr;                                         
                            if(LOID_Seq.trim() == lstLegacyID[i].trim()){
                                legacyOW.legacyID = sr.Opportunity__r.Order_ID__c;
                            }

                            //if(legacyOW.orderID.trim() == lstLegacyID[i].trim()){
                                //system.debug('***inside');
                                //legacyOW.legacyID = sr.Opportunity__r.Order_ID__c;
                            //}
                        }
                    }                   
                    
                    //if(legacyOW.orderID == sr.Legacy_Order_ID__c){
                        //legacyOW.legacyID = sr.Opportunity__r.Order_ID__c;
                    //}
                }
            }
            return orderList;               
        }
        
        /*public class orderWrapper {   
            public string orderID {get;set;}
            public string orderType {get;set;}
            public DateTime orderDueDate {get;set;}
            public string orderStatus {get;set;}        
            public string orderServiceType {get;set;}
            public string legacyOrderType {get;set;}
            public string dataOrderType {get;set;}
            public string voiceOrderType {get;set;}
            
            public string systemName {get;set;}
            public string holdJEOP {get;set;}
            public datetime custReqDate {get;set;}
            public datetime createDate {get;set;}
            public datetime completionDate {get;set;}
            public string dispStatus {get;set;}
            public string origID {get;set;}
            
            public string orderAddress {get;set;}
            //public string productDetail {get;set;}            
            public string psiFox {get;set;}
            
            public datetime issueDate {get;set;}
            public string orderActionID {get;set;}
            
            public string sequenceNum {get;set;}
            
            public string productType {get;set;}                    
            
            public string legacyID {get;set;}
            
            public orderWrapper(smb_orders_trackingtypes_v4.OrderRequest legacyOrders){             
                systemName = legacyOrders.orderKey.systemName;
                if(legacyOrders.orderKey.legacyOrderId != null) orderID = legacyOrders.orderKey.legacyOrderId;
                if(legacyOrders.orderTypeSet.legacyOrderType != null) legacyOrderType = legacyOrders.orderTypeSet.legacyOrderType;
                //product (throwing a null error message?)
                //if(legacyOrders.productList.get(0).productDetail != null)
                    //productDetail = legacyOrders.productList.get(0).productDetail;
                
                holdJEOP = legacyOrders.holdJeopCode;
                if(legacyOrders.orderStatusSet.orderStatus != null) orderStatus = legacyOrders.orderStatusSet.orderStatus;
                if(legacyOrders.orderDueDateSet.dueDate != null) orderDueDate = legacyOrders.orderDueDateSet.dueDate;
                //service addr
                if(legacyOrders.orderAddress.get(0).address != null)
                    orderAddress = legacyOrders.orderAddress.get(0).address;                
                
                custReqDate = legacyOrders.orderDueDateSet.customerRequestDate;
                createDate = legacyOrders.orderDueDateSet.creationDate;
                completionDate = legacyOrders.orderDueDateSet.completionDate;
                //psi fox
                psiFox = legacyOrders.orderIdSet.psiFoxOrder;
                
                dispStatus = legacyOrders.orderStatusSet.dispatchStatusCd;
                origID = legacyOrders.orderIdSet.originatorId;
                
                if(legacyOrders.orderTypeSet.orderServiceType != null) orderServiceType = legacyOrders.orderTypeSet.orderServiceType;               
                if(legacyOrders.orderTypeSet.dataOrderType != null) dataOrderType = legacyOrders.orderTypeSet.dataOrderType;
                if(legacyOrders.orderTypeSet.orderType != null) orderType = legacyOrders.orderTypeSet.orderType;
                if(legacyOrders.orderTypeSet.voiceOrderType != null) voiceOrderType = legacyOrders.orderTypeSet.voiceOrderType;
                
                issueDate = legacyOrders.orderKey.issueDate;
                System.debug('ISSUE DATE->'+issueDate);
                
                orderActionID = string.valueOf(legacyOrders.orderKey.orderActionId);                
                
                sequenceNum = legacyOrders.orderKey.sequenceNum;
                
                productType = legacyOrders.orderTypeSet.orderServiceType;
                
                legacyID = '';
            }   
            //insert here
             
        }   */ 
 
 
        private static Account getAccount(Id accountId) {
        if (accountId == null) return null;
        
        return [SELECT  Id, ParentId, Name, RCID__c, RecordTypeId, RecordType.DeveloperName, Account_Status__c
                FROM Account 
                WHERE Id =: accountId];
        }


    @RemoteAction
    global static List<Opportunity> getCases(Id acctId, Id parentId){
      if(acctId == null) return null;

      //system.debug('***** acctId: ' + acctId);

      list<Id> allAccountIdsInHierarchy = new list<Id>();
      allAccountIdsInHierarchy = smb_AccountUtility.getAccountsInHierarchy(acctId, parentId);

      list<Opportunity> matchedOpportunity = new list<Opportunity>();
            matchedOpportunity = [SELECT Id, Name, Owner.Name, Total_Contract_Renewal__c, StageName, CloseDate, CreatedDate, Order_ID__c, Condition__c, Order_Type__c, Description, Status__c, Requested_Due_Date__c   
                                                FROM Opportunity
                                                WHERE AccountId IN :allAccountIdsInHierarchy ];         

      //system.debug('***** matchedOpportunity : ' + matchedOpportunity );
      return matchedOpportunity ;            
    }
        @TestVisible
        private static string abbreviate(string text, integer length) {
        if (string.isBlank(text)) return '';
        
        return text.abbreviate(length);
    }   

        public String baseUrl {
        get {
            // Changes as per in Mydomain imlplementation
            //return 'https://' + URL.getSalesforceBaseUrl().getHost().substring(2,6) + '.salesforce.com';
            String inst = URLMethodUtility.getInstanceName();
            return 'https://' + inst + '.salesforce.com';
        }
    }
         

}