/*#########################################################################
# File..................: AddressCreateValidateController
# API Version...........: 26
# Created by............: Andrew Castillo
# Created Date..........: 3-4-2013
# Description...........: This is the controller for the VF page AddressCreateValidate.
#                         The class processes the new address fields and validates them by making a callout to the
#                         Canada Post web service.
# Copyright (c) 2000-2012. Astadia, Inc. All Rights Reserved.
#
# Created by the Astadia, Inc. Modification must retain the above copyright notice.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any commercial purpose, without fee, and without a written
# agreement from Astadia, Inc., is hereby forbidden. Any modification to source
# code, must include this paragraph and copyright.
#
# Permission is not granted to anyone to use this software for commercial uses.
#
# Company URL : http://www.astadia.com
###########################################################################*/
public class AddressCreateValidateController
{
    public static String CODE_TYPE_COUNTRY = 'Country';
    public static String CODE_TYPE_PROVINCE = 'Canadian Province';
    public static String RE_CANADIAN_POSTAL_CODE = '^[AaBbCcEeGgHhJjKkLlMmNnPpRrSsTtVvXxYy]{1}\\d{1}[A-Za-z]{1} *\\d{1}[A-Za-z]{1}\\d{1}$';
    public static String RE_USA_POSTAL_CODE = '^\\d{5}(-\\d{4})?$';
    
    //Properties
    //Properties
    //Properties
    
    public String acctId {get; set;}
    public smb_AddressValidationUtility.Address entryAddr {get; set;}
    public List<smb_AddressValidationUtility.Address> returnedAddrs {get; set;}
    public List<SelectOption> countryOptions {get; set;}
    public List<SelectOption> provOptions {get; set;}
    public Boolean hasRetAddr {get; set;}
    public Integer selRetAddrNum {get; set;}
    public Boolean radioChecked {get; set;}
    public Boolean canadaSelected {get; set;}
    
    public Account acct {get; set;}
    
    public List<SelectOption> getaddrType()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Billing','Billing'));
        options.add(new SelectOption('Shipping','Shipping'));
        return options;
    }
    
    
    //Constructors
    //Constructors
    //Constructors
    
    public AddressCreateValidateController(ApexPages.StandardController stdController)
    {
        acct = (Account) stdController.getRecord();    
        initialise('Shipping'); 
    }

    private void initialise(string addressType) {
        
        if (acct == null || acct.Id == null) return;
        
        acctId = acct.Id;
        
        //assume the account isn't new
        if (acctId != null) {
            acct = [Select Id, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, ShippingStreet,
                        ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry
                    From Account
                    Where Id = :acctId];
        }
        
        hasRetAddr = false;
        radioChecked = false;
        
        getCountries();
        getProvinces();
        
        populateEntryAddress(addressType);

    }


    // Page Actions
    // Page Actions
    // Page Actions
    
    public PageReference doValidate()
    {
        if (!validateEntry())
        {           
            hasRetAddr = false;
            return null;
        }
        
        smb_AddressValidationUtility.ValidateAddressResponse response;
        
        try {
                response = smb_AddressValidationUtility.validate(entryAddr);
                //QC Defect - 17692 - Assigned returned address to proprty, this will allow to set boolean field
                //hasRetAddr as true and in turn will display save buttons              
                returnedAddrs = response.returnedAddresses;
        }
        catch (smb_AddressValidationException ex) {
            for (string message : ex.messages) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, message));
            }
        }
        catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage()));
        }
        system.debug('-------response------'+response);
        
        if (response != null) {
	        for (string message : response.errorMessages) {
	            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, message));
	        }
	        for (string message : response.infoMessages) {
	            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, message));
	        }
        }
        
        if (response == null || returnedAddrs == null || ApexPages.hasMessages(ApexPages.Severity.FATAL)) {
            hasRetAddr = false;
            radioChecked = false;
            return null;
        }
        system.debug('----returnedAddrs---'+returnedAddrs);
        hasRetAddr = returnedAddrs.size() > 0;            
        radioChecked = returnedAddrs.size() == 1;
    
        if (!ApexPages.hasMessages(ApexPages.Severity.ERROR))
        {
            // QC Defect - 17692commented, as below statement do page redirect while actually it should be on same page to show save buttons
            //return doSave();
        }
        
        return null;
    }
    
    public PageReference doSaveValid()
    {
        if (!validateEntry())
        {
            return null;
        }
            
        applySelectedAddress();
        
        if (acct.Id != null) {
            update acct;
        }
        
        return createReturnPageReference();
    }

    public PageReference doSave()
    {
        if (!validateEntry())
        {
            return null;
        }
                
        applyEnteredAddress();
        
        update acct;    
        
        return createReturnPageReference();
    }
    
    public PageReference doCancel()
    {
        return createReturnPageReference();
    }
    
    private PageReference createReturnPageReference() {
        
        string retUrl = ApexPages.currentPage().getParameters().get('retUrl');
        
        if (retUrl == null) {
            retUrl = '/' + acctId;
        }
        
        System.debug(retUrl);
        
        return new PageReference(retUrl);
    }
    
    public PageReference doAddrType()
    {
        populateEntryAddress(entryAddr.seladdrType);
        
        hasRetAddr = false;
        
        return null;
    }
    
    public PageReference doAddrForm()
    {       
        hasRetAddr = false;
        
        return null;
    }
    
    public PageReference doSelectCountry()
    {
        if (entryAddr.country == 'CAN')
        {
            canadaSelected = true;
        }
        else
        {
            canadaSelected = false;
        }
        
        hasRetAddr = false;
        
        return null;
    }
    
    public void applySelectedAddress() {
        smb_AddressValidationUtility.Address selAddr = returnedAddrs.get(selRetAddrNum);
                
        // determine the address type
        if (entryAddr.seladdrType == 'Billing')
        {
            acct.BillingStreet = selAddr.addressConcat;
            acct.BillingCity = selAddr.city;
            acct.BillingState = selAddr.provState;
            acct.BillingPostalCode = selAddr.postalCode;
            acct.BillingCountry = selAddr.country;
        }
        else if (entryAddr.seladdrType == 'Shipping')
        {
            acct.ShippingStreet = selAddr.addressConcat;
            acct.ShippingCity = selAddr.city;
            acct.ShippingState = selAddr.provState;
            acct.ShippingPostalCode = selAddr.postalCode;
            acct.ShippingCountry = selAddr.country;
        }
    }
    
    public void applyEnteredAddress() {
        // determine the address type
        if (entryAddr.seladdrType == 'Billing')
        {
            acct.BillingStreet = entryAddr.addressConcat;
            acct.BillingCity = entryAddr.city;
            acct.BillingState = entryAddr.provState;
            acct.BillingPostalCode = entryAddr.postalCode;
            acct.BillingCountry = entryAddr.country;
        }
        else if (entryAddr.seladdrType == 'Shipping')
        {
            acct.ShippingStreet = entryAddr.addressConcat;
            acct.ShippingCity = entryAddr.city;
            acct.ShippingState = entryAddr.provState;
            acct.ShippingPostalCode = entryAddr.postalCode;
            acct.ShippingCountry = entryAddr.country;
        }
    }
    
    
    
    
    //Helper Methods
    //Helper Methods
    //Helper Methods
    
    /* This method populates the entry address with either the billing or shipping address
     *
    */
    private void populateEntryAddress(String addrType)
    {
        entryAddr = smb_AddressValidationUtility.createAddressFromAccount(acct, addrType);
        
        // set flag that CAN is selected
        if (entryAddr.country == 'CAN')
        {
            canadaSelected = true;
        }
    }
    
    
    

    
    /* This method validates that an address has been entered
     *
    */
    private Boolean validateEntry()
    {
        Boolean valid = true;
        
        /*if (entryAddr.civicNumber == null || entryAddr.civicNumber == '')
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please enter in a Civic Number'));
            valid = false;
        }*/
        if (entryAddr.addressLine1 == null || entryAddr.addressLine1 == '')
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please enter in an Address'));
            valid = false;
        }
        if (entryAddr.city == null || entryAddr.city == '')
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please enter in a Municipality'));
            valid = false;
        }
        if (entryAddr.provState == null || entryAddr.provState == '')
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please enter in a Province'));
            valid = false;
        }
        if (entryAddr.postalCode == null || entryAddr.postalCode == '')
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please enter in a Postal Code'));
            valid = false;
        }
        if (entryAddr.country == 'CAN' && entryAddr.postalCode != null)
        {
            entryAddr.postalCode = entryAddr.postalCode.trim();
            
            // if postal code does not have a space, add one
            if (!entryAddr.postalCode.contains(' '))
            {
                entryAddr.postalCode = entryAddr.postalCode.left(3) + ' ' + entryAddr.postalCode.mid(3, entryAddr.postalCode.length());
            }
            
            if (!Pattern.matches(RE_CANADIAN_POSTAL_CODE, entryAddr.postalCode))
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please enter in a valid Canadian Postal Code in the format (A1A 1X1)'));
                valid = false;
            }
            
        }
        if (entryAddr.country == 'USA' && entryAddr.postalCode != null)
        {
            if (!Pattern.matches(RE_USA_POSTAL_CODE, entryAddr.postalCode))
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please enter in a valid USA Postal Code in the format (11111) or (11111-1111)'));
                valid = false;
            }
            
        }
        if (entryAddr.country == null || entryAddr.country == '')
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please enter in a Country'));
            valid = false;
        }
        
        return valid;
    }
    
    /* This method validates that an address has been selected
     *
    */
    /*
    private Boolean validateSelection()
    {
        Boolean valid = false;
        
        if (selRetAddrNum == null)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please select a Matching Address'));
        }
        else
        {
            valid = true;
        }
        
        return valid;
    }*/
    
    private void getCountries()
    {
        List<Country_Province_Codes__c> Codes = queryCodes(CODE_TYPE_COUNTRY);
        
        if (Codes == null)
        {
            return;
        }
        
        countryOptions = new List<SelectOption>();
        for (Country_Province_Codes__c Code : Codes)
        {
            //SelectOption countryOption = new SelectOption(Code.Name, Code.Description__c);
            SelectOption countryOption = new SelectOption(Code.Name, Code.Description__c);
            
            countryOptions.add(countryOption);
        }
    }
    
    private void getProvinces()
    {
        List<Country_Province_Codes__c> Codes = queryCodes(CODE_TYPE_PROVINCE);
        
        if (Codes == null)
        {
            return;
        }
        
        provOptions = new List<SelectOption>();
        for (Country_Province_Codes__c Code : Codes)
        {
            //SelectOption provOption = new SelectOption(Code.Name, Code.Description__c);
            SelectOption provOption = new SelectOption(Code.Name, Code.Name + ' - ' + Code.Description__c);
            
            provOptions.add(provOption);
        }
    }
    
    private List<Country_Province_Codes__c> queryCodes(String codeType)
    {
        if (codeType == null || codeType == '')
        {
            return null;
        }
        
        List<Country_Province_Codes__c> Codes = [Select Id, Name, Description__c, Type__c
                                                 From Country_Province_Codes__c
                                                 Where Type__c = :codeType
                                                 Order By Name];
                                                 
        return Codes;
    }
    
    
}