/*****************************************************
    Name: HSIAThresholdNotificationEventImpl
    Usage: This class used process event for HSIA thersold breach
    Author – Sandip Chaudhari (IBM)
    Revision History -
    Created By/Date - Sandip Chaudhari / 04 July 2016
    
    Updated By/Date/Reason for Change - 
    
******************************************************/
global class HSIAThresholdNotifyEventHandlerImpl implements EventHandlerInterface{
    
    /*
    * Name - processEvent
    * Description - Process all events
    * Param - List<External_Event__c> eventObjects
    * Return Type - void
    */
    global void processEvent(List<External_Event__c> eventObjects, List<ExternalEventRequestResponseParameter.ResponseEventData> responseData){
        try{
            List<Messaging.SingleEmailMessage> listEmail = new List<Messaging.SingleEmailMessage>();
            set<Id> accIdSet = new set<Id>();
            map<String,Id> accCANMap = new map<String,Id>();
            map<String,Id> aseetAccMap = new map<String,Id>();
            map<String,Asset> assetMap = new map<String,Asset>();
            map<Id,Account> mapRCID = new map<Id,Account>();
            map<Id, List<contact>> accContactMap = new map<Id, List<contact>>();
            map<External_Event__c, Map<String, Object>> eventJSONMap = new map<External_Event__c, Map<String, Object>>();
            List<String> listCAN = new List<String>();
            List<String> listServiceIds = new List<String>();
            Set<String> templateNames = new Set<String>();
            
            // capture billing account number and service Ids
            for(External_Event__c eventObject: eventObjects){
                Map<String, Object> dataMap = EventHandler_Helper.parseJSON(eventObject.Event_Details__c);
                eventJSONMap.put(eventObject,dataMap);
                if(dataMap != null && !(String.isBlank((String)dataMap.get('BillingAccountNumber')))){
                    listCAN.add((String)dataMap.get('BilllingSystemId') + '-'+(String)dataMap.get('BillingAccountNumber'));
                }else{
                    String serviceId = (String)dataMap.get('ServiceID');
                    if(serviceId != null && serviceId.length() == 10){
                        String firstPart = serviceId.substring(0,3);
                        String secondPart = serviceId.substring(3,6);
                        String thirdPart = serviceId.substring(6);
                        String formattedStr = '(' + firstPart + ')' + ' ' + secondPart + '-' + thirdPart;
                        dataMap.put('ServiceID', formattedStr);
                        system.debug('@@@ Asset Number 1 : ' + formattedStr);
                    }
                    listServiceIds.add((String)dataMap.get('ServiceID'));
                }
                templateNames.add('%' + eventObject.Event_Name__c + '%');
            }
            
            system.debug('@@@ templateNamesList' + templateNames);
            // get all required email templates
            List<External_Event_Notification_Templates__c> templateConfigList = EventHandler_Helper.getTemplateConfig(templateNames);
            Map<String, EmailTemplate> emailTemplates = EventHandler_Helper.getEmailTemplates(templateConfigList);
            
            // get assets for service Ids present in json and where billing account number is blank
            assetMap = EventHandler_Helper.getAssetsByserviceId(listServiceIds);
            if(assetMap != null && assetMap.size()>0){
                 for(Asset assetObj: assetMap.values()){
                     aseetAccMap.put(assetObj.Phone__c, assetObj.AccountId);
                 }
            }
            // get account list
            List<Account> accList = EventHandler_Helper.getBillingAccounts(listCAN, aseetAccMap.values());
            system.debug('@@@ accList BAN  ' + accList );
            if(accList != null && accList.size()>0){
                for(Account accObj: accList){
                    accCANMap.put(accObj.BAN_CAN__c, accObj.Id);
                    accIdSet.add(accObj.Id);
                }
            }
            
          //  map<Id,String> addrerssMap = EventHandler_Helper.getServiceLocation(accIdSet);
            system.debug('@@@accCANMap ' + accCANMap);
            // get respective RCID accounts
            map<Id,Id> rcidMap = smb_AccountUtility.getRcidAccountId(accIdSet);
            system.debug('@@@rcidMap ' + rcidMap);
            if(rcidMap != null && rcidMap.size()>0){
                List<Account> listRCID = [SELECT id, Name, OwnerId, BAN_CAN__c, Owner.Name, RCID__c FROM Account WHERE ID IN: rcidMap.values()];
                if(listRCID != null && listRCID.size()>0){
                    for(Account accObj: listRCID){
                        mapRCID.put(accObj.Id, accObj);
                    }
                    // get all valid contacts for respective RCID
                    accContactMap = EventHandler_Helper.getAssociatedContactsForAccount(mapRCID.keySet());
                }
            }
            // get ORG wide email address to set in from address
            Map<String, OrgWideEmailAddress> oweMap = EventHandler_Helper.getOWEmailAddress(templateConfigList);
            // process events
            for(External_Event__c eventObject: eventObjects){
                List<ExternalEventRequestResponseParameter.EventError> errorList = new List<ExternalEventRequestResponseParameter.EventError>();
                ExternalEventRequestResponseParameter.ResponseEventData responseObject = new ExternalEventRequestResponseParameter.ResponseEventData();
                for(ExternalEventRequestResponseParameter.ResponseEventData responseObj: responseData){
                    if(eventObject.Event_Id__c == responseObj.eventId){
                        responseObject = responseObj;
                        break;
                    }
                }
                           
                system.debug('@@@ In Loop ');
                try{
                    Map<String, Object> dataMap = eventJSONMap.get(eventObject);
       
                    if(dataMap != null){
                        EventHandlerParameter paramObject = new EventHandlerParameter();
                        paramObject.eventObject = eventObject;
                        paramObject.dataMap = dataMap;
                        
                        String accountCAN;
                        String billingAccountNumber;
                        
                        datetime dt= system.now();
                        paramObject.currentDate = dt.format('MMM dd, yyyy');
                       
                        //Logic to process by serviceID
                        accountCAN = aseetAccMap.get((String)dataMap.get('ServiceID'));
                        system.debug('@@@ Asset accountCAN  ' + accountCAN);
                        if(assetMap.get((String)dataMap.get('ServiceID'))!= null){
                            paramObject.accountNumber = assetMap.get((String)dataMap.get('ServiceID')).Account.CAN__c;
                            if(paramObject.accountNumber == null){
                                if((String)dataMap.get('ECID') != null){
                                    paramObject.accountNumber = (String)dataMap.get('ECID');
                                }else{
                                    paramObject.accountNumber = (String)dataMap.get('ServiceID');
                                }
                            }
                            system.debug('@@@ paramObject.accountNumber Asset  ' + paramObject.accountNumber);
                            eventObject.SFDC_Object_Context__c = assetMap.get((String)dataMap.get('ServiceID')).Account.CAN__c;
                        }
                        
                        //Logic to override asset if BillingNumber exists in JSON
                        if(accCANMap.get((String)dataMap.get('BilllingSystemId') + '-'+(String)dataMap.get('BillingAccountNumber')) != null){
                           billingAccountNumber = (String)dataMap.get('BilllingSystemId') + '-'+(String)dataMap.get('BillingAccountNumber');
                           paramObject.accountNumber = (String)dataMap.get('BillingAccountNumber');
                           accountCAN = accCANMap.get(billingAccountNumber);
                           eventObject.SFDC_Object_Context__c = billingAccountNumber;
                           system.debug('@@@ BN accountCAN  ' + accountCAN);
                        }

                        system.debug('@@@ accountCAN  ' + accountCAN);
                        Account rcidObj;
                        Id accountRCID;
                        if(accountCAN != null){
                            accountRCID = rcidMap.get(accountCAN);
                            if(accountRCID != null){
                                rcidObj = mapRCID.get(accountRCID);
                                if(rcidObj != null){
                                    paramObject.rcid = rcidObj.RCID__c.Substring(rcidObj.RCID__c.Length() - 3).leftPad(rcidObj.RCID__c.Length(), '*');
                                }else{
                                    eventObject.Event_Process_Error__c = system.label.ExternalEventRCIDNotExists + '-' + accountCAN;
                                }
                            }else{
                                    eventObject.Event_Process_Error__c = system.label.ExternalEventRCIDNotExists + '-' + accountCAN;
                            }
                        }else{
                            // Error will be returned in response if its syncronous execution only
                            ExternalEventRequestResponseParameter.EventError errorObject1 = new ExternalEventRequestResponseParameter.EventError();
                            if((String)dataMap.get('BillingAccountNumber')!=null && (String)dataMap.get('BillingAccountNumber')!=''){
                                billingAccountNumber = (String)dataMap.get('BilllingSystemId') + '-'+(String)dataMap.get('BillingAccountNumber');
                                eventObject.Event_Process_Error__c = system.label.ExternalEventBillingAccountNotExists + '-' + billingAccountNumber;
                                errorObject1.errorDescription = system.label.ExternalEventBillingAccountNotExists + '-' + billingAccountNumber;
                                errorObject1.errorCode = 'EE-ERR-014';
                            }else{
                                eventObject.Event_Process_Error__c = system.label.ExternalEventBillingAccountNotExists1 + '-' + (String)dataMap.get('ServiceID');
                                errorObject1.errorDescription = system.label.ExternalEventBillingAccountNotExists1 + '-' + (String)dataMap.get('ServiceID');
                                errorObject1.errorCode = 'EE-ERR-018';
                            }
                            errorList.add(errorObject1);
                            system.debug('@@@ In Error');
                        }
                        
                        List<Contact> rcidContactList = accContactMap.get(accountRCID);
                        Integer thersholdPercentage = Integer.valueOf(dataMap.get('ThresholdPercentage'));
                        String defaultTemplateSearchString = eventObject.Event_Name__c+thersholdPercentage+'English';
                        EmailTemplate eTemplate;
                        if(rcidObj != null){ 
                            system.debug('@@@before Contact  ');
                            If(rcidContactList != null && rcidContactList.size()>0){
                                paramObject.cc = new List<String>();
                                for(Contact conObj: rcidContactList){
                                    system.debug('@@@ Email ID ' + conObj.Email);
                                    External_Event_Notification_Templates__c templateMapping;
                                    if(conObj.Email != null){
                                        if(eTemplate == null){
                                            String templateSearchString = eventObject.Event_Name__c+thersholdPercentage+conObj.Language_Preference__c;
                                            if(thersholdPercentage >= 100){
                                                templateSearchString = eventObject.Event_Name__c+'100'+conObj.Language_Preference__c;
                                                defaultTemplateSearchString = eventObject.Event_Name__c+'100English';
                                            }
                                            templateMapping = External_Event_Notification_Templates__c.getInstance(templateSearchString);
                                            if(templateMapping == null){
                                                templateMapping = External_Event_Notification_Templates__c.getInstance(defaultTemplateSearchString);        
                                            }
                                            
                                            if(emailTemplates != null && templateMapping != null){
                                                if(oweMap.get(templateMapping.Organization_Wide_Addresses_Detail__c) != null){
                                                    OrgWideEmailAddress oweObj = oweMap.get(templateMapping.Organization_Wide_Addresses_Detail__c);
                                                    if(oweObj!= null){
                                                        paramObject.orgWideEmailAddress = oweObj.Id;
                                                    }
                                                }
                                                eTemplate = emailTemplates.get(templateMapping.Email_Template_Name__c);
                                            }
                                        }
                                        
                                        if(paramObject.targetId == null){
                                            paramObject.targetId = conObj.Id;
                                            paramObject.toName = conObj.Name;
                                            paramObject.WhatId = rcidObj.Id;
                                        }else{
                                            paramObject.cc.add(conObj.Email);
                                        }
                                        system.debug('@@@ paramObject.targetId ' + paramObject.targetId);
                                    }else{
                                        eventObject.Event_Process_Error__c = system.label.ExternalEventNotValidEmailAddress;
                                    }
                                }
                                if(eTemplate != null){
                                    prepareEmailBody(eTemplate, paramObject);
                                    Messaging.SingleEmailMessage emailObj = EventHandler_Helper.setEmailAttribute(paramObject);
                                    if(emailObj != null){
                                        listEmail.add(emailObj);
                                        //system.debug('@@@ emailObj ' + emailObj);
                                    }
                                }else{
                                    eventObject.Event_Process_Error__c = system.label.ExternalEventTemplateNotFound;
                                }
                               
                            }else{
                                eventObject.Event_Process_Error__c = system.label.ExternalEventContactNotExists + ' ' + rcidObj.Name;
                            }
                        }
                }
                //end
                }catch(Exception ex){
                    eventObject.Event_Process_Error__c = ex.getmessage() + ' ' + ex.getstacktracestring();
                    EventHandler_Helper.logError('HSIAThresholdNotifyEventHandlerImpl.processEvent', eventObject.Id ,ex.getmessage(), ex.getstacktracestring(), 'Mediation');
                }
                responseObject.error = errorList;
                responseData.add(responseObject);
            }
            if(listEmail != null && listEmail.size()>0){
                //system.debug('@@@ listEmail ' + listEmail);
                Messaging.SendEmailResult[] results = Messaging.sendEmail(listEmail);
            }
        }catch(Exception ex){
            EventHandler_Helper.logError('HSIAThresholdNotifyEventHandlerImpl.processEvent', '',ex.getmessage(), ex.getstacktracestring(), 'Mediation');
        }
    }
    
    /*
    * Name - processEvent
    * Description - Validate event details - check mandatory elements in json
    * Param - String eventDetails, List<ExternalEventRequestResponseParameter.EventError> errorList
    * Return Type - void
    */
    global static void validateEventDetails(String eventDetails, List<ExternalEventRequestResponseParameter.EventError> errorList){
        Map<String, Object> jsonDataMap = (Map<String, Object>)JSON.deserializeUntyped(eventDetails);
        system.debug('@@@@JSON MAP' + jsonDataMap);
        if(jsonDataMap != null){
            String billingAccountNumber = (String)jsonDataMap.get('BilllingSystemId') + '-'+(String)jsonDataMap.get('BillingAccountNumber');
            EventHandler_Helper.validateAndLogError('ServiceID', jsonDataMap, 'EE-ERR-004', billingAccountNumber + '-' + system.label.ExternalEventMandatoryDataMissingMessage, errorList);
            EventHandler_Helper.validateAndLogError('BillCycle', jsonDataMap, 'EE-ERR-005', billingAccountNumber + '-' + system.label.ExternalEventMandatoryDataMissingMessage, errorList);
            EventHandler_Helper.validateAndLogError('Quota', jsonDataMap, 'EE-ERR-006', billingAccountNumber + '-' + system.label.ExternalEventMandatoryDataMissingMessage, errorList);
            EventHandler_Helper.validateAndLogError('SubscriberAddress', jsonDataMap, 'EE-ERR-007', billingAccountNumber + '-' + system.label.ExternalEventMandatoryDataMissingMessage, errorList);
            EventHandler_Helper.validateAndLogError('ThresholdPercentage', jsonDataMap, 'EE-ERR-008', billingAccountNumber + '-' + system.label.ExternalEventMandatoryDataMissingMessage, errorList);
            EventHandler_Helper.validateAndLogError('ActualUsage', jsonDataMap, 'EE-ERR-09', billingAccountNumber + '-' + system.label.ExternalEventMandatoryDataMissingMessage, errorList);
            EventHandler_Helper.validateAndLogError('HSIAPricePlan', jsonDataMap, 'EE-ERR-010', billingAccountNumber + '-' + system.label.ExternalEventMandatoryDataMissingMessage, errorList);
            EventHandler_Helper.validateAndLogError('ThresholdTimestamp', jsonDataMap, 'EE-ERR-011', billingAccountNumber + '-' + system.label.ExternalEventMandatoryDataMissingMessage, errorList);
            EventHandler_Helper.validateAndLogError('EventType', jsonDataMap, 'EE-ERR-012', billingAccountNumber + '-' + system.label.ExternalEventMandatoryDataMissingMessage, errorList);
            EventHandler_Helper.validateAndLogError('BilllingSystemId', jsonDataMap, 'EE-ERR-013', billingAccountNumber + '-' + system.label.ExternalEventMandatoryDataMissingMessage, errorList);
        }
        return;
    }
    
    /*
    * Name - prepareEmailBody
    * Description - Prepare html and plain email body
    * Param - EmailTemplate eTemplate , EventHandlerParameter paramObject
    * Return Type - void
    */
    global static void prepareEmailBody(EmailTemplate eTemplate , EventHandlerParameter paramObject){
            paramObject.subject = eTemplate.subject;
            paramObject.htmlBody = eTemplate.HtmlValue;
            paramObject.plainBody = eTemplate.body;
            replaceJSONValuesInTemplate('html', paramObject);
            replaceJSONValuesInTemplate('plain', paramObject);
    }
    
    /*
    * Name - replaceJSONValuesInTemplate
    * Description - Replace JSON values in templates
    * Param - String typeOfBody, EventHandlerParameter paramObject
    * Return Type - void
    */
    global static void replaceJSONValuesInTemplate(String typeOfBody, EventHandlerParameter paramObject){
            String emailBody ='';
            if(typeOfBody == 'html'){
                emailBody = paramObject.htmlBody;
            }else{
                emailBody = paramObject.plainBody;
            }
            emailBody =  emailBody.replace('{{toName}}', paramObject.toName);
            emailBody =  emailBody.replace('{{BillingAccountNumber}}', paramObject.accountNumber);
            emailBody =  emailBody.replace('{{ThresholdTimestamp}}', paramObject.currentDate);
            //if(paramObject.serviceLocation != null){
            //  emailBody =  emailBody.replace('{{serviceLocation}}', paramObject.serviceLocation);
            //}
            
            for(String str: paramObject.dataMap.keySet()){
                emailBody = emailBody.replace('{{'+str+'}}', (String)paramObject.dataMap.get(str));
            }
            
            if(typeOfBody == 'html'){
                paramObject.htmlBody = emailBody;
            }else{
                paramObject.plainBody = emailBody;
            }
     }
 
}