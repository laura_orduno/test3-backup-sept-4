// public with sharing class VITILcareMyProfileCtlr {
// NOTE: when "with sharing" is specified on the class, contactDetail save fails with error INSUFFICIENT_ACCESS_OR_READONLY, this is a mystery since the MBRMyProfileCtlr
// works file and even with "with sharing" specified

public class VITILcareMyProfileCtlr {
    
    public transient String pageError {get; set;}
    public  String generalError {get; set;}
    public String LanguageType = '';
    public transient String profileUpdateComplete {get; set;}
    public transient String usernameError {get; set;}
    public transient String oldPasswordError {get; set;}
    public transient String weakPassword {get; set;}
    public transient String passwordNotMatch {get; set;}    
    public transient String[] errors {get; private set;}
    public Account accountDetail {get; set;}
    public Contact contactDetail {get; set;}
    public String editMode {get; set;}
    public String oldPassword {get; set;}
    public String newPassword {get; set;}
    public String confirmPassword {get; set;}
    public String username {
        get {
            return contactDetail.email;
        }
        set {
            contactDetail.email = value;
        }
    }
    public STATIC String EMAIL_PATTERN = '^[_A-Za-z0-9-&\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$';
        
    public PageReference initCheck()
    {
        try
        {
            String userAccountId;
            String userContactId;
            //userAccountId = (String)UserUtil.CurrentUser.AccountId;
            //userContactId = (String)UserUtil.CurrentUser.ContactId;
            List<User> users = [SELECT Id, AccountId, ContactId, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()];
            if (users.size() > 0) {
                userAccountId = users[0].AccountId;
                userContactId = users[0].ContactId;
                String LanguageTypeB = users[0].LanguageLocaleKey;
                LanguageType = LanguageTypeB;
            }
            accountDetail = [SELECT name FROM Account WHERE id = :userAccountId LIMIT 1];
            contactDetail = [SELECT Preferred_Contact_Method__c, Email, FirstName, LastName, mailingstreet, mailingcity, mailingstate, mailingcountry, mailingpostalcode, MobilePhone, Phone, id, Title FROM Contact WHERE id = :userContactId LIMIT 1];
            editMode = ApexPages.currentPage().getParameters().get('m');
            
            if(editMode == 'completed')
            {
            	profileUpdateComplete = label.mbrProfileUpdated;        
            }
        }
        catch(Exception e)
        {
            accountDetail = null;
            contactDetail = null;
            system.debug(e);
        }
        
        return null;
    }
        
    public void updateUser()
    {
System.debug('UPDATE USER!');        
        PageReference myProfilePage = Page.VITILcareMyProfile;
        myProfilePage.setRedirect(true);

        
        User currentUser = [SELECT Id, Name, UserName, Email, Alias, AccountId, ContactId, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()];
        
        System.debug(String.format('username({0}) == currentUser({1})', new List<String>{username, currentUser.username}));        
        
        if(username != null) {
//        if(username != null && !username.equals(currentUser.username)) {
            try 
            {
System.debug('updateUser: A');        
                if(!Pattern.matches(VITILcareMyProfileCtlr.EMAIL_PATTERN, username)) {
                //if(!trac_PageUtils.isValidEmail(username)) {
                    usernameError = label.mbrInvalidEmail;
                	return;
                }
System.debug('updateUser: B');        
                List<User> users = [select email, username from user where username = :username and UserType = 'CspLitePortal' and IsActive = true];
               if(users.size()>0) {
                    usernameError = label.mbrDuplicateUsername;
System.debug('ERROR, usernameError: ' + usernameError);       
                	return;
                }
System.debug('updateUser: C, ' + username);        
                currentUser.username = username;
                currentUser.email = username;
                update currentUser;
                
                return;
            }
            catch(Exception e) 
            {
                usernameError = e.getMessage();
System.debug('updateUser: ' + usernameError);        
                
                Apexpages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, usernameError));
                
                return;
            }
        }
        else{
                usernameError = String.format('Error: User cannot be updated since the username ({0}) cannot be the same as the perferred contact email ({1})', new List<String>{currentUser.username, username});
System.debug('ERROR: ' + String.format('Error: User cannot be updated since the username ({0}) cannot be the same as the perferred contact email ({1})', new List<String>{currentUser.username, username}));        
//            Apexpages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, String.format('Error: User cannot be updated sine the username ({0}) cannot be the same as the perferred contact email ({1})', new List<String>{currentUser.username, username})));     
            
            	
        }
        
		return;
    }

    
//    public void updateContact()
    public PageReference updateContact()
    {       
//        System.debug('VITILcareMyProfileCtlr updateContact accountDetail: ' + accountDetail);

        User currentUser = [SELECT Id, Name, UserName, Email, Alias, AccountId, ContactId, FirstName, LastName, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()];
        
        if(contactDetail==null) return null;
        
        if(username != null) {
            if(!Pattern.matches(VITILcareMyProfileCtlr.EMAIL_PATTERN, username)) {
            //if (!trac_PageUtils.isValidEmail(username)) {
                usernameError = label.mbrInvalidEmail;
                return null;
            }
        }


        Database.Saveresult sr = Database.update(contactDetail, false);
System.debug('updateContact: ' + contactDetail);        
System.debug('contactDetail.id: ' + contactDetail.id);   
        currentUser.FirstName = contactDetail.FirstName;
        currentUser.LastName = contactDetail.LastName;
        currentUser.LanguageLocaleKey = LanguageType;
        update currentUser;
        
        System.debug('SAVE RESULT: ' + sr);
        
        if (!sr.isSuccess()) {
            String exceptionString = '';
            
            for (Database.Error error : sr.getErrors()) {
                exceptionString = error.getMessage();
            }
            
            generalError = label.mbrProfileUpdateError;
            return null;
        }
        
        PageReference temp;
        
        if(editMode == 'password'){
            system.debug('VITILcareMyProfileCtlr updateContact oldPassword: '+oldPassword);
            system.debug('VITILcareMyProfileCtlr updateContact newPassword: '+newPassword);
            system.debug('VITILcareMyProfileCtlr updateContact confirmPassword: '+confirmPassword);                
            if(oldPassword != '' || newPassword != '' || confirmPassword != '') {
                if(!VITILcareChangePasswordCtlr.isStrongPassword(newPassword)) {
                    weakPassword = label.mbrWeakPasswordError;
                    system.debug('VITILcareMyProfileCtlr updateContact weakPassword: '+weakPassword);
                    system.debug('VITILcareMyProfileCtlr updateContact isStrongPassword: '+newPassword+':'+confirmPassword);
                    return null;
                }
        
                if (newPassword != confirmPassword) {
                    passwordNotMatch = label.mbrPasswordDoNotMatchError;
                    system.debug('VITILcareMyProfileCtlr updateContact isvalidpassword: '+newPassword+':'+confirmPassword);
                    return null;
                }
        
                system.debug('VITILcareMyProfileCtlr updateContact Userinfo.getUserName(): '+Userinfo.getUserName());
                system.debug('VITILcareMyProfileCtlr updateContact Userinfo.getUserEmail(): '+Userinfo.getUserEmail());        
                
                system.debug(String.format('VITILcareMyProfileCtlr Site.changePassword(newPassword, confirmPassword, oldPassword)', new List<String>{newPassword, confirmPassword, oldPassword}));        
                temp = Site.changePassword(newPassword, confirmPassword, oldPassword);
                System.debug('VITILcareMyProfileCtlr updateContact temp changePasswordResult: '+temp);
            }
    	}
    
        //if (!ApexPages.getMessages().isEmpty()) {
        if (ApexPages.hasMessages()) {
            // Hack to get around a weird problem where pageMessages component
            // does not render messages generated by Site.changePassword method.
            errors = new String[]{};
            for (ApexPages.Message msg : ApexPages.getMessages()) {
                errors.add(msg.getDetail());
            }
        }
                
        if(temp==null && errors!=null){	
System.debug('Site.changePassword error: ' + errors);            
            if(errors[0].contains('You cannot reuse this old password')){
                weakPassword = label.mbrCannotReusePassword;
            }
            else if(errors[0].contains('Your old password is invalid')){
                oldPasswordError = label.mbrOldPasswordError;
            }
            else {
                weakPassword = errors[0];
            }
        }
        else {
            profileUpdateComplete = label.mbrProfileUpdated;        
            PageReference pref = Page.VITILcareMyProfile;
            pref.getParameters().put('m', 'completed');
            if (editMode != null && editMode.length() > 0) return pref;
        }
        
        return null;
    }   
    
    public PageReference cancelUpdate(){
    	return Page.VITILcareMyProfile;
    } 
   
  public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('en_US',label.VitilcarePortalAccess_EnglishLabel)); 
        options.add(new SelectOption('fr',label.VITILcarePortalAccess_FrenchLabel)); 
        return options; 
    }
                   
    public String getLanguageType() {
        return LanguageType;
    }
                    
    public void setLanguageType(String LanguageType) { this.LanguageType = LanguageType; }
}