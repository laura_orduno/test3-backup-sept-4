/**
 * @author Steve Doucette, Traction on Demand 
 * @date 2018-06-15
 */
public with sharing class PurgerUtils {
	private static final String APEX_CLASS_AND_MESSAGE = 'Purger Batch Alert System';

	/*public static List<String> adminEmails {
		get {
			if (adminEmails == null) {
				adminEmails = new List<String>();
				for (User u : [
						SELECT Id
						FROM User
						WHERE IsActive = TRUE AND Profile.Name = :PortalConstants.PROFILE_SYSTEM_ADMINISTRATOR
				]) {
					adminEmails.add(u.Id);
				}
			}
			return adminEmails;
		}
		set;
	}

	// Email admins
	public static void emailAdmins(String body, String subject, String displayName) {
		sendEmail(body, subject, displayName);
	}

	public static void emailAdmins(Exception ex, String subject, String displayName) {
		sendEmail('Error Occured: ' + '\n' + ex.getMessage() + '\n' + ex.getStackTraceString(), subject, displayName);
	}

	private static void sendEmail(String body, String subject, String displayName) {
		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
		email.setHtmlBody(body);
		email.setToAddresses(adminEmails);
		email.setSubject(subject);
		email.setSenderDisplayName(displayName);
		Messaging.sendEmail(new Messaging.SingleEmailMessage[]{
				email
		});
	}*/

	// Create Webservice Integration Log record
	public static void log(Exception ex) {
		insert new Webservice_Integration_Error_Log__c(
				Apex_Class_and_Method__c = APEX_CLASS_AND_MESSAGE,
				Error_Code_and_Message__c = ex.getMessage(),
				Stack_Trace__c = ex.getStackTraceString()
		);
	}

	public static void log(String information) {
		insert new Webservice_Integration_Error_Log__c(
				Apex_Class_and_Method__c = APEX_CLASS_AND_MESSAGE,
				Error_Code_and_Message__c = information
		);
	}
}