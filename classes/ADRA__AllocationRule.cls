/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class AllocationRule {
    global List<ADRA.AllocationRule.Assignee> assignees;
    global Boolean manual {
        get;
        set;
    }
    global AllocationRule(ADRA__AllocationRule__c target) {

    }
    global void addAssignee(ADRA.AllocationRule.Assignee assignee) {

    }
    global void addAssignees(List<ADRA.AllocationRule.Assignee> assignees) {

    }
global class Assignee {
    global User user {
        get;
        set;
    }
    global Assignee(SObject target) {

    }
}
}
