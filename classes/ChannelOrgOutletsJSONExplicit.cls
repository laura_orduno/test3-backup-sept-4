public class ChannelOrgOutletsJSONExplicit {
    
    public String currentChannelOutletID {get;set;}
    public String currentChannelOutletDescription {get;set;}
    public List<String> currentOutletCategoryKeys {get;set;}
    
    public static List<ChannelOrgOutletsJSONExplicit> parse(String json) {
        return (List<ChannelOrgOutletsJSONExplicit>) System.JSON.deserialize(json, List<ChannelOrgOutletsJSONExplicit>.class);
    }
}