/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class VideoDetailController {
    @RemoteAction
    global static Integer checkAndPlayCopiedVideo(String videoUrl) {
        return null;
    }
    global static String checkFileAlreadyProcessed(String videoUrl) {
        return null;
    }
    @RemoteAction
    global static void deleteQuiz(String quizId) {

    }
    @RemoteAction
    global static LRN.VideoDetailController.Result deleteQuizWithResult(String quizId) {
        return null;
    }
    @RemoteAction
    global static Integer deleteVideosFromLurnitureBucket(String videoUrl) {
        return null;
    }
    global static Integer encodeVideo(String videoUrl) {
        return null;
    }
    @RemoteAction
    global static List<Opportunity> getOpportunitiesToAssign(List<String> opprIds) {
        return null;
    }
    global static List<Opportunity> getOpportunitiesToRelateWithVideo(String param, String videoId) {
        return null;
    }
    global static List<LRN.VideoDetailController.UserWrapper> getUser(String searchTerm, String ownerId) {
        return null;
    }
    global static List<Opportunity> opportunityLookupSearch(String param) {
        return null;
    }
    @RemoteAction
    global static void savePopQuizQuestionOptions(LRN.PopQuizBuilderController.PopQuizWrapper quizWrapper) {

    }
    global static Integer saveQuizQuestionResponse(List<LRN.PopQuizBuilderController.PopQuizQuestion> questions, String quizId) {
        return null;
    }
    global static LRN.VideoDetailController.IdeaResult sendIdea(String userIdea) {
        return null;
    }
    @RemoteAction
    global static void updateQuizStatus(String quizId, String status) {

    }
    @RemoteAction
    global static Boolean updateVideoDuration(String videoId, Integer duration) {
        return null;
    }
    @RemoteAction
    global static Integer updateWithEncodedVideo(String videoId, String videoUrl) {
        return null;
    }
    global static List<LRN__Lurniture_User__c> userLookupSearch(String param, String videoId, Boolean canRemove) {
        return null;
    }
global class IdeaResult {
}
global class Result {
    global Result() {

    }
}
global class UserWrapper {
}
}
