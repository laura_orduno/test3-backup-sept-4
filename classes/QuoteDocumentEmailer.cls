public class QuoteDocumentEmailer {
    public Boolean ccCurrentUser {get; set;}
    public Id quoteId {get; set;}
    private Blob pdf;
    
    public QuoteDocumentEmailer(Blob pdf) {
        this.pdf = pdf;
        ccCurrentUser = false;
    }
    
    public void email(String fileName, Set<Id> contactIds) {
        Messaging.EmailFileAttachment att = new Messaging.EmailFileAttachment();
        att.setBody(pdf);
        att.setContentType('application/pdf');
        att.setFileName(fileName + '.pdf');
        att.setInline(true);
        
        Messaging.SingleEmailMessage[] emails = new Messaging.SingleEmailMessage[]{};
        for (Id contactId : contactIds) {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            if (Quote_Portal__c.getInstance().Email_Address_Id__c != null) {
                mail.setOrgWideEmailAddressId(Quote_Portal__c.getInstance().Email_Address_Id__c);
            }
            mail.setTargetObjectId(contactId);
            if (quoteId != null) {
                mail.setWhatId(quoteId);
            }
            mail.setTemplateId(Quote_Portal__c.getInstance().Email_Template_Id__c);
            mail.setSaveAsActivity(false);
            mail.setFileAttachments(new Messaging.EmailFileAttachment[]{att});
            emails.add(mail);
        }
        
        if (ccCurrentUser) {
            Messaging.SingleEmailMessage cc = new Messaging.SingleEmailMessage();
            if (Quote_Portal__c.getInstance().Email_Address_Id__c != null) {
                cc.setOrgWideEmailAddressId(Quote_Portal__c.getInstance().Email_Address_Id__c);
            }
            cc.setTargetObjectId(UserInfo.getUserId());
            cc.setTemplateId(Quote_Portal__c.getInstance().Email_Template_Id__c);
            cc.setSaveAsActivity(false);
            cc.setFileAttachments(new Messaging.EmailFileAttachment[]{att});
            emails.add(cc);
        }
        
        Messaging.sendEmail(emails);
    }
    
}