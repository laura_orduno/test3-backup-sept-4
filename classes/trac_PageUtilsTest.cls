/*
 * Tests for trac_PageUtils
 *
 * @author: Grant Adamson, Traction On Demand 
 */
@isTest
private class trac_PageUtilsTest {
	private static testMethod void testParamUtils() {
		String paramName = 'testParam';
		String paramVal = 'testValue';
		system.assert(trac_PageUtils.isParamNull(paramName));
		system.assertNotEquals(true,trac_PageUtils.isParamNotNull(paramName));
		
		trac_PageUtils.setParam(paramName,paramVal);
		system.assertNotEquals(true,trac_PageUtils.isParamNull(paramName));
		system.assert(trac_PageUtils.isParamNotNull(paramName));
		system.assertEquals(paramVal,trac_PageUtils.getParam(paramName));
	}
	
	private static testMethod void testIsValidEmail() {
		final String VALID = 'test@example.com';
		final String INVALID = 'test@example.com?';
		
		system.assert(trac_PageUtils.isValidEmail(VALID));
		system.assertNotEquals(true,trac_PageUtils.isValidEmail(INVALID));
		system.assertNotEquals(true,trac_PageUtils.isValidEmail(null));
	}
	
	private static testMethod void testAddError() {
		String errorString = 'This is an error';
		trac_PageUtils.addError(errorString);
		system.assert(ApexPages.hasMessages(ApexPages.Severity.ERROR));
		system.assertEquals(errorString,ApexPages.getMessages()[0].getSummary());
	}
	
	private static testMethod void testAddWarning() {
		String warningString = 'This is a warning';
		trac_PageUtils.addWarning(warningString);
		system.assert(ApexPages.hasMessages(ApexPages.Severity.WARNING));
		system.assertEquals(warningString,ApexPages.getMessages()[0].getSummary());
	}
	
	private static testMethod void testAddInfo() {
		String infoString = 'This is an info message';
		trac_PageUtils.addInfo(infoString);
		system.assert(ApexPages.hasMessages(ApexPages.Severity.INFO));
		system.assertEquals(infoString,ApexPages.getMessages()[0].getSummary());
	}
}