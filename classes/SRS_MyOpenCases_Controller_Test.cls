/* Developed By: Hari Neelalaka, IBM
* Created Date: 22-Dec-2014
*/

@isTest

private class SRS_MyOpenCases_Controller_Test {

    static testMethod void SRS_MyOpenCases_Controller_test() {
        // creating an account 
        Account acc = new Account();
        acc.Name = 'test';
        insert acc;
        // creating list of cases referencing an account created above
        List<case> lstcases=new List<case>();
        case cs = new case();
        cs.AccountId = acc.Id;
        cs.Status='Open'; 
        cs.ownerid=Userinfo.getUserId();
        lstcases.add(cs);
        lstcases.add(new case(AccountId=acc.id));
        insert lstcases;
        // Instantiate controller "SRS_MyOpenCases_Controller"
        SRS_MyOpenCases_Controller openCases = new SRS_MyOpenCases_Controller();        
        openCases.sortDir='ASC';
        openCases.sortBy='createddate';
        List<SObject> resultList = new List<SObject>();
        // Map resultList to the object
        Map<Object, List<SObject>> objectMap = new Map<Object, List<SObject>>();
        List<Object> keys = new List<Object>(objectMap.keySet());
        //Run the code to test
        Test.startTest();
        openCases.empty(); 
        openCases.sortList(lstCases);
        Test.stopTest();
    }
}