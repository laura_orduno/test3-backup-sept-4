@istest
public class BusinessCaseRadarExtensionTest {

    static testMethod void testController(){
        Case TestCase=new Case();
        insert TestCase;
        BusinessCase__c busCase = new BusinessCase__c(case__c=TestCase.id);
        insert busCase;
        
    	PageReference pageRef= Page.BusinessCaseRadar;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc= new ApexPages.StandardController(busCase);
        
        BusinessCaseRadarExtension busExtension = new BusinessCaseRadarExtension(sc);
		System.assertEquals(busCase.Id, busExtension.businessCaseID);
        
        //Test whether there is something in the FieldSetList (otherwise there is other problems)
        system.assert(busExtension.getFields().size()>0);

        //Test that data is returned based on the fields being returned
        system.assert(busExtension.getData().size()>0);
    }
    
}