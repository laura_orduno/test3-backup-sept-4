@isTest
public class OCOM_Asset_TriggerHelper_Test{

public static testMethod void testMethodOne()
    {
        
       Schema.DescribeSObjectResult d = Schema.SObjectType.Account; 
          Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
          Schema.RecordTypeInfo rtByName =  rtMapByName.get('CAN');
          system.debug(' id'+rtByName.getRecordTypeId());
          Id recTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Billing').getRecordTypeId();
          Id recSerTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
          Id recRCIDTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId();
        
          Test.startTest();
          Account RCIDacc = new Account(name='debs',recordtypeid=recRCIDTypeId);
          Account acc = new Account(name='Billing account',BAN_CAN__c='draft',recordtypeid=recTypeId);
          Account seracc = new Account(name='123 street',recordtypeid=recSerTypeId,parentid=RCIDacc.id);
          
          
          List<Account> acclist= new List<Account>();          
          acclist.add(RCIDacc);
          acclist.add(acc);
          acclist.add(seracc );
          insert acclist;
          
          smbcare_address__c address= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N5',FMS_Address_ID__c='987651',account__c=seracc.id,service_account_id__c=seracc.id); 
         insert address;
          
           // First, set up test price book entries.
        // Insert a test product.
        Product2 prod = new Product2(Name = 'Laptop X200', 
            Family = 'Hardware');
        insert prod;
         System.assertEquals(prod.name, 'Laptop X200');
        // Get standard price book ID.
        // This is available irrespective of the state of SeeAllData.
        Id pricebookId = Test.getStandardPricebookId();
        
        // 1. Insert a price book entry for the standard price book.
        // Standard price book entries require the standard price book ID we got earlier.
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
       
        // Create a custom price book
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        // 2. Insert a price book entry with a custom price.
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        contact c= new contact(firstname='abc',lastname='xyz');
        insert c;
         order ord = new order(Ban__c='draft',accountId=acc.id,effectivedate=system.today(),status='not submitted',Pricebook2Id=customPB.id,CustomerAuthorizedById=c.id,service_address__c=address.id);   
         order ord2 = new order(Ban__c='draft',accountId=acc.id,effectivedate=system.today(),status='not submitted',Pricebook2Id=customPB.id,CustomerAuthorizedById=c.id,service_address__c=address.id);   
         List<Order>ordlist= new List<Order>();   
         ordList.add(ord);ordList.add(ord2);
          insert ordList; 
          System.assertEquals(ordList.size(), 2);
          orderItem  OLI2= new orderItem(UnitPrice=12,orderId= ord2.Id,orderMgmt_BPI_Id__c='9871379163517',vlocity_cmt__BillingAccountId__c=acc.id,vlocity_cmt__ServiceAccountId__c=seracc.id,Quantity=2,PricebookEntryId=customPrice.id);
          orderItem  OLI= new orderItem(UnitPrice=12,orderId= ord.Id,vlocity_cmt__AssetReferenceId__c=OLI2.id,vlocity_cmt__BillingAccountId__c=acc.id,vlocity_cmt__ServiceAccountId__c=seracc.id,Quantity=2,PricebookEntryId=customPrice.id);
          
          List<OrderItem>OLIlist= new List<OrderItem>();   
          OLIList.add(OLI2);OLIList.add(OLI);
          insert OLIList;
		System.assertEquals(OLIList.size(), 2);
        
		  system.debug('Id  aAS'+String.valueOf(OLI.id).substring(0,15)+' '+OLI.id);
		   Asset testassetRec = new Asset(name='Asset1',accountId=RCIDacc.id,vlocity_cmt__AssetReferenceId__c=String.valueOf(OLI.id).substring(0,15),vlocity_cmt__BillingAccountId__c=null,orderMgmtId__c=null,vlocity_cmt__OrderId__c=null,vlocity_cmt__OrderProductId__c=null,vlocity_cmt__ServiceAccountId__c=null);
        insert testassetRec;
        testassetRec.Status='Active';
        testassetRec.vlocity_cmt__ProvisioningStatus__c='Changed';
        update testassetRec;
        System.assertEquals(testassetRec.name, 'Asset1');
		   

           
           Test.stopTest();
    }
  
}