@isTest
private class OCOM_OrderSubmitController_Contract_Test {
    
    @isTest private static void submitOrderContractTest1() {
        Profile p = [select id from profile where name='Standard User'];
        User testUser = new User(alias = 'test123', email='ocom_test123qwert@telus.com', emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', localesidkey='en_US', profileid = p.Id, country='Canada', timezonesidkey='America/Los_Angeles', username='ocom_test123qwert@telus.com');
        insert testUser;
        
        String orderId= OrdrTestDataFactory.singleMethodForOrderId();
        Order ordObj=[select Account.Credit_Profile__c,AccountId, Credit_Check_Not_Required__c,RequestedDate__C,status,Rush__c,Ban__c,Shipping_Address__c,orderMgmtId__c FROM Order where id=:orderId];
        ordObj.orderMgmtId__c='OS1234567890';
        ordObj.status='Not Submitted';
        ordObj.Rush__c=true;
        ordObj.Ban__c='12345';
        ordObj.Shipping_Address__c='test address';
        ordObj.Credit_Check_Not_Required__c=false;
        Date day1 = Date.today();
        Date todayPlus = day1.addDays(4);
        ordObj.RequestedDate__C=todayPlus;
        ordObj.Sales_Representative__c=testUser.id;
        update ordObj;
        
        Account anAccount = [select Account_Status__c from Account where id=:ordObj.AccountId limit 1];
        anAccount.Account_Status__c = 'ACTIVE';
        update anAccount;
        
       
        
        OrderItem anOrderItem=[select vlocity_cmt__RecurringTotal__c, vlocity_cmt__OneTimeTotal__c from OrderItem where OrderId=:orderId limit 1];
        anOrderItem.vlocity_cmt__RecurringTotal__c=400.00;
        anOrderItem.vlocity_cmt__OneTimeTotal__c=0.00;
        update anOrderItem;
                
        Test.startTest();
        
        PageReference pageref = Page.OCOM_SubmitOrder;
        Test.setCurrentPage(pageref);        
        pageref.getparameters().put('id', ordObj.Id);
        ApexPages.StandardController stdController = new ApexPages.standardController(ordObj);
        OCOM_OrderSubmitController orderSubmitController = new OCOM_OrderSubmitController(stdController);

        orderSubmitController.submitCustomerOrder();
        //Test code for Contract Piece
        orderSubmitController.creditStatus = 'Completed';
        orderSubmitController.areContractable = true;
        //orderSubmitController.onLoadActions();
        Object outMap = OCOM_OrderSubmitController.callContractCreationService(ordObj.id, 'Not Required',true,'false', false);
        Map<String,Object> outResult = (Map<String, Object>)outMap;

        System.debug('OutMap........'+ outResult.get('isSuccess'));

       System.assertEquals(false, (Boolean)outResult.get('isSuccess'));

        Test.stopTest();
     }
    
    @isTest static void submitOrderContractTest2() {
        Profile p = [select id from profile where name='Standard User'];
        User testUser = new User(alias = 'test123', email='ocom_test123qwert@telus.com', emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', localesidkey='en_US', profileid = p.Id, country='Canada', timezonesidkey='America/Los_Angeles', username='ocom_test123qwert@telus.com');
        insert testUser;
        
        String orderId= OrdrTestDataFactory.singleMethodForOrderId();
        

        Order ordObj=[select Account.Credit_Profile__c,AccountId, Credit_Check_Not_Required__c,RequestedDate__C,status,Rush__c,Ban__c,Shipping_Address__c,orderMgmtId__c FROM Order where id=:orderId];
        ordObj.orderMgmtId__c='OS1234567890';
        ordObj.status='Not Submitted';
        ordObj.Rush__c=true;
        ordObj.Ban__c='12345';
        ordObj.Shipping_Address__c='test address';
        ordObj.Credit_Check_Not_Required__c=false;
        Date day1 = Date.today();
        Date todayPlus = day1.addDays(4);
        ordObj.RequestedDate__C=todayPlus;
        ordObj.Sales_Representative__c=testUser.id;
        update ordObj;
        
        TestDataHelper.testContractCreation();
         TestDataHelper.testContractObj.vlocity_cmt__OrderId__c = ordObj.id;
        
        update TestDataHelper.testContractObj;
                Test.startTest();
        TestDataHelper.testContractVersionCreation();
		
        
        
        Account anAccount = [select Account_Status__c from Account where id=:ordObj.AccountId limit 1];
        anAccount.Account_Status__c = 'ACTIVE';
        update anAccount;
        
       
        
        OrderItem anOrderItem=[select vlocity_cmt__RecurringTotal__c, vlocity_cmt__OneTimeTotal__c from OrderItem where OrderId=:orderId limit 1];
        anOrderItem.vlocity_cmt__RecurringTotal__c=400.00;
        anOrderItem.vlocity_cmt__OneTimeTotal__c=0.00;
        update anOrderItem;
                        
        
        
        PageReference pageref = Page.OCOM_SubmitOrder;
        Test.setCurrentPage(pageref);        
        pageref.getparameters().put('id', ordObj.Id);

        ApexPages.StandardController stdController = new ApexPages.standardController(ordObj);
        OCOM_OrderSubmitController orderSubmitController = new OCOM_OrderSubmitController(stdController);

        orderSubmitController.submitCustomerOrder();
        //Test code for Contract Piece
        orderSubmitController.creditStatus = 'Completed';
        orderSubmitController.areContractable = true;
        //orderSubmitController.onLoadActions();
        Object outMap = OCOM_OrderSubmitController.callContractCreationService(ordObj.id, 'Not Required',true,'false', false);
        Map<String,Object> outResult = (Map<String, Object>)outMap;

        System.debug('OutMap222........'+ outResult.get('isSuccess'));

        System.assertEquals(true, (Boolean)outResult.get('isSuccess'));
         vlocity_cmt.FlowStaticMap.FlowMap.put('SendDocumentResponse','SUCCESS');
         String sendDcoumentResponse = OCOM_OrderSubmitController.sendDocument(TestDataHelper.testContractObj.id, ordObj.Id);
        //Map<String,Object> outResult = (Map<String, Object>)outMap;

        System.debug('sendDocumentResponse........'+ sendDcoumentResponse);
        System.assertEquals('SUCCESS', sendDcoumentResponse);


        Test.stopTest();
    }

@isTest static void sendDocumentContractTest_negative() {
        Profile p = [select id from profile where name='Standard User'];
        User testUser = new User(alias = 'test123', email='ocom_test123qwert@telus.com', emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', localesidkey='en_US', profileid = p.Id, country='Canada', timezonesidkey='America/Los_Angeles', username='ocom_test123qwert@telus.com');
        insert testUser;
        
        String orderId= OrdrTestDataFactory.singleMethodForOrderId();
        Order ordObj=[select Account.Credit_Profile__c,AccountId, Credit_Check_Not_Required__c,RequestedDate__C,status,Rush__c,Ban__c,Shipping_Address__c,orderMgmtId__c FROM Order where id=:orderId];
        ordObj.orderMgmtId__c='OS1234567890';
        ordObj.status='Not Submitted';
        ordObj.Rush__c=true;
        ordObj.Ban__c='12345';
        ordObj.Shipping_Address__c='test address';
        ordObj.Credit_Check_Not_Required__c=false;
        Date day1 = Date.today();
        Date todayPlus = day1.addDays(4);
        ordObj.RequestedDate__C=todayPlus;
        ordObj.Sales_Representative__c=testUser.id;
        update ordObj;
        
    	Test.startTest();
        TestDataHelper.testContractCreation();
        // TestDataHelper.testContractObj.vlocity_cmt__OrderId__c = ordObj.id;
        //update TestDataHelper.testContractObj;
		
    	
        Account anAccount = [select Account_Status__c from Account where id=:ordObj.AccountId limit 1];
        anAccount.Account_Status__c = 'ACTIVE';
        update anAccount;

        OrderItem anOrderItem=[select vlocity_cmt__RecurringTotal__c, vlocity_cmt__OneTimeTotal__c from OrderItem where OrderId=:orderId limit 1];
        anOrderItem.vlocity_cmt__RecurringTotal__c=400.00;
        anOrderItem.vlocity_cmt__OneTimeTotal__c=0.00;
        update anOrderItem;

                
        
        PageReference pageref = Page.OCOM_SubmitOrder;
        Test.setCurrentPage(pageref);        
        pageref.getparameters().put('id', ordObj.Id);
        ApexPages.StandardController stdController = new ApexPages.standardController(ordObj);
        OCOM_OrderSubmitController orderSubmitController = new OCOM_OrderSubmitController(stdController);

        orderSubmitController.submitCustomerOrder();
        //Test code for Contract Piece
        orderSubmitController.creditStatus = 'Completed';
        vlocity_cmt.FlowStaticMap.FlowMap.put('SendDocumentResponse','Failure');
        String sendDcoumentResponse = OCOM_OrderSubmitController.sendDocument(TestDataHelper.testContractObj.id, ordObj.id);
        //Map<String,Object> outResult = (Map<String, Object>)outMap;

        System.debug('sendDocumentResponse222........'+ sendDcoumentResponse);
        //System.assertEquals('SUCCESS', sendDcoumentResponse);
        //System.assertEquals(true, (Boolean)outResult.get('isSuccess'));

        Test.stopTest();
    }

@isTest static void manualContractFlowTest() {
        
      
        Profile p = [select id from profile where name='Standard User'];
        User testUser = new User(alias = 'test123', email='ocom_test123qwert@telus.com', emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', localesidkey='en_US', profileid = p.Id, country='Canada', timezonesidkey='America/Los_Angeles', username='ocom_test123qwert@telus.com');
        insert testUser;
        
        String orderId= OrdrTestDataFactory.singleMethodForOrderId();
        Order ordObj=[select Account.Credit_Profile__c,AccountId, Credit_Check_Not_Required__c,RequestedDate__C,status,Rush__c,Ban__c,Shipping_Address__c,orderMgmtId__c FROM Order where id=:orderId];
        ordObj.orderMgmtId__c='OS1234567890';
        ordObj.status='Not Submitted';
        ordObj.Rush__c=true;
        ordObj.Ban__c='12345';
        ordObj.Shipping_Address__c='test address';
        ordObj.Credit_Check_Not_Required__c=false;
        Date day1 = Date.today();
        Date todayPlus = day1.addDays(4);
        ordObj.RequestedDate__C=todayPlus;
        ordObj.Sales_Representative__c=testUser.id;
        update ordObj;
        
        TestDataHelper.testContractCreation();
        TestDataHelper.testContractVersionCreation();
		
    	Test.startTest();
        attachment a = new attachment(parentID =  TestDataHelper.testContractVersionObj.id);
        a.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        a.body=bodyBlob;
        insert a;

        TestDataHelper.testContractObj.vlocity_cmt__OrderId__c = ordObj.id;
        update TestDataHelper.testContractObj;
		
    	
        Account anAccount = [select Account_Status__c from Account where id=:ordObj.AccountId limit 1];
        anAccount.Account_Status__c = 'ACTIVE';
        update anAccount;

        OrderItem anOrderItem=[select vlocity_cmt__RecurringTotal__c, vlocity_cmt__OneTimeTotal__c from OrderItem where OrderId=:orderId limit 1];
        anOrderItem.vlocity_cmt__RecurringTotal__c=400.00;
        anOrderItem.vlocity_cmt__OneTimeTotal__c=0.00;
        update anOrderItem;

        PageReference pageref = Page.OCOM_SubmitOrder;
        Test.setCurrentPage(pageref);        
        pageref.getparameters().put('id', ordObj.Id);
        ApexPages.StandardController stdController = new ApexPages.standardController(ordObj);
        OCOM_OrderSubmitController orderSubmitController = new OCOM_OrderSubmitController(stdController);

        orderSubmitController.submitCustomerOrder();
        //Test code for Contract Piece
        orderSubmitController.creditStatus = 'Completed';
        vlocity_cmt.FlowStaticMap.FlowMap.put('SendDocumentResponse','Failure');
        String sendDcoumentResponse = OCOM_OrderSubmitController.contractManualFlow( ordObj.id);
        //Map<String,Object> outResult = (Map<String, Object>)outMap;

        System.debug('sendDocumentResponse222........'+ sendDcoumentResponse);
        //System.assertEquals('SUCCESS', sendDcoumentResponse);
        //System.assertEquals(true, (Boolean)outResult.get('isSuccess'));

        Test.stopTest();
    }

    @isTest static void manualContractFlowTest_negative() {
  
        Profile p = [select id from profile where name='Standard User'];
        User testUser = new User(alias = 'test123', email='ocom_test123qwert@telus.com', emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', localesidkey='en_US', profileid = p.Id, country='Canada', timezonesidkey='America/Los_Angeles', username='ocom_test123qwert@telus.com');
        insert testUser;
        
        String orderId= OrdrTestDataFactory.singleMethodForOrderId();
        Order ordObj=[select Account.Credit_Profile__c,AccountId, Credit_Check_Not_Required__c,RequestedDate__C,status,Rush__c,Ban__c,Shipping_Address__c,orderMgmtId__c FROM Order where id=:orderId];
        ordObj.orderMgmtId__c='OS1234567890';
        ordObj.status='Not Submitted';
        ordObj.Rush__c=true;
        ordObj.Ban__c='12345';
        ordObj.Shipping_Address__c='test address';
        ordObj.Credit_Check_Not_Required__c=false;
        Date day1 = Date.today();
        Date todayPlus = day1.addDays(4);
        ordObj.RequestedDate__C=todayPlus;
        ordObj.Sales_Representative__c=testUser.id;
        update ordObj;
        
        TestDataHelper.testContractCreation();
        Test.startTest();
        TestDataHelper.testContractVersionCreation();

        attachment a = new attachment(parentID =  TestDataHelper.testContractVersionObj.id);
        a.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        a.body=bodyBlob;
        insert a;

        //TestDataHelper.testContractObj.vlocity_cmt__OrderId__c = ordObj.id;
        //update TestDataHelper.testContractObj;
		
        
        Account anAccount = [select Account_Status__c from Account where id=:ordObj.AccountId limit 1];
        anAccount.Account_Status__c = 'ACTIVE';
        update anAccount;

        OrderItem anOrderItem=[select vlocity_cmt__RecurringTotal__c, vlocity_cmt__OneTimeTotal__c from OrderItem where OrderId=:orderId limit 1];
        anOrderItem.vlocity_cmt__RecurringTotal__c=400.00;
        anOrderItem.vlocity_cmt__OneTimeTotal__c=0.00;
        update anOrderItem;

        PageReference pageref = Page.OCOM_SubmitOrder;
        Test.setCurrentPage(pageref);        
        pageref.getparameters().put('id', ordObj.Id);
        ApexPages.StandardController stdController = new ApexPages.standardController(ordObj);
        OCOM_OrderSubmitController orderSubmitController = new OCOM_OrderSubmitController(stdController);

        orderSubmitController.submitCustomerOrder();
        //Test code for Contract Piece
        orderSubmitController.creditStatus = 'Completed';
        vlocity_cmt.FlowStaticMap.FlowMap.put('SendDocumentResponse','Failure');
        String sendDcoumentResponse = OCOM_OrderSubmitController.contractManualFlow( ordObj.id);
        //Map<String,Object> outResult = (Map<String, Object>)outMap;

        System.debug('sendDocumentResponse222........'+ sendDcoumentResponse);
        //System.assertEquals('SUCCESS', sendDcoumentResponse);
        //System.assertEquals(true, (Boolean)outResult.get('isSuccess'));

        Test.stopTest();
    }


    
}