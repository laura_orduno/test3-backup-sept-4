/*
 * Wrapper for RTS Repair classes generated from TELUS-supplied WSDL
 * The trac_Repair*Ctlr classes use this class to interact with RTS
 * Part of the SMB Repairs project
 *
 * @author: Grant Adamson, Traction On Demand
 
Change Log:
July 8, 2014 - Andy Leung
-  Updated physical damage to change field type from boolean to picklist.
 */

public without sharing class trac_RepairCallout {
	public enum Endpoint {VALIDATE, INIT_CREATE_REPAIR, GET_REPAIR, GET_REPAIR_STATUS, UPDATE_QUOTE_STATUS }
	private static final String ENGLISH_LOCALE = 'en_CA';
	private static final String FRENCH_LOCALE = 'fr_CA';
	private static final String PRODUCTION_ORG_ID = '00D3000000006o0EAA';
	
	public static trac_RepairCalloutResponse validate(Case c) {
		Repair_WS_Endpoints__c endpointDetail; 
		
		try {
			endpointDetail = getEndpoint(Endpoint.VALIDATE);
		} catch (Exception e) {
			return new trac_RepairCalloutResponse(e);
		}
		
		String defectiveSerialNumber = c.ESN_MEID_IMEI__c;
		String defectiveTelusSku = c.Accessory_SKU__c;
		String repairTypeCd = c.Repair_Type__c;
		String telephoneNumber = c.Mobile_number__c;
		String locale = getCleanLocale();
		
		svc_EquipmentRepairValidationService_2.RepairRequestValidationSvcPort port =
			new svc_EquipmentRepairValidationService_2.RepairRequestValidationSvcPort();
		
		port.endpoint_x = endpointDetail.Endpoint__c;
		
		if(endpointDetail.timeout__c != null) {
			port.timeout_x = endpointDetail.timeout__c.intValue();
		}
		
		if(endpointDetail.username__c != null) {
			String authHeader = buildBasicAuthHeader(endpointDetail.username__c, endpointDetail.password__c);   
			port.inputHttpHeaders_x = new Map<String,String>{'Authorization' => authHeader};
		}
		
		port.lenientParsing_x = true;
		
		trac_RepairCalloutResponse cr;
		svc_RepairRequestValidationSvcRequestR_2.validateRepairRequestResponse_element resp;
		
		try {
			
			// TEST RESPONSES //////////////////////////////////////////////////
			if(Test.isRunningTest()) {
				if(defectiveSerialNumber == trac_RepairTestUtils.REPAIR_ESN_WARNING) {
					resp = trac_svcPseudoMocks.validateWarning();
				} else if(defectiveSerialNumber == trac_RepairTestUtils.REPAIR_ESN_NOT_FOUND) {
					resp = trac_svcPseudoMocks.validateFailureProductNotFound();
				} else if(defectiveSerialNumber == trac_RepairTestUtils.REPAIR_ESN_NO_MATCH) {
					resp = trac_svcPseudoMocks.validateFailureESNPhoneNoMatch();
				} else if(defectiveSerialNumber == trac_RepairTestUtils.REPAIR_ESN_EXCEPTION) {
					throw new TestException('It Broke');
				} else {
					resp = trac_svcPseudoMocks.validateSuccess();
				}
			} else {
			///////////////////////////////////////////////////////////////////
			
				resp = port.validateRepairRequest(defectiveSerialNumber, defectiveTelusSku, repairTypeCd, telephoneNumber, locale);
			}
		} catch (Exception e) {
			return new trac_RepairCalloutResponse(e);
		}
		
		cr = new trac_RepairCalloutResponse(resp);
		
		if(cr.success) {
			trac_WSFieldMapper.validateResultToCase(resp, c);
		}
		
		return cr;
	}
	
	
	public static trac_RepairCalloutResponse initCreateRepair(Case c) {
		Repair_WS_Endpoints__c endpointDetail; 
		
		try {
			endpointDetail = getEndpoint(Endpoint.INIT_CREATE_REPAIR);
		} catch (Exception e) {
			return new trac_RepairCalloutResponse(e);
		}
		
		rpm_EquipmentRepairManagementService_1.EquipmentRepairManagementServicePort port = 
			new rpm_EquipmentRepairManagementService_1.EquipmentRepairManagementServicePort();
		
		port.endpoint_x = endpointDetail.Endpoint__c;
		system.debug('Port endpoint:'+port.endpoint_x);
		if(endpointDetail.timeout__c != null) {
			port.timeout_x = endpointDetail.timeout__c.intValue();
		}
		
		if(endpointDetail.username__c != null) {
			String authHeader = buildBasicAuthHeader(endpointDetail.username__c, endpointDetail.password__c);   
			port.inputHttpHeaders_x = new Map<String,String>{'Authorization' => authHeader};
		}
		
		//port.lenientParsing_x = true;
		
		String salesForceCaseID = c.Id;
		String salesForceCaseNumber = c.CaseNumber;
		
		rpm_CustomerExternalViewCommon_v5.Name clientName = new rpm_CustomerExternalViewCommon_v5.Name();
		clientName.title = c.Customer_Title_client__c;
		clientName.firstName = c.Customer_First_Name_client__c;
		clientName.lastName = c.Customer_Last_Name_client__c;
		
		String serialNumber = c.ESN_MEID_IMEI__c;
		String mobileNumber = c.Mobile_number__c;
		String accessorySKU = c.Accessory_SKU__c;
		
		rpm_CustomerExternalViewCommon_v5.Name contactName = new rpm_CustomerExternalViewCommon_v5.Name();
		contactName.title = c.Customer_Title_contact__c;
		contactName.firstName = c.Customer_First_Name_contact__c;
		contactName.lastName = c.Customer_Last_Name_contact__c;
		
		String contactNumber = c.Customer_Phone_contact__c;
		String contactEmailAddress = c.Customer_Email_contact__c;
		
		rpm_TelusCommonAddressTypes_v4.Address clientShippingAddress = new rpm_TelusCommonAddressTypes_v4.Address();
		clientShippingAddress.streetName = c.Address_shipping__c;
		clientShippingAddress.municipalityName = c.City_shipping__c;
		clientShippingAddress.provinceStateCode = c.Province_shipping__c;
		clientShippingAddress.postalZipCode = c.Postal_Code_shipping__c;
		
                String physicalDamage=c.physical_damage__c;
                Boolean physicalDamageInd = (string.isnotblank(physicalDamage)&&physicalDamage.equalsignorecase('yes')?true:false);
   		//Boolean physicalDamageInd=true;
		String troubleshootingDescription = c.Descrip_of_Defect_Troubleshooting_Perf__c;
		Boolean cosmeticDamageInd = c.Is_there_any_Cosmetic_Damage__c;
		String descriptionOfCosmeticDamage = c.Comments_create_repair__c;
		Double estimatedRepairCost = c.Estimated_Repair_Cost_Up_To__c;
		Boolean clientShippingAddressRequiredInd = c.Shipping_Required__c;
		Boolean useAlternateShippingAddressInd = c.Use_Alternate_Address__c;
		String languageCode = getCleanLocale();
		
		String defectsBehaviour;
		if(languageCode == FRENCH_LOCALE) {
			defectsBehaviour = getFrenchDefectBehaviorValue(c.id);
		}
		
		// if FRENCH locale was unavailable, or if locale is ENGLISH
		if(defectsBehaviour == null || defectsBehaviour == '') {
			defectsBehaviour = c.Defects_behaviour__c;
		}
		
		trac_RepairCalloutResponse cr;
		rpm_EquipmentRepairManagementSvcRequestR.initCreateRepairResponse_element resp;													
		try {
			
			// TEST RESPONSES //////////////////////////////////////////////////
			if(Test.isRunningTest()) {
                /*
				if(serialNumber == trac_RepairTestUtils.REPAIR_ESN_FAILURE) {
					resp = trac_svcPseudoMocks.InitCreateRepairFailure();
				} else if(serialNumber == trac_RepairTestUtils.REPAIR_ESN_EXCEPTION) {
					throw new TestException('It Broke');
				} else if(serialNumber == trac_RepairTestUtils.REPAIR_ESN_LONG_EXCEPTION) {
					throw new TestException('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');
				} else {
					resp = trac_svcPseudoMocks.InitCreateRepairSuccess();
				}*/
			} else {
			///////////////////////////////////////////////////////////////////
			
				resp = port.initCreateRepair(
					salesForceCaseID, salesForceCaseNumber, clientName, serialNumber, mobileNumber, accessorySKU,
					contactName, contactNumber, contactEmailAddress, clientShippingAddress, physicalDamageInd,
					troubleshootingDescription, defectsBehaviour, cosmeticDamageInd, descriptionOfCosmeticDamage,
					estimatedRepairCost, clientShippingAddressRequiredInd, useAlternateShippingAddressInd,
					languageCode);
			}
			
			trac_RepairLogger.log('initCreateRepair', resp.requestKey, c.id);
		} catch (Exception e) {
            system.debug(e.getstacktracestring());
			cr = new trac_RepairCalloutResponse(e);
			
			trac_RepairLogger.log('initCreateRepair', 'Call to RTS Failed to Complete. Message was: ' + cr.messageBody, c.id);
			
			c.create_repair_last_error__c =
				(cr.messageBody.length() > 255 ? cr.messageBody.substring(0,255) : cr.messageBody);
			c.create_repair_last_date__c = Datetime.now();
			c.create_repair_last_user__c = UserInfo.getName();
			
			return cr;
		}
		
		cr = new trac_RepairCalloutResponse(resp);
		
		if(cr.success) {
			c.create_repair_last_status__c = true;
			
		} else {
			c.create_repair_last_error__c =
				(cr.messageBody.length() > 255 ? cr.messageBody.substring(0,255) : cr.messageBody);
		}
		
		c.create_repair_last_date__c = Datetime.now();
		c.create_repair_last_user__c = UserInfo.getName();
		
		return cr;
	}
	
	
	public static Map<String,String> getRepairStatus(Set<String> repairIds) {
		// no try catch in this case. If getEndpoint throws exception, the error should show in the Apex Jobs list
		Repair_WS_Endpoints__c endpointDetail = getEndpoint(Endpoint.GET_REPAIR_STATUS);
		
		rpm_EquipmentRepairInformationService_3.EquipmentRepairInformationServicePort port = 
			new rpm_EquipmentRepairInformationService_3.EquipmentRepairInformationServicePort();
		
		port.endpoint_x = endpointDetail.Endpoint__c;
		
		if(endpointDetail.timeout__c != null) {
			port.timeout_x = endpointDetail.timeout__c.intValue();
		}
		
		if(endpointDetail.username__c != null) {
			String authHeader = buildBasicAuthHeader(endpointDetail.username__c, endpointDetail.password__c);   
			port.inputHttpHeaders_x = new Map<String,String>{'Authorization' => authHeader};
		}
		
		port.lenientParsing_x = true;
		
		rpm_EquipmentRepairInformationSvcRequest.getRepairStatusResponse_element resp;
		
		rpm_EquipmentRepairInformationSvcRequest.RepairIdList repairIdList = trac_WSFieldMapper.idSetToRepairIdList(repairIds);
			
		// TEST RESPONSES //////////////////////////////////////////////////
		if(Test.isRunningTest()) {
			if(repairIds.contains(trac_RepairTestUtils.REPAIR_ID_EMPTY)) {
				resp = new rpm_EquipmentRepairInformationSvcRequest.getRepairStatusResponse_element();
			} else if(repairIds.contains(trac_RepairTestUtils.REPAIR_ID_EXCEPTION)){
				throw new RepairCalloutException();
			} else {
				resp = trac_svcPseudoMocks.getRepairStatusSuccess();
			}
		} else {
		///////////////////////////////////////////////////////////////////
		
			resp = port.getRepairStatus(repairIdList);
		}
		
		if(resp.RepairStatusList != null) {
			return trac_WSFieldMapper.repairStatusByIdToMap(resp.RepairStatusList);
		} else {
			return new Map<String,String>();
		}
	}
	
	
	public static trac_RepairCalloutResponse getRepair(Case c) {
		Repair_WS_Endpoints__c endpointDetail; 
		
		try {
			endpointDetail = getEndpoint(Endpoint.GET_REPAIR);
		} catch (Exception e) {
			return new trac_RepairCalloutResponse(e);
		}
		
		rpm_EquipmentRepairInformationService_3.EquipmentRepairInformationServicePort port = 
			new rpm_EquipmentRepairInformationService_3.EquipmentRepairInformationServicePort();
		
		port.endpoint_x = endpointDetail.Endpoint__c;
		
		if(endpointDetail.timeout__c != null) {
			port.timeout_x = endpointDetail.timeout__c.intValue();
		}
		
		if(endpointDetail.username__c != null) {
			String authHeader = buildBasicAuthHeader(endpointDetail.username__c, endpointDetail.password__c);   
			port.inputHttpHeaders_x = new Map<String,String>{'Authorization' => authHeader};
		}
		
		port.lenientParsing_x = true;
		
		String repairId = c.Repair__c;
		String languageCD = getCleanLocale();
		
		trac_RepairCalloutResponse cr;
		rpm_EquipmentRepairInformationSvcRequest.getRepairResponse_element resp;
		
		try {
			
			// TEST RESPONSES //////////////////////////////////////////////////
			if(Test.isRunningTest()) {
				if(repairId == trac_RepairTestUtils.REPAIR_ID_FAILURE) {
					resp = trac_svcPseudoMocks.getRepairFailure();
				} else if(repairId == trac_RepairTestUtils.REPAIR_ID_EXCEPTION) {
					throw new TestException('It Broke');
				} else if(repairID == trac_RepairTestUtils.REPAIR_ID_USE_CASE_ID) {
					resp = trac_svcPseudoMocks.getRepairSuccess([SELECT Id FROM Case Limit 1].id); // allows for testing of caseID checking. repairID contains caseid
				} else {
					resp = trac_svcPseudoMocks.getRepairSuccess();
				}
			} else {
			///////////////////////////////////////////////////////////////////
			
				resp = port.getRepair(repairID, languageCD);
			}
		} catch (Exception e) {
			return new trac_RepairCalloutResponse(e);
		}
		
		cr = new trac_RepairCalloutResponse(resp);
		
		if(cr.success) {
			trac_WSFieldMapper.getRepairResultToCase(resp, c);
		}
		
		return cr;
	}
	
	
	public static trac_RepairCalloutResponse updateQuoteStatus(Case c, String quoteStatus) {
		Repair_WS_Endpoints__c endpointDetail; 
		
		try {
			endpointDetail = getEndpoint(Endpoint.UPDATE_QUOTE_STATUS);
		} catch (Exception e) {
			return new trac_RepairCalloutResponse(e);
		}
		
		rpm_EquipmentRepairManagementService_1.EquipmentRepairManagementServicePort port = 
			new rpm_EquipmentRepairManagementService_1.EquipmentRepairManagementServicePort();
		
		port.endpoint_x = endpointDetail.Endpoint__c;
		
		if(endpointDetail.timeout__c != null) {
			port.timeout_x = endpointDetail.timeout__c.intValue();
		}
		
		if(endpointDetail.username__c != null) {
			String authHeader = buildBasicAuthHeader(endpointDetail.username__c, endpointDetail.password__c);   
			port.inputHttpHeaders_x = new Map<String,String>{'Authorization' => authHeader};
		}
		
		port.lenientParsing_x = true;
		
		String repairId = c.Repair__c;
    String repairQuoteStatus = quoteStatus;
    String note; // TODO - MAP THIS
    Boolean clientContacted = c.Client_Contacted__c;
    String userID;
    try {
    	userID = getUserTeamTelusId();
    }catch (Exception e) {
    	return new trac_RepairCalloutResponse(e);
    }
    
    DateTime quoteExtension;
    String languageCode = getCleanLocale();		
		
		trac_RepairCalloutResponse cr;
		rpm_EquipmentRepairManagementSvcRequestR.updateEquipmentRepairQuotationResponse_element resp;
		
		try {
			
			// TEST RESPONSES //////////////////////////////////////////////////
			if(Test.isRunningTest()) {
				if(repairId == trac_RepairTestUtils.REPAIR_ID_FAILURE) {
					resp = trac_svcPseudoMocks.updateQuoteFailureInvalidRequest();
				} else if(repairId == trac_RepairTestUtils.REPAIR_ID_EXCEPTION) {
					throw new TestException('It Broke');
				} else {
					resp = trac_svcPseudoMocks.updateQuoteSuccess();
				}
			} else {
			///////////////////////////////////////////////////////////////////
			
				resp = port.updateEquipmentRepairQuotation(
					repairID, repairQuoteStatus, note, clientContacted, userID, quoteExtension,languageCode
				);
			}
		} catch (Exception e) {
			return new trac_RepairCalloutResponse(e);
		}
		
		cr = new trac_RepairCalloutResponse(resp);
		
		if(cr.success) {
			// what to update?
		}
		
		return cr;
	}
	
	
	private static String buildBasicAuthHeader(String username, String password) {
		Blob headerValue = Blob.valueOf(username + ':' + password);   
		String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
		
		return authorizationHeader;
	}
	
	
	private static String getUserTeamTelusId() {
		String teamTelusId = [SELECT Team_TELUS_ID__c
													FROM User
													WHERE Id = :UserInfo.getUserId()].Team_TELUS_ID__c;
		if (teamTelusId == null) {
			throw new RepairCalloutException(Label.TEAM_TELUS_ID_MISSING);
		} else {
			return teamTelusId;
		} 
	}
	
	
	private static Repair_WS_Endpoints__c getEndpoint(Endpoint ep) {
		Boolean invalid = false;
		Repair_WS_Endpoints__c epObj;
		String instanceName;
		
		if(ep == Endpoint.VALIDATE) {
			instanceName = 'validate';
		} else if(ep == Endpoint.INIT_CREATE_REPAIR || ep == Endpoint.UPDATE_QUOTE_STATUS) {
			instanceName = 'repairmgmt'; 
		} else if(ep == Endpoint.GET_REPAIR || ep == Endpoint.GET_REPAIR_STATUS) {
			instanceName = 'repairinfo';
		}
		
		if(!isProduction()) {
			instanceName += '_test';
		}
		
		epObj = Repair_WS_Endpoints__c.getInstance(instanceName);
		
		if(epObj == null) {
			throw new RepairCalloutException(Label.MISSING_ENDPOINT);
		}
		
		return epObj;
	}
	
	public static Boolean isProduction() {
		return UserInfo.getOrganizationId() == PRODUCTION_ORG_ID;
	}
	
	
	public static String getCleanLocale() {
		
		if(Label.LANGUAGE == 'FR') {
			return FRENCH_LOCALE;
		} else {
			return ENGLISH_LOCALE;
		}
	}
	
	
	private static String getFrenchDefectBehaviorValue(Id caseId) {
		final String START_TAG = '<span id="page:field">';
		final String END_TAG = '</span>';
		
		PageReference ref = Page.Repair_French_Picklist_Reader;
		ref.getParameters().put('id', caseId);

		String html;
		if(Test.isRunningTest()) {
			html = trac_RepairTestUtils.getFrenchDefectBehaviourTestBody();
		} else {
			html = ref.getContent().toString();
		}
		
		Integer startPos = html.indexOf(START_TAG) + START_TAG.length();
		Integer endPos = html.indexOf(END_TAG);
		
		String frenchValue = html.substring(startPos,endPos);
		
		return frenchValue;
	}
	
	
	private class RepairCalloutException extends Exception{}
	
	private class TestException extends Exception{}
}