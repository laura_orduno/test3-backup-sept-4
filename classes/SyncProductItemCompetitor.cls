/*****************************************************
    Class Name: SyncProductItemCompetitor 
    Usage: This class to sync the Opportunity Product Item records with the
           Competitor object
    Author – Clear Task    
    Date – 03/01/2011       
    Revision History
******************************************************/
public class SyncProductItemCompetitor{
    //used for avoid Competitor list is selected as None
    private static final String NONE = '(None)';
    //used for making Competitor list for a opportunity from OpportunityCompetitor
    private Map<Id, Set<String>> oppCompetitorMap = new Map<Id, Set<String>>();
    //used for making Competitor list for a opportunity from Opp_Product_Item__c
    private Map<Id, Set<String>> oppCompeForPdctItmCmptrMap = new Map<Id, Set<String>>();
    //used for find the OpportunityCompetitor Id for a opportunity with Competitor 
    private Map<Id, Map<String, OpportunityCompetitor>> comptIdForOppCompMap = new Map<Id, Map<String, OpportunityCompetitor>>();
    /*
     * doSyncProductItemCompetitor method  to sync the Opportunity Product Item records
     * with the Competitor object- Start
     */
    public void doSyncProductItemCompetitor(List<Opp_Product_Item__c> oppProductItemList){
        if(oppProductItemList != null && oppProductItemList.size()>0){
            Set<String> tempCompetitorSet = new Set<String>();
            Id tempOppId = oppProductItemList[0].Opportunity__c;
            Integer index = 1;
            for(Opp_Product_Item__c opi :oppProductItemList){
                if(tempOppId == opi.Opportunity__c){ 
                    String strCompetitorList = opi.Competitor__c;
                    if(strCompetitorList != null){
                        List<String> competitorList = strCompetitorList.split(';');
                        if(competitorList != null && competitorList.size()>0){
                            for(String s :competitorList ){
                                if(s != null && !(s.equals(NONE))){
                                    tempCompetitorSet.add(s);
                                }
                            }
                        }
                    } 
                }else if(tempOppId != opi.Opportunity__c){
                    if(tempCompetitorSet != null && tempCompetitorSet.size()>0){
                        if(oppCompeForPdctItmCmptrMap != null && oppCompeForPdctItmCmptrMap.containsKey(tempOppId)){
                            Set<String> competitorForOpp = oppCompeForPdctItmCmptrMap.get(tempOppId);
                            if( competitorForOpp == null){competitorForOpp = new Set<String>();}
                            competitorForOpp.addAll(tempCompetitorSet);
                            oppCompeForPdctItmCmptrMap.remove(tempOppId); 
                            oppCompeForPdctItmCmptrMap.put(tempOppId, competitorForOpp); 
                        }else{       
                            oppCompeForPdctItmCmptrMap.put(tempOppId, tempCompetitorSet); 
                        }
                    }else{
                        oppCompeForPdctItmCmptrMap.put(tempOppId, null); 
                    }
                    tempCompetitorSet = new Set<String>();   
                    tempOppId = opi.Opportunity__c;
                    String strCompetitorList = opi.Competitor__c;
                    if(strCompetitorList != null){
                        List<String> competitorList = strCompetitorList.split(';');
                        if(competitorList != null && competitorList.size()>0){
                            for(String s :competitorList ){
                                if(s != null && !(s.equals(NONE))){
                                    tempCompetitorSet.add(s);
                                }
                            }
                        }
                    }  
                }
                if(index == oppProductItemList.size()){
                    if(tempCompetitorSet != null && tempCompetitorSet.size()>0){
                        if(oppCompeForPdctItmCmptrMap != null && oppCompeForPdctItmCmptrMap.containsKey(tempOppId)){
                            Set<String> competitorForOpp = oppCompeForPdctItmCmptrMap.get(tempOppId);
                            if( competitorForOpp == null){competitorForOpp = new Set<String>();}
                            competitorForOpp.addAll(tempCompetitorSet);
                            oppCompeForPdctItmCmptrMap.remove(tempOppId); 
                            oppCompeForPdctItmCmptrMap.put(tempOppId, competitorForOpp); 
                        }else{       
                            oppCompeForPdctItmCmptrMap.put(tempOppId, tempCompetitorSet); 
                        }
                    }else{
                        oppCompeForPdctItmCmptrMap.put(tempOppId, null); 
                    }    
                }
                index = index + 1;
            }
            System.debug('oppCompeForPdctItmCmptrMap =' +oppCompeForPdctItmCmptrMap );
            if(oppCompeForPdctItmCmptrMap != null && oppCompeForPdctItmCmptrMap.size()>0){
                createUniqueCompetitor();
                deleteUniqueCompetitor();
            }
        }
    }
   /*
    * Create Unique Competitor in OpportunityCompetitor - Start
    */  
    private void createUniqueCompetitor(){             
        List<OpportunityCompetitor> oppCompetitorList = [Select o.OpportunityId, o.CompetitorName From OpportunityCompetitor o where o.OpportunityId in :oppCompeForPdctItmCmptrMap.keySet() order by o.OpportunityId];
        if(oppCompetitorList != null && oppCompetitorList.size()>0){
            Id tempOppId = oppCompetitorList[0].OpportunityId;
            Set<String> tempCompetitorSet = new Set<String>();
            Integer index = 1;   
            for(OpportunityCompetitor c :oppCompetitorList ){
                if(tempOppId == c.OpportunityId){                
                    tempCompetitorSet.add(c.CompetitorName);
                    if(comptIdForOppCompMap != null){
                        Map<String, OpportunityCompetitor> compIdMap = comptIdForOppCompMap.get(tempOppId);
                        if(compIdMap == null){
                            compIdMap = new Map<String, OpportunityCompetitor>(); 
                            compIdMap.put(c.CompetitorName, c);
                            comptIdForOppCompMap.put(tempOppId, compIdMap);                            
                         }else{
                            compIdMap.put(c.CompetitorName, c);
                            comptIdForOppCompMap.put(tempOppId, compIdMap);   
                         }
                     }                
                 }else if(tempOppId != c.OpportunityId){
                     if(tempCompetitorSet != null && tempCompetitorSet.size()>0){
                         if(oppCompetitorMap != null && oppCompetitorMap.containsKey(tempOppId)){
                             Set<String> competitorForOpp = oppCompetitorMap.get(tempOppId);
                             if( competitorForOpp == null){competitorForOpp = new Set<String>();}
                             competitorForOpp.addAll(tempCompetitorSet);
                             oppCompetitorMap.remove(tempOppId); 
                             oppCompetitorMap.put(tempOppId, competitorForOpp); 
                         }else{       
                             oppCompetitorMap.put(tempOppId, tempCompetitorSet); 
                         }
                      }
                      tempCompetitorSet = new Set<String>();
                      tempCompetitorSet.add(c.CompetitorName); 
                      if(comptIdForOppCompMap != null){
                                 Map<String, OpportunityCompetitor > compIdMap = comptIdForOppCompMap.get(tempOppId);
                                 if(compIdMap == null){
                                     compIdMap = new Map<String, OpportunityCompetitor >(); 
                                     compIdMap.put(c.CompetitorName, c);
                                     comptIdForOppCompMap.put(tempOppId, compIdMap);                            
                                 }else{
                                     compIdMap.put(c.CompetitorName, c);
                                     comptIdForOppCompMap.put(tempOppId, compIdMap);   
                                 }
                       }   
                       tempOppId = c.OpportunityId; 
                 }
                 if(index == oppCompetitorList.size()){
                     if(tempCompetitorSet != null && tempCompetitorSet.size()>0){
                         if(oppCompetitorMap != null && oppCompetitorMap.containsKey(tempOppId)){
                             Set<String> competitorForOpp = oppCompetitorMap.get(tempOppId);
                             if( competitorForOpp == null){competitorForOpp = new Set<String>();}
                             competitorForOpp.addAll(tempCompetitorSet);
                             oppCompetitorMap.remove(tempOppId); 
                             oppCompetitorMap.put(tempOppId, competitorForOpp); 
                         }else{       
                             oppCompetitorMap.put(tempOppId, tempCompetitorSet); 
                         }
                      }    
                 }
                 index = index + 1;
            }    
        }
        System.debug('oppCompetitorMap =' +oppCompetitorMap);
        List<OpportunityCompetitor> oppCompInsertList = new List<OpportunityCompetitor>(); 
        if(oppCompetitorMap != null && oppCompetitorMap.size()>0){
            for(String s :oppCompeForPdctItmCmptrMap.keySet()){
                Set<String> productItemCmptrSet = oppCompeForPdctItmCmptrMap.get(s);
                Set<String> oppCompetitorSet = oppCompetitorMap.get(s);                
                if(productItemCmptrSet != null && oppCompetitorSet != null && productItemCmptrSet.size()>0 && oppCompetitorSet.size()>0){               
                    for(String newComptr :productItemCmptrSet ){
                        boolean existFlag = true;
                        for(String c :oppCompetitorSet ){
                            if(c.equals(newComptr)){
                                existFlag = false;    
                            }
                        }
                        if(existFlag ){
                            OpportunityCompetitor oc = new OpportunityCompetitor(
                                OpportunityId = s,
                                CompetitorName = newComptr   
                            );
                            oppCompInsertList.add(oc);
                        }
                    }
                }
            }
        }else{
            for(String s :oppCompeForPdctItmCmptrMap.keySet()){
                Set<String> productItemCmptrSet = oppCompeForPdctItmCmptrMap.get(s);
                if(productItemCmptrSet != null && productItemCmptrSet.size()>0){
                    for(String newComptr :productItemCmptrSet ){
                         OpportunityCompetitor oc = new OpportunityCompetitor(
                             OpportunityId = s,
                             CompetitorName = newComptr   
                         );
                         oppCompInsertList.add(oc);
                    }
                }
            }
        }
        System.debug('oppCompInsertList=' +oppCompInsertList );
        if(oppCompInsertList != null && oppCompInsertList.size()>0){
            try{             
                insert oppCompInsertList;                         
            }catch(DMLException ex){System.debug('Exception in inserting OpportunityCompetitor : ' + ex.getDMLMessage(0));}    
        }              
    }
    /*
     * Create Unique Competitor in OpportunityCompetitor - End
     */
    /*
     * Delete Competitor in OpportunityCompetitor - Start
     */
    private void deleteUniqueCompetitor(){  
        if(oppCompeForPdctItmCmptrMap != null && oppCompeForPdctItmCmptrMap.size()> 0){
            Map<Id, Set<String>> compeForOppPdctForDeleteMap = new Map<Id, Set<String>>();
            List<Opp_Product_Item__c> opiList = [Select o.Opportunity__c, o.Competitor__c From Opp_Product_Item__c o where  o.Opportunity__c in :oppCompeForPdctItmCmptrMap.keySet() order by o.Opportunity__c];
            if(opiList != null && opiList.size()>0){                
                Set<String> tempCompetitorSet = new Set<String>();
                Id tempOppId = opiList[0].Opportunity__c;
                Integer index = 1;
                for(Opp_Product_Item__c opi :opiList){
                    if(tempOppId == opi.Opportunity__c){ 
                        String strCompetitorList = opi.Competitor__c;
                        if(strCompetitorList != null){
                            List<String> competitorList = strCompetitorList.split(';');
                            if(competitorList != null && competitorList.size()>0){
                                for(String s :competitorList ){
                                    if(s != null && !(s.equals(NONE))){
                                        tempCompetitorSet.add(s);
                                    }
                                }
                            }
                         } 
                    }else if(tempOppId != opi.Opportunity__c){
                        if(tempCompetitorSet != null && tempCompetitorSet.size()>0){
                            if(compeForOppPdctForDeleteMap != null && compeForOppPdctForDeleteMap.containsKey(tempOppId)){
                                Set<String> competitorForOpp = compeForOppPdctForDeleteMap.get(tempOppId);
                                if( competitorForOpp == null){competitorForOpp = new Set<String>();}
                                competitorForOpp.addAll(tempCompetitorSet);
                                compeForOppPdctForDeleteMap.remove(tempOppId); 
                                compeForOppPdctForDeleteMap.put(tempOppId, competitorForOpp); 
                             }else{       
                                compeForOppPdctForDeleteMap.put(tempOppId, tempCompetitorSet); 
                             }
                        }
                        tempCompetitorSet = new Set<String>();   
                        tempOppId = opi.Opportunity__c;
                        String strCompetitorList = opi.Competitor__c;
                        if(strCompetitorList != null){
                        List<String> competitorList = strCompetitorList.split(';');
                            if(competitorList != null && competitorList.size()>0){
                                for(String s :competitorList ){
                                    if(s != null && !(s.equals(NONE))){
                                        tempCompetitorSet.add(s);
                                    }
                                }
                            }
                        }  
                    }
                    if(index == opiList.size()){
                        if(tempCompetitorSet != null && tempCompetitorSet.size()>0){
                            if(compeForOppPdctForDeleteMap != null && compeForOppPdctForDeleteMap.containsKey(tempOppId)){
                                Set<String> competitorForOpp = compeForOppPdctForDeleteMap.get(tempOppId);
                                if( competitorForOpp == null){competitorForOpp = new Set<String>();}
                                competitorForOpp.addAll(tempCompetitorSet);
                                compeForOppPdctForDeleteMap.remove(tempOppId); 
                                compeForOppPdctForDeleteMap.put(tempOppId, competitorForOpp); 
                            }else{       
                                compeForOppPdctForDeleteMap.put(tempOppId, tempCompetitorSet); 
                            }
                       }    
                   }
                   index = index + 1;
                }    
            }else{
                List<OpportunityCompetitor> oppCompetitorForDeleteList = [Select o.OpportunityId, o.CompetitorName From OpportunityCompetitor o where o.OpportunityId in :oppCompeForPdctItmCmptrMap.keySet() order by o.OpportunityId];    
                System.debug('oppCompetitorForDeleteList =' +oppCompetitorForDeleteList );
                if(oppCompetitorForDeleteList != null && oppCompetitorForDeleteList.size()>0){
                    try{             
                        delete oppCompetitorForDeleteList ; 
                        compeForOppPdctForDeleteMap = null;                        
                    }catch(DMLException ex){System.debug('Exception in Deleting OpportunityCompetitor : ' + ex.getDMLMessage(0));}    
                }
            }
            System.debug('compeForOppPdctForDeleteMap=' +compeForOppPdctForDeleteMap);
            List<OpportunityCompetitor> oppCompDeleteList = new List<OpportunityCompetitor>(); 
            if(compeForOppPdctForDeleteMap != null && compeForOppPdctForDeleteMap.size()>0){
                for(String s :compeForOppPdctForDeleteMap.keySet()){
                    Set<String> productItemCmptrSet = compeForOppPdctForDeleteMap.get(s);
                    Set<String> oppCompetitorSet = oppCompetitorMap.get(s);                
                    if(oppCompetitorSet != null && oppCompetitorSet.size()>0){               
                        for(String newComptr :oppCompetitorSet){
                            boolean existFlag = true;
                            for(String c :productItemCmptrSet){
                                if(c.equals(newComptr)){
                                    existFlag = false;    
                                }
                            }
                            if(existFlag){
                                if(comptIdForOppCompMap != null){
                                    Map<String, OpportunityCompetitor> oppCompMap = comptIdForOppCompMap.get(s);
                                    if(oppCompMap != null){
                                        OpportunityCompetitor ocObj = oppCompMap.get(newComptr);
                                        if(ocObj != null){
                                            oppCompDeleteList.add(ocObj);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                System.debug('oppCompDeleteList=' +oppCompDeleteList);
                if(oppCompDeleteList != null && oppCompDeleteList.size()>0){
                    try{             
                        delete oppCompDeleteList;                         
                    }catch(DMLException ex){System.debug('Exception in Deleting OpportunityCompetitor : ' + ex.getDMLMessage(0));}    
                }
            }
        }
    }
    /*
     * Delete Competitor in OpportunityCompetitor - End
     */
}