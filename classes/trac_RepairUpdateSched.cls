/*
 * Schedulable wrapper for trac_RepairUpdateBatch
 * Part of the SMB Repairs project
 *
 * @author: Grant Adamson, Traction On Demand 
 */

global class trac_RepairUpdateSched implements Schedulable{
  private static final String JOB_NAME = 'Update Repair Details from RTS' + (Test.isRunningTest() ? ' TEST' : '');
  private static final String CRON_SCHED = '0 0 * * * ?';
  
  
  // convenience method to submit scheduled job
  public static void schedule() {
    system.schedule(JOB_NAME, CRON_SCHED, new trac_RepairUpdateSched());
  }
  
  
  global void execute(SchedulableContext SC) {
    trac_RepairUpdateBatch.run();
  }
  
}