@isTest
public class trac_CaseBusinessHoursTest {
	
	private static testMethod void testTrigger() {
		SMET_Request__c req = new SMET_Request__c(Status__c = 'test1');
		insert req;
		
		req = [
			SELECT Time_With_Customer__c, Time_With_Support__c, Time_in_New_Status__c, Time_In_Re_Open_Status__c
			FROM SMET_Request__c
			WHERE Id = :req.id
		];
		
		system.assertEquals(0, req.Time_With_Customer__c);
		system.assertEquals(0, req.Time_With_Support__c);
		system.assertEquals(0, req.Time_in_New_Status__c);
		system.assertEquals(0, req.Time_In_Re_Open_Status__c);
		
		req.status__c = 'test2';
		req.Time_With_Customer__c = null;
		req.Time_With_Support__c = null;
		req.Time_in_New_Status__c = null;
		req.Time_In_Re_Open_Status__c = null;
		update req;
		
	}

}