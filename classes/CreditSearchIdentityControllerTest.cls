@isTest
public class CreditSearchIdentityControllerTest
{
    private static CreditSearchIdentityController controller;

    @isTest()
	public static void search()
    {
        initialize();

        CreditSearchIdentityController.CreditInfoSearchParam param = new CreditSearchIdentityController.CreditInfoSearchParam();
        param.sin = '220380919';
        param.driversLicenseNum = 'DL987321';
        param.driversLicenseProvince = 'BC';
        param.provincialIdNum = '3333';
        param.provincialIdProvince = 'BC';
        param.passportNum = '123456789';
        param.passportCountry = 'CAN';
        param.healthCardNum = '12345689';

        controller.searchParam = param;

        controller.search();
    }

    @isTest()
    public static void searchEmptyParam()
    {
        initialize();

        controller.search();
    }

    private static void initialize()
    {
        Account a = new Account(name='Tester');
        insert a;

        controller = new CreditSearchIdentityController(new ApexPages.StandardController(a));

        setupTestData();
    }

    private static void setupTestData()
    {
        Credit_Profile__c profile = new Credit_Profile__c();

        profile.SIN__c = '220380919';
        profile.Driver_s_License__C = 'DL987321';
        profile.Drivers_License_province__c = 'Ontario';
        
        profile.Provincial_ID__c = '3333';
        profile.Provincial_ID_Province__c = 'Ontario';
        
        profile.Passport__c = '123456789';
        profile.Passport_Country__c = 'Canada';
        

        profile.SIN_Hash__c = CreditCrypto.hash(profile.SIN__c);
        profile.Driver_s_License_Hash__C = CreditCrypto.hash(profile.Driver_s_License__C);
        profile.Provincial_ID_Hash__c = CreditCrypto.hash(profile.Provincial_ID__c);
        profile.Passport_Hash__c = CreditCrypto.hash(profile.Passport__c);

        insert profile;

        Credit_Assessment__c car = new Credit_Assessment__c();

        car.Driver_s_License__c = 'DL987321';
        car.Drivers_License_province__c = 'BC';
        
        car.Provincial_ID__c = '3333';
        car.Provincial_ID_Province__c = 'BC';
        
        car.Passport__c = '123456789';
        car.Passport_Country__c = 'CAN';        
        
        car.Health_Card_No__c = '12345689';

        car.Driver_s_License_Hash__c = CreditCrypto.hash(car.Driver_s_License__c);
        car.Provincial_ID_Hash__c = CreditCrypto.hash(car.Provincial_ID__c);
        car.Passport_Hash__c = CreditCrypto.hash(car.Passport__c);
        car.Health_Card_No_Hash__c = CreditCrypto.hash(car.Health_Card_No__c);

        insert car;
    }
}