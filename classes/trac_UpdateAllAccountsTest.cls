@isTest
private class trac_UpdateAllAccountsTest {
    private static testMethod void testTrac_UpdateAllAccounts() {
    	Account a = new Account(name = 'test');
    	insert a;
    	
    	Test.startTest();
    	
    	Database.executeBatch(new trac_UpdateAllAccounts(), 1000);
    	
    	Test.stopTest();
    }
}