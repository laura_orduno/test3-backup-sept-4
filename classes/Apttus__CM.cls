/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global abstract class CM {
    global static String RULETYPE_AGREEMENT_TEMPLATE;
    global static String RULETYPE_AGREEMENT_TYPE;
    global static String RULETYPE_CONTENT_WORKSPACE;
    global static String RULETYPE_DOCUMENT_FOLDER;
    global static String RULETYPE_EMAIL_TEMPLATE;
    global static String RULETYPE_QUEUE_ASSIGNMENT;
    global static String RULETYPE_SUBMIT_REQUEST_MODE;
    global CM() {

    }
global class AgreementLockDO {
    webService Apttus__APTS_Agreement__c AgreementSO {
        get;
    }
    webService List<Apttus.CM.CheckoutDO> Checkouts {
        get;
    }
    webService Boolean HasMultipleCheckouts {
        get;
    }
    webService Boolean IsLocked {
        get;
    }
    webService Id LockedById {
        get;
    }
    webService String LockedByName {
        get;
    }
    webService Datetime LockedDate {
        get;
    }
}
global class CheckoutDO {
    webService Id CheckoutById {
        get;
    }
    webService String CheckoutByName {
        get;
    }
    webService Datetime CheckoutDate {
        get;
    }
}
global class ClauseCategoryInfo {
    webService List<Apttus.CM.ClauseInfo> Clauses {
        get;
    }
    webService Boolean HasClauses {
        get;
    }
    webService Boolean HasSubCategories {
        get;
    }
    webService String Name {
        get;
    }
    webService List<Apttus.CM.ClauseSubCategoryInfo> SubCategories {
        get;
    }
}
global class ClauseInfo {
    webService Id ClauseId {
        get;
    }
    webService String ClauseName {
        get;
    }
}
global class ClauseInfoTree {
    webService List<Apttus.CM.ClauseCategoryInfo> Categories {
        get;
    }
    webService List<Apttus.CM.ClauseInfo> Clauses {
        get;
    }
    webService String ClauseType {
        get;
    }
    webService Boolean HasCategories {
        get;
    }
    webService Boolean HasClauses {
        get;
    }
    webService String RecordType {
        get;
    }
}
global class ClauseSubCategoryInfo {
    webService List<Apttus.CM.ClauseInfo> Clauses {
        get;
    }
    webService Boolean HasClauses {
        get;
    }
    webService String Name {
        get;
    }
}
}
