@isTest
private class RecomboInboundWSTest {
    private static final String WAYBILL_ID = '12345';
		private static final String ACCEPTANCE_DATA_XML = '<root><document><waybillId>' + WAYBILL_ID + '</waybillId></document></root>';
    private static final String SUCCESS_XML = '<response>Success</response>'; 
    
    private static testMethod void testProcessAgreement() {
			Account a = new Account(name = 'test');
			insert a;
			
			Opportunity o = new Opportunity(name = 'test', accountid = a.id, stagename = 'test', closedate = Date.today());
			insert o;
			
			SBQQ__Quote__c quote = new SBQQ__Quote__c(SBQQ__Opportunity__c = o.id);
			insert quote;
			
			Agreement__c ag = new Agreement__c(Waybill_ID__c = WAYBILL_ID, quote__c = quote.id);
			insert ag;
			
			String result = RecomboInboundWS.processAgreement(ACCEPTANCE_DATA_XML);
			
			system.assertEquals(SUCCESS_XML, result);
			
			ag = [SELECT status__c, Signed_Date__c FROM Agreement__c WHERE Id = :ag.id];
    	system.assertEquals(QuoteAgreementStatus.SIGNED, ag.status__c);
    	system.assertNotEquals(null, ag.Signed_Date__c);
    }
    
    private static testMethod void testProcessAgreementGeneralException() {
    	String result = RecomboInboundWS.processAgreement(null);
    	
    	system.assert(result.startsWith('Acceptance data cannot be null'));
    }
    
    private static testMethod void testProcessAgreementLoadingException() {
   	
    	String result = RecomboInboundWS.processAgreement(ACCEPTANCE_DATA_XML);
    	
    	system.assert(result.startsWith('Failed to load agreements for waybill ID ' + WAYBILL_ID));
    }
    
    private static testMethod void testExtractWaybillId() {
    	final String ACCEPTANCE_DATA_XML_EXTENDED = '<root><document><waybillId> ' + WAYBILL_ID + 
    		' </waybillId><someTag></someTag></document></root>';
    		
    	system.assertEquals(WAYBILL_ID, RecomboInboundWS.extractWaybillId(ACCEPTANCE_DATA_XML_EXTENDED));
    }
    
    private static testMethod void testExtractWaybillIdException() {
    	Boolean caughtException = false;
    	
    	try {
    		String waybillId = RecomboInboundWS.extractWaybillId(null);
    	} catch(Exception e) {
    		caughtException = true;
    		system.assertEquals('Acceptance data cannot be null', e.getMessage());
    	}
    	
    	system.assert(caughtException);
    }
}