global class RestApi_MyAccountHelper_CreateCase {

	public static final String BAN = Schema.SObjectType.Account.getRecordTypeInfosByName().get('BAN').getRecordTypeId();
	public static final String CAN = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CAN').getRecordTypeId();
	public static final String DEFAULT_ORIGIN = RestApi_MyAccountHelper_Utility.DEFAULT_ORIGIN;

	// GetCasesRequestObject object classes
	public class RequestUser {
		public String uuid = '';
	}

	public class CaseAttachment {
		public String name = '';
		public Blob body = null;
	}

	public class RequestCreateCase {
		public String billingAccountNumber = '';    //  * billing account number (e.g. 101-45222)
		public Boolean sendNotification = false;    //  * notifications flag (if False, system will not send notification to originator)
		public CaseAttachment attachment = null;  //    * case attachment
		public String category = '';                //  * category
		public String requestType = '';             //  * request type
		public String requestSubtype = '';            //  * request subtype
		public String subject = '';                 //  * subject
		public String description = '';             //  * description (note: description must contain labels and corresponding values from the submitted case form.)
		public List<String> collaborators = null;               //  * List of Collaborators (max 3) (data types TBD)
		public Boolean mobilityRequest = false;
		public String languagePreference = '';
	}

	public class CreateCaseRequestObject extends RestApi_MyAccountHelper.RequestObject {
		public RequestUser user;
		public RequestCreateCase caseToCreate;
	}
	// end GetCasesRequestObject classes

	// CreateCaseResponse
	// derived child class to act as the JSON response
	global class CreateCaseResponse extends RestApi_MyAccountHelper.Response {
		public String userName = '';
		public Case caseDetails = null;
		public List<Attachment> attachments = new List<Attachment>();

		public CreateCaseResponse(CreateCaseRequestObject request) {
			super(request);
			System.debug(LoggingLevel.ERROR, 'Line 45 Error');
			// CHECK REQUEST FOR COMPLETENESS

			// check if user object in request defined
			if (request.user == null) {
				throw new xException('User object for request not defined');
			}
			// check if uuid specified
			if (request.user.uuid == null) {
				throw new xException('Field "uuid" in user object for request not defined');
			}

			// check if case information was submitted
			if (request.caseToCreate == null) {
				throw new xException('No case information was submitted for creation');
			}

			if (request.caseToCreate.mobilityRequest == null) {
				throw new xException('Mobility Request information missing for case submitted for creation');
			}

			// CONFIRM THAT DATA LIVES WITHIN SALESFORCE
			System.debug('User: ' + request.user.uuid);
			// get user by uuid ( refers to the field "federationIdentifier" in the Salesforce User object)
			User u = RestApi_MyAccountHelper_Utility.getUser(request.user.uuid);

			// if user not found
			if (u == null) {
				throw new xException('User not found for uuid: ' + request.user.uuid);
			}

			if (!u.Contact.MASR_Enabled__c) {
				throw new xException('User is not MASR Enabled.');
			}

			username = u.name;

			if (u.ContactId == null) {
				throw new xException('No contact associated with user: ' + request.user.uuid);
			}

			Account a = getBillingAccount(request.caseToCreate.billingAccountNumber);

			// create case
			Case caseToInsert = new Case();

			if (a != null && a.RecordTypeId == BAN) {
				caseToInsert.related_BAN_Account__c = a.Id;
			}

			if (a != null && a.RecordTypeId == CAN) {
				caseToInsert.related_BCAN_Account__c = a.Id;
			}

			caseToInsert.AccountId = u.Contact.AccountId;
			caseToInsert.ContactId = u.ContactId;
			caseToInsert.Subject = request.caseToCreate.subject;
			caseToInsert.Description = request.caseToCreate.description;
			caseToInsert.NotifyCustomer__c = request.caseToCreate.sendNotification;
			caseToInsert.My_Business_Requests_Type__c = request.caseToCreate.requestType;
			caseToInsert.Request_Type__c = request.caseToCreate.requestSubtype;
			caseToInsert.NotifyCollaboratorString__c = RestApi_MyAccountHelper_Utility.createCollaborators(request.caseToCreate.collaborators); // This will set caseDetails.Contributors to what we need.
			caseToInsert.RecordTypeId = determineRecordType(request.caseToCreate.requestType);
			caseToInsert.Origin = DEFAULT_ORIGIN;
			caseToInsert.MobilityRequest__c = request.caseToCreate.mobilityRequest;
			if (request.caseToCreate.languagePreference == 'French') {
				caseToInsert.Language_Preference__c = request.caseToCreate.languagePreference;
			} else {
				caseToInsert.Language_Preference__c = 'English';
			}

			try {
				insert caseToInsert;
			} catch (DMLException e) {
				throw new xException('Case Insertion: ' + e.getMessage());
			}

			caseDetails = RestApi_MyAccountHelper_Utility.getCaseDetails(caseToInsert.Id);

			if (request.caseToCreate.attachment != null) {
				Attachment attachmentToCreate = new Attachment();
				attachmentToCreate.ParentId = caseDetails.Id;
				attachmentToCreate.Name = request.caseToCreate.attachment.Name;
				attachmentToCreate.Body = request.caseToCreate.attachment.Body;
				System.debug(LoggingLevel.ERROR, 'Line 3 Error');
				try {
					insert attachmentToCreate;
					request.caseToCreate.attachment.body = null;
					Attachment attach = RestApi_MyAccountHelper_Utility.getAttachmentDetails(attachmentToCreate.id);
					attachments.add(attach);
				} catch (DMLException e) {
					throw new xException('Attachment Insertion: ' + e.getMessage());
				}
			}

		}

		private Account getBillingAccount(String billingAccountNumber) {
			try {
				// check if null
				if (billingAccountNumber == null || billingAccountNumber == '') {
					return null;
				}
				List<String> splitString = billingAccountNumber.split('-');
				String billingSystem = '';
				String CANId = '';
				if (splitString.size() == 2) {
					billingSystem = splitString[0];
					CANId = splitString[1];
				}

				return [
						SELECT id, RecordTypeId
						FROM Account
						WHERE BAN_CAN__c = :billingAccountNumber
						AND (RecordTypeId = :PortalConstants.ACCOUNT_RECORDTYPE_BAN OR RecordTypeId = :PortalConstants.ACCOUNT_RECORDTYPE_CAN)
						LIMIT 1
				];
			} catch (QueryException e) {
				//throw new xException('Billing Account: ' + e.getMessage());
				System.debug('Billing Account: ' + e.getMessage());
				return null;
			} catch (Exception e) {
				throw new xException('Billing Account: ' + e.getMessage());
			}
		}

		private Id determineRecordType(String requestType) {
			try {
				ExternalToInternal__c recordTypeName = [SELECT Internal__c FROM ExternalToInternal__c WHERE External__c = :requestType LIMIT 1];
				return Schema.SObjectType.Case.getRecordTypeInfosByName().get(recordTypeName.Internal__c).getRecordTypeId();
			} catch (QueryException e) {
				throw new xException('Request Type Invalid: ' + e.getMessage());
			} catch (Exception e) {
				throw new xException('Request Type Invalid: ' + e.getMessage());
			}

		}
	}

	// object to be returned as JSON response
	public CreateCaseResponse response = null;

	public RestApi_MyAccountHelper_CreateCase(String requestString) {
		// convert request to object
		CreateCaseRequestObject createCaseRequest = (CreateCaseRequestObject) JSON.deserialize(requestString, CreateCaseRequestObject.class);
		response = new CreateCaseResponse(createCaseRequest);
	}
}