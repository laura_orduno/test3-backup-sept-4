/******************************************************************************************************
Controller class for product picker - Competitor Intelligence
Has functionality for - 
1. Looking up products based on the hierarchy
2. Choosing Opportunity Product Item Record Type

Author: Simon Cheng

******************************************************************************************************/

public with sharing class CompetitorIntelligenceProductController {

    public Id opportunityId;
    public Id competitorIntelligenceId;
    public Id pliId;
    public Competitor_Intelligence__c CompetitorIntelligence {get; set;}
    public ProductSearchComponent component {get; set;}
    public boolean flagSearchAll{get; set;} 
    public String bpc1 {get; set;}
    public String bpc2 {get; set;}
    public String bpc3 {get; set;}
    
    public List<SelectOption> bpc1Options {get; set;}
    public List<SelectOption> bpc2Options {get; set;}
    public List<SelectOption> bpc3Options {get; set;}
    
    public List<Product2> productList {get; set;}
    
    public String selectedProduct {get; set;}
    public String selectedProductName {get; set;}
    public Decimal selectedProductLeadTime {get; set;}
    
    public String pageMessage {get; private set;}
    
    public static final Integer MIN_LENGTH_REQD = 2;

    /*
     * Pagination Variable -Start   
     */
        private List<Product2>  pageProductList;    
        private Integer pageNumber;    
        private Integer pageSize;    
        private Integer totalPageNumber;
        //return current pagenumber
        public Integer getPageNumber(){
            return pageNumber;
        }
        //retulr partner list to show by pagesize
        public List<Product2> getPageProductList(){
            return pageProductList;
        }
        //return pagesize
        public Integer getPageSize(){
           return pageSize;
        }
        
        /*
         *  return total page 
         */
        public Integer getTotalPageNumber(){
            if (totalPageNumber == 0 && productList !=null){    
            totalPageNumber = productList .size() / pageSize;    
            Integer mod = productList .size() - (totalPageNumber * pageSize);    
            if (mod > 0)    
                totalPageNumber++;    
            }    
            return totalPageNumber;
        }

    /*
    * Pagination Variable -End
    */ 
    
    public CompetitorIntelligenceProductController() {
        competitorIntelligenceId = System.currentPageReference().getParameters().get('competitorIntelligenceId');
	    component = new ProductSearchComponent();
        init();
         //Pagination -Start
             pageNumber = 0;    
             totalPageNumber = 0;    
             pageSize = 30;
        //Pagination - Ends  
        flagSearchAll=false; 
    }
    
    private void init() {
        //CompetitorIntelligence = [Select Id, Name, Competitor_Service__c, Sales_Opportunity__c, Product_ID__c from Competitor_Intelligence__c where Id = :competitorIntelligenceId];
        reset();
    }
    
    private void reset() {
        bpc1Options = new List<SelectOption>();
        bpc2Options = new List<SelectOption>();
        bpc3Options = new List<SelectOption>();
        bpc1 = null;
        bpc2 = null;
        bpc3 = null;
        pageMessage = null;
        
        productList = new List<Product2>();
    }
 /*
  * Pagination-Start
  */
      public PageReference firstBtnClick() {
        bindData(1);    
        return null;
      }
      
      public PageReference previousBtnClick() {
        bindData(pageNumber - 1);    
        return null;
      }
      
      public PageReference nextBtnClick() {
        bindData(pageNumber + 1);    
        return null;
      }
      
      public PageReference lastBtnClick() {
        pageNumber =totalPageNumber -1;
        bindData(totalPageNumber );    
        return null;
      }
      
      // bindData method is used for show the partner record as per current pagesize
      
      private void bindData(Integer newPageIndex){
        try{    
            if (productList== null){
                System.debug('########hits='+ productList);    
                return;
            }
            pageProductList= new List<Product2> ();    
            Transient Integer counter = 0;    
            Transient Integer min = 0;    
            Transient Integer max = 0;    
            if (newPageIndex > pageNumber){    
                min = pageNumber * pageSize;    
                max = newPageIndex * pageSize;                
            }else{    
                max = newPageIndex * pageSize;    
                min = max - pageSize;                 
            }   
            for(Product2 p : productList){    
                counter++;    
                if (counter > min && counter <= max)    
                pageProductList.add(p);    
            }    
            pageNumber = newPageIndex;        
            if (pageProductList== null || pageProductList.size() <= 0) {    
            }
        }    
        catch(Exception ex){         
        }
      }
  /*
  * Pagination-End
  */
    
     public Pagereference cancel() {
        return new Pagereference('/' + competitorIntelligenceId);
    }
    
    public Pagereference addProduct() {
        return new Pagereference('/' + competitorIntelligenceId);
    }

    
    public List<Product2> getSearchResults() {
        productList = component.searchProducts();
        return productList;
    }
    public void doRenderBlock(){
        productList.clear();
        if (component.searchOption.equals('Search All') ) {
            flagSearchAll=true; 
        }else{
            flagSearchAll=false; 
        }
    }
    public Pagereference selectProduct() {
        system.debug('competitorIntelligenceId ++ ' + competitorIntelligenceId);
        competitorIntelligence = new Competitor_Intelligence__c();
        try {
            if ( competitorIntelligenceId != null) {
                system.debug('selectedProduct ++ '+ selectedProduct);
                competitorIntelligence.Id = competitorIntelligenceId;
                competitorIntelligence.Product_ID__c = selectedProduct;
				
                update competitorIntelligence;
                system.debug('update in selected product');
                return new Pagereference('/' + competitorIntelligenceId);
            }
        } catch (Exception e) {
// *****            pageMessage = e.getMessage();
            return null;
        }
       
        System.debug('name = ' + CompetitorIntelligence.name);
        //System.debug('CompetitorIntelligence.Sales_Opportunity__r.Name = ' + CompetitorIntelligence.Sales_Opportunity__r.Name);
        System.debug('selectedProductName = ' + selectedProductName);
        Decimal temp_sbsd = 0;
        String temp_lead = String.valueOf(selectedProductLeadTime);
        if(temp_lead == '0' || temp_lead == null) temp_sbsd = 150;
        else temp_sbsd = selectedProductLeadTime;
        return new Pagereference('/' + competitorIntelligenceId);
    }

    public Pagereference searchProducts() {
        reset();
        if (component == null) {
            component = new ProductSearchComponent();
        }
       /* if (component.searchOption.equals('Search All') ) {
            flagSearchAll=true;
            component.searchText='';
        } 
        if (!component.searchOption.equals('Search All') ) {
            flagSearchAll=false;            
        } */
        if (Util.isBlank(component.searchText)) {
            pageMessage = 'Please enter a search criteria.';
            return null;
        } else if (component.searchText.length() < MIN_LENGTH_REQD) {
            pageMessage = 'The search criteria should be at least ' + MIN_LENGTH_REQD + ' characters';
            return null;
        }
        
        productList = component.searchProducts();
        if (component.searchOption != null ) {
            if (component.searchOption.equals('BPC_1__c')) {
                System.debug('Set Options for bpc1');
                setOptions(productList, bpc1Options, 'BPC1');
            } else if (component.searchOption.equals('BPC_2__c')) {
                System.debug('Set Options for bpc2');
                setOptions(productList, bpc2Options, 'BPC2');
            }  else if (component.searchOption.equals('BPC_3__c')) {
                System.debug('Set Options for bpc3');
                setOptions(productList, bpc3Options, 'BPC3');
            } else if (component.searchOption.equals('Search All')) {
                System.debug('Set Options for bpc3');
                setOptions(productList, bpc1Options, 'BPC1');
            }          
        }
        totalPageNumber = 0;    
        bindData(1); 
        if (productList.isEmpty()) {
            pageMessage = 'No products found for the search criteria.';
        }
        return null;
    }
    
    public Pagereference bpc3Selected() {
        if (Util.isBlank(bpc3)) {
            return bpc2Selected();
        }
        String query = 'Select ' + Util.join(ProductSearchComponent.FIELDS_TO_RETRIEVE,',',null) + ' from Product2 where BPC_3__c = :bpc3 And IsActive = true';
        if (bpc2 != null && (!bpc2.equals(''))) {
            query += ' and BPC_2__c = :bpc2';
        }
        if (bpc1 != null && (!bpc1.equals(''))) {
            query += ' and BPC_1__c = :bpc1';
        }
        //productList = Database.query('Select Id, Name, BPC_1__c, BPC_2__c, BPC_3__c, Family from Product2 where BPC_3__c = :bpc3 and BPC_2__c = :bpc2 and BPC_1__c = :bpc1' );
        productList = Database.query(query + ProductSearchComponent.ORDER_BY);
        totalPageNumber = 0;    
        bindData(1); 
        return null;
    }
    
    public Pagereference bpc2Selected() {
        if (Util.isBlank(bpc2)) {
            return bpc1Selected();
        }
        bpc3Options = new List<SelectOption>();
        bpc3Options.add(new SelectOption('', '-Select-'));
        String query = 'Select ' + Util.join(ProductSearchComponent.FIELDS_TO_RETRIEVE,',',null) + ' from Product2 where BPC_2__c = :bpc2 And IsActive = true';
        if (bpc1 != null && (!bpc1.equals(''))) {
            query += ' and BPC_1__c = :bpc1';
        }
        //productList = Database.query('Select Id, Name, BPC_1__c, BPC_2__c, BPC_3__c, Family from Product2 where BPC_2__c = :bpc2 and BPC_1__c = :bpc1' );
        productList = Database.query(query + ProductSearchComponent.ORDER_BY);
        Set<String> bpc3Set = new Set<String>();
        for(Product2 p : productList) {
            bpc3Set.add(p.BPC_3__c);
        }
        for (String s : bpc3Set) {
            bpc3Options.add(new SelectOption(s, s));
        }
        totalPageNumber = 0;    
        bindData(1); 
        return null;
    }
    
    public Pagereference bpc1Selected() {
        if (Util.isBlank(bpc1)) {
            return searchProducts();
        }
        bpc2Options = new List<SelectOption>();
        bpc2Options.add(new SelectOption('', '-Select-'));
        productList = Database.query('Select ' + Util.join(ProductSearchComponent.FIELDS_TO_RETRIEVE,',',null) + ' from Product2 where BPC_1__c = :bpc1 And IsActive = true' + ProductSearchComponent.ORDER_BY);
        Set<String> bpc2Set = new Set<String>();
        for(Product2 p : productList) {
            bpc2Set.add(p.BPC_2__c);
        }
        for (String s : bpc2Set) {
            bpc2Options.add(new SelectOption(s, s));
        }
        /*if (bpc2Options.size() > 0) {
            bpc2 = bpc2Options.get(0).getValue();
            return bpc2Selected();
        }*/
        totalPageNumber = 0;    
        bindData(1); 
        return null;
    }
    
    public List<SelectOption> getSearchOptions() {
        List<SelectOption> searchOptions = new List<SelectOption>();
        //searchOptions.add(new SelectOption('', '--Select--'));
        searchOptions.add(new SelectOption('Search All',  'Search All'));
        searchOptions.add(new SelectOption('BPC_1__c', 'BPC1'));
        searchOptions.add(new SelectOption('BPC_2__c', 'BPC2'));
        searchOptions.add(new SelectOption('BPC_3__c', 'BPC3'));
        searchOptions.add(new SelectOption('PRODUCT_FAMILY__c',  'EPP5'));
        //add Search All in PickList
       
        return searchOptions;
    }
    
    private void setOptions(List<Product2> productList, List<SelectOption> selectOptionList, String bpcIdentifier) {
        selectOptionList.clear();
        selectOptionList.add(new SelectOption('', '-Select-'));
        Set<String> bpcSet = new Set<String>();
        for (Product2 p : productList) {
            if (bpcIdentifier.equals('BPC1')) {
                bpcSet.add(p.BPC_1__c);
            } else if (bpcIdentifier.equals('BPC2')) {
                bpcSet.add(p.BPC_2__c);
            } else if (bpcIdentifier.equals('BPC3')) {
                bpcSet.add(p.BPC_3__c);
            }
        }
        for (String s : bpcSet) {
            selectOptionList.add(new SelectOption(s, s));
        }
        
/*      if (selectOptionList.size() > 0) {
            if (bpcIdentifier.equals('BPC1')) {
                bpc1 = selectOptionList.get(0).getValue();
                bpc1Selected();
            } else if (bpcIdentifier.equals('BPC2')) {
                bpc2 = selectOptionList.get(0).getValue();
                bpc2Selected();
            } else if (bpcIdentifier.equals('BPC3')) {
                bpc3 = selectOptionList.get(0).getValue();
                bpc3Selected();
            }
        }*/
        System.debug('bpcSet = ' + bpcSet);
        System.debug('bpc1Options = ' + bpc1Options.size());
    }
    
}