global class RecomboInboundWS {
    public class RecomboInboundWSException extends Exception {}
    
    WebService static string processAgreement(String acceptancedata) {
        Savepoint sp = Database.setSavepoint();
        try {
            String waybillId = extractWaybillId(acceptancedata);
            
            // Manually disabled Recombo expiration when processing acceptance.
            QuoteAgreementUtilities.expireInRecombo = false;
            
            Agreement__c[] agreements = QuoteAgreementUtilities.loadAgreementsByWaybill(waybillId);
            if (agreements == null || agreements.size() == 0) {
                throw new RecomboInboundWSException('Failed to load agreements for waybill ID ' + waybillId);
            }
            
            QuoteAgreementUtilities.processAcceptance(agreements[0].Id);
        } catch (Exception e) {
            Database.rollback(sp);
            QuoteUtilities.log(e, false);
            return e.getMessage();
        }
        return '<response>Success</response>';
    }
    
    public static string extractWaybillId(String acceptancedata) {
        if (acceptancedata == null) {
            throw new RecomboInboundWSException('Acceptance data cannot be null');
        }
        DOM.Document doc = new DOM.Document();
        doc.load(acceptancedata);
        Dom.XmlNode document = doc.getRootElement().getChildElement('document', null);
        Dom.XmlNode waybillId = document.getChildElement('waybillId', null);
        return waybillId.getText().trim();
    }

}