/*
QuoteExpireAgreementsSchedulable qeas = new QuoteExpireAgreementsSchedulable();
String sch = '0 10 * * * ?';
system.schedule('Expire Agreements on Expiry Date', sch, qeas); */
global class QuoteExpireAgreementsSchedulable implements Schedulable {
    global void execute(SchedulableContext SC) {
        try {
            System.debug('\r\n\r\nExecuting Scheduled Job: QuoteExpireAgreements\r\n');
            // This batch size must be 1:
                // otherwise, if one quote errors out the rest of the batch doesn't get updated.
                // This causes duplicate delete call attempts which cause more problems!
            Id batchInstanceId = Database.executeBatch(new QuoteExpireAgreementsBatchable(), 1); 
            System.debug('\r\nBatch Id: ' + batchInstanceId + '\r\n');
        } catch (Exception e) {
            QuoteUtilities.log(e);
        }
    }    
}