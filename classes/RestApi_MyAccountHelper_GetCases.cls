global class RestApi_MyAccountHelper_GetCases {

	public static final Id RT_RCID = Schema.SObjectType.Account.getRecordTypeInfosByName().get(PortalConstants.ACCOUNT_TYPE_RCID).getRecordTypeId();
	public static final String CUSTOMER_SEGMENT = 'SBS';
	public static final String DEFAULT_ORIGIN = RestApi_MyAccountHelper_Utility.DEFAULT_ORIGIN;
	public static final Id COMMUNITY_PROFILE_ID = [SELECT Id FROM Profile WHERE Name = :PortalConstants.PROFILE_CUSTOMER_COMMUNITY LIMIT 1].Id;
	public static final String CUSTOMER_PORTAL_NAME = 'My Account';

	@TestVisible private static final String ERROR_NO_RCIDS_TITLE = 'No RCIDs found';
	@TestVisible private static final String ERROR_NO_RCIDS_MSG = 'No RCIDs found for billing account associations.';

	// GetCasesRequestObject object classes
	public class RequestUser {
		public String uuid = '';
		public String firstName = '';
		public String lastName = '';
		public String email = '';
	}

	public class RequestBilling {
		public String accountIds = '';
	}

	public class AccountWrapper {
		public String accountNumber;
		public String accountType;
		public String accountSubType;

		public AccountWrapper(String accountNumber, String accountType, String accountSubType) {
			this.accountNumber = accountNumber;
			this.accountType = accountType;
			this.accountSubType = accountSubType;
		}
	}

	public class GetCasesRequestObject extends RestApi_MyAccountHelper.RequestObject {
		public List<String> filters;
		public RequestUser user;
		public List<String> billing; // changed from Requestbilling as we might have multiple
		public List<AccountWrapper> accounts;
		// public List<String> users;

	}
	// end GetCasesRequestObject classes

	// GetCasesResponse
	// derived child class to act as the JSON response
	// supported request type:
	//     RestApi_MyAccount.REQUEST_TYPE_GET_CASES
	global class GetCasesResponse extends RestApi_MyAccountHelper.Response {
		String userName = '';
		String contactName = '';
		String team10Characters = '';
		String sbsFlag = null;
		String masrEnabled = '';
		List<Case> userCases = null;

		public GetCasesResponse(GetCasesRequestObject request) {
			super(request);

			// check if user object in request defined
			if (request.user == null) {
				throw new xException('User object for request not defined');
			}
			system.debug('uuid:=' + request.user.uuid);
			// check if uuid specified
			if (String.isBlank(request.user.uuid)) {
				throw new xException('Field "uuid" in user object for request not defined');
			}

			// get user by uuid ( refers to the field "federationIdentifier" in the Salesforce User object)
			User u = RestApi_MyAccountHelper_Utility.getUser(request.user.uuid);
			system.debug('User:=' + u);
			Contact c;
			List<Account> relatedAccounts = getAccountsByBCANIds(request.billing);
			relatedAccounts = updateBillingAccounts(request.accounts, relatedAccounts);
			List<Contact_Linked_Billing_Account__c> clbas;
			if (u != null) {
				// if user found
				userName = u.username;
				// get contact associated with user
				c = RestApi_MyAccountHelper_Utility.getContact(u.contactId);
				clbas = getRelatedContactLinkedBillingAccountsByContact(c);

				// update contact linked billing accounts
				updateContactLinkedBillingAccounts(request.billing, c, clbas);
				clbas = getRelatedContactLinkedBillingAccountsByContact(c); // Need refreshed formula values

				// if contact exists
				if (c != null) {
					contactName = c.name;
					// get cases

					// update contact
					c = updateContact(c, request.user, isMASRElegible(clbas), isEPPCustomer(clbas));

					// update user - need to send in ID and a Data Structure that is not a SObject or Class to complete in a future class to avoid mixed DML Operations Issues.
					Map<String, String> tempUser = new Map<String, String>();
					tempUser.put('email', request.user.email);
					tempUser.put('firstName', request.user.firstName);
					tempUser.put('lastName', request.user.lastName);
					RestApi_MyAccountHelper_Utility.updateUser(u.Id, tempUser);

					// Set return variables
					team10Characters = c.Account.Sub_Segment_Code__r.Team_10Characters__c;
					if (c.MASR_Enabled__c) {
						setSuccessVariables(request, c);
					} else {
						setFailureVariables();
					}
				}
			} else {
				// User doesn't exist yet - ALWAYS CREATE A CONTACT + USER AND UPDATE LINKED BILLING ACCOUNTS

				// Determine the account that the user should be associated to
				if (!relatedAccounts.isEmpty()) {

					// See if we have any EPP, or SBS billing accounts
					Account provisioningAccount = getProvisioningAccount(relatedAccounts);

					if (provisioningAccount != null) {
						c = createContact(request.user, provisioningAccount.ParentId);

						updateContactLinkedBillingAccounts(request.billing, c, new List<Contact_Linked_Billing_Account__c>());
						clbas = getRelatedContactLinkedBillingAccountsByContact(c); // Need refreshed formula values

						// Need to update the contact now that we have the EPP/MASR/SBS formula values
						c = updateContact(c, request.user, isMASRElegible(clbas), isEPPCustomer(clbas));

						createCommunityUser(request, c);

						// Set return variables
						contactName = c.firstname + ' ' + c.lastname;
						team10Characters = provisioningAccount.Parent == null ?
								provisioningAccount.Sub_Segment_Code__r.Team_10Characters__c : provisioningAccount.Parent.Sub_Segment_Code__r.Team_10Characters__c;
						if (c.MASR_Enabled__c) {
							setSuccessVariables(request, c);
						} else {
							setFailureVariables();
						}
					} else {
						setErrorVariables();
					}
				} else {
					// Could not find link for provisioning request, return empty case list and set appropriate response variables
					setErrorVariables();
				}
			}
		}

		// update contact fields
		private Contact updateContact(Contact c, RequestUser user, Boolean isMASREnabled, Boolean isEPP) {
			if (user.email != null && !PortalUtils.validateEmail(user.email)) {
				throw new xException('Invalid email address.');
			}

			// update contact fields if specified in the user section of the request
			c.firstName = user.firstName != null ? user.firstName : c.firstName;
			c.lastName = user.lastName != null ? user.lastName : c.lastName;
			c.email = user.email != null ? user.email : c.email;
			c.MASR_Enabled__c = isMASREnabled;
			c.EPP__c = isEPP;

			try {
				update c;
				return c;
			} catch (DMLException e) {
				throw new xException('Update Contact :' + e.getMessage());
			} catch (Exception e) {
				throw new xException(e.getMessage());
			}
		}

		/*
		* Need to create a Contact in order to create a User
		*
		*/
		private Contact createContact(RequestUser u, Id accountId) {
			try {
				Contact c = new Contact(
						firstName = u.firstName,
						lastName = u.lastName,
						email = u.email,
						accountId = accountId
				);
				insert c;
				return c;
			} catch (DmlException e) {
				throw new xException('Create Contact: ' + e.getMessage());
			} catch (Exception e) {
				throw new xException('Create Contact: ' + e.getMessage());
			}
		}

		private void updateContactLinkedBillingAccounts(List<String> billing, Contact c, List<Contact_Linked_Billing_Account__c> relatedCLBAs) {
			List<Account> baccounts;
			if (billing == null) {
				baccounts = new List<Account>();
			} else {
				baccounts = getAccountsByBCANIds(billing);
			}
			Map<String, Id> baccountmap = new Map<String, Id>();
			for (Account a : baccounts) {
				baccountmap.put(a.BAN_CAN__c, a.id);
			}

			List<Contact_Linked_Billing_Account__c> toUpsert = new List<Contact_Linked_Billing_Account__c>();

			// Deactivate all results
			for (Contact_Linked_Billing_Account__c ba : relatedCLBAs) {
				ba.Active__c = false;
			}

			// Activate those that are mention in the string
			if (billing != null) {
				for (String bId : billing) {
					Boolean inList = false;
					Id billingId = null;
					for (Contact_Linked_Billing_Account__c ba : relatedCLBAs) {
						if (bId == ba.Billing_Account__r.BAN_CAN__c) {
							inList = true;
							ba.Active__c = true;
							break;
						}
					}
					if (!inList) {
						if (baccountmap.containsKey(bId)) {
							toUpsert.add(new Contact_Linked_Billing_Account__c(Active__c = true, Billing_Account__c = baccountmap.get(bId), Contact__c = c.Id));
						} else {
							// WHAT DO WE DO IF THE BILLING ACCOUNT ID IS NOT VALID?
						}
					}
				}
			}
			toUpsert.addAll(relatedCLBAs);

			try {
				upsert toUpsert;
			} catch (DMLException e) {
				throw new xException('Update Billing : ' + e.getMessage());
			}
		}

		private List<Account> getAccountsByBCANIds(List<String> aIds) {
			if (aIds == null) {
				return new List<Account>();
			}
			Set<String> billingSystemIds = new Set<String>();
			Set<String> CANIds = new Set<String>();

			for (String s : aIds) {
				List<String> splitString = s.split('-');
				if (splitString.size() == 2) {
					billingSystemIds.add(splitString[0]);
					CANIds.add(splitString[1]);
				}
			}

			List<Account> billingAccounts = [
					SELECT Id, ParentId, Parent.RecordTypeId, Parent.Sub_Segment_Code__r.Cust_Segment_A__c,
							Parent.Sub_Segment_Code__r.Team_10Characters__c, Billing_Account_Type__c, Billing_Account_SubType__c,
							BAN_CAN__c, Sub_Segment_Code__r.Team_10Characters__c, Billing_System_ID__c, CAN__c
					FROM Account
					WHERE BAN_CAN__c IN :aIds
					AND (RecordTypeId = :PortalConstants.ACCOUNT_RECORDTYPE_BAN OR RecordTypeId = :PortalConstants.ACCOUNT_RECORDTYPE_CAN)

			];

			// Since the query may find accounts with similiar Billing System / CAN we need to filter to only have exact matches
			Integer i = 0;
			while (i < billingAccounts.size()) {
				Account a = billingAccounts[i];
				if (!aIds.contains(a.BAN_CAN__c) && !aIds.contains(a.Billing_System_ID__c + '-' + a.CAN__c)) {
					billingAccounts.remove(i); // Remove current account
				} else {
					i++;
				}
			}

			return billingAccounts;
		}

		private List<Contact_Linked_Billing_Account__c> getRelatedContactLinkedBillingAccountsByContact(Contact c) {
			return [
					SELECT Id, Active__c, Billing_Account__c, Billing_Account__r.BAN_CAN__c, MASR_Enabled__c, Billing_Account__r.Parent.RecordTypeId,
							Billing_Account__r.Sub_Segment_Code__r.Team_10Characters__c, EPP__c, Billing_Account__r.ParentId, Billing_Account__r.Parent.Sub_Segment_Code__r.Team_10Characters__c
					FROM Contact_Linked_Billing_Account__c
					WHERE Contact__c = :c.Id
			];

		}

		private Account getProvisioningAccount(List<Account> relatedAccounts) {
			// First see if we have any EPP accounts
			Account sbsAccount;
			Account rcidAccount;
			for (Account a : relatedAccounts) {
				if (a.Parent.RecordTypeId == RT_RCID) {
					if (a.Billing_Account_Type__c == 'C' && a.Billing_Account_SubType__c == 'E') {
						// Found EPP Account, use this to drive contact/user creation
						return a;
					}
					if (a.Parent.Sub_Segment_Code__r.Cust_Segment_A__c == CUSTOMER_SEGMENT && sbsAccount == null) {
						// Found SBS Account, use this to drive contact/user creation IFF we end up not finding an EPP account
						sbsAccount = a;
					}
					if (rcidAccount == null) {
						rcidAccount = a;
					}
				}
			}

			return sbsAccount != null ? sbsAccount : rcidAccount != null ? rcidAccount : null;
		}

		private List<Account> updateBillingAccounts(List<AccountWrapper> requestAccounts, List<Account> relatedAccounts) {
			if (requestAccounts == null) {
				return new List<Account>();
			}
			List<Account> accountsToUpdate = new List<Account>();
			for (AccountWrapper aw : requestAccounts) {
				for (Account a : relatedAccounts) {
					if (aw.accountNumber == a.BAN_CAN__c) {
						a.Billing_Account_Type__c = aw.accountType;
						a.Billing_Account_SubType__c = aw.accountSubType;
						accountsToUpdate.add(a);
						break;
					}
				}
			}
			update accountsToUpdate;
			return accountsToUpdate;
		}

		private Boolean isMASRElegible(List<Contact_Linked_Billing_Account__c> relatedCLBAs) {
			for (Contact_Linked_Billing_Account__c clba : relatedCLBAs) {
				if (clba.MASR_Enabled__c && clba.Active__c) {
					return true;
				}
			}
			return false;
		}

		private Boolean isEPPCustomer(List<Contact_Linked_Billing_Account__c> relatedCLBAs) {
			for (Contact_Linked_Billing_Account__c clba : relatedCLBAs) {
				if (clba.EPP__c && clba.Active__c) {
					return true;
				}
			}
			return false;
		}

		private void createCommunityUser(GetCasesRequestObject request, Contact c) {
			Map<String, String> tempContact = new Map<String, String>();
			tempContact.put('email', request.user.email);
			tempContact.put('firstName', request.user.firstName);
			tempContact.put('lastName', request.user.lastName);
			tempContact.put('id', c.id);
			RestApi_MyAccountHelper_Utility.createCommunityUser(request.user.uuid, tempContact);
		}

		private void setSuccessVariables(GetCasesRequestObject request, Contact c) {
			List<String> internalFilters = RestApi_MyAccountHelper_Utility.convertListStatusToInternal(request.filters);
			List<Case> allCasesForUser = RestApi_MyAccountHelper_Utility.getCasesForUserWithFilters(c.Id, request.billing, internalFilters);
			userCases = RestApi_MyAccountHelper_Utility.convertListStatusToExternal(allCasesForUser, internalFilters);
			masrEnabled = 'Yes';
			sbsFlag = 'Yes';
		}

		@TestVisible
		private void setFailureVariables() {
			userCases = new List<Case>(); // Return empty list
			masrEnabled = 'No';
			sbsFlag = 'No';
		}

		@TestVisible
		private void setErrorVariables() {
			userCases = new List<Case>();
			masrEnabled = null;
			sbsFlag = null;
			error = new RestApi_MyAccountHelper.Error(ERROR_NO_RCIDS_TITLE, ERROR_NO_RCIDS_TITLE);
			return;
		}
	}

	public GetCasesResponse response = null;

	public RestApi_MyAccountHelper_GetCases(String requestString) {
		// convert request to object
		GetCasesRequestObject getCasesRequest = (GetCasesRequestObject) JSON.deserialize(requestString, GetCasesRequestObject.class);

		response = new GetCasesResponse(getCasesRequest);
	}
}