public class Recombo {
	public class Person {
		public String uid {get; set;}
	    public String firstName {get; set;}
	    public String lastName {get; set;}
	    
	    public Person() {
	    	this(null,null,null);
	    }
	    
	    public Person(String uid, String firstName, String lastName) {
	    	this.uid = uid;
	    	this.firstName = firstName;
	    	this.lastName = lastName;
	    }
	}
	
	public class EmailInfo {
		public String subject {get; set;}
	    public String message {get; set;}
	    
	    public EmailInfo() {
	    	this(null,null);
	    }
	    
	    public EmailInfo(String subject, String message) {
	    	this.subject = subject;
	    	this.message = message;
	    }
	}
	
	public class Document {
		public String id {get; private set;}
		public Blob body {get; private set;}
		public String fileName {get; private set;}
		public String contentType {get; private set;}
		
		public Document(String id, String fileName, String contentType, Blob body) {
			this.id = id;
			this.fileName = fileName;
			this.contentType = contentType;
			this.body = body;
		}
	}
	
	public class LoginException extends Exception {}
	
	public class SendException extends Exception {
		public String errorCode {get; set;}
		public String errorMessage {get; set;}
	}
	
	
	// TESTS
	
	private static testMethod void testPersonDefaultCtor() {
		Person p = new Person();
		system.assertEquals(null,p.uid);
		system.assertEquals(null,p.firstName);
		system.assertEquals(null,p.lastName);
	}
	
	private static testMethod void testPersonCtorWithParams() {
		final String UID = 'uid';
		final String FIRST_NAME = 'first';
		final String LAST_NAME = 'last';
		
		Person p = new Person(UID, FIRST_NAME, 'last');
		system.assertEquals(UID,p.uid);
		system.assertEquals(FIRST_NAME,p.firstName);
		system.assertEquals(LAST_NAME,p.lastName);
	}
	
	private static testMethod void testEmailInfoDefaultCtor() {
		EmailInfo ei = new EmailInfo();
		system.assertEquals(null,ei.subject);
		system.assertEquals(null,ei.message);
	}
	
	private static testMethod void testEmailInfoCtorWithParams() {
		final String SUBJECT = 'subject';
		final String MESSAGE = 'message';
		
		EmailInfo ei = new EmailInfo(SUBJECT, MESSAGE);
		system.assertEquals(SUBJECT, ei.subject);
		system.assertEquals(MESSAGE, ei.message);
	}
	
	private static testMethod void testDocument() {
		final String ID = 'id';
		final Blob BODY = Blob.valueOf('body');
		final String FILE_NAME = 'filename';
		final String CONTENT_TYPE = 'type';
		
		Document doc = new Document(ID, FILE_NAME, CONTENT_TYPE, BODY);
		system.assertEquals(ID, doc.id);
		system.assertEquals(FILE_NAME, doc.fileName);
		system.assertEquals(CONTENT_TYPE, doc.contentType);
		system.assertEquals(BODY, doc.body);
	}
	
	private static testMethod void testSendException() {
		SendException se = new SendException();
		se.errorCode = 'test';
		se.errorMessage = 'text';
	}
}