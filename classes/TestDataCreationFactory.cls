/****************
 * Arpit : 2-Dec-2016
 * Sprint/Release : Sprint 9
 * Description : This class will eb used a respository for all test data creation methods 
 *****************/
public class TestDataCreationFactory {

    //Inserts "numAccts" RCID Account
    public static List<Account> createRCIDAccounts(Integer numAccts) {
         List<Account> accList = new List<Account>();
        //Map<string,Account> accMap = new map<string,Account>();
        for(Integer i=0; i<numAccts; i++) {
            Account acc = new Account(Name='TestAccount' + i, CustProfId__c='123456' );                    
            //acc.RCID__c = String.valueOf((Integer)Math.floor(Math.random()*10+i));
            acc.RCID__c = String.valueOf('123'+i);
            acc.RecordTypeId = getAccountRecordTypeId('RCID');
            //accMap.put(acc.RCID__c,acc);
            accList.add(acc);
            system.debug('Account----------->>>>>>>>>>'+ acc);
        }
       insert accList;
       return accList;
        //insert accMap.values();
        //return accMap.values();
    }
    
    //Creats account of 'recordType' type
    public static List<Account> createAccounts(Integer numAccts, String recordType) {
        List<Account> accList = new List<Account>();
        for(Integer i=0; i<numAccts; i++) {
            Account acc = new Account(Name='TestAccount' + i, CustProfId__c='123456' );                    
            //acc.RCID__c = String.valueOf((Integer)Math.floor(Math.random()*10+i));
            acc.RCID__c = String.valueOf('123'+i);
            acc.RecordTypeId = getAccountRecordTypeId(recordType);
            accList.add(acc);
        }
        insert accList;
        return accList;
    }
    
    
    //Inserts "conNum" contacts on "acc" Account object
    public static List<Contact> addContactOnAccount(Account acc , Integer conNum){
        List<Contact> contactList = new List<Contact>();
        for(Integer i=0; i<conNum; i++) {
            Contact conObj =  new Contact() ;
            conObj.FirstName = 'TestFirstName' + i ;
            conObj.LastName = 'TestLastName' + i ;
            
            conObj.AccountId = acc.Id ;
            conObj.Email = conObj.LastName + '@test.com' ;
            conObj.Phone = '12345666' ;
            contactList.add(conObj) ;
        }
        insert contactList ;
        return contactList ;
                       
    }
    
    
    public static Order createOrder(Account acc){
        Order orderObj =  new Order();
        List<Contact> conList =  TestDataCreationFactory.addContactOnAccount(acc, 1);
        system.debug('conList ------>' + conList);
        orderObj.AccountId = acc.id ;
        orderObj.Status = 'Draft';
        orderObj.EffectiveDate  = system.today() ;
        orderObj.CurrencyIsoCode = 'CAD' ;
        //system.debug('conList[0].Id ------>' + conList[0].Id);
        //orderObj.CustomerAuthorizedBy.Id = [Select Id from Contact where id =: conList[0].Id].id;
        insert orderObj ;
        system.debug('conList[0].Name ---->'  + conList[0].Name);
        orderObj.CustomerAuthorizedById = conList[0].Id ;
        update orderObj ;
        system.debug('orderObj ------->' +orderObj);
        return orderObj ;
    }
    
    //Creates user
     public static List<User> getUsers(Integer numOfUsersReq, String profileName){
        List<User> userList = new List<User>();
        for(Integer counter=0; counter<numOfUsersReq; counter++) {
            User user = new User();
            user.alias = 'user1' + counter;
            user.username='user.alias'+'@testing.com'; 
            user.email= 'user.alias'+'@testorg.com'; 
            user.emailencodingkey='UTF-8'; 
            user.lastname='testUser'+ counter; 
            user.languagelocalekey='en_US'; 
            user.localesidkey='en_US';
            List<Profile> prof = [Select Id from profile where name= :profileName LIMIT 1];
            user.profileid = prof[0].Id; 
            user.timezonesidkey='America/Los_Angeles';
            userList.add(user);
        }
        return userList;
    }
    /***********************************************************************************************************************
    * @author :  Arpit Goyal
    * @date : 2-Dec-2016
    * @param : String
    * @return : Id
    * @description : returns the recordTypeID for the record type name for Account
    * <B>Sprint:</B></BR>  
    ***********************************************************************************************************************/
    public static Id getAccountRecordTypeId(String recordTypeName) {
     
        Id recordTypeId;
        if(recordTypeName != null && recordTypeName != '') {
            
            
            try{
                Map<String,Schema.RecordTypeInfo> mapRecTypeInfo = Schema.SObjectType.Account.getRecordTypeInfosByName();
                system.debug('----->getRecordTypeId(), mapRecTypeInfo : ' + mapRecTypeInfo);
                
                if(mapRecTypeInfo != null) {
                    Schema.RecordTypeInfo recTypeInfo = mapRecTypeInfo.get(recordTypeName);
                    recordTypeId = recTypeInfo.getRecordTypeId();
                }
            }catch(Exception e){
                system.debug('Error occured while returning recordId' + e.getStackTraceString());
            }
            
        }
        system.debug('----->getRecordTypeId(), returning recordTypeId : ' + recordTypeId);
        return recordTypeId;
    }
    
    //Adds SMBCare_Address__c on Account
    public static SMBCare_Address__c createSMBCareAddressOnAccount(Account acc){
        SMBCare_Address__c addressObj = new SMBCare_Address__c();
        addressObj.Account__c = acc.id ;
        addressObj.Service_Account_Id__c = acc.Id ;
        
        addressObj.address_type__c= OrdrConstants.ADDRESS_TYPE_STANDARD;
        addressObj.Suite_Number__c='10T2';
        addressObj.Street_Number__c='3777';
        addressObj.Street_Name__c='Kingsway';                  
        addressObj.Street_Type_Code__c='ST';
        addressObj.City__c='Burnaby';
        addressObj.Municipality_Name__c='Burnaby'; 
        addressObj.Province__c='BC'; 
        addressObj.Postal_Code__c='V5H 3Z7';
        addressObj.FMS_Address_ID__c='1919282';
        addressObj.Location_Id__c='3243244';
        addressObj.Status__c = 'Valid' ; 
        return addressObj;       
    }
    
    public static List<Opportunity> createOpprtunityOnAccount(Integer numOpps, String accountId) {
        
        List<Opportunity> opptyList = new List<Opportunity>();
        for(Integer i=0; i<numOpps; i++) {
            Opportunity o = new Opportunity(Name='TestOpp' + i, StageName='Prospecting', CloseDate=System.today().addMonths(1), AccountId=accountId);
            opptyList.add(o);
        }
        insert opptyList;
        return opptyList;
    }
    
    public static PriceBook2 createPriceBook(){
        PriceBook2 pbook = new PriceBook2(Name = 'testPriceBook', isActive =  true);
        insert pBook ;
        return pBook ;
    }
    
    public static PricebookEntry createPricebookEntry(Id pricebookId,Product2 p){
        PricebookEntry pbe = new PricebookEntry(product2id = p.id, unitprice = 1, usestandardprice = false, isActive = true, pricebook2id = pricebookId);
        insert pbe;
        return pbe;
    }
    
    public static Quote createQuote( Id opportunityId, Id priceBookId ){
        Quote quote = new Quote(Name='Test Quote',OpportunityId = opportunityId);
        quote.Pricebook2Id = priceBookId;
        insert quote;
        System.debug('Quote ='+  quote);
        return quote;
    }
    
    
    
}