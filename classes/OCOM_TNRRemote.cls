/*
    *******************************************************************************************************************************
    Class Name:     OCOM_TNRRemote 
    Purpose:        Controller for TNR  Apex Remote       
    Created by:     Niran Kanagan       29-Sept-2016
	Updated by:     Niran Kanagan       21-Feb-2017     QC-10623        fixed bug related smartring portability check
    				Niran Kanagan      	10-Mar-2017     QC-11164  		Add TNR Capability to "Change Phone" offer
    *******************************************************************************************************************************
*/
global with sharing class OCOM_TNRRemote 
{
   // public vlocity_cmt.CardCanvasController controllerHybridcpqRef;
    Public OCOM_TNRRemote(){}
    
    public OCOM_TNRRemote(Object controller) {
            
       // this.controllerHybridcpqRef=controller;
        
    }
    
    @RemoteAction
    global static AutoSelectTNRemoteResponse autoSelectTN(ID orderId, ID itemId, String attributeId, String provGroup)
    {
        System.debug('##TNR##: START OCOM_TNRRemote.autoSelectTN');
        
        OCOM_TNRController tnr = new OCOM_TNRController(orderId, itemId, attributeId, provGroup);
        return mapResponse(tnr.autoSelectTN());
        
        //System.debug('##TNR##: END OCOM_TNRRemote.autoSelectTN');
    }
    
    @RemoteAction
    global static string releaseTN(ID orderId, ID itemId, String attributeId, String provGroup)
    {
        System.debug('##TNR##: START OCOM_TNRRemote.releaseTN');
        OCOM_TNRController tnr = new OCOM_TNRController(orderId, itemId, attributeId, provGroup);
        tnr.releaseTN();
        return '';
    }
    
    @RemoteAction
    global static TnPortabilityCheck tnPortabilityCheck(ID orderId, ID itemId, String tn, String provGroup)
    {
        System.debug('##TNR##: START OCOM_TNRRemote.tnPortabilityCheck for: '+tn);
        
        OrdrTnReservationType lineType = tnLineType(itemId, provGroup);
        try
        {
            if(lineType != null)
            {
                String formatedTN = tn.replace(' ', '').replace('-', '');
                Order orderObj = [select id, Service_Address__c from Order where id =:orderId];
                OrdrTechnicalAvailabilityManager.TnPortability tnPort;
                if(lineType == OrdrTnReservationType.WLN)
                {
                    System.debug('##TNR##: OCOM_TNRRemote.tnPortabilityCheck: Wireline');
                    if(!Test.isRunningTest())
                    	tnPort = OrdrTechnicalAvailabilityManager.checkWirelineTnPortability(orderObj.Service_Address__c, formatedTN);
                    else
                    {
                        tnPort = new OrdrTechnicalAvailabilityManager.TnPortability();
                        tnPort.portableIndicator = 'true';
                    }
                }/*
                else if (lineType == OrdrTnReservationType.WLS)
                {
                    System.debug('##TNR##: OCOM_TNRRemote.tnPortabilityCheck: Wireless');
                    if(!Test.isRunningTest())
                    	tnPort = OrdrTechnicalAvailabilityManager.checkWirelessTnPortability(formatedTN, 'productType', 'brandId');
                    else
                    {
                        //tnPort = new OrdrTechnicalAvailabilityManager.TnPortability();
                        //tnPort.portableIndicator = 'true';
                        tnPort = null;
                    }
                }*/
                else
                {
                    System.debug('##TNR##: OCOM_TNRRemote.tnPortabilityCheck: Not Available for '+lineType.name());
                    tnPort = null;
                }
                
                if(tnPort != null)
                {
                    TnPortabilityCheck response = mapResponse(tnPort);
                    System.debug('##TNR##: END OCOM_TNRRemote.tnPortabilityCheck');    
                    return response;
                }
            }
            return mapErrorResponse(null);
        }
        catch(Exception e)
        {
            system.debug('##TNR##: END OCOM_TNRRemote.tnPortabilityCheck: !!!exception!!! '+e);
            return mapErrorResponse(e);
        }
    }
    
    public Static OrdrTnReservationType tnLineType(Id itemId, String provGroupNcId)
    {
        System.debug('##TNR##: START OCOM_TNRRemote.getLineType');
        OCOM_TNR_Settings__c lineSettings = OCOM_TNR_Settings__c.getOrgDefaults();
        //QC-11164 Start
        if(String.isNotBlank(provGroupNcId) && !provGroupNcId.equalsIgnoreCase('undefined'))
        {
            if(provGroupNcId.equalsIgnoreCase(lineSettings.WLN_Provisioning_Group__c))
			{
                System.debug('##TNR##: OCOM_TNRRemote.getLineType: Line Type is Wireline');
                return OrdrTnReservationType.WLN;
            }
            else if(provGroupNcId.equalsIgnoreCase(lineSettings.WLS_Provisioning_Group__c))
            {
                System.debug('##TNR##: OCOM_TNRRemote.getLineType: Line Type is Wireless');
                return OrdrTnReservationType.WLN;
            }
            else if(provGroupNcId.equalsIgnoreCase(lineSettings.LDIT_Provisioning_Group__c))
            {
                System.debug('##TNR##: OCOM_TNRRemote.getLineType: Line Type is Toll-Free Number');
                return OrdrTnReservationType.TOLLFREE;
            }
        }
        //QC-11164 - End
        List<OrderItem> items = [select id, vlocity_cmt__JSONAttribute__c from OrderItem where Id =:itemId];
        
        if(items == null || items.size()<1)
            return null;
        OrderItem item = items.get(0);
        if(String.isNotBlank(item.vlocity_cmt__JSONAttribute__c))
        {
            Map<String,OCOM_VlocityJSONParseUtil.JSONWrapper> jsonMap = OCOM_VlocityJSONParseUtil.getAttributesFromJSON(item.vlocity_cmt__JSONAttribute__c);
            for(String key : jsonMap.keySet())
            {
                if(key == lineSettings.Line_Type__c)
                {
                    OCOM_VlocityJSONParseUtil.JSONWrapper data = jsonMap.get(key);
                    if(data !=null && String.isNotBlank(data.value) && (data.value == lineSettings.Single_Line__c || data.value == lineSettings.Overline__c || data.value == lineSettings.Pilot__c))
                    {
                        System.debug('##TNR##: OCOM_TNRRemote.getLineType: Line Type is Wireline');
                        return OrdrTnReservationType.WLN;
                    }
                    /*if(String.isNotBlank(data.value) && data.value == 'Wireless')
                    {
                        System.debug('##TNR##: OCOM_TNRRemote.getLineType: Line Type is Wireless');
                        return OrdrTnReservationType.WLS;
                    }*/
                }
                if(key == lineSettings.Toll_Free_Number__c)
                {
                    System.debug('##TNR##: OCOM_TNRRemote.getLineType: Line Type is Toll-Free Number');
                    return OrdrTnReservationType.TOLLFREE;           
                }
                //Niran: QC-10623 - Start
                if(key == lineSettings.Offering_ID__c)
                {
                     OCOM_VlocityJSONParseUtil.JSONWrapper data = jsonMap.get(key);
                    if(data != null && String.isNotBlank(data.value) && (data.value.containsIgnoreCase(lineSettings.Smart_Ring__c)))
                    {
                        System.debug('##TNR##: OCOM_TNRRemote.getLineType: Line Type is Wireline (Smart Ring)');
                        return OrdrTnReservationType.WLN;
                    }
                }
                //Niran: QC-10623 - End
            }
            System.debug('##TNR##: OCOM_TNRRemote.getLineType: Unable to identify Line Type');
            return null;
        }
        else
        {
            System.debug('##TNR##: OCOM_TNRRemote.getLineType: JSON is missing');
            return null;
        }  
    }
    public static AutoSelectTNRemoteResponse mapResponse (OCOM_TNRController.AutoSelectTNResponse source)
    {
        AutoSelectTNRemoteResponse out = new AutoSelectTNRemoteResponse();
        out.tn = source.tn;
        out.isError = source.isError;
        out.errorMsg = source.errorMsg;
        System.debug('##TNR##: OCOM_TNRRemote.AutoAssign: TN:'+out.tn+' isError'+out.isError+' errorMsg'+ out.errorMsg);
        return out;
    }
    
    public static TnPortabilityCheck mapResponse(OrdrTechnicalAvailabilityManager.TnPortability source)
    {
        TnPortabilityCheck out = new TnPortabilityCheck();
        /*
         //WLN
        out.phoneNumber = source.phoneNumber;
        out.portableIndicator = source.portableIndicator;
        out.localRoutingNumber = source.localRoutingNumber;
        out.previousLocalServiceProviderRequiredIndicator = source.previousLocalServiceProviderRequiredIndicator;
        out.telusNpaNxxIndicator = source.telusNpaNxxIndicator;
        out.useableIndicator = source.useableIndicator;
        //WLS
        out.oldServiceProviderId = source.oldServiceProviderId;
        out.cdmaPrepaidCoverage = source.cdmaPrepaidCoverage;
        out.cdmaPostpaidCoverage = source.cdmaPostpaidCoverage;
        out.cdmaCoverage = source.cdmaCoverage;
        out.hspaPrepaidCoverage = source.hspaPrepaidCoverage;
        out.hspaPostpaidCoverage = source.hspaPostpaidCoverage;
        out.hspaCoverage = source.hspaCoverage;
        out.mikeCoverage = source.mikeCoverage;
        out.npDirectionIndicator = source.npDirectionIndicator;
        out.currentBrand = source.currentBrand;
        out.responseCode = source.responseCode;
        out.platform = source.platform;
        */
        out.isError = false;
        out.isPortable = false;
        out.errorMsg = null;
            
        if(String.isNotBlank(source.portableIndicator) && source.portableIndicator.toLowerCase() == 'true')
        {
            out.isPortable = true;
        }
        return out;
    }
    
    public static TnPortabilityCheck mapErrorResponse(Exception e)
    {
        TnPortabilityCheck out = new TnPortabilityCheck();
        out.isError = true;
        out.isPortable = false;
        //Niran: QC-10623 - Start
        if(e == null)
            out.errorMsg= Label.OCOM_Unable_to_identify_the_Line_Type;//Unable to identify the Line Type of the selected voice product. Please raise a TELUSFroce case.
        else
        	out.errorMsg = Label.OCOM_An_unexpected_error_code_was_received;//'An unexpected error code was received. The reservation system is likely unavailable. Please try again';
        //Niran: QC-10623 - End
        return out; 
    }
    global class TnPortabilityCheck 
    {
        public boolean isPortable;
        public boolean isError;
        public String  errorMsg;
        
        //WLN
        public String phoneNumber;
        public String portableIndicator;
        public String localRoutingNumber;
        public String previousLocalServiceProviderRequiredIndicator;
        public String telusNpaNxxIndicator;
        public String useableIndicator;
        //WLS
        public String oldServiceProviderId;
        public String cdmaPrepaidCoverage;
        public String cdmaPostpaidCoverage;
        public String cdmaCoverage;
        public String hspaPrepaidCoverage;
        public String hspaPostpaidCoverage;
        public String hspaCoverage;
        public String mikeCoverage;
        public String npDirectionIndicator;
        public String currentBrand;
        public String responseCode;
        public String platform;
        
    }
    
    global class AutoSelectTNRemoteResponse
    {
        public String tn;
        public boolean isError;
        public String  errorMsg;
    }
    
}