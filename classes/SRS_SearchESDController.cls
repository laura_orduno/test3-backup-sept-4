/******************************************************************************
 * Project      : SRS R3
 * Class        : SRS_SearchESDController
 * Description  : Apex class for SRS_SearchESD VF page
 * Author       : Hari Neelalaka, IBM
 * Modification Log:
 * ---------------------------------------------------------------------------
 * Ver      Date                Modified By           Description
 * ---------------------------------------------------------------------------
 * 
 * ---------------------------------------------------------------------------
 * EsdServiceUser
******************************************************************************/
public class SRS_SearchESDController {
    //Traction Added 07-14-2015
    private static final String SERVICE_REQUEST_RECORD_TYPE_NAME = 'SRRTNAME';
    public String demarcationLocation {get; set;}
    public String endCustomerName {get; set;}
    public String npaNXX {get; set;}
    public String location {get; set;}
    public String cli {get; set;}
    public String existingWorkingTN {get; set;}
    public String siteReady {get; set;}
    public String buildingName {get; set;}
    public String newBuidling {get; set;}
    public String floor {get; set;}
    public String cabinet {get; set;}
    public String rack {get; set;}
    public String shelf {get; set;}
    public Boolean isRecordSelected {get; set;}
    public String checkBoxStatus {get; set;}
    public String postalCode {get; set;}
    public String srRecordTypeName {get; set;}
    private static final String NONE = '--NONE--';
    //END

    public Service_Request__c srvcReq {get;set;}
    public String serviceType {get;set;}
    public String serviceLocationCity {get;set;}
    public Integer CSID {get;set;}
    public List<wrapper> esdList {get;set;} 
    public Id SRId = ApexPages.currentPage().getParameters().get('SRID');
    public String srAcctId = ApexPages.currentPage().getParameters().get('ACCTID');
    public Boolean esdFlag {get;set;}    
    public boolean disabled {get;set;}
    public String id {get;set;}
    public Integer i;
    public Boolean renderShowandCancel{get;set;}  
    Boolean chkAll = false;
    public String index {
            get;
            // *** setter is NOT being called ***
            set {
                index = value;
                System.debug('value: '+value);
            }
        }        
    public Integer checkbox_count;
    /*
    * Called from the VF page's checkbox when page loads, Populates the page with checkboxes next to the Status 
    */
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();  
        options.add(new SelectOption('1','Design'));  // selectOption = (value,label)
        options.add(new SelectOption('2','Terminated '));
        options.add(new SelectOption('3','Operational '));        
        options.add(new SelectOption('4','Suspended '));
        options.add(new SelectOption('5','Working In Progress'));
        options.add(new SelectOption('6','All Statuses'));
        return options; 
    }
    String[] status = new String[]{'3'}; //holds the values of the selected checkboxes & Default state: ‘Operational’ selected
    /*
    *   returns the selected list back to the VF page
    */
    public String[] getStatus() {
        return status;  
    }    
    /*
    *   stores the selected list from the VF page in the controller
    */
    public void setStatus(String[] status) {        
        this.status = status;       
        system.debug('@@@ Status selected:'+status);
        checkbox_count = status.size();
        system.debug('@@@: '+checkbox_count);       
    }   
    
    public boolean doSort= false;
    public String sortDirection = 'DESC';
    public String sortExp = 'CSID';
    // Default sorting field --> CSID
    public static string SORT_FIELD ='CSID' ;
    // Sorting direction ASCENDING or DESCENDING
    public static string SORT_DIR = 'ASC';   
        
    
    //...................................................................................................................................//
    public  String sortExpression
               {
                 get
                 {
                    return sortExp;
                 }
                 set
                 {
                   //if the column is clicked on then switch between Ascending and Descending modes
                   if (value == sortExp)
               sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
               else
               sortDirection = 'ASC';
               sortExp = value;
                   
                   system.debug('@@@@CHECK SORT EXP '+sortExp+'  '+sortDirection);
                 }
               }
               
    public String getSortDirection(){
            
        //if not column is selected 
        if (sortExpression == null || sortExpression == '')
            return 'ASC';
        else
            return sortDirection;
    }
         
    public void setSortDirection(String value){  
        sortDirection = value;
    }  
 
    /*
    * Each column sorted in ascending/descending order
    * Setter property for wrapper list
    */
    
    public List<wrapper> getwrapperList() {
    
         // Sort using the custom compareTo() method
                if(doSort==true){
                    SORT_DIR = sortDirection;
                    SORT_FIELD =sortExpression;
                    esdList.sort();
                    }
    
                   List<wrapper> esdList1 = new List<wrapper>();
                   if(esdList.size()>=10)
                   for(integer i=0;i<10;i++)
                   {    
                    esdList1.add(esdList[i]);                        
                   }
                   else{
                       for(wrapper s:esdList)
                       {    esdList1.add(s);
                            
                       }
                        esdList=esdList1;
                   }
        return esdList;
    } 
     
    public SRS_SearchESDController(ApexPages.StandardController con) {
    
        disabled = true;
        esdList = new List<wrapper>();
        esdFlag = false; 
        renderShowandCancel= true;  
        srvcReq = (Service_Request__c)con.getRecord();
        system.debug('@@@@@: '+srvcReq);   
        this.srRecordTypeName = ApexPages.currentPage().getParameters().get(SERVICE_REQUEST_RECORD_TYPE_NAME);
    }
        
    /* 
    * Reset of selected check-boxes when 'All Statuses' is checked 
    */
    
    public void select_allchkbox()
    {
        
        Integer i = 0;
        for(i=0; i<checkbox_count; i++)
        {

            if (status[i].equals('6'))
            {  
                chkAll = TRUE;
                break;
            }
        }
        
        if (chkAll)
        {
        
            if (i == checkbox_count)
            {
            
                status.clear();
                chkAll = FALSE;
            }
            else
            {
                status.clear();  
                status.add('1');
                status.add('2');
                status.add('3');
                status.add('4');
                status.add('5');
                status.add('6');
            }
        }
        else
        {
            for (i=0; i<checkbox_count; i++)
            {
                string val = status[i];
                status.add(val);
            }
        }

    } 
    
    /* 
    * Method called by the Visualforce page's search button
    * Call the SOAP service with the ESD info
    */
    
    public void search()
    {   
        esdFlag = true;         
        string elementValue = 'Not Found';
        Integer i = 0;
    try
    {
    SRS_WS_ESD_Service.EsdProductInstanceServicePort testServ = new SRS_WS_ESD_Service.EsdProductInstanceServicePort();
    SRS_WS_ESD_ReqResponse.ServiceInstanceOutput respSearchId = new SRS_WS_ESD_ReqResponse.ServiceInstanceOutput();
    List<SRS_WS_ESD_ReqResponse.ServiceInstanceOutput> respSearchName = new List<SRS_WS_ESD_ReqResponse.ServiceInstanceOutput>();

    /*
    * Setting request parameters
    * getCustomerServiceInstanceByName 
    */
       if(srvcReq.ESD_Customer_Short_Name__c!=NULL){
            List<Integer> statusList = new List<Integer>();
            string custName ='%'+ srvcReq.ESD_Customer_Short_Name__c.replaceAll('\\*','%') +'%';
            for(i=0;i<checkbox_count;i++)
            {
            system.debug('>>> :'+status[i]);
            if(status[i] != '6') // This indicates "All statuses" checkbox value is not captured
                statusList.add(Integer.valueOf(status[i])); 
            }   
            
            // Send request and capture response results
            respSearchName = testServ.getCustomerServiceInstanceByName(custName,serviceType,statusList,serviceLocationCity);
            system.debug('>>> :'+respSearchName);
            esdList.clear();
            if(respSearchName != null){
            for(SRS_WS_ESD_ReqResponse.ServiceInstanceOutput a : respSearchName)
            {            
                esdList.add(new wrapper(String.valueOf(a.customerServiceId),a.serviceStatusName,a.customerName,a.serviceTypeDescription,a.serviceLocationName,a.municipalityName,a.provinceStateLCode,i));           
                i++;            
            }
            system.debug('>>> ESD List :'+esdList);
            }
            else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, 'No records were found, please check your search.'));
            }
       }
       
    // getCustomerServiceInstanceByCSID 
        else if(CSID!=NULL){            
            // Send request and capture response results
            respSearchId = testServ.getCustomerServiceInstanceByCSID(CSID);
            system.debug('>>> :'+respSearchId); 
            esdList.clear();            
            if(respSearchId != null){
            esdList.add(new wrapper(String.valueOf(respSearchId.customerServiceId),respSearchId.serviceStatusName,respSearchId.customerName,respSearchId.serviceTypeDescription,respSearchId.serviceLocationName,respSearchId.municipalityName,respSearchId.provinceStateLCode,i)); 
            system.debug('>>> ESD List :'+esdList);
            }
            else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, 'No records were found, please check your search.'));
            }
        } 
        
    } // try block

    catch(Exception e){            
                /*
                * Max search results is 200 records, if 200+, error message is displayed.
                */
                
                if(e.getMessage().contains('ESD_ERR_0001')){               
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, 'More than 200 records found. Please refine your search.'));
                }
                else{
                    ApexPages.addMessages(e);
                    SMB_WebserviceHelper.logWebserviceError('ESD','Search ESD Customer Details','Service Request', '' ,null, e.getMessage(),'SRS_WS_ESD_Service.EsdProductInstanceServicePort.getCustomerServiceInstanceByName',srvcReq.id);
                }               
                system.debug('>>> Exception:' +e); 
    } // catch block    

    } //end of search method
    
    // Wrapper Class Construction
          
        public class wrapper implements Comparable{
        
            public String csid{get;set;}
            public String status{get;set;}
            public String esd_cust_name{get;set;}
            public String service_type{get;set;}
            public String service_location{get;set;}
            public String city{get;set;}
            public String pr_st{get;set;}
            public Boolean checked{get; set;}
            public Integer index{get; set;}
            
            // Wrapper Constructor
            public wrapper(String csid, String status, String esd_cust_name, String service_type, String service_location, String city, String pr_st, Integer i){
                this.csid = csid;
                this.status = status;
                this.esd_cust_name = esd_cust_name;
                this.service_type = service_type;
                this.service_location = service_location;
                this.city = city;
                this.pr_st = pr_st;
                this.checked = false;
                this.index = i;
            }
            
                        
            // Implement the compareTo() method
            public Integer compareTo(Object other) {
                
                //wrapper compareToEsd = (wrapper)compareTo;
                    
                //  if (id == compareToEsd.csid) return 0;
                //  if (id > compareToEsd.csid) return 1;
                //  return -1;        
                        
            String sortfield;
            String currInstField;

            if(SORT_FIELD=='CSID')
            {   
                sortfield  =((wrapper)other).CSID  != null ?  ((wrapper)other).CSID : '';           
                currInstField  =this.CSID  != null ?  this.CSID: '';                
            }
            else if(SORT_FIELD=='Status')
            {   
                sortfield  =((wrapper)other).Status  != null ?  ((wrapper)other).Status : '';           
                currInstField  =this.Status  != null ?  this.Status: '';
            }
            
            else if(SORT_FIELD=='ESD Customer Name')
            {   
                sortfield  =((wrapper)other).esd_cust_name  != null ?  ((wrapper)other).esd_cust_name : '';           
                currInstField  =this.esd_cust_name  != null ?  this.esd_cust_name: '';
            }
            else if(SORT_FIELD=='Service Type')
            {   
                sortfield  =((wrapper)other).service_type  != null ?  ((wrapper)other).service_type : '';           
                currInstField  =this.service_type  != null ?  this.service_type: '';
            }
            else if(SORT_FIELD=='Service Location')
            {   
                sortfield  =((wrapper)other).service_location  != null ?  ((wrapper)other).service_location : '';           
                currInstField  =this.service_location  != null ?  this.service_location: '';
            }
             
            else if(SORT_FIELD=='City')
            {   
                sortfield  =((wrapper)other).City  != null ?  ((wrapper)other).City : '';           
                currInstField  =this.City  != null ?  this.City: '';
            }
            else if(SORT_FIELD=='Pr/ST')
            {   
                sortfield  =((wrapper)other).pr_st  != null ?  ((wrapper)other).pr_st : '';           
                currInstField  =this.pr_st  != null ?  this.pr_st: '';
            }
            
            String otherSRString =other != null ?  sortfield : '';  
            
             if (SORT_DIR== 'ASC')     
               return currInstField.compareTo(otherSRString);   
             else
               return otherSRString.compareTo(currInstField);  
        }          


    }   // end of wrapper class
                    
    public void SortRelatedListwithColumn(){
        doSort=true; 
        getwrapperList();       
    } 
    
    /*
    * Select CSID and associate to Service Request
    * Using Search ESD, screen - system will populate:
    * Service Identifier Type = ‘CSID’
    * Existing Service ID = ‘CSID number’
    * ESD Customer Name = ‘ESD Customer Name’
    * If Service Request Type=Install, system will store only the ESD Customer Name - not the CSID
    */
    
    public PageReference linkToSerReq(){    
        
        PageReference pr;
        Id srsProductId;
        Boolean isProductNew = false;

        System.debug('SR Id ::::: ' + SRID);
        if(SRID!= NULL){
        srvcReq = [Select Id,Name,recordtypeid,ESD_Customer_ID__c,ESD_Customer_Short_Name__c, SRS_PODS_Product__c FROM Service_Request__c WHERE Id=:SRId];         
                
                RecordType rt = [SELECT Id,Name FROM RecordType WHERE SobjectType='Service_Request__c' AND Id=:srvcReq.recordtypeid];
                Schema.DescribeSObjectResult d = Schema.SObjectType.Service_Request__c; 
                Map<Id,Schema.RecordTypeInfo> rtMapById = d.getRecordTypeInfosById();
                Schema.RecordTypeInfo rtById =  rtMapById.get(rt.id);
                Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
                Schema.RecordTypeInfo rtByName =  rtMapByName.get(rt.name);
                system.debug('>>> :'+rtByName.getName());
                String recName = rtByName.getName();
                
                for(wrapper s: esdList){
                    if(s.checked == true) {
                       if((recName == 'Provide/Install') || (recName == 'Prequal') || (recName == 'Move')){
                            System.debug('Provide/Install');
                            srvcReq.ESD_Customer_Short_Name__c  = s.esd_cust_name;
                            srvcReq.Service_Identifier_Type__c = 'New';
                       }
                       else if((recName == 'Disconnect') || (recName == 'Change') || (recName == 'Records Only')){
                            System.debug('Disconnect/Change');
                            srvcReq.ESD_Customer_Short_Name__c  = s.esd_cust_name;                          
                            srvcReq.Existing_Service_ID__c = s.csid;
                            srvcReq.Service_Identifier_Type__c = 'CSID';

                            //Thomas Tran, Traction on Demand - Added Address creation and product assignment - 07-08-2015
                           try{
                            SRS_SearchESDControllerExt.createServiceAddress(s, srvcReq.Id, srAcctId, demarcationLocation, endCustomerName, npaNXX, location, cli, existingWorkingTN, siteReady, buildingName, newBuidling, floor, cabinet, rack, shelf, postalCode);

                            srsProductId = SRS_SearchESDControllerExt.retrieveProductId(s.service_type);
                           } catch(Exception ex){
                                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));
                                return null;
                           }

                           System.debug('Product Association ::::: ' + srsProductId);
                           if(srsProductId != null){
                                srvcReq.SRS_PODS_Product__c = srsProductId;
                                isProductNew = true;
                           }

                           if(!isProductNew){
                                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Warning, 'No product found for the service type selected, please select the desired product on the service request layout as necessary.'));
                            }

                       } 

                       
                       
                        
                    //  srvcReq.ESD_Customer_ID__c = s.csid;  
                        system.debug('@@@ ESD cust name: '+srvcReq.ESD_Customer_Short_Name__c);
                        system.debug('@@@ ESD cust id: '+srvcReq.ESD_Customer_ID__c);                           
                    }
                }       
        
            try{

                update srvcReq;

                if(recName == 'Disconnect'){
                    S2C_SearchCSIDBTNCtlr.updateRelatedSRs(srvcReq);
                }
            } catch(Exception ex){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, ex.getMessage()));
                return null;
            }
            


            system.debug('@@@ SR record: '+srvcReq);
            pr = new PageReference('/'+SRId);
            pr.setRedirect(true);
            
        }   
        //return pr;
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, 'ESD customer name & CSID saved successfully.'));

        
        renderShowandCancel= false;
        return null;
        
    }
    
    public PageReference Cancel() {
        pageReference page;
        if(SRID!= NULL){
            page = new PageReference('/'+SRId);
            page.setRedirect(true);
        } 
         return page;
    }
    
    public void checked(){       
        
        /*
        * User selects a row by checking the box, and then the ‘Associate with Service Request’ button is enabled. 
        * User must select 1 & only 1
        * If user selects 0 or > 1, error message is displayed 
        */
        
        Integer i = 0;
            for(wrapper s: esdList){
                    if(s.checked == true) {
                        i++;
                        disabled = false;                           
                    }
            }

            if(!disabled){
                isRecordSelected = true;
            } else{
                isRecordSelected = false;
            }
            if(i>1){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Select one row to associate with this Service Request.'));
            }
            else if(i==0){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Select one row to associate with this Service Request.'));
            }   
            
            
        /*
        * If user has already selected a CSID, and selects another - a warning message is displayed
        */
            
            if(SRId != NULL)
            {
                srvcReq = [Select Id,Name,ESD_Customer_ID__c,ESD_Customer_Short_Name__c FROM Service_Request__c WHERE Id=:SRId];                                
                if(srvcReq.ESD_Customer_Short_Name__c!=NULL){
                                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Warning, 'Are you sure you want to replace existing CSID with new?'));
                    }
            }
    } 
    
    public PageReference reset() {
        PageReference pg = new PageReference(System.currentPageReference().getURL());
        pg.setRedirect(true);
        return pg;
    }

    /**
     * getSiteReadyValues
     * @description Queries all the picklist values.
     * @author Thomas Tran
     * @date 07-14-2015
     */
    public List<SelectOption> getSiteReadyValues(){
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = SRS_Service_Address__c.Site_Ready__c.getDescribe();
        
        options.add(new SelectOption('', NONE));

        for(Schema.PicklistEntry currentOptionTemp : fieldResult.getPicklistValues()){
            options.add(new SelectOption(currentOptionTemp.getLabel(), currentOptionTemp.getValue()));
        }

        return options;
    }

    /**
     * getNewBuildingValues
     * @description Queries all the picklist values.
     * @author Thomas Tran
     * @date 07-14-2015
     */
    public List<SelectOption> getNewBuildingValues(){
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = SRS_Service_Address__c.New_Building__c.getDescribe();
        
        options.add(new SelectOption('', NONE));

        for(Schema.PicklistEntry currentOptionTemp : fieldResult.getPicklistValues()){
            options.add(new SelectOption(currentOptionTemp.getLabel(), currentOptionTemp.getValue()));
        }

        return options;
    }

     /**
     * getLocationValues
     * @description Queries all the picklist values.
     * @author Thomas Tran
     * @date 07-14-2015
     */
    public List<SelectOption> getLocationValues(){
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = SRS_Service_Address__c.Location__c.getDescribe();
        
        options.add(new SelectOption('', NONE));

        for(Schema.PicklistEntry currentOptionTemp : fieldResult.getPicklistValues()){
            options.add(new SelectOption(currentOptionTemp.getLabel(), currentOptionTemp.getValue()));
        }

        return options;
    }

    /**
     * checkboxCheck
     * @description Converts 
     * @author Thomas Tran, Traction on Demand
     * @date 07-14-2015
     */
    public void checkboxCheck(){
        isRecordSelected = Boolean.valueOf(checkBoxStatus);
    }

    public PageReference clearValues(){
        CSID = 0;
        srvcReq.ESD_Customer_Short_Name__c = '';
        serviceType = '';
        serviceLocationCity = '';
        esdList = new List<wrapper>();

        return null;
    }
      
} // end of SRS_SearchESDController