/**
 *	Utility class for the online community
 *
 * @lastmodified
 *   Dan Reich (TOD), 10/22/2014
 *   Alex Kong (TOD), 11/18/2014
 *
 */
public without sharing class MBRUtils {
	        
    private static Map<String, List<String>> CACHE_CATEGORY_TO_REQUESTTYPES;
    private static Map<String, List<String>> CACHE_STATUS_EXTERNALTOINTERNAL;
    private static Map<String, List<String>> CACHE_REQUESTTYPE_EXTERNALTOINTERNAL;
    private static Boolean HAS_CACHED_EXTERNAL_TO_INTERNAL = false;

    private static Account_Configuration__c CACHE_ACCOUNT_CONFIGURATION;
    private static Boolean CACHE_RESTRICT_TECH_SUPPORT;

    
    /**
     *	Utility method for accessing cached category to request types map
     *
     * @lastmodified
     *   Dan Reich (TOD), 10/22/14
     *
     */
    public static Map<String, List<String>> getCategoryRequestTypes() {
        if ( CACHE_CATEGORY_TO_REQUESTTYPES == null ) {
			List<ParentToChild__c> ptcs = ParentToChild__c.getAll().values();
		    CACHE_CATEGORY_TO_REQUESTTYPES = new Map<String, List<String>>();
		    for (ParentToChild__c ptc : ptcs) {
		        if(ptc.Identifier__c.equals('CCICatRequest')) {
		            if(CACHE_CATEGORY_TO_REQUESTTYPES.get(ptc.Parent__c) == null) {
		                CACHE_CATEGORY_TO_REQUESTTYPES.put(ptc.Parent__c, new List<String>());
		            }
		            CACHE_CATEGORY_TO_REQUESTTYPES.get(ptc.Parent__c).add(ptc.Child__c);
		        }
		    }
    	}
    	return CACHE_CATEGORY_TO_REQUESTTYPES;
    }
    
    /**
     *	Utility method for accessing cached status picklist values list
     *
     * @lastmodified
     *   Dan Reich (TOD), 10/22/14
     *
     */
    public static Map<String, List<String>> getExternalToInternalStatuses() {
    	if ( CACHE_STATUS_EXTERNALTOINTERNAL == null ) {
			cacheExternalToInternalMaps();
    	}
    	return CACHE_STATUS_EXTERNALTOINTERNAL;
    }
    
    /**
     *	Utility method for accessing cached status picklist values list
     *
     * @lastmodified
     *   Dan Reich (TOD), 10/22/14
     *
     */
    public static Map<String, List<String>> getRequestTypeToRecordTypeNames() {
    	if ( CACHE_REQUESTTYPE_EXTERNALTOINTERNAL == null ) {
			cacheExternalToInternalMaps();
    	}
        return CACHE_REQUESTTYPE_EXTERNALTOINTERNAL;
    }	

    private static void cacheExternalToInternalMaps() {
    	if( !HAS_CACHED_EXTERNAL_TO_INTERNAL ) {
    		List<ExternalToInternal__c> extToInts = ExternalToInternal__c.getAll().values();
		    CACHE_STATUS_EXTERNALTOINTERNAL = new Map<String, List<String>>();
		    CACHE_REQUESTTYPE_EXTERNALTOINTERNAL = new Map<String, List<String>>();
		    for (ExternalToInternal__c  extToInt :  extToInts) {
		        if( extToInt.Identifier__c.equals('onlineStatus')) {
		            if(CACHE_STATUS_EXTERNALTOINTERNAL.get( extToInt.External__c) == null) {
		                CACHE_STATUS_EXTERNALTOINTERNAL.put( extToInt.External__c, new List<String>());
		            }
		            CACHE_STATUS_EXTERNALTOINTERNAL.get( extToInt.External__c).add( extToInt.Internal__c);
		        } else if( extToInt.Identifier__c.equals( 'onlineRequestTypeToCaseRT' ) ) {
		            if(CACHE_REQUESTTYPE_EXTERNALTOINTERNAL.get( extToInt.External__c) == null) {
		                CACHE_REQUESTTYPE_EXTERNALTOINTERNAL.put( extToInt.External__c, new List<String>());
		            }
		            CACHE_REQUESTTYPE_EXTERNALTOINTERNAL.get( extToInt.External__c).add( extToInt.Internal__c);
		        }
		    }
    	}
    }

    public static Set<String> getRecordTypeIdsByCategory( String category ) {
    	Set<String> recordTypeIds = new Set<String>();
    	List<String> requestTypes = getCategoryRequestTypes().get( category );
    	if( !requestTypes.isEmpty() ) {
    		List<String> recordTypeNames = new List<String>();
    		for( String requestType : requestTypes ) {
    			List<String> typeList = getRequestTypeToRecordTypeNames().get( requestType );
    			if( typeList != null ) {
    				recordTypeNames.addAll( typeList );
    			}
    		}
    		if( !recordTypeNames.isEmpty() ) {
    			Map<String, Schema.RecordTypeInfo> rtNameToInfo = Schema.SObjectType.Case.getRecordTypeInfosByName();
    			for( String name : recordTypeNames ) {
    				Schema.RecordTypeInfo rtInfo = rtNameToInfo.get( name );
    				if( rtInfo != null ) {
    					recordTypeIds.add( rtInfo.getRecordTypeId() );
    				}
    			}
    		}
    	}
    	return recordTypeIds;
    }

    public static String getRequestTypeFilterForCategory( String category ) {
    	return getInClause( getCategoryRequestTypes().get( category ) );
    }

    public static String getInternalStatusFilterByExternal( String external ) {
    	return getInClause( getExternalToInternalStatuses( ).get( external ) );
    }

	public static String getInClause( List<String> values ) {
		return '( \'' + String.join( values, '\', \'' ) + '\' )';
	}

	public static String getInClause( Set<String> values ) {
		return getInClause( new List<String>( values ) );
	}

    /**
     *  Utility method for accessing cached Account_Configuration__c object
     *
     * @lastmodified
     *   Alex Kong (TOD), 11/18/14
     *
     */
    public static Account_Configuration__c getAccountConfiguration() {
        if (CACHE_ACCOUNT_CONFIGURATION == null) {
            List<Account_Configuration__c> configs = new List<Account_Configuration__c>();
            Account_Configuration__c config = new Account_Configuration__c();
            Id userId = UserInfo.getUserId();
            User u = [SELECT Id, Contact.Account.Id FROM User WHERE Id = :userId];
            //System.debug('akong: helperGetAccountConfiguration: u.Contact.Account.Id: ' + u.Contact.Account.Id);
            if (u.Contact.Account.Id != null) {
                configs = [
                    SELECT Id, Restrict_Tech_Support__c 
                    FROM Account_Configuration__c 
                    WHERE Account__c = :u.Contact.Account.Id
                    ORDER BY CreatedDate ASC
                ];
            }
            //System.debug('akong: helperGetAccountConfiguration: configs: ' + configs);
            if (configs.size() > 0) {
                CACHE_ACCOUNT_CONFIGURATION = configs[0];
            }
        }
        return CACHE_ACCOUNT_CONFIGURATION;
    }

    /**
     *  Utility method for accessing cached Account_Configuration__c.Restrict_Tech_Support__c boolean
     *
     * @lastmodified
     *   Alex Kong (TOD), 11/18/14
     *
     */
    public static Boolean restrictTechSupport() {
        if (CACHE_RESTRICT_TECH_SUPPORT == null) {
            Account_Configuration__c cfg = getAccountConfiguration();
            if (cfg != null && cfg.Restrict_Tech_Support__c) {
                CACHE_RESTRICT_TECH_SUPPORT = true;
            } else {
                CACHE_RESTRICT_TECH_SUPPORT = false;
            }
        }
        return CACHE_RESTRICT_TECH_SUPPORT;
    }

    /**
     *  Utility method for sending templated Case-related emails to an arbitrary list of email addresses.
     *  This method uses the transaction rollback trick so we can merge data into a SingleEmailMessage object without
     *  being forced to send to a Contact.
     *
     * @lastmodified
     *   Alex Kong (TOD), 2015-03-04
     *   Christian Wico (TOD), 2015-03-05
     */
    public static List<Messaging.SendEmailResult> sendTemplatedCaseEmails(List<String> toAddresses, Id fromEmailAddressId, Id templateId, Id caseId) {
        List<Messaging.SendEmailResult> mailResults = new List<Messaging.SendEmailResult>();
        if (toAddresses.size() > 0) {
           
            // get a prepopulated list of email messages from Case Email Template 
            List<Messaging.SingleEmailMessage> lstMsgsToSend = getMergedEmailsForCase(toAddresses, fromEmailAddressId, templateId, caseId);            
            System.debug('Collab lstMsgsToSend: ' + lstMsgsToSend);
            
            // reserve capacity
            Integer reserveCount = lstMsgsToSend.size() * toAddresses.size();
            System.debug('Collab reserveCount: ' + reserveCount);
            Messaging.reserveSingleEmailCapacity(reserveCount);
            
            // execute mail out
            mailResults = Messaging.sendEmail(lstMsgsToSend);
            System.debug('Collab mailResults: ' + mailResults);
        }
        return mailResults;
    }

    /**
    * Retrieves a list of email merged templates for a case
    * @lastmodified
    *       Christian Wico (TOD), 2015-03-05
    *       Alex Kong (TOD), 2015-03-09
    */
    public static List<Messaging.SingleEmailMessage> getMergedEmailsForCase(List<String> toAddresses, Id fromEmailAddressId, Id templateId, Id caseId) {
        // setup the SingleEmailMessage as if sending to a contact

        MBR_Case_Collaborator_Settings__c collabSettings = MBR_Case_Collaborator_Settings__c.getOrgDefaults();
        //System.debug('Collab collabSettings: ' + collabSettings);
        Id dummyContactId = collabSettings.Dummy_Contact_id__c;
        //Contact c = [SELECT Id, Email FROM Contact WHERE Id = :dummyContactId LIMIT 1];

        List<Messaging.SingleEmailMessage> lstMsgs = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
        //msg.setTemplateId( [select id from EmailTemplate where DeveloperName='MBR_new_case_created_VF'].id );
        //msg.setTemplateId( '00X4000000163uYEAQ' );
        msg.setTemplateId( templateId );
        msg.setWhatId( caseId );
        msg.setTargetObjectId(dummyContactId);
        msg.setToAddresses( toAddresses );
        if (fromEmailAddressId != null) {
            msg.setOrgWideEmailAddressId(fromEmailAddressId);
        }
        System.debug('Collab toAddresses: ' + toAddresses);
        
        lstMsgs.add(msg);
        
        // Send the emails in a transaction, then roll it back (so nothing actually goes out)
        Savepoint sp = Database.setSavepoint();
        Messaging.sendEmail(lstMsgs);
        Database.rollback(sp);

       // For each SingleEmailMessage that was just populated by the sendEmail() method, copy its
        // contents to a new SingleEmailMessage. Then send those new messages.
        List<Messaging.SingleEmailMessage> lstMsgsToSend = new List<Messaging.SingleEmailMessage>();

        for (Messaging.SingleEmailMessage email : lstMsgs) {
            Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
            emailToSend.setToAddresses(email.getToAddresses());
            emailToSend.setPlainTextBody(email.getPlainTextBody());
            emailToSend.setHTMLBody(email.getHTMLBody());
            emailToSend.setSubject(email.getSubject());
            if (fromEmailAddressId != null) {
                emailToSend.setOrgWideEmailAddressId(fromEmailAddressId);
            }
            lstMsgsToSend.add(emailToSend);
        }
       


        return lstMsgsToSend;
    }

}