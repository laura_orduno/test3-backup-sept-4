@isTest
public class NAAS_Capture_OLI_BillingAddrCtrlTest 
{
    @testSetup
    static void testDataSetup() 
    {
        //String orderId= OrdrTestDataFactory.orderIdWithVoiceProducts();
    }
    //--getOLIWrapperToBillingAddressMap()
    //--search()
    //-runSearch()
    //-performSearch()
    //--SaveBillingAccount()
    //--SaveBillingOnSelect()
    //--UpdateBillingForMACD()
    //
        
    @isTest
    private static void getOLIWrapperToBillingAddressMapTest() 
    {
        //Order ordObj = [SELECT Id FROM Order WHERE orderMgmtId__c='1234567890' LIMIT 1];
        String orderId= OrdrTestDataFactory.orderIdWithVoiceProducts();
        //if (ordObj != null) 
        if (String.isNotBlank(orderId))
        {
            Test.startTest();
            NAAS_Capture_OLI_BillingAddrController billingAddrCtrl = new NAAS_Capture_OLI_BillingAddrController();
            billingAddrCtrl.OderId = (orderId);
            //billingAddrCtrl.OderId = (String.valueOf(ordObj.id));
            billingAddrCtrl.getOLIWrapperToBillingAddressMap();
            Test.stopTest();
            //system.assert(billingAddrCtrl.isCarRequired, true);
        }
    }
    
    @isTest
    private static void searchTest() 
    {
        String orderId= OrdrTestDataFactory.orderIdWithVoiceProducts();
        Order ordObj = [SELECT Id FROM Order WHERE orderMgmtId__c='1234567890' LIMIT 1];
        if (ordObj != null) 
        {
            Test.startTest();
            NAAS_Capture_OLI_BillingAddrController billingAddrCtrl = new NAAS_Capture_OLI_BillingAddrController();
            billingAddrCtrl.OderId = (String.valueOf(ordObj.id));
            billingAddrCtrl.searchString = '';
            billingAddrCtrl.search();
            Test.stopTest();
            //system.assert(billingAddrCtrl.isCarRequired, true);
        }
    }
    
    @isTest
    private static void SaveBillingAccountTest() 
    {
        String orderId= OrdrTestDataFactory.orderIdWithVoiceProducts();
        Order ordObj = [SELECT Id,Service_Address__r.Service_Account_Id__c FROM Order WHERE orderMgmtId__c='1234567890' LIMIT 1];
        Id BillingRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Billing').getRecordTypeId();
        Account billingacc = new Account(name='Billing-000006644',RecordTypeId = BillingRecordType);
        Insert billingacc;
        BillingAccountNamePrefix__c cutomsetting = new BillingAccountNamePrefix__c(Name = 'Billing');
        Insert cutomsetting;
        if (ordObj != null) 
        {
            Test.startTest();
            
            NAAS_Capture_OLI_BillingAddrController billingAddrCtrl = new NAAS_Capture_OLI_BillingAddrController();
            billingAddrCtrl.OderId = (String.valueOf(ordObj.id));
            billingAddrCtrl.getOLIWrapperToBillingAddressMap();
            //billingAddrCtrl.getServiceAddressSummary(ordObj.Service_Address__r.Service_Account_Id__c);
            billingAddrCtrl.SaveBillingAccount();
            Test.stopTest();
            //system.assert(billingAddrCtrl.isCarRequired, true);
        }
    }
    
    @isTest
    private static void SaveBillingOnSelectTest() 
    {
        String orderId= OrdrTestDataFactory.orderIdWithVoiceProducts();
        Order ordObj = [SELECT Id FROM Order WHERE orderMgmtId__c='1234567890' LIMIT 1];
        Account billAcc = [Select id from Account where BillingCity='TestCity' LIMIT 1];
        if (ordObj != null && billAcc != null) 
        {
            Test.startTest();
            
            NAAS_Capture_OLI_BillingAddrController billingAddrCtrl = new NAAS_Capture_OLI_BillingAddrController();
            billingAddrCtrl.OderId = (String.valueOf(ordObj.id));
            billingAddrCtrl.selectedAccId = (String.valueOf(billAcc .id));
            billingAddrCtrl.getOLIWrapperToBillingAddressMap();
            billingAddrCtrl.SaveBillingOnSelect();
            Test.stopTest();
            //system.assert(billingAddrCtrl.isCarRequired, true);
        }
    }
    
    @isTest
    private static void UpdateBillingForMACDTest() 
    {
        String orderId= OrdrTestDataFactory.orderIdWithVoiceProducts();
        Order ordObj = [SELECT Id FROM Order WHERE orderMgmtId__c='1234567890' LIMIT 1];
        if (ordObj != null) 
        {
            Test.startTest();
            NAAS_Capture_OLI_BillingAddrController billingAddrCtrl = new NAAS_Capture_OLI_BillingAddrController();
            billingAddrCtrl.OderId = (String.valueOf(ordObj.id));
            billingAddrCtrl.selectedOliBillAcc= '111-1234';
            billingAddrCtrl.getOLIWrapperToBillingAddressMap();
            billingAddrCtrl.UpdateBillingForMACD();
            billingAddrCtrl.UpdateNotes();
            billingAddrCtrl.SaveBillingOnSelect();
            Test.stopTest();
            //system.assert(billingAddrCtrl.isCarRequired, true);
        }
    }
    @isTest
    private static void Exceptionhandling() 
    {
        String orderId= OrdrTestDataFactory.orderIdWithVoiceProducts();
        Order ordObj = [SELECT Id FROM Order WHERE orderMgmtId__c='1234567890' LIMIT 1];
        if (ordObj != null) 
        {
            Test.startTest();
            NAAS_Capture_OLI_BillingAddrController billingAddrCtrl = new NAAS_Capture_OLI_BillingAddrController();
            billingAddrCtrl.OderId = (String.valueOf(ordObj.id));
            billingAddrCtrl.selectedOliBillAcc= null;
            billingAddrCtrl.getOLIWrapperToBillingAddressMap();
            billingAddrCtrl.UpdateBillingForMACD();
            billingAddrCtrl.UpdateNotes();
            billingAddrCtrl.search();
            Test.stopTest();
            //system.assert(billingAddrCtrl.isCarRequired, true);
        }
    }
}