/**
 * S2C_SR_RelatedListControllerExt
 * @description Functionality to clone move service requests
 * @author Thomas Tran, Traction on Demand
 * @date 07-27-2015
 */
global with sharing class S2C_SR_RelatedListControllerExt {    
	/**
	 * executeCloneByProduct
	 * @description Checks if an existing clone is currently executing, then stops
	 *              the user from cloning for the time being.
	 *              Otherwise format the required information passes it to proper method
	 * @author Thomas Tran, Traction on Demand
	 * @date 08-14-2015
	 */
	public static void executeCloneByProduct(Service_Request__c existingSR, Integer numberOfClones){
		String jsonSR = JSON.serialize(existingSR);
		SRS_SR_RelatedListController.logId = '';

		Webservice_Integration_Error_Log__c log = getWebServiceLog(existingSR, 'S2C_SR_RelatedListControllerExt.cloneServiceRequestByProduct');
		
		if(log != null){
			String jsonLOG = JSON.serialize(log);
			cloneServiceRequestByProduct(jsonSR, jsonLOG, numberOfClones);
		}
	}

	/**
	 * executeCloneByAddress
	 * @description Checks if an existing clone is currently executing, then stops
	 *              the user from cloning for the time being.
	 *              Otherwise format the required information passes it to proper method
	 * @author Thomas Tran, Traction on Demand
	 * @date 08-14-2015
	 */
	public static void executeCloneByAddress(Service_Request__c existingSR, Integer numberOfClones){

		String jsonSR = JSON.serialize(existingSR);
		SRS_SR_RelatedListController.logId = '';

		Webservice_Integration_Error_Log__c log = getWebServiceLog(existingSR, 'S2C_SR_RelatedListControllerExt.cloneServiceRequestByProduct');
		
		if(log != null){
			String jsonLOG = JSON.serialize(log);
			cloneSeviceRequestByAddress(jsonSR, jsonLOG, numberOfClones);
		}
	}

	/**
	 * progressCheck
	 * @description Method used by the vf page that indicates if a clones is in progress
	 *              for the service request
	 * @author Thomas Tran, Traction on Demand
	 * @date 08-14-2015
	 */
	global static String progressCheck(String logId){
		String logStatus;

		List<Webservice_Integration_Error_Log__c> logList = [SELECT Status__c 
															 FROM Webservice_Integration_Error_Log__c 
															 WHERE Id = :logId
															 ORDER BY LastModifiedDate DESC
															 LIMIT 1];

		if(!logList.isEmpty()){
			logStatus = logList[0].Status__c;
		} else{
			logStatus = 'No Log';
		}

		return logStatus;
	}

	/**
	 * popupBlocker
	 * @description Method used by the vf page that indicates if a clones is in progress
	 *              for the service request
	 * @author Thomas Tran, Traction on Demand
	 * @date 08-14-2015
	 */
	public static Boolean popupBlocker(String srId){
		Boolean blockPopup = false;

		List<Webservice_Integration_Error_Log__c> logList = [SELECT Status__c 
															 FROM Webservice_Integration_Error_Log__c 
															 WHERE SFDC_Record_Id__c = :srId
															 	AND Status__c = 'In Progress'
															 LIMIT 1];

		if(!logList.isEmpty()){
			blockPopup = true;
			SRS_SR_RelatedListController.logId = logList[0].Id;
		}

		return blockPopup;
	}

    
	/**
	 * cloneServiceRequestByProduct
	 * @description Future call that clones an existing service request and its paired service request with all the 
	 *              required associated objects.
	 * @author Thomas Tran, Traction on Demand
	 * @date 08-14-2015
	 */
	@future
	public static void cloneServiceRequestByProduct(String currentSr, String trackerObj, Integer numberOfClones){
        // service request
		List<Service_Request__c> srListToInsert = new List<Service_Request__c>();
        // related orders
		List<SRS_Service_Request_Related_Order__c> srroListToInsert = new List<SRS_Service_Request_Related_Order__c>();
        // PODS answers
		List<SRS_PODS_Answer__c> srsPodsAnswerToInsert = new List<SRS_PODS_Answer__c>();
        // contacts
		List<Service_Request_Contact__c> srcToInsert = new List<Service_Request_Contact__c>();
        // employees
		List<Service_Request_Employee__c> sreToInsert = new List<Service_Request_Employee__c>();
        // contracts
		List<Contracts__c> contractsToInsert = new List<Contracts__c>();
        
		List<ServiceRequestClonePair> cloneList = new List<ServiceRequestClonePair>();
        
        // existing service request from JSON string
		Service_Request__c existingSR = (Service_Request__c)JSON.deserialize(currentSr, Service_Request__c.class);
        
		Webservice_Integration_Error_Log__c log = (Webservice_Integration_Error_Log__c)JSON.deserialize(trackerObj, Webservice_Integration_Error_Log__c.class);
        
		List<ServiceRequestPair> matchedSRROList = findMatchingPair(existingSR, true);

		Service_Request__c cloneOfOriginalSR;
		Service_Request__c cloneOfPairSR;

		for(ServiceRequestPair currentServiceRequestPairTemp : matchedSRROList){
			for(Integer i = 0; i < numberOfClones; i++){
				Integer pairNumber = i + 1;
				DateTime currentDateTime = System.now();

				cloneOfOriginalSR = currentServiceRequestPairTemp.existingSR.clone(false, true, false, false);
				cloneOfOriginalSR.Description__c = currentServiceRequestPairTemp.existingSR.Account_Name__r.Name + ' - COMPLEX ORDER - ' + currentDateTime + ' - MOVE ' + pairNumber; 
				srListToInsert.add(cloneOfOriginalSR);
				cloneOfPairSR = currentServiceRequestPairTemp.matchedSR.clone(false, true, false, false);
				cloneOfPairSR.Description__c = currentServiceRequestPairTemp.matchedSR.Account_Name__r.Name + ' - COMPLEX ORDER - ' + currentDateTime + ' - MOVE ' + pairNumber;
				srListToInsert.add(cloneOfPairSR);
				cloneList.add(new ServiceRequestClonePair(cloneOfOriginalSR, cloneOfPairSR, currentServiceRequestPairTemp.existingSR, currentServiceRequestPairTemp.matchedSR,
									currentServiceRequestPairTemp.existingSRRO, currentServiceRequestPairTemp.pairedSRRO));
			}
		}

		if(!srListToInsert.isEmpty()){
			SavePoint currentSP = Database.setSavePoint();
			try{
				insert srListToInsert;

				srroListToInsert = cloneRelatedOrdersOrRefereces(cloneList);

				if(!srroListToInsert.isEmpty()){
					insert srroListToInsert;
				}
				srsPodsAnswerToInsert = cloneProductTemplate(cloneList);

				if(!srsPodsAnswerToInsert.isEmpty()){
					insert srsPodsAnswerToInsert;
				}

				srcToInsert = cloneServiceRequestContact(cloneList);

				if(!srcToInsert.isEmpty()){
					insert srcToInsert;
				}

				sreToInsert = cloneTeamMembers(cloneList);

				if(!sreToInsert.isEmpty()){
					insert sreToInsert;
				}

				contractsToInsert = cloneRelatedContracts(cloneList);

				if(!contractsToInsert.isEmpty()){
					insert contractsToInsert;
				}

				log.Status__c = 'Passed';
				update log;

			}catch(Exception ex){
				Database.rollback(currentSP);

				log.Error_Code_and_Message__c = ex.getMessage();
				log.Stack_Trace__c = ex.getStackTraceString();
				log.Status__c = 'Failed';
				update log;

			}
		}
	}

	/**
	 * cloneSeviceRequestByAddress
	 * @description Future call that clones an existing service request and its paired service request with all the 
	 *              required associated objects.
	 * @author Thomas Tran, Traction on Demand
	 * @date 08-14-2015
	 */
	@future
	public static void cloneSeviceRequestByAddress(String currentSr, String trackerObj, Integer numberOfClones){        
        // service request
		List<Service_Request__c> srListToInsert = new List<Service_Request__c>();
        // related orders        
		List<SRS_Service_Request_Related_Order__c> srroListToInsert = new List<SRS_Service_Request_Related_Order__c>();
        // service addresses 
		List<SRS_Service_Address__c> srsAddressToInsert = new List<SRS_Service_Address__c>();
        // contacts
		List<Service_Request_Contact__c> srcToInsert = new List<Service_Request_Contact__c>();
        // employees
		List<Service_Request_Employee__c> sreToInsert = new List<Service_Request_Employee__c>();
        // contracts
		List<Contracts__c> contractsToInsert = new List<Contracts__c>();        
        
		List<ServiceRequestClonePair> cloneList = new List<ServiceRequestClonePair>();
        // existing service request from JSON string
		Service_Request__c existingSR = (Service_Request__c)JSON.deserialize(currentSr, Service_Request__c.class);

        Webservice_Integration_Error_Log__c log = (Webservice_Integration_Error_Log__c)JSON.deserialize(trackerObj, Webservice_Integration_Error_Log__c.class);
        
		List<ServiceRequestPair> matchedSRROList = findMatchingPair(existingSR, false);

		Service_Request__c cloneOfOriginalSR;
		Service_Request__c cloneOfPairSR;

		for(ServiceRequestPair currentServiceRequestPairTemp : matchedSRROList){
			for(Integer i = 0; i < numberOfClones; i++){
				Integer pairNumber = i + 1;
				DateTime currentDateTime = System.now();

				cloneOfOriginalSR = currentServiceRequestPairTemp.existingSR.clone(false, true, false, false);
				cloneOfOriginalSR.SRS_PODS_Product__c = null;
				cloneOfOriginalSR.Description__c = currentServiceRequestPairTemp.existingSR.Account_Name__r.Name + ' - COMPLEX ORDER - ' + currentDateTime + ' - MOVE ' + pairNumber; 
				srListToInsert.add(cloneOfOriginalSR);
				cloneOfPairSR = currentServiceRequestPairTemp.matchedSR.clone(false, true, false, false);
				cloneOfPairSR.SRS_PODS_Product__c = null;
				cloneOfPairSR.Description__c = currentServiceRequestPairTemp.matchedSR.Account_Name__r.Name + ' - COMPLEX ORDER - ' + currentDateTime + ' - MOVE ' + pairNumber; 
				srListToInsert.add(cloneOfPairSR);
				cloneList.add(new ServiceRequestClonePair(cloneOfOriginalSR, cloneOfPairSR, currentServiceRequestPairTemp.existingSR, currentServiceRequestPairTemp.matchedSR,
									currentServiceRequestPairTemp.existingSRRO, currentServiceRequestPairTemp.pairedSRRO));
			}
		}        
        
		if(!srListToInsert.isEmpty()){
			SavePoint currentSP = Database.setSavePoint();
			try{
				insert srListToInsert;

				srroListToInsert = cloneRelatedOrdersOrRefereces(cloneList);

				if(!srroListToInsert.isEmpty()){
					insert srroListToInsert;
				}
				srsAddressToInsert = cloneServiceAddress(cloneList);

				if(!srsAddressToInsert.isEmpty()){
					insert srsAddressToInsert;
				}

				srcToInsert = cloneServiceRequestContact(cloneList);

				if(!srcToInsert.isEmpty()){
					insert srcToInsert;
				}

				sreToInsert = cloneTeamMembers(cloneList);

				if(!sreToInsert.isEmpty()){
					insert sreToInsert;
				}

				contractsToInsert = cloneRelatedContracts(cloneList);

				if(!contractsToInsert.isEmpty()){
					insert contractsToInsert;
				}

				log.Status__c = 'Passed';
				update log;

			}catch(Exception ex){
				Database.rollback(currentSP);

				log.Error_Code_and_Message__c = ex.getMessage();
				log.Stack_Trace__c = ex.getStackTraceString();
				log.Status__c = 'Failed';
				update log;

			}
		}
	}

	/**
	 * findMatchingPair
	 * @description Finds the matching Service Request to the selected Service Request.
	 * @author Thomas Tran, Traction on Demand
	 * @date 08-06-2015
	 */
	public static List<ServiceRequestPair> findMatchingPair(Service_Request__c existingSR, Boolean isProduct){
		Map<String, SRS_Service_Request_Related_Order__c> possibleMatch = new Map<String, SRS_Service_Request_Related_Order__c>();
		List<ServiceRequestPair> pairingList = new List<ServiceRequestPair>();

		//Query all  SRRO associated to the the existingSR and Map them
		for(SRS_Service_Request_Related_Order__c currentSRROTemp : [SELECT Name, Reference_Number__c, Reference_Type__c, Comments__c, RO_Service_Request__c, RO_Service_Request__r.Name, RO_Service_Request__r.SRS_PODS_Product__r.Name, System__c, RecordType.Name
																    FROM SRS_Service_Request_Related_Order__c 
																    WHERE RO_Service_Request__c =:existingSR.Id 
																    		AND RecordType.Name = 'Reference Number']){
			possibleMatch.put(currentSRROTemp.Name, currentSRROTemp);
		}

		if(!possibleMatch.isEmpty()){
			Map<Id, Service_Request__c> idToSrMap = new Map<Id, Service_Request__c>([SELECT Id, Name, Service_Request_Type__c, Target_Date_Type__c, Target_Date__c, SRS_PODS_Product__r.Name, Legacy_Order_count__c, 
																							Requested_Date__c, Opportunity__c, RecordTypeId, Account_Name__c, PrimaryContact__c, Billing_Account__c, Billing_Number__c, 
																							Billing_Remarks__c, Billing_Account_New__c, Billing_Account_Type__c, Same_As_Account_Name__c, Billing_Name__c, 
																							Same_As_Service_Address__c, Billing_Address__c, Service_Identifier_Type__c, ESD_Customer_Short_Name__c, Existing_Service_ID__c,
																							ESD_Customer_ID__c, Product_Template_Status__c, TELUS_Caused_Recovery_Reason__c, Requested_By_TELUS__c, Approved_By_Customer__c, 
																							Approved_By_TELUS__c, Recovery_Remarks__c, Details_Of_Service__c, Workflow_Request_Type__c, Description__c, Account_Name__r.Name,
																							City__c, Prov__c, Opportunity_Product_Item__c, Contract_Length__c,Contract__c 
																					 FROM Service_Request__c
																					 WHERE Name IN :possibleMatch.keySet() AND Opportunity__c = :existingSR.Opportunity__c]);
		
			//Query all SRRO that has a matching ID of SR based on the name of the SR and SR that is in the opportunity.
			for(SRS_Service_Request_Related_Order__c currentSRROTemp : [SELECT Name, Reference_Number__c, Reference_Type__c, Comments__c, RO_Service_Request__c, RO_Service_Request__r.Name, RO_Service_Request__r.SRS_PODS_Product__r.Name, System__c, RecordType.Name
																	    FROM SRS_Service_Request_Related_Order__c 
																	    WHERE RO_Service_Request__c IN :idToSrMap.keySet()
																				AND Name = :existingSR.Name]){
				SRS_Service_Request_Related_Order__c originalSRRO = possibleMatch.get(String.valueOf(currentSRROTemp.RO_Service_Request__r.Name));

				if(originalSRRO != null){
					//Check if the return SRRO SR Id is the name on the current SRRO and the current SRRO SR Id is the name of the returned SRRO

					if(String.valueOf(originalSRRO.RO_Service_Request__r.Name).equals(currentSRROTemp.Name) && (originalSRRO.Name).equals(String.valueOf(currentSRROTemp.RO_Service_Request__r.Name))){
						Service_Request__c tempSR = idToSrMap.get(currentSRROTemp.RO_Service_Request__c);
						pairingList.add(new ServiceRequestPair(existingSR, tempSR, originalSRRO, currentSRROTemp));
					}
				}
			}
		}

		return pairingList;
	}

	/**
	 * cloneServiceAddress
	 * @description Clones the address of the original service address on to the new 
	 *              service requests.
	 * @author Thomas Tran, Traction on Demand
	 * @date 08-05-2015
	 */
	private static List<SRS_Service_Address__c> cloneServiceAddress(List<ServiceRequestClonePair> serviceRequestClonePairList){
		Map<Id, List<SRS_Service_Address__c>> srIdToAddressMap = new Map<Id, List<SRS_Service_Address__c>>();
		Set<Id> originalSRIds = new Set<Id>();
		List<SRS_Service_Address__c> addressToInsert = new List<SRS_Service_Address__c>();
		List<SRS_Service_Address__c> currentAddresses = new List<SRS_Service_Address__c>();

		for(ServiceRequestClonePair currentSRCPTemp : serviceRequestClonePairList){
        	originalSRIds.add(currentSRCPTemp.originalSR.Id);
        	originalSRIds.add(currentSRCPTemp.pairSR.Id);
        }

        if(!originalSRIds.isEmpty()){
        	for(SRS_Service_Address__c currentAddressTemp : [SELECT Id, Address__c, Building_Name__c, Cabinet__c, CLLI__c, Demarcation_Location__c, Existing_Working_TN__c, Floor__c, Location__c, 
        												New_Building__c, NPA_NXX__c, Rack__c, Shelf__c, Site_Ready__c, Service_Request__c
        										  FROM SRS_Service_Address__c
        										  WHERE Service_Request__c IN :originalSRIds]){
        		if(!srIdToAddressMap.containsKey(currentAddressTemp.Service_Request__c)){
        			srIdToAddressMap.put(currentAddressTemp.Service_Request__c, new List<SRS_Service_Address__c>());
        		}

        		currentAddresses = srIdToAddressMap.get(currentAddressTemp.Service_Request__c);
        		currentAddresses.add(currentAddressTemp);
        	}

        	if(!srIdToAddressMap.isEmpty()){
        		List<SRS_Service_Address__c> currentAddressesForSR;
        		List<SRS_Service_Address__c> currentAddressesForSRPair;
        		SRS_Service_Address__c clonedAddressForSR;
        		SRS_Service_Address__c clonedAddressForSRPair;

        		for(ServiceRequestClonePair currentPair : serviceRequestClonePairList){
        			currentAddressesForSR = srIdToAddressMap.get(currentPair.originalSR.Id);

        			if(currentAddressesForSR != null && !currentAddressesForSR.isEmpty()){
        				for(SRS_Service_Address__c currentAddressTemp : currentAddressesForSR){
        					clonedAddressForSR = currentAddressTemp.clone(false, true, false, false);
        					clonedAddressForSR.Service_Request__c = currentPair.clonedExistingSR.Id;
        					addressToInsert.add(clonedAddressForSR);
        				}
        			}

        			currentAddressesForSRPair = srIdToAddressMap.get(currentPair.pairSR.Id);

        			if(currentAddressesForSRPair != null && !currentAddressesForSRPair.isEmpty()){
        				for(SRS_Service_Address__c currentAddressTemp : currentAddressesForSRPair){
        					clonedAddressForSRPair = currentAddressTemp.clone(false, true, false, false);
        					clonedAddressForSRPair.Service_Request__c = currentPair.clonedPairedSR.Id;
        					addressToInsert.add(clonedAddressForSRPair);
        				}
        			}
	        	}
        	}
        }

        return addressToInsert;
	}
	

	/**
	 * cloneProductTemplate
	 * @description
	 * @author Thomas Tran, Traction on Demand
	 * @date 07-28-2015
	 */
	private static List<SRS_PODS_Answer__c> cloneProductTemplate(List<ServiceRequestClonePair> serviceRequestClonePairList){
		Map<Id, List<SRS_PODS_Answer__c>> srIDToRPTMap = new Map<Id, List<SRS_PODS_Answer__c>>();
		Map<Id, List<SRS_PODS_Answer__c>> srIDToQAMap = new Map<Id, List<SRS_PODS_Answer__c>>();

		List<SRS_PODS_Answer__c> productTemplateListToInsert = new List<SRS_PODS_Answer__c>(); 
        Map<ID, Schema.RecordTypeInfo> recordTypeMap = Schema.SObjectType.Service_request__c.getRecordTypeInfosById();
        Set<Id> originalSRIds = new Set<Id>();
        Set<String> originalSRPodsProductName = new Set<String>();
        String originalOrderType;
        String pairOrderType;
        String productName;
        Schema.RecordTypeInfo originalRecordType = recordTypeMap.get(serviceRequestClonePairList[0].originalSR.RecordTypeId);
        Schema.RecordTypeInfo pairRecordType = recordTypeMap.get(serviceRequestClonePairList[0].pairSR.RecordTypeId);
        Set<String> orderTypes = new Set<String>();

        originalOrderType = ((originalRecordType.getName()).equals('prequal') || (originalRecordType.getName()).equals('Provide/Install')) ? 'install': originalRecordType.getName();
        pairOrderType = ((pairRecordType.getName()).equals('prequal') || (pairRecordType.getName()).equals('Provide/Install')) ? 'install': pairRecordType.getName();

        orderTypes.add(originalOrderType);
        orderTypes.add(pairOrderType);

        for(ServiceRequestClonePair currentSRCPTemp : serviceRequestClonePairList){
        	originalSRIds.add(currentSRCPTemp.originalSR.Id);
        	originalSRIds.add(currentSRCPTemp.pairSR.Id);
        	originalSRPodsProductName.add(currentSRCPTemp.originalSR.SRS_PODS_Product__r.Name);
        	originalSRPodsProductName.add(currentSRCPTemp.pairSR.SRS_PODS_Product__r.Name);
        }

        //COnvert loop into maps
        //Use the pairings ids to create clones
        //Add to a map
        if(!originalSRIds.isEmpty() && !originalSRPodsProductName.isEmpty()){
        	List<SRS_PODS_Answer__c> tempRPList;
        	SYstem.debug('originalSRIds :::: ' + originalSRIds);
        	SYstem.debug('orderType :::: ' + orderTypes);
	        for(SRS_PODS_Answer__c currentRPTTemp : [SELECT Service_Request__c, GroupSerial__c, SRS_Question_ID__c, SRS_Group_Question__c, FOX_PARAMETER__c ,
														    SRS_PODS_Products__r.Id, SRS_PODS_Products__r.SRS_PODS_Product_Master__r.Name, SRS_PODS_Products__r.Order_Type__c, 
														    SRS_PODS_Products__r.Name, SRS_PODS_Answer__c
												     FROM SRS_PODS_Answer__c
												     WHERE Service_Request__c IN :originalSRIds
												     		AND SRS_PODS_Products__r.SRS_PODS_Product_Master__r.Name IN :originalSRPodsProductName 
												     		AND SRS_PODS_Products__r.Order_Type__c IN :orderTypes]){
	        	if(!srIDToRPTMap.containsKey(currentRPTTemp.Service_Request__c)){
	        		srIDToRPTMap.put(currentRPTTemp.Service_Request__c, new List<SRS_PODS_Answer__c>());
	        	}
	        	tempRPList = srIDToRPTMap.get(currentRPTTemp.Service_Request__c);
	    		tempRPList.add(currentRPTTemp);
	    	}

	    	SYstem.debug('srIDToRPTMap :::: ' + srIDToRPTMap);

	    	List<SRS_PODS_Answer__c> tempQAList;
	    	for(SRS_PODS_Answer__c currentQATemp : [SELECT Service_Request__c, GroupSerial__c, SRS_Question_ID__c, SRS_Group_Question__c, FOX_PARAMETER__c, SRS_PODS_Products__r.Id, 
			        									   SRS_PODS_Products__r.SRS_PODS_Product_Master__r.Name, SRS_PODS_Products__r.Order_Type__c, SRS_PODS_Products__r.Name, SRS_PODS_Answer__c
			        							    FROM SRS_PODS_Answer__c
			        							    WHERE Service_Request__c IN :originalSRIds
			        							    	  AND GroupSerial__c != null
			        							    ORDER BY GroupSerial__c]){
	    		if(!srIDToQAMap.containsKey(currentQATemp.Service_Request__c)){
	        		srIDToQAMap.put(currentQATemp.Service_Request__c, new List<SRS_PODS_Answer__c>());
	        	}
	        	tempQAList = srIDToQAMap.get(currentQATemp.Service_Request__c);
	    		tempQAList.add(currentQATemp);
	    	}

	    	SYstem.debug('srIDToQAMap :::: ' + srIDToQAMap);

	    	List<SRS_PODS_Answer__c> originalRPTRecords;
	    	List<SRS_PODS_Answer__c> originalRPTPairRecords;
	    	List<SRS_PODS_Answer__c> originalQARecords;
	    	List<SRS_PODS_Answer__c> originalQAPairRecords;
	    	SRS_PODS_Answer__c clonedRPT;
	    	SRS_PODS_Answer__c clonedRPTPair;
	    	SRS_PODS_Answer__c clonedQA;
	    	SRS_PODS_Answer__c clonedQAPair;

	    	for(ServiceRequestClonePair currentPair : serviceRequestClonePairList){

	    		originalRPTRecords = srIDToRPTMap.get(currentPair.originalSR.Id);
	    		originalRPTPairRecords = srIDToRPTMap.get(currentPair.pairSR.Id);
	    		originalQARecords = srIDToQAMap.get(currentPair.originalSR.Id);
				originalQAPairRecords = srIDToQAMap.get(currentPair.pairSR.Id);

	    		if(originalRPTRecords != null){
	    			System.debug('originalRPTRecords :::: ' + originalRPTRecords);
	    			for(SRS_PODS_Answer__c currentRPTTemp : originalRPTRecords){
	    				clonedRPT = currentRPTTemp.clone(false, true, false, false);
		        		clonedRPT.Service_Request__c = currentPair.clonedExistingSR.Id;
		        		productTemplateListToInsert.add(clonedRPT);
	    			}
	    		}

	    		if(originalRPTPairRecords != null){
	    			System.debug('originalRPTPairRecords :::: ' + originalRPTPairRecords);
	    			for(SRS_PODS_Answer__c currentRPTTemp : originalRPTPairRecords){
	    				clonedRPTPair = currentRPTTemp.clone(false, true, false, false);
	        			clonedRPTPair.Service_Request__c = currentPair.clonedPairedSR.Id;
	        			productTemplateListToInsert.add(clonedRPTPair);
	    			}
	    			
	    		}

	    		if(originalQARecords != null){
	        		for(SRS_PODS_Answer__c currentQATTemp : originalQARecords){
	    				clonedQA = currentQATTemp.clone(false, true, false, false);
	        			clonedQA.Service_Request__c = currentPair.clonedExistingSR.Id;
	        			productTemplateListToInsert.add(clonedQA);
	    			}
	    		}

	    		if(originalQAPairRecords != null){
	        		for(SRS_PODS_Answer__c currentQATTemp : originalQAPairRecords){
		    			clonedQAPair = currentQATTemp.clone(false, true, false, false);
		        		clonedQAPair.Service_Request__c = currentPair.clonedPairedSR.Id;
		        		productTemplateListToInsert.add(clonedQAPair);
	    			}
	    		}
	        }
        }
        return productTemplateListToInsert;
	}
    
    
	/**
	 * cloneServiceRequestContact
	 * @description
	 * @author Thomas Tran, Traction on Demand
	 * @date 07-28-2015
	 */
	private static List<Service_Request_Contact__c> cloneServiceRequestContact(List<ServiceRequestClonePair> serviceRequestClonePairList){
		List<Service_Request_Contact__c> srcListToInsert = new List<Service_Request_Contact__c>(); 
		Map<Id, List<Service_Request_Contact__c>> srIdToSRCMap = new Map<Id, List<Service_Request_Contact__c>>();
		List<Service_Request_Contact__c> currentSRCList = new List<Service_Request_Contact__c>();
		Set<Id> originalSRIds = new Set<Id>();


		for(ServiceRequestClonePair currentSRCPTemp : serviceRequestClonePairList){
        	originalSRIds.add(currentSRCPTemp.originalSR.Id);
        	originalSRIds.add(currentSRCPTemp.pairSR.Id);
        }

		for(Service_Request_Contact__c currentSRCTemp : [SELECT Id, Account_Name__c, Contact__c, Contact_Role__c, Contact_Type__c, Email__c, Mobile__c, Onsite_customer_Contain__c, Phone__c, 
															    PrimaryContact__c,Service_Request__c,SRAccount__c,Title__c 
													     FROM Service_Request_Contact__c 
													     WHERE Service_Request__c IN :originalSRIds]){
			if(!srIdToSRCMap.containsKey(currentSRCTemp.Service_Request__c)){
				srIdToSRCMap.put(currentSRCTemp.Service_Request__c, new List<Service_Request_Contact__c>());
			}

			currentSRCList = srIdToSRCMap.get(currentSRCTemp.Service_Request__c);
			currentSRCList.add(currentSRCTemp);
    	}

    	if(!srIdToSRCMap.isEmpty()){
    		List<Service_Request_Contact__c> originalSRC;
    		List<Service_Request_Contact__c> originalSRCPair;
    		Service_Request_Contact__c clonedSRC;
    		Service_Request_Contact__c clonedSRCPair;

	    	for(ServiceRequestClonePair currentPair : serviceRequestClonePairList){
	    		originalSRC = srIdToSRCMap.get(currentPair.originalSR.Id);

	    		if(originalSRC != null && !originalSRC.isEmpty()){
	    			for(Service_Request_Contact__c currentSRCTemp : originalSRC){
						clonedSRC = currentSRCTemp.clone(false, true, false, false);
						clonedSRC.Service_Request__c = currentPair.clonedExistingSR.Id;
						srcListToInsert.add(clonedSRC);
	    			}
	    		}

				originalSRCPair = srIdToSRCMap.get(currentPair.pairSR.Id);

				if(originalSRCPair != null && !originalSRCPair.isEmpty()){
					for(Service_Request_Contact__c currentSRCTemp : originalSRCPair){
						clonedSRCPair = currentSRCTemp.clone(false, true, false, false);
						clonedSRCPair.Service_Request__c = currentPair.clonedPairedSR.Id;
						srcListToInsert.add(clonedSRCPair);
					}
				}
	        }
	    }

	    return srcListToInsert;
	}

	/**
	 * cloneTeamMembers
	 * @description
	 * @author Thomas Tran, Traction on Demand
	 * @date 07-28-2015
	 */
	private static List<Service_Request_Employee__c> cloneTeamMembers(List<ServiceRequestClonePair> serviceRequestClonePairList){
		List<Service_Request_Employee__c> sreListToInsert = new List<Service_Request_Employee__c>();
		Map<Id, List<Service_Request_Employee__c>> srIdToSREMap = new Map<Id, List<Service_Request_Employee__c>>();
		List<Service_Request_Employee__c> currentSREList = new List<Service_Request_Employee__c>();
		Set<Id> originalSRIds = new Set<Id>();

		for(ServiceRequestClonePair currentSRCPTemp : serviceRequestClonePairList){
        	originalSRIds.add(currentSRCPTemp.originalSR.Id);
        	originalSRIds.add(currentSRCPTemp.pairSR.Id);
        }

		for(Service_Request_Employee__c currentSRETemp :[SELECT Id, Contact_Owner_Alias__c, Contact_Type__c, Email__c, Fox_Queue__c, Mobile__c, Phone__c, Role__c, Service_Request__c, Team_Member__c,
																Title__c 
														 FROM Service_Request_Employee__c
														 WHERE Service_Request__c IN :originalSRIds]){
			if(!srIdToSREMap.containsKey(currentSRETemp.Service_Request__c)){
				srIdToSREMap.put(currentSRETemp.Service_Request__c, new List<Service_Request_Employee__c>());
			}

			currentSREList = srIdToSREMap.get(currentSRETemp.Service_Request__c);
			currentSREList.add(currentSRETemp);
		}

		if(!srIdToSREMap.isEmpty()){
			List<Service_Request_Employee__c> originalSRE;
    		List<Service_Request_Employee__c> originalSREPair;
    		Service_Request_Employee__c clonedSRE;
    		Service_Request_Employee__c clonedSREPair;

	    	for(ServiceRequestClonePair currentPair : serviceRequestClonePairList){
	    		originalSRE = srIdToSREMap.get(currentPair.originalSR.Id);

	    		if(originalSRE != null && !originalSRE.isEmpty()){
	    			for(Service_Request_Employee__c currentSRETemp : originalSRE){
	    				clonedSRE = currentSRETemp.clone(false, true, false, false);
	    				clonedSRE.Service_Request__c = currentPair.clonedExistingSR.Id;
	    				sreListToInsert.add(clonedSRE);
	    			}
	    		}

				originalSREPair = srIdToSREMap.get(currentPair.pairSR.Id);

				if(originalSREPair != null && !originalSREPair.isEmpty()){
	    			for(Service_Request_Employee__c currentSRETemp : originalSREPair){
	    				clonedSREPair = currentSRETemp.clone(false, true, false, false);
	    				clonedSREPair.Service_Request__c = currentPair.clonedPairedSR.Id;
	    				sreListToInsert.add(clonedSREPair);
	    			}
	    		}
	        }
		}

		return sreListToInsert;
	}

	/**
	 * cloneRelatedContracts
	 * @description
	 * @author Thomas Tran, Traction on Demand
	 * @date 07-28-2015
	 */
	private static List<Contracts__c> cloneRelatedContracts(List<ServiceRequestClonePair> serviceRequestClonePairList){
		List<Contracts__c> contractListToInsert = new List<Contracts__c>();
		Map<Id, List<Contracts__c>> srIdToContractsMap = new Map<Id, List<Contracts__c>>();
		List<Contracts__c> currentContractList = new List<Contracts__c>();
		Set<Id> originalSRIds = new Set<Id>();

		for(ServiceRequestClonePair currentSRCPTemp : serviceRequestClonePairList){
        	originalSRIds.add(currentSRCPTemp.originalSR.Id);
        	originalSRIds.add(currentSRCPTemp.pairSR.Id);
        }

		for(Contracts__c currentContractTemp : [SELECT Id, Contract_Number__c, Contract_Remarks__c, Contract_Term_Months__c, Contract_Type__c, Contract__c, New_Record_Type__c, Opportunity__c
													FROM Contracts__c
													WHERE Contract__c IN :originalSRIds]){
			//Creates a list in the map if the key does nto exist
			if(!srIdToContractsMap.containsKey(currentContractTemp.Contract__c)){
				srIdToContractsMap.put(currentContractTemp.Contract__c, new List<Contracts__c>());
			}

			currentContractList = srIdToContractsMap.get(currentContractTemp.Contract__c);
			currentContractList.add(currentContractTemp);

		}

		if(!srIdToContractsMap.isEmpty()){
			List<Contracts__c> originalContracts;
    		List<Contracts__c> originalContractsPair;
    		Contracts__c clonedContracts;
    		Contracts__c clonedContractsPair;

	    	for(ServiceRequestClonePair currentPair : serviceRequestClonePairList){
	    		originalContracts = srIdToContractsMap.get(currentPair.originalSR.Id);

	    		if(originalContracts != null && !originalContracts.isEmpty()){
	    			for(Contracts__c currentContracts : originalContracts){
	    				clonedContracts = currentContracts.clone(false, true, false, false);
	    				clonedContracts.Contract__c = currentPair.clonedExistingSR.Id;
	    				contractListToInsert.add(clonedContracts);
	    			}
	    		}

				originalContractsPair = srIdToContractsMap.get(currentPair.pairSR.Id);

				if(originalContractsPair != null && originalContractsPair != null){
					for(Contracts__c currentContracts : originalContractsPair){
	    				clonedContractsPair = currentContracts.clone(false, true, false, false);
	    				clonedContractsPair.Contract__c = currentPair.clonedPairedSR.Id;
	    				contractListToInsert.add(clonedContractsPair);
	    			}
				}
	        }
		}

		return contractListToInsert;
	}

	/**
	 * cloneRelatedOrdersOrRefereces
	 * @description Clones the Related Orders of the service request
	 * @author Thomas Tran, Traction on Demand
	 * @date 07-29-2015
	 */
	private static List<SRS_Service_Request_Related_Order__c> cloneRelatedOrdersOrRefereces(List<ServiceRequestClonePair> serviceRequestClonePairList){
		List<SRS_Service_Request_Related_Order__c> srroListToInsert = new List<SRS_Service_Request_Related_Order__c>();
		
		Set<Id> srIds = new Set<Id>();

		for(ServiceRequestClonePair currentPairTemp : serviceRequestClonePairList){
			srIds.add(currentPairTemp.clonedExistingSR.Id);
			srIds.add(currentPairTemp.clonedPairedSR.Id);
		}

		Map<Id, Service_Request__c> srIdToSR = new Map<Id, Service_Request__c>([SELECT Name FROM Service_Request__c WHERE Id IN :srIds]);

		for(ServiceRequestClonePair currentClonePair : serviceRequestClonePairList){
			currentClonePair.clonedExistingSRRO = currentClonePair.existingSRRO.clone(false, true, false, false);
			currentClonePair.clonedExistingSRRO.Name = srIdToSR.get(currentClonePair.clonedPairedSR.Id).Name;
			currentClonePair.clonedExistingSRRO.RO_Service_Request__c = currentClonePair.clonedExistingSR.Id;

			srroListToInsert.add(currentClonePair.clonedExistingSRRO);

			currentClonePair.clonedPairedSRRO = currentClonePair.pairedSRRO.clone(false, true, false, false);
			currentClonePair.clonedPairedSRRO.Name = srIdToSR.get(currentClonePair.clonedExistingSR.Id).Name;
			currentClonePair.clonedPairedSRRO.RO_Service_Request__c = currentClonePair.clonedPairedSR.Id;

			srroListToInsert.add(currentClonePair.clonedPairedSRRO);
		}

		return srroListToInsert;
	}
    
    

	/**
	 * srPairCheck
	 * @description Determines if the Service Request does have at least one pairing.
	 * @author Thomas Tran, Traction on Demand
	 * @date 08-05-2015
	 */
	public static Boolean srPairCheck(String serviceRequestId){
		Boolean hasPair = false;
		Map<String, SRS_Service_Request_Related_Order__c> possibleMatch = new Map<String, SRS_Service_Request_Related_Order__c>();
		Service_Request__c existingSR = [SELECT Id, Opportunity__c, Name FROM Service_Request__c WHERE Id = : serviceRequestId];
		if(existingSR != null){
			for(SRS_Service_Request_Related_Order__c currentSRROTemp : [SELECT Name, RO_Service_Request__c, RO_Service_Request__r.Name
																	    FROM SRS_Service_Request_Related_Order__c 
																	    WHERE RO_Service_Request__c =: existingSR.Id
																	    		AND RecordTypeId IN (SELECT Id 
																	    							  FROM RecordType 
																	    							  WHERE SObjectType = 'SRS_Service_Request_Related_Order__c' 
																	    							  		AND Name IN ('Reference Number'))]){
				possibleMatch.put(currentSRROTemp.Name, currentSRROTemp);
			}


			if(!possibleMatch.isEmpty()){

				Map<Id, Service_Request__c> idToSrMap = new Map<Id, Service_Request__c>([SELECT Id
																						 FROM Service_Request__c
																						 WHERE Name IN :possibleMatch.keySet() AND Opportunity__c = :existingSR.Opportunity__c]);

				for(SRS_Service_Request_Related_Order__c currentSRROTemp : [SELECT Id, Name, RO_Service_Request__c, RO_Service_Request__r.Name
																		    FROM SRS_Service_Request_Related_Order__c 
																		    WHERE RO_Service_Request__c IN :idToSrMap.keySet()
																					AND Name = :existingSR.Name]){
					SRS_Service_Request_Related_Order__c originalSRRO = possibleMatch.get(String.valueOf(currentSRROTemp.RO_Service_Request__r.Name));

					if(originalSRRO != null){
						if(String.valueOf(originalSRRO.RO_Service_Request__r.Name).equals(currentSRROTemp.Name) && (originalSRRO.Name).equals(String.valueOf(currentSRROTemp.RO_Service_Request__r.Name))){
							hasPair = true;
							break;
						}
					}
				}
			}
		}

		return hasPair;

	}


	public static Webservice_Integration_Error_Log__c getWebServiceLog(Service_Request__c currentSR, String classMethodText){
		Webservice_Integration_Error_Log__c log;

		try{
			log = new Webservice_Integration_Error_Log__c(
	    		Apex_Class_and_Method__c = classMethodText,
	    		SFDC_Record_Id__c = currentSR.Id,
	    		Object_Name__c = 'Service_Request__c'
	    	);

	    	insert log;
	    	SRS_SR_RelatedListController.logId = log.Id;

			List<Webservice_Integration_Error_Log__c> webServiceIntegeration = [SELECT Id, Apex_Class_and_Method__c, Error_Code_and_Message__c, SFDC_Record_Id__c, Object_Name__c
																			    FROM Webservice_Integration_Error_Log__c
																			    WHERE SFDC_Record_Id__c = :currentSR.Id
																			    	  AND Object_Name__c = 'Service_Request__c'
																      				  AND Status__c = 'In Progress'
																      			ORDER BY CreatedDate ASC];

			if(webServiceIntegeration != null && !webServiceIntegeration.isEmpty()){
				Webservice_Integration_Error_Log__c firstLog = webServiceIntegeration[0];

				if(!((firstLog.Id).equals(log.Id))){ //Checks the first item to ensure its the latest one
					delete log;
					SRS_SR_RelatedListController.logId = '';
					log = null;
				}
			}
		} catch(Exception ex){
			SRS_SR_RelatedListController.logId = '';
    		log = null;
		}

		return log;
	}

	/**
	 * ServiceRequestPair
	 * @description Class that is used to Pair original records for cloning.
	 * @author Thomas Tran, Traction on Demand
	 * @date 07-27-2015
	 */
	public class ServiceRequestPair{
		public Service_Request__c existingSR {get; set;}
		public Service_Request__c matchedSR {get; set;}

		public SRS_Service_Request_Related_Order__c existingSRRO {get; set;}
		public SRS_Service_Request_Related_Order__c pairedSRRO {get; set;}

		public ServiceRequestPair(Service_Request__c existingSR, Service_Request__c matchedSR, SRS_Service_Request_Related_Order__c existingSRRO, SRS_Service_Request_Related_Order__c pairedSRRO){
			this.existingSR = existingSR;
			this.matchedSR = matchedSR;
			this.existingSRRO = existingSRRO;
			this.pairedSRRO = pairedSRRO;
		}
	}

	/**
	 * ServiceRequestClonePair
	 * @description Class that is used to keep track of the cloned pairs
	 * @author Thomas Tran, Traction on Demand
	 * @date 07-27-2015
	 */
	public class ServiceRequestClonePair{
		public Service_Request__c originalSR {get; set;}
		public Service_Request__c pairSR {get; set;}

		public Service_Request__c clonedExistingSR {get; set;}
		public Service_Request__c clonedPairedSR {get; set;}

		public SRS_Service_Request_Related_Order__c existingSRRO {get; set;}
		public SRS_Service_Request_Related_Order__c pairedSRRO {get; set;}

		public SRS_Service_Request_Related_Order__c clonedExistingSRRO {get; set;}
		public SRS_Service_Request_Related_Order__c clonedPairedSRRO {get; set;}

		public ServiceRequestClonePair(Service_Request__c clonedExistingSR, Service_Request__c clonedPairedSR, Service_Request__c originalSR, Service_Request__c pairSR,
											SRS_Service_Request_Related_Order__c existingSRRO, SRS_Service_Request_Related_Order__c pairedSRRO){
			this.clonedExistingSR = clonedExistingSR;
			this.clonedPairedSR = clonedPairedSR;
			this.originalSR = originalSR;
			this.pairSR = pairSR;
			this.existingSRRO = existingSRRO;
			this.pairedSRRO = pairedSRRO;
		}
	}
}