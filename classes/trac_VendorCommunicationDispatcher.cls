public with sharing class trac_VendorCommunicationDispatcher extends Dispatcher {

	public List<Vendor_Communication__c> newList {get; set;}
    public List<Vendor_Communication__c> oldList {get; set;}
    public Map<Id, Vendor_Communication__c> newMap {get; set;}
    public Map<Id, Vendor_Communication__c> oldMap {get; set;}
    public static boolean firstRun = true;
    private final String currentUserName = UserInfo.getName();

	public trac_VendorCommunicationDispatcher() {
		super();
	}
	public override void init() {
        newList = (trigger.new != null) ? (List<Vendor_Communication__c>)trigger.new : new List<Vendor_Communication__c>();
        oldList = (trigger.old != null) ? (List<Vendor_Communication__c>)trigger.old : new List<Vendor_Communication__c>();
        newMap = (trigger.newMap != null) ? (Map<Id, Vendor_Communication__c>)trigger.newMap : new Map<Id, Vendor_Communication__c>();
        oldMap = (trigger.oldMap != null) ? (Map<Id, Vendor_Communication__c>)trigger.oldMap : new Map<Id, Vendor_Communication__c>();
    }

    public override void execute(){
    	//functions to be used. 
    	if(LimitExecutionEvent(TriggerEvent.AFTERINSERT)){
            if(LimitExecutionCount('manageVendorComm',1)){
    		  manageVendorComm();
            }            
    	}	
        if(LimitExecutionEvent(TriggerEvent.AFTERUPDATE)){
            if(LimitExecutionCount('updateCaseComment',1)){
                updateCaseComment(newList,oldMap);            
            }
        }
    }
    //Overloaded function to allow the other sendVendorObject to be called from other classes. 
    public void manageVendorComm(){
        manageVendorComm(newList);
    }


    public void manageVendorComm(List<Vendor_Communication__c> vendorComm){
    	
    	List<Vendor_Communication__c> vcToSend = new List<Vendor_Communication__c>();
        List<Vendor_Communication__c> vendorCreateCaseComment = new List<Vendor_Communication__c>();
        
        //catch whether the Vendor communication object has been created.  
    	for(Vendor_Communication__c v: vendorComm){
    		if(v.ConnectionReceivedId == null && v.Case_Comment_ID__c != null ){
                //System.Debug('The case comment id of the vendor object: ' + v.Case_Comment_ID__c);
    			vcToSend.add(v);
    		}
            //else if (v.ConnectionReceivedId != null && v.Case_Comment_ID__c != null){
            //    vendorUpdateCaseComment.add(v);
            //}
            else{
                vendorCreateCaseComment.add(v);
            }
    	}
        //System.Debug('The size of vendor communication to send: ' + vcToSend.size());
    	if( vcToSend.size() > 0){  
    		trac_S2S_connection_Helper.sendToRingCentral(vcToSend, null, null, 'RingCentral',false,false,false);
    	}

        if(vendorCreateCaseComment.size() > 0 || Test.isRunningTest()){
             createCaseComment(vendorCreateCaseComment);
        }
    }

    public void createCaseComment(List<Vendor_Communication__c> toCaseComment){
        
        List<CaseComment> insertCC = new List<CaseComment>();
        //Set<Case> caseIds = new Set<Case>();
        List<Vendor_Communication__c> updateWithCCId = new List<Vendor_Communication__c>();
        Map<Id,CaseComment> vendorCommToCaseComment = new Map<Id,CaseComment>();
        
        for(Vendor_Communication__c v: toCaseComment){
            //string.isblank();
            if( v.ConnectionReceivedId != null || Test.isRunningTest() ){
                CaseComment tmpCC;
                if(!String.isBlank(v.Case_ID__c)){
                    tmpCC = new CaseComment(
                        ParentId = v.Case_ID__c,
                        CommentBody = v.Vendor_Comments__c,
                        Ispublished = false                                             
                    );
                }
                else{
                    tmpCC = new CaseComment(
                        ParentId = v.Case__c,
                        CommentBody = v.Vendor_Comments__c,
                        Ispublished = false                                             
                    );
                }
                vendorCommToCaseComment.put(v.id,tmpCC);
                insertCC.add(tmpCC);
            }
        }

        if(insertCC.size() > 0 ){
            //============================================================================================================================================================
            //The value of 'processCountMap' is being set to 1 using the function'countExecuted'. This affects the 'execute()' and 'createVendorComm' methods in the class
            // 'trac_CaseCommentDispatcher'. The reason is to perform an insert on CaseComment without firing the logic from an AFTERINSERT event.
            //After the insert is completed, the  'processCountMap' is reset to zero.
            //============================================================================================================================================================
            Dispatcher.CountExecuted('createVendorComm');      
            insert insertCC;
            Dispatcher.ResetProcessCount('createVendorComm');
        }

        if(toCaseComment.size() > 0){
            updateWithCCId = [
                SELECT Id, Case_Comment_ID__c
                FROM Vendor_Communication__c
                WHERE Id in :toCaseComment
            ];
        }
        for(Vendor_Communication__c v:updateWithCCId){
            if(vendorCommToCaseComment.containsKey(v.id)){  
                CaseComment tmpCC = vendorCommToCaseComment.get(v.Id);                
                v.Case_Comment_ID__c = tmpCC.Id; 
                v.Case_ID__c = tmpCC.ParentId;              
            }
        }
        Dispatcher.CountExecuted('updateCaseComment');
        update updateWithCCId;
        Dispatcher.ResetProcessCount('updateCaseComment');
    }

    /**    
    *updateCaseComment
    *@description when a Vendor Communication object is updated from S2S, update the correct Case Comment.
    *@authoer David Barrera
    *@Date 9/3/14
    */
    public void updateCaseComment(List<Vendor_Communication__c> toCaseComment, Map<Id, Vendor_Communication__c> oldVends){

        Set<Id> caseCommentIDs = new Set<Id>();
        List<CaseComment> updatedCaseComments = new List<CaseComment>();
        Map<String,Vendor_Communication__c> ccIdToVendorComm = new Map<String,Vendor_Communication__c>();

        for(Vendor_Communication__c v:toCaseComment){
            Vendor_Communication__c oldV = oldVends.get(v.Id);
            System.Debug('This is the current User: ' + currentUserName);
            if(v.OwnerId == oldV.OwnerId && currentUserName == 'Connection User' ){
                //Check to see if the owner of the case is the Connection User. Get the Connection User from a Custom setting.
                if( String.isNotBlank(v.Case_Comment_ID__c) ){
                    caseCommentIDs.add(v.Case_Comment_ID__c);
                    ccIdToVendorComm.put(v.Case_Comment_ID__c, v);
                }
            }
        }
        System.Debug('This is what is inside of Case commend ids: ' + caseCommentIDs);
        if(caseCommentIDs.size() > 0){
            updatedCaseComments = [
                SELECT Id,CommentBody,ParentId,ConnectionReceivedId
                FROM CaseComment
                WHERE Id in:caseCommentIDs
            ];
        }
        System.Debug('this is the size of update casecomments ' + updatedCaseComments.size());

        for(CaseComment cc:updatedCaseComments){
            if(ccIdToVendorComm.containsKey(cc.Id)){
                Vendor_Communication__c tmpV = ccIdToVendorComm.get(cc.ID);
                cc.CommentBody = tmpV.Vendor_Comments__c;
            }

        }

        if(updatedCaseComments.size() > 0){
            Dispatcher.CountExecuted('updateVenComm'); 
            System.Debug('Updating updateCaseComments: ' + updatedCaseComments);
            update updatedCaseComments;
            Dispatcher.ResetProcessCount('updateVenComm');
        }        
    }
}