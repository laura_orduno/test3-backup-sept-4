@isTest(SeeAllData=true)
private class RoleHierarchyTest {
	static testmethod void testBatchUpdate(){
        //create test data
       /* UserRole role = [select id from UserRole limit 1];//where name like '%Sales%'
        Profile p = [select id from Profile limit 1];
        //User u = new User(firstname='test1', lastname = 'test1', userroleid = role.id, profileid = p.id);
        User u = new User(alias = 'standt', email='standarduser@testorg.com', 
                            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                             localesidkey='en_US', profileid = p.Id, userroleid = role.id,
                            timezonesidkey='America/Los_Angeles', username='standarduser23@testorg.com');   
        insert u;*/
        User u = [select id from User where Do_Not_Include_Name_in_Mgr_1_4__c = false and isactive = true and UserType <> 'PowerPartner' and userrole.name <> null and (not userrole.name like '%#%') limit 1];
        System.debug('User = ' + u.id);
        Test.startTest();
        
         RoleHierarchy  hierarchy = new RoleHierarchy() ;
         hierarchy.query = 'select id,name, userRoleId, userrole.parentroleid, userrole.name, Manager_1__c, Manager_2__c, Manager_3__c, Manager_4__c from user where Do_Not_Update_Manager_1_4__c = false and id = \'' + u.id + '\'';
         Database.executeBatch(hierarchy);
        Test.stopTest();
         
        //User u1 = [select Manager_1__c, Manager_2__c, Manager_3__c, Manager_4__c from user where id = :u.id];
        
    }
  
    
    static testmethod void testBatchUpdateForStarUser(){
        //create test data
       /* UserRole role = [select id from UserRole limit 1];//where name like '%Sales%'
        Profile p = [select id from Profile limit 1];
        //User u = new User(firstname='test1', lastname = 'test1', userroleid = role.id, profileid = p.id);
        User u = new User(alias = 'standt', email='standarduser@testorg.com', 
                            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                             localesidkey='en_US', profileid = p.Id, userroleid = role.id,
                            timezonesidkey='America/Los_Angeles', username='standarduser23@testorg.com');   
        insert u;*/
        List<User> users = [select id from User where Do_Not_Include_Name_in_Mgr_1_4__c = false and isactive = true and UserType <> 'PowerPartner' and Id Not In ('005400000013JMrAAM', '005T0000000rJUCIA2') and userrole.name like '%*%' limit 1];
        if (users.isEmpty()) return;
        User u = users.get(0);
        Test.startTest();
        
         RoleHierarchy  hierarchy = new RoleHierarchy() ;
         hierarchy.query = 'select id,name, userRoleId, userrole.parentroleid, userrole.name, Manager_1__c, Manager_2__c, Manager_3__c, Manager_4__c from user where Do_Not_Update_Manager_1_4__c = false and id = \'' + u.id + '\'';
         Database.executeBatch(hierarchy);
        Test.stopTest();
         
        //User u1 = [select Manager_1__c, Manager_2__c, Manager_3__c, Manager_4__c from user where id = :u.id];
        
    }  
  
    
    static testmethod void testBatchUpdateForHashUser(){
        //create test data
       /* UserRole role = [select id from UserRole limit 1];//where name like '%Sales%'
        Profile p = [select id from Profile limit 1];
        //User u = new User(firstname='test1', lastname = 'test1', userroleid = role.id, profileid = p.id);
        User u = new User(alias = 'standt', email='standarduser@testorg.com', 
                            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                             localesidkey='en_US', profileid = p.Id, userroleid = role.id,
                            timezonesidkey='America/Los_Angeles', username='standarduser23@testorg.com');   
        insert u;*/
        List<User> users = [select id from User where Do_Not_Include_Name_in_Mgr_1_4__c = false and isactive = true and UserType <> 'PowerPartner' and Id Not In ('005400000013JMrAAM', '005T0000000rJUCIA2') and userrole.name like '%#%' limit 1];
        if (users.isEmpty()) return;
        User u = users.get(0);
        Test.startTest();
        
         RoleHierarchy  hierarchy = new RoleHierarchy() ;
         hierarchy.query = 'select id,name, userRoleId, userrole.parentroleid, userrole.name, Manager_1__c, Manager_2__c, Manager_3__c, Manager_4__c from user where Do_Not_Update_Manager_1_4__c = false and id = \'' + u.id + '\'';
         Database.executeBatch(hierarchy);
        Test.stopTest();
         
        //User u1 = [select Manager_1__c, Manager_2__c, Manager_3__c, Manager_4__c from user where id = :u.id];
        
    }
}