global with sharing class vlocity_addon_HwFloatSummaryTable_CP implements vlocity_cmt.VlocityOpenInterface {

  public Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {
      list<string> voicePlanFields = new list<string>{ 'Allowed_Hardware_Units__c','Eligibility_Period__c','Deal_Support__r.Device_Term__c'};
      list<string> filters = new list<string>{'Contract__c=:contractId','Type__c'+'=\''+'Hardware Float'+'\''};
      Boolean success = true;
      Map<id, List<Map<String, String>>> cliRowData = new Map<id, List<Map<String, String>>>(); 
      Map<string, List<Map<String, String>>> eachTableData = new Map<string, List<Map<String, String>>>(); 
      map<string,object> outputMap = new map<string,object>();
      string content = '';
      if(methodName == 'buildDocumentSectionContent')
      { 
          Id contractId = (Id) inputMap.get('contextObjId');
          inputMap.put('queryfields', voicePlanFields);
          inputMap.put('filters',filters);
          inputMap.put('hwtype','Hardware Float');
          map<string,object> mapTableInfo = vlocity_addon_DynamicTableHelper_CP.getHwAllotmentOrFloatSumry(inputMap);
          system.debug('mapTableInfo'+ mapTableInfo);          
          if(mapTableInfo.size() > 0 && !mapTableInfo.isEmpty()){
              cliRowData = (Map<id, List<Map<String, String>>>)	mapTableInfo.get('headerRowDataMap');
              for(id cliId: cliRowData.keyset() ){
                  list<map<string,string>> cliTableRows = cliRowData.get(cliID);
                  eachTableData.put('NA',cliTableRows);
                  mapTableInfo.put('tableRowsMap', eachTableData);
                  inputMap.putall(mapTableInfo);
                  outputMap =  vlocity_addon_DynamicTableHelper_CP.buildDocumentSectionContent(inputMap,outMap, options);
                  content += outputMap.get('sectionContent');
                  system.debug('content::: '+ content);
              }
              
              outMap.put('sectionContent',content);
              system.debug('::All Table Content::  ' + content);
          }
          else {
              return false;
          }
          
          
      }       
      return success; 
  }

}