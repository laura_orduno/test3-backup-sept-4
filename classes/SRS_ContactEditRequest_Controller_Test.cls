/* Developed By: Hari Neelalaka, IBM
*  Created Date: 18-Dec-2014
*/
@isTest

private Class SRS_ContactEditRequest_Controller_Test{
    
    static testMethod void testContactEditRequest(){  
        try{    
            // creating an account and using assertion for validation
            Account acc = new Account();
            acc.Name = 'test';
            insert acc;
            system.assertEquals(acc.Name, 'test');
            // creating a service request referencing an account created above
            Service_Request__c sr = new Service_Request__c();
            sr.Account_Name__c = acc.Id;
            insert sr;
            // Setting the url parameter values for SRS_ContactEditRequest VF page
            PageReference pageRef = Page.SRS_ContactEditRequest;    
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('AcctId', acc.id);
            ApexPages.currentPage().getParameters().put('serviceRequestId', sr.id);
            // Instantiating the controller of the Apex class "SRS_ContactEditRequest_Controller"
            SRS_ContactEditRequest_Controller objCtrl=new SRS_ContactEditRequest_Controller();
            // Using a standard controller, passing standard object "Account"
            ApexPages.StandardController sc = new ApexPages.StandardController(acc);
            // Instantiating the controller of the Apex class "SRS_ContactEditRequest_Controller" passing the above standard controller as parameter
            SRS_ContactEditRequest_Controller objCtrl1=new SRS_ContactEditRequest_Controller(sc);
        }catch(Exception ex)
        {
        }
    }    
}