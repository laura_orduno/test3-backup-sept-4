public class NAAS_Async_AssignWFM_ReqRes_v2 {
    public class CreateWorkOrderResponseFuture extends System.WebServiceCalloutFuture {
        public SMB_AssignmentWFM_OrderTypes_v2.WorkOrder getValue() {
            SMB_AssignmentWFM_ServiceReqRes_v2.CreateWorkOrderResponse response = (SMB_AssignmentWFM_ServiceReqRes_v2.CreateWorkOrderResponse)System.WebServiceCallout.endInvoke(this);
            return response.workOrder;
        }
    }
    public class CancelWorkOrderResponseFuture extends System.WebServiceCalloutFuture {
        public String getValue() {
            SMB_AssignmentWFM_ServiceReqRes_v2.CancelWorkOrderResponse response = (SMB_AssignmentWFM_ServiceReqRes_v2.CancelWorkOrderResponse)System.WebServiceCallout.endInvoke(this);
            return response.workOrderId;
        }
    }
    public class UpdateWorkOrderResponseFuture extends System.WebServiceCalloutFuture {
        public SMB_AssignmentWFM_OrderTypes_v2.WorkOrder getValue() {
            SMB_AssignmentWFM_ServiceReqRes_v2.UpdateWorkOrderResponse response = (SMB_AssignmentWFM_ServiceReqRes_v2.UpdateWorkOrderResponse)System.WebServiceCallout.endInvoke(this);
            return response.workOrder;
        }
    }
    public class GetWorkOrderResponseFuture extends System.WebServiceCalloutFuture {
        public SMB_AssignmentWFM_OrderTypes_v2.WorkOrder getValue() {
            SMB_AssignmentWFM_ServiceReqRes_v2.GetWorkOrderResponse response = (SMB_AssignmentWFM_ServiceReqRes_v2.GetWorkOrderResponse)System.WebServiceCallout.endInvoke(this);
            return response.workOrder;
        }
    }
    public class SearchWorkOrderResponseFuture extends System.WebServiceCalloutFuture {
        public SMB_AssignmentWFM_OrderTypes_v2.WorkOrder[] getValue() {
            SMB_AssignmentWFM_ServiceReqRes_v2.SearchWorkOrderResponse response = (SMB_AssignmentWFM_ServiceReqRes_v2.SearchWorkOrderResponse)System.WebServiceCallout.endInvoke(this);
            return response.workOrderList;
        }
    }
    public class SearchWorkOrderListResponseFuture extends System.WebServiceCalloutFuture {
        public SMB_AssignmentWFM_OrderTypes_v2.WorkOrderMap[] getValue() {
            SMB_AssignmentWFM_ServiceReqRes_v2.SearchWorkOrderListResponse response = (SMB_AssignmentWFM_ServiceReqRes_v2.SearchWorkOrderListResponse)System.WebServiceCallout.endInvoke(this);
            return response.searchWorkOrderResponseList;
        }
    }
}