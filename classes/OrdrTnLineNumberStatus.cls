/**
 * @author Santosh Rath
 * Used for Telephone Number Reservation
 */
public Enum OrdrTnLineNumberStatus {
    AVAILABLE, RESERVED_UNASSIGNED, RESERVED_ASSIGNED ,UNRESERVED
}