/*
    To start this batch:
    Id batchInstanceId = Database.executeBatch(new trac_UpdateAllAccounts(), 1000);
*/
global class trac_UpdateAllAccounts implements Database.Batchable<sObject>, Database.AllowsCallouts {
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'Select Id From Account';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext bc, List<sObject> scope) {
        update scope;
    }
    global void finish(Database.BatchableContext bc) {
        // cleanup, email alert, stats, etc
    }
    
}