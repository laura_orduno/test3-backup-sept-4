/**
 * An apex page controller that exposes the site login functionality
 */
global with sharing class MBRLoginCtlr {
    global transient String usernameError {get;set;}
    global transient String loginError {get;set;}
    global String username{get;set;}
    global String password{get;set;}
    public transient String[] errors {get; private set;}
    
    public Boolean showForgotPassword{get; set;}
        
    global MBRLoginCtlr () {
        showForgotPassword = true;
        usernameError = '';
        loginError = '';
    }
    
    global PageReference login() {
        String EMAIL_PATTERN = '^[_A-Za-z0-9-&\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$';
        usernameError = '';
        loginError = '';    
        if(!Pattern.matches(EMAIL_PATTERN, username)) {
        //if(!trac_PageUtils.isValidEmail(username)) {
            usernameError = label.mbrInvalidEmail;
            system.debug('usernameError: '+usernameError);
            return null;
        }
       
        showForgotPassword = true;
  		User loginUser = null;
        
        try{
  			loginUser = [SELECT id, Customer_Portal_Name__c FROM USER WHERE username = :username];
        }
        catch(QueryException e){
  			loginUser = null;
            System.debug('ERROR: Login user not found (' + username + ')');
        }

        errors = new String[]{};
            
        if(loginUser != null){
            if(loginUser.Customer_Portal_Name__c != null && loginUser.Customer_Portal_Name__c.containsIgnoreCase('vitilcare')){
                usernameError = Label.VITILcareWrongPortal;
                return null;
            }
        }
        else{
            errors.add('ERROR: Login user not found (' + username + ')');            
        }
           
        String startUrl = System.currentPageReference().getParameters().get('startURL');

        PageReference result =  Site.login(username, password, startUrl!=null? startUrl:'/overview');
        
        if (!ApexPages.getMessages().isEmpty()) {
	        if(loginUser != null){
                // check if user account is locked out
                LoginHistory userLoginHistory = [select Id, UserId, LoginTime, Status from LoginHistory where UserId = :loginUser.id order by LoginTime desc limit 1];            
                
                if(userLoginHistory != null && userLoginHistory.status == 'Password Lockout'){
                    showForgotPassword = false;
                    
                    errors.add('Your user account has been locked. Please try logging in again in 30 minutes.');                
                }
                else{
                    for (ApexPages.Message msg : ApexPages.getMessages()) {
                        errors.add(msg.getDetail());
                    }            
                }            
            }    
        }
        if(errors.size() > 0){
            loginError = errors[0];
            system.debug('loginError: '+loginError);
            return null;
        }
        else return result;
    }
}