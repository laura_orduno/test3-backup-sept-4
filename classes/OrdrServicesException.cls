/**
* @author Santosh Rath
* 
*
*/
public virtual class OrdrServicesException extends Exception {
    
    
    public String sourceSystem;
    public String nativeErrorCode;
    public String exceptionType;
    
    public String[] errorMessages;
    public String errorCode = OrdrErrorCodes.DEFAULT_UNEXPECTED_ERROR; //This cannot be blank, and it needs to be initialized either by the proper error code or be left to the Default value
    
    public OrdrServicesException(String errorCode,String message) {
        setMessage(message);
        if (errorCode != null && errorCode.trim().length() > 0){
            this.errorCode = errorCode;
        }
    }
    
    public OrdrServicesException(String message, String sourceSystem, String nativeErrorCode, String exceptionType, String errorCode) {
        
        this.sourceSystem = sourceSystem;
        this.nativeErrorCode = nativeErrorCode;
        this.exceptionType = exceptionType;
        if (errorCode != null && errorCode.trim().length() > 0){
            this.errorCode = errorCode;
        }
    }
       
    public String getSourceSystem() {
        return sourceSystem;
    }
    
    public void setSourceSystem(String sourceSystem) {
        this.sourceSystem = sourceSystem;
    }
    
    public String getNativeErrorCode() {
        return nativeErrorCode;
    }
    
    public void setNativeErrorCode(String nativeErrorCode) {
        this.nativeErrorCode = nativeErrorCode;
    }
    
    public String getExceptionType() {
        return exceptionType;
    }
    
    public void setExceptionType(String exceptionType) {
        this.exceptionType = exceptionType;
    }   

    public String[] getErrorMessages() {
        return errorMessages;
    }
    
    public void setErrorMessages(String[] errorMessages) {
        this.errorMessages = errorMessages;
    }
    
    public virtual String getErrorCode() {
        String errorMessageFromFault=getMessage();
        if(String.isNotBlank(errorMessageFromFault)){
            if (errorMessageFromFault.indexOf('NO SPARES FOR NXX') !=-1) {
                return OrdrErrorCodes.TNR_NO_TN_AVAILABLE_FOR_SELECTED_NPANXX;
            }
            else if (errorMessageFromFault.indexOf('TELEPHONE LINE NUMBER REQUESTED IS NOT SPARE') !=-1) {
                return OrdrErrorCodes.TNR_RESERVATION_FAILED;
            }
            else if (errorMessageFromFault.indexOf('HELD-FOR-DISPLAY') !=-1) {
                return OrdrErrorCodes.TNR_RESERVATION_FAILED;
            }
            else if (errorMessageFromFault.indexOf('NXX DOES NOT EXIST FOR SPECIFIED COID') !=-1) {
                return OrdrErrorCodes.TNR_INVALID_NXX_FOR_SELECTED_NPANXX;
            }
            else if (errorMessageFromFault.indexOf('Real TN was not reserved') !=-1) {
                // ASF Error message returned when making an attempt to reserve a Dark DSL TN through TOCP ReserveResource/ASF GetServiceID()
                return OrdrErrorCodes.TNR_RESERVATION_FAILED;
            } else if (errorMessageFromFault.indexOf('Read timed out') !=-1) {
                // ASF Error message returned when making an attempt to reserve a Dark DSL TN through TOCP ReserveResource/ASF GetServiceID()
                return OrdrErrorCodes.TNR_WEBSERVICE_CALL_TIMEOUT_ERROR;
            }            
            else {
                if(String.isBlank(this.errorCode)){
                    return OrdrErrorCodes.TNR_UNEXPECTED_ERROR;
                } else {
                    return this.errorCode;
                }               
            }
            
        }
        return null;
    }
    
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
    
    public String toString() {
        //String s = 'errorCode: '+errorCode+' '+super.toString();      
        String s = 'errorCode: '+errorCode+' ';     
        return s;
    }  
}