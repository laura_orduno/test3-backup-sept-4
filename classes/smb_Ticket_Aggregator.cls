/*
###########################################################################
# File..................: smb_Ticket_Agreegate
# Version...............: 36
# Created by............: Sandip Chaudhari
# Created Date..........: 08 March 2016

# Description...........: This class contains methods to call troble ticket details from SRM, TTODS, LavaStorm
#
###########################################################################
# Last Modified Details......: 
*/
public class smb_Ticket_Aggregator{
    
    public TicketVO.ServiceRequestAndResponseParameter reqResObj;
    
    // Method to get trouble tickets from each source system
    public Continuation getTroubleTickets(TicketVO.ServiceRequestAndResponseParameter reqResParam){
        reqResObj = reqResParam;
        Continuation cont = new Continuation(TicketVO.TIMEOUT_INT_SECS);
        cont.continuationMethod = reqResObj.callBackMethodName; // specify callback method name, This method should be defined in main controller of VF page
        beginTTODSAsyncCall(cont); // get TTODS details
        beginSRMAsyncCall(cont); // get SRM details - 19 May 2016 - Sandip - Removed commentted line for deploying SRM changes
        return cont;
    }
    
    /*
        Get details for specific trouble ticket
        This method will prepare the list of tickets from sources and return the same. 
        The invoker method should iterate the list to get specific ticket details. 
        This is implemented using list because TTODS does not support method for specific ticket details 
        and SRM method not imported properly from wsdl as salesforce does not support extension in xsd 
    */
    public Continuation getTTDetailBySource(TicketVO.ServiceRequestAndResponseParameter reqResParam){
        reqResObj = reqResParam;
        Continuation cont = new Continuation(TicketVO.TIMEOUT_INT_SECS);
        cont.continuationMethod = reqResObj.callBackMethodName;
        if(reqResParam.source == TicketVO.SRC_TTODS){
            system.debug('@@@In TTODS');
            beginTTODSAsyncCall(cont); // Get tickets for TTODS
        }else if (reqResParam.source == TicketVO.SRC_SRM){
            system.debug('@@@In SRM');
            beginSRMAsyncCall(cont); // 19 May 2016 - Sandip - Removed commentted line for deploying SRM changes
        }
        return cont;
    }
    
    // Get trouble tickets from TTODS system
    public Continuation beginTTODSAsyncCall(Continuation cont){
        
        // for test - comment below line before move to production or SIT
        //reqResObj.TIDValue = 'T1234567';
        //reqResObj.RCIDValue = '0003206700';
        // end test
        string requestSystemName = 'SFDC';
        string requestUserId = reqResObj.TIDValue;
        Asyncsmb_TTODS_queryticketservice.AsyncQueryTicketBinding_v1_5 servicePort = new Asyncsmb_TTODS_queryticketservice.AsyncQueryTicketBinding_v1_5();
        servicePort = prepareCallout(servicePort);
        smb_TTODS_troubleticket.CustomerIdFilter ttCustomerIdFilter = new smb_TTODS_troubleticket.CustomerIdFilter();
        ttCustomerIdFilter.value = reqResObj.RCIDValue;
        ttCustomerIdFilter.operator = '=';
        ttCustomerIdFilter.customerIdType = 'CSHM';
        smb_TTODS_troubleticket.TicketFilter ttTicketFilter = new smb_TTODS_troubleticket.TicketFilter();
        ttTicketFilter.toInclude = true;
        integer upToXLatestModified = reqResObj.upToXLatestModified;
        smb_TTODS_troubleticket.ActivityFilter ttActivityFilter = new smb_TTODS_troubleticket.ActivityFilter();
        ttActivityFilter.toInclude = true;
        ttActivityFilter.upToXLatestModified = reqResObj.upToXLatestModified;
        try{
            
            reqResObj.ttListFuture = servicePort.beginGetTickets(cont,requestSystemName,requestUserId,null,null,null,null,
                                                                 ttCustomerIdFilter, ttTicketFilter, ttTicketFilter, upToXLatestModified, ttActivityFilter);
            System.debug('#### ttListFuture' + reqResObj.ttListFuture);
        }catch (Exception ex){
            System.debug('#### Excep' + ex);
            logError('smb_Ticket_Aggregate.beginTTODSAsyncCall', '' , ex.getmessage(), ex.getstacktracestring());
        }
        return cont;
    }
    
    // Get trouble ticket from SRM
    public Continuation beginSRMAsyncCall(Continuation cont){
        //for test - comment below line before move to production or SIT
        //reqResObj.RCIDValue = '0001058404';
        //reqResObj.RCIDValue = '0000990733';
        // end test
        AsyncSRM1_2_TQTroubleTicketSvc_1.AsyncTQTroubleTicketSvcPort objCaller = new AsyncSRM1_2_TQTroubleTicketSvc_1.AsyncTQTroubleTicketSvcPort();
        SRM1_2_TQBaseCriteriaTypes_v1.RangeCriteria ranegCrit = new SRM1_2_TQBaseCriteriaTypes_v1.RangeCriteria();
        
        String creds;
        SMBCare_WebServices__c wsUsername = SMBCare_WebServices__c.getValues('username');
        SMBCare_WebServices__c wsPassword = SMBCare_WebServices__c.getValues('password');
        if (String.isNotBlank(WSusername.Value__c)&& String.isNotBlank(wsPassword.Value__c))
            creds = WSusername.Value__c+':'+wsPassword.Value__c;
        String encodedusernameandpassword = EncodingUtil.base64Encode(Blob.valueOf(creds)); 
        objCaller.inputHttpHeaders_x = new Map<String, String>();
        objCaller.inputHttpHeaders_x.put('Authorization','Basic ' + encodedusernameandpassword );
        ranegCrit.maxCount = reqResObj.upToXLatestModified;
        try{
            reqResObj.srmTTListFuture = objCaller.beginSearchTroubleTicketsByRegionalCustomerId(cont,reqResObj.RCIDValue,ranegCrit);
        }catch (Exception ex){
            logError('smb_Ticket_Aggregate.beginSRMAsyncCall', '' , ex.getmessage(), ex.getstacktracestring());
            System.debug('#### Excep' + ex);
        }   
        return cont;
        
    }
    
    /*Method to set Credentials in header of the TTODS request*/
    private Asyncsmb_TTODS_queryticketservice.AsyncQueryTicketBinding_v1_5 prepareCallout(Asyncsmb_TTODS_queryticketservice.AsyncQueryTicketBinding_v1_5 serPort)
    {
        String creds;
        SMBCare_WebServices__c wsUsername = SMBCare_WebServices__c.getValues('username');
        SMBCare_WebServices__c wsPassword = SMBCare_WebServices__c.getValues('password');
        if (String.isNotBlank(WSusername.Value__c)&& String.isNotBlank(wsPassword.Value__c))
            creds = WSusername.Value__c+':'+wsPassword.Value__c;
        String encodedusernameandpassword = EncodingUtil.base64Encode(Blob.valueOf(creds));
        
        serPort.inputHttpHeaders_x = new Map<String, String>();
        serPort.inputHttpHeaders_x.put('Authorization','Basic ' + encodedusernameandpassword );
        // Adding EM Header - START
        
        smb_cust_telus_em_header_v1_new.emRoutingRulesType er = new smb_cust_telus_em_header_v1_new.emRoutingRulesType();
        er.transport = 'http';
        er.host = 'troubleticketsvc-pt.tsl.telus.com';
        er.port = '80';
        er.uri='/troubleticketODS/assurance/webservice/QueryTicketPort';
        //er.envString = 'AT01'; To be left blank
        smb_cust_telus_em_header_v1_new.emHeaderType eh = new smb_cust_telus_em_header_v1_new.emHeaderType();
        eh.routingRules = er;
        serPort.emHeader = eh;
        // Adding EM Header - END
        return serPort; 
    }
    
/*
* Log error message in object
* This is future method which invoked asynchronousely so that user dont need to wait untill to finish this method
*/
    @future
    public static void logError(String classAndMethodName, String recId, String errorCodeAndMsg, String stackTrace){
        webservice_integration_error_log__c errorLog = new webservice_integration_error_log__c
            (apex_class_and_method__c=classAndMethodName,
             external_system_name__c='Ticket Aggregation',
             webservice_name__c='SRM and TTODS',
             object_name__c='Account',
             sfdc_record_id__c=recId,
             Error_Code_and_Message__c = errorCodeAndMsg,
             Stack_Trace__c = stackTrace
            );
        insert errorLog;
    }
    
/* 
Please DO NOT Delete this method, it will be invoked in test class smb_Ticket_Aggregator_Test after continuation process.
*/
    public Object processTTAggregationResponse_test(){
        return null;
    }
}