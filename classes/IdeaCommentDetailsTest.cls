@isTest
private class IdeaCommentDetailsTest {

    private static testMethod void testCtor() {
    	Idea i = new Idea(Title = 'test', communityId = getCommunityIdForTest());
    	insert i;
    	
    	IdeaComment ic = new IdeaComment(IdeaId = i.id, CommentBody = 'body');
    	insert ic;
    	
    	IdeaCommentDetails icd = new IdeaCommentDetails(i,ic);
    	
    	system.assertEquals(false,icd.read);
    }
    
    private static testMethod void testGetIdeasWithComments() {
    	Idea i1 = new Idea(Title = 'test1', communityId = getCommunityIdForTest());
    	Idea i2 = new Idea(Title = 'test2', communityId = getCommunityIdForTest());
    	insert new List<Idea>{i1, i2};
    	
    	IdeaComment ic1 = new IdeaComment(IdeaId = i1.id, CommentBody = 'body1');
    	IdeaComment ic2 = new IdeaComment(IdeaId = i2.id, CommentBody = 'body2');
    	insert new List<IdeaComment>{ic1, ic2};
    	
    	List<Idea> ideas = [SELECT Title, LastCommentId FROM Idea WHERE Id = :i1.id OR Id = :i2.id];
    	
    	List<IdeaCommentDetails> results = IdeaCommentDetails.getIdeasWithComments(ideas);
    	
    	system.assertEquals(2, results.size());
    	system.assertEquals(i1.id,results[0].idea.id);
    	system.assertEquals(ic1.id, results[0].ideaComment.id);
    	system.assertEquals(i2.id,results[1].idea.id);
    	system.assertEquals(ic2.id, results[1].ideaComment.id);
    }
    
    private static testMethod void testGetIdeasWithCommentsMap() {
    	Idea i1 = new Idea(Title = 'test1', communityId = getCommunityIdForTest());
    	Idea i2 = new Idea(Title = 'test2', communityId = getCommunityIdForTest());
    	List<Idea> ideas = new List<Idea>{i1, i2};
    	insert ideas;
    	
    	IdeaComment ic1 = new IdeaComment(IdeaId = i1.id, CommentBody = 'body1');
    	IdeaComment ic2 = new IdeaComment(IdeaId = i2.id, CommentBody = 'body2');
    	insert new List<IdeaComment>{ic1, ic2};
    	
    	Map<Id, IdeaComment> commentMap = new Map<Id, IdeaComment>{i1.id => ic1, i2.id => ic2};
    	
    	List<IdeaCommentDetails> results = IdeaCommentDetails.getIdeasWithComments(ideas, commentMap);
    	
    	system.assertEquals(2, results.size());
    	system.assertEquals(i1.id,results[0].idea.id);
    	system.assertEquals(ic1.id, results[0].ideaComment.id);
    	system.assertEquals(i2.id,results[1].idea.id);
    	system.assertEquals(ic2.id, results[1].ideaComment.id);
    }
    
		private static testMethod void testGetIdeasWithCommentsByUser() {
    	Idea i1 = new Idea(Title = 'test1', communityId = getCommunityIdForTest());
    	Idea i2 = new Idea(Title = 'test2', communityId = getCommunityIdForTest());
    	insert new List<Idea>{i1, i2};
    	
    	IdeaComment ic1 = new IdeaComment(IdeaId = i1.id, CommentBody = 'body1');
    	IdeaComment ic2 = new IdeaComment(IdeaId = i2.id, CommentBody = 'body2');
    	insert new List<IdeaComment>{ic1, ic2};
    	
    	List<Idea> ideas = [SELECT Title, LastCommentId FROM Idea WHERE Id = :i1.id OR Id = :i2.id];
    	
    	List<IdeaCommentDetails> results = IdeaCommentDetails.getIdeasWithComments(ideas, UserInfo.getUserId());
    	
    	system.assertEquals(2, results.size());
    }
    
    private static testMethod void testGetIdeasWithCommentsAndReadStatus() {
    	Idea i1 = new Idea(Title = 'test1', communityId = getCommunityIdForTest());
    	Idea i2 = new Idea(Title = 'test2', communityId = getCommunityIdForTest());
    	insert new List<Idea>{i1, i2};
    	
    	IdeaComment ic1 = new IdeaComment(IdeaId = i1.id, CommentBody = 'body1');
    	IdeaComment ic2 = new IdeaComment(IdeaId = i2.id, CommentBody = 'body2');
    	insert new List<IdeaComment>{ic1, ic2};
    	
    	List<Idea> ideas = [SELECT Title, LastCommentId FROM Idea WHERE Id = :i1.id OR Id = :i2.id];
    	
    	List<IdeaCommentDetails> results = IdeaCommentDetails.getIdeasWithCommentsAndReadStatus(ideas, new List<Id>{i1.id});
    	
    	system.assertEquals(2, results.size());
    	system.assertEquals(i1.id,results[0].idea.id);
    	system.assertEquals(ic1.id, results[0].ideaComment.id);
    	system.assertEquals(false,results[0].read);
    	system.assertEquals(i2.id,results[1].idea.id);
    	system.assertEquals(ic2.id, results[1].ideaComment.id);
    	system.assert(results[1].read);
    }
    
    
    // TEST HELPERS
    
    @istest
    private static Id getCommunityIdForTest() {
    	return [SELECT id FROM Community WHERE isActive = true LIMIT 1].id;
    }
}