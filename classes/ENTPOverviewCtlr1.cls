/**
*  ENTPOverviewCtlr1.cls
*
*  @description
*       The Controller for the Overview and Manage Requests Page.  Copied to a new file and cleaned by Dan.  Authored By Rauza, Christian, Jimmy and Alex as well.
* rbrekke
*   @date - 03/01/2014
*   @author - Dan Reich: Traction on Demand
*/

public with sharing class ENTPOverviewCtlr1 extends Portal_OverviewCtlr {
	public string CatListPull = '';
	public string AddFuncPull = '';
	public string CustRole = '';
	public String telusID { get; set; }

	public static ENTP_Customer_Interface_Settings__c cis;
	public static Portal_Interface_Settings cs;

	public static final string ENTPIDENTIFIER = 'onlineCaseRTToRequestType';
	public static final string CASEORIGIN = 'ENTP';

	public ENTPOverviewCtlr1() {
		super(ENTPIDENTIFIER, CASEORIGIN, getInterfaceSettings());
		CasNumViaExistingLynxTicket = ApexPages.currentPage().getParameters().get('CasNumViaExistingLynxTicket');

		if (!Test.isRunningTest()) {
			resetRequestView();
		}

	}

	public static Portal_Interface_Settings getInterfaceSettings() {
		cis = ENTP_Customer_Interface_Settings__c.getInstance();
		String vco = cis.Case_Origin_Value__c;
		Decimal mrll = cis.Manage_Requests_List_Limit__c;
		Decimal ocl = cis.Overview_Case_Limit__c;
		if (cis == null) {
			cs = new Portal_Interface_Settings(caseOrigin, 10, 5);
		} else {
			cs = new Portal_Interface_Settings(vco, mrll, ocl);
		}

		return cs;
	}

	// Override
	public override PageReference searchOverview() {

		PageReference pageRef = Page.ENTPManageTickets;
		if (!Test.isRunningTest()) {
			resetRequestView();
		}
		pageRef.getParameters().put('search', q);
		pageRef.getParameters().put('CasNumViaExistingLynxTicket', CasNumViaExistingLynxTicket);
		pageRef.setRedirect(true);

		return pageRef;
	}

	//override
	public override PageReference initCheck() {
		System.debug('ENTPOverviewCtlr1, initCheck');

		getRecentCases();

		System.debug('UserInfo.getUserId(): ' + UserInfo.getUserId());

		userAccountId = [SELECT AccountId FROM User WHERE Id = :UserInfo.getUserId()].AccountId;
		//string TelusProducts;
		string additionalFunc;
		system.debug('userAccountId: ' + userAccountId);
		List<User> TelusProducts = [SELECT Customer_Portal_Role__C FROM User WHERE id = :UserInfo.getUserId()];
		//system.debug('Customer_Portal_Role__C: ' + TelusProducts);

		CustRole = TelusProducts[0].Customer_Portal_Role__C;
		if (CustRole != '' && CustRole != null) {

			string[] CustRoleT = CustRole.split(';');
			CustRole1 = CustRoleT[0];
			//return CustRole1;
			system.debug('Customer Role: ' + CustRole1);
			if (CustRole1 == 'private cloud' || CustRole1 == 'mits') {
				String BUIDChk = '';
				STring COMIDChk = '';
				user cntId1 = [Select ContactId, AccountId from User where id = :UserInfo.getUserId()];
				string ContactI1 = cntId1.ContactId;
				String AccountI1 = cntId1.AccountId;
				Account g1 = [Select Remedy_Business_Unit_Id__c, Remedy_Company_Id__c from Account where id = :AccountI1 LIMIT 1];
				BUIDChk = g1.Remedy_Business_Unit_Id__c;
				COMIDChk = g1.Remedy_Company_Id__c;

				Contact p1 = [Select Remedy_PIN__c from Contact where id = :ContactI1 LIMIT 1];
				String PINNumchk = p1.Remedy_PIN__c;
				System.debug('Line 72 PIN: ' + p1.Remedy_PIN__c);
				if (PINNumchk == null) {
					PinNumRet();

				}
			}
			if (CustRole != '') {
				List<ENTP_portal_additional_functions__c> ENTPPortalMap = [Select Name,Customer_Portal_Role__c,Additional_Function__c from ENTP_portal_additional_functions__c where Customer_Portal_Role__c = :CustRole];
				if (ENTPPortalMap.size() > 0) {
					CatListPull = ENTPPortalMap[0].Additional_Function__c;
					String[] CatListPullT = CatListPull.split(';');
					AddFuncPull1 = CatListPullT[0];
					system.debug('CatListPullT: ' + CatListPullT);
				}

			}
		}
		return null;
	}

	//override
	public override PageReference createNewCase() {
		//categoryType = 'CCICatRequest11';
		PageReference pageRef = Page.ENTPNewCase;
		if (categoryType != null) {

			pageRef.getParameters().put('category', categoryType);
		}
		pageRef.setRedirect(true);

		return pageRef;
	}

	//override
	public override PageReference getCaseDetails() {
		//categoryType = 'CCICatRequest11';
		PageReference pageRef = Page.ENTPCaseDetailPage;
		if (caseNumber != null) {

			pageRef.getParameters().put('caseNumber', caseNumber);
		}
		pageRef.setRedirect(true);

		return pageRef;
	}

	// Override
	public override List<SelectOption> getCategoryFilterOptions() {
		List<SelectOption> options = new List<SelectOption>();
		Boolean restrictTechSupport = ENTPUtils.restrictTechSupport();
		options.add(new SelectOption('', label.mbrFilterAllCategories));
		for (String category : ENTPUtils.getCategoryRequestTypes().keySet()) {
			if (!category.equals('Tech support') || !restrictTechSupport) {
				options.add(new SelectOption(category, category));
			}
		}
		return options;
	}

	// Override
	public override List<SelectOption> getStatusFilterOptions() {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('', label.mbrFilterAllStatuses));
		for (String status : ENTPUtils.getExternalToInternalStatuses().keySet()) {
			options.add(new SelectOption(status, status));
		}
		return options;
	}

	// Unique
	public void callMethod() {
		// if(!test.isRunningTest()){
		resetRequestView();
		// }
	}

	//Override
	public override List<Case> executeSearch(Integer counter, Integer size, Boolean recount) {
		Set<Case> tmpCases = new Set<Case>();
		List<Case> soqlCases = new List<Case>();
		List<Case> soslCases = new List<Case>();
		List<Case> retval = new List<Case>();
		String soslCasesIdString = '';
		searchError = null; //null out the search error string

		System.debug('executeSearch: ' + q);
		if (String.isNotEmpty(q) && q.length() > 1) {
			// now do a general begins-with sosl search
			String qWild = q + '*';
			String whereClause = ' WHERE Origin = \'' + String.escapeSingleQuotes(this.caseOriginValue) + '\' ';
			String whereJoiner = ' AND ';
			if (String.isNotEmpty(categoryFilter)) {
				whereClause += whereJoiner + 'My_Business_Requests_Type__c IN ' + ENTPUtils.getRequestTypeFilterForCategory(categoryFilter);
			}
			if (String.isNotEmpty(statusFilter)) {
				// whereClause += whereJoiner + ' Status IN ' + ENTPUtils.getInternalStatusFilterByExternal( statusFilter );
				if (statusFilter != 'My tickets') {
					whereClause += whereJoiner + ' Status IN ' + ENTPUtils.getInternalStatusFilterByExternal(statusFilter);
				} else {
					whereClause += whereJoiner + ' ContactId = :UserInfo.getUserId() ';
				}

			}
			String feedbackCond1 = 'Complaint';
			String feedbackCond2 = 'Compliment';
			String feedbackCond3 = 'Idea';
			String feedbackCond4 = 'Feedback';
			whereClause += whereJoiner + 'My_Business_Requests_Type__c <> :feedbackCond1' + whereJoiner + 'My_Business_Requests_Type__c <> :feedbackCond2' + whereJoiner + 'My_Business_Requests_Type__c <> :feedbackCond3' + whereJoiner + 'My_Business_Requests_Type__c <> :feedbackCond4';

			String orderByClause = ' ORDER BY LastModifiedDate DESC ';
			String limitOffsetClause = ' LIMIT ' + size + ' OFFSET ' + counter;
			String allSoslQuery = 'FIND \'' + String.escapeSingleQuotes(qWild) + '\' IN ALL FIELDS RETURNING Case (Id, CaseNumber, Subject, Description ' + whereClause + orderByClause + ')';
			System.debug('akong: executeSearch: allSoslQuery: ' + allSoslQuery);
			try {
				List<List<Case>> allSoslResults = search.query(allSoslQuery);
				soslCases = (List<Case>) allSoslResults[0];
			} catch (Exception e) {
				System.debug('akong: executeSearch: sosl search exception: ' + e);
			}
			System.debug('akong: executeSearch: soslCases: ' + soslCases);
			// build the string of case IDs to use in the main soql query
			if (soslCases.size() > 0) {
				for (Case soslCase : soslCases) {
					soslCasesIdString += (String.isNotEmpty(soslCasesIdString) ? ', ' : '') + '\'' + soslCase.Id + '\'';
				}
				soslCasesIdString = '(' + soslCasesIdString + ')';
			} else if (!q.isNumeric()) {
				// search was a non-numeric keyword search, and we got no hits... time to bail!
				searchError = 'Ticket Not Found, If searching for a ticket number, please wait and try searching again as your ticket may not have been visible ';
				System.debug('Search Error: ' + searchError);
				System.debug('line 458, no hits: soslCases: ' + soslCases);
				return soslCases;
			}
		}

		System.debug('akong: executeSearch: soslCases.size(): ' + soslCases.size());

		// time to build the main soql query
		String queryString = 'SELECT CaseNumber, Status, RecordTypeId, Subject, LastModifiedDate, My_Business_Requests_Type__c, Lynx_Ticket_Number__c, Trouble_Ticket__c, Contact.FirstName, Contact.LastName FROM Case';
		String whereClause = ' WHERE Origin = \'' + String.escapeSingleQuotes(this.caseOriginValue) + '\' ';
		String whereJoiner = ' AND ';
		if (String.isNotEmpty(categoryFilter)) {
			whereClause += whereJoiner + ' My_Business_Requests_Type__c IN ' + ENTPUtils.getRequestTypeFilterForCategory(categoryFilter);
		}
		if (String.isNotEmpty(statusFilter)) {
			system.debug('Status Filter' + statusFilter);
			if (statusFilter != 'My tickets') {
				whereClause += whereJoiner + ' Status IN ' + ENTPUtils.getInternalStatusFilterByExternal(statusFilter);
			} else {
				List<user> CtactId = [Select AccountId from user where ID = :UserInfo.getUserId()];
				if (CtactId.size() > 0) {
					string userAccountId = CtactId[0].AccountId;
					System.debug('Line 519: Account ID: ' + userAccountId);
					whereClause += whereJoiner + 'CreatedById =\'' + String.escapeSingleQuotes(UserInfo.getUserId()) + '\' ';
				}

			}
		}
		if (String.isNotEmpty(q) && q.isNumeric()) {
			// case number search
			String queryCase = '\'%' + q + '%\'';
			whereClause += whereJoiner + ' (CaseNumber LIKE ' + queryCase;
			if (soslCases.size() > 0) {
				// fold in list of case IDs from sosl search
				whereClause += ' OR Id IN ' + soslCasesIdString;
			}
			whereClause += ')';
			whereJoiner = ' AND ';
		} else if (soslCases.size() > 0) {
			// we have a list of case IDs from the sosl search
			whereClause += whereJoiner + ' Id IN ' + soslCasesIdString;
			whereJoiner = ' AND ';
		}
		String feedbackCond1 = 'Complaint';
		String feedbackCond2 = 'Compliment';
		String feedbackCond3 = 'Idea';
		String feedbackCond4 = 'Feedback';
		whereClause += whereJoiner + 'My_Business_Requests_Type__c <> :feedbackCond1' + whereJoiner + 'My_Business_Requests_Type__c <> :feedbackCond2' + whereJoiner + 'My_Business_Requests_Type__c <> :feedbackCond3' + whereJoiner + 'My_Business_Requests_Type__c <> :feedbackCond4';

		if (String.isNotEmpty(whereClause)) {
			queryString += whereClause;
		}

		String orderByClause = ' ORDER BY LastModifiedDate DESC';
		String limitOffsetClause = ' LIMIT ' + size + ' OFFSET ' + counter;

		String limitLargeSize = ' LIMIT ' + 1000 + ' OFFSET ' + 0;
		System.debug('akong: executeSearch: queryString: ' + queryString);
		System.debug('akong: executeSearch: orderByClause: ' + orderByClause);
		System.debug('akong: executeSearch: limitOffsetClause: ' + limitOffsetClause);
		if (recount) {

			soqlCases = (List<Case>) database.query(queryString + orderByClause + limitLargeSize);
		} else {
			soqlCases = (List<Case>) database.query(queryString + orderByClause + limitOffsetClause);
		}

		// catchall logic if we searched for non-numeric keyword and got nothing from keyword search
		System.debug('soslCases.size: ' + soslCases.size());
		System.debug('String is Not empty: ' + String.isNotEmpty(q));
		System.debug('qNumeric: ' + q.isNumeric());
		if (String.isNotEmpty(q) && q.isNumeric() && soslCases.size() == 0) {

			if (CustRole1 == 'tps') {
				// getTroubleTicketDetail(telusID, RCID);
				system.debug('Future Count =' + FutCount);
				if (FutCount == 0) {
					system.debug('GetLynxTicketInfo-' + q);
					//GetLynxTicketInfo(q);
					LynxTicketSearchRET(q);
					FutCount += 1;
					resetRequestView();
					//executeSearch( counter, size, recount );
				}
			}

		}
		if (String.isNotEmpty(q) && !q.isNumeric() && soslCases.size() == 0) {
			soqlCases.clear();
		}

		System.debug('akong: executeSearch: soqlCases: ' + soqlCases);

		retval.addAll(soqlCases);

		if (retval.size() == 1 && retval[0].CaseNumber == q) {
			c = retval[0]; // immediate redirect to case details
		}

		return retval;
	}

	// @future(callout=true)
	/*  public void GetLynxTicketInfo(String ticketNum)
	  {

		  System.debug('Line 765 Lynx Ticket Number: ' + ticketNum);

		  //Rob - 08/15/2016 : Make the call to LynxUpdateActivity to send info to Lynx Web Service
		  //LynxTicketSearchRET.sendRequest smbCallOut = new LynxTicketSearchRET.sendRequest();
		  if(ticketNum != '' && ticketNum != null){
			  LynxTicketSearchRET(ticketNum);
		  }


	  } */

	// UNIQUE
	// public static void LynxTicketSearchRET(string ticketNum)
	public void LynxTicketSearchRET(string ticketNum) {

		List<String> TypeCode = new List<String>();
		string TELUSTicketId;
		String CommentsLynx;
		string DescriptionLynx;
		string SiteAddressAdd = '';
		string houseNumberAdd = '';
		string streetNameAdd = '';
		string cityNameAdd = '';
		string provStateNameAdd = '';
		string postalCodeAdd = '';

		String activityTypeCode;
		String TELUSActivityId;
		String assignToWorkgroup;
		String activityComments;
		String resolutionCode1;
		String resolutionCode2;
		String resolutionCode3 ;
		DateTime actualCompleteDateTime ;
		String completedFlag ;
		String statusCode ;
		DateTime actvitiyLastUpdateTimeTTODS ;
		String ContactFullName;//Added By RobB
		String ContactPhNumber;//Added By RobB
		String ContactEmail;//Added By RobB
		String ContactAddress;//Added By RobB
		String ContactType;//Added By RobB
		String ticketComments;//Added By CF
		String ticketNumber;//Added By Bharat
		String ticketStatus;//Added By Bharat
		String ticketType;//Added By Bharat
		String ticketSeverity;//Added By RobB
		String ticketWorkforce;//Added By RobB
		String ticketAssignedTo;//Added By Bharat
		String ticketDescription;//Added By Bharat
		String ticketPriority;//Added By Bharat
		DateTime ticketCreatedDate;//Added By Bharat
		DateTime ticketClosedDate;//Added By Bharat
		String ticketCustomerContact;//Added By Bharat
		String ticketCreatedBy;//Added By Bharat
		String ticketSiteAddress;//Added By RobB
		string LineOfBusiness;

		Integer I = 0;
		System.debug('CALLOUT Variables- LynxTicketSearch: ' + ticketNum);
		// Create the request envelope

		DOM.Document doc = new DOM.Document();
		//String endpoint = 'https://soa-mp-toll-st01.tsl.telus.com:443/SMO/ProblemMgmt/LynxTroubleTicketMgmtService_v1_0_vs0';
		//String endpoint = 'https://xmlgwy-pt1.telus.com:9030/st01/SMO/ProblemMgmt/TroubleTicketODSQueryService_v1_5_vs0';

		// TODO: Re-leverage custom setting for endpoint, no hard-coding
		String endpoint = SMBCare_WebServices__c.getInstance('TTODS_Endpoint_Search_Lynx').Value__c;
		//String endpoint = 'https://xmlgwy.telus.com:9030/SMO/ProblemMgmt/LynxTroubleTicketMgmtService_v1_0_vs0';
		////////////
		String soapNS = 'http://schemas.xmlsoap.org/soap/envelope/';
		string tro = 'http://www.telus.com/schema/servicestatus/troubleticket';
		// string ebon = 'http://ebonding.telus.com';
		// string asr = 'http://assurance.ebonding.telus.com';

		string logicalId = 'TELUS-SFDC';

		dom.XmlNode envelope
				= doc.createRootElement('Envelope', soapNS, 'soapenv');
		envelope.setNamespace('tro', tro);

		dom.XmlNode header = envelope.addChildElement('Header', soapNS, null);

		dom.XmlNode body = envelope.addChildElement('Body', soapNS, null);
		dom.XmlNode QueryTicket = body.addChildElement('QueryTicketSearchCriteria', tro, 'tro');
		QueryTicket.addChildElement('requestSystemName', tro, null).
				addTextNode(logicalId);
		QueryTicket.addChildElement('requestUserId', tro, null).
				addTextNode('');
		dom.XmlNode TicketId = QueryTicket.addChildElement('telusTicketId', tro, 'tro');
		TicketId.addChildElement('value', tro, null).
				addTextNode(ticketNum);
		TicketId.addChildElement('operator', tro, null).
				addTextNode('=');
		dom.XmlNode includeOpenTickets = QueryTicket.addChildElement('includeOpenTickets', tro, 'tro');
		includeOpenTickets.addChildElement('toInclude', tro, null).
				addTextNode('true');
		dom.XmlNode includeClosedTickets = QueryTicket.addChildElement('includeClosedTickets', tro, 'tro');
		includeClosedTickets.addChildElement('toInclude', tro, null).
				addTextNode('true');
		QueryTicket.addChildElement('upToXLatestModified', tro, null).
				addTextNode('200');
		dom.XmlNode includeActivities = QueryTicket.addChildElement('includeActivities', tro, 'tro');
		includeActivities.addChildElement('toInclude', tro, null).
				addTextNode('True');
		dom.XmlNode activityTypes = includeActivities.addChildElement('activityTypes', tro, 'tro');
		activityTypes.addChildElement('value', tro, null).
				addTextNode('Route');
		// activityTypes.addChildElement('value', tro, null).
		// addTextNode('Alert');
		activityTypes.addChildElement('value', tro, null).
				addTextNode('Notes');
		activityTypes.addChildElement('value', tro, null).
				addTextNode('Call - Inbound');
		activityTypes.addChildElement('value', tro, null).
				addTextNode('Notify Customer');
		activityTypes.addChildElement('value', tro, null).
				addTextNode('Resolution');
		activityTypes.addChildElement('value', tro, null).
				addTextNode('Test Remarks');
		activityTypes.addChildElement('operator', tro, null).
				addTextNode('IN');
		dom.XmlNode activityStates = includeActivities.addChildElement('activityStates', tro, 'tro');
		activityStates.addChildElement('value', tro, null).
				addTextNode('Cancelled');
		activityStates.addChildElement('operator', tro, null).
				addTextNode('NOT IN');
		includeActivities.addChildElement('upToXLatestModified', tro, null).
				addTextNode('200');
		System.debug(doc.toXmlString());

		// TODO: add exception handling

		// Send the request
		HttpRequest req = new HttpRequest();
		req.setMethod('POST');
		req.setTimeout(120000);
		req.setEndpoint(endpoint);
		// TODO: specify a timeout of 2 minutes
		// I.E. req.setTimeout(milliseconds);

		String creds;
		SMBCare_WebServices__c wsUsername = SMBCare_WebServices__c.getValues('username');
		SMBCare_WebServices__c wsPassword = SMBCare_WebServices__c.getValues('password');

		if (String.isNotBlank(wsUsername.Value__c) && String.isNotBlank(wsPassword.Value__c))
			creds = wsUsername.Value__c + ':' + wsPassword.Value__c;
		String encodedusernameandpassword = EncodingUtil.base64Encode(Blob.valueOf(creds));

		//Blob headerValue = Blob.valueOf(encodedusernameandpassword);
		String authorizationHeader = 'BASIC ' + encodedusernameandpassword;
		// EncodingUtil.base64Encode(headerValue);
		req.setHeader('Authorization', authorizationHeader);
		req.setHeader('Content-Type', 'text/xml');

		req.setBodyDocument(doc);

		// TODO: Determine the level of detail webservice is returning in regards to errors.

		Http http = new Http();
		HttpResponse res = new HttpResponse();
		dom.Document resDoc = null;
		XmlStreamReader reader = null;
		if (!Test.isRunningTest()) {
			res = http.send(req);
			System.debug('GetBody line 110:' + res.getBody());
			if (res.getStatusCode() != 200) {
				insert new Webservice_Integration_Error_Log__c(
						Apex_Class_and_Method__c = 'ENTPOverviewCtlr1.LynxTicketSearchRET',
						Error_Code_and_Message__c = 'Error during callout to Lynx: ' + res.getStatus(),
						Request_Payload__c = res.getBody()
				);
			}
		} else {
			StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'ENTP_LynxTicketSearchXMLResult' LIMIT 1];
			res.setBody(sr.Body.toString());
			System.debug('SD: ' + res.getBody());
		}

		resDoc = res.getBodyDocument();
		reader = res.getXmlStreamReader();
		//System.assertEquals(500, res.getStatusCode());

		// Read through the XML
		TELUSTicketId = '';
		DescriptionLynx = '';
		CommentsLynx = '';
		ticketStatus = '';
		ticketSeverity = '';
		ticketPriority = '';
		ticketWorkforce = '';
		ticketType = '';
		SiteAddressAdd = '';
		houseNumberAdd = '';
		streetNameAdd = '';
		cityNameAdd = '';
		ProvStateNameAdd = '';
		postalCodeAdd = '';
		LineOfBusiness = '';
		ticketAssignedTo = '';
		ticketCustomerContact = '';
		ContactPhNumber = '';

		ContactEmail = '';

		while (reader != null && reader.hasNext()) {
			System.debug('Event Type:' + reader.getEventType());

			if (reader.getEventType() == XmlTag.START_ELEMENT) {
				System.debug('getLocalName: ' + reader.getLocalName());
				TELUSTicketId = TELUSTicketId == '' ? getValueFromReader(reader, 'TELUSTicketId', false) : TELUSTicketId;
				DescriptionLynx = DescriptionLynx == '' ? getValueFromReader(reader, 'description', false) : DescriptionLynx;
				CommentsLynx = CommentsLynx == '' ? getValueFromReader(reader, 'comments', true) : CommentsLynx;
				ticketStatus = ticketStatus == '' ? getValueFromReader(reader, 'statusCode', false) : ticketStatus;
				ticketType = ticketType == '' ? getValueFromReader(reader, 'ticketType', false) : ticketType;
				ticketSeverity = ticketSeverity == '' ? getValueFromReader(reader, 'severityCode', false) : ticketSeverity;
				ticketPriority = ticketPriority == '' ? getValueFromReader(reader, 'priorityCode', false) : ticketPriority;
				ticketWorkforce = ticketWorkforce == '' ? getValueFromReader(reader, 'workforceReqiured', false) : ticketWorkforce;
				LineOfBusiness = LineOfBusiness == '' ? getValueFromReader(reader, 'lineOfBusinessCode', false) : LineOfBusiness;
				SiteAddressAdd = SiteAddressAdd == '' ? getValueFromReader(reader, 'serviceAddress', false) : SiteAddressAdd;
				houseNumberAdd = houseNumberAdd == '' ? getValueFromReader(reader, 'houseNumber', false) : houseNumberAdd;
				streetNameAdd = streetNameAdd == '' ? getValueFromReader(reader, 'streetName', false) : streetNameAdd;
				cityNameAdd = cityNameAdd == '' ? getValueFromReader(reader, 'cityName', false) : cityNameAdd;
				ProvStateNameAdd = ProvStateNameAdd == '' ? getValueFromReader(reader, 'provinceStateName', false) : ProvStateNameAdd;
				postalCodeAdd = postalCodeAdd == '' ? getValueFromReader(reader, 'postalCode', false) : postalCodeAdd;

				ticketAssignedTo = ticketAssignedTo == '' ? getValueFromReader(reader, 'assignToWorkgroupName', false) : ticketAssignedTo;
				ticketCustomerContact = ticketCustomerContact == '' ? getValueFromReader(reader, 'fullName', false) : ticketCustomerContact;
				ContactPhNumber = ContactPhNumber == '' ? getValueFromReader(reader, 'telephoneNumber', false) : ContactPhNumber;

				ContactEmail = ContactEmail == '' ? getValueFromReader(reader, 'electronicAddress', false) : ContactEmail;

				if ('electronicAddressValue' == (reader.getLocalName())) {

					boolean isSafeToGetNextXmlElement = true;
					while (isSafeToGetNextXmlElement) {
						if (reader.getEventType() == XmlTag.END_ELEMENT) {
							break;
						} else if (reader.getEventType() == XmlTag.CHARACTERS) {
							ContactEmail = +ContactEmail + '-' + reader.getText();

							//System.debug('Email :' + ContactEmail);
						}
						// Always use hasNext() before calling next() to confirm
						// that we have not reached the end of the stream

						if (reader.hasNext()) {

							reader.next();

						} else {

							isSafeToGetNextXmlElement = false;
							break;
						}
					}

				}

				if ('activityTypeCode' == (reader.getLocalName()) || 'actvitiyLastUpdateTimeTTODS' == (reader.getLocalName()) || 'assignToWorkgroup' == (reader.getLocalName()) || 'activityComments' == (reader.getLocalName()) || 'statusCode' == (reader.getLocalName())) {
					String actType = 'No';
					String dateUpd = 'No';

					if ('activityTypeCode' == (reader.getLocalName())) {
						I = I + 1;
						actType = 'Yes';
					}
					if ('actvitiyLastUpdateTimeTTODS' == (reader.getLocalName())) {
						dateUpd = 'yes';

					}
					boolean isSafeToGetNextXmlElement = true;

					while (isSafeToGetNextXmlElement) {
						if (reader.getEventType() == XmlTag.END_ELEMENT) {
							break;
						} else if (reader.getEventType() == XmlTag.CHARACTERS) {
							activityTypeCode = reader.getText();
							if (actType == 'Yes') {
								if (I == 1) {
									TypeCode.Add('<strong>' + activityTypeCode + ':</strong>&nbsp;&nbsp;');
								} else {
									TypeCode.Add('<br/><br/><strong>' + activityTypeCode + ':</strong>&nbsp;&nbsp;');
								}

							} else

							{
								TypeCode.Add(activityTypeCode + '&nbsp;&nbsp;');
							}

							System.debug('Activity Type Code :' + activityTypeCode);
							System.debug('Activity Type Code List :' + TypeCode);
						}
						// Always use hasNext() before calling next() to confirm
						// that we have not reached the end of the stream

						if (reader.hasNext()) {

							reader.next();

						} else {

							isSafeToGetNextXmlElement = false;
							break;
						}
					}

				}
			}
			reader.next();
		}
		ticketSiteAddress = SiteAddressAdd + ', ' + houseNumberAdd + ', ' + streetNameAdd + ', ' + cityNameAdd + ',' + postalCodeAdd;
		envelope = resDoc != null ? resDoc.getRootElement() : null;
		//  String troubleTick = 'http://www.telus.com/schema/servicestatus/troubleticket';
		//String wsa = 'http://schemas.xmlsoap.org/soap/envelope/';
		//Dom.XMLNode TroubleTicket = resDoc.getRootElement();
		// dom.XmlNode header1 = envelope.getChildElement('Header', soapNS);
		// String TELUSTicketId = TroubleTicket.getChildElement('TELUSTicketId',null).getText();
		//System.debug('TELUSTicketId: ' + TELUSTicketId);
		if (resDoc != null) {
			System.debug(resDoc.toXmlString());
		}
		System.debug('Envelope=:' + envelope);
		//  System.debug('TroubleTicket=:' + TroubleTicket);
		//System.debug(header1);
		//System.debug(testTime);
		System.debug('TELUSTicketId? ' + TELUSTicketId);

		/******* Create ticket in SFDc */
		if (TELUSTicketId != '' && TELUSTicketId != null) {

			List<Case> chk = [Select CaseNumber from Case where Lynx_Ticket_Number__c = :TELUSTicketId];

			if (chk.isEmpty()) {
				try {
					Case addCase = new Case(
							Subject = DescriptionLynx,
							Description = CommentsLynx,
							Lynx_Ticket_Number__c = TELUSTicketId,
							Ticketing_System__c = 'LYNX',
							My_Business_Requests_Type__c = LineOfBusiness,
							Request_Type__c = 'Imported from Lynx',
							Origin = 'ENTP',
							RecordTypeId = '01240000000DweCAAS'
					);
					insert addCase;
					system.debug('Insert Case from Lynx, ID=' + addCase.id);

					List<Case> c = [Select CaseNumber, id from Case where Lynx_Ticket_Number__c = :TELUSTicketId];
					system.debug('Select statement for SFDC Lynx Ticket-' + c);

					CasNumViaExistingLynxTicket = c[0].CaseNumber;
					system.debug('SFDC CasNumViaExistingLynxTicket-' + CasNumViaExistingLynxTicket);
					//ENTPOverviewCtlr1.searchOverview();
					// GetLynxDetailPage(CasNumViaExistingLynxTicket);
					// GetcaseDetailLynx(c[0].CaseNumber);
					// CaseListing.getPageOfCases(0,1);
					/*    PageReference pageref = Page.ENTPCaseDetailPage;
	   pageref.getParameters().put('caseNumber', c[0].CaseNumber);
	   pageref.setRedirect(true); */

				} catch (DmlException e) {
					System.debug('An unexpected error has occurred: ' + e.getMessage());
				}

			}
		}

		//return null;

	}

	@TestVisible private String getValueFromReader(XmlStreamReader reader, String name, Boolean getMulti) {
		String result = '';
		Boolean isSafe = true;
		Boolean done = false;

		if (name == (reader.getLocalName())) {
			System.debug('getValueFromReader: ' + name);
			isSafe = true;
			while (isSafe) {
				if (reader.getEventType() == XmlTag.END_ELEMENT) {
					break;
				} else if (reader.getEventType() == XmlTag.CHARACTERS) {
					System.debug('getMulti: ' + getMulti);
					if (getMulti) {
						result += reader.getText();
						System.debug('multi result: ' + result);
					} else {
						System.debug('reader.getText(): ' + reader.getText());
						result = reader.getText();
						done = true;
						System.debug('result: ' + result);
					}
				}
				// Always use hasNext() before calling next() to confirm
				// that we have not reached the end of the stream

				if (reader.hasNext()) {

					reader.next();

					if (done) {
						break;
					}
				} else {
					isSafe = false;
					break;
				}
			}
		}

		return result;
	}
	//@future(callout = true)
	public void PinNumRet() {
		RemedyGetPIN.sendRequest smbCallOut = new RemedyGetPIN.sendRequest();
		smbCallOut.SRequest(userInfo.getUserEmail());

	}
/*  public static PageReference GetLynxDetailPage(string q){
        system.debug('Q = ' + q);
       PageReference pageRef = new PageReference('/apex/ENTPManageRequests');
      // resetRequestView();
       pageRef.getParameters().put('search', q);
       pageRef.setRedirect(true);
        
        return pageRef;} */

}