@isTest
private class SFDCEncoderTest {
	private static final String INPUT = '&';
  private static final String EXPECTED_OUTPUT = '&amp;';
	
	private static testMethod void testSFDC_HTMLENCODE() {
    	
    	
    	String results = SFDCEncoder.SFDC_HTMLENCODE(INPUT);
    	
    	system.assertEquals(EXPECTED_OUTPUT, results);
    }
    
    private static testMethod void testSFDC_HTMLENCODENull() {    	
    	String results = SFDCEncoder.SFDC_HTMLENCODE(null);
    	
    	system.assertEquals(null, results);
    }
}