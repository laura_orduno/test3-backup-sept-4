@isTest
private class OCOM_PAC_MACDContactExt_Test{
    
    @TestSetup
    private static void setupData(){
		Schema.DescribeSObjectResult d = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
          
        Id recSerTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        Id recRCIDTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId();        
          
        Account RCIDacc = new Account(name='debs',recordtypeid=recRCIDTypeId);
        Account seracc = new Account(name='123 street',recordtypeid=recSerTypeId,parentid=RCIDacc.id);          
          
        List<Account> acclist= new List<Account>();          
        acclist.add(RCIDacc);
        acclist.add(seracc );
        insert acclist;
          
        smbcare_address__c address= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N5',FMS_Address_ID__c='987650',account__c=RCIDacc.id); 
        smbcare_address__c address3= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N5',FMS_Address_ID__c='987652',account__c=RCIDacc.id,service_account_Id__c=seracc.id); 
        smbcare_address__c address2= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N5',FMS_Address_ID__c='987651',account__c=RCIDacc.id); 
        List<smbcare_address__c> smbCarelist= new List<smbcare_address__c>(); 
        smbCarelist.add(address);
        smbCarelist.add(address3);
        insert smbCarelist;
        
        ServiceAddressData SData = new ServiceAddressData();
        SData.isManualCapture=true;
        SData.fmsId='987652';
        
        Contact Cont = new Contact (FirstName ='FName', LastName='LNAme', AccountId=seracc.Id, Email='Abc@abc.com');
        insert Cont;
        
        Contract Ct = new Contract(AccountId = RCIDacc.Id, Status = 'Draft',StartDate = Date.Today(), ContractTerm = 12);
        Insert Ct;
        
        Id standardPriceBookId = Test.getStandardPriceBookId();
        
        SMBCare_Address__c smbCareAddr = new SMBCare_Address__c(Account__c=RCIDacc.Id, Postal_Code__c='M1T3N3', Street_Number__c='Street 1', Street_Name__c='Street Name 1', Province__c='ONT', Suite_Number__c='1001', COID__c='COID 1', Building_Number__c='Building 1', Country__c='CAN', FMS_Address_ID__c='FMSID 121');
        insert smbCareAddr;
        
         Opportunity Opp= new Opportunity(Name='Test Opp',StageName='Discovery',CloseDate=System.today().addDays(5));
        insert Opp; 
        
        Quote Qt= new Quote(Name='Test Quote11',opportunityid=opp.id);
        insert Qt;
        
        Order Od = new Order(Name = 'ORD-000001', Shipping_Address__c='2010 100 AV NW EDMONTON AB T5A3M1', Service_Address_Text__c='2010 100 AV NW EDMONTON AB T5A3M1' , FMS_Address_ID__c='FMSID 121', ShippingStreet='2010 100 AV', ShippingCity='EDMONTON', BAN__c='Ban 21' , PriceBook2Id =standardPriceBookId  , AccountId = RCIDacc.Id, EffectiveDate = Date.Today(), ContractId = Ct.Id, Status = 'Draft', vlocity_cmt__QuoteId__c=Qt.Id);
        Od.Service_Address__c = address3.Id;
        Insert Od;
        
        Product2 Pd = new Product2(Name='Porduct 1',isActive=true);
        insert Pd;

       
        PricebookEntry Pbe = new PricebookEntry(Pricebook2Id=standardPriceBookId, Product2Id=pd.Id, UnitPrice=99, isActive=true);
        insert Pbe;

        OrderItem ordPd = new OrderItem(PriceBookEntryId=Pbe.Id, OrderId=Od.Id, Quantity=1, UnitPrice=99);
        insert ordPd;
        
        //Custom Setting for Endpoint Field
         
        SMBCare_WebServices__c cs = new SMBCare_WebServices__c(Name='SRS_ASFEndPoint',Value__c='https://xmlgwy-pt1.telus.com:9030/dv01/SMO/OrderMgmt/ServiceAddressManagementService_v1_1_vs0'); 
        insert cs;
        
        //Custom Setting for username Field
        SMBCare_WebServices__c cs1 = new SMBCare_WebServices__c(Name='username',Value__c ='APP_SFDC'); 
        insert cs1;
        //Custom Setting for password Field
        SMBCare_WebServices__c cs2 = new SMBCare_WebServices__c(Name='password',Value__c='soaorgid'); 
        insert cs2; 
    }
    
    @isTest
    private static void testOnLoad_1() {

    	List<Account> account_list = [SELECT Id FROM Account];
        System.assertEquals(2, account_list.size());
        
        Account seracc = [SELECT Id FROM Account WHERE RecordType.Name = 'Service'];
        Account RCIDacc = [SELECT Id FROM Account WHERE RecordType.Name = 'RCID'];
        System.assertNotEquals(NULL, seracc);
        System.assertNotEquals(NULL, RCIDacc);
        
        List<SMBCare_Address__c> address_list = [SELECT Id FROM SMBCare_Address__c];
        System.assertEquals(3, address_list.size());
        
        SMBCare_Address__c address = [SELECT Id FROM SMBCare_Address__c WHERE FMS_Address_ID__c='987650'];
        System.assertNotEquals(NULL, address);
        
        List<Contact> contact_list = [SELECT Id FROM Contact];
        System.assertEquals(1, contact_list.size());
        
        List<Order> order_list = [SELECT Id FROM Order];
        System.assertEquals(1, order_list.size());
        
        Contact Cont = contact_list.get(0);
        System.assertNotEquals(NULL, Cont);
        
        Order Od = order_list.get(0);
        System.assertNotEquals(NULL, Od);
    	
    	PageReference pageRef = Page.OCOM_InOrderFlowContact;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('orderId', od.Id);
        ApexPages.currentPage().getParameters().put('src', 'os');
        
        OCOM_InOrderFlowContactController controller = new OCOM_InOrderFlowContactController();
        OCOM_PAC_MACDContactExt myPageExt = new OCOM_PAC_MACDContactExt(controller);
        
        
        Test.startTest();
        myPageExt.onLoad();
        Test.stopTest();
    }
    
    @isTest
    private static void testOnLoad_2() {

    	List<Account> account_list = [SELECT Id FROM Account];
        System.assertEquals(2, account_list.size());
        
        Account seracc = [SELECT Id FROM Account WHERE RecordType.Name = 'Service'];
        Account RCIDacc = [SELECT Id FROM Account WHERE RecordType.Name = 'RCID'];
        System.assertNotEquals(NULL, seracc);
        System.assertNotEquals(NULL, RCIDacc);
        
        List<SMBCare_Address__c> address_list = [SELECT Id FROM SMBCare_Address__c];
        System.assertEquals(3, address_list.size());
        
        SMBCare_Address__c address = [SELECT Id FROM SMBCare_Address__c WHERE FMS_Address_ID__c='987650'];
        System.assertNotEquals(NULL, address);
        
        List<Contact> contact_list = [SELECT Id FROM Contact];
        System.assertEquals(1, contact_list.size());
        
        List<Order> order_list = [SELECT Id FROM Order];
        System.assertEquals(1, order_list.size());
        
        Contact Cont = contact_list.get(0);
        System.assertNotEquals(NULL, Cont);
        
        Order Od = order_list.get(0);
        System.assertNotEquals(NULL, Od);
    	
    	PageReference pageRef = Page.OCOM_InOrderFlowContact;
        Test.setCurrentPage(pageRef);
        //ApexPages.currentPage().getParameters().put('orderId', od.Id);
        //ApexPages.currentPage().getParameters().put('src', 'os');						// src!=os
        ApexPages.currentPage().getParameters().put('id', RCIDacc.Id);	
        
        OCOM_InOrderFlowContactController controller = new OCOM_InOrderFlowContactController();
        OCOM_PAC_MACDContactExt myPageExt = new OCOM_PAC_MACDContactExt(controller);
        
        
        Test.startTest();
        myPageExt.onLoad();
        myPageExt.onSelectContact();
        myPageExt.cancelNewContact();
        myPageExt.toggleNewContactForm();
        myPageExt.createNewContact();
        myPageExt.onSearch();
        Test.stopTest();
    }
    
    @isTest
    private static void testOnLoad_3_NoParams() {

    	PageReference pageRef = Page.OCOM_InOrderFlowContact;
        Test.setCurrentPage(pageRef);
        //ApexPages.currentPage().getParameters().put('orderId', od.Id);
        //ApexPages.currentPage().getParameters().put('src', 'os');						// src!=os
        //ApexPages.currentPage().getParameters().put('id', RCIDacc.Id);	
        
        OCOM_InOrderFlowContactController controller = new OCOM_InOrderFlowContactController();
        OCOM_PAC_MACDContactExt myPageExt = new OCOM_PAC_MACDContactExt(controller);

        Test.startTest();
        myPageExt.onLoad();
        Test.stopTest();
    }
    
    @isTest
    private static void testOnLoad_4_JustCalls() {

    	List<Account> account_list = [SELECT Id FROM Account];
        System.assertEquals(2, account_list.size());
        
        Account seracc = [SELECT Id FROM Account WHERE RecordType.Name = 'Service'];
        Account RCIDacc = [SELECT Id FROM Account WHERE RecordType.Name = 'RCID'];
        System.assertNotEquals(NULL, seracc);
        System.assertNotEquals(NULL, RCIDacc);
        
        List<SMBCare_Address__c> address_list = [SELECT Id FROM SMBCare_Address__c];
        System.assertEquals(3, address_list.size());
        
        SMBCare_Address__c address = [SELECT Id FROM SMBCare_Address__c WHERE FMS_Address_ID__c='987650'];
        System.assertNotEquals(NULL, address);
        
        List<Contact> contact_list = [SELECT Id FROM Contact];
        System.assertEquals(1, contact_list.size());
        
        List<Order> order_list = [SELECT Id FROM Order];
        System.assertEquals(1, order_list.size());
        
        Contact Cont = contact_list.get(0);
        System.assertNotEquals(NULL, Cont);
        
        Order Od = order_list.get(0);
        System.assertNotEquals(NULL, Od);
    	
    	PageReference pageRef = Page.OCOM_InOrderFlowContact;
        Test.setCurrentPage(pageRef);
        //ApexPages.currentPage().getParameters().put('orderId', od.Id);
        //ApexPages.currentPage().getParameters().put('src', 'os');						// src!=os
        ApexPages.currentPage().getParameters().put('id', RCIDacc.Id);	
        
        OCOM_InOrderFlowContactController controller = new OCOM_InOrderFlowContactController();
        OCOM_PAC_MACDContactExt myPageExt = new OCOM_PAC_MACDContactExt(controller);
        
        Test.startTest();
        myPageExt.onLoad();
        myPageExt.onSelectContact();
        myPageExt.cancelNewContact();
        myPageExt.toggleNewContactForm();
        myPageExt.createNewContact();
        myPageExt.onSearch();
        Test.stopTest();
    }
    
    
    @isTest
    private static void testOnSearch() {

    	List<Account> account_list = [SELECT Id FROM Account];
        System.assertEquals(2, account_list.size());
        
        Account seracc = [SELECT Id FROM Account WHERE RecordType.Name = 'Service'];
        Account RCIDacc = [SELECT Id FROM Account WHERE RecordType.Name = 'RCID'];
        System.assertNotEquals(NULL, seracc);
        System.assertNotEquals(NULL, RCIDacc);
        
        List<SMBCare_Address__c> address_list = [SELECT Id FROM SMBCare_Address__c];
        System.assertEquals(3, address_list.size());
        
        SMBCare_Address__c address = [SELECT Id FROM SMBCare_Address__c WHERE FMS_Address_ID__c='987650'];
        System.assertNotEquals(NULL, address);
        
        List<Contact> contact_list = [SELECT Id FROM Contact];
        System.assertEquals(1, contact_list.size());
        
        List<Order> order_list = [SELECT Id FROM Order];
        System.assertEquals(1, order_list.size());
        
        Contact Cont = contact_list.get(0);
        System.assertNotEquals(NULL, Cont);
        
        Order Od = order_list.get(0);
        System.assertNotEquals(NULL, Od);
    	
    	PageReference pageRef = Page.OCOM_InOrderFlowContact;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', RCIDacc.Id);	
        
        OCOM_InOrderFlowContactController controller = new OCOM_InOrderFlowContactController();
        OCOM_PAC_MACDContactExt myPageExt = new OCOM_PAC_MACDContactExt(controller);
        
        myPageExt.searchTerm = 'FNAME';
        
        Test.startTest();
        myPageExt.onSearch();
        Test.stopTest();
        
        System.assertEquals(null, myPageExt.contacts);
        System.assertEquals(0, myPageExt.contactsCount);
    }
    // This method was the only method in the class > coverage 0%
    testMethod static void CreateContacttest() {       

 		  Schema.DescribeSObjectResult d = Schema.SObjectType.Account; 
          Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();      
          Id recSerTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
          Id recRCIDTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId();       
          
          Account RCIDacc = new Account(name='debs',recordtypeid=recRCIDTypeId);
          Account seracc = new Account(name='123 street',recordtypeid=recSerTypeId,parentid=RCIDacc.id);        
          
          List<Account> acclist= new List<Account>();          
          acclist.add(RCIDacc);
          acclist.add(seracc );
          insert acclist;
         system.assertEquals(acclist.size(), 2); 
          
         smbcare_address__c address= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N5',FMS_Address_ID__c='987651',account__c=seracc.id); 
         insert address;
  		ServiceAddressData SDate = new ServiceAddressData();
       // PageReference pageref = Page.OCOM_PAC_MACD;
       // Test.setCurrentPage(pageref);        
     //   pageref.getparameters().put('id', seracc.Id);
        
       // OCOM_PAC_MACD_Controller con= new OCOM_PAC_MACD_Controller();
       // OCOM_PAC_MACDContactExt testCon= new OCOM_PAC_MACDContactExt(con);   
       Contact conTest = new Contact(LastName='testLastName',FirstName='testFirstName',email='a@gmail.com',phone='1234567890');   
        insert conTest;
        Test.setCurrentPageReference(new PageReference('Page.OCOM_PAC_MACD')); 
		System.currentPageReference().getParameters().put('id', '004132121212121');
        //testCon.new_contact=conTest;
        //testCon.onSelectContact();        
        //testCon.onLoad();        
		System.currentPageReference().getParameters().put('id',seracc.id);
       // testCon.onLoad();
      //  testCon.toggleNewContactForm();
                
		System.currentPageReference().getParameters().put('id',conTest.id);
        try{
        //testCon.createNewContact();
        }catch(exception ex){}
     //   testCon.searchTerm='test';
     //   testCon.onSearch(); 
     //   OCOM_PAC_MACDContactExt.selectedContactId=conTest.id;
       // testCon.onSelectContact();
            
    }


}