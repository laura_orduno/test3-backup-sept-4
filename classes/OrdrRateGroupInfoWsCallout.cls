/* 2015-12-12[Roderick de Vera]
 * Helper class which invokes the Web Service Callout for Rate Group Info 
 */
public class OrdrRateGroupInfoWsCallout {
    
    public static String ping() {
        String version;
        try { 
            RateGroupInfo_BillingaccountmgmtRa.RateGroupInfoService_v1_0_SOAP servicePort = new RateGroupInfo_BillingaccountmgmtRa.RateGroupInfoService_v1_0_SOAP(); 
            servicePort = prepareCallout(servicePort);          
            version = servicePort.ping();      
        } 
        catch (CalloutException e) {
            throw e;
        }
        return version;
    }
    
    public static RateGroupInfo_ResourceRes.RateGroupAndForborneAttribute getRateGroupAndForborneInformationByNpaNxx(String npaNxx) {
        RateGroupInfo_ResourceRes.RateGroupAndForborneAttribute response = new RateGroupInfo_ResourceRes.RateGroupAndForborneAttribute();
        try { 
            RateGroupInfo_BillingaccountmgmtRa.RateGroupInfoService_v1_0_SOAP servicePort = new RateGroupInfo_BillingaccountmgmtRa.RateGroupInfoService_v1_0_SOAP(); 
            servicePort = prepareCallout(servicePort);          
            response = servicePort.getRateGroupAndForborneInformationByNpaNxx(prepareRateGroupAndForborneCriteria(npaNxx));      
        } 
        catch (CalloutException e) {

            OrdrUtilities.auditLogs('OrdrRateGroupInfoWsCallout.getRateGroupAndForborneInformationByNpaNxx', 
                                    'ASF', 
                                    'RateGroupInfoService', 
                                    null, 
                                    null, 
                                    true, 
                                    e.getMessage(), 
                                    e.getStackTraceString());
            
            throw new OrdrExceptions.RateGroupInfoServiceException(e.getMessage(), e);        

        }
        
        return response;
    } 
    
    public static RateGroupInfo_ResourceRes.RateGroupAttribute getRateGroupInfoByNpaNxx(String npaNxx) {
        RateGroupInfo_ResourceRes.RateGroupAttribute response = new RateGroupInfo_ResourceRes.RateGroupAttribute();
        try { 
            RateGroupInfo_BillingaccountmgmtRa.RateGroupInfoService_v1_0_SOAP servicePort = new RateGroupInfo_BillingaccountmgmtRa.RateGroupInfoService_v1_0_SOAP(); 
            servicePort = prepareCallout(servicePort);          
            response = servicePort.getRateGroupInfoByNpaNxx(prepareCalculationCriteria(npaNxx));      
        } 
        catch (CalloutException e) {

            OrdrUtilities.auditLogs('OrdrRateGroupInfoWsCallout.getRateGroupInfoByNpaNxx', 
                                    'ASF', 
                                    'RateGroupInfoService', 
                                    null, 
                                    null, 
                                    true, 
                                    e.getMessage(), 
                                    e.getStackTraceString());
            
            throw new OrdrExceptions.RateGroupInfoServiceException(e.getMessage(), e);        

        }
        return response;
    }
    
    private static RateGroupInfo_BillingaccountmgmtRa.RateGroupInfoService_v1_0_SOAP 
        prepareCallout(RateGroupInfo_BillingaccountmgmtRa.RateGroupInfoService_v1_0_SOAP servicePort) {
            Ordering_WS__c rateGroupInfoEndpoint = Ordering_WS__c.getValues('RateGroupInfoEndpoint');           
            servicePort.endpoint_x = rateGroupInfoEndpoint.value__c;
            // Set SFDC Webservice call timeout
            servicePort.timeout_x = OrdrConstants.WS_TIMEOUT_MS;    
            
            if (servicePort.endpoint_x.startsWith('https://xmlgwy')) {
                Ordering_WS__c wsUserName =  Ordering_WS__c.getValues(OrdrConstants.CREDENTIAL_USERNAME);
                Ordering_WS__c wsPassword =  Ordering_WS__c.getValues(OrdrConstants.CREDENTIAL_PASSWORD); 
                String credentials = wsUserName.value__c + ':' + wsPAssword.value__c;
                String encodedUserNameAndPassword = EncodingUtil.base64Encode(Blob.valueOf(credentials));   
                
                servicePort.inputHttpHeaders_x = new Map<String, String>();
                servicePort.inputHttpHeaders_x.put('Authorization', 'Basic ' + encodedUserNameAndPassword);
            } else {
                servicePort.clientCertName_x = OrdrConstants.SDF_CERT_NAME;
            }
            
            return servicePort;
    } 
    
    private static RateGroupInfo_ResourceRes.RateGroupAndForborneCriteria prepareRateGroupAndForborneCriteria(String npaNxx) {
        RateGroupInfo_ResourceRes.RateGroupAndForborneCriteria criteria = new RateGroupInfo_ResourceRes.RateGroupAndForborneCriteria();

        criteria.npaNxx = npaNxx;
        criteria.effectiveStartDate = System.now();    
        criteria.customerType = 'B';
        return criteria;
    }
    
    private static RateGroupInfo_ResourceRes.CalculationCriteria prepareCalculationCriteria(String npaNxx) {
        RateGroupInfo_ResourceRes.CalculationCriteria criteria = new RateGroupInfo_ResourceRes.CalculationCriteria();
 
        criteria.npaNxx = npaNxx;
        criteria.effectiveStartDate = System.now();
        return criteria;
    }    

   
}