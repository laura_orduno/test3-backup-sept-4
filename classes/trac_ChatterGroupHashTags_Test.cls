@isTest
private class trac_ChatterGroupHashTags_Test {
	static testMethod void feedItemTest() {	
		Chatter_Hash_Groups__c chg = new Chatter_Hash_Groups__c(
			name = 'tRac38461623',
			group_name__c = 'TRACTIONTESTGROUP38461623'
		);
		insert chg;
		
		Chatter_Hash_Groups__c chg2 = new Chatter_Hash_Groups__c(
			name = 'trac48392300',
			group_name__c = 'TRACTIONTESTGROUP38461623'
		);
		insert chg2;
		
		CollaborationGroup cg = new CollaborationGroup(
			name = 'TRACTIONTESTGROUP38461623',
      CollaborationType = 'Public'
    );
    insert cg;
        
    FeedItem fi = new FeedItem(
    	parentid = UserInfo.getUserId(),
    	body = 'This is a #TrAC38461623 post',
    	type = 'TextPost'
    );
    insert fi;
        
    Integer groupCount = 0;
		for (CollaborationGroupFeed cgf : [SELECT body FROM CollaborationGroupFeed WHERE parentId = :cg.id]) {
			if (cgf.body == fi.body) {
				groupCount++;
			}
		}
		System.assertEquals(1,groupCount);
		
		Integer feedCount = 0;
		for (FeedItem anFi : [SELECT body FROM FeedItem WHERE parentId = :UserInfo.getUserID()]) {
			if (anFi.body == fi.body) {
				feedCount++;
			}
		}
		System.assertEquals(1,feedCount);
		
		FeedItem fi2 = new FeedItem(
			parentid = UserInfo.getUserId(),
			body = 'This is a #TRAC48392300 post',
			type = 'TextPost'
		);
    insert fi2;
        
    groupCount = 0;
		for (CollaborationGroupFeed cgf : [SELECT body FROM CollaborationGroupFeed WHERE parentId = :cg.id]) {
			if (cgf.body == fi2.body) {
				groupCount++;
			}
		}
		System.assertEquals(1,groupCount);
        
	}

  static testMethod void userStatusTest() {
  	Profile p = [SELECT id FROM Profile WHERE name = 'Standard User']; 
    User u = new User(
    	username = 'TESTUSER@TRACTIONSM.COM',
      email = 'test@example.com',
      lastname = 'test',
      alias = 'test',
      TimezoneSIDKey = 'America/Los_Angeles',
      LocaleSIDKey = 'en_US',
      EmailEncodingKey = 'UTF-8',
      ProfileId = p.id,
      LanguageLocaleKey = 'en_US'
    );
    
    User runner = u.clone();
    runner.username = 'HASHTAGSTESTRUNNER@TRACTIONSM.COM';
    
    insert u;
    
    system.runas(runner) {    
			Chatter_Hash_Groups__c chg = new Chatter_Hash_Groups__c(
				name = 'TRAC38461623',
				group_name__c = 'TRACTIONTESTGROUP38461623'
			);
			insert chg;
			
			CollaborationGroup cg = new CollaborationGroup(
				name = 'TRACTIONTESTGROUP38461623',
	      CollaborationType = 'Public'
	    );
	    insert cg;
	        
			u.CurrentStatus = 'Status Update containing #TrAc38461623';
			update u;
			
			Integer groupCount = 0;
			for (CollaborationGroupFeed cgf : [SELECT body FROM CollaborationGroupFeed WHERE parentId = :cg.id]) {
				if (cgf.body == u.CurrentStatus) {
					groupCount++;
				}
			}
			
			System.assertEquals(1,groupCount);
    }
  }
}