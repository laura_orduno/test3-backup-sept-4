/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class trac_GenericTest {

    static testMethod void myUnitTest() {

        User u1 = [SELECT Id FROM User WHERE IsActive = true AND Profile.Name = 'Sales Prime / ASR / Coordinator' LIMIT 1];
        User u2 = [SELECT Id FROM User WHERE IsActive = true AND LastName like '%Integration%' LIMIT 1];
        
        Contact myCon = new Contact();
        
        myCon.FirstName = 'Test';
        myCon.LastName = 'Test';
        insert myCon;
        
        ContactVO myVO = new ContactVO(myCon);
        
        string sGet;
        boolean bGet;
        Id idGet;
        
        myVO.FirstName = 'Test';
        myVO.LastName = 'Test';
        myVO.Title = 'Test';
        myVO.Role = 'Test';
        myVO.Department = 'Test';
        myVO.TitleCategory = 'Test';
        myVO.Language = 'Test';
        myVO.ContactMethod = 'Test';
        myVO.Phone = '111-111-1111';
        myVO.OtherPhone = '111-111-1111';
        myVO.Fax = '111-111-1111';
        myVO.MobilePhone = '111-111-1111';
        myVO.EMail = 'test@test.com';
        myVO.MailingStreet = 'Street';
        myVO.MailingCity = 'City';
        myVO.MailingProvince = 'Province';
        myVO.MailingPostalCode = 'PostalCode';
        myVO.MailingCountry = 'Country';
        myVO.OptIn = true;

        sGet = myVO.FirstName;
        sGet = myVO.LastName;
        sGet = myVO.Title;
        sGet = myVO.Role;
        sGet = myVO.Department;
        sGet = myVO.TitleCategory;
        sGet = myVO.Language;
        sGet = myVO.ContactMethod;
        sGet = myVO.Phone;
        sGet = myVO.OtherPhone;
        sGet = myVO.Fax;
        sGet = myVO.MobilePhone;
        sGet = myVO.EMail;
        sGet = myVO.MailingStreet;
        sGet = myVO.MailingCity;
        sGet = myVO.MailingProvince;
        sGet = myVO.MailingPostalCode;
        sGet = myVO.MailingCountry;
        bGet = myVO.OptIn;
        
        Account myAcc = new Account();
        
        myAcc.Name = 'Test';
        myAcc.RCID__c = 'TEST';
        myAcc.BillingState = 'BC';
        myAcc.Phone = '778-910-1029';
        insert myAcc;
        
        System.RunAs(u2) {
            myAcc.OwnerId = u2.Id;
            update myAcc;
        }
        
        AccountTeamMember myAccMem = new AccountTeamMember();
        
        myAccMem.AccountId = myAcc.Id;
        myAccMem.UserId = UserInfo.getUserId();
        myAccMem.TeamMemberRole = 'Manager';
        
        insert myAccMem;
        
        AccountTeamWrapper myAC = new AccountTeamWrapper(myAccMem);
        
        myAC.Checked = false;        
        bGet = myAC.Checked;
                        
        ApprovalRequests myApp = new ApprovalRequests();

        myApp.RelatedTo = 'Test';
        myApp.Status = 'Test';
        myApp.ApprovalType = 'Test';
        myApp.CreatedDate = 'Test';
        
        IdGet = myApp.RowId;
        sGet = myApp.RelatedTo;
        sGet = myApp.Status;
        sGet = myApp.ApprovalType;
        sGet = myApp.CreatedDate;   
        
        Opportunity myOpp = new Opportunity();
        
        myOpp.AccountId = myAcc.Id;
        myOpp.CloseDate = Date.Today();
        myOpp.StageName = 'Prospect';
        myOpp.Name = 'Test';
        myOpp.OwnerId = u1.Id;
        insert myOpp;    
        
        Product2 myProd = new Product2();
        
        myProd.Name = 'Test';
        myProd.IsActive = true; // Changed from false
        insert myProd;
        
        Opp_Product_Item__c myItem = new Opp_Product_Item__c();
        
        myItem.Opportunity__c = myOpp.Id;
        myItem.Product__c = myProd.Id;
        insert myItem;
        
        OpportunityTeamMember myOM = new OpportunityTeamMember();
        
        myOM.OpportunityId = myOpp.Id;
        myOM.UserId = UserInfo.getUserId();
        myOM.TeamMemberRole = 'Manager';
        insert myOM;        
        //System.RunAs(u1) {
            
            myItem.Building_Name__c = 'Test';
            update myItem;
        
            myOpp.StageName = 'Qualified';
            /*
			* Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
			* START
			*/
				myOpp.Stage_update_time__c=System.Now();
			/*
			* Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
			* END
			*/
            update myOpp;
        //}
                     
        OpportunityTeamWrapper myOC = new OpportunityTeamWrapper(myOM);
        
        myOC.Checked = false;        
        bGet = myOC.Checked;
        
    }
}