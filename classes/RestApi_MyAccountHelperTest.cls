@IsTest
public with sharing class RestApi_MyAccountHelperTest {

    private static final String SUBJECT = 'This is the subject for this case';
    private static final String DESCRIPTION = 'Description for this case';
    private static final String UUID = '909090';
    private static final String UUIDNOT = '1010101';
    private static final String CASE_ORIGIN = 'My Account';
    private static final String EMAIL = 'puser000@amamama.com';
    private static final String BILLINGACCOUNTNUMBER = '1337';
    private static final String REQUESTTYPE = 'Account update';
    private static final List<String> COLLABORATORS = new list<String> { 'test1@telus.com' , 'test2@telus.com' , 'test3@telus.com'};
    private static final String CHECKCOLLABORATORS = 'test1@telus.com;test2@telus.com;test3@telus.com';
    private static final Boolean STOPNOTIFCATIONS = true;
    private static final Boolean ALLOWNOTIFICATIONS = false;
    private static final Boolean CHECKALLOWNOTIFICATIONS = true;
    private static final String BADUUID = '909091';
    private static final String ATTACHMENTNAME = 'test.jpg';
    private static final Blob ATTACHMENTBODY = Blob.valueOf('iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABmJLR0QA/wD/AP+gvaeTAAAMvElEQVRoge2ZfZBU1ZXAf/d99Mf0xzAzMAwwfAjoCIwgOAYwRrCAIimT0sgWJSDGmFhY2V00m2RrQTeWVYFUuVuJbrK7sfygEEJMKK1FZsu4JBGIxijJEESQDBJhZoCZYYbpoT9fv3737h/d/abfdM+ARvePLU/Vqdvz5r1zz++ec+897z74VD6VT0TEx2lsO9TpcIuAhQbME9AgIaQAHRLAuRy8A7xpwMHVMPhx9f1Xg/wUahTcp8H9wPSIrmfqqqpCYb9fqzIM0DRQCpnLkc5miWezzkXLSsWlDOrwXg6e9sG21XnQ/3uQAsA/CthY5/PJKQ0N4XG1tejBICKRgFQKZVkIKUGpvBoGyjDAMLBTKS4kEnQkEokBKZWCf/XBDz4q0EcC2Qlf0eDf6oNBc+aMGcFwXR309UF3NyoWQ3McRIlxAajCbwUoTYNgEBUKoXw+BhMJTsXjyYuOY0m4fx289ImCPAuRKthuatrKuTNmVI2ZPBnV0YE8cwbNttHAVVGiRVElKoutrqPCYVQgwEA8ztFUKuXA7gw88FXIfOwg26AhAK/VVlVd1XzDDX7dsnCOHkVLpz0AGiAiEbS5cxFTp4Lfn+/IslCnT6PeeQeVSCALMK4aBkQiWFJyLB7PxKU8rsGK1XDxYwPZBeMF/HHimDH11yxaZMr330eeOoWmFBqgA1okgr5mDfpttyFuvhlRABguKpOB119HtrYiX3gBZzhUKIQ0DP4cj2cvSNmp4MZ1MPBXg+yEqA5/nFRbO23m4sWGc+QIsqtrCCAQwHzoIYyNGxHhMEIIj3oglPLqwADOD3+I85OfIDMZnAKM8vuRfj/tiUT2gpQnLFh4uTS7LMgu2DM2HF4x59Zbg7k//QnZ2ZkHAPRp0/Dv2oU+Z47ruKZpHohiq5TywEgph34fPYq9Zg1OZycScADl8+GYJu+mUqmUUj+/C+4bzU9ttH/uhPt8mra8afHioN3eTq6zc+jBxYvx/+pXaLNnl0WhElTp7zJtbsb49a/RFi507atsFmyba3y+KgF37YA7RvN1xIg8D/UGnJrb3ByO1tWROXDAjYTZ0kLV3r2IQMB1cHhrDwww0NaG1duLzGQwolEis2YRnT3bExFPm8lg3XYbdlvbUGQCAQZsm5OOM2DAVSNVA8ZIIDr8U104bFQ3NZF+9dWhSEyciP/558HvL0sfIQTnWlt5/8c/pu+NN1C5XJnd4MSJTF2/nukbNuCrr0cIgVIq3/r9GDt2kFu+HM6fdyMTNU3CjuO/BH8HbLniiGyHOhM6FyxcGDSzWezDh92lNfzCC5grVpRFIdvby9vr19P/u9+NNDYeMceM4fonnmDiqlVl0bFfeYX03Xd7luakUrznOHEHJtwDyeH2Ks4RA+6uDgZlcOpUrBMn3E3MWLYMfdmysvsT7e3sX7LkiiEA7FiMQ/fey/tPPln2P33lSvSlS4c2UMchoGlUAQJWVbJXEUSDB8ZPnx6yz55FptP58kIIfJs3A0MrEEC2r483V60i3dV1xRClcuyRR+jYudP9u2jb2LwZhMiDFGq1WiEiBmy4IpDnoV7CzDFTppA9c8atkbS5c9Gbmz33KqV458EHSZ0+XWbYjEaZ/rWv8ZlnnmHR9u3MefhhwjNnVoR591vfIn7ypOeaPm8eoqUl3w8gpSQKSLjxKagabqNssgtYWmWaab2mJmJ3d6MVDBkrVrjOFydo7NAhzre2ljk2fvlyPrNtG4H6es/1ax9+mL88/TRHvv1tpG27151MhvYtW7j+2WfdPgCMz38e+9ChfFSkRNM0fEplQnAz8D+ltssiosGC6rq6kHPpEiqXc/PUXL7cs6kBnCl0XCrjli5l8Ysv4hs7tmwnRwiuuv9+Wp57ruy57r17sXp7PX3oN93kKTQVUAV+AfOHP18WER2uD0SjWm5w0H0YXUe79lq3AyEE0rbp3bfPOwimyfU/+hHCMJBSVixRACZ++ctc9/jjpM6d8/zP6u3FLAwAgNbUhNI0lJTuXPGBD5h7WRAHJprBIE4qhaTwLlFbCz6fZ7TSHR3YA95aru6WWwhOnepCFKFLIYrRmbZhQ1nESssWpRQEAlBTg+rvdwdVz/vUONzvstQSENJM000rCRAKDRkvOJMpGc2iRK+7rswpKWXZPnE5LQVWoZAntQoOV182IkBZXspstmx+lC7BQ6MgXAdGi4jH0cuotKyyFzKGXjhHBhEQz9k2fsMYenhgACUlQtddR3zDViSAZHt7WVqVAn8YGMhvhE4sVvYSRoV6qwxEwjk7nZ4nGhqKD0E6Te7cOczGodQMNDaih0I4yaFq4cJvfkOmpwd/SQ01XIrXTj3xBINHjniuT7rnHmo++1kXxunqKotIYdHuHG630s5+ODU46GjRqCec2QMHvCNmmtQuWeJ50EmnOfad76BK5kWlydz/xhuc3LKFnpdfdrW3tZXg1Kme+7O//73bvwSkEGQh4+TPxkYHkdAW7+9PadEoyudzDwmsffvKHJtw111lo9DT2sq73/wm0rLciV464Xv37aNt7VqUlJ7n6m69Ff+kSV6Q115zIVShXMmALaBteL9l1e8vYJwNZ69budK0jh8n29mZfxsMBBj71lsYNTWel6LDd95J7K23yoCCU6Ywaf16ovPmoRwHq7ubnj176N+/v+xeoess2LuXyNy5LkTu4kUuLlqEk8nko6Jp5JSiQynbB9WrIT0qCMAuONrY1NRcPX488YMHERRK+K9/ncgjj3hArLNnafvSl8heuFDJ1BXJ9E2bmPyNb3iiEX/sMVLbtrkZoQyDQcchptTBtbBkuI2K1a+C/+g7cyZlNDTkD9LIhze5YwfO2bOeDv2TJjHnqacwotGPBDFh3ToaH3jAYzN3+jTJnTs9c0MKQUKphIL/rGSnIogJu9KZjEicPk1g1izXoGNZxL77XZTjeCZwpKWF+Xv3EmpqumIAYZpM37SJq7dudcsPpRTKcYg/9hjStoeioetYUmJDLgv/VcmeXunibrDuhGprYGBB/Y03mlZXFzKbBSD3wQeodBr/5z7nWV6NMWOYsHYtvrFjSZ08Se7SpcoAhkH97bcz68knqV25EkogAOJbtpB66aUhCE1D6Tr9uVzShq1fgQMV7Y40Yk9BVRg+aJwzpz5aX09s/340pfLHoEJQ873vEVq7dsjQsPf35PHjXGprw+7rw8lkMCIRgjNnUnPTTeiRSMUKIbljB4OPPopUyl36RSBAwrbVgON0mHDNash+KBCAZ2BtUIinm5Yurcr19pI+dsxzphu64w5qvv99RCBQblhUNl1xk0yniW3aRHLPHs/mJ/x+HKDbstLdsOYf4JeAVcluxdQqiO9lOPVFmJ3p6ZkxbsECQ1oWucGh6sA+cQLrt7/Fd/XV6BMnjmJqZMm+/TYXN24kffBgGQS6To9lZWLws42wrchN/qToikB08nW//y/w+g253BezPT3hsS0thrRtcrGYe6PT00Ni927s48cxJ0xAb2iAEaLhipRYf/gDA48+Suzxx8n19nohAgGE309fKpVOKPXnzfBQqnDMhfcw35WRevQBfiAABJbCpHvhxTHV1WMnL1rkszo6SJ04gSikSennA2P8eKpWrsQ/fz5GY6N7mK0sC6erC6utjeSrr+L09uavl3ikhEALhfKTOx7PJqQ8+wNYdxT6yZ/9lqpnrowGEijV+dDw9/BcJBhsnLxggV8oRfLoUZx4vOyjzmjGVYXfCsA00SMRHMehPx63BqU8tRX+9nT+s0LReWvYb1dGSi2NfGWsF7Ub7DfhvxfkcldluromG4ahVzc3IwIBcokEsnCq6PkyNYqW7thGdTVaOEwymaQvkbC7lPrlZvjn3vxnOBvIFdQuUU+xNhJI8WWseNyrASIJ6hU4OBnORmKxluT58yJQXa2Hm5owotG8c7aNdJzRYQwDPRRCq6nBiEaxMhn6Y7HsJdsefA22/gv8PDuUPvYIrUdGm5U6+bTyV9LxUP0gfLURVpmGQfW4cf5wXR1GOJyfD6lUvgLOZvNzSdMQhoHQdRCCXDJJKpEgnkxaOaXkSdj97/Czi/koZMmnznAtplTZofLlvo9oJTC+Cq05DWr+BpZdA7cHYQZCWMFg0AwEg6ZumnkIpXCkxMlmyVpWNm1ZOQG+OLS/B62/gAPdcKlktItqVYAqW3qvBKQoRceLapa0RTWmQOQLcMMEuLoWmvwwTiukrwQnAz0Xob0LTu6Fw72Qwpv/ObzpM1wrHBR8OJDivRUBSrS4OJR+Gy3tp3SeOyWaY3SgEQE+CkipaKNAFEGKrzHFfkoXNLegLrS5YUBZhq1KnxRIJTvDQYoww6USSLH9VP7fyP8CNGNTUEDntqIAAAAASUVORK5CYII=');
    private static final String DEFAULTRTNAME = 'SMB Care Billing';
    private static final Id DEFAULTRTID = Schema.SObjectType.Case.getRecordTypeInfosByName().get(DEFAULTRTNAME).getRecordTypeId();
    private static final String ORIGIN = 'My Account';

    private static final String CONTACTFIRST = 'Phillip';
    private static final String CONTACTLAST = 'Smith';
    private static final String CONTACTFULL = CONTACTFIRST + ' ' + CONTACTLAST;
    private static final String USERNAME = 'psmith@telus.com' + System.currentTimeMillis();
    private static final String EXTERNALSTATUS = 'EX In Progress';
    private static final String INTERNALSTATUS = 'IN In Progress';

    private static User u;
    private static Account a;
    private static Account parentAcc;
    private static Case c;
    private static ExternalToInternal__c ei;
    private static Segment__c segment; 
    private static RestApi_MyAccountHelper_GetCases getCaseObj;
    private static RestApi_MyAccountHelper_GetCases.RequestUser ru;
    private static List<String> filters = new List<String>{EXTERNALSTATUS};
    private static RestApi_MyAccountHelper_GetCases.GetCasesREsponse resp;
    private static RestApi_MyAccountHelper.Response intresp;


    static {
		trac_TriggerHandlerBase.blockTrigger = true;
    	User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    	System.RunAs(thisUser){
            List<ExternalToInternal__c> ei = new List<ExternalToInternal__c>();
    		ei.add(RestApi_MyAccountHelper_TestUtils.createEtoI());
            ei.add(RestApi_MyAccountHelper_TestUtils.createEtoIRT());
            ei.add(RestApi_MyAccountHelper_TestUtils.createEtoICancelled());
    		insert ei;
   		}

        RestApi_MyAccountHelper_TestUtils.createMBRCustomSetting();

		RestApi_MyAccountHelper_TestUtils.MASRUserBundle bundle = RestApi_MyAccountHelper_TestUtils.createMASREnabledUser();

        c = RestApi_MyAccountHelper_TestUtils.createCase(bundle.banAccount, bundle.u);
        insert c;
		trac_TriggerHandlerBase.blockTrigger = false;
    }



	@IsTest
	private static void testRequest(){
    	Test.startTest();
    	RestApi_MyAccountHelper.Response trp = RestApi_MyAccountHelper.processPostRequest(createTestRequest());
    	Test.stopTest();
        
        System.assert(trp.error == null);
	}

	@IsTest
	private static void testGetCasesRequest(){
    	Test.startTest();
    	RestApi_MyAccountHelper.Response trp = RestApi_MyAccountHelper.processPostRequest(createGetCasesRequest());
    	System.debug('Test Traction: ' + trp);
        test.stopTest();
        
        System.assert(trp.error == null);
	}

    /*@IsTest
    private static void testGetCasesRequestNewUUID(){
        Test.startTest();
        RestApi_MyAccountHelper.Response trp = RestApi_MyAccountHelper.processPostRequest(createGetCasesRequestNewUUID());
        System.debug('Test Traction: ' + trp);
        test.stopTest();
        List<User> u = [SELECT Id, FederationIdentifier FROM User WHERE FederationIdentifier = :RestApi_MyAccountHelper_TestUtils.BADUUID];
        *//*System.assert(u[0].FederationIdentifier == RestApi_MyAccountHelper_TestUtils.BADUUID);
        System.assert(trp.error == null);*//*
    }*/

	@IsTest
	private static void testGetCaseDetailsRequest(){
    	Test.startTest();
    	RestApi_MyAccountHelper.Response trp = RestApi_MyAccountHelper.processPostRequest(createGetCaseDetailsRequest());
    	System.debug('Case Details Traction: ' + trp);
        Test.stopTest();

        System.assert(trp.error == null);
	}

	@IsTest
	private static void testUpdateCaseRequest(){
    	Test.startTest();
        RestApi_MyAccountHelper.Response trp = RestApi_MyAccountHelper.processPostRequest(createUpdateCaseRequest());
    	System.debug(' Update Traction: ' + trp);
        Test.stopTest();
        
        System.assert(trp.error == null);
	}

	@IsTest
	private static void testCreateAttachmentRequest(){
    	Test.startTest();
    	RestApi_MyAccountHelper.Response trp = RestApi_MyAccountHelper.processPostRequest(createCreateAttachmentRequest());
    	System.debug('Create Attach Traction: ' + trp);
        Test.stopTest();
        
        System.assert(trp.error == null);
	}

	@IsTest
	private static void testCreateCaseCommentRequest(){
	   Test.startTest();
	   RestApi_MyAccountHelper.Response trp = RestApi_MyAccountHelper.processPostRequest(createCreateCaseCommentRequest());
       Test.stopTest();

       System.assert(trp.error == null);

	}

	@IsTest
	private static void testCreateCaseRequest(){
    	Test.startTest();
    	RestApi_MyAccountHelper.Response trp = RestApi_MyAccountHelper.processPostRequest(createCreateCaseRequest());
    	System.debug('Create Case Traction: ' + trp);
        Test.stopTest();
        
        System.assert(trp.error == null);
	}

    @IsTest
    private static void testNotValidRequest(){
        Test.startTest();
        RestApi_MyAccountHelper.Response trp = RestApi_MyAccountHelper.processPostRequest(createFailingRequest());
        System.debug('Create Case Traction: ' + trp);
        Test.stopTest();

        System.assert(trp.error != null);
    }
	
    // Working
    private static String createGetCasesRequest() {
    	return '{"type": "' + RestApi_MyAccountHelper_TestUtils.REQUEST_TYPE_GET_CASES + '", "users": [],"user": {"uuid": "'+ RestApi_MyAccountHelper_TestUtils.UUID + '","lastName": "'+ RestApi_MyAccountHelper_TestUtils.CONTACTLAST + '","firstName": "'+ RestApi_MyAccountHelper_TestUtils.CONTACTFIRST + '","email": "'+ RestApi_MyAccountHelper_TestUtils.EMAIL + '"},"filters": ["' + RestApi_MyAccountHelper_TestUtils.EXTERNALSTATUS + '"],"billing": [' + RestApi_MyAccountHelper_TestUtils.BILLINGACCOUNTNUMBER + '], "accounts": [{"accountNumber":"' + RestApi_MyAccountHelper_TestUtils.BILLINGACCOUNTNUMBER + '", "accountType":"C", "accountSubType":"E"}]}';
    }

    // Working
    private static String createGetCasesRequestNewUUID() {
        return '{"type": "' + RestApi_MyAccountHelper_TestUtils.REQUEST_TYPE_GET_CASES + '", "users": [],"user": {"uuid": "'+ RestApi_MyAccountHelper_TestUtils.BADUUID + '","lastName": "'+ RestApi_MyAccountHelper_TestUtils.CONTACTLAST + '","firstName": "'+ RestApi_MyAccountHelper_TestUtils.CONTACTFIRST + '","email": "'+ RestApi_MyAccountHelper_TestUtils.EMAIL + '"},"filters": ["' + RestApi_MyAccountHelper_TestUtils.EXTERNALSTATUS + '"],"billing": [' + RestApi_MyAccountHelper_TestUtils.BILLINGACCOUNTNUMBER + '], "accounts": [{"accountNumber":"' + RestApi_MyAccountHelper_TestUtils.BILLINGACCOUNTNUMBER + '", "accountType":"C", "accountSubType":"E"}]}';
    }

    // Working
    private static String createUpdateCaseRequest(){
    	return '{"type" : "'+ RestApi_MyAccountHelper_TestUtils.REQUEST_TYPE_UPDATE_CASE + '","uuid":"' + RestApi_MyAccountHelper_TestUtils.UUID + '","billing":[],"caseToUpdate":{"id":"'+ c.Id +'","cancelCase":true}}';
    }

    // Working
    private static String createCreateAttachmentRequest(){
    	return '{"type" : "'+ RestApi_MyAccountHelper_TestUtils.REQUEST_TYPE_CREATE_ATTACHMENT + '", "user" : {"uuid":"' + RestApi_MyAccountHelper_TestUtils.UUID + '"}, "attachmentToCreate": {"name" : "Title.txt","body" : "dGVzdGluZw==","caseId" : "' + c.Id + '"}}';
    }

    // Working
    private static String createGetCaseDetailsRequest(){
    	return '{"type" : "'+ RestApi_MyAccountHelper_TestUtils.REQUEST_TYPE_GET_CASE_DETAILS + '","caseId":"'+c.Id+'",	"uuid":"' + RestApi_MyAccountHelper_TestUtils.UUID + '","billing":['+RestApi_MyAccountHelper_TestUtils.BILLINGACCOUNTNUMBER+']}';
    }

    // Working
    private static String createCreateCaseCommentRequest(){
    	return '{"type" : "'+ RestApi_MyAccountHelper_TestUtils.REQUEST_TYPE_CREATE_COMMENT + '","user" : { "uuid":"' + RestApi_MyAccountHelper_TestUtils.UUID + '"},"comment": {"body" : "This is an example body",	"parentId" : "'+ c.Id + '"}}';
    }

    // No Rows
    private static String createCreateCaseRequest(){
    	return '{"type" : "'+ RestApi_MyAccountHelper_TestUtils.REQUEST_TYPE_CREATE_CASE + '","user" : { "uuid":"' + RestApi_MyAccountHelper_TestUtils.UUID + '"},"caseToCreate": { "subject" : "Test Subject - Ryan","sendNotification" : false,"requestType": "Account Update","description" : "Test Description","mobilityRequest": false,"collaborators": ["test@test.com"],"billingAccountNumber": "' + RestApi_MyAccountHelper_TestUtils.BILLINGACCOUNTNUMBER + '","attachment" : { "Name" : "test.txt","Body" : "exampel test"}}}';
    }

    // Working
    private static String createTestRequest(){
    	return '{"type" : "'+ RestApi_MyAccountHelper_TestUtils.REQUEST_TYPE_TEST +'"}';
    }

    // Working
    private static String createFailingRequest(){
        return '{"type" : "not-valid"}';
    }
}