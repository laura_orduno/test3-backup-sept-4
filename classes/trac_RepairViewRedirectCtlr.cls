/*
 * Controller for the RepairViewRedirect page
 * Part of the SMB Repairs project
 *
 * @author: Grant Adamson, Traction On Demand 
 */

public with sharing class trac_RepairViewRedirectCtlr extends trac_RepairCaseCtlr{
	
	
	public trac_RepairViewRedirectCtlr() {
		super();
	}
	
	
	public PageReference redirect() {
		String url = buildUrl();
		return new PageReference(url);
	}
	
	
	private String buildUrl() {
		return Label.VIEW_IN_SRD_LINK + '&repairId=' + theCase.repair__c + '&languageCode=' + getLangCode();
	}
	
	
	public static String getLangCode() {
		String temp = UserInfo.getLanguage();
		
		// en_CA -> EN
		return temp.substring(0,2).toUpperCase();
	}
	
}