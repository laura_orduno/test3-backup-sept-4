@isTest
private class OCOM_TelusSignorsTest{

    static testmethod void OCOM_TelUsSignorsMethodsTest(){
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User newUser = new User(); 
        newUser.Alias = 'testcat9';
        newUser.Email='standarduser@testorg.com32wfsdfsdfsdf';
        newUser.EmailEncodingKey='UTF-8';
        newUser.LastName='Testing';
        newUser.LanguageLocaleKey='en_US';
        newUser.LocaleSidKey='en_US';
        newUser.ProfileId = p.Id; 
        newUser.TimeZoneSidKey='America/Los_Angeles';
        newUser.UserName= uniqueName + '@test' + orgId + '.org';
        newUser.Release_Code__c = '05';
        insert newUser;
        
        
        Approval_Threshold__c threshold = new Approval_Threshold__c();
        threshold.Transaction_Type__c ='Contract';
        threshold.Business_unit__c= 'BU';
        threshold.Release_Code_0_Threshold__c =0.00;
        threshold.Release_Code_1_Threshold__c =400.00;
        threshold.Release_Code_2_Threshold__c =0.00;
        threshold.Release_Code_3_Threshold__c =5000.00;
        threshold.Release_Code_4_Threshold__c =5000.00;
        threshold.Release_Code_5_Threshold__c =10000.00;
        threshold.Release_Code_6_Threshold__c =100000.00;
        threshold.Release_Code_7_Threshold__c =1000000.00;
        threshold.Release_Code_8_Threshold__c =3000000.00;
        threshold.Release_Code_9_Threshold__c  =100000000.00;
        threshold.Release_Code_10_Threshold__c =1000000000.00;
        threshold.RecordTypeId = getRecordTypeId('Approval_Threshold__c','Release Code Threshold');
        insert threshold;
        
        Map<String,Object> outMap = new Map<String,Object>();
        Map<String,Object> options = new Map<String,Object>();
        Map<String,Object> inputMap = new Map<String,Object>();
        map<string,object> val = new map<string,object>();
        Test.startTest();
        val.put('totalDealValue',10.20);
        val.put('searchString','hello');
        inputMap.put('TelusSignor',val);
        OCOM_TelusSignors ot = new OCOM_TelusSignors();
        ot.invokeMethod('getAuthorizedTELUSApprovers', inputMap, outMap,options);
        OCOM_TelusSignors otelse = new OCOM_TelusSignors();
        otelse.invokeMethod('Elsecheck', inputMap, outMap,options);
        Test.stopTest();
    }

    public static id getRecordTypeId(String sobjectName , String recordTypeName ){
        return Schema.describeSObjects(new List<String>{sobjectName})[0].
            getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
    }

}