public class ENTPSubmitFeedbackCtlr {
	public class NewCaseException extends Exception {
	}

	private static final Map<String, String> requestTypeRecordTypeMap;
	private static final Map<String, String> recordTypeRequestTypeMap;
	private static final Map<String, Id> caseRTNameToId;
	private static final Map<String, List<String>> typeFieldsOrder;
	private static final Map<String, List<ParentToChild__c>> ptcListByCategoryMap;

	private static final Map<String, List<String>> categoryRequestTypesMap;
	private static final Map<String, String> requestTypesCategoryMap;

	public static Map<String, String> reqTypeFormIdMap { get; set; }
	public String reqTypeFormIdMapJSON { get {return JSON.serialize(reqTypeFormIdMap);} }
	public String userLanguage {
		get {
			return PortalUtils.getUserLanguage();
		}
		set;
	}

	public Boolean createNewOnSubmit {
		get {
			if (createNewOnSubmit == null) return false;
			return createNewOnSubmit;
		}
		set;
	}

	static {
		List<TypeToFields__c> ttfs = TypeToFields__c.getAll().values();
		typeFieldsOrder = new Map<String, List<String>>();

		List<ExternalToInternal__c> etis = ExternalToInternal__c.getAll().values();
		requestTypeRecordTypeMap = new Map<String, String>();
		recordTypeRequestTypeMap = new Map<String, String>();
		for (ExternalToInternal__c eti : etis) {
			if (eti.Identifier__c != null && eti.Identifier__c.equals('onlineRequestTypeToCaseRT')) {
				requestTypeRecordTypeMap.put(eti.External__c, eti.Internal__c);
				recordTypeRequestTypeMap.put(eti.Internal__c, eti.External__c);
			}
		}

		List<ParentToChild__c> ptcs = ParentToChild__c.getAll().values();
		ptcListByCategoryMap = new Map<String, List<ParentToChild__c>>();
		categoryRequestTypesMap = new Map<String, List<String>>();
		requestTypesCategoryMap = new Map<String, String>();
		reqTypeFormIdMap = new Map<String, String>();
		reqTypeFormIdMap.put(label.mbrSelectType, '');
		for (ParentToChild__c ptc : ptcs) {
			if (ptc.Identifier__c.equals('CCICatRequest')) {
				// build ptc list map so it can be used for sorting the request type(child) entries
				if (!ptcListByCategoryMap.containsKey(ptc.parent__c)) {
					ptcListByCategoryMap.put(ptc.parent__c, new List<ParentToChild__c>());
				}
				List<ParentToChild__c> ptcListForCategory = ptcListByCategoryMap.get(ptc.parent__c);
				ptcListForCategory.add(ptc);
				ptcListByCategoryMap.put(ptc.parent__c, ptcListForCategory);
				////////////////////////////////////////////////////////////////////////////////////
				requestTypesCategoryMap.put(ptc.Child__c, ptc.Parent__c);
				if (ptc.FormComponent__c == null || ptc.FormComponent__c.equals('')) {
					reqTypeFormIdMap.put(ptc.Child__c, 'descriptionField');
				} else {
					reqTypeFormIdMap.put(ptc.Child__c, ptc.FormComponent__c);
				}
			}
		}

		// perform ptc list sorting for each category
		Map<Integer, ParentToChild__c> ptcOrderedMap = new Map<Integer, ParentToChild__c>();
		List<ParentToChild__c> ptcOrphanedList = new List<ParentToChild__c>();

		for (String category : ptcListByCategoryMap.keyset()) {
			Integer minOrderNumber = null;
			Integer maxOrderNumber = null;

			ptcOrderedMap.clear();
			ptcOrphanedList.clear();
			List<ParentToChild__c> ptcSortedList = new List<ParentToChild__c>();

			for (ParentToChild__c ptc : ptcListByCategoryMap.get(category)) {
				Integer orderNumber = Integer.valueOf(ptc.order_number__c);
				// init
				minOrderNumber = minOrderNumber == null ? orderNumber : minOrderNumber;
				maxOrderNumber = maxOrderNumber == null ? orderNumber : maxOrderNumber;
				if (orderNumber == null || ptcOrderedMap.containsKey(orderNumber)) {
					ptcOrphanedList.add(ptc);
				} else {
					// update
					minOrderNumber = orderNumber < minOrderNumber ? orderNumber : minOrderNumber;
					maxOrderNumber = orderNumber > maxOrderNumber ? orderNumber : maxOrderNumber;
					ptcOrderedMap.put(orderNumber, ptc);
				}
			}

			for (Integer key = minOrderNumber; key <= maxOrderNumber; key++) {
				if (ptcOrderedMap.containsKey(key)) {
					ParentToChild__c ptc = ptcOrderedMap.get(key);
					ptcSortedList.add(ptc);
				}
			}

			ptcSortedList.addAll(ptcOrphanedList);

			List<String> ptcSortedStringList = new List<String>();
			for (ParentToChild__c ptc : ptcSortedList) {
				ptcSortedStringList.add(ptc.child__c);
			}
			categoryRequestTypesMap.put(category, ptcSortedStringList);
		}
		/////////////////////////////////////////////////////////////////////////////////////

		for (String s : categoryRequestTypesMap.keySet()) {
			system.debug('categoryRequestTypesMap: ' + s + ' values: ' + categoryRequestTypesMap.get(s));
		}

		Schema.DescribeSObjectResult caseToken = Schema.SObjectType.Case;
		Map<String, RecordTypeInfo> nameToRTInfo = caseToken.getRecordTypeInfosByName();
		caseRTNameToId = new Map<String, Id>();
		for (String s : nameToRTInfo.keyset()) {
			caseRTNameToId.put(s, nameToRTInfo.get(s).getRecordTypeId());
		}
	}

	/* Input Variables */
	public String selectedCategory { get; set; } {
		selectedCategory = Label.MBRSelectCategory;
	}
	Public String selectedFeedbackType { get; set; }
	public Case caseToInsert { get; set; }
	public static String parentCaseType;
	public static String parentCategory;
	public ENTPFormWrapper formWrapper { get; set; }
	public Account account { get; private set; }
	public Boolean caseSubmitted { get; set; }
	public String requestType {
		get;
		set {
			requestType = value;
			if (requestTypeRecordTypeMap.get(requestType) != null && caseRTNameToId.get(requestTypeRecordTypeMap.get(requestType)) != null) {
				caseToInsert.recordtypeid = caseRTNameToId.get(requestTypeRecordTypeMap.get(requestType));
			}
		}
	} {
		requestType = 'Feedback';
	}

	/* public Attachment attachment {
		 get {
			 if (attachment == null)
				 attachment = new Attachment();
			 return attachment;
		 }
		 set;
	 }*/

	public ENTPSubmitFeedbackCtlr() {
		priorityError = '';
		typeError = '';
		formWrapper = new ENTPFormWrapper();
		userLanguage = PortalUtils.getUserLanguage();

		// Customer_Interface_Settings__c cis = Customer_Interface_Settings__c.getInstance();
		ENTP_Customer_Interface_Settings__c cis = ENTP_Customer_Interface_Settings__c.getInstance();
		if (cis.Case_Origin_Value__c != null) {
			caseToInsert = new case(Origin = cis.Case_Origin_Value__c, NotifyCustomer__c = true);
		} else {
			caseToInsert = new case(Origin = 'ENTP', NotifyCustomer__c = False);
		}
		caseSubmitted = false;

/*****        
        if (ApexPages.currentPage() != null) {
            String parentCaseNumber = ApexPages.currentPage().getParameters().get('reopen');
            if (parentCaseNumber != null) {
                List<Case> parentCaseList = [SELECT Id, Subject, Description, My_Business_Requests_Type__c, LastModifiedDate, NotifyCollaboratorString__c, CreatedDate, CaseNumber, recordType.name, Type, Status FROM Case WHERE CaseNumber = :parentCaseNumber AND Status = 'Closed'];
                if(parentCaseList.size() > 0) {
                    caseToInsert.Parent = parentCaseList[0];
                    caseToInsert.ParentId = parentCaseList[0].Id;
                    caseToInsert.Subject = Label.MBRSubmitRequestReopenSubject + ' ' + caseToInsert.Parent.Subject;
                    List<String> lines = new List<String>();
                    if (caseToInsert.Parent.Description != null) {
                        lines = caseToInsert.Parent.Description.split('\n');
                    }
                    String description = '';
                    for (String s : lines) {
                        //description += '\n> ' + s;
                    }
                    caseToInsert.Description = '\n> ' + Label.MBRSubmitRequestReopenDescription + '\n>' + description;
                    caseToInsert.NotifyCollaboratorString__c = caseToInsert.Parent.NotifyCollaboratorString__c;
                    parentCaseType = caseToInsert.Parent.My_Business_Requests_Type__c;
                    System.debug('requestTypesCategoryMap: ' + requestTypesCategoryMap);
                    if (parentCaseType != null && requestTypesCategoryMap.get(parentCaseType) != null) {
                        parentCategory = requestTypesCategoryMap.get(parentCaseType);
                        selectedCategory = parentCategory;
                        requestType = parentCaseType;
                    }
                } else {
                    // invalid parent caseNumber specified...
                }
            }
        }
*/
	}

	public transient String pageValidationError { get; set; }
	public transient String priorityError { get; set; }
	public transient String typeError { get; set; }

	public PageReference initCheck() {
		//String userAccountId;
		//userAccountId = (String)UserUtil.CurrentUser.AccountId;
		String userAccountId;
		List<User> users = [SELECT Id, AccountId FROM User WHERE Id = :UserInfo.getUserId()];
		if (users.size() > 0) {
			userAccountId = users[0].AccountId;
		}
		Id aid = (Id) userAccountId;
		if (caseToInsert.Parent == null) {
			resetValues();
		}
		caseToInsert.AccountId = aid;
		return null;
	}

	public void resetValues() {
		caseToInsert.description = '';
		caseToInsert.subject = 'Feedback';
		requestType = 'Feedback';//Label.MBRSelectType;
		String categoryType = ApexPages.currentPage().getParameters().get('category');
		if (caseSubmitted) {
			selectedCategory = Label.MBRSelectCategory;
		} else if (categoryType != null) {
			ParentToChild__c categType = ParentToChild__c.getAll().get(categoryType);
			selectedCategory = categType.Parent__c;
			categoryType = null;
		}
	}

	public String getRecordType(String externalName) {
		String typeName;

		if (userLanguage == 'fr') {
			typeName = 'Rétroaction';

		} else {
			typeName = 'Feedback';
		}

		return typeName;
	}

	public PageReference createNewCase() {
		PageReference submitNewPage = Page.ENTPFeedback;
		submitNewPage.setRedirect(true);
		submitNewPage.getParameters().put('previousCaseSubmitted', '1');

		if (caseToInsert == null) return null;

		String recType = getRecordType(requestType);

		Map<String, Schema.RecordTypeInfo> recordTypes = Schema.SObjectType.Case.getRecordTypeInfosByName();
		for (Schema.RecordTypeInfo rt : recordTypes.values()) {
			System.debug('Schema.SObjectType.Case.RecordTypeInfosByName: ' + rt);
		}
		System.debug(recType);
		Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(recType).getRecordTypeId();
		System.debug('recTypeId: ' + recTypeId);
		if (recTypeId != null) {
			caseToInsert.recordtypeid = recTypeId;
		}

		if (caseToInsert.recordtypeid == null) {
			pageValidationError = label.mbrMissingRecordType;
			return null;
		}
		ENTPFormWrapper.FeedbackInfoAct FeedbackInfo = formWrapper.FeedbackInfo;
		//caseToInsert.My_Business_Requests_Type__c = FeedbackInfo.FeedbackType;
		caseToInsert.My_Business_Requests_Type__c = 'Feedback';
		//append request type in front of subject

		// Added for running Case Assignment Rules -- Dan
		Database.DMLOptions dml = new Database.DMLOptions();
		dml.AssignmentRuleHeader.useDefaultRule = true;
		caseToInsert.description = '';
		caseToInsert.description += '\n' + populateDescription(requestType);

		Database.Saveresult sr = Database.insert(caseToInsert, dml);
		System.debug('createNewCase: ' + sr.isSuccess());

		if (!sr.isSuccess()) {
			for (Database.Error error : sr.getErrors()) {
				pageValidationError = error.getMessage();
			}
			System.debug('createNewCase, pageValidationError: ' + pageValidationError);
		}

		if (caseToInsert.Id == null) {
			return null;
		} else {

			PageReference page = System.Page.ENTPThankYou;
			Case c = [Select casenumber from Case where id = :caseToInsert.id LIMIT 1];
			page.getParameters().put('caseNumber', c.casenumber);
			page.getParameters().put('TicketType', 'New');
			page.setRedirect(true);
			caseSubmitted = true;
			caseToInsert = new case();
			selectedCategory = label.mbrSelectCategory;

			return createNewOnSubmit ? submitNewPage : page;
		}
	}

	public String populateDescription(String reqType) {

		Integer index = 1;
		String description = '';
		string subjectLeft = '';
		String formId = reqTypeFormIdMap.get(reqType);
		ENTPFormWrapper.FeedbackInfoAct FeedbackInfo = formWrapper.FeedbackInfo;

		if (true) {
			if (String.isNotBlank(FeedbackInfo.FeedbackType)) {
				description += '\n Feedback Type: ' + FeedbackInfo.FeedbackType;
			}
			if (caseToInsert.subject == null || caseToInsert.subject.equals('')) {
				//caseToInsert.Subject = FeedbackInfo.FeedbackType;
				////RB - Jan 19 - 2016 Added subject first 20 characters of Description
				subjectLeft = FeedbackInfo.additionalNote.left(30);
				caseToInsert.Subject = subjectLeft;
			}
//         if(formId!=null && formId.equals('GetTicketInfoForm')){

			if (String.isNotBlank(FeedbackInfo.additionalNote)) {
				description += '\n Feedback Comments: ' + FeedbackInfo.additionalNote;
			}

			index++;
		}

		return description;
	}

	public void clear() {
		caseToInsert = null;
	}

	public List<SelectOption> getRequestTypes() {
		Map<String, List<String>> rts = categoryRequestTypesMap;
		List<SelectOption> options = new List<SelectOption>();
		List<String> temps = categoryRequestTypesMap.get(selectedCategory);
		if (temps == null) {
			options.add(new SelectOption(label.mbrSelectCategoryFirst, label.mbrSelectCategoryFirst));
			return options;
		}
//        temps.sort();
		Set<String> tempsSet = new Set<String>(temps);
		if (!tempsSet.contains(requestType)) {
			requestType = label.mbrSelectType;
		}
		// -- Added by DR, Traction 09-10-2014 --/
		options.add(new SelectOption(label.mbrSelectType, label.mbrSelectType));
		// -- End Added by DR, Traction 09-10-2014 --/
		for (String temp : temps) {
			options.add(new SelectOption(temp, temp));
		}
		return options;
	}

	public List<SelectOption> getCategories() {
		Map<String, List<String>> rts = categoryRequestTypesMap;
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption(label.mbrSelectCategory, label.mbrSelectCategory));
		for (String temp : rts.keySet()) {
			if (!temp.equals('Tech support') || !VITILcareUtils.restrictTechSupport()) {
				options.add(new SelectOption(temp, temp));
			}
		}
		return options;
	}

	public Case getParentCase() {
		return caseToInsert.Parent;
	}

	public String getParentCaseType() {
		return parentCaseType;
	}

	public String getParentCategory() {
		return parentCategory;
	}

	public Case getCaseToInsert() {
		return caseToInsert;
	}

	/**
	 *  Query for all My Business Requests cases that are status = 'new' and time
	 *  elapsed > 24 hours, then update priority to 'Urgent'
	 *
	 * @lastmodified
	 *   Alex Kong (TOD), 12/11/14
	 *
	 */
	public static void updateCasePriorityToUrgent() {

		List<Case> cases = new List<Case>();

		// find the Case Origin string
		String originValue = 'ENTP';

		System.debug('originValue: ' + originValue);

		// query for all relevant cases
		DateTime cutoffDateTime = DateTime.now().addHours(-24);
		System.debug('Rbrekke: cutoffDateTime: ' + cutoffDateTime);
		if (!Test.isRunningTest()) {

			cases = [
					SELECT Id, Subject, Status, Priority, CreatedDate
					FROM Case
					WHERE Origin = :originValue
					AND Status = 'New'
					AND Priority != 'Urgent'
					AND (CreatedDate <= :cutoffDateTime)
			];
			system.debug('Rbrekke: Select Statement Line 726:' + cutoffDateTime);
		} else {
			// remove the createdDate requirement for testing
			cases = [
					SELECT Id, Subject, Status, Priority, CreatedDate
					FROM Case
					WHERE Origin = :originValue
					AND Status = 'New'
					AND Priority != 'Urgent'
			];
		}

		System.debug('akong: cases: ' + cases);

		// loop through each case to determine if we should update priority
		Date sunday = Date.newInstance(1900, 1, 7); // 1900-01-07 is a sunday

		System.debug('akong: sunday: ' + sunday);

		for (Case c : cases) {
			System.debug('akong: c.Id: ' + c.Id);
			Integer dayOfWeek = Math.mod(sunday.daysBetween(c.CreatedDate.date()), 7); // dayOfWeek, 0 is sunday
			if (Test.isRunningTest()) {
				System.debug('Rbrekke: dayOfWeekBeforeIntegerCheck: ' + c.Subject);
				string RemStringDay = c.Subject;
				if (RemStringDay.isNumeric()) {
					dayOfWeek = Integer.valueOf(RemStringDay);
				} else {
					RemStringDay = c.Subject.remove('TEST');
					dayOfWeek = Integer.valueOf(RemStringDay);
				}
				System.debug('Rbrekke: dayOfWeekAfterIntegerCheck: ' + dayOfWeek);
			}
			System.debug('akong: dayOfWeek: ' + dayOfWeek);
			if (dayOfWeek >= 1 && dayOfWeek <= 4) {
				// monday to thursday, 24 hour check in soql query is sufficient
				c.Priority = 'Urgent';
			} else if (dayOfWeek == 5) {
				// friday, 72 hour check
				Integer hours = (Integer) (DateTime.now().getTime() - c.CreatedDate.getTime()) / 1000 / 60 / 60;
				if (hours >= 72 || Test.isRunningTest()) {
					c.Priority = 'Urgent';
				}
			} else if (dayOfWeek == 6) {
				// saturday, custom check for monday right before midnight (i.e. monday at 11:59:59 pm)
				DateTime mondayMidnight = DateTime.newInstance(c.CreatedDate.date().addDays(2), Time.newInstance(23, 59, 59, 999));
				if (mondayMidnight <= DateTime.now() || Test.isRunningTest()) {
					c.Priority = 'Urgent';
				}
			} else if (dayOfWeek == 0) {
				// sunday, custom check for monday right before midnight (i.e. monday at 11:59:59 pm)
				DateTime mondayMidnight = DateTime.newInstance(c.CreatedDate.date().addDays(1), Time.newInstance(23, 59, 59, 999));
				if (mondayMidnight <= DateTime.now() || Test.isRunningTest()) {
					c.Priority = 'Urgent';
				}
			}
		}

		// update
		update cases;
		System.debug('akong: final cases: ' + cases);

	}

}