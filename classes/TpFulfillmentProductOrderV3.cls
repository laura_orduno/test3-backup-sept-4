//Generated by FuseIT WSDL2Apex (http://www.fuseit.com/Solutions/SFDC-Explorer/Help-WSDL-Parser.aspx)
    
//Namespace=http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0
    
public class TpFulfillmentProductOrderV3 {
    /* 2015-12-12 [Roderick de Vera]
     * This schema class is generated from BPM-TP representation of the SID model (TM Forum)
     * This schema class was refactored due to the following reasons
     * ---- limitation of WSDL2Apex-> does not support deep level schema extensions thus, the resulting class are missing elements
     * ---- disabling subclasses which are not needed by the following Web Services Client
     * -------- OrdrWsPerformCustomerOrderFeasibility
     * -------- OrdrWsSubmitCustomerOrder
     * -------- OrdrWsCancelCustomerOrder
     */    
    
    /* Disabled: properties of this subclass were moved up to TpFulfillmentCustomerOrderV3.CustomerOrder class
    
    public class ProductOrder {
        public DateTime InteractionDate;
        public String Description;
        public DateTime InteractionDateComplete;
        public String InteractionStatus;
        public String InteractionPriority;
        public String ErrorCode;
        public String ErrorDescription;
        public TpCommonBusinessInteractionExtV3.BusinessInteractionExtensions BusinessInteractionExtensions;
        private String[] InteractionDate_type_info = new String[]{'InteractionDate','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','dateTime','0','1','false'};
        private String[] Description_type_info = new String[]{'Description','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','string','0','1','false'};
        private String[] InteractionDateComplete_type_info = new String[]{'InteractionDateComplete','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','dateTime','0','1','false'};
        private String[] InteractionStatus_type_info = new String[]{'InteractionStatus','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','string','0','1','false'};
        private String[] InteractionPriority_type_info = new String[]{'InteractionPriority','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','string','0','1','false'};
        private String[] ErrorCode_type_info = new String[]{'ErrorCode','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','string','0','1','false'};
        private String[] ErrorDescription_type_info = new String[]{'ErrorDescription','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','string','0','1','false'};
        private String[] BusinessInteractionExtensions_type_info = new String[]{'BusinessInteractionExtensions','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','BusinessInteractionExtensions','0','1','false'};
        public DateTime ExpectedDeliveryDate;
        public DateTime RequestedDate;
        public TpCommonPartyV3.PartyRole[] PartyRole;
        public TpCommonBusinessInteractionExtV3.RequestExtensions RequestExtensions;
        private String[] ExpectedDeliveryDate_type_info = new String[]{'ExpectedDeliveryDate','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','dateTime','0','1','false'};
        private String[] RequestedDate_type_info = new String[]{'RequestedDate','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','dateTime','0','1','false'};
        private String[] PartyRole_type_info = new String[]{'PartyRole','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','PartyRole','0','-1','false'};
        private String[] RequestExtensions_type_info = new String[]{'RequestExtensions','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','RequestExtensions','0','1','false'};
        public String ExternalId;
        public String IsValid;
        public Date AssignedResponsibilityDate;
        public TpFulfillmentProductOrderV3.SiteOrderItem[] SiteOrderItem;
        public TpFulfillmentProductOrderV3.ProductOrderItem[] ProductOrderItem;
        public TpFulfillmentProductOrderExtV3.ProductOrderExtensions ProductOrderExtensions;
        private String[] ExternalId_type_info = new String[]{'ExternalId','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','string','0','1','false'};
        private String[] IsValid_type_info = new String[]{'IsValid','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','string','0','1','false'};
        private String[] AssignedResponsibilityDate_type_info = new String[]{'AssignedResponsibilityDate','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','date','0','1','false'};
        private String[] SiteOrderItem_type_info = new String[]{'SiteOrderItem','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','SiteOrderItem','0','-1','false'};
        private String[] ProductOrderItem_type_info = new String[]{'ProductOrderItem','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','ProductOrderItem','0','-1','false'};
        private String[] ProductOrderExtensions_type_info = new String[]{'ProductOrderExtensions','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','ProductOrderExtensions','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','false','false'};
        private String[] field_order_type_info = new String[]{'InteractionDate','Description','InteractionDateComplete','InteractionStatus','InteractionPriority','ErrorCode','ErrorDescription','BusinessInteractionExtensions','ExpectedDeliveryDate','RequestedDate','PartyRole','RequestExtensions','ExternalId','IsValid','AssignedResponsibilityDate','SiteOrderItem','ProductOrderItem','ProductOrderExtensions'};
    }
	*/

    public class ProductOrderItem {
		public TpCommonBaseExtV3.IbmTelecomObjectExtensions IbmTelecomObjectExtensions;
        private String[] IbmTelecomObjectExtensions_type_info = new String[]{'IbmTelecomObjectExtensions','http://www.ibm.com/telecom/common/schema/base/v3_0','IbmTelecomObjectExtensions','0','1','false'};
        public String Id;
        public String Name;
        public DateTime fromTimestamp;
        public DateTime toTimestamp;
        public TpCommonBaseV3.CharacteristicValue[] CharacteristicValue;
        public TpCommonBaseExtV3.EntityExtensions EntityExtensions;
        private String[] Id_type_info = new String[]{'Id','http://www.ibm.com/telecom/common/schema/base/v3_0','string','0','1','false'};
        private String[] Name_type_info = new String[]{'Name','http://www.ibm.com/telecom/common/schema/base/v3_0','string','0','1','false'};
        private String[] fromTimestamp_type_info = new String[]{'fromTimestamp','http://www.ibm.com/telecom/common/schema/base/v3_0','dateTime','0','1','false'};
        private String[] toTimestamp_type_info = new String[]{'toTimestamp','http://www.ibm.com/telecom/common/schema/base/v3_0','dateTime','0','1','false'};
        private String[] CharacteristicValue_type_info = new String[]{'CharacteristicValue','http://www.ibm.com/telecom/common/schema/base/v3_0','CharacteristicValue','0','-1','false'};
        private String[] EntityExtensions_type_info = new String[]{'EntityExtensions','http://www.ibm.com/telecom/common/schema/base/v3_0','EntityExtensions','0','1','false'};
        public TpCommonBaseV3.EntitySpecification Specification;
        public TpCommonBaseExtV3.EntityWithSpecificationExtensions EntityWithSpecificationExtensions;
        private String[] Specification_type_info = new String[]{'Specification','http://www.ibm.com/telecom/common/schema/base/v3_0','EntitySpecification','0','1','false'};
        private String[] EntityWithSpecificationExtensions_type_info = new String[]{'EntityWithSpecificationExtensions','http://www.ibm.com/telecom/common/schema/base/v3_0','EntityWithSpecificationExtensions','0','1','false'};

        public String Action;
        public DateTime ExpectedDeliveryDate;
        public DateTime RequestedDate;
        public TpCommonBaseV3.EntityWithSpecification[] AppointmentId;
        public TpCommonBaseV3.EntityWithSpecification[] ReservationId;
        public String ErrorCode;
        public String ErrorDescription;
        public TpCommonUrbanPropertyAddressV3.UrbanPropertyAddress Address;
        public TpCommonBusinessInteractionExtV3.BusinessInteractionItemExtensions BusinessInteractionItemExtensions;
        private String[] Action_type_info = new String[]{'Action','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','string','0','1','false'};
        private String[] ExpectedDeliveryDate_type_info = new String[]{'ExpectedDeliveryDate','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','dateTime','0','1','false'};
        private String[] RequestedDate_type_info = new String[]{'RequestedDate','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','dateTime','0','1','false'};
        private String[] AppointmentId_type_info = new String[]{'AppointmentId','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','EntityWithSpecification','0','-1','false'};
        private String[] ReservationId_type_info = new String[]{'ReservationId','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','EntityWithSpecification','0','-1','false'};
        private String[] ErrorCode_type_info = new String[]{'ErrorCode','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','string','0','1','false'};
        private String[] ErrorDescription_type_info = new String[]{'ErrorDescription','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','string','0','1','false'};
        private String[] Address_type_info = new String[]{'Address','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','UrbanPropertyAddress','0','1','false'};
        private String[] BusinessInteractionItemExtensions_type_info = new String[]{'BusinessInteractionItemExtensions','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','BusinessInteractionItemExtensions','0','1','false'};
        public TpCommonBaseV3.EntityWithSpecification SiteOrderItemId;
        public TpCommonBaseV3.EntityWithSpecification CustomerAccountOrderItemId;
        public TpInventoryProductConfigV3.OrderedProduct Product;
        public TpFulfillmentProductOrderV3.ProductOrderItem[] ComponentProductOrderItem;
        public TpFulfillmentProductOrderExtV3.ProductOrderItemExtensions ProductOrderItemExtensions;
        private String[] SiteOrderItemId_type_info = new String[]{'SiteOrderItemId','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','EntityWithSpecification','0','1','false'};
        private String[] CustomerAccountOrderItemId_type_info = new String[]{'CustomerAccountOrderItemId','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','EntityWithSpecification','0','1','false'};
        private String[] Product_type_info = new String[]{'Product','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','OrderedProduct','1','1','false'};
        private String[] ComponentProductOrderItem_type_info = new String[]{'ComponentProductOrderItem','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','ProductOrderItem','0','-1','false'};
        private String[] ProductOrderItemExtensions_type_info = new String[]{'ProductOrderItemExtensions','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','ProductOrderItemExtensions','0','1','false'};
        
        //2015-12-14 [Roderick de Vera]: set elementFormDefault(2nd element of the array) to 'false'
        private String[] apex_schema_type_info = new String[]{'http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','false','false'};
        private String[] field_order_type_info = new String[]{
            	'IbmTelecomObjectExtensions',
                'Id',
                'Name',
                'fromTimestamp',
                'toTimestamp',
                'CharacteristicValue',
                'EntityExtensions',
                'Specification',
                'EntityWithSpecificationExtensions',
            	'Action',
                'ExpectedDeliveryDate',
                'RequestedDate',
                'AppointmentId',
                'ReservationId',
                'ErrorCode',
                'ErrorDescription',
                'Address',
                'BusinessInteractionItemExtensions',
                'SiteOrderItemId',
                'CustomerAccountOrderItemId',
                'Product',
                'ComponentProductOrderItem',
                'ProductOrderItemExtensions'};
    }
    public class SiteOrderItem {
        public String Action;
        public DateTime ExpectedDeliveryDate;
        public DateTime RequestedDate;
        public TpCommonBaseV3.EntityWithSpecification[] AppointmentId;
        public TpCommonBaseV3.EntityWithSpecification[] ReservationId;
        public String ErrorCode;
        public String ErrorDescription;
        public TpCommonUrbanPropertyAddressV3.UrbanPropertyAddress Address;
        public TpCommonBusinessInteractionExtV3.BusinessInteractionItemExtensions BusinessInteractionItemExtensions;
        private String[] Action_type_info = new String[]{'Action','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','string','0','1','false'};
        private String[] ExpectedDeliveryDate_type_info = new String[]{'ExpectedDeliveryDate','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','dateTime','0','1','false'};
        private String[] RequestedDate_type_info = new String[]{'RequestedDate','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','dateTime','0','1','false'};
        private String[] AppointmentId_type_info = new String[]{'AppointmentId','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','EntityWithSpecification','0','-1','false'};
        private String[] ReservationId_type_info = new String[]{'ReservationId','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','EntityWithSpecification','0','-1','false'};
        private String[] ErrorCode_type_info = new String[]{'ErrorCode','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','string','0','1','false'};
        private String[] ErrorDescription_type_info = new String[]{'ErrorDescription','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','string','0','1','false'};
        private String[] Address_type_info = new String[]{'Address','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','UrbanPropertyAddress','0','1','false'};
        private String[] BusinessInteractionItemExtensions_type_info = new String[]{'BusinessInteractionItemExtensions','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','BusinessInteractionItemExtensions','0','1','false'};
        public TpCommonBusinessInteractionV3.BusinessInteractionEntity Site;
        public TpFulfillmentProductOrderExtV3.SiteOrderItemExtensions SiteOrderItemExtensions;
        private String[] Site_type_info = new String[]{'Site','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','BusinessInteractionEntity','1','1','false'};
        private String[] SiteOrderItemExtensions_type_info = new String[]{'SiteOrderItemExtensions','http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','SiteOrderItemExtensions','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0','false','false'};
        private String[] field_order_type_info = new String[]{'Action','ExpectedDeliveryDate','RequestedDate','AppointmentId','ReservationId','ErrorCode','ErrorDescription','Address','BusinessInteractionItemExtensions','Site','SiteOrderItemExtensions'};
    }
}