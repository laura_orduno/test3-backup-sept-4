/* This class is used for Order creation Logic .
Developer: Aditya Jamwal (IBM)
Created Date: 19-June-2017
*/
global without sharing  class VlocityCPQUtil implements vlocity_cmt.VlocityOpenInterface2{
    
    global static String CUSTOMER_SOLUTION_RECORDTYPE_NAME = 'Customer Solution';
    global static String CUSTOMER_ORDER_RECORDTYPE_NAME = 'Customer Order';
    
    global Object invokeMethod(String methodName, Map<String, Object> input, Map<String, Object> output, Map<String, Object> options)  {
        
        if (methodName.indexOf('createCustomerSolution')!=-1) {
            createCustomerSolution(input, output, options);
        }
        
        if (methodName.indexOf('createCustomerOrders')!=-1) {
            createCustomerOrders(input, output, options);
        }
        
        if (methodName.indexOf('createCustomerSolutionAndCustomerOrders')!=-1) {
            createCustomerSolutionAndCustomerOrders(input, output, options);
        }
        
        if(methodName.indexOf('deleteCustomerOrders')!=-1){
            deleteCustomerOrders(input, output, options);           
        }
        
        if(methodName.indexOf('getCustomerOrdersLocation')!=-1){
            getCustomerOrdersLocation(input, output, options);
        }
        
        if(methodName.indexOf('cancelOrders')!=-1){
            cancelOrders(input, output, options);
        }
       
        if(methodName.indexOf('updateOrders')!=-1){
            updateOrders(input, output, options);
        }

        // BMPF -19
        if (methodName.indexOf('checkServiceLocationsAndCreateSolutionOrder')!=-1) {
            system.debug('In Invoke');
            String customerSolutionId = checkServiceLocationsAndCreateSolutionOrder(input, output, options);
            return customerSolutionId;
        }// End BMPF - 19
        
        return false;
    }
    
    private void createCustomerSolutionAndCustomerOrders(Map<String, Object> input, Map<String, Object> output, Map<String, Object> options){
        createCustomerSolution(input, output, options);
         if(null != output.get('customerSolutionId')){
             input.put('customerSolutionId',output.get('customerSolutionId'));
                createCustomerOrders(input, output, options);
            }        
    }
   private void createCustomerSolution(Map<String, Object> input, Map<String, Object> output, Map<String, Object> options){
        try{
          
            String RCID = (String)input.get('rcid');            
            String recordTypeId = getRecordTypeId(CUSTOMER_SOLUTION_RECORDTYPE_NAME);
            String orderType = (String)input.get('orderType'); 
            String contactId = (String)input.get('contactId'); 
            system.debug(' in vlocity cpq  '+recordTypeId);
            if(String.isNotBlank(recordTypeId)){
            Order customerSolution = new Order(recordTypeId = recordTypeId,accountid = RCID);
                  customerSolution.EffectiveDate = Date.today();
                  customerSolution.Status = OrdrConstants.ORDR_NOT_SUBMITTED;
                  customerSolution.Sales_Representative__c = UserInfo.getUserId();
                  if(String.isNotBlank(contactId)){
                      customerSolution.CustomerAuthorizedById = contactId;
                  }
                  customerSolution.Type = OrdrConstants.ORDER_TYPE_NEW_SERVICES;
                  if(String.isNotBlank(orderType)){
                  customerSolution.Type = orderType;  
                  }
                // RTA - 617 fix
                String oppId = (String)input.get('oppId'); 
                System.debug('@@@@ oppId' + oppId);
                if(String.isNotBlank(oppId)){
                    customerSolution.opportunityid = oppId;
                    customerSolution.vlocity_cmt__OpportunityId__c = oppId;
                }
                // End
            insert customerSolution;
             system.debug(' in vlocity cpq  1 '+customerSolution.id);   
            output.put('customerSolutionId',customerSolution.id);
            }else{
                output.put('error',OrdrConstants.CUSTOMER_SOLUTION_RECORDTYPEID_NOT_FOUND);
            }
             if(Test.isRunningTest()){  integer i=4/0;}   
        }catch(exception e){
            String errorMessage = Label.OCOM_ExceptionMessagePrefix+e.getMessage();
            system.debug('@@Error ' + errorMessage);
            output.put('error',errorMessage);
        }
    }
    
    private void createCustomerOrders(Map<String, Object> input, Map<String, Object> output, Map<String, Object> options){
        try{
           system.debug('!!@@ input createCustomerOrders '+input);
            String RCID = (String)input.get('rcid');    
            String customerSolutionId = (String)input.get('customerSolutionId'); 
            String recordTypeId = getRecordTypeId(CUSTOMER_ORDER_RECORDTYPE_NAME);
            List<OrderWrapper> childOrders = (List<OrderWrapper>) input.get('childOrders');
            Map<String,OrderWrapper>  createOrderResultMap = new Map<String,OrderWrapper>();
            if(String.isNotBlank(recordTypeId)){
                if(null != childOrders){
                   List<Order> orderList  = new List<Order>();
                   for(OrderWrapper co :childOrders){
                         OrderWrapper ordrWrap = new OrderWrapper();
                         ordrWrap.orderId = co.orderId;
                       Order customerOrder = new Order(recordTypeId = recordTypeId,accountid = RCID);         
                             customerOrder.EffectiveDate = Date.today();
                             customerOrder.Status = OrdrConstants.ORDR_NOT_SUBMITTED;
                             customerOrder.Type = OrdrConstants.ORDER_TYPE_NEW_SERVICES;
                            if(String.isNotBlank(co.orderType)){
                             customerOrder.Type = co.orderType;  
                            }
                            if(String.isNotBlank(co.serviceAddressId)){
                             customerOrder.service_address__c = co.serviceAddressId;
                            }
                            if(String.isNotBlank(co.moveServiceAddressId)){
                             customerOrder.MoveOutServiceAddress__c = co.moveServiceAddressId;
                            }
                             customerOrder.Sales_Representative__c = UserInfo.getUserId();
                        
                        if(String.isNotBlank(co.SolutionOrderId)){
                            customerOrder.parentId__c = co.SolutionOrderId;
                        }else if(String.isNotBlank(customerSolutionId)){
                           customerOrder.parentId__c = customerSolutionId;
                        }
                       ordrWrap.isError = false;
                       ordrWrap.message = 'success';
                       createOrderResultMap.put(ordrWrap.orderId, ordrWrap);
                      orderList.add(customerOrder);
                    }
                    output.put('createOrderResultMap',performDMLOperationOnOrders(orderList,createOrderResultMap,'create'));
                 //   output.put('success','success');
                }else{
                   output.put('error',OrdrConstants.NO_CUSTOMER_ORDERS_FOUND_TO_CREATE);  
                }                
            }else{
                output.put('error',OrdrConstants.CUSTOMER_ORDER_RECORDTYPEID_NOT_FOUND);
            }
             if(Test.isRunningTest()){integer i=4/0;}   
        }catch(exception e){
            String errorMessage = Label.OCOM_ExceptionMessagePrefix+e.getMessage();
            output.put('error',errorMessage);
        }
    }
    
    //Update: Orders
    private void updateOrders(Map<String, Object> input, Map<String, Object> output, Map<String, Object> options){        
        List<OrderWrapper> orderList = (List<OrderWrapper>) input.get('orderList');
        List<Order> updateOrdersList = new List<Order>();
        system.debug('!!@@ updateOrderList 0 '+orderList);
        Map<String,OrderWrapper>  updateOrderResultMap = new Map<String,OrderWrapper>();
        try{
            if(null != orderList){
                for(OrderWrapper co : orderList){
                     system.debug('!!@@ updateOrderList 1 '+co);
                    OrderWrapper ordrWrap = new OrderWrapper();
                    ordrWrap.orderId = co.orderId;
                    if(null != co.orderId){
                        Order ordr = new Order();
                        ordr.Id = co.orderId;                    
                        ordrWrap.isError = false;
                        ordrWrap.message = 'success';
                        if(String.isNotBlank(co.orderType)){
                            ordr.Type =  co.orderType;                        
                        }
                        ordr.Service_Address__c = co.serviceAddressId;
                        ordr.MoveOutServiceAddress__c = co.moveServiceAddressId;   
                        
                        updateOrderResultMap.put(ordrWrap.orderId,ordrWrap);
                        updateOrdersList.add(ordr);
                    }else{
                        ordrWrap.isError = true;
                        ordrWrap.message = OrdrConstants.NO_ORDERID_PROVIDED_TO_UPDATE_ORDER;  
                    }
                }            
                output.put('updateOrderResultMap',performDMLOperationOnOrders(updateOrdersList,updateOrderResultMap,'update'));
                system.debug('!!@@ updateOrderResultMap '+updateOrderResultMap);
            }
             if(Test.isRunningTest()){  integer i=4/0;}   
        }catch(exception e){
            String errorMessage = Label.OCOM_ExceptionMessagePrefix+e.getMessage();
            output.put('error',errorMessage);
        }
    }
    
  //Purpose: Cancel orders  
     private void cancelOrders(Map<String, Object> input, Map<String, Object> output, Map<String, Object> options){
        try{
            List<String> cancelOrderIdList = (List<String>) input.get('cancelOrderIds');
            Set<String>  cancelOrderIdSet = new Set<String>();             
            String reasonCode = '';        
            String generalNoteRemarks = '';
            Map<String,OrderWrapper>  cancelOrderResultMap = new Map<String,OrderWrapper>();
            List<Order>  toBeCancelledOrderList = new List<Order>();
            List<Credit_Assessment__c>  toBeCancelledCARList = new List<Credit_Assessment__c>();
            List<ID>  toBeDeletedMRCNRCCARList = new List<ID>();
            Map<String,Boolean> OrderIdToIsCancellableMap = new Map<String,Boolean>();
            List<String> serviceAddressList = new List<String>();
            
                if(null != cancelOrderIdList){                    
                    cancelOrderIdSet.addAll(cancelOrderIdList);
                }
                if(NULL != cancelOrderIdSet && cancelOrderIdSet.size()>0 ){
                     OrderIdToIsCancellableMap = OrdrUtilities.isCancellable(cancelOrderIdSet);
                    system.debug('OrderIdToIsCancellableMap '+ OrderIdToIsCancellableMap);
                    if(null != input.get('reasonCode')){ reasonCode = (String) input.get('reasonCode'); }
                    if(null != input.get('generalNotesRemark')){ generalNoteRemarks = (String) input.get('generalNotesRemark');}
                    List<Order> orderList  = [Select id, parentId__c, orderMgmtId__c,change_type__c, reason_code__c,Status, General_Notes_Remarks__c,
                                              Credit_Assessment__r.CAR_Result__c,Credit_Assessment__r.CAR_Status__c,Credit_Assessment__c,
                                              ServiceAddressSummary__c, parentId__r.Credit_Assessment__c,
                                              Credit_Assessment__r.CAR_Code__c,(select id,Contract_Line_Item__r.vlocity_cmt__ContractId__c from orderitems) from order where id = :cancelOrderIdSet or parentId__c = :cancelOrderIdSet];
                    List<String> contractIdSet=new List<String>();
                    List<String> oiIdsList=new List<String>();
                    for(Order ordr :orderList){
                        String previousOrderStatus = ordr.Status; 
                        Boolean sendOrderToNC = previousOrderStatus == OrdrConstants.ORDR_INPROGRESS_STATUS || previousOrderStatus == OrdrConstants.ORDR_INPROGRESS_STATUS || previousOrderStatus == OrdrConstants.ORDR_ERROR_STATUS || previousOrderStatus == OrdrConstants.ORDR_ACTIVATED_STATUS || previousOrderStatus  == OrdrConstants.ORDR_SUBMITTED_STATUS;
                        OrderWrapper cancelWrap = new OrderWrapper();
                        cancelWrap.orderId = ordr.id;
                        // added as per RTA-647 213.                            
                        if(String.isNotBlank(ordr.ServiceAddressSummary__c)){       
                            serviceAddressList.add(ordr.ServiceAddressSummary__c);      
                        } //end 647
                        if(null != OrderIdToIsCancellableMap && OrderIdToIsCancellableMap.size()>0 && null != OrderIdToIsCancellableMap.get(ordr.id)){
                             
                            if(OrderIdToIsCancellableMap.get(ordr.id)){
                                
                                for(OrderItem orderItemRec : ordr.orderitems){
                                    if(String.isNotBlank(orderItemRec.Contract_Line_Item__r.vlocity_cmt__ContractId__c)){
                                        contractIdSet.add(orderItemRec.Contract_Line_Item__r.vlocity_cmt__ContractId__c);
                                        String oiId=orderItemRec.id;
                                        oiIdsList.add(oiId);
                                        if(String.isNotBlank(oiId) && oiId.length()>15){
                                            oiId=oiId.substring(0, 15);
                                            oiIdsList.add(oiId);
                                        }
                                        
                                    }
                                }
                                ordr.Change_Type__c = OrdrConstants.ORDER_CANCEL_TYPE;
                                ordr.Reason_Code__c = reasonCode;
                                ordr.General_Notes_Remarks__c = generalNoteRemarks;
                                ordr.Status = OrdrConstants.ORDR_CANCELLED_STATUS;
                                ordr.Activities__c= OrdrConstants.ORDER_CANCEL_ACTIVITIES;
                                if(sendOrderToNC){
                                    ID jobID = System.enqueueJob(new OrdrCancelOrderManager(ordr.id));   
                                    ordr.Status = OrdrConstants.ORDR_PENDING;
                                }                                
                                //cancel order in SF.
                                cancelWrap.status = ordr.Status;
                                //Check if CAR exists and it needs to be cancelled.
                                if(string.isnotblank(ordr.Credit_Assessment__c) &&  ordr.Credit_Assessment__r.CAR_Status__c !='Cancelled'){
                                    Credit_Assessment__c CAR= new Credit_Assessment__c();
                                    CAR.id = ordr.Credit_Assessment__c;
                                    CAR.CAR_Status__c = OrdrConstants.CAR_CANCEL_STATUS;     
                                    CAR.CAR_Code__c = OrdrConstants.CAR_CANCEL_CODE;  
                                    toBeCancelledCARList.add(CAR);
                                }
                                // added as per RTA-647     
                                if(ordr.parentId__c != null && ordr.parentId__r.Credit_Assessment__c != null){      
                                    toBeDeletedMRCNRCCARList.add(ordr.parentId__r.Credit_Assessment__c);        
                                } //end 647
                                //unreserve used TNs.
                                OrdrUtilities.unreserveUsedTnsFuture(ordr.id);
                                //cancel WFM booking
                                OCOM_WFM_DueDate_Mgmt_Helper.cancelWFMBookingByOrderId(ordr.id);    
                                
                            }else if(!OrderIdToIsCancellableMap.get(ordr.id)){                                 
                                cancelWrap.isError = true;
                                cancelWrap.message = OrdrConstants.ORDER_STATUS_PONR_PASSED;
                            }  
                        }else{ //This condition shouldn't be reached.
                            cancelWrap.isError = true;
                            cancelWrap.message = OrdrConstants.ORDER_CANCELLATION_REQUEST_FAILED;                            
                        }
                        cancelOrderResultMap.put(ordr.id,cancelWrap);
                        toBeCancelledOrderList.add(ordr);
                    }  
                    if(!contractIdSet.isEmpty() || !oiIdsList.isEmpty()){
                        OrdrUtilities.clearContractLinesAssociatedWithOrder(contractIdSet,oiIdsList);
                    }
                    // cancel CAR
                    if(null != toBeCancelledCARList && toBeCancelledCARList.size()>0){
                        update toBeCancelledCARList;
                    }
                    // Delete MRC/NRC associated with cancel order -  added as per RTA-647      
                    List<Credit_Related_Product__c> MRC_NRC_ToDelete = [SELECT Id FROM Credit_Related_Product__c WHERE      
                                                (Credit_Assessment_MRC__c IN: toBeDeletedMRCNRCCARList OR Credit_Assessment_NRC__c IN: toBeDeletedMRCNRCCARList)     
                                                AND Service_Address__c IN: serviceAddressList];      
                    if(MRC_NRC_ToDelete != null && MRC_NRC_ToDelete.size()>0){      
                        delete MRC_NRC_ToDelete;        
                    }
                    
                    //update Order
                    if(null != toBeCancelledOrderList && toBeCancelledOrderList.size()>0){
                      output.put('cancelOrdersResult',performDMLOperationOnOrders(toBeCancelledOrderList,cancelOrderResultMap,'update'));
                    }
                    
                }else{
                   output.put('error',OrdrConstants.NO_ORDERID_PROVIDED_TO_CANCEL_ORDER);  
                }
            if(Test.isRunningTest()){  integer i=4/0;}            
        }catch(exception e){
            String errorMessage = Label.OCOM_ExceptionMessagePrefix+e.getMessage();
            output.put('error',errorMessage);
        }
    }
    
     private void deleteCustomerOrders(Map<String, Object> input, Map<String, Object> output, Map<String, Object> options){
        try{
            List<String> deleteOrderIdList = (List<String>) input.get('deleteOrderIds');
            Map<String,OrderWrapper>  deleteOrderResultMap = new Map<String,OrderWrapper>();
            List<Order> orderList  = new List<Order>();
                if(null != deleteOrderIdList){
                    
                    for(String orderId :deleteOrderIdList){
                        OrderWrapper ordrWrap = new OrderWrapper();
                        ordrWrap.orderId = orderId;
                        ordrWrap.isError = false;
                        ordrWrap.message = 'success';
                        Order customerOrder = new Order(id = orderId); 
                        orderList.add(customerOrder);                        
                     }
                    system.debug('@@ delete '+orderList);
                    output.put('deleteOrderResultMap',performDMLOperationOnOrders(orderList,deleteOrderResultMap,'delete'));
                }else{
                   output.put('error',OrdrConstants.NO_ORDER_FOUND_TO_DELETE);  
                }
                 if(Test.isRunningTest()){  integer i=4/0;}   
        }catch(exception e){
            String errorMessage = Label.OCOM_ExceptionMessagePrefix+e.getMessage();
            output.put('error',errorMessage);
        }
    }
    
    private void getCustomerOrdersLocation(Map<String, Object> input, Map<String, Object> output, Map<String, Object> options){
        try{
            
            String customerSolutionId = (String)input.get('customerSolutionId');             
            List<CustomerOrderLocationWrapper> CustomerOrderLocationsList = new List<CustomerOrderLocationWrapper>();
            
            if(String.IsNotBlank(customerSolutionId)){
                
                for(Order ordr : [select id,ordernumber,service_address__c,service_address__r.Address__c,service_address__r.Suite_Number__c,service_address__r.Street_Address__c,
                                  service_address__r.AddressText__c,service_address__r.City__c,service_address__r.Province__c,
                                  service_address__r.Country__c,parentId__c,service_address__r.isTVAvailable__c,service_address__r.Forbone__c,
                                  Service_Address__r.is_ILEC__c,service_address__r.Maximum_Speed__c,sub_order_number__c,MoveOutServiceAddress__c,Move_Out_Service_Address_Text__c
                                  from Order where parentId__c = :customerSolutionId and service_address__c != NULL and Status !='Cancelled']){ 
                                  CustomerOrderLocationWrapper cOLWrapper = new CustomerOrderLocationWrapper();
                                  cOLWrapper.serviceAddressId = ordr.service_address__c;
                                  cOLWrapper.serviceAddressText = ordr.service_address__r.AddressText__c;
                                  cOLWrapper.serviceAddressWithOrderNumber = ordr.service_address__r.AddressText__c + '  -  ' + ordr.sub_order_number__c;
                                  cOLWrapper.orderId = ordr.id;
                                  cOLWrapper.orderNumber = ordr.sub_order_number__c;
                         if(String.isNotBlank(ordr.MoveOutServiceAddress__c)){
                             cOLWrapper.moveOutServiceAddressId = ordr.MoveOutServiceAddress__c;
                             cOLWrapper.moveOutServiceAddress =   ordr.Move_Out_Service_Address_Text__c;
                         }

                        // 12/20/2017 Sid: Commenting out for BSBD_RTA-655: Duplicated unit #s for existing addresses. AddressText__c already contains Suite_Number__c. 
                        //  if(NULL != ordr.service_address__r.Suite_Number__c && string.isNotBlank(ordr.service_address__r.Suite_Number__c)){
                        //    cOLWrapper.serviceAddressWithOrderNumber = ordr.service_address__r.Suite_Number__c+ ' '+ ordr.service_address__r.AddressText__c +' '+ ordr.sub_order_number__c;
                        //  }
                         
                         cOLWrapper.tvAvailabilityInfo = ordr.service_address__r.isTVAvailable__c == true ? Label.OCOM_TV_Available : Label.OCOM_TV_Not_Available; 
                         cOLWrapper.forborneInfo = ordr.service_address__r.Forbone__c == true ?  Label.OCOM_Forborne : Label.OCOM_Non_Forborne;
                         cOLWrapper.isILEC = ordr.service_address__r.Is_ILEC__c != null ? ordr.service_address__r.Is_ILEC__c : false;
                         cOLWrapper.maxSpeed = ordr.service_address__r.Maximum_Speed__c != null ? ordr.service_address__r.Maximum_Speed__c : '';
                         cOLWrapper.maxSpdCopper =  cOLWrapper.maxSpeed != '' ? false : true;
                                      
                         CustomerOrderLocationsList.add(cOLWrapper);
                                      
                         system.debug(' customerSolutionId 2 '+customerSolutionId);                                      
                        }
                output.put('CustomerOrderLocations',CustomerOrderLocationsList);
            }else{
                output.put('error',OrdrConstants.NO_ORDER_FOUND);  
            }                
           if(Test.isRunningTest()){  integer i=4/0;}    
        }catch(exception e){
            String errorMessage = Label.OCOM_ExceptionMessagePrefix+e.getMessage();
            output.put('error',errorMessage);
        }
    }
    
    //update Orders        
    public Map<String,OrderWrapper> performDMLOperationOnOrders(List<Order> OrdersList,Map<String,OrderWrapper> OrderIdwithOrderWrapperMap,String operation){
         System.debug('!!@@performDMLOperationOnOrders '+operation);
        Database.SaveResult[] saveResults;
        Database.DeleteResult[] deleteResults; 
        if(operation == 'update'){
         saveResults = Database.update(OrdersList,false);
        }
        if(operation == 'create'){
         saveResults = Database.insert(OrdersList,false);
        }
        if(Test.isRunningTest()){
         OrdersList.add(new Order());
         saveResults = Database.insert(OrdersList,false);   
        // deleteResults = Database.delete(new List<Order>{ new Order()}, false);  
        }
        if(operation == 'delete'){
         deleteResults = Database.delete(OrdersList, false);
        }
        if(saveResults != null){
            for (Database.SaveResult result : saveResults) {
                if (!result.isSuccess()) {
                    Database.Error[] errs = result.getErrors();
                    OrderIdwithOrderWrapperMap = processFailedDMLrecords(result.getId(),OrderIdwithOrderWrapperMap,errs);
                }else{
                    System.debug(' Success SR '+result.getId());
                }
            }
        }

        if(deleteResults != null ){
             for (Database.DeleteResult result : deleteResults) {
                  if (!result.isSuccess()) {
                       Database.Error[] errs = result.getErrors();
                       OrderIdwithOrderWrapperMap = processFailedDMLrecords(result.getId(),OrderIdwithOrderWrapperMap,errs);
                  }
             }
        }
        
        return OrderIdwithOrderWrapperMap;
    }

    private Map<String,OrderWrapper> processFailedDMLrecords(String OrderId,Map<String,OrderWrapper> OrderIdwithOrderWrapperMap,Database.Error[] errs){
        for(Database.Error err : errs){
            if(null != OrderIdwithOrderWrapperMap.get(OrderId)){                                            
                OrderWrapper ordrWrap = OrderIdwithOrderWrapperMap.get(OrderId);
                ordrWrap.isError = true;
                ordrWrap.message = err.getMessage();   
                OrderIdwithOrderWrapperMap.put(ordrWrap.orderId,ordrWrap);
            } 
            System.debug(' Error SR '+err.getStatusCode() + ' - ' + err.getMessage());                                    
        }

        return OrderIdwithOrderWrapperMap;
    }
    
    public String getRecordTypeId(String recordTypeName){
        
        String recordTypeId;
        
        if(String.isNotBlank(recordTypeName)){
            
            Schema.DescribeSObjectResult resSchema = Order.sObjectType.getDescribe();
            //getting all Recordtype  Order
            Map<String,Schema.RecordTypeInfo> recordTypeInfo = resSchema.getRecordTypeInfosByName(); 
            //Getting Business Record Type Id
            if(null != recordTypeInfo && null != recordTypeInfo.get(recordTypeName)){
                recordTypeId = recordTypeInfo.get(recordTypeName).getRecordTypeId();
            }            
        }
        return recordTypeId;
        
    }
    
    // BMPF - 19
    private String checkServiceLocationsAndCreateSolutionOrder(Map<String, Object> input, Map<String, Object> output, Map<String, Object> options){
        String RCID = (String)input.get('rcid');
        Set<String> ilecProvinces = new Set<String>();
        for(Country_Province_Codes__c ilecProvince : [Select id,Name,is_ILEC__c from Country_Province_Codes__c where is_ILEC__c = true]){
            if(ilecProvince.is_ILEC__c){
                ilecProvinces.add(ilecProvince.Name);
            } 
        }
        List<smbcare_address__c> serviceLocations = [SELECT id,Address__c,  Street__c,Street_Number__c,Street_Name__c,Suite_Number__c ,Street_Address__c,AddressText__c,
                                  City__c,FMS_Address_ID__c,Location_Id__c,
                                  Province__c,Country__c 
                                  FROM smbcare_address__c 
                                  WHERE (Account__c = :RCID OR Account__r.ParentId = :RCID  )
                                  AND ( Location_Id__c != NULL 
                                       OR FMS_Address_ID__c != NULL 
                                       OR Service_Account_Id__c != NULL 
                                       OR ( Province__c != NULL AND Province__c Not IN :ilecProvinces )
                                      )
                                  ];
        system.debug('In checkServiceLocationsAndCreateSolutionOrder');
        if(serviceLocations != null && serviceLocations.size()>0){
            system.debug('Service location found');
            return null;
        }else{
            system.debug('Service location not found');
            invokeMethod('createCustomerSolution', input, output, options);
            if(output.get('customerSolutionId')!=null){
                String customerId = (String)output.get('customerSolutionId');
                return customerId;
            }else{
                return null;
            }
        }
    }
    // End BMPF -19

    global class CustomerOrderLocationWrapper{
        global String serviceAddressId;
        global String orderId;
        global String orderNumber;
        global String serviceAddressText;
        global String serviceAddressWithOrderNumber;
        global String tvAvailabilityInfo;
        global String forborneInfo;
        global Boolean isILEC;
        global String maxSpeed;
        global Boolean maxSpdCopper;
        global String moveOutServiceAddressId;
        global String moveOutServiceAddress;
    }
    
    global class OrderWrapper{
        global String serviceAddressId;
        global String moveServiceAddressId;
        global String SolutionOrderId;
        global String orderId;
        global String orderType;
        global String moveType;
        global String status;
        global Boolean isError = false;
        global String message;
    }
}