public class change_RecordType_Utility {
    //Record Type names. We should update these staic variables If there are any changes in FOBOChecklist__c recordtype names. 
    Private static final String cFiledRecordType = 'C Field Work';
    Private static final String cancel_Service_RecordType = 'Cancel Service';
    Private static final String fobolist_RecordType= 'FOBOChecklist Layout';
    Private static final String inet_RecordType= 'Internet Feature Change';
    Private static final String move_RecordType= 'move';
    Private static final String other_Change_RecordType= 'Other Changes';
    Private static final String pswd_Change_RecordType= 'Password Change';
    Private static final String voice_Change_RecordType= 'Voice Feature Change';
    Private static final String TBO_RecordType= 'TBO';
    
    //Request type pick list values
    //Record Type names. We should update these staic variables If there are any changes in FOBOChecklist__c recordtype names. 
    Private static final String cFiled= 'C Field Work';
    Private static final String cancel_Service = 'Cancel Service';
    Private static final String inet = 'Internet Change';
    Private static final String move = 'Move';
    Private static final String other_Change = 'Other Changes';
    Private static final String pswd_Change= 'Password Change';
    Private static final String voice_Change= 'Voice Feature Change';
    Private static final String TBO= 'TBO';
    
    public static void changeRecordtype(list<FOBOChecklist__c> checklist){
        
    //Get all record type names and Id's from FOBOChecklist__c object. 
    map<String,Id> recordId_map = new map<String,Id>();
     for(RecordType rtype: [SELECT Name,Id FROM RecordType WHERE SobjectType = 'FOBOChecklist__c']){
                recordId_map.put(rtype.Name.toUpperCase(),rtype.Id);
                system.debug('==========RecordType========' + rtype);
     }
    for(FOBOChecklist__c fobo: checklist){
        
        if(fobo.Request_Type__c == null)
            fobo.RecordTypeId = recordId_map.get(fobolist_RecordType.toUpperCase());
            
        else if(fobo.Request_Type__c.toUpperCase() ==cFiled.toUpperCase()){
            
            fobo.RecordTypeId = recordId_map.get(cFiledRecordType.toUpperCase());
        }
        else if(fobo.Request_Type__c.toUpperCase() ==cancel_Service.toUpperCase()){
            
            fobo.RecordTypeId = recordId_map.get(cancel_Service_RecordType.toUpperCase());
        }
        else if(fobo.Request_Type__c.toUpperCase() ==inet.toUpperCase()){
            
            fobo.RecordTypeId = recordId_map.get(inet_RecordType.toUpperCase());
        }
        else if(fobo.Request_Type__c.toUpperCase() == move.toUpperCase()){
            
            fobo.RecordTypeId = recordId_map.get(move_RecordType.toUpperCase());
        }
        else if(fobo.Request_Type__c.toUpperCase() == other_Change.toUpperCase()){
            
            fobo.RecordTypeId = recordId_map.get(other_Change_RecordType.toUpperCase());
        }
        else if(fobo.Request_Type__c.toUpperCase() == pswd_Change.toUpperCase()){
            
            fobo.RecordTypeId = recordId_map.get(pswd_Change_RecordType.toUpperCase());
        }
        else if(fobo.Request_Type__c.toUpperCase() == voice_Change.toUpperCase()){
            
            fobo.RecordTypeId = recordId_map.get(voice_Change_RecordType.toUpperCase());
        }
        else if(fobo.Request_Type__c.toUpperCase() == TBO.toUpperCase()){
            
            fobo.RecordTypeId = recordId_map.get(TBO_RecordType.toUpperCase());
        }
    }
  }
}