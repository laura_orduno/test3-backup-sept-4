@isTest
private class trac_Offer_Print_ControllerTest {
    @isTest(SeeAllData=true)
    public static void testMyController() {
        
        // User must have a profile and a role
        Id PROFILEID = [SELECT id FROM Profile WHERE name='Sales Manager / Director / MD' LIMIT 1].id;
        
        // Get a user to test
        User tester = [SELECT Id FROM User WHERE isActive = true AND ProfileId = :PROFILEID LIMIT 1];
        
        // Create Offer
        Offer_House_Demand__c o = new Offer_House_Demand__c();
        map<string,schema.recordtypeinfo> recordTypeMap=offer_house_demand__c.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        o.recordtypeid=recordTypeMap.get('WLN Deal Support').getrecordtypeid();
        o.type_of_contract__c='CCA';
        o.signature_required__c='Yes';
        system.runAs(tester) {  insert o; }
        
        // create an instance of the controller
        PageReference thePage = Page.trac_Offer_Print;
        thePage.getParameters().put('ID', o.Id);
        Test.setCurrentPage(thePage);
        trac_Offer_Print_Controller myPageCon = new trac_Offer_Print_Controller();
    }
}