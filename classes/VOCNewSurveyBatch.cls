global class VOCNewSurveyBatch implements database.batchable<sobject>{
    global final string query;
    global VOCNewSurveyBatch(string query){
        this.query=query;
    }
    global database.querylocator start(database.batchablecontext bc){
        return database.getquerylocator(this.query);
    }
    global void execute(database.batchablecontext bc,list<sobject> scope){
    	set<string> agentIds=new set<string>();
        map<id,integer> idIndexMap=new map<id,integer>();
    	list<Account> rcidAccounts = new list<Account>();
        set<string> emails = new set<string>();
    	set<string> rcids=new set<string>();
        map<string,id> agentIdUserIdMap=new map<string,id>();
        map<string,id> activeAgentIdUserIdMap=new map<string,id>();
        map<string,id> agentParentRoleMap=new map<string,id>();
        map<id,string> roleManagerEmailMap=new map<id,string>();
        map<id,string> roleManagerIdMap=new map<id,string>();
        map<string,string> agentManagerIdMap=new map<string,string>();
        map<id,contact> newContactsMap=new map<id,contact>();
        map<string,string> agentManagerEmailMap=new map<string,string>();
        map<string,voc_survey_emails__c> vocSurveyEmailsMap=VOC_Survey_Emails__c.getall();
        integer counter=0;
        for(sobject s:scope){
            voc_survey__c survey=(voc_survey__c)s;
            if(string.isnotblank(survey.agent_id__c))
            {
            	agentIds.add(survey.agent_id__c.toLowerCase());
            }
            if(string.isnotblank(survey.rcid__c)){
                rcids.add(survey.rcid__c);
            }
            if(string.isnotblank(survey.custom_id__c)&&string.isnotblank(survey.customer_email__c)){
                emails.add(survey.customer_email__c);
            }
            idIndexMap.put(survey.id,counter);
            counter++;
        }
        list<user> users=[select id,isactive,Team_TELUS_ID__c,employeeid__c,userrole.parentroleid from user where License_Type__c = 'Salesforce' and Team_TELUS_ID__c=:agentIds];
        for(user agent:users){
            agentParentRoleMap.put(agent.Team_TELUS_ID__c.toLowerCase(),agent.userrole.parentroleid);
            agentIdUserIdMap.put(agent.Team_TELUS_ID__c.toLowerCase(),agent.id);
            if(agent.isactive){
                activeAgentIdUserIdMap.put(agent.Team_TELUS_ID__c.toLowerCase(),agent.id);
            }
        }
        // Setup parent role's email and ID map
        list<user> managers=[select id,email,userroleid from user where userroleid=:agentParentRoleMap.values()];
        for(user manager:managers){
            if(roleManagerEmailMap.containskey(manager.userroleid)){
                roleManagerIdMap.put(manager.userroleid,roleManagerIdMap.get(manager.userroleid)+','+manager.id);
                roleManagerEmailMap.put(manager.userroleid,roleManagerEmailMap.get(manager.userroleid)+','+manager.email);
            }else{
                roleManagerIdMap.put(manager.userroleid,manager.id);
                roleManagerEmailMap.put(manager.userroleid,manager.email);
            }
        }
        // Setup Agent Manager Map by using Parent Role's Email Map
        for(string agentId:agentParentRoleMap.keySet()){
            agentManagerIdMap.put(agentId,roleManagerIdMap.get(agentParentRoleMap.get(agentId)));
            agentManagerEmailMap.put(agentId,roleManagerEmailMap.get(agentParentRoleMap.get(agentId)));
        }
        map<string,string> rcidCbucidMap=new map<string,string>();
        map<string,id> rcidToAccount = new map<string,id>();
        for(list<account> accts:[select id,rcid__c,cbucid__c from account where rcid__c=:rcids]){
            for(account acct:accts){
                rcidToAccount.put(acct.rcid__c,acct.id);
                if(string.isnotblank(acct.cbucid__c)){
                	rcidCbucidMap.put(acct.rcid__c,acct.cbucid__c);
                }
            }
        }
        list<contact> matchingContacts=new list<contact>();
        for(list<contact> queriedContacts:[SELECT id,email,firstname,lastname,accountid,account.rcid__c,account.cbucid__c,createddate FROM contact WHERE active__c=true and accountid!=null and account.recordtype.name='RCID' and (account.rcid__c=:rcids or email=:emails) order by email,createddate desc]){
            matchingContacts.addall(queriedContacts);
        }
        // Need to keep multiple matching contacts with same emails, in case:
        // 
        // andy.leung@telus.com : RCID 123 : CBUCID 456
        // andy.leung@telus.com : RCID 789 : CBUCID 899
        map<string,list<integer>> emailContactIndexMap=new map<string,list<integer>>();
        integer numContacts=0;
        for(Contact con:matchingContacts){
            list<integer> emailContactIndex=emailContactIndexMap.get(con.email);
            if(emailContactIndex==null){
                emailContactIndex=new list<integer>();
            }
            emailContactIndex.add(numContacts);
            emailContactIndexMap.put(con.email,emailContactIndex);
            numContacts++;
        }
        contact newContact;
        voc_dummy_account__c vocDummyAccount=voc_dummy_account__c.getall().values().get(0);
        boolean foundMatch=false;
        boolean newContactCreated=false;
        for(sobject s:scope){
            voc_survey__c survey=(voc_survey__c)s;
            // Search email first, if matching, either matching RCID or not, use Contact's account
            // if no email found then find matching RCID and create new contact.
            // If no matching RCID, TBD
            newContactCreated=false;
            foundMatch=false;
            if(emailContactIndexMap.containskey(survey.customer_email__c)){
                list<integer> emailContactIndex=emailContactIndexMap.get(survey.customer_email__c);
                // Match email and associated RCID
                for(integer index:emailContactIndex){
                    contact contact=matchingContacts.get(index);
                    if(string.isblank(survey.rcid__c)){
                        // No RCID, trust first email (recently created) and skip the rest
                        survey.contact_id__c=contact.id;
                        survey.account__c=contact.accountid;
                        foundMatch=true;
                        break;
                    }else{
                        if(survey.rcid__c.equalsignorecase(contact.account.rcid__c)){
                            // If RCID is the same this is exact match
                            survey.contact_id__c=contact.id;
                            survey.account__c=contact.accountid;
                            foundMatch=true;
                            break;
                        }else{
                            // RCID is different, check their CBUCID
                            if(rcidToAccount.containskey(survey.rcid__c)){
                                if(rcidCbucidMap.containskey(survey.rcid__c)&&rcidCbucidMap.get(survey.rcid__c).equalsignorecase(contact.account.cbucid__c)){
                                    // Same, trust email, no new contact created
                                    survey.contact_id__c=contact.id;
                                    survey.account__c=contact.accountid;
                                    foundMatch=true;
                                    break;
                                }else{
                                    // Different CBUCID or no such CBUCID found in SFDC, create new contact under given RCID
                                    newContact=new contact(firstname=survey.customer_first_name__c,lastname=survey.customer_last_name__c,
                                                           email=survey.customer_email__c,accountid=rcidToAccount.get(survey.rcid__c),phone=survey.customer_phone__c);
                                    newContactCreated=true;
                                    foundMatch=true;
                                    break;
                                }
                            }else{
                                // Given RCID's CBUCID is not found in SFDC at all but email is found, 
                                survey.contact_id__c=contact.id;
                                survey.account__c=contact.accountid;
                                foundMatch=true;
                                break;
                            }
                        }
                    }
                }
                // After all no matching found, create in dummy account
                if(!foundMatch){
                    newContact=new contact(firstname=survey.customer_first_name__c,lastname=survey.customer_last_name__c,
                                           email=survey.customer_email__c,accountid=vocDummyAccount.account_id__c,phone=survey.customer_phone__c);
                    newContactCreated=true;
                    foundMatch=true;
                }
                if(newContactCreated){
                    newContactsMap.put(survey.id,newContact);
                }
            }else{
                if(rcidToAccount.containsKey(survey.rcid__c)){
                    survey.account__c=rcidToAccount.get(survey.rcid__c);
                }
                if(string.isnotblank(survey.account__c)){
                    newContact=new contact(firstname=survey.customer_first_name__c,lastname=survey.customer_last_name__c,
                                           email=survey.customer_email__c,accountid=survey.account__c,phone=survey.customer_phone__c);
                }else{
                    newContact=new contact(firstname=survey.customer_first_name__c,lastname=survey.customer_last_name__c,
                                           email=survey.customer_email__c,accountid=vocDummyAccount.account_id__c,phone=survey.customer_phone__c);
                    survey.account__c=vocDummyAccount.account_id__c;
                }
            	newContactsMap.put(survey.id,newContact);
            }            
            if(string.isnotblank(survey.agent_id__c)){
                String surveyAgentId = survey.agent_id__c.toLowerCase();
                if(agentManagerEmailMap.containskey(surveyAgentId)){
                    survey.manager_ids__c=agentManagerIdMap.get(surveyAgentId);
                    survey.manager_emails__c=agentManagerEmailMap.get(surveyAgentId);
                    survey.agent_id_user__c=agentIdUserIdMap.get(surveyAgentId);
                }else{
                    survey.manager_emails__c=vocSurveyEmailsMap.get(string.valueof(integer.valueof(survey.survey_type__c))).emails__c;
                }
                if(activeAgentIdUserIdMap.get(surveyAgentId)!=null)
                {
                	survey.OwnerId = activeAgentIdUserIdMap.get(surveyAgentId);
            	}
            }else
            {
                survey.manager_emails__c=vocSurveyEmailsMap.get(string.valueof(integer.valueof(survey.survey_type__c))).emails__c;
            }
            
        }
        // Insert new contact first, then update corresponding survey's contact_id__c and account id
        database.saveresult[] results=database.insert(newContactsMap.values(),false);
        for(database.saveresult result:results){
            if(!result.issuccess()){
                for(database.error error:result.geterrors()){
                    string fields=string.join(error.getfields(),',');
                    system.debug('fields:'+fields+', status code:'+error.getstatuscode()+', message:'+error.getmessage());
                }
            }
        }
        for(id surveyId:newContactsMap.keyset()){
            voc_survey__c survey=(voc_survey__c)(scope.get(idIndexMap.get(surveyId)));
            if(survey!=null){
                contact insertedContact=newContactsMap.get(survey.id);
                if(insertedContact!=null&&string.isnotblank(insertedContact.id)){
                    survey.contact_id__c=insertedContact.id;
                    survey.account__c=insertedContact.accountid;
                }
            }
        }
        
        results=database.update(scope,false);
        for(database.saveresult result:results){
            if(!result.issuccess()){
                for(database.error error:result.geterrors()){
                    string fields=string.join(error.getfields(),',');
                    system.debug('fields:'+fields+', status code:'+error.getstatuscode()+', message:'+error.getmessage());
                }
            }
        }
    }
    global void finish(database.batchablecontext bc){}
}