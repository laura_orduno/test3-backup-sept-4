public class AddtoCartItemResponse{
    //public Integer totalSize;
    //public List<AddtoCartItem_messages> messages;
    public List<AddtoCartItem_records> records;
    
   /* public AddtoCartItemResponse(Integer totalSize, List<AddtoCartItem_messages> messages,List<AddtoCartItem_records> records){
      //  this.totalSize = totalSize;
        //this.messages = messages;
        this.records = records;
    }*/
    public AddtoCartItemResponse(List<AddtoCartItem_records> records){
      //  this.totalSize = totalSize;
        //this.messages = messages;
        this.records = records;
    }
    
    
  /*  public class AddtoCartItem_messages{
        public String severity;
        public String messageId;
        public String message;
        public String code;
        public AddtoCartItem_actions actions;
        
        public AddtoCartItem_messages(String severity, String messageId, String message, String code, AddtoCartItem_actions actions){
            this.severity = severity;
            this.messageId = messageId;
            this.message = message;
            this.code = code;
            this.actions = actions;
        }
    }*/
    
    
    public class AddtoCartItem_records{
      
        public AddtoCartItem_ID Id; 
        
        public AddtoCartItem_records(AddtoCartItem_ID Id){
            this.Id = Id;
        }
    }
    
  /*  public class AddtoCartItem_actions{
        public AddtoCartItem_DETAILS DETAILS;
        
        public AddtoCartItem_actions(AddtoCartItem_DETAILS DETAILS){
            this.DETAILS = DETAILS;
        }
    }
    */
  /*  public class AddtoCartItem_DETAILS{
        public AddtoCartItem_rest rest;
        public AddtoCartItem_remote remote;
        public AddtoCartItem_client client;
        
        public AddtoCartItem_DETAILS(AddtoCartItem_rest rest, AddtoCartItem_remote remote, AddtoCartItem_client client){
            this.rest = rest;
            this.remote = remote;
            this.client = client;
        }
    }*/
  /*  public class AddtoCartItem_rest{
        public AddtoCartItem_params params;
        public String method;
        public String link;
        
        public AddtoCartItem_rest(AddtoCartItem_params params, String method, String link){
            this.params = params;
            this.method = method;
            this.link = link;
        }
    }*/
    
   /* public class AddtoCartItem_params{
        public String id;
        public String cartId;
        public String methodName;
        
        public AddtoCartItem_params(String id, String cartId, String methodName){
            this.id = id;
            this.cartId = cartId;
            this.methodName = methodName;
        }
    }*/
    
    public class AddtoCartItem_ID{
        public String value;
        public String label;
        
        public AddtoCartItem_ID(String value,String label){  
            this.value = value;
            this.label = label;
        }
    }
  /*  public class AddtoCartItem_remote{
        public AddtoCartItem_params params;
        
        public AddtoCartItem_remote(AddtoCartItem_params params){
            this.params = params;
        }
    }*/
   /* public class AddtoCartItem_client{
        public AddtoCartItem_params params;
        
        public AddtoCartItem_client(AddtoCartItem_params params){
            this.params= params;
        }
    }*/
    //public List<AddtoCartItem_records>records;
    /*
    public class AddtoCartItem_records{
        public List<AddtoCartItem_messages>
    }
    */
}