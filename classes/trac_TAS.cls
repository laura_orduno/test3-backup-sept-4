public with sharing class trac_TAS {
  
  public static void checkForDuplicates(List<Target_Account_Selling__c> theNewTAS) {
  	
  	Set<String> theNewNames = new Set<String>();
  	for (Target_Account_Selling__c tas : theNewTAS) {
  		if (tas.Name != null) theNewNames.add(tas.Name);
  	}
    
    List<Target_Account_Selling__c> theExistingTAS = [SELECT Id, Name
                                                      FROM Target_Account_Selling__c
                                                      WHERE Name IN :theNewNames];
    for (Target_Account_Selling__c tas : theNewTAS) {
    	for (Target_Account_Selling__c tase : theExistingTAS) {
    		if (tas.Name == tase.Name && tas.Id != tase.Id)
    		  tas.Name.addError('A record with this name already exists. Please choose another name.');
    	}
    }
  }
}