public class trac_TestUtils {

	
	public static Group getGroup(string groupName,string groupType) {
        Group g = new Group(Name=groupName, Type =groupType);
		return g;
	}

	public static Language_Groups__c createLanguageGroup(string groupName) {
        Language_Groups__c lg = new Language_Groups__c(
	    		Name = groupName,
	    		English__c = 'Test - EN',
	    		French__c = 'Test - FR'
	    	);
		insert lg;
		return lg;
	}
	
	
	public static UserRole_Groups__c createUserRoleGroups(string groupName) {
        UserRole_Groups__c urg = new UserRole_Groups__c(
	    		Name = groupName,
	    		UserRole__c = 'Dealer Tom Harris AB Partner Executive',
	    		Group__c = 'Test'
	    	);
		insert urg;
		return urg;
	}
	
}