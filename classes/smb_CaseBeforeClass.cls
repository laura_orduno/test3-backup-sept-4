public class smb_CaseBeforeClass {

    public static void syncParentCaseOwnerField (List<Case> triggerNew, Boolean isUpdate, Boolean isInsert, Map<Id, Case> triggerOldMap) {
        
        // looking to keep ParentId in sync with Parent Case Owner field
        
        Set<Id> caseIds = new Set<Id>();
        
        for (Case c0 : triggerNew) {
            if (isInsert) { 
                if (c0.ParentId != null)
                    if (!caseIds.contains(c0.ParentId))
                        caseIds.add(c0.ParentId);
            } else if (isUpdate) {
                if (c0.ParentId != triggerOldMap.get(c0.Id).ParentId)
                    if (!caseIds.contains(c0.ParentId))
                        caseIds.add(c0.ParentId);       
            }
        }
        
        if (!caseIds.isEmpty()) {
            List<Case> cases = [SELECT OwnerId FROM Case WHERE Id IN : caseIds];
            
            Set<Id> queueOwnedId = new Set<Id>();
            Map<Id, Id> caseOwnerLookupMap = new Map<Id, Id>();
            
            for (Case c1 : cases) {
                String OwnerId = c1.OwnerId;
                if (ownerId.startsWith('00G'))
                    queueOwnedId.add(c1.Id);
                else 
                    caseOwnerLookupMap.put(c1.Id, c1.OwnerId);      
            }
            
            for (Case c2 : triggerNew) {
                if (c2.ParentId == null || queueOwnedId.contains(c2.ParentId))
                    c2.Parent_Case_Owner__c = null;
                else if (c2.ParentId != null && caseOwnerLookupMap.containsKey(c2.ParentId))
                    c2.Parent_Case_Owner__C = caseOwnerLookupMap.get(c2.ParentId);
            }
        }
    }

    public static void establishStartEndTimes(List<Case> triggerNew, Boolean isUpdate, Boolean isInsert, Map<Id, Case> triggerOldMap) {
            
        if (isInsert) {
            for (Case c0 : triggerNew) {
                
                c0.Current_Owner_Start_Time__c = System.now();
                c0.Current_Status_Start_Time__c = System.now();
                
            }   
        }
    }   
}