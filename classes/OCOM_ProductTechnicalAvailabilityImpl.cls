/*
 * 27 March 2017 - Defect # 11733 - Added by Sandip - Checked null value in dropExceptionCode
 */
global without sharing class OCOM_ProductTechnicalAvailabilityImpl {
    public DropStatus dropStatus {get;set;}

    // Thomas OCOM-1041
    public List<ServicePath> servicePathList{get;set;}
    //Updated by Arvind as part of US-1138
    public OCOM_InOrderFlowPACController controllerRef;
    public List<ProductCategory> productCategoriesTop{get;set;} // added to render to vf
    //Updated by Arvind as part of US-1138
    public OC_SelectAndNewAddressController newAddressRef;
    public OCOM_ProductTechnicalAvailabilityImpl(OCOM_InOrderFlowPACController controller) {
        this.controllerRef=controller;
        this.productCategoriesTop= new List<ProductCategory>(); // added to render to vf
    }
    
    public OCOM_ProductTechnicalAvailabilityImpl(OC_SelectAndNewAddressController controller) {
        this.newAddressRef = controller;
        this.productCategoriesTop= new List<ProductCategory>(); // added to render to vf
    }
   
    public OCOM_ProductTechnicalAvailabilityImpl(OCOM_OutOrderFlowPACController controller) {
        controllerRef = null;
        this.productCategoriesTop= new List<ProductCategory>(); // added to render to vf
    } 
    
    private static final Map<String, String> iconMap;
    private static final Map<String, String> categoryMap;
    static {
        iconMap = new Map<String, String>();
        iconMap.put('INTERNET', 'icon-mouse');
        iconMap.put('VOICE', 'icon-homephone');
        iconMap.put('MOBILE', 'icon-smartphone');
        iconMap.put('TV', 'icon-tv');
        iconMap.put('WiFi', 'icon-wifi');
        
        categoryMap = new Map<String, String>();
        categoryMap.put('TELUS Internet Product', 'INTERNET');
        categoryMap.put('TELUS Office Phone Product', 'VOICE');
        categoryMap.put('TELUS Wireless Product', 'MOBILE');
        categoryMap.put('TELUS TV Product', 'TV');
        categoryMap.put('TELUS Public Wi-Fi Product', 'WiFi');
        categoryMap.put('TELUS TV Product (v2)', 'TV');
    }

    public class ProductCategory {
        public String type {get;set;} 
        public String availability {get;set;} 
        public String icon {get;set;} 
        public String internet_type {get;set;}
        public List<Characteristic> characteristics  {get;set;} 
    }
    
    public class Characteristic implements Comparable {
        public String name {get;set;} 
        public String value {get;set;} 
        public Integer displaySequence {get;set;} 
        
        // Implement the compareTo() method
        public Integer compareTo(Object compareTo) {
            Characteristic characteristic = (Characteristic)compareTo;
            if (displaySequence == characteristic.displaySequence) return 0;
            if (displaySequence > characteristic.displaySequence) return 1;
            return -1;       
        }        
    }
    
    // Roderick OCOM-1041 Start
    public class ServicePath {
        public String circuitNumber {get;set;}
        public String name {get;set;}
        public String clearanceDate {get;set;}
        public String serviceType {get;set;}
        public String statusCode {get;set;} 
        public String lineDesignation {get;set;} 
        public String leasedLoop {get;set;} 
    }    
    // Roderick OCOM-1041 End
    
    public class DropStatus {
        
        public String fsa {get;set;}
        public String dropType {get;set;}
        public String dropPermission {get;set;}
        public String dropStatus {get;set;}
        public String fsaReadyDate {get;set;}
        public String coid {get;set;}
        public String gisId {get;set;}
        public String roeStatus {get;set;}
        public String premiseType {get;set;}
    }
    
    
    public PageReference refreshAvailableProduct(){
        productCategoriesTop = new List<ProductCategory>();
        return null;
    }

    public PageReference getGponDropStatus() {

        try {
            String serviceAddressId = apexpages.currentpage().getparameters().get('serviceAddressId');
            
            OrdrTechnicalAvailabilityManager.GponDropStatus gponDropStatus = OrdrTechnicalAvailabilityManager.getGponDropStatus(serviceAddressId);
            
            system.debug('getGponDropStatus:' + gponDropStatus);
            
            dropStatus = new DropStatus();
            if (gponDropStatus != null) {
                dropStatus.fsaReadyDate = gponDropStatus.fsaReadyDate;
                dropStatus.dropStatus   = gponDropStatus.dropStatus;
                dropStatus.fsa 			= gponDropStatus.fsa;
                dropStatus.dropType		= gponDropStatus.dropType;
                dropStatus.dropPermission = gponDropStatus.dropPermission;
                dropStatus.coid 		= gponDropStatus.coid;
                dropStatus.gisId 		= gponDropStatus.gisId;
                dropStatus.roeStatus 	= gponDropStatus.roeStatus;
                dropStatus.premiseType	= gponDropStatus.premiseType; 
                
                if (String.isNotBlank(gponDropStatus.dropType)) {
                    if (gponDropStatus.dropType == 'A') dropStatus.dropType = 'Aerial';
                    if (gponDropStatus.dropType == 'B') dropStatus.dropType = 'Buried';
                    if (gponDropStatus.dropType == 'U') dropStatus.dropType = 'Underground';
                    if (gponDropStatus.dropType == 'T') dropStatus.dropType = 'Temporary';
                    if (gponDropStatus.dropType == 'P') dropStatus.dropType = 'Proposed';
                }               
                
                if (String.isNotBlank(gponDropStatus.premiseType)) {
                    if (gponDropStatus.premiseType == 'MDU') dropStatus.premiseType = 'Multi-Dwelling Unit';
                    if (gponDropStatus.premiseType == 'SDU') dropStatus.premiseType = 'Single-Dwelling Unit';
                    if (gponDropStatus.premiseType == 'MBU') dropStatus.premiseType = 'Multi-Business Unit';
                    if (gponDropStatus.premiseType == 'SBU') dropStatus.premiseType = 'Single-Business Unit';
                    if (gponDropStatus.premiseType == 'SXU') dropStatus.premiseType = 'SXU';
                }
            }
            
        } catch (Exception e) {
            if(controllerRef != null) {
                controllerRef.errorMessages.add(e.getMessage());
            }
            if(newAddressRef != null){
                newAddressRef.errorMessages.add(e.getMessage());  
            }
        }
        return null;
    }

    
    public PageReference getProductsTechnicalAvailability() {
        System.debug('OCOM_ProductTechnicalAvailabilityImpl::getProductsTechnicalAvailability() start');
        try {

            String smbCareAddrId=apexpages.currentpage().getparameters().get('smbCareAddressGTAId');
            System.debug('OCOM_ProductTechnicalAvailabilityImpl::getProductsTechnicalAvailability() smbCareAddressGTAId('+smbCareAddrId+')');
            //QC-7757: Aruna: Added below if condition.
            if(smbCareAddrId == NULL || smbCareAddrId == ''){
                smbCareAddrId = apexpages.currentpage().getparameters().get('smbCareAddrId');               
            }
            System.debug('OCOM_ProductTechnicalAvailabilityImpl::getProductsTechnicalAvailability() smbCareAddrId('+smbCareAddrId+')');

            productCategoriesTop = new List<ProductCategory>();
            List<OrdrTechnicalAvailabilityManager.ProductCategory> products = OrdrTechnicalAvailabilityManager.checkProductTechnicalAvailability(smbCareAddrId);
            System.debug('OCOM_ProductTechnicalAvailabilityImpl::getProductsTechnicalAvailability() products('+products+')');
            
            Map<String, ProductCategory> categoriesMap = new Map<String, ProductCategory>();
            
            Boolean isCharactersticsUnconfirmedAdded = false;
            for (OrdrTechnicalAvailabilityManager.ProductCategory product : products) {
                
                ProductCategory category = new ProductCategory();
                if (categoryMap.containsKey(product.category)) {
                    String categoryKey = categoryMap.get(product.category);
                    category = new ProductCategory();
                    if (categoriesMap.containsKey(categoryKey)) {
                        category = categoriesMap.get(categoryKey);
                    } else {
                        category.type = categoryMap.get(product.category);
                        category.availability = product.availability;
                        category.icon = iconMap.get(category.type);
                    }
                    
                    List<Characteristic> characteristics = new List<Characteristic>();
                    if (category.type.equals('INTERNET')) {
	                    Characteristic characteristic = new Characteristic();
                        if (String.isNotBlank(product.accessType)) {
                        if (product.accessType.equals('Fibre - FIFA')) {
                        	characteristic.name = 'PureFibre (FIFA)';
                            characteristic.value = '1G+ Mbps';
                            characteristic.displaySequence = 1;
	                        characteristics.add(characteristic);
                        } else if (product.accessType.equals('Fibre - Non-FIFA')) {
                        	characteristic.name = 'PureFibre';
                            characteristic.value = '150+ Mbps';
                            characteristic.displaySequence = 2;
                            characteristics.add(characteristic);
                        } else if (product.accessType.equals('Bonded Copper')) {
                            characteristic.name = product.accessType;
                            characteristic.value = product.downloadSpeed;
                            characteristic.displaySequence = 3;
                            characteristics.add(characteristic);
                            if (String.isNotBlank(product.ports)) {
                                characteristic = new Characteristic();
                                characteristic.name = 'Available Ports';
                                characteristic.value = product.ports;
                            	characteristic.displaySequence = 4;
                                characteristics.add(characteristic);
                            }
	                    } else if (product.accessType.equals('Copper')) {
                            characteristic.name = product.accessType;
                            characteristic.value = product.downloadSpeed;
                            characteristic.displaySequence = 5;
	                        characteristics.add(characteristic);
                            if (String.isNotBlank(product.ports)) {
                                characteristic = new Characteristic();
                                characteristic.name = 'Available Ports';
                                characteristic.value = product.ports;
                            	characteristic.displaySequence = 6;
                                characteristics.add(characteristic);
                            }
                            } else {
                                characteristic.name = 'PureFibre';
                                characteristic.value = 'unconfirmed';
                                characteristic.displaySequence = 7;
                                characteristics.add(characteristic);
                                characteristic = new Characteristic();
                                characteristic.name = 'Copper';
                                characteristic.value = 'unconfirmed';
                                characteristic.displaySequence = 8;
                                characteristics.add(characteristic);
                            }
                        } else {
                            characteristic.name = 'PureFibre';
                            characteristic.value = 'unconfirmed';
                            characteristic.displaySequence = 7;
	                        characteristics.add(characteristic);
                            characteristic = new Characteristic();
                            characteristic.name = 'Copper';
                            characteristic.value = 'unconfirmed';
                            characteristic.displaySequence = 8;
	                        characteristics.add(characteristic);
                        }
                        
                    }

                    if (category.characteristics == null) category.characteristics = new List<Characteristic>();
                    category.characteristics.addAll(characteristics);
                    category.characteristics.sort();
                    
                    categoriesMap.put(categoryKey, category);
                
                }
                
            }
            
            productCategoriesTop = categoriesMap.values();
            System.debug('OCOM_ProductTechnicalAvailabilityImpl::getProductsTechnicalAvailability() productCategoriesTop('+productCategoriesTop+')');
            if(newAddressRef != null){
                newAddressRef.blnGTACall = True; 
            }
            if(controllerRef != null){
            controllerRef.blnGTACall = True;
            }
            
            //Asynchronous call
            OrdrProductEligibilityManager.cacheAvailableOffers(smbCareAddrId);
            
        } catch (Exception e) {
            if(controllerRef != null) {
                controllerRef.errorMessages.add(e.getMessage());
            }
            if(newAddressRef != null){
                newAddressRef.errorMessages.add(e.getMessage());  
            }
            System.debug('In Exception '+e.getStackTraceString()+')');
        }
        System.debug('OCOM_ProductTechnicalAvailabilityImpl::getProductsTechnicalAvailability() productCategoriesTop('+productCategoriesTop+')');
        return null;
    }
    
    public PageReference getRateGroupAndForbearanceInfo() {

        try {
            String serviceAddressId = apexpages.currentpage().getparameters().get('serviceAddressId');
            
            OrdrProductEligibilityManager.retrieveRateGroupInfo(serviceAddressId);
            
        } catch (Exception e) {
            if(controllerRef != null) {
                controllerRef.errorMessages.add(e.getMessage());
            }
            if(newAddressRef != null){
                newAddressRef.errorMessages.add(e.getMessage());  
            }
        }
        return null;
    }


    
    // Thomas OCOM-1041 Start
    public PageReference getServicePath(){
        try {
            System.debug('OCOM_ProductTechnicalAvailabilityImpl::getServicePath() start');

            String smbCareAddrId=apexpages.currentpage().getparameters().get('smbCareAddrId');
            System.debug('OCOM_ProductTechnicalAvailabilityImpl::getServicePath() smbCareAddrId('+smbCareAddrId+')');
            servicePathList = new List<ServicePath>();
            List<OrdrTechnicalAvailabilityManager.ServicePath> servicePaths = OrdrTechnicalAvailabilityManager.getServicePath(smbCareAddrId);
            System.debug('OCOM_ProductTechnicalAvailabilityImpl::getServicePath() servicePaths('+servicePaths+')');
            
            for (OrdrTechnicalAvailabilityManager.ServicePath aServicePath: servicePaths) {
                System.debug('OCOM_ProductTechnicalAvailabilityImpl::getServicePath() aServicePath('+aServicePath+')');
                ServicePath servicePath = new ServicePath();
                servicePath.circuitNumber = aServicePath.primaryTelephoneNumber;
                servicePath.name = aServicePath.customerName;
                servicePath.clearanceDate = aServicePath.clearanceDate;
                servicePath.serviceType = aServicePath.serviceTypeCode;
                servicePath.statusCode = aServicePath.servicePathStatusCode;
                servicePath.lineDesignation = aServicePath.cablePairDedicatedPlantCode;
                servicePath.leasedLoop = aServicePath.leasedLoopFlag;
                System.debug('OCOM_ProductTechnicalAvailabilityImpl::getServicePath() servicePath('+servicePath+')');
                servicePathList.add(servicePath);
            }           

            System.debug('OCOM_ProductTechnicalAvailabilityImpl::getServicePath() servicePathList('+servicePathList+')');
        } catch (Exception e) {
            if(controllerRef != null) {
                controllerRef.errorMessages.add(e.getMessage());
            }
            if(newAddressRef != null){
                newAddressRef.errorMessages.add(e.getMessage());  
            }
        }
        return null;
    }
    // Thomas OCOM-1041 End
    
}