/**
 * Temporary ForborneWS class that encapsulates all integration in this one class.  A better
 * integration framework should replace this class in the future.
 */
public class ForborneWS{
    @future(callout=true)
    public static void getForborneStatusByNpaNxxList(id srsServiceAddressId){
        map<string,schema.recordtypeinfo> recordTypeInfoMap=webservice_integration_error_log__c.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        webservice_integration_error_log__c log=new webservice_integration_error_log__c(recordtypeid=recordTypeInfoMap.get(label.webservice_integration_log_success).getrecordtypeid(),object_name__c='SRS_Service_Address__c',external_system_name__c='Forborne',sfdc_record_id__c=srsServiceAddressId);
        try{
            srs_service_address__c serviceAddress=[select id,npa_nxx__c,forborne__c from srs_service_address__c where id=:srsServiceAddressId];
            //TODO - Use Custom Label to parse and pass the XML
            httpresponse resp=ForborneWS.send('');
            //TODO - Parse response to get result or error code
            string responseBody=resp.getBody();
            if(responseBody.containsignorecase('<exchangeForborneStatusInd>true</exchangeForborneStatusInd>')){
                serviceAddress.forborne__c=label.forborne_yes;
            }else if(responseBody.containsignorecase('<exchangeForborneStatusInd>false</exchangeForborneStatusInd>')){
                serviceAddress.forborne__c=label.forborne_no;
            }else{
                serviceAddress.forborne__c='';
                log.recordtypeid=recordTypeInfoMap.get(label.webservice_integration_log_success).getrecordtypeid();
                log.error_code_and_message__c=responseBody;
            }
        }catch(queryexception qe){
            log.recordtypeid=recordTypeInfoMap.get(label.webservice_integration_log_failure).getrecordtypeid();
            log.error_code_and_message__c=qe.getmessage();
            log.stack_trace__c=qe.getstacktracestring();
        }catch(calloutexception ce){
            log.recordtypeid=recordTypeInfoMap.get(label.webservice_integration_log_failure).getrecordtypeid();
            log.error_code_and_message__c=ce.getmessage();
            log.stack_trace__c=ce.getstacktracestring();
        }catch(exception e){
            log.recordtypeid=recordTypeInfoMap.get(label.webservice_integration_log_failure).getrecordtypeid();
            log.error_code_and_message__c=e.getmessage();
            log.stack_trace__c=e.getstacktracestring();
        }finally{
            try{
                log.error_code_and_message__c=(string.isnotblank(log.error_code_and_message__c)&&log.error_code_and_message__c.length()>32767?log.error_code_and_message__c.substring(0,32767):log.error_code_and_message__c);
                log.stack_trace__c=(string.isnotblank(log.stack_trace__c)&&log.stack_trace__c.length()>32767?log.stack_trace__c.substring(0,32767):log.stack_trace__c);
                insert log;
            }catch(exception e){
                Util.emailSystemError(e);
            }
        }
    }
    /**
     * Separating the transmission call to fulfill Apex Test purposes using either
     * mock interface or static resources.
     */
    public static httpresponse send(string body){
        //TODO - Use Https POST request directly instead of generated Apex stub
        httprequest forborneRequest=new httprequest();
        forborneRequest.setendpoint('');
        forborneRequest.setmethod('POST');
        forborneRequest.settimeout(5000);
        forborneRequest.setbody(body);
        http h=new http();
        httpresponse response=h.send(forborneRequest);
        return response;
    }
}