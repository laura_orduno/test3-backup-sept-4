/*
    To start this batch:
    Id batchInstanceId = Database.executeBatch(new QuoteExpireAgreementsBatchable(), 9);
*/

global class QuoteExpireAgreementsBatchable implements Database.Batchable<sObject>, Database.AllowsCallouts {
    global Database.QueryLocator start(Database.BatchableContext bc) {
        System.debug('\r\n\r\nExecuting Batchable Job: QuoteExpireAgreements\r\n');
        String query = 'Select Id, Name From Agreement__c Where Status__c = \'' + QuoteAgreementStatus.PENDING + '\' and Expiry_Date__c <= TODAY';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<sObject> scope) {
        System.debug('\r\nFound agreements:\r\n');
        System.debug(scope);
        Id[] targetIds = new List<Id>();
        for (sObject s : scope) { targetIds.add(s.Id); }
        Boolean result = QuoteAgreementUtilities.expireAgreements(targetIds, QuoteAgreementStatus.EXPIRED);
        System.debug('\r\nThis execute batch returned ' + result);
    }

    global void finish(Database.BatchableContext bc) {
        // cleanup, email alert, stats, etc
    }

}