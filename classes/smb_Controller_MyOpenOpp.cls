public class smb_Controller_MyOpenOpp {

 
 public list<Opportunity> lstOpp{get{
    return sortList(lstOpp);
 } set;}
 
 public String sortBy { get; set; }
 public String sortDir { get; set; }
 
 public smb_Controller_MyOpenOpp(){
            
        lstOpp = [select id,createddate,name,stagename,type,Accname__c,Order_ID__c,
                  Quote_id__c,Expiry_Date__c,Quote_Expiration_Date__c,LastModifiedDate,Product_Type__c,
                  Order_Type__c,Requested_Due_Date__c,Contract_Acceptance_Expired_Date__c,credit_request_status__c, Credit_Assessment__c    
                  from Opportunity
                  where 
                  (ownerid =: Userinfo.getUserId() and recordtype.name='SMB Care Opportunity' and stageName != 'Order Request Completed'
                   and stageName != 'Order Request Cancelled' and stageName != 'Quote Cancelled' and 
                   stageName != 'Quote Expired' and stageName != 'Contract Acceptance Expired') or
                  (ownerid =: Userinfo.getUserId() and recordtype.name='SMB Care Opportunity' and stageName = 'Order Request Cancelled' and Order_Request_Cancelled_Date__c >= LAST_N_DAYS:6) or
                  (ownerid =: Userinfo.getUserId() and recordtype.name='SMB Care Opportunity' and stageName= 'Quote Cancelled' and Quote_Cancelled_Date__c >= LAST_N_DAYS:6) or 
                  (ownerid =: Userinfo.getUserId() and recordtype.name='SMB Care Opportunity' and stageName= 'Quote Expired' and Quote_Expiration_Date_date_field__c >= LAST_N_DAYS:6) or
                  (ownerid =: Userinfo.getUserId() and recordtype.name='SMB Care Opportunity' and stageName= 'Contract Acceptance Expired' and Contract_Acceptance_Expired_Date__c >= LAST_N_DAYS:6)
                  or
                  (ownerid =: Userinfo.getUserId() and recordtype.name='SMB Care Amend Order Opportunity' and stageName != 'Order Request Completed'
                   and stageName != 'Order Request Cancelled' and stageName != 'Quote Cancelled' and 
                   stageName != 'Quote Expired' and stageName != 'Contract Acceptance Expired') or
                  (ownerid =: Userinfo.getUserId() and recordtype.name='SMB Care Amend Order Opportunity' and stageName = 'Order Request Cancelled' and Order_Request_Cancelled_Date__c >= LAST_N_DAYS:6) or
                  (ownerid =: Userinfo.getUserId() and recordtype.name='SMB Care Amend Order Opportunity' and stageName= 'Quote Cancelled' and Quote_Cancelled_Date__c >= LAST_N_DAYS:6) or 
                  (ownerid =: Userinfo.getUserId() and recordtype.name='SMB Care Amend Order Opportunity' and stageName= 'Quote Expired' and Quote_Expiration_Date_date_field__c >= LAST_N_DAYS:6) or
                  (ownerid =: Userinfo.getUserId() and recordtype.name='SMB Care Amend Order Opportunity' and stageName= 'Contract Acceptance Expired' and Contract_Acceptance_Expired_Date__c >= LAST_N_DAYS:6)
                  or
                  (ownerid =: Userinfo.getUserId() and recordtype.name='SMB Care Locked Order Opportunity' and stageName != 'Order Request Completed'
                   and stageName != 'Order Request Cancelled' and stageName != 'Quote Cancelled' and 
                   stageName != 'Quote Expired' and stageName != 'Contract Acceptance Expired') or
                  (ownerid =: Userinfo.getUserId() and recordtype.name='SMB Care Locked Order Opportunity' and stageName = 'Order Request Cancelled' and Order_Request_Cancelled_Date__c >= LAST_N_DAYS:6) or
                  (ownerid =: Userinfo.getUserId() and recordtype.name='SMB Care Locked Order Opportunity' and stageName= 'Quote Cancelled' and Quote_Cancelled_Date__c >= LAST_N_DAYS:6) or 
                  (ownerid =: Userinfo.getUserId() and recordtype.name='SMB Care Locked Order Opportunity' and stageName= 'Quote Expired' and Quote_Expiration_Date_date_field__c >= LAST_N_DAYS:6) or
                  (ownerid =: Userinfo.getUserId() and recordtype.name='SMB Care Locked Order Opportunity' and stageName= 'Contract Acceptance Expired' and Contract_Acceptance_Expired_Date__c >= LAST_N_DAYS:6)
                   order by Requested_Due_Date__c ASC];

        //system.debug('@@@@lstOpp : '+lstOpp);
        
    }
    
    public PageReference empty() { return null; }
    
    public List<SObject> sortList(List<SObject> cleanList){
        
        //system.debug('@@@@@cleanList : '+cleanList);
        //system.debug('@@@@@sortBy : '+sortBy);
        
        if (sortBy == null) { return cleanList; }
        
        List<SObject> resultList = new List<SObject>();
        Map<Object, List<SObject>> objectMap = new Map<Object, List<SObject>>();
        
        for (SObject item : cleanList) {
          //system.debug('@@@@@item:'+item);
          
          if (objectMap.get(item.get(sortBy)) == null) {
            objectMap.put(item.get(sortBy), new List<SObject>());
          }
          objectMap.get(item.get(sortBy)).add(item);
        }
        
        List<Object> keys = new List<Object>(objectMap.keySet());
        keys.sort();
        
        for(Object key : keys) {
          resultList.addAll(objectMap.get(key));
        }
        
        cleanList.clear();
        
        if (sortDir == 'ASC') {
          for (SObject item : resultList) {
            cleanList.add(item);
          }
        } else {
          for (Integer i = resultList.size()-1; i >= 0; i--) {
            cleanList.add(resultList[i]);
          }
        }
        
        return cleanList;
  }
  
    
 
}