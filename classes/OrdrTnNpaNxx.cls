/**
 * @author Santosh Rath
 * 
 **/
public virtual class OrdrTnNpaNxx{
   // npa (i.e. first 3 digits of a TN representing the area code)
	public String npa = null;
	// nxx (i.e. 3 digits following the Npa - 4th, 5th and 6th digits of a TN)
	public String nxx = null;
	
	// servingCoid (i.e. Identifier of the CO serving the above OrdrTnNpaNxx- info originally provided by the OrdrTnServiceAddress details from FMS while validating a SA)
	// Note that the servingCoid could differ from the SA's COID if the agent manually entered a non-native Nxx and ASF getCOIDByOrdrTnNpaNxxwas invoked
	// - info provided by ASF getNativeNpaNxxByCOIDAndLocation or getCOIDByOrdrTnNpaNxx- InventoryService
	public String servingCoid = null;
	// switchNumber (i.e. Identifier of the switch - info provided by ASF getNativeNpaNxxByCOIDAndLocation or getCOIDByOrdrTnNpaNxx- InventoryService)
	public String switchNumber = null;

	// defaultInd (identifies the OrdrTnNpaNxxwhich should be selected by default in the UI)
	private boolean defaultInd = false;
	// native OrdrTnNpaNxx(identifies if the OrdrTnNpaNxxis originally part of the native OrdrTnNpaNxxlist By COID and location)
	public boolean nativeNpaNxxInd = false;

	public String salesforceId;

	public OrdrTnNpaNxx() {
    }

    public OrdrTnNpaNxx(
            String npa,
            String nxx) {
            this.npa = npa;
            this.nxx = nxx;
            // Assume the default value for the defaultInd is false, set the switchNumber to '000'
     }
    
    public OrdrTnNpaNxx(
            String npa,
            String nxx,
            String servingCoid,
            String switchNumber) {
    		this(npa, nxx);
            this.servingCoid = servingCoid;
            this.switchNumber = switchNumber;
            // Assume the default value for the defaultInd is false
     }
    
  //Left just to give some time for that class to use another OrdrTnNpaNxxconstructor - missing the servingCoid
  //TNReservationAction
  //We create a new OrdrTnNpaNxxobject build out of data entered/selected on UI and add it to the list.
  //retVal = new OrdrTnNpaNxx(getNpa(), getNxx(), EMPTY_STRING, boolean.TRUE);    
    
    public OrdrTnNpaNxx(
            String npa,
            String nxx,
            String switchNumber,
            boolean defaultInd) {
    	this(npa, nxx, '', switchNumber);
        this.defaultInd = defaultInd;
     }
    
    public OrdrTnNpaNxx(
            String npa,
            String nxx,
            String servingCoid,
            String switchNumber,
            boolean defaultInd) {
    	this(npa, nxx, servingCoid, switchNumber);
        this.defaultInd = defaultInd;
     }

    public OrdrTnNpaNxx(
            String npa,
            String nxx,
            String servingCoid,
            String switchNumber,
            boolean defaultInd,
            boolean nativeNpaNxxInd) {
    	this(npa, nxx, servingCoid, switchNumber, defaultInd);
        this.nativeNpaNxxInd = nativeNpaNxxInd;
     }

	/**
	 * @return the npa
	 */
	public String getNpa() {
		return npa;
	}

	/**
	 * @param npa the npa to set
	 */
	public void setNpa(String npa) {
		this.npa = npa;
	}

	/**
	 * @return the nxx
	 */
	public String getNxx() {
		return nxx;
	}

	/**
	 * @param nxx the nxx to set
	 */
	public void setNxx(String nxx) {
		this.nxx = nxx;
	}

	/**
	 * @return the servingCoid
	 */
	public String getServingCoid() {
		return servingCoid;
	}

	/**
	 * @param servingCoid the servingCoid to set
	 */
	public void setServingCoid(String servingCoid) {
		this.servingCoid = servingCoid;
	}
	
	/**
	 * @return the switchNumber
	 */
	public String getSwitchNumber() {
		return switchNumber;
	}

	/**
	 * @param switchNumber the switchNumber to set
	 */
	public void setSwitchNumber(String switchNumber) {
		this.switchNumber = switchNumber;
	}

	/**
	 * @return the defaultInd
	 */
	public boolean getDefaultInd() {
		return defaultInd;
	}

	/**
	 * @param defaultInd the defaultInd to set
	 */
	public void setDefaultInd(boolean defaultInd) {
		this.defaultInd = defaultInd;
	}    

	/**
	 * @return the nativeNpaNxxInd
	 */
	public boolean getNativeNpaNxxInd() {
		return nativeNpaNxxInd;
	}

	/**
	 * @param nativeNpaNxxInd the nativeNpaNxxInd to set
	 */
	public void setNativeNpaNxxInd(boolean nativeNpaNxxInd) {
		this.nativeNpaNxxInd = nativeNpaNxxInd;
	}

	/**
	 * @return the concatenation of: npa and nxx
	 */
	public String getSixDigitsNpaNxx() {
		return npa + nxx;
	}
	override
	public String toString() {
		return 'Npa-Nxx: '+this.getNpa()+'-'+this.getNxx()+' coid: '+this.getServingCoid()+' switch: '+this.getSwitchNumber()+' Default: '+this.getDefaultInd()+' native: '+this.getNativeNpaNxxInd();
	} 

	public void setSalesforceId(String salesforceId){
		this.salesforceId=salesforceId;
	}
	public String getSalesforceId(){
		return this.salesforceId;
	}
}