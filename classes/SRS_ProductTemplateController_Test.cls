/*******************************************************************************
 * Project      : SRS R3
 * Class        : SRS_ProductTemplateController_Test
 * Description  : Test class for SRS ProductTemplate Controller class
 * Author       : Aditya Jamwal, IBM
 * Date         : July-15-2014
 *
 * Modification Log:
 * ------------------------------------------------------------------------------
 * Ver      Date                Modified By           Description
 * ------------------------------------------------------------------------------
 * 
 * ------------------------------------------------------------------------------
 *******************************************************************************/

@isTest
private class SRS_ProductTemplateController_Test {

    static testMethod void SRS_ProductTemplateController_test() {
    List<SRS_PODS_Template__c> QuestionTempList = new List<SRS_PODS_Template__c>();
    List<SRS_PODS_Answer__c> AnswerList = new List<SRS_PODS_Answer__c>();
        SRS_Test_Data_Utility testData= new SRS_Test_Data_Utility();
        testData.createDataFor_SRS_ProductTemplateController();
        QuestionTempList =testData.QuestionTempList;
        
        System.assertEquals(7,QuestionTempList.size());
        
        AnswerList=testData.AnswerList;
        
        System.assertEquals(6,AnswerList.size());
        
        ApexPAges.StandardController sc = new ApexPages.StandardController(testData.sereq);
        SRS_ProductTemplateController src= new SRS_ProductTemplateController(sc);
        Test.setCurrentPageReference(new PageReference('Page.SRS_ProductTemplate')); 
        System.currentPageReference().getParameters().put('indexKey', '4');
        
        src.productname='ISDN PRI';
        src.OrderType='install';
        src.populateForm();        
        src.setQuestionWrapperbyQId();
        src.processChild();
        src.SaveTemplate();
        src.PopulateRepeatablequestSet();
        src.SaveRepeatablequestSet();
        src.Addrow();
        src.DeleteRow();
        src.setQuestionCount();
       
        SRS_ProductTemplateController.QuestionWrapper QS = new SRS_ProductTemplateController.QuestionWrapper();
        QS.CloneQuestionWrapper(QS);
        List<SRS_ProductTemplateController.QuestionWrapper> QSlist = new List<SRS_ProductTemplateController.QuestionWrapper>();
        QSlist.add(QS);
         src.sortQuestionWrapperListWithQuestionID(QSlist);
         
         List<SRS_ProductTemplateController.QuestionWrapper> QSlist1 = new List<SRS_ProductTemplateController.QuestionWrapper>();
        SRS_ProductTemplateController.QuestionWrapper QS1 = new SRS_ProductTemplateController.QuestionWrapper();
        QS1.isMandatory=true;
        QS1.onScreen=true;
        QS1.questionValue='';
        src.renderRepeatable=true;
        QSlist1.add(QS1);
        src.RQuesSet=new List<List<SRS_ProductTemplateController.QuestionWrapper>>();
        src.fieldlist=new List<SRS_ProductTemplateController.QuestionWrapper>();
        src.RQuesSet.add(QSlist1);         
         src.fieldlist=QSlist1;
         src.saveTemplate();
         src.existingPODSAnswer.clear();
         src.saveTemplate();
         QS1.questionValue='test123test123test123';
         for(integer i=0;i<4;i++)
         QS1.questionValue=QS1.questionValue+QS1.questionValue;  
         QS1.questionText='qtext';       
         QS1.Type='T';        
         src.saveTemplate();
         src.existingPODSAnswer=AnswerList;
         src.saveTemplate();
         
         src.selectedProduct='test';
         src.selecterddProductType='test';
         testData.sereq=[select id,SRS_PODS_Product__c,SRS_PODS_Product__r.name from Service_request__c where id=:testData.sereq.id];
         ApexPAges.StandardController sc1 = new ApexPages.StandardController(testData.sereq);
         SRS_ProductTemplateController src1= new SRS_ProductTemplateController(sc1);
         src1.populateForm();
         
         map<decimal, string> mapTest=new map<decimal,string>();
         mapTest.put(123.00,'123.00');
         mapTest.put(124.00,'test1');
         mapTest.put(125.00,'125.00');
         System.currentPageReference().getParameters().put('indexKey', '123.00');
        
         src1.getParentChildRecs.put(123.00,mapTest);
         src1.getParentChildRecs.put(124.00,mapTest);
         src1.getParentChildRecs.put(125.00,mapTest);
         SRS_ProductTemplateController.QuestionWrapper ques=new  SRS_ProductTemplateController.QuestionWrapper();
          ques.systemQuestionID=123.00;
          ques.type='T';
          ques.questionValue='123.00';
          
          SRS_ProductTemplateController.QuestionWrapper ques1=new  SRS_ProductTemplateController.QuestionWrapper();
          ques1.systemQuestionID=124.00;
          ques1.type='R';
          ques1.questionValue='124.00';
          
          SRS_ProductTemplateController.QuestionWrapper ques2=new  SRS_ProductTemplateController.QuestionWrapper();
          ques2.systemQuestionID=125.00;
          ques2.type='R';
          ques2.questionValue='125.00';
          
                    
          src1.QuestionWrapperbyQIdMap.put(123.00,ques);
          src1.QuestionWrapperbyQIdMap.put(124.00,ques1);
          src1.QuestionWrapperbyQIdMap.put(125.00,ques2);
          
        src1.processChild();
        List<SRS_PODS_Answer__c> SAList= new List<SRS_PODS_Answer__c>();
        SRS_PODS_Answer__c SA1 = new SRS_PODS_Answer__c(GroupSerial__c=1,FOX_PARAMETER__c='sads',SRS_Question_ID__c=1,Service_Request__c=testData.sereq.id,SRS_PODS_Products__c=QuestionTempList[0].id,SRS_PODS_Answer__c='asda');
        SRS_PODS_Answer__c SA2 = new SRS_PODS_Answer__c(GroupSerial__c=1,FOX_PARAMETER__c='sads',SRS_Question_ID__c=2,Service_Request__c=testData.sereq.id,SRS_PODS_Products__c=QuestionTempList[1].id,SRS_PODS_Answer__c='asda');
        SAList.add(SA1);
        SAList.add(SA2);
         insert SAList;
         src.PopulateRepeatablequestSet();
         src.RQues= new SRS_ProductTemplateController.RepeatableQSet();
         src.RQues.QuesSet = new List<List<SRS_ProductTemplateController.QuestionWrapper>>();
         SRS_ProductTemplateController.QuestionWrapper QS4 = new SRS_ProductTemplateController.QuestionWrapper();
         QS4.isMandatory=true;
        
         QS4.onScreen=true;
         QS4.questionValue='test123test123test123';
         for(integer i=0;i<4;i++)
         QS4.questionValue=QS4.questionValue+QS4.questionValue;  
         QS4.systemQuestionID=1;
         QS4.GroupNumber=0;
         QS4.questionText='qtext';
         QS4.Type='T';
         QSlist1.add(QS4); 
         SRS_ProductTemplateController.QuestionWrapper QS0 = new SRS_ProductTemplateController.QuestionWrapper();
         QS0.isMandatory=true;
        
         QS0.onScreen=true;
         QS0.questionValue='test123test123test123';
         for(integer i=0;i<4;i++)
         QS0.questionValue=QS0.questionValue+QS0.questionValue;  
         QS0.systemQuestionID=1;
         QS0.GroupNumber=0;
         QS0.questionText='qtext';
         QS0.Type='T';
         QSlist1.add(QS0); 
         
         SRS_ProductTemplateController.QuestionWrapper QS3 = new SRS_ProductTemplateController.QuestionWrapper();
         QS3.isMandatory=true;
         QS3.onScreen=true;         
         QS3.systemQuestionID=2;
         QS3.questionValue='@@('; 
         QS3.Type='T';
         QS3.regexValue='^[0-9]{4}$';
         QS3.GroupNumber=0;
          QS3.questionText='qtext';
         QSlist1.add(QS3);        
         src.RQues.QuesSet.add(QSlist1);
       //  src.RQues.QuesSet.add(QS1);
         src.SaveRepeatablequestSet();
         SRS_ProductTemplateController.QuestionWrapper QS8 = new SRS_ProductTemplateController.QuestionWrapper();
         QS8.isMandatory=true;
         QS8.onScreen=true;
         QS8.questionValue='@@('; 
         QS8.Type='T';
         QS8.regexValue='^[0-9]{4}$';
         QS8.systemQuestionID=2;
         QS8.GroupNumber=2;
         QSlist1.add(QS8); 
        src.SaveTemplate();
    }
    
    
}