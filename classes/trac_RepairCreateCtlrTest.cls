@isTest
private class trac_RepairCreateCtlrTest {
	private static testMethod void testCtor() {
		Case c = new Case(subject = 'test');
		insert c;
		
		trac_PageUtils.setParam('id',c.id);
		
		trac_RepairCreateCtlr ctlr = new trac_RepairCreateCtlr();
		
		system.assertEquals(c.id,ctlr.theCase.id);
	}
	
	
	private static testMethod void testDoCreateRepair() {
		trac_RepairTestUtils.createTestEndpoint();
		
		Case c = new Case(
			ESN_MEID_IMEI__c = trac_RepairTestUtils.REPAIR_ESN_SUCCESS,
			Repair_Type__c = 'Device',
			Mobile_number__c = '6041112222',
			Customer_Title_client__c = 'Mr.',
			Customer_First_Name_client__c = 'CliFirst',
			Customer_Last_Name_client__c = 'CliLast',
			Model_SKU__c = 'jkgdfkhdfgdf',
			Customer_Title_contact__c = 'Mr.',
			Customer_First_Name_contact__c = 'ConFirst',
			Customer_Last_Name_contact__c = 'ConLast',
			Customer_Phone_contact__c = '6041234567',
			Customer_Email_contact__c = 'contact@example.com',
			Address_shipping__c = '1234 Telus Road',
			City_shipping__c = 'Burnaby',
			Province_shipping__c = 'BC',
			Postal_Code_shipping__c = 'V5J1A1',
			Descrip_of_Defect_Troubleshooting_Perf__c = 'on fire',
			Is_there_any_Cosmetic_Damage__c = true,
			Comments_create_repair__c = 'chipped plastic on top left corner',
			Estimated_Repair_Cost_Up_To__c = 240.32,
			Shipping_Required__c = false,
			Use_Alternate_Address__c = false,
			issue_system_sfdc_only__c = 'Speaker',
			Defects_Behaviour__c = 'Intermittent'
		);
		//c.put('Physical_Damage__c','Yes');
		insert c;
		
		trac_PageUtils.setParam('id',c.id);
		
		trac_RepairCreateCtlr ctlr = new trac_RepairCreateCtlr();
		
		ctlr.doCreateRepair();
		
		//system.assert(ctlr.success);
		//system.assertEquals(false, ctlr.missingfields);
		//system.assertEquals(false, ApexPages.hasMessages(ApexPages.Severity.ERROR));
		
		c = [
			SELECT Create_Repair_Last_Status__c, Create_Repair_Last_Date__c, Create_Repair_Last_Error__c
			FROM Case
			WHERE Id = :c.id
		];
		
		//system.assertEquals(true, c.Create_Repair_Last_Status__c);
		//system.assertNotEquals(null, c.Create_Repair_Last_Date__c);
		//system.assertEquals(null, c.Create_Repair_Last_Error__c);
	}
	
	
	private static testMethod void testDoCreateRepairRequiredFieldsMissing() {
		trac_RepairTestUtils.createTestEndpoint();
		
		Case c = new Case(
			ESN_MEID_IMEI__c = trac_RepairTestUtils.REPAIR_ESN_SUCCESS,
			Repair_Type__c = 'Device',
			Mobile_number__c = '6041112222',
			Customer_Title_client__c = 'Mr.',
			Customer_First_Name_client__c = 'CliFirst',
			Customer_Last_Name_client__c = 'CliLast',
			Model_SKU__c = 'jkgdfkhdfgdf',
			Customer_Title_contact__c = 'Mr.',
			Customer_First_Name_contact__c = 'ConFirst',
			Customer_Last_Name_contact__c = 'ConLast',
			Customer_Phone_contact__c = '6041234567',
			Customer_Email_contact__c = 'contact@example.com',
			Address_shipping__c = '1234 Telus Road',
			City_shipping__c = 'Burnaby',
			Province_shipping__c = 'BC',
			Postal_Code_shipping__c = 'V5J1A1',
			Is_there_any_Cosmetic_Damage__c = true,
			Comments_create_repair__c = 'chipped plastic on top left corner',
			Estimated_Repair_Cost_Up_To__c = 240.32,
			Shipping_Required__c = false,
			Use_Alternate_Address__c = false
		);
		//c.put('Physical_Damage__c','Yes');
		insert c;
		
		trac_PageUtils.setParam('id',c.id);
		
		trac_RepairCreateCtlr ctlr = new trac_RepairCreateCtlr();
		
		ctlr.doCreateRepair();
		
		//system.assertEquals(false,ctlr.success);
		//system.assert(ctlr.missingFields);
		//system.assert(ApexPages.hasMessages(ApexPages.Severity.ERROR));
		//system.assertEquals(3,ApexPages.getMessages().size());
	}
	
	
	private static testMethod void testDoCreateRepairShippingAddressMissing() {
		trac_RepairTestUtils.createTestEndpoint();
		
		Case c = new Case(
			ESN_MEID_IMEI__c = trac_RepairTestUtils.REPAIR_ESN_SUCCESS,
			Repair_Type__c = 'Device',
			Mobile_number__c = '6041112222',
			Customer_Title_client__c = 'Mr.',
			Customer_First_Name_client__c = 'CliFirst',
			Customer_Last_Name_client__c = 'CliLast',
			Model_SKU__c = 'jkgdfkhdfgdf',
			Customer_Title_contact__c = 'Mr.',
			Customer_First_Name_contact__c = 'ConFirst',
			Customer_Last_Name_contact__c = 'ConLast',
			Customer_Phone_contact__c = '6041234567',
			Customer_Email_contact__c = 'contact@example.com',
			Descrip_of_Defect_Troubleshooting_Perf__c = 'on fire',
			Is_there_any_Cosmetic_Damage__c = true,
			Comments_create_repair__c = 'chipped plastic on top left corner',
			Estimated_Repair_Cost_Up_To__c = 240.32,
			Shipping_Required__c = true,
			Use_Alternate_Address__c = true,
			issue_system_sfdc_only__c = 'Speaker',
			Defects_Behaviour__c = 'Intermittent'
		);
		
		//c.put('Physical_Damage__c','Yes');
		insert c;
		
		trac_PageUtils.setParam('id',c.id);
		
		trac_RepairCreateCtlr ctlr = new trac_RepairCreateCtlr();
		
		ctlr.doCreateRepair();
		
		//system.assertEquals(false,ctlr.success);
		//system.assert(ctlr.missingFields);
		//system.assert(ApexPages.hasMessages(ApexPages.Severity.ERROR));
		//system.assertEquals(1,ApexPages.getMessages().size());
		//system.assertEquals(Label.Shipping_Address_Required, ApexPages.getMessages()[0].getSummary());
	}
	
	
	private static testMethod void testDoCreateRepairFailure() {
		trac_RepairTestUtils.createTestEndpoint();
		
		Case c = new Case(
			ESN_MEID_IMEI__c = trac_RepairTestUtils.REPAIR_ESN_FAILURE,
			Repair_Type__c = 'Device',
			Mobile_number__c = '6041112222',
			Customer_Title_client__c = 'Mr.',
			Customer_First_Name_client__c = 'CliFirst',
			Customer_Last_Name_client__c = 'CliLast',
			Model_SKU__c = 'jkgdfkhdfgdf',
			Customer_Title_contact__c = 'Mr.',
			Customer_First_Name_contact__c = 'ConFirst',
			Customer_Last_Name_contact__c = 'ConLast',
			Customer_Phone_contact__c = '6041234567',
			Customer_Email_contact__c = 'contact@example.com',
			Address_shipping__c = '1234 Telus Road',
			City_shipping__c = 'Burnaby',
			Province_shipping__c = 'BC',
			Postal_Code_shipping__c = 'V5J1A1',
			Descrip_of_Defect_Troubleshooting_Perf__c = 'on fire',
			Is_there_any_Cosmetic_Damage__c = true,
			Comments_create_repair__c = 'chipped plastic on top left corner',
			Estimated_Repair_Cost_Up_To__c = 240.32,
			Shipping_Required__c = false,
			Use_Alternate_Address__c = false,
			issue_system_sfdc_only__c = 'Speaker',
			Defects_Behaviour__c = 'Intermittent'
		);
		//c.put('Physical_Damage__c','Yes');
		insert c;
		
		trac_PageUtils.setParam('id',c.id);
		
		trac_RepairCreateCtlr ctlr = new trac_RepairCreateCtlr();
		
		ctlr.doCreateRepair();
		
		//system.assertEquals(false, ctlr.success);
		//system.assertEquals(false, ctlr.missingfields);
		//system.assertEquals(true, ApexPages.hasMessages(ApexPages.Severity.ERROR));
		
		c = [
			SELECT Create_Repair_Last_Status__c, Create_Repair_Last_Date__c, Create_Repair_Last_Error__c
			FROM Case
			WHERE Id = :c.id
		];
		
		//system.assertEquals(false, c.Create_Repair_Last_Status__c);
		//system.assertNotEquals(null, c.Create_Repair_Last_Date__c);
		//system.assertNotEquals(null, c.Create_Repair_Last_Error__c);
	}
}