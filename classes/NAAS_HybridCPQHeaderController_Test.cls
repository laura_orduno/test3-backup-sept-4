@isTest
public class NAAS_HybridCPQHeaderController_Test {

//PageReference opptyPage = new PageReference('/apex/NAAS_test_hybridcpq');
   // Test.setCurrentPage(opptyPage);
 
   public static testmethod void testHybridCPQHeaderController() {
        Account acc = new Account(Name='Test Account');
        insert acc;
       
        
        Account acc1 = [select name , Id from Account where name='Test Account' limit 1];
        SMBCare_Address__c address = new SMBCare_Address__c(Account__c=acc1.Id,Street_Address__c='adasd',City__c='asdasd', FMS_Address_ID__c='sfsdf'); 
        insert address;
        
        //Added by Manpreet 18/04/2017----------------start
        Product2 prod0 = new Product2(Name = 'Laptop X200 Parent', 
            Family = 'Hardware2',orderMgmtId__c='914144966581343623k4',OCOM_Shippable__c='Yes',Sellable__c= true);
        insert prod0;
        // Insert a test product.
        Product2 prod = new Product2(Name = 'Laptop X200', 
            Family = 'Hardware',OCOM_Shippable__c='Yes',ProductSpecification__c=prod0.id,Sellable__c= true);
        insert prod;
         System.assertEquals(prod.name, 'Laptop X200');
        // Get standard price book ID.
        // This is available irrespective of the state of SeeAllData.
        Id pricebookId = Test.getStandardPricebookId();
        
        // 1. Insert a price book entry for the standard price book.
        // Standard price book entries require the standard price book ID we got earlier.
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
       
        // Create a custom price book
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        // 2. Insert a price book entry with a custom price.
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
         
        contact cont= new contact(firstname='abc',lastname='xyz ');
        insert cont;

        order ord = new order(Ban__c='draft',accountId=acc.id,effectivedate=system.today(),status='not submitted',Pricebook2Id=customPB.id,CustomerAuthorizedById=cont.id,service_address__c=address.id);   
        //order ord2 = new order(Ban__c='draft',accountId=acc.id,effectivedate=system.today(),status='not submitted',Pricebook2Id=customPB.id,CustomerAuthorizedById=cont.id,service_address__c=address.id);   
        //List<Order>ordlist= new List<Order>();   
        //ordList.add(ord);ordList.add(ord2);
         //insert ordList; 
         insert ord;
        Order order = [Select Id from Order where AccountId = :acc1.Id]; 

        orderItem  OLI2= new orderItem(Shipping_Address__c='',UnitPrice=12,orderId= ord.Id,orderMgmt_BPI_Id__c='9871379163517',vlocity_cmt__BillingAccountId__c=acc.id,vlocity_cmt__ServiceAccountId__c=acc.id,Quantity=2,PricebookEntryId=customPrice.id,vlocity_cmt__JSONAttribute__c='{"TELUSCHAR":[{"$$AttributeDefinitionEnd$$":null,"attributeRunTimeInfo":{"default":"WirelessFailover","uiDisplayType":"Text","dataType":"Text"},"valuedescription__c":null,"valuedatatype__c":"Text","value__c":"WirelessFailover","uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":true,"id":"a7P2200000001GjEAI","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":true,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":true,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Offering ID","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"21","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-096","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q2200000000UBEAY","attributeid__c":"a7R220000004CUaEAM","objectid__c":"01t220000003udlAAA","$$AttributeDefinitionStart$$":null},{"$$AttributeDefinitionEnd$$":null,"attributeRunTimeInfo":{"uiDisplayType":"Text","dataType":"Text"},"valuedescription__c":null,"valuedatatype__c":"Text","value__c":null,"uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":false,"id":"a7P2200000001FpEAI","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":true,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":true,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Subcriber first name","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"1","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-246","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q2200000000UBEAY","attributeid__c":"a7R220000004Cb7EAE","objectid__c":"01t220000003udlAAA","$$AttributeDefinitionStart$$":null},{"$$AttributeDefinitionEnd$$":null,"attributeRunTimeInfo":{"default":"1 GB","uiDisplayType":"Text","dataType":"Text"},"valuedescription__c":null,"valuedatatype__c":"Text","value__c":"1 GB","uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":true,"id":"a7P2200000001FqEAI","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":true,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":true,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Subscriber Last Name","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"2","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-138","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q2200000000UBEAY","attributeid__c":"a7R220000004CXKEA2","objectid__c":"01t220000003udlAAA","$$AttributeDefinitionStart$$":null},{"$$AttributeDefinitionEnd$$":null,"attributeRunTimeInfo":{"default":"Blocked","uiDisplayType":"Text","dataType":"Text"},"valuedescription__c":null,"valuedatatype__c":"Text","value__c":"Blocked","uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":true,"id":"a7P2200000001FrEAI","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":true,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":true,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Voice","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"4","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-142","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q2200000000UBEAY","attributeid__c":"a7R220000004CXUEA2","objectid__c":"01t220000003udlAAA","$$AttributeDefinitionStart$$":null},{"$$AttributeDefinitionEnd$$":null,"attributeRunTimeInfo":{"default":"connect.telus.com","uiDisplayType":"Text","dataType":"Text"},"valuedescription__c":null,"valuedatatype__c":"Text","value__c":"connect.telus.com","uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":true,"id":"a7P2200000001FsEAI","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":true,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":true,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"APN","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"7","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-143","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q2200000000UBEAY","attributeid__c":"a7R220000004CXZEA2","objectid__c":"01t220000003udlAAA","$$AttributeDefinitionStart$$":null},{"$$AttributeDefinitionEnd$$":null,"attributeRunTimeInfo":{"uiDisplayType":"Text","dataType":"Text"},"valuedescription__c":null,"valuedatatype__c":"Text","value__c":null,"uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":false,"id":"a7P2200000001FtEAI","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":true,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":true,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Contact Name","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"14","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-148","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q2200000000UBEAY","attributeid__c":"a7R220000004CXoEAM","objectid__c":"01t220000003udlAAA","$$AttributeDefinitionStart$$":null},{"$$AttributeDefinitionEnd$$":null,"attributeRunTimeInfo":{"uiDisplayType":"Text","dataType":"Text"},"valuedescription__c":null,"valuedatatype__c":"Text","value__c":null,"uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":false,"id":"a7P2200000001FuEAI","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":true,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":true,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Contact Number","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"15","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-248","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q2200000000UBEAY","attributeid__c":"a7R220000004CbHEAU","objectid__c":"01t220000003udlAAA","$$AttributeDefinitionStart$$":null},{"$$AttributeDefinitionEnd$$":null,"attributeRunTimeInfo":{"default":"Copper","uiDisplayType":"Text","dataType":"Text"},"valuedescription__c":null,"valuedatatype__c":"Text","value__c":"Copper","uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":true,"id":"a7P2200000001FvEAI","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":true,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":true,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Provisioning Indicator","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"16","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-150","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q2200000000UBEAY","attributeid__c":"a7R220000004CXtEAM","objectid__c":"01t220000003udlAAA","$$AttributeDefinitionStart$$":null},{"$$AttributeDefinitionEnd$$":null,"attributeRunTimeInfo":{"uiDisplayType":"Text","dataType":"Text"},"valuedescription__c":null,"valuedatatype__c":"Text","value__c":null,"uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":false,"id":"a7P2200000001FxEAI","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":true,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":true,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Model Number","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"17","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-153","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q2200000000UBEAY","attributeid__c":"a7R220000004CY8EAM","objectid__c":"01t220000003udlAAA","$$AttributeDefinitionStart$$":null},{"$$AttributeDefinitionEnd$$":null,"attributeRunTimeInfo":{"uiDisplayType":"Text","dataType":"Text"},"valuedescription__c":null,"valuedatatype__c":"Text","value__c":null,"uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":false,"id":"a7P2200000001FwEAI","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":true,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":true,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Shipping Address","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"18","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-249","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q2200000000UBEAY","attributeid__c":"a7R220000004CbMEAU","objectid__c":"01t220000003udlAAA","$$AttributeDefinitionStart$$":null},{"$$AttributeDefinitionEnd$$":null,"attributeRunTimeInfo":{"uiDisplayType":"Text","dataType":"Text"},"valuedescription__c":null,"valuedatatype__c":"Text","value__c":null,"uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":false,"id":"a7P2200000001G3EAI","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":true,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":true,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Shipping Address Characteristic","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"19","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-155","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q2200000000UBEAY","attributeid__c":"a7R220000004CXQEA2","objectid__c":"01t220000003udlAAA","$$AttributeDefinitionStart$$":null},{"$$AttributeDefinitionEnd$$":null,"attributeRunTimeInfo":{"uiDisplayType":"Text","dataType":"Text"},"valuedescription__c":null,"valuedatatype__c":"Text","value__c":null,"uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":false,"id":"a7P2200000001GYEAY","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":true,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":true,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Province of use","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"20","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-250","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q2200000000UBEAY","attributeid__c":"a7R220000004CbREAU","objectid__c":"01t220000003udlAAA","$$AttributeDefinitionStart$$":null},{"$$AttributeDefinitionEnd$$":null,"attributeRunTimeInfo":{"value":null,"uiDisplayType":"Text","dataType":"Text"},"valuedescription__c":null,"valuedatatype__c":"Text","value__c":null,"uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":false,"id":"a7P2200000001K4EAI","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":false,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":false,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"City of use","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"22","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-184","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q2200000000UBEAY","attributeid__c":"a7R220000004CZLEA2","objectid__c":"01t220000003udlAAA","$$AttributeDefinitionStart$$":null}]}');
        orderItem  OLI= new orderItem(Shipping_Address__c='',vlocity_cmt__RootItemId__c=OLI2.id,UnitPrice=12,orderId= order.Id,vlocity_cmt__AssetReferenceId__c=OLI2.id,vlocity_cmt__BillingAccountId__c=acc1.id,vlocity_cmt__ServiceAccountId__c=acc.id,Quantity=2,PricebookEntryId=customPrice.id,vlocity_cmt__JSONAttribute__c='{"TELUSCHAR":[{"$$AttributeDefinitionEnd$$":null,"attributeRunTimeInfo":{"default":"WirelessFailover","uiDisplayType":"Text","dataType":"Text"},"valuedescription__c":null,"valuedatatype__c":"Text","value__c":"WirelessFailover","uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":true,"id":"a7P2200000001GjEAI","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":true,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":true,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Offering ID","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"21","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-096","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q2200000000UBEAY","attributeid__c":"a7R220000004CUaEAM","objectid__c":"01t220000003udlAAA","$$AttributeDefinitionStart$$":null},{"$$AttributeDefinitionEnd$$":null,"attributeRunTimeInfo":{"uiDisplayType":"Text","dataType":"Text"},"valuedescription__c":null,"valuedatatype__c":"Text","value__c":null,"uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":false,"id":"a7P2200000001FpEAI","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":true,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":true,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Subcriber first name","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"1","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-246","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q2200000000UBEAY","attributeid__c":"a7R220000004Cb7EAE","objectid__c":"01t220000003udlAAA","$$AttributeDefinitionStart$$":null},{"$$AttributeDefinitionEnd$$":null,"attributeRunTimeInfo":{"default":"1 GB","uiDisplayType":"Text","dataType":"Text"},"valuedescription__c":null,"valuedatatype__c":"Text","value__c":"1 GB","uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":true,"id":"a7P2200000001FqEAI","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":true,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":true,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Subscriber Last Name","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"2","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-138","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q2200000000UBEAY","attributeid__c":"a7R220000004CXKEA2","objectid__c":"01t220000003udlAAA","$$AttributeDefinitionStart$$":null},{"$$AttributeDefinitionEnd$$":null,"attributeRunTimeInfo":{"default":"Blocked","uiDisplayType":"Text","dataType":"Text"},"valuedescription__c":null,"valuedatatype__c":"Text","value__c":"Blocked","uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":true,"id":"a7P2200000001FrEAI","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":true,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":true,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Voice","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"4","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-142","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q2200000000UBEAY","attributeid__c":"a7R220000004CXUEA2","objectid__c":"01t220000003udlAAA","$$AttributeDefinitionStart$$":null},{"$$AttributeDefinitionEnd$$":null,"attributeRunTimeInfo":{"default":"connect.telus.com","uiDisplayType":"Text","dataType":"Text"},"valuedescription__c":null,"valuedatatype__c":"Text","value__c":"connect.telus.com","uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":true,"id":"a7P2200000001FsEAI","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":true,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":true,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"APN","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"7","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-143","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q2200000000UBEAY","attributeid__c":"a7R220000004CXZEA2","objectid__c":"01t220000003udlAAA","$$AttributeDefinitionStart$$":null},{"$$AttributeDefinitionEnd$$":null,"attributeRunTimeInfo":{"uiDisplayType":"Text","dataType":"Text"},"valuedescription__c":null,"valuedatatype__c":"Text","value__c":null,"uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":false,"id":"a7P2200000001FtEAI","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":true,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":true,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Contact Name","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"14","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-148","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q2200000000UBEAY","attributeid__c":"a7R220000004CXoEAM","objectid__c":"01t220000003udlAAA","$$AttributeDefinitionStart$$":null},{"$$AttributeDefinitionEnd$$":null,"attributeRunTimeInfo":{"uiDisplayType":"Text","dataType":"Text"},"valuedescription__c":null,"valuedatatype__c":"Text","value__c":null,"uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":false,"id":"a7P2200000001FuEAI","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":true,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":true,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Contact Number","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"15","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-248","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q2200000000UBEAY","attributeid__c":"a7R220000004CbHEAU","objectid__c":"01t220000003udlAAA","$$AttributeDefinitionStart$$":null},{"$$AttributeDefinitionEnd$$":null,"attributeRunTimeInfo":{"default":"Copper","uiDisplayType":"Text","dataType":"Text"},"valuedescription__c":null,"valuedatatype__c":"Text","value__c":"Copper","uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":true,"id":"a7P2200000001FvEAI","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":true,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":true,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Provisioning Indicator","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"16","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-150","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q2200000000UBEAY","attributeid__c":"a7R220000004CXtEAM","objectid__c":"01t220000003udlAAA","$$AttributeDefinitionStart$$":null},{"$$AttributeDefinitionEnd$$":null,"attributeRunTimeInfo":{"uiDisplayType":"Text","dataType":"Text"},"valuedescription__c":null,"valuedatatype__c":"Text","value__c":null,"uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":false,"id":"a7P2200000001FxEAI","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":true,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":true,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Model Number","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"17","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-153","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q2200000000UBEAY","attributeid__c":"a7R220000004CY8EAM","objectid__c":"01t220000003udlAAA","$$AttributeDefinitionStart$$":null},{"$$AttributeDefinitionEnd$$":null,"attributeRunTimeInfo":{"uiDisplayType":"Text","dataType":"Text"},"valuedescription__c":null,"valuedatatype__c":"Text","value__c":null,"uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":false,"id":"a7P2200000001FwEAI","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":true,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":true,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Shipping Address","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"18","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-249","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q2200000000UBEAY","attributeid__c":"a7R220000004CbMEAU","objectid__c":"01t220000003udlAAA","$$AttributeDefinitionStart$$":null},{"$$AttributeDefinitionEnd$$":null,"attributeRunTimeInfo":{"uiDisplayType":"Text","dataType":"Text"},"valuedescription__c":null,"valuedatatype__c":"Text","value__c":null,"uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":false,"id":"a7P2200000001G3EAI","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":true,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":true,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Shipping Address Characteristic","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"19","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-155","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q2200000000UBEAY","attributeid__c":"a7R220000004CXQEA2","objectid__c":"01t220000003udlAAA","$$AttributeDefinitionStart$$":null},{"$$AttributeDefinitionEnd$$":null,"attributeRunTimeInfo":{"uiDisplayType":"Text","dataType":"Text"},"valuedescription__c":null,"valuedatatype__c":"Text","value__c":null,"uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":false,"id":"a7P2200000001GYEAY","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":true,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":true,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Province of use","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"20","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-250","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q2200000000UBEAY","attributeid__c":"a7R220000004CbREAU","objectid__c":"01t220000003udlAAA","$$AttributeDefinitionStart$$":null},{"$$AttributeDefinitionEnd$$":null,"attributeRunTimeInfo":{"value":null,"uiDisplayType":"Text","dataType":"Text"},"valuedescription__c":null,"valuedatatype__c":"Text","value__c":null,"uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":false,"id":"a7P2200000001K4EAI","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":false,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":false,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"City of use","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"22","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-184","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q2200000000UBEAY","attributeid__c":"a7R220000004CZLEA2","objectid__c":"01t220000003udlAAA","$$AttributeDefinitionStart$$":null}]}');
        prod0.Sellable__c = true;
        List<OrderItem>OLIlist= new List<OrderItem>();   
        OLIList.add(OLI2);OLIList.add(OLI);
        insert OLIList;
        //End Manpreet ---18/04/2017
        
        Test.setCurrentPageReference(new PageReference('Page.NAAS_test_hybridcpq')); 
        System.currentPageReference().getParameters().put('id', order.Id);
        NAAS_HybridCPQHeaderController Ctrl = new NAAS_HybridCPQHeaderController();
        Boolean val = True;
        String typeval = '';
        typeval = Ctrl.Type;
        val = Ctrl.displayComponents;
        val = Ctrl.displayViewPdfBtn;
        string a = Ctrl.genericAccountName;
        //System.assertEquals(a,'Test Account');
        string c = Ctrl.orderNumber;
        Ctrl.previousOrderStatus = 'Completed';
        Ctrl.cancelOrder();
        
       
   }
   
    public static testmethod void tesnegativetHybridCPQHeaderController() {
        Account acc = new Account(name='Test Account');
        insert acc;
        
        Account acc1 = [select name , Id from Account where name='Test Account' limit 1];
        SMBCare_Address__c address = new SMBCare_Address__c(Account__c=acc1.Id); 
        insert address;
        Order ord = new Order(AccountId = acc1.Id, Status='Draft',EffectiveDate=System.Today(),service_address__c=address.id);
       //ord.Reason_Code__c='';
        
        insert ord;
        
        Order order = [Select Id from Order where AccountId = :acc1.Id]; 
        
        Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.NAAS_test_hybridcpq')); 
        //System.currentPageReference().getParameters().put('id', order.Id);
        NAAS_HybridCPQHeaderController Ctrl = new NAAS_HybridCPQHeaderController();
      //  String c = Ctrl.moveOutAddress;
           Boolean IsCancelFail= true;
        string a = Ctrl.genericAccountName;
        //string b = Ctrl.genericAddress;
        String type = Ctrl.Type;  
        
        Test.StopTest();     
   }
   
   public static testmethod void testHybridCPQHeaderControllerwithoutId() {
        Account acc = new Account(Name='Test Account');
        insert acc;
       
        webservice_integration_error_log__c log = new webservice_integration_error_log__c();
        
        Account acc1 = [select name , Id from Account where name='Test Account' limit 1];
        SMBCare_Address__c address = new SMBCare_Address__c(Account__c=acc1.Id,Street_Address__c='adasd',City__c='asdasd', FMS_Address_ID__c='sfsdf'); 
        insert address;
       Credit_Assessment__c CAR= new Credit_Assessment__c();
        Order ord = new Order(AccountId = acc1.Id, Status='Draft',EffectiveDate=System.Today(),service_address__c=address.id);
     // ord.Credit_Assessment__c = CAR;
       insert ord;
        
        List<Order> order = [Select Id from Order where AccountId = :acc1.Id];
        String TestorderId = order[0].Id;
        Test.setCurrentPageReference(new PageReference('Page.NAAS_test_hybridcpq')); 
        System.currentPageReference().getParameters().put('id',TestorderId);
        NAAS_HybridCPQHeaderController Ctrl = new NAAS_HybridCPQHeaderController();
        Ctrl.orderId = TestorderId;
        Boolean IsCancelFail= true;
        Boolean val = True;
        String typeval = '';
        typeval = Ctrl.Type;
        val = Ctrl.displayComponents;
        val = Ctrl.displayViewPdfBtn; 
        string a = Ctrl.genericAccountName;
        string c = Ctrl.orderNumber;
        Ctrl.previousOrderStatus = '';
        Ctrl.cancelOrder();
        Ctrl.RedirectPage = '';
        Ctrl.DoOk();
        Ctrl.invalidForm();
        Ctrl.CancelButton();
        Ctrl.updateOrderStatus();
        Ctrl.DoCancel(TestorderId,'Test reasin Code','Test Notes');
        NAAS_HybridCPQHeaderController.vfrPrepareOrderCancellationRequest(TestorderId, 'Test reason Code','Test Notes');
        Ctrl.cancelNCOrder(TestorderId,log);
        
   }
     public static testmethod void testHybridCPQHeaderController1() {
        Account acc = new Account(Name='Test Account');
        insert acc;
       
        webservice_integration_error_log__c log = new webservice_integration_error_log__c();
        
        Account acc1 = [select name , Id from Account where name='Test Account' limit 1];
        SMBCare_Address__c address = new SMBCare_Address__c(Account__c=acc1.Id,Street_Address__c='adasd',City__c='asdasd', FMS_Address_ID__c='sfsdf'); 
        insert address;
       Credit_Assessment__c CAR= new Credit_Assessment__c();
         insert CAR;
        Order ord = new Order(AccountId = acc1.Id, Status='Draft',EffectiveDate=System.Today(),service_address__c=address.id);
         ord.Credit_Assessment__c = CAR.Id;
       ord.General_Notes_Remarks__c='Test';
         ord.Reason_Code__c='Customer-initiated';
         insert ord;
         
       TpFulfillmentCustomerOrderV3.CustomerOrder cancelOrderResponse = new TpFulfillmentCustomerOrderV3.CustomerOrder();
        cancelOrderResponse.ErrorCode = '';
        
        List<Order> order = [Select Id from Order where AccountId = :acc1.Id];
        String TestorderId = order[0].Id;
        Test.setCurrentPageReference(new PageReference('Page.NAAS_test_hybridcpq')); 
        System.currentPageReference().getParameters().put('id',TestorderId);
        NAAS_HybridCPQHeaderController Ctrl = new NAAS_HybridCPQHeaderController();
        Ctrl.orderId = TestorderId;
        
        Boolean val = False;
        String typeval = '';
        typeval = Ctrl.Type;
        
        val = Ctrl.displayComponents;
        val = Ctrl.displayViewPdfBtn; 
        string a = Ctrl.genericAccountName;
        string c = Ctrl.orderNumber;
        Ctrl.cancelOrder();
        Ctrl.RedirectPage = '';
        Ctrl.displayCancelOrder= true;
        Ctrl.DoOk();
        Ctrl.invalidForm();
        Ctrl.CancelButton();
        Ctrl.updateOrderStatus();
        Ctrl.DoCancel(TestorderId,'Test reasin Code','Test Notes');
        NAAS_HybridCPQHeaderController.vfrPrepareOrderCancellationRequest(TestorderId, 'Test reason Code','Test Notes');
        //Added By Manpreet
        Ctrl.cancelNCOrder(TestorderId,log);
        //End
        NAAS_HybridCPQHeaderController.callRemoteMethodForDefaultlanguage(TestorderId);
        String lang = NAAS_HybridCPQHeaderController.lang;
        NAAS_HybridCPQHeaderController.setChoosenLanguageOnOrder(TestorderId, 'French');
   }
   /*public static testmethod void testCancelOrder() {
       webservice_integration_error_log__c log = new webservice_integration_error_log__c();
       
       //List<Account> accList = TestDataCreationFactory.createRCIDAccounts(1);
       String orderId=OrdrTestDataFactory.singleMethodForOrderId();
       
       //Order orderObj= new Order(AccountId = accList [0].Id, Status='In Progress', EffectiveDate=System.Today());
       //insert orderObj;
       
       
       List<Order> order = [Select Id from Order where Id =: orderId];
       order[0].Status = 'Error' ;
       update order; 
       //String orderID = order[0].Id;
        List<SMBCare_WebServices__c> smbSettingList =  new List<SMBCare_WebServices__c>();
        SMBCare_WebServices__c smbWsBook = new SMBCare_WebServices__c(name = 'BookDueDate_EndPoint', Use_Soap_For_PT__c =  false, value__c = 'https://xmlgwy-pt1.telus.com:9030/is02/RMO/ProcessMgmt/FieldWorkAssignmentMgmtService_v2_0_vs0');
        smbSettingList.add(smbWsBook );
        SMBCare_WebServices__c smbWsuser = new SMBCare_WebServices__c(name = 'username', Use_Soap_For_PT__c =  false, value__c = 'APP_SFDC');
        smbSettingList.add(smbWsuser );
        SMBCare_WebServices__c smbWspass = new SMBCare_WebServices__c(name = 'password', Use_Soap_For_PT__c =  false, value__c = 'soaorgid');
        smbSettingList.add(smbWspass );
        
        insert smbSettingList;
        
        
        
        Test.setCurrentPageReference(new PageReference('Page.NAAS_hybridcpq')); 
        System.currentPageReference().getParameters().put('id',orderID );
        NAAS_HybridCPQHeaderController Ctrl = new NAAS_HybridCPQHeaderController();
        Ctrl.orderId = orderID ;
        Boolean val = True;
        String typeval = '';
        typeval = Ctrl.Type;
        val = Ctrl.displayComponents;
        val = Ctrl.displayViewPdfBtn; 
        string a = Ctrl.genericAccountName;
        string c = Ctrl.orderNumber;
        Ctrl.cancelOrder();
        Ctrl.RedirectPage = '';
        Ctrl.DoOk();
        Ctrl.invalidForm();
        Ctrl.CancelButton();
        Ctrl.updateOrderStatus();
        Ctrl.DoCancel(orderID ,'Test Reason Code','Test Notes');
        NAAS_HybridCPQHeaderController.vfrPrepareOrderCancellationRequest(orderID , 'Test Reason Code','Test Notes');
        Ctrl.cancelNCOrder(orderID ,log);
   
   }*/
 }