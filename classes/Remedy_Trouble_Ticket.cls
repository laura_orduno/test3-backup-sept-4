global class Remedy_Trouble_Ticket {
    
global class TroubleTicket {
        public String Business_Unit_Id;
        public String Client_Phone;
        public String Company_Id;
        public String Configuration_id;
        public String Customer_Update_Info;
        public String Description;
        public String E_mail_address;
        public String First_Name;
        public String Last_modified_by;
        public String Last_Name;
        public String Location;
        public DateTime Modified_date;
        public DateTime Resolve_Time;
        public DateTime Start_Time;
        public String Origin;
        public String PIN;
        public String Problem;
        public String Resolution;
        public String Resolution_Code;
        public String Severity;
        public String Short_Description;
        public String Ticket_Number;
        public String Ticket_Status;
        public String Customer_Ticket;
              
       
        private String[] Business_Unit_Id_type_info = new String[]{'Business_Unit_Id','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] Client_Phone_type_info = new String[]{'Client_Phone','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] Company_Id_type_info = new String[]{'Company_Id','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] Configuration_id_type_info = new String[]{'Configuration_id','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] Customer_Update_Info_type_info = new String[]{'Customer_Update_Info','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] Description_type_info = new String[]{'Description','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] E_mail_address_type_info = new String[]{'E-mail_address','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] First_Name_type_info = new String[]{'First_Name','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] Last_Name_type_info = new String[]{'Last_Name','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] Last_modified_by_type_info = new String[]{'Last-modified-by','http://www.w3.org/2001/XMLSchema','dateTime','0','1','false'};
        private String[] Resolve_Time_type_info = new String[]{'Resolve-Time','http://www.w3.org/2001/XMLSchema','dateTime','0','1','false'};
        private String[] Start_Time_type_info = new String[]{'Start-Time','http://www.w3.org/2001/XMLSchema','dateTime','0','1','false'};
        private String[] Origin_type_info = new String[]{'Origin','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] PIN_type_info = new String[]{'PIN','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] Problem_type_info = new String[]{'Problem','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] Resolution_type_info = new String[]{'Resolution','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] Resolution_Code_type_info = new String[]{'Resolution_Code','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] Severity_type_info = new String[]{'Severity','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] Short_Description_type_info = new String[]{'Short_Description','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] Ticket_Number_type_info = new String[]{'Ticket_Number','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] Ticket_Status_type_info = new String[]{'Ticket_Status','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] Customer_Ticket_type_info = new String[]{'Customer_Ticket_','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
       
        private String[] field_order_type_info = new String[]{'Business_Unit_Id','Client_Phone','Company_Id','Configuration_id','Customer_Update_Info','Description','E-mail_address','First_Name','Last_Name','Last-modified-by','Resolve-Time','Start-Time','Origin','PIN','Problem','Resolution','Resolution_Code','Severity','Short_Description','Ticket_Number','Ticket_Status','Customer_Ticket_'};
    }    

}