public abstract class RuleProcessorSupport {
	public Map<Id,SBQQ__SummaryVariable__c> referencedVariables {get; private set;}
	
	protected Boolean initialized = false;
    protected SummaryVariableCalculator variableCalculator;
    
    protected virtual SObject getEvaluatedRecord(SObject target) {
    	return target;
    }
    
    /* protected List<SObject[]> processQuoteLines(Id quoteId, SObject[] lines) {
		if (lines.isEmpty()) {
			return new List<SObject[]>();
		}
		System.Savepoint sp = Database.setSavepoint();
		try {
			return doProcessQuoteLines(quoteId, lines);
		} finally {
			Database.rollback(sp);
		}
	}
	
	private List<SObject[]> doProcessQuoteLines(Id quoteId, SObject[] lines) {
		List<SObject[]> pairs = new List<SObject[]>();
		Map<Id, SObject> clonedMap = new Map<Id, SObject>();
		Set<Id> clonedIds = new Set<Id>();
		List<SObject> clonedQuoteLines = new List<SObject>();
		for (SObject ql : lines) {	
			SObject clonedQL = ql.clone(false, false);
			AliasedMetaDataUtils.setFieldValue(clonedQL, SBQQ__QuoteLine__c.SBQQ__Quote__c, quoteId);
			AliasedMetaDataUtils.setFieldObject(clonedQL, SBQQ__QuoteLine__c.SBQQ__Product__c, null);
			AliasedMetaDataUtils.setFieldObject(clonedQL, SBQQ__QuoteLine__c.SBQQ__RequiredBy__c, null);
			AliasedMetaDataUtils.setFieldValue(clonedQL, SBQQ__QuoteLine__c.SBQQ__Incomplete__c, true);
			clonedQuoteLines.add(clonedQL);
		}
		
		insert clonedQuoteLines;
		
		for (Integer i = 0; i < clonedQuoteLines.size(); i++) {
			SObject clonedQL = clonedQuoteLines.get(i);
			SObject ql = lines.get(i);				
			clonedIds.add(clonedQL.Id);				
			clonedMap.put(clonedQL.Id, ql);
		}
		clonedQuoteLines = loadClonedQuoteLines(quoteId, clonedIds);
		for (SObject clonedQL : clonedQuoteLines) {		
			SObject ql = clonedMap.get(clonedQL.Id);						
			SObject[] pair  = new SObject[] {ql, clonedQL};
			pairs.add(pair);	
		}		
		return pairs;
	}
	
	private SObject[] loadClonedQuoteLines(Id quoteId, Set<Id> clonedIds) {
		Schema.SObjectType qlType = AliasedMetaDataUtils.determineQuoteLineType(quoteId);
		
		QueryBuilder qb = new QueryBuilder(qlType);
		SObjectType objectType = SBQQ__QuoteLine__c.sObjectType;
		Schema.DescribeSObjectResult describeResult = qlType.getDescribe();
		Map<String, Schema.SObjectField> fieldMap = describeResult.fields.getMap();
		for (String fieldName : fieldMap.keySet()) {
			qb.getSelectClause().addField(fieldName);					
		}
		qb.getWhereClause().addExpression(qb.InExpr(SBQQ__QuoteLine__c.Id, clonedIds));
		return Database.query(qb.buildSOQL());		
	} */
}