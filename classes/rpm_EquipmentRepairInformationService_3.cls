public class rpm_EquipmentRepairInformationService_3 {
    public class EquipmentRepairInformationServicePort {
        public String endpoint_x = 'http://rmosr-resourcelifecyclemgmt-dv103.tmi.telus.com/RMO/LifeCycleMgmt/Repair/EquipmentRepairInformationService_v3_0';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        public Boolean lenientParsing_x;
        private String[] ns_map_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Product/Product/ProductCommon_v1', 'rpm_ProductCommon_v1', 'http://xmlschema.tmi.telus.com/xsd/Resource/Resource/CourierShippingMgmtTypes_v1', 'rpm_CourierShippingMgmtTypes_v1', 'http://xmlschema.tmi.telus.com/xsd/Resource/Resource/EquipmentRepairTypes_v1', 'rpm_EquipmentRepairTypes_v1', 'http://xmlschema.tmi.telus.com/srv/RMO/LifeCycleMgmt/EquipmentRepairInformationSvcRequestResponse_v3', 'rpm_EquipmentRepairInformationSvcRequest', 'http://xmlschema.tmi.telus.com/xsd/Product/ProductOffering/ProductOfferingCommon_v1', 'rpm_ProductOfferingCommon_v1', 'http://telus.com/wsdl/RMO/LifeCycleMgmt/EquipmentRepairInformationService_3', 'rpm_EquipmentRepairInformationService_3', 'http://xmlschema.tmi.telus.com/xsd/Partner/Partner/ChannelPartnerCommon_v1', 'rpm_ChannelPartnerCommon_v1', 'http://xmlschema.tmi.telus.com/xsd/Product/ProductOffering/ProductOfferingCommon_v2', 'rpm_ProductOfferingCommon_v2', 'http://xmlschema.tmi.telus.com/xsd/Partner/Partner/ChannelPartnerCommon_v2', 'rpm_ChannelPartnerCommon_v2', 'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/ping_v1', 'rpm_ping_v1', 'http://xmlschema.tmi.telus.com/xsd/Product/Product/ProductCommon_v2', 'rpm_ProductCommon_v2', 'http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/Customer_Billing_Sub_Domain_v1', 'rpm_Customer_Billing_Sub_Domain_v1', 'http://xmlschema.tmi.telus.com/xsd/Customer/Customer/Customer_Sub_Domain_v1', 'rpm_Customer_Sub_Domain_v1', 'http://xmlschema.tmi.telus.com/xsd/Product/ProductOffering/ProductOfferingExternalViewCommon_v5', 'rpm_ProductOfferingExternalViewCommon_vX', 'http://xmlschema.tmi.telus.com/xsd/common/exceptions/Exceptions_v1_0', 'rpm_Exceptions_v1_0', 'http://xmlschema.tmi.telus.com/xsd/Customer/BaseTypes/CustomerCommon_v1', 'rpm_CustomerCommon_v1', 'http://xmlschema.tmi.telus.com/xsd/Customer/BaseTypes/CustomerCommon_v2', 'rpm_CustomerCommon_v2', 'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v1', 'rpm_EnterpriseCommonTypes_v1', 'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/TelusCommonAddressTypes_v5', 'rpm_TelusCommonAddressTypes_v5', 'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v2', 'rpm_EnterpriseCommonTypes_v2', 'http://xmlschema.tmi.telus.com/xsd/Customer/Customer/CustomerExternalViewCommon_v5', 'rpm_CustomerExternalViewCommon_v5', 'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/TelusCommonAddressTypes_v4', 'rpm_TelusCommonAddressTypes_v4', 'http://xmlschema.tmi.telus.com/xsd/Customer/Customer/CustomerExternalViewCommon_v6', 'rpm_CustomerExternalViewCommon_v6', 'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5', 'rpm_EnterpriseCommonTypes_v5', 'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v7', 'rpm_EnterpriseCommonTypes_v7'};
        public rpm_EquipmentRepairInformationSvcRequest.validateRepairResponse_element validateRepair(String productId,String serialNumber,String equipmentTypeId,String equipmentTrackingTypeId,String userTypeId,String productBrandId) {
            rpm_EquipmentRepairInformationSvcRequest.validateRepair_element request_x = new rpm_EquipmentRepairInformationSvcRequest.validateRepair_element();
            rpm_EquipmentRepairInformationSvcRequest.validateRepairResponse_element response_x;
            request_x.productId = productId;
            request_x.serialNumber = serialNumber;
            request_x.equipmentTypeId = equipmentTypeId;
            request_x.equipmentTrackingTypeId = equipmentTrackingTypeId;
            request_x.userTypeId = userTypeId;
            request_x.productBrandId = productBrandId;
            Map<String, rpm_EquipmentRepairInformationSvcRequest.validateRepairResponse_element> response_map_x = new Map<String, rpm_EquipmentRepairInformationSvcRequest.validateRepairResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'validateRepair',
              'http://xmlschema.tmi.telus.com/srv/RMO/LifeCycleMgmt/EquipmentRepairInformationSvcRequestResponse_v3',
              'validateRepair',
              'http://xmlschema.tmi.telus.com/srv/RMO/LifeCycleMgmt/EquipmentRepairInformationSvcRequestResponse_v3',
              'validateRepairResponse',
              'rpm_EquipmentRepairInformationSvcRequest.validateRepairResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x;
        }
        public rpm_ping_v1.pingResponse_element ping() {
            rpm_ping_v1.ping_element request_x = new rpm_ping_v1.ping_element();
            rpm_ping_v1.pingResponse_element response_x;
            Map<String, rpm_ping_v1.pingResponse_element> response_map_x = new Map<String, rpm_ping_v1.pingResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'ping',
              'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/ping_v1',
              'ping',
              'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/ping_v1',
              'pingResponse',
              'rpm_ping_v1.pingResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x;
        }
        public rpm_EquipmentRepairInformationSvcRequest.getRepairStatusResponse_element getRepairStatus(rpm_EquipmentRepairInformationSvcRequest.RepairIdList repairIdList) {
            rpm_EquipmentRepairInformationSvcRequest.getRepairStatus_element request_x = new rpm_EquipmentRepairInformationSvcRequest.getRepairStatus_element();
            rpm_EquipmentRepairInformationSvcRequest.getRepairStatusResponse_element response_x;
            request_x.repairIdList = repairIdList;
            Map<String, rpm_EquipmentRepairInformationSvcRequest.getRepairStatusResponse_element> response_map_x = new Map<String, rpm_EquipmentRepairInformationSvcRequest.getRepairStatusResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'getRepairStatus',
              'http://xmlschema.tmi.telus.com/srv/RMO/LifeCycleMgmt/EquipmentRepairInformationSvcRequestResponse_v3',
              'getRepairStatus',
              'http://xmlschema.tmi.telus.com/srv/RMO/LifeCycleMgmt/EquipmentRepairInformationSvcRequestResponse_v3',
              'getRepairStatusResponse',
              'rpm_EquipmentRepairInformationSvcRequest.getRepairStatusResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x;
        }
        public rpm_EquipmentRepairInformationSvcRequest.getRepairResponse_element getRepair(String repairId,String languageCode) {
            rpm_EquipmentRepairInformationSvcRequest.getRepair_element request_x = new rpm_EquipmentRepairInformationSvcRequest.getRepair_element();
            rpm_EquipmentRepairInformationSvcRequest.getRepairResponse_element response_x;
            request_x.repairId = repairId;
            request_x.languageCode = languageCode;
            Map<String, rpm_EquipmentRepairInformationSvcRequest.getRepairResponse_element> response_map_x = new Map<String, rpm_EquipmentRepairInformationSvcRequest.getRepairResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'getRepair',
              'http://xmlschema.tmi.telus.com/srv/RMO/LifeCycleMgmt/EquipmentRepairInformationSvcRequestResponse_v3',
              'getRepair',
              'http://xmlschema.tmi.telus.com/srv/RMO/LifeCycleMgmt/EquipmentRepairInformationSvcRequestResponse_v3',
              'getRepairResponse',
              'rpm_EquipmentRepairInformationSvcRequest.getRepairResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x;
        }
    }
}