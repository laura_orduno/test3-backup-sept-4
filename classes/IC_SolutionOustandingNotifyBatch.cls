global class IC_SolutionOustandingNotifyBatch implements Database.Batchable<sObject>{
     private final String query; 
     private final String status; 
     private final String recodTypeName; 
     private final Integer daysAfter; 
    
     // Batch Constructor
     global IC_SolutionOustandingNotifyBatch(String q, String Status, String RecodTypeName){
         this.query = q;
         this.status = status;
         this.recodTypeName = RecodTypeName;
     }
     
     // Start Method
     global Database.QueryLocator start(Database.BatchableContext BC){
         return Database.getQueryLocator(query);
     }
    
   // Execute Logic
    global void execute(Database.BatchableContext BC, List<sObject>scope){
        List<IC_Solution__c> icsItemstoUpdate = new List<IC_Solution__c>();
        Map<id,Sobject> notifyMap = new Map<id,Sobject>();  
        for(Sobject s : scope){
            notifyMap.put((Id)s.get('Id'),s);
        }
        String commentFieldName = 'Device_Comment__c';
        Set<id> idlist = notifyMap.keySet();
        //Filter the records which comments field update has been made less than or equals to Follow up days which emails should not sent
        String filterQuery = 'SELECT parentId,  Field, newValue, CreatedDate FROM IC_Solution__History where parentId in :idlist and Field =:commentFieldName and CreatedDate >= LAST_N_DAYS:'+IC_SolutionRemindNotifyScheduler.FOLLOWUP_DAY;
        List<Sobject> filterRecods = Database.query(filterQuery);
        
        //Remove the items which emails should not be sent to 
        for(Sobject fs : filterRecods){
            notifyMap.remove((Id)fs.get('parentId'));
        }
        for(Sobject s : notifyMap.values()){
            IC_SolutionTriggerHandler.ICSItem icsItem  = new IC_SolutionTriggerHandler.ICSItem();
            icsItem.Id =(Id) s.get('Id');
            icsItem.Name = (String)s.get('Name');
            String ProviderContactEmails =(String) s.getSObject('Inventory_Item__r').getSObject('Device_Provider__r').get('ICS_Notification_Email__c');         
            icsItem.ProviderContactEmails =ProviderContactEmails.split(',');
            System.debug('icsItem : ' + icsItem);
            IC_NotificationHelper.sendRemindNotificationEmail(icsItem);
            IC_Solution__c icsRecordUpdated = new IC_Solution__c(id=icsItem.id);
            icsRecordUpdated.Provider_Reminder_Notification_Date__c =  DateTime.Now();
            icsItemstoUpdate.add(icsRecordUpdated);   
        }
        update icsItemstoUpdate;
    }
   
    global void finish(Database.BatchableContext BC){
        // Get the ID of the AsyncApexJob representing this batch job
        // from Database.BatchableContext.
        // Query the AsyncApexJob object to retrieve the current job's information.
       AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
          TotalJobItems, CreatedBy.Email
          FROM AsyncApexJob WHERE Id =
          :BC.getJobId()];
       // Send an email to the Apex job's submitter notifying of job completion.
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       String[] toAddresses = new String[] {a.CreatedBy.Email};
       mail.setToAddresses(toAddresses);
       mail.setSubject('Apex Sharing Recalculation ' + a.Status);
       mail.setPlainTextBody
       ('The batch Apex job processed ' + a.TotalJobItems +
       ' batches with '+ a.NumberOfErrors + ' failures.');
       //Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
     
}