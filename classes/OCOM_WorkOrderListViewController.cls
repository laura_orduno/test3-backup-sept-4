/*
    ###########################################################################
    # Start : 
    # Project Name .........: Rendezvous Dispatch Project
    # Created for...........: US # 01 - Vlocity Order Dispatch Status
    # Created by............: Rajan Tatuskar (Tech Mahindra)
    # Created Date..........: 07-Feb-2017
    # Last Modified Date....: 16-Feb-2017 (Umesh Atry - TechMahindra)
    # Description ..........: Apex-class for show workorder(WFM) list with pagination and search functionality.
    ############################################################################
*/
Public Class OCOM_WorkOrderListViewController{
        public String OrderID {get;set;}
        public List<WorkOrder> workOrderList;
        public string deleteWorkOrderId{get;set;}
        public Integer totalPages {get;set;}
        public Integer currentPage {get;set;}
        public Boolean hasWorkOrders{get{ 
            if (workOrderList!=null && workOrderList.size()>0){
                return true;
            } else {
                return false;               
            }
        }}
        
        public string searchText
        {
        get
        {
        if (searchText==null) searchText = '';
        return searchText;
        }
        set;
        }
        
        public void init(){
            OrderID = Apexpages.currentPage().getParameters().get('OrderID');
            
            if(OrderID != null){
                system.debug('OCOM_WorkOrderListViewController::constructor() order Id('+OrderID+')');
                String queryStr = 'SELECT id, WFM_Number__c, name, Scheduled_Datetime__c, Product_Name__c, Permanent_Comments__c, Cancellation_Remarks__c, Duration__c, Location_Contact__c, Location_Contact__r.Name, Status__c,Dispatch_Status__c,Onsite_contact_Name__c,Onsite_Contact_Email__c,Onsite_Contact_cellPhone__c  FROM Work_Order__c  WHERE Order__c = \''+ OrderID + '\' order by name limit 10000';
                system.debug('OCOM_WorkOrderListViewController::constructor() queryStr :-->  ' + queryStr);
                getWorkOrders(queryStr);
                system.debug('OCOM_WorkOrderListViewController::constructor() workOrderList('+workOrderList+')');
            }
        }
        
        public OCOM_WorkOrderListViewController(){
            init();
        
        }
        
        public void deleteWorkOrder(){
            if (String.isNotEmpty(deleteWorkOrderId)) {
                List<Work_Order__c> deleteWorkOrderList = [SELECT id FROM Work_Order__c WHERE id = :deleteWorkOrderId];
                if(deleteWorkOrderList.size() > 0){
                    delete deleteWorkOrderList;
                }
            }        
        }
        
        // instantiate the StandardSetController from a query locator
    public ApexPages.StandardSetController con {get;set;}
    public void getWorkOrders(String queryStr){
        
        con = new ApexPages.StandardSetController(Database.getQueryLocator(queryStr));
        
        Integer pageSize = 50;
        Integer totalRec = con.getResultSize(); 
        if(totalRec > 0){
            con.setPageSize(pageSize);          
            Integer reminder = Math.Mod(totalRec,pageSize);
            if(reminder > 0){
                totalPages = (totalRec/pageSize)+1;
            }else{
                totalPages = (totalRec/pageSize);
            }
        }else{
            con.setPageNumber(0);
            totalPages = 0;
            currentPage = 0;
        }
    }
    
    public PageReference Search(){
		try{
			String queryStr = '';
			String strFilter = '';
			if(searchText != null && (searchText).trim() !='')  {
			   strFilter  = strFilter  +  ' WFM_Number__c Like \'%'+searchText.deleteWhitespace()+'%\' Or name like \'%'+searchText.deleteWhitespace()+'%\'' ;
			}
		   
			if(strFilter != '') {
				queryStr = 'SELECT id, WFM_Number__c, name, Scheduled_Datetime__c, Product_Name__c, Permanent_Comments__c, Cancellation_Remarks__c, Duration__c, Location_Contact__c, Location_Contact__r.Name, Status__c,Dispatch_Status__c,Onsite_contact_Name__c,Onsite_Contact_Email__c,Onsite_Contact_cellPhone__c FROM Work_Order__c  WHERE Order__c = \''+ OrderID + '\' and ('+ strFilter +') order by name limit 10000';
				System.debug('Search Related Query ---->'+ queryStr );             
				getWorkOrders(queryStr);
			}  
		}catch(Exception e){
			system.debug('ERROR OCCURED in Search Method which is following : ->>> '+e);
			return null;
		}
       return null;
    }
    
    public PageReference reset(){
        searchText = '';
        init();     
        return null;
    }
    
    // returns the list of WorkOrder
    public List<WorkOrder> getWorkOrderList() {
		workOrderList = new List<WorkOrder>();
		try{			
			for (Work_Order__c currentWorkOrder : (List<Work_Order__c>)con.getRecords()){
				WorkOrder workOrder = new WorkOrder();
				workOrder.workOrderObj = currentWorkOrder;
				
				workOrder.statusIcon = getStatusImage(currentWorkOrder);
				workOrder.statusTooltip  = getStatusTooltip(currentWorkOrder);
				
				workOrderList.add(workOrder);
			}
			system.debug('----OCOM_WorkOrderListViewController::getWorkOrderList workOrderList.size() : '+ workOrderList.size());
		
        }catch(Exception e){
			system.debug('ERROR OCCURED in getWorkOrderList Method which is following : ->>> '+e);
		}
        return workOrderList;
    }
    
    // returns status icon based on due date and current status.
    public String getStatusImage(Work_Order__c currentWorkOrder){
        //String imageName = 'URLFOR($Resource.SRSColorImages, ' ; //'ColorImages/Red.jpg'
        if(!String.isBlank(currentWorkOrder.Dispatch_Status__c)){
            if(currentWorkOrder.Dispatch_Status__c.equalsIgnoreCase('OPEN')){
                if(currentWorkOrder.Scheduled_Datetime__c != null){
                    Integer intNoOfWorkingDays = findNoOfWorkingDays(Date.TODAY(), currentWorkOrder.Scheduled_Datetime__c.DATE());      
                    if(intNoOfWorkingDays > 1)
                        return 'ColorImages/White.jpg';
                    else if(intNoOfWorkingDays == 1)
                        return 'ColorImages/Yellow.jpg';
                    else
                        return 'ColorImages/Red.jpg';
                }
            }else if(currentWorkOrder.Dispatch_Status__c.equalsIgnoreCase('TENTATIVE')){
                if(currentWorkOrder.Scheduled_Datetime__c != null){
                    Integer intNoOfWorkingDays = findNoOfWorkingDays(Date.TODAY(), currentWorkOrder.Scheduled_Datetime__c.DATE()); 
                    if(intNoOfWorkingDays > 1)
                        return 'ColorImages/Green.jpg';
                    else if(intNoOfWorkingDays == 1)
                        return 'ColorImages/Yellow.jpg';
                    else
                        return 'ColorImages/Red.jpg';
                }
            }else if(currentWorkOrder.Dispatch_Status__c.equalsIgnoreCase('DISPATCHED')){
                return 'ColorImages/Green.jpg';
            }else if(currentWorkOrder.Dispatch_Status__c.equalsIgnoreCase('ACCEPTED')){
                return 'ColorImages/Checkmark.jpg';
            }else if(currentWorkOrder.Dispatch_Status__c.equalsIgnoreCase('REJECTED')){
                return 'ColorImages/Grey.jpg';
            }else if(currentWorkOrder.Dispatch_Status__c.equalsIgnoreCase('COMPLETE')){
                return 'ColorImages/Green.jpg';
            }else if(currentWorkOrder.Dispatch_Status__c.equalsIgnoreCase('INCOMPLETE')){
                return 'ColorImages/Red.jpg';
            }else if(currentWorkOrder.Dispatch_Status__c.equalsIgnoreCase('CANCELLED')){
                return 'ColorImages/Cancelled.jpg';
            }else if(currentWorkOrder.Dispatch_Status__c.equalsIgnoreCase('EN_ROUTE')){
                return 'ColorImages/Green.jpg';
            }else if(currentWorkOrder.Dispatch_Status__c.equalsIgnoreCase('ON_SITE')){
                return 'ColorImages/Green.jpg';
            }
			
        }
        
        
        return null;
    }
    
    // # Description .........: Find total number of working days excluding weekends for range of dates
    public Integer findNoOfWorkingDays( Date startDate , Date endDate ) {
        
        system.debug('startDate ====>>>'+startDate);
        system.debug('endDate ====>>>'+endDate);
        Integer NoOfDays = 0;
        system.debug('$$$$$$$$$$$$$$$$$$$$$$$ findNoOfWorkingDays start ==== NoOfDays ' + NoOfDays);
        Date tempStartDate = startDate;
        for( integer i = 1; tempStartDate < endDate; i ++ ) {
            if(tempStartDate.daysBetween(tempStartDate.toStartofWeek()) == 0 || 
                tempStartDate.daysBetween(tempStartDate.toStartofWeek()) == -6) {
                    tempStartDate = tempStartDate.adddays(1); 
                    continue;
            }
            NoOfDays = NoOfDays+1; 
            tempStartDate = tempStartDate.adddays(1);
        }
        system.debug('$$$$$$$$$$$$$$$$$$$$$$$ findNoOfWorkingDays end ==== NoOfDays ' + NoOfDays);
        return NoOfDays ;
    }

    // returns status tooltip based on due date and current status.
    public String getStatusTooltip(Work_Order__c currentWorkOrder){
        if(!String.isBlank(currentWorkOrder.Dispatch_Status__c)){
            if(currentWorkOrder.Dispatch_Status__c.equalsIgnoreCase('OPEN')){
                return Label.OCOM_Dispatch_Status_Open_Tooltip;
            }else if(currentWorkOrder.Dispatch_Status__c.equalsIgnoreCase('TENTATIVE')){
                return Label.OCOM_Dispatch_Status_Tentative_Tooltip;
            }else if(currentWorkOrder.Dispatch_Status__c.equalsIgnoreCase('DISPATCHED')){
                return Label.OCOM_Dispatch_Status_Dispatched_Tooltip;
            }else if(currentWorkOrder.Dispatch_Status__c.equalsIgnoreCase('ACCEPTED')){
                return Label.OCOM_Dispatch_Status_Accepted_Tooltip;
            }else if(currentWorkOrder.Dispatch_Status__c.equalsIgnoreCase('REJECTED')){
                return Label.OCOM_Dispatch_Status_Rejected_Tooltip;
            }else if(currentWorkOrder.Dispatch_Status__c.equalsIgnoreCase('COMPLETE')){
                return Label.OCOM_Dispatch_Status_Completed_Tooltip;
            }else if(currentWorkOrder.Dispatch_Status__c.equalsIgnoreCase('INCOMPLETE')){
                return Label.OCOM_Dispatch_Status_Incomplete_Tooltip;
            }else if(currentWorkOrder.Dispatch_Status__c.equalsIgnoreCase('CANCELLED')){
                return Label.OCOM_Dispatch_Status_Cancelled_Tooltip;
            }else if(currentWorkOrder.Dispatch_Status__c.equalsIgnoreCase('EN_ROUTE')){
                return Label.OCOM_Dispatch_Status_En_Route_Tooltip;
            }else if(currentWorkOrder.Dispatch_Status__c.equalsIgnoreCase('ON_SITE')){ 
                return Label.OCOM_Dispatch_Status_On_Site_Tooltip;
            }
        }
        
        return null;
    }
    
    // directly goes to page number entered by user.
    public PageReference gotoPage(){
         system.debug('--Rajan----gotoPage()----currentPage---' + currentPage);
         con.setPageNumber(currentPage);
         return null;
    }
     
    // indicates whether there are more records after the current page set.
    public Boolean hasNext {
        get {
            return con.getHasNext();
        }
        set;
    }

    // indicates whether there are more records before the current page set.
    public Boolean hasPrevious {
        get {
            return con.getHasPrevious();
        }
        set;
    }

    // returns the page number of the current page set
    public Integer pageNumber {
        get {
            return con.getPageNumber();
        }
        set;
    }

    // returns the first page of records
     public void first() {
         con.first();
     }

     // returns the last page of records
     public void last() {
         con.last();
     }

     // returns the previous page of records
     public void previous() {
         con.previous();
     }

     // returns the next page of records
     public void next() {
         con.next();
     }

     // returns the PageReference of the original page, if known, or the home page.
     public void cancel() {
         con.cancel();
     }
     
     public class WorkOrder{
         public Work_Order__c workOrderObj {get;set;}
         public String statusIcon {get;set;}
         public String statusTooltip {get;set;}
     }
}