/*
 * Tests for trac_RepairQuoteCtlr
 * Part of the SMB Repairs project
 *
 * @author: Grant Adamson, Traction On Demand 
 */

@isTest
private class trac_RepairQuoteCtlrTest {	
	private static testMethod void testCtor() {
		Case c = new Case(subject = 'test');
		insert c;
		
		trac_PageUtils.setParam('id',c.id);
		trac_PageUtils.setParam('status','accept');
		
		trac_RepairQuoteCtlr ctlr = new trac_RepairQuoteCtlr();
	}
	
	
	private static testMethod void testCtorNoStatus() {
		Case c = new Case(subject = 'test');
		insert c;
		
		trac_PageUtils.setParam('id',c.id);
		
		Boolean caughtException = false;
		try {
			trac_RepairQuoteCtlr ctlr = new trac_RepairQuoteCtlr();
		} catch (Exception e) {
			caughtException = true;
			system.assertEquals(Label.NO_STATUS, e.getMessage());
		}
		system.assert(caughtException);
	}
	
	
	private static testMethod void testCtorInvalidStatus() {
		Case c = new Case(subject = 'test');
		insert c;
		
		trac_PageUtils.setParam('id',c.id);
		trac_PageUtils.setParam('status','bad');
		
		Boolean caughtException = false;
		try {
			trac_RepairQuoteCtlr ctlr = new trac_RepairQuoteCtlr();
		} catch (Exception e) {
			caughtException = true;
			system.assertEquals(Label.INVALID_STATUS, e.getMessage());
		}
		system.assert(caughtException);
	}
	
	
	private static testMethod void testDoUpdateQuoteStatus() {
		system.runas(getTestUser()) {
			trac_RepairTestUtils.createTestEndpoint();
			
			Case c = new Case(repair__c = trac_RepairTestUtils.REPAIR_ID_SUCCESS);
			insert c;
			
			trac_PageUtils.setParam('id',c.id);
			trac_PageUtils.setParam('status','accept');
			
			trac_RepairQuoteCtlr ctlr = new trac_RepairQuoteCtlr();
			
			ctlr.doUpdateQuoteStatus();
			
			system.assert(ctlr.success);
			system.assertEquals(false,ApexPages.hasMessages(ApexPages.Severity.ERROR));
		}
	}
	
	
	private static testMethod void testDoUpdateQuoteStatusFailure() {
		system.runas(getTestUser()) {
			trac_RepairTestUtils.createTestEndpoint();
			
			Case c = new Case(repair__c = trac_RepairTestUtils.REPAIR_ID_FAILURE);
			insert c;
			
			trac_PageUtils.setParam('id',c.id);
			trac_PageUtils.setParam('status','accept');
			
			trac_RepairQuoteCtlr ctlr = new trac_RepairQuoteCtlr();
			
			ctlr.doUpdateQuoteStatus();
			
			system.assertEquals(false,ctlr.success);
			system.assertEquals(true,ApexPages.hasMessages(ApexPages.Severity.ERROR));
		}
	}
	
	
	private static testMethod void testDoUpdateQuoteStatusMissingRepairId() {
		system.runas(getTestUser()) {
			trac_RepairTestUtils.createTestEndpoint();
			
			Case c = new Case();
			insert c;
			
			trac_PageUtils.setParam('id',c.id);
			trac_PageUtils.setParam('status','accept');
			
			trac_RepairQuoteCtlr ctlr = new trac_RepairQuoteCtlr();
			
			ctlr.doUpdateQuoteStatus();
			
			system.assertEquals(false,ctlr.success);
			system.assertEquals(true,ApexPages.hasMessages(ApexPages.Severity.ERROR));
			system.assertEquals(Label.REPAIR_ID_REQUIRED, ApexPages.getMessages()[0].getSummary());
		}
	}

	
	@istest
	private static User getTestUser() {
		Id PROFILEID = [SELECT id FROM Profile WHERE name='Standard User' OR name='Utilisateur standard' LIMIT 1].id;
		return new User(username = 'NOIDVMDFJREFSRI@TRACTIONSM.COM',
						  email = 'test@example.com',
						  title = 'test',
						  lastname = 'test',
						  alias = 'test',
						  TimezoneSIDKey = 'America/Los_Angeles',
						  LocaleSIDKey = 'en_US',
						  EmailEncodingKey = 'UTF-8',
						  ProfileId = PROFILEID,
						  LanguageLocaleKey = 'en_US',
						  Team_TELUS_Id__c = 'test'); // Need this id populated
	}
}