/**
* @author Santosh Rath
*
*/
public class OrdrTnServiceAddress {
    private String salesforceAddressId;
    
    private String addressId;
	private String externalAddressId;
	private String externalServiceAddressId;
	private String COID;
	private String switchNumber;
	private String npa;
    private String provinceStateCode;
	private String municipalityName;
	private String rateCenter;
	private String switchType;
    private List<OrdrTnNpaNxx> npaNxxList;
    private List<OrdrTnLineNumber> lineNumberList;
    private OrdrTnNpaNxx selectedOrdrTnNpaNxx;
    private OrdrTnNpaNxx rpaNxxToUseForRequestingTns;
    private OrdrTnNpaNxx defaultNpaNxx;
    private String lineNumberPrefix;
    private String[] availableNpas;
    private String[] availableNxas;
    private String selectedNpa;
    private String selectedNxx;
    private List<String> availableNpaList;
    private List<String> availableNxxList;
    private List<String> availableNpaNxxList;
    private List<String> availableTnsList;
    private Boolean noSpareTnFlag=false;
	private String fmsStreet;
	private String smbCareId;
	private String countryCode;
	private String postalZipCode;
	private String unitNumber;	
	private String streetNumberSuffix;
	private String streetNumber;
	private String streetName;
	
	private String servingCOID;
    private  SMBCare_Address__c addressObj;
    
    public SMBCare_Address__c getAddressObj() {
		return addressObj;
	}

	public void setAddressObj(SMBCare_Address__c addressObj) {
		this.addressObj = addressObj;
	}
	public String getServingCOID() {
		return servingCOID;
	}

	public void setServingCOID(String servingCOID) {
		this.servingCOID = servingCOID;
	}
    /**
     * Gets the unitNumber value for this Address.
     * 
     * @return unitNumber
     */
    public String getUnitNumber() {
        return unitNumber;
    }


    /**
     * Sets the unitNumber value for this Address.
     * 
     * @param unitNumber
     */
    public void setUnitNumber(String unitNumber) {
        this.unitNumber = unitNumber;
    }

	/**
     * Gets the postalZipCode value for this Address.
     * 
     * @return postalZipCode
     */
    public String getPostalZipCode() {
        return postalZipCode;
    }


    /**
     * Sets the postalZipCode value for this Address.
     * 
     * @param postalZipCode
     */
    public void setPostalZipCode(String postalZipCode) {
        this.postalZipCode = postalZipCode;
    }
  
    /**
     * Gets the streetNumberSuffix value for this ServiceAddress.
     * 
     * @return streetNumberSuffix
     */
    public String getStreetNumberSuffix() {
        return streetNumberSuffix;
    }


    /**
     * Sets the streetNumberSuffix value for this ServiceAddress.
     * 
     * @param streetNumberSuffix
     */
    public void setStreetNumberSuffix(String streetNumberSuffix) {
        this.streetNumberSuffix = streetNumberSuffix;
    }
	   /**
     * Gets the streetNumber value for this Address.
     * 
     * @return streetNumber
     */
    public String getStreetNumber() {
        return streetNumber;
    }


    /**
     * Sets the streetNumber value for this Address.
     * 
     * @param streetNumber
     */
    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }
	    /**
     * Gets the streetName value for this Address.
     * 
     * @return streetName
     */
    public String getStreetName() {
        return streetName;
    }


    /**
     * Sets the streetName value for this Address.
     * 
     * @param streetName
     */
    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

	/**
	* Generic Service address webservice constructor
	*/
    public OrdrTnServiceAddress() {
    }



    public Boolean getNoSpareTnFlag() {
        return noSpareTnFlag;
    }
    public void setNoSpareTnFlag(Boolean noSpareTnFlag) {
        this.noSpareTnFlag = noSpareTnFlag;
    }
    public List<String> getAvailableTnsList() {
        return availableTnsList;
    }
    public void setAvailableTnsList(List<String> availableTnsList) {
        this.availableTnsList = availableTnsList;
    }
    public List<String> getAvailableNpaNxxList() {
        return availableNpaNxxList;
    }
    public void setAvailableNpaNxxList(List<String> availableNpaNxxList) {
        this.availableNpaNxxList = availableNpaNxxList;
    }
    public List<String> getAvailableNpaList() {
        return availableNpaList;
    }
    public void setAvailableNpaList(List<String> availableNpaList) {
        this.availableNpaList = availableNpaList;
    }
    public List<String> getAvailableNxxList() {
        return availableNxxList;
    }
    public void setAvailableNxxList(List<String> availableNxxList) {
        this.availableNxxList = availableNxxList;
    }
    public String getSelectedNpa() {
        return selectedNpa;
    }
    public void setSelectedNpa(String selectedNpa) {
        this.selectedNpa = selectedNpa;
    }
    public String getSelectedNxx() {
        return selectedNxx;
    }
    public void setSelectedNxx(String selectedNxx) {
        this.selectedNxx = selectedNxx;
    }
    public String[] getAvailableNpas() {
        return availableNpas;
    }
    public void setAvailableNpas(String[] availableNpas) {
        this.availableNpas = availableNpas;
    }
    public String[] getAvailableNxas() {
        return availableNxas;
    }
    public void setAvailableNxas(String[] availableNxas) {
        this.availableNxas = availableNxas;
    }
    public List<OrdrTnNpaNxx> getNpaNxxList() {
        return npaNxxList;
    }
    public void setNpaNxxList(List<OrdrTnNpaNxx> npaNxxList) {
        this.npaNxxList = npaNxxList;
    }
    public List<OrdrTnLineNumber> getLineNumberList() {
        return lineNumberList;
    }
    public void setLineNumberList(List<OrdrTnLineNumber> lineNumberList) {
        this.lineNumberList = lineNumberList;
    }
    public OrdrTnNpaNxx getSelectedOrdrTnNpaNxx() {
        return selectedOrdrTnNpaNxx;
    }
    public void setSelectedOrdrTnNpaNxx(OrdrTnNpaNxx selectedOrdrTnNpaNxx) {
        this.selectedOrdrTnNpaNxx = selectedOrdrTnNpaNxx;
    }
    public OrdrTnNpaNxx getRpaNxxToUseForRequestingTns() {
        return rpaNxxToUseForRequestingTns;
    }
    public void setRpaNxxToUseForRequestingTns(
        OrdrTnNpaNxx rpaNxxToUseForRequestingTns) {
            this.rpaNxxToUseForRequestingTns = rpaNxxToUseForRequestingTns;
        }
    public OrdrTnNpaNxx getDefaultNpaNxx() {
        return defaultNpaNxx;
    }
    public void setDefaultNpaNxx(OrdrTnNpaNxx defaultNpaNxx) {
        this.defaultNpaNxx = defaultNpaNxx;
    }
    public String getLineNumberPrefix() {
        return lineNumberPrefix;
    }
    public void setLineNumberPrefix(String lineNumberPrefix) {
        this.lineNumberPrefix = lineNumberPrefix;
    }
	 public String getFmsStreet() {
		return fmsStreet;
	}

	public void setFmsStreet(String fmsStreet) {
		this.fmsStreet = fmsStreet;
	}
	 public String getSmbCareId() {
		return smbCareId;
	}

	public void setSmbCareId(String smbCareId) {
		this.smbCareId = smbCareId;
	}
    /**
* Gets the addressId value for this Address.
*
* @return addressId
*/
    public String getAddressId() {
        return addressId;
    }
    /**
* Sets the addressId value for this Address.
*
* @param addressId
*/
    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }
    /**
* Gets the externalAddressId value for this Address.
*
* @return externalAddressId
*/
    public String getExternalAddressId() {
        return externalAddressId;
    }
    /**
* Sets the externalAddressId value for this Address.
*
* @param externalAddressId
*/
    public void setExternalAddressId(String externalAddressId) {
        this.externalAddressId = externalAddressId;
    }
    /**
* Gets the externalServiceAddressId value for this Address.
*
* @return externalServiceAddressId
*/
    public String getExternalServiceAddressId() {
        return externalServiceAddressId;
    }
    /**
* Sets the externalServiceAddressId value for this Address.
*
* @param externalServiceAddressId
*/
    public void setExternalServiceAddressId(String externalServiceAddressId) {
        this.externalServiceAddressId = externalServiceAddressId;
    }
  
    /**
* Gets the COID value for this ResourceLocationAddressComponent.
*
* @return COID
*/
    public String getCOID() {
        return COID;
    }
    /**
* Sets the COID value for this ResourceLocationAddressComponent.
*
* @param COID
*/
    public void setCOID(String COID) {
        this.COID = COID;
    }
    /**
* Gets the switchNumber value for this ServiceAddress.
*
* @return switchNumber
*/
    public String getSwitchNumber() {
        return switchNumber;
    }
    /**
* Sets the switchNumber value for this ServiceAddress.
*
* @param switchNumber
*/
    public void setSwitchNumber(String switchNumber) {
        this.switchNumber = switchNumber;
    }
    /**
* Gets the npa value for this ServiceAddress.
*
* @return npa
*/
    public String getNpa() {
        return npa;
    }
    /**
* Sets the npa value for this ServiceAddress.
*
* @param npa
*/
    public void setNpa(String npa) {
        this.npa = npa;
    }
    public String getSalesforceAddressId() {
        return salesforceAddressId;
    }
    public void setSalesforceAddressId(String salesforceAddressId) {
        this.salesforceAddressId = salesforceAddressId;
    }

	 /**
     * Gets the provinceStateCode value for this Address.
     * 
     * @return provinceStateCode
     */
    public String getProvinceStateCode() {
        return provinceStateCode;
    }


    /**
     * Sets the provinceStateCode value for this Address.
     * 
     * @param provinceStateCode
     */
    public void setProvinceStateCode(String provinceStateCode) {
        this.provinceStateCode = provinceStateCode;
    }

	   /**
     * Gets the municipalityName value for this Address.
     * 
     * @return municipalityName
     */
    public String getMunicipalityName() {
        return municipalityName;
    }


    /**
     * Sets the municipalityName value for this Address.
     * 
     * @param municipalityName
     */
    public void setMunicipalityName(String municipalityName) {
        this.municipalityName = municipalityName;
    }
	 /**
     * Gets the rateCenter value for this ServiceAddress.
     * 
     * @return rateCenter
     */
    public String getRateCenter() {
        return rateCenter;
    }


    /**
     * Sets the rateCenter value for this ServiceAddress.
     * 
     * @param rateCenter
     */
    public void setRateCenter(String rateCenter) {
        this.rateCenter = rateCenter;
    }

	/**
     * Gets the switchType value for this ServiceAddress.
     * 
     * @return switchType
     */
    public String getSwitchType() {
        return switchType;
    }


    /**
     * Sets the switchType value for this ServiceAddress.
     * 
     * @param switchType
     */
    public void setSwitchType(String switchType) {
        this.switchType = switchType;
    }
	
    /**
     * Gets the countryCode value for this Address.
     * 
     * @return countryCode
     */
    public String getCountryCode() {
        return countryCode;
    }


    /**
     * Sets the countryCode value for this Address.
     * 
     * @param countryCode
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
	
   
}