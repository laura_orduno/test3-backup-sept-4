public class vlocity_addon_DynamicTableHelper_CP {
    public static string HEADERSTRING = 'headermap';
    public static string FORMATCOLUMN = 'formatcolumn'; 
    public static string TABLESTYLE  = 'tablestyle';
    public static string HEADERSTYLE  = 'headerstyle';
    public static string TBLHEADERSTYLE  = 'tblHeaderStyle';
    public static string TBLROWSTYLE  = 'tblRowStyle';
    public static string TBLCELLSTYLE  = 'tableCellStyle';
    
    public static map<string,object> getHwAllotmentData(string contractId){
        map<string,object> mapTableInfo=new  map<string,object>();
        map<string,string> headerTextAllignMap = new map<string, string>();
        String tableCssStyle = 'border: 1px solid black;width: 100%;border-collapse: collapse;';
        String headerCssStyle='border-bottom: 1px solid black;border-right: 1px solid black;text-align:left;font-weight: bold;';          
        String tableHeaderStyle='font-size:8pt;font-family:Arial,sans-serif;';
        string tableRowStyle='';     
        string tableCellStyle='font-size:8pt;font-family:Arial,sans-serif;border: 1px solid black;border-right: 1px solid black;border-bottom: 1px solid black;';
        Map<Id , Agreement_Hardware_Allotment__c> hwAllotmentMap=new Map<Id , Agreement_Hardware_Allotment__c>();
        list<sobject> lstSobject=new list<sobject>();
        
        mapTableInfo.put(HEADERSTRING,new Map<String, String>{'Calculation Method'=> 'Calculation_Type__c',       
            												  'Allowed Units'=> 'Allowed_Hardware_Units__c',
            												  'Eligibility Period'=>'Eligibility_Period__c'                     
            												 });
        
        mapTableInfo.put('groupBy','NA');
        
        if(contractId != null){
            for (Agreement_Hardware_Allotment__c hwAllotment: [Select id,name,Calculation_Type__c ,	
                                                               Allowed_Hardware_Units__c,	Eligibility_Period__c 
                                                               from Agreement_Hardware_Allotment__c 
                                                               where Contract__c =:contractId and Type__c = 'Hardware Allotment' and Contract__r.Hardware_Refresh_Type__c includes ('Allotments') ]){
                                                                  
                                                                   hwAllotmentMap.put(hwAllotment.id,hwAllotment);
                                                                   lstSobject.add(hwAllotment);
                                                               
                                                               }
        }                                           
        
        
        mapTableInfo.put('items',lstSobject);
        //mapTableInfo.put('textAllign', headerTextAllignMap);
        mapTableInfo.put(TABLESTYLE, tableCssStyle ); 
        mapTableInfo.put(HEADERSTYLE, headerCssStyle ); 
        mapTableInfo.put(TBLHEADERSTYLE, tableHeaderStyle );
        mapTableInfo.put(TBLROWSTYLE, tableRowStyle );
        mapTableInfo.put(TBLCELLSTYLE, tableCellStyle);
        System.debug('List of ContreactLineItem' + lstSobject);
        return mapTableInfo;
        
    }
    
    public static map<string,object> getChurnAllotmentData(string contractId){
        map<string,object> mapTableInfo=new  map<string,object>();
        map<string,string> headerTextAllignMap = new map<string, string>();
        String tableCssStyle = 'border: 1px solid black;width: 100%;border-collapse: collapse;';
        String headerCssStyle='border-bottom: 1px solid black;border-right: 1px solid black;text-align:left;font-weight: bold;';          
        String tableHeaderStyle='font-size:8pt;font-family:Arial,sans-serif;';
        string tableRowStyle='';     
        string tableCellStyle='font-size:8pt;font-family:Arial,sans-serif;border: 1px solid black;border-right: 1px solid black;border-bottom: 1px solid black;';
        Map<Id , 	Agreement_Churn_Allotment__c> hwAllotmentMap=new Map<Id , 	Agreement_Churn_Allotment__c>();
        list<sobject> lstSobject=new list<sobject>();
        
        mapTableInfo.put(HEADERSTRING,new Map<String, String>{'Calculation Method'=> 'Calculation_Type__c',       
                                                                'Allowed Units'=> 'Allowed_Churn_Units__c',
            													'Applicable Deactivation Charge' => 'Deactivation_Charge__c'
                                                                });
        
        mapTableInfo.put('groupBy','NA');
        
        if(contractId != null){
            for (	Agreement_Churn_Allotment__c hwAllotment: [Select id,name,Calculation_Type__c ,	
                                                               Allowed_Churn_Units__c,	Eligibility_Period__c ,	Deactivation_Charge__c
                                                               from 	Agreement_Churn_Allotment__c 
                                                               where Contract__c =:contractId]){
                                                                   
                                                                   hwAllotmentMap.put(hwAllotment.id,hwAllotment);
                                                                   lstSobject.add(hwAllotment);
                                                                   
                                                               }
        }                                           
        
        
        mapTableInfo.put('items',lstSobject);
        //mapTableInfo.put('textAllign', headerTextAllignMap);
        mapTableInfo.put(TABLESTYLE, tableCssStyle ); 
        mapTableInfo.put(HEADERSTYLE, headerCssStyle ); 
        mapTableInfo.put(TBLHEADERSTYLE, tableHeaderStyle );
        mapTableInfo.put(TBLROWSTYLE, tableRowStyle );
        mapTableInfo.put(TBLCELLSTYLE, tableCellStyle);
        System.debug('List of ContreactLineItem' + lstSobject);
        return mapTableInfo;
        
    }
    
    public static map<string,object> getVoiceDataRatePlan( map<string,object> inputMap){
        map<string,object> mapTableInfo=new  map<string,object>();
        map<string,string> headerTextAllignMap = new map<string, string>();
        String tableCssStyle = 'border: 1px solid black;width: 100%;border-collapse: collapse;';//
        String headerCssStyle='border-bottom: 1px solid black;border-right: 1px solid black;text-align:left;font-weight: bold;';          
        String tableHeaderStyle='font-size:8pt;font-family:Arial,sans-serif;';
        string tableRowStyle='background-color:#dfb3ff;';     
        string tableCellStyle='font-size:8pt;font-family:Arial,sans-serif;border: 1px solid black;border-right: 1px solid black;border-bottom: 1px solid black;';
		string columnStyle = 'background-color:#66cc00;width:30%;';
        map<string,string> colToStyleMap = new map<string,string>{'col1' => columnStyle};
         id contractId = (id)inputMap.get('contextObjId');
        list<string> queryfields = (list<string>)inputMap.get('queryfields');
        list<string> filters = (list<string>) inputMap.get('filters');
       // set fieldsToValidate = queryfields.split(',');
        system.debug('FIlters*******' + filters);
        
        list<sobject> lstSobject=new list<sobject>();
        map<string,string> rowDataMap = new map<string,string>();
        list<map<string,string>> headerRowDataMap = new list<map<string,string>>();
        string fldLabel = '';
        string objName = 'vlocity_cmt__ContractLineItem__c';
        Map<String, List<Map<String, String>>> dataMap = new Map<String, List<Map<String, String>>>();    
        Map<id, List<Map<String, String>>> cliRowData = new Map<id, List<Map<String, String>>>(); 
        
        String cliQuery = Util.queryBuilder(ObjName, queryfields,  filters); 
        
        mapTableInfo.put(HEADERSTRING,new Map<String, String>{'A'=> '','B'=> ''});
        
        mapTableInfo.put('groupBy','NA');
        /*string cliQuery = 'Select ' + queryfields;
        cliquery += 'from vlocity_cmt__ContractLineItem__c';
        cliquery += 'where vlocity_cmt__ContractId__c'*/
            
        if(contractId != null){
            list<vlocity_cmt__ContractLineItem__c> cliList = database.query(cliQuery);
            for (	vlocity_cmt__ContractLineItem__c cli: cliList){
                                                                   
                                                               		system.debug('cli Id:: '+ cli.id );
                                                                   //hwAllotmentMap.put(hwAllotment.id,hwAllotment);
                                                                   //lstSobject.add(hwAllotment); 
                                                                   headerRowDataMap.clear();                                                                  
                                                                    for (string validationfield : queryfields){
                                                                        //system.debug('Field Name:: ' + validationfield);
                                                                        if( validationfield.equalsIgnoreCase('name') ||validationfield.equalsIgnoreCase('Monthly_Plan_Rate__c')  || validationfield.equalsIgnoreCase('RP_Included_Minutes__c') ){
                                                                            fldLabel = getfieldLabel(objName, validationfield);
                                                                            rowDataMap = addHeaderRowData(fldLabel,string.valueof(cli.get(validationfield) ));
                                                                            headerRowDataMap.add(rowDataMap);
                                                                        }
                                                         
                                                                        else if( validationfield.equalsIgnoreCase('RP_Included_Features_Select__c') && cli.get(validationfield)!=''&& cli.get(validationfield)!= null ){
                                                                            fldLabel = 'Included Features';
                                                                            
                                                                            String formattedSrvsString = formatMultiSelectfield(string.valueof(cli.get(validationfield)));
                                                                            rowDataMap = addHeaderRowData(fldLabel,formattedSrvsString);
                                                                            headerRowDataMap.add(rowDataMap);
                                                                        }
                                                                        else if( validationfield.equalsIgnoreCase('RP_Terms_and_Conditions__c') && cli.get(validationfield)!=''&& cli.get(validationfield)!= null ){
                                                                           
                                                                           	fldLabel = getfieldLabel(objName, validationfield); 
                                                                            String formattedSrvsString = formatMultiSelectfield(string.valueof(cli.get(validationfield)).replaceall('\r\n',';'));
                                                                            rowDataMap = addHeaderRowData(fldLabel,formattedSrvsString);
                                                                            headerRowDataMap.add(rowDataMap);
                                                                        }
                                                                        else if(cli.get(validationfield)!=''&& cli.get(validationfield)!= null){
                                                                            fldLabel = getfieldLabel(objName, validationfield);
                                                                            rowDataMap = addHeaderRowData(fldLabel,string.valueof(cli.get(validationfield) ));
                                                                            headerRowDataMap.add(rowDataMap);
                                                                            
                                                                        }
                                                                    }
                                                                 
                                                               cliRowData.put(cli.id,new list<map<string,string>>(headerRowDataMap)); 
                                                               	system.debug('cli name'+ headerRowDataMap[0]);
                                                               }
        }   
        
        for (id tst: cliRowData.keyset()){
            system.debug('headerRowDataMapNam______' + cliRowData.get(tst));
        }
        mapTableInfo.put('items',lstSobject);
        mapTableInfo.put('headerRowDataMap',cliRowData);
        //mapTableInfo.put('textAllign', headerTextAllignMap);
        mapTableInfo.put(TABLESTYLE, tableCssStyle ); 
        mapTableInfo.put(HEADERSTYLE, headerCssStyle ); 
        mapTableInfo.put(TBLHEADERSTYLE, tableHeaderStyle );
        mapTableInfo.put(TBLROWSTYLE, tableRowStyle );
        mapTableInfo.put(TBLCELLSTYLE, tableCellStyle);
        mapTableInfo.put('rowDriven', true);
        mapTableInfo.put('colStyle', colToStyleMap);
       // System.debug('List of ContreactLineItem' + lstSobject);
        return mapTableInfo;
        
    }
     
     public static map<string,object> getHwAllotmentOrFloatSumry(map<string,object> inputMap){
        map<string,object> mapTableInfo=new  map<string,object>();
        map<string,string> headerTextAllignMap = new map<string, string>();
        String tableCssStyle = 'border: 1px solid black;width: 100%;border-collapse: collapse;';//
        String headerCssStyle='border-bottom: 1px solid black;border-right: 1px solid black;text-align:left;font-weight: bold;';          
        String tableHeaderStyle='font-size:8pt;font-family:Arial,sans-serif;';
        string tableRowStyle='background-color:#dfb3ff;';     
        string tableCellStyle='font-size:8pt;font-family:Arial,sans-serif;border: 1px solid black;border-right: 1px solid black;border-bottom: 1px solid black;';
		string columnStyle = 'background-color:#66cc00;width:30%;';
        map<string,string> colToStyleMap = new map<string,string>{'col1' => columnStyle};
        id contractId = (id)inputMap.get('contextObjId');
        list<string> queryfields = (list<string>)inputMap.get('queryfields');
        list<string> filters = (list<string>) inputMap.get('filters');
		string sHWType = (string) inputMap.get('hwtype');
        system.debug('Filters*******' + filters);
        
        list<sobject> lstSobject=new list<sobject>();
        map<string,string> rowDataMap = new map<string,string>();
        list<map<string,string>> headerRowDataMap = new list<map<string,string>>();
        string fldLabel = '';
        string objName = 'Agreement_Hardware_Allotment__c';
        Map<String, List<Map<String, String>>> dataMap = new Map<String, List<Map<String, String>>>();    
        Map<id, List<Map<String, String>>> hwRowData = new Map<id, List<Map<String, String>>>(); 
        
        String hwQuery = Util.queryBuilder(ObjName, queryfields,  filters); 
        system.debug('hwQuery ***' + hwQuery );
        mapTableInfo.put(HEADERSTRING,new Map<String, String>{'A'=> '','B'=> ''});
        
        mapTableInfo.put('groupBy','NA');
            
        if(contractId != null){
            list<Agreement_Hardware_Allotment__c> hwList = database.query(hwQuery);
            for (	Agreement_Hardware_Allotment__c hw: hwList){
                                                                   
                system.debug('hw Id:: '+ hw.id );
                headerRowDataMap.clear();                                                                  
                for (string validationfield : queryfields){
                    
                    if( validationfield.equalsIgnoreCase('Allowed_Hardware_Units__c') ){
                        if(sHWType.equalsIgnoreCase(system.label.Contract_HW_Float_Type)){
                            rowDataMap = addHeaderRowData('Hardware Float',string.valueof(hw.get(validationfield) ));
                            headerRowDataMap.add(rowDataMap);
                        }
                        else if (sHWType.equalsIgnoreCase(system.label.Contract_HW_Allotment_Type)){
                          	rowDataMap = addHeaderRowData('Hardware Allotment',string.valueof(hw.get(validationfield) ));
                            headerRowDataMap.add(rowDataMap); 
                        }
                        
                    }
                    else if(validationfield.equalsIgnoreCase('Deal_Support__r.Device_Term__c')){
                        if(sHWType.equalsIgnoreCase(system.label.Contract_HW_Float_Type)){
                            string sHWFloatPrice='TELUS\' device price for activations on a '+ string.valueof(hw.getSobject('Deal_Support__r').get('Device_Term__c') )+' month Device Term as at the date the replacement Customer Device is activated';
                            rowDataMap = addHeaderRowData('Hardware Float Price',sHWFloatPrice);
                        }
                        else if(sHWType.equalsIgnoreCase(system.label.Contract_HW_Allotment_Type)){
                            string sHWFloatPrice='TELUS\' device price for activations on a '+ string.valueof(hw.getSobject('Deal_Support__r').get('Device_Term__c') )+' month Device Term as at the date the replacement Customer Device is activated';
                            rowDataMap = addHeaderRowData('Hardware Allotment Price',sHWFloatPrice);
                           
                        }
                         headerRowDataMap.add(rowDataMap);
                    }
                    else if(validationfield.equalsIgnoreCase('Eligibility_Period__c')){
						if(sHWType.equalsIgnoreCase(system.label.Contract_HW_Float_Type))
							rowDataMap = addHeaderRowData('Hardware Float Expiry Date','Expiration of the Agreement Term');
						else if(sHWType.equalsIgnoreCase(system.label.Contract_HW_Allotment_Type))
							rowDataMap = addHeaderRowData('Hardware Allotment Expiry Date','30th month from the Effective Date');
						
                        headerRowDataMap.add(rowDataMap);
                    }
					
                }
                
                hwRowData.put(hw.id,new list<map<string,string>>(headerRowDataMap)); 
             }
        }   
        for (id tst: hwRowData.keyset()){
            system.debug('headerRowDataMapNam______' + hwRowData.get(tst));
        }
        mapTableInfo.put('items',lstSobject);
        mapTableInfo.put('headerRowDataMap',hwRowData);
        mapTableInfo.put(TABLESTYLE, tableCssStyle ); 
        mapTableInfo.put(HEADERSTYLE, headerCssStyle ); 
        mapTableInfo.put(TBLHEADERSTYLE, tableHeaderStyle );
        mapTableInfo.put(TBLROWSTYLE, tableRowStyle );
        mapTableInfo.put(TBLCELLSTYLE, tableCellStyle);
        mapTableInfo.put('rowDriven', true);
        mapTableInfo.put('colStyle', colToStyleMap);
        return mapTableInfo;
        
    }
    
    
    
    public static Map<String,Object> buildDocumentSectionContent (Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options){                 
        String contextObjName = (String) inputMap.get('contextObjName');          
        Id contractId = (Id) inputMap.get('contextObjId'); 
        List<SObject> lineItems = (List<SObject>) inputMap.get('items');
        String docStyle = (String) inputMap.get('documentFontStyle');
        String groupByFieldName = (String)inputMap.get('groupBy');
        boolean rowDriven =false;
        
        Map<String,string> headerMap = (Map<String,String>)inputMap.get(HEADERSTRING);  
        map<string, Object> preparedataInputMap = new map<string,object>();
        Map<String,string> formatcolumnmap = new map<String,String>();
        Map<string,string> headerTextAllignMap = new map<string, string>();
        
        string sRowStyle='';
     
        if(inputMap.containsKey(FORMATCOLUMN))
            preparedataInputMap.put(FORMATCOLUMN,inputMap.get(FORMATCOLUMN))  ; 
        if(inputMap.containsKey('rowDriven')){
            	preparedataInputMap.put('rowDriven',inputMap.get('rowDriven'))  ; 
            	rowDriven = (boolean) inputMap.get('rowDriven');
        }
           

       
        if(inputMap.containsKey(FORMATCOLUMN))
            formatcolumnmap =  (Map<String,String>)inputMap.get(FORMATCOLUMN); 
        
        //prepare body
        Map<String, List<Map<String, String>>> dataMap = new Map<String, List<Map<String, String>>>();
        
        if(lineItems.size() > 0 && !lineItems.isEmpty() && !rowDriven)
            dataMap = prepareData(lineItems,groupByFieldName, headerMap,preparedataInputMap );
        else if(rowDriven == true && inputMap.containsKey('tableRowsMap')){
            dataMap = (map<string,List<map<string, string>>>) inputMap.get('tableRowsMap');
        }
       
        string tableCssStyle;
        string headerCssStyle;
        string tableHeaderStyle;
        string tableCellStyle;
        string columnStyle = '';
        map<string,string> colToStyleMap = new map<string,string>();
         
        if(inputMap.containsKey(TABLESTYLE))
             tableCssStyle  = (string)inputMap.get(TABLESTYLE);
        if(inputMap.containsKey(HEADERSTYLE))
             headerCssStyle = (string)inputMap.get(HEADERSTYLE);
        if(inputMap.containsKey(TBLHEADERSTYLE))
             tableHeaderStyle = (string)inputMap.get(TBLHEADERSTYLE); 
        if(inputMap.containsKey('textAllign'))     
             headerTextAllignMap =  (map<string,string>)inputMap.get('textAllign');
        if(inputMap.containsKey(TBLROWSTYLE))
            sRowStyle =  (string)inputMap.get(TBLROWSTYLE); 
        if(inputMap.containsKey(TBLCELLSTYLE))
           tableCellStyle = (string)inputMap.get(TBLCELLSTYLE);
        if(inputMap.containsKey('colStyle')){
            colToStyleMap = (map<string,string>)inputMap.get('colStyle');
        }
           
        headerCssStyle=headerCssStyle+tableHeaderStyle;
        system.debug('headerStyle::: '+headerCssStyle );
        system.debug('tableHeaderStyle::: '+tableHeaderStyle );
        
       /* if(rowDriven == true){
             headerCssStyle +=  'display:none;';       
        }*/
        String content = '';
        if(dataMap.size()>0 && !dataMap.isEmpty()) {
            content = '<p style=\"'+tableCssStyle+'\"> <table style=\"'+tableCssStyle+'\">';
           /* if(colToStyleMap.size()>0 && !colToStyleMap.isEmpty()){
               content += '<colgroup>';
            for(string colstyle: colToStyleMap.keyset()){
                
                content += '<col style=\"' + colToStyleMap.get(colstyle) +'\">';
            }
                content += '</col></colgroup>';
            }*/
            if(rowDriven == false) {
            content +='<thead><tr>';      
                for(String key: headerMap.keySet()){               
                    content +='<th style=\"'+headerCssStyle+'\">';
                    system.debug('Header style' + content );
                    content +='<viawrapper>'+key+'</viawrapper></th>';
                }
           

            	content +='</tr></thead>';
                 }
            //String tableCellStyle='font-size:10pt;font-family:Arial,sans-serif;border: 1px solid black;border-right: 1px solid black;border-bottom: 1px solid black;';
            String bodyStyle='<td style=\"'+tableCellStyle;
            content +='<tbody>';
            for(String key : dataMap.keySet()){
                if(groupByFieldName != 'Na' && key != null){
                    content+='<tr>';
                    content+=bodyStyle+'font-weight:bold;\">';
                    content+='<viawrapper>'+key+'</viawrapper></td>';
                   for (integer i=1; i<headerMap.size();i++ ){
                        content+=bodyStyle+'\">';
                        content+= '</td>';
                    }
                  content+='</tr>';
                }
                List<Map<String, String>> rowData = dataMap.get(key);
                boolean textSplit = false;
                for(Map<String, String> eachRow : rowData){
                    content+='<tr>';
                    for(String header : headerMap.keySet()){             
                        string sFormattedIdentifier=''; 
                        string textAllign = '';
                        if(headerTextAllignMap.containsKey(header) )
                           textAllign = headerTextAllignMap.get(header);
                        if(formatcolumnmap.containsKey(header)){
                            sFormattedIdentifier=formatcolumnmap.get(header);
                        }
   						
                        content+=bodyStyle; 
                       // system.debug('Header Value' + header + 'eachRow.get(header)::' + eachRow.get(header));
                        if(header == 'A') {
                            content+= 'background-color:#66cc00;width:200px;' + '\">';
                        }
                       
                        else{
                            content+='\">';
                        }
                       
                            content+='<viawrapper>'+ sFormattedIdentifier +'' +eachRow.get(header)+'</viawrapper></td>';
                    }
                    content+='</tr>';
                }
            }
            
            content +='</tbody></table></p>';
        }
        System.debug(' content is '+content);
        outMap.put('sectionContent', content);
        return outMap;
    }
    
    public static Map<String, List<Map<String, String>>> prepareData (List<SObject> items, string groupByName, map<string, string> headerMap, map<string,object>preparedataInputMap){
        String nameSpaceprefix = 'vlocity_cmt__';
        Map<String,string> formatColumnMap = new  Map<String,string> ();
        map<id,Decimal> cliLevelMap = new Map<id,decimal> ();

       
         if(preparedataInputMap.containsKey(FORMATCOLUMN) ){
            formatColumnMap = (map<string,string> )preparedataInputMap.get(FORMATCOLUMN);
        }

        
        Map<String, List<Map<String, String>>> statusMap = new Map<String, List<Map<String, String>>> ();
        for(SObject item : items){   
            string status ;
           
            //String status = (String) item.get(nameSpaceprefix+'Status__c');
            if(groupByName == 'Na')
                status = groupByName;
            else 
                status = (String) item.get(groupByName);
            
            if(statusMap.get(status) !=null){
                
                Map<String, String> newdata = new Map<String, String> ();
                for( string header: headerMap.keySet()){
       
                    
                    String[] relationalObjs = headerMap.get(header).split('\\.');
                    string feildName;
                    string relationalObjName;
                    if(relationalObjs.size() == 2 ){
                        relationalObjName = relationalObjs[0];
                        feildName = relationalObjs[1];
                        if(!formatColumnMap.containsKey(header))
                            newdata.put(header, String.valueOf(item.getsObject(relationalObjName).get(feildName)));
                        else if(item.getsObject(relationalObjName).get(feildName) != null && formatColumnMap.containsKey(header))
                            newdata.put(header, String.valueOf(((decimal)item.getsObject(relationalObjName).get(feildName)).format()));

                    }
                    else if(relationalObjs.size()>0 && !relationalObjs.isEmpty() ) {
                        feildName = relationalObjs[0];
                        if(!formatColumnMap.containsKey(header))
                            newdata.put(header, String.valueOf(item.get(feildName)));
                        else if(item.get(feildName) != null && formatColumnMap.containsKey(header)) 
                            newdata.put(header, String.valueOf(((decimal)item.get(feildName)).format()));
                    }
                    
                }
                 statusMap.get(status).add(newdata);
            }
            else {
                List<Map<String, String>> itemList = new List<Map<String, String>> ();
                Map<String, String> newdata = new Map<String, String> ();
                for( string header: headerMap.keySet()){
                    String[] relationalObjs = headerMap.get(header).split('\\.');
                    string feildName;
                    string relationalObjName;
                    if(relationalObjs.size() == 2 ){
                        relationalObjName = relationalObjs[0];
                        feildName = relationalObjs[1];
                         if(!formatColumnMap.containsKey(header))
                            newdata.put(header, String.valueOf(item.getsObject(relationalObjName).get(feildName)));
                        else if(item.getsObject(relationalObjName).get(feildName) != null && formatColumnMap.containsKey(header))
                            newdata.put(header, String.valueOf(((decimal)item.getsObject(relationalObjName).get(feildName)).format()));
                    }
                    else if(relationalObjs.size()>0 && !relationalObjs.isEmpty() && relationalObjs.size() == 1 ) {
                        feildName = relationalObjs[0];
                        if(!formatColumnMap.containsKey(header))
                            newdata.put(header, String.valueOf(item.get(feildName)));
                        else if(item.get(feildName) != null && formatColumnMap.containsKey(header)) 
                           newdata.put(header, String.valueOf(((decimal)item.get(feildName)).format())); 
                    }
                }
                itemList.add(newdata);
                statusMap.put(status, itemList);
            }               
        }
        
        System.debug('Status Map '+statusMap);
        return statusMap;
    }
    
    public static string getfieldLabel(string ObjName, string fieldApiName){
        String objtype = ObjName;
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType objSchema = schemaMap.get(objtype);
        string fieldLabel = objSchema.getDescribe().fields.getMap().get(fieldApiName).getDescribe().getLabel();
        return fieldLabel;
    } 
    
    public static Map<string,string> addHeaderRowData(String fldLabel, String fldValue ){
       // list<Map<string,string>> headerRowDataMap = new list<map<string,string>>();
        Map<string, string> rowDataMap = new map<string,string>();
        rowDataMap.put('A',fldLabel);
        rowDataMap.put('B',fldValue);
        return rowDataMap;
        //headerRowDataMap.add(rowDataMap);
        
    }
    
    // Use this method to format Multipicklist values to show each value per line.
    public static string formatMultiSelectfield(string multiselectfield){
        list<string> fieldValueSet =  multiselectfield.split(';');  
        string prefixtags = '<viawrapper>';
        string sufixtags = '</viawrapper><br/>';
        string formattedString = '';
        integer i = 0;
        for(string fieldVlaue: fieldValueSet){
             i++;
            formattedString += i ==  fieldValueSet.size() ? prefixtags + fieldVlaue + sufixtags.removeEndIgnoreCase('<br/>') : prefixtags + fieldVlaue + sufixtags;
        }
        formattedString = '</viawrapper><p>' + formattedString + '</p><viawrapper>';
        
        return formattedString;
        
    }
}