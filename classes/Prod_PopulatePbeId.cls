global with sharing class Prod_PopulatePbeId implements Database.Batchable<sObject>
{
    List<Id> productIds = new List<Id>();
    Map<Id,Id> prod2IdPbeId = new Map<Id,Id>();
    List<Asset> assetListToBeUpdated = new List<Asset>();
    List<Asset> assetList = new List<Asset>();

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
            string query = '';
         if(!test.isRunningTest())
             query = 'select Id, Product2Id, vlocity_cmt__PricebookEntryId__c from Asset where vlocity_cmt__PricebookEntryId__c = null and orderMgmt_BPI_Id__c <> NULL and vlocity_cmt__LineNumber__c <> NULL';
        if(test.isRunningTest())
            query = 'select Id, Product2Id, vlocity_cmt__PricebookEntryId__c from Asset';

            return Database.getQueryLocator(query);   
    }
    //assetList = [select Id, Product2Id, vlocity_cmt__PricebookEntryId__c from Asset where vlocity_cmt__PricebookEntryId__c = null limit 10000];
    global void execute(Database.BatchableContext BC,List<Asset> assets)
    {
        for(Asset assetListWithBlankPbeIds: assets)
        {
            productIds.add(assetListWithBlankPbeIds.Product2ID);   
        }
        
        Pricebook2 pb = [select Id from Pricebook2 where Name = 'OCOM Pricebook' limit 1];

        for(PriceBookEntry pbe: [select Id, Product2Id from PricebookEntry where Product2Id in :productIds and Pricebook2Id = :pb.Id])
        {
            prod2IdPbeId.put(pbe.Product2ID, pbe.Id);
        }

        for(Asset assetListWithBlankPbeIds: assets)
        {
            assetListWithBlankPbeIds.Vlocity_cmt__PricebookEntryId__c = prod2IdPbeId.get(assetListWithBlankPbeIds.Product2Id);
            assetListToBeUpdated.add(assetListWithBlankPbeIds);
        }

        system.debug('assetListToBeUpdated size-- '+assetListToBeUpdated.size());
        if(assetListToBeUpdated != null && assetListToBeUpdated.size() > 0)
            update assetListToBeUpdated;
    }

    global void finish(Database.BatchableContext BC)
    {
    }
}