/*
    *******************************************************************************************************************************
    Class Name:     NaaS_InOrderFlowCOntactController 
    Purpose:        Controller for Contact Page        
    Created by:     *******             dd-Mon-20yy     OCOM-xxxx       No previous version
    Modified by:    
    *******************************************************************************************************************************
*/

public without sharing class NAAS_InOrderFlowContactController {

    public String ConList { get; set; }

    public Account MoveOutAccount  {get;set;}
    public Account MoveInAccount  {get;set;}
    public String ContId{get;set;}
    public String typeOfOrder{get;set;}
    public id parentAccId;
    Public Id acctId;
    public String parentAccountName {get; set;}
    // Added by Wael - Start OCOM-1202  & OCOM-1207
    public String smbCareAddrId {get; set;}
    public String existingOrderId {get; set;}
    public Boolean displayPageComp{get;set;}
    public String src {get; set;}
    //added by danish -21/09/2016----to get service address and conatcts.
    Public List<SMBCare_Address__c > serviceAddress{get; set;}
    Public List<SMBCare_Address__c> serviceContactList {get; set;}
    
    // Wael - End OCOM-1202 & OCOM-1207

    // fhong - outside order flow origination
    public String addressOrigin{get;set;}

    public NAAS_InOrderFlowContactController(){
       acctId = System.currentPageReference().getParameters().get('id');
       //Danish -----adding below query-20/09/2016
        serviceAddress=[SELECT Street__c,City__c,State__c,Country__c,Postal_Code__c,Name From SMBCare_Address__c  where  account__c=: acctId];
        //serviceContactList=[SELECT Contact_Name__c,Contact_Phone__c From SMBCare_Address__c where account__c=:acctId];
        parentAccId = System.currentPageReference().getParameters().get('ParentAccId');
        
        // Added by Wael - Start OCOM-1202  & OCOM-1207
        smbCareAddrId = System.currentPageReference().getParameters().get('smbCareAddrId');
        contId = System.currentPageReference().getParameters().get('contactId');
        existingOrderId = System.currentPageReference().getParameters().get('OrderId');
        src= System.currentPageReference().getParameters().get('src');
       // Wael - End OCOM-1202 & OCOM-1207
       typeOfOrder = System.currentPageReference().getParameters().get('OrderType');
      if(acctId==null)
        acctId = System.currentPageReference().getParameters().get('aid');
       if(acctId!=null){
            list<account> lstAcc = new list<account>();
            lstAcc  = [select Id,parentId,Name from Account where id =  :acctId limit 1];
            if(lstAcc.size() >0 ){
                MoveOutAccount = lstAcc[0];
            }
       }

        // fhong - outside order flow origination
        addressOrigin = System.currentPageReference().getParameters().get('addressOrigin');
        
       // Added by Wael - Start OCOM-1202  & OCOM-1207
       
       if(src=='os'){
           Order theOrder = [SELECT Id, CustomerAuthorizedBy.Id, Service_Address__c, Service_Address__r.Id, AccountId FROM Order WHERE Id=: existingOrderId ];
           if(theOrder != null){
              contId = theOrder.CustomerAuthorizedBy.Id; 
              if( theOrder.Service_Address__c!=null){
                  SMBCare_Address__c theAddress = [SELECT Id, Service_Account_Id__c, Service_Account_Id__r.Id FROM SMBCare_Address__c WHERE Id=: theOrder.Service_Address__r.Id LIMIT 1 ];
                  if(theAddress !=null){
                      smbCareAddrId = theAddress.Id;
                      Account theAccount = [SELECT Id, parentId, Name FROM Account where id =: theAddress.Service_Account_Id__r.Id];
                      acctId = theAccount.Id;
                  } else {
                      smbCareAddrId = '';
                  }
              }
              parentAccId = theOrder.AccountId;
           }
           if(acctId!=null){
               MoveOutAccount = [SELECT Id, parentId, Name FROM Account where id =: acctId];
               parentAccountName = MoveOutAccount.Name; //25-Nov-2016 : Sprint 9 : Arpit added for displaying account name in header
           }
       }       
       // Wael - End OCOM-1202 & OCOM-1207
      }
 // Vlocity Adding Create Order here
 // Wael renamed the method from CreateOrder to CreateUpdateOrder OCOM-1202/OCOM-1207
      public PageReference CreateUpdateOrder(){
              //Arpit : 21-Sept-2016 : Sprint 4 : MultiSite Implementation
              String isBulkUploadUsed = System.currentPageReference().getParameters().get('bulkUpload');
              // Wael - start of code required for OCOM-1202 add the if condition
              String passedOrderId = '';
              if(existingOrderId==null || existingOrderId=='null' || existingOrderId==''){
              
                  // here create order
                  displayPageComp =false;
                  Id accountId = MoveOutAccount.id;
                  vlocity_cmt.VlocityOpenInterface vlOpenInt = new NAAS_HybridCPQUtil();
                  Map<String, Object> options = new Map<String, Object>();
                  Map<String, Object> input = new Map<String, Object>();
                  Map<String, Object> output = new Map<String, Object>();
    
                  // RCID Id
                  //input.put('RCIDAcctId',);
                  input.put('ParentAccId',parentAccId);
                  // Service Adderess
                  input.put('ServiceAcctId', MoveOutAccount.Id);
                  // Contact Id
                  input.put('ContactId', ContId);
                  // Order Type: added by Prashant on 15-Feb-2017
                  if(typeOfOrder != null  &&  typeOfOrder != '')
                  vlOpenInt.invokeMethod('createOcomOrder', input, output, options);
    
                  Id OrderId = (Id)output.get('OrderId');
                  System.debug('######' + OrderId);
                  //return new PageReference('/apex/TESTING?ContId='+ContId + '&AccountId='+MoveOutAccount.Id);
                  //PageReference pg =  new PageReference('/apex/OCOM_InOrderFlowContact?id='+MoveOutAccount.Id +'&OrderType='+typeOfOrder+ '&OrderId='+OrderId);
                  //PageReference pg =  new PageReference('/apex/Order_PAC_Address?id='+OrderId+'&sfdc.override=1');
                  //
                  // Page redirects to HYBRID CPQ -Pete H.
                  // Temp Change by Wael passing ServiceAcctId and ParentAccId in addition to OrderId that was initially passed, added ContactId as well
                  
                  // Wael start
                  passedOrderId = '' + OrderId; 
                  // Wael end
                  
              } else {
                  displayPageComp =true;
                  passedOrderId = existingOrderId;
                  Order order_to_update = [SELECT Id FROM Order WHERE Id=:existingOrderId ];
                  order_to_update.CustomerAuthorizedById = ContId;
                  order_to_update.CustomerSignedBy__c = ContId;
                  order_to_update.Service_Address__c = smbCareAddrId;
                  order_to_update.Type = typeOfOrder; // added by prashant on 15-Feb-2015
                  SMBCare_Address__c smbCareAddr ;
                  if(smbCareAddrId != null){
                  smbCareAddr  = [SELECT Id, Address__c, City__c, Province__c, Postal_Code__c, Country__c
                                                      FROM SMBCare_Address__c WHERE Id=: smbCareAddrId];
                  }
                  if(smbCareAddr !=null){
                      order_to_update.Service_Address_Text__c = smbCareAddr.Address__c + ' ' + 
                                                            smbCareAddr.City__c + ' ' +
                                                            smbCareAddr.Province__c + ' ' + 
                                                            smbCareAddr.Postal_Code__c + ' ' + 
                                                            smbCareAddr.Country__c;
                  }
                  update order_to_update;
              }
              // Before Wael changes for OCOM-1202
              // PageReference pg =  new PageReference('/apex/ocom_hybridcpq?id='+ OrderId + '&ServiceAcctId=' + MoveOutAccount.Id + '&ParentAccId=' + ParentAccId + '&smbCareAddrId=' + smbCareAddrId + '&contactId=' + contId);
              // After Wael Changes for OCOM-1202 - Start
              //PageReference pg =  new PageReference('/apex/ocom_hybridcpq?id='+ passedOrderId + '&ServiceAcctId=' + MoveOutAccount.Id + '&ParentAccId=' + ParentAccId + '&smbCareAddrId=' + smbCareAddrId + '&contactId=' + contId);
              // Wael End for OCOM-1202
              // Peter Adding navigation back to OOTB instead
             // PageReference pg =  new PageReference('/apex/vlocity_cmt__hybridcpq?id='+ passedOrderId + '&ServiceAcctId=' + MoveOutAccount.Id + '&ParentAccId=' + ParentAccId + '&smbCareAddrId=' + smbCareAddrId + '&contactId=' + contId);
              /* below is commented by Prashant on 26-Sep-2016- Multisite implementation
             
              PageReference pg =  new PageReference('/apex/ocom_hybridcpq?id='+ passedOrderId + '&ServiceAcctId=' + MoveOutAccount.Id + '&ParentAccId=' + ParentAccId + '&smbCareAddrId=' + smbCareAddrId + '&contactId=' + contId + '&addressOrigin=' + addressOrigin);
              */
              //Prashant : 26-Sept-2016 : Sprint 5 : Multisite Implemetation
              PageReference pg =  new PageReference('/apex/NAAS_SiteSummaryPage?id='+ passedOrderId + '&ServiceAcctId=' + MoveOutAccount.Id + '&ParentAccId=' + ParentAccId + '&smbCareAddrId=' + smbCareAddrId + '&contactId=' + contId + '&addressOrigin=' + addressOrigin);
              //Prashant 
              //Arpit : 21-Sept-2016 : Sprint 4 : MultiSite Implementation
              /*if(isBulkUploadUsed == 'true'){
                  system.debug('passedOrderId--->' + passedOrderId );
                  system.debug('ParentAccId --->' + ParentAccId );
                  system.debug('addressOrigin--->' + addressOrigin);
                  system.debug('contactId--->' + contId);
                  pg  = new PageReference('/apex/ocom_hybridcpq?id='+ passedOrderId + '&ParentAccId=' + acctId + '&addressOrigin=' + addressOrigin + '&contactId=' + contId) ;
              }*/
              
              pg.setredirect(true);
              return pg;
      }
   public List<selectOption> getList() {
   serviceContactList = new list<SMBCare_Address__c>();
   Id acctId = System.currentPageReference().getParameters().get('id');
        List<selectOption> ConList =new List<selectOption>();
         ConList.add(new selectOption('None', 'None'));
         serviceContactList=[SELECT id,Contact_Name__c,Contact_Phone__c,account__c,Name From SMBCare_Address__c where account__c=:acctId];
        for (SMBCare_Address__c sctl : serviceContactList){
                if(sctl.Contact_Name__c != '' && sctl.Contact_Name__c != null){
                     ConList.add(new selectOption(sctl.Contact_Name__c,sctl.Contact_Name__c));
                 }
             }
        return ConList;
}

    public void settypeOfOrder(String typeOfOrder) { this.typeOfOrder = typeOfOrder; }
    
    public PageReference onSelectContact() {
      return null;
    }
}