/* Developed By: Daniel Ho
* Created Date: 16-Mar-2015
*/

@isTest
public class S2C_MyWorkRequests_Test {
    static testMethod void S2C_MyWorkRequests_Test() {
        // creating an account 
        Account acc = new Account();
        acc.Name = 'test';
        insert acc;
        // creating list of Work Requests referencing an account created above
        List<work_request__c> lstWRs=new List<work_request__c>();
        work_request__c wr = new work_request__c();
        wr.Account__c = acc.Id;
        wr.Status__c='In Progress'; 
        wr.Transfer_to_Queue__c = 'Contract/SRT AB';
        wr.ownerid=Userinfo.getUserId();
        lstWRs.add(wr);
        lstWRs.add(new work_request__c(Account__c=acc.id,Transfer_to_Queue__c = 'Contract/SRT AB',Status__c='In Progress',ownerid=Userinfo.getUserId()));
        insert lstWRs;
        // Instantiate controller "MyWorkRequestsController"
        S2C_MyWorkRequestsController openWRs = new S2C_MyWorkRequestsController();        
        openWRs.sortDir='ASC';
        openWRs.sortBy='createddate';
        List<SObject> resultList = new List<SObject>();
        // Map resultList to the object
        Map<Object, List<SObject>> objectMap = new Map<Object, List<SObject>>();
        List<Object> keys = new List<Object>(objectMap.keySet());
        //Run the code to test
        Test.startTest();
        openWRs.empty(); 
        openWRs.sortList(lstWRs);
        Test.stopTest();
    }

}