global with sharing class CustomProductSelectorImplementation implements vlocity_cmt.VlocityOpenInterface{
    private String  ALL_PBE_BASE_SQL = 'Select Id, Product2.vlocity_cmt__CategoryData__c, Pricebook2Id, Product2Id,X2_Year_Contract_Price__c, ProductCode,Product2.Description,Product2.ProductSpecification__r.Name, Product2.orderMgmtId__c,Product2.RelationshipType__c, Product2.Catalog__r.Name,UnitPrice,Name,Product2.Name, Product2.vlocity_cmt__IsConfigurable__c,Product2.vlocity_cmt__Type__c,Product2.vlocity_cmt__SubType__c,vlocity_cmt__RecurringPrice__c,IsActive,Product2.OCOM_Promotion_Applies_To__c,Product2.DetailedDescription__c,Product2.VLAdditionalConfigData__c,Product2.vlocity_cmt__EndDate__c,Product2.Product_Language_Support__c,Product2.Product_Language_Support__r.Name_FR_CA__c,Product2.Product_Language_Support__r.Description_FR_CA__c from PricebookEntry where  (IsActive = true) ';
    //private String  ALL_PBE_BASE_SQL_WITH_JSON = 'Select Id, Pricebook2Id, Product2Id, ProductCode,Product2.Description, UnitPrice,Name,Product2.Name, Product2.vlocity_cmt__IsConfigurable__c, Product2.vlocity_cmt__JSONAttribute__c, Product2.vlocity_cmt__Type__c,Product2.vlocity_cmt__SubType__c,vlocity_cmt__RecurringPrice__c,IsActive from PricebookEntry where  (IsActive = true) AND (Product2.recordTypeId = null OR Product2.recordType.DeveloperName = \'Product\')';
    // Adding Product2.ProductSpecification__r.Name, Product2.RelationshipType__c, Product2.Catalog__r.Name
    private String  ALL_PBE_BASE_SQL_WITH_JSON = 'Select Id,Product2.vlocity_cmt__CategoryData__c,X2_Year_Contract_Price__c,Product2.ProductSpecification__r.Name, Product2.RelationshipType__c, Pricebook2Id, Product2.orderMgmtId__c,Product2Id, ProductCode,Product2.Description, UnitPrice,Name,Product2.Name, Product2.vlocity_cmt__IsConfigurable__c, Product2.vlocity_cmt__JSONAttribute__c, Product2.vlocity_cmt__Type__c,Product2.vlocity_cmt__SubType__c,vlocity_cmt__RecurringPrice__c,IsActive,Product2.OCOM_Promotion_Applies_To__c,Product2.Catalog__r.Name,Product2.DetailedDescription__c,Product2.VLAdditionalConfigData__c,Product2.vlocity_cmt__EndDate__c,Product2.Product_Language_Support__c,Product2.Product_Language_Support__r.Name_FR_CA__c,Product2.Product_Language_Support__r.Description_FR_CA__c from PricebookEntry where (IsActive = true)';
    global Boolean invokeMethod(String methodName, Map<String, Object> input, Map<String, Object> output, Map<String, Object> options)  {
            system.debug('>>>>>input___::' + input);
            system.debug('methodName____' + methodName);
            if (methodName.equals('getAllProducts')){
                return getAllProducts(input,output,options);
            }
            return false;
        }
    private Boolean getAllProducts(Map<String, Object> input, Map<String, Object> output, Map<String, Object> options){
        List<PriceBookEntry> prodList = new List<PriceBookEntry> ();
        output.put('listOfProducts',prodList);//always send an empty list even if none...
        try{
            String formattedSql = ALL_PBE_BASE_SQL;
            if(input.get('includeAttributes') != null) {
                Boolean includeAttributes = (Boolean)input.get('includeAttributes');
                if(includeAttributes)
                    formattedSql = ALL_PBE_BASE_SQL_WITH_JSON;
                    system.debug('formattedSql111____' + formattedSql);
            }

            if(input.get('fieldsToReturn') != null) {
                List<String> fieldsToReturn = (List<String>)input.get('fieldsToReturn');
                if(fieldsToReturn != null && fieldsToReturn.size() > 0)
                    formattedSql = buildQueryString(fieldsToReturn);
                    
            }

            List<Id> childProductIds = (List<Id>)input.get('childProductIds');
            if(childProductIds != null)
                formattedSql += ' AND Product2.Id in :childProductIds ';
                system.debug('formattedSql22____' + formattedSql);
            List<vlocity_cmt.CpqQueryObject> queries = (List<vlocity_cmt.CpqQueryObject>)input.get('Queries');
            //build the sql
            Set<Id> firstInQuery = null;
            if(queries != null){
                for(vlocity_cmt.CpqQueryObject tempQuery : queries){
                    if( (tempQuery.qType == vlocity_cmt.CpqQueryObject.QUERY_TYPE.CONSTANT) || (tempQuery.qType == vlocity_cmt.CpqQueryObject.QUERY_TYPE.EQUAL_TO_OP)){
                        formattedSql = addConstantQuery(formattedSql,tempQuery);
                        system.debug('formattedSql33____' + formattedSql);
                    }
                    else if( (tempQuery.qType == vlocity_cmt.CpqQueryObject.QUERY_TYPE.IN_OP) && (firstInQuery == null)){
                        firstInQuery = new Set<Id>();
                        formattedSql = addInQuery(formattedSql,tempQuery,firstInQuery,'firstInQuery');
                        system.debug('formattedSql444____' + formattedSql);
                    }
                }
            }   
            if(formattedSql != null && !String.isBlank(formattedSql)) {
                system.debug('formattedSql5555____' + formattedSql);
                String sortBy = (String)input.get('sortBy');
                if(sortBy == null || sortBy.equals('Name') || sortBy.equals('Featured')) {
                    sortBy = 'Product2.Name';
                }
                else {
                    if(sortBy.equals('NonRecurringCost'))
                        sortBy = 'UnitPrice';
                    if(sortBy.equals('RecurringCost'))
                        sortBy = 'vlocity_cmt__RecurringPrice__c';       
                }
                Integer numberOfProds = 4000;
                if(input.get('pageSize') != null) { 
                     numberOfProds = ((Integer)input.get('pageSize'))*2;
                     sortBy = ' Id ';
                }
                // adidng condition for removing order by 
                if(String.isNotEmpty(sortBy) && sortBy != 'id'){
                    formattedSql = formattedSql + ' ORDER BY ' + sortBy + ' LIMIT '+String.valueOf(numberOfProds);
                }else {
                    formattedSql = formattedSql + ' LIMIT '+String.valueOf(numberOfProds);
                }
                system.debug('####  formattedSQL=' + formattedSQL);
            }
            system.debug('################  formattedSQL=' + formattedSQL);
            prodList = Database.query(formattedSql);
            output.put('listOfProducts',prodList);
            return true;
            }catch(Exception e){
                //Logger.err(e);
            }
        return false;
    }
     @TestVisible private String addConstantQuery(String formattedSql, vlocity_cmt.CpqQueryObject query){
        String expr = query.constantVal;
        expr = expr.replace('\\\'', '\''); // might be required in OMni..not sure where to generate \\\\..!!!
        expr = '( ' + expr + ' )'; // Internally we will add quotes around the query without changing the query sent by Omni or any other caller? . OM-240
        if(query.logOpp == vlocity_cmt.CpqQueryObject.QUERY_LOGICAL_OPERATOR.AND_OPP)
            {return formattedSql + ' AND ' + expr;}
        else if(query.logOpp == vlocity_cmt.CpqQueryObject.QUERY_LOGICAL_OPERATOR.OR_OPP)
            { return formattedSql + ' OR ' + expr;}
    return formattedSql; //incase nothing return orig string
         
    }
     @TestVisible private String addInQuery(String formattedSql, vlocity_cmt.CpqQueryObject query,Set<Id> firstInQuery, String bindVarName){
        firstInQuery.addAll (query.inRVal);
         ////system.debug('firstInQuery :: '+firstInQuery);
         List<Product2> prodList = [SELECT Id, RecordTypeId, recordType.DeveloperName, IsActive From Product2 WHERE Id IN :firstInQuery];
         //system.debug('prodList :: '+prodList);
        String expr = '( ' + query.inLVal +  ' = :' + bindVarName +    ' )'; 
        if(query.logOpp == vlocity_cmt.CpqQueryObject.QUERY_LOGICAL_OPERATOR.AND_OPP)
            {return formattedSql + ' AND ' + expr;}
        else if(query.logOpp == vlocity_cmt.CpqQueryObject.QUERY_LOGICAL_OPERATOR.OR_OPP)
            { return formattedSql + ' OR ' + expr;}
    return formattedSql; //incase nothing return orig string            
    }

    //Utility method to build the query string

    private static String buildQueryString(List<String> objfields){
         String query = 'Select Id, Product2Id  ';

        for (String field : objfields)
        {
            if(field.equalsIgnoreCase('id') || field.equalsIgnoreCase('Product2Id'))
                continue;
            query +=' , ';
            query += field;
        }

        query += ' from PricebookEntry where  (IsActive = true) ';
        return query;
    }
  
    public class QueryUnsupportedException extends Exception{}
}