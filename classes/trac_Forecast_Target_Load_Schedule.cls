//schedule with System.schedule('Update Target Load Forecast','0 0 0 * * ?', new trac_Forecast_Target_Load_Schedule());
global class trac_Forecast_Target_Load_Schedule implements schedulable {

	global void execute(SchedulableContext sc) {
		runUpdate();
	}
	
	public void runUpdate() {
		
		trac_Forecast_Report myR = new trac_Forecast_Report();
		
		myR.getForecasts_Target_Load();
	}
}