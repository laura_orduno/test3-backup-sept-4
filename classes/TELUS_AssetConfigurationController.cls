global with sharing class TELUS_AssetConfigurationController {
    global String nsPrefix {get; set;}
    global static String currencyLocaleSymbol {get; private set;}
    //global String thousandSep {get; private set;}
    //global String decimalSep {get; private set;}
    global static List<Map<String, String>> columnsMap {get; set;}
    global static String columnsList {get; set;}
    global static String fieldSetName {get; set;}
    global static String listTypeAsset {get; set;}
    global ApexPages.StandardController stdController
    {
        get;
        set
        {
            stdController = value;
        }
    }
    
    global TELUS_AssetConfigurationController() {
        currencyLocaleSymbol = '$';
        //thousandSep = vloProductListUtility.getLocaleThousandSep();
        //decimalSep = ProductListUtility.getLocaleDecimalSep();
        nsPrefix =  'vlocity_cmt__';
    }
    
    global TELUS_AssetConfigurationController(ApexPages.StandardController controller) {
        stdController = controller;
        currencyLocaleSymbol = '$';
        //thousandSep = ProductListUtility.getLocaleThousandSep();
        //decimalSep = ProductListUtility.getLocaleDecimalSep();
        nsPrefix =  'vlocity_cmt__';
    }
    
    
    
    
    @RemoteAction
    global static List<Object> getAssetList(Id accId, String fieldStName, String listType){
        listTypeAsset = listType;
        debugAssetQuery(accId, fieldStName);
        return getAssets(accId, fieldStName);
    }   
    

    @future 
    global static void debugAssetQuery(Id accId, String fieldStName) {

        String nsp = 'vlocity_cmt__';
        Set<String> currencyfields = new set<String>{'vlocity_cmt__RecurringTotal__c,vlocity_cmt__OneTimeTotal__c'};
            Set<String> datefields = new set<String>{''};
        List<Object> assets = new List<Object>();
        List<Object> tempAsset = new List<Object>();
        Map<String,Object> temp = null;
        List<Asset> tempassets = null;
        Map<String, String> assetLabels = new Map<String, String>();
        List<String> rootParentIdList = new List<String>();
        Map<String, String> rootLnNumToRootParId = new Map<String, String>();
        // can't use list of assets as orderitemid needs to be added later
        
        //ading logic for FieldSet
        if(fieldStName != null){
            vlocity_cmt.FieldSetService.fieldSetName = fieldStName;
            vlocity_cmt.FieldSetService.objName = Asset.SObjectType;
            
            columnsMap = vlocity_cmt.FieldSetService.getFieldMapList();
    
            if(columnsMap != null)
                columnsList = JSON.serialize(columnsMap);
        }
        if(columnsMap != null){
            List<String> fieldsToCheck = new List<String> {nsp+'LineNumber__c', nsp+'OrderId__c','Id', nsp+'RootItemId__c', 'AccountId'};
            Set<String> fieldsPresent = new Set<String>();
            String query = 'SELECT ';
            for(Map<String, String> f : columnsMap) {
                fieldsPresent.add(f.get('id'));
                query += f.get('id') + ', ';
                assetLabels.put(f.get('id'), f.get('name'));
            }
            for(String s : fieldsToCheck){
                if(!(fieldsPresent.contains(s))){
                    query += s + ' ,';
                }
            }
            query = query.trim().removeEnd(','); // remove trailing ','
            
            //query += ' FROM ASSET WHERE vlocity_cmt__ProvisioningStatus__c != \'DELETED\' AND vlocity_cmt__ProvisioningStatus__c != \'NEW\;
            query += ' FROM ASSET WHERE vlocity_cmt__ProvisioningStatus__c in (\'Active\',\'Changed\') ';
            String rootquery = query;
            //query += ' AND (NOT vlocity_cmt__LineNumber__c LIKE \'%.%\')';
            query += ' AND vlocity_cmt__parentItemId__c = null ';
            //checking type of list to be displayed on VF page
            if(listTypeAsset == null || listTypeAsset.equals('') || listTypeAsset.equals('Service')){
                query += ' AND vlocity_cmt__ServiceAccountId__c = \'' + accId;
            } else {
                if(listTypeAsset.equals('Billing')){
                    query += ' AND vlocity_cmt__BillingAccountId__c = \'' + accId;
                }

                if(listTypeAsset.equals('Asset')){
                    query += ' AND AccountId = \'' + accId;
                }
            }
            query += '\' ORDER BY '+nsp+'OrderId__c, '+nsp+'LineNumber__c';

            system.debug('#### VLOCITY #### TELUS_AssetConfigurationController 1: query =' + query );
        }
    }


    global static List<Object> getAssets(Id accId, String fieldStName){
        String nsp = 'vlocity_cmt__';
        Set<String> currencyfields = new set<String>{'vlocity_cmt__RecurringTotal__c,vlocity_cmt__OneTimeTotal__c'};
            Set<String> datefields = new set<String>{''};
        List<Object> assets = new List<Object>();
        List<Object> tempAsset = new List<Object>();
        Map<String,Object> temp = null;
        List<Asset> tempassets = null;
        Map<String, String> assetLabels = new Map<String, String>();
        List<String> rootParentIdList = new List<String>();
        Map<String, String> rootLnNumToRootParId = new Map<String, String>();
        // can't use list of assets as orderitemid needs to be added later
        
        //ading logic for FieldSet
        if(fieldStName != null){
            vlocity_cmt.FieldSetService.fieldSetName = fieldStName;
            vlocity_cmt.FieldSetService.objName = Asset.SObjectType;
            
            columnsMap = vlocity_cmt.FieldSetService.getFieldMapList();
    
            if(columnsMap != null)
                columnsList = JSON.serialize(columnsMap);
        }
        if(columnsMap != null){
            List<String> fieldsToCheck = new List<String> {nsp+'LineNumber__c', nsp+'OrderId__c','Id', nsp+'RootItemId__c', 'AccountId'};
            Set<String> fieldsPresent = new Set<String>();
            String query = 'SELECT ';
            for(Map<String, String> f : columnsMap) {
                fieldsPresent.add(f.get('id'));
                query += f.get('id') + ', ';
                assetLabels.put(f.get('id'), f.get('name'));
            }
            for(String s : fieldsToCheck){
                if(!(fieldsPresent.contains(s))){
                    query += s + ' ,';
                }
            }
            query = query.trim().removeEnd(','); // remove trailing ','
            
            //query += ' FROM ASSET WHERE vlocity_cmt__ProvisioningStatus__c != \'DELETED\' AND vlocity_cmt__ProvisioningStatus__c != \'NEW\;
             query += ', vlocity_cmt__parentItemId__c FROM ASSET WHERE vlocity_cmt__ProvisioningStatus__c in (\'Active\',\'Changed\') ';
            String rootquery = query;
            //query += ' AND (NOT vlocity_cmt__LineNumber__c LIKE \'%.%\')';
            query += ' AND vlocity_cmt__parentItemId__c = null ';
            //checking type of list to be displayed on VF page
            if(listTypeAsset == null || listTypeAsset.equals('') || listTypeAsset.equals('Service')){
                query += ' AND vlocity_cmt__ServiceAccountId__c = \'' + accId;
            } else {
                if(listTypeAsset.equals('Billing')){
                    query += ' AND vlocity_cmt__BillingAccountId__c = \'' + accId;
                }

                if(listTypeAsset.equals('Asset')){
                    query += ' AND AccountId = \'' + accId;
                }
            }
            query += '\' ORDER BY '+nsp+'OrderId__c, '+nsp+'LineNumber__c';

            tempassets = Database.query(query);
            for(Asset root : tempassets) {
                rootParentIdList.add((String)root.get('vlocity_cmt__RootItemId__c'));
                rootLnNumToRootParId.put((String)root.get('vlocity_cmt__RootItemId__c'),(String)root.get('vlocity_cmt__LineNumber__c'));
            }
            //querying child for root elements selected
            //rootquery += ' AND vlocity_cmt__LineNumber__c LIKE \'%.%\' AND vlocity_cmt__RootItemId__c IN :rootParentIdList';
            if(listTypeAsset == null || listTypeAsset.equals('') || listTypeAsset.equals('Service')){
                rootquery += ' AND vlocity_cmt__ServiceAccountId__c = \'' + accId + '\'';
            }
            rootquery += ' AND vlocity_cmt__RootItemId__c IN :rootParentIdList';
            

            rootquery += ' ORDER BY '+nsp+'OrderId__c, '+nsp+'LineNumber__c';

            system.debug('');
            List<Asset> tempChildren = Database.query(rootquery);

            system.debug('#### VLOCITY #### TELUS_AssetConfigurationController 2: rootQuery =' + rootQuery );
            system.debug('#### VLOCITY #### TELUS_AssetConfigurationController 3: rootParentIdList =' + rootParentIdList );
            //adding all children by checking rootItemId of parent

            List<Asset> children = new List<Asset>();

            for(Asset a: tempChildren){
                if(a.vlocity_cmt__parentItemId__c != null){
                    children.add(a);
                }
            }
            for(Asset child : children) {
                    String lineNum = (String)child.get('vlocity_cmt__LineNumber__c');
                    String rootItemId = (String)child.get('vlocity_cmt__RootItemId__c');
                    for(String rootId : rootParentIdList) {
                        if(rootItemId != null && rootId != null && rootItemId.equals(rootId) && (lineNum.startsWith(rootLnNumToRootParId.get(rootId)))) {
                            tempassets.add(child);
                            break;
                        }
                    }
            }
            //renumbering assets before sending to UI
            tempAssets = renumberAssets(tempAssets);
            assets.add(assetLabels.values());
            for(Asset asset : tempassets){//TODO
                
                temp = new Map<String,Object>();
                temp.put('Id',asset.Id);
                temp.put('LineNumber',asset.get(nsp+'LineNumber__c'));
                temp.put('RootItemId',asset.get(nsp+'RootItemId__c'));
                for(String field : assetLabels.keySet()) {
                    if(!field.contains('.'))
                        temp.put(assetLabels.get(field), asset.get(field));
                    else{ // u cannot go beyond 1 level down in fieldset it seems
                        List<String> relatedFields = field.split('\\.');
                        if(relatedFields.size() == 2){
                            SObject s = asset.getSobject(relatedFields[0]);
                            if(s != null)
                                temp.put(assetLabels.get(field), s.get(relatedFields[1]));
                        }else{
                            new TELUS_AssetConfigurationControllerException('Lookups fields in second level is not supported in Fieldset');
                        }
                        
                    }
                } 
                tempAsset.add(temp);
            }
        }else{
            String query = 'SELECT Id, Name, Quantity, Status, vlocity_cmt__ProvisioningStatus__c, vlocity_cmt__RecurringTotal__c, vlocity_cmt__OneTimeTotal__c, vlocity_cmt__LineNumber__c , vlocity_cmt__OrderId__c, vlocity_cmt__RootItemId__c, AccountId FROM ASSET WHERE vlocity_cmt__ProvisioningStatus__c != \'DELETED\' AND vlocity_cmt__ProvisioningStatus__c != \'NEW\'';
            String rootquery = query;
            //query += ' AND (NOT vlocity_cmt__LineNumber__c LIKE \'%.%\')';
            query += ' AND vlocity_cmt__parentItemId__c = null ';

            //checking type of list to be displayed on VF page
            if(listTypeAsset == null || listTypeAsset.equals('') || listTypeAsset.equals('Service')){
                query += ' AND vlocity_cmt__ServiceAccountId__c = \'' + accId;
            } else {
                if(listTypeAsset.equals('Billing')){
                    query += ' AND vlocity_cmt__BillingAccountId__c = \'' + accId;
                }

                if(listTypeAsset.equals('Asset')){
                    query += ' AND AccountId = \'' + accId;
                }
            }
            query += '\' ORDER BY '+nsp+'OrderId__c, '+nsp+'LineNumber__c';
            tempassets = Database.query(query);
            for(Asset root : tempassets) {
                rootParentIdList.add((String)root.get('vlocity_cmt__RootItemId__c'));
                rootLnNumToRootParId.put((String)root.get('vlocity_cmt__RootItemId__c'),(String)root.get('vlocity_cmt__LineNumber__c'));
            }
            //querying child for root elements selected
            rootquery += ' AND vlocity_cmt__LineNumber__c LIKE \'%.%\' AND vlocity_cmt__RootItemId__c IN :rootParentIdList';
            rootquery += ' ORDER BY '+nsp+'OrderId__c, '+nsp+'LineNumber__c';
            List<Asset> children = Database.query(rootquery);
            //adding all children by checking rootItemId of parent
            for(Asset child : children) {
                String lineNum = (String)child.get('vlocity_cmt__LineNumber__c');
                String rootItemId = (String)child.get('vlocity_cmt__RootItemId__c');
                for(String rootId : rootParentIdList) {
                    if(rootItemId != null && rootId != null && rootItemId.equals(rootId) && (lineNum.startsWith(rootLnNumToRootParId.get(rootId)))) {
                        tempassets.add(child);
                        break;
                    }
                }  
            }
            //renumbering assets before sending to UI
            tempAssets = renumberAssets(tempAssets);
            Map<String, String> labMap = new Map<String, String> {'Name' => 'Asset Name', 'Quantity' => 'Quantity', 'vlocity_cmt__ProvisioningStatus__c' => 'Provisioning Status', 'vlocity_cmt__RecurringTotal__c' => 'Recurring Total', 'vlocity_cmt__OneTimeTotal__c' => 'One Time Total'};
            assetLabels = labMap.clone();
            assets.add(assetLabels.values());
            for(Asset asset : tempassets){//TODO
                
                temp = new Map<String,Object>();
                temp.put('Id',asset.Id);
                temp.put('LineNumber',asset.vlocity_cmt__LineNumber__c);
                temp.put('RootItemId',asset.vlocity_cmt__RootItemId__c);
                for(String field : assetLabels.keySet()) {
                    temp.put(assetLabels.get(field), asset.get(field));
                } 
                tempAsset.add(temp);
            }
        }
        if(tempAsset != null){
            for(Object ob : tempAsset){
                Map<String, Object> asst = (Map<String, Object>)ob;
                for(String s : asst.keySet()){
                    if(currencyfields.contains(s)){
                        Object o = asst.get(s);
                        String res = processPriceFields(o);
                        asst.put(s,res);
                    }
                    if(datefields.contains(s)){
                        Date d = (Date)asst.get(s);
                        if(d != null){
                            String formatDate = d.format();
                            asst.put(s, formatDate);
                        }
                    }
                }
            }
        }
        assets.addAll(tempAsset);
        return assets;
    }

    public static List<Asset> renumberAssets (List<Asset> tempAsset) {
        //Logger.err('Inside renumberAssets');
        List<Asset> result = new List<Asset>();
        List<TELUS_LineItemWrapper> liwList = new List<TELUS_LineItemWrapper>();
        String rootLineNumber = '0001';
        Map<String, String> rootItemToRootLineNum = new Map<String, String>();
        for(Asset ast : tempAsset) {
            String lineNum = (String)ast.get('vlocity_cmt__LineNumber__c');
            String rootItemId = (String)ast.get('vlocity_cmt__RootItemId__c');
            if(lineNum != null && !lineNum.contains('.')) {
                ast.put('vlocity_cmt__LineNumber__c',rootLineNumber);
                if(rootItemId != null) {
                    rootItemToRootLineNum.put(rootItemId,rootLineNumber);
                    //rootLineNumber = vlocity_cmt.LineNumber.incrementLineNum(rootLineNumber);
                }
            }
        }

        for(Asset ast : tempAsset) {
            String lineNum = (String)ast.get('vlocity_cmt__LineNumber__c');
            String rootItemId = (String)ast.get('vlocity_cmt__RootItemId__c');
            String rootLineNum = rootItemToRootLineNum.get(rootItemId);
            if(lineNum != null && lineNum.contains('.')) {
                lineNum  = lineNum.replaceFirst(lineNum.substring(0, lineNum.indexOf('.')), rootLineNum);
                ast.put('vlocity_cmt__LineNumber__c',lineNum);
            }
        }

        //sorting the list
        for(Asset ast : tempAsset){
            TELUS_LineItemWrapper liw = new TELUS_LineItemWrapper(ast);
            liwList.add(liw);
        }
        TELUS_LineItemWrapper.sortBy = 'Ascending';
        liwList.sort();
        for(TELUS_LineItemWrapper liw : liwList) {
            result.add((Asset)liw.sObjectItem);
        }
        return result;
    }
    
    @TestVisible
    private static String processPriceFields(Object ob){
        String priceString;
        String i = null;
        Decimal cost = (Decimal)ob;
        if(cost != null){
            cost = cost.setScale(2, RoundingMode.HALF_UP);
            i = String.valueOf(cost);
        }
        String s = ( Decimal.valueOf(i==null||i.trim()==''?'0':i).setScale(2) + 0.001 ).format();
        if(currencyLocaleSymbol == null)
            currencyLocaleSymbol = '$';
        priceString =  (currencyLocaleSymbol != null ? currencyLocaleSymbol+s.substring(0,s.length()-1) : s.substring(0,s.length()-1));
        return priceString;
    }

    /*
    @RemoteAction
    global static String changeToFDO(Id acctId, List<Id> assetIdList, Date requestDate){
        Map<String,String> returnJsonVal = new Map<String,String>();
        try{
            String defaultFDOImplementation = CustomSettingsUtilities.getCustomClassImplemenationName('FDO');
            Type t = Type.forName(defaultFDOImplementation);
            if(t == null){
                Logger.err('No implementation found for FDOInterface');
                return null;
            }
            vlocity_cmt.VlocityOpenInterface voi = (vlocity_cmt.VlocityOpenInterface)t.newInstance();
            Map<String, Object> inputMap = new Map<String, Object>();
            Map<String, Object> outputMap = new Map<String, Object>();
            Map<String, Object> options = new Map<String, Object>();
            inputMap.put('accountId', acctId);
            inputMap.put('assetIdList', assetIdList);
            Map<String, Object> reqDate = new Map<String, Object>();
            reqDate.put('RequestDate', requestDate);
            inputMap.put('Request Date', reqDate);          
            voi.invokeMethod('createFDO',inputMap, outputMap, options);
            Id fdoId = (Id)outputMap.get('fdoId');
            PageReference pageRef = new PageReference('/' + fdoId);
            returnJsonVal.put('location',pageRef.getUrl());

        }
        catch(Exception e){
            Logger.err(e);
            returnJsonVal.put('Status',e.getMessage());
        }
        return JSON.serialize(returnJsonVal);
    }
    
    
    */
    @RemoteAction
    global static String changeToOrderFromAsset(Id AcctId, List<Id> svcAssetId, String listType){
        System.debug('AcctId***'+AcctId);
        System.debug('svcAssetId***'+svcAssetId);
        System.debug('listType***'+listType);
        listTypeAsset = listType;
        String jsonVal = vlocity_cmt.AssetConfigurationController.changeToOrder(AcctId, svcAssetId);
        System.debug('jsonVal***'+jsonVal);
        //Map<String,Object> jsondeserialized = (Map<String,Object>)JSON.deserializeUntyped(jsonVal);
        return 'test';
    }
/**
    global static String changeToOrder(Id AcctId, List<Id> svcAssetId){
        Map<String,String> returnJsonVal = new Map<String,String>();
        if(AcctId != null && svcAssetId != null){
            try{
            Set<Id> temp = new Set<Id>();
            temp.addAll(svcAssetId);
            //checking parent Id of first root asset
            String acctype = listTypeAsset.equalsIgnoreCase('Service') ? 'ServiceAccountId__c' : (listTypeAsset.equalsIgnoreCase('Billing') ? 'BillingAccountId__c' : 'AccountId');
            String parQuery = 'SELECT Id, AccountId, LineNumber__c FROM Asset WHERE '+acctype+' = \''+AcctId+'\' AND (NOT LineNumber__c LIKE \'%.%\') ORDER BY LineNumber__c';
            List<Asset> parAccount = Database.query(parQuery);
            vlocity_cmt.CloneSelectedLineItemsController cont = new vlocity_cmt.CloneSelectedLineItemsController((Id)parAccount[0].get('AccountId'), temp, listTypeAsset);
            Id createdOrderId = cont.doCreateOrder();
            //post order creation handler
            runPostChangeImplementation(AcctId, createdOrderId, svcAssetId);
            PageReference pageRef = new PageReference('/' + createdOrderId);
            pageRef.setRedirect(true);
            returnJsonVal.put('location',pageRef.getUrl());
            }catch(Exception e){
                Logger.err(e);
                returnJsonVal.put('Status',e.getMessage());
            }
        }
       return JSON.serialize(returnJsonVal);    
    }
    
    @RemoteAction
    global static String changeToQuoteFromAsset(Id AcctId, List<Id> svcAssetId, String listType){
        listTypeAsset = listType;
        return changeToQuote(AcctId, svcAssetId);
    }

    global static String changeToQuote(Id AcctId, List<Id> svcAssetId){
        Map<String,String> returnJsonVal = new Map<String,String>();
        if(AcctId != null && svcAssetId != null){
            try{
            Set<Id> temp = new Set<Id>();
            temp.addAll(svcAssetId);
            //checking parent Id of first root asset
            String acctype = listTypeAsset.equalsIgnoreCase('Service') ? 'ServiceAccountId__c' : (listTypeAsset.equalsIgnoreCase('Billing') ? 'BillingAccountId__c' : 'AccountId');
            String parQuery = 'SELECT Id, AccountId, LineNumber__c FROM Asset WHERE '+acctype+' = \''+AcctId+'\' AND Id IN :svcAssetId AND (NOT LineNumber__c LIKE \'%.%\') ORDER BY LineNumber__c';
            List<Asset> parAccount = Database.query(parQuery);
            CloneSelectedLineItemsController cont = new CloneSelectedLineItemsController((Id)parAccount[0].get('AccountId'), temp, listTypeAsset);
            Id createdQuoteId = cont.doCreateQuote();
            //post quote creation handler
            runPostChangeImplementation(AcctId, createdQuoteId, svcAssetId);
            PageReference pageRef = new PageReference('/' + createdQuoteId);
            pageRef.setRedirect(true);
            returnJsonVal.put('location',pageRef.getUrl());
            }catch(Exception e){
                Logger.err(e);
                returnJsonVal.put('Status',e.getMessage());
            }
        }
        return JSON.serialize(returnJsonVal); 
    
    }*/

    
    /*private static void runPostChangeImplementation(Id accountId, Id createdObjectId, List<Id> assetIds){
        String postChange = CustomSettingsUtilities.getCustomClassImplemenationName('PostChangeHandler');
        Type t = Type.forName(postChange);
        VlocityOpenInterface postChangeImp = (VlocityOpenInterface)t.newInstance();
        Map<String, Object> postChangeInput = new Map<String, Object>();
        Map<String, Object> postChangeOutput = new Map<String, Object>();
        
        postChangeInput.put('accountId', accountId);
        postChangeInput.put('createdObjectId', createdObjectId);
        String methodName = createdObjectId.getSObjectType() == Quote.SObjectType ? 'postChangeForQuote' : 'postChangeForOrder';
        postChangeInput.put('assetIds', assetIds);
        try{
            postChangeImp.invokeMethod(methodName,postChangeInput, postChangeOutput, null);
        }catch(Exception e){
            System.debug(LoggingLevel.ERROR,'error is::' + e.getMessage());
        }
        
    }*/

public class TELUS_AssetConfigurationControllerException extends Exception {}  
}