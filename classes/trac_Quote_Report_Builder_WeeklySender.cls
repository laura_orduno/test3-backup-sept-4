global class trac_Quote_Report_Builder_WeeklySender implements Schedulable{
   global void execute(SchedulableContext sc) {
      // Send the weekly email
      new trac_Quote_Report_Builder().sendTodaysWeeklyEmail();
   }
   private static testMethod void testSender() {
       trac_Quote_Report_Builder_WeeklySender sender = new trac_Quote_Report_Builder_WeeklySender();
       sender.execute(null);
   }
}