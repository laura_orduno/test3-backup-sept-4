/**
 * Unit tests for <code>ConfigurationSummaryController</code>
 * 
 * @author Max Rudman
 * @since 4/8/2011
 */
@isTest
private class ConfigurationSummaryControllerTests {
	private static Product2 product;
	private static Product2 option1;
	private static Product2 option2;
	private static Product2 option2A;
	private static List<SBQQ__ProductOption__c> options;
	
	testMethod static void testTotalsWithFilter() {
		setUp();
		
		ProductModel pmodel = ProductModel.loadById(product.Id, null, null, null);
		ProductOptionSelectionModel selections = new ProductOptionSelectionModel(pmodel);
		selections.addSelection(pmodel.getAllOptions()[0]);
		selections.addSelection(pmodel.getAllOptions()[1]);
		ConfigurationSummaryController target = new ConfigurationSummaryController();
		target.selections = selections;
		target.totalFilterField = SBQQ__ProductOption__c.SBQQ__ProductCode__c.getDescribe().getName();
		target.totalFilterValue = 'C1';
		Decimal total = target.getTotal();
		System.assertEquals(100, total);
	}
	
	testMethod static void testGrouping() {
		setUp();
		
		ProductModel pmodel = ProductModel.loadById(product.Id, null, null, null);
		ProductOptionSelectionModel selections = new ProductOptionSelectionModel(pmodel);
		selections.addSelection(pmodel.getAllOptions()[0]);
		ProductOptionSelectionModel.Selection sel2 = selections.addSelection(pmodel.getAllOptions()[1]);
		ProductModel pmodel2 = ProductModel.loadById(pmodel.getAllOptions()[1].optionalProductId, null, null, null);
		sel2.getNestedSelections().addSelection(pmodel2.getAllOptions()[0]);
		ConfigurationSummaryController target = new ConfigurationSummaryController();
		target.includeBundled = true;
		target.includeNested = true;
		target.selections = selections;
		target.groupField = SBQQ__ProductOption__c.SBQQ__ProductCode__c.getDescribe().getName();
		List<ConfigurationSummaryController.SummaryGroup> groups =
			target.getSummaryGroups();
/*****        
		System.assertEquals(3, groups.size());
		System.assertEquals(1, groups[0].options.size());
		System.assertEquals('C2.A', groups[0].title);
		System.assertEquals(10, groups[0].options[0].target.SBQQ__Quantity__c, 'Option:' + groups[0].options[0]);
		System.assertEquals(1, groups[1].options.size());
		System.assertEquals('C1', groups[1].title);
		System.assertEquals(1, groups[2].options.size());
		System.assertEquals('C2', groups[2].title);
*/		
	}
	
	private static void setUp() {
		product = new Product2(Name='Test');
		option1 = new Product2(Name='Option1',ProductCode='C1');
		option2 = new Product2(Name='Option2',ProductCode='C2');
		option2A = new Product2(Name='Option2.A',ProductCode='C2.A');
		insert new List<Product2>{product,option1,option2,option2A};
		
		options = new List<SBQQ__ProductOption__c>();
		options.add(new SBQQ__ProductOption__c(SBQQ__ConfiguredSKU__c=product.Id,SBQQ__OptionalSKU__c=option1.Id,SBQQ__Number__c=1,SBQQ__UnitPrice__c=100));
		options.add(new SBQQ__ProductOption__c(SBQQ__ConfiguredSKU__c=product.Id,SBQQ__OptionalSKU__c=option2.Id,SBQQ__Quantity__c=2,SBQQ__Number__c=2,SBQQ__UnitPrice__c=200));
		options.add(new SBQQ__ProductOption__c(SBQQ__ConfiguredSKU__c=option2.Id,SBQQ__OptionalSKU__c=option2A.Id,SBQQ__Number__c=3,SBQQ__Quantity__c=5,SBQQ__UnitPrice__c=10));
		insert options;
	}
}