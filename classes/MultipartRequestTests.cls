@isTest
private class MultipartRequestTests {
	 
    static testMethod void testPrepare() {
		MultipartRequest multipartRequest = new MultipartRequest();
		MultipartRequest.Part part = new MultipartRequest.Part();
        part.setContentDisposition('form-data');
        part.addContentDispositionAttribute('name', 'part1');
        part.setContentType('text/plain');
        part.setContentTransferEncoding('text');
        part.setContent('content');
        multipartRequest.addPart(part);    	
        multipartRequest.prepare();
        System.assert(multipartRequest.getBoundary() != null);
        System.assert(multipartRequest.getBody() != null);
    }

}