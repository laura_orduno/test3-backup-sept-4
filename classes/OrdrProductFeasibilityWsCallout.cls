/* 2017-07-12[Roderick de Vera]
 * Helper class which invokes the Web Service Callout for Product Feasibility Check
 */
public class OrdrProductFeasibilityWsCallout {
    
    private class LineItem {
        public String id;
        public String parentId;
        public String action;
        public String bpiId;
        public String offeringKey;
        public String locationKey;
        public List<LineItem> childItems;
    }
    
    public static TpFulfillmentCustomerOrderV3.CustomerOrder performCustomerOrderFeasibility(Map<String, Object> inputMap) {
        TpFulfillmentCustomerOrderV3.CustomerOrder customerOrder = new TpFulfillmentCustomerOrderV3.CustomerOrder();
        try { 
            OrdrWsPerformCustomerOrderFeasibility.PerformCustomerOrderFeasibilitySOAPPort servicePort = new OrdrWsPerformCustomerOrderFeasibility.PerformCustomerOrderFeasibilitySOAPPort(); 
            servicePort = prepareCallout(servicePort);          
            customerOrder = servicePort.performCustomerOrderFeasibility(prepareCustomerOrderPayload(inputMap));    
        } 
        catch (CalloutException e) {

           OrdrUtilities.auditLogs('OrdrProductFeasibilityWsCallout.performCustomerOrderFeasibility', 'NetCracker', 'PerformCustomerOrderFeasibility', null, null, true, e.getMessage(), e.getStackTraceString());
            
            throw new OrdrExceptions.ProductFeasibilityException(e.getMessage(), e);
        }
        return customerOrder;
    }
    
    private static OrdrWsPerformCustomerOrderFeasibility.PerformCustomerOrderFeasibilitySOAPPort 
        prepareCallout(OrdrWsPerformCustomerOrderFeasibility.PerformCustomerOrderFeasibilitySOAPPort servicePort) {
            Ordering_WS__c productEligibilityEndpoint = Ordering_WS__c.getValues('ProductEligibilityEndpoint');           
            servicePort.endpoint_x = productEligibilityEndpoint.value__c;
            // Set SFDC Webservice call timeout
            servicePort.timeout_x = OrdrConstants.WS_TIMEOUT_MS;    
            
            if (servicePort.endpoint_x.startsWith('https://xmlgwy')) {
                Ordering_WS__c wsUserName =  Ordering_WS__c.getValues(OrdrConstants.CREDENTIAL_USERNAME);
                Ordering_WS__c wsPassword =  Ordering_WS__c.getValues(OrdrConstants.CREDENTIAL_PASSWORD); 
                String credentials = wsUserName.value__c + ':' + wsPAssword.value__c;
                String encodedUserNameAndPassword = EncodingUtil.base64Encode(Blob.valueOf(credentials));   
                
                servicePort.inputHttpHeaders_x = new Map<String, String>();
                servicePort.inputHttpHeaders_x.put('Authorization', 'Basic ' + encodedUserNameAndPassword);
            } else {
                servicePort.clientCertName_x = OrdrConstants.SDF_CERT_NAME;
            }
            
            return servicePort;
    } 
    
    private static TpFulfillmentCustomerOrderV3.CustomerOrder prepareCustomerOrderPayload(Map<String, Object> inputMap) {
        TpFulfillmentCustomerOrderV3.CustomerOrder customerOrder = new TpFulfillmentCustomerOrderV3.CustomerOrder();
        List<TpCommonBaseV3.CharacteristicValue> characteristicValues = new List<TpCommonBaseV3.CharacteristicValue>();
        List<TpFulfillmentCustomerOrderV3.CustomerAccountOrderItem> customerAccountOrderItems = new List<TpFulfillmentCustomerOrderV3.CustomerAccountOrderItem>();
        
        /*create customerOrder/Specification section
        */
        TpCommonBaseV3.EntitySpecification specification = new TpCommonBaseV3.EntitySpecification();
        specification.Name = OrdrConstants.SPECIFICATION_NAME;
        specification.Type_x = OrdrConstants.SPECIFICATION_TYPE_AVAILABILITY;
        specification.Category = OrdrConstants.SPECIFICATION_CATEGORY;
        customerOrder.Specification = specification;
        
        /*create customerOrder/CharacteristicValue/Characteristic/Name section
        */

        characteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.CHARACTERISTIC_TYPE, 'feasibility'));
        characteristicValues.add(OrdrUtilities.constructCharacteristicValue('interactionRole', 'CustomerAccount'));
        characteristicValues.add(OrdrUtilities.constructCharacteristicValue('customerAccountTypeKey', 'BusinessCustomerAccountType'));
        characteristicValues.add(OrdrUtilities.constructCharacteristicValue('customerAccountKey', (String)inputMap.get(OrdrConstants.CUSTOMER_PROFILE_ID)));
        customerOrder.CharacteristicValue = characteristicValues;
        
        /*create customerOrder/CustomerAccountOrderItem/Address/CharacteristicValue/Characteristic/Name section
        */
        TpFulfillmentCustomerOrderV3.CustomerAccountOrderItem customerAccountOrderItem = new TpFulfillmentCustomerOrderV3.CustomerAccountOrderItem();
        TpCommonUrbanPropertyAddressV3.UrbanPropertyAddress address = OrdrUtilities.constructAddress(inputMap);
        
        customerAccountOrderItem.Address = address;
        //create an empty Customer Account tag as per PCOF IA
        customerAccountOrderItem.CustomerAccount = new TpInventoryCustomerV3.BaseCustomerAccount();
        customerAccountOrderItems.add(customerAccountOrderItem);     
        
        customerOrder.CustomerAccountOrderItem = customerAccountOrderItems;
        
        
        //loop through all LineItems
        List<TpFulfillmentProductOrderV3.ProductOrderItem> productOrderItems = new List<TpFulfillmentProductOrderV3.ProductOrderItem>();
        for (LineItem li : getLineItemsHierarchy((String)inputMap.get('orderId'))) {
            /*create customerOrder/ProductOrderItem section
             * 	Action
                CharacteristicValue/Characteristic/Name
             */            
            TpFulfillmentProductOrderV3.ProductOrderItem productOrderItem = new TpFulfillmentProductOrderV3.ProductOrderItem();
            productOrderItem.Action = li.action;
            characteristicValues = new List<TpCommonBaseV3.CharacteristicValue>();
            characteristicValues.add(OrdrUtilities.constructCharacteristicValue('locationKey', li.locationKey));
            characteristicValues.add(OrdrUtilities.constructCharacteristicValue('offeringKey', li.offeringKey));
            characteristicValues.add(OrdrUtilities.constructCharacteristicValue('lineItemExternalId', li.Id));
            productOrderItem.CharacteristicValue = characteristicValues;            

            /*create customerOrder/ProductOrderItem/Product section
             * CharacteristicValue/Characteristic/Name
             */
            characteristicValues = new List<TpCommonBaseV3.CharacteristicValue>();
            characteristicValues.add(OrdrUtilities.constructCharacteristicValue('apidKey', li.bpiId));
            TpInventoryProductConfigV3.OrderedProduct orderedProduct = new TpInventoryProductConfigV3.OrderedProduct();
            orderedProduct.CharacteristicValue = characteristicValues;
            productOrderItem.Product = orderedProduct;
            
            //loop recursively through child items
            if (li.childItems != null) {
	            productOrderItem.ComponentProductOrderItem = buildComponentProductItems(li.childItems);
            }
            
        	productOrderItems.add(productOrderItem);
            
        }
        customerOrder.ProductOrderItem = productOrderItems;
            
        return customerOrder;
    }
    @TestVisible
    private static List<TpFulfillmentProductOrderV3.ProductOrderItem> buildComponentProductItems(List<LineItem> childItems) {
        List<TpFulfillmentProductOrderV3.ProductOrderItem> componentProductOrderItems = new List<TpFulfillmentProductOrderV3.ProductOrderItem>();
        for (LineItem li : childItems) {
            /*create customerOrder/ProductOrderItem/ComponentProductOrderItem section
            */
            TpFulfillmentProductOrderV3.ProductOrderItem componentProductOrderItem = new TpFulfillmentProductOrderV3.ProductOrderItem();
            componentProductOrderItem.Action = li.action;
            List<TpCommonBaseV3.CharacteristicValue> characteristicValues = new List<TpCommonBaseV3.CharacteristicValue>();
            characteristicValues.add(OrdrUtilities.constructCharacteristicValue('locationKey', li.locationKey));
            characteristicValues.add(OrdrUtilities.constructCharacteristicValue('offeringKey', li.offeringKey));
            characteristicValues.add(OrdrUtilities.constructCharacteristicValue('lineItemExternalId', li.Id));
            componentProductOrderItem.CharacteristicValue = characteristicValues;

            if (li.childItems != null) {
	            componentProductOrderItem.ComponentProductOrderItem = buildComponentProductItems(li.childItems);
            }

            componentProductOrderItems.add(componentProductOrderItem);            

        }
        return componentProductOrderItems;
    }
    @TestVisible
    private static List<LineItem> getLineItemsHierarchy(String orderId) {
        List<OrderItem> orderItems = [
            SELECT Quantity,id, vlocity_cmt__ParentItemId__c, 
            	order.Service_Address__r.Location_Id__c,
            	order.type,
            	Service_Address__r.Location_Id__c, 
            	vlocity_cmt__Product2Id__r.orderMgmtId__c,
            	orderMgmt_BPI_Id__c,
            	isDeleted,
            	amendStatus__c,
            	vlocity_cmt__ProvisioningStatus__c,upgradedowngrade__c
            FROM OrderItem 
            WHERE orderId = :orderId 
            AND vlocity_cmt__Product2Id__r.RelationshipType__c !='Promotion' //Samir - BSBD_RTA-1061 added to filter out Promotion while creating payload 
            ALL ROWS //added to make sure query all deleted records
        ];
        
        //create order item dictionary
        Map<Id, LineItem> orderItemsLookup = new Map<Id, LineItem>();
        Map<String,List<OrderItem>> upgradeDowngradeBpiIdMap=new Map<String,List<OrderItem>>();
        for (OrderItem oli : orderItems) {
            if(oli.UpgradeDowngrade__c){
                if(String.isNotBlank(oli.orderMgmt_BPI_Id__c)){
                    if(upgradeDowngradeBpiIdMap.keySet().contains(oli.orderMgmt_BPI_Id__c)){
                        List<OrderItem> oiList=upgradeDowngradeBpiIdMap.get(oli.orderMgmt_BPI_Id__c);
                         oiList.add(oli);
                        upgradeDowngradeBpiIdMap.put(oli.orderMgmt_BPI_Id__c,oiList);
                    } else{
                        List<OrderItem> oiList=new List<OrderItem>();
                        oiList.add(oli);
                        upgradeDowngradeBpiIdMap.put(oli.orderMgmt_BPI_Id__c,oiList);
                    }
                    
                }
                
            }
        }
        for (OrderItem oli : orderItems) {
            if (oli.isDeleted && String.isBlank(oli.orderMgmt_BPI_Id__c)) {
                //skip and do nothing
                //no need to send deleted line items 
                //when it does not have a corresponding NC Product instance
            } else {
                String provisioningStatus = oli.vlocity_cmt__ProvisioningStatus__c;
                if(oli.upgradedowngrade__c && String.isNotBlank(oli.orderMgmt_BPI_Id__c)
                   && provisioningStatus.equals(OrdrConstants.MACD_STATUS_DELETED)){
                       if(upgradeDowngradeBpiIdMap.keySet().contains(oli.orderMgmt_BPI_Id__c) &&
                          upgradeDowngradeBpiIdMap.get(oli.orderMgmt_BPI_Id__c)!=null && upgradeDowngradeBpiIdMap.get(oli.orderMgmt_BPI_Id__c).size()>1) {
                              continue;
                          }                       
                   }
                LineItem li = new LineItem();
                li.Id = oli.Id;
                li.parentId = oli.vlocity_cmt__ParentItemId__c;
                
                li.action = getLineItemAction(oli,upgradeDowngradeBpiIdMap);
                li.bpiId = oli.orderMgmt_BPI_Id__c;
                li.locationKey = String.isNotBlank(oli.Service_Address__r.Location_Id__c) ? oli.Service_Address__r.Location_Id__c : oli.order.Service_Address__r.Location_Id__c;
                li.offeringKey = oli.vlocity_cmt__Product2Id__r.orderMgmtId__c;
                orderItemsLookup.put(oli.Id, li);
            }   
        }
        
        //build tree
        for (OrderItem oli : orderItems) {
            String key = oli.vlocity_cmt__ParentItemId__c;
            if (String.isNotBlank(key) && orderItemsLookup.containsKey(key)) {
                if (orderItemsLookup.get(key).childItems == null) {
                    orderItemsLookup.get(key).childItems = new List<LineItem>();
                }
                orderItemsLookup.get(key).childItems.add(orderItemsLookup.get(oli.Id));
            }
        }
        
        //trim the tree leaving only the roots (top offer)
        List<LineItem> oliHierarchy = new List<LineItem>();
        for (String key : orderItemsLookup.keySet()) {
            LineItem li = orderItemsLookup.get(key);
            if (String.isBlank(li.parentId)) {
                oliHierarchy.add(li);
            }
        }
        
        return oliHierarchy;
    }
    //Modified for issue BSBD_RTA-820 
    @TestVisible
    private static String getLineItemAction(OrderItem oli,Map<String,List<OrderItem>> orderItemMap) {
        if (oli.order.type == 'New Services') {
            return OrdrConstants.ACTION_ADD;
        } else if (oli.order.type == 'Amend') {
            if (oli.isDeleted && String.isNotBlank(oli.orderMgmt_BPI_Id__c)) {
                return OrdrConstants.ACTION_DELETE;
            }
            String provisioningStatus = oli.amendStatus__c;
            if(String.isNotBlank(provisioningStatus)){
                if(provisioningStatus.equals(OrdrConstants.AMEND_STATUS_ADD)){
                    return OrdrConstants.ACTION_ADD;
                }
                if(provisioningStatus.equals(OrdrConstants.AMEND_STATUS_CHANGE)){
                    return OrdrConstants.ACTION_MODIFY;
                }
            }
        } else if (oli.order.type == 'Change') {
            String provisioningStatus = oli.vlocity_cmt__ProvisioningStatus__c;
            if(oli.Quantity == 0){
                provisioningStatus = OrdrConstantS.MACD_STATUS_DELETED;
            }
            if(String.isNotBlank(provisioningStatus)){
                if(oli.upgradedowngrade__c){
                    if(String.isNotBlank(oli.orderMgmt_BPI_Id__c)){
                       if(orderItemMap.keySet().contains(oli.orderMgmt_BPI_Id__c) &&
                          orderItemMap.get(oli.orderMgmt_BPI_Id__c)!=null && orderItemMap.get(oli.orderMgmt_BPI_Id__c).size()>1) {
                              return OrdrConstants.ACTION_MODIFY;
                          }
                    }
                    
                } else {
                    if(provisioningStatus.equals(OrdrConstants.MACD_STATUS_NEW)){
                        return OrdrConstants.ACTION_ADD;
                    }
                    if(provisioningStatus.equals(OrdrConstants.MACD_STATUS_CHANGED)){
                        return OrdrConstants.ACTION_MODIFY;
                    }
                    if(provisioningStatus.equals(OrdrConstants.MACD_STATUS_DELETED)){
                        return OrdrConstants.ACTION_DELETE;
                    }
                }
                
            }
        } else if (oli.order.type == 'Move') {
            String provisioningStatus = oli.vlocity_cmt__ProvisioningStatus__c;
            if(String.isNotBlank(provisioningStatus) && provisioningStatus.equals(OrdrConstants.MACD_STATUS_NEW)){
                return OrdrConstants.ACTION_ADD;
            }
            if(String.isNotBlank(provisioningStatus) && provisioningStatus.equals(OrdrConstants.MACD_STATUS_CHANGED)){
                return OrdrConstants.ACTION_MODIFY;
            }
            if(String.isNotBlank(provisioningStatus) && provisioningStatus.equals(OrdrConstants.MACD_STATUS_DELETED)){
                return OrdrConstants.ACTION_DELETE;
            }
        }
        
        return OrdrConstants.ACTION_NONE;
    }
    
}