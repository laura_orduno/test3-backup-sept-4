/**
 * Tests for trac_RepairLogger
 * Part of the SMB Repairs project
 *
 * @author: Grant Adamson, Traction On Demand 
 */
 
@isTest
private class trac_RepairLoggerTest {
	private static final String TEST_TYPE = 'testType';
	private static final String TEST_MESSAGE = 'test message';

    private static testMethod void testLog() {
    	Case c = new Case();
    	insert c;
    	
    	Test.startTest();
    	
    	trac_RepairLogger.log(TEST_TYPE, TEST_MESSAGE, c.id);
    	
    	Test.stopTest();
    	
    	List<Repair_Log__c> logs = [SELECT type__c, message__c, case__c FROM Repair_Log__c];
    	system.assertEquals(1, logs.size());
    	system.assertEquals(TEST_TYPE, logs[0].type__c);
    	system.assertEquals(TEST_MESSAGE, logs[0].message__c);
    	system.assertEquals(c.id, logs[0].case__c);
    }
}