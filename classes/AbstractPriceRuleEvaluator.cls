public abstract class AbstractPriceRuleEvaluator extends RuleProcessorSupport {
	public Map<Id,SBQQ__SummaryVariable__c> referencedVariables {get; private set;}
	
    public final Id quoteId;
    private Map<Id, List<SBQQ__PriceAction__c>> actionsMap;
    private Map<Id, List<SBQQ__PriceCondition__c>> conditionsMap;
    private SObject quote;
    
    protected SummaryVariableCalculator variableCalculator;
    
    public AbstractPriceRuleEvaluator(Id quoteId) {  
        this.quoteId = quoteId; 
    }
    
    protected Map<Id, List<SBQQ__PriceCondition__c>> getConditionsMap() {
        return conditionsMap;
    }
    
    public void evaluate() {
        if (!initialized) {
            init();
        }
        
        variableCalculator = new SummaryVariableCalculator(referencedVariables.values());
        variableCalculator.calculate(quote, getTargetObjects());
        
        SBQQ__PriceRule__c[] priceRules = getPriceRules();
        for (SBQQ__PriceRule__c priceRule : priceRules) {
            List<SBQQ__PriceAction__c> priceActions = actionsMap.get(priceRule.Id);
            if (priceActions == null) {
                continue;
            }
            if (!verifyRuleConsistency(priceRule)) {
                continue;
            }
            SObject[] targetObjects = getTargetObjects();
            for (SObject targetObject : targetObjects) {
                if(isRuleSatisfied(priceRule, getEvaluatedRecord(targetObject))) {
                    applyRuleActions(priceActions, targetObject);
                }
            }
        }
    }      
    
    protected void init() {
        loadPriceActions();
        loadPriceConditions();    
        loadQuote();
        initialized = true;
    }
    
    private Boolean verifyRuleConsistency(SBQQ__PriceRule__c priceRule) {
        List<SBQQ__PriceCondition__c> priceConditions = conditionsMap.get(priceRule.Id);
        if (priceConditions == null) {
            return false;
        }
        SObject[] targetObjects = getTargetObjects();
        for (SObject targetObject : targetObjects) {
            SObjectType objectType = targetObject.getSObjectType();     
                for (SBQQ__PriceCondition__c priceCondition : priceConditions) {
                    SObjectType conditionObjectType = AliasedMetadataUtils.getType(priceCondition.SBQQ__Object__c);
                    if (conditionObjectType != SBQQ__Quote__c.sObjectType && conditionObjectType != objectType) {
                        return false;
                }               
            }       
        }
        return true;
    }
    
    private void loadPriceConditions() {
    	Set<Id> referencedVariableIds = new Set<Id>();
    	
        List<Id> priceRuleIds = new List<Id>();
        SBQQ__PriceRule__c[] priceRules = getPriceRules();
        for (SBQQ__PriceRule__c priceRule : priceRules) {
            priceRuleIds.add(priceRule.Id);
        }               
        List<SBQQ__PriceCondition__c> allConditions = [SELECT Id, SBQQ__Object__c, SBQQ__Field__c, SBQQ__Operator__c, SBQQ__Value__c, SBQQ__Rule__c, SBQQ__TestedVariable__c
                                            FROM SBQQ__PriceCondition__c 
                                            WHERE SBQQ__Rule__c in :priceRuleIds];
        conditionsMap = new Map<Id, List<SBQQ__PriceCondition__c>>();     
        for (SBQQ__PriceCondition__c condition : allConditions) {
            List<SBQQ__PriceCondition__c> conditions = conditionsMap.get(condition.SBQQ__Rule__c);
            if (conditions == null) {
                conditions = new List<SBQQ__PriceCondition__c>();
                conditionsMap.put(condition.SBQQ__Rule__c, conditions);
            }
            conditions.add(condition);
            if (condition.SBQQ__TestedVariable__c != null) {
            	referencedVariableIds.add(condition.SBQQ__TestedVariable__c);
            }
        }
        referencedVariables = new Map<Id,SBQQ__SummaryVariable__c>(loadVariables(referencedVariableIds));
    }
    
    private SBQQ__SummaryVariable__c[] loadVariables(Set<Id> varIds) {
    	if (!varIds.isEmpty()) {
    		SummaryVariableDAO dao = new SummaryVariableDAO();
    		return dao.loadByIds(varIds);
    	}
    	return new SBQQ__SummaryVariable__c[]{};
    }
    
    private void loadPriceActions() {
        List<Id> priceRuleIds = new List<Id>();
        SBQQ__PriceRule__c[] priceRules = getPriceRules();
        for (SBQQ__PriceRule__c priceRule : priceRules) {
            priceRuleIds.add(priceRule.Id);
        }               
        List<SBQQ__PriceAction__c> allActions = [SELECT Id, SBQQ__Field__c, SBQQ__Value__c, SBQQ__Rule__c, SBQQ__ValueField__c
                                        FROM SBQQ__PriceAction__c 
                                        WHERE SBQQ__Rule__c in :priceRuleIds];                                
        actionsMap = new Map<Id, List<SBQQ__PriceAction__c>>();       
        for (SBQQ__PriceAction__c action : allActions) {
            List<SBQQ__PriceAction__c> actions = actionsMap.get(action.SBQQ__Rule__c);
            if (actions == null) {
                actions = new List<SBQQ__PriceAction__c>();
                actionsMap.Put(action.SBQQ__Rule__c, actions);
            }
            actions.add(action);
        }
    }
   
    private void applyRuleActions(List<SBQQ__PriceAction__c> priceActions, SObject targetObject) {
        for (SBQQ__PriceAction__c priceAction : priceActions) {
            Schema.SObjectType objectType = targetObject.getSObjectType();
            SObjectField targetField = AliasedMetaDataUtils.getField(objectType, priceAction.SBQQ__Field__c);
            if (targetField != null) {
                if (priceAction.SBQQ__Value__c != null) {
                    targetObject.put(targetField, QuoteUtils.toObject(targetField, priceAction.SBQQ__Value__c));
                } else if (priceAction.SBQQ__ValueField__c != null) {
                    SObject evaluableObject = getEvaluatedRecord(targetObject);
                    Object value = evaluableObject.get(priceAction.SBQQ__ValueField__c);
                    if (value == null) {
                        targetObject.put(targetField, null);
                    } else {
                        targetObject.put(targetField, QuoteUtils.convertNumber(targetField, value));
                    }
                }
            }
        }       
    }

    private Boolean isRuleSatisfied(SBQQ__PriceRule__c rule, SObject evaluableObject) {
        List<SBQQ__PriceCondition__c> priceConditions = conditionsMap.get(rule.Id);
        if (priceConditions == null) {
            return false;
        }
        SObjectType objectType = evaluableObject.getSObjectType();
        Boolean result = false;
        for (SBQQ__PriceCondition__c priceCondition : priceConditions) {
            SObjectType conditionObjectType = AliasedMetadataUtils.getType(priceCondition.SBQQ__Object__c);
            if (SBQQ__Quote__c.sObjectType == conditionObjectType) {
                result = evaluateCondition(rule, priceCondition, quote);
            } else if (objectType == conditionObjectType) {
                result = evaluateCondition(rule, priceCondition, evaluableObject);
            }
            if (result && rule.SBQQ__ConditionsMet__c == 'Any') {
                return true;            
            } else if (!result && rule.SBQQ__ConditionsMet__c == 'All') {
                return false;           
            }
        }
        return rule.SBQQ__ConditionsMet__c == 'All';
    }
    
    private Boolean evaluateCondition(SBQQ__PriceRule__c rule, SBQQ__PriceCondition__c priceCondition, SObject targetObject) {
        Object testedValue = null;
        if (priceCondition.SBQQ__Field__c != null) {
        	SObjectField field = AliasedMetaDataUtils.getField(priceCondition.SBQQ__Object__c, priceCondition.SBQQ__Field__c);
        	testedValue = targetObject.get(field);
        } else if (priceCondition.SBQQ__TestedVariable__c != null) {
        	testedValue = getCombinedValue(priceCondition.SBQQ__TestedVariable__c);
        }
        Object filterValue = priceCondition.SBQQ__Value__c;
        Operators.Logical op = Operators.getInstance(priceCondition.SBQQ__Operator__c);
        if (op == null) {
            throw new ConfigurationException('Invalid operator: ' + priceCondition.SBQQ__Operator__c + ' [' + priceCondition.Id + ']');
        }
        return op.evaluate(testedValue, filterValue);
    }
    
    protected void loadQuote() {
        Set<String> fieldNames = new Set<String>();
        List<SBQQ__PriceRule__c> priceRules = getPriceRules();
        Map<Id, List<SBQQ__PriceCondition__c>> conditionsMap = getConditionsMap();
        for (SBQQ__PriceRule__c priceRule : priceRules) {
            List<SBQQ__PriceCondition__c> priceConditions = conditionsMap.get(priceRule.Id);
            if (priceConditions != null) {
                loadQuoteFieldNames(priceConditions, fieldNames);
            }
        }
        
        Schema.SObjectType quoteType = AliasedMetaDataUtils.determineQuoteType(quoteId);
        QueryBuilder qb = new QueryBuilder(quoteType);
        Boolean hasFields = false;
        for (String fieldName : fieldNames) {
            qb.getSelectClause().addField(fieldName);
            hasFields = true;
        }
        if (hasFields) {
            qb.getWhereClause().addExpression(qb.eq(SBQQ__Quote__c.Id, quoteId));
            quote = Database.query(qb.buildSOQL());         
        } else {
            quote = quoteType.newSObject(quoteId);      
        }
    }    
    
    private void loadQuoteFieldNames(List<SBQQ__PriceCondition__c> priceConditions, Set<String> fieldNames) {
        for (SBQQ__PriceCondition__c priceCondition : priceConditions) {
            if (priceCondition.SBQQ__Object__c == 'Quote' && priceCondition.SBQQ__Field__c != null) {
                fieldNames.add(priceCondition.SBQQ__Field__c);
            }
        }       
    }  
    
    public Decimal getCombinedValue(Id varId) {
        Decimal value = variableCalculator.getResult(varId);
        SBQQ__SummaryVariable__c var = referencedVariables.get(varId);
        if ((var != null) && (var.SBQQ__CombineWith__c != null)) {
            value = QuoteUtils.addAsZero(value, getCombinedValue(var.SBQQ__CombineWith__c));
        }
        return value;
    }
    
    public abstract SBQQ__PriceRule__c[] getPriceRules();
    public abstract SObject[] getTargetObjects();
    
    public class ConfigurationException extends Exception {}
    
        
 }