@isTest
public class RestApi_MyAccountHelper_UpdateCaseTest {

	private static final String REQUEST_TYPE_UPDATE_CASE = 'update-case';
	private static final String UUID = '909090';
	private static final String CASE_ORIGIN = 'My Account';
	private static final String BILLINGACCOUNTNUMBER = '1337';
	private static final String CONTACTFIRST = 'Phillip';
	private static final String CONTACTLAST = 'Smith';
	private static final String CONTACTFULL = CONTACTFIRST + ' ' + CONTACTLAST;
	private static final String USERNAME = 'psmith@telus.com' + System.currentTimeMillis();

	private static User myUser;
	private static Case myCase;
	private static Account myAcct;
	private static Account portalAcct;
	private static RestApi_MyAccountHelper_UpdateCase UpdateCaseRequestObj;
	private static RestApi_MyAccountHelper_UpdateCase.RequestUpdateCase ruc;
	private static RestApi_MyAccountHelper_UpdateCase.UpdateCaseResponse resp;

	private static void setup() {
		trac_TriggerHandlerBase.blockTrigger = true;
		User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
		System.RunAs(thisUser) {
			List<ExternalToInternal__c> ei = new List<ExternalToInternal__c>();
			ei.add(RestApi_MyAccountHelper_TestUtils.createEtoICancelled());
			ei.add(RestApi_MyAccountHelper_TestUtils.createEtoI());
			ei.add(RestApi_MyAccountHelper_TestUtils.createEtoIRT());
			insert ei;
		}

		RestApi_MyAccountHelper_TestUtils.MASRUserBundle bundle = RestApi_MyAccountHelper_TestUtils.createMASREnabledUser();

		myCase = RestApi_MyAccountHelper_TestUtils.createCase(bundle.banAccount, bundle.u);
		insert myCase;

		ruc = createRUC();
		trac_TriggerHandlerBase.blockTrigger = false;
	}

	@isTest
	private static void testRequest() {
		setup();
		createRequest(ruc, RestApi_MyAccountHelper_TestUtils.UUID, new List<String>{
				RestApi_MyAccountHelper_TestUtils.BILLINGACCOUNTNUMBER
		});

		Test.startTest();
		UpdateCaseRequestObj = new RestApi_MyAccountHelper_UpdateCase(RestContext.request.requestBody.toString());
		resp = UpdateCaseRequestObj.response;
		Test.stopTest();

	}

	@isTest
	private static void testRequestUUIDBlank() {
		setup();
		createRequest(ruc, '', new List<String>{
				RestApi_MyAccountHelper_TestUtils.BILLINGACCOUNTNUMBER
		});

		try {
			Test.startTest();
			UpdateCaseRequestObj = new RestApi_MyAccountHelper_UpdateCase(RestContext.request.requestBody.toString());
			resp = UpdateCaseRequestObj.response;
			Test.stopTest();
			System.assert(false, 'error');
		} catch (Exception e) {
			System.assert(true, 'error was thrown');
		}
	}

	@isTest
	private static void testRequestBillingAccountNumber() {
		setup();
		createRequest(ruc, RestApi_MyAccountHelper_TestUtils.UUID, null);

		try {
			Test.startTest();
			UpdateCaseRequestObj = new RestApi_MyAccountHelper_UpdateCase(RestContext.request.requestBody.toString());
			resp = UpdateCaseRequestObj.response;
			Test.stopTest();
			System.assert(false, 'error');
		} catch (Exception e) {
			System.assert(true, 'error was thrown');
		}

	}

	@isTest
	private static void testRequestCaseNull() {
		setup();
		createRequest(null, RestApi_MyAccountHelper_TestUtils.UUID, new List<String>{
				RestApi_MyAccountHelper_TestUtils.BILLINGACCOUNTNUMBER
		});

		try {
			Test.startTest();
			UpdateCaseRequestObj = new RestApi_MyAccountHelper_UpdateCase(RestContext.request.requestBody.toString());
			resp = UpdateCaseRequestObj.response;
			Test.stopTest();
			System.assert(false, 'error');
		} catch (Exception e) {
			System.assert(true, 'error was thrown');
		}
	}

	private static void createRequest(RestApi_MyAccountHelper_UpdateCase.RequestUpdateCase ruc, String uuid, List<String> billing) {
		RestApi_MyAccountHelper_UpdateCase.UpdateCaseRequestObject updateCaseRequest = new RestApi_MyAccountHelper_UpdateCase.UpdateCaseRequestObject();
		updateCaseRequest.uuid = uuid;
		updateCaseRequest.caseToUpdate = ruc;
		updateCaseRequest.billing = billing;
		updateCaseRequest.type = REQUEST_TYPE_UPDATE_CASE;
		RestRequest request = new RestRequest();
		request.requestUri = '/services/apexrest/RestApi_MyAccount/';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(JSON.serialize(updateCaseRequest));
		RestContext.request = request;
	}

	private static RestApi_MyAccountHelper_UpdateCase.RequestUpdateCase createRUC() {
		RestApi_MyAccountHelper_UpdateCase.RequestUpdateCase reqUpdateCase = new RestApi_MyAccountHelper_UpdateCase.RequestUpdateCase();
		reqUpdateCase.id = myCase.Id;
		reqUpdateCase.sendNotification = false;
		reqUpdateCase.collaborators = null;
		reqUpdateCase.cancelCase = false;
		return reqUpdateCase;
	}

	private static User createPortalUser() {
		Contact contact1 = new Contact(
				FirstName = CONTACTFIRST,
				Lastname = CONTACTLAST,
				AccountId = portalAcct.Id,
				Email = System.now().millisecond() + 'test@test.com'
		);

		insert contact1;

		return new User(
				ProfileId = [SELECT Id FROM Profile WHERE Name = 'Customer Community User Custom'].Id,
				LastName = CONTACTLAST,
				FirstName = CONTACTFIRST,
				Email = 'puser000@amamama.com',
				Username = USERNAME,
				CompanyName = 'TEST',
				Title = 'title',
				Alias = 'alias',
				TimeZoneSidKey = 'America/Los_Angeles',
				EmailEncodingKey = 'UTF-8',
				LanguageLocaleKey = 'en_US',
				LocaleSidKey = 'en_US',
				ContactId = contact1.Id,
				FederationIdentifier = UUID
		);
	}

	private static Case createCase(Account a, User u) {
		Case caseToInsert = new Case();
		caseToInsert.Origin = CASE_ORIGIN;
		caseToInsert.accountId = a.Id;
		caseToInsert.contactId = u.ContactId;
		caseToInsert.Status = 'Cancelled';
		return caseToInsert;
	}
}