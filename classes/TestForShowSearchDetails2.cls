@isTest(SeeAllData=true)
public class TestForShowSearchDetails2{
    public static testMethod void testStrProv(){       
       String cbucid='0002229208';
       String rcid='0001362414';
       String strProv='PQ';
       Integer newPageIndex=1;
       PageReference objPage;
       System.currentPagereference().getParameters().put('cbucid','0002229208');
       System.currentPagereference().getParameters().put('strCbucid','0002229208');
       System.currentPagereference().getParameters().put('rcid','0001362414');       
       System.currentPagereference().getParameters().put('strProv','PQ');   
       System.currentPagereference().getParameters().put('strAlpa','PQ');   
       System.currentPagereference().getParameters().put('strIndex','0.0');   
           
       ShowDetailForAssignmentProv obj=new ShowDetailForAssignmentProv();
       obj.strProv='SK';
       obj.showCustomerDetailWithStatus();
       obj.strProv='PQ';
       obj.showCustomerDetailWithStatus();
    }
    
    public static testMethod void testAtmList(){       
       String cbucid='0002229208';
       String rcid='0001362414';
       String strProv='PQ';
       Integer newPageIndex=1;
       PageReference objPage;
       System.currentPagereference().getParameters().put('cbucid','0002229208');
       System.currentPagereference().getParameters().put('strCbucid','0002229208');
       System.currentPagereference().getParameters().put('rcid','0001362414');       
       System.currentPagereference().getParameters().put('strProv','PQ');   
       System.currentPagereference().getParameters().put('strAlpa','PQ');   
       System.currentPagereference().getParameters().put('strIndex','0.0');   
       
       AccountTeamMember atm=new AccountTeamMember(accountId='0014000000QKxNFAA1',Userid='00540000001CHGvAAO',TeamMemberRole='TEST');
       insert atm;
       
       ShowDetailForAssignmentProv obj=new ShowDetailForAssignmentProv();
       obj.strProv='PQ';
       
       obj.Rcidnumber='0001362414';
       obj.cbucidnumber='0002229208';
     
       obj.customerName='LES EDITIONS CEC';
       obj.cbucidName='QUEBECOR MEDIA INC';
       obj.banOrBcan='LES EDITIONS CEC INC';
       
       obj.selectedTabForSearch='customerName';
       obj.txtForSearch='zzzzzz';
       obj.searchCustomerName();
       obj.txtForSearch='CITY OF VANCOUVER';
       obj.searchCustomerName();
       
       obj.selectedTabForSearch='cbucidName';
       obj.txtForSearch='zzzzzz';
       obj.searchCbucidName();
       obj.txtForSearch='QUEBECOR MEDIA INC';
       obj.searchCbucidName();
       
       obj.selectedTabForSearch='banOrBcan';
       obj.txtForSearch='zzzzzz';
       obj.searchBanOrBcan();
       obj.txtForSearch='LES EDITIONS CEC INC';
       obj.searchBanOrBcan();
       
       obj.selectedTabForSearch='Cbucidnumber';
       obj.txtForSearch='0002229208';
       obj.searchCbucidnumber();
       
       obj.selectedTabForSearch='rcidnumber';
       obj.txtForSearch='0001362414';
       obj.searchRcidnumber();

      
       objPage=obj.firstBtnClick();
       System.assertEquals(null,objPage);
       objPage=obj.previousBtnClick(); 
       System.assertEquals(null,objPage);
       objPage=obj.nextBtnClick(); 
       System.assertEquals(null,objPage);
       objPage=obj.lastBtnClick(); 
       System.assertEquals(null,objPage); 
       newPageIndex=2;         
       obj.getPageNumber();
       obj.getPageSize();
       obj.getTotalPageNumber();
       obj.getPageAccountList();
       
       obj.getSalesList();
       obj.getShowClientServiceCreditList();
       obj.getCustStatusTool();
       obj.getAccountDetail();
       obj.getMsgForSearchResult();
       obj.getMsgForPosition();
       
       List<AccountTeamMember> atmList=new List<AccountTeamMember>();
                        atmList=[Select a.UserId,a.user.name, a.user.firstname, a.user.lastname, a.user.email, a.user.phone,
                        a.TeamMemberRole, a.AccountId From AccountTeamMember a
                        where a.user.isactive=true and a.Account.RCID__c='0001362414' limit 1000];
       for(AccountTeamMember atest:atmList){
           atest.TeamMemberRole='Test';
       }
       update atmList;
       
       obj.showCustomerDetailWithStatus();
       
       atmList=new List<AccountTeamMember>();
                        atmList=[Select a.UserId,a.user.name, a.user.firstname, a.user.lastname, a.user.email, a.user.phone,
                        a.TeamMemberRole, a.AccountId From AccountTeamMember a
                        where a.user.isactive=true and a.Account.RCID__c='0001362414' limit 1000];
       for(AccountTeamMember atest:atmList){
           atest.TeamMemberRole='Bus Sales Analyst';
       }
       update atmList;
       
       obj.showCustomerDetailWithStatus();
              
       obj.showSalesDetail(strProv); 

       System.currentPagereference().getParameters().put('strColorStatus','Red');   
       obj.searchCustomerBasedOnStatus();
       System.currentPagereference().getParameters().put('strColorStatus','Yellow');   
       obj.searchCustomerBasedOnStatus();
       System.currentPagereference().getParameters().put('strColorStatus','Green');   
       obj.searchCustomerBasedOnStatus();
       
       objPage=obj.doSearchProv();
       System.assertEquals(new PageReference('/apex/ShowDetailForAssignmentProv').getURL(),objPage.getURL());
       obj.doSearchCbucid();
       obj.showSearchResult();
       obj.searchFromAccountTab(); 
       obj.showAtoZEasyMap();
       obj.showAtoZEasyMapFR();
          
       Map<String, AtoZ_Assignment__c> mapAtoZ=obj.populateNatlAssignments();
       Schema.DescribeFieldResult implMtrxStage = AtoZ_Assignment__c.AtoZ_Field__c.getDescribe();
       List<Schema.PicklistEntry> Ple = implMtrxStage.getPicklistValues();
       
       List<AtoZ_Assignment__c> clientServiceCreditList=[
        Select a.Tel__c, a.Email__c, a.AtoZ_Field__c, a.AtoZ_Contact__r.Id, a.AtoZ_Contact__r.Full_Name__c,
        a.AtoZ_Contact__r.Email__c ,a.AtoZ_Contact__r.Phone__c, a.AtoZ_Contact__r.External_Email__c
        From AtoZ_Assignment__c a
        where a.Prov_Team__c=:strProv
        limit 1
       ];
       
       Map<String, String> assignClientServiceCredit1=obj.assignClientServiceCreditValue(Ple,clientServiceCreditList);
       Map<String, String> assignClientServiceCredit2=obj.assignClientServiceCreditValueNew (mapAtoZ);  
       String strProvSt = 'BC';     
       
       List<Sales_Assignment__c> findSalesList=[
        Select s.Id,s.User__r.Id, s.User__r.Team_TELUS_ID__c, s.Role__c, s.User__r.firstname,s.User__r.lastname, s.User__r.Email,
        s.User__r.Phone,s.User__r.UserRoleID,s.User__r.UserRole.Name, s.Account__r.rcid__c
        From Sales_Assignment__c s
        where s.Account__r.rcid__c='0001362414'
        limit 1
       ];
       
       Map<String, String> showSalesRole=obj.showSalesRoleUserName(findSalesList, strProvSt);  

    }
    
    public static testMethod void testAtmList2(){       
       String cbucid='0002229208';
       String rcid='0001362414';
       String strProv='PQ';
       Integer newPageIndex=1;
       PageReference objPage;
       System.currentPagereference().getParameters().put('cbucid','0002229208');
       System.currentPagereference().getParameters().put('cbucidnumber','0002229208');
       System.currentPagereference().getParameters().put('strCbucid','0002229208');
       System.currentPagereference().getParameters().put('rcid','0001362414');
       System.currentPagereference().getParameters().put('rcidnumber','0001362414');       
       System.currentPagereference().getParameters().put('banOrBcan','0001362414');       
       System.currentPagereference().getParameters().put('strProv','PQ');   
       System.currentPagereference().getParameters().put('strAlpa','PQ');   
       System.currentPagereference().getParameters().put('strIndex','0.0');   
       
       AccountTeamMember atm=new AccountTeamMember(accountId='0014000000QKxNFAA1',Userid='00540000001CHGvAAO',TeamMemberRole='TEST');
       insert atm;
       
       ShowDetailForAssignmentProv obj=new ShowDetailForAssignmentProv();
       obj.strProv='PQ';
       
       obj.Rcidnumber='0001362414';
       obj.cbucidnumber='0002229208';
     
       obj.customerName='LES EDITIONS CEC';
       obj.cbucidName='QUEBECOR MEDIA INC';
       obj.banOrBcan='LES EDITIONS CEC INC';
       
       obj.selectedTabForSearch='customerName';
       obj.txtForSearch='zzzzzz';
       obj.searchCustomerName();
       obj.txtForSearch='CITY OF VANCOUVER';
       obj.searchCustomerName();
       
       obj.selectedTabForSearch='cbucidName';
       obj.txtForSearch='zzzzzz';
       obj.searchCbucidName();
       obj.txtForSearch='QUEBECOR MEDIA INC';
       obj.searchCbucidName();
       
       obj.selectedTabForSearch='banOrBcan';
       obj.txtForSearch='zzzzzz';
       obj.searchBanOrBcan();
       obj.txtForSearch='LES EDITIONS CEC INC';
       obj.searchBanOrBcan();
       
       obj.selectedTabForSearch='Cbucidnumber';
       obj.txtForSearch='0002229208';
       obj.searchCbucidnumber();
       
       obj.selectedTabForSearch='rcidnumber';
       obj.txtForSearch='0001362414';
       obj.searchRcidnumber();

      
       objPage=obj.firstBtnClick();
       System.assertEquals(null,objPage);
       objPage=obj.previousBtnClick(); 
       System.assertEquals(null,objPage);
       objPage=obj.nextBtnClick(); 
       System.assertEquals(null,objPage);
       objPage=obj.lastBtnClick(); 
       System.assertEquals(null,objPage); 
       newPageIndex=2;         
       obj.getPageNumber();
       obj.getPageSize();
       obj.getTotalPageNumber();
       obj.getPageAccountList();
       
       obj.getSalesList();
       obj.getShowClientServiceCreditList();
       obj.getCustStatusTool();
       obj.getAccountDetail();
       obj.getMsgForSearchResult();
       obj.getMsgForPosition();

       List<Account> aList=new List<Account>();
                        aList=[Select a.rcid__c From Account a
                        where a.RCID__c='0001362414' limit 1000];
       for(Account atest:aList){
           atest.rcid__c='234234234';
       }
       update aList;
       
       /*List<AccountTeamMember> atmList=new List<AccountTeamMember>();
                        atmList=[Select a.UserId,a.user.name, a.user.firstname, a.user.lastname, a.user.email, a.user.phone,
                        a.TeamMemberRole, a.AccountId From AccountTeamMember a
                        where a.Account.CBUCID_formula__c='0002229208' limit 1000];
       for(AccountTeamMember atest:atmList){
           atest.TeamMemberRole='Test';
       }
       update atmList;
       
       obj.showCustomerDetailWithStatus();
       */
       List<AccountTeamMember> atmList=new List<AccountTeamMember>();
                        atmList=[Select a.UserId,a.user.name, a.user.firstname, a.user.lastname, a.user.email, a.user.phone,
                        a.TeamMemberRole, a.AccountId From AccountTeamMember a
                        where a.user.isactive=true and a.Account.rcid__c='234234234' limit 1000];
       for(AccountTeamMember atest:atmList){
           atest.TeamMemberRole='Bus Sales Analyst';
       }
       update atmList;
       
       obj.showCustomerDetailWithStatus();
              
       obj.showSalesDetail(strProv); 

       System.currentPagereference().getParameters().put('strColorStatus','Red');   
       obj.searchCustomerBasedOnStatus();
       System.currentPagereference().getParameters().put('strColorStatus','Yellow');   
       obj.searchCustomerBasedOnStatus();
       System.currentPagereference().getParameters().put('strColorStatus','Green');   
       obj.searchCustomerBasedOnStatus();
       
       objPage=obj.doSearchProv();
       System.assertEquals(new PageReference('/apex/ShowDetailForAssignmentProv').getURL(),objPage.getURL());
       obj.doSearchCbucid();
       obj.showSearchResult();
       obj.searchFromAccountTab(); 
       obj.showAtoZEasyMap();
       obj.showAtoZEasyMapFR();
          
       Map<String, AtoZ_Assignment__c> mapAtoZ=obj.populateNatlAssignments();
       Schema.DescribeFieldResult implMtrxStage = AtoZ_Assignment__c.AtoZ_Field__c.getDescribe();
       List<Schema.PicklistEntry> Ple = implMtrxStage.getPicklistValues();
       
       List<AtoZ_Assignment__c> clientServiceCreditList=[
        Select a.Tel__c, a.Email__c, a.AtoZ_Field__c, a.AtoZ_Contact__r.Id, a.AtoZ_Contact__r.Full_Name__c,
        a.AtoZ_Contact__r.Email__c ,a.AtoZ_Contact__r.Phone__c, a.AtoZ_Contact__r.External_Email__c
        From AtoZ_Assignment__c a
        where a.Prov_Team__c=:strProv
        limit 1
       ];
       
       Map<String, String> assignClientServiceCredit1=obj.assignClientServiceCreditValue(Ple,clientServiceCreditList);
       Map<String, String> assignClientServiceCredit2=obj.assignClientServiceCreditValueNew (mapAtoZ);  
       String strProvSt = 'BC';     
       
       List<Sales_Assignment__c> findSalesList=[
        Select s.Id,s.User__r.Id, s.User__r.Team_TELUS_ID__c, s.Role__c, s.User__r.firstname,s.User__r.lastname, s.User__r.Email,
        s.User__r.Phone,s.User__r.UserRoleID,s.User__r.UserRole.Name, s.Account__r.rcid__c
        From Sales_Assignment__c s
        where s.Account__r.rcid__c='0001362414'
        limit 1
       ];
       
       Map<String, String> showSalesRole=obj.showSalesRoleUserName(findSalesList, strProvSt);  

    }
    
    public static testMethod void testShowSalesRoleUserName(){       
       String cbucid='0000563979';
       String rcid='0000513162';
       String strProv='PQ';
       Integer newPageIndex=1;
       PageReference objPage;
       System.currentPagereference().getParameters().put('cbucid','0000563979');
       System.currentPagereference().getParameters().put('strCbucid','0000563979');
       System.currentPagereference().getParameters().put('rcid','0000513162');       
       System.currentPagereference().getParameters().put('strProv','PQ');   
       System.currentPagereference().getParameters().put('strAlpa','PQ');   
       System.currentPagereference().getParameters().put('strIndex','0.0');   
       
       AccountTeamMember atm=new AccountTeamMember(accountId='0014000000QKxNFAA1',Userid='00540000001CHGvAAO',TeamMemberRole='TEST');
       insert atm;
       
       ShowDetailForAssignmentProv obj=new ShowDetailForAssignmentProv();
       obj.strProv='PQ';
       
       obj.Rcidnumber='0000513162';
       obj.cbucidnumber='0000563979';
     
       obj.customerName='LES EDITIONS CEC';
       obj.cbucidName='QUEBECOR MEDIA INC';
       obj.banOrBcan='LES EDITIONS CEC INC';
       
       obj.selectedTabForSearch='customerName';
       obj.txtForSearch='zzzzzz';
       obj.searchCustomerName();
       obj.txtForSearch='CITY OF VANCOUVER';
       obj.searchCustomerName();
       
       obj.selectedTabForSearch='cbucidName';
       obj.txtForSearch='zzzzzz';
       obj.searchCbucidName();
       obj.txtForSearch='QUEBECOR MEDIA INC';
       obj.searchCbucidName();
       
       obj.selectedTabForSearch='banOrBcan';
       obj.txtForSearch='zzzzzz';
       obj.searchBanOrBcan();
       obj.txtForSearch='LES EDITIONS CEC INC';
       obj.searchBanOrBcan();
       
       obj.selectedTabForSearch='Cbucidnumber';
       obj.txtForSearch='0000563979';
       obj.searchCbucidnumber();
       
       obj.selectedTabForSearch='rcidnumber';
       obj.txtForSearch='0000513162';
       obj.searchRcidnumber();

      
       objPage=obj.firstBtnClick();
       System.assertEquals(null,objPage);
       objPage=obj.previousBtnClick(); 
       System.assertEquals(null,objPage);
       objPage=obj.nextBtnClick(); 
       System.assertEquals(null,objPage);
       objPage=obj.lastBtnClick(); 
       System.assertEquals(null,objPage); 
       newPageIndex=2;         
       obj.getPageNumber();
       obj.getPageSize();
       obj.getTotalPageNumber();
       obj.getPageAccountList();
       
       obj.getSalesList();
       obj.getShowClientServiceCreditList();
       obj.getCustStatusTool();
       Account testaccount = obj.getAccountDetail();
       obj.getMsgForSearchResult();
       obj.getMsgForPosition();
              
       String strProvSt = 'BC';     
       
       List<Sales_Assignment__c> findSalesList=[
        Select s.Id,s.User__r.Id, s.User__r.Team_TELUS_ID__c, s.Role__c, s.User__r.firstname,s.User__r.lastname, s.User__r.Email,
        s.User__r.Phone,s.User__r.UserRoleID,s.User__r.UserRole.Name, s.Account__r.rcid__c
        From Sales_Assignment__c s
        where s.Account__r.rcid__c='0001362414'
        limit 1
       ];
       
       Map<String, String> showSalesRole=obj.showSalesRoleUserName(findSalesList, strProvSt);                
              
       System.debug('testaccount.Id:'+testaccount.Id);
       List<AtoZ_Assignment__c> atozaList=new List<AtoZ_Assignment__c>();
       atozaList=[Select a.Id,a.Tel__c, a.Email__c, a.AtoZ_Field__c, a.AtoZ_Contact__r.Id,a.AtoZ_Contact__c,
                a.AtoZ_Contact__r.Full_Name__c,a.AtoZ_Contact__r.Email__c ,a.AtoZ_Contact__r.Phone__c  From AtoZ_Assignment__c a where a.RCID__c=:testaccount.Id];
       System.debug('atozaList size:'+atozaList.size());
        
       for(AtoZ_Assignment__c atest:atozaList){
           atest.AtoZ_Field__c='ACCTPRIME';
       }
       update atozaList;
       showSalesRole=obj.showSalesRoleUserName(findSalesList, strProvSt);
       
       atozaList=new List<AtoZ_Assignment__c>();
       atozaList=[Select a.Id,a.Tel__c, a.Email__c, a.AtoZ_Field__c, a.AtoZ_Contact__r.Id,a.AtoZ_Contact__c,
                a.AtoZ_Contact__r.Full_Name__c,a.AtoZ_Contact__r.Email__c ,a.AtoZ_Contact__r.Phone__c  From AtoZ_Assignment__c a where a.RCID__c=:testaccount.Id];
       for(AtoZ_Assignment__c atest:atozaList){
           atest.AtoZ_Field__c='ASR';
       }
       update atozaList;
       showSalesRole=obj.showSalesRoleUserName(findSalesList, strProvSt);
       
       atozaList=new List<AtoZ_Assignment__c>();
       atozaList=[Select a.Id,a.Tel__c, a.Email__c, a.AtoZ_Field__c, a.AtoZ_Contact__r.Id,a.AtoZ_Contact__c,
                a.AtoZ_Contact__r.Full_Name__c,a.AtoZ_Contact__r.Email__c ,a.AtoZ_Contact__r.Phone__c  From AtoZ_Assignment__c a where a.RCID__c=:testaccount.Id];
       for(AtoZ_Assignment__c atest:atozaList){
           atest.AtoZ_Field__c='WLS_SR';
       }
       update atozaList;
       showSalesRole=obj.showSalesRoleUserName(findSalesList, strProvSt);

       atozaList=new List<AtoZ_Assignment__c>();
       atozaList=[Select a.Id,a.Tel__c, a.Email__c, a.AtoZ_Field__c, a.AtoZ_Contact__r.Id,a.AtoZ_Contact__c,
                a.AtoZ_Contact__r.Full_Name__c,a.AtoZ_Contact__r.Email__c ,a.AtoZ_Contact__r.Phone__c  From AtoZ_Assignment__c a where a.RCID__c=:testaccount.Id];
       for(AtoZ_Assignment__c atest:atozaList){
           atest.AtoZ_Field__c='WLS_ASR';
       }
       update atozaList;
       showSalesRole=obj.showSalesRoleUserName(findSalesList, strProvSt);
       
       

    }
    
    public static testMethod void testSearchTabs(){       
           
       ShowDetailForAssignmentProv obj=new ShowDetailForAssignmentProv();
              
       obj.selectedTabForSearch='rcidnumber';
       obj.txtForSearch='0001362414';
       obj.showSearchResult();
       obj.selectedTabForSearch='cbucidnumber';
       obj.showSearchResult();
       obj.selectedTabForSearch='customerName';
       obj.showSearchResult();
       obj.selectedTabForSearch='cbucidName';
       obj.showSearchResult();
       obj.selectedTabForSearch='banOrBcan';
       obj.showSearchResult();

    }
    
    public static testMethod void testProvNull(){       
       String cbucid='0002229208';
       String rcid='0001362414';
       Integer newPageIndex=1;
       PageReference objPage;
       System.currentPagereference().getParameters().put('strAlpa','0002229208');
       System.currentPagereference().getParameters().put('strCbucid','0002229208');
       System.currentPagereference().getParameters().put('rcid','0001362414');       
       System.currentPagereference().getParameters().put('strProv','PQ');   
       System.currentPagereference().getParameters().put('strAlpa','PQ');   
       System.currentPagereference().getParameters().put('strIndex','0.0');   
           
           
       ShowDetailForAssignmentProv obj=new ShowDetailForAssignmentProv();
       obj.doSearchProv();
       
    }
    
    public static testMethod void testNull(){       
       ShowDetailForAssignmentProv obj=new ShowDetailForAssignmentProv();
       obj.searchcbucidnumber();
       
    }
    
    public static testMethod void testProvABAssignFG(){       
       String cbucid='0002229208';
       String rcid='0001362414';
       String strProv='AB';
       Integer newPageIndex=1;
       PageReference objPage;
       System.currentPagereference().getParameters().put('cbucid','0002229208');
       System.currentPagereference().getParameters().put('cbucidnumber','0002229208');
       System.currentPagereference().getParameters().put('strCbucid','0002229208');
       System.currentPagereference().getParameters().put('rcid','0001362414');
       System.currentPagereference().getParameters().put('rcidnumber','0001362414');       
       System.currentPagereference().getParameters().put('banOrBcan','0001362414');       
       System.currentPagereference().getParameters().put('strProv','AB');   
       System.currentPagereference().getParameters().put('strAlpa','AB');   
       System.currentPagereference().getParameters().put('strIndex','0.0');   
       
       AccountTeamMember atm=new AccountTeamMember(accountId='0014000000QKxNFAA1',Userid='00540000001CHGvAAO',TeamMemberRole='TEST');
       insert atm;
       
       ShowDetailForAssignmentProv obj=new ShowDetailForAssignmentProv();
       obj.strProv='AB';
       
       obj.Rcidnumber='0001362414';
       obj.cbucidnumber='0002229208';
     
       obj.customerName='LES EDITIONS CEC';
       obj.cbucidName='QUEBECOR MEDIA INC';
       obj.banOrBcan='LES EDITIONS CEC INC';
       
       obj.selectedTabForSearch='customerName';
       obj.txtForSearch='zzzzzz';
       obj.searchCustomerName();
       obj.txtForSearch='CITY OF VANCOUVER';
       obj.searchCustomerName();
       
       obj.selectedTabForSearch='cbucidName';
       obj.txtForSearch='zzzzzz';
       obj.searchCbucidName();
       obj.txtForSearch='QUEBECOR MEDIA INC';
       obj.searchCbucidName();
       
       obj.selectedTabForSearch='banOrBcan';
       obj.txtForSearch='zzzzzz';
       obj.searchBanOrBcan();
       obj.txtForSearch='LES EDITIONS CEC INC';
       obj.searchBanOrBcan();
       
       obj.selectedTabForSearch='Cbucidnumber';
       obj.txtForSearch='0002229208';
       obj.searchCbucidnumber();
       
       obj.selectedTabForSearch='rcidnumber';
       obj.txtForSearch='0001362414';
       obj.searchRcidnumber();

      
       objPage=obj.firstBtnClick();
       System.assertEquals(null,objPage);
       objPage=obj.previousBtnClick(); 
       System.assertEquals(null,objPage);
       objPage=obj.nextBtnClick(); 
       System.assertEquals(null,objPage);
       objPage=obj.lastBtnClick(); 
       System.assertEquals(null,objPage); 
       newPageIndex=2;         
       obj.getPageNumber();
       obj.getPageSize();
       obj.getTotalPageNumber();
       obj.getPageAccountList();
       
       obj.getSalesList();
       obj.getShowClientServiceCreditList();
       obj.getCustStatusTool();
       obj.getAccountDetail();
       obj.getMsgForSearchResult();
       obj.getMsgForPosition();

       List<Account> aList=new List<Account>();
                        aList=[Select a.rcid__c,a.CBU_Code_RCID__c From Account a
                        where a.RCID__c='0001362414' limit 1000];
       for(Account atest:aList){
           atest.rcid__c='234234234';
       }
       update aList;
       
       /*List<AccountTeamMember> atmList=new List<AccountTeamMember>();
                        atmList=[Select a.UserId,a.user.name, a.user.firstname, a.user.lastname, a.user.email, a.user.phone,
                        a.TeamMemberRole, a.AccountId From AccountTeamMember a
                        where a.Account.CBUCID_formula__c='0002229208' limit 1000];
       for(AccountTeamMember atest:atmList){
           atest.TeamMemberRole='Test';
       }
       update atmList;
       
       obj.showCustomerDetailWithStatus();
       */
       List<AccountTeamMember> atmList=new List<AccountTeamMember>();
                        atmList=[Select a.UserId,a.user.name, a.user.firstname, a.user.lastname, a.user.email, a.user.phone,
                        a.TeamMemberRole, a.AccountId From AccountTeamMember a
                        where a.user.isactive=true and a.Account.rcid__c='234234234' limit 1000];
       for(AccountTeamMember atest:atmList){
           atest.TeamMemberRole='Bus Sales Analyst';
       }
       update atmList;
       
       obj.showCustomerDetailWithStatus();
              
       obj.showSalesDetail(strProv); 

       System.currentPagereference().getParameters().put('strColorStatus','Red');   
       obj.searchCustomerBasedOnStatus();
       System.currentPagereference().getParameters().put('strColorStatus','Yellow');   
       obj.searchCustomerBasedOnStatus();
       System.currentPagereference().getParameters().put('strColorStatus','Green');   
       obj.searchCustomerBasedOnStatus();
       
       objPage=obj.doSearchProv();
       System.assertEquals(new PageReference('/apex/ShowDetailForAssignmentProv').getURL(),objPage.getURL());
       obj.doSearchCbucid();
       obj.showSearchResult();
       obj.searchFromAccountTab(); 
       obj.showAtoZEasyMap();
       obj.showAtoZEasyMapFR();
          
       Map<String, AtoZ_Assignment__c> mapAtoZ=obj.populateNatlAssignments();
       Schema.DescribeFieldResult implMtrxStage = AtoZ_Assignment__c.AtoZ_Field__c.getDescribe();
       List<Schema.PicklistEntry> Ple = implMtrxStage.getPicklistValues();
       
       List<AtoZ_Assignment__c> clientServiceCreditList=[
        Select a.Tel__c, a.Email__c, a.AtoZ_Field__c, a.AtoZ_Contact__r.Id, a.AtoZ_Contact__r.Full_Name__c,
        a.AtoZ_Contact__r.Email__c ,a.AtoZ_Contact__r.Phone__c, a.AtoZ_Contact__r.External_Email__c
        From AtoZ_Assignment__c a
        where a.Prov_Team__c=:strProv
        limit 1
       ];
       
       Map<String, String> assignClientServiceCredit1=obj.assignClientServiceCreditValue(Ple,clientServiceCreditList);
       Map<String, String> assignClientServiceCredit2=obj.assignClientServiceCreditValueNew (mapAtoZ);  
       String strProvSt = 'BC';     
       
       List<Sales_Assignment__c> findSalesList=[
        Select s.Id,s.User__r.Id, s.User__r.Team_TELUS_ID__c, s.Role__c, s.User__r.firstname,s.User__r.lastname, s.User__r.Email,
        s.User__r.Phone,s.User__r.UserRoleID,s.User__r.UserRole.Name, s.Account__r.rcid__c
        From Sales_Assignment__c s
        where s.Account__r.rcid__c='0001362414'
        limit 1
       ];
       
       Map<String, String> showSalesRole=obj.showSalesRoleUserName(findSalesList, strProvSt);  

    }
    
    public static testMethod void testAccountData(){       
       String cbucid='0002229208';
       String rcid='0001362414';
       String strProv='AB';
       Integer newPageIndex=1;
       PageReference objPage;
       System.currentPagereference().getParameters().put('cbucid','0002229208');
       System.currentPagereference().getParameters().put('cbucidnumber','0002229208');
       System.currentPagereference().getParameters().put('strCbucid','0002229208');
       System.currentPagereference().getParameters().put('rcid','0001362414');
       System.currentPagereference().getParameters().put('rcidnumber','0001362414');       
       System.currentPagereference().getParameters().put('banOrBcan','0001362414');       
       System.currentPagereference().getParameters().put('strProv','AB');   
       System.currentPagereference().getParameters().put('strAlpa','AB');   
       System.currentPagereference().getParameters().put('strIndex','0.0');   
       
       AccountTeamMember atm=new AccountTeamMember(accountId='0014000000QKxNFAA1',Userid='00540000001CHGvAAO',TeamMemberRole='TEST');
       insert atm;
       
       ShowDetailForAssignmentProv obj=new ShowDetailForAssignmentProv();
       obj.strProv='AB';
       
       obj.Rcidnumber='0001362414';
       obj.cbucidnumber='0002229208';
     
       obj.customerName='LES EDITIONS CEC';
       obj.cbucidName='QUEBECOR MEDIA INC';
       obj.banOrBcan='LES EDITIONS CEC INC';
       
       obj.selectedTabForSearch='customerName';
       obj.txtForSearch='zzzzzz';
       obj.searchCustomerName();
       obj.txtForSearch='CITY OF VANCOUVER';
       obj.searchCustomerName();
       
       obj.selectedTabForSearch='cbucidName';
       obj.txtForSearch='zzzzzz';
       obj.searchCbucidName();
       obj.txtForSearch='QUEBECOR MEDIA INC';
       obj.searchCbucidName();
       
       obj.selectedTabForSearch='banOrBcan';
       obj.txtForSearch='zzzzzz';
       obj.searchBanOrBcan();
       obj.txtForSearch='LES EDITIONS CEC INC';
       obj.searchBanOrBcan();
       
       obj.selectedTabForSearch='Cbucidnumber';
       obj.txtForSearch='0002229208';
       obj.searchCbucidnumber();
       
       obj.selectedTabForSearch='rcidnumber';
       obj.txtForSearch='0001362414';
       obj.searchRcidnumber();

      
       objPage=obj.firstBtnClick();
       System.assertEquals(null,objPage);
       objPage=obj.previousBtnClick(); 
       System.assertEquals(null,objPage);
       objPage=obj.nextBtnClick(); 
       System.assertEquals(null,objPage);
       objPage=obj.lastBtnClick(); 
       System.assertEquals(null,objPage); 
       newPageIndex=2;         
       obj.getPageNumber();
       obj.getPageSize();
       obj.getTotalPageNumber();
       obj.getPageAccountList();
       
       obj.getSalesList();
       obj.getShowClientServiceCreditList();
       obj.getCustStatusTool();
       obj.getAccountDetail();
       obj.getMsgForSearchResult();
       obj.getMsgForPosition();

       List<Account> aList=new List<Account>();
                        aList=[Select a.rcid__c,a.CBU_CD__c From Account a
                        where a.id='0014000000bxyeUAAQ' limit 1000];
       for(Account atest:aList){
           atest.CBU_CD__c='A1';
       }
       update aList;
       
       /*List<AccountTeamMember> atmList=new List<AccountTeamMember>();
                        atmList=[Select a.UserId,a.user.name, a.user.firstname, a.user.lastname, a.user.email, a.user.phone,
                        a.TeamMemberRole, a.AccountId From AccountTeamMember a
                        where a.Account.CBUCID_formula__c='0002229208' limit 1000];
       for(AccountTeamMember atest:atmList){
           atest.TeamMemberRole='Test';
       }
       update atmList;
       
       obj.showCustomerDetailWithStatus();
       */
       List<AccountTeamMember> atmList=new List<AccountTeamMember>();
                        atmList=[Select a.UserId,a.user.name, a.user.firstname, a.user.lastname, a.user.email, a.user.phone,
                        a.TeamMemberRole, a.AccountId From AccountTeamMember a
                        where a.Account.rcid__c='234234234' limit 1000];
       for(AccountTeamMember atest:atmList){
           atest.TeamMemberRole='Bus Sales Analyst';
       }
       update atmList;
       
       obj.showCustomerDetailWithStatus();
              
       obj.showSalesDetail(strProv); 

       System.currentPagereference().getParameters().put('strColorStatus','Red');   
       obj.searchCustomerBasedOnStatus();
       System.currentPagereference().getParameters().put('strColorStatus','Yellow');   
       obj.searchCustomerBasedOnStatus();
       System.currentPagereference().getParameters().put('strColorStatus','Green');   
       obj.searchCustomerBasedOnStatus();
       
       objPage=obj.doSearchProv();
       System.assertEquals(new PageReference('/apex/ShowDetailForAssignmentProv').getURL(),objPage.getURL());
       obj.doSearchCbucid();
       obj.showSearchResult();
       obj.searchFromAccountTab(); 
       obj.showAtoZEasyMap();
       obj.showAtoZEasyMapFR();
          
       Map<String, AtoZ_Assignment__c> mapAtoZ=obj.populateNatlAssignments();
       Schema.DescribeFieldResult implMtrxStage = AtoZ_Assignment__c.AtoZ_Field__c.getDescribe();
       List<Schema.PicklistEntry> Ple = implMtrxStage.getPicklistValues();
       
       List<AtoZ_Assignment__c> clientServiceCreditList=[
        Select a.Tel__c, a.Email__c, a.AtoZ_Field__c, a.AtoZ_Contact__r.Id, a.AtoZ_Contact__r.Full_Name__c,
        a.AtoZ_Contact__r.Email__c ,a.AtoZ_Contact__r.Phone__c, a.AtoZ_Contact__r.External_Email__c
        From AtoZ_Assignment__c a
        where a.Prov_Team__c=:strProv
        limit 1
       ];
       
       Map<String, String> assignClientServiceCredit1=obj.assignClientServiceCreditValue(Ple,clientServiceCreditList);
       Map<String, String> assignClientServiceCredit2=obj.assignClientServiceCreditValueNew (mapAtoZ);  
       String strProvSt = 'BC';     
       
       List<Sales_Assignment__c> findSalesList=[
        Select s.Id,s.User__r.Id, s.User__r.Team_TELUS_ID__c, s.Role__c, s.User__r.firstname,s.User__r.lastname, s.User__r.Email,
        s.User__r.Phone,s.User__r.UserRoleID,s.User__r.UserRole.Name, s.Account__r.rcid__c
        From Sales_Assignment__c s
        where s.Account__r.rcid__c='0001362414'
        limit 1
       ];
       
       Map<String, String> showSalesRole=obj.showSalesRoleUserName(findSalesList, strProvSt);
       obj.doSearchcustomerName();  

    }
}