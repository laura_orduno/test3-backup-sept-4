/*****************************************************
    Name: EventHandler_Helper
    Usage: This is helper class to define common methods used in event handler classes
    Author – Sandip Chaudhari (IBM)
    Revision History -
    Created By/Date - Sandip Chaudhari / 04 July 2016
    
    Updated By/Date/Reason for Change - 
    
******************************************************/
global Class EventHandler_Helper {

    /*
    * Name - setEmailAttribute
    * Description - Set Paameter for Singleemailmessage API
    * Param - EventHandlerParameter
    * Return Type - Messaging.Singleemailmessage
    */
    global static Messaging.Singleemailmessage setEmailAttribute(EventHandlerParameter paramObject){
         Messaging.Singleemailmessage email;
         if(paramObject.dataMap.keySet() != null){
            //Create instance and set parameter to send emails 
            email = new Messaging.Singleemailmessage();
            if(paramObject.orgWideEmailAddress != null){
                email.setOrgWideEmailAddressId(paramObject.orgWideEmailAddress);
            }else if(paramObject.fromaddress != null){
                email.setSenderDisplayName(paramObject.fromaddress);
            }
            email.setTargetObjectId(paramObject.targetId);
            email.setSaveAsActivity(true);
            email.setSubject(paramObject.subject);
            if(paramObject.cc!= null && paramObject.cc.size()>0){
                email.setCcAddresses(paramObject.cc);
            }
            if(paramObject.whatId != null){
                email.setWhatId(paramObject.whatId);
            }
            email.setPlainTextBody(paramObject.plainBody);
            email.setHtmlBody(paramObject.htmlBody);
            
            //email.setReplyTo ('test@test.com');
        }
        return email;
    }
    
    /*
    * Name - parseJSON
    * Description - Parse JSON of event details 
    * Param - String 
    * Return Type - Map<String, Object>
    */
    global static Map<String, Object> parseJSON(String eventDetails){
        Map<String, Object> dataMap = (Map<String, Object>)JSON.deserializeUntyped(eventDetails);
        return dataMap;
    }
    
    /*
    * Name - getEmailTemplates
    * Description - get email templates for respective names
    * Param - Set<String>
    * Return Type - Map<String, Object>
    */
    global static Map<String, EmailTemplate> getEmailTemplates(List<External_Event_Notification_Templates__c> templateList){
        Map<String, EmailTemplate> emailTemplates;
        List<EmailTemplate> listEmailTEmplates;
        system.debug('@@@ templateList'+ templateList);
        Set<String> templateNames = new set<String>();
        if(templateList != null && templateList.size()>0){
            for(External_Event_Notification_Templates__c templateObj: templateList){
                templateNames.add(templateObj.Email_Template_Name__c);
            }
            
            if(templateNames != null){
                listEmailTEmplates = [SELECT developername, Id, Subject, HtmlValue, Body 
                                                       FROM EmailTemplate 
                                                       WHERE developername =: templateNames];
            }
            if(listEmailTEmplates != null){
                emailTemplates = new Map<String, EmailTemplate>();
                for(EmailTemplate templateObj: listEmailTEmplates){
                    emailTemplates.put(templateObj.developername, templateObj);
                }
            }
        }        
        return emailTemplates;
    }
    
    global static List<External_Event_Notification_Templates__c> getTemplateConfig(Set<String> lstTemplateNames){
        List<External_Event_Notification_Templates__c> templateList = [Select Name, Email_Template_Name__c, Organization_Wide_Addresses_Detail__c
                                                                       FROM External_Event_Notification_Templates__c
                                                                       WHERE Name LIKE: lstTemplateNames];
        return templateList;
    }
    
    global static Map<String, OrgWideEmailAddress> getOWEmailAddress(List<External_Event_Notification_Templates__c> templateList){
        Map<String, OrgWideEmailAddress> oweMap = new Map<String, OrgWideEmailAddress>();
        List<String> orgEmailIds = new List<String>();
        if(templateList != null){
            for(External_Event_Notification_Templates__c configObj: templateList){
                orgEmailIds.add(configObj.Organization_Wide_Addresses_Detail__c);
            }
            List<OrgWideEmailAddress> oweList = [SELECT ID,IsAllowAllProfiles,DisplayName,Address 
                                       FROM OrgWideEmailAddress 
                                       WHERE Address IN: orgEmailIds];
            if(oweList != null)
            for(OrgWideEmailAddress oweObj: oweList){
                oweMap.put(oweObj.Address, oweObj);
            }
           
        }
        return oweMap;  
    }      
    /*
    * Name - validateAndLogError
    * Description - Check mandatory elements in json
    * Param - String, Map<String, Object>, String, String, List<ExternalEventRequestResponseParameter.EventError>
    * Return Type - void
    */
    global static void validateAndLogError(String nodeName, Map<String, Object> jsonDataMap, String errorCode, String errorDescription, List<ExternalEventRequestResponseParameter.EventError> errorList){
        if(jsonDataMap.get(nodeName) != null){
            String val = (String)jsonDataMap.get(nodeName);
            Boolean isBlankValue = string.isBlank(val)?true:false;
            if(isBlankValue){
                ExternalEventRequestResponseParameter.EventError errorObject = new ExternalEventRequestResponseParameter.EventError();
                errorObject.errorCode = errorCode;
                errorObject.errorDescription = errorDescription + ' ' + nodeName;
                errorList.add(errorObject);
            }
        }   
    }
    
    /*
    * Name - logError
    * Description - Log exception/error in webservice_integration_error_log__c object
    * Param - String, String, String, String, String
    * Return Type - void
    */
    global static void logError(String classAndMethodName, String recId, String errorCodeAndMsg, String stackTrace, String externalSystem){
        webservice_integration_error_log__c errorLog = new webservice_integration_error_log__c
                                                    (apex_class_and_method__c = classAndMethodName,
                                                     external_system_name__c = externalSystem,
                                                     webservice_name__c = 'External Event Management Framework - External system ' + externalSystem +' consumed Salesforce webservice to process events',
                                                     object_name__c = 'External_Event__c',
                                                     sfdc_record_id__c = recId,
                                                     Error_Code_and_Message__c = errorCodeAndMsg,
                                                     Stack_Trace__c = stackTrace
                                                     );
        insert errorLog;
    }
    
    /*
    * Name - getAssetsByserviceId
    * Description - Get assets for respective service Ids
    * Param - List<String> serviceIds
    * Return Type - map<String,Id> 
    */
    global static map<String,Asset> getAssetsByserviceId(List<String> serviceIds){
        map<String,Asset> aseetAccMap = new map<String,Asset>();
        if(serviceIds != null && serviceIds.size()>0){
            List<Asset> accServiceIdList = [SELECT Id, AccountId, Phone__c, Account.CAN__c FROM Asset where Phone__c IN: serviceIds];
            System.debug('@@@AssetList ' + accServiceIdList);
            if(accServiceIdList != null && accServiceIdList.size() > 0){
                for(Asset assetObj: accServiceIdList){
                    if(assetObj.AccountId != null){
                        //serviceAccIds.add(assetObj.Account.Id);
                        aseetAccMap.put(assetObj.Phone__c, assetObj);
                    }
                }
            }
        }
        return aseetAccMap;
    }
    
    /*
    * Name - getBillingAccounts
    * Description - Get account details
    * Param - List<String> banCanList, List<ID> accIds
    * Return Type - List<Account>
    */
    global static List<Account> getBillingAccounts(List<String> banCanList, List<ID> accIds){
        List<Account> accList = [SELECT id, Name, OwnerId, BAN_CAN__c, Owner.Name, CAN__c FROM Account  
                                        WHERE ID IN: accIds OR BAN_CAN__C IN: banCanList];
        return accList;
        
    }
    
    /*
    * Name - getAssociatedContactsForAccount
    * Description - Get list of contacts for respective accounts
    * Param - set<Id> accIds
    * Return Type - map<Id, List<contact>>
    */
    global static map<Id, List<contact>> getAssociatedContactsForAccount(set<Id> accIds){
        map<Id, List<contact>> accContactMap = new map<Id, List<contact>>();
        List<Contact> lstContact = [Select Id,Email, accountId, Name, Language_Preference__c from Contact 
                                WHERE accountId IN: accIds 
                                AND Active__c = true 
                                AND HSIA_Data_Usage_Email_Notification__c = true];
                                
        system.debug('@@@lstContact ' + lstContact);
        If(lstContact != null && lstContact.size()>0){
            for(Contact conObj: lstContact){
                List<Contact> lstTmpContact;
                if(accContactMap.get(conObj.accountId) != null){
                    lstTmpContact = accContactMap.get(conObj.accountId);
                }else{
                    lstTmpContact = new List<Contact>();
                }
                lstTmpContact.add(conObj);
                accContactMap.put(conObj.accountId, lstTmpContact);
            }
        }
        return accContactMap;
    }
    
    /*
    * Name - getServiceLocation
    * Description - Get latest service address for respective account
    * Param - set<Id> accIds
    * Return Type - Map<Id, String>
    */
    /*global static Map<Id, String> getServiceLocation(Set<ID> accIds){
        map<Id,String> addrerssMap = new map<Id,String>();
        List<SMBCare_Address__c> addressList = [SELECT id, Service_Location__c FROM SMBCare_Address__c  
                                        WHERE Account__c IN: accIds];
         If(addressList != null && addressList.size()>0){
            for(SMBCare_Address__c addrObj: addressList){
                addrerssMap.put(addrObj.Id, addrObj.Service_Location__c);
            }
         }
        return addrerssMap;
    }*/
}