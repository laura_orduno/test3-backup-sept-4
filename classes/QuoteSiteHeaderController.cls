public without sharing class QuoteSiteHeaderController {
    
    public SBQQ__Quote__c quote {get; private set;}
    public Boolean quoteLoaded {get; set;}
    public Boolean isDealer {get { return QuotePortalUtils.isUserDealer(); }}
    
    public QuoteSiteHeaderController() {
        quoteLoaded = false;
        Id quoteId = ApexPages.currentPage().getParameters().get('qid');
        if (null != QuoteId) {
            init(quoteId);
        }
    }
    private String[] greetings = new List<String>{'Hello, %u', 'Welcome back, %u'};
    
    public String UserGreeting {get {
        
        Datetime dt = Datetime.now();
        Integer hour = dt.hour();
        if (hour < 5) {
            greetings.add('Up late, %u?');
        } if (hour < 7) {
            greetings.add('Starting early, %u?');
        } else if (hour < 12) {
            greetings.add('Good morning, %u');
        } else if (hour < 18) {
            greetings.add('Good afternoon, %u');
        } else if (hour < 23) {
            greetings.add('Good evening, %u');
        } else if (hour >= 23) {
            greetings.add('Up late, %u?');
        }
        
        User u = [Select Name From User Where Id = :UserInfo.getUserId()];
        if (u != null) {
            Integer rI = 0;
            if (greetings.size() != 1) {
                rI = Integer.valueof(Math.ceil(Math.random() * (greetings.size())) - 1);
            }
            return greetings[rI].replace('%u', '<strong>' + u.Name + '</strong>');
        } else {
            return 'Hello, Guest User';
        }
    }
    }
    
    /**
     * Constructor used by component that generates fulfillment emails
     */
    public QuoteSiteHeaderController(Id quoteId) {
        
        quoteLoaded = false;
        if (null != QuoteId || '' != QuoteId) {
            init(quoteId);
        }
    }
    
    private void init(Id quoteId) {
        quote = QuotePortalUtils.loadQuoteById(quoteId);
        if (null != quote) {
            quoteLoaded = true;
        }
    }
    
    public String getQuoteName() {
        if (null != quote && quote.number_of_quote_lines__c > 0) {
            return quote.Name;
        } return null;
    }
    
}