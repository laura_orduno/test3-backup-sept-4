@isTest
public class OC_ContactController_Test {
	public TestMethod static void TestCreateNewContact() {
        TestDataHelper.testAccountCreation();
    	
        String methodName = 'createnewContact';
        Map<String,Object> inputMap = new Map<String,Object>();
        Map<String,Object> outMap =  new Map<String,Object>();
        Map<String,Object> options = new Map<String,Object>();
        Map<String,Object> NewContactNode = new Map<String,Object>();
        NewContactNode.put('FirstName','test');
        List<String>langPref = new List<String>();
        langPref.add('English');
        NewContactNode.put('LanguagePref',langPref);
        NewContactNode.put('LastName','test');
        NewContactNode.put('Phone','9999999999');
        NewContactNode.put('Email','test@test.com');
        NewContactNode.put('Title','Mr');
        NewContactNode.put('AccountId',TestDataHelper.testAccountObj.id);
        
        inputMap.put('createnewContact',NewContactNode);
        OC_ContactController obj = new OC_ContactController();
        Test.startTest();
        obj.invokeMethod(methodName,inputMap,outMap,options);
        Test.stopTest();
	}
}