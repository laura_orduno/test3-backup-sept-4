/**
 *  VITILcareNewCaseCtlr.cls
 *
 *  @description
 *      New Case Controller for the VITILcare Site.  Copied, not created by dan
 *
 *  @date - 03/01/2014
 *  @author - Dan Reich: Traction on Demand
 */
public with sharing class VITILcareNewCaseCtlr extends VisualforcePageController{
    public class NewCaseException extends Exception {}

    private static final Map<String, String> requestTypeRecordTypeMap;
    private static final Map<String, String> recordTypeRequestTypeMap;
    private static final Map<String, Id> caseRTNameToId;
    private static final Map<String, List<String>> typeFieldsOrder;
    private static final Map<String, List<ParentToChild__c>> ptcListByCategoryMap;
    //public string userLanguage {get; set;}
    private static final Map<String, List<String>> categoryRequestTypesMap;
    private static final Map<String, String> requestTypesCategoryMap;
    public string LynxTickNum {get;set;}
    public string  NewSubject {get; set;}
    

    public static Map<String, String> reqTypeFormIdMap {get;set;}
    public String reqTypeFormIdMapJSON {get{return JSON.serialize(reqTypeFormIdMap);}}
    public string userLanguage1 {get; set;}
    public Boolean createNewOnSubmit {get {
        if (createNewOnSubmit == null) return false;
        return createNewOnSubmit;
    } set;}
    
    static {
        List<TypeToFields__c> ttfs = TypeToFields__c.getAll().values();
        typeFieldsOrder = new Map<String, List<String>>();
       
        List<ExternalToInternal__c> etis = ExternalToInternal__c.getAll().values();
        requestTypeRecordTypeMap = new Map<String, String>();
        recordTypeRequestTypeMap = new Map<String, String>();
        for (ExternalToInternal__c eti : etis) {
             System.debug('eti.Identifier__c: ' + eti.Identifier__c);
            if(eti.Identifier__c!=null && eti.Identifier__c.equals('onlineRequestTypeToCaseRT')) {
                requestTypeRecordTypeMap.put(eti.External__c,eti.Internal__c);
                
                recordTypeRequestTypeMap.put(eti.Internal__c,eti.External__c);
                
            }
            
        }
        System.debug('requestTypeRecordTypeMap: ' + requestTypeRecordTypeMap);
            System.debug('recordTypeRequestTypeMap: ' + recordTypeRequestTypeMap);
        
        List<ParentToChild__c> ptcs = ParentToChild__c.getAll().values();
        ptcListByCategoryMap = new Map<String, List<ParentToChild__c>>();
        categoryRequestTypesMap = new Map<String, List<String>>();
        requestTypesCategoryMap = new Map<String, String>();
        reqTypeFormIdMap = new Map<String, String>();
        reqTypeFormIdMap.put(label.mbrSelectType, '');
        for (ParentToChild__c ptc : ptcs) {
            if(ptc.Identifier__c.equals('CCICatRequest')) {
                // build ptc list map so it can be used for sorting the request type(child) entries                
                if(!ptcListByCategoryMap.containsKey(ptc.parent__c)){
                    ptcListByCategoryMap.put(ptc.parent__c, new List<ParentToChild__c>());
                }    
                List<ParentToChild__c> ptcListForCategory = ptcListByCategoryMap.get(ptc.parent__c);    
                ptcListForCategory.add(ptc);
                ptcListByCategoryMap.put(ptc.parent__c, ptcListForCategory);
                ////////////////////////////////////////////////////////////////////////////////////
                requestTypesCategoryMap.put(ptc.Child__c, ptc.Parent__c);
                if(ptc.FormComponent__c==null || ptc.FormComponent__c.equals('')){
                    reqTypeFormIdMap.put(ptc.Child__c, 'descriptionField');
                }
                else{
                    reqTypeFormIdMap.put(ptc.Child__c, ptc.FormComponent__c);
                }
            }
        }
        
        // perform ptc list sorting for each category
        Map<Integer, ParentToChild__c> ptcOrderedMap = new Map<Integer, ParentToChild__c>();
        List<ParentToChild__c> ptcOrphanedList = new List<ParentToChild__c>();
        
        for(String category : ptcListByCategoryMap.keyset()) {    
            Integer minOrderNumber = null;
            Integer maxOrderNumber = null;
            
            ptcOrderedMap.clear();
            ptcOrphanedList.clear();
            List<ParentToChild__c> ptcSortedList = new List<ParentToChild__c>();
            
            for(ParentToChild__c ptc : ptcListByCategoryMap.get(category)) {
                Integer orderNumber = Integer.valueOf(ptc.order_number__c);        
                // init
                minOrderNumber = minOrderNumber == null ? orderNumber : minOrderNumber;
                maxOrderNumber = maxOrderNumber == null ? orderNumber : maxOrderNumber;                
                if(orderNumber == null || ptcOrderedMap.containsKey(orderNumber)){
                    ptcOrphanedList.add(ptc);
                }
                else{
                    // update
                    minOrderNumber = orderNumber < minOrderNumber ? orderNumber : minOrderNumber;
                    maxOrderNumber = orderNumber > maxOrderNumber ? orderNumber : maxOrderNumber;
                    ptcOrderedMap.put(orderNumber, ptc);
                }        
            }
        
            for(Integer key=minOrderNumber;key<=maxOrderNumber;key++){
                if(ptcOrderedMap.containsKey(key)){
                    ParentToChild__c ptc = ptcOrderedMap.get(key);
                    ptcSortedList.add(ptc);
                }
            }
                
            ptcSortedList.addAll(ptcOrphanedList);     

            List<String> ptcSortedStringList = new List<String>();           
            for(ParentToChild__c ptc : ptcSortedList){
                ptcSortedStringList.add(ptc.child__c);
            }            
            categoryRequestTypesMap.put(category, ptcSortedStringList);            
        }
        /////////////////////////////////////////////////////////////////////////////////////
        
        
        for(String s:categoryRequestTypesMap.keySet()){
            system.debug('categoryRequestTypesMap: '+s+' values: '+categoryRequestTypesMap.get(s));
        }
        
        Schema.DescribeSObjectResult caseToken = Schema.SObjectType.Case;
        Map<String, RecordTypeInfo> nameToRTInfo = caseToken.getRecordTypeInfosByName();
        caseRTNameToId = new Map<String, Id>();
        for (String s : nameToRTInfo.keyset()) {
            system.debug('s Value:'+ s);
            if(s == label.VITILcareRequestType){
            caseRTNameToId.put(s, nameToRTInfo.get(s).getRecordTypeId());
            }
        }
         system.debug('caseRTNameToId: '+caseRTNameToId);
    }

    /* Input Variables */
    public String selectedCategory {get; set;} { selectedCategory = Label.MBRSelectCategory; }
     
    public Case caseToInsert {get; set;}
    public static String parentCaseType;
    public static String parentCategory;
    public VITILcareFormWrapper formWrapper {get; set;}
    public Account account {get; private set;}
    public Boolean caseSubmitted {get; set;}
   // public string requestType {get; set;}
    public String requestType {get; 
        set{
            requestType = value;
             System.debug('requestTypeRecordTypeMap.get(requestType): ' + requestTypeRecordTypeMap.get(requestType));
             System.debug('caseRTNameToId.get(requestTypeRecordTypeMap.get(requestType)): ' + caseRTNameToId.get(requestTypeRecordTypeMap.get(requestType)));
            if(requestTypeRecordTypeMap.get(requestType) != null && caseRTNameToId.get(requestTypeRecordTypeMap.get(requestType)) != null) {
                system.debug('CaseToInsert:'+caseRTNameToId.get(requestTypeRecordTypeMap.get(requestType)));
                //caseToInsert.recordtypeid = caseRTNameToId.get(requestTypeRecordTypeMap.get(requestType));
            }    
        } 
    } { requestType = label.VITILcareRequestType; }
    
   
    
    public Attachment attachment {
        get {
            if (attachment == null)
                attachment = new Attachment();
            return attachment;
        }
        set;
    }
    
    public VITILcareNewCaseCtlr() {
        priorityError = '';
        typeError = '';
        formWrapper = new VITILcareFormWrapper();
     
//        Customer_Interface_Settings__c cis = Customer_Interface_Settings__c.getInstance();
        VITILCare_Customer_Interface_Settings__c cis = VITILCare_Customer_Interface_Settings__c.getInstance();
        if(cis.Case_Origin_Value__c != null) {
System.debug('CASE ORIGIN SETTING: ' + cis.Case_Origin_Value__c);            
            caseToInsert = new case(Origin = cis.Case_Origin_Value__c, NotifyCustomer__c=true);
        }
        else {
System.debug('CASE ORIGIN: VITILcare');            
            caseToInsert = new case(Origin = 'VITILcare', NotifyCustomer__c=true);
        }
        caseSubmitted = false;

        if (ApexPages.currentPage() != null) {
            String parentCaseNumber = ApexPages.currentPage().getParameters().get('reopen');
            if (parentCaseNumber != null) {
              
                List<Case> parentCaseList = [SELECT Id, Subject, Description, My_Business_Requests_Type__c, LastModifiedDate, NotifyCollaboratorString__c, CreatedDate, CaseNumber, recordType.name, Type, Status, Lynx_Ticket_Number__c,Project_number__c, Contact_Phone_Number2__c, Customer_First_Name_contact__c, Customer_Last_Name_contact__c, Customer_Phone_contact__c, ENTP_AccessDay__c, ENTP_Condition__c FROM Case WHERE CaseNumber = :parentCaseNumber AND Status = 'Closed'];
                if(parentCaseList.size() > 0) {
                    caseToInsert.Project_number__c = parentCaseList[0].Project_number__c;
                    caseToInsert.ENTP_Condition__c = parentCaseList[0].ENTP_Condition__c;
                    caseToInsert.ENTP_AccessDay__c = parentCaseList[0].ENTP_AccessDay__c;
                    caseToInsert.Customer_Phone_contact__c = parentCaseList[0].Customer_Phone_contact__c;
                    caseToInsert.Customer_First_Name_contact__c = parentCaseList[0].Customer_First_Name_contact__c ;

                    caseToInsert.Parent = parentCaseList[0];
                    LynxTickNum = parentCaseList[0].Lynx_Ticket_Number__c;
                    System.debug('LynxTicketNumber: ' + LynxTickNum);
                    caseToInsert.ParentId = parentCaseList[0].Id;
                    NewSubject = Label.VITILcareRequestSubjectreopen +  LynxTickNum + '] ' + caseToInsert.Parent.Subject;
                    caseToInsert.Subject = Label.VITILcareRequestSubjectreopen +  LynxTickNum + '] ' + caseToInsert.Parent.Subject;
                    List<String> lines = new List<String>();
                    if (caseToInsert.Parent.Description != null) {
                        lines = caseToInsert.Parent.Description.split('\n');
                    }
                    String description = '';
                    for (String s : lines) {
                        description += '\n> ' + s;
                    }
                    caseToInsert.Description = '\n\n\n\n' + Label.VITILcareSubmitRequestReopenDescp + LynxTickNum + '\n>' + description;
                    caseToInsert.NotifyCollaboratorString__c = caseToInsert.Parent.NotifyCollaboratorString__c;
                    parentCaseType = caseToInsert.Parent.My_Business_Requests_Type__c;
                    System.debug('requestTypesCategoryMap: ' + requestTypesCategoryMap);
                    if (parentCaseType != null && requestTypesCategoryMap.get(parentCaseType) != null) {
                        parentCategory = requestTypesCategoryMap.get(parentCaseType);
                        selectedCategory = parentCategory;
                        requestType = parentCaseType;
                         System.debug('requestType: ' + requestType);
                    }
                } else {
                    // invalid parent caseNumber specified...
                }
            }
        }
    }
    
    public transient String pageValidationError {get; set;}
    public transient String priorityError {get; set;}
    public transient String typeError {get; set;}

    //public PageReference populateForm(){
    //    List<ParentToChild__c> parentToChild= [SELECT FormComponent__c from ParentToChild__c WHERE Child__c = :requestType LIMIT 1];
    //    System.debug(requestType + ' 11111111111111 '+parentToChild);
    //    if(!parentToChild.isEmpty()){
    //        formId = parentToChild[0].FormComponent__c;
    //    }
    //    return null;
    //}
    
    public PageReference initCheck() {
       userLanguage1 = [SELECT LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()].LanguageLocaleKey; 
          
 system.debug('UserLanguage:' + userLanguage1);  
        //String userAccountId;
        //userAccountId = (String)UserUtil.CurrentUser.AccountId;
        String userAccountId;
        List<User> users = [SELECT Id, AccountId, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()];
        if (users.size() > 0) {
            userAccountId = users[0].AccountId;
           // userLanguage1 = users[0].LanguageLocaleKey;
           
        }
        Id aid = (Id) userAccountId;
        if (caseToInsert.Parent == null) {
            resetValues();
        }
        caseToInsert.AccountId = aid;
        return null;
    }

    public void resetValues(){
        caseToInsert.description = '';
        caseToInsert.subject = '';
       requestType = label.VITILcareRequestType;//Label.MBRSelectType;
          System.debug('requestType: ' + requestType);
        String categoryType = ApexPages.currentPage().getParameters().get('category'); 
        if(caseSubmitted){
            selectedCategory = Label.MBRSelectCategory;
        }
        else if(categoryType!=null){
            ParentToChild__c categType = ParentToChild__c.getAll().get(categoryType);
            selectedCategory = categType.Parent__c;
            categoryType = null;
        }
    }

    public String getRecordType(String externalName){        
         System.debug('externalName: ' + externalName);
              
        List<ExternalToInternal__c> etis = ExternalToInternal__c.getAll().values();
        String typeName = '';
        for (ExternalToInternal__c eti : etis) {
            if(eti.Identifier__c.equals('onlineRequestTypeToCaseRT') && eti.External__c.equals(externalName)) {
               
                typeName = eti.Internal__c;
                    System.debug('typeName: ' + typeName); 

            }
        }

        return typeName;
    }

    
    public PageReference createNewCase() {
        PageReference submitNewPage = Page.VITILcareNewCase;
        submitNewPage.setRedirect(true);
        submitNewPage.getParameters().put('previousCaseSubmitted', '1');

        if(caseToInsert==null) return null;



        //String recType = getRecordType(requestType);
        String recType = label.VITILcareRequestType;
         System.debug('recType: ' + recType);
System.debug('Schema.SObjectType.Case.RecordTypeInfosByName: ' + Schema.SObjectType.Case.RecordTypeInfosByName);        
        
       // System.debug('caseRTNameToId.get(requestTypeRecordTypeMap.get(requestType)): ' + caseRTNameToId.get(requestTypeRecordTypeMap.get(recType))); 
       // Id recTypeId = caseRTNameToId.get(requestTypeRecordTypeMap.get(recType));
     Id recTypeId = Schema.SObjectType.Case.RecordTypeInfosByName.get(recType).RecordTypeId;
System.debug('recTypeId: ' + recTypeId); 
        
        
        if(recTypeId != null){
            caseToInsert.recordtypeid = recTypeId;
        }
        if(recTypeId == null){
                     System.debug('VITILcareNewCase::createNewCase, error: ' + pageValidationError);        
System.debug('VITILcareNewCase::return null 324'); 
            pageValidationError = label.mbrMissingRecordType;
     
             return null;
        }
           
       /* if(caseToInsert.recordtypeid == null) {
            pageValidationError = label.mbrMissingRecordType;
            return null;
        } */
        
        caseToInsert.My_Business_Requests_Type__c = label.VITILcareRequestType;
    
       
       VITILcareFormWrapper.GetTicketInfoAct getTicketInfo = formWrapper.getTicketInfo;
        System.debug('Subject: ' +  NewSubject);
    System.debug('Subject: ' +  getTicketInfo.subject);
       
        //append request type in front of subject
        /*if(caseToInsert.subject==null || caseToInsert.subject.equals('')){
           */
           if(String.isNotBlank(NewSubject))
           {
             caseToInsert.Subject = NewSubject;
           } else{
             caseToInsert.Subject = getTicketInfo.subject;
           }
           system.debug('CaseToInsert.Subject: '+caseToInsert.Subject);
            /*caseToInsert.Loaner_Required__c = 'No';*/
      /*  }*/
         if(caseToInsert.Customer_First_Name_contact__c == null || caseToInsert.Customer_First_Name_contact__c.equals(''))
        {
            caseToInsert.Customer_First_Name_contact__c = getTicketInfo.ReportedBy;
             System.debug('CaseToInsert.Customer_First_Name_contact__c: ' + caseToInsert.Customer_First_Name_contact__c);
        }
    if(caseToInsert.Customer_Phone_contact__c  == null || caseToInsert.Customer_Phone_contact__c .equals(''))
        {
            caseToInsert.Customer_Phone_contact__c  = getTicketInfo.ReportedByPhNum;
             System.debug('CaseToInsert.Customer_Phone_contact__c : ' + caseToInsert.Customer_Phone_contact__c );
        }
       if(caseToInsert.ENTP_Condition__c  == null || caseToInsert.ENTP_Condition__c.equals(''))
        {
            caseToInsert.ENTP_Condition__c  = getTicketInfo.ConditionTypes;
             System.debug('CaseToInsert.ENTP_Condition__c : ' + caseToInsert.ENTP_Condition__c );
        }
         if(caseToInsert.ENTP_AccessDay__c  == null || caseToInsert.ENTP_AccessDay__c.equals(''))
        {
            caseToInsert.ENTP_AccessDay__c  = getTicketInfo.AccessHours;
             System.debug('CaseToInsert.ENTP_AccessDay__c : ' + caseToInsert.ENTP_AccessDay__c );
        }
        if(caseToInsert.Project_number__c == null || caseToInsert.Project_number__c.equals('')) {
            caseToInsert.Project_number__c = getTicketInfo.projectNum;
        } 
                // Added for running Case Assignment Rules -- Dan
        Database.DMLOptions dml = new Database.DMLOptions();
        dml.AssignmentRuleHeader.useDefaultRule = true;
       if(caseToInsert.description==null || caseToInsert.description.equals('')){
         caseToInsert.description = '';
           caseToInsert.description += '\n' + populateDescription(requestType);
       system.debug('Description: '+ caseToInsert.description);
       }
        caseToInsert.Ticketing_System__c = 'LYNX';
            
        Database.Saveresult sr = Database.insert(caseToInsert, dml);
        
        if (!sr.isSuccess()) {
            for (Database.Error error : sr.getErrors()) {
                system.debug('line374 errorSRError:='+error.getMessage());
                pageValidationError = error.getMessage();
            }
        }

        if ( caseToInsert.Id == null){ 
            return null; 
        } else {
            if(attachment.body != null && attachment.body.size() > 0) {
                attachment.OwnerId = UserInfo.getUserId();
                attachment.ParentId = caseToInsert.id;
                attachment.IsPrivate = false;
                caseToInsert.description += '\n\n' + Label.MBR_See_Attachment + attachment.name;
                try {
                  insert attachment;
                  update caseToInsert;
                } catch (DMLException e) {
                  ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
                  return null;
                } finally {
                  attachment = new Attachment(); 
                }
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
            }

            /*****
            // send notification to collaborators
            System.debug('Collab caseToInsert.NotifyCollaboratorString__c: ' + caseToInsert.NotifyCollaboratorString__c);
            if (caseToInsert.NotifyCollaboratorString__c != null && caseToInsert.NotifyCollaboratorString__c.length() > 0) {
                List<String> collabEmails = caseToInsert.NotifyCollaboratorString__c.split(';', 0);
                if (collabEmails.size() > 0) {
                    MBR_Case_Collaborator_Settings__c collabSettings = MBR_Case_Collaborator_Settings__c.getOrgDefaults();
                    System.debug('Collab collabSettings: ' + collabSettings);
                    //String templateName = collabSettings.New_Case_Template_Name__c;
                    //List<EmailTemplate> templates = [select id from EmailTemplate where DeveloperName = :templateName LIMIT 1];
                    //System.debug('Collab templates: ' + templates);
                    Id emailTemplateId = collabSettings.New_Case_Template_Id__c;
                    if (emailTemplateId != null) {
                        //Id emailTemplateId = templates[0].Id;
                        //Id emailTemplateId = '00X4000000163uYEAQ';
                        Id fromEmailId = null;
                        List<OrgWideEmailAddress> fromEmails = [select Id from OrgWideEmailAddress where DisplayName = 'My Business Requests'];
                        if (fromEmails.size() > 0) {
                            fromEmailId = fromEmails[0].Id;
                        }
                        try {
                            List<Messaging.SendEmailResult> mailResults = VITILcareUtils.sendTemplatedCaseEmails(collabEmails, fromEmailId, emailTemplateId, caseToInsert.Id);
                        } catch (Exception e) {
                            System.debug('Collab caught MBRUtls.sendTemplatedCaseEmails() exception: ' + e);
                        }
                    }
                }
            }
*/            
             System.debug('Line 422 select: ' + caseToInsert.id);
       /*    case s = [Select casenumber, Description, Project_number__c, subject, Contact_phone_number__c from Case where id = :caseToInsert.id LIMIT 1];
        //Rob - 01/19/2016 : Make the call to VITILcare_LynxCreate to send info to Lynx Web Service
        VITILcare_LynxCreate.sendRequest smbCallOut = new VITILcare_LynxCreate.sendRequest();
        smbCallOut.SRequest(s.subject, s.description, s.Project_number__c, s.casenumber, UserInfo.getFirstName(),UserInfo.getLastName(), s.Contact_phone_number__c, userInfo.getUserEmail());
        //VITILcare_TTODS_CreateHelper smbCallOut = new VITILcare_TTODS_CreateHelper();
        //smbCallOut.AddTickets('This is a test','Another Test','brekke','5555555555');
        System.debug('CALLOUT LINE 424: ' + smbCallOut);            
            return null;*/
            
            PageReference page = System.Page.VITILcareCaseDetail;
            Case c = [Select casenumber, Description from Case where id = :caseToInsert.id LIMIT 1];
                     
            page.getParameters().put('caseNumber', c.casenumber);
            page.getParameters().put('TicketType', 'New');
            page.setRedirect(true);
            caseSubmitted = true;
            caseToInsert = new case();
            selectedCategory = label.mbrSelectCategory;
 
        
            return createNewOnSubmit ? submitNewPage:page;
            
       
            
        }    
    }
    


    public String populateDescription(String reqType){
       String userLanguage;
        if(userLanguage == null) {
        userLanguage = [SELECT LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()].LanguageLocaleKey;
    }
        Integer index = 1;
        String description = '';
        String formId = reqTypeFormIdMap.get(reqType);
         VITILcareFormWrapper.GetTicketInfoAct getTicketInfo = formWrapper.getTicketInfo;

         if(true){
        if(userLanguage == 'en_US'){
             if(String.isNotBlank(getTicketInfo.subject)){
                 description = '\n\nSubject: ' + getTicketInfo.subject;
             }  
             if(String.isNotBlank(getTicketInfo.projectNum)){
                 description = '\n\nProject number: ' + getTicketInfo.projectNum;
             }             
             if(String.isNotBlank(getTicketInfo.phoneNumber)){
                 description += '\n Phone numbers/devices affected: ' + getTicketInfo.phoneNumber;
             } 
               
             if(String.isNotBlank(getTicketInfo.siteAddress)){
                 description += '\n Site address: ' + getTicketInfo.siteAddress;
             }
             if(String.isNotBlank(getTicketInfo.additionalNote)){
                 description += '\n Comments: ' + getTicketInfo.additionalNote;
             }
             if(String.isNotBlank(getTicketInfo.ReportedBy)){
                 description += '\n Reported by name: ' + getTicketInfo.ReportedBy;
             }
             if(String.isNotBlank(getTicketInfo.ReportedByCompany)){
                 description += '\n Reported by company: ' + getTicketInfo.ReportedByCompany;
             }
             if(String.isNotBlank(getTicketInfo.ReportedByPhNum)){
                 description += '\n Reported by phone number: ' + getTicketInfo.ReportedByPhNum;
             }
             if(String.isNotBlank(getTicketInfo.ReportedByEm)){
                 description += '\n Reported by email address: ' + getTicketInfo.ReportedByEm;
             }
             if(String.isNotBlank(getTicketInfo.OnsiteContactName)){
                 description += '\n Onsite contact name: ' + getTicketInfo.OnsiteContactName;
             }
              if(String.isNotBlank(getTicketInfo.OnsiteContactNumber)){
                 description += '\n Onsite contact phone number: ' + getTicketInfo.OnsiteContactNumber;
             }
             if(String.isNotBlank(getTicketInfo.SiteCompanyName)){
                 description += '\n Site company name: ' + getTicketInfo.SiteCompanyName;
             }
             if(String.isNotBlank(getTicketInfo.AccessHours)){
                 description += '\n Access hours/days: ' + getTicketInfo.AccessHours;
             }
             /* if(String.isNotBlank(getTicketInfo.CustomerTicketNum)){
                 description += '\n Internal Ticket Number: ' + getTicketInfo.CustomerTicketNum;
            } */
                 description += '\n Customer ticket number: ' + getTicketInfo.CustomerTicketNum;
             if(String.isNotBlank(getTicketInfo.OngoingCalls)){
            
                 description += '\n Customer is able to make outgoing connections?: ' + getTicketInfo.OngoingCalls;
             } 
             /*if(String.isNotBlank(getTicketInfo.SetModelColor)){
                 description += '\n Set Model/Color: ' + getTicketInfo.SetModelColor;
             }
             if(String.isNotBlank(getTicketInfo.SetLocation)){
                 description += '\n Set Location: ' + getTicketInfo.SetLocation;
             }
              description += '\n Set model/color: ' + getTicketInfo.SetModelColor;
            description += '\n Set location: ' + getTicketInfo.SetLocation;*/
             
             if(String.isNotBlank(getTicketInfo.AllSetsAffected)){
                 description += '\n All devices are affected?: ' + getTicketInfo.AllSetsAffected;
             }
             if(String.isNotBlank(getTicketInfo.PowerIssue)){
                 description += '\n Recent/ongoing power issues?: ' + getTicketInfo.PowerIssue;
             }
              if(String.isNotBlank(getTicketInfo.ConditionTypes)){
                     description += '\n Condition?: ' + getTicketInfo.ConditionTypes;
                 }
             
             index++;
         }
             if(userLanguage == 'fr'){
             if(String.isNotBlank(getTicketInfo.subject)){
                 description = '\n\nSubject: ' + getTicketInfo.subject;
             }  
             if(String.isNotBlank(getTicketInfo.projectNum)){
                 description = '\n\nNuméro de projet: ' + getTicketInfo.projectNum;
             }             
             if(String.isNotBlank(getTicketInfo.phoneNumber)){
                 description += '\n Numéros de téléphone/appareils touchés: ' + getTicketInfo.phoneNumber;
             } 
               
             if(String.isNotBlank(getTicketInfo.siteAddress)){
                 description += '\n Adresse du site: ' + getTicketInfo.siteAddress;
             }
             if(String.isNotBlank(getTicketInfo.additionalNote)){
                 description += '\n Commentaires: ' + getTicketInfo.additionalNote;
             }
             if(String.isNotBlank(getTicketInfo.ReportedBy)){
                 description += '\n Signalé par nom: ' + getTicketInfo.ReportedBy;
             }
             if(String.isNotBlank(getTicketInfo.ReportedByCompany)){
                 description += '\n Signalé par l\'entreprise: ' + getTicketInfo.ReportedByCompany;
             }
             if(String.isNotBlank(getTicketInfo.ReportedByPhNum)){
                 description += '\n Signalé par téléphone: ' + getTicketInfo.ReportedByPhNum;
             }
             if(String.isNotBlank(getTicketInfo.ReportedByEm)){
                 description += '\n Signalé par adresse de courriel: ' + getTicketInfo.ReportedByEm;
             }
             if(String.isNotBlank(getTicketInfo.OnsiteContactName)){
                 description += '\n Personne responsable sur place nom: ' + getTicketInfo.OnsiteContactName;
             }
              if(String.isNotBlank(getTicketInfo.OnsiteContactNumber)){
                 description += '\n Personne responsable sur place téléphone: ' + getTicketInfo.OnsiteContactNumber;
             }
             if(String.isNotBlank(getTicketInfo.SiteCompanyName)){
                 description += '\n Nom de l\'entreprise sur le site: ' + getTicketInfo.SiteCompanyName;
             }
             if(String.isNotBlank(getTicketInfo.AccessHours)){
                 description += '\n Heures et jours d\'accès: ' + getTicketInfo.AccessHours;
             }
             /* if(String.isNotBlank(getTicketInfo.CustomerTicketNum)){
                 description += '\n Internal Ticket Number: ' + getTicketInfo.CustomerTicketNum;
            } */
                 description += '\n Numéro du billet du client: ' + getTicketInfo.CustomerTicketNum;
             if(String.isNotBlank(getTicketInfo.OngoingCalls)){
            
                 description += '\n Le client est en mesure d\'établir des connexions sortantes?: ' + getTicketInfo.OngoingCalls;
             } 
             /*if(String.isNotBlank(getTicketInfo.SetModelColor)){
                 description += '\n Set Model/Color: ' + getTicketInfo.SetModelColor;
             }
             if(String.isNotBlank(getTicketInfo.SetLocation)){
                 description += '\n Set Location: ' + getTicketInfo.SetLocation;
             }
              description += '\n Set model/color: ' + getTicketInfo.SetModelColor;
            description += '\n Set location: ' + getTicketInfo.SetLocation;*/
             
             if(String.isNotBlank(getTicketInfo.AllSetsAffected)){
                 description += '\n Est-ce que tous les appareils sont touchés?: ' + getTicketInfo.AllSetsAffected;
             }
             if(String.isNotBlank(getTicketInfo.PowerIssue)){
                 description += '\n Avez-vous des problèmes d\'alimentation récurrents ou en avez-vous eu récemment?: ' + getTicketInfo.PowerIssue;
             }
              if(String.isNotBlank(getTicketInfo.ConditionTypes)){
                     description += '\n Condition?: ' + getTicketInfo.ConditionTypes;
                 }
             index++;
                  
         }
            
         }
        
       
       return description;
    }

   public List<SelectOption> getPaymentOptions(){
         List<SelectOption> paymentTypes = new List<SelectOption>();
         paymentTypes.add(new SelectOption('charge to airtime account', 'charge to airtime account'));
         paymentTypes.add(new SelectOption('existing hardware account', 'existing hardware account'));
         paymentTypes.add(new SelectOption('other', 'other'));
        return paymentTypes;
    }

    public void clear() {
        caseToInsert = null;
    }
        
    public List<SelectOption> getRequestTypes() {
        Map<String, List<String>> rts = categoryRequestTypesMap;
        List<SelectOption> options = new List<SelectOption>();
        List<String> temps = categoryRequestTypesMap.get(selectedCategory);
        if(temps == null) {
            options.add(new SelectOption(label.mbrSelectCategoryFirst,label.mbrSelectCategoryFirst));
            return options;
        }
//        temps.sort();
        Set<String> tempsSet = new Set<String>(temps);
        if(!tempsSet.contains(requestType)) {
            requestType = label.mbrSelectType;
        }
        // -- Added by DR, Traction 09-10-2014 --/
        options.add( new SelectOption(label.mbrSelectType, label.mbrSelectType) );
        // -- End Added by DR, Traction 09-10-2014 --/
        for(String temp : temps) {
            options.add(new SelectOption(temp,temp));
        }
        return options;
    }
    
    public List<SelectOption> getCategories() {
        Map<String, List<String>> rts = categoryRequestTypesMap;
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption(label.mbrSelectCategory, label.mbrSelectCategory));
        for(String temp : rts.keySet()){
            if (!temp.equals('Tech support') || !VITILcareUtils.restrictTechSupport()) {
                options.add(new SelectOption(temp,temp));
            }
        }
        return options;
    }

    public Case getParentCase() {
        return caseToInsert.Parent;
    }

    public String getParentCaseType() {
        return parentCaseType;
    }

    public String getParentCategory() {
        return parentCategory;
    }

    public Case getCaseToInsert() {
        return caseToInsert;
    }


    /**
     *  Query for all My Business Requests cases that are status = 'new' and time 
     *  elapsed > 24 hours, then update priority to 'Urgent'
     *
     * @lastmodified
     *   Alex Kong (TOD), 12/11/14
     *
     */
    public static void updateCasePriorityToUrgent() {

        List<Case> cases = new List<Case>();

        // find the Case Origin string
        String originValue = 'VITILcare';
        //Customer_Interface_Settings__c cis = Customer_Interface_Settings__c.getInstance();
       VITILCare_Customer_Interface_Settings__c cis = VITILCare_Customer_Interface_Settings__c.getInstance();
        if(cis.Case_Origin_Value__c != null) {
            originValue = cis.Case_Origin_Value__c;
        }

        System.debug('originValue: ' + originValue);

        // query for all relevant cases
        DateTime cutoffDateTime = DateTime.now().addHours(-24);
        if (!Test.isRunningTest()) {
            cases = [SELECT Id, Subject, Status, Priority, CreatedDate
                FROM Case 
                WHERE Origin = :originValue 
                AND Status = 'New' 
                AND Priority != 'Urgent' 
                AND (CreatedDate <= :cutoffDateTime)];
        } else {
            // remove the createdDate requirement for testing
            cases = [SELECT Id, Subject, Status, Priority, CreatedDate 
                FROM Case 
                WHERE Origin = :originValue 
                AND Status = 'New' 
                AND Priority != 'Urgent'];
        }

        System.debug('akong: cases: ' + cases);


        // loop through each case to determine if we should update priority
        Date sunday = Date.newInstance(1900, 1, 7); // 1900-01-07 is a sunday

        System.debug('akong: sunday: ' + sunday);

        for (Case c : cases) {
            System.debug('akong: c.Id: ' + c.Id);
            Integer dayOfWeek = Math.mod(sunday.daysBetween( c.CreatedDate.date() ), 7); // dayOfWeek, 0 is sunday
            if (Test.isRunningTest()) {
                dayOfWeek = Integer.valueOf(c.Subject);
            }
            System.debug('akong: dayOfWeek: ' + dayOfWeek);
            if (dayOfWeek >= 1 && dayOfWeek <= 4) {
                // monday to thursday, 24 hour check in soql query is sufficient
                c.Priority = 'Urgent';
            } else if (dayOfWeek == 5) {
                // friday, 72 hour check
                Integer hours = (Integer)( DateTime.now().getTime() - c.CreatedDate.getTime() ) / 1000 / 60 / 60;
                if (hours >= 72 || Test.isRunningTest()) {
                    c.Priority = 'Urgent';
                }
            } else if (dayOfWeek == 6) {
                // saturday, custom check for monday right before midnight (i.e. monday at 11:59:59 pm)
                DateTime mondayMidnight = DateTime.newInstance( c.CreatedDate.date().addDays(2), Time.newInstance(23, 59, 59, 999));
                if (mondayMidnight <= DateTime.now() || Test.isRunningTest()) {
                    c.Priority = 'Urgent';
                }
            } else if (dayOfWeek == 0) {
                // sunday, custom check for monday right before midnight (i.e. monday at 11:59:59 pm)
                DateTime mondayMidnight = DateTime.newInstance( c.CreatedDate.date().addDays(1), Time.newInstance(23, 59, 59, 999));
                if (mondayMidnight <= DateTime.now() || Test.isRunningTest()) {
                    c.Priority = 'Urgent';
                }
            }
        }

        // update
        update cases;
        System.debug('akong: final cases: ' + cases);
    }
}