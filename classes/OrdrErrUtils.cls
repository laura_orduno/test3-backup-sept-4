public class OrdrErrUtils 
{      
     public static void  blankExternalOrderIdHandler(String payload, String apexClzAndmethodName,String wsName)
    {     
        logCalloutError(null,null, null, apexClzAndmethodName, OrdrConstants.EXTERNAL_SYSTEM_NAME_NC, wsName,
        OrdrConstants.OBJECT_NAME, payload); 
        throw new OrdrExceptions.InvalidParameterException(payload);       
    }
    
    public static void orderNotFoundHandler(Id orderId, String mesg, String apexClassAndMethodName)
    { 
        String description = mesg + orderId;
        logExternalOrderIdNotFound
        ( 
            description,
            apexClassAndMethodName,
            OrdrConstants.EXTERNAL_SYSTEM_NAME_NC, 
            OrdrCancelOrderWsCallout.WS_NAME,                                      
            OrdrConstants.OBJECT_NAME
        );
            
        throw new OrdrExceptions.ResultNotFoundException(description);
      
    }
    
    public static void externalOrderIdNotFoundHandler(Id orderId, String apexClzAndmethodName, String mesg)
    {
         String description = mesg + orderId; 
         logExternalOrderIdNotFound
         ( 
             description,   
             apexClzAndmethodName,
             OrdrConstants.EXTERNAL_SYSTEM_NAME_NC, 
             OrdrCancelOrderWsCallout.WS_NAME,                                      
             OrdrConstants.OBJECT_NAME
         ); 
        
         throw new OrdrExceptions.InvalidParameterException(description);
     
    }
    
    
    public static void logError(Exception e, 
                                String classAndMethodName, 
                                String externalSystemName, 
                                String wsName,
                                String objectName,
                                String payload)
    {            
        Webservice_Integration_Error_Log__c errLog = 
                new Webservice_Integration_Error_Log__c
                (
                    apex_class_and_method__c = classAndMethodName,
                    external_system_name__c= externalSystemName,
                    webservice_name__c= wsName,
                    object_name__c = objectName                     
                );     
    
        appendExceptionDetails(e, errLog, payload);
       
        insert errLog;  
   }   
    
     //select Apex_Class_and_Method__c from Webservice_Integration_Error_Log__c can be used to display Webservice_Integration_Error_Log__c per Developer Console 
   public static void logCalloutError
                                    (
                                      String orderId,
                                      String externalOrderid, 
                                      Exception e, 
                                      String classAndMethodName, 
                                      String externalSystemName, 
                                      String wsName,                                      
                                      String objectName,
                                      String payload
                                    )
   {            
   		Webservice_Integration_Error_Log__c errLog = 
                new Webservice_Integration_Error_Log__c
                (
                    apex_class_and_method__c = classAndMethodName,
                    external_system_name__c= externalSystemName,
                    webservice_name__c= wsName,
                    object_name__c = objectName                     
                );
       
     	if(String.isNotBlank(orderId))
     	{
        	errLog.SFDC_Record_Id__c = 'orderId='+ orderId;
     	}
     	else if(String.isNotBlank(externalOrderid))
     	{
        	errLog.SFDC_Record_Id__c = 'externalOrderid='+ externalOrderid;
     	}
    
     	appendExceptionDetails(e, errLog, payload);
       
     	insert errLog;  
   }   
   
   public static void logExternalOrderIdNotFound(   
                                                    String payload,  
                                                    String apexClassAndMethod, 
                                                    String systemName, 
                                                    String webserviceName,                                      
                                                    String objectName
                                                )
   {       
       	writeToWebserviceIntegrationErrorLog(null, objectName, apexClassAndMethod, webserviceName, payload, null);
   }
   
   public static void writeToWebserviceIntegrationErrorLog(
                                                                String errorRecordId,
                                                                String objectName,
                                                                String apexClassAndMethod,
                                                                String webserviceName,
                                                                String payload,
                                                                Exception e
                                                            )
   {        
       	Webservice_Integration_Error_Log__c errLog = new Webservice_Integration_Error_Log__c();
            
       	if(String.isNotBlank(objectName))
       	{
          	errLog.Object_Name__c = objectName;
       	}
            
       	if(String.isNotBlank(errorRecordId))
       	{
          	errLog.SFDC_Record_Id__c = errorRecordId;
       	}
            
       	if(String.isNotBlank(apexClassAndMethod))
       	{
          	errLog.Apex_Class_and_Method__c = apexClassAndMethod;     
       	}
            
       	if(String.isNotBlank(webserviceName))  
       	{                 
           	errLog.Webservice_Name__c = webserviceName;
       	}
            
       	if(String.isNotBlank(payload))
       	{
          	errLog.Error_Code_and_Message__c = errLog.Error_Code_and_Message__c   + payload;
       	}
               
       	if(e != null)
       	{
          	errLog.Error_Code_and_Message__c = getErrorCode(e) + ', ' + e.getMessage();              
          	errLog.Stack_Trace__c = e.getStackTraceString();
       	}
            
       insert errLog;    
    }    
   
    private static String getErrorCode(Exception e)
    {          
        String totalStr = e.getMessage() + e.getStackTraceString();             
        String substringAfterLastSeparator = totalStr.substringAfterLast('ERROR CODE:');
        String errorCode = substringAfterLastSeparator.substringBeforeLast('}');  
        
        return errorCode;        
    }
    
    private static void appendExceptionDetails(Exception e, Webservice_Integration_Error_Log__c errLog, String payload)
    {
        if(e != null)
        {
            String message = e.getMessage();      
            if( String.isNotBlank(message))
            {
                errLog.Error_Code_and_Message__c = 'Error message='+ message;
            }
       
            String stackTrace =  e.getStackTraceString();
            if( String.isNotBlank(stackTrace))
            {
                errLog.Error_Code_and_Message__c = 'Error stackTrace='+ stackTrace;
            }
       
            String typeName = e.getTypeName();
            if( String.isNotBlank(typeName))
            {
                errLog.Error_Code_and_Message__c = 'typeName='+ typeName;
            }
        }
       
       errLog.CreatedDate = System.now();
       errLog.LastModifiedDate = System.now();
        
       if(String.isNotBlank(payload))
       {
           if(errLog.Error_Code_and_Message__c != null)
           {
               errLog.Error_Code_and_Message__c = errLog.Error_Code_and_Message__c + '; More detail message=' + payload;
        }
           else
           {
               errLog.Error_Code_and_Message__c = 'Error Message=' + payload;
           }
       }        
       
    }
}