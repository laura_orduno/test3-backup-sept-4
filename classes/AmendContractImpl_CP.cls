global with sharing class AmendContractImpl_CP implements vlocity_cmt.VlocityOpenInterface{
    
    public Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {
        Boolean success = true;
        try{
            if(methodName == 'AmendContractMethod') {             
                 AmendContractMethod(inputMap, outMap, options);                
            }
          }catch(Exception e){
                    
                    success=false;
         }

        return success;        
    }
    
     private static void AmendContractMethod(Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options){
         system.debug( 'InpuMap ContainsKey' + inputMap.containsKey('ContextId'));
         list<string> errorMessages = new list<string>();
         if(inputMap.containsKey('ContextId'))
         {
             Id conId = (id)inputMap.get('ContextId');
             system.debug('Amending Contract...' + conID);
             outMap = AmendContractProcess_CP.createDealSupport(conId, outMap);
             system.debug('OutMap' + outMap);
         	
         }
    }


}