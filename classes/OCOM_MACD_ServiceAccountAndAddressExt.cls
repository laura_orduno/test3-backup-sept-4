public without sharing class OCOM_MACD_ServiceAccountAndAddressExt {
    public OCOM_InOrderFlowPACController controllerRef;
    public OCOM_PAC_MACD_Controller MacdcontrollerRef;
    public OCOM_OutOrderFlowPACController OutsideFlowControllerRef;
    public OC_SelectAndNewAddressController newAddressController;
    
    public OCOM_MACD_ServiceAccountAndAddressExt() {
    }
    
    public OCOM_MACD_ServiceAccountAndAddressExt(OC_SelectAndNewAddressController controller) {
        this.newAddressController = controller;
    }
    
    public OCOM_MACD_ServiceAccountAndAddressExt(OCOM_InOrderFlowPACController controller) {
        this.controllerRef=controller;
    }
    
    public OCOM_MACD_ServiceAccountAndAddressExt(OCOM_PAC_MACD_Controller controller) {
        this.MacdcontrollerRef=controller;
    }
    
    public OCOM_MACD_ServiceAccountAndAddressExt(OCOM_OutOrderFlowPACController controller) {
        this.OutsideFlowControllerRef=controller;
    }
    
    // Added by Aditya Jamwal(IBM),This method will return Servcice Account
    Public Account CreateServiceAccount(Id ParentAccountID, SMBCare_Address__c smbCareAddr, ServiceAddressData SAData)
    {
        Id ServiceRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        List<SMBCare_Address__c> SMBCareList = New List<SMBCare_Address__c>();
        OCOM_ServiceAddressControllerHelper callSAHelper = new OCOM_ServiceAddressControllerHelper();        
        
        //**********************************************************************************************************/     
        
        if(SAData.isManualCapture){
            smbCareAddr.Address__c = SAData.address;
            smbCareAddr.Street_Name__c = SAData.address;
            smbCareAddr.city__c = SAData.city;
            smbCareAddr.Municipality_Name__c = SAData.city;
            smbCareAddr.Province__c = SAData.province;
            smbCareAddr.Postal_Code__c= SAData.postalCode;  
            
            smbCareAddr.Country__c=SAData.country;    
            smbCareAddr.Send_to_SACG__c = SAData.isSACG;
            smbCareAddr.Street_Number__c = SAData.streetNumber; // Added as per BSBDTR-710 - Billing Fallout for NaaS Non-ILEC Orders
            
            Account ac = CreateServiceAcc(SAData, ParentAccountID); 
            system.debug('!! 2 acc '+ac);
            id smbCareAddrId =callSAHelper.createServiceAddress(smbCareAddr, SAData,ParentAccountID,ac.Id);
            system.debug('!! 3 acc '+ac);
            return ac;                                                                       
        }
        
        boolean serviceAccountFound=false;
        // Start TF Case 03359357, JIRA defect BSBD_RTA-1272
        if(String.isBlank(SAData.fmsId) && String.isNotBlank(SAData.locationId)){
        		SMBCareList = [select Service_Account_Id__c,id, Account__c, FMS_Address_ID__c, Status__c,Postal_Code__c , Street_Number__c, city__c , 
                       Street__c, Street_Name__c, Street_Direction_Code__c , Suite_Number__c , Province__c, COID__c , Building_Number__c , 
                       Country__c,  State__c, Civic_Number__c, Street_Type_Code__c , Location_Id__c , Address__c , Address_Type__c ,  
                       LocalRoutingNumber__c , NPA_NXX__c , SwitchNumber__c , TerminalNumber__c, Service_Account_Id__r.RecordTypeId  
                       from SMBCare_Address__c where Account__c =:ParentAccountID and  Location_Id__c=:SAData.locationId];
        }else{
        		SMBCareList = [select Service_Account_Id__c,id, Account__c, FMS_Address_ID__c, Status__c,Postal_Code__c , Street_Number__c, city__c , 
                       Street__c, Street_Name__c, Street_Direction_Code__c , Suite_Number__c , Province__c, COID__c , Building_Number__c , 
                       Country__c,  State__c, Civic_Number__c, Street_Type_Code__c , Location_Id__c , Address__c , Address_Type__c ,  
                       LocalRoutingNumber__c , NPA_NXX__c , SwitchNumber__c , TerminalNumber__c, Service_Account_Id__r.RecordTypeId  
                       from SMBCare_Address__c where Account__c =:ParentAccountID and  FMS_Address_ID__c=:SAData.fmsId];
        }
        // End TF Case 03359357, JIRA defect BSBD_RTA-1272
        system.debug('!! 1 '+SMBCareList);
        if(SMBCareList!=null) {
            for(SMBCare_Address__c SmbCare: SMBCareList){         
                if(String.isNotBlank(SmbCare.Service_Account_Id__c) && SmbCare.Service_Account_Id__r.RecordTypeId.equals(ServiceRecordType) && SmbCare.Account__c ==ParentAccountID){
                    serviceAccountFound=true;
                    callSAHelper.updateServiceAddress(SmbCare, SAData, SmbCare.id);
                    system.debug('!! 1acc '+SmbCare.Service_Account_Id__c);
                    system.debug('$$$$serviceAddressId:'+SmbCare.id);
                    return new Account(id=SmbCare.Service_Account_Id__c,parentId=ParentAccountID);
                }  
            }
        }
        
        if(!serviceAccountFound && SMBCareList.size()>0){
            List<SMBCare_Address__c> toUpdateList=new List<SMBCare_Address__c>();
            Account ac = new Account();
            for(SMBCare_Address__c SmbCare: SMBCareList){
                if(String.isBlank(SmbCare.Service_Account_Id__c) && SmbCare.Account__c ==ParentAccountID){
                    ac = CreateServiceAcc(SAData, ParentAccountID);
                    SmbCare.Service_Account_Id__c=ac.id;  
                    toUpdateList.add(SmbCare);  
                    break;   
                }
            }
            if(toUpdateList.size()>0){                  
                update toUpdateList;  
            } 
            system.debug('!! 2 acc '+ac);
            return ac;      
        }
        
        if(SMBCareList==null || SMBCareList.size()==0){
            System.debug('No record found###'); 
            Account ac = CreateServiceAcc(SAData, ParentAccountID); 
            id smbCareAddrId =callSAHelper.createServiceAddress(smbCareAddr, SAData,ParentAccountID,ac.Id);
            system.debug('!! 3 acc '+ac);
            return ac;
        }
        system.debug('!! 4 acc ');  
        return null; 
        //**********************************************************************************************************/         
    }
    
    
    Public Account CreateServiceAcc(ServiceAddressData SAData, String parent)
    {
        Id ServiceRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        Account Acc;
        if(SAData.ismanualcapture){
            String sAddr = ' '+(String)SAData.address+ ' ' +(String)SAData.city+ ' ' +(String)SAData.province+ ' '+(String)SAData.postalCode+' ' +(String)SAData.country;
            Acc = New Account(Name=sAddr +' '+SAData.fmsId, RecordTypeId=ServiceRecordType, ParentId=parent);
        }else{
            // added below condition as per BSBD_RTA-1272
            String fmsOrLocId = String.isNotBlank(SAData.fmsId)?SAData.fmsId:SAData.locationId;
            //Acc = New Account(Name=SAData.searchString +' '+SAData.fmsId, RecordTypeId=ServiceRecordType, ParentId=parent);
            Acc = New Account(Name=SAData.searchString +' '+fmsOrLocId, RecordTypeId=ServiceRecordType, ParentId=parent);
            // End BSBD_RTA-1272
        }        
        try
        {
            Insert Acc;
        }
        catch(exception e)
        {
            throw e;
            return null;
        }
        return Acc;
    }
    
    /* Service Account creation end */
}