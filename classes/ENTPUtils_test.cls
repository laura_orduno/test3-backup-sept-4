@isTest
private class ENTPUtils_test {

	@isTest static void testGetCategoryRequestTypes() {
		List<ParentToChild__c> ptcList = new List<ParentToChild__c>();
		ptcList.add(new ParentToChild__c(Name = 'TestCatRequest1', Identifier__c = 'ENTPCatRequest', Parent__c = 'parent1'));
		ptcList.add(new ParentToChild__c(Name = 'TestCatRequest2', Identifier__c = 'CCICatRequest', Parent__c = 'parent2'));
		ptcList.add(new ParentToChild__c(Name = 'TestCatRequest3', Identifier__c = 'CCIDogRequest', Parent__c = 'parent1'));
		ptcList.add(new ParentToChild__c(Name = 'TestCatRequest4', Identifier__c = 'CCICatRequest', Parent__c = 'parent1'));
		ptcList.add(new ParentToChild__c(Name = 'TestCatRequest5', Identifier__c = 'CCICatRequest', Parent__c = 'parent1'));
		insert ptcList;

		Test.startTest();
		Map<String, List<String>> catToReqTypes = ENTPUtils.getCategoryRequestTypes();
		Test.stopTest();

		// System.assertEquals(2, catToReqTypes.size());
		// System.assertEquals(3, catToReqTypes.get('parent1').size());
	}

	@isTest static void testGetExternalToInternalStatuses() {
		List<ExternalToInternal__c> e2iList = new List<ExternalToInternal__c>();
		e2iList.add(new ExternalToInternal__c(Name = 'e2iA', Identifier__c = 'onlineStatus', External__c = 'external1'));
		e2iList.add(new ExternalToInternal__c(Name = 'e2iB', Identifier__c = 'onlineStatus', External__c = 'external2'));
		e2iList.add(new ExternalToInternal__c(Name = 'e2iC', Identifier__c = 'onlineStatus', External__c = 'external1'));
		e2iList.add(new ExternalToInternal__c(Name = 'e2iD', Identifier__c = 'onlineStatus', External__c = 'external1'));
		e2iList.add(new ExternalToInternal__c(Name = 'e2iE', Identifier__c = 'beepbeep', External__c = 'external1'));
		insert e2iList;

		Test.startTest();
		Map<String, List<String>> e2iStatuses = ENTPUtils.getExternalToInternalStatuses();
		Test.stopTest();

		System.assertEquals(2, e2iStatuses.size());
		System.assertEquals(3, e2iStatuses.get('external1').size());
	}

	@isTest static void testGetRequestTypeToRecordTypeNames() {
		List<ExternalToInternal__c> e2iList = new List<ExternalToInternal__c>();
		e2iList.add(new ExternalToInternal__c(Name = 'e2iA', Identifier__c = 'onlineRequestTypeToCaseRT', External__c = 'external1'));
		e2iList.add(new ExternalToInternal__c(Name = 'e2iB', Identifier__c = 'onlineRequestTypeToCaseRT', External__c = 'external2'));
		e2iList.add(new ExternalToInternal__c(Name = 'e2iC', Identifier__c = 'onlineRequestTypeToCaseRT', External__c = 'external1'));
		e2iList.add(new ExternalToInternal__c(Name = 'e2iD', Identifier__c = 'onlineRequestTypeToCaseRT', External__c = 'external1'));
		e2iList.add(new ExternalToInternal__c(Name = 'e2iE', Identifier__c = 'beepbeep', External__c = 'external1'));
		insert e2iList;

		Test.startTest();
		Map<String, List<String>> reqTypes2RecTypes = ENTPUtils.getRequestTypeToRecordTypeNames();
		Test.stopTest();

		System.assertEquals(2, reqTypes2RecTypes.size());
		System.assertEquals(3, reqTypes2RecTypes.get('external1').size());
	}

	@isTest static void testGetRecordTypeIdsByCategory() {
		List<ParentToChild__c> ptcList = new List<ParentToChild__c>();
		ptcList.add(new ParentToChild__c(Name = 'ptc1', Child__c = 'external1', Identifier__c = 'CCICatRequest', Parent__c = 'parent1'));
		ptcList.add(new ParentToChild__c(Name = 'ptcA', Child__c = 'externalA', Identifier__c = 'CCICatRequest', Parent__c = 'parent2'));
		ptcList.add(new ParentToChild__c(Name = 'ptcB', Child__c = 'externalB', Identifier__c = 'CCIDogRequest', Parent__c = 'parent1'));
		ptcList.add(new ParentToChild__c(Name = 'ptcC', Child__c = 'externalC', Identifier__c = 'CCICatRequest', Parent__c = 'parent1'));
		ptcList.add(new ParentToChild__c(Name = 'ptc2', Child__c = 'external2', Identifier__c = 'CCICatRequest', Parent__c = 'parent1'));
		insert ptcList;

		List<ExternalToInternal__c> e2iList = new List<ExternalToInternal__c>();
		//e2iList.add( new ExternalToInternal__c(Name='e2iA', Identifier__c='onlineRequestTypeToCaseRT', External__c='external1', Internal__c='ASR Activities') );
		//e2iList.add( new ExternalToInternal__c(Name='e2iB', Identifier__c='onlineRequestTypeToCaseRT', External__c='external2', Internal__c='BSR Activities') );
		e2iList.add(new ExternalToInternal__c(Name = 'e2iC', Identifier__c = 'onlineRequestTypeToCaseRT', External__c = 'external1', Internal__c = 'Generic'));
		e2iList.add(new ExternalToInternal__c(Name = 'e2iD', Identifier__c = 'onlineRequestTypeToCaseRT', External__c = 'external1', Internal__c = 'Feedback'));
		e2iList.add(new ExternalToInternal__c(Name = 'e2iE', Identifier__c = 'beepbeep', External__c = 'external1', Internal__c = 'Contact Center Activities'));
		insert e2iList;

		Test.startTest();
		Set<String> recTypes = ENTPUtils.getRecordTypeIdsByCategory('parent1');
		Test.stopTest();

		// System.assertEquals(2, recTypes.size());
	}

	@isTest static void testGetInClause() {
		List<String> testList = new List<String>();
		testList.add('one');
		testList.add('two');
		testList.add('three');
		testList.add('four');
		String retval = ENTPUtils.getInClause(testList);
		System.assert(retval.length() > 0, 'Failed list join test');

		Set<String> testSet = new Set<String>();
		testSet.add('foo');
		testSet.add('bar');
		testSet.add('baz');
		testSet.add('beep');
		String retval2 = ENTPUtils.getInClause(testSet);
		System.assert(retval2.length() > 0, 'Failed set join test');
	}

	@isTest static void testGetRequestTypeFilterForCategory() {
		List<ParentToChild__c> ptcList = new List<ParentToChild__c>();
		ptcList.add(new ParentToChild__c(Name = 'ptc1', Child__c = 'external1', Identifier__c = 'ENTPCatRequest', Parent__c = 'parent1'));
		ptcList.add(new ParentToChild__c(Name = 'ptcA', Child__c = 'externalA', Identifier__c = 'CCICatRequest', Parent__c = 'parent2'));
		ptcList.add(new ParentToChild__c(Name = 'ptcB', Child__c = 'externalB', Identifier__c = 'CCIDogRequest', Parent__c = 'parent1'));
		ptcList.add(new ParentToChild__c(Name = 'ptcC', Child__c = 'externalC', Identifier__c = 'CCICatRequest', Parent__c = 'parent1'));
		ptcList.add(new ParentToChild__c(Name = 'ptc2', Child__c = 'external2', Identifier__c = 'CCICatRequest', Parent__c = 'parent1'));
		insert ptcList;

		Test.startTest();
		// String myString = ENTPUtils.getRequestTypeFilterForCategory('Test1,Test2');
		Test.stopTest();

		//System.assert(myString.length() > 0);
	}

	@isTest static void testGetInternalStatusFilterByExternal() {
		List<ExternalToInternal__c> e2iList = new List<ExternalToInternal__c>();
		e2iList.add(new ExternalToInternal__c(Name = 'e2iA', Identifier__c = 'onlineStatus', External__c = 'external1', Internal__c = 'ASR Activities'));
		e2iList.add(new ExternalToInternal__c(Name = 'e2iB', Identifier__c = 'onlineStatus', External__c = 'external2', Internal__c = 'BSR Activities'));
		e2iList.add(new ExternalToInternal__c(Name = 'e2iC', Identifier__c = 'onlineStatus', External__c = 'external1', Internal__c = 'Generic'));
		e2iList.add(new ExternalToInternal__c(Name = 'e2iD', Identifier__c = 'onlineStatus', External__c = 'external1', Internal__c = 'Feedback'));
		e2iList.add(new ExternalToInternal__c(Name = 'e2iE', Identifier__c = 'beepbeep', External__c = 'external1', Internal__c = 'Contact Center Activities'));
		insert e2iList;

		Test.startTest();
		String myString = ENTPUtils.getInternalStatusFilterByExternal('external1');
		Test.stopTest();

		System.assert(myString.length() > 0);
	}

	@isTest static void testGetAccountConfiguration() {

		User u = MBRTestUtils.createPortalUser('');
		System.debug('akong: u: ' + u);
		User user = [SELECT Id, Contact.Account.Id FROM User WHERE Id = :u.Id][0];
		System.debug('akong: user: ' + user);
		System.debug('akong: user.Contact.Account.Id: ' + user.Contact.Account.Id);
		//System.debug('akong: u.Contact.Account: ' + u.Contact.Account);

		Account_Configuration__c ac = new Account_Configuration__c();
		ac.Account__c = user.Contact.Account.Id;
		ac.Name = 'Account Config for Unit Test Account';
		ac.Restrict_Tech_Support__c = true;
		insert ac;

		System.runAs(u) {
			Account_Configuration__c ac2 = ENTPUtils.getAccountConfiguration();
			System.assertEquals(ac.Id, ac2.Id);

			Boolean restrictTechSupport = ENTPUtils.restrictTechSupport();
			System.assertEquals(ac.Restrict_Tech_Support__c, restrictTechSupport);
		}
	}
	@isTest(SeeAllData=false) static void ENTPUtils() {
		User u = MBRTestUtils.createENTPPortalUser('test-user-1', false, 'en_US');

		System.runAs(u) {
			ENTPUtils.getCategoryRequestTypes();
			//ENTPUtils.getRequestTypeFilterForCategory('Test');
			ENTPUtils.getExternalToInternalStatuses();
			ENTPUtils.getRequestTypeToRecordTypeNames();
			//ENTPUtils.cacheExternalToInternalMaps();
			ENTPUtils.getRecordTypeIdsByCategory('test');
			ENTPUtils.getAccountConfiguration();
			ENTPUtils.restrictTechSupport();
			ENTPUtils.validateEmail('test@testmail.com');
			ENTPUtils.mockWebserviceResponse(ENTPUtils.LYNX_TICKET_SEARCH_XML_RESPONSE);
		}
	}
	@isTest public static void processCaseComments_test() {
		String userName = 'test-user-1';
		String lang = 'en_US';
		Account a = new Account(Name = 'Unit Test Account');
		a.strategic__c = true;
		insert a;
		Contact c = new Contact(Email = userName + '@unit-test.com', LastName = 'Test', AccountId = a.Id);
		insert c;
		Profile p = [SELECT Id FROM Profile WHERE Name = 'Customer Community User Custom' AND UserType = 'CSPLitePortal' LIMIT 1];
		User u = new User(Alias = 'TestUser', Email = userName + '@unit-test.com', ContactId = c.Id,
				EmailEncodingKey = 'UTF-8', FirstName = 'Unit', LastName = 'Testing', LanguageLocaleKey = lang,
				LocaleSidKey = lang, CommunityNickname = userName + 'UNTST', ProfileId = p.Id,
				TimeZoneSidKey = 'America/Los_Angeles',
				UserName = userName + '@unit-test.com');
		u.Customer_Portal_Role__C = 'tps';
		insert u;

		Test.startTest();
		System.runAs(u) {
			List<Case> testCases = ENTPTestUtils.createENTPCases(2, a.Id);
			//create a comment - not EMAIL Comment
			ENTPTestUtils.createComment(testCases[0].Id, false, false);
			//create a comment - EMAIL Comment
			ENTPTestUtils.createComment(testCases[0].Id, true, true);
			List<CaseComment> caseComments = [Select id,CommentBody,Createdby.LastName,Createdby.firstname, CreatedDate from CaseComment where ParentId = :testCases[0].Id];
			//Email message create
			// ENTPCaseCommentTriggerHelper ctlr = new ENTPCaseCommentTriggerHelper();
			Set<Id> caseCommentIds = new Set<Id>();
			for (CaseComment cc : caseComments) {
				caseCommentIds.add(cc.id);
			}
			trac_TriggerHandlerBase.blockTrigger = true;
			ENTPCaseCommentTriggerHelper.processCaseComments(caseCommentIds);
		}
		Test.stopTest();
	}
}