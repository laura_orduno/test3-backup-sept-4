@isTest
public class CreditSrchConsumerProfileControllerTest
{
    private static CreditSearchConsumerProfileController controller;
    
	@isTest
    public static void selectProfileForUpdate()
    {
        initialize();

        controller.selectProfileForUpdate();
    }
    
	@isTest
    public static void selectProfileForUpdateWithEmptyCurrentCreditProfile()
    {
        initialize();

        controller.currentCreditProfile = new CreditSearchConsumerProfileController.DisplayConsumerProfile();
        controller.consumerProfileForUpdateId = '100';

        List<CreditSearchConsumerProfileController.DisplayConsumerProfile> displayProfiles = new List<CreditSearchConsumerProfileController.DisplayConsumerProfile>();
        CreditSearchConsumerProfileController.DisplayConsumerProfile displayProfile = new CreditSearchConsumerProfileController.DisplayConsumerProfile();
        displayProfile.key = '100';
        displayProfile.driversLicenseNum = 'DL987321';
        displayProfile.driversLicenseProvinceCd = 'BC';
        displayProfile.provincialIdNum = '3333';
        displayProfile.provincialIdProvinceCd = 'BC';
        displayProfile.passportNum = '123456789';
        displayProfile.passportCountry = 'CAN';
        displayProfile.healthCardNum = '9876543210';

        displayProfiles.add(displayProfile);

        controller.consumerProfiles = displayProfiles;

        controller.selectProfileForUpdate();
    }

    @isTest
    public static void executeUpdate()
    {
        initialize();

        List<CreditSearchConsumerProfileController.DisplayConsumerProfile> displayProfiles = new List<CreditSearchConsumerProfileController.DisplayConsumerProfile>();

        CreditSearchConsumerProfileController.DisplayConsumerProfile displayProfile = new CreditSearchConsumerProfileController.DisplayConsumerProfile();
        displayProfile.key = '100';
        displayProfile.driversLicenseNum = 'DL987321';
        displayProfile.driversLicenseProvinceCd = 'BC';
        displayProfile.provincialIdNum = '3333';
        displayProfile.provincialIdProvinceCd = 'BC';
        displayProfile.passportNum = '123456789';
        displayProfile.passportCountry = 'CAN';
        displayProfile.healthCardNum = '9876543210';

        displayProfiles.add(displayProfile);

        controller.consumerProfiles = displayProfiles;
        controller.consumerProfileForUpdateId = '100';
        controller.columnToSave = 'HEALTHCARD';

        controller.executeUpdate();
    }

    @isTest
    public static void cancelUpdate()
    {
        initialize();

        controller.cancelUpdate();
    }

    @isTest
    public static void updateCreditProfile()
    {
        initialize();

        List<CreditSearchConsumerProfileController.DisplayConsumerProfile> displayProfiles = new List<CreditSearchConsumerProfileController.DisplayConsumerProfile>();

        CreditSearchConsumerProfileController.DisplayConsumerProfile displayProfile = new CreditSearchConsumerProfileController.DisplayConsumerProfile();
        displayProfile.key = '100';
        displayProfile.birthdate = Date.newInstance(1995, 1, 1);
        displayProfile.sin = '220380919';
        displayProfile.driversLicenseNum = 'DL987321';
        displayProfile.driversLicenseProvinceCd = 'BC';
        displayProfile.provincialIdNum = '3333';
        displayProfile.provincialIdProvinceCd = 'BC';
        displayProfile.passportNum = '123456789';
        displayProfile.passportCountry = 'CAN';
        displayProfile.healthCardNum = '9876543210';

        displayProfiles.add(displayProfile);

        controller.consumerProfiles = displayProfiles;

        controller.consumerProfileForUpdateId = '100';

        controller.columnToSave = 'BIRTHDATE';
        controller.assignColumnToSave();

        controller.columnToSave = 'SIN';
        controller.assignColumnToSave();

        controller.columnToSave = 'DRIVERS_LICENSE';
        controller.assignColumnToSave();

        controller.columnToSave = 'PROVINCIAL_ID';
        controller.assignColumnToSave();

        controller.columnToSave = 'PASSPORT';
        controller.assignColumnToSave();

        controller.columnToSave = 'HEALTHCARD';
        controller.assignColumnToSave();

        controller.updateCreditProfile();
    }

    @isTest
    public static void search()
    {
        initialize();

        Test.startTest();

        CreditProfileServiceCallout.CreditSearchParamForIndividualProfile param = new CreditProfileServiceCallout.CreditSearchParamForIndividualProfile();
        param.driversLicenseNum = 'DL987321';
        param.driversLicenseProvinceCd = 'BC';

        controller.userSearchParam = param;

        controller.search();

        Test.stopTest();
    }

    @isTest
    public static void searchIndividualCreditProfiles()
    {
        initialize();

        CreditProfileServiceCallout.CreditSearchParamForIndividualProfile param = new CreditProfileServiceCallout.CreditSearchParamForIndividualProfile();
        param.healthCardNum = '123456789';

        controller.searchIndividualCreditProfiles(param);
    }

    @isTest
    public static void assignColumnToSave()
    {
        initialize();

        List<CreditSearchConsumerProfileController.DisplayConsumerProfile> displayProfiles = new List<CreditSearchConsumerProfileController.DisplayConsumerProfile>();

        CreditSearchConsumerProfileController.DisplayConsumerProfile displayProfile = new CreditSearchConsumerProfileController.DisplayConsumerProfile();
        displayProfile.key = '100';
        displayProfile.driversLicenseNum = 'DL987321';
        displayProfile.driversLicenseProvinceCd = 'BC';
        displayProfile.provincialIdNum = '3333';
        displayProfile.provincialIdProvinceCd = 'BC';
        displayProfile.passportNum = '123456789';
        displayProfile.passportCountry = 'CAN';
        displayProfile.healthCardNum = '9876543210';

        displayProfiles.add(displayProfile);

        controller.consumerProfiles = displayProfiles;

        controller.consumerProfileForUpdateId = '100';

        controller.columnToSave = 'DRIVERS_LICENSE';
        controller.assignColumnToSave();

        controller.columnToSave = 'PROVINCIAL_ID';
        controller.assignColumnToSave();

        controller.columnToSave = 'PASSPORT';
        controller.assignColumnToSave();

        controller.columnToSave = 'HEALTHCARD';
        controller.assignColumnToSave();
    }

    @isTest
    public static void previewCreateConsumerCreditProfile()
    {
        initialize();

        controller.previewCreateConsumerCreditProfile();
    }

    @isTest
    public static void executeCreateProfile()
    {
        initialize();

        controller.executeCreateProfile();
    }

    @isTest
    public static void cancelCreateProfile()
    {
        initialize();

        controller.previewCreateConsumerCreditProfile();
        controller.cancelCreateProfile();
    }

    @isTest
    public static void createConsumerCreditProfile()
    {
        initialize();

        Test.startTest();

        controller.createConsumerCreditProfile();

        Test.stopTest();
    }

    @isTest
    public static void getConsumerProfiles()
    {
        initialize();

        controller.getConsumerProfiles();
    }

    @isTest
    public static void getCurrentCreditProfile()
    {
        initialize();

        controller.getCurrentCreditProfile();
    }

    @isTest
    public static void getDisplayProfileForColumnSelection()
    {
        initialize();

        controller.getDisplayProfileForColumnSelection();
    }

    @isTest
    public static void getSearchParam()
    {
        initialize();

        CreditSearchConsumerProfileController.DisplayConsumerProfile displayProfile = new CreditSearchConsumerProfileController.DisplayConsumerProfile();
        displayProfile.healthCardNum = '9876543210';
        displayProfile.driversLicenseNum = 'DL987321';
        displayProfile.driversLicenseProvinceCd = 'BC';

        controller.currentCreditProfile = displayProfile;

        controller.getSearchParam();
    }

    @isTest
    public static void getSelectedProfile()
    {
        initialize();

        List<CreditSearchConsumerProfileController.DisplayConsumerProfile> displayProfiles = new List<CreditSearchConsumerProfileController.DisplayConsumerProfile>();

        CreditSearchConsumerProfileController.DisplayConsumerProfile displayProfile = new CreditSearchConsumerProfileController.DisplayConsumerProfile();
        displayProfile.key = '100';
        displayProfile.healthCardNum = '9876543210';
        displayProfile.driversLicenseNum = 'DL987321';
        displayProfile.driversLicenseProvinceCd = 'BC';

        displayProfiles.add(displayProfile);

        controller.consumerProfiles = displayProfiles;

        controller.consumerProfileForUpdateId = '100';

        controller.getSelectedProfile();
    }

    @isTest
    public static void initializeControllerWithCar()
    {
        initializeWithCAR();
    }

    @isTest
    public static void getDefinedCreditProfile()
    {
        initialize();

        controller.getDefinedCreditProfile();
    }

    @isTest
    public static void runDisplayConsumerProfile()
    {
        CreditSearchConsumerProfileController.DisplayConsumerProfile profile = new CreditSearchConsumerProfileController.DisplayConsumerProfile();
        profile.key = '1';
        profile.consumerProfileId = '100';
        profile.name = 'Test';
        profile.addressLine = 'AddressLine';
        profile.city = 'City';
        profile.provinceStateCode = 'Prov';
        profile.postalCode = 'PostalCode';
        profile.countryCode = 'Country';
        profile.birthdate = null;
        profile.sin = '123';
        profile.driversLicenseNum = '123';
        profile.driversLicenseProvinceCd = 'Code';
        profile.provincialIdNum = '123';
        profile.provincialIdProvinceCd = 'Code';
        profile.passportNum = '123';
        profile.passportCountry = 'Country';
        profile.healthCardNum  = '123';

 //10/11       profile.getResidentialAddress();

        profile.isCreditInfoEmpty();
    }

    private static void initialize()
    {
        Test.setMock(WebServiceMock.class, new BusinessCreditProfileMgmtServiceMock());

        setupTestData();

        String Id = [Select Id From Credit_Profile__c Where Name='UnitTest'].Id;

        Account a = new Account(name='Tester');
        insert a;

        ApexPages.currentPage().getParameters().put('inputID', Id);
        ApexPages.currentPage().getParameters().put('inputType', 'CP');

        controller = new CreditSearchConsumerProfileController(new ApexPages.StandardController(a));
    }

    private static void initializeWithCAR()
    {
        Test.setMock(WebServiceMock.class, new BusinessCreditProfileMgmtServiceMock());

        setupTestData();

        String Id = [Select Id From Credit_Assessment__c Where First_Name__c='Unit'].Id;

        Account a = new Account(name='Tester');
        insert a;

        ApexPages.currentPage().getParameters().put('inputID', Id);
        ApexPages.currentPage().getParameters().put('inputType', 'CAR');

        controller = new CreditSearchConsumerProfileController(new ApexPages.StandardController(a));
    }

    private static void setupTestData()
    {
        Contact contact = new Contact();
        contact.FirstName = 'Unit';
        contact.LastName = 'Test';
        contact.Email = 'noreply@telus.com';

        insert contact;

        Credit_Profile__c profile = new Credit_Profile__c();

        profile.Name = 'UnitTest';
        profile.First_Name__C = 'Unit';
        profile.Last_Name__c = 'Test';
        profile.SIN__c = '220380919';
        profile.Driver_s_License__C = 'DL987321';
        profile.Drivers_License_province__c = 'BC';
        profile.Provincial_ID__c = '3333';
        profile.Provincial_ID_Province__c = 'BC';
        profile.Passport__c = '123456789';
        profile.Passport_Country__c = 'CAN';
        profile.Health_Card_No__c = '123456789';
        profile.Sole_Proprietor_Contact__c = contact.id;
        profile.Day_Telephone_Number__c = '6041111111';
        profile.Mobile__c = '6041112222';

        insert profile;

        Credit_Assessment__c car = new Credit_Assessment__c();
        car.First_Name__c = 'Unit';
        car.Last_Name__c = 'Test';
        car.Credit_Profile__c = profile.Id;
        car.SIN__c = '220380919';
        car.Driver_s_License__C = 'DL987321';
        car.Drivers_License_province__c = 'BC';
        car.Provincial_ID__c = '3333';
        car.Provincial_ID_Province__c = 'BC';
        car.Passport__c = '123456789';
        car.Passport_Country__c = 'CAN';
        car.Health_Card_No__c = '123456789';

        insert car;

    }
}