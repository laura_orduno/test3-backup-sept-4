public without sharing class StayInTouchUtil {

 /*
  * getLeadList method return the Lead List for the Lead ID
  */  
   public static List<Lead> getLeadList(Id leadId) {
    return [Select l.FirstName, l.LastName, l.Phone,l.Email,l.Street, l.City, l.State, l.Country, l.PostalCode, l.Last_Stay_in_touch_Date__c, l.GUID__c,l.IsConverted  From Lead l where l.id=:leadId ];
   }

 /*
  * getContactList method return the Contact List for the Contact ID
  */
   public static List<Contact> getContactList(Id contactId) {
    return [Select c.lastname, c.Phone, c.FirstName, c.Email , c.Last_Stay_in_touch_Date__c, c.GUID__c,c.mailingstreet, c.mailingcity, c.mailingcountry, c.mailingstate, c.mailingpostalcode From Contact c where c.id=:contactId ];
   }

    public static void updateCall(sObject obj){
        
            update obj;
        
    }
}