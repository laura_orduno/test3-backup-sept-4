/**
 * @author Ryan Draper, Traction on Demand
 * @description Generic Utility class to be used by any class requiring the contained functionality.  Goal is to reduce code
 *              duplication.
 * @created Oct 31, 2017
 */
public class PortalUtils {

	public static final String DEFAULT_ORIGIN = RestApi_MyAccountHelper_Utility.DEFAULT_ORIGIN;

	private static final String COOKIE_LANGUAGE_NAME = 'lang';
	@TestVisible private static final String COOKIE_LANGUAGE_ENG = 'en';
	@TestVisible private static final String COOKIE_LANGAUGE_FR = 'fr';
	private static final Integer COOKIE_AGE_INFINITE = -1;

	private static Map<Id, User> contactToUser = new Map<Id, User>();

	private static String userLanguage;

	/**
	 * @author Steve Doucette, Traction on Demand
	 * @date 4/4/2018
	 * @description Get the users cookie language code
	 * @return Language code of the users cookie
	 */
	public static String getUserLanguage() {
		Cookie languageCookie = ApexPages.currentPage().getCookies().get(COOKIE_LANGUAGE_NAME);
		if (languageCookie == null) {
			languageCookie = createCookie(COOKIE_LANGUAGE_ENG);
			setCookie(languageCookie);
		}
		return languageCookie.getValue();
	}

	/**
	 * @author Steve Doucette, Traction on Demand
	 * @date 4/4/2018
	 * @description Sets the language cookie
	 */
	public static void setUserLanguage() {
		Cookie languageCookie = ApexPages.currentPage().getCookies().get(COOKIE_LANGUAGE_NAME);
		if (languageCookie == null) {
			languageCookie = createCookie(COOKIE_LANGUAGE_ENG);
		} else {
			if (languageCookie.getValue() == COOKIE_LANGUAGE_ENG) {
				languageCookie = createCookie(COOKIE_LANGAUGE_FR);
			} else if (languageCookie.getValue() == COOKIE_LANGAUGE_FR) {
				languageCookie = createCookie(COOKIE_LANGUAGE_ENG);
			}
		}
		setCookie(languageCookie);
	}

	public static void setUserLanguage(String lang) {
		if (String.isBlank(lang)) {
			getUserLanguage(); // Use default flow
		} else {
			setCookie(createCookie(lang));
		}
	}

	public static void setCookie(Cookie cookie) {
		setCookies(new List<Cookie>{
				cookie
		});
	}

	public static void setCookies(List<Cookie> cookies) {
		ApexPages.currentPage().setCookies(cookies);
	}

	private static Cookie createCookie(String language) {
		return new Cookie(COOKIE_LANGUAGE_NAME, language, null, COOKIE_AGE_INFINITE, false);
	}

	/**
     *  Utility method for sending templated Case-related emails to an arbitrary list of email addresses.
     *  This method uses the transaction rollback trick so we can merge data into a SingleEmailMessage object without
     *  being forced to send to a Contact.
     *
     * @lastmodified
     *   Alex Kong (TOD), 2015-03-04
     *   Christian Wico (TOD), 2015-03-05
     */
	public static List<Messaging.SendEmailResult> sendTemplatedCaseEmails(List<String> toAddresses, Id fromEmailAddressId, Id templateId, Id caseId) {
		List<Messaging.SendEmailResult> mailResults = new List<Messaging.SendEmailResult>();
		System.Debug('Traction Debugging: In sendTemplatedCaseEmails');
		if (toAddresses.size() > 0) {

			// get a prepopulated list of email messages from Case Email Template
			List<Messaging.SingleEmailMessage> lstMsgsToSend = getMergedEmailsForCase(toAddresses, fromEmailAddressId, templateId, caseId);

			// reserve capacity
			Integer reserveCount = lstMsgsToSend.size() * toAddresses.size();
			Messaging.reserveSingleEmailCapacity(reserveCount);

			// execute mail out
			mailResults = Messaging.sendEmail(lstMsgsToSend);
		}
		return mailResults;
	}

	/**
	* Retrieves a list of email merged templates for a case
	* @lastmodified
	*       Christian Wico (TOD), 2015-03-05
	*       Alex Kong (TOD), 2015-03-09
	*/
	public static List<Messaging.SingleEmailMessage> getMergedEmailsForCase(List<String> toAddresses, Id fromEmailAddressId, Id templateId, Id caseId) {
		// setup the SingleEmailMessage as if sending to a contact

		MBR_Case_Collaborator_Settings__c collabSettings = MBR_Case_Collaborator_Settings__c.getOrgDefaults();
		Id dummyContactId = collabSettings.Dummy_Contact_id__c;

		List<Messaging.SingleEmailMessage> lstMsgs = new List<Messaging.SingleEmailMessage>();
		Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
		msg.setTemplateId(templateId);
		msg.setWhatId(caseId);
		msg.setTargetObjectId(dummyContactId);
		msg.setToAddresses(toAddresses);
		if (fromEmailAddressId != null) {
			msg.setOrgWideEmailAddressId(fromEmailAddressId);
		}

		lstMsgs.add(msg);

		// Send the emails in a transaction, then roll it back (so nothing actually goes out)
		Savepoint sp = Database.setSavepoint();
		Messaging.sendEmail(lstMsgs);
		Database.rollback(sp);

		// For each SingleEmailMessage that was just populated by the sendEmail() method, copy its
		// contents to a new SingleEmailMessage. Then send those new messages.
		List<Messaging.SingleEmailMessage> lstMsgsToSend = new List<Messaging.SingleEmailMessage>();

		for (Messaging.SingleEmailMessage email : lstMsgs) {
			Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
			emailToSend.setToAddresses(email.getToAddresses());
			emailToSend.setPlainTextBody(email.getPlainTextBody());
			emailToSend.setHTMLBody(email.getHTMLBody());
			emailToSend.setSubject(email.getSubject());
			if (fromEmailAddressId != null) {
				emailToSend.setOrgWideEmailAddressId(fromEmailAddressId);
			}
			lstMsgsToSend.add(emailToSend);
		}

		return lstMsgsToSend;
	}

	/**
	* @author Dane Peterson, Traction on Demand
	* @description Takes inbound email messages and adds them as case comments for My Account cases.
	* @param messages: a Map of Id, Email Messages that should be converted to case comments.
	**/
	public static void updateCase(Map<Id, EmailMessage> messages) {
		try {
			List<CaseComment> commentToInsert = new List<CaseComment>();
			Map<Id, List<EmailMessage>> caseToEmail = mapCaseToEmail(messages.values());
			Map<Id, Case> caseList = new Map<Id, Case>([
					SELECT Id, Status, isClosed, notifyCustomer__c, ClosedDate, CaseNumber,
							Contact.Id, Contact.Email, CreatedBy.Email, CreatedById
					FROM Case
					WHERE Id IN :caseToEmail.keySet() AND Origin = :DEFAULT_ORIGIN
			]);
			//		contactToUser = getUsers(caseList.values());
			for (Case tmpCase : caseList.values()) {
				List<EmailMessage> emailList = caseToEmail.get(tmpCase.Id);
				for (EmailMessage tmp : emailList) {
					if (tmp.TextBody.contains('ref:_')) {
						CaseComment tmpComment = createNewComment(tmp, tmpCase);

						if (tmpComment != null) {
							commentToInsert.add(tmpComment);
						}
					}
				}
			}

			insert commentToInsert;
		} catch (Exception e) {
			System.debug(e);
			insert new Webservice_Integration_Error_Log__c(
					Apex_Class_and_Method__c = 'PortalUtils.updateCase',
					Error_Code_and_Message__c = 'Error during email reply to case: ' + e.getMessage(),
					Request_Payload__c = 'Emails: ' + JSON.serializePretty(messages),
					Stack_Trace__c = e.getStackTraceString()
			);
		}
	}

	/**
	* @author Dane Peterson, Traction on Demand
	* @description Takes a single email message and adds it as comment .
	* @param EmailMessage: a single Email Message to be converted to a Case Comment
	* @param tmpCase: The cooresponding case that a case comment should be under.
	**/

	public static CaseComment createNewComment(EmailMessage em, Case tmpCase) {
		CaseComment newComment = new CaseComment(
				IsPublished = true,
				commentBody = '');
		newComment.parentId = tmpCase.id;
		newComment.commentBody = em.FromName + ': ';

		String commentBody = getCommentBody(em, newComment.commentBody.length());

		if (String.isBlank(commentBody)) {
			return null;
		}

		newComment.commentBody += commentBody;

		return newComment;
	}

	/**
	* @author Dane Peterson, Traction on Demand
	* @description
	* @param EmailMessage: a single Email Message to be converted to a Case Comment
	* @param tmpCase: The cooresponding case that a case comment should be under.
	**/
	public static String getCommentBody(EmailMessage em, Integer authorName) {
		Integer allowedSize = 2000 - authorName;
		Integer authorLength = 0;
		String commentBody = '';

		if (em != null && em.TextBody != null) {
			List<String> emailBody = em.TextBody.split('\n');
			for (String str : emailBody) {
				//if(str.containsIgnoreCase(Label.MBRReplyAbove) || str.startsWith('From:') || str.startsWith('> On') || str.contains('-Original Message-')){
				if (str.startsWith('From:') || str.startsWith('> On') || str.contains('-Original Message-')) {
					break;
				} else if (str.length() < allowedSize) {

					commentBody += str.trim() + '\n';
					allowedSize -= str.trim().length();
				} else if (commentBody.length() == authorLength) {
					commentBody += str.substring(0, allowedSize).trim();
					break;
				}
			}
		}
		return commentBody;
	}

	public static Map<Id, List<EmailMessage>> mapCaseToEmail(List<EmailMessage> emails) {

		Map<Id, List<EmailMessage>> caseToEmail = new Map<Id, List<EmailMessage>>();

		for (EmailMessage em : emails) {
			if (em.ParentId.getSObjectType() == Case.getSObjectType() && em.Incoming && em.Subject != null) {
				if (!caseToEmail.containsKey(em.ParentId)) {
					caseToEmail.put(em.ParentId, new List<EmailMessage>());
				}
				caseToEmail.get(em.ParentId).add(em);
			}
		}

		return caseToEmail;
	}

	/*public static Map<Id, User> getUsers(List<Case> caseList) {
		Map<Id, User> ctToUser = new Map<Id, User>();
		Set<Id> contactIds = new Set<Id>{
		};
		if (caseList != null) {
			for (Case c : caseList) {
				contactIds.add(c.ContactId);
			}
		}
		List<User> userList = [
				SELECT Id, Email, Name, ContactId
				FROM User
				WHERE ContactId IN :contactIds
		];
		if (userList != null) {
			for (User ct : userList) {
				ctToUser.put(ct.ContactId, ct);
			}
		}

		return ctToUser;
	}*/

	public static Boolean validateEmail(String email) {
		Boolean res = true;
		String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$'; // source: <a href="http://www.regular-expressions.info/email.html" target="_blank" rel="nofollow">http://www.regular-expressions.info/email.html</a>
		Pattern MyPattern = Pattern.compile(emailRegex);
		Matcher MyMatcher = MyPattern.matcher(email);

		if (!MyMatcher.matches()) {
			res = false;
		}

		return res;
	}
}