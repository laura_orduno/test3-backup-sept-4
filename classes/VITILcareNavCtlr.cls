public class VITILcareNavCtlr {
 String userAccountId;
    public string CatListPull {get;set;}
     
    Public String CPRole {get; set;}
     public VITILcareNavCtlr() {
         
           userAccountId = [SELECT AccountId FROM User WHERE Id = :UserInfo.getUserId()].AccountId;
          List<User> uaccount = [SELECT id, Customer_Portal_Role__c FROM User WHERE Id = :UserInfo.getUserId()];
       CPRole = uaccount[0].Customer_Portal_Role__c;
        system.debug('userAccountId: ' + userAccountId);
       List<Account_Configuration__c> AccountConfig = [SELECT Id, Zenoss_URL__c  FROM Account_Configuration__c WHERE Account__c =:userAccountId];
     if(AccountConfig.size() > 0){
         system.debug('AccountConfig[0].AccountConfig: ' + AccountConfig);
        system.debug('AccountConfig[0].Zenoss_URL__c: ' + AccountConfig[0].Zenoss_URL__c);
       
        CatListPull = AccountConfig[0].Zenoss_URL__c;
        system.debug('CatListPull: ' + CatListPull);
     }
     }
}