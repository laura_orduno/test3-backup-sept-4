/**
* Description of the purpose of the class:This is a common data object which would store all the service address attributes.
  All these attributes will have its getter and setter methods defined in it.  
* Author: Aditya Jamwal (IBM) / Sandip Chaudhari
* Date: 01-oct-2015
* Project/Release: Predictive Address Capture
* Change Control Information:
* Date Who Reason for the change
*/
public class ServiceAddressData {
    
    public ServiceAddressData (){
        isManualCapture = false;
        isTempJobPhone = false;
        isNewConstruction = false;
        isILEC = false;
        isSACG = true;
        isASFDown = false; //31 March 2017 - Defect # 11733 - Added by Sandip
    }
    public string searchString{get;set;}
    public string searchCriteria{get;set;}
    public string addrStatus{get;set;} // Status after checking with ASF Service for existing address
    public string captureNewAddrStatus{get;set;} // Status update to 'Valid' when checked 'Capture New Address' otherwise 'Invalid'
    public Boolean isManualCapture { get; set; }
    public Boolean isSearchStringRequired { get; set; }
     
    public String fmsId { get; set; }
    public Boolean isFmsIdRequired { get; set; }
    
    public String address{ get; set; }
    public Boolean isAdressRequired { get; set; }
    
    public String country { get; set; }
    public Boolean isCountryRequired { get; set; }

    public String utm { get; set; }
    public Boolean isUtmRequired { get; set; }
    
    public String gpsCordinate { get; set; }
    public Boolean isGpsCordinateRequired { get; set; }

    public String state { get; set; }
    public Boolean isStateRequired { get; set; }
    
    public String numb { get; set; }
    public Boolean isNumbRequired { get; set; }
    
    public String type { get; set; }
    public Boolean isTypeRequired { get; set; }

    public String subdivision { get; set; }
    public Boolean isSubdivisionRequired { get; set; }
    
    public String meridian { get; set; }
    public Boolean isMeridianRequired { get; set; }
    
    public String range { get; set; }
    public Boolean isRangeRequired { get; set; }

    public String township { get; set; }
    public Boolean isTownshipRequired { get; set; }

    public String section { get; set; }
    public Boolean isSectionRequired { get; set; }
    
    public String quarter { get; set; } 
    public Boolean isQuarterRequired { get; set; }
    
    public String plan { get; set; }
    public Boolean isPlanRequired { get; set; }

    public String block { get; set; }
    public Boolean isBlockRequired { get; set; }

    public String lot { get; set; }
    public Boolean isLotRequired { get; set; }

    public String googleMapLink { get; set; }
    public Boolean isGoogleMapLinkRequired { get; set; }

    public String streetDirection { get; set; }
    public Boolean isStreetDirectionRequired { get; set; }
    
    public String postalCode { get; set; }
    public Boolean isPostalCodeRequired { get; set; }
    
    public String province { get; set; }
    public Boolean isProvinceRequired { get; set; }
    
    public String canadaProvince { get; set; }
    public String usState { get; set; }
      
    public String street { get; set; }
    public Boolean isStreetRequired { get; set; }
    
    public String streetNumber { get; set; }
    public Boolean isStreetNumberRequired { get; set; }
    
    public String streetName { get; set; }
    public Boolean isStreetNameRequired { get; set; }
    
    public String city { get; set; }
    public Boolean isCityRequired { get; set; }

    public String suiteNumber { get; set; }
    public Boolean isSuiteNumberRequired { get; set; }
    
    public String buildingNumber { get; set; }
    public Boolean isBuildingNumberRequired { get; set; }
    
    public String vector { get; set; }
    public Boolean isVectorRequired { get; set; }
    
    public String aptNumber { get; set; }
    public Boolean isAptNumberRequired { get; set; }
    //public String serviceRequestId {get;set;}
    
    // PAC R2 - missing mappings - 01 March 2016
    public String houseSuffix{ get; set; }
    public String actionType{ get; set; }
    public String suiteSorting{ get; set; }
    public String reSrcCode{ get; set; }
    //End PAC R2
    
    //Sandip: added below to store addtional data returned by FMS
    public String clliCode { get; set; }
    public String coid { get; set; }
    public String localRoutingNumber { get; set; }    
    public String ratingNpaNxx { get; set; }
    public String switchNumber { get; set; }
    public String terminalNumber { get; set; }
    public String npa { get; set; }
    public String lowestNxx { get; set; }
    
    // additional details return by FMS/ASF Service added in PAC R2
    public String civicNumber { get; set; }
    public String civicNumberSuffix { get; set; }
    public String canadaPostBuildName { get; set; }
    public String canadaPostLocnName { get; set; }
    public String canadaPostRecordType { get; set; }
    public String careOf { get; set; }
    public String ruralRouteNumber { get; set; }
    public String ruralRouteTypeCode { get; set; }
    public String stationName { get; set; }
    public String stationQualifier { get; set; }
    public String stationTypeCode { get; set; }
    public String fleetMailOfficeName { get; set; }
    public String hmcsName { get; set; }
    public String xCoordinate { get; set; }
    public String yCoordinate { get; set; }
    public String ILS { get; set; }
    public String vector_fms { get; set; }
    public String addressId { get; set; }
    public String addressTypeCode { get; set; }
    public String[] additionalAddressInformation { get; set; }
    public String[] renderedAddress { get; set; }
    public String addressAssignmentId { get; set; }
    public String addressAssignmentSubTypeCode { get; set; }
    public String addressAssignmentTypeCode { get; set; }
    public String addressMatchingStatusCode { get; set; }
    public String addressSearchText { get; set; }
    public String countryCode { get; set; }
    public String emailAddressText { get; set; }
    public String externalAddressId { get; set; }
    public String externalAddressSourceId { get; set; }
    public String externalServiceAddressId { get; set; }
    public String mailingTypeCode { get; set; }
    public String municipalityName { get; set; }
    public String postOfficeBoxNumber { get; set; }
    public String postalZipCode { get; set; }
    public String provinceStateCode { get; set; }
    public String relateAddressAssignmentId { get; set; }
    public String streetDirectionCode { get; set; }
    public String streetName_fms { get; set; }
    public String streetNumber_fms { get; set; }
    public String streetTypeCode { get; set; }
    public String unitNumber { get; set; }
    public String validateAddressIndicator { get; set; }
    public String unitTypeCode { get; set; }
    public String municipalityClli { get; set; }
    public String prewireDate { get; set; }
    public String enterPhone { get; set; }
    public String jurisdiction { get; set; }
    public String baseRateAreaCode { get; set; }
    public String baseRateAreaMileage { get; set; }
    public String transmissionZone { get; set; }
    public String nnxCodeSpecial { get; set; }
    public String rateCenter { get; set; }
    public String commentLine1 { get; set; }
    public String commentLine2 { get; set; }
    public String switchType { get; set; }
    public String switchId { get; set; }
    public String terminalCode { get; set; }
    public String serviceCount { get; set; }
    public String serviceAddressIndividualLineServiceCode { get; set; }
    public String portabilityCode { get; set; }
    public String rateCentreRemarks { get; set; }
    public Boolean blockAddressInd { get; set; }
    public String legalLandDescription { get; set; }
    public String streetNumberSuffix { get; set; }
    // End PAc R2 additional details from FMS 
    
    // Added below as per PAC R2 for SACG
    public String locationId { get; set; }
    public String SACGType { get; set; } // radio field value
    public String notesForSACGTeam { get; set; }
    public Id accountId { get; set; }
    public Id opportunityId { get; set; }
    public Id contactId { get; set; }
    public Boolean isSACG{ get; set; }
    public Boolean isTempJobPhone{ get; set; }
    public Boolean isNewConstruction{ get; set; }
    public String SACGAddress{ get; set; }
    public String caseId{ get; set; }
    public String caseNumber{ get; set; }
    public String returnMessage{ get; set; }
    public String fullCaseURL { get; set; }
    public String sacgStatus { get; set; } // used in PACUtil class
    public String onLoadSearchAddress{get;set;} // used to prepopulate in search box for injecting address from outer context
        
    // End PAC R2
    
    //Added by Arvind as part of ServiceAddressManagement Service V1.1 (US-OCOM-1101)
    
    public String dropFacilityCode { get; set; }
    public String dropTypeCode { get; set; }
    public String dropLength { get; set; }
    public String dropExceptionCode { get; set; }
    public String currentTransportTypeCode { get; set; }
    public String futureTransportTypeCode { get; set; }
    public String futurePlantReadyDate { get; set; }
    public String futureTransportRemarkTypeCode { get; set; }
    
    public String gponBuildTypeCode { get; set; }
    public String provisioningSystemCode { get; set; }
    
    public Boolean futureASfCall { get; set; }
    //
    
    // Added by Aditya Jamwal as a part of User Story OCOM-1214 (PAC Additional LPDS Fields)
    public String municipalityNameLPDS { get; set; }
    public String civicNumberPrefix { get; set; }
    public String streetTypePrefix { get; set; }
    public String streetTypeSuffix { get; set; }
    //**********************************************// 

    public Boolean isILEC { get; set; }
    public Boolean isASFDown {get;set;} // 31 March 2017 - Defect # 11733 - Added by Sandip
}