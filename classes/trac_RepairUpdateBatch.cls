/*
 * Batch class for mass update of Repair cases. Scheduled with trac_RepairUpdateSched
 * Part of the SMB Repairs project
 *
 * @author: Grant Adamson, Traction On Demand 
 */

global class trac_RepairUpdateBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {
	private static final Integer BATCH_SIZE = 9;
	
	private Boolean failures;
	
	
	global trac_RepairUpdateBatch() {
		failures = false;
	}
	
	
	public static void run() {
		Database.executeBatch(new trac_RepairUpdateBatch(),BATCH_SIZE);
	}
	
	
	global Database.QueryLocator start(Database.BatchableContext bc) {
		String query =
			'SELECT Repair__c, SRD_Repair_Status_Id__c ' +
			'FROM Case ' +
			'WHERE isClosed = false ' +
			'AND repair__c != null';

		return Database.getQueryLocator(query);
	}


	global void execute(Database.BatchableContext bc, List<sObject> scope) {
		List<Case> activeCases = (List<Case>)scope;
		
		Map<String,String> repairStatusMap;
		
		try {
			repairStatusMap = getRepairStatusMap(activeCases);
		} catch (Exception e) {
			failures = true;
			return;
		}
		
		List<Case> casesToUpdate = findCasesToUpdate(activeCases, repairStatusMap);
		
		updateCases(casesToUpdate);
	}
	
	
	global void finish(Database.BatchableContext bc) {
		if(failures) {
			// future automated email alert functionality
		}
	}
	
	
	private static Map<String,String> getRepairStatusMap(List<Case> activeCases) {
		Set<String> repairIds = new Set<String>();
		for (Case c : activeCases) {
			repairIds.add(c.repair__c);
		}

		return trac_RepairCallout.getRepairStatus(repairIds);
	}
	
	
	private static List<Case> findCasesToUpdate(List<Case> activeCases, Map<String,String> repairStatusMap) {
		List<Case> casesToUpdate = new List<Case>();
		for(Case c : activeCases) {
			if(repairStatusMap.containsKey(c.repair__c) &&
			   repairStatusMap.get(c.repair__c) != c.SRD_Repair_Status_Id__c) {
				casesToUpdate.add(c);
			}
		}
		
		return casesToUpdate;
	}
	
	
	private static void updateCases(List<Case> cases) {
		List<Case> updatedCases = new List<Case>();
		for(Case theCase : cases) {
			trac_RepairCalloutResponse resp = trac_RepairCallout.getRepair(theCase);
			
			if(resp.success && !theCase.Repair_Case_Id_Mismatch__c) {
				updatedCases.add(theCase);
			} 
		}
		
		if(updatedCases.size() > 0) {
			update updatedCases;
		}
	}
	
}