/* 
        OrderStatusUpdates_WS
        Web Service Class
        -------------------------------------------------------------------
        Author          |   Date        |   Description
        -------------------------------------------------------------------
        Aditya Jamwal      15-July-2014   CRDB System will invoke this class for pushing Condition updates for Legacy order attached to Service request.

    */
    global class OrderStatusUpdates_WS {
        
        public static Map <Id, String> suffixMap;
        public static OrderStatusUpdates_ComplexTypes.Response GenerateSuccess(){
            OrderStatusUpdates_ComplexTypes.Response resp = new OrderStatusUpdates_ComplexTypes.Response();
            OrderStatusUpdates_ComplexTypes.SuccessResponse s = new OrderStatusUpdates_ComplexTypes.SuccessResponse();
            s.Message = 'Success';
            resp.SuccessCode = s;
            return resp;
        }
    
        public static OrderStatusUpdates_ComplexTypes.Response GenerateError(String msg){
            OrderStatusUpdates_ComplexTypes.Response resp = new OrderStatusUpdates_ComplexTypes.Response();
            OrderStatusUpdates_ComplexTypes.FaultResponse f = new OrderStatusUpdates_ComplexTypes.FaultResponse();
            f.Message = msg;
            resp.FaultCode = f;
            return resp;
        }
    
        webservice static OrderStatusUpdates_ComplexTypes.Response UpdateOrderStatus(OrderStatusUpdates_ComplexTypes.OrderStatusUpdate orderUpdate){
            // Single Order per call
            system.debug('~~~~~~~~~~~~~~~~~~Total Incoming Orders:' + orderUpdate.LineItemList.size());
            return ProcessOrderStatus(orderUpdate);
        }
        
      
       public static OrderStatusUpdates_ComplexTypes.Response ProcessOrderStatus(OrderStatusUpdates_ComplexTypes.OrderStatusUpdate orderUpdate){
        
        // Our logic here
       SRS_CRDB_ConditionCal Cal =new SRS_CRDB_ConditionCal();
       system.debug('~~~~~~~~~Before making call total orders in the list: ' + orderUpdate.LineItemList.size());
       String Result=Cal.PMOProcessing(orderUpdate.LineItemList);
       OrderStatusUpdates_ComplexTypes.Response Res = new OrderStatusUpdates_ComplexTypes.Response();
        OrderStatusUpdates_ComplexTypes.SuccessResponse success  = new OrderStatusUpdates_ComplexTypes.SuccessResponse(); 
         if(Result.contains('Ok'))
         {
         success.message='Order Updated Successfully';
         Res.SuccessCode=success;
         }
        else
        {
         success.message='Order Update Failed: ' + Result; 
         Res.SuccessCode=success; 
         }
        
        return Res;
        
        
        }

   
    }