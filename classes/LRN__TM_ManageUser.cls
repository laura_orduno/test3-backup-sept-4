/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class TM_ManageUser {
    @RemoteAction
    global static LRN.TM_ManageUser.ManageUserWrapper getEditedUser(String userId) {
        return null;
    }
    @RemoteAction
    global static void saveEditedUser(String userId, Boolean canAssign, Boolean canPublish, Boolean canEdit, Boolean canShare, Boolean canAccessManagerDashboard, Boolean canCreateTags, Boolean canShareExternally, Boolean canShareInternally, Boolean canShareWithRoles, Boolean canShareWithUsers, Boolean canRemoveSharing, Boolean notifyWeekly, Boolean notifyMonthly, Boolean neverNotify) {

    }
global class ManageUserWrapper {
}
}
