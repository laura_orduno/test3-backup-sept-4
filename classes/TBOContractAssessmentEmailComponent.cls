/*
    ###########################################################################
    # File..................: TBOContractAssessmentEmailComponent
    # Version...............: 1
    # Created by............: Umesh Atry (TechM)
    # Created Date..........: 25/07/2016 
    # Last Modified by......: Umesh Atry (TechM)
    # Last Modified Date....: 25/07/2016
    # Description...........: Class is to populate Contract Assessment records based on completed Case.
    #
    ############################################################################
    */

public class TBOContractAssessmentEmailComponent{
        public Id tboID {get;set;}
        public String fullRecordURL {get;set;}
        
        public String baseURL {get;set;}
        public String baseRef {get;set;}
        public String basePath {get;set;}
        public String CurrentRequestUrl {get;set;}
        
        public TBOContractAssessmentEmailComponent(){
                        
        }   
    
        
        public List<Contract_Assessment__c> ContractAssessmentList {
            get{
             if(tboID!= null){
             fullRecordURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + tboID;
             
              return [Select id,Name,Impacted_Contract_s__c,Comments__c from Contract_Assessment__c  where TBO_Request_Case__c =:tboID limit 1000];
                        
            }else return null;
            }set;
        }
       
            
}