/**
 * S2C_WRHistoryOPCtlr_Test
 * @description Test class for S2C_WRHistoryOrderProvisioningCtlr
 * @author Thomas Tran, Traction on Demand
 * @date 09-03-2015
 */
@isTest(seeAllData=false)
private class S2C_WRHistoryOPCtlr_Test {
	public static final Id PREQUEL_RT = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Prequal').getRecordTypeId();
	
	@isTest static void retrieveHistory() {
		PageReference pageRef = Page.S2C_WRHistoryOrderProvisioning;
		Test.setCurrentPage(pageRef);

		Product2 newProduct1 = S2C_TestUtility.createProduct('Test Product', 'Test');
		insert newProduct1;

		SRS_PODS_Product__c newSRSPodsProduct1 = S2C_TestUtility.createSRSPODSProduct(newProduct1.Product_Family__c);

		insert newSRSPodsProduct1;
		Contact newContact = new Contact(
			FirstName = 'Test 001',
			LastName = 'Test 001'
		);

		insert newContact;

		Account newAccount = new Account(Name = 'Account Test 001');
		insert newAccount;

		Opportunity newOpportunity = new Opportunity(
			Name = 'Opp 001',
			AccountId = newAccount.Id,
			Primary_Order_Contact__c = newContact.Id,
			StageName = 'Open',
			CloseDate = Date.today()
		);
		insert newOpportunity;

		Service_Request__c newSR = new Service_Request__c(
			Opportunity__c = newOpportunity.Id,
			Account_Name__c = newAccount.Id,
			RecordTypeId = PREQUEL_RT,
			PrimaryContact__c = newContact.Id
		);
		insert newSR;

		Service_Request_Contact__c newSRC = new Service_Request_Contact__c(
			Service_Request__c = newSR.Id,
			Contact_Type__c = 'Customer Onsite',
			Contact__c = newSR.PrimaryContact__c
		);
		insert newSRC;

		SRS_Service_Request_Charge__c newCharge = new SRS_Service_Request_Charge__c(
			Service_Request__c = newSR.Id
		);
		insert newCharge;

		SRS_Service_Address__c newAddy = new SRS_Service_Address__c(
			Service_Request__c = newSR.Id
		);
		insert newAddy;

		Work_Request__c newWR = new Work_Request__c(
			Service_Request__c = newSR.Id
		);

		insert newWR;

		ApexPages.StandardController sc = new ApexPages.StandardController(newWR);
		S2C_WRHistoryOrderProvisioningCtlr ctlr = new S2C_WRHistoryOrderProvisioningCtlr(sc);
		Boolean realIdTrue = ctlr.checkIfRealId(newWR.Id);
		Boolean realIdFalse = ctlr.checkIfRealId('asdasd');

		Test.startTest();

		List<S2C_WRHistoryOrderProvisioningCtlr.SelectHistory> selectHistoryList = ctlr.getSelectHistories();

		Test.stopTest();

		System.assertEquals(true, realIdTrue);
		System.assertEquals(false, realIdFalse);
		System.assert(!selectHistoryList.isEmpty());

	}
	
}