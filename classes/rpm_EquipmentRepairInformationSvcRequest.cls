public class rpm_EquipmentRepairInformationSvcRequest {
    public class RepairStatusById {
        public String repairID;
        private String[] repairID_type_info = new String[]{'repairID','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/srv/RMO/LifeCycleMgmt/EquipmentRepairInformationSvcRequestResponse_v3','false','false'};
        private String[] field_order_type_info = new String[]{'repairID'};
        
        // copied from rpm_EquipmentRepairTypes_v1.RepairStatus - SFDC cannot understand inheritance when using WSDL classes
        public String repairStatusID;
        public rpm_EnterpriseCommonTypes_v7.MultilingualCodeDescTextList repairStatusDescription;
        private String[] repairStatusID_type_info = new String[]{'repairStatusID','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] repairStatusDescription_type_info = new String[]{'repairStatusDescription','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v7','MultilingualCodeDescTextList','0','1','false'};
        //private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Resource/Resource/EquipmentRepairTypes_v1','false','false'};
        //private String[] field_order_type_info = new String[]{'repairStatusID','repairStatusDescription'};
    }
    public class validateRepair_element {
        public String productId;
        public String serialNumber;
        public String equipmentTypeId;
        public String equipmentTrackingTypeId;
        public String userTypeId;
        public String productBrandId;
        private String[] productId_type_info = new String[]{'productId','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] serialNumber_type_info = new String[]{'serialNumber','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] equipmentTypeId_type_info = new String[]{'equipmentTypeId','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] equipmentTrackingTypeId_type_info = new String[]{'equipmentTrackingTypeId','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] userTypeId_type_info = new String[]{'userTypeId','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] productBrandId_type_info = new String[]{'productBrandId','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/srv/RMO/LifeCycleMgmt/EquipmentRepairInformationSvcRequestResponse_v3','false','false'};
        private String[] field_order_type_info = new String[]{'productId','serialNumber','equipmentTypeId','equipmentTrackingTypeId','userTypeId','productBrandId'};
    }
    public class getRepairStatusResponse_element {
        public rpm_EquipmentRepairInformationSvcRequest.RepairStatusList repairStatusList;
        private String[] repairStatusList_type_info = new String[]{'repairStatusList','http://xmlschema.tmi.telus.com/srv/RMO/LifeCycleMgmt/EquipmentRepairInformationSvcRequestResponse_v3','RepairStatusList','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/srv/RMO/LifeCycleMgmt/EquipmentRepairInformationSvcRequestResponse_v3','false','false'};
        private String[] field_order_type_info = new String[]{'repairStatusList'};
    }
    public class getRepairStatus_element {
        public rpm_EquipmentRepairInformationSvcRequest.RepairIdList repairIdList;
        private String[] repairIdList_type_info = new String[]{'repairIdList','http://xmlschema.tmi.telus.com/srv/RMO/LifeCycleMgmt/EquipmentRepairInformationSvcRequestResponse_v3','RepairIdList','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/srv/RMO/LifeCycleMgmt/EquipmentRepairInformationSvcRequestResponse_v3','false','false'};
        private String[] field_order_type_info = new String[]{'repairIdList'};
    }
    public class RepairIdList {
        public String[] repairID;
        private String[] repairID_type_info = new String[]{'repairID','http://www.w3.org/2001/XMLSchema','string','0','100','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/srv/RMO/LifeCycleMgmt/EquipmentRepairInformationSvcRequestResponse_v3','false','false'};
        private String[] field_order_type_info = new String[]{'repairID'};
    }
    public class validateRepairResponse_element {
        public Integer statusCode;
        public rpm_EnterpriseCommonTypes_v7.MultilingualCodeDescTextList statusMessage;
        private String[] statusCode_type_info = new String[]{'statusCode','http://www.w3.org/2001/XMLSchema','int','1','1','false'};
        private String[] statusMessage_type_info = new String[]{'statusMessage','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v7','MultilingualCodeDescTextList','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/srv/RMO/LifeCycleMgmt/EquipmentRepairInformationSvcRequestResponse_v3','false','false'};
        private String[] field_order_type_info = new String[]{'statusCode','statusMessage'};
    }
    public class RepairStatusList {
        public rpm_EquipmentRepairInformationSvcRequest.RepairStatusById[] repairStatusById;
        private String[] repairStatusById_type_info = new String[]{'repairStatusById','http://xmlschema.tmi.telus.com/srv/RMO/LifeCycleMgmt/EquipmentRepairInformationSvcRequestResponse_v3','RepairStatusById','0','100','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/srv/RMO/LifeCycleMgmt/EquipmentRepairInformationSvcRequestResponse_v3','false','false'};
        private String[] field_order_type_info = new String[]{'repairStatusById'};
    }
    public class getRepair_element {
        public String repairId;
        public String languageCode;
        private String[] repairId_type_info = new String[]{'repairId','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] languageCode_type_info = new String[]{'languageCode','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/srv/RMO/LifeCycleMgmt/EquipmentRepairInformationSvcRequestResponse_v3','false','false'};
        private String[] field_order_type_info = new String[]{'repairId','languageCode'};
    }
    public class getRepairResponse_element {
        public rpm_EquipmentRepairTypes_v1.Repair repair;
        private String[] repair_type_info = new String[]{'repair','http://xmlschema.tmi.telus.com/xsd/Resource/Resource/EquipmentRepairTypes_v1','Repair','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/srv/RMO/LifeCycleMgmt/EquipmentRepairInformationSvcRequestResponse_v3','false','false'};
        private String[] field_order_type_info = new String[]{'repair'};
        
        // copied from rpm_EnterpriseCommonTypes_v7.ResponseMessage - SFDC cannot understand inheritance when using WSDL classes
        public DateTime dateTimeStamp;
        public String errorCode;
        public String messageType;
        public String transactionId;
        public rpm_EnterpriseCommonTypes_v7.Message[] messageList;
        public String contextData;
        private String[] dateTimeStamp_type_info = new String[]{'dateTimeStamp','http://www.w3.org/2001/XMLSchema','dateTime','1','1','false'};
        private String[] errorCode_type_info = new String[]{'errorCode','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] messageType_type_info = new String[]{'messageType','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] transactionId_type_info = new String[]{'transactionId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] messageList_type_info = new String[]{'messageList','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v7','Message','0','10','false'};
        private String[] contextData_type_info = new String[]{'contextData','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        //private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v7','true','false'};
        //private String[] field_order_type_info = new String[]{'dateTimeStamp','errorCode','messageType','transactionId','messageList','contextData'};
        
    }
    
}