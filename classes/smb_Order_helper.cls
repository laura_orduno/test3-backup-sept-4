public class smb_Order_helper{
public list<string> lststrQuotes= new list<string>();
public list<string> lststrOrders= new list<string>();
public map<id,List<VlocityRemarks__c>> mapQuoteRemarksRemarks = new map<id,List<VlocityRemarks__c>>();
public List<VlocityRemarks__c> UpdateVlocityRemarks = new  List<VlocityRemarks__c>();


public smb_Order_helper(){
}

public void intake(list<Order> triggernew,string Eventname){
    if(Eventname=='AFTERINSERT'){
        for(order ord:triggernew){
            lststrQuotes.add(ord.vlocity_cmt__QuoteId__c);
            lststrOrders.add(ord.id);            
           }
        system.debug('LIST OF REMARKS'+getRemarks(lststrQuotes));
        system.debug('LIST OF triggernew'+triggernew);
        mapQuoteRemarksRemarks=getRemarks(lststrQuotes);
        for(order objOrd:triggernew){
            if(mapQuoteRemarksRemarks.containsKey(objOrd.vlocity_cmt__QuoteId__c))
                for(VlocityRemarks__c vrem:mapQuoteRemarksRemarks.get(objOrd.vlocity_cmt__QuoteId__c)){
                    VlocityRemarks__c vremClone=vrem.clone(false,false,false,false);
                    vremClone.Order_Number__c=objOrd.id;
                    vremClone.Quote_Number__c=null;
                    UpdateVlocityRemarks.add(vremClone);
                }                
        }
        DML_Operataions();
    }
}

public static map<id,List<VlocityRemarks__c>> getRemarks(List<String> lstQuotes){
    map<id,List<VlocityRemarks__c>> mapQuoteIDRemarks= new map<id,List<VlocityRemarks__c>>();

   List<VlocityRemarks__c> lstVlocityRemarks= [select id,Quote_Number__c,Remark_Type__c,Order_Number__c,Remarks__c from VlocityRemarks__c where Quote_Number__c in:lstQuotes];
   

    for(String quot:lstQuotes){
        List<VlocityRemarks__c> lstRemarks= new  List<VlocityRemarks__c>();
          for(VlocityRemarks__c vlocityRemark:lstVlocityRemarks){
           if( vlocityRemark.Quote_Number__c==quot){
               lstRemarks.add(vlocityRemark);
            }
            if(!mapQuoteIDRemarks.containsKey(quot))
                mapQuoteIDRemarks.put(quot,lstRemarks);
           
        }
    }
    
 
    return mapQuoteIDRemarks;
}

public void DML_Operataions(){
    system.debug('UpdateVlocityRemarks'+UpdateVlocityRemarks);
    if(UpdateVlocityRemarks.size()>0)
        insert UpdateVlocityRemarks;
    
}

public void statusChange(list<Order> triggernew){
    for(Order ord: triggernew){
            if(ord.status== 'Draft'){ 
                ord.status= 'Not Submitted';
                }
        } 

    } 
}