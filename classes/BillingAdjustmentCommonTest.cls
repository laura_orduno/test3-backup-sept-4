@isTest
public class BillingAdjustmentCommonTest{

    public static Integer accRoleSeq = 1;
    public static List<RecordType> accountRecordTypes = [select Id, Name from RecordType where SObjectType = 'Account'];
    public static List<RecordType> baRecordTypes = [select Id, Name from RecordType where SObjectType = 'Billing_Adjustment__C'];
    private static Account acc;
    
    public static List<User> sampleUser = [Select id from user limit 2];
    public static Map<String,User> sampleUserMap;
    public static Map<String,id> recordTypeMap = new Map<String,id>();
    static {
        for(RecordType bart : baRecordTypes){
            recordTypeMap.put(bart.Name, bart.id);
        }
        for(RecordType art : accountRecordTypes){
            recordTypeMap.put(art.Name, art.id);
        }
        createSampleUserHierarchy();
     }
   /*
    ********************* Test Helper Mehtods *******************************************
    */
    
    /*
    * Create Basic Parent Account
    */
    public static Account createParentAccount() { 
        Account parentAccount = new Account(Name = 'Test RCID Account-Parent');
        parentAccount.Phone='6047894561';
        parentAccount.RecordTypeId = recordTypeMap.get('RCID');
        insert parentAccount;
        return parentAccount;
    }
   /*
    * Create Basic Account (Which has a Parent Account)
    */
    public static Account createAccount() { 
        Account parentAccout = createParentAccount();
        return createAccount(parentAccout.Id);
    }
    
   /*
    * Create Basic Account for a given parent account
    */    
    public static Account createAccount(Id parentAccoutId) { 
        Account account = new Account(Name = 'Test CAN Account');
        account.RecordTypeId = recordTypeMap.get('CAN');
        account.Phone='6047897777';
        account.parentId = parentAccoutId;
        insert account;
        return account;
    }
    
    /**
     * Create Basic Contact
     */
    public static Contact createContact(id accountId){
        Contact contact = new Contact();
        contact.AccountId = accountId;
        contact.FirstName ='Tin';
        contact.LastName ='Tin';
        insert contact;
        return contact;
    }
    /*
    * Create Account with Salse Assignment(Assign Salse Dir to the account)
    * Return  account
    */
    public static Account createAccountWithSalseAssignments() { 
        Integer pre = Integer.valueOf(Math.random()*100000000);
        if(acc==null){
         acc = createAccount();
        }
        Sales_Assignment__c saDir = createSalesAssignment(acc,'DIR');
        Sales_Assignment__c saVP = createSalesAssignment(acc,'MD');
        Sales_Assignment__c saAP = createSalesAssignment(acc,'ACCTPRIME');
        
        
        List<Sales_Assignment__c> saList = new List<Sales_Assignment__c>{saDir,saVP,saAP};
        insert saList;
        return acc;
    }
    /**
     * Crate Salse Assignment contact
     * @param acc Account
     * @param role String 
     * return salses Assignment
     */
    public static Sales_Assignment__c createSalesAssignment(Account acnt, String role){
        Integer pre = Integer.valueOf(Math.random()*100000000);
        Sales_Assignment__c sa = new Sales_Assignment__c();
        sa.Account__c = acnt.ParentId;
        sa.User__c = sampleUserMap.get('sa'+role).id ;
        sa.Role__c= role;
        sa.AcctRole__c =pre+ accRoleSeq +'-' + role;
        sa.Name=pre+ accRoleSeq +'-' + role;
        return sa;
    }
    /**
     * Create billing adjustment with Recordtype Credit and Business Unit Enterprise
     * @param amount Decimal
     * return Billing Adjstment 
     */
    
    public static Billing_Adjustment__c createBillingAdjustment(Decimal total,String recordType, String businessUnit){
       Billing_Adjustment__c ba = getBillingAdjustment(total,recordType,businessUnit,sampleUserMap.get('agent').id);
       insert ba;
       return ba; 
    }
    
     public static Billing_Adjustment__c createBillingAdjustment(Decimal total,String recordType, String businessUnit, User creator){
       Billing_Adjustment__c ba = getBillingAdjustment(total,recordType,businessUnit,creator.id);
       insert ba;
       return ba; 
    }
    
    public static Billing_Adjustment__c getBillingAdjustment(Decimal amount,String recordType, String businessUnit){
            return getBillingAdjustment(amount,recordType,businessUnit,sampleUserMap.get('agent').id);
    }
    
    
    public static Billing_Adjustment__c getBillingAdjustment(Decimal amount,String recordType, String businessUnit, Id creator){
       createReasonCodeData(); 
       Billing_Adjustment__c ba = new Billing_Adjustment__c();
       ba.Account__c = createAccountWithSalseAssignments().id;
       ba.Contact__c  = createContact(ba.Account__c).id;
       
       ba.RecordType= new RecordType(id=getRecordTypeId('Billing_Adjustment__c',recordType), Name=recordType);
       ba.RecordTypeId = getRecordTypeId('Billing_Adjustment__c',recordType);
       ba.Business_Unit__c = businessUnit;
       ba.Amount__c = amount;
       ba.Adjustment_Date_From__c = Date.Today();
       ba.Adjustment_Date_To__c = Date.Today();
       ba.Product_Type__c = 'Wireline';
       ba.RC_Level_1__c = 'Bill Corrections';
       ba.RC_Level_2__c = 'Billing Error';
       ba.RC_Level_3__c = 'Billing Error';
       ba.RC_Product__c= 'Internet';
       ba.RC_Responsible_Area__c= 'Billing Systems';
        
       ba.Calculation_Accuracy__c ='Exact'; 
       ba.Calculation_Reason_Description__c= 'aaa'; 
       ba.Billing_Team__c ='AS400 Adjustments';
       ba.Original_RecordType__c=recordType;
       ba.ownerId = creator;
       ba.Required_Release_Code__c ='3'; 
       ba.Approver_Release_Code__c ='1'; 
       return ba;
        
    }
    
    public static User getAgentUser(){
      return sampleUserMap.get('agent');
    }
    
    public static User getAgentManagerUser(){
      return sampleUserMap.get('manager');
       
    }
    
    public static User getManager(User u){
       return sampleUserMap.get('manager');
    }
    
    public static User getUser(id uid){
        User u = [SELECT Id, Name, Release_code__c FROM user where Id = :uid];
        return u;
    }
    
    public static User getUserByName(String name){
      return [SELECT Id, Name, managerid, Release_code__c FROM user where Name =:name];
    }
   
    public static void createReleaseCodeThresholdData(String transactionType,String businessUnit){
        Approval_Threshold__c threshold = new Approval_Threshold__c();
        threshold.Transaction_Type__c =transactionType;
        threshold.Business_unit__c= businessUnit;
        threshold.Release_Code_0_Threshold__c =0.00;
        threshold.Release_Code_1_Threshold__c =400.00;
        threshold.Release_Code_2_Threshold__c =0.00;
        threshold.Release_Code_3_Threshold__c =5000.00;
        threshold.Release_Code_4_Threshold__c =5000.00;
        threshold.Release_Code_5_Threshold__c =10000.00;
        threshold.Release_Code_6_Threshold__c =100000.00;
        threshold.Release_Code_7_Threshold__c =1000000.00;
        threshold.Release_Code_8_Threshold__c =3000000.00;
        threshold.Release_Code_9_Threshold__c  =100000000.00;
        threshold.Release_Code_10_Threshold__c =1000000000.00;
        threshold.RecordTypeId = getRecordTypeId('Approval_Threshold__c','Release Code Threshold');
        insert threshold;
    }
    
     public static void createControllerThresholdData(String transactionType,String businessUnit){
        createControllerThresholdData(transactionType,businessUnit,15000.00, 100000.00, 20000.00);
    }
    
     public static void createControllerThresholdData(String transactionType,String businessUnit,Decimal buThreshold ,Decimal corpThreshold, Decimal vpThreshold){
        Approval_Threshold__c threshold = new Approval_Threshold__c();
        threshold.Transaction_Type__c =transactionType;
        threshold.Business_unit__c= businessUnit;
        threshold.BU_Threshold__c = buThreshold;
        threshold.Corporate_Threshold__c = corpThreshold;
        threshold.VP_Threshold__c = vpThreshold;
        threshold.RecordTypeId = getRecordTypeId('Approval_Threshold__c','Controller Threshold');
        insert threshold;
    }
    
    public static void createFinanceControllerData(String businessUnit){
        Billing_Adjustment_BU_Controller_Map__c financeConroller = new Billing_Adjustment_BU_Controller_Map__c();
        financeConroller.Business_Unit__c = businessUnit;
        financeConroller.Business_Unit_Controller__c = sampleUserMap.get('buController').id;
        financeConroller.Corporate_Controller__c = sampleUserMap.get('coporateController').id;
        financeConroller.VP_Controller__c = sampleUserMap.get('vpController').id;
        insert financeConroller;
    }
    
    public static void createReasonCodeData(){
        List<Billing_Adjustment_RC_Mapping__c> reasonCodeList = new List<Billing_Adjustment_RC_Mapping__c>();
        reasonCodeList.add(new Billing_Adjustment_RC_Mapping__c(Name = 'Bill Corrections',Code__c = 'B' ,field_Type__c = 'Adjustment Driver'));
        reasonCodeList.add(new Billing_Adjustment_RC_Mapping__c(Name = 'Billing Error',Code__c = 'E' ,field_Type__c = 'Description'));
        reasonCodeList.add(new Billing_Adjustment_RC_Mapping__c(Name = 'Billing Error',Code__c = '21',field_Type__c = 'Reason'));
        reasonCodeList.add(new Billing_Adjustment_RC_Mapping__c(Name = 'Internet',Code__c = '60',field_Type__c = 'Product'));
        reasonCodeList.add(new Billing_Adjustment_RC_Mapping__c(Name = 'Billing Systems',Code__c = 'K',field_Type__c = 'Responsible Area'));
        
        reasonCodeList.add(new Billing_Adjustment_RC_Mapping__c(Name = 'Bill Transactions',Code__c = 'T' ,field_Type__c = 'Adjustment Driver'));
        reasonCodeList.add(new Billing_Adjustment_RC_Mapping__c(Name = 'Contract',Code__c = 'C' ,field_Type__c = 'Description'));
        reasonCodeList.add(new Billing_Adjustment_RC_Mapping__c(Name = 'Advertising',Code__c = '22',field_Type__c = 'Reason'));
        reasonCodeList.add(new Billing_Adjustment_RC_Mapping__c(Name = 'Optik',Code__c = '61',field_Type__c = 'Product'));
        reasonCodeList.add(new Billing_Adjustment_RC_Mapping__c(Name = 'Billing Systems',Code__c = 'K',field_Type__c = 'Responsible Area'));
        
        insert reasonCodeList;
    }
    
    public static void createAprovlMtxcCustomSettingData(){
        Billing_Adjustment_Approval_Flow__c flowMatrix = Billing_Adjustment_Approval_Flow__c.getValues('Credit');
        if(flowMatrix==null){
             flowMatrix = new Billing_Adjustment_Approval_Flow__c(name='Credit');
            flowMatrix.Relase_Code_Base_Approval__c =true;
            flowMatrix.Controller_Approval__c = true;
            flowMatrix.Engage_Sales_Approval__c = true;
            flowMatrix.VP_Controller_Approval__c = true;
            insert flowMatrix;
        }    
    }
    
       
    public static id getRecordTypeId(String sobjectName , String recordTypeName ){
        return Schema.describeSObjects(new List<String>{sobjectName})[0].
            getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
    }
    
    public static void createSampleUserHierarchy(){
        
        List<User> sampleUsrList = [select id, Name, Release_Code__c, managerid, Username, LastName, Email, Alias, CommunityNickname, TimeZoneSidKey, LocaleSidKey, EmailEncodingKey, ProfileId, LanguageLocaleKey 
                                        from User where isActive=true and UserType='Standard' Limit 9];
        User sampleUsr = [select id, Name, Release_Code__c, managerid, Username, LastName, Email, Alias, CommunityNickname, TimeZoneSidKey, LocaleSidKey, EmailEncodingKey, ProfileId, LanguageLocaleKey 
                                        from User where id = :UserInfo.getUserId()];
        PermissionSet permSet = [select id, name from PermissionSet where name ='Billing_Adjustment_User_Permissions'];
        List<PermissionSetAssignment> assignmentList = new List<PermissionSetAssignment>();
        
        User agent = sampleUsr.clone(false,true);
        User manager = sampleUsr.clone(false,true);
        User dir = sampleUsr.clone(false,true);
        User vp = sampleUsr.clone(false,true);
        User buController = sampleUsr.clone(false,true);
        User coporateController = sampleUsr.clone(false,true);
        User saDir = sampleUsr.clone(false,true);
        User saMD = sampleUsr.clone(false,true);
        User saPrime = sampleUsr.clone(false,true);
        User svp = sampleUsr.clone(false,true);
        User evp = sampleUsr.clone(false,true);
        User ceo = sampleUsr.clone(false,true);
        User usrNoManager = sampleUsr.clone(false,true);
        User usrNoReleaseCode = sampleUsr.clone(false,true);
        User vpController = sampleUsr.clone(false,true);
        
        agent.Release_Code__c = '1';
        manager.Release_Code__c = '3';
        dir.Release_Code__c = '5';
        vp.Release_Code__c = '6';
        buController.Release_Code__c = '3';
        coporateController.Release_Code__c = '3';
        vpController.Release_Code__c = '3';
        saDir.Release_Code__c = '5';
        saMD.Release_Code__c = '6';
        saPrime.Release_Code__c = '3';
        svp.Release_Code__c = '7';
        evp.Release_Code__c = '8';
        ceo.Release_Code__c = '9';
        usrNoManager.Release_code__c ='1';
        usrNoReleaseCode.Release_code__c =null;
        
        sampleUserMap = new Map<String,User>
            {'agent'=>agent,
            'manager'=>manager,
            'dir'=>dir,
            'vp'=>vp,
            'buController'=>buController,
            'coporateController'=>coporateController,
            'saDIR'=>saDir,
            'saMD'=>saMD,
            'saACCTPRIME'=>saPrime,
            'svp'=>svp,
            'evp'=>evp,
            'ceo'=>ceo,
            'usrNoManager'=>usrNoManager,
            'vpController'=>vpController
            };
        for(String key :sampleUserMap.keySet()){
            User  u = sampleUserMap.get(key);
            u.username =key+'@telus.com.ebam'; 
            u.CommunityNickname ='ba'+key;
            u.FirstName =key;
            u.LastName =key;
            if(key.length()>=8)
                u.Alias =key.substring(0,6);
            else
                u.Alias =key;
        }
                
        Database.insert(sampleUserMap.values());
        agent.ManagerId = manager.id;
        manager.ManagerId = dir.id;       
        dir.ManagerId = vp.id;
        vp.ManagerId = svp.id;
        saMD.ManagerId = svp.id;
        svp.ManagerId = evp.id;
        evp.ManagerId = ceo.id;
        usrNoManager.ManagerId =null;
        Database.update(new List<User>{agent,manager,dir,saMD,svp,evp,ceo,usrNoManager});
    
    }
    
    public static  void addPermissionSetToUser(User u){
        PermissionSet permSet = [select id, name from PermissionSet where name ='Billing_Adjustment_User_Permissions'];
        PermissionSetAssignment assignment = new PermissionSetAssignment(PermissionSetId=permSet.id,Assigneeid=u.id); 
        insert assignment;
    }
    
}