/*
    *******************************************************************************************************************************
    Created by:     *******             dd-Mon-20yy     OCOM-xxxx       No previous version
    Modified by:    Santosh Rath        23-Feb-2017     10493           Check Order item PONR status before order cancel

                    
    *******************************************************************************************************************************
*/
global class OCOM_HybridCPQHeaderController {
    public String workRequestRecordTypeId{
        get{
             RecordType rt = [SELECT Id, Name, DeveloperName FROM RecordType WHERE SObjectType = 'Work_Request__c' AND DeveloperName = 'Order_Support'];
            if(rt!=null){
                return rt.id;
            }
            return null;
        }
    }
    public String accountWorkRequestJson {get{
        FieldDefinition fd=[SELECT Id, QualifiedApiName,EntityDefinitionId,DurableId FROM FieldDefinition WHERE EntityDefinition.QualifiedApiName = 'Work_Request__c' AND QualifiedApiName = 'Account__c'];
        String returnStr=JSON.serialize(fd);
        return returnStr;
    }}
     public String opportunityWorkRequestJson {get{
        FieldDefinition fd=[SELECT Id, QualifiedApiName,EntityDefinitionId,DurableId FROM FieldDefinition WHERE EntityDefinition.QualifiedApiName = 'Work_Request__c' AND QualifiedApiName = 'Opportunity__c'];
        String returnStr=JSON.serialize(fd);
        return returnStr;
    }}
    public String orderWorkRequestJson {get{
        FieldDefinition fd=[SELECT Id, QualifiedApiName,EntityDefinitionId,DurableId FROM FieldDefinition WHERE EntityDefinition.QualifiedApiName = 'Work_Request__c' AND QualifiedApiName = 'Order__c'];
        String returnStr=JSON.serialize(fd);
        return returnStr;
    }}
    public Boolean displayTaskWorkRequestButton{get;set{displayTaskWorkRequestButton=String.isBlank(orderId);}}
    public String oppId {get;set;}
    public String oppName {get;set;}
    public String accountId{get;set;}
    public String accountNameStr{get;set;}
    public String orderName{get;set;}
   Public String orderId {get;set;}
   //Id original_param_order_id = '80122000000Ckxx';
   public Order order {get;set;}
   public String OrderNumber{get;set;}
   
   public string Title{get;set;}
   public string Email{get;set;}
   public string Phone{get;set;}
   //Arpit : 19-May-2017 : Sprint-3 : Toggleing ENglish-French Logic
   public static string lang {get ; set;}
   
   public string Owner{get;set;}
   public String OwnerId{get;set;}
   //public string MaxSpeed{get;set;}
   public string orderStatus{get;set;}
   public Boolean isOrderCancellable{get;set;} // Defect# 10493 - Added this line
   //Niran: QC-10203
   public string OrderMgmtId{get;set;}
   public Boolean displayComponents{get;set;}
   public Boolean displayViewPdfBtn{get;set;}
   public Boolean disableViewPdfBtn{get;set;}
   public String smbCareAddrId{get;set;}
   public String moveOutAddrId{get;set;}
   public string ncOrderStatus{get;set;}
   /*public void setsmbCareAddrId(String s){
      smbCareAddrId = s;
      OCOM_HybridCPQHeaderController();
   }*/
   Map<String, SMBCare_Address__c> addressToIDMap = new Map<String, SMBCare_Address__c>();
   public SMBCare_Address__c srvcAddress = new SMBCare_Address__c();

   
   //public Boolean srvcAddrssNotValid{get;set;}
  // public String ad;

   public  OCOM_HybridCPQHeaderController() {
    
    orderId = ApexPages.currentPage().getParameters().get('OrderId') == null ? '' : ApexPages.currentPage().getParameters().get('OrderId');
       displayTaskWorkRequestButton=String.isBlank(orderId);
    smbCareAddrId = ApexPages.currentPage().getParameters().get('smbCareAddrId') == null ? '' : ApexPages.currentPage().getParameters().get('smbCareAddrId');
     oppId = ApexPages.currentPage().getParameters().get('oppId') == null ? '' : ApexPages.currentPage().getParameters().get('oppId');
       if(String.isNotBlank(oppId)){
           Opportunity queriedOppObj=[select name from opportunity where id=:oppId];
           if(queriedOppObj!=null){
               oppName=queriedOppObj.name;
           }
           
       }
       
     accountId = ApexPages.currentPage().getParameters().get('aid') == null ? '' : ApexPages.currentPage().getParameters().get('aid');       
       if(String.isNotBlank(accountId)){
           Account queriedAccntobj=[select Name from Account where id=:accountId];
           if(queriedAccntobj!=null && String.isNotBlank(queriedAccntobj.Name)){
               accountNameStr=queriedAccntobj.Name;
               System.debug('accountName='+accountNameStr);
           }
           
       }
       if(String.isBlank(accountNameStr)){
          accountId = ApexPages.currentPage().getParameters().get('ParentAccId') == null ? '' : ApexPages.currentPage().getParameters().get('ParentAccId');     
           if(String.isNotBlank(accountId)){
           Account queriedAccntobj=[select Name from Account where id=:accountId];
           if(queriedAccntobj!=null && String.isNotBlank(queriedAccntobj.Name)){
               accountNameStr=queriedAccntobj.Name;
               System.debug('accountName='+accountNameStr);
           }
           
       }
       }
    moveOutAddrId = ApexPages.currentPage().getParameters().get('moveOutAddrId') == null ? '' : ApexPages.currentPage().getParameters().get('moveOutAddrId');
    if(orderId == null || orderId == '')
        orderId = ApexPages.currentPage().getParameters().get('id') == null ? '' : ApexPages.currentPage().getParameters().get('id');
      displayTaskWorkRequestButton=String.isBlank(orderId);
    if (orderId != null) {
        //Aruna: amended below SOQL to include field : CustomerAuthorizedById
        
        //Niran: QC-10203 - added OrderMgmtId__c
        list<order> orderList = [select accountid,opportunityid,opportunity.name,ordermgmtid_status__c, Name, OrderNumber, Status, OrderMgmtId__c, Type, Account.Name, CustomerAuthorizedById, CustomerAuthorizedBy.Name, CustomerAuthorizedBy.Phone, RequestedDate__c,Service_Address__r.Address__c,Service_Address__r.Street_Address__c,Service_Address__r.City__c,Service_Address__r.FMS_Address_ID__c,Service_Address__r.Send_to_SACG__c,Service_Address_Text__c,Service_Address__r.Maximum_Speed__c, Service_Address__r.isTVAvailable__c,Service_Address__r.Forbone__c,Service_Address__r.Status__c, Maximum_Speed__c,Forbone__c, ContactTitle__c, ContactEmail__c, ContactPhone__c,MoveOutServiceAddress__c,MoveOutServiceAddress__r.Suite_Number__c,MoveOutServiceAddress__r.Address__c,MoveOutServiceAddress__r.City__c,MoveOutServiceAddress__r.Postal_Code__c, MoveOutServiceAddress__r.Country__c,MoveOutServiceAddress__r.Province__c,MoveOutServiceAddress__r.AddressText__c , Service_Address__r.Is_ILEC__c from Order where Id =: orderId];
        List<OrderItem> orderItemRecs = [Select Id, orderMgmtId_Status__c, PriceBookEntry.product2.IncludeQuoteContract__c,PriceBookEntry.product2.Sellable__c FROM OrderItem WHERE OrderId = :orderId];
        
        system.debug('>>>>>> Order' + orderList);
        if(orderList.size()>0 && !orderList.isEmpty() ){
          order = orderList[0];
          orderName=order.OrderNumber;
            oppId=order.opportunityid;
            oppName=order.opportunity.name;
            accountId=order.AccountId;
            accountNameStr=order.Account.Name;
            System.debug('accountName='+accountNameStr);

          Title = order.ContactTitle__c != null ? order.ContactTitle__c: '';
          Email = order.ContactEmail__c != null ? order.ContactEmail__c: '';
          Phone = order.ContactPhone__c != null ? order.ContactPhone__c: '';
          //Forborne =  order.Forbone__c == true ?  Label.OCOM_Forborne : Label.OCOM_Non_Forborne;
          //Type = order.Type != null ? order.Type:'';
          Owner = order.CustomerAuthorizedBy.Name != null ? order.CustomerAuthorizedBy.Name :'';
          //Aruna: amended below line to remove "Invalid Id" error.
          OwnerId = order.CustomerAuthorizedByID != null ? ''+order.CustomerAuthorizedById :'';
          //MaxSpeed = order.Service_Address__r.Maximum_Speed__c != null ? order.Service_Address__r.Maximum_Speed__c : '';
          OrderNumber = order.OrderNumber != null? order.OrderNumber : '';
          orderStatus = order.Status != null? order.Status:'';
          //Niran: QC-10203
          OrderMgmtId = order.OrderMgmtId__c != null? order.OrderMgmtId__c:'';
           
          ncOrderStatus=order.ordermgmtid_status__c != null? order.ordermgmtid_status__c:order.Status;
        }
        if(smbCareAddrId != null){
          set<string > addressIdSet = new Set<String>{smbCareAddrId,moveOutAddrId};

          for (SMBCare_Address__c serviceAddress : [SELECT ID, Service_Account_Id__c, Service_Account_Id__r.Id,
                                              FMS_Address_ID__c, Send_to_SACG__c, isTVAvailable__c, Forbone__c, Maximum_Speed__c, Status__c ,AddressText__c,Is_ILEC__c
                                            FROM SMBCare_Address__c WHERE ID =:addressIdSet]){
                                              addressToIDMap.put(serviceAddress.id, serviceAddress);
                                            }
          /*if(serviceAddressList.size() > 0 && !serviceAddressList.isEmpty())   {
               srvcAddress = serviceAddressList[0];
              if(srvcAddress != null){
                addrNotValid = (srvcAddress.Status__c != null && srvcAddress.Status__c != 'Valid')? true:false;
                tvAvailability = srvcAddress.isTVAvailable__c == true? Label.OCOM_TV_Available : Label.OCOM_TV_Not_Available ; 
                forborne = srvcAddress.Forbone__c == true? Label.OCOM_Forborne : Label.OCOM_Non_Forborne;
                maximumSpeed = srvcAddress.Maximum_Speed__c != null ? srvcAddress.Maximum_Speed__c : '';
                maxSpeedCopper = maximumSpeed == '' ? true:false;
             }
             else {
                tvAvailability = '' ; 
                forborne = '';
                maximumSpeed = '';
                maxSpeedCopper = false;
             }
          }*/
        }
        isOrderCancellable=true; // Defect# 10493 - Added this line

        //Niran OCOM-403
        disableViewPdfBtn = true;
        if(orderItemRecs != null && orderItemRecs.size()>0)
        {
            for(OrderItem orderRec : orderItemRecs)
            {
                // Defect# 10493 - Begin
                System.debug('orderRec.orderMgmtId_Status__c='+orderRec.orderMgmtId_Status__c);
                
                  if((String.isNotBlank(orderRec.orderMgmtId_Status__c) && OrdrConstants.OI_PONR_STATUS.equalsIgnoreCase(orderRec.orderMgmtId_Status__c)) || 
                  (String.isNotBlank(orderRec.orderMgmtId_Status__c) && OrdrConstants.OI_COMPLETED_STATUS.equalsIgnoreCase(orderRec.orderMgmtId_Status__c))){
                     isOrderCancellable=false; 
                  } 
                // Defect# 10493 - End
                if(orderRec.PriceBookEntry.product2.Sellable__c!=null && orderRec.PriceBookEntry.product2.Sellable__c==true){
                   if(orderRec.PriceBookEntry.product2.IncludeQuoteContract__c == true)
                        disableViewPdfBtn = false; 
                }
                
            }
        }
        System.debug('isOrderCancellable='+isOrderCancellable);
      }

    }
     public string Type{get{
      if((Type == null || Type == '') && order != null){
          return  Type = order.Type != null ? order.Type:'';
        }
        else 
        return  Type ;
      }
      set;}
   
    public String genericAddress{
      get{
        if(smbCareAddrId != null && addressToIDMap.ContainsKey(smbCareAddrId)){
        return genericAddress = addressToIDMap.get(smbCareAddrId).AddressText__c != null ? addressToIDMap.get(smbCareAddrId).AddressText__c:'';
       }
      else if((genericAddress == null || genericAddress == '') && order != null){
        return genericAddress = order.Service_Address_Text__c != null? order.Service_Address_Text__c:'';
      } 
      else 
        return genericAddress; 
    }
    set;}

     public String genericAccountName{
      get{
      if(order != null){
        return genericAccountName = order.Account.Name != null ? order.Account.Name:'';
      } 
     
      else 
        return genericAccountName;
    }
    set;}

    public String moveOutAddress{
      get{
        if((moveOutAddress == null || moveOutAddress == '') && moveOutAddrId != null && addressToIDMap.ContainsKey(moveOutAddrId)){
          return genericAddress = addressToIDMap.get(moveOutAddrId).AddressText__c != null ? addressToIDMap.get(moveOutAddrId).AddressText__c :'';
      }
      else if((moveOutAddress == null || moveOutAddress == '') && order != null && order.MoveOutServiceAddress__c != null){
         String movoutAddressText;   
          movoutAddressText =    order.MoveOutServiceAddress__r.AddressText__c;
        return moveOutAddress = movoutAddressText != null && movoutAddressText != '' ? movoutAddressText :'';
        
      }  
      else 
        return moveOutAddress; 
    }
    set;}

     public Boolean srvcAddrssNotValid{get{
      if(smbCareAddrId != null && addressToIDMap.ContainsKey(smbCareAddrId)){
        return srvcAddrssNotValid = (addressToIDMap.get(smbCareAddrId).Status__c != null && addressToIDMap.get(smbCareAddrId).Status__c != 'Valid') ? true  :false;
      }
      else if( order != null ){
        return srvcAddrssNotValid = (order.Service_Address__r.Status__c != null && order.Service_Address__r.Status__c != 'Valid')? true:false;
      }   
      else 
        return srvcAddrssNotValid; 
     }
     set;}

     public String tvAvailabilityInfo{get{
      if(smbCareAddrId != null && addressToIDMap.ContainsKey(smbCareAddrId)){
        return tvAvailabilityInfo = addressToIDMap.get(smbCareAddrId).isTVAvailable__c == true ? Label.OCOM_TV_Available : Label.OCOM_TV_Not_Available;
      }
      else if((tvAvailabilityInfo == null || tvAvailabilityInfo == '' ) && order != null){
        return tvAvailabilityInfo = order.Service_Address__r.isTVAvailable__c == true ? Label.OCOM_TV_Available : Label.OCOM_TV_Not_Available;
      } 
      else 
        return tvAvailabilityInfo; 
     }
     set;}

     public string forborneInfo{get{
      if(smbCareAddrId != null && addressToIDMap.ContainsKey(smbCareAddrId)){
        return forborneInfo = addressToIDMap.get(smbCareAddrId).Forbone__c == true ? Label.OCOM_Forborne : Label.OCOM_Non_Forborne;
      }
      else if((forborneInfo == null || forborneInfo == '' ) && order != null){
        system.debug('>>>>>forborneInfo' + forborneInfo);
        return forborneInfo = order.Service_Address__r.Forbone__c == true ?  Label.OCOM_Forborne : Label.OCOM_Non_Forborne;
      } 
      else 
        return forborneInfo; 
      }set;}

      public string MaxSpeed{get{

       if(smbCareAddrId != null && addressToIDMap.ContainsKey(smbCareAddrId)){
        return MaxSpeed = addressToIDMap.get(smbCareAddrId).Maximum_Speed__c != null ? addressToIDMap.get(smbCareAddrId).Maximum_Speed__c : '';
        }
      else if((MaxSpeed == null || MaxSpeed == '' ) && order != null){
        system.debug('>>>>>MaxSpeed' + MaxSpeed);
        
        return MaxSpeed = order.Service_Address__r.Maximum_Speed__c != null ? order.Service_Address__r.Maximum_Speed__c : '';
      } 
      else {
        //maxSpdCopper = true;
        return MaxSpeed;
      }
      }set;}

     public Boolean maxSpdCopper{get{
        if( order != null)
         return maxSpdCopper =  MaxSpeed != '' ? false : true;

        else return maxSpdCopper;
      }set;}

   public Boolean isILEC{get{
        if(smbCareAddrId != null && addressToIDMap.ContainsKey(smbCareAddrId)){
            return isILEC = addressToIDMap.get(smbCareAddrId).Is_ILEC__c != null ? addressToIDMap.get(smbCareAddrId).Is_ILEC__c : false;
         }
        else if( order != null)
            return isILEC =  order.Service_Address__r.Is_ILEC__c != null ? order.Service_Address__r.Is_ILEC__c : false;
        
         else return true;
        //else return isILEC;
      }set;}

  public PageReference cancelOrder() {    
    PageReference pageRef = new PageReference('/apex/OCOM_CancelOrder?id='+orderId);
    pageRef.setRedirect(true);
    return pageRef;        
  }    
  
  
  @RemoteAction
  global static void setChoosenLanguageOnOrder( String orderId , String language){
      system.debug('orderId in setChoosenLanguageOnOrder() ------' + orderId );
      Order orderObj = [Select Id, Chosen_Language__c, CustomerAuthorizedBy.Language_Preference__c  from Order where  Id =: orderId];
      
      orderObj.Chosen_Language__c = language;
      
      update orderObj ;
      system.debug('orderObj in setChoosenLanguageOnOrder() ------' + orderObj );
      
  }
  
  
  @RemoteAction
  global static void callRemoteMethodForDefaultlanguage(String orderId){
      system.debug('orderId in callRemoteMethodForDefaultlanguage() ------' + orderId );
      Order orderObj = [Select Id, Chosen_Language__c, CustomerAuthorizedBy.Language_Preference__c  from Order where  Id =: orderId];
      
      if(orderObj.CustomerAuthorizedBy.Language_Preference__c.equalsIgnoreCase(Label.English_Label))
            orderObj.Chosen_Language__c = Label.en_US_Label;
      else
            orderObj.Chosen_Language__c = Label.fr_Label;  
            
      update orderObj ;
      system.debug('orderObj in callRemoteMethodForDefaultlanguage() ------' + orderObj );
      
  }
}