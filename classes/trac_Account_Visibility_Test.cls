/**
 * trac_Account_Visibility_Test.cls - Test class for trac_Account_Visibility_extension
 * @description Test class for trac_Account_Visibility_extension
 *
 * @author Ryan Draper, Traction on Demand
 * @date 2013-08-12
 */
@isTest
public without sharing class trac_Account_Visibility_Test {
	
	public static testMethod void testController(){
		UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
		Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
		User portalAccountOwner1 = new User(
			UserRoleId = portalRole.Id,
			ProfileId = profile1.Id,
			Username = 'portalOwnerTest1@example.com',
		   	Alias = 'portal1',
			Email='portalOwnerTest1@example.com',
			EmailEncodingKey='UTF-8',
			Firstname='portalOwnerTest1_unassigned',
			Lastname='portalOwnerTest1',
			LanguageLocaleKey='en_US',
			LocaleSidKey='en_US',
			TimeZoneSidKey='America/Chicago'
		);
		insert portalAccountOwner1;
		
		system.runAs(portalAccountOwner1){
			Test.startTest();
			
			Account a = new Account(name = 'test account', 
									Dealer_Account__c = false,
									Information_verified_by__c = portalAccountOwner1.id,
									Wireline_Escalation_Timestamp__c = System.now());
			insert a;
			a.Dealer_Account__c = true;
			update a;
			
			PageReference testPage = Page.DealerAccountView;
			testPage.getParameters().put('id',a.id);
			Test.setCurrentPage(testPage);
			trac_Account_Visibility_extension testExt = new trac_Account_Visibility_extension(new ApexPages.standardController(a));
			testExt.populateAccountStatus();
			testExt.removeIdRows();
			testExt.populateLangPrefs();
			testExt.opportunityType = 'test';
			testExt.products = 'test';
			testExt.relationshipWithClientDetails = 'test';
			
			testExt.recordTypeName = 'CBUCID';
			testExt.isOwner = true;
			testExt.determineIsRequestable();
			
			testExt.recordTypeName = 'CBUCID';
			testExt.isOwner = false;
			testExt.currentUser = portalAccountOwner1;
			testExt.currentUserName = 'unassigned';
			testExt.determineIsRequestable();
			
			testExt.recordTypeName = 'CBUCID';
			testExt.isOwner = false;
			testExt.currentUserName = 'test';
			testExt.isSameDealer = false;
			testExt.determineIsRequestable();
			
			testExt.recordTypeName = 'CBUCID';
			testExt.isSameDealer = true;
			testExt.currentUserType = 'partner';
			testExt.determineIsRequestable();
			
			testExt.recordTypeName = 'CBUCID';
			testExt.isOwner = false;
			testExt.currentUserName = 'test';
			testExt.currentUserType = 'powerpartner';
			testExt.isSameDealer = true;
			testExt.determineIsRequestable();
			
			testExt.getDateFromString('01/01/2012');

			
			PageReference testOnSave = testExt.onSave();//assert make sure url is correct
			system.assertEquals(testOnSave.getUrl(), '/apex/DealerAccountView');
			PageReference testOnCancel = testExt.onCancel();
			system.assertEquals(testOnCancel.getUrl(), '/apex/DealerAccountView');
			PageReference testOnEdit = testExt.onEdit();
			system.assertEquals(testOnEdit.getUrl(), '/apex/DealerAccountEdit');
			PageReference testOnRequestOwnership = testExt.onRequestOwnership();
			system.assertEquals(testOnRequestOwnership.getUrl(), '/apex/RequestOwnership');
			PageReference testOnWirelineEscalation = testExt.onWirelineEscalation();
			system.assertEquals(testOnWirelineEscalation.getUrl(), '/apex/SMBEscalationPage');
			
			testExt.opportunityType = null;
			//system.assertEquals(testOnSendOwnershipEmail.getUrl(), '/apex/DealerAccountView');
			Test.stopTest();
		}
	}
	
	/*
	public static testmethod void testHistoryWrapper(){
		UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
		Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
		User portalAccountOwner1 = new User(
			UserRoleId = portalRole.Id,
			ProfileId = profile1.Id,
			Username = 'portalOwnerTest1@example.com',
		   	Alias = 'portal1',
			Email='portalOwnerTest1@example.com',
			EmailEncodingKey='UTF-8',
			Firstname='portalOwnerTest1_unassigned',
			Lastname='portalOwnerTest1',
			LanguageLocaleKey='en_US',
			LocaleSidKey='en_US',
			TimeZoneSidKey='America/Chicago'
		);
		insert portalAccountOwner1;
		
		system.runAs(portalAccountOwner1){
			Test.startTest();
			
			AccountHistory hist = new AccountHistory(Field = 'Name', OldValue = 'bob', NewValue = 'jim');
			insert hist;
			
			trac_Account_Visibility_extension.HistoryWrapper hw 
						= new trac_Account_Visibility_extension.HistoryWrapper(hist);
			Test.stopTest();
		}
	}
	*/
	
	public static testmethod void testSendEmail(){
		Account a = new Account(name = 'test account 2');
		insert a;
		
		PageReference testPage = Page.DealerAccountView;
		testPage.getParameters().put('id',a.id);
		Test.setCurrentPage(testPage);
		
		trac_Account_Visibility_extension testExt = new trac_Account_Visibility_extension(new ApexPages.standardController(a));
		
		PageReference testOnSendOwnershipEmail = testExt.sendOwnershipEmail();
		
		testExt.opportunityType = null;
		testOnSendOwnershipEmail = testExt.sendOwnershipEmail();
		
		testExt.products = null;
		testOnSendOwnershipEmail = testExt.sendOwnershipEmail();
		
		testExt.relationshipWithClientDetails = null;
		testOnSendOwnershipEmail = testExt.sendOwnershipEmail();
		
		testExt.opportunityType = 'test';
		testExt.products = 'test';
		testExt.relationshipWithClientDetails = 'test';
		testOnSendOwnershipEmail = testExt.sendOwnershipEmail();
	}
	
	public static testMethod void testOnSubmitEscalation(){
		Account a = new Account(name = 'test account 2');
		insert a;
		Contact testContact = New Contact(LastName = 'test');
		insert testContact;	
		
		PageReference testPage = Page.DealerAccountView;
		testPage.getParameters().put('id',a.id);
		Test.setCurrentPage(testPage);
				
		trac_Account_Visibility_extension testExt = new trac_Account_Visibility_extension(new ApexPages.standardController(a));

		testExt.typeOfIssue = 'Sales/Care';
		testExt.onSubmitEscalation();
			
		testExt.typeOfIssue = 'Sales/Care';
		testExt.telusWLNPhone  = 'TEST';
		testExt.escalationContactId = testContact.id;
		testExt.contactPhoneNumber = 'TEST';
		testExt.descriptionOfIssue  = 'TEST';
		testExt.whatHasBeenDone  = 'TEST';
		testExt.whatNeedsToBeDone  = 'TEST';
		testExt.onSubmitEscalation();
			
		testExt.typeOfIssue = 'Assure';
		testExt.onSubmitEscalation();
	}
	
	
	public static testMethod void testException(){
		Account a = new Account(name = 'test account', Dealer_Account__c=false);
		insert a;
		
		PageReference testPage = Page.DealerAccountView;
		testPage.getParameters().put('id',a.id);
		Test.setCurrentPage(testPage);
		trac_Account_Visibility_extension testExt = new trac_Account_Visibility_extension(new ApexPages.standardController(a));
		
		Test.setReadOnlyApplicationMode(true);
		PageReference testRef = testExt.onSave();
		
		system.assertEquals(null,testRef);
	}
	
	public static testMethod void testUserAccess(){
		
		//Create portal account owner
		UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
		Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
		User portalAccountOwner1 = new User(
			UserRoleId = portalRole.Id,
			ProfileId = profile1.Id,
			Username = 'portalOwnerTest1@example.com',
		   	Alias = 'portal1',
			Email='portalOwnerTest1@example.com',
			EmailEncodingKey='UTF-8',
			Firstname='portalOwnerTest1',
			Lastname='portalOwnerTest1',
			LanguageLocaleKey='en_US',
			LocaleSidKey='en_US',
			TimeZoneSidKey='America/Chicago'
		);
		insert portalAccountOwner1;
		
		Profile portalProfile = [SELECT Id FROM Profile WHERE Name = 'Telus Partner User 2' limit 1];
		    	
		//Create user
		System.runAs(portalAccountOwner1){
			
			//Create account
			Account portalAccount1 = new Account(
				Name = 'TestAccount',
				OwnerId = portalAccountOwner1.Id
			);
			insert portalAccount1;
			    	
			//Create contact
			Contact contact1 = new Contact(
			   	FirstName = 'Test',
			    	Lastname = 'McFly',
				AccountId = portalAccount1.Id,
			    	Email = System.now().millisecond() + 'test@test.com'
			);
			insert contact1;
			
			
			User user1 = new User(
				Username = System.now().millisecond() + 'portalUserTest12345@test.com',
				ContactId = contact1.Id,
				ProfileId = portalProfile.Id,
				Alias = 'portal2',
				Email = 'portalUserTest12345@test.com',
				EmailEncodingKey = 'UTF-8',
				LastName = 'McFly',
				CommunityNickname = 'portalUserTest12345',
				TimeZoneSidKey = 'America/Los_Angeles',
				LocaleSidKey = 'en_US',
				LanguageLocaleKey = 'en_US'
			);
			insert user1;
		}
		
			
		
		
		//Grant's code
		/*
		Id ADMIN_PROFILE_ID = [SELECT id FROM Profile WHERE name='System Administrator' LIMIT 1].id;
		User runner = new User(username = 'runner@TRACTIONSM.COM',
                                                  email = 'runner@example.com',
                                                  title = 'test',
                                                  lastname = 'runner',
                                                  alias = 'runner',
                                                  TimezoneSIDKey = 'America/Los_Angeles',
                                                  LocaleSIDKey = 'en_US',
                                                  EmailEncodingKey = 'UTF-8',
                                                  ProfileId = ADMIN_PROFILE_ID,
                                                  LanguageLocaleKey = 'en_US');
                insert runner;
                
		Id PROFILEID = [SELECT id FROM Profile WHERE name='Telus Partner User 2' LIMIT 1].id;
		Id DEALER_USER_ROLE = [SELECT Id, Name FROM UserRole WHERE Name = 'Dealer - Tom Harris BC Partner Manager' limit 1].id;
		
		//
		Account portalOwner = new Account(name = 'test account2');
		//insert portalOwner;
		
		Contact contact1 = new Contact(lastname='test1',accountid=portalOwner.id);
		Contact contact2 = new Contact(lastname='test2',accountid=portalOwner.id);
		insert new Contact[] {contact1, contact2};
		
		User u1 = new User(username = 'visibilityTest1@TRACTIONSM.COM',
                                                  email = 'test1@example.com',
                                                  title = 'test',
                                                  lastname = 'test1',
                                                  alias = 'test1',
                                                  TimezoneSIDKey = 'America/Los_Angeles',
                                                  LocaleSIDKey = 'en_US',
                                                  EmailEncodingKey = 'UTF-8',
                                                  ProfileId = PROFILEID,
                                                  LanguageLocaleKey = 'en_US',
                                                  ContactId = contact1.Id,
                                                  UserRoleId = DEALER_USER_ROLE);
                
                
         User u2 = new User(username = 'visibilityTest2@TRACTIONSM.COM',
                                                  email = 'test2@example.com',
                                                  title = 'test',
                                                  lastname = 'test2',
                                                  alias = 'test2',
                                                  TimezoneSIDKey = 'America/Los_Angeles',
                                                  LocaleSIDKey = 'en_US',
                                                  EmailEncodingKey = 'UTF-8',
                                                  ProfileId = PROFILEID,
                                                  LanguageLocaleKey = 'en_US',
                                                  ContactId = contact2.Id,
                                                  UserRoleId = DEALER_USER_ROLE);
		insert new User[] {u1, u2};
		
		system.runAs(runner) {
			insert portalOwner;
			//insert new User[] {u1, u2};
		}
		
		
		system.runAs(u2){
			Account a = new Account(name = 'test account', ownerId = u1.Id);
			insert a;
			PageReference testPage = Page.DealerAccountView;
			testPage.getParameters().put('id',a.id);
			Test.setCurrentPage(testPage);
			trac_Account_Visibility_extension testExt = new trac_Account_Visibility_extension(new ApexPages.standardController(a));
			system.assertEquals(false, testExt.isSameDealer);
		}
		*/
		
	}
	
	
}