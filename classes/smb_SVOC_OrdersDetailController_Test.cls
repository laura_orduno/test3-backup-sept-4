@isTest(SeeAllData=false)
public class smb_SVOC_OrdersDetailController_Test{
    static testMethod void MyUnitTest() {
        PageReference pageref = Page.smb_SVOC_OrdersDetail;
        Test.setCurrentPage(pageref);
        
        Account acc = smb_test_utility.createAccount('New_Account', True);
        pageref.getparameters().put('id', acc.Id);
        pageref.getparameters().put('orderId', '1234');
        pageref.getparameters().put('orderActionId', '12345678900000');
        pageref.getparameters().put('seqNumber', '1');
        pageref.getparameters().put('sysName', 'test_sys_Name');
        pageref.getparameters().put('issueDate', String.valueof(DateTime.now()));
        
        smb_SVOC_OrdersDetailController oc = new smb_SVOC_OrdersDetailController();
        oc.init();
    }
    
    // Added by Arvind to increase the test Coverage (Earlier Coverage was 21% only)
    
    static testMethod void testSVOC_OrdersDetail(){         
        
        //..... Test Custom Setting data......
        SMBCare_WebServices__c cs = new SMBCare_WebServices__c(); 
        cs.Name = 'SMB_LegacyOrderEndpoint';
        cs.Value__c = 'https://partnerservices-pt.telus.com:443/SOA/CMO/OrderMgmt/OrderTrackingService_v4_2_vs0_vs0';
        insert cs;
        
        SMBCare_WebServices__c SmbC = new SMBCare_WebServices__c(); 
        SmbC.Name = 'CRDB_LegacyOrderEndpoint';
        SmbC.Value__c = 'https://partnerservices-pt.telus.com:443/SOA/CMO/OrderMgmt/OrderTrackingService_v4_2_vs0_vs0';
        insert SmbC;
        
        SMBCare_WebServices__c cs1 = new SMBCare_WebServices__c(); //Custom Setting for username Field
        cs1.Name = 'username';
        cs1.Value__c = 'APP_SFDC';
        insert cs1;
        
        SMBCare_WebServices__c cs2 = new SMBCare_WebServices__c(); //Custom Setting for password Field
        cs2.Name = 'password';
        cs2.Value__c = 'soaorgid';
        insert cs2;  
        
        // creating an account
        Account acc = new Account();
        acc.Name = 'test';
        insert acc;
        // creating a service request referencing an account created above
        Service_Request__c sr = new Service_Request__c();
        sr.Account_Name__c = acc.Id;
        insert sr;
        
        SRS_Service_Request_Related_Order__c relatedOrder=new SRS_Service_Request_Related_Order__c();
        relatedOrder.seq__C='2';
        relatedOrder.Name='1234567';
        relatedOrder.isValid__C=True;
        relatedOrder.System__c='SRT2';
        relatedOrder.RO_Service_Request__c=sr.id;
        insert relatedOrder;        
        
        smb_SVOC_OrdersDetailController src=new smb_SVOC_OrdersDetailController();
    //  src.getOrderDetail(relatedOrder.Name);       
        
        smb_orders_trackingtypes_v4.OrderRequest legacyOrders=new smb_orders_trackingtypes_v4.OrderRequest();
        
        smb_orders_trackingtypes_v4.OrderRequestKey orderKey=new smb_orders_trackingtypes_v4.OrderRequestKey();  
        legacyOrders.orderKey=orderKey;
        orderKey.systemName='test';
        orderKey.circuitNumber='test';
        orderKey.sequenceNum='test';
      
        //Added as part of CRDB Generic Remark changes..      
        orderKey.issueDate = date.today();
       	orderKey.orderActionId = long.valueOf('1006443628');
     	orderKey.sequenceNum = '1';
     	orderKey.systemName = 'TOM-TQ';
     	orderkey.legacyOrderId = 'C73395';
     	
     	
     	smb_orders_trackingtypes_v4.RemarkList remarkList=new smb_orders_trackingtypes_v4.RemarkList();
     	legacyOrders.remarkList=remarkList;
     	remarkList.equipmentRemark ='Test';
     	remarkList.dispatchRemark ='Test';
     	remarkList.equipmentRemark ='Test';
     	remarkList.ecopsDiaryRemark ='Test';
     	
     	//remarkList.genericRemarkList ='Test';
     	
     	/*smb_orders_trackingtypes_v4.GenericRemark GenericRemarkList=new smb_orders_trackingtypes_v4.GenericRemark();
     	legacyOrders.GenericRemarkList=GenericRemarkList;
     	GenericRemarkList.GenericRemark ='Test';
     	*/
     	     	
     	
     	smb_orders_trackingtypes_v4.OutOfBand outOfBandInfo=new smb_orders_trackingtypes_v4.OutOfBand();
     	legacyOrders.outOfBandInfo=outOfBandInfo;
     	outOfBandInfo.outOfBandOrder ='Test';
     	outOfBandInfo.outOfBandCircuit ='Test';
     	
     	smb_orders_trackingtypes_v4.Attribute orderAttributeList=new smb_orders_trackingtypes_v4.Attribute();
     	legacyOrders.orderAttributeList=orderAttributeList;
     	orderAttributeList.circuitSpeed ='Test';
     	orderAttributeList.offNetCircuit ='Test';
     	    	
      
        
        smb_orders_trackingtypes_v4.OrderRequestStatus orderStatusSet=new smb_orders_trackingtypes_v4.OrderRequestStatus();
        legacyOrders.orderStatusSet=orderStatusSet;
        orderStatusSet.orderStatus='test';
        orderStatusSet.dispatchStatusCd='test';     
        orderStatusSet.fmcStatus='test';
        
        smb_orders_trackingtypes_v4.DueDate orderDueDateSet=new smb_orders_trackingtypes_v4.DueDate();
        legacyOrders.orderDueDateSet=orderDueDateSet;
        orderDueDateSet.dueDate=datetime.now();
        orderDueDateSet.creationDate=datetime.now();
        orderDueDateSet.completionDate=datetime.now();
        orderDueDateSet.originalDueDate=datetime.now();
        orderDueDateSet.erdDueDate=datetime.now();
        orderDueDateSet.prdDueDate=datetime.now();
        orderDueDateSet.ptdDueDate=datetime.now();
        
        smb_orders_trackingtypes_v4.OrderRequestId orderIdSet=new smb_orders_trackingtypes_v4.OrderRequestId();
        legacyOrders.orderIdSet=orderIdSet;
        orderIdSet.psiFoxOrder='test';
        orderIdSet.originatorId='test';
        orderIdSet.coordinatorTid='test';
        orderIdSet.traceNumber='test';
        orderIdSet.csID=1234;
        
        smb_orders_trackingtypes_v4.OrderRequestType orderTypeSet=new smb_orders_trackingtypes_v4.OrderRequestType();
        legacyOrders.orderTypeSet=orderTypeSet;
        orderTypeSet.orderServiceType='test';
        orderTypeSet.dataOrderType='test';
        orderTypeSet.orderType='test';
        orderTypeSet.voiceOrderType='test';
        
        smb_orders_trackingtypes_v4.Customer customer=new smb_orders_trackingtypes_v4.Customer();
        legacyOrders.customer=customer;
        customer.billingAddress='test';
        customer.customerName='test';
        customer.rcidName='test';
        customer.rcid='test';       
        
        smb_orders_trackingtypes_v4.OrderCompletedDate orderCompletedDateSet=new smb_orders_trackingtypes_v4.OrderCompletedDate();
        legacyOrders.orderCompletedDateSet=orderCompletedDateSet;
        orderCompletedDateSet.erdCompletedDate=datetime.now();
        orderCompletedDateSet.prdCompletedDate=datetime.now();
        orderCompletedDateSet.ptdCompletedDate=datetime.now();
        
        smb_orders_trackingtypes_v4.FacilityTrackingInfo facilityTrackingInfo=new smb_orders_trackingtypes_v4.FacilityTrackingInfo();
        legacyOrders.facilityTrackingInfo=facilityTrackingInfo;
        facilityTrackingInfo.facilityTrackingCircuit='test';
        
        smb_orders_trackingtypes_v4.OrderRequestAddress orderAddress=new smb_orders_trackingtypes_v4.OrderRequestAddress();
        List<smb_orders_trackingtypes_v4.OrderRequestAddress> orderAddressL=new List<smb_orders_trackingtypes_v4.OrderRequestAddress>();    
        legacyOrders.orderAddress=orderAddressL;
        orderAddress.Address='test';
        orderAddress.city='test';
        orderAddress.province='00000';
        orderAddressL.add(orderAddress);
        
        smb_SVOC_OrdersDetailController.orderWrapper ow = new smb_SVOC_OrdersDetailController.orderWrapper(legacyOrders);
        List<smb_SVOC_OrdersDetailController.orderWrapper> owL = new List<smb_SVOC_OrdersDetailController.orderWrapper>();
        ow.orderID='test';
         	
        ow.orderType='test';
        ow.orderRequestedDate=datetime.now();
        ow.orderStatus='test';        
        ow.orderServiceType='test';
        ow.legacyOrderType='test';
        ow.dataOrderType='test';
        ow.voiceOrderType='test';
        ow.bAddress='test';
        ow.orderDueDate=datetime.now();
        ow.systemName='test';
        ow.holdJEOP='test';
        ow.custReqDate=datetime.now();
        ow.createDate=datetime.now();
        ow.completionDate=datetime.now();
        ow.dispStatus='test';
        ow.origID='test'; 
        ow.circuitNumber='test';  
        ow.seqNumber='test';
        ow.fmcStatus='test';
        ow.custName='test';
        ow.custRCIDName='test';
        ow.custRCID='test';
        ow.origDueDate=datetime.now();
        ow.erdDueDate=datetime.now();
        ow.erdCompletedDate=datetime.now();
        ow.prdDueDate=datetime.now();
        ow.prdCompletedDate=datetime.now();
        ow.ptdDueDate=datetime.now();
        ow.ptdCompletedDate=datetime.now();
        ow.equipmentRemark='test';
        ow.dispatchRemark='test';
        ow.ecopsDiaryRemark='test';
        ow.psiFox='test';  
        ow.coordinatorTid='test';     
        ow.traceNumber='test';
        ow.csID=1234;
        ow.facilityTrackingUsso='test';
        ow.facilityTrackingCircuit='test';
        ow.outOfBandOrder='test';
        //ow.erdCustName='test';  
        ow.outOfBandCircuit='test';
        ow.circuitSpeed='test';
        ow.offNetCircuit='test';
        ow.requestAddress='test';
        ow.requestCity='test';
        ow.requestProvince='test';
        owL.add(ow);
        
    }
    
    
}