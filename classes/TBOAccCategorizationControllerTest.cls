/*
###########################################################################
# File..................: TBOAccCategorizationControllerTest
# Version...............: 1
# Created by............: Adhir Parchure (TechM)
# Created Date..........: 02/05/2016 
# Last Modified by......: Adhir Parchure (TechM)
# Last Modified Date....: 02/05/2016 
# Description...........: Test class is to provide coverage for TBOAccCategorizationController.
#
############################################################################
*/


@isTest

private Class TBOAccCategorizationControllerTest{
    
  public static testmethod void PositiveRun()
    {
        list<account> listTestacc = new list<account> ();
        Schema.DescribeSObjectResult tboSchema = Schema.SObjectType.Account;
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = tboSchema.getRecordTypeInfosByName(); 

        Schema.DescribeSObjectResult tboCaseSchema = Schema.SObjectType.Case;
        Map<String,Schema.RecordTypeInfo> caseRecordTypeInfo = tboCaseSchema.getRecordTypeInfosByName(); 

		//RCID account
		Account RCIDAcc = new Account(Name='testTBORCID',recordtypeid=AccountRecordTypeInfo .get('RCID').getRecordTypeId(), RCID__C='RCID123',  Inactive__c=false,Billing_Account_Active_Indicator__c='Y' );
        insert RCIDAcc;
		
		//Billing account1
        Account TestAcc1= new account(Name='TestAcc1',recordtypeid=AccountRecordTypeInfo.get('CAN').getRecordTypeId(), CAN__C='CAN001',BTN__c='CBN001',BCAN__c='111-CAN001',BAN_CAN__c='111-CAN001', BCAN_Pilot_Org_Indicator__c=1,Inactive__c=false,Billing_Account_Active_Indicator__c='Y');
        listTestacc.add(TestAcc1);
		
		//Billing account1
		Account TestAcc2= new account(Name='TestAcc2',recordtypeid=AccountRecordTypeInfo.get('CAN').getRecordTypeId(), CAN__C='CAN002',BTN__c='CBN002',BCAN__c='111-CAN002',BAN_CAN__c='111-CAN002', BCAN_Pilot_Org_Indicator__c=1,Inactive__c=false,Billing_Account_Active_Indicator__c='Y');
        listTestacc.add(TestAcc2);
		
		insert listTestacc;
		
		// TBO Case
        Case tboCase= new Case(Subject='testTBO',Created_Date__c=system.TODAY(),status='Draft',
                                recordtypeid=caseRecordTypeInfo.get('TBO Request').getRecordTypeId(),
                                Accountid=RCIDAcc.id);
        insert tboCase;
		
		//Test case account 1
		Case_Account__c TestCaseAcc1= new Case_Account__c( CAN__C='CAN001',BTN__c='CBN001', BCAN_Pilot_Org_Indicator__c=1, Case__c=tboCase.id, Account__c=TestAcc1.id);
        insert TestCaseAcc1;
		
		//Test case account 1
		Case_Account__c TestCaseAcc2= new Case_Account__c( CAN__C='CAN002',BTN__c='CBN002', BCAN_Pilot_Org_Indicator__c=1, Case__c=tboCase.id, Account__c=TestAcc2.id);
        insert TestCaseAcc2;
        List<Case_Account__c> listTestCaseAcc = new list<Case_Account__c>();
        listTestCaseAcc.add(TestCaseAcc1);
        listTestCaseAcc.add(TestCaseAcc2);
		
		Test.setCurrentPageReference(new PageReference('Page.TBOAccountCategorization'));
		System.currentPageReference().getParameters().put('id', tboCase.id);

		TBOAccCategorizationController testAccCat = New TBOAccCategorizationController();
		system.debug('>>> Test Id :'+testAccCat.caseId);
		ApexPages.StandardSetController setconTest= new ApexPages.StandardSetController(listTestCaseAcc);
		testAccCat.setcon=null;
		
		Pagereference pg = testAccCat.save();
		Pagereference pg1= testAccCat.Cancel();
		testAccCat.first();
		testAccCat.last();
		testAccCat.previous();
		String str= testAccCat.generateString();
		List<Case_Account__c> listTestCaseAcc1= testAccCat.getCaseAccounts();
		testAccCat.next();
		if(testAccCat.hasprevious)
			system.debug('True');
		if(testAccCat.hasnext)
			system.debug('True');
			
		testAccCat.total=10;
		str= testAccCat.generateString();
		
		//Pagereference pg = testAccCat.save();
		
		

    } 
}