/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seealldata=true)
private class smb_test_NewAccountController2 {

    static testMethod void newAccountControllerTest() {
        
       PageReference ref = new PageReference('/apex/smb_NewAccount?stid=3&retUrl=http://www.google.com'); 
       Test.setCurrentPage(ref);        
        
        
       RecordType recordType = [Select r.Id, r.DeveloperName From RecordType r where r.SobjectType = 'Account' and r.DeveloperName='RCID' limit 1];
            
       Account acc1 = new Account(Name='Testing Software', BillingCountry = 'US', BillingState = 'IL', recordTypeId=recordType.Id , No_Chronic_Incidents__c =10.0);
       insert acc1;
       
       smb_NewAccountController.displayAccount testAcct = new smb_NewAccountController.displayAccount(acc1);
       String tempString = testAcct.businessNameJSSafe;
       smb_NewAccountController.makeJSSafe('test');
       
       Contact cont = new Contact(Lastname= 'lastname', Account = acc1);
       insert cont;
       
       smb_NewAccountController smb = new smb_NewAccountController();
       
       ref = smb.save();
       smb.updatePage();
       Boolean FlagAvailableForRecordType = smb.isFieldAvailableForRecordType();
       System.assertEquals(FlagAvailableForRecordType, false);
       String recordTypeSelected = smb.getCurrentRecordTypeSelected();
       System.assertEquals(recordTypeSelected, 'RCID');
       smb.setCurrentRecordTypeSelected();
       List<SelectOption> results = smb.getRecordTypesAsSelectOptions();
       System.assertEquals((results.size() > 0), true);

       ref = smb.cancel();  
       List<SelectOption> selCountries=smb.getCountriesAsSelectOptions();
       List<SelectOption> selProv=smb.getProvincesAsSelectOptions();
       List<SelectOption> selAmer=smb.getAmericanStatesAsSelectOptions();
       String tempURLParam = smb.ctiParamString;
       tempURLParam = smb.searchTabId;
       smb.refresh();
       ref = smb.saveOverride();
       Account acc2 = new Account(Name='Testing Software', BillingCountry = 'US', BillingState = 'IL', recordTypeId=recordType.Id , No_Chronic_Incidents__c =10.0);
       smb.Account = acc2;
       ref = smb.save();
       smb.updatePage();
       ref = smb.saveOverride();       
       Account acc3 = new Account(Name='Testing', BillingCountry = 'US', BillingState = 'IL', recordTypeId=recordType.Id , No_Chronic_Incidents__c =10.0);
       smb.Account = acc3;
       ref = smb.save();
       smb.updatePage();
       ref = smb.saveOverride();
       boolean testbool = smb.openAccountInCurrentWindow;
    }          
}