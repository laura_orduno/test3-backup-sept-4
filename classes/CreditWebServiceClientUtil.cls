public class CreditWebServiceClientUtil {
    
    private static String limitLongTextLength(String longText){
        if(String.isNotBlank(longText)
           &&longText.length()>30000)
            longText = longText.substring(0, 30000);
        return longText;
    }
    
    
    public static void logCalloutErrors(
        String creditID
        , String creditObject
        , String operationOrMethod
        , String payload
        , String result
        , Integer retryCount
        , String retryStatus
        , String status
        , String createdById
        , DateTime createdDate
        , CalloutException e
    ){

        payload = limitLongTextLength(payload);    
        
        status = 'Error';
        result = e.getTypeName() 
            + e.getCause() 
            + e.getLineNumber() 
            + e.getMessage() 
            + e.getStackTraceString();
        
        CreditWebServiceClientUtil.writeToCreditRequestHistory(
            creditID
            , creditObject
            , operationOrMethod
            , payload
            , result
            , retryCount
            , retryStatus
            , status
            , createdById
            , createdDate);
        
        CreditWebServiceClientUtil.writeToWebserviceIntegrationErrorLogWithPayload(
            creditID
            , creditObject
            , operationOrMethod
            , 'BusinessCreditProfileMgmtService_v1_0_SOAP.createBusinessCreditProfile'
            , payload
            , e);
        System.debug(' ... dyy ... e.getMessage = ' + e.getMessage()); 
        System.debug(' ... dyy ... e.getCause = ' + e.getCause()); 
        System.debug(' ... dyy ... e.getStackTraceString = ' + e.getStackTraceString()); 
    }
    
    public static void writeToCreditRequestHistory(
        String creditID
        , String creditObject
        , String operationOrMethod
        , String payload
        , String result
        , Integer retryCount
        , String retryStatus
        , String status
        , String createdById
        , DateTime createdDate
    ){
        
        payload = limitLongTextLength(payload);   
        
        Credit_Request_History__c 
            requestHistory = new Credit_Request_History__c();
        requestHistory.Credit_ID__c = creditID;
        requestHistory.Credit_Object__c = creditObject;
        requestHistory.Operation_or_Method__c = operationOrMethod;
        requestHistory.Payload__c = payload;
        requestHistory.Status__c = status;
        requestHistory.Result__c = result;
        requestHistory.Retry_Count__c = retryCount;
        requestHistory.Retry_Status__c = retryStatus;
        requestHistory.CreatedById = createdById;
        requestHistory.CreatedDate = createdDate;
        insert requestHistory;
    }
    
    
    public static void writeToWebserviceIntegrationErrorLog(
        String errorRecordId
        ,String objectName
        ,String apexClassAndMethod
        ,String webserviceName
        ,CalloutException e){        
            Webservice_Integration_Error_Log__c 
                errorLog = new Webservice_Integration_Error_Log__c();
            errorLog.Object_Name__c = objectName;
            errorLog.SFDC_Record_Id__c = errorRecordId;
            errorLog.Apex_Class_and_Method__c = apexClassAndMethod;            
            errorLog.Webservice_Name__c = webserviceName;
            errorLog.Error_Code_and_Message__c = getErrorCode(e) + ', ' + e.getMessage();
            errorLog.Stack_Trace__c = e.getStackTraceString();
            insert errorLog;    
        }
    
    
    public static void writeToWebserviceIntegrationErrorLogWithPayload(
        String errorRecordId
        ,String objectName
        ,String apexClassAndMethod
        ,String webserviceName
        ,String payload
        ,CalloutException e){      
            
        payload = limitLongTextLength(payload);   
            
            Webservice_Integration_Error_Log__c 
                errorLog = new Webservice_Integration_Error_Log__c();
            errorLog.Object_Name__c = objectName;
            errorLog.SFDC_Record_Id__c = errorRecordId;
            errorLog.Apex_Class_and_Method__c = apexClassAndMethod;            
            errorLog.Webservice_Name__c = webserviceName;
            errorLog.Error_Code_and_Message__c = getErrorCode(e) + ', ' + e.getMessage() + payload;
            errorLog.Stack_Trace__c = e.getStackTraceString();
            insert errorLog;    
        }
    
    @TestVisible 
    private static String getFaultcode(CalloutException e){          
        String totalStr = e.getMessage() + e.getStackTraceString();            
        String substringAfterLastSeparator = totalStr.substringAfterLast('faultcode=');
        String errorCode = substringAfterLastSeparator.substringBeforeLast('fault');  
        return errorCode;        
    }
    
    @TestVisible 
    private static String getErrorCode(CalloutException e){          
        String totalStr = e.getMessage() + e.getStackTraceString();             
        String substringAfterLastSeparator = totalStr.substringAfterLast('ERROR CODE:');
        String errorCode = substringAfterLastSeparator.substringBeforeLast('}');  
        return errorCode;        
    }
    
    
    public static CreditAssessmentBusinessTypes.CreditAuditInfo getAssessmentAuditInfo(){     
        CreditAssessmentBusinessTypes.CreditAuditInfo 
            auditInfo = new CreditAssessmentBusinessTypes.CreditAuditInfo();  
        auditInfo.userId = UserInfo.getUserId();
        auditInfo.originatorApplicationId = 6161L;
        auditInfo.requestTimestamp = Date.today();
        return auditInfo;
    }
    
    public static CreditProfileServiceTypes.CreditAuditInfo getProfileAuditInfo(){     
        CreditProfileServiceTypes.CreditAuditInfo 
            auditInfo = new CreditProfileServiceTypes.CreditAuditInfo();  
        auditInfo.userId = UserInfo.getUserId();
        auditInfo.originatorApplicationId = 6161L;
        auditInfo.requestTimestamp = Date.today();
        return auditInfo;
    }
    
    
    public static String getQueueId(String queueName){ 
        String queueId;
        Group[] groups = [
            select Id, name, type
            from Group 
            where Type = 'Queue'
            and name = :queueName
        ];
        if(groups!=null &&groups.size()>0)
            for (Group theGroup:groups){
                queueId = theGroup.id;
                break;
            }
        return queueId;
    }
    
    
    
    public static String getPhoneNumberDigitOnly(String phoneNumber){  
        if(phoneNumber==null) return null;
        phoneNumber = phoneNumber.remove('(');
        phoneNumber = phoneNumber.remove(')');
        phoneNumber = phoneNumber.remove('-');
        phoneNumber = phoneNumber.remove(' ');
        return phoneNumber;
    }
    
    
    public static String removeAllSpaces(String str){  
        if(str==null) return null;
        str = str.remove(' ');
        return str;
    }
    
    
    public static String removeLeadingZeros(String str){  
        if(str==null) return null;
        while(str!=null &&str.startsWith('0'))
            str = str.removeStart('0');
        return str;
    }

    
    public static String getShortId(String str){  
        if(str==null) return null;
        Integer strLenth = str.length();
        str = str.substring(0, strLenth-3);
        return str;
    }
    
    
    public static Boolean isLong (String str){
        Boolean isLong = false;
        try{
            Long.valueof(str);
            isLong = true;
        }catch(Exception e){
            System.debug(str + ' is not of Long data type');
        }
        return isLong;
    }
    
    
    public static List<String> getListOfStringsPerLinebreak (String othernames){
        if(othernames==null) return null;
        List<String> listOfStrings = new List<String> ();               
        othernames = othernames.replace('\r\n', 'dyylinebreak');
        othernames = othernames.replace('\n', 'dyylinebreak');
        othernames = othernames.replace('\r', 'dyylinebreak');
        Integer countMatches = othernames.countMatches('dyylinebreak');            
        Boolean hasLineBreaks = true;
        while(hasLineBreaks){
            System.debug('... dyy ... countMatches = ' + countMatches);
            String subStr = othernames.substringBefore('dyylinebreak');
            othernames = othernames.substringAfter('dyylinebreak');
            if(subStr!=null) listOfStrings.add(subStr);  
            countMatches = countMatches-1;
            hasLineBreaks = (countMatches>-1);
        }
        return listOfStrings;
    }
    

    
    public static String getStringOfLimit(String str, Integer strLimit){  
        if(String.isBlank(str)) return null;
        str = str.trim();
        Integer strLenth = str.length();
        if(strLenth>strLimit){
            str = str.substring(0, strLimit);
        }
        return str;
    }
    
    
}