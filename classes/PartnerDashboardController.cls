public with sharing class PartnerDashboardController {
	public Chart unreadRenewalLeadsChart {get; private set;}
	public Chart openLeadCountThisMonthChart {get; private set;}
	public Chart openProspectLeadsChart {get; private set;}
	public Chart opportunitiesClosingThisMonthChart {get; private set;}
	public Chart opportunitiesWonThisMonthChart {get; private set;}
	public List<DashboardRow> unreadProspectLeadsDashboard {get; private set;}
	public Chart unreadProspectLeadsChart {get; private set;}
	
	public PageReference onGenerate() {
		chartUnreadRenewalLeads();
		chartOpenLeadCountThisMonth();
		chartOpenProspectLeads();
		chartOpportunitiesClosingThisMonth();
		chartOpportunitiesWonThisMonth();
		chartUnreadProspectLeads();
		return null;
	}
	
	private void chartUnreadRenewalLeads() {
		Integer maxCount = 0;
		Map<String,Integer> leadCountsByOwner = new Map<String,Integer>();
		for (Lead lead : [
			SELECT Owner.Name
			FROM Lead
			WHERE IsUnreadByOwner = true
			  AND RecordTypeId = '01240000000DOGM'
		]) {
			Integer count = leadCountsByOwner.get(lead.Owner.Name);
			if (count == null) {
				count = 0;
			}
			leadCountsByOwner.put(lead.Owner.Name, count + 1);
			maxCount = Math.max(maxCount, count + 1);
		}
		
		unreadRenewalLeadsChart = new Chart(leadCountsByOwner);
	}
	
	private void chartUnreadProspectLeads() {
		Integer maxCount = 0;
		Map<String,Integer> leadCountsByOwner = new Map<String,Integer>();
		for (Lead lead : [
			SELECT Owner.Name
			FROM Lead
			WHERE IsUnreadByOwner = true
			  AND (RecordTypeId = '01240000000DOGC' OR RecordTypeId = '01240000000DOGH')
		]) {
			Integer count = leadCountsByOwner.get(lead.Owner.Name);
			if (count == null) {
				count = 0;
			}
			leadCountsByOwner.put(lead.Owner.Name, count + 1);
			maxCount = Math.max(maxCount, count + 1);
		}
		unreadProspectLeadsChart = new Chart(leadCountsByOwner);
	}
	
	private void chartOpenProspectLeads() {
		DateTime endDate = System.now().addDays(-7);
		Integer maxCount = 0;
		Map<String,Integer> leadCountsByOwner = new Map<String,Integer>();
		for (Lead lead : [
			SELECT Owner.Name
			FROM Lead
			WHERE CreatedDate < :endDate
			  AND Status = 'Open'
			  AND (RecordTypeId = '01240000000DOGC' OR RecordTypeId = '01240000000DOGH')
		]) {
			Integer count = leadCountsByOwner.get(lead.Owner.Name);
			if (count == null) {
				count = 0;
			}
			leadCountsByOwner.put(lead.Owner.Name, count + 1);
			maxCount = Math.max(maxCount, count + 1);
		}
		
		openProspectLeadsChart = new Chart(leadCountsByOwner);
	}
	
	private void chartOpportunitiesClosingThisMonth() {
		Integer maxCount = 0;
		Date endDate = System.today().addMonths(1);
		Map<String,Integer> oppCountsByOwner = new Map<String,Integer>();
		for (Opportunity opp : [
			SELECT Owner.Name
			FROM Opportunity
			WHERE CloseDate >= :System.today()
			  AND CloseDate <= :endDate
			  AND IsClosed = false
		]) {
			Integer count = oppCountsByOwner.get(opp.Owner.Name);
			if (count == null) {
				count = 0;
			}
			oppCountsByOwner.put(opp.Owner.Name, count + 1);
			maxCount = Math.max(maxCount, count + 1);
		}
		
		opportunitiesClosingThisMonthChart = new Chart(oppCountsByOwner);
	}
	
	private void chartOpportunitiesWonThisMonth() {
		Integer maxCount = 0;
		Date startDate = Date.newInstance(System.today().year(), System.today().month(),1);
		Date endDate = startDate.addMonths(1).addDays(-1);
		Map<String,Integer> oppCountsByOwner = new Map<String,Integer>();
		for (Opportunity opp : [
			SELECT Owner.Name
			FROM Opportunity
			WHERE CloseDate >= :startDate
			  AND CloseDate <= :endDate
			  AND IsWon = true
		]) {
			Integer count = oppCountsByOwner.get(opp.Owner.Name);
			if (count == null) {
				count = 0;
			}
			oppCountsByOwner.put(opp.Owner.Name, count + 1);
		}
		
		opportunitiesWonThisMonthChart = new Chart(oppCountsByOwner);
	}
	
	private void chartOpenLeadCountThisMonth() {
		DateTime startDate = DateTime.newInstance(System.today().year(), System.today().month(),1,0,0,0);
		DateTime endDate = startDate.addMonths(1);
		Integer count = [
			SELECT Count()
			FROM Lead
			WHERE CreatedDate >= :startDate
			  AND CreatedDate < :endDate
		];
		
		openLeadCountThisMonthChart = new Chart(count, String.valueOf(count));
	}
	
	public class Chart {
		// Chart data
		private String chd;  
		// Chart data scaling
		private String chds;
		// Chart labels
		private String chl;
		// Axis ranges
		private String chxr;
		
		private Integer labelCount;
		
		public Chart(Map<String,Integer> countsByName) {
			labelCount = countsByname.keySet().size();
			
			Integer maxCount = 0;
			chd = 't:';
			chl = '';
			for (String name : countsByName.keySet()) {
				
				if(name != null) {
					chl = chl + EncodingUtil.urlEncode(abbreviateName(name),'UTF-8') + '|';						
					chd = chd + countsByName.get(name) + ',';
					maxCount = Math.max(maxCount, countsByName.get(name));
				}
			}
			// Chop off trailing separator
			if (chd.length() > 0) {
				chd = chd.substring(0, chd.length()-1);
			}
			if (chl.length() > 0) {
				chl = chl.substring(0, chl.length()-1);
			}
			
			// Make chart look decent in absemce of data
			if (chd == 't') {
				chd = 't:0';
			}
			if (maxCount == 0) {
				maxCount = 1;
			}
			Integer step = Math.max(1, Math.round(Decimal.valueOf(maxCount) / Decimal.valueOf(5)));
			chxr = '1,0,' + maxCount + ',' + step;
			chds = '0,' + maxCount;
		}
		
		public Chart(Integer value, String label) {
			chd = 't:' + value;
			chl = label;
		}
		
		private String abbreviateName(String name) {
			if (name == null) {
				return null;
			}
			String[] parts = name.split(' ');
			if (parts.size() == 1) {
				return name;
			}
			return parts[0].substring(0,1) + '. ' + parts[parts.size()-1];
		}
		
		public Integer getLabelCount() {
			return labelCount;
		}
		
		public String getChd() {
			return chd;
		}
		
		public String getChl() {
			return chl;
		}
		
		public String getChxr() {
			return chxr;
		}
		
		public String getChds() {
			return chds;
		}
	}
	
	public class DashboardRow {
		private String name;
		private Integer count;
		
		public DashboardRow(String name, Integer count) {
			this.name = name;
			this.count = count;
		}
		
		public String getName() {
			return name;
		}
		
		public Integer getCount() {
			return count;
		}
	}

}