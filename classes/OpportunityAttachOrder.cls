global class OpportunityAttachOrder {
    List<Order> orders;
    Opportunity opp;
    private static Map<Id, OpportunityOrderInfo> opiIdToOrdiInfoMap = new Map<Id, OpportunityOrderInfo>();
    public OpportunityAttachOrder(ApexPages.StandardController controller){
        opp=(Opportunity)controller.getRecord();
    }
    public List<Order> getOrders() {
        if(orders == null){
            //orders = [SELECT Id, Account.name, Status, Description, Name, OrderNumber, TotalAmount, vlocity_cmt__AccountId__c, vlocity_cmt__OneTimeTotal__c, vlocity_cmt__RecurringTotal__c,  Opportunity_Name__c, RequestedDate__c, Scheduled_Due_Date__c FROM Order where vlocity_cmt__OpportunityId__c=:opp.id LIMIT 10];
            //001c0000018mO64AAE
        orders = [SELECT Id, Account.name, Status, Description, Name, OrderNumber, TotalAmount, vlocity_cmt__AccountId__c, vlocity_cmt__OneTimeTotal__c, vlocity_cmt__RecurringTotal__c,  Opportunity_Name__c, RequestedDate__c, Scheduled_Due_Date__c FROM Order where accountid='001c0000018mO64AAE' LIMIT 10];
        }
        
        return orders;
    }
    
    webservice static String attachOrder(Id opportunityId, String opiJSON){
        System.debug('aaaaaaaaaaaaaaaaaaa '+opiJSON);
		List<String> ordIdList = extractOrdIdList(opiJSON);
        System.debug('aaaaaaaaaaaaaaaaaaa '+ordIdList);
        List<Order> ordList=[select vlocity_cmt__OpportunityId__c,opportunityid from order where id in: ordIdList];
            for(Order ordObj:ordList){
                ordObj.vlocity_cmt__OpportunityId__c=opportunityId;
                ordObj.opportunityid=opportunityId;
            }
        Database.SaveResult[] saveResultlist=Database.update(ordList, true);
        for (Database.SaveResult sr : saveResultlist) {
             if (sr.isSuccess()) {
                 continue;
             }
            return 'false';
        }
        return 'true';
    }
    @TestVisible
    private static List<String> extractOrdIdList(String opiJson){
        List<String> ordIdList=new List<String>();
		List<OpportunityOrderInfo> opiInfoList = (List<OpportunityOrderInfo>)JSON.deserialize(opiJson, List<OpportunityOrderInfo>.class);
		Map<Id, OpportunityOrderInfo> opportunityOrderInfoMap = new Map<Id, OpportunityOrderInfo>();

		for(OpportunityOrderInfo currentOpportunityOrderInfo : opiInfoList){
			//opportunityOrderInfoMap.put(currentOpportunityOrderInfo.recordId, currentOpportunityOrderInfo);
			ordIdList.add(currentOpportunityOrderInfo.recordId);
		}

		return ordIdList;
	}
    
    public class OpportunityOrderInfo{
		public Id recordId {get; set;}
		public OpportunityOrderInfo(Id recordId){
			this.recordId = recordId;			
		}
		
	}
    
    public PageReference newOrder(){
        return null;
    }
    public PageReference attachOrder(){
        return null;
    }
}