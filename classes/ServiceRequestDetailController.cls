/******************************************************************************
# File..................: ServiceRequestDetailController
# Version...............: 1
# Created by............: Sandip Chaudhari (IBM)
# Description...........: Class is to Create, Validate, Save Billing Address for Service Request. 
                          This class is main controller for detail page. Created as a part of PAC R2
******************************************************************************/

Public class ServiceRequestDetailController{
    public AddressData PAData  { get; set; }
    Service_Request__c saObj;
    public String isError{ get; set; }
    
    public ServiceRequestDetailController(ApexPages.StandardController controller){
        isError = 'onLoad';
        PAData = new AddressData();
        PAData.isAddressLine1Required = true;
        PAData.isProvinceRequired = true;
        PAData.isCityRequired = true;
        saObj = (Service_Request__c)controller.getRecord();
        saObj = [SELECT Id, Billing_Address__c, Billing_Address_Status__c
                        FROM Service_Request__c
                        WHERE Id=: saObj.Id];
        if(saObj != null){
            PAData.searchString = saObj.Billing_Address__c;
            if(saObj.Billing_Address_Status__c == System.Label.PACInValidCaptureNewAddrStatus){
                PAData.isManualCapture = true;
            }else{
                PAData.isManualCapture = false;
            }
            
            //Split the address devided by comma and assigned to respective vaiables for AddressData class
            if(saObj.Billing_Address__c != null){
                integer i=0;
                string[] addrPart = saObj.Billing_Address__c.split(',');
                if(addrPart != null && addrPart.size() > 0){
                    if(addrPart.size() > i){
                        PAData.addressLine1 = addrPart[i];
                       // PAData.onLoadSearchAddress = addrPart[i];
                        i++;
                    }
                    if(addrPart.size() > i){
                        if(addrPart[i] != null){
                            PAData.city  = addrPart[i].trim();
                            i++;
                        }
                    }
                    if(addrPart.size() > i){
                        if(addrPart[i] != null){
                            PAData.canadaProvince = addrPart[i].trim();
                            PAData.usState = addrPart[i].trim();
                            PAData.province = addrPart[i].trim();
                        }
                        i++;
                    }
                    if(addrPart.size() > i){
                        if(addrPart[i] != null){
                            PAData.country = addrPart[i].trim();
                        }
                        i++;
                    }
                    
                    if(addrPart.size() > i){
                        if(addrPart[i] != null){
                            PAData.postalCode = addrPart[i].trim();
                        }
                    }
                }
            }
        }
    }
    
     /**
    * Name - mapAddress
    * Description - Update Billing address in existing Billing_Address__c field
    * Param - 
    * Return type - void
    **/
    
    public pageReference updateBillingAddress (){
        mapAddress();
        try{
            isError = 'inSave';
            update saObj;
        }catch(Exception ex){
            isError = 'InError';
            ApexPages.addMessages(ex) ;
        }
        //PageReference redirectPage = new PageReference('/apex/ServiceRequestDetail?id='+ saObj.Id +'&sfdc.override=1');
        //redirectPage.setRedirect(true);
        //return redirectPage;
        return null;
    }
    
    /**
    * Name - mapAddress
    * Description - Map Billing address with existing Billing_Address__c field
    * Param - 
    * Return type - void
    **/
    public void mapAddress(){
        system.debug('#### City' + PAData.city);
        system.debug('#### postalCode' + PAData.postalCode);
        String address = '';
        if(PAData.addressLine1 != null)
            address  = PAData.addressLine1.trim();
        address  = address  + ', ' + PAData.city;
        address  = address  + ', ' + PAData.province;
        address  = address  + ', ' + PAData.country;
        address  = address  + ', ' + PAData.postalCode;
        saObj.Billing_Address__c = address;
        saObj.Billing_Address_Status__c = PAData.captureNewAddrStatus;
    }
}