global class OrdrTnHangingTnsBatch implements Database.Batchable<sObject>,Database.AllowsCallouts , Schedulable {

 public String query;

    global OrdrTnHangingTnsBatch(String query){
        this.query=query;
    }
     global OrdrTnHangingTnsBatch(){
        
    }
   global Database.QueryLocator start(Database.BatchableContext BC){
   Datetime batchDateTime = Datetime.now().addHours(-8);
   String tnStatus='RESERVED_UNASSIGNED';
       if(String.isBlank(query)){
           query='select  Address__c,COID__c, CacheKey__c, FMS_Id__c, Is_TN_in_Use__c, LPDS_Id__c, LineNum__c, LineNumPrefix__c, NPA__c, NXX__c, '+
     ' Non_Native_COID__c, Order_Id__c, order_Item_id__c, Product_Type__c, Province__c, Reservation_Type__c,  '+
     ' Switch_Number__c, TN_Reservation_Key__c, Telephone_Number__c, defaultInd__c, status__c, CreatedById, '+
     ' CreatedDate, CurrencyIsoCode, IsDeleted, LastModifiedById, LastModifiedDate, OwnerId, ConnectionReceivedId, Id, RecordTypeId, '+
     ' ConnectionSentId, SystemModstamp, Name FROM TELUS_TN_RESERVATION__c where order_Item_id__c=null  and CreatedDate<:batchDateTime order by Address__c desc';
       }
     
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<sObject> scope){
   
    Map<String,List<OrdrTnUnReserveData>> unreserveDataListMap=new Map<String,List<OrdrTnUnReserveData>>();
    String keyStr=null;
		List<TELUS_TN_RESERVATION__c> tnCacheTobeCleared= new List<TELUS_TN_RESERVATION__c>();
    for(sObject tnSObj:scope){
        OrdrTnUnReserveData unreserveData =null;
        if(tnSObj instanceof TELUS_TN_RESERVATION__c){
            TELUS_TN_RESERVATION__c tnObj=(TELUS_TN_RESERVATION__c)tnSObj;
            if(String.isBlank(tnObj.Address__c)){
                    continue;
                }
                if((String.isBlank(tnObj.Telephone_Number__c))||(String.isNotBlank(tnObj.Telephone_Number__c) && (tnObj.Telephone_Number__c.indexOf(OrdrConstants.NO_SPARE_TN)!=-1))){
                    continue;
                }
				if(String.isBlank(tnObj.status__c) || String.valueOf(OrdrTnLineNumberStatus.AVAILABLE).equals(tnObj.status__c)){
					tnCacheTobeCleared.add(tnObj);
					continue;
				}
				if(!'RESERVED_UNASSIGNED'.equals(tnObj.status__c)){					
					continue;
				}
                keyStr=String.valueOf(tnObj.Order_Id__c)+String.valueOf(tnObj.Address__c)+String.valueOf(tnObj.Reservation_Type__c);
                List<OrdrTnUnReserveData> alreadyStoredTnList=null;
                if(String.isNotBlank(keyStr) && unreserveDataListMap.containsKey(keyStr)){
                    alreadyStoredTnList=unreserveDataListMap.get(keyStr);
                    if(alreadyStoredTnList==null || alreadyStoredTnList.size()==0){
                        continue;
                    }
                    OrdrTnUnReserveData unresData=alreadyStoredTnList.get(0);
                    String orderId=unresData.getOrderId();        
                    String orderItemId=unresData.getOrderItemId();                              
                    SMBCare_Address__c addressObj=unresData.getAddressObj();                                
                    String addressId=unresData.getAddressId();                              
                    String reservationType=unresData.getReservationType();
                    List<String> tnListToUnreserve=unresData.getTnList();
                    if(tnListToUnreserve==null){
                        tnListToUnreserve=new List<String>();
                    }
                    Set<String> tnSet=new Set<String>();
                    tnSet.addAll(tnListToUnreserve);
                    if(!tnSet.contains(tnObj.Telephone_Number__c)){
                        tnListToUnreserve.add(tnObj.Telephone_Number__c);
                        unresData.setTnList(tnListToUnreserve); 
                    }
                    
                } else {                    
                    alreadyStoredTnList=new List<OrdrTnUnReserveData>();
                    
                    unreserveData=new OrdrTnUnReserveData();
                    unreserveData.setOrderId(tnObj.Order_Id__c);
                    unreserveData.setOrderItemId(tnObj.order_Item_id__c);
                    unreserveData.setAddressId(tnObj.Address__c);
                    unreserveData.setReservationType(tnObj.Reservation_Type__c);
                    SMBCare_Address__c addressObj=new SMBCare_Address__c(id=tnObj.Address__c);
                    unreserveData.setAddressObj(addressObj);
                    List<String> tnListToUnreserve=new List<String>();
                    tnListToUnreserve.add(tnObj.Telephone_Number__c);
                    unreserveData.setTnList(tnListToUnreserve); 
                    alreadyStoredTnList.add(unreserveData);
                    unreserveDataListMap.put(keyStr,alreadyStoredTnList);
                    continue;
                }
            }
        }
        
        
        for(String mapKey:unreserveDataListMap.keySet()){
            List<OrdrTnUnReserveData> dataObList=unreserveDataListMap.get(mapKey);
            for(OrdrTnUnReserveData dataObj:dataObList){
                List<String> tnList=dataObj.getTnList();
                OrdrTnReservationType reservationType=null;
                String resTypeStr=dataObj.getReservationType();
                //WLN, TOLLFREE, WLS
                if(String.isNotBlank(resTypeStr) && resTypeStr.equals(String.valueOf(OrdrTnReservationType.WLN))){
                    reservationType=OrdrTnReservationType.WLN;
                }
                if(String.isNotBlank(resTypeStr) && resTypeStr.equals(String.valueOf(OrdrTnReservationType.TOLLFREE))){
                    reservationType=OrdrTnReservationType.TOLLFREE;
                }
                if(String.isNotBlank(resTypeStr) && resTypeStr.equals(String.valueOf(OrdrTnReservationType.WLS))){
                    reservationType=OrdrTnReservationType.WLS;
                }
                try{
                    OrdrTnReservationImpl.unreserveOrdrTnList(dataObj.getOrderId(),dataObj.getOrderItemId(),dataObj.getAddressObj(), tnList, 'BATCH', '', reservationType);
                }Catch(Exception e){
                    System.debug('Exception occurred during TN Unreserve through batch job '+e);
                }
                
            }
        }
		if (tnCacheTobeCleared!=null && tnCacheTobeCleared.size()>0) {
                delete tnCacheTobeCleared;                              
            }
   }
   /*global void execute(Database.BatchableContext BC, List<sObject> scope){
   
    Map<String,List<OrdrTnUnReserveData>> unreserveDataListMap=new Map<String,List<OrdrTnUnReserveData>>();
    String keyStr=null;
    for(sObject tnSObj:scope){
        OrdrTnUnReserveData unreserveData =null;
        if(tnSObj instanceof TELUS_TN_RESERVATION__c){
            TELUS_TN_RESERVATION__c tnObj=(TELUS_TN_RESERVATION__c)tnSObj;
            if(String.isBlank(tnObj.Address__c)){
                    continue;
                }
            if((String.isBlank(tnObj.Telephone_Number__c))||(String.isNotBlank(tnObj.Telephone_Number__c) && (tnObj.Telephone_Number__c.indexOf(OrdrConstants.NO_SPARE_TN)!=-1))){
                    continue;
                }
             keyStr=String.valueOf(tnObj.Order_Id__c)+String.valueOf(tnObj.Address__c)+String.valueOf(tnObj.Reservation_Type__c);
        List<OrdrTnUnReserveData> alreadyStoredTnList=null;
       if(String.isNotBlank(keyStr) && unreserveDataListMap.containsKey(keyStr)){
            alreadyStoredTnList=unreserveDataListMap.get(keyStr);
         } 

         if(alreadyStoredTnList==null){
            alreadyStoredTnList=new List<OrdrTnUnReserveData>();
            unreserveData=new OrdrTnUnReserveData();
                    unreserveData.setOrderId(tnObj.Order_Id__c);
                    unreserveData.setOrderItemId(tnObj.order_Item_id__c);
                    unreserveData.setAddressId(tnObj.Address__c);
                    unreserveData.setReservationType(tnObj.Reservation_Type__c);
                    SMBCare_Address__c addressObj=new SMBCare_Address__c(id=tnObj.Address__c);
                    unreserveData.setAddressObj(addressObj);
                    List<String> tnListToUnreserve=new List<String>();
                    tnListToUnreserve.add(tnObj.Telephone_Number__c);
                     unreserveData.setTnList(tnListToUnreserve); 
                    alreadyStoredTnList.add(unreserveData);
                    unreserveDataListMap.put(keyStr,alreadyStoredTnList);
                    continue;           
            
         } 
         
         if(alreadyStoredTnList.size()>0){
            Boolean isFound=false;
                    for(OrdrTnUnReserveData unresData:alreadyStoredTnList){
                        String orderId=unresData.getOrderId();        
                        String orderItemId=unresData.getOrderItemId();                              
                        SMBCare_Address__c addressObj=unresData.getAddressObj();                                
                        String addressId=unresData.getAddressId();                              
                        String reservationType=unresData.getReservationType();
                        List<String> tnListToUnreserve=unresData.getTnList();
                            if(tnListToUnreserve==null){
                                tnListToUnreserve=new List<String>();
                            }
                        Boolean matchOrderData=((String.isBlank(orderId) && String.isBlank(tnObj.Order_Id__c)) || (String.isNotBlank(orderId) && String.isNotBlank(tnObj.Order_Id__c) && orderId.equals(tnObj.Order_Id__c)));
                        Boolean matchOrderItemData=((String.isBlank(orderItemId) && String.isBlank(tnObj.order_Item_id__c)) || (String.isNotBlank(orderItemId) && String.isNotBlank(tnObj.order_Item_id__c) && orderItemId.equals(tnObj.order_Item_id__c)));
                        Boolean matchAddressIdData=((String.isBlank(addressId) && String.isBlank(tnObj.Address__c)) || (String.isNotBlank(addressId) && String.isNotBlank(tnObj.Address__c) && addressId.equals(tnObj.Address__c)));
                        Boolean matchReservationTypeData=((String.isBlank(reservationType) && String.isBlank(tnObj.Reservation_Type__c)) || (String.isNotBlank(reservationType) && String.isNotBlank(tnObj.Reservation_Type__c) && reservationType.equals(tnObj.Reservation_Type__c)));
                                                
                         if(matchOrderData && matchOrderItemData &&  matchAddressIdData && matchReservationTypeData){
                             
                              Set<String> tnSet=new Set<String>();
                                tnSet.addAll(tnListToUnreserve);
                                if(!tnSet.contains(tnObj.Telephone_Number__c)){
                                    tnListToUnreserve.add(tnObj.Telephone_Number__c);
                                    unresData.setTnList(tnListToUnreserve); 
                                }
                            isFound=true;   
                            break;  
                         }      
                    }
                    if(isFound){
                        continue;
                    }
                    unreserveData=new OrdrTnUnReserveData();
                    unreserveData.setOrderId(tnObj.Order_Id__c);
                    unreserveData.setOrderItemId(tnObj.order_Item_id__c);
                    unreserveData.setAddressId(tnObj.Address__c);
                    unreserveData.setReservationType(tnObj.Reservation_Type__c);
                    SMBCare_Address__c addressObj=new SMBCare_Address__c(id=tnObj.Address__c);
                    unreserveData.setAddressObj(addressObj);
                    List<String> tnListToUnreserve=new List<String>();
                    tnListToUnreserve.add(tnObj.Telephone_Number__c);
                     unreserveData.setTnList(tnListToUnreserve); 
                    alreadyStoredTnList.add(unreserveData);
                    unreserveDataListMap.put(keyStr,alreadyStoredTnList);
         } 
        
        }
       
    }

    for(String mapKey:unreserveDataListMap.keySet()){
            List<OrdrTnUnReserveData> dataObList=unreserveDataListMap.get(mapKey);
            for(OrdrTnUnReserveData dataObj:dataObList){
                List<String> tnList=dataObj.getTnList();
                OrdrTnReservationType reservationType=null;
                String resTypeStr=dataObj.getReservationType();
                //WLN, TOLLFREE, WLS
                if(String.isNotBlank(resTypeStr) && resTypeStr.equals(String.valueOf(OrdrTnReservationType.WLN))){
                    reservationType=OrdrTnReservationType.WLN;
                }
                if(String.isNotBlank(resTypeStr) && resTypeStr.equals(String.valueOf(OrdrTnReservationType.TOLLFREE))){
                    reservationType=OrdrTnReservationType.TOLLFREE;
                }
                if(String.isNotBlank(resTypeStr) && resTypeStr.equals(String.valueOf(OrdrTnReservationType.WLS))){
                    reservationType=OrdrTnReservationType.WLS;
                }
                try{
                    OrdrTnReservationImpl.unreserveOrdrTnList(dataObj.getOrderId(),dataObj.getOrderItemId(),dataObj.getAddressObj(), tnList, 'BATCH', '', reservationType);
                }Catch(Exception e){
                    System.debug('Exception occurred during TN Unreserve through batch job '+e);
                }
                
            }
        }

   }
 */
   global void execute(SchedulableContext sc) {
      OrdrTnHangingTnsBatch batch = new OrdrTnHangingTnsBatch(); 
      Database.executebatch(batch);
   }


   global void finish(Database.BatchableContext BC){
    
   }
   class OrdrTnUnReserveData{
     String orderId;
     String orderItemId;
     SMBCare_Address__c addressObj;
     String addressId;
     List<String> tnList;
     String reservationType;
     public String getKeyStr() {
       String ketStr='';
       if(String.isNotBlank(getOrderId())){
            ketStr=ketStr+getOrderId();
       }
       if(String.isNotBlank(getAddressId())){
            ketStr=ketStr+getAddressId();
       }
       if(String.isNotBlank(getReservationType())){
            ketStr=ketStr+getReservationType();
       }
      
        return ketStr;
    }
    public SMBCare_Address__c getAddressObj() {
        return addressObj;
    }

    public void setAddressObj(SMBCare_Address__c addressObj) {
        this.addressObj = addressObj;
    }
    public String getOrderId() {
        return orderId;
    }
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
    public String getOrderItemId() {
        return orderItemId;
    }
    public void setOrderItemId(String orderItemId) {
        this.orderItemId = orderItemId;
    }
    public String getAddressId() {
        return this.addressId;
    }
    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }
    public List<String> getTnList() {
        return tnList;
    }
    public void setTnList(List<String> tnList) {
        this.tnList = tnList;
    }
    public String getReservationType() {
        return reservationType;
    }
    public void setReservationType(String reservationType) {
        this.reservationType = reservationType;
    }
   }

 }