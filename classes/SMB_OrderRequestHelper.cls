public with sharing class SMB_OrderRequestHelper 
{
    public static void updateOpportunityStatus(list<Order_Request__c> lstOrderRequest, Boolean isUpdate, map<Id,Order_Request__c> mapOldOrderRequest)
    {
        map<Id,String> mapOpportunityStatus = new map<Id, String>();
        set<Id> setOppIds = new set<Id>();
        set<Id> setORIds = new set<Id>();
        list<Opportunity> lstOpportunity = new list<Opportunity>();
                
        RecordType objRecTypeOrder = [Select Id from RecordType where sObjectType = 'Opportunity' and isActive = true and DeveloperName = 'SMB_Care_Amend_Order_Opportunity' limit 1];
        for(Order_Request__c objQR: lstOrderRequest){
            if(objQR.Order__c != null && objQR.Order__c != ''){
                if(isUpdate){
                        if(objQR.Order__c != mapOldOrderRequest.get(objQR.Id).Order__c){
                            setORIds.add(objQR.Id);
                            setOppIds.add(objQR.Opportunity__c);
                            if(!mapOpportunityStatus.containsKey(objQR.Opportunity__c)){
                                mapOpportunityStatus.put(objQR.Opportunity__c,objQR.Order__c);
                            }
                        }
                }
                else{
                    setORIds.add(objQR.Id);
                    setOppIds.add(objQR.Opportunity__c);
                    if(!mapOpportunityStatus.containsKey(objQR.Opportunity__c)){
                        mapOpportunityStatus.put(objQR.Opportunity__c,objQR.Order__c);
                     }
                }
            }
        }
        
        if(setORIds.size()>0){
            list<Service_Request__c> lstSR = [Select Id, Opportunity__c, Order_Request_Lookup__c, Status__c from Service_Request__c where Order_Request_Lookup__c IN: setORIds and Status__c <> 'Cancelled' and Order_Request_Lookup__r.Order__c = 'Cancelled'];
            if(lstSR!= null && lstSR.size()>0){
                for(Service_Request__c objSR:lstSR){
                    if(setOppIds.contains(objSR.Opportunity__c))
                        setOppIds.Remove(objSR.Opportunity__c);
                }
            }
        }
            
        if(setOppIds.size()>0)
        {
        	lstOpportunity = [Select Id, StageName, recordTypeId from Opportunity where Id IN: setOppIds];
            if(lstOpportunity.size()>0)
            {
                for(Opportunity objOpp:lstOpportunity)
                {
                   // BRW 02-06-2015 Added the Order Request Cancelled stage to the exclusion list
                   // because no stage transition can occur once the oppty has moved to Order Request Cancelled
                   if(objOpp.StageName != 'Order Request Cancelled')
                   {
                         if(!((objOpp.StageName == 'Contract On Hold (Customer)' || 
                               objOpp.StageName == 'Order Request on Hold (Customer)') &&
                               objOpp.RecordTypeId == objRecTypeOrder.Id)){
                       
                            if(mapOpportunityStatus.containsKey(objOpp.Id))
                            {
                                if(mapOpportunityStatus.get(objOpp.Id) == 'Need Additional Information'){
                                    objOpp.StageName = 'Order Request Needs More Info';
                                    /*
									* Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
									* START
									*/
										objOpp.Stage_update_time__c=System.Now();
									/*
									* Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
									* END
									*/
                                }
                                else{
                                    objOpp.StageName = 'Order Request ' + mapOpportunityStatus.get(objOpp.Id);
                                    /*
									* Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
									* START
									*/
										objOpp.Stage_update_time__c=System.Now();
									/*
									* Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
									* END
									*/
                                }
                                update lstOpportunity;
                            }
                         }
                   }
                }
            }
         }
    }
}