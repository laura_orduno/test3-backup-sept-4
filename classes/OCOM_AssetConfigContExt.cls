public with sharing class OCOM_AssetConfigContExt {

    Account acct; 
    public OCOM_AssetConfigContExt(ApexPages.StandardController stdController) {
        this.acct= (Account)stdController.getRecord();
    }

    public  void calibrateServiceAccountAssets(){
        if(this.acct.id != null){
            OrdrAssignedProductsManager.calibrateServiceAccountAssets(this.acct.id);
        }
    }

}