global with sharing  class OC_ContactController  implements vlocity_cmt.VlocityOpenInterface{
   
     public Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {
         
     if(methodName == 'createnewContact')
     {
         List<Object> signors = new List<Object>();
         Map<String,Object> responseMap = new Map<String,Object>();
         system.debug('***CustomerSignor'+Options.get('OrderContactToNewContact'));
        
         Map<string, object> contactmap=createnewContact(inputMap,outMap,options);
        
         if (contactmap.get('Id')!= null ) 
         {             
             signors.add(contactmap);
             //responseMap.put('OrderContact',signors);
             system.debug('***signors'+signors);
             //outMap.put('newContacts', options.put('CustomerSignor', signors)); 
        //options.put('OrderContactToNewContact', signors);
             //outMap.put('newContacts', signors);
        //outMap.put('OrderContact',responseMap.get('OrderContact'));
             //outMap.put('newContacts', responseMap.get('OrderContact'));
             //outMap.put('CustomerSignor', responseMap.get('OrderContact'));
             system.debug('***outmap newContacts'+outMap.get('newContacts'));
         }
         else 
         {           
             outMap.put('error','* fill the required fields to create a New Contact');
             return false;
         }
         
         if (Options.get('OrderContactToNewContact') != null)
        {
            signors.addAll((List<Object>) Options.get('OrderContactToNewContact'));
        }
        options.put('OrderContactToNewContact', signors);
        responseMap.put('OrderContact',signors);
        outMap.put('OrderContact',responseMap.get('OrderContact'));
        
        //Added by Jaya to blank out the New Contact fields.
        Map<String, Object> NewContactNode = (Map<String, Object>)inputMap.get('createnewContact');
        //String firstName = (String)NewContactNode.get('FirstName');
        outMap.put((String)NewContactNode.get('FirstName'),'');
        outMap.put((String)NewContactNode.get('LastName'),'');
        outMap.put((String)NewContactNode.get('Phone'),'');
        //outMap.put(NewContactNode,null);
        
        outMap.put('FirstName','');
        outMap.put('LastName','');
        outMap.put('Phone','');
     }
    return false;
     }
    
    private Map<string, object> createnewContact(Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options)
    {
       
        Map<String, Object> NewContactNode = (Map<String, Object>)inputMap.get('createnewContact');
        system.debug('***NewContactNode'+NewContactNode);
        Map<string, String> conMap =  new Map<string, String>();
        Contact con = new Contact();
        
        con.FirstName = (string) NewContactNode.get('FirstName');
        system.debug('firstname***'+con.FirstName);
        system.debug('languagepresa---f***' + options.get('LanguagePref'));
        List<String>langPref = (List<String>) NewContactNode.get('LanguagePref');
        con.Language_Preference__c = langPref != null && langPref.size()>0 ? langPref[0] : 'English';
        system.debug('languagepref***'+con.Language_Preference__c);
        con.LastName = (string) NewContactNode.get('LastName');
        system.debug('lastname***'+con.LastName);
        con.Phone =(string) NewContactNode.get('Phone');
        system.debug('phone***'+con.Phone);
        con.Email =(string) NewContactNode.get('Email');
        system.debug('email***'+con.Email);
        con.Title =(string) NewContactNode.get('Title');
        system.debug('title***'+con.Title);
        if( con.FirstName != null && con.FirstName != '' &&  con.LastName!= null && con.LastName!= '' &&  con.Phone!= null && con.Phone!= '' &&  con.Email!= null && con.Email!= '' &&  con.Language_Preference__c != null && con.Language_Preference__c != '')
        {
             system.debug('trying to insert');
            con.Active__c = true;
            system.debug('options='+options);
            con.AccountId =(string)options.get('AccountId');
            system.debug('AccountId='+(string)options.get('AccountId'));
                Database.saveresult sr= Database.insert(con,false);
            if (sr.isSuccess())
            {
                system.debug('con.id='+con.id);
                Contact contact_1 =[select id,FirstName,Language_Preference__c,LastName,Phone,Email,Title,Active__c,LastModifiedDate FROM Contact WHERE id=: con.id LIMIT 1];
                conMap.put('Active', string.valueOf(contact_1.Active__c));
                conMap.put('Email', contact_1.Email);
                conMap.put('Id', contact_1.id);
                conMap.put('LastModifiedDate', string.valueOf(contact_1.LastModifiedDate.format('MMM dd, YYYY')) );//myDT.format('MMM dd, YYYY')
                conMap.put('Name', con.firstname + ' ' + contact_1.Lastname);
                conMap.put('PreferedLang', contact_1.Language_Preference__c);
                conMap.put('Title', contact_1.Title);//Phone 
                conMap.put('Phone', contact_1.Phone);    
            } else {
                system.debug('failed to insert');
            }
        }
       
        
     
        return conMap;
    }

}