public class CreditDocumentHelper {

    @invocablemethod
    public static void deleteDocuments() {
        Document[] docs = new List<Document>();
        Credit_Related_Info__c[] CCR_Records = new List<Credit_Related_Info__c>();
		ID [] toBeDeleted = new List<ID>();

        try {
            for (Credit_Related_Info__c docInfo :[select id, Document_Internal_ID__c
                              from Credit_Related_Info__c
                             where Delete_Document__c = true]) {
                System.debug(' ... dyy ... document Internal ID = ' + docInfo.Document_Internal_ID__C);                        
                toBeDeleted.add(ID.valueof(docInfo.Document_Internal_ID__C));  
                docInfo.Document_Internal_ID__c=null;
                docInfo.Delete_Document__c=false;
                CCR_Records.add(docInfo);
                                                         
            }

            for (Document doc: [select id,name
                              from Document
                                where id in :toBeDeleted]) {
               doc.ContentType=null;
               docs.add(doc);
            }

            //First update the content to null for all documents to be deleted 
            update docs;
            
           	if(docs!=null &&docs.size()!=0) 
                 delete docs;

            update CCR_Records;

        }  catch (DMLException e) {
             System.debug(' ... dyy ... DML Exception = ' + e);
        }        
        
    }
    
}