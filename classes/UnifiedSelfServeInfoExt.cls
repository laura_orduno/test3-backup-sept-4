/**
 * Contact page section that shows Unified Self Serve Information.  The info
 * is shown as read-only and no user control on the UI.  The VF page will use
 * a 10 seconds poll period to pull latest update from UUID.
 */
public class UnifiedSelfServeInfoExt {
    public contact currentContact{get;set;}
    public string status{get;set;}
    public UnifiedSelfServeInfoExt(apexpages.standardcontroller controller){
        this.currentContact=(contact)controller.getrecord();
        status='Checking association status...';
    }
    public pagereference updateContactBillingAccount(){
        list<contact_linked_billing_account__c> contactAssociations=[select id,last_integration_status__c from contact_linked_billing_account__c where contact__c=:currentContact.id];
        if(contactAssociations.isempty()){
            status='No contact billing account association yet';
        }else{
            integer numIncomplete=0;
            integer numCompleted=0;
            for(contact_linked_billing_account__c contactAssociation:contactAssociations){
                if(string.isnotblank(contactAssociation.last_integration_status__c)&&contactAssociation.last_integration_status__c.equalsignorecase('completed')){
                	numCompleted++;
                }
            }
            numIncomplete=contactAssociations.size()-numCompleted;
            if(numIncomplete==0){
                status='All contact associations are completed';
            }else{
                status='There '+(numIncomplete==1?'is ':'are ')+numIncomplete+' association'+(numIncomplete==1?'':'s')+' still in progress';
            }
        }
        return null;
    }
}