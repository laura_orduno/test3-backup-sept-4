public class ContactLinkedBillingAccountExt {
    public contact_linked_billing_account__c association{get;set;}
    public string returnUrl{get;set;}
    public ContactLinkedBillingAccountExt(){}
    public ContactLinkedBillingAccountExt(apexpages.standardcontroller controller){
        if(!test.isrunningtest()){
            controller.addfields(new list<string>{'name'});
        }
        association=(contact_linked_billing_account__c)controller.getrecord();
        returnUrl=apexpages.currentpage().getparameters().get('retURL');
        if(string.isblank(association.id)){
            association.active__c=true;
        }
    }
    public pagereference init(){
        return null;
    }
    public pagereference doSave(){
        pagereference returnPage=null;
        if(string.isblank(association.id)){
            if(association.active__c){
                if(isValid()){
                    associate();
                    if(association.active__c){
                        returnPage=(new apexpages.standardcontroller(association)).view();
                    }
                }
            }
        }else{
            if(association.active__c){
                if(isValid()){
                    associate();
                    if(association.active__c){
                        returnPage=(new apexpages.standardcontroller(association)).view();
                    }
                }
            }else{
                disassociate();
                if(!association.active__c){
                    returnPage=(new apexpages.standardcontroller(association)).view();
                }
            }
        }
        return returnPage;
    }
    public pagereference doCancel(){
        pagereference returnPage=null;
        if(string.isblank(returnURL)){
            returnPage=(new apexpages.standardcontroller(association)).view();
        }else{
            returnPage=new pagereference('/'+returnURL);
        }
        return returnPage;
    }
    /**
     * Search all contact billing account records to see if dup contact with same email but different in UUID
     * exists, if yes, return error on such linkage before calling to Profile Management
     */
    boolean isValid(){
        boolean isValid=true;
        // Billing account must be the same as contact's account
        contact selectedContact=[select id,accountid from contact where id=:association.contact__c];
        account selectedBillingAccount=[select id,parentid from account where id=:association.billing_account__c];
        if(selectedContact.accountid!=selectedBillingAccount.parentid){
            apexpages.addmessage(new apexpages.message(apexpages.severity.error,label.only_pilot_billing_account));
            isValid=false;
        }
        if(string.isblank(association.id)){
            set<string> emails=new set<string>();
            map<string,integer> emailCountMap=new map<string,integer>();
            map<id,id> existingContactBillingAccountIdMap=new map<id,id>();
            try{
                account selectedAccount=[select id,recordtype.name from account where id=:association.billing_account__c and recordtype.name='CAN'];
            }catch(exception e){
                apexpages.addmessage(new apexpages.message(apexpages.severity.error,label.only_pilot_billing_account));
                isValid=false;
            }
            
            // As long as the billing account is added, it will throw already linked error
            integer numSameExistingAssociation=[select count() from contact_linked_billing_account__c where contact__c!=null and contact__c=:association.contact__c and billing_account__c!=null and billing_account__c=:association.billing_account__c];
            if(numSameExistingAssociation>0){
                apexpages.addmessage(new apexpages.message(apexpages.severity.error,label.Existing_Contact_Billing_Account_Error));
                isValid=false;
            }
            if(association.active__c){
                try{
                    selectedContact=[select id,name,email from contact where id=:association.contact__c and email!=null];
                    list<contact_linked_billing_account__c> numSameEmailContacts=[select id,contact__c,contact__r.name from contact_linked_billing_account__c where contact__c!=:selectedContact.id and contact__r.email=:selectedContact.email limit 1];
                    if(!numSameEmailContacts.isempty()){
                        apexpages.addmessage(new apexpages.message(apexpages.severity.error,label.Use_Contact_With_Registered_UUID.replace('<CONTACT_NAME>','<a href="/'+numSameEmailContacts.get(0).contact__c+'" target="_blank" rel="nofollow">'+numSameEmailContacts.get(0).contact__r.name+'</a>')));
                        isValid=false;
                    }
                }catch(exception e){
                    apexpages.addmessage(new apexpages.message(apexpages.severity.error,e.getmessage()));
                    isValid=false;
                }
            }
        }else{
            integer numSameExistingAssociation=[select count() from contact_linked_billing_account__c where id!=:association.id and contact__c!=null and contact__c=:association.contact__c and billing_account__c!=null and billing_account__c=:association.billing_account__c];
            if(numSameExistingAssociation>0){
                apexpages.addmessage(new apexpages.message(apexpages.severity.error,label.Existing_Contact_Billing_Account_Error));
                isValid=false;
            }
        }
        return isValid;
    }
    void associate(){
        boolean doSync=true;
        if(string.isnotblank(association.id)){
            contact_linked_billing_account__c beforeChange=[select id,active__c from contact_linked_billing_account__c where id=:association.id];
            if(beforeChange.active__c==association.active__c){
                doSync=false;
            }
        }
        if(doSync){
            ServiceAssociationRequest saRequest=getServiceAssociationRequest(association);
            try{
                httpresponse response=associateService(saRequest);
                handleServiceAssociation(response);
            }catch(calloutexception ce){
                apexpages.addmessage(new apexpages.message(apexpages.severity.error,label.system_error_retry_message));
            }
        }
    }
    void disassociate(){
        boolean doSync=true;
        if(string.isnotblank(association.id)){
            contact_linked_billing_account__c beforeChange=[select id,active__c from contact_linked_billing_account__c where id=:association.id];
            if(beforeChange.active__c==association.active__c){
                doSync=false;
            }
        }
        if(doSync){
            ServiceDisassociationRequest saRequest=getServiceDisassociationRequest(association);
            try{
                httpresponse response=disassociateService(saRequest);
                handleServiceDisassociation(response);
            }catch(calloutexception ce){
                apexpages.addmessage(new apexpages.message(apexpages.severity.error,label.system_error_retry_message));
            }
        }
    }
    public void disassociateAssociations(set<id> associationIds){
        list<contact_linked_billing_account__c> disassociations=[select id,active__c,contact__c,contact__r.firstname,contact__r.lastname,billing_account__c,billing_account__r.name from contact_linked_billing_account__c where id=:associationIds and active__c=true];
        for(contact_linked_billing_account__c disassocation:disassociations){
            try{
                ServiceDisassociationRequest saRequest=getServiceDisassociationRequest(disassocation);
                httpresponse response=disassociateService(saRequest);
                handleServiceDisassociation(response);
            }catch(exception e){
                util.emailSystemError(e);
            }
        }
    }
    ServiceAssociationRequest getServiceAssociationRequest(contact_linked_billing_account__c association){
        contact selectedContact=[select id,firstname,lastname,email,self_serve_uuid__c from contact where id=:association.contact__c];
        account selectedAccount=[select id,bcan__c from account where id=:association.billing_account__c];
        ServiceAssociationRequest req=new ServiceAssociationRequest(userinfo.getusername(),'SFDC',selectedContact.email,selectedAccount.bcan__c,selectedContact.firstname,selectedContact.lastname,'1');
        if(string.isnotblank(selectedContact.self_serve_uuid__c)){
            req.uuid=selectedContact.self_serve_uuid__c;
        }
		return req;
    }
    httpresponse associateService(ServiceAssociationRequest req){
        httpresponse response;
        map<string,schema.recordtypeinfo> recordTypeInfoMap=webservice_integration_error_log__c.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        webservice_integration_error_log__c log=new webservice_integration_error_log__c(recordtypeid=recordTypeInfoMap.get(label.webservice_integration_log_success).getrecordtypeid(),object_name__c='Contact_Linked_Billing_Account__c',external_system_name__c='Profile Management');
        string jsonString=json.serialize(req);
        log.request_payload__c='Request:\n'+(string.isnotblank(jsonString)&&jsonString.length()>32767?jsonString.substring(0,32767):jsonString);
        log.sfdc_record_id__c=association.id;
        log.webservice_name__c='associateService';
        log.apex_class_and_method__c='ContactLinkedBillingAccountExt.associateService';
        try{
            webservice_config__c webConfig=webservice_config__c.getinstance('AssociateService');
            http h=new http();
            httprequest saRequest=new httprequest();
            saRequest.setClientCertificateName(webConfig.client_cert_name__c);
            saRequest.setheader('Content-Type','application/json');
            saRequest.settimeout(integer.valueof(webConfig.timeout__c));
            saRequest.setMethod(webConfig.method__c);
            saRequest.setEndpoint(webConfig.endpoint__c);
            saRequest.setbody(jsonString);
            response=h.send(saRequest);
            log.error_code_and_message__c='\n\nResponse:\n'+response.getbody();
        }catch(calloutexception ce){
            log.recordtypeid=recordTypeInfoMap.get(label.webservice_integration_log_failure).getrecordtypeid();
            log.error_code_and_message__c=ce.getmessage();
            log.stack_trace__c=ce.getstacktracestring();
            if(ce.getmessage().containsignorecase('read timed out')){
                throw ce;
            }
        }catch(exception e){
            log.recordtypeid=recordTypeInfoMap.get(label.webservice_integration_log_failure).getrecordtypeid();
            log.error_code_and_message__c=e.getmessage();
            log.stack_trace__c=e.getstacktracestring();
        }finally{
            log.error_code_and_message__c=(string.isnotblank(log.error_code_and_message__c)&&log.error_code_and_message__c.length()>32767?log.error_code_and_message__c.substring(0,32767):log.error_code_and_message__c);
            log.stack_trace__c=(string.isnotblank(log.stack_trace__c)&&log.stack_trace__c.length()>32767?log.stack_trace__c.substring(0,32767):log.stack_trace__c);
            try{
                insert log;
            }catch(exception e){
                Util.emailSystemError(e);
            }
        }
        return response;
    }
    void handleServiceAssociation(httpresponse response){
        contact selectedContact=[select id,firstname,lastname,self_serve_uuid__c from contact where id=:association.contact__c];
        apexpages.message userMessage;
        if(response!=null){                
            ServiceAssociationResponse associateResponse=ServiceAssociationResponse.parse(response);
            if(associateResponse!=null){
                if(associateResponse.status!=null&&string.isnotblank(associateResponse.status.statusCd)){
                    if(associateResponse.status.statusCd.equalsignorecase('200')){
                        selectedContact.self_serve_uuid__c=associateResponse.userProfileId;
                        association.active__c=true;
                        if(string.isblank(association.id)){
                            insert association;
                            update selectedContact;
                        }else{
                            update association;
                            update selectedContact;
                        }
                        userMessage=new apexpages.message(apexpages.severity.info,associateResponse.status.statusTxt);
                    }else{
                        userMessage=new apexpages.message(apexpages.severity.error,associateResponse.status.statusTxt);
                    }                    
                }else{
                    userMessage=new apexpages.message(apexpages.severity.fatal,'Status is empty.  '+label.System_Error.replace('<Contact>',selectedContact.firstname+' '+selectedContact.lastname));
                }
            }else{
                userMessage=new apexpages.message(apexpages.severity.fatal,'Associate response is empty.  '+label.System_Error.replace('<Contact>',selectedContact.firstname+' '+selectedContact.lastname));
            }
            userMessage=new apexpages.message(apexpages.severity.fatal,'It is unsuccessful.  '+label.System_Error.replace('<Contact>',selectedContact.firstname+' '+selectedContact.lastname));
        }else{
            userMessage=new apexpages.message(apexpages.severity.fatal,'It is unsuccessful.  '+label.System_Error.replace('<Contact>',selectedContact.firstname+' '+selectedContact.lastname));
        }
        apexpages.addmessage(userMessage);
    }
    ServiceDisassociationRequest getServiceDisassociationRequest(contact_linked_billing_account__c association){
        contact_linked_billing_account__c contactLinkedBillingAccount=[select id,billing_account__r.bcan__c,contact__c,contact__r.firstname,contact__r.lastname,contact__r.self_serve_uuid__c,last_integration_status__c from contact_linked_billing_account__c where id=:association.id];
        ServiceDisassociationRequest req=new ServiceDisassociationRequest(contactLinkedBillingAccount.contact__r.self_serve_uuid__c,userinfo.getusername(),'SFDC',contactLinkedBillingAccount.billing_account__r.bcan__c);
		return req;
    }
    httpresponse disassociateService(ServiceDisassociationRequest req){
        httpresponse response;
        map<string,schema.recordtypeinfo> recordTypeInfoMap=webservice_integration_error_log__c.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        webservice_integration_error_log__c log=new webservice_integration_error_log__c(recordtypeid=recordTypeInfoMap.get(label.webservice_integration_log_success).getrecordtypeid(),object_name__c='Contact_Linked_Billing_Account__c',external_system_name__c='Profile Management');
        string jsonString=json.serialize(req);
        log.request_payload__c='Request:\n'+(string.isnotblank(jsonString)&&jsonString.length()>32767?jsonString.substring(0,32767):jsonString);
        log.sfdc_record_id__c=association.id;
        log.webservice_name__c='disassociateService';
        log.apex_class_and_method__c='ContactLinkedBillingAccountExt.disassociateService';
        try{
            webservice_config__c webConfig=webservice_config__c.getinstance('DisassociateService');
            http h=new http();
            httprequest saRequest=new httprequest();
            saRequest.setClientCertificateName(webConfig.client_cert_name__c);
            saRequest.setheader('Content-Type','application/json');
            saRequest.settimeout(integer.valueof(webConfig.timeout__c));
            saRequest.setMethod(webConfig.method__c);
            saRequest.setEndpoint(webConfig.endpoint__c);
            saRequest.setbody(jsonString);
            response=h.send(saRequest);
            log.error_code_and_message__c='\n\nResponse:\n'+response.getbody();
        }catch(calloutexception ce){
            log.recordtypeid=recordTypeInfoMap.get(label.webservice_integration_log_failure).getrecordtypeid();
            log.error_code_and_message__c=ce.getmessage();
            log.stack_trace__c=ce.getstacktracestring();
            if(ce.getmessage().containsignorecase('read timed out')){
                throw ce;
            }
        }catch(exception e){
            log.recordtypeid=recordTypeInfoMap.get(label.webservice_integration_log_failure).getrecordtypeid();
            log.error_code_and_message__c=e.getmessage();
            log.stack_trace__c=e.getstacktracestring();
        }finally{
            log.error_code_and_message__c=(string.isnotblank(log.error_code_and_message__c)&&log.error_code_and_message__c.length()>32767?log.error_code_and_message__c.substring(0,32767):log.error_code_and_message__c);
            log.stack_trace__c=(string.isnotblank(log.stack_trace__c)&&log.stack_trace__c.length()>32767?log.stack_trace__c.substring(0,32767):log.stack_trace__c);
            try{
                insert log;
            }catch(exception e){
                Util.emailSystemError(e);
            }
        }
        return response;
    }
    void handleServiceDisassociation(httpresponse response){
        contact selectedContact=[select id,firstname,lastname,self_serve_uuid__c from contact where id=:association.contact__c];
        apexpages.message userMessage;
        association.active__c=true;
        if(response!=null){                
            ServiceAssociationResponse associateResponse=ServiceAssociationResponse.parse(response);
            if(associateResponse!=null){
                if(associateResponse.status!=null&&string.isnotblank(associateResponse.status.statusCd)){
                    if(associateResponse.status.statusCd.equalsignorecase('200')){
                        association.active__c=false;
                        update association;
                        update selectedContact;
                        userMessage=new apexpages.message(apexpages.severity.info,associateResponse.status.statusTxt);
                    }else if(associateResponse.status.statusCd.equalsignorecase('400')){
                        if(associateResponse.status.statusSubCd.equalsignorecase('ae')){
                            association.active__c=true;
                            update association;
                            update selectedContact;
                            userMessage=new apexpages.message(apexpages.severity.info,associateResponse.status.statusTxt);
                        }else{
                            association.active__c=false;
                            userMessage=new apexpages.message(apexpages.severity.error,associateResponse.status.statusTxt);
                        }
                    }else{
                            userMessage=new apexpages.message(apexpages.severity.error,associateResponse.status.statusTxt);
                    }
                }else{
                    userMessage=new apexpages.message(apexpages.severity.fatal,'Status is empty.  '+label.System_Error.replace('<Contact>',selectedContact.firstname+' '+selectedContact.lastname));
                }
            }else{
                userMessage=new apexpages.message(apexpages.severity.fatal,'Associate response is empty.  '+label.System_Error.replace('<Contact>',selectedContact.firstname+' '+selectedContact.lastname));
            }
            userMessage=new apexpages.message(apexpages.severity.fatal,'It is unsuccessful.  '+label.System_Error.replace('<Contact>',selectedContact.firstname+' '+selectedContact.lastname));
        }else{
            userMessage=new apexpages.message(apexpages.severity.fatal,'It is unsuccessful.  '+label.System_Error.replace('<Contact>',selectedContact.firstname+' '+selectedContact.lastname));
        }
        apexpages.addmessage(userMessage);
    }
}