/**
 * S2C_MoveWizardCtlr_Test
 * @description Test class for S2C_MoveWizard
 */
@isTest(seeAllData=false)
private class S2C_MoveWizardCtlr_Test {
	private static final String REQUESTED_DATE                   = 'RREQUESTED_DATE';
	private static final String OPP_AMOUNT                       = 'OPP_AMOUNT';
	private static final String OPP_ACCOUNT_ID                   = 'OPP_ACCOUNT_ID';
	private static final String OPP_TYPE                         = 'OPP_TYPE';
	private static final String OPP_STAGE_NAME                   = 'OPP_STAGE_NAME';
	private static final String OPP_NAME                         = 'OPP_NAME';
	private static final String CONTACT_ID                       = 'CONTACT_ID';
	private static final String WIZARD_SECTION                   = 'WIZARD_SECTION';
	private static final String TO_SECTION                       = 'To';
	private static final String PRODUCT_ID                       = 'PRODUCTID';
	private static final String FROM_SECTION                       = 'FROM';
	
	@isTest static void save2_btn_with_opp(){
		Account newAccount = new Account(Name = 'New Account 003');
		insert newAccount;

		SMBCare_Address__c newSMBCareAddress = new SMBCare_Address__c(Account__c = newAccount.Id);
		insert newSMBCareAddress;

		Product2 newProduct = S2C_TestUtility.createProduct('Test Product', 'Test');
		insert newProduct;

		SRS_PODS_Product__c newSRSPodsProduct = S2C_TestUtility.createSRSPODSProduct(newProduct.Product_Family__c);
		insert newSRSPodsProduct;

		Test.setCurrentPage(initialDataSetupWithOpp());

		S2C_MoveWizardCtlr ctlr = new S2C_MoveWizardCtlr();
		ctlr.searchValue = 'BTN';
		ctlr.btnAcctID = newAccount.Id;
		ctlr.btnBillingAddress = 'TestForce CAN 9450 TRANS CANADA HWY SAINT LAURENT QC CAN, H4S1R7';
		ctlr.btnValue = '7809006002';
		ctlr.btnCANBan = '101-255123698';
		ctlr.btnCanValue = '7809006002';
		ctlr.btnLegalName = 'Test';
		ctlr.btnServiceLocation = '112 9650 TESTING SA STREET EDMONTON AB T6N1G1';
		ctlr.btnSMBAddressId = newSMBCareAddress.Id;
		ctlr.btnTradeName = 'TESTFORCE';
		ctlr.btnProvince = 'AB';
		ctlr.demarcationLocation = 'Test';
		ctlr.endCustomerName = 'Test';
		ctlr.npaNXX = 'Test';
		ctlr.location = 'Test';
		ctlr.cli = 'Test';
		ctlr.existingWorkingTN = 'Test';
		ctlr.siteReady = 'Test';
		ctlr.buildingName = 'Test';
		ctlr.newBuidling = 'Test';
		ctlr.floor = 'Test';
		ctlr.cabinet = 'Test';
		ctlr.rack = 'Test';
		ctlr.shelf = 'Test';
		ctlr.postalCode = 'Test';
		ctlr.requestedDate = '22/07/2015';
		ctlr.productValue = newSRSPodsProduct.Id;
		ctlr.hasServiceLocation = 'true';

		Test.startTest();

		PageReference pageRef = ctlr.save2();

		Test.stopTest();
	}

	@isTest static void save2_btn(){
		Account newAccount = new Account(Name = 'New Account 003');
		insert newAccount;

		SMBCare_Address__c newSMBCareAddress = new SMBCare_Address__c(Account__c = newAccount.Id);
		insert newSMBCareAddress;

		Product2 newProduct = S2C_TestUtility.createProduct('Test Product', 'Test');
		insert newProduct;

		SRS_PODS_Product__c newSRSPodsProduct = S2C_TestUtility.createSRSPODSProduct(newProduct.Product_Family__c);
		insert newSRSPodsProduct;

		Test.setCurrentPage(initialDataSetup());

		S2C_MoveWizardCtlr ctlr = new S2C_MoveWizardCtlr();
		ctlr.searchValue = 'BTN';
		ctlr.btnAcctID = newAccount.Id;
		ctlr.btnBillingAddress = 'TestForce CAN 9450 TRANS CANADA HWY SAINT LAURENT QC CAN, H4S1R7';
		ctlr.btnValue = '7809006002';
		ctlr.btnCANBan = '101-255123698';
		ctlr.btnCanValue = '7809006002';
		ctlr.btnLegalName = 'Test';
		ctlr.btnServiceLocation = '112 9650 TESTING SA STREET EDMONTON AB T6N1G1';
		ctlr.btnSMBAddressId = newSMBCareAddress.Id;
		ctlr.btnTradeName = 'TESTFORCE';
		ctlr.btnProvince = 'AB';
		ctlr.demarcationLocation = 'Test';
		ctlr.endCustomerName = 'Test';
		ctlr.npaNXX = 'Test';
		ctlr.location = 'Test';
		ctlr.cli = 'Test';
		ctlr.existingWorkingTN = 'Test';
		ctlr.siteReady = 'Test';
		ctlr.buildingName = 'Test';
		ctlr.newBuidling = 'Test';
		ctlr.floor = 'Test';
		ctlr.cabinet = 'Test';
		ctlr.rack = 'Test';
		ctlr.shelf = 'Test';
		ctlr.postalCode = 'Test';
		ctlr.requestedDate = '22/07/2015';
		ctlr.productValue = newSRSPodsProduct.Id;
		ctlr.hasServiceLocation = 'true';

		Test.startTest();

		PageReference pageRef = ctlr.save2();

		Test.stopTest();
	}

	@isTest static void save2_csid(){
		Account newAccount = new Account(Name = 'New Account 002');
		insert newAccount;

		SMBCare_Address__c newSMBCareAddress = new SMBCare_Address__c(
			Account__c = newAccount.Id,
			Civic_Number__c = 'Test',
			Street__c = 'Test',
			Type__c = 'Test',
			Unit_Number__c = 'Test',
			Street_Address__c = 'Test',
			City__c = 'Test',
			State__c = 'Test',
			Postal_Code__c = 'Test',
			SRS_Service_Location__c = 'Test',
			NPA_NXX__c = 'Test',
			Province__c = 'BC',
			Address_Type__c = 'Standard',
			Suite_Number__c = '1',
			Street_Number__c = '12',
			Street_Name__c = 'test'
		);
		insert newSMBCareAddress;

		Product2 newProduct = S2C_TestUtility.createProduct('Test Product', 'Test');
		insert newProduct;

		SRS_PODS_Product__c newSRSPodsProduct = S2C_TestUtility.createSRSPODSProduct(newProduct.Product_Family__c);
		insert newSRSPodsProduct;

		Test.setCurrentPage(initialDataSetup());

		S2C_MoveWizardCtlr ctlr = new S2C_MoveWizardCtlr();
		ctlr.tempCSIDServiceType = newSRSPodsProduct.Name;
		ctlr.searchValue = 'CSID';
		ctlr.csidCSIDValue = '1050696';
		ctlr.csidESDCustName = 'ABC Resources Triple AAA Test';
		ctlr.csidServiceType = newSRSPodsProduct.Name;
		ctlr.csidServiceLocation = 'Test';
		ctlr.csidCity = 'Calgary';
		ctlr.csidPRST = 'AB';
		ctlr.demarcationLocation = 'Test';
		ctlr.endCustomerName = 'Test';
		ctlr.npaNXX = 'Test';
		ctlr.location = 'Test';
		ctlr.cli = 'Test';
		ctlr.existingWorkingTN = 'Test';
		ctlr.siteReady = 'Test';
		ctlr.buildingName = 'Test';
		ctlr.newBuidling = 'Test';
		ctlr.floor = 'Test';
		ctlr.cabinet = 'Test';
		ctlr.rack = 'Test';
		ctlr.shelf = 'Test';
		ctlr.postalCode = 'Test';
		ctlr.requestedDate = '22/07/2015';
		ctlr.productValue = newSRSPodsProduct.Id;
		ctlr.hasServiceLocation = 'true';

		ctlr.populateProductLookupWithServiceType();

		Test.startTest();

		PageReference pageRef = ctlr.save2();

		Test.stopTest();
	}

/*
	@isTest static void continue_btn() {
		Account newAccount = new Account(Name = 'New Account 002');
		insert newAccount;

		SMBCare_Address__c newSMBCareAddress = new SMBCare_Address__c(Account__c = newAccount.Id);
		insert newSMBCareAddress;

		Product2 newProduct = S2C_TestUtility.createProduct('Test Product', 'Test');
		insert newProduct;

		SRS_PODS_Product__c newSRSPodsProduct = S2C_TestUtility.createSRSPODSProduct(newProduct.Product_Family__c);
		insert newSRSPodsProduct;

		Test.setCurrentPage(initialDataSetup());

		S2C_MoveWizardCtlr ctlr = new S2C_MoveWizardCtlr();
		ctlr.searchValue = 'BTN';
		ctlr.btnAcctID = newAccount.Id;
		ctlr.btnBillingAddress = 'TestForce CAN 9450 TRANS CANADA HWY SAINT LAURENT QC CAN, H4S1R7';
		ctlr.btnValue = '7809006002';
		ctlr.btnCANBan = '101-255123698';
		ctlr.btnCanValue = '7809006002';
		ctlr.btnLegalName = 'Test';
		ctlr.btnServiceLocation = '112 9650 TESTING SA STREET EDMONTON AB T6N1G1';
		ctlr.btnSMBAddressId = newSMBCareAddress.Id;
		ctlr.btnTradeName = 'TESTFORCE';
		ctlr.btnProvince = 'AB';
		ctlr.demarcationLocation = 'Test';
		ctlr.endCustomerName = 'Test';
		ctlr.npaNXX = 'Test';
		ctlr.location = 'Test';
		ctlr.cli = 'Test';
		ctlr.existingWorkingTN = 'Test';
		ctlr.siteReady = 'Test';
		ctlr.buildingName = 'Test';
		ctlr.newBuidling = 'Test';
		ctlr.floor = 'Test';
		ctlr.cabinet = 'Test';
		ctlr.rack = 'Test';
		ctlr.shelf = 'Test';
		ctlr.postalCode = 'Test';
		ctlr.requestedDate = '22/07/2015';
		ctlr.productValue = newSRSPodsProduct.Id;
		ctlr.hasServiceLocation = 'true';

		Test.startTest();

		PageReference pageRef = ctlr.continueToToPage();

		Test.stopTest();


		String wizardSection = pageRef.getParameters().get(WIZARD_SECTION);
		String oppAmount = pageRef.getParameters().get(OPP_AMOUNT);
		String oppAccountId = pageRef.getParameters().get(OPP_ACCOUNT_ID);
		String contactId = pageRef.getParameters().get(CONTACT_ID);
		String oppType = pageRef.getParameters().get(OPP_TYPE);
		String oppStageName = pageRef.getParameters().get(OPP_STAGE_NAME);
		String oppName = pageRef.getParameters().get(OPP_NAME);
		String productId = pageRef.getParameters().get(PRODUCT_ID);
		String requestedDate = pageRef.getParameters().get(REQUESTED_DATE);
		String fromEncoding = ApexPages.currentPage().getParameters().get(FROM_SECTION);

		System.assertEquals('To', wizardSection);
		System.assertEquals(ctlr.oppAmount, oppAmount);
		System.assertEquals(ctlr.accountId, oppAccountId);
		System.assertEquals(ctlr.contactId, contactId);
		System.assertEquals(ctlr.oppType, oppType);
		System.assertEquals(ctlr.oppStageName, oppStageName);
		System.assertEquals(ctlr.oppName, oppName);
		System.assertEquals(ctlr.productValue, productId);
		System.assertEquals(ctlr.requestedDate, requestedDate);
		System.assert(String.isBlank(fromEncoding));
		
	}

	@isTest static void save_btn() {
		Account newAccount = new Account(Name = 'New Account 002');
		insert newAccount;

		SMBCare_Address__c newSMBCareAddress = new SMBCare_Address__c(
			Account__c = newAccount.Id,
			Civic_Number__c = 'Test',
			Street__c = 'Test',
			Type__c = 'Test',
			Unit_Number__c = 'Test',
			Street_Address__c = 'Test',
			City__c = 'Test',
			State__c = 'Test',
			Postal_Code__c = 'Test',
			SRS_Service_Location__c = 'Test',
			NPA_NXX__c = 'Test',
			Province__c = 'BC',
			Address_Type__c = 'Standard',
			Suite_Number__c = '1',
			Street_Number__c = '12',
			Street_Name__c = 'test'
		);
		insert newSMBCareAddress;

		Product2 newProduct = S2C_TestUtility.createProduct('Test Product', 'Test');
		insert newProduct;

		SRS_PODS_Product__c newSRSPodsProduct = S2C_TestUtility.createSRSPODSProduct(newProduct.Product_Family__c);
		insert newSRSPodsProduct;

		Test.setCurrentPage(initialDataSetup());

		S2C_MoveWizardCtlr ctlr = new S2C_MoveWizardCtlr();
		ctlr.searchValue = 'BTN';
		ctlr.btnAcctID = newAccount.Id;
		ctlr.btnBillingAddress = 'TestForce CAN 9450 TRANS CANADA HWY SAINT LAURENT QC CAN, H4S1R7';
		ctlr.btnValue = '7809006002';
		ctlr.btnCANBan = '101-255123698';
		ctlr.btnCanValue = '7809006002';
		ctlr.btnLegalName = 'Test';
		ctlr.btnServiceLocation = '112 9650 TESTING SA STREET EDMONTON AB T6N1G1';
		ctlr.btnSMBAddressId = newSMBCareAddress.Id;
		ctlr.btnTradeName = 'TESTFORCE';
		ctlr.btnProvince = 'AB';
		ctlr.demarcationLocation = 'Test';
		ctlr.endCustomerName = 'Test';
		ctlr.npaNXX = 'Test';
		ctlr.location = 'Test';
		ctlr.cli = 'Test';
		ctlr.existingWorkingTN = 'Test';
		ctlr.siteReady = 'Test';
		ctlr.buildingName = 'Test';
		ctlr.newBuidling = 'Test';
		ctlr.floor = 'Test';
		ctlr.cabinet = 'Test';
		ctlr.rack = 'Test';
		ctlr.shelf = 'Test';
		ctlr.postalCode = 'Test';
		ctlr.requestedDate = '22/07/2015';
		ctlr.productValue = newSRSPodsProduct.Id;
		ctlr.hasServiceLocation = 'true';


		PageReference pageRef = ctlr.continueToToPage();

		System.debug(pageRef);
		Test.setCurrentPage(pageRef);

		S2C_MoveWizardCtlr ctlr1 = new S2C_MoveWizardCtlr();
		ctlr1.searchValue = 'CSID';
		ctlr1.csidCSIDValue = '1050696';
		ctlr1.csidESDCustName = 'ABC Resources Triple AAA Test';
		ctlr1.csidServiceType = newSRSPodsProduct.Name;
		ctlr1.csidServiceLocation = 'ABCR_123 TEST AVE CALGARY AB';
		ctlr1.csidCity = 'Calgary';
		ctlr1.csidPRST = 'AB';
		ctlr1.demarcationLocation = 'Test';
		ctlr1.endCustomerName = 'Test';
		ctlr1.npaNXX = 'Test';
		ctlr1.location = 'Test';
		ctlr1.cli = 'Test';
		ctlr1.existingWorkingTN = 'Test';
		ctlr1.siteReady = 'Test';
		ctlr1.buildingName = 'Test';
		ctlr1.newBuidling = 'Test';
		ctlr1.floor = 'Test';
		ctlr1.cabinet = 'Test';
		ctlr1.rack = 'Test';
		ctlr1.shelf = 'Test';
		ctlr1.postalCode = 'Test';
		ctlr1.requestedDate = '22/07/2015';
		ctlr1.productValue = newSRSPodsProduct.Id;
		ctlr1.hasServiceLocation = 'true';

		Test.startTest();

		PageReference saveRef = ctlr1.save();

		Test.stopTest();

		List<Service_Request__c> srResults = [SELECT Id FROM Service_Request__c WHERE Opportunity__r.AccountId =: ctlr1.accountId];
		List<Service_Request_Contact__c> srcResults = [SELECT Id FROM Service_Request_Contact__c WHERE Service_Request__r.Opportunity__r.AccountId =: ctlr1.accountId];

		System.assert(!srResults.isEmpty());
		System.assert(srResults.size() == 2);
		System.assert(!srcResults.isEmpty());
		System.assert(srcResults.size() == 2);

	}

	@isTest static void save_without_addresses() {
		Account newAccount = new Account(Name = 'New Account 002');
		insert newAccount;

		SMBCare_Address__c newSMBCareAddress = new SMBCare_Address__c(
			Account__c = newAccount.Id,
			Civic_Number__c = 'Test',
			Street__c = 'Test',
			Type__c = 'Test',
			Unit_Number__c = 'Test',
			Street_Address__c = 'Test',
			City__c = 'Test',
			State__c = 'Test',
			Postal_Code__c = 'Test',
			SRS_Service_Location__c = 'Test',
			NPA_NXX__c = 'Test',
			Province__c = 'BC',
			Address_Type__c = 'Standard',
			Suite_Number__c = '1',
			Street_Number__c = '12',
			Street_Name__c = 'test'
		);
		insert newSMBCareAddress;

		Product2 newProduct = S2C_TestUtility.createProduct('Test Product', 'Test');
		insert newProduct;

		SRS_PODS_Product__c newSRSPodsProduct = S2C_TestUtility.createSRSPODSProduct(newProduct.Product_Family__c);
		insert newSRSPodsProduct;

		Test.setCurrentPage(initialDataSetup());

		S2C_MoveWizardCtlr ctlr = new S2C_MoveWizardCtlr();
		ctlr.productValue = '000000000000000';
		ctlr.requestedDate = '22/07/2015';


		PageReference pageRef = ctlr.continueToToPage();

		System.debug(pageRef);
		Test.setCurrentPage(pageRef);

		S2C_MoveWizardCtlr ctlr1 = new S2C_MoveWizardCtlr();
		ctlr1.productValue = '000000000000000';
		ctlr1.requestedDate = '22/07/2015';
		ctlr1.productValue = newSRSPodsProduct.Id;

		Test.startTest();

		PageReference saveRef = ctlr1.save();

		Test.stopTest();

		List<Service_Request__c> srResults = [SELECT Id FROM Service_Request__c WHERE Opportunity__r.AccountId =: ctlr1.accountId];
		List<Service_Request_Contact__c> srcResults = [SELECT Id FROM Service_Request_Contact__c WHERE Service_Request__r.Opportunity__r.AccountId =: ctlr1.accountId];

		System.assert(!srResults.isEmpty());
		System.assert(srResults.size() == 2);
		System.assert(!srcResults.isEmpty());
		System.assert(srcResults.size() == 2);
	}

	@isTest static void save_btn_btn_address() {
		Account newAccount = new Account(Name = 'New Account 002');
		insert newAccount;

		SMBCare_Address__c newSMBCareAddress = new SMBCare_Address__c(
			Account__c = newAccount.Id,
			Civic_Number__c = 'Test',
			Street__c = 'Test',
			Type__c = 'Test',
			Unit_Number__c = 'Test',
			Street_Address__c = 'Test',
			City__c = 'Test',
			State__c = 'Test',
			Postal_Code__c = 'Test',
			SRS_Service_Location__c = 'Test',
			NPA_NXX__c = 'Test',
			Province__c = 'BC',
			Address_Type__c = 'Standard',
			Suite_Number__c = '1',
			Street_Number__c = '12',
			Street_Name__c = 'test'
		);
		insert newSMBCareAddress;

		Product2 newProduct = S2C_TestUtility.createProduct('Test Product', 'Test');
		insert newProduct;

		SRS_PODS_Product__c newSRSPodsProduct = S2C_TestUtility.createSRSPODSProduct(newProduct.Product_Family__c);
		insert newSRSPodsProduct;

		Test.setCurrentPage(initialDataSetup());

		S2C_MoveWizardCtlr ctlr = new S2C_MoveWizardCtlr();
		ctlr.searchValue = 'BTN';
		ctlr.btnAcctID = newAccount.Id;
		ctlr.btnBillingAddress = 'TestForce CAN 9450 TRANS CANADA HWY SAINT LAURENT QC CAN, H4S1R7';
		ctlr.btnValue = '7809006002';
		ctlr.btnCANBan = '101-255123698';
		ctlr.btnCanValue = '7809006002';
		ctlr.btnLegalName = 'Test';
		ctlr.btnServiceLocation = '112 9650 TESTING SA STREET EDMONTON AB T6N1G1';
		ctlr.btnSMBAddressId = newSMBCareAddress.Id;
		ctlr.btnTradeName = 'TESTFORCE';
		ctlr.btnProvince = 'AB';
		ctlr.demarcationLocation = 'Test';
		ctlr.endCustomerName = 'Test';
		ctlr.npaNXX = 'Test';
		ctlr.location = 'Test';
		ctlr.cli = 'Test';
		ctlr.existingWorkingTN = 'Test';
		ctlr.siteReady = 'Test';
		ctlr.buildingName = 'Test';
		ctlr.newBuidling = 'Test';
		ctlr.floor = 'Test';
		ctlr.cabinet = 'Test';
		ctlr.rack = 'Test';
		ctlr.shelf = 'Test';
		ctlr.postalCode = 'Test';
		ctlr.requestedDate = '22/07/2015';
		ctlr.productValue = newSRSPodsProduct.Id;
		ctlr.hasServiceLocation = 'true';

		ctlr.populateProductLookupWithServiceType();


		PageReference pageRef = ctlr.continueToToPage();

		System.debug(pageRef);
		Test.setCurrentPage(pageRef);

		S2C_MoveWizardCtlr ctlr1 = new S2C_MoveWizardCtlr();

		ctlr1.searchValue = 'BTN';
		ctlr1.btnAcctID = newAccount.Id;
		ctlr1.btnBillingAddress = 'TestForce CAN 9450 TRANS CANADA HWY SAINT LAURENT QC CAN, H4S1R7';
		ctlr1.btnValue = '7809006002';
		ctlr1.btnCANBan = '101-255123698';
		ctlr1.btnCanValue = '7809006002';
		ctlr1.btnLegalName = 'Test';
		ctlr1.btnServiceLocation = '112 9650 TESTING SA STREET EDMONTON AB T6N1G1';
		ctlr1.btnSMBAddressId = newSMBCareAddress.Id;
		ctlr1.btnTradeName = 'TESTFORCE';
		ctlr1.btnProvince = 'AB';
		ctlr1.demarcationLocation = 'Test';
		ctlr1.endCustomerName = 'Test';
		ctlr1.npaNXX = 'Test';
		ctlr1.location = 'Test';
		ctlr1.cli = 'Test';
		ctlr1.existingWorkingTN = 'Test';
		ctlr1.siteReady = 'Test';
		ctlr1.buildingName = 'Test';
		ctlr1.newBuidling = 'Test';
		ctlr1.floor = 'Test';
		ctlr1.cabinet = 'Test';
		ctlr1.rack = 'Test';
		ctlr1.shelf = 'Test';
		ctlr1.postalCode = 'Test';
		ctlr1.requestedDate = '22/07/2015';
		ctlr1.productValue = newSRSPodsProduct.Id;
		ctlr1.hasServiceLocation = 'true';

		Test.startTest();

		PageReference saveRef = ctlr1.save();

		Test.stopTest();

		List<Service_Request__c> srResults = [SELECT Id FROM Service_Request__c WHERE Opportunity__r.AccountId =: ctlr1.accountId];
		List<Service_Request_Contact__c> srcResults = [SELECT Id FROM Service_Request_Contact__c WHERE Service_Request__r.Opportunity__r.AccountId =: ctlr1.accountId];

		System.assert(!srResults.isEmpty());
		System.assert(srResults.size() == 2);
		System.assert(!srcResults.isEmpty());
		System.assert(srcResults.size() == 2);
	}

	@isTest static void save_none_btn_address() {
		Account newAccount = new Account(Name = 'New Account 002');
		insert newAccount;

		SMBCare_Address__c newSMBCareAddress = new SMBCare_Address__c(
			Account__c = newAccount.Id,
			Civic_Number__c = 'Test',
			Street__c = 'Test',
			Type__c = 'Test',
			Unit_Number__c = 'Test',
			Street_Address__c = 'Test',
			City__c = 'Test',
			State__c = 'Test',
			Postal_Code__c = 'Test',
			SRS_Service_Location__c = 'Test',
			NPA_NXX__c = 'Test',
			Province__c = 'BC',
			Address_Type__c = 'Standard',
			Suite_Number__c = '1',
			Street_Number__c = '12',
			Street_Name__c = 'test'
		);
		insert newSMBCareAddress;

		Product2 newProduct = S2C_TestUtility.createProduct('Test Product', 'Test');
		insert newProduct;

		SRS_PODS_Product__c newSRSPodsProduct = S2C_TestUtility.createSRSPODSProduct(newProduct.Product_Family__c);
		insert newSRSPodsProduct;

		Test.setCurrentPage(initialDataSetup());

		S2C_MoveWizardCtlr ctlr = new S2C_MoveWizardCtlr();
		ctlr.requestedDate = '22/07/2015';
		ctlr.productValue = newSRSPodsProduct.Id;

		PageReference pageRef = ctlr.continueToToPage();

		System.debug(pageRef);
		Test.setCurrentPage(pageRef);

		S2C_MoveWizardCtlr ctlr1 = new S2C_MoveWizardCtlr();

		System.debug('FROM Information :::: ' + ctlr1.fromInformation);

		ctlr1.searchValue = 'BTN';
		ctlr1.btnAcctID = newAccount.Id;
		ctlr1.btnBillingAddress = 'TestForce CAN 9450 TRANS CANADA HWY SAINT LAURENT QC CAN, H4S1R7';
		ctlr1.btnValue = '7809006002';
		ctlr1.btnCANBan = '101-255123698';
		ctlr1.btnCanValue = '7809006002';
		ctlr1.btnLegalName = 'Test';
		ctlr1.btnServiceLocation = '112 9650 TESTING SA STREET EDMONTON AB T6N1G1';
		ctlr1.btnSMBAddressId = newSMBCareAddress.Id;
		ctlr1.btnTradeName = 'TESTFORCE';
		ctlr1.btnProvince = 'AB';
		ctlr1.demarcationLocation = 'Test';
		ctlr1.endCustomerName = 'Test';
		ctlr1.npaNXX = 'Test';
		ctlr1.location = 'Test';
		ctlr1.cli = 'Test';
		ctlr1.existingWorkingTN = 'Test';
		ctlr1.siteReady = 'Test';
		ctlr1.buildingName = 'Test';
		ctlr1.newBuidling = 'Test';
		ctlr1.floor = 'Test';
		ctlr1.cabinet = 'Test';
		ctlr1.rack = 'Test';
		ctlr1.shelf = 'Test';
		ctlr1.postalCode = 'Test';
		ctlr1.requestedDate = '22/07/2015';
		ctlr1.productValue = newSRSPodsProduct.Id;
		ctlr1.hasServiceLocation = 'true';

		Test.startTest();

		PageReference saveRef = ctlr1.save();

		Test.stopTest();

		List<Service_Request__c> srResults = [SELECT Id FROM Service_Request__c WHERE Opportunity__r.AccountId =: ctlr1.accountId];
		List<Service_Request_Contact__c> srcResults = [SELECT Id FROM Service_Request_Contact__c WHERE Service_Request__r.Opportunity__r.AccountId =: ctlr1.accountId];

		System.assert(!srResults.isEmpty());
		System.assert(srResults.size() == 2);
		System.assert(!srcResults.isEmpty());
		System.assert(srcResults.size() == 2);
	}

	@isTest static void save_csid_btn_address() {
		Account newAccount = new Account(Name = 'New Account 002');
		insert newAccount;

		SMBCare_Address__c newSMBCareAddress = new SMBCare_Address__c(
			Account__c = newAccount.Id,
			Civic_Number__c = 'Test',
			Street__c = 'Test',
			Type__c = 'Test',
			Unit_Number__c = 'Test',
			Street_Address__c = 'Test',
			City__c = 'Test',
			State__c = 'Test',
			Postal_Code__c = 'Test',
			SRS_Service_Location__c = 'Test',
			NPA_NXX__c = 'Test',
			Province__c = 'BC',
			Address_Type__c = 'Standard',
			Suite_Number__c = '1',
			Street_Number__c = '12',
			Street_Name__c = 'test'
		);
		insert newSMBCareAddress;

		Product2 newProduct = S2C_TestUtility.createProduct('Test Product', 'Test');
		insert newProduct;

		SRS_PODS_Product__c newSRSPodsProduct = S2C_TestUtility.createSRSPODSProduct(newProduct.Product_Family__c);
		insert newSRSPodsProduct;

		Test.setCurrentPage(initialDataSetup());

		S2C_MoveWizardCtlr ctlr = new S2C_MoveWizardCtlr();
		ctlr.tempCSIDServiceType = newSRSPodsProduct.Name;
		ctlr.searchValue = 'CSID';
		ctlr.csidCSIDValue = '1050696';
		ctlr.csidESDCustName = 'ABC Resources Triple AAA Test';
		ctlr.csidServiceType = newSRSPodsProduct.Name;
		ctlr.csidServiceLocation = 'Test';
		ctlr.csidCity = 'Calgary';
		ctlr.csidPRST = 'AB';
		ctlr.demarcationLocation = 'Test';
		ctlr.endCustomerName = 'Test';
		ctlr.npaNXX = 'Test';
		ctlr.location = 'Test';
		ctlr.cli = 'Test';
		ctlr.existingWorkingTN = 'Test';
		ctlr.siteReady = 'Test';
		ctlr.buildingName = 'Test';
		ctlr.newBuidling = 'Test';
		ctlr.floor = 'Test';
		ctlr.cabinet = 'Test';
		ctlr.rack = 'Test';
		ctlr.shelf = 'Test';
		ctlr.postalCode = 'Test';
		ctlr.requestedDate = '22/07/2015';
		ctlr.productValue = newSRSPodsProduct.Id;
		ctlr.hasServiceLocation = 'true';

		ctlr.populateProductLookupWithServiceType();


		PageReference pageRef = ctlr.continueToToPage();

		System.debug(pageRef);
		Test.setCurrentPage(pageRef);

		S2C_MoveWizardCtlr ctlr1 = new S2C_MoveWizardCtlr();
		ctlr1.searchValue = 'BTN';
		ctlr1.btnAcctID = newAccount.Id;
		ctlr1.btnBillingAddress = 'TestForce CAN 9450 TRANS CANADA HWY SAINT LAURENT QC CAN, H4S1R7';
		ctlr1.btnValue = '7809006002';
		ctlr1.btnCANBan = '101-255123698';
		ctlr1.btnCanValue = '7809006002';
		ctlr1.btnLegalName = 'Test';
		ctlr1.btnServiceLocation = '112 9650 TESTING SA STREET EDMONTON AB T6N1G1';
		ctlr1.btnSMBAddressId = newSMBCareAddress.Id;
		ctlr1.btnTradeName = 'TESTFORCE';
		ctlr1.btnProvince = 'AB';
		ctlr1.demarcationLocation = 'Test';
		ctlr1.endCustomerName = 'Test';
		ctlr1.npaNXX = 'Test';
		ctlr1.location = 'Test';
		ctlr1.cli = 'Test';
		ctlr1.existingWorkingTN = 'Test';
		ctlr1.siteReady = 'Test';
		ctlr1.buildingName = 'Test';
		ctlr1.newBuidling = 'Test';
		ctlr1.floor = 'Test';
		ctlr1.cabinet = 'Test';
		ctlr1.rack = 'Test';
		ctlr1.shelf = 'Test';
		ctlr1.postalCode = 'Test';
		ctlr1.requestedDate = '22/07/2015';
		ctlr1.productValue = newSRSPodsProduct.Id;
		ctlr1.hasServiceLocation = 'true';

		Test.startTest();

		PageReference saveRef = ctlr1.save();

		Test.stopTest();

		List<Service_Request__c> srResults = [SELECT Id FROM Service_Request__c WHERE Opportunity__r.AccountId =: ctlr1.accountId];
		List<Service_Request_Contact__c> srcResults = [SELECT Id FROM Service_Request_Contact__c WHERE Service_Request__r.Opportunity__r.AccountId =: ctlr1.accountId];

		System.assert(!srResults.isEmpty());
		System.assert(srResults.size() == 2);
		System.assert(!srcResults.isEmpty());
		System.assert(srcResults.size() == 2);
	}

	@isTest static void save_csid_only_address() {
		Account newAccount = new Account(Name = 'New Account 002');
		insert newAccount;

		SMBCare_Address__c newSMBCareAddress = new SMBCare_Address__c(
			Account__c = newAccount.Id,
			Civic_Number__c = 'Test',
			Street__c = 'Test',
			Type__c = 'Test',
			Unit_Number__c = 'Test',
			Street_Address__c = 'Test',
			City__c = 'Test',
			State__c = 'Test',
			Postal_Code__c = 'Test',
			SRS_Service_Location__c = 'Test',
			NPA_NXX__c = 'Test',
			Province__c = 'BC',
			Address_Type__c = 'Standard',
			Suite_Number__c = '1',
			Street_Number__c = '12',
			Street_Name__c = 'test'
		);
		insert newSMBCareAddress;

		Product2 newProduct = S2C_TestUtility.createProduct('Test Product', 'Test');
		insert newProduct;

		SRS_PODS_Product__c newSRSPodsProduct = S2C_TestUtility.createSRSPODSProduct(newProduct.Product_Family__c);
		insert newSRSPodsProduct;

		Test.setCurrentPage(initialDataSetup());

		S2C_MoveWizardCtlr ctlr = new S2C_MoveWizardCtlr();
		ctlr.tempCSIDServiceType = newSRSPodsProduct.Name;
		ctlr.searchValue = 'CSID';
		ctlr.csidCSIDValue = '1050696';
		ctlr.csidESDCustName = 'ABC Resources Triple AAA Test';
		ctlr.csidServiceType = newSRSPodsProduct.Name;
		ctlr.csidServiceLocation = 'Test';
		ctlr.csidCity = 'Calgary';
		ctlr.csidPRST = 'AB';
		ctlr.demarcationLocation = 'Test';
		ctlr.endCustomerName = 'Test';
		ctlr.npaNXX = 'Test';
		ctlr.location = 'Test';
		ctlr.cli = 'Test';
		ctlr.existingWorkingTN = 'Test';
		ctlr.siteReady = 'Test';
		ctlr.buildingName = 'Test';
		ctlr.newBuidling = 'Test';
		ctlr.floor = 'Test';
		ctlr.cabinet = 'Test';
		ctlr.rack = 'Test';
		ctlr.shelf = 'Test';
		ctlr.postalCode = 'Test';
		ctlr.requestedDate = '22/07/2015';
		ctlr.productValue = newSRSPodsProduct.Id;
		ctlr.hasServiceLocation = 'true';

		ctlr.populateProductLookupWithServiceType();


		PageReference pageRef = ctlr.continueToToPage();

		System.debug(pageRef);
		Test.setCurrentPage(pageRef);

		S2C_MoveWizardCtlr ctlr1 = new S2C_MoveWizardCtlr();
		ctlr1.requestedDate = '22/07/2015';
		ctlr1.productValue = newSRSPodsProduct.Id;

		Test.startTest();

		PageReference saveRef = ctlr1.save();

		Test.stopTest();

		List<Service_Request__c> srResults = [SELECT Id FROM Service_Request__c WHERE Opportunity__r.AccountId =: ctlr1.accountId];
		List<Service_Request_Contact__c> srcResults = [SELECT Id FROM Service_Request_Contact__c WHERE Service_Request__r.Opportunity__r.AccountId =: ctlr1.accountId];

		System.assert(!srResults.isEmpty());
		System.assert(srResults.size() == 2);
		System.assert(!srcResults.isEmpty());
		System.assert(srcResults.size() == 2);
	}

	@isTest static void save_btn_only_address() {
		Account newAccount = new Account(Name = 'New Account 002');
		insert newAccount;

		SMBCare_Address__c newSMBCareAddress = new SMBCare_Address__c(
			Account__c = newAccount.Id,
			Civic_Number__c = 'Test',
			Street__c = 'Test',
			Type__c = 'Test',
			Unit_Number__c = 'Test',
			Street_Address__c = 'Test',
			City__c = 'Test',
			State__c = 'Test',
			Postal_Code__c = 'Test',
			SRS_Service_Location__c = 'Test',
			NPA_NXX__c = 'Test',
			Province__c = 'BC',
			Address_Type__c = 'Standard',
			Suite_Number__c = '1',
			Street_Number__c = '12',
			Street_Name__c = 'test'
		);
		insert newSMBCareAddress;

		Product2 newProduct = S2C_TestUtility.createProduct('Test Product', 'Test');
		insert newProduct;

		SRS_PODS_Product__c newSRSPodsProduct = S2C_TestUtility.createSRSPODSProduct(newProduct.Product_Family__c);
		insert newSRSPodsProduct;

		Test.setCurrentPage(initialDataSetup());

		S2C_MoveWizardCtlr ctlr = new S2C_MoveWizardCtlr();
		ctlr.searchValue = 'BTN';
		ctlr.btnAcctID = newAccount.Id;
		ctlr.btnBillingAddress = 'TestForce CAN 9450 TRANS CANADA HWY SAINT LAURENT QC CAN, H4S1R7';
		ctlr.btnValue = '7809006002';
		ctlr.btnCANBan = '101-255123698';
		ctlr.btnCanValue = '7809006002';
		ctlr.btnLegalName = 'Test';
		ctlr.btnServiceLocation = '112 9650 TESTING SA STREET EDMONTON AB T6N1G1';
		ctlr.btnSMBAddressId = newSMBCareAddress.Id;
		ctlr.btnTradeName = 'TESTFORCE';
		ctlr.btnProvince = 'AB';
		ctlr.demarcationLocation = 'Test';
		ctlr.endCustomerName = 'Test';
		ctlr.npaNXX = 'Test';
		ctlr.location = 'Test';
		ctlr.cli = 'Test';
		ctlr.existingWorkingTN = 'Test';
		ctlr.siteReady = 'Test';
		ctlr.buildingName = 'Test';
		ctlr.newBuidling = 'Test';
		ctlr.floor = 'Test';
		ctlr.cabinet = 'Test';
		ctlr.rack = 'Test';
		ctlr.shelf = 'Test';
		ctlr.postalCode = 'Test';
		ctlr.requestedDate = '22/07/2015';
		ctlr.productValue = newSRSPodsProduct.Id;
		ctlr.hasServiceLocation = 'true';


		PageReference pageRef = ctlr.continueToToPage();

		System.debug(pageRef);
		Test.setCurrentPage(pageRef);

		S2C_MoveWizardCtlr ctlr1 = new S2C_MoveWizardCtlr();
		ctlr1.requestedDate = '22/07/2015';
		ctlr1.productValue = newSRSPodsProduct.Id;

		Test.startTest();

		PageReference saveRef = ctlr1.save();

		Test.stopTest();

		List<Service_Request__c> srResults = [SELECT Id FROM Service_Request__c WHERE Opportunity__r.AccountId =: ctlr1.accountId];
		List<Service_Request_Contact__c> srcResults = [SELECT Id FROM Service_Request_Contact__c WHERE Service_Request__r.Opportunity__r.AccountId =: ctlr1.accountId];

		System.assert(!srResults.isEmpty());
		System.assert(srResults.size() == 2);
		System.assert(!srcResults.isEmpty());
		System.assert(srcResults.size() == 2);
	}
*/
	@isTest static void execute_page_methods_setup() {
		Account newAccount = new Account(Name = 'New Account 002');
		insert newAccount;

		SMBCare_Address__c newSMBCareAddress = new SMBCare_Address__c(Account__c = newAccount.Id);
		insert newSMBCareAddress;

		Product2 newProduct = S2C_TestUtility.createProduct('Test Product', 'Test');
		insert newProduct;

		SRS_PODS_Product__c newSRSPodsProduct = S2C_TestUtility.createSRSPODSProduct(newProduct.Product_Family__c);
		insert newSRSPodsProduct;

		Test.setCurrentPage(initialDataSetup());

		S2C_MoveWizardCtlr ctlr = new S2C_MoveWizardCtlr();

		Test.startTest();

		List<SelectOption> searchOptions = ctlr.getSearchValues();
		ctlr.updateVariables();
		ctlr.isRecordSelected = true;
		ctlr.checkBoxStatus = 'true';
		ctlr.tempProductId = newSRSPodsProduct.Id;
		ctlr.updateProduct();
		ctlr.clearProductSelection();

		Test.stopTest();
	}



	private static PageReference initialDataSetup(){
		List<Document> docToInsert = new List<Document>();
		Account newAccount = new Account(Name = 'New Account 001');
		insert newAccount;

		Contact newContact = new Contact(FirstName = 'First Contact 001', LastName = 'Last Contact 001');
		insert newContact;

		Document newDocument1 = new Document(Name = 'Datepicker icon', FolderId = UserInfo.getUserId(), DeveloperName = 'Datepicker_icon');
		docToInsert.add(newDocument1);

        Document newDocument2 = new Document(Name = 'Clear', FolderId = UserInfo.getUserId(), DeveloperName = 'Clear');		
		docToInsert.add(newDocument2);

		insert docToInsert;

		PageReference refPage = Page.S2C_MoveWizard;

		refPage.getParameters().put('OPP_AMOUNT', '1000.00');
		refPage.getParameters().put('OPP_ACCOUNT_ID', newAccount.Id);
		refPage.getParameters().put('OPP_TYPE', 'Complex Order');
		refPage.getParameters().put('OPP_STAGE_NAME', 'Originated');
		refPage.getParameters().put('OPP_NAME', newAccount.Name + ' - COMPLEX ORDER - ' + System.now());
		refPage.getParameters().put('CONTACT_ID', newContact.Id);

		return refPage;

	}

	private static PageReference initialDataSetupWithOpp(){
		List<Document> docToInsert = new List<Document>();
		String orderOppRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('SRS Order Request').getRecordTypeId();
		Account newAccount = new Account(Name = 'New Account 001');
		insert newAccount;

		Contact newContact = new Contact(FirstName = 'First Contact 001', LastName = 'Last Contact 001');
		insert newContact;

		Opportunity newOpp = new Opportunity(
			Name = newAccount.Name + ' - COMPLEX ORDER - ' + System.now(),
			AccountId = newAccount.Id,
			Amount = Decimal.valueOf('1000.00'),
			CloseDate = System.Today(),
			Primary_Order_Contact__c = newContact.Id,
			Type = 'Complex Order',
			StageName = 'Originated',
			RecordTypeId = orderOppRecordTypeId
		);

		insert newOpp;

		Order_Request__c ordReq = new Order_Request__c(
			Opportunity__c = newOpp.Id
		);
		insert ordReq;

		newContact.Opportunity__c = newOpp.Id;
		newContact.SRS_Contact_Type__c = 'Primary Contact';

		update newContact;

		Document newDocument1 = new Document(Name = 'Datepicker icon', FolderId = UserInfo.getUserId(), DeveloperName = 'Datepicker_icon');
		docToInsert.add(newDocument1);

        Document newDocument2 = new Document(Name = 'Clear', FolderId = UserInfo.getUserId(), DeveloperName = 'Clear');		
		docToInsert.add(newDocument2);

		insert docToInsert;

		PageReference refPage = Page.S2C_MoveWizard;

		refPage.getParameters().put('OPP_ID', newOpp.Id);

		return refPage;

	}
	
}