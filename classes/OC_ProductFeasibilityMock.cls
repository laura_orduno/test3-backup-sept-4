/* This class can be used for testing Feasibility logic in the Vlocity cart when external systems (NetCracker/GIS) are unreliable.
 * It is similar to OC_ProductFeasibilityImpl, but omits the webservice callout (OrdrProductFeasibilityWsCallout.performCustomerOrderFeasibility)
 *
 * Service address must include string 'Feasibility' and house number indicates the test scenario (e.g. 1 Feasibility Ave, 2 Feasibility Ave, etc.)
 *
 * Author: Andrew Fee / NSPI / Release Train A
 * Date:   March 5, 2018
*/
global with sharing class OC_ProductFeasibilityMock implements vlocity_cmt.VlocityOpenInterface {

    static final String ADDRESS_SEARCH_TERM = 'Feasibility';
    
    public class FeasibilityInfo {
        public String offeringKey; //orderMgmtId
        public String lineItemExternalId; //SFDC OLI Id
        public String locationKey; //locationId
        public String underlyingConnectivityId;
        public String offeringId;
        public String serviceDesignation; //Regular Consumer, Public Wi-fi
        public PricebookEntry pbe;
    }

    public Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {

        if(methodName == 'checkProductFeasibility') {
           if(inputMap.Containskey('orderId')){
                checkProductFeasibility((Id)inputMap.get('orderId'), outMap);
           }
        } else {
            return false;
        }
        return true;
    }
    
    private void checkProductFeasibility(Id orderId, Map<String,Object> outMap) {
        String feasibility = 'true';
        String reason = '';
        
        Order order = [select OrderNumber, Service_Address__r.Id, Service_Address_Text__c, account.CustProfId__c from order where Id = :orderId LIMIT 1];
        String orderNumber = order.OrderNumber; 
        Id serviceAddressId = order.Service_Address__r.Id;
        
        //Service Address Text on the Order is null Order 2.0, we need to get it from the SMBCare_Address__c
        String serviceAddress;

        if (order.Service_Address_Text__c != null){
            System.debug(LoggingLevel.ERROR, '********** Order 1.0 **********');
            serviceAddress = order.Service_Address_Text__c;
        } else {
            System.debug(LoggingLevel.ERROR, '********** Order 2.0 **********');
            SMBCare_Address__c address = [SELECT AddressText__c FROM SMBCare_Address__c WHERE Id = :serviceAddressId LIMIT 1];
            serviceAddress = address.AddressText__c;
        }

        //
        //NOTE: no Feasibility callout is performed in this implementation.  We mock up different responses here based on the address
        //

        System.debug(LoggingLevel.ERROR, 'orderId: ' + orderId);
        System.debug(LoggingLevel.ERROR, 'orderNumber: ' + orderNumber);
        System.debug(LoggingLevel.ERROR, 'serviceAddressId: ' + serviceAddressId);
        System.debug(LoggingLevel.ERROR, 'serviceAddress: ' + serviceAddress);

        Boolean starts = serviceAddress.startsWith('1');
        System.debug(LoggingLevel.ERROR, 'serviceAddress starts with 1: ' + starts);

        Boolean feas = serviceAddress.containsIgnoreCase(ADDRESS_SEARCH_TERM);
        System.debug(LoggingLevel.ERROR, 'serviceAddress contains feasbility: ' + feas);
        

        //Add Connectivity to the feasibilityInfoList with the appropriate Service Designation based on the offers in the cart.
        Boolean fibreOffer = false;
        Boolean publicWifiOffer = false;

        List<OrderItem> cartRecords = [SELECT Product2.Name, Product2.VLAdditionalConfigData__c FROM OrderItem WHERE OrderId = :orderId];
        
        for (OrderItem record : cartRecords) {
            System.debug(LoggingLevel.ERROR, 'Order Item: ' + record.Product2.Name + ' | ' + record.Product2.VLAdditionalConfigData__c);
            
            if (record.Product2.VLAdditionalConfigData__c.indexOf('FIFA-INET') != -1) {
                //FIFA Internet offer exists in cart
                fibreOffer = true;
                System.debug(LoggingLevel.ERROR, '*** found FIFA-INET');
            } 
            else if (record.Product2.VLAdditionalConfigData__c.indexOf('FIFA-P-WIFI') != -1) {
                //TELUS Public Wi-Fi offer exists in cart
                publicWifiOffer = true;
                System.debug(LoggingLevel.ERROR, '*** found FIFA-P-WIFI');
            }
        }

        List<PriceBookEntry> connPbeList = [SELECT Id, Name, Product2.orderMgmtId__c from PricebookEntry WHERE Product2.VLAdditionalConfigData__C = '{"Offering ID":"FIFA-Connectivity"}'];

        if (connPbeList.size() == 0) {
            outMap.put('feasibility', false);
            outMap.put('reason', 'FIFA-Connectivity does not exist in catalog.');
            outMap.put('feasibilityInfoList', new List<FeasibilityInfo>());
            return;
        } 
        PriceBookEntry connPbe = connPbeList[0];

        List<FeasibilityInfo> feasibilityInfoList = new List<FeasibilityInfo>();

        FeasibilityInfo feasibilityInfoRegular = new FeasibilityInfo();
        feasibilityInfoRegular.offeringKey = connPbe.Product2.orderMgmtId__c;
        feasibilityInfoRegular.lineItemExternalId = null;
        feasibilityInfoRegular.locationKey = null;
        feasibilityInfoRegular.underlyingConnectivityId = null;
        feasibilityInfoRegular.offeringId = 'FIFA-Connectivity';
        feasibilityInfoRegular.serviceDesignation = 'Regular Consumer';
        feasibilityInfoRegular.pbe = connPbe;

        FeasibilityInfo feasibilityInfoWifi = new FeasibilityInfo();
        feasibilityInfoWifi.offeringKey = connPbe.Product2.orderMgmtId__c;
        feasibilityInfoWifi.lineItemExternalId = null;
        feasibilityInfoWifi.locationKey = null;
        feasibilityInfoWifi.underlyingConnectivityId = null;
        feasibilityInfoWifi.offeringId = 'FIFA-Connectivity';
        feasibilityInfoWifi.serviceDesignation = 'Public Wi-fi';
        feasibilityInfoWifi.pbe = connPbe;


        //The street number on address will indicate the different test scenarios
        if (serviceAddress.containsIgnoreCase(ADDRESS_SEARCH_TERM)) {
            
            //Connectivity required for both
            if (serviceAddress.startsWith('1')) {
                if (fibreOffer) {
                    feasibilityInfoList.add(feasibilityInfoRegular);
                }
                if (publicWifiOffer) {
                    feasibilityInfoList.add(feasibilityInfoWifi);
                }
            }

            //Connectivity required for FIFA Fibre Internet only
            else if (serviceAddress.startsWith('2')) {
                if (fibreOffer) {
                    feasibilityInfoList.add(feasibilityInfoRegular);
                }
            }

            //Connectivity required for Public Wi-fi only
            else if (serviceAddress.startsWith('3')) {
                if (publicWifiOffer) {
                    feasibilityInfoList.add(feasibilityInfoWifi);
                }
            }

            //Connectivity is available, but no install required for either
            else if (serviceAddress.startsWith('4')) {
                feasibility = 'true';  //it should return true, but nothing added to cart
                reason = '';
            }

            //No Connectivity available for either (return false if either exists in cart)
            else if (serviceAddress.startsWith('5')) {
                if (fibreOffer || publicWifiOffer) {
                    feasibility = 'false';
                    reason = 'GPON Connectivity RFS is already used by another HSIA service';
                }
            }

            //No Connectivity available for WiFi, but Connectivity required for FIFA Fibre Internet
            else if (serviceAddress.startsWith('6')) {
                if (fibreOffer) {
                    feasibilityInfoList.add(feasibilityInfoRegular);
                }
                if (publicWifiOffer) {
                    feasibility = 'false';
                    reason = 'GPON Connectivity RFS is already used by another HSIA service';
                }
            }

            //No Connectivity available for WiFi, no install required for FIFA Fibre Internet
            else if (serviceAddress.startsWith('7')) {
                if (publicWifiOffer) {
                    feasibility = 'false';
                    reason = 'GPON Connectivity RFS is already used by another HSIA service';
                }
            }

        }

        //simulate a web service response delay
        sleep(2500);

        outMap.put('feasibility', feasibility.equalsIgnoreCase('true'));
        outMap.put('reason', reason);
        outMap.put('feasibilityInfoList', feasibilityInfoList);
        return;
    }

    private static void sleep(integer milliseconds) 
    {
        Long timeDiff = 0;
        DateTime firstTime = System.now();
        do
        {
            timeDiff = System.now().getTime() - firstTime.getTime();
        }
        while(timeDiff <= milliseconds);      
    }
    
}