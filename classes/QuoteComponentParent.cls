public without sharing virtual class QuoteComponentParent {
    
  private QuoteComponentController componentController;
    
  public virtual QuoteComponentController getComponentController() {
    return componentController;
  }

  public virtual void setComponentController(QuoteComponentController compController) {
    componentController = compController;
  }
    
  public QuoteComponentParent getThis() {
    return this;
  }
  
}