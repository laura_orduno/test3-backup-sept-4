/**
 * A callback that is used to create Billable Charge whenever a Service Request is created.
 */
global class BillabeChargeCallback {
    webservice static void onServiceRequestsCreated(list<id> serviceRequestIds){
        if(serviceRequestIds!=null&&!serviceRequestIds.isempty()){
            list<service_request__c> serviceRequests=[select id,opportunity_product_item__r.all_non_recurring_revenue__c,opportunity_product_item__r.monthly_recurring_revenue__c from service_request__c where id=:serviceRequestIds];
            if(!serviceRequests.isempty()){
                list<srs_service_request_charge__c> newBillableCharges=new list<srs_service_request_charge__c>();
                list<schema.picklistentry> locationEntries=srs_service_request_charge__c.location__c.getdescribe().getpicklistvalues();
                string defaultLocation='';
                for(schema.picklistentry locationEntry:locationEntries){
                    if(locationEntry.isactive()&&locationEntry.isdefaultvalue()){
                        defaultLocation=locationEntry.getvalue();
                    }
                }
                if(string.isblank(defaultLocation)){
                    if(!locationEntries.isempty()){
                        defaultLocation=locationEntries[0].getvalue();
                    }
                }
                for(service_request__c serviceRequest:serviceRequests){
                    srs_service_request_charge__c allNonRecurringBillingCharge=new srs_service_request_charge__c(service_request__c=serviceRequest.id,location__c=defaultLocation,amount__c=serviceRequest.opportunity_product_item__r.all_non_recurring_revenue__c,charge_type__c='Activation/Non-Recurring');
                    srs_service_request_charge__c monthlyRecurringBillingCharge=new srs_service_request_charge__c(service_request__c=serviceRequest.id,location__c=defaultLocation,amount__c=serviceRequest.opportunity_product_item__r.monthly_recurring_revenue__c,charge_type__c='Monthly Recurring');
                    newBillableCharges.add(allNonRecurringBillingCharge);
                    newBillableCharges.add(monthlyRecurringBillingCharge);
                }
                if(!newBillableCharges.isempty()){
                    insert newBillableCharges;
                }
            }
        }
    }
}