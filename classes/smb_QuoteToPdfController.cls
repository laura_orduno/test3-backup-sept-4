/*
###########################################################################
# File..................: smb_QuoteToPdfController
# Version...............: 26
# Created by............: Puneet Khosla
# Created Date..........: 28-Dec-2013
# Last Modified by......: 
# Last Modified Date....: 
# Description...........: Quote PDF generation
# Copyright (c) 2000-2012. Astadia, Inc. All Rights Reserved.
#
# Created by the Astadia, Inc. Modification must retain the above copyright notice. 
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any commercial purpose, without fee, and without a written
# agreement from Astadia, Inc., is hereby forbidden. Any modification to source
# code, must include this paragraph and copyright.
#
# Permission is not granted to anyone to use this software for commercial uses.
#
# Contact address: 2839 Paces Ferry Road, Suite 350, Atlanta, GA 30339
# Company URL : http://www.astadia.com
###########################################################################
*/
/**
Change Log

Sept 18, 2014
Andy Leung
-  Updated the Product Name for product to include Telephone Number and Phone Setup Status
*/

public class smb_QuoteToPdfController 
{
    //-------------------- Properties --------------------
    public Opportunity opp {get;set;}   
    public list<ProductWrapper> pwMonthlyChargeList {get;set;}
    public list<ProductWrapper> pwMonthlyChargeBTList {get;set;}
    public list<ProductWrapper> pwOneChargeList {get;set;}
    public list<ProductWrapper> pwOneChargeBTList {get;set;}
    public list<MiscChargesWrapper> nrcMiscChargeList {get;set;}
    public boolean hasMRCBundlingBonus {get;set;}
    public boolean hasPositiveVoiceCharges {get;set;}
    public boolean hasNegativeVoiceCharges {get;set;}
    public boolean hasTrueBundle {get;set;}
    public string serviceAddress {get;set;}
    public string printedTotalMRC {get;set;}
    public string printedTotalNRC {get;set;}
    
    //-------------------- Variables --------------------
    public list<OpportunityLineItem> oliList;
    public id oppId;
    public string errorMsg;
    public Blob pdfBlob;
    private string attachmentName;
    private map<string,ProductWrapper> productBundleMap;
    
    //-------------------- Constants --------------------
    private static final string MRC_CHARGE = 'MRC';
    private static final string NRC_CHARGE = 'NRC';
    
    
    //-------------------- Constructor --------------------    
    public smb_QuoteToPdfController()
    {
        oppId = Id.ValueOf(ApexPages.currentPage().getParameters().get('id'));
        init();
    }
    
    //-------------------- Overloaded Constructor --------------------
    public smb_QuoteToPdfController(id opportunityId)
    {
        oppId = opportunityId;
        init();
        
    }
    
    /*
    * @author       : Puneet Khosla
    * @method Name  : init()
    * @description  : This method is the initialise method
    * @return       : Void
    * @param        : None          
    */  
    public void init()
    {
        errorMsg = '';
        try
        {
            opp = [SELECT id, Name, Printed_Total_MRC__c,Printed_Total_NRC__c, Account.Correct_Legal_Name__c, Account.Name, Quote_Expiration_Date__c, 
                        Printed_Service_Address__c,Order_ID__c,Send_Quote_Additional_Email__c,Send_Quote_Contact__r.email, Bundle_Type__c,Send_Quote_Contact__c,StageName,
                        Printed_Total_MRC_Bundling_Bonus__c,Printed_Voice_Positive_Service_Charges__c,Printed_Voice_Negative_Service_Charges__c
                        FROM Opportunity WHERE id = :oppId LIMIT 1].get(0); 
            
            // Check the stage again
            checkOppStage();
            hasMRCBundlingBonus = false;
            hasPositiveVoiceCharges = false;
            hasNegativeVoiceCharges = false;
            nrcMiscChargeList = new list<MiscChargesWrapper>();
            printedTotalMRC = '';
            printedTotalNRC = '';
            hasTrueBundle = false;
            if(String.IsBlank(errorMsg))
            {
                if(opp.Printed_Service_Address__c == null)
                    serviceAddress = '';
                else
                    serviceAddress = opp.Printed_Service_Address__c.replaceAll('\r\n','<br/>');
                
                // Check bundling bonus
                
                if(opp.Printed_Total_MRC_Bundling_Bonus__c != null)
                {
                    string cCharges = opp.Printed_Total_MRC_Bundling_Bonus__c;
                    cCharges = cCharges.replaceAll('[^0-9]','').trim();
                    if(Decimal.valueOf(cCharges) != 0.00)
                        hasMRCBundlingBonus = true;
                    
                }
                if(opp.Printed_Voice_Positive_Service_Charges__c != null)
                {
                    string cCharges = opp.Printed_Voice_Positive_Service_Charges__c;
                    cCharges = cCharges.replaceAll('[^0-9]','').trim();
                    if(Decimal.valueOf(cCharges) != 0.00)
                        nrcMiscChargeList.add(new MiscChargesWrapper('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Service Installation Charges',opp.Printed_Voice_Positive_Service_Charges__c));
                }
                                     
                if(opp.Printed_Voice_Negative_Service_Charges__c != null)
                {
                    string cCharges = opp.Printed_Voice_Negative_Service_Charges__c;
                    cCharges = cCharges.replaceAll('[^0-9]','').trim();
                    if(Decimal.valueOf(cCharges) != 0.00)
                        nrcMiscChargeList.add(new MiscChargesWrapper('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Service Installation Charges Waived',opp.Printed_Voice_Negative_Service_Charges__c));
                }
                
                if(opp.Printed_Total_MRC__c != null)
                    printedTotalMRC = SMB_Helper.formattedAmount('$' + String.ValueOf(opp.Printed_Total_MRC__c));
                if(opp.Printed_Total_NRC__c != null)
                    printedTotalNRC = SMB_Helper.formattedAmount('$' + String.ValueOf(opp.Printed_Total_NRC__c));
                
                    
                // Create an attachment Name
                string oppName = opp.Name;
                oppName = oppName.replaceAll('[^\\w\\s-]','');
                if(oppName.length() > 230)
                    oppName = oppName.substring(229);
                        
                attachmentName = oppName + ' - ' + datetime.now().format('dd-MMM-yyyy_hhmmss') + '.pdf';
                
                // Check for email
                if(opp.Send_Quote_Contact__c == null || String.IsBlank(String.valueOf(opp.Send_Quote_Contact__c)))
                {
                    errorMsg = ' There is no Quote Contact Associated with the Order.'; 
                }
                else if(opp.Send_Quote_Contact__r.email == null || String.IsBlank(opp.Send_Quote_Contact__r.email))
                {
                    errorMsg = ' Email Address of the Quote Contact is missing, thus Quote cannot be sent. ';
                }
                else
                {  
                
                    oliList = [SELECT Business_Tool__c,Printed_Display_Order__c,Printed_Indent_Level__c,Printed_MRC__c,Printed_NRC__c,Printed_Product_Name__c,
                                                Quantity,Element_Type__c,LineNo__c,Parent_Bundle_No__c,Parent_Product_No__c,Parent_Feature_No__c, 
                                                Display_Printed_MRC__c,Display_Printed_NRC__c,
                                                Printed_Child_Amount_Derived_MRC__c,Printed_Child_Product_Name_Derived_MRC__c,Printed_Child_Quantity_Derived_MRC__c,
                                                Printed_Child_Amount_Derived_NRC__c,Printed_Child_Product_Name_Derived_NRC__c,Printed_Child_Quantity_Derived_NRC__c,
                                                Printed_Document_Display_in_MRC__c, Printed_Document_Display_in_NRC__c
                                                FROM OpportunityLineItem 
                                                WHERE OpportunityId = :oppId AND Printed_Suppression_Flag__c = false
                                                ORDER BY Printed_Display_Order__c];
                    createList();
                }
            }
        }
        catch(Exception e)
        {
            errorMsg = ' Error in getting Opportunity Details . ' + e.getMessage();
            system.debug('### e : ' + e);
        }
    }
    
    /*
    * @author       : Puneet Khosla
    * @method Name  : checkOppStage()
    * @description  : This method checks the Order Stage as per custom setting
    * @return       : Void
    * @param        : None          
    */  
    private void checkOppStage()
    {
        map<string,SMB_Order_Stage_for_Quote_Agreement__c> orderStageQuoteMap = SMB_Order_Stage_for_Quote_Agreement__c.getAll();
        
        if(orderStageQuoteMap.containsKey(opp.StageName))
        {
            if(!(orderStageQuoteMap.get(opp.StageName).Allow_Quotes__c))
            {
                errorMsg = 'Quote cannot be sent at the current Order Stage : ' + opp.StageName;
            }
        }
        else
        {
            errorMsg = 'Quote cannot be sent at the current Order Stage : ' + opp.StageName;
        }  
    }
    
    /*
    * @author       : Puneet Khosla
    * @method Name  : createList()
    * @description  : This method starts the creation of the list
    * @return       : Void
    * @param        : None          
    */
    private void createList()
    {
        list<ProductWrapper> pwMonthlyList = new list<ProductWrapper>();
        list<ProductWrapper> pwOneList = new list<ProductWrapper>();
        
        pwMonthlyChargeList = new list<ProductWrapper>();
        pwMonthlyChargeBTList = new list<ProductWrapper>();
        pwOneChargeList = new list<ProductWrapper>();
        pwOneChargeBTList = new list<ProductWrapper>();
        productBundleMap = new map<string,ProductWrapper>();
          
        for(OpportunityLineItem oli : oliList)
        {
            if(oli.Element_Type__c != null && oli.Element_Type__c.containsIgnoreCase('Feature'))
            {
                //if(oli.Display_Printed_MRC__c)
                //{
                    ProductWrapper pw = productBundleMap.get(MRC_CHARGE + oli.Parent_Product_No__c);
                    if(pw != null)
                    {
                        //pw.hasCharges = true;
                        FeaturesWrapper fw = new FeaturesWrapper(oli,true);
                        pw.features.add(fw);
                        pw.featureMap.put(Integer.valueOf(oli.LineNo__c),fw);
                        /*if(oli.Display_Printed_MRC__c)
                        {
                            pw.hasCharges = true;
                            //fillParentCharges(MRC_CHARGE,pw,oli);
                        }*/
                    }
                        
                //}
                //if(oli.Display_Printed_NRC__c)
                //{
                    pw = productBundleMap.get(NRC_CHARGE + oli.Parent_Product_No__c);
                    if(pw != null)
                    {
                        //pw.hasCharges = true;
                        FeaturesWrapper fw = new FeaturesWrapper(oli,false);
                        pw.features.add(fw);
                        pw.featureMap.put(Integer.valueOf(oli.LineNo__c),fw);
                        /*if(oli.Display_Printed_NRC__c)
                        {
                            pw.hasCharges = true;
                            //fillParentCharges(NRC_CHARGE,pw,oli);
                        }*/
                    }   
                //}
            }
            else if(oli.Element_Type__c != null && !(oli.Element_Type__c.containsIgnoreCase('Header Bundle')))
            {
                //if(oli.Printed_MRC__c != null)
                //{
                    ProductWrapper pw = new ProductWrapper(oli,true);
                    productBundleMap.put(MRC_CHARGE + oli.LineNo__c,pw); 
                    pwMonthlyList.add(pw);
                    //fillParentCharges(MRC_CHARGE,pw,oli);
                    
                //}
                //if(oli.Printed_NRC__c != null)
                //{
                    pw = new ProductWrapper(oli,false);
                    productBundleMap.put(NRC_CHARGE + oli.LineNo__c,pw);
                    pwOneList.add(pw);
                    //fillParentCharges(NRC_CHARGE,pw,oli);   
                //}
                
                if(oli.Element_Type__c.equalsIgnoreCase('Bundle'))
                    hasTrueBundle = true;
            }        
        }
        
        // Separate Business Tools and add PrintedProd Name in the end of features
        map<integer, list<ProductWrapper>> tempMap = createFinalList(pwMonthlyList);
        pwMonthlyChargeList = tempMap.get(1);
        pwMonthlyChargeBTList = tempMap.get(2);
        
        tempMap = createFinalList(pwOneList);
        pwOneChargeList = tempMap.get(1);
        pwOneChargeBTList = tempMap.get(2);
                
    }
    
    
    /*
    * @author       : Puneet Khosla
    * @method Name  : fillParentCharges()
    * @description  : If a child has charges then ensure that Parent is displayed too
    * @return       : Void
    * @param        : string chargeType : Whether MRC or NRC
    *                 ProductWrapper pw : Product Wrapper Object being referred
    *                 OpportunityLineItem oli : OLI being traversed    
    */
    /* No Longer Required 
    public void fillParentCharges(string chargeType,ProductWrapper pw, OpportunityLineItem oli)
    {
        System.debug('### pw ' + pw + ' ### oli ' + oli);
        
        if( oli.Element_Type__c.containsIgnoreCase('Sub Feature'))  
        {
            FeaturesWrapper fw = pw.featureMap.get(Integer.valueOf(oli.Parent_Feature_No__c));
            if(fw != null)
            {
                fw.hasCharges = true;
            }
        }
                
        if(pw.hasCharges && pw.elementType.contains('product'))
        {
            ProductWrapper pwB = productBundleMap.get(chargeType + oli.Parent_Bundle_No__c);
            if(pwB != null)
                pwB.hasCharges = true;
            if( pw.elementType == 'over line product')  
            {
                ProductWrapper pwP = productBundleMap.get(chargeType + oli.Parent_Product_No__c);
                if(pwP != null)
                    pwP.hasCharges = true;
            }
        }
    }
    */
    /*
    * @author       : Puneet Khosla
    * @method Name  : createFinalList()
    * @description  : Separate BusinessTools and Products
    * @return       : map<integer,list<ProductWrapper>> : containing 2 lists
    * @param        : list<ProductWrapper> pwList : Combined List    
    */
    public map<integer,list<ProductWrapper>> createFinalList(list<ProductWrapper> pwList)
    {
        map<integer, list<ProductWrapper>> returnMap = new map<integer, list<ProductWrapper>>();
        
        list<ProductWrapper> mainList = new list<ProductWrapper>();
        list<ProductWrapper> btList = new list<ProductWrapper>();
        
        for(ProductWrapper pw : pwList)
        {
            list<FeaturesWrapper> fwList = new list<FeaturesWrapper>();
            for(FeaturesWrapper fw : pw.features)
            {
                if (fw.hasCharges)
                    fwList.add(fw);
                if(fw.hasPrintedChild)
                    fwList.add(new FeaturesWrapper(fw.printedChildName,fw.printedChildQuantity,fw.printedChildCharges));     
            }
            pw.features.clear();
            //pw.features.addAll(fwList);
            
            if(pw.hasPrintedChild)
            {                 
                pw.features.add(new FeaturesWrapper(pw.printedChildName,pw.printedChildQuantity,pw.printedChildCharges));
            }
            pw.features.addAll(fwList);
            system.debug('### pw.features : ' + pw.features);
            if(pw.businessTool && pw.elementType.contains('product'))
            {
                if(pw.hasCharges)
                    btList.add(pw);
            }
            else if(pw.hasCharges)
                mainList.add(pw);
        }
        returnMap.put(1,mainList);
        returnMap.put(2,btList);
        return returnMap;
    }
    
    /*
    * @author       : Puneet Khosla
    * @method Name  : attachQuote()
    * @description  : Function to Attach Quote PDF
    * @return       : void
    * @param        : None    
    */
    public void attachQuote() 
    {
        
        if(String.IsBlank(errorMsg))
        {
            /* Get the page definition */
            PageReference pdfPage = new PageReference( '/apex/smb_QuoteToPdf' );
            
            /* set the quote id on the page definition */
            pdfPage.getParameters().put('id',oppId);
            pdfPage.getParameters().put('isdtp','vw');
            
            System.debug('pdfPage : ' + pdfPage);
            /* generate the pdf blob */
            try{ 
                pdfBlob = pdfPage.getContent();
            }catch(Exception e){
                if(!Test.isRunningTest()){
                    errorMsg = 'Error while Generating PDF' + e.getMessage();
                }
                pdfBlob = Blob.valueOf('Error while Generating PDF ');
            }
            
            System.debug('pdfBlob : ' + pdfBlob);
            /* create the attachment against the quote */
            if(String.IsBlank(errorMsg)){
             Attachment a = new Attachment(parentId = opp.id, name= attachmentName, body = pdfBlob);
            
                /* insert the attachment */
                try
                {
                    insert a;
                }
                catch(Exception e)
                {
                    errorMsg = ' Error in generating PDF. ' + e.getMessage();
                }   
            }
        }
         
    }
    
    /*
    * @author       : Puneet Khosla
    * @method Name  : sendEmail()
    * @description  : Function to Send Email
    * @return       : void
    * @param        : None    
    */
    public void sendEmail()
    {
        if(String.IsBlank(errorMsg))
        {
            try
            {
            
                map<string,SMB_QuoteEmailTemplates__c> emailTemplates = SMB_QuoteEmailTemplates__c.getAll();
                
                string emailTemplateName;
                Id emailTemplateId; 
                
                if(String.isNotBlank(opp.Bundle_Type__c) & emailTemplates.containsKey(opp.Bundle_Type__c))
                {
                    emailTemplateName = emailTemplates.get(opp.Bundle_Type__c).TemplateName__c;
                }
                else
                {
                    emailTemplateName = emailTemplates.get('Default').TemplateName__c;
                }
                
                
                if(String.IsBlank(emailTemplateName))
                {
                    errorMsg = 'Missing Email Template.';
                }
                else
                {
                    
                    EmailTemplate emlTemp = [Select Id From EmailTemplate e WHERE DeveloperName = :emailTemplateName LIMIT 1].get(0);
                    emailTemplateId = Id.ValueOf(emlTemp.Id);
                                   
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    List<String> lstEmail = new List<String>();
                        
                    //lstEmail.add(opp.Send_Quote_Contact__r.email);
                    mail.setToAddresses(lstEmail);
                    mail.setSenderDisplayName('noreply@salesforce.com');
                    mail.setReplyTo('noreply@salesforce.com');
                    
                    mail.setTargetObjectId(opp.Send_Quote_Contact__c);
                    mail.setWhatId(oppId);
                    mail.setTemplateId(emailTemplateId);
                   
                    Messaging.EmailFileAttachment pdfAttc = new Messaging.EmailFileAttachment();
                    
                    
                    pdfAttc.setFileName(attachmentName);
                    pdfAttc.setBody(pdfBlob);
                    mail.setFileAttachments(new Messaging.EmailFileAttachment[]{pdfAttc});
                               
                        
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                    
                    if(String.IsNotBlank(opp.Send_Quote_Additional_Email__c))
                    {
                        
                                       
                        Messaging.SingleEmailMessage addMail = new Messaging.SingleEmailMessage();
                        lstEmail.clear();
                        lstEmail.add(opp.Send_Quote_Additional_Email__c);
                        addMail.setToAddresses(lstEmail);
                     
                        if(mail.getHtmlBody() != null && mail.getHtmlBody() != '')
                            addMail.setHtmlBody(mail.getHtmlBody());
                       
                        if(mail.getPlainTextBody() != null && mail.getPlainTextBody()!= '')
                            addMail.setPlainTextBody(mail.getPlainTextBody());
                       
                        if(addMail.getHtmlBody() == null && addMail.getPlainTextBody() == null) 
                            addMail.setPlainTextBody('');
                        
                        
                        addMail.setSubject(mail.getSubject());
                        
                        addMail.setToAddresses(lstEmail);
                        addMail.setSenderDisplayName('noreply@salesforce.com');
                        addMail.setReplyTo('noreply@salesforce.com');
                    
                        addMail.setFileAttachments(new Messaging.EmailFileAttachment[]{pdfAttc});
                        
                        
                        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { addMail });
                    }
                }
            }
            catch(Exception e)
            {
                errorMsg = ' Error in sending email. ' + e.getMessage();
            }
        }
                
    }
    
    /*
    * @author       : Puneet Khosla
    * @Class Name   : ProductWrapper
    * @description  : Wrapper Class for Products / Bundles
    */
    public class ProductWrapper
    {
        public string printedProductName {get;set;}
        public integer quantity{get;set;}
        public string charges{get;set;}
        public string printedChildName {get;set;}
        public integer printedChildQuantity {get;set;}
        public string printedChildCharges {get;set;}
        public boolean hasPrintedChild;
        public boolean businessTool {get;set;}
        private string spaceChar = '&nbsp;';
        public string styleClass{get;set;}
        public List<string> styleHeaderClassList {get;set;}
        public string colStyle {get;set;}
        public string elementType{get;set;}
        public list<FeaturesWrapper> features {get;set;}
        public boolean hasCharges;
        public map<integer,FeaturesWrapper> featureMap;
        
        
        /*public ProductWrapper(string pName, decimal pQty, string pCharges)
        {
            printedProductName = pName;   
            quantity = pQty;
            charges = pCharges; 
            hasCharges = true;             
        }*/
        
        public ProductWrapper(OpportunityLineItem oli,boolean mrc)
        {
            features = new list<FeaturesWrapper>();
            featureMap = new map<integer,FeaturesWrapper>();
            hasPrintedChild = false;
            styleHeaderClassList = new list<string>();
            
            if(oli.Printed_Indent_Level__c >= 0)
                printedProductName = spaceChar.repeat((Integer.ValueOf(oli.Printed_Indent_Level__c)+1) * 5) + oli.Printed_Product_Name__c;
            else
                 printedProductName = oli.Printed_Product_Name__c;   
            quantity = Integer.ValueOf(oli.quantity);
                        
            elementType = oli.Element_Type__c;
            if(elementType==null)
            {
                elementType = '';
            }
            else
                elementType = elementType.toLowerCase();
            
            if(mrc)
            {
                charges = oli.Printed_MRC__c;
                hasCharges = oli.Printed_Document_Display_in_MRC__c;
                
                //if(elementType.containsIgnoreCase('Product') && (oli.Printed_Child_Product_Name_Derived_MRC__c!= null || String.IsNotBlank(oli.Printed_Child_Product_Name_Derived_MRC__c)))
                if((oli.Printed_Child_Product_Name_Derived_MRC__c!= null || String.IsNotBlank(oli.Printed_Child_Product_Name_Derived_MRC__c)))
                {
                    printedChildName = spaceChar.repeat((Integer.ValueOf(oli.Printed_Indent_Level__c) + 2) * 5) + oli.Printed_Child_Product_Name_Derived_MRC__c;
                    //printedChildQuantity = Integer.ValueOf(oli.Printed_Child_Quantity_Derived_MRC__c);
                    printedChildCharges = oli.Printed_Child_Amount_Derived_MRC__c;
                    //hasPrintedChild = true;
                    //hasCharges = true;
                }
                
            }       
            else
            {
                charges = oli.Printed_NRC__c;
                hasCharges = oli.Printed_Document_Display_in_NRC__c;
                
                //if(elementType.containsIgnoreCase('Product') && (oli.Printed_Child_Product_Name_Derived_NRC__c!= null || String.IsNotBlank(oli.Printed_Child_Product_Name_Derived_NRC__c)))
                if((oli.Printed_Child_Product_Name_Derived_NRC__c!= null || String.IsNotBlank(oli.Printed_Child_Product_Name_Derived_NRC__c)))
                {
                    printedChildName = spaceChar.repeat((Integer.ValueOf(oli.Printed_Indent_Level__c) + 2) * 5) + oli.Printed_Child_Product_Name_Derived_NRC__c;
                    //printedChildQuantity = Integer.ValueOf(oli.Printed_Child_Quantity_Derived_NRC__c);
                    printedChildCharges = oli.Printed_Child_Amount_Derived_NRC__c;
                    //hasPrintedChild = true;
                    //hasCharges = true;
                }
                
            }
            
            if(String.IsNotBlank(printedChildName) && String.IsNotBlank(printedChildCharges))
            {
                string pcCharges = printedChildCharges;
                pcCharges = pcCharges.replaceAll('[^0-9]','').trim();
                if(pcCharges.IsNumeric())
                {
                    if(Integer.valueOf(pcCharges) != 0)
                        hasPrintedChild = true;
                }
            }
            printedChildCharges = null;
            printedChildQuantity = null;
            businessTool = oli.Business_Tool__c;
            
            // Set Stylesheet
            colStyle = 'background-color:white';
            if(elementType.contains('bundle'))
            {
                if(hasPrintedChild)
                {
                    styleHeaderClassList.add('lavenderTextName');
                    styleHeaderClassList.add('lavenderTextCenterQty');
                    styleHeaderClassList.add('lavenderTextCenterCharge');
                    colStyle = 'background-color:#e8d9f1';
                }
                else
                    styleClass = 'lavenderback';
            }
            else if(elementType.contains('product'))
                styleClass = 'productOnly';
            else
                styleClass = 'normalText';
           
           if(styleHeaderClassList.size() < 3)
           {
                styleHeaderClassList.clear();
                styleHeaderClassList.add('normalTextName');
                styleHeaderClassList.add('normalTextCenterQty');
                styleHeaderClassList.add('normalTextCenterCharge');
           }
            
        }
                
    }
    
    /*
    * @author       : Puneet Khosla
    * @Class Name   : FeaturesWrapper
    * @description  : Wrapper Class for Features
    */    
    public class FeaturesWrapper
    {
        public string printedProductName {get;set;}
        public integer quantity{get;set;}
        public string charges{get;set;}
        private string spaceChar = '&nbsp;';
        public boolean hasCharges;
        public string printedChildName {get;set;}
        public integer printedChildQuantity {get;set;}
        public string printedChildCharges {get;set;}
        public boolean hasPrintedChild;
        
        public FeaturesWrapper(OpportunityLineItem oli,boolean mrc)
        {
            hasPrintedChild = false;
            if(oli.Printed_Indent_Level__c >= 0)
                 printedProductName = spaceChar.repeat((Integer.ValueOf(oli.Printed_Indent_Level__c)+1) * 5) + oli.Printed_Product_Name__c;
            else
                 printedProductName = oli.Printed_Product_Name__c;   
            quantity = Integer.ValueOf(oli.quantity);
                        
            if(mrc)
            {
                charges = oli.Printed_MRC__c;
                hasCharges = oli.Printed_Document_Display_in_MRC__c;
                
                //if(elementType.containsIgnoreCase('Product') && (oli.Printed_Child_Product_Name_Derived_MRC__c!= null || String.IsNotBlank(oli.Printed_Child_Product_Name_Derived_MRC__c)))
                if((oli.Printed_Child_Product_Name_Derived_MRC__c!= null && String.IsNotBlank(oli.Printed_Child_Product_Name_Derived_MRC__c)))
                {
                    printedChildName = spaceChar.repeat((Integer.ValueOf(oli.Printed_Indent_Level__c) + 2) * 5) + oli.Printed_Child_Product_Name_Derived_MRC__c;
                    printedChildQuantity = Integer.ValueOf(oli.Printed_Child_Quantity_Derived_MRC__c);
                    printedChildCharges = oli.Printed_Child_Amount_Derived_MRC__c;
                    //hasPrintedChild = true;
                    //hasCharges = true;
                }   
                
            }       
            else
            {
                charges = oli.Printed_NRC__c;
                hasCharges = oli.Printed_Document_Display_in_NRC__c;
                
                //if(elementType.containsIgnoreCase('Product') && (oli.Printed_Child_Product_Name_Derived_NRC__c!= null || String.IsNotBlank(oli.Printed_Child_Product_Name_Derived_NRC__c)))
                if((oli.Printed_Child_Product_Name_Derived_NRC__c!= null && String.IsNotBlank(oli.Printed_Child_Product_Name_Derived_NRC__c)))
                {
                    printedChildName = spaceChar.repeat((Integer.ValueOf(oli.Printed_Indent_Level__c) + 2) * 5) + oli.Printed_Child_Product_Name_Derived_NRC__c;
                    printedChildQuantity = Integer.ValueOf(oli.Printed_Child_Quantity_Derived_NRC__c);
                    printedChildCharges = oli.Printed_Child_Amount_Derived_NRC__c;
                    //hasPrintedChild = true;
                    //hasCharges = true;
                }
                
            }
            
            if(String.IsNotBlank(printedChildName) && String.IsNotBlank(printedChildCharges))
            {
                string pcCharges = printedChildCharges;
                pcCharges = pcCharges.replaceAll('[^0-9]','').trim();
                if(String.IsNotBlank(pcCharges))
                {
                    if(Integer.valueOf(pcCharges) != 0)
                        hasPrintedChild = true;
                }
            } 
            printedChildCharges = null;
            printedChildQuantity = null;
        }
        public FeaturesWrapper(string pName, decimal pQty, string pCharges)
        {
            printedProductName = pName;   
            quantity = Integer.valueof(pQty);
            charges = pCharges; 
            hasCharges = true;
            hasPrintedChild = false;             
        }
                
    }   
    
    /*
    * @author       : Puneet Khosla
    * @Class Name   : MiscChargesWrapper
    * @description  : Wrapper Class for Misc Charges
    */    
    public class MiscChargesWrapper
    {
        public string printedChargesName {get;set;}
        public integer quantity{get;set;}
        public string charges{get;set;}
        
        public MiscChargesWrapper (string cName, string cCharges)
        {
            printedChargesName = cName;
            quantity = 1;
            charges = cCharges;
        }
    }
    
}