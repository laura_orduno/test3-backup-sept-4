@isTest
public class ContractSavePropagateCtrlExtTest {

    @istest
    public static void testSavePropagate(){
        
        Account acc = new Account(Name='Testing Software', BillingCountry = 'US', BillingState = 'IL');
        insert acc;
        Contract agr = new Contract();
        agr.AccountId = acc.Id;
        agr.Billing_Cycle__c = '30';
        for(String f :getUpdatableCheckBoxField('Contract')){
            agr.put(f,false);
        }
        agr.Churn_Risk_Process__c = 'ABCD';
        insert agr;
        Subsidiary_Account__c sub = new Subsidiary_Account__c();
        sub.Contract__c = agr.id;
        insert sub;
        
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController(agr);
        ContractSavePropagateCtrlExt ctrlExt = new ContractSavePropagateCtrlExt(stdCtrl);
        ctrlExt.doEdit();
        ctrlExt.doCancel();
        ctrlExt.doSave();
        ctrlExt.getShowEditButton();
       ctrlExt.showPropagate();
       ctrlExt.displayItems.get(0).selected = true;
       //ctrlExt.SavePropagate(); 
        
        
    }
    public static  List<String> getUpdatableCheckBoxField(String objName){
         Map<String, Schema.SObjectField> fieldMap = Util.getObjectTypeDescribe(objName).fields.getMap();
        List<String> updatableFields = new List<String>(); 
        for(Schema.SObjectField f : fieldMap.values()) {
          if( f.getDescribe().isUpdateable()  && 'BOOLEAN'.equals(String.valueOf(f.getDescribe().getType())))
                updatableFields.add(f.getDescribe().Name);
        }
        return updatableFields;
    }    
}