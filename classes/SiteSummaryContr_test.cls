@isTest
Public class SiteSummaryContr_test{

      @isTest
      Public static void TestingMethod(){
        Schema.DescribeSObjectResult d = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
          
        Id recSerTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        Id recRCIDTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId();        
          
        Account RCIDacc = new Account(name='debs',recordtypeid=recRCIDTypeId);
        Account seracc = new Account(name='123 street',recordtypeid=recSerTypeId,parentid=RCIDacc.id);          
          
        List<Account> acclist= new List<Account>();          
        acclist.add(RCIDacc);
        acclist.add(seracc );
        insert acclist;
          
        smbcare_address__c address= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N5',FMS_Address_ID__c='987650',account__c=RCIDacc.id); 
        smbcare_address__c address3= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N5',FMS_Address_ID__c='987652',account__c=RCIDacc.id,service_account_Id__c=seracc.id); 
        address3.Block__c = 'block number';
        address3.Building_Number__c = '1234';
        address3.Street_Number__c = '2222';
        address3.Street_Name__c = 'some street name';
        
        smbcare_address__c address2= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N5',FMS_Address_ID__c='987651',account__c=RCIDacc.id); 
        List<smbcare_address__c> smbCarelist= new List<smbcare_address__c>(); 
        smbCarelist.add(address);
        smbCarelist.add(address3);
        insert smbCarelist;
        
        ServiceAddressData SData = new ServiceAddressData();
        SData.isManualCapture=true;
        SData.fmsId='987652';
        
        Contact Cont = new Contact (FirstName ='FName', LastName='LNAme', AccountId=seracc.Id, Email='Abc@abc.com');
        insert Cont;
        
        Contract Ct = new Contract(AccountId = RCIDacc.Id, Status = 'Draft',StartDate = Date.Today(), ContractTerm = 12);
        Insert Ct;
        
        Id standardPriceBookId = Test.getStandardPriceBookId();
        
        SMBCare_Address__c smbCareAddr = new SMBCare_Address__c(Account__c=RCIDacc.Id, Postal_Code__c='M1T3N3', Street_Number__c='Street 1', Street_Name__c='Street Name 1', Province__c='ONT', Suite_Number__c='1001', COID__c='COID 1', Building_Number__c='Building 1', Country__c='CAN', FMS_Address_ID__c='FMSID 121');
        insert smbCareAddr;
        
         Opportunity Opp= new Opportunity(Name='Test Opp',StageName='Discovery',CloseDate=System.today().addDays(5));
        insert Opp; 
        
        Quote Qt= new Quote(Name='Test Quote11',opportunityid=opp.id);
        insert Qt;
        
        Order Od = new Order(Name = 'ORD-000001', Shipping_Address__c='2010 100 AV NW EDMONTON AB T5A3M1', Service_Address_Text__c='2010 100 AV NW EDMONTON AB T5A3M1' , FMS_Address_ID__c='FMSID 121', ShippingStreet='2010 100 AV', ShippingCity='EDMONTON', BAN__c='Ban 21' , PriceBook2Id =standardPriceBookId  , AccountId = RCIDacc.Id, EffectiveDate = Date.Today(), ContractId = Ct.Id, Status = 'Draft', vlocity_cmt__QuoteId__c=Qt.Id);
        Od.Service_Address__c = address3.Id;
        Insert Od;
        
        Product2 Pd = new Product2(Name='Porduct 1',isActive=true);
        insert Pd;
        
        ServiceAddressOrderEntry__c JuncObj1 = new ServiceAddressOrderEntry__c (Order__c=Od.Id,Address__c = smbCareAddr.Id);
        ServiceAddressOrderEntry__c JuncObj2 = new ServiceAddressOrderEntry__c (Order__c=Od.Id,Address__c = address.Id);
        List<ServiceAddressOrderEntry__c> SelectedServiceAddress = new List<ServiceAddressOrderEntry__c>();
        SelectedServiceAddress.add(JuncObj1);
        SelectedServiceAddress.add(JuncObj2);
        insert  SelectedServiceAddress;

       
        PricebookEntry Pbe = new PricebookEntry(Pricebook2Id=standardPriceBookId, Product2Id=pd.Id, UnitPrice=99, isActive=true);
        insert Pbe;

        OrderItem ordPd = new OrderItem(PriceBookEntryId=Pbe.Id, OrderId=Od.Id, Quantity=1, UnitPrice=99);
        insert ordPd;
        
        //Custom Setting for Endpoint Field
         
        SMBCare_WebServices__c cs = new SMBCare_WebServices__c(Name='SRS_ASFEndPoint',Value__c='https://xmlgwy-pt1.telus.com:9030/dv01/SMO/OrderMgmt/ServiceAddressManagementService_v1_1_vs0'); 
        insert cs;
        
        
        SMBCare_WebServices__c cs1 = new SMBCare_WebServices__c(Name='username',Value__c ='APP_SFDC'); 
        insert cs1;
      
        SMBCare_WebServices__c cs2 = new SMBCare_WebServices__c(Name='password',Value__c='soaorgid'); 
        insert cs2; 
        Test.setCurrentPageReference(new PageReference('Page.NAAS_SiteSummaryPage')); 
        System.currentPageReference().getParameters().put('id', Od.Id);
        System.currentPageReference().getParameters().put('ParentAccId',RCIDacc.Id);
        SiteSummaryPageContr site = new SiteSummaryPageContr();
        site.ParentAccId = RCIDacc.Id;
        site.contactId = Cont.Id; 
        site.getSMBCareWrapperClass();
        site.RedirecttoCartPage();
        site.NexttoCartPage();
        site.getSMBCareWrapperClass();
        site.updateSaoObject(Od.Id, Cont.Id);
        Test.setCurrentPageReference(new PageReference('Page.NAAS_SiteSummaryPage')); 
        System.currentPageReference().getParameters().put('id', Od.Id);
        System.currentPageReference().getParameters().put('ParentAccId',null);
        SiteSummaryPageContr siteWithoutParentId = new SiteSummaryPageContr();
        site.updateSaoObject();
        siteWithoutParentId.RedirecttoCartPage();
        siteWithoutParentId.getSMBCareWrapperClass();        
      }
 }