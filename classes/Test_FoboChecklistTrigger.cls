@istest(SeeAllData=true)
private class Test_FoboChecklistTrigger {
	@istest
	static void testBasicFlows(){
		/*
		list<sobject> fobochecklists=test.loaddata(fobochecklist__c.sobjecttype,'TestFoboChecklist');
		for(sobject fobo:fobochecklists){
			fobochecklist__c fobochecklist=(fobochecklist__c)fobo;
			fobochecklist.opportunity__c=Oppt.id;
		}*/
		test.starttest();
        integer numNCIntegration= [SELECT count() FROM Product2 WHERE Sterling_Item_ID__c ='TLSSMBVLTN-NC-TSTPRDCT'];
        if(numNCIntegration==0){
			map<string,schema.recordtypeinfo> productRecordTypeMap=product2.getSObjectType().getdescribe().getrecordtypeinfosbyname();
			product2 product=new product2(name='NC Integration Product',recordtypeid=productRecordTypeMap.get('SMB Care Customer Central Product').getrecordtypeid(),sterling_item_id__c='TLSSMBVLTN-NC-TSTPRDCT',
			Sterling_Item_Key__c='20140114183444978382',IncludeQuoteContract__c=true,isactive=true,SBQQ__AssetConversion__c='One per quote line');
			insert product;
        }
        FOBO_Test_Util.createFobocheckList();
		//insert fobochecklists;
		test.stoptest();
	}
}