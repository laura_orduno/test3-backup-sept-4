@isTest(SeeAllData=true)
private class ProductTeamMemberServiceTest {
	testMethod static void testDeleteMember() {
		Account acc = TestUtil.createAccount();
		Opportunity opp = TestUtil.createOpportunity(acc.Id);
		Opportunity_Solution__c solution = TestUtil.createOpportunitySolution(opp.Id);
		List<Opp_Product_Item__c> items = TestUtil.createPLIList(opp.Id);

		User teamMember = [Select Id from User where isActive = true and Id <> :UserInfo.getUserId() limit 1];

		Product_Sales_Team__c pst = new Product_Sales_Team__c(Product__c = items.get(0).Id, Member__c = teamMember.Id, Access__c = 'Read');

		ProductTeamMemberService.deleteMember(pst.Id);		
		insert pst;	

		ProductTeamMemberService.deleteMember(pst.Id);		
	}
}