@isTest
private class AliasedMetaDataUtilsTest {
    private static testMethod void testGetType() {
        system.assertEquals(null, AliasedMetaDataUtils.getType(null));
        system.assertEquals(SBQQ__Quote__c.sObjectType, AliasedMetaDataUtils.getType('Quote'));
        system.assertEquals(Account.sObjectType, AliasedMetaDataUtils.getType('Account'));
    }
    
    private static testMethod void testGetField() {
        String nullString;
        system.assertEquals(null, AliasedMetaDataUtils.getField('Account', nullString));
        system.assertEquals(null, AliasedMetaDataUtils.getField(Account.SObjectType, nullString));
        system.assertEquals(null, AliasedMetaDataUtils.getField(nullString, 'Name'));
        
        system.assertEquals(SBQQ__Quote__c.SBQQ__Type__c, AliasedMetaDataUtils.getField('Quote', 'Type'));
        system.assertEquals(Account.name, AliasedMetaDataUtils.getField('Account', 'Name'));
        
        system.assertEquals(SBQQ__Quote__c.SBQQ__Type__c, AliasedMetaDataUtils.getField(SBQQ__Quote__c.sObjectType, 'Type'));
        system.assertEquals(Account.name, AliasedMetaDataUtils.getField(Account.sObjectType, 'Name'));
    }
    
    private static testMethod void testSetFieldValue() {
        Account a = new Account();
        AliasedMetaDataUtils.setFieldValue(a, Account.name, 'test');
        system.assertEquals('test', a.name);
        
        SBQQ__Quote__c q = new SBQQ__Quote__c();
        AliasedMetaDataUtils.setFieldValue(q, SBQQ__Quote__c.SBQQ__Type__c, 'test');
        system.assertEquals('test', q.SBQQ__Type__c);
    }
    
    private static testMethod void testGetFieldValue() {
        Account a = new Account(name = 'test');
        system.assertEquals('test', AliasedMetaDataUtils.getFieldValue(a, Account.name));
        system.assertEquals('test', AliasedMetaDataUtils.getFieldValue(a, 'Name'));
        
        SBQQ__Quote__c q = new SBQQ__Quote__c(SBQQ__Type__c = 'test');
        system.assertEquals('test', AliasedMetaDataUtils.getFieldValue(q, SBQQ__Quote__c.SBQQ__Type__c));
        system.assertEquals('test', AliasedMetaDataUtils.getFieldValue(q, 'SBQQ__Type__c'));
    }
    
    private static testMethod void testSetFieldObject() {
        Account a = new Account();
        Contact c = new Contact();
        
        AliasedMetaDataUtils.setFieldObject(c, Contact.AccountId, a);
        
        system.assertEquals(c.Account, a);
    }
    
    private static testMethod void testGetFieldObject() {
        Account a = new Account();
        Contact c = new Contact(Account = a);
        
        system.assertEquals(a, AliasedMetaDataUtils.getFieldObject(c, Contact.AccountId));
    }
    
    private static testMethod void testDetermineQuoteType() {
        Opportunity o = new Opportunity(name = 'test', closedate = Date.today(), stagename = 'test');
        insert o;
        
        SBQQ__Quote__c quote = new SBQQ__Quote__c(SBQQ__Opportunity__c = o.id);
        insert quote;
        
        SBQQ__WebQuote__c webquote = new SBQQ__WebQuote__c(SBQQ__Opportunity__c = o.id);
        insert webquote;
        
        system.assertEquals(SBQQ__Quote__c.SObjectType, AliasedMetaDataUtils.determineQuoteType(quote.id));
        system.assertEquals(SBQQ__WebQuote__c.SObjectType, AliasedMetaDataUtils.determineQuoteType(webquote.id));
    }
    
    private static testMethod void testDetermineQuoteLineType() {
        Opportunity o = new Opportunity(name = 'test', closedate = Date.today(), stagename = 'test');
        insert o;
        
        SBQQ__Quote__c quote = new SBQQ__Quote__c(SBQQ__Opportunity__c = o.id);
        insert quote;
        
        SBQQ__WebQuote__c webquote = new SBQQ__WebQuote__c(SBQQ__Opportunity__c = o.id);
        insert webquote;
        
        system.assertEquals(SBQQ__QuoteLine__c.SObjectType, AliasedMetaDataUtils.determineQuoteLineType(quote.id));
        system.assertEquals(SBQQ__WebQuoteLine__c.SObjectType, AliasedMetaDataUtils.determineQuoteLineType(webquote.id));
    }
}