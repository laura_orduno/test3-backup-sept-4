/**
 * @author Santosh Rath
 * 
 **/
public class OrdrTnLineNumber  extends OrdrTnNpaNxx {
    // Telephone: Line Number (i.e. representing the last 4 digits of TN) with their associated availability status (AVAILABLE, RESERVED_UNASSIGNED, RESERVED_ASSIGNED)
	private String lineNumber;
	private OrdrTnLineNumberStatus status = OrdrTnLineNumberStatus.AVAILABLE;
	private String salesforceLineItemId;
	// OrdrTnReservationType: Used to track in Salesforce which backend system is the source for this TN (this is used later on by TOCP to route the TN reservation requests to the proper system e.g. FMS versus SMS)
	// Enum: WLN, TOLLFREE, WLS - respectively for:
	// Wireline (sourced from FMS through ASF & TOCP - encompassing Voice Single-Line, Multi-line, Overline, Internet Dark ADSL),
	// Toll-Free (sourced from SMS through TOCP - encompassing Business Tools - Toll-Free),  
	// and Wireless
	private OrdrTnReservationType numberReservationType = null;

	//  OrdrTnAssignedProductType: Used to track in Salesforce which product type is assigned to a given Reserved TN (this is used later on to clear display to Agents where a TN is attached)
	// (Enum: SL, ML, OL, ADSL, WLS, TF - respectively for Voice Single-Line, Multi-line, Overline, Internet Dark ADSL, Wireless and Toll-Free)
	private  OrdrTnAssignedProductType assignedProductType = null;

	//ReleaseDate is provided by ASF/FMS when invoking TOCP FindResource contract to request real TN(s)
	private String releaseDate = null;
	
	//Comments set while reserving TN(s) e.g. error descriptions
	private String comments = null;

	public OrdrTnLineNumber() {
	}

	public OrdrTnLineNumber(
			String npa,
			String nxx,
			String lineNumber,
			OrdrTnReservationType numberReservationType) {
		super(npa, nxx);
		this.lineNumber = lineNumber;
		this.numberReservationType = numberReservationType; 
		// Assume the status is set to AVAILABLE by default when instantiating a LineNumber object with this constructor
	}
	
	public OrdrTnLineNumber(
			String npa,
			String nxx,
			String lineNumber,
			String releaseDate,
			OrdrTnReservationType numberReservationType) {
		super(npa, nxx);
		this.lineNumber = lineNumber;
		this.releaseDate = releaseDate;
		this.numberReservationType = numberReservationType; 
		// Assume the status is set to AVAILABLE by default when instantiating a LineNumber object with this constructor
	}

	public OrdrTnLineNumber(
			String npa,
			String nxx,
			String lineNumber,
			OrdrTnReservationType numberReservationType,
			OrdrTnLineNumberStatus status) {
		this(npa, nxx, lineNumber, numberReservationType);
		this.status = status;
	}

	public OrdrTnLineNumber(
			String npa,
			String nxx,
			String lineNumber,
			OrdrTnReservationType numberReservationType,
			OrdrTnLineNumberStatus status,
			OrdrTnAssignedProductType assignedProductType) {
		this(npa, nxx, lineNumber, numberReservationType, status);
		this.assignedProductType = assignedProductType; 
	}

	public OrdrTnLineNumber(
			String tenDigitsTn,
			OrdrTnReservationType numberReservationType,
			OrdrTnLineNumberStatus status) {
		this.npa = tenDigitsTn.substring(0, 3);
		this.nxx = tenDigitsTn.substring(3, 6);
		this.lineNumber = tenDigitsTn.substring(6);
		this.numberReservationType = numberReservationType; 
		this.status = status;
	}
	
	public OrdrTnLineNumber(
			String tenDigitsTn,
			OrdrTnReservationType numberReservationType,
			OrdrTnLineNumberStatus status,
			OrdrTnAssignedProductType assignedProductType) {
		this(tenDigitsTn, numberReservationType, status);
		this.assignedProductType = assignedProductType; 
	}
	
    public OrdrTnLineNumber(
    		String tenDigitsTn,
            String servingCoid,
            String switchNumber,
            boolean nativeNpaNxxInd,
			OrdrTnReservationType numberReservationType,
			OrdrTnLineNumberStatus status,
			OrdrTnAssignedProductType assignedProductType) {
    	super(tenDigitsTn.substring(0, 3), tenDigitsTn.substring(3, 6), servingCoid, switchNumber, false, nativeNpaNxxInd);
		this.lineNumber = tenDigitsTn.substring(6);
		this.numberReservationType = numberReservationType; 
		this.status = status;
		this.assignedProductType = assignedProductType; 
     }	
	
	/**
	 * @return the lineNumber
	 */
	public String getLineNumber() {
		return lineNumber;
	}

	/**
	 * @param lineNumber the lineNumber to set
	 */
	public void setLineNumber(String lineNumber) {
		this.lineNumber = lineNumber;
	}

	/**
	 * @return the status
	 */
	public OrdrTnLineNumberStatus getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(OrdrTnLineNumberStatus status) {
		this.status = status;
	}  
	
	/**
	 * @return the numberReservationType
	 */
	public OrdrTnReservationType getOrdrTnReservationType() {
		return numberReservationType;
	}

	/**
	 * @param numberReservationType the numberReservationType to set
	 */
	public void setOrdrTnReservationType(OrdrTnReservationType numberReservationType) {
		this.numberReservationType = numberReservationType;
	}	

	/**
	 * @return the assignedProductType
	 */
	public  OrdrTnAssignedProductType getOrdrTnAssignedProductType() {
		return assignedProductType;
	}

	/**
	 * @param assignedProductType the assignedProductType to set
	 */
	public void setOrdrTnAssignedProductType(OrdrTnAssignedProductType assignedProductType) {
		this.assignedProductType = assignedProductType;
	}
	
	/**
	 * @return the concatenation of: npa, nxx and lineNumber
	 */
	public String getTenDigitsTn() {
		return npa + nxx + lineNumber;
	}

	/**
	 * @return the releaseDate
	 */
	public String getReleaseDate() {
		return releaseDate;
	}

	/**
	 * @param releaseDate the releaseDate to set
	 */
	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	

	public void setSalesforceLineItemId(String salesforceLineItemId){
	  this.salesforceLineItemId=salesforceLineItemId;
	}
	public String getSalesforceLineItemId(){
	  return this.salesforceLineItemId;
	}
	
	
}