//Generated by wsdl2apex

public class rpm_EnterpriseCommonTypes_v5 {
    public class MultilingualCodeDescriptiontList {
        public String code;
        public rpm_EnterpriseCommonTypes_v5.Description[] description;
        private String[] code_type_info = new String[]{'code','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5',null,'1','1','false'};
        private String[] description_type_info = new String[]{'description','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5',null,'0','10','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5','true','false'};
        private String[] field_order_type_info = new String[]{'code','description'};
    }
    public class AuditInfo {
        public String userId;
        public String userTypeCode;
        public String salesRepresentativeId;
        public String channelOrganizationId;
        public String outletId;
        public String originatorApplicationId;
        public String correlationId;
        public DateTime timestamp;
        private String[] userId_type_info = new String[]{'userId','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5',null,'0','1','false'};
        private String[] userTypeCode_type_info = new String[]{'userTypeCode','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5',null,'0','1','false'};
        private String[] salesRepresentativeId_type_info = new String[]{'salesRepresentativeId','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5',null,'0','1','false'};
        private String[] channelOrganizationId_type_info = new String[]{'channelOrganizationId','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5',null,'0','1','false'};
        private String[] outletId_type_info = new String[]{'outletId','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5',null,'0','1','false'};
        private String[] originatorApplicationId_type_info = new String[]{'originatorApplicationId','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5',null,'0','1','false'};
        private String[] correlationId_type_info = new String[]{'correlationId','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5',null,'0','1','false'};
        private String[] timestamp_type_info = new String[]{'timestamp','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5','true','false'};
        private String[] field_order_type_info = new String[]{'userId','userTypeCode','salesRepresentativeId','channelOrganizationId','outletId','originatorApplicationId','correlationId','timestamp'};
    }
    public class brandType {
        public String brandId;
        public rpm_EnterpriseCommonTypes_v5.CodeDescText brandNameDecode;
        private String[] brandId_type_info = new String[]{'brandId','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5',null,'1','1','false'};
        private String[] brandNameDecode_type_info = new String[]{'brandNameDecode','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5','true','false'};
        private String[] field_order_type_info = new String[]{'brandId','brandNameDecode'};
    }
    public class Description {
        public String locale;
        public String descriptionText;
        private String[] locale_type_info = new String[]{'locale','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5',null,'1','1','false'};
        private String[] descriptionText_type_info = new String[]{'descriptionText','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5','true','false'};
        private String[] field_order_type_info = new String[]{'locale','descriptionText'};
    }
    public class Message {
        public String locale;
        public String message;
        private String[] locale_type_info = new String[]{'locale','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5',null,'1','1','false'};
        private String[] message_type_info = new String[]{'message','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5','true','false'};
        private String[] field_order_type_info = new String[]{'locale','message'};
    }
    public class MultilingualDescriptiontList {
        public rpm_EnterpriseCommonTypes_v5.Description[] description;
        private String[] description_type_info = new String[]{'description','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5',null,'1','10','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5','true','false'};
        private String[] field_order_type_info = new String[]{'description'};
    }
    public class ResponseMessage {
        public DateTime dateTimeStamp;
        public String errorCode;
        public String messageType;
        public String transactionId;
        public rpm_EnterpriseCommonTypes_v5.Message messageList;
        public String contextData;
        private String[] dateTimeStamp_type_info = new String[]{'dateTimeStamp','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5',null,'1','1','false'};
        private String[] errorCode_type_info = new String[]{'errorCode','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5',null,'0','1','false'};
        private String[] messageType_type_info = new String[]{'messageType','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5',null,'0','1','false'};
        private String[] transactionId_type_info = new String[]{'transactionId','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5',null,'0','1','false'};
        private String[] messageList_type_info = new String[]{'messageList','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5',null,'1','1','false'};
        private String[] contextData_type_info = new String[]{'contextData','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5','true','false'};
        private String[] field_order_type_info = new String[]{'dateTimeStamp','errorCode','messageType','transactionId','messageList','contextData'};
    }
    public class MultilingualNameList {
        public rpm_EnterpriseCommonTypes_v5.Name[] name;
        private String[] name_type_info = new String[]{'name','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5',null,'1','10','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5','true','false'};
        private String[] field_order_type_info = new String[]{'name'};
    }
    public class CodeDescText {
        public String locale;
        public String codeDescText;
        private String[] locale_type_info = new String[]{'locale','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5',null,'1','1','false'};
        private String[] codeDescText_type_info = new String[]{'codeDescText','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5','true','false'};
        private String[] field_order_type_info = new String[]{'locale','codeDescText'};
    }
    public class MessageType {
        public rpm_EnterpriseCommonTypes_v5.MultilingualCodeDescTextList message;
        private String[] message_type_info = new String[]{'message','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5','true','false'};
        private String[] field_order_type_info = new String[]{'message'};
    }
    public class MultilingualCodeDescTextList {
        public rpm_EnterpriseCommonTypes_v5.CodeDescText[] codeDescText;
        private String[] codeDescText_type_info = new String[]{'codeDescText','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5',null,'1','10','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5','true','false'};
        private String[] field_order_type_info = new String[]{'codeDescText'};
    }
    public class Name {
        public String locale;
        public String name;
        private String[] locale_type_info = new String[]{'locale','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5',null,'1','1','false'};
        private String[] name_type_info = new String[]{'name','http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v5','true','false'};
        private String[] field_order_type_info = new String[]{'locale','name'};
    }
}