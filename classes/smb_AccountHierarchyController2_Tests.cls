@isTest
public class smb_AccountHierarchyController2_Tests {

    static testMethod void test_pageSize() {
        
        smb_Application_Settings__c setting = smb_Application_Settings__c.getInstance('NumAccountsToPreloadInHierarchy');
        
        if (setting == null || setting.value__c == null) {
            system.assert(smb_AccountHierarchyController2.pageSize == smb_AccountHierarchyController2.DEFAULT_PAGE_SIZE);
        }
        else {
            system.assert(smb_AccountHierarchyController2.pageSize == integer.valueOf(setting.value__c));
        }
        
    }
    
    static List<Account> readyAccountHierarchy() {
        Map<string, Id> recordTypes = new Map<string, Id>();
        Map<string,schema.RecordtypeInfo> recordTypeInfoMap=Account.sobjecttype.getDescribe().getRecordTypeInfosByName();
        
        for (Schema.RecordTypeInfo recordTypeInfo:recordTypeInfoMap.values()) {
        
            if ((recordTypeInfo.getName().equalsignorecase('CBUCID'))
            || (recordTypeInfo.getName().equalsignorecase('RCID'))
            || (recordTypeInfo.getName().equalsignorecase('CAN'))
            || (recordTypeInfo.getName().equalsignorecase('BAN'))) //added for ICCD changes
           {
                                        
            recordTypes.put(recordTypeInfo.getName(), recordTypeInfo.getRecordTypeId());                               
            }
        }                                      
        List<Account> results = new List<Account>();
        
        Account parent = new Account (
            Name = 'CBUCID',
            RecordTypeId = recordTypes.get('CBUCID'),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            CBUCID__c = '123456'
        );
        
        insert parent;
        
        results.add(parent);
        
        Account child = new Account (
            Name = 'RCID',
            parentId = parent.Id,
            RecordTypeId = recordTypes.get('RCID'),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            RCID__c = '987654'
        );
        
        insert child;
        
        results.add(child);
        
        Account grandChild = new Account (
            Name = 'CAN',
            parentId = child.Id,
            RecordTypeId = recordTypes.get('CAN'),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            CAN__c = '5577993311',
            Billing_System_ID__c='101'
        );
        insert grandChild;
        results.add(grandChild);
        // ICCD code changes start
        Account grandChildOrgCan = new Account (
            Name = 'Org CAN',
            parentId = child.Id,
            RecordTypeId = recordTypes.get('CAN'),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            CAN__c = '5577993311',
            Billing_System_ID__c='101',
            BCAN_ORG__c = 'TestBCAN'
        );
        
        Account grandChildCan = new Account (
            Name = 'CAN2',
            parentId = grandChildOrgCan.Id,
            RecordTypeId = recordTypes.get('CAN'),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            CAN__c = '5577993311',
            Billing_System_ID__c='101',
            BAN_CAN__c = 'BCAN111',
            BCAN__c = 'BCAN112' 
        );
        
        Account grandChildBCan = new Account (
            Name = 'BCAN',
            parentId = child.Id,
            RecordTypeId = recordTypes.get('CAN'),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            CAN__c = '5577993311',
            Billing_System_ID__c='101',
            BAN_CAN__c = 'BCAN113',
            BCAN__c = 'BCAN113'
        );
        
        Account grandBANChild = new Account (
            Name = 'BAN',
            parentId = child.Id,
            RecordTypeId = recordTypes.get('BAN'),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            CAN__c = '5577993311',
            Billing_System_ID__c='103'
        );
        
        list<Account> insertChildList = new list<Account>();
        insertChildList.add(grandChildOrgCan);
        insertChildList.add(grandChildCan);
        insertChildList.add(grandChildBCan);
        insertChildList.add(grandBANChild);
        
        insert insertChildList;

        
        results.add(grandChildOrgCan);
        results.add(grandChildCan);
        results.add(grandChildBCan);
        results.add(grandBANChild);
        //ICCD code changes end

        Asset WTN = new Asset (
            Type__c = 'WTN',
            Name = '4035551212',
            Phone__c = '4035551212',
            AccountId = grandChild.Id
        );
        
        insert WTN;
        
        return results;
        
    }
    
    static testMethod void test_readyAccountHierarchy() {
        readyAccountHierarchy();
        
        List<Account> accounts = [SELECT Id 
                                  FROM Account];
                                  
        System.assert(Accounts.size() == 7);// changed size for ICCD
    }
    
    static testMethod void test_getRootNode_CBUCID() {
        
        List<Account> testAccounts = readyAccountHierarchy();
        
        Id CBUCIDAccountId = testAccounts.get(0).Id;
        Id RcidAccountId = testAccounts.get(1).Id;
        
        smb_AccountHierarchyController2.rootNodeResults result = smb_AccountHierarchyController2.getRootNode(CBUCIDAccountId,true);
        
        System.assert(result.rootNode.recordId == CBUCIDAccountId);
        
        System.assert(result.accountsIdsInPathToRoot.size() == 1);
        
        System.assert(result.accountsIdsInPathToRoot.contains(CBUCIDAccountId));
        
        System.assert(result.rootNode.childrenLoaded);
        
        System.assert(result.rootNode.children.size() == 1);
        
        System.assert(result.rootNode.children.get(0).recordId == RcidAccountId);
    }
    
    static testMethod void test_getRootNode_RCID() {
        
        List<Account> testAccounts = readyAccountHierarchy();
        
        Id CBUCIDAccountId = testAccounts.get(0).Id;
        Id RcidAccountId = testAccounts.get(1).Id;
        
        smb_AccountHierarchyController2.rootNodeResults result = smb_AccountHierarchyController2.getRootNode(RcidAccountId,true);
        
        System.assert(result.rootNode.recordId == CBUCIDAccountId);
        
        System.assert(result.accountsIdsInPathToRoot.size() == 2);
        
        System.assert(result.accountsIdsInPathToRoot.contains(CBUCIDAccountId));
        System.assert(result.accountsIdsInPathToRoot.contains(RcidAccountId));
        
        System.assert(!result.rootNode.childrenLoaded);
        
        System.assert(result.rootNode.children.size() == 1);
        
        System.assert(result.rootNode.children.get(0).recordId == RcidAccountId);
        
        System.assert(result.rootNode.children.get(0).children == null);
    }
    
    static testMethod void test_getRootNode_CAN() {
        
        List<Account> testAccounts = readyAccountHierarchy();
        
        Id CBUCIDAccountId = testAccounts.get(0).Id;
        Id RcidAccountId = testAccounts.get(1).Id;
        Id CanAccountId = testAccounts.get(2).Id;
        
        smb_AccountHierarchyController2.rootNodeResults result = smb_AccountHierarchyController2.getRootNode(CanAccountId,true);
        
        System.assert(result.rootNode.recordId == CBUCIDAccountId);
        
        System.assert(result.accountsIdsInPathToRoot.size() == 3);
        
        System.assert(result.accountsIdsInPathToRoot.contains(CBUCIDAccountId));
        System.assert(result.accountsIdsInPathToRoot.contains(RcidAccountId));
        System.assert(result.accountsIdsInPathToRoot.contains(CanAccountId));
        
        System.assert(!result.rootNode.childrenLoaded);
        
        System.assert(result.rootNode.children.size() == 1);
        
        System.assert(result.rootNode.children.get(0).recordId == RcidAccountId);
        
        System.assert(result.rootNode.children.get(0).children.size() == 4);// changed size for ICCD
        
        System.assert(result.rootNode.children.get(0).children.get(0).recordId == CanAccountId);
    }
    
    static testMethod void test_getChildNodes_RCID() {
        
        List<Account> testAccounts = readyAccountHierarchy();
        
        Id CBUCIDAccountId = testAccounts.get(0).Id;
        Id RcidAccountId = testAccounts.get(1).Id;
        Id CanAccountId = testAccounts.get(2).Id;
        
        List<smb_AccountHierarchyController2.AccountNode> results = smb_AccountHierarchyController2.getChildNodes(RcidAccountId, 0, new List<Id> {},true);
        
        System.assert(results.size() == 4);// changed size for ICCD
        
        System.assert(results.get(0).recordId != null);// ICCD code change
        
        System.assert(!results.get(0).childrenLoaded);
        
        System.assert(results.get(0).children == null);//ICCD code change
        
    }
    
    static testMethod void test_getChildAssetNodes_CAN() {
        
        List<Account> testAccounts = readyAccountHierarchy();
        
        Id CBUCIDAccountId = testAccounts.get(0).Id;
        Id RcidAccountId = testAccounts.get(1).Id;
        Id CanAccountId = testAccounts.get(2).Id;
        
        List<smb_AccountHierarchyController2.AccountNode> results = smb_AccountHierarchyController2.getChildAssetNodes(CanAccountId, 0,true);
        
        System.assert(results.size() == 1);
        
        System.assert(results.get(0).objectType == 'Asset');
    
    }
    //ICCD code changes start
    static testMethod void test_ICCDChanges() {
        
        List<Account> testAccounts = readyAccountHierarchy();
        
        Id CBUCIDAccountId = testAccounts.get(0).Id;
        Id RcidAccountId = testAccounts.get(1).Id;
        Id grandChildId = testAccounts.get(2).Id;
        Id grandChildOrgCan = testAccounts.get(3).Id;
        Id grandChildCan = testAccounts.get(4).Id;
        Id grandChildBCan = testAccounts.get(5).Id;
        Id grandBANChild = testAccounts.get(6).Id;
        smb_AccountHierarchyController2.rootNodeResults result = smb_AccountHierarchyController2.getRootNode(RcidAccountId,true);
        smb_AccountHierarchyController2.rootNodeResults result2 = smb_AccountHierarchyController2.getRootNode(grandChildCan,true);
        
    }
    //ICCD code changes end
}