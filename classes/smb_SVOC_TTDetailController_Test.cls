/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 * Name - smb_SVOC_TTDetailController_Test
 * 1. Created By - Sandip - 07 March 2016
 * 2. Modified By 
 */

@isTest(SeeAllData=false)
public class smb_SVOC_TTDetailController_Test{
    
    public static testmethod void testSRMWebService() { 
        Test.setMock(WebServiceMock.class, new smb_Ticket_Aggregator_Test.AsyncSoapServiceMockImpl());
        Account acc = smb_Ticket_Aggregator_Test.createAccount();
             
        Test.startTest();
        PageReference pageRef = Page.smb_SVOC_TTDetail;
        smb_Ticket_Aggregator_Test.putParameter(pageRef, acc.Id, 'TERM-20160309-0002', TicketVO.SRC_SRM, '3','3');
        Test.setCurrentPage(pageRef);
        
        smb_SVOC_TTDetailController detailController = new smb_SVOC_TTDetailController();
        detailController.RCID='0001058404';
        detailController.init();
        Continuation conti = detailController.getTTDetail(); 
        Map<String, HttpRequest> requests = conti.getRequests();
        //19 May 2016 - Sandip - Removed commentted line for deploying SRM changes
        system.assert(requests.size() == 1); 
        system.debug('@@@@ requests' +requests); 
        HttpResponse response = new HttpResponse();
        String mockBody = smb_Ticket_Aggregator_Test.srmMockBody;
        response.setBody(mockBody); 
        String requestLabel = requests.keyset().iterator().next(); 
        Test.setContinuationResponse(requestLabel, response);
        Object result = Test.invokeContinuationMethod(detailController, conti);
        Test.stopTest(); 
    }
    public static testmethod void testLavaStorm() { 
        Account acc = smb_Ticket_Aggregator_Test.createAccount();
        LavaStorm_Trouble_Ticket__c lsObj = new LavaStorm_Trouble_Ticket__c();
        lsObj.Trouble_Id__c = 'test_001';
        lsObj.BAN__c = '0001058404_zzz';
        lsObj.Customer_Id__c = '31211_zzz'; 
        lsObj.Account__c = acc.Id;
        lsObj.Problem_Description_Details__c = 'test';
        lsObj.Status__c = 'Open';
        lsObj.Closed_date__c = system.now();
        lsObj.Time_of_Resolution__c = system.now();
        insert lsObj;
        
        LavaStorm_Trouble_Ticket__c lsObj1 = new LavaStorm_Trouble_Ticket__c();
        lsObj1.Trouble_Id__c = lsObj.Trouble_Id__c + '-' + 'test_001';
        lsObj1.BAN__c = '0001058404_zzz';
        lsObj1.Customer_Id__c = '31211_zzz'; 
        lsObj1.Account__c = acc.Id;
        insert lsObj1;
                
        Test.startTest();
        PageReference pageRef = Page.smb_SVOC_TTDetail;
        smb_Ticket_Aggregator_Test.putParameter(pageRef, acc.Id, lsObj.Trouble_Id__c, TicketVO.SRC_LAVASTORM, '3','3');
        Test.setCurrentPage(pageRef);     
        smb_SVOC_TTDetailController objj = new smb_SVOC_TTDetailController();
        objj.init();
        objj.getLavaStormTicketDetails();
        Test.stopTest(); 
    }
    public static testmethod void testTTODSWebService() { 
        Test.setMock(WebServiceMock.class, new smb_Ticket_Aggregator_Test.AsyncSoapServiceMockImpl());
        Account acc = smb_Ticket_Aggregator_Test.createAccount();
             
        Test.startTest();
        PageReference pageRef = Page.smb_SVOC_TTDetail;
        smb_Ticket_Aggregator_Test.putParameter(pageRef, acc.Id, '1000119421', TicketVO.SRC_TTODS, '3', '3');
        Test.setCurrentPage(pageRef);
        
        smb_SVOC_TTDetailController detailController = new smb_SVOC_TTDetailController();
        detailController.RCID='0001058404';
        detailController.init();
        Continuation conti = detailController.getTTDetail(); 
        Map<String, HttpRequest> requests = conti.getRequests();
        system.assert(requests.size() == 1);
        system.debug('@@@@ requests' +requests); 
        HttpResponse response = new HttpResponse();
        String mockBody = smb_Ticket_Aggregator_Test.ttodsMockBody;
        response.setBody(mockBody); 
        String requestLabel = requests.keyset().iterator().next(); 
        Test.setContinuationResponse(requestLabel, response);
        Object result = Test.invokeContinuationMethod(detailController, conti); 

        Test.stopTest(); 
    }
    
    public static testmethod void testMethodForException () { 
        Test.setMock(WebServiceMock.class, new smb_Ticket_Aggregator_Test.AsyncSoapServiceMockImpl());
                     
        Test.startTest();
        PageReference pageRef = Page.smb_SVOC_TTDetail;
        pageRef.getParameters().put('id', '12345');
        pageRef.getParameters().put('ticketID', 'TERM-20160309-0002');
        pageRef.getParameters().put('source', 'SRM');
        Test.setCurrentPage(pageRef);
        
        smb_SVOC_TTDetailController detailController = new smb_SVOC_TTDetailController();
        Continuation conti = detailController.getTTDetail(); 
        Map<String, HttpRequest> requests = conti.getRequests();
        system.debug('@@@@ requests' +requests); 
        HttpResponse response = new HttpResponse();
        String mockBody = 'test mock up';
        response.setBody(mockBody); 
        try{
            String requestLabel = requests.keyset().iterator().next(); 
            Test.setContinuationResponse(requestLabel, response);
            Object result = Test.invokeContinuationMethod(detailController, conti); 
        }catch(Exception ex){
            System.debug(ex);
        }
        Test.stopTest(); 
    }
}