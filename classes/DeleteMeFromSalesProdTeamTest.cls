@isTest
private class DeleteMeFromSalesProdTeamTest {
  
  @isTest(SeeAllData=true)
  private static void testInit() {   
    User runner = getRunnerUser();
    
    UserRole role = new UserRole(name = 'Test Manager');
    
    system.runas(runner) {
    	insert role;
    }
    
    User u = new User(
      username = Datetime.now().getTime() + '@TRACTIONSM.COM',
      email = 'test@example.com',
      title = 'test',
      lastname = 'test',
      alias = 'test',
      TimezoneSIDKey = 'America/Los_Angeles',
      LocaleSIDKey = 'en_US',
      EmailEncodingKey = 'UTF-8',
      ProfileId = runner.ProfileId,
      LanguageLocaleKey = 'en_US',
      UserRoleId = role.id
    );
    User u2 = u.clone(false,false,false,false);
    u2.username = Datetime.now().getTime() + '2@TRACTIONSM.COM';
    
    
    system.runas(runner) {
      insert new User[] { u, u2 };
    }

    system.runas(u) {
	    Opportunity o = new Opportunity(name = 'test', closedate = Date.today(), stagename = 'test');
	    
	    Product2 p = new Product2(name = 'Test Product', isActive = true);
	    
	    insert new List<SObject>{o, p};
	        
	    Id PRICEBOOKID = [SELECT id FROM Pricebook2 WHERE isStandard = true].id;
	        
	    PricebookEntry pbe = new PricebookEntry(
	    	product2id = p.id,
	    	unitprice = 1,
	    	usestandardprice = false,
	    	isactive = true,
	    	pricebook2id = PRICEBOOKID
	    );
	    
	    Opp_Product_Item__c opi = new Opp_Product_Item__c(Opportunity__c = o.id, Product__c = p.id);
	    
	    insert new List<SObject>{pbe, opi};
	                
	    OpportunityLineItem oli = new OpportunityLineItem(
	    	OpportunityId = o.id,
	    	PricebookEntryId = pbe.id,
	    	quantity = 1,
	    	totalprice = 1
	    );
	    
	    Product_Sales_Team__c pst = new Product_Sales_Team__c(Product__c = opi.id, Member__c = u.id);
	    Product_Sales_Team__c pst2 = new Product_Sales_Team__c(Product__c = opi.id, Member__c = u2.id);
	    
	    insert new List<SObject>{oli, pst, pst2};
        
	    OpportunityTeamMember[] members = new List<OpportunityTeamMember>{
	        new OpportunityTeamMember(OpportunityId = o.id, UserId = u.id),
	        new OpportunityTeamMember(OpportunityId = o.id, UserId = u2.id)
	    };
	    insert members;
        
      ApexPages.currentPage().getParameters().put('oppId', o.id);
        

      DeleteMeFromSalesProdTeam del = new DeleteMeFromSalesProdTeam();
      del.init();
        
      system.assert(del.flag);
    }
  }
  
  private static testMethod void testInitNonManagerUser() {    
    User runner = getRunnerUser();
    
    UserRole role = new UserRole(name = 'Test Manager');
    
    system.runas(runner) {
    	insert role;
    }
        
    User u = new User(
      username = Datetime.now().getTime() + '@TRACTIONSM.COM',
      email = 'test@example.com',
      title = 'test',
      lastname = 'test',
      alias = 'test',
      TimezoneSIDKey = 'America/Los_Angeles',
      LocaleSIDKey = 'en_US',
      EmailEncodingKey = 'UTF-8',
      ProfileId = runner.ProfileId,
      LanguageLocaleKey = 'en_US',
      UserRoleId = role.id
    );
    User u2 = u.clone(false,false,false,false);
    u2.username = Datetime.now().getTime() + '2@TRACTIONSM.COM';
    
    system.runas(runner) {
    	insert new User[] { u, u2 };
    }

    Opportunity o = new Opportunity(name = 'test', closedate = Date.today(), stagename = 'test');
    insert o;
    
    OpportunityTeamMember[] members = new List<OpportunityTeamMember>{
    	new OpportunityTeamMember(OpportunityId = o.id, UserId = u.id),
    	new OpportunityTeamMember(OpportunityId = o.id, UserId = UserInfo.getUserId())
    };
    insert members;
    
    ApexPages.currentPage().getParameters().put('oppId', o.id);
        
    DeleteMeFromSalesProdTeam del = new DeleteMeFromSalesProdTeam();
    del.init();
    
    system.assert(del.flag);
  }
  
  
  private static testMethod void testInitNoTeam() {   
    User runner = getRunnerUser();
    
    UserRole role = new UserRole(name = 'Test Manager');
    
    system.runas(runner) {
    	insert role;
    }
    
    User u = new User(
      username = Datetime.now().getTime() + '@TRACTIONSM.COM',
      email = 'test@example.com',
      title = 'test',
      lastname = 'test',
      alias = 'test',
      TimezoneSIDKey = 'America/Los_Angeles',
      LocaleSIDKey = 'en_US',
      EmailEncodingKey = 'UTF-8',
      ProfileId = runner.ProfileId,
      LanguageLocaleKey = 'en_US',
      UserRoleId = role.id
    );
    
    system.runas(runner) {
    	insert u;
    }
    
    Opportunity o = new Opportunity(name = 'test', closedate = Date.today(), stagename = 'test');
    insert o;
    
    ApexPages.currentPage().getParameters().put('oppId', o.id);
      
    system.runas(u) {
    	DeleteMeFromSalesProdTeam del = new DeleteMeFromSalesProdTeam();
      del.init();
    }
  }
  
  
  private static testMethod void testInitNoOppId() {
    DeleteMeFromSalesProdTeam del = new DeleteMeFromSalesProdTeam();
    
    del.init();
    
    system.assertEquals(false,del.flag);
  }
  
  
  @isTest(SeeAllData=true)
  private static void testDeleteOpportunityTeamMember() {    
    User runner = getRunnerUser();
    
    UserRole role = new UserRole(name = 'Test Manager');
    
    system.runas(runner) {
    	insert role;
    }
    
   	User u = new User(
      username = Datetime.now().getTime() + '@TRACTIONSM.COM',
      email = 'test@example.com',
      title = 'test',
      lastname = 'test',
      alias = 'test',
      TimezoneSIDKey = 'America/Los_Angeles',
      LocaleSIDKey = 'en_US',
      EmailEncodingKey = 'UTF-8',
      ProfileId = runner.ProfileId,
      LanguageLocaleKey = 'en_US',
      UserRoleId = role.id
    );
        
        
    system.runas(runner) {
    	insert u;
    }
        
    system.runas(u) {
	    Opportunity o = new Opportunity(name = 'test', closedate = Date.today(), stagename = 'test');
	    
	    Product2 p = new Product2(name = 'Test Product', isActive = true);
	    
	    insert new List<SObject>{o, p};
	        
	    Id PRICEBOOKID = [SELECT id FROM Pricebook2 WHERE isStandard = true].id;
            
	    PricebookEntry pbe = new PricebookEntry(product2id = p.id, unitprice = 1, usestandardprice = false, isactive = true, pricebook2id = PRICEBOOKID);
	    insert pbe;
	                
	    OpportunityLineItem oli = new OpportunityLineItem(OpportunityId = o.id, PricebookEntryId = pbe.id, quantity = 1, totalprice = 1);
	    
	    Opp_Product_Item__c opi = new Opp_Product_Item__c(Opportunity__c = o.id, Product__c = p.id);
	    
	    OpportunityTeamMember member = new OpportunityTeamMember(OpportunityId = o.id, UserId = u.id);
	    
	    insert new List<Sobject>{oli, opi, member};
        
      Product_Sales_Team__c pst = new Product_Sales_Team__c(Product__c = opi.id, Member__c = u.id);
      insert pst;

      ApexPages.currentPage().getParameters().put('oppId', o.id);

      DeleteMeFromSalesProdTeam del = new DeleteMeFromSalesProdTeam();
        
      del.init();
        
      del.opportunityTeamWrapperList[0].checked = true;
        
      del.deleteOpportunityTeamMember();
        
      //system.assertEquals(false,del.flag);
    }
  }
  
  private static testMethod void testFindUsersInRoleHierarchy() {
    Map<Id, User> userObjMap = new Map<Id, User>();
    Map<String, User> tempUserInRoleHierarchyMap = new Map<String, User>();
    
    DeleteMeFromSalesProdTeam del = new DeleteMeFromSalesProdTeam();
    
    Map<Id, User> results = del.findUsersInRoleHierarchy(userObjMap, tempUserInRoleHierarchyMap);
  }
  
  private static testMethod void testFindChildHierarchyByMap() {
  	UserRole role1 = new UserRole(name = 'test1');
    UserRole role2 = new UserRole(name = 'test2');
		UserRole role3 = new UserRole(name = 'test3');
		
    insert new UserRole[] { role1, role2, role3 };
    
    Map<Id, Set<UserRole>> roleHierarchyChildMap = new Map<Id, Set<UserRole>>{
    	role1.id => new Set<UserRole>{ role2, role3 },
    	role2.id => new Set<UserRole>{ role3 }
    };
    
    DeleteMeFromSalesProdTeam del = new DeleteMeFromSalesProdTeam();
    
    del.roleIds = new Set<Id>(); // simulating call to init
    
    del.findChildHierarchyByMap(role1.id, roleHierarchyChildMap);
    
    //system.assert(del.roleIds.contains(''));
  }
  
  private static testMethod void testCancel() {
    DeleteMeFromSalesProdTeam del = new DeleteMeFromSalesProdTeam();
    
    PageReference ref = del.cancel();
    
    system.assertEquals('/006/o', ref.getUrl());
  }
  
  @isTest
  private static User getRunnerUser() {
  	Id PROFILEID = [SELECT id FROM Profile WHERE name='Standard User' LIMIT 1].id;
    
    User runner = new User(
      username = Datetime.now().getTime() + 'runner@TRACTIONSM.COM',
      email = 'test@example.com',
      title = 'test',
      lastname = 'test',
      alias = 'test',
      TimezoneSIDKey = 'America/Los_Angeles',
      LocaleSIDKey = 'en_US',
      EmailEncodingKey = 'UTF-8',
      ProfileId = PROFILEID,
      LanguageLocaleKey = 'en_US'
    );
  	
  	return runner;
  }
    
}