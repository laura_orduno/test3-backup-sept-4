/*
###########################################################################
# File..................: smb_QuoteToPdfStatusController
# Version...............: 26
# Created by............: Puneet Khosla
# Created Date..........: 28-Dec-2013
# Last Modified by......: 
# Last Modified Date....: 
# Description...........: Intermediate Status Page for Quote Sending
###########################################################################
*/

public class smb_QuoteToPdfStatusController 
{
    //-------------------- Properties --------------------
    public Opportunity opp{get;set;}
    public String PageTitle {get;set;}
    public String opportunityDetails {get;set;}
    public string quoteAttached {get;set;}
    public string emailMsg {get;set;}
    public Boolean RenderSuccess {get;set;}
    public boolean sourceConsole {get;set;}
    public string pTabId {get;set;}
	
	//-------------------- Variables --------------------
	public ApexPages.StandardController controller;
	
	//----------------Constructor-----------------------
    public smb_QuoteToPdfStatusController(ApexPages.StandardController c) 
    {
        RenderSuccess=false;
        opp = (Opportunity) c.getRecord();
        controller = c;
        string src = ApexPages.currentPage().getParameters().get('src');
        // Check if the request came from Console
        if(src !=null && src.trim() == 'C')
        {
        	sourceConsole = true;
        	pTabId = ApexPages.currentPage().getParameters().get('PTabId');
        }
        else
            sourceConsole = false;
    }
    
    /*
    * @author       : Puneet Khosla
    * @method Name  : GeneratePDF()
    * @description  : This method is used to call various stages of pdf generation
    * @return       : PageReference
    * @param        : None          
    */
    
    public PageReference GeneratePDF()
    {
        smb_QuoteToPdfController obj = new smb_QuoteToPdfController(opp.Id);
        if(String.IsBlank(obj.errorMsg))
        {
            opportunityDetails = 'Order Details Retrieved Successfully';
            obj.attachQuote();
        }
        else
        {
            opportunityDetails = obj.errorMsg;
        }
        if(String.IsBlank(obj.errorMsg))
        {
                quoteAttached = 'Quote Pdf generated and attached in the Order Record';       
            obj.sendEmail();
        }
        else
        {
            quoteAttached =  obj.errorMsg;
        }
        if(String.IsBlank(obj.errorMsg))
        {
                emailMsg = 'Email sent';
                RenderSuccess = true;
        }    
                //return controller.view();
        else
            emailMsg = obj.errorMsg;   
        
        return null;
    }
    
    /*
    * @author       : Puneet Khosla
    * @method Name  : ReturnOrder()
    * @description  : This method is to return back to Opportunity
    * @return       : PageReference
    * @param        : None          
    */
    public PageReference ReturnOrder()
    {
        return controller.view();
    }
}