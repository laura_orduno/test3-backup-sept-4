global class ENTPManualTicketHelper {
    public static boolean firstRun = true;
    public static id TicketevntID_Old;
   
    
    public static void ENTPAddToManual(id TicketID) {
        
        system.debug('FirstRun ENTPTicketHelper='+firstRun);
        //for(Ticket_Event__c s : trigger.new){
        try {    
            // list<Ticket_Event__c> chk = [select Case_Number__c, Case_Id__c, Lynx_Ticket_Number__c, Id from Ticket_Event__c where Status__c = 'Complete' order by LastModifiedDate desc limit 1];
            //Ticket_Event__c[] chk = [select Case_Number__c, Case_Id__c, Remedy_PIN__c, Lynx_Ticket_Number__c, Id, Case_id__r.Ticketing_System__c from Ticket_Event__c where Status__c = 'Complete' order by LastModifiedDate desc limit 1];
System.debug('ENTPManualTicketHelper.ENTPAddToManual: ' + TicketID);        

            Ticket_Event__c[] all = [select Case_Number__c, Case_Id__c, Remedy_PIN__c, Lynx_Ticket_Number__c, Id, Case_id__r.Ticketing_System__c from Ticket_Event__c];
System.debug('ENTPManualTicketHelper.ENTPAddToManual,ALL:' + all);
            
            Ticket_Event__c[] chk = [select Case_Number__c, Case_Id__c, Remedy_PIN__c, Lynx_Ticket_Number__c, Id, Case_id__r.Ticketing_System__c from Ticket_Event__c where id=:TicketID];
System.debug('ENTPManualTicketHelper.ENTPAddToManual,CHK:' + chk);
            
            if(chk.size()>0){
                system.debug('select size-'+chk.size());
                ID CaseID = chk[0].Case_Id__c;
                String casenumber = chk[0].Case_Number__c;
                string LynxTicketNum = chk[0].Lynx_Ticket_Number__c;
                ID TicketEventID = chk[0].Id;
                
                
                String TicketingSystem = chk[0].case_id__r.Ticketing_System__c;
                String RemedyPIN = chk[0].Remedy_PIN__c;
                System.debug('TicketEvent Trigger TicketEventID:' + TicketEventID);
                if(LynxTicketNum!=null || LynxTicketNum!=''){
                    
                    //Rob - 01/19/2016 : Make the call to VITILcare_LynxCreate to send info to Lynx Web Service
                    //LynxTicketSearch.sendRequest smbCallOut2 = new LynxTicketSearch.sendRequest();
                    //  smbCallOut2.SRequest('1000145233', s.casenumber, s.Id);
                    if(TicketingSystem == 'Lynx'){
                        if (firstRun) {
                            if(!Test.isRunningTest()){
                                firstRun = false; }
                            system.debug('FirstRun ENTPTicketHelper After Lynx='+firstRun);
                            LynxTicketSearch.sendRequest(LynxTicketNum, casenumber, CaseID, TicketEventID);
                            System.debug('CALLOUT Variables- LynxTicketNum: '+LynxTicketNum+' CaseNumber: ' + casenumber + ' CaseID: ' + CaseID + ' TicketEventID: ' +TicketEventID); 
                            
                        }
                        else {
                            System.debug('Already ran!');
                            return;
                        }
                        
                    }
                    if(TicketingSystem == 'WLN Remedy'){
                        if (firstRun) {
                            if(!Test.isRunningTest()){
                                firstRun = false; }
                            system.debug('FirstRun ENTPTicketHelper After Remedy='+firstRun);
                            RemedyViewTicket.sendRequest(RemedyPIN,LynxTicketNum, casenumber, CaseID, TicketEventID);
                            //sendRemRequest(RemedyPIN,LynxTicketNum, casenumber, CaseID, TicketEventID);
                            System.debug('CALLOUT Variables- RemedyViewTicket: '+LynxTicketNum+' PIN: ' + RemedyPIN );
                            
                        }
                        else {
                            System.debug('Already ran!');
                            return;
                        }
                        
                        
                    }
                }         
                
                TicketevntID_Old = TicketEventID;
                System.debug('TicketEvent Trigger OLDTicketEventID:' + TicketevntID_Old);
            }
        }
        catch(DmlException e) {
            System.debug('ENTPManualTicketComp->TicketEvent, exception: ' + e.getMessage());
        }
        
        
    } 
    
}