public without sharing class trac_TAS_Controller {
  
  // Wrapper used to display and edit decision criteria
  public class DecisionCriteriaWrapper {
    
    public TAS_Relationship_Questionare__c theObj {get; set;}
    public Boolean isEditable {get; set;}
    
    public List<TAS_Player__c> thePlayers {get; set;}
    public TAS_Player__c theNewPlayer {get; set;}
    public DecisionCriteriaWrapper() {
      theObj = new TAS_Relationship_Questionare__c();
      theNewPlayer = new TAS_Player__c();
      thePlayers = new List<TAS_Player__c>();
    }
  }
  
  // Id variables used by action methods
  public ID theID {get; set;}
  public ID theAccountID {get; set;}
  public ID theOppID {get; set;}
  public ID longRecordType {get; set;}
  public ID shortRecordType {get; set;}
  public ID theTASRecordTypeID {get; set;}
  public ID removeActivityID {get; set;}
  public ID removeMilestoneID {get; set;}
  public ID removeBusinessPartnerID {get; set;}
  public ID removeCompetitorID {get; set;}
  public ID removeDecisionCriteriaID {get; set;}
  public ID removeDecisionCriteriaPlayerID {get; set;}
  public ID removeTASContactID {get; set;}
  public ID removeRelationshipStrategyID {get; set;}
  public ID activityRecordTypeID {get; set;}
  public ID milestonesRecordTypeID {get; set;}
  public ID businessPartnersRecordTypeID {get; set;}
  public ID competitorsRecordTypeID {get; set;}
  public ID decisionCriteriaRecordTypeID {get; set;}
  public ID TASContactsRecordTypeID {get; set;}
  public ID relationshipStrategiesRecordTypeID {get; set;}
  public ID decisionCriteriaPlayerParent {get; set;}
  
  public String editableFieldId {get; set;} // the current editable field
  
  // Display texts for vf page
  public String theTASOwnerName {get; set;}
  public String debugMessage {get; set;}
  
  // Reusable Query strings
  public String queryLimit {get; set;}
  public String theTASQuery {get; set;}
  public String theOppsQuery {get; set;}
  public String theLabelsQuery {get; set;}
  public String theAccountQuery {get; set;}
  public String theActivitiesQuery {get; set;}
  public String theBusinessPartnerCompetitorQuery {get; set;}
  public String theRelationshipQuestionaireQuery {get; set;}
  public String theDecisionCriteriaQuery {get; set;}
  public String theNestedQuery {get; set;}
  public String theTASPlayerQuery {get; set;}
  
  // Boolean indicators for VF page
  public Boolean waitForPermissions {get; set;} // if true, grant permissions and reload page
  public Boolean tasError {get; set;}
  public Boolean isNew {get; set;}
  public Boolean isFrench {get; set;}
  public Boolean isLong {get; set;}
  public Boolean hasEditAccess {get; set;}
  public Boolean hasDeleteAccess {get; set;}
  public String genericError {get; set;}
  
  // Main object variables
  public TAS_Settings__c theSettings {get; set;} // TAS settings for the page
  public Target_Account_Selling__c theTAS {get; set;} // the main TAS object being viewed
  public User theTASOwner {get; set;} // the owner user for this TAS
  public Account theAccount {get; set;} // the Account the TAS is under
  public Opportunity theOpp {get; set;} // the Opportunity to link the TAS to, if landing here from opp page
  public List<Opportunity> theOpps {get; set;} // the full list of opps linked to this TAS
  public TAS_labels__c theLabels {get; set;} // labels to display on the page, english/french
  
  // Related list variables
  public List<Task> theActivities {get; set;}
  public List<Task> theMilestones {get; set;}
  public List<TAS_Business_Partners_Competitors__c> theBusinessPartners {get; set;}
  public List<TAS_Business_Partners_Competitors__c> theCompetitors {get; set;}
  public List<TAS_Relationship_Questionare__c> theDecisionCriteria {get; set;}
  public List<DecisionCriteriaWrapper> theDecisionCriteriaWrapped {get; set;}
  public List<TAS_Relationship_Questionare__c> theTASContacts {get; set;}
  public List<TAS_Relationship_Questionare__c> theRelationshipStrategies {get; set;}
  public List<TAS_Player__c> theDecisionCriteriaPlayers {get; set;}
  
  // Variables to store new related list object data
  public Task newActivity {get; set;}
  public Task newMilestone {get; set;}
  public TAS_Business_Partners_Competitors__c newBusinessPartner {get; set;}
  public TAS_Business_Partners_Competitors__c newCompetitor {get; set;}
  public TAS_Relationship_Questionare__c newDecisionCriteria {get; set;}
  public TAS_Player__c newDecisionCriteriaPlayer {get; set;}
  public TAS_Relationship_Questionare__c newTASContact {get; set;}
  public TAS_Relationship_Questionare__c newRelationshipStrategy {get; set;}
  
  // Extend exception class
  public class tasException extends Exception {}
  
  // Blank constructor
  public trac_TAS_Controller() {}
  
  
  // Initialization done by action method
  // This is necessary in order for the action method to be able to grant object
  // permissions to necessary users. Constructor methods cannot conduct DML operations.
  public void doInitialize() {
    
    try {
  	queryLimit = '';//Label.TAS_Display_Limit == '0' ? '' : '';
    
    // Object and Data Initialization methods
    
    initParameters(); // parameters from URL
    initIndicators(); // boolean/text status indicators
    initQueryStrings(); // reusable query strings
    
    initLanguage(); // language english/french
    initOpps(); // linked opps to this TAS
    initAccessLevel(); // User access level to the TAS and the Opp (read/edit)
    initCustomSettings(); // custom settings (deprecated)
    handleObjectAccess(); // grant/remove object access (reload page if necessary)
    
    initLabelsObject(); // page labels english/french
    initTASRecordTypes(); // record types for TAS
    initTaskRecordTypes(); // record types for TAS children
    
    initTAS(); // the TAS object
    initTASOwner(); // owner user of TAS object
    initAccount(); // related account
    
    // init TAS children
    initActivities();
    initMilestones();
    initCompetitors();
    initTASContacts();
    initBusinessPartners();
    initDecisionCriteria();
    initRelationshipStrategies();
    
    // Starts at 0
    // Action methods set it to 1 on success
    // Page resets to 0
    // If value does not change (0 -> 1), page assumes method did not run
    // and there was an unknown (salesforce level, field level, uncatchable) error
    genericError = '0';
    
    } catch (Exception e) {
        throw(e);
    	// catch init errors
      tasError = true;
      ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
    }
  }
  
  public void handleObjectAccess() {
  	if (!isNew) {
      
      // get the opps linked to this TAS 
      List<ID> theOppsIds = new List<ID>();
      for (Opportunity o : theOpps) theOppsIds.add(o.Id);
      
      
      List<UserRecordAccess> theOppsAccess = [SELECT HasEditAccess, RecordId 
                                              FROM UserRecordAccess
                                              WHERE UserId =: UserInfo.getUserId()
                                               AND RecordId IN :theOppsIds];
      
      // if the user has edit access to any of the opps
      Boolean oppEditAccess = false;
      for (UserRecordAccess ura : theOppsAccess) if (ura.HasEditAccess) oppEditAccess = true;
      if (!theOpps.isEmpty()) {
        
        // If the user has no TAS access but can edit the opp
        if (!hasEditAccess && oppEditAccess) {
          // then grant edit access to the TAS
          grantEditAccess(UserInfo.getUserId(), theID);
          waitForPermissions = true;
          return;
        }
        // If the user has no Opp access but can edit the TAS
        else if (hasEditAccess && !oppEditAccess) {
          // then remove edit access to the TAS
          removeEditAccess(UserInfo.getUserId(), theID);
          waitForPermissions = true;
          return;
        }
      }
    }
  }
  
  public void initParameters() {
  	
    theID = ApexPages.currentPage().getParameters().get('ID');
    theAccountID = ApexPages.currentPage().getParameters().get('ACCOUNTID');
    theOppId = ApexPages.currentPage().getParameters().get('OPPID');
    theTASRecordTypeID = ApexPages.currentPage().getParameters().get('RECORDTYPEID');
  }
  
  public void initQueryStrings() {
  	
    theTASQuery = trac_Utilities.createObjectQuery('Target_Account_Selling__c');
    // issue: trac_Utilities.createObjectQuery retrieves all fields for an object
    // this is causing "max view state" limit intermittently  
    theOppsQuery = trac_Utilities.createObjectQuery('Opportunity');
//    theOppsQuery = 'SELECT id, Name, AccountId, OwnerId, StageName, Probability, Total_Contract_Value__c, Total_Contract_Growth__c, Total_Contract_Renewal__c, Renewal_Units_Rollup__c, Quantity_Rollup__c, Quantity_Wtd_Rollup__c, Target_Account_Selling_Plan__c FROM Opportunity';          
    theLabelsQuery = trac_Utilities.createObjectQuery('TAS_labels__c');

    // issue: trac_Utilities.createObjectQuery retrieves all fields for an object
    // this is causing "max view state" limit intermittently  
    //theAccountQuery = trac_Utilities.createObjectQuery('Account');
	// use simpler SOQL for the requirement
    theAccountQuery = 'SELECT id, name FROM Account';          
      
    theTASPlayerQuery = trac_Utilities.createObjectQuery('TAS_Player__c');
    // issue: trac_Utilities.createObjectQuery retrieves all fields for an object
    // this is causing "max view state" limit intermittently  
    theActivitiesQuery = trac_Utilities.createObjectQuery('Task');
//    theActivitiesQuery = 'SELECT id, Subject, Resources_Required__c, OwnerId, ActivityDate FROM Task';          

    theRelationshipQuestionaireQuery = trac_Utilities.createObjectQuery('TAS_Relationship_Questionare__c');
    theRelationshipQuestionaireQuery = trac_Utilities.addField(theRelationshipQuestionaireQuery, 'Contact__r.Name');
    theBusinessPartnerCompetitorQuery = trac_Utilities.createObjectQuery('TAS_Business_Partners_Competitors__c');
    theNestedQuery = '(SELECT Id, Name, TAS_Relationship_Questionare__c FROM TAS_Players__r)';
    theDecisionCriteriaQuery = trac_Utilities.addField(theRelationshipQuestionaireQuery, theNestedQuery);
    
  }
  
  public void initIndicators() {
  	
    waitForPermissions = false;
    isNew = (theID == null);
    tasError = false;
  }
  
  public Boolean initCustomSettings() {
    if (theSettings == null) theSettings = TAS_Settings__c.getOrgDefaults();
    return (theSettings != null);
  }
  
  public void initLanguage() {
    isFrench = !(UserInfo.getLanguage() == 'en_US');
  }
  
  @future
  public static void grantEditAccess(ID theUserId, ID theTASID) {
    
	  Target_Account_Selling__Share newSharingRule = new Target_Account_Selling__Share();
	  newSharingRule.ParentId = theTASID;
	  newSharingRule.UserOrGroupId = theUserId;
	  newSharingRule.AccessLevel = 'Edit';
	  insert newSharingRule;
  }
  
  @future
  public static void removeEditAccess(ID theUserId, ID theTASID) {

	  List<Target_Account_Selling__Share> theSharingRules = [SELECT Id, ParentId, UserOrGroupId, AccessLevel
	                                                         FROM Target_Account_Selling__Share
	                                                         WHERE UserOrGroupId = :theUserId
	                                                          AND RowCause = 'Manual'
	                                                          AND ParentId = :theTASID];
	  delete theSharingRules;
  }
  
  public void initAccessLevel() {
    
    // Get user permissions for record
    ID recID = isNew ? theOppId : theID;
    
    List<UserRecordAccess> listUserRecordAccess = [SELECT RecordId, HasReadAccess, HasEditAccess, HasDeleteAccess 
                                                   FROM UserRecordAccess 
                                                   WHERE UserId=:UserInfo.getUserId() 
                                                    AND RecordId =: recID];
                                                  
    hasEditAccess = listUserRecordAccess.isEmpty() ? false : listUserRecordAccess[0].HasEditAccess;
    hasDeleteAccess = listUserRecordAccess.isEmpty() ? false : listUserRecordAccess[0].HasDeleteAccess;
  }
  
  public void initTASRecordTypes() {
    
    List<RecordType> temp = [SELECT Id, SObjectType, Name 
                             FROM RecordType 
                             WHERE SObjectType = 'Target_Account_Selling__c'];
    
    for (RecordType rt : temp) {
      if (rt.Name == theSettings.Long_Record_Type__c) longRecordType = rt.Id;
      if (rt.Name == theSettings.Short_Record_Type__c) shortRecordType = rt.Id;
    }
  }
  public void initTaskRecordTypes() {
    
    List<RecordType> temp = [SELECT Id, Name, SObjectType 
                             FROM RecordType 
                             WHERE SObjectType = 'Task'
                               OR SObjectType = 'TAS_Business_Partners_Competitors__c'
                               OR SObjectType = 'TAS_Relationship_Questionare__c'];
    
    for (RecordType rt : temp) {
      if (rt.SobjectType == 'Task') {
        if (rt.Name == 'TAS Actions') activityRecordTypeID = rt.Id;
        if (rt.Name == 'TAS Milestones in Buying Process') milestonesRecordTypeID = rt.Id;
      }
      if (rt.SobjectType == 'TAS_Business_Partners_Competitors__c') {
        if (rt.Name == 'Business Partner') businessPartnersRecordTypeID = rt.Id;
        if (rt.Name == 'Competitor') competitorsRecordTypeID = rt.Id;
      }
      if (rt.SobjectType == 'TAS_Relationship_Questionare__c') {
        if (rt.Name == 'Decision Criteria') decisionCriteriaRecordTypeID = rt.Id;
        if (rt.Name == 'Relationship Strategy') relationshipStrategiesRecordTypeID = rt.Id;
        if (rt.Name == 'TAS Contact') TASContactsRecordTypeID = rt.Id;
      }
    }
  }
  
  public void initLabelsObject() {
  	if (isFrench) theLabelsQuery += ' WHERE Lang__c = \'French\' ';
    List<TAS_labels__c> temp = database.query(theLabelsQuery + ' ORDER BY CreatedDate LIMIT 1');
    if (!temp.isEmpty()) theLabels = temp[0];
    else theLabels = new TAS_labels__c();
  }
  
  public void initTAS() {
    if (isNew) {
      theTAS = new Target_Account_Selling__c();
      isLong = (longRecordType == theTASRecordTypeID);
      theTAS.Account__c = theAccountId;
      
      // Set default values
      theTAS.Customer_s_Business_Profile__c = Label.Customer_s_Business_Profile_Default;
      theTAS.Opportunity_Profile__c = Label.Opportunity_Profile_Default;
      theTAS.Compelling_Event__c = Label.Compelling_Event_Default;
      theTAS.Unique_Business_Value__c = Label.Unique_Business_Value_Default;
      theTAS.Your_Solution__c = Label.Your_Solution_Default;
      theTAS.Our_Strengths__c = Label.Our_Strengths_Default;
      theTAS.Our_Weaknesses__c = Label.Our_Weaknesses_Default;
      theTAS.Goal__c = Label.Goal_Default;
      theTAS.Sales_Objective__c = Label.Sales_Objective_Default;
      theTAS.Competitive_Strategy__c = Label.Competitive_Strategy_Default;
      
    } else {
      List<Target_Account_Selling__c> temp = database.query(theTASQuery + ' WHERE Id =:theId');
      if (!temp.isEmpty()) {
        theTAS = temp[0];
        isLong = (longRecordType == theTAS.RecordTypeID);
      }
    }
  }
  
  public void initTASOwner() {
  	if (isNew) {
  		theTASOwnerName = UserInfo.getUserName();
  	} else {
  		theTASOwnerName = '';
  		List<User> temp = [SELECT Id, Name FROM User WHERE Id =: theTAS.OwnerId];
  		if (!temp.isEmpty()) {
  			theTASOwner = temp[0];
  			theTASOwnerName = theTASOwner.Name;
  		}
  	}
  }
  
  public void initOpps() {
    
    List <Opportunity> temp = database.query(theOppsQuery + ' WHERE Id =:theOppId');
    if (!temp.isEmpty()) theOpp = temp[0];
    
    theOpps = database.query(theOppsQuery + ' WHERE Target_Account_Selling_Plan__c != null'
                                          + ' AND Target_Account_Selling_Plan__c =:theId ' + queryLimit);
  }
  
  public void initAccount() {
    
    if (theOpp != null) {
      theAccountId = theOpp.AccountId;
      if (!isNew && theTAS.Account__c == null) theTAS.Account__c = theAccountId; 
    }
 
    List<Account> temp = database.query(theAccountQuery + ' WHERE Id =:theAccountId');
    if (!temp.isEmpty()) theAccount = temp[0];
  }
  
  public void initActivities() {
    
    initNewActivity();
    theActivities = database.query(theActivitiesQuery
                + ' WHERE WhatId != null AND WhatId =:theId AND RecordTypeId =:activityRecordTypeID ' + queryLimit); 
  }
  public void initMilestones() {
    
    initNewMilestone();
    theMilestones = database.query(theActivitiesQuery 
                + ' WHERE WhatId != null AND WhatId =:theId AND RecordTypeId =:milestonesRecordTypeID ' + queryLimit); 
  }
  public void initBusinessPartners() {
    
    initNewBusinessPartner();
    theBusinessPartners = database.query(theBusinessPartnerCompetitorQuery 
                      + ' WHERE Target_Account_Selling__c != null'
                      + ' AND Target_Account_Selling__c =:theId AND RecordTypeId =:businessPartnersRecordTypeID ' + queryLimit); 
  }
  public void initCompetitors() {
    
    initNewCompetitor();
    theCompetitors = database.query(theBusinessPartnerCompetitorQuery 
                 + ' WHERE Target_Account_Selling__c != null' 
                 + ' AND Target_Account_Selling__c =:theId AND RecordTypeId =:competitorsRecordTypeID ' + queryLimit); 
  }
  public void initDecisionCriteria() {
    
    initNewDecisionCriteria();
    theDecisionCriteria = database.query(theDecisionCriteriaQuery
                      + ' WHERE Target_Account_Selling__c != null'
                      + ' AND Target_Account_Selling__c =:theId AND RecordTypeId =:decisionCriteriaRecordTypeID ' + queryLimit);
    
    // Store decision criteria Ids
    Set<ID> theDCKeySet = new Set<ID>();
    for (TAS_Relationship_Questionare__c dc : theDecisionCriteria) theDCKeySet.add(dc.Id);
    
    // Get decision criteria players
    theDecisionCriteriaPlayers = database.query(theTASPlayerQuery
                                + ' WHERE TAS_Relationship_Questionare__c IN :theDCKeySet ' + queryLimit);
    
    // Wrap decision criteria objects
    theDecisionCriteriaWrapped = new List<DecisionCriteriaWrapper>();
    for (TAS_Relationship_Questionare__c dc : theDecisionCriteria) {
      
      DecisionCriteriaWrapper theDCWrapper = new DecisionCriteriaWrapper();
      theDCWrapper.theObj = dc;
      theDCWrapper.theNewPlayer = new TAS_PLayer__c(TAS_Relationship_Questionare__c = dc.Id);
      
      // Wrap decision criteria player objects
      List<TAS_Player__c> theNewPlayers = new List<TAS_Player__c>();
      for (TAS_Player__c dcp : theDecisionCriteriaPlayers) {
        if (dcp.TAS_Relationship_Questionare__c == dc.Id)
          theNewPlayers.add(dcp);
      }
      theDCWrapper.thePlayers = theNewPlayers;
      
      theDecisionCriteriaWrapped.add(theDCWrapper);
    }
    
  }

  public void initRelationshipStrategies() {
    
    initNewRelationshipStrategy();
    theRelationshipStrategies = database.query(theRelationshipQuestionaireQuery 
                 + ' WHERE Target_Account_Selling__c != null'
                 + ' AND Target_Account_Selling__c =:theId AND RecordTypeId =:relationshipStrategiesRecordTypeID ' + queryLimit); 
  }
  public void initTASContacts() {
    
    initNewTASContact();
    theTASContacts = database.query(theRelationshipQuestionaireQuery
                 + ' WHERE Target_Account_Selling__c != null'
                 + ' AND Target_Account_Selling__c =:theId AND RecordTypeId =:TASContactsRecordTypeID ' + queryLimit); 
  }
  
  public void initNewActivity() {
    newActivity = new Task();
    newActivity.WhatId = theId;
    newActivity.RecordTypeId = activityRecordTypeID;
    theTAS.New_Activity_Owner__c = null;
  }
  public void initNewMilestone() {
    newMilestone = new Task();
    newMilestone.WhatId = theId;
    newMilestone.RecordTypeId = milestonesRecordTypeID;
    theTAS.New_Milestone_Owner__c = null;
  }
  public void initNewBusinessPartner() {
    newBusinessPartner = new TAS_Business_Partners_Competitors__c();
    newBusinessPartner.Target_Account_Selling__c = theId;
    newBusinessPartner.RecordTypeId = businessPartnersRecordTypeID;
  }
  public void initNewCompetitor() {
    newCompetitor = new TAS_Business_Partners_Competitors__c();
    newCompetitor.Target_Account_Selling__c = theId;
    newCompetitor.RecordTypeId = competitorsRecordTypeID;
  }
  public void initNewDecisionCriteria() {
    newDecisionCriteria = new TAS_Relationship_Questionare__c();
    newDecisionCriteria.Target_Account_Selling__c = theId;
    newDecisionCriteria.RecordTypeId = decisionCriteriaRecordTypeID;
  }
  public void initNewRelationshipStrategy() {
    newRelationshipStrategy = new TAS_Relationship_Questionare__c();
    newRelationshipStrategy.Target_Account_Selling__c = theId;
    newRelationshipStrategy.RecordTypeId = relationshipStrategiesRecordTypeID;
  }
  public void initNewTASContact() {
    newTASContact = new TAS_Relationship_Questionare__c();
    newTASContact.Target_Account_Selling__c = theId;
    newTASContact.RecordTypeId = TASContactsRecordTypeID;
  }
  
  public void checkActivityFields() {
    Boolean fieldError = false;
    
    if (theTAS.New_Activity_Owner__c == null) {
      theTAS.New_Activity_Owner__c.addError('Field cannot be blank');
      fieldError = true;
    }
    if (fieldError) throw new tasException();
  }
  
  public void checkMilestoneFields() {
    Boolean fieldError = false;
    
    if (newMilestone.Name_Customer_Responsible__c == null) {
      newMilestone.Name_Customer_Responsible__c.addError('Field cannot be blank');
      fieldError = true;
    }
    if (fieldError) throw new tasException();
  }
  
  public void addActivity() {
    try {
      newActivity.OwnerId = theTAS.New_Activity_Owner__c;
      
      checkActivityFields();
      insert newActivity;
      
      initActivities();
      tasError = false;
    
    } catch (Exception e) {
      tasError = true;
      if (!e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
      throw e;  
    }
  }
  public void addMilestone() {
    try {
      //newMilestone.OwnerId = theTAS.New_Milestone_Owner__c;
      
      checkMilestoneFields();
      insert newMilestone;
      
      initMilestones();
      tasError = false;
    } catch (Exception e) {
      tasError = true;
      if (!e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
      throw e;  
    }
  }
  public void addBusinessPartner() {
    try {
      
      insert newBusinessPartner;
      
      initBusinessPartners();
      tasError = false;
    
    } catch (Exception e) {
      tasError = true;
      if (!e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
      throw e;  
    }
  }
  public void addCompetitor() {
    try {
      insert newCompetitor;
      initCompetitors();
      tasError = false;
    
    } catch (Exception e) {
      tasError = true;
      if (!e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
      throw e;  
    }
  }
  public void addDecisionCriteria() {
    try {
      
      insert newDecisionCriteria;
      
      initDecisionCriteria();
      tasError = false;
    
    } catch (Exception e) {
      tasError = true;
      if (!e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')) 
        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
      throw e;  
    }
  }
  public void addDecisionCriteriaPlayer() {
    try {
      for (DecisionCriteriaWrapper dc : theDecisionCriteriaWrapped) {
        if (dc.theObj.Id == decisionCriteriaPlayerParent) {
          insert dc.theNewPlayer;
        }
      }
      initDecisionCriteria();
      tasError = false;
      
    } catch (Exception e) {
      tasError = true;
      if (!e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
      throw e;  
    }
  }
  
  
  public void addRelationshipStrategy() {
    try {
      insert newRelationshipStrategy;
      initRelationshipStrategies();
      tasError = false;
    
    } catch (Exception e) {
      tasError = true;
      if (!e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
      throw e;  
    }
  }
  public void addTASContact() {
    try {
      insert newTASContact;
      initTASContacts();
      tasError = false;
    
    } catch (Exception e) {
      tasError = true;
      if (!e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
      throw e;  
    }
  }
  
  public void removeActivity() {
    try {
      Task taskToRemove;
      for (Task t : theActivities) if (t.Id == removeActivityId) taskToRemove = t;
      if (taskToRemove != null) delete taskToRemove;
      initActivities();
      tasError = false;
    
    } catch (Exception e) {
      tasError = true;
      ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
      throw e;  
    }
  }
  public void removeMilestone() {
    try {
      Task taskToRemove;
      for (Task t : theMilestones) if (t.Id == removeMilestoneId) taskToRemove = t;
      if (taskToRemove != null) delete taskToRemove;
      initMilestones();
      tasError = false;
    
    } catch (Exception e) {
      tasError = true;
      ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
      throw e;  
    }
  }
  public void removeBusinessPartner() {
    try { 
      TAS_Business_Partners_Competitors__c partnerToRemove;
      for (TAS_Business_Partners_Competitors__c p : theBusinessPartners) 
        if (p.Id == removeBusinessPartnerID) partnerToRemove = p;
      if (partnerToRemove != null) delete partnerToRemove;
      initBusinessPartners();
      tasError = false;
    
    } catch (Exception e) {
      tasError = true;
      ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
      throw e;  
    }
  }
  public void removeCompetitor() {
    try {
      TAS_Business_Partners_Competitors__c competitorToRemove;
      for (TAS_Business_Partners_Competitors__c c : theCompetitors) 
        if (c.Id == removeCompetitorId) competitorToRemove = c;
      if (competitorToRemove != null) delete competitorToRemove;
      initCompetitors();
      tasError = false;
    
    } catch (Exception e) {
      tasError = true;
      ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
      throw e;  
    }
  }
  public void removeDecisionCriteria() {
    try {
      TAS_Relationship_Questionare__c decisionCriteriaToRemove;
      for (TAS_Relationship_Questionare__c dc : theDecisionCriteria) 
        if (dc.Id == removeDecisionCriteriaId) decisionCriteriaToRemove = dc;
      if (decisionCriteriaToRemove != null) delete decisionCriteriaToRemove;
      initDecisionCriteria();
      tasError = false;
    
    } catch (Exception e) {
      tasError = true;
      ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
      throw e;  
    }
  }
  public void removeDecisionCriteriaPlayer() {
    try {
      for (TAS_Player__c dcp : theDecisionCriteriaPlayers) {
        if (dcp.Id == removeDecisionCriteriaPlayerID) {
          delete dcp;
        }
      }
      initDecisionCriteria();
    
    } catch (Exception e) {
      tasError = true;
      ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
      throw e;  
    }
  }
  public void removeRelationshipStrategy() {
    try {
      TAS_Relationship_Questionare__c relationshipStrategyToRemove;
      for (TAS_Relationship_Questionare__c rs : theRelationshipStrategies) 
        if (rs.Id == removeRelationshipStrategyId) relationshipStrategyToRemove = rs;
      if (relationshipStrategyToRemove != null) delete relationshipStrategyToRemove;
      initRelationshipStrategies();
      tasError = false;
    
    } catch (Exception e) {
      tasError = true;
      ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
      throw e;  
    }
  }
  public void removeTASContact() {
    try {
      TAS_Relationship_Questionare__c TASContactToRemove;
      for (TAS_Relationship_Questionare__c tc : theTASContacts) 
        if (tc.Id == removeTASContactId) TASContactToRemove = tc;
      if (TASContactToRemove != null) delete TASContactToRemove;
      initTASContacts();
      tasError = false;
    
    } catch (Exception e) {
      tasError = true;
      ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
      throw e;  
    }
  }
  
  public List<SObject> cloneRelatedList(ID theNewLookupId, List<SObject> theListToClone) {
    
    List<SObject> theNewList = new List<SObject>();
    for (SObject theObj : theListToClone) {
      SObject theObjClone = theObj.clone(false, true, false, false);
      if (theObjClone.get('Target_Account_Selling__c') != null)
        theObjClone.put('Target_Account_Selling__c', theNewLookupId);
      theNewList.add(theObjClone);
    }
    return theNewList;
  }  
  public List<SObject> cloneTaskList(ID theNewLookupId, List<Task> theListToClone) {
    
    List<Task> theNewList = new List<Task>();
    for (Task theObj : theListToClone) {
      Task theObjClone = theObj.clone(false, true, false, false);
      if (theObjClone.get('WhatId') != null)
        theObjClone.put('WhatId', theNewLookupId);
      theNewList.add(theObjClone);
    }
    return theNewList;
  }
  
  public void cloneDecisionCriteria(ID theNewLookupId) {
    
    Map<ID, TAS_Relationship_Questionare__c> theDCCloneMap = new Map<ID, TAS_Relationship_Questionare__c>();
    for (TAS_Relationship_Questionare__c dc : theDecisionCriteria){
      TAS_Relationship_Questionare__c clonedDC = dc.clone(false, true, false ,false);
      clonedDC.Target_Account_Selling__c = theNewLookupId;
      theDCCloneMap.put(dc.Id, clonedDC);
    }
    insert theDCCloneMap.values();
    
    // clone nested decision criteria players
    List<TAS_Player__c> theClonedDecisionCriteriaPlayers = new List<TAS_Player__c>();
    for (TAS_Player__c dcp : theDecisionCriteriaPlayers) {
      
      if (theDCCloneMap.containsKey(dcp.TAS_Relationship_Questionare__c)) {
        
        TAS_Player__c theClonedPlayer = dcp.clone(false, true, false, false);
        theClonedPlayer.TAS_Relationship_Questionare__c = theDCCloneMap.get(dcp.TAS_Relationship_Questionare__c).Id;
        theClonedDecisionCriteriaPlayers.add(theClonedPlayer);
      }
    }
    
    insert theClonedDecisionCriteriaPlayers;
  }
  
  public PageReference doClone() {
     try {
       if (!isNew) {
        
        Target_Account_Selling__c theTASClone = theTAS.clone(false, true, false, false);
        theTASClone.Name = theTASClone.Name + ' - Clone';
        insert theTASClone;
        
        // clone each linked object
        List<Task> theClonedActivities = cloneTaskList(theTASClone.Id, theActivities);
        List<Task> theClonedMilestones = cloneTaskList(theTASClone.Id, theMilestones);
        List<TAS_Business_Partners_Competitors__c> theClonedBusinessPartners = 
                                    cloneRelatedList(theTASClone.Id, theBusinessPartners);
        List<TAS_Business_Partners_Competitors__c> theClonedCompetitors = 
                                    cloneRelatedList(theTASClone.Id, theCompetitors);
        List<TAS_Relationship_Questionare__c> theClonedTASContacts = cloneRelatedList(theTASClone.Id, theTASContacts);
        List<TAS_Relationship_Questionare__c> theRelationshipStrategies = cloneRelatedList(theTASClone.Id, theRelationshipStrategies);
        
        insert theClonedActivities;
        insert theClonedMilestones;
        insert theClonedBusinessPartners;
        insert theClonedCompetitors;
        insert theClonedTASContacts;
        insert theRelationshipStrategies;
        
        cloneDecisionCriteria(theTASClone.Id);
        
        PageReference newPage = Page.trac_TAS;
        newPage.getParameters().put('ID', theTASClone.Id);
        newPage.getParameters().put('ACCOUNTID', theAccountID);
        
        isNew = false;
        tasError = false;
  
        newPage.setRedirect(true);
        return newPage;
        
       }
       tasError = false;
       genericError = '1';
       return null;
    
    } catch (Exception e) {
      tasError = true;
      if (!e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')) 
        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
      throw e;  
      return null;
    }
  }
  
  public PageReference doDelete() {
    
    try {
      
      if (!isNew) {
        
        List<ID> theActivitiesIdsList = new List<ID>();
        List<ID> theMilestonesIdsList = new List<ID>();
        List<ID> theBusinessPartnersIdsList = new List<ID>();
        List<ID> theCompetitorsIdsList = new List<ID>();
        List<ID> theDecisionCriteriaIdsList = new List<ID>();
        List<ID> theRelationshipStrategiesIdsList = new List<ID>();
        List<ID> theTASContactsIdsList = new List<ID>();
        
        for (Task obj: theActivities) theActivitiesIdsList.add(obj.Id);
        for (Task obj: theMilestones) theMilestonesIdsList.add(obj.Id);
        for (TAS_Business_Partners_Competitors__c obj: theBusinessPartners) theBusinessPartnersIdsList.add(obj.Id);
        for (TAS_Business_Partners_Competitors__c obj: theCompetitors) theCompetitorsIdsList.add(obj.Id);
        for (TAS_Relationship_Questionare__c obj: theDecisionCriteria) theDecisionCriteriaIdsList.add(obj.Id);
        for (TAS_Relationship_Questionare__c obj: theRelationshipStrategies) theRelationshipStrategiesIdsList.add(obj.Id);
        for (TAS_Relationship_Questionare__c obj: theTASContacts) theTASContactsIdsList.add(obj.Id);
        
        trac_TAS_Unrestricted.deleteTheTAS(theId, 
                                           theActivitiesIdsList, 
                                           theMilestonesIdsList, 
                                           theBusinessPartnersIdsList, 
                                           theCompetitorsIdsList, 
                                           theDecisionCriteriaIdsList, 
                                           theRelationshipStrategiesIdsList, 
                                           theTASContactsIdsList);
        
        PageReference newPage = new PageReference('/' + theAccountID);
        isNew = false;
        tasError = false;
  
        newPage.setRedirect(true);
        return newPage;
      }
      tasError = false;
      genericError = '1';
      return null;
    } catch (Exception e) {
      tasError = true;
      ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
      throw e;  
      return null;
    }
  }
  
  public PageReference doSave() {
     try {
       if (isNew) {
        
        if (theTASRecordTypeId != null) theTAS.RecordTypeId = theTASRecordTypeId;
        if (theAccountId != null) theTAS.Account__c = theAccountId;
        insert theTAS;
        
        PageReference newPage = Page.trac_TAS;
        newPage.getParameters().put('ID', theTAS.Id);
        newPage.getParameters().put('OPPID', theOpp.Id);
        
        if (theOpp.Target_Account_Selling_Plan__c == null && theTAS.Id != null) {
          theOpp.Target_Account_Selling_Plan__c = theTAS.Id;
          update theOpp;
        }
        
        isNew = false;
       
        initTAS();
        initOpps();
        
        newPage.setRedirect(true);
        tasError = false;
        genericError = '1';
        return newPage;
        
       } else {
        update theTAS;
        initTAS();
       }
       
       tasError = false;
       genericError = '1';
       return null;
      
    } catch (Exception e) {
      tasError = true;
      if (!e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')) 
        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
      throw e;  
      return null;
    }
  }
  
  public void saveEditable() {
    
    for (Task w : theActivities) {
      if (w.Id == editableFieldId) {
        update w;
        initActivities();
        return;
      }
    }
    for (Task w : theMilestones) {
      if (w.Id == editableFieldId) {
        update w;
        initMilestones();
        return;
      }
    }
    for (TAS_Business_Partners_Competitors__c w : theBusinessPartners) {
      if (w.Id == editableFieldId) {
        update w;
        initBusinessPartners();
        return;
      }
    }
    for (TAS_Business_Partners_Competitors__c w : theCompetitors) {
      if (w.Id == editableFieldId) {
        update w;
        initCompetitors();
        return;
      }
    }
    for (TAS_Relationship_Questionare__c w : theRelationshipStrategies) {
      if (w.Id == editableFieldId) {
        update w;
        initRelationshipStrategies();
        return;
      }
    }
    for (TAS_Relationship_Questionare__c w : theTASContacts) {
      if (w.Id == editableFieldId) {
        update w;
        initTASContacts();
        return;
      }
    }
    for (TAS_Relationship_Questionare__c w : theDecisionCriteria) {
      if (w.Id == editableFieldId) {
        update w;
        initDecisionCriteria();
        return;
      }
    }
    for (TAS_Player__c w : theDecisionCriteriaPlayers) {
      if (w.Id == editableFieldId) {
        update w;
        initDecisionCriteria();
        return;
      }
    }
  }

}