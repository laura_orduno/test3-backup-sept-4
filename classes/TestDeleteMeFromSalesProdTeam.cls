@isTest
private class TestDeleteMeFromSalesProdTeam{
     static testmethod void testCodeCoverage(){
         // created Account -Start
         Account account = new Account(
             Name= 'TheTestDeleteMeFromSalesTeam Account',
             BillingState = 'AB',
             Phone = '604-120-1910'
         );
         insert account;            
         //created Account -End
         
         User u = [SELECT Id FROM User WHERE IsActive = true AND Id != :UserInfo.getUserId() LIMIT 1];
         
         //Create Opportunity-Start             
         Datetime dateTimetemp = System.now() + 5;
         Date dateTemp = Date.newInstance(dateTimetemp.year(),dateTimetemp.month(),dateTimetemp.day());
         Opportunity opp = new Opportunity (
             accountId = account.Id,
             Name = 'Testing Opportunity for Owner', 
             CloseDate = dateTemp,                  
             OwnerId = u.id, 
             StageName = 'I have a Potential Opportunity (20%)'
         );        
         insert opp ; 
         OpportunityTeamMember optTeamMember = new OpportunityTeamMember(
             OpportunityId = opp.Id,
             UserId = u.Id
         );
         insert optTeamMember ;
         OpportunityTeamMember optTeamMember2 = new OpportunityTeamMember(
             OpportunityId = opp.Id,
             UserId = UserInfo.getUserId()
         );
         insert optTeamMember2;
         
         //Create Opportunity-End
         //Create Product -Start
         Product2 prodObj = new Product2(
             Name = 'Test -Product',
             IsActive = true,
             Product_Family_Code__c = 'TEST11'
         );
         insert prodObj;
         //Create Product -End
         Opp_Product_Item__c opiObj = new Opp_Product_Item__c(
             Product__c = prodObj.Id,
             Opportunity__c = opp.Id            
         );
         insert opiObj;
         Product_Sales_Team__c prodSalesTeam = new Product_Sales_Team__c(
             Product__c = opiObj.Id,
             Member__c = u.Id    
         );
         insert prodSalesTeam;
         Product_Sales_Team__c prodSalesTeam2 = new Product_Sales_Team__c(
             Product__c =  opiObj.Id,
             Member__c = UserInfo.getUserId()  
         );
         insert prodSalesTeam2;
         
         //Test Case--User can delete by login if exist as Account Team Member 
         System.runAs(u){
            ApexPages.currentPage().getParameters().put('oppId', opp.Id);
            DeleteMeFromSalesProdTeam obj = new DeleteMeFromSalesProdTeam();
            obj.init();
            obj.cancel();
            obj.deleteOpportunityTeamMember();
         }      

         DeleteMeFromSalesProdTeam obj2 = new DeleteMeFromSalesProdTeam();
         obj2.init();
         obj2.cancel();
     }
}