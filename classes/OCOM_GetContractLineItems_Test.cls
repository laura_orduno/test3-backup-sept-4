@isTest
private class OCOM_GetContractLineItems_Test {

     static testmethod void ServiceRequestItemsTest1(){
        //TestDataHelper.testContractLineItemListCreation_ConE();
         TestDataHelper.testContractCreationWithContractRequest();
         TestDataHelper.testContractLineItemListWithProductCreation();
         Test.startTest();
         Id Id1 = null;
     
        Map<String,Object> outMap=new Map<String,Object>();
        Apexpages.currentPage().getparameters().put('id',TestDataHelper.testContractRequest.id);
        OCOM_GetContractLineItems ServiceRequestItem = new OCOM_GetContractLineItems();
        boolean blnSuccess = ServiceRequestItem.invokeMethod('getItemsList',new Map<String,Object>{'contextObjId' => TestDataHelper.testContractRequest.id},outMap,new Map<String,Object>());
        system.assertEquals(outMap!=null && outMap.size() > 0, true);
        system.assertEquals(blnSuccess, true);
        
        Test.stopTest();
    }

    static testmethod void DealSupportItemsTest2(){
        //TestDataHelper.testContractLineItemListCreation_ConE();
        TestDataHelper.testContractCreationwithDealSupport();
        TestDataHelper.testContractLineItemListWithProductCreation();
       
        Test.startTest();
        Id Id1 = null;
     
    Map<String,Object> outMap=new Map<String,Object>();
        Apexpages.currentPage().getparameters().put('id',TestDataHelper.testDealSupport.id);
        OCOM_GetContractLineItems DealSupportItemsData = new OCOM_GetContractLineItems();
        boolean blnSuccess=DealSupportItemsData.invokeMethod('getItemsList',new Map<String,Object>{'contextObjId' => TestDataHelper.testDealSupport.id},outMap,new Map<String,Object>());
        system.assertEquals(outMap!=null && outMap.size() > 0, true);
        system.assertEquals(blnSuccess, true);
        
        

    Test.stopTest();
    }
    
     static testmethod void OrderLineItemsTest2(){
        //TestDataHelper.testContractLineItemListCreation_ConE();
        //TestDataHelper.testContractCreationwithOrder();
        //TestDataHelper.testContractLineItemListWithProductCreation();
  
        Test.startTest();
        Id Id1 = null;
     
        Map<String,Object> outMap=new Map<String,Object>();
        String orderId= OrdrTestDataFactory.singleMethodForOrderId();
        //Apexpages.currentPage().getparameters().put('id',TestDataHelper.orderObject.id);
        Apexpages.currentPage().getparameters().put('id',orderid);
        OCOM_GetContractLineItems OrderLineItemsData = new OCOM_GetContractLineItems();
        //boolean blnSuccess=OrderLineItemsData.invokeMethod('getItemsList',new Map<String,Object>{'contextObjId' => TestDataHelper.orderObject.id},outMap,new Map<String,Object>());
        
        boolean blnSuccess=OrderLineItemsData.invokeMethod('getItemsList',new Map<String,Object>{'contextObjId' => orderId},outMap,new Map<String,Object>());
        
        system.assertEquals(outMap!=null && outMap.size() > 0, true);
        system.assertEquals(blnSuccess, true);

    Test.stopTest(); 
    
    
        
    } 
    
    
}