@isTest
private class Test_SetOwnerName{
     static testmethod void testMappingTrigger(){
         // created Account -Start
             Account acc = new Account(
                 Name= 'Test123',
                 Phone = '6049969449'
             );
             insert acc;
             System.debug('Account='+acc);
             //Account is updated as Partner Account 
             acc.IsPartner = true;
             update acc;
             System.debug('Ispartner='+acc);
         //created Account -End  
         //Contact created  -Start 
             Contact con = new Contact(
                 LastName = 'Test Contact',
                 AccountId= acc.Id,
                 Phone = '6049969449'
             );
             insert con;
             System.debug('contact='+con); 
         //Contact created  -End
         //Created a Partner User - Start
             //Selected a profile Partner User             
             Profile pPowerPartner =[select id from Profile where userType= 'PowerPartner' limit 1];             
             System.debug('pPowerPartner ='+pPowerPartner );         
             User u = new User(
                 FirstName = 'FTestName',
                 LastName = 'LTestName',
                 Alias = 'tXtN',
                 Email = 'tes234t@abc.com',
                 Username = 'partner1234567890@abc.com',
                 CommunityNickname = 'testxvf',
                 emailencodingkey = 'UTF-8',
                 languagelocalekey = 'en_US',
                 localesidkey = 'en_US',
                 ContactId = con.Id,
      phone = '6049969449',
                 timezonesidkey = 'America/Los_Angeles',             
                 profileId = pPowerPartner.Id
             );
             insert u;             
         //Created a Partner User - End
             System.debug('UserType=' + u.UserType);  
         //Create Opportunity-Start             
             Datetime dateTimetemp = System.now() + 5;
             Date dateTemp = Date.newInstance(dateTimetemp.year(),dateTimetemp.month(),dateTimetemp.day());
             Opportunity opp = new Opportunity (
                 accountId = acc.Id,
                 Name = 'Testing Opportunity for Owner', 
                 CloseDate = dateTemp,                  
                 OwnerId = u.id, 
                 StageName = 'I have a Potential Opportunity (20%)'
             );        
             insert opp ; 
             User usr = [select  FirstName, LastName, Contact.AccountId, UserType from User where id=:opp.OwnerId];            
             System.debug('usr =' + usr );
             Opportunity o = [select OwnerId , Owner_Name__c from Opportunity where id= :opp.id];
             System.debug('Opp=' + o);
             System.assertEquals(null, o.Owner_Name__c);
             //System.assertEquals(usr.FirstName + ' ' + usr.LastName, o.Owner_Name__c);
             List<Group> q = [Select g.Name, g.Id From Group g limit 1];             
             //o.OwnerId = q[0].id;
             //update o;
             //o = [select Owner_Name__c from Opportunity where id=:o.id];
             //System.assertEquals(q[0].Name, o.Owner_Name__c);  
         //Create Opportunity-End        
         //Created A lead -Start
             Lead led = new Lead(                 
                 Company = 'Testing Lead for Owner', 
                 LastName = 'Test', 
                 Phone = '111111111',
                 OwnerId = u.id, 
                 Status = 'Qualified'
             );        
             insert led;         
         //Created A lead -End        
             usr = [select  FirstName, LastName, Contact.AccountId, UserType from User where id=:led.OwnerId];
             Lead l = [select OwnerId , Owner_Name__c from Lead where id=:led.id];
             System.assertEquals(null, l.Owner_Name__c); 
             //System.assertEquals(usr.FirstName + ' ' + usr.LastName, l.Owner_Name__c); 
             System.debug('IDDD===Lead='+ l.OwnerId + ' Queue='+ q[0].id);             
             /*l.OwnerId = q[0].id;
             update l;
             l = [select Owner_Name__c from Lead where id=:l.id];
             System.assertEquals(q[0].Name, l.Owner_Name__c); */
             
          
     }
}