// This class performs two main jobs, first is to parse the total number of records from CSV,
// and second is to determine if this job process the parsed record(s) or calls distributor
// to distribute the job to more async jobs.
// 
// As per conversion, this class will perform the following:
// 
// 1. Use data mapping fieldset to map data
// 2. Perform PAC validation on address
// 3. Create Contact
// 4. Create Service Address
// 5. Create Bulk Order Load Record
// 
// Parallel processing: if there is a need to perform parallel processing because of
// performance, a better de-depulicate new Contact and Service Address machanism to be
// completed such as using a temporary object to sanitize the uniqueness of service
// address before creating new.

public class BulkOrderLoadRecordConverter implements queueable,database.allowscallouts{
    id bulkOrderLoadId{get;set;}
    string sessionId{get;set;}
    public bulk_order_load_file__c bulkOrderLoadFile{get;set;}
    public list<BulkLoadMaster.BulkOrderLoadRecordRow> bulkOrderLoadRecordRows{get;set;}
    public integer conversionPerJob{get;set;}
    
    void init(string sessionId){
        // 120 seconds maximum callout time, 4 jobs takes 30 seconds each
        conversionPerJob=integer.valueof(label.Validation_Job_Size);
        this.sessionId=sessionId;
    }
    public BulkOrderLoadRecordConverter(string sessionId,id bulkOrderLoadId){
        this.bulkOrderLoadId=bulkOrderLoadId;
        init(sessionId);
    }
    public BulkOrderLoadRecordConverter(string sessionId,id bulkOrderLoadId,list<BulkLoadMaster.BulkOrderLoadRecordRow> bulkOrderLoadRecordRows){
        this.bulkOrderLoadRecordRows=bulkOrderLoadRecordRows;
        this.bulkOrderLoadId=bulkOrderLoadId;
        init(sessionId);
    }
    public void execute(queueablecontext context){
        if(bulkOrderLoadRecordRows==null||bulkOrderLoadRecordRows.isempty()){
            parseAttachment();
        }
        if(bulkOrderLoadRecordRows!=null){
            //TODO determine based on configured conversion per job to determine if calling distributor is needed
            integer numRecords=bulkOrderLoadRecordRows.size();
//System.debug('Bulkload: BulkOrderLoadRecordConverter: ' + numRecords + ' <= ' + conversionPerJob);            
            if(numRecords<=conversionPerJob){
                processBulkOrderLoadRecords();
            }else{
System.debug('Bulkload: BulkOrderLoadRecordConverter, requestDistribution');            
                requestDistribution();
                //TODO Unable to handle this many conversions, call job distributor
                /*
                string body='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bul="http://soap.sforce.com/schemas/class/BulkLoadMaster">'+
                    '<soapenv:Header><bul:SessionHeader><bul:sessionId>00De0000005WWbk!AR0AQCL9mMc7J1HV7YJ.SRrMavDIkjbzjqIGm16a9G7AabjDKVA9oJw2KG0ykTd.FAJsE_SaEaSIbeVf7D4V9WcVzGGsooz9</bul:sessionId></bul:SessionHeader></soapenv:Header><soapenv:Body>';
                body+='<bul:processAccounts>';
                for(id accountId:accountIds){
                    body+='<bul:accountIds>'+accountId+'</bul:accountIds>';
                }
                body+='</bul:processAccounts></soapenv:Body></soapenv:Envelope>';
                http http=new http();
                httprequest req=new httprequest();
                req.setendpoint('https://telus--bulkload.cs15.my.salesforce.com/services/Soap/class/BulkLoadMaster');
                req.setmethod('POST');
                req.setheader('Content-Type','text/xml');
                req.setheader('SOAPAction','""');
                req.setBody(body);
                httpresponse res=http.send(req);
                system.debug('Total accounts queried:'+accountIds.size()+', Total SOQL Used:'+limits.getQueries()+', total callouts:'+limits.getCallouts());
                system.debug('response:'+res.getBody());
				*/
            }
        }
    }
    void retry(){
        system.enqueuejob(new BulkOrderLoadRecordConverter(this.sessionId,this.bulkOrderLoadId,this.bulkOrderLoadRecordRows));
    }
    void requestDistribution(){
        system.debug('Bulkload, requestDistribution: ' + sessionId + ', ' + bulkOrderLoadRecordRows);
        system.debug('session id:'+sessionId);
        // Pass one big call instead of multiple, this will minimize the redundant network consumption
        string body='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bul="http://soap.sforce.com/schemas/class/BulkLoadMaster">'+
            '<soapenv:Header><bul:SessionHeader><bul:sessionId>'+sessionId+'</bul:sessionId></bul:SessionHeader></soapenv:Header><soapenv:Body>';
        body+='<bul:bulkOrderLoadRecordsDistributor><bul:bulkOrderLoadId>'+bulkOrderLoadId+'</bul:bulkOrderLoadId>';
        for(BulkLoadMaster.BulkOrderLoadRecordRow bulkOrderLoadRecordRow:bulkOrderLoadRecordRows){
            body+='<bul:bulkOrderLoadRecordRows>';
            for(string column:bulkOrderLoadRecordRow.columns){
                body+='<bul:columns>'+column+'</bul:columns>';
            }
            body+='<bul:rowNumber>'+bulkOrderLoadRecordRow.rowNumber+'</bul:rowNumber></bul:bulkOrderLoadRecordRows>';
        }
        body+='</bul:bulkOrderLoadRecordsDistributor></soapenv:Body></soapenv:Envelope>';
        http http=new http();
        httprequest req=new httprequest();
        req.setendpoint(url.getsalesforcebaseurl().getprotocol()+'://'+url.getsalesforcebaseurl().gethost()+'/services/Soap/class/BulkLoadMaster');
        req.setmethod('POST');
        req.setheader('Content-Type','text/xml');
        req.setheader('SOAPAction','""');
        req.setBody(body);
        
        system.debug('Total accounts queried:'+bulkOrderLoadRecordRows.size()+', Total SOQL Used:'+limits.getQueries()+', total callouts:'+limits.getCallouts());
        if(!Test.isRunningTest()){
	        httpresponse res=http.send(req);
	        system.debug('response:'+res.getBody());            
        }
/*        
        else{
            // call explicit method, cannot call web services from test methods
			BulkLoadMaster.bulkOrderLoadRecordsDistributor(bulkOrderLoadId, bulkOrderLoadRecordRows);            
        }
*/
    }
    // This is only processing up certain # of rows in CSV as this is an async job that can sustain only up to 12MB heap size as of Jan 2017.
    // If this job exceeds heap size, the upload process will have to be redesigned.
    void parseAttachment(){
        if(string.isnotblank(bulkOrderLoadId)){
            try{
                bulkOrderLoadFile=[select id,submission_status__c,submission_error__c,total_records_in_template__c from bulk_order_load_file__c where id=:this.bulkOrderLoadId];
                attachment bulkOrderLoadFileAttachment=[select id,body from attachment where parentid=:bulkOrderLoadId order by createddate desc limit 1];
                list<list<string>> bulkOrderLoadRecords=BulkLoadUtils.parseCSV(bulkOrderLoadFileAttachment.body.tostring(),true);

                bulkOrderLoadRecordRows=new list<BulkLoadMaster.BulkOrderLoadRecordRow>();
                integer rowNumber=integer.valueof(label.Row_Number_Starts_With);
                for(list<string> bulkOrderLoadRecord:bulkOrderLoadRecords){
                    bulkOrderLoadRecordRows.add(new BulkLoadMaster.BulkOrderLoadRecordRow(bulkOrderLoadRecord,rowNumber));
                    rowNumber++;
                    string line='';
// system.debug('Num fields:'+bulkOrderLoadRecord.size());
                    for(string record:bulkOrderLoadRecord){
                        line+=record+',';
                    }
// system.debug('Line:'+line);
                }
// system.debug('# of lines:'+bulkOrderLoadRecords.size());
                decimal result=(decimal.valueof(limits.getheapsize())).divide((decimal.valueof(limits.getlimitheapsize())),2);
                if(result>0.8){
                    Util.emailSystemError('BulkOrderLoadRecordConverter - Heap Size exceeds 80%','Please note that Bulk Order Load Record Converter exceeds 80% of the heap size:'+limits.getheapsize());
                }
            }catch(exception e){
                failProcessingBulkOrderLoad(e);
            }
        }
    }
    public void processBulkOrderLoadRecords(){
System.debug('BulkLoad: processBulkOrderLoadRecords: ' + bulkOrderLoadRecordRows);    
        
        list<bulk_order_load_record__c> insertBulkOrderLoadRecords=new list<bulk_order_load_record__c>();
        list<service_request__c> insertServiceRequests=new list<service_request__c>();
        map<decimal,id> productIdMap=new map<decimal,id>();
        map<string,id> userTIDMap=new map<string,id>();
        schema.displaytype displayType;
        string fieldPath;
        schema.fieldsetmember recordFieldSetMember;
        string bulkOrderLoadRecordField;
        boolean hasMappingError=false;
        //TODO perform mapping with exception of relationship fields
        schema.fieldset bulkOrderLoadRecordFieldSet=bulk_order_load_record__c.sobjecttype.getdescribe().fieldsets.getmap().get('Data_Load_Mapping');
        list<schema.fieldsetmember> recordFieldSetMembers=bulkOrderLoadRecordFieldSet.getfields();
        integer numFieldSetMembers=recordFieldSetMembers.size();
        map<string,schema.sobjectfield> bulkOrderLoadRecordFieldsMap=schema.sobjecttype.bulk_order_load_record__c.fields.getmap();
        map<string,schema.describefieldresult> fieldResultMap=new map<string,schema.describefieldresult>();
        for(schema.fieldsetmember member:recordFieldSetMembers){
            fieldResultMap.put(member.getfieldpath(),bulkOrderLoadRecordFieldsMap.get(member.getfieldpath()).getdescribe());
        }
        integer numFieldsInFile=0;
        for(BulkLoadMaster.BulkOrderLoadRecordRow bulkOrderLoadRecordRow:bulkOrderLoadRecordRows){
            //******************************************************
            // Data Mapping - Simple Data Type Validation
            //******************************************************
            try{
                numFieldsInFile=bulkOrderLoadRecordRow.columns.size();
                system.debug('Num Fields in File (first iteration):'+numFieldsInFile);
                for(integer i=0;i<numFieldSetMembers;i++){
                    recordFieldSetMember=recordFieldSetMembers[i];
                    displayType=recordFieldSetMember.gettype();
                    fieldPath=recordFieldSetMember.getfieldpath();
//System.debug('Bulkload: fieldPath: ' + fieldPath);                    
                    if(recordFieldSetMember.getrequired()){
                        if(i<numFieldsInFile){
                            bulkOrderLoadRecordField=bulkOrderLoadRecordRow.columns[i];
//System.debug('Bulkload: bulkOrderLoadRecordField: + ' + bulkOrderLoadRecordField);                                                    
                            if(string.isnotblank(bulkOrderLoadRecordField)){
// system.debug('Now mapping field:'+fieldPath);
                                //if(displayType==schema.DisplayType.REFERENCE){
                                if(fieldPath.equalsignorecase('product_external_id__c')){
                                    productIdMap.put(decimal.valueof(bulkOrderLoadRecordField),null);
// system.debug('Mapping '+fieldPath+':'+decimal.valueof(bulkOrderLoadRecordField));
                                }else if(fieldPath.equalsignorecase('owner_tid__c')){
                                    userTIDMap.put(bulkOrderLoadRecordField.toLowerCase(), null);
                                    userTIDMap.put(bulkOrderLoadRecordField.toUpperCase(), null);
// system.debug('Mapping '+fieldPath+':'+bulkOrderLoadRecordField);
                                }
                                //}
                            }
                            else{
//System.debug('Bulkload: isBlank');                                                    
                            }
                        }
                    }
                }
            }catch(exception e){
                //TODO May need to review exception message would be user-friendly enough for users to take appropriate action.
                hasMappingError=true;
                failProcessingBulkOrderLoad(e);
            }
        }
        if(!hasMappingError){
            integer customerRequestDateDayOfMonth,customerRequestDateMonth,customerRequestDateYear;
            list<srs_pods_product__c> srsProducts=[select id,product_id__c from srs_pods_product__c where product_id__c=:productIdMap.keyset() limit 50000];
// system.debug('Num SRS PODS Product found:'+srsProducts.size());
            for(srs_pods_product__c srsProduct:srsProducts){
                productIdMap.put(srsProduct.product_id__c,srsProduct.id);
            }
//System.debug('Bulkload: userTIDMap.keyset(): ' + userTIDMap.keyset());            
            list<user> allUsers=[select id,team_telus_id__c from user where team_telus_id__c=:userTIDMap.keyset() limit 50000];
// system.debug('Num user found:'+allUsers.size());
            for(user allUser:allUsers){
                // ensure keys contain upper and lower case versions of TID
                userTIDMap.put(allUser.team_telus_id__c.toLowerCase(),allUser.id);
                userTIDMap.put(allUser.team_telus_id__c.toUpperCase(),allUser.id);
            }
            for(BulkLoadMaster.BulkOrderLoadRecordRow bulkOrderLoadRecordRow:bulkOrderLoadRecordRows){
                bulk_order_load_record__c insertBulkOrderLoadRecord=new bulk_order_load_record__c(bulk_order_load_file__c=bulkOrderLoadId,status__c='In Progress',template_row_nbr__c=bulkOrderLoadRecordRow.rowNumber);
//                bulk_order_load_record__c insertBulkOrderLoadRecord=new bulk_order_load_record__c(bulk_order_load_file__c=bulkOrderLoadId,status__c='In Progress',sr_null__c=true,template_row_nbr__c=bulkOrderLoadRecordRow.rowNumber);
                //******************************************************
                // Data Mapping - Simple Data Type Validation
                //******************************************************
                try{
                    numFieldsInFile=bulkOrderLoadRecordRow.columns.size();
                    for(integer i=0;i<numFieldSetMembers;i++){
                        recordFieldSetMember=recordFieldSetMembers[i];
                        displayType=recordFieldSetMember.gettype();
                        fieldPath=recordFieldSetMember.getfieldpath();
//System.debug('Bulkload: fieldPath: ' + fieldPath);                    
                        if(recordFieldSetMember.getrequired()){
                            if(i<numFieldsInFile){
                                bulkOrderLoadRecordField=bulkOrderLoadRecordRow.columns[i];
//system.debug('Bulkload: Now mapping field: ' + fieldPath);
                                if(displayType==schema.DisplayType.EMAIL||displayType==schema.DisplayType.STRING||displayType==schema.DisplayType.TEXTAREA||displayType==schema.DisplayType.PICKLIST){
                                    insertBulkOrderLoadRecord.put(bulkOrderLoadRecordFieldsMap.get(fieldPath),(string.isblank(bulkOrderLoadRecordField)?bulkOrderLoadRecordField:bulkOrderLoadRecordField.left(fieldResultMap.get(fieldPath).getlength())));
//system.debug('Text field:'+fieldPath+', value:'+bulkOrderLoadRecordField.left(fieldResultMap.get(fieldPath).getlength()));
                                }
//System.debug('Bulkload: fieldPath.equalsignorecase(product_external_id__c): ' + fieldPath.equalsignorecase('product_external_id__c'));                    
//System.debug('Bulkload: fieldPath.equalsignorecase(owner_tid__c): ' + fieldPath.equalsignorecase('owner_tid__c'));                    
                                if(fieldPath.equalsignorecase('product_external_id__c')){
                                    insertBulkOrderLoadRecord.put('SR_Product__c',(string.isblank(bulkOrderLoadRecordField)?null:productIdMap.get(decimal.valueof(bulkOrderLoadRecordField))));
                                }else if(fieldPath.equalsignorecase('owner_tid__c')){
                                    insertBulkOrderLoadRecord.put('SR_Owner__c',(string.isblank(bulkOrderLoadRecordField)?null:userTIDMap.get(bulkOrderLoadRecordField)));
                                }
                            }else{
                                insertBulkOrderLoadRecord.put(bulkOrderLoadRecordFieldsMap.get(fieldPath),null);
                            }
                        }
                    }
                    customerRequestDateDayOfMonth=customerRequestDateMonth=customerRequestDateYear=-1;
                    if(insertBulkOrderLoadRecord.get('Cust_Req_Due_Dt_Day__c')!=null&&insertBulkOrderLoadRecord.get('Cust_Req_Due_Dt_Month__c')!=null&&insertBulkOrderLoadRecord.get('Cust_Req_Due_Dt_Year__c')!=null){
                        try{
                            customerRequestDateYear=integer.valueof(insertBulkOrderLoadRecord.get('Cust_Req_Due_Dt_Year__c'));
                            customerRequestDateMonth=integer.valueof(insertBulkOrderLoadRecord.get('Cust_Req_Due_Dt_Month__c'));
                            customerRequestDateDayOfMonth=integer.valueof(insertBulkOrderLoadRecord.get('Cust_Req_Due_Dt_Day__c'));
                            insertBulkOrderLoadRecord.put('SR_Customer_Req_Date__c',Date.newinstance(customerRequestDateYear,customerRequestDateMonth,customerRequestDateDayOfMonth));
                        }catch(exception customerRequestedDateException){}
                    }
                    insertBulkOrderLoadRecord.put('Target_Date_Type__c',label.Prequal_Requested);
                }catch(exception e){
                    //TODO May need to review exception message would be user-friendly enough for users to take appropriate action.
                    insertBulkOrderLoadRecord.put('System_Message__c',e.getmessage()+'\n\n'+e.getstacktracestring().left(32678));
                    insertBulkOrderLoadRecord.put('Status__c',label.Cannot_Create_SR);
                    hasMappingError=true;
                    failProcessingBulkOrderLoad(e);
                }
                insertBulkOrderLoadRecord.status__c=label.Ready_to_Create_SR;
                
                // ensures any text fields in record that are encased in quotes will have them stripped out
				cleanBulkLoadRecordFields(insertBulkOrderLoadRecord);
    			// add
    			insertBulkOrderLoadRecords.add(insertBulkOrderLoadRecord);
            }
        }
        //*************************************************************
        // All Functions below will be executed based on the
        // mapped Bulk Order Load Records because fields are
        // properly mapped and truncated.
        // 
        // NOTE: However the following will not be executed as
        // long as there is any error in data mapping.
        //
        // PAC - Address Validation and Add New Service Address
        //
        // Upon successful of the above two, insert new Service Requests
        //**************************************************************
        /*if(!hasMappingError){
            map<string,schema.recordtypeinfo> logRecordTypeMap=webservice_integration_error_log__c.sobjecttype.getdescribe().getrecordtypeinfosbyname();
            list<webservice_integration_error_log__c> logs=new list<webservice_integration_error_log__c>();
            SRS_ASFService.ServiceAddressManagementServicePort asfPort;
            SRS_ASFValidationReqResponse.ServiceAddress address;
            SRS_ASFValidationReqResponse.searchServiceAddressResponse_element respSearch;
            SRS_ASFValidationReqResponse.getServiceAddressDetailsResponse_element getServiceAddressResp;
            long startTime=0;
            long endTime=0;
            user currentUser=[select id,team_telus_id__c from user where id=:userinfo.getuserid()];
            for(bulk_order_load_record__c insertBulkOrderLoadRecord:insertBulkOrderLoadRecords){
                webservice_integration_error_log__c log=new webservice_integration_error_log__c(recordtypeid=logRecordTypeMap.get(label.Webservice_Integration_Log_Success).getrecordtypeid(),
                                                                                               apex_class_and_method__c='BulkOrderLoadRecordConverter.processBulkOrderLoadRecords',
                                                                                               object_name__c='bulk_order_load_record__c',
                                                                                               status__c='In Progress',
                                                                                               webservice_name__c='SRS_ASFService.ServiceAddressManagementServicePort.searchServiceAddress');
                try{
                    asfPort=new SRS_ASFService.ServiceAddressManagementServicePort();
                    address=new SRS_ASFValidationReqResponse.ServiceAddress();
                    respSearch = new SRS_ASFValidationReqResponse.searchServiceAddressResponse_element();
                    getServiceAddressResp = new SRS_ASFValidationReqResponse.getServiceAddressDetailsResponse_element();
                    address.countryCode=insertBulkOrderLoadRecord.country__c;
                    address.municipalityName=insertBulkOrderLoadRecord.city__c;
                    address.provinceStateCode=insertBulkOrderLoadRecord.province__c;
                    address.streetName=insertBulkOrderLoadRecord.address__c;
                    address.streetNumber='';       
                    address.unitNumber='';
                    address.vector='';
                    log.request_payload__c='Address:'+address.streetName+', City:'+address.municipalityName+', Province:'+address.provinceStateCode+', Country:'+address.countryCode;
                    startTime=system.now().gettime();
                    respSearch = asfPort.searchServiceAddress((string.isnotblank(currentUser.team_telus_id__c)?currentUser.team_telus_id__c:'x139294'),system.now(),address,10,'','','','');
                    endTime=system.now().gettime();
                    SRS_ASFValidationReqResponse.ServiceAddress respaddress  = new SRS_ASFValidationReqResponse.ServiceAddress();  
                    SRS_ASFValidationReqResponse.LikeStreetList respLikeStreet = new SRS_ASFValidationReqResponse.LikeStreetList();
                    SRS_ASFValidationReqResponse.likeMunicipalityList respLikeMunicipality = new SRS_ASFValidationReqResponse.likeMunicipalityList();
                    SRS_ASFValidationReqResponse.likeHouseList respLikeHouse = new SRS_ASFValidationReqResponse.likeHouseList();
                    SRS_ASFValidationReqResponse.likeApartmentList respLikeApartment = new SRS_ASFValidationReqResponse.likeApartmentList();
                    respLikeStreet = respSearch.likeStreetList;
                    respLikeMunicipality = respSearch.likeMunicipalityList;
                    respLikeHouse = respSearch.likeHouseList;
                    respLikeApartment = respSearch.likeApartmentList;
                    respaddress = respSearch.address;
                    // If exact match found, updates Status to Valid        
                    if((respAddress!=NULL)&&(respLikeStreet==NULL)&&(respLikeMunicipality==NULL)&&(respLikeApartment==NULL)&&(respLikeHouse==NULL)){
                        insertBulkOrderLoadRecord.status__c='Ready to Create SR';
                        insertBulkOrderLoadRecord.fms_address_id__c=respaddress.addressId;
                        insertBulkOrderLoadRecord.postal_code__c=string.isnotblank(respaddress.postalZipCode)?respaddress.postalZipCode:insertBulkOrderLoadRecord.postal_code__c;
                    }else{
                        insertBulkOrderLoadRecord.status__c=null;
                    }
                    log.status__c='Passed';
                }catch(calloutexception ce){
                    endTime=system.now().gettime();
                    log.status__c='Failed';
                    log.recordtypeid=logRecordTypeMap.get(label.Webservice_Integration_Log_Failure).getrecordtypeid();
                    log.stack_trace__c=ce.getstacktracestring().left(32768);
                    insertBulkOrderLoadRecord.status__c='Cannot Create SR';
                    insertBulkOrderLoadRecord.put('System_Message__c',ce.getmessage()+'\n\n'+ce.getmessage().left(32678));
                    hasMappingError=true;
                }catch(exception e){
                    if(endTime==0){
                        endTime=system.now().gettime();
                    }
                    log.status__c='Failed';
                    log.stack_trace__c=e.getstacktracestring().left(32768);
                    log.recordtypeid=logRecordTypeMap.get(label.Webservice_Integration_Log_Failure).getrecordtypeid();
                    insertBulkOrderLoadRecord.status__c='Cannot Create SR';
                    insertBulkOrderLoadRecord.put('System_Message__c',e.getmessage()+'\n\n'+e.getmessage().left(32678));
                    hasMappingError=true;
                }finally{
                    log.time_span__c=endTime-startTime;
                }
                logs.add(log);
                startTime=0;
                endTime=0;
                //TODO - Successful PAC, Create Service Request
            }
            Util.logWebserviceAsync(logs);
        }*/
        // Regardless of PAC is executed or not, all Bulk Order Load Records
        // should still be inserted
        savepoint sp=database.setsavepoint();
        try{
System.debug('Bulkload: insertBulkOrderLoadRecords: ' + insertBulkOrderLoadRecords);            
            insert insertBulkOrderLoadRecords;
        }catch(exception e){
            database.rollback(sp);
            //Retry by putting itself back to the queue.
            if(e.getmessage().containsignorecase(label.unable_to_obtain_exclusive_access)){
                retry();
            }
        }
    }
    public void failProcessingBulkOrderLoad(exception failedException){
        //TODO retrieve Bulk Order Load File and update the failed reason field
        bulkOrderLoadRecordRows=null;
        try{
            if(bulkOrderLoadFile!=null){
                bulkOrderLoadFile.submission_status__c=label.New;
                bulkOrderLoadFile.submission_error__c=failedException.getmessage()+'\n\n'+failedException.getstacktracestring().left(32768);
                //TODO Update with error message of why it failed
                update bulkOrderLoadFile;
            }
        }catch(dmlexception dmle){
            //TODO If unable to update Bulk Order Load File, there has to be permission or other issue that CRM Admin could handle
            Util.emailSystemError(dmle);
            system.debug(dmle.getstacktracestring());
        }catch(exception e){
            //TODO Other exceptions, just notify Admin with stack trace
            Util.emailSystemError(e);
            system.debug(e.getstacktracestring());
        }
    }
    
    private static void cleanBulkLoadRecordFields(bulk_order_load_record__c blr){
        Map<String, Schema.SObjectField> schemaFieldMap = Schema.SObjectType.bulk_order_load_record__c.fields.getMap();
        
        Set<String> fieldsToClean = new Set<String>{
            'additional_service_details__c', 
            'city__c', 
            'province__c', 
            'postal_code__c', 
            'country__c', 
            'address_request_notes__c', 
            'address__c'
        };
            
        for(String fieldKey : schemaFieldMap.keyset()){
            if(fieldsToClean.contains(fieldKey)){
	            String value = (String)blr.get(fieldKey);

                if(value != null){                    
                    value = value.replaceAll('"', '');
                    
                    blr.put(fieldKey, value);
                }		                
            }
        }
    }
}