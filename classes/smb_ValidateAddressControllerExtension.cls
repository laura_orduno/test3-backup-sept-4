global class smb_ValidateAddressControllerExtension {

	@RemoteAction
	global static smb_AddressValidationUtility.ValidateAddressResponse validateAddress(smb_AddressValidationUtility.Address address) {
		return smb_AddressValidationUtility.validate(address);	
	}
	
	global smb_ValidateAddressControllerExtension(smb_NewAccountController controller) {}

}