global class Frontline {

    webservice static Case createCase(String subject, String description, String sfdcAccountId, String lynxTicketNo) {        
        return FrontlineHelper.createCase(subject, description, sfdcAccountId, lynxTicketNo);
    }
}