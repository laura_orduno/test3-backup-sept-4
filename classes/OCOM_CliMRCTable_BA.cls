global with sharing class OCOM_CliMRCTable_BA implements vlocity_cmt.VlocityOpenInterface {

  public Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {
        Boolean success = true;
        if(methodName == 'buildDocumentSectionContent')
        { 
             Id contractId = (Id) inputMap.get('contextObjId');
            map<string,object> mapTableInfo = OCOM_DynamicTableHelper_BA.OCOM_getCliMRCData(contractId);
                       
            if(mapTableInfo.size() > 0 && !mapTableInfo.isEmpty()){
                inputMap.putall(mapTableInfo);
                map<string,object> outputMap =  OCOM_DynamicTableHelper_BA.buildDocumentSectionContent(inputMap,outMap, options);
                outMap.putAll(outputMap);
            }
            else {
                return false;
            }
                
         
             }       
          return success; 
  }

}