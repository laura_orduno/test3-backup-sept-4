/*****************************************************
    Apex: OpportunityTeamWrapper
    Usage: This class used for Opportunity Team as Opportunity
    Author – Clear Task
    Date – 06/11/2011
    Revision History
******************************************************/
public with sharing class OpportunityTeamWrapper {
    public Boolean checked{get; set;}
    public OpportunityTeamMember oppTeam{get; set;}    
    public OpportunityTeamWrapper(OpportunityTeamMember a){
        oppTeam = a;
        checked = false;
    } 
}