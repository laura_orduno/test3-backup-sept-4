/********************************************************************************************************************************
Class Name:     OCOM_AdditionalDetailsCntrl 
Purpose:        This class is a controller class for OCOM_AdditionalDetails VF .      
Test class Name : OCOM_AdditionalDetailsCntrl_test
Created by:     Adharsh	Fnu	       
Modified by:    Aditya Jamwal      16-Feb-2016      Defect# 10518 : Contact Name, Contact Number and Contact Email not displayed for WLS devices.
													Defect# 10516 : Contact Number and Email not visible in NC for TELUS SIM Card.
*********************************************************************************************************************************
*/
public class OCOM_AdditionalDetailsCntrl {
    public Order objOrder{get;set;}
    public string orderId{get;set;}
    public list<string> filter ;
    public boolean arecontractable{get;set;}
    public static Map<String,String> canadaPostValMap {get;set;}
    public static boolean isCPValdationComplete = false;
    public static boolean isShippableProductPresent{get;set;}
    
    public OCOM_AdditionalDetailsCntrl(){
        system.debug('%%######%% START: OCOM_AdditionalDetailsCntrl:OCOM_AdditionalDetailsCntrl  %%######%% ');
        objOrder=new Order();    
        orderId=ApexPages.currentPage().getparameters().get('id');
        objOrder = getOrderDetail();
        arecontractable=false;
        isShippableProductPresent=false;
        system.debug('%%######%% END: OCOM_AdditionalDetailsCntrl:OCOM_AdditionalDetailsCntrl %%######%% ');
    }
    
    //Start : Added by Aditya Jamwal (IBM) to validate Shipping Address from Canada Post....  
    public void doShippingAddrValidation(){   
        system.debug('%%######%% START: OCOM_AdditionalDetailsCntrl:doShippingAddrValidation  %%######%% ');
        If(!isCPValdationComplete){  
            canadaPostValMap  = new Map<String,String>();
            system.debug('!!!Shipping Address '+orderId);
            List<OrderItem> ordrItemRecs;
            List<OrderItem> updateOrdrItemRecs = new List<OrderItem> ();
            try{
                ordrItemRecs =[select id,order.CustomerAuthorizedBy.Name,Shipping_Address_Contact__c,pricebookentry.product2.OCOM_Shippable__c
                               ,order.CustomerAuthorizedBy.Email,Shipping_Address__c,order.Service_Address__r.city__c,order.Service_Address__r.province__c
                               ,order.ServiceAddressSummary__c,order.CustomerAuthorizedBy.Phone,Shipping_Contact_Email__c,Shipping_Contact_Number__c
                               ,orderId,vlocity_cmt__JSONAttribute__c,order.Service_Address__r.Street__c,order.Service_Address__r.Building_Number__c
                               ,order.Service_Address__r.Postal_Code__c from OrderItem where orderId =:orderId];
            }catch(exception ex){
                system.debug('!!!Shipping Address '+ex);
            }    
            system.debug('!!!Shipping Address ordrItemRecs '+ordrItemRecs);
            OCOM_ShippingAddrCPValdationUtility.serviceAddressData saData = new OCOM_ShippingAddrCPValdationUtility.serviceAddressData();        
            if(null !=ordrItemRecs  &&ordrItemRecs.size()>0){
                String status=' ';
                Map<String,String> charactersticValueMap = new  Map<String,String>();
                if(null!=ordrItemRecs[0].order.Service_Address__c){         
                    if(null !=OrdrConstants.CHAR_SHIPPING_ADDRESS_CHARACTERISTIC){charactersticValueMap.put(OrdrConstants.CHAR_SHIPPING_ADDRESS_CHARACTERISTIC,ordrItemRecs[0].order.ServiceAddressSummary__c);}
                    if(null !=OrdrConstants.CHAR_SHIPPING_ADDRESS){charactersticValueMap.put(OrdrConstants.CHAR_SHIPPING_ADDRESS,ordrItemRecs[0].order.ServiceAddressSummary__c);}
                    if(null !=OrdrConstants.CHAR_CITY_OF_USE){charactersticValueMap.put(OrdrConstants.CHAR_CITY_OF_USE,ordrItemRecs[0].order.Service_Address__r.city__c);}
                    if(null !=OrdrConstants.CHAR_PROVINCE_OF_USE){charactersticValueMap.put(OrdrConstants.CHAR_PROVINCE_OF_USE,ordrItemRecs[0].order.Service_Address__r.province__c);}
                    saData.postalCode=ordrItemRecs[0].order.Service_Address__r.Postal_Code__c ; saData.streetName=ordrItemRecs[0].order.Service_Address__r.Street__c; saData.buildingNumber=ordrItemRecs[0].order.Service_Address__r.Building_Number__c;
                    try{ 
                        status= OCOM_ShippingAddrCPValdationUtility.validateAddress(saData);
                    }catch(exception ex){
                        system.debug('!!!Shipping Address '+ex);
                    }   
                }  if(Test.isRunningTest()){status='valid'; }
                boolean isContactNeeded=false;boolean isShippingAddressNeeded=false;
                for(orderItem oli :ordrItemRecs ){           
                    String JsonChar =oli.vlocity_cmt__JSONAttribute__c; 
                    Boolean isShippableCharInJSON = false;
                    Map<String,OCOM_VlocityJSONParseUtil.JSONWrapper> attributesMap = OCOM_VlocityJSONParseUtil.getAttributesFromJSON(JsonChar);
                    if(null != attributesMap && null!= OrdrConstants.CHAR_SHIPPING_REQUIRED && null != attributesMap.get(OrdrConstants.CHAR_SHIPPING_REQUIRED) && String.isNotBlank((attributesMap.get(OrdrConstants.CHAR_SHIPPING_REQUIRED)).value)
                       && (attributesMap.get(OrdrConstants.CHAR_SHIPPING_REQUIRED)).value.equalsIgnoreCase(OrdrConstants.CHAR_VALUE_YES)){
                           isShippableCharInJSON= true;
                    }
                    if(oli.pricebookentry.product2.OCOM_Shippable__c == OrdrConstants.CHAR_VALUE_YES || isShippableCharInJSON){
                        isShippableProductPresent=true;
                        system.debug('!!!!oli'+status+' '+oli.Shipping_Address__c);   
                        
                        if(String.isBlank(oli.Shipping_Address__c) && status=='valid'){                 
                            oli.Shipping_Address__c=oli.order.ServiceAddressSummary__c;                    
                            system.debug('!!!!debug before charactersticValueMap'+charactersticValueMap); 
                            
                            isShippingAddressNeeded =true; 
                        }else { 
                            charactersticValueMap.clear();
                        } 
                        if(null!= oli.order.CustomerAuthorizedBy && ( (String.isNotBlank(oli.order.CustomerAuthorizedBy.Name) && String.isBlank(oli.Shipping_Address_Contact__c))
                                                                    ||(String.isNotBlank(oli.order.CustomerAuthorizedBy.Email) && String.isBlank(oli.Shipping_Contact_Email__c))
                                                                    ||(String.isNotBlank(oli.order.CustomerAuthorizedBy.Phone) && String.isBlank(oli.Shipping_Contact_Number__c)))){
                            oli.Shipping_Address_Contact__c = oli.order.CustomerAuthorizedBy.Name;
                            if(null !=OrdrConstants.CHAR_SHIPPING_CONTACT_NAME){charactersticValueMap.put(OrdrConstants.CHAR_SHIPPING_CONTACT_NAME,oli.order.CustomerAuthorizedBy.Name);}                       
                            //Start    Aditya Jamwal      16-Feb-2016      Defect# 10516,10518
                            oli.Shipping_Contact_Email__c = oli.order.CustomerAuthorizedBy.Email;
                            oli.Shipping_Contact_Number__c = oli.order.CustomerAuthorizedBy.Phone;
                            if(null !=OrdrConstants.CHAR_SHIPPING_CONTACT_EMAIL){charactersticValueMap.put(OrdrConstants.CHAR_SHIPPING_CONTACT_EMAIL,oli.order.CustomerAuthorizedBy.Email);}                       
                            if(null !=OrdrConstants.CHAR_SHIPPING_CONTACT_NUMBER){charactersticValueMap.put(OrdrConstants.CHAR_SHIPPING_CONTACT_NUMBER,oli.order.CustomerAuthorizedBy.Phone);}                       
                            //End    Aditya Jamwal      16-Feb-2016      Defect# 10516,10518
                            isContactNeeded=true;
                          system.debug('!!@@ Shipping'+oli);                                              
                        } 
                        if(isShippingAddressNeeded || isContactNeeded){
                            try{ 
                                JsonChar = OCOM_VlocityJSONParseUtil.setAttributesInJSON(JsonChar,charactersticValueMap);
                            }catch(exception ex){
                                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,Label.OCOM_CaptureShippingDetailsFailed));
                            }
                            if(String.IsNotBlank(JsonChar)){                     
                                oli.vlocity_cmt__JSONAttribute__c= JsonChar;
                                updateOrdrItemRecs.add(oli);
                            }                    
                        }  
                        isShippingAddressNeeded = isContactNeeded =false;
                    //Start    Aditya Jamwal      16-Feb-2016      Defect# 10516,10518
                    }else if(null != attributesMap && null!= OrdrConstants.CHAR_SHIPPING_REQUIRED && null != attributesMap.get(OrdrConstants.CHAR_SHIPPING_REQUIRED) && String.isNotBlank((attributesMap.get(OrdrConstants.CHAR_SHIPPING_REQUIRED)).value)
                             && (attributesMap.get(OrdrConstants.CHAR_SHIPPING_REQUIRED)).value.equalsIgnoreCase(OrdrConstants.CHAR_VALUE_NO)){   
                          JsonChar =  OCOM_VlocityJSONParseUtil.setAttributesInJSON(JsonChar,new Map<String,String>{OrdrConstants.CHAR_SHIPPING_ADDRESS_CHARACTERISTIC => ' ',OrdrConstants.CHAR_SHIPPING_ADDRESS => ' ',OrdrConstants.CHAR_CITY_OF_USE => ' ',OrdrConstants.CHAR_PROVINCE_OF_USE => ' ',OrdrConstants.CHAR_SHIPPING_CONTACT_NAME => ' '});
                         if(String.IsNotBlank(JsonChar)){                     
                              oli.vlocity_cmt__JSONAttribute__c= JsonChar;
                              oli.Shipping_Address_Contact__c = '';
                              oli.Shipping_Address__c= '';
                              updateOrdrItemRecs.add(oli);
                         }     
                     }
                    //End    Aditya Jamwal      16-Feb-2016      Defect# 10516,10518
                    canadaPostValMap.put(oli.order.ServiceAddressSummary__c,status);
                } 
                update updateOrdrItemRecs;
            } 
        } 
        system.debug(' canadaPostValMap '+canadaPostValMap);
        system.debug('%%######%% END: OCOM_AdditionalDetailsCntrl:doShippingAddrValidation  %%######%% ');    
    }
    
    //End: Added by Aditya Jamwal (IBM) to validate Shipping Address from Canada Post....    
    
    public Order getOrderDetail(){
        system.debug('%%######%% START: OCOM_AdditionalDetailsCntrl:getOrderDetail  %%######%% ');
        try {
            if(filter ==null && orderId != null){
                //System.debug('Filter: ' + filter + '; rel: ' + relFieldName + '; recId: ' + orderId);
                
                filter = new List<String>{'id' + '=\''+ orderId +'\'' };
                    }
            System.debug('Filter: ' + filter + '; rel: ' + 'id' + '; recId: ' + orderId);
            List<string> fieldNames = new List<string>{'id','ContactEmail__c',
                'ContactName__c',
                'ContactPhone__c','ContactTitle__c','DeliveryMethod__c',
                'Service_Address_Text__c','Account.Id','AccountId',
                'CustomerAuthorizedByID','CustomerAuthorizedBy.Name','CustomerSignedBy__c',
                'CustomerAuthorizedBy.Email','CustomerAuthorizedBy.Phone','CustomerAuthorizedBy.Title',
                'CustomerSignedBy__r.Name','CustomerSignedBy__r.Email','CustomerSignedBy__r.Phone','CustomerSignedBy__r.Title'};
                    System.debug('getting query');
            String query = Util.queryBuilder('Order', fieldNames, filter);
            System.debug(LoggingLevel.Error,'query :' + query);
            list<order> order = Database.query(query);
            if(order.size() > 0 && !order.isEmpty() )
                objOrder = order[0];
        }catch(Exception anException){
            system.debug('OCOM_AdditionalDetails::GetOrderDetails() exception(' + anException + ')');
            throw anException;
            return null; 
        }
        system.debug('%%######%% END: OCOM_AdditionalDetailsCntrl:getOrderDetail  %%######%% ');
        return objOrder;
    }
    
}