/**
  @author Vlocity Implements GetItemsListInterface
  @version Jan 27 2017
*/


global with sharing class OCOM_GetContractLineItems implements vlocity_cmt.VlocityOpenInterface{
    private static string CONTRACTOBJ = 'Contracts__c';
    private static string DEALSUBPPORTOBJ = 'Offer_House_Demand__c';
    private static string ORDER = 'Order';
    
  public Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {
  Boolean success = true;
   
  if(methodName == 'getItemsList')
  { 
    getItemsList(inputMap, outMap, options); 

  }
   
  return success; 
  }
   
    private void getItemsList (Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options){
        String objectName;  
        map<id,schema.recordtypeinfo> contractReqRecordTypeMap = Contracts__c.sobjecttype.getdescribe().getrecordtypeinfosbyid();
        
        Id contextId = (Id) inputMap.get('contextObjId');

        Schema.SObjectType contextObjType =  Id.valueOf(contextId).getSObjectType();
       String contextObjName = contextObjType.getDescribe().getName();
        
        system.debug('contextObjName::'+ contextObjName);
        List<SObject> items = new List<SObject> ();
         
        if(CONTRACTOBJ.equalsignoreCase(contextObjName)){
           List<Contracts__c> conReqList = [SELECT ID,recordtypeid FROM Contracts__c where ID =: contextId];
           if(conReqList != null && conReqList.size() > 0 ){
              Contracts__c conReqObj = conReqList[0];
            if(contractReqRecordTypeMap.get(conReqObj.recordtypeid).getname().equalsignorecase('CPQ Contract') || contractReqRecordTypeMap.get(conReqObj.recordtypeid).getname().equalsignorecase('CPQ Legacy Contract')) {
                items = [Select Id from OrderItem where Contract_Request__c =:contextId  ];
            }
            else{
                  /* 2017 Dec - Mark H - The new structure for CRs is to have CLIs as a related list. Need to support new and old structure */
                  items = [Select Id, Name from eContract_Line_Item__c  where Contract_Request__c =:contextId];
                  if (items.size() == 0) {
                      /* old structure */
                      items = [Select Id, Name from Service_Request__c  where Contract__c =:contextId];
                  }
                }
            }
        }
        else if(DEALSUBPPORTOBJ.equalsignoreCase(contextObjName)) {
            items = [Select Id, Name from Rate_Plan_Item__c where Deal_Support__c =:contextId];
        }  
        else if(ORDER.equalsignoreCase(contextObjName)) {
            items = [Select Id from OrderItem where OrderId =:contextId];
        } 
        outMap.put('itemsList', items);
               
    } 
}