/********************************************************************************************************************************
Class Name:     OCOM_ShippingAddrCPValdationUtility 
Purpose:        This is a utility class to validate shipping address with CANADA Post's rest service.     
Created by:     Aditya Jamwal      26-August-2016 
*********************************************************************************************************************************
*/
public class OCOM_ShippingAddrCPValdationUtility {
static  string CanadaPostKey; 
    static  string findPreURl; 
    static  string findPostURl;
    static  string reteriveURl;
    
    private static boolean setURLData(){
        PACConfig__c custData= PACConfig__c.getValues('shipping');
        if(null!=custData){
            CanadaPostKey = custData.API_Key__c;
            findPreURl =  (custData.End_Point_URL__c.remove('callback=?'))+'&Key='+CanadaPostKey+'&SearchTerm=';
            findPostURl = '&LastId=&SearchFor=Everything&Country=CAN&LanguagePreference=en&MaxSuggestions=0&MaxResults=1&OrderBy=UserLocation';
            reteriveURl = (custData.Shipping_Utility_Second_URL__c.remove('callback=?'))+'&Key='+CanadaPostKey+'&Id=';
            return true;
        }   
        return false;    
    }    
    public static String validateAddress(serviceAddressData serDataObj){  
        if(setURLData()) {
            system.debug('CanadaPostKey ' +CanadaPostKey);  
            system.debug('findPreURl ' +findPreURl); 
            system.debug('findPostURl ' +findPostURl); 
            system.debug('reteriveURl ' +reteriveURl); 
            string searchItem=EncodingUtil.urlEncode(serDataObj.postalCode+' '+serDataObj.buildingNumber+' '+serDataObj.streetName,'UTF-8');
            String url = findPreURl +searchItem+findPostURl ;   
            // do Find Call for detail Address for comparision  
            HTTPResponse resp = sendRequest(url);
            CPAutoCompleteWrapper autoCompletCallResult= new CPAutoCompleteWrapper();
            system.debug('!!!!HTTPResponse 0 '+resp.getBody()); 
            try{
                if(resp!=null && String.isNotBlank(resp.getBody())){
                    String respJson = resp.getBody();
                    system.debug('!!!!respJson '+respJson); 
                    autoCompletCallResult = autoCompletCallResult.parse(respJson); 
                    List<CPAutoCompleteItemWrapper> autoCompItemList = new List<CPAutoCompleteItemWrapper>();
                    autoCompItemList =autoCompletCallResult.Items;
                    system.debug('!!!!HTTPResponse 1 '+ autoCompItemList);
                    if(null !=autoCompItemList && autoCompItemList.size()>0){
                        string id=EncodingUtil.urlEncode(autoCompItemList[0].id,'UTF-8');
                        // doReterive Call for detail Address for comparision  
                        HTTPResponse respDetail = sendRequest(reteriveURl+id);
                        if(respDetail !=null && String.isNotBlank(respDetail.getBody())){ 
                            String respDetailJson = respDetail.getBody();
                            system.debug('!!!!HTTPResponse 10 '+ respDetailJson); 
                            CPAddressdetail detailObj = new CPAddressdetail();
                            detailObj=detailObj.parse(respDetailJson);
                            for(CPAddressdetailItems itm: detailObj.Items){
                                if (itm.Language=='ENG'){
                                    system.debug('!!!!HTTPResponse 10 '+ itm);   
                                    if(compareAddress(serDataObj,itm)){
                                        system.debug('Compare result '+compareAddress(serDataObj,itm));
                                        return 'Valid';
                                    }else{
                                        system.debug('Compare result '+compareAddress(serDataObj,itm));
                                        return 'Invalid';
                                    }                    
                                }                
                            }   
                            return 'Invalid';
                        }
                    }  
                    return 'Please validate the address Manually.';
                } 
                return 'Please validate the address Manually.';
            }catch(exception e){            
                system.debug('exception !!! '+e);
                return 'Please validate the address Manually.';
            }
        }else  {
            return 'Please validate the address Manually.';
        }
    }
    
    public class serviceAddressData{
        public  String postalCode;
        public   String streetName;
        public   String buildingNumber;
        public    String province;
        public    String city;
        public serviceAddressData(){}
        
    }
    
    private static HTTPResponse sendRequest(String url){
        HttpRequest req = new HttpRequest();
        Http http = new Http();
        // set the request method
        req.setMethod('GET');
        req.setEndpoint(url);
        HTTPResponse resp;         
       	resp = http.send(req);
        system.debug('!!!resp.getBody() '+resp.getBody());
     
    return resp;
   
   }
    private static boolean compareAddress(serviceAddressData serviceAddrObj,CPAddressdetailItems cpObj){
        boolean matchFound=false;
        if(null !=serviceAddrObj && null !=cpObj){
            if(String.isNotBlank(serviceAddrObj.postalCode) && String.isNotBlank(cpObj.PostalCode) && (serviceAddrObj.postalCode.deleteWhitespace() == cpObj.PostalCode.deleteWhitespace())){
                if(String.isNotBlank(serviceAddrObj.buildingNumber) && String.isNotBlank(cpObj.BuildingNumber) && (serviceAddrObj.buildingNumber.deleteWhitespace() == cpObj.BuildingNumber.deleteWhitespace())){
                    if(String.isNotBlank(serviceAddrObj.streetName) && String.isNotBlank(cpObj.Street) && (serviceAddrObj.streetName.deleteWhitespace() == cpObj.Street.deleteWhitespace())){
                        matchFound=true;  
                    }
                }                
            }            
        } 
        return matchFound;  
    }
    class CPAutoCompleteItemWrapper {
        public String Id;   //CAN|A|15343289
        public String Text; //343 Ch D'entrelacs, Ste-Marguerite-Du-Lac-Masson, QC
        public String Highlight;    //
        public Integer Cursor;  //0
        public String Description;  //
        public String Next; //Retrieve
    }
    public class CPAutoCompleteWrapper{
        public CPAutoCompleteItemWrapper[] Items;
        public CPAutoCompleteWrapper parse(String json){
            return (CPAutoCompleteWrapper) System.JSON.deserialize(json, CPAutoCompleteWrapper.class);
        }    
    }
    
    public class CPAddressdetail{
        public CPAddressdetailItems[] Items;
        //
        public CPAddressdetail parse(String json){
            return (CPAddressdetail) System.JSON.deserialize(json, CPAddressdetail.class);
        }
    }
    
    
    class CPAddressdetailItems {
        public String Id;   //CA|CP|A|6978053
        public String DomesticId;   //6978053
        public String Language; //ENG
        public String LanguageAlternatives; //ENG,FRE
        public String Department;   //
        public String Company;  //
        public String SubBuilding;  //
        public String BuildingNumber;   //474
        public String BuildingName; //
        public String SecondaryStreet;  //
        public String Street;   //Tenth Ave E
        public String Block;    //
        public String Neighbourhood;    //
        public String District; //
        public String City; //New Westminster
        public String Line1;    //474 Tenth Ave E
        public String Line2;    //
        public String Line3;    //
        public String Line4;    //
        public String Line5;    //
        public String AdminAreaName;    //
        public String AdminAreaCode;    //
        public String Province; //BC
        public String ProvinceName; //British Columbia
        public String ProvinceCode; //BC
        public String PostalCode;   //V3L 4R8
        public String CountryName;  //Canada
        public String CountryIso2;  //CA
        public String CountryIso3;  //CAN
        public Integer CountryIsoNumber;    //124
        public String SortingNumber1;   //
        public String SortingNumber2;   //
        public String Barcode;  //
        public String POBoxNumber;  //
        public String Label;    //474 Tenth Ave E
        public String Type; //Residential
        public String DataLevel;    //Premise
        
    }
}