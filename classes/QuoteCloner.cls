/*
    Quote Cloner
    QuoteCloner.cls
    Part of the TELUS QuoteQuickly Portal
    
    Author: Alex Miller @ Traction <amiller@tractionondemand.com>
    Created: March 5, 2012
    
    Since: Release 5
    Initial Definition: R5rq2
    
    Known Issues:
    1. Quote Groups are not modified or cloned. If a quote is part of a group, the cloned quote will also be a member of that group [GH Issue 2]
    2. The related opportunity is not cloned. The source and destination quotes both point to the same opportunity [GH Issue 1]
    
    Things to Work on:
     - modularizing the doClone() method. currently a lot (pretty much all) of the cloning logic is locked inside.
            It may be beneficial to move some pieces to more methods.
            

    Outstanding Questions:
    1. Should the Sales Rep field on the destination quote be changed to the current user?
    2. How should the tool handle quote groups? It is possible to extend it slightly to automatically clone & rehome all quotes in a group
        Note: Price change messages may be a mess with this.
    3. Should a new opportunity be created for the new quote? Or is it acceptable to have the cloned quote point to the same opportunity as the source quote?

    New Field Audit:
     - Object -          - API Name -            - Label -           - Type -                    - Since -
    SBQQ__Quote__c      Source_Quote__c         Source Quote        Lookup(SBQQ__Quote__c)      March 8, 2012
    
*/
public without sharing class QuoteCloner {
    private system.savepoint rollbackSp {get; set;}
    public Boolean success {get; private set;}
    private boolean debug = false;
    
    // Quote Variables
    public String newQuoteId {get; private set;}
    public String sourceQuoteId {get; private set;}
    private SBQQ__Quote__c sourceQuote;
    private SBQQ__Quote__c destQuote;
    
    // Opportunity Variables
    /*  Currently not in use.
    private Opportunity sourceOpp;
    private Opportunity destOpp; */
    
    // Quote Line Variables
    private Map<Id, SBQQ__QuoteLine__c> sourceQuoteLines;
    private Map<Id, SBQQ__QuoteLine__c> sourceToDestQuoteLines;
    private Map<Id, Id> sourceToDestQuoteLineIds;
    private Map<Id, SBQQ__QuoteLine__c> destQuoteLines;
    private Map<Id, Boolean> sourceQuoteLinesThatFailedValidationAndWereNotCloned;
    
    // Additional Information Variables
    private Map<Id, AdditionalInformation__c> sourceAdditionalInformations;
    private Map<Id, AdditionalInformation__c> sourceToDestAdditionalInformations;
    private Map<Id, Id> sourceToDestAddlInfoIds;
    private Map<Id, AdditionalInformation__c> destAdditionalInformations;
    private Map<Id, Boolean> sourceAdditionalInformationsThatFailedValidationAndWereNotCloned;
    private Map<Id, Id> destQuoteLineToDestAdditionalInformations;
    
    // Product Variables
    private List<Id> productIds;
    private Map<Id, Product2> products;
    
    // Messaging Variables
    private String message {get; set;}
    public string messages {get{
        return EncodingUtil.urlEncode(message, 'UTF-8');
    }}
    public String messagestring {get; private set;}
    public String[] pageMessages {get; private set;}
    private integer lastMessageProcessed = 0;

    private void init() {
        try {
        pageMessages = new List<String>();
        sourceToDestQuoteLines = new Map<Id, SBQQ__QuoteLine__c>();
        destQuoteLines = new Map<Id, SBQQ__QuoteLine__c>();
        sourceToDestQuoteLineIds = new Map<Id, Id>();
        sourceQuoteLinesThatFailedValidationAndWereNotCloned = new Map<Id, Boolean>();
        
        sourceToDestAdditionalInformations = new Map<Id, AdditionalInformation__c>();
        destAdditionalInformations = new Map<Id, AdditionalInformation__c>();
        sourceToDestAddlInfoIds = new Map<Id, Id>();
        sourceAdditionalInformationsThatFailedValidationAndWereNotCloned = new Map<Id, Boolean>();
        destQuoteLineToDestAdditionalInformations = new Map<Id, Id>();
        productIds = new List<Id>();
        message = '';
        messagestring = '';
        
        setSavepoint();
        } catch (Exception e) { QuoteUtilities.log(e); }
    }

    public QuoteCloner() {init();}
    public QuoteCloner(String quoteId) {
        init();
        sourceQuoteId = quoteId;
    }
    public boolean doClone(String quoteId) {
        sourceQuoteId = quoteId;
        return doClone();
    }
    public boolean doClone() {
        if (sourceQuoteId == null || sourceQuoteId == '') {
            // Return error that no quote id was provided.
            system.debug('QuoteCloner::doClone: no source Id provided. Clone failed.'); success = false; addMessage('Clone failed'); addMessage('No source quote'); return success;
        }
        if (debug) addMessage('Clone in progress. Quote Id: ' + sourceQuoteId);
        
        /* Begin Clone Setup */
        // Load the source quote record
        sourceQuote = QuoteUtilities.loadQuote(sourceQuoteId);
        if (sourceQuote == null || sourceQuote.Id == null) {
            // Return error that the source quote could not be loaded.
            system.debug('QuoteCloner::doClone: Error loading source quote with Id ' + sourceQuoteId + '. Clone failed.'); success = false; addMessage('Clone failed'); addMessage('Unable to load source quote record'); return success;
        }
        
        // Load the source quote lines
        /* Relationships to update: sbqq__quote__c, sbqq__requiredby__c */
        sourceQuoteLines = QuoteUtilities.loadQuoteLinesMap(sourceQuote.Id);
        if (sourceQuoteLines == null) {
            // Return error that the source quote lines could not be loaded.
            system.debug('QuoteCloner::doClone: Error loading source quote lines for quote Id ' + sourceQuote.Id + '. Clone failed.'); success = false; addMessage('Clone failed'); addMessage('Unable to load source quote line records'); return success;
        } else if (sourceQuoteLines.size() == 0) {
            // Return error that no source quote lines were loaded.
            system.debug('QuoteCloner::doClone: No source quote lines found for quote Id ' + sourceQuote.Id + '. Clone failed.'); success = false; addMessage('Clone failed'); addMessage('No source quote line records to load'); return success;
        }
        
        // Load the source quote's additional information records
        /* Relationships to update: quote__c, quote_line__c */
        sourceAdditionalInformations = QuoteUtilities.loadAdditionalInformationMap(sourceQuote.Id);
        if (sourceAdditionalInformations == null) {
            // Return error that the source additional information records could not be loaded.
            system.debug('QuoteCloner::doClone: Error loading source quote additional information records for quote Id ' + sourceQuote.Id + '. Clone failed.'); success = false; addMessage('Clone failed'); addMessage('Unable to load source quote additional information records'); return success;
        } else if (sourceAdditionalInformations.size() == 0) {
            // Return error that no source quote lines were loaded.
            system.debug('QuoteCloner::doClone: No source additional information records found for quote Id ' + sourceQuote.Id + '. Clone failed.'); success = false; addMessage('Clone failed'); addMessage('No source additional informations records to load'); return success;
        }
        /* End Clone Setup */
     
        // Begin with the shallow clone on the source quote record
        destQuote = sourceQuote.clone(false); /* Set some default values on the cloned quote */ destQuote.SBQQ__Status__c = 'Draft'; /* Clear these - otherwise quotes will expire prematurely */ destQuote.Expiry_Date__c = Date.today().addDays(14); destQuote.SBQQ__ExpirationDate__c = Date.today().addDays(14); destQuote.Credit_Check_Requested__c = false; destQuote.Provisioning_Requested__c = false; destQuote.Waybill_Id__c = ''; destQuote.Agreement_Accepted__c = false; destQuote.Agreement_Expiration_Date__c = null; destQuote.Agreement_Sent_Date__c = null; destQuote.Agreement_Url__c = null; /* Point the cloned quote back to the source */ destQuote.Source_Quote__c = sourceQuote.Id;
        
        /* Update the old quote status to 'Replaced Agreement' */ sourceQuote.SBQQ__Status__c = 'Replaced Agreement';
        
        // Clear credit check information from the web account
        Web_Account__c wa = [Select Name, Credit_Check_Status__c, Credit_Reference__c,Credit_Check_Authorized__c From Web_Account__c Where Id = :sourceQuote.SBQQ__Opportunity__r.Web_Account__c Limit 1];
        wa.Credit_Check_Status__c = null; wa.Credit_Reference__c = null;
        
        try { insert destQuote; update sourceQuote; update wa; } catch (exception e) { QuoteUtilities.log(e); system.debug('QuoteCloner::doClone: Error insert cloned quote record.'); success = false; addMessage('Clone failed'); addMessage('Unable to insert cloned quote record'); rollitback(); return success; }
        
        if (debug) addMessage('Quote record cloned');
        newQuoteId = destQuote.id;
        system.debug(destQuote);
        
        Map<Id, SBQQ__QuoteLine__c> qlsThatNeedParentsReconfigged = new Map<Id, SBQQ__QuoteLine__c>();
        
        // Clone the quote lines and rehome them
        for (Id i : sourceQuoteLines.keySet()) {
            // i contains the original quote line id
            // Shallow clone w/o original Id the source quote line.
            system.debug('QuoteCloner::doClone: cloning QL ' + i);
            SBQQ__QuoteLine__c nql = sourceQuoteLines.get(i).clone(false);
            nql.SBQQ__Quote__c = destQuote.Id;
            
            // Now, validate this quote line!
            // Right now, we only check that the product is active.
            if (sourceQuoteLinesThatFailedValidationAndWereNotCloned.get(nql.SBQQ__RequiredBy__c) == true) {
                system.debug('QuoteCloner::doClone: quote line not cloned - required by a quote line with an inactive product');
                sourceQuoteLinesThatFailedValidationAndWereNotCloned.put(i, true);
                if (nql.Category__c == 'Devices' || nql.Category__c == 'Services') {
                    //destQuote.Completed__c = false; Fixing 11918?
                }
                
                
                continue;
            }
            if (nql.SBQQ__Product__r.IsActive == false) {
                system.debug('QuoteCloner::doClone: quote line not cloned - inactive product');
                SBQQ__QuoteLine__c pql = sourceQuoteLines.get(nql.SBQQ__RequiredBy__c);
                if (pql == null) {
                    addMessage(nql.SBQQ__Product__r.Name + ' was removed from because it is inactive.');
                } else {
                    addMessage(nql.SBQQ__Product__r.Name + ' was removed from "' + pql.SBQQ__Product__r.Name + '" because it is inactive.');
                }
                sourceQuoteLinesThatFailedValidationAndWereNotCloned.put(i, true);
                if (nql.Category__c == 'Devices' || nql.Category__c == 'Services') {
                    //destQuote.Completed__c = false; Fixing 11918?
                }
                if (nql.Category__c != 'Devices') {
                    qlsThatNeedParentsReconfigged.put(i, nql);
                }
                continue;
            }
            sourceToDestQuoteLines.put(i, nql);
        }
        for (Id i : sourceToDestQuoteLines.keySet()) {
            if (sourceQuoteLinesThatFailedValidationAndWereNotCloned.get(sourceToDestQuoteLines.get(i).SBQQ__RequiredBy__c) == true) {
                system.debug('QuoteCloner::doClone: quote line not cloned - required by a quote line with an inactive product');
                sourceToDestQuoteLines.remove(i);
                sourceQuoteLinesThatFailedValidationAndWereNotCloned.put(i, true);
                continue;
            }
        }
        
        // Process all child QLs that need their parents to be reconfigured
        for (Id i : qlsThatNeedParentsReconfigged.keyset()) {
            SBQQ__QuoteLine__c cQL = qlsThatNeedParentsReconfigged.get( i );
            if (sourceQuoteLinesThatFailedValidationAndWereNotCloned.get( cQL.SBQQ__RequiredBy__c ) == true) {
                continue;
            }
            SBQQ__QuoteLine__c pQL = sourceToDestQuoteLines.get( cQL.SBQQ__RequiredBy__c );
            if (pQL == null) { continue; }
            pQL.SBQQ__ConfigurationRequired__c = true;
            
            sourceToDestQuoteLines.put( cQL.SBQQ__RequiredBy__c, pQL );
        }
        try { upsert sourceToDestQuoteLines.values(); } catch (exception e) { QuoteUtilities.log(e); system.debug('QuoteCloner::doClone: Error insert cloned quote line records.'); success = false; addMessage('Clone failed'); addMessage('Unable to insert cloned quote line records'); rollitback(); return success; }
        
        for (Id i : sourcetoDestQuoteLines.keySet()) {
            // i contains the original quote line id
            // Put the destination quote lines
            destQuoteLines.put(sourceToDestQuoteLines.get(i).Id, sourceToDestQuoteLines.get(i));
            sourceToDestQuoteLineIds.put(i, sourceToDestQuoteLines.get(i).id);
        }
        
        for (Id i : DestQuoteLines.keySet()) {
            // i contains the original quote line id
            // Repoint the required by field
            destQuoteLines.get(i).SBQQ__RequiredBy__c = sourceToDestQuoteLineIds.get( destQuoteLines.get(i).SBQQ__RequiredBy__c );
        }
        
        
        // Clone the additional information records and rehome them
        for (Id i : sourceAdditionalInformations.keySet()) {
            // i contains the original addl info id
            system.debug('QuoteCloner::doClone: cloning AI ' + i);
            AdditionalInformation__c nai = sourceAdditionalInformations.get(i).clone(false);
            nai.Quote__c = destQuote.Id;
            nai.Quote_Line__c = sourceToDestQuoteLineIds.get(sourceAdditionalInformations.get(i).Quote_Line__c);
            
            // Now, validate this additional information record!
            // Right now, we only check that quote line that this additional information record belongs to was cloned.
            if (sourceQuoteLinesThatFailedValidationAndWereNotCloned.get(sourceAdditionalInformations.get(i).Quote_Line__c) == true) {
                system.debug('QuoteCloner::doClone: addl info not cloned - related to a quote line that was not cloned');
                sourceAdditionalInformationsThatFailedValidationAndWereNotCloned.put(i, true);
                continue;
            }
            
            sourceToDestAdditionalInformations.put(i, nai);
        }
        try { insert sourceToDestAdditionalInformations.values(); } catch (exception e) { QuoteUtilities.log(e); system.debug('QuoteCloner::doClone: Error insert cloned additional information records.'); success = false; addMessage('Clone failed'); addMessage('Unable to insert cloned additional information records'); rollitback(); return success; }
        
        for (Id i : sourceToDestAdditionalInformations.keySet()) {
            // i contains the original addl info record id
            destAdditionalInformations.put(sourceToDestAdditionalInformations.get(i).Id, sourceToDestAdditionalInformations.get(i));
            sourceToDestAddlInfoIds.put(i, sourceToDestAdditionalInformations.get(i).id);
            System.debug('QuoteCloner::doClone: cloned additional info record for quote line ' + sourceToDestAdditionalInformations.get(i).Quote_Line__c);
            System.debug('QuoteCloner::doClone: cont... addl info record id ' + sourceToDestAdditionalInformations.get(i).Id);
            destQuoteLineToDestAdditionalInformations.put(sourceToDestAdditionalInformations.get(i).Quote_Line__c, sourceToDestAdditionalInformations.get(i).Id);
        }
        
        // Price change checking
        for (Id i : destQuoteLines.keySet()) {
            // i contains the dest quote line Id
            productIds.add( destQuoteLines.get(i).SBQQ__Product__c );
        }
        // Pricebook Selection
        String pbn = destQuote.Province__c + ' - Band ' + destQuote.Rate_Band__c;
        PriceBook2 pricebook = [Select Id From PriceBook2 Where IsActive = true And Name = :pbn Limit 1];
        if (pricebook.Id == null) {system.debug('QuoteCloner::doClone: Error retrieving pricebook.'); success = false; addMessage('Clone failed'); addMessage('Couldn\'t load pricebook.'); rollitback(); return success;}
        // Map the products to the pricebook entries
        List<PriceBookEntry> pricebookEntries = [Select Id, Product2Id, UnitPrice From PriceBookEntry Where PriceBook2Id = :pricebook.Id And Product2Id in :productIds];
        Map<Id, PriceBookEntry> productToPriceBookEntry = new Map<Id, PriceBookEntry>();
        for (PriceBookEntry pbe : priceBookEntries) {
            productToPriceBookEntry.put(pbe.Product2Id, pbe);
        }
        
        
        for (Id i : destQuoteLines.keySet()) {
            // i contains the dest quote line Id
            // Check if the price listed in the pricebook entry is different from the quote line price. If it is,
            //      update the quote line!
            SBQQ__QuoteLine__c ql = destQuoteLines.get(i);
            Product2 p = ql.SBQQ__Product__r;
            SBQQ__ProductOption__c po = ql.SBQQ__ProductOption__r;
            PriceBookEntry pbe = productToPriceBookEntry.get(ql.SBQQ__Product__c);
            
            System.debug('QuoteCloner::doClone: Price checking ' + p.Name + ' quote line id: ' + ql.Id);
            
            // Check to see if this QL has a term price
            AdditionalInformation__c ai = destAdditionalInformations.get( destQuoteLineToDestAdditionalInformations.get(ql.Id) );
            
            if (ai != null && (ai.Service_Term__c != null || ai.Service_Term_QED__c != null)) {
                /* Price changes in the term pricing */
                String term = '';
                if (ai.Service_Term__c != null) { term = ai.Service_Term__c; }
                if (ai.Service_Term_QED__c != null) { term = ai.Service_Term_QED__c; }
                System.debug('QuoteCloner::doClone: price check for ' + p.Name +': term is ' + term + ' and existing price is ' + ql.SBQQ__ListPrice__c);
                Decimal newPrice = 0.00;
                newPrice.setScale(2);
                if (term == 'MTM') {
                    newPrice = p.MTM__c;
                } else if (term == '1 Year') {
                    newPrice = po.X1_Year__c;
                } else if (term == '2 Year') {
                    newPrice = po.X2_Year__c;
                } else if (term == '3 Year') {
                    newPrice = po.X3_Year__c;
                }
                System.debug('QuoteCloner::doClone: price check for ' + p.Name + ': new price is ' + newPrice);
                
                if (newPrice != null && ql.SBQQ__NetPrice__c - newPrice != 0) {
                    system.debug('QuoteCloner::doClone: price for ' + p.Name+ ' changed');
                    // Price went down - service term check
                    if (ql.SBQQ__NetPrice__c >= newPrice) {
                        Decimal priceDiff = ql.SBQQ__NetPrice__c - newPrice;
                        priceDiff.setScale(2);
                        addMessage('The price of "' + p.Name + '" decreased $' + priceDiff + ' to $' + newPrice);
                        
                        ql.SBQQ__ConfigurationRequired__c = true; ql.SBQQ__ListPrice__c = p.MTM__c; ql.SBQQ__CustomerPrice__c = p.MTM__c; ql.SBQQ__NetPrice__c = p.MTM__c; ql.SBQQ__SpecialPrice__c = p.MTM__c; ql.SBQQ__ProratedPrice__c = p.MTM__c; ql.SBQQ__ProratedListPrice__c = p.MTM__c; ql.SBQQ__UnitCost__c = p.MTM__c; ql.SBQQ__RegularPrice__c = p.MTM__c;
                    }
                    // Price went up - service term check
                    else if (ql.SBQQ__NetPrice__c <= newPrice) {
                        Decimal priceDiff = newPrice - ql.SBQQ__NetPrice__c;
                        priceDiff.setScale(2);
                        addMessage('The price of "' + p.Name + '" increased $' + priceDiff + ' to $' + newPrice);
                        ql.SBQQ__ConfigurationRequired__c = true; ql.SBQQ__ListPrice__c = p.MTM__c; ql.SBQQ__CustomerPrice__c = p.MTM__c; ql.SBQQ__NetPrice__c = p.MTM__c; ql.SBQQ__SpecialPrice__c = p.MTM__c; ql.SBQQ__ProratedPrice__c = p.MTM__c; ql.SBQQ__ProratedListPrice__c = p.MTM__c; ql.SBQQ__UnitCost__c = p.MTM__c; ql.SBQQ__RegularPrice__c = p.MTM__c;
                    }
                } else if (newPrice == null) {
                    system.debug('QuoteCloner::doClone: no price found for ' + p.Name);
                } else if (ql.SBQQ__ListPrice__c - newPrice == 0) {
                    system.debug('QuoteCloner::doClone: no price change for ' + p.Name);
                }
                
            } else {
                /* Price changes from PriceBookEntries */
                System.debug('QuoteCloner::doClone: price check for ' + p.Name +': existing price is ' + ql.SBQQ__ListPrice__c + ' price book entry says ' + pbe.UnitPrice);
                if (ql.SBQQ__ListPrice__c == pbe.UnitPrice) {
                    // Prices are the same
                    system.debug('QuoteCloner::doClone: no price change for ' + p.Name);
                } else {
                    // Prices are different
                    // Price went down - PBE check
                    if (ql.SBQQ__ListPrice__c >= pbe.UnitPrice) {
                        Decimal priceDiff = ql.SBQQ__ListPrice__c - pbe.UnitPrice;
                        priceDiff.setScale(2);
                        System.debug('QuoteCloner::doClone: price check for ' + p.Name +': price decreased $' + priceDiff + ' to $' + pbe.UnitPrice);
                        addMessage('The price of "' + p.Name + '" decreased $' + priceDiff + ' to $' + pbe.UnitPrice);
                        
                        ql.SBQQ__ListPrice__c = pbe.UnitPrice; ql.SBQQ__CustomerPrice__c = pbe.UnitPrice; ql.SBQQ__NetPrice__c = pbe.UnitPrice; ql.SBQQ__SpecialPrice__c = pbe.UnitPrice; ql.SBQQ__ProratedPrice__c = pbe.UnitPrice; ql.SBQQ__ProratedListPrice__c = pbe.UnitPrice;
                    }
                    // Price went up - PBE check
                    else if (ql.SBQQ__ListPrice__c <= pbe.UnitPrice) {
                        Decimal priceDiff = pbe.UnitPrice- ql.SBQQ__ListPrice__c;
                        priceDiff.setScale(2);
                        System.debug('QuoteCloner::doClone: price check for ' + p.Name +': price increased $' + priceDiff + ' to $' + pbe.UnitPrice);
                        addMessage('The price of "' + p.Name + '" increased $' + priceDiff + ' to $' + pbe.UnitPrice);
                        
                        ql.SBQQ__ListPrice__c = pbe.UnitPrice; ql.SBQQ__CustomerPrice__c = pbe.UnitPrice; ql.SBQQ__NetPrice__c = pbe.UnitPrice; ql.SBQQ__SpecialPrice__c = pbe.UnitPrice; ql.SBQQ__ProratedPrice__c = pbe.UnitPrice; ql.SBQQ__ProratedListPrice__c = pbe.UnitPrice;
                    }
                }
            }
            
            destQuotelines.put(i, ql);
        }
        try { update destQuote; update destQuoteLines.values(); update destAdditionalInformations.values(); } catch (exception e) { QuoteUtilities.log(e); system.debug('QuoteCloner::doClone: Error updating all modified cloned records.'); success = false; addMessage('Clone failed'); addMessage('Unable to update cloned records'); rollitback(); return success; }
        
        SBQQ__Quote__c dumbQ = [Select Name From SBQQ__Quote__c Where Id = :destQuote.Id];
        addMessage('Congratulations, clone completed. New Quote number: ' + dumbQ.Name);
        
        success = true;
        parseMessages();
        return success;
    }
    
    // Method to add a page message to the queue. Order is maintained.
    private void addMessage(String message) {
        pageMessages.add(message);
        parseMessages();
    }
    // Method to turn all queued page messages into ApexPage messages.
    public void parseMessages() {
        parseMessages(true);
    }
    public void parseMessages(Boolean addToPage) {
        for (Integer i = lastMessageProcessed; i < pageMessages.size(); i++) {
            if (addToPage) { ApexPages.addMessage(new ApexPages.Message( ApexPages.Severity.INFO , pageMessages.get(i) )); }
            if (message != '') { message = message + '|'; }
            message = message + pageMessages.get(i);
            lastMessageProcessed = i;
        }
    } 
    
    // Method to set database savepoint. No boolean defaults to no-overwrite.
    private boolean setSavepoint() { return setSavepoint(false);}
    private boolean setSavepoint(Boolean overwrite) {
        if (rollbackSp == null || overwrite == true) {
            try { rollbackSp = Database.setSavepoint(); }
            catch (exception e) {
                // Error getting savepoint
                QuoteUtilities.log(e);
                system.debug('setSavepoint: setting savepoint failed. exception message: ' + e.getMessage());
                addMessage('Error setting database savepoint');
                return false;
            }
        } else if (rollbackSp != null) { system.debug('setSavepoint: savepoint already set.'); }
        if (rollbackSp == null) { return false; }
        
        system.debug('setSavepoint: database savepoint set.');
        if (debug) addMessage('Database savepoint set');
        return true;
    }
    
    // Method to rollback from savepoint
    private boolean rollitback() {
        if (rollbackSp == null) {
            system.debug('rollitback: no savepoint set');
        }
        try {
            database.rollback(rollbackSp);
        } catch (exception e) {
            QuoteUtilities.log(e);
            system.debug('rollitback: exception thrown while rolling back database. message: ' + e.getMessage());
            addMessage('Database rollback failed');
            addMessage(e.getMessage());
            return false;
        }
        newQuoteId = null;
        system.debug('QuoteCloner::rollitback: rollback succeeded.');
        addMessage('Database rollback succeeded');
        return true;
    }
}