/*****************************************************
    Name: ProductTranslationBatchJob
    Usage: This batch job will update bookable flag = No for White Glove Installation
            TO RUN THIS batch job FROM ANONYMOUS MODE  paste this code :
            use below code to run next job
            ProductTranslationBatchJob jobObject = new ProductTranslationBatchJob (true);
            Id batchJobId = Database.executeBatch(jobObject,2000);
            
            To Restrict next job, use below code
            ProductTranslationBatchJob jobObject = new ProductTranslationBatchJob();
            Id batchJobId = Database.executeBatch(jobObject,2000);
            
    Author – Sandip Chaudhari (IBM)
    Revision History -
    Created By/Date - Sandip Chaudhari / 24 May 2017
    
    Updated By/Date/Reason for Change - 
      
******************************************************/
public class ProductTranslationBatchJob implements Database.Batchable<sObject>{
    public String query = 'SELECT ID, Product_Language_Support__c, orderMgmtId__c' +
                          ' FROM Product2 ';
                          //' WHERE Name = \'White Glove Service Configuration\''+
                          //' OR Name = \'On-Site CPE Spare - Lanner 7525A\'';
    
    public String exceptionStr = '';
    public Boolean isError = false;
    public Boolean executeNextJob = false;
    
    // Constructor to get query from outside
    public ProductTranslationBatchJob(String queryp){
        query = queryp;
    }
    
    public ProductTranslationBatchJob(Boolean isExecuteNextJob){
        executeNextJob = isExecuteNextJob;
    }
    
    // Constructor to get query from outside and decide whether to execute next job or not
    public ProductTranslationBatchJob(String queryp, Boolean isExecuteNextJob){
        query = queryp;
        executeNextJob = isExecuteNextJob;
    }
    
    // Constructor with default query
    public ProductTranslationBatchJob(){
               
    }
    
    // Start the batch job
    public Database.QueryLocator start(Database.BatchableContext BC){
       // Access initialState here
        return Database.getQueryLocator(query);
    }
    
    // Execute the batch job
    public void execute(Database.BatchableContext BC, List<sObject> scope) {
        // Access initialState here
        try{
            List<Product2> lstProdUpdate = new List<Product2>();
            List<String> productExternalIds = new List<String>();
            //Prepare list of Product external Ids and product object
            for(Sobject sObj : scope){
                Product2 objProd = (Product2)sObj;
                productExternalIds.add(objProd.orderMgmtId__c);
                lstProdUpdate.add(objProd);
            }
            List<VL_Product_NLS__c> lstProductNLS;
            List<VL_Product_Translate__c> lstProductTranslation;
            if(productExternalIds != null && productExternalIds.size()>0){
                // Get records for translation text populated by ETL 
                lstProductNLS = [SELECT Language_Id__c, NLS_Field_Name__c, NLS_Field_Value__c, Linked_Product__c, Product_External_Id__c
                                  FROM VL_Product_NLS__c 
                                  WHERE Product_External_Id__c IN:productExternalIds];
                
                // get list of earlier post ETL processed records for respective products.
                lstProductTranslation = [SELECT Id, Product_External_Id__c, Name_FR_CA__c, Description_FR_CA__c, DetailedDescription_FR_CA__c
                                         //Name_DE__c
                                        FROM VL_Product_Translate__c
                                        WHERE Product_External_Id__c IN: productExternalIds
                                        ];
            }
            
            // Preapre map of earlier processed records which are already in salesforce obejct
            Map<String, VL_Product_Translate__c> existingTranslationMap = new Map<String, VL_Product_Translate__c>();
            if(lstProductTranslation != null && lstProductTranslation.size()>0){
                for(VL_Product_Translate__c transObj: lstProductTranslation){
                    existingTranslationMap.put(transObj.Product_External_Id__c, transObj);
                }
            }
            
            /*List<NC_Language_Code_Mapping__mdt> isoCodeMetaDataList = [SELECT MasterLabel,DeveloperName FROM NC_Language_Code_Mapping__mdt];
            Map<String, NC_Language_Code_Mapping__mdt> isoCodeMetaDataMap = new Map<String, NC_Language_Code_Mapping__mdt>();
            if(isoCodeMetaDataList != null && isoCodeMetaDataList.size()>0){
                for(NC_Language_Code_Mapping__mdt metaDataObj: isoCodeMetaDataList){
                    isoCodeMetaDataMap.put(metaDataObj.DeveloperName, metaDataObj);
                }
            }*/
            
            Map<String, VL_Product_Translate__c> cacheTranslationMap = new Map<String, VL_Product_Translate__c>();
            if(lstProductNLS != null && lstProductNLS.size()>0){
                for(VL_Product_NLS__c  NLSObj: lstProductNLS){
                    VL_Product_Translate__c tempTransObj = new VL_Product_Translate__c();
                    // Check if record already processed earlier or not for respetive product
                    if(existingTranslationMap != null && NLSObj.Product_External_Id__c != null && existingTranslationMap.get(NLSObj.Product_External_Id__c) != null){
                        tempTransObj = existingTranslationMap.get(NLSObj.Product_External_Id__c);
                    }
                    
                    // Check if record already in cache for respetive product
                    if(cacheTranslationMap != null && NLSObj.Product_External_Id__c!= null && cacheTranslationMap.get(NLSObj.Product_External_Id__c) != null){
                        VL_Product_Translate__c tempCacheTransObj = cacheTranslationMap.get(NLSObj.Product_External_Id__c);
                        tempTransObj.Name_FR_CA__c = tempCacheTransObj.Name_FR_CA__c;
                        tempTransObj.DetailedDescription_FR_CA__c = tempCacheTransObj.DetailedDescription_FR_CA__c;
                        tempTransObj.Description_FR_CA__c = tempCacheTransObj.Description_FR_CA__c;
                    }
                    tempTransObj.Product_External_Id__c = NLSObj.Product_External_Id__c;
                    /*tempTransObj.Product_Link__c = NLSObj.Product_Link__c;
                    if(NLSObj.Language_Id__c != null && isoCodeMetaDataMap.get(NLSObj.Language_Id__c) != null){
                        //tempTransObj.Name_DE__c = isoCodeMetaDataMap.get(NLSObj.Language_Id__c);
                    }else{
                        //tempTransObj.Name_DE__c = 'ENG';
                    }*/
                    
                    // Assign values
                    if(NLSObj.NLS_Field_Name__c == 'Name'){
                        tempTransObj.Name_FR_CA__c = NLSObj.NLS_Field_Value__c;
                    }else if(NLSObj.NLS_Field_Name__c=='DetailedDescription__c'){
                        tempTransObj.DetailedDescription_FR_CA__c = NLSObj.NLS_Field_Value__c;
                    }else if(NLSObj.NLS_Field_Name__c=='Description'){
                        tempTransObj.Description_FR_CA__c = NLSObj.NLS_Field_Value__c;
                    }
                    cacheTranslationMap.put(NLSObj.Product_External_Id__c, tempTransObj);
                }
                
                if(cacheTranslationMap != null && cacheTranslationMap.size()>0){
                    // upsert records
                    List<VL_Product_Translate__c> lstProdTranslationUpsert = cacheTranslationMap.values();
                    upsert lstProdTranslationUpsert;
                    
                    // update products
                    for(Product2 tmpProdObj: lstProdUpdate){
                        If(tmpProdObj.orderMgmtId__c != null && cacheTranslationMap.get(tmpProdObj.orderMgmtId__c) != null){
                            tmpProdObj.Product_Language_Support__c = cacheTranslationMap.get(tmpProdObj.orderMgmtId__c).Id;
                        }
                    }
                    update lstProdUpdate;
                }
            }
            
        }catch (Exception ex){
            isError = true;
            exceptionStr = ex.getMessage();
        }
    }
    
    public void finish(Database.BatchableContext BC) {
        // Initiate next batch job
        ExternalAuditLogHelper.ExternalAuditLogCreateParameter paramObj = new  ExternalAuditLogHelper.ExternalAuditLogCreateParameter();
        paramObj.emailAddress = '';
        paramObj.jobName = ExternalAuditLogHelper.OCOM_PRODUCT_TRANSLATION_BATCH_JOB;
        if(isError){
            paramObj.result = ExternalAuditLogHelper.OCOM_ETL_JOB_ERROR;
        }else{
            paramObj.result = ExternalAuditLogHelper.OCOM_ETL_JOB_SUCCESS;
        }
        paramObj.resultDescription = exceptionStr;
        paramObj.batchId = bc.getJobId();
        paramObj.emailSubject = System.Label.ProductTranslationBatchJob+ ' ';
        paramObj.executeNextJob = executeNextJob;
        ExternalAuditLogHelper.createAuditLog (paramObj);
    }
}