@isTest
private class trac_UtilitiesTest {

  private static testMethod void testQueryUtilities() {
    
    // Create test query
    String theTestQuery = trac_Utilities.createObjectQuery('Lead');
    
    // Test query field removal
    String theTestQueryMinus = trac_Utilities.removeField(theTestQuery, 'FirstName');
    system.assertEquals(false, theTestQueryMinus.contains('FirstName'));
    
    // Test query field addition
    String theTestQueryPlus = trac_Utilities.addField(theTestQueryMinus, 'FirstName');
    system.assertEquals(true, theTestQueryPlus.contains('FirstName'));
    
    // Re-test removal
    String theTestQueryMinusRepeat = trac_Utilities.removeField(theTestQueryPlus, 'FirstName');
    system.assertEquals(false, theTestQueryMinusRepeat.contains('FirstName'));

  }
  
  private static testMethod void testIsFieldAccessible() {
    system.assert(trac_Utilities.isFieldAccessible('Opportunity', 'StageName'));
    system.assertEquals(false,trac_Utilities.isFieldAccessible('Opportunity', 'Invalid'));
  }
  
  private static testMethod void testIsFieldEditable() {
  	system.assert(trac_Utilities.isFieldEditable('Account', 'Name'));
  	system.assertEquals(false, trac_Utilities.isFieldEditable('Account', 'Id'));
  	system.assertEquals(false, trac_Utilities.isFieldEditable('Account', 'Invalid'));
  }
}