@isTest
private class ProductVOTest {

@isTest(SeeAllData=true)
private static void testProductVO() {
		Id PRICEBOOKID = [SELECT id FROM Pricebook2 WHERE isStandard = true LIMIT 1].id;
		
		Product2 p = new Product2(
			name = 'test',
			CurrencyIsoCode = 'CAD',
			SBQQ__ConfigurationType__c = 'required',
			SBQQ__SubscriptionPricing__c = 'Percent of Total',
			SBQQ__PricingMethod__c = 'Block',
			SBQQ__ConfigurationEvent__c = 'Always',
			Core_Product_Ids__c = '01t40000001jG3SAAU,01t50000001jG3SAAU'
		);
		Product2 p2 = new Product2(
			name = 'optional test',
			CurrencyIsoCode = 'CAD',
			SBQQ__ConfigurationType__c = 'allowed'
		);
		insert new Product2[] { p, p2 };
		
		PricebookEntry pbe = new PricebookEntry(
			product2id = p.id,
			unitprice = 100,
			usestandardprice = false,
			isactive = true,
			pricebook2id = PRICEBOOKID
		);
    insert pbe;
		
		SBQQ__ProductFeature__c f = new SBQQ__ProductFeature__c(
			SBQQ__ConfiguredSKU__c = p.id,
			SBQQ__Number__c = 1
		);
		SBQQ__ProductFeature__c f2 = new SBQQ__ProductFeature__c(
			SBQQ__ConfiguredSKU__c = p.id,
			SBQQ__Number__c = 2
		);
		insert new SBQQ__ProductFeature__c[] { f, f2 };
		
		SBQQ__ProductOption__c po = new SBQQ__ProductOption__c(
			SBQQ__ConfiguredSKU__c = p.id,
			SBQQ__Feature__c = f.id,
			SBQQ__Number__c = 1
		);
		SBQQ__ProductOption__c po2 = new SBQQ__ProductOption__c(
			SBQQ__ConfiguredSKU__c = p.id,
			SBQQ__Feature__c = f2.id,
			SBQQ__Number__c = 1,
			SBQQ__OptionalSKU__c = p2.id
		);
		insert new SBQQ__ProductOption__c[] { po, po2 };
		
		SBQQ__BlockPrice__c bp = new SBQQ__BlockPrice__c(
			SBQQ__Product__c = p.id,
			SBQQ__Price__c = 100,
			SBQQ__LowerBound__c = 80,
			SBQQ__UpperBound__c = 120
		);
		insert bp;
		
		SBQQ__Cost__c c = new SBQQ__Cost__c(
			SBQQ__Active__c = true,
			SBQQ__Product__c = p.id,
			SBQQ__UnitCost__c = 75,
			CurrencyIsoCode = 'CAD'
		);
		insert c;
		
		SBQQ__OptionConstraint__c cons = new SBQQ__OptionConstraint__c(
			SBQQ__ConstrainedOption__c = po.id,
			SBQQ__ConstrainingOption__c = po2.id,
			SBQQ__ConfiguredSKU__c = p.id,
			SBQQ__Type__c = 'Exclusion'
		);
		insert cons;
		
		p = [
			SELECT name, Core_Product_Ids__c, CurrencyIsoCode, SBQQ__ConfigurationType__c, SBQQ__SubscriptionPricing__c,
						 SBQQ__PricingMethod__c, SBQQ__ConfigurationEvent__c, SBQQ__ConfigurationFields__c,
				(SELECT SBQQ__ConfiguredSKU__c, SBQQ__Number__c FROM SBQQ__Features__r),
				(SELECT SBQQ__ConfiguredSKU__c, SBQQ__Feature__c, SBQQ__Number__c FROM SBQQ__Options__r),
				(SELECT SBQQ__Price__c, SBQQ__LowerBound__c, SBQQ__UpperBound__c FROM SBQQ__BlockPrices__r),
				(SELECT SBQQ__Active__c, SBQQ__Product__c, SBQQ__UnitCost__c, CurrencyIsoCode FROM SBQQ__Costs__r),
				(SELECT SBQQ__ConstrainedOption__c, SBQQ__ConstrainingOption__c, SBQQ__Type__c FROM SBQQ__Option_Constraints__r),
				(SELECT UnitPrice, Pricebook2Id, CurrencyIsoCode FROM PricebookEntries)
			FROM Product2
			WHERE Id = :p.id
		];
		
		ProductVO vo = new ProductVO(p);
		
		system.assertEquals('test', vo.getName());
		
		system.assertNotEquals(null, vo.getOptionById(po.id));
		system.assertNotEquals(null, vo.getFeatureById(f.id));
		
		system.assertEquals(0, vo.getOptionsWithoutFeature().size());
		
		system.assert(vo.isConfigurationRequired());
		system.assertEquals(false, vo.isConfigurationAllowed());
		
		system.assert(vo.isDynamicSubscription());
		system.assert(vo.isSubscription());
		
		Set<Id> optionProductIds = vo.getOptionProductIds();
		system.assertEquals(0, optionProductIds.size());
		
		Set<Id> coreProductIds = vo.getCoreProductIds();
		system.assertEquals(2, coreProductIds.size());
		
		system.assert(vo.isBlockPriced());
		
		system.assert(vo.isConfigurationEventAlways());
		system.assertEquals(false, vo.isConfigurationEventAdd());
		system.assertEquals(false, vo.isConfigurationEventEdit());
		
		system.assertEquals(false, vo.hasConfigurationFields());
		
		system.assertEquals(75, vo.getCost('CAD'));
		system.assertEquals(100, vo.getDefaultListPrice());
		system.assertEquals(100, vo.getListPrice(PRICEBOOKID, 'CAD'));
				
	}
	
	private static testMethod void testProductVONoRelatedItems() {
		Product2 p = new Product2(name = 'test');
		insert p;
		
		p = [
			SELECT name,
				(SELECT id FROM SBQQ__Features__r),
				(SELECT id FROM SBQQ__Options__r),
				(SELECT id FROM SBQQ__BlockPrices__r),
				(SELECT id FROM SBQQ__Costs__r),
				(SELECT id FROM SBQQ__Option_Constraints__r)
			FROM Product2
			WHERE Id = :p.id];
		
		ProductVO vo = new ProductVO(p);
	}
	
	private static testMethod void testOption() {
		Product2 p = new Product2(name = 'test');
		insert p;
		
		SBQQ__ProductFeature__c f = new SBQQ__ProductFeature__c(
			SBQQ__ConfiguredSKU__c = p.id,
			SBQQ__Number__c = 1
		);
		insert f;
		
		SBQQ__ProductOption__c po = new SBQQ__ProductOption__c(
			SBQQ__ConfiguredSKU__c = p.id,
			SBQQ__Number__c = 1,
			SBQQ__Feature__c = f.id
		);
		insert po;
		
		ProductVO vo = new ProductVO(p);
		
		ProductVO.Feature feature = new ProductVO.Feature(vo, f);
		
		ProductVO.Option o = new ProductVO.Option(vo, po, feature);
		
		system.assertEquals(true, o.isQuantityEditable());
		system.assertEquals(false, o.hasDependencies());
		system.assertEquals(false, o.hasExclusions());
		system.assertEquals(false, o.isExclusiveWith(o));
		system.assertEquals(false, o.isDependentOn(o));
		system.assertEquals(false, o.isConfigurationAllowed());
		system.assertEquals(false, o.isConfigurationRequired());
	}
}