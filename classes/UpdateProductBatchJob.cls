/*****************************************************
    Name: UpdateProductBatchJob
    Usage: This batch job will update bookable flag = No for White Glove Installation
            TO RUN THIS batch job FROM ANONYMOUS MODE  paste this code :
            use below code to run next job
            UpdateProductBatchJob jobObject = new UpdateProductBatchJob(true);
            Id batchJobId = Database.executeBatch(jobObject,2000);
            
            To Restrict next job, use below code
            UpdateProductBatchJob jobObject = new UpdateProductBatchJob();
            Id batchJobId = Database.executeBatch(jobObject,2000);
            
    Author – Sandip Chaudhari (IBM)
    Revision History -
    Created By/Date - Sandip Chaudhari / 08 Dec 2016
    
    Updated By/Date/Reason for Change - 
    15 May 2017 - Changes by Sandip - Part of BP1-101 requirement - 
                                    Update Product2.VLAdditionalConfigData__c with {"Code":"2Play"} where Name in 
                                    ('Office Phone & Internet Bundle - 25 or 50Mbps', 'Office Phone & Internet Bundle - 150Mbps')
    26 July 2017 - User Story # BP1-90 - Changes by Sandip - Whenever there is an attribute called "Offering ID" with a value specified for a product, 
                                    please update VLAdditional Config field at Product2 level (if it is blank or append to it, if it is not blank) 
                                    e.g. {"Offering ID":"FIFA-INET-250"}’

    
******************************************************/
public class UpdateProductBatchJob implements Database.Batchable<sObject>{
    //15 May 2017 - Changes by Sandip - Part of BP1-101 requirement
    /*public String query = 'SELECT id, name, IncludeQuoteContract__c, OCOM_Bookable__c, VLAdditionalConfigData__c, ' +
                          ' vlocity_cmt__JSONAttribute__c ' +
                          ' FROM Product2 WHERE name Like \'%White Glove Installation%\'' + 
                          ' OR Name = \'Office Phone & Internet Bundle - 25 or 50Mbps\'' +
                          ' OR Name = \'Office Phone & Internet Bundle - 150Mbps\'';
    */  
    public String query = 'SELECT id, name, IncludeQuoteContract__c, OCOM_Bookable__c, VLAdditionalConfigData__c, ' +
                          ' vlocity_cmt__JSONAttribute__c ' +
                          ' FROM Product2';
    
    public String exceptionStr = '';
    public Boolean isError = false;
    public Boolean executeNextJob = false;
    
    // Constructor to get query from outside
    public UpdateProductBatchJob(String queryp){
        query = queryp;
    }
    
    public UpdateProductBatchJob(Boolean isExecuteNextJob){
        executeNextJob = isExecuteNextJob;
    }
    
    // Constructor to get query from outside and decide whether to execute next job or not
    public UpdateProductBatchJob(String queryp, Boolean isExecuteNextJob){
        query = queryp;
        executeNextJob = isExecuteNextJob;
    }
    
    // Constructor with default query
    public UpdateProductBatchJob(){
               
    }
    
    // Start the batch job
    public Database.QueryLocator start(Database.BatchableContext BC){
       // Access initialState here
        return Database.getQueryLocator(query);
    }
    
    // Execute the batch job
    public void execute(Database.BatchableContext BC, List<sObject> scope) {
        // Access initialState here
        try{
            // 26 July 2017 - User Story # BP1-90 - Changes by Sandip
            Set<Id> prodIds = new Set<Id>();
            Map<Id, vlocity_cmt__AttributeAssignment__c> attrAssignMap = new Map<Id, vlocity_cmt__AttributeAssignment__c>();
            for(Sobject sObj : scope){
                Product2 objProd = (Product2)sObj;
                prodIds.add(objProd.Id);
            }
            
            List<vlocity_cmt__AttributeAssignment__c> attrAssignList= [SELECT vlocity_cmt__ObjectId__c, vlocity_cmt__AttributeId__c, 
                                                        vlocity_cmt__AttributeName__c, vlocity_cmt__Value__c 
                                                        FROM vlocity_cmt__AttributeAssignment__c 
                                                        WHERE vlocity_cmt__ObjectId__c IN: prodIds
                                                        AND vlocity_cmt__AttributeName__c=: 'Offering ID' ];
                                                        
            for(vlocity_cmt__AttributeAssignment__c attObj: attrAssignList){
                attrAssignMap.put(attObj.vlocity_cmt__ObjectId__c, attObj);
            } 
            // End BP1-90
                                                     
            List<Product2> lstUpdate = new List<Product2>();
            for(Sobject sObj : scope){
                Boolean isUpdate = false;
                Product2 objProd = (Product2)sObj;
                if(objProd.name != null && objProd.name.contains('White Glove Installation')){
                    objProd.OCOM_Bookable__c = 'No';
                    isUpdate = true;
                }
                //15 May 2017 - Changes by Sandip - Part of BP1-101 requirement
                if(objProd.name != null && (objProd.name == 'Office Phone & Internet Bundle - 25 or 50Mbps' || objProd.name == 'Office Phone & Internet Bundle - 150Mbps')){
                    objProd.VLAdditionalConfigData__c = '{"Code":"2Play"}';
                    isUpdate = true;
                }
                // End BP1-101
                // 26 July 2017 - User Story # BP1-90 - Changes by Sandip
                //createJSON(objProd);
                if(attrAssignMap.get(objProd.Id) != null){
                    vlocity_cmt__AttributeAssignment__c attrObj =  attrAssignMap.get(objProd.Id);
                    String offeringJSON = '"'+attrObj.vlocity_cmt__AttributeName__c+'":"'+attrObj.vlocity_cmt__Value__c+'"}';
                    if(objProd.VLAdditionalConfigData__c != null){
                        if(!(objProd.VLAdditionalConfigData__c.contains('Offering ID'))){
                            objProd.VLAdditionalConfigData__c = objProd.VLAdditionalConfigData__c.replace('}' , ','+offeringJSON);
                            isUpdate = true;
                        }
                    }else{
                        objProd.VLAdditionalConfigData__c = '{'+offeringJSON;
                        isUpdate = true;
                    }
                }
                //end BP1-90
                if(isUpdate){
                   lstUpdate.add(objProd); 
                }
            }
            if(lstUpdate.size()>0){
                update lstUpdate;
            }
        }catch (Exception ex){
            isError = true;
            exceptionStr = ex.getMessage();
        }
    }
    
    public void finish(Database.BatchableContext BC) {
        // Initiate next batch job
        ExternalAuditLogHelper.ExternalAuditLogCreateParameter paramObj = new  ExternalAuditLogHelper.ExternalAuditLogCreateParameter();
        paramObj.emailAddress = '';
        paramObj.jobName = ExternalAuditLogHelper.OCOM_UPDATE_PROD_BATCH_JOB;
        if(isError){
            paramObj.result = ExternalAuditLogHelper.OCOM_ETL_JOB_ERROR;
        }else{
            paramObj.result = ExternalAuditLogHelper.OCOM_ETL_JOB_SUCCESS;
        }
        paramObj.resultDescription = exceptionStr;
        paramObj.batchId = bc.getJobId();
        paramObj.emailSubject = System.Label.UpdateProductBatchJobEmailSubject + ' ';
        paramObj.executeNextJob = executeNextJob;
        ExternalAuditLogHelper.createAuditLog (paramObj);
    }
    
}