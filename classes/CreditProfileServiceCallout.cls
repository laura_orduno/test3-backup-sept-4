global class CreditProfileServiceCallout
{
    private static final Integer WS_TIMEOUT_MS = 120000;

    webservice static void ping()
    {
        try
        {
            CreditProfileService.BusinessCreditProfileMgmtService_v1_0_SOAP servicePort = new CreditProfileService.BusinessCreditProfileMgmtService_v1_0_SOAP();
            servicePort = prepareCallout(servicePort);
            servicePort.Ping();
        }
        catch(CalloutException e){
            System.debug(' ... dyy ... e.getMessage = ' + e.getMessage());
            System.debug(' ... dyy ... e.getCause = ' + e.getCause());
            System.debug(' ... dyy ... e.getStackTraceString = ' + e.getStackTraceString());
        }
    }

    webservice static List<CreditSearchProfileResult> searchIndivdualCreditProfile(CreditSearchParamForIndividualProfile param)
    {
        CreditProfileService.BusinessCreditProfileMgmtService_v1_0_SOAP servicePort = new CreditProfileService.BusinessCreditProfileMgmtService_v1_0_SOAP();
        servicePort = prepareCallout(servicePort);
        CreditProfileServiceTypes.IndividualProfile[] profiles = servicePort.searchIndivdualCreditProfile(buildCreditSearchCriteria(param), buildCreditSearchAuditInfo());

        return mapToCreditProfiles(profiles);
    }

    webservice static Long createIndividualCustomerWithCreditWorthiness(CreateConsumerCreditProfileRequest request)
    {
        CreditProfileService.BusinessCreditProfileMgmtService_v1_0_SOAP servicePort = new CreditProfileService.BusinessCreditProfileMgmtService_v1_0_SOAP();
        servicePort = prepareCallout(servicePort);

        try
        {
            return servicePort.createIndividualCustomerWithCreditWorthiness(buildIndividualCustomerProfile(request), buildIndividualCreditProfileBase(request), buildCreditSearchAuditInfo());
        }
        catch(CalloutException e)
        {
            writeToWebserviceIntegrationErrorLog('CreditProfileServiceCallout.createIndividualCustomerWithCreditWorthiness', 'BusinessCreditProfileMgmtService_v1_0_SOAP.createIndividualCustomerWithCreditWorthiness', e);

            throw e;
        }
    }

    private static CreditProfileService.BusinessCreditProfileMgmtService_v1_0_SOAP prepareCallout(CreditProfileService.BusinessCreditProfileMgmtService_v1_0_SOAP svcPort)
    {
        if ( !Test.isRunningTest() )
        {
            SMBCare_WebServices__c wsUsername = SMBCare_WebServices__c.getValues('username');
            SMBCare_WebServices__c wsPassword = SMBCare_WebServices__c.getValues('password');

            System.debug('... dyy ... username = ' + wsUsername.value__c);
            System.debug('... dyy ... password = ' + wsPassword.value__c);

            String creds;

            if (String.isNotBlank(WSusername.Value__c) && String.isNotBlank(wsPassword.Value__c)) {
                creds = WSusername.Value__c + ':' + wsPassword.Value__c;
            }

            String encodedusernameandpassword = EncodingUtil.base64Encode(Blob.valueOf(creds));
            svcPort.inputHttpHeaders_x = new Map<String, String>();
            svcPort.inputHttpHeaders_x.put('Authorization', 'Basic ' + encodedusernameandpassword);

            //Set SFDC Apex Webservice call timeout
            svcPort.timeout_x = WS_TIMEOUT_MS;
        }

        return svcPort;
    }

    private static String queryCurrentUserTelusId()
    {
        Id userId = UserInfo.getUserId();

        String telusId = [Select Id, Team_TELUS_ID__c from User where Id = :userId].Team_TELUS_ID__c;

        system.debug('Telus Employee ID: ' + telusId);

        return telusId;
    }

    private static CreditProfileServiceTypes.IndividualCreditIdentification buildCreditSearchCriteria(CreditSearchParamForIndividualProfile param)
    {
        CreditProfileServiceTypes.IndividualCreditIdentification criteria = new CreditProfileServiceTypes.IndividualCreditIdentification();

        criteria.sin = getSinCriteria(param);
        criteria.driverlicense = buildDriverLicenseCriteria(param);
        criteria.healthcard = buildHealthCardCriteria(param);
        criteria.passport = buildPassportCriteria(param);
        criteria.provincialIdCard = buildProvincialIdCardCriteria(param);

        return criteria;
    }

    private static String getSinCriteria(CreditSearchParamForIndividualProfile param)
    {
        return param.sin;
    }

    private static CreditProfileServiceTypes.DriverLicense buildDriverLicenseCriteria(CreditSearchParamForIndividualProfile param)
    {
        CreditProfileServiceTypes.DriverLicense driverLicenseCriteria = new CreditProfileServiceTypes.DriverLicense();

        driverLicenseCriteria.driverLicenseNum = param.driversLicenseNum;
        driverLicenseCriteria.provinceCd = param.driversLicenseProvinceCd;

        return driverLicenseCriteria;
    }

    private static CreditProfileServiceTypes.HealthCard buildHealthCardCriteria(CreditSearchParamForIndividualProfile param)
    {
        CreditProfileServiceTypes.HealthCard healthCardCriteria = new CreditProfileServiceTypes.HealthCard();

        healthCardCriteria.healthCardNum = param.healthCardNum;

        return healthcardCriteria;
    }

    private static CreditProfileServiceTypes.Passport buildPassportCriteria(CreditSearchParamForIndividualProfile param)
    {
        CreditProfileServiceTypes.Passport passportCriteria = new CreditProfileServiceTypes.Passport();

        passportCriteria.passportNum = param.passportNum;
        passportCriteria.countryCd = param.passportCountry;

        return passportCriteria;
    }

    private static CreditProfileServiceTypes.ProvincialIdCard buildProvincialIdCardCriteria(CreditSearchParamForIndividualProfile param)
    {
        CreditProfileServiceTypes.ProvincialIdCard provincialIdCardCriteria = new CreditProfileServiceTypes.ProvincialIdCard();

        provincialIdCardCriteria.provincialIdNum = param.provincialIdNum;
        provincialIdCardCriteria.provinceCd = param.provincialIdProvinceCd;

        return provincialIdCardCriteria;
    }

    private static CreditProfileServiceTypes.CreditAuditInfo buildCreditSearchAuditInfo()
    {
        CreditProfileServiceTypes.CreditAuditInfo auditInfo = new CreditProfileServiceTypes.CreditAuditInfo();

        auditInfo.userId = queryCurrentUserTelusId();
        auditInfo.originatorApplicationId = 6161;
        auditInfo.requestTimestamp = Datetime.now();

        return auditInfo;
    }

    private static List<CreditSearchProfileResult> mapToCreditProfiles(CreditProfileServiceTypes.IndividualProfile[] customerProfiles)
    {
        List<CreditSearchProfileResult> creditProfiles = new List<CreditSearchProfileResult>();

        if ( customerProfiles != null )
        {
            for (CreditProfileServiceTypes.IndividualProfile customerProfile : customerProfiles)
            {
                creditProfiles.add(buildCreditProfile(customerProfile));
            }
        }
        return creditProfiles;
    }

    private static CreditSearchProfileResult buildCreditProfile(CreditProfileServiceTypes.IndividualProfile customerProfile)
    {
        CreditProfileServiceTypes.IndividualCustomerProfile individualCustomerProfile = customerProfile.individualCustomerProfile;
        CreditProfileServiceTypes.IndividualCreditProfile individualCreditProfile = customerProfile.individualCreditProfile;
        CreditProfileServiceTypes.IndividualCreditIdentification creditIdentification = individualCreditProfile.creditIdentification;

        CreditSearchProfileResult creditProfileResult = new CreditSearchProfileResult();

        creditProfileResult.profileId = individualCustomerProfile.individualCustomerId;
        creditProfileResult.name = buildCustomerName(individualCustomerProfile);
        creditProfileResult.firstName = individualCustomerProfile.firstName;
        creditProfileResult.middleName = individualCustomerProfile.middleName;
        creditProfileResult.lastName = individualCustomerProfile.lastName;
        creditProfileResult.homePhoneNumber = individualCustomerProfile.homePhoneNum;
        creditProfileResult.cellPhoneNumber = individualCustomerProfile.cellPhoneNum;
        creditProfileResult.emailTxt = individualCustomerProfile.emailTxt;

        if ( individualCreditProfile.creditAddress != null )
        {
            creditProfileResult.addressLine = buildAddressLine(individualCreditProfile.creditAddress);
            creditProfileResult.city = individualCreditProfile.creditAddress.cityName;
            creditProfileResult.provinceStateCode = individualCreditProfile.creditAddress.provinceCd;
            creditProfileResult.postalCode = individualCreditProfile.creditAddress.postalCd;
            creditProfileResult.countryCode = individualCreditProfile.creditAddress.countryCd;
        }

        if ( individualCreditProfile.personalInfo != null )
        {
            creditProfileResult.birthdate = individualCreditProfile.personalInfo.birthdate;
            creditProfileResult.creditCheckConsentCd = individualCreditProfile.personalInfo.creditCheckConsentCd;
        }

        creditProfileResult.sin = creditIdentification.sin;

        if ( creditIdentification.driverLicense != null )
        {
            creditProfileResult.driversLicenseNum = creditIdentification.driverLicense.driverLicenseNum;
            creditProfileResult.driversLicenseProvinceCd = creditIdentification.driverLicense.provinceCd;
        }

        if ( creditIdentification.provincialIdCard != null )
        {
            creditProfileResult.provincialIdNum = creditIdentification.provincialIdCard.provincialIdNum;
            creditProfileResult.provincialIdProvinceCd = creditIdentification.provincialIdCard.provinceCd;
        }

        if ( creditIdentification.passport != null )
        {
            creditProfileResult.passportNum = creditIdentification.passport.passportNum;
            creditProfileResult.passportCountry = creditIdentification.passport.countryCd;
        }

        if ( creditIdentification.healthCard != null )
        {
            creditProfileResult.healthCardNum = creditIdentification.healthCard.healthCardNum;
        }

        return creditProfileResult;
    }

    private static String buildCustomerName(CreditProfileServiceTypes.IndividualCustomerProfile individualCustomerProfile)
    {
        String customerName = '';

        if ( individualCustomerProfile != null )
        {
            if ( String.isNotBlank(individualCustomerProfile.firstName) )
            {
                customerName += individualCustomerProfile.firstName;
            }

            if ( String.isNotBlank(individualCustomerProfile.middleName) )
            {
                customerName += ' ' + individualCustomerProfile.middleName;
            }

            if ( String.isNotBlank(individualCustomerProfile.lastName) )
            {
                customerName += ' ' + individualCustomerProfile.lastName;
            }

            customerName.trim();
        }

        return customerName;
    }

    private static String buildAddressLine(CreditProfileServiceTypes.CreditCheckAddress creditCheckAddress)
    {
        String address = '';

        if ( creditCheckAddress != null )
        {
            if ( String.isNotBlank(creditCheckAddress.addressLineOneTxt) )
            {
                address += creditCheckAddress.addressLineOneTxt;
            }

            if ( String.isNotBlank(creditCheckAddress.addressLineTwoTxt) )
            {
                address += ' ' + creditCheckAddress.addressLineTwoTxt;
            }

            if ( String.isNotBlank(creditCheckAddress.addressLineThreeTxt) )
            {
                address += ' ' + creditCheckAddress.addressLineThreeTxt;
            }

            if ( String.isNotBlank(creditCheckAddress.addressLineFourTxt) )
            {
                address += ' ' + creditCheckAddress.addressLineFourTxt;
            }

            if ( String.isNotBlank(creditCheckAddress.addressLineFiveTxt) )
            {
                address += ' ' + creditCheckAddress.addressLineFiveTxt;
            }

            address = address.trim();
        }

        return address;
    }

    private static CreditProfileServiceTypes.IndividualCustomerProfile buildIndividualCustomerProfile(CreateConsumerCreditProfileRequest request)
    {
        CreditProfileServiceTypes.IndividualCustomerProfile customerProfile = new CreditProfileServiceTypes.IndividualCustomerProfile();

        customerProfile.firstName = request.firstName;
        customerProfile.middleName = request.middleName;
        customerProfile.lastName = request.lastName;

        customerProfile.homePhoneNum = request.homePhoneNumber;
        customerProfile.cellPhoneNum = request.cellPhoneNumber;
        customerProfile.emailTxt = request.emailTxt;

        return customerProfile;
    }

    private static CreditProfileServiceTypes.CreditCheckAddress buildCreditCheckAddress(CreateConsumerCreditProfileRequest request)
    {
        CreditProfileServiceTypes.CreditCheckAddress address = null;

        String creditCheckConsentCd = request.creditCheckConsentCd;

        if ( String.isNotBlank(creditCheckConsentCd) && 'Y'.equalsIgnoreCase(creditCheckConsentCd) )
        {
            address = new CreditProfileServiceTypes.CreditCheckAddress();

            address.addressLineOneTxt = request.addressLine;
            address.cityName = request.city;
            address.provinceCd = request.provinceStateCode;
            address.postalCd = request.postalCode;
            address.countryCd = request.countryCode;
        }

        return address;
    }

    private static CreditProfileServiceTypes.IndividualPersonalInfo buildIndividualPersonalInfo(CreateConsumerCreditProfileRequest request)
    {
        CreditProfileServiceTypes.IndividualPersonalInfo personalInfo = new CreditProfileServiceTypes.IndividualPersonalInfo();

        personalInfo.birthDate = request.birthdate;
        personalInfo.creditCheckConsentCd = request.creditCheckConsentCd;

        return personalInfo;
    }

    private static CreditProfileServiceTypes.IndividualCreditProfileBase buildIndividualCreditProfileBase(CreateConsumerCreditProfileRequest request)
    {
        CreditProfileServiceTypes.IndividualCreditProfileBase creditProfileBase = new CreditProfileServiceTypes.IndividualCreditProfileBase();

        creditProfileBase.applicationProvinceCd = request.applicationProvinceCd;
        creditProfileBase.creditAddress = buildCreditCheckAddress(request);
        creditProfileBase.personalInfo = buildIndividualPersonalInfo(request);
        creditProfileBase.creditIdentification = buildIndividualCreditIdentification(request);

        return creditProfileBase;
    }

    private static CreditProfileServiceTypes.IndividualCreditIdentification buildIndividualCreditIdentification(CreateConsumerCreditProfileRequest request)
    {
        CreditProfileServiceTypes.IndividualCreditIdentification creditIdentification = new CreditProfileServiceTypes.IndividualCreditIdentification();

        creditIdentification.sin = request.sin;
        creditIdentification.driverLicense = buildDriversLicense(request);
        creditIdentification.provincialIdCard = buildProvincialIdCard(request);
        creditIdentification.passport = buildPassport(request);
        creditIdentification.healthCard = buildHealthCard(request);

        return creditIdentification;
    }

    private static CreditProfileServiceTypes.DriverLicense buildDriversLicense(CreateConsumerCreditProfileRequest request)
    {
        CreditProfileServiceTypes.DriverLicense driverLicense = null;

        if ( String.isNotBlank(request.driversLicenseNum) )
        {
            driverLicense = new CreditProfileServiceTypes.DriverLicense();

            driverLicense.driverLicenseNum = request.driversLicenseNum;
            driverLicense.provinceCd = request.driversLicenseProvinceCd;
        }

        return driverLicense;
    }

    private static CreditProfileServiceTypes.ProvincialIdCard buildProvincialIdCard(CreateConsumerCreditProfileRequest request)
    {
        CreditProfileServiceTypes.ProvincialIdCard provincialIdCard = null;

        if ( String.isNotBlank(request.provincialIdNum) )
        {
            provincialIdCard = new CreditProfileServiceTypes.ProvincialIdCard();

            provincialIdCard.provincialIdNum = request.provincialIdNum;
            provincialIdCard.provinceCd = request.provincialIdProvinceCd;
        }

        return provincialIdCard;
    }

    private static CreditProfileServiceTypes.Passport buildPassport(CreateConsumerCreditProfileRequest request)
    {
        CreditProfileServiceTypes.Passport passport = null;

        if ( String.isNotBlank(request.passportNum) )
        {
            passport = new CreditProfileServiceTypes.Passport();

            passport.passportNum = request.passportNum;
            passport.countryCd = request.passportCountry;
        }

        return passport;
    }

    private static CreditProfileServiceTypes.HealthCard buildHealthCard(CreateConsumerCreditProfileRequest request)
    {
        CreditProfileServiceTypes.HealthCard healthCard = null;

        if ( String.isNotBlank(request.healthCardNum) )
        {
            healthCard = new CreditProfileServiceTypes.HealthCard();

            healthCard.healthCardNum = request.healthCardNum;
        }

        return healthCard;
    }

    public static void writeToWebserviceIntegrationErrorLog(String apexMethodPath, String webserviceName, CalloutException e)
    {
        Webservice_Integration_Error_Log__c errorLog = new Webservice_Integration_Error_Log__c();

        errorLog.Apex_Class_and_Method__c = apexMethodPath;
        errorLog.Webservice_Name__c = webserviceName;
        errorLog.Error_Code_and_Message__c = getErrorCode(e);
        errorLog.Stack_Trace__c = e.getMessage() + e.getStackTraceString();

        insert errorLog;
    }

    private static String getErrorCode(CalloutException e)
    {
        String totalStr = e.getMessage();
        String substringAfterLastSeparator = totalStr.substringAfterLast('faultcode=');
        String errorCode = substringAfterLastSeparator.substringBeforeLast('fault');

        return errorCode;
    }

    global class CreditSearchParamForIndividualProfile
    {
        public String sin {set; get;}
        public String driversLicenseNum {set; get;}
        public String driversLicenseProvinceCd {set; get;}
        public String provincialIdNum {set; get;}
        public String provincialIdProvinceCd {set; get;}
        public String passportNum {set; get;}
        public String passportCountry {set; get;}
        public String healthCardNum {set; get;}
    }

    global class CreditSearchProfileResult
    {
        public Long profileId {set; get;}
        public String name {set; get;}          //Concatenated name for display
        public String firstName {set; get;}
        public String middleName {set; get;}
        public String lastName {set; get;}
        public String homePhoneNumber {set; get;}
        public String cellPhoneNumber {set; get;}
        public String emailTxt {set; get;}
        public String addressLine {set; get;}
        public String city {set; get;}
        public String provinceStateCode {set; get;}
        public String postalCode {set; get;}
        public String countryCode {set; get;}
        public Date birthdate {set; get;}
        public String creditCheckConsentCd {set; get;}
        public String sin {set; get;}
        public String driversLicenseNum {set; get;}
        public String driversLicenseProvinceCd {set; get;}
        public String provincialIdNum {set; get;}
        public String provincialIdProvinceCd {set; get;}
        public String passportNum {set; get;}
        public String passportCountry {set; get;}
        public String healthCardNum {set; get;}
    }

    global class CreateConsumerCreditProfileRequest
    {
        public String applicationProvinceCd;
        public String creditCheckConsentCd;
        public String firstName {set; get;}
        public String middleName {set; get;}
        public String lastName {set; get;}
        public String homePhoneNumber {set; get;}
        public String cellPhoneNumber {set; get;}
        public String emailTxt {set; get;}
        public String addressLine {set; get;}
        public String city {set; get;}
        public String provinceStateCode {set; get;}
        public String postalCode {set; get;}
        public String countryCode {set; get;}
        public Date birthdate {set; get;}
        public String sin {set; get;}
        public String driversLicenseNum {set; get;}
        public String driversLicenseProvinceCd {set; get;}
        public String provincialIdNum {set; get;}
        public String provincialIdProvinceCd {set; get;}
        public String passportNum {set; get;}
        public String passportCountry {set; get;}
        public String healthCardNum {set; get;}
    }
}