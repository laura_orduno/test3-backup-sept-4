@isTest
public class CreditChecklistRelatedInfo_Test {
    Public static Account acc;
    Public static Credit_Profile__c creditProfile;
    Public static Credit_Assessment__c creditAssessment;
    Public static Credit_checklist__c CC;
    
    public static testMethod void testBillingAndHistoricalRecord(){        
        //Setup Test Data
        setupTestData();
        insert new Credit_ID_Mapping__c(Name='Credit Checklist', Field_ID__c='00Ne0000001JO6F');

        PageReference pageRef = Page.Billing_Historical_Record;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id',String.valueOf(cc.id));
        
        //Page controller
        ApexPages.StandardController sc = new ApexPages.StandardController(CC);
        CreditChecklistRelatedInfo_Controller controller = new CreditChecklistRelatedInfo_Controller(sc);
        CreditChecklistRelatedInfo_Controller setController = new CreditChecklistRelatedInfo_Controller(controller.recsBHR);
        
        System.assertEquals('CHK123',setController.checklistName);        
        
        //Check that list size (in this case should be 0)        
        List<Credit_Related_Info__c> CRI_List =setController.getBillingHistoricalRecords();
        System.assertEquals(CRI_List.size(), 0);
    }

    public static testMethod void testDelinquentAccounts(){        
        //Setup Test Data
        setupTestData();
        insert new Credit_ID_Mapping__c(Name='Credit Checklist', Field_ID__c='00Ne0000001JO6F');

        PageReference pageRef = Page.Delinquent_Account;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id',String.valueOf(cc.id));
        
        //Page controller
        ApexPages.StandardController sc = new ApexPages.StandardController(CC);
        CreditChecklistRelatedInfo_Controller controller = new CreditChecklistRelatedInfo_Controller(sc);
        CreditChecklistRelatedInfo_Controller setController = new CreditChecklistRelatedInfo_Controller(controller.recsDA);
        
        System.assertEquals('CHK123',setController.checklistName);        
        
        //Check that list size (in this case should be 0)        
        List<Credit_Related_Info__c> CRI_List =setController.getDelinquentAccounts();
        System.assertEquals(CRI_List.size(), 0);
    }

    public static testMethod void testDirectorHistory(){        
        //Setup Test Data
        setupTestData();
        insert new Credit_ID_Mapping__c(Name='Credit Checklist', Field_ID__c='00Ne0000001JO6F');

        PageReference pageRef = Page.Director_History;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id',String.valueOf(cc.id));
        
        //Page controller
        ApexPages.StandardController sc = new ApexPages.StandardController(CC);
        CreditChecklistRelatedInfo_Controller controller = new CreditChecklistRelatedInfo_Controller(sc);
        CreditChecklistRelatedInfo_Controller setController = new CreditChecklistRelatedInfo_Controller(controller.recsDH);
        
        System.assertEquals('CHK123',setController.checklistName);        
        
        //Check that list size (in this case should be 0)        
        List<Credit_Related_Info__c> CRI_List =setController.getDirectorHistory();
        System.assertEquals(CRI_List.size(), 0);
    }

    public static testMethod void testOtherProfileRecords(){        
        //Setup Test Data
        setupTestData();
        insert new Credit_ID_Mapping__c(Name='Credit Checklist', Field_ID__c='00Ne0000001JO6F');

        PageReference pageRef = Page.Other_Profile_Record;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id',String.valueOf(cc.id));
        
        //Page controller
        ApexPages.StandardController sc = new ApexPages.StandardController(CC);
        CreditChecklistRelatedInfo_Controller controller = new CreditChecklistRelatedInfo_Controller(sc);
        CreditChecklistRelatedInfo_Controller setController = new CreditChecklistRelatedInfo_Controller(controller.recsOPR);
        
        System.assertEquals('CHK123',setController.checklistName);        
        
        //Check that list size (in this case should be 0)        
        List<Credit_Related_Info__c> CRI_List =setController.getOtherProfileRecords();
        System.assertEquals(CRI_List.size(), 0);
    }


    public static testMethod void testRelatedAccount(){        
        //Setup Test Data
        setupTestData();
        insert new Credit_ID_Mapping__c(Name='Credit Checklist', Field_ID__c='00Ne0000001JO6F');

        PageReference pageRef = Page.Related_Account;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id',String.valueOf(cc.id));
        
        //Page controller
        ApexPages.StandardController sc = new ApexPages.StandardController(CC);
        CreditChecklistRelatedInfo_Controller controller = new CreditChecklistRelatedInfo_Controller(sc);
        CreditChecklistRelatedInfo_Controller setController = new CreditChecklistRelatedInfo_Controller(controller.recsRA);
        
        System.assertEquals('CHK123',setController.checklistName);        
        
        //Check that list size (in this case should be 0)        
        List<Credit_Related_Info__c> CRI_List =setController.getRelatedAccounts();
        System.assertEquals(CRI_List.size(), 0);
    }

    public static testMethod void testTradeReferences(){        
        //Setup Test Data
        setupTestData();
        insert new Credit_ID_Mapping__c(Name='Credit Checklist', Field_ID__c='00Ne0000001JO6F');

        PageReference pageRef = Page.Trade_Reference;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id',String.valueOf(cc.id));
        
        //Page controller
        ApexPages.StandardController sc = new ApexPages.StandardController(CC);
        CreditChecklistRelatedInfo_Controller controller = new CreditChecklistRelatedInfo_Controller(sc);
        CreditChecklistRelatedInfo_Controller setController = new CreditChecklistRelatedInfo_Controller(controller.recsTR);
        
        System.assertEquals('CHK123',setController.checklistName);        
        
        //Check that list size (in this case should be 0)        
        List<Credit_Related_Info__c> CRI_List =setController.getTradeReferences();
        System.assertEquals(CRI_List.size(), 0);
    }
    
    public static void setupTestData() {
       acc= new Account(
            Name = 'RCID Test',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            RCID__c = '987654'
        );
        insert acc; 
        creditProfile=new Credit_Profile__c(name='CP-RCID',
                                            is_auto_update__c = true,
                                            account__c=acc.id
        );
        insert creditProfile;
        
        //Insert the CAR
        creditAssessment=new Credit_Assessment__c(CAR_Account__c=acc.id,
                                                   Credit_Profile__c=creditProfile.id);
        insert creditAssessment;
        
        CC= new Credit_Checklist__c(name='CHK123',
                                    credit_assessment_ref_no__c=creditAssessment.id);
        insert CC;
        
    }
}