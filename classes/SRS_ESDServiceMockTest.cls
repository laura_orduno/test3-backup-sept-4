global class SRS_ESDServiceMockTest implements WebServiceMock {
      global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
           
      if(soapAction == 'getCustomerServiceInstanceByName'){
      
        SRS_WS_ESD_ReqResponse.getCustomerServiceInstanceByNameResponse_element responseElement=new SRS_WS_ESD_ReqResponse.getCustomerServiceInstanceByNameResponse_element();
        SRS_WS_ESD_ReqResponse.ServiceInstanceOutput testServiceInstanceOutput=new SRS_WS_ESD_ReqResponse.ServiceInstanceOutput();
        testServiceInstanceOutput.customerServiceId=123456;
        testServiceInstanceOutput.serviceLocationName='testLoc';
        testServiceInstanceOutput.serviceStreetName='testStreet';
        testServiceInstanceOutput.serviceTypeDescription='testType';
        testServiceInstanceOutput.serviceTypeId=78934;
        testServiceInstanceOutput.serviceLevelAgreementId=23456;
        testServiceInstanceOutput.serviceStatusCode='testStatusCode';
        testServiceInstanceOutput.serviceStatusName='testStatusName';
        testServiceInstanceOutput.customerName='testCustName';
        testServiceInstanceOutput.serviceAddressId=1234567;
        testServiceInstanceOutput.serviceLocationId=456789;
        testServiceInstanceOutput.specificLocationDescription='testLocDesc';
        testServiceInstanceOutput.municipalityName='testMunicipal';
        testServiceInstanceOutput.provinceStateLCode='testProv';          
        list<SRS_WS_ESD_ReqResponse.ServiceInstanceOutput> lstServiceInstanceOutput=new list<SRS_WS_ESD_ReqResponse.ServiceInstanceOutput>();
        lstServiceInstanceOutput.add(testServiceInstanceOutput);
        responseElement.customerServiceInstance = lstServiceInstanceOutput;
        response.put('response_x', responseElement);
      }
      
      if(soapAction == 'getCustomerServiceInstanceByCSID'){
      
        SRS_WS_ESD_ReqResponse.getCustomerServiceInstanceByCSIDResponse_element responseElement=new SRS_WS_ESD_ReqResponse.getCustomerServiceInstanceByCSIDResponse_element();
        SRS_WS_ESD_ReqResponse.ServiceInstanceOutput testServiceInstanceOutput=new SRS_WS_ESD_ReqResponse.ServiceInstanceOutput();
        testServiceInstanceOutput.customerServiceId=123456;
        testServiceInstanceOutput.serviceLocationName='testLoc';
        testServiceInstanceOutput.serviceStreetName='testStreet';
        testServiceInstanceOutput.serviceTypeDescription='testType';
        testServiceInstanceOutput.serviceTypeId=78934;
        testServiceInstanceOutput.serviceLevelAgreementId=23456;
        testServiceInstanceOutput.serviceStatusCode='testStatusCode';
        testServiceInstanceOutput.serviceStatusName='testStatusName';
        testServiceInstanceOutput.customerName='testCustName';
        testServiceInstanceOutput.serviceAddressId=1234567;
        testServiceInstanceOutput.serviceLocationId=456789;
        testServiceInstanceOutput.specificLocationDescription='testLocDesc';
        testServiceInstanceOutput.municipalityName='testMunicipal';
        testServiceInstanceOutput.provinceStateLCode='testProv';
        responseElement.customerServiceInstance = testServiceInstanceOutput;
        response.put('response_x', responseElement); 
         
      }
      
    }
  }