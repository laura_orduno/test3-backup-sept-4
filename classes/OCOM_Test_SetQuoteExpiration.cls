@IsTest
public class OCOM_Test_SetQuoteExpiration
{
    static testmethod void insertQuote()
    {
        Opportunity o = new Opportunity();
        o.Name = 'test Opportunity for Quote expiration date set';
        o.StageName = 'NewStage';
        o.CloseDate = Date.today().addDays(1);    
        insert o;
        
        Quote q = new Quote();
        q.OpportunityId = o.Id;
        q.ExpirationDate = date.today();
        q.name = 'test quote for expiration Quote expiration date set';
        q.Status = 'In Progress';
       
        insert q;
        
        Date expDate = [SELECT Id, ExpirationDate FROM Quote WHERE Name = 'test quote for expiration Quote expiration date set' LIMIT 1].ExpirationDate;
        System.Assert(expDate != null);
    }
}