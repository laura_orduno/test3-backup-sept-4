/* Developed By: Rahul Badal, IBM
*  Created Date: 10/08/2014
*  Modified By/date:Adhir Parchure, TechMahindra 10-Oct-2015
*/
@isTest

private class SRS_MyOpenServiceRequests_Test {

    static testMethod void SRS_MyOpenSereviceRequests_test() {
        /*Custom setting add*/
        Holidays__c holidays = new Holidays__c();
        holidays.name = 'Leave';
        holidays.ON__c = TRUE;
        holidays.ISD_Dispatch__c = TRUE;
        holidays.Date__c = system.today();
        insert holidays;
        
         /*Custom setting add*/
        Id SrRecordtypeID =Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Provide/Install').getRecordTypeId();
        contact c= new contact(firstname='aditya',lastname='jamwal');
        insert c;    
        Account objacc=new Account(name='testAcc');
        insert objacc;
        
        Opportunity opp= new Opportunity(Name='OppTest', StageName='Originated', CloseDate=system.today(),Primary_Order_Contact__c=c.id);
        Service_Request__c sereq  = new Service_Request__c(Account_Name__c=objAcc.id, opportunity__c=opp.id,recordtypeid=SrRecordtypeID,DD_Scheduled__c=system.today(),Requested_Date__c=system.today(),Jeopardy_Description__c='test',Jeop_code__c='wewew',Condition_color__c='green', Workflow_Request_Type__c='Solution Control');
        insert Opp;
        insert sereq;        
        
        Service_Request__c sereq1  = new Service_Request__c(Account_Name__c=objAcc.id,opportunity__c=opp.id,Legacy_Orders_count__c='20',Fox__c='1234',Target_Date_Type__c='notnull',Service_Request_Status__c='completed',Target_Date__c=system.today(),Submitted_Date__c=system.now(),recordtypeid=SrRecordtypeID,DD_Scheduled__c=Date.newInstance(1960, 2, 17),Requested_Date__c=system.today(),Jeopardy_Description__c='test',Jeop_code__c='wewew',Condition_color__c='green', DAC_Scheduled__c = system.today().addDays(1), RTI_Scheduled__c = system.today().addDays(3), PTD_Scheduled__c = system.today().addDays(25), Workflow_Request_Type__c='Solution Control');        
        insert sereq1;
                
        Service_Request__c sereq2  = new Service_Request__c(Account_Name__c=objAcc.id, opportunity__c=opp.id,Legacy_Orders_count__c='20',Fox__c='1234',Target_Date_Type__c='notnull',Service_Request_Status__c='completed',Target_Date__c=system.today(),Submitted_Date__c=system.now(),recordtypeid=SrRecordtypeID,DD_Scheduled__c=Date.newInstance(1960, 2, 17),Requested_Date__c=system.today(),Condition_color__c='green', DAC_Scheduled__c = system.today().addDays(1), RTI_Scheduled__c = system.today().addDays(3), PTD_Scheduled__c = system.today().addDays(25), RTI_Actual__c = system.today().addDays(1), Workflow_Request_Type__c='Solution Control');        
        insert sereq2;
        
        SMBCare_Address__c objAddress=new SMBCare_Address__c(Account__c=objAcc.id,State__c='BC',Postal_Code__c='V6B 0M3',
                            Suite_Number__c='3rd',Address_Type__c='Standard', country__c = 'Canada',
                            Street_Number__c='12',Street_Name__c='testStr',street_Direction__c='left',
                            GPS_Coordinates__c='23',Google_Map_Link__c='gmail.com',
                            City__c='Vancouver',Province__c='BC'
                            );
        insert objAddress;
        
        List<SRS_Service_Address__c> lstObjSA=new List<SRS_Service_Address__c>();
        
        SRS_Service_Address__c objSA1=new SRS_Service_Address__c(Location__c='Location A',Building_Name__c='test_buld',
                        Service_Request__c=sereq.id,CLLI__c='123',Address__c=objAddress.id,
                        NPA_NXX__c='123321',Floor__c='2nd',New_Building__c='testNewBuld',Cabinet__c='2nd',Rack__c='23',Shelf__c='12'
            );
        lstObjSA.add(objSA1);
        SRS_Service_Address__c objSA2=new SRS_Service_Address__c(Location__c='Location A',Building_Name__c='test_buld',
                        Service_Request__c=sereq1.id,CLLI__c='123',Address__c=objAddress.id,
                        NPA_NXX__c='123321',Floor__c='2nd',New_Building__c='testNewBuld',Cabinet__c='2nd',Rack__c='23',Shelf__c='12'
            );
        lstObjSA.add(objSA2);
        
        SRS_Service_Address__c objSA3=new SRS_Service_Address__c(Location__c='Location A',Building_Name__c='test_buld',
                        Service_Request__c=sereq2.id,CLLI__c='123',Address__c=objAddress.id,
                        NPA_NXX__c='123321',Floor__c='2nd',New_Building__c='testNewBuld',Cabinet__c='2nd',Rack__c='23',Shelf__c='12'
            );
        lstObjSA.add(objSA3);
        
        insert lstObjSA;
        
    // Start - Code to cover ISD Bamboo TC ISD 1780918  code changes to  smb_ServiceRequestTrigger trigger
    Id SrRecordtypeID1 =Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Prequal').getRecordTypeId();
        Service_Request__c seRequest  = new Service_Request__c(Account_Name__c=objAcc.id,opportunity__c=opp.id,Legacy_Orders_count__c='20',Fox__c='1234',Target_Date_Type__c='Prequal Requested',Service_Request_Status__c='completed',Target_Date__c=system.today(),Submitted_Date__c=system.now(),recordtypeid=SrRecordtypeID1,DD_Scheduled__c=Date.newInstance(1960, 2, 17),Requested_Date__c=system.today(),Jeopardy_Description__c='test',Jeop_code__c='wewew',Condition_color__c='green', DAC_Scheduled__c = system.today().addDays(1), RTI_Scheduled__c = system.today().addDays(3), PTD_Scheduled__c = system.today().addDays(25), Workflow_Request_Type__c='Solution Control');        
        insert seRequest;
    Test.startTest();
    SrRecordtypeID1 =Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Provide/Install').getRecordTypeId();
    seRequest.recordtypeid=SrRecordtypeID1;
        try{
            update seRequest;
        }
        catch(exception e){}
    Test.stopTest();
    // End - Code to cover ISD Bamboo TC ISD 1780918  code changes to  smb_ServiceRequestTrigger trigger
    
    
        SRS_MyOpenSereviceRequests_Controller.SerReq objSerReq=new SRS_MyOpenSereviceRequests_Controller.SerReq();
        List<SRS_MyOpenSereviceRequests_Controller.SerReq> SerReqL = new List<SRS_MyOpenSereviceRequests_Controller.SerReq>();
        objSerReq.serReq =sereq;
        objSerReq.Recordtypename='prequal';
        SerReqL.add(objSerReq);
        SerReqL.sort();
        
        //ApexPAges.StandardController sc = new ApexPages.StandardController(opp);
        SRS_MyOpenSereviceRequests_Controller src= new SRS_MyOpenSereviceRequests_Controller();
        src.SerReqList=SerReqL;
        src.getSortDirection();
        src.SortRelatedListwithColumn();
        src.setSortDirection('DESC');       
        src.getSerReqList();
        src.getSRList();
        //Date schdate = date1.addDays(5); 
        List<Holidays__c> lstHolidays = Holidays__c.getAll().Values();
        src.findNoOfWorkingDays(system.today(), system.today().addDays(83));
        src.findNoOfHolidays(system.today(), system.today().addDays(83), 'ON', lstHolidays );
        
        SerReqL.sort();
        
        //................Sorting by Last Modified Date....................
        
        SRS_MyOpenSereviceRequests_Controller.SORT_FIELD='LastModifiedDate';
        SRS_MyOpenSereviceRequests_Controller.SerReq req1= new SRS_MyOpenSereviceRequests_Controller.SerReq();
        req1.SerReq=sereq1;
        SerReqL.add(req1);
        SerReqL.sort();
      
        //................Sorting by Submitted Date.................... 
        
        SRS_MyOpenSereviceRequests_Controller.SORT_FIELD='Submitted_Date__c';
        SRS_MyOpenSereviceRequests_Controller.SerReq req2= new SRS_MyOpenSereviceRequests_Controller.SerReq();
        req2.SerReq=sereq1; 
        SerReqL.add(req2);
        SerReqL.sort();      
        
        //................Sorting by DD Scheduled....................   
    /*    
        SRS_MyOpenSereviceRequests_Controller.SORT_FIELD='DD_Scheduled__c';
        SRS_MyOpenSereviceRequests_Controller.SerReq req3= new SRS_MyOpenSereviceRequests_Controller.SerReq();
        req3.SerReq=sereq1;
        SerReqL.add(req3);
        SerReqL.sort();
    */  
        //................Sorting by Requested Date....................   
        
        SRS_MyOpenSereviceRequests_Controller.SORT_FIELD='Requested_Date__c';
        SRS_MyOpenSereviceRequests_Controller.SerReq req4= new SRS_MyOpenSereviceRequests_Controller.SerReq();
        req4.SerReq=sereq1;
        SerReqL.add(req4);
        SerReqL.sort();
        
        //................Sorting by Service Request Status.................... 
        
        SRS_MyOpenSereviceRequests_Controller.SORT_FIELD='Service_Request_Status__c';
        SRS_MyOpenSereviceRequests_Controller.SerReq req5= new SRS_MyOpenSereviceRequests_Controller.SerReq();
        req5.SerReq=sereq1;
        SerReqL.add(req5);
        SerReqL.sort();
        
        //................Sorting by Service Request Type....................   
        
        SRS_MyOpenSereviceRequests_Controller.SORT_FIELD='Service_Request_Type__c';
        SRS_MyOpenSereviceRequests_Controller.SerReq req6= new SRS_MyOpenSereviceRequests_Controller.SerReq();
        req6.SerReq=sereq1;
        req6.Recordtypename='prequal';      
        SerReqL.add(req6);
        SerReqL.sort();  
        
        //................Sorting by Fox....................            
        
        SRS_MyOpenSereviceRequests_Controller.SORT_FIELD='Fox__c';
        SRS_MyOpenSereviceRequests_Controller.SerReq req7= new SRS_MyOpenSereviceRequests_Controller.SerReq();
        req7.SerReq=sereq1;
        SerReqL.add(req7);
        SerReqL.sort();
        
        //................Sorting by Name....................   
        
        SRS_MyOpenSereviceRequests_Controller.SORT_FIELD='Name';
        SRS_MyOpenSereviceRequests_Controller.SerReq req8= new SRS_MyOpenSereviceRequests_Controller.SerReq();
        req8.SerReq=sereq1;
        SerReqL.add(req8);
        SerReqL.sort();
        
        //................Sorting by Legacy Order count....................
        
        SRS_MyOpenSereviceRequests_Controller.SORT_FIELD='Legacy_Order_count__c';
        SRS_MyOpenSereviceRequests_Controller.SerReq req9= new SRS_MyOpenSereviceRequests_Controller.SerReq();
        req9.SerReq=sereq1;
        SerReqL.add(req9);
        SerReqL.sort();
        
         //................Sorting by Account_Name__r.name....................        
        SRS_MyOpenSereviceRequests_Controller.SORT_FIELD='Account_Name__r.name';  
         SRS_MyOpenSereviceRequests_Controller.SerReq req10= new SRS_MyOpenSereviceRequests_Controller.SerReq();
        SerReqL.add(req10);
        SerReqL.sort();
        
        //................Sorting by Condition_Sort_Index__c....................        
        SRS_MyOpenSereviceRequests_Controller.SORT_FIELD='Condition_Sort_Index__c';       
         SRS_MyOpenSereviceRequests_Controller.SerReq req11= new SRS_MyOpenSereviceRequests_Controller.SerReq();
        SerReqL.add(req11);
        SerReqL.sort();
        
         //................Sorting by Requested_Date__c....................                
        SRS_MyOpenSereviceRequests_Controller.SORT_FIELD='Requested_Date__c';  
        SRS_MyOpenSereviceRequests_Controller.SerReq req12= new SRS_MyOpenSereviceRequests_Controller.SerReq();
        SerReqL.add(req12);
        SerReqL.sort();
        
        //................Sorting by DD_Scheduled__c....................        
        SRS_MyOpenSereviceRequests_Controller.SORT_FIELD='DD_Scheduled__c';    
         SRS_MyOpenSereviceRequests_Controller.SerReq req13= new SRS_MyOpenSereviceRequests_Controller.SerReq();
        SerReqL.add(req13);
        SerReqL.sort();
        
       //................Sorting by Order_ID__c....................        
        SRS_MyOpenSereviceRequests_Controller.SORT_FIELD='Order_ID__c';    
         SRS_MyOpenSereviceRequests_Controller.SerReq req14= new SRS_MyOpenSereviceRequests_Controller.SerReq();
        SerReqL.add(req14);
        SerReqL.sort();
        
        //................Sorting by Jeopdary_Image__c....................        
        SRS_MyOpenSereviceRequests_Controller.SORT_FIELD='Jeopdary_Image__c'; 
         SerReqL.sort();
        
        //................Sorting by Product_Type__c....................        
        SRS_MyOpenSereviceRequests_Controller.SORT_FIELD='Product_Type__c'; 
         SerReqL.sort();
        
        SRS_MyOpenSereviceRequests_Controller reqCtrl = new SRS_MyOpenSereviceRequests_Controller();
        reqCtrl.SRId ='abc';
        reqCtrl.showRelatedList = true;
        
        //................Sorting by Description__c....................        
        SRS_MyOpenSereviceRequests_Controller.SORT_FIELD='Description__c'; 
         SerReqL.sort();
        
        //................Sorting by ServiceRequest.serReq.Target_Date_Type__c....................        
        SRS_MyOpenSereviceRequests_Controller.SORT_FIELD='Target_Date_Type__c'; 
         SerReqL.sort();
        
         SRS_MyOpenSereviceRequests_Controller.SORT_FIELD='ContractNumber';
         SRS_MyOpenSereviceRequests_Controller.SerReq req15 = new SRS_MyOpenSereviceRequests_Controller.SerReq();
         SerReqL.add(req15);
         SerReqL.sort();
         
         SRS_MyOpenSereviceRequests_Controller.SORT_FIELD='Status'; 
         SRS_MyOpenSereviceRequests_Controller.SerReq req16 = new SRS_MyOpenSereviceRequests_Controller.SerReq();
         SerReqL.add(req16);
         SerReqL.sort();
        
        SRS_MyOpenSereviceRequests_Controller.SORT_FIELD='Critical_Notes__c'; 
        SRS_MyOpenSereviceRequests_Controller.SerReq req17 = new SRS_MyOpenSereviceRequests_Controller.SerReq();
         SerReqL.add(req17);
         SerReqL.sort();
         
         SRS_MyOpenSereviceRequests_Controller.SORT_FIELD='Service_Address__c'; 
         SRS_MyOpenSereviceRequests_Controller.SerReq req18 = new SRS_MyOpenSereviceRequests_Controller.SerReq();
         req18.SerReq=sereq1;
         SerReqL.clear();
         SerReqL.add(req18);
         
         SRS_MyOpenSereviceRequests_Controller.SerReq req28 = new SRS_MyOpenSereviceRequests_Controller.SerReq();
         req28.SerReq=sereq2;
         SerReqL.add(req28);
         SerReqL.sort();
         
         /*
         SRS_MyOpenSereviceRequests_Controller src1 = new SRS_MyOpenSereviceRequests_Controller();
         SRS_MyOpenSereviceRequests_Controller.SORT_FIELD='Service_Address__c'; 
         SRS_MyOpenSereviceRequests_Controller.SerReq req18 = new SRS_MyOpenSereviceRequests_Controller.SerReq();
         req18.SerReq=sereq2;
         req18.SerReq.Service_Address__r = lstObjSA;
         SerReqL.clear();
         SerReqL.add(req18);
        src1.SerReqList=SerReqL;
        src1.getSortDirection();
        src1.SortRelatedListwithColumn();
        src1.setSortDirection('DESC');       
        src1.getSerReqList();
        src1.getSRList();
        
        SerReqL.sort();
        */
         SRS_MyOpenSereviceRequests_Controller.SORT_FIELD='City__c'; 
         SRS_MyOpenSereviceRequests_Controller.SerReq req19 = new SRS_MyOpenSereviceRequests_Controller.SerReq();
         req19.SerReq=sereq2;
         //SerReqL.clear();
         SerReqL.add(req19);
         SerReqL.sort();
         
         SRS_MyOpenSereviceRequests_Controller.SORT_FIELD='Prov__c';
         SRS_MyOpenSereviceRequests_Controller.SerReq req20 = new SRS_MyOpenSereviceRequests_Controller.SerReq();
         req20.SerReq=sereq2;
         //SerReqL.clear();
         SerReqL.add(req20);         
         SerReqL.sort();
         
         SRS_MyOpenSereviceRequests_Controller.SORT_FIELD='Opportunity__r.name'; 
         SRS_MyOpenSereviceRequests_Controller.SerReq req21 = new SRS_MyOpenSereviceRequests_Controller.SerReq();
         SerReqL.add(req21);
         SerReqL.sort();
         
         SRS_MyOpenSereviceRequests_Controller.SORT_FIELD='Next_to_work__c'; 
         SRS_MyOpenSereviceRequests_Controller.SerReq req26 = new SRS_MyOpenSereviceRequests_Controller.SerReq();
         SerReqL.add(req26);
         SerReqL.sort();
         
         SRS_MyOpenSereviceRequests_Controller.SORT_FIELD='ERD_Scheduled__c'; 
         SRS_MyOpenSereviceRequests_Controller.SerReq req22 = new SRS_MyOpenSereviceRequests_Controller.SerReq();
         SerReqL.add(req22);
         SerReqL.sort();
         
         SRS_MyOpenSereviceRequests_Controller.SORT_FIELD='DAC_Scheduled__c'; 
         SRS_MyOpenSereviceRequests_Controller.SerReq req23 = new SRS_MyOpenSereviceRequests_Controller.SerReq();
         SerReqL.add(req23);
         SerReqL.sort();
         
         SRS_MyOpenSereviceRequests_Controller.SORT_FIELD='RTI_Scheduled__c';
        SRS_MyOpenSereviceRequests_Controller.SerReq req24 = new SRS_MyOpenSereviceRequests_Controller.SerReq();
         SerReqL.add(req24);         
         SerReqL.sort();
         
         SRS_MyOpenSereviceRequests_Controller.SORT_FIELD='PTD_Scheduled__c'; 
         SRS_MyOpenSereviceRequests_Controller.SerReq req25 = new SRS_MyOpenSereviceRequests_Controller.SerReq();
         SerReqL.add(req25);
         SerReqL.sort();
         
         
         SRS_MyOpenSereviceRequests_Controller.SORT_FIELD='Workflow_Request_Type__c'; 
         SRS_MyOpenSereviceRequests_Controller.SerReq req27 = new SRS_MyOpenSereviceRequests_Controller.SerReq();
         SerReqL.add(req27);
         SerReqL.sort();
      }
    
    
}