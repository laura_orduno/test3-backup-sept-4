/* 
###########################################################################
# File..................: smb_moveProductToAsset_sch
# Version...............: 26
# Created by............: Vinay Sharma
# Created Date..........: 24-Apr-2013
# Last Modified by......: 
# Last Modified Date....: 
# Description...........: This batch class is used by smb_moveProductToAsset_sch.
Logic: 	1.Collect all unique quote line items. A quote line item is unique based on combination of product namr and parentQuoteline ID
		2. Create a new asset record based on different values in quoteline item such as pproduct name, price etc.
#
# Copyright (c) 2000-2012. Astadia, Inc. All Rights Reserved.
#
# Created by the Astadia, Inc. Modification must retain the above copyright notice.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any commercial purpose, without fee, and without a written
# agreement from Astadia, Inc., is hereby forbidden. Any modification to source
# code, must include this paragraph and copyright.
#
# Permission is not granted to anyone to use this software for commercial uses.
#
# Contact address: 2839 Paces Ferry Road, Suite 350, Atlanta, GA 30339
# Company URL : http://www.astadia.com
###########################################################################
*/
global class smb_UpdateHierarchyOnAsset_batch implements Database.Batchable<sObject>
{
   global final String query;
   

   global smb_UpdateHierarchyOnAsset_batch(String q)
   {
		query=q;
   }
	
	/**
    * The start method is called at the beginning of a batch Apex job. Use the start method to collect the records or objects to be passed to the interface method execute. 
    * This method returns either a Database.QueryLocator object or an iterable that contains the records or objects being passed into the job.
    */ 
   global Database.QueryLocator start(Database.BatchableContext BC)
   {
      	return Database.getQueryLocator(query);
   }

	/**
    *   The execute method is called for each batch of records passed to the method. Use this method to do all required processing for each chunk of data.
    *   This method reassigns the next approver based on configuration in the custom setting for the region/ product/ approval step.
    *   @praram:-A reference to the Database.BatchableContext object.
    *   @param :- A list of sObjects, such as List<sObject>, or a list of parameterized types. If you are using a Database.QueryLocator, the returned list should be used.
    * 
    */
   global void execute(Database.BatchableContext BC, List<sObject> scope)
   {
   		if(scope.size()>0)
   		updateHierarchy(scope);
   }

	 /*
     * The finish method is called after all batches are processed. 
     * 
     */
   global void finish(Database.BatchableContext BC)
   {

   }
   
      /*
   This method will update Asset record baseed on the required by value in related quoteline item record   
   */
   private static void updateHierarchy(list<Asset> lstAssets)
   {
   		map<ID,ID> mapAssetQuoteLine = new map<ID,ID>();
   		list<Asset> lstToBeUpdatedAsset = new list<Asset>();

   		for (Asset a : lstAssets)	
   		{
   			mapAssetQuoteLine.put(a.SBQQ__QuoteLine__r.SBQQ__RequiredBy__c,a.id) ;  		
   		}
   		
   		for (Asset a : lstAssets)	
   		{
   			if(a.SBQQ__QuoteLine__r.SBQQ__RequiredBy__c != null)
   			{
	   			a.ParentId__c = mapAssetQuoteLine.get(a.SBQQ__QuoteLine__r.SBQQ__RequiredBy__c);
	   			lstToBeUpdatedAsset.add(a);
   			}
   		}
   		
   		try
   		{  
   			if (lstToBeUpdatedAsset.size()>0)
   			{
   				update lstToBeUpdatedAsset;
   			
   			}   			
   			
   		}catch (DmlException e) 
   			{
        		System.debug(e.getMessage());
    		}     		
   }
 
 
   


   
}