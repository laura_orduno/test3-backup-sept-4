/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class OrgChartAPI {
    global OrgChartAPI() {

    }
    @RemoteAction
    global static DMAPP.OrgChartContactRestResource.OrgChartContacts addOrgChartContact(DMAPP.OrgChartContactRestResource.OrgChartContacts occs) {
        return null;
    }
    @RemoteAction
    global static DMAPP.OrgChartContactRestResource.OrgChartContact addPlaceholderContact(String mapId, String reportsToId) {
        return null;
    }
    @RemoteAction
    global static DMAPP.AccountOrgChartContactRestResource.OrgChartContact addPlaceholderContactAM(String mapId, String reportsToId) {
        return null;
    }
    @RemoteAction
    global static void associateAccountLevelMap(String opportunityExtraId, String accountPolMapId) {

    }
    @RemoteAction
    global static void associatePlanUnitAccountOrgChart(String planunitid, String accountPolMapId) {

    }
    @RemoteAction
    global static void importFromAccountOrgChartsToAccountForPlanUnit(String accountmapid, String planunitid, List<String> mapids) {

    }
    @RemoteAction
    global static void importFromAccountOrgCharts(String opportunityExtraId, List<String> mapids) {

    }
    @RemoteAction
    global static void importFromOpportunityOrgChartsToAccountForPlanUnit(String accountmapid, String planunitid, List<String> oppids) {

    }
    @RemoteAction
    global static void importFromOpportunityOrgCharts(String opportunityExtraId, List<String> oppids) {

    }
    @RemoteAction
    global static DMAPP.OrgChartRestResource.OrgChart loadOMOrgChart(String orgChartId) {
        return null;
    }
    @RemoteAction
    global static Object query(String soql) {
        return null;
    }
    @RemoteAction
    global static void removeMultipleOrgChartContactsAM(List<String> contactIds) {

    }
    @RemoteAction
    global static void removeMultipleOrgChartContacts(List<String> contactIds) {

    }
    @RemoteAction
    global static void removeOrgChartContact(String contactId) {

    }
    @RemoteAction
    global static void removeOrgChartContactAM(String id) {

    }
    @RemoteAction
    global static DMAPP.PolMapSortOrderHelper.PolMapSortOrderResponse reorderContact(String mapId, String parentId, List<String> siblingIds, Boolean isAM) {
        return null;
    }
    @RemoteAction
    global static DMAPP.OrgChartRestResource.ContactInfo replacePlaceholder(String mapId, String id, String replacementContactId) {
        return null;
    }
    @RemoteAction
    global static DMAPP.AccountOrgChartRestResource.ContactInfo replacePlaceholderAM(String mapId, String id, String replacementContactId) {
        return null;
    }
    @RemoteAction
    global static void resetMap(String mapId, Boolean isAM) {

    }
    @RemoteAction
    global static DMAPP.OrgChartContactRestResource.OrgChartContacts updateContactAttribute(DMAPP.OrgChartContactRestResource.OrgChartContacts occs) {
        return null;
    }
    @RemoteAction
    global static Contact updateReportsTo(Contact c, String planunitid, String id, Boolean isAccMap, String mapId, String reportsToLinkType) {
        return null;
    }
}
