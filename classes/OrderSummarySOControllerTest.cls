@isTest
public class OrderSummarySOControllerTest {
    private static void createData(){
        System.runAs(new User(Id = Userinfo.getUserId())) {
        List<SMBCare_WebServices__c> custSetting= new List<SMBCare_WebServices__c>();
        SMBCare_WebServices__c wsUsername = new SMBCare_WebServices__c(Name='username',value__c='APP_SFDC'); custSetting.add(wsUsername);
        SMBCare_WebServices__c wsPassword = new SMBCare_WebServices__c(Name='password',value__c='SOAORGID'); custSetting.add(wsPassword);
        SMBCare_WebServices__c SearchDueDate_EndPoint = new SMBCare_WebServices__c(Name='SearchDueDate_EndPoint',value__c='https://xmlgwy-pt1.telus.com:9030/is01/RMO/ProcessMgmt/FieldWorkAppointmentService_v2_0_vs0');
        SMBCare_WebServices__c BookDueDate_EndPoint = new SMBCare_WebServices__c(Name='BookDueDate_EndPoint',value__c='https://xmlgwy-pt1.telus.com:9030/is01/RMO/ProcessMgmt/FieldWorkAssignmentMgmtService_v2_0_vs0');
        custSetting.add(SearchDueDate_EndPoint);custSetting.add(BookDueDate_EndPoint); 
        insert custSetting;
        }
    }
     @isTest
    private static void testOrderSummarySOController(){
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        PageReference pageRef = Page.OC_OrderSummarySingleSite;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('parentOrderId', orderId );
        Order ordObj=[select id from Order where id=:orderId];
        ApexPages.StandardController sc = new ApexPages.standardController(ordObj);
        Test.startTest();
        OrdrTestDataFactory.associateContractWithOrder(orderId);
        List<Order> chOrdObjList=[select id,General_Notes_Remarks__c,Order_Submit_Date__c from Order where parentId__C=:orderId];
        for(Order ord:chOrdObjList){
            ord.General_Notes_Remarks__c='This is a general remark';
            ord.Order_Submit_Date__c=Date.today();
        }
        update chOrdObjList;
        OrdrTestDataFactory.associateContractWithOrder(chOrdObjList.get(0).id);
       	
        OrderSummarySOController cntrl=new OrderSummarySOController(sc);
        try{
            OrderSummarySOController.contractableProductsMap = null;
       		OrderSummarySOController.callContractCreationService(chOrdObjList.get(0).id,'Approved',true,'',false);
            Boolean b1 = cntrl.isContractRegistered;
            Boolean b2 = cntrl.displayUpdateOrder;
        }catch(Exception ex){}

    }
    @isTest
    private static void updateOrder(){
        OrderSummarySOController.updateOrder('test');
    }
    @isTest
    private static void emailOrderConfirmations(){
        OrderSummarySOController.emailOrderConfirmations('test@test.com');
    }
    @isTest
    private static void saveOrderRemarks(){
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();        
        Test.startTest();
        OrderSummarySOController.saveOrderRemarks(orderId, 'test');
        Test.stopTest();
    }
    @isTest
    private static void cancelOrder(){
        createData();
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        Test.startTest();
        OrdrTestDataFactory.associateContractWithOrder(orderId);
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        OrdrTestDataFactory.associateContractWithOrder(orderId);
        OrderSummarySOController.cancelOrder(orderId);
        Test.stopTest();
    }
    @isTest
    private static void submitOrder1(){
         String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        OrdrTestDataFactory.associateContractWithOrder(orderId);
        OrderSummarySOController.submitOrder(orderId,'test remarks', 'testOutlet', 'testsalesrep', 'testsalesreptid', false,'test@test.com');
        Test.stopTest();
    }
     @isTest
    private static void submitOrder2(){
         String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        OrdrTestDataFactory.associateContractWithOrder(orderId);
        OrderSummarySOController.submitOrder(orderId,'test remarks', 'testOutlet', 'testsalesrep', 'testsalesreptid', false,'test@test.com');
        Test.stopTest();
    }
     @isTest
    private static void submitOrder3(){
         String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        OrdrTestDataFactory.associateContractWithOrder(orderId);
        List<Order> childOrdList=[select id from order where parentid__c=:orderId];        
        OrderSummarySOController.submitOrder(orderId,'test remarks', 'testOutlet', 'testsalesrep', 'testsalesreptid', false,'test@test.com');
        Test.stopTest();
    }
     @isTest
    private static void submitOrder4(){
         String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        OrdrTestDataFactory.associateContractWithOrder(orderId);
        List<Order> chOrdObjList=[select id from Order where parentId__C=:orderId];
        OrderSummarySOController.submitOrder(orderId,'test remarks', 'testOutlet', 'testsalesrep', 'testsalesreptid', true,'test@test.com,test1@test.com');
        Test.stopTest();
    }
    @isTest
    private static void fetchChildOrderSummaryDataJson(){
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        String childOrder='';
        List<Order> ordList=[select id,OrderMgmt_BO_Remaraks__c from order where parentid__c=:orderId];
       
        Order chObj = ordList.get(0);
        chObj.OrderMgmt_BO_Remaraks__c='This is a test comment';
        update chObj;
         childOrder=ordList.get(0).id;
        Test.startTest();
        OrderSummarySOController.fetchChildOrderSummaryDataJson(childOrder);
        List<OrderHistory> ohlist = new List<OrderHistory>();
        OrderHistory oh1 = new OrderHistory(OrderId = childOrder, Field = 'Status');
        OrderHistory oh2 = new OrderHistory(OrderId = childOrder, Field = 'OrderMgmt_BO_Remaraks__c');
        OrderHistory oh3 = new OrderHistory(OrderId = childOrder, Field = 'Order_Submit_Date__c');
        ohlist.add(oh1);
        ohlist.add(oh2);
        ohlist.add(oh3);
        insert ohlist;
        OrderSummarySOController.fetchChildOrderSummaryDataJson(childOrder);
        Test.stopTest();
    }
    @isTest
	private static void auxiliaryTest1(){
	String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        PageReference pageRef = Page.OC_OrderSummaryMultiSite;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('parentOrderId', orderId );
	   Test.startTest();
	   OrderSummarySOController.OrderHistoryFriendlyRecord obj1=new OrderSummarySOController.OrderHistoryFriendlyRecord();
	    obj1.AgentFullName='';
        obj1.AgentEmailAddress='';
        obj1.FieldUpdateDateTime=null;
        obj1.FieldUpdateDateTimeText='';
        obj1.FieldName='';
        obj1.OldValue=null;
        obj1.NewValue=null;
		OrderSummarySOController.OrderResponse obj2=new OrderSummarySOController.OrderResponse();
		obj2.validationMessage='';
		
		OrderSummarySOController.ContractDetails obj3=new OrderSummarySOController.ContractDetails();
		obj3.contractNum='';
        obj3.contractAddress='';
        obj3.customerSignor='';
        obj3.signorEmail='';
        obj3.registeredDate=null;
        obj3.expiredDate=null;
		
		OrderSummarySOController.OrderDetails obj4=new OrderSummarySOController.OrderDetails();
		OrderSummarySOController.ServiceCnt obj5=new OrderSummarySOController.ServiceCnt();
		List<OrderSummarySOController.ServiceCnt> cntList=new List<OrderSummarySOController.ServiceCnt>();
		cntList.add(obj5);
		obj4.serviceCnt=cntList;
		obj4.creditModifiedDate=null;
		
		OrderSummarySOController cntrl=new OrderSummarySOController();
		//System.debug(cntrl.opportunityWorkRequestJson);
		//System.debug(cntrl.accountWorkRequestJson);
		//System.debug(cntrl.workRequestRecordTypeId);
		//System.debug(cntrl.orderWorkRequestJson);
		
		cntrl.childOrderDataJson='';
		cntrl.masterOrderDataJson='';
		cntrl.sendEmailFlag=false;
		cntrl.selectedEmailIds='';
		cntrl.ordrIdStr='';
		cntrl.validBillingAddress=false;
		cntrl.validShippingAddress=false;
		cntrl.validCreditAssessment=false;
		cntrl.validContract=false;
		
		
		Test.stopTest();
	}
    
    @isTest
	private static void amendOrder(){
		String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        PageReference pageRef = Page.OC_OrderSummaryMultiSite;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('parentOrderId', orderId );
        Order ordObj=[select id from Order where id=:orderId];
        ApexPages.StandardController sc = new ApexPages.standardController(ordObj);
		Test.startTest();
		OrdrTestDataFactory.associateContractWithOrder(orderId);
		OrderSummarySOController cntrl=new OrderSummarySOController(sc);
		cntrl.amendOrder();
		Test.stopTest();
	}
    
     @isTest
    private static void isDueDateBookingComplete(){
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        Order childOrder=null;
        List<Order> ordList=[select (select start_date__c,install_type__c, status__c, time_slot__c, 
                                 lastmodifieddate, Product_Technology_Code__c, Job_Type__c, 
                                 Permanent_Comments__c, Duration__c, WFM_Number__c,
                                 Scheduled_Due_Date_Location__c, Onsite_Contact_HomePhone__c,
                                 Onsite_Contact_Name__c, Onsite_Contact_Email__c,
                                 Onsite_Contact_CellPhone__c, Location_Contact__c, Scheduled_Datetime__c from Work_Orders__r ORDER BY CREATEDDATE DESC limit 1),id,captureLatestOnsiteContact__c from order where parentid__c=:orderId];
        String jsonStr='{ 	"sameAsOrderContact": false, 	"onsitePhoneMoveOut": "", 	"onsitePhone": "123123123", 	"onsiteEmailMoveOut": "", 	"onsiteEmail": "test@test.com", 	"onsiteContactMoveOut": "", 	"onsiteContact": "test", 	"installRemarksMoveOut": "", 	"installRemarks": "" }';
        for(Order chOrd:ordList){
            chOrd.captureLatestOnsiteContact__c=jsonStr;
        }
        update ordList;
        childOrder=ordList.get(0);
        
        Test.startTest();
        OrdrTestDataFactory.associateContractWithOrder(orderId);
        OrderSummarySOController cntrl=new OrderSummarySOController();
        cntrl.isDueDateBookingComplete(childOrder);
        Test.stopTest();
    }
    @isTest
    private static void isDueDateBookingComplete1(){
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        Order childOrder=null;
        List<Order> ordList=[select (select start_date__c,install_type__c, status__c, time_slot__c, 
                                 lastmodifieddate, Product_Technology_Code__c, Job_Type__c, 
                                 Permanent_Comments__c, Duration__c, WFM_Number__c,
                                 Scheduled_Due_Date_Location__c, Onsite_Contact_HomePhone__c,
                                 Onsite_Contact_Name__c, Onsite_Contact_Email__c,
                                 Onsite_Contact_CellPhone__c, Location_Contact__c, Scheduled_Datetime__c from Work_Orders__r ORDER BY CREATEDDATE DESC limit 1),id,captureLatestOnsiteContact__c from order where parentid__c=:orderId];
        
        childOrder=ordList.get(0);
        
        Test.startTest();
        OrdrTestDataFactory.associateContractWithOrder(orderId);
        OrderSummarySOController cntrl=new OrderSummarySOController();
        cntrl.isDueDateBookingComplete(childOrder);
        Test.stopTest();
    }
    @isTest
    private static void isDueDateBookingComplete2(){
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        List<Work_Order__c> wList=[select order__c,id from work_order__c];
        for(Work_Order__c wo:wList){
            wo.order__c=null;
        }
        update wList;
        Order childOrder=null;
        List<Order> ordList=[select (select start_date__c,install_type__c, status__c, time_slot__c, 
                                 lastmodifieddate, Product_Technology_Code__c, Job_Type__c, 
                                 Permanent_Comments__c, Duration__c, WFM_Number__c,
                                 Scheduled_Due_Date_Location__c, Onsite_Contact_HomePhone__c,
                                 Onsite_Contact_Name__c, Onsite_Contact_Email__c,
                                 Onsite_Contact_CellPhone__c, Location_Contact__c, Scheduled_Datetime__c from Work_Orders__r ORDER BY CREATEDDATE DESC limit 1),id,captureLatestOnsiteContact__c from order where parentid__c=:orderId];
        
        childOrder=ordList.get(0);
        
        Test.startTest();
        OrdrTestDataFactory.associateContractWithOrder(orderId);
        OrderSummarySOController cntrl=new OrderSummarySOController();
        cntrl.isDueDateBookingComplete(childOrder);
        Test.stopTest();
    }
	
    /*
* public static Order ordObjFromContext=null;
public static Map<String, vlocity_cmt__CatalogProductRelationship__c> contractableProductsMap;
public OrderSummarySOController(ApexPages.StandardController stdController){
public OrderSummarySOController(){
public  PageReference amendOrder() {
public class OrderDetails{
public class ServiceTerm{
public class CarDetails{
public class ContractDetails{
public class ServiceCnt{
public static List<Order> getOrder(String orderId) {
public class OrderResponse{
public class OrderHistoryFriendlyRecord{
*/
}