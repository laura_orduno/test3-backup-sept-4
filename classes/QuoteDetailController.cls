public without sharing class QuoteDetailController {
    
    public QuoteVO quote {get; private set;}
    public Web_Account__c account {get; private set;}
    public List<QuoteLineVO> products {get; private set;}
    public List<ContactVO> contacts {get; private set;}
    public List<AgreementVO> agreements {get; private set;}
    
    public String quoteDetailLink {get; private set;}
    public Boolean multiSite {get; private set;}
    public Address__c installLocation {get; private set;}
    public Boolean sendAgreementDisabled {get{return isSendAgreementDisabled();}}
    public Boolean creditCheckDisabled {get{return isCreditCheckDisabled();}}
    
    public Boolean dealerUser {get{return QuotePortalUtils.isUserDealer();}}
    public Boolean isChannelInbound {get{ return Test.isRunningTest() ? true : QuotePortalUtils.getChannel().equalsIgnoreCase('inbound'); }}
    public Boolean isChannelOutbound {get{ return Test.isRunningTest() ? true : QuotePortalUtils.getChannel().equalsIgnoreCase('outbound'); }}
    
    public PicklistValuesOptionModel preferredInstallationTime {get; private set;}
    public PicklistValuesOptionModel quoteStatusOptions {get; private set;}
    public PicklistValuesOptionModel assignedAccountOrder {get; private set;}
    public Boolean sendEmailDisabled {get{return isSendEmailDisabled();}}
    //public Boolean quoteHasMissingInformation {get{return quoteHasMissingInformation();}}
    
    private String returnURL;
    private Boolean detailView = true;
    
    public transient SBQQ__Quote__c quoteRec {get; set;}
    
    public Boolean eBANreq {get; set;}
    public Boolean eCANreq {get; set;}
    public Boolean eUnserviceableReq {get; set;}
    
    public class Status {}
    
    public QuoteDetailController() {
        try {
        Id quoteId = ApexPages.currentPage().getParameters().get('qid');
        returnURL = ApexPages.currentPage().getParameters().get('retURL');
        detailView = (ApexPages.currentPage().getParameters().get('pd') == '1');
        init(quoteId);
        preferredInstallationTime = new PicklistValuesOptionModel(SBQQ__Quote__c.Preferred_Installation_Time__c);
        assignedAccountOrder = new PicklistValuesOptionModel(SBQQ__Quote__c.Assigned_Account_Order__c);
        
        // If page messages are specified in the URL, display them
        String messagesURLen = ApexPages.currentPage().getParameters().get('messages');
        if (messagesURLen != null) { String[] messages = EncodingUtil.urlDecode(messagesURLen, 'UTF-8').split('\\|'); if (messages != null && messages.size() != 0) { for (String s : messages) { ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.INFO,  s) ); } } }
        initQuoteStatusOptions();
        
        eBANreq = false;
        eCANreq = false;
        eUnserviceableReq = false; } catch (Exception e) {QuoteUtilities.log(e);}
    }
    
    /**
     * Constructor used by component that generates fulfillment emails
     */
    public QuoteDetailController(Id quoteId) {
        try { init(quoteId); } catch (Exception e) {QuoteUtilities.log(e);}
    }
    
    // Updated
    private void initQuoteStatusOptions() {
        try{
        quoteStatusOptions = new PicklistValuesOptionModel(SBQQ__Quote__c.SBQQ__Status__c);
        for (SelectOption opt : quoteStatusOptions.getOptions()) {
            opt.setDisabled(true);
            QuoteStatus.Permissions permissions = QuoteStatus.permissions.get(opt.getValue().toLowerCase());
            if (permissions != null) {
                if (QuotePortalUtils.isExperienceExcellenceUser() && permissions.isSelectableForEE) {
                    opt.setDisabled(false);
                } else if (QuotePortalUtils.isUserDealer() == false && permissions.isSelectable) {
                    opt.setDisabled(false);
                } else if (QuotePortalUtils.isUserDealer() && permissions.isSelectableForDealer) {
                    opt.setDisabled(false);
                }
            }
        } } catch (Exception e) {QuoteUtilities.log(e);}
    }
    
    public Boolean isSendAgreementDisabled() {
        try {
        if (quote.isAddon && (account.Credit_Assessment_Approved__c == null || account.Credit_Assessment_Approved__c != 'Yes')) {
            return true;
        }
        return (quote.agreementExists && !QuotePortalUtils.isExperienceExcellenceUser() && quote.status != 'Sent agreement, waiting acceptance') || // Traction change for TC12020
            quote.status == 'Customer Signed Agreement in Person' || 
            quote.status == 'Client Declined' ||
            (account.Credit_Check_Status__c != 'Submitted' && account.Credit_Reference__c == null) ||
            (account.Credit_Check_Status__c != 'Submitted' && account.Credit_Check_Status__c != 'Approved' && account.Credit_Check_Status__c != 'Pre-Approved Credit'); } catch (Exception e) {QuoteUtilities.log(e);} return null;
    }
    
    public Boolean isCreditCheckDisabled() {
        if (quote == null || account == null) { return false; }
        if (quote.isAddon) {
            return quote.creditCheckRequested || quote.status == 'Client Declined' || account.Credit_Assessment_Approved__c != null;
        } else {
            return quote.creditCheckRequested ||  quote.status == 'Client Declined' || (account.Credit_Check_Status__c == 'Submitted' && account.Credit_Reference__c == null) || ( account.Credit_Reference__c != null && (account.Credit_Check_Status__c == 'Submitted' || account.Credit_Check_Status__c == 'Approved' || account.Credit_Check_Status__c == 'Pre-Approved Credit'));
        } return false;
    }
    
    public Boolean isSendEmailDisabled() {
        if (quote == null || quote.record == null) { return false; }
        return quote.record.Email_Enabled__c.equalsIgnoreCase('no') || (quote.status == 'Client Declined');
    }
    
    public String getDetailPageURL() {
        try {
        PageReference pref = Page.QuoteDetail;
        pref.getParameters().put('qid', quote.id);
        if (multiSite) {
            pref.getParameters().put('ms','1');
        }
        pref.getParameters().put('step', ApexPages.currentPage().getParameters().get('step'));
        pref.getParameters().put('addon', ApexPages.currentPage().getParameters().get('addon'));
        pref.setRedirect(true);
        return pref.getUrl(); } catch (Exception e) {QuoteUtilities.log(e);} return null;
    }
    
    public String getSummaryPageURL() {
        try {
        PageReference pref = Page.QuoteSummary;
        pref.getParameters().put('qid', quote.id);
        if (multiSite) { pref.getParameters().put('ms','1'); }
        pref.getParameters().put('step', ApexPages.currentPage().getParameters().get('step'));
        pref.getParameters().put('addon', ApexPages.currentPage().getParameters().get('addon'));
        pref.setRedirect(true);
        return pref.getUrl(); } catch (Exception e) {QuoteUtilities.log(e);} return null;
    }
    public void setSummaryPageURL(string summary) {}
    
    /*
    public Boolean quoteHasMissingInformation() {
        //return false;
        //try {
        //Id[] qids = new Id[]{quote.id};
        //return QuotePortalUtils.quotesHaveDevicesWithMissingInformation(qids).get(quote.id); } catch (Exception e) {QuoteUtilities.log(e);} return null;
    }
    */
    
    private void init(Id quoteId) {
        try {
            quote = new QuoteVO(QuoteUtilities.loadQuote(quoteId));
            if (quote.status.equalsIgnoreCase('Unserviceable')) { eUnserviceableReq = true; }
            products = new List<QuoteLineVO>();
            for (SBQQ__QuoteLine__c line : QuotePortalUtils.loadQuoteLines(quoteId, detailView)) {
                products.add(new QuoteLineVO(line));
            }
            products = QuotePortalUtils.sortLines(products);
            
            Id accountId = quote.record.SBQQ__Opportunity__r.Web_Account__c;
            
            QuoteAccountController qac = new QuoteAccountController();
            account = qac.selectedAccount;
            
            multiSite = quote.record.Group__c != null;
            if (quote.record.Install_Location__c != null) {
                installLocation = QuoteUtilities.loadAddressById(quote.record.Install_Location__c);
            }
            
            List<Agreement__c> sobjectagreements = [Select Name, Sent_Date__c, Closed_Date__c, Signed_Date__c, URL__c, Expiry_Date__c, Quote__c, Recipient__c, Type__c, Waybill_ID__c, Status__c, CreatedDate, CreatedById From Agreement__c Where Quote__c = :quoteId Order By Name Desc];
            agreements = new List<AgreementVO>();
            if (sobjectagreements != null) {
                for (Agreement__c a : sobjectagreements) {
                    // Add the agreement record into the AgreementVO list
                    agreements.add(new AgreementVO(a));
                }
            } } catch (Exception e) { QuoteUtilities.log(e); }
    }
    
    // Updated
    private Boolean isAgreementAcceptedStatus(String status) {
        try {
        QuoteStatus.Permissions p = QuoteStatus.permissions.get(status.toLowerCase());
        return (p == null) ? false : p.isAcceptedStatus; } catch (Exception e) {QuoteUtilities.log(e);} return false;
    }
    
    public PageReference onSave() {
        System.Savepoint sp = Database.setSavepoint();
        PageReference pref;
        try {
                pref = new PageReference(returnURL);
                pref.setRedirect(true);
            try { update installLocation; } catch (exception e) {}
            
            if (quote.status == 'Unserviceable' && (quote.unserviceableItems == null || quote.unserviceableItems == '')) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'You must enter a value for unserviceable items when the quote status is set to Unserviceable.'));
                return null;
            }
            
            if (installLocation != null) {
                if (quote.record.SBQQ__Status__c.equalsIgnoreCase('provisioning complete') && (installLocation.BAN__c == '' || installLocation.CAN__c == '' || installLocation.BAN__c == null || installLocation.CAN__c == null)) {
                    
                    if (installLocation.BAN__c == '' || installLocation.BAN__c == null) { eBANreq = true; }
                    if (installLocation.CAN__c == '' || installLocation.CAN__c == null) { eCANreq = true; }
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'You must enter a value for BAN and CAN before completing provisioning.')); return null;
                }
            }
            
            if(isAgreementAcceptedStatus(quote.record.SBQQ__Status__c)) { quote.record.Agreement_Accepted__c = true; }
            
            SBQQ__Quote__c[] quotes = new List<SBQQ__Quote__c>{quote.record};
            Quote_Group__c grp = null;
            if (quote.record.Group__c != null) {
                grp = [Select Name, Status__c, Order_Complete__c, Client_In_Store__c, Kana_Queue__c From Quote_Group__c Where Id = :quote.record.Group__c];
                grp.Status__c = quote.record.SBQQ__Status__c;
                grp.Order_Complete__c = quote.record.Order_Complete__c;
                grp.Client_In_Store__c = quote.record.Client_In_Store__c;
                grp.Kana_Queue__c = quote.record.Kana_Queue__c;
                
                SBQQ__Quote__c[] otherQuotes = QuoteUtilities.loadQuotesByGroup(quote.record.Group__c);
                for (SBQQ__Quote__c other : otherQuotes) {
                    // other.SBQQ__Status__c = quote.record.SBQQ__Status__c;
                    other.Order_Complete__c = quote.record.Order_Complete__c;
                    other.Client_In_Store__c = quote.record.Client_In_Store__c;
                    other.Assigned_Account_Order__c = quote.record.Assigned_Account_Order__c;
                    
                    // TC0012345
                    other.Agreement_Accepted__c = quote.record.Agreement_Accepted__c;
                    
                    if (other.Id != quote.record.Id) {
                        quotes.add(other);
                    }
                }
            }
            
            update quotes;
            if (grp != null) { update grp; }
            
            if (quote.record.SBQQ__Status__c == QuoteStatus.SIGNED_IN_PERSON) {
                Agreement__c[] ags = [Select Status__c from Agreement__c Where Status__c = 'Pending' and Type__c = 'Wet Ink' and Quote__c = :quote.record.id];
                if (ags.size() == 1) {
                    QuoteAgreementUtilities.processAcceptance(ags.get(0).Id, true);
                } else {
                    Agreement__c a = new Agreement__c(Recipient__c = quote.record.SBQQ__PrimaryContact__c, Quote__c = quote.record.Id, Type__c = 'Wet Ink', Sent_Date__c = DateTime.now());
                    insert a;
                    QuoteAgreementUtilities.processAcceptance(a.Id, true);
                }
            }
        } catch (Exception e) {Database.rollback(sp); QuoteUtilities.log(e); return null;}
        return pref;
    }
    
    public PageReference onCancel() {
        try {
        PageReference pref = new PageReference(returnURL);
        pref.setRedirect(true);
        return pref; } catch (Exception e) {QuoteUtilities.log(e);}return null;
    }
    
    public PageReference onPrint() {
        try {
        PageReference pref = Page.PrintQuote;
        pref.getParameters().put('qids', quote.id);
        pref.getParameters().put('retURL', getSummaryPageURL());
        pref.setRedirect(true);
        return pref; } catch (Exception e) {QuoteUtilities.log(e);} return null;
    }
    
    public PageReference onEmail() {
        try {
        PageReference pref = Page.SendQuoteEmail;
        pref.getParameters().put('qids', quote.id);
        pref.getParameters().put('aid', account.Id);
        pref.getParameters().put('retURL', getSummaryPageURL());
        pref.setRedirect(true);
        return pref; } catch (Exception e) {QuoteUtilities.log(e);} return null;
    }
    
    public PageReference onSendAgreement() {
        try {
        PageReference pref = Page.SendQuoteAgreement;
        pref.getParameters().put('qids', quote.id);
        pref.getParameters().put('aid', account.Id);
        pref.getParameters().put('retURL', getSummaryPageURL());
        pref.setRedirect(true);
        
        // Traction addition for QTR5 Req 10 + 11
        // Allowing Account & Contact fields to be optional until later in the quote process
        if (!QuoteUtilities.isAccountValid(account.Id, quote.Id, getSummaryPageURL(), 'agreement')) {
            return null;
        }
        if (!QuoteUtilities.isContactValid(account.Id, quote.Id, getSummaryPageURL())) {
            return null;
        }
        if(!QuoteCustomConfigurationService.isRequiredAdditionalInformationFilled(quote.Id, QuoteStatus.SENT_AGREEMENT)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 
            'There is required data that needs to be filled before you can send the agreement. Click <a href="QuoteCompleteInformation?qid=' 
                + quote.id + '&stage=' + EncodingUtil.urlEncode(QuoteStatus.SENT_AGREEMENT, 'UTF-8') 
                + '&retUrl=' + EncodingUtil.urlEncode(pref.getUrl(), 'UTF-8') + '">here </a> to complete the data.'));
            return null;
        }
        
        return pref; } catch (Exception e) {QuoteUtilities.log(e);} return null;
    }
    
    public PageReference onLoad() {
        try {
        String link = ApexPages.currentPage().getParameters().get('link');
        if (link == '1') {
            QuoteCustomConfigurationController.linkUnassignedAdditionalInformation(quote.record.Id);
        } } catch (Exception e) {QuoteUtilities.log(e);} return null;
    }
    
    public PageReference onCheckCredit() {
        try {
        if (!quotePortalUtils.isUserDealer()) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Fatal, 'Agents must use the Credit Portal to obtain a Credit Reference Number. It can be entered on the Account details page.'));
            return null;
        }
        // Traction addition for QTR5 Req 10 + 11
        // Allowing Account & Contact fields to be optional until later in the quote process
        if (!QuoteUtilities.isAccountValid(account.Id, quote.Id, getSummaryPageURL(), 'creditcheck')) { return null; }
        if (!QuoteUtilities.isContactValid(account.Id, quote.Id, getSummaryPageURL())) { return null; }
        
        if(!QuoteCustomConfigurationService.isRequiredAdditionalInformationFilled(quote.Id, QuoteStatus.SENT_FOR_CREDIT_CHECK)){
            PageReference pr = Page.QuoteSummary;
            pr.getParameters().put('qid', quote.id);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 
            'You must complete all required fields on the Product Details page before you can continue.' +
            ' <a href="QuoteCompleteInformation?qid=' + quote.id + '&stage=' + EncodingUtil.urlEncode(QuoteStatus.SENT_FOR_CREDIT_CHECK, 'UTF-8') 
                + '&retUrl=' + EncodingUtil.urlEncode(pr.getUrl(), 'UTF-8') 
                + '">Click here</a> to complete required fields.'));
            return null;
        }
        
        quote.record.Credit_Check_Requested__c = true;
        update quote.record;
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Credit check request sent.'));
        
        } catch (Exception e) {QuoteUtilities.log(e); return null;} return ApexPages.currentPage();
    }
    
    public String getQuoteRefresh() {
        try {
        String id = ApexPages.currentPage().getParameters().get('qid');
        if ((quote != null) && (quote.record.Id != id)) {
            init(id);
        }
        return ''; } catch (Exception e) {QuoteUtilities.log(e);} return null;
    }
    
    
        public Boolean getCanEdit() {
        try {
        QuoteStatus.Permissions p = QuoteStatus.permissions.get(quote.status.toLowerCase());
        if (p == null) { return false; }
        if (quote.agreementAccepted) {
            if (QuotePortalUtils.isExperienceExcellenceUser()) {
                return p.isQuoteEditableAfterSigningForEE;
            } else if (QuotePortalUtils.isUserDealer()) {
                return p.isQuoteEditableAfterSigningForDealer;
            }
            return p.isQuoteEditableAfterSigning;
        } else {
            if (QuotePortalUtils.isExperienceExcellenceUser()) {
                return p.isQuoteEditableForEE;
            }
            if (QuotePortalUtils.isUserDealer()) {
                return p.isQuoteEditableForDealer;
            }
            return p.isQuoteEditable;
        }
        return false; } catch (Exception e) {QuoteUtilities.log(e);} return null;
    }
    
    // Updated
    public Boolean getCanEditProducts() {
        try {
        QuoteStatus.Permissions p = QuoteStatus.permissions.get(quote.status.toLowerCase());
        if (p == null) { return false; }
        if (quote.agreementAccepted) {
            if (QuotePortalUtils.isExperienceExcellenceUser()) {
                return p.areProductsEditableAfterSigningForEE;
            }
            /* This criteria doesn't exist yet!
            if (QuotePortalUtils.isUserDealer()) {
                return p.areProductsEditableAfterSigningForDealer;
            }
            */
            return p.areProductsEditableAfterSigning;
        } else {
            if (QuotePortalUtils.isExperienceExcellenceUser()) {
                return p.areProductsEditableForEE;
            }
            /* This criteria doesn't exist yet!
            if (QuotePortalUtils.isUserDealer()) {
                return p.areProductsEditableForDealer;
            }
            */
            return p.areProductsEditable;
        }
        return false; } catch (Exception e) {QuoteUtilities.log(e);} return null;
    }
    
    public Boolean getCanClone() {
        // Temp override
        return quote.isCloneable && getUserCanCloneQuotes();
    }
    public Boolean getUserCanCloneQuotes() {
        return QuoteCloneController.channelCanClone(QuotePortalUtils.getChannel());
    }
    
    public PageReference onClone() {
        if (!getCanClone()) {
            QuoteUtilities.addMessageToPage('You are not permitted to clone this quote.');
            return null;
        }
        // Move them to the new quote
        PageReference pr = Page.QuoteClone;
        pr.getParameters().put('qid', quote.id);
        pr.setRedirect(true);
        return pr;
    }
    
    public Boolean getCanRequestClone() {
        // Temp override
        return quote.isCloneable && getUserCanIssueCloneRequests();
    }
    public Boolean getUserCanIssueCloneRequests() {
        return QuoteCloneController.channelCanRequestClone(QuotePortalUtils.getChannel());
    }
    
    public PageReference onRequestClone() {
        if (!getCanRequestClone()) {
            QuoteUtilities.addMessageToPage('You are not permitted to request cloning.');
            return null;
        }
        // Move them to the new quote
        PageReference pr = Page.QuoteCloneRequest;
        pr.getParameters().put('qid', quote.id);
        pr.setRedirect(true);
        return pr;
    }
}