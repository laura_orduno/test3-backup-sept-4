/**
 * @description Domain class for CaseComment
 * @author      Dane Peterson, Traction on Demand
 * @date        2017-12-07
 */
public with sharing class trac_CaseComment extends trac_TriggerHandlerBase {
    public override void handleBeforeInsert() {
        trac_CaseCommentDispatcher dis = new trac_CaseCommentDispatcher();
        dis.init();
        dis.execute();
        dis.finish();

    }

    public override void handleBeforeUpdate() {
        trac_CaseCommentDispatcher dis = new trac_CaseCommentDispatcher();
        dis.init();
        dis.execute();
        dis.finish();
    }

    public override void handleAfterInsert() {
    	trac_UpdateCaseFromCaseComment.execute((List<CaseComment>) newRecordsList);
        trac_CaseCommentDispatcher dis = new trac_CaseCommentDispatcher();
        dis.init();
        dis.execute();
        dis.finish();
    }


    public override void handleAfterUpdate() {
        trac_CaseCommentDispatcher dis = new trac_CaseCommentDispatcher();
        dis.init();
        dis.execute();
        dis.finish();

    }

}