/**
 * An apex page controller that exposes the change password functionality
 */
public class TelusPRMChangePasswordController {
    public String oldPassword {get; set;}
    public String newPassword {get; set;}
    public String verifyNewPassword {get; set;}        


   public PageReference changePassword() {
    Pagereference pageRef = Site.changePassword(newPassword, verifyNewPassword, oldpassword);
    if(pageRef == null){
        return pageRef;
    }
    String prefix = Site.getPrefix();        
    String retURL = System.currentPageReference().getParameters().get('retURL');
    System.debug('retURL = ' + retURL);        
    if(prefix != null){            
        retURL = (retURL == null) ? prefix + '/home/home.jsp' : retURL;
    } else {
        retURL = (retURL == null) ? '/home/home.jsp' : retURL;
    }
 
    PageReference pr = new PageReference(retURL);
    pr.setRedirect(true);
    return pr;
	}
     
}