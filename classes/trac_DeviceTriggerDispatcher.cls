public with sharing class trac_DeviceTriggerDispatcher extends Dispatcher{

    public List<Device__c> newList {get; set;}
    public List<Device__c> oldList {get; set;}
    public Map<Id, Device__c> newMap {get; set;}
    public Map<Id, Device__c> oldMap {get; set;}
    public static boolean firstRun = true;


    public trac_DeviceTriggerDispatcher() {
        super();
    }

    public override void init() {

        newList = (trigger.new != null) ? (List<Device__c>)trigger.new : new List<Device__c>();
        oldList = (trigger.old != null) ? (List<Device__c>)trigger.old : new List<Device__c>();
        newMap = (trigger.newMap != null) ? (Map<Id, Device__c>)trigger.newMap : new Map<Id, Device__c>();
        oldMap = (trigger.oldMap != null) ? (Map<Id, Device__c>)trigger.oldMap : new Map<Id, Device__c>();

    }

    public override void execute(){
    	//functions to be used. 
    	if( limitExecutionEvent(TriggerEvent.BEFOREINSERT)  || limitExecutionEvent(TriggerEvent.BEFOREUPDATE) ){
    		getParentIdAtSource();
            parentDeviceToCase();
            
    	}	
        if( limitExecutionEvent(TriggerEvent.AFTERINSERT)  || limitExecutionEvent(TriggerEvent.AFTERUPDATE) ){
            sendDevice();
        }   
    }

    public void parentDeviceToCase(){
        parentDeviceToCase(newList);
    }

    public void sendDevice(){
        sendDevice(newList);
    }

    public void getParentIdAtSource(){
        getParentIdAtSource(newList);
    }


    public static void getParentIdAtSource(List<Device__c> localDevices){
        Map<Id,Id> localCaseToVendorCase = new Map<Id,Id>();
        localCaseToVendorCase = getVendorCase(localDevices);

         for(Device__c dev: localDevices){
            if(localCaseToVendorCase.containsKey(dev.Voip_Case__c)){
                dev.Parent_Id_At_Source__c = localCaseToVendorCase.get(dev.Voip_Case__c);
            }
        }
    }

    public static void sendDevice(List<Device__c> newDevices){

     	Set<Id> caseIds = new Set<Id>();
        List<PartnerNetworkRecordConnection> pnrcList = new List<PartnerNetworkRecordConnection>();

    	List<Device__c> localDevices = new List<Device__c>();        	
        //List<Case> relatedCases = new List<Case>();
        List<Device__c> devicesWithVendor = new List<Device__c>();
        Map<Id, Device__c> caseToDevice = new Map<Id, Device__c>();

        for(Device__c dev :newDevices){    
            caseIds.add(dev.Voip_Case__c);
            caseToDevice.put(dev.Voip_Case__c, dev);            
        }

        //relatedCases = [SELECT Id, Vendor_Case_ID__c
        //                FROM Case
        //                WHERE Id in:caseIds
        //                ];

        if(caseIds.size() > 0){
            pnrcList =[ Select id,status,LocalRecordId,StartDate,PartnerRecordId
                        From PartnerNetworkRecordConnection
                        Where LocalRecordId in :caseIds
                        AND Status <> 'Inactive'
                        ];
        }

        for(PartnerNetworkRecordConnection pnrc:pnrcList){
            if(caseToDevice.containsKey(pnrc.LocalRecordId)){
                Device__c tmpDev = caseToDevice.get(pnrc.LocalRecordId);                
                devicesWithVendor.add(tmpDev);
            }
        }

    	for(Device__c dev :devicesWithVendor){        	
    		if (dev.ConnectionReceivedId == null ){    			
    			localDevices.add(dev);

    		}
    	}

    	if(localDevices.size() > 0 ){
    		trac_S2S_connection_Helper.sendToRingCentral(localDevices, null, null, 'RingCentral',false,false,false);
    	}
    }

    /**
    *@author: David Barrera 
    *@description: Query the Partner Netowrk Record Connection object to get the Id of the case that is on
    *              the vendor side.
    **/
    public static Map<Id,Id> getVendorCase(List<Device__c> devices){
        
        List<Id> caseIds = new List<Id>();
        List<PartnerNetworkRecordConnection> pnrcList = new List<PartnerNetworkRecordConnection>();

        
        Map<Id, Id> localCaseToPnrc = new Map<Id,Id>();

        for(Device__c dev: devices){
            if( dev.Voip_Case__c != null && dev.ConnectionReceivedId == null){
                caseIds.add(dev.Voip_Case__c);
            }
        }
        if(caseIds.size() > 0){
            pnrcList =[ Select id,status,LocalRecordId,StartDate,PartnerRecordId
                        From PartnerNetworkRecordConnection
                        Where LocalRecordId in :caseIds
                        ];
        }


        for(PartnerNetworkRecordConnection pnrc:pnrcList){
            if(pnrc.PartnerRecordId != null && pnrc.LocalRecordId != null){
                localCaseToPnrc.put(pnrc.LocalRecordId, pnrc.PartnerRecordId);
            }
        }
        return localCaseToPnrc;

    }
    /**
    *@author: David Barrera 
    *@description: When a device object comes in from the RingCentral Org parent the device to the case associated with the ID in parent id at source.
    **/
    public static void parentDeviceToCase(List<Device__c> devices){

        List<Device__c> incomingDevices = new List<Device__c>();        
        Map<Id,Case> parentCases = new Map<Id,Case>();
        Map<Id,String> deviceToParentCaseId = new Map<Id,String>();

        for(Device__c dev: devices){
            if( (dev.ConnectionReceivedId != null || Test.isRunningTest() ) && String.isNotBlank(dev.Local_Parent_Id__c)){
                //parentIds.add(dev.Parent_Id_At_Source__c);
                incomingDevices.add(dev);
                deviceToParentCaseId.put(dev.Id,dev.Local_Parent_Id__c);
                System.Debug('This is the parent id at source' + dev.Local_Parent_Id__c);
            }
        }

        if(deviceToParentCaseId.size() > 0){
            parentCases = new Map<Id,Case> ([SELECT Id
                                                FROM Case
                                                WHERE Id IN :deviceToParentCaseId.values()
                                                ]);
        }
        for(Device__c dev: incomingDevices){
            if(deviceToParentCaseId.containsKey(dev.Id)){        
                Case tmpCase = new Case();
                tmpCase = parentCases.get(deviceToParentCaseId.get(dev.Id));
                System.Debug('This is the tmp Case: ' + tmpCase);
                dev.Voip_Case__c = tmpCase.Id;
            }
        }   

    }   
}