public with sharing class ContractEnvelopeTriggerHandler {
    map<id,schema.recordtypeinfo> contractRecordTypeMap;
    public ContractEnvelopeTriggerHandler() {
        if(trigger.isexecuting){
            contractRecordTypeMap=contract.sobjecttype.getdescribe().getrecordtypeinfosbyid();
            system.debug('>>>contractRecordTypeMap___' + contractRecordTypeMap);
            //Modified by Jaya for recursion check
            //if(trigger.isbefore){
              if(trigger.isbefore){
                beforeInit();
                //Modified by Jaya for recursion check
                //if(trigger.isinsert){
                if(trigger.isinsert && CheckRecursion.runOnceBeforeInsert()){
                     beforeInsert();
                }
                //Modified by Jaya for recursion check
                //else if(trigger.isupdate){
                else if(trigger.isupdate && CheckRecursion.runOnceBeforeUpdate()){
                    beforeUpdate();
                }
            }else if(trigger.isafter){
                afterInit();
                //Modified by Jaya for recursion check
                //if(trigger.isinsert){
                if(trigger.isinsert && CheckRecursion.runOnceAfterInsert()){
                    afterInsert();
                }
                //Modified by Jaya for recursion check
                //else if(trigger.isupdate){
                else if(trigger.isupdate){
                    afterUpdate();
                }
            }
        }
        
    }

     void beforeInit(){}
    /**
     * Shared initializations in all AFTER events.
     */
    void afterInit(){}
    void beforeUpdate(){
     
    }
    void beforeInsert(){
     
    }
    void afterInsert(){
        List<Contract > contractoUpdateList = new List<Contract>();
        for(sobject record:trigger.new){
            System.debug('>>>> after Insert' + record);
            vlocity_cmt__ContractEnvelope__c newContractEnvp =(vlocity_cmt__ContractEnvelope__c)record;
            contract con = [Select ID, recordtypeid from contract where Id =: newContractEnvp.vlocity_cmt__ContractId__c LIMIT 1];
            if(con != null ){
                 if(contractRecordTypeMap.get(con.recordtypeid).getname().equalsignorecase('Contract')){
               
                //Contract conObj = new Contract(Id = newContractEnvp.vlocity_cmt__ContractId__c, Status = 'In Progress') ;
                con.Status = 'In Progress';
                contractoUpdateList.add(con);
           }
            }
           

           
        }
        if(contractoUpdateList != null && contractoUpdateList.size() > 0 && !contractoUpdateList.isEmpty())
            update contractoUpdateList;
    }
    
    void afterUpdate()
    {
       List<Contract> contractRevokeToUpdateList = new List<Contract>();
       
       for(sobject record1:trigger.new)
       {
           System.debug('>>>> after update' + record1);
           vlocity_cmt__ContractEnvelope__c newContractEnvp = (vlocity_cmt__ContractEnvelope__c)record1;
           if(newContractEnvp.vlocity_cmt__Status__c == 'Voided')
           {                         
               Contract conObj = new Contract(Id = newContractEnvp.vlocity_cmt__ContractId__c, ContractRevoked__c = true) ;
               contractRevokeToUpdateList.add(conObj);
           }           
       }
       if(contractRevokeToUpdateList != null && contractRevokeToUpdateList.size() > 0 && !contractRevokeToUpdateList.isEmpty())
           update contractRevokeToUpdateList;
    }

}