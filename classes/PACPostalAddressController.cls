/**
* Description of the purpose of the class:
This class provides methods to save postal address and invoke the integration points to load the postal address extended attributes. 
Also class also checks if a postal  address is already registered for that customer account. 
If it exists, it will update the postal address details with the new values. Otherwise, it will create a new postal address.

* Author: Sandip Chaudhari 
* Date: 27-oct-2015
* Project/Release: Predictive Address Capture
* Change Control Information:
* Date Who Reason for the change
* ### Sandip - 26 Jan 2016 - PAC R2 Changes
**/
global class PACPostalAddressController{

public String idx {get;set;}
public AddressData cData {get;set;}
public String PACconfigName {get;set;}
public static String keyConfig {get;set;}
public Boolean useAsAttri {get;set;}
//public String selectedIso {get;set;}
public String selectedLanguages {get;set;}
public PACConfig__c pacConfigDetails {get;set;}
public PACConfig__c pacSAConfigDetails {get;set;}
public string Init{set;}
public string config{set;}
public Boolean isNewAddressCapture{get;set;}

Public string order_id{get;set;} // added by Prashant on 22-May-2017 for Billing language
Public string billingDtls{get;set;} // 26 July 2017 - added by Sandip to on-off billing section
Public order orderobj{get;set;}
Public string ordercontactlanguage{get;set;} // added by Prashant on 22-May-2017 for Billing language
public List<selectOption> billingSystemList {get;set;} // added by Prashant on 19-May-2017 for Billing System support
public List<selectOption> billingLangList;// added  by Barkha on 13/06/2017 for Billing Language support
    
//Start: Added by Aditya Jamwal(IBM) to capture domain, sub-domain and responsible group values for TQ Billing A/C.   
public List<selectOption> domainPickList  {get; set;}
public List<selectOption> subDomainPickList  {set;}
public List<selectOption> responsibleGroupPickList {get; set;}
public Map<String, List<selectOption>> domainTosubDomainPicklistValuesMap { get; set; }

//Added by Jaya to check if the billing address should be retained
public Boolean retainBillingAddress{get;set;}
 
public List<selectOption> getsubDomainPickList(){
    List<selectOption> subDomainPickList1 = new List<selectOption>();
    subDomainPickList1.add(new SelectOption('', Label.BillingSystem_none));
    system.debug('Inside getsubDomainPickList '+cData.domainCode+' '+ domainTosubDomainPicklistValuesMap );
        if(String.isNotBlank(cData.domainCode) && null != domainTosubDomainPicklistValuesMap && domainTosubDomainPicklistValuesMap.size() > 0){
            subDomainPickList1 = new List<selectOption>();
            subDomainPickList1 = domainTosubDomainPicklistValuesMap.get(cData.domainCode);
        }
     system.debug('Inside getsubDomainPickList '+subDomainPickList1 );
    return subDomainPickList1;
}
    
public void getsubDomainValues(){ getsubDomainPickList();
/*    subDomainPickList.add(new SelectOption('', Label.BillingSystem_none));
    system.debug('Inside getsubDomainPickList '+cData.domainCode+' '+ domainTosubDomainPicklistValuesMap );
        if(String.isNotBlank(cData.domainCode) && null != domainTosubDomainPicklistValuesMap && domainTosubDomainPicklistValuesMap.size() > 0){
            subDomainPickList = new List<selectOption>();
            subDomainPickList = domainTosubDomainPicklistValuesMap.get(cData.domainCode);
        }
     system.debug('Inside getsubDomainPickList '+subDomainPickList );
  */ }   
    
public void populateTQPicklistValues(){
    domainTosubDomainPicklistValuesMap = new Map<String, List<selectOption>>();
    
    domainPickList.add(new SelectOption('', Label.BillingSystem_none));
    
    responsibleGroupPickList.add(new SelectOption('', Label.BillingSystem_none));
    
    List<BAC_TQ_Picklist_Values__c> TQPicklistValuesList = [Select id, name,Related_Domain__c,Code__c,isDomain__c,isResponsilbeGroup__c,isSubDomain__c,Value__c from BAC_TQ_Picklist_Values__c ];
    if(NULL != TQPicklistValuesList && TQPicklistValuesList.size()>0){
        for(BAC_TQ_Picklist_Values__c value : TQPicklistValuesList){
        if(value.isDomain__c){
         domainPickList.add(new SelectOption(value.Code__c,value.Value__c));   
        }
        if(value.isSubDomain__c){
        // subDomainPickList.add(new SelectOption(value.Code__c,value.Value__c)); 
            if(String.isNotBlank(value.Related_Domain__c)) {
                List<selectOption> subDomainList = new List<selectOption>();
                if(null !=domainTosubDomainPicklistValuesMap && null != domainTosubDomainPicklistValuesMap.get(value.Related_Domain__c)){
                   subDomainList = domainTosubDomainPicklistValuesMap.get(value.Related_Domain__c);
                }
                subDomainList.add(new SelectOption(value.Code__c,value.Value__c));
                domainTosubDomainPicklistValuesMap.put(value.Related_Domain__c,subDomainList); 
            }
        }
        if(value.isResponsilbeGroup__c){
         responsibleGroupPickList.add(new SelectOption(value.Code__c,value.Value__c));   
        }
      }
    }
}
//End: Added by Aditya Jamwal(IBM) to capture domain, sub-domain and responsible group values for TQ Billing A/C.   
 
//public String RCIDAccountId; 
 public List<selectOption> getbillingLangList(){ return PopuateBillingLanguageOptions(); }
 public void setbillingLangList(String rid){
        //RCIDAccountId = rid;
        
        //PopuateBillingLanguageOptions();
    }

public List<selectOption> billingFormatList {get;set;}// Added  by Barkha on 13/06/2017 for Billing Format support

//public String PACSAconfigName{get;set;}
public String pacJSconfigName {get;set;}
public Boolean isUsedInVfInd {get;set;}
public String captureNewLabel{get;set;}
public List<selectOption> countryList {get;set;}
public String onSelectElementId{get;set;} // Change event get fire after selecting address from drop down
public String onCheckElementId{get;set;} // Change event get fire after check 'Capture New Address' checkbox
public static  String apiKeyErrorMessage{get;set;}
public List<selectOption> usStateList {get;set;}
public List<selectOption> canadaProvinceList {get;set;}

// PAC R2 Changes
public String inValidStatus{get;set;}
public String validStatus{get;set;}
public PACUtility pacUtil = new PACUtility();    

    /* Constructor
    */
    public PACPostalAddressController()  
      {     
       system.debug('SUKHDEEP @@@In Postal Service cData ====> '+cData);
        pacConfigDetails = new PACConfig__c();
        isNewAddressCapture = false;
        countryList = new List<SelectOption>();
        usStateList = new List<SelectOption>();
        canadaProvinceList = new List<SelectOption>();
        pacUtil.getCountryStateList();
        countryList = pacUtil.countryList;
        usStateList = pacUtil.usStateList;
        canadaProvinceList = pacUtil.canadaProvinceList;
        //getCountries();
        //getCanadaProvince();
        //getUSstates();
        system.debug('PACPostalAddressController>>>orderid>>>>'+order_id);
        
        billingSystemList = new List<selectOption>();// added by Prashant on 19-May-2017 for Billing System support
        billingLangList  =  new List<selectOption>();// added  by Barkha on 13/06/2017 for Billing Language support
        billingFormatList  =  new List<selectOption>();//Added  by Barkha on 13/06/2017 for Billing Format support
        selectedLanguages = UserInfo.getLanguage();
        if(selectedLanguages != null && selectedLanguages.length() > 2){
           selectedLanguages = selectedLanguages.substring(0,2); 
        }
        PopuateBillingSystemOptions();// added by prashant Tiwari
        //PopuateBillingLanguageOptions();// added  by Barkha on 13/06/2017 for Billing Language support
        PopuateBillingFormatOptions();//Added  by Barkha on 13/06/2017 for Billing Format support
          
        //Start: Added by Aditya Jamwal(IBM) to capture domain, sub-domain and responsible group values for TQ Billing A/C.     
        domainPickList = new List<selectOption>();
      //  subDomainPickList = new List<selectOption>();
        responsibleGroupPickList = new List<selectOption>();
        populateTQPicklistValues();
        //End: Added by Aditya Jamwal(IBM) to capture domain, sub-domain and responsible group values for TQ Billing A/C.   
      }
       /**Method: PopuateBillingLanguageOptions()
      Added  by Barkha on 13/06/2017 for Billing Format support
        Input args: None
        Output args: None
      **/
     Public void PopuateBillingFormatOptions(){
      billingFormatList.clear();
      billingFormatList.add(new SelectOption('', Label.BillingSystem_none));
      billingFormatList.add(new SelectOption('Paper','Paper'));
      billingFormatList.add(new SelectOption('PDF','PDF'));
     }
    
    /**Method: PopuateBillingLanguageOptions()
      Added  by Barkha on 13/06/2017 for Billing Language support
        Input args: None
        Output args: None
      **/
     Public List<selectOption> PopuateBillingLanguageOptions(){
      billingLangList.clear();
      if(order_id != null){
            orderobj = [select id, chosen_language__c from order where id =: order_id];
        }
     // billingLangList.add(new SelectOption('', Label.BillingSystem_none));
         if(orderobj != null && orderobj.chosen_language__c != null){
             if(orderobj.chosen_language__c.equalsIgnoreCase('en_US') || String.isBlank(orderobj.chosen_language__c)){
                 billingLangList.add(new SelectOption('English','EN'));
                 billingLangList.add(new SelectOption('French','FR'));
             }
             else{
                 billingLangList.add(new SelectOption('French','FR'));
                 billingLangList.add(new SelectOption('English','EN'));
             }
         }
         else{
              billingLangList.add(new SelectOption('English','EN'));
              billingLangList.add(new SelectOption('French','FR'));
          }
          return billingLangList;
     }
      
      /**Method: PopuateBillingSystemOptions()
       Added by Prashant on 19-May-2017 for billing system support
        Input args: None
        Output args: None
      **/
      
      Public void PopuateBillingSystemOptions(){
          Map<string,LEGACY_ACCOUNT_ID__c> mapBillingSystemCodes = new Map<string,LEGACY_ACCOUNT_ID__c>();//LEGACY_ACCOUNT_ID__c.getAll();
          for(LEGACY_ACCOUNT_ID__c la : LEGACY_ACCOUNT_ID__c.getall().values()){
              if(la.isBillingSystem__c){
                   mapBillingSystemCodes.put(la.name,la);   
              }
          }
          billingSystemList.add(new SelectOption('', Label.BillingSystem_none));
          for(LEGACY_ACCOUNT_ID__c la : mapBillingSystemCodes.values()){
              billingSystemList.add(new SelectOption(la.name, la.Billing_System_Name__c));
          }
          
      }
      /*Public void PopuateBillingSystemOptions1(){
        billingSystemList.clear();
        if(cData != null && cData.canadaProvince.equalsIgnoreCase(Label.BillingSystem_AB) ){
            billingSystemList.add(new SelectOption(Label.BillingSystem_CoreAB,Label.BillingSystem_Core_AB));
            billingSystemList.add(new SelectOption(Label.BillingSystem_CoreBC, Label.BillingSystem_Core_BC));
            billingSystemList.add(new SelectOption(Label.BillingSystem_TQ_CBSS, Label.BillingSystem_CBSS));

        }
        
        else if(cData != null && cData.canadaProvince.equalsIgnoreCase(Label.BillingSystem_BC) ){
            billingSystemList.add(new SelectOption(Label.BillingSystem_CoreBC, Label.BillingSystem_Core_BC));
            billingSystemList.add(new SelectOption(Label.BillingSystem_CoreAB, Label.BillingSystem_Core_AB));            
            billingSystemList.add(new SelectOption(Label.BillingSystem_TQ_CBSS, Label.BillingSystem_CBSS));

        }
        else if(cData != null && cData.canadaProvince.equalsIgnoreCase(Label.BillingSystem_QC) ){
            billingSystemList.add(new SelectOption(Label.BillingSystem_TQ_CBSS, Label.BillingSystem_CBSS));
            billingSystemList.add(new SelectOption(Label.BillingSystem_CoreAB, Label.BillingSystem_Core_AB));            
            billingSystemList.add(new SelectOption(Label.BillingSystem_CoreBC, Label.BillingSystem_Core_BC));
            
            

        }
        else{
            billingSystemList.add(new SelectOption('', Label.BillingSystem_none));
            billingSystemList.add(new SelectOption(Label.BillingSystem_TQ_CBSS, Label.BillingSystem_CBSS));
            billingSystemList.add(new SelectOption(Label.BillingSystem_CoreAB, Label.BillingSystem_Core_AB));            
            billingSystemList.add(new SelectOption(Label.BillingSystem_CoreBC, Label.BillingSystem_Core_BC));
            
            

        }
        
               billingSystemList.add(new SelectOption('', Label.BillingSystem_none));
            billingSystemList.add(new SelectOption(Label.BillingSystem_TQ_CBSS, Label.BillingSystem_CBSS));
            billingSystemList.add(new SelectOption(Label.BillingSystem_CoreAB, Label.BillingSystem_Core_AB));            
            billingSystemList.add(new SelectOption(Label.BillingSystem_CoreBC, Label.BillingSystem_Core_BC));
             
        
        
    } /*/
    /**Method: getConfig()
     getConfig - Used to get configuration details from custom settings
     Parameter - None
     Return Type - String 
    **/
    public String getConfig(){
         // PAC R2 Changes
        inValidStatus = System.Label.PACInValidCaptureNewAddrStatus;
        validStatus = System.Label.PACValidCaptureNewAddrStatus;
        // PAC R2 Changes
       // system.debug('@@@In Postal Service cData ====> '+cData);
        keyConfig = pacConfigName;
        pacConfigDetails = PACConfig__c.getInstance(pacConfigName);
        
        List<ExternalPACJSName__c> allData = ExternalPACJSName__c.getAll().values();
        if(cData!=null && cData.country == null){
            cData.country = 'CAN'; //Label.CAN_Label;
        }
        if(captureNewLabel == null){
            captureNewLabel = System.Label.PACCaptureNewAddress;
        }
        if(allData != null){
            for (ExternalPACJSName__c obj: allData){
                if (obj.Is_Active__c){
                    pacJSconfigName = obj.Name;
                    break;
                }
            }
       }
        return pacConfigName;
    }
    
     /* 
    * getCountries - Used to get list of countries and invoked from constructor of this class
    * Parameter - 
    * Return Type - void
    */
 /*   public void getCountries(){
        system.debug('@@@get Countries');
        //countryList.add(new SelectOption('', 'Not Specified'));     
        for (Country_Province_Codes__c country : [SELECT Name, Description__c 
                                                  FROM Country_Province_Codes__c
                                                  WHERE Type__c = 'Country'
                                                  ORDER BY Description__c]) {
        countryList.add(new SelectOption(country.Name, country.Description__c));    
        }
    }
 */ 
   /* 
    * getUSStates - Used to get list of States of US and invoked from constructor of this class
    * Parameter - 
    * Return Type - void
    */
 /*   public void getUSStates(){
        system.debug('@@@get US state');
        usStateList.add(new SelectOption('', 'Select'));
        for (Country_Province_Codes__c country : [SELECT Name, Description__c 
                                                  FROM Country_Province_Codes__c
                                                  WHERE Type__c = 'American State'
                                                  ORDER BY Description__c]) {
        usStateList.add(new SelectOption(country.Name, country.Description__c));    
        }
        usStateList.add(new SelectOption(' ', ' '));
    }
 */   
    /* 
    * getCanadaProvince - Used to get list of States of Canada and invoked from constructor of this class
    * Parameter - 
    * Return Type - void
    */
/*    public void getCanadaProvince(){
        system.debug('@@@get Canada prov');
        canadaProvinceList.add(new SelectOption('', 'Select'));
        for (Country_Province_Codes__c country : [SELECT Name, Description__c 
                                                  FROM Country_Province_Codes__c
                                                  WHERE Type__c = 'Canadian Province'
                                                  ORDER BY Description__c]) {
        canadaProvinceList.add(new SelectOption(country.Name, country.Description__c));    
        }
        canadaProvinceList.add(new SelectOption(' ', ' '));
    }
 */    
  /**Method : syncViewState()
     handShake - This method is invoked using apex:function in VF component. 
                 This is required because the outer context is expected to get validated address from this controller instance.
     Parameter - None
    Return Type - void
    **/
  
  public void syncViewState(){
      // Invoked from the VF component, keep this as blank or write logic as needed to execute after selecting address.
  }
  
  /* 
    * getPCKey - This method is invoked remotely on load event of component. 
    *            Used get API key for Canada Post
    * Parameter - 
    * Return Type - String
    */
  @RemoteAction
  global static String getPCKey(String keyConfig1){
        PACConfig__c configdtls = PACConfig__c.getInstance(keyConfig1);
        if(configdtls != null && configdtls.API_Key__c != null){
            return configdtls.API_Key__c;
        }else{
            apiKeyErrorMessage = System.Label.PACAPIKeyErrorMessage;
            return null;
        }
  }
    
}