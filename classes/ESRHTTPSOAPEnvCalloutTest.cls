@isTest
private class ESRHTTPSOAPEnvCalloutTest {
    @isTest static void testCallout() {
       // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new ESRCalloutMock());
        
        // Call method to test.
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock. 
        test.StartTest();
        string reqXML = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:elec="http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/ElectronicServiceRequestServiceRequestResponse_v1">'+
            '<soapenv:Header/>'+
            '<soapenv:Body>'+
            '<elec:updateRequestStatusObject>'+
            '<elec:serviceRequestId></elec:serviceRequestId>'+
            '<elec:requestStatusCd></elec:requestStatusCd>'+
            '</elec:updateRequestStatusObject>'+
            '</soapenv:Body>'+
            '</soapenv:Envelope>';
         string respXML ;
        String expectedValue;
        try{
             respXML = new ESRHTTPSOAPEnvCallout().HttpCallout(reqXML,'http://foo','POST','Create',
                                                                     'scott','tiger');
            system.debug('*****@'+respXML);
             expectedValue = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body><createServiceResponse xmlns="http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/ElectronicServiceRequestServiceRequestResponse_v1"><createServiceResult xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><contextData i:nil="true"/><dateTimeStamp>0001-01-01T00:00:00</dateTimeStamp><errorCode>0</errorCode><messageType>Inserted Success</messageType><transactionId i:nil="true"/><serviceResponseId>0</serviceResponseId></createServiceResult></createServiceResponse></s:Body></s:Envelope>';
            
        }Catch(Exception ex){}
        
        try{
           
            string respXMLerr = new ESRHTTPSOAPEnvCallout().HttpCallout(reqXML,'http://fooerror','POST','Create',
                                                                        'scott','tiger');
        }Catch(Exception ex){}
        try{
            string respXMLNullErr = new ESRHTTPSOAPEnvCallout().HttpCallout(NULL,NULL,'POST','Create',
                                                                            'scott','tiger');
        }Catch(Exception ex){}
        try{
            string res = new ESRHTTPSOAPEnvCallout().HttpCallout(reqXML,NULL,'POST','Create',
                                                                 'scott','tiger');
        }Catch(Exception ex){}
        System.assertEquals(respXML, expectedValue);
        test.StopTest();
    }
    
    
    public class ESRCalloutMock implements HttpCalloutMock{
        
        public HTTPResponse respond(HttpRequest req)
        {
            try{
                if(req.getEndpoint() == 'http://fooerror'){
                    throw new CalloutException('Error');}
            HttpResponse resp = new HttpResponse();
            String strXML = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body><createServiceResponse xmlns="http://xmlschema.tmi.telus.com/srv/CMO/OrderMgmt/ElectronicServiceRequestServiceRequestResponse_v1"><createServiceResult xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><contextData i:nil="true"/><dateTimeStamp>0001-01-01T00:00:00</dateTimeStamp><errorCode>0</errorCode><messageType>Inserted Success</messageType><transactionId i:nil="true"/><serviceResponseId>0</serviceResponseId></createServiceResult></createServiceResponse></s:Body></s:Envelope>';
            resp.setHeader('Content-Type', 'Text/xml');
            resp.setBody(strXML);
            resp.setStatusCode(200);
            return resp;
            }
            catch(Exception e){
            String respXML = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Header/><s:Body><s:Fault><faultcode xmlns:a="http://schemas.microsoft.com/ws/2005/05/addressing/none">a:ActionNotSupported</faultcode><faultstring xml:lang="en-US">The message</faultstring></s:Fault></s:Body></s:Envelope>';
            throw new CalloutException(respXML );
            }
        }
    
    }
}