@RestResource(urlMapping='/RestApi_MyAccount/*')

// Reference:
// 		to get oAuth token:
// 			url: https://telus--scrum2.cs14.my.salesforce.com/services/oauth2/token
// 			content-type: application/x-www-form-urlencoded
// 			raw body:	
// 			
//              grant_type=password&client_id=3MVG9TYG6AjyhS_KByN0gz8UA2Gses7_UWgg_R1DBO0iXgWCNelQANO82m_w4hxy8WBxRzfilkbOmwl4zsOld&client_secret=6398298211905108544&username=a@b.com.scrum2&password=******

// 		to make call to CaseRestApi:
// 			url: https://telus--scrum2.cs14.my.salesforce.com/services/apexrest/RestApi_MyAccount
// 			content-type: application/x-www-form-urlencoded
// 			AUthorization Header: Bearer *** access token recieved from above oAuth token request ***

global class RestApi_MyAccount{        
    // specify the method that will be called for a GET request
	@HttpPost
 	global static RestApi_MyAccountHelper.Response postRequest(){
        return RestApi_MyAccountHelper.processPostRequest(RestContext.request.requestBody.toString());
 	}
}