@isTest
private class UpdateAllocationObjectsTest {
	public testMethod static void unit() {
      ADRA__Allocation__c allocation = new ADRA__Allocation__c();
      insert allocation;
      
      Test.startTest();
      allocation.ADRA__Expired__c = true;
      update allocation;
      Test.stopTest();
      System.assertEquals([Select Expiration_Trigger__c from  ADRA__Allocation__c where Id = :allocation.Id].Expiration_Trigger__c, true);
      
  }
}