/*
###########################################################################
# File..................: smb_SVOC_TTDetail
# Version...............: 36
# Created by............: Sandip Chaudhari
# Created Date..........: 08 March 2016

# Description...........: This class has methods show details of specific trouble tickets
#
###########################################################################
# Last Modified Details......: 
*/
public class smb_SVOC_TTDetailController{
    
    public Account acct {get; set;}
    public List<Case> matchedCases {get; set;}
    public String tID {get;set;}    
    public String RCID {get;set;}
    public String lavaStormConstant{get;set;}
    public Boolean isLavaStorm{get;set;}
    public TicketVO.TicketData  ttDetailObj{get;set;}
    // public list<TicketVO.TicketData> lstTickets {get;set;}
    TicketVO.ServiceRequestAndResponseParameter reqResObj = new TicketVO.ServiceRequestAndResponseParameter();
    public smb_SVOC_TTDetailController(){
        ttDetailObj = new TicketVO.TicketData();
        lavaStormConstant = TicketVO.SRC_LavaStorm;
    }
    
    public String ticketID;
    public String sourceSystem{get;set;}
    
    public transient List<Id> AllAccountIdsInHierarchy {get;set;}
    
    // Intitiate the parameters
    public PageReference init() {
        // lstTickets = new list<TicketVO.TicketData>();
        id accountId = ApexPages.currentPage().getParameters().get('id');            
        ticketID = ApexPages.currentPage().getParameters().get('ticketID');
        sourceSystem = ApexPages.currentPage().getParameters().get('source');
        if(sourceSystem == lavaStormConstant){
            isLavaStorm = true;
        }else{
            isLavaStorm = false;
        }
        system.debug('***** Id: ' + accountId);
        system.debug('***** ticketID: ' + ticketID);
        
        acct = getAccount(accountId);
        
        allAccountIdsInHierarchy = smb_AccountUtility.getAccountsInHierarchy(accountId, acct.ParentId);
        
        tID = [select Team_TELUS_ID__c from User where id = :Userinfo.getUserId()].Team_TELUS_ID__c;    
        
        matchedCases = [SELECT Id, CaseNumber, Status, Priority, Subject, CreatedDate, RecordType.Name 
                        FROM Case 
                        WHERE AccountId IN :allAccountIdsInHierarchy 
                        AND IsClosed = False 
                        ORDER BY smb_PrioritySortOrder__c DESC, CreatedDate DESC];
        return null;        
    }
    
    //Get account details
    private static Account getAccount(Id accountId) {
        if (accountId == null) return null;
        return [SELECT  Id, ParentId, Name, RCID__c
                FROM Account 
                WHERE Id =: accountId];
    }
    
    /* Process response of continuation callout
       This is call back method for implementation of continuation for trouble tickets
       Get details for specific trouble ticket
       This method will invoke mapping method to preapre the list of tickets from sources. 
       The method should iterate the list of tickets to get specific ticket details. 
       This is implemented using list because TTODS does not support method for specific ticket details 
       and SRM method not imported properly from wsdl as salesforce does not support extension in xsd 
    */
    public Object processTTAggregationResponse(){
        try{
            system.debug('@@@@In response');
            reqResObj.lstTickets = new List<TicketVO.TicketData>();
            if(sourceSystem == TicketVO.SRC_TTODS){
                smb_Ticket_Mapping_Helper.doTTODSMapping(reqResObj.ttListFuture.getValue(), reqResObj);
            }else if (sourceSystem == TicketVO.SRC_SRM){
                smb_Ticket_Mapping_Helper.doSRMMapping(reqResObj.srmTTListFuture.getValue(), reqResObj);
            }else if (sourceSystem == TicketVO.SRC_LavaStorm){
                smb_Ticket_Mapping_Helper.doLavaStormMapping(reqResObj);
            }
            list<TicketVO.TicketData> lstTT = reqResObj.lstTickets;
            // for loop to break the loop once get specific ticket details
            if(lstTT != null){
                Integer troubleTicketCount = lstTT.size();
                System.debug('##@@@ In response troubleTicketCount ' + troubleTicketCount);  
                if(troubleTicketCount != 0){
                    for(TicketVO.TicketData ttObj:lstTT){
                        if(ttObj.TELUSTicketId == reqResObj.ticketIdToSearch){
                            ttDetailObj = ttObj;
                            break; // break the loop if record record found
                        }                     
                    }
                }
            }
        }catch(Exception ex){
            smb_Ticket_Aggregator.logError('smb_SVOCController.getAggregateTickets', '' , ex.getmessage(), ex.getstacktracestring());
        }
        return null;
    }
    
    // Get the details of trouble ticket for each system
    public Continuation getTTDetail(){
        Continuation cont = new Continuation(TicketVO.TIMEOUT_INT_SECS);
        try{
            reqResObj.lstTickets = new List<TicketVO.TicketData>();
            System.debug('##@@@ In apex function');  
            smb_Ticket_Aggregator ttObj = new smb_Ticket_Aggregator();
            // set the input parameter which need pass to aggrgate method
            reqResObj.TIDValue = tID;
            reqResObj.RCIDValue = acct.RCID__c;
            reqResObj.accountId = acct.Id;
            reqResObj.source = sourceSystem;
            reqResObj.ticketIdToSearch = ticketID;
            reqResObj.callBackMethodName = 'processTTAggregationResponse';
            reqResObj.recordLimits = Integer.valueOf(Apexpages.currentPage().getParameters().get('recLimit'));
            reqResObj.upToXLatestModified = Integer.valueOf(Apexpages.currentPage().getParameters().get('upToXLatestModified'));
            // Invoke method to get aggrigate tickets
            cont = ttObj.getTTDetailBySource(reqResObj);
        }catch(Exception ex){
            smb_Ticket_Aggregator.logError('smb_SVOCController.getAggregateTickets', '' , ex.getmessage(), ex.getstacktracestring());
        }
        // Return continuation object to VF page so that callback method will get invoked after all callout executed
        return cont;
    }
    public pageReference getLavaStormTicketDetails(){
        try{
            reqResObj.lstTickets = new List<TicketVO.TicketData>();
            reqResObj.TIDValue = tID;
            reqResObj.RCIDValue = acct.RCID__c;
            reqResObj.accountId = acct.Id;
            reqResObj.source = sourceSystem;
            reqResObj.ticketIdToSearch = ticketID;
            reqResObj.isCallForLavaStormIndividualTT=true;
            smb_Ticket_Mapping_Helper.doLavaStormMapping(reqResObj);
            list<TicketVO.TicketData> lstTT = reqResObj.lstTickets;
            if(lstTT != null){
                    Integer troubleTicketCount = lstTT.size();
                    System.debug('##@@@ In response troubleTicketCount ' + troubleTicketCount);  
                    if(troubleTicketCount != 0){
                        for(TicketVO.TicketData ttObj:lstTT){
                            if(ttObj.TELUSTicketId == reqResObj.ticketIdToSearch){
                                ttDetailObj = ttObj;
                                break; // break the loop if record record found
                            }                     
                        }
                    }
                }
        }catch(Exception ex){
            smb_Ticket_Aggregator.logError('smb_SVOCController.getLavaStormTicketDetails', '' , ex.getmessage(), ex.getstacktracestring());
        }
        return null;
    }
    
   
}