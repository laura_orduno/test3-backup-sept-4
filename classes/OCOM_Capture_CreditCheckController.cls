public class OCOM_Capture_CreditCheckController {
    public Order order {get; set;}
    public String currentCARId {get; set;}
    public String oId;
    public String parentOrderId {get; set;}
    public Boolean isParentOrder {get; set;}
    public String accountId {get; set;}
    public String getOId() { return oId; }
    public void setOId(String inputValue) {
        system.debug('OCOM_Capture_CreditCheckController::setOId() inputValue(' + inputValue + ')');
        oId = inputValue;
        init();
    }
    public Credit_Assessment__c creditAssessment {get; set;}
    public Boolean isCarRequired {get; set;}
    public Boolean existingCar {get; set;}
    public Boolean noCreditProfile {get; set;}
    // Thomas OCOM-923
    public Boolean moreThan10PercentChange{get; set;}
    public Boolean oneOftheChildOrderIsSubmitted{get; set;}
    
    public OCOM_Capture_CreditCheckController() {
        oneOftheChildOrderIsSubmitted = false;
        isParentOrder = false;
        parentOrderId = ApexPages.currentPage().getParameters().get('parentOrderId');
        if(String.isBlank(parentOrderId)){
            parentOrderId= NULL;
        }else{ isParentOrder = true;}
        system.debug('%%######%% START: OCOM_Capture_CreditCheckController:OCOM_Capture_CreditCheckController  %%######%% ');
        init();
        system.debug('%%######%% END: OCOM_Capture_CreditCheckController:OCOM_Capture_CreditCheckController  %%######%% ');
    }
    
    public void init() {
        system.debug('%%######%% START: OCOM_Capture_CreditCheckController:init  %%######%% ');
        system.debug('OCOM_Capture_CreditCheckController::init() oId(' + oId + ')');
        if (oId != null) {
            // Thomas OCOM-923
            List<order> ordlist = [SELECT id, orderMgmtId__c, Credit_Assessment__c, Credit_Assessment_Status__c, vlocity_cmt__AccountId__c,accountId 
                     FROM Order
                     WHERE id=:oId 
                     LIMIT 1];
            if (ordlist != null && ordlist.size()>0) {
                order = ordlist[0];
                system.debug('OCOM_Capture_CreditCheckController::init() order(' + order + ')');
                if (order.Credit_Assessment__c != null) {
                    currentCARId = order.Credit_Assessment__c;
                    creditAssessment = [SELECT id, Name, Owner.name, LastModifiedDate 
                                        FROM Credit_Assessment__c 
                                        WHERE id=:order.Credit_Assessment__c 
                                        LIMIT 1];
                    system.debug('OCOM_Capture_CreditCheckController::init() creditAssessment(' + creditAssessment + ') Owner.name(' + creditAssessment.Owner.name + ')');
                }
                accountId = order.accountId;
            }
        }
    // system.debug('%%######%% END: OCOM_Capture_CreditCheckController:init  %%######%% ');
    // Order 2.0 changes, One CAR for all child orders
        if(String.isNotBlank(parentOrderId)){
        List<order> orderList = [SELECT id, orderMgmtId__c, Credit_Assessment__c, Credit_Assessment_Status__c, vlocity_cmt__AccountId__c,accountId,ParentId__c 
                     FROM Order
                     WHERE id = :parentOrderId or parentId__c = :parentOrderId];
            if (orderList != null && orderList.size() >0 ) {
                 accountId = orderList[0].accountId;
                system.debug('OCOM_Capture_CreditCheckController::init() order(' + order + ')');
                for(Order ord :orderList){
                if (ord.Credit_Assessment__c != null && String.isBlank(ord.parentId__c) && ord.id == parentOrderId) {
                    order = ord;
                    currentCARId = ord.Credit_Assessment__c;
                    creditAssessment = [SELECT id, Name, Owner.name, LastModifiedDate 
                                        FROM Credit_Assessment__c 
                                        WHERE id=:order.Credit_Assessment__c 
                                        LIMIT 1];
                    // system.debug('OCOM_Capture_CreditCheckController::init() creditAssessment(' + creditAssessment + ') Owner.name(' + creditAssessment.Owner.name + ')');
                }else if(ord.parentId__c == parentOrderId && String.isNotBlank(ord.orderMgmtId__c)){
                    oneOftheChildOrderIsSubmitted = true;
                      
                }
             } 
            }
        }
    }
    
    public void invokeCreditAssessment() {
        system.debug('%%######%% START: OCOM_Capture_CreditCheckController:invokeCreditAssessment  %%######%% ');
        system.debug('OCOM_Capture_CreditCheckController::invokeCreditAssessment() starts');
        system.debug('OCOM_Capture_CreditCheckController::invokeCreditAssessment() oId(' + oId + ')');
        
        Map<String, Object> outputMap = new Map<String, Object>();
        OCOM_CreditAssessment_Helper.checkCredit(oId, outputMap); 
        isCarRequired = (Boolean) outputMap.get('isCarRequired');
        existingCar = (Boolean) outputMap.get('existingCar');
        noCreditProfile = (Boolean) outputMap.get('noCreditProfile');
        // Thomas OCOM-923
        moreThan10PercentChange = (Boolean) outputMap.get('moreThan10PercentChange');
        currentCARId = (String) outputMap.get('currentCARId');
        system.debug('OCOM_Capture_CreditCheckController::invokeCreditAssessment() isCarRequired(' + outputMap.get('isCarRequired') + ')');
        system.debug('OCOM_Capture_CreditCheckController::invokeCreditAssessment() existingCar(' + outputMap.get('existingCar') + ')');
        system.debug('OCOM_Capture_CreditCheckController::invokeCreditAssessment() noCreditProfile(' + outputMap.get('noCreditProfile') + ')');
        // Thomas OCOM-923
        system.debug('OCOM_Capture_CreditCheckController::invokeCreditAssessment() moreThan10PercentChange(' + outputMap.get('moreThan10PercentChange') + ')');
        system.debug('OCOM_Capture_CreditCheckController::invokeCreditAssessment() currentCARId(' + outputMap.get('currentCARId') + ')');
        system.debug('OCOM_Capture_CreditCheckController::invokeCreditAssessment() validationFailureMessage(' + outputMap.get('validationFailureMessage') + ')');
        system.debug('OCOM_Capture_CreditCheckController::invokeCreditAssessment() accountId(' + outputMap.get('accountId') + ')');
        system.debug('OCOM_Capture_CreditCheckController::invokeCreditAssessment() orderId(' + outputMap.get('orderId') + ')');
        system.debug('%%######%% END: OCOM_Capture_CreditCheckController:invokeCreditAssessment  %%######%% ');
    }
    
}