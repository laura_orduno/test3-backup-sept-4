/*
###########################################################################
# File..................: smb_OrderTracking_CalloutHelper
# Version...............: 01
# Created by............: Chris Frei / Vinay Sharma
# Created Date..........: 19-September-2013
# Last Modified by......: 
# Last Modified Date....: 
# Description...........: This class contains methods to call OrderTracking web service operations
#
# Copyright (c) 2000-2013. Astadia, Inc. All Rights Reserved.
#
# Created by the Astadia, Inc. Modification must retain the above copyright notice.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any commercial purpose, without fee, and without a written
# agreement from Astadia, Inc., is hereby forbidden. Any modification to source
# code, must include this paragraph and copyright.
#
# Permission is not granted to anyone to use this software for commercial uses.
#
# Contact address: 2839 Paces Ferry Road, Suite 350, Atlanta, GA 30339
# Company URL : http://www.astadia.com
###########################################################################
*/
global class smb_OrderTracking_CalloutHelper {

    public list<smb_orders_trackingtypes_v4.OrderRequest> getOrdersByRCID(string RCID, string toRow){
        if(string.isBlank(RCID)) return null;
        
        list<smb_orders_trackingtypes_v4.OrderRequest> lstOrders = new list<smb_orders_trackingtypes_v4.OrderRequest>();
        
        smb_orders_trackingservice_v4.OrderTrackingService_v4_2_SOAP servicePort = new smb_orders_trackingservice_v4.OrderTrackingService_v4_2_SOAP();
        servicePort = prepareCallout2(servicePort);
                
        smb_orders_trackingtypes_v4.DateRange dueDateRange = new smb_orders_trackingtypes_v4.DateRange();
        //dueDateRange.fromDate = Datetime.valueOf('2011-01-01 12:00:00');
        //system.debug('*** Subtract 1 year: '+system.now().addYears(-1));
        dueDateRange.fromDate = system.now().addYears(-1);              
        //dueDateRange.toDate = Datetime.now();
        dueDateRange.toDate = Datetime.valueOf('9999-12-31 12:00:00');  
        
        smb_orders_trackingtypes_v4.ReturnListRange returnRange = new smb_orders_trackingtypes_v4.ReturnListRange();
        returnRange.fromRowNumber = '1';
        returnRange.toRowNumber = toRow;        
        
        lstOrders = servicePort.getOrderListByRCID(RCID, dueDateRange, returnRange);
        return lstOrders;       
    }
    
    // Added by RB
    
    public list<smb_orders_trackingtypes_v4.OrderRequest> getOrdersByCustomerName(String customerName,String businessUnit,String searchMode){
        if(string.isBlank(customerName)) return null;
        
        list<smb_orders_trackingtypes_v4.OrderRequest> lstOrders = new list<smb_orders_trackingtypes_v4.OrderRequest>();
        
        smb_orders_trackingservice_v4.OrderTrackingService_v4_2_SOAP servicePort = new smb_orders_trackingservice_v4.OrderTrackingService_v4_2_SOAP();
        
        servicePort = prepareCallout2(servicePort);
        // servicePort.endpoint_x = 'http://requestb.in/1fzvim21';
        system.debug('~~~~End point:' + servicePort.endpoint_x);                
        smb_orders_trackingtypes_v4.DateRange createDateRange = new smb_orders_trackingtypes_v4.DateRange();
        createDateRange.toDate = system.now();            
        createDateRange.fromDate = system.now().addDays(-7); 
        lstOrders = servicePort.getLatestOrderListByCustomerName(customerName,businessUnit,createDateRange,searchMode);         
        //system.debug('Total orders received:' + lstOrders.size());
        return lstOrders;       
    }
    
     public list<smb_orders_trackingtypes_v4.OrderRequest> getOrdersByLegacyOrderID(String legacyOderID){
        if(string.isBlank(legacyOderID)) return null;
        
        list<smb_orders_trackingtypes_v4.OrderRequest> lstOrders = new list<smb_orders_trackingtypes_v4.OrderRequest>();
        smb_orders_trackingservice_v4.OrderTrackingService_v4_2_SOAP servicePort = new smb_orders_trackingservice_v4.OrderTrackingService_v4_2_SOAP();
        servicePort = prepareCallout2(servicePort);
        smb_orders_trackingtypes_v4.DateRange dueDateRange = new smb_orders_trackingtypes_v4.DateRange();
        dueDateRange.toDate = Datetime.newinstance(9999,12,31) ;            
        dueDateRange.fromDate = system.now().addYears(-1);              
        //dueDateRange.toDate = Datetime.now();
        dueDateRange.toDate = Datetime.valueOf('9999-12-31 12:00:00'); 
        lstOrders = servicePort.getOrderListByLegacyOrderId(legacyOderID,dueDateRange,null,null,null,null,null);         
        //system.debug('Total orders received:' + lstOrders.size());
        return lstOrders;       
    }
    
    public list<smb_orders_trackingtypes_v4.OrderRequest> getOrdersByRCIDMini(string RCID, string toRow){
        if(string.isBlank(RCID)) return null;
        
        list<smb_orders_trackingtypes_v4.OrderRequest> lstOrders = new list<smb_orders_trackingtypes_v4.OrderRequest>();
        
        smb_orders_trackingservice_v4.OrderTrackingService_v4_2_SOAP servicePort = new smb_orders_trackingservice_v4.OrderTrackingService_v4_2_SOAP();
        servicePort = prepareCallout2(servicePort);
                
        smb_orders_trackingtypes_v4.DateRange dueDateRange = new smb_orders_trackingtypes_v4.DateRange();
        //dueDateRange.fromDate = Datetime.valueOf('2011-01-01 12:00:00');
        //system.debug('*** Subtract 1 year: '+system.now().addYears(-1));
        dueDateRange.fromDate = system.now().addDays(-10);              
        //dueDateRange.toDate = Datetime.now();
        dueDateRange.toDate = system.now().addDays(10); 
        
        smb_orders_trackingtypes_v4.ReturnListRange returnRange = new smb_orders_trackingtypes_v4.ReturnListRange();
        returnRange.fromRowNumber = '1';
        returnRange.toRowNumber = toRow;        
        
        lstOrders = servicePort.getOrderListByRCID(RCID, dueDateRange, returnRange);
        return lstOrders;       
    }

    public list<smb_orders_trackingtypes_v4.OrderRequest> getOrderDetail(string OrderID, string RCID){      
        if(string.isBlank(OrderID)) return null;
        
        list<smb_orders_trackingtypes_v4.OrderRequest> lstOrder = new list<smb_orders_trackingtypes_v4.OrderRequest>();
        
        smb_orders_trackingservice_v4.OrderTrackingService_v4_2_SOAP servicePort = new smb_orders_trackingservice_v4.OrderTrackingService_v4_2_SOAP();
        servicePort = prepareCallout2(servicePort);     
        
        smb_orders_trackingtypes_v4.DateRange dueDateRange = new smb_orders_trackingtypes_v4.DateRange();
        dueDateRange.fromDate = Datetime.valueOf('2011-01-01 12:00:00');
        dueDateRange.toDate = Datetime.now();
        system.debug('check RCID-->'+RCID);
        lstOrder = servicePort.getOrderListByLegacyOrderID(OrderID, dueDateRange, null, null, RCID, null, null);
        //servicePort.getOrderListByLegacyOrderID(legacyOrderId, dueDateRange, customerName, businessUnit, rcid, orderServiceType, circuitNumber)       
        return lstOrder;
    }
    
    public list<smb_orders_trackingtypes_v4.OrderRequest> getDetail(long orderActionID, string legacyOrderID, string seqNum, string sysName, datetime issueDate){

        list<smb_orders_trackingtypes_v4.OrderRequest> lstOrder = new list<smb_orders_trackingtypes_v4.OrderRequest>();
        list<smb_orders_trackingtypes_v4.OrderRequestKey> requestKey = new list<smb_orders_trackingtypes_v4.OrderRequestKey>();
        
        smb_orders_trackingservice_v4.OrderTrackingService_v4_2_SOAP servicePort = new smb_orders_trackingservice_v4.OrderTrackingService_v4_2_SOAP();
        servicePort = prepareCallout2(servicePort);     

        smb_orders_trackingtypes_v4.OrderRequestKey orKey = new smb_orders_trackingtypes_v4.OrderRequestKey();
        orKey.orderActionId = orderActionID;
        orKey.legacyOrderId = legacyOrderID;
        orKey.sequenceNum = seqNum;
        orKey.systemName = sysName;
        orKey.issueDate = issueDate;        

/*      Hard coded for testing 
        orKey.orderActionId = 1005596700;  //Optional but recommended
        orKey.legacyOrderId = 'D7804591444';
        orKey.sequenceNum = '01';
        orKey.systemName = 'CRIS3-AB';
        orKey.issueDate = datetime.valueOf('2012-02-28 00:00:00');  //Shown as optional but apparently this is not true per Jacqueline Viljoen 1/9/14
*/                                  
        requestKey.add(orKey);      

        lstOrder = servicePort.getOrderDetail(requestKey);      
        return lstOrder;
    }
    
    //Added by Arvind to test CRDB_OrderTrackingService_4 but lately merged this with existing classes
    /*
    public list<CRDB_OrderTrackingTypes_v4.OrderRequest> getDetails(long orderActionID, string legacyOrderID, string seqNum, string sysName, datetime issueDate){

        list<CRDB_OrderTrackingTypes_v4.OrderRequest> lstOrder = new list<CRDB_OrderTrackingTypes_v4.OrderRequest>();
        list<CRDB_OrderTrackingTypes_v4.OrderRequestKey> requestKey = new list<CRDB_OrderTrackingTypes_v4.OrderRequestKey>();
        
        CRDB_OrderTrackingService_4.OrderTrackingService_v4_3_SOAP servicePort = new CRDB_OrderTrackingService_4.OrderTrackingService_v4_3_SOAP();
        servicePort = prepareCalloutCRDB(servicePort);     

        CRDB_OrderTrackingTypes_v4.OrderRequestKey orKey = new CRDB_OrderTrackingTypes_v4.OrderRequestKey();
        
        orKey.orderActionId = orderActionID;
        orKey.legacyOrderId = legacyOrderID;
        orKey.sequenceNum = seqNum;
        orKey.systemName = sysName;
        orKey.issueDate = issueDate;  
        requestKey.add(orKey);      

        lstOrder = servicePort.getOrderDetail(requestKey);      
        return lstOrder;
    }
    */
    //Addition End
    
    

    public list<smb_orderTrackingTypes_v4.OrderRequest> getCompletedOrdersByRCID(string RCID)
    {
        if(string.isBlank(RCID)) return null;       
        
        //list of completed orders to return
        list<smb_orderTrackingTypes_v4.OrderRequest> lstOrders = new list<smb_orderTrackingTypes_v4.OrderRequest>();
        
        //web service primary class object
        smb_orderTrackingService_v4.OrderTrackingService_v4_2_SOAP servicePort = new smb_orderTrackingService_v4.OrderTrackingService_v4_2_SOAP(); 
        servicePort = prepareCallout(servicePort);
        
        //business unit
        string businessUnit = 'TBS';  //TBS listed in samples?   
        
        //due date range
        smb_orderTrackingTypes_v4.DateRange dueDateRange = new smb_orderTrackingTypes_v4.DateRange();
        dueDateRange.fromDate = Datetime.valueOf('2009-01-01 00:00:00');
        dueDateRange.toDate = Datetime.now();

        //Order Request Key -- Has system information per Jacqueline Viljoen 11/22
        smb_orderTrackingTypes_v4.OrderRequestKey orderKey = new smb_orderTrackingTypes_v4.OrderRequestKey();       
        
    
        //Callout
        lstOrders = servicePort.getCompletedOrderListByRCID(RCID, dueDateRange, null, businessUnit, null, null, null);
        
        return lstOrders;   
    }

    public smb_orderTrackingService_v4.OrderTrackingService_v4_2_SOAP prepareCallout(smb_orderTrackingService_v4.OrderTrackingService_v4_2_SOAP svcPort) 
    {   
        String creds;
        SMBCare_WebServices__c wsUsername = SMBCare_WebServices__c.getValues('username');
        SMBCare_WebServices__c wsPassword = SMBCare_WebServices__c.getValues('password');
        if (String.isNotBlank(WSusername.Value__c)&& String.isNotBlank(wsPassword.Value__c))
            creds = WSusername.Value__c+':'+wsPassword.Value__c;
        String encodedusernameandpassword = EncodingUtil.base64Encode(Blob.valueOf(creds));
        
        svcPort.inputHttpHeaders_x = new Map<String, String>();
        svcPort.inputHttpHeaders_x.put('Authorization','Basic ' + encodedusernameandpassword );
        return svcPort;                     
    }
    
    public smb_orders_trackingservice_v4.OrderTrackingService_v4_2_SOAP prepareCallout2(smb_orders_trackingservice_v4.OrderTrackingService_v4_2_SOAP svcPort){
        String creds;
        SMBCare_WebServices__c wsUsername = SMBCare_WebServices__c.getValues('username');
        SMBCare_WebServices__c wsPassword = SMBCare_WebServices__c.getValues('password');
        if (String.isNotBlank(WSusername.Value__c)&& String.isNotBlank(wsPassword.Value__c))
            creds = WSusername.Value__c+':'+wsPassword.Value__c;
        String encodedusernameandpassword = EncodingUtil.base64Encode(Blob.valueOf(creds));
        
        svcPort.inputHttpHeaders_x = new Map<String, String>();
        svcPort.inputHttpHeaders_x.put('Authorization','Basic ' + encodedusernameandpassword );
        return svcPort;                     
    }
    
    //Added by Arvind to test CRDB_OrderTrackingService_4 but lately merged this with existing classes
    /*
    public CRDB_OrderTrackingService_4.OrderTrackingService_v4_3_SOAP prepareCalloutCRDB(CRDB_OrderTrackingService_4.OrderTrackingService_v4_3_SOAP svcPort)
    {
            String creds;
            SMBCare_WebServices__c wsUsername = SMBCare_WebServices__c.getValues('username');
            SMBCare_WebServices__c wsPassword = SMBCare_WebServices__c.getValues('password');
            if (String.isNotBlank(WSusername.Value__c)&& String.isNotBlank(wsPassword.Value__c))
            creds = WSusername.Value__c+':'+wsPassword.Value__c;
            String encodedusernameandpassword = EncodingUtil.base64Encode(Blob.valueOf(creds));
            svcPort.inputHttpHeaders_x = new Map<String, String>();
            svcPort.inputHttpHeaders_x.put('Authorization','Basic ' + encodedusernameandpassword );
            return svcPort;                     
    }
    */
    //Addition End
    

}