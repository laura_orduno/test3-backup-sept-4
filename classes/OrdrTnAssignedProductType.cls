/**
 * @author Santosh Rath
 * Used for Telephone Number Reservation
 * Need to track in Salesforce/Vlocity which product type is assigned to a given Reserved TN
 * This is to clear display to Agents where a TN is attached
 * 
 * SL - Voice Single-Line, 
 * ML - Voice Multi-line, 
 * OL - Voice Overline,
 * SR - Voice Smart Ring,
 * ADSL - Internet Dark ADSL, 
 * WLS - Wireless
 * TF - Toll-Free
 */
public Enum   OrdrTnAssignedProductType {
    SL, ML, OL, SR, ADSL, WLS, TF
}