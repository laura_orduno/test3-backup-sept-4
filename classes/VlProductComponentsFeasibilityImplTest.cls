@isTest
private class VlProductComponentsFeasibilityImplTest {
    
    @isTest
    private static void testInvokeMethod() {
        VlProductComponentsFeasibilityImpl componentFeasibility = new VlProductComponentsFeasibilityImpl();
        Map<String, Object> input = new Map<String, Object>();
        Map<String, Object> output = new Map<String, Object>();
        Map<String, Object> options = new Map<String, Object>();
        
        List<Order> orders = OrdrTestDataFactory.createOrdersWithServiceAddress();
        input.put('objectId', orders[0].Id);
        
        List<vlocity_cmt.ProductWrapper> productDefinitions = new List<vlocity_cmt.ProductWrapper>();
        vlocity_cmt.ProductWrapper product = new vlocity_cmt.ProductWrapper();
        product.productId='01t40000004JwWyAAK';
        product.name = 'Office Internet Basic';
        product.JSONAttribute = '{"TELUSCHAR":[{"name":"Office Internet Basic","attributedisplayname__c":"Download speed","attributeRunTimeInfo":{"values":[{"displayText":"25 Mbps"}]}}]}';
        productDefinitions.add(product);
        input.put('productDefinition', productDefinitions);
        
        vlocity_cmt__ProductChildItem__c pci = new vlocity_cmt__ProductChildItem__c(vlocity_cmt__ChildLineNumber__c='001', vlocity_cmt__ChildProductId__c='01t40000004JwWyAAK', OCOMOverrideType__c='Override Child');
        insert pci;  
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        Boolean returnVal = componentFeasibility.invokeMethod('configureProduct', input, output, options);
        System.assert(returnVal);
        Test.stopTest();
        
    }
    
    @isTest
    private static void testInvokeMethod_Exception() {
        VlProductComponentsFeasibilityImpl componentFeasibility = new VlProductComponentsFeasibilityImpl();
        Map<String, Object> input = new Map<String, Object>();
        Map<String, Object> output = new Map<String, Object>();
        Map<String, Object> options = new Map<String, Object>();
        
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        try {
        	Boolean returnVal = componentFeasibility.invokeMethod('reConfigureProduct', input, output, options);
            System.assert(false, 'Exception expected');
        }
        catch(Exception e) {
            System.debug(e.getTypeName() + ': ' + e.getMessage());
        }
    }
    
}