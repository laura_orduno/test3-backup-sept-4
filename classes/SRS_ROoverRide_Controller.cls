/*
##################################################################################
# File..................: SRS_ROoverRide_Controller
# Version...............: 01
# Created by............: Rahul Badal
# Created Date..........: 03-September-2014
# Last Modified by......: I-yuan Han
# Last Modified Date....: 31-Oct0ber-2014
# Modification .........: Applied changes for seq__c that was changed from Numeric(3,0) to Text (3)  
# Description...........: This class is the Controller for CRDB Order Detail Page
#
##################################################################################
*/
public with sharing class SRS_ROoverRide_Controller {
//*****************************************************
    public SRS_Service_Request_Related_Order__c relatedOrder {get;set;}
    public string RelatedOrderId {get;set;}
    public boolean showDetail{get;set;}
    public string tabName{get;set;}
    public list<orderWrapper> orderList {get;set;}
    public list<smb_orders_trackingtypes_v4.OrderRequest> lstOrders {get;set;}
    public string customerName {get;set;}
    public string cRCID {get;set;}  
    public string bContact {get;set;}
    private string sequence;
    public SRS_ROoverRide_Controller (ApexPages.StandardController stdController)
    {        
    try
        {
         RelatedOrderId = stdController.getId();
         relatedOrder = [select isValid__C,Name,id,seq__C from SRS_Service_Request_Related_Order__c where id =:RelatedOrderId];
         tabName = relatedOrder.Name; 

         if(relatedOrder.isValid__C)
         {
            // Make a Web Service Call
            showDetail = false;
            system.debug('!!!!'+relatedOrder);
            if(relatedOrder.seq__C != NULL)
                sequence = relatedOrder.seq__C;             
            getOrderDetail(relatedOrder.Name); 
         }
         else
         {
            showDetail = true;
            PageReference redirectPage = new PageReference('/' + RelatedOrderId);
            redirectPage.setRedirect(true);
         }
        }
    catch(exception e)
       {
           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please contact the Administrator, there is some issue'+ e.getmessage()));
               
       }  
    }
        
    public void getOrderDetail(string orderId){
        smb_OrderTracking_CalloutHelper call = new smb_OrderTracking_CalloutHelper();
        list<smb_orders_trackingtypes_v4.OrderRequest> lstOrders = new list<smb_orders_trackingtypes_v4.OrderRequest>();
        lstOrders = call.getOrdersByLegacyOrderID(orderId);       
      
        if(lstOrders != null) 
         {   System.debug('~~~~~~~~COUNT->'+lstOrders.size()); 
        orderList = new list<orderWrapper>(); 

        for(smb_orders_trackingtypes_v4.OrderRequest lstOR:lstOrders){
            customerName = lstOR.customer.customerName;
            cRCID = lstOR.customer.rcid;                
            bContact = lstOR.customer.billingContact;  
            system.debug('~~~~: Original Sequence ' +  sequence);
            system.debug('~~~~: CRDB Sequence ' +  string.valueof(lstOR.orderKey.sequenceNum));
            
            if(string.isnotblank(sequence)&& string.isnotblank(lstOR.orderKey.sequenceNum))
            {
                if(integer.valueof(sequence) == integer.valueof(lstOR.orderKey.sequenceNum))
                   orderList.add(new orderWrapper(lstOR));
            }  
            else
                orderList.add(new orderWrapper(lstOR));
                                 
        }                       

        System.debug('FINAL->'+orderList);      
        //return orderList;           
         }   
          
    }

    public class orderWrapper { 
        public string orderID {get;set;}
        // Added by Arvind to get orderActionID and other fields as these are being used in getOrderDetail()
        public string orderActionID {get;set;} 
        public string sequenceNum {get;set;}
        public string issueDate {get;set;}
        public list<smb_orders_trackingtypes_v4.OrderRequest> listOrders {get;set;}
        public string orderType {get;set;}
        public DateTime orderRequestedDate {get;set;}
        public string orderStatus {get;set;}        
        public string orderServiceType {get;set;}
        public string legacyOrderType {get;set;}
        public string dataOrderType {get;set;}
        public string voiceOrderType {get;set;}
        public string bAddress{get;set;}
        public DateTime orderDueDate {get;set;}
        public string systemName {get;set;}
        public string holdJEOP {get;set;}
        public datetime custReqDate {get;set;}
        public datetime createDate{get;set;}
        public datetime completionDate {get;set;}
        public string dispStatus {get;set;}
        public string origID {get;set;} 
        public string circuitNumber {get;set;}  
        public string seqNumber {get;set;}
        public string fmcStatus {get;set;}
        public string custName {get;set;}
        public string custRCIDName {get;set;}
        public string custRCID {get;set;}
        public datetime origDueDate {get;set;}
        public datetime erdDueDate {get;set;}
        public datetime erdCompletedDate {get;set;}
        public datetime prdDueDate {get;set;}
        public datetime prdCompletedDate {get;set;}
        public datetime ptdDueDate {get;set;}
        public datetime ptdCompletedDate {get;set;}
        public string equipmentRemark {get;set;}
        public string dispatchRemark {get;set;}
        public string ecopsDiaryRemark {get;set;}
        //Added by Arvind
        public List<smb_orders_trackingtypes_v4.GenericRemark>  GenericRemark {get;set;}
        public String Remarks {get;set;}
        public String RemarksString  {get;set;}
        //public string psiFoxDiaryRemark {get;set;}
        public string psiFox {get;set;}  
        public string coordinatorTid {get;set;}     
        public string traceNumber {get;set;}
        public double csID {get;set;}
        public string facilityTrackingUsso {get;set;}
        public string facilityTrackingCircuit {get;set;}
        public string outOfBandOrder {get;set;}
        public string erdCustName {get;set;}  
        public string outOfBandCircuit {get;set;}
        public string circuitSpeed {get;set;}
        public string offNetCircuit {get;set;}
        public string requestAddress {get;set;}
        public string requestCity {get;set;}
        public string requestProvince {get;set;}          
        //Sandip - 08 June 2016 - Translation Phase II
        public String strOrderRequestedDate {get;set;}
        public String strOrderDueDate {get;set;}
        public String strCustReqDate {get;set;}
        public String strCreateDate {get;set;}
        public String strCompletionDate {get;set;}
        public String strOrigDueDate {get;set;}
        public String strErdDueDate {get;set;}
        public String strErdCompletedDate {get;set;}
        public String strPrdDueDate {get;set;}
        public String strPrdCompletedDate {get;set;}
        public String strPtdDueDate {get;set;}
        public String strPtdCompletedDate {get;set;}
        //public String strIssueDate {get;set;}
        // End         
        public orderWrapper(smb_orders_trackingtypes_v4.OrderRequest legacyOrders){   
            system.debug('~~~~~~~~~~~~~~~~~~~~~~~~' + legacyOrders);                                      
            systemName = legacyOrders.orderKey.systemName;
            boolean toAdd = false;
            //Added by Arvind 
            if(legacyOrders.orderKey.legacyOrderId != null) orderID = legacyOrders.orderKey.legacyOrderId;
            orderActionID = string.valueOf(legacyOrders.orderKey.orderActionId);     
            sequenceNum = legacyOrders.orderKey.sequenceNum;
            issueDate = string.valueOfGmt(legacyOrders.orderKey.issueDate);
            
            if(legacyOrders.orderTypeSet.legacyOrderType != null) legacyOrderType = legacyOrders.orderTypeSet.legacyOrderType;
            //product
            holdJEOP = legacyOrders.holdJeopCode;
            if(legacyOrders.orderStatusSet.orderStatus != null) orderStatus = legacyOrders.orderStatusSet.orderStatus;
            if(legacyOrders.orderDueDateSet.dueDate != null) orderDueDate = legacyOrders.orderDueDateSet.dueDate;

            custReqDate = legacyOrders.orderDueDateSet.customerRequestDate;
           
            createDate = legacyOrders.orderDueDateSet.creationDate;
            //createDate = ApexPages.currentPage().getParameters().get('issueDate');
            
            completionDate = legacyOrders.orderDueDateSet.completionDate;
            psiFox = legacyOrders.orderIdSet.psiFoxOrder;                       
            dispStatus = legacyOrders.orderStatusSet.dispatchStatusCd;
            origID = legacyOrders.orderIdSet.originatorId;
            
            if(legacyOrders.orderTypeSet.orderServiceType != null) orderServiceType = legacyOrders.orderTypeSet.orderServiceType;               
            if(legacyOrders.orderTypeSet.dataOrderType != null) dataOrderType = legacyOrders.orderTypeSet.dataOrderType;
            if(legacyOrders.orderTypeSet.orderType != null) orderType = legacyOrders.orderTypeSet.orderType;
            if(legacyOrders.orderTypeSet.voiceOrderType != null) voiceOrderType = legacyOrders.orderTypeSet.voiceOrderType;
            
            bAddress = legacyOrders.customer.billingAddress;
            circuitNumber = legacyOrders.orderKey.circuitNumber;        
            seqNumber = legacyOrders.orderKey.sequenceNum;
            
            fmcStatus = legacyOrders.orderStatusSet.fmcStatus;
            
            custName = legacyOrders.customer.customerName;
            custRCIDName = legacyOrders.customer.rcidName;
            custRCID = legacyOrders.customer.rcid;
            
            origDueDate = legacyOrders.orderDueDateSet.originalDueDate;
            erdDueDate = legacyOrders.orderDueDateSet.erdDueDate;
            erdCompletedDate = legacyOrders.orderCompletedDateSet.erdCompletedDate;
            prdDueDate = legacyOrders.orderDueDateSet.prdDueDate;
            prdCompletedDate = legacyOrders.orderCompletedDateSet.prdCompletedDate;
            ptdDueDate = legacyOrders.orderDueDateSet.ptdDueDate;
            ptdCompletedDate = legacyOrders.orderCompletedDateSet.ptdCompletedDate;

            coordinatorTid = legacyOrders.orderIdSet.coordinatorTid != null ? legacyOrders.orderIdSet.coordinatorTid : '' ;
            traceNumber = legacyOrders.orderIdSet.traceNumber != null ? legacyOrders.orderIdSet.traceNumber : '';

            system.debug('~~~~~~~~~~~~~~~possible trouble');
            system.debug('!!!legacyOrders'+legacyOrders);
             facilityTrackingCircuit = null != legacyOrders.facilityTrackingInfo ? legacyOrders.facilityTrackingInfo.facilityTrackingCircuit : '';  // Error:  Attempt to de-reference a null object?         
             outOfBandOrder = null != legacyOrders.outOfBandInfo ? legacyOrders.outOfBandInfo.outOfBandOrder : '';      // Error:  Attempt to de-reference a null object
             outOfBandCircuit = null != legacyOrders.outOfBandInfo ? legacyOrders.outOfBandInfo.outOfBandCircuit : '';  // Error:  Attempt to de-reference a null object
             circuitSpeed = null != legacyOrders.orderAttributeList ? legacyOrders.orderAttributeList.circuitSpeed : '';     // Error:  Attempt to de-reference a null object
             offNetCircuit = null != legacyOrders.outOfBandInfo ? legacyOrders.orderAttributeList.offNetCircuit : '';   // Error:  Attempt to de-reference a null object
             erdCustName = null != legacyOrders.orderAttributeList ? legacyOrders.orderAttributeList.endCustomerName : '';   // Error:  Attempt to de-reference a null object
             equipmentRemark = null != legacyOrders.remarkList  ? legacyOrders.remarkList.equipmentRemark : ''; // Error:  Attempt to de-reference a null object
             dispatchRemark = null != legacyOrders.remarkList  ? legacyOrders.remarkList.dispatchRemark : ''; // Error:  Attempt to de-reference a null object
             ecopsDiaryRemark = null != legacyOrders.remarkList  ? legacyOrders.remarkList.ecopsDiaryRemark : '' ; // Error:  Attempt to de-reference a null object
            
             //GenericRemark = legacyOrders.remarkList.genericRemarkList;
             
             // Added by Arvind on 11/04/2016 as part of CRDB CHANGE ORDER No. 05 to SOW No. 2015-09
            System.debug('orderActionID####'+orderActionID+orderID+sequenceNum+systemName+issueDate) ;
            //if(orderActionID !='' && orderID !='' && sequenceNum !='' && systemName !='' && issueDate !='') 
            if(!Test.isrunningtest())
            RemarksString = getOrderRemarks(orderActionID, orderID, sequenceNum, systemName, issueDate);
			//Test Record
			//RemarksString = getOrderRemarks('1006443628', 'C73395', '1', 'TOM-TQ', 'Wed Dec 23 00:00:00 GMT 2015');
            requestAddress = legacyOrders.orderAddress.get(0).Address; 
            requestCity =  legacyOrders.orderAddress.get(0).city;
            requestProvince = legacyOrders.orderAddress.get(0).province; 
            csID = legacyOrders.orderIdSet.csID ;
            //Sandip - 08 June 2016 - Translation Phase II
            if(UserInfo.getLocale().startsWith('fr') && orderStatus != null){
                Order_Status_Translation__c objStatus = Order_Status_Translation__c.getInstance(orderStatus);
                if(objStatus != null && objStatus.French_Translation_Value__c != null){
                    orderStatus = objStatus.French_Translation_Value__c;
                }
            }
            try{
                 system.debug('@@@ test date: ' + createDate);
                 strOrderDueDate = SMB_WebserviceHelper.getDateFormat(OrderDueDate);
                 strCustReqDate = SMB_WebserviceHelper.getDateFormat(custReqDate);
                 strCreateDate = SMB_WebserviceHelper.getDateFormat(createDate);
                 strCompletionDate = SMB_WebserviceHelper.getDateFormat(completionDate);
                 strOrderRequestedDate = SMB_WebserviceHelper.getDateFormat(orderRequestedDate);
                 strOrigDueDate = SMB_WebserviceHelper.getDateFormat(origDueDate);
                 strErdDueDate = SMB_WebserviceHelper.getDateFormat(erdDueDate);
                 strErdCompletedDate = SMB_WebserviceHelper.getDateFormat(erdCompletedDate);
                 strPrdDueDate = SMB_WebserviceHelper.getDateFormat(prdDueDate);
                 strPrdCompletedDate = SMB_WebserviceHelper.getDateFormat(prdCompletedDate);
                 strPtdDueDate = SMB_WebserviceHelper.getDateFormat(ptdDueDate);
                 strPtdCompletedDate = SMB_WebserviceHelper.getDateFormat(ptdCompletedDate);
                 //strIssueDate = SMB_WebserviceHelper.getDateFormat(issueDate);
            }catch(Exception ex){
                system.debug('###lstLegacyID dates: '+ OrderDueDate + ' ' + custReqDate);
                system.debug('###lstLegacyID dates: '+ createDate + ' ' + completionDate);
            }
        }   
       // New Method Added to get the GenericRemarks through getOrderDetail() "Added by Arvind on 11/04/2016 as part of CRDB CHANGE ORDER No. 05 to SOW No. 2015-09"
   	  public String getOrderRemarks(string orderActionId, string orderId, string seqNumber, string sysName, string issueDate){
	    String Remarks ='';
        smb_OrderTracking_CalloutHelper smbCallOut = new smb_OrderTracking_CalloutHelper();
        listOrders = new list<smb_orders_trackingtypes_v4.OrderRequest>();       

        Map <String, Integer> monthNames = new Map <String, Integer> {'Jan'=>1, 'Feb'=>2, 'Mar'=>3, 'Apr'=>4, 'May'=>5, 'Jun'=>6, 'Jul'=>7, 'Aug'=>8, 'Sep'=>9, 'Oct'=>10, 'Nov'=>11, 'Dec'=>12};
        List <String> stringParts = issueDate.split(' ');
        List <String> timeParts=new List<String>(); 
        List<String> dateSplit=new List<String>();
        if(stringParts.size()==6) 
            timeParts = stringParts[3].split(':');
        else
        {
            timeParts = stringParts[1].split(':');
            dateSplit=stringParts[0].split('-');
        }
        
        DateTime iDate=null;
        if(stringParts.size()==6)
            iDate = DateTime.newInstanceGmt(Integer.valueOf(stringParts[5]), monthNames.get(stringParts[1]), Integer.valueOf(stringParts[2]), Integer.valueOf(timeParts[0]), Integer.valueOf(timeParts[1]), Integer.valueOf(timeParts[2]));
        else
            iDate = DateTime.newInstanceGmt(Integer.valueOf(dateSplit[0]), Integer.valueOf(dateSplit[1]), Integer.valueOf(dateSplit[2]), Integer.valueOf(timeParts[0]), Integer.valueOf(timeParts[1]), Integer.valueOf(timeParts[2]));                  
        Long actionID = long.valueOf(orderActionId);
       
        listOrders = smbCallOut.getDetail(actionID, orderId, seqNumber, sysName, iDate); 
      
        if(listOrders != null) 
        {
        for(smb_orders_trackingtypes_v4.OrderRequest lstOR:listOrders){
        	if(null!=lstOR&&null!=lstOR.remarkList&&null!=lstOR.remarkList.genericRemarkList)
        	for( Integer i=0; i < lstOR.remarkList.genericRemarkList.size() ; i++ ){
            Remarks  += lstOR.remarkList.genericRemarkList.get(i).genericRemarkText+'<br/><br/>';   
            }            
        	}
        }
       return Remarks;             
       }
	}
    
    
}