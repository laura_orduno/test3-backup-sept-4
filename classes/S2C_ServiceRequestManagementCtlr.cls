/**
 * S2C_ServiceRequestManagementCtlr
 * @description Manages the service request for a contract. Display all the contracts that are related through
 *              opportunity the contract is associated with.
 * @author Thomas Tran, Traction on Demand
 * @date 03-16-2015
 */
public with sharing class S2C_ServiceRequestManagementCtlr {
    private static final String OPPORTUNITY_ID = 'oppId';
    private static final String CONTRACT_ID = 'contractId';
    private static final String REDIRECT_URL = 'redirect';

    public Id oppId {get; set;}
    public Id contractId {get; set;}
    public String redirectURL {get; set;}

    public List<ServiceRequest> relatedSRList {get; set;}
    public id contractSFDCRecordTypeId{get;set;}
    public List<ServiceRequest> unrelatedSRList {get; set;}
    public id contractLegacyRecordTypeId{get;set;}

    public Boolean isReRender { get; set; }

    public S2C_ServiceRequestManagementCtlr() {
        this.oppId = ApexPages.currentPage().getParameters().get(OPPORTUNITY_ID);
        this.contractId = ApexPages.currentPage().getParameters().get(CONTRACT_ID);
        this.redirectURL = Apexpages.currentPage().getParameters().get(REDIRECT_URL);

        System.debug('Redirect: ' + redirectURL);
        /* Commented by Arvind to reduce SOQL.. added Schema.SObjectType to get RecordType Ids..
        list<recordtype> contractRecordTypes=[select id,developername from recordtype where sobjecttype='Contracts__c'];
        for(recordtype rt:contractRecordTypes){
            if(rt.developername.equalsignorecase('eContract')){
                contractSFDCRecordTypeId=rt.id;
            }else if(rt.developername.equalsignorecase('legacy')){
                contractLegacyRecordTypeId=rt.id;
            }
        }
        */
        contractSFDCRecordTypeId= Schema.SObjectType.Contracts__c.getRecordTypeInfosByName().get('eContract').getRecordTypeId();
        contractLegacyRecordTypeId= Schema.SObjectType.Contracts__c.getRecordTypeInfosByName().get('Legacy').getRecordTypeId();
        
        relatedSRList = getRelatedSRs(oppId, contractId);
        unrelatedSRList = getUnrelatedSRs(oppId, contractId);
        isReRender = false;
    }

    /**
     * getRelatedSRs
     * @description Retrieves all the related service request and service addresses associated to the SR.
     * @author Thomas Tran, Traction on Demand
     * @date 03-16-2015
     * @param Id, Id
     * @return List
     */
    private List<ServiceRequest> getRelatedSRs(Id oppId, Id contractId){
        List<ServiceRequest> serviceRequestList = new List<ServiceRequest>();
        List<Service_Request__c> mySRs = new List<Service_Request__c>();        
        Contracts__c myContract = [Select id, recordtypeid from contracts__c where id =: contractId limit 1];
        
        
        system.debug('RecordTypeId: '+myContract.recordtypeid);
        system.debug('contractSFDCRecordTypeId: '+contractSFDCRecordTypeId);
        if (myContract.recordtypeid==contractSFDCRecordTypeId)
        {
            mySRs = [SELECT Name, Service_Request_Type__c, SRS_PODS_Product__r.Name, Status__c, CreatedDate, Opportunity__r.Name, Contract__c, Service_Request_Status__c, Contract_Length__c,Contractible_Service__c,quantity__c,
                     contract__r.recordtypeid,contract__r.recordtype.name,contract__r.econtract__r.status,forborne_service__c,forborne__c,
                                                (SELECT Full_Address__c, City__c, Province__c FROM Service_Address__r)
                                                FROM Service_Request__c
                                                WHERE Opportunity__c = :oppId and Contractible_Service__c!='' and forborne_service__c=true
                                                AND ((Contract__c = null OR Contract__c = :contractId) 
                                                    AND (Contract_Length__c > 0)
                                                    AND ((Service_Request_Status__c != 'Completed' AND Service_Request_Status__c != 'Cancelled') OR (Contract__c = :contractId)))];
        }
        else
        {
             mySRs = [SELECT Name, Service_Request_Type__c, SRS_PODS_Product__r.Name, Status__c, CreatedDate, Opportunity__r.Name, Contract__c, Service_Request_Status__c, Contract_Length__c,Contractible_Service__c,quantity__c,
                      contract__r.recordtypeid,contract__r.recordtype.name,contract__r.econtract__r.status,forborne_service__c,forborne__c,
                                                (SELECT Full_Address__c, City__c, Province__c FROM Service_Address__r)
                                                FROM Service_Request__c
                                                WHERE Opportunity__c = :oppId
                                                AND ((Contract__c = null OR Contract__c = :contractId) 
                                                    AND (Contract_Length__c > 0)
                                                    AND ((Service_Request_Status__c != 'Completed' AND Service_Request_Status__c != 'Cancelled') OR (Contract__c = :contractId)))];
            
        }
        /*[SELECT Name, Service_Request_Type__c, SRS_PODS_Product__r.Name, Status__c, CreatedDate, Opportunity__r.Name, Contract__c, Service_Request_Status__c, Contract_Length__c,Contractible_Service__c,
                                                (SELECT Full_Address__c, City__c, Province__c FROM Service_Address__r)
                                                FROM Service_Request__c
                                                WHERE Opportunity__c = :oppId and Contractible_Service__c!=null and contract__r.recordtypeid=:contractSFDCRecordTypeId
                                                AND ((Contract__c = null OR Contract__c = :contractId) 
                                                    AND (Contract_Length__c > 0)
                                                    AND ((Service_Request_Status__c != 'Completed' AND Service_Request_Status__c != 'Cancelled') OR (Contract__c = :contractId)))]*/
        
        for(Service_Request__c currentSRTemp : mySRs) {
                                                
            serviceRequestList.add(new ServiceRequest(currentSRTemp));
        }

        return serviceRequestList;
    }

    /**
     * getUnrelatedSRs
     * @description Retrieves all the related service request and service addresses associated to the SR.
     * @author Thomas Tran, Traction on Demand
     * @date 03-16-2015
     * @param Id, Id
     * @return List
     */
    private List<ServiceRequest> getUnrelatedSRs(Id oppId, Id contractId){
        List<ServiceRequest> serviceRequestList = new List<ServiceRequest>();
        List<Service_Request__c> mySRs = new List<Service_Request__c>(); 
        Contracts__c myContract = [Select id, recordtypeid from contracts__c where id =: contractId limit 1];
        
        
        system.debug('RecordTypeId: '+myContract.recordtypeid);
        system.debug('contractSFDCRecordTypeId: '+contractSFDCRecordTypeId);
        //Recordtype = contract
        if (myContract.recordtypeid==contractSFDCRecordTypeId)
        {
           system.debug('Line 116');
            mySRs =  [SELECT Id, Name, Contract__c, Service_Request_Type__c, SRS_PODS_Product__r.Name, Status__c, CreatedDate, Opportunity__r.Name, Service_Request_Status__c, Contract_Length__c,Contractible_Service__c,quantity__c,
                      contract__r.recordtypeid,contract__r.recordtype.name,contract__r.econtract__r.status,forborne_service__c,forborne__c,
                      (SELECT Full_Address__c, City__c, Province__c FROM Service_Address__r)
                      FROM Service_Request__c
                      WHERE Opportunity__c = :oppId
                      AND (Contractible_Service__c = null 
                           OR (Contractible_Service__c <> '' AND (Forborne__c='No' OR Forborne__c=''))
                           OR  ((Contract__c != '' AND Contract__c != :contractId)                            
                                OR (Contract_Length__c = 0)
                                OR ((Service_Request_Status__c = 'Completed' OR Service_Request_Status__c = 'Cancelled') AND (Contract__c != :contractId))))];            
            
        }
        else //Recordtype = Legacy
        {            
           system.debug('Line 130'); 
            mySRs = [SELECT Id, Name, Contract__c, Service_Request_Type__c, SRS_PODS_Product__r.Name, Status__c, CreatedDate, Opportunity__r.Name, Service_Request_Status__c, Contract_Length__c,Contractible_Service__c,quantity__c,
                     contract__r.recordtypeid,contract__r.recordtype.name,contract__r.econtract__r.status,forborne_service__c,forborne__c,
                     (SELECT Full_Address__c, City__c, Province__c FROM Service_Address__r)
                     FROM Service_Request__c
                     WHERE Opportunity__c = :oppId
                     AND (((Contract__c != null AND Contract__c != :contractId)                                                      
                           OR (Contract_Length__c = 0)
                           OR ((Service_Request_Status__c = 'Completed' OR Service_Request_Status__c = 'Cancelled') AND (Contract__c != :contractId)))) ];            
        }
            

        for(Service_Request__c currentSRTemp : mySRs){
            serviceRequestList.add(new ServiceRequest(currentSRTemp));
        }

        return serviceRequestList;
    }

    /**
     * saveRecords
     * @description Updates the SR based on the boolean field being checked.
     *              If its checked, it'll assign the contract Id to the SR otherwise
     *              it will remove it from the SR.
     * @author Thomas Tran, Traction on Demand
     * @date 03-16-2015
     * @param n/a
     * @return PageReference
     */
    public void saveRecords(){
        List<Service_Request__c> srToUpdate = new List<Service_Request__c>();
        Service_Request__c srTemp;

        for(ServiceRequest currentSRTemp : relatedSRList){
            srTemp = currentSRTemp.serviceRequest;
            system.debug('Quantity:'+currentSRTemp.serviceRequest.quantity__c);
            if(currentSRTemp.isAssigned){
                srTemp.Contract__c = contractId;
            } else{
                srTemp.Contract__c = null;
            }
            //if(currentSRTemp.currentAssigned == currentSRTemp.isAssigned) {
                srToUpdate.add(srTemp);
            //}
            system.debug('update size:'+srToUpdate.size());
        }

        if(!srToUpdate.isEmpty()){
            try {
                update srToUpdate;

            } catch(DMLException e) {
                system.debug('update size:'+e.getmessage());
                isReRender = true;
                ApexPages.Message msg = new ApexPages.Message(Apexpages.Severity.ERROR, e.getdmlMessage(0));
                ApexPages.addMessage(msg);
                PageReference p = ApexPages.Currentpage();

            }
            
        }

    }

    /**
     * closeWindow
     * @description Refereshes the window
     * @author Thomas Tran, Traction on Demand
     * @date 03-16-2015
     * @param n/a
     * @return PageReference
     */
    public PageReference closeWindow(){
        return new PageReference('/console'); 
    }

    /**
     * ServiceRequest
     * @description Inner class that helps consolidate the service request information
     *              to be displayed.
     * @author Thomas Tran, Traction on Demand
     * @date 03-16-2015
     */
    public class ServiceRequest{
        public Boolean isAssigned {get; set;}
        public Boolean currentAssigned { get; set; }
        public String serviceRequestStatus { get; set; }
        public Service_Request__c serviceRequest {get; set;}
        public String address {get; set;}
        public String city {get; set;}
        public String province {get; set;}
        /*
        public boolean isQuantityReadOnly{get;set;}
        public boolean isQuantityRendered{get;set;}*/

        public ServiceRequest(Service_Request__c serviceRequest){
            this.serviceRequest = serviceRequest;
            serviceRequestStatus = serviceRequest.Service_Request_Status__c;
            List<SRS_Service_Address__c> serviceAddress = serviceRequest.Service_Address__r;

            if(serviceRequest.Contract__c != null){
                isAssigned = true;
                currentAssigned = false;
            } else{
                isAssigned = false;
                currentAssigned = true;
            }
            /*
            isQuantityReadOnly=true;
            isQuantityRendered=false;
            if(serviceRequest.contract__c==null||(serviceRequest.contract__c!=null&&string.isnotblank(serviceRequest.contract__r.recordtypeid)&&serviceRequest.contract__r.recordtype.name.equalsignorecase('eContract'))){
                isQuantityRendered=true;
            }
            if(serviceRequest.contract__c==null||serviceRequest.contract__r.econtract__c==null||((serviceRequest.contract__r.recordtypeid!=null&&string.isnotblank(serviceRequest.contract__r.recordtype.name)&&!serviceRequest.contract__r.recordtype.name.equalsignorecase('legacy'))&&
               (serviceRequest.contract__r.econtract__c!=null&&string.isnotblank(serviceRequest.contract__r.econtract__r.status)&&(serviceRequest.contract__r.econtract__r.status.equalsignorecase('draft')||serviceRequest.contract__r.econtract__r.status.equalsignorecase('cancelled'))))){
                isQuantityReadOnly=false;
            }*/

            if(!serviceAddress.isEmpty()){
                SRS_Service_Address__c firstAddress = serviceAddress[0];

                this.address = firstAddress.Full_Address__c;
                this.city = firstAddress.City__c;
                this.province = firstAddress.Province__c;
            }
        }
    }
}