/*
    *******************************************************************************************************************************
    Class Name:     OC_ValidateCustomerOrderTest 
    Purpose:        Test code for OC_ValidateCustomerOrder class        
    Created by:     Sandip Chaudhari   02-Aug-2017        
    Modified by:    
    *******************************************************************************************************************************
*/

@isTest
private class OC_ValidateCustomerOrderTest {
    public static Id masterOrderId;
    public static Product2 productInst;
    public static Id orderItemId0;
    private static Map<Id,List<Id>> createData() {
        Credit_Assessment__c aCAR = new Credit_Assessment__c(CAR_Status__c='Draft');
        insert aCAR;
        
        
        Account accObj = new Account(Name='TestAccount', Account_Status__c = 'Active');
        insert accObj;
        
        List<Credit_Profile__c> cpList = new List<Credit_Profile__c>();
        Credit_Profile__c cpObj = new Credit_Profile__c();
        cpObj.Account__c = accObj.Id;
        cpObj.credit_reference_TL__c = 'test';
        cpList.add(cpObj);
        Credit_Profile__c cpObj1 = new Credit_Profile__c();
        cpObj1.Account__c = accObj.Id;
        cpObj1.credit_reference_TL__c = 'test';
        cpList.add(cpObj1);
        insert cpList;
        
        Id pricebookId= Test.getStandardPricebookId();
        
        productInst = new Product2(name = 'Test Product', productcode = 'TESTCODE', isActive = true);
        productInst.Sellable__c=true;
        productInst.orderMgmtId__c='123456780';
        productInst.vlocity_cmt__TrackAsAgreement__c=true;
        productInst.IncludeQuoteContract__c=false;
        productInst.OCOM_Shippable__c = 'Yes';
        insert productInst;
        PricebookEntry pbe = new PricebookEntry(product2id = productInst.id, unitprice = 1,  isactive = true, pricebook2id = pricebookId);
        insert pbe;
        
        Order masterOrder=new Order();
        masterOrder.AccountId=accObj.Id;
        masterOrder.CurrencyIsoCode='CAD';
        masterOrder.EffectiveDate=Date.today();
        masterOrder.Name='MasterOrder';
        masterOrder.Status='Not Submitted';
        insert masterOrder;
        masterOrderId = masterOrder.Id;
        
        List<Order> childOrderList = new List<Order>();
        Order childOrder1=new Order();
        childOrder1.AccountId=accObj.Id;
        childOrder1.ParentId__c=masterOrder.Id;
        childOrder1.CurrencyIsoCode='CAD';
        childOrder1.EffectiveDate=Date.today();
        childOrder1.Name='ChildOrder1';
        childOrder1.Status='Not Submitted';
        childOrder1.Pricebook2Id=pricebookId;
        childOrder1.Credit_Assessment__c=aCAR.id;
        childOrderList.add(childOrder1);
        
        Order childOrder2=new Order();
        childOrder2.AccountId=accObj.Id;
        childOrder2.ParentId__c=masterOrder.Id;
        childOrder2.CurrencyIsoCode='CAD';
        childOrder2.EffectiveDate=Date.today();
        childOrder2.Name='ChildOrder2';
        childOrder2.Status='Not Submitted';
        childOrder2.Pricebook2Id=pricebookId;
        childOrderList.add(childOrder2);
        
        insert childOrderList;
        
        List<OrderItem> orderItemList = new List<OrderItem>();
        OrderItem ordrItem0=new OrderItem();
        ordrItem0.OrderId=childOrder1.id;
        ordrItem0.PricebookEntryId=pbe.id;
        ordrItem0.Quantity=1;
        ordrItem0.UnitPrice=100.00;
        ordrItem0.vlocity_cmt__RecurringTotal__c=0.00;
        ordrItem0.vlocity_cmt__OneTimeTotal__c=100.00;
        ordrItem0.vlocity_cmt__LineNumber__c='0001';
        ordrItem0.vlocity_cmt__ProvisioningStatus__c='New';
        ordrItem0.orderMgmtId__c='123456780';
        ordrItem0.Scheduled_Due_Date__c = date.today();
        ordrItem0.vlocity_cmt__BillingAccountId__c=null;
        ordrItem0.Shipping_Address__c=null;
        orderItemList.add(ordrItem0 );

        OrderItem ordrItem1=new OrderItem();
        ordrItem1.OrderId=childOrder2.id;
        ordrItem1.PricebookEntryId=pbe.id;
        ordrItem1.Quantity=1;
        ordrItem1.UnitPrice=100.00;
        ordrItem1.vlocity_cmt__RecurringTotal__c=0.00;
        ordrItem1.vlocity_cmt__OneTimeTotal__c=100.00;
        ordrItem1.vlocity_cmt__LineNumber__c='0001.0001';
        ordrItem1.vlocity_cmt__ProvisioningStatus__c='New';
        ordrItem1.orderMgmtId__c='123456781';
        orderItemList.add(ordrItem1);
        
        OrderItem ordrItem2=new OrderItem();
        ordrItem2.OrderId=childOrder2.id;
        ordrItem2.PricebookEntryId=pbe.id;
        ordrItem2.Quantity=1;
        ordrItem2.UnitPrice=100.00;
        ordrItem2.vlocity_cmt__RecurringTotal__c=0.00;
        ordrItem2.vlocity_cmt__OneTimeTotal__c=100.00;
        ordrItem2.vlocity_cmt__LineNumber__c='0001.0001';
        ordrItem2.vlocity_cmt__ProvisioningStatus__c=null;
        ordrItem2.orderMgmtId__c='123456782';
        orderItemList.add(ordrItem2);
        
        insert orderItemList;
        orderItemId0 = orderItemList[0].Id;
        
        List<ID> childIds = new List<Id>();
        childIds.add(childOrder1.Id);
        childIds.add(childOrder2.Id);
        Map<Id,List<Id>> orderMap = new Map<Id,List<Id>>();
        orderMap.put(masterOrder.Id, childIds);
        return orderMap;
    }
    @isTest
    private static void validateOrder1() {
        Map<Id,List<Id>> orderMap = createData();
        Test.StartTest();
        OC_ValidateCustomerOrder.validateOrderOnSubmit(orderMap);
        Test.StopTest();
    }
    
    @isTest
    private static void validateOrder2() {
        Map<Id,List<Id>> orderMap = createData();
        List<Id> blankList;
        orderMap.put(masterOrderId, blankList);
        List<Order> orderList = [Select status from Order Where ParentId__c =: masterOrderId];
        orderList[1].status = 'Processing';
        orderList[1].orderMgmtId_Status__c = 'Processing';
        orderList[0].Type='Move';
        update orderList;
        productInst.vlocity_cmt__TrackAsAgreement__c=false;
        update productInst;
                
        Test.StartTest();
        OC_ValidateCustomerOrder.validateOrderOnSubmit(orderMap);
        Test.StopTest();
    }
    @isTest
    private static void validateOrder3() {
        Map<Id,List<Id>> orderMap = createData();
        
        OrderItem orderItemObj = [Select vlocity_cmt__RecurringTotal__c,vlocity_cmt__OneTimeTotal__c
                                    From OrderItem Where Id =: orderItemId0];
        orderItemObj.vlocity_cmt__OneTimeTotal__c=0.00;
        update orderItemObj;
        
        Test.StartTest();
        OC_ValidateCustomerOrder.validateOrderOnSubmit(orderMap);
        Test.StopTest();
    }
     @isTest
    private static void validateOrder4() {
        Map<Id,List<Id>> orderMap = createData();
        List<Order> ordList=[select id,type from order];
        for(Order ord:ordList){
            ord.type='Move';
        }
        update ordList;
        OrderItem orderItemObj = [Select vlocity_cmt__RecurringTotal__c,vlocity_cmt__OneTimeTotal__c
                                    From OrderItem Where Id =: orderItemId0];
        orderItemObj.vlocity_cmt__OneTimeTotal__c=0.00;
        update orderItemObj;
        
        Test.StartTest();
        OC_ValidateCustomerOrder.validateOrderOnSubmit(orderMap);
        Test.StopTest();
    }
     @isTest
    private static void validateOrder5() {
        Map<Id,List<Id>> orderMap = createData();
       
        OrderItem orderItemObj = [Select id,vlocity_cmt__ProvisioningStatus__c,amendStatus__c,Credit_Assessment_Required__c,vlocity_cmt__RecurringTotal__c,vlocity_cmt__OneTimeTotal__c
                                    From OrderItem Where Id =: orderItemId0];
        orderItemObj.vlocity_cmt__OneTimeTotal__c=0.00;
        orderItemObj.amendStatus__c='Add';
        orderItemObj.Credit_Assessment_Required__c='Yes';
        orderItemObj.vlocity_cmt__RecurringTotal__c=2.0;
        update orderItemObj;
        
        Test.StartTest();
        OC_ValidateCustomerOrder.validateOrderOnSubmit(orderMap);
        Test.StopTest();
    }
     @isTest
    private static void updateOrderCarNotRequiredTest() {
        
         String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        
        Test.StartTest();
        List<Order> ordList=[select id from order];
        OC_ValidateCustomerOrder ob=new OC_ValidateCustomerOrder();
        OC_ValidateCustomerOrder.updateOrderCarNotRequired(ordList);
        Test.StopTest();
    }
}