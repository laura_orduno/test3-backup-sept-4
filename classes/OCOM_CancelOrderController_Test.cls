@istest(seealldata=false)
private class OCOM_CancelOrderController_Test {
    private static void createData(){
        System.runAs(new User(Id = Userinfo.getUserId())) {            
            SMBCare_WebServices__c cs = new SMBCare_WebServices__c(Name='EmailAddress',Value__c='test@test.com'); 
            insert cs;            
            //Custom Setting for username Field
            SMBCare_WebServices__c cs1 = new SMBCare_WebServices__c(Name='username',Value__c ='APP_SFDC'); 
            insert cs1;
            //Custom Setting for password Field
            SMBCare_WebServices__c cs2 = new SMBCare_WebServices__c(Name='password',Value__c='soaorgid'); 
            insert cs2; 
            OrdrTestDataFactory.createOrderingWSCustomSettings('CancelOrderEndpoint', 'https://webservice1.preprd.teluslabs.net/CMO/OrderMgmt/CancelCustomerOrder_v2_1_vs1');
         }
    }
   
  static testMethod void testMethod1() {
            createData();
    
           Schema.DescribeSObjectResult d = Schema.SObjectType.Account; 
          Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
          Schema.RecordTypeInfo rtByName =  rtMapByName.get('CAN');
          system.debug(' id'+rtByName.getRecordTypeId());
          Id recTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Billing').getRecordTypeId();
          Id recSerTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
          Id recRCIDTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId();
        
          Test.startTest();
          Account RCIDacc = new Account(name='debs',recordtypeid=recRCIDTypeId);
          Account acc = new Account(name='Billing account',BAN_CAN__c='draft',recordtypeid=recTypeId);
          Account seracc = new Account(name='123 street',recordtypeid=recSerTypeId,parentid=RCIDacc.id);
          
          
          List<Account> acclist= new List<Account>();          
          acclist.add(RCIDacc);
          acclist.add(acc);
          acclist.add(seracc );
          insert acclist;
          
          smbcare_address__c address= new smbcare_address__c(account__c=seracc.id); 
         
          
           // First, set up test price book entries.
        // Insert a test product.
        Product2 prod = new Product2(Name = 'Laptop X200', 
            Family = 'Hardware');
        insert prod;
        
        // Get standard price book ID.
        // This is available irrespective of the state of SeeAllData.
        Id pricebookId = Test.getStandardPricebookId();
        
        // 1. Insert a price book entry for the standard price book.
        // Standard price book entries require the standard price book ID we got earlier.
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        // Create a custom price book
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        // 2. Insert a price book entry with a custom price.
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        contact c= new contact(firstname='abc',lastname='xyz');
        insert c;
        List< Credit_Assessment__c> CARList= new List<Credit_Assessment__c>();
          Credit_Assessment__c CAR= new Credit_Assessment__c(CAR_Status__c='test',CAR_Result__c='Denied');CARList.add(CAR);
          Credit_Assessment__c CAR2= new Credit_Assessment__c(CAR_Status__c='test',CAR_Result__c='Denied');CARList.add(CAR2);
         insert CARList;
         order ord = new order(Credit_Assessment__c=car.id,Ban__c='draft',Reason_Code__c='34aas',General_Notes_Remarks__c='123123',accountId=seracc.id,effectivedate=system.today(),status='Draft',Pricebook2Id=customPB.id,CustomerAuthorizedById=c.id,service_address__c=address.id);   
         order ord2 = new order(Credit_Assessment__c=car2.id,Ban__c='draft',Reason_Code__c='34aas',General_Notes_Remarks__c='123',accountId=acc.id,effectivedate=system.today(),status='Draft',Pricebook2Id=customPB.id,CustomerAuthorizedById=c.id,service_address__c=address.id);   
         List<Order>ordlist= new List<Order>();   
         ordList.add(ord);ordList.add(ord2);
          insert ordList; 


          
          orderItem  OLI2= new orderItem(UnitPrice=12,orderId= ord2.Id,vlocity_cmt__LineNumber__c='1',orderMgmt_BPI_Id__c='9871379163517',vlocity_cmt__BillingAccountId__c=acc.id,vlocity_cmt__ServiceAccountId__c=seracc.id,Quantity=2,PricebookEntryId=customPrice.id);
          orderItem  OLI= new orderItem(UnitPrice=12,orderId= ord.Id,vlocity_cmt__LineNumber__c='1',vlocity_cmt__AssetReferenceId__c=OLI2.id,vlocity_cmt__BillingAccountId__c=acc.id,vlocity_cmt__ServiceAccountId__c=seracc.id,Quantity=2,PricebookEntryId=customPrice.id);
          
          List<OrderItem>OLIlist= new List<OrderItem>();   
          OLIList.add(OLI2);OLIList.add(OLI);
          insert OLIList;
          asset ast= new asset(name='test',accountID=RCIDacc.id,vlocity_cmt__OrderId__c=ord2.id,status='Shipped');
          insert ast;
          ord.status = 'Not Submitted';
          update ord;
          ord2.Status = 'Submitted';
          update ord2;
          Test.stopTest();
    
        PageReference pageref = Page.OCOM_CancelOrder;
        Test.setCurrentPage(pageref);        
        pageref.getparameters().put('id',ord.Id);
        ApexPages.StandardController sc_1 = new ApexPages.standardController(ord);
        OCOM_CancelOrderController corder= new OCOM_CancelOrderController(sc_1);
        corder.isParentOrder = false;
        corder.DoOK();
        corder.invalidForm();
        corder.CancelButton();
        OCOM_CancelOrderController.vfrPrepareOrderCancellationRequest(ord.Id,'ewqe','wqe');     
        PageReference pageref1 = Page.OCOM_CancelOrder;
        pageref1.getparameters().put('id',ord2.Id);
        ApexPages.StandardController sc_2 = new ApexPages.standardController(ord2);
        OCOM_CancelOrderController corder1= new OCOM_CancelOrderController(sc_2);
        corder1.previousOrderStatus='Delivery';
        OCOM_CancelOrderController.vfrPrepareOrderCancellationRequest(ord2.Id,'ewqe','wqe');  
        corder1.previousOrderStatus='Activated';
        corder1.isParentOrder = true;
        corder1.DoOK();
        corder1.CancelButton();
        corder1.mockTestVariable=1;
        corder1.DoOK();
        corder1.mockTestVariable=3;
        corder1.DoOK();
		corder1.previousOrderStatus  = 'Not Submitted';
       corder1.cancelOrder();
    }  

   @isTest static void test_CancelContractsWithOutEnvelope() {

    Map<String,Object> TestinputMap = new Map<String,Object>();
    Map<String,Object> TestoutMap = new Map<String,Object>();
    TestDataHelper.testContractCreation1();
    String orderId= OrdrTestDataFactory.singleMethodForOrderId();
        Order ordObj=[select Account.Credit_Profile__c,AccountId, Credit_Check_Not_Required__c,RequestedDate__C,status,Rush__c,Ban__c,Shipping_Address__c,orderMgmtId__c FROM Order where id=:orderId];
        ordObj.orderMgmtId__c='OS1234567890';
        ordObj.status='Not Submitted';
        ordObj.Rush__c=true;
        ordObj.Ban__c='12345';
        ordObj.Shipping_Address__c='test address';
        ordObj.Credit_Check_Not_Required__c=false;
        Date day1 = Date.today();
        Date todayPlus = day1.addDays(4);
        ordObj.RequestedDate__C=todayPlus;
        update ordObj;
        insertFieldMapper();

        TestDataHelper.testContractObj.vlocity_cmt__OrderId__c = ordObj.id;
        TestDataHelper.testContractObj.Status='Contract Sent';
        update TestDataHelper.testContractObj;
      
        TestinputMap.put('orderId',ordObj.id);
        //TestinputMap.put('isSuccess',true);
        TestinputMap.put('creditStatus','Approved');
        TestinputMap.put('areContractable',True);
        TestinputMap.put('isMoveOutOrder',false);
      
        Test.startTest();
        OCOM_ContractUtil.createNewContract(TestinputMap,TestoutMap);
        Boolean isSuccess = (Boolean)TestoutMap.get('isSuccess');
        String contractID = (String)TestoutMap.get('contractID');
        String contractStatus = (String)TestoutMap.get('contractStatus');
        String contractNumber = (String)TestoutMap.get('contractNumber');
        System.assertEquals(true, isSuccess);
        System.assertNotEquals(null,contractID);
        System.assertNotEquals(null,contractNumber);

        OCOM_ContractUtil.SetValueFromTestMethod = 'SUCCESS'; 

        TestDataHelper.testContractObj.Status='Draft';
        TestDataHelper.testContractObj.No_Amend__c= false;
            update TestDataHelper.testContractObj;


       ApexPages.StandardController stdController = new ApexPages.standardController(ordObj);
       OCOM_CancelOrderController cancelOrderObj = new  OCOM_CancelOrderController(stdController);

        String response = OCOM_CancelOrderController.cancelEnvelope(ordObj.id);
        //OCOM_ContractUtil.contractableOrderCheck(TestinputMap,TestoutMap);
       // Map<String,Object> output = OCOM_ContractUtil.AmendOrder(ordObj,'Cancel');
        System.assertEquals('SUCCESS',response);
       webservice_integration_error_log__c log = new webservice_integration_error_log__c();
       
       OCOM_CancelOrderController.isCancellable(ordObj.ID);
       cancelOrderObj.cancelNCOrder(ordObj.ID, log);
       
      Test.stopTest();
  }

  public static void insertFieldMapper(){

          String nameSpaceprefix = 'vlocity_cmt__';
     Vlocity_cmt__CustomFieldMap__c customFieldMap1 = new Vlocity_cmt__CustomFieldMap__c( Name='OrderItem>ContractLineItem__c0',
                                  Vlocity_cmt__SourceFieldName__c = 'ListPrice',
                                  Vlocity_cmt__DestinationFieldName__c =nameSpaceprefix+'UnitPrice__c',
                                  Vlocity_cmt__DestinationSObjectType__c=nameSpaceprefix+'ContractLineItem__c',
                                  vlocity_cmt__SourceFieldType__c='currency',                                 
                                  Vlocity_cmt__SourceSObjectType__c='OrderItem');
         Vlocity_cmt__CustomFieldMap__c customFieldMap2 = new Vlocity_cmt__CustomFieldMap__c( Name='OrderItem>ContractLineItem__c1',
                                  Vlocity_cmt__SourceFieldName__c = 'Quantity',
                                  Vlocity_cmt__DestinationFieldName__c =nameSpaceprefix+'Quantity__c',
                                  Vlocity_cmt__DestinationSObjectType__c=nameSpaceprefix+'ContractLineItem__c',
                                  Vlocity_cmt__SourceFieldType__c='double',
                                  Vlocity_cmt__SourceSObjectType__c='OrderItem');
     Vlocity_cmt__CustomFieldMap__c customFieldMap3 = new Vlocity_cmt__CustomFieldMap__c( Name='OrderItem>ContractLineItem__c2',
                                  Vlocity_cmt__SourceFieldName__c = 'vlocity_cmt__LineNumber__c',
                                  Vlocity_cmt__DestinationFieldName__c = 'vlocity_cmt__LineNumber__c',
                                  Vlocity_cmt__SourceFieldType__c='string',
                                  Vlocity_cmt__DestinationSObjectType__c=nameSpaceprefix+'ContractLineItem__c',
                                  Vlocity_cmt__SourceSObjectType__c='OrderItem');
     Vlocity_cmt__CustomFieldMap__c customFieldMap4 = new Vlocity_cmt__CustomFieldMap__c( Name='OrderItem>ContractLineItem__c3',
                                  Vlocity_cmt__SourceFieldName__c = 'PricebookEntryId',
                                  Vlocity_cmt__DestinationFieldName__c =nameSpaceprefix+'PricebookEntryId__c',
                                  Vlocity_cmt__SourceFieldSObjectType__c='PricebookEntry',
                                  Vlocity_cmt__SourceFieldType__c='reference',
                                  Vlocity_cmt__DestinationSObjectType__c=nameSpaceprefix+'ContractLineItem__c',
                                  Vlocity_cmt__SourceSObjectType__c='OrderItem'); 
      Vlocity_cmt__CustomFieldMap__c customFieldMap5 = new Vlocity_cmt__CustomFieldMap__c(Name='OrderItem>ContractLineItem__c4',
                                  Vlocity_cmt__SourceFieldName__c = 'vlocity_cmt__RecurringTotal__c',
                                  Vlocity_cmt__DestinationFieldName__c ='vlocity_cmt__RecurringTotal__c',
                                  
                                  Vlocity_cmt__SourceFieldType__c='currency',
                                  Vlocity_cmt__DestinationSObjectType__c=nameSpaceprefix+'ContractLineItem__c',
                                  Vlocity_cmt__SourceSObjectType__c='OrderItem'); 
     Vlocity_cmt__CustomFieldMap__c customFieldMap6 = new Vlocity_cmt__CustomFieldMap__c(Name='Order>Contract0',
                                  Vlocity_cmt__SourceFieldName__c = 'AccountId',
                                  Vlocity_cmt__DestinationFieldName__c ='AccountId',
                                  Vlocity_cmt__DestinationSObjectType__c='Contract',
                                  Vlocity_cmt__SourceFieldType__c='reference',
                                  Vlocity_cmt__SourceSObjectType__c='Order');
     Vlocity_cmt__CustomFieldMap__c customFieldMap7 = new Vlocity_cmt__CustomFieldMap__c(Name='Order>Contract1',
                                  Vlocity_cmt__SourceFieldName__c = 'Id',
                                  Vlocity_cmt__DestinationFieldName__c =nameSpaceprefix+'OrderId__c',
                                  Vlocity_cmt__DestinationSObjectType__c='Contract',
                                  Vlocity_cmt__SourceFieldType__c='reference',
                                    Vlocity_cmt__SourceSObjectType__c='Order');
    List<Vlocity_cmt__CustomFieldMap__c> listCustomFieldMaps = new List<Vlocity_cmt__CustomFieldMap__c> ();
    listCustomFieldMaps.add(customFieldMap1);
    listCustomFieldMaps.add(customFieldMap2);
    listCustomFieldMaps.add(customFieldMap3);
    listCustomFieldMaps.add(customFieldMap4);
    listCustomFieldMaps.add(customFieldMap5);
    listCustomFieldMaps.add(customFieldMap6);
    listCustomFieldMaps.add(customFieldMap7);
    insert listCustomFieldMaps;
  }
    @isTest 
private static void flagTimeoutWithOrderObjTest(){
  String orderId= OrdrTestDataFactory.singleMethodForOrderId();
        Order ordObj=[select orderMgmtId_Status__c from order where id=:orderId];
       ApexPages.StandardController stdController = new ApexPages.standardController(ordObj);
       OCOM_CancelOrderController cntrl = new  OCOM_CancelOrderController(stdController);
        cntrl.flagTimeoutWithOrderObj(ordObj);      
}
}