/**
 * This is the OCOM implementation for 'PreConfigureHandler'..
 * CONFIGURE:-
 * It shows how to access the attribute runtime info in a list of vlocity_cmt.ProductWrapper and hide/disable it.
 * It also shows how to remove a line item while configuring a product using this hook
 * RECONFIGURE:-
 * While reconfiguring the changes made in configuring can be reverted...
 */
global with sharing class VlProductComponentsFeasibilityImpl implements vlocity_cmt.VlocityOpenInterface {
    global Boolean invokeMethod(String methodName, Map<String, Object> input, Map<String, Object> output, Map<String, Object> options) {
        if (methodName.equals('configureProduct')){
            System.debug('in configure::::::::');
            return configureProduct(input, output, options);
            
        }
        else if (methodName.equals('reConfigureProduct')) {
            System.debug('in reconfigure::::::::');
            return reConfigureProduct(input, output, options);
        }
        return false;
    }

    private Boolean configureProduct(Map<String, Object> input, Map<String, Object> output, Map<String, Object> options) {
        List<vlocity_cmt.ProductWrapper> productDefinitions = (List<vlocity_cmt.ProductWrapper>)input.get('productDefinition');
        try{
            Id objectId = (Id) input.get('objectId');
            //Map<String, OrdrProductEligibilityManager.Offer> availableOffers = OrdrProductEligibilityManager.getAvailableOffers(objectId);
            Set<Id> childProductIds = new Set<Id>(); 
            for (vlocity_cmt.ProductWrapper product : productDefinitions) {
                System.debug('productName=' + product.name + ' minQuantity=' + product.minQuantity);
                if(product.productId != null){
                    childProductIds.add(product.productId);

                }

                //calibrateProductConfiguration(product, availableOffers);
            }
            
            System.debug('childProductIds::' + childProductIds);
            Set <Id> excludedChildProducts  = new Set <Id>();

            for (vlocity_cmt__ProductChildItem__c pci :[Select OCOMOverrideType__c,vlocity_cmt__ChildProductId__c FROM vlocity_cmt__ProductChildItem__c where vlocity_cmt__ChildProductId__c in :childProductIds AND OCOMOverrideType__c = 'Override Child']){
                if(pci.vlocity_cmt__ChildProductId__c != null)
                    excludedChildProducts.add(pci.vlocity_cmt__ChildProductId__c);

            }
            System.debug('excludedChildProducts::' + excludedChildProducts);
            for (vlocity_cmt.ProductWrapper product : productDefinitions) {
                if(excludedChildProducts.contains(product.productId)){
                    product.minQuantity = 0;
                    product.maxQuantity = 1;
                    product.JSONAttribute = null;
                    System.debug('here::' + product);
                }

            }
            
            output.put('productDefinition', productDefinitions);
            return true;

        } catch (Exception e) {
            String msg = e.getMessage();
            msg += e.getStackTraceString().startsWith('()') ? '' : ': ' + e.getStackTraceString();
            throw new OrdrExceptions.ProductEligibilityException(msg);
        }
    }
        
    
    private Boolean reConfigureProduct(Map<String, Object> input, Map<String, Object> output, Map<String, Object> options){
        return configureProduct(input, output, options);
    }
    
    /*
    private void calibrateProductConfiguration(vlocity_cmt.ProductWrapper product, Map<String, OrdrProductEligibilityManager.Offer> offers) {
        
        if (offers != null && !offers.isEmpty()) {
            if (offers.containsKey(product.name)) {
                
                OrdrProductEligibilityManager.Offer offer = offers.get(product.name);
                if (offer.availability != 'available' && offer.availability != 'limited' && offer.availability != 'unknown') {
                    if (product.minQuantity == 0) {
                        product.maxQuantity = 0; // this will set the Offer to be not eligible to be added to the cart;
                    }
                    else {
                        System.debug(LoggingLevel.ERROR, 'Mandatory child offer is not eligible.');
                    }
                    
                }
                
                Map<String, List<OrdrProductEligibilityManager.Value>> characteristics = offer.characteristics;
                Map<String, Object> attributes = (Map<String, Object>) JSON.deserializeUntyped(product.JSONAttribute);
                
                for (String key : attributes.keySet()) {
                    
                    for (Object attribute : (List<Object>) attributes.get(key)) {
                        Map<String, Object> attributeMap = (Map<String, Object>) attribute;
                        String attributeName = (String) attributeMap.get('attributedisplayname__c');
                        
                        System.debug('attributeName=' + attributeName);
                        
                        if (characteristics.containsKey(attributeName)) {
                            for (OrdrProductEligibilityManager.Value characteristicValue : characteristics.get(attributeName)) {
                                if (characteristicValue.availability != 'available' && characteristicValue.availability != 'limited' && characteristicValue.availability != 'unknown') {
                                    
                                    Map<String, Object> attributeRunTimeInfo = (Map<String, Object>) attributeMap.get('attributeRunTimeInfo');
                                    
                                    for(Object values : (List<Object>) attributeRunTimeInfo.get('values')){
                                        Map<String, Object> attributeValues = (Map<String, Object> ) values;
                                        if ((String) attributeValues.get('displayText') == characteristicValue.label) {
                                            attributeValues.put('isreadonly__c', true);
                                        }  
                                    }
                                }
                            }
                        }
                    }
                }
                product.JSONAttribute = JSON.serialize(attributes);
            }
            else {
                for (String key : offers.keySet()) {
                    calibrateProductConfiguration(product, offers.get(key).childOffers);
                }
            }
        }
        
    }
	*/
}