public class RestApi_MyAccountHelper_Utility {

	public static final String DEFAULT_ORIGIN = 'My Account';
	public static final Id RT_RCID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId();
	public static final Id EMAIL2CASE_PROFILE = [SELECT Metadata_Value__c FROM Apex_Org_Default__mdt WHERE DeveloperName = 'System_Admin_Profile_token_access' LIMIT 1].Metadata_Value__c;
	public static final Id INTEGRATION_PROFILE = [SELECT Metadata_Value__c FROM Apex_Org_Default__mdt WHERE DeveloperName = 'System_Admin_Profile_token_access' LIMIT 1].Metadata_Value__c;
	public static final Id COMMUNITY_PROFILE_ID = [SELECT Metadata_Value__c FROM Apex_Org_Default__mdt WHERE DeveloperName = 'Customer_Community_Profile' LIMIT 1].Metadata_Value__c;
	public static final String CUSTOMER_PORTAL_NAME = 'My Account';

	public Static User getUser(String uuid) {
		try {
			// check if null
			if (uuid == null) {
				return null;
			}

			return [
					SELECT Id,
							Name,
							ContactId,
							Contact.AccountId,
							Contact.Name,
							FederationIdentifier,
							Username,
							Contact.MASR_Enabled__c
					FROM User
					WHERE FederationIdentifier = :uuid
					LIMIT 1
			];
		} catch (QueryException e) {
			return null;
		}
	}

	public static Contact getContact(Id contactId) {
		try {
			return [
					SELECT Id,
							AccountId,
							Account.Sub_Segment_Code__c,
							Account.Sub_Segment_Code__r.Cust_Segment_A__c,
							Account.Sub_Segment_Code__r.Team_10Characters__c,
							Email,
							Name,
							FirstName,
							LastName,
							MASR_Enabled__c
					FROM Contact
					WHERE Id = :contactId
			];
		} catch (QueryException e) {
			return null;
		}
	}

	public static Case getCaseDetails(Id caseId) {
		try {

			return [
					SELECT Id,
							AccountId,
							ContactId,
							Contact.Name,
							Contact.MASR_Enabled__c,
							Related_BAN_Account__c,
							Related_BCAN_Account__c,
							Status,
							Subject,
							Description,
							NotifyCustomer__c,
							My_Business_Requests_Type__c,
							NotifyCollaboratorString__c,
							RecordTypeId,
							CreatedDate,
							CaseNumber,
							LastModifiedDate,
							Account__c,
							Origin,
							MobilityRequest__c
					FROM Case
					WHERE Id = :caseId
			];

		} catch (Exception e) {
			throw new xException('Case: ' + e.getMessage());
		}
	}

	public static Case getCaseDetailsForUser(Id caseId, Id contactId, List<String> accountIds) {
		try {
			return [
					SELECT Id,
							AccountId,
							ContactId,
							Contact.Name,
							Related_BAN_Account__c,
							Related_BCAN_Account__c,
							Related_BAN_Account__r.BAN_CAN__c,
							Related_BCAN_Account__r.BAN_CAN__c,
							Related_BAN_Account__r.Billing_System_ID__c,
							Related_BCAN_Account__r.Billing_System_ID__c,
							Related_BAN_Account__r.CAN__c,
							Related_BCAN_Account__r.CAN__c,
							Status,
							Subject,
							Description,
							NotifyCustomer__c,
							My_Business_Requests_Type__c,
							NotifyCollaboratorString__c,
							RecordTypeId,
							CreatedDate,
							CaseNumber,
							LastModifiedDate,
							Account__c,
							Origin,
							MobilityRequest__c,
							Owner.type,
							OwnerId,
							CreatedById
					FROM Case
					WHERE Id = :caseId
					AND (
							ContactId = :contactId
							OR
							Related_BAN_Account__r.BAN_CAN__c in :accountIds
							OR
							Related_BCAN_Account__r.BAN_CAN__c in :accountIds
					)
					AND Origin = :DEFAULT_ORIGIN
					LIMIT 1
			];

		} catch (QueryException e) {
			return null;
		}
	}

	public static List<Case> getCasesForUserWithFilters(Id contactId, List<String> accountIds, List<String> filters) {
		try {

			List<Case> cases = [
					SELECT Id,
							AccountId,
							ContactId,
							Contact.Name,
							Related_BAN_Account__c,
							Related_BCAN_Account__c,
							Related_BAN_Account__r.BAN_CAN__c,
							Related_BCAN_Account__r.BAN_CAN__c,
							Related_BAN_Account__r.Billing_System_ID__c,
							Related_BCAN_Account__r.Billing_System_ID__c,
							Related_BAN_Account__r.CAN__c,
							Related_BCAN_Account__r.CAN__c,
							Status,
							Subject,
							Description,
							NotifyCustomer__c,
							My_Business_Requests_Type__c,
							NotifyCollaboratorString__c,
							RecordTypeId,
							CreatedDate,
							CaseNumber,
							LastModifiedDate,
							Account__c,
							Origin,
							MobilityRequest__c
					FROM Case
					WHERE (
							ContactId = :contactId
							OR
							Related_BAN_Account__r.BAN_CAN__c in :accountIds
							OR
							Related_BCAN_Account__r.BAN_CAN__c in :accountIds
					)
					AND Origin = :DEFAULT_ORIGIN
					AND Status IN :filters
			];

			return cases;

		} catch (Exception e) {
			throw new xException('Case: ' + e.getMessage());
		}
	}

	// update user fields
	@Future
	public static void updateUser(Id uId, Map<String, String> user) {
		User u = [SELECT Id, firstName, lastName, email FROM User WhERE id = :uId];

		if (user.get('email') != null && !PortalUtils.validateEmail(user.get('email'))) {
			throw new xException('Invalid email address.');
		}

		// update user fields if specified in the user section of the request
		u.firstName = user.get('firstName') != null ? user.get('firstName') : u.firstName;
		u.lastName = user.get('lastName') != null ? user.get('lastName') : u.lastName;
		u.email = user.get('email') != null ? user.get('email') : u.email;

		try {
			update u;
		} catch (DMLException e) {
			throw new xException('Update User : ' + e.getMessage());
		} catch (Exception e) {
			throw new xException(e.getMessage());
		}
	}

	@Future
	public static void createCommunityUser(String uuid, Map<String, String> c) {
		if (c.get('email') == null || String.isBlank(c.get('email'))) {
			throw new xException('Email needs to be provided.');
		}

		try {
			String communityUsername = c.get('id') + '@' + URL.getCurrentRequestUrl().getAuthority();
			User u = new User(Alias = 'MyAccUsr', Email = c.get('email'), ContactId = c.get('id'), Customer_Portal_Name__c = CUSTOMER_PORTAL_NAME,
					EmailEncodingKey = 'UTF-8', FirstName = c.get('firstName'), LastName = c.get('lastName'), LanguageLocaleKey = 'en_US',
					LocaleSidKey = 'en_US', CommunityNickname = c.get('id'), ProfileId = COMMUNITY_PROFILE_ID, TimeZoneSidKey = 'America/New_York',
					UserName = communityUsername, FederationIdentifier = uuid, uas__Sync_to_Contact__c = false);

			insert u;
		} catch (DMLException e) {
			throw new xException('Create User: ' + e.getMessage());
		} catch (Exception e) {
			throw new xException('Create User: ' + e.getMessage());
		}
	}

	public static Attachment getAttachmentDetails(Id aid) {
		try {
			Attachment a = [
					SELECT ParentId, Name, Body, CreatedDate, Id
					FROM Attachment
					WHERE Id = :aid
			];

			return a;
		} catch (Exception e) {
			throw new xException('Get Attachment: ' + e.getMessage());
		}
	}

	public static Boolean caseVisibleToUser(Case theCase, User theUser) {
		return theCase.AccountId == theUser.Contact.AccountId;
	}

	public static String createCollaborators(List<String> collabs) {
		String collaborators = '';
		if (collabs != null) {
			collaborators = String.join(collabs, ';');
		}

		return collaborators;
	}

	public static List<Case> convertListStatusToExternal(List<Case> cases, List<String> internalStatuses) {

		try {
			Map<String, String> internalToExternal = new Map<string, string>();
			List<ExternalToInternal__c> results = [SELECT Internal__c, External__c FROM ExternalToInternal__c WHERE Internal__c IN :internalStatuses];
			for (ExternalToInternal__c result : results) {
				internalToExternal.put(result.Internal__c, result.External__c);
			}

			for (Case c : cases) {
				c.Status = internalToExternal.get(c.Status);
			}

			return cases;
		}

		catch (Exception e) {
			throw new xException('Status Conversion: ' + e.getMessage());
		}
	}

	public static List<String> convertListStatusToInternal(List<String> externalStatuses) {
		try {
			List<ExternalToInternal__c> externals = [SELECT Internal__c FROM ExternalToInternal__c WHERE External__c IN :externalStatuses];
			List<String> internalStatuses = new List<String>();
			for (ExternalToInternal__c e : externals) {
				internalStatuses.add(e.Internal__c);
			}

			return internalStatuses;
		}

		catch (Exception e) {
			throw new xException('Status Conversion: ' + e.getMessage());
		}
	}

	public static String translateStatus(String status) {
		try {
			ExternalToInternal__c statusTranslation = [SELECT External__c FROM ExternalToInternal__c WHERE Internal__c = :status LIMIT 1];
			return statusTranslation.External__c;
		} catch (QueryException e) {
			throw new xException('Request Type Invalid: ' + e.getMessage());
		}
	}

}