@isTest
private class QuoteGroupControllerTests {
    private static Web_Account__c account;
    private static SBQQ__Quote__c quote;
    private static SBQQ__Quote__c quotetwo;
    private static Quote_Group__c qgroup;
    
    
    testMethod static void testIsSendAgreementDisabled() {
        setUp();
        
        ApexPages.currentPage().getParameters().put('gid', qgroup.Id);
        QuoteGroupController target = new QuoteGroupController();
        
        System.assertEquals(true, target.isSendAgreementDisabled());
    }
    
    testMethod static void testOnSendAgreement() {
        setUp();
        
        ApexPages.currentPage().getParameters().put('gid', qgroup.Id);
        QuoteGroupController target = new QuoteGroupController();
        
        PageReference pref = target.onSendAgreement();
        System.assert(pref != null);
    }
    
    testMethod static void testOnCheckCredit() {
        setUp();
        
        ApexPages.currentPage().getParameters().put('gid', qgroup.Id);
        QuoteGroupController target = new QuoteGroupController();
        target.getBackURL();
        
        System.assertEquals(true, target.isCreditCheckDisabled());
        
        PageReference pref = target.onCheckCredit();
        System.assert(pref != null);
    }
    
    private static void setUp() {
        Account sa = new Account(Name = 'TestUAC');
        insert sa;
        
        account = new Web_Account__c(Name='UnitTest');
        insert account;
        
        Contact c = new Contact(FirstName = 'UAC', LastName = 'LAC', AccountId = sa.Id, Web_Account__c = account.id);
        insert c;
        
        Opportunity opp = new Opportunity(Name='Test',Web_Account__c=account.Id,CloseDate=System.today(),StageName='Prospecting');
        insert opp;
        
        qgroup = new Quote_Group__c();
        qgroup.Customer_Contact__c = c.Id;
        insert qgroup;
        
        quote = new SBQQ__Quote__c(SBQQ__Opportunity__c=opp.Id);
        quote.Rate_Band__c = 'F';
        quote.Province__c = 'BC';
        quote.Group__c = qgroup.Id;
        quote.Completed__c = true;
        insert quote;
        
        SBQQ__QuoteLine__c line = new SBQQ__QuoteLine__c(SBQQ__Quote__c=quote.Id,Category__c='devices');
        insert line;
        
        quotetwo = new SBQQ__Quote__c(SBQQ__Opportunity__c=opp.Id);
        quotetwo.Rate_Band__c = 'F';
        quotetwo.Province__c = 'BC';
        quotetwo.Group__c = qgroup.Id;
        quotetwo.Completed__c = true;
        insert quotetwo;
        
        SBQQ__QuoteLine__c linetwo = new SBQQ__QuoteLine__c(SBQQ__Quote__c=quotetwo.Id,Category__c='devices');
        insert linetwo;
    }
}