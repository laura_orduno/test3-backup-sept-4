@isTest
public with sharing class MBRTestUtils {

    public static final String testUserPassword = 'Traction123..';

	public static List<Case> createCCICases(Integer numToInsert, Id accountId) {
        List<Case> cases = new List<Case>();
        for( Integer i = 0; i < numToInsert; i++ ) {
            Case theCase = new Case();
            theCase.Account__c = accountId;
            theCase.Subject = 'TEST SUBJECT';
            theCase.Description = 'TEST';
            theCase.Status = 'New';
            theCase.Origin = Label.MBRCCI_Self_Serve;
            theCase.Request_Type__c = 'Other';
            theCase.Priority = 'Medium';
            cases.add( theCase ); 
        }
        insert cases;
        System.debug('2014-11-15 MBRTestUtils.createCCICases(): inserted cases: ' + cases);
        return cases;
    }

	public static List<Case> createVITILcareCases(Integer numToInsert, Id accountId) {
/*        
        Contact theContact = new Contact(Email = 'test@unit-test.com', FirstName = 'Test', LastName = 'Test', AccountId = accountId);
        try{
        	insert theContact;
        }
        catch(DmlException e){
        	throw(e);    
        }   
*/        
        List<Case> cases = new List<Case>();        
        for( Integer i = 0; i < numToInsert; i++ ) {
            
            Case theCase = new Case();
            theCase.Account__c = accountId;
            theCase.Subject = 'TEST SUBJECT';
            theCase.Description = 'TEST';
            theCase.Status = 'New';
            theCase.Origin = 'VITILcare';
            theCase.Request_Type__c = 'Other';
            theCase.Priority = 'Medium';
//            theCase.contact = theContact;
            cases.add( theCase ); 
        }
        insert cases;
        System.debug('2014-11-15 MBRTestUtils.createVITILcareCases(): inserted cases: ' + cases);
        return cases;
    }
    public static List<Case> createENTPCases(Integer numToInsert, Id accountId) {

        List<Case> cases = new List<Case>();        
        for( Integer i = 0; i < numToInsert; i++ ) {
            
            Case theCase = new Case();
            theCase.Account__c = accountId;
            theCase.Subject = 'TEST SUBJECT';
            theCase.Description = 'TEST';
            theCase.Status = 'New';
            theCase.Origin = 'ENTP';
            theCase.Request_Type__c = 'Other';
            theCase.Priority = 'Medium';
            theCase.Lynx_Ticket_Number__c = '1111111111';
//            theCase.contact = theContact;
            cases.add( theCase ); 
        }
        insert cases;
        System.debug('2014-11-15 MBRTestUtils.createENTPCases(): inserted cases: ' + cases);
        return cases;
    }
    public static EmailMessage createEmail(Id pId, Boolean incom, String emailBody){
        EmailMessage em = new EmailMessage(ParentId = pId,
                                Incoming = incom,
                                TextBody = emailBody + '\n write above this line [ref:_001]',
                                Subject = '[ref:_001]',
                                FromAddress = 'rzhenissova@tractionondemand.com',
                                ToAddress = 'mybusinessrequest@telus.com'
                                );
        System.debug('2014-11-15 MBRTestUtils.createEmail(): returning EmailMessage em: ' + em);
        return em;
    }

    public static List<Attachment> createAttachment(Id pId, Integer numToInsert, String ext){
        List<Attachment> attList = new List<Attachment>{};
        for(Integer i=0; i<numToInsert; i++){
            Attachment att = new Attachment(parentId = pId,
                                            Name = 'test attachment.'+ ext,
                                           Body = Blob.valueOf('Unit Test Attachment Body ' + i));
            attList.add(att);
        }
        insert attList;
        return attList;
    }
	
    public static void createComment(Id caseId){
        CaseComment comm = new CaseComment(
                                ParentId = caseId,
                                CommentBody = 'test comment');
        insert comm;
    }

    public static User createGuestUser() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Guest License User']; 
        User u = new User(Alias = 'guest', Email='guest@unit-test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='guest@unit-test.com');

        insert u;
        return u;
    }

    public static User createPortalUser(String portalName) {
        Account a = new Account(Name = 'Unit Test Account');
        if(portalName == 'ENTP'){
            a.strategic__c = true;
        }
        insert a;

        Contact c = new Contact(Email = 'test@unit-test.com', LastName = 'Test', AccountId = a.Id);
        insert c;

        Profile p = [SELECT Id FROM Profile WHERE UserType='CSPLitePortal' LIMIT 1]; 
        User u = new User(Alias = 'TestUser', Email='test-user@unit-test.com', ContactId=c.Id, 
                            EmailEncodingKey='UTF-8', FirstName='Unit', LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', CommunityNickname='UNTST', ProfileId=p.Id, TimeZoneSidKey='America/Los_Angeles', 
                            UserName='test-user@unit-test.com');

        if(portalName == 'ENTP'){
            u.Customer_Portal_Role__C = 'tps';
        }
        
        insert u;
        //Id userId = Site.createPortalUser(u, a.Id, testUserPassword);
        //u.Id = userId;
        return u;
    }

    public static User createENTPPortalUser(String userName, Boolean strategic, String lang) {
        String CustRole = '';
        Account a = new Account(Name = 'Unit Test Account');
        if(lang=='fr'){
          CustRole = 'private cloud';
        }
        else{
          CustRole = 'tps'; 
        }
           
        a.strategic__c = strategic;
        insert a;
        ENTP_portal_additional_functions__c ENTPPortal = new ENTP_portal_additional_functions__c(Name='Test',Customer_Portal_Role__c = CustRole, Additional_Function__c='Test');
        insert ENTPPortal;
        Contact c = new Contact(Email = userName+'@unit-test.com', LastName = 'Test', AccountId = a.Id, Remedy_PIN__c = '70208');
        insert c;
        Account_Configuration__c ac = new Account_Configuration__c(TELUS_Product__c = 'Private_Cloud', Account__c = a.Id);
        insert ac;
        Profile p = [SELECT Id FROM Profile WHERE Name='Customer Community User Custom' AND UserType='CSPLitePortal' LIMIT 1]; 
   system.debug('CustRole-MBRTestUtils:'+CustRole);
//		UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];

        User u = new User(Alias = 'TestUser', Email=userName+'@unit-test.com', ContactId=c.Id, 
                            EmailEncodingKey='UTF-8', FirstName='Unit', LastName='Testing', LanguageLocaleKey=lang, 
                            LocaleSidKey=lang, CommunityNickname=userName+'UNTST', /*UserRoleId = portalRole.Id,*/ ProfileId=p.Id, 
                          	TimeZoneSidKey='America/Los_Angeles', 
                            UserName=userName+'@unit-test.com');

        u.Customer_Portal_Role__C = CustRole;
        
        insert u;
        //Id userId = Site.createPortalUser(u, a.Id, testUserPassword);
        //u.Id = userId;
        return u;
    }
      public static User createVITILcarePortalUser(String userName, Boolean strategic, String lang) {
        String CustRole = '';
        Account a = new Account(Name = 'Unit Test Account');
        
          CustRole = 'vitilcare'; 
        
           
        a.strategic__c = strategic;
        insert a;
        ENTP_portal_additional_functions__c ENTPPortal = new ENTP_portal_additional_functions__c(Name='Test',Customer_Portal_Role__c = CustRole, Additional_Function__c='Test');
        insert ENTPPortal;
        Contact c = new Contact(Email = userName+'@unit-test.com', LastName = 'Test', AccountId = a.Id, Remedy_PIN__c = '70208');
        insert c;
        Account_Configuration__c ac = new Account_Configuration__c(TELUS_Product__c = 'Private_Cloud', Account__c = a.Id);
        insert ac;
        Profile p = [SELECT Id FROM Profile WHERE Name='Customer Community User Custom' AND UserType='CSPLitePortal' LIMIT 1]; 
   system.debug('CustRole-MBRTestUtils:'+CustRole);
//      UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];

        User u = new User(Alias = 'TestUser', Email=userName+'@unit-test.com', ContactId=c.Id, 
                            EmailEncodingKey='UTF-8', FirstName='Unit', LastName='Testing', LanguageLocaleKey=lang, Customer_Portal_Name__c='vitilcare', 
                            LocaleSidKey=lang, CommunityNickname=userName+'UNTST', /*UserRoleId = portalRole.Id,*/ ProfileId=p.Id, 
                            TimeZoneSidKey='America/Los_Angeles', 
                            UserName=userName+'@unit-test.com');

        //u.Customer_Portal_Role__C = CustRole;
        
        insert u;
        //Id userId = Site.createPortalUser(u, a.Id, testUserPassword);
        //u.Id = userId;
        return u;
    }   
    public static List<ExternalToInternal__c> createExternalToInternalCustomSettings() {
        List<ExternalToInternal__c> eis = new List<ExternalToInternal__c> {
            new ExternalToInternal__c(Name = 'request1', External__c = 'Billing inquiry', Identifier__c = 'onlineStatus', Internal__c = 'SMB Care Billing'), 
            new ExternalToInternal__c(Name = 'request2', External__c = 'Account review', Identifier__c = 'onlineRequestTypeToCaseRT', Internal__c = 'Generic'), 
            new ExternalToInternal__c(Name = 'request3', External__c = 'Generic', Identifier__c = 'onlineStatus', Internal__c = 'Generic'),
            new ExternalToInternal__c(Name = 'request4', External__c = 'Test', Identifier__c = 'onlineRequestTypeToCaseRT', Internal__c = 'Generic'),
            new ExternalToInternal__c(Name = 'request5', External__c = 'Dummy', Identifier__c = 'onlineRequestTypeToCaseRT', Internal__c = 'Generic'),
            new ExternalToInternal__c(Name='test', Identifier__c='onlineRequestTypeToCaseRT', Internal__c='SMB Care Product', External__c='New product/service - add-on order'),
            new ExternalToInternal__c(Name='test1', Identifier__c='onlineRequestTypeToCaseRT', Internal__c='SMB Care Product', External__c='New product/service - hardware/accessory order'),
            new ExternalToInternal__c(Name='test2', Identifier__c='onlineRequestTypeToCaseRT', Internal__c='SMB Care Product', External__c='New product/service - renewal order'),
            new ExternalToInternal__c(Name='test3', Identifier__c='onlineRequestTypeToCaseRT', Internal__c='SMB Care Product', External__c='Change service'),
            new ExternalToInternal__c(Name='test4', Identifier__c='onlineRequestTypeToCaseRT', Internal__c='SMB Care Product', External__c='Transfer of business ownership'),
            new ExternalToInternal__c(Name='test5', Identifier__c='onlineRequestTypeToCaseRT', Internal__c='SMB Care Product', External__c='Account Update - update authorized person'),
            new ExternalToInternal__c(Name='test6', Identifier__c='onlineRequestTypeToCaseRT', Internal__c='SMB Care Product', External__c='Billing inquiry'),

            new ExternalToInternal__c(Name='ENTPTest', Identifier__c='onlineRequestTypeENTP', Internal__c='onlineStatus', External__c='Enhanced - Standard (i.e. Ethernet 10 Mg, 100Mg, WAN ADSL, etc.)'),
            new ExternalToInternal__c(Name='ENTPTest1', Identifier__c='onlineRequestTypeENTP', Internal__c='ENT Care Assure', External__c='L1L (Leased Loops, Unbundled Loops) (i.e. No Dial Tone)'),
            new ExternalToInternal__c(Name='ENTPTest2', Identifier__c='onlineRequestTypeENTP', Internal__c='ENT Care Assure', External__c='Mainstream/Data (i.e. T1, T3, DS0, etc)'),
            new ExternalToInternal__c(Name='ENTPTest3', Identifier__c='onlineRequestTypeENTP', Internal__c='ENT Care Assure', External__c='Voice - PRI'),
            new ExternalToInternal__c(Name='ENTPTest4', Identifier__c='onlineRequestTypeENTP', Internal__c='ENT Care Assure', External__c='	Voice - Phone Number (i.e. 10 digit)')
        };

        insert eis;
        return eis;
    }

    public static List<ParentToChild__c> createParentToChildCustomSettings() {
        List<ParentToChild__c> pcs = new List<ParentToChild__c> {
            new ParentToChild__c(Name = 'CCICatRequest1', Child__c = 'Account review', Identifier__c = 'CCICatRequest', Parent__c = 'Account and billing'),
            new ParentToChild__c(Name = 'CCICatRequest20', Child__c = 'Other, please specify in description', Identifier__c = 'CCICatRequest', Parent__c = 'Tech support'),
            new ParentToChild__c(Name = 'CCICatRequest21', Child__c = 'Tech Support Child', Identifier__c = 'CCICatRequest', Parent__c = 'Tech support'),
            new ParentToChild__c(Name = 'Test', Child__c = 'Test Child', Identifier__c = 'CCICatRequest', Parent__c = 'Test'),

            new ParentToChild__c(Name = 'test1', Identifier__c='CCICatRequest', Child__c = 'New product/service - add-on order', Parent__c = 'Products and services', FormComponent__c='AddOnProductForm'),
            new ParentToChild__c(Name = 'test2', Identifier__c='CCICatRequest', Child__c = 'New product/service - hardware/accessory order', Parent__c = 'Products and services', FormComponent__c='productHardwareForm'),
            new ParentToChild__c(Name = 'test3', Identifier__c='CCICatRequest', Child__c = 'New product/service - renewal order', Parent__c = 'Products and services', FormComponent__c='productRenewalForm'),
            new ParentToChild__c(Name = 'test4', Identifier__c='CCICatRequest', Child__c = 'Change service', Parent__c = 'Products and services', FormComponent__c='changeServiceForm', order_number__c=15),
            new ParentToChild__c(Name = 'test5', Identifier__c='CCICatRequest', Child__c = 'Transfer of business ownership', Parent__c = 'Account and billing', FormComponent__c='transferOwnershipForm'),
            new ParentToChild__c(Name = 'test6', Identifier__c='CCICatRequest', Child__c = 'Account Update - update authorized person', Parent__c = 'Account and billing', FormComponent__c='authorizedPersonForm'),
            new ParentToChild__c(Name = 'test7', Identifier__c='CCICatRequest', Child__c = 'Billing inquiry', Parent__c = 'Account and billing', FormComponent__c='billingInquiryForm'),

            new ParentToChild__c(Name = 'ENTPTest1', Identifier__c='ENTPCatRequest', Child__c = 'Mainstream/Data (i.e. T1, T3, DS0, etc)', Parent__c = 'Enterprise'),
            new ParentToChild__c(Name = 'ENTPTest2', Identifier__c='ENTPCatRequest', Child__c = 'Enhanced - Standard (i.e. Ethernet 10 Mg, 100Mg, WAN ADSL, etc.)', Parent__c = 'Enterprise'),
            new ParentToChild__c(Name = 'ENTPTest3', Identifier__c='ENTPCatRequest', Child__c = 'Voice - Phone Number (i.e. 10 digit)', Parent__c = 'Enterprise'),
            new ParentToChild__c(Name = 'ENTPTest4', Identifier__c='ENTPCatRequest', Child__c = 'Voice - PRI', Parent__c = 'Enterprise'),
            new ParentToChild__c(Name = 'ENTPTest5', Identifier__c='ENTPCatRequest', Child__c = 'L1L (Leased Loops, Unbundled Loops) (i.e. No Dial Tone)', Parent__c = 'Enterprise')
        };

        insert pcs;
        return pcs;
    }

    public static List<TypeToFields__c> createTypeToFieldsCustomSettings() {
        List<TypeToFields__c> tfcs = new List<TypeToFields__c> {
            new TypeToFields__c(Name = '1', Fieldname__c = 'field1', Order__c = 1, Type__c = 'SMB Care Billing'),
            new TypeToFields__c(Name = '2', Fieldname__c = 'field3', Order__c = 2, Type__c = 'SMB Care Billing'),
            new TypeToFields__c(Name = '4', Fieldname__c = 'field3', Order__c = 4, Type__c = 'SMB Care Billing'),
            new TypeToFields__c(Name = '6', Fieldname__c = 'field3', Order__c = 6, Type__c = 'SMB Care Billing')
        };

        insert tfcs;
        return tfcs;
    }

    public static List<MBRFormWrapper.AddOnUser> createAddOnUsers(){
        MBRFormWrapper.AddOnUser aUser = new MBRFormWrapper.AddOnUser();
        aUser.selectedProvince='BC';
        aUser.selectedCity='Van'; 
        aUser.existingDevice='iphone';
        aUser.existingSIM= '1';
        aUser.accessories='case';
        aUser.addInfo='test';
        aUser.isTransfer='false';
        aUser.isOwnDevice='true';

        MBRFormWrapper.AddOnUser aUser2 = new MBRFormWrapper.AddOnUser();
        aUser2.existingNumber='111';
        aUser2.currentProvider='Van'; 
        aUser2.newDevice='iphone';
        aUser2.accessories='case';
        aUser2.addInfo='test';
        aUser2.isTransfer='true';
        aUser2.isOwnDevice='false';

        List<MBRFormWrapper.AddOnUser> users = new List<MBRFormWrapper.AddOnUser>{aUser, aUser2};
        return users;
    }

    public static MBRFormWrapper.Hardware createHardware(){
        MBRFormWrapper.Hardware hardw = new MBRFormWrapper.Hardware();
        hardw.accessories = 'case';
        hardw.addInfo = 'new info';
        return hardw;
    }

    public static MBRFormWrapper.PaymentInfo createPaymentInfo(String paymentType){
        MBRFormWrapper.PaymentInfo pInfo = new MBRFormWrapper.PaymentInfo();
        pInfo.selectedPaymentType = paymentType;
        pInfo.shippingAddress = 'vancouver';
        pInfo.shippingName = 'Test';
        pInfo.shippingPhone = '1111';
        return pInfo;
    }

    public static MBRFormWrapper.ServiceUser createServiceUser(){
        MBRFormWrapper.ServiceUser sUser = new MBRFormWrapper.ServiceUser();
        sUser.mobileNo = '111';
        sUser.ratePlan = 'p60';
        sUser.newService = 'Internet';
        sUser.addInfo = 'more info';
        return sUser;
    }

    public static MBRFormWrapper.RenewalUser createRenewalUser(){
        MBRFormWrapper.RenewalUser sUser = new MBRFormWrapper.RenewalUser();
        sUser.mobileNo = '111';
        sUser.keepExsiting = 'true';
        sUser.accessories = 'case';
        sUser.addInfo = 'more info';
        return sUser;
    }

    public static MBRFormWrapper.TransferOwnership createTransferOwnership(){
        MBRFormWrapper.TransferOwnership trans = new MBRFormWrapper.TransferOwnership();
        trans.name = 'test';
        trans.phoneNumber = '1111';
        trans.email = 'r@r.com';
        trans.transferNumbers = '11111';
        trans.companyName = 'comp';
        trans.accountOwner = 'test';
        trans.transferDate = '31/01/15';
        return trans;
    }

    public static MBRFormWrapper.AuthorizedPerson createAuthorizedPerson(){
        MBRFormWrapper.AuthorizedPerson auth = new MBRFormWrapper.AuthorizedPerson();
        auth.name='test';
        auth.phoneNumber='1111';
        auth.email='t@t.com';
        auth.additionalNote='add info';
        return auth;
    }

    public static MBRFormWrapper.BillingInquiry createBilling(){
        MBRFormWrapper.BillingInquiry bill = new MBRFormWrapper.BillingInquiry();
        bill.invoiceDate = '31/01/2015';
        bill.amount = '2000';
        bill.accNumber = '2500';
        bill.phoneNumbers = '778884585';
        return bill;
    }


    

}