/*
    ###########################################################################
    # File..................: TboWorkRequestContrExtension
    # Version...............: 1
    # Created by............: Umesh Atry (TechM)
    # Created Date..........: 17/02/2016 
    # Last Modified by......: Umesh Atry (TechM)
    # Last Modified Date....: 19/05/2016
    # Description...........: Class is to populate Case Account records based on selected work request.
    #
    ############################################################################
    */

public with sharing class TboWorkRequestContrExtension  {
        private ApexPages.StandardController controller;
        public final Work_Request__c Work_Request {get;set;}
       
        public List<Case_Account__c> CaseAcc {get;set;}
        public Work_Request__c  workRequest {get;set;}
        public string error {get;set;} 
        
        public TboWorkRequestContrExtension  (ApexPages.StandardController controller) {
        try{
            this.controller = controller;
            this.Work_Request =(Work_Request__c)controller.getRecord();
        }Catch(Exception e){
             error=' Id is not Valid.';
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,error));
        }
       }
       
        
    

 
public void getCaseAccounts() {
    try{
            if(Work_Request!= null){
                    workRequest = [Select id,Service_Type__c,case__r.casenumber from Work_Request__c  where id =:Work_Request.id];
                    system.debug('workRequest '+workRequest );
                    CaseAcc = new List<Case_Account__c>();
                    String picks = workRequest.Service_Type__c;                  
    
                    system.debug('picks =='+picks);
                    CaseAcc = [select id,Name,Acc_Current__c,CAN__c,BTN__c,Billing_System__c,Account__r.Name,Service_Type__c,case__r.Casenumber from Case_Account__c                    
                    where case__r.CaseNumber =:workRequest.case__r.casenumber and Service_Type__c INCLUDES (:picks) ] ;
                    
                    
                    system.debug(CaseAcc.size());
                    
                                   
           
            }else{
               error=' Work Request is not Valid.';
              ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,error));             
                
            }
        } catch(Exception e){         
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage()));
         
        }
    }      
            
}