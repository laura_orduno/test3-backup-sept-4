/**
 * Unit tests for <code>ProductService</code>
 * 
 * @author Max Rudman
 * @since 4/6/2011
 */
@isTest
private class ProductServiceTests {
    private static Product2 product;
    private static SBQQ__ProductFeature__c feature;
    private static SBQQ__ProductOption__c option;
    private static SBQQ__PriceRule__c rule;
    
    testMethod static void testComputeConfigurationType() {
        setUp();
        
        /* Product2 test = [SELECT SBQQ__ConfigurationType__c FROM Product2 WHERE Id = :product.Id];
        System.assertEquals('Required', test.SBQQ__ConfigurationType__c);
        
        product.SBQQ__ConfigurationType__c = null;
        update product;
        feature.SBQQ__MinOptionCount__c = 0;
        update feature;
        
        test = [SELECT SBQQ__ConfigurationType__c FROM Product2 WHERE Id = :product.Id];
        System.assertEquals('Allowed', test.SBQQ__ConfigurationType__c);
        
        product.SBQQ__ConfigurationType__c = null;
        update product;
        option.SBQQ__Required__c = true;
        update option;
        test = [SELECT SBQQ__ConfigurationType__c FROM Product2 WHERE Id = :product.Id];
        System.assertEquals('Required', test.SBQQ__ConfigurationType__c);
        
        product.SBQQ__ConfigurationType__c = null;
        update product;
        option.SBQQ__Quantity__c = 1;
        update option;
        test = [SELECT SBQQ__ConfigurationType__c FROM Product2 WHERE Id = :product.Id];
        System.assertEquals('Disabled', test.SBQQ__ConfigurationType__c); */
    }
    
    private static void setUp() {
        Product2 optional = new Product2(Name='Option');
        product = new Product2(Name='Test');
        insert new Product2[]{product, optional};
        
        SBQQ__Cost__c cost = new SBQQ__Cost__c(SBQQ__Product__c=product.Id);
        cost.SBQQ__UnitCost__c = 100;
        insert cost;
        
        SBQQ__BlockPrice__c price = new SBQQ__BlockPrice__c(SBQQ__Product__c=product.Id);
        price.SBQQ__Price__c = 100;
        price.SBQQ__LowerBound__c = 10;
        price.SBQQ__UpperBound__c = 20;
        insert price;
        
        feature = new SBQQ__ProductFeature__c(SBQQ__ConfiguredSKU__c=product.Id);
        feature.SBQQ__Number__c = 1;
        insert feature;
        
        option = new SBQQ__ProductOption__c(SBQQ__ConfiguredSKU__c=product.Id);
        option.SBQQ__Number__c = 1;
        option.SBQQ__Bundled__c = true;
        option.SBQQ__OptionalSKU__c = optional.Id;
        option.SBQQ__Feature__c = feature.Id;
        insert option;
        
        SBQQ__OptionConstraint__c constr = new SBQQ__OptionConstraint__c(SBQQ__ConfiguredSKU__c=product.Id);
        constr.SBQQ__ConstrainingOption__c = option.Id;
        constr.SBQQ__ConstrainedOption__c = option.Id;
        insert constr;
        
        rule = new SBQQ__PriceRule__c(Name='Test',SBQQ__Product__c=product.Id);
        insert rule;
        
        SBQQ__PriceCondition__c cond = new SBQQ__PriceCondition__c(SBQQ__Rule__c=rule.Id,SBQQ__Object__c='Quote');
        cond.SBQQ__Field__c = String.valueOf(SBQQ__Quote__c.SBQQ__ListAmount__c);
        cond.SBQQ__Operator__c = 'greater than';
        cond.SBQQ__Value__c = '100';
        insert cond;
        
        SBQQ__PriceAction__c action = new SBQQ__PriceAction__c(SBQQ__Rule__c=rule.Id);
        action.SBQQ__Field__c = 'Additional Discount (%)';
        action.SBQQ__Value__c = '10';
        insert action;
        
        Document doc = new Document(Name='Test',Body=Blob.valueOf('Test'),FolderId=UserInfo.getUserId());
        insert doc;
        
        SBQQ__RelatedContent__c rc = new SBQQ__RelatedContent__c(SBQQ__Product__c=product.Id,SBQQ__ExternalId__c=doc.Id);
        insert rc;
        
        SBQQ__ProductRule__c pr = new SBQQ__ProductRule__c(Name='Test',SBQQ__Scope__c='Product');
        insert pr;
        
        SBQQ__ConfigurationRule__c cr = new SBQQ__ConfigurationRule__c(SBQQ__Product__c=product.Id,SBQQ__ProductRule__c=pr.Id);
        insert cr;
    }
}