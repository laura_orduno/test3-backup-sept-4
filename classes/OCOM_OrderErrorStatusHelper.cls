Public without sharing class OCOM_OrderErrorStatusHelper{
    Public static void UpdateErrorStatusOnOli(Order OrderObject){
                System.debug('Inside the Class OCOM_OrderErrorStatusHelper');
                If(OrderObject.Id != null){
                    List<OrderItem> OrderProductsToBeUpdated = new List<OrderItem>();
                    List<OrderItem> OrderProducts = [SELECT Id from OrderItem where OrderId =: OrderObject.Id];
                    If(OrderProducts.size()>0){
                        for(OrderItem o : OrderProducts){
                            o.orderMgmtId_Status__c=OrdrConstants.ORDR_ERROR_STATUS;
                            System.debug('Inside the Class OCOM_OrderErrorStatusHelper_____________>'+o.orderMgmtId_Status__c);
                            OrderProductsToBeUpdated.add(o);
                        }
                  }
                 Update OrderProductsToBeUpdated;
                }
            }
            
}