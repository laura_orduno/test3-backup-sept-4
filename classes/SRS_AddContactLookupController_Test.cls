@isTest
private Class SRS_AddContactLookupController_Test{
    private static SRS_AddcontactLookupController addLookup = new SRS_AddcontactLookupController(); 
    static testMethod void testLookup(){  
    try{
        test.startTest();            
        Account acc = new Account();
        acc.Name = 'test';
        insert acc;
        List<Service_Request__c> lstSR=new List<Service_Request__c>();
        Service_Request__c sr = new Service_Request__c();
        sr.Account_Name__c = acc.Id;
        lstSR.add(sr);
        lstSR.add(new Service_Request__c(Account_Name__c=acc.id));
        insert lstSR;         
        PageReference pageRef = Page.SRS_ContactLookup;
        String Id = sr.Id; 
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('srId', Id);
        addLookup.init();
        addLookup.cancel();
        addLookup.save();    
        system.assertEquals(acc.Name, 'test');
        addLookup = new SRS_AddcontactLookupController();
        PageReference pageRef1 = Page.SRS_ContactLookup;         
        Test.setCurrentPage(pageRef1);
        ApexPages.currentPage().getParameters().put('srId', lstSR[1].id);
        addLookup.init(); 
        addLookup.SaveAndNew();
         ApexPages.currentPage().getParameters().put('srId',null);
        addLookup.init(); 
        addLookup.srContact= null;
        addLookup.save(); 
        addLookup.SaveAndNew();
        addLookup.serReqId= null;
        addLookup.save(); 
        addLookup.SaveAndNew();
        System.AssertEquals([select Service_Request__c from Service_Request_Contact__c where Service_Request__c=:lstSR[1].id].Service_Request__c,lstSR[1].id);
        test.stopTest();  
    }catch(Exception exc)
    {
    }
    }    
}