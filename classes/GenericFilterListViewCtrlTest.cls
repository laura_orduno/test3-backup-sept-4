@isTest
public class GenericFilterListViewCtrlTest {

    public static account acc ;
    public static Opportunity opp;
    public static Offer_House_Demand__c dealSupport;
    public static Agreement_Contact__c agcon;
   
    static {
       crateAccount();
       createOpportunity();
       createDealSupport('Open','CCA',null);
    }
    @isTest
    public static void IdealRun() {
        Test.startTest();
        Test.setCurrentPageReference(New PageReference('Page.myPage'));
        ApexPages.currentPage().getParameters().put('id', dealSupport.id);
        GenericListViewCompCtrl testClass = new GenericListViewCompCtrl();
        testClass.ObjName = 'Agreement_Contact__c';
        testClass.relFieldName = 'Offer_House_Demand__c';
        testClass.RecId = dealSupport.id;
        testClass.ColFields = 'ListViewFields';
        testClass.getRecords();
        testClass.getFieldsList();
        testClass.getShowEdit();
       
        Test.stopTest();
    }
    
  
     @isTest
     public static void GenericFilterListViewCtrlTest() {
        Test.startTest();
       	Test.setCurrentPageReference(New PageReference('Page.myPage'));
        createAgreementContact();
        ApexPages.currentPage().getParameters().put('sourceId',dealSupport.id );
        ApexPages.currentPage().getParameters().put('lnkfld', 'Deal_Support__c');
        ApexPages.currentPage().getParameters().put('trgobj', 'Agreement_Contact__c');
        ApexPages.currentPage().getParameters().put('dfsn', 'ListViewFields');
        ApexPages.currentPage().getParameters().put('ffsn', 'ListViewFields');
        ApexPages.currentPage().getParameters().put('datafilter','Is_Supporting_Team__c%3D%27Yes%27' );
        GenericFilterListViewCtrl ctrl = new GenericFilterListViewCtrl();
        ctrl.targetObjInstance.put('Is_Supporting_Team__c','Yes');
        ctrl.doFilter();
        ctrl.paginator.getShowPaginationButtons();
        ctrl.paginator.sortExpression = 'name';
        ctrl.paginator.doSort();  
        Test.stopTest();
    }
    
     @isTest
     public static void GenericFilterListViewCtrlParamTest() {
        Test.startTest();
       	Test.setCurrentPageReference(New PageReference('Page.myPage'));
        createAgreementContact();
        ApexPages.currentPage().getParameters().put('sourceId',dealSupport.id );
        GenericFilterListViewCtrl ctrl = new GenericFilterListViewCtrl();
        Test.stopTest();
    }
    
    private static void crateAccount(){
        acc = new Account(); 
        acc.name = 'Test Account';
        System.debug('Inserting account');
        insert acc;
        System.debug(LoggingLevel.INFO,'New Account' +acc);
        
    }
    
    private static void createOpportunity(){
        opp = new Opportunity();
        opp.Name = 'TEST OPP';
        opp.AccountId = acc.Id;
        opp.StageName = 'NewStage';
        Opp.CloseDate = Date.newInstance(2015,12,25);
        System.debug('Inserting Opp');
        insert opp;
       
    }
    
    private static  void createDealSupport(String status,String typeOfContract,id extAgr){
        dealSupport = new Offer_House_Demand__c();
        dealSupport.OPPORTUNITY__c = opp.id;
        dealSupport.TERM_OF_CONTRACT__c = 24;
        dealSupport.Type__c ='Current';
        dealSupport.Status__c= status;
        dealSupport.TYPE_OF_CONTRACT__c= typeOfContract;
        dealSupport.Existing_Agreement__c = extAgr;
        System.debug('Inserting Deal Support');
        insert dealSupport; 
       
    }
    
    private static Agreement_Contact__c createAgreementContact() {
    	Agreement_Contact__c acontact = new Agreement_Contact__c();
        acontact.Contact__c = createContact().id;
        acontact.Deal_Support__c = dealSupport.id;
        acontact.Is_Supporting_Team__c = 'Yes';
        System.debug('Inserting Agreement Contact');
        insert acontact;
        return acontact;
    }
    
    private static Contact createContact() {
        Contact contact = new Contact();
        contact.AccountId = acc.id;
        contact.LastName = 'Smith';
        contact.Phone = '1234567890';
        contact.Language_Preference__c = 'English';     
        System.debug('inserting contact');
        insert contact;
    	return contact;
    }
}