public class CloudProject {
    public Id projectId;	
    public String projectName;
    public Decimal allocationAmount
    {
        get { return getAllocationAmount(); }
        set ;
    }
    public Map<Id, CloudProjectTeamMember> teamMemberMap;
    public Datetime startDate;
    public Datetime releaseDate;
    
    public CloudProject(Id projectId, String projectName, Datetime startDate, Datetime releaseDate) {
        this.projectId = projectid;
        this.projectName = projectName;
        this.startDate = startDate;
        this.releaseDate = releaseDate;
        
        this.teamMemberMap = new Map<Id, CloudProjectTeamMember>();
    }
    
    private Decimal getAllocationAmount(){
        Decimal ap = 0;
        
        if(teamMemberMap != null)
        {
            for(Id teamMemberId : teamMemberMap.keySet())
            {
                CloudProjectTeamMember cpTeamMember = teamMemberMap.get(teamMemberId);
                if(cpTeamMember != null)
                {
                    ap += cpTeamMember.allocationAmount != null ? cpTeamMember.allocationAmount : 0.0;
                }
            }
        }    
        
        return ap;
    }
    
    public void addTeamMember(Id teamMemberId, String teamMemberName, Decimal allocationPercentage) {
        if(!teamMemberMap.containsKey(teamMemberId))
        {
            teamMemberMap.put(teamMemberId, new CloudProjectTeamMember(teamMemberId, teamMemberName, '', allocationPercentage) );
        }                
    }
}