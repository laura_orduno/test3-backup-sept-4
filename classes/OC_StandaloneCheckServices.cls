/*******************************************************************************

     * Description  : Apex class for Standalone Check Address and Check Portability features
     * Author       : Scott Wilton
     * Date         : Oct-15-2017
     *
     * Modification Log:
     * ------------------------------------------------------------------------------
     * Ver      Date                Modified By           Description
     * ------------------------------------------------------------------------------
     * 1.0     
     
     *******************************************************************************/

public without sharing class OC_StandaloneCheckServices {
    public ServiceAddressData SAData {get;set;}
    //Anne-lise BSBD_RTA1135&72
    public ServiceAddressData SAData1 {get;set;}
    public String smbCareAddrId {get;set;}
    public List<ProductCategory> productCategoriesTop{get;set;}
    //public Account serviceAccount {get;set;}
    //public String existingServices {get;set;}
    public String typeOfOrder {get;set;}
    public String orderPath {get;set;}
    public String customerLocation {get;set;}
    public String errorMsg {get;set;}
    public String maxSpeed {get;set;}
    public String portabilityCheckMessage {get;set;}
    public String portabilityCheckTitle {get;set;}
    public Boolean portabilityCheckFailure {get;set;}
    public Boolean portabilityCheckReturned {get;set;}
    public List<String> errorMessages{get;set;}
    public Boolean showErrorMsg{
        get{
            return (errorMessages.size() > 0);
        }
        set;} 
    public List<AccountSummary> AccountList {get;set;}
    public String addressOrigin{get;set;}
    
       public String dropInfo { 
        get{
            String retVal = String.isNotBlank(dropExceptionCode) ? dropExceptionCode : '-';
            retVal += '/' + (String.isNotBlank(dropFacilityCode) ? dropFacilityCode : '-');
            retVal += '/' + (String.isNotBlank(dropTypeCode) ? dropTypeCode : '-');
            retVal += '/' + (String.isNotBlank(dropLength) ? dropLength : '-');
            return retVal;
        }
        set; 
    }
    public String dropFacilityCode { get; set; }
    public String dropTypeCode { get; set; }
    public String dropLength { get; set; }
    public String dropExceptionCode { get; set; }
    public String currentTransportTypeCode { 
        get{
            return (currentTransportTypeCode == 'F') ? 'Fibre' : 'Copper';
        } 
        set; 
    }
    public String futureTransportTypeCode { get; set; }
    public String futurePlantReadyDate { get; set; }
    public String futureTransportRemarkTypeCode { get; set; }
    public String gponBuildTypeCode { 
        get{
            return (gponBuildTypeCode == 'E') ? 'Sell as you build' : 'No';
        }
        set; 
    }
    public String provisioningSystemCode { 
        get{
            return (provisioningSystemCode == 'F') ? 'FIFA' : 'Compass';
        } 
        set; 
    }
    public String ratingNpaNxx { get; set; }
    public String COID { get; set; }
    
    //Added by Arvind as part of display the additional fields receiving via ServiceAddressMgmtService V1.1 (US-OCOM-1101)
    
     
    public class AccountSummary {
        public Id smbCareAddressId {get;set;}
        public String name {get;set;}
        public String billingName {get;set;}
        public String billingAccountId {get;set;}
        public String dateOpened {get;set;}
        public String serviceAccountId {get;set;}
        public string accountId {get;set;}
        
        public AccountSummary(SMBCare_Address__c aObj){
            if(aObj.id != null)
                smbCareAddressId = string.valueof(aObj.id);
            if(string.isNotBlank(aObj.Account__r.name))
                name = aObj.Account__r.name;
            if(string.isNotBlank(aObj.Account__r.Billing_Account_Name__c))
                billingName = aObj.Account__r.Billing_Account_Name__c;
            if(string.isNotBlank(aObj.Account__r.Billing_Account_ID__c))
                billingAccountId = aObj.Account__r.Billing_Account_ID__c;
            if(aObj.Account__r.Account_Open_Since__c != null)
                dateOpened = aObj.Account__r.Account_Open_Since__c.format();
            if(string.isNotBlank(aObj.Service_Account_Id__c))
                serviceAccountId = aObj.Service_Account_Id__c;
            if(aObj.Account__c != null)
                accountId = string.valueof(aObj.Account__c);
        }
    }
    
    public OC_StandaloneCheckServices(){
        SAData                   = new ServiceAddressData();
        //Anne-lise BSBD_RTA1135&72
        SAData1                   = new ServiceAddressData();
        /*PAData                 = new AddressData();   */
        AccountList              = new List<AccountSummary>();
        customerLocation         = 'Current';
        typeOfOrder              = 'New Services';
        orderPath                = 'Simple';
        addressOrigin            = 'outsideflow';
        smbCareAddrId            = System.currentPageReference().getParameters().get('smbCareAddrId');
        errorMessages            = new List<String>();
        portabilityCheckFailure  = null;
        portabilityCheckReturned = false;
        portabilityCheckTitle    = '';
        portabilityCheckMessage  = '';
    }


    public List<AccountSummary> getAccountList(){
        if(AccountList.size() == 0)
            LoadAccounts();
        return AccountList;
    }
    public PageReference getSAData() {
        return null;
    }
    public PageReference getSAData1() {
        return null;
    }
    public void ProcessChange(){
        system.debug('ProcessChange:smbCareAddrId='+smbCareAddrId);
    }

    private SMBCare_Address__c findAccount(string smbCareAddressId){
        SMBCare_Address__c smbCareAddr;
        Id SMBCare_AddressId;
        boolean idValid = false;
        
        try{
            SMBCare_AddressId = id.valueOf(smbCareAddressId);
            idValid = true;
        }
        catch(exception e){
            system.debug('findAccount:Id INVALID: smbCareAddressId=' + smbCareAddressId);
        }
        
        if(smbCareAddressId == 'newaccount' || !idValid){
            return null; 
        }

        try{
            smbCareAddr = [Select Service_Account_Id__c,id, Account__c, FMS_Address_ID__c, Status__c,Postal_Code__c , Street_Number__c, city__c , Street__c, Street_Name__c, Street_Direction_Code__c , Suite_Number__c , Province__c, COID__c , Building_Number__c , Country__c, State__c, Civic_Number__c, Street_Type_Code__c , Location_Id__c , Address__c , Address_Type__c ,  LocalRoutingNumber__c , NPA_NXX__c , SwitchNumber__c , TerminalNumber__c, Service_Account_Id__r.RecordTypeId from SMBCare_address__c where Id=:SMBCare_AddressId limit 1];
        }
        catch(exception e){
            system.debug('findAccount:Query returned no rows for smbCareAddressId=' + smbCareAddressId);
        }

        if(smbCareAddr == null) {
            system.debug('findAccount:could not find smbCareAddressId='+smbCareAddressId);
        }

        return smbCareAddr;
    }
    
    public void LoadAccounts(){
        List<SMBCare_Address__c> smbCareAddrList;

        system.debug('LoadAccounts:AccountList.size()='+AccountList.size());
        system.debug('LoadAccounts:smbCareAddrId='+smbCareAddrId);

//        if(!SAData.captureNewAddrStatus.equalsIgnoreCase('Valid') || string.isBlank(SAData.searchString))
        if(string.isBlank(SAData.searchString)){
            system.debug('LoadAccounts:SAData.searchString is blank');
            return;
        }
        
        AccountList.clear();
        
        // Load all the accounts with the inputted address
        string queryStr = 'SELECT id, Service_Account_Id__c, Account__c, FMS_Address_ID__c, Status__c,Postal_Code__c, Street_Number__c, city__c , Street__c, Street_Name__c, Street_Direction_Code__c, Suite_Number__c, Province__c, COID__c , Building_Number__c , Country__c,  State__c, Civic_Number__c, Street_Type_Code__c, Location_Id__c, Address__c , Address_Type__c, LocalRoutingNumber__c, NPA_NXX__c , SwitchNumber__c, TerminalNumber__c, Service_Account_Id__r.RecordTypeId, Account__r.name, Account__r.Account_Open_Since__c, Account__r.Billing_Account_Name__c, Account__r.Billing_Account_ID__c FROM SMBCare_address__c ';
        string whereStr = GetWhereClause();
        if(string.isNotBlank(whereStr))
            queryStr += whereStr;
        else
            queryStr = '';
        
        system.debug('LoadAccounts:queryStr = ' + queryStr);

        try {
            if(string.isNotBlank(queryStr)){
                smbCareAddrList = database.query(queryStr + ' limit 20');
            }
        }
        catch(queryexception qe) {
            errorMsg=qe.getmessage();
            errorMessages.add(errorMsg);
        }

        if(smbCareAddrList == null) {
            system.debug('LoadAccounts:smbCareAddrList = null');
            return;
        }
        
        system.debug('LoadAccounts:smbCareAddrList.size() = ' + smbCareAddrList.size());

        // Format Account summary fields and store in list - only adding unique accounts
        set<id> uniqueAccts = new set<id>();
        for(SMBCare_Address__c aItm : smbCareAddrList){
            if(uniqueAccts.add(aItm.Account__c) == true){
                AccountList.add(new AccountSummary(aItm));
                system.debug('LoadAccounts:AccountName=' + aItm.Account__r.name + ': serviceAccountId=' + aItm.Service_Account_Id__c + ': accountId=' + aItm.Account__c);
            }
        }
        
        if(string.isBlank(smbCareAddrId) || smbCareAddrId == 'newaccount') {
            for(AccountSummary itm : AccountList) {
                if(string.isNotBlank(itm.accountId)){
                    smbCareAddrId = itm.smbCareAddressId;
                    break;
                }
            }
            if(string.isBlank(smbCareAddrId))
                smbCareAddrId = 'newaccount';

            system.debug('LoadAccounts:Set new value for AccountList.smbCareAddrId='+smbCareAddrId);
        }

        system.debug('LoadAccounts:After function:AccountList.size()='+AccountList.size());
        system.debug('LoadAccounts: AccountList('+AccountList+')');
        
        return;
    }
    
    // Thomas QC-7727/OCOM-1042 Start
    public PageReference updateSAData() {
        System.debug('OCOM_OutOrderFlowPACController::updateSAData() start');
        dropFacilityCode              = SAData.dropFacilityCode;
        dropTypeCode                  = SAData.dropTypeCode;
        dropLength                    = SAData.dropLength;
        dropExceptionCode             = SAData.dropExceptionCode;
        currentTransportTypeCode      = SAData.currentTransportTypeCode;
        futureTransportTypeCode       = SAData.futureTransportTypeCode;
        futurePlantReadyDate          = SAData.futurePlantReadyDate;
        futureTransportRemarkTypeCode = SAData.futureTransportRemarkTypeCode;
        gponBuildTypeCode             = SAData.gponBuildTypeCode;
        provisioningSystemCode        = SAData.provisioningSystemCode;
        ratingNpaNxx                  = SAData.ratingNpaNxx;
        COID                          = SAData.COID;
        System.debug('OCOM_OutOrderFlowPACController::updateSAData() end');
        
        return null;
    }
    // Thomas QC-7727/OCOM-1042 End

    public List<SelectOption> getOrderPathValues() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Simple','Simple'));
        options.add(new SelectOption('Complex','Complex'));
        return options;
    }

    public List<SelectOption> getExistingServicesValues() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Yes','Yes'));
        options.add(new SelectOption('No','No'));
        return options;
    }   
    
    public List<SelectOption> getcustomerLocationValues() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Current','Current'));
        options.add(new SelectOption('New','New'));
        return options;
    } 
    
    public List<SelectOption> getTypeOfOrderValues() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('New Services','New Services'));
        options.add(new SelectOption('Move in address','Move in address'));
         return options;
    }

    public string GetWhereClause() {
        System.debug('OCOM_StandalonCheckServicesController::GetWhereClause() Start');
        string queryStr = ' ';
        string andStr = ' ';
        
        if(string.isNotBlank(SAData.suiteNumber)){
            andStr           = string.isNotBlank(queryStr) ? ' AND ' : ' ';
            queryStr        += andStr + 'Suite_Number__c=\'' + SAData.suiteNumber + '\'';
        }
        if(string.isNotBlank(SAData.buildingNumber)){
            andStr           = string.isNotBlank(queryStr) ? ' AND ' : ' ';
            queryStr        += andStr + 'Building_Number__c=\'' + SAData.buildingNumber + '\'';
        }
        //if(string.isNotBlank(SAData.streetNumber)){
        //    andStr     = string.isNotBlank(queryStr) ? ' AND ' : ' ';
        //    queryStr + = andStr + 'Street_Number__c=\'' + SAData.streetNumber + '\'';
        //}
        if(string.isNotBlank(SAData.street)){
            andStr           = string.isNotBlank(queryStr) ? ' AND ' : ' ';
            queryStr        += andStr + 'Street__c=\'' + SAData.street + '\'';
        }
        //if(string.isNotBlank(SAData.streetName)){
        //    andStr     = string.isNotBlank(queryStr) ? ' AND ' : ' ';
        //    queryStr + = andStr + 'Street_Name__c=\'' + SAData.streetName + '\'';
        //}
        if(string.isNotBlank(SAData.streetDirectionCode)){
            andStr           = string.isNotBlank(queryStr) ? ' AND ' : ' ';
            queryStr        += andStr + 'Street_Direction_Code__c=\'' + SAData.streetDirectionCode + '\'';
        }
        if(string.isNotBlank(SAData.city)){
            andStr           = string.isNotBlank(queryStr) ? ' AND ' : ' ';
            queryStr        += andStr + 'city__c=\'' + SAData.city + '\'';
        }
        if(string.isNotBlank(SAData.province)){
            andStr           = string.isNotBlank(queryStr) ? ' AND ' : ' ';
            queryStr        += andStr + 'Province__c=\'' + SAData.province + '\'';
        }
        if(string.isNotBlank(SAData.country)){
            andStr           = string.isNotBlank(queryStr) ? ' AND ' : ' ';
            queryStr        += andStr + 'Country__c=\'' + SAData.country + '\'';
        }
        if(string.isNotBlank(SAData.postalCode)){
            andStr           = string.isNotBlank(queryStr) ? ' AND ' : ' ';
            queryStr        += andStr + 'Postal_Code__c=\'' + SAData.postalCode + '\'';
        }
        
        if(string.isNotBlank(queryStr))
            queryStr         = ' where ' + queryStr;
            System.debug('queryStr::'+queryStr);
            System.debug('OCOM_StandalonCheckServicesController::GetWhereClause() End');
        return queryStr;
    }
    
    public static string GetAddressSearchString(SMBCare_Address__c smbCareAddr) {
        if(smbCareAddr == null)
            return null;
        
        string searchStr = ' ';

        if(smbCareAddr.Status__c.equalsIgnoreCase('valid')){
            if(string.isNotBlank(smbCareAddr.Suite_Number__c)){
                searchStr += smbCareAddr.Suite_Number__c + ' ';
            }
            if(string.isNotBlank(smbCareAddr.Building_Number__c)){
                searchStr += smbCareAddr.Building_Number__c + ' ';
            }
        }
        if(string.isNotBlank(smbCareAddr.Street__c)){
            searchStr += smbCareAddr.Street__c + ' ';
        }
        if(string.isNotBlank(smbCareAddr.city__c)){
            searchStr += smbCareAddr.city__c + ' ';
        }
        if(string.isNotBlank(smbCareAddr.Province__c)){
            searchStr += smbCareAddr.Province__c + ' ';
        }
        if(string.isNotBlank(smbCareAddr.Postal_Code__c)){
            searchStr += smbCareAddr.Postal_Code__c;
        }

        return searchStr;
    }
    
    public PageReference init(){

        System.debug('smbCareAddrId::'+smbCareAddrId);
        SMBCare_Address__c smbCareAddr = findAccount(smbCareAddrId);
        string addressStr              = GetAddressSearchString(smbCareAddr);
        SAData                         = new ServiceAddressData();
        //Anne-lise BSBD_RTA1135&72
        SAData1                         = new ServiceAddressData();

        AccountList                    = new List<AccountSummary>();



        if(string.isNotBlank(addressStr)) {
            SAData.onLoadSearchAddress  = addressStr;
            SAData.searchString         = addressStr;
            //Anne-lise BSBD_RTA1135&72
            SAData1.onLoadSearchAddress  = addressStr;
            SAData1.searchString         = addressStr;
        }  

        return null;
    }

    public PageReference initPortabilityCheck() {

        //system.debug('initPortabilityCheck - Init');

        String phoneNumber = Apexpages.currentPage().getParameters().get('phoneNumber');
        if ( phoneNumber.length() <=0 ){ 
            portabilityCheckReturned = true;
            portabilityCheckFailure = true;
            portabilityCheckTitle = Label.OCOM_WFMERROR;
            portabilityCheckMessage = Label.OC_Please_Enter_a_Phone_Number;
            return null;
        }
        //system.debug('initPortabilityCheck' + phoneNumber);

        //system.debug('SAData1' + JSON.serializePretty(SAData1));
        //system.debug('SAData1.streetName_fms' + SAData1.streetName_fms);
        //system.debug('SAData1.streetName' + SAData1.streetName);
        //system.debug('SAData1.street' + SAData1.street);
        //system.debug('SAData1.coid' + SAData1.coid);

        Map<string,Object> input = new Map<string,Object>();

        Map<string,Object> output = new Map<string,Object>();
        
        //Anne-lise BSBD_RTA1135&72
        input.put('fmsId',              SAData1.fmsId);
        input.put('locationId',         SAData1.locationId);
        input.put('province',           SAData1.province); 
        input.put(OrdrConstants.ADDR_PROVINCE_CODE,  SAData1.provinceStateCode); 
        input.put(OrdrConstants.ADDR_STREET_NUMBER,  SAData1.streetNumber); 
        input.put('coid',               SAData1.coid );
        input.put('municipalityName',   SAData1.municipalityNameLPDS);
        input.put('postalCode',         SAData1.postalCode);
        input.put('streetDirection',    SAData1.streetDirection);
        input.put('streetTypeSuffix',   SAData1.streetTypeSuffix);
        input.put('streetTypePrefix',   SAData1.streetTypePrefix);
        input.put('streetName_fms',     SAData1.streetName_fms);
        //input.put('streetName_fms',     SAData1.street);
        input.put('streetName',         SAData1.streetName);
        input.put('street',             SAData1.street);
        input.put('buildingNumber',     SAData1.buildingNumber); 
        input.put('suiteNumber',        SAData1.suiteNumber);
        input.put('npa_nxx',            SAData1.ratingNpaNxx);
        input.put('phoneNumber',        phoneNumber);

         OC_IntegrationAPIsUtil integrationUtil= new OC_IntegrationAPIsUtil();

        //system.debug('INPUT-STANDALONE: '+ input);
        integrationUtil.invokeMethod('getPortabilityCheck', input, output, new Map<string,Object>());
        //system.debug('!!@@ output '+ output.keyset());


        portabilityCheckReturned = true;
        if ( output.containsKey('PortabilityCheckRequestFailed') && output.get('PortabilityCheckRequestFailed') == true ){
                portabilityCheckTitle =  Label.OCOM_WFMERROR;
                portabilityCheckFailure = true;
                portabilityCheckMessage = (String) output.get('PortabilityCheckException');
        } else {
            if (output.containsKey('portableIndicator') && output.get('portableIndicator') == true) {
                portabilityCheckTitle = Label.OC_Success;
                portabilityCheckFailure = false;
                portabilityCheckMessage = Label.OC_Portability_Check_Success; //'This number can be ported';
            } else {
                portabilityCheckTitle = Label.Webservice_Integration_Log_Failure;
                portabilityCheckFailure = true;
                portabilityCheckMessage = Label.OC_This_Number_Cant_Be_Ported;
            }
        }

        return null;
        
    }


    public PageReference callIntegrationUtil() {
        system.debug('locationId' + SAData.locationId);
        system.debug('fmsId' + SAData.fmsId);
        system.debug('municipalityName' + SAData.municipalityNameLPDS);
        system.debug('SAData.street' + SAData.street);

        Map<string,Object> input = new Map<string,Object>();

        Map<string,Object> output = new Map<string,Object>();
        
        input.put('fmsId',SAData.fmsId);
        input.put('locationId', SAData.locationId);
        input.put('province',SAData.province);      
        input.put('coid',SAData.coid );
        input.put('municipalityName',SAData.municipalityNameLPDS);
        input.put('postalCode',SAData.postalCode);
        input.put('streetDirection',SAData.streetDirection);
        input.put('streetTypeSuffix',SAData.streetTypeSuffix);
        input.put('streetTypePrefix',SAData.streetTypePrefix);
        input.put('streetName_fms',SAData.streetName_fms);
        input.put('streetName',SAData.streetName);
        input.put('street',SAData.street);
        input.put('buildingNumber',SAData.buildingNumber); 
        input.put('suiteNumber',SAData.suiteNumber);
        input.put('npa_nxx',SAData.ratingNpaNxx);     
        system.debug('callIntegrationUtil SAData.fmsId' + SAData.fmsId);
        system.debug('callIntegrationUtil SAData.locationId' +SAData.locationId);
        system.debug('callIntegrationUtil SAData.province' + SAData.province);
        system.debug('callIntegrationUtil SAData.coid' + SAData.coid);   
        system.debug('callIntegrationUtil input' + input);   
        
        OC_IntegrationAPIsUtil integrationUtil= new OC_IntegrationAPIsUtil();
        
        integrationUtil.invokeMethod('getProductsTechnicalAvailability', input, output, new Map<string,Object>());
        
        productCategoriesTop = new List<ProductCategory>();
        List<OrdrTechnicalAvailabilityManager.ProductCategory> products = (List<OrdrTechnicalAvailabilityManager.ProductCategory>) output.get('productCategories');
        if(null != products ){
        //***********
         Map<String, ProductCategory> categoriesMap = new Map<String, ProductCategory>();
            
            Boolean isCharactersticsUnconfirmedAdded = false;
            maxSpeed = '';

            for (OrdrTechnicalAvailabilityManager.ProductCategory product : products) {
                
                ProductCategory category = new ProductCategory();
                if (categoryMap.containsKey(product.category)) {
                    String categoryKey = categoryMap.get(product.category);
                    category = new ProductCategory();
                    if (categoriesMap.containsKey(categoryKey)) {
                        category = categoriesMap.get(categoryKey);
                    } else {
                        category.type = categoryMap.get(product.category);
                        category.availability = product.availability;
                        category.icon = iconMap.get(category.type);
                    }
                    
                    List<Characteristic> characteristics = new List<Characteristic>();
                    if (category.type.equals('INTERNET')) {
                        Characteristic characteristic = new Characteristic();
                        if (String.isNotBlank(product.accessType)) {
                        if (product.accessType.equals('Fibre - FIFA')) {
                            characteristic.name            = 'PureFibre (FIFA)';
                            characteristic.value           = '1G+ Mbps';
                            maxSpeed                       = '1G+ Mbps';
                            characteristic.displaySequence = 1;
                            characteristics.add(characteristic);
                        } else if (product.accessType.equals('Fibre - Non-FIFA')) {
                            characteristic.name            = 'PureFibre';
                            characteristic.value           = '150+ Mbps';
                            maxSpeed                       = '150+ Mbps';
                            characteristic.displaySequence = 2;
                            characteristics.add(characteristic);
                        } else if (product.accessType.equals('Bonded Copper')) {
                            characteristic.name            = product.accessType;
                            characteristic.value           = product.downloadSpeed;
                            maxSpeed                       = product.downloadSpeed;
                            characteristic.displaySequence = 3;
                            characteristics.add(characteristic);
                            if (String.isNotBlank(product.ports)) {
                            characteristic                 = new Characteristic();
                            characteristic.name            = 'Available Ports';
                            characteristic.value           = product.ports;
                            characteristic.displaySequence = 4;
                                characteristics.add(characteristic);
                            }
                        } else if (product.accessType.equals('Copper')) {
                            characteristic.name            = 'Copper';
                            characteristic.value           = product.downloadSpeed;
                            maxSpeed                       = product.downloadSpeed;
                            characteristic.displaySequence = 5;
                            characteristics.add(characteristic);
                            if (String.isNotBlank(product.ports)) {
                            characteristic                 = new Characteristic();
                            characteristic.name            = 'Available Ports';
                            characteristic.value           = product.ports;
                            characteristic.displaySequence = 6;
                                characteristics.add(characteristic);
                            }
                            } else {
                            characteristic.name            = 'PureFibre';
                            characteristic.value           = 'unconfirmed';
                            characteristic.displaySequence = 7;
                                characteristics.add(characteristic);
                            characteristic                 = new Characteristic();
                            characteristic.name            = 'Copper';
                            characteristic.value           = 'unconfirmed';
                            characteristic.displaySequence = 8;
                                characteristics.add(characteristic);
                            }
                        } else {
                            characteristic.name            = 'PureFibre';
                            characteristic.value           = 'unconfirmed';
                            characteristic.displaySequence = 7;
                            characteristics.add(characteristic);
                            characteristic                 = new Characteristic();
                            characteristic.name            = 'Copper';
                            characteristic.value           = 'unconfirmed';
                            characteristic.displaySequence = 8;
                            characteristics.add(characteristic);
                        }
                        
                    }

                    if (category.characteristics == null) category.characteristics = new List<Characteristic>();
                    category.characteristics.addAll(characteristics);
                    category.characteristics.sort();
                    
                    categoriesMap.put(categoryKey, category);
                
                }
                
            }
            
            productCategoriesTop = categoriesMap.values();    
        //********
        } else {
            maxSpeed = '';
        }
        
        system.debug('output' + output);

        return null;
    }
    
     private static final Map<String, String> iconMap;
    private static final Map<String, String> categoryMap;
    static {
        iconMap = new Map<String, String>();
        iconMap.put('INTERNET', 'icon-mouse');
        iconMap.put('VOICE', 'icon-homephone');
        iconMap.put('MOBILE', 'icon-smartphone');
        iconMap.put('TV', 'icon-tv');
        iconMap.put('WiFi', 'icon-wifi');
        
        categoryMap = new Map<String, String>();
        categoryMap.put('TELUS Internet Product', 'INTERNET');
        categoryMap.put('TELUS Office Phone Product', 'VOICE');
        categoryMap.put('TELUS Wireless Product', 'MOBILE');
        categoryMap.put('TELUS TV Product', 'TV');
        categoryMap.put('TELUS Public Wi-Fi Product', 'WiFi');
        categoryMap.put('TELUS TV Product (v2)', 'TV');
    }

    public class ProductCategory {
        public String type {get;set;} 
        public String availability {get;set;} 
        public String icon {get;set;} 
        public String internet_type {get;set;}
        public List<Characteristic> characteristics  {get;set;} 
    }
    
    public class Characteristic implements Comparable {
        public String name {get;set;} 
        public String value {get;set;} 
        public Integer displaySequence {get;set;} 
        
        // Implement the compareTo() method
        public Integer compareTo(Object compareTo) {
            Characteristic characteristic = (Characteristic)compareTo;
            if (displaySequence == characteristic.displaySequence) return 0;
            if (displaySequence > characteristic.displaySequence) return 1;
            return -1;       
        }        
    }
    public PageReference RedirectToNextPage() {
        
        SMBCare_Address__c smbCareAddr = findAccount(smbCareAddrId);
        if(smbCareAddr == null){
            system.debug('RedirectToNextPage:could not find smbCareAddrId='+smbCareAddrId);
            smbCareAddrId = 'newaccount';
            orderPath = 'Simple';
        }
        
        if (orderPath == 'Complex')
            return new PageReference('/apex/smb_newQuoteFromAccount?aid='+smbCareAddr.Account__c+'&OrderPath='+orderPath);
        
        else if(smbCareAddrId == 'newaccount')
            return new PageReference('/apex/smb_NewAccount');
        
        else
            return new PageReference('/apex/OCOM_InOrderFlowContact?id=' +smbCareAddr.Service_Account_Id__c + '&OrderType=' +typeOfOrder+ 
                                     '&ParentAccId=' + smbCareAddr.Account__c + '&smbCareAddrId=' + smbCareAddrId + '&addressOrigin=outsideflow');
    }


    public class SampleResultInfo  {
        public ServiceAddressData SAData {get;set;}
        public String PhoneNumber {get;set;}

        public SampleResultInfo() {
            this.SAData = new ServiceAddressData();
            this.PhoneNumber = '';

        }
    }

}