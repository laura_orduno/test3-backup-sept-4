@isTest		
public class OCOM_UpdateOliAttributes_Test {
	private static List<PricebookEntry> pbeList = new List<PricebookEntry>();
     @isTest 
	public  static void testUpateJSON1() {
    
        HookCpqTestData.setUpData();
        List<Country_Province_Codes__c> mrktList = new List<Country_Province_Codes__c>();
        Country_Province_Codes__c mrkt = new Country_Province_Codes__c();
        mrkt.Name='ON';
        mrkt.Description__c = 'Ontario';
        mrkt.Type__c = 'Province';
        mrktList.add(mrkt);
        Country_Province_Codes__c mrkt1 = new Country_Province_Codes__c();
        mrkt1.Name='CAN';
        mrkt1.Description__c = 'Canada';
        mrkt1.Type__c = 'Country';
        mrktList.add(mrkt1);
        Country_Province_Codes__c mrkt2 = new Country_Province_Codes__c();
        mrkt2.Name='NJ';
        mrkt2.Description__c = 'New Jersey';
        mrkt2.Type__c = 'American State';
        mrktList.add(mrkt2);
        Country_Province_Codes__c mrkt3 = new Country_Province_Codes__c();
        mrkt3.Name='AB';
        mrkt3.Description__c = 'Alberta';
        mrkt3.Type__c = 'Canadian Province';
        mrktList.add(mrkt3);
        insert mrktList;
        vlocity_cmt__CpqConfigurationSetup__c cpqConfReq = new vlocity_cmt__CpqConfigurationSetup__c(Name ='OCOM_HookAvailaibilityTrigger',vlocity_cmt__SetupValue__c='ON');
        insert cpqConfReq;
		system.debug('!@@serviceAddressId 1_testUpateJSON1 '+HookCpqTestData.serviceAddressId);
    
        Test.startTest(); 
        Map<String, Object> input = new Map<String, Object>();
        Map<String, Object> output = new Map<String, Object>();
        Map<String, Object> options = new Map<String, Object>();
        input.put('cartId',HookCpqTestData.ord.id);
        /*List<OrderItem> ordItems = [Select vlocity_cmt__RootItemId__c, UpgradeDowngrade__c From OrderItem Where OrderId =: HookCpqTestData.ord.id];
        if(ordItems != null){
            for(OrderItem oliObj: ordItems){
                oliObj.vlocity_cmt__RootItemId__c = oliObj.Id;
                oliObj.UpgradeDowngrade__c = true;
            }
            //update ordItems;
        }*/
        OCOM_CustomEligibilityHook customHook1 = new OCOM_CustomEligibilityHook();
    
    	Set<String> PBEId = new Set<String>(); 
    	PBEId.add(HookCpqTestData.customPrice.id);
        vlocity_cmt.FlowStaticMap.FlowMap.put('AddedOLIsPBEIdSet',PBEId);
    	Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        //customHook1.invokeMethod('postCartsItems.PostInvoke',input,output,options);    
        //customHook1.invokeMethod('getCarts.PreInvoke',input,output,options);     
        input.put('Id', HookCpqTestData.prod.id);
        OCOM_UpdateOliAttributes.updateRunTimeAttributes(input);
    	List<vlocity_cmt.ItemWrapper> itemsList = new List<vlocity_cmt.ItemWrapper>();
    	OCOM_UpdateOliAttributes.updateRunTimeAttributes(itemsList);
    	system.debug('!@@serviceAddressId 2 '+HookCpqTestData.serviceAddressId);
        //OrdrProductEligibilityManager.getOfferAvailableCharacteristics(serviceAddressId, '9143210725313949412');
        OCOM_UpdateOliAttributes.getOLIMarketNew1();
        OCOM_UpdateOliAttributes.getOLIMarket();
        OCOM_UpdateOliAttributes.getOLIMarketNew();
        try{
            String tstSet='';
            vlocity_cmt.FlowStaticMap.FlowMap.put('preInvoke.addedItem',tstSet);
            List<vlocity_cmt.ItemWrapper> itemList = new List<vlocity_cmt.ItemWrapper>();
            OCOM_UpdateOliAttributes.updateRunTimeAttributes(itemList);
        }catch(Exception ex){}
        Test.stopTest();
        
	}

}