@isTest(SeeAllData=true)
private class QuoteMigratorTest {
testMethod static void testMigrator() {
        SBQQ__Quote__c quote = [Select Id, SBQQ__Opportunity__r.Web_Account__c From SBQQ__Quote__c Where Number_of_quote_lines__c > 6 and Completed__c = true Order By CreatedDate DESC Limit 1];
        system.assertnotequals(null, quote);
        ApexPages.currentPage().getParameters().put('qid', quote.id); 
        
        QuoteMigrateController qmc = new QuoteMigrateController();
        //System.assertNotEquals(null, qmc.onMigrate());
        //System.assertNotEquals(null, qmc.onCancel());
        
        QuoteMigrator qm = new QuoteMigrator(quote.id, qmc.destinationBundle);
        //system.assertequals(true, qm.success);
        qm = new QuoteMigrator();
        qm.doMigration(quote.id, qmc.destinationBundle);
        //system.assertequals(true, qm.success);
    }
    
    private static testMethod void testMigratorErrors() {
    	QuoteMigrator qm = new QuoteMigrator();
    	
    	Boolean result = qm.doMigration();
    	system.assertEquals(false, result);
    	
    	qm = new QuoteMigrator('invalid', null);
    	result = qm.doMigration();
    	system.assertEquals(false, result);
    	
    	SBQQ__Quote__c quote = [
	    	SELECT SBQQ__Opportunity__r.Web_Account__c
	    	FROM SBQQ__Quote__c
	    	WHERE Number_of_quote_lines__c > 6
	    	AND Completed__c = true
	    	ORDER BY CreatedDate DESC
	    	LIMIT 1
      ];
      system.assertnotequals(null, quote);
      
      qm = new QuoteMigrator(quote.id, null);
      
      result = qm.doMigration();
    	system.assertEquals(false, result);
    }
}