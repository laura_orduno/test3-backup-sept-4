/*******
* [Aditya Jamwal]
* Utility class to manage Billing Account Creation
* Test Class : BillingAccountCreationManager_Test
*******/
public class BillingAccountCreationManager {
    
    
    public class BillingAccountCreationRequest{
        
        public List<ContactPerson> contactPersonList;    
        public BillingAddress billingAddress;
        public String businessLegalName;
        public String customerProfileID;
        public String customerBusinessUnitCustomerId;
        public String regionalCustomerId;
        public String customerServiceRepresentativeId;
        public String taxProvince;
        public String creditClass;
        public String displayLanguage = 'EN';  //Default value
        public String billingLanguage = 'EN'; //Default value
        public String coreAccountPrefix;
        public String businessDomain;
        public String businessSubDomain;
        public String provincialTaxAppliesInd;
        public String billingPeriodNumber;
        public String federalTaxAppliesInd;
        public String serviceTypeCode;
        public String serviceRequested;
        public List<String> typeOfService;
        public String contractLengthInMonths;
        public String coreSandECodes;
        public Date startDate;
        public String responsiblegroup;
        public String validateAddress = 'false'; //Default value
        public String billingSystemId;
    }
    
    public class ContactPerson{
        
        public String contactFirstName;
        public String contactLastName;
        public String contactEmail;
        public String contactPhoneNumber;   
    }
    
    public class BillingAddress{
        
        public String suiteNumber;
        public String houseNumber;
        public String streetName;
        public String streetType;
        public String streetDirection;
        public String city;
        public String province;
        public String postalCode;      
        
    }
    
    public class BillingAccountCreationResponse {
        public String customerAccountNumber;
        public String billingTelephoneNumber;
        public String billingSystemId;
    }
    
    @Future(callout=true)
    public static void createBillingAccountFuture(String customerAccountJSONString , String accountId){ 
        if(String.isNotBlank(customerAccountJSONString)){
            BillingAccountCreationRequest customerAccount = (BillingAccountCreationRequest) JSON.deserialize(customerAccountJSONString,BillingAccountCreationRequest.class);
            BillingAccountCreationResponse billingAccountCreationResponse = createBillingAccount(customerAccount);
            saveBillingAccount(billingAccountCreationResponse,accountId);
        }else{
            throw new OrdrExceptions.InvalidParameterException('Input Parameter cannot be empty.');
        }
    } 
    
    public static void createBillingAccountFuture(BillingAccountCreationRequest customerAccount,String accountId){    
        if(null != customerAccount){
            String customerAccountJSONString = JSON.serialize(customerAccount);
            createBillingAccountFuture(customerAccountJSONString,accountId);
        }else{
            throw new OrdrExceptions.InvalidParameterException('Input Parameter cannot be empty.');
        }        
    }

	public static void createBillingAccountAndSave(BillingAccountCreationRequest customerAccount,String accountId){     
        BillingAccountCreationResponse billingAccountCreationResponse = createBillingAccount(customerAccount);
        saveBillingAccount(billingAccountCreationResponse,accountId);
    }    
    
    public static BillingAccountCreationResponse createBillingAccount(BillingAccountCreationRequest customerAccount){        
        system.debug(' before webservice call managar ');
        TpFulfillmentCustomerOrderV3.CustomerOrder customerOrder =  BillingAccountCreationWSCallout.createBillingAccount(customerAccount);
        system.debug(' OUTSIDE in managar ');
        return  fetchBillingAccountResponse(customerOrder);
    }
    
    private static BillingAccountCreationResponse fetchBillingAccountResponse(TpFulfillmentCustomerOrderV3.CustomerOrder customerOrder ) {
        
        BillingAccountCreationResponse billingAccountCreationResponse = new BillingAccountCreationResponse();
        if(null != customerOrder){
            if(null != customerOrder.CustomerAccountOrderItem){
                for(TpFulfillmentCustomerOrderV3.CustomerAccountOrderItem cAOI : customerOrder.CustomerAccountOrderItem){
                    if(null != cAOI.CustomerAccount){
                        if(null != cAOI.CustomerAccount.CharacteristicValue){  
                            for(TpCommonBaseV3.CharacteristicValue charVal : cAOI.CustomerAccount.CharacteristicValue) {
                                System.debug(charVal.Characteristic.Name + '=' + charVal.Value[0]); //OrdrConstants.BA_CUSTOMER_ACCOUNT_NUMBER   
                                if (charVal.Characteristic.Name == 'customerAccountNumber') billingAccountCreationResponse.customerAccountNumber = charVal.Value[0];
                                if (charVal.Characteristic.Name == OrdrConstants.BA_BILLING_SYSTEM_ID) billingAccountCreationResponse.billingSystemId = charVal.Value[0];
                                if (charVal.Characteristic.Name == OrdrConstants.BA_BTN) billingAccountCreationResponse.billingTelephoneNumber = charVal.Value[0];
                            }
                        }
                    }    
                }
            }
        }
        return billingAccountCreationResponse;  
    }
    
    private static Id saveBillingAccount(BillingAccountCreationResponse bAR,String accountId) {
        
        if(String.isNotBlank(accountId)){
            
            Account billingAccount = new Account(Id = accountId);  
            
            if(null!= bAR){
                if(String.isNotBlank(bAR.billingSystemId)){
                    billingAccount.Billing_System_ID__c = bAR.billingSystemId;
                }
                if(String.isNotBlank(bAR.billingTelephoneNumber)){
                    billingAccount.BTN__c = bAR.billingTelephoneNumber;
                }
                if(String.isNotBlank(bAR.customerAccountNumber)){
                    //Placeholder for customerAccountNumber  billingAccount. = bAR.customerAccountNumber;
                }
                update billingAccount;        
            }  
            return billingAccount.Id;
        }        
        
        return null;
    }    
   
}