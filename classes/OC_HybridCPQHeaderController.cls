/*
*******************************************************************************************************************************
Created by:     Sandip Chaudhari    18-Aug-2017     Order 2.0     No previous version

*******************************************************************************************************************************
*/
public class OC_HybridCPQHeaderController {
    public String workRequestRecordTypeId{
        get{
             RecordType rt = [SELECT Id, Name, DeveloperName FROM RecordType WHERE SObjectType = 'Work_Request__c' AND DeveloperName = 'Order_Support'];
            if(rt!=null){
                return rt.id;
            }
            return null;
        }
    }
     public String accountWorkRequestJson {get{
        FieldDefinition fd=[SELECT Id, QualifiedApiName,EntityDefinitionId,DurableId FROM FieldDefinition WHERE EntityDefinition.QualifiedApiName = 'Work_Request__c' AND QualifiedApiName = 'Account__c'];
        String returnStr=JSON.serialize(fd);
        return returnStr;
    }}
     public String opportunityWorkRequestJson {get{
        FieldDefinition fd=[SELECT Id, QualifiedApiName,EntityDefinitionId,DurableId FROM FieldDefinition WHERE EntityDefinition.QualifiedApiName = 'Work_Request__c' AND QualifiedApiName = 'Opportunity__c'];
        String returnStr=JSON.serialize(fd);
        return returnStr;
    }}
    public String orderWorkRequestJson {get{
        FieldDefinition fd=[SELECT Id, QualifiedApiName,EntityDefinitionId,DurableId FROM FieldDefinition WHERE EntityDefinition.QualifiedApiName = 'Work_Request__c' AND QualifiedApiName = 'Order__c'];
        String returnStr=JSON.serialize(fd);
        return returnStr;
    }}
    public String oppId {get;set;}
    public String oppName {get;set;}
    Public String orderId {get;set;}
    public Order order {get;set;}
    public String OrderNumber{get;set;}
    
    public string Title{get;set;}
    public string Email{get;set;}
    public string Phone{get;set;}
    
    public string Owner{get;set;}
    public String OwnerEmail{get;set;}
    public String OwnerPhone{get;set;}
    public String OwnerId{get;set;}
    public string orderStatus{get;set;}
    public Boolean isOrderCancellable{get;set;}
	public Boolean isContractRegistered{get;set;}
    public Boolean isContractInProgress{get;set;}
    public string OrderMgmtId{get;set;}
    public Boolean displayComponents{get;set;}
    public Boolean displayViewPdfBtn{get;set;}
    public Boolean disableViewPdfBtn{get;set;}
    public string ncOrderStatus{get;set;}
    public double mrc{get;set;}
    public double nrc{get;set;}
    public String stepNameP{get;set;}
    public string shortOrderId{get;set;}
    public string shortOwnerId{get;set;}
    public Id wrRecordTypeId{get;set;}
    public String rcidId {get;set;} 
    
    public  OC_HybridCPQHeaderController() {
        mrc=0.0;
        nrc=0.0;
        
        //orderId = ApexPages.currentPage().getParameters().get('OrderId') == null ? '' : ApexPages.currentPage().getParameters().get('OrderId');
        
        //system.debug('!@#!orderId '+orderId);
       //  system.debug('>>>>>> ApexPages.currentPage()' + json.serializePretty(ApexPages.currentPage()));
        system.debug('>>>>>> ApexPages.currentPage().getParameters()' + json.serializePretty(ApexPages.currentPage().getParameters()));
        //if(String.isBlank(orderId)){
            orderId = ApexPages.currentPage().getParameters().get('parentOrderId') ;
            system.debug('>>>>>> parentOrderId' + orderId);
        //}
        oppId = ApexPages.currentPage().getParameters().get('oppId') == null ? '' : ApexPages.currentPage().getParameters().get('oppId');
       if(String.isNotBlank(oppId)){
           Opportunity queriedOppObj=[select name from opportunity where id=:oppId];
           if(queriedOppObj!=null){
               oppName=queriedOppObj.name;
           }
           
       }
        try{
            wrRecordTypeId = Schema.SObjectType.Work_Request__c.getRecordTypeInfosByName().get('Order Support').getRecordTypeId();
            
        }catch (Exception ex){}
        string rcidAccId = ApexPages.currentPage().getParameters().get('rcid'); 
        if(String.isNotBlank(rcidAccId)){
            List<account> acclist =  [select id,name from account where id =:rcidAccId];
            if(null != acclist && acclist.size()>0){
                genericAccountName = acclist[0].name;
                rcidId = acclist[0].Id;
            }
        }  
        
        //if(orderId == null || orderId == ''){
        //    orderId = ApexPages.currentPage().getParameters().get('id') == null ? '' : ApexPages.currentPage().getParameters().get('id');
        //    system.debug('>>>>>> Order' + orderId);
        //}
        
        if (orderId != null) {
            list<order> orderList = [SELECT ordermgmtid_status__c, Name, OrderNumber, Status, OrderMgmtId__c, Type, Account.Name, AccountId,
                                     CustomerAuthorizedById, CustomerAuthorizedBy.Name, CustomerAuthorizedBy.Email,
                                     CustomerAuthorizedBy.Phone, OwnerId, Owner.Name, Owner.Email,
                                     Owner.Phone,Sales_Representative__c,Master_Order_Status__c,
                                     RequestedDate__c,Service_Address__r.Address__c,
                                     Service_Address__r.Street_Address__c,Service_Address__r.City__c,Service_Address__r.FMS_Address_ID__c,
                                     Service_Address__r.Send_to_SACG__c,Service_Address_Text__c,Service_Address__r.Maximum_Speed__c, 
                                     Service_Address__r.isTVAvailable__c,Service_Address__r.Forbone__c,Service_Address__r.Status__c, 
                                     Maximum_Speed__c,Forbone__c, ContactTitle__c, ContactEmail__c, ContactPhone__c,MoveOutServiceAddress__c,
                                     MoveOutServiceAddress__r.Suite_Number__c,MoveOutServiceAddress__r.Address__c,MoveOutServiceAddress__r.City__c,
                                     MoveOutServiceAddress__r.Postal_Code__c, MoveOutServiceAddress__r.Country__c,MoveOutServiceAddress__r.Province__c,
                                     MoveOutServiceAddress__r.AddressText__c , Service_Address__r.Is_ILEC__c 
                                     FROM Order 
                                     WHERE Id =: orderId];
                                     
            List<OrderItem> orderItemRecs = [SELECT Contract_Line_Item__r.vlocity_cmt__ContractId__r.status,Id, orderMgmtId_Status__c, PriceBookEntry.product2.IncludeQuoteContract__c,
                                             vlocity_cmt__RecurringTotal__c,vlocity_cmt__OneTimeTotal__c,
                                             PriceBookEntry.product2.Sellable__c, Order.Status
                                             FROM OrderItem WHERE Order.parentID__c != null AND Order.parentID__c = :orderId AND Order.Status != 'Cancelled'];
            
            system.debug('>>>>>> Order' + orderList);
            if(orderList.size()>0 && !orderList.isEmpty() ){
                order = orderList[0];
                rcidId = order.AccountId;
                if(String.isNotBlank(order.Id)){
                    shortOrderId = String.valueOf(order.Id).substring(0, 15);
                }
                
                if(String.isNotBlank(order.Sales_Representative__c)){
                    shortOwnerId = String.valueOf(order.Sales_Representative__c).substring(0, 15);
                }else{
                    shortOwnerId = String.valueOf(order.ownerId).substring(0, 15);
                }
                
                Title = order.ContactTitle__c != null ? order.ContactTitle__c: '';
                Email = order.ContactEmail__c != null ? order.ContactEmail__c: '';
                Phone = order.ContactPhone__c != null ? order.ContactPhone__c: '';
                if(order.CustomerAuthorizedBy != null){
                    system.debug('>>>>>> Order : Owner with CustomerAuthorizedBy=' + order.CustomerAuthorizedBy);
                    Owner = order.CustomerAuthorizedBy.Name != null ? order.CustomerAuthorizedBy.Name :'';
                    OwnerId = order.CustomerAuthorizedByID != null ? ''+order.CustomerAuthorizedById :'';
                    OwnerPhone = order.CustomerAuthorizedBy.Phone != null ? order.CustomerAuthorizedBy.Phone:'';
                    OwnerEmail = order.CustomerAuthorizedBy.Email!= null ? order.CustomerAuthorizedBy.Email:'';
                }else{
                    //Defect #744-Order header defaulting with sales rep name, commenting out populating owner with current user                    
                    /*
                    Owner = order.Owner.Name != null ? order.Owner.Name :'';
                    OwnerId = order.Owner!= null ? ''+order.Owner:'';
                    OwnerPhone = order.Owner.Phone != null ? order.Owner.Phone:'';
                    OwnerEmail = order.Owner.Email!= null ? order.Owner.Email:''; */
                    
                    OwnerId = order.Owner!= null ? ''+order.Owner:'';
                    Owner = '';
                    OwnerPhone = '';
                    OwnerEmail = '';
                    //End of Fix : Defect #744-Order
                }
                OrderNumber = order.OrderNumber != null? order.OrderNumber : '';
                orderStatus = order.Master_Order_Status__c != null? order.Master_Order_Status__c:'Not Submitted';
                OrderMgmtId = order.OrderMgmtId__c != null? order.OrderMgmtId__c:'';
                
                ncOrderStatus=order.ordermgmtid_status__c != null? order.ordermgmtid_status__c:order.Status;
            }else{
                system.debug('>>>>>> OrderList is null : setting ownner with current user.');
                //Defect #744-Order header defaulting with sales rep name, commenting out populating owner with current user
                //setCurrentUserInfo();
                orderStatus = OrdrConstants.ORDR_NOT_SUBMITTED;
            }
            
            isOrderCancellable=true; 
            
            disableViewPdfBtn = true;
            if(orderItemRecs != null && orderItemRecs.size()>0)
            {
                for(OrderItem orderRec : orderItemRecs)
                {
                    System.debug('orderRec.orderMgmtId_Status__c='+orderRec.orderMgmtId_Status__c);
                    if(orderRec.vlocity_cmt__OneTimeTotal__c != null){
                        nrc = nrc + orderRec.vlocity_cmt__OneTimeTotal__c;
                    }
                    if(orderRec.vlocity_cmt__RecurringTotal__c != null){
                        mrc = mrc + orderRec.vlocity_cmt__RecurringTotal__c;
                    }                
					if((
                        String.isNotBlank(orderRec.orderMgmtId_Status__c) && 
                        OrdrConstants.OI_PONR_STATUS.equalsIgnoreCase(orderRec.orderMgmtId_Status__c)) || 
                       (String.isNotBlank(orderRec.orderMgmtId_Status__c) && 
                        OrdrConstants.OI_COMPLETED_STATUS.equalsIgnoreCase(orderRec.orderMgmtId_Status__c))
                      || (String.isNotBlank(orderRec.Contract_Line_Item__r.vlocity_cmt__ContractId__r.status) &&
                          ('Contract Registered'.equalsIgnoreCase(orderRec.Contract_Line_Item__r.vlocity_cmt__ContractId__r.status)
                          || 'Customer Accepted'.equalsIgnoreCase(orderRec.Contract_Line_Item__r.vlocity_cmt__ContractId__r.status)
                        || 'Contract Accepted'.equalsIgnoreCase(orderRec.Contract_Line_Item__r.vlocity_cmt__ContractId__r.status)
                        || 'Signature Declined'.equalsIgnoreCase(orderRec.Contract_Line_Item__r.vlocity_cmt__ContractId__r.status)
                        || 'In Progress'.equalsIgnoreCase(orderRec.Contract_Line_Item__r.vlocity_cmt__ContractId__r.status )))
                      ){
                           isOrderCancellable=false; 
                       } 
                    if(String.isNotBlank(orderRec.Contract_Line_Item__r.vlocity_cmt__ContractId__r.status) &&
                       ('Customer Accepted'.equalsIgnoreCase(orderRec.Contract_Line_Item__r.vlocity_cmt__ContractId__r.status)
                        ||'Contract Accepted'.equalsIgnoreCase(orderRec.Contract_Line_Item__r.vlocity_cmt__ContractId__r.status)
                        || 'Signature Declined'.equalsIgnoreCase(orderRec.Contract_Line_Item__r.vlocity_cmt__ContractId__r.status)
                        || 'In Progress'.equalsIgnoreCase(orderRec.Contract_Line_Item__r.vlocity_cmt__ContractId__r.status))){
                        isContractInProgress=true;
                    }
                    if(String.isNotBlank(orderRec.Contract_Line_Item__r.vlocity_cmt__ContractId__r.status) &&
                       ('Contract Registered'.equalsIgnoreCase(orderRec.Contract_Line_Item__r.vlocity_cmt__ContractId__r.status)                    
                      )){
                        isContractRegistered=true;
                    }
                    
                    if(orderRec.PriceBookEntry.product2.Sellable__c!=null && orderRec.PriceBookEntry.product2.Sellable__c==true){
                        if(orderRec.PriceBookEntry.product2.IncludeQuoteContract__c == true)
                            disableViewPdfBtn = false; 
                    }
                    
                }
            }
            System.debug('isOrderCancellable='+isOrderCancellable);
        }else{
            //Defect #744-Order header defaulting with sales rep name, commenting out populating owner with current user
            //setCurrentUserInfo();
            orderStatus = OrdrConstants.ORDR_NOT_SUBMITTED;
        }
        
    }
    public string Type{get{
        if((Type == null || Type == '') && order != null){
            return  Type = order.Type != null ? order.Type:'';
        }
        else 
            return  Type ;
    }
                       set;}
    
    public String genericAccountName{
        get{
            if(order != null){
                return genericAccountName = order.Account.Name != null ? order.Account.Name:'';
            } 
            
            else 
                return genericAccountName;
        }
        set;}
    
    public PageReference cancelOrder() {    
        // ToDO: Change below code for Order2.0
        PageReference pageRef = new PageReference('/apex/OCOM_CancelOrder?id='+orderId);
        pageRef.setRedirect(true);
        return pageRef;        
    }
    
   /* public void setCurrentUserInfo() {    
        USer currentUserObj = [SELECT Id, email, Name, MobilePhone, Phone 
                                   FROM User 
                                   WHERE Id=: UserInfo.getUserId()];
        Owner = currentUserObj.Name;
        OwnerId = currentUserObj.ID;
        OwnerPhone = currentUserObj.Phone;
        OwnerEmail = currentUserObj.Email;
        system.debug('@@@ currentUserObj ' + currentUserObj);        
    }*/
    
    /*public void getMRC(Id masterOrderId){
        AggregateResult[] groupedResults = [SELECT SUM(vlocity_cmt__RecurringTotal__c)summ FROM Order where Order.ParentId__c =: masterOrderId];
        mrc = (Decimal)groupedResults[0].get('summ');
    }
    
    public void getNRC(Id masterOrderId){
        AggregateResult[] groupedResults = [SELECT SUM(vlocity_cmt__OneTimeTotal__c)summ FROM Order where Order.ParentId__c =: masterOrderId];
        nrc = (Decimal)groupedResults[0].get('summ');
    } */   
}