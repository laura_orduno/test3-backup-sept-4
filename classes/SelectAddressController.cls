/******************************************************************************************************
Controller class for Searching and selecting addresses.
Has functionality for - 
1. Looking up all addresses associated with the opp Account - CBUCID / RCID / BAN / Addresses
2. Search and select an address
3. Clone/Create new address.

Author - Rohit Mehta
Date - Oct 2009

******************************************************************************************************/

public with sharing class SelectAddressController {
	
	//opp / pli to which this address needs to be associated with
	public Id opportunityId;
	public Id pliId;
	public Opportunity opportunity {get; set;}
	public Opp_Product_Item__c pli {get; set;}
	
	//this is the address displayed on the page for the user to search
	public Address__c address {get; set;}
	//selected address from the page
	public String selectedAddress {get; set;}
	//search results
	public List<AddressWrapper> addressList {get; set;}
	
	public String selectedTab {get; set;}
	public String srcObject {get; set;}
	public String srcDescription {get; set;}
	public Boolean duplicate {get; set;}
	
	public static final Integer MAX_NO_OF_RECORDS = 200;
	
	public SelectAddressController(){
		opportunityId = system.currentPageReference().getParameters().get('oppId');
		pliId = system.currentPageReference().getParameters().get('pliId');

		selectedTab = 'selectAddressTab';
		duplicate = false;
		addressList = new List<AddressWrapper>();
		address = new Address__c();

		init();

	}
	
	private void init(){
		opportunity = [Select Name, AccountId, Account.Name from Opportunity where Id = :opportunityId];

		if (pliId == null){ //invoke from Opportunity page
			pli = null;
			srcObject = 'Opportunity';
			srcDescription = opportunity.Name;
		} else { //invoke from Opportunity Product Item page
			pli = [Select Name, Opportunity__c, Quantity__c, Product__r.Product_Family__c from Opp_Product_Item__c where Id = :pliId];
			srcObject = 'Product Item';
			srcDescription = pli.Product__r.Product_Family__c;
		}	
	}
	
	public Pagereference cancel(){
		if (pliId == null){
			return new Pagereference('/' + opportunityId);
		} else {
			return new Pagereference('/' + pliId);
		} 
	}

	//called for m the page on "SearchAccount" click
	public Pagereference searchAddress(){
		SearchAddressUtil searchUtil = new SearchAddressUtil(opportunity.AccountId, MAX_NO_OF_RECORDS); 
		System.debug('address = ' + address);
		resetAddress(address);
		addressList = searchUtil.getAddresses(address);
		if (addressList == null || addressList.isEmpty()) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, System.Label.Select_Address_No_Record));
		} else if (addressList.size() == MAX_NO_OF_RECORDS) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, MAX_NO_OF_RECORDS + ' ' + System.Label.Select_Address_Too_Many_Records));
		}
		System.debug('addressList = ' + addressList.size());
		return null;
	}
	
	//called for the page - when user selects an address
	public Pagereference selectAddress(){
		if (selectedAddress == null) {
			//maybe add a message here
			return null; 
		}
		
		AddressWrapper wrapper = addressList.get(Integer.valueOf(selectedAddress));
		try {
			if (pli != null) {
				pli.Street__c = wrapper.getStreet();
				pli.City__c = wrapper.getCity();
				pli.State_Province__c = wrapper.getState();
				pli.Country__c = wrapper.getCountry();
				pli.Postal_Code__c = wrapper.getPostalCode();
				pli.Building_Name__c = wrapper.getBuildingName();
				pli.Floor__c = wrapper.getFloor();
				pli.Suite_Unit__c = wrapper.getSuiteUnit();
				pli.Street_Type__c = wrapper.getStreetType();
				pli.Street_Direction__c = wrapper.getStreetDirection();
				update pli;
			} else { //associate it with opp
				opportunity.Street__c = wrapper.getStreet();
				opportunity.City__c = wrapper.getCity();
				opportunity.State_Province__c = wrapper.getState();
				opportunity.Country__c = wrapper.getCountry();
				opportunity.Postal_Code__c = wrapper.getPostalCode();
				opportunity.Building_Name__c = wrapper.getBuildingName();
				opportunity.Floor__c = wrapper.getFloor();
				opportunity.Suite_Unit__c = wrapper.getSuiteUnit();
				opportunity.Street_Type__c = wrapper.getStreetType();
				opportunity.Street_Direction__c = wrapper.getStreetDirection();
				update opportunity;
			}
		} catch (Exception e) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
			return null;
		}
		//go back where u came from
		return cancel();	
	}

	public Pagereference copyAddress(){
		if (selectedAddress == null) {
			//maybe add a message here
			return null; 
		}
		
		AddressWrapper wrapper = addressList.get(Integer.valueOf(selectedAddress));
		try {
			address.Street__c = wrapper.getStreet();
			address.City__c = wrapper.getCity();
			address.State_Province__c = wrapper.getState();
			address.Country__c = wrapper.getCountry();
			address.Postal_Code__c = wrapper.getPostalCode();
			address.Building_Name__c = wrapper.getBuildingName();
			address.Floor__c = wrapper.getFloor();
			address.Suite_Unit__c = wrapper.getSuiteUnit();
			address.Street_Type__c = wrapper.getStreetType();
			address.Street_Direction__c = wrapper.getStreetDirection();
		} catch (Exception e) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
			return null;
		}
		selectedTab = 'addNewAddressTab';
		return null;	
	}

	public Pagereference searchDuplicateAddress(){ 
		//Check for duplicate address
		SearchAddressUtil searchUtil = new SearchAddressUtil(opportunity.AccountId, MAX_NO_OF_RECORDS);
		System.debug('address = ' + address);
		//Exclude fields in search by setting the value to null in a temp address variable
		Address__c add = address.clone(false, false);
		resetAddress(add);
		addressList = searchUtil.getAddresses(add);
		System.debug('addressList = ' + addressList.size());
	

		//Flag duplicate if any
		duplicate = (addressList.size() > 0);
		//If no duplicates found, add address
		if (!duplicate) {		
			return addAddress();
		}

		return null;
	}

	public Pagereference enterNewAddress(){
		//Clear duplicate address list
		duplicate = false;
		
		return null;
	}

	public Pagereference addAddress(){
		
		//If no duplicate found, insert new record into Address table
		address.Account__c = opportunity.AccountId;
		address.Comments__c = 'Created via Select Address function';
		if (duplicate) {
			address.Comments__c = address.Comments__c + ' (duplicate address found and ignored by creator)';		
		}
		insert address;
		
		//Copy the address to either the Opportunity or Product Item record
		if (pliId == null){
			opportunity.Street__c = address.Street__c;
			opportunity.City__c = address.City__c;
			opportunity.State_Province__c = address.State_Province__c;
			opportunity.Postal_Code__c = address.Postal_Code__c;
			opportunity.Country__c = address.Country__c;
			opportunity.Building_Name__c = address.Building_Name__c;
			opportunity.Floor__c = address.Floor__c;
			opportunity.Suite_Unit__c = address.Suite_Unit__c;
			opportunity.Street_Type__c = address.Street_Type__c;
			opportunity.Street_Direction__c = address.Street_Direction__c;
			update opportunity;
			return new Pagereference('/' + opportunityId);
		} else {
			pli.Street__c = address.Street__c;
			pli.City__c = address.City__c;
			pli.State_Province__c = address.State_Province__c;
			pli.Postal_Code__c = address.Postal_Code__c;
			pli.Country__c = address.Country__c;
			pli.Building_Name__c = address.Building_Name__c;
			pli.Floor__c = address.Floor__c;
			pli.Suite_Unit__c = address.Suite_Unit__c;
			pli.Street_Type__c = address.Street_Type__c;
			pli.Street_Direction__c = address.Street_Direction__c;
			update pli;
			return new Pagereference('/' + pliId);
		} 		
	}
	
	private void resetAddress(Address__c a) {
		a.Country__c = null;
		a.Building_Name__c = null;
		a.Floor__c = null;
		a.Suite_Unit__c = null;
		a.Street_Type__c = null;
		a.Street_Direction__c = null;
		
	}

}