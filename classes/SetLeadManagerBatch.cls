global with sharing class SetLeadManagerBatch implements Database.batchable<sObject>{ 

	public String query;

	global database.querylocator start(Database.BatchableContext BC){
            return Database.getQueryLocator(query);}

/*   global Iterable start(Database.BatchableContext info){ 
       return new CustomAccountIterable(); 
   }*/     
   global void execute(Database.BatchableContext info, List<sObject> scope){
       List<Lead> leadsToUpdate = new List<Lead>();
       Set<Id> ownerIds = new Set<Id>();
       for(sObject l : scope){
       		ownerIds.add((Id) l.get('ownerId'));
       }
       Map<id, User> ownerMap = new Map<Id,User>([select Manager_1__c, Manager_2__c, Manager_3__c, Manager_4__c from User where id in :ownerIds]);
       for(sObject l : scope){ 
            l.put('Manager_1__c',ownerMap.get((Id) l.get('ownerId')).Manager_1__c);
            l.put('Manager_2__c',ownerMap.get((Id) l.get('ownerId')).Manager_2__c);
            l.put('Manager_3__c',ownerMap.get((Id) l.get('ownerId')).Manager_3__c);
            l.put('Manager_4__c',ownerMap.get((Id) l.get('ownerId')).Manager_4__c);
           leadsToUpdate.add((Lead) l); 
       } 
       update leadsToUpdate; 
   }     
   global void finish(Database.BatchableContext info){     
   } 
   
}