@istest
public class OC_GetDocumentTemplatetest {

    testmethod static void testinvokemethod()
    {
        Test.startTest();
        Id orderId= OrdrTestDataFactory.singleMethodForOrderId();
        Account acct= new Account(name='Test Account', Phone = '123456' , fax = '1234567');
        insert acct;
       Opportunity opp1= newOpportunity(acct.id);
        Contact con1 = newcontact(acct.id);
        User user1= newuser();
        id RecordTypeId= Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Vlocity Contracts').getRecordTypeId();
       /* Contract testContractObj = new Contract(recordtypeid=RecordTypeId, status = 'Draft',
                                                Contract_Type__c='New',AccountId=acct.id,ContractTerm=12,
                                                StartDate=system.today(),CustomerSignedId=con1.id);
        insert testContractObj;*/
      
        Contracts__c Conreq1= newcontractRequest(Opp1.id,con1.id,user1.id,acct.id, orderId,null);
        //invokeMethod(String methodName, , , )
        String methodName='getContractTemplateName';
        Map<String,Object> inputMap = new Map<String,Object>();
        Map<String,Object> outMap = new Map<String,Object>();
        Map<String,Object> options = new Map<String,Object>();
        inputMap.put('contextObjId', Conreq1.id);
        inputmap.put('contractObj', null);
        inputmap.put('itemsObj', null);
        OC_GetDocumentTemplate OC_GetDocumentTemplate1 = new OC_GetDocumentTemplate();
        OC_GetDocumentTemplate1.invokeMethod(methodName, inputMap, outMap, options);
        /*Conreq1.Type_Of_Contract__c ='Amendment';
        update Conreq1;
        OC_GetDocumentTemplate1.invokeMethod(methodName, inputMap, outMap, options);*/
    }
     private static Opportunity newOpportunity(id AccId)
    {
        Opportunity opp = new Opportunity();
        opp.Name = 'TEST OPP';
        opp.AccountId =AccId;
        opp.StageName = 'NewStage';
        Opp.CloseDate = Date.newInstance(2015,12,25);
        insert opp;
        return opp;
    }
    private static user newuser()
   {
      Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User user1= new User(
            Alias = 'testcat9',
            Email='standarduser@testorg.com32wfsdfsdfsdf', 
            EmailEncodingKey='UTF-8',
            LastName='Robinson',
            LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US',
            ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles',
            UserName='standarduser@testorg.com32wfsdfsdfsdf',
            Release_Code__c = '05'
        );
       insert user1;
       return user1;
       
   }
     private static Contracts__c newcontractRequest(id OppId ,id Conid, id userid,id accid,id OrderId,id contractid)
    {
        Contracts__c conreq1= new Contracts__c();
        Id rectypeid= Schema.SObjectType.Contracts__c.getRecordTypeInfosByName().get('eContract').getRecordTypeId();
        conreq1.Contract_Type__c = 'New';
        conreq1.Opportunity__c = OppId;
        Conreq1.RecordTypeId=rectypeid;
       Conreq1.Customer_Signor__c=Conid;
       Conreq1.TELUS_Signor__c=userid;
        conreq1.Account__c= accid;
         //conreq1.Language_Preference__c= 'English';
       // conreq1.Order__c = OrderId;
        conreq1.eContract__c=contractid;
        conreq1.Type_Of_Contract__c = 'New';
        insert Conreq1;
        return conreq1;
    }
     private static Contact newcontact(id AccId)
   {
       
           Contact testContactObj = new Contact(firstname = 'TestConFirstName', LastName = 'TestConLastname', accountId = AccId , Phone = '123456' , fax = '1234567', Email = 'test@gmail.com');
            insert testContactObj;     
       return testContactObj;
   }
}