/*
###########################################################################
# File..................: SMB_Bundle_CallOutHelper
# Version...............: 26
# Created by............: Vinay Sharma
# Created Date..........: 28-Aug-2013
# Last Modified by......: 
# Last Modified Date....: 
# Description...........: This class contains methods to call Bundle Quote web service oprations
#
# Copyright (c) 2000-2012. Astadia, Inc. All Rights Reserved.
#
# Created by the Astadia, Inc. Modification must retain the above copyright notice.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any commercial purpose, without fee, and without a written
# agreement from Astadia, Inc., is hereby forbidden. Any modification to source
# code, must include this paragraph and copyright.
#
# Permission is not granted to anyone to use this software for commercial uses.
#
# Contact address: 2839 Paces Ferry Road, Suite 350, Atlanta, GA 30339
# Company URL : http://www.astadia.com

# 25 April 2017 - Defect # 12560 - OCOM RWMS Changes
# 07 June 2017 Sandip - TF Case - 02454126 - Defect # 56391 - Province missing in request
###########################################################################
*/
public  class SMB_Bundle_CallOutHelper {
    public  void createBundleCommitmentHelper(string productName , ID oPPID, ID accountId, map<string,integer> offerMap){
        map<string,schema.recordtypeinfo> recordTypeInfoMap=webservice_integration_error_log__c.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        webservice_integration_error_log__c log=new webservice_integration_error_log__c(recordtypeid=recordTypeInfoMap.get(label.webservice_integration_log_success).getrecordtypeid(),object_name__c='SMB_Bundle_CallOutHelper',external_system_name__c='RWMS');
        Opportunity oppToUpdate = [Select Id, RWMS_Response__c from Opportunity where Id = :oPPID];
        try{
            SMB_Bundle_BundleCommExtSvcReqResp_v1.CreateBundleCommitmentResponse resBundleService = new SMB_Bundle_BundleCommExtSvcReqResp_v1.CreateBundleCommitmentResponse();
            scpq__SciQuote__c quote = [Select scpq__QuoteId__c, smb_QuoteNumber__c  from scpq__SciQuote__c where scpq__OpportunityId__c=:oPPID and scpq__Primary__c = true LIMIT 1];
            Account accountObj = [Select Name,Billing_Account_ID__c, BillingStreet, BillingState, BillingPostalCode, BillingCountry, BillingCity  from Account where Id =: accountId limit 1];
            string billingAccountNum  = accountObj.Billing_Account_ID__c;
            SMB_Bundle_BundleCommitmentCommonType_v1.CustomerInfo customerInfo = new SMB_Bundle_BundleCommitmentCommonType_v1.CustomerInfo();
            SMB_Bundle_TelusCommonAddressTypes_v5.Address billingAddress = new SMB_Bundle_TelusCommonAddressTypes_v5.Address();
            billingAddress.postalZipCode = accountObj.BillingPostalCode;
            billingAddress.provinceStateCode = accountObj.BillingState;
            billingAddress.countryCode = accountObj.BillingCountry;
            billingAddress.streetName = accountObj.BillingStreet;
            billingAddress.municipalityName= accountObj.BillingCity;
            customerInfo.address = billingAddress;
                
            SMB_Bundle_EnterpriseCommonName_v1.IndividualName customerName = new SMB_Bundle_EnterpriseCommonName_v1.IndividualName();
            customerName.firstName= accountObj.Name;
            customerName.lastName= '';
            customerInfo.customer = customerName;
            
            list<SMB_Bundle_BundleCommitmentCommonType_v1.WirelessOfferReference> wirelessOfferRefList = new list<SMB_Bundle_BundleCommitmentCommonType_v1.WirelessOfferReference>();
            SMB_Bundle_BundleCommitmentCommonType_v1.WirelessOfferReference worObj ;
            if(offerMap.size()>0)
            {
                for (string s :offerMap.keyset())
                {   
                    worObj = new SMB_Bundle_BundleCommitmentCommonType_v1.WirelessOfferReference();
                    worObj.wirelessOfferCode = s;
                    worObj.originalOffersAvailableNum = offerMap.get(s);
                    wirelessOfferRefList.add(worObj);       
                }
            }
    
    
            //Web service primary class object
            SMB_Bundle_BundleCommitmentExtSvc_1.BundleCommitmentExtSvc_v1_0_SOAP servicePort = new SMB_Bundle_BundleCommitmentExtSvc_1.BundleCommitmentExtSvc_v1_0_SOAP();
            
            servicePort = prepareCallout(servicePort);
                        
            //Additional variable assignments are pending for customerInfo and wirelessOfferRefList
            
            //Callout
                log.request_payload__c='Opportunity :'+oppId+', Quote #:'+quote.smb_quotenumber__c+', Product Name:'+productName+', Billing Account #:'+billingAccountNum;
            resBundleService = servicePort.createBundleCommitment(quote.smb_QuoteNumber__c, productName, billingAccountNum, customerInfo, wirelessOfferRefList);
            if (string.isNotBlank(resBundleService.errorCode)){
                if ( resBundleService.errorCode == '0'){
                    oppToUpdate.RWMS_Response__c = 'Bundle registration successful with Code :'+resBundleService.errorCode;
                }else{
                    oppToUpdate.RWMS_Response__c  = string.valueof('Bundle registration Error with Code :'+resBundleService.errorCode + ' and Message : '+resBundleService.messageList[0].message);
                    
                }
                log.error_code_and_message__c=oppToUpdate.rwms_response__c;
                update oppToUpdate;
            }
        }catch (CalloutException ce){
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,e.getmessage()));
            log.recordtypeid=recordTypeInfoMap.get(label.webservice_integration_log_failure).getrecordtypeid();
            log.error_code_and_message__c=ce.getmessage();
            log.stack_trace__c=ce.getstacktracestring();
            log.Status__c='Failed';
            string statusMsg;
            statusMsg = 'Bundle registration callout Failure : '+ce.getmessage();
            if (statusMsg.length()>255)
            {
                oppToUpdate.RWMS_Response__c = statusMsg.substring(0, 255);
                update oppToUpdate;
            }
        }catch(exception e){
            log.recordtypeid=recordTypeInfoMap.get(label.webservice_integration_log_failure).getrecordtypeid();
            log.error_code_and_message__c=e.getmessage();
            log.stack_trace__c=e.getstacktracestring();
            log.Status__c='Failed';
        }finally{
            log.error_code_and_message__c=(string.isnotblank(log.error_code_and_message__c)&&log.error_code_and_message__c.length()>32767?log.error_code_and_message__c.substring(0,32767):log.error_code_and_message__c);
            log.stack_trace__c=(string.isnotblank(log.stack_trace__c)&&log.stack_trace__c.length()>32767?log.stack_trace__c.substring(0,32767):log.stack_trace__c);
            try{
                insert log;
            }catch(exception e){
                Util.emailSystemError(e);
            }
        }
    }
    
    //Defect # 12560 - OCOM RWMS Changes
    public  void createOCOMBundleCommitmentHelper(string productName , ID orderID, ID accountId, map<string,integer> offerMap, Id serviceAddrId, String orderItemNumber){
        map<string,schema.recordtypeinfo> recordTypeInfoMap=webservice_integration_error_log__c.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        webservice_integration_error_log__c log=new webservice_integration_error_log__c(recordtypeid=recordTypeInfoMap.get(label.webservice_integration_log_success).getrecordtypeid(),object_name__c='SMB_Bundle_CallOutHelper',external_system_name__c='RWMS');
        Order orderToUpdate = [Select Id, RWMS_Response__c, OrderNumber from Order where Id = :orderID];
        try{
            SMB_Bundle_BundleCommExtSvcReqResp_v1.CreateBundleCommitmentResponse resBundleService = new SMB_Bundle_BundleCommExtSvcReqResp_v1.CreateBundleCommitmentResponse();
            //scpq__SciQuote__c quote = [Select scpq__QuoteId__c, smb_QuoteNumber__c  from scpq__SciQuote__c where scpq__OpportunityId__c=:orderID and scpq__Primary__c = true LIMIT 1];
            Account accountObj = [Select Name,Billing_Account_ID__c, BillingStreet, BillingState, BillingPostalCode, BillingCountry, BillingCity  from Account where Id =: accountId limit 1];
            SMBCare_Address__c serviceAddrObj;
            if(serviceAddrId != null){
                serviceAddrObj = [SELECT Province__c, Street__c, State__c, Postal_Code__c, Country__c, City__c FROM SMBCare_Address__c WHERE ID=: serviceAddrId]; 
            }
            string billingAccountNum  = accountObj.Billing_Account_ID__c;
            SMB_Bundle_BundleCommitmentCommonType_v1.CustomerInfo customerInfo = new SMB_Bundle_BundleCommitmentCommonType_v1.CustomerInfo();
            SMB_Bundle_TelusCommonAddressTypes_v5.Address billingAddress = new SMB_Bundle_TelusCommonAddressTypes_v5.Address();
            if(serviceAddrObj != null){
                billingAddress.postalZipCode = serviceAddrObj.Postal_Code__c;
                //billingAddress.provinceStateCode = serviceAddrObj.State__c;
                // 07 June 2017 Sandip - TF Case - 0245412 - Defect # 56391 - Province missing in request
                if(String.IsNotBlank(serviceAddrObj.Province__c)){
                    billingAddress.provinceStateCode = serviceAddrObj.Province__c;
                }else{
                    billingAddress.provinceStateCode = serviceAddrObj.State__c;
                }
                // End 56391 - TF Case - 0245412
                billingAddress.countryCode = serviceAddrObj.Country__c;
                billingAddress.streetName = serviceAddrObj.Street__c;
                billingAddress.municipalityName= serviceAddrObj.City__c;
            }
            customerInfo.address = billingAddress;
                
            SMB_Bundle_EnterpriseCommonName_v1.IndividualName customerName = new SMB_Bundle_EnterpriseCommonName_v1.IndividualName();
            customerName.firstName= accountObj.Name;
            customerName.lastName= '';
            customerInfo.customer = customerName;
            
            list<SMB_Bundle_BundleCommitmentCommonType_v1.WirelessOfferReference> wirelessOfferRefList = new list<SMB_Bundle_BundleCommitmentCommonType_v1.WirelessOfferReference>();
            SMB_Bundle_BundleCommitmentCommonType_v1.WirelessOfferReference worObj ;
            if(offerMap.size()>0)
            {
                for (string s :offerMap.keyset())
                {   
                    worObj = new SMB_Bundle_BundleCommitmentCommonType_v1.WirelessOfferReference();
                    worObj.wirelessOfferCode = s;
                    worObj.originalOffersAvailableNum = offerMap.get(s);
                    wirelessOfferRefList.add(worObj);       
                }
            }
    
    
            //Web service primary class object
            SMB_Bundle_BundleCommitmentExtSvc_1.BundleCommitmentExtSvc_v1_0_SOAP servicePort = new SMB_Bundle_BundleCommitmentExtSvc_1.BundleCommitmentExtSvc_v1_0_SOAP();
            
            servicePort = prepareCallout(servicePort);
                        
            //Additional variable assignments are pending for customerInfo and wirelessOfferRefList
            
            //Callout
            
            //productName = 'BA any Test';
            //String testOrdNum = 'OR-TEST1234'
            log.request_payload__c='Order :'+orderID+', Product Name:'+productName+', Service Account #:'+billingAccountNum;
            resBundleService = servicePort.createBundleCommitment(orderToUpdate.OrderNumber, productName, billingAccountNum, customerInfo, wirelessOfferRefList);
            if (string.isNotBlank(resBundleService.errorCode)){
                if ( resBundleService.errorCode == '0'){
                    orderToUpdate.RWMS_Response__c =  'Bundle registration successful with Code :'+resBundleService.errorCode;
                }else{
                    if(String.isBlank(orderToUpdate.RWMS_Response__c)){
                        orderToUpdate.RWMS_Response__c  = 'Order Item Number:' + OrderItemNumber + ' - ' + string.valueof('Bundle registration Error with Code :'+resBundleService.errorCode + ' and Message : '+resBundleService.messageList[0].message);
                    }
                }
                log.error_code_and_message__c=orderToUpdate.rwms_response__c;
                update orderToUpdate;
            }
        }catch (CalloutException ce){
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,e.getmessage()));
            log.recordtypeid=recordTypeInfoMap.get(label.webservice_integration_log_failure).getrecordtypeid();
            log.error_code_and_message__c=ce.getmessage();
            log.stack_trace__c=ce.getstacktracestring();
            log.Status__c='Failed';
            string statusMsg;
            statusMsg = 'Order Item Number:' + OrderItemNumber + ' - Bundle registration callout Failure : '+ce.getmessage();
            if (statusMsg!= null && statusMsg.length()>255){
                orderToUpdate.RWMS_Response__c = statusMsg.substring(0, 255);
            }else{
                orderToUpdate.RWMS_Response__c = statusMsg;
            }
            update orderToUpdate;
        }catch(exception e){
            log.recordtypeid=recordTypeInfoMap.get(label.webservice_integration_log_failure).getrecordtypeid();
            log.error_code_and_message__c=e.getmessage();
            log.stack_trace__c=e.getstacktracestring();
            log.Status__c='Failed';
        }finally{
            log.error_code_and_message__c=(string.isnotblank(log.error_code_and_message__c)&&log.error_code_and_message__c.length()>32767?log.error_code_and_message__c.substring(0,32767):log.error_code_and_message__c);
            log.stack_trace__c=(string.isnotblank(log.stack_trace__c)&&log.stack_trace__c.length()>32767?log.stack_trace__c.substring(0,32767):log.stack_trace__c);
            try{
                insert log;
            }catch(exception e){
                Util.emailSystemError(e);
            }
        }
    } // end Defect # 12560 
    
    /*Method to set Credentials in header of the request*/
    private SMB_Bundle_BundleCommitmentExtSvc_1.BundleCommitmentExtSvc_v1_0_SOAP prepareCallout(SMB_Bundle_BundleCommitmentExtSvc_1.BundleCommitmentExtSvc_v1_0_SOAP serport)
    {
        String creds;
        SMBCare_WebServices__c wsUsername = SMBCare_WebServices__c.getValues('username');
        SMBCare_WebServices__c wsPassword = SMBCare_WebServices__c.getValues('password');
        if (String.isNotBlank(WSusername.Value__c)&& String.isNotBlank(wsPassword.Value__c))
            creds = WSusername.Value__c+':'+wsPassword.Value__c;
        String encodedusernameandpassword = EncodingUtil.base64Encode(Blob.valueOf(creds));
        
        serPort.inputHttpHeaders_x = new Map<String, String>();
        serPort.inputHttpHeaders_x.put('Authorization','Basic ' + encodedusernameandpassword );
        
        // Adding EM Header - START
                    
            smb_cust_telus_em_header_v1_new.emRoutingRulesType er = new smb_cust_telus_em_header_v1_new.emRoutingRulesType();
            er.transport = 'http';
            er.host = 'cmassurance-ccmgmt-pt168.tmi.telus.com';
            er.port = '80';
            er.uri='/bundleCommitmentExt/BundleCommitmentExternalSvc';
            //er.envString = 'AT01'; To be left blank
            smb_cust_telus_em_header_v1_new.emHeaderType eh = new smb_cust_telus_em_header_v1_new.emHeaderType();
            eh.routingRules = er;
            serPort.emHeader = eh;
            // Adding EM Header - END
        return serPort; 
    }

}