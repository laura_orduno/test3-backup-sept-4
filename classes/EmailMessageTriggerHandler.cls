public class EmailMessageTriggerHandler {
public EmailMessageTriggerHandler(){
        if(trigger.isexecuting){
            if(trigger.isbefore){
                if(trigger.isinsert){
                     beforeInsert();
                }
            }
        }
    }

    void beforeInsert(){
        for(sObject record:trigger.new){
            EmailMessage em=(EmailMessage)record;
            string messageBody=(string.isnotblank(em.HTMLBody)?em.HTMLBody:em.TextBody);
            if(string.isNotBlank(messageBody)){    
                if(messageBody.length() < 30000){
                    em.Alerts__c = '';
                } else if(messageBody.length() >= 30000){
                    em.Alerts__c = system.label.EmailConcatenatedInHTMLViewer;
                }
            }
        }
    }
}