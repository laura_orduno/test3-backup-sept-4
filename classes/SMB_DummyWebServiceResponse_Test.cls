/*
###########################################################################
# File..................: SMB_DummyWebServiceResponse_Test
# Version...............: 1
# Created by............: Vinay Sharma
# Created Date..........: 07/03/2014
# Last Modified by......: Vinay Sharma
# Last Modified Date....: 07/03/2014
# Description...........: This class is used for creating a dummy respons of webservice in Apex Test Execution
#
# Copyright (c) 2000-2012. Astadia, Inc. All Rights Reserved.
#
# Created by the Astadia, Inc. Modification must retain the above copyright notice. 
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any commercial purpose, without fee, and without a written
# agreement from Astadia, Inc., is hereby forbidden. Any modification to source
# code, must include this paragraph and copyright.
#
# Permission is not granted to anyone to use this software for commercial uses.
#
# Contact address: 2839 Paces Ferry Road, Suite 350, Atlanta, GA 30339
# Company URL : http://www.astadia.com
###################################################9########################
###########################################################################
# Last Modified by......: Anthony Michael
# Last Modified Date....: 08-Jan-2016
############################################################################
*/
public class SMB_DummyWebServiceResponse_Test {
    public static map<string,object> responseMap;
    public SMB_DummyWebServiceResponse_Test(){
        responseMap = new map<string,object>();
    }
    public void putResponse(string responseType,object response_x){
        responseMap.put(responseType,response_x);
    }
    public static object return_Response_x;
    public static object getResponse(string className){
        
        if(responseMap <> Null && responseMap.containsKey(className)){
            return_Response_x = responseMap.get(className);
        }else{
            //creating a dummy webservice response for 'fullfil' methode of 'smb_OrderSubmit_SubmitCustOrder' class
            if(className.equalsIgnoreCase('smb_OrderSubmit_SubmitCustOrder.fullfil')){
                smb_OrderSubmit_CustomerOrderMessage.CustomerOrderMessage response_x = new smb_OrderSubmit_CustomerOrderMessage.CustomerOrderMessage();
                response_x.customerOrder = new smb_OrderSubmit_Customer_Order_v3.CustomerOrder();
                smb_OrderSubmit_base_v3.CharacteristicValue CharacteristicValue = new smb_OrderSubmit_base_v3.CharacteristicValue();
                CharacteristicValue.Value = new list<string>{'1'};
                    response_x.customerOrder.CharacteristicValue = new list<smb_OrderSubmit_base_v3.CharacteristicValue>{CharacteristicValue};
                        return_Response_x = response_x;
            }//creating a dummy webservice response for 'Ping' methode of 'SMB_Bundle_BundleCommitmentExtSvc_1' class
            else if(className.equalsIgnoreCase('SMB_Bundle_BundleCommitmentExtSvc_1.Ping')){
                SMB_Bundle_ping_v1.pingResponse_element response_x = new SMB_Bundle_ping_v1.pingResponse_element();
                response_x.version = '2.0.0.1';
                return_Response_x = response_x;
            }//creating a dummy webservice response for 'createBundleCommitment' methode of 'SMB_Bundle_BundleCommitmentExtSvc_1' class
            else if(className.equalsIgnoreCase('SMB_Bundle_BundleCommitmentExtSvc_1.createBundleCommitment')){
                SMB_Bundle_BundleCommExtSvcReqResp_v1.CreateBundleCommitmentResponse response_x = new SMB_Bundle_BundleCommExtSvcReqResp_v1.CreateBundleCommitmentResponse();
                response_x.errorCode = '0';
                return_Response_x = response_x;
            }//creating a dummy webservice response for 'searchWorkOrder' methode of 'SMB_AssignmentWFM_AssignMgmtService_2' class
            else if(className.equalsIgnoreCase('SMB_AssignmentWFM_AssignMgmtService_2.searchWorkOrder')){
                SMB_AssignmentWFM_ServiceReqRes_v2.SearchWorkOrderResponse response_x = new SMB_AssignmentWFM_ServiceReqRes_v2.SearchWorkOrderResponse();
                return_Response_x = response_x;
            }//creating a dummy webservice response for 'updateWorkOrder' methode of 'SMB_AssignmentWFM_AssignMgmtService_2' class
            else if(className.equalsIgnoreCase('SMB_AssignmentWFM_AssignMgmtService_2.updateWorkOrder')){
                SMB_AssignmentWFM_ServiceReqRes_v2.UpdateWorkOrderResponse response_x = new SMB_AssignmentWFM_ServiceReqRes_v2.UpdateWorkOrderResponse();
                return_Response_x = response_x;
            }//creating a dummy webservice response for 'createWorkOrder' methode of 'SMB_AssignmentWFM_AssignMgmtService_2' class
            else if(className.equalsIgnoreCase('SMB_AssignmentWFM_AssignMgmtService_2.createWorkOrder')){
                SMB_AssignmentWFM_ServiceReqRes_v2.CreateWorkOrderResponse response_x = new SMB_AssignmentWFM_ServiceReqRes_v2.CreateWorkOrderResponse();
                response_x.workOrder = new SMB_AssignmentWFM_OrderTypes_v2.WorkOrder();
                response_x.workOrder.workOrderId  = '1232';
                return_Response_x = response_x;
            }//creating a dummy webservice response for 'getWorkOrder' methode of 'SMB_AssignmentWFM_AssignMgmtService_2' class
            else if(className.equalsIgnoreCase('SMB_AssignmentWFM_AssignMgmtService_2.getWorkOrder')){
                SMB_AssignmentWFM_ServiceReqRes_v2.GetWorkOrderResponse response_x = new SMB_AssignmentWFM_ServiceReqRes_v2.GetWorkOrderResponse();
                return_Response_x = response_x;
            }//creating a dummy webservice response for 'searchWorkOrderList' methode of 'SMB_AssignmentWFM_AssignMgmtService_2' class
            else if(className.equalsIgnoreCase('SMB_AssignmentWFM_AssignMgmtService_2.searchWorkOrderList')){
                SMB_AssignmentWFM_ServiceReqRes_v2.SearchWorkOrderListResponse response_x = new SMB_AssignmentWFM_ServiceReqRes_v2.SearchWorkOrderListResponse();
                return_Response_x = response_x;
            }//creating a dummy webservice response for 'Ping' methode of 'SMB_AssignmentWFM_AssignMgmtService_2' class
            else if(className.equalsIgnoreCase('SMB_AssignmentWFM_AssignMgmtService_2.Ping')){
                SMB_AssignmentWFM_ping_v1.pingResponse_element response_x = new SMB_AssignmentWFM_ping_v1.pingResponse_element();
                return_Response_x = response_x;
            }//creating a dummy webservice response for 'cancelWorkOrder' methode of 'SMB_AssignmentWFM_AssignMgmtService_2' class
            else if(className.equalsIgnoreCase('SMB_AssignmentWFM_AssignMgmtService_2.cancelWorkOrder')){
                SMB_AssignmentWFM_ServiceReqRes_v2.CancelWorkOrderResponse response_x = new SMB_AssignmentWFM_ServiceReqRes_v2.CancelWorkOrderResponse();
                return_Response_x = response_x;
            }//creating a dummy webservice response for 'searchAvailableAppointmentList' methode of 'SMB_Appointment_FWAppointmentService_2' class
            else if(className.equalsIgnoreCase('SMB_Appointment_FWAppointmentService_2.searchAvailableAppointmentList')){
                SMB_Appointment_FWAppointmentSvcReqRes.SearchAvailableAppointmentListResponse response_x = new SMB_Appointment_FWAppointmentSvcReqRes.SearchAvailableAppointmentListResponse();
                return_Response_x = response_x;
            }//creating a dummy webservice response for ping methode of 'SMB_Appointment_FWAppointmentService_2' class
           
            else if(className.equalsIgnoreCase('SMB_Appointment_FWAppointmentService_2.ping')){
                SMB_Appointment_ping_v1.pingResponse_element response_x = new SMB_Appointment_ping_v1.pingResponse_element();
                response_x.version = '2.0.0.1';
                return_Response_x = response_x;
            }else if(className.equalsIgnoreCase('smb_TTODS_CalloutHelper.getTicketsByQueryCall')){
                smb_TTODS_troubleticket.TroubleTicket tt = new smb_TTODS_troubleticket.TroubleTicket();
                tt.TELUSTicketId = 'T1234567';
                tt.sourceTypeCode = 'CUSTOMER';
                tt.ticketTypeCode ='Ticket';
                tt.severityCode = '5 - MEDIUM';
                tt.priorityCode = 'In Service';
                tt.statusCode ='Open';
                tt.subStatusCode = 'Assigned';
                tt.customerDueDate = datetime.now().addDays(1);
                tt.createDate = datetime.now().addDays(-1);
                tt.closeDate = datetime.now().addDays(2);
                tt.createdByName = 'T010027';
                
                smb_TTODS_troubleticket.Contact con = new smb_TTODS_troubleticket.Contact();
                con.LastName = 'Test';
                con.FullName='Test';
                
                smb_TTODS_troubleticket.TelecommunicationsAddress telAddress = new smb_TTODS_troubleticket.TelecommunicationsAddress();
                telAddress.telephoneNumber = '1234567891';
                telAddress.telecommunicationsAddressUsageType ='CBR';
                
                con.telecommunicationsaddress = new list<smb_TTODS_troubleticket.TelecommunicationsAddress>();               
                con.telecommunicationsaddress.add(telAddress);
                
                tt.contact = new list<smb_TTODS_troubleticket.Contact>();
                tt.contact.add(con);
                smb_TTODS_troubleticket.TroubleTicketList listTT = new smb_TTODS_troubleticket.TroubleTicketList(); 
                listTT.TroubleTicket  = new list< smb_TTODS_troubleticket.TroubleTicket>{tt};
                return_Response_x = listTT ;         
            }
             else if(className.equalsIgnoreCase('cmsv3_ContractManagementSvc_3.verifyValidReplacement')){
                cmsv3_ContractManagementSvcReqRes_v3.verifyValidReplacementResponse_element response_x = new cmsv3_ContractManagementSvcReqRes_v3.verifyValidReplacementResponse_element();
                return_Response_x = response_x;
            }
            else if(className.equalsIgnoreCase('cmsv3_ContractManagementSvc_3.ping')){
                cmsv3_ping_v1.pingResponse_element response_x = new cmsv3_ping_v1.pingResponse_element();
                return_Response_x = response_x;
            }
            else if(className.equalsIgnoreCase('cmsv3_ContractManagementSvc_3.getContract')){
                cmsv3_ContractManagementSvcReqRes_v3.getContractResponse_element response_x = new cmsv3_ContractManagementSvcReqRes_v3.getContractResponse_element();
                return_Response_x = response_x;
            }
            else if(className.equalsIgnoreCase('cmsv3_ContractManagementSvc_3.replaceContractSubmission')){
                cmsv3_ContractManagementSvcReqRes_v3.replaceContractSubmissionResponse_element response_x = new cmsv3_ContractManagementSvcReqRes_v3.replaceContractSubmissionResponse_element();
                return_Response_x = response_x;
            }
             else if(className.equalsIgnoreCase('cmsv3_ContractManagementSvc_3.cancelContractSubmission')){
                cmsv3_ContractManagementSvcReqRes_v3.cancelContractSubmissionResponse_element response_x = new cmsv3_ContractManagementSvcReqRes_v3.cancelContractSubmissionResponse_element();
                return_Response_x = response_x;
            }
             else if(className.equalsIgnoreCase('cmsv3_ContractManagementSvc_3.triggerResendContract')){
                cmsv3_ContractManagementSvcReqRes_v3.triggerResendContractResponse_element response_x = new cmsv3_ContractManagementSvcReqRes_v3.triggerResendContractResponse_element();
                return_Response_x = response_x;
            }
             else if(className.equalsIgnoreCase('cmsv3_ContractManagementSvc_3.findContractsByAgentId')){
                cmsv3_ContractManagementSvcReqRes_v3.findContractsByAgentIdResponse_element response_x = new cmsv3_ContractManagementSvcReqRes_v3.findContractsByAgentIdResponse_element();
                return_Response_x = response_x;
            }
             else if(className.equalsIgnoreCase('cmsv3_ContractManagementSvc_3.getContractDocument')){
                cmsv3_ContractManagementSvcReqRes_v3.getContractDocumentResponse_element response_x = new cmsv3_ContractManagementSvcReqRes_v3.getContractDocumentResponse_element();
                return_Response_x = response_x;
            }
            else if(className.equalsIgnoreCase('cmsv3_ContractManagementSvc_3.getContractDocumentMetaData')){
                cmsv3_ContractManagementSvcReqRes_v3.getContractDocumentMetaDataResponse_element response_x = new cmsv3_ContractManagementSvcReqRes_v3.getContractDocumentMetaDataResponse_element();
                return_Response_x = response_x;
            }
            else if(className.equalsIgnoreCase('cmsv3_ContractManagementSvc_3.createContractSubmission')){
                cmsv3_ContractManagementSvcReqRes_v3.createContractSubmissionResponse_element response_x = new cmsv3_ContractManagementSvcReqRes_v3.createContractSubmissionResponse_element();
                return_Response_x = response_x;
            }
            else if(className.equalsIgnoreCase('cmsv3_ContractManagementSvc_3.calculateTerminationCharges')){
                cmsv3_ContractManagementSvcReqRes_v3.calculateTerminationChargesResponse_element response_x = new cmsv3_ContractManagementSvcReqRes_v3.calculateTerminationChargesResponse_element();
                return_Response_x = response_x;
            }
            else if(className.equalsIgnoreCase('cmsv3_ContractManagementSvc_3.findContractAmendmentsByAssociateNum')){
                cmsv3_ContractManagementSvcReqRes_v3.findContractAmendmentsByAssociateNumResponse_element response_x = new cmsv3_ContractManagementSvcReqRes_v3.findContractAmendmentsByAssociateNumResponse_element();
                return_Response_x = response_x;
            }
            else if(className.equalsIgnoreCase('cmsv3_ContractManagementSvc_3.findContractsByCustomerId')){
                cmsv3_ContractManagementSvcReqRes_v3.findContractsByCustomerIdResponse_element response_x = new cmsv3_ContractManagementSvcReqRes_v3.findContractsByCustomerIdResponse_element();
                return_Response_x = response_x;
            }
            else if(className.equalsIgnoreCase('cmsv3_ContractManagementSvc_3.findContractData')){
                cmsv3_ContractManagementSvcReqRes_v3.findContractDataResponse_element response_x = new cmsv3_ContractManagementSvcReqRes_v3.findContractDataResponse_element();
                return_Response_x = response_x;
            }
             else if(className.equalsIgnoreCase('cmsv3_ContractManagementSvc_3.updateContractSubmission')){
                cmsv3_ContractManagementSvcReqRes_v3.updateContractSubmissionResponse_element response_x = new cmsv3_ContractManagementSvcReqRes_v3.updateContractSubmissionResponse_element();
                return_Response_x = response_x;
            }
        }
        return return_Response_x;
    }
    public static void intializeMockWebservice(){
        responseMap = new map<String, object>();
        responseMap.put('SMB_Bundle_ping_v1.pingResponse_element',getResponse('SMB_Bundle_BundleCommitmentExtSvc_1.Ping'));
        responseMap.put('SMB_Bundle_BundleCommExtSvcReqResp_v1.CreateBundleCommitmentResponse',getResponse('SMB_Bundle_BundleCommitmentExtSvc_1.createBundleCommitment'));
        responseMap.put('smb_OrderSubmit_CustomerOrderMessage.CustomerOrderMessage',getResponse('smb_OrderSubmit_SubmitCustOrder.fullfil'));
        responseMap.put('SMB_AssignmentWFM_ServiceReqRes_v2.SearchWorkOrderResponse',getResponse('SMB_AssignmentWFM_AssignMgmtService_2.searchWorkOrder'));
        responseMap.put('SMB_AssignmentWFM_ServiceReqRes_v2.UpdateWorkOrderResponse',getResponse('SMB_AssignmentWFM_AssignMgmtService_2.updateWorkOrder'));
        responseMap.put('SMB_AssignmentWFM_ServiceReqRes_v2.CreateWorkOrderResponse',getResponse('SMB_AssignmentWFM_AssignMgmtService_2.createWorkOrder'));
        responseMap.put('SMB_AssignmentWFM_ServiceReqRes_v2.GetWorkOrderResponse',getResponse('SMB_AssignmentWFM_AssignMgmtService_2.getWorkOrder'));
        responseMap.put('SMB_AssignmentWFM_ServiceReqRes_v2.SearchWorkOrderListResponse',getResponse('SMB_AssignmentWFM_AssignMgmtService_2.searchWorkOrderList'));
        responseMap.put('SMB_AssignmentWFM_ping_v1.pingResponse_element',getResponse('SMB_AssignmentWFM_AssignMgmtService_2.Ping'));
        responseMap.put('SMB_AssignmentWFM_ServiceReqRes_v2.CancelWorkOrderResponse',getResponse('SMB_AssignmentWFM_AssignMgmtService_2.cancelWorkOrder'));
        responseMap.put('SMB_Appointment_FWAppointmentSvcReqRes.SearchAvailableAppointmentListResponse',getResponse('SMB_Appointment_FWAppointmentService_2.searchAvailableAppointmentList'));
        responseMap.put('SMB_Appointment_ping_v1.pingResponse_element',getResponse('SMB_Appointment_FWAppointmentService_2.ping'));
        responseMap.put('smb_TTODS_troubleticket.TroubleTicketList',getResponse('smb_TTODS_CalloutHelper.getTicketsByQueryCall'));
        
        responseMap.put('cmsv3_ContractManagementSvcReqRes_v3.verifyValidReplacementResponse_element',getResponse('cmsv3_ContractManagementSvc_3.verifyValidReplacement'));
        responseMap.put('cmsv3_ping_v1.pingResponse_element',getResponse('cmsv3_ContractManagementSvc_3.ping'));
        responseMap.put('cmsv3_ContractManagementSvcReqRes_v3.getContractResponse_element',getResponse('cmsv3_ContractManagementSvc_3.getContract'));
        responseMap.put('cmsv3_ContractManagementSvcReqRes_v3.replaceContractSubmissionResponse_element',getResponse('cmsv3_ContractManagementSvc_3.replaceContractSubmission'));
        responseMap.put('cmsv3_ContractManagementSvcReqRes_v3.cancelContractSubmissionResponse_element',getResponse('cmsv3_ContractManagementSvc_3.cancelContractSubmission'));
        responseMap.put('cmsv3_ContractManagementSvcReqRes_v3.triggerResendContractResponse_element',getResponse('cmsv3_ContractManagementSvc_3.triggerResendContract'));
        responseMap.put('cmsv3_ContractManagementSvcReqRes_v3.findContractsByAgentIdResponse_element',getResponse('cmsv3_ContractManagementSvc_3.findContractsByAgentId'));
        responseMap.put('cmsv3_ContractManagementSvcReqRes_v3.getContractDocumentResponse_element',getResponse('cmsv3_ContractManagementSvc_3.getContractDocument'));
        responseMap.put('cmsv3_ContractManagementSvcReqRes_v3.getContractDocumentMetaDataResponse_element',getResponse('cmsv3_ContractManagementSvc_3.getContractDocumentMetaData'));
        responseMap.put('cmsv3_ContractManagementSvcReqRes_v3.createContractSubmissionResponse_element',getResponse('cmsv3_ContractManagementSvc_3.createContractSubmission'));
        responseMap.put('cmsv3_ContractManagementSvcReqRes_v3.calculateTerminationChargesResponse_element',getResponse('cmsv3_ContractManagementSvc_3.calculateTerminationCharges'));
        responseMap.put('cmsv3_ContractManagementSvcReqRes_v3.findContractAmendmentsByAssociateNumResponse_element',getResponse('cmsv3_ContractManagementSvc_3.findContractAmendmentsByAssociateNum'));
        responseMap.put('cmsv3_ContractManagementSvcReqRes_v3.findContractsByCustomerIdResponse_element',getResponse('cmsv3_ContractManagementSvc_3.findContractsByCustomerId'));
        responseMap.put('cmsv3_ContractManagementSvcReqRes_v3.findContractDataResponse_element',getResponse('cmsv3_ContractManagementSvc_3.findContractData'));
        responseMap.put('cmsv3_ContractManagementSvcReqRes_v3.updateContractSubmissionResponse_element',getResponse('cmsv3_ContractManagementSvc_3.updateContractSubmission'));
        
        
        GenericWebserviceMock genericWebserviceMock = new GenericWebserviceMock(responseMap);
        Test.setMock(WebServiceMock.class, genericWebserviceMock);
        
    }
}