/**
 *  MBRCommunityInviteCtlr.cls
 *  @description  Controller extension for MBRCommunityInvite.page
 *  @date  2015-02-18
 *  @author  Alex Kong: Traction on Demand
 */
public with sharing class MBRCommunityInviteCtlr {

    private final Contact myContact;
    public String errorMsg { get; set; }
    private Boolean emailSent { get; set; }
    public String userProfileName { get; set; }
    public Boolean inviteToVITILcareEnabled { get; set; }
    string userAccountId;
    public string CatListPull { get; set; }
    public string communityIs { get; set; }
    // The extension constructor initializes the private member
    // variable myContact by using the getRecord method from the standard
    // controller.
    public MBRCommunityInviteCtlr(ApexPages.StandardController stdController) {

        inviteToVITILcareEnabled = false;
        userProfileName = '';
        this.errorMsg = '';

        this.emailSent = false;
        /*
		String qsSent = ApexPages.currentPage().getParameters().get('sent');
		this.emailSent = qsSent != 'null' && qsSent == '1' ? true : false;
		*/
        System.debug('this.emailSent: ' + this.emailSent);

        List<String> addFields = new List<String>{
                'Name', 'FirstName', 'LastName', 'Email', 'AccountId', 'CanAllowPortalSelfReg'
        };
        // can't call addFields during unit test, because test controller defines the stdController fields
        if (!Test.isRunningTest()) {
            stdController.addFields(addFields);
        }
        this.myContact = (Contact) stdController.getRecord();
        System.debug('this.myContact: ' + this.myContact);

        if (this.myContact.Email == null || this.myContact.Email == '') {
            // check if email is empty
            errorMsg = 'Contact email is empty.';
        } else {
            // grab user for this contact
            List<User> usrs = [
                    SELECT Id, Name, Profile.name, Customer_Portal_Name__c
                    FROM User
                    WHERE ContactId = :this.myContact.Id
            ];

            // check if already a user
            if (usrs.size() > 0) {
                errorMsg = 'Contact is registered for the ' + usrs[0].Customer_Portal_Name__c + ' portal.';
            } else {
                Id userId = UserInfo.getProfileId();
                Profile userProfile = [Select Name from Profile where Id = :userId LIMIT 1];
                userProfileName = userProfile.name;
                System.debug('userProfileName: ' + userProfileName);

                Map<String, VITILcare_Invite_Access_Profiles__c> vitilCareInviteAccessProfileMap = VITILcare_Invite_Access_Profiles__c.getAll();
                // set invite to VITILcare flag based on if the profile name exists or not in the map
                inviteToVITILcareEnabled = vitilCareInviteAccessProfileMap.containsKey(userProfileName);

                // make sure account is set
                if (this.myContact.AccountId == null) {
                    errorMsg = 'Contact is missing an account.';
                } else {
                    List<Account> accounts = [SELECT Id, Name, RCID__c, RecordType.Name, ParentId FROM Account WHERE Id = :this.myContact.AccountId];
                    if (accounts.size() == 0) {
                        errorMsg = 'Account is invalid';
                    } else {
                        // check for RCID record type
                        Account acc = accounts[0];
                        if (acc.RecordType.Name != 'RCID') {
                            errorMsg = 'Contact must be linked to the RCID account';
                        } else {
                            // check for RCID
                            if (acc.RCID__c == null || acc.RCID__c == '') {
                                errorMsg = 'Account is missing RCID';
                            } else {
                                // check for account parent
                                if (acc.ParentId == null) {
                                    errorMsg = 'RCID account is missing a parent CBUCID account';
                                } else {
                                    // make sure account parent is a CBUCID acc
                                    List<Account> parentAccts = [SELECT Id, Name, RecordType.Name, CBUCID_Parent_Id__c FROM Account WHERE Id = :acc.ParentId];
                                    Account parentAcct = null;
                                    if (parentAccts.size() > 0) {
                                        parentAcct = parentAccts[0];
                                    }
                                    if (parentAcct == null ||
                                            parentAcct.RecordType.Name != 'CBUCID' ||
                                            parentAcct.CBUCID_Parent_Id__c == null ||
                                            parentAcct.CBUCID_Parent_Id__c == '') {
                                        // fail
                                        errorMsg = 'RCID account is not linked to a valid CBUCID account';
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }
    }

    /*
	public String getRecordName() {
		return 'Hello ' + this.myContact.get('FirstName') + ' ' + this.myContact.get('LastName') + ' (' + this.myContact.get('Id') + ')';
	}
	*/

    public Contact getMyContact() {
        return this.myContact;
    }

    public PageReference UpdateAccount_config() {
        /*check to see if account_configuration exists and if zenoss_url is there*/

        /************************************/
        return null;
    }
    public Void sendInvite() {
        String community = Apexpages.currentPage().getParameters().get('community');
        System.debug('Entering sendInvite()?: ' + community);

        PageReference pageRef = null;
        //PageReference pageRef = Page.MBRCommunityInvite;
        //pageRef.setRedirect(true);

        if (!this.myContact.CanAllowPortalSelfReg) {
            this.myContact.CanAllowPortalSelfReg = true;
            update this.myContact;
            System.debug('updated this.myContact: ' + this.myContact);
        }

        try {
            if (community == 'vitilcare' || community == 'VITILcare_Invite' || community == 'VITILcare' || community == 'VITILcare TQ') {
                List<Account_Configuration__c> AccountConfig = [SELECT Id, Zenoss_URL__c, Account__c FROM Account_Configuration__c WHERE Account__c = :this.myContact.AccountId];
                if (AccountConfig.size() > 0) {
                    system.debug('AccountConfig[0].AccountConfig: ' + AccountConfig);
                    system.debug('AccountConfig[0].Zenoss_URL__c: ' + AccountConfig[0].Zenoss_URL__c);
                    CatListPull = AccountConfig[0].Zenoss_URL__c;
                    system.debug('CatListPull: ' + CatListPull);
                    if (CatListPull == '' || CatListPull == null) {
                        Account_Configuration__c AccountConfig1 = [SELECT Id, Zenoss_URL__c, Account__c FROM Account_Configuration__c WHERE Account__c = :this.myContact.AccountId];
                        system.debug('AccountConfig[0].AccountConfig1: ' + AccountConfig1);

                        if (AccountConfig1.Id != null || AccountConfig1.Id != '') {

                            string ZenossURL = Label.VITILcarePrfmMgmtNew;
                            AccountConfig1.Zenoss_URL__c = ZenossURL;
                            update AccountConfig1;
                        }

                    }
                } else /*account config record does not exist*/ {

                    Account_Configuration__c accountid = new Account_Configuration__c(Account__c = this.myContact.AccountId);
                    system.debug('AccountConfig[0].accountid: ' + accountid);
                    AccountConfig.add(accountid);

                    insert AccountConfig;

                    Account_Configuration__c AccountConfig1 = [SELECT Id, Zenoss_URL__c, Account__c FROM Account_Configuration__c WHERE Account__c = :this.myContact.AccountId];
                    system.debug('AccountConfig[0].AccountConfig1: ' + AccountConfig1);

                    if (AccountConfig1.Id != null || AccountConfig1.Id != '') {

                        string ZenossURL = Label.VITILcarePrfmMgmtNew;
                        AccountConfig1.Zenoss_URL__c = ZenossURL;
                        update AccountConfig1;
                    }
                }

            }
            // retrieve the email template Id
            Id templateId;
            // String templateApiName = community == 'vitilcare' ? 'VITILcare_Invite' : 'MBRInvite';
            String templateApiName = '';
            If (community == 'VITILcare') {
                templateApiName = 'VITILcare_Invite';
            }
            If (community == 'MBR') {
                templateApiName = 'MBRInvite';
            }
            If (community == 'VITILcare TQ') {
                templateApiName = 'VITILcare_TQ_Invite';
            }
            templateId = [select id, name from EmailTemplate where developername = :templateApiName].id;

            // retrieve the org-wide email address
            Id orgWideEmailId;
            for (OrgWideEmailAddress owa : [select id, DisplayName, Address from OrgWideEmailAddress]) {
                // if(community == 'vitilcare'){
                if (community == 'VITILcare' || community == 'VITILcare TQ') {
                    if (owa.Address.contains('donotreply.vitilcare')) {
                        orgWideEmailId = owa.id;
                    }
                } else {
                    if (owa.Address.contains('donotreply.mybusinessrequests')) {
                        orgWideEmailId = owa.id;
                    }
                }
            }

            // now send the mail
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTargetObjectId(this.myContact.Id);
            mail.setTemplateId(templateId);
            mail.saveAsActivity = true;
            mail.setorgWideEmailAddressId(orgWideEmailId);
            //mail.setWhatId(custom_object.Id);

            List<Messaging.SendEmailResult> sendResults = Messaging.sendEmail(new Messaging.SingleEmailMessage[]{
                    mail
            });
            this.emailSent = true;
            if (sendResults.size() > 0) {
                for (Messaging.SendEmailResult sendResult : sendResults) {
                    if (!sendResult.isSuccess()) {
                        this.emailSent = false;
                        List<Messaging.SendEmailError> sendErrors = sendResult.getErrors();
                        if (sendErrors.size() > 0) {
                            for (Messaging.SendEmailError sendError : sendErrors) {
                                this.errorMsg = sendError.getMessage();
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            // caught an exception, set errorMsg
            this.errorMsg = e.getMessage();
        }

        System.debug('this.emailSent: ' + this.emailSent);
        System.debug('this.errorMsg: ' + this.errorMsg);
        /*
		pageRef = ApexPages.currentPage();
		pageRef.getParameters().put('sent', '1');
		pageRef.setRedirect(true);
		*/

        //return pageRef;
    }

    public Boolean getEmailSent() {
        return this.emailSent;
    }
    String EInvite = null;

    public PageReference test() {
        return null;
    }

    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('MBR', 'MBR'));
        options.add(new SelectOption('VITILcare', 'VITILcare'));
        options.add(new SelectOption('VITILcare TQ', 'VITILcare TQ')); return options;
    }

    public String getEInvite() {
        return EInvite;
    }

    public void setEInvite(String EInvite) {
        this.EInvite = EInvite;
    }
    public void SetCommunity() {
        communityIs = EInvite;
    }

}