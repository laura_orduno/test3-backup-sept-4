@isTest
public class GenericMultiSelectCtrlTest {
    
    private static GenericMultiSelectCtrl controller;
    private static GenericMultiSelectCtrl.ItemSelect itemSelect;
    private static List<Product2> products;
    
    static {

        products = createProducts();
        
        PageReference pageRef = ApexPages.currentPage();
		pageRef.getParameters().put('sourceId', createDealSupport());
		pageRef.getParameters().put('sfld', 'Deal_Support__c');
		pageRef.getParameters().put('tfld', 'Product__c');
		pageRef.getParameters().put('trgobj', 'Product2');
		pageRef.getParameters().put('dfsn', 'Rate_Plan_Columns');
		pageRef.getParameters().put('junobj', 'Rate_Plan_Item__c');
		pageRef.getParameters().put('cpfs', 'Rate_Plan_Copy_Fields');
		//pageRef.getParameters().put('datafilter', 'RecordType.Name Like \'Rate Plan\'');
        pageRef.getParameters().put('shrt', 'Rate Plan');
        pageRef.getParameters().put('mrt', 'true');

        controller = new GenericMultiSelectCtrl();
        itemSelect = new GenericMultiSelectCtrl.ItemSelect(new Product2(),false);

    }
    
    private static List<Product2> createProducts(){
        List<Product2> toInsert = new List<Product2>(); 
        String[] productPlanTypes = new String[]{'Data Plan','Voice Plan'};
        id recTypeId = Util.getRecordTypeIdByName('Product2','Rate Plan');
        for(String typ : productPlanTypes){
            toInsert.add(new Product2(Name='RP'+typ, RP_Plan_Type__c=typ, RP_SOC_Code__c='XCVVA'+typ, RP_Monthly_Plan_Rate__c=20.00, RecordTypeId=recTypeId));
        } 
        insert toInsert;
        return toInsert;
    }
    
    private static Id createDealSupport(){
    	Offer_House_Demand__c offerHouse = new Offer_House_Demand__c();
        map<string,schema.recordtypeinfo> recordTypeMap=offer_house_demand__c.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        offerHouse.recordtypeid=recordTypeMap.get('WLN Deal Support').getrecordtypeid();
        offerHouse.type_of_contract__c='CCA';
        offerHouse.signature_required__c='Yes';
        insert offerHouse;
        return offerHouse.id;
    }
    
    @isTest
    static void testMissingParam() {
       ApexPages.currentPage().getParameters().put('sourceId',null);
       GenericMultiSelectCtrl controllerError1 = new GenericMultiSelectCtrl();
      
       ApexPages.currentPage().getParameters().put('datafilter', 'RecordType \'Rate Plan\''); 
       GenericMultiSelectCtrl controllerError2 = new GenericMultiSelectCtrl(); 

    }
    
     @isTest
    static void testInvalidFilter() {
       ApexPages.currentPage().getParameters().put('datafilter', 'RecordType \'Rate Plan\''); 
       GenericMultiSelectCtrl controllerError2 = new GenericMultiSelectCtrl();

    }
    
    
    @isTest
    static void doFilterPlan_Type() {     
        Test.startTest();
        
        controller.selectedRecType = Util.getRecordTypeIdByName('Product2','Rate Plan');
        controller.targetObjInstance.put('RP_Plan_Type__c','Data Plan');
		controller.doFilter();
        
        Test.StopTest();
    } 
    
    @isTest
    static void doFilterSOC_Code() {     
        Test.startTest();
        
        controller.selectedRecType = Util.getRecordTypeIdByName('Product2','Rate Plan');
        controller.targetObjInstance.put('RP_SOC_Code__c','XCVVAVoice Plan');
		controller.doFilter();
        Test.StopTest();
    } 

    @isTest
    static void doFilterPlan_Rate() {     
        Test.startTest();
        
        controller.selectedRecType = Util.getRecordTypeIdByName('Product2','Rate Plan');
        controller.targetObjInstance.put('RP_Monthly_Plan_Rate__c',20.00);
		controller.doFilter();
        Test.StopTest();
    } 
    
    @isTest
    static void testgetMethods() {     
       
        controller.getSelectedItemList();
        controller.getRecordTypeList();
       
    } 
    
    @isTest
    static void testActionMethods() {     
        
        List<GenericMultiSelectCtrl.ItemSelect> items = controller.getdisplayItems();
        for (GenericMultiSelectCtrl.ItemSelect item : items){
            item.selected = true;
        }
        controller.addSelected();
        for (Product2 product : products) {
           controller.selectedItemMap.put(product.id,product); 
        }
        controller.saveSelected();
        controller.cancel();
        controller.getdisplayItems();
        controller.clearSelected();
    } 
    
    @isTest
     static void testPagination() {     
        Test.startTest();
        GenericPagination p = new GenericPagination('select name from account limit 1'); 
        controller.paginator.first();
        controller.paginator.last();
        controller.paginator.previous();
        controller.paginator.next();
        controller.paginator.getSize();
        controller.paginator.getNoOfRecords();
        controller.paginator.getInfo() ;
         try{
             GenericPagination perror = new GenericPagination('select name from accounta'); 
         }catch(GenericPagination.PaginationException e) {
             
         }
        Test.StopTest();
    } 
    
}