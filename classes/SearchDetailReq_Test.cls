@isTest
private class SearchDetailReq_Test {

    static testMethod void TestDetailReqSearch() 
    {
    	PageReference pageRef = Page.assignDetailedRequirements;
        Test.setCurrentPage(pageRef);	
        Astadia_Component__c cComponent = new Astadia_Component__c();
    	Apexpages.Standardcontroller sController = new Apexpages.Standardcontroller(cComponent);
    	AssignDetailedRequirementsController tController = new AssignDetailedRequirementsController(sController);

    	Datetime myDT = Datetime.now();
    	 List<Astadia_Detailed_Requirements__c> lstDetailReq = new List<Astadia_Detailed_Requirements__c>();
        Astadia_project__c P1 =new Astadia_project__c( name ='Sample Project', Start_Date__c= datetime.now().Date(), End_Date__c= datetime.now().Date(),Topics__c='Sample Topic' );
		
		insert P1;
		
		Astadia_High_Level_Requirement__c H1 = new Astadia_High_Level_Requirement__c(name='Sample HLevel Req',Process_Area__c='Lead to Quote', Project__c = P1.Id);
		insert H1;
		
		Astadia_Component__c tempC1 = new Astadia_Component__c(name='Sample Component',project__c = P1.ID);
		insert tempC1;
		Astadia_Component__c C2 = [Select id,name from Astadia_Component__c where ID =:tempC1.Id];
		
		Astadia_Detailed_Requirements__c D1 = new Astadia_Detailed_Requirements__c
		(Requirement_Name__c='Sample D req',
		High_Level_Requirement__c = H1.Id,
		Type__c='Reporting',
		Source__c= 'Test',
		Business_Group__c = 'Finance',
		Requirement_Description__c='Test Desc',
		Primary_Function_or_Object__c='Account');
		insert D1;
		
		DetReqBuildComp__c DRBC1 = new DetReqBuildComp__c (Detailed_Requirement__c = D1.Id, Build_Component__c = tempC1.Id);
		insert DRBC1;
		
		tController.strProject = P1.Id;
		tController.strHLReq =  H1.ID;
		tController.componentId = tempC1.Id;
		tController.ReqType = 'Reporting';
		tController.businessGroup ='Finance';
		tController.strFunctionOrObject=  'Account';
		tController.assignedCheck = false;
		tController.getProjectNames();
		tController.getHighLevelRequirements();
		tController.getFunctionListData();
 		pageRef = tController.searchRecords();
 		tController.getResults();
 		tController.AssignToComponent();
 		tController.getComponentNameForButton();
 		tController.Cancel();
 		tController.Reset();		
		
    }
    
    static testMethod void TestDetailReqSearch2() 
    {
    	PageReference pageRef = Page.assignDetailedRequirements;
        Test.setCurrentPage(pageRef);	
        Astadia_Component__c cComponent = new Astadia_Component__c();
    	Apexpages.Standardcontroller sController = new Apexpages.Standardcontroller(cComponent);
    	AssignDetailedRequirementsController tController = new AssignDetailedRequirementsController(sController);

    	Datetime myDT = Datetime.now();
    	 List<Astadia_Detailed_Requirements__c> lstDetailReq = new List<Astadia_Detailed_Requirements__c>();
        Astadia_project__c P1 =new Astadia_project__c( name ='Sample Project', Start_Date__c= datetime.now().Date(), End_Date__c= datetime.now().Date(),Topics__c='Sample Topic' );
		
		insert P1;
		
		Astadia_High_Level_Requirement__c H1 = new Astadia_High_Level_Requirement__c(name='Sample HLevel Req',Process_Area__c='Lead to Quote', Project__c = P1.Id);
		insert H1;
		
		Astadia_Component__c tempC1 = new Astadia_Component__c(name='Sample Component',project__c = P1.ID);
		insert tempC1;
		Astadia_Component__c C2 = [Select id,name from Astadia_Component__c where ID =:tempC1.Id];
		
		Astadia_Detailed_Requirements__c D1 = new Astadia_Detailed_Requirements__c
		(Requirement_Name__c='Sample D req',
		High_Level_Requirement__c = H1.Id,
		Type__c='Reporting',
		Source__c= 'Test',
		Business_Group__c = 'Finance',
		Requirement_Description__c='Test Desc',
		Primary_Function_or_Object__c='Account');
		insert D1;
		
		DetReqBuildComp__c DRBC1 = new DetReqBuildComp__c (Detailed_Requirement__c = D1.Id, Build_Component__c = tempC1.Id);
		insert DRBC1;
		
		tController.strProject = P1.Id;
		tController.strHLReq =  H1.ID;
		tController.componentId = tempC1.Id;
		tController.ReqType = 'Reporting';
		tController.businessGroup ='Finance';
		tController.strFunctionOrObject=  'Account';
		tController.assignedCheck = true;
		tController.getProjectNames();
		tController.getHighLevelRequirements();
		tController.getFunctionListData();
 		pageRef = tController.searchRecords();
 		tController.getResults();
 		tController.AssignToComponent();
 		tController.getComponentNameForButton();
 		tController.Cancel();
 		tController.Reset();		
		
    }
    
}