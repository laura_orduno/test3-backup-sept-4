/********************************************************************************************************************************
Class Name:     OC_SelectAndNewAddressController 
Purpose:        Controller for OC_SelectAddress and OC_NewAddress VFPages       
TestClass Name: OC_SelectAndNewAddressController_Test
Created by:     Aditya Jamwal(IBM)     19-June-2017     
*********************************************************************************************************************************
Added global access to use VF remoting method
*/
global class OC_SelectAndNewAddressController {
    public String rid{get;set;}
    public String rcidName{get;set;}
    public String parentOrderId{get;set;}
    public String contactId {get;set;}
    public String primaryContactId {get;set;} // RTA - 616
    public ServiceAddressData SAData {get;set;}  
    public SMBCare_Address__c smbCareAddr {get;set;}
    public Id smbCareAddrId {get;set;}
    public Account serviceAccount {get;set;}
    public Boolean displayPageComp{get;set;}
    public List<String> errorMessages{get;set;}
    public String orderType{get;set;}
    public ServiceAddressData moveOutSAData {get;set;}
    public String dropExceptionCode { get; set; }
    public Boolean isASFDown{get;set;} 
    public Boolean blnGTACall = False; 
    private String caseIds;
    public String ratingNpaNxx {get; set;}
    public String COID {get; set;}
    
    
    public static String orderPath{get;set;}
    public static String showHeader{get;set;}
    public String oppId {get;set;} // RTA - 617 fix
    public  Boolean showErrorMsg{
        get{
            return (errorMessages.size() > 0);
        }
        set;}
    
    public List<SelectOption> getOrderPathValues() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Simple','Simple'));
        options.add(new SelectOption('Complex','Complex'));
        return options;
    }
    public OC_SelectAndNewAddressController() {
        isASFDown = false;
        errorMessages = new List<String>();
        SAData = new ServiceAddressData();
        rid = apexpages.currentpage().getparameters().get('rcid');
        parentOrderId = apexpages.currentpage().getparameters().get('parentOrderId');
        if(String.isBlank(rid)){
            Order ordObj=[select accountid from Order where id=:parentOrderId];
            rid=ordObj.accountid;
        }
        primaryContactId = apexpages.currentpage().getparameters().get('primaryContactId'); // RTA - 616 fix
        //RTA 616 Fix
        if(primaryContactId==null || primaryContactId=='null'){
            primaryContactId='';
        }else{
            try{
               ID sID = primaryContactId;
               primaryContactId = sID; 
            } catch (System.StringException e){}
        }
        // End RTA 616 Fix
        oppId=System.currentPageReference().getParameters().get('oppId'); // RTA - 617 fix
        //RTA 617 Fix
        if(oppId==null || oppId=='null'){
            oppId='';
        }
        // End RTA 617 Fix
        orderPath = 'Simple';
        system.debug(' @@ inside CreateServiceAccount -1 '+' '+rid + ' '+parentOrderId);
        if(parentOrderId != null && parentOrderId != ''){
            displayPageComp =true;
        }else {
            displayPageComp =false;
        }
        
        if(string.isNotBlank(rid)){
          List<account> acclist =  [select id,name from account where id =:rid];
            if(null != acclist && acclist.size()>0){
               rcidName = acclist[0].name;
            }
        }

        showHeader = apexpages.currentpage().getparameters().get('showHeader');
        if(string.isBlank(showHeader)){
            showHeader = 'false';
        }
        
    }
    
 public  void CreateServiceAccount(){
  String addressType = System.currentPageReference().getParameters().get('addressType');
     system.debug(' @@ inside CreateServiceAccount '+addressType+' '+rid + ' '+parentOrderId);
     SMBCare_Address__c newAddressRecord = CreateServiceAccount(addressType,rid);             
 }
 public SMBCare_Address__c CreateServiceAccount(String addressType ,String RCID){
           if(String.isBlank(RCID) || !( RCID instanceof Id)){
               RCID=null;
           }
        try {
             errorMessages = new List<String>();
            system.debug('**********addressType='+addressType);
            ServiceAddressData svcData;
            Account svcAccount;
            system.debug('*********SAData =' + SAData);
            if (addressType == 'MOVE_OUT') {
          //      svcData = moveOutSAData;
            } else {
                svcData = SAData;
            }
            
            isASFDown = svcData.isASFDown; 
            Id accountId = null;
            if(String.isNotBlank(RCID) && RCID instanceof Id){
                accountId=Id.valueOf(RCID);
            }
                       
            SMBCare_Address__c addressRecord = new SMBCare_Address__c();
            addressRecord.Address_Type__c = 'Standard'; // Service Address Type defaults to Standard.
            addressRecord.recordtypeid = Schema.SObjectType.SMBCare_Address__c.getRecordTypeInfosByName().get('Address').getRecordTypeId();
            
            OCOM_MACD_ServiceAccountAndAddressExt ext= new OCOM_MACD_ServiceAccountAndAddressExt();
            System.debug('debugging RCID='+RCID);
            if (String.isNotBlank(RCID) && RCID instanceof Id){
                if (svcData.isManualCapture) {
                    
                    if (Country_Province_Codes__c.getValues(SAData.province) != null && Country_Province_Codes__c.getValues(svcData.province).is_ILEC__c) {
                        svcData.isILEC = true;
                    } else {
                        svcData.isILEC = false;
                        //svcData.locationId = OrdrUtilities.generateUniqueHashCode();
                        //Needs to update new method from stage
                        svcData.locationId = OrdrUtilities.generateUniqueHashCode(svcData.address,svcData.province,svcData.city,svcData.postalCode,svcData.country);
                    }                    
                    svcAccount= ext.CreateServiceAccount(RCID, addressRecord, svcData);                    
                } else {
                    // Start TF Case 03359357, JIRA defect BSBD_RTA-1272
                    //svcData.isILEC = true;
                    if(String.IsNotBlank(SAData.locationId) && SAData.province == 'QC'){
                    	svcData.isILEC = false;
                    }else{
                        svcData.isILEC = true;
                    }
                    // End TF Case 03359357, JIRA defect BSBD_RTA-1272
                    svcAccount = ext.CreateServiceAccount(RCID, addressRecord, svcData);

                    addressRecord = [Select Service_Account_Id__c,id, Account__c, FMS_Address_ID__c, Status__c,Postal_Code__c , 
                                     Street_Number__c, city__c , Street__c, Street_Name__c, Street_Direction_Code__c , Suite_Number__c , 
                                     Province__c, COID__c , Building_Number__c , Country__c,  State__c, Civic_Number__c, Street_Type_Code__c , 
                                     Location_Id__c , Address__c , Address_Type__c ,  LocalRoutingNumber__c , NPA_NXX__c , SwitchNumber__c , 
                                     TerminalNumber__c, Service_Account_Id__r.RecordTypeId ,AddressText__c
                                     from SMBCare_address__c 
                                     where Service_Account_Id__c = :svcAccount.Id];                    
              
                }
                if (addressType == 'MOVE_OUT') {
              //      moveOutAddr = addressRecord;
             //       moveOutServiceAccount = svcAccount;
                } else {
                    smbCareAddr = addressRecord;
                    serviceAccount = svcAccount;
                }  
                smbCareAddrId = smbCareAddr.Id;
            return smbCareAddr;
            }
        } catch (Exception e) {
            system.debug(Logginglevel.ERROR, e.getMessage());
            errorMessages.add(Label.ACCT0001);
            System.debug(e.getStackTraceString());
        }
      return null;  
    }
    
    public PageReference RedirectToSelectAddressPage(){
        string rcid = apexpages.currentpage().getparameters().get('rcid');
        string parentId =  apexpages.currentpage().getparameters().get('parentOrderId');
        string showHeader = apexpages.currentpage().getparameters().get('showHeader');
        system.debug(' in RedirectToSelectAddressPage ' + rcid + ' '+parentId);
     
        PageReference pr = new PageReference('/apex/OC_SelectAddress?rcid=' + rcid 
                                      + '&parentOrderId='+ parentId
                                      + '&showHeader=' + showHeader
                                      + '&oppId=' + oppId
                                      + '&primaryContactId='+primaryContactId);
            pr.setRedirect(true);
            return  pr; 
    }
    
    public PageReference RedirectToContactPage(){
        
        processManualAddress();
        
        if(contactId == null){
           contactId = apexpages.currentpage().getparameters().get('contactId'); 
          }
        
            system.debug('***********!orderId:' + parentOrderId);
            system.debug('***********!typeOfOrder:' + orderType);
            system.debug('***********!smbCareAddr:' + smbCareAddr);
            system.debug('***********!contactId:' + contactId);
            system.debug('***********!caseIds:' + caseIds);

           PageReference pr = new PageReference('/apex/OC_SelectContact?parentOrderId=' + parentOrderId 
                                      + '&rcid=' + rid 
                                      + '&contactId=' + contactId
                                      + '&caseIds=' + caseIds
                                      + '&showHeader=' + showHeader
                                      + '&oppId=' + oppId
                                      + '&primaryContactId='+primaryContactId
                                     );
        pr.setRedirect(true);
            return  pr;
    }
    
    public PageReference RedirectToNewAddressPage(){
      
      string rcid = apexpages.currentpage().getparameters().get('rcid');
      string parentId =  apexpages.currentpage().getparameters().get('parentOrderId');
      string orderPath =  apexpages.currentpage().getparameters().get('orderPath');
      string showHeader = apexpages.currentpage().getparameters().get('showHeader');

      system.debug(' in RedirectToSelectAddressPage ' + rcid + ' '+parentId+' '+orderPath);
      PageReference pr;

        if (String.isNotBlank(orderPath) && orderPath.equalsignoreCase('Complex')) {

          pr = new PageReference('/apex/smb_newQuoteFromAccount?aid='+rcid+'&OrderPath='+orderPath+'&isdtp=vw'
                                      + '&showHeader=' + showHeader);

        }else{
             
          pr = new PageReference('/apex/OC_NewAddress?rcid=' + rcid 
                                      + '&parentOrderId='+ parentId
                                      + '&showHeader=' + showHeader
                                	  + '&oppId=' + oppId
                                      + '&primaryContactId='+primaryContactId);
        }
            pr.setRedirect(true);
            return  pr;
        }  
    
    @TestVisible    
    private void processManualAddress(){
        system.debug('*****processManualAddress');
        system.debug('SAData.isManualCapture=' + SAData.isManualCapture);
        system.debug('SAData.isSACG=' +  SAData.isSACG);
        system.debug('moveOutSAData.isManualCapture=' + SAData.isManualCapture);
        system.debug('moveOutSAData.isSACG=' +  SAData.isSACG);
        
        if (SAData.isManualCapture) {
            // Sandip - 15 March 2017 - Defect 11440
            if(Country_Province_Codes__c.getValues(SAData.province) != null){
                SAData.isILEC = Country_Province_Codes__c.getValues(SAData.province).is_ILEC__c;
                if (SAData.isILEC) SAData.isSACG = true;
            } // End Defect 11440
            CreateServiceAccount('CREATE',rid);
            //future call
            OrdrProductEligibilityManager.cacheAvailableOffers(smbCareAddr.Id);
        }
      
        List<ServiceAddressData> svcAddrData = new List<ServiceAddressData>();
        if (SAData.isSACG) {
            svcAddrData.add(SAData);
        }
        
        if (svcAddrData.size() > 0) {
            system.debug('*****sendToSACG');
            PACUtility utilObj = new PACUtility();
            utilObj.sendToSACG(svcAddrData);
        }
        
        for (ServiceAddressData sData : svcAddrData) {
            if (String.isNotBlank(sData.caseId)) {
                caseIds += ':' + sData.caseId;
            }
        }
        if(String.isNotBlank(caseIds)){
            caseIds.removeStart(':');
        }
    } 
    
    @RemoteAction
    global static boolean isILEC(String province){
        Boolean isILEC = false;
        if (Country_Province_Codes__c.getValues(province) != null && Country_Province_Codes__c.getValues(province).is_ILEC__c) {
            isILEC = true;
        } else {
            isILEC = false;
        }
        return isILEC;
    }
}