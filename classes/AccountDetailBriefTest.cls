@isTest
public class AccountDetailBriefTest {

    private static testMethod void testAccountDetailBrief () {
        
        AccountDetailBriefController accountBriefController = new AccountDetailBriefController();

        Account acc= new Account(
            Name = 'RCID Test',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            RCID__c = '987654'
        );

        insert acc; 

        acc = [select Id, Name, BillingCity from account where id=:acc.id];
        
        accountBriefController.acctId = acc.id;
        
        System.assertEquals(accountBriefController.acct.BillingCity,acc.BillingCity);
        
    }
    
    
}