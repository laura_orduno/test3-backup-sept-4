/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class smb_CPQ_OpportunityProductExtTest {

    static testMethod void myUnitTest() {
        PageReference pageref = Page.SMB_OrderSuccess;
        Test.setCurrentPage(pageref);

        Test.startTest();
        
        Opportunity testOpp = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        insert testOpp;
        
        smb_test_utility.createPQLI('PQL_Test_Data',testOpp.Id);
        
        list<scpq__PrimaryQuoteLine__c> pqli = [select id from scpq__PrimaryQuoteLine__c where scpq__OpportunityId__c =:testOpp.Id];
        system.assert(pqli.size()>0); 
        
        list<Provisioning__c> prov = [select id from Provisioning__c where Parent_Order__c=:testOpp.Id];
        system.assert(prov.size()>0);
        
        list<OpportunityLineItem> testOLi = [select id, opportunityId from OpportunityLineItem where opportunityId =:testOpp.id limit 1]; 
        //system.assert(testOLi.size() + prov.size()== pqli.size());
        system.assert(testOLi.size()>0);
        
        
        ApexPages.StandardController sc = new ApexPages.standardController(testOLi[0]);
        smb_CPQ_OpportunityProductExt contToTest = new smb_CPQ_OpportunityProductExt(sc);

        contToTest.getOppz();
        Test.stopTest();
    }
}