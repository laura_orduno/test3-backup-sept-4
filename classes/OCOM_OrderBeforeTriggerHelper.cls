/*
*******************************************************************************************************************************
Class Name:     OCOM_OrderBeforeTriggerHelper
Purpose:        Helper class handling Trigger functionality of Order object. 
Class methods are used in OCOM_OrderBeforeTrigger trigger.     
Created by:     Aditya Jamwal     16-Jan-2017   QC-6481 No previous version
Modified by:    Roderick de Vera  14-Aug-2017   BMPF-3
Modified by:    Aditya Jamwal     28 Aug 2017   BMPF-40 CAR Logic to Support Single CAR for Parent/Child Orders
*******************************************************************************************************************************
*/
public class OCOM_OrderBeforeTriggerHelper {
    
    // Replacement for OCOM Credit - Linking Order and CARs (Product Info) Process Builder  QC-6481
    public static Boolean OrderBeforeTrigger_Fired = false;
    //public static Boolean byPassTrigger=false;
    public static Pricebook2 defaultPricebook=null;
    public static List<Order> creditRelOrdList=null;
    public static List<OrderItem> creditRelOIList=null;
    static{
        List<Pricebook2> pList=[select id,name from Pricebook2 where name='OCOM Pricebook' and vlocity_cmt__IsOrderDefault__c=true and isActive=true];
        if(!pList.isEmpty()){
           defaultPricebook= pList.get(0);
        }
        
    }
    public static void setPriceBook(Boolean isBefore, Boolean isInsert,List<Order> newOrderList){
        
        if(isBefore && isInsert){
            for(Order inst:newOrderList){
                if(String.isBlank(inst.pricebook2id) && !Test.isRunningTest()){
                    inst.pricebook2id=defaultPricebook.id;
                }
            }
        }
    }
    public static void linkingOrderAndCARProductInfo(Boolean isBefore, Boolean isInsert, Boolean isUpdate,Map<Id,Order> oldOrderMap,List<Order> newOrderList){
        if(isBefore) {                                           
            Set<String> withNewCAROrderIdSet = new Set<String>();
            Set<String> relatedOrderIdSet = new Set<String>();                                            
            Map<String,String> orderIdwithCARdMap = new Map<String,String>();
            List<OrderItem> updateOrderItemList = new  List<OrderItem>();   
            Set<String> CARIdSet = new Set<String>();    
            List<Order> updateRelatedOrder = new List<Order>();
            system.debug('@@@@Inside Order Helper');                                             
            for(order newOrder : newOrderList){
                
                Order oldOrder; 
                if(isUpdate){
                    oldOrder = oldOrderMap.get(newOrder.id);
                }            
                
                if(( null == oldOrder  || oldOrder.Credit_Assessment__c != newOrder.Credit_Assessment__c) && String.isNotBlank(newOrder.Credit_Assessment__c)){
                    system.debug('@@@@Inside Order Helper first Loop'); 
                    withNewCAROrderIdSet.add(newOrder.Id);
                    if(String.isNotBlank(newOrder.Related_Order__c)){
                        relatedOrderIdSet.add(newOrder.Related_Order__c); 
                        orderIdwithCARdMap.put(newOrder.Related_Order__c,newOrder.Credit_Assessment__c); //Add relatedOrder Id with current Order CAR Id
                    }                  
                    orderIdwithCARdMap.put(newOrder.Id,newOrder.Credit_Assessment__c); //Add current Order Id with it's CAR Id
                    CARIdSet.add(newOrder.Credit_Assessment__c);                
                }
                if(String.isNotBlank(newOrder.Related_Order__c) && ( null == oldOrder  || oldOrder.Credit_Assessment__c != newOrder.Credit_Assessment__c)){                   
                    relatedOrderIdSet.add(newOrder.Related_Order__c);
                    
                }
            }
            
            //First Condition,Step 3 update related orders with Blank CAR
            if(Test.isRunningTest()){
                if(creditRelOrdList==null){
                    creditRelOrdList=  [Select id,Credit_Assessment__c,Related_Order__c from Order where Related_Order__c = :withNewCAROrderIdSet OR id = :relatedOrderIdSet];                                        
                }                
            }  else {
                creditRelOrdList=  [Select id,Credit_Assessment__c,Related_Order__c from Order where Related_Order__c = :withNewCAROrderIdSet OR id = :relatedOrderIdSet];                                        
            }
          
        for(Order relOrder : creditRelOrdList){
                if(String.isBlank(relOrder.Credit_Assessment__c ) && null != orderIdwithCARdMap) {
                    if(null != orderIdwithCARdMap.get(relOrder.Related_Order__c)){
                        relOrder.Credit_Assessment__c = orderIdwithCARdMap.get(relOrder.Related_Order__c);  
                        updateRelatedOrder.add(relOrder);  
                    }else if(null != orderIdwithCARdMap.get(relOrder.Id)){
                        relOrder.Credit_Assessment__c = orderIdwithCARdMap.get(relOrder.Id);  
                        updateRelatedOrder.add(relOrder);  
                    }
                }                                             
            }                                             
            //First Condition,Step 1 :Update Product Lines  
            if(withNewCAROrderIdSet!=null && withNewCAROrderIdSet.size() > 0) {
                // RTA - 647 - added status in query to check Cancelled condition
                if(Test.isRunningTest()){
                    if(creditRelOIList==null){
                        creditRelOIList=[Select id,Credit_Assessment__c,orderid,order.parentId__c, order.status from orderItem where orderid IN :withNewCAROrderIdSet OR order.parentId__c IN :withNewCAROrderIdSet];
                    }
                
                } else {
                    creditRelOIList=[Select id,Credit_Assessment__c,orderid,order.parentId__c, order.status from orderItem where orderid IN :withNewCAROrderIdSet OR order.parentId__c IN :withNewCAROrderIdSet];
                }
                
                for(orderItem oli : creditRelOIList){
                	if(oli.order.status != 'Cancelled'){
                        system.debug('@@@@Inside Order Helper updating OLIs'); 
                        if(null !=  orderIdwithCARdMap && ( null != orderIdwithCARdMap.get(oli.orderid) && oli.Credit_Assessment__c != orderIdwithCARdMap.get(oli.orderid)
                                                           || null != orderIdwithCARdMap.get(oli.order.parentId__c) && oli.Credit_Assessment__c != orderIdwithCARdMap.get(oli.order.parentId__c)
                                                          )
                          ) {
                             system.debug('@@@@Inside Order Helper updating OLIs If loop orderItem id: '+oli.id+' status :'+oli.order.status);  
                                //Check added for BMPF-40 CAR on master order (order 2.0)
                                if(String.isNotBlank(oli.order.parentId__c)){
                                    oli.Credit_Assessment__c = orderIdwithCARdMap.get(oli.order.parentId__c); 
                                }else{
                            oli.Credit_Assessment__c = orderIdwithCARdMap.get(oli.orderid);
                                }                        
                            updateOrderItemList.add(oli);
                		}
                	}
            	}
            }
            
            //First Condition  
            Map<id,Credit_Assessment__c> carIDToRecordMap = new Map<id,Credit_Assessment__c>([Select id,Name from Credit_Assessment__c where id IN :CARIdSet]);  
            for(order newOrder : newOrderList){
                if(null != newOrder.Credit_Assessment_ID__c && null != carIDToRecordMap && null!= carIDToRecordMap.get(newOrder.Credit_Assessment_ID__c)){
                    newOrder.Credit_Reference_No__c  = carIDToRecordMap.get(newOrder.Credit_Assessment_ID__c).Name;
                }
            } 
            
            if(null!= updateRelatedOrder && updateRelatedOrder.size()>0){
                update updateRelatedOrder;                                             
            }
            
            if(null != updateOrderItemList && updateOrderItemList.size()>0){
                system.debug('@@@@Inside Order Helper updating OLIs '+updateOrderItemList); 
                update updateOrderItemList;                                              
            }
            
          }  
            
    }
    
    //BMPF-3
    public static void generateSubOrderNumber(Boolean isInsert, Boolean isUpdate, List<Order> newOrders, Map<Id, Order> oldOrders) {

        //get all parent orders
        List<String> parentIdList = new List<String>();
        for (Order orderInst : newOrders) {
           if (String.isBlank(orderInst.ParentId__c)) {
               orderInst.name=orderInst.OrderNumber;
           }
            
            if (String.isNotBlank(orderInst.ParentId__c)) {
	            parentIdList.add(orderInst.ParentId__c);
            }
        }
        if(parentIdList.isEmpty()){
            return;
        }
        system.debug('parentIdList=' + parentIdList);
        
        //put parent order number in a searchable map
        Map<Id, Order> orderNumberLookup = new Map<Id, Order>([SELECT Id, OrderNumber FROM Order WHERE Id in :parentIdList]);
        system.debug('orderNumberLookup=' + orderNumberLookup);
        
        AggregateResult[] groupedResults = [SELECT ParentId__c, max(sub_order_number__c) lastCount FROM Order WHERE ParentId__c in :orderNumberLookup.keySet() GROUP BY ParentId__c];
        //set dictionary for the initial child count
        Map<String, Integer> childOrderCount = new Map<String, Integer>();
        for (AggregateResult ar : groupedResults) {
            String subOrder = (String)ar.get('lastCount');
            Integer lastCount = 0;
            if (String.isNotBlank(subOrder) && subOrder.contains('.')) {
                lastCount = Integer.valueOf(subOrder.split('\\.')[1]);
            }
			system.debug('lastCount=' + lastCount);
            childOrderCount.put((String)ar.get('ParentId__c'), lastCount);
        }
        system.debug('childOrderCount=' + childOrderCount);
        
        for (Order newOrder : newOrders) {
            if (isInsert) {
                if (String.isNotBlank(newOrder.ParentId__c)) {
                    String orderNumber = orderNumberLookup.get(newOrder.ParentId__c).OrderNumber;
                    Integer childCounter = 1;
                    if (childOrderCount.containsKey(newOrder.ParentId__c)) {
                    	childCounter = childOrderCount.get(newOrder.ParentId__c) + 1;
                    }
                    childOrderCount.put(newOrder.ParentId__c, childCounter);
                    newOrder.Sub_Order_Number__c = orderNumber + '.' + String.valueOf(childCounter).leftPad(4, '0');
                    newOrder.name=newOrder.Sub_Order_Number__c;
                }
            } else if (isUpdate) {
                Order oldOrder = oldOrders.get(newOrder.Id);
                if (newOrder.ParentId__c != oldOrder.ParentId__c) {
                    if (String.isNotBlank(newOrder.ParentId__c)) {
                        String orderNumber = orderNumberLookup.get(newOrder.ParentId__c).OrderNumber;
                        Integer childCounter = 1;
                        if(null != childOrderCount.get(newOrder.ParentId__c) ){
                           childCounter = childOrderCount.get(newOrder.ParentId__c) + 1;
                        }
                        childOrderCount.put(newOrder.ParentId__c, childCounter);
                        newOrder.Sub_Order_Number__c = orderNumber + '.' + String.valueOf(childCounter).leftPad(4, '0');
                        newOrder.name=newOrder.Sub_Order_Number__c;
                    } else {
                        newOrder.Sub_Order_Number__c = null;
                        newOrder.name=null;
                    }
                } else {
                    if (String.isNotBlank(newOrder.ParentId__c)) {
                        if (String.isBlank(oldOrder.Sub_Order_Number__c)) {
                            String orderNumber = orderNumberLookup.get(newOrder.ParentId__c).OrderNumber;
                            system.debug('orderNumber='+orderNumber);
                            Integer childCounter = childOrderCount.get(newOrder.ParentId__c) + 1;
                            childOrderCount.put(newOrder.ParentId__c, childCounter);
                            system.debug('childCounter='+childCounter);
                            newOrder.Sub_Order_Number__c = orderNumber + '.' + String.valueOf(childCounter).leftPad(4, '0');
                            newOrder.name=newOrder.Sub_Order_Number__c;
                        } else {
                            newOrder.Sub_Order_Number__c = oldOrder.Sub_Order_Number__c;
                            newOrder.name=newOrder.Sub_Order_Number__c;
                        }
                    } else {
                        newOrder.Sub_Order_Number__c = null;
                        newOrder.name=newOrder.Sub_Order_Number__c;
                    }
                    
                }
            }
        }
    }    

// start BMPF-40
    public static void updateTotalMRCAndNRCOnParentOrder(Boolean isBefore, Boolean isInsert, Boolean isUpdate,Map<Id,Order> oldOrderMap,List<Order> newOrderList, Map<Id,Order> newOrderMap){
        
            Set<String> parentOrderIdsSet = new Set<String>();
            Map<String,double> totalMRConParentOrderMap = new Map<String,double>();
            Map<String,double> totalNRConParentOrderMap = new Map<String,double>();
            Map<String,double> totalMRCDiscountonParentOrderMap = new Map<String,double>();
            Map<String,double> totalNRCDiscountonParentOrderMap = new Map<String,double>();

        if(isBefore && isUpdate && (!OrderBeforeTrigger_Fired || Test.isRunningTest())){
             for(Order ord : newOrderList){
                 // check if it a child order
                 if(String.isNotBlank(ord.parentId__c)){
                     parentOrderIdsSet.add(ord.parentId__c);
                 }
             }

            if(null != parentOrderIdsSet && parentOrderIdsSet.size() > 0){

                List<Order> updateParentOrderList = new List<Order>();
                Map<Id,Order>  updateParentOrdersMap = new Map<Id,Order>();

                Map<id,Order> allChildOrdersMap = new Map<id,Order>([Select id,parentId__c,Total_Monthly_Discount__c,Total_One_Time_Discount__c,Total_Monthly_Charges__c,
                                                    Total_One_Time_Charges__c, vlocity_cmt__RecurringTotal__c,vlocity_cmt__OneTimeTotal__c,vlocity_cmt__TotalMonthlyDiscount__c,
                                                    vlocity_cmt__TotalOneTimeDiscount__c from order where parentId__c = :parentOrderIdsSet Order BY parentId__c ]);

                if(null != allChildOrdersMap && allChildOrdersMap.size()>0){
                    Double totalMRC,totalNRC,totalMRCDiscount,totalNRCDiscount = 0;
                   for(Order ordr : allChildOrdersMap.values() ){
                        Order ord = new Order(id = ordr.parentId__c ); 
                        
                        if(null != newOrderMap.get(ordr.Id)){
                            ordr = newOrderMap.get(ordr.Id);
                        }     

                        totalMRC = ordr.vlocity_cmt__RecurringTotal__c;
                        totalNRC = ordr.vlocity_cmt__OneTimeTotal__c;
                        totalMRCDiscount = ordr.vlocity_cmt__TotalMonthlyDiscount__c;
                        totalNRCDiscount = ordr.vlocity_cmt__TotalOneTimeDiscount__c;

                        //Calculate Total MRC from child..            
                        if(null != totalMRConParentOrderMap && null != totalMRConParentOrderMap.get(ordr.parentId__c)){
                            totalMRC += totalMRConParentOrderMap.get(ordr.parentId__c);
                        }
                        totalMRConParentOrderMap.put(ordr.parentId__c,totalMRC);
                        //Calculate Total NRC from child..
                        if(null != totalNRConParentOrderMap &&  null != totalNRConParentOrderMap.get(ordr.parentId__c)){
                            totalNRC += totalNRConParentOrderMap.get(ordr.parentId__c);
                        }
                        totalNRConParentOrderMap.put(ordr.parentId__c,totalNRC);

                        //Calculate Total MRC Discount from child..    
                        if(null != totalMRCDiscountonParentOrderMap && null != totalMRCDiscountonParentOrderMap.get(ordr.parentId__c)){
                            totalMRCDiscount += totalMRCDiscountonParentOrderMap.get(ordr.parentId__c);
                        }
                        totalMRCDiscountonParentOrderMap.put(ordr.parentId__c,totalMRCDiscount);

                        //Calculate Total NRC Discount from child..    
                        if(null != totalNRCDiscountonParentOrderMap &&  null != totalNRCDiscountonParentOrderMap.get(ordr.parentId__c)){
                            totalNRCDiscount += totalNRCDiscountonParentOrderMap.get(ordr.parentId__c);
                        }
                        totalNRCDiscountonParentOrderMap.put(ordr.parentId__c,totalNRCDiscount);    

                        //Update latest MRC and NRC....
                        ord.Total_Monthly_Charges__c = totalMRC;   
                        ord.Total_One_Time_Charges__c = totalNRC;
                        ord.Total_Monthly_Discount__c = totalMRCDiscount;   
                        ord.Total_One_Time_Discount__c = totalNRCDiscount;              
                  
                     if(null == newOrderMap.get(ord.Id)){
                      updateParentOrdersMap.put(ord.id,ord);
                     }
                  }
                }
                if(null != updateParentOrdersMap && updateParentOrdersMap.size() > 0){
                    updateParentOrderList.addAll(updateParentOrdersMap.values());
                    update updateParentOrderList;
                }                
            } 
        }

    }

    //end BMPF-40
}