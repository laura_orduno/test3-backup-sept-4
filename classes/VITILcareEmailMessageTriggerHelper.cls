/**
* @author = Travis Cote, TOD. 10/09/2015
*/



public with sharing class VITILcareEmailMessageTriggerHelper {
    private static String VITILCARE_EMAIL_INDICATOR = 'VITILcare';
    private static String LYNX_TICKET_EMAIL_INDICATOR = 'Update Lynx Ticket Number';
    private static Integer LYNX_TICKET_MIN_LENGTH = 6;
    
	public static void updateCase(List<EmailMessage> messages, Map<Id, EmailMessage> messageMap){        
        
        try{
            List<VITILcare_Case_Email__c> vitilCareEmails = new List<VITILcare_Case_Email__c>();
            List<CaseComment> commentToInsert = new List<CaseComment>{};  
          
            for(EmailMessage message : messages){   
                if(message.TextBody == null){
                    continue;    
                }
                
                if(message.TextBody.containsIgnoreCase(VITILCARE_EMAIL_INDICATOR)){
                    if(message.TextBody.containsIgnoreCase(LYNX_TICKET_EMAIL_INDICATOR)){
                        // extract case Id, and lynx ticket number from email body		
                        String caseId = getValueAtSearchText(message.TextBody, 'Case Id: ', new Set<String>{' ', 'Case'});    
                        
                        String lynxTicketNumber = getLynxTicketNumberFromText(message.TextBody);
// DEPRECATED                        String lynxTicketNumber = getValueAtSearchText(message.TextBody, null, new Set<String>{'-'});    
                        
                        // for each Apropos email recieved that contains LYNX_TICKET_EMAIL_INDICATOR criteria, create a VITILcare_Case_Email__c so that the workflow "VITILcare Update Case From Email" will fire
                        // and the corresponding case can be updated
                        VITILcare_Case_Email__c vitilcareCaseEmail = new VITILcare_Case_Email__c();            
                        vitilcareCaseEmail.VITILcare_Case__c = Id.valueOf(caseId.substring(0, 15));
                        vitilcareCaseEmail.From__c = 'Apropos';                    
                        vitilcareCaseEmail.Lynx_Ticket_Number__c = lynxTicketNumber;                                    
                        vitilCareEmails.add(vitilcareCaseEmail);                    
                    }
                    else{                    
                        // extract case Id so that comments can be added to it
                        String caseId = getValueAtSearchText(message.TextBody, 'Case Id: ', new Set<String>{' ', 'Case'});    
                        
                        if(!String.isEmpty(message.TextBody) && !String.isEmpty(caseId) && message.TextBody.contains('ref:_')){
                            CaseComment tmpComment = createNewComment(message, caseId); 
                            // add to insert list
                            if(tmpComment != null){
                                commentToInsert.add(tmpComment);
                            }
                        }            
                    }
                }    
            }
                    
            if(vitilCareEmails.size() > 0){
	            insert vitilCareEmails;                
            }
            if(commentToInsert.size() > 0){
	            insert commentToInsert;                
            }
        }
        catch(DmlException e){
            throw(e);
        }
        catch(Exception e){
            throw(e);            
        }        
	}

    public static String getValueAtSearchText(String text, String searchText, Set<String> terminators){
        String value = ''; 
        Integer rightIndex = -1;
        Integer lastRightIndex = -1;
        
        Integer leftIndex = 0;
        if(searchText != null){
        	leftIndex = text.indexOf(searchText);
            
            if(leftIndex >= 0){
        		leftIndex += searchText.length();                
            }
            else{
                return '';
            }
        }
        for(String terminatorKey : terminators){
        	rightIndex = text.indexOf(terminatorKey, leftIndex);
            
        	if(rightIndex == -1){
				rightIndex = lastRightIndex;
                continue;
            }                            
        	if(lastRightIndex != -1 && rightIndex != -1 && lastRightIndex < rightIndex){
                rightIndex = lastRightIndex;
            }                
            lastRightIndex = rightIndex;
        }                
        if(rightIndex == -1){
            rightIndex = text.length();
        }
        
        value = text.substring(leftIndex, rightIndex);
                
        return value;
    }
    
    public static String getLynxTicketNumberFromText(String text){
		String lynxTicketNumber = '';
    	Integer length = 0;
		
        List<String> candidates = new List<String>();
        
        // find largest numeric string
        for(String num : getNumbersFromText(text)){
    		if(num.length() > LYNX_TICKET_MIN_LENGTH){
                candidates.add(num);
            }
        }        
        
        if(!candidates.isEmpty()){
            lynxTicketNumber = candidates.get(0);
        }    
        
        return lynxTicketNumber;
    }
    
    public static List<String> getNumbersFromText(String text){
        List<String> numbers = new List<String>();
        Pattern p = Pattern.compile('\\d+');
        Matcher m = p.matcher( text );
        
        while (m.find()) {
            numbers.add(m.group());
        }
                
        return numbers;
    }
    
	public static CaseComment createNewComment(EmailMessage em, Id caseId){
		CaseComment newComment = new CaseComment(
										IsPublished = true, 
										commentBody = '');
		newComment.parentId = caseId;
		newComment.commentBody = '';

	    String commentBody = getCommentBody(em, newComment.commentBody.length());

		if(commentBody == null || commentBody.equals('')){
			return null;
		}
	    
	    newComment.commentBody += commentBody;

        return newComment;
	}

	public static String getCommentBody(EmailMessage em, Integer authorName){
		Integer allowedSize = 2000 - authorName;
		String commentBody = '';
	    
	    if(em !=null && em.TextBody!= null){
	    	List<String> emailBody = em.TextBody.split('\n');

	    	for(String str : emailBody){
				if(str.containsIgnoreCase(Label.MBRReplyAbove) || str.startsWith('From:') || str.startsWith('> On') || str.contains('-Original Message-')){
	    			break;
	    		}
	    		else if(str.length() < allowedSize){
                    
	    			commentBody += str.trim() + '\n';
                    commentBody = commentBody.replace('-','');
	    			allowedSize -= str.trim().length();
	    		}
	    	}
	    }
        return commentBody;
	}
        
    public static void preventInvalidEmails(List<EmailMessage> messages){
        for(EmailMessage message : messages){   
			if(message.ToAddress != null && message.ToAddress.contains('vitilcare')){
                if(!(message.TextBody.containsIgnoreCase(VITILCARE_EMAIL_INDICATOR) || message.TextBody.containsIgnoreCase(LYNX_TICKET_EMAIL_INDICATOR))){
                    message.addError('VITILcare email not sent from Apropos, discard');
                }
            }    
        }    
    }
}