/*****************************************************
    Name: ExternalEventManager
    Usage: This is event Manager class to define methods used to trigger event for respective event name. 
    This class creates the instance of event handler classes dynamically
    Author – Sandip Chaudhari (IBM)
    Revision History -
    Created By/Date - Sandip Chaudhari / 04 July 2016
    
    Updated By/Date/Reason for Change - 
    
******************************************************/

global class ExternalEventManager{
    // Handle the event for respective event name
    global static final Integer BATCH_SIZE = 15; // Batch size to handle events in bulk.
    
    /*
    * Name - handleEvent
    * Description - Handle the events received from external system, creates the instance of event handler classes dynamically
    *               Decide to process events async or sync way
    * Param - List<External_Event__c> eventObjects
    * Return Type - void
    */
    global static void handleEvent(List<External_Event__c> eventObjects, List<ExternalEventRequestResponseParameter.ResponseEventData> responseData){
        if(eventObjects != null && eventObjects.size()==1){
           External_Event_Config__c eventHandlerConfig = External_Event_Config__c.getInstance(eventObjects[0].Event_Name__c);
           if(eventHandlerConfig != null){
                if(eventHandlerConfig.Is_Asynchronous_Execution__c){
                    // Execute asyncronousely using batch apex
                    ExternalEventBatchProcessor batchJobObject = new ExternalEventBatchProcessor(eventObjects, responseData);
                    ID batchprocessid = Database.executeBatch(batchJobObject, BATCH_SIZE);
                }else{
                    // Execute syncronousely
                    EventHandlerInterface classObject = ExternalEventManager_Helper.createHandlerInstance(eventObjects[0].Event_Name__c);
                    if(classObject != null){
                        classObject.processEvent(eventObjects, responseData);
                        system.debug('@@@ responseData manager' + responseData);
                    }else{
                        eventObjects[0].Event_Process_Error__c = system.label.ExternalEventControllerNameMissing;
                    }
                    update eventObjects[0];
                }
           }        
        }else if(eventObjects != null && eventObjects.size()>1){
            // always execute asyncronousely if more than one events
            ExternalEventBatchProcessor batchJobObject = new ExternalEventBatchProcessor(eventObjects, responseData);
            ID batchprocessid = Database.executeBatch(batchJobObject, BATCH_SIZE);
        }else{
            return;
        }
        return;
    }
    
    /*
    * Name - validateEventDetails
    * Description - Validate the event details before saving records in sfdc
    * Param - String eventName, String eventDetails, List<ExternalEventRequestResponseParameter.EventError> errorlist
    * Return Type - void
    */
    global static void validateEventDetails(String eventName, String eventDetails, List<ExternalEventRequestResponseParameter.EventError> errorlist){
            EventHandlerInterface classObject = ExternalEventManager_Helper.createHandlerInstance(eventName);
            if(classObject != null){
                try{
                    classObject.validateEventDetails(eventDetails, errorList);
                }catch(Exception ex){
                    ExternalEventRequestResponseParameter.EventError errorObject = new ExternalEventRequestResponseParameter.EventError();
                    errorObject.errorCode = 'EE-ERR-03';
                    errorObject.errorDescription = ex.getMessage() + ' - ' +  ex.getstacktracestring();
                    errorList.add(errorObject);
                }
            }
    }
    
      
}