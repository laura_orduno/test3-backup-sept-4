public with sharing class CreditProfile_Controller {

  public String cpId{get; set;}

  public String queryString{get; set;}
  public Credit_Profile__c cp {
    get{

        if (cpId != null) {
          queryString = 'Select Id, Name';

          for(Schema.FieldSetMember fld :SObjectType.Credit_Profile__c.FieldSets.Account_Credit_Profile.getFields()) {
            queryString += ', ' + fld.getFieldPath();
          }
          queryString += ' From credit_profile__c Where Id = :cpId limit 1';
      
         Credit_Profile__c[] cpList = Database.query(queryString);
         if (cpList.size()>0)
             cp=cpList[0];
        }
      return cp;
    }

    set;
  }
}