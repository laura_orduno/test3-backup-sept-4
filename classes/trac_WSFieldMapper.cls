/*
 * Methods for mapping WebService request/response fieds to SObject fields and vice versa
 * Used by trac_RepairCallout and trac_RepairWS classes
 * Part of the SMB Repairs project
 *
 * @author: Grant Adamson, Traction On Demand 
 */

public without sharing class trac_WSFieldMapper {
	public static final String QUOTE_READY_STATUS = '31';
	public static final String CLOSED_STATUS = '5';
	public static final String CANCELLED_STATUS = '3';
	public static final String SFDC_CANCELLED_STATUS = 'Cancelled';
	public static final String ACCESSORY_REPAIR_TYPE = 'Accessory';
	public static final String DEVICE_REPAIR_TYPE = 'Device';
	public static final String SIM_REPAIR_TYPE = 'SIM';
	
	
	public static void tracRepairResultToCase(trac_RepairWS.RepairRequest req, Case c) {
		c.repair__c = req.repairID + '';
		c.Repair_in_Progress__c = true;
		if(req.clientName != null) {
			c.Customer_Title_client__c = req.clientName.title;
			c.Customer_First_Name_client__c = req.clientName.firstName;
			c.Customer_Middle_Name_client__c = req.clientName.middleName;
			c.Customer_Last_Name_client__c = req.clientName.lastName;
		}
		if(req.defectiveSerialNumber != null) {
			c.ESN_MEID_IMEI__c = req.defectiveSerialNumber;
		}
		if(req.mobileNumber != null) {
			c.Mobile_number__c = req.mobileNumber;
		}
		if(req.defectiveSKU != null) {
			c.Model_SKU__c = req.defectiveSKU;
		}
		if(req.contactName != null) {
			c.Customer_Title_contact__c = req.contactName.title;
			c.Customer_First_Name_contact__c = req.contactName.firstName;
			c.Customer_Middle_Name_contact__c = req.contactName.middleName;
			c.Customer_Last_Name_contact__c = req.contactName.lastName;
		}
		if(req.contactPhoneNumber != null) {
			c.Customer_Phone_contact__c = req.contactPhoneNumber;
		}
		if(req.contactEmailAddress != null && c.Repair_Type__c != ACCESSORY_REPAIR_TYPE) { // contactEmailAddress is returned blank for accessory repair type
			c.Customer_Email_contact__c = req.contactEmailAddress;
		}
		if(req.clientShippingAddress != null) {
			c.shipping_required__c = true;
			c.use_alternate_address__c = true;
			
			c.Address_shipping__c = req.clientShippingAddress.streetName;
			c.City_shipping__c = req.clientShippingAddress.municipalityName;
			c.Province_shipping__c = req.clientShippingAddress.provinceStateCode;
			c.Postal_Code_shipping__c = req.clientShippingAddress.postalZipCode;
		} 
		if(req.clientBusinessName != null) {
			c.Business_Name__c = req.clientBusinessName;
		}
		if(req.physicalDamageInd != null) {
			if(req.physicalDamageInd){
				c.put('physical_damage__c','Yes');
			}else{
				c.put('physical_damage__c','No');
			}
		}
		if(req.issue != null) {
			c.Issue_System__c = req.issue;
		}
		if(req.defectsBehaviour != null) {
			c.Defects_behaviour__c = req.defectsBehaviour;
		}
		if(req.descriptionAndTroubleshooting != null) {
		c.Descrip_of_Defect_Troubleshooting_Perf__c = req.descriptionAndTroubleshooting;
		}
		if(req.cosmeticDamageInd != null) {
			c.Is_there_any_Cosmetic_Damage__c = req.cosmeticDamageInd;
		}
		if(req.descriptionOfCosmeticDamage != null) {
			c.Comments_create_repair__c = req.descriptionOfCosmeticDamage;
		}
		if(req.estimatedRepairCost != null) {
			c.Estimated_Repair_Cost_Up_To__c = req.estimatedRepairCost;
		}
	}
	
	
	public static void validateResultToCase(svc_RepairRequestValidationSvcRequestR_2.validateRepairRequestResponse_element resp, Case c) {
		if (resp.defectiveSummary != null) {
			c.Model_SKU__c = resp.defectiveSummary.defectTelusSku;
			
			if(c.Repair_Type__c != ACCESSORY_REPAIR_TYPE) {
				c.DOA_Expiry_Date__c =
					(resp.defectiveSummary.doaExpiryDate != null ? resp.defectiveSummary.doaExpiryDate.dateGmt() : null);
			}

			c.Warranty_Status__c = resp.defectiveSummary.warrantyStatusCd;
			c.Warranty_Expiry_Date__c =
				(resp.defectiveSummary.warrantyExpiryDate != null ? resp.defectiveSummary.warrantyExpiryDate.dateGmt() : null);

			if (resp.defectiveSummary.productName != null && resp.defectiveSummary.productName.name != null &&
					resp.defectiveSummary.productName.name.size() > 0) {
				for (svc_EnterpriseCommonTypes_v7.Name prodList : resp.defectiveSummary.productName.name) {
					if(prodList.locale == 'EN_CA') {
						c.Product__c = prodList.name;
					}
					if(prodList.locale == 'FR_CA') {
						c.Product_FR__c = prodList.name;
					}
				}
			}
			if(resp.defectiveSummary.clientName != null) {
				c.Customer_First_Name_client__c = resp.defectiveSummary.clientName.firstName;
				c.Customer_Last_Name_client__c = resp.defectiveSummary.clientName.lastName;
				c.Business_Name__c = resp.defectiveSummary.clientName.legalBusinessName;
			}
			if(resp.defectiveSummary.clientAddress != null) {
				if(resp.defectiveSummary.clientAddress.streetNumber != null ||
					 resp.defectiveSummary.clientAddress.streetName != null) {
					c.Address_account__c =
						(resp.defectiveSummary.clientAddress.streetNumber != null ? resp.defectiveSummary.clientAddress.streetNumber : '') +
						(resp.defectiveSummary.clientAddress.streetNumber != null ? ' ' :'') +
						(resp.defectiveSummary.clientAddress.streetName != null ? resp.defectiveSummary.clientAddress.streetName : '') +
						
						(resp.defectiveSummary.clientAddress.unitTypeCode != null ? ' ' + resp.defectiveSummary.clientAddress.unitTypeCode : '') +
						(resp.defectiveSummary.clientAddress.unitName != null ? ' ' + resp.defectiveSummary.clientAddress.unitName : '');
				}
				c.City_account__c = resp.defectiveSummary.clientAddress.municipalityName;
				c.Province_account__c = resp.defectiveSummary.clientAddress.provinceStateCode;
				c.Postal_Code_account__c = resp.defectiveSummary.clientAddress.postalZipCode;
			}
		}
		c.repair_validated__c = (resp.validationStatus.toUpperCase() == 'SUCC');
	}
	
	
	public static void getRepairResultToCase(rpm_EquipmentRepairInformationSvcRequest.getRepairResponse_element resp, Case c) {
		
		if(resp.repair != null) {
			
			// ID Mismatch check
			c.repair_case_id_mismatch__c = false;
			
			if(resp.repair.salesForceCaseInfo != null && resp.repair.salesForceCaseInfo.caseID != null &&
				 resp.repair.salesForceCaseInfo.caseID != ''){
				
				Id idFromResponse;	
				try {
					idFromResponse = (Id)resp.repair.salesForceCaseInfo.caseID;
				} catch (Exception e) {
					c.repair_case_id_mismatch__c = true;
					return;
				}
				
				if(idFromResponse != c.id) {
					c.repair_case_id_mismatch__c = true;
					return;
				}
			}
			
			if(resp.repair.subscriberNumber != null) { // SFDC validation requires a record to have a valid mobile number
				c.Mobile_number__c = resp.repair.subscriberNumber;
			}
	    c.Repair_Completion_ETA__c =
	    	(resp.repair.repairCompletionETA != null ? resp.repair.repairCompletionETA.dateGmt() : null);

	    
	    if(resp.repair.repairStatus != null) {
	    	c.SRD_Repair_Status_ID__c = resp.repair.repairStatus.repairStatusID;
	    	processRepairStatusId(c);
	    	
	    	if(resp.repair.repairStatus.repairStatusDescription != null && 
	    		 resp.repair.repairStatus.repairStatusDescription.codeDescText != null &&
	    		 resp.repair.repairStatus.repairStatusDescription.codeDescText.size() > 0) {
					for (rpm_EnterpriseCommonTypes_v7.CodeDescText statusList : resp.repair.repairStatus.repairStatusDescription.codeDescText) {
						if(statusList.locale == 'EN_CA') {
							c.SRD_Repair_Status__c = statusList.codeDescText;
						} else if(statusList.locale == 'FR_CA') {
							c.SRD_Repair_Status_FR__c = statusList.codeDescText;
						}
					}
	    	
	    	} // end repairStatusDescription
	    
	    } // end repairStatus
	    
	    if(resp.repair.clientAccountInfo != null) {
	    	if(resp.repair.clientAccountInfo.clientName != null) {
	    		c.Customer_First_Name_client__c = resp.repair.clientAccountInfo.clientName.firstName;
	    		c.Customer_Last_Name_client__c = resp.repair.clientAccountInfo.clientName.lastName;
	    		c.Business_Name__c = resp.repair.clientAccountInfo.clientName.legalBusinessName;
	    	}
	    	
	    	if(resp.repair.clientAccountInfo.contactName != null) {
	    		List<String> nameParts = resp.repair.clientAccountInfo.contactName.split(',');
	    		if(nameParts.size() > 0) {
	    			c.Customer_First_Name_contact__c = nameParts[0];
	    			if(nameParts.size() > 1) {
	    				c.Customer_Last_Name_contact__c = nameParts[1];
	    			}
	    		}
	    	}
	    	
	    	if(c.Repair_Type__c != ACCESSORY_REPAIR_TYPE) { // contactEmailAddress is returned blank for accessory repair type
	    		c.Customer_Email_contact__c = resp.repair.clientAccountInfo.contactEmailAddress;
	    	}
    		c.Customer_Phone_contact__c = resp.repair.clientAccountInfo.contactPhoneNumber;
	    
	    } // end clientAccountInfo
	    
	    
	    if(resp.repair.productInfo != null) {
	    	
	    	c.Model_SKU__c = resp.repair.productInfo.defectiveSKU;
				c.ESN_MEID_IMEI__c = resp.repair.productInfo.defectiveSerialNumber;
				c.Warranty_Status__c = resp.repair.productInfo.warrantyStatus;
				c.Product__c = resp.repair.productInfo.productName;
	    
	    } // end productInfo
	    
	    
	    if(resp.repair.repairInfo != null) {
	    	
	    	c.Transaction_type__c = resp.repair.repairInfo.transactionType;
	    	c.put('Physical_Damage__c',(resp.repair.repairInfo.physicalDamageInd != null&&resp.repair.repairInfo.physicalDamageInd==true ? 'Yes' : 'No'));
	    	c.Descrip_of_Defect_Troubleshooting_Perf__c = resp.repair.repairInfo.troubleshootingDescription;
	    	if(c.repair_type__c != ACCESSORY_REPAIR_TYPE) {
		    	c.DOA_Expiry_Date__c =
		    		(resp.repair.repairInfo.doaExpiryDate != null ? resp.repair.repairInfo.doaExpiryDate.dateGmt() : null);
		    }
	    	c.Warranty_Expiry_Date__c =
	    		(resp.repair.repairInfo.warrantyExpiryDate != null ? resp.repair.repairInfo.warrantyExpiryDate.dateGmt() : null);
	    	
				if(resp.repair.repairInfo.repairResolutionTypeCodeList != null && 
					 resp.repair.repairInfo.repairResolutionTypeCodeList.repairResolutionDetail != null &&
	    		 resp.repair.repairInfo.repairResolutionTypeCodeList.repairResolutionDetail.size() > 0) {
	    				
					if(resp.repair.repairInfo.repairResolutionTypeCodeList.repairResolutionDetail[0].resolutionCode != null &&
						 resp.repair.repairInfo.repairResolutionTypeCodeList.repairResolutionDetail[0].resolutionCode.codeDescText != null &&
    				 resp.repair.repairInfo.repairResolutionTypeCodeList.repairResolutionDetail[0].resolutionCode.codeDescText.size() > 0) {
						for(rpm_EnterpriseCommonTypes_v7.CodeDescText resCode :
							resp.repair.repairInfo.repairResolutionTypeCodeList.repairResolutionDetail[0].resolutionCode.codeDescText) {
							if(resCode.locale == 'EN_CA') {
								c.Resolution_Code__c = resCode.codeDescText;
							} else if(resCode.locale == 'FR_CA') {
								c.Resolution_Code_FR__c = resCode.codeDescText;
							}
						}
					
					} // end resolutionCode
					
					if(resp.repair.repairInfo.repairResolutionTypeCodeList.repairResolutionDetail[0].resolutionComment != null &&
						 resp.repair.repairInfo.repairResolutionTypeCodeList.repairResolutionDetail[0].resolutionComment.codeDescText != null &&
    				 resp.repair.repairInfo.repairResolutionTypeCodeList.repairResolutionDetail[0].resolutionComment.codeDescText.size() > 0) {
						for(rpm_EnterpriseCommonTypes_v7.CodeDescText resComment :
							resp.repair.repairInfo.repairResolutionTypeCodeList.repairResolutionDetail[0].resolutionComment.codeDescText) {
							if(resComment.locale == 'EN_CA') {
								c.Resolution_Comment__c = resComment.codeDescText;
							} else if(resComment.locale == 'FR_CA') {
								c.Resolution_Comment_FR__c = resComment.codeDescText;
							}
						}
					
					} // end resolutionComment
					
				} // end repairResolutionTypeCodeList
				
    		if(resp.repair.repairInfo.repairComplaintList != null &&
    			 resp.repair.repairInfo.repairComplaintList.repairComplaint != null &&
    			 resp.repair.repairInfo.repairComplaintList.repairComplaint.size() > 0 &&
    			 resp.repair.repairInfo.repairComplaintList.repairComplaint[0].complaintDescription != null &&
    			 resp.repair.repairInfo.repairComplaintList.repairComplaint[0].complaintDescription.codeDescText != null &&
    			 resp.repair.repairInfo.repairComplaintList.repairComplaint[0].complaintDescription.codeDescText.size() > 0) { 

					for(rpm_EnterpriseCommonTypes_v7.CodeDescText comDesc :
						resp.repair.repairInfo.repairComplaintList.repairComplaint[0].complaintDescription.codeDescText) {
						if(comDesc.locale == 'EN_CA') {
							c.Issue_System__c = comDesc.codeDescText;
						} else if(comDesc.locale == 'FR_CA') {
							c.Issue_System_FR__c = comDesc.codeDescText;
						}
					}
					
    		}
	    	
	    	if(resp.repair.repairInfo.repairSymptomList != null &&
	    		 resp.repair.repairInfo.repairSymptomList.repairSymptom != null &&
    			 resp.repair.repairInfo.repairSymptomList.repairSymptom.size() > 0 &&
    			 resp.repair.repairInfo.repairSymptomList.repairSymptom[0].repairSymptomText != null &&
    			 resp.repair.repairInfo.repairSymptomList.repairSymptom[0].repairSymptomText.codeDescText != null &&
    			 resp.repair.repairInfo.repairSymptomList.repairSymptom[0].repairSymptomText.codeDescText.size() > 0) { 
					
					for(rpm_EnterpriseCommonTypes_v7.CodeDescText sympText :
						resp.repair.repairInfo.repairSymptomList.repairSymptom[0].repairSymptomText.codeDescText) {
						if(sympText.locale == 'EN_CA') {
							c.Defects_behaviour__c = sympText.codeDescText;
						} // picklist, no french mapping required
					}
					
    		} // end repairSymptomList
	    	
	    	if(resp.repair.repairInfo.cosmeticDamageInd != null) {
	    		c.Is_there_any_Cosmetic_Damage__c = resp.repair.repairInfo.cosmeticDamageInd;
	    	}
	    	c.Comments_create_repair__c = resp.repair.repairInfo.cosmeticDamageDescription;
	    	c.Authorized_Repair_Cost_up_to__c = resp.repair.repairInfo.authorizedRepairCost;
			
			} // end repairinfo
	    
	    if(resp.repair.equipmentQuotation != null) {
	    	if(resp.repair.equipmentQuotation.quotationContactInd != null) {
	    		c.Client_Contacted__c = resp.repair.equipmentQuotation.quotationContactInd;
	    	}

	    	c.Quote_Date__c =
	    		(resp.repair.equipmentQuotation.quotationDate != null ? resp.repair.equipmentQuotation.quotationDate.dateGmt() : null);

	    	c.Quote_Cost__c = resp.repair.equipmentQuotation.quotationCost;
	    	c.Accepted_Rejected_Extended__c = resp.repair.equipmentQuotation.quotationAcceptRejectStatus;
	    	c.Notes__c = resp.repair.equipmentQuotation.quotationReplyComment;
	    	c.Quote_Extension__c =
	    		(resp.repair.equipmentQuotation.quotationExtensionDate != null ? resp.repair.equipmentQuotation.quotationExtensionDate.dateGmt() : null);

	    }
	    
	    if(resp.repair.loanerProductInfo != null) {
	    	c.Loaner__c = true;
				c.Loaner_ESN_MEID__c = resp.repair.loanerProductInfo.loanerSerialNumber;
	    }
	    
	   	if(resp.repair.shipmentDetail != null) {
	    	
	    	if(resp.repair.shipmentDetail.originShippingInformation != null &&
	    		 resp.repair.shipmentDetail.originShippingInformation.address != null) {
    			c.shipping_required__c = true;
    			c.Use_Alternate_Address__c = true;
    			
    			if(resp.repair.shipmentDetail.originShippingInformation.address.renderedAddress != null &&
    				 resp.repair.shipmentDetail.originShippingInformation.address.renderedAddress.size() > 0) {
    				c.Address_shipping__c = resp.repair.shipmentDetail.originShippingInformation.address.renderedAddress[0];
    			}
    			
    			c.City_shipping__c = resp.repair.shipmentDetail.originShippingInformation.address.municipalityName;
    			c.Province_shipping__c = resp.repair.shipmentDetail.originShippingInformation.address.provinceStateCode;
    			c.Postal_Code_shipping__c = resp.repair.shipmentDetail.originShippingInformation.address.postalZipCode;
	    	
	    	} // end originShippingInformation
	    	
	    	c.Waybill_Number__c = resp.repair.shipmentDetail.waybillCode;
	    
	    } // end shipmentDetail
		
		} // end repair
	}
	
	
	public static Map<String,String> repairStatusByIdToMap(rpm_EquipmentRepairInformationSvcRequest.RepairStatusList rsl) {
		Map<String,String> statusMap = new Map<String,String>();
		
		for(rpm_EquipmentRepairInformationSvcRequest.RepairStatusById rsbi : rsl.repairStatusById) {
			statusMap.put(rsbi.repairId,rsbi.repairStatusId);
		}
		
		return statusMap;
	}
	
	
	public static rpm_EquipmentRepairInformationSvcRequest.RepairIdList idSetToRepairIdList(Set<String> repairIds) {
		rpm_EquipmentRepairInformationSvcRequest.RepairIdList idList = new rpm_EquipmentRepairInformationSvcRequest.RepairIdList();
			
		idList.repairID = new List<String>();
		idList.repairId.addAll(repairIds);
		
		return idList;
	}
	
	
	private static void processRepairStatusId(Case c) {
		if(c.SRD_Repair_Status_Id__c != null) {
			String status = c.SRD_Repair_Status_Id__c;
			
			c.Repair_Quote_Available__c = false; // reset quote ready status
			
			if(status == QUOTE_READY_STATUS) {
				c.Repair_Quote_Available__c = true;
			}
			if(status == CLOSED_STATUS) {
				c.Repair_In_Progress__c = false;
			}
			if(status == CANCELLED_STATUS) {
				c.Repair_in_Progress__c = false; // prevent validation rules from blocking close
				c.status = SFDC_CANCELLED_STATUS;
			}
		}
	}

}