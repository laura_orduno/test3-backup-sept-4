//@isTest
//Before commenting, 98%
public class BillingAdjustmentDataHelperTest {
    /*
    public static final String TRANSCATION_TYPE ='Credit';
    public static final String BUSINESS_UNIT ='TBS';
    
    private static List<Billing_Adjustment__c> baList = new List<Billing_Adjustment__c>{
            BillingAdjustmentCommonTest.getBillingAdjustment(500.00,'Credit','Entrprise'),
            BillingAdjustmentCommonTest.getBillingAdjustment(700.00,'Debit','TBS')
        };
            
    private static Account accoutWithSA = BillingAdjustmentCommonTest.createAccountWithSalseAssignments();            
    private static Account parentAccount = BillingAdjustmentCommonTest.createParentAccount();
    private static Account subAccount = BillingAdjustmentCommonTest.createAccount(parentAccount.id);
    private static Account subsubAccount = BillingAdjustmentCommonTest.createAccount(subAccount.id);
    private static Account noparentAccount = BillingAdjustmentCommonTest.createAccount(null);
    
    static {
                BillingAdjustmentCommonTest.createReasonCodeData();
                BillingAdjustmentCommonTest.createReleaseCodeThresholdData(TRANSCATION_TYPE,BUSINESS_UNIT);
                BillingAdjustmentCommonTest.createControllerThresholdData(TRANSCATION_TYPE,BUSINESS_UNIT);
                BillingAdjustmentCommonTest.createFinanceControllerData(BUSINESS_UNIT);
           }
    
    @isTest()
    public static void testAddAndGetMethods(){
        for(Billing_Adjustment__c ba : baList){
                BillingAdjustmentDataHelper.addData(ba);
        }    
        System.assertEquals(true, BillingAdjustmentDataHelper.getReasonCodeDescSet().size()>0);
        System.assertEquals(true, BillingAdjustmentDataHelper.getAccountId().size()>0);
        System.assertEquals(true, BillingAdjustmentDataHelper.getAdjustmentTypeAndBusinessUnit().size()>0);
        System.assertEquals(true, BillingAdjustmentDataHelper.getUserList().size()>0);
    }

    
    @isTest()
    public static void testGetReasonDecCodeMap(){ 
        Set<BillingAdjustmentDataHelper.ReasonCodeDescription> resonDecsSet =new Set<BillingAdjustmentDataHelper.ReasonCodeDescription>{
            new  BillingAdjustmentDataHelper.ReasonCodeDescription('Bill Corrections','Adjustment Driver'), new  BillingAdjustmentDataHelper.ReasonCodeDescription('Billing Error','Description'),
                 new  BillingAdjustmentDataHelper.ReasonCodeDescription('Billing Error','Reason'), new  BillingAdjustmentDataHelper.ReasonCodeDescription('Internet','Product'),
                 new  BillingAdjustmentDataHelper.ReasonCodeDescription('Billing Systems','Responsible Area'),
                 new  BillingAdjustmentDataHelper.ReasonCodeDescription('Bill Transactions','Adjustment Driver'), new  BillingAdjustmentDataHelper.ReasonCodeDescription('Contract','Description'),
                 new  BillingAdjustmentDataHelper.ReasonCodeDescription('Advertising','Reason'), new  BillingAdjustmentDataHelper.ReasonCodeDescription('Optik','Product')}; 
      System.debug(BillingAdjustmentDataHelper.getReasonDescCodeMap(resonDecsSet));
      System.debug(BillingAdjustmentDataHelper.getReasonDescCodeMap(resonDecsSet).size());  
      System.assertEquals(true,BillingAdjustmentDataHelper.getReasonDescCodeMap(resonDecsSet).size()>0,'Testing ReasonDescCode Map');
    }
    
    @isTest()
    public static void testGetReasonDescCode(){ 
        BillingAdjustmentDataHelper.addData(baList.get(0));
        BillingAdjustmentDataHelper.loadData();
        /*
         *  Description: character 1
            Responsible Area: character 2
            Reason: character 3,4
            Product: character 5,6
         */ /*
       System.assertEquals('EK2160',BillingAdjustmentDataHelper.getReasonCode(baList.get(1)),'Testing ReasonCode For Given BA');
    }
    
     @isTest()
    public static void testIsValidRCOption(){ 
        BillingAdjustmentDataHelper.addData(baList.get(0));
        BillingAdjustmentDataHelper.loadData();
        BillingAdjustmentDataHelper.validateReleaseCode(baList.get(1));
        baList.get(1).RC_level_1__c='Inv';
        BillingAdjustmentDataHelper.validateReleaseCode(baList.get(1));
        baList.get(1).RC_level_2__c='Inv';
        BillingAdjustmentDataHelper.validateReleaseCode(baList.get(1));
        baList.get(1).RC_level_3__c='Inv';
        BillingAdjustmentDataHelper.validateReleaseCode(baList.get(1));
        baList.get(1).RC_Product__c='Inv';
        BillingAdjustmentDataHelper.validateReleaseCode(baList.get(1));
        baList.get(1).RC_Responsible_Area__c='Inv';
        BillingAdjustmentDataHelper.validateReleaseCode(baList.get(1));
    }   
   
    @isTest()
    public static void testGetAccountIdMap(){ 
        Map<id,id> parentaccountMap = BillingAdjustmentDataHelper.getParentAccountMap(new Set<id>{subAccount.id,subsubAccount.id,noparentAccount.id});
        System.assertEquals(parentAccount.id,parentaccountMap.get(subAccount.id),'Sub Account Parent account') ;
        System.assertEquals(parentAccount.id,parentaccountMap.get(subsubAccount.id),'Sub Sub Account Parent account') ;
    }
    
    @isTest()
    public static void testGetAccountID(){ 
        baList.get(0).account__c = subAccount.id;
        BillingAdjustmentDataHelper.addData(baList.get(0));
        baList.get(1).account__c = subsubAccount.id;
        BillingAdjustmentDataHelper.addData(baList.get(1));
        BillingAdjustmentDataHelper.loadData();
        System.assertEquals(parentAccount.id,BillingAdjustmentDataHelper.getParentAccountId(baList.get(0)),'Testing Parent Account Id  For Given BA(0)');
        System.assertEquals(parentAccount.id,BillingAdjustmentDataHelper.getParentAccountId(baList.get(1)),'Testing ReasonCode For Given BA (1)');
    }
    
    @isTest()
    public static void testGetSalesAssinmentRoleMap(){
        Account parentAccWithSA = BillingAdjustmentCommonTest.createAccountWithSalseAssignments();
        Map<id,Map<String,id>> saUserMap =  BillingAdjustmentDataHelper.getSalesAssignmentsMap(new List<id>{parentAccWithSA.ParentId});   
        System.assertEquals(true,saUserMap.size()>0,'Sales Account Map should not be empty') ;
        
    }
    
    @isTest()
    public static void testGetSalesAssinmentRoleByRole(){
        baList.get(0).account__c = accoutWithSA.id;
        baList.get(0).Parent_Account_id__c = accoutWithSA.ParentId;
        BillingAdjustmentDataHelper.addData(baList.get(0));
        BillingAdjustmentDataHelper.loadData();
        System.assertNotEquals(null,BillingAdjustmentDataHelper.getSalesAssimnetUserIdByRole(baList.get(0),'MD'),'Get Sales Assinment User id By Role') ;
        System.assertNotEquals(null,BillingAdjustmentDataHelper.getSalesAssinmentUserByRole(baList.get(0),'MD'),'Get Sales Assinment User By Role') ;
         
        
        
    }
   
   @isTest()
    public static void testGetSalesAssinmentUserEmailByRole(){
        baList.get(0).account__c = accoutWithSA.id;
        baList.get(0).Parent_Account_id__c = accoutWithSA.ParentId;
        BillingAdjustmentDataHelper.addData(baList.get(0));
        BillingAdjustmentDataHelper.loadData();
        System.assertNotEquals('',BillingAdjustmentDataHelper.getSalesAssinmentUserEmailByRole(baList.get(0),'DIR'),'Get Sales Assinment By Role email') ;
        System.assertEquals('',BillingAdjustmentDataHelper.getSalesAssinmentUserEmailByRole(baList.get(0),'MK'),'Get Sales Assinment By Role email for Wrong Role') ;
    }
    
   @isTest()
    public static void testGetSalesAssinmentUserList(){
        baList.get(0).account__c = accoutWithSA.id;
        baList.get(0).Parent_Account_id__c = accoutWithSA.ParentId;
        BillingAdjustmentDataHelper.addData(baList.get(0));
        BillingAdjustmentDataHelper.loadData();
        System.assertEquals(3,BillingAdjustmentDataHelper.getSalesAssimnetUserIdList().size(),'Get Sales Assinment User List') ;
        
    }
    
    @isTest()
    public static void testgetUserMap(){
        baList.get(0).account__c = accoutWithSA.id;
        baList.get(0).Parent_Account_id__c = accoutWithSA.ParentId;
        BillingAdjustmentDataHelper.addData(baList.get(0));
        BillingAdjustmentDataHelper.loadData();
        System.assert(BillingAdjustmentDataHelper.getUserMap().size()>0,'test getUser Map');
        
    }
    
   @isTest()
    public static void testgetUserMenthods(){
        //test UserById , UserEmail,MangerId
        Billing_Adjustment__c ba = baList.get(0); 
        ba.account__c = accoutWithSA.id;
        ba.Parent_Account_id__c = accoutWithSA.ParentId;
        User usrNoManager= BillingAdjustmentCommonTest.sampleUserMap.get('usrNoManager');
        ba.ownerId = BillingAdjustmentCommonTest.sampleUserMap.get('usrNoManager').id;
        ba.Approver_Release_Code__c ='1';
        ba.Required_Release_Code__c ='2';
        ba.Requested_By__c = BillingAdjustmentCommonTest.sampleUserMap.get('agent').id;
        BillingAdjustmentDataHelper.addData(baList.get(0));
        BillingAdjustmentDataHelper.loadData();
        System.assertNotEquals(null,BillingAdjustmentDataHelper.getUserById(ba.ownerId),'User by User id') ;
        System.assertNotEquals(null,BillingAdjustmentDataHelper.getUserEmail(ba.ownerId),' User Email') ;
        System.assertEquals(null,BillingAdjustmentDataHelper.getMangerId(ba, ba.ownerId),' User Email Manager') ;
        System.assertEquals(null,BillingAdjustmentDataHelper.getManger(ba,ba.ownerId),'getManger Without Manager: ' + ba.ownerId) ;
        System.assertNotEquals(null,BillingAdjustmentDataHelper.getManger(ba,ba.Requested_By__c),'getManger With Manager: ' + ba.Requested_By__c);
        
        
    }
    
    @isTest()
    public static void testIsInUserHierarchy(){
        User usr = new User(TeamCard_Level1_Mgr_ID__c='00065438', TeamCard_Level2_Mgr_ID__c='00773014', 
                            TeamCard_Level3_Mgr_ID__c='00802918', TeamCard_Level4_Mgr_ID__c='00821532');
        System.assertEquals(true,BillingAdjustmentDataHelper.isInUserHierarchy(usr,'00802918'),'Test 1');
        System.assertEquals(true,BillingAdjustmentDataHelper.isInUserHierarchy(usr,'00065438'),'Test 2');
        System.assertEquals(true,BillingAdjustmentDataHelper.isInUserHierarchy(usr,'00773014'),'Test 3');
        System.assertEquals(true,BillingAdjustmentDataHelper.isInUserHierarchy(usr,'00821532'),'Test 4');
        System.assertEquals(false,BillingAdjustmentDataHelper.isInUserHierarchy(usr,'00859171'),'Test 5');
    }
    
    @isTest()
    public static void testIsInApprovalProcess(){
        Billing_Adjustment__c ba1 = BillingAdjustmentCommonTest.createBillingAdjustment(21000,'Credit','TBS');
        ba1.RC_Product__c = 'Customer';
        ba1.Amount__c = 10.00;
        ba1.Approval_status__c='Pending';
        ba1.Approval_Step_Number__c=1;
        
        Billing_Adjustment__c ba2 = new Billing_Adjustment__c(Id=ba1.Id);
        ba2.RC_Product__c = 'Customer';
        ba2.Amount__c = 10.00;
        ba2.account__c = ba1.account__c;
        ba2.Approval_status__c='Pending';
        ba2.Approval_Step_Number__c=2;
        
        Map<Id, Billing_Adjustment__c> oldMap = new Map<Id, Billing_Adjustment__c>{ba1.Id => ba1};
        BillingAdjustmentDataHelper.OldMap =oldMap;
        
        System.assertEquals(true,BillingAdjustmentDataHelper.isApprovalProcessUpdate(ba2),'Test if Apprval process update ba2') ;
        ba2.Approval_Step_Number__c=1;
        System.assertEquals(false,BillingAdjustmentDataHelper.isApprovalProcessUpdate(ba2),'Test if Apprval process update ba3') ;
        
        Billing_Adjustment__c newBA = BillingAdjustmentCommonTest.getBillingAdjustment(2000, 'Credit','TBS');
        
    }
    
    
    @isTest()
    public static void testisReminderNotificationUpdate(){
        Billing_Adjustment__c ba1 = BillingAdjustmentCommonTest.createBillingAdjustment(21000,'Credit','TBS');
        ba1.RC_Product__c = 'Customer';
        ba1.Amount__c = 10.00;
        ba1.Approval_status__c='Pending';
        ba1.Approver_Notifications__c=1;
        
        Billing_Adjustment__c ba2 = new Billing_Adjustment__c(Id=ba1.Id);
        ba2.RC_Product__c = 'Customer';
        ba2.Amount__c = 10.00;
        ba2.account__c = ba1.account__c;
        ba2.Approval_status__c='Pending';
        ba2.Approver_Notifications__c=2;
        
        Map<Id, Billing_Adjustment__c> oldMap = new Map<Id, Billing_Adjustment__c>{ba1.Id => ba1};
        BillingAdjustmentDataHelper.OldMap =oldMap;
        
        System.assertEquals(true,BillingAdjustmentDataHelper.isReminderNotificationUpdate(ba2),'Test if Apprval process update ba2');
        ba2.Approver_Notifications__c=1;
        System.assertEquals(false,BillingAdjustmentDataHelper.isReminderNotificationUpdate(ba2),'Test if Apprval process update ba3');
        
        Billing_Adjustment__c newBA = BillingAdjustmentCommonTest.getBillingAdjustment(2000, 'Credit','TBS');
        System.assertEquals(false,BillingAdjustmentDataHelper.isReminderNotificationUpdate(newBA),'Test for new Before Insert when id=null');
    }
    
    
    @isTest()
    public static void testHasFieldUpdated(){
        Test.startTest();
        
        Billing_Adjustment__c ba = BillingAdjustmentCommonTest.createBillingAdjustment(21000,'Credit','TBS');
        ba.RC_Product__c = 'Customer';
        ba.Amount__c = 10.00;
        
        Billing_Adjustment__c ba2 = new Billing_Adjustment__c(Id=ba.Id);
        ba2.RC_Product__c = 'Customer';
        ba2.Amount__c = 11.00;
        ba2.account__c = ba.account__c; 
       
        Map<Id, Billing_Adjustment__c> oldMap = new Map<Id, Billing_Adjustment__c>{ba.Id => ba};
        System.assertEquals(true,BillingAdjustmentDataHelper.hasUpdated(ba2,'RC_Product__c'),'testing if the Product has updated beofe InitUpdates');    
            
        BillingAdjustmentDataHelper.OldMap =oldMap;
        System.debug('hasUpdate ' + BillingAdjustmentDataHelper.hasUpdated(ba2,'RC_Product__c'));
        System.debug('hasUpdate List ' + BillingAdjustmentDataHelper.hasUpdatedAny(ba2,new List<String>{'RC_Product__c','Amount__c'}));
        Test.stopTest();
        System.assertEquals(false,BillingAdjustmentDataHelper.hasUpdated(ba2,'RC_Product__c'),'testing if the Product has updated');
        System.assertEquals(true,BillingAdjustmentDataHelper.hasUpdated(ba2,'Amount__c'),'testing if the Amount has updated');
        System.assertEquals(true,BillingAdjustmentDataHelper.hasUpdatedAny(ba2,new List<String>{'RC_Product__c','Amount__c'}),'testing if Any given Field has been updated');
        System.assertEquals(false,BillingAdjustmentDataHelper.hasUpdatedAny(ba2,new List<String>{'RC_Product__c','Account__c'}),'testing if Any given Field has been updated');         
    }
    
     @isTest()
    public static void testgetCoporateController(){
        Billing_Adjustment__c ba = BillingAdjustmentCommonTest.createBillingAdjustment(3000000,'Credit','TBS');
        BillingAdjustmentDataHelper.addData(ba);
        BillingAdjustmentDataHelper.loadData();
        System.assertNotEquals(null,BillingAdjustmentDataHelper.getCoporateController(ba),'Check Coportate Controller');
    }
    
    @isTest
    public static void testControllersApprovalPending(){
       Billing_Adjustment__c ba = new Billing_Adjustment__c();
       ba.Controller_Approval_Required__c =false;
       ba.Controller_Approved__c =false;
       System.assertEquals(false,BillingAdjustmentHandlerHelper.isBCApprovalPending(ba));
       
       ba.Controller_Approval_Required__c =true;
       ba.Controller_Approved__c =false; 
       System.assertEquals(true,BillingAdjustmentHandlerHelper.isBCApprovalPending(ba)); 
       
       ba.Controller_Approval_Required__c =true;
       ba.Controller_Approved__c =true; 
       System.assertEquals(false,BillingAdjustmentHandlerHelper.isBCApprovalPending(ba));  
       // Check Corporate Controler 
       ba.Corporate_Controller_Approval_Required__c =false;
       ba.Corporate_Controller_Approved__c =false;
       System.assertEquals(false,BillingAdjustmentHandlerHelper.isCCApprovalPending(ba));
        
       ba.Corporate_Controller_Approval_Required__c =true;
       ba.Corporate_Controller_Approved__c =false;
       System.assertEquals(true,BillingAdjustmentHandlerHelper.isCCApprovalPending(ba)); 
        
        
       ba.Corporate_Controller_Approval_Required__c =true;
       ba.Corporate_Controller_Approved__c =true;
       System.assertEquals(false,BillingAdjustmentHandlerHelper.isCCApprovalPending(ba));  
       
    }

*/   
}