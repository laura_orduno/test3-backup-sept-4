public with sharing class MBRAttachmentTrigHelper {

	public static Map<Id, EmailMessage> emailIdMap = new Map<Id, EmailMessage>();
    public static boolean firstRun = true;
    public static final Set<String> EXTENSIONS = new Set<String>{'xlsm','XLSM','RTF','rtf','TXT','txt','CSV','csv','XLSX','xlsx','DOCX','docx','JPG','jpg','GIF','gif','PNG','png','DOC','doc','XLS','xls','PDF','pdf','ZIP','zip'};
    public static final Integer SIZE_LIMIT = 5 * Math.pow(1024, 2).intValue();
     

	public static void processNew(List<Attachment> attList){
		List<Attachment> attachedList = validateAttachments(attList);
		if(attachedList!=null && !attachedList.isEmpty()){
			emailIdMap = mapAttToEmails(attachedList);
			List<Id> caseIds = createAttachment(attachedList);
			List<AttachmentWrapper> wrapperList = convertToWrapper(attachedList);

			if(System.isFuture()) {
	            getCommentsBeforeFuture(caseIds);
	        }
	        else{
	        	getComments(JSON.serialize(caseIds), JSON.serialize(wrapperList));
	        }
	    }
	}

	public static List<Attachment> validateAttachments(List<Attachment> attList){
		List<Attachment> newList = new List<Attachment>{};
		for(Attachment att : attList){
			String ext = att.name.substringAfter('.');
			if(EXTENSIONS.contains(ext) && att.BodyLength < SIZE_LIMIT && att.ParentId.getSObjectType() == EmailMessage.getSObjectType()){
				newList.add(att);
			}
		}
		return newList;
	}

	public static List<Id> createAttachment(List<Attachment> attachedList){
		List<Attachment> caseAttachments = new List<Attachment>{};
		List<Id> caseIds = new List<Id>{};
		for(Attachment att : attachedList){
			EmailMessage tmpEmail = null;
			if(emailIdMap != null && !emailIdMap.isEmpty()){
				tmpEmail = emailIdMap.get(att.ParentId);
			}
			if(tmpEmail != null){
				Attachment tmp = new Attachment(
								body = att.body,
								ContentType = att.ContentType,
								Name = att.Name,
								ParentId  = tmpEmail.ParentId);
				caseAttachments.add(tmp);
				caseIds.add(tmpEmail.ParentId);
			}
		}

		if(caseAttachments != null){
			insert caseAttachments;	
		}
		return caseIds;
	}

	public static Map<Id, EmailMessage> mapAttToEmails(List<Attachment> attachedList){
		List<Id> emailIds = new List<Id>{};

		for(Attachment att : attachedList){
			emailIds.add(att.ParentId);
		}

		Map<Id, EmailMessage> emailMap = new Map<Id, EmailMessage>([SELECT Id, ParentId, TextBody
										FROM EmailMessage
										WHERE Id IN :emailIds]);
		return emailMap;
	}

	
	public static void updateComment(List<AttachmentWrapper> wrapperList, Map<Id, List<CaseComment>> caseToComment){
		Integer allowedSize = 2000;
		Map<Id, CaseComment> commentsToUpdate = new Map<Id, CaseComment>{};
		Map<Id, String> caseComments = new Map<Id, String>{};
		List<Id> emailIds = new List<Id>{};

		for(AttachmentWrapper att : wrapperList){
			emailIds.add(att.ParentId);
		}

		Map<Id, EmailMessage> emailMap = new Map<Id, EmailMessage>([SELECT Id, ParentId, TextBody
										FROM EmailMessage
										WHERE Id IN :emailIds]);


		for(AttachmentWrapper att : wrapperList){
			if(att.ParentId.getSObjectType() == EmailMessage.getSObjectType() && !emailMap.isEmpty()){
				String commentBody = MBREmailMessageTriggerHelper.getCommentBody(emailMap.get(att.ParentId), 0);
				if(!caseToComment.get(emailMap.get(att.ParentId).ParentId).isEmpty()){
					for(CaseComment cc : caseToComment.get(emailMap.get(att.ParentId).ParentId)){

						Boolean rightComment = false;
						if(commentBody.length() > 100){
							rightComment = cc.CommentBody.contains(Label.MBREmailCommentAuthor) && cc.CommentBody.contains(commentBody.substring(0, 100));
						}
						else{
							rightComment = cc.CommentBody.contains(Label.MBREmailCommentAuthor) && cc.CommentBody.contains(commentBody.trim());
						}

						if(rightComment || cc.CommentBody.equals(commentBody.trim())){

							String tmp = caseComments.get(cc.Id);
							if(tmp == null || tmp == ''){
								tmp = '\n' + Label.MBR_See_Attachment + ' ' + att.name;
							}
							else{
								tmp += '\n' + Label.MBR_See_Attachment + ' ' + att.name;
							}
							caseComments.put(cc.Id, tmp);
	            			commentsToUpdate.put(cc.Id, cc);
						}
					}
				}
			}
		}

		if(!commentsToUpdate.isEmpty()){
			for(CaseComment cc : commentsToUpdate.values()){

				String tmp = caseComments.get(cc.Id);
				if((cc.CommentBody.length() + tmp.length()) < allowedSize){
	                cc.CommentBody += tmp;
	            }
	            else{
	            	Integer leftChars = allowedSize - tmp.length();
	            	cc.CommentBody = cc.CommentBody.substring(0, leftChars) + tmp;

	            }

	        }
			update commentsToUpdate.values();
		}
	}

	public static void getCommentsBeforeFuture(List<Id> caseIds){

		Map<Id, List<CaseComment>> caseToComment = new Map<Id, List<CaseComment>>();

		List<CaseComment> commentList = [
									SELECT Id, CommentBody, ParentId
									FROM CaseComment
									WHERE ParentId In :caseIds
									AND IsPublished = true];
		if(!commentList.isEmpty()){
			for(CaseComment cc : commentList){
				List<CaseComment> ccList = caseToComment.get(cc.ParentId);
				if(ccList == null){
					ccList = new List<CaseComment>{cc};
				}
				else{
					ccList.add(cc);
				}
				caseToComment.put(cc.ParentId, ccList);
			}
		}
	}

	public static List<AttachmentWrapper> convertToWrapper(List<Attachment> attachedList){
			List<AttachmentWrapper> wrapperList = new List<AttachmentWrapper>{};
			for(Attachment att : attachedList){
				wrapperList.add(new AttachmentWrapper(att));
			}
			return wrapperList;
	}

	@future
	public static void getComments(String caseIdList, String attachmentList){

		Map<Id, List<CaseComment>> caseToComment = new Map<Id, List<CaseComment>>();
		List<Id> caseIds = (List<Id>)JSON.deserialize(caseIdList, List<Id>.class);
		List<AttachmentWrapper> wrapperList = (List<AttachmentWrapper>)JSON.deserialize(attachmentList, List<AttachmentWrapper>.class);
//		String emailTag = '%' + Label.MBREmailCommentAuthor + '%';
		if(!caseIds.isEmpty()){
			List<CaseComment> commentList = [
										SELECT Id, CommentBody, ParentId
										FROM CaseComment
										WHERE ParentId In :caseIds
										AND IsPublished = true];
			if(commentList!=null && !commentList.isEmpty()){
				for(CaseComment cc : commentList){
					List<CaseComment> ccList = caseToComment.get(cc.ParentId);
					if(ccList == null){
						ccList = new List<CaseComment>{cc};
					}
					else{
						ccList.add(cc);
					}
					caseToComment.put(cc.ParentId, ccList);
				}
			}
		}
		if(!wrapperList.isEmpty() && !caseToComment.isEmpty()){
			updateComment(wrapperList, caseToComment);
		}
	}

	public class AttachmentWrapper{
		public String name {get; set;}
		public Id ParentId {get; set;}

		public AttachmentWrapper(Attachment att){
			this.name = att.Name;
			this.ParentId = att.ParentId;
		}

	}


}