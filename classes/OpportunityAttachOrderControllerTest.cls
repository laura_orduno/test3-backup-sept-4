@isTest
public class OpportunityAttachOrderControllerTest {
@isTest
    private static void getOrders(){
        String orderId=OrdrTestDataFactory.singleMethodForOrderId();
        PageReference pageRef = Page.OpportunityAttachOrderPage;
        Test.setCurrentPage(pageRef);
        Order ordObj=[select accountid,opportunityid from order where id=:orderId];
        ApexPages.currentPage().getParameters().put('id', ordObj.opportunityid );
        Opportunity opObj=[select id from opportunity where id=:ordObj.opportunityid];
             
        Test.startTest();
        
        ApexPages.StandardController sc = new ApexPages.standardController(opObj);
        OpportunityAttachOrderController cntrl=new OpportunityAttachOrderController(sc);
        cntrl.getOrders();
        cntrl.newOrder();
        cntrl.attachOrder();
        Test.stopTest();
    }
}