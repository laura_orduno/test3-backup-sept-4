/**************************************
    @author         : Arpit Goyal
    Date            : 4-Jan-2017
    Sprint/Release  : MVP release - Sprint 11
    Purpose         : Helper class for OCOM_CustomEligibilityHook used in check readiness

**************************************/
public class OCOM_CustomEligibilityHookHelper{
    public static boolean firstRun = true;
    public static Id addedOli ; //Used to store the id of the OLI which wa added in the cart
    
    /******************************
        @author         : Arpit Goyal
        Date            : 4-Jan-2017
        Sprint/Release  : MVP release - Sprint 11 - Checkout readiness
        params          : Map<String, Object> , Id 
        return          : void
        Purpose         : Handles OLi addition status changes
    ******************************/
    public static void checkoutReadinessOnOliAddition(Map<String, Object> oliMap, Id oliId){
        if(oliId == null){
                vlocity_cmt.JSONResult results1 = (vlocity_cmt.JSONResult )oliMap.get(vlocity_cmt.JSONResult.JSON_KEY_RESULT);
                List<vlocity_cmt.JSONMessage> messageList1 ;
                if(null !=  results1 && null != results1.messages){
                     messageList1 = (List<vlocity_cmt.JSONMessage>)results1.messages;
                     system.debug('messageList1 --->' + messageList1 );             
               }
               Map<String, vlocity_cmt.JSONAction> actionObjMap ;
               for(vlocity_cmt.JSONMessage cRecords: messageList1 ){
                   system.debug('cRecords ---> '+ cRecords);
                   actionObjMap =  (Map<String, vlocity_cmt.JSONAction>)cRecords.actions;
                   system.debug('actionObjMap.get(DETAILS)---->' + actionObjMap.get('DETAILS'));
               }
               if(actionObjMap!=null && !actionObjMap.isEmpty()){
               if(actionObjMap.get('DETAILS') == null) {
                   system.debug('results1--->' + results1);
                   List<vlocity_cmt.JSONRecord> recordsList1  =  (List<vlocity_cmt.JSONRecord>)results1.records ;
                   system.debug('recordsList1  --->' + recordsList1  );
                   Map<String, vlocity_cmt.JSONAction> actionObjMap1 ;  
                   for(vlocity_cmt.JSONRecord cRecords1: recordsList1  ){
                       system.debug('cRecords1 ---> '+ cRecords1);
                       actionObjMap1 =  (Map<String, vlocity_cmt.JSONAction>)cRecords1.actions;
                       system.debug('actionObjMap1 ---->' + actionObjMap1 );
                   }
                   for(vlocity_cmt.JSONAction actionObj : actionObjMap1.values()){
                       system.debug('actionObj ---> '+ actionObj );
                       vlocity_cmt.JSONAction.JSONRemoteAction remoteAction = actionObj.remote ;
                       Map<String,Object> paramMap1 = actionObj.remote.params;
                       system.debug('paramMap1  ------>'+ paramMap1);
                       Id oliId2 = (Id)paramMap1.get('itemId');
                       system.debug('oliId2 ------>'+ oliId2 );
                       oliId = oliId2 ;          
                       
                   }
                   }
                else 
               {
                       for(vlocity_cmt.JSONAction actionObj : actionObjMap.values()){
                           system.debug('actionObj ---> '+ actionObj );
                           system.debug('actionObj.client ------>'+ actionObj.client);
                           vlocity_cmt.JSONAction.JSONClientAction clientAction = actionObj.client ;
                           Map<String,Object> paramMap = actionObj.client.params;
                           system.debug('paramMap  ------>'+ paramMap.values());
                           Id oliId1 = (Id)paramMap.get('id');
                           system.debug('oliId1 ------>'+ oliId1 );
                           oliId = oliId1 ;//Finally oli id fetched
                       }
                }
               } 
              
         }   
         
         Set<OrderItem> masterOliUpdatedSet = new Set<OrderItem>();
         if(oliMap !=  null && oliId != null ){
             addedOli = oliId ;
             OrderItem  oliOb = [Select Id, OrderID, IsMultiSiteOLI__c From OrderItem where Id =:oliId ] ;
             Id orderid = oliOb.OrderID ;
             if(oliOb.IsMultiSiteOLI__c){
                     //Fetching all Olis on order
                     List<OrderItem> allOliOnOrder = [Select Id, isConfigurationComplete__c , vlocity_cmt__ParentItemId__c, Service_Address__c  from OrderItem where OrderId =: orderid ];
                     system.debug('allOliOnOrder  --->All oli on order --->' + allOliOnOrder );
                     //Fetching all unconfigured oli on order
                     List<OrderItem> allUnConfiguredOliOnOrder = new List<OrderItem>();
                     for(OrderItem allOliObj : allOliOnOrder ){
                         if(!allOliObj.isConfigurationComplete__c ){
                             allUnConfiguredOliOnOrder.add(allOliObj);// All Unconfigured oli fetched
                         } 
                     }
                     
                     Map<Id, List<OrderItem>> allUnconfiguredOLIonAddressMap =  new  Map<Id, List<OrderItem>>(); // Map of ServiceAddressId v/s List of unconfigured oli
                     Map<Id, List<OrderItem>> allConfiguredOLIonAddressMap =  new  Map<Id,  List<OrderItem>>(); // Map of ServiceAddressId v/s List of Configured oli
                     
                     if(allOliOnOrder.size() > 0){
                         for(OrderItem oliObj : allOliOnOrder ){
                         if(!oliObj.isConfigurationComplete__c &&  oliObj.vlocity_cmt__ParentItemId__c == null ){
                             if(allUnconfiguredOLIonAddressMap.containskey(oliObj.Service_Address__c)){
                                 allUnconfiguredOLIonAddressMap.get(oliObj.Service_Address__c).add(oliObj);
                             }
                             else{
                                 allUnconfiguredOLIonAddressMap.put(oliObj.Service_Address__c,new list<orderitem>{oliObj});
                             }
                         
                          } else {
                              if(allConfiguredOLIonAddressMap.containskey(oliObj.Service_Address__c)){
                                 allConfiguredOLIonAddressMap.get(oliObj.Service_Address__c).add(oliObj);
                              }
                              else{
                                 allConfiguredOLIonAddressMap.put(oliObj.Service_Address__c,new list<orderitem>{oliObj});
                              }
                          
                              }   
                                                                                                                    
                         }
                     }
                     system.debug('allUnconfiguredOLIonAddressMap--->All Unconfigured oli vs address --->' + allUnconfiguredOLIonAddressMap);
                     system.debug('allConfiguredOLIonAddressMap--->All configured oli vs address --->' + allConfiguredOLIonAddressMap);
                     system.debug('allUnConfiguredOliOnOrder--->All Unconfigured oli on order --->' + allUnConfiguredOliOnOrder);
                     //Checking if incoming Oli has not been configured
                     
                     //Case 1 : if no unconfigured olis are present on order and incoming oli is not configured ie this is the first unconfigured oli in cart
                     //Case 2 : if unconfigured olis are present on order and incoming oli is completly configured
                     //Checking for isRequired and value attributes from oli json
        
                     vlocity_cmt.JSONResult results = (vlocity_cmt.JSONResult)oliMap.get(vlocity_cmt.JSONResult.JSON_KEY_RESULT);
                     system.debug('results --->' + results );
                     OrderItem oliObj  = [Select Id, Service_Address__c, isConfigurationComplete__c from OrderItem where id =: oliId]; 
                     //ServiceAddressOrderEntry__c saoObj = [Select Id, isSiteReadyForCheckout__c from ServiceAddressOrderEntry__c  where Address__c =: oliObj.Service_Address__c];
                     Map<Id, OrderItem> toBeUpdatedOliMapUnconfigured= new Map<Id, OrderItem>(); //for Unconfigured Oli map on id to be send for updation 
             Map<Id, OrderItem> toBeUpdatedOliMapConfigured= new Map<Id, OrderItem>();
             List<vlocity_cmt.JSONMessage> messageList ;
             if(null !=  results && null != results.messages){
                 messageList = (List<vlocity_cmt.JSONMessage>)results.messages;
                 system.debug('messageList --->' + messageList );             
             }
             Boolean IsInsideUnconfigured = false;
             for(vlocity_cmt.JSONMessage cRecords: messageList ){
                  String strMsg = cRecords.message;                 
                  system.debug('strMsg --->' + strMsg );
                  if(strMsg.containsIgnoreCase('Required attribute missing')){// Incoming oli is not configured
                      system.debug('Inside unconfigured part');
                      IsInsideUnconfigured  = true ;
                      oliObj.isConfigurationComplete__c =  false; 
                      toBeUpdatedOliMapUnconfigured.put(oliObj.Id, oliObj);
                      system.debug('toBeUpdatedOliMapUnconfigured--->' + toBeUpdatedOliMapUnconfigured); 
                      masterOliUpdatedSet.add(oliObj); //to be used later                                                                                               
                      update toBeUpdatedOliMapUnconfigured.values();
                  }else if(strMsg.containsIgnoreCase('Successfully') && !IsInsideUnconfigured ){// Incoming oli is configured
                      system.debug('Inside completed part'); 
                      oliObj.isConfigurationComplete__c=  true;
                      toBeUpdatedOliMapConfigured.put(oliObj.Id, oliObj);
                      system.debug('toBeUpdatedOliMapConfigured--->' + toBeUpdatedOliMapConfigured);
                      masterOliUpdatedSet.add(oliObj); //to be used later 
                      update toBeUpdatedOliMapConfigured.values();  

                  }
             }
                 
                 
    
                 Set<ServiceAddressOrderEntry__c> saoSet = new Set<ServiceAddressOrderEntry__c>();
                 Set<Id> addressIdSet = new Set<Id>();
                 List<OrderItem> masterOliUpdatedOli = new List<OrderItem>();
                  masterOliUpdatedOli.addAll(masterOliUpdatedSet);
                  system.debug('Add, masterOliUpdatedOli---------->' + masterOliUpdatedOli);
                  ID masterAddress ;
                  if(masterOliUpdatedOli.size() != 0 ){
                        masterAddress = masterOliUpdatedOli[0].Service_Address__c ;
                        system.debug('Add, masterAddress ---------->' + masterAddress );
                        addressIdSet.add(masterAddress);
                  }
                 
                 
                 List<ServiceAddressOrderEntry__c> soaList = [Select Id, isSiteReadyForCheckout__c from ServiceAddressOrderEntry__c  where Address__c IN: addressIdSet ] ; // List of all SOA from addresses
                 List<ServiceAddressOrderEntry__c> saoToBeUpdatedList =  new List<ServiceAddressOrderEntry__c>();
                 //When incoming oli is unconfigured and there are no previous unconfigured oli present in the cart
                 if(toBeUpdatedOliMapUnconfigured.size() != 0 /*&& allUnConfiguredOliOnOrder.size() == 0*/){
                    for(OrderItem oliObjUnConfig : toBeUpdatedOliMapUnconfigured.values()){
                         addressIdSet.add(oliObjUnConfig.Service_Address__c);                 
                     }
                     system.debug('addressIdSet for unconfigured oli--->' + addressIdSet );  
                     saoSet.addAll(soaList);
                     system.debug('saoSet---> All SAO objects to be updated for Unconfiguration----->' + saoSet);  
                     for(ServiceAddressOrderEntry__c saoToBeUpdated : saoSet){
                         saoToBeUpdated.isSiteReadyForCheckout__c  =  false; //Updating SAO object
                         saoToBeUpdatedList.add(saoToBeUpdated);
                     }
                     system.debug('saoToBeUpdatedList--->' + saoToBeUpdatedList); 
                     update saoToBeUpdatedList ;
                 }
                 
                 //When incoming oli is fully configured and there are pre-existing unconfigured oli present for that address 
                 else if(toBeUpdatedOliMapConfigured.size() != 0 && allUnConfiguredOliOnOrder.size() != 0 && allUnconfiguredOLIonAddressMap.get(masterAddress) != null){//Check if incoming OLI's address is part of unconfigured address map 
                     system.debug(' Do noting as such SAO will already be in unconfigured state');
                 }//When incoming oli is fully configured and there are no pre-existing unconfigured oli present for that address
                 else if(toBeUpdatedOliMapConfigured.size() != 0  && allUnConfiguredOLIonAddressMap.get(masterAddress) == null){
                     for(OrderItem oliObjConfig : toBeUpdatedOliMapConfigured.values()){
                         addressIdSet.add(oliObjConfig.Service_Address__c);                 
                     }
                     system.debug('soaList--->' + soaList);  
                     saoSet.addAll(soaList);
                     system.debug('saoSet---> All SAO objects to be updated for configuration----->' + saoSet);  
                     for(ServiceAddressOrderEntry__c saoToBeUpdated : saoSet){
                         saoToBeUpdated.isSiteReadyForCheckout__c  =  true; //Updating SAO object
                         saoToBeUpdatedList.add(saoToBeUpdated);
                     }
                     system.debug('saoToBeUpdatedList--->' + saoToBeUpdatedList); 
                     update saoToBeUpdatedList ;
                 }
                 }
            }              
          }
        
      
      
      /******************************
        @author         : Arpit Goyal
        Date            : 4-Jan-2017
        Sprint/Release  : MVP release - Sprint 11 - Checkout readiness
        params          : Map<String, Object> , Id 
        return          : void
        Purpose         : Handles OLi updation and deletion status changes
    ******************************/
      public static void checkoutReadinessOnOliUpdation(List<OrderItem> oliList, Set<Id> deletedOliIDSet , Id addedOliId){
      
      
      
      Set<OrderItem> masterOliUpdatedSet =  new Set<OrderItem>();
      Boolean deleteCase = false ;
      system.debug('deletedOliIDSet.size()--->' + deletedOliIDSet.size());
      system.debug('addedOliId--->' + addedOliId); 
      //Get readiness of incoming oli
          
              if(firstRun){
                  if(oliList != null){
                      Boolean newUnconfiguredInsertCase =  false ; //Flag for a special case when cart doesn't already contains an unconfigured oli
                      Set<ServiceAddressOrderEntry__c> saoSetTobeUpdated =  new Set<ServiceAddressOrderEntry__c>();
                      //Preparing  map of oliID vs  saoObject          
                      Map<String, Id> smbMap  =  new Map<String, Id>();// Map of oli id v/s service address id string
                      Map<Id, ServiceAddressOrderEntry__c> saoMap =  new Map<Id, ServiceAddressOrderEntry__c>();// Map of oli id v/s its sao object
                      system.debug('oliList----->' + oliList);
                       
                      for(OrderItem oliObj : oliList){
                          smbMap.put((String)oliobj.Id , oliobj.Service_Address__c);
                      }
                      system.debug('smbMap---------->' + smbMap);
                      Set<ServiceAddressOrderEntry__c> saoSet = new Set<ServiceAddressOrderEntry__c>();
                      Map<Id, ServiceAddressOrderEntry__c> smbAddressSAOMap = new Map<Id, ServiceAddressOrderEntry__c>{};                  
                      for(ServiceAddressOrderEntry__c saoObj : [Select Id, Address__c, isSiteReadyForCheckout__c from ServiceAddressOrderEntry__c where Address__c IN :  smbMap.values()]){
                          smbAddressSAOMap.put(saoObj.Address__c , saoObj );
                      }
                      system.debug('smbAddressSAOMap---------->' + smbAddressSAOMap);
                     
                      
                      if(deletedOliIDSet.size() == 0 ){
                          for(OrderItem oliObj : oliList){
                              String jsonString = oliObj.vlocity_cmt__JSONAttribute__c  ;
                              if(jsonString != null && !deletedOliIDSet.contains(oliObj.id) && addedOliId != oliObj.Id ){ //Prevent the execution if oliIds are either deleted or added
                                  
                                  
                                  Map<String, Object> attributes = (Map<String, Object>) JSON.deserializeUntyped(jsonString);
                                  for (String key : attributes.keySet()) {
                                            for (Object attribute : (List<Object>) attributes.get(key)) {
                                                 Map<String, Object> attributeMap = (Map<String, Object>) attribute;
                                                 
                                                 Boolean attributeNameIsRequired = (Boolean) attributeMap.get('isrequired__c');
                                                 system.debug('attributeNameIsRequired ----->' + attributeNameIsRequired );//Got isRequired__c value
                                                 
                                                 if(null != attributeNameIsRequired ){
                                                     if(attributeNameIsRequired){//Enter only if a product requires a configuration
                                                         Map<String, Object> runTimeInfoMap = (Map<String, Object>) attributeMap.get('attributeRunTimeInfo');
                                                         system.debug('runTimeInfoMap ----->' + runTimeInfoMap );
                                                         Object attributeValueRunTimeInfo = (Object) runTimeInfoMap.get('value');  
                                                         if(runTimeInfoMap.keySet().contains('value') && attributeValueRunTimeInfo != null){
                                                             system.debug('Inside complete case ----->');
                                                             oliObj.isConfigurationComplete__c =  true;
                                                             system.debug('oliObj inside complete case---------->' + oliObj); 
                                                             masterOliUpdatedSet.add(oliObj); //to be used later                                                                                               
                                                             ServiceAddressOrderEntry__c soaToBeUpdated = smbAddressSAOMap.get(oliObj.Service_Address__c);
                                                             if(soaToBeUpdated != null){
                                                                 soaToBeUpdated.isSiteReadyForCheckout__c = true ;                                                 
                                                                 saoSetTobeUpdated.add(soaToBeUpdated);    
                                                             }
                                                             system.debug('masterOliUpdatedSet.size()---------->' + masterOliUpdatedSet.size());                                                                                                                                           
                                                         }else if( runTimeInfoMap.keySet().contains('value') && attributeValueRunTimeInfo == null){
                                                            system.debug('Inside incomplete case----->');
                                                            newUnconfiguredInsertCase  = true;
                                                            deleteCase = true ;
                                                            oliObj.isConfigurationComplete__c =  false;
                                                            system.debug('oliObj inside incomplete case---------->' + oliObj); 
                                                            masterOliUpdatedSet.add(oliObj); //to be used later                                                   
                                                            ServiceAddressOrderEntry__c soaToBeUpdated = smbAddressSAOMap.get(oliObj.Service_Address__c);
                                                            if(soaToBeUpdated != null){
                                                                soaToBeUpdated.isSiteReadyForCheckout__c = false ;
                                                                saoSetTobeUpdated.add(soaToBeUpdated);                                                        
                                                            }
                                                            system.debug('masterOliUpdatedSet.size()---------->' + masterOliUpdatedSet.size());                                                                                              
                                                         }
                                                     } 
                                                 }
                                                 
                                             }
                                             system.debug('saoSetTobeUpdated ---------->' + saoSetTobeUpdated );
                                             //update saoListTobeUpdated ;
                                          } 
                                                                     
                                      }  
                                  }
                              }
                              
                              
                              
                               //Fetch all unconfigured oli from order
                              Set<Id> orderIdSet = new  Set<Id>();
                              Map<Id, List<OrderItem>> allUnconfiguredOLIonAddressMap =  new  Map<Id, List<OrderItem>>(); // Map of ServiceAddressId v/s List of unconfigured oli
                              Map<Id, List<OrderItem>> allConfiguredOLIonAddressMap =  new  Map<Id,  List<OrderItem>>(); // Map of ServiceAddressId v/s List of Configured oli
                              for(OrderItem oliObj : oliList){                          
                                    orderIdSet.add(oliObj.orderId);//Set of orderIDs from Oli
                                    system.debug('orderIdSet ---------->' + orderIdSet);                
                              }
                              List<List<Id>> listOfServiceAddress =  new  List<List<Id>>() ; //List of service address 
                              List<ServiceAddressOrderEntry__c> addressList = [Select id, Order__c, isSiteReadyForCheckout__c , Address__c from  ServiceAddressOrderEntry__c where Order__c IN : orderIdSet ];              
                              system.debug('addressList ---------->' + addressList );  
                              
                              List<Id> smbAddressIdList = new List<Id>();
                              for(ServiceAddressOrderEntry__c saoObj : addressList ){
                                  smbAddressIdList.add(saoObj.Address__c );
                              } 
                              system.debug('smbAddressIdList---------->' + smbAddressIdList);
                              
                              
                              
                              List<OrderItem> allOlis = [Select Id,  isConfigurationComplete__c , vlocity_cmt__ParentItemId__c, Service_Address__c  from OrderItem where OrderID IN :  orderIdSet ];
                              system.debug('allOlis ---------->' + allOlis );
                             
                              for(OrderItem oliObj : allOlis ){
                                  
                                  if(!oliObj.isConfigurationComplete__c &&  oliObj.vlocity_cmt__ParentItemId__c == null ){
                                     if(allUnconfiguredOLIonAddressMap.containskey(oliObj.Service_Address__c)){
                                         allUnconfiguredOLIonAddressMap.get(oliObj.Service_Address__c).add(oliObj);
                                     }
                                     else{
                                         allUnconfiguredOLIonAddressMap.put(oliObj.Service_Address__c,new list<orderitem>{oliObj});
                                     }
                                 
                                  } else if(oliObj.isConfigurationComplete__c &&  oliObj.vlocity_cmt__ParentItemId__c == null ) {
                                      if(allConfiguredOLIonAddressMap.containskey(oliObj.Service_Address__c)){
                                         allConfiguredOLIonAddressMap.get(oliObj.Service_Address__c).add(oliObj);
                                      }
                                      else{
                                         allConfiguredOLIonAddressMap.put(oliObj.Service_Address__c,new list<orderitem>{oliObj});
                                      }
              
                                      }                                                                                     
                              }
                              List<OrderItem> masterOliUpdatedOli = new List<OrderItem>();
                              masterOliUpdatedOli.addAll(masterOliUpdatedSet);
                              system.debug('masterOliUpdatedOli---------->' + masterOliUpdatedOli);
                              ID masterAddress ;
                              if(masterOliUpdatedOli.size() != 0 ){
                                    masterAddress = masterOliUpdatedOli[0].Service_Address__c ;
                                    system.debug('masterAddress ---------->' + masterAddress );
                              }
                              List<ServiceAddressOrderEntry__c> masterSaoObject ;
                              if(masterAddress != null)
                                   masterSaoObject = [Select id, Address__c, isSiteReadyForCheckout__c  From ServiceAddressOrderEntry__c  where Address__c =: masterAddress]; 
                              
                              system.debug('allUnconfiguredOLIonAddressMap---------->' + allUnconfiguredOLIonAddressMap);// Map of service address v/s List of unconfigured OLi
                              system.debug('allConfiguredOLIonAddressMap---------->' + allConfiguredOLIonAddressMap);// Map of service address v/s List of configured OLi
                              
                              //Boolean enteredInUnconfiguredScope = false ;
                              Set<ID> unconfiguredServiceAddressObject = new Set<ID>();
                              for(Id key : allUnconfiguredOLIonAddressMap.keySet()){
                                  if(allUnconfiguredOLIonAddressMap.get(key) != null){
                                      unconfiguredServiceAddressObject.add(key);
                                  }
                              }
                              
                              system.debug('unconfiguredServiceAddressObject ---------->' + unconfiguredServiceAddressObject );
                                                                        
                              Map<Id, ServiceAddressOrderEntry__c> toBeUpdatedSAOMap =  new Map<Id, ServiceAddressOrderEntry__c>();                              
                              List<ServiceAddressOrderEntry__c> toBeUpdatedSAOList = new List<ServiceAddressOrderEntry__c>();
                              
                              if(null != unconfiguredServiceAddressObject && null != masterSaoObject ){
                              if(unconfiguredServiceAddressObject.size() != 0  && masterSaoObject.size() != 0){//Getting the uncofigured service address ids
                                 for(ServiceAddressOrderEntry__c saoObj : masterSaoObject){ 
                                     system.debug('allUnconfiguredOLIonAddressMap.get(masterAddress)---------->' + allUnconfiguredOLIonAddressMap.get(masterAddress ));
                                     //system.debug('(allUnconfiguredOLIonAddressMap.get(masterAddress).size()---------->' + (allUnconfiguredOLIonAddressMap.get(masterAddress).size()));
                                     if(allUnconfiguredOLIonAddressMap.get(masterAddress) != null && !masterOliUpdatedOli[0].isConfigurationComplete__c &&  allUnconfiguredOLIonAddressMap.get(masterAddress).size() > 1){//IF the incoming oli was not the last one
                                         saoObj.isSiteReadyForCheckout__c  =  false ;                                         
                                         toBeUpdatedSAOMap.put(saoObj.Id, saoObj); 
                                         system.debug('toBeUpdatedSAOMap 1---------->' + toBeUpdatedSAOMap); 
                                     }
                                 }
                                 
                                } 
                              }
                              
                                                                                           
                              if(masterAddress != null && masterSaoObject.size() != 0 ){//That means the incoming oli is not part of unconfigured oli map
                                List<ServiceAddressOrderEntry__c> masterSaoObj = [Select id, Address__c, isSiteReadyForCheckout__c  From ServiceAddressOrderEntry__c  where Address__c =: masterAddress];
                                system.debug('allConfiguredOLIonAddressMap.get(masterAddress)---------->' + allConfiguredOLIonAddressMap.get(masterAddress));
                                system.debug('masterSaoObject---------->' + masterSaoObject);
                                if(allConfiguredOLIonAddressMap.get(masterSaoObject[0].Address__c) != null
                                    &&  (allUnconfiguredOLIonAddressMap.get(masterAddress) == null 
                                    || masterOliUpdatedOli[0].isConfigurationComplete__c) ){//13-Jan-2017 : modified this condition from get(masterAddress).size() != 0 
                                    if(allUnconfiguredOLIonAddressMap.get(masterAddress) != null){
                                        for(OrderItem oli : allUnconfiguredOLIonAddressMap.get(masterAddress)){
                                            if(masterSaoObject.size() > 0){
                                                if(oli.isConfigurationComplete__c == masterOliUpdatedOli[0].isConfigurationComplete__c){
                                                    masterSaoObject[0].isSiteReadyForCheckout__c  = true;
                                                    toBeUpdatedSAOMap.put(masterSaoObj[0].Id, masterSaoObject[0]); 
                                                    
                                                    system.debug('toBeUpdatedSAOMap 2---------->' + toBeUpdatedSAOMap);
                                                }
                                            }
                                        }   
                                    }
                                } 
                              }
                              
                              //CASE :: If an OLI was sent into unonfigured state then status of SAO shoud be chnaged
                              //ie : configured map will contain the incoming oli but not master oli will have isConfigurationComplete__c =  false
                              if(allUnconfiguredOLIonAddressMap.get(masterAddress) ==  null && allConfiguredOLIonAddressMap.get(masterAddress) != null){
                                  for(OrderItem oli : allConfiguredOLIonAddressMap.get(masterAddress)){
                                      if(masterSaoObject.size() > 0){
                                          if(oli.id == masterOliUpdatedOli[0].id && !masterOliUpdatedOli[0].isConfigurationComplete__c ){
                                              system.debug('Inside ---> If an OLI was sent into unonfigured state then status of SAO shoud be chnaged');    
                                              masterSaoObject[0].isSiteReadyForCheckout__c = false;
                                              toBeUpdatedSAOMap.put(masterSaoObject[0].Id, masterSaoObject[0]); 
                                              system.debug('toBeUpdatedSAOMap 2.1---------->' + toBeUpdatedSAOMap);
                                          }
                                      }
                                  }
                              }
                              
                                  
                              //CASE : If incoming oli is the last unconfigurd OLI on location to be configured
                              // Ie masterOLi would have its isConfigurationComplete__c = true
                              
                              if(masterOliUpdatedOli.size() != 0 && masterOliUpdatedOli[0].isConfigurationComplete__c && allUnconfiguredOLIonAddressMap.get(masterAddress) != null){                                  
                                  if(allUnconfiguredOLIonAddressMap.get(masterAddress).size()== 1){
                                      system.debug('Inside ---> If incoming oli is the last unconfigurd OLI on location to be configured');    
                                      //List<ServiceAddressOrderEntry__c> masterSAO = [Select id, isSiteReadyForCheckout__c, Address__c  From ServiceAddressOrderEntry__c  where Address__c =: masterAddress ] ;
                                      if(masterSaoObject.size() != 0){
                                          masterSaoObject[0].isSiteReadyForCheckout__c = true ;
                                          if(saoSetTobeUpdated.contains(masterSaoObject[0])){
                                              toBeUpdatedSAOMap.put(masterSaoObject[0].Id, masterSaoObject[0]); 
                                              system.debug('toBeUpdatedSAOMap 3---------->' + toBeUpdatedSAOMap);
                                          }
                                      }    
                                  }
                              }
                              
                              
                              update  toBeUpdatedSAOMap.values() ;
                              
                              
                              //Delete case ************
                              Set<ServiceAddressOrderEntry__c> tobeUpdatedSAOforDeletion =  new Set<ServiceAddressOrderEntry__c>();
                              
                              if(deletedOliIDSet.size() != 0){
                                 
                                 //fetching deleted OLi 
                                 OrderItem deletedOli = [Select Id, isDeleted, isConfigurationComplete__c , OrderID, Service_Address__c  from OrderItem where ID IN : deletedOliIDSet ALL ROWS];
                                 
                                 if(deletedOli != null){
                                     ServiceAddressOrderEntry__c saoObj = [Select Id, Address__c, isSiteReadyForCheckout__c 
                                                                           from ServiceAddressOrderEntry__c 
                                                                           where Address__c =: deletedOli.Service_Address__c and Order__c =: deletedOli.OrderId];
                                     Integer oliCountAfterDeletion = [Select count() from OrderItem where Service_Address__c =: deletedOli.Service_Address__c  ];                            
                                     //Fetch status of its SOA object
                                     system.debug('deletedOli ---------->' + deletedOli );                                    
                                     system.debug('saoObj ---------->' + saoObj );
                                     if(deletedOli.isConfigurationComplete__c && oliCountAfterDeletion != 0 ){
                                         system.debug('Case 1');
                                         system.debug('No change in status is required');//As all other olis that are present on the location may or may not be configured
                                     }else if(oliCountAfterDeletion == 0){// i.e This was the last oli on the SOA                                 
                                         system.debug('Case 2');
                                         saoObj.isSiteReadyForCheckout__c = false;
                                         tobeUpdatedSAOforDeletion.add(saoObj);                                             
                                     }else if(!deletedOli.isConfigurationComplete__c && allUnconfiguredOLIonAddressMap.get(saoObj.Address__c) != null ){//For the case when deleted Oli was unconfigured and more unconfigured oli were present on its SAO
                                         system.debug('Case 3');
                                         saoObj.isSiteReadyForCheckout__c = false; 
                                         tobeUpdatedSAOforDeletion.add(saoObj);
                                     }else{//Last Case : when the OLi was unconfigured and all other were configured
                                         system.debug('Case 4');
                                         saoObj.isSiteReadyForCheckout__c = true; 
                                         tobeUpdatedSAOforDeletion.add(saoObj);
                                     }
                                     system.debug('tobeUpdatedSAOforDeletion---------->' + tobeUpdatedSAOforDeletion);
                                     List<ServiceAddressOrderEntry__c> tobeUpdatedSAOforDeletionList = new List<ServiceAddressOrderEntry__c>();
                                     tobeUpdatedSAOforDeletionList.addAll(tobeUpdatedSAOforDeletion);
                                     update tobeUpdatedSAOforDeletionList ;
                                 }
                                 
                                                                  
                              }
                              
                              
                              }                          
                              
                            }                
                                                
                  
                  firstRun = false ;
             
            }
                              
}