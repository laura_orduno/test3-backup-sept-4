/**
    @author Vlocity
    @Version 3/11/2016
    Controller class for OCOM_PAC_MACD
*/

public without sharing class OCOM_PAC_MACD_Controller {

/* Commented by Arvind as code moved into OCOM_InOrderFlowPACController..
    public Account MoveOutAccount  {get;set;}
    public Account MoveInAccount  {get;set;}
    public ServiceAddressData SAData {get;set;}
    public SMBCare_Address__c smbCareAddr;
    //public String selectedContactId{get;set;}
    public String ContId{get;set;}
    public String orderPath{get;set;}
    public String customerLocation{get;set;}
    public String typeOfOrder{get;set;}

    public OCOM_PAC_MACD_Controller(){
       Id acctId = System.currentPageReference().getParameters().get('id');
      if(acctId==null)
        acctId = System.currentPageReference().getParameters().get('aid');
       if(acctId!=null){
            MoveOutAccount = [select Id,parentId,Name from Account where id =  :acctId];
       }

       // Added by Aditya jamwal (IBM) -start
        SAData  = new ServiceAddressData();
       //SAData.isSearchStringRequired = true;
       smbCareAddr = new SMBCare_Address__c();
       smbCareAddr.Address_Type__c = 'Standard'; // Service Address Type defaults to Standard.
       smbCareAddr.Status__c='Valid';
       smbCareAddr.recordtypeid = Schema.SObjectType.SMBCare_Address__c.getRecordTypeInfosByName().get('Address').getRecordTypeId();
      //---End
      
       orderPath = 'Simple';
   }

    public PageReference CreateServiceAccount(){
        // here we will need to create service account
        Id accountId = MoveOutAccount.id;//'0015600000450H6';

        //system.debug('OCOM_PAC_MACD_Controller::CreateServiceAccount() orderPath: ' + orderPath);
        if (orderPath == 'Complex') {
            return new PageReference('/apex/smb_newQuoteFromAccount?aid='+accountId);
        }
        
        system.debug(' Moving out '+MoveOutAccount +' SAData.searchString '+SAData.searchString);
        OCOM_MACD_ServiceAccountAndAddressExt ext= new OCOM_MACD_ServiceAccountAndAddressExt(this);
        if (null !=MoveOutAccount.parentId && (string.isNotBlank(SAData.searchString) || SAData.isManualCapture)){
            //here we will get new service account
            system.debug('Inside with Manual Capture');
            //

            //  OCOM_MACD_ServiceAccountAndAddressExt ext= new OCOM_MACD_ServiceAccountAndAddressExt(this);
            //  MoveInAccount= ext.CreateServiceAccount(MoveOutAccount.parentId, smbCareAddr,SAData);
            MoveInAccount= ext.CreateServiceAccount(MoveOutAccount.Id, smbCareAddr,SAData);
            system.debug(' After Inside with Manual Capture'+MoveInAccount);
            //return new PageReference('/apex/c__MACDMove?id='+accountid +'#/OmniScriptType/MACD/OmniScriptSubType/Move/OmniScriptLang/English/ContextId/'+accountid+'+'+MoveInAccount.id+'/PrefillDataRaptorBundle//true').setRedirect(true);

            // this selectedContactId will be used for assigning contact
            Id contactId = accountId;
            if(ContId !=null){
               contactId = ContId;
            }

            //return new PageReference('/apex/c__MACDMove?moveInAccId='+ MoveInAccount.Id + '&id=' + MoveOutAccount.id  +'#/OmniScriptType/MACD/OmniScriptSubType/Move/OmniScriptLang/English/ContextId/'+accountid+'+'+MoveInAccount.id+ '+'+ContId+'/PrefillDataRaptorBundle/true').setRedirect(true);
            return  new PageReference('/apex/OCOM_InOrderFlowContact?id='+MoveInAccount.Id);
       } else {
           // Templorary Solution Vlocitiy July 11
           //MoveInAccount= ext.CreateServiceAccount(MoveOutAccount.Id, smbCareAddr,SAData);
            MoveInAccount= ext.CreateServiceAccount(MoveOutAccount.Id, smbCareAddr,SAData);

           //return new PageReference('/apex/c__MACDMove?id='+accountid+'#/OmniScriptType/MACD/OmniScriptSubType/Move/OmniScriptLang/English/ContextId/'+accountid+'/PrefillDataRaptorBundle//true').setRedirect(true);
           return  new PageReference('/apex/OCOM_InOrderFlowContact?id='+MoveInAccount.Id);
       }
    }

     // Vlocity Adding Create Order here
      public PageReference CreateOrder(){
              // here create order
              vlocity_cmt.VlocityOpenInterface vlOpenInt = new OCOM_HybridCPQUtil();
              Map<String, Object> options = new Map<String, Object>();
              Map<String, Object> input = new Map<String, Object>();
              Map<String, Object> output = new Map<String, Object>();

              // RCID Id
              //input.put('RCIDAcctId',);
              // Service Adderess
              input.put('ServiceAcctId', MoveOutAccount.Id);
              // Contact Id
              input.put('ContactId', ContId);

              vlOpenInt.invokeMethod('createOcomOrder', input, output, options);

              Id OrderId = (Id)output.get('OrderId');

              //return new PageReference('/apex/TESTING?ContId='+ContId + '&AccountId='+MoveOutAccount.Id);
              return new PageReference('/' + OrderId);

      }


    public PageReference onSelectContact() {
      return null;
    }

    //For sprint 3 review purposes -pete
    public PageReference redirectUser(){
      PageReference ref = new PageReference('/apex/OCOM_InOrderFlowContact?id='+MoveInAccount.Id);
      return ref;
    }

    public List<SelectOption> getOrderPathValues() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Simple','Simple'));
        options.add(new SelectOption('Complex','Complex'));
        return options;
    }

    public List<SelectOption> getCustomerLocationValues() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Current','Current'));
        options.add(new SelectOption('New','New'));
        return options;
    } 

    public List<SelectOption> getTypeOfOrderValues() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Move Services','Move Services'));
        options.add(new SelectOption('New Services','New Services'));
        return options;
    }     
    */    
}