/*
###########################################################################
# File..................: smb_Test_TTODS
# Version...............: 26
# Created by............: Vinay Sharma
# Created Date..........: 05-Dec-2013
# Last Modified by......: 
# Last Modified Date....: 
# Description...........: This is a test class to provide coverage to TTODS functionality
#
# Copyright (c) 2000-2012. Astadia, Inc. All Rights Reserved.
#
# Created by the Astadia, Inc. Modification must retain the above copyright notice. 
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any commercial purpose, without fee, and without a written
# agreement from Astadia, Inc., is hereby forbidden. Any modification to source
# code, must include this paragraph and copyright.
#
# Permission is not granted to anyone to use this software for commercial uses.
#
# Contact address: 2839 Paces Ferry Road, Suite 350, Atlanta, GA 30339
# Company URL : http://www.astadia.com
###########################################################################
*/
@isTest (SeeAllData = false)
private class smb_Test_TTODS {

    static testMethod void testSmb_TTODSControllerAndHelper() 
    {
        list<SMBCare_WebServices__c> smbCareWSList = new List<SMBCare_WebServices__c>();
        if(SMBCare_WebServices__c.getInstance('TTODS_EndPoint') == null)
        {
            SMBCare_WebServices__c oSMBCare = new  SMBCare_WebServices__c();
            oSMBCare.Name = 'TTODS_EndPoint';
            oSMBCare.Value__c = 'https://partnerservices-pt.telus.com:443/SOA/SMO/ProblemMgmt/TroubleTicketODSQueryService_v1_5_vs1_vs0';
            oSMBCare.Use_Soap_For_PT__c = false;
            smbCareWSList.add(oSMBCare);
        }
        
        if(SMBCare_WebServices__c.getValues('username') == null)
        {
            SMBCare_WebServices__c oSMBCare = new  SMBCare_WebServices__c();
            oSMBCare.Name = 'username';
            oSMBCare.Value__c = 'APP_SFDC';
            oSMBCare.Use_Soap_For_PT__c = false;
            smbCareWSList.add(oSMBCare);
        }
        
        if(SMBCare_WebServices__c.getValues('password') == null)
        {
            SMBCare_WebServices__c oSMBCare = new  SMBCare_WebServices__c();
            oSMBCare.Name = 'password';
            oSMBCare.Value__c = 'soaorgid';
            oSMBCare.Use_Soap_For_PT__c = false;
            smbCareWSList.add(oSMBCare);
        } 
        
        insert smbCareWSList;
        
        
        RecordType recordType = [Select r.Id, r.DeveloperName From RecordType r where r.SobjectType = 'Account' and r.DeveloperName='RCID' limit 1];
        Account acc = new Account(Name='Testing Software Parent ACC', 
                                BillingCountry = 'US',
                                
                                BillingState = 'IL', 
                            
                                No_Chronic_Incidents__c =1.0);
       insert acc; 
        Account acc1 = new Account(Name='Testing Software', 
                                BillingCountry = 'US',
                                
                                BillingState = 'IL', 
                                parentId = acc.id,
                                recordTypeId=recordType.Id,
                                No_Chronic_Incidents__c =10.0);
       insert acc1; 
        List<Account> accList = [SELECT Id,parentId,Name from Account where id = :acc1.Id LIMIT 1];
        smb_TTODS_CalloutHelper tdosCalloutHelper = new smb_TTODS_CalloutHelper();
        list<smb_TTODS_troubleticket.TroubleTicket> troubleTicketList = tdosCalloutHelper.getTicketsByQueryCall('T1234567','0003206700');
        smb_SVOCControllerTC tcCont = new smb_SVOCControllerTC();
        ApexPages.currentPage().getParameters().put('Id',accList.get(0).Id); 
        tcCont.init();
        boolean test = tcCont.AllowCreationOfRelatedRecords;
        string str = tcCont.baseUrl;
         //List<Case> caseList = smb_SVOCControllerTC.getCases(accList.get(0).Id,accList.get(0).ParentId);
         List<smb_TTODS_troubleticket.TroubleTicket> ttList = smb_SVOCControllerTC.getTroubleTickets('T1234567','0003206700');
        
        
        smb_SVOC_TicketsDetail tickDetails = new smb_SVOC_TicketsDetail();
        ApexPages.currentPage().getParameters().put('Id',accList.get(0).Id);
        ApexPages.currentPage().getParameters().put('TicketId','T1234567');  
        tickDetails.init();
        smb_TTODS_troubleticket.TroubleTicketActivity ttActivity = new smb_TTODS_troubleticket.TroubleTicketActivity();
        ttActivity.activityTypeCode = 'testCode';
        ttActivity.activityTypeCode = 'testTypeCode';        
        ttActivity.TELUSActivityId = 'testActivityID';        
        ttActivity.assignToWorkgroup = 'testWorkGroup';
        ttActivity.activityComments = 'testComments';
        ttActivity.resolutionCode1 = 'testResolutionCode1';
        ttActivity.resolutionCode2 = 'TestResolutionCode2';
        ttActivity.resolutionCode3 = 'testResolutionCode3';        
        ttActivity.actualCompleteDateTime = datetime.now();      
        ttActivity.completedFlag = 'testCompletedFlag';        
        ttActivity.actvitiyLastUpdateTimeTTODS = datetime.now();
        
        smb_SVOC_TicketsDetail.TroubleTicketActivityWrapper ttActivityWrapper;
         ttActivityWrapper = new smb_SVOC_TicketsDetail.TroubleTicketActivityWrapper(ttActivity);
        
        list<smb_TTODS_troubleticket.TroubleTicket> lstTroubleTicket = new list<smb_TTODS_troubleticket.TroubleTicket>(); 
        lstTroubleTicket = smb_SVOCController.getTroubleTickets('T1234567','0003206700');
         
        
    }
    
    //This methods provide test coverage to All the classes generated by WSDL to APEX parser
    static testMethod void testsmb_TTODSWebService() 
    {
        smb_TTODS_query smb = new smb_TTODS_query();
        smb_TTODS_query.CompareListAndOperator t1= new smb_TTODS_query.CompareListAndOperator();
        smb_TTODS_query.CompareValueAndOperator t2= new smb_TTODS_query.CompareValueAndOperator();
        smb_TTODS_query.Query t3= new smb_TTODS_query.Query();
        smb_TTODS_query.TicketsBySubscriptionIdQuery t4 = new smb_TTODS_query.TicketsBySubscriptionIdQuery();
        smb_TTODS_query.TroubleTicketOrderBy t5 = new smb_TTODS_query.TroubleTicketOrderBy();
        
        smb_TTODS_troubleticket smb1 = new smb_TTODS_troubleticket();
        smb_TTODS_troubleticket.ActivityFilter t6 = new smb_TTODS_troubleticket.ActivityFilter();
        smb_TTODS_troubleticket.ActivityStateListAndOperator t7 = new smb_TTODS_troubleticket.ActivityStateListAndOperator();
        smb_TTODS_troubleticket.ActivityTypeListAndOperator t8 = new smb_TTODS_troubleticket.ActivityTypeListAndOperator();
        smb_TTODS_troubleticket.Contact t9 = new smb_TTODS_troubleticket.Contact();
        smb_TTODS_troubleticket.ContactIdentification t10 = new smb_TTODS_troubleticket.ContactIdentification();
        smb_TTODS_troubleticket.ContractData t11 = new smb_TTODS_troubleticket.ContractData();
        smb_TTODS_troubleticket.ContractIdentification t12 = new smb_TTODS_troubleticket.ContractIdentification();
        smb_TTODS_troubleticket.Customer t13 = new smb_TTODS_troubleticket.Customer();
        smb_TTODS_troubleticket.CustomerIdentification t14 = new smb_TTODS_troubleticket.CustomerIdentification();
        smb_TTODS_troubleticket.CustomerIdFilter t15 = new smb_TTODS_troubleticket.CustomerIdFilter();
        smb_TTODS_troubleticket.CustomerSubscription t16 = new smb_TTODS_troubleticket.CustomerSubscription();
        smb_TTODS_troubleticket.CustomerSubscriptionIdentification t17 = new smb_TTODS_troubleticket.CustomerSubscriptionIdentification();
        smb_TTODS_troubleticket.ElectronicAddress t18 = new smb_TTODS_troubleticket.ElectronicAddress();
        smb_TTODS_troubleticket.ExternalTicketIdFilter t19 = new smb_TTODS_troubleticket.ExternalTicketIdFilter();
        smb_TTODS_troubleticket.ExternalTroubleTicket t20 = new smb_TTODS_troubleticket.ExternalTroubleTicket();
        smb_TTODS_troubleticket.ListAndOperator t21 = new smb_TTODS_troubleticket.ListAndOperator();
        smb_TTODS_troubleticket.LocationAddress t22 = new smb_TTODS_troubleticket.LocationAddress();
        smb_TTODS_troubleticket.Product t23 = new smb_TTODS_troubleticket.Product();
        smb_TTODS_troubleticket.ProductApplicationXref t24 = new smb_TTODS_troubleticket.ProductApplicationXref();
        smb_TTODS_troubleticket.QueryTicketSearchCriteria t25 = new smb_TTODS_troubleticket.QueryTicketSearchCriteria();
        smb_TTODS_troubleticket.SubscriptionIdFilter t26 = new smb_TTODS_troubleticket.SubscriptionIdFilter();
        smb_TTODS_troubleticket.TelecommunicationsAddress t27 = new smb_TTODS_troubleticket.TelecommunicationsAddress();
        smb_TTODS_troubleticket.TicketFilter t28 = new smb_TTODS_troubleticket.TicketFilter();
        smb_TTODS_troubleticket.TroubleTicket t29 = new smb_TTODS_troubleticket.TroubleTicket();
        smb_TTODS_troubleticket.TroubleTicketActivity t30 = new smb_TTODS_troubleticket.TroubleTicketActivity();
        smb_TTODS_troubleticket.TroubleTicketList t31 = new smb_TTODS_troubleticket.TroubleTicketList();
        smb_TTODS_troubleticket.ValueAndOperator t32 = new smb_TTODS_troubleticket.ValueAndOperator();

    }
    @isTest (SeeAllData = false)
    static void testsmb_TTODS_queryticketservice()
    {
         list<SMBCare_WebServices__c> smbCareWSList = new List<SMBCare_WebServices__c>();
        if(SMBCare_WebServices__c.getInstance('TTODS_EndPoint') == null)
        {
            SMBCare_WebServices__c oSMBCare = new  SMBCare_WebServices__c();
            oSMBCare.Name = 'TTODS_EndPoint';
            oSMBCare.Value__c = 'https://partnerservices-pt.telus.com:443/SOA/SMO/ProblemMgmt/TroubleTicketODSQueryService_v1_5_vs1_vs0';
            oSMBCare.Use_Soap_For_PT__c = false;
            smbCareWSList.add(oSMBCare);
        }
        
        if(SMBCare_WebServices__c.getValues('username') == null)
        {
            SMBCare_WebServices__c oSMBCare = new  SMBCare_WebServices__c();
            oSMBCare.Name = 'username';
            oSMBCare.Value__c = 'APP_SFDC';
            oSMBCare.Use_Soap_For_PT__c = false;
            smbCareWSList.add(oSMBCare);
        }
        
        if(SMBCare_WebServices__c.getValues('password') == null)
        {
            SMBCare_WebServices__c oSMBCare = new  SMBCare_WebServices__c();
            oSMBCare.Name = 'password';
            oSMBCare.Value__c = 'soaorgid';
            oSMBCare.Use_Soap_For_PT__c = false;
            smbCareWSList.add(oSMBCare);
        } 
        
        insert smbCareWSList;
        
        list<smb_TTODS_troubleticket.TroubleTicket> lstTroubleTicket = new list<smb_TTODS_troubleticket.TroubleTicket>();
        string requestSystemName = 'SFDC';
        string requestUserId = 'T1234567';
        smb_TTODS_queryticketservice.QueryTicketPort servicePort = new smb_TTODS_queryticketservice.QueryTicketPort();
        lstTroubleTicket = servicePort.getTickets(requestSystemName, requestUserId,null,null,null,null, null, null, null, null, null);
        lstTroubleTicket = servicePort.getTicketsByQuery(requestSystemName, requestUserId);
    }
    
     public static testmethod void testGetTickets() {
        Test.setMock(WebServiceMock.class, new smb_Ticket_Aggregator_Test.AsyncSoapServiceMockImpl());
        Account acc = smb_Ticket_Aggregator_Test.createAccount();
        LavaStorm_Trouble_Ticket__c lsObj = new LavaStorm_Trouble_Ticket__c();
        lsObj.Trouble_Id__c = 'test_001';
        lsObj.BAN__c = '0001058404_zzz';
        lsObj.Customer_Id__c = '31211_zzz'; 
        lsObj.Account__c = acc.Id;
        lsObj.Problem_Description_Details__c = 'test';
        lsObj.Status__c = 'Open';
        lsObj.Closed_date__c = system.now();
        lsObj.Time_of_Resolution__c = system.now();
        insert lsObj;
        
        LavaStorm_Trouble_Ticket__c lsObj1 = new LavaStorm_Trouble_Ticket__c();
        lsObj1.Trouble_Id__c = lsObj.Trouble_Id__c + '-' + 'test_001';
        lsObj1.BAN__c = '0001058404_zzz';
        lsObj1.Customer_Id__c = '31211_zzz'; 
        lsObj1.Account__c = acc.Id;
        insert lsObj1;
        Case csObj = new Case();
        csObj.AccountId = acc.Id;
        csObj.Subject='Test Controller Acct Case';
        insert csObj;
        
        Test.startTest();
        TicketVO.ServiceRequestAndResponseParameter reqResObj = new TicketVO.ServiceRequestAndResponseParameter();
        reqResObj.isCallForLavaStormIndividualTT=true;
        reqResObj.ticketIdToSearch = 'test_001';
        reqResObj.accountId = acc.Id;
        TicketVO.TicketData tdObj = new TicketVO.TicketData();
		tdObj.compareTo(new TicketVO.TicketData());
         
        smb_Ticket_Mapping_Helper.doLavaStormMapping(reqResObj);
            
        PageReference pageRef = Page.smb_SVOC_TicketsCases;
        smb_Ticket_Aggregator_Test.putParameter(pageRef, acc.Id, '', '', '3','3');
        Test.setCurrentPage(pageRef);
        
        smb_SVOCControllerTC ttObj = new smb_SVOCControllerTC();
        ttObj.RCID = '31211';
        //ttObj.pageInstanceId  = '31211';
        string tst = ttObj.pageInstanceId;
        ttObj.init();
        Continuation conti = ttObj.getAggregateTickets();
        Map<String, HttpRequest> requests = conti.getRequests();
        system.debug('@@@TEst Request ' + requests);
        system.debug('@@@TEst Request ' + requests.size());
        system.assert(requests.size() == 2);// 19 May 2016 - Sandip - Removed commentted line for deploying SRM changes
        //TTODS
        HttpResponse response = new HttpResponse();
        String mockBody = smb_Ticket_Aggregator_Test.ttodsMockBody;
        response.setBody(mockBody); 
        Test.setContinuationResponse('Continuation-1', response);
        //SRM
        HttpResponse response1 = new HttpResponse();
        String mockBody1 = smb_Ticket_Aggregator_Test.srmMockBody;
        response1.setBody(mockBody1); 
        Test.setContinuationResponse('Continuation-2', response1);
        
        Object result = Test.invokeContinuationMethod(ttObj, conti); 
        Test.stopTest(); 
    }
     public static testmethod void testSRMWebService() { 
        Test.setMock(WebServiceMock.class, new smb_Ticket_Aggregator_Test.AsyncSoapServiceMockImpl());
        Account acc = smb_Ticket_Aggregator_Test.createAccount();
             
        Test.startTest();
        PageReference pageRef = Page.smb_SVOC_TTDetail;
        smb_Ticket_Aggregator_Test.putParameter(pageRef, acc.Id, 'TERM-20160309-0002', TicketVO.SRC_SRM, '3','3');
        Test.setCurrentPage(pageRef);
        
        smb_SVOC_TTDetailController detailController = new smb_SVOC_TTDetailController();
        detailController.RCID='0001058404';
        detailController.init();
        Continuation conti = detailController.getTTDetail(); 
        Map<String, HttpRequest> requests = conti.getRequests();
        //19 May 2016 - Sandip - Removed commentted line for deploying SRM changes
        system.assert(requests.size() == 1); 
        system.debug('@@@@ requests' +requests); 
        HttpResponse response = new HttpResponse();
        String mockBody = smb_Ticket_Aggregator_Test.srmMockBody;
        response.setBody(mockBody); 
        String requestLabel = requests.keyset().iterator().next(); 
        Test.setContinuationResponse(requestLabel, response);
        Object result = Test.invokeContinuationMethod(detailController, conti);
        Test.stopTest(); 
    }
    public static testmethod void testLavaStorm() { 
      
        Account acc = smb_Ticket_Aggregator_Test.createAccount();
        LavaStorm_Trouble_Ticket__c lsObj = new LavaStorm_Trouble_Ticket__c();
        lsObj.Trouble_Id__c = 'test_001';
        lsObj.BAN__c = '0001058404_zzz';
        lsObj.Customer_Id__c = '31211_zzz'; 
        lsObj.Account__c = acc.Id;
        lsObj.Problem_Description_Details__c = 'test';
        lsObj.Status__c = 'Open';
        lsObj.Closed_date__c = system.now();
        lsObj.Time_of_Resolution__c = system.now();
        insert lsObj;
        
        LavaStorm_Trouble_Ticket__c lsObj1 = new LavaStorm_Trouble_Ticket__c();
        lsObj1.Trouble_Id__c = lsObj.Trouble_Id__c + '-' + 'test_001';
        lsObj1.BAN__c = '0001058404_zzz';
        lsObj1.Customer_Id__c = '31211_zzz'; 
        lsObj1.Account__c = acc.Id;
        insert lsObj1;
                
        Test.startTest();
        PageReference pageRef = Page.smb_SVOC_TTDetail;
        smb_Ticket_Aggregator_Test.putParameter(pageRef, acc.Id, lsObj.Trouble_Id__c, TicketVO.SRC_LAVASTORM, '3','3');
        Test.setCurrentPage(pageRef); 
        smb_SVOC_TTDetailController objj = new smb_SVOC_TTDetailController();
        objj.init();
        objj.getLavaStormTicketDetails();
        Test.stopTest(); 
    }
 public static testmethod void testTTODSWebService() { 
        Test.setMock(WebServiceMock.class, new smb_Ticket_Aggregator_Test.AsyncSoapServiceMockImpl());
        Account acc = smb_Ticket_Aggregator_Test.createAccount();
             
        Test.startTest();
        PageReference pageRef = Page.smb_SVOC_TTDetail;
        smb_Ticket_Aggregator_Test.putParameter(pageRef, acc.Id, '1000119421', TicketVO.SRC_TTODS, '3', '3');
        Test.setCurrentPage(pageRef);
        
        smb_SVOC_TTDetailController detailController = new smb_SVOC_TTDetailController();
        detailController.RCID='0001058404';
        detailController.init();
        Continuation conti = detailController.getTTDetail(); 
        Map<String, HttpRequest> requests = conti.getRequests();
        system.assert(requests.size() == 1);
        system.debug('@@@@ requests' +requests); 
        HttpResponse response = new HttpResponse();
        String mockBody = smb_Ticket_Aggregator_Test.ttodsMockBody;
        response.setBody(mockBody); 
        String requestLabel = requests.keyset().iterator().next(); 
        Test.setContinuationResponse(requestLabel, response);
        Object result = Test.invokeContinuationMethod(detailController, conti); 

        Test.stopTest(); 
    }
    
}