/* Developed By: Hari Neelalaka K (IBM)
*  Created Date: 29-Oct-2014
*  Modified Date: 20-jan-2015
*/
@isTest
    private class SRS_SearchESDController_Test {
    
    //.............. Test customer search by name ............... 
     @isTest static void testCallout1(){
        Service_Request__c sr = new Service_Request__c(ESD_Customer_Short_Name__c='test');
        Integer CSID=12345;
        List<string> lstStr=new List<string>();
        lstStr.add('6');
        ApexPages.StandardController stdcon = new ApexPages.StandardController(sr);
        // Set the mock callout mode            
            Test.setMock(WebServiceMock.class, new SRS_ESDServiceMockTest());        
            
        //............. Call the method that performs the callout..................
        SRS_SearchESDController con1 = new SRS_SearchESDController(stdcon);
        con1.setStatus(lstStr);
            SMBCare_WebServices__c cs = new SMBCare_WebServices__c(); 
            cs.Name = 'SRS_ESDEndPoint';
            cs.Value__c = 'https://xmlgwy-pt1.telus.com:9030/st02/RMO/OrderMgmt/EsdProductInstanceService_v1_0_vs0';
            insert cs;
            
            SMBCare_WebServices__c cs1 = new SMBCare_WebServices__c(); //Custom Setting for username Field
            cs1.Name = 'ESDUsername';
            cs1.Value__c = 'ESDUser';
            insert cs1;       
            
            SMBCare_WebServices__c cs2 = new SMBCare_WebServices__c(); //Custom Setting for password Field
            cs2.Name = 'ESDPassword';
            cs2.Value__c = 'ESDUser';
            insert cs2; 
            Test.startTest();
            con1.search();
            Test.stoptest();
        }
        
    
    ////////////////////////////////////////////////////////////////////////    
        @isTest static void testCallout2() {
         
            Account acc = new Account(); 
            acc.Name = 'test';
            insert acc;
            Id SrRecordtypeID =Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Provide/Install').getRecordTypeId();
            Service_Request__c sr = new Service_Request__c();
            sr.Account_Name__c = acc.Id;            
            sr.recordtypeid=SrRecordtypeID;
            insert sr;         
            PageReference pageRef = Page.SRS_SearchESD;
            String Id = sr.Id;          
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('SRID', Id);
            ApexPages.currentPage().getParameters().put('ACCTID', acc.Id);
            ApexPages.StandardController stdcon = new ApexPages.StandardController(sr);
            SRS_SearchESDController con = new SRS_SearchESDController(stdcon); 
            con.sortExpression='';
            con.getSortDirection();
            con.sortExpression='test';
            con.getSortDirection();
            con.getItems();
            con.getStatus();
            List<string> lstStr=new List<string>();
            lstStr.add('6');
            con.setStatus(lstStr);
            con.getSortDirection();
            con.setSortDirection('ASC');
            con.getwrapperList();
            string strTest='Sort';
            con.SortRelatedListwithColumn();
            con.srAcctId = acc.Id;
            con.checkBoxStatus = 'false';
            con.checkboxCheck();
            con.getLocationValues();
            con.linkToSerReq();
            con.clearValues();
            con.getSiteReadyValues();
            con.getNewBuildingValues();
            con.cancel();
            con.checked();
            con.sortExpression='ASC';
            con.index='1';
            con.select_allchkbox();
            con.reset();
            lstStr.add('2');
            con.select_allchkbox();
            
        /*  try{ 
            
            }catch(Exception ex)
            {
            }
        */  

        
        /////////////////////////////////////////////////////
    /*    
        //......... Set the mock callout mode ...........................           
            Test.setMock(WebServiceMock.class, new SRS_ESDServiceMockTest());        
            
        //............. Call the method that performs the callout..................
            Test.startTest();
            con.search();
            Test.stoptest();
    */
        // Verify response received contains values returned by the mock response.
        // System.assertEquals(3, accounts.size()); 

        List<SRS_SearchESDController.wrapper> wrapperL = new List<SRS_SearchESDController.wrapper>();
 
    //................Sorting by CSID....................   
        
        SRS_SearchESDController.SORT_FIELD='CSID';
        SRS_SearchESDController.SORT_DIR = 'ASC';
        SRS_SearchESDController.wrapper req0= new SRS_SearchESDController.wrapper('123456', 'testStatus', 'testCustName', 'testServiceType', 'testServiceLocation', 'testCity', 'testPrSt', 1);
        req0.checked=true;  
        wrapperL.add(req0);
        wrapperL.sort();
        
    //................Sorting by Status....................   
        
        SRS_SearchESDController.SORT_FIELD='Status';
        SRS_SearchESDController.SORT_DIR = 'ASC';
        SRS_SearchESDController.wrapper req1= new SRS_SearchESDController.wrapper('123456', 'testStatus', 'testCustName', 'testServiceType', 'testServiceLocation', 'testCity', 'testPrSt', 1);
        req1.checked=true;  
        wrapperL.add(req1);
        wrapperL.sort();
        
    //................Sorting by ESD Customer Name....................   
        
        SRS_SearchESDController.SORT_FIELD='ESD Customer Name';
        SRS_SearchESDController.SORT_DIR = 'ASC';
        SRS_SearchESDController.wrapper req2= new SRS_SearchESDController.wrapper('123456', 'testStatus', 'testCustName', 'testServiceType', 'testServiceLocation', 'testCity', 'testPrSt', 1);        
        req2.checked=true;  
        wrapperL.add(req2);
        wrapperL.sort();

    //................Sorting by Service Type....................   
        
        SRS_SearchESDController.SORT_FIELD='Service Type';
        SRS_SearchESDController.SORT_DIR = 'ASC';
        SRS_SearchESDController.wrapper req3= new SRS_SearchESDController.wrapper('123456', 'testStatus', 'testCustName', 'testServiceType', 'testServiceLocation', 'testCity', 'testPrSt', 1);        
        req3.checked=true;
        wrapperL.add(req3);
        wrapperL.sort();
 
    //................Sorting by Service Location....................   
        
        SRS_SearchESDController.SORT_FIELD='Service Location';
        SRS_SearchESDController.SORT_DIR = 'ASC';
        SRS_SearchESDController.wrapper req4= new SRS_SearchESDController.wrapper('123456', 'testStatus', 'testCustName', 'testServiceType', 'testServiceLocation', 'testCity', 'testPrSt', 1);        
        req4.checked=true;
        wrapperL.add(req4);
        wrapperL.sort();
        
    //................Sorting by City....................   
        
        SRS_SearchESDController.SORT_FIELD='City';
        SRS_SearchESDController.SORT_DIR = 'ASC';
        SRS_SearchESDController.wrapper req5= new SRS_SearchESDController.wrapper('123456', 'testStatus', 'testCustName', 'testServiceType', 'testServiceLocation', 'testCity', 'testPrSt', 1);        
        req5.checked=true;
        wrapperL.add(req5);
        wrapperL.sort();

    //................Sorting by Pr/ST...................   
        
        SRS_SearchESDController.SORT_FIELD='Pr/ST';
        SRS_SearchESDController.SORT_DIR = 'ASC';
        SRS_SearchESDController.wrapper req6= new SRS_SearchESDController.wrapper('123456', 'testStatus', 'testCustName', 'testServiceType', 'testServiceLocation', 'testCity', 'testPrSt', 1);        
        req6.checked=true;
        wrapperL.add(req6);
        wrapperL.sort();
        
        SRS_SearchESDController.SORT_FIELD='CSID';
        SRS_SearchESDController.SORT_DIR = 'DSC';
        SRS_SearchESDController.wrapper req7= new SRS_SearchESDController.wrapper('123456', 'testStatus', 'testCustName', 'testServiceType', 'testServiceLocation', 'testCity', 'testPrSt', 1);        
        req7.checked=true;
        wrapperL.add(req7);
        wrapperL.sort();
        
        con.esdList=wrapperL;
        con.checked();
        con.linkToSerReq();
        //con.chkAll=false;
        con.checkbox_count=0;
        con.select_allchkbox();
        
        Id SrRecordtypeID1 =Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Change').getRecordTypeId();
        Service_Request__c sr1 = new Service_Request__c();
        sr1.Account_Name__c = acc.Id;            
        sr1.recordtypeid=SrRecordtypeID1;
        insert sr1;
        
        PageReference pageRef1 = Page.SRS_SearchESD;
        String Id1 = sr1.Id;          
        Test.setCurrentPage(pageRef1);
        ApexPages.currentPage().getParameters().put('SRID', Id1);
        ApexPages.StandardController stdcon1 = new ApexPages.StandardController(sr1);
        SRS_SearchESDController con1 = new SRS_SearchESDController(stdcon1);
        con1.esdList=wrapperL;
        con1.linkToSerReq();
        con1.esdList.addAll(wrapperL);
        con1.getwrapperList();
        con1.esdList.addAll(wrapperL);
        con1.getwrapperList();
        
        }
        
    
    //.............. Test customer search by CSID ............... 
     @isTest static void testCallout3(){
        Service_Request__c sr = new Service_Request__c();   
        ApexPages.StandardController stdcon1 = new ApexPages.StandardController(sr);
        // Set the mock callout mode            
            Test.setMock(WebServiceMock.class, new SRS_ESDServiceMockTest());        
            
        //............. Call the method that performs the callout..................
        SRS_SearchESDController con2 = new SRS_SearchESDController(stdcon1);
        con2.CSID=12345;
            SMBCare_WebServices__c cs = new SMBCare_WebServices__c(); 
            cs.Name = 'SRS_ESDEndPoint';
            cs.Value__c = 'https://xmlgwy-pt1.telus.com:9030/st02/RMO/OrderMgmt/EsdProductInstanceService_v1_0_vs0';
            insert cs;
            
            SMBCare_WebServices__c cs1 = new SMBCare_WebServices__c(); //Custom Setting for username Field
            cs1.Name = 'ESDUsername';
            cs1.Value__c = 'ESDUser';
            insert cs1;       
            
            SMBCare_WebServices__c cs2 = new SMBCare_WebServices__c(); //Custom Setting for password Field
            cs2.Name = 'ESDPassword';
            cs2.Value__c = 'ESDUser';
            insert cs2; 
            Test.startTest();
            con2.search();
            Test.stoptest();
        }   
    }