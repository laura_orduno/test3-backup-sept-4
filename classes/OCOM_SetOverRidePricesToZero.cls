global with sharing class OCOM_SetOverRidePricesToZero implements vlocity_cmt.VlocityOpenInterface {
     global Boolean invokeMethod(String methodName, Map<String, Object> input, Map<String, Object> output, Map<String, Object> options)  {
        if (methodName.equals('priceItems')){
            return priceItems(input,output,options);
        }
        return true;
    }
    
    private Boolean priceItems(Map<String, Object> inputMap, Map<String, Object> outputMap, Map<String, Object> optionsMap){
        set<string> rootOverlineProdItemIds = new set<string>();
        map<id,sobject> rootItTosObjectMap = new map<id,sObject>();
        map<string,Object> rootObjectMap = new map<string,Object>();
        Set<Id> childOliToupdate = new Set<Id>();
        map<id,id>oiToProdIDMap = new map<id,id>();
        map<id,id>prodToOiMap = new map<id,id>();
        Map<Id,Id> rootIdTooverilineIdMap = new Map<Id,Id>();
        Map<String, Object> filterEvaluatorOutput = (Map<String, Object>)inputMap.get('filterEvaluatorOutput');
        system.debug('filterEvaluatorOutput_____' +filterEvaluatorOutput);
        Map<Id, Sobject> sobjectIdToSobject = (Map<Id, Sobject>)filterEvaluatorOutput.get('sobjectIdToSobject');

         system.debug('sobjectIdToSobject______' +sobjectIdToSobject);
        List<Id> qualifiedObjectIds = new List<Id>((Set <Id>)inputMap.get('qualifiedObjectIds'));
         system.debug('qualifiedObjectIds______' +qualifiedObjectIds);
        // here 

        for (orderItem oi: [select  id,vlocity_cmt__Product2Id__c,PriceBookEntry.product2.Id,PriceBookEntry.product2.Name,vlocity_cmt__RootItemId__c from orderItem where id=:qualifiedObjectIds]){
            oiToProdIDMap.put(oi.id,oi.PriceBookEntry.product2.Id );
            prodToOiMap.put(oi.PriceBookEntry.product2.Id,oi.id);
			if(oi.PriceBookEntry.product2.Name != null && oi.PriceBookEntry.product2.Name.containsIgnoreCase('Overline') ){
                rootIdTooverilineIdMap.put(oi.vlocity_cmt__RootItemId__c,oi.PriceBookEntry.product2.id);
            }
        }

        for(Integer i = 0; i < qualifiedObjectIds.size(); i++){
            Sobject objectSO = sobjectIdToSobject.get(qualifiedObjectIds[i]);
             string rootItemId = String.Valueof(objectSO.get('vlocity_cmt__RootItemId__c'));
             sObject rootObject = sobjectIdToSobject.get(rootItemId);

             if(!rootItTosObjectMap.containskey(rootItemId)){
                system.debug('rootItemId_____'+rootItemId);
                rootItTosObjectMap.put(rootItemId,rootObject);
              
                map<String,Object> prceBookEntry = (map<string,Object>)rootObjectMap.get('PricebookEntry');
                if(oiToProdIDMap.ContainsKey(rootItemId)){
                        rootOverlineProdItemIds.add(String.valueOf(oiToProdIDMap.get(rootItemId)));
                    }
                 if(rootIdTooverilineIdMap != null && !rootIdTooverilineIdMap.isEmpty() && rootIdTooverilineIdMap.size() > 0 && rootIdTooverilineIdMap.containsKey(rootItemId))  {
                    rootOverlineProdItemIds.add(String.valueOf(rootIdTooverilineIdMap.get(rootItemId)));
                }  
             }
         }
        system.debug('List of RootProdct Items ID ---> ' + rootOverlineProdItemIds );

        if(rootOverlineProdItemIds.size() > 0 && !rootOverlineProdItemIds.isEmpty() ){
                 for(vlocity_cmt__ProductChildItem__c pci: [select id,name,OCOMOverrideType__c,
                                                            vlocity_cmt__ParentProductId__c,vlocity_cmt__ParentProductId__r.name,
                                                            vlocity_cmt__ChildProductId__c 
                                                            from vlocity_cmt__ProductChildItem__c 
                                                            where vlocity_cmt__ParentProductId__r.id = :rootOverlineProdItemIds]){
                        if(pci.OCOMOverrideType__c == 'Override Price'){
                            childOliToupdate.add(pci.vlocity_cmt__ChildProductId__c);
                        }
                }
        }

       /* for(Integer i = 0; i < childOliToupdate.size(); i++){
            system.debug('childOliToUpdate' + childOliToupdate);
            if(prodToOiMap.containsKey(childOliToupdate[i])){
                string childOi = prodToOiMap.get(childOliToupdate[i]);
                 Sobject objectSO2 = sobjectIdToSobject.get(childOi);
                 System.debug(LoggingLevel.ERROR,'objectSO2:::::' + objectSO2);
                 objectSO2.put('vlocity_cmt__OneTimeTotal__c', 0.0);
                objectSO2.put('vlocity_cmt__RecurringTotal__c', 0.0);
                objectSO2.put('vlocity_cmt__OneTimeCharge__c', 0.0);
                objectSO2.put('vlocity_cmt__RecurringCharge__c', 0.0);
                objectSO2.put('vlocity_cmt__EffectiveOneTimeTotal__c',0.0);
                objectSO2.put('vlocity_cmt__EffectiveRecurringTotal__c',0.0);
                
            }
     
        }*/
        system.debug('childOliToUpdate' + childOliToupdate);
            
        for(id orderID : oiToProdIDMap.keySet()){
                id product2ID = oiToProdIDMap.get(orderID);
            system.debug('product2ID' + product2ID);
            if(childOliToupdate.contains(product2ID)){
                Sobject objectSO2 = sobjectIdToSobject.get(orderID);
                System.debug(LoggingLevel.ERROR,'objectSO2:::::' + objectSO2);
                 objectSO2.put('vlocity_cmt__OneTimeTotal__c', 0.0);
                objectSO2.put('vlocity_cmt__RecurringTotal__c', 0.0);
                objectSO2.put('vlocity_cmt__OneTimeCharge__c', 0.0);
                objectSO2.put('vlocity_cmt__RecurringCharge__c', 0.0);
                objectSO2.put('vlocity_cmt__EffectiveOneTimeTotal__c',0.0);
                objectSO2.put('vlocity_cmt__EffectiveRecurringTotal__c',0.0);
            } 
         }
        return true;         
    }
}