//Generated by wsdl2apex

public class smb_OrderSubmit_Urban_Prop_Address_v3 {
    public class UrbanPropertyAddress {
        public String Id;
        public String Name;
        public DateTime fromTimestamp;
        public DateTime toTimestamp;
        public smb_OrderSubmit_base_v3.CharacteristicValue[] CharacteristicValue;
        public smb_OrderSubmit_base_v3.EntitySpecification Specification;
        public String Country;
        public String StateOrProvince;
        public String Locality;
        public String Postcode;        
        public String StreetNrFirst;
        public String StreetNrLast;
        public String StreetName;
        public smb_OrderSubmit_Place_Ext_v3.UrbanPropertyAddressExtensions UrbanPropertyAddressExtensions;
        private String[] Id_type_info = new String[]{'Id','http://www.ibm.com/telecom/common/schema/base/v3_0',null,'0','1','false'};
        private String[] Name_type_info = new String[]{'Name','http://www.ibm.com/telecom/common/schema/base/v3_0',null,'0','1','false'};
        private String[] fromTimestamp_type_info = new String[]{'fromTimestamp','http://www.ibm.com/telecom/common/schema/base/v3_0',null,'0','1','false'};
        private String[] toTimestamp_type_info = new String[]{'toTimestamp','http://www.ibm.com/telecom/common/schema/base/v3_0',null,'0','1','false'};
        private String[] CharacteristicValue_type_info = new String[]{'CharacteristicValue','http://www.ibm.com/telecom/common/schema/base/v3_0',null,'0','-1','false'};
        private String[] Specification_type_info = new String[]{'Specification','http://www.ibm.com/telecom/common/schema/base/v3_0',null,'0','1','false'};
        private String[] Country_type_info = new String[]{'Country','http://www.ibm.com/telecom/common/schema/place/v3_0',null,'0','1','false'};
        private String[] StateOrProvince_type_info = new String[]{'StateOrProvince','http://www.ibm.com/telecom/common/schema/place/v3_0',null,'0','1','false'};
        private String[] Locality_type_info = new String[]{'Locality','http://www.ibm.com/telecom/common/schema/place/v3_0',null,'0','1','false'};
        private String[] Postcode_type_info = new String[]{'Postcode','http://www.ibm.com/telecom/common/schema/place/v3_0',null,'0','1','false'};
        
        private String[] StreetNrFirst_type_info = new String[]{'StreetNrFirst','http://www.ibm.com/telecom/common/schema/urban_property_address/v3_0',null,'0','1','false'};
        private String[] StreetNrLast_type_info = new String[]{'StreetNrLast','http://www.ibm.com/telecom/common/schema/urban_property_address/v3_0',null,'0','1','false'};
        private String[] StreetName_type_info = new String[]{'StreetName','http://www.ibm.com/telecom/common/schema/urban_property_address/v3_0',null,'0','1','false'};
        private String[] UrbanPropertyAddressExtensions_type_info = new String[]{'UrbanPropertyAddressExtensions','http://www.ibm.com/telecom/common/schema/urban_property_address/v3_0',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.ibm.com/telecom/common/schema/urban_property_address/v3_0','false','false'};
        private String[] field_order_type_info = new String[]{'Id','Name','fromTimestamp','CharacteristicValue','Specification','Country','StateOrProvince','Locality','Postcode','StreetNrFirst','StreetNrLast','StreetName','UrbanPropertyAddressExtensions'};
    }
}