public class SObjectHistoryUtil {
	
	public List<SObject> createHistoricalObject(List<SObject> mainObjList, List<SObject> historicalObjList, String objectName, Map<String, String> fieldMap) {
		
		System.debug('Trigger Size = ' + mainObjList.size());
		Schema.SObjectType objType = Schema.getGlobalDescribe().get(objectName);
		for (SObject sobj : mainObjList) {
			System.debug('sobj = ' + sobj.id);
			SObject historicalObj = getHistoricalObject(objType);
			historicalObj.put('Start_Date__c', Date.today());
			historicalObjList.add(historicalObj);

			for (String key : fieldMap.keySet()) {
				System.debug('key = ' + key);
				String mainObjField = fieldMap.get(key);
				System.debug('mainObjField = ' + mainObjField);
				Object val = sobj.get(mainObjField);
				//check if field is of type picklist as obj does not work for picklist
				try {
					if (val != null) {
						if (val instanceof Decimal) {
							System.debug(mainObjField + ' val instanceof Decimal = ' + val);					
							historicalObj.put(key, convertToDecimal(val));
						} else if (val instanceof String) {
							System.debug(mainObjField + ' val instanceof String = ' + val);
							historicalObj.put(key, val);	
						} else {
							System.debug(mainObjField + ' val in else = ' + val);
							historicalObj.put(key, val);
						}
					}
				} catch(Exception e) {
					System.debug('e = ' + e.getMessage());
				}
			}
		}
		System.debug('historicalObjList Size = ' + historicalObjList.size());
		return historicalObjList;
	}
	
	public Boolean isObjectChanged(SObject changedObj, SObject oldObj, Set<String> historyTrackingFields) {
		for (String fieldName : historyTrackingFields) {
			Object newFieldValue = changedObj.get(fieldName);
			Object oldFieldValue = oldObj.get(fieldName);
			
			if ( SObjectHistoryUtil.isChanged(oldFieldValue, newFieldValue)) {
				return true;
			}
		}
		return false;
	}
	
	public SObject getHistoricalObject(Schema.SObjectType objType) {
		return objType.newSObject();
	}
	
	public static Boolean isChanged(Object oldVal, Object newVal) {
		return (checkNull(newVal) != checkNull(oldVal)); 
	}
	
	public static String checkNull(Object val) {
		return (val == null) ? '' : String.valueOf(val);
	}

	private Decimal convertToDecimal(Object val) {
		Decimal result = null;
		if (val == null) {
			return result;
		}
		try {
			result =  Decimal.valueOf(String.valueOf(val));
		} catch (TypeException te) {
		}
		return result;
	}
}