/**
 *  VITILcareGetHelpController
 *  @description Contoller for MBRGetHelp.page
 *  @author Majeda Mahfuz, Traction on Demand
 *  @date 2014-10-30
 */
public without sharing class ENTPGetHelpController extends Portal_GetHelpController {

    public static final String PORTAL_TYPE = 'ENTP';

    public ENTPGetHelpController(){
        super(PORTAL_TYPE);
    }
}