/*
    *******************************************************************************************************************************
    Class Name:     OCOM_TaskEventTest 
    Purpose:        Test class for OCOM_TaskEvent        
    Created by:     Thomas Su             30-Dec-2016     QC-9301       No previous version
    Modified by:    
    *******************************************************************************************************************************
*/

@isTest
private class OCOM_TaskEventTest {
    @isTest
    // test scenario where 
    private static void taskEventTest1() {
        List<OCOM_TaskEvent> taskEventList = new List<OCOM_TaskEvent>();
        OCOM_TaskEvent aTaskEvent1 = new OCOM_TaskEvent();
        aTaskEvent1.id = '1';
        aTaskEvent1.subject = 'Note 1';
        aTaskEvent1.whoId = UserInfo.getUserId();
        aTaskEvent1.whoName = 'tester';
        aTaskEvent1.isTask = true;
        aTaskEvent1.createdDate = system.now();
        aTaskEvent1.dueDate = system.now().format();
        aTaskEvent1.status = 'New';
        aTaskEvent1.priority = 'Low';
        aTaskEvent1.ownerId = UserInfo.getUserId();
        aTaskEvent1.ownerName = 'Tester';
        aTaskEvent1.lastModifiedDate = system.now().format();
        aTaskEvent1.summary = 'summary 1';
        taskEventList.add(aTaskEvent1);
        OCOM_TaskEvent aTaskEvent2 = new OCOM_TaskEvent();
        aTaskEvent2.id = '2';
        aTaskEvent2.subject = 'Note 2';
        aTaskEvent2.whoId = UserInfo.getUserId();
        aTaskEvent2.whoName = 'tester';
        aTaskEvent2.isTask = true;
        aTaskEvent2.createdDate = aTaskEvent1.createdDate;
        aTaskEvent2.dueDate = system.now().format();
        aTaskEvent2.status = 'New';
        aTaskEvent2.priority = 'Low';
        aTaskEvent2.ownerId = UserInfo.getUserId();
        aTaskEvent2.ownerName = 'Tester';
        aTaskEvent2.lastModifiedDate = system.now().format();
        aTaskEvent2.summary = 'summary 2';
        taskEventList.add(aTaskEvent2);
        OCOM_TaskEvent aTaskEvent3 = new OCOM_TaskEvent();
        aTaskEvent3.id = '3';
        aTaskEvent3.subject = 'Note 3';
        aTaskEvent3.whoId = UserInfo.getUserId();
        aTaskEvent3.whoName = 'tester';
        aTaskEvent3.isTask = true;
        aTaskEvent3.createdDate = system.now().addMinutes(2);
        aTaskEvent3.dueDate = system.now().format();
        aTaskEvent3.status = 'New';
        aTaskEvent3.priority = 'Low';
        aTaskEvent3.ownerId = UserInfo.getUserId();
        aTaskEvent3.ownerName = 'Tester';
        aTaskEvent3.lastModifiedDate = system.now().format();
        aTaskEvent3.summary = 'summary 3';
        taskEventList.add(aTaskEvent3);
        OCOM_TaskEvent aTaskEvent4 = new OCOM_TaskEvent();
        aTaskEvent4.id = '4';
        aTaskEvent4.subject = 'Note 4';
        aTaskEvent4.whoId = UserInfo.getUserId();
        aTaskEvent4.whoName = 'tester';
        aTaskEvent4.isTask = true;
        aTaskEvent4.createdDate = system.now().addMinutes(1);
        aTaskEvent4.dueDate = system.now().format();
        aTaskEvent4.status = 'New';
        aTaskEvent4.priority = 'Low';
        aTaskEvent4.ownerId = UserInfo.getUserId();
        aTaskEvent4.ownerName = 'Tester';
        aTaskEvent4.lastModifiedDate = system.now().format();
        aTaskEvent4.summary = 'summary 4';
        taskEventList.add(aTaskEvent4);
        taskEventList.sort();
    }

}