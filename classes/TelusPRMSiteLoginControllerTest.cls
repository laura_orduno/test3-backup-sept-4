@isTest
private class TelusPRMSiteLoginControllerTest {
		public static testMethod void testSiteLoginController () {
        // Instantiate a new controller with all parameters in the page
        TelusPRMSiteLoginController controller = new TelusPRMSiteLoginController ();
        Boolean testFlag;
        //successful scenario
        controller.username = 'test@salesforce.com';
        controller.password = '123456'; 
        testFlag = controller.getErrFlag();        
        System.assertEquals(controller.login(),null);
        System.assertEquals(controller.loginEn(),null);  
        
        //Test case if username is null
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Test Summary', ' Test detail')); 
        controller.username = '';
        controller.password = '123456'; 
                
        System.assertEquals(controller.login(),null);
        System.assertEquals(controller.loginEn(),null);                            
        
        //Test case if password is null
        controller.username = 'test@salesforce.com';
        controller.password = ''; 
                
        System.assertEquals(controller.login(),null);
        System.assertEquals(controller.loginEn(),null);    
        
        //Test case if both are null
        controller.username = 'test@salesforce.com';
        controller.password = '123456'; 
                
        System.assertEquals(controller.login(),null);
        System.assertEquals(controller.loginEn(),null);  
    }    
}