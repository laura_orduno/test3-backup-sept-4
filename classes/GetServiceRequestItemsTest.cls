@isTest
private class GetServiceRequestItemsTest {

     static testmethod void ServiceRequestItemsTest1(){
        //TestDataHelper.testContractLineItemListCreation_ConE();
         TestDataHelper.testContractCreationWithContractRequest();
         TestDataHelper.testContractLineItemListWithProductCreation();
        Test.startTest();
     Id Id1 = null;
     
    Map<String,Object> outMap=new Map<String,Object>();
        Apexpages.currentPage().getparameters().put('id',TestDataHelper.testContractRequest.id);
        GetServiceRequestItems ServiceRequestItemsData = new GetServiceRequestItems();
        boolean blnSuccess=ServiceRequestItemsData.invokeMethod('getItemsList',new Map<String,Object>{'contextObjId' => TestDataHelper.testContractRequest.id},outMap,new Map<String,Object>());
        system.assertEquals(outMap!=null && outMap.size() > 0, true);
        system.assertEquals(blnSuccess, true);

    Test.stopTest();
    }

    static testmethod void ServiceRequestItemsTest2(){
        //TestDataHelper.testContractLineItemListCreation_ConE();
        TestDataHelper.testContractCreationwithDealSupport();
        TestDataHelper.testContractLineItemListWithProductCreation();
       
        Test.startTest();
     	Id Id1 = null;
     
    Map<String,Object> outMap=new Map<String,Object>();
        Apexpages.currentPage().getparameters().put('id',TestDataHelper.testDealSupport.id);
        GetServiceRequestItems ServiceRequestItemsData = new GetServiceRequestItems();
        boolean blnSuccess=ServiceRequestItemsData.invokeMethod('getItemsList',new Map<String,Object>{'contextObjId' => TestDataHelper.testDealSupport.id},outMap,new Map<String,Object>());
        system.assertEquals(outMap!=null && outMap.size() > 0, true);
        system.assertEquals(blnSuccess, true);

    Test.stopTest();
    }
    
    
}