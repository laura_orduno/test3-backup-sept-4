/*****************************************************
    Apex: AccountTeamWrapper
    Usage: This class used for Account Team as Account
    Author – Clear Task
    Date – 06/11/2011
    Revision History
******************************************************/
public with sharing class AccountTeamWrapper {
    public Boolean checked{get; set;}
    public AccountTeamMember accountTeam{get; set;}    
    public AccountTeamWrapper(AccountTeamMember a){
        accountTeam = a;
        checked = false;
    }    
}