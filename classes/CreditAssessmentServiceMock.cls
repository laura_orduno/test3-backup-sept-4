@isTest
global class CreditAssessmentServiceMock implements WebServiceMock{
    
    global void doInvoke(
        Object stub
        , Object request
        , Map<String
        , Object> response
        , String endpoint
        , String soapAction
        , String requestName
        , String responseNS
        , String responseName
        , String responseType){            
            
            if ( 'ping'.equalsIgnoreCase(requestName) ){
                response.put('response_x', ping());
                
            }else if ( 'getBusinessCreditAssessmentList'.equalsIgnoreCase(requestName) ){
                response.put('response_x', getBusinessCreditAssessmentList());
                
            }else if ( 'attachCreditReportWithCarId'.equalsIgnoreCase(requestName) ){
                response.put('response_x', attachCreditReportWithCarId());
                
            }else if ( 'attachCreditReport'.equalsIgnoreCase(requestName) ){
                response.put('response_x', attachCreditReport());
                
            }else if ( 'getCreditReport'.equalsIgnoreCase(requestName) ){
                response.put('response_x', getCreditReport());
                
            }else if ( 'performAutoUpdateBusinessCreditProfile'.equalsIgnoreCase(requestName) ){
                response.put('response_x', performAutoUpdateBusinessCreditProfile());
                
            }else if ( 'performBusinessCreditAssessment'.equalsIgnoreCase(requestName) ){
                response.put('response_x', performBusinessCreditAssessment());
                
            }else if ( 'removeCreditReport'.equalsIgnoreCase(requestName) ){
                response.put('response_x', removeCreditReport());
                
            }
            
        }
    
    
    /*------------------------------------------------------------*/
    private CreditAssessmentPing.pingResponse_element
        ping(){
           CreditAssessmentPing.pingResponse_element 
                response_x = new CreditAssessmentPing.pingResponse_element();
            return response_x;
        }
    
    private CreditAssessmentRequestResponse.getBusinessCreditAssessmentListResponse_element
        getBusinessCreditAssessmentList(){
           CreditAssessmentRequestResponse.getBusinessCreditAssessmentListResponse_element
                response_x = new CreditAssessmentRequestResponse.getBusinessCreditAssessmentListResponse_element();
            return response_x;
        }
    
    
    
    private CreditAssessmentRequestResponse.attachCreditReportResponse_element
        attachCreditReportWithCarId(){
           CreditAssessmentRequestResponse.attachCreditReportResponse_element
                response_x = new CreditAssessmentRequestResponse.attachCreditReportResponse_element();
            return response_x;
        }
    
    private CreditAssessmentRequestResponse.attachCreditReportResponse_element
        attachCreditReport(){
           CreditAssessmentRequestResponse.attachCreditReportResponse_element
                response_x = new CreditAssessmentRequestResponse.attachCreditReportResponse_element();
            return response_x;
        }
    
    private CreditAssessmentRequestResponse.getCreditReportResponse_element
        getCreditReport(){
           CreditAssessmentRequestResponse.getCreditReportResponse_element
                response_x = new CreditAssessmentRequestResponse.getCreditReportResponse_element();
            return response_x;
        }
    
    private CreditAssessmentRequestResponse.getCreditReportListResponse_element
        getCreditReportList(){
           CreditAssessmentRequestResponse.getCreditReportListResponse_element
                response_x = new CreditAssessmentRequestResponse.getCreditReportListResponse_element();
            return response_x;
        }
    
    private CreditAssessmentRequestResponse.performAutoUpdateBusinessCreditProfileResponse_element
        performAutoUpdateBusinessCreditProfile(){
           CreditAssessmentRequestResponse.performAutoUpdateBusinessCreditProfileResponse_element
                response_x = new CreditAssessmentRequestResponse.performAutoUpdateBusinessCreditProfileResponse_element();
            return response_x;
        }
    
    private CreditAssessmentRequestResponse.performBusinessCreditAssessmentResponse_element
        performBusinessCreditAssessment(){
           CreditAssessmentRequestResponse.performBusinessCreditAssessmentResponse_element
                response_x = new CreditAssessmentRequestResponse.performBusinessCreditAssessmentResponse_element();
            return response_x;
        }
    
    private CreditAssessmentRequestResponse.removeCreditReportResponse_element
        removeCreditReport(){
           CreditAssessmentRequestResponse.removeCreditReportResponse_element
                response_x = new CreditAssessmentRequestResponse.removeCreditReportResponse_element();
            return response_x;
        }
    
    
    
}