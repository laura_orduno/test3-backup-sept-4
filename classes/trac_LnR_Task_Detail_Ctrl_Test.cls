/**
 * Unit Tests for the trac_LnR_Task_Detail_Ctrl Visualforce Controller Extension.
 *
 * @author Matt Freedman, Traction on Demand
 * @date 2017-01-07
 */
@isTest
private class trac_LnR_Task_Detail_Ctrl_Test {

	@testSetup
	private static void setup() {
		Supplemental_Activity_Data__c taskExtend = new Supplemental_Activity_Data__c();
		insert taskExtend;

		Task tsk = new Task(Supplemental_Activity_Data__c = taskExtend.Id);
		insert tsk;
	}

	@isTest
	private static void testController() {
		Task tsk = [SELECT Id FROM Task];

		// Instantiate the Standard Controller and Controller Extension
		ApexPages.currentPage().getParameters().put('id', tsk.Id);
		ApexPages.StandardController stdController = new ApexPages.StandardController(tsk);
		trac_LnR_Task_Detail_Ctrl controller = new trac_LnR_Task_Detail_Ctrl(stdController);

		Supplemental_Activity_Data__c taskExtend = [SELECT Id FROM Supplemental_Activity_Data__c];

		System.assertEquals(taskExtend.Id, controller.taskExtendRecord.Id);
	}

	@isTest
	private static void testController_noSupplemental() {
		Task tsk = [SELECT Id FROM Task];
		tsk.Supplemental_Activity_Data__c = null;
		update tsk;

		// Instantiate the Standard Controller and Controller Extension
		ApexPages.currentPage().getParameters().put('id', tsk.Id);
		ApexPages.StandardController stdController = new ApexPages.StandardController(tsk);
		trac_LnR_Task_Detail_Ctrl controller = new trac_LnR_Task_Detail_Ctrl(stdController);

		System.assertEquals(null, controller.taskExtendRecord);
	}

}