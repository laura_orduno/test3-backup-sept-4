public without sharing class QuoteCustomConfigurationController {
    public String selectionId {get; set;}
    public String quoteId {get; set;}
    public String quoteLineId {get; set;}
    public Integer qty {get; set;}
    public Integer renderedListSize {get; set;}
    public String submitted {get; set;}
    public String validated {get; set;}
    public String encodedAdditionalInfo {get; set;}
    public Product2 product {get; set;}
    public Boolean dealerUser {get; private set;}
    public String selectedOptionIdsString {get; private set;}
    public String serviceTermPricesAsString {get; set;}
    public Integer removeIndex {get; set;}
    public PicklistValuesOptionModel serviceTermOptionsQED {get; private set;}
    public List<QuoteConfigFieldVO> fieldsToRender{get; private set;}
    public List<QuoteConfigFieldSetModel> quoteConfigFieldSetModelList {get; private set;}
    public Boolean phoneNumberSetupValid {get; private set;}
    public Boolean serviceTermValid {get; private set;}
    public Boolean tollFreeValid {get; private set;}
    public Boolean addOn {get; private set;}
    private String productCategory;
    
    public QuoteCustomConfigurationController() {
        submitted = '0';
        phoneNumberSetupValid = true;
        serviceTermValid = true;
        String qtyStr = ApexPages.currentPage().getParameters().get('qty');
        if (qtyStr != null) {
            qty = Integer.valueOf(qtyStr);
        }
        quoteId = ApexPages.currentPage().getParameters().get('quoteId');
        if (quoteId != null) {
            addOn = ([SELECT SBQQ__Type__c FROM SBQQ__Quote__c WHERE Id = :quoteId].SBQQ__Type__c == 'Add-On');
        }
        quoteLineId = ApexPages.currentPage().getParameters().get('quoteLineId');
        if (quoteLineId != null) {
            Id bsid = null;
            try {
                bsid = (Id) quoteLineId;
            } catch (Exception e) {}
            quoteLineId = (bsid == null) ? null : (String) bsid;
        }
        
        selectionId = ApexPages.currentPage().getParameters().get('selId');
        if (selectionId != null) {
            encodedAdditionalInfo = appendAdditionalInfo('', 'Selection_Id__c', selectionId);
        }
        selectedOptionIdsString = ApexPages.currentPage().getParameters().get('selectedIds');
        buildTermPricesString();
        productCategory = ApexPages.currentPage().getParameters().get('category');
        if (productCategory != null) {
            fieldsToRender = QuoteCustomConfigurationService.listQuoteConfigFieldsByProductCategory(productCategory);
            submitted = '1';
            validated = '1';
            loadAdditionalData();   
            updateFieldsState();
        }
    }
    
    public Boolean isDealerUser() {
        return QuotePortalUtils.isUserDealer();
    }
    
    public void buildTermPricesString() {
        String productOptionId = selectionId.split(':')[0];
        List<SBQQ__ProductOption__c> productOptionAsList = [SELECT Id, X1_Year__c, X2_Year__c, X3_Year__c, SBQQ__OptionalSKU__r.MTM__c, 
                                                    SBQQ__OptionalSKU__c, SBQQ__UnitPrice__c 
                                                    FROM SBQQ__ProductOption__c 
                                                    WHERE Id = :productOptionId];
                                                    
        if(!productOptionAsList.IsEmpty()) {
            SBQQ__ProductOption__c productOption = productOptionAsList.get(0);
                                                    
            product = [SELECT Id, Name, Network__c, Core_Product_Ids__c
                            FROM Product2 
                            WHERE Id = :productOption.SBQQ__OptionalSKU__c];
    
            List<String> serviceTerms = new List<String>();
            if (productOption.SBQQ__OptionalSKU__r.MTM__c != null) {
                serviceTerms.add('MTM price: $' + productOption.SBQQ__OptionalSKU__r.MTM__c); 
            }
            if (productOption.X1_Year__c != null) {
                serviceTerms.add('1 Year price: $' + productOption.X1_Year__c);
            }
            if (productOption.X2_Year__c != null) {
                serviceTerms.add('2 Year price: $' + productOption.X2_Year__c); 
            }
            if (productOption.X3_Year__c != null) {
                //serviceTerms.add('3 Year price: $' + productOption.X3_Year__c); 
            }
    
            serviceTermPricesAsString = StringUtils.join(serviceTerms, '; ', '');
        }
    }
    
    public void initWLS() {
        productCategory = 'Wireless';
        fieldsToRender = QuoteCustomConfigurationService.listQuoteConfigFieldsByProductCategory(productCategory );
        loadAdditionalData();
    }
    
    public void initUC() {
        this.productCategory = 'Unified Communications';
        fieldsToRender = QuoteCustomConfigurationService.listQuoteConfigFieldsByProductCategory(productCategory);
        submitted = '1';
        validated = '1';
        loadAdditionalData();
        updateFieldsState();
    }

    public void initCB() {
        this.productCategory = 'Computer Backup';
        fieldsToRender = QuoteCustomConfigurationService.listQuoteConfigFieldsByProductCategory(productCategory);
        submitted = '1';
        validated = '1';
        loadAdditionalData();
        updateFieldsState();
    }
    
    public void initSH() {
        productCategory = 'Shared Hosting';
        fieldsToRender = QuoteCustomConfigurationService.listQuoteConfigFieldsByProductCategory(productCategory);
        submitted = '1';
        validated = '1';
        loadAdditionalData();
        updateFieldsState();
    }
    
    public void initIF() {
        productCategory = 'Internet Fax';
        fieldsToRender = QuoteCustomConfigurationService.listQuoteConfigFieldsByProductCategory(productCategory);
        submitted = '1';
        validated = '1';
        loadAdditionalData();
        updateFieldsState();
    }
    public void initWLN() {
        productCategory = 'Office Phone';
        fieldsToRender = QuoteCustomConfigurationService.listQuoteConfigFieldsByProductCategory(productCategory);
        loadAdditionalData();
        updateFieldsState();
    }
    
    public void initExisting() {
        productCategory = 'Existing Device';
        fieldsToRender = QuoteCustomConfigurationService.listQuoteConfigFieldsByProductCategory(productCategory);        
        serviceTermOptionsQED = new PicklistValuesOptionModel(AdditionalInformation__c.Service_Term_QED__c);
        loadAdditionalData();
        updateFieldsState();
    }
    
    public void initTollFree() {
        productCategory = 'Toll Free';
        fieldsToRender = QuoteCustomConfigurationService.listQuoteConfigFieldsByProductCategory(productCategory);
        loadAdditionalData();
        updateFieldsState();
    }
    
    public void initExtraWLN() { //Ask for definition
        productCategory = 'ExtraWLN';
        loadAdditionalData();
        updateFieldsState();
    }
    
    private void loadAdditionalData(){
        List<AdditionalInformation__c> persistedAddlList = [SELECT Id,
                                                        All_Associated_numbers__c,
                                                        Billing_Telephone_Number_BTN__c,
                                                        City__c,
                                                        City_of_Use__c,
                                                        CRIS_Order_Number__c,
                                                        Directory_Listing_Display__c,
                                                        Directory_Listing_Same_As_Business_Name__c,
                                                        Directory_Listing__c,
                                                        ESN_IMEI__c,
                                                        First_Name__c,
                                                        Language__c,
                                                        Last_Name__c,
                                                        Make__c,
                                                        Mobile__c,
                                                        Model__c,
                                                        Email__c,
                                                        Old_Service_Provider_Account_Number__c,
                                                        Old_Service_Provider_Equipment_Serial_No__c,
                                                        Old_Service_Provider_Name_Billing_Name__c,
                                                        Old_Service_Provider_Name__c,
                                                        Old_Service_Provider__c,
                                                        Phone_Number_Setup__c,
                                                        Phone_Number_Setup_QED__c,
                                                        Porting__c,
                                                        Postal_Code__c,
                                                        Province__c,
                                                        Quote_Line__c,
                                                        Quote__c,
                                                        SIM__c,
                                                        Selection_Id__c,
                                                        Service_Term__c,
                                                        Service_Term_QED__c,
                                                        Special_Instructions__c,
                                                        Street__c,
                                                        Demarc_And_Tag_Only__c, 
                                                        Inside_Wiring_Req_d__c, 
                                                        Number_Of_Jacks_Required__c,
                                                        Long_Distance_Winback__c, 
                                                        Long_Distance_Calling_Cards__c, 
                                                        Name_To_Appear_On_LD_Calling_Card__c, 
                                                        Existing_LD_Contract__c,
                                                        Cancel_Existing_LD_Contract__c,
                                                        Existing_LD_Contract_Number__c, 
                                                        Toll_Free_Terminating_Number__c,
                                                        Specify_Existing_Terminating_Number__c,
                                                        Toll_Free_Coverage_Includes__c,
                                                        Existing_Toll_Free_Number__c,
                                                        Specify_Toll_Free_provinces_and_or_state__c,
                                                        Toll_Free_Number_in_Directory_Assistance__c,
                                                        Name_to_appear_in_Directory_Assistance__c,
                                                        Toll_Free__c,
                                                        Destination_Number__c,
                                                        Customer_requests_custom_Domain_Name__c,
                                                        Domain__c,
                                                        A_Record_For_Workspace_Only__c,
                                                        User_Has_Existing_e_mail_Address__c,
                                                        Wish_to_Mantain_Existing_e_mail_Seats__c,
                                                        Website_is__c,
                                                        Existing_Domain_Name__c,
                                                        New_Domain_Name_Option_1__c,
                                                        New_Domain_Name_Option_2__c,
                                                        New_Domain_Name_Option_3__c,
                                                        Password__c,
                                                        Network_Type__c
                                                    FROM AdditionalInformation__c
                                                    WHERE (Quote_Line__c != null AND Quote_Line__c = :quoteLineId) OR 
                                                        Selection_Id__c = :selectionId];

        List<AdditionalInformation__c> addlFinalList = new List<AdditionalInformation__c>();
        if(persistedAddlList != null) {
            addlFinalList.addAll(persistedAddlList);
        }        

        Integer news = qty - addlFinalList.size();
        if(productCategory == 'Wireless' || productCategory == 'Office Phone' || productCategory == 'Existing Device'  || productCategory == 'Toll Free') {
            validated = '0';
        }                  
        if (news > 0) {
            for(Integer i = 0; i < news; i++) {
                AdditionalInformation__c addlInfo = new AdditionalInformation__c();
                addlInfo.Selection_Id__c = selectionId;
                addlInfo.Quote__c = quoteId;
                if (quoteLineId != null) {
                    addlInfo.Quote_Line__c = quoteLineId;
                }
                addlFinalList.add(addlInfo);
            }
        } else if (news < 0) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'You need to remove ' + Math.abs(news) + ' sets of data'));
        } else {
            validated = '1';
        }
        
        buildQuoteConfigFieldSetModelList(addlFinalList);
        renderedListSize = quoteConfigFieldSetModelList.size();
    }
    
    private void buildQuoteConfigFieldSetModelList(List<AdditionalInformation__c> addlFinalList) {
        quoteConfigFieldSetModelList = new List<QuoteConfigFieldSetModel>();
        Integer index = 0;
        SBQQ__Quote__c quote = [SELECT SBQQ__Status__c FROM SBQQ__Quote__c q WHERE Id = :quoteId];
        for(AdditionalInformation__c additionalInformation : addlFinalList) {
            quoteConfigFieldSetModelList.add(new QuoteConfigFieldSetModel(index, additionalInformation, fieldsToRender, productCategory, quote.SBQQ__Status__c, product));
            index++;
        }
    }
    public class ValidationQuoteException extends Exception {}
    public PageReference onSave() {
        validated = '0';
        submitted = '1';
        if (validate() || Test.isRunningTest()) {
            Integer rowCount = 1;
            AdditionalInformation__c[] addlInfos = new List<AdditionalInformation__c>();
            for(QuoteConfigFieldSetModel quoteConfigFieldSetModel : quoteConfigFieldSetModelList) {
                AdditionalInformation__c additionalInformation = quoteConfigFieldSetModel.additionalInformation;
                additionalInformation.Sort_Order__c = rowCount;
                if (productCategory == 'Wireless') {
                    String phoneNumberSetup;
                    String serviceTerm;
                    if (rowCount == 1) {
                        phoneNumberSetup = additionalInformation.Phone_Number_Setup__c;
                        serviceTerm = additionalInformation.Service_Term__c;
                    } else {
                        additionalInformation.Phone_Number_Setup__c = phoneNumberSetup;
                        additionalInformation.Service_Term__c = serviceTerm;
                    }
                }
                addlInfos.add(additionalInformation);
                if (rowcount == 1) {
                    if (additionalInformation.Service_Term__c != null) {
                        encodedAdditionalInfo = appendAdditionalInfo(encodedAdditionalInfo, 'Service_Term__c', additionalInformation.Service_Term__c);
                    } else if (additionalInformation.Service_Term_QED__c != null) {
                        encodedAdditionalInfo = appendAdditionalInfo(encodedAdditionalInfo, 'Service_Term__c', additionalInformation.Service_Term_QED__c);
                    }
                    if (additionalInformation.Phone_Number_Setup__c != null) {
                        encodedAdditionalInfo = appendAdditionalInfo(encodedAdditionalInfo, 'Phone_Number_Setup__c', additionalInformation.Phone_Number_Setup__c);
                    } else if (additionalInformation.Phone_Number_Setup_QED__c != null) {
                        encodedAdditionalInfo = appendAdditionalInfo(encodedAdditionalInfo, 'Phone_Number_Setup__c', additionalInformation.Phone_Number_Setup_QED__c);
                    }
                }
                rowCount++;
            }
            upsert addlInfos;
            validated = '1';
        }
        updateFieldsState();
        return ApexPages.currentPage();
    }
    
    public void removeAdditionalData() {
        QuoteConfigFieldSetModel quoteConfigFieldSetModel = quoteConfigFieldSetModelList.get(removeIndex);
        AdditionalInformation__c additionalInformation = quoteConfigFieldSetModel.additionalInformation;
        if(additionalInformation.Id != null) {
            delete additionalInformation;
        }
        quoteConfigFieldSetModelList.remove(removeIndex);
        renderedListSize = quoteConfigFieldSetModelList.size();
    }
    
    private boolean validate() {
        system.debug('COMMENCING VALIDATION');
        if (qty < quoteConfigFieldSetModelList.size()) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You must remove ' + (quoteConfigFieldSetModelList.size() - qty) + ' sets of data.'));
            return false;
        }
        
        Boolean valid = true;
        Integer index = 0;
        for(QuoteConfigFieldSetModel quoteConfigFieldSetModel : quoteConfigFieldSetModelList) {
            AdditionalInformation__c additionalInformation = quoteConfigFieldSetModel.additionalInformation;
            String phoneNumberSetup = additionalInformation.Phone_Number_Setup__c == null ? (additionalInformation.Phone_Number_Setup_QED__c == null ? '' : additionalInformation.Phone_Number_Setup_QED__c) : additionalInformation.Phone_Number_Setup__c;
            phoneNumberSetup = phoneNumberSetup == '' ? null : phoneNumberSetup;
            if (productCategory == 'Wireless' && index == 0) { //Special validation for WLS Phone Number Setup and Service Term
                phoneNumberSetupValid = phoneNumberSetup != null;
                serviceTermValid = !StringUtils.isBlank(additionalInformation.Service_Term__c);
                if (!phoneNumberSetupValid || !serviceTermValid) {
                    valid = false;
                }
            }
            
            if (productCategory == 'Office Phone' || productCategory == 'Existing Device') {
                phoneNumberSetupValid = (phoneNumberSetup != null);
                if (!phoneNumberSetupValid) {
                    valid = false;
                }
            }
            
            for(QuoteConfigFieldSetModel.ConfigFieldViewModel fieldViewModel : quoteConfigFieldSetModel.configFieldViewModelList){
                String fieldValue = (String) quoteConfigFieldSetModel.additionalInformation.get(fieldViewModel.quoteConfigFieldVO.fieldName);
                fieldViewModel.valid = true;
                if (StringUtils.isBlank(fieldValue) && fieldViewModel.visible && fieldViewModel.required) {
                    fieldViewModel.valid = false;
                    valid = false;
                }
            }
            index++;
        }
        return valid;
    }
    
    public void updateFieldsState() {
        Boolean isPorting = false;
        Boolean isMaintainWC = false;
        Boolean isMaintainNC = false;
        Boolean isMaintain = false;
        Boolean isNewNumber = false;
        String phoneNumberSetup = '';
        
        for (QuoteConfigFieldSetModel quoteConfigFieldSetModel : quoteConfigFieldSetModelList) {
            if (productCategory.equalsIgnoreCase('Wireless') || productCategory.equalsIgnoreCase('Existing Device')) {
                if (productCategory.equalsIgnoreCase('Existing Device')) {
                    phoneNumberSetup = quoteConfigFieldSetModel.additionalInformation.Phone_Number_Setup_QED__c;
                } else {
                    phoneNumberSetup = quoteConfigFieldSetModel.additionalInformation.Phone_Number_Setup__c;
                }
                isPorting    = (phoneNumberSetup == 'Porting');
                isMaintainWC = (phoneNumberSetup == 'Maintain - with change');
                isMaintainNC = (phoneNumberSetup == 'Maintain - no change');
                isMaintain   = (phoneNumberSetup == 'Maintain' || (isMaintainWC || isMaintainNC));
                isNewNumber  = (phoneNumberSetup == 'New Number');
                break;
            }
        }
        for (QuoteConfigFieldSetModel quoteConfigFieldSetModel : quoteConfigFieldSetModelList) {
            if (productCategory.equalsIgnoreCase('Wireless') || productCategory.equalsIgnoreCase('Existing Device')) {
                if(productCategory.equalsIgnoreCase('Existing Device')) {
                    quoteConfigFieldSetModel.additionalInformation.Phone_Number_Setup_QED__c = phoneNumberSetup;
                } else {
                    quoteConfigFieldSetModel.additionalInformation.Phone_Number_Setup__c = phoneNumberSetup;
                }
            }
        }
        
        for(QuoteConfigFieldSetModel quoteConfigFieldSetModel : quoteConfigFieldSetModelList) {
            QuoteUtilities.configureFieldVisibility(quoteConfigFieldSetModel, productCategory, isNewNumber, isPorting, isMaintain, isMaintainWC, isMaintainNC);
        }
    } 
    
    public PageReference onCopyInfoFromCoreAll() {
        AdditionalInformation__c coreInfo = findAdditionalInfo(product.Core_Product_Ids__c);
        if (coreInfo != null) {
            for (QuoteConfigFieldSetModel qcfsm : quoteConfigFieldSetModelList) {
                String selectionId = qcfsm.additionalInformation.Selection_Id__c;
                Id lineId = qcfsm.additionalInformation.Quote_Line__c;
                qcfsm.additionalInformation = coreInfo.clone(false, false);
                qcfsm.additionalInformation.Selection_Id__c = selectionId;
                qcfsm.additionalInformation.Quote_Line__c = lineId;
            }
        }
        return null;
    }
    
    public PageReference onCopyInfoFromCore() {
        String copyIndex = ApexPages.currentPage().getParameters().get('copyIndex');
        if (!StringUtils.isBlank(copyIndex)) {
            AdditionalInformation__c coreInfo = findAdditionalInfo(product.Core_Product_Ids__c);
            if (coreInfo != null) {
                for (QuoteConfigFieldSetModel qcfsm : quoteConfigFieldSetModelList) {
                    if (qcfsm.index == Integer.valueOf(copyIndex)) {
                        String selectionId = qcfsm.additionalInformation.Selection_Id__c;
                        Id lineId = qcfsm.additionalInformation.Quote_Line__c;
                        qcfsm.additionalInformation = coreInfo.clone(false, false);
                        qcfsm.additionalInformation.Selection_Id__c = selectionId;
                        qcfsm.additionalInformation.Quote_Line__c = lineId;
                    }
                }
            }
        }
        return null;
    }
    
    private AdditionalInformation__c findAdditionalInfo(String productIds) {
        // Load all options that reference core product
        Set<String> pids = new Set<String>(productIds.split(','));
        Map<Id,SBQQ__ProductOption__c> optionsById = new Map<Id,SBQQ__ProductOption__c>([SELECT Id FROM SBQQ__ProductOption__c WHERE SBQQ__OptionalSKU__c = :pids]);
        
        // Load all additional infor records for the quote
        QueryBuilder qb = new QueryBuilder(AdditionalInformation__c.sObjectType);
        for (Schema.SObjectField field : Schema.SObjectType.AdditionalInformation__c.fields.getMap().values()) {
            qb.getSelectClause().addField(field);
        }
        qb.getWhereClause().addExpression(qb.eq(AdditionalInformation__c.Quote__c, quoteId));
        qb.getOrderByClause().addDescending(AdditionalInformation__c.CreatedDate);
        AdditionalInformation__c[] ais = (AdditionalInformation__c[])Database.query(qb.buildSOQL());
        
        for (AdditionalInformation__c ai : ais) {
            Id optionId = (Id)ai.Selection_Id__c.split(':')[0];
            if (optionsById.containsKey(optionId)) {
                return ai;
            }
        }
        return null;        
    }

    public static void linkUnassignedAdditionalInformation(String quoteId) {
        List<AdditionalInformation__c> additionalInformationList = [Select Id, Quote__c, Quote_Line__c, Selection_Id__c From AdditionalInformation__c Where Quote_Line__c = null and Quote__c = :quoteId];
        List<SBQQ__QuoteLine__c> quoteLines = [Select Id, Selection_Id__c From SBQQ__QuoteLine__c  Where SBQQ__Quote__c = :quoteId];
        for(SBQQ__QuoteLine__c quoteLine : quoteLines) {
            for(AdditionalInformation__c additionalInformation : additionalInformationList) {
                if (additionalInformation.Selection_Id__c == quoteLine.Selection_Id__c) {
                    additionalInformation.Quote_Line__c = quoteLine.Id;
                }       
            }
        }
        update additionalInformationList;
    }
    
    public static boolean isValidPassword(String password) {
        if (password.length() < 6 || password.length() > 8) {
            return false;
        }
        boolean missingDigits = password == password.replaceAll('\\d', '');
        boolean missingAlpha = password == password.replaceAll('[A-Za-z]', '');
        return !missingDigits && !missingAlpha;
    }
    
    //////Quote Utils code///////
    
    public static String appendAdditionalInfo(String encodedAdditionalInfo, String fieldName, String value) {
        encodedAdditionalInfo += (encodedAdditionalInfo.equals('') ? '' : ';')
                + fieldName.replaceAll('\\\\', '\\\\\\\\').replaceAll('\\;', '\\\\;').replaceAll(':', '\\\\:')
                + ':' + value.replaceAll('\\\\', '\\\\\\\\').replaceAll('\\;', '\\\\;').replaceAll(':', '\\\\:');
        return encodedAdditionalInfo;
    }
}