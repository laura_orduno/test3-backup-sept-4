public class ENTPSev1Ctlr {

	String country = null;
	public string SelectChoice = null;
	public static String userLanguage {
		get {
			return PortalUtils.getUserLanguage();
		}
		set;
	}

	public ENTPSev1Ctlr() {
	}

	public PageReference test() {
		List<User> users = [SELECT Id, FirstName, LastName, email, ContactId, phone FROM User WHERE Id = :UserInfo.getUserId()];
		string FullName = users[0].FirstName + users[0].LastName;
		String Uemail = users[0].email;
		String contId = users[0].ContactId;
		string phoneNum = users[0].phone;
		system.debug('Answer=:' + this.country);
		SelectChoice = this.country;

		if (SelectChoice == 'YES') {
			//PageReference page = System.Page.ENTPCaseDetailPage;
			PageReference page = new PageReference('http://btwn001612.corp.ads:8081/Chat/servlet/AppMain?__lFILE=index.jsp&NAME=' + FullName + '&EMAILADDRESS=' + Uemail + '&PHONE=VARPHONE&TICKET=VARTICKET&CONTACTID=' + contId);
			page.setRedirect(true);
			return page;
		}
		if (SelectChoice == 'NO') {
			PageReference page = System.Page.ENTPNewCase_new;
			page.setRedirect(true);
			return page;
		}

		return null;

	}

	public List<SelectOption> getItems() {

		List<SelectOption> options = new List<SelectOption>();

		options.add(new SelectOption('YES', 'Yes (Will initiate a priority chat)'));

		options.add(new SelectOption('NO', 'No')); return options;

	}

	public String getCountry() {

		return country;

	}

	public void setCountry(String country) {
		this.country = country;
	}

}