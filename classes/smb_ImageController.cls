public class smb_ImageController {

	public string imageName {get;
		set {
			imageName = value;
			if (value != null) {
				imageLocation = Image_Locations__c.getValues(imageName);
				renderImage = (imageLocation != null);
			}
		}
	}
	
	public Image_Locations__c imageLocation {get;private set;}
	
	public Boolean renderImage {get;private set;}

}