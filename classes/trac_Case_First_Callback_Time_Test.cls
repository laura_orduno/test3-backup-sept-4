@isTest(SeeAllData=true)
private class trac_Case_First_Callback_Time_Test {
    static testMethod void testFirstCallback() {
        Account a = new Account();
        a.Name = 'TestAccount';
        insert a;
        Contact c = new Contact();
        c.FirstName = 'Alex';
        c.LastName = 'Miller';
        c.AccountId = a.Id;
        c.Phone = '6049969449';
        c.Email = 'amiller@tractionondemand.com';
        insert c;
        Case x = new Case(Account = a, Contact = c);
        insert x;
        Date d = Date.today();
        d.addDays(-1);
        DateTime dt = Datetime.now();
        Task t = new Task(Subject = 'First Call', ActivityDate = d, Type = 'Call', Priority = 'Test', Status = 'Not Started', WhoId = c.Id, WhatId = x.Id, Call_Outcome__c = 'not null', SMB_Call_Type__c = 'not null either');
        insert t;
        t = [Select CreatedDate From Task Where Id = :t.Id];
        x = [Select First_Callback_Time__c from Case Where Id = :x.id limit 1];
        system.assertEquals(t.CreatedDate, x.First_Callback_Time__c);
    }
}