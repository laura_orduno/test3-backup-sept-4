@isTest
private class MBREmailMessageTriggerTest {
	
	@isTest static void createComments() {
		Account acc = TestUtil.createAccount();
		List<Id> parentIds = new List<Id>{};
		List<Case> testCases = MBRTestUtils.createCCICases(5, acc.Id);
		testCases[1].Status = 'Client Issue Resolved';
		testCases[1].notifyCustomer__c = true;
		update testCases;

		Test.startTest();

		String comBody = 'testing email';
		String longParagraph = 'A while back I needed to count the amount of letters that a piece of text in an email template had (to avoid passing any character limits). Unfortunately, I could not think of a quick way to do so on my macbook and I therefore turned to the Internet.There were a couple of tools out there, but none of them met my standards and since I am a web designer I thought: why not do it myself and help others along the way? And... here is the result, hope it helps out!A while back I needed to count the amount of letters that a piece of text in an email template had (to avoid passing any character limits). Unfortunately, I could not think of a quick way to do so on my macbook and I therefore turned to the Internet.There were a couple of tools out there, but none of them met my standards and since I am a web designer I thought: why not do it myself and help others along the way? And... here is the result, hope it helps out!A while back I needed to count the amount of letters that a piece of text in an email template had (to avoid passing any character limits). Unfortunately, I could not think of a quick way to do so on my macbook and I therefore turned to the InternetThere were a couple of tools out there, but none of them met my standards and since I am a web designer I thought: why not do it myself and help others along the way? And... here is the result, hope it helps out!A while back I needed to count the amount of letters that a piece of text in an email template had (to avoid passing any character limits). Unfortunately, I could not think of a quick way to do so on my macbook and I therefore turned to the Internet.There were a couple of tools out there, but none of them met my standards and since I am a web designer I thought: why not do it myself and help others along the way? And... here is the result, hope it helps out!A while back I needed to count the amount of letters that a piece of text in an email template had (to avoid passing any character limits). Unfortunately, I could not think of a quick way to do so on my macbook and I therefore turned to the Internet.There were a couple of tools out there, but none of them met my standards and since I am a web designer I thought: why not do it myself and help others along the way? And... here is the result, hope it helps out!';
		String writeAbove = 'Testing writing above \n WRITE ABOVE THIS LINE';
		List<String> textBodies = new List<String>{comBody, longParagraph, writeAbove};

		System.debug('2014-11-15 MBREmailMessageTriggerTest.testComments(): about to call generateEmail(textBodies.size('+textBodies.size()+'), '+testCases[0].Id+')');
		generateEmail(textBodies, testCases[0].Id);

		MBRTestUtils.createComment(testCases[1].Id);
		generateEmail(textBodies, testCases[1].Id);
		Test.stopTest();

		List<CaseComment> comments = [
									SELECT CommentBody, CreatorName, createdby.firstname, createdby.lastname
									FROM CaseComment
									WHERE ParentId = :testCases[0].Id];
		for(CaseComment cc : comments){
			System.assert(!cc.CommentBody.contains('WRITE ABOVE THIS LINE'));
			System.assert(cc.CommentBody.length()<=2000);
			System.assertNotEquals(cc.CommentBody, longParagraph);
		}

		System.debug('2014-11-15 MBREmailMessageTriggerTest.testComments() COMMENTS: ' + comments);
		
		System.assertEquals(comments.size(), 3);

		List<CaseComment> closedComments = [
									SELECT CommentBody, CreatorName, createdby.firstname, createdby.lastname
									FROM CaseComment
									WHERE ParentId = :testCases[1].Id];

		System.assertEquals(closedComments.size(), 1);
	}

	@isTest static void testAttachment() {
		Test.startTest();

		Account acc = TestUtil.createAccount();
		List<Case> testCases = MBRTestUtils.createCCICases(1, acc.Id);
		String longParagraph = 'A while back I needed to count the amount of letters that a piece of text in an testing email template had (to avoid passing any character limits). Unfortunately, I could not think of a quick way to do so on my macbook and I therefore turned to the Internet.There were a couple of tools out there, but none of them met my standards and since I am a web designer I thought: why not do it myself and help others along the way? And... here is the result, hope it helps out!A while back I needed to count the amount of letters that a piece of text in an email template had (to avoid passing any character limits). Unfortunately, I could not think of a quick way to do so on my macbook and I therefore turned to the Internet.There were a couple of tools out there, but none of them met my standards and since I am a web designer I thought: why not do it myself and help others along the way? And... here is the result, hope it helps out!A while back I needed to count the amount of letters that a piece of text in an email template had (to avoid passing any character limits). Unfortunately, I could not think of a quick way to do so on my macbook and I therefore turned to the InternetThere were a couple of tools out there, but none of them met my standards and since I am a web designer I thought: why not do it myself and help others along the way? And... here is the result, hope it helps out!A while back I needed to count the amount of letters that a piece of text in an email template had (to avoid passing any character limits). Unfortunately, I could not think of a quick way to do so on my macbook and I therefore turned to the Internet.There were a couple of tools out there, but none of them met my standards and since I am a web designer I thought: why not do it myself and help others along the way? And... here is the result, hope it helps out!A while back I needed to count the amount of letters that a piece of text in an email template had (to avoid passing any character limits). Unfortunately, I could not think of a quick way to do so on my macbook and I therefore turned to the Internet.There were a couple of tools out there, but none of them met my standards and since I am a web designer I thought: why not do it myself and help others along the way? And... here is the result, hope it helps out!';
		String shortParagraph = 'testing attachment'; 
		List<EmailMessage> emList = generateEmail(new List<String>{longParagraph}, testCases[0].Id);
		//List<Attachment> attachments = MBRTestUtils.createAttachment(emList[0].Id, 1);
		List<Attachment> attachmentLong = MBRTestUtils.createAttachment(emList[0].Id, 1, 'txt');
		MBRAttachmentTrigHelper.getCommentsBeforeFuture(new List<Id>{testCases[0].Id});

		Test.stopTest();

		List<CaseComment> comments = [
									SELECT CommentBody, CreatorName, createdby.firstname, createdby.lastname
									FROM CaseComment
									WHERE ParentId = :testCases[0].Id];
		System.assertEquals(comments.size(), 1);
		System.assert(comments[0].CommentBody.contains('test attachment'));
		System.assert(comments[0].CommentBody.contains(Label.MBR_See_Attachment));
	}

	@isTest static void testWrongExtension() {
		Test.startTest();

		Account acc = TestUtil.createAccount();
		List<Case> testCases = MBRTestUtils.createCCICases(1, acc.Id);
		String shortParagraph = 'testing attachment'; 
		List<EmailMessage> emList = generateEmail(new List<String>{shortParagraph}, testCases[0].Id);
		//List<Attachment> attachments = MBRTestUtils.createAttachment(emList[0].Id, 1);
		List<Attachment> attachmentLong = MBRTestUtils.createAttachment(emList[0].Id, 1, 'wrong');

		Test.stopTest();

		List<CaseComment> comments = [
									SELECT CommentBody, CreatorName, createdby.firstname, createdby.lastname
									FROM CaseComment
									WHERE ParentId = :testCases[0].Id];
		System.assertEquals(comments.size(), 1);
		System.assert(comments[0].CommentBody.contains('testing'));
		System.assert(!comments[0].CommentBody.contains(Label.MBR_See_Attachment));
	}

	@isTest static void testNewCasePrevention() {
		Test.startTest();

		Account acc = TestUtil.createAccount();
		List<Case> testCases = MBRTestUtils.createCCICases(1, acc.Id);
		String shortParagraph = 'testing attachment'; 
		EmailMessage newEmail =  MBRTestUtils.createEmail(testCases[0].Id, true, shortParagraph);
		try{
			newEmail.TextBody = shortParagraph;
			insert newEmail;
			throw new MyException('Exception wasnt trown and new case was created'); // 1. If we get to this line it means an error was not added and the test class should throw an exception here.
		}
		catch(Exception e){
			System.assert(e.getMessage().contains('Cannot create a new case. Please add Thread Id'));
		} 
		Test.stopTest();

	}

	testmethod static void VITILcare_testLynxTicketEmailFromAprapos() {        
		Account acc = TestUtil.createAccount();
//		List<Case> testCases = MBRTestUtils.createCCICases(1, acc.Id);
		List<Case> testCases = MBRTestUtils.createVITILcareCases(1, acc.Id);
		String emailBody1 = '987654321 ------ testing attachment VITILcare Case Id: ' + testCases[0].id + ' Case number: 01513485 '; 
		String emailBody2 = '987654321 ------ testing attachment VITILcare Update Lynx Ticket Number Case Id: ' + testCases[0].id + ' Case number: 01513485'; 
		String emailBody3 = '987654321 ------ testing attachment Case Id: ' + testCases[0].id; 

        EmailMessage email1 =  MBRTestUtils.createEmail(testCases[0].Id, true, emailBody1);
        email1.ToAddress = 'vitilcare.dev@telus.com';            
		EmailMessage email2 =  MBRTestUtils.createEmail(testCases[0].Id, true, emailBody2);
        email2.ToAddress = 'vitilcare.dev@telus.com';
		EmailMessage email3 =  MBRTestUtils.createEmail(testCases[0].Id, true, emailBody3);
        email3.ToAddress = 'vitilcare.dev@telus.com';
		EmailMessage email4 =  MBRTestUtils.createEmail(testCases[0].Id, true, '');
        email4.ToAddress = 'vitilcare.dev@telus.com';
        
        List<EmailMessage> emails = new List<EmailMessage>();
		try{
            
            emails.add(email1);
            emails.add(email2);
            emails.add(email3);
            emails.add(email4);
            
			insert emails;
            
			throw new MyException('Exception wasnt trown and new case was created'); // 1. If we get to this line it means an error was not added and the test class should throw an exception here.
		}
		catch(Exception e){
System.debug('EXCEPTION: ' + e.getMessage());            
//			System.assert(e.getMessage().contains('Cannot create a new case. Please add Thread Id'));
		} 
        
        
        
/*****        
		List<Case> testCases = MBRTestUtils.createVITILcareCases(1, acc.Id);
		String textBody = '0000008' +
			'----------------------------' + 
			'From: vitilcare.dev@telus.com' +
			'Sent: Monday, September 21, 2015 5:33:55 PM' +
			'To: cimuat.e2e@telus.com' +
			'CC: ' + 
			'Subject: Sandbox: [R-01513485] Re: SFDC VITILcare - new ticket submitted by Test RCID -#' +
			'Please do not detele the original content when replying to this email. ' +
			'New ticket has been submitted via Salesforce VITILcare portal. ' +
			'Ticket details: ' + 
			'Case Id: 1234567' + 
			'Case number: 01513485 ' + 
			'Submitted by: ' + 
			'Contact: Oksana1 VITILcare Test1 ' + 
			'Contact phone: 9999999991 ' + 
			'Contact email: oksana.sergiyko@gmail.com ' + 
			'Company: Test RCID ' + 
			'RCID: 0003206700 ' +
			'Description: trouble description #5 ' +
			'Ticket Details: ' +
			'Project Number: 8765876578 Phone numbers/devices affected: phone1; phone2; Site address: Site address Comments: Comments blah blah Reported by name: Reported by name Reported by company: Reported by company Reported by phone number: Reported by phone Reported by email address: Reported by email Onsite contact name: Onsite contact name Onsite contact phone number: Onsite contact company Site company name: Site company name Access hours/days: 8-5/Mon-Fri Customer ticket number: 345 Customer is able to make outgoing connections?: Yes All devices are affected?: No Recent/ongoing power issues?: No ' ;
     
        EmailMessage newEmail = new EmailMessage(ParentId = testCases[0].Id,
                                Incoming = true,
                                TextBody = textBody + '\n write above this line [ref:_001]',
                                Subject = '[ref:_001]',
                                FromAddress = 'cciuat@telus.com',
                                ToAddress = 'vitilcare.dev@telus.com'
                                );
        
//        EmailMessage newEmail =  MBRTestUtils.createEmail(testCases[0].Id, true, textBody);
		try{
//			newEmail.TextBody = textBody;
			insert newEmail;
			throw new MyException('Exception wasnt trown and new case was created'); // 1. If we get to this line it means an error was not added and the test class should throw an exception here.
		}
		catch(Exception e){
			System.assert(e.getMessage().contains('Cannot create a new case. Please add Thread Id'));
		} 
		Test.stopTest();
*/
	}
    
	public static List<EmailMessage> generateEmail(List<String> emailBody, Id caseId){
		List<EmailMessage> emails = new List<EmailMessage>{};
		for(String str : emailBody){
			System.debug('2014-11-15 MBREmailMessageTriggerTest.generateEmail(): about to call MBRTestUtils.createEmail('+caseId+', true, str)');
			EmailMessage validEmail = MBRTestUtils.createEmail(caseId, true, str);
			emails.add(validEmail);
		}
		System.debug('2014-11-15 MBRTestUtils.createEmail(): about to insert ' + emails.size() + ' emails');
		insert emails;
		return emails;
	}
}