public class ContractSavePropagateCtrlExt {
    
    public String mode {get;set;}
    public String modeLabel {get;set;}
    private  ApexPages.StandardController controller;
    
    public Map<id,sObject> selectedItemMap {get; set;}
    public List<ItemSelect> displayItems {get;set;}
    public Sobject oldValues {get;set;}
    public List<String> updatedFields {get;set;}
    
    public Sobject currentObj {get;set;} 
    
    public Static String PROPERGATE_TRGET_OBJNAME = 'Subsidiary_Account__c';
    public Static String AGREEMENT_OBJNAME = 'Contract';
    public Static String[] DETAIL_FIELDSETS = new String[]{'New_Details','New_Detail_Activations','New_Details_Billing','New_Detail_Renewal','New_Detail_Churn'};
        
    public ContractSavePropagateCtrlExt(ApexPages.StandardController stdController){
        controller = stdController;
        currentObj = stdController.getRecord();
        displayItems = new  List<ItemSelect>();
        updatedFields = new List<string>();
        mode = 'maindetail';
    }
    
    public void doEdit(){
        mode = 'edit';
        modeLabel = Label.Edit;
    }
    
    public PageReference doCancel(){
        mode = 'maindetail';
        PageReference pr =  Page.ContractCareViewPage;
        pr.getParameters().put('id',currentObj.id);
        pr.setRedirect(true);
        return pr;
    }
    
    public void doSave(){
        mode = 'maindetail';
        modeLabel = Label.Detail;
        controller.save();
    }
    
    public PageReference SavePropagate() {
        List<Sobject> toUpdate = new List<Sobject>();
        for(ItemSelect obj : displayItems){
            if(obj.selected){
                for(String f :updatedFields){
                    obj.obj.put(f, currentObj.get(f));
                }
                toUpdate.add(obj.obj);
            }
        }
        controller.save();
        update toUpdate;
        PageReference pg = Page.ContractCareViewPage;
        pg.setRedirect(true);
        pg.getParameters().put('id',currentObj.id);
        return pg;
    }
    
    public PageReference showPropagate(){
        displayItems.clear();
        updatedFields.clear();
        mode = 'detail';
        PageReference pg =Page.ContractPropargatePage;
        currentObj = controller.getRecord();
        selectedItemMap = new Map<id,sObject>();
        String query = Util.queryBuilder(PROPERGATE_TRGET_OBJNAME,new List<String>{'id','Account_Name__c','Account__c','Subsidiary_Account__c.Name' },
                                         new List<string>{'Contract__c'+'=\''+currentObj.id+'\''});
        List<Sobject> itemList = Database.query(query);
        List<String>  fields = Util.getFieldSetFields('Contract', 'New_Detail_Activations');
        String oldRecQuery = Util.queryBuilder('Contract',Util.getUpdatableFields('Contract'),new List<string>{'id'+'=\''+currentObj.id+'\''});
        List<Sobject> oldRecs =Database.query(oldRecQuery);
        oldValues = oldRecs.get(0);
        for(Sobject item : itemList) {
            if(selectedItemMap.containsKey(item.id)){
                displayItems.add(new ItemSelect(item,true));    
            }else{
                displayItems.add(new ItemSelect(item,false));
            }
        }
        Set<String> restrictedTo = new Set<String>();
        for(String fs :DETAIL_FIELDSETS){
            restrictedTo.addALL(Util.getFieldSetFields(AGREEMENT_OBJNAME, fs));
        }
        Set<String> excludeFieldSet = new Set<String>();
        excludeFieldSet.addAll(Util.getFieldSetFields(AGREEMENT_OBJNAME, 'Exclude_Propagat'));
        updatedFields = getChangesFields(AGREEMENT_OBJNAME, currentObj,oldValues,restrictedTo, excludeFieldSet);
        return pg;
        
    }
    
    private List<String> getChangesFields(String objName, SObject newInstance, Sobject oldInstance,Set<String> restrictList,Set<String> exclude){
        List<String> updatedField = new List<String>();
        List<String>  updatableField = Util.getUpdatableFields(objName);
        for(String field : updatableField){
            if( !exclude.contains(field) && restrictList.contains(field))
                if(newInstance.get(field) != oldInstance.get(field)){
                    updatedField.add(field);
                }
        }
        return updatedField;
        
    }
    public Boolean getShowSFDCViewButton(){
        Schema.DescribeSObjectResult od = Util.getObjectTypeDescribe(AGREEMENT_OBJNAME);
        return od.isUpdateable();
    }
    public Boolean getShowEditButton(){
        boolean isEditButtonShown=true;
        Schema.DescribeSObjectResult od = Util.getObjectTypeDescribe(AGREEMENT_OBJNAME);
        try{
            //integer numMembers=[select count() from groupmember where group.name=:label.CONTRACT_CARE_VIEW_EDITABLE_GROUP_NAME and userorgroupid=:userinfo.getuserid()];
            list<groupmember> members=[select id,userorgroupid,group.type,group.relatedid from groupmember where group.name=:label.CONTRACT_CARE_VIEW_EDITABLE_GROUP_NAME];
            set<id> userIds=new set<id>();
            for(groupmember member:members){
                if(member.userorgroupid.getsobjecttype().getdescribe().getname().equalsignorecase('group')){
                    list<group> subGroups=[select id,relatedid from group where id=:member.userorgroupid and type='RoleAndSubordinatesInternal'];
                    if(!subGroups.isempty()){
                        for(group subGroup:subGroups){
                            list<userrole> subUsers=[select id from userrole where id=:subGroup.relatedid or parentroleid=:subGroup.relatedid];
                            list<user> subOrdUsers=[select id from user where userroleid=:subUsers];
                            for(user u:subOrdUsers){
                                userIds.add(u.id);
                            }
                        }
                    }
                }else{
                    userIds.add(member.userorgroupid);
                }
            }
            isEditButtonShown=(od.isupdateable()&&userIds.contains(userinfo.getuserid()));
        }catch(exception e){
            isEditButtonShown=false;
        }
        return isEditButtonShown;
    }
    
    public class ItemSelect{
        public sObject obj {get; set;}
        public boolean selected {get; set;}
        public ItemSelect(sObject obj,boolean sel) {
            this.obj = obj;
            selected = sel;
        }
    }
    
}