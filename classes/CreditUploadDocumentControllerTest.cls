@isTest
public class CreditUploadDocumentControllerTest {
    
    
    @isTest
    public static void upload() {  
        
        Test.startTest();    
        
        createTestuser();         
        User testUser = getTestuser();
        System.debug('... dyy ... testuser = ' + testUser.Name);
        
        System.runAs(testUser){  
            
            Test.setCurrentPageReference(new PageReference('dyy test page'));   
            
            
            credit_related_info__c crr = createCreditRelatedInfo();
            Credit_Profile__c cp = CreditUnitTestHelper.getCreditProfile();
            Credit_Checklist__c checklist = CreditUnitTestHelper.getChecklist();
            
            
            System.currentPageReference().getParameters().put('crr_ID', crr.id);            
            System.currentPageReference().getParameters().put('credit_profile_id', cp.id);
            
            
            System.currentPageReference().getParameters().put('doctype', 'trade');            
            ApexPages.StandardController 
                sc = new ApexPages.StandardController(new Credit_Related_Info__c());            
            CreditUploadDocumentController 
                controller = new CreditUploadDocumentController(sc);  
            uploadThisOne(controller);
            
            System.currentPageReference().getParameters().put('cp_ID', cp.id);
            System.currentPageReference().getParameters().put('crr_ID', crr.id);
            System.currentPageReference().getParameters().put('doctype', 'external');
            sc = new ApexPages.StandardController(new Credit_Related_Info__c());    
            controller = new CreditUploadDocumentController(sc);  
            uploadThisOne(controller);
            
            System.currentPageReference().getParameters().put('car_id', null);
            System.currentPageReference().getParameters().put('checklist_id', checklist.id);
            System.currentPageReference().getParameters().put('doctype', 'external');
            sc = new ApexPages.StandardController(new Credit_Related_Info__c());    
            controller = new CreditUploadDocumentController(sc);  
            uploadThisOne(controller);
            
            System.currentPageReference().getParameters().put('cp_ID', cp.id);
            System.currentPageReference().getParameters().put('crr_ID', null);
            System.currentPageReference().getParameters().put('doctype', 'external');
            sc = new ApexPages.StandardController(new Credit_Related_Info__c());    
            controller = new CreditUploadDocumentController(sc);  
            uploadThisOne(controller);
            
            System.currentPageReference().getParameters().put('doctype', 'otherSecure');
            sc = new ApexPages.StandardController(new Credit_Related_Info__c());    
            controller = new CreditUploadDocumentController(sc);  
            uploadThisOne(controller);
            
            System.currentPageReference().getParameters().put('doctype', 'public');
            sc = new ApexPages.StandardController(new Credit_Related_Info__c());    
            controller = new CreditUploadDocumentController(sc);  
            uploadThisOne(controller);
            
            System.currentPageReference().getParameters().put('doctype', 'financial');
            sc = new ApexPages.StandardController(new Credit_Related_Info__c());    
            controller = new CreditUploadDocumentController(sc);  
            uploadThisOne(controller);
            
            System.currentPageReference().getParameters().put('doctype', 'other');
            sc = new ApexPages.StandardController(new Credit_Related_Info__c());    
            controller = new CreditUploadDocumentController(sc);  
            uploadThisOne(controller);
            
            
            
            sc = new ApexPages.StandardController(new Credit_Related_Info__c());   
            controller = new CreditUploadDocumentController(sc);  
            controller.updateCreditRelatedInfo(getDocument().id);
            controller.credit_profile_id = cp.id;
            controller.businessCustomerId = 123456L;
            controller.showText ='showText';
            
            String myString = 'StringToBlob';
            Blob myBlob = Blob.valueof(myString);
            controller.renderasPdf = myBlob;
            
            
            controller.crr_Id = crr.id;
            
            controller.previewPDF();
            controller.getFolderId();
            
            Credit_Checklist__c[] chks = new List<Credit_Checklist__c>();
            chks.add(checklist);
            //controller.processChecklist(chks);
            
            
        }
        
        Test.StopTest();    
    }
    
	@isTest
    public static void upload2()
    {
        System.currentPageReference().getParameters().put('chk_ID', '1111');
        System.currentPageReference().getParameters().put('cp_ID', null);
        System.currentPageReference().getParameters().put('car_ID', null);
        
        Test.startTest();
        
        ApexPages.StandardController sc = new ApexPages.StandardController(new Credit_Related_Info__c());            
        CreditUploadDocumentController controller = new CreditUploadDocumentController(sc);
        
        Document document = new Document();
        document.body = EncodingUtil.base64Decode('Test');
        
        controller.document = document;
        
        controller.upload();
        
        /**/
        System.currentPageReference().getParameters().put('doctype', '');
        sc = new ApexPages.StandardController(new Credit_Related_Info__c());    
        controller = new CreditUploadDocumentController(sc);  
        uploadThisOne(controller);
        
        controller.previewPDFviaSFDC('123');
        
        controller.doctype = 'otherSecure';
        controller.updateCreditRelatedInfo('123');
        
        Test.stopTest();        
    }
    
    @isTest
    public static void previewPDF()
    {
        Credit_Related_Info__c CRI_Object = new Credit_Related_Info__c();
        CRI_Object.document_id__c = '1';
        
        insert CRI_Object;
        
        System.currentPageReference().getParameters().put('doctype', 'other');
        System.currentPageReference().getParameters().put('doc_ID', '1111');
        System.currentPageReference().getParameters().put('crr_ID', CRI_Object.Id);

        Test.startTest();
        
        ApexPages.StandardController sc = new ApexPages.StandardController(new Credit_Related_Info__c());            
        CreditUploadDocumentController controller = new CreditUploadDocumentController(sc);

        controller.previewPDF();
        
        Test.stopTest();
    }

	@isTest
    public static void previewPDFWithNullCrrId()
    {
        System.currentPageReference().getParameters().put('doctype', 'other');
        System.currentPageReference().getParameters().put('doc_ID', '1111');

        Test.startTest();
        
        ApexPages.StandardController sc = new ApexPages.StandardController(new Credit_Related_Info__c());            
        CreditUploadDocumentController controller = new CreditUploadDocumentController(sc);
        
        controller.previewPDF();
        
        Test.stopTest();
    }

    private static void uploadThisOne(CreditUploadDocumentController controller){    
        Credit_Assessment__c[] cars = createCars();   
        controller.document = getDocument();
        controller.car_id = cars[1].id;
        controller.upload();  
    }
    
    
    private static void createTestuser(){
        User user = new User();
        user.Username = 'dyy.dyy@telus.com';
        user.LastName = 'X33X';  
        user.Email = 'dyy.dyy@telus.com';   
        user.CommunityNickname = 'dyy';   
        user.TimeZoneSidKey = 'Pacific/Kiritimati';   
        user.LocaleSidKey = UserInfo.getLocale();   
        user.EmailEncodingKey = 'ISO-8859-1';   
        user.ProfileId = UserInfo.getProfileId();   
        user.LanguageLocaleKey = UserInfo.getLanguage();   
        user.Alias = 'dyy';
        insert user;
    }
    
    private static User getTestuser(){
        User testuser = [select id,name,UserRoleId from User where Username = 'dyy.dyy@telus.com'];            
        return testuser;
    }

    private static Document getDocument(){
        Document doc = new Document();
        doc.name = 'doc name';
        doc.Body = Blob.valueOf('Unit Test Document Body');
        doc.FolderId =UserInfo.getUserId();
        insert doc;
        return doc;
    }
    
    
    private static Credit_Assessment__c[] createCars(){
        System.debug('... dyy ... createCars ...');
        Credit_Assessment__c[] cars = new List<Credit_Assessment__c> ();
        for(Integer i=0; i<2; i++){
            cars.add(CreditUnitTestHelper.getCar());
        }
        return cars;
    }
    
    private static credit_related_info__c createCreditRelatedInfo(){
        credit_related_info__c cri = new credit_related_info__c();
        insert cri;
        return cri;
    }
    
    
	/*----------------------------------------------------------*/        
    @isTest
    public static void uploadForDocumentBodyNull() {         
        Test.startTest();  
        
        Test.setCurrentPageReference(new PageReference('a test page'));               
        ApexPages.StandardController 
            sc = new ApexPages.StandardController(new Credit_Related_Info__c());                
        CreditUploadDocumentController 
            controller = new CreditUploadDocumentController(sc);  
        
        Document document = controller.document;
        document.body = null;
        controller.upload();
        
        Test.StopTest();    
    
    }
    
    
    @isTest
    public static void updateCreditRelatedInfo() {         
        Test.startTest();  
        
        Test.setCurrentPageReference(new PageReference('a test page'));               
        ApexPages.StandardController 
            sc = new ApexPages.StandardController(new Credit_Related_Info__c());                
        CreditUploadDocumentController 
            controller = new CreditUploadDocumentController(sc);  
        
        String DocumentID = '1234586';
        controller.doctype = 'external';
        controller.updateCreditRelatedInfo(DocumentID);
        
        Test.StopTest();    
        
    }      
    @isTest
    public static void getFolderId() {         
        Test.startTest();  
        
        Test.setCurrentPageReference(new PageReference('a test page'));               
        ApexPages.StandardController 
            sc = new ApexPages.StandardController(new Credit_Related_Info__c());                
        CreditUploadDocumentController 
            controller = new CreditUploadDocumentController(sc);  
        
        controller.getFolderId();
        
        Test.StopTest();    
    
    }
    
    
}