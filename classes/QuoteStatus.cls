public class QuoteStatus {
    public static final String Draft = 'Draft';
    public static final String Sent_For_Credit_Check = 'Sent for Credit Check';
    public static final String Sent_Agreement = 'Sent Agreement, waiting acceptance';
    public static final String Signed_In_Person = 'Customer Signed Agreement in Person';
    //public static final String Accepted_Pending_Dealer_Input = 'Customer Accepted - Pending Dealer input';
    public static final String Accepted_Awaiting_Provisioning = 'Customer Accepted Agreement, Awaiting Provisioning';
    public static final String Agreement_Expired = 'Agreement Expired';
    public static final String Provisioning_In_Progress = 'Provisioning in Progress';
    public static final String Wireless_Provisioning_Complete = 'Wireless Provisioning Complete';
    public static final String Wireline_Order_Entry_Complete = 'Wireline Order Entry Complete';
    public static final String Provisioning_Complete = 'Provisioning Complete';
    
    public class Permissions {
        public string Name {get; set;}
        public integer Ordinal {get; set;}
        public boolean isSelectable {get; set;}
        public boolean isSelectableForDealer {get; set;}
        public boolean isSelectableForEE {get; set;}
        public boolean isQuoteEditable {get; set;}
        public boolean isQuoteEditableForDealer {get; set;}
        public boolean isQuoteEditableForEE {get; set;}
        public boolean isQuoteEditableAfterSigning {get; set;}
        public boolean isQuoteEditableAfterSigningForDealer {get; set;}
        public boolean isQuoteEditableAfterSigningForEE {get; set;}
        public boolean isQuoteCloneable {get; set;}
        public boolean isQuoteCloneableForDealer {get; set;}
        public boolean isQuoteCloneableAfterSigning {get; set;}
        public boolean isQuoteCloneableAfterSigningForDealer {get; set;}
        public boolean areProductsEditable {get; set;}
        public boolean areProductsEditableForEE {get; set;}
        public boolean areProductsEditableAfterSigning {get; set;}
        public boolean areProductsEditableAfterSigningForEE {get; set;}
        public boolean isAcceptedStatus {get; set;}
        public Permissions(
            String n,
            Integer o,
            Boolean isx,
            Boolean isd,
            Boolean ise,
            Boolean iqe,
            Boolean iqefd,
            Boolean iqefe,
            Boolean iqeas,
            Boolean iqeasfd,
            Boolean iqeasfe,
            Boolean iqc,
            Boolean iqcfd,
            Boolean iqcas,
            Boolean iqcasfd,
            Boolean ape,
            Boolean apefe,
            Boolean apeas,
            Boolean apeasfe,
            Boolean ias) {
            Name = n;
            Ordinal = o;
            isSelectable = isx;
            isSelectableForDealer = isd;
            isSelectableForEE = ise;
            isQuoteEditable = iqe;
            isQuoteEditableForDealer = iqefd;
            isQuoteEditableForEE = iqefe;
            isQuoteEditableAfterSigning = iqeas;
            isQuoteEditableAfterSigningForDealer = iqeasfd;
            isQuoteEditableAfterSigningForEE = iqeasfe;
            isQuoteCloneable = iqc;
            isQuoteCloneableForDealer = iqcfd;
            isQuoteCloneableAfterSigning = iqcas;
            isQuoteCloneableAfterSigningForDealer = iqcasfd;
            areProductsEditable = ape;
            areProductsEditableForEE = apefe;
            areProductsEditableAfterSigning = apeas;
            areProductsEditableAfterSigningForEE = apeasfe;
            isAcceptedStatus = ias;
        }
    }
    public static final Map<String, Permissions> permissions {get; set;}
    static {
        permissions = new Map<String, Permissions> {
            'draft' => new Permissions('Draft', 1, false/*TC0019568*/, false/*TC0019568*/, false/*TC0019568*/, true, true, true, false, false, false, false, false, false, false, true, true, false, false, false),
            
            'sent quote inquiry' => new Permissions('Sent Quote Inquiry', 2, false, false, false, true, true, true, false, false, false, false, false, false, false, true, true, false, false, false),
            
            'sent for credit check' => new Permissions('Sent for Credit Check', 3, false, false, false, true, true, true, false, false, false, false, false, false, false, true, true, false, false, false),
            
            'ee sent for credit check' => new Permissions('EE Sent for Credit Check', 4, false, false, true, true, true, true, false, false, true/*tc13135*/, false, false, false, false, true, true, false, false, false),
            
            'credit approval received' => new Permissions('Credit Approval Received', 5, false, false, false, true, true, true, false, false, true/*tc13135*/, false, false, false, false, true, true, false, false, false),
            
            'credit check failed' => new Permissions('Credit Check Failed', 6, false, false, true, true, true, true, false, false, true/*tc13135*/, false, false, false, false, true, true, false, false, false),
            
            'sent agreement, waiting acceptance' => new Permissions('Sent Agreement, waiting acceptance', 7, false, false, false, true, true, true, false, false, false, false, false, false, false, true, true, false, false, false),
            
            'customer signed agreement in person' => new Permissions('Customer Signed Agreement in Person', 8, false, false/*TC0019568*/, true, false, false, false, true, true, true, false, false, true, true, false, false, true, true/*?*/, true),
            
            'customer accepted agreement, awaiting provisioning' => new Permissions('Customer Accepted Agreement, Awaiting Provisioning', 9, false, false, false, false, false, false, true, true, true, false, false, false/*TC0019791*/, false/*TC0019791*/, false, false, false, true/*?*/, true),
            
            'agreement complete - awaiting provisioning' => new Permissions('Agreement Complete - Awaiting Provisioning', 10, false, false, false, false, false, false, true, true, true, false, false, true, true, false, false, false, true/*?*/, true),
            
            'provisioning in progress' => new Permissions('Provisioning in Progress', 11, false, false, true, false, false, false, true, true, true, false, false, true, true, false, false, false, true/*?*/, true),
            
            'held for customer' => new Permissions('Held for Customer', 12, false, false, true, true, true, true, true, true, true, false, false, true, true, true, true, false, false, false),
            
            'wireline order entry complete' => new Permissions('Wireline Order Entry Complete', 13, false, false, true, false, false, false, true, true, true, false, false, false, false, false, false, false, false, true),
            
            'wireless provisioning complete' => new Permissions('Wireless Provisioning Complete', 14, false, false, true, false, false, false, true, true, true, false, false, true, true, false, false, false, false, true),
            
            'provisioning complete' => new Permissions('Provisioning Complete', 15, false, false, true, false, false, false, true, true, true, false, false, true, true, false, false, false, false, true),
            
            'rejected to sales' => new Permissions('Rejected to Sales', 16, false, false, true, true, true, true, true, true, true, false, false, true, true, false, false, false, false, false),
            
            'client declined' => new Permissions('Client Declined', 17, true, true, true, true, true, true, true, true, true, false, false, true, true, false, false, false, false, false),
            
            'replaced agreement' => new Permissions('Replaced Agreement', 18, false/*TC0019568*/, false/*TC0019568*/, false/*TC0019568*/, true, true, true, true, true, true, false, false, true, true, false, false, false, false, false),
            
            'quote expired' => new Permissions('Quote Expired', 19, false, false, false, false, false, false, false, false, true/*tc13135*/, true, true, true, true, false, false, false, false, false),
            
            'agreement expired' => new Permissions('Agreement Expired', 20, false, false, false, false, false, false, false, false, true/*tc13135*/, true, true, true, true, false, false, false, false, false),
            
            'unserviceable' => new Permissions('Unserviceable', 21, /*Selectable*/ false, false, true, /*Editable*/ true, true, true, true, true, true, /*Cloning*/ false, false, false, false, false, false, false, false, false)
        };
    }
}