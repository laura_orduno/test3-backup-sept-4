/* Created by Tech Mahindra Ltd. 

Test Class for smb_newQuoteFromAccountController  */

@isTest
public class smb_newQuoteFromAccountController_Test {
    
    @testsetup
    static void createRecords(){
        
        RecordType rt = [select Id from RecordType where Name = 'RCID' and SobjectType = 'Account' limit 1];        
        RecordType recordType = [select Id from RecordType where Name = 'Complex Provisioning Handoff' and SobjectType = 'Work_Request__c' limit 1];
        
        Account act =new Account();
        act.Name= 'Test Account';
        act.Phone='1234567890';        
        act.RecordTypeId = rt.Id;
        insert act;
                        
        Contact new_contact=new Contact();
        new_contact.AccountId =act.Id;
        new_contact.FirstName = 'Test Fname';
        new_contact.LastName='TestLName';
        new_contact.Email='TestEmail@Test.com';
        new_contact.Title='Test ContatcTitle';
        new_contact.Phone='44444444';
        new_contact.Language_Preference__c='English';
        insert new_contact;
        
        Opportunity oppt = new Opportunity();
        Oppt.CloseDate = Date.today();
        Oppt.Name = 'Test Opp';
        Oppt.Primary_Order_Contact__c = new_contact.Id;
        Oppt.StageName = 'Quote Originated';
        insert oppt;
        
    }
   Static TestMethod Void CreateContactTestMethod(){
        
        Account acc = [Select Id,Name,ParentId from Account where Name =: 'Test Account']; 
        Contact con = [Select id,Email from Contact Where email =: 'TestEmail@Test.com'];        
        
        smb_newQuoteFromAccountController newQuote = new smb_newQuoteFromAccountController();
        ApexPages.currentPage().getParameters().put('aid', acc.Id);
        ApexPages.currentPage().getParameters().put('isammend','true');
        
       Opportunity opp = [Select id,Name from Opportunity where StageName =: 'Quote Originated'];
         ApexPages.currentPage().getParameters().put('oppId',opp.Id);
        
        newQuote.selectedValue='NULL';
        newQuote.searchTerm='Test John';
        newQuote.selectedOrderType = 'NULL';      	
        newQuote.oppName='';
        newQuote.errorMessage='';        
        newQuote.selectedContactId = con.Id; 
        newQuote.Order_Type = 'null';
       
        newQuote.populateOrderType();
        newQuote.getRelated_Order();
        newQuote.getTypes();
        newQuote.onLoad();
        newQuote.toggleNewContactForm();
        newQuote.onSearch();
        newQuote.onSelectContact();       
    }
    Static TestMethod Void TestMethodWithComplex()
    {
        Account acc = [Select Id,Name,ParentId from Account where Name =: 'Test Account'];
        Contact con = [Select id,Email from Contact Where email =: 'TestEmail@Test.com'];

        smb_newQuoteFromAccountController newQuote = new smb_newQuoteFromAccountController();
        ApexPages.currentPage().getParameters().put('aid', acc.Id); 
        ApexPages.currentPage().getParameters().put('isammend','true');
        
        Opportunity opp = [Select id,Name from Opportunity where StageName =: 'Quote Originated'];
         ApexPages.currentPage().getParameters().put('oppId',opp.Id);
        newQuote.selectedValue='Complex';
        newQuote.selectedOrderType='Change';
        newQuote.selectedContactId = con.Id;        
        newQuote.new_contact=con;
        newQuote.oppName='Test Opportunity';
        newQuote.isAmmend = true;
        newQuote.onLoad();
        newQuote.Order_Type = NULL;
        newQuote.populateOrderType();       
        newQuote.onSelectContact();
            
    }
    Static TestMethod Void TestMethodWithComplexNull()
    {
        Account acc = [Select Id,Name,ParentId from Account where Name =: 'Test Account'];
        Contact con = [Select id,Email from Contact Where email =: 'TestEmail@Test.com'];

        smb_newQuoteFromAccountController newQuote = new smb_newQuoteFromAccountController();
        ApexPages.currentPage().getParameters().put('aid', acc.Id); 
        ApexPages.currentPage().getParameters().put('isammend','true');
        
        //Opportunity opp = [Select id,Name from Opportunity where StageName =: 'Quote Originated'];
         ApexPages.currentPage().getParameters().put('oppId',Null);
        newQuote.selectedValue='Complex';
        newQuote.selectedOrderType=Null;
        newQuote.selectedContactId = con.Id;        
        newQuote.new_contact=con;
       // newQuote.oppName='Test Opportunity';
        newQuote.isAmmend = true;
        newQuote.onLoad();
        newQuote.populateOrderType();       
        newQuote.onSelectContact();
            
    }
    
    Static TestMethod Void TestMethodWithSimpleOrderType()
    {
        Account acc = [Select Id,Name,ParentId from Account where Name =: 'Test Account'];
        Contact con = [Select id,Email from Contact Where email =: 'TestEmail@Test.com'];
        
        smb_newQuoteFromAccountController newQuote = new smb_newQuoteFromAccountController();
        newQuote.oppName = '';
        newQuote.getCPQaccessforUser();
        newQuote.selectedValue='Simple (Voice, HSIA, Bundles & Toll-Free)';
        newQuote.selectedOrderType='New Provide/Upgrade Order';
        newQuote.isAmmend = true;
        newQuote.oppName='Test Opportunity';
        ApexPages.currentPage().getParameters().put('aid', acc.Id);
        ApexPages.currentPage().getParameters().put('isammend','true');
        Opportunity opp = [Select id,Name from Opportunity where StageName =: 'Quote Originated'];
         ApexPages.currentPage().getParameters().put('oppId',opp.Id);
        newQuote.onLoad();        
        newQuote.new_contact=con;
        newQuote.selectedContactId=con.Id;
        newQuote.populateOrderType();
        newQuote.onSelectContact();            
    }
    
    Static TestMethod Void TestMethodSimpleOrderType()
    {
        Account acc = [Select Id,Name,ParentId from Account where Name =: 'Test Account'];
        Contact con = [Select id,Email from Contact Where email =: 'TestEmail@Test.com'];

        ApexPages.currentPage().getParameters().put('aid', acc.Id);
        ApexPages.currentPage().getParameters().put('isammend','true');
        
        Opportunity opp = [Select id,Name from Opportunity where StageName =: 'Quote Originated'];
        ApexPages.currentPage().getParameters().put('oppId',opp.Id);
        smb_newQuoteFromAccountController newQuote = new smb_newQuoteFromAccountController();
        newQuote.getCPQaccessforUser();
        newQuote.oppName='Test Opportunity';
        newQuote.selectedValue='Simple (Voice, HSIA, Bundles & Toll-Free)';
        newQuote.selectedOrderType='All Other Orders';
        newQuote.new_contact=con;
        newQuote.selectedContactId=con.Id;
        
        newQuote.onLoad();
        newQuote.populateOrderType();
        newQuote.onSelectContact();            
    }
    
    Static TestMethod Void TestMethodCreateNewContact()
    {
        Account acc = [Select Id,Name,ParentId from Account where Name =: 'Test Account'];
        
        Contact cont=new Contact();
        cont.AccountId =acc.Id;
        cont.FirstName = 'Test Fname11';
        cont.LastName='Test John11';
        cont.Email='TestEmail1234@Test.com';
        cont.Title='Test ContatcTitle1';
        cont.Phone='44444444';
        
        
        smb_newQuoteFromAccountController newQuote = new smb_newQuoteFromAccountController();
        ApexPages.currentPage().getParameters().put('aid', acc.Id);
        newQuote.onLoad();
        newQuote.new_contact = cont;
        ApexPages.StandardController sc = new ApexPages.StandardController(newQuote.new_contact); 
        newQuote.createNewContact();    
    }   
    Static TestMethod Void TestMethodWithOnselectContact()
    {
        Account acc = [Select Id,Name,ParentId from Account where Name =: 'Test Account'];
        Contact con = [Select id,Email from Contact Where email =: 'TestEmail@Test.com'];
        
        smb_newQuoteFromAccountController newQuote = new smb_newQuoteFromAccountController();
        ApexPages.currentPage().getParameters().put('aid', acc.Id);
        ApexPages.currentPage().getParameters().put('isammend','true');
        
        Opportunity opp = [Select id,Name from Opportunity where StageName =: 'Quote Originated'];
         ApexPages.currentPage().getParameters().put('oppId',opp.Id);
        
        newQuote.new_contact = con;
        newQuote.oppName='';
        newQuote.selectedValue='Complex';
        newQuote.selectedOrderType='Move';
        newQuote.selectedContactId = con.Id;
        newQuote.isAmmend = true;
        newQuote.onLoad();
        newQuote.onSelectContact();       
    } 
    
    Static TestMethod Void TestMethodWithSelectedOrderNULL()
    {
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Admin TEAM' LIMIT 1];
        
     	User usr = new User(LastName = 'LIVESTON',
                           FirstName='JASON',
                           Alias = 'jliv',
                           Email = 'jason.liveston@asdf.com',
                           Username = 'blah.blah@test1telusbcx.com',
                           ProfileId = profileId.id,
                           TimeZoneSidKey = 'GMT',
                           LanguageLocaleKey = 'en_US',
                           EmailEncodingKey = 'UTF-8',
                           LocaleSidKey = 'en_US',
                            CPQ_User_Access__c = true,
                            CPQ_Super_User_Access__c = true
                           );
        System.runAs(usr){ 
        Account acc = [Select Id,Name,ParentId from Account where Name =: 'Test Account'];
        Contact con = [Select id,Email from Contact Where email =: 'TestEmail@Test.com'];
        
        smb_newQuoteFromAccountController newQuote = new smb_newQuoteFromAccountController();
        ApexPages.currentPage().getParameters().put('aid', acc.Id);
        newQuote.getCPQaccessforUser();
        newQuote.selectedValue='Simple (Voice, HSIA, Bundles & Toll-Free)';
        newQuote.selectedOrderType='NULL';
        newQuote.onLoad();
        newQuote.new_contact=con;
        newQuote.selectedContactId=con.Id;
        newQuote.isAmmend=true;
        newQuote.getRelated_Order();
        newQuote.onSelectContact();      
    } 
    }
    Static TestMethod Void TestMethodWithSelectedNULL()
    {
        Account acc = [Select Id,Name,ParentId from Account where Name =: 'Test Account'];
        Contact con = [Select id,Email from Contact Where email =: 'TestEmail@Test.com'];
        
        smb_newQuoteFromAccountController newQuote = new smb_newQuoteFromAccountController();
        ApexPages.currentPage().getParameters().put('aid', acc.Id);
        newQuote.getCPQaccessforUser();
        newQuote.selectedValue='Simple (Voice, HSIA, Bundles & Toll-Free)';
        newQuote.selectedOrderType=NULL;
        newQuote.onLoad();
        newQuote.selectedContactId=Null;
        newQuote.isAmmend=true;
        newQuote.getRelated_Order();
        newQuote.onSelectContact();       
    } 
}