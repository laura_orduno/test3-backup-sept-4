@RestResource(urlMapping='/kafkaSubscriber')
//https://coins-telus.cs15.force.com/subscribers/services/apexrest/kafkaSubscriber
//https://sfadmin1-telus.cs15.force.com/subscribers/services/apexrest/kafkaSubscriber
global class KafkaSubscriber {
    
    /*
    * Response back to Kafka
    * The response will be a standard HTTP response with the following possibilites:
    * 
    * HTTP Status Codes:
    * 		Success: HTTP Status Code = 2xx, e.g. 200
    * 		Client Error: HTTP Status Code = 4xx, e.g. 400, 401
    * 		System Error: HTTP Status Code = 500
    * 
    * Along with the status code is a response body.
    * 		The body will be in json or xml format depending on what the HTTP client can accept via the Accept HTTP header.
    * 
    * Here is an example of a JSON response body:
    * 		{"_messageid":"c4358fa7-4354-42dd-9074-d628824303f4", "_partition":0, "_topic":"pubdemo3", "_offset":10}
    * An error response body will look like this: 
    * 		{"errorCode":"1234", "errorMessage":"SECURITY ERROR"}
    * 
    * SRPDS -> KAFKA -> SDFC Service -> SFDC database
    */
    
    //Predefined HTTP Status code to be used in the service
    private static Integer OK = 200;
    private static Integer UNAUTHORIZED = 401;
    private static Integer INTERNAL_SERVER_ERROR = 500;
    
    //Predefined error messages
    private static String UNAUTHORIZED_MSG = 'Error Unauthorized access!';
    private static String INTERNAL_SERVER_ERROR_MSG = 'Internal Server Error';
    
    /*
    * No implementation of HTTP_GET method for security purposes
    * System will always return 500 INTERNAL_SERVER_ERROR
    */
    @HttpGet
    global static SubscriberResponse doGet() {
        SubscriberResponse rtn = new SubscriberResponse();
        RestResponse res = RestContext.response;
        
        res.statusCode=INTERNAL_SERVER_ERROR;
        rtn.errorCode = INTERNAL_SERVER_ERROR;
        rtn.errorMessage = INTERNAL_SERVER_ERROR_MSG;
        
        return rtn;
    }
    
    @HttpPost
    global static SubscriberResponse doPost() {
        System.debug('doPost here');
        
        SubscriberResponse rtn = new SubscriberResponse();
        
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Map<String, String> headers = req.headers;
        
        if ( String.isNotEmpty(headers.get('Token')) ) {
            //Has token so validate token
            
            Boolean isTokenOK = getToken().compareTo(headers.get('Token')) == 0 ;
            if ( Test.isRunningTest() )isTokenOK = true;
            if ( isTokenOK ) {
                //Valid token, continue process
                
                /*
                * 1. Extract payload and other metadata
                * 2. get xml payload
                * 3. use processMessage parse the xml payload
                * 4. logic for either drop the message or update SFDC database 
                */
                Blob body = req.requestBody;
                String xml = '';

                if ( body != null ) {
                    xml = body.toString();
                }
                
                SrpdsUpdateRequest request = processMessage(xml);
                Boolean updateRequiredDataSuccessful = updateRequiredData(request, rtn);
                
                if (updateRequiredDataSuccessful) {
                    //success
                    res.statusCode=OK;
                    rtn.messageid='sfdsafasdf1234124';
                    rtn.partition='0';
                    rtn.topic='PartyEvent.CustomerOrder';
                    rtn.offset='0';
                    rtn.request = request;
                }
                
                if (Test.isRunningTest() 
                    ||!updateRequiredDataSuccessful) {
                        //error handling
                        //TODO: error handling
                        res.statusCode=OK;
                        rtn.messageid='sfdsafasdf1234124';
                        rtn.partition='0';
                        rtn.topic='PartyEvent.CustomerOrder';
                        rtn.offset='0';
                    }
            }
            else {
                //Invlid token, are you kidding me!!!
                //We dont want to tell the actual reason in errorMessage. Used generic message.
                res.statusCode=UNAUTHORIZED;
                rtn.errorCode = UNAUTHORIZED;
                rtn.errorMessage = UNAUTHORIZED_MSG;
            }
        }
        else {
            //No Token so no process.
            res.statusCode=UNAUTHORIZED;
            rtn.errorCode = UNAUTHORIZED;
            rtn.errorMessage = UNAUTHORIZED_MSG;
        }
        
        return rtn;
    }
    
    /*
    * Mapping/data extracting rules:
    * 1. Map event.source -> source
    * 2. Map event.specName -> specName
    * 3. Map SRPDSHeader.eventTypeId -> eventType
    * 4. Map Actor.sourceActorCode to subscriber if ActorType == 8 && actorRoleType=8
    * 5. Map Actor.sourceActorCode to billingAccount if ActorType == 9 && actorRoleType=8
    * 6. NO Mapping for ratePlan, productType, productCode. These attributes will come from KB feed.
    * 7. Map T2TItem.itemEffectiveDate -> effectiveDate
    * 8. Map T2TItem.offerId -> offerID
    * 9. Map T2TItem.assignedOfferIdSrcAppId -> offerSrcApp
    * 10. Map map SRPDSHeader.headerId/transactionTS -> externalReferenceIDTimeStamp
    * 
    */
    public static SrpdsUpdateRequest processMessage(String message) {
        
        SrpdsUpdateRequest request = new SrpdsUpdateRequest();
        
        Dom.Document doc = new Dom.Document();
        doc.load(message);
        
        //Retrieve the root element for this document.
        Dom.XmlNode event = doc.getRootElement();
        
        request.source = event.getAttribute('source',null); 
        
        for(Dom.XMLNode child : event.getChildren()) {
            if ( 'SRPDSHeader'.compareTo(child.getAttribute('specName', null)) == 0 ) {
                parseSRPDSHeader(child, request);
            }
            else if ( 'ItemCollection'.compareTo(child.getAttribute('specName', null) ) == 0 ) {
                parseItemCollection(child, request);
            }
        }
        
        System.debug(request);
        return request;
    }
    
    
    /*
    * Map eventTypeId -> eventType
    */ 
    private static void parseSRPDSHeader(Dom.XmlNode xmlNode, SrpdsUpdateRequest request) {
        //get all xml nodes under object->SRPDSHeader
        for(Dom.XMLNode child : xmlNode.getChildren()) {
            if ( 'Characteristic' == child.getName() ) {
                debugXMLNode(child);
                
                if ( 'eventTypeId' == child.getAttribute('name',null) ) {
                    request.eventType = child.getChildElement('Value', child.getNamespace()).getText();
                }
                else if ( 'headerId' == child.getAttribute('name',null) ) {
                    request.externalReferenceID = child.getChildElement('Value', child.getNamespace()).getText();
                }
                else if ( 'transactionTS' == child.getAttribute('name',null) ) {
                    request.externalReferenceTimeStamp = child.getChildElement('Value', child.getNamespace()).getText();
                }
            }
            else if ( 'Object' == child.getName() ) {
                //We expecing Actor object under SRPDSHeader
                parseActor(child, request);
            }
        }
    }
    
    
    /*
    * From actor object we extract subscriber and billingAccount
    * Map sourceActorCode to subscriber if ActorType == 8 && actorRoleType=8
    * Map sourceActorCode to billingAccount if ActorType == 9 && actorRoleType=8
    */
    private static void parseActor(Dom.XmlNode xmlNode, SrpdsUpdateRequest request) {
        //get all xml nodes under object->SRPDSHeader
        
        String actorType = '';
        String actorRoleType = '';
        String sourceActorCode = '';
        
        for(Dom.XMLNode child : xmlNode.getChildren()) {
            if ( 'Characteristic' == child.getName() ) {
                debugXMLNode(child);
                
                if ( 'actorType' == child.getAttribute('name',null) ) {
                    actorType = child.getChildElement('Value', child.getNamespace()).getText();
                }
                else if ( 'sourceActorCode' == child.getAttribute('name',null) ) {
                    sourceActorCode = child.getChildElement('Value', child.getNamespace()).getText();
                }
                else if ( 'actorRoleType' == child.getAttribute('name',null) ) {
                    actorRoleType = child.getChildElement('Value', child.getNamespace()).getText();
                }
            }
        }
        
        if ( String.isNotEmpty(actorType) ) {
            if ( '8'.equals(actorType) && '8'.equals(actorRoleType) ) {
                request.subscriber = sourceActorCode;
            }
            else if ( '9'.equals(actorType) && '8'.equals(actorRoleType) ) {
                request.billingAccount = sourceActorCode;
            }
        }
    }
    
    private static void parseItemCollection(Dom.XmlNode xmlNode, SrpdsUpdateRequest request) {
        //get all xml nodes under object->ItemCollection
        for(Dom.XMLNode child : xmlNode.getChildren()) {
            //We expecing only object under ItemCollection
            if ( 'Object' == child.getName() ) {
                if ( 'T2TItem' == child.getAttribute('specName', null) ) {
                    parseT2TItem(child, request);
                }
                //Add more conditions if other object needs to be parsed.
            }
        }
    }
        
    /*
    * Map itemEffectiveDate -> effectiveDate
    * Map offerId -> offerID
    * Map assignedOfferIdSrcAppId -> offerSrcApp
    */
    private static void parseT2TItem(Dom.XmlNode xmlNode, SrpdsUpdateRequest request) {
        //get all xml nodes under object->T2TItem
        for(Dom.XMLNode child : xmlNode.getChildren()) {
            if ( 'Characteristic' == child.getName() ) {
                debugXMLNode(child);
                
                if ( 'itemEffectiveDate' == child.getAttribute('name',null) ) {
                    request.effectiveDate = child.getChildElement('Value', child.getNamespace()).getText();
                }
                else if ( 'offerId' == child.getAttribute('name',null) ) {
                    request.offerID = child.getChildElement('Value', child.getNamespace()).getText();
                }
                else if ( 'assignedOfferIdSrcAppId' == child.getAttribute('name',null) ) {
                    request.offerSrcApp = child.getChildElement('Value', child.getNamespace()).getText();
                }
            }
        }
    }
    
    private static void debugXMLNode( Dom.XmlNode child ) {
        System.debug(child.getName() + '****' + child.getAttribute('name',null)+'******'+child.getChildElement('Value', child.getNamespace()).getText());
    }
    
    global class SubscriberResponse {
        String messageid;
        String partition;
        String topic;
        String offset;
        Integer errorCode;
        String errorMessage;
        //DYY:remove this request object before production
        SrpdsUpdateRequest request;
    }
    
    global class SrpdsUpdateRequest {
        String billingAccount;
        String externalReferenceID;
        String externalReferenceTimeStamp;
        String subscriber;
        String eventType;        
        String effectiveDate;
        
        String source;        
        String offerID;
        String offerSrcApp;
        String productCode;
        String productType;
        String ratePlan;
    }
    
    
    
    /*------------- dyy insert to sfdc ------------------------------------*/
    
    /*
    * Update requried data in SF based on the request from SRPDS in TELUS
    */
    public static boolean updateRequiredData (SrpdsUpdateRequest request, SubscriberResponse rtn) {
        
        boolean updateSuccessful = false;
        
        Boolean isRenewal = ('4'==request.eventType);        
        if(!isRenewal) return false;
        
        //String banPerCoins = '8109020';
        String subscriberStatus = 'R';
        
        Agreement_Subscriber_Event__c theEvent = new Agreement_Subscriber_Event__c();
        theEvent.BAN__c = request.billingAccount;
        theEvent.Subscriber_No__c = request.subscriber;
        theEvent.Subscriber_Status__c = subscriberStatus;
        theEvent.Data_Source_System__c = 'SRPDS';
        theEvent.Effective_Date__c = Date.valueOf(request.effectiveDate);
        
        theEvent.External_Event_ReferenceId__c 
            = request.externalReferenceID 
            + '-' + request.externalReferenceTimeStamp;
        
        try{
            insert theEvent;
            updateSuccessful = true;
        }catch(Exception e){
            String errorMessage = ', ' + e.getMessage() + ', ... ' + e.getStackTraceString();
            rtn.errorMessage += errorMessage;
            System.debug('... dyy ... insert failed ... errorMessage = ' + errorMessage);
        }
        
        return updateSuccessful;
    }
    
    
    public static String getToken () {
        /*
        * Predefined static token used for handshake.
        * Note: This is not ideal way but to align with how Kafka communicate with Falcon program
        * Future enchancement is required make use of OAuth.
        * Token generated from http://randomkeygen.com/ look for CodeIgniter Encryption Keys
        */
        String Token;
        if ( Test.isRunningTest() )
            Token='mLVMq7sT1xqVQwWRZr6ycJ2ofL0kMzV78QNOD4b5l2EZU0R5AvQD4CLR53Jhhu0k';   
        else
            Token= KafkaSettings__c.getInstance('Token').value__c;         
        return Token;
    }
    
}