/*
    Cloned from trac_New_Opp_From_Account_Controller
    
    Updated to provide New Case functionality from Account and to query all Contacts up/down the Account Hierarchy
*/
public with sharing class smb_New_Case_From_Account_Controller {
    public class NewCaseException extends Exception {}

    public Boolean loaded {get; private set;}
    public Account account {get; private set;}
    public Contact[] contacts {get; private set;}
    public Integer contactsCount {get {if(contacts == null){return 0;} return contacts.size();}}
    
    /* Input Variables */
    public String searchTerm {get; set;}
    public Id selectedContactId {get; set;}
    
    public Boolean showNewContactForm {get; private set;}
    public Contact new_contact {get; set;}
    public Id mostRecentlyCreatedContactId {get; private set;}

	//*********** TBO US # 146 - TBO related changes ***********		    public smb_New_Case_From_Account_Controller() {}
	public String TBOCustomer {get; set;}		
	public List<RecordType> caseRecTypeList {get; set;}		
	public Boolean displayTBO {get; set;}		
				
	public smb_New_Case_From_Account_Controller() {		
		displayTBO = false;		
		TBOCustomer = null;		
		caseRecTypeList = [Select id, Name, DeveloperName, SobjectType from RecordType where SobjectType = 'Case' order by Name];		
	}		
			
	public PageReference showTBORadio(){		
		system.debug('----------showTBORadio---selectedCaseRecordType---' + selectedCaseRecordType);		
		system.debug('----------showTBORadio---start---displayTBO---' + displayTBO);		
		if(selectedCaseRecordType != null && caseRecTypeList != null){		
			for(RecordType caseRecType : caseRecTypeList){		
				system.debug('----------showTBORadio---caseRecType.id---' + caseRecType.id);		
				if(selectedCaseRecordType == caseRecType.id){		
					if(caseRecType.DeveloperName.equalsIgnoreCase('TBO_Request'))		
						displayTBO = true;		
					else		
						displayTBO = false;		
				}		
			}		
		}else{		
			displayTBO = false;		
		}		
		system.debug('----------showTBORadio---end---displayTBO---' + displayTBO);		
		return null;		
	}		
			
	//*********** END of TBO US # 146 - TBO related changes ***********		
	    
    public String selectedCaseRecordType {get; set;}
    
    public Boolean flashContact {get;set;}
    
    public Id flashContactId {get;set;}
    
    // used for custom exception messages
    public String msgTitle {get; set;}
    public String msgSummary {get; set;}
    public Boolean msgRendered {get; set;}
    public String msgDetail {get; set;}
    public Integer msgStrength {get; set;}
    public String msgSeverity {get; set;}
    
    public List<SelectOption> caseRecordTypes {
        get {
            if (caseRecordTypes != null) return caseRecordTypes;
        
            // Use dynamic Apex to generate a select list of Record Ids + Name
            Map<Id,Schema.RecordTypeInfo> caseRTMapById = Schema.SObjectType.Case.getRecordTypeInfosById();

            caseRecordTypes = new List<SelectOption>();
            caseRecordTypes.add(new SelectOption('CHOOSEAGAIN', Label.Case_Choose_Record_type_Default));
                
            // Format the select list in case record type name order
            //for (RecordType recType : [Select id, Name, DeveloperName, SobjectType from RecordType where SobjectType = 'Case' order by Name]) {
            for (RecordType recType : caseRecTypeList){
                // Get the schema info for the record type using the record type id
                Schema.RecordTypeInfo caseRT = caseRTMapById.get(recType.id);

                // the record type is available to the logged in user
                // Exclude the Master record type as it shouldn't be visible
                if (caseRT.isAvailable() && caseRT.getName() != 'Master') { 
                    caseRecordTypes.add(new SelectOption(caseRT.getRecordTypeId(), caseRT.getName()));
                }
            }
            return caseRecordTypes; 
        }
        private set;
    }
    
    public void onLoad() {
        flashContact = false;
        
        Savepoint sp = database.setSavepoint();
        try {
            loaded = false;
            showNewContactForm = false;
            if (ApexPages.currentPage() == null || ApexPages.currentPage().getParameters() == null) { clear(); return; }
            String aid = ApexPages.currentPage().getParameters().get('aid');
            if (aid == null || aid == '' || aid.substring(0, 3) != '001') { clear(); return; }
            Id xid = (Id) aid;
            account = [Select Name, ParentId, Owner.Name From Account Where Id = :xid Limit 1];
            if (account == null) { clear(); throw new NewCaseException('Unable to load account'); return; }

            loadContacts();
            if (contacts == null || contacts.size() == 0) { showNewContactForm = true; }
            
            new_contact = new Contact(AccountId = account.Id);

            loaded = true;
        } catch (Exception e) { clear(); Database.rollback(sp); QuoteUtilities.log(e); }
    }
    
    private void loadContacts() {
        List<Id> allAccountsInHierarchy = smb_AccountUtility.getAccountsInHierarchy(account.id, account.parentId);
        
        contacts = [Select Name, Title, Email, Active__c, Account.Name, Account.RecordType.Name From Contact Where AccountId in :allAccountsInHierarchy Order By Active__c DESC, CreatedDate DESC LIMIT 500];
    }
    
    public PageReference toggleNewContactForm() {
        try {
            showNewContactForm = !showNewContactForm;
        } catch (Exception e) { clear(); QuoteUtilities.log(e); }
        return null;
    }
    
    public void onSearch() {
        try {
            if (searchTerm == null || searchTerm == '') { loadContacts(); return; }
            //searchTerm = '*' + searchTerm + '*';
            List<List<sObject>> searchResult = [FIND :searchTerm IN ALL FIELDS RETURNING Contact (name, email, title, Active__c, Account.Name, Account.RecordType.Name Where AccountId = :account.Id)];
            contacts = ((List<Contact>)searchResult[0]);
        } catch (Exception e) { QuoteUtilities.log(e); } return;
    }
    
    
    public PageReference createNewContact() {
            
            //insert new_contact;
            //ApexPages.standardController sc = new ApexPages.standardController(new_contact);
            //sc.save();
            
            Database.Saveresult sr = Database.insert(new_contact, false);
            
            System.debug(sr);
            
            if (!sr.isSuccess()) {
                String exceptionString = '';
                
                for (Database.Error error : sr.getErrors()) {
                    exceptionString = error.getMessage();
                }
                
                
                if (exceptionString.contains('DupeBlocker: Duplicate found')) {
                    // caught a DupeBlocker exception
                    if (exceptionString.indexOf('dupes=/') != -1) {
                        String ContactId = exceptionString.substring(exceptionString.indexOf('dupes=/') + 7, exceptionString.indexOf('dupes=/') + 25);
                        System.debug('#################### ContactId Id for Dupeblocker :' + ContactId);
                        flashContact = true;
                        flashContactId = ContactId;
                        
                        throwCustomExceptionMsg('Duplicate contact found', 'The contact cannot be saved, because an existing contact with the same details was found in the database.  Please use this contact instead.', true, null, 2, 'Warning');
                        return null;
                    }
                }
                
                throwCustomExceptionMsg('Error Occured', 'An error occured while saving the contact', true, exceptionString, 3, 'Error');
            }
            if (new_contact.Id == null) { return null; }
            new_contact = [Select FirstName, LastName, Name, Email, Title, Phone From Contact Where Id = :new_contact.Id];
            if (new_contact.Id != null) {
                showNewContactForm = false;
                mostRecentlyCreatedContactId = new_contact.id;
                new_contact = new Contact(AccountId = account.id);
                flashContact = true;
                flashContactId = new_contact.Id;
                loadContacts();
            }
            return null;
    }
    
    public PageReference onSelectContact() {
        try {
            if (selectedContactId == null) { throw new NewCaseException(Label.Case_Valid_Contact_Error_Message); return null;}
            
            if (selectedCaseRecordType == 'CHOOSEAGAIN') { 
                throwExceptionMsg(Label.Case_Record_Type_Error_Message, 'warning');
                return null;
            }
            
            //*********** TBO US # 146 - TBO related changes ***********		
	            		
	            if(selectedCaseRecordType != null && caseRecTypeList != null){		
	                for(RecordType caseRecType : caseRecTypeList){		
	                    system.debug('----------showTBORadio---caseRecType.id---' + caseRecType.id);		
	                    if(selectedCaseRecordType == caseRecType.id){		
	                        if(caseRecType.DeveloperName.equalsIgnoreCase('TBO_Request') && displayTBO == true){		
	                            if(TBOCustomer == null || TBOCustomer == ''){		
	                                throwExceptionMsg(Label.TBO_CustomerTypeMandatory, 'warning');		
	                                return null;		
	                            }else{		
	                                Case TBOCase = new Case();		
	                                TBOCase.recordtypeid = selectedCaseRecordType;		
	                                TBOCase.Subject = 'TBO initated by ' + account.name;		
	                                TBOCase.Status = 'Draft';		
	                                TBOCase.Created_Date__c = Date.Today();		
	                                		
	                                if(TBOCustomer == 'outgoing'){		
	                                    TBOCase.AccountId = account.Id;		
	                                    TBOCase.ContactId = selectedContactId;		
	                                }else{		
	                                    TBOCase.Master_Account_Name__c = account.Id;		
	                                    TBOCase.Contact_phone_number__c = selectedContactId;		
	                                }		
	                                		
	                                insert TBOCase;		
	                                		
	                                String TBOCaseId = TBOCase.Id;		
	                                TBOCaseId = TBOCaseId.substring(0, 15);		
	                                PageReference TBOPage = new PageReference('/' + TBOCaseId + '/e');		
	                                TBOPage.getParameters().put('retURL', TBOCase.Id);		
	                                TBOPage.getParameters().put('ent', Case.sObjectType.getDescribe().getName());		
	                                //TBOPage.getParameters().put('RecordType', selectedCaseRecordType);		
	                                TBOPage.getParameters().put('isdtp','vw'); // for proper rendering in Service Cloud Console		
	                                TBOPage.setRedirect(true);		
	                                return TBOPage;		
	                            }		
	                        }                   		
	                    }		
	                }		
	            }		
	        		
	        //*********** END of TBO US # 146 - TBO related changes ***********		
	            
            
            PageReference pr = new PageReference('/' + Case.sObjectType.getDescribe().getKeyPrefix() + '/e');
            pr.getParameters().put('retURL', '%2F'+selectedContactId);
            pr.getParameters().put('def_account_id', account.Id);
            pr.getParameters().put('def_contact_id', selectedContactId);
            pr.getParameters().put('ent', Case.sObjectType.getDescribe().getName());
            pr.getParameters().put('RecordType', selectedCaseRecordType);
            pr.getParameters().put('isdtp','vw'); // for proper rendering in Service Cloud Console
            
            pr.setRedirect(true);
            return pr;
        } catch (Exception e) { QuoteUtilities.log(e); } return null;
    }
    
    public void clear() {
        account = null;
        contacts = null;
        new_contact = null;
    }
    
    /* Test Methods! */
    
    public void clearExceptionArea() {
        
        msgTitle = '';
        msgSummary = '';
        msgRendered = false;
        msgDetail = '';
        msgStrength = 1;
        msgSeverity = 'confirm';
    }
    
    public void throwCustomExceptionMsg(String msgTitle1, String msgSummary1, Boolean msgRendered1, String msgDetail1, Integer msgStrength1, String msgSeverity1) {
        
        msgTitle = msgTitle1;
        msgSummary = msgSummary1;
        msgRendered = msgRendered1;
        msgDetail = msgDetail1;
        msgStrength = msgStrength1;
        msgSeverity = msgSeverity1;
    }
    
    public void addETP(String e) {
        ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, e));
    }
    
    public void throwExceptionMsg(String message, String severity) {
            
        if (severity.equalsIgnoreCase('warning')) {     
            System.debug('Throwing this warning message : ' + message); 
            ApexPages.Message msg= new ApexPages.Message(ApexPages.Severity.WARNING, message, message);
            ApexPages.addmessage(msg);
        } else if (severity.equalsIgnoreCase('confirm')) {     
            System.debug('Throwing this confirm message : ' + message); 
            ApexPages.Message msg= new ApexPages.Message(ApexPages.Severity.CONFIRM, message, message);
            ApexPages.addmessage(msg);
        } else { //assume error
            System.debug('Throwing this error message : ' + message);   
            ApexPages.Message msg= new ApexPages.Message(ApexPages.Severity.ERROR, message, message);
            ApexPages.addmessage(msg);
        } 
    }  
    
/*   
    private static testMethod void testWithNoContacts() {
        trac_New_Opp_From_Account_Controller tno = new trac_New_Opp_From_Account_Controller();
        Account a = new Account(Name = 'This is a test account');
        insert a;
        ApexPages.currentPage().getParameters().clear();
        ApexPages.currentPage().getParameters().put('aid', a.id);
        tno.onLoad();
        system.assertEquals(true, tno.showNewContactForm);
        system.assertEquals(0, tno.contactsCount);
        system.assertEquals(null, tno.toggleNewContactForm());
        system.assertEquals(false, tno.showNewContactForm);
    }
    
    private static testMethod void testAddContact() {
        trac_New_Opp_From_Account_Controller tno = new trac_New_Opp_From_Account_Controller();
        Account a = new Account(Name = 'This is a test account');
        insert a;
        ApexPages.currentPage().getParameters().clear();
        ApexPages.currentPage().getParameters().put('aid', a.id);
        tno.onLoad();
        system.assertEquals(true, tno.showNewContactForm);
        system.assertEquals(0, tno.contactsCount);
        
        tno.new_contact.FirstName = 'Alex';
        tno.new_contact.LastName = 'Miller';
        tno.new_contact.Title = 'Developer';
        tno.new_contact.Phone = '6049969449';
        tno.new_contact.Email = 'amiller@tractionondemand.com';
        tno.createNewContact();
        system.assertNotEquals(0, tno.contactsCount);
        system.assertEquals(tno.contacts[0].Id, tno.mostRecentlyCreatedContactId);
    }
    
    private static testMethod void testWithContact() {
        trac_New_Opp_From_Account_Controller tno = new trac_New_Opp_From_Account_Controller();
        Account a = new Account(Name = 'This is a test account');
        insert a;
        Contact c = new Contact(FirstName = 'Alex', LastName = 'Miller', Email = 'amiller@tractionondemand.com', Phone = '6049969449', Title = 'Developer', AccountId = a.Id);
        insert c;
        ApexPages.currentPage().getParameters().clear();
        ApexPages.currentPage().getParameters().put('aid', a.id);
        tno.onLoad();
        system.assertEquals(false, tno.showNewContactForm);
        system.assertEquals(1, tno.contactsCount);
    }
    
    private static testMethod void testSearch() {
        // Really ought to be called 'pretendToTestSearch'
        trac_New_Opp_From_Account_Controller tno = new trac_New_Opp_From_Account_Controller();
        Account a = new Account(Name = 'This is a test account');
        insert a;
        Contact c = new Contact(FirstName = 'Alex', LastName = 'Miller', Email = 'amiller@tractionondemand.com', Phone = '6049969449', Title = 'Developer', AccountId = a.Id);
        insert c;
        ApexPages.currentPage().getParameters().clear();
        ApexPages.currentPage().getParameters().put('aid', a.id);
        tno.onLoad();
        tno.searchTerm = 'miller';
        tno.onSearch();
        // Remember boys and girls, no SOSL in test classes!
        // system.assertEquals(1, tno.contactsCount);
    }
    
    private static testMethod void testChooseContact() {
        trac_New_Opp_From_Account_Controller tno = new trac_New_Opp_From_Account_Controller();
        Account a = new Account(Name = 'This is a test account');
        insert a;
        Contact c = new Contact(FirstName = 'Alex', LastName = 'Miller', Email = 'amiller@tractionondemand.com', Phone = '6049969449', Title = 'Developer', AccountId = a.Id);
        insert c;
        ApexPages.currentPage().getParameters().clear();
        ApexPages.currentPage().getParameters().put('aid', a.id);
        tno.onLoad();
        system.assertEquals(1, tno.contactsCount);
        tno.selectedContactId = tno.contacts[0].Id;
        PageReference pr = new PageReference('/006/e');
        pr.getParameters().put('retURL', '%2F'+tno.contacts[0].Id);
        pr.getParameters().put('accid', a.Id);
        pr.getParameters().put('conid', tno.contacts[0].Id);
        pr.getParameters().put('ent', 'Opportunity');
        pr.setRedirect(true);
        system.assertequals(pr.getUrl(), tno.onSelectContact().getUrl());
    }
    
    private static testMethod void testToggleForm() {
        trac_New_Opp_From_Account_Controller tno = new trac_New_Opp_From_Account_Controller();
        Account a = new Account(Name = 'This is a test account');
        insert a;
        Contact c = new Contact(FirstName = 'Alex', LastName = 'Miller', Email = 'amiller@tractionondemand.com', Phone = '6049969449', Title = 'Developer', AccountId = a.Id);
        insert c;
        ApexPages.currentPage().getParameters().clear();
        ApexPages.currentPage().getParameters().put('aid', a.id);
        tno.onLoad();
        system.assertEquals(1, tno.contactsCount);
        tno.showNewContactForm = null;
        tno.toggleNewContactForm();
    }
    private static testMethod void testNoAccount() {
        trac_New_Opp_From_Account_Controller tno = new trac_New_Opp_From_Account_Controller();
        tno.onLoad();
        system.assertEquals(false, tno.loaded);
    }
    private static testMethod void testNonExistentAccount() {
        trac_New_Opp_From_Account_Controller tno = new trac_New_Opp_From_Account_Controller();
        ApexPages.currentPage().getParameters().put('aid', '0014000000by1yA');
        tno.onLoad();
        system.assertEquals(false, tno.loaded);
    } */
}