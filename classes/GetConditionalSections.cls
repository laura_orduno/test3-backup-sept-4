/**
@author Vlocity Implements GetItemsListInterface
@version May 20 2016
*/

global with sharing class GetConditionalSections implements vlocity_cmt.VlocityOpenInterface{
    private static string CONTRACTOBJ = 'Contracts__c';
    private static set <string> tableSections = new set<string>{ 'Voice Plan','Data Plan','Voice and Data Plan',
        'Voice Features',
        'Data Features',
        'Text Features',
        'Roaming Add On',
        'Roaming Pay Per Use'};
            
            public Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {
                Boolean success = true;
                
                if(methodName == 'getConditionalTemplateSections')
                { 
                    getConditionalSectionsBasedonServices(inputMap, outMap, options); 
                }
                
                return success; 
            }
    
    private void getConditionalSectionsBasedonServices (Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options){
        map<string,object> mapTableInfo=new map<string,object>();
        set <string> conditionSet = new set<string>(); 
        //string services;
        string content = '';
        set<string> sectionCondSet = new set<string>();
        map<id, vlocity_cmt__DocumentTemplateSection__c> sectionContentMap = new map<id,vlocity_cmt__DocumentTemplateSection__c>();
        map<id,id> sectionEmTempID = new map<id,id>();
        map<id,string> embeddedTempsections = new map<id,string>();
        set <string> allCustomSections = new set <string>();
        List<vlocity_cmt__DocumentTemplateSection__c> qualifiedSections = new List<vlocity_cmt__DocumentTemplateSection__c> ();
        
        Map<Id, vlocity_cmt__DocumentTemplateSection__c> sectionsMap = (Map<Id, vlocity_cmt__DocumentTemplateSection__c>) inputMap.get('allSections');
        Map<id,vlocity_cmt__DocumentTemplateSection__c> filteredSections = new Map<Id, vlocity_cmt__DocumentTemplateSection__c>();
        Id contextId = (Id) inputMap.get('contextId');
        contract conObj = new contract();
        Map<string, Object> qualifiedSectionsMap = new Map<string,Object>();
        
        List<SObject> items = new List<SObject> ();
        Id ContractRecordTypeId = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Corporate Wireless').getRecordTypeId();
        list<Contract_Section_Filter__c> lstContractMapping=Contract_Section_Filter__c.getall().values(); 
        
        for (contract con: [select id, name, Services__c,recordTypeID,Hardware_Refresh_Type__c, Fulfillment_Type__c,Churn_deactivation_Allowance__c,Discounts__c,
                            Employee_Purchase_Plan_Requested__c,Offer_House_Demand__r.Loyalty_Credit__c ,Offer_House_Demand__r.Churn_deactivation_Allowance__c,	Contract_Type__c,
                            (select id,name, Activation_Credit__c , vlocity_cmt__ContractId__r.Contract_Type__c,Conversion_Credit__c , Renewal_Credit__c ,Hardware_Discounts__c,Plan_Type__c from vlocity_cmt__ContractLineItems__r )  
                            from contract where id =:contextId limit 1]){
                                conObj = con;
                                if(con.Services__c != null && con.Services__c != ''){
                                    conditionSet.addall(con.Services__c.split(';'));
                                    
                                }
                                for(vlocity_cmt__ContractLineItem__c cli: con.vlocity_cmt__ContractLineItems__r ){
                                    if(cli.vlocity_cmt__ContractId__r.Contract_Type__c == 'Amendment'){
                                        conditionSet.add(Label.CP_Exhibit_1_Rate_Plans); 
                                    }
                                    if( cli.Activation_Credit__c != null || cli.Conversion_Credit__c != null ||cli.Renewal_Credit__c != null ){
                                        conditionSet.add(Label.CP_AIRTIME_CREDITS);
                                        conditionSet.add(Label.CP_RETURN_OF_AIRTIME_CREDITS);
                                        
                                        break;
                                    }
                                }
                                for(vlocity_cmt__ContractLineItem__c cli: con.vlocity_cmt__ContractLineItems__r ){
                                    if( cli.Hardware_Discounts__c != null && cli.Hardware_Discounts__c != '$0' && cli.Hardware_Discounts__c != '0' && cli.Hardware_Discounts__c != '0%' && cli.Hardware_Discounts__c != ''){
                                        conditionSet.add(Label.CP_PROMOTIONAL_HARDWARE_PRICING);
                                        break;
                                    }
                                }
                                for(vlocity_cmt__ContractLineItem__c cli: con.vlocity_cmt__ContractLineItems__r ){
                                    string planType = string.valueof(cli.Plan_Type__c);
                                    //system.debug('PlanType::' + planType +'::::: ' + tableSections.contains(planType) );
                                    if( cli.Plan_Type__c != null && tableSections.contains(planType) ){
                                        conditionSet.add(planType);
                                       
                                    }
                                }
                                
                                if(con.Hardware_Refresh_Type__c != null && con.Hardware_Refresh_Type__c != ''){
                                    conditionSet.addall(con.Hardware_Refresh_Type__c.split(';'));
                                }
                                
                                conditionSet.addall(getSectionNames(lstContractMapping,con));           
                            }
        
        if( conObj != null && (conObj.recordTypeID != ContractRecordTypeId  || ( conditionSet.size() == 0 || conditionSet.isEmpty()))) {
            qualifiedSectionsMap =  DefaultConditionalTemplateSections.getTempSectionsBasedonProdcts(inputMap,outMap,options );
            outMap.putAll(qualifiedSectionsMap);
        }
        else {
            system.debug('****conditionSet***'+conditionSet);
            Map<string,Contract_Template_Section_Mapping__c> objNameMap =  Contract_Template_Section_Mapping__c.getAll();
            
            for ( Contract_Template_Section_Mapping__c servCon : objNameMap.values() ){
                allCustomSections.add(servCon.Template_Section_Name__c );
            }
            
            for (string conds:  conditionSet){
                if(objNameMap.size() > 0 && !objNameMap.isEmpty())
                    if(objNameMap.containsKey(conds) ){
                        string sCondition =  objNameMap.get(conds).Template_Section_Name__c;
                        sectionCondSet.add(sCondition); 
                    }
                if(conds.equalsIgnoreCase(Label.CP_AIRTIME_CREDITS) || conds.equalsIgnoreCase(Label.CP_RETURN_OF_AIRTIME_CREDITS) || conds.equalsIgnoreCase(Label.CP_Telus_Investments) || conds.equalsIgnoreCase(Label.CP_Return_of_TELUS_Investments)  ){
                    if(objNameMap.containsKey(Label.CP_Credits) ){
                        string sCondition =  objNameMap.get(Label.CP_Credits).Template_Section_Name__c;
                        sectionCondSet.add(sCondition); 
                    }
                }
            }
            allCustomSections.removeAll(sectionCondSet);       
            
            system.debug('::sectionCondSet:: ' + sectionCondSet + '::All CustomSection::' + allCustomSections );
            system.debug('sectionsMapsize'+ sectionsMap.size());
            
            set <id> sectionsToExclude = new set <id>();
            if(sectionsMap.size() > 0 && !sectionsMap.isEmpty() ){
                
                
                for (id tempSectionId:  sectionsMap.keyset() ){
                    string sectionName = sectionsMap.get(tempSectionId).name;
                    
                    for ( string srvcCondToExclude : allCustomSections){
                        if(sectionName.startsWithIgnoreCase(srvcCondToExclude)){
                            system.debug('Excluded Sections:  ' + sectionName );
                            sectionsToExclude.add(tempSectionId);                               
                        }    
                    }
                    
                }
                for (id tempSectionId:  sectionsMap.keyset() ){
                    string sectionName = sectionsMap.get(tempSectionId).name;                             
                    if(!sectionsToExclude.contains(tempSectionId)){                       
                        filteredSections.put(tempSectionId, sectionsMap.get(tempSectionId));
                    }    
                }
                
            }
            
            system.debug('Service excluded section Size: ' + filteredSections.size());
            if( filteredSections.size() > 0 && !filteredSections.isEmpty() ) {
                inputMap.put('allSections',filteredSections);
                qualifiedSectionsMap =  DefaultConditionalTemplateSections.getTempSectionsBasedonProdcts(inputMap,outMap,options );
                outMap.putAll(qualifiedSectionsMap);
            } 
            
        }
    }
    private list<string> getSectionNames(list<Contract_Section_Filter__c> lstContractSectionFilter,Sobject objContract){
        list<string> lstSectionName=new list<string>();
        map<string, string> multiConditionSections = new map<string, string>();
        map<string, string> mutliConSecToRemove = new map<string, string> ();
        for(Contract_Section_Filter__c objContactMapping : lstContractSectionFilter){
            string sSourceField=objContactMapping.Field_API_Name__c;
            string sFieldValue='';
            if(sSourceField.contains('__r')){
                list<string> objTempName = sSourceField.split('\\.');
                if(objTempName != null && objTempName.size()>0){
                    if(objTempName.size()==4 && objContract.getSobject(objTempName[0])!=null && objContract.getSobject(objTempName[0]).getSobject(objTempName[1])!=null &&  objContract.getSobject(objTempName[0]).getSobject(objTempName[1]).getSobject(objTempName[2])!=null &&  objContract.getSobject(objTempName[0]).getSobject(objTempName[1]).getSobject(objTempName[2]).get(objTempName[3])!=null && objContract.getSobject(objTempName[0]).getSobject(objTempName[1]).getSobject(objTempName[2]).get(objTempName[3])!='')
                    {
                        sFieldValue=string.valueof(objContract.getSobject(objTempName[0]).getSobject(objTempName[1]).getSobject(objTempName[2]).get(objTempName[3]));
                    }
                    else if(objTempName.size()==3 && objContract.getSobject(objTempName[0]) != null && objContract.getSobject(objTempName[0]).getSobject(objTempName[1]) != null && objContract.getSobject(objTempName[0]).getSobject(objTempName[1]).get(objTempName[2]) != null && objContract.getSobject(objTempName[0]).getSobject(objTempName[1]).get(objTempName[2]) != '')
                    {
                        sFieldValue=string.valueof(objContract.getSobject(objTempName[0]).getSobject(objTempName[1]).get(objTempName[2]));
                    }
                    else if(objTempName.size()==2 && objContract.getSobject(objTempName[0]) != null && objContract.getSobject(objTempName[0]).get(objTempName[1]) != null && objContract.getSobject(objTempName[0]).get(objTempName[1]) != '')
                    {
                        sFieldValue=string.valueof(objContract.getSobject(objTempName[0]).get(objTempName[1]));
                    }
                }
            }
            else{
                sFieldValue=string.valueof(objContract.get(objContactMapping.Field_API_Name__c));
            }
            if(sFieldValue != null && objContactMapping.Filter_Operator__c == '=' && sFieldValue.equalsignorecase(objContactMapping.Filter_Value__c) ){
                lstSectionName.add(objContactMapping.Section_Name__c);
            }
            if(sFieldValue != null && sFieldValue != '' && (objContactMapping.Filter_Operator__c == '!=' || objContactMapping.Filter_Operator__c == '<>') && sFieldValue != objContactMapping.Filter_Value__c){
                
                if(!mutliConSecToRemove.containskey(sSourceField+sSourceField+objContactMapping.Section_Name__c) &&  objContactMapping.Section_Name__c != null  ){
                    system.debug(':: objContactMapping.Section_Name__c :: ' + objContactMapping.Section_Name__c);
                    multiConditionSections.put(sSourceField+objContactMapping.Section_Name__c ,objContactMapping.Section_Name__c );
                }
                
            }
            else if(objContactMapping.Section_Name__c != null) {
                
                mutliConSecToRemove.put(sSourceField+objContactMapping.Section_Name__c,objContactMapping.Section_Name__c);
            }
            
        }
        
        for (string FieldName: multiConditionSections.keyset()){
            string sectionName = multiConditionSections.get(FieldName);
            if(!mutliConSecToRemove.containsKey(FieldName) ){
                lstSectionName.add(sectionName); 
            }
           
            else {
                system.debug('Remove section name' + sectionName );
            }
        }
        
        system.debug('****'+lstSectionName);
        return lstSectionName;
    }
}