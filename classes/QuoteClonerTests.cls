@isTest
private class QuoteClonerTests {
	private static Web_Account__c account;
    private static SBQQ__Quote__c quote;
	
    static testMethod void testSimpleQuote() {
        setUp();
        
        QuoteCloner cloner = new QuoteCloner(quote.id);
        cloner.doClone();
        
    }
    
    @isTest
    private static void setUp() {
        account = new Web_Account__c(Name='UnitTest');
        insert account;
        
        Address__c installLocation = new Address__c(Web_Account__c=account.Id,Special_Location__c='Test');
        installLocation.Rate_Band__c = 'F';
        installLocation.State_Province__c = 'BC';
		insert installLocation;
        
        Product2 p = new Product2(Name='Test',SBQQ__ConfigurationFields__c='Service_Term__c');
        insert p;
        
        Opportunity opp = new Opportunity(Name='Test',Web_Account__c=account.Id,CloseDate=System.today(),StageName='Prospecting');
        insert opp;
        
        quote = new SBQQ__Quote__c(SBQQ__Opportunity__c=opp.Id);
        quote.Rate_Band__c = 'F';
        quote.Province__c = 'BC';
        quote.Install_Location__c = installLocation.Id;
        insert quote;
        
        SBQQ__QuoteLine__c line = new SBQQ__QuoteLine__c(SBQQ__Quote__c=quote.Id,SBQQ__Bundle__c=true,SBQQ__Product__c=p.Id);
        insert line;
    }
    
}