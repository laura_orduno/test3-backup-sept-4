/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class smb_Test_AccountHierarchyController {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
       //PageReference ref = new PageReference(); 
       //Test.setCurrentPage(ref);     	
    	
	   RecordType recordType = [Select r.Id, r.DeveloperName From RecordType r where r.SobjectType = 'Account' and r.DeveloperName='RCID' limit 1];
	        
	   Account acc1 = new Account(Name='Testing Software',CustProfId__c= '31722403', BillingCountry = 'US', BillingState = 'IL', recordTypeId=recordType.Id , No_Chronic_Incidents__c =10.0);
	   insert acc1;

	   Account acc2 = new Account(Name='Testing Software', parentid=acc1.id, CustProfId__c= '31722403', BillingCountry = 'US', BillingState = 'IL', recordTypeId=recordType.Id , No_Chronic_Incidents__c =10.0);
	   insert acc2;	   
	   
	   Contact cont = new Contact(Lastname= 'lastname', Account = acc1);
	   insert cont;
	   
	   smb_AccountHierarchyController smb = new smb_AccountHierarchyController();
	   smb.accountId = acc2.id;
	   smb.ultimateParentId = acc1.id;
	   smb.SelectedAccount = acc1;
	 
	   //smb.selectedAccountNumber;
	   system.debug('**** acc1 '+ acc1);
	   system.debug('**** smb.SelectedAccount '+ smb.SelectedAccount);	   
	   smb.getTreeHTML(); 
	   
	   smb_InvalidParameterException smbInvPar = new smb_InvalidParameterException();
	   smb_ParameterNotSpecifiedException smbParNotSpecEx = new smb_ParameterNotSpecifiedException();
	  
    }
}