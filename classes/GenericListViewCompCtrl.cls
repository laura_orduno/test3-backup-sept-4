public virtual class GenericListViewCompCtrl {
    
    public String relFieldName {get;set;}
    
    public String ColFields {get;set;}
    
    public String ObjName {get;set;}
    
    public String RecId {get;set;}
    
    public String returnURL {get;set;}
    
    public String FFields {get;set;}
    
    public Integer pageSize {get;set;}
    
    public GenericPagination paginator {get;set;}
    
    public List<String> filter {get;set;}
    
    public GenericListViewCompCtrl(){
        System.debug('ContructorGeneric');
   		returnURL = 'retURL=' + EncodingUtil.urlEncode(ApexPages.currentPage().getURL(), 'UTF-8');
    }
     
    public List<SObject> getRecords(){
        System.debug(LoggingLevel.Error,'getRecords: ObjName :' + ObjName + ' ' + 'ColFields :' + ColFields + ' filter :' + filter );            
        if(paginator==null){
            System.debug('CheckFilter');
            if(filter ==null){
                System.debug('Filter: ' + filter + '; rel: ' + relFieldName + '; recId: ' + RecId);
                
            	filter = new List<String>{relFieldName + '=\''+ RecId +'\'' };
            }
            System.debug('Filter: ' + filter + '; rel: ' + relFieldName + '; recId: ' + RecId);
            List<Sobject> items = new List<Sobject>();
            System.debug('getting query');
            String query = Util.queryBuilder(ObjName, Util.getFieldSetFields(ObjName, ColFields), filter);
            System.debug(LoggingLevel.Error,'query :' + query);
            if(pageSize==null){
                pageSize=10;
            }
            try{
          	 paginator = new GenericPagination(query,pageSize); 
            }catch(Exception e){
                 System.debug(LoggingLevel.ERROR, e);
            }
        }
        System.debug('getRecords ' + paginator);
        List<SObject> objs = new List<Sobject>();
        if(paginator != null && paginator.getRecords() != null){
            objs.addAll(paginator.getRecords());
        }
        
        return objs;
    }
    
    public List<Field> getFieldsList(){
        List<Field> fields = new List<Field>();
        for(String f :Util.getFieldSetFields(ObjName, ColFields)){
            fields.add(new Field(f,Util.getFieldDescribe(ObjName,f).isSortable()));
        }
        return fields; 
    }
   
    public class Field{
        public String name {get;set;}
        public boolean sortable {get;set;}
        public Field(String name, boolean sortable){
            this.name= name;
            this.sortable = sortable;
        }
    }
    
    public boolean getShowEdit(){
        return Util.getObjectTypeDescribe(ObjName).isUpdateable();
    }
  
}