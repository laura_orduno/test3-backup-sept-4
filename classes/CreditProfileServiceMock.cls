@isTest
global class CreditProfileServiceMock implements WebServiceMock{
    
    global void doInvoke(
        Object stub
        , Object request
        , Map<String
        , Object> response
        , String endpoint
        , String soapAction
        , String requestName
        , String responseNS
        , String responseName
        , String responseType){
            if ( 'ping'.equalsIgnoreCase(requestName) ){
                response.put('response_x', ping());
                
            }else if ( 'addBusinessPartner'.equalsIgnoreCase(requestName)){
                response.put('response_x', addBusinessPartner());
                
            }else if ( 'addIndividualPartner'.equalsIgnoreCase(requestName)){
                response.put('response_x', addIndividualPartner());
                
            }else if ( 'attachCreditReport'.equalsIgnoreCase(requestName)){
                response.put('response_x', attachCreditReport());
                
            }else if ( 'createBusinessCreditProfile'.equalsIgnoreCase(requestName)){
                response.put('response_x', createBusinessCreditProfile());
                
            }else if ( 'createIndividualCustomerWithCreditWorthiness'.equalsIgnoreCase(requestName)){
                response.put('response_x', createIndividualCustomerWithCreditWorthiness());
                
            }else if ( 'getBusinessCreditProfileSummary'.equalsIgnoreCase(requestName)){
                response.put('response_x', getBusinessCreditProfileSummary());
                
            }else if ( 'getCreditReport'.equalsIgnoreCase(requestName)){
                response.put('response_x', getCreditReport());
                
            }else if ( 'getCreditReportList'.equalsIgnoreCase(requestName)){
                response.put('response_x', getCreditReportList());
                
            }else if ( 'removePartner'.equalsIgnoreCase(requestName)){
                response.put('response_x', removePartner());
                
            }else if ( 'replaceBusinessProprietor'.equalsIgnoreCase(requestName)){
                response.put('response_x', replaceBusinessProprietor());
                
            }else if ( 'searchBusinessCreditProfile'.equalsIgnoreCase(requestName)){
                response.put('response_x', searchBusinessCreditProfile());
                
            }else if ( 'searchIndivdualCreditProfile'.equalsIgnoreCase(requestName)){
                response.put('response_x', searchIndivdualCreditProfile());
                
            }else if ( 'updateBusinessCreditProfile'.equalsIgnoreCase(requestName)){
                response.put('response_x', updateBusinessCreditProfile());
                
            }else if ( 'updateIndivdualCreditProfile'.equalsIgnoreCase(requestName)){
                response.put('response_x', updateIndivdualCreditProfile());
                
            }else if ( 'voidCreditReport'.equalsIgnoreCase(requestName)){
                response.put('response_x', voidCreditReport());
                
            }
            
            
        }
    
    
    /*------------------------------------------------------------*/
    private CreditProfileServicePing.pingResponse_element 
        ping(){
            CreditProfileServicePing.pingResponse_element 
                pingResponse = new CreditProfileServicePing.pingResponse_element();
            return pingResponse;
        }
    
    
    private CreditProfileServiceRequestResponse.addBusinessPartnerResponse_element  
        addBusinessPartner(){
            CreditProfileServiceRequestResponse.addBusinessPartnerResponse_element 
                response_x = new CreditProfileServiceRequestResponse.addBusinessPartnerResponse_element();
            return response_x;
        }
    
    
    private CreditProfileServiceRequestResponse.addIndividualPartnerResponse_element 
        addIndividualPartner(){
            CreditProfileServiceRequestResponse.addIndividualPartnerResponse_element 
                response_x = new CreditProfileServiceRequestResponse.addIndividualPartnerResponse_element ();
            return response_x;
        }
    
    
    private CreditProfileServiceRequestResponse.attachCreditReportResponse_element
        attachCreditReport(){
            CreditProfileServiceRequestResponse.attachCreditReportResponse_element
                response_x = new CreditProfileServiceRequestResponse.attachCreditReportResponse_element();
            return response_x;
        }
    
    
    private CreditProfileServiceRequestResponse.createBusinessCreditProfileResponse_element 
        createBusinessCreditProfile(){
            CreditProfileServiceRequestResponse.createBusinessCreditProfileResponse_element 
                response_x = new CreditProfileServiceRequestResponse.createBusinessCreditProfileResponse_element();
            return response_x;
        }
    
    
    private CreditProfileServiceRequestResponse.createIndividualCustomerWithCreditWorthinessResponse_element
        createIndividualCustomerWithCreditWorthiness(){
            CreditProfileServiceRequestResponse.createIndividualCustomerWithCreditWorthinessResponse_element
                response_x = new CreditProfileServiceRequestResponse.createIndividualCustomerWithCreditWorthinessResponse_element();
            return response_x;
        }
    
    
    private CreditProfileServiceRequestResponse.getBusinessCreditProfileSummaryResponse_element 
        getBusinessCreditProfileSummary(){
            CreditProfileServiceRequestResponse.getBusinessCreditProfileSummaryResponse_element 
                response_x = new CreditProfileServiceRequestResponse.getBusinessCreditProfileSummaryResponse_element ();
            return response_x;
        }
    
    
    private CreditProfileServiceRequestResponse.getCreditReportResponse_element
        getCreditReport(){
            CreditProfileServiceRequestResponse.getCreditReportResponse_element
                response_x = new CreditProfileServiceRequestResponse.getCreditReportResponse_element();
            return response_x;
        }
    
    
    private CreditProfileServiceRequestResponse.getCreditReportListResponse_element
        getCreditReportList(){
            CreditProfileServiceRequestResponse.getCreditReportListResponse_element
                response_x = new CreditProfileServiceRequestResponse.getCreditReportListResponse_element();
            return response_x;
        }
    
    
    private CreditProfileServiceRequestResponse.removePartnerResponse_element
        removePartner(){
            CreditProfileServiceRequestResponse.removePartnerResponse_element
                response_x = new CreditProfileServiceRequestResponse.removePartnerResponse_element();
            return response_x;
        }
    
    
    private CreditProfileServiceRequestResponse.replaceBusinessProprietorResponse_element
        replaceBusinessProprietor(){
            CreditProfileServiceRequestResponse.replaceBusinessProprietorResponse_element
                response_x = new CreditProfileServiceRequestResponse.replaceBusinessProprietorResponse_element();
            return response_x;
        }
    
    
    private CreditProfileServiceRequestResponse.searchBusinessCreditProfileResponse_element
        searchBusinessCreditProfile(){
            CreditProfileServiceRequestResponse.searchBusinessCreditProfileResponse_element
                response_x = new CreditProfileServiceRequestResponse.searchBusinessCreditProfileResponse_element();
            return response_x;
        }
    
    
    private CreditProfileServiceRequestResponse.searchIndivdualCreditProfileResponse_element
        searchIndivdualCreditProfile(){
            CreditProfileServiceRequestResponse.searchIndivdualCreditProfileResponse_element
                response_x = new CreditProfileServiceRequestResponse.searchIndivdualCreditProfileResponse_element();
            return response_x;
        }
    
    
    private CreditProfileServiceRequestResponse.updateBusinessCreditProfileResponse_element
        updateBusinessCreditProfile(){
            CreditProfileServiceRequestResponse.updateBusinessCreditProfileResponse_element
                response_x = new CreditProfileServiceRequestResponse.updateBusinessCreditProfileResponse_element();
            return response_x;
        }
    
    
    private CreditProfileServiceRequestResponse.updateIndivdualCreditProfileResponse_element
        updateIndivdualCreditProfile(){
            CreditProfileServiceRequestResponse.updateIndivdualCreditProfileResponse_element
                response_x = new CreditProfileServiceRequestResponse.updateIndivdualCreditProfileResponse_element();
            return response_x;
        }
    
    
    private CreditProfileServiceRequestResponse.voidCreditReportResponse_element
        voidCreditReport(){
            CreditProfileServiceRequestResponse.voidCreditReportResponse_element
                response_x = new CreditProfileServiceRequestResponse.voidCreditReportResponse_element();
            return response_x;
        }
    
    
    
    
}