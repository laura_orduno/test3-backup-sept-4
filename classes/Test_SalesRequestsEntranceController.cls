@istest
public class Test_SalesRequestsEntranceController {
	@istest
    public static void test(){
        inuksuk_case_field__c customSetting=new inuksuk_case_field__c();
        customSetting.name='Inuksuk_Billing__c';
        customSetting.field_id__c='00N19000000K6Oy ';
        insert customSetting;
    	test.starttest();
        map<string,string> params=apexpages.currentpage().getparameters();
        params.put('RequestType','Profiler');
        SalesRequestsEntranceController controller=new SalesRequestsEntranceController();
        params.put('RequestType','Voice or Data Billing');
        controller=new SalesRequestsEntranceController();
        controller.useMyTelusInternalContact();
        params.put('RequestType','Billing');
        controller=new SalesRequestsEntranceController();
        controller.useMyTelusInternalContact();
        params.put('RequestType','Expedite');
        controller=new SalesRequestsEntranceController();
        controller.useMyTelusInternalContact();
        params.put('RequestType','createcontract');
        controller.selectRequest();
        controller.searchText='TestForce';
        controller.searchAccountContact();
        controller.useMyTelusInternalContact();
        params.put('RequestType','Order');
        controller=new SalesRequestsEntranceController();
        controller.useMyTelusInternalContact();
        params.put('RequestType','General');
        controller=new SalesRequestsEntranceController();
        controller.useMyTelusInternalContact();
        s2c_Sales_MyCasesController caseComponentController=new s2c_Sales_MyCasesController();
        test.stoptest();
    }
}