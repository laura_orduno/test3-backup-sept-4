@isTest
private class trac_Mass_AssignTest {
		@isTest(SeeAllData=true)
    private static void testReassign() {
        Lead l = new Lead();
        l.FirstName = 'Alex';
        l.LastName = 'Miller';
        l.Company = 'Developer';
        l.Phone = '6049969449';
        l.Email = 'amiller@tractionondemand.com';
        insert l;
        Note n = new Note(Body = 'Test body', Title = 'A Note', ParentId = l.Id);
        insert n;
        Date d = Date.today();
        d.addDays(-1);
        DateTime dt = Datetime.now();
        Task t = new Task(ActivityDate = d, Type = 'Call', Subject = 'Test call', Status = 'Not Started', WhoId = l.Id, Call_Outcome__c = 'not null', SMB_Call_Type__c = 'not null either');
        insert t;
        Event e = new Event(DurationInMinutes = 60, Meeting_type__c = 'Lab', ActivityDate = d, ActivityDateTime = dt, Subject = 'Test call', WhoId = l.Id);
        insert e;
        ApexPages.currentPage().getParameters().put('ids', l.id);
        trac_Mass_Assign tma = new trac_Mass_Assign();
        system.assertNotEquals(null, tma.getLeads());
        tma.newOwnerId = tma.newOwnerOptions[1].getValue();
        System.assertEquals(new PageReference('/00Q/o').getUrl(), tma.onAssign().getUrl());
    }
    
    private static testMethod void testError() {
        trac_Mass_Assign tma = new trac_Mass_Assign();
        system.assertEquals(new List<Lead>(), tma.getLeads());
        System.assertEquals(null, tma.onAssign());
    }
}