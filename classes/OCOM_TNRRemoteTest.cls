@isTest
public class OCOM_TNRRemoteTest 
{
    @testSetup
    static void testDataSetup() 
    {
        insert new OCOM_TNR_Settings__c(Line_Designation__c = 'Line Designation', 
                                    Line_Type__c = 'Line Type',
                                    Offering_ID__c = 'Offering ID',
                                    Overline__c = 'Overline',
                                    Phone_Number__c = 'Phone Number',
                                    Pilot__c = 'Pilot', 
                                    Single_Line__c = 'Single Line',
                                    Smart_Ring__c = 'SmartRing',
                                    Telephone_Number__c = 'Telephone Number',
                                    Telephone_Number_Setup__c = 'Telephone Number Setup',
                                    Toll_Free_Number__c='Toll-Free Number',
                                   	Wireless_Device_Type__c = 'Wireless Device Type',
                                   	Responsible_Provisioning_Group__c = 'Responsible Provisioning Group',
                                   	WLN_Provisioning_Group__c = '9145615376313186289',
                                   	WLS_Provisioning_Group__c = '9145615376313186291',
                                   	LDIT_Provisioning_Group__c = '9145615376313186293');
    }
    
    @isTest
    private static void autoAssignTNTest()
    {
        test.startTest();
        String orderId= OrdrTestDataFactory.orderIdWithVoiceProducts();
        String attributeid = 'a7R220000004CZQEA2';
        List<OrderItem> items = [select id from OrderItem where OrderId =:orderId and Product_display_name__c = 'Business Select Office Phone - Single Line'];
        String itemId = items.get(0).id;
        OCOM_TNRRemote rem1 = new OCOM_TNRRemote();
        //Object bj;
        //Object obj = new vlocity_cmt.CardCanvasController();
        OCOM_TNRRemote rem2 = new OCOM_TNRRemote();
        
            
            
        OCOM_TNRRemote.autoSelectTN(Id.valueOf(orderId), Id.valueOf(itemId), Id.valueOf(attributeid), 'undefined');
        OCOM_TNRRemote.releaseTN(Id.valueOf(orderId), Id.valueOf(itemId), Id.valueOf(attributeid), 'undefined');
        OCOM_TNRRemote.tnPortabilityCheck(Id.valueOf(orderId), Id.valueOf(itemId), '7803459080','9145615376313186289');
        OCOM_TNRRemote.tnPortabilityCheck(Id.valueOf(orderId), Id.valueOf(itemId), '7803459080','9145615376313186291');
        OCOM_TNRRemote.tnPortabilityCheck(Id.valueOf(orderId), Id.valueOf(itemId), '7803459080','9145615376313186293');
        OCOM_TNRRemote.tnPortabilityCheck(Id.valueOf(orderId), Id.valueOf(itemId), '7803459080','undefined');
        OCOM_TNRRemote.mapErrorResponse(null);
        OCOM_TNRRemote.mapErrorResponse(new MyException('test ex'));
         test.stopTest();
    }
    public class MyException extends Exception {}
}