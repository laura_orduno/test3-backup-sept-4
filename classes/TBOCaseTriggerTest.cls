/* 
    Added by Rajan T on 26-Feb-2016 for Code Coverge on 'smb_CaseBefore' Trigger 
    for TBO US : 34, 82. 
    */                                                                                                                                                                              
@isTest
public class TBOCaseTriggerTest{
    public static testmethod void testCodeCoverage(){
        

            test.starttest();
                Schema.DescribeSObjectResult tboSchema = Schema.SObjectType.Account;
                Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = tboSchema.getRecordTypeInfosByName(); 
                
                Schema.DescribeSObjectResult tboCaseSchema = Schema.SObjectType.Case;
                Map<String,Schema.RecordTypeInfo> caseRecordTypeInfo = tboCaseSchema.getRecordTypeInfosByName(); 
                
                Schema.DescribeSObjectResult tboCPSchema = Schema.SObjectType.Credit_Profile__c;
                Map<String,Schema.RecordTypeInfo> CPRecordTypeInfo = tboCPSchema.getRecordTypeInfosByName(); 
                
                //List<RecordType> rt = [Select Id from RecordType where Name = 'TBO Request'];
                //List<RecordType> rtCreditPr = [Select Id from RecordType where Name = 'Business'];
                
                //List<RecordType> rt1 = [Select Id from RecordType where Name = 'RCID'];
                
                Account ppAccount = new Account();
                ppAccount.Name = 'PPtestAccount';
                ppAccount.RecordTypeId = AccountRecordTypeInfo.get('RCID').getRecordTypeId();
                //ptestAccount.parentid = '';
                insert ppAccount;
            
                Account ptestAccount = new Account();
                ptestAccount.Name = 'ptestAccount';
                ptestAccount.RecordTypeId = AccountRecordTypeInfo .get('RCID').getRecordTypeId();
                ptestAccount.parentid = ppAccount.id;
                insert ptestAccount;
        
                Account ptestAccount1 = new Account();
                ptestAccount1.Name = 'ptestAccount1';
                ptestAccount1.RecordTypeId = AccountRecordTypeInfo .get('RCID').getRecordTypeId();
                //ptestAccount1.parentid = '';
                insert ptestAccount1;
        
        
                Account testAccount = new Account();
                testAccount.Name = 'testAccount';
                testAccount.RecordTypeId = AccountRecordTypeInfo .get('RCID').getRecordTypeId();
                //testAccount.parentid = ptestAccount.id;
                insert testAccount;
                
                contact contTest = new contact();
                contTest.Lastname = 'test1';
                contTest.AccountId = testAccount.Id;
                insert contTest;
                
                Account testAccount1 = new Account();
                testAccount1.Name = 'testAccount1';
                testAccount1.RecordTypeId = AccountRecordTypeInfo .get('RCID').getRecordTypeId();
                //testAccount1.parentid = ptestAccount1.id;
                insert testAccount1;
                
                contact contTest1 = new contact();
                contTest1.Lastname = 'test1';
                contTest1.AccountId = testAccount1.Id;
                insert contTest1;
                
                Credit_Profile__c cpr = new Credit_Profile__c ();
                cpr.Account__c = testAccount1.Id;
                cpr.RecordTypeId = CPRecordTypeInfo.get('Business').getRecordTypeId();
                cpr.Name = 'TestCreditProfile';
                cpr.Legal_Entity_Type__c = 'Limited(LTD)';
                cpr.Corporate_Registry_Status__c = 'Active';
                cpr.Risk_Indicator__c='dsds';
                cpr.Registration_Jurisdiction__c = 'sdds';
                cpr.Registration_Jurisdiction_Country__c ='dsdsd';
                //cpr.Credit_Profile_Status__c = 'esdfdf';
                insert cpr;
                
            Entitlement ent = new Entitlement();
            ent.Name  = 'SMB Care';
            ent.accountID = testAccount1.Id ;
            insert ent;
            
            
               // Added by Arvind for Too many SOQL issue 12/01/2017
               List<Case> lstCases = new List<Case>();
                Case c = new Case();
                //c.OwnerId = u1.id;
                c.Subject = '';
                c.CreatedDate = System.now();
                c.Status = 'Draft';
                //c.Account__c = testAccount.Id;
            c.Escalated_Case__c = true;
             // c.Escalation_End__c = '';
                c.Requested_Transfer_Date__c = System.today();
                c.accountID = testAccount.Id;
                c.ContactId =   contTest.Id;
                c.RecordTypeId = caseRecordTypeInfo .get('TBO Request').getRecordTypeId();
                c.Master_Account_Name__c = testAccount1.Id;   
                c.Contact_phone_number__c =contTest1.id;
                c.Incoming_Only__c = true;
                c.Credit_Profile__c = cpr.id;
                //c.Account__c = testAccount1.id;
                c.Escalated_Case__c =true;
                c.Escalation_End__c = null;
                c.Verified_Outgoing_Customer_Not_Available__c = 'this is outside cutomer';
                //insert c;
                lstCases.add(c);
                
                
             //List<RecordType> rt11 = [Select Id from RecordType where Name = 'Legal Name Update'];
               
       
         
        case c1 = new case();
        c1.Updated_Legal_Name__c = 'legal name update';
        c1.Priority = 'High';
        c1.Status = 'In Progress';
        c1.TBO_In_Progress_Status__c = 'Default';
        c1.accountID = testAccount.Id;
        c1.ContactId =   contTest.Id;
       // c1.Account__c = testAccount.id;
        c1.Source__c = 'Legal Name';
        c1.recordTypeId = caseRecordTypeInfo.get('Legal Name Update').getRecordTypeId();
        c1.Requested_Transfer_Date__c = System.today();
        c1.transfer_type__c = 'Legal Name Change';
        c1.Business_Type__c ='Society/Association/Council';
        c1.Jurisdiction__c ='FEDERAL';
        //insert c1; 
        //System.assert(c.Credit_Profile__c == cpr.Id);
        lstCases.add(c1);
        insert lstCases;
                
       test.stoptest(); 
         
    
    }
    public static testMethod void testCreditProfile() {
        test.starttest();
                Schema.DescribeSObjectResult tboSchema = Schema.SObjectType.Account;
                Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = tboSchema.getRecordTypeInfosByName(); 
                
                Schema.DescribeSObjectResult tboCaseSchema = Schema.SObjectType.Case;
                Map<String,Schema.RecordTypeInfo> caseRecordTypeInfo = tboCaseSchema.getRecordTypeInfosByName(); 
                
                Schema.DescribeSObjectResult tboCPSchema = Schema.SObjectType.Credit_Profile__c;
                Map<String,Schema.RecordTypeInfo> CPRecordTypeInfo = tboCPSchema.getRecordTypeInfosByName();
                
                //List<RecordType> rt = [Select Id from RecordType where Name = 'TBO Request'];
                //List<RecordType> rtCreditPr = [Select Id from RecordType where Name = 'Business'];
                
                //  List<RecordType> rt1 = [Select Id from RecordType where Name = 'RCID'];
             Account ptestAccount = new Account();
                ptestAccount.Name = 'ptestAccount';
                ptestAccount.RecordTypeId = AccountRecordTypeInfo.get('RCID').getRecordTypeId();
                ptestAccount.BCAN__c = 'TestAccountBCAN';
                //ptestAccount.parentid = '';
                insert ptestAccount;
             Account ptestAccount1 = new Account();
                ptestAccount1.Name = 'ptestAccount1';
                ptestAccount1.RecordTypeId = AccountRecordTypeInfo.get('RCID').getRecordTypeId();
                //ptestAccount1.parentid = '';
                insert ptestAccount1;
        
        
                Account testAccount = new Account();
                testAccount.Name = 'testAccount';
                testAccount.RecordTypeId = AccountRecordTypeInfo.get('RCID').getRecordTypeId();
                testAccount.parentid = ptestAccount.id;
                
                insert testAccount;
                
                contact contTest = new contact();
                contTest.Lastname = 'test1';
                contTest.AccountId = testAccount.Id;
                insert contTest;
                
                Account testAccount1 = new Account();
                testAccount1.Name = 'testAccount1';
                testAccount1.RecordTypeId = AccountRecordTypeInfo.get('RCID').getRecordTypeId();
                testAccount1.parentid = ptestAccount1.id;
                insert testAccount1;
                
                contact contTest1 = new contact();
                contTest1.Lastname = 'test1';
                contTest1.AccountId = testAccount1.Id;
                insert contTest1;
                
                Credit_Profile__c cpr = new Credit_Profile__c ();
                cpr.Account__c = testAccount1.Id;
                cpr.RecordTypeId = CPRecordTypeInfo.get('Business').getRecordTypeId();
                cpr.Name = 'TestCreditProfile';
                cpr.Legal_Entity_Type__c = 'Limited(LTD)';
                cpr.Corporate_Registry_Status__c = 'Active';
                cpr.Risk_Indicator__c='dsds';
                cpr.Registration_Jurisdiction__c = 'sdds';
                cpr.Registration_Jurisdiction_Country__c ='dsdsd';
                //cpr.Credit_Profile_Status__c = 'esdfdf';
                insert cpr;
        
               
                Case c = new Case();
                c.Subject = '';
                c.CreatedDate = System.now();
                c.Status = 'Draft';
                c.Requested_Transfer_Date__c = System.today();
                c.accountid = testAccount.Id;
                c.ContactId =   contTest.Id;
                c.RecordTypeId = CaseRecordTypeInfo.get('TBO Request').getRecordTypeId();
                c.Master_Account_Name__c = testAccount1.Id;   
                c.Contact_phone_number__c =contTest1.id;
                c.Incoming_Only__c = true;
                c.Credit_Profile__c = cpr.id;
                c.Account__c = testAccount1.id;
                c.Escalated_Case__c =true;
                //c.Escalation_End__c = null;
                c.Verified_Outgoing_Customer_Not_Available__c = 'this is outside cutomer';
                insert c;
        
                System.assert(c.Credit_Profile__c == cpr.Id);
                
           test.stoptest();   
         
    }
    // for work request before insert trigger - umesh(25-May-2016)
   public static testMethod void TBOWorkRequestTriggerTest() {
       test.startTest();
        Schema.DescribeSObjectResult tboSchema = Schema.SObjectType.Account;
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = tboSchema.getRecordTypeInfosByName(); 
        
        Schema.DescribeSObjectResult tboCaseSchema = Schema.SObjectType.Case;
        Map<String,Schema.RecordTypeInfo> caseRecordTypeInfo = tboCaseSchema.getRecordTypeInfosByName();
        
        Schema.DescribeSObjectResult wrSchema = Schema.SObjectType.Work_Request__c;
        Map<String,Schema.RecordTypeInfo> wrTypeInfo = wrSchema.getRecordTypeInfosByName();        
             
        Account testAccount = new Account();
        testAccount.Name = 'testAccount';
        testAccount.RecordTypeId = AccountRecordTypeInfo.get('RCID').getRecordTypeId();
        insert testAccount;
        
        contact contTest = new contact();
        contTest.Lastname = 'test1';
        contTest.AccountId = testAccount.Id;
        insert contTest;
        
        
        
        Account testAccount1 = new Account();
        testAccount1.Name = 'testAccount1';
        testAccount1.RecordTypeId = AccountRecordTypeInfo.get('RCID').getRecordTypeId();
        insert testAccount1;
        
        contact contTest1 = new contact();
                contTest1.Lastname = 'test1';
                contTest1.AccountId = testAccount1.Id;
                insert contTest1;
        
        Case c = new Case();
        c.Status = 'Draft';
        c.RecordTypeId = CaseRecordTypeInfo.get('TBO Request').getRecordTypeId();
        
        //c.Account__c = testAccount.Id;
        //c.AccountId = testAccount1.Id;
       
        c.Subject = '';
        c.Master_Account_Name__c = testAccount.Id;
        c.CreatedDate = System.now();
        c.Status = 'Draft';
        c.Dept__c = 'Business';
        c.Region__c = '';
        c.Requested_Transfer_Date__c = System.today();
        c.accountID = testAccount.Id;
        c.ContactId =   contTest.Id;
       
        c.Master_Account_Name__c = testAccount1.Id;   
        c.Contact_phone_number__c =contTest1.id;
        //c.Incoming_Only__c = true;
        //c.Escalated_Case__c =true;
        //c.Verified_Outgoing_Customer_Not_Available__c = 'qwdwdasdas';        
        insert c;
        
        System.assert(c.accountID == testAccount.Id);
        c.status = 'In progress';
        c.TBO_In_Progress_Status__c = 'Customer Validation';
        c.Transfer_Type__c = 'Legal Name Change';
        c.Other_Working_TELUS_Service__c = 'Test123';
        c.Directory_Information__c = 'Listed';
        c.Listing_Name__c = 'sdsdsads';
        c.Business_Type__c = 'Society/Association/Council';
        c.Jurisdiction__c = 'BC';
        c.Enter_State__c ='US';
        c.accountID = testAccount.Id;
        c.Incoming_Only__c = true;
        c.Verified_Outgoing_Customer_Not_Available__c = 'qwdwdasdas';
        update c;       
       
      
       
      
        
         case_account__c caseacc = new case_account__c();
          caseacc.Service_type__c = 'Centrex/PRI';
          caseacc.Case__c = c.id;
          insert caseacc;
         case_account__c caseacc1 = new case_account__c();
          caseacc1.Service_type__c = 'BTV;LDIT';
          caseacc1.Case__c = c.id;
          insert caseacc1;   
        
        
        
          work_request__c wreq = new work_request__c();
          wreq.Case__c = c.id;
          wreq.Status__c = 'New';
          wreq.RecordTypeId = wrtypeInfo.get('TBO Work Request').getRecordTypeId();
          wreq.Service_Type__c = 'Centrex/PRI';
          insert wreq;
        
        work_request__c wreq2 = new work_request__c();
          wreq2.Case__c = c.id;
          wreq2.Status__c = 'New';
          wreq2.RecordTypeId = wrtypeInfo.get('TBO Work Request').getRecordTypeId();
          wreq2.Service_Type__c = 'Centrex/PRI';
          
       try {
        insert wreq2;
         test.stopTest();
      
      } catch (exception E){
         Boolean expectedExceptionThrown =  e.getMessage().contains('You are not allowed to create two Work Requests for the same Service types') ? true : false;
          System.AssertEquals(expectedExceptionThrown, true);
       }
            
         
    }


    /* public static testMethod void testErrorCondition() {
        test.starttest();
        
        Schema.DescribeSObjectResult tboSchema = Schema.SObjectType.Account;
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = tboSchema.getRecordTypeInfosByName(); 
        
        Schema.DescribeSObjectResult tboCaseSchema = Schema.SObjectType.Case;
        Map<String,Schema.RecordTypeInfo> caseRecordTypeInfo = tboCaseSchema.getRecordTypeInfosByName();
        
        Schema.DescribeSObjectResult wrSchema = Schema.SObjectType.Work_Request__c;
        Map<String,Schema.RecordTypeInfo> wrTypeInfo = wrSchema.getRecordTypeInfosByName();
        
        //List<RecordType> rt = [Select Id from RecordType where Name = 'TBO Request'];
        //     List<RecordType> rt1 = [Select Id from RecordType where Name = 'RCID'];        
          Account testAccount = new Account();
        testAccount.Name = 'testAccount';
        testAccount.RecordTypeId = AccountRecordTypeInfo.get('RCID').getRecordTypeId();
        insert testAccount;
        
        contact contTest = new contact();
                contTest.Lastname = 'test1';
                contTest.AccountId = testAccount.Id;
                insert contTest;
        
        
        
        Account testAccount1 = new Account();
        testAccount1.Name = 'testAccount1';
        testAccount1.RecordTypeId = AccountRecordTypeInfo.get('RCID').getRecordTypeId();
        insert testAccount1;
        
               
        contact contTest1 = new contact();
                contTest1.Lastname = 'test1';
                contTest1.AccountId = testAccount1.Id;
                insert contTest1;
        
        
        
        Case c = new Case();
        c.Status = 'Draft';
        //if(rt != null && rt.size() > 0)
            c.RecordTypeId = CaseRecordTypeInfo.get('TBO Request').getRecordTypeId();

         c.Subject = '';
       c.CreatedDate = System.now();
        c.Status = 'Draft';
        c.Requested_Transfer_Date__c = System.today();
        c.accountID = testAccount.Id;
        c.ContactId =   contTest.Id;
       
        c.Master_Account_Name__c = testAccount1.Id;   
        c.Contact_phone_number__c =contTest1.id;
        c.Incoming_Only__c = true;
        
        c.Verified_Outgoing_Customer_Not_Available__c = 'this is outside cutomer';
      
                
        insert c;
       
        test.stoptest();
        System.assert(c.Status == 'Draft');
    } 
      public static testMethod void differRtname() {
          test.starttest();
          
          Schema.DescribeSObjectResult tboSchema = Schema.SObjectType.Account;
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = tboSchema.getRecordTypeInfosByName(); 
        
        Schema.DescribeSObjectResult tboCaseSchema = Schema.SObjectType.Case;
        Map<String,Schema.RecordTypeInfo> caseRecordTypeInfo = tboCaseSchema.getRecordTypeInfosByName();
        
        //List<RecordType> rt = [Select Id from RecordType where Name = 'Legal Name Update'];
        //List<RecordType> rt1 = [Select Id from RecordType where Name = 'RCID'];        
          
        Account testAccount = new Account();
        testAccount.Name = 'testAccount';
        testAccount.RecordTypeId = AccountRecordTypeInfo.get('RCID').getRecordTypeId();
        insert testAccount;
         
        case c = new case();
        c.Updated_Legal_Name__c = 'legal name update';
        
        c.Priority = 'High';
        c.Status = 'On hOld';
        c.Account__c = testAccount.id;
        c.Source__c = 'Legal Name';
        insert c; 
          test.stoptest();
         
         
    
        }*/
}