/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PopQuizBuilderController {
    global static void savePopQuizQuestionOptions(LRN.PopQuizBuilderController.PopQuizWrapper quizWrapper) {

    }
    global static Integer saveQuizQuestionResponse(List<LRN.PopQuizBuilderController.PopQuizQuestion> questions, String quizId) {
        return null;
    }
global class PopQuizChoice {
}
global class PopQuizQuestion {
}
global class PopQuizWrapper {
}
}
