@isTest
public class restrictDeleteAccountTest {
    
    private static testMethod void testTrigger() {
            Id PROFILEID = [SELECT id FROM Profile WHERE name='Standard User' LIMIT 1].id;
            User u = new User(
                username = 'VMDFJREFSRI@TRACTIONSM.COM',
              email = 'test@example.com',
              title = 'test',
              lastname = 'test',
              alias = 'test',
              TimezoneSIDKey = 'America/Los_Angeles',
              LocaleSIDKey = 'en_US',
              EmailEncodingKey = 'UTF-8',
              ProfileId = PROFILEID,
              LanguageLocaleKey = 'en_US'
            );
            
            system.runas(u) {
                Account r = new Account(name = 'RCID');
                insert r;
                
                Account a = new Account(name = 'test', RCID__c = r.id);
                insert a;
                
                Boolean caughtException = false;
                try {
                    delete a;
                } catch (Exception e) {
                    caughtException = true;
                    system.assert(e.getMessage().contains('You cannot delete ECH Accounts'));
                }
                //commented out to deactivate trigger - system.assert(caughtException);
                
                Account a2 = new Account(name = 'test');
                insert a2;
                
                delete a2;
            }
    }
}