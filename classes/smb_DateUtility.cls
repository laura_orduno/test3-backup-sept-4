global class smb_DateUtility {

    private static final Map<string, List<string>> monthNames = new Map<string, List<string>>{'en' => new List<string> {'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'},
                                                                                              'fr' => new List<string> {'janv', 'févr', 'mars', 'Avril', 'mai', 'juin', 'juil', 'août', 'sept', 'oct', 'nov', 'déc'}};
    
    global static string getMonthName(integer monthNumber) {
        if (1 > monthNumber || monthNumber > 12) {
            //todo: throw an error
            return null;
        }
        
        if (UserInfo.getLocale().startsWith('fr')) {
            return monthNames.get('fr').get(monthNumber - 1);
        } 
        else {
            return monthNames.get('en').get(monthNumber - 1);
        }
        
         
    }
    
    global static string formatDate(Date dateValue) {
        if (dateValue == null) return null;
        return getMonthName(dateValue.month()) + ' ' + string.valueOf(dateValue.day()) + ', ' + string.valueOf(dateValue.year());
    }
    
    global static string formatDate(DateTime dateValue) {
        if (dateValue == null) return null;
        return formatDate(dateValue.Date());
    }
    
    // converts string to date format.
    // RB IBM
    global static date convertStringtoDate(string formatedDate)
    {
        list<string> dateList = formatedDate.replace(',','').split(' ');
        
        if (datelist.size() != 3) return null;
        
        list<string> lstMonthNames = new list<string>();
        lstMonthNames = UserInfo.getLocale().startsWith('fr')? monthNames.get('fr'):monthNames.get('en');
        integer monthIndex = 1;
        
        for( string s :lstMonthNames )
        {
            if( s == dateList[0])
            {
                break;
            }
            monthIndex++;
            
        }
         if (monthIndex >12 ) // error
         return null;
         return date.newInstance(integer.valueof(datelist[2]), monthIndex, integer.valueof(datelist[1]));
    
    }

}