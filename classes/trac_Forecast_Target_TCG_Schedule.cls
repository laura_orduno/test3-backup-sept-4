//schedule with System.schedule('Update Target TCG Forecast','0 0 1 * * ?', new trac_Forecast_Target_TCG_Schedule());
global class trac_Forecast_Target_TCG_Schedule implements schedulable {

	global void execute(SchedulableContext sc) {
		runUpdate();
	}
	
	public void runUpdate() {
		
		trac_Forecast_Report myR = new trac_Forecast_Report();
		
		myR.getForecasts_Target_TCG();
	}
}