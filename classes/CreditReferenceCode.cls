public class CreditReferenceCode {
     
    public static String getLegalEntityTypeCd(String salesforceValue){
        if(String.isBlank(salesforceValue)) return null;
        String refpdsCode='';
        List<LegalEntityType__c> theTypeList = LegalEntityType__c.getAll().values();        
        for(LegalEntityType__c theType:theTypeList){
            if(salesforceValue.equalsIgnoreCase(theType.Salesforce_Value__c)){
                refpdsCode = theType.Refpds_Code__c;
                break;
            }
        }
        return refpdsCode;
    }
    
    public static String getCreditRiskIndicatorCd(String salesforceValue){
        if(salesforceValue==null) return null;
        String refpdsCode='';
        List<RiskIndicator__c> theTypeList = RiskIndicator__c.getAll().values();        
        for(RiskIndicator__c theType:theTypeList){
            salesforceValue = CreditWebServiceClientUtil.removeAllSpaces(salesforceValue);
            String theTypeValue = theType.Salesforce_Value__c;
            theTypeValue = CreditWebServiceClientUtil.removeAllSpaces(theTypeValue);
            if(salesforceValue.equalsIgnoreCase(theTypeValue)){
                refpdsCode = theType.Refpds_Code__c;
                break;
            }
        }
        return refpdsCode;
    }
    
    public static String getCustomerStatusCd(String salesforceValue){
        if(String.isBlank(salesforceValue)) return null;
        String refpdsCode='';
        List<CustomerType__c> theTypeList = CustomerType__c.getAll().values();        
        for(CustomerType__c theType:theTypeList){
            if(salesforceValue.equalsIgnoreCase(theType.Salesforce_Value__c)){
                refpdsCode = theType.Refpds_Code__c;
                break;
            }
        }
        return refpdsCode;
    }

    public static String getCoporateRegistryStatusCd(String salesforceValue){
        if(String.isBlank(salesforceValue)) return null;
        String refpdsCode='';
        List<CorporateRegistryStatus__c> theTypeList = CorporateRegistryStatus__c.getAll().values();        
        for(CorporateRegistryStatus__c theType:theTypeList){
            if(salesforceValue.equalsIgnoreCase(theType.Salesforce_Value__c)){
                refpdsCode = theType.Refpds_Code__c;
                break;
            }
        }
        return refpdsCode;
    }
    
    public static String getJurisdictionCountryCd(String salesforceValue){
        if(String.isBlank(salesforceValue)) return null;
        String refpdsCode = '';
		if ( salesforceValue != null )
        {
            List<RegistrationJurisdictionCountry__c> theTypeList = RegistrationJurisdictionCountry__c.getAll().values();        
            for(RegistrationJurisdictionCountry__c theType:theTypeList){
                if(salesforceValue.equalsIgnoreCase(theType.Salesforce_Value__c)){
                    refpdsCode = theType.Refpds_Code__c;
                    break;
                }
            }
        }
        return refpdsCode;
    }
    
    public static String getJurisdictionCountryName(String refpdsCode ){
        if(String.isBlank(refpdsCode)) return null;
        String salesforceValue= '';
		if ( refpdsCode != null )
        {
            List<RegistrationJurisdictionCountry__c> theTypeList = RegistrationJurisdictionCountry__c.getAll().values();        
            for(RegistrationJurisdictionCountry__c theType:theTypeList){
                if(refpdsCode.equalsIgnoreCase(theType.Refpds_Code__c )){
                    salesforceValue = theType.Salesforce_Value__c;
                    break;
                }
            }
        }
        return salesforceValue;
    }
   
    public static String getJurisdictionProvinceCd(String salesforceValue){
        if(String.isBlank(salesforceValue)) return null;
        String refpdsCode = '';
		if ( salesforceValue != null )
        {
            List<RegistrationJurisdictionProvince__c> theTypeList = RegistrationJurisdictionProvince__c.getAll().values();        
            for(RegistrationJurisdictionProvince__c theType:theTypeList){
                if(salesforceValue.equalsIgnoreCase(theType.Salesforce_Value__c)){
                    refpdsCode = theType.Refpds_Code__c;
                    break;
                }
            }
        }            
        return refpdsCode;
    }
    
    public static String getJurisdictionProvinceName(String refpdsCode ){
        if(String.isBlank(refpdsCode)) return null;
        String salesforceValue= '';
		if ( refpdsCode != null )
        {
            List<RegistrationJurisdictionProvince__c> theTypeList = RegistrationJurisdictionProvince__c.getAll().values();        
            for(RegistrationJurisdictionProvince__c theType:theTypeList){
                if(refpdsCode.equalsIgnoreCase(theType.Refpds_Code__c )){
                    salesforceValue = theType.Salesforce_Value__c;
                    break;
                }
            }
        }
        return salesforceValue;
    }
    
    public static String getOrderTypeCd(String salesforceValue){
        if(String.isBlank(salesforceValue)) return null;
        String refpdsCode='';
        List<PrimaryOrderType__c> theTypeList = PrimaryOrderType__c.getAll().values();        
        for(PrimaryOrderType__c theType:theTypeList){
            if(salesforceValue.equalsIgnoreCase(theType.Salesforce_Value__c)){
                refpdsCode = theType.Refpds_Code__c;
                break;
            }
        }
        return refpdsCode;
    }
    
    public static String getProductTypeCd(String salesforceValue){
        if(String.isBlank(salesforceValue)) return null;
        String refpdsCode='';
        List<Credit_Product_Codes__c> theTypeList = Credit_Product_Codes__c.getAll().values();        
        for(Credit_Product_Codes__c theType:theTypeList){
            if(salesforceValue.equalsIgnoreCase(theType.Salesforce_Value__c)){
                refpdsCode = theType.Refpds_Code__c;
                break;
            }
        }
        return refpdsCode;
    }
    
    public static String getReportTypeCd(String salesforceValue){
        if(String.isBlank(salesforceValue)) return null;
        String refpdsCode='';
        List<Credit_Report_Type__c> theTypeList = Credit_Report_Type__c.getAll().values();        
        for(Credit_Report_Type__c theType:theTypeList){
            if(salesforceValue.equalsIgnoreCase(theType.Salesforce_Value__c)){
                refpdsCode = theType.Refpds_Code__c;
                break;
            }
        }
        return refpdsCode;
    }
    
    public static String getReportSourceCd(String salesforceValue){
        if(String.isBlank(salesforceValue)) return null;
        String refpdsCode='';
        List<Credit_Report_Source__c> theTypeList = Credit_Report_Source__c.getAll().values();        
        for(Credit_Report_Source__c theType:theTypeList){
            if(salesforceValue.equalsIgnoreCase(theType.Salesforce_Value__c)){
                refpdsCode = theType.Refpds_Code__c;
                break;
            }
        }
        return refpdsCode;
    }
    
    public static String getCorporateRegistryStatus(String salesforceValue){
        if(String.isBlank(salesforceValue)) return null;
        String refpdsCode='';
        List<CorporateRegistryStatus__c> theTypeList = CorporateRegistryStatus__c.getAll().values();        
        for(CorporateRegistryStatus__c theType:theTypeList){
            if(salesforceValue.equalsIgnoreCase(theType.Salesforce_Value__c)){
                refpdsCode = theType.Refpds_Code__c;
                break;
            }
        }
        return refpdsCode;
    }
    
    public static String getSingleLetterCode(String str){
        if('Yes'.equalsIgnoreCase(str)) return 'Y';
        else return 'N';        
    }
    
	public static String getYesNo(String value)
    {
        return ('Yes'.equalsIgnoreCase(value) ? 'Yes' : ('No'.equalsIgnoreCase(value) ? 'No' : ''));
    }
}