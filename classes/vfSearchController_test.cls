@isTest(SeeAllData=false)
private class vfSearchController_test {

	private static Profile p = [SELECT Id FROM Profile WHERE UserType = 'CSPLitePortal' LIMIT 1];
	private static User u;
	private static VFSearchController ctlr;

	private static void runTestForRole(String custRole) {
		ctlr = new VfSearchController();
		ctlr.searchstring = 'test';
		ctlr.morePages = true;
		ctlr.totalPages = 2;
		ctlr.selectedPage = 1;
		ctlr.getDataCategoryGroupInfo();
		ctlr.test();
		ctlr.getLanguageLocaleKey();

		Account a = new Account(Name = 'Unit Test Account');
		a.Remedy_Business_Unit_Id__c = 'PCLOUD';
		a.Remedy_Company_Id__c = 'TCS';
		a.strategic__c = false;
		insert a;

		ENTP_portal_additional_functions__c epaf = new ENTP_portal_additional_functions__c(Additional_Function__c = 'Test', Name = 'Tester', Customer_Portal_Role__c = custRole);
		insert epaf;
		Contact c = new Contact(Email = 'test-user-1@unit-test.com', LastName = 'Test', AccountId = a.Id);
		c.Remedy_PIN__c = '70208';
		insert c;

		u = new User(Alias = 'TestUser', Email = 'test-user-1@unit-test.com', ContactId = c.Id,
				EmailEncodingKey = 'UTF-8', FirstName = 'Unit', LastName = 'Testing', LanguageLocaleKey = 'en_US',
				LocaleSidKey = 'en_US', CommunityNickname = 'test-user-1UNTST', /*UserRoleId = portalRole.Id,*/ ProfileId = p.Id,
				TimeZoneSidKey = 'America/Los_Angeles',
				UserName = 'test-user-1@unit-test.com');

		u.Customer_Portal_Role__c = custRole;
		insert u;

		Account_Configuration__c aconfig = new Account_Configuration__c(Account__c = a.id, TELUS_Product__c = 'Private_Cloud');
		insert aconfig;

		List<KnowledgeArticleVersion> lstSKAV = new List<KnowledgeArticleVersion>();

		string qryString = 'SELECT Id, title, UrlName, LastPublishedDate,LastModifiedById FROM KnowledgeArticleVersion WHERE (PublishStatus = \'online\' and Language = \'en_US\')';

		lstSKAV = Database.query(qryString);
		integer maxSize = lstSKAV.size() ;

		DataCategoryGroupInfo[] categoryGroups = DataCategoryUtil.getInstance().getAllCategoryGroups();
		Test.setCurrentPageReference(new PageReference('Page.ArticleList_new'));
		System.currentPageReference().getParameters().put('categoryType_' + 'TELUS_product', 'How_To');
		ctlr.getCategoryKeyword();

		Test.setCurrentPageReference(new PageReference('Page.ArticleList_new'));
		System.currentPageReference().getParameters().put('categoryType_' + 'TELUS_product', 'NoFilter');
		ctlr.getCategoryKeyword();

		System.runAs(u) {
			ctlr.getCustRole();

			string searchstring = 'test';

			system.assertEquals(searchstring, 'test');
			boolean morePages = true;
			system.assertEquals(morePages, true);
			ctlr.refreshSearchResult();
			ctlr.refreshSearchResultPC();
			ctlr.getPrevRequired();
			ctlr.getNextRequired();
			ctlr.getCurrentPageNumber();
			ctlr.next();
			ctlr.previous();
			ctlr.getKavResults();
			ctlr.getCATLISTPULL();
			ctlr.setCATLISTPULL('test');
			ctlr.getKavResults();
			ctlr.pageNumbers = new List<Integer>{
					1
			};
			System.assert(ctlr.pageNumbers != null);

			ctlr.maxSize = ctlr.pageSize = 1;
			ctlr.resetSearch();
		}
	}

	@isTest(SeeAllData=false) static void VfSearchControllerPC() {
		runTestForRole('private cloud');
	}

	@isTest(SeeAllData=false) static void VfSearchControllerTPS() {
		runTestForRole('tps');
	}

	@isTest(SeeAllData=false) static void VfSearchControllerMITS() {
		runTestForRole('mits');
	}
}