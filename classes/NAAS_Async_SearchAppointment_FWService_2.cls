public class NAAS_Async_SearchAppointment_FWService_2 {
    public class AsyncFieldWorkAppointmentService_v2_0_SOAP {
       public String endpoint_x = SMBCare_WebServices__c.getInstance('SearchDueDate_EndPoint').Value__c;
     //  public String endpoint_x ='http://requestb.in/1gv8c4s1';
        public Map<String,String> inputHttpHeaders_x;
       public String clientCertName_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://telus.com/wsdl/RMO/ProcessMgmt/FieldWorkAppointmentService_2', 'SMB_Appointment_FWAppointmentService_2', 'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/ping_v1', 'SMB_Appointment_ping_v1', 'http://xmlschema.tmi.telus.com/xsd/Resource/Resource/WorkForceManagementOrderTypes_v2', 'SMB_Appointment_WFMgmtOrderTypes_V2', 'http://xmlschema.tmi.telus.com/srv/RMO/ProcessMgmt/FieldWorkAppointmentServiceRequestResponse_v2', 'SMB_Appointment_FWAppointmentSvcReqRes', 'http://xmlschema.tmi.telus.com/xsd/common/exceptions/Exceptions_v1_0', 'SMB_App_XsdCommonExcep'};
        public OCOM_Async_SearchAppointment_ping_v1.pingResponse_elementFuture beginPing(System.Continuation continuation) {
            SMB_Appointment_ping_v1.ping_element request_x = new SMB_Appointment_ping_v1.ping_element();
            return (OCOM_Async_SearchAppointment_ping_v1.pingResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              OCOM_Async_SearchAppointment_ping_v1.pingResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              'ping',
              'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/ping_v1',
              'ping',
              'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/ping_v1',
              'pingResponse',
              'SMB_Appointment_ping_v1.pingResponse_element'}
            );
        }
        public NAAS_Async_SearchAppointment_FWSvcReqRes.SearchAvailableAppointmentListResponseFuture beginSearchAvailableAppointmentList(System.Continuation continuation,SMB_Appointment_WFMgmtOrderTypes_V2.InputHeader inputHeader,SMB_Appointment_WFMgmtOrderTypes_V2.WorkOrder workOrder,String appointmentProfileName,DateTime startDate,DateTime endDate,Boolean gradeAppointmentInd,Boolean fullSearchInd) {
            SMB_Appointment_FWAppointmentSvcReqRes.SearchAvailableAppointmentList request_x = new SMB_Appointment_FWAppointmentSvcReqRes.SearchAvailableAppointmentList();
            request_x.inputHeader = inputHeader;
            request_x.workOrder = workOrder;
            request_x.appointmentProfileName = appointmentProfileName;
            request_x.startDate = startDate;
            request_x.endDate = endDate;
            request_x.gradeAppointmentInd = gradeAppointmentInd;
            request_x.fullSearchInd = fullSearchInd;
            system.debug(' inAsyn 0'+request_x);
            return (NAAS_Async_SearchAppointment_FWSvcReqRes.SearchAvailableAppointmentListResponseFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              NAAS_Async_SearchAppointment_FWSvcReqRes.SearchAvailableAppointmentListResponseFuture.class,
              continuation,
              new String[]{endpoint_x,
              'searchAvailableAppointmentList',
              'http://xmlschema.tmi.telus.com/srv/RMO/ProcessMgmt/FieldWorkAppointmentServiceRequestResponse_v2',
              'searchAvailableAppointmentList',
              'http://xmlschema.tmi.telus.com/srv/RMO/ProcessMgmt/FieldWorkAppointmentServiceRequestResponse_v2',
              'searchAvailableAppointmentListResponse',
              'SMB_Appointment_FWAppointmentSvcReqRes.SearchAvailableAppointmentListResponse'}
            );
        }
    }
}