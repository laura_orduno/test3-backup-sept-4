/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class SMB_CPQQuoteHelper_Test {

    static testMethod void createAgreementTest() {
        Opportunity testOpp   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp.Type = 'All Other Orders';
        insert testOpp;
        OpportunityContactRole opprole = new OpportunityContactRole(opportunityID = testOpp.Id,contactId = testOpp.Contract_Signor__c,isPrimary = true);
        insert opprole;
        system.debug(opprole);
        smb_test_utility.createPQLI('PQL_Test_Data',testOpp.Id);
        Test.startTest();
        SMB_CPQQuoteHelper.createAgreement(testOpp.Id);
        SMB_CPQQuoteHelper.UpdateContractsToCancelled(testOpp.Id);
        Test.stopTest();
    }
    
    static testMethod void createAgreementTest2() {
        Opportunity testOpp   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp.Type = 'All Other Orders';
        insert testOpp;
        OpportunityContactRole opprole = new OpportunityContactRole(opportunityID = testOpp.Id,contactId = testOpp.Contract_Signor__c,isPrimary = true);
        insert opprole;
        system.debug(opprole);
        smb_test_utility.createPQLI('PQL_Test_Data',testOpp.Id);
        Test.startTest();
        SMB_CPQQuoteHelper.createVlocityAgreement(testOpp.Id);
        SMB_CPQQuoteHelper.createVlocityAgreement(testOpp.Id);
        Test.stopTest();
    }
}