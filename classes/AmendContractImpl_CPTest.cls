@isTest
public class AmendContractImpl_CPTest {
    static testmethod void unitTestOne(){
        TestDataHelper.testContractCreationwithDealSupport();
        //TestDataHelper.testAccountObj.ownerid=userinfo.getuserid();
        //update TestDataHelper.testAccountObj;
        TestDataHelper.testProductCreation_CP('Voice Plan');
        if(TestDataHelper.testContractLineItemList == null){
            TestDataHelper.testContractLineItemList =new list<vlocity_cmt__ContractLineItem__c>();
      TestDataHelper.testContractLineItemList.add(new vlocity_cmt__ContractLineItem__c(vlocity_cmt__RecurringTotal__c=9,vlocity_cmt__Product2Id__c=TestDataHelper.testProductObj2.id,vlocity_cmt__ContractId__c=TestDataHelper.testContractObj.id,Monthly_Variable_Total__c=10,vlocity_cmt__OneTimeTotal__c=12,name='test',vlocity_cmt__OneTimeCharge__c=2,vlocity_cmt__Quantity__c=12));
      TestDataHelper.testContractLineItemList.add(new vlocity_cmt__ContractLineItem__c(vlocity_cmt__RecurringTotal__c=6,vlocity_cmt__Product2Id__c=TestDataHelper.testProductObj2.id,vlocity_cmt__ContractId__c=TestDataHelper.testContractObj.id,Monthly_Variable_Total__c=8,vlocity_cmt__OneTimeTotal__c=6,name='test',vlocity_cmt__OneTimeCharge__c=3,vlocity_cmt__Quantity__c=12));
      TestDataHelper.testContractLineItemList.add(new vlocity_cmt__ContractLineItem__c(vlocity_cmt__RecurringTotal__c=3,vlocity_cmt__Product2Id__c=TestDataHelper.testProductObj2.id,vlocity_cmt__ContractId__c=TestDataHelper.testContractObj.id,Monthly_Variable_Total__c=7,vlocity_cmt__OneTimeTotal__c=5,name='test',vlocity_cmt__OneTimeCharge__c=4,vlocity_cmt__Quantity__c=12));
      insert  TestDataHelper.testContractLineItemList;
        }
        Id ChurnRecTypeIdFixed = Util.getRecordTypeIdByName('Agreement_Churn_Allotment__c', 'Fixed Amount For Contract Term');
        Id ChurnRecTypeIdPeriod = Util.getRecordTypeIdByName('Agreement_Churn_Allotment__c', 'Specified Amount For a Specific Period');
        
        Agreement_Churn_Allotment__c churnRec1 = new Agreement_Churn_Allotment__c(Allowed_Churn_Units__c = 10 ,Deal_Support__c = TestDataHelper.testDealSupport.Id, contract__c = TestDataHelper.testContractObj.id, RecordTypeId= ChurnRecTypeIdFixed) ;
        Agreement_Churn_Allotment__c churnRec2 = new Agreement_Churn_Allotment__c(Allowed_Churn_Units__c = 10 , contract__c = TestDataHelper.testContractObj.id,
                     Start_Date__c = Date.newInstance(2016, 1, 1),End_Date__c =   Date.newInstance(2016, 5, 30), RecordTypeId= ChurnRecTypeIdPeriod );  
        insert churnRec1;
        
        Id hardwareRecTypeIdFixed = Util.getRecordTypeIdByName('Agreement_Hardware_Allotment__c', 'Fixed Amount For Contract Term');
        Id hardwareRecTypeIdPeriod = Util.getRecordTypeIdByName('Agreement_Hardware_Allotment__c', 'Specified Amount For a Specific Period');
        Agreement_Hardware_Allotment__c hardwareRec1 = new Agreement_Hardware_Allotment__c(Allowed_Hardware_Units__c = 30 , contract__c = TestDataHelper.testContractObj.id, Deal_Support__c = TestDataHelper.testDealSupport.Id,  Type__c = 'Hardware Float', RecordTypeId = hardwareRecTypeIdFixed);
        Agreement_Hardware_Allotment__c hardwareRec2 = new Agreement_Hardware_Allotment__c(Allowed_Hardware_Units__c = 30 , Contract__c = TestDataHelper.testContractObj.id,Type__c = 'Hardware Float',
                     Start_Date__c = Date.newInstance(2016, 1, 1),End_Date__c =   Date.newInstance(2016, 5, 30),RecordTypeId= hardwareRecTypeIdPeriod);
    
     	insert hardwareRec1;
        
        
        list<vlocity_cmt__CustomFieldMap__c> cfmList = new list<vlocity_cmt__CustomFieldMap__c>();
        
        list<TestDataHelper.fieldMapWrapper> fmwList = new list<TestDataHelper.fieldMapWrapper>();
        fmwList.add(new TestDataHelper.fieldMapWrapper('TERM_OF_CONTRACT__c','TERM_OF_CONTRACT__c','Contract','offer_house_demand__c'));
        //fmwList.add(new TestDataHelper.fieldMapWrapper('TERM_OF_CONTRACT__c','TERM_OF_CONTRACT__c','Contract','offer_house_demand__c'));
        fmwList.add(new TestDataHelper.fieldMapWrapper('vlocity_cmt__Product2Id__c','Product__c','vlocity_cmt__ContractLineItem__c','Rate_Plan_Item__c'));
        fmwList.add(new TestDataHelper.fieldMapWrapper('vlocity_cmt__Product2Id__c','vlocity_cmt__ContractId__c','Contract__c','Rate_Plan_Item__c'));
        TestDataHelper.insertCustomFieldMappingData(fmwList);
        
       /* 
        vlocity_cmt__CustomFieldMap__c cfm1 = new vlocity_cmt__CustomFieldMap__c();
        cfm1.Name = 'Contract>Contract0';
        cfm1.vlocity_cmt__DestinationFieldName__c = 'TERM_OF_CONTRACT__c';
        cfm1.vlocity_cmt__SourceFieldName__c = 'TERM_OF_CONTRACT__c';
        cfm1.vlocity_cmt__SourceSObjectType__c = 'Contract';
        cfm1.vlocity_cmt__DestinationSObjectType__c = 'offer_house_demand__c';
        cfmList.add(cfm1);
        //insert cfm;
        
        vlocity_cmt__CustomFieldMap__c cfm2 = new vlocity_cmt__CustomFieldMap__c();
        cfm2.Name = 'vlocity_cmt__C>Rate_Plan_Item0';
        cfm2.vlocity_cmt__DestinationFieldName__c = 'Product__c';
        cfm2.vlocity_cmt__SourceFieldName__c = 'vlocity_cmt__Product2Id__c';
        cfm2.vlocity_cmt__SourceSObjectType__c = 'vlocity_cmt__ContractLineItem__c';
        cfm2.vlocity_cmt__DestinationSObjectType__c = 'Rate_Plan_Item__c';
       	cfmList.add(cfm2);
        
        vlocity_cmt__CustomFieldMap__c cfm3 = new vlocity_cmt__CustomFieldMap__c();
        cfm3.Name = 'vlocity_cmt__C>Rate_Plan_Item0';
        cfm3.vlocity_cmt__DestinationFieldName__c = 'Contract__c';
        cfm3.vlocity_cmt__SourceFieldName__c = 'vlocity_cmt__ContractId__c';
        cfm3.vlocity_cmt__SourceSObjectType__c = 'vlocity_cmt__ContractLineItem__c';
        cfm3.vlocity_cmt__DestinationSObjectType__c = 'Rate_Plan_Item__c';
       	cfmList.add(cfm3);
        
        vlocity_cmt__CustomFieldMap__c cfm3 = new vlocity_cmt__CustomFieldMap__c();
        cfm3.Name = 'vlocity_cmt__C>Rate_Plan_Item0';
        cfm3.vlocity_cmt__DestinationFieldName__c = 'Contract__c';
        cfm3.vlocity_cmt__SourceFieldName__c = 'vlocity_cmt__ContractId__c';
        cfm3.vlocity_cmt__SourceSObjectType__c = 'vlocity_cmt__ContractLineItem__c';
        cfm3.vlocity_cmt__DestinationSObjectType__c = 'Rate_Plan_Item__c';
       	cfmList.add(cfm3);
        
        
        insert cfmList;
        */
       
        Map<String,Object> inputMap = new   Map<String,Object>(); 
        inputMap.put('ContextId', TestDataHelper.testContractObj.Id);
        Map<String,Object> outMap =  new   Map<String,Object>();
        Map<String,Object> options = new   Map<String,Object>();
        test.startTest();  
        AmendContractImpl_CP classObj = new AmendContractImpl_CP();
        classObj.invokeMethod('AmendContractMethod',inputMap,outMap, Options);
        test.stopTest();
    }

}