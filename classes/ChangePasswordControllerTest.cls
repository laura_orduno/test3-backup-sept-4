@isTest
private class ChangePasswordControllerTest {
	public static testMethod void testChangePasswordController() {
        // Instantiate a new controller with all parameters in the page
        ChangePasswordController controller = new ChangePasswordController();
        controller.oldPassword = '123456';
        controller.newPassword = 'qwerty1'; 
        controller.verifyNewPassword = 'qwerty1';                
        
        System.assertEquals(null, controller.changePassword());            
        System.assertEquals(0, ApexPages.getMessages().size(), 'Errors:' + ApexPages.getMessages());
    }
}