/*****************************************************
    Name: ExternalEventService_RS_v1
    Usage: This class used to expose rest call to external systems to create multiple events in salesforce per call
    Author – Sandip Chaudhari (IBM)
    Revision History -
    Created By/Date - Sandip Chaudhari / 04 July 2016
    
    Updated By/Date/Reason for Change - 
    
******************************************************/
@RestResource(urlMapping='/CreateEvents/*')
global class  SFDCExternalEventsRSService_v1_0{
   
    @HttpPost 
    global static List<ExternalEventRequestResponseParameter.ResponseEventData> createEvents(List<ExternalEventRequestResponseParameter.RequestEventData> eventData){
       List<ExternalEventRequestResponseParameter.ResponseEventData> responseData =  ExternalEventService_Helper.processEvent(eventData);
       return responseData;
    }

}