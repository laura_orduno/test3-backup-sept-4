@isTest
public class PACContractRequestDetailController_Test{
    public static testmethod void testPACContractDtl(){
        Account acc = new Account(Name='Testing Software', BillingCountry = '703 carabob, ON, CAN, M1T 3N3', BillingState = 'IL');
        insert acc;
        List<Country_Province_Codes__c> lst = new List<Country_Province_Codes__c>();
        Country_Province_Codes__c cpcObj = new Country_Province_Codes__c();
        cpcObj.Name = 'CA';
        cpcObj.Type__c = 'Country';
        lst.add(cpcObj);
        Country_Province_Codes__c cpcObj1 = new Country_Province_Codes__c();
        cpcObj1.Name = 'ON';
        cpcObj1.Type__c = 'Canadian Province';
        lst.add(cpcObj1);
        insert lst;
        Opportunity op = new Opportunity(Name='TestOpp', StageName='Prospecting', CloseDate=System.today().addMonths(1), AccountId=acc.id);
		insert op;
        Contracts__c contObj = new Contracts__c();
        contObj.Contract_Billing_City__c = 'test';
        contObj.Account__c = acc.Id;
		contObj.Opportunity__c=op.id;
        contObj.Contract_Billing_Country__c = 'test';
        contObj.Contract_Billing_Postal_Code__c = 'test'; 
        contObj.Contract_Billing_Province__c = 'test';
        contObj.Contract_Billing_Street__c = 'test';
        insert contObj;
        test.startTest();
        PageReference page = new PageReference('/apex/PACContractRequestDetail'); 
        Test.setCurrentPage(page);
        ApexPages.standardController controller = new ApexPages.standardController(contObj);
        PACContractRequestDetailController obj = new PACContractRequestDetailController (controller);
        obj.updateBillingAddress();
        PACContractRequestDetailController.getCountryDescription('CA','ON');
        test.stopTest();
    }
}