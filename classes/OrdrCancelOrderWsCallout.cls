public class OrdrCancelOrderWsCallout 
{       
    public static final String WS_NAME = 'Cancel Customer Order'; 
    private static final String CANCEL_ORDER_END_POINT = 'CancelOrderEndpoint';
    private static final String APPEX_CLASS_AND_METHOD_NAME = 'CancelCustomerOrder_v2_1_vs0.OrdrCancelOrderWsCallout#cancelOrder';
           
    
    /**
    This method will be invoked by OrdrCancelOrderManager#cancelOrder() method
    */
    
    public static TpFulfillmentCustomerOrderV3.CustomerOrder cancelOrder(String externalOrderId)
    {
        System.debug('Entering OrdrCancelOrderWsCallout#cancelOrder() with the externalOrderId=' + externalOrderId);
        TpFulfillmentCustomerOrderV3.CustomerOrder customerOrder = null;
        if (String.isNotBlank(externalOrderId)) 
        { 
           System.debug(' externalOrderId@@length='+ externalOrderId.length());
           externalOrderId = externalOrderId.trim();
           try
           { 
            OrdrWsCancelCustomerOrder.CancelCustomerOrderSOAPPort servicePort = OrdrUtilities.updateSoapPort(new OrdrWsCancelCustomerOrder.CancelCustomerOrderSOAPPort(), CANCEL_ORDER_END_POINT);  
            customerOrder = servicePort.cancelCustomerOrder(prepareCustomerOrderPayload(externalOrderId));
           }
            catch (CalloutException e)
            {             
               // OrdrErrUtils.logCalloutError(null, externalOrderId, e, APPEX_CLASS_AND_METHOD_NAME, OrdrConstants.EXTERNAL_SYSTEM_NAME_NC, WS_NAME, OrdrConstants.OBJECT_NAME,null);                     
                throw e;                
            }   
        }
        else
        {  
            //should not allow externalOrderId be null or blank                      
           OrdrErrUtils.blankExternalOrderIdHandler('External orderId cannot be null or blank', APPEX_CLASS_AND_METHOD_NAME, WS_NAME);
        }       
        
        return customerOrder;

    } 
   
    
    private static TpFulfillmentCustomerOrderV3.CustomerOrder prepareCustomerOrderPayload(String orderId) 
    {
        TpFulfillmentCustomerOrderV3.CustomerOrder customerOrder = new TpFulfillmentCustomerOrderV3.CustomerOrder();
        List<TpCommonBaseV3.CharacteristicValue> characteristicValues = new List<TpCommonBaseV3.CharacteristicValue>();
        List<TpFulfillmentProductOrderV3.ProductOrderItem> productOrderItems = new List<TpFulfillmentProductOrderV3.ProductOrderItem>();
        
        TpCommonBaseV3.EntitySpecification specification = new TpCommonBaseV3.EntitySpecification();
        specification.Name = OrdrConstants.SPECIFICATION_NAME;
        specification.Type_x = OrdrConstants.SPECIFICATION_TYPE_X;
        specification.Category = OrdrConstants.SPECIFICATION_CATEGORY;
        customerOrder.Specification = specification;        
       
        TpCommonBaseV3.CharacteristicValue characteristicValue = new TpCommonBaseV3.CharacteristicValue();
       
        TpCommonBaseV3.Characteristic characteristic = new TpCommonBaseV3.Characteristic();
        characteristic.Name = OrdrConstants.SALES_ORDER_KEY;
        characteristicValue.Characteristic = characteristic;            
        characteristicValue.Value = new List<String>{orderId};       
        
        characteristicValues.add(characteristicValue);           
        
       // TpFulfillmentProductOrderV3.ProductOrderItem productOrderItem = new TpFulfillmentProductOrderV3.ProductOrderItem();
        //An empty Product tag needs to be provided under customerOrder/ProductOrderItem 
       // TpInventoryProductConfigV3.OrderedProduct product = new TpInventoryProductConfigV3.OrderedProduct();
       // productOrderItem.Product =  product;
        
       // productOrderItem.CharacteristicValue = characteristicValues;
       // productOrderItems.add(productOrderItem);         
        
        customerOrder.CharacteristicValue = characteristicValues;   
        
        return customerOrder;
    }        
}