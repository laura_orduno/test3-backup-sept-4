global with sharing  class WorkRequestCreation implements vlocity_cmt.VlocityOpenInterface2{
    public Boolean invokeMethod(String methodName, Map<String, Object> input, Map<String, Object> output, Map<String, Object> options)  
    {
        try {
            if (methodName.indexOf('createWorkRequest')!=-1) {
                Map<String,Object> workparamnode= (Map<String,Object>)input.get('workRequestParam');
                String orderId = (String)workparamnode.get('ContextId');
                String contractId = (String)workparamnode.get('ContractId');
                createWorkRequest(input,output,options);
                Boolean ignoreOrderModificationCheck=true;
                Map<String,Object> resp = GetAllExistingWorkRequestsOfOrder(contractId,orderId,output,ignoreOrderModificationCheck);
                if(resp.containsKey('WorkRequestDetails'))
                output.put('WorkRequestDetails',resp.get('WorkRequestDetails'));
                output.put('closedWorkRequestExistsRemote',resp.get('closedWorkRequestExists'));
                output.put('IsWorkrequestExists',resp.get('IsWorkrequestExists'));
                output.put('closedWorkRequestStatus',resp.get('closedWorkRequestStatus'));
                System.debug('output' +output);
            }
            else if (methodName.indexOf('GetWorkRequestsOfOrder')!=-1 ){
                Map<String,Object> workparamnode= (Map<String,Object>)input.get('workRequestParam');
                String orderId = (String)workparamnode.get('ContextId');
                String contractId = (String)workparamnode.get('ContractId');
                Boolean ignoreOrderModificationCheck=false;
                Map<String,Object> resp = GetAllExistingWorkRequestsOfOrder(contractId,orderId,output,ignoreOrderModificationCheck);
                if(resp.containsKey('WorkRequestDetails'))
                output.put('WorkRequestDetails',resp.get('WorkRequestDetails'));
                output.put('closedWorkRequestExists',resp.get('closedWorkRequestExists'));
                output.put('IsWorkrequestExists',resp.get('IsWorkrequestExists'));
                output.put('closedWorkRequestStatus',resp.get('closedWorkRequestStatus'));
                system.debug('output '+JSON.serializePretty(output));
            }
        }catch(exception e){
            System.debug(LoggingLevel.ERROR, 'Exception is '+e);
            System.debug(LoggingLevel.ERROR, 'Exception stack trace '+e.getStackTraceString());
        }
       
        return false;
    }
    
    
    private Map<String, Object> GetAllExistingWorkRequestsOfOrder(String ContractId,String orderId,Map<String, Object> output,Boolean ignoreOrderModificationCheck)
    {
       
                String closedWorkRequestStatus = 'NONE';
                Boolean closedWorkRequestExists = false; 
                boolean workexistsvalue = false;
                List<Work_Request__c> wrList = new List<Work_Request__c>();
                Map<String,object> responseMap = new Map<String, Object>(); 
                List<Contract> contractList=null;
                List<OrderItem> oiList=null;
            try {
                
                system.debug('inside GetAllExistingWorkRequestsOfOrder...');
                contractList=[select id,status,(select OrderLineItemId__c ,id,term__c from vlocity_cmt__ContractLineItems__r) from contract where id=: ContractId];
                
                oiList=[select term__c,id from orderitem where order.parentid__c=:orderId];
                wrList = [SELECT Id, Name, Status__c, Owner.Name, Owner.Id, 
                          LastModifiedDate,Description__c, 
                          eContract__r.Quality_Check_Complete__c 
                          FROM Work_Request__c WHERE eContract__c =: ContractId 
                          ORDER BY LastModifiedDate DESC limit 1];
                     Boolean isOrderModified=false;
                Set<String> oiIdSet=new Set<String>();
                Set<String> assetRefIdsSet=new Set<String>();
                Map<String,String> oiIdMap=new Map<String,String>();
                Map<String,String> assetRefIdsMap=new Map<String,String>();
                if(oiList!=null && !oiList.isEmpty()){
                    for(OrderItem oiInstant:oiList){
                        
                        oiIdSet.add(String.valueOf(oiInstant.id).substring(0,15));
                        oiIdMap.put(String.valueOf(oiInstant.id).substring(0,15),oiInstant.term__c);
                    }                  
                }
             if(contractList!=null && !contractList.isEmpty()){
                 for(Contract coInstant:contractList){
                     if(coInstant.vlocity_cmt__ContractLineItems__r!=null){
                         for(vlocity_cmt__ContractLineItem__c cl:coInstant.vlocity_cmt__ContractLineItems__r){
                             if(String.isNotBlank(cl.OrderLineItemId__c) && cl.OrderLineItemId__c instanceof Id){
                                 assetRefIdsSet.add(String.valueOf(cl.OrderLineItemId__c).substring(0,15));
                                 assetRefIdsMap.put(String.valueOf(cl.OrderLineItemId__c).substring(0,15),cl.Term__c);
                             }
                         }
                     }
                 }
             }
                System.debug('assetRefIdsSet='+assetRefIdsSet);
                System.debug('oiIdSet='+oiIdSet);
                Set<String> oiIdSetCp=new Set<String>();
                oiIdSetCp.addAll(oiIdSet);
                oiIdSetCp.removeAll(assetRefIdsSet);
                if(!oiIdSetCp.isEmpty()){
                    //isOrderModified=true;
                }
                
                Set<String> assetRefIdsSetCp=new Set<String>();
                assetRefIdsSetCp.addAll(assetRefIdsSet);
                assetRefIdsSetCp.removeAll(oiIdSet);

                  if(!assetRefIdsSetCp.isEmpty()){
                   // isOrderModified=true;
                } 
                for(String assetRefId:assetRefIdsSet){
                    String oiTerm=oiIdMap.get(assetRefId);
                    String clTerm=assetRefIdsMap.get(assetRefId);
                    if(oiTerm!=clTerm){
                     isOrderModified=true;   
                    }
                }
                if(ignoreOrderModificationCheck){
                    isOrderModified=false;
                }
                if(isOrderModified){
                    workexistsvalue=false;
                   /* if( wrList.size() > 0 ) {
                        for(Work_Request__c wrkInst:wrList){
                            wrkInst.eContract__c=null;
                        }
                        update wrList;
                        wrList=new List<Work_Request__c>();
                    }*/
                }
                System.debug('debugging isOrderModified='+isOrderModified);
                //Change by Jaya to check for Work Request status
                //if(wrList.size() > 0 && wrList[0] != null && wrList[0].eContract__r.Quality_Check_Complete__c == false){
                if( wrList.size() > 0 && wrList[0] != null && !isOrderModified) {                         
                             if(wrList[0].Status__c == 'Closed')
                             {                                 
                                 closedWorkRequestExists = true;
                                 closedWorkRequestStatus = 'YES';
                             }
                             else{                               
                                closedWorkRequestStatus = 'NO';
                             }
                             
                            
                             workexistsvalue = true;
                        
                             //Added by Jaya for quality check flag
                             //Removing QUality check complete for WR due to V100 bug
                             // Before v100, update api was not creating new Version. Now there is new version with blank attachment so Quality_Check_Complete__c is always cleared.
                             //Adharsh: reverting back Summer17 bug workaround changes. 
                             
                             if(workexistsvalue == true && closedWorkRequestExists == true && wrList[0].eContract__r.Quality_Check_Complete__c == false ){             
                                 workexistsvalue = false;       //'NOWR'    
                                 closedWorkRequestStatus = 'NOWR';  
                                 wrList = new List<Work_Request__c>();
                             }
                             //Adharsh:Comment end
                             
                         responseMap.put('WorkRequestDetails',wrList);  
                } 
                else if(workexistsvalue == false){
                   
                        closedWorkRequestStatus = 'NOWR';       //'NOWR'
                }   
                
                //Added by Jaya for assigning the Work request details node if the Quality check is true.
                if(wrList.size() > 0 && wrList[0] != null && wrList[0].eContract__r.Quality_Check_Complete__c == true && !isOrderModified){
                   responseMap.put('WorkRequestDetails',wrList); 
                }
                    
                    
                 responseMap.put('closedWorkRequestStatus',closedWorkRequestStatus);              
                 responseMap.put('closedWorkRequestExists',closedWorkRequestExists);
                 responseMap.put('IsWorkrequestExists' , workexistsvalue);
                 system.debug('workexistsvalue'+ workexistsvalue);
                 

            }catch(exception e){
                System.debug('***** Error *** ' );
                System.debug(LoggingLevel.ERROR, 'Exception is '+e);
                System.debug(LoggingLevel.ERROR, 'Exception stack trace '+e.getStackTraceString());
                return  null;
            }
       return  responseMap;
    }
   
    private void createWorkRequest(Map<String, Object> input, Map<String, Object> output, Map<String, Object> options){
        try{
            
            //Adharsh: TODO :: send only the required parameters from OS
           //Adharsh: Modifying the code to capture the values from right node.
           //Get Order Id
           
            Map<String,Object> workparamnode= (Map<String,Object>)input.get('workRequestParam');
            String orderId = (String)workparamnode.get('ContextId');
           
            //Get Contract ID
            String contractId = (String)workparamnode.get('ContractId');
            //Get Contract Request ID
            String contractRequestId=(String)workparamnode.get('ContractRequestID');
        
           // Adharsh: Commenting out the varaible assignment
            /*String orderId = (String)input.get('orderId');
            String contractId = (String)input.get('contractId');
            String contractRequestId=(String)input.get('contractRequestId');*/
            
            
            /*if(String.isBlank(orderId)){
                orderId='801c0000000B31tAAC';
            }
            if(String.isBlank(contractId)){
                contractId='800c0000000T3RNAA0';
            }
            if(String.isBlank(contractRequestId)){
                contractRequestId='a5Oc00000000mEOEAY';
            }*/
           
            Work_Request__c wreq = new Work_Request__c();
            
            RecordType rt = [SELECT Id, Name, DeveloperName FROM RecordType WHERE SObjectType = 'Work_Request__c' AND DeveloperName = 'Contracts'];
             if(rt!=null){
                wreq.recordtypeId = rt.Id;  
                
            }
            
            Order ordObj=[select ownerId,accountId,opportunityId from Order where id=:orderId];
           
            if(ordObj != null){
                
                wreq.OwnerId = ordObj.ownerId;
                wreq.Account__c=ordObj.accountId;
                wreq.Opportunity__c=ordObj.opportunityId;
                
            }
            if(String.isNotBlank(contractRequestId)){
             
                wreq.Contract__c=contractRequestId;
            }
            
            wreq.Type__c='Contract Validation';
            if(String.isNotBlank(contractId)){
                
                wreq.eContract__c=contractId;
            }
                
            wreq.Order__c = orderId;
            wreq.Reminder__c='REMINDER: for any pricing requests, service prequalifications or contract builds, a BSA will require: \n - Product Breakdowns (service type and speeds) \n - Pricing \n - All service /site addresses  \n - Onsite contacts \n \n For contracts: \n - Is it new or replacement?Do ';
            wreq.Description__c = (string)options.get('Comments');    
            wreq.Status__c = 'New';    
                     
            
            //Added by Jaya - putting the result of the insert in saveresult
            Database.SaveResult wreqResult = Database.insert(wreq,false);
            System.debug('***** work req create ***** ' + wreq.id);
        }catch(exception e)
        {
            System.debug('***** Error ***** ' + e.getMessage());
            String errorMessage=e.getMessage();
            output.put('error',errorMessage);
        }
    }
    public void createTaskNotify(Map<String, Object> input, Map<String, Object> output, Map<String, Object> options) {
        String ordId = (String)input.get('ContextId');
        Group ownerGroup = [select Id from group where DeveloperName = 'SCA_Support' limit 1];
        String defaultOwnerStr='';
        if(ownerGroup != null){
            defaultOwnerStr = ownerGroup.Id;
        }
        system.debug('defaultOwnerStr '+defaultOwnerStr);
        String taskRecordTypeStr='BCX_General_Task';
        String orderManagerRoleStr='Order Manager';
        String taskOwnerId;
        User ownerDefault;
        RecordType taskRecordType;
        try{
            taskRecordType = [select Id, DeveloperName from RecordType where SObjectType = 'Task' and DeveloperName = : taskRecordTypeStr and IsActive = true limit 1];
        }
        catch (Exception e) {
            System.debug('createTaskNotify: Query Task record type: received exception: Task Record type error' + e.getMessage());
        }
        try{
           
            ownerDefault = [select Id from User where name = : defaultOwnerStr limit 1];
            if(ownerDefault != null){
                taskOwnerId = ownerDefault.Id;
                
            }
            else{
                taskOwnerId = id.valueof('');
                
            }
        }
        catch (Exception e) {
            
            System.debug('createTaskNotify: Query Task Owner: received exception: ' + e.getMessage());
        }
        Task taskRec = new Task();
        if(taskRecordType != null)
            taskRec.RecordTypeId = taskRecordType.Id;
        taskRec.Priority = 'Normal';
        taskRec.Activity_Type__c = 'Task';
        taskRec.Status = 'New';
        taskRec.WhatId = ordId;
        taskrec.Subject = 'Order amended';
        Id tskOwnId =  taskOwnerId;
        taskRec.OwnerId = tskOwnId;
        insert taskRec;
        system.debug('createTaskNotify:Created task:'+taskRec);
       
    }
}