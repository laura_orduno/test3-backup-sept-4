@isTest
private class trac_SMETProjectReqQuickAddExtTest {
private static testMethod void testCtor() {
        SMET_Project__c p = new SMET_Project__c(name = 'test', Impact__c = 5, Complexity__c = 5);
        insert p;
        
        SMET_Requirement__c r = new SMET_Requirement__c(
        	Name = 'Test', Priority__c = 'High',
        	Requirement_Detail__c = 'Test details',
        	Related_SMET_Project__c = p.Id,
        	order__c = 0
        );
        insert r;
        
        trac_SMETProjectReqQuickAddExt ctlr = new trac_SMETProjectReqQuickAddExt(new ApexPages.StandardController(p));
        
        system.assertEquals(1, ctlr.cart.size());
        system.assertEquals(r.id, ctlr.cart.get(0).id);
    }
    
    private static testMethod void testCtorNoExistingRequirements() {
        SMET_Project__c p = new SMET_Project__c(name = 'test', Impact__c = 5, Complexity__c = 5);
        insert p;
        
        trac_SMETProjectReqQuickAddExt ctlr = new trac_SMETProjectReqQuickAddExt(new ApexPages.StandardController(p));
        
        system.assertEquals(1, ctlr.cart.size());
        system.assertEquals(null, ctlr.cart.get(0).id);
    }
    
    private static testMethod void testAddItem() {
        		
		SMET_Project__c p = new SMET_Project__c(name = 'test', Impact__c = 5, Complexity__c = 5);
        insert p;
        
        SMET_Requirement__c r = new SMET_Requirement__c(
        	Name = 'Test', Priority__c = 'High',
        	Requirement_Detail__c = 'Test details',
        	Related_SMET_Project__c = p.Id,
        	order__c = 0
        );
        insert r;
        
        trac_SMETProjectReqQuickAddExt ctlr = new trac_SMETProjectReqQuickAddExt(new ApexPages.StandardController(p));
        
        ctlr.addItem();
        
        system.assertEquals(2, ctlr.cart.size());
        system.assertEquals(r.id, ctlr.cart.get(0).id);
        system.assertEquals(null, ctlr.cart.get(1).id);

    }
    
    private static testMethod void testAddDefaultItems() {
        insert new CloudProjectKeyDeliverableTemplate__c(Name = 'Test', KD_Name__c = 'High');
		
		SMET_Project__c p = new SMET_Project__c(name = 'test', Impact__c = 5, Complexity__c = 5);
        insert p;
        
        SMET_Requirement__c r = new SMET_Requirement__c(
        	Name = 'Test', Priority__c = 'High',
        	Requirement_Detail__c = 'Test details',
        	Related_SMET_Project__c = p.Id,
        	order__c = 0
        );
        insert r;
        
        trac_SMETProjectReqQuickAddExt ctlr = new trac_SMETProjectReqQuickAddExt(new ApexPages.StandardController(p));
        
        ctlr.addItem();
        
        system.assertEquals(2, ctlr.cart.size());
        system.assertEquals(r.id, ctlr.cart.get(0).id);
        system.assertEquals(null, ctlr.cart.get(1).id);
        
        ctlr.addDefaultItems();
        system.assertEquals(3, ctlr.cart.size());
    }
    
    private static testMethod void testRemoveFromCart() {
        SMET_Project__c p = new SMET_Project__c(name = 'test', Impact__c = 5, Complexity__c = 5);
        insert p;
        
        SMET_Requirement__c r1 = new SMET_Requirement__c(
        	Name = 'Test1',
        	Priority__c = 'High',
        	Requirement_Detail__c = 'Test details1',
        	Related_SMET_Project__c = p.Id,
        	order__c = 0
        );
        SMET_Requirement__c r2 = new SMET_Requirement__c(
        	Name = 'Test2',
        	Priority__c = 'High',
        	Requirement_Detail__c = 'Test details2',
        	Related_SMET_Project__c = p.Id,
        	order__c = 1
        );
        
        insert new List<SMET_Requirement__c>{ r1, r2 };
        
        trac_SMETProjectReqQuickAddExt ctlr = new trac_SMETProjectReqQuickAddExt(new ApexPages.StandardController(p));
        
        ctlr.itemToDelete = 1;
				ctlr.removeFromCart();
				
				system.assertEquals(1,ctlr.cart.size());
				
				ctlr.itemToDelete = 0;
				ctlr.removeFromCart();
				
				system.assertEquals(1,ctlr.cart.size());
				system.assertEquals(null,ctlr.cart.get(0).id); // new blank record added automatically
    }
    
    private static testMethod void testUndo() {
    	SMET_Project__c p = new SMET_Project__c(name = 'test', Impact__c = 5, Complexity__c = 5);
        insert p;
        
	    SMET_Requirement__c r = new SMET_Requirement__c(
	    	Name = 'Test',
	    	Priority__c = 'High',
	    	Requirement_Detail__c = 'Test details',
	    	Related_SMET_Project__c = p.Id,
	    	order__c = 0
	    );
	    insert r;
	    
	    trac_SMETProjectReqQuickAddExt ctlr = new trac_SMETProjectReqQuickAddExt(new ApexPages.StandardController(p));
        
      ctlr.itemToDelete = 0;
			ctlr.removeFromCart();
			
			system.assertEquals(null, ctlr.cart.get(0).id);
			
			ctlr.undo();
			
			system.assertEquals(r.id, ctlr.cart.get(0).id);
    }
    
    private static testMethod void testOnSave() {
        SMET_Project__c p = new SMET_Project__c(name = 'test', Impact__c = 5, Complexity__c = 5);
        insert p;
        
        SMET_Requirement__c r1 = new SMET_Requirement__c(
        	Name = 'Test1',
        	Priority__c = 'High',
        	Requirement_Detail__c = 'Test details1',
        	Related_SMET_Project__c = p.Id,
        	order__c = 0
        );
        SMET_Requirement__c r2 = new SMET_Requirement__c(
        	Name = 'Test2',
        	Priority__c = 'High',
        	Requirement_Detail__c = 'Test details2',
        	Related_SMET_Project__c = p.Id,
        	order__c = 1
        );
        
        insert new List<SMET_Requirement__c>{ r1, r2 };
        
        trac_SMETProjectReqQuickAddExt ctlr = new trac_SMETProjectReqQuickAddExt(new ApexPages.StandardController(p));
        
        ctlr.itemToDelete = 1;
				ctlr.removeFromCart();
				
				ctlr.addItem();
				ctlr.cart.get(1).name = 'new';
				ctlr.cart.get(1).order__c = 1;
				
				ctlr.onSave();
				
				system.assertEquals(false,ApexPages.hasMessages());
				
				Map<Id,SMET_Requirement__c> reqs = new Map<Id,SMET_Requirement__c>(
					[SELECT name FROM SMET_Requirement__c WHERE Related_SMET_Project__c = :p.id]
				);
				
				system.assertEquals(2,reqs.size());
				system.assert(reqs.containsKey(r1.id));
				system.assertEquals(false,reqs.containsKey(r2.id));
				
				for(Id key : reqs.keyset()) {
					if (key != r1.id) {
						system.assertEquals('new',reqs.get(key).name);
						break;
					}
				}
    }
    
    private static testMethod void testOnSaveException() {
        SMET_Project__c p = new SMET_Project__c(name = 'test', Impact__c = 5, Complexity__c = 5);
        insert p;
        
        SMET_Requirement__c r = new SMET_Requirement__c(
        	Name = 'Test',
        	Priority__c = 'High',
        	Requirement_Detail__c = 'Test details',
        	Related_SMET_Project__c = p.Id,
        	order__c = 0
        );        
        insert r;
        
        trac_SMETProjectReqQuickAddExt ctlr = new trac_SMETProjectReqQuickAddExt(new ApexPages.StandardController(p));
				
				Test.setReadOnlyApplicationMode(true);
				
				ctlr.onSave();
				
				system.assert(ApexPages.hasMessages());
				system.assert(ApexPages.getMessages()[0].getSummary().startsWith('Upsert failed.'));
    }
    
    private static testMethod void testOnSaveDeletionException() {
        SMET_Project__c p = new SMET_Project__c(name = 'test', Impact__c = 5, Complexity__c = 5);
        insert p;
        
        SMET_Requirement__c r1 = new SMET_Requirement__c(
        	Name = 'Test1',
        	Priority__c = 'High',
        	Requirement_Detail__c = 'Test details1',
        	Related_SMET_Project__c = p.Id,
        	order__c = 0
        );
        SMET_Requirement__c r2 = new SMET_Requirement__c(
        	Name = 'Test2',
        	Priority__c = 'High',
        	Requirement_Detail__c = 'Test details2',
        	Related_SMET_Project__c = p.Id,
        	order__c = 1
        );
        
        insert new List<SMET_Requirement__c>{ r1, r2 };
        
        trac_SMETProjectReqQuickAddExt ctlr = new trac_SMETProjectReqQuickAddExt(new ApexPages.StandardController(p));
        
        ctlr.itemToDelete = 1;
				ctlr.removeFromCart();
				
				delete r2;
				
				ctlr.onSave();
				
				system.assert(ApexPages.hasMessages());
				system.assertEquals('Record deleted',ApexPages.getMessages()[0].getSummary());
    }
}