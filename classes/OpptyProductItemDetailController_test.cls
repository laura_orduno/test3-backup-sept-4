/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 * Name - OpptyProductItemDetailController_test
 * 1. Created By - Sandip - 06 Feb 2016
 * 2. Modified By 
 */
@isTest
private class OpptyProductItemDetailController_test{
    static testMethod void runTestCase() {
        test.startTest();
        
        
        
        Opportunity Oppt = FOBO_Test_Util.createOpportunity();
        Contact c = FOBO_Test_Util.createContactFromAccount(Oppt.AccountId);
        Product2 prod = new Product2(Name = 'Laptop X200',
                                     Family = 'Hardware',
                                     isActive = true);
        insert prod;
        Opp_Product_Item__c opptyProd = new Opp_Product_Item__c();
        opptyProd.Product__c = prod.Id;
        opptyProd.Opportunity__c = Oppt.Id;
        opptyProd.City__c = 'Markham';
        opptyProd.State_Province__c = 'ON';
        opptyProd.Country__c = 'CAN';
        opptyProd.Postal_Code__c = 'M1T 3N3';
        opptyProd.Suite_Unit__c = '703';
        opptyProd.Street__c = 'Carabob';
        opptyProd.Street_Direction__c = 'N';
        opptyProd.Is_SACG__c = true;
        opptyProd.Notes_for_SACG_team__c = 'Notes';
        opptyProd.New_Construction__c = System.Label.PACNewConstruction;
        opptyProd.Temporary_Job_Phone__c = System.Label.PACTempJobPhome;
        insert opptyProd;
        
        SMB_SACGSettings__c mp = new SMB_SACGSettings__c(Name='ContactIds', Data__c = c.Id);
        SMB_SACGSettings__c mp1 = new SMB_SACGSettings__c(Name='OrgWideEmailAddressId', Data__c = '0D240000000CbJi');
        SMB_SACGSettings__c mp2 = new SMB_SACGSettings__c(Name='TemplateId', Data__c = '00X40000001546Q');
        List<SMB_SACGSettings__c> lstmp = new List<SMB_SACGSettings__c>();
        lstmp.add(mp);
        lstmp.add(mp1);
        lstmp.add(mp2);
        try{
            insert lstmp;
        }catch(Exception ex){}
        PageReference page = new PageReference('/apex/OpportunityProductItemDetail'); 
        page.getParameters().put('Id', opptyProd.Id);
        Test.setCurrentPage(page);
        ApexPages.standardController controller = new ApexPages.standardController(opptyProd);
        OpportunityProductItemDetailController obj = new OpportunityProductItemDetailController(controller);
        
        obj.SAData.contactId = c.Id;
        obj.SAData.accountId = Oppt.AccountId;
        obj.updateServiceLocation();
        
        opptyProd.Service_Address_Status__c = System.Label.PACInValidCaptureNewAddrStatus;
        opptyProd.New_Construction__c = '';
        opptyProd.Temporary_Job_Phone__c = '';
        opptyProd.Is_SACG__c = false;
        obj.updateServiceLocation();
        test.stopTest();             
    }
    
}