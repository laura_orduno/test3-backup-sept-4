public class LynxCreateTicket {

	public class sendRequest {
		public LynxCreateTicket.sendRequest SRequest(string description, string comments, string projectNum, string casenumber, string firstName, string lastName, string phonenumber, string email, ID CaseId, string ticketType, string condType, string AccessType, string repByPhone, string repName) {
			/* if(Test.isRunningTest()){
				return null;
			 }	*/

			// Create the request envelope
			DOM.Document doc = new DOM.Document();
			//String endpoint = 'https://soa-mp-toll-st01.tsl.telus.com:443/SMO/ProblemMgmt/LynxTroubleTicketMgmtService_v1_0_vs0';
			//String endpoint = 'https://xmlgwy-pt1.telus.com:9030/st01/SMO/ProblemMgmt/LynxTroubleTicketMgmtService_v1_0_vs0';
			string endpoint = '';

			// TODO: Re-leverage custom setting for endpoint, no hard-coding
			if (!Test.isRunningTest()) {
				endpoint = SMBCare_WebServices__c.getInstance('TTODS_Endpoint_Create_Lynx').Value__c;
			} else {
				endpoint = 'https://xmlgwy.telus.com:9030/SMO/ProblemMgmt/LynxTroubleTicketMgmtService_v1_0_vs0';
			}

			//String endpoint = 'https://xmlgwy.telus.com:9030/SMO/ProblemMgmt/LynxTroubleTicketMgmtService_v1_0_vs0';
			////////////
			String soapNS = 'http://schemas.xmlsoap.org/soap/envelope/';
			string ebon = 'http://ebonding.telus.com';
			string asr = 'http://assurance.ebonding.telus.com';
			datetime creationDateTime = datetime.now();
			date mydate = creationDateTime.date();
			datetime getmytime = datetime.now();
			time mytime = getmytime.time();
			string sourceTypeCode = '';
			string lineOfBusinessCode = '';
			string troubleTypeCode = '';
			string level1 = '';
			string level2 = '';
			string testTime = string.valueOf(mydate) + 'T' + string.valueOf(mytime);
			string createdate = string.valueOfGmt(creationDateTime);
			string logicalId = 'TELUS-SFDC';
			string messageId = logicalId + creationDateTime;
			String encodemessageId = EncodingUtil.base64Encode(Blob.valueOf(messageId));

			/*if (projectNum != '')
			{
				projectNum = 'CPS' + projectNum;
			}*/

			if (casenumber != '') {
				//casenumber = casenumber + 'New Salesforce case created';
				casenumber = casenumber;
			}
			if (ticketType == 'Mainstream') {
				sourceTypeCode = 'TPS';
				lineOfBusinessCode = 'Mainstream';
				troubleTypeCode = 'Mainstream - Data';
			}
			if (ticketType == 'Enhanced') {
				sourceTypeCode = 'TPS';
				lineOfBusinessCode = 'Enhanced';
				troubleTypeCode = 'NETWORK';
			}
			if (ticketType == 'Voice') {
				sourceTypeCode = 'TPS';
				lineOfBusinessCode = 'Voice';
				troubleTypeCode = 'BASIC';
				level1 = 'No Dial Tone';
				level2 = 'Incoming OK? - No';
			}
			if (ticketType == 'Voice-PRI') {
				sourceTypeCode = 'TPS';
				lineOfBusinessCode = 'Voice';
				troubleTypeCode = 'BASIC';
				level1 = 'No Dial Tone';
				level2 = 'Incoming OK? - No';
			}
			if (ticketType == 'L1L') {
				sourceTypeCode = 'OTHER TELCO';
				lineOfBusinessCode = 'Voice';
				troubleTypeCode = 'CIRCUIT';
				level1 = 'L1L Circuit';
				level2 = 'Not Required';
			}
			dom.XmlNode envelope
					= doc.createRootElement('Envelope', soapNS, 'soapenv');
			envelope.setNamespace('ebon', ebon);
			envelope.setNamespace('asr', asr);

			dom.XmlNode header = envelope.addChildElement('Header', soapNS, null);
			dom.XmlNode eBondingheader = header.addChildElement('eBondingHeader', ebon, 'ebon');
			dom.XmlNode sender = eBondingheader.addChildElement('sender', ebon, null);
			sender.addChildElement('logicalId', ebon, null).
					addTextNode(logicalId);
			sender.addChildElement('component', ebon, null).
					addTextNode('');
			sender.addChildElement('referenceId', ebon, null).
					addTextNode('');
			sender.addChildElement('confirmation', ebon, null).
					addTextNode('Always');
			eBondingheader.addChildElement('creationDateTime', ebon, null).
					addTextNode(testTime);
			eBondingheader.addChildElement('messageId', ebon, null).
					addTextNode(encodemessageId);
			dom.XmlNode credentials = eBondingheader.addChildElement('credentials', ebon, null);
			credentials.addChildElement('userName', ebon, null).
					addTextNode('');
			credentials.addChildElement('password', ebon, null).
					addTextNode('');
			dom.XmlNode businessServiceRequest = eBondingheader.addChildElement('businessServiceRequest', ebon, null);
			businessServiceRequest.addChildElement('service', ebon, null).
					addTextNode('AssuranceService');
			businessServiceRequest.addChildElement('operation', ebon, null).
					addTextNode('AddTicket');
			dom.XmlNode body = envelope.addChildElement('Body', soapNS, null);
			dom.XmlNode AddTicket = body.addChildElement('AddTicket', asr, 'asr');
			AddTicket.addChildElement('customerTicketId', asr, null).
					addTextNode(casenumber);
			AddTicket.addChildElement('description', asr, null).
					addTextNode(description);
			AddTicket.addChildElement('sourceTypeCode', asr, null).
					addTextNode(sourceTypeCode);
			AddTicket.addChildElement('severityCode', asr, null).
					addTextNode('3 - MINOR');
			AddTicket.addChildElement('priorityCode', asr, null).
					addTextNode(condType);
			AddTicket.addChildElement('accessInformation', asr, null).
					addTextNode(AccessType);
			AddTicket.addChildElement('comments', asr, null).
					addTextNode(comments);
			AddTicket.addChildElement('contactMode', asr, null).
					addTextNode('Self Serve');
			dom.XmlNode troubleClassification = AddTicket.addChildElement('troubleClassification', asr, 'asr');
			troubleClassification.addChildElement('lineOfBusinessCode', asr, null).
					addTextNode(lineOfBusinessCode);
			troubleClassification.addChildElement('troubleTypeCode', asr, null).
					addTextNode(troubleTypeCode);
			troubleClassification.addChildElement('level1', asr, null).
					addTextNode(level1);
			troubleClassification.addChildElement('level2', asr, null).
					addTextNode(level2);
			dom.XmlNode customerSubscriptionIncomingTicket = AddTicket.addChildElement('customerSubscriptionIncomingTicket', asr, 'asr');
			dom.XmlNode customerSubscriptionIdentification = customerSubscriptionIncomingTicket.addChildElement('customerSubscriptionIdentification', asr, null);
			customerSubscriptionIdentification.addChildElement('customerSubscriptionIdTypeCode', asr, null).
					addTextNode('SIEBEL');
			customerSubscriptionIdentification.addChildElement('customerSubscriptionIdValue', asr, null).
					addTextNode(projectNum);
			dom.XmlNode contact = AddTicket.addChildElement('contact', asr, 'asr');
			contact.addChildElement('contactType', asr, null).
					addTextNode('Reported By');
			dom.XmlNode name = contact.addChildElement('name', asr, 'asr');
			name.addChildElement('lastName', asr, null).
					addTextNode('');
			name.addChildElement('firstName', asr, null).
					addTextNode(repName);
			dom.XmlNode telecommunicationAddress = contact.addChildElement('telecommunicationAddress', asr, 'asr');
			telecommunicationAddress.addChildElement('telecommunicationAddressUsageType', asr, null).
					addTextNode('CBR');
			telecommunicationAddress.addChildElement('telephoneNumber', asr, null).
					addTextNode(repByPhone);
			dom.XmlNode electronicAddress = contact.addChildElement('electronicAddress', asr, 'asr');
			electronicAddress.addChildElement('electronicAddressUsageType', ebon, 'ebon').
					addTextNode('EMAIL');
			electronicAddress.addChildElement('electronicAddressValue', ebon, 'ebon').
					addTextNode(email);
			dom.XmlNode partnerData = AddTicket.addChildElement('partnerData', asr, 'asr');
			partnerData.addChildElement('gatewayReceivedTimeStamp', asr, null).
					addTextNode(testTime);
			partnerData.addChildElement('externalSourcedInfo', asr, null).
					addTextNode(description);
			partnerData.addChildElement('failedProvisioningIndicator', asr, null).
					addTextNode('N');
			System.debug(doc.toXmlString());

			// TODO: add exception handling

			// Send the request
			//try{
			HttpRequest req = new HttpRequest();
			req.setMethod('POST');
			req.setEndpoint(endpoint);

			// TODO: specify a timeout of 2 minutes
			// I.E. req.setTimeout(milliseconds);

			String creds = '';

			if (!Test.isRunningTest()) {
				SMBCare_WebServices__c wsUsername = SMBCare_WebServices__c.getValues('username');
				SMBCare_WebServices__c wsPassword = SMBCare_WebServices__c.getValues('password');
				if (String.isNotBlank(wsUsername.Value__c) && String.isNotBlank(wsPassword.Value__c))
					creds = wsUsername.Value__c + ':' + wsPassword.Value__c;
			} else {
				creds = 'APP_SFDC:soaorgid';
			}

			String encodedusernameandpassword = EncodingUtil.base64Encode(Blob.valueOf(creds));

			//Blob headerValue = Blob.valueOf(encodedusernameandpassword);
			String authorizationHeader = 'BASIC ' + encodedusernameandpassword;
			// EncodingUtil.base64Encode(headerValue);
			req.setHeader('Authorization', authorizationHeader);
			req.setHeader('Content-Type', 'text/xml');

			req.setBodyDocument(doc);

			// TODO: Determine the level of detail webservice is returning in regards to errors.

			Http http = new Http();
			HttpResponse res = new HttpResponse();
			try {
				if (!Test.isRunningTest()) {
					res = http.send(req);
				} else {
					res = ENTPUtils.mockWebserviceResponse(ENTPUtils.LYNX_XMLCREATE_RESPONSE);
				}
				//HttpResponse res = http.send(req);
				System.debug('HttpResponse res:' + res.getStatusCode());
				//System.assertEquals(500, res.getStatusCode());
				if (res.getStatusCode() != 200) {
					try {
						Ticket_Event__c NewEvent = new Ticket_Event__c(
								Case_Number__c = caseNumber,
								Event_Type__c = 'New Ticket',
								Status__c = 'New',
								Failure_Reason__c = 'Ticket request returned an error.' + res.getStatusCode() + ' - ' + res.getBody(),                             //Lynx_Ticket_Number__c = ticketNum,
								Case_Id__c = CaseId
						);
						System.debug('LynxTicketCreate->NewEvent, CaseNumber(' + caseNumber + ')');
						insert NewEvent;

					} catch (DmlException e) {
						System.debug('LynxTicketCreate->TicketEvent, exception: ' + e.getMessage());
					}
				}
				System.debug('GetBody line 110:' + res.getBody());
				// System.assertEquals(200, res.getStatusCode());

				dom.Document resDoc = res.getBodyDocument();
				XmlStreamReader reader = res.getXmlStreamReader();

				// Read through the XML
				/*     String OKTicketId = '';
					 while(reader.hasNext()) {
						 System.debug('Event Type:' + reader.getEventType());
						 if (reader.getEventType() == XmlTag.START_ELEMENT) {
							 System.debug('getLocalName: ' +reader.getLocalName());
							 if ('OK' == (reader.getLocalName())){
								 boolean isSafeToGetNextXmlElement = true;
								  while(isSafeToGetNextXmlElement) {

									 if (reader.getEventType() == XmlTag.END_ELEMENT) {
										 OKTicketId = reader.getLocalName();
										 System.debug('OK Ticket Element Line 181 :' + OKTicketId);
										 break;
									 } else if (reader.getEventType() == XmlTag.CHARACTERS) {
										OKTicketId = reader.getText();
										 System.debug('OK Ticket Element Line 185 :' + OKTicketId);
																   }
									 // Always use hasNext() before calling next() to confirm
									 // that we have not reached the end of the stream

									 if (reader.hasNext()) {

										 reader.next();

									 } else {

										 isSafeToGetNextXmlElement = false;
										 break;
									 }
								 }

							 }
								}
								reader.next();
								}
								 if(OKTicketId==null || OKTicketId=='')
							  {
							  try{
								  Ticket_Event__c NewEvent = new Ticket_Event__c(
									  Case_Number__c = caseNumber,
									  Event_Type__c = 'New Ticket',
									  Status__c = 'New',
									  Failure_Reason__c = 'Lynx did not accept ticket create',
									  //Lynx_Ticket_Number__c = ticketNum,
									  Case_Id__c = CaseId
								  );
								   System.debug('LynxTicketCreate->NewEvent, CaseNumber(' + caseNumber + ')');
								  insert NewEvent;

							  }
							  catch(DmlException e) {
							  System.debug('LynxTicketCreate->TicketEvent, exception: ' + e.getMessage());
							 }

						  } */
				envelope = resDoc.getRootElement();
				//  String troubleTick = 'http://www.telus.com/schema/servicestatus/troubleticket';
				//String wsa = 'http://schemas.xmlsoap.org/soap/envelope/';
				//Dom.XMLNode TroubleTicket = resDoc.getRootElement();
				// dom.XmlNode header1 = envelope.getChildElement('Header', soapNS);
				// String TELUSTicketId = TroubleTicket.getChildElement('TELUSTicketId',null).getText();
				//System.debug('TELUSTicketId: ' + TELUSTicketId);
				System.debug(resDoc.toXmlString());
				System.debug('Envelope=:' + envelope);
				//  System.debug('TroubleTicket=:' + TroubleTicket);
				//System.debug(header1);
				//System.debug(testTime);

			} catch (Exception e) {
				System.debug('ERROR:' + e);
				try {
					Ticket_Event__c NewEvent = new Ticket_Event__c(
							Case_Number__c = caseNumber,
							Event_Type__c = 'New Ticket',
							Status__c = 'New',
							Failure_Reason__c = 'Ticket request did not go through: ' + e,
							//Lynx_Ticket_Number__c = ticketNum,
							Case_Id__c = CaseId
					);
					System.debug('LynxTicketCreate->NewEvent, CaseNumber(' + caseNumber + ')');
					insert NewEvent;

				} catch (DmlException ex) {
					System.debug('LynxTicketCreate->TicketEvent, exception: ' + ex.getMessage());
				}
			}

			return null;
		}

	}

}