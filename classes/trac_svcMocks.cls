/*
 * Mock responses for testing SMB Repairs services provided by RTS
 * Part of the SMB Repairs project
 *
 * @author: Grant Adamson, Traction On Demand 
 */

@isTest
global class trac_svcMocks {
	
	global class ValidateMockSuccess implements WebServiceMock {
		
		global void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction,
           							 String requestName, String responseNS, String responseName, String responseType) {
       
      		response.put('response_x', trac_svcPseudoMocks.validateSuccess());
  		}
	}


	global class ValidateMockFailureProductNotFound implements WebServiceMock {
		
		global void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction,
           							 String requestName, String responseNS, String responseName, String responseType) {
       
      response.put('response_x', trac_svcPseudoMocks.validateFailureProductNotFound());
  	}
	}
	
	
	global class ValidateMockFailureESNPhoneNoMatch implements WebServiceMock {
		
		global void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction,
           							 String requestName, String responseNS, String responseName, String responseType) {
       
      response.put('response_x', trac_svcPseudoMocks.validateFailureESNPhoneNoMatch());
  	}
	}
	
	
	global class validatePingMock implements WebServiceMock {
		
		global void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction,
           							 String requestName, String responseNS, String responseName, String responseType) {
    	
    	response.put('response_x', new svc_ping_v1.pingResponse_element());       							 	
  	}
	}
	
	
	global class InitCreateRepairMockSuccess implements WebServiceMock {
		
		global void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction,
           							 String requestName, String responseNS, String responseName, String responseType) {
	  	
	  	response.put('response_x', trac_svcPseudoMocks.InitCreateRepairSuccess());
    }
	}
	
	
	global class updateQuoteMockSuccess implements WebServiceMock {
		
		global void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction,
           							 String requestName, String responseNS, String responseName, String responseType) {
        
    	response.put('response_x', trac_svcPseudoMocks.updateQuoteSuccess());
    }
	}
	
	
	global class updateQuoteMockFailureUtiCheckFailed implements WebServiceMock {
		
		global void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction,
           							 String requestName, String responseNS, String responseName, String responseType) {
    	
    	response.put('response_x', trac_svcPseudoMocks.updateQuoteFailureUtiCheckFailed());
    }
	}
	
	
	global class updateQuoteMockFailureInvalidRequest implements WebServiceMock {
		
		global void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction,
           							 String requestName, String responseNS, String responseName, String responseType) {
    	
    	response.put('response_x', trac_svcPseudoMocks.updateQuoteFailureInvalidRequest());
    }
	}
	
	
	global class updateQuoteMockFailureNoDataUpdated implements WebServiceMock {
		
		global void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction,
           							 String requestName, String responseNS, String responseName, String responseType) {
    	
    	response.put('response_x', trac_svcPseudoMocks.updateQuoteFailureNoDataUpdated());
    }
	}
	
	
	global class updateQuoteMockFailureQuoteReplyExpired implements WebServiceMock {
		
		global void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction,
           							 String requestName, String responseNS, String responseName, String responseType) {
    	
    	response.put('response_x', trac_svcPseudoMocks.updateQuoteFailureQuoteReplyExpired());
    }
	}
	
	
	global class ManagementPingMock implements WebServiceMock {
		
		global void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction,
           							 String requestName, String responseNS, String responseName, String responseType) {
    	
    	response.put('response_x', new svc_ping_v1.pingResponse_element());    							 	
  	}
	}
	
	
	global class getRepairStatusMockSuccess implements WebServiceMock {
		
		global void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction,
           							 String requestName, String responseNS, String responseName, String responseType) {
			
    	response.put('response_x', trac_svcPseudoMocks.getRepairStatusSuccess());
    }
	}
	
	
	global class getRepairMockSuccess implements WebServiceMock {
		
		global void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction,
           							 String requestName, String responseNS, String responseName, String responseType) {
      
    	response.put('response_x', trac_svcPseudoMocks.getRepairSuccess());
    }
	}
	
	
	global class getRepairMockFailure implements WebServiceMock {
		
		global void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction,
           							 String requestName, String responseNS, String responseName, String responseType) {
				
		  response.put('response_x', trac_svcPseudoMocks.getRepairFailure());
    }
	}
	
	
	global class InformationPingMock implements WebServiceMock {
		
		global void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction,
           							 String requestName, String responseNS, String responseName, String responseType) {
    	
    	response.put('response_x', new svc_ping_v1.pingResponse_element());       							 	
  	}
	}
	
	
	// for legacy code coverage only
	global class InformationValidateMock implements WebServiceMock {
		global void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction,
           							 String requestName, String responseNS, String responseName, String responseType) {
    	
    	svc_EquipmentRepairInformationSvcRequest.validateRepairResponse_element resp =
    		new svc_EquipmentRepairInformationSvcRequest.validateRepairResponse_element();
    	
      resp.statusCode = -1;
      resp.statusMessage = new svc_EnterpriseCommonTypes_v7.MultilingualCodeDescTextList();
    	
    	response.put('response_x', resp);       							 	
  	}
	}
	
}