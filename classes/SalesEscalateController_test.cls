@isTest(seeAllData = true)
public class SalesEscalateController_test {

    static testMethod void success() {
        // get RCID account record type 
       	RecordType recordType = [Select r.Id, r.DeveloperName From RecordType r where r.SobjectType = 'Account' and r.DeveloperName='RCID' limit 1];
        // create test RCID to be associated with the contact
       	Account testRCID = new Account(Name='Testing Software', BillingCountry = 'US', BillingState = 'IL', recordTypeId=recordType.Id , No_Chronic_Incidents__c =10.0);        
       	insert testRCID;
        
        // create test contact
       	Contact testContact = new Contact(Lastname= 'lastname', Account = testRCID);
        insert testContact;
        
        // create standard controller
        ApexPages.StandardController scFail = new ApexPages.StandardController(new Contact());
        SalesEscalateController seFailTestController = new SalesEscalateController(scFail);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(testContact);
        SalesEscalateController seController = new SalesEscalateController(sc);
        
        seController.formId = '';
        seController.setFormInFocus();
        seController.formId = 'Held Orders';
        seController.setFormInFocus();
		seController.caseType = 'New';
        seController.updateSubTypePicklist();
		seController.caseType = 'Mobile';
        seController.updateSubTypePicklist();
        
        seController.caseRecord.Escalation_Reason__c = 'Test';
        seController.caseRecord.WTN__c = '1234567890';
        seController.saveRecord();
        
        seController.caseRecord.Escalation_Reason__c = 'Test';
        seController.caseRecord.WTN__c = 'invalid phone number';
        seController.saveRecord();
        
        seController.caseRecord.Escalation_Reason__c = '';
        seController.saveRecord();
        
        seController.caseRecord = null;
        seController.saveRecord();
        
//        seController.caseRecord = new Case();
//        seController.saveRecord();
        
        PageReference pageRef = Page.SalesEscalateMenuSalesforce1;
        pageRef.getParameters().put('id', String.valueOf(testContact.Id));
        Test.setCurrentPage(pageRef);        
    }    
}