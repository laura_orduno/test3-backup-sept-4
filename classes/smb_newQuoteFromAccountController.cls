public with sharing class smb_newQuoteFromAccountController {

    //Static Fields to maintain the options : added by Sudheer
     Private static final String newInstall= 'New Provide/Upgrade Order';
     Private static final String planChange= 'New Move Order';
     Private static final String other_Change = 'All Other Orders';
     Private static final String SRS_RECNAME = 'SRS Order Request';
     
     // SRS R 3.0 variables
       public String oppName {get;set;}
    public String selectedValue { get; set; }
    public String selectedOrderType{get;set;}
    public List<SelectOption> prodCategory { get; set; }
    public List<SelectOption> orderType {get; set;}
        
    public String errorMessage {get;set;}
    public Boolean flagErrorProdCategory{get;set;}
    public Boolean flagErrorOrdType{get;set;}
    
    class ServiceRequest{
        public String reasonForDisconnect;
        
        public ServiceRequest(String orderType){
            reasonForDisconnect = '';
//            reasonForDisconnect = orderType == 'disconnect' ? 'No Longer Needs Service or Porting/Leaving to Competitor' : reasonForDisconnect;
        }
    }
    
    public smb_newQuoteFromAccountController(){
    	flagErrorProdCategory = false;
        flagErrorOrdType = false;
        order_Type = other_Change;        //Added for R3SRS because option button is removed
        prodCategory = new List<SelectOption>();
        orderType = new List<SelectOption>();
        orderType.add(new SelectOption('NULL', 'Select Order Type'));
        populateProductCategory();
    }
     
    public void populateProductCategory(){
        prodCategory.add(new SelectOption('NULL', 'Select Product Category'));
        if(isSRSUser())
            prodCategory.add(new SelectOption('Complex', 'Complex'));
         prodCategory.add(new SelectOption('Simple (Voice, HSIA, Bundles & Toll-Free)', 'Simple (Voice, HSIA, Bundles & Toll-Free)'));         
    }
     
    public void populateOrderType(){
        system.debug('-----populateOrderType-----');
        orderType.clear();        
        if(selectedValue=='Simple (Voice, HSIA, Bundles & Toll-Free)'){
            system.debug('-----Simple -----');
            orderType.add(new SelectOption('NULL', 'Select Order Type'));
            orderType.add(new SelectOption(newInstall,newInstall));
            orderType.add(new SelectOption(planChange,planChange));
            orderType.add(new SelectOption(other_Change,other_Change ));
        }
        else if(selectedValue=='Complex'){
            system.debug('------Complex -----');
            orderType.add(new SelectOption('NULL', 'Select Order Type'));
            orderType.add(new SelectOption('Change', 'Change'));            
            orderType.add(new SelectOption('Disconnect', 'Disconnect'));            
            orderType.add(new SelectOption('Move', 'Move'));            
            orderType.add(new SelectOption('Prequal', 'Prequal'));
            orderType.add(new SelectOption('Provide/Install', 'Provide/Install'));
            //Select Option Removed -"Records Only" - Yogesh Narad(TechM) - 03-31-2017
            //orderType.add(new SelectOption('Records Only', 'Records Only'));
            //ends
        }else{
            orderType.add(new SelectOption('NULL', 'Select Order Type'));
        }
    }
     
     
     //To capture the order type: Added by Sudheer
      public String Order_Type { get; set; }
     //To Populate Related order : Added by Sudheer
    Public String getRelated_Order(){
    
        String roid = ApexPages.currentPage().getParameters().get('roid');
        if(roid !=null)
            return 'Create Order: Related to ' + roid;
        else
            return 'Create Order';
    }
    Boolean isCPQaccess;
   //To get wheter user has CPQ access
   public Boolean getCPQaccessforUser(){
       user loginUser =  [SELECT Id, Name, CPQ_User_Access__c, CPQ_Super_User_Access__c  FROM User WHERE Id  =:UserInfo.getUserId()];
       
       if(loginUser.CPQ_User_Access__c == true || loginUser.CPQ_Super_User_Access__c == true)
            isCPQaccess = true;     
        else
            order_Type = other_Change;
        return isCPQaccess;
    }
    //To display order options in VF: Added by Sudheer
    public List<SelectOption> getTypes(){           
           //List<selectOption> options = Util_Class_for_Complex_Order.findPickListVlaues('Order_Request__c', 'Order_Type__c');
           List<selectOption> options = new List<selectOption>();
           options.add(new SelectOption(newInstall,newInstall));
           options.add(new SelectOption(planChange,planChange));
           options.add(new SelectOption(other_Change,other_Change ));           
           return options;
     }
    public class NewQuoteException extends Exception {}
    public Boolean isAmmend {get;set;}
    public Boolean loaded {get; private set;}
    public Account account {get; private set;}
    public Contact[] contacts {get; private set;}
    public Integer contactsCount {get {if(contacts == null){return 0;} return contacts.size();}}
    
    /* Input Variables */
    public String searchTerm {get; set;}
    public Id selectedContactId {get; set;}
    
    public Boolean showNewContactForm {get; private set;}
    public Contact new_contact {get; set;}
    public Id mostRecentlyCreatedContactId {get; private set;}
  
    
    public void onLoad() {
        Savepoint sp = database.setSavepoint();
        try {
            loaded = false;
            showNewContactForm = false;
            if (ApexPages.currentPage() == null || ApexPages.currentPage().getParameters() == null) { clear(); return; }
            String aid = ApexPages.currentPage().getParameters().get('aid');
            if (aid == null || aid == '' || aid.substring(0, 3) != '001') { clear(); return; }
            Id xid = (Id) aid;
            account = [Select Name, Owner.Name From Account Where Id = :xid Limit 1];
            if (account == null) { clear(); throw new NewQuoteException('Unable to load account'); return; }

            loadContacts();
            if (contacts == null || contacts.size() == 0) { showNewContactForm = true; }
            
            new_contact = new Contact(AccountId = account.Id);
            
            if(ApexPages.currentPage().getParameters().containsKey('isammend')){
                if(ApexPages.currentPage().getParameters().get('isammend')=='true'){
                    isAmmend = true;
                }
                else{
                    isAmmend = false;
                }
            }
            else{
                isAmmend = false;
            }
            loaded = true;
        } catch (Exception e) { clear(); Database.rollback(sp); QuoteUtilities.log(e); }
    }
    
    private void loadContacts() {
        contacts = [Select Name, Title, Email, Active__c From Contact Where AccountId = :account.Id Order By Active__c DESC, CreatedDate DESC limit 50];
    }
    
    public PageReference toggleNewContactForm() {
        try {
            showNewContactForm = !showNewContactForm;
        } catch (Exception e) { clear(); QuoteUtilities.log(e); }
        return null;
    }
    
    public void onSearch() {
        try {
            if (searchTerm == null || searchTerm == '') { loadContacts(); return; }
            //searchTerm = '*' + searchTerm + '*';
            List<List<sObject>> searchResult = [FIND :searchTerm IN ALL FIELDS RETURNING Contact (name, email, title, Active__c Where AccountId = :account.Id) limit 50];
            contacts = ((List<Contact>)searchResult[0]);
        } catch (Exception e) { QuoteUtilities.log(e); } return;
    }
    
    
    public PageReference createNewContact() {
            //insert new_contact;
            ApexPages.standardController sc = new ApexPages.standardController(new_contact);
            sc.save();
            if (sc.getId() == null) { return null; }
            new_contact = [Select FirstName, LastName, Name, Email, Title, Phone From Contact Where Id = :sc.getId()];
            if (new_contact.Id != null) {
                showNewContactForm = false;
                mostRecentlyCreatedContactId = new_contact.id;
                new_contact = new Contact(AccountId = account.id);
                loadContacts();
            }
            return null;
    }
    
    public PageReference onSelectContact() {
        // Begin SRS R3 Changes
        //************start*****
        errorMessage = '';
        flagErrorProdCategory = false;
        flagErrorOrdType = false;
        system.debug('=======selectedValue======'+selectedValue);
        if(selectedValue == 'NULL'){
            system.debug('=======selectedValue======'+selectedValue);
            errorMessage = 'Please select Product Category.';
            flagErrorProdCategory = true;
            return null;
        }else if(selectedOrderType == 'NULL'){
            errorMessage = 'Please select Order Type.';
            flagErrorOrdType = true;
            return null;        
        }
        system.debug('~~~~~~~~~~~~~~' + selectedValue + '~~~~~~~~~' + selectedOrderType);
        if(selectedValue=='Complex'){
            //Traction added move statement - Thomas Tran - 06-29-2015
            //Start
            if(selectedOrderType == 'Move'){
                return S2C_smb_newQuoteFromAccountCtlrExt.launchWizard(0, account, selectedContactId, 'Complex Order', 'Originated', oppName);
            //End
            } else if(selectedOrderType == 'Prequal' || selectedOrderType == 'Provide/Install' || selectedOrderType == 'Change' || selectedOrderType == 'Disconnect'){
                   //Condition Removed for "Records Only" - Yogesh Narad(TechM)- 03-31-2017
                   //|| selectedOrderType == 'Records Only'
                   //end
                Opportunity ord_Req_Opp = new Opportunity();
                ord_Req_Opp.Amount = 0;
                ord_Req_Opp.CloseDate = system.today();
                ord_Req_Opp.AccountId = account.Id;
                ord_Req_Opp.Primary_Order_Contact__c = selectedContactId;
                system.debug('------oppName-----'+oppName);
                
                if(!String.isEmpty(oppName))
                {
                    ord_Req_Opp.Name = oppName;
                }else{
                    ord_Req_Opp.Name = account.Name + '- COMPLEX ORDER - ' +  System.now();
                }    
                ord_Req_Opp.Type = 'Complex Order';
                ord_Req_Opp.StageName = 'Originated';
                RecordType recType = [Select Id From RecordType Where SobjectType = 'Opportunity' And Name = 'SRS Order Request'];
                //RecordType srRecType = [Select Id From RecordType Where SobjectType = 'Service_Request__c' And Name = 'Prequal'];
                Schema.DescribeSObjectResult d = Schema.SObjectType.Service_Request__c;
                Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
                system.debug('======rtMapByName====='+rtMapByName);
                Id srRecTypeId  = rtMapByName.get(selectedOrderType).getRecordTypeId();
                system.debug('========srRecTypeId====='+srRecTypeId);
                //recType = rtMapByName.get(sr.RecordTypeId).getName();
                //Id srRecTypeId = SR_Record_Type__c.getValues(selectedOrderType).RecordType__c;//Custom Settings
                ord_Req_Opp.RecordTypeId = recType.Id;
                PageReference pageRef;
                try{
                insert ord_Req_Opp;                
                }catch(Exception e){  system.debug('##########'+e.getCause());}
                system.debug('***********'+ord_Req_Opp.Id);
                if(ord_Req_Opp.Id!= Null){
                    Order_Request__c ord_Req = new Order_Request__c();
                    ord_Req.Opportunity__c = ord_Req_Opp.Id;
                    Contact con = [Select id from Contact Where Id =: selectedContactId];
                    con.Opportunity__c = ord_Req_Opp.Id;
                    con.SRS_Contact_Type__c = 'Primary Contact';                  
                    try{
                        insert ord_Req;
                        system.debug('#@#@#@#@#@@#'+ord_Req.Id);
                        update con;
                        system.debug('####Contact###'+con);
                    }catch(Exception e){e.getCause();}
                        if(ord_Req.Id != Null){
							ServiceRequest serReq = new ServiceRequest(selectedOrderType);
                            Service_Request__c newPrequalSR = new Service_Request__c();
                            newPrequalSR.Opportunity__c = ord_Req_Opp.Id;
                            newPrequalSR.Account_Name__c = account.Id;
                            newPrequalSR.Order_Request_Lookup__c = ord_Req.Id;
                            newPrequalSR.RecordTypeId = srRecTypeId;
                            newPrequalSR.Service_Request_Status__c = 'Originated';
                            newPrequalSR.Status__c = 'Not Submitted';
                            newPrequalSR.PrimaryContact__c = ord_Req_Opp.Primary_Order_Contact__c;
                            newPrequalSR.Disconnect_Reason__c = serReq.reasonForDisconnect;
                            try{
                                insert newPrequalSR;
                                system.debug('@@@@newPrequalSR@@@@@@'+newPrequalSR);
                                
								//START :: Non-TQ TF Case 1904646, BR: S2CP-82 :: added by Rajan ( JIRA URL : https://jira.tsl.telus.com/browse/S2CP-82 )
								// Non-TQ TF Case 1904646, BR: S2CP-82 :: moved Service_Request_Contact__c insert logic to "smb_ServiceRequestTrigger.trigger"
                                /*
								Service_Request_Contact__c newSROnsiteContact = new Service_Request_Contact__c();            
                                newSROnsiteContact.Service_Request__c = newPrequalSR.id;
                                newSROnsiteContact.contact__c = newPrequalSR.PrimaryContact__c;
                                
                                //Start - ISD Bamboo TC ISD 1846751 - Requestor auto population Introduced label instead of 'Custoemr Onsite' text
                                 // newSROnsiteContact.Contact_Type__c = system.label.SRS_ContactTypeCustomerOnsite;
                                  
                                // Code Change by Priyanka Dhomne (Tech M.) Dt. 13 Feb 2017 for BR = 002
                                newSROnsiteContact.Contact_Type__c = System.label.SRS_ContactTypeRequestingCustomer;
                                
                                //End - ISD Bamboo TC ISD 1846751
                                
                                insert newSROnsiteContact;
                                */
								//END :: Non-TQ TF Case 1904646, BR: S2CP-82 :: added by Rajan
								
                            }catch(Exception e){}
                            //pageRef = new PageReference('/apex/SRS_NewServiceRequest?OID='+ord_Req_Opp.Id);
                            pageRef = new PageReference('/'+newPrequalSR.Id+'/e?retURL=/'+newPrequalSR.Id);
                            system.debug('pegeRef####'+pageRef);
                        }
                    system.debug('@@@@@ pageRef@@@@@'+ pageRef);  
                    pageRef.setRedirect(true);      
                    return pageRef;
                }else{
                    return null;
                }
            }else if(selectedOrderType == Null){   
                return null;
            }   
        }else{      //***********End SRS R3.0 Changes***********//
    
        try
        {
            if (selectedContactId == null)
            {
                throw new NewQuoteException('You must select a valid contact to create a quote.');
                return null;
            }
           
            if(isCPQaccess == false)
               order_Type = other_Change;
             else
               order_Type = selectedOrderType;
            
            if (Order_Type == null)
            {
                throw new NewQuoteException('Please select an Order Type.');
                return null;
            }
            
            // query for SMB Opportunity record type
            List<RecordType> RT = [Select Id From RecordType Where SobjectType = 'Opportunity' And Name = 'SMB Care Opportunity'];
            
            // insert new opportunity
            Opportunity Oppt = new Opportunity();
            Oppt.AccountId = account.Id;
            // Included logic to default Opportunity name.
            if(!String.isEmpty(oppName))
                {
                    Oppt.Name = oppName;
                }else{
                    Oppt.Name = 'Opportunity for ' + '-' + System.Now();
                } 
            
            Oppt.CloseDate = Date.today();
            if(isAmmend){
                Oppt.Provide_Order__c = true;
                if(ApexPages.currentPage().getParameters().containsKey('oppId') && ApexPages.currentPage().getParameters().get('oppId') != null && ApexPages.currentPage().getParameters().get('oppId') != ''){
                    Oppt.Related_Orders__c = ApexPages.currentPage().getParameters().get('oppId');
                }
            }
            //Oppt.StageName = 'I have a Potential Opportunity (20%)';
            Oppt.Primary_Order_Contact__c = selectedContactId;
            Oppt.Send_Quote_Contact__c = selectedContactId;
            Oppt.StageName = 'Quote Originated';
            System.debug('~~~~~~~ Order Type for the opportunity' + Order_Type);
            Oppt.Type = Order_Type;
            
            if (RT.size() > 0)
            {
                Oppt.RecordTypeId = RT[0].Id;
            }
            
            insert Oppt;
            System.debug('NEW OPP->'+Oppt);
             Opportunity opptForLink = new Opportunity();
              opptForLink = [select Order_ID__c from Opportunity where id = :oppt.id];
              system.debug('~~~~~~~~~~~~~~ OpptForlink' + opptForLink.Order_ID__c);
            
            //insert primary contact
            OpportunityContactRole OCR = new OpportunityContactRole();
            OCR.ContactId = selectedContactId;
            OCR.OpportunityId = Oppt.Id;
            OCR.IsPrimary = true;
            OCR.Role = 'Approver';
            
            insert OCR;
            system.debug('=========order type=========' + order_Type + '==' + newInstall);
            //Changed Order_Type to the new value RB 12/08/14
            if(selectedOrderType != other_Change){
                String hostURL = System.URL.getCurrentRequestUrl().getHost();            
                hostURL = hostURL.substring(hostURL.indexOf('.') + 1, hostURL.indexOf('.', hostURL.indexOf('.') + 1));
                // Code Change by Puneet
                string oppFieldId = Label.smb_Opportunity_Id_FieldId;
                if(oppFieldId ==  null)
                    oppFieldId = '';
                else
                    oppFieldId = String.valueOf(oppFieldId).trim();        
                //PageReference PR = new PageReference('https://scpq.' + hostURL + '.visual.force.com/apex/VisualforceSCIQuoteAdd?retURL=%2F' + Oppt.Id + '&CF00NM00000013291=' + Oppt.Name + '&sfdc.override=1&CF00NM00000013291_lkid=' + Oppt.Id + '&scontrolCaching=1');
                //PageReference PR = new PageReference('/apex/smb_sciQuoteAdd?retURL=%2F' + Oppt.Id + '&CF00NM00000013291=' + EncodingUtil.urlEncode(Oppt.Name, 'UTF-8') + '&sfdc.override=1&CF00NM00000013291_lkid=' + Oppt.Id + '&scontrolCaching=1');
                // Added by Rahul
                PageReference PR = new PageReference('/apex/smb_sciQuoteAdd?retURL=%2F' + Oppt.Id + '&CF'+ oppFieldId +'=' + EncodingUtil.urlEncode(Oppt.Name, 'UTF-8') + '&sfdc.override=1&CF'+ oppFieldId +'_lkid=' + Oppt.Id + '&scontrolCaching=1&OppOrdID=' + opptForLink.Order_ID__c );
                // PageReference PR = new PageReference('https://scpq.' + hostURL + '.salesforce.com/apex/smb_sciQuoteAdd?retURL=%2F' + Oppt.Id + '&CF'+ oppFieldId +'=' + EncodingUtil.urlEncode(Oppt.Name, 'UTF-8') + '&sfdc.override=1&CF'+ oppFieldId +'_lkid=' + Oppt.Id + '&scontrolCaching=1');   
                // PageReference PR = new PageReference('https://scpq.' + hostURL + '.visual.force.com/apex/VisualforceSCIQuoteAdd?retURL=%2F' + Oppt.Id + '&CF'+ oppFieldId +'=' + EncodingUtil.urlEncode(Oppt.Name, 'UTF-8') + '&sfdc.override=1&CF'+ oppFieldId +'_lkid=' + Oppt.Id + '&scontrolCaching=1');   
                //PR.setRedirect(true);
                return PR;
            }
            else {
                String roid =  ApexPages.currentPage().getParameters().get('roid');
                PageReference pr = new PageReference('/apex/complex_Order');
                pr.getParameters().put('retURL', '%2F'+selectedContactId);
                pr.getParameters().put('Accid', account.Id);
                pr.getParameters().put('Id', Oppt.Id);
                pr.getParameters().put('conid', selectedContactId);
                pr.getParameters().put('otype', order_Type);
                pr.getParameters().put('roid', roid );
                pr.getParameters().put('nonce', ApexPages.currentPage().getParameters().get('nonce')); 
                pr.getParameters().put('sfdcIFrameOrigin', ApexPages.currentPage().getParameters().get('sfdcIFrameOrigin')); 
                pr.setRedirect(true);
                return pr;
            } 
          }
        catch (Exception e) 
        {
            QuoteUtilities.log(e); 
        } 
        }
        return null;
    }
    
    private void clear() {
        account = null;
        contacts = null;
        new_contact = null;
    }
    // Method to determine if user is able to create Complex orders
    private boolean isSRSUser()
    {
    String toMatchRecordType = 'SRS Order Request';
List<String> names = new List<String>();
List<RecordTypeInfo> infos = Opportunity.SObjectType.getDescribe().getRecordTypeInfos();

    if (infos.size() > 1) {
        for (RecordTypeInfo i : infos) {
           if (i.isAvailable() && !String.valueOf(i.getRecordTypeId()).endsWith('AAA'))
                names.add(i.getName());
        }
    } 
    // Just the Master record type,
    else names.add(infos[0].getName());
    boolean isMatch = false;
    for(string s :names)
    {
        system.debug('~~ Record Type:' + s);
    if(s.equalsIgnoreCase(toMatchRecordType))
    {
        isMatch = True;
        break;
    }
    }
    // Return value
    return isMatch; 
    }
    
  
}