@istest
private class CreditAssessmentRedirectController_Test {
    Public static Account acc;
    Public static Credit_Profile__c creditProfile;
        
    private static testMethod void testLNV () {    
        setupTestData();
        Credit_ID_Mapping__c Credit_CS = new Credit_ID_Mapping__c(Name='Credit Product', Field_ID__c='00Ne0000001JO6F');
        insert Credit_CS;
        
        Test.setCurrentPage(Page.CreditAssessmentRedirect);
        ApexPages.currentPage().getParameters().put('paramAccountID',acc.id);
        ApexPages.currentPage().getParameters().put('paramCARType','LNV');
        ApexPages.currentPage().getParameters().put('paramLegalEntityType','INC');
        ApexPages.StandardController stdAccount = new ApexPages.StandardController(acc);
        CreditAssessmentRedirectController RedirectController = new CreditAssessmentRedirectController(stdAccount);
        PageReference PR = RedirectController.redirect();
        String CAR_ID = RedirectController.outputCARID;
        System.debug('...LNV Test...CAR_ID: ' + CAR_ID);
        System.debug('Page Reference PR: ' + PR);
        Credit_Assessment__c car = new Credit_Assessment__c();
        if (CAR_ID<>null) {
            car = [select id, name, RCID__c, Credit_Assessment_Request_Type__c from credit_assessment__c where id=:CAR_ID];
        }
        
        System.assertEquals('Legal Name Validation',car.Credit_Assessment_Request_Type__c);        
    }

    
    private static testMethod void testLNVSubmit () {    
        setupTestData();
        Credit_ID_Mapping__c Credit_CS = new Credit_ID_Mapping__c(Name='Credit Product', Field_ID__c='00Ne0000001JO6F');
        insert Credit_CS;
        
        Credit_Assessment__c CAR = new Credit_Assessment__c(
                                    Credit_Assessment_Request_Type__c='Legal Name Validation',
                                    CAR_Account__c = acc.id,
                                    Credit_Profile__c = creditProfile.id);
        insert CAR;
        Test.setCurrentPage(Page.CreditAssessmentRedirect);
        ApexPages.currentPage().getParameters().put('paramAccountID',acc.id);
        ApexPages.currentPage().getParameters().put('paramCARID', CAR.id);
        ApexPages.currentPage().getParameters().put('operation','Submit');
        ApexPages.StandardController stdAccount = new ApexPages.StandardController(acc);
        CreditAssessmentRedirectController RedirectController = new CreditAssessmentRedirectController(stdAccount);
        PageReference PR = RedirectController.redirect();
        String CAR_ID = RedirectController.outputCARID;
        
        if (CAR_ID<>null) {
            car = [select id, name, CAR_Status__c from credit_assessment__c where id=:CAR_ID];
        }
        
        System.assertEquals('Submitted',car.CAR_Status__c);        
        
    } 

    private static testMethod void testPolicyRedirect () {    
        setupTestData();

        Test.setCurrentPage(Page.PrivacyPolicyRedirect);
        ApexPages.currentPage().getParameters().put('paramAccountID',acc.id);
        ApexPages.StandardController stdAccount = new ApexPages.StandardController(acc);
        CreditAssessmentRedirectController RedirectController = new CreditAssessmentRedirectController(stdAccount);
        PageReference PR = RedirectController.getPolicyRedirect();
        String outputLegalEntityType = RedirectController.outputLegalEntityType;
        
        System.assertEquals(null,RedirectController.outputLegalEntityType);        
    }
    
    private static testMethod void testParameters () {
        setupTestData();

        Test.setCurrentPage(Page.CreditAssessmentRedirect);
        ApexPages.currentPage().getParameters().put('paramAccountID',acc.id);
        ApexPages.currentPage().getParameters().put('paramUserAlias','Somebody');
        ApexPages.currentPage().getParameters().put('paramCARType','LNV');
        ApexPages.currentPage().getParameters().put('paramOppID','1234');
        ApexPages.currentPage().getParameters().put('paramCBCBypass','Y');
        ApexPages.currentPage().getParameters().put('paramPPBypass','N');
        ApexPages.currentPage().getParameters().put('paramLegalEntityType','PR');
        
        ApexPages.StandardController stdAccount = new ApexPages.StandardController(acc);
        CreditAssessmentRedirectController RedirectController = new CreditAssessmentRedirectController(stdAccount);

        System.assertEquals(acc.id,RedirectController.paramAccountID);
        System.assertEquals('Somebody',RedirectController.paramUserAlias);
        System.assertEquals('LNV',RedirectController.paramCARType);
        System.assertEquals('1234',RedirectController.paramOppID);
        System.assertEquals('Y',RedirectController.paramCBCBypass);
        System.assertEquals('N',RedirectController.paramPPBypass);
        System.assertEquals('PR',RedirectController.paramLegalEntityType);
    } 

    public static void setupTestData() {
       acc= new Account(
            Name = 'RCID Test',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            RCID__c = '987654'
        );
        insert acc; 
        
        system.debug('...account created: ' + acc.name);
        
        creditProfile=new Credit_Profile__c(name='CP-RCID',
                                            account__c=acc.id,
                RecordTypeId = Schema.SObjectType.Credit_Profile__c.getRecordTypeInfosByName().get('Business').getRecordTypeId(),
                                            Legal_Entity_Type__c='Incorporated (INC)'
        );
        insert creditProfile;

        system.debug('...Credit Profile Created: ' + creditProfile.name);
                
        acc.Credit_Profile__c=creditProfile.id;
        update acc;
        
        system.debug('... Account link: ' + acc.Credit_Profile__c);
    }    
}