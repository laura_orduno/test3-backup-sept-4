global class OC_SendForeSignatureCntrl {
    
    public id contractId{get;set;}
    public contract conObj= new contract(); 
    public string LanguagePref {get;set;}
    public list<CustomerInfo> lstCustomerInfo{get;set;}
    public string sCustomerInfo{get;set;}
    public  OC_SendForeSignatureCntrl(vlocity_cmt.DocuSignEnvelopeController con){
        lstCustomerInfo=new list<CustomerInfo>();
        contractID = ApexPages.currentPage().getParameters().get('id');
        System.debug('>>ContractId:'+contractID);
        conObj = [select id,name , Customer_Signor__c, Customer_Signor__r.name,  Customer_Signor__r.email, 
               Customer_Signor__r.id , TELUS_Signor__c, TELUS_Signor__r.id, TELUS_Signor__r.name,TELUS_Signor__r.email,Language_Preference__c 
               from contract where id = :contractID limit 1];
        LanguagePref =conObj.Language_Preference__c;
        if(conObj != null && conObj.id != null){
            if(conObj.Customer_Signor__c != null)
                lstCustomerInfo.add(new CustomerInfo(conObj.Customer_Signor__r.email,conObj.Customer_Signor__r.name,conObj.Customer_Signor__r.Id));
            if(conObj.TELUS_Signor__c != null)
                lstCustomerInfo.add(new CustomerInfo(conObj.TELUS_Signor__r.email,conObj.TELUS_Signor__r.name,conObj.TELUS_Signor__r.Id));
        }
        sCustomerInfo=JSON.serialize(lstCustomerInfo);
        System.debug('CustomerInfo' + sCustomerInfo);
    }
    @RemoteAction
    global static list<SignerTypes> getSignerTypes() {
        list<SignerTypes> options = new list<SignerTypes>();
        // Get the list of picklist values for this field.
        list<Schema.PicklistEntry> values =vlocity_cmt__ContractRecipient__c.Signer_Type__c.getDescribe().getPicklistValues();
        // Add these values to the selectoption list.
        for (Schema.PicklistEntry a : values){ 
            options.add(new SignerTypes(a.getLabel(), a.getValue())); 
        }   
        return options;
    }
    @RemoteAction
    global static void updateRecipient(string contractId,string sObjectRecipientContract ) {
        system.debug('****lstObjectRecipientContract***'+sObjectRecipientContract);
        list<recipientContract> lstRecipientSignerTypes=(list<recipientContract>)json.deserialize( sObjectRecipientContract,list<recipientContract>.class);
        map<string,string> mapRecipientSignerTypes=new map<string,string>();
        for(recipientContract objrecipientContract : lstRecipientSignerTypes){
            system.debug('****objrecipientContract***'+objrecipientContract);
            mapRecipientSignerTypes.put(string.valueof(objrecipientContract.routingOrder),objrecipientContract.signerType);
        }
        system.debug('****mapRecipientSignerTypes***'+mapRecipientSignerTypes);
        list<vlocity_cmt__ContractEnvelope__c> lstContractEnvelope=[select id,(select id,vlocity_cmt__RoutingOrder__c,Signer_Type__c 
                                                                               from vlocity_cmt__ContractRecipients__r) from 
                                                                    vlocity_cmt__ContractEnvelope__c where vlocity_cmt__ContractVersionId__r.vlocity_cmt__ContractId__c=:contractId 
                                                                    and vlocity_cmt__ContractVersionId__r.vlocity_cmt__Status__c='Active'];
        list<vlocity_cmt__ContractRecipient__c> lstContractRecipient=new list<vlocity_cmt__ContractRecipient__c>();
        if(lstContractEnvelope != null && lstContractEnvelope.size() > 0 && lstContractEnvelope[0].vlocity_cmt__ContractRecipients__r != null &&
          lstContractEnvelope[0].vlocity_cmt__ContractRecipients__r.size() > 0){
            for(vlocity_cmt__ContractRecipient__c objContractRecipient : lstContractEnvelope[0].vlocity_cmt__ContractRecipients__r){
                if(objContractRecipient.vlocity_cmt__RoutingOrder__c != null && mapRecipientSignerTypes.containsKey(string.valueof(objContractRecipient.vlocity_cmt__RoutingOrder__c))){
                    objContractRecipient.Signer_Type__c=mapRecipientSignerTypes.get(string.valueof(objContractRecipient.vlocity_cmt__RoutingOrder__c));
                    lstContractRecipient.add(objContractRecipient);
                }
            }
            update lstContractRecipient;
        }
        
    }
    global class recipientContract{
        public Integer routingOrder{get;set;}
        public string signerType{get;set;}
    }
    global class SignerTypes{
        public string label{get;set;}
        public string value{get;set;}
        public SignerTypes(string label,string value){
            this.label=label;
            this.value=value;
        }
    }
    
    @RemoteAction
    global static list<SignerTypes> saveSignerType() {
        list<SignerTypes> options = new list<SignerTypes>();
        // Get the list of picklist values for this field.
        list<Schema.PicklistEntry> values =vlocity_cmt__ContractRecipient__c.Signer_Type__c.getDescribe().getPicklistValues();
        // Add these values to the selectoption list.
        for (Schema.PicklistEntry a : values){ 
            options.add(new SignerTypes(a.getLabel(), a.getValue())); 
        }   
        return options;
    }
    
    public PageReference updateContractMethod() {
         
        try {
            conObj.Status = 'In Progress';
            update conObj;
            return null;

        }catch (Exception e){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
            return null;
        }finally{
            return null;
        }

        return null;

    }

    public class CustomerInfo{
        public string email{get;set;}
        public string name{get;set;}
        public string id{get;set;}
        public CustomerInfo(string email,string name,string id){
            this.email = email;
            this.name = name;
            this.id = id;
        }
    }
    
}