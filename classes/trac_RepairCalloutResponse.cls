/*
 * Wrapper for RTS Repair Response classes generated from TELUS-supplied WSDL
 * Used by trac_RepairCallout to provide a universal response to return to the trac_Repair*Ctlr classes
 * Part of the SMB Repairs project
 *
 * @author: Grant Adamson, Traction On Demand 
 */

public class trac_RepairCalloutResponse {
		public static final String MANUAL_ERROR_MSG_TYPE = 'ERROR';
		public static final String SUCCESS_STRING = 'SUCC';
		public static final String FAILURE_STRING = 'FAILED';
		private static final String CASE_ID_MISMATCH_CODE = 'CASE_ID_MISMATCH';
				
		public Boolean success {get; private set;}
		public DateTime dateTimeStamp {get; private set;}
    public String errorCode {get; private set;}
    public String messageType {get; private set;}
    public String transactionId {get; private set;}
    public String messageBody {get; private set;}
    public String contextData {get; private set;}
    public boolean hasWarning;
    public boolean hasWarning(){
    	return hasWarning;
    }
				
		// generic exception type, just stores exception message in status
		// used for WS timeouts, parsing failures, etc
		public trac_RepairCalloutResponse(Exception e) {			
			messageType = MANUAL_ERROR_MSG_TYPE;
			success = false;
			hasWarning=false;
			// "friendly" error messages for timeouts or general callout exceptions
			if(e instanceOf CalloutException) {
				if(e.getMessage().contains('Read timed out')) {
					messageBody = buildErrorMessage(Label.RTS_TIMEOUT, e.getMessage());
				} else {
					messageBody = buildErrorMessage(Label.RTS_UNKNOWN_RESPONSE, e.getMessage());
				}
			} else {
				messageBody = e.getMessage();
			}
		}
		
		private static String buildErrorMessage(String friendlyName, String error) {
			return friendlyName + '<br/><br/>' + error + '<br/>' + Datetime.now();
		}
		
		
		public trac_RepairCalloutResponse(svc_RepairRequestValidationSvcRequestR_2.validateRepairRequestResponse_element resp) {
			hasWarning=false;
			success = parseStatus(resp.validationStatus, resp.validationMessageList);
		}
		
		
		public trac_RepairCalloutResponse(rpm_EquipmentRepairManagementSvcRequestR.initCreateRepairResponse_element resp) {
			// have to manually build the response message due to restrictions on inheritance
			rpm_EnterpriseCommonTypes_v7.ResponseMessage message = new rpm_EnterpriseCommonTypes_v7.ResponseMessage();
			
			message.dateTimeStamp = resp.dateTimeStamp;
      message.errorCode = resp.errorCode;
      message.messageType = resp.messageType;
      message.transactionId = resp.transactionId;
      message.messageList = resp.messageList;
      message.contextData = resp.contextData;
			
			String status = (message.errorCode != null ? FAILURE_STRING : SUCCESS_STRING);
			rpm_EnterpriseCommonTypes_v7.ValidationMessageList validationMessageList=new rpm_EnterpriseCommonTypes_v7.ValidationMessageList();
			validationMessageList.validationMessage=new list<rpm_EnterpriseCommonTypes_v7.ResponseMessage>();
			validationMessageList.validationMessage.add(message);
			success = parseStatus(status, validationMessageList);
		}
		
		
		public trac_RepairCalloutResponse(rpm_EquipmentRepairInformationSvcRequest.getRepairResponse_element resp) {
			rpm_EnterpriseCommonTypes_v7.ResponseMessage message = new rpm_EnterpriseCommonTypes_v7.ResponseMessage();
			
			message.dateTimeStamp = resp.dateTimeStamp;
      message.errorCode = resp.errorCode;
      message.messageType = resp.messageType;
      message.transactionId = resp.transactionId;
      message.messageList = resp.messageList;
      message.contextData = resp.contextData;
			
			String status = (message.errorCode != null ? FAILURE_STRING : SUCCESS_STRING);
			
			rpm_EnterpriseCommonTypes_v7.ValidationMessageList validationMessageList=new rpm_EnterpriseCommonTypes_v7.ValidationMessageList();
			validationMessageList.validationMessage=new list<rpm_EnterpriseCommonTypes_v7.ResponseMessage>();
			validationMessageList.validationMessage.add(message);
			success = parseStatus(status, validationMessageList);
		}
		
		
		public trac_RepairCalloutResponse(rpm_EquipmentRepairManagementSvcRequestR.updateEquipmentRepairQuotationResponse_element resp) {
			rpm_EnterpriseCommonTypes_v7.ResponseMessage message = new rpm_EnterpriseCommonTypes_v7.ResponseMessage();
			
			message.dateTimeStamp = resp.dateTimeStamp;
      message.errorCode = resp.errorCode;
      message.messageType = resp.messageType;
      message.transactionId = resp.transactionId;
      message.messageList = resp.messageList;
      message.contextData = resp.contextData;
			
			String status = (message.errorCode != null ? FAILURE_STRING : SUCCESS_STRING);
			
			rpm_EnterpriseCommonTypes_v7.ValidationMessageList validationMessageList=new rpm_EnterpriseCommonTypes_v7.ValidationMessageList();
			validationMessageList.validationMessage=new list<rpm_EnterpriseCommonTypes_v7.ResponseMessage>();
			validationMessageList.validationMessage.add(message);
			success = parseStatus(status, validationMessageList);
		}
		
		
		private Boolean parseStatus(String status, svc_EnterpriseCommonTypes_v7.ValidationMessageList validationMessageList) {
			boolean parseStatus=false;
			webservice_integration_error_log__c errorLog=new webservice_integration_error_log__c(apex_class_and_method__c='trac_RepairCalloutResponse.cls');
			if(status != null &&
					(status.toUpperCase() == SUCCESS_STRING || status.toUpperCase() == FAILURE_STRING)) {
				errorLog.error_code_and_message__c=status+'\n\n';
				// Always parse messages, call may be successful, but have warnings
				if(validationMessageList != null) {
					svc_EnterpriseCommonTypes_v7.ResponseMessage[] responseMessages=validationMessageList.validationMessage;
					errorLog.error_code_and_message__c+='Num Validation Message:'+(responseMessages!= null?responseMessages.size()+'\n\n':'\n\n');
					for(svc_EnterpriseCommonTypes_v7.ResponseMessage responseMessage:responseMessages){
						dateTimeStamp = responseMessage.dateTimeStamp;
						errorCode = responseMessage.errorCode;
						messageType = responseMessage.messageType;
						transactionId = responseMessage.transactionId;
						contextData = responseMessage.contextData;
						errorLog.error_code_and_message__c+='Datetime Stamp:'+dateTimeStamp+'\n'+
						'Error Code:'+errorCode+'\n'+
						'Message Type:'+messageType+'\n'+
						'Transaction ID:'+transactionId+'\n'+
						'Context Data:'+contextData+'\n\n';
						if(string.isnotblank(messageType)&&messageType.equalsIgnoreCase(trac_RepairValidateCtlr.MESSAGE_TYPE_WARNING)){
							hasWarning=true;
						}
						errorLog.error_code_and_message__c+='Num Messages:'+(responseMessage.messageList != null?responseMessage.messageList.size()+'\n\n':'\n\n');
						if(responseMessage.messageList != null && !responseMessage.messageList.isempty()) {
							String locale = trac_RepairCallout.getCleanLocale();
							for(svc_EnterpriseCommonTypes_v7.message msg : responseMessage.messageList) {
								if(msg.locale.toUpperCase() == locale.toUpperCase()) {
									messageBody = msg.message;
								}
							}
						}
						
						if(messageBody == null) {	
							if (errorCode != null){
								messageBody = errorCode;
							} else {
								messageBody = Label.UNKNOWN_RESPONSE;
							}
						}
						errorLog.error_code_and_message__c+='\nMessage Body:'+messageBody+'\n\n------------------\n\n';
					}
				}
				parseStatus=(status.toUpperCase() == SUCCESS_STRING);
			
			} else { // Unkown status
				messageType = MANUAL_ERROR_MSG_TYPE;
				messageBody = Label.UNKNOWN_RESPONSE;
				errorLog.error_code_and_message__c=messageType+'\n'+messageBody;
			}
			try{
				insert errorLog;
			}catch(exception e){
				system.debug(e.getmessage());
			}
			return parseStatus;
		}
    Boolean parseStatus(String status, rpm_EnterpriseCommonTypes_v7.ValidationMessageList validationMessageList) {
        boolean parseStatus=false;
        webservice_integration_error_log__c errorLog=new webservice_integration_error_log__c(apex_class_and_method__c='trac_RepairCalloutResponse.cls');
        if(status != null &&
           (status.toUpperCase() == SUCCESS_STRING || status.toUpperCase() == FAILURE_STRING)) {
               errorLog.error_code_and_message__c=status+'\n\n';
               // Always parse messages, call may be successful, but have warnings
               if(validationMessageList != null) {
                   rpm_EnterpriseCommonTypes_v7.ResponseMessage[] responseMessages=validationMessageList.validationMessage;
                   errorLog.error_code_and_message__c+='Num Validation Message:'+(responseMessages!= null?responseMessages.size()+'\n\n':'\n\n');
                   for(rpm_EnterpriseCommonTypes_v7.ResponseMessage responseMessage:responseMessages){
                       dateTimeStamp = responseMessage.dateTimeStamp;
                       errorCode = responseMessage.errorCode;
                       messageType = responseMessage.messageType;
                       transactionId = responseMessage.transactionId;
                       contextData = responseMessage.contextData;
                       errorLog.error_code_and_message__c+='Datetime Stamp:'+dateTimeStamp+'\n'+
                           'Error Code:'+errorCode+'\n'+
                           'Message Type:'+messageType+'\n'+
                           'Transaction ID:'+transactionId+'\n'+
                           'Context Data:'+contextData+'\n\n';
                       if(string.isnotblank(messageType)&&messageType.equalsIgnoreCase(trac_RepairValidateCtlr.MESSAGE_TYPE_WARNING)){
                           hasWarning=true;
                       }
                       errorLog.error_code_and_message__c+='Num Messages:'+(responseMessage.messageList != null?responseMessage.messageList.size()+'\n\n':'\n\n');
                       if(responseMessage.messageList != null && !responseMessage.messageList.isempty()) {
                           String locale = trac_RepairCallout.getCleanLocale();
                           for(rpm_EnterpriseCommonTypes_v7.message msg : responseMessage.messageList) {
                               if(msg.locale.toUpperCase() == locale.toUpperCase()) {
                                   messageBody = msg.message;
                               }
                           }
                       }
                       
                       if(messageBody == null) {	
                           if (errorCode != null){
                               messageBody = errorCode;
                           } else {
                               messageBody = Label.UNKNOWN_RESPONSE;
                           }
                       }
                       errorLog.error_code_and_message__c+='\nMessage Body:'+messageBody+'\n\n------------------\n\n';
                   }
               }
               parseStatus=(status.toUpperCase() == SUCCESS_STRING);
               
           } else { // Unkown status
               messageType = MANUAL_ERROR_MSG_TYPE;
               messageBody = Label.UNKNOWN_RESPONSE;
               errorLog.error_code_and_message__c=messageType+'\n'+messageBody;
           }
        try{
            insert errorLog;
        }catch(exception e){
            system.debug(e.getmessage());
        }
        return parseStatus;
    }
	public class TestException extends Exception{}
}