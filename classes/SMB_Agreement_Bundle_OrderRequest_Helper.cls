public class SMB_Agreement_Bundle_OrderRequest_Helper {
    /*
    list<Apttus__APTS_Agreement__c> lstRegisteredAgreements = new list<Apttus__APTS_Agreement__c>();
    map<id,list<Apttus__APTS_Agreement__c>> mapAgreementSiblingToWork = new map<id,list<Apttus__APTS_Agreement__c>>();
    public void Initialize(list<Apttus__APTS_Agreement__c> lstAgreements,map<ID,Apttus__APTS_Agreement__c> mapOldAgreements)
    {
        for (Apttus__APTS_Agreement__c agg : lstAgreements)
        {
            if (null != agg.Apttus__Related_Opportunity__c 
            // When contract Status = Contract Registered
                && agg.Apttus__Status_Category__c == 'In Effect' && agg.Apttus__Status__c == 'Activated'
                && agg.Apttus__Status__c != mapOldAgreements.get(agg.Id).Apttus__Status__c
                 && agg.Apttus__Status_Category__c != mapOldAgreements.get(agg.Id).Apttus__Status_Category__c)
            {
                lstRegisteredAgreements.add(agg);
            }
        }
        
        system.debug('---lstRegisteredAgreements'+lstRegisteredAgreements);
        if (lstRegisteredAgreements.size()>0)
        {
            
            searchBundleProduct(lstRegisteredAgreements);
            //an opportunity can have more than one agreement. Adding Opportunity ID for each agreement in set for unique values.
            set<ID> setOppID = new set<ID>();
            for(Apttus__APTS_Agreement__c a:lstRegisteredAgreements)
            {
                setOppID.add(a.Apttus__Related_Opportunity__c);             
            }
            list<ID> temp = new list<ID> ();
            PrepareOrderRequest(new list<ID>(setOppID));
        }
        
    }
    
    public void searchBundleProduct(list<Apttus__APTS_Agreement__c> lstAgreements) 
    {
        list<OpportunityLineItem> lstOLIBundle = [  SELECT 
                                                        PricebookEntry.Product2.Name, 
                                                        Id,
                                                        OpportunityId,
                                                        Opportunity.OwnerId, 
                                                        Opportunity.AccountId,
                                                        LineNo__c
                                                    FROM 
                                                        OpportunityLineItem
                                                    WHERE   
                                                        (
                                                                PricebookEntry.Product2.Name LIKE '%Business Anywhere%' 
                                                                OR 
                                                                PricebookEntry.Product2.Name LIKE '%BusinessAnywhere%' 
                                                        )                                                         
                                                        AND
                                                            Agreement__c in :lstAgreements 
                                                        AND 
                                                            Element_Type__c = 'Bundle'
                                                  ];
        
        system.debug('---lstOLIBundle'+lstOLIBundle);
        
        set<id> oppIdSet = new set<id>();
        
        map<id,map<integer,OpportunityLineItem>> bundleLineMap = new map<id,map<integer,OpportunityLineItem>>();
        // Map containing Oppid -> Bundle # no -> Offer Code and Frequency
        map<id,map<integer,map<string,integer>>> offerCodeFreq = new map<id,map<integer,map<string,integer>>>();
        
        if(lstOLIBundle.size()>0)
        {
            list<String> listParentId = new list<String>();
            for(OpportunityLineItem ol: lstOLIBundle){
                listParentId.add(ol.Id);
                oppIdSet.add(ol.OpportunityId);
                
                map<integer,OpportunityLineItem> tempBundleLine;
                if(bundleLineMap.containsKey(ol.OpportunityId))
                    tempBundleLine = bundleLineMap.get(ol.OpportunityId);
                else
                    tempBundleLine = new map<integer,OpportunityLineItem>();
                tempBundleLine.put(Integer.valueOf(ol.LineNo__c),ol);
                bundleLineMap.put(ol.OpportunityId,tempBundleLine);
            }
            
            // Find Bundle which has Offer Code and how many times?
            List<OpportunityLineItem> oliList = [SELECT id, PriceBookEntry.Product2.Wireless_Offer_Code__c,Parent_Bundle_No__c, OpportunityId 
                                                FROM OpportunityLineItem WHERE OpportunityId IN :oppIdSet AND
                                                (PriceBookEntry.Product2.Wireless_Offer_Code__c != '' OR PriceBookEntry.Product2.Wireless_Offer_Code__c != null) AND Parent_Bundle_No__c != null];
            if(oliList.size()>0)
            {
                for(OpportunityLineItem ol : oliList)
                {
                    map<string,integer> tempFreq;
                    map<integer,map<string,integer>> tempBundles;
                    
                    // Check for Opportunity
                    if(offerCodeFreq.containsKey(ol.OpportunityId))
                    {
                        // Check for bundle
                        tempBundles = offerCodeFreq.get(ol.OpportunityId);
                        // Does bundle exist
                        if(tempBundles.containsKey(Integer.valueof(ol.Parent_Bundle_No__c)))
                        {
                            // Check the frequency of the Offer Code for a particular bundle in an opp
                            tempFreq = tempBundles.get(Integer.valueof(ol.Parent_Bundle_No__c));
                            if(tempFreq.containsKey(ol.PriceBookEntry.Product2.Wireless_Offer_Code__c))
                            {
                                tempFreq.put(ol.PriceBookEntry.Product2.Wireless_Offer_Code__c,tempFreq.get(ol.PriceBookEntry.Product2.Wireless_Offer_Code__c)+1);
                            }
                            else
                            {
                                tempFreq.put(ol.PriceBookEntry.Product2.Wireless_Offer_Code__c,1);
                            }
                        }
                        else
                        {
                            // No bundle found, thus no data
                            tempFreq = new map<string,integer>();
                            tempFreq.put(ol.PriceBookEntry.Product2.Wireless_Offer_Code__c,1);
                        }
                    }
                    else
                    {
                        // No Opp found, so no data added yet
                        tempBundles = new map<integer,map<string,integer>>();
                        tempFreq = new map<string,integer>();
                        tempFreq.put(ol.PriceBookEntry.Product2.Wireless_Offer_Code__c,1);
                    }
                    tempBundles.put(Integer.valueof(ol.Parent_Bundle_No__c),tempFreq);
                    offerCodeFreq.put(ol.OpportunityId,tempBundles);
                }
            }   
                                                    
            
            
            for(id oppId : offerCodeFreq.keyset())
            {
                map<integer,OpportunityLineItem> tempBundleLineMap = bundleLineMap.get(oppId);
                map<integer,map<string,integer>> tempBundles  = offerCodeFreq.get(oppId);
                system.debug('--------------tempBundles.keyset()---------'+tempBundles.keyset());
                for(integer bundleNo : tempBundles.keyset())
                {
                    if(tempBundleLineMap.containsKey(bundleNo))
                    {
                        OpportunityLineItem tempOli = tempBundleLineMap.get(bundleNo);
                        CallBundleService(tempOli.PricebookEntry.Product2.Name,tempOli.OpportunityId, tempOli.Opportunity.AccountId, tempBundles.get(bundleNo));
                    }
                }
            }
            
            
        
        }
    }
        
    

    public void syncOpportunityStatus(list<Apttus__APTS_Agreement__c> lstAgreements,Boolean isUpdate,map<ID,Apttus__APTS_Agreement__c> mapOldAgreements){
        
        list<Apttus__APTS_Agreement__c> lstProcessAgreements = new list<Apttus__APTS_Agreement__c> ();
        list<Opportunity> lstOpportunity = new list<Opportunity>();
        
        //RecordType objRecTypeOrder = [Select Id from RecordType where sObjectType = 'Opportunity' and isActive = true and DeveloperName = 'SMB_Care_Amend_Order_Opportunity' limit 1];
        set<Id> setOppId = new set<Id>();
        map<Id,Opportunity> mapOpportunity;
            for(Apttus__APTS_Agreement__c agg: lstAgreements){
                if(!isUpdate || (agg.APTS_Contract_Status__c != mapOldAgreements.get(agg.Id).APTS_Contract_Status__c || agg.Apttus__Related_Opportunity__c != mapOldAgreements.get(agg.Id).Apttus__Related_Opportunity__c)){
                    lstProcessAgreements.add(agg);
                    setOppId.add(agg.Apttus__Related_Opportunity__c);
                }
            }
            system.debug('SET OPP->'+setOppId);
        

        if(setOppId.size()>0){
            mapOpportunity = new map<Id,Opportunity>([Select Id, order_id__c,StageName, RecordTypeId from Opportunity where Id in:setOppId]);            
        }
        system.debug('MAP OPP->'+mapOpportunity );
        if(lstProcessAgreements.size()>0 && mapOpportunity!= null && mapOpportunity.size()>0){
            Opportunity objOpp;
            for(Apttus__APTS_Agreement__c agg: lstProcessAgreements){
            System.debug('CONTRACT STATUS->'+agg.APTS_Contract_Status__c);
            
                if(agg.Apttus__Related_Opportunity__c!= null && mapOpportunity.containsKey(agg.Apttus__Related_Opportunity__c)){
                    objOpp = mapOpportunity.get(agg.Apttus__Related_Opportunity__c);
                    System.debug('CONTRACT STATUS->'+agg.APTS_Contract_Status__c+', opportunity status:'+objOpp.stagename);
                    if(!(objOpp.StageName.equalsIgnoreCase('Contract On Hold (Customer)') || objOpp.StageName.equalsIgnoreCase('Order Request on Hold (Customer)')|| objOpp.StageName.equalsIgnoreCase('Order Request Cancelled'))){
                        if(agg.APTS_Contract_Status__c != null)
                        {
                            if(agg.APTS_Contract_Status__c.equalsIgnoreCase('Contract Created')){
                                
                                objOpp.StageName = 'Contract Created';
                                objOpp.Stage_update_time__c=System.Now();
                                
                                lstOpportunity.add(objOpp);
                            }
                            else if(agg.APTS_Contract_Status__c.equalsIgnoreCase('Contract Sent')){
                                
                                objOpp.StageName = 'Contract Sent';
                                objOpp.Stage_update_time__c=System.Now();
                                lstOpportunity.add(objOpp); 
                            }
                            else if(agg.APTS_Contract_Status__c.equalsIgnoreCase('Client Declined')){
                                
                                objOpp.StageName = 'Contract Declined';
                                objOpp.Stage_update_time__c=System.Now();
                                lstOpportunity.add(objOpp);
                            }
                            else if(agg.APTS_Contract_Status__c.equalsIgnoreCase('Fax Contract Validation') || agg.APTS_Contract_Status__c.equalsIgnoreCase('Fax Contract Validation Required')){
                                
                                objOpp.StageName = 'Contract Fax Validation';
                                objOpp.Stage_update_time__c=System.Now();
                                lstOpportunity.add(objOpp);
                            }
                            else if(agg.APTS_Contract_Status__c.equalsIgnoreCase('Contract Registered') && objOpp.StageName != 'Order Request New'){
                                
                                objOpp.StageName = 'Contract Registered';
                                objOpp.Stage_update_time__c=System.Now();
                                lstOpportunity.add(objOpp);
                            }
                            else if(agg.APTS_Contract_Status__c.equalsIgnoreCase('Acceptance Expired')){
                                
                                objOpp.StageName = 'Contract Acceptance Expired';
                                objOpp.Stage_update_time__c=System.Now();
                                lstOpportunity.add(objOpp);
                            }
                        }    
                    }
                }
            }
        }
        System.debug('COUNT->'+lstOpportunity.size());    
        if(lstOpportunity.size()>0){
            for(opportunity opp:lstOpportunity){
                system.debug('Opportunity ID:'+opp.id+', Order:'+opp.order_id__c+', Stage:'+opp.stagename);
            }
            update lstOpportunity;
        }
    }

    
    
    @future (callout=true)
    public static void CallBundleService(string productName,ID oppID, ID AccountId, String  OfferID)
    {   
        SMB_Bundle_CallOutHelper helper = new SMB_Bundle_CallOutHelper();
        helper.createBundleCommitmentHelper(productName, oPPID, AccountId, OfferID);
    }
    
    @future (callout=true)
    public static void CallBundleService(string productName,ID oppID, ID AccountId, map<string,integer> offerMap)
    {   
        system.debug('----------OfferMap---------'+OfferMap);
        SMB_Bundle_CallOutHelper helper = new SMB_Bundle_CallOutHelper();
        helper.createBundleCommitmentHelper(productName, oPPID, AccountId, OfferMap);
    }
    

    public void PrepareOrderRequest(list<ID> lstOpportunityIDs)
    {
   
        for (ID uniqueId : lstOpportunityIDs)
        {
            smb_OrderSubmit_Callout_Helper.SubmitOrderRequest(uniqueId);
        }   
    }*/
    
}