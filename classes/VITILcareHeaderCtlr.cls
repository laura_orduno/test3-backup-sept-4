public class VITILcareHeaderCtlr {
    
      public string userLanguage {get; set;}
    public VITILcareHeaderCtlr(){
      getLanguageLocaleKey();  
    }
    
 public String getLanguageLocaleKey() {
        //string userLanguage;
    if(userLanguage == null) {
        userLanguage = [SELECT LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()].LanguageLocaleKey;
    }
    return userLanguage;
}
}