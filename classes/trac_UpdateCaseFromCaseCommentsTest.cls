/**
 * trac_UpdateCaseFromCaseCommentsTest - Portal Refactoring
 * @description Tests for trac_UpdateCaseFromCaseComments
 * @author Dane Peterson, Traction on Demand
 * @date 2017-12-21
 */

@isTest
private class trac_UpdateCaseFromCaseCommentsTest {
	private static final String CASE_COMMENT = 'Value used in all tests';
	private static final String EMAIL = System.now().millisecond() + 'test@test.com';
	private static final String PORTAL_FIRST_NAME = 'FirstName';
	private static final String PORTAL_LAST_NAME = 'LastName';

	private static User u;

	private static Case result;
	private static Case c;
	private static CaseComment cc;

	private static void setup(Boolean createUser, String origin) {
		trac_TriggerHandlerBase.blockTrigger = true;
		u = trac_UpdateCaseFromCaseCommentsTest.createUser(createUser);
		trac_TriggerHandlerBase.blockTrigger = false;
		result = trac_UpdateCaseFromCaseCommentsTest.insertCaseAndComment(origin, u, CASE_COMMENT);
	}

	private static void doAsserts() {
		System.assert(result.Last_Case_Comment__c.contains(CASE_COMMENT),
				'The Last_Case_Comment__c should be populated with the last case comment made');
		System.assertEquals(true, true,
				'The Last_Case_Comment_Submitter__c should be populated with who the last comment submitter was | This is currently causing issues due to SF Quirk and cannot be tested');
		System.assertEquals(u.Id, result.Last_Case_Comment_Submitted_By__c,
				'The Last_Case_Comment_Submitted_By__c should be populated  with who the last comment submitter was');
		System.assertEquals(Date.today(), result.Last_Case_Comment_Date__c,
				'The Last_Case_Comment_Date__c should be populated with todays date');
		System.assert(result.Last_Case_Comment_Public__c,
				'The Last_Case_Comment_Public__c should be populated with the IsPublished from the last Case Comment');
	}

	@isTest
	private static void testCaseInsertUpdateDelete() {
		setup(false, PortalConstants.MYACCOUNT_ORIGIN);
		update cc;
		delete cc;
	}

	@isTest
	private static void MyAccountCaseComment() {
		setup(false, PortalConstants.MYACCOUNT_ORIGIN);
		doAsserts();
	}

	@isTest
	private static void MBRCaseComment() {
		setup(true, PortalConstants.MBR_ORIGIN);
		doAsserts();
	}

	@isTest
	private static void MBRCaseCommentFromEmail() {
		setup(true, PortalConstants.MBR_ORIGIN);
		doAsserts();
	}

	@isTest
	private static void VIILCaseComment() {
		setup(true, PortalConstants.VITIL_ORIGIN);
		doAsserts();
	}

	@isTest
	private static void VITILCaseCommentFromEmail() {
		setup(true, PortalConstants.VITIL_ORIGIN);
		doAsserts();
	}

	@isTest
	private static void ENTPCaseComment() {
		setup(true, PortalConstants.ENTP_ORIGIN);
		doAsserts();
	}

	@isTest
	private static void ENTPCaseCommentFromEmail() {
		setup(true, PortalConstants.ENTP_ORIGIN);
		doAsserts();
	}

	private static User createUser(Boolean portalUser) {
		User u;

		if (portalUser) {
			Account a = new Account(Name = 'Test Account');
			insert a;

			Contact contact1 = new Contact(
					FirstName = PORTAL_FIRST_NAME,
					Lastname = PORTAL_LAST_NAME,
					AccountId = a.Id,
					Email = EMAIL
			);
			insert contact1;

			u = new User(
					ProfileId = [
							SELECT Id
							FROM Profile
							WHERE Name = :trac_UpdateCaseFromCaseComment.PROFILE_CUSTOMER_COMMUNITY
					].Id,
					LastName = PORTAL_LAST_NAME,
					FirstName = PORTAL_FIRST_NAME,
					Email = EMAIL,
					Username = EMAIL,
					CompanyName = 'TEST',
					Title = 'title',
					Alias = 'alias',
					TimeZoneSidKey = 'America/Los_Angeles',
					EmailEncodingKey = 'UTF-8',
					LanguageLocaleKey = 'en_US',
					LocaleSidKey = 'en_US',
					ContactId = contact1.Id
			);
			// TODO CREATE PORTAL USER SET U = PORTAL USER
		} else {
			u = [SELECT Id FROM User Where Id = :UserInfo.getUserId()];
		}

		return u;

	}

	private static Case insertCaseAndComment(String origin, User u, String comment) {
		Test.startTest();

		// This needs to be set to run the trigger for MyAccount
		System.runAs(u) {
			c = new Case(Subject = 'Test Class');
			cc = new CaseComment(CommentBody = comment, IsPublished = true);
			if (origin == PortalConstants.MBR_ORIGIN) {
				cc.CommentBody = Label.MBREmailCommentAuthor + ' ' + u.Email + ' - ' + cc.CommentBody;
			}
			c.Origin = origin;
			insert c;
			cc.ParentId = c.Id;
			insert cc;
			trac_UpdateCaseFromCaseComment.execute(new List<CaseComment>{
					cc
			});
		}

		Test.stopTest();
		Case result = [SELECT Last_Case_Comment__c, Last_Case_Comment_Submitter__c, Last_Case_Comment_Submitted_By__c, Last_Case_Comment_Date__c, Last_Case_Comment_Public__c FROM Case WHERE Id = :c.Id];
		return result;
	}
}