@isTest
public class OrderSummaryControllerTest {
    private static void createData(){
        System.runAs(new User(Id = Userinfo.getUserId())) {
        List<SMBCare_WebServices__c> custSetting= new List<SMBCare_WebServices__c>();
        SMBCare_WebServices__c wsUsername = new SMBCare_WebServices__c(Name='username',value__c='APP_SFDC'); custSetting.add(wsUsername);
        SMBCare_WebServices__c wsPassword = new SMBCare_WebServices__c(Name='password',value__c='SOAORGID'); custSetting.add(wsPassword);
        SMBCare_WebServices__c SearchDueDate_EndPoint = new SMBCare_WebServices__c(Name='SearchDueDate_EndPoint',value__c='https://xmlgwy-pt1.telus.com:9030/is01/RMO/ProcessMgmt/FieldWorkAppointmentService_v2_0_vs0');
        SMBCare_WebServices__c BookDueDate_EndPoint = new SMBCare_WebServices__c(Name='BookDueDate_EndPoint',value__c='https://xmlgwy-pt1.telus.com:9030/is01/RMO/ProcessMgmt/FieldWorkAssignmentMgmtService_v2_0_vs0');
        custSetting.add(SearchDueDate_EndPoint);custSetting.add(BookDueDate_EndPoint); 
        insert custSetting;
        }
    }
   /* @isTest
    private static void testDebug(){
        try{
             String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        }catch(Exception e){
            System.debug(e.getStackTraceString());
            System.debug(e);
        }
        Test.startTest();
        OrdrTestDataFactory.associateContractWithOrder(orderId);
        Test.stopTest();
    }*/
    @isTest
    private static void testOrderSummaryController(){
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        PageReference pageRef = Page.OC_OrderSummaryMultiSite;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('parentOrderId', orderId );
        Order ordObj=[select id,General_Notes_Remarks__c from Order where id=:orderId];  
        ordObj.General_Notes_Remarks__c='test remarks';
        update ordObj;
        Test.startTest();
        OrdrTestDataFactory.associateContractWithOrder(orderId);
        List<Order> chOrdObjList=[select id from Order where parentId__C=:orderId];
        OrdrTestDataFactory.associateContractWithOrder(chOrdObjList.get(0).id);
        ApexPages.StandardController sc = new ApexPages.standardController(ordObj);
        OrderSummaryController cntrl=new OrderSummaryController(sc);
        Test.stopTest();
    }
    @isTest
    private static void updateOrder(){
        OrderSummaryController.updateOrder('test');
    }
    @isTest
    private static void emailOrderConfirmations(){
        OrderSummaryController.emailOrderConfirmations('test@test.com');
    }
    @isTest
    private static void saveOrderRemarks(){
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();        
        Test.startTest();
        OrderSummaryController.saveOrderRemarks(orderId, orderId+'=test');
        Test.stopTest();
    }
     @isTest
    private static void saveOrderRemarks1(){
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();        
        Test.startTest();
        String childOrd1=[select id from order where parentid__c=:orderId].id;
        OrderSummaryController.saveOrderRemarks(childOrd1, childOrd1+'=test');
        Test.stopTest();
    }
    @isTest
    private static void cancelOrders(){
        createData();
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        List<String> orderList=new List<String>();
        orderList.add(orderId);
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        OrdrTestDataFactory.associateContractWithOrder(orderId);
        OrderSummaryController.cancelOrders(orderList);
        Test.stopTest();
    }
    @isTest
    private static void submitOrders1(){
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        List<Order> childOrdList=[select id,Ordernumber,sub_order_number__c,parentid__r.ordernumber from order where parentid__c=:orderId];
        List<String> orderList=new List<String>();
        orderList.add(orderId);
        for(Order co:childOrdList){
            orderList.add(co.id);
            orderList.add(co.Ordernumber);
            orderList.add(co.sub_order_number__c);
            orderList.add(co.parentid__r.ordernumber);
        }
        Test.startTest();
		Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        OrdrTestDataFactory.associateContractWithOrder(orderId);
        OrderSummaryController.submitOrders(orderList,'test remarks', 'testOutlet', 'testsalesrep', 'testsalesreptid', false,'test@test.com');
        Test.stopTest();
        
    }
	  @isTest
    private static void submitOrders2(){
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        List<String> orderList=new List<String>();
        orderList.add(orderId);
        Test.startTest();
		Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        OrdrTestDataFactory.associateContractWithOrder(orderId);
        OrderSummaryController.submitOrders(orderList,'test remarks', 'testOutlet', 'testsalesrep', 'testsalesreptid', false,'test@test.com');
        Test.stopTest();
        
    }
      @isTest
    private static void submitOrders3(){
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        List<String> orderList=new List<String>();
        //orderList.add(orderId);
        Test.startTest();
		Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        OrdrTestDataFactory.associateContractWithOrder(orderId);
        List<Order> childOrdList=[select id from order where parentid__c=:orderId];
        orderList.add(childOrdList.get(0).id);
        OrderSummaryController.submitOrders(orderList,'test remarks', 'testOutlet', 'testsalesrep', 'testsalesreptid', true,'test@test.com');
        Test.stopTest();
        
    }
    @isTest
    private static void fetchMasterOrderSummaryDataJson(){
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        Test.startTest();
        OrdrTestDataFactory.associateContractWithOrder(orderId);
        OrderSummaryController.fetchMasterOrderSummaryDataJson(orderId,0,20);
        Test.stopTest();
    }
    @isTest
    private static void fetchChildOrderSummaryDataJson(){
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        Test.startTest();
        OrdrTestDataFactory.associateContractWithOrder(orderId);
         String childOrder='';
        List<Order> ordList=[select id from order where parentid__c=:orderId];
        childOrder=ordList.get(0).id;
		Order ordObj=[select order_submit_date__c,id from order where id=:childOrder];
		ordObj.order_submit_date__c=System.today();
        ordObj.OrderMgmt_BO_Remaraks__c='test remarks';
        update ordObj;
        ordObj.status='Submitted';
        update ordObj;
        
        OrderSummaryController.fetchChildOrderSummaryDataJson(childOrder);
        Test.stopTest();
        
        
    }
    @isTest
    private static void amendMasterOrder() {
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        PageReference pageRef = Page.OC_OrderSummaryMultiSite;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('parentOrderId', orderId );
        Order ordObj=[select id from Order where id=:orderId];
        ApexPages.StandardController sc = new ApexPages.standardController(ordObj);
        Test.startTest();
        OrdrTestDataFactory.associateContractWithOrder(orderId);
        OrderSummaryController.amendMasterOrder();
        Test.stopTest();
    }
    
    @isTest
    private static void isValidContract(){
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        Order ordObj=[select id from order where id=:orderId];
        Test.startTest();
        OrdrTestDataFactory.associateContractWithOrder(orderId);
        //OrderSummaryController.isValidContract(ordObj);
        Test.stopTest();
        
    }
    @isTest
    private static void getOffers(){
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        String childOrder='';
        List<Order> ordList=[select id from order where parentid__c=:orderId];
        childOrder=ordList.get(0).id;
        Test.startTest();
        OrdrTestDataFactory.associateContractWithOrder(orderId);
        OrderSummaryController cntrl=new OrderSummaryController();
        //cntrl.getOffers(childOrder);
        Test.stopTest();    
    }
    @isTest
    private static void isValidCreditAssessment(){
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        Order childOrder=null;
        List<Order> ordList=[select id from order where parentid__c=:orderId];
        childOrder=ordList.get(0);
        Test.startTest();
        OrdrTestDataFactory.associateContractWithOrder(orderId);
		//OrderSummaryController.isValidCreditAssessment(childOrder);
        Test.stopTest();  
    }
    @isTest
    private static void isValidShippingAddress(){
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        Order childOrder=null;
        List<Order> ordList=[select id from order where parentid__c=:orderId];
        childOrder=ordList.get(0);
        Test.startTest();
        OrdrTestDataFactory.associateContractWithOrder(orderId);
        //OrderSummaryController.isValidShippingAddress(childOrder);
        Test.stopTest();
    }
    @isTest
    private static void isValidBillingAddress() {
         String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        Order childOrder=null;
        List<Order> ordList=[select id from order where parentid__c=:orderId];
        childOrder=ordList.get(0);
        Test.startTest();
        OrdrTestDataFactory.associateContractWithOrder(orderId);
        //OrderSummaryController.isValidBillingAddress(childOrder);
        Test.stopTest();
    }
    @isTest
    private static void isDueDateBookingComplete(){
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        Order childOrder=null;
        List<Order> ordList=[select (select start_date__c,install_type__c, status__c, time_slot__c, 
                                 lastmodifieddate, Product_Technology_Code__c, Job_Type__c, 
                                 Permanent_Comments__c, Duration__c, WFM_Number__c,
                                 Scheduled_Due_Date_Location__c, Onsite_Contact_HomePhone__c,
                                 Onsite_Contact_Name__c, Onsite_Contact_Email__c,
                                 Onsite_Contact_CellPhone__c, Location_Contact__c, Scheduled_Datetime__c from Work_Orders__r ORDER BY CREATEDDATE DESC limit 1),id,captureLatestOnsiteContact__c from order where parentid__c=:orderId];
        String jsonStr='{ 	"sameAsOrderContact": false, 	"onsitePhoneMoveOut": "", 	"onsitePhone": "123123123", 	"onsiteEmailMoveOut": "", 	"onsiteEmail": "test@test.com", 	"onsiteContactMoveOut": "", 	"onsiteContact": "test", 	"installRemarksMoveOut": "", 	"installRemarks": "" }';
        for(Order chOrd:ordList){
            chOrd.captureLatestOnsiteContact__c=jsonStr;
        }
        update ordList;
        childOrder=ordList.get(0);
        
        Test.startTest();
        OrdrTestDataFactory.associateContractWithOrder(orderId);
        OrderSummaryController.isDueDateBookingComplete(childOrder);
        Test.stopTest();
    }
    @isTest
    private static void isDueDateBookingComplete1(){
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        Order childOrder=null;
        List<Order> ordList=[select (select start_date__c,install_type__c, status__c, time_slot__c, 
                                 lastmodifieddate, Product_Technology_Code__c, Job_Type__c, 
                                 Permanent_Comments__c, Duration__c, WFM_Number__c,
                                 Scheduled_Due_Date_Location__c, Onsite_Contact_HomePhone__c,
                                 Onsite_Contact_Name__c, Onsite_Contact_Email__c,
                                 Onsite_Contact_CellPhone__c, Location_Contact__c, Scheduled_Datetime__c from Work_Orders__r ORDER BY CREATEDDATE DESC limit 1),id,captureLatestOnsiteContact__c from order where parentid__c=:orderId];
        
        childOrder=ordList.get(0);
        
        Test.startTest();
        OrdrTestDataFactory.associateContractWithOrder(orderId);
        OrderSummaryController.isDueDateBookingComplete(childOrder);
        Test.stopTest();
    }
	@isTest
	private static void auxiliaryTest1(){
	String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        PageReference pageRef = Page.OC_OrderSummaryMultiSite;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('parentOrderId', orderId );
	   Test.startTest();
	   OrderSummaryController.OrderHistoryFriendlyRecord obj1=new OrderSummaryController.OrderHistoryFriendlyRecord();
	    obj1.AgentFullName='';
        obj1.AgentEmailAddress='';
        obj1.FieldUpdateDateTime=null;
        obj1.FieldUpdateDateTimeText='';
        obj1.FieldName='';
        obj1.OldValue=null;
        obj1.NewValue=null;
		OrderSummaryController.OrderResponse obj2=new OrderSummaryController.OrderResponse();
		obj2.validationMessage='';
		
		OrderSummaryController.ContractDetails obj3=new OrderSummaryController.ContractDetails();
		obj3.contractNum='';
        obj3.contractAddress='';
        obj3.customerSignor='';
        obj3.signorEmail='';
        obj3.registeredDate=null;
        obj3.expiredDate=null;
		
		OrderSummaryController.OrderDetails obj4=new OrderSummaryController.OrderDetails();
		OrderSummaryController.ServiceCnt obj5=new OrderSummaryController.ServiceCnt();
		List<OrderSummaryController.ServiceCnt> cntList=new List<OrderSummaryController.ServiceCnt>();
		cntList.add(obj5);
		obj4.serviceCnt=cntList;
		obj4.creditModifiedDate=null;
		
		OrderSummaryController cntrl=new OrderSummaryController();
		System.debug(cntrl.opportunityWorkRequestJson);
		System.debug(cntrl.accountWorkRequestJson);
		System.debug(cntrl.workRequestRecordTypeId);
		System.debug(cntrl.orderWorkRequestJson);
		
		cntrl.childOrderDataJson='';
		cntrl.masterOrderDataJson='';
		cntrl.sendEmailFlag=false;
		cntrl.selectedEmailIds='';
		cntrl.ordrIdStr='';
		cntrl.validBillingAddress=false;
		cntrl.validShippingAddress=false;
		cntrl.validCreditAssessment=false;
		cntrl.validContract=false;
		
		
		Test.stopTest();
	}
	
	@isTest
	private static void checkOrderType(){
	String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        PageReference pageRef = Page.OC_OrderSummaryMultiSite;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('parentOrderId', orderId );
		Test.startTest();
        OrdrTestDataFactory.associateContractWithOrder(orderId);
		OrderSummaryController cntrl=new OrderSummaryController();
		cntrl.checkOrderType();
		Test.stopTest();
	}
    @isTest
    private static void getOffersTest(){
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        Test.startTest();
        
		OrderSummaryController cntrl=new OrderSummaryController();
        String child=[select id from order where parentid__C=:orderId limit 1].id;
		cntrl.getOffers(child);
		Test.stopTest();
    } 
     @isTest
    private static void isValidCreditAssessmentTest(){
        String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        Test.startTest();
        
		//OrderSummaryController cntrl=new OrderSummaryController();
        Order ordOb=[select id from order where id=:orderId limit 1];
		OrderSummaryController.isValidCreditAssessment(ordOb);
		Test.stopTest();
    }
    
    /*	
public String workRequestRecordTypeId{
public String accountWorkRequestJson {get{
public String opportunityWorkRequestJson {get{
public String orderWorkRequestJson {get{
public OrderSummaryController(ApexPages.StandardController stdController){
public static List<Order> getOrder(List<String> ordNumList) {
public PageReference checkOrderType() {*/
}