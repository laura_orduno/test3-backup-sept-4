/**
    Controller Extension of OCOM_OrderPdfNewWrapper VF page
    @author Vlocity
    @version 02/26/2016

*/


public with sharing class OCOM_OrderPdfNewWrapperContExtention {

    public boolean displayModal {get; set;}     
    public Order o {get;set;}
    public String CCEmails{get;set;}
    public Contact tempCont{get;set;}
    //private QuoteDocument doc;
    Attachment attachment;

    public OCOM_OrderPdfNewWrapperContExtention(ApexPages.StandardController stdController) {
        this.o = (Order)stdController.getRecord();
        if(o.id!=null){
            o= [   select 
                    Id, 
                    Name, 
                    Order_Number__c,
                    OrderNumber,
                    Status,
                    OpportunityId, 
                    Opportunity.Bundle_Type__c,
                    Opportunity.Send_Quote_Additional_Email__c,
                    CustomerAuthorizedById,
                    CustomerAuthorizedBy.Email  
                    from Order 
                    where Id = :o.Id];
        }
    }

    public void SavePDF() {
         resetMessages();
         List<Attachment> lisAtt = [select Name 
                                    from Attachment 
                                    where parentId = :o.id and name like 'Q%-V%' order by CreatedDate desc];
        Integer versionNum = 1;
        if(lisAtt.size() >0){
            String n = lisAtt[0].name;
            String vNumString = n.substring(n.length()-5, n.length()-4);
            try{
                Integer vNum = Integer.valueOf(vNumString);
                if(vNum !=null){
                    versionNum = vNum + 1;
                }
            } catch(Exception e){}
            
        }


         PageReference pr = Page.OrderPdf_New;
         pr.getParameters().put('id', o.id);
         Blob cont;
         if(!Test.isRunningTest()){
            cont = pr.getContentAsPDF();
         }else {
            cont =Blob.valueOf('FOR UNIT TEST');
         }

         attachment = new Attachment(parentId = o.id, name=o.Order_Number__c +'-V'+versionNum + '.pdf', body = cont);


         try{
            insert  attachment;
            
            //q.Is_PDF_Generated__c = true;
            //update  q;
         }catch (DmlException de){

         }         
         //SuccessMsg='New version of Quote PDF was successfully saved.';
    }

    ///*
    //    Originally from  smb_QuoteToPdfStatusController.cls
    //    reusing sendEmail()  
    //    Vlocity Feb 26 2016
    //    Modified to 
    //    - Select EmailTempalte from CustomSetting
    //    - Populate Quote.ContactId to TO: 
    //*/

    ///*
    //* @author       : Puneet Khosla
    //* @method Name  : sendEmail()
    //* @description  : Function to Send Email
    //* @return       : void
    //* @param        : None    
    //*/

    public String ErrorMsg {get;set;}
    public String SuccessMsg {get;set;}

    private void resetMessages(){
        ErrorMsg=null;
        SuccessMsg = null;
    }

    public void sendEmail()
    {
        resetMessages();
        displayModal = false;
        //Checking Quote has ContactId 
        if(o.CustomerAuthorizedById== null){
            errorMsg = 'Error sending email: Contact is not specified.';
        }

        if(attachment == null){
                    List<Attachment> lisAtt = [select Name,Body
                                    from Attachment 
                                    where parentId = :o.id and name like 'Q%-V%' order by CreatedDate desc];
            if(lisAtt.size()>0){
                attachment = lisAtt[0];
            }
            else {
                errorMsg = 'Error sending email: PDF has not generated yet.';
            }
        }


        if(String.IsBlank(errorMsg))
        {
            String oppBundleType; 
            try
            {
                
                map<string,SMB_QuoteEmailTemplates__c> emailTemplates = SMB_QuoteEmailTemplates__c.getAll();
                
                string emailTemplateName;
                Id emailTemplateId; 
                
                if(String.isNotBlank(oppBundleType) & emailTemplates.containsKey(oppBundleType))
                {
                    //emailTemplateName = emailTemplates.get(opp.Bundle_Type__c).TemplateName__c;
                    emailTemplateName = emailTemplates.get(oppBundleType).TemplateName__c;
                }
                else
                {
                    emailTemplateName = emailTemplates.get('Default Order PDF').TemplateName__c;
                }
                
                
                if(String.IsBlank(emailTemplateName))
                {
                    errorMsg = 'Missing Email Template.';
                }
                else
                {
                    
                    
                    EmailTemplate emlTemp ;
                    if(!Test.isRunningTest()){
                        emlTemp= [Select Id,Subject,body From EmailTemplate e WHERE DeveloperName = :emailTemplateName LIMIT 1].get(0); 
                        emailTemplateId = Id.ValueOf(emlTemp.Id);
                    }


                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    List<String> lstEmail = new List<String>();
                    List<String> lstCCEmail = new List<String>();
                        
                    //lstEmail.add(opp.Send_Quote_Contact__r.email);
                    //System.debug('#### Email: q.Contact.Email='+q.Contact.Email + '+q.ContactId=' +q.ContactId );
                    if(String.isNotEmpty(o.CustomerAuthorizedById)){
                        Contact c = [select email from Contact where id = :o.CustomerAuthorizedById];
                        lstEmail.add(c.Email);
                    }else {
                        errorMsg = 'Contact is not specified.';
                        return; 
                    }
                
                    if(String.IsNotEmpty(CCEmails)){
                        lstCCEmail = CCEmails.split(';');
                        if(lstCCEmail.size()>0){
                            mail.setCcAddresses( lstCCEmail);
                        }
                    }


                    mail.setToAddresses(lstEmail);
                    
                    
                    mail.setSenderDisplayName('noreply@salesforce.com');
                    mail.setReplyTo('noreply@salesforce.com');
                    
                    mail.setTargetObjectId(o.CustomerAuthorizedById);

                    mail.setWhatId(o.OpportunityId);

                    if(!Test.isRunningTest()){
                        mail.setTemplateId(emailTemplateId);
                    }
            
                    Messaging.EmailFileAttachment pdfAttc = new Messaging.EmailFileAttachment();
                    
                    
                    pdfAttc.setFileName(attachment.name);
                    pdfAttc.setBody(attachment.body);
                    // set to inline 
                    pdfAttc.setInline(true);

                    mail.setFileAttachments(new Messaging.EmailFileAttachment[]{pdfAttc});
                               
                    if(!Test.isRunningTest()){
                       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                    }
                    
                    // Set Quote Status to Sent
                    //q.status = 'Sent';
                    //q.Activities__c = ' Sent to '+lstEmail[0]+' on ' + date.today().format();
                    //try{
                    //    update q; 
                    //}catch (DMLException de){
                    //    errorMsg = 'Not able to quote status to Sent.';
                    //}
                    // create Task as Email
                   
                    //Task t = new Task (WhatId = q.id, WhoId = q.contactId, status='Completed', ActivityDate = Date.Today());
                    //if(!Test.isRunningTest()){
                    //    t.subject = 'Email: '+emlTemp.Subject;

                    //    if(String.IsNotEmpty(CCEmails)){
                    //        t.Description = 'CC: '+ CCEmails;
                    //    }
                    //    //t.Description += '\r\n' +emlTemp.body;
                    //}
                    //try{
                    //    insert t; 
                    //}catch (DMLException de){
                    //    errorMsg = 'Error creating task.';
                    //}

                   
                }
            }
            catch(Exception e)
            {
                errorMsg = ' Error in sending email. ' + e.getMessage();
            }
            SuccessMsg='The email was successfully sent.';
        }
                
    }
    
    public void openModal() {        
        displayModal= true;    
    }     
    public void hideModal() {        
        displayModal = false;    
    }
    

}