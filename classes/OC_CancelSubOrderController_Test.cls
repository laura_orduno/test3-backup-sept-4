@istest(seealldata=false)
private class OC_CancelSubOrderController_Test {
    private static void createData(){
        System.runAs(new User(Id = Userinfo.getUserId())) {            
            SMBCare_WebServices__c cs = new SMBCare_WebServices__c(Name='EmailAddress',Value__c='test@test.com'); 
            insert cs;            
            //Custom Setting for username Field
            SMBCare_WebServices__c cs1 = new SMBCare_WebServices__c(Name='username',Value__c ='APP_SFDC'); 
            insert cs1;
            //Custom Setting for password Field
            SMBCare_WebServices__c cs2 = new SMBCare_WebServices__c(Name='password',Value__c='soaorgid'); 
            insert cs2; 
            OrdrTestDataFactory.createOrderingWSCustomSettings('CancelOrderEndpoint', 'https://webservice1.preprd.teluslabs.net/CMO/OrderMgmt/CancelCustomerOrder_v2_1_vs1');
         }
    }
    
    static testMethod void testMethod1() {
        createData();
        
        Schema.DescribeSObjectResult d = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
        Schema.RecordTypeInfo rtByName =  rtMapByName.get('CAN');
        system.debug(' id'+rtByName.getRecordTypeId());
        Id recTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Billing').getRecordTypeId();
        Id recSerTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        Id recRCIDTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId();
        
        
        Account RCIDacc = new Account(name='debs',recordtypeid=recRCIDTypeId);
        Account acc = new Account(name='Billing account',BAN_CAN__c='draft',recordtypeid=recTypeId);
        Account seracc = new Account(name='123 street',recordtypeid=recSerTypeId,parentid=RCIDacc.id);
        
        
        List<Account> acclist= new List<Account>();          
        acclist.add(RCIDacc);
        acclist.add(acc);
        acclist.add(seracc );
        insert acclist;
        
        smbcare_address__c address= new smbcare_address__c(account__c=seracc.id); 
        
        
        // First, set up test price book entries.
        // Insert a test product.
        Product2 prod = new Product2(Name = 'Laptop X200', 
        Family = 'Hardware');
        insert prod;
        
        // Get standard price book ID.
        // This is available irrespective of the state of SeeAllData.
        Id pricebookId = Test.getStandardPricebookId();
        
        // 1. Insert a price book entry for the standard price book.
        // Standard price book entries require the standard price book ID we got earlier.
        PricebookEntry standardPrice = new PricebookEntry(
        Pricebook2Id = pricebookId, Product2Id = prod.Id,
        UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        // Create a custom price book
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        // 2. Insert a price book entry with a custom price.
        PricebookEntry customPrice = new PricebookEntry(
        Pricebook2Id = customPB.Id, Product2Id = prod.Id,
        UnitPrice = 12000, IsActive = true);
        insert customPrice;
        contact c= new contact(firstname='abc',lastname='xyz');
        insert c;
        List< Credit_Assessment__c> CARList= new List<Credit_Assessment__c>();
        Credit_Assessment__c CAR= new Credit_Assessment__c(CAR_Status__c='test',CAR_Result__c='Denied');CARList.add(CAR);
        Credit_Assessment__c CAR2= new Credit_Assessment__c(CAR_Status__c='test',CAR_Result__c='Denied');CARList.add(CAR2);
        insert CARList;
        order masterOrder = new order(Credit_Assessment__c=car.id,Ban__c='draft',Reason_Code__c='34aas',General_Notes_Remarks__c='123123',accountId=seracc.id,effectivedate=system.today(),status='Draft',Pricebook2Id=customPB.id,CustomerAuthorizedById=c.id,service_address__c=address.id);   
        insert masterOrder;
        order ord = new order(parentId__c = masterOrder.Id, Credit_Assessment__c=car.id,Ban__c='draft',Reason_Code__c='34aas',General_Notes_Remarks__c='123123',accountId=seracc.id,effectivedate=system.today(),status='Draft',Pricebook2Id=customPB.id,CustomerAuthorizedById=c.id,service_address__c=address.id);   
        order ord2 = new order(parentId__c = masterOrder.Id, Credit_Assessment__c=car2.id,Ban__c='draft',Reason_Code__c='34aas',General_Notes_Remarks__c='123',accountId=acc.id,effectivedate=system.today(),status='Draft',Pricebook2Id=customPB.id,CustomerAuthorizedById=c.id,service_address__c=address.id);   
        List<Order>ordlist= new List<Order>();   
        ordList.add(ord);ordList.add(ord2);
        insert ordList; 
        
        
        
        orderItem  OLI2= new orderItem(UnitPrice=12,orderId= ord2.Id,orderMgmt_BPI_Id__c='9871379163517',vlocity_cmt__BillingAccountId__c=acc.id,vlocity_cmt__ServiceAccountId__c=seracc.id,Quantity=2,PricebookEntryId=customPrice.id);
        orderItem  OLI= new orderItem(UnitPrice=12,orderId= ord.Id,vlocity_cmt__AssetReferenceId__c=OLI2.id,vlocity_cmt__BillingAccountId__c=acc.id,vlocity_cmt__ServiceAccountId__c=seracc.id,Quantity=2,PricebookEntryId=customPrice.id);
        
        List<OrderItem>OLIlist= new List<OrderItem>();   
        OLIList.add(OLI2);OLIList.add(OLI);
        //insert OLIList;
        asset ast= new asset(name='test',accountID=RCIDacc.id,vlocity_cmt__OrderId__c=ord2.id,status='Shipped');
        insert ast;
        ord.status = 'Not Submitted';
        update ord;
        //ord2.Status = 'Submitted';
        //update ord2;
        String ordNum1='';
        String ordNum2='';
        String ordId1='';
        String ordId2='';
        try{
            List<Order> ordList1= [Select Id, OrderNumber From Order where Id IN: ordList];
            ordNum1 =  ordList1[0].OrderNumber;
            ordNum2 =  ordList1[1].OrderNumber;
            ordId1 =  ordList1[0].Id;
            ordId2 =  ordList1[1].Id;
        }catch(Exception ex){
        }
        Test.startTest();
            OC_CancelSubOrderController.cancelEnvelope(ordid1+','+ordId2);
            PageReference pageref = Page.OC_CancelSubOrder;
            Test.setCurrentPage(pageref);        
            pageref.getparameters().put('id',masterOrder.Id);
            pageref.getparameters().put('selectedChildOrderIds',ordNum1 +','+ordNum2);
            ApexPages.StandardController sc_1 = new ApexPages.standardController(masterOrder);
            OC_CancelSubOrderController corder= new OC_CancelSubOrderController(sc_1);
            //corder.dummyOrder = new Order();
            corder.dummyOrder.Reason_Code__c = 'test1';
            corder.dummyOrder.General_Notes_Remarks__c = 'test1';
            corder.DoOK();
            corder.invalidForm();
            corder.CancelButton();
            corder.showMessage();
        Test.stopTest();
    }
}