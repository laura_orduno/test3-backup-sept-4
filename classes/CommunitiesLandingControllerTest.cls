/**
 * An apex page controller that takes the user to the right start page based on credentials or lack thereof
 */
@IsTest public with sharing class CommunitiesLandingControllerTest {
    @IsTest public static void testCommunitiesLandingController() {
        // Instantiate a new controller with all parameters in the page
        CommunitiesLandingController controller = new CommunitiesLandingController();
      }
    
     private static void createData(){
          System.runAs(new User(Id = Userinfo.getUserId())) {
              OrdrTestDataFactory.createOrderingWSCustomSettings('ChannelPortalCacheMgmtEndPoint', 'https://webservice1.preprd.teluslabs.net/RMO/ResourceMgmt/ReserveResource_v2_0_vs1_DV');
              Account a = new Account(Name='TestAccount', CustProfId__c='123456',Channel_Org_Code__c='testV');
              insert a;
              Contact testContact = new Contact(LastName='TestContact',Phone = '9876543281' ,
                                                Email = 'TestMail@telus.com', 
                                                HomePhone = '6476486478', 
                                                MobilePhone = '6476486478');
              testContact.accountid=a.id;
              insert testContact;
              User userObj=[SELECT UserRoleId,Id, ContactId, Contact.Account.Channel_Org_Code__c,FederationIdentifier FROM User WHERE id=:Userinfo.getUserId()];
              userObj.FederationIdentifier=Userinfo.getUserId();
              //userObj.ContactId=testContact.id;
             
              UserRole role = new UserRole(name = 'Test Manager');    
              insert role;
              
              userObj.UserRoleId=role.id;
               update userObj;
              
          }
      }
    @isTest
    private static void updateContactTest(){
               createData();  
        Test.startTest();
         PageReference pageref = Page.PartnerCommunitiesLogin;
        Test.setCurrentPage(pageref);       
        String cacheKey='1234567';
        pageref.getparameters().put('key', cacheKey);
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
         CommunitiesLandingController cntrl=new CommunitiesLandingController();
        String accountId=[select id from Account limit 1].id;
        String contactId=[select id from Contact limit 1].id;
            ChannelPortalCacheMgmtSrvReqRes.GetCacheListResponseType resp = ChannelPortalCacheMgmtWSCallout.getCacheList(cacheKey);
            
            ChannelPortalCacheMgmtSrvReqRes.MapItem[] customCacheList = resp.customCacheList;
            
           Map<String,String> keyValMap = new Map<String,String>();
            for(ChannelPortalCacheMgmtSrvReqRes.MapItem mapItemObj:customCacheList){
                if(String.isNotBlank(mapItemObj.key) && 'channelOrgSalesReps_JSON'.equalsIgnoreCase(mapItemObj.key)){
                    continue;
                }
                if(String.isNotBlank(mapItemObj.key) && 'channelOrgOutlets_JSON'.equalsIgnoreCase(mapItemObj.key)){
                    continue;
                }
                keyValMap.put(mapItemObj.key, mapItemObj.value); 
            }
        String baseUrl='http://telus.com';
        CommunitiesLandingController.updateContact(keyValMap, contactId, accountId);
        Test.stopTest();
    }
     @isTest
    private static void updateUserTest(){
        createData();  
        Test.startTest();
         PageReference pageref = Page.PartnerCommunitiesLogin;
        Test.setCurrentPage(pageref);       
        String cacheKey='1234567';
        pageref.getparameters().put('key', cacheKey);
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
         CommunitiesLandingController cntrl=new CommunitiesLandingController();
        String uuid=UserInfo.getUserId();
        String roleName=[select id,name from UserRole limit 1].name;
            //ChannelPortalCacheMgmtSrvReqRes.GetCacheListResponseType resp = ChannelPortalCacheMgmtWSCallout.getCacheList(cacheKey);
            
           // ChannelPortalCacheMgmtSrvReqRes.MapItem[] customCacheList = resp.customCacheList;
            
           Map<String,String> keyValMap = new Map<String,String>();
        keyValMap.put('portalUserID','testV');
        keyValMap.put('firstName','testV');
        keyValMap.put('lastName','testV');
        keyValMap.put('portalRoleName','Test Manager');
        keyValMap.put('currentChannelOutletID','testV');
        keyValMap.put('loginUserID',UserInfo.getUserId());
           /* for(ChannelPortalCacheMgmtSrvReqRes.MapItem mapItemObj:customCacheList){
                if(String.isNotBlank(mapItemObj.key) && 'channelOrgSalesReps_JSON'.equalsIgnoreCase(mapItemObj.key)){
                    continue;
                }
                if(String.isNotBlank(mapItemObj.key) && 'channelOrgOutlets_JSON'.equalsIgnoreCase(mapItemObj.key)){
                    continue;
                }
                keyValMap.put(mapItemObj.key, mapItemObj.value); 
            }*/
        String baseUrl='http://telus.com';
        CommunitiesLandingController.updateUser(baseUrl,  keyValMap, uuid, roleName,cacheKey);
        Test.stopTest();
    }
   /*  @isTest
    private static void updateRoleTest(){
        createData();  
        Test.startTest();
        CommunitiesLandingController cntrl=new CommunitiesLandingController();
        String uuid=UserInfo.getUserId();
        String roleId=[select id from UserRole limit 1].id;
        cntrl.updateRole(uuid, roleId);
        Test.stopTest();
        
    }*/
@isTest
    private static void forwardToLandingPageTest(){
        createData();        
        Test.startTest();
        
        PageReference pageref = Page.PartnerCommunitiesLogin;
        Test.setCurrentPage(pageref);        
        pageref.getparameters().put('key', '1234567');
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        CommunitiesLandingController cntrl=new CommunitiesLandingController();
        cntrl.forwardToStartPage();
        Test.stopTest();
    }
   
}