/*
    *******************************************************************************************************************************
    Class Name:     OC_LandingPageMyTaskController
    Purpose:        
    Test Class Name : OC_LandingPageMyTaskControllerTest
    Created by:      Sandip Chaudhari        06-Nov-2017     
  
    *******************************************************************************************************************************
*/
global without sharing class OC_LandingPageMyTaskController{
     global OC_LandingPageMyTaskController(){
     }
     
     @RemoteAction
     global static List<TaskDetails> getMyTasks(){
         List<TaskDetails> taskDetails = new List<TaskDetails>();
         Id currentUserId = UserInfo.getUserId();
         List<Id> salesRepIds = DealerPortalUtil.getDealerSalesRepIdsFromCache();
         User userObj = [SELECT Id, Name, profile.Name, UserRole.Name FROM User WHERE Id=:userInfo.getUserId() LIMIT 1];
		 String roleName = userObj.UserRole.Name;
         List<Task> myTasks = new List<Task>();
         Set<String> dealeradminPricipalRoleSet = DealerPortalUtil.getDealerAdminPricipalRoles();
         System.debug('@@@ dealeradminPricipalRoleSet ' + dealeradminPricipalRoleSet);
         if(dealeradminPricipalRoleSet.contains(roleName)){
         	  myTasks = [SELECT Status, ActivityDate, Subject, WhoId,Who.Name, WhatId, What.Name, 
                    AccountId, Account.Name, Account.ID, Priority
                    FROM Task 
                    WHERE Status != 'Completed' AND (OwnerId =: currentUserId OR OwnerId IN: salesRepIds)
                    ORDER BY ActivityDate ASC limit 10000];
         }else{
              myTasks = [SELECT Status, ActivityDate, Subject, WhoId,Who.Name, WhatId, What.Name, 
                    AccountId, Account.Name, Account.ID, Priority
                    FROM Task 
                    WHERE Status != 'Completed' AND OwnerId =: currentUserId  
                    ORDER BY ActivityDate ASC limit 10000];
         }
         if(myTasks != null && myTasks.size()>0){
             for(Task taskObj: myTasks){
                 TaskDetails taskDtlObj = new TaskDetails();
                 taskDtlObj.taskId = taskObj.Id;
                 if(String.IsNotBlank(taskObj.subject)){
                     taskDtlObj.subject = taskObj.subject;
                 }else{
                     taskDtlObj.subject = '-';
                 }
                 taskDtlObj.status = taskObj.status;
                 taskDtlObj.activityDate = taskObj.activityDate;
                 if(taskObj.ActivityDate != null && taskObj.ActivityDate < System.Today()){
                     taskDtlObj.activityDateColor = 'color:#FF0000';
                 }
                 taskDtlObj.whoId = taskObj.whoId;
                 taskDtlObj.whoName = taskObj.who.Name;
                 taskDtlObj.whatId = taskObj.whatId;
                 taskDtlObj.whatName = taskObj.what.Name;
                 taskDtlObj.accountId = taskObj.accountId;
                 taskDtlObj.accuntName = taskObj.Account.Name;
                 taskDtlObj.priority = taskObj.priority;
                 taskDetails.add(taskDtlObj);
             }
         }
         return taskDetails;
     }
     
     global Class TaskDetails{
         public Id taskId{get;set;}
         public string status{get;set;}
         public string subject{get;set;}
         public DateTime activityDate{get;set;}
         public string activityDateColor{get;set;}
         public string whoId{get;set;}
         public string whoName{get;set;}
         public string whatId{get;set;}
         public string whatName{get;set;}
         public string accountId{get;set;}
         public string accuntName{get;set;}
         public string priority{get;set;}
     }
}