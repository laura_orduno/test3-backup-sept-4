/********************************************************************************************************************************
Class Name:     OC_GetServiceLocationsAndcGTAController 
Purpose:        Controller for checkTechnicalAvailability Component       
TestClass Name: OC_GetServiceLocationsAndcGTACntrl_Test
Created by:     Aditya Jamwal(IBM)     06-June-2017
Modified By: Santosh
*********************************************************************************************************************************
*/
global class OC_GetServiceLocationsAndcGTAController extends vlocity_cmt.VlocityContinuationIntegration implements vlocity_cmt.VlocityOpenInterface2 {
    
    global static String rid{get;set;}
    global static String rcidName{get;set;}
    global static List<addressWrapper> allServiceAddressList {get; set;}
    global static String customerSolutionId{get;set;}
    
    global override Object invokeMethod(String methodName, Map<String, Object> input, Map<String, Object> output, Map<String, Object> options)  {
        system.debug('!!@@@ input '+methodName +'   '+input);
        try{
            if (methodName.indexOf('getAllServiceAddressForRCID') != -1) {
                getAllServiceAddressForRCID(input, output, options);           
            }
        }catch(exception ex){
            output.put('error',ex.getStackTraceString());
        }
        return null;
    }

    private void getAllServiceAddressForRCID(Map<String, Object> input, Map<String, Object> output, Map<String, Object> options){
        
        system.debug('input '+input);
        String RCID = (String) input.get('rcid');
        String customerSolutionId = (String) input.get('customerSolutionId');
        String pageType = (String) input.get('pageType');
        output.put('addressesObj',getAllServiceAddressForRCID(RCID,customerSolutionId,pageType));
        system.debug('output '+output);
        
    }
    global OC_GetServiceLocationsAndcGTAController() {
        if(NULL != ApexPages.currentPage()){
        rid = apexpages.currentpage().getparameters().get('rcid');
        customerSolutionId = apexpages.currentpage().getparameters().get('parentOrderId');
        system.debug(' customerSolutionId 0 '+customerSolutionId);
        
        if(string.isNotBlank(rid)){
          List<account> acclist =  [select id,name from account where id =:rid];
            if(null != acclist && acclist.size()>0){
               rcidName = acclist[0].name;
            }
        }
         allServiceAddressList = new List<addressWrapper>();        
        } 
    }
    
    @RemoteAction
    global static Object getAllServiceAddressForRCID(string RCID, string customerSolutionId,string pageType){
        
        allServiceAddressList = new List<addressWrapper>();
        RCIDAddressesWrapper addressesObj = new RCIDAddressesWrapper();
       Set<String> existingOrdersAddressIds = new Set<String>();
       Set<String> ilecProvinces = new Set<String>();
        for(Country_Province_Codes__c ilecProvince : [Select id,Name,is_ILEC__c from Country_Province_Codes__c where is_ILEC__c = true]){
            if(ilecProvince.is_ILEC__c){
                ilecProvinces.add(ilecProvince.Name);
            } 
        }
       system.debug('!!allServiceAddressList 0'+ RCID);
       system.debug(' pageType 1 '+ pageType );
        if(String.IsNotBlank(customerSolutionId)){
            List<Order> customerSolutionOrder =[Select id,type from Order where id =:customerSolutionId];
         if(null !=customerSolutionOrder && customerSolutionOrder.size()>0){
                 addressesObj.orderType = customerSolutionOrder[0].Type;
                 system.debug('@@test -1 '+ addressesObj.orderType);
         }
            
    for(Order ordr : [select id,parentId__c,Type,service_address__c,service_address__r.Address__c,service_address__r.Suite_Number__c,service_address__r.Street_Address__c,
        service_address__r.AddressText__c,service_address__r.City__c,service_address__r.Province__c,
        service_address__r.Country__c,service_address__r.Maximum_Speed__c,service_address__r.MaxSpeedWithType__c,
        service_address__r.isTVAvailable__c,service_address__r.Voice_Serviceability__c,service_address__r.TVServicability__c,
        service_address__r.Connectivity_Type__c,service_address__r.isInternetAvailable__c,
        service_address__r.isMobileAvailable__c,service_address__r.isVoiceAvailable__c,service_address__r.PortsAvailable__c,
        MoveOutServiceAddress__c,MoveOutServiceAddress__r.Address__c,MoveOutServiceAddress__r.Suite_Number__c,MoveOutServiceAddress__r.Street_Address__c,
        MoveOutServiceAddress__r.AddressText__c,MoveOutServiceAddress__r.City__c,MoveOutServiceAddress__r.Province__c,
        MoveOutServiceAddress__r.Country__c,MoveOutServiceAddress__r.Maximum_Speed__c,MoveOutServiceAddress__r.MaxSpeedWithType__c,
        MoveOutServiceAddress__r.isTVAvailable__c,MoveOutServiceAddress__r.Voice_Serviceability__c,MoveOutServiceAddress__r.TVServicability__c,
        MoveOutServiceAddress__r.Connectivity_Type__c,MoveOutServiceAddress__r.isInternetAvailable__c,
        MoveOutServiceAddress__r.isMobileAvailable__c,MoveOutServiceAddress__r.isVoiceAvailable__c,MoveOutServiceAddress__r.PortsAvailable__c,
        (select orderid,Contract_Line_Item__r.vlocity_cmt__ContractId__r.status from orderitems)
       from Order where parentId__c = :customerSolutionId and (service_address__c != NULL OR MoveOutServiceAddress__c != NULL) and Status !='Cancelled' order by createdDate desc]){ 
               Boolean contractRegistered=false;
                for(OrderItem orderItemRec : ordr.orderitems){
                    if(String.isNotBlank(orderItemRec.Contract_Line_Item__r.vlocity_cmt__ContractId__r.status) &&
                          ('Contract Registered'.equalsIgnoreCase(orderItemRec.Contract_Line_Item__r.vlocity_cmt__ContractId__r.status)
                          || 'Customer Accepted'.equalsIgnoreCase(orderItemRec.Contract_Line_Item__r.vlocity_cmt__ContractId__r.status)
                        || 'Contract Accepted'.equalsIgnoreCase(orderItemRec.Contract_Line_Item__r.vlocity_cmt__ContractId__r.status)
                        || 'Signature Declined'.equalsIgnoreCase(orderItemRec.Contract_Line_Item__r.vlocity_cmt__ContractId__r.status)
                        || 'In Progress'.equalsIgnoreCase(orderItemRec.Contract_Line_Item__r.vlocity_cmt__ContractId__r.status ))){
                           contractRegistered=true; 
                       }
                }
              addressesObj.orderType = ordr.Type;
              existingOrdersAddressIds.add(ordr.service_address__c);        
              addressWrapper addr = new addressWrapper();
              addr.addressId = ordr.service_address__c;
              addr.orderType = ordr.Type;
              system.debug('@@test 0 '+ addressesObj.orderType);
              //Start: Move Out Address
              if(String.isNotBlank(ordr.Type) && ordr.Type.equalsIgnoreCase('Move') && String.isNotBlank (ordr.service_address__c)){
                  addr.moveType ='Move In';  
                  system.debug('@@test 0 '+ addr.moveType);
              }
              if(String.isNotBlank(addr.orderType) && addr.orderType.equalsIgnoreCase('Move') && null != ordr.MoveOutServiceAddress__c ){
                system.debug('@@test1 '+ordr.MoveOutServiceAddress__r.Connectivity_Type__c+' '+ String.isNotBlank(ordr.MoveOutServiceAddress__r.Connectivity_Type__c));
                
                addressWrapper moveAddr = new addressWrapper();
                moveAddr.isContractRegistered=contractRegistered;
                moveAddr.addressId = ordr.MoveOutServiceAddress__c;
                moveAddr.orderType = ordr.Type;
                moveAddr.isSelected = true;
                moveAddr.parentOrderId = ordr.parentId__c;
                moveAddr.OrderId = ordr.Id;
                moveAddr.moveType ='Move Out';

                moveAddr.serviceAddress = ordr.MoveOutServiceAddress__r.Address__c;
                if(NULL != ordr.MoveOutServiceAddress__r.Suite_Number__c && string.isNotBlank(ordr.MoveOutServiceAddress__r.Suite_Number__c)){
                    moveAddr.serviceAddress = ordr.MoveOutServiceAddress__r.Suite_Number__c +' '+ ordr.MoveOutServiceAddress__r.Address__c;                      
                }
                moveAddr.city = ordr.MoveOutServiceAddress__r.City__c;
                moveAddr.province = ordr.MoveOutServiceAddress__r.Province__c;
                moveAddr.country = ordr.MoveOutServiceAddress__r.Country__c;
                moveAddr.isVoiceAvailable = ordr.MoveOutServiceAddress__r.isVoiceAvailable__c;
                    if(moveAddr.isVoiceAvailable){addr.voiceAvailability = 'available';} 
                moveAddr.isMobileAvailable = ordr.MoveOutServiceAddress__r.isMobileAvailable__c;
                    if(moveAddr.isMobileAvailable){addr.mobileAvailability = 'available';}
                moveAddr.isTVAvailable = ordr.MoveOutServiceAddress__r.isTVAvailable__c;
                    if(moveAddr.isTVAvailable){addr.TVAvailability = 'available';}
                moveAddr.isInternetAvailable = ordr.MoveOutServiceAddress__r.isInternetAvailable__c;
                    if(moveAddr.isInternetAvailable){
                        
               if(String.isNotBlank(ordr.MoveOutServiceAddress__r.Connectivity_Type__c) ){
                    List<String> connectivityType = (ordr.MoveOutServiceAddress__r.Connectivity_Type__c).split(';');
                    moveAddr.internetProducts = new List<internetProduct>();
                    for(String tech : connectivityType){
                        internetProduct ip = new internetProduct();
                        ip.technology = tech;
                        if(String.isNotBlank(ordr.MoveOutServiceAddress__r.Maximum_Speed__c)) {
                            ip.speed = ordr.MoveOutServiceAddress__r.Maximum_Speed__c; 
                            }
                        if(String.isNotBlank(tech) &&  tech.containsIgnoreCase('copper')){
                            ip.availablePorts = ordr.MoveOutServiceAddress__r.PortsAvailable__c;
                        }                                
                        moveAddr.internetProducts.add(ip);
                    }
                    
                    moveAddr.internetAvailability = 'available';
                    
                }else{moveAddr.internetAvailability = 'unknown';}
             }
                if(String.isNotBlank(ordr.service_address__c)){
                   //moveAddr.moveInAddressWrapper = addr;
                     addr.moveOutAddressWrapper = moveAddr;                     
                }
                allServiceAddressList.add(moveAddr);
                addressesObj.addrIdToaddressWrapperMap.put(moveAddr.addressId,moveAddr);              
                system.debug(' customerSolutionId 2 '+moveAddr);              
              }

              //End: Move Out Address
              addr.isContractRegistered=contractRegistered;
              addr.isSelected = true;
              addr.parentOrderId = ordr.parentId__c;
              addr.OrderId = ordr.Id;
              addr.serviceAddress = ordr.service_address__r.Address__c;
              if(NULL != ordr.service_address__r.Suite_Number__c && string.isNotBlank(ordr.service_address__r.Suite_Number__c)){
                  addr.serviceAddress = ordr.service_address__r.Suite_Number__c +' '+ ordr.service_address__r.Address__c;                      
              }
              addr.city = ordr.service_address__r.City__c;
              addr.province = ordr.service_address__r.Province__c;
              addr.country = ordr.service_address__r.Country__c;
              addr.isVoiceAvailable = ordr.service_address__r.isVoiceAvailable__c;
                   if(addr.isVoiceAvailable){addr.voiceAvailability = 'available';} 
              addr.isMobileAvailable = ordr.service_address__r.isMobileAvailable__c;
                   if(addr.isMobileAvailable){addr.mobileAvailability = 'available';}
              addr.isTVAvailable = ordr.service_address__r.isTVAvailable__c;
                   if(addr.isTVAvailable){addr.TVAvailability = 'available';}
              addr.isInternetAvailable = ordr.service_address__r.isInternetAvailable__c;
                   if(addr.isInternetAvailable){
                       
            if(String.isNotBlank(ordr.service_address__r.Connectivity_Type__c) 
                ){
                    List<String> connectivityType = (ordr.service_address__r.Connectivity_Type__c).split(';');
                    addr.internetProducts = new List<internetProduct>();
                    for(String tech : connectivityType){
                        internetProduct ip = new internetProduct();
                        ip.technology = tech;
                        if(String.isNotBlank(ordr.service_address__r.Maximum_Speed__c)) {
                            ip.speed = ordr.service_address__r.Maximum_Speed__c; 
                            }
                        if(String.isNotBlank(tech) &&  tech.containsIgnoreCase('copper')){
                            ip.availablePorts = ordr.service_address__r.PortsAvailable__c;
                        }                                
                        addr.internetProducts.add(ip);
                    }
                    
                    addr.internetAvailability = 'available';
                    
                }else{addr.internetAvailability = 'unknown';}
            }
              if(String.isNotBlank(ordr.service_address__c)){
                allServiceAddressList.add(addr);
                addressesObj.addrIdToaddressWrapperMap.put(addr.addressId,addr);
               }
              system.debug(' customerSolutionId 2 '+customerSolutionId);
                                   
            }  
            system.debug(' customerSolutionId 4 '+customerSolutionId); 
        }
        system.debug('!!allServiceAddressList 2 '+allServiceAddressList);
        if(String.isNotBlank(RCID) && String.isNotBlank(pageType)){
            Set<String> uniqueAddressIdentifierSet = new Set<String> ();
             system.debug('!!allServiceAddressList 1'+ RCID);
            /* BMPF - 28 - Filter critera in SOQL to show all addresses linked to both the RCID as well as to those child accounts where the Parent ID = RCID Account ID
                           Show those addresses that have an FMS-ID or Location ID (do not limit this to only service account records)
                            Show all addresses outside of AB, BC (aka non-ilec)
            */ 
            for(smbcare_address__c smbaddr : [select id,Address__c, Street__c,Street_Number__c,Street_Name__c,Suite_Number__c ,Street_Address__c,AddressText__c,
                                              City__c,FMS_Address_ID__c,Location_Id__c,
                                              Province__c,Country__c from smbcare_address__c 
                                              where (Account__c = :RCID OR Account__r.ParentId = :RCID  )
                                              AND ( Location_Id__c != NULL 
                                                   OR FMS_Address_ID__c != NULL 
                                                   OR Service_Account_Id__c != NULL 
                                                   OR ( Province__c != NULL AND Province__c Not IN :ilecProvinces )
                                                  )
                                              order by LastModifiedDate desc]){
               
               string addressUniqunessKey = ('FMS'+smbaddr.FMS_Address_ID__c+'L'+ smbaddr.Location_Id__c);
               //BMPF - 28 - check duplicate locationId + fmsId
               if(NULL != uniqueAddressIdentifierSet && !uniqueAddressIdentifierSet.contains(addressUniqunessKey)){ 
                   uniqueAddressIdentifierSet.add(addressUniqunessKey);
               addressWrapper addr = new addressWrapper();
                if(null != addressesObj.addrIdToaddressWrapperMap && null != addressesObj.addrIdToaddressWrapperMap.get(smbaddr.id)){
                    addr =  addressesObj.addrIdToaddressWrapperMap.get(smbaddr.id);   
                    addr.isSelected = true;
                 }                
                addr.addressId = smbaddr.id;
                addr.serviceAddress = smbaddr.Address__c;
                if(string.isNotBlank(smbaddr.Suite_Number__c)){
                addr.serviceAddress = smbaddr.Suite_Number__c +' '+ smbaddr.Address__c;                    
                }
            //      addr.serviceAddress +='  '+  addressUniqunessKey;
            //     addr.serviceAddress = smbaddr.Suite_Number__c == NULL ?  ' ' : smbaddr.Suite_Number__c;
           //      addr.serviceAddress += ' '+ smbaddr.Street_Number__c == NULL ?  ' ' : smbaddr.Street_Number__c;
          //      addr.serviceAddress += ' '+ smbaddr.Street_Name__c == NULL ?  ' ' : smbaddr.Street_Name__c;                                
                addr.city = smbaddr.City__c;
                addr.province = smbaddr.Province__c;
                addr.country = smbaddr.Country__c;
                allServiceAddressList.add(addr);
                system.debug('!!allServiceAddressList 2'+ addr);
                addressesObj.addrIdToaddressWrapperMap.put(addr.addressId,addr);
            }
           }                                        
            if(null != addressesObj.addrIdToaddressWrapperMap && null!= addressesObj.addrIdToaddressWrapperMap.values()){
                addressesObj.totatAddress = (addressesObj.addrIdToaddressWrapperMap.values()).size();
            }            
        }
        system.debug('!!allServiceAddressList 3 '+ RCID+' '+addressesObj);
        return addressesObj;
    }
   
    @RemoteAction
    global static Object checkGTA(List<addressWrapper> addressWrapperListPerBatch,List<addressWrapper> allAddresses){
        
        Integer TIMEOUT_INT_SECS = 60;  
        Continuation cont = new Continuation(TIMEOUT_INT_SECS);
        cont.continuationMethod = 'processcheckGTAResponse';
        GTARequestState GTACallRequestState = new GTARequestState();
        GTACallRequestState.GTAResList = new List<GTAResponse>();     
       system.debug('!!## addressWrapperListPerBatch '+ addressWrapperListPerBatch);
       
        for(addressWrapper addrWrapper : addressWrapperListPerBatch){
            try{ 
                
                OrdrWsCheckGenericTechnicalAvailability.TpFulfillmentResourceOrderV3Future GTACallFuture;
                
                OrdrWsCheckGenericTechnicalAvailability.AsynCheckGenericTechnicalAvailabilitySOAPPort servicePort = new OrdrWsCheckGenericTechnicalAvailability.AsynCheckGenericTechnicalAvailabilitySOAPPort();
                servicePort = OrdrTechnicalAvailabilityWsCallout.asynPrepareCallout(servicePort);
                
                GTAResponse GTACallObj = new GTAResponse();
                GTACallObj.addressId = addrWrapper.addressId;                
                Map<String, String> inputMap = new Map<String, String>();
                try{
                    inputMap = OrdrUtilities.constructServiceAddress(GTACallObj.addressId);
                    GTACallFuture = servicePort.beginCheckGenericTechnicalAvailability(cont, OrdrTechnicalAvailabilityWsCallout.prepareTechnicalAvailabilityPayload(inputMap));    
                
                }catch(exception ex){
                    if(ex.getMessage().contains('Missing required fields')){
                       String exceptionMsg ='Missing required fields';
                       GTACallRequestState.exceptionMsg += exceptionMsg;
                       addrWrapper.isError = true;
                       addrWrapper.errorMessage = exceptionMsg;
                        
                    }
                    system.debug('!!!Exception Search 0 ' +ex.getMessage());
                 // OrdrExceptions.InvalidParameterException: Missing required fields: ["locationId"]. Unable to execute Product Availability/Eligibility operation.
                }
                GTACallObj.GTACallRes = GTACallFuture;
                GTACallRequestState.GTAResList.add(GTACallObj);
                GTACallRequestState.addrIdToaddressWrapperMap.put(addrWrapper.addressId, addrWrapper);
                //   system.debug('wfmSearchRequestState  '+wfmSearchRequestState);
                
            }catch(exception e){
                system.debug('!!!Exception Search' +e);
            }            
        }
        cont.state = GTACallRequestState;
        return cont;
    } 
    
    global static Object processcheckGTAResponse(object state) {
        
        GTARequestState GTAResponseResult = (GTARequestState)state;
        system.debug('!@# State '+ GTAResponseResult);
        
        List<OrdrWsCheckGenericTechnicalAvailability.TpFulfillmentResourceOrderV3Future> CalloutReslist = GTAResponseResult.GTAReqList;
        
        List<addressWrapper> addList = new List<addressWrapper>();
        if(null != GTAResponseResult && null != GTAResponseResult.GTAResList){
            
            for(GTAResponse GTARes : GTAResponseResult.GTAResList){ 
                addressWrapper add= new addressWrapper();
                if(null != GTAResponseResult.addrIdToaddressWrapperMap && null != GTAResponseResult.addrIdToaddressWrapperMap.get(GTARes.addressId)){
                    add = GTAResponseResult.addrIdToaddressWrapperMap.get(GTARes.addressId);
                }
                if(!add.isError){
                    TpFulfillmentResourceOrderV3.ResourceOrder response;
                    try{
                        response = GTARes.GTACallRes.getValue();
                        system.debug('!@# response '+response);
                    }catch(exception ex){
                        system.debug('!@# response 3 '+add.addressId + ' '+ add.errorMessage);
                        add.isError = true;
                        //ex.getMessage();
                        OrdrUtilities.errList=new List<webservice_integration_error_log__c>();
                        OrdrUtilities.auditLogs('checkTechnicalAvailability_controller.processcheckGTAResponse', 
                                                'NetCracker', 
                                                'CheckGenericTechnicalAvailability', 
                                                 null, 
                                                 null, 
                                                 true,                                                                                
                                                 ex.getMessage(), 
                                                 ex.getStackTraceString());
                        OrdrExceptions.TechnicalAvailabilityServiceException expt = new OrdrExceptions.TechnicalAvailabilityServiceException();
                        add.errorMessage =  expt.getMessage();
                        system.debug('!@# response 2 '+add.errorMessage);
                    }                    
                    if(null != response){
                        //....
                        List<OrdrTechnicalAvailabilityManager.ProductCategory> productCategories = OrdrTechnicalAvailabilityManager.mapTechnicalAvailabilityResponse(response);
                        system.debug('@@## response ' +productCategories );
                             add.internetProducts = new List<internetProduct>();   
                        for (OrdrTechnicalAvailabilityManager.ProductCategory product : productCategories) {                    
                            system.debug('!@# product '+product);
                            String productType;
                            String productAvailable;
                             
                            //Check what types of product needs to look.
                            if (OrdrConstants.GTACategoryMap.containsKey(product.category)) {
                                
                                productType = OrdrConstants.GTACategoryMap.get(product.category);
                                productAvailable = product.availability;                    
                                system.debug('!!@@ productType '+ productType);
                                
                                if(String.isNotBlank(productType) && String.isNotBlank(productAvailable)){    
                                    system.debug(' !! productType '+ productType);
                                    if(productAvailable =='available' || productAvailable =='unknown' ){                                        
                                        
                                        if(productType == 'INTERNET'){ 
                                            add.isInternetAvailable = true;
                                            if(productAvailable =='unknown'){
                                                // add.internetTechnology ='unknown';
                                                add.internetAvailability = productAvailable;
                                            }else if(productAvailable =='available'){
                                                internetProduct ip = new internetProduct();
                                                ip = processInternetProduct(product,productType,ip);
                                                add.internetProducts.add(ip);  
                                            }                             
                                        }
                                        if(productType == 'TV' ){ add.isTVAvailable = true; add.TVAvailability = productAvailable;}
                                        if(productType == 'MOBILE'){ add.isMobileAvailable = true; add.mobileAvailability = productAvailable;}
                                        if(productType == 'VOICE'){ add.isVoiceAvailable = true; add.voiceAvailability = productAvailable;}
                                        if(productType == 'WiFi'){ add.isWIFIAvailable = true; add.WIFIAvailability = productAvailable;}
                                        system.debug(' !! add '+ add);
                                    }
                                }                                                                       
                            }
                        }                    
                        //....
                        if(null!= GTARes.addressId){
                            OrdrTechnicalAvailabilityManager.saveConnectionType(GTARes.addressId, productCategories);
                            //Asynchronous call
                            OrdrProductEligibilityManager.cacheAvailableOffers(GTARes.addressId);
                        }
                    }    
                }
                add.addressId = GTARes.addressId;
                add.loading = false;
                addList.add(add);
                    
            } 
        }
        system.debug('@@## response ' +addList );
        return addList;
    } 
    
    global static internetProduct processInternetProduct(OrdrTechnicalAvailabilityManager.ProductCategory product, String productType, internetProduct ip){
        system.debug(' processInternetProduct ' +product +' '+ productType);
        if (productType.equals('INTERNET')) {
            if(String.isNotBlank(product.accessType)){
            if (product.accessType.equals('Fibre - FIFA')) {
                ip.technology  = 'PureFibre (FIFA)';
                ip.speed = '1G+ Mbps';
                    } else if (product.accessType.equals('Fibre - Non-FIFA')) {
                        ip.technology  = 'PureFibre';
                        ip.speed = '150+ Mbps';
                    } else if (product.accessType.equals('Bonded Copper')) {
                        ip.technology  = product.accessType;
                        ip.speed = product.downloadSpeed;
                            if (String.isNotBlank(product.ports)) {
                                ip.availablePorts = product.ports;
                            }
                    } else if (product.accessType.equals('Copper')) {
                        ip.technology  = product.accessType;
                        ip.speed = product.downloadSpeed;
                        if (String.isNotBlank(product.ports)) {
                            ip.availablePorts = product.ports;
                        }
                    } 
            }else{
                ip.technology = 'PureFibre';
                ip.speed = 'unconfirmed';
                ip.technology = 'Copper';
                ip.speed = 'unconfirmed';
            }
        }
        return ip; 
    }
   
    global Class RCIDAddressesWrapper{
        Integer totatAddress = 0; 
        Integer noOfGTACallsallowed = 100; 
        global Map<string,addressWrapper> addrIdToaddressWrapperMap = new Map<string,addressWrapper>();
        global String errorMessage;
        String orderType ='New Services';
        
    }    
    
    global Class addressWrapper{
        //Start- Added for BMPF-44
        global Boolean isContractRegistered{get;set;}
        global String orderType {get;set;}
        global List<String> moveTypeList = new List<String> {'Move Out','Move In'};
        global String moveType;
        global addressWrapper moveInAddressWrapper;
        global addressWrapper moveOutAddressWrapper;
        //End- Added for BMPF-44
        global String parentOrderId {get;set;}
        global String OrderId {get;set;}
        global String addressId {get;set;}
        global String serviceAddress {get;set;}
        global String city {get;set;}
        global String province {get;set;}
        global String country {get;set;}
        global boolean isInternetAvailable = false;
        global String  internetSpeed = null ;
        global String  internetTechnology = null ;
        global List<internetProduct> internetProducts;
        global String  internetAvailability = null; 
        global boolean isTVAvailable = false;
        global String  TVAvailability = null;
        global boolean isMobileAvailable = false;
        global String  mobileAvailability = null;
        global boolean isVoiceAvailable = false;
        global String  voiceAvailability = null;
        global boolean isWIFIAvailable = false;
        global String  WIFIAvailability = null;
        global boolean isError = false;
        global String  errorMessage;
        global boolean loading = false;
        global boolean isSelected = false;
    }  
    
    global class internetProduct{
       global String technology;
       global String speed;       
       global String availablePorts;
     //  global String availability; 
    }
    
    global Class GTARequestState{
        global String exceptionMsg;
        global List<GTAResponse> GTAResList;    
        global List<OrdrWsCheckGenericTechnicalAvailability.TpFulfillmentResourceOrderV3Future> GTAReqList;    
        global Map<string,addressWrapper> addrIdToaddressWrapperMap = new Map<string,addressWrapper>();
    }
    
    global Class GTAResponse{
        global String messageBody;
        global String addressId;        
        global OrdrWsCheckGenericTechnicalAvailability.TpFulfillmentResourceOrderV3Future GTACallRes;       
    }
    
}