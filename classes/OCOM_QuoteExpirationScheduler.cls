global class OCOM_QuoteExpirationScheduler implements Schedulable
{
    global void execute(SchedulableContext SC)
    {
        try
        {
            Database.executeBatch(new OCOM_QuoteExpirationWorker());
        }
        catch(Exception e)
        {
            
        }
    }    
}