public class SMB_SendEmailcontroller
{
    private final Opportunity OpptyOBJ ;
    public SMB_SendEmailcontroller(ApexPages.StandardController stdController) 
    {
        this.OpptyOBJ = (opportunity)stdController.getRecord();
    }
    public PageReference autoRun() 
    {
        String OpptyOBJ = ApexPages.currentPage().getParameters().get('p3_lkid');
        if (OpptyOBJ == null) {
            return null;
        }
        // Redirect the user back to the original page
        PageReference pageRef = new PageReference('/_ui/core/email/author/EmailAuthor?p3_lkid=' + OpptyOBJ+'&retURL='+OpptyOBJ);
        pageRef.setRedirect(true);
        return pageRef;
    }
}