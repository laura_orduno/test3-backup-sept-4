/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PPTAPI {
    global PPTAPI() {

    }
    @RemoteAction
    global static DMAPP.PPTRestResourceDelegate.DeleteResult delPPTX(String id) {
        return null;
    }
    @RemoteAction
    global static DMAPP.PPTRestResourceDelegate.GetResult getPPTX(String id) {
        return null;
    }
    @RemoteAction
    global static DMAPP.PPTRestResourceDelegate.PostResult postPPTX(String id, DMAPP.PPTRestResourceDelegate.PostParam param) {
        return null;
    }
    @RemoteAction
    global static DMAPP.PPTRestResourceDelegate.PutResult putPPTX(DMAPP.PPTRestResourceDelegate.PutParam param) {
        return null;
    }
}
