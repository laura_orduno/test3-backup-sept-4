/*****************************************************
    Name: ExternalEventManager_Helper
    Usage: This class used define common methods used in event manager and batch processor
    Author – Sandip Chaudhari (IBM)
    Revision History -
    Created By/Date - Sandip Chaudhari / 08 July 2016
    
    Updated By/Date/Reason for Change - 
    
******************************************************/
global class ExternalEventManager_Helper{
	// Create the instance for perticuler event using class name stored in custom setting
    global static EventHandlerInterface createHandlerInstance(String eventName){
    	EventHandlerInterface classObject;
    	if(EventName != null){
            External_Event_Config__c handlerMapping = External_Event_Config__c.getInstance(eventName);
            if(handlerMapping != null && handlerMapping.Event_Handler_Name__c != null){
                Type classObj = Type.forName(handlerMapping.Event_Handler_Name__c);
                if(classObj == null) {
                    // Attempt to get the type again with the namespace explicitly set to blank
                    classObj = Type.forName('', handlerMapping.Event_Handler_Name__c);
                }
                if(classObj == null) {
                    System.debug(LoggingLevel.Error, 'Failed to find type for ['+handlerMapping.Event_Handler_Name__c+']');
                    return null;
                }
                // Create an instance to confirm the type
                classObject = (EventHandlerInterface)classObj.newInstance();
            }
      	}
      	return classObject;
    }

}