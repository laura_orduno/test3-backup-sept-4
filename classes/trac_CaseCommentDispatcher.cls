/**
*trac_CaseCommentDispatcher
*@description Upon creating or update of a case comment this comment will either create a Vendor Communication object( if the Case Comment is private)
*             or will simply send the case comment straight to the Ring Central Org if it is public.
*@authoer David Barrera
*@Date 8/28/14
*/

public with sharing class trac_CaseCommentDispatcher extends Dispatcher{

	public List<CaseComment> newList {get; set;}
    public List<CaseComment> oldList {get; set;}
    public Map<Id, CaseComment> newMap {get; set;}
    public Map<Id, CaseComment> oldMap {get; set;}
    public static boolean firstRun = true;

	public trac_CaseCommentDispatcher() {
		super();
	}
	public override void init() {
        newList = (trigger.new != null) ? (List<CaseComment>)trigger.new : new List<CaseComment>();
        oldList = (trigger.old != null) ? (List<CaseComment>)trigger.old : new List<CaseComment>();
        newMap = (trigger.newMap != null) ? (Map<Id, CaseComment>)trigger.newMap : new Map<Id, CaseComment>();
        oldMap = (trigger.oldMap != null) ? (Map<Id, CaseComment>)trigger.oldMap : new Map<Id, CaseComment>();
    }
    public override void execute(){
        
        //===========================================================================================================================================================
        // The value of 'processCountMap' is being set to 1,using the method 'countExecuted' from the method 'createCaseComment()' that resides in the class 
        // 'trac_VendorCommunicationDispatcher'. The reason is to perform an insert on CaseComment without firing the logic from an AFTERINSERT event.
        // After the insert is completed, the  'processCountMap' is reset to zero.
        //===========================================================================================================================================================
        
        if( LimitExecutionEvent(TriggerEvent.AFTERINSERT)){
            if(LimitExecutionCount('createVendorComm', 1) ) {
                createVendorComm();
            }
    	}	
        if( LimitExecutionEvent(TriggerEvent.AFTERUPDATE)){
            System.Debug('Vendor AfterUpdate');
            if(LimitExecutionCount('updateVenComm', 1) ){
                //System.Debug('The exuction is now in \'AFTERUPDATE\'');
                updateVenComm();
            }
        }
        
    }

    public void createVendorComm(){
        createVendorComm(newList);
    }

    public void updateVenComm(){
        //System.debug('public void updateVenComm() START');
        updateVenComm(newList);
        //System.debug('public void updateVenComm() END');
    }
    /**    
    *createVendorComm
    *@description Function that creates a Vendor Communication refrence when needed.
    *@authoer David Barrera
    *@Date 9/3/14
    */
    public static void createVendorComm( List<CaseComment> newCC){
    	// check to see if the current case comment is private or has a case comment over in the RC org.
    	//If it does then create a new list of vendor communication. If it isn't send a list of case comments to next org.
    	Set<id> caseIds = new Set<Id>();

        List<Case> relatedCases = new List<Case>();
        Map<CaseComment,Id> ccToCase = new Map<CaseComment,Id>();
        List<CaseComment> casesCommentsToVendor = new List<CaseComment>();  
    	List<PartnerNetworkRecordConnection> pnrcList = new List<PartnerNetworkRecordConnection>();
        List<Vendor_Communication__c> newVendor = new List<Vendor_Communication__c>();
        List<Vendor_Communication__c> newVendorOnCaseFirstShare = new List<Vendor_Communication__c>();

        Map<Id, Id> caseIdToPNRC = new Map<Id, Id>();        
        Map<Id, CaseComment> caseToCaseComment = new Map<Id, CaseComment>();
    	//get the PNRC of the case to get the vendor case id to create a new vendor comm object with that Id.

    	for(CaseComment cc : newCC ){  
            //only if the case comment is not received from a RingCentral.            
            if(cc.ConnectionReceivedId == null){          
    		  caseIds.add(cc.ParentId);
              caseToCaseComment.put(cc.ParentId, cc);                           
            }
    	}

        if(caseIds.size() > 0){
            relatedCases = [
                SELECT Id, Vendor_Case_ID__c
                FROM Case
                WHERE Id in:caseIds
            ];

            //System.Debug('relatedCases.size(): ' + relatedCases.size());

            pnrcList =[ 
                Select id,status,LocalRecordId,StartDate,PartnerRecordId
                From PartnerNetworkRecordConnection
                Where LocalRecordId in :caseIds
                And Status <> 'Inactive'
            ];
            //System.Debug('pnrcList.size(): ' + pnrcList.size());
        }

        for (PartnerNetworkRecordConnection pnrc: pnrcList){            
            caseIdToPNRC.put(pnrc.LocalRecordId, pnrc.PartnerRecordId); 
        }

        for(Case aCase:relatedCases){
            //if(caseIdToPNRC.containsKey(aCase.Id)){
            casesCommentsToVendor.add(caseToCaseComment.get(aCase.Id));
            //}
            if(caseToCaseComment.containsKey(aCase.Id)){
                ccToCase.put(caseToCaseComment.get(aCase.Id),aCase.Id );
            }
        }
        //System.Debug('casesCommentsToVendor.size(): ' + casesCommentsToVendor.size());

        //get all cases comments that have the case_comment id in the list of case comment ids;
    	for(CaseComment cc:casesCommentsToVendor ){
            //!cc.isPublished &&
    		if( cc.ConnectionReceivedId == null){
    			if(caseIdToPNRC.containsKey(cc.ParentId)){
                    System.Debug(cc.Id);
    				newVendor.add( new Vendor_Communication__c(Case_Comment_ID__c =cc.Id,
    														   Case_ID__c = cc.ParentId,
    														   Vendor_Case_ID__c = caseIdToPNRC.get(cc.ParentId), 
    														   Vendor_Comments__c=cc.CommentBody,
                                                               isPublished__c = cc.IsPublished
    														   ));    		
    			}
                else{
                    newVendorOnCaseFirstShare.add(new Vendor_Communication__c(Case_Comment_ID__c =cc.Id,                                                               
                                                               Case__c = ccToCase.get(cc),                                                             
                                                               Vendor_Comments__c=cc.CommentBody,
                                                               isPublished__c = cc.IsPublished
                                                               ));
                }
    		}           
    	}
        //System.Debug('new vendor size:' + newVendor.size() );
        //System.Debug(newVendor);
    	if(newVendor.size() > 0 ){
    		//DataBase.upsertResult[] urList = database.upsert( newVendor, Vendor_Communication__c.Case_Comment_ID__c.getDescribe().getsObjectField(), false );
            //System.Debug('upserting new vendor.');
            upsert newVendor Case_Comment_ID__c;
            //System.Debug('This is the save result list.' + urList);
    	}

        //============================================================================================================================================================
            //The value of 'processCountMap' is being set to 1 using the function'countExecuted'. This affects the 'execute()' and 'manageVendorComm' methods in the class
            // 'trac_VendorCommunicationDispatcher'. The reason is to perform an insert on Vendor Communication without firing the logic from an AFTERINSERT event.
            //After the insert is completed, the  'processCountMap' is reset to zero.
        //============================================================================================================================================================
            
        if(newVendorOnCaseFirstShare.size() > 0 ){
            Dispatcher.CountExecuted('manageVendorComm');
            Dispatcher.CountExecuted('updateCaseComment');
            upsert newVendorOnCaseFirstShare Case_Comment_ID__c;
            Dispatcher.ResetProcessCount('updateCaseComment');
            Dispatcher.ResetProcessCount('manageVendorComm');
        }

    	
    } 

    /**    
    *updateVenComm
    *@description Function that updates a Vendor Communication refrence when a case comment is created on a case.
    *@authoer David Barrera
    *@Date 9/3/14
    */
    public static void updateVenComm(List<CaseComment> updateCC){
        //System.Debug('The execution is now in \'updateVenComm\'');

        //System.debug('public static void updateVenComm(List<CaseComment> updateCC) START');

        List<String> ccToVendor = new List<String>();
        List<Vendor_Communication__c> updateVen = new List<Vendor_Communication__c>();
        List<Vendor_Communication__c> createdVendor = new List<Vendor_Communication__c>();
        Map<String, Vendor_Communication__c> ccIdToVen = new Map<String,Vendor_Communication__c>();

        for(CaseComment cc:updateCC ){  
            //only if the case comment is not received from a RingCentral.
            if(cc.Id != null){                                
                ccToVendor.add(cc.Id);           
            }
        }

        if(ccToVendor.size() > 0){
            //System.debug('if(ccToVendor.size() > 0)');
             createdVendor = [
                SELECT Case_Comment_ID__c,Case_ID__c, Vendor_Case_ID__c,Vendor_Comments__c
                FROM Vendor_Communication__c
                WHERE Case_Comment_ID__c IN :ccToVendor
                And Case_Comment_ID__c <> null
            ];
        }

        //System.debug('AFTER if(ccToVendor.size() > 0)');

        for(Vendor_Communication__c ven:createdVendor){
            ccIdToVen.put(ven.Case_Comment_ID__c, ven);
        }

        for(CaseComment cc:updateCC){
            if(ccIdToVen.containsKey(cc.Id)){
                Vendor_Communication__c tmpVen = ccIdToVen.get(cc.Id);
                tmpVen.Vendor_Comments__c = cc.CommentBody;
                updateVen.add(tmpVen);
            }
        }

        if(updateVen.size() > 0){            
            update updateVen;// Case_Comment_ID__c;
        }
        //System.debug('public static void updateVenComm(List<CaseComment> updateCC) END');
    }
}