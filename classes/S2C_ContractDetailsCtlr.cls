/**
 * S2C_ContractDetailsCtlr
 * @description Retrieves information related to the order opporunity and
 *              displays a summary on the page.
 *
 * @author Thomas Tran, Merisha Shim, Traction on Demand
 * @date 02-27-2015
 * @lastmodifiedby Merisha Shim
 * @datelastmodified 03-04-2015
 */
public with sharing class S2C_ContractDetailsCtlr {
	private static final String ORDER_OPPORTUNITY_ID = 'orderOppId';
	private static final String SALES_OPPPORTUNITY_ID = 'salesOppId';
	private static final String CONTRACT_ID = 'contractId';

	public Id orderOppId {get; set;}
	public Id salesOppId {get; set;}
	public Id contractId { get; set; }

	//these fiels are for the Order Opportunity Details on the top of the page
	public String orderOppIdField { get; set; }
	public String orderOppAccountRCID { get; set; }
	public String orderOppAccountCorrectLegalName { get; set; }
	public String orderOppPrimaryOrderContact { get; set; }
	public String orderOppPrimaryOrderContactEmail { get; set; }
	public String orderOppAccountOwnerName { get; set; }
	public String orderOppCreditAssessment { get; set; }
	public String orderOppBidReference { get; set; }
	public String orderOppAccountDeliquency { get; set; }
	public String orderOppAccountLegalNameChecked { get; set; }
	public String orderOppCreditPortalTier { get; set; }

	public String contractType { get; set; }
	public String contractSignor { get; set; }
	public String contractSignorPhone { get; set; }
	public String contractSignorEmail { get; set; }
	public String contractTitle { get; set; }
	public String contractRemarks { get; set; }
	public String contractECBContractId { get; set; }
	public String contractSystem { get; set; }
	public String contractNumber { get; set; }
	public Boolean contractSigned { get; set; }

	public S2C_ContractDetailsCtlr() {
		this.orderOppId = ApexPages.currentPage().getParameters().get(ORDER_OPPORTUNITY_ID);
		this.contractId = ApexPages.currentPage().getParameters().get(CONTRACT_ID);

		List<Service_Request__c> serviceRequestList = [
			SELECT Opportunity_Product_Item__r.Opportunity__c
			FROM Service_Request__c
			WHERE Opportunity__c = :orderOppId
		];
		
		if(!serviceRequestList.isEmpty()){
			this.salesOppId = serviceRequestList[0].Opportunity_Product_Item__r.Opportunity__c;
		} else{
			this.salesOppId = null;
		}

		setOpportunityOrderInformation();
		displayContractInfo();
	}

	/**
	 * setOpportunityOrderInformation
	 * @description Set the Order Opportunity fields
	 *
	 * @author Merisha Shim, Traction on Demand
	 * @date 03-02-2015
	 */
	 private void setOpportunityOrderInformation() {
	 	Opportunity o = [SELECT Order_ID__c, RCID__c, Account.Correct_Legal_Name__c, Primary_Order_Contact__r.Name, Primary_Order_Contact__r.Email,
	 							Credit_Assesment__c, Account.Owner.Name, Account.Delinquency_Flag__c, Account.Legal_Name_Checked__c, 
                         		Account.Credit_Portal_Tier__c, Account.CreditRef_Num__c,Credit_Assessment__c , Credit_Assessment__r.Name
							FROM Opportunity
							WHERE Id = :orderOppId];
		
		if(salesOppId != null) {
			//get the Sales Opportunity's Bid Reference
			orderOppBidReference = [SELECT Bid_Reference__c FROM Opportunity WHERE Id =: salesOppId].Bid_Reference__c;
		}
		

		orderOppIdField = o.Order_ID__c;
		orderOppAccountRCID = o.RCID__c;
		orderOppAccountCorrectLegalName = o.Account.Correct_Legal_Name__c;
		orderOppPrimaryOrderContact = o.Primary_Order_Contact__r.Name;
		orderOppPrimaryOrderContactEmail = o.Primary_Order_Contact__r.Email;
		orderOppAccountOwnerName = o.Account.Owner.Name;		
		orderOppAccountDeliquency = o.Account.Delinquency_Flag__c;
		orderOppAccountLegalNameChecked = o.Account.Legal_Name_Checked__c;
		orderOppCreditPortalTier = o.Account.Credit_Portal_Tier__c;
         
         if(o.Credit_Assessment__c != null){
				orderOppCreditAssessment = o.Credit_Assessment__r.Name;
			} else{
				if(String.isNotBlank(o.Credit_Assesment__c)){
					orderOppCreditAssessment = o.Credit_Assesment__c;
				} else{
					orderOppCreditAssessment = o.Account.CreditRef_Num__c;
				}
			}
	 }


	/**
	 * getServiceRequestInfo
	 * @description Returns a list of Service_Request__c.
	 *
	 * @author Thomas Tran, Traction on Demand
	 * @date 03-02-2015
	 */
	public List<Service_Request__c> getServiceRequestInfo(){
		return [
			SELECT Name, BTN__c, SRS_PODS_Product__r.Name, 	Contract_Length_Months__c, Contract_Length__c, Opportunity_Product_Item__r.Existing_Contract_Nbr__c, 
				Service_Identifier_Type__c, Existing_Service_Id__c, Target_Date__c, Billing_Account_New__c, Billing_Account__r.BillingStreet, Billing_Account__r.BillingCity, Billing_Account__r.BillingState, Billing_Account__r.BillingPostalCode, Billing_Account__r.BillingCountry, Billing_Address__c,Target_Date_Type__c,
			(SELECT Full_Address__c, City__c, Province__c, Location__c, NPA_NXX__c, RecordType.Name FROM Service_Address__r),
			(SELECT Charge_Type__c, Amount__c, Location__c, Approved_By_TELUS__r.Name, Approved_Date__c, Pre_approved__c, Charge_Method__c, EBAM__c, Charge_Remarks__c, Status__c 
				FROM Billable_Charges__r
				WHERE Charge_Type__c = 'Monthly Recurring' 
				OR Charge_Type__c = 'Activation/Non-Recurring'
				OR Charge_Type__c = 'Access Construction')
			FROM Service_Request__c
			WHERE Contract__c = :contractId
		];
	}

	/**
	 * getOpportunityProductItemInfo
	 * @description Returns a list of Opp_Product_Item__c.
	 *
	 * @author Thomas Tran, Traction on Demand
	 * @date 03-02-2015
	 */
	public List<Opp_Product_Item__c> getOpportunityProductItemInfo(){
		return [
			SELECT Name, Contract_Length_Month__c
			FROM Opp_Product_Item__c
			WHERE Opportunity__c = :salesOppId
		];
	}

	/**
	 * getContactInfo
	 * @description Returns the contract.
	 *
	 * @author Thomas Tran, Traction on Demand
	 * @date 03-02-2015
	 */
	public void displayContractInfo(){
		Contracts__c con = [SELECT Type_Of_Contract__c, Contract_Number__c, Customer_Signor__r.Name, Customer_Signor__r.Email, 
                            Customer_Signor__r.Phone, Customer_Signor__r.Title, Contract_Remarks__c, Contract_Signed__c, Contract_System__c, 
                            ECB_Contract_ID__c,Existing_ECB_Contract_ID__c,Existing_Contract_Number__c
							FROM Contracts__c
							WHERE Id = :contractId];
		contractType = con.Type_Of_Contract__c;
		contractSignor = con.Customer_Signor__r.Name;
		contractSignorPhone = con.Customer_Signor__r.Phone;
		contractSignorEmail = con.Customer_Signor__r.Email;
		contractTitle = con.Customer_Signor__r.Title;
		contractRemarks = con.Contract_Remarks__c;
		contractECBContractId = con.Existing_ECB_Contract_ID__c;
		contractSystem = con.Contract_System__c;
		contractNumber = con.Existing_Contract_Number__c;
		contractSigned = con.Contract_Signed__c;
	}

	/**
	 * getWorkRequest
	 * @description Returns a list of Contracts__c.
	 *
	 * @author Thomas Tran, Traction on Demand
	 * @date 03-02-2015
	 */
	public List<Work_Request__c> getWorkRequestInfo(){
		return [
			SELECT Name, Type__c, Status__c, Contract_Status_Type__c, Description__c, Owner.Name, LBCO_Calculator__c,Pending_Reason__c 
			FROM Work_Request__c
			WHERE Contract__c = :contractId
		];
	}
}