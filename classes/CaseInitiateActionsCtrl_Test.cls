@isTest
private class CaseInitiateActionsCtrl_Test { 
    
    @isTest static void test1() {
        Id rtId =Schema.SObjectType.Case.getRecordTypeInfosByName().get('SMB Wireless Repair in Progress').getRecordTypeId();
        
        Case cs = new Case();
        cs.RecordTypeId = rtId;
        cs.Repair_Type__c = 'Device';
        cs.ESN_MEID_IMEI__c = '900931421150053';
        cs.Mobile_number__c = '1234567890';
        insert cs;
        ApexPages.currentPage().getParameters().put('Id', cs.Id);
        Test.StartTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(cs);
        CaseInitiateActionsCtrl cls = new CaseInitiateActionsCtrl(sc);
        Test.StopTest();
    }
    
    @isTest static void test2() {
        Id rtId =Schema.SObjectType.Case.getRecordTypeInfosByName().get('SMB Wireless Repair').getRecordTypeId();
        
        Case cs = new Case();
        cs.RecordTypeId = rtId;
        cs.Repair_Type__c = 'Device';
        cs.ESN_MEID_IMEI__c = '900931421150053';
        cs.Mobile_number__c = '1234567890';
        insert cs;
        ApexPages.currentPage().getParameters().put('Id', cs.Id);
        Test.StartTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(cs);
        CaseInitiateActionsCtrl cls = new CaseInitiateActionsCtrl(sc);
        Test.StopTest();
    }    
    
    @isTest static void test3() {
        Id rtId =Schema.SObjectType.Case.getRecordTypeInfosByName().get('SMB Wireless Repair Post-Validation').getRecordTypeId();
        
        Case cs = new Case();
        cs.RecordTypeId = rtId;
        cs.Repair_Type__c = 'Device';
        cs.ESN_MEID_IMEI__c = '900931421150053';
        cs.Mobile_number__c = '1234567890';
        insert cs;
        ApexPages.currentPage().getParameters().put('Id', cs.Id);
        Test.StartTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(cs);
        CaseInitiateActionsCtrl cls = new CaseInitiateActionsCtrl(sc);
        Test.StopTest();
    }     
}