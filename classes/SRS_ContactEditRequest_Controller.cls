public class SRS_ContactEditRequest_Controller{

    public SRS_ContactEditRequest_Controller(ApexPages.StandardController controller) {

    }

    Id accountId;
    Id sreviceReqId;
    public String lookupContact {get;set;}
    List<Contact> addContacts;
    public SRS_ContactEditRequest_Controller(){
        accountId = ApexPages.currentPage().getParameters().get('AcctId');   
        sreviceReqId = ApexPages.currentPage().getParameters().get('serviceRequestId');
        if(sreviceReqId!=Null && accountId!=Null){
            addContacts = [Select Id, AccountId from contact Where Id =: accountId];
        }
    }

}