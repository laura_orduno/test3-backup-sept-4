/**
 * Test class for S2C_OrderProvisioningDetails
 *
 * @author Thomas Tran, Merisha Shim, Traction on Demand
 * @date 03-15-2015
 */
@isTest
private class S2C_OrderProvisioningDetails_Test {

	public static final Id PREQUEL_RT = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Prequal').getRecordTypeId();
	public static final Id ORDER_PROV_RT = Schema.SObjectType.Work_Request__c.getRecordTypeInfosByName().get('Complex Order Provisioning').getRecordTypeId();

	@isTest static void displayOrderProvisioningPage_Test() {
		System.runAs(new User(Id = UserInfo.getUserId())){
			Account newAccount = S2C_TestUtility.createAccount('Test Account');
			insert newAccount;

			Opportunity newOpportunity = S2C_TestUtility.createOpportunity(newAccount.Id, 'Test Opportunity');
			newOpportunity.CloseDate = Date.today();
			newOpportunity.StageName = S2C_TestUtility.ORIGINATED_STAGE_NAME;
			insert newOpportunity;

			Product2 newProduct2 = S2C_TestUtility.createProduct('Test Product', 'Test');
			insert newProduct2;

			Opp_Product_Item__c newOppProductItem = S2C_TestUtility.createOpportunityProductItem(newProduct2.Id, newOpportunity.Id);
			insert newOppProductItem;

			SRS_PODS_Product__c newSRSPodsProduct = S2C_TestUtility.createSRSPODSProduct(newProduct2.Product_Family__c);
			insert newSRSPodsProduct;

			//insert service request
			Service_Request__c sr = new Service_Request__c(RecordTypeId = PREQUEL_RT);
			insert sr;

			//insert order provisioning work request
			Work_Request__c wr = new Work_Request__c(RecordTypeId = ORDER_PROV_RT, Type__c = 'Order Provisioning', Transfer_to_Queue__c = 'Medium Voice AB');
			insert wr;

			SRS_PODS_Template__c newTemplate = new SRS_PODS_Template__c(
				QUESTION_EN__c = 'Is this a test?',
				SRS_PODS_Product_Master__c = newSRSPodsProduct.Id,
				ORDER_TYPE__c = 'Install'
			);

			insert newTemplate;

			SRS_PODS_Answer__c newAnswer = new SRS_PODS_Answer__c(
				SRS_PODS_Answer__c = 'Test',
				Service_Request__c = sr.Id,
				SRS_PODS_Products__c = newTemplate.Id
			);

			insert newAnswer;

			List<SRS_PODS_Answer__c> srspodsAnswers = [SELECT SRS_PODS_Products__r.QUESTION_EN__c, SRS_PODS_Answer__c
																	FROM SRS_PODS_Answer__c
																	WHERE Service_Request__c =: sr.Id AND SRS_PODS_Products__c != null];
			System.debug('srspodsAnswers ' + srspodsAnswers);

			System.debug('newTemplate ' + newTemplate);
			System.debug('newAnswer ' + newAnswer);

			Test.startTest();


			PageReference testPageRef = Page.S2C_OrderProvisioningDetails;
			testPageRef.getParameters().put('orderOppId', newOpportunity.Id);
			testPageRef.getParameters().put('serviceRequestId', sr.Id);
			Test.setCurrentPage(testPageRef);
			S2C_OrderProvisioningDetailsCtlr ctrl = new S2C_OrderProvisioningDetailsCtlr();
			ctrl.getSRSPodsTemplateInfo();
			ctrl.getWorkRequestInfo();
			ctrl.getContractInfo();
			ctrl.getServiceRequestInfo();


			Test.stopTest();
		}

	}
	
	
	
}