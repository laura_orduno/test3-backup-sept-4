/**
 * S2C_MoveOrderCtlr
 * @description Controls the functionality of the wizard
 * @author Thomas Tran, Traction on Demand
 * @date 07-08-2015
 */
public with sharing class S2C_MoveOrderCtlr {
	private static final String OPP_AMOUNT      = 'OPP_AMOUNT';
	private static final String OPP_ACCOUNT_ID  = 'OPP_ACCOUNT_ID';
	private static final String OPP_TYPE        = 'OPP_TYPE';
	private static final String OPP_STAGE_NAME  = 'OPP_STAGE_NAME';
	private static final String OPP_NAME        = 'OPP_NAME';
	private static final String CONTACT_ID      = 'CONTACT_ID';
	public String oppAmount                     {get; set;}
	public String accountId                     {get; set;}
	public String contactId                     {get; set;}
	public String oppType                       {get; set;}
	public String oppStageName                  {get; set;}
	public String oppName                       {get; set;}
	public String datePickerLink                {get; set;}
	public String outRequestDate                {get; set;}
	public String outExistingServiceId          {get; set;}
	public String outServiceIdType              {get; set;}
	public String inRequestDate                 {get; set;}
	public String inExistingServiceId           {get; set;}
	public String inServiceIdType               {get; set;}
	public String orderOppRecordTypeId          {get; set;}
	public String disconnectRecordTypeId        {get; set;}
	public String provideInstallRecordTypeId    {get; set;}
	public String referenceNumberRecordTypeId   {get; set;}
	public String productId                     {get; set;}
	public String inRequesteDateTemp            {get; set;}
	public Service_Request__c outServiceRequest {get; set;}
	public Service_Request__c inServiceRequest  {get; set;}

	public S2C_MoveOrderCtlr() {
		getDatePickerIcon();
		outServiceRequest = new Service_Request__c();
		inServiceRequest = new Service_Request__c();
		inRequestDate = '';
		outRequestDate = '';

		this.oppAmount = ApexPages.currentPage().getParameters().get(OPP_AMOUNT);
		this.accountId = ApexPages.currentPage().getParameters().get(OPP_ACCOUNT_ID);
		this.contactId = ApexPages.currentPage().getParameters().get(CONTACT_ID);
		this.oppType = ApexPages.currentPage().getParameters().get(OPP_TYPE);
		this.oppStageName = ApexPages.currentPage().getParameters().get(OPP_STAGE_NAME);
		this.oppName = ApexPages.currentPage().getParameters().get(OPP_NAME);

		List<RecordType> srsOrderRequestId = [SELECT Id FROM RecordType WHERE SObjectType = 'Opportunity' AND DeveloperName = 'SRS_Order_Request'];
		orderOppRecordTypeId = srsOrderRequestId[0].Id;

		List<RecordType> disconnectId = [SELECT Id FROM RecordType WHERE SObjectType = 'Service_Request__c' AND DeveloperName = 'Provide'];
		disconnectRecordTypeId = disconnectId[0].Id;

		List<RecordType> provideInstallId = [SELECT Id FROM RecordType WHERE SObjectType = 'Service_Request__c' AND DeveloperName = 'Disconnect'];
		provideInstallRecordTypeId = provideInstallId[0].Id;

		List<RecordType> refNumberRecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'SRS_Service_Request_Related_Order__c' AND DeveloperName = 'Reference_Number'];
		referenceNumberRecordTypeId = refNumberRecordTypeId[0].Id;
	}

	/**
	 * save
	 * @description Goes through the process of creating an opportunity and
	 *              creating and assigning the service request to the opp.
	 * @author Thomas Tran, Traction on Demand
	 * @date 07-08-2015
	 */
	public PageReference save(){
		PageReference newRef = null;
		Opportunity newOrder;
		//Order_Request__c newOrderRequest;
		Contact existingContact;
		Service_Request_Contact__c disconnectSRC;
		Service_Request_Contact__c provideInstallSRC;
		SRS_Service_Request_Related_Order__c disconnectSRToProvideInstallSR;
		SRS_Service_Request_Related_Order__c provideInstallSRTodisconnectSR;

		if(outServiceRequest.SRS_PODS_Product__c != null && outRequestDate != '' && inServiceRequest.SRS_PODS_Product__c != null && inRequestDate != ''){
			try{
				newOrder = createOrderOpportunity(Decimal.valueOf(oppAmount), accountId, contactId, oppName, oppType, oppStageName);
				insert newOrder;

				/* Traction Removed - 08-24-2015 - Not Necessary for functionality
				newOrderRequest = createOrderRequest(newOrder);
				insert newOrderRequest;
				*/

				existingContact = updateExistingContact(newOrder, contactId);
				update existingContact;

				createServiceRequest(outServiceRequest, newOrder, accountId, disconnectRecordTypeId, outServiceIdType, outExistingServiceId, outRequestDate);
				insert outServiceRequest;

				createServiceRequest(inServiceRequest, newOrder, accountId, provideInstallRecordTypeId, inServiceIdType, inExistingServiceId, inRequestDate);
				insert inServiceRequest;

				disconnectSRC = createServiceRequestContact(outServiceRequest);
				insert disconnectSRC;

				provideInstallSRC = createServiceRequestContact(inServiceRequest);
				insert provideInstallSRC;

				Service_Request__c outSRName = [SELECT Name FROM Service_Request__c WHERE Id = :outServiceRequest.Id];
				Service_Request__c inSRName = [SELECT Name FROM Service_Request__c WHERE Id = :inServiceRequest.Id];

				disconnectSRToProvideInstallSR = createServiceRequestRelatedOrder(referenceNumberRecordTypeId, outSRName.Name, inServiceRequest);
				insert disconnectSRToProvideInstallSR;

				provideInstallSRTodisconnectSR = createServiceRequestRelatedOrder(referenceNumberRecordTypeId, inSRName.Name, outServiceRequest);
				insert provideInstallSRTodisconnectSR;

				newRef = new PageReference('/' + newOrder.Id);
			} catch(Exception ex){
				ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
			}

		} else{
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill in all the required fields denoted with *'));
		}

		return newRef;
	}

	/**
	 * getDatePickerIcon
	 * @description Retrirves an icon to be used for the date picker.
	 * @author Thomas Tran, Traction on Demand
	 * @date 07-08-2015
	 */
	private void getDatePickerIcon(){
		List<Document> documents = [SELECT Id FROM Document WHERE DeveloperName = 'Datepicker_icon'];
		datePickerLink = '/servlet/servlet.FileDownload?file=' + documents[0].Id;
	}

	/**
	* formatDate
	* @Description Parses the proper values from the date string
	*    and creates a new date value from it.
	* @author Thomas Tran, Traction on Demand
	* @date 07-08-2015
	*/
	private static Date formatDate(String currentDate){
		Integer monthEndNum = currentDate.indexOf('/');
		Integer dayEndNum = currentDate.lastindexOf('/');

		String monthValue = currentDate.substring(0, monthEndNum);
		String dayValue = currentDate.substring(monthEndNum + 1, dayEndNum);
		String yearValue = currentDate.substring(dayEndNum + 1);

		Date newDate = Date.newinstance(Integer.valueOf(yearValue), Integer.valueOf(monthValue), Integer.valueOf(dayValue));

		return newDate;
	}

	/**
	 * getServiceTypeValues
	 * @description Retrives the values stored on the Service Indentifier Type found
	 *              on the service request object.
	 * @author Thomas Tran, Traction on Demand
	 * @date 07-08-2015
	 */
	public static List<SelectOption> getServiceTypeValues(){
		List<SelectOption> options = new List<SelectOption>();
		Schema.DescribeFieldResult fieldResult = Service_Request__c.Service_Identifier_Type__c.getDescribe();
		
		options.add(new SelectOption('', ''));
		
		for(Schema.PicklistEntry currentOptionTemp : fieldResult.getPicklistValues()){
			options.add(new SelectOption(currentOptionTemp.getLabel(), currentOptionTemp.getValue()));
		}

		return options;
	}
    
	/**
	 * getReasonForDisconnectValues
	 * @description Retrives the values stored on the Disconnect Reason Type found
	 *              on the service request object.
	 * @author Thomas Tran, Traction on Demand
	 * @date 07-08-2015
	 */
	public static List<SelectOption> getReasonForDisconnectValues(){
		List<SelectOption> options = new List<SelectOption>();
		Schema.DescribeFieldResult fieldResult = Service_Request__c.Disconnect_Reason__c.getDescribe();
		
		for(Schema.PicklistEntry currentOptionTemp : fieldResult.getPicklistValues()){
			options.add(new SelectOption(currentOptionTemp.getLabel(), currentOptionTemp.getValue()));
		}

		return options;
	}
    
	/**
	* createOrderOpportunity
	* @description Creates new order opportunity
	* @author Thomas Tran, Traction on Demand
	* @date 07-08-2015
	*/
	private Opportunity createOrderOpportunity(Decimal oppAmount, Id accountId, Id contactId, String oppName, String oppType, String oppStageName){
		return new Opportunity(
			Amount = oppAmount,
			CloseDate = System.Today(),
			AccountId = accountId,
			Primary_Order_Contact__c = contactId,
			Name = oppName,
			Type = oppType,
			StageName = oppStageName,
			RecordTypeId = orderOppRecordTypeId
		);
	}

	/**
	 * createServiceRequest
	 * @description Creates service request to be associated to the
	 *              opportunity.
	 * @author Thomas Tran, Traction on Demand
	 * @date 07-08-2015
	 */
	private void createServiceRequest(Service_Request__c currentSR, Opportunity newOrder, Id accountId, Id srRecordTypeId, String serviceType, String serviceId, String requestedDate){
		currentSR.Opportunity__c = newOrder.Id;
		currentSR.Account_Name__c = accountId;
		currentSR.RecordTypeId = srRecordTypeId;
		currentSR.Service_Request_Status__c = 'Originated';
		currentSR.Status__c = 'Not Submitted';
		currentSR.PrimaryContact__c = newOrder.Primary_Order_Contact__c;
		currentSR.Existing_Service_ID__c = serviceId;
		currentSR.Service_Identifier_Type__c = serviceType;
		currentSR.Requested_Date__c = formatDate(requestedDate);
	}

	/**
	 * createServiceRequestContact
	 * @description Sets the primary contacts for the service request
	 * @author Thomas Tran, Traction on Demand
	 * @date 07-08-2015
	 */
	private Service_Request_Contact__c createServiceRequestContact(Service_Request__c currentSR){
		return new Service_Request_Contact__c(
			Service_Request__c = currentSR.Id,
			Contact__c = currentSR.PrimaryContact__c,
			Contact_Type__c = 'Customer Onsite'
		);
	}

	/**
	 * createServiceRequestRelatedOrder
	 * @description Creates a link between two different service request
	 * @author Thomas Tran, Traction on Demand
	 * @date 07-08-2015
	 */
	private SRS_Service_Request_Related_Order__c createServiceRequestRelatedOrder(Id referenceNumberRecordTypeId, String orderRefName, Service_Request__c currentSR){
		return new SRS_Service_Request_Related_Order__c(
			RecordTypeId = referenceNumberRecordTypeId,
			Name = orderRefName,
			Reference_Type__c = 'Related Serv Req',
			RO_Service_Request__c = currentSR.Id
		);
	}

	/**
	 * updateExistingContact
	 * @description Creates a contact object which will be used to update an existing
	 *              contact.
	 * @author Thomas Tran, Traction on Demand
	 * @date 07-08-2015
	 */
	private Contact updateExistingContact(Opportunity newOrder, Id contactId){
		return new Contact(
			Opportunity__c = newOrder.Id,
			SRS_Contact_Type__c = 'Primary Contact',
			Id = contactId
		);
	}

	/**
	 * createOrderRequest
	 * @description Creates a new order request.
	 * @author Thomas Tran, Traction on Demand
	 * @date 07-08-2015
	 */
	/*
	private Order_Request__c createOrderRequest(Opportunity newOrder){
		return new Order_Request__c(
			Opportunity__c = newOrder.Id
		);
	}
	*/

	/**
	 * populateInProductField
	 * @description Populates the IN field when the OUT field is
	 *              set with a value.
	 * @author Thomas Tran, Traction on Demand
	 * @date 07-09-2015
	 */
	public void populateInProductField(){
		inServiceRequest.SRS_PODS_Product__c = (inServiceRequest.SRS_PODS_Product__c == null) ? productId : inServiceRequest.SRS_PODS_Product__c;
	}

	/**
	 * populateInDateField
	 * @description Populates the IN field when the OUT field is
	 *              set with a value.
	 * @author Thomas Tran, Traction on Demand
	 * @date 07-09-2015
	 */
	public void populateInDateField(){
		inRequestDate = (inRequestDate == '') ? inRequesteDateTemp : inRequestDate;
	}

	/**
	 * updateInProduct
	 * @description Populates the IN field when the OUT field is
	 *              set with a value.
	 * @author Thomas Tran, Traction on Demand
	 * @date 07-09-2015
	 */
	public void updateInProduct(){
		inServiceRequest.SRS_PODS_Product__c = productId;
	}

	/**
	 * updateInDate
	 * @description Populates the IN field when the OUT field is
	 *              set with a value.
	 * @author Thomas Tran, Traction on Demand
	 * @date 07-09-2015
	 */
	public void updateInDate(){
		inRequestDate = inRequesteDateTemp;
	}


}