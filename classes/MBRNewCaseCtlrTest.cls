/**
 *  MBRNewCaseCtlrTest.cls
 *
 *  @description
 *      Test Class for the New Case Controller used in the My Business Requests Community. Copied, Not Created By Dan.
 *
 *  @date - 03/01/2014
 *  @author - Dan Reich: Traction on Demand
 *  @author - Christian Wico: cwico@tractionondemand.com
 *  @author - Alex Kong: akong@tractionondemand.com
 */
@isTest(SeeAllData=false)
private class MBRNewCaseCtlrTest {
    static User u {get;set;}

    static {
        u = MBRTestUtils.createPortalUser('');
        MBRTestUtils.createExternalToInternalCustomSettings();
        MBRTestUtils.createParentToChildCustomSettings();
        MBRTestUtils.createTypeToFieldsCustomSettings();

    }

    @isTest public static void testOther() {
        MBRNewCaseCtlr ctlr = new MBRNewCaseCtlr();

        System.assert(ctlr.getCategories().size() > 0);
        System.assert(ctlr.getRequestTypes().size() > 0);        
        System.assert(ctlr.createNewOnSubmit  == false);
        ctlr.createNewOnSubmit = true;
        System.assert(ctlr.createNewOnSubmit == true);
        ctlr.clear();
        ctlr.requestType = 'onlineStatus';
        System.assert(ctlr.getRecordType('Dummy') == 'Generic');
        Attachment a  = ctlr.attachment;

        
    }

    @isTest public static  void testResetValues() {
        PageReference pref = Page.MBRNewCase;
        pref.getParameters().put('category', 'CCICatRequest1');
        Test.setCurrentPage(pref);

        MBRNewCaseCtlr ctlr = new MBRNewCaseCtlr();
        // reset values
        ctlr.caseSubmitted = true;
        ctlr.resetValues();
        System.assert(ctlr.selectedCategory == Label.MBRSelectCategory);

        ctlr.caseSubmitted = false;
        ctlr.resetValues();
        System.assert(ctlr.selectedCategory == 'Account and billing');
    }

    @isTest public static void testInitCheck() {
        
        System.runAs(u) {
            MBRNewCaseCtlr ctlr = new MBRNewCaseCtlr();
            System.assert(ctlr.initCheck() == null);
        }
    }

    @isTest public static void testCreateNewCase() {

        //System.runAs(u) { // sharing issue with this context

            MBRNewCaseCtlr ctlr = new MBRNewCaseCtlr();

            ctlr.attachment.Name='Unit Test Attachment';
            ctlr.attachment.body=Blob.valueOf('Unit Test Attachment Body');

            System.assert(ctlr.createNewCase() == null);

            ctlr.requestType = label.mbrSelectCategoryFirst;   
            System.assert(ctlr.createNewCase() == null);
            
            ctlr.selectedCategory = 'Test';
            ctlr.requestType = label.mbrSelectCategoryFirst;  
            System.assert(ctlr.createNewCase() == null); 
            
            ctlr.requestType = label.mbrSelectType;  
            System.assert(ctlr.createNewCase() == null);

            ctlr.requestType = 'Test';  
            ctlr.createNewCase();
            //System.assert(ctlr.createNewCase() == null);

            ctlr.caseToInsert.recordtypeid = null;   
            System.assert(ctlr.createNewCase() == null);

            ctlr.requestType = 'onlineStatus';
            ctlr.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId;   
            ctlr.createNewCase();
        //}
    }

    @isTest public static void testCreateParentCase() {

        MBRNewCaseCtlr ctlr = new MBRNewCaseCtlr();


        ctlr.selectedCategory = 'Account and billing';
        ctlr.requestType = 'Account review';
        ctlr.caseToInsert.Description = 'Test Description';
        ctlr.caseToInsert.Subject = 'Test Subject';
        ctlr.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId;   
        ctlr.createNewCase();

        List<Case> parentCases = [SELECT Id, CaseNumber, My_Business_Requests_Type__c FROM Case ORDER BY CreatedDate DESC];
        Case parentCase = null;
        if (parentCases.size() > 0) {
            parentCase = parentCases[0];
            parentCase.status = 'Closed';
            update parentCase;
        }

        // set pageReference and querystring params
        PageReference submitPageRef = Page.MBRNewCase;
        Test.setCurrentPage(submitPageRef);
        ApexPages.currentPage().getParameters().put('reopen', parentCase.CaseNumber);

        // instantiate a second controller for the child insert
        MBRNewCaseCtlr ctlr2 = new MBRNewCaseCtlr();
        //ctlr2.selectedCategory = null;
        //ctlr2.requestType = null;
        ctlr2.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId;   
        ctlr2.createNewCase();

        Case childCase = [SELECT Id, Parent.Id, ParentId, CaseNumber FROM Case WHERE ParentId != null ORDER BY CreatedDate DESC LIMIT 1][0];
        System.assertEquals(parentCase.Id, childCase.Parent.Id);
    }

    @isTest public static void testUpdateCasePriorityToUrgent() {
        
        MBRNewCaseCtlr ctlr = new MBRNewCaseCtlr();

        String originValue = 'CCI Self-Serve';
        Customer_Interface_Settings__c cis = Customer_Interface_Settings__c.getInstance();
        if(cis.Case_Origin_Value__c != null) {
            originValue = cis.Case_Origin_Value__c;
        }

        List<Case> cases = new List<Case>();
        cases.add( new Case(Subject='4', Status = 'New', Priority = 'Medium', Origin = originValue) ); // subject will be used for day of week
        cases.add( new Case(Subject='5', Status = 'New', Priority = 'Medium', Origin = originValue) );
        cases.add( new Case(Subject='6', Status = 'New', Priority = 'Medium', Origin = originValue) );
        cases.add( new Case(Subject='0', Status = 'New', Priority = 'Medium', Origin = originValue) );
        cases.add( new Case(Subject='0', Status = 'New', Priority = 'Urgent', Origin = originValue) );
        cases.add( new Case(Subject='0', Status = 'In Progress', Priority = 'Medium', Origin = originValue) );

        System.debug('akong: cases A: ' + cases);

        try {
            insert cases;
        } catch (Exception e) {
            System.debug('akong: case insert exception: ' + e);
        }

        Test.startTest();
        MBRNewCaseCtlr.updateCasePriorityToUrgent();
        Test.stopTest();

        List<Case> urgentCases = [SELECT Id, Priority FROM Case WHERE Priority = 'Urgent'];
        System.assertEquals(5, urgentCases.size());
    }

    @isTest
    public static void testAddOnForm(){
        MBRNewCaseCtlr ctlr = new MBRNewCaseCtlr();
        ctlr.requestType = 'New product/service - add-on order';
        ctlr.selectedCategory = 'Products and services';
        ctlr.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId; 

        ctlr.formWrapper.addOnUsers = MBRTestUtils.createAddOnUsers();
        List<SelectOption> paymentTypes = ctlr.getPaymentOptions();
        ctlr.formWrapper.payment = MBRTestUtils.createPaymentInfo(paymentTypes[0].getValue());
        ctlr.formWrapper.additionalInfo = 'More information';

        ctlr.createNewCase();
        List<Case> cases = [SELECT Id, Description, Subject FROM Case];
        System.assertEquals(cases.size(), 1);
        System.assert(cases[0].Description.contains('This user has his/her own device'));
        System.assert(cases[0].Description.contains(ctlr.formWrapper.additionalInfo));
        System.assert(cases[0].Subject.equalsIgnoreCase(ctlr.requestType));
    }

    @isTest
    public static void testHardwareForm(){
        MBRNewCaseCtlr ctlr = new MBRNewCaseCtlr();
        List<SelectOption> paymentTypes = ctlr.getPaymentOptions();
        ctlr.formWrapper.payment = MBRTestUtils.createPaymentInfo(paymentTypes[0].getValue());
        ctlr.requestType = 'New product/service - hardware/accessory order';
        ctlr.selectedCategory = 'Products and services';
        ctlr.formWrapper.additionalInfo = 'More information';
        ctlr.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId; 
        ctlr.formWrapper.hardwares.add(MBRTestUtils.createHardware());
        ctlr.createNewCase();
        List<Case> cases = [SELECT Id, Description, Subject FROM Case];
        System.assertEquals(cases.size(), 1);
        System.assert(cases[0].Description.contains('Hardware/Accessory'));
        System.assert(cases[0].Subject.equalsIgnoreCase(ctlr.requestType));
        System.assert(cases[0].Description.contains(ctlr.formWrapper.additionalInfo));
    }

    @isTest
    public static void testRenewalForm(){
        MBRNewCaseCtlr ctlr = new MBRNewCaseCtlr();
        List<SelectOption> paymentTypes = ctlr.getPaymentOptions();
        ctlr.formWrapper.payment = MBRTestUtils.createPaymentInfo(paymentTypes[0].getValue());

        ctlr.formWrapper.additionalInfo = 'More information';
        ctlr.requestType = 'New product/service - renewal order';
        ctlr.selectedCategory = 'Products and services';
        ctlr.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId; 
        ctlr.formWrapper.renewalUsers.add(MBRTestUtils.createRenewalUser());
        ctlr.createNewCase();
        List<Case> cases = [SELECT Id, Description, Subject FROM Case];
        System.assertEquals(cases.size(), 1);
        System.assert(cases[0].Description.contains('Mobile Subscriber Number:'));
        System.assert(cases[0].Subject.equalsIgnoreCase(ctlr.requestType));
        System.assert(cases[0].Description.contains(ctlr.formWrapper.additionalInfo));
    }

    @isTest
    public static void testChangeServiceForm(){
        MBRNewCaseCtlr ctlr = new MBRNewCaseCtlr();
        ctlr.requestType = 'Change service';
        ctlr.selectedCategory = 'Products and services';
        ctlr.formWrapper.additionalInfo = 'More information';
        ctlr.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId; 
        ctlr.formWrapper.userServiceChange.add(MBRTestUtils.createServiceUser());
        ctlr.createNewCase();
        List<Case> cases = [SELECT Id, Description, Subject FROM Case];
        System.assertEquals(cases.size(), 1);
        System.assert(cases[0].Description.contains('Additional Information:'));
        System.assert(cases[0].Description.contains(ctlr.formWrapper.additionalInfo));
        System.assert(cases[0].Subject.equalsIgnoreCase(ctlr.requestType));
    }

    @isTest
    public static void testTransferOwnershipForm(){
        MBRNewCaseCtlr ctlr = new MBRNewCaseCtlr();
        ctlr.requestType = 'Transfer of business ownership';
        ctlr.selectedCategory = 'Account and billing';
        ctlr.caseToInsert.MobilityRequest__c = true;
        ctlr.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId; 
        ctlr.formWrapper.ownerTransfer = MBRTestUtils.createTransferOwnership();
        ctlr.createNewCase();
        List<Case> cases = [SELECT Id, Description, Subject FROM Case];
        System.assertEquals(cases.size(), 1);
        //System.assert(cases[0].Description.contains('Transfer of Business Ownership'));
        //System.assert(cases[0].Description.contains('Mobile number(s) To Transfer'));
        System.assert(cases[0].Subject.equalsIgnoreCase(ctlr.requestType));
        delete cases;

        // test WLN version
        ctlr.requestType = 'Transfer of business ownership';
        ctlr.selectedCategory = 'Account and billing';
        ctlr.caseToInsert.MobilityRequest__c = false;
        ctlr.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId; 
        ctlr.createNewCase();
        List<Case> WLNCases = [SELECT Id, Description, Subject FROM Case];
        System.assertEquals(WLNCases.size(), 1);
        //System.assert(WLNCases[0].Description.contains('Transfer of Business Ownership'));
        System.assert(WLNCases[0].Description.contains('New Owner Name'));
        System.assert(WLNCases[0].Subject.equalsIgnoreCase(ctlr.requestType));
    }

    @isTest
    public static void testAuthPersonForm(){
        MBRNewCaseCtlr ctlr = new MBRNewCaseCtlr();
        ctlr.requestType = 'Account Update - update authorized person';
        ctlr.selectedCategory = 'Account and billing';
        ctlr.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId; 
        ctlr.formWrapper.authPerson = MBRTestUtils.createAuthorizedPerson();
        ctlr.createNewCase();
        List<Case> cases = [SELECT Id, Description, Subject FROM Case];
        System.assertEquals(cases.size(), 1);
        System.assert(cases[0].Description.contains(Label.MBRAuthorizedName));
        System.assert(cases[0].Subject.equalsIgnoreCase(ctlr.requestType));
    }

    @isTest
    public static void testBillingForm(){
        MBRNewCaseCtlr ctlr = new MBRNewCaseCtlr();
        ctlr.requestType = 'Billing inquiry';
        ctlr.selectedCategory = 'Account and billing';
        ctlr.formWrapper.additionalInfo = 'More information';
        ctlr.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId; 
        ctlr.formWrapper.billing = MBRTestUtils.createBilling();
        ctlr.createNewCase();
        List<Case> cases = [SELECT Id, Description, Subject FROM Case];
        System.assertEquals(cases.size(), 1);
//        System.assert(cases[0].Description.contains('Invoice Date'));
//        System.assert(cases[0].Subject.equalsIgnoreCase(ctlr.requestType));
//        System.assert(cases[0].Description.contains(ctlr.formWrapper.additionalInfo));
    }



	/* VITILcare portal */

    @isTest public static void VITILcare_testOther() {
        VITILcareNewCaseCtlr ctlr = new VITILcareNewCaseCtlr();

        System.assert(ctlr.getCategories().size() > 0);
        System.assert(ctlr.getRequestTypes().size() > 0);        
        System.assert(ctlr.createNewOnSubmit  == false);
        ctlr.createNewOnSubmit = true;
        System.assert(ctlr.createNewOnSubmit == true);
        ctlr.clear();
        ctlr.requestType = 'onlineStatus';
//        System.assert(ctlr.getRecordType('Dummy') == 'Generic');
        Attachment a  = ctlr.attachment;

        
    }

    @isTest public static  void VITILcare_testResetValues() {
        PageReference pref = Page.VITILcareNewCase;
        pref.getParameters().put('category', 'CCICatRequest1');
        Test.setCurrentPage(pref);

        VITILcareNewCaseCtlr ctlr = new VITILcareNewCaseCtlr();
        // reset values
        ctlr.caseSubmitted = true;
        ctlr.resetValues();
        System.assert(ctlr.selectedCategory == Label.MBRSelectCategory);

        ctlr.caseSubmitted = false;
        ctlr.resetValues();
        System.assert(ctlr.selectedCategory == 'Account and billing');
    }

    @isTest public static void VITILcare_testInitCheck() {
        
        System.runAs(u) {
            VITILcareNewCaseCtlr ctlr = new VITILcareNewCaseCtlr();
            System.assert(ctlr.initCheck() == null);
        }
    }

    @isTest public static void VITILcare_testCreateNewCase() {

        //System.runAs(u) { // sharing issue with this context

            VITILcareNewCaseCtlr ctlr = new VITILcareNewCaseCtlr();

            ctlr.attachment.Name='Unit Test Attachment';
            ctlr.attachment.body=Blob.valueOf('Unit Test Attachment Body');

//            System.assert(ctlr.createNewCase() == null);

            ctlr.requestType = label.mbrSelectCategoryFirst;   
//            System.assert(ctlr.createNewCase() == null);
            
            ctlr.selectedCategory = 'Test';
            ctlr.requestType = label.mbrSelectCategoryFirst;  
//            System.assert(ctlr.createNewCase() == null); 
            
            ctlr.requestType = label.mbrSelectType;  
//            System.assert(ctlr.createNewCase() == null);

            ctlr.requestType = 'Test';  
            ctlr.createNewCase();
            //System.assert(ctlr.createNewCase() == null);

            ctlr.caseToInsert.recordtypeid = null;   
//            System.assert(ctlr.createNewCase() == null);

            ctlr.requestType = 'onlineStatus';
            ctlr.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId;   
            ctlr.createNewCase();
        //}
    }

    @isTest public static void VITILcare_testCreateParentCase() {

        VITILcareNewCaseCtlr ctlr = new VITILcareNewCaseCtlr();


        ctlr.selectedCategory = 'Account and billing';
        ctlr.requestType = 'Account review';
        ctlr.caseToInsert.Description = 'Test Description';
        ctlr.caseToInsert.Subject = 'Test Subject';
        ctlr.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId;   
        ctlr.createNewCase();

        List<Case> parentCases = [SELECT Id, CaseNumber, My_Business_Requests_Type__c FROM Case ORDER BY CreatedDate DESC];
        Case parentCase = null;
        if (parentCases.size() > 0) {
            parentCase = parentCases[0];
            parentCase.status = 'Closed';
            update parentCase;
        }

        // set pageReference and querystring params
        PageReference submitPageRef = Page.VITILcareNewCase;
        Test.setCurrentPage(submitPageRef);
        ApexPages.currentPage().getParameters().put('reopen', parentCase.CaseNumber);

        // instantiate a second controller for the child insert
        VITILcareNewCaseCtlr ctlr2 = new VITILcareNewCaseCtlr();
        //ctlr2.selectedCategory = null;
        //ctlr2.requestType = null;
        ctlr2.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId;   
        ctlr2.createNewCase();

        Case childCase = [SELECT Id, Parent.Id, ParentId, CaseNumber FROM Case WHERE ParentId != null ORDER BY CreatedDate DESC LIMIT 1][0];
        System.assertEquals(parentCase.Id, childCase.Parent.Id);
    }

    /*****
    @isTest public static void VITILcare_testUpdateCasePriorityToUrgent() {
        
        VITILcareNewCaseCtlr ctlr = new VITILcareNewCaseCtlr();

        String originValue = 'CCI Self-Serve';
        Customer_Interface_Settings__c cis = Customer_Interface_Settings__c.getInstance();
        if(cis.Case_Origin_Value__c != null) {
            originValue = cis.Case_Origin_Value__c;
        }

        List<Case> cases = new List<Case>();
        cases.add( new Case(Subject='4', Status = 'New', Priority = 'Medium', Origin = originValue) ); // subject will be used for day of week
        cases.add( new Case(Subject='5', Status = 'New', Priority = 'Medium', Origin = originValue) );
        cases.add( new Case(Subject='6', Status = 'New', Priority = 'Medium', Origin = originValue) );
        cases.add( new Case(Subject='0', Status = 'New', Priority = 'Medium', Origin = originValue) );
        cases.add( new Case(Subject='0', Status = 'New', Priority = 'Urgent', Origin = originValue) );
        cases.add( new Case(Subject='0', Status = 'In Progress', Priority = 'Medium', Origin = originValue) );

        System.debug('akong: cases A: ' + cases);

        try {
            insert cases;
        } catch (Exception e) {
            System.debug('akong: case insert exception: ' + e);
        }

        Test.startTest();
        VITILcareNewCaseCtlr.updateCasePriorityToUrgent();
        Test.stopTest();

        List<Case> urgentCases = [SELECT Id, Priority FROM Case WHERE Priority = 'Urgent'];
        System.assertEquals(5, urgentCases.size());
    }
*/
    
    @isTest
    public static void VITILcare_testAddOnForm(){
        VITILcareNewCaseCtlr ctlr = new VITILcareNewCaseCtlr();
        ctlr.requestType = 'New product/service - add-on order';
        ctlr.selectedCategory = 'Products and services';
        ctlr.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId; 

//        ctlr.formWrapper.addOnUsers = MBRTestUtils.createAddOnUsers();
        List<SelectOption> paymentTypes = ctlr.getPaymentOptions();
//        ctlr.formWrapper.payment = MBRTestUtils.createPaymentInfo(paymentTypes[0].getValue());
        ctlr.formWrapper.additionalInfo = 'More information';

        ctlr.createNewCase();
        List<Case> cases = [SELECT Id, Description, Subject FROM Case];
        System.assertEquals(cases.size(), 1);
//        System.assert(cases[0].Description.contains('This user has his/her own device'));
//        System.assert(cases[0].Description.contains(ctlr.formWrapper.additionalInfo));
//        System.assert(cases[0].Subject.equalsIgnoreCase(ctlr.requestType));
    }

    @isTest
    public static void VITILcare_testHardwareForm(){
        VITILcareNewCaseCtlr ctlr = new VITILcareNewCaseCtlr();
        List<SelectOption> paymentTypes = ctlr.getPaymentOptions();
//        ctlr.formWrapper.payment = MBRTestUtils.createPaymentInfo(paymentTypes[0].getValue());
        ctlr.requestType = 'New product/service - hardware/accessory order';
        ctlr.selectedCategory = 'Products and services';
        ctlr.formWrapper.additionalInfo = 'More information';
        ctlr.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId; 
//        ctlr.formWrapper.hardwares.add(MBRTestUtils.createHardware());
        ctlr.createNewCase();
        List<Case> cases = [SELECT Id, Description, Subject FROM Case];
//        System.assertEquals(cases.size(), 1);
//        System.assert(cases[0].Description.contains('Hardware/Accessory'));
//        System.assert(cases[0].Subject.equalsIgnoreCase(ctlr.requestType));
//        System.assert(cases[0].Description.contains(ctlr.formWrapper.additionalInfo));
    }

    @isTest
    public static void VITILcare_testRenewalForm(){
        VITILcareNewCaseCtlr ctlr = new VITILcareNewCaseCtlr();
        List<SelectOption> paymentTypes = ctlr.getPaymentOptions();
//        ctlr.formWrapper.payment = MBRTestUtils.createPaymentInfo(paymentTypes[0].getValue());

        ctlr.formWrapper.additionalInfo = 'More information';
        ctlr.requestType = 'New product/service - renewal order';
        ctlr.selectedCategory = 'Products and services';
        ctlr.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId; 
//        ctlr.formWrapper.renewalUsers.add(MBRTestUtils.createRenewalUser());
        ctlr.createNewCase();
        List<Case> cases = [SELECT Id, Description, Subject FROM Case];
        System.assertEquals(cases.size(), 1);
//        System.assert(cases[0].Description.contains('Mobile Subscriber Number:'));
//        System.assert(cases[0].Subject.equalsIgnoreCase(ctlr.requestType));
//        System.assert(cases[0].Description.contains(ctlr.formWrapper.additionalInfo));
    }

    @isTest
    public static void VITILcare_testChangeServiceForm(){
        VITILcareNewCaseCtlr ctlr = new VITILcareNewCaseCtlr();
        ctlr.requestType = 'Change service';
        ctlr.selectedCategory = 'Products and services';
        ctlr.formWrapper.additionalInfo = 'More information';
        ctlr.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId; 
//        ctlr.formWrapper.userServiceChange.add(MBRTestUtils.createServiceUser());
        ctlr.createNewCase();
        List<Case> cases = [SELECT Id, Description, Subject FROM Case];
//        System.assertEquals(cases.size(), 1);
//        System.assert(cases[0].Description.contains('Additional Information:'));
//        System.assert(cases[0].Description.contains(ctlr.formWrapper.additionalInfo));
//        System.assert(cases[0].Subject.equalsIgnoreCase(ctlr.requestType));
    }

    /*****
    @isTest
    public static void VITILcare_testTransferOwnershipForm(){
        VITILcareNewCaseCtlr ctlr = new VITILcareNewCaseCtlr();
        ctlr.requestType = 'Transfer of business ownership';
        ctlr.selectedCategory = 'Account and billing';
        ctlr.caseToInsert.MobilityRequest__c = true;
        ctlr.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId; 
//        ctlr.formWrapper.ownerTransfer = MBRTestUtils.createTransferOwnership();
        ctlr.createNewCase();
        List<Case> cases = [SELECT Id, Description, Subject FROM Case];
        System.assertEquals(cases.size(), 1);
        //System.assert(cases[0].Description.contains('Transfer of Business Ownership'));
        //System.assert(cases[0].Description.contains('Mobile number(s) To Transfer'));
        System.assert(cases[0].Subject.equalsIgnoreCase(ctlr.requestType));
        delete cases;

        // test WLN version
        ctlr.requestType = 'Transfer of business ownership';
        ctlr.selectedCategory = 'Account and billing';
        ctlr.caseToInsert.MobilityRequest__c = false;
        ctlr.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId; 
        ctlr.createNewCase();
        List<Case> WLNCases = [SELECT Id, Description, Subject FROM Case];
        System.assertEquals(WLNCases.size(), 1);
        //System.assert(WLNCases[0].Description.contains('Transfer of Business Ownership'));
        System.assert(WLNCases[0].Description.contains('New Owner Name'));
        System.assert(WLNCases[0].Subject.equalsIgnoreCase(ctlr.requestType));
    }
*/
    
    @isTest
    public static void VITILcare_testAuthPersonForm(){
        VITILcareNewCaseCtlr ctlr = new VITILcareNewCaseCtlr();
        ctlr.requestType = 'Account Update - update authorized person';
        ctlr.selectedCategory = 'Account and billing';
        ctlr.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId; 
//        ctlr.formWrapper.authPerson = MBRTestUtils.createAuthorizedPerson();
        ctlr.createNewCase();
        List<Case> cases = [SELECT Id, Description, Subject FROM Case];
        System.assertEquals(cases.size(), 1);
//        System.assert(cases[0].Description.contains(Label.MBRAuthorizedName));
//        System.assert(cases[0].Subject.equalsIgnoreCase(ctlr.requestType));
    }

    @isTest
    public static void VITILcare_testBillingForm(){
        VITILcareNewCaseCtlr ctlr = new VITILcareNewCaseCtlr();
        ctlr.requestType = 'Billing inquiry';
        ctlr.selectedCategory = 'Account and billing';
        ctlr.formWrapper.additionalInfo = 'More information';
        ctlr.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId; 
//        ctlr.formWrapper.billing = MBRTestUtils.createBilling();
        ctlr.createNewCase();
        List<Case> cases = [SELECT Id, Description, Subject FROM Case];
        System.assertEquals(cases.size(), 1);
//        System.assert(cases[0].Description.contains('Invoice Date'));
//        System.assert(cases[0].Subject.equalsIgnoreCase(ctlr.requestType));
//        System.assert(cases[0].Description.contains(ctlr.formWrapper.additionalInfo));
    }

    @isTest
    public static void VITILcare_testRecorDType(){
        VITILcareNewCaseCtlr ctlr = new VITILcareNewCaseCtlr();
        String s = ctlr.getRecordType('NoMatch');
    }

    @isTest
    public static void VITILcare_PopulateDescription(){
        VITILcareNewCaseCtlr ctlr = new VITILcareNewCaseCtlr();
        VITILcareFormWrapper fw = new VITILcareFormWrapper();
        fw.getTicketInfo.subject = 'subject';
        fw.getTicketInfo.projectNum = 'projNum';
        fw.getTicketInfo.phoneNumber = 'phoneNumber';
        fw.getTicketInfo.siteAddress = 'sitesaddres';
        fw.getTicketInfo.additionalNote = 'additionalNote';
        fw.getTicketInfo.reportedBy = 'reportedby';
        fw.getTicketInfo.reportedByCompany = 'reportedByCompany';
        fw.getTicketInfo.reportedByPhNum = 'reportedByPhNum';
        fw.getTicketInfo.ReportedByEm = 'reportedbyem';
        fw.getTicketInfo.OnsiteContactName = 'OnsiteContactName';
        fw.getTicketInfo.OnsiteContactNumber = 'OnsiteContactNumber';
        fw.getTicketInfo.SiteCompanyName = 'SiteCompanyName';
        fw.getTicketInfo.AccessHours = 'AccessHours';
        fw.getTicketInfo.OngoingCalls = 'OngoingCalls';
        fw.getTicketInfo.AllSetsAffected = 'setsAffected';
        fw.getTicketInfo.PowerIssue = 'powerIssue';
        ctlr.formWrapper = fw;
        ctlr.populateDescription('Test');
    }

     @isTest
    public static void VITILcare_PopulateDescriptionFrench(){
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
       System.runAs(thisUser){
            u.LanguageLocaleKey = 'fr';
            update u;
        }
        VITILcareNewCaseCtlr ctlr = new VITILcareNewCaseCtlr();
        VITILcareFormWrapper fw = new VITILcareFormWrapper();
        fw.getTicketInfo.subject = 'subject';
        fw.getTicketInfo.projectNum = 'projNum';
        fw.getTicketInfo.phoneNumber = 'phoneNumber';
        fw.getTicketInfo.siteAddress = 'sitesaddres';
        fw.getTicketInfo.additionalNote = 'additionalNote';
        fw.getTicketInfo.reportedBy = 'reportedby';
        fw.getTicketInfo.reportedByCompany = 'reportedByCompany';
        fw.getTicketInfo.reportedByPhNum = 'reportedByPhNum';
        fw.getTicketInfo.ReportedByEm = 'reportedbyem';
        fw.getTicketInfo.OnsiteContactName = 'OnsiteContactName';
        fw.getTicketInfo.OnsiteContactNumber = 'OnsiteContactNumber';
        fw.getTicketInfo.SiteCompanyName = 'SiteCompanyName';
        fw.getTicketInfo.AccessHours = 'AccessHours';
        fw.getTicketInfo.OngoingCalls = 'OngoingCalls';
        fw.getTicketInfo.AllSetsAffected = 'setsAffected';
        fw.getTicketInfo.PowerIssue = 'powerIssue';
        ctlr.formWrapper = fw;
        System.runAs(u){
            ctlr.populateDescription('Test');
        }
    }

   

}