public with sharing class OCOM_Chargeable_Pricing {
    public Static void setChargeableJSONOli(List<vlocity_cmt.ItemWrapper> itemList) {
        String addedProdId = '';
        String parentOliId = '';
        String addedOliId = '';
        String childProdSpecName = '';
        String orderId = '';
        Map<String, OrderItem>overlineChildMap  = new Map<String,OrderItem>();
        Map<String,OrderItem> topOfferAndOverlineMap = new Map<String,OrderItem>();
        Map<String,OrderItem> childLineItemsMap  = new Map<String,OrderItem>();
        Map<String,String> chargeableItemsMap = new Map<String,String>();
        Map<String,String> chargeableItems_True_Map = new Map<String,String>();
        Map<String, Decimal> topOfferIncFeatureMap = new Map<String,Decimal>();
        Map<String, Decimal> topOfferCallGaurdianFeatureMap = new Map<String,Decimal>();
        Map<String,OrderItem> topOfferMap = new Map<String,OrderItem>();
        Map<String,OrderItem> topOfferOliIDMap = new Map<String,OrderItem>();
        Map<Integer,String>oiToChargeableCountMap = new Map<Integer,String>();
        Map<Integer,String>oiToCallGaurdianChargeableCountMap = new Map<Integer,String>();
        Map<String,String> parentOliToProdIDMap = new  Map<String,String>();
        Boolean isNotChargeable = false;
        String topOfferID = '';
        String addedPbeId = '';
        OrderItem addedOrderItem = new OrderItem();
         List<OrderItem> orderItemListToUpdate = new List<OrderItem>();
         Boolean isAddtoOrderCall = false;
         Boolean proceedToCargeable = true;
        
        //added for chargeablecount
        /****/

        // Get OrderID, AddOrderItemId and PrentID from static flow MAP
    try {
        if(vlocity_cmt.FlowStaticMap.FlowMap.Containskey('preInvoke.addedItem')){
                String inputPreHook = (String)vlocity_cmt.FlowStaticMap.FlowMap.get('preInvoke.addedItem');
                Map<String,Object> inputPreHookMap = (Map<String,Object>)JSON.deserializeUntyped(inputPreHook);
                
                // List<SObject> itemList =  (list<SObject>)vlocity_cmt.FlowStaticMap.flowMap.get('itemList');
                 // System.debug('Validation itemslist___::' + JSON.serializePretty(itemList)) ;   
                if(inputPreHookMap.containsKey('items')){
                    Object  items = (Object) inputPreHookMap.get('items');
                    //System.debug('Validation Impl Input_____::' + JSON.serializePretty(items));
                    if(items instanceof List<Object> ){
                        Object itemsNode = ((List<Object>)items)[0];
                        if(itemsNode instanceof Map<String,Object>){
                            Map<String,Object> itemsNodeMap = (Map<String,Object>)itemsNode;
                            addedPbeId = (String)itemsNodeMap.get('itemId');
                            if(itemsNodeMap.containsKey('parentRecord'))
                                { 
                                    isAddtoOrderCall = true;
                                    Map<String,Object> parentRecords  = (Map<String,Object>)itemsNodeMap.get('parentRecord');
                                    if(parentRecords.containsKey('records')) { 
                                        Object recordsInfo = parentRecords.get('records');
                                        if(recordsInfo instanceof List<Object>)  { 
                                            List<Object> recordsInfoList = (List<Object>) recordsInfo;
                                            if(recordsInfoList.Size() > 0 && !recordsInfoList.isEmpty()){
                                                Map<String,Object> recordsMap  = (Map<String,Object>)recordsInfoList[0];
                                                if(recordsMap != null && recordsMap.containsKey('Id') && (recordsMap.get('Id') instanceof Map<String,Object>)  ){
                                                     Map<String,Object>parentRecInfo = (Map<String,Object>)recordsMap.get('Id');
                                                     parentOliId = (String) parentRecInfo.get('value');
                                                     //('****** parentId *******' + parentOliId);
                                                         if(recordsMap.containsKey('vlocity_cmt__RootItemId__c')){
                                                            Map<String,Object> topofferInfo = (Map<String,Object>)recordsMap.get('vlocity_cmt__RootItemId__c');
                                                            topOfferID = (String)topofferInfo.get('value');
                                                            vlocity_cmt.FlowStaticMap.FlowMap.put('postCartItems.preinvoke.isaddToOrder',true);
                                                        }
                                                        else{
                                                            proceedToCargeable = false;
                                                            vlocity_cmt.FlowStaticMap.FlowMap.put('postCartItems.preinvoke.isdevice',true);
                                                        }
                                                    }
                                                }
                                            }
                                         }
                                     }
                                }
                            }
                        }
            orderId = (String)inputPreHookMap.get('cartId');

            Map<id, OrderItem> addedOliIdMap = new Map<id, OrderItem>();
            //String addedOliId ;

            for (vlocity_cmt.ItemWrapper ItemWrapper:  itemList){
                if(ItemWrapper.itemSObject != null ){
 
                    SObject sObj = ItemWrapper.itemSObject;
                if(sObj.getSObjectType().getDescribe().getName()=='OrderItem'){
                     OrderItem oli = (OrderItem) sObj;
                    if(oli.PricebookEntryId == addedPbeId && oli.vlocity_cmt__ParentItemId__c == parentOliId && oli.vlocity_cmt__RootItemId__c == topOfferID ){
                        addedOliIdMap.put(oli.id, oli);
                        addedOliId = oli.id;
                    }
                }
                }
 
            }

            if(Test.isRunningTest() && vlocity_cmt.FlowStaticMap.FlowMap != null && vlocity_cmt.FlowStaticMap.FlowMap.containsKey('testdata.chargeableaddedOli')  ){
                addedOliId =  (String)vlocity_cmt.FlowStaticMap.FlowMap.get('testdata.chargeableaddedOli');
            }
             
            System.debug('addedOliId *************' +addedOliId);

            if(proceedToCargeable == true && isAddtoOrderCall == true){
                         if(orderId != null && orderId != ''){
                          Map<String,Object> chargeableConstructmap  = OCOM_CustomHookUtil.constructChargeableItemsMap(orderId,topOfferID,addedOliId, parentOliId);

                            topOfferAndOverlineMap = (Map<String,OrderItem>)chargeableConstructmap.get('topOfferAndOverlineMap');
                            overlineChildMap = (Map<String, OrderItem>)chargeableConstructmap.get('overlineChildMap');
                            childLineItemsMap = (Map<String,OrderItem>)chargeableConstructmap.get('childLineItemsMap');
                            topOfferMap = (Map<String,OrderItem>)chargeableConstructmap.get('topOfferMap');
                            topOfferOliIDMap = (Map<String,OrderItem>)chargeableConstructmap.get('topOfferOliIDMap');
                            addedOrderItem = (OrderItem)chargeableConstructmap.get('addedOrderItem');
                            parentOliToProdIDMap = (Map<String,String>)chargeableConstructmap.get('parentOliToProdIDMap');
                           // isNotChargeable = (Boolean)chargeableConstructmap.get('isNotChargeable');
                        }
                       // system.debug('addedOrderItem__' + addedOrderItem);
                    if(addedOrderItem != null ){
                            addedOliId = (String)addedOrderItem.id;
                            
                             if(overlineChildMap != null && overlineChildMap.size()>0 && !overlineChildMap.isEmpty() && overlineChildMap.containsKey(addedOliId) ){
                                               topOfferMap.clear();
                                               topOfferMap.put(overlineChildMap.get(addedOliId).vlocity_cmt__Product2Id__c, overlineChildMap.get(addedOliId)) ;
                                            }

                            // Query AttributeAssignment Table to get the Chargeable OLIS
                            Map<String,Object> chargeableOutputMap= OCOM_CustomHookUtil.getChargeableItemsDetails(parentOliToProdIDMap);

                            chargeableItemsMap = (Map<String,String>)chargeableOutputMap.get('chargeableItemsMap');
                            topOfferIncFeatureMap = (Map<String,Decimal>)chargeableOutputMap.get('topOfferIncFeatureMap');
                            chargeableItems_True_Map = (Map<String,String>)chargeableOutputMap.get('chargeableItems_True_Map');
                            topOfferCallGaurdianFeatureMap = (Map<String,Decimal>)chargeableOutputMap.get('topOfferCallGaurdianFeatureMap');
                            system.debug('chargeableItemsMap___' +chargeableItemsMap.Containskey((String)addedOrderItem.vlocity_cmt__Product2Id__c));
                      
                  //if(childLineItemsMap.ContainsKey(addedOliId))
                   // System.debug('>>>> Spec Name' + childLineItemsMap.get(addedOliId));

                        // Check if the currently Added item is Chargeable and set the Boolean for Chargeble JSON update
                    if(chargeableItemsMap != null && chargeableItemsMap.Containskey((String)addedOrderItem.vlocity_cmt__Product2Id__c)){    
                            String childProdSpec = 'xx';
                            //system.debug('>>> chil line items' + childLineItemsMap.ContainsKey(addedOliId));
                        if(childLineItemsMap.ContainsKey(addedOliId))
                            childProdSpec = childLineItemsMap.get(addedOliId).vlocity_cmt__Product2Id__r.ProductSpecification__r.Name;
                            String oliId = (String)addedOrderItem.id;
                           // OrderItem topOffer = topOfferOliIDMap.get(topOfferID);
                           OrderItem parentOli;

                            //Adharsh: Code change for accounting parent offers in bundles
                            if(childLineItemsMap != null && childLineItemsMap.containsKey(addedOrderItem.vlocity_cmt__ParentItemId__c)){
                                  String parentItemId = addedOrderItem.vlocity_cmt__ParentItemId__c;
                                  parentOli = childLineItemsMap.get(parentItemId);
                            }
                            else if(topOfferOliIDMap != null && topOfferOliIDMap.containsKey(addedOrderItem.vlocity_cmt__ParentItemId__c)){
                                 parentOli = topOfferOliIDMap.get(addedOrderItem.vlocity_cmt__ParentItemId__c);
                            }
                           // OrderItem parentOffer = addedOrderItem.vlocity_cmt__ParentItemId__c;
                            // Code change end
                            Integer chargeableCount = Integer.Valueof(parentOli.Running_Count__c);
                            Integer callGaurdianChargeableCount = Integer.Valueof(parentOli.Call_Gaurdian_Items_Count__c);
                                system.debug('childProdSpec__' +childProdSpec);
                                
                                    if( childProdSpec!=null && !childProdSpec.containsIgnoreCase('Call Guardian')){
                                        if(overlineChildMap != null && overlineChildMap.size() > 0 && !overlineChildMap.isEmpty() && overlineChildMap.containsKey(oliId)){
                                            
                                            OrderItem parentOverline = overlineChildMap.get(oliId);
                                            chargeableCount = Integer.Valueof(parentOverline.Running_Count__c);
                                            System.debug('OverLine Offer Running count__' + parentOverline.Running_Count__c);
                                               // parentofferToCompare = parentOverline;
                                            }
                                        //updateTopOfferCounter = true;
                                        chargeableCount++;
                                        oiToChargeableCountMap.put(chargeableCount,oliId); 
                                        Map<String,Object> inputItemsMap = new Map<String,Object>();
                                        inputItemsMap.put('oiToChargeableCountMap', oiToChargeableCountMap);
                                        inputItemsMap.put('topOffer',parentOli);
                                        inputItemsMap.put('overlineChildMap',overlineChildMap);
                                        Decimal topOfferPreviousCount = parentOli.Running_Count__c;
                                        inputItemsMap.put('topOfferIncFeatureMap',topOfferIncFeatureMap);
                                        inputItemsMap.put('childLineItemsMap', childLineItemsMap);
                                        orderItemListToUpdate = OCOM_CustomHookUtil.setNonChargeableItem_new(inputItemsMap);
                                        //system.debug('Increment Cahrgeable Counter....' +orderItemListToUpdate);
                                        
                                    }
                                    // ADHARSH :COMMENTING OUT CALL GAURDIAN LOGIC
                                    /*else if(childProdSpec != null && childProdSpec.containsIgnoreCase('Call Guardian')){
                                        if(overlineChildMap != null && overlineChildMap.size() > 0 && !overlineChildMap.isEmpty() && overlineChildMap.containsKey(oliId)){
                                            
                                            OrderItem parentOverline = overlineChildMap.get(oliId);
                                            callGaurdianChargeableCount = Integer.Valueof(parentOverline.Call_Gaurdian_Items_Count__c);
                                            System.debug('OverLine Offer Running count__' + parentOverline.Call_Gaurdian_Items_Count__c);
                                               // parentofferToCompare = parentOverline;
                                            }
                                        //updateTopOfferCallGaurdianCounter = true;
                                        callGaurdianChargeableCount++;
                                        oiToCallGaurdianChargeableCountMap.put(callGaurdianChargeableCount, oliId);
                                        Map<String,Object> inputItemsMap = new Map<String,Object>();
                                        inputItemsMap.put('overlineChildMap',overlineChildMap);
                                        inputItemsMap.put('oiToCallGaurdianChargeableCountMap', oiToCallGaurdianChargeableCountMap);
                                        inputItemsMap.put('topOffer',topOffer);
                                        Decimal topOfferPreviousCount = topOffer.Call_Gaurdian_Items_Count__c;
                                        inputItemsMap.put('topOfferCallGaurdianFeatureMap',topOfferCallGaurdianFeatureMap);
                                        inputItemsMap.put('childLineItemsMap', childLineItemsMap);
                                        orderItemListToUpdate = OCOM_CustomHookUtil.setNonChargeableItem_new(inputItemsMap);
                                        System.debug('Increment Call Gaurdian Counter....' + orderItemListToUpdate);

                                        }*/
                                        //COMMENT END
                                    
                            }  
                                     
                        }
                    }
                }
        if(orderItemListToUpdate.Size() > 0 && !orderItemListToUpdate.isEmpty())
                update orderItemListToUpdate;

        }catch(Exception ex){
            System.debug(LoggingLevel.ERROR, 'Exception is '+ex);
            System.debug(LoggingLevel.ERROR, 'Exception stack trace '+ex.getStackTraceString());
            throw ex;
               
        }
    }
}