/**
 * S2C_TestUtility
 * @description Class that has helper methods that create commonly used objects
 * @author Thomas Tran, Traction on Demand
 * @date 01-29-2015
 */
@isTest(seeAllData=false)
public class S2C_TestUtility {
	public static final String COMPLEX_ORDER_TYPE = 'Complex Order';
	public static final String ORIGINATED_STAGE_NAME = 'Originated';
	
	/**
	 * Creates new Account
	 * @param  String
	 * @return Account
	 */
	public static Account createAccount(String name){
		Account newAccount = new Account(
			Name = name
		);

		return newAccount;
	}

	/**
	 * Creates new Opportunity
	 * @param  Id, String
	 * @return Opportunity
	 */
	public static Opportunity createOpportunity(Id accountId, String name){
		Opportunity newOpportunity = new Opportunity(
			AccountId = accountId,
			Name = name
		);

		return newOpportunity;
	}

	/**
	 * Creates new OpportunityContactRole
	 * @param  String
	 * @return Contact
	 */
	public static Contact createContact(String lastName){
		Contact newContact = new Contact(
			LastName = lastName
		);
		
		return newContact;
	}
	/**
	 * Creates new OpportunityContactRole
	 * @param  Id, Id, Boolean
	 * @return OpportunityContactRole
	 */
	public static OpportunityContactRole createOpportunityContactRole(Id opportunityId, Id contactId, Boolean isPrimary){
		OpportunityContactRole newOpportunityContactRole = new OpportunityContactRole(
			ContactId = contactId,
			OpportunityId = opportunityId,
			IsPrimary = isPrimary
		);

		return newOpportunityContactRole;
	}

	/**
	 * Retrieves the Order Opportunity that was created
	 * @param  Id
	 * @return Opportunity
	 */
	public static Opportunity getOrderOpportunity(Id salesOpportunityId){
		Opportunity orderOpportunity = [
			SELECT AccountId, OwnerId, Name, Primary_Order_Contact__c, Type, RecordTypeId, StageName, CloseDate, Sales_Opportunity_Id__c, 
				(SELECT Id, SRS_PODS_Product__r.Name, Product_Name__c FROM Service_Requests__r)
			FROM Opportunity
			WHERE Sales_Opportunity_Id__c = :salesOpportunityId
		];

		return orderOpportunity;
	}

	/**
	 * Create new Product
	 * @param  String, String
	 * @return Product2
	 */
	public static Product2 createProduct(String name, String familyName){
		Product2 newProduct = new Product2(
			Name = name,
			Product_Family__c = familyName,
			IsActive = true
		);

		return newProduct;
	}

	/**
	 * Creates new Opp Product Item
	 * @param  Id, Id
	 * @return Opp_Product_Item__c
	 */
	public static Opp_Product_Item__c createOpportunityProductItem(Id productId, Id opportunityId){
		Opp_Product_Item__c newOppProductItem = new Opp_Product_Item__c(
			Product__c = productId,
			Opportunity__c = opportunityId,
			Contract_Length_Month__c = 12.0
		);

		return newOppProductItem;
	}

	/**
	 * Creates new SRS_PODS_Product
	 * @param  String
	 * @return SRS_PODS_Product__c
	 */
	public static SRS_PODS_Product__c createSRSPODSProduct(String name){
		SRS_PODS_Product__c newSRSPodsProduct = new SRS_PODS_Product__c(
			Name = name
		);

		return newSRSPodsProduct;
	}

	/**
	 * Creates new SRS_PODS_Product
	 * @param  null
	 * @return List
	 */
	public static List<S2C_Service_Request_Creation_Order_Types__c> createOrderType(){
		List<S2C_Service_Request_Creation_Order_Types__c> orderTypeList = new List<S2C_Service_Request_Creation_Order_Types__c>();

		orderTypeList.add(new S2C_Service_Request_Creation_Order_Types__c(Name = 'Change', Developer_Name__c = 'Change', Picklist_Value__c = 'Change'));
		orderTypeList.add(new S2C_Service_Request_Creation_Order_Types__c(Name = 'Disconnect', Developer_Name__c = 'Disconnect', Picklist_Value__c = 'Disconnect'));
		orderTypeList.add(new S2C_Service_Request_Creation_Order_Types__c(Name = 'Prequal', Developer_Name__c = 'Prequal', Picklist_Value__c = 'Prequal'));
		orderTypeList.add(new S2C_Service_Request_Creation_Order_Types__c(Name = 'Provide', Developer_Name__c = 'Provide', Picklist_Value__c = 'Provide/Install'));
		orderTypeList.add(new S2C_Service_Request_Creation_Order_Types__c(Name = 'Records_Only', Developer_Name__c = 'Records_Only', Picklist_Value__c = 'Records Only'));

		return orderTypeList;
	}
}