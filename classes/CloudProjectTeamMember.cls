public class CloudProjectTeamMember{
    
    public Id userId;
    public String userName;
    public String teamName;
    public Decimal allocationAmount;
    
    public CloudProjectTeamMember(Id userId, String userName, String teamName, Decimal allocationAmount)
    {
        this.userId = userId;
        this.userName = userName;
        this.teamName = teamName;
        this.allocationAmount = allocationAmount != null ? allocationAmount : 0;
    }           
}