/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
//@isTest (SeeAllData = true)
//Before Commenting out, it's at 52%
public class smb_OpportunityProductHelper_Test {
    /*
    static testMethod void Test_smb_OpportunityProductHelper() {
        //smb_test_utility.createCustomSettingData();
        Account accObj = smb_test_utility.createAccount('', true);
        Contact contObj = smb_test_utility.createContact('', accObj.Id, true);
        Opportunity testOpp = smb_test_utility.createOpportunity('SMB_Care_Opportunity', accObj.Id, contObj.Id, true);
        
        //smb_test_utility.createPQLI('PQL_Test_Data', testOpp.Id);
        //smb_test_utility.createPQLI('PQL_Test_Data_Modified',testOpp.Id);
        
        //list<OpportunityLineItem> lstOLIBundle = [SELECT ID,Agreement__c FROM OpportunityLineItem WHERE OpportunityId =:testOpp.Id];
        
        Test.startTest();
        Product2 product = new Product2(name='AstadiaTestProduct',Sterling_Item_ID__c='p1');
        insert product;
        
        Pricebook2 pricebook = [select id from Pricebook2 where isStandard=true];
        
        PricebookEntry pbe = new PricebookEntry(pricebook2id = pricebook.id, product2id = product.id,unitprice=1.0,isActive=true);
        insert pbe;
        
        //List<Product2> productList = [SELECT id, Name FROM Product2 WHERE name='AstadiaTestProduct'];
        //List<PricebookEntry> pbeList = [SELECT id FROM PricebookEntry WHERE pricebook2id =: productList.get(0).Id];
        OpportunityLineItem oliObj = new OpportunityLineItem(OpportunityId = testOpp.Id, PriceBookEntryId = pbe.Id, Quantity = 1, UnitPrice = 1);
        oliObj.Service_Street__c  = 'Test Street 123';
        oliObj.SACG_Type__c = '1';
        insert oliObj;
        
        Map<Id,OpportunityLineItem> oliOldMap = new Map<Id,OpportunityLineItem>();
        oliOldMap.put(oliObj.Id, oliObj);
		
        OpportunityLineItem newOliRec = oliObj;
        newOliRec.Service_Street__c  = 'Test Street';
		update newOliRec;
		
		Case caseObj = new Case(Opportunity__c = testOpp.Id, SMB_Case_Service_Add_Key__c = 'test key');
		smb_OpportunityProductHelper.createCase(new List<OpportunityLineItem>{newOliRec}, true, oliOldMap);
		smb_OpportunityProductHelper.updateOpportunitySvcAddr(new List<OpportunityLineItem>{newOliRec}, oliOldMap);
		//smb_OpportunityProductHelper.createTask(new List<OpportunityLineItem>{newOliRec});
		smb_OpportunityProductHelper.updateOpportunityForNecessaryDueDates(new List<OpportunityLineItem>{newOliRec}, new List<Opportunity>{testOpp});
		
		Profile prof = [Select id from Profile where name like 'SMB Care%' LIMIT 1];
		User userObj = smb_test_utility.createUser(prof.Id,true);
		system.runAs(userObj) {
			smb_OpportunityProductHelper.restrictDeletionUpdation(new List<OpportunityLineItem>{newOliRec});
		}
		
		//smb_OpportunityProductHelper.CreateEmail(targetId, theCaseId, orgwideId, templateId);
        Test.stopTest();
    }*/
}