/**
 * @author Steve Doucette, Traction on Demand 
 * @date 2018-06-15
 * @description Deletes records in the org based on the Custom Metadata that is passed from the PurgerScheduler
 */
public with sharing class PurgerBatchJob implements Database.Batchable<sObject>, Database.Stateful {

	private static final DateTime NOW = DateTime.now();

	private Purger_Setting__mdt purgerSetting;

	private Integer batchNumber = 0;
	private Integer totalRecordsToBeDelete = 0;

	public PurgerBatchJob(Purger_Setting__mdt purgerSetting) {
		this.purgerSetting = purgerSetting;
	}

	public Database.QueryLocator start(Database.BatchableContext bc) {
		try {
			String queryString = 'SELECT Id, Name FROM ' + purgerSetting.MasterLabel;

			// Determine if we need to add a WHERE clause to query
			List<String> whereClauses = new List<String>();
			if (String.isNotBlank(purgerSetting.Criteria__c)) {
				whereClauses.add('(' + purgerSetting.Criteria__c + ')');
			}
			if (String.isNotBlank(purgerSetting.Exclusion_Field__c)) {
				whereClauses.add(purgerSetting.Exclusion_Field__c + ' = FALSE');
			}
			if (purgerSetting.Retention_Time__c != null && purgerSetting.Retention_Time__c > 0) {
				DateTime retentionTime = NOW.addMinutes(-Integer.valueOf(purgerSetting.Retention_Time__c));
				whereClauses.add('CreatedDate < ' + retentionTime.formatGMT('YYYY-MM-dd') + 'T' + retentionTime.formatGMT('HH:mm:ss') + 'Z');
			}

			// Add WHERE clause (if applicable)
			if (!whereClauses.isEmpty()) {
				queryString += ' WHERE ' + String.join(whereClauses, ' AND ');
			}
			return Database.getQueryLocator(queryString);
		} catch (Exception e) {
			PurgerUtils.log(e);
		}
		// If we reach this then we ran into an error during the Dynamic Query generation therefore don't delete anything
		// NOTE: log is created in above catch statement
		return Database.getQueryLocator('');
	}

	public void execute(Database.BatchableContext bc, List<sObject> recordsToDelete) {
		try {
			batchNumber++;
			String deleteErrors = '';
			totalRecordsToBeDelete += recordsToDelete.size();
			List<Database.DeleteResult> drs = Database.delete(recordsToDelete, false);
			for (Database.DeleteResult dr : drs) {
				if (!dr.isSuccess()) {
					deleteErrors += 'Failed to delete ' + dr.getId() + ': ' + dr.getErrors() + '\n';
				}
			}
			if (String.isNotBlank(deleteErrors)) {
				PurgerUtils.log('Error(s) in Batch ' + batchNumber + '@' + NOW + ' UTC'
						+ '\n' + deleteErrors);
			}
		} catch (Exception e) {
			PurgerUtils.log(e);
		}
	}

	public void finish(Database.BatchableContext bc) {
		try {
			/*if (batchNumber != 0) {
				PurgerUtils.log('Summary of Purger on ' + purgerSetting.MasterLabel + ' Object\nBatches: ' + batchNumber
						+ '\nTotal Records attempted to be deleted: ' + totalRecordsToBeDelete
						+ '\nStart Time: ' + NOW + ' UTC'
						+ '\nNext Expected Run Time: ' + NOW.addMinutes(Integer.valueOf(purgerSetting.Frequency__c)) + ' UTC');
			}*/

			Metadata.CustomMetadata purgerSettingMetadata =
					(Metadata.CustomMetadata) Metadata.Operations.retrieve(Metadata.MetadataType.CustomMetadata,
							new List<String>{
									'Purger_Setting__mdt.' + purgerSetting.DeveloperName
							})[0];

			for (Metadata.CustomMetadataValue metadataValue : purgerSettingMetadata.values) {
				if (metadataValue.field == 'Last_Run__c') {
					metadataValue.value = NOW;
				}
			}
			Metadata.DeployContainer container = new Metadata.DeployContainer();
			container.addMetadata(purgerSettingMetadata);
			Metadata.Operations.enqueueDeployment(container, null);
		} catch (Exception e) {
			PurgerUtils.log(e);
		}
	}
}