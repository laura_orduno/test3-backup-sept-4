/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 * Name - ExternalAuditLogHelper_Test
 * 1. Modified By 
 */
@isTest(SeeAllData=false)
public class ExternalAuditLogHelper_Test{
    
    public static List<Product2> lstPrd = new List<Product2>();
    public static OCOM_Post_Catalogue_Load_Execution_Steps__c createConfigObject(String jobName, Boolean executeJob){
        OCOM_Post_Catalogue_Load_Execution_Steps__c obj = new OCOM_Post_Catalogue_Load_Execution_Steps__c (Name = jobName, Execute__c = executeJob, batch_size__c = 200);
        return obj;
    }
    
    public static void createConfigObject(Boolean executeJob){
        List<OCOM_Post_Catalogue_Load_Execution_Steps__c> lstInsert = new List<OCOM_Post_Catalogue_Load_Execution_Steps__c>();
        OCOM_Post_Catalogue_Load_Execution_Steps__c obj1 = createConfigObject('FixProductAttribJSONBatchJob',executeJob);
        lstInsert.add(obj1);
        OCOM_Post_Catalogue_Load_Execution_Steps__c obj2 = createConfigObject('MaskNonReservableTelNumbeBatchJob',executeJob);
        lstInsert.add(obj2);
        OCOM_Post_Catalogue_Load_Execution_Steps__c obj3 = createConfigObject('UMResolveProductHierarchyBatchJob',executeJob);
        lstInsert.add(obj3);
        OCOM_Post_Catalogue_Load_Execution_Steps__c obj4 = createConfigObject('UpdateProductBatchJob',executeJob);
        lstInsert.add(obj4);
        OCOM_Post_Catalogue_Load_Execution_Steps__c obj5 = createConfigObject('UpdateProductChildItemBatchJob',executeJob);
        lstInsert.add(obj5);
        OCOM_Post_Catalogue_Load_Execution_Steps__c obj6 = createConfigObject('OCOMPriceBookUpdatePreETLLoad', executeJob);
        lstInsert.add(obj6);
        OCOM_Post_Catalogue_Load_Execution_Steps__c obj7 = createConfigObject('OCOMRootCatalogRelationshipJob', executeJob);
        lstInsert.add(obj7);
        OCOM_Post_Catalogue_Load_Execution_Steps__c obj8 = createConfigObject('OCOMCatalogueDeleteJob', executeJob);
        lstInsert.add(obj8);
        OCOM_Post_Catalogue_Load_Execution_Steps__c obj9 = createConfigObject('OCOMCatalogueLoadJob', executeJob);
        lstInsert.add(obj9);
        OCOM_Post_Catalogue_Load_Execution_Steps__c obj10 = createConfigObject('TelcoAdminConsoleController', executeJob);
        lstInsert.add(obj10);
        OCOM_Post_Catalogue_Load_Execution_Steps__c obj11 = createConfigObject('OCOM_productAddlCfgDataUpdBatch', executeJob);
        lstInsert.add(obj11);
        OCOM_Post_Catalogue_Load_Execution_Steps__c obj12 = createConfigObject('ProductTranslationBatchJob', executeJob);
        lstInsert.add(obj12);
        Database.insert(lstInsert, false);
    }
    
    private static testMethod void testExternalAuditLogHelper1(){
        createConfigObject(false);
        Id recId = Schema.SObjectType.External_ETL_Audit_Log__c.getRecordTypeInfosByName().get('Product Catalogue Job').getRecordTypeId();
        test.startTest();
        // Inserting following objects without list as trgger process one record at a time and other would be ignored
        External_ETL_Audit_Log__c obj0 = new External_ETL_Audit_Log__c (Job_Name__c = 'OCOMCatalogueDeleteJob', RecordTypeId = recId, Result__c = 'Success');
        insert(obj0);
        External_ETL_Audit_Log__c obj1 = new External_ETL_Audit_Log__c (Job_Name__c = 'OCOMCatalogueLoadJob', RecordTypeId = recId, Result__c = 'Success');
        insert(obj1);
        External_ETL_Audit_Log__c obj2 = new External_ETL_Audit_Log__c (Job_Name__c = 'UpdateProductChildItemBatchJob', RecordTypeId = recId, Result__c = 'Success');
        insert(obj2);
        External_ETL_Audit_Log__c obj3 = new External_ETL_Audit_Log__c (Job_Name__c = 'MaskNonReservableTelNumbeBatchJob', RecordTypeId = recId, Result__c = 'Success');
        insert(obj3);
        External_ETL_Audit_Log__c obj4 = new External_ETL_Audit_Log__c (Job_Name__c = 'UpdateProductBatchJob', RecordTypeId = recId, Result__c = 'Success');
        insert(obj4);
        External_ETL_Audit_Log__c obj5 = new External_ETL_Audit_Log__c (Job_Name__c = 'UpdateProductChildItemBatchJob', RecordTypeId = recId, Result__c = 'Success');
        insert(obj5);
        External_ETL_Audit_Log__c obj6 = new External_ETL_Audit_Log__c (Job_Name__c = 'OCOMRootCatalogRelationshipJob', RecordTypeId = recId, Result__c = 'Success');
        insert(obj6);
        External_ETL_Audit_Log__c obj7 = new External_ETL_Audit_Log__c (Job_Name__c = 'FixProductAttribJSONBatchJob', RecordTypeId = recId, Result__c = 'Success');
        insert(obj7);
        test.stopTest();
    }
    
    private static testMethod void testExternalAuditLogHelper2(){
        createProduct();
        createConfigObject(true);
        createAttributeAssignment();
        PriceBook2 pbObj = new PriceBook2(Name = 'OCOM Pricebook');
        insert pbObj;
        Id recId = Schema.SObjectType.External_ETL_Audit_Log__c.getRecordTypeInfosByName().get('Product Catalogue Job').getRecordTypeId();
        test.startTest();
        
        // Inserting following objects without list as trgger process one job at a time and other would be ignored
        External_ETL_Audit_Log__c obj0 = new External_ETL_Audit_Log__c (Job_Name__c = 'OCOMCatalogueDeleteJob', RecordTypeId = recId, Result__c = 'Success');
        insert(obj0);
        External_ETL_Audit_Log__c obj1 = new External_ETL_Audit_Log__c (Job_Name__c = 'OCOMCatalogueLoadJob', RecordTypeId = recId, Result__c = 'Success');
        insert(obj1);
        External_ETL_Audit_Log__c obj2 = new External_ETL_Audit_Log__c (Job_Name__c = 'UpdateProductChildItemBatchJob', RecordTypeId = recId, Result__c = 'Success');
        insert(obj2);
        External_ETL_Audit_Log__c obj3 = new External_ETL_Audit_Log__c (Job_Name__c = 'MaskNonReservableTelNumbeBatchJob', RecordTypeId = recId, Result__c = 'Success');
        insert(obj3);
        External_ETL_Audit_Log__c obj4 = new External_ETL_Audit_Log__c (Job_Name__c = 'UpdateProductBatchJob', RecordTypeId = recId, Result__c = 'Success');
        insert(obj4);
        External_ETL_Audit_Log__c obj5 = new External_ETL_Audit_Log__c (Job_Name__c = 'UpdateProductChildItemBatchJob', RecordTypeId = recId, Result__c = 'Success');
        insert(obj5);
        External_ETL_Audit_Log__c obj6 = new External_ETL_Audit_Log__c (Job_Name__c = 'OCOMRootCatalogRelationshipJob', RecordTypeId = recId, Result__c = 'Success');
        insert(obj6);
        External_ETL_Audit_Log__c obj7 = new External_ETL_Audit_Log__c (Job_Name__c = 'FixProductAttribJSONBatchJob', RecordTypeId = recId, Result__c = 'Success');
        insert(obj7);
        External_ETL_Audit_Log__c obj8 = new External_ETL_Audit_Log__c (Job_Name__c = 'FixProductAttribJSONBatchJob', RecordTypeId = recId, Result__c = 'Success');
        insert(obj8);
        test.stopTest();
    }
    private static testMethod void testBatchApexConstructor(){
        test.startTest();
        
        String query = 'SELECT Id, vlocity_cmt__AttributeName__c, orderMgmtId__c, Reservable__c, vlocity_cmt__FormatMask__c ' + 
                ' FROM vlocity_cmt__AttributeAssignment__c ' +
                ' WHERE Reservable__c <> \'Yes\' ' +
                ' AND vlocity_cmt__AttributeName__c LIKE \'%Phone%\'';
        MaskNonReservableTelNumbeBatchJob jobObject1 = new MaskNonReservableTelNumbeBatchJob(query, false);
        MaskNonReservableTelNumbeBatchJob jobObject2 = new MaskNonReservableTelNumbeBatchJob(query);
        MaskNonReservableTelNumbeBatchJob jobObject3 = new MaskNonReservableTelNumbeBatchJob(false);
        MaskNonReservableTelNumbeBatchJob jobObject4 = new MaskNonReservableTelNumbeBatchJob();
        
        String query1 = 'select id, name, IncludeQuoteContract__c, OCOM_Bookable__c FROM Product2 WHERE name Like \'%White Glove Installation%\'';
        UpdateProductBatchJob jobObject5 = new UpdateProductBatchJob(query1, true);
        UpdateProductBatchJob jobObject6 = new UpdateProductBatchJob(query);
        UpdateProductBatchJob jobObject7 = new UpdateProductBatchJob(false);
        UpdateProductBatchJob jobObject8 = new UpdateProductBatchJob();
        
        String query3 = 'select vlocity_cmt__Quantity__c, vlocity_cmt__MinQuantity__c FROM vlocity_cmt__ProductChildItem__c ' +
                'WHERE vlocity_cmt__ChildProductId__r.Catalog__r.Name LIKE \'Essentials%\'';
        UpdateProductChildItemBatchJob jobObject9 = new UpdateProductChildItemBatchJob(query3,true);
        UpdateProductChildItemBatchJob jobObject10 = new UpdateProductChildItemBatchJob(query3);
        UpdateProductChildItemBatchJob jobObject11 = new UpdateProductChildItemBatchJob(false);
        UpdateProductChildItemBatchJob jobObject12 = new UpdateProductChildItemBatchJob();
         
        test.stopTest();
    }
    public static void createAttributeAssignment(){
        vlocity_cmt__AttributeCategory__c catObj = new vlocity_cmt__AttributeCategory__c();
        catObj.Name = 'testcattt';
        catObj.vlocity_cmt__Code__c = 'testcodexxyyyz';
        catObj.vlocity_cmt__DisplaySequence__c = 1;
        insert catObj;
        
        List<vlocity_cmt__Attribute__c> attList = new List<vlocity_cmt__Attribute__c>();
        vlocity_cmt__Attribute__c attObj = new vlocity_cmt__Attribute__c();
        attObj.Name = 'Phone_xx_xx';
        attObj.OCOM_Reservable__c = 'No';
        attObj.vlocity_cmt__AttributeCategoryId__c = catObj.Id;
        attList.add(attObj);
        
        vlocity_cmt__Attribute__c attObj1 = new vlocity_cmt__Attribute__c();
        attObj1.Name = 'TOLL-FREE TERMINATING NUMBER';
        attObj1.OCOM_Reservable__c = 'No';
        attObj1.vlocity_cmt__AttributeCategoryId__c = catObj.Id;
        attList.add(attObj1);
        
        vlocity_cmt__Attribute__c attObj2 = new vlocity_cmt__Attribute__c();
        attObj2.Name = 'Name to be Displayed';
        attObj2.OCOM_Reservable__c = 'No';
        attObj2.vlocity_cmt__AttributeCategoryId__c = catObj.Id;
        attList.add(attObj2);
        
        vlocity_cmt__Attribute__c attObj3 = new vlocity_cmt__Attribute__c();
        attObj3.Name = 'City Of Use';
        attObj3.OCOM_Reservable__c = 'No';
        attObj3.vlocity_cmt__AttributeCategoryId__c = catObj.Id;
        attList.add(attObj3);
        
        vlocity_cmt__Attribute__c attObj4 = new vlocity_cmt__Attribute__c();
        attObj4.Name = 'Offering Id';
        attObj4.OCOM_Reservable__c = 'No';
        attObj4.vlocity_cmt__AttributeCategoryId__c = catObj.Id;
        attList.add(attObj4);
        
        Database.Insert(attList, false);
        
        List<vlocity_cmt__AttributeAssignment__c> attasnList = new List<vlocity_cmt__AttributeAssignment__c>();
        vlocity_cmt__AttributeAssignment__c aaObj = new vlocity_cmt__AttributeAssignment__c();
        aaObj.Name= 'Xyz_pp_yyz';
        aaObj.vlocity_cmt__AttributeId__c = attList[0].Id;
        aaObj.vlocity_cmt__AttributeCategoryId__c = catObj.Id;
        aaObj.vlocity_cmt__FormatMask__c = '8890001234';
        attasnList.add(aaObj);
        
        vlocity_cmt__AttributeAssignment__c aaObj1 = new vlocity_cmt__AttributeAssignment__c();
        aaObj1.Name= 'toll free';
        aaObj1.vlocity_cmt__AttributeId__c = attList[1].Id;
        aaObj1.vlocity_cmt__AttributeCategoryId__c = catObj.Id;
        aaObj1.vlocity_cmt__FormatMask__c = '8890001234';
        attasnList.add(aaObj1);
        
        vlocity_cmt__AttributeAssignment__c aaObj2 = new vlocity_cmt__AttributeAssignment__c();
        aaObj2.Name= 'Name to be Displayed';
        aaObj2.vlocity_cmt__AttributeId__c = attList[2].Id;
        aaObj2.vlocity_cmt__AttributeCategoryId__c = catObj.Id;
        aaObj2.vlocity_cmt__FormatMask__c = '8890001234';
        attasnList.add(aaObj2);
        
        vlocity_cmt__AttributeAssignment__c aaObj3 = new vlocity_cmt__AttributeAssignment__c();
        aaObj3.Name= 'city of use';
        aaObj3.vlocity_cmt__AttributeId__c = attList[3].Id;
        aaObj3.vlocity_cmt__AttributeCategoryId__c = catObj.Id;
        aaObj3.vlocity_cmt__FormatMask__c = '8890001234';
        attasnList.add(aaObj3);
        
        vlocity_cmt__AttributeAssignment__c aaObj4 = new vlocity_cmt__AttributeAssignment__c();
        aaObj4.Name= 'Offering Id';
        aaObj4.vlocity_cmt__AttributeId__c = attList[4].Id;
        aaObj4.vlocity_cmt__AttributeCategoryId__c = catObj.Id;
        aaObj4.vlocity_cmt__FormatMask__c = '8890001234';
        aaObj4.vlocity_cmt__ObjectId__c = lstPrd[0].Id;
        attasnList.add(aaObj4);
        
        insert attasnList;
    }
    public static void createProduct(){
        vlocity_cmt__Catalog__c catObj = new vlocity_cmt__Catalog__c();
        catObj.Name = 'Essentials_testtt';
        insert catObj;
        
        Product2 prdObj = new Product2();
        prdObj.Name = 'Test_White Glove Installation_TEst';
        prdObj.orderMgmtId__c = 'testexternalKey_prd_0001';
        lstPrd.add(prdObj);
        
        Product2 prdObj1 = new Product2();
        prdObj1.Name = 'Essentials_testtt';
        prdObj1.Catalog__c = catObj.Id;
        prdObj1.orderMgmtId__c = 'testexternalKey_prd_0002';
        lstPrd.add(prdObj1);
        
        Product2 prdObj2 = new Product2();
        prdObj2.Name = 'Display Name';
        prdObj2.Catalog__c = catObj.Id;
        prdObj2.orderMgmtId__c = 'testexternalKey_prd_0003';
        lstPrd.add(prdObj2);
                
        Product2 prdObj3 = new Product2();
        prdObj3.Name = 'Office Phone & Internet Bundle - 150Mbps';
        prdObj3.orderMgmtId__c = 'testexternalKey_prd_0004';
        lstPrd.add(prdObj3);
        
        insert lstPrd;
        
        vlocity_cmt__ProductChildItem__c prdChildObj = new vlocity_cmt__ProductChildItem__c();
        prdChildObj.Name = 'testch';
        prdChildObj.vlocity_cmt__ChildProductId__c = lstPrd[1].Id;
        prdChildObj.vlocity_cmt__ChildLineNumber__c = '1';
        insert prdChildObj;
        
        List<VL_Product_NLS__c> nlsList = new List<VL_Product_NLS__c>();
        VL_Product_NLS__c nlsObj1 = new VL_Product_NLS__c();
        nlsObj1.NLS_Field_Name__c = 'Name';
        nlsObj1.NLS_Field_Value__c = 'testfr';
        nlsObj1.Linked_Product__c = lstPrd[1].Id;
        nlsObj1.Product_External_Id__c = lstPrd[1].orderMgmtId__c;
        nlsObj1.Language_Id__c = 'testlangId';
        nlsList.add(nlsObj1);
        
        VL_Product_NLS__c nlsObj2 = new VL_Product_NLS__c();
        nlsObj2.NLS_Field_Name__c = 'Description';
        nlsObj2.NLS_Field_Value__c = 'testfr';
        nlsObj2.Linked_Product__c = lstPrd[1].Id;
        nlsObj2.Product_External_Id__c = lstPrd[1].orderMgmtId__c;
        nlsObj2.Language_Id__c = 'testlangId';
        nlsList.add(nlsObj2);
        
        VL_Product_NLS__c nlsObj3 = new VL_Product_NLS__c();
        nlsObj3.NLS_Field_Name__c = 'DetailedDescription__c';
        nlsObj3.NLS_Field_Value__c = 'testfr';
        nlsObj3.Linked_Product__c = lstPrd[1].Id;
        nlsObj3.Product_External_Id__c = lstPrd[1].orderMgmtId__c;
        nlsObj3.Language_Id__c = 'testlangId';
        nlsList.add(nlsObj3);
        
        insert nlsList;
    }
}