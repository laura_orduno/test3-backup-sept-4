public class vlocity_addon_AutoCreateContractDocCntrl {
    
    public id versionId {get;set;}
    string updateParam = '';
    public id contractID{get;set;}
    public static vlocity_cmt__ContractVersion__c activeContVersion {get;set;}
    public list<vlocity_cmt__ContractVersion__c> conVersion = new list<vlocity_cmt__ContractVersion__c>();
    public boolean isUpdate { get;set;}
    public map<string,string> mapSourceDestinationFieldsMap;
    
    public boolean isInConsole {
        get{
            Map<String, String> parametersMap = ApexPages.currentPage().getParameters();   
            for(String key : parametersMap.keySet()){
                if(key.trim().equalsIgnoreCase('isInConsole')){
                    String value=parametersMap.get(key);
                    if(value.trim().equals('true')){
                        return true;
                    }
                    else if(value.trim().equals('false')){
                        return false;
                    }
                    
                }
            }
            
            return false;
        }
        set;
    }
    
    public vlocity_addon_AutoCreateContractDocCntrl(vlocity_cmt.ContractDocumentCreationController con){ 
        contractID = ApexPages.currentPage().getParameters().get('id');
        updateParam = ApexPages.currentPage().getParameters().get('isUpdate');
        if(updateParam != null && updateParam.trim().equalsIgnoreCase('true'))
            isUpdate = true;
        else 
            isUpdate = false;
        
        system.debug('isUpdate'+ isUpdate);
        
    }
    
    public void attachDoc(){
        //Create new Version if the request is update
        system.debug('isInConsole::'+isInConsole);
        try{
            if(isUpdate == true && contractID != null){
                
                updateObjMapping();
                versionId = vlocity_cmt.ContractServiceResource.createNewContractVersionDocument(contractID);
                List<vlocity_cmt__ContractSection__c> oldList = new List<vlocity_cmt__ContractSection__c>();
                List<vlocity_cmt__ContractVersion__c> oldTemplate = new List<vlocity_cmt__ContractVersion__c>();
                for (vlocity_cmt__ContractVersion__c conVersions: [Select Id,vlocity_cmt__ContractId__r.Contract_Type__c, vlocity_cmt__DocumentTemplateId__c,
                                                                   (select id from vlocity_cmt__Contract_Sections__r) 
                                                                   from vlocity_cmt__ContractVersion__c 
                                                                   where Id = :versionId limit 1]){
                                                                       oldList.addAll(conVersions.vlocity_cmt__Contract_Sections__r);
                                                                       oldTemplate.add(conVersions);                                           
                                                                   }

                if( oldTemplate.size() >0 &&  !oldTemplate.isEmpty()){
                    oldTemplate[0].vlocity_cmt__DocumentTemplateId__c=null;
                    delete oldList;
                    update oldTemplate[0];
                    activeContVersion = oldTemplate[0];
                }
                System.debug('New Version ID:' + versionId);
            }else {
                
                // Attach a Template to the esixting Version and then attach a Doc.
                conVersion = [Select ID,vlocity_cmt__ContractId__r.Contract_Type__c from vlocity_cmt__ContractVersion__c where vlocity_cmt__ContractId__c = :contractID 
                              and vlocity_cmt__Status__c = 'Active' ]; 
                if(conVersion.size()>0 && !conVersion.isEmpty() ) {
                    activeContVersion = conVersion[0];
                    versionId = conVersion[0].id;
                    System.debug('Old Version ID:' + versionId);
                }
            }
            if(activeContVersion != null && activeContVersion.vlocity_cmt__ContractId__r.Contract_Type__c != null   ){
                String documentTemplateName = '';
                String documentTemplateCode = activeContVersion.vlocity_cmt__ContractId__r.Contract_Type__c;
                // 2018-03 - Mark H - Check for ConE template
                if ((activeContVersion.vlocity_cmt__ContractId__r.Contract_Type__c == 'New') ||
                    (activeContVersion.vlocity_cmt__ContractId__r.Contract_Type__c == 'Amendment')) {
                    // check Record Type of Attribute record
                    list<Service_Request__c> serviceRequests = [Select ID, Contract_Attribute__r.RecordTypeId, SRS_PODS_Product__r.Contractible_Product__r.Product_Type__c from Service_Request__c where Contract__r.New_eContract__c = :contractID];
                    if (serviceRequests.size() <> 0 && serviceRequests[0].Contract_Attribute__r.RecordTypeId != null) {
                        list<RecordType> caRecordTypes = [Select DeveloperName from RecordType where Id = :serviceRequests[0].Contract_Attribute__r.RecordTypeId];
                        if (caRecordTypes.size() <> 0 && caRecordTypes[0].DeveloperName == 'eContract' ) {
                            documentTemplateCode = documentTemplateCode  + '_' + serviceRequests[0].SRS_PODS_Product__r.Contractible_Product__r.Product_Type__c;
                        }
                    }
                }
                // get Template name based on Contract Type
                documentTemplateName = getDocumentTemplateName (documentTemplateCode );
                if(documentTemplateName != null){
                    Map<String, String> objNameMap = new Map<String, String> ();
                    Id documentTemplateId = getTemplateId(documentTemplateName, 'Contract');
                    // Attach template to the Contract Version
                    vlocity_cmt.ContractDocumentDisplayController.createContractSections(documentTemplateId, activeContVersion.id); 
                }                    
            }              
        }catch(exception e){
            System.debug(LoggingLevel.ERROR, 'Exception is '+e);
            System.debug(LoggingLevel.ERROR, 'Exception stack trace '+e.getStackTraceString());
        }
    }
    
    
    
    // Get Template name from the custom settings
    private String getDocumentTemplateName (String ContractType){
        
        Set_Doc_Template__c docTemplate =  Set_Doc_Template__c.getInstance(ContractType);
        
        if(docTemplate!=null){
            String documentTemplateName = docTemplate.DocumentTemplateName__c; 
            return documentTemplateName;
        }
        return null;
    }
    
    //get active template ID for the contract 
    private Id getTemplateId(String documentTemplateName, String objTypeName){
        
        List<vlocity_cmt__DocumentTemplate__c> templateIds = [Select Id from vlocity_cmt__DocumentTemplate__c 
                                                              where Name=:documentTemplateName And vlocity_cmt__IsActive__c=true 
                                                              AND vlocity_cmt__ApplicableTypes__c INCLUDES (:objTypeName)];
        
        if(templateIds !=null && templateIds.size()>0){
            return templateIds[0].Id;
        }
        else{
            String message = Label.vlocity_cmt.PDF_NoTemplateId;
            throw new NoTemplateIdException(message);
        }
        return null;    
    }
    
    public void updateObjMapping() {
        List<Contract> contractNewList = new List<Contract> ();
        List<Contract> updateContractNewList = new List<Contract> ();
        set<string> setContractFields=new set<string>{'Id', 'Name', 'Account.Name', 'Complex_Fulfillment_Contract__c'};      
            mapSourceDestinationFieldsMap=new map<string,string>();
        
        if(isUpdate == true && contractID != null){
            list<vlocity_cmt__CustomFieldMap__c> lstCustomFieldMap=[select vlocity_cmt__DestinationFieldName__c,vlocity_cmt__DestinationSObjectType__c,vlocity_cmt__SourceFieldName__c,
                                                                    vlocity_cmt__SourceFieldSObjectType__c  ,vlocity_cmt__SourceFieldType__c,vlocity_cmt__SourceSObjectType__c from vlocity_cmt__CustomFieldMap__c 
                                                                    where vlocity_cmt__SourceSObjectType__c='Contracts__c' and vlocity_cmt__DestinationSObjectType__c='Contract'];
            
            for(vlocity_cmt__CustomFieldMap__c objCustomFieldMap : lstCustomFieldMap){
                setContractFields.add(objCustomFieldMap.vlocity_cmt__DestinationFieldName__c);
                setContractFields.add('Complex_Fulfillment_Contract__r.'+objCustomFieldMap.vlocity_cmt__SourceFieldName__c);
                mapSourceDestinationFieldsMap.put('Complex_Fulfillment_Contract__r.'+objCustomFieldMap.vlocity_cmt__SourceFieldName__c,objCustomFieldMap.vlocity_cmt__DestinationFieldName__c);
            }    
        }
        list<string> lstContractFields=new list<string>();
        
        lstContractFields.addall(setContractFields);
        system.debug('::lstContractFields:: ' + lstContractFields);
        
        List<String> lstContractFilter = new List<String>{'ID =: contractID'};
            String sContractQuery = Util.queryBuilder('Contract',lstContractFields, lstContractFilter);
        contractNewList = Database.query(sContractQuery);
        
        for (Contract conVar : contractNewList)
        {
            conVar.Name = conVar.Account.Name + ' - ' + 'AGR'; 
            updateContractNewList.add(conVar);
        }
        if(contractNewList.size() >0 && isUpdate == true && contractID != null ) {
            updateContractInfo(contractNewList[0]);     
        }
        
        update updateContractNewList;
        
    } 
    
    public Sobject updateContractInfo(Sobject objContract){
        system.debug('Update Cusom Mapping::');
        for(string sSourceField : mapSourceDestinationFieldsMap.keyset()){
            if(sSourceField.contains('__r')){
                list<string> objTempName = sSourceField.split('\\.');
                if(objTempName != null && objTempName.size()>0){
                    if(objTempName.size()==4 && objContract.getSobject(objTempName[0])!=null && objContract.getSobject(objTempName[0]).getSobject(objTempName[1])!=null &&  objContract.getSobject(objTempName[0]).getSobject(objTempName[1]).getSobject(objTempName[2])!=null &&  objContract.getSobject(objTempName[0]).getSobject(objTempName[1]).getSobject(objTempName[2]).get(objTempName[3])!=null && objContract.getSobject(objTempName[0]).getSobject(objTempName[1]).getSobject(objTempName[2]).get(objTempName[3])!='')
                    {
                        objContract.put(mapSourceDestinationFieldsMap.get(sSourceField),objContract.getSobject(objTempName[0]).getSobject(objTempName[1]).getSobject(objTempName[2]).get(objTempName[3]));
                    }
                    else if(objTempName.size()==3 && objContract.getSobject(objTempName[0]) != null && objContract.getSobject(objTempName[0]).getSobject(objTempName[1]) != null && objContract.getSobject(objTempName[0]).getSobject(objTempName[1]).get(objTempName[2]) != null && objContract.getSobject(objTempName[0]).getSobject(objTempName[1]).get(objTempName[2]) != '')
                    {
                        objContract.put(mapSourceDestinationFieldsMap.get(sSourceField),objContract.getSobject(objTempName[0]).getSobject(objTempName[1]).get(objTempName[2]));
                    }
                    else if(objTempName.size()==2 && objContract.getSobject(objTempName[0]) != null && objContract.getSobject(objTempName[0]).get(objTempName[1]) != null && objContract.getSobject(objTempName[0]).get(objTempName[1]) != '')
                    {
                        objContract.put(mapSourceDestinationFieldsMap.get(sSourceField),objContract.getSobject(objTempName[0]).get(objTempName[1]));
                    }
                    else
                        objContract.put(mapSourceDestinationFieldsMap.get(sSourceField),null);
                }
            }
        }
        return objContract;
    }
    
    public class NoTemplateIdException extends Exception{}    
}