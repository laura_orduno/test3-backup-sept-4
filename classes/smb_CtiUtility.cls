global class smb_CtiUtility {

	global static Map<string, string> parseUrl(string url) {
    	
    	System.debug('::DD::CC::');
    	System.debug(url);
    	
    	Map<string, string> parameters = new Map<string, string>();
    	
    	String paramsString = getCtiParameterString(url);
    	
    	if (paramsString == null) return parameters;
    	
    	List<string> params = EncodingUtil.urlDecode(paramsString, 'UTF-8').split(';');
    	
    	System.debug('::DD::CC::');
    	System.debug(params);
    	    	
    	for (string param : params) {
    		System.debug(param);
    		if (!param.contains('=')) {
    			parameters.put(param, null);
    		}	
    		else {
    			string[] paramParts = param.split('=', 2);
    			parameters.put(paramParts[0], paramParts[1]);
    		}
    	}
    	
    	System.debug('::DD::CC::');
    	System.debug(parameters);
    	
    	return parameters;
    	
    }
    
    global static string getCtiParameterString(string url) {
    	System.debug(url);
    	
    	string[] urlParts = url.split('\\?', 2);
    	
    	if (urlParts.size() < 2) return null;
    	
    	List<string> allParams = EncodingUtil.urlDecode(urlParts[1], 'UTF-8').split('&');
    	
    	for (string aParam : allParams) {
    		if (aParam.startsWithIgnoreCase('TCCS_UTN')) {
    			return aParam;
    		}
    	}
    	
    	return null;
    }

}