/**
 * This is a WSCallout class ... (used for Wireline real TN as well as Toll Free Number)
 * TOCP is the intermediate integration layer between Salesforce (northbound) and the ASF and SMS systems (southbound) for BSE Telephone Number Reservation (TNR) functionality.
 * Telephone Number Reservation Functionality
 * TOCP TNR interfaces support the following Wireline products: Single Line (SL), Multi Line (ML), Dark High Speed and Toll Free (TF)
 * 
 * For SL, ML and Dark High Speed, the TOCP Contract is mapped to the following ASF API
 * ReserveResource 
 * - Reserve real wireline telephone number(s) in FMS
 * - Reserve Dark DSL in FMS
 * 
 * For Toll Free (TF), the TOCP Contract is mapped to the following SMS API
 * ReserveResource 
 * - Reserve toll free number in SMS
 * 
 * @author Santosh Rath
 */
public class OrdrTnReserveResourceWsCallout {


 public static TpFulfillmentResourceOrderV3.ResourceOrder reserveResource(TpCommonMtosiV3.Header_T header,TpFulfillmentResourceOrderV3.ResourceOrder resourceOrder,Boolean isTollFree) {
        TpFulfillmentResourceOrderV3.ResourceOrder respResourceOrder;
                 
        try {
			System.debug('The name of the TNR WebService called is reserveResource');
                 OrdrTnResrResInvOprtn.ReserveResourceSOAPBinding binding = new OrdrTnResrResInvOprtn.ReserveResourceSOAPBinding();
                binding = prepareCallout(binding,isTollFree); 
                if(!OrdrUtilities.useAuditFlag){
                	respResourceOrder = binding.reserveResource(resourceOrder);
                }
            
        }catch (CalloutException ce) {
            System.debug(ce.getStackTraceString());
		    System.debug('CalloutException occurred while invoking binding.reserveResource' +ce);
            System.debug(ce.getmessage());
            String msg=ce.getmessage();
            if(String.isNotBlank(msg) && (msg.indexOf(OrdrConstants.SDF_INTERNAL_SERVER_ERR)!=-1)){
                msg=msg+OrdrConstants.SDF_SUPPORT_MSG;
            }
			OrdrUtilities.auditLogs('OrdrTnReserveResourceWsCallout.reserveResource','TOCP','reserveResource',null,null,true,false,msg,ce.getStackTraceString());
            OrdrServicesException ose=new OrdrServicesException(ce.getmessage(),ce);
            throw ose;
        } catch (Exception e) {
            System.debug(e.getStackTraceString());
		    System.debug('Exception occurred while invoking binding.reserveResource' +e);
            System.debug(e.getmessage());
             String msg=e.getmessage();
            if(String.isNotBlank(msg) && (msg.indexOf(OrdrConstants.SDF_INTERNAL_SERVER_ERR)!=-1)){
                msg=msg+OrdrConstants.SDF_SUPPORT_MSG;
            }
			OrdrUtilities.auditLogs('OrdrTnReserveResourceWsCallout.reserveResource','TOCP','reserveResource',null,null,false,true,msg,e.getStackTraceString());
            OrdrServicesException ose=new OrdrServicesException(e.getmessage(),e);
            throw ose;
        }
        return respResourceOrder;
    }
    private static OrdrTnResrResInvOprtn.ReserveResourceSOAPBinding prepareCallout(OrdrTnResrResInvOprtn.ReserveResourceSOAPBinding binding,Boolean isTollFree) {
        Ordering_WS__c reserveResourceWirelineEndpoint = Ordering_WS__c.getValues('ReserveResourceWirelineEndpoint');  //ReserveResourceTollfreeEndpoint
		if(isTollFree){
			reserveResourceWirelineEndpoint = Ordering_WS__c.getValues('ReserveResourceTollfreeEndpoint');
		}
          
        binding.endpoint_x = reserveResourceWirelineEndpoint.value__c;
        System.debug('ReserveResourceWirelineEndpoint: '+ reserveResourceWirelineEndpoint.value__c);
      
       // servicePort.clientCertName_x = OrdrConstants.SDF_CERT_NAME;          
        // Set SFDC Webservice call timeout
        //binding.timeout_x = 30000;
        binding.timeout_x = OrdrConstants.WS_TIMEOUT_MS;
		if (binding.endpoint_x.startsWith(OrdrConstants.ENDPOINT_XML_GATEWAY_SYMBOL)) {
                Ordering_WS__c wsUserName =  Ordering_WS__c.getValues(OrdrConstants.CREDENTIAL_USERNAME);
                Ordering_WS__c wsPassword =  Ordering_WS__c.getValues(OrdrConstants.CREDENTIAL_PASSWORD);  
                if (String.isNotBlank(wsUserName.value__c) && String.isNotBlank(wsPassword.value__c)) {
						String credentials = 'APP_CPQ' + ':' + wsPAssword.value__c;
						String encodedUserNameAndPassword = EncodingUtil.base64Encode(Blob.valueOf(credentials));
						binding.inputHttpHeaders_x = new Map<String, String>();
						binding.inputHttpHeaders_x.put(OrdrConstants.AUTHORIZATION, OrdrConstants.BASIC + encodedUserNameAndPassword);
					}   
                }
          else {
                binding.clientCertName_x = OrdrConstants.SDF_CERT_NAME;
            }
        return binding;
    } 


 /**
	 * Used for Real TN and Toll Free number reservation using the TOCP ReserveResource Business Contract
	 * For SL, ML and Dark High Speed, the TOCP Contract is mapped to the following ASF API
	 * ReserveResource 
	 * - Reserve real wireline telephone number(s) in FMS
	 * - Reserve Dark DSL in FMS
	 * 
	 * For Toll Free (TF), the TOCP Contract is mapped to the following SMS API
	 * ReserveResource 
	 * - Reserve toll free number in SMS
	 * 
	 * @param  
	 * @return 
	 * 
	 */		
	public static List<OrdrTnLineNumber> reserveResourceForTnList(OrdrTnServiceAddress serviceAddress, List<OrdrTnLineNumber> tnList, String userId, String referenceNumber, OrdrTnReservationType reservationType,  OrdrTnAssignedProductType assignedProductType){
		
		//ReserveResourcePT binding = null;
		String asfBindingError = null;
		String asfErrorMessage = null;
		
		System.debug('-- ReserveResourceImpl.reserveResourceForTnList - processing starts at '+System.now());

		TpCommonMtosiV3.Header_T header=new TpCommonMtosiV3.Header_T();
		
		TpFulfillmentResourceOrderV3.ResourceOrder  resourceOrder = null;

		if (reservationType != null) {
			if (String.valueOf(reservationType).equals(String.valueOf(OrdrTnReservationType.WLN))) {
				//Preparing the WS call to TOCP to reach ASF for Wireline TN reservation
				//The process to auto assign and reserve a TN differs depending on the product type:
				// - for Voice SL & ML: System needs to invoke in sequence (a) get OrdrTnNpaNxxlist, (b) request a list of spare TNs, (c) reserve one TN from that list, (d) release others
				//   ==> thus in this case, the List<OrdrTnLineNumber> tnList should NOT be null
				// - for Dark ADSL: System only needs to invoke (c) reserve TN (intentionally omitting to provide a TN as an input, as ASF will take care of assigning & reserving one that it will return back to Salesforce
				//   ==> thus in this case, the List<OrdrTnLineNumber> tnList should be null
				if ((assignedProductType != null) && (assignedProductType.equals(String.valueOf(OrdrTnAssignedProductType.ADSL)))) {
					resourceOrder = generateTocpResourceOrderForDarkDslSoapRequest(serviceAddress, tnList, userId, referenceNumber);
				}
				else {
					resourceOrder = generateTocpResourceOrderForRealTnSoapRequest(serviceAddress, tnList, userId, referenceNumber);
				}
			}
			else if (String.valueOf(reservationType).equals(String.valueOf(OrdrTnReservationType.TOLLFREE))) {
				//Preparing the WS call to TOCP to reach SMS for Toll Free Number reservation
				resourceOrder = generateTocpResourceOrderForTollFreeSoapRequest(serviceAddress, tnList, userId, referenceNumber);
			}

			if (resourceOrder != null) {
					// Invoke the TOCP - ReserveResource web service
					try {
						if (String.valueOf(reservationType).equals(String.valueOf(OrdrTnReservationType.TOLLFREE))) {
							System.debug( 'Invoking SMS reserveTelephoneNumber through TOCP ReserveResource');
						}
						else {
							if ((assignedProductType != null) && (String.valueOf(assignedProductType).equals(String.valueOf(OrdrTnAssignedProductType.ADSL)))) {
								System.debug( 'Invoking ASF InventoryService.getServiceID through TOCP ReserveResource');
							}
							else {
								System.debug( 'Invoking ASF InventoryService.reserveTelephoneNumber through TOCP ReserveResource');
							}
						}
						resourceOrder=reserveResource(header, resourceOrder,String.valueOf(reservationType).equals(String.valueOf(OrdrTnReservationType.TOLLFREE)));
					} 
    				catch (Exception e) {
    					System.debug(e.getStackTraceString());
                        asfErrorMessage=e.getMessage();
                    	throw e;
    				}

				// Process the TOCP response
				if (asfErrorMessage == null) {
					tnList = processTocpResponseForReservingTnList(tnList, reservationType, resourceOrder);
				}
				else {
					if ((tnList != null) && (tnList.size()>0)) {
						for (Integer k=0; k<tnList.size(); k++) {
							// if the TN reservation failed - error message returned or exception/fault message thrown - tag this TN as still Available
							tnList.get(k).setStatus(OrdrTnLineNumberStatus.AVAILABLE);
							tnList.get(k).setComments(asfErrorMessage);
							System.debug('Error caught in ReserveResourceImpl.reserveResourceForTnList for '+tnList.get(k).getTenDigitsTn()+' : '+asfErrorMessage);
						}
					}
					else {
						// No TN was provided as an input for Dark ADSL
						// Generating and returning a new tnList based on the TN provided by the ASF GetService method (from Inventory Service) to TOCP
						//tnList = new List<OrdrTnLineNumber>(Arrays.asList(new OrdrTnLineNumber('', 'COID', OrdrConstants.SWITCH_CODE, false, reservationType, OrdrTnLineNumberStatus.AVAILABLE, null) ));
						tnList = new List<OrdrTnLineNumber>();
						tnList.add(new OrdrTnLineNumber('', 'COID', OrdrConstants.SWITCH_CODE, false, reservationType, OrdrTnLineNumberStatus.AVAILABLE, null) );
					}
				}
			}
		}
		if (tnList == null) {
			tnList = new List<OrdrTnLineNumber>();
		}
		return tnList;
	}	


	private static List<OrdrTnLineNumber> processTocpResponseForReservingTnList(List<OrdrTnLineNumber> tnList, OrdrTnReservationType reservationType, TpFulfillmentResourceOrderV3.ResourceOrder resourceOrder) {
		// Process the TOCP response
		if (resourceOrder != null) {
			List<TpFulfillmentResourceOrderV3.ResourceOrderItem> listRoi = resourceOrder.ResourceOrderItem;
			if ((listRoi != null) && (listRoi.size() > 0)) {
				for (Integer i=0; i<listRoi.size(); i++) {
					if (listRoi.get(i) != null) {
						TpCommonBusinessInteractionV3.BusinessInteractionEntity res = listRoi.get(i).Resource;
						if ((res != null) && (res.CharacteristicValue != null)) {
							Map <String, String> hm = convertTocpV2CharacteristicValuesToMap(res.CharacteristicValue);
							String npa = null;
							String nxx = null;
							String line = null;
							String serviceId = null;
							String telephoneNumberStatus = null;
							String errorMessage = null;
							String tollFreeNumber = null;
							String lineNumberTenDigits = null;

							if (hm.containsKey('npa')) {
								npa = hm.get('npa');
							}	
							if (hm.containsKey('nxx')) {
								nxx = hm.get('nxx');
							}	
							if (hm.containsKey('line')) {
								line = hm.get('line');
							}
							if (hm.containsKey('telephoneNumberStatus')) {
								telephoneNumberStatus = hm.get('telephoneNumberStatus');
							}
							if (hm.containsKey('serviceId')) {
								serviceId = hm.get('serviceId');
							}
							if (hm.containsKey('tollFreeNumber')) {
								tollFreeNumber = hm.get('tollFreeNumber');
							}
							if (hm.containsKey('errorMessage')) {
								errorMessage = hm.get('errorMessage');
							}

							if (String.isNotBlank(serviceId)) {
								lineNumberTenDigits = serviceId;
								// No TN was provided as an input for Dark ADSL
								// Generating and returning a new tnList based on the TN provided by the ASF GetService method (from Inventory Service) to TOCP
								tnList = new List<OrdrTnLineNumber>();
								tnList.add(new OrdrTnLineNumber(serviceId, 'COID', OrdrConstants.SWITCH_CODE, false, reservationType, OrdrTnLineNumberStatus.AVAILABLE, null) );
								telephoneNumberStatus = 'reserved';
							}
							else if (String.isNotBlank(tollFreeNumber)) {
								lineNumberTenDigits = tollFreeNumber;
								telephoneNumberStatus = 'reserved';
							}
							else if ( String.isNotBlank(npa) && String.isNotBlank(nxx) && String.isNotBlank(line)) {
								lineNumberTenDigits = npa+nxx+line;
							}
							if (lineNumberTenDigits != null) {
								Integer j = 0;
								if (tnList != null) {
									j = tnList.size();
									}
								for (Integer k=0; k<j; k++) {
									String tnTenDigits = tnList.get(k).getNpa()+tnList.get(k).getNxx()+tnList.get(k).getLineNumber();
									if ((tnTenDigits != null) && (tnTenDigits.equalsIgnoreCase(lineNumberTenDigits))) {
										if (telephoneNumberStatus != null) {
											if (telephoneNumberStatus.equalsIgnoreCase('reserved')) {
												// if the TN was successfully reserved - tag this TN as RESERVED_UNASSIGNED 
												tnList.get(k).setStatus(OrdrTnLineNumberStatus.RESERVED_UNASSIGNED);
												tnList.get(k).setComments(null);
                                                System.debug(' the reserved no from the TOCP interface is '+tnTenDigits);
											}
											else
											{
												// if the TN reservation failed - error message returned or exception/fault message thrown - tag this TN as still Available
												tnList.get(k).setStatus(OrdrTnLineNumberStatus.AVAILABLE);
												tnList.get(k).setComments(null);
												System.debug('Error caught in ReserveResourceImpl.reserveResourceForTnList for '+tnList.get(k).getTenDigitsTn()+' : '+errorMessage);
											}
										}
										break;
									}
								}
							}
						}
					}
				}
			}
		}
		return tnList;
	}

	private static TpFulfillmentResourceOrderV3.ResourceOrder generateTocpResourceOrderForRealTnSoapRequest(OrdrTnServiceAddress serviceAddress, List<OrdrTnLineNumber> tnList, String userId, String referenceNumber) {
		TpFulfillmentResourceOrderV3.ResourceOrder resourceOrder = new TpFulfillmentResourceOrderV3.ResourceOrder();
		//Order action reference number
		resourceOrder.Id=checkNullInput(referenceNumber);
		resourceOrder.Specification=generateTocpV2EntitySpecificationForSoapRequest('Wireline', 'Telephone Number', 'Logical Resource');
		List<TpCommonBaseV3.CharacteristicValue> charValList=new List<TpCommonBaseV3.CharacteristicValue>();
		charValList.add(generateTocpV2CharacteristicValueForSoapRequest('productType', 'Real TN', true));
		charValList.add(generateTocpV2CharacteristicValueForSoapRequest('userId', 'SFDCTN', false));
		resourceOrder.CharacteristicValue=charValList;
		
		List<TpFulfillmentResourceOrderV3.ResourceOrderItem> resourceOrderItemList=new List<TpFulfillmentResourceOrderV3.ResourceOrderItem>();
        if(tnList!=null){
            		for (Integer i=0; i<tnList.size(); i++) {
			TpFulfillmentResourceOrderV3.ResourceOrderItem resourceOrderItem =  new TpFulfillmentResourceOrderV3.ResourceOrderItem();
			TpCommonBusinessInteractionV3.BusinessInteractionEntity businessInteractionEntity = new TpCommonBusinessInteractionV3.BusinessInteractionEntity();
			 List<TpCommonBaseV3.CharacteristicValue> busIntEnttyCharValList=new List<TpCommonBaseV3.CharacteristicValue>();
			 busIntEnttyCharValList.add(generateTocpV2CharacteristicValueForSoapRequest('npa', checkNullInput(tnList.get(i).getNpa()), true));
			 busIntEnttyCharValList.add(generateTocpV2CharacteristicValueForSoapRequest('nxx', checkNullInput(tnList.get(i).getNxx()), false));
			 busIntEnttyCharValList.add(generateTocpV2CharacteristicValueForSoapRequest('line', checkNullInput(tnList.get(i).getLineNumber()), false));
			 busIntEnttyCharValList.add(generateTocpV2CharacteristicValueForSoapRequest('releaseDate', '', false));
			 businessInteractionEntity.CharacteristicValue=busIntEnttyCharValList;
			
			resourceOrderItem.Resource=businessInteractionEntity;
			if (i == 0) {
				String switchNumber = tnList.get(i).getSwitchNumber();
				String coid = tnList.get(i).getServingCoid();
				if (String.isBlank(coid)) {
					coid = serviceAddress.getCOID();
				}
				if (String.isNotBlank(switchNumber)) {
					switchNumber =OrdrConstants.SWITCH_CODE;
				}

				charValList.add(generateTocpV2CharacteristicValueForSoapRequest('coid', coid, false));
				charValList.add(generateTocpV2CharacteristicValueForSoapRequest('switchNumber', switchNumber, false));
				System.debug('Input to Reserve callout coid='+coid);
				System.debug('Input to Reserve callout switchNumber='+switchNumber);
				resourceOrder.CharacteristicValue=charValList;
				
			}
			
			resourceOrderItemList.add(resourceOrderItem);
		}
        }

		resourceOrder.CharacteristicValue=charValList;
		resourceOrder.ResourceOrderItem=resourceOrderItemList;
		return resourceOrder;
	}


	@TestVisible private static TpFulfillmentResourceOrderV3.ResourceOrder generateTocpResourceOrderForDarkDslSoapRequest(OrdrTnServiceAddress serviceAddress, List<OrdrTnLineNumber> tnList, String userId, String referenceNumber) {
		TpFulfillmentResourceOrderV3.ResourceOrder resourceOrder = new TpFulfillmentResourceOrderV3.ResourceOrder();
		//Order action reference number
		resourceOrder.Id=checkNullInput(referenceNumber);
		resourceOrder.Specification=generateTocpV2EntitySpecificationForSoapRequest('Wireline', 'Telephone Number', 'Logical Resource');
        List<TpCommonBaseV3.CharacteristicValue> charValList=new List<TpCommonBaseV3.CharacteristicValue>();
        charValList.add(generateTocpV2CharacteristicValueForSoapRequest('productType', 'Dark DSL', true));
        charValList.add(generateTocpV2CharacteristicValueForSoapRequest('userId', 'SFDCTN' , false));
        charValList.add(generateTocpV2CharacteristicValueForSoapRequest('servicePathId', '', false));
        charValList.add(generateTocpV2CharacteristicValueForSoapRequest('coid', checkNullInput(serviceAddress.getCOID()), false));
        resourceOrder.CharacteristicValue=charValList;
		
		TpFulfillmentResourceOrderV3.ResourceOrderItem resourceOrderItem =  new TpFulfillmentResourceOrderV3.ResourceOrderItem();
		// Service Address attributes passed in the ResourceOderItem.Address - urbanPropertyAddress type
		if (serviceAddress != null){
			TpCommonUrbanPropertyAddressV3.UrbanPropertyAddress urbanPropertyAddress = new TpCommonUrbanPropertyAddressV3.UrbanPropertyAddress();
			urbanPropertyAddress.Id=checkNullInput(serviceAddress.getAddressId());
             List<TpCommonBaseV3.CharacteristicValue> urbCharValList=new List<TpCommonBaseV3.CharacteristicValue>();
            urbCharValList.add(generateTocpV2CharacteristicValueForSoapRequest('street', checkNullInput(serviceAddress.getStreetName()), true));
            urbCharValList.add(generateTocpV2CharacteristicValueForSoapRequest('province', checkNullInput(serviceAddress.getProvinceStateCode()), false));
            urbCharValList.add(generateTocpV2CharacteristicValueForSoapRequest('city', checkNullInput(serviceAddress.getMunicipalityName()), false));
            urbCharValList.add(generateTocpV2CharacteristicValueForSoapRequest('houseNumber', checkNullInput(serviceAddress.getStreetNumber()), false));
            urbCharValList.add(generateTocpV2CharacteristicValueForSoapRequest('houseNumberSuffix', checkNullInput(serviceAddress.getStreetNumberSuffix()), false));
            urbCharValList.add(generateTocpV2CharacteristicValueForSoapRequest('postalCode', checkNullInput(serviceAddress.getPostalZipCode()), false));
            urbCharValList.add(generateTocpV2CharacteristicValueForSoapRequest('apartmentNumber', checkNullInput(serviceAddress.getUnitNumber()), false));
            urbanPropertyAddress.CharacteristicValue=urbCharValList;
			
			resourceOrderItem.Address=urbanPropertyAddress;
		} 
        List<TpFulfillmentResourceOrderV3.ResourceOrderItem> resOrdItemList=new List<TpFulfillmentResourceOrderV3.ResourceOrderItem>();
        resOrdItemList.add(resourceOrderItem);
		resourceOrder.ResourceOrderItem=resOrdItemList;
		return resourceOrder;
	}

	private static TpFulfillmentResourceOrderV3.ResourceOrder generateTocpResourceOrderForTollFreeSoapRequest(OrdrTnServiceAddress serviceAddress, List<OrdrTnLineNumber> tnList, String userId, String referenceNumber) {
		TpFulfillmentResourceOrderV3.ResourceOrder resourceOrder = new TpFulfillmentResourceOrderV3.ResourceOrder();
		//Order action reference number - digit alphanumeric correlation id used by SMS - must begin with a capital 'O'
		String id = String.valueOf(System.currentTimeMillis()).substring(4,13);
		// Total length of ID passed to SMS should be exactly 10 : 9 digits number prefixed by 'O'
		resourceOrder.Id=('O'+id);
		resourceOrder.Specification=generateTocpV2EntitySpecificationForSoapRequest('', 'Toll Free Telephone Number', 'Logical Resource');
		List<TpFulfillmentResourceOrderV3.ResourceOrderItem> resourceOrderItemList=new List<TpFulfillmentResourceOrderV3.ResourceOrderItem>();
		for (Integer i=0; i<tnList.size(); i++) {
			TpFulfillmentResourceOrderV3.ResourceOrderItem resourceOrderItem =  new TpFulfillmentResourceOrderV3.ResourceOrderItem();
			TpCommonBusinessInteractionV3.BusinessInteractionEntity businessInteractionEntity = new TpCommonBusinessInteractionV3.BusinessInteractionEntity();
            
            
             List<TpCommonBaseV3.CharacteristicValue> busIntEnttyCharValList=new List<TpCommonBaseV3.CharacteristicValue>();
			 busIntEnttyCharValList.add(generateTocpV2CharacteristicValueForSoapRequest('respOrg', 'EUS01', true));
			 busIntEnttyCharValList.add(generateTocpV2CharacteristicValueForSoapRequest('tollFreeNumber', checkNullInput(tnList.get(i).getTenDigitsTn()), false));
			 busIntEnttyCharValList.add(generateTocpV2CharacteristicValueForSoapRequest('notes', 'SFDCTN', false));
			 businessInteractionEntity.CharacteristicValue=busIntEnttyCharValList;
            
			
			resourceOrderItem.Resource=businessInteractionEntity;
			resourceOrderItem.Action='reserve';
			resourceOrderItemList.add(resourceOrderItem);
			
		}
		resourceOrder.ResourceOrderItem=resourceOrderItemList;
		return resourceOrder;
	}


	private static String checkNullInput(String sInput) {
		String sOutput = '';
		sOutput = (String.isBlank(sInput) ? sOutput : sInput);  
		return sOutput;
	}



@TestVisible private static List<TpCommonBaseV3.EntityWithSpecification> generateTocpSourceCriteriaForWirelineDarkDslAndRealTnSoapRequest(OrdrTnServiceAddress serviceAddress, String userId, String npa, String alternateNxx) {
    	List<TpCommonBaseV3.EntityWithSpecification> lews = new List<TpCommonBaseV3.EntityWithSpecification>();
    	TpCommonBaseV3.EntityWithSpecification sourceCriteria = new TpCommonBaseV3.EntityWithSpecification();
    	sourceCriteria.Specification=generateTocpV2EntitySpecificationForSoapRequest('Wireline', 'Telephone Number', 'Logical Resource');
		List<TpCommonBaseV3.CharacteristicValue> charValList=new List<TpCommonBaseV3.CharacteristicValue>();
		charValList.add(generateTocpV2CharacteristicValueForSoapRequest('userId', 'SFDCTN', true));
		charValList.add(generateTocpV2CharacteristicValueForSoapRequest('npa', checkNullInput(npa), false));
		charValList.add(generateTocpV2CharacteristicValueForSoapRequest('nxx', checkNullInput(alternateNxx), false));
		sourceCriteria.CharacteristicValue=charValList;
    	
		lews.add(sourceCriteria);
    	return lews;
    }
  
 @TestVisible  private static List<TpCommonBaseV3.EntityWithSpecification> generateTocpSourceCriteriaForTollFreeSoapRequest(OrdrTnServiceAddress serviceAddress, String userId, String tn) {
		//Order action reference number - digit alphanumeric correlation id used by SMS - must begin with a capital 'O'
		String id = String.valueOf(System.currentTimeMillis()).substring(4,13);
		// Total length of ID passed to SMS should be exactly 10 : 9 digits number prefixed by 'O'
    	List<TpCommonBaseV3.EntityWithSpecification> lews = new List<TpCommonBaseV3.EntityWithSpecification>();
    	TpCommonBaseV3.EntityWithSpecification sourceCriteria = new TpCommonBaseV3.EntityWithSpecification();
    	sourceCriteria.Specification=generateTocpV2EntitySpecificationForSoapRequest('', 'Toll Free Telephone Number', 'Logical Resource');
    	sourceCriteria.Id=('O'+id);
		List<TpCommonBaseV3.CharacteristicValue> charValList=new List<TpCommonBaseV3.CharacteristicValue>();
		charValList.add(generateTocpV2CharacteristicValueForSoapRequest('respOrg', 'EUS01', true));
		charValList.add(generateTocpV2CharacteristicValueForSoapRequest('tollFreeNumber', checkNullInput(tn), false));
		sourceCriteria.CharacteristicValue=charValList;
    	
		lews.add(sourceCriteria);
    	return lews;
    }
 
   @TestVisible private static List<TpCommonBaseV3.EntityWithSpecification> generateTocpTargetCriteriaForTollFreeSoapRequest(OrdrTnServiceAddress serviceAddress, String userId, String tn) {
    	List<TpCommonBaseV3.EntityWithSpecification> lews = new List<TpCommonBaseV3.EntityWithSpecification>();
    	TpCommonBaseV3.EntityWithSpecification targetCriteria = new TpCommonBaseV3.EntityWithSpecification();
		List<TpCommonBaseV3.CharacteristicValue> charValList=new List<TpCommonBaseV3.CharacteristicValue>();
		charValList.add(generateTocpV2CharacteristicValueForSoapRequest('searchOption', 'telephoneNumber', true));
		targetCriteria.CharacteristicValue=charValList;
		lews.add(targetCriteria);
    	return lews;
    }
  
	

	


	public static Map <String, String> convertTocpV2CharacteristicValuesToMap (List<TpCommonBaseV3.CharacteristicValue> listCharVal) {
		Map <String, String> hm = new Map <String, String>();
		for(Integer z=0;z<listCharVal.size();z++) {
			TpCommonBaseV3.CharacteristicValue cv = listCharVal.get(z);
			if ((cv != null) && (cv.Characteristic != null)) {
				TpCommonBaseV3.Characteristic c = cv.Characteristic;
				String cName  = c.Name;
				if (String.isNotBlank(cName)) {
					String cValue = null;
					if ((cv.Value != null) && (cv.Value.size()>0)) {
						cValue = cv.Value.get(0);
						if (!hm.containsKey(cName)) {
							hm.put(cName, cValue);
						}
					}
				}
			}
		}
		System.debug('Created map from the resserve callout response is '+hm);
		return hm;
	}


	public static TpCommonBaseV3.EntitySpecification generateTocpV2EntitySpecificationForSoapRequest(
			String specificationName, String specificationType, String specificationCategory) {
    	// Setting service specifications
		TpCommonBaseV3.EntitySpecification entitySpec = new TpCommonBaseV3.EntitySpecification();
   		entitySpec.Name=checkNullInput(specificationName);
    	entitySpec.Type_x=checkNullInput(specificationType);
    	entitySpec.Category=checkNullInput(specificationCategory);
		return entitySpec;
	}

		public static TpCommonBaseV3.CharacteristicValue generateTocpV2CharacteristicValueForSoapRequest(
			String nameOfCharacteristic, String valueOfCharacteristic, boolean firstElement) {

		TpCommonBaseV3.CharacteristicValue charValueOne = null;
		if (String.isNotBlank(nameOfCharacteristic)) {
			charValueOne = new TpCommonBaseV3.CharacteristicValue();
			TpCommonBaseV3.Characteristic charOne = new TpCommonBaseV3.Characteristic();			 
			charOne.Name=nameOfCharacteristic;
			charValueOne.Characteristic=charOne;
			List<String> valueList=new List<String>();
			valueList.add(checkNullInput(valueOfCharacteristic));
			charValueOne.Value=valueList;
			
		}
		return charValueOne;
	}

 }