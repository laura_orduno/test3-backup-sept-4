@isTest
public class BillingAdjustmentApprovalProcessTest{

    static{
        BillingAdjustmentCommonTest.createReleaseCodeThresholdData('Credit', 'THPS');
        BillingAdjustmentCommonTest.createFinanceControllerData('THPS');
        BillingAdjustmentCommonTest.createControllerThresholdData('Credit', 'THPS');
        BillingAdjustmentCommonTest.createReasonCodeData();
        BillingAdjustmentCommonTest.createAprovlMtxcCustomSettingData();
        loadProductApprovalThresholdSettings();
    }

    @isTest
    static void testSubmitAgentApproval(){
        User agent = BillingAdjustmentCommonTest.getAgentUser();
        Id productApprovalId = Schema.SObjectType.Approval_Threshold__c.getRecordTypeInfosByName().get('Product Approval Threshold').getRecordTypeId();
        System.runAs(agent){
            Approval_Threshold__c newAT = new Approval_Threshold__c(
                RecordTypeId = productApprovalId,
                Product_Category_1__c = 'Wireline',
                Product_Category_2__c = 'Internet',
                Credit_Threshold__c = 300.00,
                Product_Approval_Threshold_Active__c = true
            );

            insert newAt;

            Billing_Adjustment__c ba = BillingAdjustmentCommonTest.createBillingAdjustment(400,'Credit', 'THPS');
            ba = loadBillingAdustment(ba.id);
           
            Approval.ProcessSubmitRequest req1 =  new Approval.ProcessSubmitRequest();
            req1.setComments('Submitting request for approval.');
            req1.setObjectId(ba.id);
                 
            Approval.ProcessResult result = Approval.process(req1);

            System.assert(result.isSuccess());
            System.assertEquals(
                'Pending', result.getInstanceStatus(), 
                'Instance Status: '+result.getInstanceStatus());

            List<Id> newWorkItemIds = result.getNewWorkitemIds();

            Approval.ProcessWorkitemRequest req2 = 
            new Approval.ProcessWorkitemRequest();
            req2.setComments('Approving request.');
            req2.setAction('Approve');
            req2.setWorkitemId(newWorkItemIds.get(0));

            Approval.ProcessResult result2 =  Approval.process(req2);
        
            // Verify the results
            System.assert(result2.isSuccess(), 'Result Status:'+result2.isSuccess());
            
            System.assertEquals(
                'Approved', result2.getInstanceStatus(), 
                'Instance Status'+result2.getInstanceStatus());
        }
   }

   @isTest
    static void testSubmitCustomThresholdAgentApproval(){
        User agent = BillingAdjustmentCommonTest.getAgentUser();
        Id productApprovalId = Schema.SObjectType.Approval_Threshold__c.getRecordTypeInfosByName().get('Product Approval Threshold').getRecordTypeId();
        Id individualApprovalId = Schema.SObjectType.Approval_Threshold__c.getRecordTypeInfosByName().get('Individual Threshold').getRecordTypeId();

        System.debug('agent.Id ::: ' + agent.Id);
        System.debug('agent.ManagerId ::: ' + agent.ManagerId);

        Approval_Threshold__c newIndividualAT = new Approval_Threshold__c(
            RecordTypeId = individualApprovalId,
            Individual_User__c = agent.ManagerId,
            Individual_Threshold__c = 10000.00,
            Transaction_Type__c = 'Credit',
            Business_Unit__c = 'THPS'
        );

        insert newIndividualAT;

        System.runAs(agent){
            Approval_Threshold__c newAT = new Approval_Threshold__c(
                RecordTypeId = productApprovalId,
                Product_Category_1__c = 'Wireline',
                Product_Category_2__c = 'Internet',
                Credit_Threshold__c = 300.00,
                Product_Approval_Threshold_Active__c = true
            );

            insert newAt;

            Billing_Adjustment__c ba = BillingAdjustmentCommonTest.createBillingAdjustment(9999.00,'Credit', 'THPS');
            ba = loadBillingAdustment(ba.id);
           
            Approval.ProcessSubmitRequest req1 =  new Approval.ProcessSubmitRequest();
            req1.setComments('Submitting request for approval.');
            req1.setObjectId(ba.id);
                 
            Approval.ProcessResult result = Approval.process(req1);

            System.assert(result.isSuccess());
            System.assertEquals(
                'Pending', result.getInstanceStatus(), 
                'Instance Status: '+result.getInstanceStatus());

            List<Id> newWorkItemIds = result.getNewWorkitemIds();

            Approval.ProcessWorkitemRequest req2 = 
            new Approval.ProcessWorkitemRequest();
            req2.setComments('Approving request.');
            req2.setAction('Approve');
            req2.setWorkitemId(newWorkItemIds.get(0));

            Approval.ProcessResult result2 =  Approval.process(req2);
        
            // Verify the results
            System.assert(result2.isSuccess(), 'Result Status:'+result2.isSuccess());
            
            System.assertEquals(
                'Pending', result2.getInstanceStatus(), 
                'Instance Status'+result2.getInstanceStatus());
        }
   }

   /**
    * testPMApprovalNotRequired
    * @description SFDC auto rejects in test scenario when a step is skipped
    *              The step that is skipped is the PM check.
    * @author Thomas Tran, Traction on Demand
    * @date 03-02-2016
    */
    @isTest
    static void testPMApprovalNotRequired(){
        User agent = BillingAdjustmentCommonTest.getAgentUser();
        System.runAs(agent){

            Billing_Adjustment__c ba = BillingAdjustmentCommonTest.createBillingAdjustment(400,'Credit', 'THPS');
            ba = loadBillingAdustment(ba.id);
           
            Approval.ProcessSubmitRequest req1 =  new Approval.ProcessSubmitRequest();
            req1.setComments('Submitting request for approval.');
            req1.setObjectId(ba.id);
                 
            Approval.ProcessResult result = Approval.process(req1);

            System.assert(result.isSuccess());
            System.assertEquals(
                'Approved', result.getInstanceStatus(), 
                'Instance Status: '+result.getInstanceStatus());
        }
   }

    @isTest
    static void testSubmitManagerApproval(){
        User manager = BillingAdjustmentCommonTest.getAgentManagerUser();
        Approval.ProcessResult result=null; 
        System.runAs(manager){
            Billing_Adjustment__c ba = BillingAdjustmentCommonTest.createBillingAdjustment(1200,'Credit', 'THPS');
            ba = loadBillingAdustment(ba.id);
            Approval.ProcessSubmitRequest req1 =  new Approval.ProcessSubmitRequest();
            req1.setComments('Submitting request for approval.');
            req1.setObjectId(ba.id);
                 
            // Submit the approval request for the account
            result = Approval.process(req1);
        
            System.assert(result.isSuccess());
            System.assertEquals(
                'Pending', result.getInstanceStatus(), 
                'Instance Status: '+result.getInstanceStatus());
            
         }
     }
   
    @isTest()
    static void testApprovalFlowDynamic(){
        System.debug('testSubmitManagerApproval - Start');
        User agent = BillingAdjustmentCommonTest.getAgentUser();
        User nextApprover =null;
        Billing_Adjustment__c ba=null; 
        Approval.ProcessResult result = null;
       System.runAs(agent){
            ba = BillingAdjustmentCommonTest.createBillingAdjustment(500000.00,'Credit', 'THPS');
            ba.Status__c = 'Submitted';
            ba = loadBillingAdustment(ba.id);
            Approval.ProcessSubmitRequest req1 =  new Approval.ProcessSubmitRequest();
            req1.setComments('Submitting request for approval.');
            req1.setObjectId(ba.id);
            result = Approval.process(req1);
            System.assert(result.isSuccess());
            System.assertEquals('Pending', result.getInstanceStatus(),'Instance Status:Submit for Approval '+result.getInstanceStatus());
            nextApprover  = BillingAdjustmentCommonTest.getUser(ba.Next_Approver__c);
         }
        
       for(Integer i=1; i<=3;i++){
            System.runAs(nextApprover){
                System.debug('@Approving Bar  L'+ i + ' ' + nextApprover );
                List<Id> newWorkItemIds = result.getNewWorkitemIds();
                Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
                req2.setComments('Approving request.');
                req2.setAction('Approve');
                req2.setWorkitemId(newWorkItemIds.get(0));
                result =  Approval.process(req2);
                System.assert(result.isSuccess(), 'Result Status:2 '+result.isSuccess());
                System.assertEquals('Pending', result.getInstanceStatus(),'Instance Status'+result.getInstanceStatus());
                System.debug('@After Approval Status  L'+ i + ' ' + result.getInstanceStatus());
                ba = loadBillingAdustment(ba.id);
                nextApprover  = BillingAdjustmentCommonTest.getUser(ba.Next_Approver__c);
                System.debug('@After Approval Next Approver  L'+ i + ' ' + nextApprover);
                
            }
       
       }
    }
   
    @isTest()
    static void testFirstApproverAdjustmentRequester(){
        System.debug('testSubmitManagerApproval - Start');
         User agent = BillingAdjustmentCommonTest.sampleUserMap.get('agent');
         User usrNoManager = BillingAdjustmentCommonTest.sampleUserMap.get('usrNoManager');
         Approval.ProcessResult result1=null;
         User nextApprover =null;
         Billing_Adjustment__c ba=null; 
         Approval.ProcessResult result = null;
         BillingAdjustmentDataHelper.getReasonCodeDescSet();
         BillingAdjustmentDataHelper.getAccountId();
         BillingAdjustmentDataHelper.getAdjustmentTypeAndBusinessUnit();
         BillingAdjustmentDataHelper.getUserList();
       System.runAs(agent){
            ba = BillingAdjustmentCommonTest.getBillingAdjustment(5000000,'Credit', 'THPS',agent.id);
            ba.Requested_By__c = usrNoManager.id;
            ba.First_Approver__c ='Adjustment Requester';
            insert ba;
            ba = loadBillingAdustment(ba.id);
            Approval.ProcessSubmitRequest req1 =  new Approval.ProcessSubmitRequest();
            req1.setComments('Submitting request for approval.');
            req1.setObjectId(ba.id);
            result = Approval.process(req1);
            System.assert(result.isSuccess());
            System.assertEquals('Pending', result.getInstanceStatus(),'Instance Status:Submit for Approval '+result.getInstanceStatus());
            nextApprover  = BillingAdjustmentCommonTest.getUser(ba.Next_Approver__c);
         }
        
       for(Integer i=1; i<=1;i++){
            System.runAs(nextApprover){
                System.debug('@Approving Bar  L'+ i + ' ' + nextApprover );
                List<Id> newWorkItemIds = result.getNewWorkitemIds();
                Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
                req2.setComments('Approving request.');
                req2.setAction('Approve');
                req2.setWorkitemId(newWorkItemIds.get(0));
                try{
                result =  Approval.process(req2);
                }catch(Exception e){
                    System.debug('manager error :' + e.getMessage());
                    return;
                }    
                System.assert(result.isSuccess(), 'Result Status:2 '+result.isSuccess());
                System.assertEquals('Pending', result.getInstanceStatus(),'Instance Status'+result.getInstanceStatus());
                System.debug('@After Approval Status  L'+ i + ' ' + result.getInstanceStatus());
                ba = loadBillingAdustment(ba.id);
                nextApprover  = BillingAdjustmentCommonTest.getUser(ba.Next_Approver__c);
                System.debug('@After Approval Next Approver  L'+ i + ' ' + nextApprover);
                
            }
       
       }
    }
        
    @isTest()
    static void testFirstApproverOtherManager(){
       //Test the FirstApprover as Other Manager
       User agent = BillingAdjustmentCommonTest.sampleUserMap.get('agent');
       User usrNoManager = BillingAdjustmentCommonTest.sampleUserMap.get('usrNoManager'); 
        System.runAs(agent){
            Billing_Adjustment__c ba1 = BillingAdjustmentCommonTest.getBillingAdjustment(5000000,'Credit', 'THPS',agent.id);
            ba1.Requested_By__c = usrNoManager.id;
            ba1.First_Approver__c ='Other Manager';
            ba1.Other_Approval_Manager__c  = BillingAdjustmentCommonTest.sampleUserMap.get('dir').id;
            insert ba1;
            Approval.ProcessSubmitRequest req2 =  new Approval.ProcessSubmitRequest();
            req2.setComments('Submitting request for approval.');
            req2.setObjectId(ba1.id);
            Approval.ProcessResult result2 = Approval.process(req2);
            System.assert(result2.isSuccess());
            System.assertEquals('Pending', result2.getInstanceStatus(),'Instance Status:Submit for Approval '+result2.getInstanceStatus());
           
         } 
    }
    
    @isTest()
    static void testSetEmailNotifySalesPrimeUsers(){
        User agent = BillingAdjustmentCommonTest.sampleUserMap.get('agent');
        User vp = BillingAdjustmentCommonTest.sampleUserMap.get('vp');
        User dir = BillingAdjustmentCommonTest.sampleUserMap.get('dir');
        System.debug('is IN User H: '+BillingAdjustmentDataHelper.isInUserHierarchy(vp, dir.id));
        Billing_Adjustment__c ba = BillingAdjustmentCommonTest.getBillingAdjustment(5000000,'Credit', 'THPS',agent.id);
        ba.Next_Approver__c = dir.id;
        ba.Product_Type__c ='Wireless';
        
        BillingAdjustmentDataHelper.addData(ba);
        BillingAdjustmentDataHelper.needAccountIdLoad=true;
        BillingAdjustmentDataHelper.loadData();
        BillingAdjustmentDataHelper.addUserToMap(dir);
        ba.Parent_Account_id__c = BillingAdjustmentDataHelper.getParentAccountId(ba);
        BillingAdjustmentHandlerHelper.setEmailNotifySalesPrimeUsers(ba);
        ba.Product_Type__c ='Wireline';
        BillingAdjustmentHandlerHelper.setEmailNotifySalesPrimeUsers(ba);
    }

    private static Billing_Adjustment__c  loadBillingAdustment(Id baId){
        return [select id, Account__c, Total_Amount__c, Approver_Release_Code__c, Required_Release_Code__c ,Next_Approver__c,Next_Approver__r.ManagerId,Controller_Approver__c, 
              Corporate_Controller_Approver__c, Approval_Status__c,  Corporate_Controller_Approval_Required__c, 
              Controller_Approval_Required__c, Last_Approver_User__c, Transaction_type__c, OwnerId,Business_Unit__c,
                RC_Level_1__C, RC_Level_2__C, RC_Level_3__c,RC_Product__C, Billing_Team__c,BC_Approval_Pending__c,CC_Approval_Pending__c
                from Billing_Adjustment__c where id =:baId ];
    }

    @isTest 
    public static void testHandleApproval(){
        User agent =  BillingAdjustmentCommonTest.sampleUserMap.get('agent');
        User svp =  BillingAdjustmentCommonTest.sampleUserMap.get('svp');
        Billing_Adjustment__c ba =BillingAdjustmentCommonTest.createBillingAdjustment(5000000,'Credit', 'THPS',agent);
        ba.Required_Release_Code__c = '8';
        ba.Approver_Release_Code__c = '8';
        ba.Next_Approver__c = agent.id;
        ba.Next_Hierarchy_Approver__c = null;
        try{
            BillingAdjustmentHandlerHelper.handleApproval(ba);
        }catch(Exception e){}    
    }

    /**
     * testVPApproval
     * @description Tests that the VP approval process is completed inb this flow
     * @author Thomas Tran, Traction on Demand
     * @date 03-10-2016
     */
    @isTest
    public static void testVPApproval(){
        User vp = BillingAdjustmentCommonTest.sampleUserMap.get('vp');
        Id productApprovalId = Schema.SObjectType.Approval_Threshold__c.getRecordTypeInfosByName().get('Product Approval Threshold').getRecordTypeId();
        System.runAs(vp){
            Approval_Threshold__c newAT = new Approval_Threshold__c(
                RecordTypeId = productApprovalId,
                Product_Category_1__c = 'Wireline',
                Product_Category_2__c = 'Internet',
                Credit_Threshold__c = 300.00,
                Product_Approval_Threshold_Active__c = true
            );

            insert newAt;

            Billing_Adjustment__c ba = BillingAdjustmentCommonTest.createBillingAdjustment(100000,'Credit', 'THPS');
            ba = loadBillingAdustment(ba.id);
           
            Test.startTest();

            Approval.ProcessSubmitRequest req =  new Approval.ProcessSubmitRequest();
            req.setComments('Submitting request for approval.');
            req.setObjectId(ba.id);
                 
            Approval.ProcessResult result = Approval.process(req);

            if(result.getInstanceStatus() == 'Pending'){
                Approval.ProcessWorkitemRequest pwReq;
                do{
                    System.assert(result.isSuccess(), 'Result Status:'+result.isSuccess());
                    System.assertEquals(
                        'Pending', result.getInstanceStatus(), 
                        'Instance Status'+result.getInstanceStatus());

                    List<Id> newWorkItemIds = result.getNewWorkitemIds();

                    pwReq = new Approval.ProcessWorkitemRequest();
                    pwReq.setComments('Approving request.');
                    pwReq.setAction('Approve');
                    pwReq.setWorkitemId(newWorkItemIds.get(0));

                    result =  Approval.process(pwReq);

                } while(result.getInstanceStatus() == 'Pending');

                System.assert(result.isSuccess(), 'Result Status:'+result.isSuccess());
                    System.assertEquals(
                        'Approved', result.getInstanceStatus(), 
                        'Instance Status'+result.getInstanceStatus());
            }

            Test.stopTest();

            List<Billing_Adjustment__c> baList = [SELECT VP_Approval_Pending__c, VP_Controller_Approval_Required__c, VP_Controller_Approved__c
                                                  FROM Billing_Adjustment__c
                                                  WHERE Id = :ba.Id];

            System.assertEquals(false, baList[0].VP_Approval_Pending__c);
            System.assertEquals(true, baList[0].VP_Controller_Approval_Required__c);
            System.assertEquals(true, baList[0].VP_Controller_Approved__c);
        }
    }

    /**
     * loadProductApprovalThresholdSettings
     * @description Loads the threshold fields to search for in the query
     * @author Thomas Tran, Traction on Demand
     * @date 02-29-2016
     */
    private static void loadProductApprovalThresholdSettings(){
        List<Product_Approval_Threshold_Settings__c> patSettings = new List<Product_Approval_Threshold_Settings__c>();

        patSettings.add(new Product_Approval_Threshold_Settings__c(Name='Credit', Threshold_API_Name__c='Credit_Threshold__c'));
        patSettings.add(new Product_Approval_Threshold_Settings__c(Name='Debit', Threshold_API_Name__c='Debit_Threshold__c'));
        patSettings.add(new Product_Approval_Threshold_Settings__c(Name='TLC Charge', Threshold_API_Name__c='TLC_Charge_Threshold__c'));
        patSettings.add(new Product_Approval_Threshold_Settings__c(Name='TLC Waive', Threshold_API_Name__c='TLC_Waive_Threshold__c'));

        insert patSettings;

    }
}