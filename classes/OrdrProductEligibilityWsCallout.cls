/* 2015-12-12[Roderick de Vera]
 * Helper class which invokes the Web Service Callout for Product Availability Check
 */
public class OrdrProductEligibilityWsCallout {
    
    public static TpFulfillmentCustomerOrderV3.CustomerOrder performCustomerOrderFeasibility(Map<String, Object> inputMap) {
        TpFulfillmentCustomerOrderV3.CustomerOrder customerOrder = new TpFulfillmentCustomerOrderV3.CustomerOrder();
        try { 
            OrdrWsPerformCustomerOrderFeasibility.PerformCustomerOrderFeasibilitySOAPPort servicePort = new OrdrWsPerformCustomerOrderFeasibility.PerformCustomerOrderFeasibilitySOAPPort(); 
            servicePort = prepareCallout(servicePort);          
            customerOrder = servicePort.performCustomerOrderFeasibility(prepareCustomerOrderPayload(inputMap));    
        } 
        catch (CalloutException e) {

            OrdrUtilities.auditLogs('OrdrProductEligibilityWsCallout.performCustomerOrderFeasibility', 
                                    'NetCracker', 
                                    'PerformCustomerOrderFeasibility', 
                                    null, 
                                    null, 
                                    true,
                                    e.getMessage(), 
                                    e.getStackTraceString());
            
            throw new OrdrExceptions.ProductEligibilityException(e.getMessage(), e);
        }
        return customerOrder;
    }
    
    private static OrdrWsPerformCustomerOrderFeasibility.PerformCustomerOrderFeasibilitySOAPPort 
        prepareCallout(OrdrWsPerformCustomerOrderFeasibility.PerformCustomerOrderFeasibilitySOAPPort servicePort) {
            Ordering_WS__c productEligibilityEndpoint = Ordering_WS__c.getValues('ProductEligibilityEndpoint');           
            servicePort.endpoint_x = productEligibilityEndpoint.value__c;
            // Set SFDC Webservice call timeout
            servicePort.timeout_x = OrdrConstants.WS_TIMEOUT_MS;    
            
            if (servicePort.endpoint_x.startsWith('https://xmlgwy')) {
                Ordering_WS__c wsUserName =  Ordering_WS__c.getValues(OrdrConstants.CREDENTIAL_USERNAME);
                Ordering_WS__c wsPassword =  Ordering_WS__c.getValues(OrdrConstants.CREDENTIAL_PASSWORD); 
                String credentials = wsUserName.value__c + ':' + wsPAssword.value__c;
                String encodedUserNameAndPassword = EncodingUtil.base64Encode(Blob.valueOf(credentials));   
                
                servicePort.inputHttpHeaders_x = new Map<String, String>();
                servicePort.inputHttpHeaders_x.put('Authorization', 'Basic ' + encodedUserNameAndPassword);
            } else {
                servicePort.clientCertName_x = OrdrConstants.SDF_CERT_NAME;
            }
            
            return servicePort;
    } 
    
    private static TpFulfillmentCustomerOrderV3.CustomerOrder prepareCustomerOrderPayload(Map<String, Object> inputMap) {
        TpFulfillmentCustomerOrderV3.CustomerOrder customerOrder = new TpFulfillmentCustomerOrderV3.CustomerOrder();
        List<TpCommonBaseV3.CharacteristicValue> characteristicValues = new List<TpCommonBaseV3.CharacteristicValue>();
        List<TpFulfillmentCustomerOrderV3.CustomerAccountOrderItem> customerAccountOrderItems = new List<TpFulfillmentCustomerOrderV3.CustomerAccountOrderItem>();
        
        /*create customerOrder/Specification section
        */
        TpCommonBaseV3.EntitySpecification specification = new TpCommonBaseV3.EntitySpecification();
        specification.Name = OrdrConstants.SPECIFICATION_NAME;
        specification.Type_x = OrdrConstants.SPECIFICATION_TYPE_AVAILABILITY;
        specification.Category = OrdrConstants.SPECIFICATION_CATEGORY;
        customerOrder.Specification = specification;
        
        /*create customerOrder/CharacteristicValue/Characteristic/Name section
        */
        characteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.CHARACTERISTIC_TYPE, OrdrConstants.CHARACTERISTIC_TYPE_VALUE));
        //todo: need to figure out where to get actual values
        //characteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.DISTRIBUTION_CHANNEL_KEY, '9143202970313945129'));
        characteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.DISTRIBUTION_CHANNEL_KEY, OrdrUtilities.getDistributionChannel()));
        //todo: need to figure out where to get actual values
        characteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.CUSTOMER_CATEGORY_KEY, '9141752080013288574'));
        
        characteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.USER_ID, OrdrUtilities.getCurrentUserTelusId()));
        customerOrder.CharacteristicValue = characteristicValues;
        
        /*create customerOrder/CustomerAccountOrderItem/Address/CharacteristicValue/Characteristic/Name section
        */
        TpFulfillmentCustomerOrderV3.CustomerAccountOrderItem customerAccountOrderItem = new TpFulfillmentCustomerOrderV3.CustomerAccountOrderItem();
        TpCommonUrbanPropertyAddressV3.UrbanPropertyAddress address = OrdrUtilities.constructAddress(inputMap);
        
        customerAccountOrderItem.Address = address;
        //create an empty Customer Account tag as per PCOF IA
        customerAccountOrderItem.CustomerAccount = new TpInventoryCustomerV3.BaseCustomerAccount();
        customerAccountOrderItems.add(customerAccountOrderItem);     
        
        customerOrder.CustomerAccountOrderItem = customerAccountOrderItems;
            
        
        return customerOrder;
    }
    
}