/**
* @author: Travis Cote
* @description: Unit Test for ENTP
*/

@isTest(SeeAllData=false)
private class ENTP_Test {

	@isTest(SeeAllData=false) static void loginController() {
		ENTPLoginCtlr ctlr = new ENTPLoginCtlr();
		///////
		// scenario one, success
		///////
		Test.setCurrentPage(Page.ENTPLogin);
		User u = MBRTestUtils.createPortalUser('VITILcare');
		ctlr.username = u.username;
		ctlr.password = MBRTestUtils.testUserPassword;
		PageReference result = ctlr.login(); // salesforce does not assert Site.login
		///////

		///////
		// scenario two, fail: user not found
		ctlr.username = null;
		ctlr.password = null;
		result = ctlr.login(); // salesforce does not assert Site.login
		///////

		///////
		// scenario three, test errors
		///////
		ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: Invalid Input.');
		ApexPages.addMessage(myMsg);
		ctlr.username = 'test@testing.com';
		ctlr.password = 'aaaaaaaaaa';
		ctlr.login();
		System.assert(ctlr.errors.size() > 0);
	}

	@isTest(SeeAllData=false) static void SubmitFeedCaseController() {
		MBRTestUtils.createExternalToInternalCustomSettings();
		MBRTestUtils.createParentToChildCustomSettings();
		MBRTestUtils.createTypeToFieldsCustomSettings();

		PageReference currentPage = Page.ENTPFeedback;
		Test.setCurrentPageReference(currentPage);

		ENTPSubmitFeedbackCtlr ctlr = new ENTPSubmitFeedbackCtlr();
		/*  ctlr.attachment.Name='Unit Test Attachment';
		  ctlr.attachment.body=Blob.valueOf('Unit Test Attachment Body'); */
		string s = ctlr.selectedFeedbackType;
		List<SelectOption> cs = ctlr.getRequestTypes();
		List<SelectOption> gt = ctlr.getCategories();
		ctlr.initCheck();
		ctlr.createNewOnSubmit = true;

		ctlr.caseSubmitted = true;
		ctlr.resetValues();

		Case saveParent = ctlr.caseToInsert.parent;
		//        Map<String, Schema.RecordTypeInfo> saveCaseRecordTypeMap = ctlr.caseRecordTypeMap;
		//      ctlr.caseRecordTypeMap = new Map<String, Schema.RecordTypeInfo>();
		ctlr.createNewCase();

		//        ctlr.caseRecordTypeMap = saveCaseRecordTypeMap;
		ctlr.selectedCategory = 'Feedback';
		ctlr.formWrapper.FeedbackInfo.FeedbackType = 'Complaint';
		ctlr.formWrapper.FeedbackInfo.AdditionalNote = 'And another thing.';
		ctlr.caseToInsert.subject = null;
		ctlr.createNewCase();
		ctlr.resetValues();

/*****        
        List<Case> parentCases = [SELECT Id, CaseNumber, My_Business_Requests_Type__c FROM Case ORDER BY CreatedDate DESC LIMIT 1];
        Case parentCase = null;
        if (parentCases.size() > 0) {
            parentCase = parentCases[0];
            parentCase.status = 'Closed';
            update parentCase;
        }
*/
		Case parentCase = [SELECT Id, CaseNumber, My_Business_Requests_Type__c FROM Case ORDER BY CreatedDate DESC LIMIT 1];

//System.debug('parentCase: ' + parentCase);        
		// set pageReference and querystring params
		PageReference submitPageRef = Page.ENTPFeedback;
		Test.setCurrentPage(submitPageRef);
		ApexPages.currentPage().getParameters().put('reopen', parentCase.CaseNumber);

		// instantiate a second controller for the child insert
// ******        ENTPSubmitFeedbackCtlr ctlr2 = new ENTPSubmitFeedbackCtlr();
		//ctlr2.selectedCategory = null;
		//ctlr2.requestType = null;
		ctlr.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId;
		ctlr.caseToInsert.ParentId = parentCase.Id;
		ctlr.createNewCase();
		Case childCase = [SELECT Id, Parent.Id, ParentId, CaseNumber FROM Case WHERE ParentId != null ORDER BY CreatedDate DESC LIMIT 1][0];
		System.assertEquals(parentCase.Id, childCase.Parent.Id);

/*****        
        if (parentCase != null) {
            parentCase.status = 'Closed';
            update parentCase;
        }
*/

		// error test case
		ctlr.selectedCategory = null;
		ctlr.requestType = null;

		/*      //        ctlr.userLanguage = 'klingon';
			  ctlr.caseToInsert.parent = saveParent;
			  ctlr.createNewCase();

			  // error test case
			  ctlr.caseToInsert.parent = null;
			  ctlr.initCheck();
			  ctlr.createNewCase();

			  // error test case
			  ctlr.caseSubmitted = false;
			  ctlr.resetValues();
			  ctlr.clear();

			  // error test case, DML insert exception
			  String error = '';
			  Case errorCase = new Case();
			  insert errorCase;
			  //        ctlr.insertCase(errorCase, error);
		  */
		ctlr.getParentCase();
		ctlr.getParentCaseType();
		ctlr.getParentCategory();
		ctlr.getCaseToInsert();
		//Attachment att = ctlr.attachment;
		//

	}

	@isTest public static void testCloseCase() {

		ENTPSubmitFeedbackCtlr ctlr = new ENTPSubmitFeedbackCtlr();

		String originValue = 'ENTP';

		Case c = new Case(Subject = '4', Status = 'New', Priority = 'Medium', Origin = originValue);

		try {
			insert c;
		} catch (Exception e) {
			System.debug('akong: case insert exception: ' + e);
		}

		Test.startTest();

		c.NotifyCollaboratorString__c = 'testtest@unit-test.com;testtest2@unit-test.com';
		c.Status = 'Closed';
		update c;
		Test.stopTest();
	}
	@isTest public static void testCloseCaseUpdate() {

		ENTPNewCaseTriggerHelper ctlr = new ENTPNewCaseTriggerHelper();

		String originValue = 'ENTP';

		Case c = new Case(Subject = '4', Status = 'New', Priority = 'Medium', Origin = originValue);

		try {
			insert c;
		} catch (Exception e) {
			System.debug('akong: case insert exception: ' + e);
		}

		Test.startTest();

		c.NotifyCollaboratorString__c = 'testtest@unit-test.com;testtest2@unit-test.com';
		c.Status = 'Closed';
		update c;
		Test.stopTest();
	}
	@isTest public static void testUpdateCasePriorityToUrgent() {
		ENTPSubmitFeedbackCtlr ctlr = new ENTPSubmitFeedbackCtlr();

		String originValue = 'ENTP';

		List<Case> cases = new List<Case>();
		cases.add(new Case(Subject = '4', Status = 'New', Priority = 'Medium', Origin = originValue)); // subject will be used for day of week
		cases.add(new Case(Subject = '5', Status = 'New', Priority = 'Medium', Origin = originValue));
		cases.add(new Case(Subject = '6', Status = 'New', Priority = 'Medium', Origin = originValue));
		cases.add(new Case(Subject = '0', Status = 'New', Priority = 'Medium', Origin = originValue));
		cases.add(new Case(Subject = '0', Status = 'New', Priority = 'Urgent', Origin = originValue));
		cases.add(new Case(Subject = '0', Status = 'In Progress', Priority = 'Medium', Origin = originValue));

		System.debug('akong: cases A: ' + cases);

		try {
			insert cases;
		} catch (Exception e) {
			System.debug('akong: case insert exception: ' + e);
		}

		Test.startTest();
		ENTPSubmitFeedbackCtlr.updateCasePriorityToUrgent();
		Test.stopTest();

		List<Case> urgentCases = [SELECT Id, Priority FROM Case WHERE Priority = 'Urgent'];
		System.assertEquals(5, urgentCases.size());
	}

	/*@isTest(SeeAllData=false) static void caseDetailController() {
		PageReference currentPage = Page.ENTPCaseDetailPage;
		Test.setCurrentPageReference(currentPage);
		ENTPCaseDetail ctlr = new ENTPCaseDetail();
		String testInput = 'test';
		System.assertEquals(testInput, 'test');
		Attachment attachedFile = ctlr.attachedFile;
		case testcase = new case(Subject = 'Test Case', Status = 'New');
		insert testcase;
		System.currentPageReference().getParameters().put('caseNumber', testcase.caseNumber);
		ctlr = new ENTPCaseDetail();
		User currentUser = [SELECT Id, AccountId, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()];
		//system.assert(String.isNotEmpty(currentUser.AccountId));
		List<account> Accounts = [SELECT Strategic__c, ID from Account where ID = :userAccountId];
		//system.assert(String.isNotEmpty(currentUser.LanguageLocaleKey));
		String CustRole = 'tps';
		List<ENTP_portal_additional_functions__c> ENTPPortalMap = [Select Name,Customer_Portal_Role__c,Additional_Function__c from ENTP_portal_additional_functions__c where Customer_Portal_Role__c = :CustRole];
		//system.assert(String.isNotEmpty(ENTPPortalMap[0].Additional_Function__c));
		string CatListPull = ENTPPortalMap[0].Additional_Function__c;
		//System.assertEquals(CatListPull, CatListPull);
		//system.assert(String.isNotEmpty(currentUser.AccountId));

		// Cover various minor functions / variables
		String other = ctlr.getJSStart();
		other = ctlr.getJSStartC();
		other = ctlr.getJSEnd();
		other = ctlr.getJSEndC();
		other = ctlr.removeDiacritics(null);
		other = ctlr.getCaseStatus();
	}*/

	@isTest(SeeAllData=false)
	static void authCheckController() {
		System.debug('MBRTestUtils.createPortalUser()?');
		User u = MBRTestUtils.createENTPPortalUser('test-user-1', false, 'en_US');

		Test.setCurrentPage(Page.ENTPLogin);
		System.runAs(u) {
			ENTPAuthCheckCtlr ctlr = new ENTPAuthCheckCtlr();
			String language = Portal_AuthCheckCtlr.userLanguage;
			Portal_AuthCheckCtlr.switchLanguage();
			if (ctlr.initCheck() != null) {
				return;
			}

			String c = ctlr.getLanguageLocaleKey();
			String s = ctlr.getTypeofCust();

			String userAccountId = [SELECT AccountId FROM User WHERE Id = :UserInfo.getUserId()].AccountId;
			system.assert(String.isNotEmpty(userAccountId));
			List<account> Accounts = [SELECT Strategic__c, ID from Account where ID = :userAccountId];
			String userLanguage = [SELECT LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()].LanguageLocaleKey;
			system.assert(String.isNotEmpty(userLanguage));
			String CustRole = 'tps';
			String SFDCAccountId = 'test';
			System.assertEquals(SFDCAccountId, 'test');
/*****            
            List<ENTP_portal_additional_functions__c> ENTPPortalMap = [Select Name,Customer_Portal_Role__c,Additional_Function__c from ENTP_portal_additional_functions__c where Customer_Portal_Role__c =: CustRole];
            system.assert(String.isNotEmpty(ENTPPortalMap[0].Additional_Function__c));
            String AddFuncPull1 = 'test';
            System.assertEquals( AddFuncPull1, 'test');
            string CatListPull = ENTPPortalMap[0].Additional_Function__c;
            System.assertEquals( CatListPull, CatListPull);
            List<User> users = [SELECT Id, AccountId, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()];
            system.assert(String.isNotEmpty(users.get(0).AccountId));
*/
		}
		u = MBRTestUtils.createENTPPortalUser('test-user-2', true, 'en_US');
		System.runAs(u) {
			Test.setCurrentPage(Page.ENTPLogin);
			ENTPAuthCheckCtlr ctlr = new ENTPAuthCheckCtlr();
			if (ctlr.initCheck() != null) {
				return;
			}

			String c = ctlr.getLanguageLocaleKey();
			String s = ctlr.getTypeofCust();

			String userAccountId = [SELECT AccountId FROM User WHERE Id = :UserInfo.getUserId()].AccountId;
			system.assert(String.isNotEmpty(userAccountId));
			List<account> Accounts = [SELECT Strategic__c, ID from Account where ID = :userAccountId];
			String userLanguage = [SELECT LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()].LanguageLocaleKey;
			system.assert(String.isNotEmpty(userLanguage));
			String CustRole = 'tps';
			String SFDCAccountId = 'test';
			System.assertEquals(SFDCAccountId, 'test');
/*            
            List<ENTP_portal_additional_functions__c> ENTPPortalMap = [Select Name,Customer_Portal_Role__c,Additional_Function__c from ENTP_portal_additional_functions__c where Customer_Portal_Role__c =: CustRole];
            system.assert(String.isNotEmpty(ENTPPortalMap[0].Additional_Function__c));
            String AddFuncPull1 = 'test';
            System.assertEquals( AddFuncPull1, 'test');
            string CatListPull = ENTPPortalMap[0].Additional_Function__c;
            System.assertEquals( CatListPull, CatListPull);
            List<User> users = [SELECT Id, AccountId, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()];
            system.assert(String.isNotEmpty(users.get(0).AccountId));
*/
		}

		u = MBRTestUtils.createGuestUser();

		System.debug('MBRTestUtils.createGuestPortalUser(): ' + u);

		System.runAs(u) {
			Test.setCurrentPage(Page.ENTPLogin);
			ENTPAuthCheckCtlr ctlr = new ENTPAuthCheckCtlr();
			if (ctlr.initCheck() != null) {
				return;
			}

			String c = ctlr.getLanguageLocaleKey();
			String s = ctlr.getTypeofCust();

			String userAccountId = [SELECT AccountId FROM User WHERE Id = :UserInfo.getUserId()].AccountId;
			system.assert(String.isNotEmpty(userAccountId));
			List<account> Accounts = [SELECT Strategic__c, ID from Account where ID = :userAccountId];
			String userLanguage = [SELECT LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()].LanguageLocaleKey;
			system.assert(String.isNotEmpty(userLanguage));
			String CustRole = 'tps';
			String SFDCAccountId = 'test';
			System.assertEquals(SFDCAccountId, 'test');
/*            
            List<ENTP_portal_additional_functions__c> ENTPPortalMap = [Select Name,Customer_Portal_Role__c,Additional_Function__c from ENTP_portal_additional_functions__c where Customer_Portal_Role__c =: CustRole];
            system.assert(String.isNotEmpty(ENTPPortalMap[0].Additional_Function__c));
            String AddFuncPull1 = 'test';
            System.assertEquals( AddFuncPull1, 'test');
            string CatListPull = ENTPPortalMap[0].Additional_Function__c;
            System.assertEquals( CatListPull, CatListPull);
            List<User> users = [SELECT Id, AccountId, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()];
            system.assert(String.isNotEmpty(users.get(0).AccountId));
*/
		}
	}

	@isTest(SeeAllData=false)
	static void Sev1Controller() {
		User u = MBRTestUtils.createENTPPortalUser('test-user-1', false, 'en_US');

		System.runAs(u) {
			ENTPSev1Ctlr ctlr = new ENTPSev1Ctlr();

			ctlr.setCountry('YES');
			String country = ctlr.getCountry();
			// String SelectChoice = 'YES';
			pageReference page = ctlr.test();

			List<SelectOption> cs = ctlr.getItems();

			ENTPSev1Ctlr ctlr2 = new ENTPSev1Ctlr();
			ctlr2.setCountry('NO');
			ctlr2.getCountry();
			// String SelectChoice = 'YES';
			ctlr2.test();

			ctlr2.getItems();
			/*
		PageReference pageRef = System.Page.ENTPNewCase;
		Test.setCurrentPage(pageRef);*/
		}
	}
	/* removed and put in ENTPNavCtlr_test
	@isTest(SeeAllData=false) static void NavController() {
		ENTP_portal_additional_functions__c paf = new ENTP_portal_additional_functions__c(Name = 'tps',Customer_Portal_Role__c = 'tps',Additional_Function__c = 'Severity 1');
		insert paf;
		User u = MBRTestUtils.createENTPPortalUser('test-user-1', false, 'en_US');
		Test.setCurrentPage(Page.ENTPUserTemplate);
		System.runAs(u) {
			ENTPNavCtlr ctlr = new ENTPNavCtlr();
			ctlr.combinedStrings = null;
			String[] sResult = ctlr.strings;
			ctlr.combinedStrings = 'a, b';
			sResult = ctlr.strings;

			String userAccountId = [SELECT AccountId FROM User WHERE Id = :UserInfo.getUserId()].AccountId;
			system.assert(String.isNotEmpty(userAccountId));
			List<account> Accounts = [SELECT Strategic__c, ID from Account where ID =:userAccountId];
			String userLanguage = [SELECT LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()].LanguageLocaleKey;
			system.assert(String.isNotEmpty(userLanguage));
			String CustRole = 'tps';
			String SFDCAccountId='test';
			System.assertEquals( SFDCAccountId, 'test');
			String CatListPull1='test';
			System.assertEquals( CatListPull1, 'test');
/*
			List<ENTP_portal_additional_functions__c> ENTPPortalMap = [Select Name,Customer_Portal_Role__c,Additional_Function__c from ENTP_portal_additional_functions__c where Customer_Portal_Role__c =: CustRole];
			system.assert(String.isNotEmpty(ENTPPortalMap[0].Additional_Function__c));
			String AddFuncPull1 = 'test';
			System.assertEquals( AddFuncPull1, 'test');
			string CatListPull = ENTPPortalMap[0].Additional_Function__c;
			System.assertEquals( CatListPull, CatListPull);
			List<User> users = [SELECT Id, AccountId, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()];
			system.assert(String.isNotEmpty(users.get(0).AccountId));
*/
	/*  }
  }*/

	@isTest(SeeAllData=false) static void LoginRedirectController() {
		ENTPLoginRedirectCtlr ctlr = new ENTPLoginRedirectCtlr();
		String o = ctlr.getOrgId();
	}

	@isTest(SeeAllData=false) static void ENTPAccounTContactUpdate() {
		User u = MBRTestUtils.createENTPPortalUser('test-user-1', false, 'en_US');
		System.runAs(u) {
			ENTPAccounTContactUpdate.UpdateAccCont('1234', '1234', '1234');
		}
	}

	/* Rob removed, causing issues with other tests, Dane/Ryan have a look at */

	@isTest(SeeAllData=false) static void ENTPGetHelpController() {
		List<CCIGetHelp__c> questions = ENTPTestUtils.createHelpQuestions();

		System.debug('questions: ' + questions);
		Test.setCurrentPage(Page.ENTPGetHelpAnswer);

		User u = MBRTestUtils.createENTPPortalUser('test-user-1', false, 'en_US');
		System.runAs(u) {
			System.currentPageReference().getParameters().put('q', questions[1].id);
			ENTPGetHelpController ctlr = new ENTPGetHelpController();
			System.currentPageReference().getParameters().put('questionNumber', questions[0].id);
			ctlr.getCategories();
			ctlr.getQuestions();
			VITILcareGetHelpController ctlr2 = new VITILcareGetHelpController();
			// clear question id no that "String.isEmpty(questionId)" scenario gets covered
			ctlr.questionId = '';
			ctlr.getQuestions();
			CCIGetHelp__c selectedQuestion = ctlr.selectedQuestion;

			AggregateResult[] results = ctlr.getResults();
			ctlr.setResults(results);
			String catname = ctlr.CatName;
			Map<Id, CCIGetHelp__c> questionList = ctlr.QuestionsList;
			String pageError = ctlr.pageError;
		}
	}

	@isTest(SeeAllData=false) static void ENTPGetHelpControllerFR() {
		List<CCIGetHelp__c> questions = ENTPTestUtils.createHelpQuestions();

		System.debug('questions: ' + questions);
		Test.setCurrentPage(Page.ENTPGetHelpAnswer);

		User u = MBRTestUtils.createENTPPortalUser('test-user-1', false, 'fr');
		System.runAs(u) {
			System.currentPageReference().getParameters().put('q', questions[1].id);
			ENTPGetHelpController ctlr = new ENTPGetHelpController();
			System.currentPageReference().getParameters().put('questionNumber', questions[0].id);
			ctlr.getCategories();
			ctlr.getQuestions();
			VITILcareGetHelpController ctlr2 = new VITILcareGetHelpController();
			// clear question id no that "String.isEmpty(questionId)" scenario gets covered
			ctlr.questionId = '';
			ctlr.getQuestions();
			CCIGetHelp__c selectedQuestion = ctlr.selectedQuestion;

			AggregateResult[] results = ctlr.getResults();
			ctlr.setResults(results);
			String catname = ctlr.CatName;
			Map<Id, CCIGetHelp__c> questionList = ctlr.QuestionsList;
			String pageError = ctlr.pageError;
		}
	}
	/*Rob - replace when testing ENTPManualTicketHelper, must fix, missing Lynx ticket Number and case number-errors out*/
	@isTest(SeeAllData=false) static void ENTPManualTicketHelper() {
		User u = MBRTestUtils.createENTPPortalUser('test-user-1', false, 'en_US');
		Contact c = [SELECT Id FROM Contact WHERE Remedy_PIN__c != NULL LIMIT 1];
		insert new SMBCare_WebServices__c(
				Name = 'ENDPOINT_Remedy_View',
				Value__c = 'Test Endpoint'
		);

		List<SMBCare_WebServices__c> customSettings = new List<SMBCare_WebServices__c>{
				new SMBCare_WebServices__c(Name = 'username_pc', Value__c = 'Test'),
				new SMBCare_WebServices__c(Name = 'password', Value__c = '1234')
		};
		insert customSettings;

		Test.setMock(WebServiceMock.class, new trac_svcMocks.ValidateMockSuccess());

		List<Case> caseList = new List<Case>();
		Case lynxCase = new Case(
				ContactId = c.Id,
				Subject = 'test lynx',
				Description = 'test lynx',
				Lynx_Ticket_Number__c = '1234',
				Ticketing_System__c = 'Lynx',
				My_Business_Requests_Type__c = 'Mainstream',
				Origin = 'ENTP',
				RecordTypeId = '01240000000DweCAAS',
				Customer_First_Name_contact__c = 'Test'
		);
		caseList.add(lynxCase);

		Case wlnRemedyCase = new Case(
				ContactId = c.Id,
				Subject = 'test remedy',
				Description = 'test remedy',
				Lynx_Ticket_Number__c = '1234',
				Ticketing_System__c = 'WLN Remedy',
				My_Business_Requests_Type__c = 'Mainstream',
				Origin = 'ENTP',
				RecordTypeId = '01240000000DweCAAS',
				Customer_First_Name_contact__c = 'Test'
		);
		caseList.add(wlnRemedyCase);

		insert caseList;
		Map<Id, Case> updatedCaseList = new Map<Id, Case>([SELECT Id, CaseNumber, Lynx_Ticket_Number__c FROM Case WHERE Id IN :caseList]);

		List<Ticket_Event__c> ticketEventList = new List<Ticket_Event__c>();
		Ticket_Event__c lynxTicket = new Ticket_Event__c(
				Case_Id__c = lynxCase.Id,
				Case_Number__c = updatedCaseList.get(lynxCase.Id).CaseNumber,
				Lynx_Ticket_Number__c = '1234'
		);
		ticketEventList.add(lynxTicket);

		Ticket_Event__c wlnRemedyTicket = new Ticket_Event__c(
				Case_Id__c = wlnRemedyCase.Id,
				Case_Number__c = updatedCaseList.get(wlnRemedyCase.Id).CaseNumber,
				Lynx_Ticket_Number__c = '1234'
		);
		ticketEventList.add(wlnRemedyTicket);

		insert ticketEventList;

		Ticket_Event__c[] all = [select Case_Number__c, Case_Id__c, Remedy_PIN__c, Lynx_Ticket_Number__c, Id, Case_id__r.Ticketing_System__c from Ticket_Event__c];
		System.debug('ENTPManualTicketHelper.ENTPAddToManual,ALL???' + all);

		ENTPManualTicketHelper.ENTPAddToManual(lynxTicket.id);

		ENTPManualTicketHelper.ENTPAddToManual(wlnRemedyTicket.id);

	}

	/* removed and put into new file - ENTPVPNDownload_test
	   @isTest(SeeAllData=false) static void ENTPVPNDownload() {
		   User u = MBRTestUtils.createENTPPortalUser('test-user-1', false, 'en_US');

		   System.runAs(u) {
			   ENTPVPNDownload ctlr = new ENTPVPNDownload();
			   ctlr.toggleContent();
			   ctlr.toggleButton();
			   ctlr.dlList();
		   }
	   }*/
	@isTest(SeeAllData=false) static void ENTPTestUtils() {
		User u = MBRTestUtils.createENTPPortalUser('test-user-1', false, 'en_US');

		System.runAs(u) {
			//ENTPTestUtils ctlr = new ENTPTestUtils();
			ENTPTestUtils.createENTPCustomSetting();
			ENTPTestUtils.createSMBCareWebServicesCustomSetting();
			ENTPTestUtils.createENTPCases(1, u.AccountId);
			Case parentCase = [SELECT Id, CaseNumber, My_Business_Requests_Type__c, LastModifiedDate FROM Case ORDER BY CreatedDate DESC LIMIT 1];
			ENTPTestUtils.createEmail(parentCase.Id, true, 'Testing');
			ENTPTestUtils.createAttachment(parentCase.Id, 1, 'Testing', parentCase.LastModifiedDate);
			ENTPTestUtils.createComment(parentCase.Id, true, true);
			ENTPTestUtils.createComment(parentCase.Id, false, true);
		}
	}
	@isTest(SeeAllData=false) static void ENTPTriggerFiredClass() {
		User u = MBRTestUtils.createENTPPortalUser('test-user-1', false, 'en_US');

		System.runAs(u) {
			ENTPTriggerFiredClass.hasAlreadyDone();
			ENTPTriggerFiredClass.setAlreadyDone();
		}
	}
}