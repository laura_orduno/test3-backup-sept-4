global class OCOM_AddTelusConnectivityHelper{
    
   public static Boolean runPost = true;
   public static Boolean preInvoke = true;
   public static Boolean allowOfficeInternet = true;
   public static Boolean deteleTelusCon = false;
   static List<OrderItem> orderToUpdate = new List<OrderItem>();
   global static Map<Id, String> telusIdParentName = new Map<Id, String>();

   public static void deleteUndelyingRec(String orderItemId, String OrderId){
       Map<String, OCOM_AddTelusConnectivity__c> parsedCustStngAddTelus = new Map<String, OCOM_AddTelusConnectivity__c>();
        List<Id> deletTelusConIds = new List<Id>();
        List<OrderItem> oriToDeleteList = new List<OrderItem>();
        for(string custName : OCOM_AddTelusConnectivity__c.getAll().keySet()){
            String key = OCOM_AddTelusConnectivity__c.getValues(custName).Name+OCOM_AddTelusConnectivity__c.getValues(custName).External_Id__c;
            parsedCustStngAddTelus.put(key, OCOM_AddTelusConnectivity__c.getValues(custName));
        }
       
       orderItem topOrItem = [SELECT Id,vlocity_cmt__LineNumber__c, vlocity_cmt__JSONAttribute__c, vlocity_cmt__Product2Id__r.orderMgmtId__c, vlocity_cmt__Product2Id__r.Name from orderItem where Id =: orderItemId LIMIT 1];
       String key = topOrItem.vlocity_cmt__Product2Id__r.Name+topOrItem.vlocity_cmt__Product2Id__r.orderMgmtId__c;
       System.debug('NameExtIDKey_______'+key);
       if(parsedCustStngAddTelus != null && parsedCustStngAddTelus.containsKey(key)){
           if(!topOrItem.vlocity_cmt__LineNumber__c.contains('.')){
               for (orderItem OrItem : [SELECT Id,vlocity_cmt__LineNumber__c, vlocity_cmt__JSONAttribute__c, vlocity_cmt__Product2Id__r.orderMgmtId__c, vlocity_cmt__Product2Id__r.Name from orderItem where OrderId =: orderId AND vlocity_cmt__LineNumber__c LIKE : topOrItem.vlocity_cmt__LineNumber__c+'%']){
                    if(OrItem.vlocity_cmt__JSONAttribute__c != null){
                        Map<String,OCOM_VlocityJSONParseUtil.JSONWrapper> attNameValueMap = OCOM_VlocityJSONParseUtil.getAttributesFromJSON(OrItem.vlocity_cmt__JSONAttribute__c);
                        if(attNameValueMap.containsKey('Underlying Connectivity ID'))
                            if(attNameValueMap.get('Underlying Connectivity ID').value !=null && attNameValueMap.get('Underlying Connectivity ID').value !=''){
                                string oriId = attNameValueMap.get('Underlying Connectivity ID').value;
                                OrderItem oriToDelete = new OrderItem(id = oriId);
                                oriToDeleteList.add(oriToDelete);
                                deletTelusConIds.add(Id.valueOf(attNameValueMap.get('Underlying Connectivity ID').value));
                                
                            }
                    }
               }
           }
           else{
               if(topOrItem.vlocity_cmt__JSONAttribute__c != null){
                   Map<String,OCOM_VlocityJSONParseUtil.JSONWrapper> attNameValueMap = OCOM_VlocityJSONParseUtil.getAttributesFromJSON(topOrItem.vlocity_cmt__JSONAttribute__c);
                            if(attNameValueMap.containsKey('Underlying Connectivity ID') && attNameValueMap.get('Underlying Connectivity ID').value !=null && attNameValueMap.get('Underlying Connectivity ID').value !=''){
                                string oriId = attNameValueMap.get('Underlying Connectivity ID').value;
                                OrderItem oriToDelete = new OrderItem(id = oriId);
                                oriToDeleteList.add(oriToDelete);
                                deletTelusConIds.add(Id.valueOf(attNameValueMap.get('Underlying Connectivity ID').value));
                            }
               }
           }
           system.debug('deletTelusConIds_____'+ deletTelusConIds);
           if(oriToDeleteList.size() > 0 && !oriToDeleteList.isEmpty())
               delete oriToDeleteList;
       }
       else{
       //orderItem OrItem : [SELECT Id,vlocity_cmt__JSONAttribute__c from orderItem where Id =: orderItemId LIMIT 1];
       for (orderItem OrItem : [SELECT Id,vlocity_cmt__JSONAttribute__c, vlocity_cmt__Product2Id__r.orderMgmtId__c, vlocity_cmt__Product2Id__r.Name from orderItem where OrderId =: orderId]){
           system.debug('===='+ OrderId +'=='+orderItemId);
                          system.debug('orderItem JSAON Attr_______'+OrItem.vlocity_cmt__JSONAttribute__c );
           if(OrItem.vlocity_cmt__JSONAttribute__c != null){
              
               Map<String,OCOM_VlocityJSONParseUtil.JSONWrapper> attNameValueMap = OCOM_VlocityJSONParseUtil.getAttributesFromJSON(OrItem.vlocity_cmt__JSONAttribute__c);
               if(attNameValueMap.containsKey('Underlying Connectivity ID'))
                   if(attNameValueMap.get('Underlying Connectivity ID').value !=null && attNameValueMap.get('Underlying Connectivity ID').value !=''){
                       system.debug('----'+ orderItemId+'--'+attNameValueMap.get('Underlying Connectivity ID').value);
                       
                       if(attNameValueMap.get('Underlying Connectivity ID').value == orderItemId){
                           
                           OrItem.vlocity_cmt__JSONAttribute__c = OCOM_VlocityJSONParseUtil.setAttributesInJSON(OrItem.vlocity_cmt__JSONAttribute__c, new Map<String,String>{'Underlying Connectivity ID'=> ''});
                            orderToUpdate.add(OrItem);
                       }
                       
                   }
           }
           
       }
       
       system.debug('OOOO'+ orderToUpdate);
       /*
       Map<String,OCOM_VlocityJSONParseUtil.JSONWrapper> attNameValueMap = OCOM_VlocityJSONParseUtil.getAttributesFromJSON(OrItem.vlocity_cmt__JSONAttribute__c);
           if(attNameValueMap.get('Underlying Connectivity ID').value !='' && attNameValueMap.get('Underlying Connectivity ID').value !=null){
              OrItem.vlocity_cmt__JSONAttribute__c = OCOM_VlocityJSONParseUtil.setAttributesInJSON(OrItem.vlocity_cmt__JSONAttribute__c, new Map<String,String>{'Underlying Connectivity ID'=> ''});
               orderToUpdate.add(OrItem);
           }
       
       List<Id> underlyinghIds = new List<Id>();
       for (orderItem OrItem : [SELECT vlocity_cmt__JSONAttribute__c from orderItem where OrderId =: orderId]){
           system.debug('===='+OrItem);
           Map<String,OCOM_VlocityJSONParseUtil.JSONWrapper> attNameValueMap = OCOM_VlocityJSONParseUtil.getAttributesFromJSON(OrItem.vlocity_cmt__JSONAttribute__c);
           if(attNameValueMap.get('Underlying Connectivity ID').value !='' && attNameValueMap.get('Underlying Connectivity ID').value !=null){
               underlyinghIds.add(Id.valueOf(attNameValueMap.get('Underlying Connectivity ID').value));
               OrItem.vlocity_cmt__JSONAttribute__c = OCOM_VlocityJSONParseUtil.setAttributesInJSON(OrItem.vlocity_cmt__JSONAttribute__c, new Map<String,String>{'Underlying Connectivity ID'=> ''});
               orderToUpdate.add(OrItem);
           }
       }
       
       if(underlyinghIds.size() > 0)
           delete [SELECT Id from OrderITem where Id IN : underlyinghIds];
       */
       if(orderToUpdate.size() > 0)
           update orderToUpdate;
       }
       
   }

    global static void addTelusConnectivity(String orderId, String OrderItemId,Map<String,Object>inputItemInfoMap, Boolean isPostCartItemCall){
       String updatedProdExtId;
       priceBookENtry addOnProd;
       String lastLineNumber = null;
       String eligibleOriId ;
       Boolean isPublicWifiOffer = false;
       Map<String, OrderItem> telUsPubWifiOfferMap = new Map<String,OrderItem>();
       Map<String,OrderItem> autoAddOriMap = new Map<String,OrderItem>();
       Map<String, OCOM_AddTelusConnectivity__c> parsedCustStngAddTelus = new Map<String, OCOM_AddTelusConnectivity__c>();
       List<OrderItem> oriToDeleteList = new List<OrderItem>();

       // Get the all the records from OCOM_AddTelusConnectivity__c Custom Setting
        for(string custName : OCOM_AddTelusConnectivity__c.getAll().keySet()){
            String key = OCOM_AddTelusConnectivity__c.getValues(custName).Name+OCOM_AddTelusConnectivity__c.getValues(custName).External_Id__c;
            parsedCustStngAddTelus.put(key, OCOM_AddTelusConnectivity__c.getValues(custName));
        }
        // Get the Updated Item Info
        if(isPostCartItemCall == false && inputItemInfoMap != null && inputItemInfoMap.containskey('updatedRecordProdName') &&  inputItemInfoMap.containskey('updatedRecordProdExtId')){
              updatedProdExtId = (String)inputItemInfoMap.get('updatedRecordProdName') +  (String)inputItemInfoMap.get('updatedRecordProdExtId');
              System.debug('**** updatedProdExtId***' +updatedProdExtId);
        } else if(isPostCartItemCall == true && inputItemInfoMap != null && inputItemInfoMap.containskey('topOfferMap') &&  inputItemInfoMap.containskey('topOfferMap')){
              Map<String,OrderItem> topOfferMap = (Map<String,OrderItem>)inputItemInfoMap.get('topOfferMap');
              for(OrderItem topOffer: topOfferMap.values()){
                      updatedProdExtId = topOffer.vlocity_cmt__Product2Id__r.Name + topOffer.vlocity_cmt__Product2Id__r.orderMgmtId__c;
                      if(parsedCustStngAddTelus != null && parsedCustStngAddTelus.containsKey(updatedProdExtId)){
                          System.debug('**** updatedProdExtId***' +updatedProdExtId);
                          break;
                        }
            }
        }

      // Check if the currently modified Item is mapped in Custom Setting.    
       if(updatedProdExtId != null && parsedCustStngAddTelus != null && parsedCustStngAddTelus.containsKey(updatedProdExtId)){ 
           OCOM_AddTelusConnectivity__c autoAddCustStngRecord = parsedCustStngAddTelus.get(updatedProdExtId);
           String autoAddOfferName = autoAddCustStngRecord.AutoAdd_Offer_Name__c;
           addOnProd = [SELECT Id,unitPrice,product2.vlocity_cmt__JSONAttribute__c FROM priceBookENtry WHERE  product2.Name = :autoAddOfferName AND priceBook2.Name = 'OCOM PriceBook' LIMIT 1];      
        
          system.debug('ORDERID'+orderId);
        
          List<Order> orderList = [SELECT Service_Address__r.Maximum_Speed__c , Maximum_Speed__c , (SELECT id,vlocity_cmt__LineNumber__c, vlocity_cmt__Product2Id__r.orderMgmtId__c, vlocity_cmt__Product2Id__r.Name,vlocity_cmt__JSONAttribute__c from orderItems  ORDER BY CreatedDate DESC)FROM Order WHERE ID =: orderId LIMIT 1];
          if(orderList != null && orderList.size()> 0){
              Order ord = orderList[0];
                 if(ord.Service_Address__r.Maximum_Speed__c != null && ord.Service_Address__r.Maximum_Speed__c != ''){
                         Integer  maxSpeed = getSpeed(ord.Service_Address__r.Maximum_Speed__c);
                       system.debug('Max Speed ______'+ maxSpeed);
                      // Check if the MAX speed of the Order is < 50MBPS
                        if( ord.orderItems != null && (maxSpeed == null || maxSpeed <= 50 )){
                            for(OrderItem ori:  ord.orderItems){
                                    if(lastLineNumber == null && ori.vlocity_cmt__LineNumber__c != null && ! String.valueOf(ori.vlocity_cmt__LineNumber__c).Contains('.')){
                                     lastLineNumber = ori.vlocity_cmt__LineNumber__c;
                                     Integer temp =Integer.valueOf(lastLineNumber);
                                     temp++;
                                     lastLineNumber = '000'+String.valueOf(temp);
                                    }
                                }
                             for(OrderItem ori:  ord.orderItems){
                                 Boolean isInsertTelCon = false;
                                 String key = ori.vlocity_cmt__Product2Id__r.Name+ori.vlocity_cmt__Product2Id__r.orderMgmtId__c;
                                  if(key != null && parsedCustStngAddTelus.get(key) != null){
                                     OCOM_AddTelusConnectivity__c tempCustSt = parsedCustStngAddTelus.get(key);
                                     Map<String,OCOM_VlocityJSONParseUtil.JSONWrapper> attNameValueMap = OCOM_VlocityJSONParseUtil.getAttributesFromJSON(ori.vlocity_cmt__JSONAttribute__c);
                                         
                                     if(tempCustSt.Is_Condition__c){
                                         string s1 = attNameValueMap.get(tempCustSt.Attribute_Name__c).value;
                                         string s2 = tempCustSt.Attribute_Value__c;
                                         // Set Boolean for Auto Add Telus Connectivity
                                         if(attNameValueMap.get(tempCustSt.Attribute_Name__c).value != null && attNameValueMap.get(tempCustSt.Attribute_Name__c).value.compareTo(tempCustSt.Attribute_Value__c)==0 && (attNameValueMap.get('Underlying Connectivity ID').value ==''|| attNameValueMap.get('Underlying Connectivity ID').value ==null)){
                                             system.debug('Auto Add Telus Connectivity Boolean set___');
                                             isInsertTelCon = true;
                                             autoAddOriMap.put(ori.Id,ori);
                                             eligibleOriId = ori.id;
                                         }else{
                                            // Here removing Telus con if Line setup is Dial tone, i.e, if the Setup value is otherthan ADLS No Local
                                           system.debug('DELETINGGGGG'+ deteleTelusCon);
                                            if((attNameValueMap.get('Underlying Connectivity ID').value != '' || attNameValueMap.get('Underlying Connectivity ID').value!= null) && deteleTelusCon){
                                              String oriId = attNameValueMap.get('Underlying Connectivity ID').value;
                                              oriToDeleteList.add( new OrderItem(id = oriId));
                                               // delete [SELECT Id FROM OrderItem Where id=: Id.valueOf(attNameValueMap.get('Underlying Connectivity ID').value)];
                                                ori.vlocity_cmt__JSONAttribute__c = OCOM_VlocityJSONParseUtil.setAttributesInJSON(ori.vlocity_cmt__JSONAttribute__c, new Map<String,String>{'Underlying Connectivity ID'=> ''});
                                                orderToUpdate.add(ori);
                                            }
                                            
                                        }
                                     }
                                     else{
                                         
                                         if( attNameValueMap.get('Underlying Connectivity ID').value ==''|| attNameValueMap.get('Underlying Connectivity ID').value ==null){
                                             system.debug('TELUS PUBLIC WIFI___Auto Add Telus Connectivity Boolean set___');
                                             isInsertTelCon = true;
                                             if(ori.vlocity_cmt__Product2Id__r.Name.compareTo('TELUS Public Wi-Fi') == 0 ){
                                                isPublicWifiOffer = true;
                                                telUsPubWifiOfferMap.put(ori.id, ori);
                                             }
                                         }
                                     }
                                 }
                            }/// END of FOR LOOP.
                           
                        }// max speed cond end
                  }
            }

      // Logic to Insert Telus Connectivity for NON TELUS Public WIFI offers
          if(  allowOfficeInternet && autoAddOriMap != null && eligibleOriId != null && autoAddOriMap.Containskey(eligibleOriId) ){
                //system.debug('IIIIII'+ori.vlocity_cmt__Product2Id__r.orderMgmtId__c);
                runPost = false;
                preInvoke = false;
                orderItem parentOri = autoAddOriMap.get(eligibleOriId);
                 System.debug('TTTT'+lastLineNumber);
                 List<OrderItem> parentOriList = insertTelusCon(addOnProd,OrderId, lastLineNumber, parentOri);
                 if(parentOriList != null && parentOriList.size() > 0 )
                     orderToUpdate.addAll( parentOriList);
                 //if(insertOutputMap != null && insertOutputMap.size() > 0 && !insertOutputMap.isEmpty()){
                   // telusIdParentName = insertOutputMap.get('telusIdParentName');
                 //}
               }

         // Logic to Insert TELUS Connectivity for TELUS Public WIFI
           if(isPublicWifiOffer == true && telUsPubWifiOfferMap != null && telUsPubWifiOfferMap.size() > 0 && !telUsPubWifiOfferMap.isEmpty()){
               orderItem parentOri = telUsPubWifiOfferMap.values()[0];
               system.debug('parentOri____' + parentOri);
                 List<OrderItem> parentOriList2 = insertTelusCon(addOnProd,OrderId, lastLineNumber, parentOri);
                 if(parentOriList2 != null && parentOriList2.size() > 0 )
                      orderToUpdate.addAll( parentOriList2);
                      System.debug('parentOriList'+parentOriList2.size());
           }
  
            if(telusIdParentName != null && telusIdParentName.size() > 0 && !telusIdParentName.isEmpty() ){
                  List<OrderItem> telusConOriList =  updateTelusConAttrs(telusIdParentName);
                  if(telusConOriList != null && telusConOriList.size() > 0 )
                      orderToUpdate.addAll( telusConOriList);
                  system.debug('----VV'+telusIdParentName);
              }
            for(OrderItem ori: orderToUpdate){
              system.debug('orderItemto Update___' +ori.Id);
            }

            if(orderToUpdate.size() > 0 && !orderToUpdate.isEmpty() )
                update orderToUpdate;

            if(oriToDeleteList.size() > 0 && !oriToDeleteList.isEmpty() )
                delete oriToDeleteList;    
         }
    }

// Method to Auto Add TelusConnectivty and Update its ID on parent OLI

 private static  List<OrderItem> insertTelusCon(priceBookENtry addOnProd, String OrderId, String lastLineNumber, OrderItem parentOri){
    List<OrderItem> orderItemToUpdate = new List<OrderItem>();
   // Map<String,Object> insertTelusConOutput = new Map<String,Object>();
      try{
          if(parentOri != null ){
                OrderItem  orderItem1 = new OrderItem(vlocity_cmt__Product2Id__c = addOnProd.product2.Id, vlocity_cmt__ProductHierarchyPath__c = addOnProd.product2.Id, vlocity_cmt__ProvisioningStatus__c = 'New', OrderId= OrderId, PricebookEntryId = addOnProd.Id, Quantity = 1, UnitPrice= addOnProd.unitPrice,vlocity_cmt__LineNumber__c = lastLineNumber);
                 orderItem1.vlocity_cmt__JSONAttribute__c = addOnProd.product2.vlocity_cmt__JSONAttribute__c;
                 system.debug('Beforeeee+'+orderItem1);
                 insert orderItem1;
                 if(orderItem1 != null && orderItem1.id != null){
                   
                   telusIdParentName.put(orderItem1.Id, parentOri.vlocity_cmt__Product2Id__r.Name);
                   parentOri.vlocity_cmt__JSONAttribute__c = OCOM_VlocityJSONParseUtil.setAttributesInJSON(parentOri.vlocity_cmt__JSONAttribute__c, new Map<String,String>{'Underlying Connectivity ID'=> orderItem1.Id});
                   system.debug('afterrrr+'+parentOri.vlocity_cmt__JSONAttribute__c);
                   
                   orderItemToUpdate.add(parentOri);
                 }
          }
          }catch(exception e){
                System.debug(LoggingLevel.ERROR, 'Exception is '+e);
                System.debug(LoggingLevel.ERROR, 'Exception stack trace '+e.getStackTraceString());
                return null;
          }
          system.debug('orderItemToUpdate____' + orderItemToUpdate);
          return orderItemToUpdate;

 }

// Method to update Telus Connectivity Manadatory Attributes
  private static List<OrderItem> updateTelusConAttrs(Map<Id,String> telusIdParentNameMap){
      List<OrderItem> orderItemToUpdate2 = new List<OrderItem>();
      try {
        if(telusIdParentNameMap != null && telusIdParentNameMap.size() > 0 && !telusIdParentNameMap.isEmpty() ){
          for(orderItem ordItem : [SELECT Id, vlocity_cmt__JSONAttribute__c from orderItem where Id IN: telusIdParentNameMap.keySet()]){
                 if(telusIdParentName.get(ordItem.Id)=='TELUS Public Wi-Fi'){
                 system.debug('UpdateTelusWifiiiiii'+telusIdParentName.get(ordItem.Id));
                     ordItem.vlocity_cmt__JSONAttribute__c = OCOM_VlocityJSONParseUtil.setAttributesInJSON(ordItem.vlocity_cmt__JSONAttribute__c, new Map<String,String>{'Service Designation'=> 'Public Wi-fi'});
                      ordItem.vlocity_cmt__JSONAttribute__c = OCOM_VlocityJSONParseUtil.setAttributesInJSON(ordItem.vlocity_cmt__JSONAttribute__c, new Map<String,String>{'Technology Type'=> 'Copper'});
                     orderItemToUpdate2.add(ordItem);      
                 }
                // system.debug('----VV22'+telusIdParentName.get(ordItem.Id));
                 else if(telusIdParentName.containskey(ordItem.Id)){
                     ordItem.vlocity_cmt__JSONAttribute__c = OCOM_VlocityJSONParseUtil.setAttributesInJSON(ordItem.vlocity_cmt__JSONAttribute__c, new Map<String,String>{'Service Designation'=> 'Regular Consumer'});
                     system.debug('UUUUUUpdating'+ordItem.vlocity_cmt__JSONAttribute__c);
                     orderItemToUpdate2.add(ordItem);  
                  }
              }
          }
         
      }catch(exception e){
          System.debug(LoggingLevel.ERROR, 'Exception is '+e);
          System.debug(LoggingLevel.ERROR, 'Exception stack trace '+e.getStackTraceString());
          return null;
      }
       system.debug('orderItemToUpdate2____' + orderItemToUpdate2);
       return orderItemToUpdate2;
    }



    public static Integer getSpeed(String maxSpeed){
        String[] stringCharacters = maxSpeed.split('');
        String finalSpeed = '';
        for(String s : stringCharacters){
            if(s.isNumeric())
                finalSpeed = finalSpeed+s;
            else
                break;
        }
        if(finalSpeed != null && finalSpeed != '')
            return Integer.valueOf(finalSpeed);   
        else
            return null;
    }
}