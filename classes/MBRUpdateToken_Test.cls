/**
* @author - Christian Wico : cwico@tractionondemand.com
* @description - Unit test for MBRUpdateToken.trigger
*/
@isTest
private class MBRUpdateToken_Test {
	@isTest static void test() {
		User u = MBRTestUtils.createPortalUser('');
		Contact c = [SELECT Token__c, Token_Encoded__c FROM Contact WHERE Id=:u.ContactId LIMIT 1];
		System.assert(c.Token__c.length() > 0);
		System.assert(c.Token_Encoded__c.length() > 0);
	}
}