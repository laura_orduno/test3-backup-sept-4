//Methods Included: queryResource
// Primary Port Class Name: QueryResourceSOAPBinding	
public class OrdrTnResQryInvOperation {
	public class QueryResourceSOAPBinding {
		public String endpoint_x = 'https://soa-mp-toll-pt.tsl.telus.com:443/RMO/InventoryMgmt/ResourceQueryService_v1_1_vs0';
		public Map<String,String> inputHttpHeaders_x;
		public Map<String,String> outputHttpHeaders_x;
		public String clientCertName_x;
		public String clientCert_x;
		public String clientCertPasswd_x;
		public Integer timeout_x;
		private String[] ns_map_type_info = new String[]{'http://www.ibm.com/telecom/common/schema/urban_property_address/v3_0','TpCommonUrbanPropertyAddressV3','http://www.ibm.com/telecom/common/schema/place/v3_0','TpCommonPlaceV3','http://www.ibm.com/telecom/inventory/schema/resource_configuration/v3_0','TpInventoryResourceConfigV3','http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/common/schema/Message','OrdrTnResQryComMsg','http://www.ibm.com/telecom/common/schema/mtosi/v3_0','TpCommonMtosiV3','http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/schema/ResourceConfigurationMessage','OrdrTnResQryResConfMsg','http://www.ibm.com/telecom/common/schema/base/v3_0','TpCommonBaseV3','http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/types/schema/QueryResource','OrdrTnResQryInvOperationType','http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/QueryResource','OrdrTnResQryInvOperation'};

		
		public OrdrTnResQryResConfMsg.ResourceConfigurationMessage queryResource(TpCommonMtosiV3.Header_T header,TpCommonBaseV3.EntityWithSpecification[] sourceCriteria,TpCommonBaseV3.EntityWithSpecification[] targetCriteria) {
		

		TpCommonMessage.LookupRequestMessage request_x = new TpCommonMessage.LookupRequestMessage();
			
			request_x.sourceCriteria = sourceCriteria;
			request_x.targetCriteria = targetCriteria;

			OrdrTnResQryResConfMsg.ResourceConfigurationMessage response_x;
			Map<String, OrdrTnResQryResConfMsg.ResourceConfigurationMessage> response_map_x = new Map<String, OrdrTnResQryResConfMsg.ResourceConfigurationMessage>();
			//Map<String, TpInventoryResourceConfigV3.LogicalResource> response_map_x = new Map<String,TpInventoryResourceConfigV3.LogicalResource>();
			response_map_x.put('response_x', response_x);
			WebServiceCallout.invoke(
				this,
				request_x,
				response_map_x,
				new String[]{endpoint_x,				
				'http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/QueryResource/queryResource',
				'http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/types/schema/QueryResource',
				'queryResource',
				'http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/types/schema/QueryResource',
				'queryResourceResponse',
				'OrdrTnResQryResConfMsg.ResourceConfigurationMessage'}
			);
			response_x = response_map_x.get('response_x');
			return response_x;
		}
	}
}