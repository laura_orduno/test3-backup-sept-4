public without sharing class MBRAccountUtil {

    public transient string CBUCID {get; private set;}
    public transient string RCID {get; private set;}
    public transient List<AccountWithLevel> childAccountsWithLevel {get; private set;}
    public transient Integer levels {get; private set;}
    public transient Set<String> accountNumbers {get; private set;}
    
    public MBRAccountUtil(String rcid) {
        accountNumbers = new Set<String>();
        this.levels = 0;
        this.RCID = rcid;
        accountNumbers.add(rcid.toLowerCase().trim());
        if (this.CBUCID == null) {
            childAccountsWithLevel = new List<AccountWithLevel>();
            List<Account> rcidAccts = [select CBUCID_formula__c, parentid from Account where rcid__c = :RCID];
            if (rcidAccts.size()>0) {
                this.CBUCID = rcidAccts[0].CBUCID_formula__c;
                accountNumbers.add(rcidAccts[0].CBUCID_formula__c.toLowerCase().trim());
                Set<Id> acctIds = new Set<Id>();
                List<Account> chilAccts = [select id, (select BAN__c, BAN_CAN__c, BCAN__c, CAN__c, name, id, RecordType.DeveloperName from ChildAccounts) from Account where id = :rcidAccts[0].parentid];
                for(Account a : chilAccts[0].getSobjects('ChildAccounts')) {
                    acctIds.add(a.id);
                    childAccountsWithLevel.add(new AccountWithLevel(1, a));
                    if(a.ban__c != null && a.ban__c != '') accountNumbers.add(a.ban__c.toLowerCase().trim());
                    if(a.BAN_CAN__c != null && a.BAN_CAN__c != '') accountNumbers.add(a.BAN_CAN__c.toLowerCase().trim());
                    if(a.CAN__c != null && a.CAN__c != '') accountNumbers.add(a.CAN__c.toLowerCase().trim());
                    if(a.BCAN__c != null && a.BCAN__c != '') accountNumbers.add(a.BCAN__c.toLowerCase().trim());                                                            
                }
                if (acctIds.size()>0) {
                    levels = 1;
                    buildHierarchy(acctIds, 2);
                }
            }
        }
    }

    private void buildHierarchy(Set<Id> ids, Integer level) {
        if (level==10) return;
        Set<Id> acctIds = new Set<Id>();
        for (List<Account> childAccts : [select BAN__c, BAN_CAN__c, BCAN__c, CAN__c, name, id, RecordType.DeveloperName from Account where parentid IN :ids]) {
            for (Account a : childAccts) {
                if (this.levels != level) this.levels = level;
                acctIds.add(a.id);
                childAccountsWithLevel.add(new AccountWithLevel(level, a));
                if(a.ban__c != null && a.ban__c != '') accountNumbers.add(a.ban__c.toLowerCase().trim());
                if(a.BAN_CAN__c != null && a.BAN_CAN__c != '') accountNumbers.add(a.BAN_CAN__c.toLowerCase().trim());
                if(a.CAN__c != null && a.CAN__c != '') accountNumbers.add(a.CAN__c.toLowerCase().trim());
                if(a.BCAN__c != null && a.BCAN__c != '') accountNumbers.add(a.BCAN__c.toLowerCase().trim());   
            }
        }
        if (acctIds.size()>0) buildHierarchy(acctIds, level+1);
    }

    public Set<String> getAccountNumbers() {
        return accountNumbers;
    }

    private class AccountWithLevel {
        public Integer level {get; private set;}
        public Account acct {get; private set;}
        
        public AccountWithLevel(Integer level, Account acct) {
            this.level = level;
            this.acct = acct;
        }
        
    }

}