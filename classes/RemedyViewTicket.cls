public class RemedyViewTicket {
	@future(callout=true)
	public static void sendRequest(string pin, string ticketNumber, string caseNumber, Id CaseId, String TicketEventId) {
		// global class sendRequest {

		//  global String OKTicketId = '';
		//  global String DescriptionView1 = '';
		//   global String ClientPhoneView = '';
		//  global String UpdateClientView = '';
		//   public RemedyViewTicket.sendRequest SRequest(string pin, string ticketNumber)
		// public list<Remedy_Trouble_Ticket.TroubleTicket> SRequest(string pin, string ticketNumber)
		// {
		//
		// Create the request envelope
		DOM.Document doc = new DOM.Document();
		//String endpoint = 'http://205.206.192.11/arsys/services/ARService?server=205.206.195.52&webService=ExternalTicketSubmissionCreate';
		// String endpoint = 'https://soa-mp-toll-dv.tsl.telus.com:443/SMO/ProblemMgmt/ExternalTicketCreationService_v1_0_vs0';
		//String endpoint = 'https://xmlgwy-pt1.telus.com:9030/dv/SMO/ProblemMgmt/ExternalTicketViewService_v1_0_vs0';

		// TODO: Re-leverage custom setting for endpoint, no hard-coding
		String endpoint = SMBCare_WebServices__c.getInstance('ENDPOINT_Remedy_View').Value__c;
		//String endpoint = 'https://xmlgwy.telus.com:9030/SMO/ProblemMgmt/ExternalTicketCreationService_v1_0_vs0';
		////////////
		String soapNS = 'http://schemas.xmlsoap.org/soap/envelope/';
		string urn = 'urn:ExternalTicketSubmissionView';
		//string asr = 'http://assurance.ebonding.telus.com';
		datetime creationDateTime = datetime.now();
		date mydate = creationDateTime.date();
		datetime getmytime = datetime.now();
		time mytime = getmytime.time();
		//string username = 'frontline';
		//string password = 'Fline2014';
		string username = 'salesforce';
		string password = 'SFDC2016';
		string assigned_group = 'SPOC FRONTLINE';
		string impact = '4-Minor/Localized';
		string reported_source = 'Self Service';
		String service_type = 'User Service Restoration';
		String status = 'New';
		String action = 'CREATE';
		String urgency = 'Item3';
		string testTime = string.valueOf(mydate) + 'T' + string.valueOf(mytime);
		string createdate = string.valueOfGmt(creationDateTime);
		string logicalId = 'TELUS-SFDC';
		string messageId = logicalId + creationDateTime;
		String encodemessageId = EncodingUtil.base64Encode(Blob.valueOf(messageId));

		/*if (projectNum != '')
{
projectNum = 'CPS' + projectNum;
}*/

		dom.XmlNode envelope
				= doc.createRootElement('Envelope', soapNS, 'soapenv');
		envelope.setNamespace('urn', urn);
		//envelope.setNamespace('asr', asr);

		dom.XmlNode header = envelope.addChildElement('Header', soapNS, null);
		dom.XmlNode AuthenticationInfo = header.addChildElement('AuthenticationInfo', urn, 'urn');

		// dom.XmlNode sender = AuthenticationInfo.addChildElement('sender', ebon, null);
		AuthenticationInfo.addChildElement('userName', urn, null).
				addTextNode(username);
		AuthenticationInfo.addChildElement('password', urn, null).
				addTextNode(password);
		AuthenticationInfo.addChildElement('authentication', urn, null).
				addTextNode('');
		AuthenticationInfo.addChildElement('locale', urn, null).
				addTextNode('');
		AuthenticationInfo.addChildElement('timeZone', urn, null).
				addTextNode('');

		dom.XmlNode body = envelope.addChildElement('Body', soapNS, null);
		dom.XmlNode OpCreate = body.addChildElement('OpCreate', urn, 'urn');
		OpCreate.addChildElement('Action', urn, null).
				addTextNode('2');
		OpCreate.addChildElement('PIN', urn, null).
				addTextNode(pin);
		OpCreate.addChildElement('Ticket_Number', urn, null).
				addTextNode(ticketNumber);
		System.debug(doc.toXmlString());

		// TODO: add exception handling

		// Send the request
		HttpRequest req = new HttpRequest();
		req.setMethod('GET');
		req.setEndpoint(endpoint);
		// TODO: specify a timeout of 2 minutes
		req.setTimeout(120000);

		String creds;
		SMBCare_WebServices__c wsUsername = SMBCare_WebServices__c.getValues('username_pc');
		SMBCare_WebServices__c wsPassword = SMBCare_WebServices__c.getValues('password');

		if (String.isNotBlank(wsUsername.Value__c) && String.isNotBlank(wsPassword.Value__c))
			creds = wsUsername.Value__c + ':' + wsPassword.Value__c;
		String encodedusernameandpassword = EncodingUtil.base64Encode(Blob.valueOf(creds));

		//Blob headerValue = Blob.valueOf(encodedusernameandpassword);
		String authorizationHeader = 'BASIC ' + encodedusernameandpassword;
		// EncodingUtil.base64Encode(headerValue);
		req.setHeader('Authorization', authorizationHeader);
		req.setHeader('Content-Type', 'text/xml');

		req.setBodyDocument(doc);

		// TODO: Determine the level of detail webservice is returning in regards to errors.
		//System.assertEquals(500, res.getStatusCode());

		//list<Remedy_Trouble_Ticket.TroubleTicket> XMLReturn = res.getBody();
		//System.debug('GetBody line 110:' + res.getBody());
		// System.assertEquals(200, res.getStatusCode());

		// Read through the XML
		String OKTicketId = '';
		String FaultStringView1 = '';
		String ClientPhoneView = '';
		String UpdateClientView = '';
		String TicketNumberView = '';

		if (!Test.isRunningTest()) {
			Http http = new Http();
			HttpResponse res = http.send(req);

			dom.Document resDoc = res.getBodyDocument();
			XmlStreamReader reader = res.getXmlStreamReader();
			while (reader.hasNext()) {
				System.debug('Event Type:' + reader.getEventType());
				if (reader.getEventType() == XmlTag.START_ELEMENT) {
					System.debug('getLocalName: ' + reader.getLocalName());
					if ('Ticket_Number' == (reader.getLocalName())) {
						boolean isSafeToGetNextXmlElement = true;
						while (isSafeToGetNextXmlElement) {
							if (reader.getEventType() == XmlTag.END_ELEMENT) {
								OKTicketId = reader.getLocalName();
								System.debug('OK Ticket Element Line 128 :' + OKTicketId);
								break;
							} else if (reader.getEventType() == XmlTag.CHARACTERS) {
								TicketNumberView = reader.getText();
								System.debug('TicketNumberView 132 :' + TicketNumberView);

							}

							if (reader.hasNext()) {

								reader.next();

							} else {

								isSafeToGetNextXmlElement = false;
								break;
							}
						}

					}

					if ('faultstring' == (reader.getLocalName())) {
						boolean isSafeToGetNextXmlElement = true;
						while (isSafeToGetNextXmlElement) {
							if (reader.getEventType() == XmlTag.END_ELEMENT) {
								OKTicketId = reader.getLocalName();
								System.debug('OK Ticket Element Line 154 :' + OKTicketId);
								break;
							} else if (reader.getEventType() == XmlTag.CHARACTERS) {
								FaultStringView1 = reader.getText();
								System.debug('FaultStringView1 Line 158 :' + FaultStringView1);

							}

							if (reader.hasNext()) {

								reader.next();

							} else {

								isSafeToGetNextXmlElement = false;
								break;
							}
						}

					}

				}
				reader.next();
			}
		} else {
			// TODO: Time constraint, adding for coverage, fix test class and remove this else block
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
			FaultStringView1 = 'test';
		}
		System.debug('TicketNumberViewFromCall->NewEvent, TicketNumberViewFromCall(' + ticketNumber + ')');
		System.debug('TicketNumberView->NewEvent, TicketNumberView(' + TicketNumberView + ')');
		System.debug('FaultStringView1->NewEvent, FaultStringView1(' + FaultStringView1 + ')');
		System.debug('TicketEventId->NewEvent, TicketEventId(' + TicketEventId + ')');
		if (((TicketNumberView == null || TicketNumberView == '') && (FaultStringView1 != '' || FaultStringView1 != null))) {
			if (TicketEventId == null || TicketEventId == '') {
				try {
					System.debug('TicketEventId->NewEvent, no Ticket Event Create New Event');
					Ticket_Event__c NewEvent = new Ticket_Event__c(
							Case_Number__c = caseNumber,
							Event_Type__c = 'New Ticket',
							Status__c = 'Verify',
							Failure_Reason__c = FaultStringView1,
							Lynx_Ticket_Number__c = ticketNumber,
							Case_Id__c = CaseId
					);
					System.debug('LynxTicketSearch->NewEvent, CaseNumber(' + caseNumber + ')');

					insert NewEvent;

				}

				catch (DmlException e) {
					System.debug('RemedyTicketSearch->TicketEvent, exception: ' + e.getMessage());
				}
			} else {
				try {
					System.debug('TicketEventId->NewEvent, Ticket Event Exists! Update Event');
					Ticket_Event__c NewEvent = [Select Event_Type__c, Status__c, Failure_Reason__c from Ticket_Event__c where Id = :TicketEventId];
					System.debug('TicketEventId->Update:-' + NewEvent);
					NewEvent.Event_Type__c = 'Verify Ticket';
					NewEvent.Status__c = 'New';
					NewEvent.Failure_Reason__c = FaultStringView1;
					Update NewEvent;
				} catch (DmlException e) {
					System.debug('RemedyTicketSearch->TicketEventUpdateNotValidated, exception: ' + e.getMessage());
				}
			}

		} else {
			try {
				System.debug('TicketEventId->NewEvent, Ticket Number Matched!');
				Ticket_Event__c NewEvent = [Select Event_Type__c, Status__c, Failure_Reason__c from Ticket_Event__c where Id = :TicketEventId];
				NewEvent.Event_Type__c = 'New Ticket';
				NewEvent.Status__c = 'Complete';
				NewEvent.Failure_Reason__c = '';
				Update NewEvent;
			} catch (DmlException e) {
				System.debug('RemedyTicketSearch->TicketEventUpdateValidated, exception: ' + e.getMessage());
			}
			Case tmpCase = [
					SELECT Id, CaseNumber, Lynx_Ticket_Number__c, Status
					FROM Case
					WHERE CaseNumber = :caseNumber
			];
			if (tmpCase != null && TicketNumberView != null) {
				tmpCase.Lynx_Ticket_Number__c = TicketNumberView;
				System.debug('Collab tmpCase.NotifyCollaboratorString__c: ' + tmpCase.NotifyCollaboratorString__c);
				if (tmpCase.NotifyCollaboratorString__c != null && tmpCase.NotifyCollaboratorString__c.length() > 0) {
					List<String> collabEmails = tmpCase.NotifyCollaboratorString__c.split(';', 0);
					if (collabEmails.size() > 0) {
						ENTP_Case_Collaborator_Settings__c collabSettings = ENTP_Case_Collaborator_Settings__c.getOrgDefaults();
						System.debug('Collab collabSettings: ' + collabSettings);
						//String templateName = collabSettings.New_Case_Template_Name__c;
						//List<EmailTemplate> templates = [select id from EmailTemplate where DeveloperName = :templateName LIMIT 1];
						//System.debug('Collab templates: ' + templates);
						Id emailTemplateId = collabSettings.New_Case_Template_Id__c;
						if (emailTemplateId != null) {
							//Id emailTemplateId = templates[0].Id;
							//Id emailTemplateId = '00X4000000163uYEAQ';
							Id fromEmailId = null;
							List<OrgWideEmailAddress> fromEmails = [select Id from OrgWideEmailAddress where DisplayName = 'Do Not Reply ENTP'];
							if (fromEmails.size() > 0) {
								fromEmailId = fromEmails[0].Id;
							}
							try {
								List<Messaging.SendEmailResult> mailResults = ENTPUtils.sendTemplatedCaseEmails(collabEmails, fromEmailId, emailTemplateId, tmpCase.Id);
							} catch (Exception e) {
								System.debug('Collab caught ENTPUtls.sendTemplatedCaseEmails() exception: ' + e);
							}
						}
					}
				}

			} System.debug('RemedyTickNumber, CaseNumber(' + tmpCase.caseNumber + '), TicketNumber(' + TicketNumberView + ')');
			update tmpCase;
		}
	}

// }

}