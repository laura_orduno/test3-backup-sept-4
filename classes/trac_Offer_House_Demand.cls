public without sharing class trac_Offer_House_Demand {

  /*
		Use Case 1 - User A, creates an OH record  and has a Sales Manager (User B) Specified on their user record. The Sales Manager (User B) specified on User A also has a Sales Manager (User C) on their user record. 
		Result: User A is the record owner, User B populates Owner Manager, User C populates Owner Director.
		Use Case 2 - User B creates an OH record and has a Sales Manager (User C) populated on their user record. User C does not have any Sales Manager list on their User Record.
		Result: User B is the owner of the record and User C populates Owner Director field
		Use Case 3 - User C creates and OH record and there is no Sales Manager Listed on their User record.
		Result: Owner Manager and Owner DIrector fields are left blank on the record
		
		Trigger only executes on create of the record.
  */
  
  public static void populateManagerDirector(List<Offer_House_Demand__c> theNewOffers) {
  	
  	// for each new offer, get owner managers, and owner directors, if any
  	Set<ID> owners = new Set<ID>();
  	Map<ID, ID> ownerManagers = new Map<ID, ID>();
  	Map<ID, ID> ownerDirectors = new Map<ID, ID>();
  	
  	// populate owner managers
  	for (Offer_House_Demand__c ohd : theNewOffers) owners.add(ohd.OwnerId);
  	List<User> theOwners = [SELECT Id, Sales_Manager__c FROM User WHERE Id IN : owners];
  	
  	owners = new Set<ID>();
  	for (User u : theOwners) {
  		ownerManagers.put(u.Id, u.Sales_Manager__c);
  		owners.add(u.Sales_Manager__c);
  	}
  	
  	// populate owner directors
  	List<User> theManagers = [SELECT Id, Sales_Manager__c FROM User WHERE Id IN : owners];
  	for (User u : theManagers) {
  		ownerDirectors.put(u.Id, u.Sales_Manager__c);
  	}
  	
  	for (Offer_House_Demand__c ohd : theNewOffers) {
  		
  		// Use Case A: OHD Owner has a manager and a director
  		if (ownerManagers.containsKey(ohd.OwnerId) &&
  		    ownerManagers.get(ohd.OwnerId) != null &&
  		    ownerDirectors.containsKey(ownerManagers.get(ohd.OwnerId)) &&
  		    ownerDirectors.get(ownerManagers.get(ohd.OwnerId)) != null) {
  		  ohd.Owner_Manager__c = ownerManagers.get(ohd.OwnerId);
  		  ohd.Owner_Director__c = ownerDirectors.get(ownerManagers.get(ohd.OwnerId));
  		  
  		// Use Case B: OHD Owner has a manager and no director 
  		} else if (ownerManagers.containsKey(ohd.OwnerId) &&
                 ownerManagers.get(ohd.OwnerId) != null) {
        ohd.Owner_Director__c = ownerManagers.get(ohd.OwnerId);
        
      // Use Case C: OHD Owner has no manager and no director
      } else {
      	// do nothing
      }
  	}
  	
  }
}