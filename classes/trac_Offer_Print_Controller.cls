global with sharing class trac_Offer_Print_Controller {
    
    public PageReference thePage {get; set;}
    public Boolean error {get; set;}
    public Boolean isFrench {get; set;} // denotes if the logged in user's language is french or english
	
    public String systemURL {get; set;} // base URL of the current salesforce instance, used to launch new pages
    public String errorMessage {get; set;}
    public String offerQuery {get; set;} // string to contain basic offer query
    public ID theID {get; set;} // id of the offer house demand object
    List<String> fieldNames {get; set;} // field names for offer house demand object
    
    public Offer_House_Wizard_Settings__c wizardSettings {get; set;} // offer house wizard custom setting

    public Set<String> salesProfiles; // contains list of sales profiles
    public Set<String> offerHouseProfiles; // containst list of offer house profiles
    public Set<String> adminProfiles; // contains list of admin profiles
    
    // Exception variables/class
    public class OfferHouseWizardException extends Exception {}
    
    public trac_Offer_Print_Controller() {
        
        error = false;
        systemURL = URL.getSalesforceBaseUrl().getHost();
        
        try {
        	
        	initLanguage();
        	
	        // Initialize and compare offer house object and versions
	        initOfferHouseObjects();
	        
	        // Get custom setting values
	        initCustomSettings();
	        
	        //errorMessage += 'USER TYPE: ' + userType;
	        errorMessage += 'USER PROFILE: ' + userProfile;
	        errorMessage += 'ADMIN PROFILES';
	        for (String s : adminProfiles) errorMessage += s + ', ';
        	
        } catch (Exception e) {
        	error = true;
					errorMessage += e.getMessage();
					ApexPages.addMessages(e);
        }
    }
    
    public void initLanguage() {
      system.debug('FRENCH DEBUG:' + UserInfo.getLanguage());
      isFrench = !(UserInfo.getLanguage() == 'en_US');
    }
    
    // Offer House Demand Object
    public Offer_House_Demand__c theOffer { 
    	get {
        	if (theOffer == null ) return initOfferHouseDemand();
        	else return theOffer;
        }
    	set;
	}
    
    public Offer_House_Demand__c initOfferHouseDemand() {
    	
    	if (offerQuery == null) offerQuery = createOfferQuery(); // get query string
        String theOfferQuery = offerQuery + ' WHERE Id = :theId'; // Add condition
        
        // Query for Offer House Demand being viewed
        if (theId != null) {
        	
        	List<Offer_House_Demand__c> theOfferList = Database.query(theOfferQuery);
        	
        	if (!theOfferList.isEmpty()) theOffer = theOfferList[0];
        	else throw new OfferHouseWizardException('The Offer not loaded.');
        	
        }
        return theOffer;
    }
    
    public void initOfferHouseObjects() {
        
        // Get current Ojbect Id
        thePage = ApexPages.currentPage();
        theId = thePage.getParameters().containsKey('ID')? thePage.getParameters().get('ID') : null;
        
        // Get Offer House Object
        initOfferHouseDemand();
    }
    
    public void initCustomSettings() {
    	
    	// load the settings
    	wizardSettings = Offer_House_Wizard_Settings__c.getOrgDefaults();
    	
    	// load the admin profiles
    	List<String> adminProfileSettingsList = wizardSettings.Admin_Profile__c != null ? wizardSettings.Admin_Profile__c.split(',',0) : new List<String>();
    	Set<String> adminProfilesSet = new Set<String>();
    	for (String s : adminProfileSettingsList) adminProfilesSet.add(s.trim());
    	List<Profile> adminProfilesList = [SELECT Id, Name FROM Profile WHERE Name IN :adminProfilesSet];
    	adminProfiles = new Set<String>();
    	for (Profile p : adminProfilesList) adminProfiles.add(p.Name);
    	
    	// load the sales profiles
    	List<String> salesProfileSettingsList = wizardSettings.Sales_Profile__c != null ? wizardSettings.Sales_Profile__c.split(',',0) : new List<String>();
    	Set<String> salesProfilesSet = new Set<String>();
    	for (String s : salesProfileSettingsList) salesProfilesSet.add(s.trim());
    	List<Profile> salesProfilesList = [SELECT Id, Name FROM Profile WHERE Name IN :salesProfilesSet];
    	salesProfiles = new Set<String>();
    	for (Profile p : salesProfilesList) salesProfiles.add(p.Name);
    	
    	// load offer house profiles
    	List<String> offerHouseProfileSettingsList = wizardSettings.Offer_House_Profile__c != null ? wizardSettings.Offer_House_Profile__c.split(',',0) : new List<String>();
    	Set<String> offerHouseProfilesSet = new Set<String>();
    	for (String s : offerHouseProfileSettingsList) offerHouseProfilesSet.add(s.trim());
    	List<Profile> offerHouseProfilesList = [SELECT Id, Name FROM Profile WHERE Name IN :offerHouseProfilesSet];
    	offerHouseProfiles = new Set<String>();
    	for (Profile p : offerHouseProfilesList) offerHouseProfiles.add(p.Name);
    	
    	
    }
    
    public String userProfile {
    	get {
	    	if (userProfile == null) {
		    	List<Profile> userProfileList = [SELECT Id, Name FROM Profile WHERE Id = :UserInfo.getProfileId()];
		    	if (!userProfileList.isEmpty()) userProfile =  userProfileList[0].Name;
		    	else userProfile = '';
		    	return userProfile;
	    	} else {
	    		return userProfile;
	    	}
    	}
    	set; 
	}
    
    // Helper Method: gets Offer House Demand Fields
    public List<String> getOfferFields() {
    	
        fieldNames = new List<String>();
        
        Map<String, Schema.SObjectField> offerFieldMap = Schema.SObjectType.Offer_House_Demand__c.fields.getMap();
        for (Schema.SObjectField fieldObj : offerFieldMap.values()) {
        	
        	Schema.DescribeFieldResult field = fieldObj.getDescribe();
        	fieldNames.add(field.getName());
        }
        
        return fieldNames;
    }
    
    // Helper Method: creates a query string for all fields in the Offer House Object
    // Returns the string
    public String createOfferQuery() {
    	
    	// Iterate through fields to select
        String theQuery = 'SELECT ' + String.join(getOfferFields(), ', ');
        
        // Add reference fields
        theQuery += ', Account__r.Name, OPPORTUNITY__r.Name, Owner.Name';
        theQuery += ' FROM Offer_House_Demand__c';
        
        return theQuery;
    }

    /*****************************************************************************************************************
	 * Methods for: Action Buttons on the Wizard
     *****************************************************************************************************************/

	// Helper Method: gets the current status of
	// the offer house record's approval process
	// Returns: the status of the object
	public String approvalProcessStatus {
		get {
			if (theOffer != null)
				return theOffer.Status__c;
			else
				return '';
		}
		set;
	}

}