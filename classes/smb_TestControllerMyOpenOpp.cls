/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData = true)
public class smb_TestControllerMyOpenOpp{
    
	static testMethod void validateMyOpenOpp() {
       list<opportunity> lstOpp = new list<opportunity>();
       
       RecordType OppRT = [SELECT Id, Name, DeveloperName
                        FROM RecordType
                        WHERE sObjectType = 'Opportunity' and
                        DeveloperName = 'SMB_Care_Opportunity' and 
                        isActive = true ];
       
       
       Opportunity OppRec = new Opportunity(Name = 'TestOpp',RecordTypeId = OppRT.id, 
                                            Type ='Penetration',Amount=500.0,closedate=date.today()+100,
                                            stagename='Originated');                 
       
       lstOpp.add(OppRec);
       Insert lstOpp;
       
      
       
       smb_Controller_MyOpenOpp OppIns = new smb_Controller_MyOpenOpp();
       OppIns.sortBy = 'Name';
       OppIns.sortDir = 'ASC';
       OppIns.sortList(lstOpp); 
       OppIns.empty();  
       
    }

	static testMethod void myUnitTest() {
    
     
    RecordType rt= [Select r.Id, r.DeveloperName From RecordType r where r.SobjectType = 'Account' and r.DeveloperName='RCID' limit 1];
            
       Account acc = new Account(Name='Testing Software', BillingCountry = 'US', BillingState = 'IL', recordTypeId=rt.id, No_Chronic_Incidents__c =10.0);
       insert acc;
       Contract Ct = new Contract(AccountId = Acc.Id, Status = 'Draft',StartDate = Date.Today(), ContractTerm = 12);
        Insert Ct;
        
        Contact ctc = new Contact(AccountId = Acc.Id, lastname='Test1', firstname='Test', Title='Engineer', Phone='999999999', Email='test@test.com' );
        Insert ctc;
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Opportunity; 
        Map<String,Schema.RecordTypeInfo> OpportunityRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
        Id rtId = OpportunityRecordTypeInfo.get('SRS Order Request').getRecordTypeId(); 
        Opportunity Oppt = new Opportunity();
        Oppt.AccountId = acc.Id;
        Oppt.Name = 'Test Opp';
        Oppt.CloseDate = Date.today();
        Oppt.StageName = 'Quote Accepted';
        Oppt.Order_Type__c = 'All Other Orders' ;
        Oppt.recordTypeId=rtId;
        insert Oppt;
          Quote q = new Quote();
        q.OpportunityId = Oppt.Id;
        q.name = 'test quote for expiration';
        q.Status = 'In Progress';
       
        insert q;
         String statusAfterInsert = [SELECT Id, status FROM Quote WHERE Name = 'test quote for expiration' LIMIT 1].status;
        
        
        q.ExpirationDate = Date.today().addDays(-1);
        update q;
          Id standardPriceBookId = Test.getStandardPriceBookId();
        SMBCare_Address__c sr = new SMBCare_Address__c();
        sr.Account__c = Acc.Id;
        sr.Unit_Number__c = '100';        
        sr.Street_Address__c = 'york st';
        sr.City__c = 'Toronto';
        sr.State__c = 'ON';
        sr.Country__c = 'Canada';
        sr.FMS_Address_ID__c='5915723';
        sr.Building_Number__c='1332';
        sr.Street__c='9 av se';
        sr.Province__c='ab';
        sr.Postal_Code__c='t7x 3h2';
        sr.COID__c='main';
        sr.Civic_Number__c='12345';
        sr.Country__c='CAN';
        insert sr;
        Order Od = new Order(Name = 'ORD-000001', PriceBook2Id =standardPriceBookId  ,Service_Address__c=sr.id, AccountId = Acc.Id, EffectiveDate = Date.Today(), ContractId = Ct.Id, Status = 'Draft', vlocity_cmt__QuoteId__c=Q.Id);
        Insert Od;
        List<Account> accList = [SELECT Id,parentId,Name from Account where id = :acc.Id LIMIT 1];
        list<SMBCare_WebServices__c> listCustomSetting =  new list<SMBCare_WebServices__c>();
        listCustomSetting.add(new SMBCare_WebServices__c(Name='SMB_SubmitOrder EndPoint',value__c='abc'));
        listCustomSetting.add(new SMBCare_WebServices__c(Name='BookDueDate_EndPoint',value__c='https://partnerservices-pt.telus.com:443/SOA/RMO/ProcessMgmt/FieldWorkAssignmentMgmtService_v2_0_vs1_vs0'));
        listCustomSetting.add(new SMBCare_WebServices__c(Name='username',value__c='uname'));
        listCustomSetting.add(new SMBCare_WebServices__c(Name='password',value__c='pwd'));
        listCustomSetting.add(new SMBCare_WebServices__c(Name='SMB_LegacyOrderEndpoint',value__c='https://xmlgwy-pt1.telus.com:9030/pt01/CMO/OrderMgmt/OrderTrackingService_v4_2_vs0'));
        listCustomSetting.add(new SMBCare_WebServices__c(Name='SMB_SuspendOrderEndpoint',value__c='pwd'));
        listCustomSetting.add(new SMBCare_WebServices__c(Name='SMB_SuspendOrderSoapAction',value__c='pwd'));
        listCustomSetting.add(new SMBCare_WebServices__c(Name='SMB_CancelOrderEndpoint',value__c='pwd'));
        listCustomSetting.add(new SMBCare_WebServices__c(Name='SMB_CancelOrderSoapAction',value__c='pwd'));
        listCustomSetting.add(new SMBCare_WebServices__c(Name='TTODS_EndPoint',value__c='EndPoint'));
        listCustomSetting.add(new SMBCare_WebServices__c(Name='SMB_BundleRegistration',value__c='EndPoint'));
        listCustomSetting.add(new SMBCare_WebServices__c(Name='SearchDueDate_EndPoint',value__c='EndPoint'));
        listCustomSetting.add(new SMBCare_WebServices__c(Name='SMB_ResumeOrderEndpoint',value__c='https://partnerservices-pt.telus.com:443/SOA/CMO/OrderMgmt/ResumeCustomerOrder_v2_1_vs0_vs0'));
        listCustomSetting.add(new SMBCare_WebServices__c(Name='SMB_ResumeOrderSoapAction',value__c='http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/fulfillment/ResumeCustomerOrder/resumeCustomerOrder'));
        Database.insert(listCustomSetting,false);
        list<SMB_DebugWebServices__c>  listCustomSettingDebugWebServices = new list<SMB_DebugWebServices__c>();
        listCustomSettingDebugWebServices.add(new SMB_DebugWebServices__c(Name = UserInfo.getUserName(),SubmitOrder__c = true,EndPoint__c = 'Test'));
        Database.insert(listCustomSettingDebugWebServices,false);
        PageReference pageref = Page.MyOpenOpp;
        Test.setCurrentPage(pageref);
        pageref.getparameters().put('id', acc.Id);
        boolean getpageinstanceid=true; 
        smb_SVOC_MyOpenOpp oc = new smb_SVOC_MyOpenOpp();
        oc.RCID='RCID';
          
        oc.init();
         system.assert(oc.pageInstanceId != null);
           // system.assert(oc.AllowCreationOfRelatedRecords!= null);
             //system.assert(oc.AllowCreationWithNewQuoteButton!= null);
            system.assert(oc.baseUrl!= null);
        List<Opportunity> oppList = smb_SVOC_MyOpenOpp.getCases(accList.get(0).Id,accList.get(0).ParentId);
        
        
    }
    
    static testMethod void myUnitTest2() {
        Account parentAccObj = smb_test_utility.createAccount('CBUCID', true);
        Account accObj = smb_test_utility.createAccount('RCID', false);
        accObj.ParentId = parentAccObj.Id;
        accObj.No_Chronic_Incidents__c = 1;
        insert accObj;
        Contact contObj = smb_test_utility.createContact('', accObj.Id, true);
        Opportunity testOpp = smb_test_utility.createOpportunity('SMB_Care_Opportunity', accObj.Id, contObj.Id, true);
        Opportunity testOpp2 = smb_test_utility.createOpportunity('SMB_Care_Opportunity', accObj.Id, contObj.Id, false);
        testOpp2.Requested_Due_Date__c = date.today().addDays(7);
        insert testOpp2;
        Opportunity testOpp3 = smb_test_utility.createOpportunity('SMB_Care_Opportunity', accObj.Id, contObj.Id, false);
        testOpp2.Requested_Due_Date__c = null;
        insert testOpp3;
        Work_Order__c wo = new Work_Order__c(Opportunity__c = testOpp.Id, Job_Type__c = 'Test' ,Install_Type__c = 'Test', 
                                            Status__c = 'Reserved', Unlinked_Work_Order__c = false, Scheduled_Datetime__c  = date.today().addDays(8));
        insert wo;
        test.setCurrentPageReference(Page.smb_SVOC);
        
        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'All Other Orders';
        insert testOpp_1;
        setDummyResponse(testOpp_1.Id);
        
        ApexPages.currentPage().getParameters().put('accountId',accObj.Id);
        Test.startTest();
            smb_SVOC_MyOpenOpp smb = new smb_SVOC_MyOpenOpp();
            smb.acct = accObj;
            
            smb.init();
            system.assert(smb.pageInstanceId != null);
          //  system.assert(smb.AllowCreationOfRelatedRecords != null);
          //  system.assert(smb.AllowCreationWithNewQuoteButton != null);
            system.assert(smb.baseUrl != null);
           
         //  smb.getLegacyOrders('Test', 'Test');
            
 }
   
    static void setDummyResponse(String oppId){
        map<string,object> responseMap = new map<string,object>();
        smb_orders_request_response_v4.getOrderListByRCIDResponse_element response_x = new smb_orders_request_response_v4.getOrderListByRCIDResponse_element();
        response_x.summaryRequestList  = new list<smb_orders_trackingtypes_v4.OrderRequest>();
        smb_orders_trackingtypes_v4.OrderRequest orderRequest = new smb_orders_trackingtypes_v4.OrderRequest();
        orderRequest.orderKey = new smb_orders_trackingtypes_v4.OrderRequestKey();
        orderRequest.orderKey.legacyOrderId=oppId;
        orderRequest.orderStatusSet = new smb_orders_trackingtypes_v4.OrderRequestStatus();
        orderRequest.orderStatusSet.orderStatus = 'Test Status';
        orderRequest.orderDueDateSet = new smb_orders_trackingtypes_v4.DueDate();
        orderRequest.orderDueDateSet.dueDate = system.today();
        orderRequest.orderAddress = new list<smb_orders_trackingtypes_v4.OrderRequestAddress>();
        smb_orders_trackingtypes_v4.OrderRequestAddress OrderRequestAddress = new smb_orders_trackingtypes_v4.OrderRequestAddress();
        OrderRequestAddress.address = 'Test';
        orderRequest.orderAddress.add(OrderRequestAddress);
        orderRequest.orderIdSet = new smb_orders_trackingtypes_v4.OrderRequestId();
        orderRequest.orderTypeSet = new smb_orders_trackingtypes_v4.OrderRequestType();
        orderRequest.orderTypeSet.orderServiceType = 'Test orderServiceType';
        orderRequest.orderTypeSet.dataOrderType = 'Test dataOrderType';
        orderRequest.orderTypeSet.legacyOrderType = 'Test legacyOrderType';
        orderRequest.orderTypeSet.orderType = 'Test orderType';
        orderRequest.orderTypeSet.voiceOrderType = 'Test voiceOrderType';
        orderRequest.customer = new smb_orders_trackingtypes_v4.Customer();
        orderRequest.orderCompletedDateSet = new smb_orders_trackingtypes_v4.OrderCompletedDate();
        orderRequest.remarkList = new smb_orders_trackingtypes_v4.RemarkList();
        orderRequest.outOfBandInfo = new smb_orders_trackingtypes_v4.OutOfBand();
        orderRequest.orderAttributeList = new smb_orders_trackingtypes_v4.Attribute();
        response_x.summaryRequestList.add(orderRequest);
        SMB_AssignmentWFM_ServiceReqRes_v2.CancelWorkOrderResponse response_y = new SMB_AssignmentWFM_ServiceReqRes_v2.CancelWorkOrderResponse();
        response_y.workOrderId = oppId;
        responseMap.put('SMB_AssignmentWFM_ServiceReqRes_v2.CancelWorkOrderResponse',response_y);
        responseMap.put('smb_orders_request_response_v4.getOrderListByRCIDResponse_element',response_x);
        GenericWebserviceMock genericWebserviceMock = new GenericWebserviceMock(responseMap);
        Test.setMock(WebServiceMock.class, genericWebserviceMock);
    } 
}