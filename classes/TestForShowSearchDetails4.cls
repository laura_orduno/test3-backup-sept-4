@isTest(SeeAllData=true)
public class TestForShowSearchDetails4{
    public static testMethod void testAtmList2(){       
       String cbucid='0000563979';
       String rcid='0000513162';
       String strProv='PQ';
       Integer newPageIndex=1;
       PageReference objPage;
       System.currentPagereference().getParameters().put('cbucid','0000563979');
       System.currentPagereference().getParameters().put('strCbucid','0000563979');
       System.currentPagereference().getParameters().put('rcid','0000513162');       
       System.currentPagereference().getParameters().put('strProv','PQ');   
       System.currentPagereference().getParameters().put('strAlpa','PQ');   
       System.currentPagereference().getParameters().put('strIndex','0.0');   
       
       AccountTeamMember atm=new AccountTeamMember(accountId='0014000000QKxNFAA1',Userid='00540000001CHGvAAO',TeamMemberRole='TEST');
       insert atm;
       
       ShowDetailForAssignmentProv obj=new ShowDetailForAssignmentProv();
       obj.strProv='PQ';
       
       obj.Rcidnumber='0000513162';
       obj.cbucidnumber='0000563979';
     
       obj.customerName='LES EDITIONS CEC';
       obj.cbucidName='QUEBECOR MEDIA INC';
       obj.banOrBcan='LES EDITIONS CEC INC';
       
       obj.selectedTabForSearch='customerName';
       obj.txtForSearch='zzzzzz';
       obj.searchCustomerName();
       obj.txtForSearch='CITY OF VANCOUVER';
       obj.searchCustomerName();
       
       obj.selectedTabForSearch='cbucidName';
       obj.txtForSearch='zzzzzz';
       obj.searchCbucidName();
       obj.txtForSearch='QUEBECOR MEDIA INC';
       obj.searchCbucidName();
       
       obj.selectedTabForSearch='banOrBcan';
       obj.txtForSearch='zzzzzz';
       obj.searchBanOrBcan();
       obj.txtForSearch='LES EDITIONS CEC INC';
       obj.searchBanOrBcan();
       
       obj.selectedTabForSearch='Cbucidnumber';
       obj.txtForSearch='0000563979';
       obj.searchCbucidnumber();
       
       obj.selectedTabForSearch='rcidnumber';
       obj.txtForSearch='0000513162';
       obj.searchRcidnumber();

      
       objPage=obj.firstBtnClick();
       System.assertEquals(null,objPage);
       objPage=obj.previousBtnClick(); 
       System.assertEquals(null,objPage);
       objPage=obj.nextBtnClick(); 
       System.assertEquals(null,objPage);
       objPage=obj.lastBtnClick(); 
       System.assertEquals(null,objPage); 
       newPageIndex=2;         
       obj.getPageNumber();
       obj.getPageSize();
       obj.getTotalPageNumber();
       obj.getPageAccountList();
       
       obj.getSalesList();
       obj.getShowClientServiceCreditList();
       obj.getCustStatusTool();
       Account testaccount = obj.getAccountDetail();
       obj.getMsgForSearchResult();
       obj.getMsgForPosition();
              
       String strProvSt = 'BC';     
       
       List<AtoZ_Assignment__c> atmList=new List<AtoZ_Assignment__c>();
                        atmList=[Select a.Id,a.Tel__c, a.Email__c, a.AtoZ_Field__c, a.AtoZ_Contact__r.Id,
                        a.AtoZ_Contact__r.Full_Name__c,a.AtoZ_Contact__r.Email__c ,a.AtoZ_Contact__r.Phone__c , a.assignment_value__c, a.assignment_Type__c
                        From AtoZ_Assignment__c a  where id='a0G40000002HXAEEA4' limit 1000];
       for(AtoZ_Assignment__c atest:atmList){
           atest.Assignment_Value__c='00023662';
           atest.Assignment_Type__c='ACCTPRIME';           
           atest.AtoZ_Field__c='ACCTPRIME';
       }
       update atmList;
       
       List<Sales_Assignment__c> findSalesList=[
        Select s.Id,s.User__r.Id, s.User__r.Team_TELUS_ID__c, s.Role__c, s.User__r.firstname,s.User__r.lastname, s.User__r.Email,
        s.User__r.Phone,s.User__r.UserRoleID,s.User__r.UserRole.Name, s.Account__r.rcid__c
        From Sales_Assignment__c s
        where s.Account__r.rcid__c='0001362414'
        limit 1
       ];
       
       Map<String, String> showSalesRole=obj.showSalesRoleUserName(findSalesList, strProvSt);                
              
       System.debug('testaccount.Id:'+testaccount.Id);
       List<AtoZ_Assignment__c> atozaList=new List<AtoZ_Assignment__c>();
       atozaList=[Select a.Id,a.Tel__c, a.Email__c, a.AtoZ_Field__c, a.AtoZ_Contact__r.Id,a.AtoZ_Contact__c,
                a.AtoZ_Contact__r.Full_Name__c,a.AtoZ_Contact__r.Email__c ,a.AtoZ_Contact__r.Phone__c  From AtoZ_Assignment__c a where a.RCID__c=:testaccount.Id];
       System.debug('atozaList size:'+atozaList.size());
        
       for(AtoZ_Assignment__c atest:atozaList){
           atest.AtoZ_Field__c='ACCTPRIME';
       }
       update atozaList;
       showSalesRole=obj.showSalesRoleUserName(findSalesList, strProvSt);
       
       atozaList=new List<AtoZ_Assignment__c>();
       atozaList=[Select a.Id,a.Tel__c, a.Email__c, a.AtoZ_Field__c, a.AtoZ_Contact__r.Id,a.AtoZ_Contact__c,
                a.AtoZ_Contact__r.Full_Name__c,a.AtoZ_Contact__r.Email__c ,a.AtoZ_Contact__r.Phone__c  From AtoZ_Assignment__c a where a.RCID__c=:testaccount.Id];
       for(AtoZ_Assignment__c atest:atozaList){
           atest.AtoZ_Field__c='ASR';
       }
       update atozaList;
       showSalesRole=obj.showSalesRoleUserName(findSalesList, strProvSt);
       
       atozaList=new List<AtoZ_Assignment__c>();
       atozaList=[Select a.Id,a.Tel__c, a.Email__c, a.AtoZ_Field__c, a.AtoZ_Contact__r.Id,a.AtoZ_Contact__c,
                a.AtoZ_Contact__r.Full_Name__c,a.AtoZ_Contact__r.Email__c ,a.AtoZ_Contact__r.Phone__c  From AtoZ_Assignment__c a where a.RCID__c=:testaccount.Id];
       for(AtoZ_Assignment__c atest:atozaList){
           atest.AtoZ_Field__c='WLS_SR';
       }
       update atozaList;
       showSalesRole=obj.showSalesRoleUserName(findSalesList, strProvSt);

       atozaList=new List<AtoZ_Assignment__c>();
       atozaList=[Select a.Id,a.Tel__c, a.Email__c, a.AtoZ_Field__c, a.AtoZ_Contact__r.Id,a.AtoZ_Contact__c,
                a.AtoZ_Contact__r.Full_Name__c,a.AtoZ_Contact__r.Email__c ,a.AtoZ_Contact__r.Phone__c  From AtoZ_Assignment__c a where a.RCID__c=:testaccount.Id];
       for(AtoZ_Assignment__c atest:atozaList){
           atest.AtoZ_Field__c='WLS_ASR';
       }
       update atozaList;
       showSalesRole=obj.showSalesRoleUserName(findSalesList, strProvSt);
       
       

    }
    
}