public class CaseModel {

    public static Id CreateCase(Id acctId ,Id contactId, String Subject, String Status, string Description) {
        
        Case newCase = new Case();
        newCase.AccountId = acctId;
        newCase.ContactId = contactId;
        newCase.Subject = Subject;
        newCase.Status = Status;
        newCase.Description = Description;
        
        insert newCase;
        
        return newCase.Id;
    }
}