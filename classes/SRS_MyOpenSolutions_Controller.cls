/*
###########################################################################
# File..................: SRS_MyOpenSolutions_Controller
# Version...............: 1
# Created by............: Aditya Jamwal (IBM)
# Created Date..........: 16-Oct-2014 
# Last Modified by......: Adhir Parchure (Tech Mahindra)
# Last Modified Date....: 30-Sept-2015
# Modification..........: Updated the class to incorporate the user stories of the BCXISD project.         
# Description...........: Class is for My openSolutions Homepage component filtering solutions specific for logged in user.
#
############################################################################ 

*/
public with sharing class SRS_MyOpenSolutions_Controller {

public Boolean showRelatedList{get;set;}
public String SRId {get; set;}
public List<Opportunity> SolutionsList = new List<Opportunity>();
public boolean doSort= false;
private String sortDirection = 'DESC';
private String sortExp = 'LastModifiedDate';
string sortFullExp = ' order by LastModifiedDate desc '; 
// Which field should be considered for sorting
public static string SORT_FIELD ='city' ;
// Sorting direction ASCENDING or DESCENDING
public static string SORT_DIR = 'ASC';   
public transient Map<ID, Schema.RecordTypeInfo> recordtypeMap = new Map<ID, Schema.RecordTypeInfo>();
public ApexPages.StandardController tempController; 

public String sortExpression
           {
             get
             {
                return sortExp;
             }
             set
             {
               //if the column is clicked on then switch between Ascending and Descending modes
               if (value == sortExp)
           sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
           else
           sortDirection = 'ASC';
           sortExp = value;
              
             }
           }
           
           
public String getSortDirection()
         {
            //if not column is selected 
            if (sortExpression == null || sortExpression == '')
              return 'ASC';
            else
             return sortDirection;
         }
         
         public void setSortDirection(String value)
         {  
           sortDirection = value;
         }


 
 public void SortRelatedListwithColumn()
    {
         
               doSort=true;          
               sortFullExp = '';
               //build the full sort expression
               sortFullExp = ' order by ' +sortExpression  + ' ' + sortDirection+' NULLS LAST ';
               
    } 
      
 public SRS_MyOpenSolutions_Controller() 
    {       
          sortExpression='Condition_Sort_Index__c';
          sortDirection = 'DESC';                        
    }   


    public List<Solution> getSolutionsList() {

    Set<id> OppIds = new Set<id>();
    for(Service_Request_Employee__c TeamMember : [Select Id, Service_Request__r.id ,Service_Request__r.Opportunity__c From Service_Request_Employee__c where Team_Member__r.User__c = :Userinfo.getUserId()])
    {OppIds.add(TeamMember.Service_Request__r.Opportunity__c);}
    
    //List<Opportunity> opplist =[select id,name,account.Id,account.Name,Condition__c,Condition_Color__c,Condition_Sort_Index__c,Order_ID__c,Status__c,Type,Product_Type__c,Requested_Due_Date__c,CreatedDate,Recordtype.name from Opportunity where (recordtype.name='SMB Care Amend Order Opportunity' or recordtype.name='SMB Care Locked Order Opportunity' or recordtype.name='SMB Care Opportunity' or recordtype.name='SRS Order Request') and
    //(ownerid = :Userinfo.getUserId() or createdbyId = :Userinfo.getUserId() or Id IN :OppIds) and (Status__c Not IN('Cancelled','Completed')) ];
        
    List<Opportunity> opplist =[select id,name,account.Id,account.Name,Condition__c,Condition_Color__c,Condition_Sort_Index__c,Order_ID__c,StageName, Status__c,Type,Product_Type__c,
    Requested_Due_Date__c,CreatedDate,Recordtype.name from Opportunity where (recordtype.developername='SMB_Care_Amend_Order_Opportunity' or 
    recordtype.developername='SMB_Care_Locked_Order_Opportunity' or recordtype.developername='SMB_Care_Opportunity' or recordtype.developername='SRS_Order_Request') and    
    (ownerid = :Userinfo.getUserId() or createdbyId = :Userinfo.getUserId() or Id IN :OppIds) and (Status__c Not IN('Cancelled', 'Completed', 'Quote Cancelled') and (StageName Not IN('Order Request Cancelled', 'Order Request Completed', 'Quote Cancelled'))) ];   
       
       //Date dt= new date.newinstance((p.createdDate).year(), (p.createdDate).month(), (p.createdDate).day());
        List<Solution> SoluList = new List<Solution>();
                  for(Opportunity p:opplist)
                   {
						p.Status__c = String.isEmpty(p.Status__c) ? p.StageName : p.Status__c;                        
                        Solution sol = new Solution(p);
                        //sol.oppCrDate= 
                        SoluList.add(sol);
                        system.debug('Solution'+ sol);
                   }
            SORT_DIR = sortDirection;
            SORT_FIELD =sortExpression;
            system.debug('>>>>SORT_DIR: '+ SORT_DIR);
            system.debug('>>>>SORT_FIELD: '+ SORT_FIELD);
            SoluList.sort() ; 
        return SoluList;
    }
    
    public class Solution implements Comparable
    {
      public Opportunity Opp{get; set;}     
        Date oppCrDate;
         
     
        public Integer compareTo(Object other) {
        
        //system.debug('>> SORT_FIELD'+SORT_FIELD);
         if (SORT_FIELD == 'LastModifiedDate') 
         {
            //return compareToSRDate(other); 
           return compareToSRDateTime(other); 
         } 
         else if (SORT_FIELD == 'Solution.Opp.CreatedDate' || SORT_FIELD== 'solution.oppCrDate')
         {
             return compareToSRDate(other);
         }
         else if (SORT_FIELD == 'DD_Scheduled__c' || SORT_FIELD =='Requested_Date__c' || SORT_FIELD =='Solution.Opp.Requested_Due_Date__c' ) 
         {
            return compareToSRDate(other); 
         } else 
             return compareStringProperty(other); 
          return 0;
          
         }
                        
         // Compares SRString field
         Integer compareStringProperty(Object other) 
         {  
             String sortfield;
             String currInstField;
             
            if(SORT_FIELD=='account.Name')
            {   
               
                 sortfield  =((Solution)other).Opp.account.Name!= null ?  ((Solution)other).Opp.account.Name : '';
                 currInstField  =this.Opp.account.Name  != null ?  this.Opp.account.Name: '';
            
            }
            else if(SORT_FIELD=='Condition_Sort_Index__c')
            {
                
                 sortfield  =((Solution)other).Opp.Condition_Sort_Index__c != null ?  ((Solution)other).Opp.Condition_Sort_Index__c : '';
                 currInstField  =this.Opp.Condition_Sort_Index__c != null ?  this.Opp.Condition_Sort_Index__c: '';
            
            }
            // Added by Adhir for User story: Service Request sorting & filtering (BR801647)
            else if(SORT_FIELD=='Solution.Opp.Order_ID__c')
            {
                
                 sortfield  =((Solution)other).Opp.Order_ID__c != null ?  ((Solution)other).Opp.Order_ID__c : '';
                 currInstField  =this.Opp.Order_ID__c != null ?  this.Opp.Order_ID__c: '';
            
            }
            else if(SORT_FIELD=='Solution.Opp.name')
            {
                
                 sortfield  =((Solution)other).Opp.name != null ?  ((Solution)other).Opp.name : '';
                 currInstField  =this.Opp.name != null ?  this.Opp.name : '';
            
            }
            else if(SORT_FIELD=='Solution.Opp.Status__c')
            {
                
                 sortfield  =((Solution)other).Opp.Status__c != null ?  ((Solution)other).Opp.Status__c : '';
                 currInstField  =this.Opp.Status__c != null ?  this.Opp.Status__c: '';
            
            }
            else if(SORT_FIELD=='Solution.Opp.Type')
            {
                
                 sortfield  =((Solution)other).Opp.Type != null ?  ((Solution)other).Opp.Type : '';
                 currInstField  =this.Opp.Type != null ?  this.Opp.Type: '';
            
            }
            else if(SORT_FIELD=='Solution.Opp.Product_Type__c')
            {
                
                 sortfield  =((Solution)other).Opp.Product_Type__c != null ?  ((Solution)other).Opp.Product_Type__c : '';
                 currInstField  =this.Opp.Product_Type__c != null ?  this.Opp.Product_Type__c: '';
            
            }
            
             String otherSRString= other != null ?  sortfield : '';   
             if (SORT_DIR== 'ASC')     
               return currInstField.compareTo(otherSRString);   
             else
               return otherSRString.compareTo(currInstField);  
        
         }
         
         Integer compareToSRDateTime(Object other) 
         {
            DateTime sortfield;
            DateTime currInstField;
            
            if(SORT_FIELD=='LastModifiedDate')
            {
            
             sortfield  =((Solution)other).Opp.LastModifiedDate != null ?  ((Solution)other).Opp.LastModifiedDate: datetime.newInstance(1900, 01, 01);
             currInstField  =this.Opp.LastModifiedDate != null ?  this.Opp.LastModifiedDate : datetime.newInstance(1900, 01, 01);
            
            }
           /* BCXISD change 
           // warranty defect: 'Created Date' Sorting Issue
           else if(SORT_FIELD=='Solution.Opp.CreatedDate')
            {
             sortfield  =((Solution)other).Opp.CreatedDate != null ?  ((Solution)other).Opp.CreatedDate: datetime.newInstance(1900, 01, 01);
             currInstField  =this.Opp.CreatedDate != null ?  this.Opp.CreatedDate : datetime.newInstance(1900, 01, 01);
            
            } 
            //End    */
            
          DateTime otherDateTime = other != null ? sortfield  : datetime.newInstance(1900, 01, 01);   
          // use Datetime.getTime() to do get the numeric time in millis
          if (SORT_DIR == 'ASC')
           return (currInstField.getTime() - otherDateTime.getTime()).intValue();
          else
           return (otherDateTime.getTime() - currInstField.getTime()).intValue();
         }
         
         Integer compareToSRDate(Object other) 
         {    
             Date sortfield;
             Date currInstField;
           if(SORT_FIELD=='Requested_Date__c' || SORT_FIELD=='Solution.Opp.Requested_Due_Date__c')
            {
                sortfield  =((Solution)other).Opp.Requested_Due_Date__c != null ?  ((Solution)other).Opp.Requested_Due_Date__c : date.newInstance(1900,01, 01);
                currInstField  =this.Opp.Requested_Due_Date__c != null ?  this.Opp.Requested_Due_Date__c : date.newInstance(1900,01, 01);
            
            }
            
           if(SORT_FIELD=='solution.oppCrDate')
            {
                sortfield  =((Solution)other).oppCrDate != null ?  ((Solution)other).oppCrDate : date.newInstance(1900,01, 01);
                currInstField  =this.oppCrDate != null ?  this.oppCrDate : date.newInstance(1900,01, 01);
            
            }     
          
          Date otherDate = other != null ? sortfield : date.newInstance(1900,01, 01);   
          // use Datetime.getTime() to do get the numeric time in millis
          if (SORT_DIR == 'ASC')
           return otherDate.daysBetween(currInstField);
           else
           return currInstField.daysBetween(otherDate);  
         }

      
      public Solution(Opportunity op)
      {
        Opp= op;
        DateTime dT = Opp.createdDate;
        Date myDate = date.newinstance(dT.year(), dT.month(), dT.day());
        oppCrDate= myDate;
      }
       public Solution()
      {
        Opp= null;
      }
    } 

}