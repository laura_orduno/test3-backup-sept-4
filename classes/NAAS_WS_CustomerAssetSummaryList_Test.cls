/*
 * NAAS_WS_CustomerAssetSummaryList_Test
 *
 * This Test class is to validate the Apex REST Service defined in the class "NAAS_WS_CustomerAssetSummaryList".
 *
 * Revision History:
 * 2017/02/22 - Jeffrey Ko - Modified test class to match the latest v1.2 API specs.  The test will now validate the new REST service
 *                           that has the pattern of 
 *                           https://instance.salesforce.com/services/apexrest/Customer/Assets/Summaries?account=101-123456789,102-1324567
 * 
 */



@isTest(SeeAllData=false)
private class NAAS_WS_CustomerAssetSummaryList_Test 
{
    // Set up test Billing System ID's
    private static final String TEST_BILLING_SYSTEM_ID1 = '101';
    private static final String TEST_BILLING_SYSTEM_ID2 = '102';



    // Set up test Billing Account Numbers
    private static final String TEST_BILLING_ACCOUNT_NUMBER1 = '0070012340';
    private static final String TEST_BILLING_ACCOUNT_NUMBER2 = '0070012341';
    private static final String TEST_BILLING_ACCOUNT_NUMBER3 = '0070012350';
    private static final String TEST_BILLING_ACCOUNT_NUMBER4 = '0070012351';
    
    /*
     * This test setup method is used to generate test data for all test methods in this class.
     * Each test method will need to query the appropriate records accordingly.  
     */
    @testSetup
    static void generateTestData()
    {
        // Create the parent RCID account
        Account rcidAccount = new Account();
        rcidAccount.Name = 'RCIDNAME 9999999999';
        rcidAccount.RCID__c = '9999999999';
        rcidAccount.RecordTypeId = [select Id, Name from RecordType where Name =: 'RCID' limit 1].id;
        insert rcidAccount;

        // Create Child accounts; these are the accounts that will  be queried and returned as part of the REST service.
        List<Account> childAccounts = new List<Account>();
    
        // Create 2 BAN Billing Accounts
        Account banAccount1 = createTestChildAccount(TEST_BILLING_SYSTEM_ID1, TEST_BILLING_ACCOUNT_NUMBER1, 'BAN', rcidAccount.Id);
        Account banAccount2 = createTestChildAccount(TEST_BILLING_SYSTEM_ID1, TEST_BILLING_ACCOUNT_NUMBER2, 'BAN', rcidAccount.Id);

        // Create 2 CAN Billing Accounts
        Account canAccount1 = createTestChildAccount(TEST_BILLING_SYSTEM_ID2, TEST_BILLING_ACCOUNT_NUMBER3, 'CAN', rcidAccount.Id);
        Account canAccount2 = createTestChildAccount(TEST_BILLING_SYSTEM_ID2, TEST_BILLING_ACCOUNT_NUMBER4, 'CAN', rcidAccount.Id);


        childAccounts.add( banAccount1 );
        childAccounts.add( banAccount2 );
        childAccounts.add( canAccount1 );
        childAccounts.add( canAccount2 );
        insert childAccounts;
        

        // Create Assets and Products (for each child account)                
        insertAssetsAndProducts(rcidAccount.Id, banAccount1.Id, '111');
        insertAssetsAndProducts(rcidAccount.Id, banAccount1.Id, '112');
        insertAssetsAndProducts(rcidAccount.Id, banAccount1.Id, '113');
        insertAssetsAndProducts(rcidAccount.Id, banAccount2.Id, '222');
        
        insertAssetsAndProducts(rcidAccount.Id, canAccount1.Id, '333');
        insertAssetsAndProducts(rcidAccount.Id, canAccount2.Id, '444');
        insertAssetsAndProducts(rcidAccount.Id, canAccount2.Id, '555');
        
        
    }



    /*
     * TEST 1A: Normal get call (one Account Only); the Billing Account Number should have 3 Assets returned (BAN)
     */
    static testmethod void testGet1()
    {   
       
        system.debug('TEST 1A ... START');
       
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        req.requestURI = '/Customer/Assets/Summaries';
        req.params.put('account', TEST_BILLING_SYSTEM_ID1  + '-' + TEST_BILLING_ACCOUNT_NUMBER1 );
        req.resourcePath = '/services/apexrest/Customer/Assets/Summaries/*';
        req.httpMethod = 'GET';
        
        system.debug('TEST 1A:  ... req=' + req);
        system.debug('TEST 1A:  ... req.requestURI=' + req.requestURI);
        
        RestContext.request = req;
        RestContext.response = res;
        
        NAAS_WS_CustomerAssetSummaryList.getAssets();
        Test.stopTest();              
        
        
        String jsondata  = res.responseBody.toString();
        
        system.debug('TEST 1A: jsondata =' + jsondata );
        
        // Make sure Status is 200
        CustomerAssetSummaryResponse testResponse = createCustomerAssetSummaryResponse(jsondata);
        
        system.debug('TEST 1A: testResponse.Status =' + testResponse.status );
        
        system.assertEquals(200, testResponse.status.statusCd);
        system.assertEquals('OK', testResponse.status.statusTxt);        
        
        // For this Billing Account, there should be 3 Assets
        List<CustomerAssetSummary> testAssetList = testResponse.customerAssetSummaryList;
        
        system.debug('TEST 1A: testAssetList =' + testAssetList  );
        
        system.assertEquals(3, testAssetList.size(), 'There should be 3 assets tied to this Billing Account' );
        
        // Loop through the list and make sure they have the desired Product Name
        for (CustomerAssetSummary testAsset: testAssetList )
        {
            system.assertEquals(testAsset.productName, 'Managed Internet Basic');
            system.assertEquals(testAsset.productCategory, 'NAAS');
        }
        
    
    }


    /*
     * TEST 1B: Normal get call; the Billing Account Number should have 1 Assets returned (BAN)
     */
    static testmethod void testGet2()
    {   
       
        system.debug('TEST 1b - testGet2 ... START');
       
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        req.requestURI = '/Customer/Assets/Summaries';
        req.params.put('account', TEST_BILLING_SYSTEM_ID1  + '-' + TEST_BILLING_ACCOUNT_NUMBER2 );
        
        req.resourcePath = '/services/apexrest/Customer/Assets/Summaries/*';
        req.httpMethod = 'GET';
        
        system.debug('TEST 1b:  ... req.requestURI=' + req.requestURI);
        
        RestContext.request = req;
        RestContext.response = res;
        
        NAAS_WS_CustomerAssetSummaryList.getAssets();
        Test.stopTest();              
        
        
        String jsondata  = res.responseBody.toString();
        
        system.debug('TEST 1b: jsondata =' + jsondata );
        
        // Make sure Status is 200
        CustomerAssetSummaryResponse testResponse = createCustomerAssetSummaryResponse(jsondata);
        
        system.debug('TEST 1b: testResponse.Status =' + testResponse.status );
        
        system.assertEquals(200, testResponse.status.statusCd);
        system.assertEquals('OK', testResponse.status.statusTxt);        
        
        // For this Billing Account, there should be 3 Assets
        List<CustomerAssetSummary> testAssetList = testResponse.customerAssetSummaryList;
        
        system.debug('TEST 1b: testAssetList =' + testAssetList  );
        
        system.assertEquals(1, testAssetList.size(), 'There should be 3 assets tied to this Billing Account' );
        
        // Loop through the list and make sure they have the desired Product Name
        for (CustomerAssetSummary testAsset: testAssetList )
        {
            system.assertEquals(testAsset.productName, 'Managed Internet Basic');
            system.assertEquals(testAsset.productCategory, 'NAAS');
        }
        
    
    }


    /*
     * TEST 1C: Normal get call; the Billing Account Number should have 1 Assets returned (CAN)
     */
    static testmethod void testGet3CAN()
    {   
       
        system.debug('TEST 1c - testGet3CAN ... START');
       
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        req.requestURI = '/Customer/Assets/Summaries';
        req.params.put('account', TEST_BILLING_SYSTEM_ID2  + '-' + TEST_BILLING_ACCOUNT_NUMBER3 );
        
        req.resourcePath = '/services/apexrest/Customer/Assets/Summaries/*';
        req.httpMethod = 'GET';
        
        system.debug('TEST 1c:  ... req.requestURI=' + req.requestURI);
        
        RestContext.request = req;
        RestContext.response = res;
        
        NAAS_WS_CustomerAssetSummaryList.getAssets();
        Test.stopTest();              
        
        
        String jsondata  = res.responseBody.toString();
        
        system.debug('TEST 1c: jsondata =' + jsondata );
        
        // Make sure Status is 200
        CustomerAssetSummaryResponse testResponse = createCustomerAssetSummaryResponse(jsondata);
        
        system.debug('TEST 1c: testResponse.Status =' + testResponse.status );
        
        system.assertEquals(200, testResponse.status.statusCd);
        system.assertEquals('OK', testResponse.status.statusTxt);        
        
        // For this Billing Account, there should be 3 Assets
        List<CustomerAssetSummary> testAssetList = testResponse.customerAssetSummaryList;
        
        system.debug('TEST 1c: testAssetList =' + testAssetList  );
        
        system.assertEquals(1, testAssetList.size(), 'There should be 3 assets tied to this Billing Account' );
        
        // Loop through the list and make sure they have the desired Product Name
        for (CustomerAssetSummary testAsset: testAssetList )
        {
            system.assertEquals(testAsset.productName, 'Managed Internet Basic');
            system.assertEquals(testAsset.productCategory, 'NAAS');
        }
        
    
    }



    /*
     * TEST 1D: Normal get call (two Accounts); these Billing Account Number should have 4 Assets returned
     */
    static testmethod void testGet4()
    {   
       
        system.debug('TEST 1D ... testGet4 ... START (Multiple Accounts)');
       
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        req.requestURI = '/Customer/Assets/Summaries';
        
        String accountQueryString = TEST_BILLING_SYSTEM_ID1  + '-' + TEST_BILLING_ACCOUNT_NUMBER1 + ',';
        accountQueryString += TEST_BILLING_SYSTEM_ID1  + '-' + TEST_BILLING_ACCOUNT_NUMBER2;
        req.params.put('account', accountQueryString );
        
        req.resourcePath = '/services/apexrest/Customer/Assets/Summaries/*';
        req.httpMethod = 'GET';
        
        system.debug('TEST 1D:  ... req.requestURI=' + req.requestURI);
        
        RestContext.request = req;
        RestContext.response = res;
        
        NAAS_WS_CustomerAssetSummaryList.getAssets();
        Test.stopTest();              
        
        
        String jsondata  = res.responseBody.toString();
        
        system.debug('TEST 1D: jsondata =' + jsondata );
        
        // Make sure Status is 200
        CustomerAssetSummaryResponse testResponse = createCustomerAssetSummaryResponse(jsondata);
        
        system.debug('TEST 1D: testResponse.Status =' + testResponse.status );
        
        system.assertEquals(200, testResponse.status.statusCd);
        system.assertEquals('OK', testResponse.status.statusTxt);        
        
        // For this Billing Account, there should be 3 Assets
        List<CustomerAssetSummary> testAssetList = testResponse.customerAssetSummaryList;
        
        system.debug('TEST 1D: testAssetList =' + testAssetList  );
        
        system.assertEquals(4, testAssetList.size(), 'There should be 4 assets tied to this Billing Account' );
        
        // Loop through the list and make sure they have the desired Product Name
        for (CustomerAssetSummary testAsset: testAssetList )
        {
            system.assertEquals(testAsset.productName, 'Managed Internet Basic');
            system.assertEquals(testAsset.productCategory, 'NAAS');
        }
        
    
    }


    /*
     * TEST 1E: Normal get call (3 Accounts); these Billing Account Number should have 6 Assets returned
     */
    static testmethod void testGet5()
    {   
       
        system.debug('TEST 1E ... testGet5 ... START (Multiple Accounts)');
       
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        req.requestURI = '/Customer/Assets/Summaries';
        String accountQueryString = TEST_BILLING_SYSTEM_ID1  + '-' + TEST_BILLING_ACCOUNT_NUMBER1 + ',';
        accountQueryString += TEST_BILLING_SYSTEM_ID1  + '-' + TEST_BILLING_ACCOUNT_NUMBER2 + ',';
        accountQueryString += TEST_BILLING_SYSTEM_ID2  + '-' + TEST_BILLING_ACCOUNT_NUMBER4;
        req.params.put('account', accountQueryString );
        
        
        req.resourcePath = '/services/apexrest/Customer/Assets/Summaries/*';
        req.httpMethod = 'GET';
        
        system.debug('TEST 1E:  ... req.requestURI=' + req.requestURI);
        
        RestContext.request = req;
        RestContext.response = res;
        
        NAAS_WS_CustomerAssetSummaryList.getAssets();
        Test.stopTest();              
        
        
        String jsondata  = res.responseBody.toString();
        
        system.debug('TEST 1E: jsondata =' + jsondata );
        
        // Make sure Status is 200
        CustomerAssetSummaryResponse testResponse = createCustomerAssetSummaryResponse(jsondata);
        
        system.debug('TEST 1E: testResponse.Status =' + testResponse.status );
        
        system.assertEquals(200, testResponse.status.statusCd);
        system.assertEquals('OK', testResponse.status.statusTxt);        
        
        // For this Billing Account, there should be 3 Assets
        List<CustomerAssetSummary> testAssetList = testResponse.customerAssetSummaryList;
        
        system.debug('TEST 1E: testAssetList =' + testAssetList  );
        
        system.assertEquals(6, testAssetList.size(), 'There should be 6 assets tied to this Billing Account' );
        
        // Loop through the list and make sure they have the desired Product Name
        for (CustomerAssetSummary testAsset: testAssetList )
        {
            system.assertEquals(testAsset.productName, 'Managed Internet Basic');
            system.assertEquals(testAsset.productCategory, 'NAAS');
        }
        
    
    }



    /*
     * TEST 2A: Check to make sure we get the correct status code for Missing Parameters
     */    
    static testmethod void testInvalidEntry1MissingParameters()
    {
        system.debug('TEST 2A ... START (No account parameter)');
    
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        req.requestURI = '/Customer/Assets/Summaries';
        req.resourcePath = '/services/apexrest/Customer/Assets/Summaries/*';
        req.httpMethod = 'GET';
                        
        RestContext.request = req;
        RestContext.response = res;
        
        NAAS_WS_CustomerAssetSummaryList.getAssets();
        
        Test.stopTest();   
        
        String jsondata  = res.responseBody.toString();
        
        // Make sure Status is 400
        CustomerAssetSummaryResponse testResponse = createCustomerAssetSummaryResponse(jsondata);
        
        system.debug('TEST 2: jsondata =' + jsondata );
        
        system.debug('TEST 2: testResponse.Status =' + testResponse.status );
        
        system.assertEquals(400, testResponse.status.statusCd);
        system.assertEquals('MSP', testResponse.status.SubCode);        
        
    }
    
    
    /*
     * TEST 2B: Check to make sure we get the correct status code for Missing Parameters
     */    
    static testmethod void testInvalidEntry2MissingParameters()
    {
        system.debug('TEST 2B ... START (bad account parameter name)');
    
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        req.requestURI = '/Customer/Assets/Summaries';
        req.params.put('test', '22222222222' );

        req.resourcePath = '/services/apexrest/Customer/Assets/Summaries/*';
        req.httpMethod = 'GET';
                        
        RestContext.request = req;
        RestContext.response = res;
        
        NAAS_WS_CustomerAssetSummaryList.getAssets();
        
        Test.stopTest();   
        
        String jsondata  = res.responseBody.toString();
        
        // Make sure Status is 400
        CustomerAssetSummaryResponse testResponse = createCustomerAssetSummaryResponse(jsondata);
        
        system.debug('TEST 2: jsondata =' + jsondata );
        
        system.debug('TEST 2: testResponse.Status =' + testResponse.status );
        
        system.assertEquals(400, testResponse.status.statusCd);
        system.assertEquals('MSP', testResponse.status.SubCode);        
        
    }



    /*
     * TEST 2C: Check to make sure the Billing Account Number has a valid System ID
     */    
    static testmethod void testInvalidBillingSystemIDs()
    {
        system.debug('TEST 2C ... START (Invalid Billing System ID)');
    
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        req.requestURI = '/Customer/Assets/Summaries';
        req.params.put('account', '999-777777777' );

        req.resourcePath = '/services/apexrest/Customer/Assets/Summaries/*';
        req.httpMethod = 'GET';
                        
        RestContext.request = req;
        RestContext.response = res;
        
        NAAS_WS_CustomerAssetSummaryList.getAssets();
        
        Test.stopTest();   
        
        String jsondata  = res.responseBody.toString();
        
        // Make sure Status is 400
        CustomerAssetSummaryResponse testResponse = createCustomerAssetSummaryResponse(jsondata);
        
        system.debug('TEST 2: jsondata =' + jsondata );
        
        system.debug('TEST 2: testResponse.Status =' + testResponse.status );
        
        system.assertEquals(400, testResponse.status.statusCd);
        system.assertEquals('ITP', testResponse.status.SubCode);        
        
    }
    
    

    /*
     * TEST 2D: Check to make sure we can handle a billing number with no system ID
     */    
    static testmethod void testNoBillingSystemIDs()
    {
        system.debug('TEST 2D ... START (No Billing System ID)');
    
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        req.requestURI = '/Customer/Assets/Summaries';
        req.params.put('account', '777777777' );

        req.resourcePath = '/services/apexrest/Customer/Assets/Summaries/*';
        req.httpMethod = 'GET';
                        
        RestContext.request = req;
        RestContext.response = res;
        
        NAAS_WS_CustomerAssetSummaryList.getAssets();
        
        Test.stopTest();   
        
        String jsondata  = res.responseBody.toString();
        
        // Make sure Status is 400
        CustomerAssetSummaryResponse testResponse = createCustomerAssetSummaryResponse(jsondata);
        
        system.debug('TEST 2: jsondata =' + jsondata );
        
        system.debug('TEST 2: testResponse.Status =' + testResponse.status );
        
        system.assertEquals(400, testResponse.status.statusCd);
        system.assertEquals('ITP', testResponse.status.SubCode);        
        
    }
    

    
    
    /*
     * TEST 3: Check to make sure we get the correct status code for not able to find the Billing Account Number
     */    
    static testmethod void testInvalidEntry3BadBillingAccountNumber()
    {
        Test.startTest();
        
        system.debug('TEST 3: START ... testInvalidEntry3BadBillingAccountNumber' );
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        req.requestURI = '/Customer/Assets/Summaries';
        req.params.put('account', '101-9999999999' );


        req.resourcePath = '/services/apexrest/Customer/Assets/Summaries/*';
        req.httpMethod = 'GET';
                        
        RestContext.request = req;
        RestContext.response = res;
        
        NAAS_WS_CustomerAssetSummaryList.getAssets();
        
        Test.stopTest();   
        
        String jsondata  = res.responseBody.toString();
        
        // Make sure Status is 400
        CustomerAssetSummaryResponse testResponse = createCustomerAssetSummaryResponse(jsondata);
        
        system.debug('TEST 3: jsondata =' + jsondata );
        
        system.debug('TEST 3: testResponse.Status =' + testResponse.status );
        
        system.assertEquals(400, testResponse.status.statusCd);
        system.assertEquals('ANF', testResponse.status.SubCode);          
    }    




    
    
    /*
     * Test the private method createWebserviceLog in the parent class
     */    
    static testmethod void testCreateWebserviceLog()
    {
        system.debug('TEST: START ... testCreateWebserviceLog' );
    
        String superLargeString = constructTestString(40000);
        NAAS_WS_CustomerAssetSummaryList.createWebserviceLog('Success', superLargeString, superLargeString, superLargeString); 
    }    
    
    
    /*
     * Test to make sure you can retrieve the Billing System ID's from the custom metadata
     */
    static testmethod void testGetBillingSystemIdMap()
    {
        system.debug('TEST: START ... testGetBillingSystemIdMap' );
    
        // The test should check to make sure we have 101, 102 ... we just need to make sure there are some values there.
        Map<String,String> tempMap = NAAS_WS_CustomerAssetSummaryList.getBillingSystemIdMap();
        
        system.assert( tempMap != null && tempMap.size() > 0 );
    
    }
    
    
    /*
     * Test method isBcanNumberInSystemId
     */
    static testmethod void testIsBcanNumberInSystemId()
    {
        system.debug('TEST: START ... testIsBcanNumberInSystemId' );
    
        // The test should check to make sure we have 101, 102 ... we just need to make sure there are some values there.
        Map<String,String> tempMap = NAAS_WS_CustomerAssetSummaryList.getBillingSystemIdMap();
        
        system.assert( tempMap != null && tempMap.size() > 0 );
        
        String tempSystemID = tempMap.values().get(0);
        
        
        Boolean flag1 = NAAS_WS_CustomerAssetSummaryList.isBcanNumberInSystemId( tempMap, new String[]{ tempSystemID  + '-22222222222'} );
        
        system.assertEquals( true, flag1 );

        Boolean flag2 = NAAS_WS_CustomerAssetSummaryList.isBcanNumberInSystemId( tempMap, new String[]{ '999-22222222222', '888-222222222'} );
        
        system.assertEquals( false, flag2 );
        
    
    }    
    
    
    //================================================================================================================
    // HELPER METHODS - Used to assist in the test method executions
    //================================================================================================================
    
    
    /*
     * Create a big test string
     */
    private static String constructTestString(Integer stringSize) 
    {
        String temp = '';
        for (Integer i = 0; i < stringSize; i ++) 
        {
            temp += 'A';
        }

        return temp;
    }
        
    
    /*
     * Creates and returns a test child account
     */
    private static Account createTestChildAccount(String billingSystemCode, String bcanCode, String accountType, Id parentAccountId)
    {
    
        Account childAccount= new Account();
        childAccount.Name = 'BILLINGNAME ' + bcanCode;
        childAccount.BCAN__c = billingSystemCode + '-' + bcanCode;
        childAccount.Billing_System_ID__c = billingSystemCode;
        childAccount.CAN__c = bcanCode;
        childAccount.Operating_Name_DBA__c = 'OperatingName ' + bcanCode;
        childAccount.RecordTypeId = [select Id, Name from RecordType where Name =: accountType limit 1].id;
        childAccount.ParentId = parentAccountId;

        return childAccount;
    }     
    
    
    
    /*
     * Create and insert Assets and Products into database
     */
     private static void insertAssetsAndProducts(Id rcidAccountId, Id billingAccountId, String code)
     {
     
        Asset testAsset = new Asset();
        testAsset.Name = 'TEST Asset ' + code;
        testAsset.accountId = rcidAccountId;
        testAsset.orderMgmt_BPI_Id__c = code;
        
        //set up the product + product specification
        Product2 testProduct = new Product2();
        testProduct.Name = 'Managed Internet Basic';
               
        Product2 testProductSpec = new Product2();
        testProductSpec.Name = 'Managed Internet Product';
        testProductSpec.orderMgmtId__c = code;
        insert testProductSpec;
        
        testProduct.ProductSpecification__c = testProductSpec.id;        
        
        // Add catalog
        vlocity_cmt__Catalog__c testCatalog = new vlocity_cmt__Catalog__c();
        testCatalog.Name = 'NAAS';
        insert testCatalog;
        
        testProduct.Catalog__c = testCatalog.Id;
        insert testProduct;
        
        testAsset.Product2Id = testProduct.id;
        testAsset.vlocity_cmt__BillingAccountId__c = billingAccountId;
        testAsset.vlocity_cmt__ServiceAccountId__c = billingAccountId;
                testAsset.AccountId = billingAccountId;
        insert testAsset;
     
     }
   
   
   
   
    //================================================================================================================
    // HELPER OBJECTS - Used to represent the JSON object from the Customer Asset Summary REST Service
    //================================================================================================================
     
    /*
     * Create a CustomerAssetSummaryResponse; used for testing only
     */
    private static CustomerAssetSummaryResponse createCustomerAssetSummaryResponse(String json)
    {
        CustomerAssetSummaryResponse r = new CustomerAssetSummaryResponse(System.JSON.createParser(json));
        System.assert(r != null);
        
        return r;
    }
    
    
    /*
     * Consume the JSON Object
     */
    private static void consumeObject(JSONParser parser) {
        Integer depth = 0;
        do {
            JSONToken curr = parser.getCurrentToken();
            if (curr == JSONToken.START_OBJECT || 
                curr == JSONToken.START_ARRAY) {
                depth++;
            } else if (curr == JSONToken.END_OBJECT ||
                curr == JSONToken.END_ARRAY) {
                depth--;
            }
        } while (depth > 0 && parser.nextToken() != null);
    }    

    /*
     * Status class used to represent part of the JSON response; used for testing only
     */
    public class CustomerAssetSummaryResponse
    {
        public List<CustomerAssetSummary> customerAssetSummaryList {get;set;} 
        public Status status {get;set;} 
        
        
        // CONSTRUCTOR
        public CustomerAssetSummaryResponse(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'CustomerAssetSummaryList') {
                            customerAssetSummaryList = new List<CustomerAssetSummary>();
                            while (parser.nextToken() != JSONToken.END_ARRAY) {
                                customerAssetSummaryList.add(new CustomerAssetSummary(parser));
                            }
                        } else if (text == 'status') {
                            status = new Status(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'Root consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }        
  
    }


    /*
     * Status class used to represent part of the JSON response; used for testing only
     */
    private class Status 
    {
        public Integer statusCd {get;set;} 
        public String statusTxt {get;set;} 
        public String SubCode {get;set;} 

        public Status(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'statusCd') {
                            statusCd = parser.getIntegerValue();
                        } else if (text == 'statusTxt') {
                            statusTxt = parser.getText();
                        } else if (text == 'SubCode') {
                            SubCode = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Status consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }



    /*
     * CustomerAssetSummaryList class used to represent part of the JSON response; used for testing only
     */
    private class CustomerAssetSummary 
    {
        public String assetName {get;set;} 
        public Integer bpiIdentifier {get;set;} 
        public String productName {get;set;} 
        public String billing {get;set;} 
        public String service {get;set;} 
        public String productCategory {get;set;} 

        public CustomerAssetSummary(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'assetName') {
                            assetName = parser.getText();
                        } else if (text == 'bpiIdentifier') {
                            bpiIdentifier = parser.getIntegerValue();
                        } else if (text == 'productName') {
                            productName = parser.getText();
                        } else if (text == 'billing') {
                            billing = parser.getText();
                        } else if (text == 'service') {
                            service = parser.getText();
                        } else if (text == 'productCategory') {
                            productCategory = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'CustomerAssetSummary consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }

}