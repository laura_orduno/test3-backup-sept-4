@isTest
global class CreditProfileCalloutUpdateMock implements WebServiceMock
{
    global void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction,
                         String requestName, String responseNS, String responseName, String responseType)
    {
        if ( 'ping'.equalsIgnoreCase(requestName) ) {
            response.put('response_x', ping());
        }else if ( 'createBusinessCreditProfile'.equalsIgnoreCase(requestName)){
            response.put('response_x', createBusinessCreditProfile());
        }else if('getCreditReport'.equalsIgnoreCase(requestName)){
            response.put('response_x', getCreditReport());
        }else if('attachCreditReport'.equalsIgnoreCase(requestName)){
            response.put('response_x', attachCreditReport());
        }else if('voidCreditReport'.equalsIgnoreCase(requestName)){
            response.put('response_x', voidCreditReport());
        }
        
    }
    
    private CreditProfileServicePing.pingResponse_element ping()
    {
        CreditProfileServicePing.pingResponse_element 
            pingResponse = new CreditProfileServicePing.pingResponse_element();
        pingResponse.version = 'Mock-1.0';
        return pingResponse;
    }
    
    
    private CreditProfileServiceRequestResponse.createBusinessCreditProfileResponse_element 
        createBusinessCreditProfile() {
        
        CreditProfileServiceRequestResponse.createBusinessCreditProfileResponse_element 
            response = new CreditProfileServiceRequestResponse.createBusinessCreditProfileResponse_element();        
        return response;
    }
    
    
    private String attachCreditReport(){
        return null;
    }
    
    private CreditProfileServiceRequestResponse.getCreditReportResponse_element getCreditReport(){
        CreditProfileServiceRequestResponse.getCreditReport_element 
            request_x 
            = new CreditProfileServiceRequestResponse.getCreditReport_element();
            
        CreditProfileServiceRequestResponse.getCreditReportResponse_element 
            response_x
            = new CreditProfileServiceRequestResponse.getCreditReportResponse_element();
        
        CreditProfileServiceTypes.BusinessCreditReportFullDetails
            businessCreditReportFullDetails = new CreditProfileServiceTypes.BusinessCreditReportFullDetails();
        
        response_x.businessCreditReportFullDetails = businessCreditReportFullDetails;
        businessCreditReportFullDetails.documentId = 'abcd1234';
        businessCreditReportFullDetails.creditReportContent = 'creditReportContent';
        return response_x;
    }
        
    private CreditProfileServiceRequestResponse.voidCreditReportResponse_element voidCreditReport(){
        CreditProfileServiceRequestResponse.voidCreditReportResponse_element
            response_x
            = new CreditProfileServiceRequestResponse.voidCreditReportResponse_element();
        return response_x;
    }
    
}