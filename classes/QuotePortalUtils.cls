public class QuotePortalUtils {
    public class QuotePortalUtilsException extends Exception{}
    public static User me;
    public static void initme() {
        me = [SELECT Quoting_Dealer__c, Quoting_Experience_Excellence__c, Channel__c, Contact.AccountId, Contact.Channel_Outlet_ID__c, Assigned_Accounts__c FROM User WHERE Id = :UserInfo.getUserId()];
        if (me == null) {
            throw new QuotePortalUtilsException('Couldn\'t load the current user\'s information.');
        }
    }
    
    public static void addConfigurationSummaryParams(PageReference target) {
        target.getParameters().put('cs', '1');
        target.getParameters().put('csin', '1');
        target.getParameters().put('csgf', 'Summary_Category__c');
        target.getParameters().put('cstff', 'Monthly_Charge__c');
        target.getParameters().put('cstfv', 'Yes');
        target.getParameters().put('cstfp', '{0,number,currency}/month');
        target.getParameters().put('cspfp', '{0,number,currency}');
        target.getParameters().put('calculateBtnLbl', 'Update');
        target.getParameters().put('saveBtnLbl', 'Continue');
    }
    
    public static void addReturnURLParam(PageReference target, PageReference returnPage) {
        String step = ApexPages.currentPage().getParameters().get('step');
        String multiSite = ApexPages.currentPage().getParameters().get('ms');
        String addOn = ApexPages.currentPage().getParameters().get('addOn');
        String qid = ApexPages.currentPage().getParameters().get('qid');
        if (step != null) {
            returnPage.getParameters().put('step', step);
        }
        if (multiSite != null) {
            returnPage.getParameters().put('ms', multiSite);
        }
        if (addOn != null) {
            returnPage.getParameters().put('addon', addOn);
        }
        target.getParameters().put('retURL', Site.getCurrentSiteUrl() + returnPage.getUrl().replaceAll('/apex/',''));
    }
    
    public static Boolean isUserDealer() {
        if(me==null){initme();}
        return me.Quoting_Dealer__c == true;
    }
    
    public static Boolean isExperienceExcellenceUser() {
        if(me==null){initme();}
        return me.Quoting_Experience_Excellence__c == true;
    }
    
    public static Boolean isAssignedAccountsUser() {
        if(me==null){initme();}
        return me.Assigned_Accounts__c == true;
    }
    
    public static String getChannel() {
        if(me==null){initme();}
        if (me == null) { return ''; }
        if (me.Channel__c == null) { return ''; }
        return me.Channel__c;
    }
    
    public static String getDealershipId() {
        if(me==null){initme();}
        return (me.Contact != null) ? String.valueOf(me.Contact.AccountId).substring(0,15) : null;
    }
    
    public static String getChannelOutletId() {
        Cookie coi = ApexPages.currentPage().getCookies().get('channelOutletId');
        if ((coi != null) && (coi.getValue() != null) && (coi.getValue().trim() != '')) {
            return coi.getValue();
        }
        if(me==null){initme();}
        return (me.Contact != null) ? me.Contact.Channel_Outlet_ID__c : null;
    }
    
    @future
    public static void sendProvisioningEmail(Id quoteId) {
        SBQQ__Quote__c quote = [Select Send_Provisioning_Email_Flag__c From SBQQ__Quote__c Where Id = :quoteId Limit 1];
        quote.Send_Provisioning_Email_Flag__c = true;
        update quote;
        /*
        PageReference pref = Page.QuoteProvisionRequest;
        pref.getParameters().put('qid', quote.Id);
        pref.getParameters().put('pd', '1');
        String body = Test.isRunningTest() ? 'test body' : pref.getContent().toString();
        
        Id groupId = Quote_Portal__c.getInstance().Provision_Group_Id__c;
        Messaging.SingleEmailMessage[] emails = new Messaging.SingleEmailMessage[]{};
        for (GroupMember mem : [SELECT UserOrGroupId FROM GroupMember WHERE GroupId = :groupId]) {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTargetObjectId(mem.UserOrGroupId);
            mail.setSubject('TELUS Business Anywhere Bundle - Quote Accepted - Please Action ' + quote.Name);
            mail.setHtmlBody(body);
            mail.setSaveAsActivity(false);
            emails.add(mail);
            System.debug('Queued KANA email to: ' + mem.UserOrGroupId);
        }
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        //mail.setToAddresses(new String[]{quote.Provision_Request_Email__c});
        mail.setToAddresses(new String[]{Quote_Portal__c.getInstance().Provision_Team_Email__c});
        mail.setSubject('TELUS Business Anywhere Bundle - Quote Accepted - Please Action ' + quote.Name);
        mail.setHtmlBody(body);
        mail.setSaveAsActivity(false);
        emails.add(mail);
        System.debug('Queued KANA email to: ' + Quote_Portal__c.getInstance().Provision_Team_Email__c);
        //System.debug('Queued KANA email to: ' + quote.Provision_Request_Email__c);
        // Traction change - as seen above, this debug line was incorrect
        
        Messaging.sendEmail(emails); // Removed if test clause here in R5.1 */
    }
    
    public static void sendCompleteInformationEmail(SBQQ__Quote__c quote) {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        User user = [SELECT Id, ContactId FROM User WHERE Id = :quote.CreatedById];
        if(user.ContactId != null) {
            mail.setTargetObjectId(user.ContactId);
            mail.setWhatId(quote.Id);
            EmailTemplate template = [SELECT Id FROM EmailTemplate WHERE Id = :Quote_Portal__c.getInstance().Complete_Request_Email_Id__c];
            mail.setTemplateId(template.Id);
            mail.setSaveAsActivity(false);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
        }
    }
    
    public static void updateQuoteAddress(Web_Account__c acct, Id locationId, SBQQ__Quote__c quote) {
        quote.SBQQ__BillingName__c = acct.Name;
        quote.SBQQ__BillingStreet__c = acct.Billing_Address__c;
        quote.SBQQ__BillingCity__c = acct.Billing_City__c;
        quote.SBQQ__BillingState__c = acct.Billing_Province__c;
        quote.SBQQ__BillingPostalCode__c = acct.Billing_Postal_Code__c;
        quote.SBQQ__ShippingStreet__c = acct.Shipping_Address__c;
        quote.SBQQ__ShippingCity__c = acct.Shipping_City__c;
        quote.SBQQ__ShippingState__c = acct.Shipping_Province__c;
        quote.SBQQ__ShippingPostalCode__c = acct.Shipping_Postal_Code__c;
        quote.Install_Location__c = locationId;
    }
    
    public static SBQQ__QuoteLine__c[] loadQuoteLines(Id quoteId, Boolean includeDetails) {
        QueryBuilder qb = new QueryBuilder(SBQQ__QuoteLine__c.sObjectType);
        for (Schema.SObjectField field : Schema.SObjectType.SBQQ__QuoteLine__c.fields.getMap().values()) {
            qb.getSelectClause().addField(field);
        }
        qb.getSelectClause().addExpression(createAdditionalInfoQuery());
        qb.getSelectClause().addField(SBQQ__QuoteLine__c.SBQQ__Product__c, Product2.SBQQ__ConfigurationFields__c);
        qb.getWhereClause().addExpression(qb.eq(SBQQ__QuoteLine__c.SBQQ__Quote__c, quoteId));
        if (!includeDetails) {
            QueryBuilder.OrExpression oe = qb.lor();
            oe.addExpression(qb.eq(SBQQ__QuoteLine__c.SBQQ__Bundled__c, false));
            oe.addExpression(qb.lt(SBQQ__QuoteLine__c.SBQQ__OptionLevel__c, 2));
            qb.getWhereClause().addExpression(oe);
        }
        qb.getOrderByClause().addAscending(SBQQ__QuoteLine__c.SBQQ__OptionLevel__c);
        qb.getOrderByClause().addAscending(SBQQ__QuoteLine__c.Option_Order__c);
        return Database.query(qb.buildSOQL());
    }
    
    private static QueryBuilder createAdditionalInfoQuery() {
        QueryBuilder qb = new QueryBuilder('AdditionalInformation__r');
        for (Schema.SObjectField field : Schema.SObjectType.AdditionalInformation__c.fields.getMap().values()) {
            if (field.getDescribe().isCustom()) {
                qb.getSelectClause().addField(field);
            }
        }
        return qb;
    }
    
    public static QuoteLineVO[] sortLines(QuoteLineVO[] lines) {
        Map<Id,QuoteLineVO> idx = new Map<Id,QuoteLineVO>();
        for (QuoteLineVO vo : lines) {
            idx.put(vo.line.Id, vo);
        }
        
        for (QuoteLineVO vo : lines) {
            if (vo.line.SBQQ__RequiredBy__c != null) {
                QuoteLineVO parent = idx.get(vo.line.SBQQ__RequiredBy__c);
                parent.components.add(vo);
            }
        }
        
        QuoteLineVO[] result = new List<QuoteLineVO>();
        for (QuoteLineVO vo : lines) {
            if (vo.line.SBQQ__RequiredBy__c == null) {
                result.addAll(vo.getAll());
            }
        }
        return result;
    }
    
    public static SBQQ__Quote__c loadQuoteById(Id quoteId) { 
        return QuoteUtilities.loadQuote(quoteId);
    }
    
    public static Web_Account__c loadAccountById(Id accountId) {
        if (accountId == null) { return null; } // traction change
        String[] fields = new List<String>();
        for (Schema.SObjectField field : Schema.SObjectType.Web_Account__c.fields.getMap().values()) {
            fields.add(field.getDescribe().getName());
        }
        return Database.query('Select ' + String.join(fields, ', ') + ' from Web_Account__c Where Id = \''+accountId+'\'');
    }
    
    public static List<SObjectField> getAdditionalInfoFieldList() {
        List<SobjectField> fieldList = new List<SobjectField>();
        fieldList.add(AdditionalInformation__c.Phone_Number_Setup__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Phone_Number_Setup_QED__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Service_Term__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Service_Term_QED__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.First_Name__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Last_Name__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Network_Type__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.SIM__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.ESN_IMEI__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Language__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.CRIS_Order_Number__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.City_of_Use__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.City_Of_Use__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Directory_Listing_Display__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Directory_Listing__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Directory_Listing_Same_As_Business_Name__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Billing_Telephone_Number_BTN__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Old_Service_Provider_Name__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Porting__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Street__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.City__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Make__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Model__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Old_Service_Provider__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Old_Service_Provider_Equipment_Serial_No__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Old_Service_Provider_Account_Number__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Old_Service_Provider_Name_Billing_Name__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Postal_Code__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Demarc_And_Tag_Only__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Inside_Wiring_Req_d__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Number_of_Jacks_required__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Number_Of_Jacks_Required__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.All_Associated_numbers__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Mobile__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Special_Instructions__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Long_Distance_Winback__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Long_Distance_Calling_Cards__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Name_To_Appear_On_LD_Calling_Card__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Existing_LD_contract__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Cancel_existing_LD_contract__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Existing_LD_contract_number__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Destination_Number__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Existing_LD_Contract__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Cancel_Existing_LD_Contract__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Existing_LD_Contract_Number__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Toll_Free__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Toll_Free_Terminating_Number__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Specify_Existing_Terminating_Number__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Toll_Free_Coverage_Includes__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Specify_Toll_Free_provinces_and_or_state__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Toll_Free_Number_in_Directory_Assistance__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Name_to_appear_in_Directory_Assistance__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Customer_requests_custom_Domain_Name__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Domain__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.A_Record_For_Workspace_Only__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.User_Has_Existing_e_mail_Address__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Wish_to_Mantain_Existing_e_mail_Seats__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Website_is__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Existing_Domain_Name__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.New_Domain_Name_Option_1__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.New_Domain_Name_Option_2__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.New_Domain_Name_Option_3__c.getDescribe().getSObjectField());
        fieldList.add(AdditionalInformation__c.Password__c.getDescribe().getSObjectField());
        return fieldList;               
    }
}