/**
 *  ENTPNewCaseCtlr.cls
 *
 *  @description
 *      New Case Controller for the Enterprise Site.  
 *
 *  @date - 08/01/2016
 *  @author - Rob Brekke: Telus
 */

public with sharing class ENTPNewCaseCtlr {
/*****    
    public class NewCaseException extends Exception {}
 String userAccountId;
     public string CatListPull = '';
    private static final Map<String, String> requestTypeRecordTypeMap;
    private static final Map<String, String> recordTypeRequestTypeMap;
    private static final Map<String, Id> caseRTNameToId;
    private static final Map<String, List<String>> typeFieldsOrder;
    private static final Map<String, List<ParentToChild__c>> ptcListByCategoryMap;

    private static final Map<String, List<String>> categoryRequestTypesMap;
    private static final Map<String, String> requestTypesCategoryMap;
    public string LynxTickNum {get;set;}
    public static Map<String, String> reqTypeFormIdMap {get;set;}
    public String reqTypeFormIdMapJSON {get{return JSON.serialize(reqTypeFormIdMap);}}
    public static User user{get;set;}
   

    public Boolean createNewOnSubmit {get {
        if (createNewOnSubmit == null) return false;
        return createNewOnSubmit;
    } set;}
    
    static {
        List<TypeToFields__c> ttfs = TypeToFields__c.getAll().values();
        typeFieldsOrder = new Map<String, List<String>>();
                
        List<ExternalToInternal__c> etis = ExternalToInternal__c.getAll().values();
        requestTypeRecordTypeMap = new Map<String, String>();
        recordTypeRequestTypeMap = new Map<String, String>();
        for (ExternalToInternal__c eti : etis) {
            if(eti.Identifier__c!=null && eti.Identifier__c.equals('onlineRequestTypeENTP')) {
                requestTypeRecordTypeMap.put(eti.External__c,eti.Internal__c);
                recordTypeRequestTypeMap.put(eti.Internal__c,eti.External__c);
                system.debug('requestTypeRecordTypeMap: '+requestTypeRecordTypeMap.put(eti.External__c,eti.Internal__c)); 
               system.debug('recordTypeRequestTypeMap: '+recordTypeRequestTypeMap.put(eti.Internal__c,eti.External__c)); 
            }
        }
        
        List<ParentToChild__c> ptcs = ParentToChild__c.getAll().values();
    	ptcListByCategoryMap = new Map<String, List<ParentToChild__c>>();
        categoryRequestTypesMap = new Map<String, List<String>>();
        requestTypesCategoryMap = new Map<String, String>();
        reqTypeFormIdMap = new Map<String, String>();
        reqTypeFormIdMap.put(label.mbrSelectType, '');
        for (ParentToChild__c ptc : ptcs) {
            if(ptc.Identifier__c.equals('ENTPCatRequest')) {
				// build ptc list map so it can be used for sorting the request type(child) entries                
                if(!ptcListByCategoryMap.containsKey(ptc.parent__c)){
                    ptcListByCategoryMap.put(ptc.parent__c, new List<ParentToChild__c>());
                }    
                List<ParentToChild__c> ptcListForCategory = ptcListByCategoryMap.get(ptc.parent__c);    
                ptcListForCategory.add(ptc);
                ptcListByCategoryMap.put(ptc.parent__c, ptcListForCategory);
                ////////////////////////////////////////////////////////////////////////////////////
                requestTypesCategoryMap.put(ptc.Child__c, ptc.Parent__c);
                if(ptc.FormComponent__c==null || ptc.FormComponent__c.equals('')){
                    reqTypeFormIdMap.put(ptc.Child__c, 'descriptionField');
                }
                else{
                    reqTypeFormIdMap.put(ptc.Child__c, ptc.FormComponent__c);
                }
            }
        }
        
        // perform ptc list sorting for each category
       	Map<Integer, ParentToChild__c> ptcOrderedMap = new Map<Integer, ParentToChild__c>();
        List<ParentToChild__c> ptcOrphanedList = new List<ParentToChild__c>();
        
        for(String category : ptcListByCategoryMap.keyset()) {    
            Integer minOrderNumber = null;
            Integer maxOrderNumber = null;
            
            ptcOrderedMap.clear();
            ptcOrphanedList.clear();
        	List<ParentToChild__c> ptcSortedList = new List<ParentToChild__c>();
            
            for(ParentToChild__c ptc : ptcListByCategoryMap.get(category)) {
                Integer orderNumber = Integer.valueOf(ptc.order_number__c);        
                // init
                minOrderNumber = minOrderNumber == null ? orderNumber : minOrderNumber;
                maxOrderNumber = maxOrderNumber == null ? orderNumber : maxOrderNumber;                
                if(orderNumber == null || ptcOrderedMap.containsKey(orderNumber)){
                    ptcOrphanedList.add(ptc);
                }
                else{
                    // update
                    minOrderNumber = orderNumber < minOrderNumber ? orderNumber : minOrderNumber;
                    maxOrderNumber = orderNumber > maxOrderNumber ? orderNumber : maxOrderNumber;
                    ptcOrderedMap.put(orderNumber, ptc);
                }        
            }
        
            for(Integer key=minOrderNumber;key<=maxOrderNumber;key++){
                if(ptcOrderedMap.containsKey(key)){
                    ParentToChild__c ptc = ptcOrderedMap.get(key);
                    ptcSortedList.add(ptc);
                }
            }
                
            ptcSortedList.addAll(ptcOrphanedList);     

			List<String> ptcSortedStringList = new List<String>();           
            for(ParentToChild__c ptc : ptcSortedList){
				ptcSortedStringList.add(ptc.child__c);
    		}            
            categoryRequestTypesMap.put(category, ptcSortedStringList);            
        }
        /////////////////////////////////////////////////////////////////////////////////////
        
        
        for(String s:categoryRequestTypesMap.keySet()){
            system.debug('categoryRequestTypesMap: '+s+' values: '+categoryRequestTypesMap.get(s));
        }
        
        Schema.DescribeSObjectResult caseToken = Schema.SObjectType.Case;
        Map<String, RecordTypeInfo> nameToRTInfo = caseToken.getRecordTypeInfosByName();
        caseRTNameToId = new Map<String, Id>();
        for (String s : nameToRTInfo.keyset()) {
            caseRTNameToId.put(s, nameToRTInfo.get(s).getRecordTypeId());
        }
    }

    public String selectedCategory {get; set;} { selectedCategory = Label.MBRSelectCategory; }
    
    public Case caseToInsert {get; set;}
    public static String parentCaseType;
    public static String parentCategory;
    public ENTPFormWrapper formWrapper {get; set;}
    public Account account {get; private set;}
    public Boolean caseSubmitted {get; set;}
   public String requestType {get; 
        set{
            requestType = value;
            if(requestTypeRecordTypeMap.get(requestType) != null && caseRTNameToId.get(requestTypeRecordTypeMap.get(requestType)) != null) {
                caseToInsert.recordtypeid = caseRTNameToId.get(requestTypeRecordTypeMap.get(requestType));
            }    
        } 
    } { requestType = 'ENT Care Assure'; }
    
    public Attachment attachment {
        get {
            if (attachment == null)
                attachment = new Attachment();
            return attachment;
        }
        set;
    }
    
    public ENTPNewCaseCtlr() {
        priorityError = '';
        typeError = '';
        formWrapper = new ENTPFormWrapper();
     
        VITILCare_Customer_Interface_Settings__c cis = VITILCare_Customer_Interface_Settings__c.getInstance();
        if(cis.Case_Origin_Value__c != null) {
System.debug('CASE ORIGIN SETTING: ' + cis.Case_Origin_Value__c);            
            caseToInsert = new case(Origin = 'ENTP', NotifyCustomer__c=true);
        }
        else {
System.debug('CASE ORIGIN: ENTP');            
            caseToInsert = new case(Origin = 'ENTP', NotifyCustomer__c=true);
        }
        caseSubmitted = false;


       if (ApexPages.currentPage() != null) {
            String parentCaseNumber = ApexPages.currentPage().getParameters().get('reopen');
            if (parentCaseNumber != null) {
                List<Case> parentCaseList = [SELECT Id, Subject, Description, My_Business_Requests_Type__c, LastModifiedDate, NotifyCollaboratorString__c, CreatedDate, CaseNumber, recordType.name, Type, Status, Lynx_Ticket_Number__c FROM Case WHERE CaseNumber = :parentCaseNumber AND Status = 'Closed'];
                if(parentCaseList.size() > 0) {
                    caseToInsert.Parent = parentCaseList[0];
                    LynxTickNum = parentCaseList[0].Lynx_Ticket_Number__c;
                    System.debug('LynxTicketNumber: ' + LynxTickNum);
                    caseToInsert.ParentId = parentCaseList[0].Id;
                    caseToInsert.Subject = Label.VITILcareRequestSubjectreopen +  LynxTickNum + '] ' + caseToInsert.Parent.Subject;
                    List<String> lines = new List<String>();
                    if (caseToInsert.Parent.Description != null) {
                        lines = caseToInsert.Parent.Description.split('\n');
                    }
                    String description = '';
                    for (String s : lines) {
                        description += '\n> ' + s;
                    }
                    caseToInsert.Description = '\n\n\n\n' + Label.VITILcareSubmitRequestReopenDescp + LynxTickNum + '\n>' + description;
                    caseToInsert.NotifyCollaboratorString__c = caseToInsert.Parent.NotifyCollaboratorString__c;
                    parentCaseType = caseToInsert.Parent.My_Business_Requests_Type__c;
                    System.debug('requestTypesCategoryMap: ' + requestTypesCategoryMap);
                     System.debug('parentCaseType: ' + parentCaseType);
                    if (parentCaseType != null && requestTypesCategoryMap.get(parentCaseType) != null) {
                        parentCategory = requestTypesCategoryMap.get(parentCaseType);
                        selectedCategory = parentCategory;
                        requestType = parentCaseType;
                    }
                } else {
                    // invalid parent caseNumber specified...
                }
            }
        }
    }
    
    public transient String pageValidationError {get; set;}
    public transient String priorityError {get; set;}
    public transient String typeError {get; set;}


    
    public PageReference initCheck() {
        //String userAccountId;
        //userAccountId = (String)UserUtil.CurrentUser.AccountId;
        String userAccountId;
        List<User> users = [SELECT Id, AccountId FROM User WHERE Id = :UserInfo.getUserId()];
        if (users.size() > 0) {
            userAccountId = users[0].AccountId;
            System.debug('Line 223: Account ID: ' + userAccountId);
        }
        Id aid = (Id) userAccountId;
        if (caseToInsert.Parent == null) {
            System.debug('Line 227: caseToInsert.Parent: ' + caseToInsert.Parent);
            resetValues();
        }
        System.debug('Line 230: Account ID: ' + aid);
        caseToInsert.AccountId = aid;
        return null;
    }

    public void resetValues(){
        caseToInsert.description = '';
        caseToInsert.subject = '';
       requestType = 'ENT Care Assure';//Label.MBRSelectType;
        String categoryType = ApexPages.currentPage().getParameters().get('category'); 
        if(caseSubmitted){
            selectedCategory = Label.MBRSelectCategory;
        }
        else if(categoryType!=null){
            ParentToChild__c categType = ParentToChild__c.getAll().get(categoryType);
            selectedCategory = categType.Parent__c;
            categoryType = null;
        }
    }

    public String getRecordType(String externalName){        
        
        String typeName = 'ENT Care Assure';

        return typeName;
    }
    
    public PageReference createNewCase() {
System.debug('ENTPNewCase::createNewCase, caseToInsert: ' + caseToInsert);        
        PageReference submitNewPage = Page.ENTPNewCase;
        submitNewPage.setRedirect(true);
        submitNewPage.getParameters().put('previousCaseSubmitted', '1');

        if(caseToInsert==null) return null;


        String recType = getRecordType(requestType);
//System.debug('Schema.SObjectType.Case.RecordTypeInfosByName: ' + Schema.SObjectType.Case.RecordTypeInfosByName);        
        Id recTypeId = Schema.SObjectType.Case.RecordTypeInfosByName.get(recType).RecordTypeId;
//System.debug('recTypeId: ' + recTypeId);        
        if(recTypeId != null){
            caseToInsert.recordtypeid = recTypeId;
        }
        
        if(caseToInsert.recordtypeid == null) {
            pageValidationError = label.mbrMissingRecordType;
System.debug('ENTPNewCase::createNewCase, error: ' + pageValidationError);        
            return null;
        }
        
        caseToInsert.My_Business_Requests_Type__c = requestType;
    
       
       ENTPFormWrapper.GetTicketInfoAct getTicketInfo = formWrapper.getTicketInfo;
    System.debug('Subject: ' +  getTicketInfo.subject);
       string ticketType = '';
        //append request type in front of subject
        if(caseToInsert.subject==null || caseToInsert.subject.equals('')){
           
            caseToInsert.Subject = getTicketInfo.subject;
        }
        
        if(caseToInsert.Project_number__c == null || caseToInsert.Project_number__c.equals('')) {
            caseToInsert.Project_number__c = getTicketInfo.projectNum;
            ticketType = 'Mainstream';
        } 
        if(caseToInsert.Project_number__c == null || caseToInsert.Project_number__c.equals('')) {
            caseToInsert.Project_number__c = getTicketInfo.CSID;
            ticketType = 'Enhanced';
        } 
        if(caseToInsert.Project_number__c == null || caseToInsert.Project_number__c.equals('')) {
            caseToInsert.Project_number__c = getTicketInfo.phNum;
            ticketType = 'Voice';
        } 
        if(caseToInsert.Project_number__c == null || caseToInsert.Project_number__c.equals('')) {
            caseToInsert.Project_number__c = getTicketInfo.PRINum;
            ticketType = 'Voice-PRI';
        } 
        if(caseToInsert.Project_number__c == null || caseToInsert.Project_number__c.equals('')) {
            caseToInsert.Project_number__c = getTicketInfo.circuitNum;
            ticketType = 'L1L';
        } 
         if(caseToInsert.Contact_Phone_Number2__c == null || caseToInsert.Contact_Phone_Number2__c.equals('')){
            
            caseToInsert.Contact_Phone_Number2__c = getTicketInfo.OnsiteContactNumber;
        }
        
        // Added for running Case Assignment Rules -- Dan
        Database.DMLOptions dml = new Database.DMLOptions();
        dml.AssignmentRuleHeader.useDefaultRule = true;
        if(caseToInsert.description==null || caseToInsert.description.equals('')){
            caseToInsert.description = '';
            caseToInsert.description += '\n' + populateDescription(requestType); }
        caseToInsert.Ticketing_System__c = 'LYNX';
        
        Database.Saveresult sr = Database.insert(caseToInsert, dml);
        
        if (!sr.isSuccess()) {
            for (Database.Error error : sr.getErrors()) {
                pageValidationError = error.getMessage();
            }
System.debug('ENTPNewCase::createNewCase, DML error: ' + pageValidationError);        
        }

        if ( caseToInsert.Id == null){ 
System.debug('ENTPNewCase::createNewCase, DML error: case id null');        
            return null; 
        } else {
            if(attachment.body != null && attachment.body.size() > 0) {
                attachment.OwnerId = UserInfo.getUserId();
                attachment.ParentId = caseToInsert.id;
                attachment.IsPrivate = false;
                caseToInsert.description += '\n\n' + Label.MBR_See_Attachment + attachment.name;
                try {
                  insert attachment;
                  update caseToInsert;
                } catch (DMLException e) {
                  ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
                  return null;
                } finally {
                  attachment = new Attachment(); 
                }
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
            }


           SendLynxCreate(caseToInsert.id, ticketType); 
            
            PageReference page = System.Page.ENTPCaseDetailPage;
            Case c = [Select casenumber from Case where id = :caseToInsert.id LIMIT 1];
            page.getParameters().put('caseNumber', c.casenumber);
            page.setRedirect(true);
            caseSubmitted = true;
            caseToInsert = new case();
            selectedCategory = label.mbrSelectCategory;

System.debug('ENTPNewCase::return: ' + createNewOnSubmit);        
            
            return createNewOnSubmit ? submitNewPage : page;
        }    
    }
        
    public String populateDescription(String reqType){
        String userLanguage;
        if(userLanguage == null) {
        userLanguage = [SELECT LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()].LanguageLocaleKey;
    }
        Integer index = 1;
        String description = '';
        String formId = reqTypeFormIdMap.get(reqType);
         ENTPFormWrapper.GetTicketInfoAct getTicketInfo = formWrapper.getTicketInfo;

         if(true){
             if(userLanguage == 'en_US'){
                  if(getTicketInfo.VoiceIssueType.size() > 0){
                     description = '\n <br/>Voice Issues: ' + getTicketInfo.VoiceIssueType;
            }
                 if(String.isNotBlank(getTicketInfo.subject)){
                     description += '\n\n<br/>Subject: ' + getTicketInfo.subject;
                 }
              
                 if(String.isNotBlank(getTicketInfo.additionalNote)){
                     description += '\n<br/> Comments: ' + getTicketInfo.additionalNote;
                 }
                 if(String.isNotBlank(getTicketInfo.projectNum)){
                     description += '\n\n<br/>Circuit Number: ' + getTicketInfo.projectNum;
                 } 
                 if(String.isNotBlank(getTicketInfo.CSID)){
                     description += '\n\n<br/>CSID: ' + getTicketInfo.CSID;
                 }  
                 if(String.isNotBlank(getTicketInfo.phNum)){
                     description += '\n\n<br/>Phone Number affected: ' + getTicketInfo.phNum;
                 }  
                 if(String.isNotBlank(getTicketInfo.PRINum)){
                     description += '\n\n<br/>PRI#: ' + getTicketInfo.PRINum;
                 } 
                 if(String.isNotBlank(getTicketInfo.circuitNum)){
                     description += '\n\n<br/>Circuit Number#: ' + getTicketInfo.circuitNum;
                 }  
                 
                 if(String.isNotBlank(getTicketInfo.siteAddress)){
                     description += '\n<br/> Site address: ' + getTicketInfo.siteAddress;
                 }
                 
                 if(String.isNotBlank(getTicketInfo.ReportedBy)){
                     description += '\n<br/> Reported by name: ' + getTicketInfo.ReportedBy;
                 }
                 if(String.isNotBlank(getTicketInfo.ReportedByCompany)){
                     description += '\n <br/>Reported by company: ' + getTicketInfo.ReportedByCompany;
                 }
                 if(String.isNotBlank(getTicketInfo.ReportedByPhNum)){
                     description += '\n <br/>Reported by phone number: ' + getTicketInfo.ReportedByPhNum;
                 }
                 if(String.isNotBlank(getTicketInfo.ReportedByEm)){
                     description += '\n <br/>Reported by email address: ' + getTicketInfo.ReportedByEm;
                 }
                 if(String.isNotBlank(getTicketInfo.OnsiteContactName)){
                     description += '\n <br/>Onsite contact name: ' + getTicketInfo.OnsiteContactName;
                 }
                 if(String.isNotBlank(getTicketInfo.OnsiteContactNumber)){
                     description += '\n<br/> Onsite contact phone number: ' + getTicketInfo.OnsiteContactNumber;
                 }
                 if(String.isNotBlank(getTicketInfo.SiteCompanyName)){
                     description += '\n<br/> Site company name: ' + getTicketInfo.SiteCompanyName;
                 }
                 if(String.isNotBlank(getTicketInfo.AccessHours)){
                     description += '\n<br/> Access hours/days: ' + getTicketInfo.AccessHours;
                 }
                 
                 description += '\n<br/> Customer ticket number: ' + getTicketInfo.CustomerTicketNum;
                 
                 if(String.isNotBlank(getTicketInfo.HazSafIssue)){
                     description += '\n <br/>Is this a hazardous or safety issue?: ' + getTicketInfo.HazSafIssue;
                 }
                 if(String.isNotBlank(getTicketInfo.IntrusiveTest)){
                     description += '\n <br/>Intrusive testing OK?: ' + getTicketInfo.IntrusiveTest;
                 }
                 if(String.isNotBlank(getTicketInfo.NoIntrusiveTestText)){
                     description += '\n <br/>Intrusive testing window: ' + getTicketInfo.NoIntrusiveTestText;
                 }
                 if(String.isNotBlank(getTicketInfo.PowerIssue)){
                     description += '\n <br/>Has Power to site been verified?: ' + getTicketInfo.PowerIssue;
                 }
                 if(String.isNotBlank(getTicketInfo.PowerToTelusEquip)){
                     description += '\n <br/>Has power to onsite TELUS equipment been verified?: ' + getTicketInfo.PowerToTelusEquip;
                 }
                 if(String.isNotBlank(getTicketInfo.TELUSEquipConn)){
                     description += '\n<br/> Have onsite TELUS equipment connections been verified?: ' + getTicketInfo.TELUSEquipConn;
                 }
                 if(String.isNotBlank(getTicketInfo.CirUsedFor)){
                     description += '\n<br/> Circuit is used for?: ' + getTicketInfo.CirUsedFor;
                 }
                 if(String.isNotBlank(getTicketInfo.HowManyUsers)){
                     description += '\n<br/> How many users affected?: ' + getTicketInfo.HowManyUsers;
                 }
                 if(String.isNotBlank(getTicketInfo.IsBackupThere)){
                     description += '\n <br/>Is there backup?: ' + getTicketInfo.IsBackupThere;
                 }
                 if(String.isNotBlank(getTicketInfo.TypeOfCircuit)){
                     description += '\n<br/> What type of circuit?: ' + getTicketInfo.TypeOfCircuit;
                 }
                 if(String.isNotBlank(getTicketInfo.ConditionTypes)){
                     description += '\n<br/> Condition?: ' + getTicketInfo.ConditionTypes;
                 }
                 if(String.isNotBlank(getTicketInfo.OutageSig)){
                     description += '\n<br/> How significant is the outage to your business?: ' + getTicketInfo.OutageSig;
                 }
                 if(String.isNotBlank(getTicketInfo.SitesImpacted)){
                     description += '\n<br/> How many sites are impacted by this outage?: ' + getTicketInfo.SitesImpacted;
                 }
                 if(String.isNotBlank(getTicketInfo.ServiceEverWorked)){
                     description += '\n<br/> Has the service ever worked?: ' + getTicketInfo.ServiceEverWorked;
                 }
                 if(String.isNotBlank(getTicketInfo.JacksAffected)){
                     description += '\n <br/>Jacks affected?: ' + getTicketInfo.JacksAffected;
                 }
                 if(String.isNotBlank(getTicketInfo.SetsAffected)){
                     description += '\n <br/>Sets affected?: ' + getTicketInfo.SetsAffected;
                 }
                 if(String.isNotBlank(getTicketInfo.TestResults)){
                     description += '\n<br/> Test Results: ' + getTicketInfo.TestResults;
                 }
                 if(String.isNotBlank(getTicketInfo.AssocPhoneNumber)){
                     description += '\n <br/>Asscociated phone number: ' + getTicketInfo.AssocPhoneNumber;
                 }
                 if(String.isNotBlank(getTicketInfo.POI)){
                     description += '\n <br/>POI: ' + getTicketInfo.POI;
                 }
                 if(String.isNotBlank(getTicketInfo.CentralOffice)){
                     description += '\n <br/>Central Office: ' + getTicketInfo.CentralOffice;
                 }
                 if(String.isNotBlank(getTicketInfo.SchdDateOfInstall)){
                     description += '\n <br/>Scheduled date of install : ' + getTicketInfo.SchdDateOfInstall;
                 }
                 if(String.isNotBlank(getTicketInfo.SPID)){
                     description += '\n <br/>SPID : ' + getTicketInfo.SPID;
                 }
                 if(String.isNotBlank(getTicketInfo.FailedProvisioning)){
                     description += '\n <br/>Failed Provisioning? : ' + getTicketInfo.FailedProvisioning;
                 }
                 
                 index++;
             }
       
             if(userLanguage == 'fr'){
             if(String.isNotBlank(getTicketInfo.subject)){
                 description = '\n\nSubject: ' + getTicketInfo.subject;
             }  
             if(String.isNotBlank(getTicketInfo.projectNum)){
                 description += '\n\nNuméro de projet: ' + getTicketInfo.projectNum;
             }             
             if(String.isNotBlank(getTicketInfo.phoneNumber)){
                 description += '\n Numéros de téléphone/appareils touchés: ' + getTicketInfo.phoneNumber;
             } 
               
             if(String.isNotBlank(getTicketInfo.siteAddress)){
                 description += '\n Adresse du site: ' + getTicketInfo.siteAddress;
             }
             if(String.isNotBlank(getTicketInfo.additionalNote)){
                 description += '\n Commentaires: ' + getTicketInfo.additionalNote;
             }
             if(String.isNotBlank(getTicketInfo.ReportedBy)){
                 description += '\n Signalé par nom: ' + getTicketInfo.ReportedBy;
             }
             if(String.isNotBlank(getTicketInfo.ReportedByCompany)){
                 description += '\n Signalé par l\'entreprise: ' + getTicketInfo.ReportedByCompany;
             }
             if(String.isNotBlank(getTicketInfo.ReportedByPhNum)){
                 description += '\n Signalé par téléphone: ' + getTicketInfo.ReportedByPhNum;
             }
             if(String.isNotBlank(getTicketInfo.ReportedByEm)){
                 description += '\n Signalé par adresse de courriel: ' + getTicketInfo.ReportedByEm;
             }
             if(String.isNotBlank(getTicketInfo.OnsiteContactName)){
                 description += '\n Personne responsable sur place nom: ' + getTicketInfo.OnsiteContactName;
             }
              if(String.isNotBlank(getTicketInfo.OnsiteContactNumber)){
                 description += '\n Personne responsable sur place téléphone: ' + getTicketInfo.OnsiteContactNumber;
             }
             if(String.isNotBlank(getTicketInfo.SiteCompanyName)){
                 description += '\n Nom de l\'entreprise sur le site: ' + getTicketInfo.SiteCompanyName;
             }
             if(String.isNotBlank(getTicketInfo.AccessHours)){
                 description += '\n Heures et jours d\'accès: ' + getTicketInfo.AccessHours;
             }
                    description += '\n Numéro du billet du client: ' + getTicketInfo.CustomerTicketNum;
             if(String.isNotBlank(getTicketInfo.OngoingCalls)){
            
                 description += '\n Le client est en mesure d\'établir des connexions sortantes?: ' + getTicketInfo.OngoingCalls;
             } 
            
             if(String.isNotBlank(getTicketInfo.AllSetsAffected)){
                 description += '\n Est-ce que tous les appareils sont touchés?: ' + getTicketInfo.AllSetsAffected;
             }
             if(String.isNotBlank(getTicketInfo.PowerIssue)){
                 description += '\n Avez-vous des problèmes d\'alimentation récurrents ou en avez-vous eu récemment?: ' + getTicketInfo.PowerIssue;
             }
             index++;
                  
         }
            
         }
        
       
       return description;
    }
 @future(callout=true)    
        public static void SendLynxCreate(String caseID, string ticketType)
        {
            System.debug('Line 452 select: ' + caseID);
            case s = [Select Subject, casenumber, Description, Project_number__c, Contact_Phone_Number2__c, Id from Case where id = :caseID LIMIT 1];
            System.debug('Line 454 Select: ' + s);
            System.debug('Line 455 Subject: ' + s.Subject);
            System.debug('Line 456 casenumber: ' + s.casenumber);
            System.debug('Line 457 Description: ' + s.Description);
            System.debug('Line 458 Project Number: ' + s.Project_number__c);
            System.debug('Line 459 Phone Number: ' + s.Contact_Phone_Number2__c);
            //Rob - 01/19/2016 : Make the call to VITILcare_LynxCreate to send info to Lynx Web Service
            LynxCreateTicket.sendRequest smbCallOut = new LynxCreateTicket.sendRequest();
            smbCallOut.SRequest(s.Subject, s.Description, s.Project_number__c, s.casenumber, UserInfo.getFirstName(),UserInfo.getLastName(), s.Contact_Phone_Number2__c, userInfo.getUserEmail(), caseID, ticketType);
           
            
            //VITILcare_TTODS_CreateHelper smbCallOut = new VITILcare_TTODS_CreateHelper();
            //smbCallOut.AddTickets('This is a test','Another Test','brekke','5555555555');
               
        }

    public List<SelectOption> getPaymentOptions(){
         List<SelectOption> paymentTypes = new List<SelectOption>();
         paymentTypes.add(new SelectOption('charge to airtime account', 'charge to airtime account'));
         paymentTypes.add(new SelectOption('existing hardware account', 'existing hardware account'));
         paymentTypes.add(new SelectOption('other', 'other'));
        return paymentTypes;
    }
    
    
    public void clear() {
        caseToInsert = null;
    }
   public String[] getCATLISTPULL() {
        
        userAccountId = [SELECT AccountId FROM User WHERE Id = :UserInfo.getUserId()].AccountId;
        
       List<Account_Configuration__c> AccountConfig = [SELECT Id, TELUS_Product__c  FROM Account_Configuration__c WHERE Account__c =:userAccountId];
       system.debug('AccountConfig[0].TELUS_Product__c: ' + AccountConfig[0].TELUS_Product__c);
       
        CatListPull = AccountConfig[0].TELUS_Product__c;
        
        String[] CatListPullT = CatListPull.split(';');
         
        system.debug('CatListPullT: ' +  CatListPullT);
        
        return CatListPullT;
       
    }
    
    Public void setCATLISTPULL(String catListPull) {
        catListPull = CatListPull;
    }   
    public List<SelectOption> getRequestTypes() {
      Map<String, List<String>> rts = categoryRequestTypesMap; 
        List<SelectOption> options = new List<SelectOption>();
        selectedCategory = 'Enterprise';
      List<String> temps = categoryRequestTypesMap.get(selectedCategory);
      //  List<String> temps = 'Enterprise';
      if(temps == null) { 
           //options.add(new SelectOption(label.mbrSelectCategoryFirst,label.mbrSelectCategoryFirst));
        options.add( new SelectOption(label.mbrSelectType, label.mbrSelectType) );
          
            return options;
      }
     //   } 
//        temps.sort();
      Set<String> tempsSet = new Set<String>(temps);
        if(!tempsSet.contains(requestType)) {
            requestType = label.mbrSelectType;
        }
        // -- Added by DR, Traction 09-10-2014 --/
        options.add( new SelectOption(label.mbrSelectType, label.mbrSelectType) );
        // -- End Added by DR, Traction 09-10-2014 --/
        for(String temp : temps) {
            options.add(new SelectOption(temp,temp));
        } 
         options.add( new SelectOption('Other', 'Other (will initiate a chat)') );
        return options;
    }
    
    public List<SelectOption> getCategories() {
        Map<String, List<String>> rts = categoryRequestTypesMap;
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption(label.mbrSelectCategory, label.mbrSelectCategory));
        for(String temp : rts.keySet()){
            if (!temp.equals('Tech support') || !MBRUtils.restrictTechSupport()) {
                options.add(new SelectOption(temp,temp));
            }
        }
        return options;
    }

    public Case getParentCase() {
        return caseToInsert.Parent;
    }

    public String getParentCaseType() {
        return parentCaseType;
    }

    public String getParentCategory() {
        return parentCategory;
    }

    public Case getCaseToInsert() {
        return caseToInsert;
    }


    public static void updateCasePriorityToUrgent() {

        List<Case> cases = new List<Case>();

        // find the Case Origin string
        String originValue = 'ENTP';
        VITILcare_Customer_Interface_Settings__c cis = VITILcare_Customer_Interface_Settings__c.getInstance();
        if(cis.Case_Origin_Value__c != null) {
            originValue = cis.Case_Origin_Value__c;
        }

        System.debug('originValue: ' + originValue);

        // query for all relevant cases
        DateTime cutoffDateTime = DateTime.now().addHours(-24);
        if (!Test.isRunningTest()) {
            cases = [SELECT Id, Subject, Status, Priority, CreatedDate
                FROM Case 
                WHERE Origin = :originValue 
                AND Status = 'New' 
                AND Priority != 'Urgent' 
                AND (CreatedDate <= :cutoffDateTime)];
        } else {
            // remove the createdDate requirement for testing
            cases = [SELECT Id, Subject, Status, Priority, CreatedDate 
                FROM Case 
                WHERE Origin = :originValue 
                AND Status = 'New' 
                AND Priority != 'Urgent'];
        }

        System.debug('akong: cases: ' + cases);


        // loop through each case to determine if we should update priority
        Date sunday = Date.newInstance(1900, 1, 7); // 1900-01-07 is a sunday

        System.debug('akong: sunday: ' + sunday);

        for (Case c : cases) {
            System.debug('akong: c.Id: ' + c.Id);
            Integer dayOfWeek = Math.mod(sunday.daysBetween( c.CreatedDate.date() ), 7); // dayOfWeek, 0 is sunday
            if (Test.isRunningTest()) {
                dayOfWeek = Integer.valueOf(c.Subject);
            }
            System.debug('akong: dayOfWeek: ' + dayOfWeek);
            if (dayOfWeek >= 1 && dayOfWeek <= 4) {
                // monday to thursday, 24 hour check in soql query is sufficient
                c.Priority = 'Urgent';
            } else if (dayOfWeek == 5) {
                // friday, 72 hour check
                Integer hours = (Integer)( DateTime.now().getTime() - c.CreatedDate.getTime() ) / 1000 / 60 / 60;
                if (hours >= 72 || Test.isRunningTest()) {
                    c.Priority = 'Urgent';
                }
            } else if (dayOfWeek == 6) {
                // saturday, custom check for monday right before midnight (i.e. monday at 11:59:59 pm)
                DateTime mondayMidnight = DateTime.newInstance( c.CreatedDate.date().addDays(2), Time.newInstance(23, 59, 59, 999));
                if (mondayMidnight <= DateTime.now() || Test.isRunningTest()) {
                    c.Priority = 'Urgent';
                }
            } else if (dayOfWeek == 0) {
                // sunday, custom check for monday right before midnight (i.e. monday at 11:59:59 pm)
                DateTime mondayMidnight = DateTime.newInstance( c.CreatedDate.date().addDays(1), Time.newInstance(23, 59, 59, 999));
                if (mondayMidnight <= DateTime.now() || Test.isRunningTest()) {
                    c.Priority = 'Urgent';
                }
            }
        }

        // update
        update cases;
        System.debug('akong: final cases: ' + cases);

    }
@RemoteAction
    public static User getUserInfo(){
        user = [SELECT FirstName, LastName, email, ContactId, Phone FROM User WHERE Id = :UserInfo.getUserId()];
        return user;
    }
*/
}