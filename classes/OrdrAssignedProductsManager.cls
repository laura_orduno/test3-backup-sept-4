public class OrdrAssignedProductsManager {

    
    private static final Map<String, String> ossjNameMap;
    private static final Map<String, Product2> productMap;
    static {
        ossjNameMap = new Map<String, String>();
        for (OCOMCharactersticsOSSJMapping__c ossj : OCOMCharactersticsOSSJMapping__c.getAll().values()) {
            if (!ossjNameMap.containsKey(ossj.CharacteristicsOSSJName__c)) {
                ossjNameMap.put(ossj.CharacteristicsOSSJName__c, ossj.CharacteristicName__c);
            }
        }
        
        List<Product2> products = [select id, name, orderMgmtId__c, vlocity_cmt__JSONAttribute__c, sellable__c
                                   from product2 
                                   where vlocity_cmt__Status__c = 'Active' 
                                   and orderMgmtId__c != null];
        productMap = new Map<String, Product2>();
        for (Product2 product : products) {
            productMap.put(product.orderMgmtId__c, product);
        }
    }
    
    public static void calibrateServiceAccountAssets(Id serviceAccountId) {

        Account serviceAccount = getServiceAccount(serviceAccountId);
        String customerProfileId = serviceAccount.parent.CustProfId__c;
        String locationId = getLocationId(serviceAccountId);

        TpInventoryProductConfigV3.Product productConfiguration = OrdrRetrieveProductConfigurationCallout.retrieveProductConfigurationByServiceAccount(customerProfileId, locationId);
        
        Map<String, Asset> assetMap = getServiceAccountAssets(serviceAccountId);
        
        assetMap = updateAssets(serviceAccount, assetMap, productConfiguration.ComponentProduct, null);
         system.debug('productConfiguration >>>>>'+productConfiguration );
         system.debug('productConfiguration.ComponentProduct >>>>>'+productConfiguration.ComponentProduct );
        //Map<String, OrdrProductEligibilityManager.Offer> availableOffers = OrdrProductEligibilityManager.getAvailableOffers(serviceAccountId);
        
        //OrdrProductEligibilityManager.persistMaximumSpeed(serviceAccountId, OrdrProductEligibilityManager.getMaximumSpeed(availableOffers));
        
        //changed to update from upsert - requirement is not create asset via getAssignedProducts
        system.debug('assetMap.values()>>>>>'+assetMap.values());
        //upsert assetMap.values();
        update assetMap.values();
        
    }
    
    private static String getLocationId(Id serviceAccountId) {
        SMBCare_Address__c address = [select Location_Id__c from SMBCare_Address__c where Service_Account_Id__c = :serviceAccountId LIMIT 1];
        return address.location_id__c;
    }

    private static Account getServiceAccount(Id serviceAccountId) {
        Account account = [select Id, parentId, parent.CustProfId__c from Account where Id = :serviceAccountId LIMIT 1];
        return account;
    }
    
    private static Map<String, Asset> getServiceAccountAssets(Id serviceAccountId) {
        List<Asset> assets = [select id, name, orderMgmt_BPI_Id__c, vlocity_cmt__JSONAttribute__c, 
                              vlocity_cmt__LineNumber__c, Product2Id, orderMgmt_Status__c, 
                              accountId, vlocity_cmt__ServiceAccountId__c, vlocity_cmt__ParentItemId__c, 
                              vlocity_cmt__AssetReferenceId__c, vlocity_cmt__BillingAccountId__c, download_speed__c
                              from Asset 
                              where vlocity_cmt__ServiceAccountId__c = :serviceAccountId
                              order by vlocity_cmt__LineNumber__c];
        
        Map<String, Asset> assetMap = new Map<String, Asset>();
        for (Asset asset : assets) {
            if (String.isNotEmpty(asset.orderMgmt_BPI_Id__c) && asset.orderMgmt_BPI_Id__c != null) {
                assetMap.put(asset.orderMgmt_BPI_Id__c, asset);
            }
        }

       
        return assetMap;
    }
    
    private static Map<String, Asset> updateAssets(Account serviceAccount, Map<String, Asset> assetMap, TpInventoryProductConfigV3.Product[] products, String parentLineNum) {
        
        if (products != null) {
            for (TpInventoryProductConfigV3.Product product : products) {
                String lineNumber;
                //look for the BPI Id if it can be found in the assets object
                if (assetMap.containsKey(product.Id)) {
                    Asset asset = assetMap.get(product.Id);
                    Product2 product2;
                    for (TpCommonBaseV3.CharacteristicValue charVal : product.Characteristicvalue) {
                        asset = updateAssetCharacteristics(asset, charVal);
                        if (charVal.Characteristic.Name == 'offeringId' && productMap.containsKey(charVal.Value[0])) {
                            product2 = productMap.get(charVal.Value[0]);
                        }
                        if (product2 != null && product2.Sellable__c && charVal.Characteristic.Name == 'download_speed') {
                            asset.download_speed__c = charVal.Value[0];
                        }
                       if (charVal.Characteristic.Name == 'Extended Product Instance Status') { // changed from 'Status' to 'Extended Product Instance Status' by Prashant on 7-April-2017
                            asset.Status  = charVal.Value[0]; // changed from orderMgmt_Status__c to status by prashant on 18-April-2017
                            system.debug('asset.Status>>>>'+charVal.Value[0]);
                        }
                    }
                    assetMap.put(product.Id, asset);
                    
                    lineNumber = asset.vlocity_cmt__LineNumber__c;
                }
                else {
                    //do not create the asset if not found as per the requirement from Collette
                    System.debug('BPI Id=' + product.Id);
                    //lineNumber = getLineNumber(serviceAccount.Id, assetMap, parentLineNum);
                    //Asset asset = createAsset(serviceAccount, product, lineNumber);
                    //if (String.isNotEmpty(asset.name)) assetMap.put(product.Id, asset);
                }
                if (product.ComponentProduct != null && !product.ComponentProduct.isEmpty()) {
                    assetMap = updateAssets(serviceAccount, assetMap, product.ComponentProduct, lineNumber);
                }
            }
        }
            
        return assetMap;    
    }
    
    private static Asset updateAssetCharacteristics(Asset asset, TpCommonBaseV3.CharacteristicValue charVal) {

        if (String.isNotEmpty(asset.vlocity_cmt__JSONAttribute__c)) {
            Map<String, Object> attributes = (Map<String, Object>) JSON.deserializeUntyped(asset.vlocity_cmt__JSONAttribute__c);
            
            for (String key : attributes.keySet()) {
                
                for (Object attribute : (List<Object>) attributes.get(key)) {
                    Map<String, Object> attributeMap = (Map<String, Object>) attribute;
                    String attributeName = (String) attributeMap.get('attributedisplayname__c');
                    
                    if (attributeName == translateOssjName(charVal.Characteristic.Name)) {
                        //System.debug('attributeName=' + attributeName + ':' + translateOssjName(charVal.Characteristic.Name));
                        
                        Map<String, Object> attributeRunTimeInfo = (Map<String, Object>) attributeMap.get('attributeRunTimeInfo');
                        if ((String) attributeRunTimeInfo.get('dataType') == 'Text') {
                            attributeRunTimeInfo.put('value', charVal.Value[0]);
                        }
                        else if ((String) attributeRunTimeInfo.get('dataType') == 'Picklist') {
                            Map<String, Object> seletedItem = (Map<String, Object>) attributeRunTimeInfo.get('selectedItem');
                            for(Object values : (List<Object>) attributeRunTimeInfo.get('values')){
                                Map<String, Object> attributeValues = (Map<String, Object>) values;
                                if ((String) attributeValues.get('displayText') == charVal.Value[0]) {
                                    if(seletedItem!=null){
                                        seletedItem.put('id', attributeValues.get('id'));
                                        seletedItem.put('displayText', charVal.Value[0]);
                                    }
                                    
                                }
                            }  
                        }
                    }
                }
            }        
            
            asset.vlocity_cmt__JSONAttribute__c = JSON.serialize(attributes);
        }
        
        return asset;
    }
    
    
    /****
    public static Asset createAsset(Account serviceAccount, TpInventoryProductConfigV3.Product productConfig, String lineNumber) {

        Asset asset = new Asset();
        Product2 product;
        system.debug('productConfig.Characteristicvalue >>>>'+productConfig.Characteristicvalue.size() );
        system.debug('Status >>>>'+productConfig.Status );
        for (TpCommonBaseV3.CharacteristicValue charVal : productConfig.Characteristicvalue) {
            if (charVal.Characteristic.Name == 'offeringId' && productMap.containsKey(charVal.Value[0])) {
                product = productMap.get(charVal.Value[0]);
                //system.debug('Status_type_info >>>>'+productConfig.Status_type_info );
                
                asset.name = product.name;
                asset.orderMgmt_BPI_Id__c = productConfig.Id;
                asset.vlocity_cmt__JSONAttribute__c = product.vlocity_cmt__JSONAttribute__c;
                asset.Product2Id = product.Id;
                asset.accountId = serviceAccount.parentId;
                asset.vlocity_cmt__ServiceAccountId__c = serviceAccount.Id;
                asset.vlocity_cmt__LineNumber__c = lineNumber;
                asset.vlocity_cmt__BillingAccountId__c = serviceAccount.parentId;
            }
            if (product != null && product.Sellable__c && charVal.Characteristic.Name == 'download_speed') {
                asset.download_speed__c = charVal.Value[0];
            }
            if (charVal.Characteristic.Name == 'externalId') {
                asset.vlocity_cmt__AssetReferenceId__c = charVal.Value[0];
            }
            if (charVal.Characteristic.Name == 'Extended Product Instance Status') { // changed from 'Status' to 'Extended Product Instance Status' by Prashant on 7-April-2017
                asset.orderMgmt_Status__c = charVal.Value[0];
            }

        }   
        for (TpCommonBaseV3.CharacteristicValue charVal : productConfig.Characteristicvalue) {
            asset = updateAssetCharacteristics(asset, charVal);
        }        
        return asset;
        
    }****/
   
    
    
    
    private static String translateOssjName(String ossjName) {
        if (ossjNameMap.containsKey(ossjName)) {
            return ossjNameMap.get(ossjName);
        }
        return ossjName;
    }
    
    
    
    /*****
    private static String getLineNumber(String serviceAccountId, Map<String, Asset> assetMap, String parentLineNum) {
        
        String lineNumber;
        Integer nextNum = 0;
        
        if (parentLineNum == null) {
            for (String key : assetMap.keySet()) {
                Asset asset = assetMap.get(key);
                if (asset.vlocity_cmt__LineNumber__c.indexOf('.') < 0) {
                    nextNum = Integer.valueOf(asset.vlocity_cmt__LineNumber__c) > nextNum ? Integer.valueOf(asset.vlocity_cmt__LineNumber__c) : nextNum;
                    
                }
            }
            Asset asset = new Asset();
            system.debug('serviceAccountId >>>>>'+serviceAccountId );
            list<Asset> lstasset = [SELECT Id, vlocity_cmt__LineNumber__c FROM Asset 
                           where vlocity_cmt__ServiceAccountId__c = :serviceAccountId 
                           AND (NOT vlocity_cmt__LineNumber__c LIKE '%.%') 
                           ORDER BY vlocity_cmt__LineNumber__c desc nulls last LIMIT 1];
            if(lstasset != null && lstasset.size()>0){
                asset = lstasset[0];
            }
            if(asset != null && asset.vlocity_cmt__LineNumber__c != null){ // null handling added by prashant on 4-4-2017
            nextNum = Integer.valueOf(asset.vlocity_cmt__LineNumber__c) > nextNum ? Integer.valueOf(asset.vlocity_cmt__LineNumber__c) : nextNum;
            }
        }
        
        for (String key : assetMap.keySet()) {
            Asset asset = assetMap.get(key);
            String ln = asset.vlocity_cmt__LineNumber__c;
            if (ln.substringBeforeLast('.') == parentLineNum && ln.indexOf('.') > 0) {
                String suffix = ln.removeStart(parentLineNum + '.');
                if (suffix.indexOf('.') < 0) {
                    Integer tempNum = Integer.valueOf(suffix);
                    if (tempNum > nextNum) nextNum = tempNum;
                }
            }
        }

        lineNumber = String.valueOf(nextNum + 1);
        while (lineNumber.length() < 4) {
            lineNumber = '0' + lineNumber;
        }
        
        return String.isNotEmpty(parentLineNum) ? parentLineNum + '.' + lineNumber : lineNumber;
    }****/
    
    
}