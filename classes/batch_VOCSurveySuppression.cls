// Run nightly
// Get All VOC Survey records created today and have a survey type of ( 35 )
// Suppress 15% of all VOC Survey records returned with survey_type__c = 35
// To suppress set voc_suppression_for_delete__c to "suppress"
// Survey Type Values 


global class batch_VOCSurveySuppression implements database.batchable<sobject>{
    global string query{get;set;}
    
    global batch_VOCSurveySuppression(){
	    query = 'select id, voc_suppression_for_delete__c, survey_type__c, Customer_Email__c, createdDate FROM voc_survey__c where createddate = TODAY LIMIT 50000';        
    }    
    
    global database.querylocator start(database.batchablecontext bc) {
        return database.getquerylocator(query);
    }
  
    global void execute(database.batchablecontext bc, list<sobject> scope){
        list<voc_survey__c> suppressList = new list<voc_survey__c>();        

        Map<Integer, List<voc_survey__c>> vocSurveyMap = getVocSurveyMap(scope);  
        List<VOC_Survey_Type_Suppression_Percentages__c> vocSuppressionPercentageList = VOC_Survey_Type_Suppression_Percentages__c.getAll().values();        
        for(VOC_Survey_Type_Suppression_Percentages__c sp : vocSuppressionPercentageList){
        	System.debug('suppression percent for survey type ' + sp.Survey_Type__c + ': ' + sp.Percentage__c);            

            Integer key = sp.Survey_Type__c.intValue();
			// check if list exists yet, if not, create             
			System.debug('key: ' + key);                                
            if(vocSurveyMap.containsKey(key)){
		        Integer numToSuppress = (vocSurveyMap.get(key).size() * sp.Percentage__c).intValue();                
                for(voc_survey__c voc : vocSurveyMap.get(key)){
                    // log
                    System.debug('Check VOC Survey: ' + voc.id + ', ' + voc.Customer_Email__c + ', ' + voc.survey_type__c + ', ' + voc.createdDate);                                
                    // post decrement check if we have hit the suppress percent threshold
                    if(numToSuppress-- <= 0){
                        // do not add any more to suppress list
                        break;
                    }

                    if(voc.voc_suppression_for_delete__c != 'suppress'){
                        // set
                        voc.voc_suppression_for_delete__c = 'suppress';
                        
                        System.debug('Suppress VOC Survey: ' + voc.Customer_Email__c + ', ' + voc.survey_type__c + ', ' + voc.createdDate);                                
                        // add
                        suppressList.add(voc);

                    }
                }
            }             
        }
                
        try{
System.debug('suppressList.size(): ' + suppressList.size());                                
System.debug('suppressList: ' + suppressList);                                
	        update suppressList;         
        }
        catch(DMLException e){
System.debug('Exception: ' + e.getMessage());                                            
        }    
    }
    
    global void finish(database.batchablecontext bc) {}
        
    // returns a list map that uses the survey_type__c values as keys
    // in order to group the voc surveys together so they can be 
    // processed seperately 
    private Map<Integer, List<voc_survey__c>> getVocSurveyMap(List<SObject> scope){
        Map<Integer, List<voc_survey__c>> result = new Map<Integer, List<voc_survey__c>>();    
        
        for(Object obj : scope){
            voc_survey__c voc = (voc_survey__c)obj;
            // set key
            Integer key = voc.survey_type__c.intValue();
			// check if list exists yet, if not, create             
            if(!result.containsKey(key)){
                result.put(key, new List<voc_survey__c>());
            }             
            List<voc_survey__c> vocList = result.get(key);
            vocList.add(voc);
            result.put(key, vocList);
        }            

//System.debug('result: ' + result);        
        
        return result;
    } 
    
}