//Methods Included: recoverResource
// Primary Port Class Name: RecoverResourceSOAPBindingOSSKIDC	
public class OrdrTnRecvrResOperation {
	public class RecoverResourceSOAPBinding {
		public String endpoint_x = 'https://soa-mp-toll-pt.tsl.telus.com:443/RMO/ResourceMgmt/RecoverResource_v1_1_vs0';
		public Map<String,String> inputHttpHeaders_x;
		public Map<String,String> outputHttpHeaders_x;
		public String clientCertName_x;
		public String clientCert_x;
		public String clientCertPasswd_x;
		public Integer timeout_x;
		
		   private String[] ns_map_type_info = new String[]{
            'http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/fulfillment/types/schema/SubmitCustomerOrder',
                'TpCustomerOrderMessage',
                'http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/fulfillment/SubmitCustomerOrder',
                'OrdrWsSubmitCustomerOrder',
                'http://www.ibm.com/telecom/inventory/schema/service_configuration/v3_0',
                'TpInventoryServiceConfigV3',
                'http://www.ibm.com/telecom/inventory/schema/resource_configuration/v3_0',
                'TpInventoryResourceConfigV3',
                'http://www.ibm.com/telecom/common/schema/party_extensions/v3_0',
                'TpCommonPartyExtV3',
                'http://www.ibm.com/telecom/common/schema/place_extensions/v3_0',
                'TpCommonPlaceExtV3',
                'http://www.ibm.com/telecom/inventory/schema/product_configuration_extensions/v3_0',
                'TpInventoryProductConfigV3',
                'http://www.ibm.com/telecom/templates/schema/phone/v3_0',
                'TpTemplatesPhoneV3',
                'http://www.ibm.com/telecom/templates/schema/business_interaction_extensions/v3_0',
                'TpTemplatesBusinessInteractionExtV3',
                'http://www.ibm.com/telecom/common/schema/base/v3_0',
                'TpCommonBaseV3',
                'http://www.ibm.com/telecom/templates/schema/party_extensions/v3_0',
                'TpTemplatesPartyExtV3',
                'http://www.ibm.com/telecom/common/schema/party/v3_0',
                'TpCommonPartyV3',
                'http://www.ibm.com/telecom/common/schema/urban_property_address/v3_0',
                'TpCommonUrbanPropertyAddressV3',
                'http://www.ibm.com/telecom/common/schema/place/v3_0',
                'TpCommonPlaceV3',
                'http://www.ibm.com/telecom/templates/schema/set_top_box/v3_0',
                'TpTemplatesSetTopBoxV3',
                'http://www.ibm.com/telecom/templates/schema/party_role_extensions/v3_0',
                'TpTemplatesPartyRoleExtV3',
                'http://www.ibm.com/telecom/inventory/schema/customer_configuration_extensions/v3_0',
                'TpInventoryCustomerConfigExtV3',
                'http://www.ibm.com/telecom/inventory/schema/product_configuration/v3_0',
                'TpInventoryProductConfigV3',
                'http://www.ibm.com/telecom/fulfillment/schema/product_order/v3_0',
                'TpFulfillmentProductOrderV3',
                'http://www.ibm.com/telecom/fulfillment/schema/customer_order/v3_0',
                'TpFulfillmentCustomerOrderV3',
                'http://www.ibm.com/telecom/inventory/schema/customer_configuration/v3_0',
                'TpInventoryCustomerConfigV3',
                'http://www.ibm.com/telecom/inventory/schema/customer/v3_0',
                'TpInventoryCustomerV3',
                'http://www.ibm.com/telecom/common/schema/business_interaction_extensions/v3_0',
                'TpTemplatesBusinessInteractionExtV3',
                'http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/fulfillment/schema/CustomerOrderMessage',
                'TpCustomerOrderMessage',
                'http://www.ibm.com/telecom/templates/schema/contact_medium_extensions/v3_0',
                'TpTemplatesContactMediumExtV3',
                'http://www.ibm.com/telecom/common/schema/mtosi/v3_0',
                'TpCommonMtosiV3',
                'http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/common/schema/Message',
                'TpCommonMessage',
                'http://www.ibm.com/telecom/fulfillment/schema/product_order_extensions/v3_0',
                'TpFulfillmentProductOrderExtV3',
                'http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',
                'TpCommonBusinessInteractionV3',
                'http://www.ibm.com/telecom/fulfillment/schema/customer_order_extensions/v3_0',
                'TpFulfillmentCustomerOrderExtV3',
                'http://www.ibm.com/telecom/inventory/schema/service_configuration_extensions/v3_0',
                'TpInventoryServiceConfigExtV3',
                'http://www.ibm.com/telecom/common/schema/base_extensions/v3_0',
                'TpCommonBaseV3',
                'http://www.ibm.com/telecom/inventory/schema/resource_configuration_extensions/v3_0',
                'TpInventoryResourceConfigExtV3',
				'http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/fulfillment/schema/ResourceOrderMessage','TpResourceOrderMessage',
				'http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/fulfillment/types/schema/RecoverResource','OrdrTnRecvrResOperation'
                };
		public TpFulfillmentResourceOrderV3.ResourceOrder recoverResource(TpFulfillmentResourceOrderV3.ResourceOrder resourceOrder) {
			OrdrTnRecvrResOrdrMsg.ResourceOrderMessage request_x = new OrdrTnRecvrResOrdrMsg.ResourceOrderMessage();
			OrdrTnRecvrResOrdrMsg.ResourceOrderMessage response_x;
			request_x.resourceOrder = resourceOrder;
			Map<String, OrdrTnRecvrResOrdrMsg.ResourceOrderMessage> response_map_x = new Map<String, OrdrTnRecvrResOrdrMsg.ResourceOrderMessage>();
			response_map_x.put('response_x', response_x);
			WebServiceCallout.invoke(
				this,
				request_x,
				response_map_x,
				new String[]{endpoint_x,							
				'http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/fulfillment/RecoverResource/recoverResource',
				'http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/fulfillment/types/schema/RecoverResource',
				'recoverResource',
				'http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/fulfillment/types/schema/RecoverResource',
				'recoverResourceResponse',
				'OrdrTnRecvrResOrdrMsg.ResourceOrderMessage'}
			);
			//OrdrTnRecvrResOrdrV2.ResourceOrder
			response_x = response_map_x.get('response_x');
            if(response_x!=null){
                return response_x.resourceOrder;
            }
			return null;
		}
	}
	
}