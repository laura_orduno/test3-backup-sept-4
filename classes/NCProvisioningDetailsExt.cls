/**
-- Change Log --
Date: July 25, 2014
-  Added logic to pull all related opportunity details including CPQ products.

*/
public with sharing class NCProvisioningDetailsExt {
    public opportunity opp{get;set;}
    public OrderContact mainContact{get;set;}
    public OrderContact onsiteContact{get;set;}
    public string orderId{get;set;}
    public string serviceAddress{get;set;}
    public NCProvisioningDetailsExt(){
        string oppId=apexpages.currentpage().getparameters().get('id');
        if(string.isnotblank(oppId)){
            init(oppId);
        }else{
            //TODO - Handling no opportunity ID error OR-00137547
        }
    }
    void init(string oppId){
        string query='select id,name,order_id__c,salesperson__r.name,salesperson__r.team_telus_id__c,account.rcid__c,account.correct_legal_name__c,account.jurisdiction__c,account.registration_incorporation_date__c,account.registration_incorporation_number__c,account.Operating_Name_DBA__c,related_orders__c,order_remarks_permanent__c,order_remarks_temporary__c,type,requested_due_date__c,credit_reference__c,primary_order_contact__c,product_type__c,rush__c from opportunity where';
        if(oppId.startswith('OR-')){
            query+=' order_id__c=:oppId';
        }else{
            query+=' id=:oppId';
        }
        try{
            opp=database.query(query);
            this.orderId=opp.id;
            // Contacts are stored in different objects.
            if(string.isnotblank(opp.type)){
                if(opp.type.equalsignorecase('all other orders')){
                    loadAOOContacts();
                }else if(opp.type.equalsignorecase('New Provide/Upgrade Order')||opp.type.equalsignorecase('New Move Order')){
                    loadCPQContacts();
                }else{
                    //TODO - Message - Not CPQ nor AOO
                }
            }else{
                //TODO - Message - Invalid Opportunity.
            }
        }catch(exception e){
            //TODO - In case order has been deleted.
        }
    }
    void loadCPQContacts(){
        if(opp!=null){
            try{
                contact mainContactRecord=database.query('select id,name,mobilephone,phone,email,homephone,preferred_contact_method__c from contact where id=\''+opp.primary_order_contact__c+'\'');
                mainContact=new OrderContact(mainContactRecord.id,
                mainContactRecord.name,
                mainContactRecord.email,
                mainContactRecord.phone,
                mainContactRecord.mobilephone,
                mainContactRecord.homephone,
                mainContactRecord.preferred_contact_method__c,
                'Main Contact');
            }catch(exception e){
                //TODO - Message - No main contact found for CPQ.
                apexpages.addmessage(new apexpages.message(apexpages.severity.error,e.getmessage()));
            }
            opportunitylineitem onsiteContactRecord=database.query('select onsite_contact_cell__c,onsite_contact_email__c,onsite_contact_home__c,onsite_contact_name__c,onsite_contact_office__c from opportunitylineitem where opportunityid=:orderId and onsite_contact_name__c!=null order by onsite_contact_name__c limit 1');
            onsiteContact=new OrderContact('',
            onsiteContactRecord.onsite_contact_name__c,
            onsiteContactRecord.onsite_contact_email__c,
            onsiteContactRecord.onsite_contact_office__c,
            onsiteContactRecord.onsite_contact_cell__c,
            onsiteContactRecord.onsite_contact_home__c,
            '','Onsite Contact');
        }
    }
    public list<WorkOrder> getWorkOrders(){
        list<WorkOrder> workOrders=new list<WorkOrder>();
        if(opp!=null){
            list<work_order__c> loadedWorkOrders=database.query('select id,name,wfm_number__c,scheduled_due_date_location__c,permanent_comments__c,product_name__c,location_contact__r.name,time_slot__c,duration__c,install_type__c from work_order__c where opportunity__c=\''+opp.id+'\' AND status__c!=\'Cancelled\' limit 999');
            for(work_order__c loadedWorkOrder:loadedWorkOrders){
                workOrders.add(new WorkOrder(loadedWorkOrder));
            }
        }
        return workOrders;
    }
    void loadAOOContacts(){
        string query='select id,contact__c,role__c,type__c,contact_preferred_contact_method__c,contact_email__c,contact_phone__c,contact_name__c,contact_homephone__c,contact_mobilephone__c from service_contact_role__c where opportunity__c=:orderId';
        list<service_contact_role__c> serviceContactRoles=database.query(query);
        if(!serviceContactRoles.isempty()){
            for(service_contact_role__c serviceContactRole:serviceContactRoles){
                if(serviceContactRole.type__c.equalsignorecase('main contact')){
                    mainContact=new OrderContact(serviceContactRole.contact__c,
                    serviceContactRole.contact_name__c,
                    serviceContactRole.contact_email__c,
                    serviceContactRole.contact_phone__c,
                    serviceContactRole.contact_mobilephone__c,
                    serviceContactRole.contact_homephone__c,
                    serviceContactRole.contact_preferred_contact_method__c,
                    serviceContactRole.type__c);
                }else{
                    onsiteContact=new OrderContact(serviceContactRole.contact__c,
                    serviceContactRole.contact_name__c,
                    serviceContactRole.contact_email__c,
                    serviceContactRole.contact_phone__c,
                    serviceContactRole.contact_mobilephone__c,
                    serviceContactRole.contact_homephone__c,
                    serviceContactRole.contact_preferred_contact_method__c,
                    serviceContactRole.type__c);
                }
            }
        }
    }
    public list<LineItem> getCPQLineItems(){
        list<LineItem> lineItems=new list<LineItem>();
        if(string.isnotblank(orderId)){
            string query='select id,lineno__c,parentlineno__c,product_name__c,order_attribute__c,service_address_formula__c,work_order__c,Prod_Type__c,item_id__c,special_instructions__c from opportunitylineitem where opportunityid=:orderId order by lineno__c';
            list<opportunitylineitem> orderLineItems=database.query(query);
            set<string> itemIds=new set<string>();
            for(opportunitylineitem orderLineItem:orderLineItems){
                itemIds.add(orderLineItem.item_id__c);
            }
            list<provisioning__c> provisioningValues=database.query('select product_item_id__c,product_line_no__c,product_parent_line_no__c,provisioning_name__c,provisioning_value__c from provisioning__c where product_item_id__c=:itemIds and parent_order__c=:orderId order by product_line_no__c limit 1000');
            map<string,list<integer>> itemIndex=new map<string,list<integer>>();
            integer numValues=provisioningValues.size();
            system.debug('num values:'+numValues);
            list<integer> indexes=null;
            for(integer i=0;i<numValues;i++){
                if(itemIndex.containskey(provisioningValues[i].product_item_id__c+''+provisioningValues[i].product_line_no__c)){
                    indexes=itemIndex.get(provisioningValues[i].product_item_id__c+''+provisioningValues[i].product_line_no__c);
                }else{
                    indexes=new list<integer>();
                }
                indexes.add(i);
                system.debug('item Id:'+provisioningValues[i].product_item_id__c+',name:'+provisioningValues[i].Provisioning_Name__c+',value:'+provisioningValues[i].Provisioning_Value__c+', index:'+i);
                itemIndex.put(provisioningValues[i].product_item_id__c+''+provisioningValues[i].product_line_no__c,indexes);
            }
            boolean firstProduct=true;
            for(opportunitylineitem orderLineItem:orderLineItems){
                if(string.isnotblank(orderLineItem.service_address_formula__c)){
                    this.serviceAddress=orderLineItem.service_address_formula__c;
                }
                if(string.isnotblank(orderLineItem.parentlineno__c)){
                    if(string.isnotblank(orderLineItem.work_order__c)){
                        // This is Product, ignore attribute
                        if(!firstProduct){
                            lineItems.add(new LineItem('','','',''));
                        }
                        lineItems.add(new LineItem(orderLineItem.product_name__c,orderLineItem.special_instructions__c,'',orderLineItem.Prod_Type__c));
                        firstProduct=false;
                    }else{
                        // This is feature, first extract attributes
                        if(string.isnotblank(orderLineItem.order_attribute__c)){
                            if(!orderLineItem.order_attribute__c.contains('IsBundle')){
                                try{
                                    list<string> delimitedAttributes=orderLineItem.order_attribute__c.split(',');
                                    // For first attribute, put at feature level
                                    if(string.isnotblank(orderLineItem.prod_type__c)&&!firstProduct){
                                        lineItems.add(new LineItem('','','',''));
                                    }
                                    lineItems.add(new LineItem('',orderLineItem.product_name__c,delimitedAttributes.get(0),orderLineItem.Prod_Type__c));
                                    integer numAttributes=delimitedAttributes.size();
                                    //TODO - Ignore ones that are not supposed to be included, use Custom Settings
                                    for(integer i=1;i<numAttributes;i++){
                                        if(string.isnotblank(delimitedAttributes[i])&&!delimitedAttributes[i].contains('IgnoreForOrder')){
                                            lineItems.add(new LineItem('','',delimitedAttributes[i],''));
                                        }
                                    }
                                }catch(exception e){
                                    system.debug(e.getmessage());
                                }
                            }
                        }else{
                            // No attribute, just add feature
                            lineItems.add(new LineItem('',orderLineItem.product_name__c,'',''));
                        }
                    }
                    // Add all provisioning values
                    if(itemIndex.containskey(orderLineItem.item_id__c+''+orderLineItem.lineno__c)){
                        indexes=itemIndex.get(orderLineItem.item_id__c+''+orderLineItem.lineno__c);
                        integer numIndexes=indexes.size();
                        system.debug('num values:'+numIndexes);
                        provisioning__c provisioningValue=null;
                        integer index=0;
                        for(integer i=0;i<numIndexes;i++){
                            index=indexes.get(i);
                            system.debug('provisioning name:'+provisioningValues[index].provisioning_name__c+',value:'+provisioningValues[index].provisioning_value__c);
                            lineItems.add(new LineItem('',provisioningValues[index].provisioning_name__c,provisioningValues[index].provisioning_value__c,''));
                        }
                    }
                    
                }
            }
        }
        return lineItems;
    }
    /**
    Get AOO Line Items
    
    1.  Retrieve products in AOO
    2.  Retrieve associated Fieldsets
    3.  Use built-in Mapping for Fieldsets for each product
    */
    public list<LineItem> getAOOLineItems(){
        list<LineItem> lineItems=new list<LineItem>();
        if(string.isnotblank(orderId)){
            list<string> requestTypes=new list<string>();
            string query='select request_type__c from fobochecklist__c where opportunity__c=:orderId and request_type__c!=null order by request_type__c';
            list<fobochecklist__c> aooRequestTypes=database.query(query);
            for(fobochecklist__c aooRequestType:aooRequestTypes){
                requestTypes.add(aooRequestType.request_type__c);
            }
            map<string,schema.fieldset> aooFieldSets=fobochecklist__c.sobjecttype.getdescribe().fieldsets.getmap();
            set<string> fieldSetStrings=new set<string>();
            for(string requestType:requestTypes){
                schema.fieldset fieldsetMembers=aooFieldSets.get(requestType.replaceall(' ','_')+'_Provisioning');
                if(fieldsetMembers!=null){
                    list<schema.fieldsetmember> members=fieldsetMembers.getFields();
                    for(schema.fieldsetmember member:members){
                        fieldSetStrings.add(member.getfieldpath());
                    }
                }
            }
            string queryFields='';
            for(string fieldSetString:fieldSetStrings){
                queryFields+=fieldSetString+',';
            }
            if(!queryFields.contains('New_Service_Address__c')){
                queryFields+='new_service_address__c,';
            }
            query='select '+queryFields+'request_type__c,id,service_address__c from fobochecklist__c where opportunity__c=:orderId and request_type__c!=null order by request_type__c';
            aooRequestTypes=database.query(query);
            for(fobochecklist__c aooRequestType:aooRequestTypes){
                if(string.isnotblank(aooRequestType.request_type__c)){
                    if(string.isnotblank(aooRequestType.service_address__c)){
                        this.serviceAddress=aooRequestType.service_address__c;
                    }else if(string.isnotblank(aooRequestType.new_service_address__c)){
                        this.serviceAddress=aooRequestType.new_service_address__c;
                    }
                    lineItems.add(new LineItem(aooRequestType.request_type__c,'','',''));
                    schema.fieldset fieldsetMembers=aooFieldSets.get(aooRequestType.request_type__c.replaceall(' ','_')+'_Provisioning');
                    if(fieldsetMembers!=null){
                        for(schema.fieldsetmember member:fieldsetMembers.getfields()){
                            LineItem lineItem=new LineItem('',member.getlabel(),string.valueof(aooRequestType.get(member.getfieldpath())),'');
                            if(aooRequestType.request_type__c.equalsignorecase('gpon swift')){
                                if(member.getfieldpath().equalsignorecase('btn__c')){
                                    lineItem.feature='Customer\'s Primary BTN';
                                }else if(member.getfieldpath().equalsignorecase('btn_for_dsl__c')){
                                    lineItem.feature='GPON Internet Core STN';
                                }else if(member.getfieldpath().equalsignorecase('swift_wi_fi__c')){
                                    lineItem.feature='WiFi GPON Service Path Built';
                                }else if(member.getfieldpath().equalsignorecase('Customer_aware_of_length_of_time_for_ins__c')){
                                    lineItem.feature='Customer Aware of Length of Time for Installation';
                                }else if(member.getfieldpath().equalsignorecase('Discussed_Cancelling_other_Non_Telus_Pub__c')){
                                    lineItem.feature='Discussed Cancelling Other Non-Telus Public WiFi';
                                }else if(member.getfieldpath().equalsignorecase('Welcome_Email_Sent__c')){
                                    lineItem.feature='BO to Send Welcome Email';
                                }
                            }else if(aooRequestType.request_type__c.equalsignorecase('wifi')){
                                if(member.getfieldpath().equalsignorecase('Customer_aware_of_length_of_time_for_ins__c')){
                                    lineItem.feature='Customer aware of length of time for installation';
                                }
                            }else if(aooRequestType.request_type__c.equalsignorecase('fifa')){
                                if(member.getfieldpath().equalsignorecase('Customer_aware_of_length_of_time_for_ins__c')){
                                    lineItem.feature='Customer aware of length of time for installation';
                                }
                            /*}else if(aooRequestType.request_type__c.equalsignorecase('gpon')){
                                if(member.getfieldpath().equalsignorecase('Customer_aware_Static_IP_unavailable__c')){
                                    lineItem.feature='Static IP';
                                    lineItem.attribute=(string.isnotblank(lineItem.attribute)&&lineItem.attribute=='true'?'Yes':'No');
                                }*/
                            }
                            lineItems.add(lineItem);
                        }
                    }
                }
            }
        }
        return lineItems;
    }
    public class WorkOrder{
        public string wfmNumber{get;set;}
        public string workOrderNumber{get;set;}
        public string scheduledDueDateLocation{get;set;}
        public string productName{get;set;}
        public string timeSlot{get;set;}
        public string locationContactName{get;set;}
        public string installationNote{get;set;}
        public string installType{get;set;}
        public decimal workOrderDuration{get;set;}
        public WorkOrder(work_order__c workOrder){
            this.wfmNumber=workOrder.wfm_number__c;
            this.workOrderNumber=workOrder.name;
            this.scheduledDueDateLocation=workOrder.scheduled_due_date_location__c;
            this.productName=workOrder.product_name__c;
            this.timeSlot=workOrder.time_slot__c;
            this.locationContactName=workOrder.location_contact__r.name;
            this.installationNote=workOrder.permanent_comments__c;
            this.installType=workOrder.install_type__c;
            this.workOrderDuration=workOrder.duration__c;
        }
    }
    public class LineItem{
        public string name{get;set;}
        public string productType{get;set;}
        public string feature{get;set;}
        public string attribute{get;set;}
        public LineItem(string name,string feature,string attribute,string productType){
            this.name=name;
            this.feature=feature;
            this.attribute=attribute;
            this.productType=productType;
        }
    }
    public class OrderContact{
        public string id{get;set;}
        public string name{get;set;}
        public string email{get;set;}
        public string office{get;set;}
        public string cell{get;set;}
        public string home{get;set;}
        public string preferredMethod{get;set;}
        public string role{get;set;}
        public OrderContact(string id,string name,string email,string office,string cell,string home,string preferredMethod,string role){
            this.id=id;
            this.name=name;
            this.email=email;
            this.office=office;
            this.cell=cell;
            this.home=home;
            this.preferredMethod=preferredMethod;
        } 
    }
}