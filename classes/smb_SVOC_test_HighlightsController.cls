/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class smb_SVOC_test_HighlightsController {

    static testMethod void myUnitTest() {
        /*
		*/
        RecordType recordType = [Select r.Id, r.DeveloperName From RecordType r where r.SobjectType = 'Account' and r.DeveloperName='RCID' limit 1];         
        Account acc1 = new Account(rcid__c='123',Name='Testing Software', BillingCountry = 'US', BillingState = 'IL', recordTypeId=recordType.Id , No_Chronic_Incidents__c =10.0);
        insert acc1;        
        
        RecordType banRecordType = [Select r.Id, r.DeveloperName From RecordType r where r.SobjectType = 'Account' and r.DeveloperName='BAN' limit 1];         
        Account banAcc1 = new Account(rcid__c='1234',Name='Testing Software', BillingCountry = 'US', BillingState = 'IL', recordTypeId=banRecordType.Id , No_Chronic_Incidents__c =10.0);
        banAcc1.parentId = acc1.id;
        insert banAcc1;        
        
        List<VOC_Survey_Type__c> voctypes = new List<VOC_Survey_Type__c>();
        VOC_Survey_Type__c voctype1 = new VOC_Survey_Type__c(Requires_Fielded__c = false, Survey_URL__c = 'test', Type__c= 1);
        vocTypes.add(voctype1);
        VOC_Survey_Type__c voctype2 = new VOC_Survey_Type__c(Requires_Fielded__c = true, Survey_URL__c = 'test', Type__c= 2);        
        vocTypes.add(voctype2);
        insert vocTypes;
        
        List<Contact> cons = new List<Contact>();
        Contact rcidcon1 = new Contact(LastName='last',accountid=acc1.id,firstname='first',email='test@test.com');
        Contact rcidcon2 = new Contact(LastName='last',accountid=acc1.id);
        Contact rcidcon3 = new Contact(LastName='last',accountid=acc1.id,firstname='first');                
        cons.add(rcidcon1);
        cons.add(rcidcon2);
        cons.add(rcidcon3);
        insert cons;
/*        
        List<VOC_Survey__c> vocs = new List<VOC_Survey__c>();
        VOC_Survey__c voc3 = new VOC_Survey__c(
                                    Account_No__c='test',
                                    Agent_ID__c='T876522',
                                    Business_Name__c='test',
                                    Case_Owner__c='test',
                                    Case_Subject__c='test',
                                    Customer_Email__c='test4@test.com',
                                    Customer_First_Name__c='first4',
                                    Customer_Last_Name__c='last4',
                                    Customer_Phone__c='4555555555',
                                    Custom_ID__c='testtest992',
                                    Interaction_Date__c=Date.today(),
                                    Location__c='test',
                                    RCID__c='123',
                                    Reference__c='test',
                                    Overall_Satisfaction_Main__c = 'Satisfied',
                                    Ticket__c='test',
                                    Survey_Type__c=2,
                                    update_manager_emails__c=true,
                                    manager_callback__c=true,
                                    sent__c=true
                                );
        vocs.add(voc3);
        insert vocs;
*/        
        smb_SVOC_HighlightsController smb = new smb_SVOC_HighlightsController();
        Id ids = smb.accountId;
        smb.accountId = acc1.id;
        String str1 = smb.AccountNumber;
        String str2 = smb.AccountNumberLabel; 
        String str3 = smb.BillingAddress;        
        Account Acc = smb.AccountRecord;
        Boolean bool = smb.fromConsole;
        String marketingSubSegment = smb.marketingSubSegment;

//        smb_SVOC_HighlightsController.GetAccountNumberLabelFromAccountRecordType(acct1.recordTypeDeveloperName, function (result) {
//        });
        
        smb_SVOC_HighlightsController smb2 = new smb_SVOC_HighlightsController();
System.debug('banAcc1: ' + banAcc1);                
        smb2.accountId = banAcc1.id;
        str1 = smb2.AccountNumber;
        str2 = smb2.AccountNumberLabel; 
        str3 = smb2.BillingAddress;        
        Acc = smb2.AccountRecord;
        bool = smb2.fromConsole;
        marketingSubSegment = smb2.marketingSubSegment;
    }
}