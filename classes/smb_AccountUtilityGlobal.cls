public without sharing class smb_AccountUtilityGlobal {

    public static List<Id> getAccountsInHierarchy(Id accountId, Id parentId) {
        
        List<Id> allAccountIds = new List<Id>();
        
        //children      
        for (Account account :  [SELECT Id 
                                 From Account 
                                 Where ParentId = :accountId
                                 LIMIT 100]) {
            allAccountIds.add(account.Id);                          
        }
        
        //grandchildren
        for (Account account : [SELECT Id
                                FROM Account
                                WHERE ParentId IN :allAccountIds
                                LIMIT 100]) {
            allAccountIds.add(account.Id);                      
        }
        
        //parent
        if (parentId != null)
            allAccountIds.add(parentId);
        
        //self
        allAccountIds.add(accountId);
        
        //System.debug(allAccountIds);
        
        return allAccountIds;
        
    }
    
    //returns AccountIdsMappedToAccountIdFromList
    public static Map<Id,idAndAccount> getAccountsInHierarchy(List<Account> accounts) {
        
        Map<Id,Account> allAccounts = new Map<Id,Account>();
        
        List<Account> parents = new List<Account>();
        List<Account> initialAccounts = new List<Account>();
        
        List<Id> parentIds = new List<Id>();
        List<Id> initialAccountIds = new List<Id>();
        
        for (Account acc : accounts) {
            if (acc == null || 
                acc.Id == null || 
                acc.RecordType == null ||
                acc.RecordType.DeveloperName == null) continue;
            
            initialAccountIds.add(acc.id);

            if (acc.ParentId != null) {
                parentIds.add(acc.ParentId);
            }
        }
        
        //make sure we have all the fields we need.  Passed in accounts has ID at minimum
        initialAccounts = [SELECT Id, CBUCID_Parent_Id__c, rcid__c, ParentId, CBUCID_Name_formula__c, Name, RecordType.DeveloperName, Inactive__c, Account_Status__c
                   FROM Account
                   WHERE Id In :initialAccountIds
                   limit 1000];
                         
        allAccounts.putAll(initialAccounts);
        
        Integer queryCount = 0;
        
        while (parentIds.size() > 0 && queryCount < 3) {
            queryCount++;
            
            parents = [SELECT Id, CBUCID_Parent_Id__c, rcid__c, ParentId, CBUCID_Name_formula__c, Name, RecordType.DeveloperName, Inactive__c, Account_Status__c
                       FROM Account
                       WHERE Id In :parentIds AND Id NOT IN :allAccounts.keySet()
                       limit 1000];
            
            parentIds = new List<Id>(); 
             
            for (Account parent : parents) {
                //System.debug('2parent.id:'+parent.id);
                //System.debug('2parent.name:'+parent.name);
            
                if (parent.ParentId != null)
                    parentIds.add(parent.ParentId);
            }
            
            allAccounts.putAll(parents);
        }
        
        List<Account> children = accounts;
        
        while (children.size() > 0 && queryCount < 4) {
            queryCount++;
            
            children = [SELECT Id, CBUCID_Parent_Id__c, rcid__c, ParentId, CBUCID_Name_formula__c, Name, RecordType.DeveloperName, Inactive__c, Account_Status__c
                        From Account 
                        Where ParentId in :children AND 
                              Id NOT IN :allAccounts.keySet() AND
                              ParentId != NULL
                        limit 1000];
                        
            for (Account child : children) {
                //System.debug('3child.id:'+child.id);
                //System.debug('3child.name:'+child.name);
            }                        
                        
            //children      
            allAccounts.putAll(children);
        }
        
        Map<Id,idAndAccount> results = new Map<Id,idAndAccount>();
        
        //System.debug('1');
        
        for (Account account : allAccounts.values()) {

            //System.debug('2');
            
            //System.debug('account.RecordType.DeveloperName:'+account.RecordType.DeveloperName);
            //System.debug('account.id:'+account.id);
            //System.debug('account.name:'+account.name);
            
            if (account.RecordType.DeveloperName == 'RCID') {
                results.put(account.Id, new idAndAccount(account.Id,account));
              //  System.debug('Account name:'+account.name+', id:'+account.id);
                
                if (!results.containsKey(account.ParentId)) {
                    results.put(account.ParentId, new idAndAccount(account.Id,account));
                //    System.debug('Account name:'+account.name+', id:'+account.id);
                }
            }
            
            else if (account.RecordType.DeveloperName == 'BAN' || account.RecordType.DeveloperName == 'CAN') {
                
                Account parent =  allAccounts.get(account.ParentId);
                
                while(parent != null &&
                        parent.Id != null &&
                        parent.Id != parent.parentId &&
                        parent.RecordType.DeveloperName != 'RCID' &&
                        allAccounts.containsKey(account.ParentId)) {
                    parent =  allAccounts.get(parent.ParentId);
                    //System.debug('3');
                    //System.debug('parent id:'+parent.id);
                    //System.debug('parent name:'+parent.name);
                }
                
                if (parent != null) {
                    //System.debug('Account name:'+account.name);
                    //System.debug('Account id:'+account.id);
                    //System.debug('parentid:'+parent.id);
                    //System.debug('parent Name:'+parent.Name);
                    results.put(account.Id, new idAndAccount(parent.Id,parent));
                }
            }
        }
        
        return results;
    }

    //returns AccountIdsMappedToCBUCIDAccountIdFromList
    public static Map<Id,idAndAccount> getAccountsInHierarchyToCBUCID(List<Account> accounts) {
        
        Map<Id,Account> allAccounts = new Map<Id,Account>();
        
        List<Account> parents = new List<Account>();
        List<Account> initialAccounts = new List<Account>();
        
        List<Id> parentIds = new List<Id>();
        List<Id> initialAccountIds = new List<Id>();
        
        for (Account acc : accounts) {
            if (acc == null || 
                acc.Id == null || 
                acc.RecordType == null ||
                acc.RecordType.DeveloperName == null) continue;
            
            initialAccountIds.add(acc.id);

            if (acc.ParentId != null) {
                parentIds.add(acc.ParentId);
            }
        }

        //make sure we have all the fields we need.  Passed in accounts has ID at minimum
        initialAccounts = [SELECT Id, CBUCID_Parent_Id__c, rcid__c, ParentId, CBUCID_Name_formula__c, Name, RecordType.DeveloperName, Inactive__c, Account_Status__c
                   FROM Account
                   WHERE Id In :initialAccountIds
                   limit 1000];
                         
        allAccounts.putAll(initialAccounts);        
        
        Integer queryCount = 0;
        
        while (parentIds.size() > 0 && queryCount < 4) {
            queryCount++;
            
            parents = [SELECT Id, CBUCID_Parent_Id__c, rcid__c, ParentId, CBUCID_Name_formula__c, Name, RecordType.DeveloperName, Inactive__c, Account_Status__c
                       FROM Account
                       WHERE Id In :parentIds AND Id NOT IN :allAccounts.keySet()
                       LIMIT 1000];
            
            parentIds = new List<Id>(); 
             
            for (Account parent : parents) {
                if (parent.ParentId != null)
                    parentIds.add(parent.ParentId);
            }
            
            allAccounts.putAll(parents);
        }
        
        List<Account> children = accounts;
        
        while (children.size() > 0 && queryCount < 4) {
            queryCount++;
            
            children = [SELECT Id, CBUCID_Parent_Id__c, rcid__c, ParentId, CBUCID_Name_formula__c, Name, RecordType.DeveloperName, Inactive__c, Account_Status__c
                        From Account 
                        Where ParentId in :children AND 
                              Id NOT IN :allAccounts.keySet() AND
                              ParentId != NULL
                        LIMIT 1000];
            //children      
            allAccounts.putAll(children);
        }
        
        Map<Id,idAndAccount> results = new Map<Id,idAndAccount>();
        
        //System.debug('1');
        
        for (Account account : allAccounts.values()) {

            //System.debug('2');
            
            if (account.RecordType.DeveloperName == 'RCID') {
                results.put(account.Id, new idAndAccount(account.ParentId, allAccounts.get(account.ParentId)));
              //  System.debug('Account name:'+account.name+', id:'+account.id);
                
                if (!results.containsKey(account.ParentId)) {
                    results.put(account.ParentId, new idAndAccount(account.ParentId, allAccounts.get(account.ParentId)));
                //    System.debug('Account name:'+account.name+', id:'+account.id);
                }
            }
            
            else if (account.RecordType.DeveloperName == 'BAN' || account.RecordType.DeveloperName == 'CAN') {
                
                Account parent =  allAccounts.get(account.ParentId);
                
                while(parent != null &&
                        parent.Id != null &&
                        parent.Id != parent.parentId &&
                        parent.RecordType.DeveloperName != 'CBUCID' &&
                        allAccounts.containsKey(account.ParentId)) {
                    parent =  allAccounts.get(parent.ParentId);
                    //System.debug('3');
                }

                if (parent != null) {
                    results.put(account.Id, new idAndAccount(parent.Id,parent));
                   // System.debug('Account name:'+account.name+', id:'+account.id+', parentid:'+parent.id);
                }
            }
        }
        
        return results;
    }
    
    public static List<Id> getAccountsInHierarchy(Id accountId) {
        
        if (accountId == null) {
            throw new smb_InvalidParameterException('You must supply a valid accountId.');
        }
        
        List<Account> account = [SELECT parentId FROM Account WHERE Id = :accountId LIMIT 1];
        
        if (account.size() != 1) {
            throw new smb_InvalidParameterException('An account with ID ' + accountId + ' was not found or has been deleted.');
        }
        
        return getAccountsInHierarchy(accountId, account.get(0).parentId);
    }
    
    public static Id getRcidAccountId(Id accountId) {
        List<Account> accounts = [SELECT Id, parentId, RecordType.DeveloperName FROM Account WHERE Id = :accountId LIMIT 1];
        
        if (accounts.size() == 0) return null;
        
        if (accounts.get(0).RecordType.DeveloperName == 'RCID') return accountId;
        
        if (accounts.get(0).RecordType.DeveloperName == 'CBUCID') {
            List<Account> childAccounts = [SELECT Id FROM Account WHERE ParentId = :accountId AND RecordType.DeveloperName = 'RCID' LIMIT 1];
            
            if (childAccounts.size() == 0) return accountId;
            
            if (accounts.get(0).RecordType.DeveloperName == 'RCID') return accounts.get(0).Id;
            
            return accountId;   
        }
        
        integer counter = 0;
        
        List<Id> parentIds = new List<Id> { accounts.get(0).parentId };
        
        while (counter < 5) {
            counter++;
            
            accounts = [SELECT Id, RecordType.DeveloperName, ParentId FROM Account WHERE Id IN :parentIds];
            
            parentIds = new List<Id>();
            
            for (Account account : accounts) {
                parentIds.add(account.ParentId);
                
                if (account.RecordType.DeveloperName == 'RCID') return account.Id;
            }
        }
        
        return accountId;
  }
  
  /*
    * @author     : Puneet Khosla
    * @description  : This method returns the mapping of Accounts with their RCID Account
    * @return     : map<Id,Id> : Containing the Account with RCID map 
    * @param     : accountId : Set of accounts for which we need to find the RCID accounts 
   */
  
  public static map<Id,Id> getRcidAccountId(set<Id> accountId) 
  {
    List<Account> accounts = [SELECT Id, parentId, RecordType.DeveloperName,Name FROM Account WHERE Id in :accountId];
    set<id> cbucidAccounts = new set<id>();
    set<id> otherAccounts = new set<id>();
    set<id> otherParentAccounts = new set<id>();

    map<Id,Id> rcidMap = new map<id,id>();
    
    map<id,set<id>> childMap = new map<id,set<id>>();
    
    
    for(Account a: accounts)
    {
      //System.debug('### a.RecordType.DeveloperName : ' + a.RecordType.DeveloperName);
      if(a.RecordType.DeveloperName == 'RCID')
        rcidMap.put(a.id, a.id);
      else if(a.RecordType.DeveloperName == 'CBUCID')
        cbucidAccounts.add(a.id);
      else
      {
        //System.debug('### a : ' + a);
        //System.debug('### a.parentId : ' + a.parentId);
        
        otherAccounts.add(a.id);
        if(a.ParentId != null)
        {
          set<id> aId = new set<id>();
          if(childMap.containsKey(a.ParentId))
            aId = childMap.get(a.ParentId);
          aId.add(a.Id);
          if(childMap.containsKey(a.Id))
          {
            aId.addAll(childMap.get(a.Id));
            childMap.remove(a.Id);
          }
          childMap.put(a.ParentId,aId);
          otherParentAccounts.add(a.parentId);
        }
      }
    }
    
    if(cbucidAccounts.size() > 0)
    {
      List<Account> childAccounts = [SELECT Id,ParentId,RecordType.DeveloperName FROM Account WHERE ParentId in :cbucidAccounts AND RecordType.DeveloperName = 'RCID'];
      
      for(Account a : childAccounts)
        rcidMap.put(a.ParentId, a.id);
      
      cbucidAccounts = null;
      childAccounts = null;
    }
    
    integer counter = 0;
    
    while(counter < 10 && otherParentAccounts.size() > 0)
    {
      counter ++;
      
      accounts = [SELECT Id, RecordType.DeveloperName, ParentId FROM Account WHERE Id IN :otherParentAccounts];
      otherParentAccounts = new set<id>();
      for (Account account : accounts)
      {
        // If no Parent then no need to iterate further for that account
        //System.debug('### account : ' + account);
        //System.debug('### account.parentId : ' + account.parentId);
        if(account.parentId == null )
        {
          if(childMap.containsKey(account.Id))
            childMap.remove(account.Id);  
        }
        else
        {
          // If RCID account found, then no more traversing
          if (account.RecordType.DeveloperName != 'RCID')
          {
            otherParentAccounts.add(account.ParentId);
            // Create the map
            set<id> aId = new set<id>();
            if(childMap.containsKey(account.ParentId))
              aId = childMap.get(account.ParentId);
            aId.add(account.Id);
            if(childMap.containsKey(account.Id))
            {
              aId.addAll(childMap.get(account.Id));
              childMap.remove(account.Id);
            }
            childMap.put(account.ParentId,aId);
          }
        }
      }
    }
    if(childMap.size() > 0)
      for(id idSet : childMap.keyset())
        for(id childId : childMap.get(idSet))
          if(otherAccounts.contains(childId))
            rcidMap.put(childId, idSet);

    return rcidMap;
    }
  // L&R - Function Ends
    
    public static Id getUltimateParentId(Id accountId) {
        
        boolean foundUltimateParent = false;
        Id currentId = accountId;
        integer counter = 0;
        
        while (!foundUltimateParent && counter < 5){
            counter++;
            
            List<Account> parentAccounts = [SELECT ParentId 
                                            FROM Account 
                                            WHERE Id = :currentId
                                            LIMIT 1];
            
            if (parentAccounts.size() == 0) {
                foundUltimateParent = true;
            }
            else if (parentAccounts.get(0).ParentId != null) {
                currentId = parentAccounts.get(0).ParentId;
            }
            else {
                foundUltimateParent = true;
                currentId = parentAccounts.get(0).Id;
            }
        }
        
        return currentId;
        
        
    }
    
    public static String getAccountNumber(Account account) {
        if (account == null || account.RecordType == null) return '';
            
        if (account.RecordType.DeveloperName == 'CBUCID' || account.RecordType.DeveloperName == 'RCID') {
            return account.CBUCID_RCID__c;
        }

        if (account.RecordType.DeveloperName == 'CAN' || account.RecordType.DeveloperName == 'BAN') {
            return account.CAN__c;
        }
            
        return '';
    }
    
    public static string getAccountNumberLabel(string recordTypeDeveloperName) {
        if (recordTypeDeveloperName == 'CBUCID') {
                return 'CBUCID Number';
        }
        
        if (recordTypeDeveloperName == 'RCID') {
            return 'RCID Number';
        }
        
        if (recordTypeDeveloperName == 'CAN') {
            return 'Customer Account Number';
        }
        
        if (recordTypeDeveloperName == 'BAN') {
            return 'Billing Account Number';
        }
        
        if (recordTypeDeveloperName == 'BCAN') {
            return 'Consolidated Account Number';
        }
        
        return '';
    }
    
    public static string getAccountNumberShortLabel(string recordTypeDeveloperName) {
        
        if (recordTypeDeveloperName == 'CBUCID') {
            return 'CBUCID';
        }
        
        if (recordTypeDeveloperName == 'RCID') {
            return 'RCID';
        }
        
        if (recordTypeDeveloperName == 'CAN') {
            return 'CAN';
        }
        
        if (recordTypeDeveloperName == 'BAN') {
            return 'BAN';
        }
        
        return '';
    }

}