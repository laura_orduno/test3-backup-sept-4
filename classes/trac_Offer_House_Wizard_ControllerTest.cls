@isTest
private class trac_Offer_House_Wizard_ControllerTest {
  public static testMethod void testTheOfferHouseCreateSubmit() {
		insertTestSettings();
        
		User tester = insertTestUser();
    
    // Create Account
    Account a = new Account(name = 'Test Sales Account', BillingCountry = 'US');
    system.runAs(tester) {  insert a; }
    
    Test.startTest();
    
        // Initialize page without Id and controller
        PageReference theNewPage = Page.trac_Offer_House_Wizard;
        theNewPage.getParameters().put('ACCOUNTID', a.Id);
        theNewPage.getParameters().put('ACCOUNTNAME', a.Name);
        
        Test.setCurrentPage(theNewPage);
        
        // Begin wizard as Sales User
        system.runAs(tester) {
          
          // Init new offer house record
          trac_Offer_House_Wizard_Controller theController = new trac_Offer_House_Wizard_Controller();
          system.assert(theController.theOffer != null);
          
          // Save the new record
          //theController.theOffer.Name = 'Test Offer';
          Test.setCurrentPage(theController.doFirstSave());
          theController = new trac_Offer_House_Wizard_Controller();
          system.assertEquals(false, theController.isCreate);
          system.assertEquals(false, theController.wizardError);
          
          // Cancel request
          theController.doCancelRequest();
          system.assertEquals(false, theController.isCreate);
          system.assertEquals(false, theController.wizardError);
          
          // Un-cancel request
          theController.doUnCancelRequest();
          system.assertEquals(false, theController.isCreate);
          system.assertEquals(false, theController.wizardError);
      system.assertEquals('In Progress', theController.approvalProcessStatus);
          
          // Save the record
          theController.doSave();
      system.assertEquals('In Progress', theController.approvalProcessStatus);
          system.assertEquals(false, theController.isCreate);
          system.assertEquals(false, theController.wizardError);
          
          // Back to account page
          theController.doCancel();
          
          system.assertEquals(false, theController.wizardError);
          system.assertEquals(0, theController.theOffer.Version_Number__c);
          
          // Submit for approval
          theController.doSubmit();
          //system.assertEquals('Submitted', theController.approvalProcessStatus);
          //system.assertEquals(false, theController.wizardError);
          
          // Recall approval
          theController.doRecall();
     // system.assertEquals(false, theController.wizardError);
          
          // Re-Submit for approval
          theController.doSubmit();
      //system.assertEquals('Re-submitted', theController.approvalProcessStatus);
          //system.assertEquals(false, theController.wizardError);
          system.assert(theController.theOffer.Version_Number__c>=0);
          
       }

    Test.stopTest();
  }
    
    
  public static testMethod void testTheOfferHouseCreateAccept() {
    insertTestSettings();
    
    User tester = insertTestUser();
            
    // Create Account
    Account a = new Account(name = 'Test Sales Account', BillingCountry = 'US');
    system.runAs(tester) {  insert a; }
        
    Test.startTest();
        
        // Initialize page without Id and controller
        PageReference theNewPage = Page.trac_Offer_House_Wizard;
    theNewPage.getParameters().put('ACCOUNTID', a.Id);
    theNewPage.getParameters().put('ACCOUNTNAME', a.Name);
        Test.setCurrentPage(theNewPage);
    
        // Begin wizard as Offer House User
        system.runAs(tester) {
      
        // Init new offer house record
        trac_Offer_House_Wizard_Controller theController = new trac_Offer_House_Wizard_Controller();
        system.assert(theController.theOffer != null);
          
        // Save the new record
        //theController.theOffer.Name = 'Test Offer';
        Test.setCurrentPage(theController.doFirstSave());
        theController = new trac_Offer_House_Wizard_Controller();
        system.assertEquals(false, theController.isCreate);
        system.assertEquals(false, theController.wizardError);
        
        // Submit for approval
        theController.doSubmit();
    //system.assertEquals(false, theController.wizardError);
        
        // Test the comparison funciton
        theController.theJSON = null;
        theController.getTheJSON();
        
        // Reject
    theController.theOffer.Offer_House_Agent__c = tester.Id;
        theController.doReject();
    //system.assertEquals(false, theController.wizardError);
        
        // Submit again
        theController.doSubmit();
        //system.assertEquals(false, theController.wizardError);
        
        // Approve
        theController.doApprove();
        system.assertNotEquals(null, theController.approvalProcessStatus);
    //system.assertEquals(false, theController.wizardError);

        // Save approved record
       // theController.theOffer.Name = 'New Name';
        theController.doSave();
        //system.assertEquals('Review Required', theController.approvalProcessStatus);
        system.debug('Create approve test success!');
      
  	}
  
	}
	
	@isTest
	private static void insertTestSettings() {
		Offer_House_Wizard_Settings__c settings = new Offer_House_Wizard_Settings__c(
    	Admin_Profile__c = 'System Admin TEAM no PW expiry,System Admin TEAM, TELUS Marketing (Super User)',
    	Offer_House_Profile__c = 'Offer House',
    	Sales_Profile__c = 'ENT Sales Manager / Director / MD, ENT Sales Prime/ASR/Coordinator, TQ ENT Sales Manager / Director / MD, TQ ENT Sales Prime / ASR / Coordinator'
    );
    insert settings;
	}
	
	@isTest
  private static User insertTestUser() {
  	    // User must have a profile and a role
    Id PROFILEID = [SELECT id FROM Profile WHERE name='System Admin TEAM no PW expiry' LIMIT 1].id;
    
    // Get a user to test
    User tester = new User(
    	username = Datetime.now().getTime() + 'offer@TRACTIONSM.COM',
      email = 'test@example.com',
      title = 'test',
      lastname = 'test',
      alias = 'test',
      TimezoneSIDKey = 'America/Los_Angeles',
      LocaleSIDKey = 'en_US',
      EmailEncodingKey = 'UTF-8',
      ProfileId = PROFILEID,
      LanguageLocaleKey = 'en_US');
    insert tester;
    
    return tester;
  }
}