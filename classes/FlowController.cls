public class FlowController {

    public Flow.Interview.New_TF_case newTFcaseFlow { get; set; }
    
    //get Language parameter from vf page to be pass to flow
    public String Userlanguage{
         get {
             String strTemp = '';             
             strTemp = apexpages.currentpage().getparameters().get('Userlanguage');
             return strTemp;
     }
         set { Userlanguage = value; }
     }
     //get Type parameter from vf page to be pass to flow   
     public String Type{
         get {
             String strTemp = '';             
             strTemp = apexpages.currentpage().getparameters().get('Type');
             return strTemp;
     }
         set { Type= value; }
     }
     
     PageReference cpRef = ApexPages.currentPage();
     public PageReference prFinishLocation {         
         get {  
             PageReference prRef = new PageReference('/500/e?retURL=%2F500%2Fo&RecordType='+strRecordType);
             if (strCaseId != null)
                 if(strCaseOrigin == 'Go/CRM'){
                 //prRef = new PageReference('/CRMEnablement/apex/NewTELUSforceCase_VF_ConsoleView?Type=Other&Userlanguage='+Userlanguage); 
                 prRef = new PageReference('/CRMEnablement/apex/GoCRM_Flow_Finish_Page');            
             } else {
                 prRef = new PageReference('/'+strCaseId);  
             }                        
             prRef.setRedirect(true);
             return prRef;
         }
         set { prFinishLocation = value; }
     }
     // Factor newTFcaseFlow RecordType variable pull as a full GET / SET
     public String strRecordType {
         get {
             String strTemp = '';
             if(newTFcaseFlow!= null) {
             strTemp = string.valueOf(newTFcaseFlow.getVariableValue('RecordType'));
         }

             return strTemp;
     }
         set { strRecordType = value; }
     }
     // Factor newTFcaseFlow CaseId variable pull as a full GET / SET 
     public String strCaseId {
         get {
             String strTemp = '';
             if(newTFcaseFlow!= null) {
             strTemp = string.valueOf(newTFcaseFlow.getVariableValue('CaseId'));
         }

             return strTemp;
     }
         set { strCaseId = value; }
     } 
     // Factor newTFcaseFlow CaseOrigin variable pull as a full GET / SET
     public String strCaseOrigin {
         get {
             String strTemp = '';
             if(newTFcaseFlow!= null) {
             strTemp = string.valueOf(newTFcaseFlow.getVariableValue('CaseOrigin'));
         }

             return strTemp;
     }
         set { strCaseOrigin = value; }
     }
}