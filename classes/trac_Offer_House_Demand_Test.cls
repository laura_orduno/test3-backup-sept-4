/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
*/
@isTest
private class trac_Offer_House_Demand_Test {
    
    static testMethod void myUnitTest() {
        
        // User must have a profile and a role
        Id PROFILEID = [SELECT id FROM Profile WHERE name='Standard User' LIMIT 1].id;
        
        // Create Owner Director
        User ownerDirector = new User(  username = 'OFFERHOUSEUSER1@TRACTIONSM.COM',
                                      email = 'OFFERHOUSEUSER@example.com',
                                      title = 'OFFERHOUSEUSER',
                                      lastname = 'OFFERHOUSEUSER',
                                      alias = 'ofu',
                                      TimezoneSIDKey = 'America/Los_Angeles',
                                      LocaleSIDKey = 'en_US',
                                      EmailEncodingKey = 'UTF-8',
                                      ProfileId = PROFILEID,
                                      LanguageLocaleKey = 'en_US');
        insert ownerDirector;
        
        // Create Owner Manager
        User ownerManager = new User(  username = 'OFFERHOUSEUSER12@TRACTIONSM.COM',
                                     email = 'OFFERHOUSEUSER@example.com',
                                     title = 'OFFERHOUSEUSER',
                                     lastname = 'OFFERHOUSEUSER',
                                     alias = 'ofu',
                                     Sales_Manager__c = ownerDirector.Id,
                                     TimezoneSIDKey = 'America/Los_Angeles',
                                     LocaleSIDKey = 'en_US',
                                     EmailEncodingKey = 'UTF-8',
                                     ProfileId = PROFILEID,
                                     LanguageLocaleKey = 'en_US');
        insert ownerManager;
        
        // Create Offer House Owner
        User u = new User(  username = 'OFFERHOUSEUSER123@TRACTIONSM.COM',
                          email = 'OFFERHOUSEUSER@example.com',
                          title = 'OFFERHOUSEUSER',
                          lastname = 'OFFERHOUSEUSER',
                          alias = 'ofu',
                          Sales_Manager__c = ownerManager.Id,
                          TimezoneSIDKey = 'America/Los_Angeles',
                          LocaleSIDKey = 'en_US',
                          EmailEncodingKey = 'UTF-8',
                          ProfileId = PROFILEID,
                          LanguageLocaleKey = 'en_US');
        insert u;
        
        // Insert with lowest user as owner
        Offer_House_Demand__c theOffer1 = new Offer_House_Demand__c(/*Name = 'Test Offer1',*/
            OwnerId = u.Id);
        map<string,schema.recordtypeinfo> recordTypeMap=offer_house_demand__c.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        theOffer1.recordtypeid=recordTypeMap.get('WLN Deal Support').getrecordtypeid();
        theOffer1.type_of_contract__c='CCA';
        theOffer1.signature_required__c='Yes';
        insert theOffer1;
        theOffer1 = [SELECT Id, OwnerId, Owner_Manager__c, Owner_Director__c
                     FROM Offer_House_Demand__c
                     WHERE Id = :theOffer1.Id];
        
        // Insert with lowest user as owner
        Offer_House_Demand__c theOffer2 = new Offer_House_Demand__c(/*Name = 'Test Offer2',*/
            OwnerId = ownerManager.Id);
        theOffer2.recordtypeid=recordTypeMap.get('WLN Deal Support').getrecordtypeid();
        theOffer2.type_of_contract__c='CCA';
        theOffer2.signature_required__c='Yes';
        insert theOffer2;
        theOffer2 = [SELECT Id, OwnerId, Owner_Manager__c, Owner_Director__c
                     FROM Offer_House_Demand__c
                     WHERE Id = :theOffer2.Id];
        
        // Insert with lowest user as owner
        Offer_House_Demand__c theOffer3 = new Offer_House_Demand__c(/*Name = 'Test Offer3',*/
            OwnerId = ownerDirector.Id);
        theOffer3.recordtypeid=recordTypeMap.get('WLN Deal Support').getrecordtypeid();
        theOffer3.type_of_contract__c='CCA';
        theOffer3.signature_required__c='Yes';
        insert theOffer3;
        theOffer3 = [SELECT Id, OwnerId, Owner_Manager__c, Owner_Director__c
                     FROM Offer_House_Demand__c
                     WHERE Id = :theOffer3.Id];
        
    }
}