@isTest
private class trg_aftr_ins_upd_attachmentTest {
    
    private static testMethod void testAttachment(){
       
            TestDataHelper.testContractLineItemListCreation_ConE();
            Doc_url_settings__c a2 = new Doc_url_settings__c();
            a2.Name = 'vlocity_cmt__ContractVersion__c';
            insert a2;
            vlocity_cmt__ContractVersion__c a3 = new vlocity_cmt__ContractVersion__c();        
            a3.Name = 'vlocity_cmt__ContractVersion__c';
            a3.vlocity_cmt__ContractId__c = TestDataHelper.testContractObj.id;
            insert a3;
            system.debug('a3id:'+a3.Id); 
            test.startTest();
            Attachment a1 = new Attachment();
            a1.Name = 'vlocity_cmt__ContractVersion__c';
            a1.ParentId = a3.id ;
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');        
            a1.Body = bodyBlob ;
            insert a1;
            test.stopTest();
      }
    
}