/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class IC_SolutionOustandingNotifyBatchTest {
    public static String CRON_EXP = '0 0 4 * * ?';
    static testMethod void testbtachJob() {
      Test.startTest();

      // Schedule the test job
      String jobId = System.schedule('IC_SolutionOustandingNotifyBatch', CRON_EXP, new IC_SolutionRemindNotifyScheduler());

      // Get the information from the CronTrigger API object
      CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered,  NextFireTime   FROM CronTrigger WHERE id = :jobId];

      // Verify the expressions are the same
      System.assertEquals(CRON_EXP, ct.CronExpression);

      // Verify the job has not run
      System.assertEquals(0, ct.TimesTriggered);

      
      Test.stopTest();   
    }
    @isTest(SeeAllData=true)
    static void testBatch() {
	Test.startTest();
       String query = 'Select id, name, RecordType.Name, Inventory_Item__r.Device_Provider__r.ICS_Notification_Email__c from IC_Solution__c where Status__c = :status and RecordType.Name= :recodTypeName and CreatedDate < LAST_N_DAYS:'+ 1 + ' Limit 5';
       Database.executeBatch(new IC_SolutionOustandingNotifyBatch(query,'New','Loaner Device'), 5);
   	 Test.stopTest();
   }
}