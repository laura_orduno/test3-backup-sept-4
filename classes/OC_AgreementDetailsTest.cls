@isTest
private class OC_AgreementDetailsTest {
    private static order ordObj;
    private static contact testContactObj;

    static void setupData(){
    
        Profile p = [select id from profile where name='Standard User'];
        User testUser = new User(alias = 'test123', email='ocom_test123qwert@telus.com', emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', localesidkey='en_US', profileid = p.Id, country='Canada', timezonesidkey='America/Los_Angeles', username='ocom_test123qwert@telus.com');
        insert testUser;
        
        String orderId= OrdrTestDataFactory.singleMethodForOrderId();
        ordObj=[select Account.Credit_Profile__c,AccountId, Credit_Check_Not_Required__c,RequestedDate__C,DeliveryMethod__c,status,Rush__c,Ban__c,Shipping_Address__c,orderMgmtId__c FROM Order where id=:orderId];
        ordObj.orderMgmtId__c='OS1234567890';
        ordObj.status='Not Submitted';
        ordObj.Rush__c=true;
        ordObj.Ban__c='12345';
        ordObj.Shipping_Address__c='test address';
        ordObj.Credit_Check_Not_Required__c=false;
        Date day1 = Date.today();
        Date todayPlus = day1.addDays(4);
        ordObj.RequestedDate__C=todayPlus;
        ordObj.Sales_Representative__c=testUser.id;
        ordObj.DeliveryMethod__c = 'Email';
        update ordObj;
        
        Account anAccount = [select Account_Status__c from Account where id=:ordObj.AccountId limit 1];
        anAccount.Account_Status__c = 'ACTIVE';
        update anAccount;


          /*List<Order> orders = OrdrTestDataFactory.singleMethodForOrderId();

         //OrdrTestDataFactory.singleMethodForOrderId();
         system.debug('OrderID_______>'+ orders);

         if(orders.size()> 0 && !orders.isEmpty() ){
            system.debug('Order Info' + orders);
            ordObj = orders[0];
         }
         ordObj.DeliveryMethod__c = 'Email';
          ordObj.Credit_Check_Not_Required__c=false;
         update ordObj;*/
        List<product2> prod2UpdateList = new List<product2>();

        for(OrderItem OrdrItem : [Select Id,PriceBookEntry.Product2.Id ,PriceBookEntry.product2.Sellable__c,
                                              Pricebookentry.Product2.vlocity_cmt__TrackAsAgreement__c
                                              FROM OrderItem WHERE OrderId = :ordObj.id 
                                              ORDER BY vlocity_cmt__LineNumber__c Limit 1]){ 
                                Product2 prod2Update = new product2(id = OrdrItem.PriceBookEntry.Product2.Id , vlocity_cmt__TrackAsAgreement__c = true, Sellable__c = true);
                                prod2UpdateList.add(prod2Update);
            }

            if(prod2UpdateList != null && !prod2UpdateList.isEmpty()  ){
                update prod2UpdateList;
            }
    }
    
    
    @isTest static void testOCOM_ContractDetails() {
        setupData();
        Contact newContact = new Contact( AccountId = ordObj.accountID, firstname = 'Test', LastName = 'Contact', phone = '12334555', Email='test@mail.com', Language_Preference__c = 'English');

        testContactObj = new Contact(firstname = 'TestConFirstName', LastName = 'TestConLastname', accountId = ordObj.AccountId , Phone = '123456' , fax = '1234567', Email = 'test@gmail.com');
            insert testContactObj; 
        Test.startTest();
            ApexPages.currentPage().getParameters().put('id',ordObj.id);
            OC_AgreementDetails objOCOM_ContractDetails=new OC_AgreementDetails();
            OC_AgreementDetails.contractDetailsWrapper conWrapper = new OC_AgreementDetails.contractDetailsWrapper();
            objOCOM_ContractDetails.objOrder = ordObj;
            system.debug('objOrder___' + objOCOM_ContractDetails.objOrder);
            objOCOM_ContractDetails.getcontractableOrderCheck();
            objOCOM_ContractDetails.ContId = testContactObj.id;
            //objOCOM_ContractDetails.UpdateOrder();
           // objOCOM_ContractDetails.searchString = 'Test';
            objOCOM_ContractDetails.accountID = ordObj.AccountId;
            //objOCOM_ContractDetails.getSearchResult();
           // List<Contact> contactList1 = objOCOM_ContractDetails.loadContacts();
           // System.assertEquals(contactList1.size()>0, true );
            //List<Contact> contactList2 = objOCOM_ContractDetails.loadContacts();
           // System.assertEquals(contactList2.size()>0, true );
            objOCOM_ContractDetails.new_contact = newContact;
           // objOCOM_ContractDetails.createNewContact();
            objOCOM_ContractDetails.getOrderDetail();
        Test.stopTest();
        
        
    }

     @isTest static void testOCOM_ContractDetails2() {
        setupData();

        Contact newContact = new Contact( AccountId = ordObj.accountID, firstname = 'Test', LastName = 'Contact', phone = '12334555', Email='test@mail.com', Language_Preference__c = 'English');
        insert newContact;

         ordObj.CustomerSignedBy__c = newContact.id;

        testContactObj = new Contact(firstname = 'TestConFirstName', LastName = 'TestConLastname', accountId = ordObj.AccountId , Phone = '123456' , fax = '1234567', Email = 'test@gmail.com');
            insert testContactObj; 
        Test.startTest();
            ApexPages.currentPage().getParameters().put('id',ordObj.id);
            OC_AgreementDetails objOCOM_ContractDetails=new OC_AgreementDetails();
            OC_AgreementDetails.contractDetailsWrapper conWrapper = new OC_AgreementDetails.contractDetailsWrapper();
            objOCOM_ContractDetails.objOrder = ordObj;
            system.debug('objOrder___' + objOCOM_ContractDetails.objOrder);
            objOCOM_ContractDetails.getcontractableOrderCheck();
            objOCOM_ContractDetails.ContId = testContactObj.id;
           // objOCOM_ContractDetails.UpdateOrder();
            //objOCOM_ContractDetails.searchString = 'TestCon';
            objOCOM_ContractDetails.accountID = ordObj.AccountId;
            //objOCOM_ContractDetails.getSearchResult();
            //objOCOM_ContractDetails.loadContacts();
            objOCOM_ContractDetails.new_contact = new contact(lastname='test');
            //objOCOM_ContractDetails.createNewContact();
            objOCOM_ContractDetails.new_contact = new contact(FirstName='test');    
           // objOCOM_ContractDetails.createNewContact();
            ApexPages.Message[] pageMessages = ApexPages.getMessages();
         
          	Boolean pageMessageFound =   checkPageMessages(pageMessages,ApexPages.severity.ERROR, 'Please enter Contact First Name');
          	//System.assert(pageMessageFound);
          	
            objOCOM_ContractDetails.new_contact = new contact(FirstName='test',lastname='Contact');    
           // objOCOM_ContractDetails.createNewContact();
            ApexPages.Message[] pageMessages1 = ApexPages.getMessages();
          	Boolean pageMessageFound1 =   checkPageMessages(pageMessages,ApexPages.severity.ERROR, 'Please enter Contact Last Name');
          	//System.assert(pageMessageFound1);
            
            objOCOM_ContractDetails.new_contact = new contact(FirstName='test1',lastname='Contact', Phone = '122345553');    
            //objOCOM_ContractDetails.createNewContact();
            ApexPages.Message[] pageMessages2 = ApexPages.getMessages();
          	Boolean pageMessageFound2 =   checkPageMessages(pageMessages2,ApexPages.severity.WARNING, 'Please enter Contact phone');
          	//System.assert(pageMessageFound2);
            
            objOCOM_ContractDetails.new_contact = new contact(FirstName='test1',lastname='Contact', Phone = '122345553',Email = '12333@test.com' );    
           // objOCOM_ContractDetails.createNewContact();
            ApexPages.Message[] pageMessages3 = ApexPages.getMessages();
          	Boolean pageMessageFound3 =   checkPageMessages(pageMessages3,ApexPages.severity.INFO, 'Please enter Contact Email');
          	//System.assert(pageMessageFound3);
         
            objOCOM_ContractDetails.getOrderDetail();
        Test.stopTest();
        
        
    }

    public static Boolean checkPageMessages(ApexPages.Message[] pageMessages, ApexPages.Severity msgSeverity, String messageDetails){
    		system.debug('msgSeverity___' +msgSeverity);

			//System.assertNotEquals(0, pageMessages.size());

			// Check that the error message you are expecting is in pageMessages
			Boolean messageFound = false;

			for(ApexPages.Message message : pageMessages) {
				system.debug('message.getDetail()___' +message.getDetail());
				system.debug('message.getSeverity()____' +message.getSeverity());
			    if(message.getDetail() == messageDetails
			        && message.getSeverity() == msgSeverity) {
			        messageFound = true;        
			    }
			   
			}
			 return messageFound;
	}
    

    
}