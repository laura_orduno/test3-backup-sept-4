/*
    Quote Migrator
    QuoteMigrator.cls
    Part of the TELUS QuoteQuickly Portal
    
    Author: Alex Miller @ Traction <amiller@tractionondemand.com>
    Created: March 5, 2012
    
    Since: Release 5
    Initial Definition: R5rq12
    
    Known Issues:
     -- None Yet --
    
    Things to Work on:
     - modularizing the doMigration() method. currently a lot (pretty much all) of the migration logic is locked inside.
            It may be beneficial to move some pieces to more methods.
            

    Outstanding Questions:
     -- None Yet --

    New Field Audit:
     - Object -          - API Name -            - Label -           - Type -                    - Since -
    
*/
public without sharing class QuoteMigrator {
    private system.savepoint rollbackSp {get; set;}
    public Boolean success {get; private set;}
    private boolean debug = false;
    
    // Quote Variables
    public String quoteId {get; private set;}
    private SBQQ__Quote__c quote {get; set;}
    
    // Opportunity Variables
    /*  Currently not in use.
    private Opportunity Opp;
    private Opportunity Opp; */
    
    // Quote Line Variables
    private Map<Id, SBQQ__QuoteLine__c> quoteLines {get; set;}
    private List<SBQQ__QuoteLine__c> quoteLinesToAdd {get; set;}
    private Map<Id, SBQQ__QuoteLine__c> quoteLinesToDelete {get; set;}
    
    // Additional Information Variables
    private Map<Id, AdditionalInformation__c> additionalInformations {get; set;}
    private Map<Id, AdditionalInformation__c> additionalInformationsToDelete {get; set;}
    private Map<Id, AdditionalInformation__c> qlToAdditionalInformations {get; set;}
    
    // Product Variables
    private Id[] productIds {get; set;}
    private Map<Id, Product2> products {get; set;}
    private Pricebook2 pricebook {get; set;}
    private PricebookEntry[] pricebookEntries {get; set;}
    private Map<Id, PricebookEntry> productToPricebookEntries {get; set;}
    
    private Map<Id, SBQQ__ProductOption__c> productToSourceProductOption {get; set;}
    private Map<Id, SBQQ__ProductOption__c> productToDestinationProductOption {get; set;}
    
    // Quote Migration Route
    private String migrationRouteId {get; set;}
    private Quote_Migration_Route__c migrationRoute {get; set;}
    private Map<Id, Quote_Migration_Action__c> migrationActions {get; set;}
    
    // Added for 'Saving Additional Information Records'
    private Map<Id, AdditionalInformation__c> actionToSavedAdditionalInformation {get; set;}
    private Map<Id, SBQQ__Quoteline__c> actionToQMAaddedQuoteLine {get; set;}
    
    // Messaging Variables
    private String message {get; set;}
    public string messages {get{
        return EncodingUtil.urlEncode(message, 'UTF-8');
    }}
    public String[] pageMessages {get; private set;}
    private integer lastMessageProcessed = 0;

    private void init() {
        // Quote init
        quote = null;
        
        // Quote Line init
        quoteLines = new Map<Id, SBQQ__QuoteLine__c>();
        quoteLinesToAdd = new List<SBQQ__QuoteLine__c>();
        quoteLinesToDelete = new Map<Id, SBQQ__QuoteLine__c>();
        
        // Additional Information init
        additionalInformations = new Map<Id, AdditionalInformation__c>();
        additionalInformationsToDelete = new Map<Id, AdditionalInformation__c>();
        qlToAdditionalInformations = new Map<Id, AdditionalInformation__c>();
        
        // Quote Migration Route
        migrationRouteId = '';
        migrationRoute = null;
        migrationActions = new Map<Id, Quote_Migration_Action__c>();
        
        // Added for 'Saving Additional Information Records'
        actionToSavedAdditionalInformation = new Map<id, AdditionalInformation__c>();
        actionToQMAaddedQuoteLine = new Map<Id, SBQQ__QuoteLine__c>();
        
        // Product init
        productIds = new List<Id>();
        products = new Map<Id, Product2>();
        pricebook = null;
        pricebookEntries = new List<PricebookEntry>();
        productToPricebookEntries = new Map<Id, PricebookEntry>();
        
        // Product Option init
        productToSourceProductOption = new Map<Id, SBQQ__ProductOption__c>();
        productToDestinationProductOption = new Map<Id, SBQQ__ProductOption__c>();
        
        // Messaging init
        pageMessages = new List<String>();
        message = '';
        
        // Get a savepoint
        setSavepoint();
    }

    public QuoteMigrator() {init();}
    public QuoteMigrator(String qid, String mri) {
        init();
        quoteId = qid;
        migrationRouteId = mri;
    }
    public boolean doMigration(String qid, String mri) {
        quoteId = qid;
        migrationRouteId = mri;
        return doMigration();
    }
    public boolean doMigration() {
        if (QuoteId == null || QuoteId == '') {
            // Return error that no quote id was provided.
            system.debug('QuoteMigrator::doMigration: no Id provided. Migration failed.'); success = false;addMessage('Migration failed');addMessage('No quote');rollitback();return success;
        }
        
        if (debug) addMessage('Migration in progress. Quote Id: ' + quoteId);
        
        /* Begin Migration Setup */
        // Load the quote record
        quote = QuoteUtilities.loadQuote(quoteId);
        if (quote == null || quote.Id == null) {
            // Return error that the quote could not be loaded.
            system.debug('QuoteMigrator::doMigration: Error loading quote with Id ' + quoteId + '. Migration failed.');success = false;addMessage('Migration failed');addMessage('Unable to load quote record');rollitback();return success;
        }
        
        // Load Quote Migration Route
        if (migrationRouteId == null) {
            // Return error that no quote migration route id was provided.
            system.debug('QuoteMigrator::doMigration: no quote migration route id provided. Migration failed.');success = false;addMessage('Migration failed');addMessage('No quote migration route');rollitback();return success;
        }
        migrationRoute = [Select Name, Source_Product__c, Source_Product__r.Name, Destination_Product__c, Destination_Product__r.Name From Quote_Migration_Route__c Where Id = :migrationRouteId Limit 1];
        if (migrationRoute == null || migrationRoute.Id == null) {
            // Return error that the quote migration route could not be loaded.
            system.debug('QuoteMigrator::doMigration: Error loading quote migration route with Id ' + migrationRouteId + '. Migration failed.');success = false;addMessage('Migration failed');addMessage('Unable to load quote migration route record');rollitback();return success;
        }
        
        // Load Quote Migration Route
        migrationActions = new Map<Id, Quote_Migration_Action__c> ([Select Name, Product__c, Product__r.Name, Parent_Product__c, Parent_Product__r.Name, Action__c, Save_Additional_Information__c, Save_Additional_Information_For__c From Quote_Migration_Action__c Where Quote_Migration_Route__c = :migrationRoute.Id]);
        if (migrationActions == null || migrationActions.size() == 0) {
            // Return error that the quote migration actions could not be loaded.
            system.debug('QuoteMigrator::doMigration: Error loading quote migration actions from migration route Id ' + migrationRoute.Id + '. Migration failed.');
            success = false;addMessage('Migration failed');addMessage('Unable to load quote migration action records');rollitback();return success;
        }
        
        // Load Products
        productIds.add(migrationRoute.Source_Product__c);
        productIds.add(migrationRoute.Destination_Product__c);
        for (Quote_Migration_Action__c qma : migrationActions.values()) {
            productIds.add(qma.Product__c);
        }
        if (productIds == null || productIds.size() == 0) {
            // Return error that there are no products to load.
            system.debug('QuoteMigrator::doMigration: No products to load. Migration failed.');success = false;addMessage('Migration failed');addMessage('No product records to load');rollitback();return success;
        }
        products = new Map<Id, Product2>( [Select Name, IsActive, Category__c, MTM__c, X1_Year__c, X2_Year__c, X3_Year__c From Product2 Where Id in :productIds] );
        if (products == null || products.size() == 0) {
            // Return error that products could not be loaded.
            system.debug('QuoteMigrator::doMigration: Error loading products. Migration failed.');success = false;addMessage('Migration failed');addMessage('Unable to load product records');rollitback();return success;
        }
        
        // Load Pricebook
        String pbn = quote.Province__c + ' - Band ' + quote.Rate_Band__c;
        pricebook = [Select Id From PriceBook2 Where IsActive = true And Name = :pbn Limit 1];
        if (pricebook.Id == null) {
            system.debug('QuoteMigrator::doMigration: Error retrieving pricebook.');success = false;addMessage('Migration failed');addMessage('Couldn\'t load pricebook.');rollitback();return success;
        }
        
        // Load PricebookEntry2s
        pricebookEntries = [Select Name, UnitPrice, Product2Id From PricebookEntry Where Pricebook2Id = :pricebook.Id and Product2Id in :productIds];
        if (pricebookEntries == null || pricebookEntries.size() == 0) {
            system.debug('QuoteMigrator::doMigration: Error retrieving pricebook.');success = false;addMessage('Migration failed');addMessage('Couldn\'t load pricebook.');rollitback();return success;
        } else {
            for (PricebookEntry pe : pricebookEntries) {
               productToPricebookEntries.put( pe.Product2Id, pe );
            }
        }
        
        // Load Product Options
        Id[] qmaAddProductIds = new List<Id>();
        Map<Id, Id> optionalToConfiguredSkuForDestinationProductOption = new Map<Id, Id>();
        for (Quote_Migration_Action__c qma : migrationActions.values()) {
            if (qma.Action__c == 'Add') {
                qmaAddProductIds.add(qma.Product__c);
                if (qma.Parent_Product__c != null) {
                   productIds.add(qma.Parent_Product__c);
                   optionalToConfiguredSkuForDestinationProductOption.put(qma.Product__c, qma.Parent_Product__c);
                }
            }
        }
        if (qmaAddProductIds.size() != 0) {
            SBQQ__ProductOption__c[] pos = [Select SBQQ__OptionalSku__c, SBQQ__ConfiguredSku__c, SBQQ__Bundled__c From SBQQ__ProductOption__c Where SBQQ__ConfiguredSku__c = :migrationRoute.Destination_Product__c and SBQQ__OptionalSku__c in :qmaAddProductIds];
            if (pos != null && pos.size() != 0) {
                for (SBQQ__ProductOption__c po : pos) {
                    productToDestinationProductOption.put( po.SBQQ__OptionalSku__c, po );
                }
            }
        }
        if (optionalToConfiguredSkuForDestinationProductOption.size() != 0) {
            for (Id optionalId : optionalToConfiguredSkuForDestinationProductOption.keyset()) {
                SBQQ__ProductOption__c[] pos = [Select SBQQ__OptionalSku__c, SBQQ__ConfiguredSku__c, SBQQ__Bundled__c From SBQQ__ProductOption__c Where SBQQ__ConfiguredSku__c = :optionalToConfiguredSkuForDestinationProductOption.get(optionalId) and SBQQ__OptionalSku__c = :optionalId];
                if (pos != null && pos.size() != 0) {
                    for (SBQQ__ProductOption__c po : pos) {
                        productToDestinationProductOption.put( po.SBQQ__OptionalSku__c, po );
                    }
                }
            }
        }        
        // Load the quote lines
        quoteLines = QuoteUtilities.loadquoteLinesMap(Quote.Id);
        if (quoteLines == null) {
            // Return error that the quote lines could not be loaded.
            system.debug('QuoteMigrator::doMigration: Error loading quote lines for quote Id ' + quote.Id + '. Migration failed.');success = false;addMessage('Migration failed');addMessage('Unable to load quote line records');rollitback();return success;
        } else if (quoteLines.size() == 0) {
            // Return error that no quote lines were loaded.
            system.debug('QuoteMigrator::doMigration: No quote lines found for quote Id ' + quote.Id + '. Migration failed.');success = false;addMessage('Migration failed');addMessage('No quote line records to load');rollitback();return success;
        }
        
        // Load the quote's additional information records
        /* Relationships to update: quote__c, quote_line__c */
        additionalInformations = QuoteUtilities.loadAdditionalInformationMap(Quote.Id);
        if (additionalInformations == null) {
            // Return error that the additional information records could not be loaded.
            system.debug('QuoteMigrator::doMigration: Error loading quote additional information records for quote Id ' + quote.Id + '. Migration failed.');success = false;addMessage('Migration failed');addMessage('Unable to load quote additional information records');rollitback();return success;
        } else if (additionalInformations.size() == 0) {
            // Return error that no quote lines were loaded.
            system.debug('QuoteMigrator::doMigration: No additional information records found for quote Id ' + quote.Id + '. Migration failed.');success = false;addMessage('Migration failed');addMessage('No additional informations records to load');rollitback();return success;
        } else {
            for (AdditionalInformation__c ai : additionalInformations.values()) {
                qlToAdditionalInformations.put(ai.Quote_Line__c, ai);
            }
        }
        
        // Load the quote's opportunity record
        /* if (!loadOpportunity()) {
            // Return error that quote's opportunity record could not be loaded
            system.debug('QuoteMigrator::doMigration: Error loading quote additional information records ' + QuoteId + ' not found. Migration failed.');
            success = false;
            return;
        } */
        /* End Migration Setup */
        
        // First, process the bundle line
        SBQQ__QuoteLine__c bundleLine = QuoteUtilities.findBundleLine(quoteLines);
        Id bundleLineProductId;
        if (bundleLine == null || bundleLine.Id == null) {
            // Return error that no bundle line could be found
            system.debug('QuoteMigrator::doMigration: No bundle line record found for quote Id ' + quote.Id + '. Migration failed.');success = false;addMessage('Migration failed');addMessage('No bundle quote line found');rollitback();return success;
        } else if (bundleLine.SBQQ__Product__c != migrationRoute.Source_Product__c) {
            // This is an invalid migration route
            system.debug('QuoteMigrator::doMigration: Invalid migration route ' + migrationRoute.Name + ' for quote Id ' + quote.Id + '. Migration failed.');system.debug('QuoteMigrator::doMigration: Migration route expected ' + bundleLine.SBQQ__Product__r.Name + ', got ' + migrationRoute.Source_Product__r.Name);success = false;addMessage('Migration failed');addMessage('Invalid migration type');rollitback();return success;
        } else {
            // Process the initial transformation - from one bundle type to another
            bundleLineProductId = bundleLine.SBQQ__Product__c;
            bundleLine.SBQQ__Product__c = migrationRoute.Destination_Product__c;
            Decimal newPrice = productToPricebookEntries.get( migrationRoute.Destination_Product__c ).UnitPrice;
            if (newPrice == null) {
                system.debug('QuoteMigrator::doMigration: No migrated bundle price for ' + migrationRoute.Destination_Product__r.Name + ' for quote Id ' + quote.Id + '. Migration failed.');success = false;addMessage('Migration failed');addMessage('Invalid migration type');rollitback();return success;
            }
            bundleLine.SBQQ__ListPrice__c = newPrice;
            bundleLine.SBQQ__CustomerPrice__c = newPrice;
            bundleLine.SBQQ__NetPrice__c = newPrice;
            bundleLine.SBQQ__SpecialPrice__c = newPrice;
            bundleLine.SBQQ__ProratedPrice__c = newPrice;
            bundleLine.SBQQ__ProratedListPrice__c = newPrice;
            
            // Added in QQ v12.8
            //bundleLine.SBQQ__ConfigurationRequired__c = true;
                        
            system.debug(bundleLine);
            quoteLines.put(bundleLine.Id, bundleLine);
        }
        
        // Process the product option remapping
        for (Id i : quoteLines.keySet()) {
            system.debug('QuoteMigrator::doMigration: migrating QL ' + i);
            SBQQ__QuoteLine__c ql = quoteLines.get(i);
            if (ql.SBQQ__ProductOption__r.SBQQ__ConfiguredSku__c == bundleLineProductId) {
                productToSourceProductOption.put( ql.SBQQ__Product__c, ql.SBQQ__ProductOption__r );
            }
        }
        SBQQ__ProductOption__c[] pos = [Select SBQQ__OptionalSku__c, SBQQ__ConfiguredSku__c, SBQQ__Bundled__c From SBQQ__ProductOption__c
                Where SBQQ__ConfiguredSku__c = :migrationRoute.Destination_Product__c and SBQQ__OptionalSku__c in :productToSourceProductOption.keySet()];
        for (SBQQ__ProductOption__c po : pos) {
            productToDestinationProductOption.put( po.SBQQ__OptionalSku__c, po );
        }
        
        // Next, process the remove actions:
        for (Id i : quoteLines.keySet()) {
            system.debug('QuoteMigrator::doMigration: migrating QL ' + i);
            SBQQ__QuoteLine__c ql = quoteLines.get(i);
            for (Quote_Migration_Action__c qma : migrationActions.values()) {
                if (qma.Action__c == 'Remove' && qma.Product__c == ql.SBQQ__Product__c) {
                    quoteLinesToDelete.put(i, ql);
                    AdditionalInformation__c ai = qlToAdditionalInformations.get(i);
                    if (ai != null) {
                        if (!qma.Save_Additional_Information__c) {
                            additionalInformationsToDelete.put(ai.Id, ai);
                        } else if (qma.Save_Additional_Information__c && qma.Save_Additional_Information_For__c != null) {
                            actionToSavedAdditionalInformation.put(qma.Save_Additional_Information_For__c, ai);
                        }
                    }
                    quoteLines.remove(i);
                    migrationActions.remove(qma.id);
                    continue;
                }
            }
            if (quoteLinesToDelete.get(ql.SBQQ__RequiredBy__c) != null) {
                // This quote line is required by a line that will not exist after migration. Remove it.
                quoteLinesToDelete.put(i, ql);
                quoteLines.remove(i);
            }
        }
        
        
        // Reprocess the remaining quote lines to get the last stragglers...
        system.debug('QuoteMigrator::doMigration: garbage collecting');
        for (Id i : quoteLines.keySet()) {
            system.debug('QuoteMigrator::doMigration: garbage collecting QL ' + i);
            SBQQ__QuoteLine__c ql = quoteLines.get(i);
            if (quoteLinesToDelete.get(ql.SBQQ__RequiredBy__c) != null) {
                // This quote line is required by a line that will not exist after migration. Remove it.
                system.debug('QuoteMigrator::doMigration: threw out QL ' + i + ' - parent line deleted');
                quoteLinesToDelete.put(i, ql);
                quoteLines.remove(i);
                continue;
            }
            if (ql.SBQQ__ProductOption__r.SBQQ__ConfiguredSku__c == bundleLineProductId) {
                SBQQ__ProductOption__c qpo = productToDestinationProductOption.get( ql.SBQQ__ProductOption__r.SBQQ__OptionalSku__c );
                if (qpo != null) {
                    ql.SBQQ__ProductOption__c = qpo.id;
                }
            }
            quoteLines.put( ql.Id, ql );
        }
        system.debug('QuoteMigrator::doMigration: garbage collected');
        
        // Finally, process the add actions:
        integer oi = 2;
        Map<Id, SBQQ__QuoteLine__c> productToQMAaddedQuoteLine = new Map<Id, SBQQ__QuoteLine__c>();
        for (Quote_Migration_Action__c qma : migrationActions.values()) {
            if (qma.Action__c == 'Add' && qma.Parent_Product__c == null) {
                SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
                SBQQ__ProductOption__c po = productToDestinationProductOption.get( qma.Product__c );
                if (po != null) {
                    ql.SBQQ__ProductOption__c = po.id;
                    ql.SBQQ__Bundled__c = po.SBQQ__Bundled__c;
                }
                ql.SBQQ__Quote__c = quote.id;
                
                if (bundleLine.SBQQ__Product__c == '01t40000001jH4mAAE' && qma.Product__c == '01tV0000000UsWhIAK') {
                    // It's a BA Plus Single Line, this is HSIA backup
                    ql.SBQQ__Bundled__c = true;
                } // TODO: Find a better solution than hard coded product numbers
                
                // Added in QQ v12.8
                ql.SBQQ__ConfigurationRequired__c = true;
                
                /* Trying to fix errors */
                ql.SBQQ__ProrateMultiplier__c = 1;
                ql.SBQQ__BundledQuantity__c = 1;
                
                ql.SBQQ__Number__c = oi;
                oi++;
                ql.SBQQ__RequiredBy__c = bundleLine.Id;
                ql.SBQQ__OptionLevel__c = 1;
                ql.SBQQ__SubscriptionScope__c = 'Quote';
                ql.SBQQ__Quantity__c = 1;
                ql.SBQQ__Product__c = qma.Product__c;
                Decimal newPrice = productToPricebookEntries.get( qma.Product__c ).UnitPrice;
                if (newPrice == null) {
                    system.debug('QuoteMigrator::doMigration: No migrated product price for ' + qma.Product__r.Name + ' for quote Id ' + quote.Id + '. Migration failed.');success = false;addMessage('Migration failed');addMessage('Missing product price');rollitback();return success;
                }
                ql.SBQQ__ListPrice__c = newPrice;
                ql.SBQQ__CustomerPrice__c = newPrice;
                ql.SBQQ__NetPrice__c = newPrice;
                ql.SBQQ__SpecialPrice__c = newPrice;
                ql.SBQQ__ProratedPrice__c = newPrice;
                ql.SBQQ__ProratedListPrice__c = newPrice;
                ql.SBQQ__UnitCost__c = newPrice;
                ql.SBQQ__RegularPrice__c = newPrice;
                
                system.debug('QuoteMigrator::doMigration: Adding migration product ' + qma.Product__r.Name + ' for quote Id ' + quote.Id + ':');
                system.debug(ql);
                actionToQMAaddedQuoteLine.put(qma.Id, ql);
                productToQMAaddedQuoteLine.put(qma.Product__c, ql);
                quoteLinesToAdd.add(ql);
            }
        }
        
        try {
            update quote;
            upsert quoteLines.values();
            insert quoteLinesToAdd;
            upsert additionalInformations.values();
            delete quoteLinesToDelete.values();
            delete additionalInformationsToDelete.values();
        } catch (exception e) {
            system.debug('QuoteMigrator::doMigration: Error updating all modified records.');success = false;addMessage('Migration failed');addMessage('Unable to update migrated records');addMessage(e.getMessage());rollitback();return success;
        }
        
        quoteLinesToAdd.clear();
        // Process the child lines
        for (Quote_Migration_Action__c qma : migrationActions.values()) {
            if (qma.Action__c == 'Add' && qma.Parent_Product__c != null && productToQMAaddedQuoteLine.get(qma.Parent_Product__c) != null) {
                SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c();
                SBQQ__ProductOption__c po = productToDestinationProductOption.get( qma.Product__c );
                if (po != null) {
                    ql.SBQQ__ProductOption__c = po.id;
                    ql.SBQQ__Bundled__c = po.SBQQ__Bundled__c;
                }
                ql.SBQQ__Quote__c = quote.id;
                
                /* Trying to fix errors */
                ql.SBQQ__ProrateMultiplier__c = 1;
                ql.SBQQ__BundledQuantity__c = 1;
                ql.SBQQ__Number__c = oi;
                oi++;
                
                ql.SBQQ__RequiredBy__c = productToQMAaddedQuoteLine.get(qma.Parent_Product__c).Id;
                ql.SBQQ__OptionLevel__c = 2;
                ql.SBQQ__SubscriptionScope__c = 'Quote';
                ql.SBQQ__Quantity__c = 1;
                ql.SBQQ__Product__c = qma.Product__c;
                Decimal newPrice = productToPricebookEntries.get( qma.Product__c ).UnitPrice;
                if (newPrice == null) {
                    system.debug('QuoteMigrator::doMigration: No migrated product price for ' + qma.Product__r.Name + ' for quote Id ' + quote.Id + '. Migration failed.');success = false;addMessage('Migration failed');addMessage('Missing product price');rollitback();return success;
                }
                ql.SBQQ__ListPrice__c = newPrice;
                ql.SBQQ__CustomerPrice__c = newPrice;
                ql.SBQQ__NetPrice__c = newPrice;
                ql.SBQQ__SpecialPrice__c = newPrice;
                ql.SBQQ__ProratedPrice__c = newPrice;
                ql.SBQQ__ProratedListPrice__c = newPrice;
                ql.SBQQ__UnitCost__c = newPrice;
                ql.SBQQ__RegularPrice__c = newPrice;
                
                system.debug('QuoteMigrator::doMigration: Adding migration product ' + qma.Product__r.Name + ' for parent ' + qma.Parent_Product__r.Name + ' for quote Id ' + quote.Id + ':');
                system.debug(ql);
                actionToQMAaddedQuoteLine.put(qma.Id, ql);
                quoteLinesToAdd.add(ql);
            }
        }
        try {
            insert quoteLinesToAdd;
        } catch (exception e) {
            system.debug('QuoteMigrator::doMigration: Error insert child QMA added records.');success = false;addMessage('Migration failed');addMessage('Unable to insert migrated child records');addMessage(e.getMessage());rollitback();return success;
        }
        
        // Now process remapping add'l information records that were saved
        AdditionalInformation__c[] aisToUpdate = new List<AdditionalInformation__c>();
        for (Id i : actionToQMAaddedQuoteLine.keySet()) {
            // i is the ID of the QMA
            AdditionalInformation__c ai = actionToSavedAdditionalInformation.get(i);
            SBQQ__QuoteLine__c ql = actionToQMAaddedQuoteLine.get(i);
            if (ai != null && ql != null) {
                ai.Quote_Line__c = ql.Id;
                aisToUpdate.add(ai);
            }
        }
        if (aisToUpdate != null && aisToUpdate.size() != 0) {
            try {
                update aisToUpdate;
            } catch (exception e) {
                system.debug('QuoteMigrator::doMigration: Error insert rehomed additional information records.');success = false;addMessage('Migration failed');addMessage('Unable to insert migrated additional information records');addMessage(e.getMessage());rollitback();return success;
            }
        }
        
        // Do this while debugging so that you can review the debug log without 
        //rollitback();
        
        success = true;
        parseMessages();
        return success;
    }
    
    // Method to add a page message to the queue. Order is maintained.
    private void addMessage(String message) {
        pageMessages.add(message);
        parseMessages();
    }
    // Method to turn all queued page messages into ApexPage messages.
    public void parseMessages() {
        parseMessages(true);
    }
    public void parseMessages(Boolean addToPage) {
        for (Integer i = lastMessageProcessed; i < pageMessages.size(); i++) {
            if (addToPage) { ApexPages.addMessage(new ApexPages.Message( ApexPages.Severity.INFO , pageMessages.get(i) )); }
            if (message != '') { message = message + '|'; }
            message = message + pageMessages.get(i);
            lastMessageProcessed = i;
        }
    } 
    
    // Method to set database savepoint. No boolean defaults to no-overwrite.
    private boolean setSavepoint() { return setSavepoint(false);}
    private boolean setSavepoint(Boolean overwrite) {
        if (rollbackSp == null || overwrite == true) {
            try { rollbackSp = Database.setSavepoint(); }
            catch (exception e) {
                // Error getting savepoint
                system.debug('setSavepoint: setting savepoint failed. exception message: ' + e.getMessage());addMessage('Error setting database savepoint');return false;
            }
        } else if (rollbackSp != null) { system.debug('setSavepoint: savepoint already set.'); }
        if (rollbackSp == null) { return false; }
        
        system.debug('setSavepoint: database savepoint set.');
        if (debug) addMessage('Database savepoint set');
        return true;
    }
    
    // Method to rollback from savepoint
    private boolean rollitback() {
        if (rollbackSp == null) {
            system.debug('rollitback: no savepoint set');
        }
        try {
            database.rollback(rollbackSp);
        } catch (exception e) {
            system.debug('rollitback: exception thrown while rolling back database. message: ' + e.getMessage());addMessage('Database rollback failed');addMessage(e.getMessage());return false;
        }
        system.debug('QuoteMigrator::rollitback: rollback succeeded.');
        addMessage('Database rollback succeeded');
        return true;
    }
    
}