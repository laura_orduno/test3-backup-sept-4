public with sharing class ContractHistoryComponentCntrl {  
    //Properties
    public String objName {get;set;}
    public Id ObjectId {get;set;}
    public integer PageSize {get;set;}
    public boolean AllowShowMore {get;set;}
    public List<sObject> listEntityHistory {get;set;}

    public List<ObjectHistory> ObjectHistories {
        get { return getObjectHistory(ObjectId); }
    }

    //Constructors

    /**
     * Default Constructor
     */
    public ContractHistoryComponentCntrl () {
        PageSize = 5;   
        AllowShowMore = true;
    }

    //Public Methods
    public void showMore() {
        PageSize += 5;
    }

    //Private Methods

    /**
     * Returns Object History records associated to the current Object
     *
     * @param   ObjectId     the Object__c record id to retrieve
     * @return  a list of ObjectHistory objects
     */
    private List<ObjectHistory> getObjectHistory(Id ObjectId) {
        String objHistory = objName + 'History';
        system.debug('@@objHistory '+objHistory );
        DescribeSObjectResult oObjectSchema = Util.getObjectTypeDescribe(objName);
       Map<string, Schema.SObjectField> mapFields = oObjectSchema.fields.getMap();
        List<ObjectHistory> listObjectHistory = new List<ObjectHistory>();
        if (ObjectId != null) {
            DateTime dLastCreatedDate = null;
            integer limitPlusOne = PageSize + 1;
            List<String> fields = new List<String> {'Id', 'Field', 'NewValue', 'OldValue', 'CreatedDate', 'CreatedById', 'CreatedBy.Name'};
            List<String> filter = new List<String> {'ContractId=\''+ ObjectId + '\''};
            List<String> orderBy = new List<String> {'CreatedDate DESC', 'Id DESC'};
            String query  =  Util.queryBuilder(objHistory, fields, filter, orderBy); 
            query += ' LIMIT ' + limitPlusOne;
            System.debug(LoggingLevel.ERROR,query);
            if(!Test.isRunningTest()){
              listEntityHistory =  Database.query(query);    
            }else{
               listEntityHistory = new List<Sobject>(); 
               listEntityHistory.add(new AccountHistory(Field='Type'));
            }
            System.debug(LoggingLevel.ERROR, 'query' + query);
            System.debug(LoggingLevel.ERROR, 'objHistory' + listEntityHistory);
           // List<sObject> listEntityHistory = [SELECT Id, Field, NewValue, OldValue, CreatedDate, CreatedById, CreatedBy.Name FROM Object__History WHERE ParentId = :ObjectId ORDER BY CreatedDate DESC, Id DESC LIMIT :limitPlusOne];
            AllowShowMore = (listEntityHistory.size() == limitPlusOne);

            for (sObject oHistory : listEntityHistory) {
                ObjectHistory oObjectHistory = new ObjectHistory(oHistory);

                if (mapFields.containsKey((String)oHistory.get('Field'))) {
                    oObjectHistory.FieldLabel = mapFields.get((string)oHistory.get('Field')).getDescribe().Label;
                }

                if (dLastCreatedDate == (DateTime)oHistory.get('CreatedDate')) {
                    oObjectHistory.ShowDateAndUser = false;
                }else {
                    oObjectHistory.ShowDateAndUser = true;
                }

                listObjectHistory.add(oObjectHistory);
                dLastCreatedDate =  (DateTime)oHistory.get('CreatedDate');

                if (listObjectHistory.size() == PageSize) break;
            }
        }

        return listObjectHistory;
    }

    //Internal Classes

    /**
     * Data structure representing a Object History record for display
     */
    public class ObjectHistory {
        //Properties
        public boolean ShowDateAndUser {get;set;}
        public string FieldLabel {get;set;}
        public sObject History {get; private set;}

        public string ActionFormat {
            get { return getActionFormat(); }
        }

        public ObjectHistory(sObject oHistory) {
            History = oHistory;
        }

        //Private Methods
        private string getActionFormat() {
            string sActionFormat = '';

            if (History != null) {
                sActionFormat = 'Record {0}.';
          if (History.get('newValue') != null && History.get('oldValue') == null) {
                    sActionFormat = 'Changed <strong>{1}</strong> to <strong>{3}</strong>.';    
                }
                else if (History.get('newValue') != null && History.get('oldValue')!= null) {
                    sActionFormat = 'Changed <strong>{1}</strong> from {2} to <strong>{3}</strong>.';   
                }
                else if (History.get('Field') != null && ((String)History.get('Field')).equalsIgnoreCase('created')) {
                    sActionFormat = 'Created.';
                }else if(History.get('newValue') == null && History.get('oldValue')!= null){
                    sActionFormat = 'Deleted {2} in <strong>{1}</strong>.';  
                }
            }
      System.debug(LoggingLevel.ERROR,'HS '+ sActionFormat);
            return sActionFormat;
        }
    }
}