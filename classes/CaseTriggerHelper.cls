public class CaseTriggerHelper {

	private static Set<Id> statusCaseIds = new Set<Id>();
	private static Set<Id> ownerCaseIds = new Set<Id>();
	
	public static Boolean hasStatusCaseId(Id newId) {
		if (statusCaseIds.contains(newId))
			return true;
		else
			return false;
	}
	
	public static void addStatusCaseIds(Id newId) {
		if (!statusCaseIds.contains(newId))
			statusCaseIds.add(newId);
	}
	
	public static Boolean hasOwnerCaseId(Id newId) {
		if (ownerCaseIds.contains(newId))
			return true;
		else
			return false;
	}
	
	public static void addOwnerCaseIds(Id newId) {
		if (!ownerCaseIds.contains(newId))
			ownerCaseIds.add(newId);
	}
}