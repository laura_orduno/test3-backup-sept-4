/*
###########################################################################
# File..................: smb_ContactCallOuts
# Version...............: 26
# Created by............: Vinay Sharma
# Created Date..........: 25-Apr-2013
# Last Modified by......: 
# Last Modified Date....: 
# Description...........: This class contains methods to call contact management web service oprations
#
# Copyright (c) 2000-2012. Astadia, Inc. All Rights Reserved.
#
# Created by the Astadia, Inc. Modification must retain the above copyright notice.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any commercial purpose, without fee, and without a written
# agreement from Astadia, Inc., is hereby forbidden. Any modification to source
# code, must include this paragraph and copyright.
#
# Permission is not granted to anyone to use this software for commercial uses.
#
# Contact address: 2839 Paces Ferry Road, Suite 350, Atlanta, GA 30339
# Company URL : http://www.astadia.com
################################################################################

# Last Modified by......: Nitin Saxena
# Last Modified Date....: 14-Jun-2016
# Description...........: changed smb_ContractManagementSvcTypes_v3 to cms3_1ContractManagementSvcTypes_v3.1.

  The essence of the change was to accommodate a parameter type change (long to string) for 
  the findContractAmendmentsByAssociateNum() operation on the CMS web service endpoint.  A new operation was introduced on CMS V3, 
  findContractAmendmentsByAssociateNumber(), in order to accommodate this version change. 
  Due to SOA Governance policies, the web service endpoint was not to be versioned.  But instead, adding two new operations that 
  referenced an updated XSD version of 3.1 was implemented by the BTO CMS Development team.  As a result, we are still 
  naming this service within SFDC as 3.1 to show that there was a change to the service endpoint contract. 
  Technically, adding operations should result in a new end point version.  But, BTO decided against this in order to meet
  the TQ project timelines.  This is why you may see the endpoint URL pointing to version 3.0 of the web service, 
    even though it is actually version 3.1 due to the addition of two new operations
#########################################################################################
*/

public  class smb_ContactCallOuts {
    
   
    //list of contract data to be return
    cms3_1ContractManagementSvcTypes_v3.ContractList listOfContracts = new cms3_1ContractManagementSvcTypes_v3.ContractList();
    //cmsv3_ContractManagementSvcTypes_v3.ContractList listOfContracts = new cmsv3_ContractManagementSvcTypes_v3.ContractList();
    

    
    /* This method gets a customer ID value as input and return related contracts*/
    public  cms3_1ContractManagementSvcTypes_v3.ContractList getContractsByCustomerId(string customerID)
    //public  cmsv3_ContractManagementSvcTypes_v3.ContractList getContractsByCustomerId(string customerID)
    {

        try
        {
                if(!test.isRunningTest())
                {
                    //Web service primary class object
            cms3_1ContractManagementSvc_3.ContractManagementSvcPort servicePort = new cms3_1ContractManagementSvc_3.ContractManagementSvcPort();
                    //cmsv3_ContractManagementSvc_3.ContractManagementSvc_v3_0_SOAP servicePort = new cmsv3_ContractManagementSvc_3.ContractManagementSvc_v3_0_SOAP();
                    serviceport.timeout_x = 60000;
                    servicePort = prepareCallout(servicePort);
                    listOfContracts = servicePort.findContractsByCustomerId(customerID);
                }
                else
                {
                    list<string> btn = new list<string>();
                    btn.add('1234567890');
            cms3_1ContractManagementSvcTypes_v3.BillingTelephoneNumList btList = new cms3_1ContractManagementSvcTypes_v3.BillingTelephoneNumList();
                    btList.billingTelephoneNum = btn;
                    cms3_1ContractManagementSvcTypes_v3.ContractDetail ctDetail = new cms3_1ContractManagementSvcTypes_v3.ContractDetail();
                    ctDetail.billingTelephoneNumList = btList;
                    list<cms3_1ContractManagementSvcTypes_v3.ContractDetail> lstctDetail = new list<cms3_1ContractManagementSvcTypes_v3.ContractDetail>();
                    lstctDetail.add(ctDetail);
                    cms3_1ContractManagementSvcTypes_v3.ContractDetailList ctDetailList = new cms3_1ContractManagementSvcTypes_v3.ContractDetailList();
                    ctDetailList.contractDetail = lstctDetail;
                    cms3_1ContractManagementSvcTypes_v3.Contract ct = new cms3_1ContractManagementSvcTypes_v3.Contract();

            /*
                    cmsv3_ContractManagementSvcTypes_v3.BillingTelephoneNumList btList = new cmsv3_ContractManagementSvcTypes_v3.BillingTelephoneNumList();
                    btList.billingTelephoneNum = btn;
                    cmsv3_ContractManagementSvcTypes_v3.ContractDetail ctDetail = new cmsv3_ContractManagementSvcTypes_v3.ContractDetail();
                    ctDetail.billingTelephoneNumList = btList;
                    list<cmsv3_ContractManagementSvcTypes_v3.ContractDetail> lstctDetail = new list<cmsv3_ContractManagementSvcTypes_v3.ContractDetail>();
                    lstctDetail.add(ctDetail);
                    cmsv3_ContractManagementSvcTypes_v3.ContractDetailList ctDetailList = new cmsv3_ContractManagementSvcTypes_v3.ContractDetailList();
                    ctDetailList.contractDetail = lstctDetail;
                    cmsv3_ContractManagementSvcTypes_v3.Contract ct = new cmsv3_ContractManagementSvcTypes_v3.Contract();
            */
                    ct.contractNum = 'G9390';
                    ct.contractAssociateNum = '2';
            ct.effectiveDate = string.valueof(Date.today());
                    //ct.effectiveDate = Date.today();
                    ct.customerLegalName = 'TelusTesting';
                    
                    
                    
                    ct.contractDetailList = ctDetailList;
                          
                    list<cms3_1ContractManagementSvcTypes_v3.Contract> ctList = new list<cms3_1ContractManagementSvcTypes_v3.Contract>();
                    ctList.add(ct);
                    cms3_1ContractManagementSvcTypes_v3.ContractList contractList = new cms3_1ContractManagementSvcTypes_v3.ContractList();
                    contractList.contract =ctList; 
                    listOfContracts = contractList;
                    system.debug('----contractList========'+contractList);
                
                }           
        }
        catch(CalloutException e) 
        {
            System.debug(e.getMessage());
            throw e;
        }
        
        return listOfContracts;
    }
    
   
    
    /* This method gets contract number and Associate number values as input and return related amendment sets*/
    //public  list<cmsv3_ContractManagementSvcTypes_v3.ContractAmendmentList> getContractAmendmentsByAssociateNum(String contractNum,Integer contractAssociateNum)
    public  list<cms3_1ContractManagementSvcTypes_v3.ContractAmendmentList> getContractAmendmentsByAssociateNum(String contractNum,Integer contractAssociateNum)
    {
        //list of amendments to return
        //list<cmsv3_ContractManagementSvcTypes_v3.ContractAmendmentList> lstAmendments = new list<cmsv3_ContractManagementSvcTypes_v3.ContractAmendmentList>();
        list<cms3_1ContractManagementSvcTypes_v3.ContractAmendmentList> lstAmendments = new list<cms3_1ContractManagementSvcTypes_v3.ContractAmendmentList>();
        try
        {
            if(!test.isRunningTest())
            {
                //Web service primary class object
        cms3_1ContractManagementSvc_3.ContractManagementSvcPort servicePort = new cms3_1ContractManagementSvc_3.ContractManagementSvcPort();
                //cmsv3_ContractManagementSvc_3.ContractManagementSvc_v3_0_SOAP servicePort = new cmsv3_ContractManagementSvc_3.ContractManagementSvc_v3_0_SOAP();
                servicePort = prepareCallout(servicePort);
               // lstAmendments = servicePort.findContractAmendmentsByAssociateNum(contractNum,contractAssociateNum);
           //Added By Nitin for Cms3_1
                  lstAmendments = servicePort.findContractAmendmentsByAssociateNumber(contractNum,contractAssociateNum);
            }
            else
            {
                
                cms3_1ContractManagementSvcTypes_v3.ServiceDetail sDetail = new cms3_1ContractManagementSvcTypes_v3.ServiceDetail();
                sDetail.serviceName = 'TestService TQ CMS name';
                list<cms3_1ContractManagementSvcTypes_v3.ServiceDetail> lstSerDetail = new list<cms3_1ContractManagementSvcTypes_v3.ServiceDetail>();
                lstSerDetail.add(sDetail);
        cms3_1ContractManagementSvcTypes_v3.ServiceDetailList sDetList = new cms3_1ContractManagementSvcTypes_v3.ServiceDetailList();
                //cmsv3_ContractManagementSvcTypes_v3.ServiceDetailList sDetList = new cmsv3_ContractManagementSvcTypes_v3.ServiceDetailList();
                sDetList.serviceDetail = lstSerDetail;
                cms3_1ContractManagementSvcTypes_v3.ServiceAddress serAddress = new cms3_1ContractManagementSvcTypes_v3.ServiceAddress();
                //cmsv3_ContractManagementSvcTypes_v3.ServiceAddress serAddress = new cmsv3_ContractManagementSvcTypes_v3.ServiceAddress();
                serAddress.houseNum = '507';
                serAddress.streetName = 'TestStreet';
                serAddress.city = 'TestCity';
                serAddress.provStateCd = 'BC';
                serAddress.postalCd = '12345';
                serAddress.countryCd = 'Can';
                serAddress.countryName = 'Canada';
                serAddress.serviceDetailList = sDetList;
                list<cms3_1ContractManagementSvcTypes_v3.ServiceAddress> lstSerAddress = new list<cms3_1ContractManagementSvcTypes_v3.ServiceAddress>();
                lstSerAddress.add(serAddress);
                cms3_1ContractManagementSvcTypes_v3.serviceAddressList serAddrLst = new  cms3_1ContractManagementSvcTypes_v3.serviceAddressList();
                serAddrLst.serviceAddress = lstSerAddress;
                
          cms3_1ContractManagementSvcTypes_v3.ContractAmendment amendment = new cms3_1ContractManagementSvcTypes_v3.ContractAmendment ();
               //cmsv3_ContractManagementSvcTypes_v3.ContractAmendment amendment = new cmsv3_ContractManagementSvcTypes_v3.ContractAmendment ();
                amendment.serviceAddressList =serAddrLst ;
                
        list<cms3_1ContractManagementSvcTypes_v3.ContractAmendment> lstCAmendMent  = new list<cms3_1ContractManagementSvcTypes_v3.ContractAmendment>();
                //list<cmsv3_ContractManagementSvcTypes_v3.ContractAmendment> lstCAmendMent  = new list<cmsv3_ContractManagementSvcTypes_v3.ContractAmendment>();
                lstCAmendMent.add(amendment);           
                
          cms3_1ContractManagementSvcTypes_v3.ContractAmendmentList singlelstAmendment = new cms3_1ContractManagementSvcTypes_v3.ContractAmendmentList ();
                //cmsv3_ContractManagementSvcTypes_v3.ContractAmendmentList singlelstAmendment = new cmsv3_ContractManagementSvcTypes_v3.ContractAmendmentList ();
                singlelstAmendment.contractAmendment = lstCAmendMent;
                
        list<cms3_1ContractManagementSvcTypes_v3.ContractAmendmentList> lstAmendmentList = new list<cms3_1ContractManagementSvcTypes_v3.ContractAmendmentList>();                
                //list<cmsv3_ContractManagementSvcTypes_v3.ContractAmendmentList> lstAmendmentList = new list<cmsv3_ContractManagementSvcTypes_v3.ContractAmendmentList>();
                lstAmendmentList.add(singlelstAmendment);
                
                //final return
                lstAmendments = lstAmendmentList;
                
            }
                        
        }
        catch(CalloutException e) 
        {
            System.debug(e.getMessage());       
        }
        
        return lstAmendments;
    }
    
    /*Method to set Credentials in header of the request*/
    public cms3_1ContractManagementSvc_3.ContractManagementSvcPort prepareCallout(cms3_1ContractManagementSvc_3.ContractManagementSvcPort serport)
    //public cmsv3_ContractManagementSvc_3.ContractManagementSvc_v3_0_SOAP prepareCallout(cmsv3_ContractManagementSvc_3.ContractManagementSvc_v3_0_SOAP serport)
    {
        String creds;
        SMBCare_WebServices__c wsUsername = SMBCare_WebServices__c.getValues('username');
        SMBCare_WebServices__c wsPassword = SMBCare_WebServices__c.getValues('password');
        if (String.isNotBlank(WSusername.Value__c)&& String.isNotBlank(wsPassword.Value__c))
            creds = WSusername.Value__c+':'+wsPassword.Value__c;
        String encodedusernameandpassword = EncodingUtil.base64Encode(Blob.valueOf(creds));
        
        serPort.inputHttpHeaders_x = new Map<String, String>();
        serPort.inputHttpHeaders_x.put('Authorization','Basic ' + encodedusernameandpassword );
        return serPort; 
    }
    
   
}