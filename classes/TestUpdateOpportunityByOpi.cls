@isTest
private class TestUpdateOpportunityByOpi{
	static testmethod void testCodeCoverage1(){
		 Opp_Product_Item__c opiObj = createTestData();
		 Test.startTest();        
         Opp_Product_Item__c opiObjUpdate = [select Existing_Monthly_Recurring_Revenue__c, Quantity__c, All_Non_Recurring_Revenue__c, Monthly_Recurring_Revenue__c, Contract_Length_Month__c, Billing_Frequency_Month__c from Opp_Product_Item__c where Id = :opiObj.Id];
         opiObjUpdate.Existing_Monthly_Recurring_Revenue__c = 14;
         update opiObjUpdate ;
         Test.stopTest();
	}
	static testmethod void testCodeCoverage2(){
		 Opp_Product_Item__c opiObj = createTestData();
		 Test.startTest(); 
		 Opp_Product_Item__c opiObjUpdate = [select Existing_Monthly_Recurring_Revenue__c, Quantity__c, All_Non_Recurring_Revenue__c, Monthly_Recurring_Revenue__c, Contract_Length_Month__c, Billing_Frequency_Month__c from Opp_Product_Item__c where Id = :opiObj.Id];
         opiObjUpdate.Quantity__c = 14;
         update opiObjUpdate ;
         Test.stopTest(); 
	}
	static testmethod void testCodeCoverage3(){
		Opp_Product_Item__c opiObj = createTestData();
		Test.startTest(); 
		Opp_Product_Item__c opiObjUpdate = [select Existing_Monthly_Recurring_Revenue__c, Quantity__c, All_Non_Recurring_Revenue__c, Monthly_Recurring_Revenue__c, Contract_Length_Month__c, Billing_Frequency_Month__c from Opp_Product_Item__c where Id = :opiObj.Id];
	    opiObjUpdate.All_Non_Recurring_Revenue__c = 14;
	    update opiObjUpdate ;
	    Test.stopTest();   
	}
	static testmethod void testCodeCoverage4(){		        
         Opp_Product_Item__c opiObj = createTestData();
         Test.startTest(); 
         Opp_Product_Item__c opiObjUpdate = [select Existing_Monthly_Recurring_Revenue__c, Quantity__c, All_Non_Recurring_Revenue__c, Monthly_Recurring_Revenue__c, Contract_Length_Month__c, Billing_Frequency_Month__c from Opp_Product_Item__c where Id = :opiObj.Id];
         opiObjUpdate.Monthly_Recurring_Revenue__c = 14;
         update opiObjUpdate ;
         Test.stopTest();
	}
     static Opp_Product_Item__c createTestData(){
          // created Account -Start
             Account acc = new Account(
                 Name= 'Test123',
                 BillingState = 'AB',
                 Phone = '604-120-1201'
             );
             insert acc;
             System.debug('Account='+acc);             
         //created Account -End
         //Created a User - Start
             //Selected a profile Partner User             
             Profile pPowerPartner =[select id from Profile limit 1];             
             System.debug('pPowerPartner ='+pPowerPartner );         
             User u = new User(
                 FirstName = 'FTestName',
                 LastName = 'LTestName',
                 Alias = 'nmbj',
                 Email = 'ghkl@abc.com',
      phone = '6049969449',
                 Username = 'hjkltner@abc.com',
                 CommunityNickname = 'ouio',
                 emailencodingkey = 'UTF-8',
                 languagelocalekey = 'en_US',
                 localesidkey = 'en_US',                 
                 timezonesidkey = 'America/Los_Angeles',             
                 profileId = pPowerPartner.Id
             );
             insert u;             
         //Created a User - End
         //Create Opportunity-Start             
             Datetime dateTimetemp = System.now() + 5;
             Date dateTemp = Date.newInstance(dateTimetemp.year(),dateTimetemp.month(),dateTimetemp.day());
             Opportunity opp = new Opportunity (
                 accountId = acc.Id,
                 Name = 'Testing Opportunity for Owner', 
                 CloseDate = dateTemp,                  
                 OwnerId = u.id, 
                 StageName = 'I have a Potential Opportunity (20%)'
             );        
             insert opp;             
         //Create Opportunity-End
         //Create Product -Start
             Product2 prodObj = new Product2(
                 Name = 'Test -Product',
                 IsActive = true,
                 Product_Family_Code__c = 'TEST11'
             );
             insert prodObj; 
         //Create Product -End
         Opp_Product_Item__c opiObj = new Opp_Product_Item__c(
             Product__c =  prodObj.Id,
             Opportunity__c = opp.Id,
             Existing_Monthly_Recurring_Revenue__c = 12,
             Quantity__c = 12,
             All_Non_Recurring_Revenue__c = 12,
             Monthly_Recurring_Revenue__c = 12,
             Billing_Frequency_Month__c = 12            
         );
         insert opiObj;
         
         return opiObj;
         
         /*
         Opp_Product_Item__c opiObjUpdate = [select Existing_Monthly_Recurring_Revenue__c, Quantity__c, All_Non_Recurring_Revenue__c, Monthly_Recurring_Revenue__c, Contract_Length_Month__c, Billing_Frequency_Month__c from Opp_Product_Item__c where Id = :opiObj.Id];
         opiObjUpdate.Existing_Monthly_Recurring_Revenue__c = 12;
         opiObjUpdate.Quantity__c = 12;
         opiObjUpdate.All_Non_Recurring_Revenue__c = 12;
         opiObjUpdate.Monthly_Recurring_Revenue__c = 12;
         opiObjUpdate.Billing_Frequency_Month__c = 12;
         update opiObjUpdate ;
         
         opiObjUpdate = [select Existing_Monthly_Recurring_Revenue__c, Quantity__c, All_Non_Recurring_Revenue__c, Monthly_Recurring_Revenue__c, Contract_Length_Month__c, Billing_Frequency_Month__c from Opp_Product_Item__c where Id = :opiObj.Id];
         opiObjUpdate.All_Non_Recurring_Revenue__c = 12;
         update opiObjUpdate ;
         opiObjUpdate = [select Existing_Monthly_Recurring_Revenue__c, Quantity__c, All_Non_Recurring_Revenue__c, Monthly_Recurring_Revenue__c, Contract_Length_Month__c, Billing_Frequency_Month__c from Opp_Product_Item__c where Id = :opiObj.Id];
         opiObjUpdate.Monthly_Recurring_Revenue__c = 12;
         update opiObjUpdate ;
         opiObjUpdate = [select Existing_Monthly_Recurring_Revenue__c, Quantity__c, All_Non_Recurring_Revenue__c, Monthly_Recurring_Revenue__c, Contract_Length_Month__c, Billing_Frequency_Month__c from Opp_Product_Item__c where Id = :opiObj.Id];
         opiObjUpdate.Billing_Frequency_Month__c = 12;
         update opiObjUpdate ;
         */
         
         
         /*
         opiObjUpdate = [select Existing_Monthly_Recurring_Revenue__c, Quantity__c, All_Non_Recurring_Revenue__c, Monthly_Recurring_Revenue__c, Contract_Length_Month__c, Billing_Frequency_Month__c from Opp_Product_Item__c where Id = :opiObj.Id];
         opiObjUpdate.Billing_Frequency_Month__c = 14;
         update opiObjUpdate ;
         */
     }
}