@isTest
public class CreditProfileCalloutUpdateTest {
    
    
    
    @isTest
    public static void associateCustomer() {     
        Test.setMock(WebServiceMock.class, new CreditProfileCalloutUpdateMock());
        Test.startTest();  
        
        Account acc = CreditUnitTestHelper.getAccount();  
        System.debug('... dyy ... acc.CustProfId__c = ' + acc.CustProfId__c);
        
        Credit_Profile__c cp = CreditUnitTestHelper.getCreditProfile();  
        
        String businessCustomerId = cp.Account__r.CustProfId__c;
        String individualCustomerId = cp.Consumer_profile_ID__c;
        
        System.debug('... dyy ... associateCustomer ... ');
        System.debug('... dyy ... cp.id = ' + cp.id);
        System.debug('... dyy ... businessCustomerId = ' + businessCustomerId);
        System.debug('... dyy ... individualCustomerId = ' + individualCustomerId);
        /**/
        
        CreditProfileCalloutUpdate.associateCustomer(cp.id);         
         
        Test.StopTest();    
    }

    
    
    @isTest
    public static void ping()
    {
        Test.setMock(WebServiceMock.class, new CreditProfileCalloutUpdateMock());
        Test.startTest();
        CreditProfileCalloutUpdate.ping();        
        Test.StopTest();    
    }
    
    @isTest
    public static void createBusinessCreditProfile() {     
        Test.setMock(WebServiceMock.class, new CreditProfileCalloutUpdateMock());
        Test.startTest();   
        Long businessCustomerId = 123456L;
        ID cpid = CreditUnitTestHelper.getCreditProfile().id;
        CreditProfileCalloutUpdate.createBusinessCreditProfile(businessCustomerId, cpid);            
        Test.StopTest();    
    }
    
    
    @isTest
    public static void updateCreditProfile() {     
        Test.setMock(WebServiceMock.class, new CreditProfileCalloutUpdateMock());
        Test.startTest();  
        Long businessCustomerId = 123456L;
        Credit_Profile__c creditProfil = CreditUnitTestHelper.getCreditProfile();        
        CreditProfileCalloutUpdate.updateCreditProfile(creditProfil.id);          
        CreditProfileCalloutUpdate.updateBusinessCreditProfile(businessCustomerId, creditProfil);    
        
        creditProfil.Legal_Entity_Type__c='Sole Proprietor (PR)';
        CreditProfileCalloutUpdate.updateCreditProfile(creditProfil.id);          
        CreditProfileCalloutUpdate.updateBusinessCreditProfile(businessCustomerId, creditProfil); 
        Test.StopTest();    
    }

    @isTest
    public static void createCreditProfileForNewProspectAccount() {     
        Test.setMock(WebServiceMock.class, new CreditProfileCalloutUpdateMock());
        Test.startTest();   
        CreditProfileCalloutUpdate.createCreditProfileForNewProspectAccount(CreditUnitTestHelper.getAccount());        
        Test.StopTest();    
    }

    @isTest
    public static void triggerCalloutProxyToAddPartner() {     
        Test.setMock(WebServiceMock.class, new CreditProfileCalloutUpdateMock());
        Test.startTest();   
        CreditProfileCalloutUpdate.triggerCalloutProxyToAddPartner(
            null
            , CreditUnitTestHelper.getCreditProfile().id
            , CreditUnitTestHelper.getCreditProfile().id);      
        CreditProfileCalloutUpdate.triggerCalloutProxyToAddPartner(
            CreditUnitTestHelper.getCreditProfile().id
            , null
            , CreditUnitTestHelper.getCreditProfile().id);     
        Test.StopTest();    
    }

    @isTest
    public static void triggerCalloutProxyToDeletePartner() {     
        Test.setMock(WebServiceMock.class, new CreditProfileCalloutUpdateMock());
        Test.startTest();   
        CreditProfileCalloutUpdate.triggerCalloutProxyToDeletePartner(
            null
            , CreditUnitTestHelper.getCreditProfile().id
            , CreditUnitTestHelper.getCreditProfile().id);      
        CreditProfileCalloutUpdate.triggerCalloutProxyToDeletePartner(
            CreditUnitTestHelper.getCreditProfile().id
            , null
            , CreditUnitTestHelper.getCreditProfile().id);     
        Test.StopTest();    
    }
    
    
    @isTest
    public static void updateIndivdualCreditProfile() {     
        Test.setMock(WebServiceMock.class, new CreditProfileCalloutUpdateMock());
        Test.startTest();  
        Long individualCustomerId = 123456L;
        Credit_Profile__c creditProfil = CreditUnitTestHelper.getCreditProfile();                 
        CreditProfileCalloutUpdate.updateIndivdualCreditProfile(individualCustomerId, creditProfil);          
        Test.StopTest();    
    }


    @isTest
    public static void attachCreditReport() {   
        Test.setMock(WebServiceMock.class, new CreditProfileCalloutUpdateMock());  
        Test.startTest();   
        
        Long businessCustomerId = 123456L;
        Document document = CreditUnitTestHelper.getDocument();
        String documentTypeCd = '123';
        String documentSourceCd = '123';
        
        System.debug('... dyy ... document = ' + document);
        
        CreditProfileCalloutUpdate.attachCreditReport(
            businessCustomerId
            ,document
            ,documentTypeCd
            ,documentSourceCd);        
        Test.StopTest();    
    }
    

    @isTest
    public static void getCreditReport() {     
        Test.setMock(WebServiceMock.class, new CreditProfileCalloutUpdateMock());
        Test.startTest();   
        CreditProfileCalloutUpdate.getCreditReport(12345L);        
        Test.StopTest();    
    }
    

    @isTest
    public static void voidCreditReport() {     
        Test.setMock(WebServiceMock.class, new CreditProfileCalloutUpdateMock());
        Test.startTest();   
        CreditProfileCalloutUpdate.voidCreditReport('12345');        
        Test.StopTest();    
    }
    

    @isTest
    public static void CreditProfileUpdateProxy() {     
        Test.setMock(WebServiceMock.class, new CreditProfileCalloutUpdateMock());
        Test.startTest();   
        ID[] ids = new List<ID>();
        Credit_Profile__c cp = CreditUnitTestHelper.getCreditProfile();     
        ids.add(cp.id);
        CreditProfileUpdateProxy.invocablemethod(ids);        
        Test.StopTest();    
    }
    
    
        
}