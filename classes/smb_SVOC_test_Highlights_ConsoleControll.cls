/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=true)
private class smb_SVOC_test_Highlights_ConsoleControll {

    static testMethod void myUnitTestCase() {
        
       PageReference ref = new PageReference('/a00/e');         
       RecordType recordType = [Select r.Id, r.DeveloperName From RecordType r where r.SobjectType = 'Account' and r.DeveloperName='RCID' limit 1];
            
       Account acc1 = new Account(Name='Testing Software', BillingCountry = 'US', BillingState = 'IL', recordTypeId=recordType.Id , No_Chronic_Incidents__c =10.0);
       insert acc1;
       
       Case theCase = new Case();
       theCase.Account__c = acc1.id;
       theCase.Subject = 'TEST SUBJECT';
       theCase.Description = 'TEST';
       theCase.Status = 'New';
       theCase.Request_Type__c = 'Other';
       theCase.Priority = 'Medium';
       theCase.Root_Cause__c = 'Unknown';
       //theCase.IsClosed = false;
       insert theCase;  
       
       ApexPages.currentPage().getParameters().put('Id', theCase.id); 
 
       smb_SVOC_Highlights_ConsoleController smb = new smb_SVOC_Highlights_ConsoleController();
    }
    
    
    static testMethod void myUnitTestContact() {
        
       PageReference ref = new PageReference('/a00/e');         
       RecordType recordType = [Select r.Id, r.DeveloperName From RecordType r where r.SobjectType = 'Account' and r.DeveloperName='RCID' limit 1];
            
       Account acc1 = new Account(Name='Testing Software', BillingCountry = 'US', BillingState = 'IL', recordTypeId=recordType.Id , No_Chronic_Incidents__c =10.0);
       insert acc1;
       
       Contact cont = new Contact(Lastname= 'lastname', Account = acc1);
       insert cont;
            
       ApexPages.currentPage().getParameters().put('Id', cont.id);   
            
       smb_SVOC_Highlights_ConsoleController smb = new smb_SVOC_Highlights_ConsoleController();
    }
    
    static testMethod void myUnitTestTask() {
        
       PageReference ref = new PageReference('/a00/e'); 
    
       RecordType recordType = [Select r.Id, r.DeveloperName From RecordType r where r.SobjectType = 'Account' and r.DeveloperName='RCID' limit 1];
            
       Account acc1 = new Account(Name='Testing Software', BillingCountry = 'US', BillingState = 'IL', recordTypeId=recordType.Id , No_Chronic_Incidents__c =10.0);
       insert acc1;             

       Task task = new Task(subject='test', ActivityDate=Date.Today(),Status='Not Started',Priority='Normal');
       insert task;

       ApexPages.currentPage().getParameters().put('Id', task.id);          
            
       smb_SVOC_Highlights_ConsoleController smb = new smb_SVOC_Highlights_ConsoleController();
    }        
}