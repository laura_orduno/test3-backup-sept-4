/* 2017-16-14 [Aditya Jamwal]
*  Helper class which invokes the Web Service Callout for Billing Account Creation
*/
public class BillingAccountCreationWSCallout {
    
    public static TpFulfillmentCustomerOrderV3.CustomerOrder createBillingAccount(BillingAccountCreationManager.BillingAccountCreationRequest customerAccount) {
        TpFulfillmentCustomerOrderV3.CustomerOrder customerOrder = new TpFulfillmentCustomerOrderV3.CustomerOrder();
        
        try{
            BillingAccountCreationWS.CreateBillingAccountSOAPPort servicePort = new BillingAccountCreationWS.CreateBillingAccountSOAPPort();
            servicePort = prepareCallout(servicePort);
            customerOrder = servicePort.createBillingAccount(prepareCustomerOrderPayload(customerAccount)); 
            system.debug(' OUTSIDE ');
        } 
        catch (CalloutException e) {
            system.debug('In exception e'); 
            OrdrUtilities.auditLogs('BillingAccountWSCallout.createBillingAccount', 
                                    'Billing Account Management', 
                                    'CreateBillingAccountSOAPPort', 
                                    null, 
                                    null, 
                                    true,
                                    e.getMessage(), 
                                    e.getStackTraceString());
            
            throw new OrdrExceptions.BillingAccountCreationServiceException(e.getMessage(), e);
        }
        
        return customerOrder;
    }
    
    private static BillingAccountCreationWS.CreateBillingAccountSOAPPort prepareCallout(BillingAccountCreationWS.CreateBillingAccountSOAPPort servicePort) {
        Ordering_WS__c BillingAccountCreationEndpoint = Ordering_WS__c.getValues('BillingAccountCreationEndpoint');           
        servicePort.endpoint_x = BillingAccountCreationEndpoint.value__c;
        // Set SFDC Webservice call timeout
        servicePort.timeout_x = OrdrConstants.WS_TIMEOUT_MS;    
        system.debug(' endpoint '+servicePort.endpoint_x );
        
        if (servicePort.endpoint_x.startsWith('https://xmlgwy')) {
            Ordering_WS__c wsUserName =  Ordering_WS__c.getValues(OrdrConstants.CREDENTIAL_USERNAME);
            Ordering_WS__c wsPassword =  Ordering_WS__c.getValues(OrdrConstants.CREDENTIAL_PASSWORD); 
            String credentials = wsUserName.value__c + ':' + wsPAssword.value__c;
            String encodedUserNameAndPassword = EncodingUtil.base64Encode(Blob.valueOf(credentials));   
            
            servicePort.inputHttpHeaders_x = new Map<String, String>();
            servicePort.inputHttpHeaders_x.put('Authorization', 'Basic ' + encodedUserNameAndPassword);
        } else {
            servicePort.clientCertName_x = OrdrConstants.SDF_CERT_NAME;
        }
       return servicePort;
    }
   
    private static TpFulfillmentCustomerOrderV3.CustomerOrder prepareCustomerOrderPayload(BillingAccountCreationManager.BillingAccountCreationRequest customerAcc) {
        //check for mandatory fields as per TOCP IA
		validateForMandatoryValues(customerAcc);  
        
        TpFulfillmentCustomerOrderV3.CustomerOrder customerOrder = new TpFulfillmentCustomerOrderV3.CustomerOrder();
        List<TpCommonBaseV3.CharacteristicValue> characteristicValues = new List<TpCommonBaseV3.CharacteristicValue>();
        List<TpFulfillmentCustomerOrderV3.CustomerAccountOrderItem> customerAccountOrderItems = new List<TpFulfillmentCustomerOrderV3.CustomerAccountOrderItem>();
        
        /*create customerOrder/Specification section
        */
        TpCommonBaseV3.EntitySpecification specification = new TpCommonBaseV3.EntitySpecification();
        specification.Name = OrdrConstants.BA_SPECIFICATION_NAME;
        specification.Type_x = OrdrConstants.BA_SPECIFICATION_TYPE;
        specification.Category = OrdrConstants.BA_SPECIFICATION_CATEGORY;
        customerOrder.Specification = specification;
        
        /*create customerOrder/CharacteristicValue/Characteristic/Name section
         * originatorApplicationId
        */
        characteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.BA_ORIGINATOR_APPLICATION_ID, '6161'));
        /*
        characteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.CHARACTERISTIC_TYPE, OrdrConstants.CHARACTERISTIC_TYPE_VALUE));
        //todo: need to figure out where to get actual values
        characteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.DISTRIBUTION_CHANNEL_KEY, '9143202970313945129'));
        //todo: need to figure out where to get actual values
        characteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.CUSTOMER_CATEGORY_KEY, '9141752080013288574'));        
        characteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.USER_ID, OrdrUtilities.getCurrentUserTelusId()));
        */
       customerOrder.CharacteristicValue = characteristicValues;
        
        /*create customerOrder/CustomerAccountOrderItem/CustomerAccount/CharacteristicValue/Characteristic/Name section
        */
        TpFulfillmentCustomerOrderV3.CustomerAccountOrderItem customerAccountOrderItem = new TpFulfillmentCustomerOrderV3.CustomerAccountOrderItem();
        TpInventoryCustomerV3.BaseCustomerAccount customerAccount = new TpInventoryCustomerV3.BaseCustomerAccount();
         List<TpCommonBaseV3.CharacteristicValue> customerAccountCharacteristicValues = new List<TpCommonBaseV3.CharacteristicValue>();
        if(null != customerAcc){
            
     //  customerAccountCharacteristicValues.add(OrdrUtilities.constructCharacteristicValue('coreSandECodes',customerAcc.coreSandECodes));
         if(String.isNotBlank(customerAcc.businessLegalName)){
          customerAccountCharacteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.BA_CUSTOMER_ACCOUNT_BILLING_LEGAL_NAME, customerAcc.businessLegalName));
         }
            
         if(String.isNotBlank(customerAcc.customerProfileID)){
          customerAccountCharacteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.BA_CUSTOMER_ACCOUNT_CUSTOMER_PROFILE_ID, customerAcc.customerProfileID));
         }
            
         if(String.isNotBlank(customerAcc.customerBusinessUnitCustomerId)){
          customerAccountCharacteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.BA_CUSTOMER_ACCOUNT_CBUCID, customerAcc.customerBusinessUnitCustomerId));
         }
            
         if(String.isNotBlank(customerAcc.coreAccountPrefix)){
          customerAccountCharacteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.BA_CUSTOMER_ACCOUNT_CORE_ACCOUNT_PREFIX,customerAcc.coreAccountPrefix));
         }
        
         if(String.isNotBlank(customerAcc.businessDomain)){
          customerAccountCharacteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.BA_CUSTOMER_ACCOUNT_BUSINESS_DOMAIN,customerAcc.businessDomain));
         }
            
         if(String.isNotBlank(customerAcc.businessSubDomain)){
          customerAccountCharacteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.BA_CUSTOMER_ACCOUNT_BUSINESS_SUB_DOMAIN,customerAcc.businessSubDomain));
         }
            
         if(String.isNotBlank(customerAcc.responsiblegroup)){
          customerAccountCharacteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.BA_CUSTOMER_ACCOUNT_RESPONSIBLE_GROUP,customerAcc.responsiblegroup));  
         }
            
         if(String.isNotBlank(customerAcc.provincialTaxAppliesInd)){
          customerAccountCharacteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.BA_CUSTOMER_ACCOUNT_PROVINCIALTAX_APPLIES_IND,customerAcc.provincialTaxAppliesInd));  
         }
            
         if(String.isNotBlank(customerAcc.billingPeriodNumber)){
          customerAccountCharacteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.BA_CUSTOMER_ACCOUNT_BILLING_PERIOD_NUMBER,customerAcc.billingPeriodNumber));  
         } 
            
         if(String.isNotBlank(customerAcc.federalTaxAppliesInd)){
          customerAccountCharacteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.BA_CUSTOMER_ACCOUNT_FEDERALTAX_APPLIESIND,customerAcc.federalTaxAppliesInd));  
         } 
            
         if(String.isNotBlank(customerAcc.serviceTypeCode)){
          customerAccountCharacteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.BA_CUSTOMER_ACCOUNT_SERVICE_TYPE_CODE,customerAcc.serviceTypeCode));  
         }
            
         if(String.isNotBlank(customerAcc.serviceRequested)){
          customerAccountCharacteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.BA_CUSTOMER_ACCOUNT_SERVICE_REQUESTED,customerAcc.serviceRequested));  
         }
            
         if(null != customerAcc.typeOfService  && customerAcc.typeOfService.size()>0){
          customerAccountCharacteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.BA_CUSTOMER_ACCOUNT_TYPE_OF_SERVICE,customerAcc.typeOfService));  
         }
            
         if(String.isNotBlank(customerAcc.contractLengthInMonths)){
          customerAccountCharacteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.BA_CUSTOMER_ACCOUNT_CONTRACT_LENGTH_IN_MONTHS,customerAcc.contractLengthInMonths));  
         }
         if(NULL != customerAcc.startDate){
          customerAccountCharacteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.BA_CUSTOMER_ACCOUNT_START_DATE,String.ValueOf(customerAcc.startDate)));  
         }
            
         if(NULL != customerAcc.billingLanguage){
          customerAccountCharacteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.BA_CUSTOMER_ACCOUNT_BILL_LANGUAGE,String.ValueOf(customerAcc.billingLanguage)));  
         }
            
         if(String.isNotBlank(customerAcc.validateAddress)){
          customerAccountCharacteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.BA_CUSTOMER_ACCOUNT_VALIDATE_ADDRESS,String.ValueOf(customerAcc.validateAddress)));  
             
         }  
        
         if(String.isNotBlank(customerAcc.billingSystemId)){
          customerAccountCharacteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.BA_BILLING_SYSTEM_ID,customerAcc.billingSystemId));  
             
         }
            
         customerAccountCharacteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.BA_CUSTOMER_ACCOUNT_CSR_ID, OrdrUtilities.getCurrentUserTelusId()));
         customerAccountCharacteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.BA_CUSTOMER_ACCOUNT_TAX_PROVINCE, customerAcc.taxProvince));
         customerAccountCharacteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.BA_CUSTOMER_ACCOUNT_CREDIT_CLASS, customerAcc.creditClass));
         customerAccountCharacteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.BA_CUSTOMER_ACCOUNT_DISPLAY_LANGUAGE, customerAcc.displayLanguage));
         
         customerAccount.CharacteristicValue = customerAccountCharacteristicValues;  
        }
         
        
        /*create customerOrder/CustomerAccountOrderItem/CustomerAccount/ContactPerson/CharacteristicValue/Characteristic/Name section
        */
        customerAccount.ContactPerson = new List<TpCommonPartyV3.ContactPerson>();
        List<TpCommonPartyV3.ContactPerson> contactPersonList = new List<TpCommonPartyV3.ContactPerson>(); 
        // iterate to add multiple contacts
        if(null != customerAcc && null != customerAcc.contactPersonList){
            for(BillingAccountCreationManager.ContactPerson contPerson : customerAcc.contactPersonList){
                TpCommonPartyV3.ContactPerson cp = new TpCommonPartyV3.ContactPerson();
                List<TpCommonBaseV3.CharacteristicValue> contactCharacteristicValues = new List<TpCommonBaseV3.CharacteristicValue>();
                contactCharacteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.BA_CONTACT_FIRSTNAME, contPerson.contactFirstName));
                contactCharacteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.BA_CONTACT_LASTNAME, contPerson.contactLastName));
                contactCharacteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.BA_CONTACT_EMAIL, contPerson.contactEmail));
                contactCharacteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.BA_CONTACT_PHONE_NUMBER, contPerson.contactPhoneNumber));
                cp.CharacteristicValue = contactCharacteristicValues;
                contactPersonList.add(cp);
            }
        }
        customerAccount.ContactPerson = contactPersonList;        
        
        
        /*create customerOrder/CustomerAccountOrderItem/CustomerAccount/BillingAddress/CharacteristicValue/Characteristic/Name section
        */
        TpCommonBaseV3.EntityWithSpecification billingAddress = new TpCommonBaseV3.EntityWithSpecification();
         List<TpCommonBaseV3.CharacteristicValue> billingAddressCharacteristicValues = new List<TpCommonBaseV3.CharacteristicValue>();
        if(null != customerAcc && null != customerAcc.billingAddress){
            billingAddressCharacteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.BA_ADDRESS_SUITE_NUMBER, customerAcc.billingAddress.suiteNumber));
            billingAddressCharacteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.BA_ADDRESS_HOUSE_NUMBER, customerAcc.billingAddress.houseNumber));
            billingAddressCharacteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.BA_ADDRESS_STREET_NAME, customerAcc.billingAddress.streetName));
            billingAddressCharacteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.BA_ADDRESS_STREET_TYPE, customerAcc.billingAddress.streetType));
            billingAddressCharacteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.BA_ADDRESS_CITY, customerAcc.billingAddress.city));
            billingAddressCharacteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.BA_ADDRESS_PROVINCE, customerAcc.billingAddress.province));
            billingAddressCharacteristicValues.add(OrdrUtilities.constructCharacteristicValue(OrdrConstants.BA_ADDRESS_POSTAL_CODE, customerAcc.billingAddress.postalCode));
        }
         billingAddress.CharacteristicValue = billingAddressCharacteristicValues;
         customerAccount.BillingAddress = billingAddress;    
        
         
        /*create customerOrder/CustomerAccountOrderItem/Address/CharacteristicValue/Characteristic/Name section
        */
      //  TpCommonUrbanPropertyAddressV3.UrbanPropertyAddress address = OrdrUtilities.constructAddress(inputMap);
        
    //    customerAccountOrderItem.Address = address;
        //create an empty Customer Account tag as per PCOF IA
        customerAccountOrderItem.CustomerAccount = new TpInventoryCustomerV3.BaseCustomerAccount();
        customerAccountOrderItem.CustomerAccount = customerAccount;
        customerAccountOrderItems.add(customerAccountOrderItem);     
        
        customerOrder.CustomerAccountOrderItem = customerAccountOrderItems;
        for( TpFulfillmentCustomerOrderV3.CustomerAccountOrderItem ip: customerOrder.CustomerAccountOrderItem) {
            for(TpCommonBaseV3.CharacteristicValue cV:ip.CustomerAccount.CharacteristicValue)
             system.debug('!@ Customer Order '+ cV.Characteristic.Name +' value '+ cV.Value);
            
        }   
       
        return customerOrder;
    }
    
    public static boolean validateForMandatoryValues(BillingAccountCreationManager.BillingAccountCreationRequest customerAccount){
        
        Map<String, String> mandatoryFields = new Map<String, String>();
        mandatoryFields.put(OrdrConstants.BA_CUSTOMER_ACCOUNT_BILLING_LEGAL_NAME, OrdrConstants.BA_CUSTOMER_ACCOUNT_BILLING_LEGAL_NAME);
        mandatoryFields.put(OrdrConstants.BA_CUSTOMER_ACCOUNT_CUSTOMER_PROFILE_ID, OrdrConstants.BA_CUSTOMER_ACCOUNT_CUSTOMER_PROFILE_ID);
        mandatoryFields.put(OrdrConstants.BA_CUSTOMER_ACCOUNT_CBUCID, OrdrConstants.BA_CUSTOMER_ACCOUNT_CBUCID);
     // place holder for RCID   mandatoryFields.put(OrdrConstants.BA_CUSTOMER_ACCOUNT_CBUCID, OrdrConstants.BA_CUSTOMER_ACCOUNT_CBUCID);
    //    mandatoryFields.put(OrdrConstants.BA_CUSTOMER_ACCOUNT_CSR_ID, OrdrConstants.BA_CUSTOMER_ACCOUNT_CSR_ID);
        mandatoryFields.put(OrdrConstants.BA_CUSTOMER_ACCOUNT_TAX_PROVINCE, OrdrConstants.BA_CUSTOMER_ACCOUNT_TAX_PROVINCE);
        mandatoryFields.put(OrdrConstants.BA_CUSTOMER_ACCOUNT_CREDIT_CLASS, OrdrConstants.BA_CUSTOMER_ACCOUNT_CREDIT_CLASS);
        mandatoryFields.put(OrdrConstants.BA_CUSTOMER_ACCOUNT_DISPLAY_LANGUAGE, OrdrConstants.BA_CUSTOMER_ACCOUNT_DISPLAY_LANGUAGE);
        mandatoryFields.put(OrdrConstants.BA_CONTACT_FIRSTNAME, OrdrConstants.BA_CONTACT_FIRSTNAME);
        mandatoryFields.put(OrdrConstants.BA_CONTACT_LASTNAME, OrdrConstants.BA_CONTACT_LASTNAME);
        mandatoryFields.put(OrdrConstants.BA_CONTACT_EMAIL, OrdrConstants.BA_CONTACT_EMAIL);
        mandatoryFields.put(OrdrConstants.BA_CONTACT_PHONE_NUMBER, OrdrConstants.BA_CONTACT_PHONE_NUMBER);
        mandatoryFields.put(OrdrConstants.BA_ADDRESS_HOUSE_NUMBER, OrdrConstants.BA_ADDRESS_HOUSE_NUMBER);
        mandatoryFields.put(OrdrConstants.BA_ADDRESS_STREET_NAME, OrdrConstants.BA_ADDRESS_STREET_NAME);
        mandatoryFields.put(OrdrConstants.BA_ADDRESS_STREET_TYPE, OrdrConstants.BA_ADDRESS_STREET_TYPE);
        mandatoryFields.put(OrdrConstants.BA_ADDRESS_CITY, OrdrConstants.BA_ADDRESS_CITY);
        mandatoryFields.put(OrdrConstants.BA_ADDRESS_PROVINCE, OrdrConstants.BA_ADDRESS_PROVINCE);
        mandatoryFields.put(OrdrConstants.BA_ADDRESS_POSTAL_CODE, OrdrConstants.BA_ADDRESS_POSTAL_CODE);
        
        if(null != customerAccount){
            
            //Billing Legal Name is mandatory 
            if (String.isNotBlank(customerAccount.businessLegalName)) {                
                mandatoryFields.remove(OrdrConstants.BA_CUSTOMER_ACCOUNT_BILLING_LEGAL_NAME);
            }
            //Customer Profile Id is mandatory 
            if (String.isNotBlank(customerAccount.customerProfileID)) {                
                mandatoryFields.remove(OrdrConstants.BA_CUSTOMER_ACCOUNT_CUSTOMER_PROFILE_ID);
            }
            //CBUCID is mandatory 
            if (String.isNotBlank(customerAccount.customerBusinessUnitCustomerId)) {                
                mandatoryFields.remove(OrdrConstants.BA_CUSTOMER_ACCOUNT_CBUCID);
            }
            //Customer Service Representative Id is mandatory 
            if (String.isNotBlank(customerAccount.customerServiceRepresentativeId)) {                
                mandatoryFields.remove(OrdrConstants.BA_CUSTOMER_ACCOUNT_CSR_ID);
            }
            /* Place holder for RCID 
            if (String.isNotBlank(customerAccount.regionalCustomerId)) {                
                mandatoryFields.remove(OrdrConstants.BA_CUSTOMER_ACCOUNT_CSR_ID);
            } */
            //Tax Province is mandatory 
            if (String.isNotBlank(customerAccount.taxProvince)) {                
                mandatoryFields.remove(OrdrConstants.BA_CUSTOMER_ACCOUNT_TAX_PROVINCE);
            }
            //Credit Class is mandatory 
            if (String.isNotBlank(customerAccount.creditClass)) {                
                mandatoryFields.remove(OrdrConstants.BA_CUSTOMER_ACCOUNT_CREDIT_CLASS);
            }
            //Display Language is mandatory 
            if (String.isNotBlank(customerAccount.displayLanguage)) {                
                mandatoryFields.remove(OrdrConstants.BA_CUSTOMER_ACCOUNT_DISPLAY_LANGUAGE);
            }
            
            //Domain,Sub-Domain and Responsible Group are mandatory for TQ Billing Account 
            if (String.isNotBlank(customerAccount.billingSystemId) && customerAccount.billingSystemId.equals('119')){  
                if(String.isBlank(customerAccount.responsiblegroup)){
                   mandatoryFields.put('responsible group','responsible group');  
                } 
                
                if(String.isBlank(customerAccount.businessDomain)){
                   mandatoryFields.put('domain','domain');  
                }
                
                if(String.isBlank(customerAccount.businessSubDomain)){
                   mandatoryFields.put('sub domain','sub domain');  
                }
            }else if(String.isBlank(customerAccount.billingSystemId)){
                   mandatoryFields.put('billing system','billing system'); 
            }
            
            if(null!= customerAccount.billingAddress){
                
                //House Number is mandatory 
                if (String.isNotBlank(customerAccount.billingAddress.houseNumber)) {                
                    mandatoryFields.remove(OrdrConstants.BA_ADDRESS_HOUSE_NUMBER);
                }
                //Street Name is mandatory 
                if (String.isNotBlank(customerAccount.billingAddress.streetName)) {                
                    mandatoryFields.remove(OrdrConstants.BA_ADDRESS_STREET_NAME);
                }
                //Street Type is mandatory 
                if (String.isNotBlank(customerAccount.billingAddress.streetType)) {                
                    mandatoryFields.remove(OrdrConstants.BA_ADDRESS_STREET_TYPE);
                }
                //City is mandatory 
                if (String.isNotBlank(customerAccount.billingAddress.city)) {                
                    mandatoryFields.remove(OrdrConstants.BA_ADDRESS_CITY);
                }
                //Province is mandatory 
                if (String.isNotBlank(customerAccount.billingAddress.province)) {                
                    mandatoryFields.remove(OrdrConstants.BA_ADDRESS_PROVINCE);
                }
                //PostalCode is mandatory 
                if (String.isNotBlank(customerAccount.billingAddress.postalCode)) {                
                    mandatoryFields.remove(OrdrConstants.BA_ADDRESS_POSTAL_CODE);
                }
            }
            if( null != customerAccount.contactPersonList){
                for( BillingAccountCreationManager.ContactPerson cp : customerAccount.contactPersonList){
                    
                    //Contact FirstName is mandatory 
                    if (String.isNotBlank(cp.contactFirstName)) {                
                        mandatoryFields.remove(OrdrConstants.BA_CONTACT_FIRSTNAME);
                    }else{
                        mandatoryFields.put(OrdrConstants.BA_CONTACT_FIRSTNAME,OrdrConstants.BA_CONTACT_FIRSTNAME);
                    }
                    
                    //Contact LastName is mandatory 
                    if (String.isNotBlank(cp.contactLastName)) {                
                        mandatoryFields.remove(OrdrConstants.BA_CONTACT_LASTNAME);
                    }else{
                        mandatoryFields.put(OrdrConstants.BA_CONTACT_LASTNAME,OrdrConstants.BA_CONTACT_LASTNAME);
                    }
                    
                    //Contact Email is mandatory 
                    if (String.isNotBlank(cp.contactEmail)) {                
                        mandatoryFields.remove(OrdrConstants.BA_CONTACT_EMAIL);
                    }else{
                        mandatoryFields.put(OrdrConstants.BA_CONTACT_EMAIL,OrdrConstants.BA_CONTACT_EMAIL);
                    }
                    
                    //Contact Phone Number is mandatory 
                    if (String.isNotBlank(cp.contactPhoneNumber)) {                
                        mandatoryFields.remove(OrdrConstants.BA_CONTACT_PHONE_NUMBER);
                    }else{
                        mandatoryFields.put(OrdrConstants.BA_CONTACT_PHONE_NUMBER,OrdrConstants.BA_CONTACT_PHONE_NUMBER);
                    }
                }  
            }
        }
        
        if (mandatoryFields.size() > 0) {
            String requiredFields = JSON.serialize(mandatoryFields.values());
            System.debug('requiredFields:' + requiredFields);
            String msg = Label.DFLT0004;
            msg = msg.replace('{0}', Label.BAC_Service_Name);
            msg = msg.replace('{1}', requiredFields);
            throw new OrdrExceptions.InvalidParameterException(msg);                
        }
        return true;
    }
}