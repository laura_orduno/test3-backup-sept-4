public without sharing class QuoteAccountController {

    public class QACException extends Exception {}

    public static final String maskString = '*********';
    public static final Date maskDate = Date.newInstance(3333,1,1) ;
    public static final String[] protectedFields = new List<String>{
       'PCI_Drivers_License_Number__c'
       ,'PCI_Existing_TELUS_Phone_Number__c'
       ,'PCI_Home_Address_Postal_Code__c'
       ,'PCI_Home_Address_Province__c'
       ,'PCI_Home_Address_Street_Direction__c'
       ,'PCI_Home_Address_Street_Name__c'
       ,'PCI_Home_Address_Suite_Number__c'
       ,'PCI_Home_Address_Civic_Number__c'
       ,'PCI_Home_Phone_Number__c'
       ,'PCI_Passport_Number__c'
       ,'PCI_First_Name__c'
       ,'PCI_Last_Name__c'
       ,'PCI_Provincial_ID_Number__c'
       ,'PCI_Social_Insurance_Number__c'
    };
    public static final String[] protectedDateFields = new List<String>{
        'PCI_Date_of_Birth__c'
       ,'PCI_Drivers_License_Expiry_Date__c'
    };
    public Boolean decrypted {get; private set;}
    
    static final String SOLE_PROPRIETOR = 'Sole Proprietorship';
    
    public static final String[] requiredFieldsForSoleProprietor = new List<String>{
       'PCI_Home_Address_Postal_Code__c'
       ,'PCI_Home_Address_Province__c'
       ,'PCI_Home_Address_Street_Name__c'
       ,'PCI_Home_Address_Civic_Number__c'
       ,'PCI_First_Name__c'
       ,'PCI_Last_Name__c'
    };
    
    public static final String[] postalCodeValidationFields = new List<String>{
        'PCI_Home_Address_Postal_Code__c'
    };
    
    public static final String[] requiredFieldsForBusinessRegular = new List<String>{
        'Registraton__c'
       ,'Registration_Date__c'
    };
    
    public static final String[] requiredFieldsForServiceAddress = new List<String>{
       'City__c'
       //,'Street_Type__c'
       ,'State_Province__c'
       ,'Street__c'
       ,'Street_Number__c'
       ,'Special_Location__c'
       ,'Postal_Code__c'
    };

    public String searchTerm {get; set;}
    public List<Web_Account__c> accounts {get; set;}
    public List<QuoteVO> quotes {get; private set;}
    public Web_Account__c selectedAccount {get; private set;}
    
    private Map<String, String> protectedFieldOldValues {get; set;}
    
    public List<Address__c> locations {get; private set;}
    public String selectedLocationId {get; set;}
    public String selectedAccountId {get; set;}
    public Address__c newLocation {get; private set;}
    public Boolean dealerUser {get{return QuotePortalUtils.isUserDealer();}}
    
    public Boolean isAddonQuote {get; set;}
    
    public Boolean ownsThisAccount {get{
        if (selectedAccount == null) { return false; }
        if (!QuotePortalUtils.isUserDealer()) { return true; }
        if (selectedAccount.Dealership_Id__c == null || selectedAccount.Dealership_Id__c == QuotePortalUtils.getDealershipId()) {
            return true;
        }
        return false;
    }}
    
    public Id quoteId {get; private set;}
    private String returnURL;
    public Boolean multiSite {get; private set;}
    
    public String dummyText {get; set;}
    public String dummyBoolean {get; set;}
    
    private Boolean existingAccount; // R5rq1 - quote has existing account associated with it
    
    public QuoteAccountController() {
        isAddonQuote = false;
        existingAccount = false;
        Map<String, String> getParams = ApexPages.currentPage().getParameters();
        returnURL = getParams.get('retURL');
        quoteId = getParams.get('qid');
        selectedAccountId = getParams.get('aid');
        searchTerm = getParams.get('st');
        multiSite = getParams.get('ms') != null && (getParams.get('ms') == '1');
        Boolean loadCreatedAccount = (getParams.get('la') == '1');
        
        Boolean newAccount = false;
        
        if (selectedAccountId != null && searchTerm == null && !loadCreatedAccount) {
            selectedAccount = QuotePortalUtils.loadAccountById(selectedAccountId);
            loadQuotes();
            if (quoteId != null) {
                SBQQ__Quote__c q = QuoteUtilities.loadQuote(quoteId);
                if (q != null) {
                    isAddonQuote = (q.Is_Addon__c != null && q.Is_Addon__c > 0);
                }
            }
        } else {
            SBQQ__Quote__c q = QuoteUtilities.loadQuote(quoteId);
            if (q != null) {
                isAddonQuote = (q.Is_Addon__c != null && q.Is_Addon__c > 0);
                newLocation = new Address__c(Rate_Band__c=q.Rate_Band__c,State_Province__c=q.Province__c,RecordTypeId='012400000005WIh');
                SBQQ__Quote__c[] others = null;
                multiSite = false;
                if (q.Group__c != null) {
                    others = [SELECT SBQQ__Opportunity__r.Web_Account__c FROM SBQQ__Quote__c WHERE Group__c = :q.Group__c AND SBQQ__Opportunity__r.Web_Account__c != null];
                    multiSite = !others.isEmpty();
                }
    
                
                // Traction - R5RQ1 - GA - 2012/03/14
                if (selectedAccountId == null) {
                    if (q.SBQQ__Opportunity__r.Web_Account__c != null) {
                        selectedAccountId = q.SBQQ__Opportunity__r.Web_Account__c;
                        selectedAccount = QuotePortalUtils.loadAccountById(selectedAccountId);
                        ApexPages.currentPage().getParameters().put('aid',selectedAccountId);
                        ApexPages.currentPage().getParameters().put('la','1');
                        existingAccount = true;
                    } else {
                        selectedAccount = new Web_Account__c();
                        selectedAccount.Credit_Check_Authorized__c = 'Yes';
                        newAccount = true;
                    }
                }
                
                if (searchTerm != null) {
                    onSearch();
                // only on complete quote records?
                } else if (loadCreatedAccount) {
                    selectedLocationId = q.Install_Location__c;
                    newLocation = QuoteUtilities.loadAddressById(selectedLocationId);
                    selectedAccount = QuotePortalUtils.loadAccountById(newLocation.Web_Account__c);
                }
            }
        }
        
        initCrypto();
        if (newAccount) {
            decrypted = true;
        }
    }
    
    private void initCrypto(){
        // Mask Protected Fields
        if (selectedAccount == null) { return; }
        decryptProtectedFields();
        loadProtectedFieldValues();
        maskProtectedFields();
    }
    
    private boolean runServiceAddressValidation() {
        if (newLocation == null) {
            return false;
        }
        Boolean error = false;
        for (String s : requiredFieldsForServiceAddress) {
            if (newLocation.get(s) == null) {
                error = true;
                if (s=='City__c') {
                    newLocation.City__c.addError('You must enter a value.');
                }
                if (s=='Street_Type__c') {
                    newLocation.Street_Type__c.addError('You must enter a value.');
                }
                if (s=='State_Province__c') {
                    newLocation.State_Province__c.addError('You must enter a value.');
                }
                if (s=='Street__c') {
                    newLocation.Street__c.addError('You must enter a value.');
                }
                if (s=='Street_Number__c') {
                    newLocation.Street_Number__c.addError('You must enter a value.');
                }
                if (s=='Special_Location__c') {
                    newLocation.Special_Location__c.addError('You must enter a value.');
                }
                if (s=='Postal_Code__c') {
                    newLocation.Postal_Code__c.addError('You must enter a value.');
                }
            }
        }
        return !error;
    }
    
    private boolean runPersonalInformationValidation() {
        if (selectedAccount == null) {
            return false;
        }
        if (selectedAccount.BAN_Type__c == 'Business Regular') {
            Boolean error = false;
            for (String s : requiredFieldsForBusinessRegular) {
                if (selectedAccount.get(s) == null) {
                    error = true;
                    if (s=='Registraton__c') {
                        selectedAccount.Registraton__c.addError('You must enter a value.');
                    }
                    if (s=='Registration_Date__c') {
                        selectedAccount.Registration_Date__c.addError('You must enter a value.');
                    }
                }
            }
            return !error;
        }
        if (selectedAccount.BAN_Type__c == 'Sole Proprietorship') {
        
            Boolean error = false;
            for (String s : requiredFieldsForSoleProprietor) {
                if (selectedAccount.get(s) == null) {
                    error = true;
                    if (s=='PCI_Home_Address_Postal_Code__c') {
                        selectedAccount.PCI_Home_Address_Postal_Code__c.addError('You must enter a value.');
                    }
                    if (s=='PCI_Home_Address_Province__c') {
                        selectedAccount.PCI_Home_Address_Province__c.addError('You must enter a value.');
                    }
                    if (s=='PCI_Home_Address_Street_Name__c') {
                        selectedAccount.PCI_Home_Address_Street_Name__c.addError('You must enter a value.');
                    }
                    if (s=='PCI_Home_Address_Civic_Number__c') {
                        selectedAccount.PCI_Home_Address_Civic_Number__c.addError('You must enter a value.');
                    }
                    if (s=='PCI_First_Name__c') {
                        selectedAccount.PCI_First_Name__c.addError('You must enter a value.');
                    }
                    if (s=='PCI_Last_Name__c') {
                        selectedAccount.PCI_Last_Name__c.addError('You must enter a value.');
                    }
                }
            }
        
            if (selectedAccount.PCI_Drivers_License_Number__c != null && selectedAccount.PCI_Drivers_License_Expiry_Date__c == null) {
                QuoteUtilities.addMessageToPage('You must enter the expiry date for the Driver\'s License.');
                return false;
            }
            if (selectedAccount.PCI_Drivers_License_Number__c == null && selectedAccount.PCI_Drivers_License_Expiry_Date__c != null) {
                QuoteUtilities.addMessageToPage('You must enter the Driver\'s License number if an expiry date is entered.');
                return false;
            }
            if (selectedAccount.PCI_Drivers_License_Number__c == null && selectedAccount.PCI_Social_Insurance_Number__c == null) {
                QuoteUtilities.addMessageToPage('You must enter either a Driver\'s License number or Social Insurance Number.');
                return false;
            }
            
            if (error) {
                return false;
            }
        }
        return true;
    }
    
    private boolean runFieldFormatValidation() {
        if (selectedAccount == null) {
            return false;
        }
        if (selectedAccount.BAN_Type__c == 'Sole Proprietorship') {
            Boolean error = false;
            for (String s : postalCodeValidationFields) {
                if ((String) selectedAccount.get(s) == maskString) {
                    continue;
                }
                if (isValidPostalCode((String)selectedAccount.get(s)) == false) {
                    error = true;
                    if (s=='PCI_Home_Address_Postal_Code__c') {
                        selectedAccount.PCI_Home_Address_Postal_Code__c.addError('Your entry must match the format "A1A 1A1".');
                    }
                }
            }
            
            if (error) {
                return false;
            }
        }
        return true;
    }
    
    boolean isValidPostalCode(String pc) {
        if (pc == null) { return true; }
        pc = pc.toUppercase();
        Pattern postalCodePattern = Pattern.compile('^[ABCEGHJKLMNPRSTVXY]\\d[ABCEGHJKLMNPRSTVWXYZ]( )?\\d[ABCEGHJKLMNPRSTVWXYZ]\\d$');
        return postalCodePattern.matcher(pc).matches();
    }
    
    public PageReference onViewPersonalInformation() {
        try {
            if (selectedAccount == null) {
                return null;
            }
            // Verify permissions
            if (!getCanViewPersonalInformation()) {
                QuoteUtilities.addMessageToPage('You are not permitted to view personal information.');
                return null;
            }
            // Log viewing
            Personal_Information_View_Request__c pivr = new Personal_Information_View_Request__c();
            pivr.Viewer__c = UserInfo.getUserId();
            pivr.Web_Account__c = selectedAccountId;
            pivr.Timestamp__c = Datetime.now();
            insert pivr;
            // Decrypt fields
            if (pivr.Id == null) {
                return null;
            }
            unloadProtectedFieldValues();
            decrypted = true;
        } catch (Exception e) {
            QuoteUtilities.log(e);
        }
        return null;
    }
    
    private void loadProtectedFieldValues() {
        protectedFieldOldValues = new Map<String, String>();
        for (String field : protectedFields) {
            protectedFieldOldValues.put(field, (String)selectedAccount.get(field));
        }
        for (String field : protectedDateFields) {
            protectedFieldOldValues.put(field, (String)selectedAccount.get(field));
        }
    }
    
    private void unloadProtectedFieldValues() {
        if (protectedFieldOldValues == null) {
            return;
        }
        for (String field : protectedFields) {
            if (protectedFieldOldValues.get(field) != null) {
                if (selectedAccount.get(field) == null || selectedAccount.get(field) != maskString) {
                    continue;
                }
                selectedAccount.put(field, (String)protectedFieldOldValues.get(field));
            }
        }
        for (String field : protectedDateFields) {
            String uiField = field.replace('__c', '_UI__c');
            if (protectedFieldOldValues.get(field) != null) {
                if (selectedAccount.get(uiField) == null || selectedAccount.get(uiField) != maskDate) {
                    continue;
                }
                selectedAccount.put(field, (String)protectedFieldOldValues.get(field));
                selectedAccount.put(uiField, destringifyDate((String)protectedFieldOldValues.get(field)));
            }
        }
    }
    
    
    private void clearDateUIFields() {
        selectedAccount.PCI_Date_of_Birth_UI__c = null;
        selectedAccount.PCI_Drivers_License_Expiry_Date_UI__c = null;
    }
    
    private void pullDateUIFields() {
        for (String field : protectedDateFields) {
            String uiField = field.replace('__c', '_UI__c');
            if (selectedAccount.get(uiField) == null) {
                selectedAccount.put(field, null);
                continue;
            }
            if (selectedAccount.get(uiField) == maskDate) {
                continue;
            }
            selectedAccount.put(field, stringifyDate((Date)selectedAccount.get(uiField)));
        }
    }
    
    private void maskProtectedFields() {
        for (String field : protectedFields) {
            if (selectedAccount.get(field) != null && selectedAccount.get(field) != maskString) {
                protectedFieldOldValues.put(field, (String)selectedAccount.get(field));
                selectedAccount.put(field, maskString);
            }
        }
        for (String field : protectedDateFields) {
            if (selectedAccount.get(field) != null && selectedAccount.get(field) != maskString) {
                protectedFieldOldValues.put(field, (String)selectedAccount.get(field));
                selectedAccount.put(field, maskString);
                selectedAccount.put(field.replaceAll('__c', '') + '_UI__c', maskDate);
            }
        }
        decrypted = false;
    }
    
    private void decryptProtectedFields() {
        for (String field : protectedFields) {
            if (selectedAccount.get(field) == null) {
                continue;
            }
            String value = QuoteEncryptionUtilities.decryptStringFromBase64((String)selectedAccount.get(field));
            selectedAccount.put(field, value);
        }
        for (String field : protectedDateFields) {
            String uiField = field.replaceAll('__c', '') + '_UI__c';
            if (selectedAccount.get(field) == null) {
                continue;
            }
            String value = QuoteEncryptionUtilities.decryptStringFromBase64((String)selectedAccount.get(field));
            selectedAccount.put(field, value);
            selectedAccount.put(uiField, destringifyDate(value));
        }
    }
    
    Date destringifyDate(String text) {
        if (text == null) { return null; }
        String[] dS = text.split('/');
        if (dS.size() != 3) { return null; }
        return Date.newInstance(Integer.valueOf(dS[0]), Integer.valueOf(dS[1]), Integer.valueOf(dS[2]));
    }
    
    String stringifyDate(Date d) {
        if (d == null) { return null; }
        return d.year() + '/' + d.month() + '/' + d.day();
    }
    
    private void encryptProtectedFields() {
        for (String field : protectedFields) {
            if (selectedAccount.get(field) == null || (String)selectedAccount.get(field) == '') {
                continue;
            }
            String value = (String)selectedAccount.get(field);
            selectedAccount.put(field, QuoteEncryptionUtilities.encryptStringAsBase64(value));
        }
        
        for (String field : protectedDateFields) {
            if (selectedAccount.get(field) == null || (String)selectedAccount.get(field) == '') {
                continue;
            }
            String value = (String)selectedAccount.get(field);
            selectedAccount.put(field, QuoteEncryptionUtilities.encryptStringAsBase64(value));
        }
    }
    
    public String getLocationReturnURL() {
        try {
        PageReference pref = Page.QuoteWizardAccount;

        pref.getParameters().put('qid', quoteId);
        
        // R5RQ1 - Fix return URL behavior when adding locations to a quote with an account already associated to it
        if (!existingAccount) { // assuming user added a new account or selected from search, NOT a pre-existing account on this quote
            pref.getParameters().put('aid', selectedAccountId);
            pref.getParameters().put('st', searchTerm);
        }
        pref.getParameters().put('step', ApexPages.currentPage().getParameters().get('step'));
        pref.getParameters().put('ms', ApexPages.currentPage().getParameters().get('ms'));
        pref.getParameters().put('addon', ApexPages.currentPage().getParameters().get('addon'));
        return pref.getUrl();
        } catch (Exception e) { QuoteUtilities.log(e); }
        return '';
    }
    
    public PageReference onInit() {
        try {
        QuoteCustomConfigurationController.linkUnassignedAdditionalInformation(quoteId);
        } catch (Exception e) { QuoteUtilities.log(e); }
        return null;
    }
    
    public PageReference onSearch() {
        try {
        if (searchTerm == null || StringUtils.isBlank(searchTerm)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You must enter a search term'));
            return null;
        }
        String filter = '%' + String.escapeSingleQuotes(searchTerm) + '%';
        
        QueryBuilder qb = new QueryBuilder(Web_Account__c.sObjectType);
        qb.getSelectClause().addField(Web_Account__c.Name);
        qb.getSelectClause().addField(Web_Account__c.Billing_City__c);
        qb.getSelectClause().addField(Web_Account__c.RCID__c);
        qb.getSelectClause().addField(Web_Account__c.Billing_Province__c);
        qb.getSelectClause().addField(Web_Account__c.Phone__c);
        qb.getWhereClause().addExpression(qb.lke(Web_Account__c.Name, filter));
        if (QuotePortalUtils.isUserDealer()) {
            qb.getWhereClause().addExpression(qb.eq(Web_Account__c.Dealership_ID__c, QuotePortalUtils.getDealershipId()));
        }
        accounts = Database.query(qb.buildSOQL());
        } catch (Exception e) { QuoteUtilities.log(e); }
        return null;
    }
    
    public String getPreviousPageURL() {
        try {
        PageReference pref = Page.SiteConfigureProducts;
        SBQQ__QuoteLine__c bundle = [SELECT Id FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c = :quoteId AND SBQQ__Bundle__c = true LIMIT 1];
        pref.getParameters().put('lid', bundle.Id);
        pref.getParameters().put('step', 'step2');
        if (multiSite) {
            pref.getParameters().put('ms', '1');
        }
        QuotePortalUtils.addConfigurationSummaryParams(pref);
        pref.getParameters().put('qid', quoteId);
        pref.getParameters().put('cbc', '1');
        pref.getParameters().put('cba0', 'cancel');
        pref.getParameters().put('cbl0', 'Switch Bundle Types');
        pref.getParameters().put('cbu0', Site.getCurrentSiteUrl() + Page.QuoteMigrate.getUrl().replaceAll('/apex/','') + '?qid=' + quoteId );
        pref.getParameters().put('retURL', Site.getCurrentSiteUrl() + Page.QuoteHome.getUrl().replaceAll('/apex/',''));
        pref.getParameters().put('saveURL', Site.getCurrentSiteUrl() + Page.QuoteWizardAccount.getUrl().replaceAll('/apex/','') + '?qid=' + quoteId + '&step=step3' + (multiSite ? '&ms=1' : ''));
        return pref.getUrl();
        } catch (Exception e) { QuoteUtilities.log(e); }
        return null;
    }
    
    public PageReference onSave() {
        System.Savepoint sp = Database.setSavepoint();
        try {
            pullDateUIFields();
            unloadProtectedFieldValues();
            if (!runFieldFormatValidation()) {
                maskProtectedFields();
                return null;
            }
            if (!runPersonalInformationValidation()) {
                maskProtectedFields();
                return null;
            }
            clearDateUIFields();
            encryptProtectedFields();
            update selectedAccount;
            
            /* Start Traction Change 2/13/2012 for Case TC11170 */
            // Copy Account Details to the quote!
            if (quoteId != null) {
                updateQuoteAddress(selectedAccount, false);
            }
            /* End Traction Change 2/13/2012 for Case TC11170 */
            
        } catch (Exception e) {
            Database.rollback(sp);
            QuoteUtilities.log(e);
            initCrypto();
            return null;
        }
        
        initCrypto();
        return (returnURL == null) ? Page.QuoteHome : new PageReference(returnURL);
    }
    
    public PageReference onCancel() {
        if (returnURL != null) {
            return new PageReference(returnURL);
        }
        return Page.QuoteHome;
    }
    
    public PageReference onSaveAndContinue() {
        System.Savepoint sp = Database.setSavepoint();
        try {
            if(selectedAccount == null) { return null; }
            pullDateUIFields();
            unloadProtectedFieldValues();
            if (!runFieldFormatValidation()) {
                maskProtectedFields();
                return null;
            }
            if (!runPersonalInformationValidation()) {
                maskProtectedFields();
                return null;
            }
            clearDateUIFields();
            encryptProtectedFields();
            
            upsert selectedAccount;
            
            initCrypto();
    
            if(newLocation == null) { return null; }
            
            if (!runServiceAddressValidation()) {
                return null;
            }
            
            newLocation.Web_Account__c = selectedAccount.Id;
            upsert newLocation;
            selectedLocationId = newLocation.Id;
            
            SBQQ__Quote__c qx = new SBQQ__Quote__c(Id=quoteId);
            qx.Install_Location__c = newLocation.Id;
            update qx;
            
            Opportunity opp = [SELECT SBQQ__Opportunity__r.Web_Account__c FROM SBQQ__Quote__c WHERE Id = :quoteId].SBQQ__Opportunity__r;
            opp.Web_Account__c = selectedAccount.Id;
            update opp;
            
            updateQuoteAddress(selectedAccount, true);
        } catch (Exception e) {
            Database.rollback(sp);
            QuoteUtilities.log(e);
            initCrypto();
            return null;
        }
        
        PageReference pref = Page.QuoteWizardContact;
        pref.setRedirect(true);
        pref.getParameters().put('qid', quoteId);
        pref.getParameters().put('step', 'step4');
        pref.getParameters().put('ac', '1');
        pref.getParameters().put('lc', '1');
        if (multiSite) {
            pref.getParameters().put('ms', '1');
        }
        return pref;
    }
    
    private Integer countActiveQuotes(Id locationId) {
        return [SELECT count() FROM SBQQ__Quote__c WHERE Install_Location__c = :locationId AND Active__c = 'Yes' AND Completed__c = true AND Id != :quoteId];
    }
    
    private void updateQuoteAddress(Web_Account__c acct, Boolean updateLocation) {
        SBQQ__Quote__c quote = new SBQQ__Quote__c(Id=quoteId);
        quote.SBQQ__BillingName__c = acct.Name;
        quote.SBQQ__BillingStreet__c = acct.Billing_Address__c;
        quote.SBQQ__BillingCity__c = acct.Billing_City__c;
        quote.SBQQ__BillingState__c = acct.Billing_Province__c;
        quote.SBQQ__BillingPostalCode__c = acct.Billing_Postal_Code__c;
        quote.SBQQ__ShippingName__c = acct.Name;
        quote.SBQQ__ShippingStreet__c = acct.Shipping_Address__c;
        quote.SBQQ__ShippingCity__c = acct.Shipping_City__c;
        quote.SBQQ__ShippingState__c = acct.Shipping_Province__c;
        quote.SBQQ__ShippingPostalCode__c = acct.Shipping_Postal_Code__c;
        if(updateLocation == true) {quote.Install_Location__c = selectedLocationId;}
        update quote;
    }
    
    public PageReference onSelectAndContinue() {
        if (StringUtils.isBlank(selectedLocationId)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You must select a location'));
            return null;
        }
        
        if (countActiveQuotes(selectedLocationId) > 0) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'There is already an active quote against this location'));
            return null;
        }
        
        System.Savepoint sp = Database.setSavepoint();
        try {
            Web_Account__c acct = QuotePortalUtils.loadAccountById(selectedAccountId);
            updateQuoteAddress(acct, true);
            
            Opportunity opp = [SELECT SBQQ__Opportunity__r.AccountId FROM SBQQ__Quote__c WHERE Id = :quoteId].SBQQ__Opportunity__r;
            opp.Web_Account__c = selectedAccountId;
            update opp;
        } catch (Exception e) {
            Database.rollback(sp);
            QuoteUtilities.log(e);
            return null;
        }
        
        PageReference pref = Page.QuoteWizardContact;
        pref.getParameters().put('qid', quoteId);
        pref.getParameters().put('step', 'step4');
        pref.getParameters().put('ms', ApexPages.currentPage().getParameters().get('ms'));
        pref.getParameters().put('st', searchTerm);
        return pref;
    }
    
    private void loadQuotes() {
        quotes = new List<QuoteVO>();
        if (QuotePortalUtils.isUserDealer()) {
            for (SBQQ__Quote__c quote:
                [SELECT SBQQ__Status__c, Name, CreatedDate, SBQQ__ExpirationDate__c, 
                    CreatedBy.Name, LastModifiedBy.Name, LastModifiedDate,
                    SBQQ__Opportunity__r.Web_Account__c, Account_Name__c,
                    Account_Phone__c, SBQQ__BillingCity__c, 
                    SBQQ__BillingState__c, SBQQ__BillingStreet__c,
                    Assigned_Account_Order__c, Dealer_record_Dealer_code__c
                 FROM SBQQ__Quote__c WHERE SBQQ__Opportunity__r.Web_Account__c = :selectedAccountId and Dealership_ID__c = :QuotePortalUtils.getDealershipId()]) {
                quotes.add(new QuoteVO(quote));
            }
        } else {
            for (SBQQ__Quote__c quote:
                [SELECT SBQQ__Status__c, Name, CreatedDate, SBQQ__ExpirationDate__c, 
                    CreatedBy.Name, LastModifiedBy.Name, LastModifiedDate,
                    SBQQ__Opportunity__r.Web_Account__c, Account_Name__c,
                    Account_Phone__c, SBQQ__BillingCity__c, 
                    SBQQ__BillingState__c, SBQQ__BillingStreet__c,
                    Assigned_Account_Order__c, Dealer_record_Dealer_code__c
                 FROM SBQQ__Quote__c WHERE SBQQ__Opportunity__r.Web_Account__c = :selectedAccountId]) {
                quotes.add(new QuoteVO(quote));
            }
        }
    }
    
    public PageReference onSelectAccount() {
        try {
        locations = QuoteUtilities.loadAddressesByAccountId(selectedAccountId);
        // R5RQ1 - quote is now using a newly-selected account, not the account that was associated with it
        existingAccount = false;
        } catch (Exception e) { QuoteUtilities.log(e); } return null;
    }
    
    public Boolean getShowViewPersonalInformationButton() {
        return getCanViewPersonalInformation() && (decrypted == false) && selectedAccount != null && selectedAccount.BAN_Type__c == SOLE_PROPRIETOR;
    }
    
    public Boolean getCanViewPersonalInformation() {
        return channelCanViewPersonalInformation(QuotePortalUtils.getChannel());
    }
    
    public static boolean channelCanViewPersonalInformation(String channel) {
        if (channel == null) { return false; }
        channel = channel.toLowercase();
        return (channelCanViewPersonalInformationMap.containsKey(channel) && channelCanViewPersonalInformationMap.get(channel) == true);
    }
    public static Map<String, Boolean> channelCanViewPersonalInformationMap = new Map<String, Boolean> {
        'inbound' => true,
        'outbound' => true,
        'inside sales' => true,
        'dealer' => false,
        'unassigned ee' => true,
        'assigned ee' => true,
        'pre-sales' => true,
        'post-sales' => true,
        'bacc' => true,
        'nac' => true
    };
}