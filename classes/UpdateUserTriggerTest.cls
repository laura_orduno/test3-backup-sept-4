@isTest
public class UpdateUserTriggerTest {

    @isTest(SeeAlldata='true')
    public static void testUpdateUser(){
    	Test.startTest();
    	Employee__c emp = new Employee__c(Employee_ID__c='09942573',  Manager_ID__c='09942571', Release_Code__c='03', TMODS_TEAM_MEMBER_ID__c = 98765);
        User sampleUsr = [select id, Name, Release_Code__c, managerid, Username, LastName, Email, Alias, CommunityNickname, TimeZoneSidKey, LocaleSidKey, EmailEncodingKey, ProfileId, LanguageLocaleKey 
                                    	from User where id = :UserInfo.getUserId()];
        User testUser = sampleUsr.clone(false,true);
        String key = 'Usr990';
        testUser.username =key+'@telus.com.ebam'; 
        testUser.CommunityNickname ='ba'+key;
        testUser.FirstName =key;
        testUser.LastName =key;
        testUser.Alias =key;
        testUser.EmployeeId__c = '09942573';
        Database.insert(emp);
        Database.insert(testUser);
        emp.Release_Code__c ='05';
        update emp;
        Test.stopTest();
    }
    
}