@isTest
private class trac_SMETrequestTest {

   static testMethod void testSMETrequest(){

      // Retrieve two profiles, for the standard user and the system   
    
      // administrator, then populate a map with them.
      Map<String,ID> profiles = new Map<String,ID>();
      List<Profile> ps = [select id, name from Profile where name = 
         'Standard User' or name = 'System Administrator'];
      for(Profile p : ps){
         profiles.put(p.name, p.id);
      }

      // Create the users to be used in this test.  
      // First make a new user.

      User standard = new User(alias = 'standt', 
      email='standarduser123123@testorg.com', 
      emailencodingkey='UTF-8', 
      lastname='Testing', languagelocalekey='en_US', 
      localesidkey='en_US', 
      profileid = profiles.get('Standard User'), 
      timezonesidkey='America/Los_Angeles', 
      phone = '6049969449',
      username='standarduser123123@testorg.com');

      insert standard;

      // Then instantiate a user from an existing profile  
    

      User admin = new User(alias = 'admint1', 
      email='adminuser123123@testorg.com', 
      emailencodingkey='UTF-8', 
      lastname='TestingAdmin', languagelocalekey='en_US', 
      localesidkey='en_US', 
      profileid = profiles.get('System Administrator'), 
      timezonesidkey='America/Los_Angeles', 
      phone = '6049969449',
      username='adminuser123123@testorg.com');

      insert admin;

      system.runas(admin){
        Group g = new Group(name = 'testgroup');
      	insert g;
      
      	GroupMember gm = new GroupMember(GroupId = g.id, UserOrGroupId = standard.id);
      	insert gm;
          
          
	      Case myCase = new Case();
	      
	      myCase.Subject = 'TEST';
	      insert myCase;
	      
	      SMET_Request__c r = new SMET_Request__c(
	      	Subject__c = 'Test Subject 1',
	      	Description__c = 'Test Description 1',
	      	Email_From_Address__c = standard.email,
	      	ownerid = g.id
	      );
	      insert r;
	      
	      SMET_Request__c r2 = new SMET_Request__c(
	      	Subject__c = 'Test Subject 1',
	      	Description__c = 'Test Description 1',
	      	Historical_Case__c = myCase.Id,
	      	ownerid = g.id
	      );
	      insert r2;
	      
	      Task myTask =  new Task();
	      
	      myTask.Subject = 'Test';
	      myTask.WhatId = myCase.Id;
	      insert myTask;
      }
      
      system.runas(standard) {
	      SMET_Request__c r3 = new SMET_Request__c(
	          	Subject__c = 'Test Subject 1',
	          	Description__c = 'Test Description 1'
	      );
	      insert r3;
      }
   }
}