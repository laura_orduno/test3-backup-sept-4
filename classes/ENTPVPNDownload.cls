public with sharing class ENTPVPNDownload {

	Public List<ENTP_portal_downloads__c> DownLoadList { get; set; }
	public Boolean showContent { get; set; }
	public Boolean showButton { get; set; }
	public string verID { get; set; }
	public static String userLanguage {
		get {
			return PortalUtils.getUserLanguage();
		}
		set;
	}
	public ENTPVPNDownload() {
		//Set the showContent variable to true on page load
		showContent = false;
		showButton = false;
	}
	//Method called when the Toggle Content button is clicked
	public PageReference toggleContent() {

		//If the showContent variable is true, set it to false, else, set it to true
		if (showContent) {
			showContent = false;
		} else {
			showContent = true;
		}
		return null;
	}
	public PageReference toggleButton() {
		//If the showContent variable is true, set it to false, else, set it to true
		if (showButton) {
			showButton = false;
		} else {
			showButton = true;
		}
		return null;
	}
	Public PageReference dlList() {
		system.debug('Start PageReference');

		DLoadList();
		return null;
	}
	public void DLoadList() {
		system.debug('DLoadList');
		Map<String, ENTP_portal_downloads__c> PdoownL = ENTP_portal_downloads__c.getall();
		system.debug('Down Load List:-' + PdoownL);
		//DownLoadList = new List<String>();
		DownLoadList = PdoownL.values();
		// DownLoadList.addAll(PdoownL.keySet());
		system.debug('Down Load List:-' + DownLoadList);

	}

	public String getLanguageLocaleKey() {
		return userLanguage;
	}
}