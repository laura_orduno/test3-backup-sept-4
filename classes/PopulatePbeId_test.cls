@isTest
public class PopulatePbeId_test {
	@isTest
    Static void testMethod_One(){
        account acc = new account(name='Test Acc');        
        insert acc; 
        
        Id pricebookId= Test.getStandardPricebookId();
        Pricebook2 customPB = new Pricebook2(Name='OCOM Pricebook', isActive=true);
        insert customPB;
        
        Product2 prod = new Product2(Name = 'Disconnect Services',Family = 'Hardware',OCOM_Bookable__c='Yes');
        insert prod;
        
        PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod.Id,UnitPrice = 10000, IsActive = true);
        insert standardPrice;  
        
        PricebookEntry customPbe = new PricebookEntry(Pricebook2Id = customPB.Id, Product2Id = prod.Id,UnitPrice = 12000, IsActive = true);
        insert customPbe;
        
        Asset testAsset = new Asset();
        testAsset.AccountId=acc.Id;
        testAsset.Name = 'test';
        testAsset.Product2Id = prod.Id;
        insert testAsset;
        
        Test.startTest();
        Database.executeBatch(new Prod_PopulatePbeId());
        Test.stopTest();
    }
    @isTest
    Static void testMethod_two(){
        account acc = new account(name='Test Acc');        
        insert acc; 
        
        Id pricebookId= Test.getStandardPricebookId();
        Pricebook2 customPB = new Pricebook2(Name='OCOM Pricebook', isActive=true);
        insert customPB;
        
        Product2 prod = new Product2(Name = 'Disconnect Services',Family = 'Hardware',OCOM_Bookable__c='Yes');
        insert prod;
        
        PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod.Id,UnitPrice = 10000, IsActive = true);
        insert standardPrice;  
        
        PricebookEntry customPbe = new PricebookEntry(Pricebook2Id = customPB.Id, Product2Id = prod.Id,UnitPrice = 12000, IsActive = true);
        insert customPbe;
        
        List<Asset> assetList =new List<Asset>();
        for(integer i=0;i<200;i++){
            Asset testAsset = new Asset();
            testAsset.AccountId=acc.Id;
            testAsset.Name = 'test';
            testAsset.Product2Id = prod.Id;
            if(i>150){
                testAsset.vlocity_cmt__LineNumber__c = '0001.0003';
            }
        	assetList.add(testAsset);
        }
        insert assetList;
        system.debug('AssetList'+assetList.size());
        Test.startTest();
        Database.executeBatch(new Prod_PopulatePbeId());
        Test.stopTest();
    }
}