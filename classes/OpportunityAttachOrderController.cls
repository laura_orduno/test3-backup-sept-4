public class OpportunityAttachOrderController {
    List<Order> orders;
    public Opportunity opp {get;set;}
    public OpportunityAttachOrderController(ApexPages.StandardController controller){
        opp=(Opportunity)controller.getRecord();
        opp=[select AccountId,name from Opportunity where id=:opp.id];
    }
    public List<Order> getOrders() {
        if(orders == null){
            //orders = [SELECT Id, Account.name, Status, Description, Name, OrderNumber, TotalAmount, vlocity_cmt__AccountId__c, vlocity_cmt__OneTimeTotal__c, vlocity_cmt__RecurringTotal__c,  Opportunity_Name__c, RequestedDate__c, Scheduled_Due_Date__c FROM Order where vlocity_cmt__OpportunityId__c=:opp.id LIMIT 10];
            //001c0000018mO64AAE
        orders = [SELECT Id, Account.name, Status, Description, Name, OrderNumber, TotalAmount, vlocity_cmt__AccountId__c,
                  vlocity_cmt__OneTimeTotal__c, vlocity_cmt__RecurringTotal__c,  Opportunity_Name__c, RequestedDate__c, Scheduled_Due_Date__c
                  FROM Order where accountid=:opp.AccountId LIMIT 10];
        }
        
        return orders;
    }
    public PageReference newOrder(){
        return null;
    }
    public PageReference attachOrder(){
        return null;
    }
}