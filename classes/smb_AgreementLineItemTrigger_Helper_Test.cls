/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class smb_AgreementLineItemTrigger_Helper_Test {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Apttus__APTS_Agreement__c agreement = smb_test_utility.createAgreement(Null, true);
        list<Apttus__AgreementLineItem__c> listlineItem = new  list<Apttus__AgreementLineItem__c>();
        Apttus__AgreementLineItem__c lineItem1 = smb_test_utility.createAgreementLineItem(Null, agreement.Id, false);
        lineItem1.LineNo__c = 1;
        listlineItem.add(lineItem1);
        Apttus__AgreementLineItem__c lineItem2 = smb_test_utility.createAgreementLineItem(Null, agreement.Id, false);
        lineItem2.LineNo__c = 2;
        lineItem2.ParentLineNo__c = 1;
        listlineItem.add(lineItem2);
        insert listlineItem;
    }
}