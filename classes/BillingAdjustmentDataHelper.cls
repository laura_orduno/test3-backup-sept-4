public class BillingAdjustmentDataHelper{

    private static Set<ReasonCodeDescription> reasonCodeDescSet = new Set<ReasonCodeDescription>();
    private static Set<id> accountIdSet = new Set<id>();
    private static Set<ApprovalThresholdHelper.ThresholdFilter>  adjtTypeBuFilter = new Set<ApprovalThresholdHelper.ThresholdFilter>();
    private static Set<id>  userIdSet = new Set<id>();
    private static Set<String> updatedFieldSet = new Set<String>();
    private static Set<String> businessUnitSet = new Set<String>();
    
    private static Map<ReasonCodeDescription,String> reasonCodeMap;
    private static Map<id,id> accountIdMap;
    private static Map<id,Map<String,id>> salesAssingmentUserMap;
    private static Map<id,User> userMap;
    private static Map<String,Billing_Adjustment_BU_Controller_Map__c> buControllerMap;
    
    public static boolean isCachedData =false;
    public static boolean needReasonCodeLoad =false;
    public static boolean needAccountIdLoad=false;
    public static boolean needReqReleaseCodeLoad =false;
    public static boolean needControllersLoad = false;
    public static boolean needApprovalThresholdLoad =false;
    public static Map<id,sObject> oldMap {get;set;}
    public static Map<String, Product_Approval_Threshold_Settings__c> patSettings;
    public static Map<String, Approval_Threshold__c> productCodeToApprovalThreshold = new Map<String, Approval_Threshold__c>();
    
    public static Group creditServicesQueue = [select Id from Group where DeveloperName = 'Business_Credit_Services' and Type = 'Queue' Limit 1];
    
    static {
        if(Trigger.isExecuting && Trigger.isUpdate){
            oldMap = Trigger.oldMap;
        }
    }

    public static void addData(Billing_Adjustment__c ba){
        addReasonCodeDescription(ba.RC_Level_1__c,'Adjustment Driver');
        addReasonCodeDescription(ba.RC_Level_2__c,'Description');
        addReasonCodeDescription(ba.RC_Level_3__c,'Reason');
        addReasonCodeDescription(ba.RC_Product__c,'Product');
        addReasonCodeDescription(ba.RC_Responsible_Area__c,'Responsible Area');
        addAccountId(ba.account__c);
        addAdjustmentTypeAndBusinessUnit(ba.Transaction_Type__c==null?ba.RecordType.Name:ba.Transaction_Type__c,ba.Business_Unit__c);
        addUser(ba.OwnerId);addUser(ba.Next_Approver__c);addUser(ba.Next_Approver__r.ManagerId);addUser(ba.Next_Approver__r.Approval_Manager__c);addUser(ba.Original_Owner__c);
        addUser(ba.Requested_By__c);addUser(ba.Other_Approval_Manager__c);
        adduser(ba.Next_Hierarchy_Approver__c);
        addUser(ba.Last_Approver_User__c);
        addUser(UserInfo.getUserId());
        checkFieldUpdates(ba);
        
    }
    
    private static void addReasonCodeDescription(String des, String field){
        reasonCodeDescSet.add(new ReasonCodeDescription(des.toLowerCase(),field.toLowerCase()));
    }
 
    private static void addAccountId(id accId){
        accountIdSet.add(accId);
    }

    private static void addAdjustmentTypeAndBusinessUnit(String adjt, String bu){
       adjtTypeBuFilter.add( new ApprovalThresholdHelper.ThresholdFilter(adjt,bu));
        businessUnitSet.add(bu);
    }

    public static void addUser(Id userId){
        if(userId != null)
            userIdSet.add(userId);
    }

    public static Set<ReasonCodeDescription>  getReasonCodeDescSet(){
        return reasonCodeDescSet;
    }

    public static Set<id>  getAccountId(){
        return accountIdSet;
    }

    public static Set<ApprovalThresholdHelper.ThresholdFilter> getAdjustmentTypeAndBusinessUnit(){
        return adjtTypeBuFilter;
    }

    public static Set<id> getUserList(){
        return userIdSet;
    }
    
    public static Map<ReasonCodeDescription,String> getReasonDescCodeMap(Set<ReasonCodeDescription> descSet){
        Map<ReasonCodeDescription,String> codeMap =new Map<ReasonCodeDescription,String>();
        List<String> decList = new List<String>();
        for(ReasonCodeDescription rd : descSet){
            decList.add(rd.Name);
        }
        
        if(decList.size()>0){
            List<Billing_Adjustment_RC_Mapping__c> releaseCodeList = [Select Name, Code__c, Field_Type__c from Billing_Adjustment_RC_Mapping__c where Name in : decList];
            for(Billing_Adjustment_RC_Mapping__c rc : releaseCodeList){
                codeMap.put(new ReasonCodeDescription(rc.name.toLowerCase(), rc.Field_Type__c.toLowerCase()), rc.Code__c);
            }
        }
        return codeMap;    
    }

    /**
     * loadProductApprovalThresholds
     * @description Load the product approval thresholds to be evaluated
     * @author Thomas Tran, Traction on Demand
     * @date FEB-23-2016
     */
    public static void loadProductApprovalThresholds(List<Billing_Adjustment__c> baList){
        Id productApprovalId = Schema.SObjectType.Approval_Threshold__c.getRecordTypeInfosByName().get('Product Approval Threshold').getRecordTypeId();
        patSettings = Product_Approval_Threshold_Settings__c.getAll();
        Set<String> productCat1 = new Set<String>();
        Set<String> productCat2 = new Set<String>();

        for(Billing_Adjustment__c baTemp : baList){
        productCat1.add(baTemp.Product_Type__c);
        productCat2.add(baTemp.RC_Product__c);
        }

        String query = 'SELECT OwnerId, Product_Category_1__c, Product_Category_2__c, ';
        String fields = '';
        for(Product_Approval_Threshold_Settings__c temp : patSettings.values()){
          if(fields.isWhitespace()){
              fields += temp.Threshold_API_Name__c;
          } else{
              fields += ',' + temp.Threshold_API_Name__c;
          }
        }

        query += fields;
        query += ' FROM Approval_Threshold__c WHERE RecordTypeId = :productApprovalId AND Product_Approval_Threshold_Active__c = true AND Product_Category_1__c IN :productCat1 AND Product_Category_2__c IN :productCat2'; //add product to query when ready

        List<Approval_Threshold__c> atList = Database.query(query);

        for(Approval_Threshold__c atTemp : atList){
            productCodeToApprovalThreshold.put(atTemp.Product_Category_1__c + atTemp.Product_Category_2__c, atTemp);
        }

    }
       
    public static void loadData(){
            if(needReasonCodeLoad){
                reasonCodeMap = getReasonDescCodeMap(reasonCodeDescSet);
            }
            if(needAccountIdLoad){
                loadAccountAndSalseAssinmentMap();
                for(id SAuserId: getSalesAssimnetUserIdList()){
                    userIdSet.add(SAuserId);
                }
            }
            userMap = getUserMap();
            
            //if(needReqReleaseCodeLoad || needApprovalThresholdLoad){
                ApprovalThresholdHelper.loadThresholdList(adjtTypeBuFilter);
            //}
            if(needControllersLoad){
               buControllerMap = getCotrollerUserMap(businessUnitSet); 
            }
    }
    
    public static String getReasonCode(Billing_Adjustment__c ba){
        ReasonCodeDescription rcL1 = new ReasonCodeDescription(ba.RC_Level_1__c.toLowerCase(),'Adjustment Driver'.toLowerCase());
        ReasonCodeDescription rcL2 = new ReasonCodeDescription(ba.RC_Level_2__c.toLowerCase(),'Description'.toLowerCase());
        ReasonCodeDescription rcL3 = new ReasonCodeDescription(ba.RC_Level_3__c.toLowerCase(),'Reason'.toLowerCase());
        ReasonCodeDescription rcLProd = new ReasonCodeDescription(ba.RC_Product__c.toLowerCase(),'Product'.toLowerCase());
        ReasonCodeDescription rcRespArea = new ReasonCodeDescription(ba.RC_Responsible_Area__c.toLowerCase(),'Responsible Area'.toLowerCase());
        String reasonCode = reasonCodeMap.get(rcL2) + 
              reasonCodeMap.get(rcRespArea) + reasonCodeMap.get(rcL3) + reasonCodeMap.get(rcLProd);
        return reasonCode.replaceAll('null','?');
    }
    
    public static void validateReleaseCode(Billing_Adjustment__c ba){
           if(reasonCodeMap.get(new ReasonCodeDescription(ba.RC_Level_1__c.toLowerCase(),'Adjustment Driver'.toLowerCase()))==null){
                ba.RC_Level_1__c.addError('Incorrect Value');
           }
           if(reasonCodeMap.get(new ReasonCodeDescription(ba.RC_Level_2__c.toLowerCase(),'Description'.toLowerCase()))==null){
                ba.RC_Level_2__c.addError('Incorrect Value');
           }
           if(reasonCodeMap.get(new ReasonCodeDescription(ba.RC_Level_3__c.toLowerCase(),'Reason'.toLowerCase()))==null){
                ba.RC_Level_3__c.addError('Incorrect Value');
           }
           if(reasonCodeMap.get(new ReasonCodeDescription(ba.RC_Product__c.toLowerCase(),'Product'.toLowerCase()))==null){
                ba.RC_Product__c.addError('Incorrect Value');
           }
           if(reasonCodeMap.get(new ReasonCodeDescription(ba.RC_Responsible_Area__c.toLowerCase(),'Responsible Area'.toLowerCase()))==null){
                ba.RC_Responsible_Area__c.addError('Incorrect Value');
           } 
    }
    
    public static Id getParentAccountId(Billing_Adjustment__c ba){
        Map<Id,Id> idMap = getParentAccountMap(accountIdSet);
        return accountIdMap.get(ba.Account__c);
    }
    
    public static Map<id,id> getParentAccountMap(Set<id> accountIds){
       Map<id,id> idMap = new Map<id,id>();
        for(id aid : accountIds){
             Account pAccount = getParentRCID(aid);
             if(pAccount !=null){
                idMap.put(aid, pAccount.id);    
             }else{
                idMap.put(aid, null);  
             }
         }
        return idMap;
    }
    
    /**
     * Recursivley Get the Parent RCID account for the given Account Id
     */
    private static  Account getParentRCID(id accid){
        Account acc = null;
        Account acc1 =null;
        try{
           acc = [select id,Name, parentId from Account where id =:accid]; 
           acc1 = [select Name, id, RecordType.Name  from Account where id = :acc.parentId]; 
        }catch(Exception e){
            return null;
        }  
        if(acc1.RecordType.Name == 'RCID'){
            return acc1;
        }else{
            return getParentRCID(acc1.id);
        }
    }
    
    public static Map<id,Map<String,id>> getSalesAssignmentsMap(List<id> parentAccIdList){
        Map<id,Map<String,id>> salesAssingmentMap = new Map<id,Map<String,id>>();
        List<Sales_Assignment__c> salesAssinments = [select User__c, Role__c,Account__c from  Sales_Assignment__c 
                                                     where Account__c in :parentAccIdList and Role__c in ('DIR','MD','ACCTPRIME','WLS_DIR') Order By LastModifiedDate ];
        for(Sales_Assignment__c salesAssinment : salesAssinments ){
            if(salesAssingmentMap.containsKey(salesAssinment.Account__c)){
               salesAssingmentMap.get(salesAssinment.Account__c).put(salesAssinment.Role__c,salesAssinment.User__c); 
            }else{
                Map<String, Id> userRole =new Map<String, Id>();
                userRole.put(salesAssinment.Role__c,salesAssinment.User__c);
                salesAssingmentMap.put(salesAssinment.Account__c,userRole);    
            }
        }
        return salesAssingmentMap;
    }
   
    public static Map<String,Billing_Adjustment_BU_Controller_Map__c> getCotrollerUserMap(Set<String> businessUnitList){
        Map<String,Billing_Adjustment_BU_Controller_Map__c> controllerMap = new Map<String,Billing_Adjustment_BU_Controller_Map__c>();
         List<Billing_Adjustment_BU_Controller_Map__c> controllerList = [select Business_Unit__c, Business_Unit_Controller__c, Corporate_Controller__c, VP_Controller__c from Billing_Adjustment_BU_Controller_Map__c 
                                                             where Business_Unit__c in :businessUnitList];
       
        for( Billing_Adjustment_BU_Controller_Map__c controler : controllerList){
           controllerMap.put(controler.Business_Unit__c, controler);
        }
        return controllerMap;
    }
    
    public static id getBuController(Billing_Adjustment__c ba){
         return buControllerMap.get(ba.Business_Unit__c).Business_Unit_Controller__c;
    }
    
    public static id getCoporateController(Billing_Adjustment__c ba){
         return buControllerMap.get(ba.Business_Unit__c).Corporate_Controller__c;
    }

    /**
     * getVPController
     * @description Returns the VP controller ID
     * @author Thomas Tran, Traction on Demand
     * @date FEB-22-2016
     */
    public static id getVPController(Billing_Adjustment__c ba){
         return buControllerMap.get(ba.Business_Unit__c).VP_Controller__c;
    }
    
    public static id getSalesAssimnetUserIdByRole(Billing_Adjustment__c ba, String role){
        if(salesAssingmentUserMap == null)
            loadAccountAndSalseAssinmentMap();
        if(salesAssingmentUserMap !=null && !salesAssingmentUserMap.isEmpty())
            return salesAssingmentUserMap.get(ba.Parent_Account_id__c).get(role);
        else
            return null;   
    }
    
    
    public static String getSalesAssinmentUserEmailByRole(Billing_Adjustment__c ba, String role){
        User  usr = getUserById(getSalesAssimnetUserIdByRole(ba,role));
        return usr != null? usr.email:'';
    }
    
    public static User getSalesAssinmentUserByRole(Billing_Adjustment__c ba, String role){
        return getUserById(getSalesAssimnetUserIdByRole(ba,role));
    }
    
    public static List<id> getSalesAssimnetUserIdList(){
        if(salesAssingmentUserMap == null)
            loadAccountAndSalseAssinmentMap();
        List<id> SAUserIdList =new List<id>();
        for(Map<String,id> RoleUserid : salesAssingmentUserMap.values()){
            for(id userId : RoleUserid.values()){
                SAUserIdList.add(userId);    
            }
        }
        return SAUserIdList;
    }
    
    public static Map<id,User> getUserMap(){
        
            Map<id,User> usrManagerMap = new Map<id,User>();
           
            List<User> UsersWithManagers = [select id ,Name, release_code__c, ManagerId, email, EmployeeId__c, IsActive, Approval_Manager__c,
                                            TeamCard_Level1_Mgr_ID__c, TeamCard_Level2_Mgr_ID__c,TeamCard_Level3_Mgr_ID__c, TeamCard_Level4_Mgr_ID__c
                                            from User where id in: userIdSet];
            for(User user : UsersWithManagers){
                usrManagerMap.put(user.id, user);
            }
            List<id> managerIds = new List<id>();
            for(User user : usrManagerMap.values()){
                managerIds.add(user.ManagerId);
                managerIds.add(user.Approval_Manager__c);
            }
            List<User> managerList = [select id ,Name, release_code__c, ManagerId, email, EmployeeId__c, IsActive, Approval_Manager__c,
                                          TeamCard_Level1_Mgr_ID__c, TeamCard_Level2_Mgr_ID__c,TeamCard_Level3_Mgr_ID__c, TeamCard_Level4_Mgr_ID__c
                                          from User where id in: managerIds];
           
            for(User user : managerList){
                usrManagerMap.put(user.id, user);
            }
            return usrManagerMap;
        
    }
    
    public static User getUserById(id userId){
        return userMap.get(userId);
    }
    
    public static void addUserToMap(User usr){
        userMap.put(usr.id,usr);
    }
    
    public static Id getMangerId(Billing_Adjustment__c ba, Id userId){
        id mgrId = userMap.get(userId).ManagerId;
        id aprvalmgrId = userMap.get(userId).Approval_Manager__c;

        if(aprvalmgrId !=null){
             return aprvalmgrId;
        }else if(mgrId != null){
            return mgrId;
        }else{
            if(ba!=null){
                ba.addError(Label.Manager_Not_Defined.replaceAll('<user>',userMap.get(userId).Name));
            }
            return null;
        }
    }
    
    
    public static User getManger(Billing_Adjustment__c ba, Id userId){
        id mgrId = userMap.get(userId).ManagerId;
        id aprvalmgrId = userMap.get(userId).Approval_Manager__c;

        if(aprvalmgrId !=null){
             return userMap.get(aprvalmgrId);
        }else if(mgrId != null){
            if(userMap.get(mgrId)!=null){
                return userMap.get(mgrId);
            }else{
                ba.addError('System Error: Manager not Loaded');
                throw new BillingAdjustmentHandlerHelper.BillingAdjustmentException('System Error: Manager not Loaded',ba);
                //return null;
            }    
        }else{
            if(ba!=null){
                system.debug('ba.Required_Release_Code__c'+ ba.Required_Release_Code__c);
                system.debug('ba.Approver_Release_Code__c'+ ba.Approver_Release_Code__c);
                if(ba.Status__c=='Draft' && ba.Approver_Release_Code__c==null){
                    throw new BillingAdjustmentHandlerHelper.BillingAdjustmentException(Label.Manager_Not_Defined.replaceAll('<user>',userMap.get(userId).Name),ba);
                }else if(Integer.valueOf(ba.Required_Release_Code__c) > Integer.valueOf(ba.Approver_Release_Code__c)){
                    throw new BillingAdjustmentHandlerHelper.BillingAdjustmentException(Label.Manager_Not_Defined.replaceAll('<user>',userMap.get(userId).Name),ba);
                }
            }
            return null;
        }
        
    }
    
     public static String getUserEmail(Id userId){
        User  usr = getUserById(userId);
        return usr != null? usr.email:'';
    }
    
    public static boolean isInUserHierarchy(User user,String lookupUserEmpId){
        if(lookupUserEmpId==null){
            return false;
        }
        String fieldTemplt = 'TeamCard_Level{i}_Mgr_ID__c';
        for(integer index=1; index<5;index++){
            String field = fieldTemplt.replace('{i}',String.valueOf(index));
            if(user.get(field)==lookupUserEmpId){
                return true;
            }
        }
        return false;
    }
    
    public static boolean hasUpdated(sObject c, String fieldName){
        if(oldMap == null){
            return true;
        }else{
            sObject oldObject = oldMap.get(c.ID);
            if (c.get(fieldName.toLowerCase()) != oldObject.get(fieldName.toLowerCase()))
                return true; 
            else
                return false;
        } 
    }
        
    public static boolean hasUpdatedAny(sObject c, List<String> fieldNames){
        if(oldMap == null){
            return true;
        }else{
            sObject oldObject = oldMap.get(c.ID);
            Set<boolean> resultCheck = new Set<Boolean>(); 
            for (String fieldName: fieldNames) {
                if(c.get(fieldName.toLowerCase()) != oldObject.get(fieldName.toLowerCase())){
                    resultCheck.add(true);
                }
            }
            return resultCheck.contains(true);
        }
    }
    
    public static boolean isApprovalProcessUpdate(Billing_Adjustment__c ba){
        if(ba.id==null){
            return  false;
        }
        if(ba.Approval_Status__c != null && ba.Approval_Status__c.toLowerCase().contains('pending') &&
          hasUpdated(ba,'Approval_Step_Number__c') && ba.Approval_Step_Number__c!=0){
            return true;       
          }else{
            return false;
          }
    }
    
    public static boolean isReminderNotificationUpdate(Billing_Adjustment__c ba){
        system.debug('New BA: ' + ba);
        system.debug('Old BA: ' + oldMap);
        if(ba.id==null || OldMap==null){
            return  false;
        }

        Billing_Adjustment__c oldBa = (Billing_Adjustment__c) OldMap.get(ba.id);
        system.debug('Check Values: oldba' + oldBa + ', Approver_Notifications__c: ' + oldBa.Approver_Notifications__c + 'Has been updated: ' + hasUpdated(ba, 'Approver_Notifications__c'));

        if(oldBa !=null && oldBa.Approver_Notifications__c!=null && hasUpdated(ba, 'Approver_Notifications__c')){
            return true;
        }else{
            return false;
        }
    }
    
    public static void loadAccountAndSalseAssinmentMap(){
        accountIdMap = getParentAccountMap(accountIdSet);
        salesAssingmentUserMap = getSalesAssignmentsMap(accountIdMap.values());
    }
    
    private static void checkFieldUpdates(Billing_Adjustment__c ba){
        if(!needReasonCodeLoad){
            needReasonCodeLoad =hasUpdatedAny(ba,new List<String>{'RC_Level_1__C','RC_Level_2__c','RC_Level_3__c','RC_Product__c','RC_Responsible_Area__c'});
        }
        if(!needAccountIdLoad){
            needAccountIdLoad = hasUpdatedAny(ba,new List<String>{'Account__c','Product_Type__c','Approval_Step_Number__c'});
        }
        if(!needReqReleaseCodeLoad){
            needReqReleaseCodeLoad = hasUpdatedAny(ba,new List<String>{'Amount__c','GST__c','HST__c','PST__c','Late_Payment_Charge__c'});
        }
        if(!needControllersLoad){
            needControllersLoad = hasUpdatedAny(ba,new List<String>{'Amount__c','GST__c','HST__c','PST__c','Late_Payment_Charge__c'});
        }
        if(!needApprovalThresholdLoad){
            needApprovalThresholdLoad = isApprovalProcessUpdate(ba);
        }
    }
    
    public class ReasonCodeDescription{
        public String Name {get;set;}
        public String FieldType {get;set;}
        
        public ReasonCodeDescription(String name, String fieldType){
            this.Name = name;
            this.FieldType = fieldType;
        }
        
        public Boolean equals(Object obj) {
            if (obj instanceof ReasonCodeDescription) {
                ReasonCodeDescription p = (ReasonCodeDescription)obj;
                return (name.toLowerCase().equals(p.name.toLowerCase())) && (fieldType.toLowerCase().equals(p.fieldType.toLowerCase()));
            }
            return false;
        }
        
        public Integer hashCode() {
            return (31 * Name.hashCode()) ^ FieldType.hashCode();
        }
    }
 }