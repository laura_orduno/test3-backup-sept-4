public class CapacityGraphData{
    public String uid;
    public String name;
    public Decimal allocationAmount;
    public String startDate;
    public String endDate;
        
    public CapacityGraphData(Id uid, String name, Decimal allocationAmount, Datetime startDate, Datetime endDate)
    {
        this.uid = String.valueOf(uid);
    	this.name = name;
        this.allocationAmount = allocationAmount != null ? allocationAmount : 0;
    	this.startDate = startDate.formatGmt('yyyy/MM/dd');
    	this.endDate = endDate.formatGmt('yyyy/MM/dd');
    }           
}