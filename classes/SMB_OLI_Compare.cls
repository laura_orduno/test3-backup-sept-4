/*
###########################################################################
# File..................: SMB_OLI_Compare
# Version...............: 29
# Created by............: Kanishk Prasad
# Created Date..........: 26/12/2013
# Last Modified by......: Puneet Khosla
# Last Modified Date....: 27/12/2013
# Description...........: This class is a wrapper for OpportunityLineItem object and is used for sorting OLI records on the basis of LineNo
#
# Copyright (c) 2000-2012. Astadia, Inc. All Rights Reserved.
#
# Created by the Astadia, Inc. Modification must retain the above copyright notice. 
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any commercial purpose, without fee, and without a written
# agreement from Astadia, Inc., is hereby forbidden. Any modification to source
# code, must include this paragraph and copyright.
#
# Permission is not granted to anyone to use this software for commercial uses.
#
# Contact address: 2839 Paces Ferry Road, Suite 350, Atlanta, GA 30339
# Company URL : http://www.astadia.com
###########################################################################
*/

global class SMB_OLI_Compare implements Comparable{

    public Integer lineNo;
    public OpportunityLineItem OLI;
    
    // Constructor 
    
    public SMB_OLI_Compare(OpportunityLineItem OLI) {
        this.OLI=OLI;
        this.lineNo= Integer.valueOf(OLI.LineNo__c);
    }
    
    // Implement the compareTo() method 
    
    global Integer compareTo(Object compareTo) {
        String strOLI=String.valueOf(compareTo);
        Integer fieldIndex=strOLI.indexOf('LineNo__c');
        Integer fieldEqualIndex=strOLI.indexOf('=',fieldIndex);
        Integer endFieldIndex=strOLI.indexOf(',',fieldIndex);
        
        Integer compareToLineNo=Integer.valueOf(strOLI.substring(fieldEqualIndex+1,endFieldIndex));
        System.debug('LINENO->'+compareToLineNo);
        
        
//        OpportunityLineItem compareToOLI = (OpportunityLineItem)compareTo;
        
        if (lineNo == compareToLineNo) return 0;
        if (lineNo > compareToLineNo) return 1;
        return -1;        
    }
}