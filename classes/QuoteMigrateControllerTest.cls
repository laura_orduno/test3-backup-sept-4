@isTest
private class QuoteMigrateControllerTest {
	private static testMethod void testQuoteMigrateController() {
		Opportunity o = new Opportunity(name = 'test', closedate = Date.today(), stagename = 'test');
		insert o;
		
		SBQQ__Quote__c q = new SBQQ__Quote__c(SBQQ__Opportunity__c = o.id);
		insert q;
		
		Product2 p = new Product2(name = 'test');
		Product2 p2 = new Product2(name = 'test2');
		insert new Product2[] { p, p2 };
		
		SBQQ__QuoteLine__c ql = new SBQQ__QuoteLine__c(SBQQ__Quote__c = q.id, SBQQ__Product__c = p.id);
		insert ql;
		
		// no params, simply returns
		QuoteMigrateController ctlr = new QuoteMigrateController();
		
		ApexPages.currentPage().getParameters().put('qid', q.id);
		ApexPages.currentPage().getParameters().put('messages', 'message1|message2');
		ctlr = new QuoteMigrateController();
		
		// no migration route
		PageReference ref = ctlr.onMigrate();
		system.assertEquals(null, ref);
		
		// with migration route
		Quote_Migration_Route__c mr = new Quote_Migration_Route__c(
			Source_Product__c = p.id,
			Destination_Product__c = p2.id
		);
		insert mr;
		
		ctlr = new QuoteMigrateController();
		ref = ctlr.onMigrate();
		system.assertNotEquals(null, ref);
		
		PageReference cancelRef = ctlr.onCancel();
	}
}