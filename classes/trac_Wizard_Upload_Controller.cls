public without sharing class trac_Wizard_Upload_Controller {
  
  public Integer numAttachments {
  	get{
      debugMessage = 'SELECT Count() FROM Attachment WHERE ParentId = : theID';
      numAttachments = database.countQuery(debugMessage);
      return numAttachments;
    }
    set;
  }
  public ID theID {get; set;}
  public String debugMessage {get; set;}
  
  public trac_Wizard_Upload_Controller() {
  	theID = ApexPages.currentPage().getParameters().get('ID'); // the record the file is attached to
  }
  
  public Attachment newUpload {
    get {
        if (newUpload == null)
        newUpload = new Attachment();
        return newUpload;
      }
    set;
  }
  
  public void doUpload() {
      
	  System.Savepoint dataSavePoint = Database.setSavepoint();
	  try {
	    newUpload.OwnerId = UserInfo.getUserId();
	    newUpload.ParentId = ApexPages.currentPage().getParameters().get('ID'); // the record the file is attached to
	    newUpload.IsPrivate = false;
	    insert newUpload;
	    newUpload = new Attachment();
	    
	  } catch (Exception e) {
	    Database.rollback(dataSavePoint);
	    ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
	  }
  }
  
}