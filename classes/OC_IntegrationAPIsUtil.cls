/* This class provides Integration APIs for CGTA,Clearance Path, DropInfo, Rateband Services .
    Developer: Aditya Jamwal (IBM)
    Created Date: 26-Sept-2017
*/
global with sharing class OC_IntegrationAPIsUtil implements vlocity_cmt.VlocityOpenInterface2{
   
    global Object invokeMethod(String methodName, Map<String, Object> input, Map<String, Object> output, Map<String, Object> options)  {

        if (methodName.indexOf('getProductsTechnicalAvailability') != -1) {
            output.put('GTARequestFailed',false);
            getProductsTechnicalAvailability(input, output, options);
           
        }
        if (methodName.indexOf('getRateGroupInfo') != -1) {
            output.put('RateGroupInfoRequestFailed',false);
            getRateGroupInfo(input, output, options);
        }
        if (methodName.indexOf('getGponDropStatus') != -1) {
            output.put('DropInfoStatusRequestFailed',false);
            getGponDropStatus(input, output, options);
        }
        if (methodName.indexOf('getServicePath') != -1) {
           output.put('ClearancePathRequestFailed',false);
           getServicePath(input, output, options);
        }
        if (methodName.indexOf('getPortabilityCheck') != -1) {
           output.put('PortabilityCheckRequestFailed',false);
           getPortabilityCheck(input, output, options);
        }
        return null;
    }
    
    @RemoteAction
    public static  Map<String, Object>  getProductsTechnicalAvailability(ServiceAddressData SAData){
     Map<string,Object> inputMap = new Map<string,Object>();
     Map<string,Object> output = new Map<string,Object>();

    if(null != SAData){
        try{ 
     inputMap = constructServiceAddress(SAData);
     output.putAll(getRateGroupInfo(SAData.ratingNpaNxx));
     output.putAll(getGponDropStatus(SAData.locationId));

     if(String.isNotBlank((String) output.get('rateBand'))){
        inputMap.put(OrdrConstants.ADDR_RATE_BAND, (String) output.get('rateBand'));
     }

     if(null != output.get('isForborne')){
     inputMap.put(OrdrConstants.ADDR_FORBORNE, (Boolean)output.get('isForborne') ? 'Yes' : 'No');
     }
     if(null != output.get('isFIFA')){
     inputMap.put(OrdrConstants.ADDR_ISFIFA, (Boolean) output.get('isFIFA') ? 'Yes' : 'No');
     }
     if(null != output.get('isDropReady')){
     inputMap.put(OrdrConstants.ADDR_ISDROPREADY, (Boolean)output.get('isDropReady') ? 'Yes' : 'No');
     }

     TpFulfillmentResourceOrderV3.ResourceOrder resourceOrder = OrdrTechnicalAvailabilityWsCallout.checkProductTechnicalAvailability(inputMap);

    
      List<OrdrTechnicalAvailabilityManager.ProductCategory> productCategories = OrdrTechnicalAvailabilityManager.mapTechnicalAvailabilityResponse(resourceOrder);
        output.put('productCategories',productCategories);
     }catch(exception ex){
        output.put('GTARequestFailed',true);
        output.put('GTAException',ex.getStackTraceString());
        system.debug('!@GTAException '+ex.getStackTraceString());
     }     
    }else{
       output.put('GTARequestFailed',true);
       output.put('GTAException','Service Address data is not provided to initate Check GTA call.');  
    }
     return output;
    }

    @RemoteAction
    public static  Map<String, Object>  getPortabilityCheck(ServiceAddressData SAData,String phoneNumber){
     Map<string,Object> inputMap = new Map<string,Object>();
     Map<string,Object> output = new Map<string,Object>();

    if(null != SAData && String.isNotBlank(phoneNumber)){

         system.debug('PHONE-UTILS: '+ phoneNumber);
         system.debug('SAData-UTILS: '+ SAData);
        system.debug('SAData-UTILS '+ SAData.fmsId);
        system.debug('SAData-UTILS locationId '+ SAData.locationId);
        system.debug('SAData-UTILS province '+ SAData.province);
        system.debug('SAData-UTILS coid '+ SAData.coid);
        system.debug('SAData-UTILS  municipalityNameLPDS '+ SAData.municipalityNameLPDS);
        system.debug('SAData-UTILS  postalCode '+ SAData.postalCode);
        system.debug('SAData-UTILS  streetName_fms '+ SAData.streetName_fms);
        system.debug('SAData-UTILS  ratingNpaNxx '+ SAData.ratingNpaNxx);
    try{ 

        inputMap = constructServiceAddress(SAData);  
        inputMap.put(OrdrConstants.TN, phoneNumber);
        TpFulfillmentResourceOrderV3.ResourceOrder resourceOrder = OrdrTechnicalAvailabilityWsCallout.checkTnPortability(OrdrConstants.WIRELINE, inputMap);
        OrdrTechnicalAvailabilityManager.TnPortability TnPortability = OrdrTechnicalAvailabilityManager.mapTnPortabilityResponse(resourceOrder);

        output.put('TnPortability',TnPortability);
     }catch(exception ex){
        output.put('PortabilityCheckRequestFailed',true);
        String message = ex.getMessage();
         if(String.isNotBlank(message) && message.containsIgnoreCase('Technical Availability Web Service received an unexpected error')){
            message = 'The Check Phone Portability Web Service received an unexpected error. Please contact your system administrator.'; 
         }
        output.put('PortabilityCheckException',message);
        system.debug('!@PortabilityCheckException '+ex.getStackTraceString());
     }     
    }else{
       output.put('PortabilityCheckRequestFailed',true);
       if(null == SAData ){
         output.put('PortabilityCheckException','Service Address data or PhoneNumber is not provided to initate Portability Check call.');
       }
       if(String.isNotBlank(phoneNumber)){
         output.put('PortabilityCheckException','PhoneNumber is not provided to initate Portability Check call.'); 
       }
         
    }
     return output;
    }

    @RemoteAction
    public static Map<String, Object> getGponDropStatus(String locationId){
        Map<string,Object> output = new Map<string,Object>();
        if(String.isNotBlank(locationId)){
        OrdrTechnicalAvailabilityManager.GponDropStatus dropStatus = new OrdrTechnicalAvailabilityManager.GponDropStatus();

        Map<String, String> inputMap = new Map<String, String>();
        inputMap.put(OrdrConstants.LOCATION_ID, locationId);
        OrdrTnResQryResConfMsg.ResourceConfigurationMessage resourceConfig;
        try{
        resourceConfig = OrdrQueryResourceWsCallout.queryResource(inputMap);
        }catch(exception ex){
            output.put('DropInfoStatusRequestFailed',true);
            output.put('DropInfoStatusException',ex.getMessage());
            system.debug('!!@@ exception '+ex.getMessage());
        }
        if (resourceConfig != null) {
            for (TpInventoryResourceConfigV3.PhysicalResource physicalResource : resourceConfig.physicalResource) {
              //  system.debug(' c '+physicalResource.CharacteristicValue);
                if (physicalResource.CharacteristicValue != null) {
                    for (TpCommonBaseV3.CharacteristicValue charVal : physicalResource.CharacteristicValue) {
                         system.debug(' physicalResource charVal '+charVal.Characteristic.Name +' '+charVal.Value[0]);
                        if (charVal.Characteristic.Name == 'fsa') dropStatus.fsa = charVal.Value[0];
                        if (charVal.Characteristic.Name == 'dropType') dropStatus.dropType = charVal.Value[0];
                        if (charVal.Characteristic.Name == 'dropPermission') dropStatus.dropPermission = charVal.Value[0];
                        if (charVal.Characteristic.Name == 'dropStatus') dropStatus.dropStatus = charVal.Value[0];
                        if (charVal.Characteristic.Name == 'fsaReadyDate') dropStatus.fsaReadyDate = charVal.Value[0];
                        if (charVal.Characteristic.Name == 'coid') dropStatus.coid = charVal.Value[0];
                        if (charVal.Characteristic.Name == 'gisId') dropStatus.gisId = charVal.Value[0];
                        if (charVal.Characteristic.Name == 'roeStatus') dropStatus.roeStatus = charVal.Value[0];
                        if (charVal.Characteristic.Name == 'premiseType') dropStatus.premiseType = charVal.Value[0];
                    }
                }
            }

           output.put('dropStatus',dropStatus);
           output.put('isFIFA',String.isNotBlank(dropStatus.fsaReadyDate));
           output.put('isDropReady',dropStatus.dropStatus.equalsIgnoreCase('Constructed')); 
        }  
        }else{
            output.put('DropInfoStatusRequestFailed',true);
            output.put('DropInfoStatusException','location Id is not provided.');
        }
        system.debug('static output '+output);
        return output;

    }

    @RemoteAction
    public static  Map<String, Object>  getRateGroupInfo(String npa_nxx){
        Map<string,Object> output = new Map<string,Object>();
        
        if(String.isNotBlank(npa_nxx)){
        OrdrProductEligibilityManager.RateGroupInfo rateGroupInfo = new OrdrProductEligibilityManager.RateGroupInfo();

        RateGroupInfo_ResourceRes.RateGroupAndForborneAttribute attribute;
        
        try{

        attribute = OrdrRateGroupInfoWsCallout.getRateGroupAndForborneInformationByNpaNxx(npa_nxx);

        }catch(exception ex){
          output.put('RateGroupInfoRequestFailed',true);
          output.put('RateGroupInfoException',ex.getMessage());
          system.debug('!!@@ exception '+ex.getMessage());
        }

        if(null != attribute ){
         RateGroupInfo_ResourceRes.RateGroup rateGroup = attribute.rateGroupAttribute.rateGroupHistoryList.rateGroupList[0];
        
         rateGroupInfo.rateGroupAttribute = rateGroup.rateGroupAttribute;
         rateGroupInfo.rateBand = rateGroup.rateBand;
         rateGroupInfo.rateSubBand = rateGroup.rateSubBand;
         rateGroupInfo.isForborne = attribute.exchangeForborneStatus.exchangeForborneStatusInd;
         output.put('isForborne',rateGroupInfo.isForborne);
         output.put('rateBand',rateGroupInfo.rateBand);
         output.put('rateGroupInfo',rateGroupInfo);
         }
        }else{
            output.put('RateGroupInfoRequestFailed',true);
            output.put('RateGroupInfoException','npa/nxx is not provided.');
        }
        return output;

    }

    @RemoteAction
    public static Map<String, Object> getServicePath(String fmsId,String municipalityName,String province,String coid){

       Map<string,Object> output = new Map<string,Object>();
     if(!(String.isBlank(fmsId) || String.isBlank(municipalityName) || String.isBlank(province) || String.isBlank(coid))){
        List<OrdrTechnicalAvailabilityManager.ServicePath> servicePaths = new List<OrdrTechnicalAvailabilityManager.ServicePath>();

        Map<String, String> inputMap = new Map<String, String>();
        inputMap.put(OrdrConstants.FMS_ID,fmsId);
        inputMap.put(OrdrConstants.ADDR_MUNICIPALITY,municipalityName);
        inputMap.put(OrdrConstants.ADDR_PROVINCE_CODE,province);
        inputMap.put(OrdrConstants.COID,coid);
        inputMap.put(OrdrConstants.USER_ID, OrdrUtilities.getCurrentUserTelusId());

        OrdrTnResourceInfoRetrievalConfiguration.ResourceConfigurationCollectionMessage resourceConfig ;

        try{
             resourceConfig = OrdrServicePathWsCallout.findResource(inputMap);
        }catch(exception ex){
            output.put('ClearancePathRequestFailed',true);
            output.put('ClearancePathException',ex.getMessage());
            system.debug('!!@@ exception '+ex.getMessage());
        }
        if (resourceConfig != null) {
            for (TpInventoryResourceConfigV3.PhysicalResource physicalResource : resourceConfig.physicalResource) {
                OrdrTechnicalAvailabilityManager.ServicePath servicePath = new OrdrTechnicalAvailabilityManager.ServicePath();
                if (physicalResource.CharacteristicValue != null) {
                    for (TpCommonBaseV3.CharacteristicValue charVal : physicalResource.CharacteristicValue) {
                        if (charVal.Characteristic.Name == 'primaryTelephoneNumber') servicePath.primaryTelephoneNumber = charVal.Value[0];
                        if (charVal.Characteristic.Name == 'customerName') servicePath.customerName = charVal.Value[0];
                        if (charVal.Characteristic.Name == 'clearanceDate') servicePath.clearanceDate = charVal.Value[0];
                        if (charVal.Characteristic.Name == 'serviceTypeCode') servicePath.serviceTypeCode = charVal.Value[0];
                        if (charVal.Characteristic.Name == 'servicePathStatusCode') servicePath.servicePathStatusCode = charVal.Value[0];
                        if (charVal.Characteristic.Name == 'cablePairDedicatedPlantCode') servicePath.cablePairDedicatedPlantCode = charVal.Value[0];
                        if (charVal.Characteristic.Name == 'leasedLoopFlag') servicePath.leasedLoopFlag = charVal.Value[0];
                    }
                    servicePaths.add(servicePath);
                }
            }
            output.put('servicePaths',servicePaths);
        }        
        }else{
            output.put('ClearancePathRequestFailed',true);
            output.put('ClearancePathException','FMS Id,Muncipality Name,Province or COID is not provided.');
        }
        return output;
    }

    private void getProductsTechnicalAvailability(Map<String, Object> input, Map<String, Object> output, Map<String, Object> options){

        Map<String, String> inputMap = new Map<String, String>();         
        ServiceAddressData SAData = new ServiceAddressData();

        system.debug('getPRODTECH: '+input);

        String SData = (String) input.get('fmsId');

        system.debug('getPRODTECH: '+input);

        SAData.fmsId = (String) input.get('fmsId');
        SAData.locationId  = (String) input.get('locationId');
        SAData.province = (String) input.get('province');      
        SAData.coid = (String) input.get('coid');
        SAData.municipalityNameLPDS = (String) input.get('municipalityName');
        SAData.postalCode  = (String) input.get('postalCode');
        SAData.streetDirection = (String) input.get('streetDirection');
        SAData.streetTypeSuffix = (String) input.get('streetTypeSuffix');
        SAData.streetTypePrefix  = (String) input.get('streetTypePrefix');
        SAData.streetName_fms = (String) input.get('streetName_fms');
        SAData.streetName = (String) input.get('streetName');
        SAData.street  = (String) input.get('street');
        SAData.buildingNumber = (String) input.get('buildingNumber'); 
        SAData.suiteNumber = (String) input.get('suiteNumber');
        SAData.ratingNpaNxx = (String) input.get('npa_nxx');      
		system.debug('getPRODTECH__: '+SAData.fmsId);
       output.putAll(getProductsTechnicalAvailability(SAData));        
    }

    private void getRateGroupInfo(Map<String, Object> input, Map<String, Object> output, Map<String, Object> options){
        system.debug('npa_nxx '+input);
        String npa_nxx = (String) input.get('npa_nxx');
        output.putAll(getRateGroupInfo(npa_nxx));
    }

    private void getServicePath(Map<String, Object> input, Map<String, Object> output, Map<String, Object> options){
        system.debug('npa_nxx '+input);
        String FMS_ID = (String) input.get('fmsId');
        String ADDR_MUNICIPALITY = (String) input.get('municipalityName');
        String ADDR_PROVINCE = (String) input.get('province');
        String COID = (String) input.get('coid');
        output.putAll(getServicePath(FMS_ID,ADDR_MUNICIPALITY,ADDR_PROVINCE,COID));

    }

    private void getGponDropStatus(Map<String, Object> input, Map<String, Object> output, Map<String, Object> options){
        system.debug('npa_nxx '+input);
        String locationId = (String) input.get('locationId');
        output.putAll(getGponDropStatus( locationId));
        system.debug('output '+output);

    }

    private void getPortabilityCheck(Map<String, Object> input, Map<String, Object> output, Map<String, Object> options){

        Map<String, String> inputMap = new Map<String, String>();        
        String phoneNumber = (String) input.get('phoneNumber');

        system.debug('getPortINPUT'+input.values());

        system.debug('getPortPHONE'+phoneNumber);

        ServiceAddressData SAData = new ServiceAddressData();

        SAData.fmsId = (String) input.get('fmsId');
        SAData.locationId  = (String) input.get('locationId');
        SAData.province = (String) input.get('province');      
        SAData.coid = (String) input.get('coid');
        SAData.municipalityNameLPDS = (String) input.get('municipalityName');
        SAData.postalCode  = (String) input.get('postalCode');
        SAData.streetDirection = (String) input.get('streetDirection');
        SAData.streetTypeSuffix = (String) input.get('streetTypeSuffix');
        SAData.streetTypePrefix  = (String) input.get('streetTypePrefix');
        SAData.streetName_fms = (String) input.get('streetName_fms');
        SAData.streetName = (String) input.get('streetName');
        SAData.street  = (String) input.get('street');
        SAData.buildingNumber = (String) input.get('buildingNumber'); 
        SAData.suiteNumber = (String) input.get('suiteNumber');
        SAData.ratingNpaNxx = (String) input.get('npa_nxx');

        system.debug('getPortSADATA '+ SAData.fmsId);
        system.debug('getPortSADATA locationId '+ SAData.locationId);
        system.debug('getPortSADATA province '+ SAData.province);
        system.debug('getPortSADATA coid '+ SAData.coid);
        system.debug('getPortSADATA  municipalityNameLPDS '+ SAData.municipalityNameLPDS);
        system.debug('getPortSADATA  postalCode '+ SAData.postalCode);
        system.debug('getPortSADATA  streetName_fms '+ SAData.streetName_fms);
        system.debug('getPortSADATA  ratingNpaNxx '+ SAData.ratingNpaNxx);

        output.putAll(getPortabilityCheck(SAData,phoneNumber));    

        system.debug('output '+output);

    }

    public static Map<String, String> constructServiceAddress(ServiceAddressData serviceAddress) {
      
        Map<String, String> addressInfo = new Map<String, String>();

        if (null != serviceAddress) {
                 addressInfo.put(OrdrConstants.ADDRESS_TYPE, OrdrConstants.ADDRESS_TYPE_STANDARD);
            if (String.isNotBlank(serviceAddress.fmsId)){ addressInfo.put(OrdrConstants.FMS_ID, serviceAddress.fmsId.leftPad(9, '0'));}
            if (String.isNotBlank(serviceAddress.locationId)){ addressInfo.put(OrdrConstants.LOCATION_ID, serviceAddress.locationId); }

           system.debug('constructServiceAddress::serviceAddress.locationId:'+serviceAddress.locationId);
           system.debug('constructServiceAddress::serviceAddress.fmsId:'+serviceAddress.fmsId);
            if (String.isNotBlank(serviceAddress.province)){ 

                addressInfo.put(OrdrConstants.ADDR_PROVINCE_CODE, serviceAddress.province);
                Boolean isILEC = false;
                if (Country_Province_Codes__c.getValues(serviceAddress.province) != null && Country_Province_Codes__c.getValues(serviceAddress.province).is_ILEC__c) {
                    isILEC = true;
                } else {
                    isILEC = false;
                }
                addressInfo.put(OrdrConstants.IS_ILEC, isILEC ? 'Yes' : 'No');
            }
            if (String.isNotBlank(serviceAddress.coid)){ addressInfo.put(OrdrConstants.COID, serviceAddress.coid);}                    
            if (String.isNotBlank(serviceAddress.municipalityNameLPDS)) addressInfo.put(OrdrConstants.ADDR_MUNICIPALITY, serviceAddress.municipalityNameLPDS);
            if (String.isNotBlank(serviceAddress.postalCode)) addressInfo.put(OrdrConstants.ADDR_POSTAL_CODE, serviceAddress.postalCode);
            if (String.isNotBlank(serviceAddress.streetDirection)) addressInfo.put(OrdrConstants.ADDR_STREET_DIRECTION, serviceAddress.streetDirection);

            String streetType = 'Other';
            if(String.isNotBlank(serviceAddress.streetTypeSuffix)){
                streetType = serviceAddress.streetTypeSuffix;                     
            }else if (String.isNotBlank(serviceAddress.streetTypePrefix)) {
                streetType = serviceAddress.streetTypePrefix;
            }

            if (String.isNotBlank(streetType)) addressInfo.put(OrdrConstants.ADDR_STREET_TYPE, streetType);

            String streetName = '';

           system.debug('constructServiceAddress::serviceAddress.streetName_fms:'+serviceAddress.streetName_fms);
            if(string.isNotBlank(serviceAddress.streetName_fms)){
               streetName = serviceAddress.streetName_fms;
            }
            else if(string.isNotBlank(serviceAddress.streetName)){
               streetName = serviceAddress.streetName;
            }
            else if(string.isNotBlank(serviceAddress.street)){
               streetName = serviceAddress.street;
            }

            if (String.isNotBlank(streetName)) addressInfo.put(OrdrConstants.ADDR_STREET_NAME, streetName);
            if (String.isNotBlank(serviceAddress.buildingNumber)) addressInfo.put(OrdrConstants.ADDR_STREET_NUMBER, serviceAddress.buildingNumber);
            if (String.isNotBlank(serviceAddress.suiteNumber)) addressInfo.put(OrdrConstants.ADDR_UNIT_NUMBER, serviceAddress.suiteNumber);      
            system.debug('constructServiceAddress::streetName:'+streetName);     
        }

           system.debug('constructServiceAddress::addressInfo:'+addressInfo);
        return addressInfo;
    }
    

}