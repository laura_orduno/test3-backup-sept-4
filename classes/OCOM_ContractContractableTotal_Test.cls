@isTest
private class OCOM_ContractContractableTotal_Test {
	
	@isTest static void test_method_one() {
		TestDataHelper.OCOM_testContractLineItemListCreation_BA();
            vlocity_cmt__ContractLineItem__c v1 =  new vlocity_cmt__ContractLineItem__c(vlocity_cmt__Product2Id__c=TestDataHelper.testProductObj.id,TELUS_Printed_Product_Name__c = TestDataHelper.testProductObj.Name,vlocity_cmt__RecurringCharge__c = 10,
                                                                                        vlocity_cmt__ContractId__c=TestDataHelper.testContractObj.id,vlocity_cmt__LineNumber__c = '0001',
                                                                                        vlocity_cmt__OneTimeTotal__c = 5,
                                                                                        name='Name123',vlocity_cmt__Quantity__c=12);
            insert v1;
            vlocity_cmt__ContractLineItem__c v2 =  new vlocity_cmt__ContractLineItem__c(vlocity_cmt__Product2Id__c=TestDataHelper.testProductObj.id,TELUS_Printed_Product_Name__c = TestDataHelper.testProductObj.Name,vlocity_cmt__RecurringCharge__c = 20,
                                                                                        vlocity_cmt__ContractId__c=TestDataHelper.testContractObj.id,vlocity_cmt__LineNumber__c = '0001.0001'
                                                                                        ,vlocity_cmt__OneTimeTotal__c = 0,
                                                                                        name='Name123',vlocity_cmt__Quantity__c=24);

			insert v2;
            vlocity_cmt__ContractLineItem__c v3 =  new vlocity_cmt__ContractLineItem__c(vlocity_cmt__Product2Id__c=TestDataHelper.testProductObj.id,TELUS_Printed_Product_Name__c = TestDataHelper.testProductObj.Name,vlocity_cmt__RecurringCharge__c = 30,
                                                                                        vlocity_cmt__ContractId__c=TestDataHelper.testContractObj.id,vlocity_cmt__LineNumber__c = '0002',
                                                                                        	vlocity_cmt__OneTimeTotal__c = 5,
                                                                                        name='Name123',vlocity_cmt__Quantity__c=12);

			insert v3;
			List<Id> contractListids = new List<Id>{TestDataHelper.testContractObj.id};
			Test.startTest();
			OCOM_ContractContractableTotal.setContractTotalMRCNRC(contractListids);
			Test.stopTest();
	}
	
	@isTest static void test_method_two() {
		TestDataHelper.OCOM_testContractLineItemListCreation_BA();
            vlocity_cmt__ContractLineItem__c v1 =  new vlocity_cmt__ContractLineItem__c(vlocity_cmt__Product2Id__c=TestDataHelper.testProductObj.id,TELUS_Printed_Product_Name__c = TestDataHelper.testProductObj.Name,vlocity_cmt__RecurringCharge__c = 10,
                                                                                        vlocity_cmt__ContractId__c=TestDataHelper.testContractObj.id,vlocity_cmt__LineNumber__c = '0001',
                                                                                        vlocity_cmt__OneTimeTotal__c = 5,
                                                                                        name='Name123',vlocity_cmt__Quantity__c=12);
            insert v1;
            vlocity_cmt__ContractLineItem__c v2 =  new vlocity_cmt__ContractLineItem__c(vlocity_cmt__Product2Id__c=TestDataHelper.testProductObj.id,TELUS_Printed_Product_Name__c = TestDataHelper.testProductObj.Name,vlocity_cmt__RecurringCharge__c = 20,
                                                                                        vlocity_cmt__ContractId__c=TestDataHelper.testContractObj.id,vlocity_cmt__LineNumber__c = '0001.0001'
                                                                                        ,vlocity_cmt__OneTimeTotal__c = 0,
                                                                                        name='Name123',vlocity_cmt__Quantity__c=24);

			insert v2;
            vlocity_cmt__ContractLineItem__c v3 =  new vlocity_cmt__ContractLineItem__c(vlocity_cmt__Product2Id__c=TestDataHelper.testProductObj.id,TELUS_Printed_Product_Name__c = TestDataHelper.testProductObj.Name,vlocity_cmt__RecurringCharge__c = 30,
                                                                                        vlocity_cmt__ContractId__c=TestDataHelper.testContractObj.id,vlocity_cmt__LineNumber__c = '0002',
                                                                                        	vlocity_cmt__OneTimeTotal__c = 5,
                                                                                        name='Name123',vlocity_cmt__Quantity__c=12);

			insert v3;
			List<Id> contractListids = new List<Id>{TestDataHelper.testContractObj.id};
			Test.startTest();
			OCOM_ContractContractableTotal.caluclateMRCNRC(contractListids);
			Test.stopTest();
		
	}
	
}