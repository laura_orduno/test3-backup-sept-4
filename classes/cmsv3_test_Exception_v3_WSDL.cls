/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class cmsv3_test_Exception_v3_WSDL {
static testMethod void cmsv3_Exceptions_v3(){
    cmsv3_Exceptions_v3  smb11 = new cmsv3_Exceptions_v3();
        cmsv3_Exceptions_v3.Message z11 = new cmsv3_Exceptions_v3.Message();
        cmsv3_Exceptions_v3.FaultExceptionDetailsType z12 = new cmsv3_Exceptions_v3.FaultExceptionDetailsType();
        cmsv3_Exceptions_v3.MessageList z13= new cmsv3_Exceptions_v3.MessageList();
    }
}