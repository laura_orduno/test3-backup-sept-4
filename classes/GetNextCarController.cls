public class GetNextCarController {
    
    Boolean skillsetNotdefined = false;
    Boolean isManager = false;
    
    Static ID userId; 
    Static ID assigningId;  
    Static ID userRoleId;
    
    Static String queryStringGeneral;
    Static String querystringSkillset;
    Static String querystringAgentOrManager;
            
    Static String firstPriority = ' and Queue_Priority__c = 1 ';  
    Static String secondPriority = ' and Queue_Priority__c = 2 ';        
    Static String thirdPriority = ' and Queue_Priority__c = 3 ';       
    Static String fourthPriority = ' and Queue_Priority__c = 4 ';              
    Static String fifthPriority = '';         
    
    /*---------- constructor method ------------------------------------------*/
    public GetNextCarController(ApexPages.StandardController stdController) {       
        initMe(); 
    }    
    
    public GetNextCarController() {      
        initMe();  
    }   
    
    private void initMe(){    
        System.debug('...dyy GetNextCarController ...');    
        userId = UserInfo.getUserid();         
        userRoleId = UserInfo.getUserRoleId(); 
        assigningId = userId;
        querystringAgentOrManager = getQuerystringAgentOrManager();
        queryStringGeneral = 'select '
            + ' Id, OwnerId, recordtype.name, car_status__c ' 
            + ' from Credit_Assessment__c ' 
            + ' where OwnerId<>null '
            + ' and (Follow_Up_Date__c <= TODAY or Follow_Up_Date__c = null) '
            + querystringAgentOrManager
            ;
        System.debug('...dyy userId ...' + userId); 
        System.debug('...dyy queryStringGeneral ...' + queryStringGeneral);  
        System.debug('...dyy userRoleId ...' + userRoleId);
        System.debug('...dyy isCreditManager ...' + isCreditManager()); 
    }
    
    @TestVisible 
    private String getQuerystringAgentOrManager(){
        String queryString;
        if(isCreditManager()) {
            queryString = ' and CAR_Status__c in (\'Escalated\') and Queue_Assigned__c = false ';
        }else if(isManagerForReview()) {
            queryString = getIsManagerForReviewQueryString();
        } else {
            queryString = ' and CAR_Status__c in (\'Submitted\',\'Queued\') and Queue_Assigned__c = false ';
            queryString += getQuerystringSkillset();
        }
        return queryString;
    }
    
    
    @TestVisible 
    private String getIsManagerForReviewQueryString(){
        String queryString;
        queryString = ' and ( car_code__c in (\'Manager Review Requested\')';
        queryString += ' or CAR_Status__c in (\'Submitted\',\'Queued\')) and Queue_Assigned__c = false ';
        queryString += getQuerystringSkillset();
        return queryString;
    }
    
    
    private Boolean isManagerForReview(){
        Boolean isManagerForReview = false;
        Credit_Skill_Set__c[] userSkillSets;
        try{
            String queryString = 'select id,Assessment_Type__c,Business_Unit__c,Maximum__c '
                + ' from Credit_Skill_Set__c where Name = \'Manager (For Review)\' '
                + ' and id in (select Work_Queue_Definition__c '
                + ' from Credit_Skill_Set_Assignment__c '
                + ' where Credit_Agent__c = \'' + userId + '\''+')';
            System.debug('... dyy ... isManagerForReview ... ' + queryString);
            userSkillSets = Database.query(queryString);  
        }catch(Exception e){
            System.debug('... dyy ... error when getUserSkillSet ... ' + e);
        }         
        if(userSkillSets!=null &&userSkillSets.size()>0) isManagerForReview = true;        
        return isManagerForReview;
    }
    
    private Boolean isCreditManager(){
        /*
        UserRole userRole = [select 
                             id
                             , developerName
                             from userRole 
                             where 
                             id = :userRoleId];
        System.debug('...dyy userRole ...' + userRole.DeveloperName);  
        isManager= ('Credit_Manager'==userRole.DeveloperName);*/
        try{
            Groupmember[] memebers = [
                select id, groupId, userorgroupID 
                from groupmember 
                where groupid in (select id from group where developername ='Credit_Managers')
                and userorgroupID = :userId
            ];            
            if (memebers!=null &&memebers.size()>0) isManager = true;              
        }catch(Exception e){
            System.debug('...error occured ...' + e );  
        }
        return isManager;
    }
    
    /*---------- primary method --------------------------------*/
    
    
    public PageReference refresh(){        
        System.debug('...dyy refresh ...');  
        getAllCarsForSameCustomer();   
        return redirect();
    }
    
    
    
    @TestVisible 
    private PageReference setSkillsetNotdefined(){ 
            ApexPages.addmessage(
                new ApexPages.message(
                    ApexPages.severity.WARNING
                    ,Label.Not_Assigned_To_A_Skill_Set));
            skillsetNotdefined = false;
            return null;     
    }
    
    public PageReference getNextCar(){
        if(skillsetNotdefined) {     
            return setSkillsetNotdefined();
        }
        
        Boolean recordFound = false;
        if (!recordFound) recordFound = getAllCarsForSameCustomer();   
        if (!recordFound) recordFound = getNextOneCar(firstPriority);
        if (!recordFound) recordFound = getNextOneCar(secondPriority);
        if (!recordFound) recordFound = getNextOneCar(thirdPriority);
        if (!recordFound) recordFound = getNextOneCar(fourthPriority);
        if (!recordFound) recordFound = getNextOneCar(fifthPriority); 
        if (!recordFound){
            ApexPages.addmessage(
                new ApexPages.message(
                    ApexPages.severity.WARNING
                    ,Label.No_more_CARs_found));
            return null;
        }
        
        return redirect();
    }
    
    @TestVisible 
    private void bulkAssign(Credit_Assessment__c[] cars){ 
        for(Credit_Assessment__c theCar:cars) assignCar(theCar);  
        update cars; 
    }
    
    @TestVisible 
    private void singleAssign(Credit_Assessment__c theCar){ 
        assignCar(theCar);   
        update theCar; 
    }
    
    @TestVisible 
    private void assignCar(Credit_Assessment__c theCar){ 
        theCar.Queue_Assigned__c = true;        
        theCar.OwnerId = assigningId;
        if ((isManager && theCar.CAR_Status__c !='Escalated') 
            || !isManager) {
                theCar.CAR_Status__c = 'In Progress'; 
                theCar.CAR_Code__c = 'Agent Review';  
            }
    }
    
    public PageReference redirect(){
        PageReference p = new PageReference(getReturnUrl());            
        p.setRedirect(true);
        return p;
    }
    
    /*-------------------------------------------------------------------*/
    
    private Boolean getAllCarsForSameCustomer(){ 
        String queryString = queryStringGeneral + getQuerystringForSameCustomer();  
        System.debug('... dyy ... getAllCarsForSameCustomer : ' + queryString);              
        Credit_Assessment__c[] cars = getCars(queryString); 
        Boolean carsFound = (cars!=null&&cars.size()!=0);
        if (carsFound) bulkAssign(cars);
        return carsFound;
    }    
    
    private Boolean getNextOneCar(String priorityQuerystring){ 
        System.debug('dyy ... getNextOneCar ...');
        String queryString = queryStringGeneral
            + priorityQuerystring
            + ' order by CreatedDate limit 1 ';              
        Credit_Assessment__c theCar = getCar(queryString);  
        Boolean carFound = (theCar!=null);
        if(carFound){
            singleAssign(theCar);
            getAllCarsForSameCustomer();
        } 
        return carFound;
    }
    
    private Credit_Assessment__c[] getCars(String queryString){        
        Credit_Assessment__c[] cars;  
        System.debug('dyy ... queryString = ' + queryString);
        try{
            cars = Database.query(queryString);
        } catch(Exception e){
            System.debug('... no record found ...');
        }        
        return cars;
    }
    
    private Credit_Assessment__c getCar(String queryString){        
        Credit_Assessment__c theCar;  
        System.debug('dyy ... queryString = ' + queryString);
        try{
            theCar = Database.query(queryString);
        } catch(Exception e){
            System.debug('... no record found ...');
        }        
        return theCar;
    }
    
    /*------- Skill set determination -----------------*/ 
    
    private Credit_Skill_Set__c getUserSkillSet(){
        System.debug('dyy ... getUserSkillSet ...' );        
        System.debug('dyy ... userId = ' + userId );
        
        Credit_Skill_Set__c userSkillSet;
        
        try{
            
            String queryString = 'select id,Assessment_Type__c,Business_Unit__c,Maximum__c '
                + ' from Credit_Skill_Set__c where Name != \'Manager (For Review)\' '
                + ' and id in (select Work_Queue_Definition__c '
                + ' from Credit_Skill_Set_Assignment__c '
                + ' where Credit_Agent__c = \'' + userId + '\''+')';
            
            System.debug('... dyy ... queryString ... ' + queryString);
            userSkillSet=Database.query(queryString);  
            
        }catch(Exception e){
            System.debug('... dyy ... error when getUserSkillSet ... ' + e);
            skillsetNotdefined = true;
        }
        
        System.debug('dyy ... userSkillSet = ' + userSkillSet);
        return userSkillSet;
    }    
    
    private String getQuerystringSkillset(){
        System.debug('dyy ... getQuerystringSkillset ...' );
        Credit_Skill_Set__c userSkillset = getUserSkillSet();
        if(userSkillset==null) return '';
        String carType = userSkillset.Assessment_Type__c;
        System.debug('dyy ... ');
        System.debug('dyy ... carType = ' + carType );
        System.debug('dyy ... ');
        String businessUnit = userSkillset.Business_Unit__c;
        Decimal maximumCarvalueAllowed = userSkillset.Maximum__c;
        String queryString = ' and ' +  getCreditApprovalRequestTypeLikeString(carType)       
            + ' and Assessment_Queue__c in ' +  getStringToInclude(businessUnit)      
            + ' and Total_Deal_Value__c <= ' +  maximumCarvalueAllowed 
            ;
        System.debug('dyy ... userSkillSet = ' + userSkillSet);
        return queryString;
    }
    
    private String getCreditApprovalRequestTypeLikeString(String strToSplit){
        System.debug('dyy ... strToSplit = ' + strToSplit);
        String stringToInclude='(';
        String[] stringList = strToSplit.split(';');
        for(String  theString:stringList){
            stringToInclude += ' Credit_Assessment_Request_Type__c like '+ '\'' + theString + '%\' or';
        }   
        stringToInclude = stringToInclude.removeEnd('or');
        stringToInclude += ')';
        return stringToInclude;
    }
    
    private String getStringToInclude(String strToSplit){
        System.debug('dyy ... strToSplit = ' + strToSplit);
        String stringToInclude='(';
        String[] stringList = strToSplit.split(';');
        for(String  theString:stringList){
            stringToInclude += '\'' + theString + '\',';
        }   
        stringToInclude = stringToInclude.removeEnd(',');
        stringToInclude += ')';
        return stringToInclude;
    }
    
    
    /*------- car for the same customer to same agent -----------------*/ 
    private String getQuerystringForSameCustomer(){        
        Credit_Assessment__c[] userTodayCars = [select CAR_Account__r.id 
                                                from Credit_Assessment__c 
                                                where OwnerId= :userId  
                                                and LastModifiedDate = TODAY];        
        String queryString = ' and CAR_Account__r.id in (';
        for(Credit_Assessment__c  theCar:userTodayCars){
            if (theCar.CAR_Account__r!=null)
                queryString += '\'' + theCar.CAR_Account__r.id  + '\',';
        }   
        queryString = queryString.removeEnd(',');
        if(userTodayCars==null||userTodayCars.size()==0) queryString += '\''+'NOT APPLICABLE'+'\'';
        queryString += ')';    
        System.debug('... dyy ... getQuerystringForSameCustomer = ' + queryString);
        return queryString;
    }
    
    
    /*------- misc methods -----------------*/ 
    
    private String getReturnUrl(){        
        String url = '/' + SObjectType.Credit_Assessment__c.keyPrefix;  
        return url;
    }  
    
}