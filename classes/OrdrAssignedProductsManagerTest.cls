@isTest
    public class OrdrAssignedProductsManagerTest {
        @isTest
        private static void testCalibrateServiceAccountAssets() {
           Account account = OrdrTestDataFactory.createServiceAccountWithAssets();
            
              Product2 newProduct = new Product2(name='Test Product', productcode='TESTCODE', isActive=true, OCOM_Shippable__c='Yes');
              newProduct.orderMgmtId__c='9876543210';
              newProduct.Sellable__c= true;
              insert newProduct;
            
            OCOMCharactersticsOSSJMapping__c  Customsetting = new OCOMCharactersticsOSSJMapping__c();
          
            //List<OCOMCharactersticsOSSJMapping__c> customSettingChar = new List<OCOMCharactersticsOSSJMapping__c>();
            customSetting.Name = '9135896567713259864';
            customSetting.CharacteristicName__c = 'Download speed';
            customSetting.CharacteristicsOSSJName__c = 'download_speed';
            Insert Customsetting;
            
          TpCommonBaseV3.Characteristic charRes = new TpCommonBaseV3.Characteristic();
            String[] valueList = new String[]{'9876543210'};
           charRes.Name = 'offeringId'; 
        
            TpCommonBaseV3.CharacteristicValue charactersticsVal = new TpCommonBaseV3.CharacteristicValue();
            charactersticsVal.Characteristic = charRes;
            charactersticsVal.Value = valueList;
            
            TpCommonBaseV3.CharacteristicValue[] charactersticsValList = new TpCommonBaseV3.CharacteristicValue[]{charactersticsVal};
            charactersticsValList[0] = charactersticsVal;
          
            TpInventoryProductConfigV3.Product productConfig = new TpInventoryProductConfigV3.Product();
    
    
            
            Asset a = new Asset();
            a.accountId = account.parentId;
            a.vlocity_cmt__ServiceAccountId__c = account.Id;
            a.Name = 'Office Internet PLUS';
            a.orderMgmt_BPI_Id__c = '9144101066713931234';
            a.Quantity = 1;
            a.vlocity_cmt__JSONAttribute__c='{"TELUSCHAR":[{"$$AttributeDefinitionStart$$":null,"Status":"Active","objectid__c":"01t560000005tUJAAY","attributeid__c":"a6u56000000CaceAAC","attributecategoryid__c":"a6t5600000008cOAAQ","categorycode__c":"TELUSCHAR","categoryname__c":"Characteristics","attributeuniquecode__c":"ATTRIBUTE-440","attributeconfigurable__c":true,"attributedisplaysequence__c":"3","attributefilterable__c":false,"isactive__c":true,"attributedisplayname__c":"Line Item External ID","displaysequence__c":null,"formatmask__c":null,"hasrule__c":false,"isconfigurable__c":true,"ishidden__c":true,"valueinnumber__c":null,"objecttype__c":"Product2","querycode__c":null,"isreadonly__c":true,"isquerydriven__c":false,"querylabel__c":null,"id":"a6s56000000Gmi7AAC","isrequired__c":false,"rulemessage__c":null,"uidisplaytype__c":"Text","value__c":null,"valuedatatype__c":"Text","valuedescription__c":null,"attributeRunTimeInfo":{"dataType":"Picklist","uiDisplayType":"Text"},"Name":"Line Item External ID","Code":"ATTRIBUTE-440","Filterable":false,"$$hashKey":"070","$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"objectid__c":"01t560000005tUJAAY","attributeid__c":"a6u56000000CaaPAAS","attributecategoryid__c":"a6t5600000008cOAAQ","categorycode__c":"TELUSCHAR","categoryname__c":"Characteristics","attributeuniquecode__c":"ATTRIBUTE-401","attributeconfigurable__c":true,"attributedisplaysequence__c":"4","attributefilterable__c":false,"isactive__c":true,"attributedisplayname__c":"Number of Included Email Accounts","displaysequence__c":null,"formatmask__c":null,"hasrule__c":false,"isconfigurable__c":true,"ishidden__c":false,"valueinnumber__c":null,"objecttype__c":"Product2","querycode__c":null,"isreadonly__c":false,"isquerydriven__c":false,"querylabel__c":null,"id":"a6s56000000Gmi9AAC","isrequired__c":true,"rulemessage__c":null,"uidisplaytype__c":"Text","value__c":null,"valuedatatype__c":"Text","valuedescription__c":null,"attributeRunTimeInfo":{"dataType":"Picklist","uiDisplayType":"Text","value":"7"},"Name":"Number of Included Email Accounts","Code":"ATTRIBUTE-401","Filterable":false,"SegmentValue":"7","$$hashKey":"071","$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"objectid__c":"01t560000005tUJAAY","attributeid__c":"a6u56000000CaaFAAS","attributecategoryid__c":"a6t5600000008cOAAQ","categorycode__c":"TELUSCHAR","categoryname__c":"Characteristics","attributeuniquecode__c":"ATTRIBUTE-414","attributeconfigurable__c":true,"attributedisplaysequence__c":"2","attributefilterable__c":false,"isactive__c":true,"attributedisplayname__c":"Provisioning Indicator","displaysequence__c":null,"formatmask__c":null,"hasrule__c":false,"isconfigurable__c":true,"ishidden__c":false,"valueinnumber__c":null,"objecttype__c":"Product2","querycode__c":null,"isreadonly__c":false,"isquerydriven__c":false,"querylabel__c":null,"id":"a6s56000000Gmi8AAC","isrequired__c":true,"rulemessage__c":null,"uidisplaytype__c":"Text","value__c":"Copper","valuedatatype__c":"Text","valuedescription__c":null,"attributeRunTimeInfo":{"dataType":"Picklist","uiDisplayType":"Text","default":"Copper","value":"Copper"},"Name":"Provisioning Indicator","Code":"ATTRIBUTE-414","Filterable":false,"SegmentValue":"Copper","$$hashKey":"072","$$AttributeDefinitionEnd$$":null}]}';
            a.vlocity_cmt__LineNumber__c = '0002';
            Insert a;
            System.debug('ASSET from ACCOUNT NEWLY CREATED----------------------------->'+a);
            
            Test.startTest();
            Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
             OrdrAssignedProductsManager sc = new OrdrAssignedProductsManager();
            // Map<String, Product2> productMap = sc.productMap;
            OrdrAssignedProductsManager.calibrateServiceAccountAssets(account.Id);
            
            Test.stopTest();
        } 
        
        
       /* private static testMethod void testCreateAsset(){
              Account account = OrdrTestDataFactory.createServiceAccountWithAssets();
              Product2 newProduct = new Product2(name='Test Product', productcode='TESTCODE', isActive=true, OCOM_Shippable__c='Yes');
              newProduct.orderMgmtId__c='9876543210';
              newProduct.Sellable__c= true;
              insert newProduct;
            
            OCOMCharactersticsOSSJMapping__c  Customsetting = new OCOMCharactersticsOSSJMapping__c();
          
            //List<OCOMCharactersticsOSSJMapping__c> customSettingChar = new List<OCOMCharactersticsOSSJMapping__c>();
            customSetting.Name = '9135896567713259864';
            customSetting.CharacteristicName__c = 'Download speed';
            customSetting.CharacteristicsOSSJName__c = 'download_speed';
            Insert Customsetting;
            
            TpCommonBaseV3.Characteristic charRes = new TpCommonBaseV3.Characteristic();
            String[] valueList = new String[]{'9876543210'};
            charRes.Name = 'offeringId'; 
        
            TpCommonBaseV3.CharacteristicValue charactersticsVal = new TpCommonBaseV3.CharacteristicValue();
            charactersticsVal.Characteristic = charRes;
            charactersticsVal.Value = valueList;
            
            TpCommonBaseV3.CharacteristicValue[] charactersticsValList = new TpCommonBaseV3.CharacteristicValue[]{charactersticsVal};
            charactersticsValList[0] = charactersticsVal;
          
            TpInventoryProductConfigV3.Product productConfig = new TpInventoryProductConfigV3.Product();
            
            OrdrAssignedProductsManager.createAsset(account, productConfig, '0001');
            
        }*/
        
        /*private static testMethod void  testGetLineNumber(){
            Account account = OrdrTestDataFactory.createServiceAccountWithAssets();
            Asset a = new Asset();
            a.accountId = account.parentId;
            a.vlocity_cmt__ServiceAccountId__c = account.Id;
            a.Name = 'Office Internet PLUS';
            a.orderMgmt_BPI_Id__c = '9144101066713931234';
            a.Quantity = 1;
            a.vlocity_cmt__JSONAttribute__c='{"TELUSCHAR":[{"$$AttributeDefinitionStart$$":null,"Status":"Active","objectid__c":"01t560000005tUJAAY","attributeid__c":"a6u56000000CaceAAC","attributecategoryid__c":"a6t5600000008cOAAQ","categorycode__c":"TELUSCHAR","categoryname__c":"Characteristics","attributeuniquecode__c":"ATTRIBUTE-440","attributeconfigurable__c":true,"attributedisplaysequence__c":"3","attributefilterable__c":false,"isactive__c":true,"attributedisplayname__c":"Line Item External ID","displaysequence__c":null,"formatmask__c":null,"hasrule__c":false,"isconfigurable__c":true,"ishidden__c":true,"valueinnumber__c":null,"objecttype__c":"Product2","querycode__c":null,"isreadonly__c":true,"isquerydriven__c":false,"querylabel__c":null,"id":"a6s56000000Gmi7AAC","isrequired__c":false,"rulemessage__c":null,"uidisplaytype__c":"Text","value__c":null,"valuedatatype__c":"Text","valuedescription__c":null,"attributeRunTimeInfo":{"dataType":"Picklist","uiDisplayType":"Text"},"Name":"Line Item External ID","Code":"ATTRIBUTE-440","Filterable":false,"$$hashKey":"070","$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"objectid__c":"01t560000005tUJAAY","attributeid__c":"a6u56000000CaaPAAS","attributecategoryid__c":"a6t5600000008cOAAQ","categorycode__c":"TELUSCHAR","categoryname__c":"Characteristics","attributeuniquecode__c":"ATTRIBUTE-401","attributeconfigurable__c":true,"attributedisplaysequence__c":"4","attributefilterable__c":false,"isactive__c":true,"attributedisplayname__c":"Number of Included Email Accounts","displaysequence__c":null,"formatmask__c":null,"hasrule__c":false,"isconfigurable__c":true,"ishidden__c":false,"valueinnumber__c":null,"objecttype__c":"Product2","querycode__c":null,"isreadonly__c":false,"isquerydriven__c":false,"querylabel__c":null,"id":"a6s56000000Gmi9AAC","isrequired__c":true,"rulemessage__c":null,"uidisplaytype__c":"Text","value__c":null,"valuedatatype__c":"Text","valuedescription__c":null,"attributeRunTimeInfo":{"dataType":"Picklist","uiDisplayType":"Text","value":"7"},"Name":"Number of Included Email Accounts","Code":"ATTRIBUTE-401","Filterable":false,"SegmentValue":"7","$$hashKey":"071","$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"objectid__c":"01t560000005tUJAAY","attributeid__c":"a6u56000000CaaFAAS","attributecategoryid__c":"a6t5600000008cOAAQ","categorycode__c":"TELUSCHAR","categoryname__c":"Characteristics","attributeuniquecode__c":"ATTRIBUTE-414","attributeconfigurable__c":true,"attributedisplaysequence__c":"2","attributefilterable__c":false,"isactive__c":true,"attributedisplayname__c":"Provisioning Indicator","displaysequence__c":null,"formatmask__c":null,"hasrule__c":false,"isconfigurable__c":true,"ishidden__c":false,"valueinnumber__c":null,"objecttype__c":"Product2","querycode__c":null,"isreadonly__c":false,"isquerydriven__c":false,"querylabel__c":null,"id":"a6s56000000Gmi8AAC","isrequired__c":true,"rulemessage__c":null,"uidisplaytype__c":"Text","value__c":"Copper","valuedatatype__c":"Text","valuedescription__c":null,"attributeRunTimeInfo":{"dataType":"Picklist","uiDisplayType":"Text","default":"Copper","value":"Copper"},"Name":"Provisioning Indicator","Code":"ATTRIBUTE-414","Filterable":false,"SegmentValue":"Copper","$$hashKey":"072","$$AttributeDefinitionEnd$$":null}]}';
            a.vlocity_cmt__LineNumber__c = '0001';
            Insert a;
            
            Map<String, Asset> assetMap = new Map<String, Asset>();
            assetMap.put((String)a.Id , a) ;
            Test.startTest();
            TpInventoryProductConfigV3 res = new TpInventoryProductConfigV3();
           // res.Product.Characteristic.Name = 'offeringId';     
            OrdrAssignedProductsManager.getLineNumber(account.Id ,assetMap , null );
            Test.stopTest();
        }*/
        
       /* private static testMethod void  createAssetTest(){
            Account account = OrdrTestDataFactory.createServiceAccountWithAssets();
            Asset a = new Asset();
            a.accountId = account.parentId;
            a.vlocity_cmt__ServiceAccountId__c = account.Id;
            a.Name = 'Office Internet PLUS';
            a.orderMgmt_BPI_Id__c = '9144101066713931234';
            a.Quantity = 1;
            a.vlocity_cmt__JSONAttribute__c='{"TELUSCHAR":[{"$$AttributeDefinitionStart$$":null,"Status":"Active","objectid__c":"01t560000005tUJAAY","attributeid__c":"a6u56000000CaceAAC","attributecategoryid__c":"a6t5600000008cOAAQ","categorycode__c":"TELUSCHAR","categoryname__c":"Characteristics","attributeuniquecode__c":"ATTRIBUTE-440","attributeconfigurable__c":true,"attributedisplaysequence__c":"3","attributefilterable__c":false,"isactive__c":true,"attributedisplayname__c":"Line Item External ID","displaysequence__c":null,"formatmask__c":null,"hasrule__c":false,"isconfigurable__c":true,"ishidden__c":true,"valueinnumber__c":null,"objecttype__c":"Product2","querycode__c":null,"isreadonly__c":true,"isquerydriven__c":false,"querylabel__c":null,"id":"a6s56000000Gmi7AAC","isrequired__c":false,"rulemessage__c":null,"uidisplaytype__c":"Text","value__c":null,"valuedatatype__c":"Text","valuedescription__c":null,"attributeRunTimeInfo":{"dataType":"Picklist","uiDisplayType":"Text"},"Name":"Line Item External ID","Code":"ATTRIBUTE-440","Filterable":false,"$$hashKey":"070","$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"objectid__c":"01t560000005tUJAAY","attributeid__c":"a6u56000000CaaPAAS","attributecategoryid__c":"a6t5600000008cOAAQ","categorycode__c":"TELUSCHAR","categoryname__c":"Characteristics","attributeuniquecode__c":"ATTRIBUTE-401","attributeconfigurable__c":true,"attributedisplaysequence__c":"4","attributefilterable__c":false,"isactive__c":true,"attributedisplayname__c":"Number of Included Email Accounts","displaysequence__c":null,"formatmask__c":null,"hasrule__c":false,"isconfigurable__c":true,"ishidden__c":false,"valueinnumber__c":null,"objecttype__c":"Product2","querycode__c":null,"isreadonly__c":false,"isquerydriven__c":false,"querylabel__c":null,"id":"a6s56000000Gmi9AAC","isrequired__c":true,"rulemessage__c":null,"uidisplaytype__c":"Text","value__c":null,"valuedatatype__c":"Text","valuedescription__c":null,"attributeRunTimeInfo":{"dataType":"Picklist","uiDisplayType":"Text","value":"7"},"Name":"Number of Included Email Accounts","Code":"ATTRIBUTE-401","Filterable":false,"SegmentValue":"7","$$hashKey":"071","$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"objectid__c":"01t560000005tUJAAY","attributeid__c":"a6u56000000CaaFAAS","attributecategoryid__c":"a6t5600000008cOAAQ","categorycode__c":"TELUSCHAR","categoryname__c":"Characteristics","attributeuniquecode__c":"ATTRIBUTE-414","attributeconfigurable__c":true,"attributedisplaysequence__c":"2","attributefilterable__c":false,"isactive__c":true,"attributedisplayname__c":"Provisioning Indicator","displaysequence__c":null,"formatmask__c":null,"hasrule__c":false,"isconfigurable__c":true,"ishidden__c":false,"valueinnumber__c":null,"objecttype__c":"Product2","querycode__c":null,"isreadonly__c":false,"isquerydriven__c":false,"querylabel__c":null,"id":"a6s56000000Gmi8AAC","isrequired__c":true,"rulemessage__c":null,"uidisplaytype__c":"Text","value__c":"Copper","valuedatatype__c":"Text","valuedescription__c":null,"attributeRunTimeInfo":{"dataType":"Picklist","uiDisplayType":"Text","default":"Copper","value":"Copper"},"Name":"Provisioning Indicator","Code":"ATTRIBUTE-414","Filterable":false,"SegmentValue":"Copper","$$hashKey":"072","$$AttributeDefinitionEnd$$":null}]}';
            a.vlocity_cmt__LineNumber__c = '0001';
            Insert a;
            
            Map<String, Asset> assetMap = new Map<String, Asset>();
            assetMap.put((String)a.Id , a) ;
            Test.startTest();
            TpInventoryProductConfigV3 res = new TpInventoryProductConfigV3();
            res.Product  resProd = new res.Product();
            resProd.Characteristic.Name = 'offeringId';
            
            OrdrAssignedProductsManager.createAsset(account.Id ,assetMap ,  null);
            Test.stopTest();
        }*/
       
    
    }