/*
*******************************************************************************************************************************
Created by:     Sandip Chaudhari    18-Aug-2017     Order 2.0     No previous version

*******************************************************************************************************************************
*/
@isTest
public class OC_HybridCPQHeaderControllerTest{
    public static testmethod void testOCHybridCPQHeaderController(){
        Account acc = new Account(Name='Test Account');
        insert acc;
         contact cont= new contact(firstname='abc',lastname='xyz',Language_Preference__c = 'English');
        insert cont;
       
        Product2 product4 = new Product2(Name='Collect Call Deny ',ProductCode='PROD-004', Description='This is a test JSON Product 1 Generator',isActive = true,orderMgmtId__c='9143863905013553513' ,OCOM_Bookable__c='Yes',Sellable__c = true);
       product4.vlocity_cmt__JSONAttribute__c  = '{"TELUSCHAR": [{"$$AttributeDefinitionEnd$$": null, "attributeRunTimeInfo": {"selectedItem":{"value":"Overline","displayText":"Overline","id":"9145085938413814481"}",values": [{"value": "Overline", "displayText": "Overline", "id": "9145085938413814481"}], "default": [{"value": "Overline", "displayText": "Overline", "id": "9145085938413814481"}], "uiDisplayType": "Dropdown", "dataType": "Picklist"}, "categorydisplaysequence__c": 1, "customconfiguitemplate__c": null, "attributecloneable__c": true, "valuedescription__c": null, "valuedatatype__c": "Picklist", "value__c": "Overline", "uidisplaytype__c": "Dropdown", "rulemessage__c": null, "isrequired__c": true, "id": "a7P220000008lnIEAQ", "querylabel__c": null, "isquerydriven__c": false, "isreadonly__c": true, "querycode__c": null, "objecttype__c": "Product2", "valueinnumber__c": null, "ishidden__c": true, "isconfigurable__c": true, "hasrule__c": false, "formatmask__c": null, "displaysequence__c": null, "attributedisplayname__c": "Line Type", "isactive__c": true, "attributefilterable__c": false, "attributedisplaysequence__c": "2", "attributeconfigurable__c": true, "attributeuniquecode__c": "ATTRIBUTE-549", "categoryname__c": "Characteristics", "categorycode__c": "TELUSCHAR", "attributecategoryid__c": "a7Q220000009vghEAA", "attributeid__c": "a7R220000000HqcEAE", "objectid__c": "01t22000000UXFUAA4", "$$AttributeDefinitionStart$$": null }] }';
        product4.IncludeQuoteContract__c= true;
       insert product4;

        
        Pricebook2 standardBook =  new Pricebook2(Id = Test.getStandardPricebookId(), Name = 'TestPricebook', IsActive = true); 
         PricebookEntry sbe4 = new PricebookEntry(Pricebook2Id = standardBook.Id,Product2Id = product4.Id, UnitPrice = 2, Vlocity_cmt__RecurringPrice__c = 17, IsActive = true, UseStandardPrice = false);
        insert sbe4;
        Pricebook2 customPriceBook =  new Pricebook2( Name = 'CustomPricebook', IsActive = true);
        insert customPriceBook;        
        PricebookEntry pbe4 = new PricebookEntry(Pricebook2Id = customPriceBook.Id,Product2Id = product4.Id, UnitPrice = 2, Vlocity_cmt__RecurringPrice__c = 17, IsActive = true, UseStandardPrice = false);
        insert pbe4;
        
        Order parentOrd = new Order(AccountId = acc.Id, Status='Draft',EffectiveDate=System.Today(),Type = 'Move',Pricebook2Id =  customPriceBook.ID );
        parentOrd.orderMgmtId_Status__c =  OrdrConstants.OI_COMPLETED_STATUS;
        insert parentOrd;
       
        Order ord = new Order(ParentId__c=parentOrd.Id, AccountId = acc.Id, Status='Draft',EffectiveDate=System.Today(),Type = 'Move',Pricebook2Id =  customPriceBook.ID );
        ord.orderMgmtId_Status__c =  OrdrConstants.OI_COMPLETED_STATUS;
        insert ord;
        
        OrderItem  orderItem1 = new OrderItem(OrderId=Ord.Id, PricebookEntryId = pbe4.Id,vlocity_cmt__Product2Id__c=product4.Id, vlocity_cmt__LineNumber__c = '0001', Quantity = 1, UnitPrice = 10, vlocity_cmt__ProvisioningStatus__c = 'New');
        orderItem1.vlocity_cmt__JSONAttribute__c  = '{"TELUSCHAR": [{"$$AttributeDefinitionEnd$$": null, "attributeRunTimeInfo": {"selectedItem": {"value": "Overline1", "displayText": "Overline1", "id": "9145085938413814481"}, ",values": [{"value": "Overline", "displayText": "Overline", "id": "9145085938413814481"}], "default": [{"value": "Overline", "displayText": "Overline", "id": "9145085938413814481"}], "uiDisplayType": "Dropdown", "dataType": "Picklist"}, "categorydisplaysequence__c": 1, "customconfiguitemplate__c": null, "attributecloneable__c": true, "valuedescription__c": null, "valuedatatype__c": "Picklist", "value__c": "Overline", "uidisplaytype__c": "Dropdown", "rulemessage__c": null, "isrequired__c": true, "id": "a7P220000008lnIEAQ", "querylabel__c": null, "isquerydriven__c": false, "isreadonly__c": true, "querycode__c": null, "objecttype__c": "Product2", "valueinnumber__c": null, "ishidden__c": true, "isconfigurable__c": true, "hasrule__c": false, "formatmask__c": null, "displaysequence__c": null, "attributedisplayname__c": "Line Type", "isactive__c": true, "attributefilterable__c": false, "attributedisplaysequence__c": "2", "attributeconfigurable__c": true, "attributeuniquecode__c": "ATTRIBUTE-549", "categoryname__c": "Characteristics", "categorycode__c": "TELUSCHAR", "attributecategoryid__c": "a7Q220000009vghEAA", "attributeid__c": "a7R220000000HqcEAE", "objectid__c": "01t22000000UXFUAA4", "$$AttributeDefinitionStart$$": null }] }';
        insert orderItem1;
        
        Test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.oc_SelectAddress')); 
            System.currentPageReference().getParameters().put('rcid', acc.Id);
            OC_HybridCPQHeaderController Ctrl = new OC_HybridCPQHeaderController();
            string a = Ctrl.genericAccountName;
            String type = Ctrl.Type;
            Boolean displayComponents = Ctrl.displayComponents;
            Boolean displayViewPdfBtn = Ctrl.displayViewPdfBtn;
            System.currentPageReference().getParameters().put('parentOrderId', parentOrd.Id);
            OC_HybridCPQHeaderController Ctrl1 = new OC_HybridCPQHeaderController();
            Ctrl1.CancelOrder();
            parentOrd.CustomerAuthorizedById = cont.Id;
            update parentOrd;
            OC_HybridCPQHeaderController Ctrl2 = new OC_HybridCPQHeaderController();
            System.debug(Ctrl2.workRequestRecordTypeId);
        System.debug(Ctrl2.accountWorkRequestJson);
        System.debug(Ctrl2.opportunityWorkRequestJson);
            System.debug(Ctrl2.orderWorkRequestJson);
        Test.StopTest();
    }   
}