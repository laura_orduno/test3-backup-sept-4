@isTest
private class trac_AttachmentTriggerDispatcherTest
{
	@isTest(seeAllData=false)
	static void receiveAttachment()
	{
		List<Case> testCases = new List<Case>();		
		List<Attachment> testAttachments = new List<Attachment>();

		Set<Id> ccIds = new Set<Id>();
		
		Case testCase = new Case(Subject = 'Testing reparting Attachment', Priority = '1');

		testCases.add(testCase);
		insert testCase;

		testAttachments.add( new Attachment(
			parentId = String.valueOf(testCase.Id),
            body = Blob.valueOf( 'this is an attachment test' ),
			name = 'fake attachment'
		));
        
		Test.startTest();
		insert testAttachments;
		Test.stopTest();
	}
}