public with sharing class SiteSummaryPageContr{

    public PageReference updateSaoObject() {
        List<Contact> contactObj = new List<Contact>() ;
              
              if(contactId != null){
                  system.debug('contactId---->' + contactId);
                  contactObj = [Select Id, Name, Phone, Email From Contact where Id =: contactId ];
              }
              List<ServiceAddressOrderEntry__c> addressOnOrderList = [Select Id, Onsite_Contact_Name__c, Onsite_Contact_Phone__c , Onsite_Contact_Email__c
                                                                      from ServiceAddressOrderEntry__c
                                                                      where Order__c =: OrderId ];
              system.debug('addressOnOrderList---->' + addressOnOrderList);
              List<ServiceAddressOrderEntry__c> tobeUpdatedAddressOnOrderList = new List<ServiceAddressOrderEntry__c>();
              if(addressOnOrderList.size()!=0 &&  contactObj.size() != 0){
                  for(ServiceAddressOrderEntry__c saoObj : addressOnOrderList){
                      saoObj.Onsite_Contact_Name__c = contactObj[0].Name;
                      saoObj.Onsite_Contact_Phone__c = contactObj[0].Phone;
                      saoObj.Onsite_Contact_Email__c = contactObj[0].Email;
                      tobeUpdatedAddressOnOrderList.add(saoObj);
                  }
              }                                                     
              update tobeUpdatedAddressOnOrderList ; 
        return null;
    }

    
    Public Id ParentAccId{get;set;}
    public String addressOrigin{get;set;}
    Public Id OrderId{get;set;}
    Public Order OrderObj{get;set;}
    Public Id SMBCareAddressId{get;set;}
    Public List<SMBCare_Address__c> ListServiceAdress{get;set;} 
    Public Id ServiceAddreID{get;set;}
    Public List<SMBCareAddWrapperClass> ListOfSMBCareWrapperClass{get;set;}
    Public Map<id,ServiceAddressOrderEntry__c> MapSMBCareAddresstoServiceAddressOrderEntry;
    Public Id contactId {get;set;}
    Public Id ServiceAcctId{get;set;}
    Public List<Order> ParentAccIdSearch  {get;set;}
    public String parentRcidAccountName {get;set;}
    public Boolean displayPageComp{get; set; }
    
     Public SiteSummaryPageContr(){
     OrderId = System.currentPageReference().getParameters().get('id');
     
     if(OrderId != null){
         displayPageComp = true;
     }else{
         displayPageComp = false;
     }
    // code added by danish on 10/nov/2016 ---->
        if (ApexPages.currentPage().getParameters().get('ParentAccId')=='' ||
            ApexPages.currentPage().getParameters().get('ParentAccId')==null) 
          {
            ParentAccIdSearch = [select id,AccountId from Order where Id =: OrderId LIMIT 1];
            
            If(ParentAccIdSearch.size()>0 && !ParentAccIdSearch.isEmpty()){
               ParentAccId = ParentAccIdSearch[0].AccountId ;
                System.debug('ParentAccIdSearch in query--->'+ParentAccIdSearch);
                 System.debug('ParentAccIdSearch[0].AccountId--->'+ParentAccIdSearch[0].AccountId);
                 System.debug('ParentAccId-afterquery--->'+ParentAccId);
            }
          } 
        else {         
            ParentAccId = System.currentPageReference().getParameters().get('ParentAccId');
        }
        System.debug('ParentAccId-------->'+ParentAccId);
        //danishs code ends here---<
        
        SMBCareAddressId = System.currentPageReference().getParameters().get('smbCareAddrId');
        addressOrigin = System.currentPageReference().getParameters().get('addressOrigin');
        contactId = System.currentPageReference().getParameters().get('contactId');
        ServiceAcctId = System.currentPageReference().getParameters().get('ServiceAcctId');
        ListServiceAdress = [SELECT id,Name,Address__c,Account__c,State__c,city__c From SMBCare_Address__c where Account__c =:ParentAccId];
        parentRcidAccountName = [Select Name from Account where Id =:ParentAccId].Name ;
        ListOfSMBCareWrapperClass = new List<SMBCareAddWrapperClass>();
        OrderObj = new order();
        List<order> ListOfOrder = [select id,TotalAmount,vlocity_cmt__EffectiveOneTimeTotal__c,vlocity_cmt__RecurringTotal__c,vlocity_cmt__EffectiveOrderTotal__c from order where id=:OrderId];
        System.debug('ListOfOrder'+ListOfOrder);
        if(ListOfOrder.size() >0){
            OrderObj = ListOfOrder[0];
        }
        
        
              
               
        MapSMBCareAddresstoServiceAddressOrderEntry = new Map<id,ServiceAddressOrderEntry__c>();
        for(ServiceAddressOrderEntry__c sao : [SELECT Name,Order__c,address__c, isSiteReadyForCheckout__c ,PricePerSite__c, PricePerMonth__c,id FROM ServiceAddressOrderEntry__c where Order__c =:OrderId]){
            IF(!MapSMBCareAddresstoServiceAddressOrderEntry.containskey(sao.address__c)){
                MapSMBCareAddresstoServiceAddressOrderEntry.put(sao.address__c,sao);
            }
            
        }
        
    }
    
    //Arpit: 24-Jan-2016 : Sprint-14 : Updating ServiceAddressOrderEntry__c on order with order's contact details as default contact details
    public void updateSaoObject(Id orderId, Id contactId){
        List<Contact> contactObj = new List<Contact>() ;
              
              if(contactId != null){
                  system.debug('contactId---->' + contactId);
                  contactObj = [Select Id, Name, Phone, Email From Contact where Id =: contactId ];
              }
              List<ServiceAddressOrderEntry__c> addressOnOrderList = [Select Id, Onsite_Contact_Name__c, Onsite_Contact_Phone__c , Onsite_Contact_Email__c
                                                                      from ServiceAddressOrderEntry__c
                                                                      where Order__c =: OrderId ];
              system.debug('addressOnOrderList---->' + addressOnOrderList);
              List<ServiceAddressOrderEntry__c> tobeUpdatedAddressOnOrderList = new List<ServiceAddressOrderEntry__c>();
              if(addressOnOrderList.size()!=0 &&  contactObj.size() != 0){
                  for(ServiceAddressOrderEntry__c saoObj : addressOnOrderList){
                      saoObj.Onsite_Contact_Name__c = contactObj[0].Name;
                      saoObj.Onsite_Contact_Phone__c = contactObj[0].Phone;
                      saoObj.Onsite_Contact_Email__c = contactObj[0].Email;
                      tobeUpdatedAddressOnOrderList.add(saoObj);
                  }
              }                                                     
              update tobeUpdatedAddressOnOrderList ; 
    }
    
    
    Public pagereference RedirecttoCartPage(){
        PageReference pg =  new PageReference('/apex/naas_hybridcpq?id='+ OrderId+ '&ServiceAcctId=' + ServiceAcctId+ '&ParentAccId=' + ParentAccId + '&smbCareAddrId=' + ServiceAddreID+ '&contactId=' + contactId);
        return pg;
    }
    Public pagereference NexttoCartPage(){
        ServiceAddreID = ListOfSMBCareWrapperClass[0].objSMB.Id;
        PageReference pg =  new PageReference('/apex/naas_hybridcpq?id='+ OrderId+ '&ServiceAcctId=' + ServiceAcctId+ '&ParentAccId=' + ParentAccId + '&smbCareAddrId=' + ServiceAddreID+ '&contactId=' + contactId);
        return pg;
    }
    Public List<SMBCareAddWrapperClass> getSMBCareWrapperClass(){
         ListOfSMBCareWrapperClass.clear();
         //Arpit : 16-Jan-2017 - Sprint 14 : BN-1398 & 1397- Modified query , added Service_Address_Full_Name__c & Is_Mobile_available__c  
         List<SMBCare_Address__c> smbAddressList = [SELECT id,Name,Address__c,Account__c, Service_Address_Full_Name__c, State__c,city__c,GPON_Serviceability__c,HSIA_Serviceability__c,LTE_Serviceability__c,
                                                     MaxSpeedWithType__c,TVServicability__c,Voice_Serviceability__c,Mobile_Serviceability__c  
                                                     From SMBCare_Address__c 
                                                     where Account__c =:ParentAccId] ;
         for(SMBCare_Address__c sm : smbAddressList ){                                    
                               
             if(MapSMBCareAddresstoServiceAddressOrderEntry.containskey(sm.id)){
                 ListOfSMBCareWrapperClass.add(new SMBCareAddWrapperClass(sm.Account__c,sm,MapSMBCareAddresstoServiceAddressOrderEntry.get(sm.id).isSiteReadyForCheckout__c ,MapSMBCareAddresstoServiceAddressOrderEntry.get(sm.id).pricepersite__c, MapSMBCareAddresstoServiceAddressOrderEntry.get(sm.id).PricePerMonth__c));
             }
         }
         system.debug('ListOfSMBCareWrapperClass----> '+ ListOfSMBCareWrapperClass);
        return ListOfSMBCareWrapperClass;
    }
    
    
    
    
    
    Public Class SMBCareAddWrapperClass{
     Public Boolean IsSelected {get;set;}
     Public Id AccId{get;set;}
     Public SMBCare_Address__c objSMB{get;set;}
     Public Boolean Status{get;set;}
     Public decimal PricePerSite{get;set;}
     Public decimal PricePerMonth{get;set;}
     Public decimal oneTimeCost {get;set;}
     
     Public SMBCareAddWrapperClass(Id aid, SMBCare_Address__c smb,Boolean sts, decimal pps, decimal ppm){
          AccId = aid;
          objSMB = smb; 
          Status = sts;
          PricePerSite = pps;
          PricePerMonth = ppm;
          
     } 
     
 } 
}