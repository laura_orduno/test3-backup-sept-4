public class AmendContractProcess_CP {
        public static Map<string,object> createDealSupport(id ContractId, map<string,object> outMap)
        {
            map<string,string> mapConSourceDestinationFieldsMap = new map<string,string>();
            map<string,string> mapCliSourceDestinationFieldsMap = new map<string,string>();
            ID DSRId; 
            //Offer_House_Demand__c objDSRNew = new Offer_House_Demand__c();
            //objDSRNew.Contract__c = contractId;
            //objDSRNew.TYPE_OF_CONTRACT__c = 'Amendment';
            list<sObject > RpObjListToInsert = new list<sObject>();
            list<Offer_House_Demand__c> DSRListToInsert = new list<Offer_House_Demand__c>();
            set<string> cliSourceFieldSet = new set<string>{'Id'};
            set<string> destFieldSet=new set<string>{'Id','Contract__c'};  
            set <string> conSourceFieldSet = new set<string>();
            list <string> lstConFields = new list<string>();
            list <string> lstCliFields = new list<string>();
            list<Contract> orgConList = new list<Contract>();
            List<sobject> itemstoInsert = new List<sobject>();
            list<vlocity_cmt__ContractLineItem__c> cliList = new list<vlocity_cmt__ContractLineItem__c >();
            
             string sSubsidiaryAccountAPIName='Subsidiary_Account__c';
            
            List<String> relatedObjectNameList = new List<String>();
            relatedObjectNameList.add('Subsidiary_Account__c');
            relatedObjectNameList.add('Agreement_Contact__c');
            relatedObjectNameList.add('Agreement_Dealer__c');
            relatedObjectNameList.add('Agreement_Hardware_Allotment__c');
            relatedObjectNameList.add('Agreement_Churn_Allotment__c');
            relatedObjectNameList.add('Agreement_Offer_Code__c');
            relatedObjectNameList.add('Agreement_Comment__c');
            relatedObjectNameList.add('Agreement_Billing_Hierarchy__c');
            
            String[] relListtoLink = new String[]{'Agreement_Contact__c','Agreement_Dealer__c','Agreement_Comment__c'};
            
            schema.ChildRelationship[] lstContractRelationships = Schema.getGlobalDescribe().get('Contract').getDescribe().getChildRelationships();
            schema.ChildRelationship[] lstSubAccountRelationships = Schema.getGlobalDescribe().get('Subsidiary_Account__c').getDescribe().getChildRelationships();
            
            
            contract orgContract = new contract();
            
            
            map<string,Object> inputData = new map<string,Object>();
            
          try{
            if(ContractId != null)
            {
              
                // Get Contract To DSR Field Mappings
                inputData.put('sourceObjName','Contract');
                inputData.put('destObjName','Offer_House_Demand__c');
                
                map<string,Object> outputData = getFieldMappings(inputData);
                lstConFields = (list<string>)outputData.get('lstSrcObjFields');
                mapConSourceDestinationFieldsMap = (map<string,string>)outputData.get('mapSourceDestinationField');
                
                outputData.clear();
                inputData.clear();
              
                //Get CLI to Rate Plan Field Mappings
                inputData.put('sourceObjName','vlocity_cmt__ContractLineItem__c');
                inputData.put('destObjName','Rate_Plan_Item__c');
                outputData = getFieldMappings(inputData);
                lstCliFields = (list<string>)outputData.get('lstSrcObjFields');
                system.debug('lstCliFields:::::: ' + lstCliFields);
                mapCliSourceDestinationFieldsMap = (map<string,string>)outputData.get('mapSourceDestinationField');
                outputData.clear();
                inputData.clear();
                
                List<String> filter = new List<String>{'ID =: contractId'};
                
                string sChildQuery =ContractCreateRelatedObj.getChildQuery(relatedObjectNameList,lstContractRelationships);    
                
                String cliquery = '';
                String query = 'SELECT ';
                query +=  String.join(lstConFields,',');
                query += sChildQuery +' FROM ' + 'contract';
                if(filter!=null && filter.size()>0){
                    query +=' WHERE ' + String.join(filter,' and '); 
                }   
                
                // Adharsh: Adding Query for Contract Line Items for Resolving Too Many Rows issue
                if(lstCliFields != null && lstCliFields.size() > 0){
                    //query += ',(Select ' +  String.join(lstCliFields,',') + ' from vlocity_cmt__ContractLineItems__r)';
                    cliquery += 'Select ' +  String.join(lstCliFields,',') + ' from vlocity_cmt__ContractLineItem__c';
                    cliquery += ' WHERE vlocity_cmt__ContractId__c =: contractId';
                }
                
                //  String sDSRQuery = Util.queryBuilder('',lstDSRFields, lstDSRFilter);
                system.debug('\n--Contract Query--'+query);
                system.debug('\n--Contract Line Query--'+cliquery);
                
                 orgConList = Database.query(query);
                system.debug('orgConList' + orgConList);
                cliList = Database.query(cliquery);
               	if( cliList != null && cliList.size() > 0 && !cliList.isEmpty()){ 
                    //Adharsh:: Commenting Out the subquery to resolve too many rows Issue.
                    /*if( orgConList.size() > 0){
                        orgContract = orgConList[0];
                        if(orgContract.vlocity_cmt__ContractLineItems__r != null){
                            for(vlocity_cmt__ContractLineItem__c cliItems: orgContract.vlocity_cmt__ContractLineItems__r){
                                cliList.add(cliItems);
                            }
                        }*/
                        //cliList = orgContract.vlocity_cmt__ContractLineItems__r;
                      //Adharsh:: Comment END
                    system.debug('cliList__________' + cliList);
                    inputData.put('srcObjectList',orgConList );
                    inputData.put('mapSourceDestinationFieldsMap',mapConSourceDestinationFieldsMap );
                    inputData.put('ObjectName','Offer_House_Demand__c' );
                    
                    DSRListToInsert = (list<Offer_House_Demand__c>)updateTrgObjFields(inputData);   
                }
            }
            
            if(DSRListToInsert.size()>0  ){
                
                insert DSRListToInsert;
                DSRId = DSRListToInsert[0].id ;
                outMap.put('dsrId',DSRId);
                
                orgConList[0].Amended_Deal_Support__c  = DSRListToInsert[0].id ;
                orgConList[0].status='Amendment in Progress';
                update orgConList;
            }
              if(DSRId == null){
                  string errorMessage = 'Unable to Create Deal Support Record.Please try again';
                  outMap.put('error', errorMessage);
                  return outMap;
              }
              else {
                  if (cliList.size()>0){
                      
                      inputData.put('srcObjectList',cliList );
                      inputData.put('mapSourceDestinationFieldsMap',mapCliSourceDestinationFieldsMap );
                      inputData.put('ObjectName','Rate_Plan_Item__c' );
                      inputData.put('DSRID',DSRId);
                      RpObjListToInsert = updateTrgObjFields(inputData);
                  }
                  
                   if(orgConList != null && orgConList.size() > 0 && DSRId != null){
                      
                      
                      string sChildQuery =ContractCreateRelatedObj.getChildQuery(relListtoLink,lstSubAccountRelationships);
                      
                      list<Subsidiary_Account__c> subsidaryInfoList = Database.query('select id ' + sChildQuery +' from Subsidiary_Account__c where Contract__c=:contractId');
                      map<Id,Subsidiary_Account__c> mapSubsidaryInfo= new map<Id,Subsidiary_Account__c>(subsidaryInfoList);
                      
                      
                      List<sobject> itemsSAtoInsert = new List<sobject>();
                      map<string,string> mapObjectWithRelName=new map<string,string>();
                      for(String relatedObjName : relatedObjectNameList){
                          for(Schema.ChildRelationship objChildRelationship : lstContractRelationships){
                              if(objChildRelationship.getChildSObject() == Schema.getGlobalDescribe().get(relatedObjName)){
                                  mapObjectWithRelName.put(relatedObjName,objChildRelationship.getRelationshipName());
                                  break;
                              }
                          }
                      }
                      map<string,string> mapChildObjectWithRelName=new map<string,string>();
                      for(String relatedChildObjName : relListtoLink){
                          for(Schema.ChildRelationship objChildRelationship : lstSubAccountRelationships){
                              if(objChildRelationship.getChildSObject() == Schema.getGlobalDescribe().get(relatedChildObjName)){
                                  mapChildObjectWithRelName.put(relatedChildObjName,objChildRelationship.getRelationshipName());
                                  break;
                              }
                          }
                      }
                      map<Id,Sobject> mapSubsidaryAccountToInsert=new map<Id,Sobject>();
                      for(String relatedObjName : relatedObjectNameList){
                          if(mapObjectWithRelName.containsKey(relatedObjName)){
                              list<SObject> recordToUpdate = orgConList[0].getSObjects(mapObjectWithRelName.get(relatedObjName));
                              if(recordToUpdate != null){
                                  for(sObject obj : recordToUpdate){
                                      sObject newObj = obj.clone(false, true);
                                      newObj.put('Contract__c',null);
                                      newObj.put('Deal_Support__c',DSRId);
                                      if(sSubsidiaryAccountAPIName == relatedObjName){
                                          mapSubsidaryAccountToInsert.put((Id)obj.get('Id'),newObj);
                                      }
                                      else{
                                          itemstoInsert.add(newObj);
                                      }
                                  }
                              }
                          }
                      }
                      itemstoInsert.addall(mapSubsidaryAccountToInsert.values());
                      
                      if(RpObjListToInsert.size() > 0 && !RpObjListToInsert.isEmpty() ){
                          itemstoInsert.addall(RpObjListToInsert);
                      }
                      //orgConList[0].status='Amend In Progress';
                      //itemstoInsert.add(orgConList[0]);
                      insert itemstoInsert;
                      
                      for(Id subsidaryAccountId : mapSubsidaryAccountToInsert.keyset()){
                          for(String relatedChildObjName : relListtoLink){
                              if(mapChildObjectWithRelName.containsKey(relatedChildObjName) && mapSubsidaryInfo.containsKey(subsidaryAccountId)){
                                  Sobject obj=mapSubsidaryInfo.get(subsidaryAccountId);
                                  list<SObject> recordChildToUpdate = obj.getSObjects(mapChildObjectWithRelName.get(relatedChildObjName));
                                  if(recordChildToUpdate != null){
                                      for(sObject objChild : recordChildToUpdate){
                                          sObject newChildObj = objChild.clone(false, true);
                                          newChildObj.put('Subsidiary_Account__c',mapSubsidaryAccountToInsert.get(subsidaryAccountId).Id);
                                          itemsSAtoInsert.add(newChildObj);
                                      }
                                  }
                              }
                              
                          }
                      }
                      insert itemsSAtoInsert;
                  }
                  
                  //outMap.put('Error', 'OK');
               } 
        }Catch(exception e){
            outMap.put('error', string.valueof(e));
            return outMap;
        }
 
        outMap.put('error', 'OK');
        return outMap;
        
    }
    
    public static map<string,Object> getFieldMappings(map<string,Object> inputData){
        Map<string,Object> outputMap = new map<string,Object>();
        string sourceObjName = (string) inputData.get('sourceObjName');
        string destObjName = (string) inputData.get('destObjName');
        map<string,string> mapSourceDestinationFieldsMap = new Map<string,string>();
        set<string> destFieldSet=new set<string>{'Id'};  
            set <string> SourceFieldSet = new set<string>();
        list <string> lstSrcObjFields = new list<string>();
        
        
        list<vlocity_cmt__CustomFieldMap__c> lstFieldMapContractToDSR=[select vlocity_cmt__DestinationFieldName__c,vlocity_cmt__DestinationSObjectType__c,vlocity_cmt__SourceFieldName__c,
                                                                       vlocity_cmt__SourceFieldSObjectType__c  ,vlocity_cmt__SourceFieldType__c,vlocity_cmt__SourceSObjectType__c from vlocity_cmt__CustomFieldMap__c where vlocity_cmt__SourceSObjectType__c= :sourceObjName and vlocity_cmt__DestinationSObjectType__c=:destObjName];
        
        for(vlocity_cmt__CustomFieldMap__c objCustomFieldMap : lstFieldMapContractToDSR){
            destFieldSet.add(objCustomFieldMap.vlocity_cmt__DestinationFieldName__c);
            SourceFieldSet.add(objCustomFieldMap.vlocity_cmt__SourceFieldName__c);
            //destFieldSet.add('Contract__r.'+objCustomFieldMap.vlocity_cmt__SourceFieldName__c);
            mapSourceDestinationFieldsMap.put(objCustomFieldMap.vlocity_cmt__SourceFieldName__c,objCustomFieldMap.vlocity_cmt__DestinationFieldName__c);
            
        } 
        
        System.debug('mapSourceDestinationFieldsMap:: ' + mapSourceDestinationFieldsMap);
        lstSrcObjFields.addall(SourceFieldSet);
        outputMap.put('mapSourceDestinationField',mapSourceDestinationFieldsMap );
        outputMap.put('lstSrcObjFields', lstSrcObjFields);
        
        return outputMap;
        
        
    }
    
    public static list<sObject> updateTrgObjFields(map<string,Object> inputData){
        list<sObject> destObjList = new list<sObject>();
        list<sObject> srcObjectList = new list<sObject>();
        list<sObject> objTypeList = new list<sObject>();
        String contextObjName ;
        id ObjId;
        map<string,string> mapSourceDestinationFieldsMap = new Map<string,string>();
        srcObjectList = new list<sObject>();
       
        id dsrID = (id)inputData.get('DSRID');
        srcObjectList = (list<sObject>)inputData.get('srcObjectList');
		mapSourceDestinationFieldsMap = (map<string,string>)inputData.get('mapSourceDestinationFieldsMap');
               
        system.debug('srcObjectList' + srcObjectList);
        if(srcObjectList != null && srcObjectList.size() >0 ){
            objID = (id)srcObjectList[0].get('id');
            
            
            Schema.SObjectType contextObjType =  Id.valueOf(objID).getSObjectType();
            contextObjName = contextObjType.getDescribe().getName();
            system.debug('contextObjName::'+ contextObjName);
        }
        List<SObject> items = new List<SObject> ();
        
        if(contextObjName.equalsignoreCase('Contract')){
            objTypeList = (list<contract>)srcObjectList;
        }
        else if(contextObjName.equalsignoreCase('vlocity_cmt__ContractLineItem__c')){
            objTypeList = (list<vlocity_cmt__ContractLineItem__c > )srcObjectList;
            
        }

        string ObjectName = (string)inputData.get('ObjectName');
        system.debug('ObjectName' + ObjectName);
        
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        Schema.SobjectType oType = gd.get(ObjectName);
       // sobject destObj = oType.newSObject();
        system.debug('oType' + oType);
        
        if(objTypeList != null && objTypeList.size()>0){
            for (sObject src: objTypeList){
                sobject dsObj = oType.newSObject();
                for(string sSourceField : mapSourceDestinationFieldsMap.keyset()){
                    if(src != null && mapSourceDestinationFieldsMap.containsKey(sSourceField)){
                        dsObj.put(mapSourceDestinationFieldsMap.get(sSourceField), src.get(sSourceField));
                        
                    }                       
                }
 
                if((string.valueof(oType)).equalsignorecase('Rate_Plan_Item__c'))
                    dsObj.put('Deal_Support__c', dsrID);
                if((string.valueof(oType)).equalsignorecase('Offer_House_Demand__c') ){
                    dsObj.put('TYPE_OF_CONTRACT__c','Wireless Amendment');
                   
                }
                destObjList.add(dsObj);
                
            }
        }
        return destObjList;
        
    }

}