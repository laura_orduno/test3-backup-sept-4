global without sharing class AgreementAccountHierarchyController {
       
    global static final integer DEFAULT_PAGE_SIZE = 25;
    
    private static integer m_pageSize = 0;
    
    global static integer pageSize {
        get {
            if (m_pageSize == 0) {
                m_pageSize = DEFAULT_PAGE_SIZE;
                
                smb_Application_Settings__c setting = smb_Application_Settings__c.getInstance('NumAccountsToPreloadInHierarchy');
            
                if (setting != null && setting.value__c != null) {
                    try {
                        m_pageSize = integer.valueOf(setting.value__c);
                    }
                    catch (Exception ex) {}
                }
            }
            
            return m_pageSize;
        }
    }
    
    @RemoteAction
    global static AgreementAccountHierarchyController.AccountNode getRootNode(Id accountId) {
        return getRootAccount(accountId);
    }
    
    @RemoteAction
    global static List<AgreementAccountHierarchyController.AccountNode> getChildNodes(Id parentId,Integer page) {
        Integer offset = page * pageSize;
        System.debug(LoggingLevel.ERROR, 'getChildNodes :parentId ' + parentId);
        List<Account> accounts = [SELECT Id, Name, parentId, RecordType.DeveloperName, 
                                         CBUCID__c, RCID__c, CustProfId__c, CBUCID_RCID__c, Billing_System_ID__c, 
                                         BTN__c, CAN__c, BAN_CAN__c, Billing_Account_Active_Indicator__c
                                  FROM Account
                                  WHERE ParentId = :parentId and Billing_Account_Active_Indicator__c != 'N'
                                  and inactive__c != true
                                  ORDER BY Billing_System_ID__c, CBUCID__c, RCID__c, CAN__c, BAN_CAN__c, Name 
                                  LIMIT :pageSize OFFSET :offset];
        System.debug(LoggingLevel.ERROR, 'getChildNodes :accounts ' + accounts);                         
        List<AccountNode> nodes = new List<AccountNode>();
        for (Account account : accounts) {        
            nodes.add(createAccountNode(account));
        }
        
        return nodes;
    }
    
    private static AccountNode getRootAccount(Id accountId) {
        List<Account> acclist = [SELECT Id, Name, parentId, RecordType.DeveloperName, 
                                         CBUCID__c, RCID__c, CustProfId__c, CBUCID_RCID__c, Billing_System_ID__c, 
                                         BTN__c, CAN__c, BAN_CAN__c, Billing_Account_Active_Indicator__c, 
                                         (SELECT Id, Name, Phone__c, AccountId
                                            FROM Assets
                                            WHERE Type__c = 'WTN'
                                            ORDER BY Name
                                          )
                                   FROM Account 
                                   WHERE Id = :accountId] ;
        return createAccountNode(acclist.get(0));
    }
   
    private static AccountNode createAccountNode(Account account) {
        AccountNode result = new AccountNode();
        
        result.recordId = account.Id;
        result.objectType = 'Account';
        result.parentId = account.parentId;
        result.active = (account.Billing_Account_Active_Indicator__c != 'N');
        result.recordName = account.Name;
        result.billingSystemName = getBillingSystemName(account.Billing_System_ID__c);
        result.recordTypeDeveloperName = account.RecordType.DeveloperName;
        result.accountNumber = smb_AccountUtility.getAccountNumber(account);
        result.pilotName = account.BTN__c;
         
        result.primaryAttributeLabel = smb_AccountUtility.getAccountNumberShortLabel(account.RecordType.DeveloperName);
        result.primaryAttributeValue = smb_AccountUtility.getAccountNumber(account);
        
        if ((account.RecordType.DeveloperName == 'CAN' || account.RecordType.DeveloperName == 'BAN'))
        {
            result.secondaryAttributeLabel = 'BTN';
            result.secondaryAttributeValue = account.BTN__c;
        }
        return result;
    }
    
    private static String getBillingSystemName(String billingSystemId){
        String billingSystemName = '';
        if (billingSystemId != null){           
            LEGACY_ACCOUNT_ID__c legacyAccountId = LEGACY_ACCOUNT_ID__c.getValues(billingSystemId);    
            if(legacyAccountId!=null) billingSystemName = legacyAccountId.Billing_System_Name__c;
        } 
        return billingSystemName;       
    }
    
    global class AccountNode {
        global List<AccountNode> children {get;set;}
        global boolean childrenLoaded {get;set;}
        
        global Id recordId {get;set;}
        global string objectType {get;set;}
        global Id parentId {get;set;}
        
        global boolean active {get;set;}
        
        global string recordTypeDeveloperName {get;set;}
        global string accountNumber {get;set;}
        global string pilotName {get;set;}
        
        global boolean thisOrChildselected {get;set;}
        
        global string recordNamePrefix {get;set;}
        global string recordName {get;set;}
        
        global string billingSystemName {get;set;}        
        
        global string primaryAttributeLabel {get;set;}
        global string primaryAttributeValue {get;set;}
        
        global string secondaryAttributeLabel {get;set;}
        global string secondaryAttributeValue {get;set;}
        
        global AccountNode() {
            this.childrenLoaded = false;
            this.thisOrChildselected = false;
            
            this.objectType = '';
            
            this.recordTypeDeveloperName = '';
            this.accountNumber = '';
            this.pilotName = '';
            
            this.recordNamePrefix = '';
            this.recordName = '';
            
            this.billingSystemName = '';            
            
            this.primaryAttributeLabel = '';
            this.primaryAttributeValue = '';
        
            this.secondaryAttributeLabel = '';
            this.secondaryAttributeValue = '';
        }
    }
    
}