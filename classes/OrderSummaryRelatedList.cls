global without sharing class OrderSummaryRelatedList {
    
    public transient List<OrderItem> NewOrderLineItems {get;set;} 
    public Order order;
    public String parentId;
    public static Order ordObjFromContext=null;
    public static List<OrderItem> orderItemsFromContext=null;
    public static List<Event> eventQueryListFromContext=null;
    public static List<Note> noteQueryListFromContext=null;
    public static String orderIdContext {get;set;}
    public string shortOrderId{get{
        if(Order!=null && String.isNotBlank(Order.Id) && String.valueOf(Order.Id).length()>15){
          return String.valueOf(Order.Id).substring(0, 15);  
        }
        return order.Id;        
    }
                              }
    public string shortSalesRepresentativeId{
        get{            
          /*  if(salesRepresentative != null && salesRepresentative.Id != null){
                return String.valueOf(salesRepresentative.Id).substring(0, 15);
            }else{ return  null ;}
*/return  null ;
        }
    }
    public List<OCOM_NoteAttachment> noteAttachmentList{get;set;}
    public string deleteNoteAttachmentId{get;set;}
    public string deleteNoteAttachmentType{get;set;}
    public Boolean hasNotesAttachments{get{ 
        if (noteAttachmentList!=null && noteAttachmentList.size()>0){
            return true;
        } else {
            return false;
        }
    }}
    public string deleteOrderLineItemId{get;set;}
    public Boolean hasNewOrderLineItems{get{
        if (NewOrderLineItems!=null && NewOrderLineItems.size()>0){
            return true;
        } else {
            return false;
        }
    }}
    public List<OCOM_TaskEvent> taskEventList{get;set;}
    public List<OCOM_TaskEvent> openTaskEventList{get;set;}
    public List<OCOM_TaskEvent> taskEventHistoryList{get;set;}
    public string deleteTaskEventId{get;set;}
    public string deleteTaskEventType{get;set;}
    public Boolean hasTasksEvents{get{ 
        if (taskEventList!=null && taskEventList.size()>0){
            return true;
        } else {
            return false;
        }
    }}
    public Boolean hasOpenTasksEvents{get{ 
        if (openTaskEventList!=null && openTaskEventList.size()>0){
            return true;
        } else {
            return false;
        }
    }}
    public Boolean hasTasksEventsHistory{get{ 
        if (taskEventHistoryList!=null && taskEventHistoryList.size()>0){
            return true;
        } else {
            return false;
        }
    }}
    public List<Contract> contractList{get;set;}
    public string deleteContractId{get;set;}
    public Boolean hasContract{get{ 
        if (contractList!=null && contractList.size()>0){
            return true;
        } else {
            return false;
        }
    }}
    public List<Work_Order__c> workOrderList{get;set;}
     public List<Work_Request__c> workRequestList {get; set;}
     public Boolean hasWorkRequests{get{ 
        if (workRequestList!=null && workRequestList.size()>0){
            return true;
        } else {
            return false;
        }
    }}
    public string deleteWorkOrderId{get;set;}
    public Boolean hasWorkOrders{get{ 
        if (workOrderList!=null && workOrderList.size()>0){
            return true;
        } else {
            return false;
        }
    }}
    public List<Case> caseList{get;set;}
    public Boolean hasCases{get{ 
        if (caseList!=null && caseList.size()>0){
            return true;
        } else {
            return false;
        }
    }}
    
   // Samir START BSBD_RTA-961 merged missing Order Team Member Section from Order 1.0 
   
    public List<Cloud_Enablement_Team_Member__c> orderTeamMemberList {get ;set;}
    public Boolean hasOrderTeamMembers {get;set;}
    @TestVisible 
    private void getOrderTeamMembers() {
        String pOrderId=ApexPages.currentPage().getParameters().get('parentOrderId');
        System.debug('OrderSummaryRelatedList - getOrderTeamMembers parentOrderId:'+pOrderId);
        System.debug('OrderSummaryRelatedList - getOrderTeamMembers order id:'+order.Id);
        //System.debug('OrderSummaryRelatedList - getOrderTeamMembers id:'+ApexPages.currentPage().getParameters().get('id'));
        //String childOrderId=ApexPages.currentPage().getParameters().get('childOrderId');
       // System.debug('OrderSummaryRelatedList - childOrderId:'+childOrderId);
        
        try {
            orderTeamMemberList = [select id,name,Order__c,Team_Member__c,Team_Member__r.name,Team_Name__c
                                   from Cloud_Enablement_Team_Member__c where Order__c != null 
                                   AND Order__r.parentId__c=:pOrderId order by Team_Member__r.name desc];
        }
        catch(Exception e) {}
        if(orderTeamMemberList == null)
            orderTeamMemberList = new List<Cloud_Enablement_Team_Member__c>();
        hasOrderTeamMembers = orderTeamMemberList.isEmpty() == false;
    }
    
    public String deleteTeamMemberId {get;set;}
    
    public void deleteOrderTeamMember() {
	    if (String.isNotEmpty(deleteTeamMemberId)) {
	        List<Cloud_Enablement_Team_Member__c> delTeamMembers = [SELECT id FROM Cloud_Enablement_Team_Member__c WHERE id = :deleteTeamMemberId];
	        if(delTeamMembers.size() > 0){
	            delete delTeamMembers;
	        }
	    }
	    getOrderTeamMembers();
    }
   // Samir END BSBD_RTA-961 merged missing Order Team Member Section from Order 1.0
     
    // Thomas QC-9301 End    
    public PageReference loadRelatedList(){
        
        System.debug('orderIdContext is '+orderIdContext);
        List<Order> ordList=[select id,recordtype.developername from order where id=:orderIdContext or parentId__c=:orderIdContext ];
        
        Integer i=0;
        Order singleSiteOrdObj=null;
        
        for(Order ordObj:ordList){
            if('Customer_Solution'.equalsIgnoreCase(ordObj.RecordType.developername)){
                continue;
            }
            singleSiteOrdObj=ordObj;
            i++;
        }
        if(i==1){
            if(singleSiteOrdObj!=null){
               this.order=singleSiteOrdObj; 
            }            
        }
        if(this.order==null){
            this.order = [select id from order where id=:orderIdContext];     
        }
            
            getNewOrderLineItems(); 
            workRequestList=[select Name, Type__c, Status__c, Account__c, CreatedDate, Opportunity__c, Account_Owner__c from Work_Request__c where Order__c=:ApexPages.currentPage().getParameters().get('parentOrderId')];
            getContracts();
            getWorkOrders();
            getCases();
            getNotesAttachments();
            getTasksEvents();
        return null;
    }   
    
    global OrderSummaryRelatedList(ApexPages.StandardController stdController){
        System.debug('OrderSummaryRelatedList - orderIdContext is in constructor '+orderIdContext);
        parentId = ApexPages.currentPage().getParameters().get('parentOrderId'); 
        this.order = (Order)stdController.getRecord();
        if(this.order==null){
           this.order=[select id from order where parentid__C=:parentId] ;
        }
       // ApexPages.currentPage().getParameters().put('childOrderId',this.order.id); 
        if(NewOrderLineItems == null){
            getNewOrderLineItems(); 
        }
        if(workRequestList==null){
            workRequestList=[select Name, Type__c, Status__c, Account__c, CreatedDate, Opportunity__c, Account_Owner__c from Work_Request__c where Order__c=:ApexPages.currentPage().getParameters().get('parentOrderId')];
        }
        
        if (contractList == null)
            getContracts();
        if (workOrderList == null)
            getWorkOrders();
        if (caseList == null)
            getCases();
        if (noteAttachmentList == null)
            getNotesAttachments();
        if (taskEventList == null)
            getTasksEvents();
		// Samir BSBD_RTA-961 merged missing Order Team Member Section from Order 1.0             
        getOrderTeamMembers();
       
    }
    @TestVisible
    private void getNotesAttachments() {
        if(order==null || order.Id==null){
            return;
        }
        system.debug('OCOM_OrderSubmitController::getNotesAttachments() order Id('+order.Id+')');
        noteAttachmentList = new List<OCOM_NoteAttachment>();
        OCOM_NoteAttachment aNoteAttachment;
        List<Note> noteQueryList=null;
        if(noteQueryListFromContext==null){
            noteQueryList = [SELECT id, title, isdeleted, CreatedDate, LastModifiedDate, CreatedById, CreatedBy.name
                             FROM note
                             WHERE parentid = :order.Id];
            noteQueryListFromContext=noteQueryList;
        } else {
            noteQueryList=noteQueryListFromContext;
        }
        /* List<Note> noteQueryList = [SELECT id, title, isdeleted, CreatedDate, LastModifiedDate, CreatedById, CreatedBy.name
FROM note
WHERE parentid=:order.Id];*/
        system.debug('OCOM_OrderSubmitController::getNotesAttachments() noteQueryList('+noteQueryList+')');
        for (Note aNote: noteQueryList) {
            if (aNote.isdeleted == false) {
                aNoteAttachment = new OCOM_NoteAttachment();
                aNoteAttachment.id = aNote.id;
                aNoteAttachment.type = 'Note';
                aNoteAttachment.typeDisplay = Label.OCOM_Note;
                aNoteAttachment.title = aNote.title;
                aNoteAttachment.createdDate = aNote.createdDate;
                aNoteAttachment.lastModifiedDate = aNote.LastModifiedDate.format();
                aNoteAttachment.createdByName = aNote.CreatedBy.name;
                aNoteAttachment.createdBy = aNote.CreatedById;
                noteAttachmentList.add(aNoteAttachment);
            }
        }
        List<Attachment> attachmentQueryList = [SELECT id, name, isdeleted, CreatedDate, LastModifiedDate, CreatedById, CreatedBy.name
                                                FROM attachment
                                                WHERE parentid=:order.Id];
        system.debug('OCOM_OrderSubmitController::getNotesAttachments() attachmentQueryList('+attachmentQueryList+')');
        for (Attachment anAttachment: attachmentQueryList) {
            if (anAttachment.isdeleted == false) {
                aNoteAttachment = new OCOM_NoteAttachment();
                aNoteAttachment.id = anAttachment.id;
                aNoteAttachment.type = 'Attachment';
                aNoteAttachment.typeDisplay = Label.OCOM_Attachment;
                aNoteAttachment.title = anAttachment.name;
                aNoteAttachment.createdDate = anAttachment.createdDate;
                aNoteAttachment.lastModifiedDate = anAttachment.LastModifiedDate.format();
                aNoteAttachment.createdByName = anAttachment.CreatedBy.name;
                aNoteAttachment.createdBy = anAttachment.CreatedById;
                noteAttachmentList.add(aNoteAttachment);
            }
        }
        system.debug('OCOM_OrderSubmitController::getNotesAttachments() unsorted noteAttachmentList('+noteAttachmentList+')');
        noteAttachmentList.sort();
        system.debug('OCOM_OrderSubmitController::getNotesAttachments() sorted noteAttachmentList('+noteAttachmentList+')');
    }
    @TestVisible
    private void getTasksEvents() {
        if(order==null || order.Id==null){
            return;
        }
        system.debug('OCOM_OrderSubmitController::getTasksEvents() order Id('+order.Id+')');
        taskEventList = new List<OCOM_TaskEvent>();
        openTaskEventList = new List<OCOM_TaskEvent>();
        taskEventHistoryList = new List<OCOM_TaskEvent>();
        OCOM_TaskEvent aTaskEvent;
        List<Task> taskQueryList = [SELECT id, subject, whoid, who.name, ActivityDate, status, priority, ownerid, owner.name, createddate, lastmodifieddate, description
                                    FROM task
                                    WHERE whatid=:order.Id];
        system.debug('OCOM_OrderSubmitController::getTasksEvents() taskQueryList('+taskQueryList+')');
        for (Task aTask: taskQueryList) {
            aTaskEvent = new OCOM_TaskEvent();
            aTaskEvent.id = aTask.id;
            aTaskEvent.subject = aTask.subject;
            aTaskEvent.whoId = aTask.whoId;
            aTaskEvent.whoName = aTask.who.name;
            aTaskEvent.isTask = true;
            if (aTask.ActivityDate != null) {
                aTaskEvent.dueDate = aTask.ActivityDate.format();
            }
            if (aTask.lastmodifieddate != null) {
                aTaskEvent.lastModifiedDate = aTask.lastmodifieddate.format();
            }
            aTaskEvent.status = aTask.status;
            aTaskEvent.priority = aTask.priority;
            aTaskEvent.ownerId = aTask.ownerId;
            aTaskEvent.ownerName = aTask.owner.name;
            aTaskEvent.createdDate = aTask.createdDate;
            if (String.isNotEmpty(aTask.description)) {
                if (aTask.description.length()>256) {
                    aTaskEvent.summary = aTask.description.substring(0,255)+'...';
                } else {
                    aTaskEvent.summary = aTask.description;
                }
            }
            taskEventList.add(aTaskEvent);
            if (String.isNotBlank(aTask.status)) {
                if (aTask.status.equals('Completed')) {
                    taskEventHistoryList.add(aTaskEvent);
                } else {
                    openTaskEventList.add(aTaskEvent);
                }
            }
        }
        List<Event> eventQueryList=null;
        if(eventQueryListFromContext==null){
            eventQueryList = [SELECT  id, subject, whoid, who.name, EndDateTime, ownerid, owner.name, createddate, lastmodifieddate
                              FROM event
                              WHERE whatid=:order.Id];
            eventQueryListFromContext=eventQueryList;
        } else {
            eventQueryList=eventQueryListFromContext;
        }
        /* List<Event> eventQueryList = [SELECT  id, subject, whoid, who.name, EndDateTime, ownerid, owner.name, createddate, lastmodifieddate
FROM event
WHERE whatid=:order.Id];*/
        system.debug('OCOM_OrderSubmitController::getTasksEvents() eventQueryList('+eventQueryList+')');
        for (Event anEvent: eventQueryList) {
            aTaskEvent = new OCOM_TaskEvent();
            aTaskEvent.id = anEvent.id;
            aTaskEvent.subject = anEvent.subject;
            aTaskEvent.whoId = anEvent.whoId;
            aTaskEvent.whoName = anEvent.who.name;
            aTaskEvent.isTask = false;
            if (anEvent.EndDateTime != null) {
                aTaskEvent.dueDate = anEvent.EndDateTime.format();
            }
            if (anEvent.lastmodifieddate != null) {
                aTaskEvent.lastModifiedDate = anEvent.lastmodifieddate.format();
            }
            aTaskEvent.ownerId = anEvent.ownerId;
            aTaskEvent.ownerName = anEvent.owner.name;
            aTaskEvent.createdDate = anEvent.createdDate;
            taskEventList.add(aTaskEvent);
            if (anEvent.EndDateTime != null && anEvent.EndDateTime<system.today()) {
                taskEventHistoryList.add(aTaskEvent);
            } else {
                openTaskEventList.add(aTaskEvent);
            }
        }
        system.debug('OCOM_OrderSubmitController::getTasksEvents() unsorted taskEventList('+taskEventList+')');
        taskEventList.sort();
        system.debug('OCOM_OrderSubmitController::getTasksEvents() sorted taskEventList('+taskEventList+')');
    }
    @TestVisible
    private void getContracts() {
        if(order==null || order.Id==null){
            return;
        }
        system.debug('OCOM_OrderSubmitController::getContracts() order Id('+order.Id+')');
        contractList = new List<Contract>();
        List<Contract> contractQueryList = [SELECT Id, ContractNumber, name, status, AccountId, Account.name
                                            FROM contract 
                                            WHERE vlocity_cmt__OrderId__c=:order.Id];
        system.debug('OCOM_OrderSubmitController::getContracts() contractQueryList('+contractQueryList+')');
        for (Contract aContract: contractQueryList) {
            contractList.add(aContract);
        }
        system.debug('OCOM_OrderSubmitController::getContracts() contractList('+contractList+')');
    }
    @TestVisible
    private void getWorkOrders() {
        system.debug('OCOM_OrderSubmitController::getWorkOrders() order Id('+order.Id+')');
        if(order==null || order.Id==null){
            return;
        }
        workOrderList = new List<Work_Order__c>();
        List<Work_Order__c> workOrderQueryList = [SELECT id, WFM_Number__c, name, Scheduled_Datetime__c, Product_Name__c, Permanent_Comments__c, Cancellation_Remarks__c, Duration__c, Location_Contact__c, Location_Contact__r.Name, Status__c,
                                                  Onsite_Contact_Name__c,Onsite_Contact_Email__c,Onsite_Contact_CellPhone__c // QC-11081(CR)
                                                  FROM Work_Order__c
                                                  WHERE Order__c=:order.Id limit 49999];
        system.debug('OCOM_OrderSubmitController::getWorkOrders() workOrderQueryList('+workOrderQueryList+')');
        for (Work_Order__c aWorkOrder: workOrderQueryList) {
            if (String.isNotEmpty(aWorkOrder.Permanent_Comments__c)) {
                if (aWorkOrder.Permanent_Comments__c.length()>256) {
                    aWorkOrder.Permanent_Comments__c = aWorkOrder.Permanent_Comments__c.substring(0,255)+'...';
                } else {
                    aWorkOrder.Permanent_Comments__c = aWorkOrder.Permanent_Comments__c;
                }
            }
            if (String.isNotEmpty(aWorkOrder.Cancellation_Remarks__c)) {
                if (aWorkOrder.Cancellation_Remarks__c.length()>256) {
                    aWorkOrder.Cancellation_Remarks__c = aWorkOrder.Cancellation_Remarks__c.substring(0,255)+'...';
                } else {
                    aWorkOrder.Cancellation_Remarks__c = aWorkOrder.Cancellation_Remarks__c;
                }
            }
            workOrderList.add(aWorkOrder);
        }
        system.debug('OCOM_OrderSubmitController::getWorkOrders() workOrderList('+workOrderList+')');
    }
    @TestVisible
    private void getCases() {
        if(order==null || order.Id==null){
            return;
        }
        system.debug('OCOM_OrderSubmitController::getCases() order Id('+order.Id+')');
        caseList = new List<Case>();
        List<Case> caseQueryList = [SELECT id, RecordTypeId, RecordType.Name, Record_Type_Name__c, CaseNumber, Subject, Status, Priority, Current_Owner__c, Current_Owner__r.Name, ContactId, Contact.Name, AccountId, Account.Name, CreatedDate, ClosedDate
                                    FROM Case
                                    WHERE Order__c=:order.Id];
        system.debug('OCOM_OrderSubmitController::getCases() caseQueryList('+caseQueryList+')');
        for (Case aCase: caseQueryList) {
            caseList.add(aCase);
        }
        system.debug('OCOM_OrderSubmitController::getCases() caseList('+caseList+')');
    }
    // Thomas QC-9301 End
    
    // Thomas QC-9301 Start
    public void deleteOrderLineItem(){
        if (String.isNotEmpty(deleteOrderLineItemId)) {
            List<OrderItem> deleteOrderLineItemList = [SELECT id FROM orderitem WHERE id = :deleteOrderLineItemId];
            if(deleteOrderLineItemList.size() > 0){
                delete deleteOrderLineItemList;
            }
        }
        
    }
    
    public void deleteContract(){
        if (String.isNotEmpty(deleteContractId)) {
            List<Contract> deleteContractList = [SELECT id FROM contract WHERE id = :deleteContractId];
            if(deleteContractList.size() > 0 ){
                delete deleteContractList;
            }
        }
        if(!Test.isRunningTest()){
            getContracts();
        }
    }
    
    public void deleteWorkOrder(){
        if (String.isNotEmpty(deleteWorkOrderId)) {
            List<Work_Order__c> deleteWorkOrderList = [SELECT id FROM Work_Order__c WHERE id = :deleteWorkOrderId];
            if(deleteWorkOrderList.size() > 0 ){
                delete deleteWorkOrderList;
            }
        }
        
        getWorkOrders();
        
    }
    
    public void deleteTaskEvent(){
        List<Event> deleteEventList=null;
        List<Task> deleteTaskList=null;
        if (String.isNotEmpty(deleteTaskEventId) && String.isNotEmpty(deleteTaskEventType)) {
            if (deleteTaskEventType.equals('false')) {
                deleteEventList = [SELECT id FROM Event WHERE id = :deleteTaskEventId];
            }
            if (deleteTaskEventType.equals('true')) {
                deleteTaskList = [SELECT id FROM Task WHERE id = :deleteTaskEventId];
                
            }
        }
        if(deleteEventList!=null && deleteEventList.size() > 0 ){
            delete deleteEventList;
        }
        if(deleteTaskList!=null && deleteTaskList.size() > 0 ){
            delete deleteTaskList;
        }
        
        /*  
if (String.isNotEmpty(deleteTaskEventId) && String.isNotEmpty(deleteTaskEventType)) {
if (deleteTaskEventType.equals('false')) {
List<Event> deleteEventList = [SELECT id FROM Event WHERE id = :deleteTaskEventId];
if(deleteEventList.size() > 0 && !Test.isRunningTest()){
delete deleteEventList;
}
} else if (deleteTaskEventType.equals('true')) {
List<Task> deleteTaskList = [SELECT id FROM Task WHERE id = :deleteTaskEventId];
if(deleteTaskList.size() > 0 && !Test.isRunningTest()){
delete deleteTaskList;
}
}
}*/
        
            getTasksEvents();
        
    }
    
    public void deleteNoteAttachment() {
        if (String.isNotEmpty(deleteNoteAttachmentId) && String.isNotEmpty(deleteNoteAttachmentType)) {
            if (deleteNoteAttachmentType.equals('Note') ) {
                List<Note> deleteNoteList = [SELECT id FROM Note WHERE id = :deleteNoteAttachmentId];
                if(deleteNoteList.size() > 0 ){
                    delete deleteNoteList;
                }
            } else if (deleteNoteAttachmentType.equals('Attachment') ) {
                List<Attachment> deleteAttachmentList = [SELECT id FROM Attachment WHERE id = :deleteNoteAttachmentId];
                if(deleteAttachmentList.size() > 0 ){
                    delete deleteAttachmentList;
                }
            }
            
                getNotesAttachments(); 
                        
        }
    }
  
     @TestVisible    
    private void getNewOrderLineItems() {
       // System.debug('@@@getNewOrderLineItems order Id' + order.Id);
        if(order==null || order.Id==null){
            return;
        }
        NewOrderLineItems = new List<OrderItem>();
        List<OrderItem> orderItemRecs = [Select Id,
                vlocity_cmt__LineNumber__c,
                pricebookentry.Name,  
                PriceBookEntry.Product2.Name,
                PriceBookEntry.Product2.ProductCode,
                PriceBookEntry.Product2.Id,
                PriceBookEntry.product2.ProductSpecification__c,
                PriceBookEntry.product2.ProductSpecification__r.Name,
                PriceBookEntry.product2.OCOM_Shippable__c,
                PriceBookEntry.product2.IncludeQuoteContract__c,
                PriceBookEntry.product2.Catalog__c, 
                PriceBookEntry.product2.Catalog__r.Name,
                PriceBookEntry.product2.orderMgmtId__c,
                orderMgmtId_Status__c,
                Previous_JSONAttribute__c,
                pricebookentry.product2.vlocity_cmt__JSONAttribute__c,    
                ListPrice,
                UnitPrice,orderid,
                Quantity,
                vlocity_cmt__ParentItemId__c,
                vlocity_cmt__JSONAttribute__c,
                vlocity_cmt__AssetReferenceId__c,
                vlocity_cmt__AssetId__c,
                vlocity_cmt__OneTimeCharge__c,
                vlocity_cmt__OneTimeCalculatedPrice__c,
                vlocity_cmt__OneTimeManualDiscount__c,
                vlocity_cmt__OneTimeDiscountPrice__c,
                vlocity_cmt__OneTimeTotal__c,                                
                vlocity_cmt__RecurringCharge__c,
                vlocity_cmt__RecurringCalculatedPrice__c,
                vlocity_cmt__RecurringManualDiscount__c,
                vlocity_cmt__RecurringDiscountPrice__c,
                vlocity_cmt__RecurringTotal__c,
                vlocity_cmt__RootItemId__c,
                vlocity_cmt__ProvisioningStatus__c,
                vlocity_cmt__ServiceAccountId__r.Name,
                vlocity_cmt__BillingAccountId__r.Name,
                Shipping_Address1__c,
                Shipping_Address_Contact__c,
                Work_Order__c,
                Assigned_To__c, LastModifiedDate, Legacy_Order_ID__c,Pricebookentry.Product2.Family,PriceBookEntry.product2.Sellable__c,Pricebookentry.Product2.vlocity_cmt__TrackAsAgreement__c,
                amendStatus__c, orderMgmtId_SubStatus__c
                FROM OrderItem
                WHERE OrderId = :order.Id
                ORDER BY vlocity_cmt__LineNumber__c];
            for (OrderItem li: orderItemRecs ) {
              NewOrderLineItems.add(li);
                    
            }
        
    }       

    
}