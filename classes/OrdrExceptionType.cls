/**
 * @author Santosh Rath
 *
 */
public class OrdrExceptionType { 
   public static final String UNKNOWN_SYSTEM_EXCEPTION='100';
   public static final String APPLICATION_EXCEPTION='110';
	public static final String CONNECTION_EXCEPTION='120';
	public static final String INTEGRATION_EXCEPTION='130';
	public static final String SECURITY_EXCEPTION='140';
	public static final String UNKNOWN_POLICY_EXCEPTION='200';
	public static final String DATA_VALIDATION_EXCEPTION='210';
	public static final String MISSING_PARAMETER_EXCEPTION='220';
	public static final String OBJECT_NOT_FOUND_EXCEPTION='230';
	public static final String MULTIPLE_OBJECT_FOUND_EXCEPTION='240';
	
	public static final String UNKNOWN_EXCEPTION='999';
	public static final String SOA_EXCEPTION ='1000';
	
}