global class ProductTeamMemberService {
	
	/*
	Takes an Id of a ProductTeamMember record.
	1. Gets all records on that opp where this user has been added as a member and deletes it.
	2. Removes the user from the opp Sales Team. This should also remove the user from the Sharing Setting.
	*/
	webservice static void deleteMember(Id productTeamId) {
		if (productTeamId == null) {
			return;
		}
		List<Product_Sales_Team__c> l = [Select Id, Product__r.Opportunity__c, Member__c from Product_Sales_Team__c where Id = :productTeamId];
		if (l.isEmpty()) {
			return;
		}
		Product_Sales_Team__c pst = l.get(0);
		Id memberId = pst.Member__c;
		List<Product_Sales_Team__c> pstList = [Select Id from Product_Sales_Team__c where Member__c = :memberId And Product__r.Opportunity__c = :pst.Product__r.Opportunity__c];
		if (! pstList.isEmpty()) {
			delete pstList;
		}
		//also need to remove the member from the opp sales team
		List<OpportunityTeamMember> ostList = [Select Id from OpportunityTeamMember where UserId = :memberId And OpportunityId = :pst.Product__r.Opportunity__c];
		if (! ostList.isEmpty()) {
			delete ostList;
		}
	}
	
}