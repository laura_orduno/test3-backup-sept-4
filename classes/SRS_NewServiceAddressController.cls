/******************************************************************************
# File..................: SRS_NewServiceAddressController
# Version...............: 1
# Created by............: Hari Neelalaka (IBM)
# Created Date..........: 04-Jul-2014 
# Last Modified by......: Hari Neelalaka (IBM)
# Last Modified Date....: 19-Jan-2015
# Modification:           To log the error in the web service log during webservice callout exception, invoked SMB_WebserviceHelper
# Description...........: Class is to Create, Validate, Save Service Address
#
# Changed - Oct 16,2015 -  Added PAC (Predictive Address Capture) code by Sukhdeep Singh / Sandip Chaudhari
# Changed - Jan 21,2016 -  Added PAC R2 code by Sandip Chaudhari
******************************************************************************/
public class SRS_NewServiceAddressController {
    // ------------------------------------ PAC code by Sukhdeep Singh ---------------------------------------------------
     public ServiceAddressData SAData {get;set;}
     public String pacOuterChkVal {get;set;}
     public String pacJSconfigName {get;set;}
     public String pacOuterSACGVal{get;set;}
     // ------------------------------------ PAC code by Sukhdeep Singh ---------------------------------------------------

    public SRS_Service_Address__c srsSrvcAddr {get; set;}
    public SMBCare_Address__c smbCareAddr {get; set;}
    public Id SRId = ApexPages.currentPage().getParameters().get('SRID');
    public Id SAId = ApexPages.currentPage().getParameters().get('Id');
    public Service_Request__c accId;
    SRS_Service_Address__c srsSrvcAddr2;  
    public List<SelectOption> locationOptions {get;set;}
    public Boolean renderShowandCancel{get;set;}    
    public Boolean bool {get;set;}
    public Boolean provboolean {get;set;}
    public Boolean cityFlag {get;set;}
    public Boolean streetFlag {get;set;}
    public Boolean aptFlag {get;set;}
    public Boolean houseFlag {get;set;}
    public Boolean noMatchFlag {get;set;}
    @TestVisible public List<wrapper> lstw {get;set;} // Creating List for Wrapper class
    public String str {get;set;}
    public String id {get;set;}    
    public String index {
            get;
            // *** setter is NOT being called ***
            set {
                index = value;               
            }
        }
    public Boolean CityorHouse;
    public String linkUnitNumber {get;set;}
    public String FMSvalue {get;set;}   
    public List<SMBCare_Address__c> addressId;
    String radioFields = NULL;
    
    /*
    * List option to display additional fields, 'when Send to SACG' selected.
    */
    
    public List<SelectOption> getItems() {
            List<SelectOption> options = new List<SelectOption>(); 
            options.add(new SelectOption('New Construction','New Construction')); 
            options.add(new SelectOption('Temporary - Job Phone','Temporary - Job Phone')); 
            return options; 
        }
    
    // Constructor called when page is accessed.    
    public SRS_NewServiceAddressController(ApexPages.StandardController con){
        // ------------------------------------ PAC code by Sukhdeep Singh ---------------------------------------------------
          SAData  = new ServiceAddressData();
          SAData.isAdressRequired = true;
          SAData.isProvinceRequired = true;
          SAData.isCityRequired = true;
        // ------------------------------------ PAC code by Sukhdeep Singh ---------------------------------------------------
     
        bool = false;
        provboolean = false;
        cityFlag = false;
        streetFlag = false;
        aptFlag = false;
        houseFlag = false;
        noMatchFlag = false; 
        renderShowandCancel= true;  
        
        // Get the URL for the current request.
        String currentRequestURL = ApexPages.currentPage().getUrl();        
        
        lstw = new List<wrapper>(); // Instantiate wrapper class or object
        smbCareAddr = new SMBCare_Address__c();
        smbCareAddr.Address_Type__c = 'Standard'; // Service Address Type defaults to Standard.
        smbCareAddr.Status__c = 'Pending Validation'; 
        smbCareAddr.recordtypeid = Schema.SObjectType.SMBCare_Address__c.getRecordTypeInfosByName().get('Address').getRecordTypeId();       
        system.debug('>>> Address Record Type: '+smbCareAddr.recordtypeid);
        srsSrvcAddr = (SRS_Service_Address__c)con.getRecord();
        locationOptions = new List<SelectOption>();
        Schema.DescribeFieldResult locationFieldDescription = SRS_Service_Address__c.Location__c.getDescribe();
        // For each picklist value, create a new select option
        for (Schema.Picklistentry picklistEntry: locationFieldDescription.getPicklistValues()){
            locationOptions.add(new SelectOption(pickListEntry.getValue(),pickListEntry.getLabel()));
            // obtain and assign default value
            if (picklistEntry.defaultValue){
                srsSrvcAddr.Location__c = pickListEntry.getValue();
            }  
        }
        

                
        if(SRId != NULL){                
           accId = [Select Id,Account_Name__c, PrimaryContact__c, Opportunity__c FROM Service_Request__c WHERE Id=:SRId]; 
           system.debug('### SOQL REsult ' + accId);
           //PAC R2 Change
           if(accId != null){
               smbCareAddr.Account__c = accId.Account_Name__c; 
               SAData.accountId = accId.Account_Name__c;
               SAData.opportunityId = accId.Opportunity__c;
               SAData.contactId = accId.PrimaryContact__c;
           }
        }
        else if(SAId != NULL){ 
            srsSrvcAddr2 =[Select Id,Name,Address__c,Service_Request__c, Demarcation_Location__c, End_Customer_Name__c, Building_Name__c, NPA_NXX__c, New_Building__c,  
                                 Location__c, Floor__c, CLLI__c, Existing_Working_TN__c, Rack__c, Site_Ready__c, Shelf__c,
                                 Service_Request__r.PrimaryContact__c, Service_Request__r.Opportunity__c, Service_Request__r.Account_Name__c
                          FROM SRS_Service_Address__c WHERE Id=:SAId limit 1];
            
            //PAC R2 Change
           SAData.accountId = srsSrvcAddr2.Service_Request__r.Account_Name__c;
           SAData.opportunityId = srsSrvcAddr2.Service_Request__r.Opportunity__c;
           SAData.contactId = srsSrvcAddr2.Service_Request__r.PrimaryContact__c;
                       
           smbCareAddr = [Select Id,Name,Account__c, Address__c, Block__c, City__c, Country__c, GPS_Coordinates__c,
                                  Lot__c, SRS_Meridian__c, Number__c, Plan__c, Postal_Code__c, Province__c, Range__c, Section__c, Street_Direction__c, SRS_Quarter__c, Street_Name__c, Street_Number__c, Subdivision_ID__c, Suite_Number__c, 
                                  Township__c, Type__c, UTM__c, Notes__c, Use_Addresses_Entered__c, Send_to_SACG__c, Type_of_Address__c,  
                                  Service_Location__c, Status__c, Address_Type__c, State__c,SRS_State__c, Google_Map_Link__c 
                           FROM SMBCare_Address__c WHERE Id=:srsSrvcAddr2.Address__c];
            
                radioFields = smbCareAddr.Type_of_Address__c;
        }         
    }
    
      // ------------------------------------ PAC code by Sukhdeep Singh ---------------------------------------------------

    
    /*
    * Created by :  Sukhdeep Singh on 21-Oct-2015
    * This method is map the PAC address values with the existing address object
    */
    public void MapPACAddress(ServiceAddressData SAData, SMBCare_Address__c smbCareAddr){
        smbCareAddr.City__c = SAData.city;
        smbCareAddr.Province__c = SAData.province;
        /*if(SAData.buildingNumber != null && SAData.buildingNumber != ''){
            smbCareAddr.Street_Name__c = SAData.buildingNumber + ' ' + SAData.street;
        }else{
            smbCareAddr.Street_Name__c = SAData.street;
        }*/
        smbCareAddr.Street_Name__c = SAData.address;
        smbCareAddr.Suite_Number__c = SAData.suiteNumber;
        smbCareAddr.Street_Direction__c = SAData.streetDirection;
        smbCareAddr.Postal_Code__c = SAData.postalCode;
        smbCareAddr.Country__c = SAData.country;
        smbCareAddr.Vector__c = SAData.vector; // 13 Feb 2016 - added as per regression testing
        srsSrvcAddr.Floor__c = SAData.aptNumber; // 01 March 2016 - added as per production defect 
        string npa = '';
        string nxx = '';
        if(SAData.npa != null && SAData.npa != 'null' && SAData.npa != ''){
            npa  = SAData.npa;
        }
        if(SAData.lowestNxx != null && SAData.lowestNxx != 'null' && SAData.lowestNxx != ''){
            nxx = SAData.lowestNxx;
        }
        
        if(npa !=''){
            srsSrvcAddr.NPA_NXX__c = npa;
        }
        
        if(npa !='' && nxx !=''){
           //08 March 2016 - Sandip - Commented below line as per Ticket TC0364606 and rewrite the line by removing slash from NPA and NXX
           // srsSrvcAddr.NPA_NXX__c = srsSrvcAddr.NPA_NXX__c + '/' + nxx; 
            srsSrvcAddr.NPA_NXX__c = srsSrvcAddr.NPA_NXX__c + nxx;
        }
        
        //srsSrvcAddr.CLLI__c = SAData.clliCode; //Sandip - 08 Feb 2016 - Commented as per CR raised during Regression testing
        FMSvalue = SAData.fmsId;
        
        /*srsSrvcAddr.COID__c = SAData.coid;
        srsSrvcAddr.LocalRoutingNumber__c = SAData.localRoutingNumber;
        srsSrvcAddr.SwitchNumber__c = SAData.switchNumber;
        srsSrvcAddr.TerminalNumber__c = SAData.terminalNumber;*/
        
        // Date - 21 Jan 2016 - Sandip - PAC R2 Change
        smbCareAddr.Send_to_SACG__c = SAData.isSACG;
        smbCareAddr.Notes__c = SAData.notesForSACGTeam;
        
        SAData.SACGType = '';
        if(SAData.isNewConstruction){
            SAData.SACGType =   System.Label.PACNewConstruction; // 'New Construction';
        }
        if(SAData.isTempJobPhone){
            if(SAData.SACGType != ''){
                SAData.SACGType = SAData.SACGType + ';' +  System.Label.PACTempJobPhome; //'Temporary Job Phone';
            }else{
                SAData.SACGType = System.Label.PACTempJobPhome; //'Temporary Job Phone';
            }
        }
        smbCareAddr.Type_of_Address__c = SAData.SACGType;
        smbCareAddr.Status__c = SAData.captureNewAddrStatus;
        /*if(SAData.isManualCapture){
            smbCareAddr.Status__c='Invalid';
        }else{
            smbCareAddr.Status__c='Valid';
        }*/
    }
    
     /*
    * Created by :  Sandip Chaudhari on 21-Jan-2015
    * This method used to send address for validation through SACG process.
    */
     
    public void processSACG(){
        if(SAData.isSACG == true){
            PACUtility utilObj = new PACUtility();
            utilObj.sendToSACG(SAData);
        }
    }
    
    public void populateData(){
        MapPACAddress(SAData, smbCareAddr);
    }
    
    /*
    public void showCheckAddress(){
        if(pacOuterSACGVal=='doCheck'){
            //smbCareAddr.Send_to_SACG__c=true; // Changed 25 Nov
            //SAData.isPostalCodeRequired = true;
            smbCareAddr.Status__c='Invalid';
        }else{
            //smbCareAddr.Send_to_SACG__c=false; // Changed 25 Nov
            //SAData.isPostalCodeRequired = false;
            smbCareAddr.Status__c='Pending Validation';
        }

    } */
    // ------------------------------------ PAC code by Sukhdeep Singh ---------------------------------------------------

    
    /*
    * Save button
    * Ensure all mandatory fields are entered for Save, 
    * save address as entered
    * display Service Address on Service Request.   
    */
    
    public PageReference save(){
       Savepoint sp = Database.setSavepoint();
       MapPACAddress(SAData, smbCareAddr); // MAP data from PAC component to this class
       try{
       bool = false; 
       //Commented as per PAC R2
       //smbCareAddr.Type_of_Address__c = radioFields;
       string recTypeId = Schema.SObjectType.SMBCare_Address__c.getRecordTypeInfosByName().get('Address').getRecordTypeid(); 
       smbCareAddr.recordtypeid = recTypeId;
       if(smbCareAddr.Address_Type__c=='USA'){
           smbCareAddr.Country__c='US'; 
       }
        if(srsSrvcAddr.id==null)
        {   
            if(FMSvalue!='' && FMSvalue!=null)
            {   
            List<SMBCare_Address__c> lstSMBAddresses=[select id,Address_Type__c,Suite_Number__c,City__c,Status__c,
                                        Street_Number__c,Province__c,Street_Name__c,Postal_Code__c,Street_Direction__c,
                                        Google_Map_Link__c,Lot__c,Block__c,Plan__c,SRS_Quarter__c,Section__c,Township__c,
                                        Range__c,Meridian__c,Subdivision_ID__c,SRS_Type__c,Number__c,Address__c,state__c,SRS_State__c,
                                        GPS_Coordinates__c,UTM__c,Country__c from SMBCare_Address__c 
                                        where FMS_Address_ID__c=:FMSvalue and recordtypeid=:recTypeId limit 1];
                system.debug('>>lstSMBAddresses:' + lstSMBAddresses);
                system.debug('>>FMSvalue:' + FMSvalue + ' ' + recTypeId);
                if(lstSMBAddresses!=null && lstSMBAddresses.size()>0)
                {
                    
                    lstSMBAddresses[0].Address_Type__c=(smbCareAddr.Address_Type__c!=''?smbCareAddr.Address_Type__c:lstSMBAddresses[0].Address_Type__c);
                    lstSMBAddresses[0].Suite_Number__c=(smbCareAddr.Suite_Number__c!=''?smbCareAddr.Suite_Number__c:lstSMBAddresses[0].Suite_Number__c);
                    lstSMBAddresses[0].City__c=(smbCareAddr.City__c!=''?smbCareAddr.City__c:lstSMBAddresses[0].City__c);
                    lstSMBAddresses[0].Street_Number__c=(smbCareAddr.Street_Number__c!=''?smbCareAddr.Street_Number__c:lstSMBAddresses[0].Street_Number__c);
                    lstSMBAddresses[0].Province__c=(smbCareAddr.Province__c!=''?smbCareAddr.Province__c:lstSMBAddresses[0].Province__c);
                    lstSMBAddresses[0].Street_Name__c=(smbCareAddr.Street_Name__c!=''?smbCareAddr.Street_Name__c:lstSMBAddresses[0].Street_Name__c);
                    lstSMBAddresses[0].Postal_Code__c=(smbCareAddr.Postal_Code__c!=''?smbCareAddr.Postal_Code__c:lstSMBAddresses[0].Postal_Code__c);
                    lstSMBAddresses[0].Street_Direction__c=(smbCareAddr.Street_Direction__c!=''?smbCareAddr.Street_Direction__c:lstSMBAddresses[0].Street_Direction__c);
                    lstSMBAddresses[0].Google_Map_Link__c=(smbCareAddr.Google_Map_Link__c!=''?smbCareAddr.Google_Map_Link__c:lstSMBAddresses[0].Google_Map_Link__c);
                    lstSMBAddresses[0].Lot__c=(smbCareAddr.Lot__c!=''?smbCareAddr.Lot__c:lstSMBAddresses[0].Lot__c);
                    lstSMBAddresses[0].Block__c=(smbCareAddr.Block__c!=''?smbCareAddr.Block__c:lstSMBAddresses[0].Block__c);
                    lstSMBAddresses[0].Plan__c=(smbCareAddr.Plan__c!=''?smbCareAddr.Plan__c:lstSMBAddresses[0].Plan__c);
                    lstSMBAddresses[0].SRS_Quarter__c=(smbCareAddr.SRS_Quarter__c!=''?smbCareAddr.SRS_Quarter__c:lstSMBAddresses[0].SRS_Quarter__c);
                    lstSMBAddresses[0].Section__c=(smbCareAddr.Section__c!=''?smbCareAddr.Section__c:lstSMBAddresses[0].Section__c);
                    lstSMBAddresses[0].Township__c=(smbCareAddr.Township__c!=''?smbCareAddr.Township__c:lstSMBAddresses[0].Township__c);
                    lstSMBAddresses[0].Range__c=(smbCareAddr.Range__c!=''?smbCareAddr.Range__c:lstSMBAddresses[0].Range__c);
                    lstSMBAddresses[0].Meridian__c=(smbCareAddr.Meridian__c!=''?smbCareAddr.Meridian__c:lstSMBAddresses[0].Meridian__c);
                    lstSMBAddresses[0].Subdivision_ID__c=(smbCareAddr.Subdivision_ID__c!=''?smbCareAddr.Subdivision_ID__c:lstSMBAddresses[0].Subdivision_ID__c);
                    lstSMBAddresses[0].SRS_Type__c=(smbCareAddr.SRS_Type__c!=''?smbCareAddr.SRS_Type__c:lstSMBAddresses[0].SRS_Type__c);
                    lstSMBAddresses[0].Number__c=(smbCareAddr.Number__c!=''?smbCareAddr.Number__c:lstSMBAddresses[0].Number__c);
                    lstSMBAddresses[0].Address__c=(smbCareAddr.Address__c!=''?smbCareAddr.Address__c:lstSMBAddresses[0].Address__c);
                    lstSMBAddresses[0].state__c=(smbCareAddr.state__c!=''?smbCareAddr.state__c:lstSMBAddresses[0].state__c);
                    lstSMBAddresses[0].SRS_state__c=(smbCareAddr.SRS_state__c!=''?smbCareAddr.SRS_state__c:lstSMBAddresses[0].SRS_state__c);                    
                    lstSMBAddresses[0].GPS_Coordinates__c=(smbCareAddr.GPS_Coordinates__c!=''?smbCareAddr.GPS_Coordinates__c:lstSMBAddresses[0].GPS_Coordinates__c);
                    lstSMBAddresses[0].UTM__c=(smbCareAddr.UTM__c!=''?smbCareAddr.UTM__c:lstSMBAddresses[0].UTM__c);
                    lstSMBAddresses[0].Country__c=(smbCareAddr.Country__c!=''?smbCareAddr.Country__c:lstSMBAddresses[0].Country__c);
                    update lstSMBAddresses[0];
                    smbCareAddr=lstSMBAddresses[0];
                }else{
                    insert smbCareAddr;
                }
            }else{
                insert smbCareAddr;
            }
            srsSrvcAddr.Service_Request__c = accId.Id;   
            srsSrvcAddr.Address__c = smbCareAddr.Id;
            if(string.isnotblank(this.srsSrvcAddr.npa_nxx__c)){
                this.srsSrvcAddr.forborne__c=label.get_forborne_in_progress;
            }
            insert this.srsSrvcAddr;
            //TODO - Async Forborne WS Call
            if(string.isnotblank(this.srsSrvcAddr.npa_nxx__c)){
                ForborneWS.getForborneStatusByNpaNxxList(this.srsSrvcAddr.id);
            }
            processSACG(); // Date -> 21 Jan 2016 - Sandip PAC R2
            }
             
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, 'Service Address saved successfully.'));
            renderShowandCancel= false;
            return null;
        }catch(Exception ex){
            Database.rollback(sp);
            smbCareAddr = smbCareAddr.clone(false);
            ApexPages.addMessages(ex);            
            return null;
        }
    }

    /*
    public PageReference save(){
        try{
           bool = false;  
            
            Integer j = [Select count() FROM SMBCare_Address__c WHERE FMS_Address_ID__c=:FMSvalue LIMIT 1];           
            if(smbCareAddr.Id == NULL)
            {
                smbCareAddr.Type_of_Address__c = radioFields;                
                if(FMSvalue == NULL)
                {                    
                    insert smbCareAddr;  
                }
                else
                {                                   
                    if(j == 0)
                    {
                    smbCareAddr.FMS_Address_ID__c = FMSvalue;
                    insert smbCareAddr;                 
                    }
                }   
            }        
            if(srsSrvcAddr.Id == NULL)
            {
                srsSrvcAddr.Address__c = smbCareAddr.Id;
                srsSrvcAddr.Service_Request__c = accId.Id;          
                if(FMSvalue == NULL)
                {
                    insert this.srsSrvcAddr;
                }
                else
                {                                   
                    if(j == 0)
                    {
                        insert this.srsSrvcAddr;
                    }
                    else
                    {
                        SMBCare_Address__c  smbCareAddrRecord = [Select Id,Name FROM SMBCare_Address__c WHERE FMS_Address_ID__c=:FMSvalue];   
                           
                        srsSrvcAddr.Address__c = smbCareAddrRecord.Id;                
                        insert this.srsSrvcAddr;
                    }
                }   
            }
            else
            {
                update srsSrvcAddr;
                smbCareAddr.Type_of_Address__c = radioFields;
                update smbCareAddr;
                
            } 
            
        PageReference pr;
           if(SRID!= NULL){
                pr = new PageReference('/'+SRId);
                pr.setRedirect(true);
            }
          else if (srsSrvcAddr2.Service_Request__c!=NULL){
                pr = new PageReference('/'+srsSrvcAddr2.Service_Request__c);
                pr.setRedirect(true);
           }
           //return pr; 
           
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, 'Service Address saved successfully.'));
             renderShowandCancel= false;
             return null;
        }
        catch(exception ex){
            ApexPages.addMessages(ex);
            
            return null;
        }
    }
    */
    
    /*
    * Cancel button
    * Redirects page to service request detail record
    */
    public PageReference Cancel() {
        pageReference page;
        if(SRID!= NULL){
            page = new PageReference('/'+SRId);
            page.setRedirect(true);
        }
        else if (srsSrvcAddr2.Service_Request__c!=NULL){
            page = new PageReference('/'+srsSrvcAddr2.Service_Request__c);
            page.setRedirect(true);
        } 
        return page;
    }
    
    
    /*
    * This method is initiated to Validate New Service Address against FMS before saving record on object
    * Make ASF web service calls to FMS
    */
    
     public void CheckAddress(){
    
        smbCareAddr.Status__c = 'Invalid';
        MapPACAddress(SAData, smbCareAddr); // Map PAC componenet details to this class
        /*
        * Boolean variables whose value is dependent on the response from web service
        */      
        
        cityFlag = false;
        streetFlag = false;
        aptFlag = false;
        houseFlag = false;         
        System.debug('@@@In Check..');
        try
        {
        SRS_ASFService.ServiceAddressManagementServicePort testSRS = new SRS_ASFService.ServiceAddressManagementServicePort();  
        System.debug('@@@11..');
        // Populate Address Element 
        SRS_ASFValidationReqResponse.ServiceAddress address  = new SRS_ASFValidationReqResponse.ServiceAddress();     
        System.debug('@@@22..');
        SRS_ASFValidationReqResponse.searchServiceAddressResponse_element respSearch = new SRS_ASFValidationReqResponse.searchServiceAddressResponse_element();
        SRS_ASFValidationReqResponse.getServiceAddressDetailsResponse_element getServiceAddressResp = new SRS_ASFValidationReqResponse.getServiceAddressDetailsResponse_element(); 
        System.debug('@@@33..');
        // Setting request parameters
        address.countryCode = 'CAN';
        address.municipalityName = smbCareAddr.City__c;
        address.provinceStateCode = smbCareAddr.Province__c;
        address.streetName = smbCareAddr.Street_Name__c;
        address.streetNumber = smbCareAddr.Street_Number__c;       
        address.unitNumber = smbCareAddr.Suite_Number__c;
        address.vector = smbCareAddr.Street_Direction__c;           
        System.debug('@@@44..');
        // Send request and capture response results
        Long dtBeforeHit=datetime.now().getTime();
        respSearch = testSRS.searchServiceAddress('x139294',system.now(),address,10,'','','','');
        Long dtAfterHit=datetime.now().getTime();
        Long totalTimeTaken=dtAfterHit-dtBeforeHit;
        System.debug('Service Address WebService call-->total time taken in ms-->'+totalTimeTaken);  
        SRS_ASFValidationReqResponse.ServiceAddress respaddress  = new SRS_ASFValidationReqResponse.ServiceAddress();  
        SRS_ASFValidationReqResponse.LikeStreetList respLikeStreet = new SRS_ASFValidationReqResponse.LikeStreetList();
        SRS_ASFValidationReqResponse.likeMunicipalityList respLikeMunicipality = new SRS_ASFValidationReqResponse.likeMunicipalityList();
        SRS_ASFValidationReqResponse.likeHouseList respLikeHouse = new SRS_ASFValidationReqResponse.likeHouseList();
        SRS_ASFValidationReqResponse.likeApartmentList respLikeApartment = new SRS_ASFValidationReqResponse.likeApartmentList();
        respLikeStreet = respSearch.likeStreetList;
        respLikeMunicipality = respSearch.likeMunicipalityList;
        respLikeHouse = respSearch.likeHouseList;
        respLikeApartment = respSearch.likeApartmentList;
        respaddress = respSearch.address;        
        System.debug('@@@555..');
        // If exact match found, updates Status to Valid        
        if((respAddress!=NULL)&&(respLikeStreet==NULL)&&(respLikeMunicipality==NULL)&&(respLikeApartment==NULL)&&(respLikeHouse==NULL)){
            smbCareAddr.Status__c = 'Valid';
            FMSvalue = respaddress.addressId;
            smbCareAddr.Postal_Code__c = string.isNotblank(respaddress.postalZipCode)?respaddress.postalZipCode:smbCareAddr.Postal_Code__c;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, 'Valid Service Address'));
        }
        // If partial match found, updates Status to Invalid    
        else if((respAddress!=NULL)||(respLikeStreet!=NULL)||(respLikeMunicipality!=NULL)||(respLikeApartment!=NULL)||(respLikeHouse!=NULL)){
            smbCareAddr.Status__c = 'Invalid';
        }
        // If No match found, updates Status to Invalid
        else if((respAddress==NULL)&&(respLikeStreet==NULL)&&(respLikeMunicipality==NULL)&&(respLikeApartment==NULL)&&(respLikeHouse==NULL)){
            smbCareAddr.Status__c = 'Invalid';
            noMatchFlag = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, 'Invalid Service Address: No Matches Found'));
        }   
        
        Integer i = 0;
        if(respLikeStreet!= NULL){  
            streetFlag = true;
            lstw.clear();
            for(SRS_ASFValidationReqResponse.likeStreetItem a : respLikeStreet.likeStreetItem)
            {            
            lstw.add(new wrapper(a.street,a.vector,a.firstHouseNumber,a.lastHousenumber,a.treatmentCode,i));            
            i++;            
            }            
        } 
        else if(respLikeMunicipality != NULL){ 
            cityFlag = true;
            CityorHouse = true;
            lstw.clear();
            for(SRS_ASFValidationReqResponse.likeMunicipalityItem a :respLikeMunicipality.likeMunicipalityItem)
            {
            lstw.add(new wrapper(a.municipalityName,a.provinceStateCode,CityorHouse,i));
            
            i++;
            }             
        }   
        else if(respLikeApartment != NULL){ 
            aptFlag = true;
            lstw.clear();
            for(SRS_ASFValidationReqResponse.likeApartmentItem a :respLikeApartment.likeApartmentItem)
            {
            lstw.add(new wrapper(a.unitNumber,i));
            i++;
            }   
        } 
        if(respLikeHouse!= NULL){  
            houseFlag = true;
            CityorHouse = false;
            lstw.clear();
            for(SRS_ASFValidationReqResponse.likeHouseItem a : respLikeHouse.likeHouseItem)
            {   
            lstw.add(new wrapper(a.houseNumber,a.houseSuffix,CityorHouse,i));
            i++;
            }            
        } 
        } //try
        catch(Exception e){  
            System.debug('@@@Exception.. ' + e);          
            ApexPages.addMessages(e); 
            SMB_WebserviceHelper.logWebserviceError('FMS','Validate Service Address','Address', '' ,null, e.getMessage(),'SRS_ASFService.ServiceAddressManagementServicePort.searchServiceAddress',smbCareAddr.id);
        }   
    } //checkAddress method   
        
    
    /*
    * Wrapper Class Construction    
    * This class wraps each result from web service response as a generic object and returns them to the VF page as a List.
    */ 
    
    @TestVisible public class wrapper{
        public String Mcity{get;set;}
        public String Mprovince{get;set;}
        public String Sstreet{get;set;}
        public String Svector{get;set;}
        public String Sfirsthousenumber{get;set;}
        public String Slasthousenumber{get;set;}
        public String Streatmentcode{get;set;}
        public String Aunitnumber{get;set;}
        public String Hhousenumber{get;set;}
        public String Hhousesuffix{get;set;}
        public Boolean selected{get; set;} 
        public Integer index{get; set;} 
                        
        // Wrapper class constructor for like cities or like houses
        public wrapper(String param1,String param2, Boolean CityorHouse, Integer i){ 
            if(CityOrHouse)
            {
            this.Mcity = param1;
            this.Mprovince = param2;
            this.index = i;
            selected = false;
            }
            else
            {
            this.Hhousenumber = param1;
            this.Hhousesuffix = param2;
            this.index = i;
            selected = false;
            }
        }
        
        // Wrapper class constructor for like streets
        public wrapper(String Sstreet,String Svector,String Sfirsthousenumber,String Slasthousenumber, String Streatmentcode, Integer i){            
            this.Sstreet = Sstreet;
            this.Svector = Svector;
            this.Sfirsthousenumber = Sfirsthousenumber;
            this.Slasthousenumber = Slasthousenumber;
            this.Streatmentcode = Streatmentcode;
            this.index = i;
            selected = false;
        }
        
        // Wrapper class constructor for like apartments
        public wrapper(String Aunitnumber, Integer i){           
            this.Aunitnumber = Aunitnumber;
            this.index = i;
            selected = false;
        }       
    }
    
    
    public String getRadioFields() {
        return radioFields;
    }             
    public void setRadioFields(String radioFields) { 
    this.radioFields = radioFields; 
    }
    
    /*
    * Below 2 methods are for Bypass Address validation section
    * When no match found in FMS, User has option to Bypass validation and use address as entered and/or Create SACG request.
    * Unset radioFields value to NULL, allow user to select any one radio field (but not mandatory)
    */
    
    public void chckbx_fn(){
       setRadioFields(NULL);
       getRadioFields();
       if(smbCareAddr.Send_to_SACG__c == true){
            bool = true;
           // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Postal Code is required when Bypass & SACG or Bypass only selected'));
        }
        if ((smbCareAddr.Use_Addresses_Entered__c == false) && (smbCareAddr.Send_to_SACG__c == false)){
        bool = false;
        }
        else if ((smbCareAddr.Use_Addresses_Entered__c == false) && (smbCareAddr.Send_to_SACG__c == true)){
        bool = true;
        }
    }
    
    public void chckbx_checked(){ 

        /*
        * If user selects â€˜Use Address as enteredâ€™ , 
        * Send to SACG is checked by default (user can un-check)
        */
        
        if(smbCareAddr.Use_Addresses_Entered__c == true){
            smbCareAddr.Send_to_SACG__c = true;
            bool = true;          
        }
        if ((smbCareAddr.Use_Addresses_Entered__c == false) && (smbCareAddr.Send_to_SACG__c == false)){
        bool = false;
        }
        else if ((smbCareAddr.Use_Addresses_Entered__c == true) && (smbCareAddr.Send_to_SACG__c == false)){
        bool = true;
        }        
    } 
     
    /*
    * handle the action of the commandLink of type Select
    * Auto populate address fields based on selecting a record from the wrapper list of suggestions (or results)
    */
    
    public void processLinkClick() {        
        
        String url = ApexPages.currentPage().getUrl(); 
        id =  ApexPages.CurrentPage().getParameters().get('paramid');
        index = ApexPages.CurrentPage().getParameters().get('Index');        
        
        Integer i = Integer.ValueOf(index);
        
        if(lstw.isEmpty())
        {
            system.debug('processLinkClick():List<wrapper>:lstw is empty()');
            return;
        }
        
        if(id == 'City'){
        smbCareAddr.City__c = lstw.get(i).Mcity;
        smbCareAddr.Province__c = lstw.get(i).Mprovince;
        CheckAddress();
        }
        else if(id == 'Street Name'){
        smbCareAddr.Street_Name__c = lstw.get(i).Sstreet;
            if(lstw.get(i).Sfirsthousenumber != lstw.get(i).Slasthousenumber)
            {
            smbCareAddr.Street_Number__c = lstw.get(i).Sfirsthousenumber+'-'+lstw.get(i).Slasthousenumber;
            smbCareAddr.Street_Direction__c = lstw.get(i).Svector;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please enter valid Building # within this range '+'"'+ smbCareAddr.Street_Number__c+'"'+' and click "Check Address" button to re-validate the address against FMS'));
            }
            else
            {
            smbCareAddr.Street_Number__c = lstw.get(i).Sfirsthousenumber;    
            smbCareAddr.Street_Direction__c = lstw.get(i).Svector;      
            CheckAddress();
            }   
        }
        else if(id == 'Unit Number'){   
        system.debug('@@@: '+lstw.get(i).Aunitnumber);
        smbCareAddr.Suite_Number__c = lstw.get(i).Aunitnumber; 
        CheckAddress(); 
        }
        else if(id == 'House Number'){  
        smbCareAddr.Street_Number__c = lstw.get(i).Hhousenumber;
        CheckAddress(); 
        } 
     /*   else if(id == 'Vector'){  
        smbCareAddr.Street_Direction__c = lstw.get(i).Svector;
        system.debug('>>>> :'+lstw.get(i).Svector);
        CheckAddress(); 
        }  */
    }
} // end of SRS_NewServiceAddressController