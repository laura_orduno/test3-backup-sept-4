@IsTest
public with sharing class PortalUtilsTest {
	private static final String COMMENT_BODY = 'This is the comment body';
	private static final String LONGTEXT = 'This is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment bodyThis is the comment body';
    private static User myUser;
    private static Case myCase;
    private static Account myAcct;
    private static Account portalAcct;
    private static RestApi_MyAccountHelper_CreateComment createCommentObj;
    private static RestApi_MyAccountHelper_CreateComment.RequestUser ru;
    private static RestApi_MyAccountHelper_CreateComment.RequestComment rc;
    private static RestApi_MyAccountHelper_CreateComment.CreateCommentResponse resp;
	

    private static void setup() {
    	portalAcct = new Account(Name = 'Portal Account Inc.');
    	insert portalAcct;

    	myAcct = new Account(Name = 'Account Inc.', BCAN__c = RestApi_MyAccountHelper_TestUtils.BILLINGACCOUNTNUMBER);
    	insert myAcct;

    	myUser = RestApi_MyAccountHelper_TestUtils.createPortalUser(portalAcct);
    	insert myUser;

    	myCase = RestApi_MyAccountHelper_TestUtils.createCase(myAcct, myUser);
    	insert myCase;

    }


    @isTest
    private static void testComment() {
    	setup();
    	CaseComment c = new CaseComment(ParentId = myCase.Id, CommentBody = COMMENT_BODY, IsPublished = true);
        insert c;

        CaseComment cc = [SELECT Id FROM CaseComment WHERE Id =: c.Id];

        System.assert(cc != null);
    }

    @isTest
    private static void testLongComment() {
    	setup();
    	CaseComment c = new CaseComment(ParentId = myCase.Id, CommentBody = LONGTEXT, IsPublished = true);
        insert c;

        CaseComment cc = [SELECT Id FROM CaseComment WHERE Id =: c.Id];

        System.assert(cc != null);
    }

    @isTest
    private static void createNewComment(){
    	setup();

    	Test.startTest();
    	CaseComment cc = PortalUtils.createNewComment(MBRTestUtils.createEmail(myCase.Id, true, 'Test Body'), myCase);
    	Test.stopTest();

		System.assert(cc != null);    	

    }

	@isTest
	private static void testCookies(){
		setup();

		// Since we have no cookies yet this should default to english
		String language = PortalUtils.getUserLanguage();
		System.assertEquals(PortalUtils.COOKIE_LANGUAGE_ENG, language);

		// Change to french
		PortalUtils.setUserLanguage();
		language = PortalUtils.getUserLanguage();
		System.assertEquals(PortalUtils.COOKIE_LANGAUGE_FR, language);

		// Change back to english
		PortalUtils.setUserLanguage();
		language = PortalUtils.getUserLanguage();
		System.assertEquals(PortalUtils.COOKIE_LANGUAGE_ENG, language);
	}

	@isTest
	private static void testEmailValidation(){

	}
}