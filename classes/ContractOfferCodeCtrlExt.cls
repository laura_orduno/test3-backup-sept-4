public class ContractOfferCodeCtrlExt {

    public boolean IsPropargate {get;set;}
    public boolean showPropergate {get;set;}
    public List<ItemSelect> displayItems {get;set;}
    public  String linkField {get;set;}
    private String returnURL;
    
    public String mode {get;set;}
    public String modeLabel {get;set;}
    protected  ApexPages.StandardController controller;
    public Sobject currentObj {get;set;} 
    
    
    private Static final String MODE_EDIT = 'edit';
    private Static final String MODE_DETAIL = 'maindetail';
    
    public boolean isNew {get;set;}
    
    
    public void edit(){
        mode = MODE_EDIT;
        modeLabel = Label.Edit;
    }
    
    public PageReference cancel(){
        mode = MODE_DETAIL;
        return new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
    }
    
    public PageReference save(){
        id dsId = (id) currentObj.get('Deal_Support__c');
        List<Offer_house_Demand__c> ds = [select Contract__c  from Offer_house_Demand__c where id = :dsId];
        if(!ds.isEmpty()){
            Id agrid = ds.get(0).Contract__c;
            controller.getRecord().put('Contract__c',agrid);
        }     
        controller.save();
        if(isNew){
             saveNew();
        }
        if(ApexPages.hasMessages()){
            return null;
        }
        mode = MODE_DETAIL;
        modeLabel = Label.Detail;
        return new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
    }

    public void saveNew(){
   
     String query = Util.queryBuilder('Subsidiary_Account__c',new List<String>{'id','Account_Name__c','Account__c','Subsidiary_Account__c.Name'},
                                             new List<string>{ linkField +'=\''+currentObj.get(linkField)+'\''});
            List<Sobject> itemList = Database.query(query);
            System.debug('query ' + query);
            List<Agreement_Offer_Code__c> itemsToSave = new LIst<Agreement_Offer_Code__c>();
            System.debug(LoggingLevel.ERROR,'itemList' + itemList);
            for(Sobject item : itemList) {
                Agreement_Offer_Code__c ofc = new Agreement_Offer_Code__c();
                List<String> flds =  Util.getUpdatableFields('Agreement_Offer_Code__c');
                for(String fld : flds){
                    ofc.put(fld,this.currentObj.get(fld));
                }
                ofc.put('Subsidiary_Account__c',item.id);
                ofc.put('parent__c',controller.getRecord().id);
                ofc.put('Contract__c',null);
                ofc.put('Expired__c',false);
                ofc.put(linkField,null);
                System.debug(LoggingLevel.INFO,ofc);
                itemsToSave.add(ofc);
            }
            System.debug(LoggingLevel.INFO,itemsToSave);
            insert itemsToSave;
    }
    
    public void setToEditMode(){
        mode = MODE_EDIT;
        modeLabel = Label.Edit; 
    }
    
    public void setToDetailMode(){
        
        mode = MODE_DETAIL;
        modeLabel = Label.Detail;
    }

    
    public ContractOfferCodeCtrlExt(ApexPages.StandardController controller){
        returnURL = ApexPages.currentPage().getParameters().get('retURL');
        this.controller = controller;
        this.currentObj = controller.getRecord();
        if(currentObj.id ==null){
            isNew = true;
        }else{
            isNew = false;
        }
        System.debug(LoggingLevel.INFO,isNew);
        if(this.currentObj.get('Contract__c') != null){
           linkField = 'Contract__c'; 
           showPropergate = true;  
        }else if(this.currentObj.get('Subsidiary_Account__c') != null){
           linkField = 'Subsidiary_Account__c'; 
           showPropergate = false; 
        }else if(this.currentObj.get('Deal_Support__c') != null){
            linkField = 'Deal_Support__c'; 
        }
        System.debug(LoggingLevel.INFO,'linkField ' + linkField);
        if(!returnURL.contains('apex')){
            String objIdfromURL = returnURL.replaceAll('/','');
            Id returnObj = (ID) objIdfromURL;
                if('Offer_House_Demand__c' == returnObj.getSobjectType().getDescribe().getName()){
                    linkField = 'Deal_Support__c'; 
                }
        }    
        System.debug(LoggingLevel.INFO,linkField);
        if(linkField == null){
            String errmsg= 'Unexpeted Error Occord: Link Filed = ' +linkField;
            System.debug(LoggingLevel.INFO,ApexPages.currentPage().getParameters());
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL,errmsg);
            ApexPages.addMessage(msg);
            
        }else{                 
            setToEditMode();
            this.displayItems = new List<ItemSelect>();
            initPropagateToItemList();
        }
    }
    
    public void showPropergateList(){
        controller.save();
        if(ApexPages.hasMessages()){
            return;
        }
        setToDetailMode();
        IsPropargate = true; 
    }
    
    public PageReference propagate(){
        Sobject beforeSaveObj = controller.getRecord();
        System.debug(LoggingLevel.INFO,beforeSaveObj);
        controller.save();
        List<Sobject> toInsert = new LIst<Sobject>();
        System.debug(LoggingLevel.INFO,this.displayItems);
        List<Id> selectedIds = new List<Id>();
        for(ItemSelect item : this.displayItems){
            if(item.selected == true){
                selectedIds.add(item.obj.id);
            }
        }
        
        System.debug(LoggingLevel.ERROR,'selectedIds : ' + selectedIds);
        List<Agreement_Offer_Code__c> filtersLIst = [Select id, Deal_Support__c, Product_Type__c, Type__c, Start_Date__c,End_Date__c,Description__c 
                                                     from Agreement_Offer_Code__c where Subsidiary_Account__c  in :selectedIds and parent__c =:beforeSaveObj.id ];
        Sobject obj = controller.getRecord();
        System.debug(LoggingLevel.INFO,'filtersLIst : ' + filtersLIst);
        for(Agreement_Offer_Code__c offercode : filtersLIst){
            offercode.put('Description__c',obj.get('Description__c')); 
        }
        
        if(selectedIds.isEmpty()){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,'Select at least one Item');
            ApexPages.addMessage(msg);
            return null;
        }
        try{
            System.debug(LoggingLevel.INFO,filtersLIst);
            update filtersLIst;
        }catch(Exception e ){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL,e.getMessage());
            ApexPages.addMessage(msg);      
            return null;
        }    
        return new PageReference(returnURL);
    }
    
    private void initPropagateToItemList(){
        string lkfield = linkField;
        if('Subsidiary_Account__c'.equals(linkField)){
            lkfield = 'Contract__c';
        }
        String query = Util.queryBuilder('Subsidiary_Account__c',new List<String>{'id','Account_Name__c','Account__c','Subsidiary_Account__c.Name' },
                                         new List<string>{ lkfield +'=\''+currentObj.get(linkField)+'\''});
        List<Sobject> itemList = Database.query(query);
        for(Sobject item : itemList) {
              displayItems.add(new ItemSelect(item,false));
         }
    }
    
    public class ItemSelect{
        public sObject obj {get; set;}
        public boolean selected {get; set;}
        public ItemSelect(sObject obj,boolean sel) {
            this.obj = obj;
            selected = sel;
        }
    }
}