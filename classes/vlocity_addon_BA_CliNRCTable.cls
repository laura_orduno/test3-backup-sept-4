global with sharing class vlocity_addon_BA_CliNRCTable implements vlocity_cmt.VlocityOpenInterface {

  public Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {
        Boolean success = true;
        if(methodName == 'buildDocumentSectionContent')
        { 
             Id contractId = (Id) inputMap.get('contextObjId');
            map<string,object> mapTableInfo= vlocity_addon_DynamicTableHelper_BA.getCliNRCData(contractId);
                       
            if(mapTableInfo.size() > 0 && !mapTableInfo.isEmpty()){
                inputMap.putall(mapTableInfo);
                map<string,object> outputMap =  vlocity_addon_DynamicTableHelper_BA.buildDocumentSectionContent(inputMap,outMap, options);
                outMap.putAll(outputMap);
            }
            else {
                return false;
            }
                
         
             }       
          return success; 
  }

}