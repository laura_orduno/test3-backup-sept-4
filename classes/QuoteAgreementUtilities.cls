/*
    Quote Agreement Utilities
    QuoteAgreementUtilities.cls
    Part of the TELUS QuoteQuickly Portal
    
    Author: Alex Miller @ Traction <amiller@tractionondemand.com>
    Created: May 9, 2012
    
    Since: Release 5.1
    
    Known Issues:
     -- None Yet --
*/
public without sharing class QuoteAgreementUtilities {
    public static Boolean expireInRecombo {get; set;}

    static String baseAgreementQuery = 'Select Name, Quote__c, Quote__r.SBQQ__Status__c, Quote_Group__c, Attachment_ID__c,'+
                'Quote_Group__r.Status__c, Type__c, Sent_Date__c, Expiry_Date__c, Closed_Date__c, Channel_Phone_Number__c,'+
                'URL__c, Waybill_ID__c, Status__c, Signed_Date__c, Recipient__c';

    public static Agreement__c[] loadAgreementsByWaybill(String id) {
        if (id == null || id == '') { return null; }
        return loadAgreementsByWaybill(new List<String>{id});
    }
    public static Agreement__c[] loadAgreementsByWaybill(String[] ids) {
        if (ids == null || ids.size() == 0) {
            // Return error that the agreement id list is empty.
            system.debug('QuoteAgreementUtilities::loadAgreementsByWaybill: no ids provided.');
            return null;
        }
        // Load the agreement records
        return [Select Name, Quote__c, Quote__r.SBQQ__Status__c, Quote_Group__c, Attachment_ID__c,
                Quote_Group__r.Status__c, Type__c, Sent_Date__c, Expiry_Date__c, Closed_Date__c, Channel_Phone_Number__c,
                URL__c, Waybill_ID__c, Status__c, Signed_Date__c, Recipient__c from Agreement__c where Waybill_ID__c IN :ids];
    }
    // Method to load agreement
    public static Agreement__c loadAgreement(String id) {
        Agreement__c[] al = loadAgreements(new List<String>{id});
        if (al == null || al.size() == 0) { return null; }
        return al.get(0);
    }
    public static Agreement__c[] loadAgreements(String[] ids) {
        if (ids == null || ids.size() == 0) {
            // Return error that the agreement id list is empty.
            system.debug('QuoteAgreementUtilities::loadAgreements: no ids provided.');
            return null;
        }
        // Load the agreement records
        return [Select Name, Quote__c, Quote__r.SBQQ__Status__c, Quote_Group__c, Attachment_ID__c,
                Quote_Group__r.Status__c, Type__c, Sent_Date__c, Expiry_Date__c, Closed_Date__c, Channel_Phone_Number__c,
                URL__c, Waybill_ID__c, Status__c, Signed_Date__c, Recipient__c  from Agreement__c where Id IN :ids];
    }
    
    public static Agreement__c[] loadAgreementsByQuote(Id id) {
        return loadAgreementsByQuote(id, null);
    }
    public static Agreement__c[] loadAgreementsByQuote(Id id, String replacementAgreementId) {
        if (id == null) {
            // Return error that the quote Id is empty.
            system.debug('QuoteAgreementUtilities::loadAgreementByQuote: no id provided. Call failed.');
            return null;
        }
        // Load the agreement records
        if (replacementAgreementId != null) {
            return [Select Name, Quote__c, Quote__r.SBQQ__Status__c, Quote_Group__c, Attachment_ID__c,
                    Quote_Group__r.Status__c, Type__c, Sent_Date__c, Expiry_Date__c, Closed_Date__c, Channel_Phone_Number__c,
                    URL__c, Waybill_ID__c, Status__c, Signed_Date__c, Recipient__c from Agreement__c where Quote__c = :id  and Id != :replacementAgreementId];
        }
        return [Select Name, Quote__c, Quote__r.SBQQ__Status__c, Quote_Group__c, Attachment_ID__c,
                Quote_Group__r.Status__c, Type__c, Sent_Date__c, Expiry_Date__c, Closed_Date__c, Channel_Phone_Number__c,
                URL__c, Waybill_ID__c, Status__c, Signed_Date__c, Recipient__c from Agreement__c where Quote__c = :id];
    }
    
    public static Agreement__c[] loadAgreementsByQuoteGroup(Id id) {
        return loadAgreementsByQuoteGroup(id, null);
    }
    public static Agreement__c[] loadAgreementsByQuoteGroup(Id id, String replacementAgreementId) {
        if (id == null) {
            // Return error that the quote Id is empty.
            system.debug('QuoteAgreementUtilities::loadAgreementByQuoteGroup: no id provided. Call failed.');
            return null;
        }
        // Load the agreement records
        if (replacementAgreementId != null) {
            return [Select Name, Quote__c, Quote__r.SBQQ__Status__c, Quote_Group__c, Attachment_ID__c,
                Quote_Group__r.Status__c, Type__c, Sent_Date__c, Expiry_Date__c, Closed_Date__c, Channel_Phone_Number__c,
                URL__c, Waybill_ID__c, Status__c, Signed_Date__c, Recipient__c from Agreement__c where Quote_Group__c = :id  and Id != :replacementAgreementId];
        }
        return [Select Name, Quote__c, Quote__r.SBQQ__Status__c, Quote_Group__c, Attachment_ID__c,
                Quote_Group__r.Status__c, Type__c, Sent_Date__c, Expiry_Date__c, Closed_Date__c, Channel_Phone_Number__c,
                URL__c, Waybill_ID__c, Status__c, Signed_Date__c, Recipient__c from Agreement__c where Quote_Group__c = :id];
    }
    
    public static void processAcceptance(String agreementId) {
        processAcceptance(agreementId, false);
    }
    public static void processAcceptance(String agreementId, Boolean useFuture) {
        String aprefix = Agreement__c.SObjectType.getDescribe().getKeyPrefix();
        if (agreementId == null || agreementId.length() < 18 || agreementId.substring(0, 3) != aprefix) { 
        	throw new QuoteAgreementUtilitiesException('Invalid Agreement Id');
        }
        
        Agreement__c agreement = loadAgreement(agreementId);
        if (agreement == null) { throw new QuoteAgreementUtilitiesException('Unable to load agreement'); }
        
        agreement.Status__c = QuoteAgreementStatus.SIGNED;
        agreement.Signed_Date__c = Datetime.now();
        
        SBQQ__Quote__c[] quotes = null;
        Quote_Group__c qg = null;
        Id[] aids = new List<Id>();
        if (agreement.Quote__c != null) {
            quotes = QuoteUtilities.loadQuotes(new List<String>{agreement.Quote__c});
            for (Agreement__c a : loadAgreementsByQuote(agreement.Quote__c, agreement.id)) {
                aids.add(a.id);
            }
        }
        if (agreement.Quote_Group__c != null) {
            qg = [Select Id, Name, Status__c From Quote_Group__c Where Id = :agreement.Quote_Group__c];
            for (Agreement__c a : loadAgreementsByQuoteGroup(qg.Id, agreement.id)) {
                aids.add(a.id);
            }
            quotes = QuoteUtilities.loadQuotesByGroup(agreement.Quote_Group__c);
        }
        if (useFuture) {
            expireAgreementsLater(aids);
        } else {
            expireAgreements(aids);
        }
        if (quotes == null) { throw new QuoteAgreementUtilitiesException('Unable to load quote records for agreement ' + agreement.Name); }
        
        for (SBQQ__Quote__c quote : quotes) {
            quote.SBQQ__Status__c = QuoteStatus.ACCEPTED_AWAITING_PROVISIONING;
            quote.Agreement_Accepted__c = true;
        }
        
        QuoteUtilities.sendProvisioningEmail(quotes, qg);
        
        update quotes;
        if (qg != null) {
            update qg;
        }
        update agreement;
    }
    
    @future
    public static void expireOldQuoteAgreementsLater(Id quoteId, string reason) {
        try {
        expireOldQuoteAgreements(quoteId, reason, null);
        } catch (Exception e) { QuoteUtilities.logNow(e); }
    }
    public static boolean expireOldQuoteAgreements(Id quoteId, string reason) {
        return expireOldQuoteAgreements(quoteId, reason, null);
    }
    public static boolean expireOldQuoteAgreements(Id quoteId, string reason, String replacementWaybillId) {
        if (quoteId == null) { throw new QuoteAgreementUtilitiesException('Missing quote id.'); }
        Agreement__c[] agreements = loadAgreementsByQuote(quoteId);
        if (agreements == null || agreements.size() == 0) {
            System.debug('No old agreements found');
            return true;
        }
        Id[] agreementIds = new List<Id>();
        for (Agreement__c a : agreements) { agreementIds.add(a.Id); }
        return expireAgreements(agreementIds, reason, replacementWaybillId);
    }
    
    @future
    public static void expireOldQuoteGroupAgreementsLater(Id quoteId, string reason) {
        try {
        expireOldQuoteAgreements(quoteId, reason, null);
        } catch (Exception e) { QuoteUtilities.logNow(e); }
    }
    
    public static boolean expireOldQuoteGroupAgreements(Id quoteId, string reason) {
        return expireOldQuoteAgreements(quoteId, reason, null);
    }
    public static boolean expireOldQuoteGroupAgreements(Id quotegroupId, string reason, String replacementWaybillId) {//Id replacementAgreementId) {
        if (quotegroupId == null) { throw new QuoteAgreementUtilitiesException('Missing quote group id.'); }
        Id[] agreementIds = new List<Id>();
        for (Agreement__c a : loadAgreementsByQuoteGroup(quotegroupId)) {
            agreementIds.add(a.Id);
        }
        return expireAgreements(agreementIds, reason, replacementWaybillId);
    }
    @future
    public static void expireAgreementsLater(List<Id> agreementIds) {
        try {
        expireAgreements(agreementIds, null, null);
        } catch (Exception e) { QuoteUtilities.logNow(e); }
    }
    public static boolean expireAgreements(List<Id> agreementIds) {
        return expireAgreements(agreementIds, null, null);
    }
    public static boolean expireAgreements(List<Id> agreementIds, String reason) {
        return expireAgreements(agreementIds, reason, null);
    }
    public static boolean expireAgreements(List<Id> agreementIds, String reason, String replacementWaybillId) {
        if (agreementIds == null || agreementIds.size() == 0) {
            return true;
        }
        Agreement__c[] agreements = loadAgreements(agreementIds);
        SBQQ__Quote__c[] quotes = new List<SBQQ__Quote__c>();
        Quote_Group__c[] quotegroups = new List<Quote_Group__c>();
        if (agreements == null || agreements.size() == 0) {
            return false;
        }
        Quote_Portal__c qp = Quote_Portal__c.getInstance();
        
        Boolean setQuoteToExpired = false;
        String endStatus = null;
        String reasonMessage = 'This TELUS Business Anywhere agreement has been cancelled. Please contact your sales representative for more information.';
        if (reason == null) {
            endStatus = QuoteAgreementStatus.CANCELLED;
        } else if (reason.equalsIgnoreCase(QuoteAgreementStatus.EXPIRED)) {
            endStatus = QuoteAgreementStatus.EXPIRED;
            setQuoteToExpired = true;
            reasonMessage = 'This TELUS Business Anywhere agreement has expired. Please contact your sales representative for more information.';
        } else if (reason.equalsIgnoreCase(QuoteAgreementStatus.CANCELLED)) {
            endStatus = QuoteAgreementStatus.CANCELLED;
        } else if (reason.equalsIgnoreCase(QuoteAgreementStatus.REPLACED)) {
            endStatus = QuoteAgreementStatus.REPLACED;
            reasonMessage = 'This TELUS Business Anywhere agreement has been modified. A replacement agreement has been created. Please contact your sales representative for more information.';
        } else {
            endStatus = QuoteAgreementStatus.CANCELLED;
        }
        
        Boolean needsRecombo = false;
        for (Agreement__c a : agreements) {
            if (a.Type__c != null && a.Type__c.equalsIgnoreCase('recombo')) {
                needsRecombo = true;
            }
        }
        RecomboClient client = null;
        
        if (needsRecombo) {
            client = new RecomboClient(qp.Recombo_Endpoint__c, qp.Recombo_Username__c, qp.Recombo_Password__c);
        }
        
        for (Agreement__c agreement : agreements) {
            
            if (agreement.Type__c.equalsIgnoreCase('recombo') && qp.Expire_Agreements_in_Recombo__c != null && qp.Expire_Agreements_in_Recombo__c == true) {
                
                if (expireInRecombo != null && expireInRecombo == true) { continue;
                    // This is a hard override that will prevent callouts when an agreement acceptance is being processed.
                }
                
                if (!agreement.Status__c.equalsIgnoreCase('pending') && !agreement.Status__c.equalsIgnoreCase('signed')) { continue;
                    // You can't resend the deletion message to a document.
                }
                if (agreement.Waybill_ID__c == null || agreement.Waybill_ID__c == '') {
                    throw new QuoteAgreementUtilitiesException('Agreement number ' + agreement.Name + ' is type ' + agreement.Type__c + ' but the Waybill ID is empty.');
                }
                Integer response = null;
                if (replacementWaybillId != null) {
                    response = client.deleteDocument(agreement.Waybill_ID__c, reasonMessage, replacementWaybillId);
                } else {
                    response = client.deleteDocument(agreement.Waybill_ID__c, reasonMessage);
                }
                if (response == null || response < 1) { throw new QuoteAgreementUtilitiesException('Agreement number ' + agreement.Name + ' Recombo expiration failed.'); }
            }
            if (agreement.Status__c.equalsIgnoreCase('pending') || agreement.Status__c.equalsIgnoreCase('signed')) {
                agreement.Status__c = endStatus;
                agreement.Closed_Date__c = DateTime.now();
            }
            if (setQuoteToExpired) {
                if (agreement.Quote__c != null) {
                    agreement.Quote__r.SBQQ__Status__c = QuoteStatus.AGREEMENT_EXPIRED;
                    quotes.add(agreement.Quote__r);
                }
                if (agreement.Quote_Group__c != null) {
                    agreement.Quote_Group__r.Status__c = QuoteStatus.AGREEMENT_EXPIRED;
                    quotegroups.add(agreement.Quote_Group__r);
                }
            }
        }
        update agreements;
        update quotes;
        update quotegroups;
        return true;
    }
    public class QuoteAgreementUtilitiesException extends Exception {}
    
}