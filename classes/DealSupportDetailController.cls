/******************************************************************************
# File..................: ContractDetailController
# Version...............: 1
# Created by............: Sandip Chaudhari (IBM)
# Description...........: Class is to Create, Validate, Save Billing Address for Service Request. 
                          This class is main controller for detail page. Created as a part of PAC R2
******************************************************************************/

Public class DealSupportDetailController{
    public AddressData PAData  { get; set; }
    offer_house_demand__c saObj;
    public String isError{ get; set; }
    
    public DealSupportDetailController(ApexPages.StandardController controller){
        isError = 'onLoad';
        PAData = new AddressData();
        PAData.isAddressLine1Required = true;
        PAData.isProvinceRequired = true;
        PAData.isCityRequired = true;
        saObj = (offer_house_demand__c)controller.getRecord();
        saObj = [SELECT Id,billing_street__c,billing_city__c,billing_province_state__c,billing_countrY__c,billing_postal_code__c,Billing_Address_Status__c
                        FROM offer_house_demand__c
                        WHERE Id=: saObj.Id];
        if(saObj != null){
            PAData.searchString = saObj.billing_street__c+''+saObj.billing_city__c+''+saObj.billing_province_state__c+''+saObj.billing_countrY__c+''+saObj.billing_postal_code__c;
            if(saObj.Billing_Address_Status__c == System.Label.PACInValidCaptureNewAddrStatus){
                PAData.isManualCapture = true;
            }else{
                PAData.isManualCapture = false;
            }
            
            //Split the address devided by comma and assigned to respective vaiables for AddressData class
            if(saObj.billing_street__c != null){
                integer i=0;
                string[] addrPart = saObj.billing_street__c.split(',');
                if(addrPart != null && addrPart.size() > 0){
                    if(addrPart.size() > i){
                        PAData.addressLine1 = addrPart[i];
                       // PAData.onLoadSearchAddress = addrPart[i];
                        i++;
                    }
                    if(addrPart.size() > i){
                        if(addrPart[i] != null){
                            PAData.city  = addrPart[i].trim();
                            i++;
                        }
                    }
                    if(addrPart.size() > i){
                        if(addrPart[i] != null){
                            PAData.canadaProvince = addrPart[i].trim();
                            PAData.usState = addrPart[i].trim();
                            PAData.province = addrPart[i].trim();
                        }
                        i++;
                    }
                    if(addrPart.size() > i){
                        if(addrPart[i] != null){
                            PAData.country = addrPart[i].trim();
                        }
                        i++;
                    }
                    
                    if(addrPart.size() > i){
                        if(addrPart[i] != null){
                            PAData.postalCode = addrPart[i].trim();
                        }
                    }
                }
            }
        }else{
            PAData.country='Canada';
        }
    }
    
     /**
    * Name - mapAddress
    * Description - Update Billing address in existing Billing_Address__c field
    * Param - 
    * Return type - void
    **/
    
    public pageReference updateBillingAddress (){
        mapAddress();
        try{
            isError = 'inSave';
            update saObj;
        }catch(Exception ex){
            isError = 'InError';
            ApexPages.addMessages(ex) ;
        }
        //PageReference redirectPage = new PageReference('/apex/ServiceRequestDetail?id='+ saObj.Id +'&sfdc.override=1');
        //redirectPage.setRedirect(true);
        //return redirectPage;
        return null;
    }
    
    /**
    * Name - mapAddress
    * Description - Map Billing address with existing Billing_Address__c field
    * Param - 
    * Return type - void
    **/
    public void mapAddress(){
        system.debug('#### City' + PAData.city);
        system.debug('#### postalCode' + PAData.postalCode);
        //String address = '';
        if(PAData.addressLine1 != null)
            saObj.billing_street__c  = PAData.addressLine1.trim();
        saObj.billing_city__c=PAData.city;
        saObj.billing_province_state__c=PAData.province;
        saObj.billing_country__c=PAData.country;
        saObj.billing_postal_code__c=PAData.postalCode;
        //saObj.billingaddress = address;
        saObj.Billing_Address_Status__c = PAData.captureNewAddrStatus;
    }
}