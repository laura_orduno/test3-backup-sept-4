global with sharing class OCOM_DocuSignUtilClass {
  private static final string Agmt_StatusCategory_Request='Request';
    private static final string Agmt_StatusCategory_In_Signatures='In Signatures';
    private static final string Agmt_Status_Ready_for_Signatures='Ready for Signatures';
    private static final string Agmt_Status_Fully_Signed='Fully Signed';
    private static final string Agmt_Status_Other_Party_Signatures='Other Party Signatures';
    private static final string Custom_setting_name='Apttus Comply Custom API Settings';
    private static final string Document_Access_Level='Read Only';
    private static final string Document_Format='pdf';
    private static final string Document_Format_vlocity='docx';
  
     global static String doCancel_vlocity(contract contractObj) {
        //Send the cancel request to Docusign
        
        if(contractObj == null) {
            return Label.APTS021_INVALID_AGREEMENT_ID;
        }
        
        //validate contract status
       
         if(contractObj.Status == null) {
            return Label.APTS022_INVALID_AGREEMENT_STATUS;
        } else if(contractObj.Status !='Contract Sent') {
            return Label.APTS022_INVALID_AGREEMENT_STATUS;
        }
        
        //validate envelope id
        if(contractObj.Envelop_Id__c == null) {
            return Label.APTS023_INVALID_ENVELOPE_ID;
        }
        
        String accountId;
        String userId;
        String password;
        String integratorsKey;
        String webServiceUrl;
        
        //validate custom settings
        Apttus_Comply_Custom_API_Settings__c customSetting = Apttus_Comply_Custom_API_Settings__c.getValues(Custom_setting_name);
        if(customSetting == null) {
            return Label.APTS024_CUSTOM_SETTING_MISSING_INVALID;
        }
        
        if(customSetting.Account_Id__c != null) {
            accountId = customSetting.Account_Id__c;
        } else {
            return String.format(System.Label.APTS024_CUSTOM_SETTING_MISSING_INVALID, new String[] {'Account Id is not set'});
        }
        
      
        
        if(customSetting.Integrators_Key__c != null) {
            integratorsKey = customSetting.Integrators_Key__c;
        } else {
            return String.format(System.Label.APTS024_CUSTOM_SETTING_MISSING_INVALID, new String[] {'Integrators Key is not set'});
        }   
        
        if(customSetting.Web_Service_Url__c != null) {
            webServiceUrl = customSetting.Web_Service_Url__c;
        } else {
            return String.format(System.Label.APTS024_CUSTOM_SETTING_MISSING_INVALID, new String[] {'Web Service Url is not set'});
        }
        
        if(customSetting.Anchor_Tab_String__c == null){
            return String.format(System.Label.APTS024_CUSTOM_SETTING_MISSING_INVALID, new String[] {'Anchor Tab String is not set'});
        }
        
        if(customSetting.Anchor_Tab_Date_String__c == null){
            return String.format(System.Label.APTS024_CUSTOM_SETTING_MISSING_INVALID, new String[] {'Anchor Tab Date String is not set'});
        }
                                
        DocuSignStatusAPI.APIServiceSoap dssApi = new DocuSignStatusAPI.APIServiceSoap();
        dssApi.endpoint_x = webServiceUrl;

        
        //TEMP CODE START 
        String orgId = UserInfo.getOrganizationId();
       
        List<dsfs__DocuSignAccountConfiguration__c> configList = 
                            [select dsfs__SetupDone__c
                                    , dsfs__OrganizationId__c
                                    , dsfs__LookupUser__c
                                    , dsfs__IncludeDefaultAnchorTabs__c
                                    , dsfs__HideTagButton__c
                                    , dsfs__HideSendNowButton__c
                                    , dsfs__EmailSubject__c
                                    , dsfs__EmailBody__c
                                    , dsfs__DocuSignEnvironment__c
                                    , dsfs__DefaultWarnOfExpireNDays__c
                                    , dsfs__DefaultRoleValues__c
                                    , dsfs__DefaultRoleNames__c
                                    , dsfs__DefaultRepeatReminderNDays__c
                                    , dsfs__DefaultReminderNDays__c
                                    , dsfs__DefaultExpireNDays__c
                                    , dsfs__AccountId__c
                                    , dsfs__DSProSFPassword__c
                                    , dsfs__DSProSFUsername__c
                                From dsfs__DocuSignAccountConfiguration__c 
                                where dsfs__OrganizationId__c = :orgId
                                limit 1];
        //Get Requester detail
        User requestor = [select id, Name, dsfs__DSProSFUsername__c from User where id = :contractObj.OwnerId limit 1];
        String requestorDSID;
        if(requestor != null) {
            if(requestor.dsfs__DSProSFUsername__c != null ) {
                requestorDSID = requestor.dsfs__DSProSFUsername__c;
            }
        }
        String auth = '<DocuSignCredentials><Username>'+ configList[0].dsfs__DSProSFUsername__c 
            +'</Username><Password>' + configList[0].dsfs__DSProSFPassword__c
            + '</Password><IntegratorKey>' + integratorsKey 
            + '</IntegratorKey>'
            + (requestorDSID != null ? ('<SendOnBehalfOf>'+requestorDSID+'</SendOnBehalfOf>'):'')
            +'</DocuSignCredentials>';
        //TEMP CODE END
        
        dssApi.inputHttpHeaders_x = new Map<String, String>();
       
        dssApi.inputHttpHeaders_x.put(Label.Input_Http_Headers_X, auth);        
        
        try {
            //if(!Test.isRunningTest()) {
                //void envelope api call
                DocuSignStatusAPI.VoidEnvelopeStatus dsEnvelopeStatus = dssApi.VoidEnvelope(contractObj.Envelop_Id__c, 'Cancel Agreement');
                if(dsEnvelopeStatus.VoidSuccess) {
                
                //update agreement status
                contractObj.Status_Category__c = 'Cancelled';
                contractObj.BA_Contract_Status__c = '';
                contractObj.Status = 'Cancelled';    
                update contractObj;
                
                return Label.SUCCESS; 
                } else {
                    system.debug('envelope status - '+dsEnvelopeStatus);
                    return String.format(System.Label.APTS099_ERROR_OTHER, new String[] {'- Void Failed'});
                }                   
            //}
        } catch(Exception ex) {
            system.debug('exception - '+ex.getMessage());
             System.debug(LoggingLevel.ERROR, 'Exception is '+ex);
            System.debug(LoggingLevel.ERROR, 'Exception stack trace '+ex.getStackTraceString());
            return String.format(System.Label.APTS099_ERROR_OTHER, new String[] {ex.getMessage()});
        }
        return Label.SUCCESS;
    }

global static String doSend_vlocity(Id ContractId, Id generatedContractDocId, Id conversionID) {
        
        if(ContractId == null) {
            return Label.APTS011_INVALID_AGREEMENT_ID;
        }
        
        //validate agreement document id
        if(generatedContractDocId == null) {
            return Label.APTS012_INVALID_GENERATED_DOCUMENT_ID;
        }
  
        Contract c;
        id conVersId;
        try{
        c = [Select id,Name,OwnerId,Status_Category__c,Status,EndDate,ContractTerm,CustomerSigned.Email,CustomerSigned.Name,(select id from  vlocity_cmt__Contract_Versions__r where vlocity_cmt__Status__c = 'Active' ) From Contract Where id = :ContractId];
        if(c != null  ){
            for ( vlocity_cmt__ContractVersion__c cv: c.vlocity_cmt__Contract_Versions__r ){
                conVersId = cv.id;
            }
        }
        }catch(System.QueryException qe){
            return Label.APTS011_INVALID_AGREEMENT_ID;
        }
    
        //validate contract status
       if(c.Status == null) {
            return Label.APTS013_INVALID_AGREEMENT_STATUS;
        } else if(c.Status != 'Draft') {
            return Label.APTS013_INVALID_AGREEMENT_STATUS;
        }
        
        //validate recipient detail
        if(c.CustomerSigned.Email == null || c.CustomerSigned.Name == null) {
            return Label.APTS015_RECIPIENT_DETAIL_MISSING;
        }
            
        String accountId;
        String userId;
        String password;
        String integratorsKey;
        String webServiceUrl;
        
        //validate custom settings if not present create the new one.
        Apttus_Comply_Custom_API_Settings__c customSetting = Apttus_Comply_Custom_API_Settings__c.getValues(Custom_setting_name);
        if(customSetting == null) {
            return Label.APTS016_CUSTOM_SETTING_MISSING_INVALID;
        }
        
        if(customSetting.Account_Id__c != null) {
            accountId = customSetting.Account_Id__c;
        } else {
            return String.format(System.Label.APTS016_CUSTOM_SETTING_MISSING_INVALID, new String[] {'Account Id is not set'});
        }
        
        if(customSetting.Integrators_Key__c != null) {
            integratorsKey = customSetting.Integrators_Key__c;
        } else {
            return String.format(System.Label.APTS016_CUSTOM_SETTING_MISSING_INVALID, new String[] {'Integrators Key is not set'});
        }   
        
        if(customSetting.Web_Service_Url__c != null) {
            webServiceUrl = customSetting.Web_Service_Url__c;
        } else {
            return String.format(System.Label.APTS016_CUSTOM_SETTING_MISSING_INVALID, new String[] {'Web Service Url is not set'});
        }
        
        if(customSetting.Anchor_Tab_String__c == null){
            return String.format(System.Label.APTS016_CUSTOM_SETTING_MISSING_INVALID, new String[] {'Anchor Tab String is not set'});
        }
        
        String envelopeId;
        
        DocuSignAPI.APIServiceSoap dsApiSend = new DocuSignAPI.APIServiceSoap();
        dsApiSend.endpoint_x = webServiceUrl;

        String orgId = UserInfo.getOrganizationId();
       
        List<dsfs__DocuSignAccountConfiguration__c> configList = 
                            [select dsfs__SetupDone__c
                                    , dsfs__OrganizationId__c
                                    , dsfs__LookupUser__c
                                    , dsfs__IncludeDefaultAnchorTabs__c
                                    , dsfs__HideTagButton__c
                                    , dsfs__HideSendNowButton__c
                                    , dsfs__EmailSubject__c
                                    , dsfs__EmailBody__c
                                    , dsfs__DocuSignEnvironment__c
                                    , dsfs__DefaultWarnOfExpireNDays__c
                                    , dsfs__DefaultRoleValues__c
                                    , dsfs__DefaultRoleNames__c
                                    , dsfs__DefaultRepeatReminderNDays__c
                                    , dsfs__DefaultReminderNDays__c
                                    , dsfs__DefaultExpireNDays__c
                                    , dsfs__AccountId__c
                                    , dsfs__DSProSFPassword__c
                                    , dsfs__DSProSFUsername__c
                                From dsfs__DocuSignAccountConfiguration__c 
                                where dsfs__OrganizationId__c = :orgId
                                limit 1];
        //Get Requester detail
          User requestor = [select id, Name, dsfs__DSProSFUsername__c from User where id = :c.ownerid limit 1];
        
        String requestorDSID;
        if(requestor != null) {
            if(requestor.dsfs__DSProSFUsername__c != null ) {
                requestorDSID = requestor.dsfs__DSProSFUsername__c;
            }
        }
        String auth = '<DocuSignCredentials><Username>'+ configList[0].dsfs__DSProSFUsername__c 
            +'</Username><Password>' + configList[0].dsfs__DSProSFPassword__c
            + '</Password><IntegratorKey>' + integratorsKey 
            + '</IntegratorKey>'
            + (requestorDSID != null ? ('<SendOnBehalfOf>'+requestorDSID+'</SendOnBehalfOf>'):'')
            +'</DocuSignCredentials>';
        //TEMP CODE END
     
            
        dsApiSend.inputHttpHeaders_x = new Map<String, String>();
        dsApiSend.inputHttpHeaders_x.put(Label.Input_Http_Headers_X, auth);
        
        DocuSignAPI.Envelope envelope = new DocuSignAPI.Envelope();
        envelope.Subject = Label.Envelope_Subject_Send;
        envelope.EmailBlurb = Label.Envelope_Email_Blurb_Send;
        envelope.AccountId  = accountId;
        DocuSignAPI.CustomField cf = new DocuSignAPI.CustomField();
        
         cf.Name = 'Contract';
         //cf.value = contract.Id; 
         system.debug('>>Contract ID' + c.id);
         cf.value = c.Id; 
         cf.required = 'false';
         cf.show  = 'true';
         
         DocuSignAPI.CustomField cf1 = new DocuSignAPI.CustomField();
         cf1.Name = 'dsfs__Source_Object__c';
       //  cf1.value = contract.Id;
         cf1.value = c.Id; 
         
          DocuSignAPI.CustomField cf2 = new DocuSignAPI.CustomField();
         cf2.Name = 'dsfs__Custom_Field_1_Name__c';
         cf2.value = 'Contract'; 
         
         DocuSignAPI.CustomField cf3 = new DocuSignAPI.CustomField();
         cf3.Name = 'dsfs__Custom_Field_1_Value__c';
       //  cf3.value = contract.Id; 
         cf3.value = c.Id; 

         DocuSignAPI.CustomField cf4 = new DocuSignAPI.CustomField();
         cf4.Name = 'ConractVersionID';
       //  cf3.value = contract.Id; 
         cf4.value = conVersId; 
         system.debug('>> Cf4 + conversionID' + cf4.value + ':::' + conversionID + ':::'+ conVersId);
         
         DocuSignAPI.ArrayOfCustomField acf = new DocuSignAPI.ArrayOfCustomField();
         acf.CustomField = new DocuSignAPI.CustomField[]{cf,cf4}; 
         envelope.CustomFields = acf; 

            Attachment att;
            try {
                att = [select id, name, body from Attachment where id = :generatedContractDocId];
                System.debug('Attachment Details:::' + att);
            }catch(System.QueryException qe1) {
            
                return Label.APTS012_INVALID_GENERATED_DOCUMENT_ID;
            }           
  
           try {         
            // Document
            DocuSignAPI.Document document = new DocuSignAPI.Document();
            document.ID = 1;
            document.pdfBytes = EncodingUtil.base64Encode(att.body);
            document.Name = att.Name;
            document.FileExtension = Document_Format_vlocity;
            envelope.Documents = new DocuSignAPI.ArrayOfDocument();
            envelope.Documents.Document = new DocuSignAPI.Document[1];
            envelope.Documents.Document[0] = document;
        
        
        
        DocuSignAPI.Recipient recipient = new DocuSignAPI.Recipient();
        recipient.ID = 1;
        recipient.Type_x = 'Signer';
        recipient.RoutingOrder = 1;
        recipient.Email = c.CustomerSigned.Email;
        recipient.UserName = c.CustomerSigned.Name;
        

        recipient.RequireIDLookup = false;      
        
        envelope.Recipients = new DocuSignAPI.ArrayOfRecipient();
        envelope.Recipients.Recipient = new DocuSignAPI.Recipient[1];
        envelope.Recipients.Recipient[0] = recipient;
        
       // Tab
        DocuSignAPI.Tab tab1 = new DocuSignAPI.Tab();
        tab1.Type_x = 'SignHere';
        tab1.RecipientID = 1;
        tab1.DocumentID = 1;
        tab1.AnchorTabItem = new DocuSignAPI.AnchorTab();
        tab1.AnchorTabItem.AnchorTabString = string.valueOf(customSetting.Anchor_Tab_String__c);
        System.debug('** AchorSTring - Signature:' + tab1.AnchorTabItem.AnchorTabString);

        DocuSignAPI.Tab tab2 = new DocuSignAPI.Tab();
        tab2.Type_x = 'DateSigned';
        tab2.RecipientID = 1;
        tab2.DocumentID = 1;
        tab2.AnchorTabItem = new DocuSignAPI.AnchorTab();
        tab2.AnchorTabItem.AnchorTabString = string.valueOf(customSetting.Anchor_Tab_Date_String__c); 
        System.debug('** AchorSTring - Date Signed:' + tab1.AnchorTabItem.AnchorTabString);

        
        envelope.Tabs = new DocuSignAPI.ArrayOfTab();
        envelope.Tabs.Tab = new DocuSignAPI.Tab[2];
        envelope.Tabs.Tab[0] = tab1;   
        envelope.Tabs.Tab[1] = tab2;          
        
        //Set the Notification
        DocusignAPI.Notification notification = new DocuSignAPI.Notification();
        DocuSignAPI.Reminders reminders = new DocuSignAPI.Reminders();
        DocuSignAPI.Expirations expirations = new DocuSignAPI.Expirations();
        
        reminders.ReminderEnabled = customSetting.DocuSign_Reminder_Enabled__c;
        reminders.ReminderDelay = customSetting.DocuSign_Reminder__c != null ? customSetting.DocuSign_Reminder__c.intValue() : 0;
        reminders.ReminderFrequency = customSetting.DocuSign_Reminder_Frequency__c != null ? customSetting.DocuSign_Reminder_Frequency__c.intValue() : 0;
        
        expirations.ExpireEnabled = customSetting.DocuSign_Envelope_Expiry_Enabled__c;
        expirations.ExpireAfter = customSetting.DocuSign_Expiry_After_N_Days__c != null ? customSetting.DocuSign_Expiry_After_N_Days__c.intValue() : 0;
        expirations.ExpireWarn = customSetting.DocuSign_Expiry_Warning__c != null ? customSetting.DocuSign_Expiry_Warning__c.intValue() : 0;
        
        notification.Reminders = reminders;
        notification.Expirations = expirations;
        
        envelope.Notification = notification;
  
        
            //if(!Test.isRunningTest()) {
                DocuSignAPI.EnvelopeStatus es = dsApiSend.CreateAndSendEnvelope(envelope);
                envelopeId = es.EnvelopeID;
                system.debug('EnvelopID' + envelopeId);
                
                //update envelope id on agreement
                
                
                createAndUpdateEnvelope_vlocity(envelope, es.EnvelopeId);
                
               Contract cnt = new Contract(id=c.id);
                cnt.Envelop_Id__c = es.EnvelopeId;
                cnt.status = 'Contract Sent';
                cnt.No_Amend__c = false;
                update cnt;
                
                
            //}
        } catch(CalloutException e) {
            envelopeId = 'Exception - ' + e;
            return String.format(System.Label.APTS099_ERROR_OTHER, new String[] {e.getMessage()});
        }
        return Label.SUCCESS;    
    }

     public static void createAndUpdateEnvelope_vlocity(DocuSignAPI.Envelope envelope, String envelopeId) {
        dsfs__DocuSign_Envelope__c dsEnvelope = new dsfs__DocuSign_Envelope__c();
        dsEnvelope.dsfs__Source_Object__c = envelope.CustomFields.CustomField[0].Value;
        dsEnvelope.dsfs__Custom_Field_1_Name__c = 'Contract';
        dsEnvelope.dsfs__Custom_Field_1_Value__c = envelope.CustomFields.CustomField[0].Value;
        dsEnvelope.dsfs__DocuSign_Envelope_ID__c = envelopeId;
        insert dsEnvelope;
        
        dsfs__DocuSign_Envelope_Recipient__c dsRecipient = new dsfs__DocuSign_Envelope_Recipient__c();
            
            dsRecipient.dsfs__DocuSign_EnvelopeID__c = dsEnvelope.Id;
           
            dsRecipient.dsfs__DocuSign_Recipient_Role__c = 'Customer 1';
            dsRecipient.dsfs__DocuSign_Signer_Type__c = 'Signer';
            dsRecipient.dsfs__Routing_Order__c = 1;
            dsRecipient.dsfs__RoleName__c = 'Signer';
            dsRecipient.dsfs__RoleValue__c = 1;
            insert dsRecipient;
    }
 
}