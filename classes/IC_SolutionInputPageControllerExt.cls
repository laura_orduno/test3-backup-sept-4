/*
 *Controller extention class for the IC_SolutionInput lookup page. 
 * Main purpose of this controller class is to provide the Record type name VF page.
 */
 
public class IC_SolutionInputPageControllerExt{
    
    public String recordTypeName {get;set;}
    
    public IC_Solution__c icsvalue {get; set;}
    
    public IC_SolutionInputPageControllerExt(ApexPages.StandardController stdController) {
        IC_Solution__c ics = (IC_Solution__c) stdController.getRecord();
        icsvalue=ics;
        if(ics.id ==null && ics.Case__c != null){
        Case caseObj = [ Select Id, Contact.Email from Case where id =:ics.Case__c];
            if(caseObj !=null){
                ics.Case_Contact_Email__c = caseObj.Contact.Email;
            }
        }
        RecordType rt =  [select Id, Name from RecordType where Id = :ics.RecordTypeId];
        this.recordTypeName = rt.Name;
    }
   
}