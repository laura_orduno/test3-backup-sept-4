@isTest
private class QuoteDocumentGeneratorTest {
	testMethod static void test() {
        Opportunity opp = new Opportunity(Name='Test',CloseDate=System.today(),StageName='Prospecting');
        insert opp;
        
        SBQQ__Quote__c quote = new SBQQ__Quote__c(SBQQ__Opportunity__c=opp.Id);
        insert quote;
        
        SBQQ__QuoteTemplate__c template = new SBQQ__QuoteTemplate__c(Name='Test');
        insert template;
        
        QuoteDocumentGenerator target = new QuoteDocumentGenerator(template);
        target.signatureBlock = true;
        target.recombo = true;
        target.preview(quote.Id);
        target.generate(quote.Id);
       // System.assert(target.getCompanyLogoUrl(opp.Id) != null);
    }
}