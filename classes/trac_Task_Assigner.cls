global without sharing class trac_Task_Assigner {
    // Constructor - this only really matters if the autoRun function doesn't work right
    private final Task t;
    public trac_Task_Assigner(ApexPages.StandardController stdController) {
        this.t = (Task)stdController.getRecord();
    }
 
    // Code we will invoke on page load.
    public PageReference autoRun() {
 
        String theId = ApexPages.currentPage().getParameters().get('id');
 
        if (theId == null) {
            // Display the Visualforce page's content if no Id is passed over
            return null;
        }
 
        for (Task t:[select id, ownerid from Task where id =:theId]) {
            t.ownerid = UserInfo.getUserId();
            update t;
        }
 
        // Redirect the user back to the original page
        PageReference pageRef = new PageReference('/' + theId);
        pageRef.setRedirect(true);
        return pageRef;
 
    }
}