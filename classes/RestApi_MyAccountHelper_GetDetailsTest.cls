@isTest
public class RestApi_MyAccountHelper_GetDetailsTest {

	private static final String REQUEST_TYPE_GET_CASE_DETAILS = 'get-case-details';
	private static final String UUID = '909090';
    private static final String CASE_ORIGIN = 'My Account';
    private static final String BILLINGACCOUNTNUMBER = '1337';
    private static final List<String> BILLINGACCOUNTNUMBERS = new List<String>{BILLINGACCOUNTNUMBER};
    private static final String COMMENT_BODY = 'This is the comment body';
    private static final String CONTACTFIRST = 'Phillip';
    private static final String CONTACTLAST = 'Smith';
    private static final String CONTACTFULL = CONTACTFIRST + ' ' + CONTACTLAST;
    private static final String USERNAME = 'psmith@telus.com' + System.currentTimeMillis();
    private static final String REGUUID = '190190190';
    private static final String INTERNALSTATUS = 'New';
    private static final String EXTERNALSTATUS = 'Newest';

    private static User myUser;
    private static Case myCase;
    private static Account myAcct;
    private static Account portalAcct;
    private static User regUser;
    private static CaseComment myComment;
    private static Attachment myAttachment;
    private static Segment__c segment;
    private static ExternalToInternal__c etoI;

    private static RestApi_MyAccountHelper_GetCaseDetails getDetailsObj;
    private static RestApi_MyAccountHelper_GetCaseDetails.GetCaseDetailsResponse resp;

    private static void setup() {
        trac_TriggerHandlerBase.blockTrigger = true;

        segment = RestApi_MyAccountHelper_TestUtils.createSegment();
        insert segment;

        etoI = RestApi_MyAccountHelper_TestUtils.createEtoI();
        insert etoI;

    	portalAcct = new Account(Name = 'Portal Account Inc.', sub_Segment_Code__c = segment.id);
    	insert portalAcct;

    	myAcct = RestApi_MyAccountHelper_TestUtils.createAccount(segment.Id);
    	insert myAcct;

    	myUser = RestApi_MyAccountHelper_TestUtils.createPortalUser(portalAcct);
    	insert myUser;

		List<Contact> contacts = [SELECT MASR_Enabled__c FROM Contact];
		for(Contact c : contacts){
			c.MASR_Enabled__c = true;
		}
		update contacts;

    	myCase = RestApi_MyAccountHelper_TestUtils.createCase(myAcct, myUser);
    	insert myCase;

        myComment = RestApi_MyAccountHelper_TestUtils.createCaseComment(myCase);
        insert myComment;

        myAttachment = RestApi_MyAccountHelper_TestUtils.createAttachment(myCase);
        insert myAttachment;


        regUser = RestApi_MyAccountHelper_TestUtils.createRegularUser();
        insert regUser;
    	//TODO: create comments/attachements for myCase
        trac_TriggerHandlerBase.blockTrigger = false;
    }


    @isTest
    private static void testRequest() {
    	setup();
        createRequest(UUID, myCase.Id, BILLINGACCOUNTNUMBERS);
    
        Test.startTest();
        getDetailsObj = new RestApi_MyAccountHelper_GetCaseDetails(RestContext.request.requestBody.toString()); 
        resp = getDetailsObj.response;          
        Test.stopTest();
        
        System.debug('Response : ' + resp);
        
        //TODO: assert the fields of resp object.
    }

    @isTest
    private static void testNoUUID() {
        setup();
        createRequest('190129302', myCase.Id, BILLINGACCOUNTNUMBERS);

        try{
            getDetailsObj = new RestApi_MyAccountHelper_GetCaseDetails(RestContext.request.requestBody.toString()); 
            resp = getDetailsObj.response;
        } catch(Exception e) {
            System.assert(true, e.getMessage());
        }    
        //TODO: assert the fields of resp object.
    }

    @isTest
    private static void testNoCase() {
        setup();
        createRequest(UUID, null, BILLINGACCOUNTNUMBERS);

        try{
            getDetailsObj = new RestApi_MyAccountHelper_GetCaseDetails(RestContext.request.requestBody.toString()); 
            resp = getDetailsObj.response;
        } catch(Exception e) {
            System.assert(true, e.getMessage());
        }    
        //TODO: assert the fields of resp object.
    }

    @isTest
    private static void testNoBilling() {
        setup();
        createRequest(UUID,  myCase.Id, null);

        try{
            getDetailsObj = new RestApi_MyAccountHelper_GetCaseDetails(RestContext.request.requestBody.toString()); 
            resp = getDetailsObj.response;
        } catch(Exception e) {
            System.assert(true, e.getMessage());
        }    
        //TODO: assert the fields of resp object.
    }

    @isTest
    private static void testNoContact() {
        setup();
        createRequest(REGUUID,  myCase.Id, BILLINGACCOUNTNUMBERS);

        try{
            getDetailsObj = new RestApi_MyAccountHelper_GetCaseDetails(RestContext.request.requestBody.toString()); 
            resp = getDetailsObj.response;
        } catch(Exception e) {
            System.assert(true, e.getMessage());
        }    
        //TODO: assert the fields of resp object.
    }

    private static void createRequest(String uuid, String caseId, List<String> banumber) {
    	RestApi_MyAccountHelper_GetCaseDetails.GetCaseDetailsRequestObject caseDetailsReq = new RestApi_MyAccountHelper_GetCaseDetails.GetCaseDetailsRequestObject();
        caseDetailsReq.uuid = uuid;
    	caseDetailsReq.caseId = caseId;
    	caseDetailsReq.billing = banumber;
    	caseDetailsReq.type = REQUEST_TYPE_GET_CASE_DETAILS;
    	RestRequest request = new RestRequest();
        request.requestUri ='/services/apexrest/RestApi_MyAccount/';
        request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(JSON.serialize(caseDetailsReq));
        RestContext.request = request;
    }
}