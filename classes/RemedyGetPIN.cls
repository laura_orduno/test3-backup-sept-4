public without sharing class RemedyGetPIN {
//global class RemedyGetPIN {
    //global class sendRequest {
    public class sendRequest {
    /*   global String OKTicketId = '';
      global String BUID = '';
      global String COMID = '';
      global String PINNum = '';
      global String UpdateClientView = '';
         global String OKTicketId = '';*/
      public String BUID = '';
      public String COMID = '';
      public String PINNum = '';
      public String UpdateClientView = '';
      public String OKTicketId = '';
      public RemedyGetPIN.sendRequest SRequest(string email)
          // public list<Remedy_Trouble_Ticket.TroubleTicket> SRequest(string pin, string ticketNumber)
      {
          string upper = email.toUpperCase();
          // Create the request envelope
          DOM.Document doc = new DOM.Document();
          //String endpoint = 'http://205.206.192.11/arsys/services/ARService?server=205.206.195.52&webService=ExternalTicketSubmissionCreate';
           // String endpoint = 'https://soa-mp-toll-dv.tsl.telus.com:443/SMO/ProblemMgmt/ExternalTicketCreationService_v1_0_vs0';
           //String endpoint = 'https://xmlgwy-pt1.telus.com:9030/dv/SMO/ProblemMgmt/ExternalTicketClientInfoListService_v1_0_vs0';
            
            // TODO: Re-leverage custom setting for endpoint, no hard-coding
             String endpoint = SMBCare_WebServices__c.getInstance('ENDPOINT_Remedy_PIN').Value__c;
            //String endpoint = 'https://xmlgwy.telus.com:9030/SMO/ProblemMgmt/ExternalTicketCreationService_v1_0_vs0';
            ////////////
            String soapNS = 'http://schemas.xmlsoap.org/soap/envelope/';
            string urn = 'urn:ClientInfoList';
            //string asr = 'http://assurance.ebonding.telus.com';
            datetime creationDateTime = datetime.now();
            date mydate = creationDateTime.date();
            datetime getmytime = datetime.now();
            time mytime = getmytime.time();
            //string username = 'frontline';
            //string password = 'Fline2014';
          string username = 'salesforce';
            string password = 'SFDC2016';
           string emailSet = '\'E-mail address\'=';
            
                      dom.XmlNode envelope
                = doc.createRootElement('Envelope', soapNS, 'soapenv');
            envelope.setNamespace('urn', urn);
            //envelope.setNamespace('asr', asr);
            
            
            dom.XmlNode header = envelope.addChildElement('Header', soapNS, null);
            dom.XmlNode AuthenticationInfo = header.addChildElement('AuthenticationInfo', urn, 'urn');
           
           // dom.XmlNode sender = AuthenticationInfo.addChildElement('sender', ebon, null);
            AuthenticationInfo.addChildElement('userName', urn, null).
                addTextNode(username);
            AuthenticationInfo.addChildElement('password', urn, null).
                addTextNode(password);
            AuthenticationInfo.addChildElement('authentication', urn, null).
                addTextNode('');
            AuthenticationInfo.addChildElement('locale', urn, null). 
                addTextNode('');
            AuthenticationInfo.addChildElement('timeZone', urn, null).
                addTextNode('');
           
         
            dom.XmlNode body = envelope.addChildElement('Body', soapNS, null);
            dom.XmlNode OpCreate = body.addChildElement('OpGetList', urn, 'urn');
            OpCreate.addChildElement('Qualification', urn, null).
                addTextNode(emailSet +'"'+upper+'"');
            OpCreate.addChildElement('startRecord', urn, null).
                addTextNode('0');
            OpCreate.addChildElement('maxLimit', urn, null).
                addTextNode('1');
            System.debug(doc.toXmlString());
            
            // TODO: add exception handling
            
            // Send the request
            HttpRequest req = new HttpRequest();
            req.setMethod('GET');
            req.setEndpoint(endpoint);
            // TODO: specify a timeout of 2 minutes 
           req.setTimeout(120000);
            
            String creds;
         SMBCare_WebServices__c wsUsername = SMBCare_WebServices__c.getValues('username_pc');
         SMBCare_WebServices__c wsPassword = SMBCare_WebServices__c.getValues('password');
          
            if (String.isNotBlank(wsUsername.Value__c)&& String.isNotBlank(wsPassword.Value__c))
                creds = wsUsername.Value__c+':'+wsPassword.Value__c;
            String encodedusernameandpassword = EncodingUtil.base64Encode(Blob.valueOf(creds));
            
            //Blob headerValue = Blob.valueOf(encodedusernameandpassword);
            String authorizationHeader = 'BASIC ' + encodedusernameandpassword;
            // EncodingUtil.base64Encode(headerValue);
            req.setHeader('Authorization', authorizationHeader);
            req.setHeader('Content-Type', 'text/xml');
            
            req.setBodyDocument(doc);
            
            // TODO: Determine the level of detail webservice is returning in regards to errors.
            
            Http http = new Http();
            HttpResponse res = http.send(req);
            //System.assertEquals(500, res.getStatusCode());
            
 //list<Remedy_Trouble_Ticket.TroubleTicket> XMLReturn = res.getBody();           
            System.debug('GetBody line 110:' +res.getBody());
            // System.assertEquals(200, res.getStatusCode());
            
            dom.Document resDoc = res.getBodyDocument();
            XmlStreamReader reader = res.getXmlStreamReader();
            
            // Read through the XML 
           
            while(reader.hasNext()) { 
                System.debug('Event Type:' + reader.getEventType()); 
                if (reader.getEventType() == XmlTag.START_ELEMENT) { 
                    System.debug('getLocalName: ' +reader.getLocalName());
                    if ('Business_Unit_Id' == (reader.getLocalName())){
                        boolean isSafeToGetNextXmlElement = true; 
                        while(isSafeToGetNextXmlElement) { 
                           if (reader.getEventType() == XmlTag.END_ELEMENT) {
                                OKTicketId = reader.getLocalName();
                                System.debug('OK Ticket Element Line 116 :' + OKTicketId);
                                break; 
                            } else if (reader.getEventType() == XmlTag.CHARACTERS) { 
                                BUID = reader.getText();
                                System.debug('BUID Line 120 :' + BUID);
          
                            } 
         
                            if (reader.hasNext()) { 
                                
                                reader.next(); 
                                
                            } else { 
                                
                                isSafeToGetNextXmlElement = false; 
                                break; 
                            } 
                        } 
                        
                    }
                    
 if ('Company_Id' == (reader.getLocalName())){
                        boolean isSafeToGetNextXmlElement = true; 
                        while(isSafeToGetNextXmlElement) { 
                           if (reader.getEventType() == XmlTag.END_ELEMENT) {
                                OKTicketId = reader.getLocalName();
                                System.debug('OK Ticket Element Line 142 :' + OKTicketId);
                                break; 
                            } else if (reader.getEventType() == XmlTag.CHARACTERS) { 
                                COMID = reader.getText();
                                System.debug('Description Line 146 :' + COMID);
          
                            } 
         
                            if (reader.hasNext()) { 
                                
                                reader.next(); 
                                
                            } else { 
                                
                                isSafeToGetNextXmlElement = false; 
                                break; 
                            } 
                        } 
                        
                    }                    
                   if ('PIN' == (reader.getLocalName())){
                        boolean isSafeToGetNextXmlElement = true; 
                        while(isSafeToGetNextXmlElement) { 
                           if (reader.getEventType() == XmlTag.END_ELEMENT) {
                                OKTicketId = reader.getLocalName();
                                System.debug('OK Ticket Element Line 167 :' + OKTicketId);
                                break; 
                            } else if (reader.getEventType() == XmlTag.CHARACTERS) { 
                                PINNum = reader.getText();
                                System.debug('PIN Number Info Line 171 :' + PINNum);
          
                            } 
         
                            if (reader.hasNext()) { 
                                
                                reader.next(); 
                                
                            } else { 
                                
                                isSafeToGetNextXmlElement = false; 
                                break; 
                            } 
                        } 
                        
                    }                     
                    
                }
                reader.next(); 
            } 
            if(PINNum!=null || PINNum!='')
            { 
               UpdAccountContact(PINNum, COMID, BUID);
                
            }
            dom.XMLNode TroubleTicket = resDoc.getRootElement();
           system.debug('TroubleTicketDOM: '+TroubleTicket);
            //  String troubleTick = 'http://www.telus.com/schema/servicestatus/troubleticket';
            //String wsa = 'http://schemas.xmlsoap.org/soap/envelope/';
           // Dom.XMLNode TroubleTicket = resDoc.getRootElement();
           // String DescriptionView = TroubleTicket.getChildElement('Description',null).getText();
            //system.debug('Description: '+DescriptionView);
            // dom.XmlNode header1 = envelope.getChildElement('Header', soapNS);
            // String TELUSTicketId = TroubleTicket.getChildElement('TELUSTicketId',null).getText();
            //System.debug('TELUSTicketId: ' + TELUSTicketId);
            System.debug(resDoc.toXmlString());
            //string XMLReturn = resDoc.toXmlString();
            System.debug('Envelope=:' + envelope);
            //  System.debug('TroubleTicket=:' + TroubleTicket);
            //System.debug(header1);
            //System.debug(testTime);
           
            return null;
        }
 public PageReference UpdAccountContact(String PinNum, String Comid, String BUid) {
        ENTPAccounTContactUpdate.UpdateAccCont(PinNum,Comid, BUid);
        
          /*           user g = [Select AccountId, ContactId from User where id =:UserInfo.getUserId()];
            Account AccountToUPdate = [Select Remedy_Business_Unit_Id__c,Remedy_Company_Id__c from Account where Id =: g.AccountId ];
            Contact ContacttoUpdate = [Select Remedy_PIN__c from Contact where Id =: g.ContactId];
                    System.debug('RemedyAddPIN->NewPIN, PIN(' + PinNum + ')');
                     System.debug('RemedyAddCOMID->NewCOMID, COMID(' + Comid + ')');
                    System.debug('RemedyAddBUID->NewBUID, BUID(' + BUid + ')');
                    
                    AccountToUPdate.Remedy_Business_Unit_Id__c = BUid;
                    AccountToUPdate.Remedy_Company_Id__c = Comid;
                    ContacttoUpdate.Remedy_PIN__c = PinNum;
              try{      
                    Update AccountToUPdate;
                    Update ContacttoUpdate;
                    system.debug('Update Successful');
                }
                catch(System.DmlException e) {
                    System.debug('RemedyAddPIN->TicketEvent, exception: ' + e.getDmlMessage(0));
                } */
        return null;
    }   
    }
 


}