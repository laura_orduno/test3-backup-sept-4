public class QuotePortalSettings {
    public Quote_Portal__c qps {get; set;}
    public Boolean releaseFiveOneIsEnabled {
        get {
            return (
                qps != null &&
                qps.Enable_Release_5_1__c != null &&
                qps.Enable_Release_5_1__c == true);
        }
    }
    public QuotePortalSettings() { init(); } public QuotePortalSettings(QuoteDetailController q) { init(); } public QuotePortalSettings(QuoteGroupController q) { init(); }
    
    public void init() {
        qps = Quote_Portal__c.getInstance();
    }

}