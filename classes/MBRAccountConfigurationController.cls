public class MBRAccountConfigurationController {

    private ApexPages.StandardController controller {get; set;}
    private Account a;
    public List<Account_Configuration__c> configs {get; set;}
    public Boolean restrictTechSupport {get; set;}

    public MBRAccountConfigurationController(ApexPages.StandardController controller) {
        // initialize the standard controller
        this.controller = controller;
        Account tmpA = (Account)controller.getRecord();
        this.a = [SELECT Id, Name FROM Account WHERE Id = :tmpA.Id][0];
        this.configs = [SELECT Id, Restrict_Tech_Support__c FROM Account_Configuration__c WHERE Account__c = :this.a.Id ORDER BY CreatedDate ASC];
        if (this.configs.size() > 0) {
            this.restrictTechSupport = this.configs[0].Restrict_Tech_Support__c;
        } else {
            this.restrictTechSupport = false;
        }
    }
    
    public PageReference save() {
        if (this.configs.size() > 0) {
            // update
            this.configs[0].Restrict_Tech_Support__c = restrictTechSupport;
            update this.configs[0];
        } else {
            // insert
            Account_Configuration__c cfg = new Account_Configuration__c();
            cfg.Name = 'Account Configuration for ' + this.a.Name;
            cfg.Account__c = this.a.Id;
            cfg.Restrict_Tech_Support__c = restrictTechSupport;
            insert cfg;
            this.configs.add(cfg);
        }
        return null;
    }
}