@isTest
private class TestForStayInTouchUtil{
    static testMethod void testForCodeCoverage(){
        Id leadID = '00Q4000000W6g0W';
        Id contId = '0034000000Ztchp';        
        StayInTouchUtil.getLeadList(leadID );
        StayInTouchUtil.getContactList(contId );
         // created Account -Start
             Account acc = new Account(
                 Name= 'Test123',
                 phone='6049969449'
             );
             insert acc;
             System.debug('Account='+acc);
             //Account is updated as Partner Account 
             acc.IsPartner = true;
             update acc;
             System.debug('Ispartner='+acc);
         //created Account -End  
         //Contact created  -Start 
             Contact con = new Contact(
                 LastName = 'Test Contact',
                 AccountId= acc.Id,
                 phone='6049969449'
             );
             insert con;
             System.debug('contact='+con); 
         //Contact created  -End
         con.LastName = 'Test Update';
         SObject sObj = (SObject )con;
         StayInTouchUtil.updateCall(sObj);
         Contact c=[select LastName  from Contact where ID= :con.Id];
         System.assertEquals('Test Update', c.LastName); 
    }
}