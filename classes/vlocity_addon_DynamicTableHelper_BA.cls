public class vlocity_addon_DynamicTableHelper_BA {
    public static string BLDService = 'Business Long Distance Service';
    public static string BLDAdditionalInfo = 'Business Long Distance Overseas Rate Schedule';  
    public static Map<Id , vlocity_cmt__ContractLineItem__c> mapHierarchy = new Map<Id , vlocity_cmt__ContractLineItem__c>();  
    public static List<hierarchyItem> hierarchyItemList;
    private static Integer hirarchyIndex = 0;
    public static string CliPrintedProductName = 'TELUS_Printed_Product_Name__c';
    public static string HEADERSTRING = 'headermap';
    public static string FORMATCOLUMN = 'formatcolumn'; 
    public static string TABLESTYLE  = 'tablestyle';
    public static string HEADERSTYLE  = 'headerstyle';
    public static string TBLHEADERSTYLE  = 'tblHeaderStyle';
    public static string TBLROWSTYLE  = 'tblRowStyle';

    public static map<string,object> getCliMRCData(string contractId){
        map<string,object> mapTableInfo=new  map<string,object>();
        String tableCssStyle = 'border: 1px solid black;width: 100%;border-collapse: collapse;';
        String headerCssStyle='border-bottom: 1px solid black;border-right: 1px solid black;';          
        String tableHeaderStyle='font-size:11pt;font-family:Arial,sans-serif;background-color:#49166D;';
        string tableRowStyle='background-color:#dfb3ff;';
        string  TotalMRCOnContract = '';
        mapTableInfo.put(HEADERSTRING,new Map<String, String>{'TELUS Business Anywhere Bundle Includes:'=> CliPrintedProductName,       
                                     'Qty'=> 'vlocity_cmt__Quantity__c',
                                     'Monthly Charge'=>'TELUS_Printed_MRC__c'                     
                                        });
        mapTableInfo.put('groupBy','NA');
        
           list<sobject> lstSobject=new list<sobject>();
        mapHierarchy=new Map<Id , vlocity_cmt__ContractLineItem__c>([Select   vlocity_cmt__Product2Id__r.name  ,name,vlocity_cmt__ContractId__r.Agreement_Type__c, TELUS_Printed_Product_Name__c  
                                                          //  vlocity_cmt__ContractId__r.Printed_Total_MRC__c
                                                            ,TELUS_Printed_MRC__c,vlocity_cmt__Quantity__c,TELUS_Printed_Indent_Level__c,Parent_Contract_Line_Item__c ,TELUS_Business_Tool__c
                                                                     ,TELUS_Printed_Document_Display_in_MRC__c ,vlocity_cmt__ContractId__r.Printed_Total_MRC__c
                                           from vlocity_cmt__ContractLineItem__c where vlocity_cmt__ContractId__c = :contractId 
                                                                     and TELUS_Printed_Document_Display_in_MRC__c = true order by TELUS_Printed_Indent_Level__c 
                                           ]);
        Map<Id , vlocity_cmt__ContractLineItem__c> CliListforBT = mapHierarchy;
        hierarchyItemList = new List<hierarchyItem>();  
        for(vlocity_cmt__ContractLineItem__c objCLI : mapHierarchy.values()){ 
            
            if(objCLI.TELUS_Printed_Indent_Level__c  == 0 && objCLI.TELUS_Business_Tool__c  == false){  
                hierarchyItemList.add(new hierarchyItem( objCLI, 0,CliPrintedProductName));  
                mapHierarchy.remove(objCLI.id); 
                hierarchyItemList = getChildHierarchy(objCLI.id , hierarchyItemList, CliPrintedProductName);  
            }
            if(objCLI.TELUS_Printed_Indent_Level__c  == 1 && objCLI.TELUS_Business_Tool__c  == false){  
                hirarchyIndex = 1;
                hierarchyItemList.add(new hierarchyItem( objCLI, hirarchyIndex,CliPrintedProductName));  
                mapHierarchy.remove(objCLI.id); 
                hierarchyItemList = getChildHierarchy(objCLI.id , hierarchyItemList, CliPrintedProductName);  
            }
            if(TotalMRCOnContract == '' ){
               TotalMRCOnContract = string.valueof(objCLI.vlocity_cmt__ContractId__r.Printed_Total_MRC__c) ;
                system.debug('TotalMRCOnContract'+TotalMRCOnContract);
            }
            
        }
        system.debug('***hierarchyItemList***'+hierarchyItemList);
        for(hierarchyItem objhierarchyItem : hierarchyItemList){
            lstSobject.add(objhierarchyItem.objSobject);
        }
        integer row = 0;
        list<sObject> BtCliList = new list<sObject>();
        for(vlocity_cmt__ContractLineItem__c objCliBT : CliListforBT.values()){ 
            if(objCliBT.TELUS_Business_Tool__c == true ){
                if(row ==0){
                    vlocity_cmt__ContractLineItem__c  bt_cli = new vlocity_cmt__ContractLineItem__c( TELUS_Printed_Product_Name__c = 'Business Tool' ); 
                    BtCliList.add(bt_cli);
                    row++;
                }
                    string spaceCount = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    vlocity_cmt__ContractLineItem__c  bt_cli = new vlocity_cmt__ContractLineItem__c( TELUS_Printed_Product_Name__c = string.valueof(spaceCount+ objCliBT.TELUS_Printed_Product_Name__c), vlocity_cmt__Quantity__c = objCliBT.vlocity_cmt__Quantity__c, TELUS_Printed_MRC__c = objCliBT.TELUS_Printed_MRC__c );                
                    BtCliList.add(bt_cli);   
            }
        }
        
        if(BtCliList.size()> 0 && !BtCliList.isEmpty() ){
            lstSobject.addAll(BtCliList);
        }
        
        mapTableInfo.put('items',lstSobject);
        map<string,object> mapHeaderInfo=(map<string,object>)mapTableInfo.get(HEADERSTRING);
        map<string,string> mapFormatColumn=new map<string,string>();
        map<string,string> headerTextAllignMap = new map<string, string>();
        Map<String, Schema.SObjectField> fieldMap =Schema.getGlobalDescribe().get('vlocity_cmt__ContractLineItem__c').getDescribe().fields.getMap();
        integer rowcount = 0;
        for(string name : mapHeaderInfo.keyset()){
          
          Schema.DisplayType fielddataType = fieldMap.get((string)mapHeaderInfo.get(name)).getDescribe().getType(); 
            if(fielddataType == Schema.DisplayType.CURRENCY){
              mapFormatColumn.put(name,'$');
            }
            if( rowcount != 0){
              headerTextAllignMap.put(name, 'center');

            }
             rowcount ++;

         }
        vlocity_cmt__ContractLineItem__c  cl = new vlocity_cmt__ContractLineItem__c(TELUS_Printed_MRC__c = string.valueOf(TotalMRCOnContract),
                                                                                    TELUS_Printed_Product_Name__c = 'Total Monthly Charge');
        lstSobject.add(cl);
        mapTableInfo.put('textAllign', headerTextAllignMap);
        mapTableInfo.put(FORMATCOLUMN,mapFormatColumn); 
        mapTableInfo.put(TABLESTYLE, tableCssStyle ); 
        mapTableInfo.put(HEADERSTYLE, headerCssStyle ); 
        mapTableInfo.put(TBLHEADERSTYLE, tableHeaderStyle );
        mapTableInfo.put(TBLROWSTYLE, tableRowStyle );
        System.debug('List of ContreactLineItem' + lstSobject);
        return mapTableInfo;
        
    } 
    
 
    public static map<string,object> getCliNRCData(string contractId){
        map<string,object> mapTableInfo=new  map<string,object>();
        String tableCssStyle = 'border: 1px solid black;width: 100%;border-collapse: collapse;';
        String headerCssStyle='border-bottom: 1px solid black;border-right: 1px solid black;';          
        String tableHeaderStyle='font-size:11pt;font-family:Arial,sans-serif;background-color:#49166D;';
        String headerSpace = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        String headerAmount =headerSpace+'Amount'+headerSpace;
        mapTableInfo.put(HEADERSTRING,new Map<String, String>{'One Time Charges'=> CliPrintedProductName,       
                                     'Qty'=> 'vlocity_cmt__Quantity__c',
                                     headerAmount =>'TELUS_Printed_NRC__c'                     
                                        });
        mapTableInfo.put('groupBy','NA');
        
        list<sobject> lstSobject=new list<sobject>();
        string  TotalNRCOnContract = '';
        integer childDummyCount = 0;
        mapHierarchy=new Map<Id , vlocity_cmt__ContractLineItem__c>();
        for(vlocity_cmt__ContractLineItem__c cli : [Select   vlocity_cmt__Product2Id__r.name  ,name,vlocity_cmt__ContractId__r.Agreement_Type__c, TELUS_Printed_Product_Name__c , Printed_Child_Product_Name_Derived_NRC__c  
                                                    ,TELUS_Printed_MRC__c,TELUS_Printed_NRC__c,vlocity_cmt__Quantity__c,TELUS_Printed_Indent_Level__c,Parent_Contract_Line_Item__c,TELUS_Business_Tool__c 
                                                    ,TELUS_Printed_Document_Display_in_NRC__c ,vlocity_cmt__ContractId__r.Printed_Total_NRC__c 
                                                    from vlocity_cmt__ContractLineItem__c where vlocity_cmt__ContractId__c = :contractId 
                                                    and TELUS_Printed_Document_Display_in_NRC__c = true order by TELUS_Printed_Display_Order__c 
                                                   ]){
                                                       mapHierarchy.put(cli.id, cli);   
                                                       if(cli.Printed_Child_Product_Name_Derived_NRC__c != null ){
                                                           System.debug('** Parent CLI for Item Discount **'+ cli.TELUS_Printed_Product_Name__c);
                                                           string dummyId = cli.id;
                                                          // dummyId = dummyID.left(11)+'000';
                                                          dummyId = dummyID.left(11);
                                                          string countDigits = string.valueof(childDummyCount);
                                                          for( integer i=3; i >= countDigits.length() ; i-- )
                                                                    dummyId = dummyID + '0';
                                                           vlocity_cmt__ContractLineItem__c  childCli = new vlocity_cmt__ContractLineItem__c(id = dummyID+childDummyCount, Parent_Contract_Line_Item__c = cli.id, 
                                                                                                                                             TELUS_Printed_Product_Name__c = cli.Printed_Child_Product_Name_Derived_NRC__c );
            
                                                           mapHierarchy.put(childCli.id, childCli);  
                                                           childDummyCount++; 
                                                                                               
                                                       }
                                                        
                                                       
                                                   }
        hierarchyItemList = new List<hierarchyItem>();  
        integer rowcount = 0;
        
        for(vlocity_cmt__ContractLineItem__c objCLI : mapHierarchy.values()){  
            if(mapHierarchy.size() == 0){
                break;
            }
            if(objCLI.TELUS_Printed_Indent_Level__c  == 0 && objCLI.TELUS_Business_Tool__c  == false){ 
               
                hierarchyItemList.add(new hierarchyItem( objCLI, 0,CliPrintedProductName));  
                mapHierarchy.remove(objCLI.id); 
                hierarchyItemList = getChildHierarchy(objCLI.id , hierarchyItemList, CliPrintedProductName); 
            }
            if(objCLI.TELUS_Printed_Indent_Level__c  == 1 && objCLI.TELUS_Business_Tool__c  == false){  
                hirarchyIndex = 1;
                hierarchyItemList.add(new hierarchyItem( objCLI, hirarchyIndex,CliPrintedProductName));  
                mapHierarchy.remove(objCLI.id); 
                hierarchyItemList = getChildHierarchy(objCLI.id , hierarchyItemList, CliPrintedProductName); 
                
            }
            if(TotalNRCOnContract == '' ){
               TotalNRCOnContract = objCLI.vlocity_cmt__ContractId__r.Printed_Total_NRC__c ;
            }
        }
        system.debug('***hierarchyItemList***'+hierarchyItemList);
        for(hierarchyItem objhierarchyItem : hierarchyItemList){
            lstSobject.add(objhierarchyItem.objSobject);
        }

            vlocity_cmt__ContractLineItem__c  cl = new vlocity_cmt__ContractLineItem__c(TELUS_Printed_NRC__c = string.valueOf(TotalNRCOnContract),
                                                                                        TELUS_Printed_Product_Name__c = 'Total One Time Charge');
            lstSobject.add(cl);
    
        mapTableInfo.put('items',lstSobject);
        map<string,object> mapHeaderInfo=(map<string,object>)mapTableInfo.get(HEADERSTRING);
        map<string,string> mapFormatColumn=new map<string,string>();
        map<string,string> headerTextAllignMap = new map<string, string>();
        Map<String, Schema.SObjectField> fieldMap =Schema.getGlobalDescribe().get('vlocity_cmt__ContractLineItem__c').getDescribe().fields.getMap();

        for(string name : mapHeaderInfo.keyset()){
          
          Schema.DisplayType fielddataType = fieldMap.get((string)mapHeaderInfo.get(name)).getDescribe().getType(); 
            if(fielddataType == Schema.DisplayType.CURRENCY){
              mapFormatColumn.put(name,'$');
            }
            if( rowcount != 0){
              headerTextAllignMap.put(name, 'center');

            }
             rowcount ++;

         }

        mapTableInfo.put('textAllign', headerTextAllignMap);
        mapTableInfo.put(FORMATCOLUMN,mapFormatColumn); 
        mapTableInfo.put(TABLESTYLE, tableCssStyle ); 
        mapTableInfo.put(HEADERSTYLE, headerCssStyle ); 
        mapTableInfo.put(TBLHEADERSTYLE, tableHeaderStyle );
        System.debug('List of ContreactLineItem' + lstSobject);
        return mapTableInfo;
        
    } 
    
 
    public static Map<String,Object> buildDocumentSectionContent (Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options){                 
        String contextObjName = (String) inputMap.get('contextObjName');          
        Id contractId = (Id) inputMap.get('contextObjId'); 
        List<SObject> lineItems = (List<SObject>) inputMap.get('items');
        String docStyle = (String) inputMap.get('documentFontStyle');
        String groupByFieldName = (String)inputMap.get('groupBy');
        Map<String,string> headerMap = (Map<String,String>)inputMap.get(HEADERSTRING); 
        string addDataCell = addDataCells(headerMap);
        map<string, Object> preparedataInputMap = new map<string,object>();
        map<string,map<string,decimal>> cliHeaderLevelMap = new map<string,map<string,decimal>>();
        Map<String,string> formatcolumnmap = new map<String,String>();
        Map<string,string> headerTextAllignMap = new map<string, string>();
        string sRowStyle='';
     
        if(inputMap.containsKey(FORMATCOLUMN))
            preparedataInputMap.put(FORMATCOLUMN,inputMap.get(FORMATCOLUMN))  ; 
       
        if(inputMap.containsKey(FORMATCOLUMN))
            formatcolumnmap =  (Map<String,String>)inputMap.get(FORMATCOLUMN); 
        
        //prepare body
        Map<String, List<Map<String, String>>> dataMap = new Map<String, List<Map<String, String>>>();
        
        if(lineItems.size() > 0 && !lineItems.isEmpty() )
            dataMap = prepareData(lineItems,groupByFieldName, headerMap,preparedataInputMap );
        //font-size:11pt;font-family:Arial,sans-serif;background-color:#49166D;color:#ffffff;font-weight: bold;
        
       
        /* String tableStyle = 'border: 1px solid black;width: 100%;border-collapse: collapse;';
        String headerStyle='border-bottom: 1px solid black;border-right: 1px solid black;';          
        String tableHeaderStyle='font-size:9pt;font-family:Arial,sans-serif;background-color:#49166D;'; */
        string tableCssStyle;
        string headerCssStyle;
        string tableHeaderStyle;
         
        if(inputMap.containsKey(TABLESTYLE))
             tableCssStyle  = (string)inputMap.get(TABLESTYLE);
        if(inputMap.containsKey(HEADERSTYLE))
             headerCssStyle = (string)inputMap.get(HEADERSTYLE);
        if(inputMap.containsKey(TBLHEADERSTYLE))
             tableHeaderStyle = (string)inputMap.get(TBLHEADERSTYLE); 
        if(inputMap.containsKey('textAllign'))     
             headerTextAllignMap =  (map<string,string>)inputMap.get('textAllign');
        if(inputMap.containsKey(TBLROWSTYLE))
            sRowStyle =  (string)inputMap.get(TBLROWSTYLE); 
        headerCssStyle=headerCssStyle+tableHeaderStyle;
        String content = '';
        if(dataMap.size()>0 && !dataMap.isEmpty()) {
            content = '<p style=\"'+tableCssStyle+'\"> <table style=\"'+tableCssStyle+'\">';
            content +='<thead><tr>';
            integer colCount = 0;       
            for(String key: headerMap.keySet()){  
                string tAllign = 'text-align:center;';
                if(colCount == 0){
                    tAllign = 'text-align:left;';
                    colCount ++;
                }                
                content +='<th style=\"'+headerCssStyle+ tAllign + 'font-weight: bold;color:#ffffff;\">';
                system.debug('Header style' + content +'***'+ tAllign);
                content +='<viawrapper>'+key+'</viawrapper></th>';
            }
            content +='</tr></thead>';
            integer rowcount = 0;
            String tableCellStyle='font-size:10pt;font-family:Arial,sans-serif;border: 1px solid black;border-right: 1px solid black;border-bottom: 1px solid black;';
            String bodyStyle='<td style=\"'+tableCellStyle;
            content +='<tbody>';
            for(String key : dataMap.keySet()){
                if(groupByFieldName != 'Na' && key != null){
                    content+='<tr>';
                    content+=bodyStyle+'font-weight:bold;\">';
                    content+='<viawrapper>'+key+'</viawrapper></td>';
                   for (integer i=1; i<headerMap.size();i++ ){
                        content+=bodyStyle+'\">';
                        content+= '</td>';
                    }
                  content+='</tr>';
                }
                List<Map<String, String>> rowData = dataMap.get(key);
                
                for(Map<String, String> eachRow : rowData){
                    content+='<tr>';
                    boolean isGRouping=false;
                    boolean isTotal=false;
                    for(String header : headerMap.keySet()){
                        string sFormattedIdentifier='';
                        string textAllign = '';
                        if(headerTextAllignMap.containsKey(header) )
                           textAllign = headerTextAllignMap.get(header);
                        if(formatcolumnmap.containsKey(header)){
                            sFormattedIdentifier=formatcolumnmap.get(header);
                        }
                       
                        if(rowcount == 0  ){
                            content+=bodyStyle+ sRowStyle +'padding-left: 10px;font-weight:bold' + ';text-align:' + (textAllign != '' ? textAllign : 'left') +'\">';
                            content+='<viawrapper>'+ sFormattedIdentifier +'' +(eachRow.get(header) != null ? string.valueof(eachRow.get(header)): '') +'</viawrapper></td>'; 
                                
                        }
                        else if(((string.valueof(eachRow.get(header)) == 'Total One Time Charge'||string.valueof(eachRow.get(header)) == 'Total Monthly Charge' ) || isTotal == true )){
                            content+=bodyStyle+'background-color:#49166D;color:#ffffff;padding-left: 10px;font-weight:bold' + ';text-align:' + (textAllign != '' ? textAllign : 'left') +'\">';
                            content+='<viawrapper>'+ sFormattedIdentifier +'' +(eachRow.get(header) != null ? string.valueof(eachRow.get(header)): '') +'</viawrapper></td>'; 
                            isTotal=true; 
                        }
                        else if(string.valueof(eachRow.get(header)) == 'Business Tool'){
                            content+=bodyStyle+'font-weight:bold;' + 'padding-left: 10px;text-align:' + (textAllign != '' ? textAllign : 'left') +'\">';
                            content+='<viawrapper>' +(eachRow.get(header) ) +'</viawrapper></td>'; 
                            
                        }
                       else if(eachRow.get(header) == null){
                            content+=bodyStyle+' padding-left: 60px;font-weight:bold;\">';
                            content+='<viawrapper>'+'</viawrapper></td>';
                        }else if(rowcount != 0){
                            content+=bodyStyle+' padding-left: 10px;font-weight:'+(isTotal ? 'bold':'normal')+';text-align:' + (textAllign != '' ? textAllign : (isGRouping ? ( sFormattedIdentifier == '' ? 'left' : 'right') :(key =='Na' ? 'left' : 'right')))+'\">';
                            content+='<viawrapper>'+ sFormattedIdentifier +'' +eachRow.get(header)+'</viawrapper></td>';
                        }
                        isGRouping=true;
                        
                    }
                    rowcount++;
                    content+='</tr>';
                }
            }
            
            content +='</tbody></table></p>';
        }
        System.debug(' content is '+content);
        outMap.put('sectionContent', content);
        return outMap;
    }
    
    public static Map<String, List<Map<String, String>>> prepareData (List<SObject> items, string groupByName, map<string, string> headerMap, map<string,object>preparedataInputMap){
        String nameSpaceprefix = 'vlocity_cmt__';
        Map<String,string> formatColumnMap = new  Map<String,string> ();
        map<id,Decimal> cliLevelMap = new Map<id,decimal> ();
        decimal rowPadding = 0.00;
       
         if(preparedataInputMap.containsKey(FORMATCOLUMN) ){
            formatColumnMap = (map<string,string> )preparedataInputMap.get(FORMATCOLUMN);
        }

        
        Map<String, List<Map<String, String>>> statusMap = new Map<String, List<Map<String, String>>> ();
        for(SObject item : items){   
            string status ;
           
            //String status = (String) item.get(nameSpaceprefix+'Status__c');
            if(groupByName == 'Na')
                status = groupByName;
            else 
                status = (String) item.get(groupByName);
            
            if(statusMap.get(status) !=null){
                
                Map<String, String> newdata = new Map<String, String> ();
                for( string header: headerMap.keySet()){
                    if(cliLevelMap.size()>0 && !cliLevelMap.isEmpty() && header.equalsIgnoreCase('TELUS Business Anywhere Bundle Includes:')   ){
                        rowPadding = cliLevelMap.get((id)item.get('id') );
                            }
                    
                    String[] relationalObjs = headerMap.get(header).split('\\.');
                    string feildName;
                    string relationalObjName;
                    if(relationalObjs.size() == 2 ){
                        relationalObjName = relationalObjs[0];
                        feildName = relationalObjs[1];
                        if(!formatColumnMap.containsKey(header))
                            newdata.put(header, String.valueOf(item.getsObject(relationalObjName).get(feildName)));
                        else if(item.getsObject(relationalObjName).get(feildName) != null && formatColumnMap.containsKey(header))
                            newdata.put(header, String.valueOf(((decimal)item.getsObject(relationalObjName).get(feildName)).format()));

                    }
                    else if(relationalObjs.size()>0 && !relationalObjs.isEmpty() ) {
                        feildName = relationalObjs[0];
                        if(!formatColumnMap.containsKey(header))
                            newdata.put(header, String.valueOf(item.get(feildName)));
                        else if(item.get(feildName) != null && formatColumnMap.containsKey(header)) 
                            newdata.put(header, String.valueOf(((decimal)item.get(feildName)).format()));
                    }
                    
                }
                 statusMap.get(status).add(newdata);
            }
            else {
                List<Map<String, String>> itemList = new List<Map<String, String>> ();
                Map<String, String> newdata = new Map<String, String> ();
                for( string header: headerMap.keySet()){
                    String[] relationalObjs = headerMap.get(header).split('\\.');
                    string feildName;
                    string relationalObjName;
                    if(relationalObjs.size() == 2 ){
                        relationalObjName = relationalObjs[0];
                        feildName = relationalObjs[1];
                         if(!formatColumnMap.containsKey(header))
                            newdata.put(header, String.valueOf(item.getsObject(relationalObjName).get(feildName)));
                        else if(item.getsObject(relationalObjName).get(feildName) != null && formatColumnMap.containsKey(header))
                            newdata.put(header, String.valueOf(((decimal)item.getsObject(relationalObjName).get(feildName)).format()));
                    }
                    else if(relationalObjs.size()>0 && !relationalObjs.isEmpty() ) {
                        feildName = relationalObjs[0];
                        if(!formatColumnMap.containsKey(header))
                            newdata.put(header, String.valueOf(item.get(feildName)));
                        else if(item.get(feildName) != null && formatColumnMap.containsKey(header)) 
                           newdata.put(header, String.valueOf(((decimal)item.get(feildName)).format())); 
                    }
                }
                itemList.add(newdata);
                statusMap.put(status, itemList);
            }

                
        }
        System.debug('Status Map '+statusMap);
        return statusMap;
    }
    
    public static string addDataCells(Map<String,string> headerMap){
         String tableCellStyle='font-size:9pt;font-family:Arial,sans-serif;border: 1px solid black;border-right: 1px solid black;border-bottom: 1px solid black;';
            String bodyStyle='<td style=\"'+tableCellStyle;
        string dataCells = '';
        for(integer i=0; i<headerMap.size();i++ ){
            dataCells += bodyStyle + +'\">' + '</td>';
        }
        return dataCells;    
    } 
    
    private static List<hierarchyItem> getChildHierarchy(Id parentId , List<hierarchyItem> currentHierarchyItemList, string cliName){  
        hirarchyIndex = hirarchyIndex + 1;  
        for(vlocity_cmt__ContractLineItem__c objCLI : mapHierarchy.values()){  
            if(objCLI.Parent_Contract_Line_Item__c == parentId) {                 
                hierarchyItemList.add(new hierarchyItem( objCLI , hirarchyIndex, cliName));  
                mapHierarchy.remove(objCLI.id);                  
                //Get child records of child  
                hierarchyItemList = getChildHierarchy(objCLI.id , hierarchyItemList,cliName);  
            }  
            
            
        }  
        hirarchyIndex = hirarchyIndex - 1;      
        return currentHierarchyItemList;      
    } 
    public class hierarchyItem{  
        public Sobject objSobject{get;set;}   
        public integer hirarchyIndexNo{get;set;}  
        public hierarchyItem(Sobject objSobject , Integer hirarchyIndexNo,string fieldname){  
            String spaceCount = '';  
            for(integer i = 0 ; i < hirarchyIndexNo ; i++)  
                spaceCount = spaceCount  + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';  
            this.hirarchyIndexNo=hirarchyIndexNo;
            objSobject.put(fieldname,spaceCount + objSobject.get(fieldname)) ;     
            this.objSobject = objSobject;  
        }  
    }
   
}