public class ActivateOrderController_vlocity_cmt {
    public Contract contractObj           {get; set;}   
    public List<AttachmentWrapper> attachmentWrapperList {get;set;}
    public Id contractId;
    public boolean isValidateSuccess    {get; set;}
    public ActivateOrderController_vlocity_cmt(ApexPages.StandardController objController)
    {
        contractId =  ApexPages.currentPage().getParameters().get('id');
        if(contractId  != null)
        {
            contractObj = [Select Id, Name, Status, vlocity_cmt__OpportunityId__c, vlocity_cmt__OpportunityId__r.Id,vlocity_cmt__OrderId__c,vlocity_cmt__OrderId__r.id
                           From Contract where Id =:contractId ];
            if(contractObj != null)
            {
                if(attachmentWrapperList == null) 
                {
                    Set<id> contractVersionIds = new Set<Id>();
                     List<vlocity_cmt__ContractVersion__c> contractVersionList =  new List<vlocity_cmt__ContractVersion__c>();
                     contractVersionList = [Select Id, vlocity_cmt__ContractId__c From vlocity_cmt__ContractVersion__c 
                                       where vlocity_cmt__ContractId__c =:contractObj.Id ];
                    
                    System.debug('@@contractVersionListTes'+contractVersionList);
                    for (vlocity_cmt__ContractVersion__c contractVersionLoopVar : contractVersionList)
                    {
                        contractVersionIds.add(contractVersionLoopVar.Id);
                    }
                        
                    attachmentWrapperList = new List<AttachmentWrapper>();
                    
                    for(Attachment attachmentLoopVar: [Select Id, Name, Body,ParentId, Parent.Name, Owner.Name, LastModifiedDate from Attachment where ParentId =: contractId]) 
                    {
                        attachmentWrapperList.add(new AttachmentWrapper(attachmentLoopVar,'Contract'));
                    }
                    
                    for(Attachment attachmentLoopVar: [Select Id, Name, Body,ParentId, Parent.Name, Owner.Name, LastModifiedDate from Attachment where ParentId IN: contractVersionIds]) 
                    {
                        attachmentWrapperList.add(new AttachmentWrapper(attachmentLoopVar,'Contract Version'));
                    }
                    
                    
                }
                if(attachmentWrapperList.Size()> 0)
                {
                    isValidateSuccess=  true;
                }
                else
                {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'No Attachment found !');
                    ApexPages.addMessage(myMsg);
                    isValidateSuccess=  false;

                }
            }
        }
    }
    public PageReference activateContract()
    {
       
       
            try
            {
                boolean isAttachmentContract = false;
                boolean isAttachmentContractVersion = false;
                /*Opportunity opportunityObj = new Opportunity();
                opportunityObj = [Select Id, Name,stage_update_time__c, StageName from Opportunity where Id =: contractObj.vlocity_cmt__OpportunityId__r.Id ];*/
                
                List<Attachment> attachmentList = new List<Attachment>();
                                List<Attachment> attachmentListContent = new List<Attachment>();

                for(AttachmentWrapper attrVar: attachmentWrapperList)
                {
                    if(attrVar.selectedVar == true &&  attrVar.attachementType == 'Contract' )
                    {
                        attachmentList.add(attrVar.attachmentVar);   
                        isAttachmentContract = true;
                    }
                    else
                    {   if(attrVar.selectedVar == true &&  attrVar.attachementType == 'Contract Version')
                        attachmentListContent.add(attrVar.attachmentVar);
                        isAttachmentContractVersion = true; 
                    }
                     
                    
                    
                }
                
                if(attachmentList.size() == 0 && attachmentListContent.size() == 0)
                {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select atleast one attachment !');
                ApexPages.addMessage(myMsg);
                isValidateSuccess = true; 
                return null; 
                }
                 if(attachmentList.size() + attachmentListContent.size()  >1)
                {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select only one attachment !');
                ApexPages.addMessage(myMsg);
                isValidateSuccess = true; 
                return null; 
                }
                System.debug('@@attachmentList'+attachmentList);
                List<vlocity_cmt__ContractVersion__c> contractVersionList =  new List<vlocity_cmt__ContractVersion__c>();
                contractVersionList = [Select Id, vlocity_cmt__ContractId__c From vlocity_cmt__ContractVersion__c 
                                       where vlocity_cmt__ContractId__c =:contractObj.Id ];
                //Update Opportunity
                /* opportunityObj.StageName = 'Order Request New';
                update opportunityObj;*/
                //Update Contract 
                
                System.debug('@@Check');
                List<Attachment> attachmentContractVersionList = new List<Attachment>();
                for(Attachment att : attachmentList)
                {
                    Attachment attObj = new Attachment();
                    attObj.Name   = 'Final_Electronic_Copy_'+att.Name;
                    attObj.Body   = att.Body;
                    attObj.ParentId = contractVersionList[0].Id;
                    attachmentContractVersionList.add(attObj);
                }
                                System.debug('@@attachmentContractVersionList'+attachmentContractVersionList.size());
                if(isAttachmentContract)
                insert attachmentContractVersionList;
                if(isAttachmentContractVersion)
                {
                 List<Attachment> contractDocumentList = new List<Attachment>(); 
                 for(Attachment att : attachmentListContent)
                {
                    att.Name = 'Final_Electronic_Copy_'+att.Name;
                    contractDocumentList.add(att);
                }
                    update contractDocumentList;
                    
                }
                
                contractObj.Status = 'Contract Registered';
                update contractObj;
                // Call Net Cracker
                //Web service call to send full order to Netcracker directly
                    //smb_OrderSubmit_Callout_Helper.SubmitOrderRequest(opportunityObj.Id);
                //Update Opportunity
                
                //opportunityObj.StageName = 'Order Request New';
                //opportunityObj.stage_update_time__c=system.now().addseconds((math.random()>0.5?1:2));
               // update opportunityObj;
                
                
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info, 'Contract Activated Successfully');
                ApexPages.addMessage(myMsg);
                isValidateSuccess = false;
                return null;
            }catch(Exception e)
            {
                system.debug('@@'+e.getMessage());
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
                ApexPages.addMessage(myMsg);
                isValidateSuccess = false; 
                return null;
            }
        
    }
    public class AttachmentWrapper
    {
        public Attachment attachmentVar {get; set;}
        public Boolean selectedVar {get; set;}
         public String attachementType {get; set;}
        //This is the contructor method. When we create a new Contact object we pass a Contact that is set to the con property. We also set the selected value to false
        public AttachmentWrapper(Attachment attachmentParam) 
        {
            attachmentVar = attachmentParam;
            selectedVar = false;
        }
        
          public AttachmentWrapper(Attachment attachmentParam, String attachementTypeParam) 
        {
            attachmentVar = attachmentParam;
            selectedVar = false;
            attachementType = attachementTypeParam;
        }
    }
}