@isTest(SeeAllData=false)
public class ENTPNavCtlr_test {
 @isTest(SeeAllData=false) static void NavController() {
        ENTP_portal_additional_functions__c paf = new ENTP_portal_additional_functions__c(Name = 'tps',Customer_Portal_Role__c = 'tps',Additional_Function__c = 'Severity 1');
        insert paf;
        User u = MBRTestUtils.createENTPPortalUser('test-user-1', false, 'en_US');        
        Test.setCurrentPage(Page.ENTPUserTemplate);
        System.runAs(u) {
            ENTPNavCtlr ctlr = new ENTPNavCtlr(); 
            ctlr.combinedStrings = null;
            String[] sResult = ctlr.strings;
            ctlr.combinedStrings = 'a, b';
            sResult = ctlr.strings;
            
            String userAccountId = [SELECT AccountId FROM User WHERE Id = :UserInfo.getUserId()].AccountId;
            system.assert(String.isNotEmpty(userAccountId));
            List<account> Accounts = [SELECT Strategic__c, ID from Account where ID =:userAccountId];
            String userLanguage = [SELECT LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()].LanguageLocaleKey;
            system.assert(String.isNotEmpty(userLanguage));
            String CustRole = 'tps';
            String SFDCAccountId='test';
            System.assertEquals( SFDCAccountId, 'test');
            String CatListPull1='test';
            System.assertEquals( CatListPull1, 'test');
/*            
            List<ENTP_portal_additional_functions__c> ENTPPortalMap = [Select Name,Customer_Portal_Role__c,Additional_Function__c from ENTP_portal_additional_functions__c where Customer_Portal_Role__c =: CustRole];
            system.assert(String.isNotEmpty(ENTPPortalMap[0].Additional_Function__c));
            String AddFuncPull1 = 'test';
            System.assertEquals( AddFuncPull1, 'test');
            string CatListPull = ENTPPortalMap[0].Additional_Function__c;
            System.assertEquals( CatListPull, CatListPull);
            List<User> users = [SELECT Id, AccountId, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()];
            system.assert(String.isNotEmpty(users.get(0).AccountId));
*/
        }    
    }
}