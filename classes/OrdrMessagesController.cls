/*
* -------------------------------------------------------------------------------------------------
* This class retruns the order status and message indicating if the SubmitOrder integration is successful. 
* 
* -------------------------------------------------------------------------------------------------
* @author         Santosh K Rath   
* @modifiedBy     Santosh K Rath   
* @version        1.0
* @created        2015-11-01
* @modified       YYYY-MM-DD
* --------------------------------------------------------------------------------------------------
* @changes
* vX.X            Santosh K Rath
* YYYY-MM-DD      Explanation of the change.  
*
* vX.X            Santosh K Rath
* YYYY-MM-DD      
* -------------------------------------------------------------------------------------------------
*/

public class OrdrMessagesController {
        public String msgKeyVal{get;set;}
    public String orderId;
    public  String message{get;set;}
    public  String inputParam;
    public Order orderObj;
    public Boolean isOrderLocked { get; set; }
    public Boolean isOrderAmendReady { get; set; }
    public Integer countHit { get; set; }
    public Boolean cacheFlag{ get; set; }
    public OrdrMessagesController(ApexPages.StandardController controller) {
        this.orderObj=(Order)controller.getRecord();
        this.cacheFlag=false;
        readMessageUtil();
        String orderId=orderObj.id;
        String msgKey=orderId.substring(0,15)+'msg';
        countHit=0;       
    }
    public void  readMessage() {
    	 this.cacheFlag=true;
        readMessageUtil();
        String orderId=ApexPages.currentPage().getParameters().get('id');
        String msgKey=orderId.substring(0,15)+'msg';
        //if(!Test.isRunningTest() && countHit>3){
            //Cache.Session.remove(msgKey);
           // Cache.Session.remove(orderId.substring(0,15)+'submitflow');
            /* Cache.Session.remove(msgKey+'externalOrdNo');
				Cache.Session.remove('timeTakenTocreateAssets');
				Cache.Session.remove('timeTakenToSubmitOrder');
				Cache.Session.remove(orderId.substring(0,15)+'submitflow');*/
       // }
        countHit++;
    }
    public void  readMessageUtil() {
        Boolean showMessages=false;
        String extOrderStatus='';
        String orderStatusMsg='';
        String orderStatus='';
        String orderSubmissionStatus='';
        String ncSalesOrderKey='';
        String orderId=ApexPages.currentPage().getParameters().get('id');
         if(String.isBlank(orderId)){
             return;
         }
        if(String.isNotBlank(orderId)){
            List<Order> orderRecordList = [Select id, orderMgmtId__c,orderMgmtId_Status__c,status,vlocity_cmt__ValidationMessage__c,vlocity_cmt__ValidationStatus__c,vlocity_cmt__ValidationDate__c From order Where Id = :orderId];
            if(orderRecordList.get(0)!=null){
                orderStatus=orderRecordList.get(0).status;
                orderSubmissionStatus=orderRecordList.get(0).vlocity_cmt__ValidationStatus__c;
                ncSalesOrderKey=orderRecordList.get(0).orderMgmtId__c;
                extOrderStatus=orderRecordList.get(0).orderMgmtId_Status__c;
            }
       }
         if(orderStatus.equals('Submitted')|| orderStatus.equals('Cancelled') || orderStatus.equals('Activated')){
            this.isOrderLocked=true;
        } else {
            this.isOrderLocked=false;
        }
        String msgKey=orderId.substring(0,15)+'msg';
        String ncOrderSubmitStatus='';
        if(Test.isRunningTest()){
            ncOrderSubmitStatus=msgKeyVal;
        } else {
            ncOrderSubmitStatus=String.valueOf(Cache.Session.get(msgKey));
        }
        System.debug('test  ncOrderSubmitStatus='+ncOrderSubmitStatus);
        if((Test.isRunningTest()) || (String.isNotBlank(String.valueOf(Cache.Session.get(orderId.substring(0,15)+'submitflow'))))){
            showMessages=true;
        }
        if(String.isNotBlank(ncOrderSubmitStatus) && showMessages){
            if(ncOrderSubmitStatus.equals('Fail')) {
                orderStatusMsg='{"OrderStatus":"'+orderStatus+'","status":"Fail","msg":"Create Assets Failed"}';
                this.message=orderStatusMsg;
            	return;
            }
            if(ncOrderSubmitStatus.equals('Warn')) {
                String warnMsg='';
                if(!Test.isRunningTest()){
                    warnMsg=String.valueOf(Cache.Session.get('WarnMsg'));
                }
                orderStatusMsg='{"OrderStatus":"'+orderStatus+'","status":"Warn","msg":"'+warnMsg+'"}';
                this.message=orderStatusMsg;
            	return;
            }
            if(ncOrderSubmitStatus.equals('Success')) {
                orderStatusMsg='{"OrderStatus":"'+orderStatus+'","status":"Success","msg":"Create Assets in Progress"}';
                this.message=orderStatusMsg;
            	return;
            }
             if(ncOrderSubmitStatus.equals('InProgress') && !(String.isNotBlank(orderSubmissionStatus) && (orderSubmissionStatus.equals('Success') ||orderSubmissionStatus.equals('SubmitFailed'))) ){
            	orderStatusMsg='{"OrderStatus":"'+orderStatus+'","status":"InProgress","msg":"OrderSubmit in Progress"}';
            	this.message=orderStatusMsg;
            	return;
        	}
            
        }
        
       
        if(showMessages && String.isNotBlank(orderSubmissionStatus) && orderSubmissionStatus.equals('SubmitFailed')){
            orderStatusMsg='{"OrderStatus":"'+orderStatus+'","status":"Fail","msg":"'+System.Label.Order_Submission_Failed+'"}';
            this.message=orderStatusMsg;
            if(!Test.isRunningTest() && (this.cacheFlag)){
            	Cache.Session.remove(orderId.substring(0,15)+'submitflow'); 
            } 
            return;
        }
       /* if(showMessages && String.isNotBlank(orderSubmissionStatus) && orderSubmissionStatus.equals('InProgress')){
           orderStatusMsg='{"OrderStatus":"'+orderStatus+'","status":"InProgress","msg":"OrderSubmit in Progress"}';
            this.message=orderStatusMsg;                      
            return;
        }*/
        if(showMessages && String.isNotBlank(orderSubmissionStatus) && orderSubmissionStatus.equals('Success')){
            orderStatusMsg='{"OrderStatus":"'+orderStatus+'","status":"Success","msg":"'+System.Label.Order_Submitted_Successfully+' \n OrderNo '+ncSalesOrderKey+'"}';
            this.message=orderStatusMsg;
            if(!Test.isRunningTest() && (this.cacheFlag)){
            //Cache.Session.remove(msgKey);
            	Cache.Session.remove(orderId.substring(0,15)+'submitflow');            
        	}
            return;
        }
        this.isOrderAmendReady=false;
       // if(!isOrderAmendReady){
            List<OrderItem> orderItemObjList=[select orderMgmtId__c from orderitem where orderid=:orderId ];
            for(OrderItem orderItemObj :orderItemObjList){
                if(String.isNotBlank(orderItemObj.orderMgmtId__c)){
                    this.isOrderAmendReady=true;
                    break;
                }
            }
       // }
       if(isOrderAmendReady){
        if(String.isNotBlank(ncSalesOrderKey) && !orderStatus.equals('Cancelled') && (String.isNotBlank(extOrderStatus) && (extOrderStatus.equals('Submitting') ||extOrderStatus.equals('Processing') ||extOrderStatus.equals('Error')))){
            this.isOrderAmendReady=true;
        } else {
        	this.isOrderAmendReady=false;
        }
       }
        
        if(!Test.isRunningTest() && isOrderAmendReady && (this.cacheFlag)){
            //Cache.Session.remove(msgKey);
            Cache.Session.remove(orderId.substring(0,15)+'submitflow');            
        }
        orderStatusMsg='{"OrderStatus":"'+orderStatus+'","status":"NA","msg":"None","isAmendReady":"'+this.isOrderAmendReady+'"}';
        this.message=orderStatusMsg;
    }
    public String getOrderId(){
        return ApexPages.currentPage().getParameters().get('id');
    }
    public void setInputParam(String inMsg){
        this.inputParam=inMsg;
    }
    public String getInputParam(){
        String orderId=ApexPages.currentPage().getParameters().get('id');
        if(String.isNotBlank(orderId)){
            inputParam=orderId;
        }
        return inputParam;
    }
}