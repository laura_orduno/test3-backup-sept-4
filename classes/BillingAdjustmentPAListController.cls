public class BillingAdjustmentPAListController{
    public List<Billing_Adjustment__c> baList {get;set;}
    public Billing_Adjustment__c ba {get;set;}
    
    public BillingAdjustmentPAListController(ApexPages.StandardController stdController){
        this.ba = [select Id, Parent_Account_id__c from Billing_Adjustment__c where id = :stdController.getRecord().id];
        String pid = ba.Parent_Account_id__c;
        baList = [select Id, Name, Parent_Account_id__c, Account__c,  Account__r.Name, Account__r.BAN_CAN__c, Transaction_Type__c, Status__C,
                         Total_Amount__c, CreatedDate, CreatedBy.Name, Closed_Status__C, Billing_Total_Amount__c
                    from Billing_Adjustment__c 
                    where Parent_Account_id__c = :pid order by Name desc];
       
    }
    
}