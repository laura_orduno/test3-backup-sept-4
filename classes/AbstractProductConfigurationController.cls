public abstract class AbstractProductConfigurationController {
	// Properties set via AJAX requests that keep track of what's being configured.
	public Id selectedProductId {get; set;}
	public String selectedOptionId {get; set;}
	public String selectedFeatureId {get; set;}
	public String optionSelectionId {get; set;}
	public String customConfigurationPagePrefix {get; set;}
	
	// Selections and other persistent state of root products.
	public List<ProductOptionSelectionModel> configuredOptionSelections {get; private set;}
	// Selections and other persistent state of currently configured option (lower levels)
	public ProductOptionSelectionModel.Selection configuredOptionSelection {get; private set;}
	
	public transient ProductModel configuredProduct {get; private set;}
	public transient ProductFeatureModel configuredFeature {get; private set;}
	
	public List<String> productOptionFields {get{return MetaDataUtils.getFieldNames('ProductOption__c');} private set;}
	public String encodedAdditionalInfo {get; set;}
	public String quoteId {get; set;}
	public Map<String,String> processVariablesByName {get; set;}
	
	public transient String lastAjaxError {get; protected set;}
	public transient Boolean lastAjaxResult {get; protected set;}
	public transient Boolean overlayErrorOnly {get; protected set;}
	
	public Integer selectedCustomButtonIndex {get; set;}
	public CustomButton[] customButtons {get; private set;}
	
	public Id accountId {get; set;}
	public Id pricebookId {get; set;}
	public String currencyCode {get; set;}
	
	protected QuoteVO quote;
	
	private Map<Id,ProductOptionSelectionModel> selectionModelsByProductId;
	
	public AbstractProductConfigurationController() {
		selectionModelsByProductId = new Map<Id,ProductOptionSelectionModel>();
		configuredOptionSelections = new List<ProductOptionSelectionModel>();
		initCustomButtons();
	}
	
	protected void init(QuoteVO quote) {
		quoteId = quote.quoteId;
		pricebookId = quote.getPricebookId();
		accountId = quote.getAccountId();
		if (UserInfo.isMultiCurrencyOrganization()) {
			currencyCode = (String)quote.getCurrencyCode();
		}
	}
	
	/**
	 * Returns option selection model for specified product ID.
	 */
	public ProductOptionSelectionModel getSelectionModelByProductId(Id productId) {
		return selectionModelsByProductId.get(productId);
	}
	
	public Boolean validateProductConfigurations(Map<Id,ProductModel> products) {
		//restoreConfiguredProducts();
		
		Boolean valid = true;
		List<String> errors = new List<String>();
		for (ProductOptionSelectionModel smodel : selectionModelsByProductId.values()) {
			smodel.restoreProduct(products.get(smodel.productId), true);
			Boolean pvalid =  smodel.validate();
			if (!pvalid && (smodel.validationMessages != null)) {
				errors.addAll(smodel.validationMessages);
			}
			valid = valid && pvalid;
		}
		
		if (!valid) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid configuration. Please check your selections and try again.'));
			for (String err : errors) {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, err));
			}
			return false;
		}
		return true;
	}
	
	/**
	 * Deep loads configured products with selected and required options.
	 */
	public ProductModel[] loadConfiguredProducts() {
		return loadConfiguredProducts(selectionModelsByProductId);
	}
	
	public ProductModel[] loadAutoConfiguredProducts(ProductModel[] products) {
		Map<Id,ProductOptionSelectionModel> smodels = new Map<Id,ProductOptionSelectionModel>();
		for (ProductModel pmodel : products) {
			smodels.put(pmodel.id, new ProductOptionSelectionModel(pmodel));
		}
		return loadConfiguredProducts(smodels);
	}
	
	public ProductModel[] loadConfiguredProducts(Map<Id,ProductOptionSelectionModel> smodels) {
		// Build the set of all options selected in all configured products.
		Set<Id> optionIds = new Set<Id>();
		for (ProductOptionSelectionModel smodel : smodels.values()) {
			optionIds.addAll(smodel.getSelectedOptionIds(true));
		}
		// Now deep load configured products and selected options 
		List<ProductVO> products = 
			ProductService.getInstance().findProductsWithSelectedOptionsAndPrices(
					smodels.keySet(), optionIds, pricebookId, currencyCode).values();
		ProductModel[] configuredProducts = new List<ProductModel>();
		for (ProductVO product : products) {
			ProductModel pmodel = new ProductModel(product);
			ProductOptionSelectionModel smodel = smodels.get(pmodel.id);
			pmodel.setSelections(smodel);
			smodel.restoreProduct(pmodel);
			configuredProducts.add(pmodel);
		}
		return configuredProducts;
	}
	
	public void configureProducts(ProductModel[] pmodels) {
		initState();
		for (ProductModel pmodel : pmodels) {
			ProductOptionSelectionModel smodel = new ProductOptionSelectionModel(pmodel);
			registerSelectionModel(smodel);
			pmodel.setSelections(smodel);
		}
		generateProvisioningSummaries();
	}
	
	public void configureQuoteLineRow(QuoteVO.LineItem item) {
		initState();
		ProductModel pmodel = ProductModel.loadWithOptionPrices(item.productId, accountId, pricebookId, currencyCode);
		ProductOptionSelectionModel smodel = new ProductOptionSelectionModel(item);
		registerSelectionModel(smodel);
		if (item.isPackage()) {
			selectComponents(smodel, item);
		}
		smodel.restoreProduct(pmodel);
		generateProvisioningSummaries();
	}
	
	private void initState() {
		selectedProductId = null;
		selectedFeatureId = null;
		selectedOptionId = null;
		optionSelectionId = null;
		configuredOptionSelections.clear();
		selectionModelsByProductId.clear();
	}
	
	private void selectComponents(ProductOptionSelectionModel parent, QuoteVO.LineItem item) {
		for (QuoteVO.LineItem component : item.getComponents()) {
			// Add selection for component row (selecting the appropriate option).
			ProductOptionSelectionModel.Selection sel = parent.addSelection(component);
			if (component.isPackage()) {
				selectComponents(sel.getNestedSelections(), component);
			}
		}
	}
	
	public PageReference onLookupOptions() {
		try {
			overlayErrorOnly = true;
			assertNotBlank(selectedProductId, 'onLookupOption: Missing selectedProductId');
			assertNotBlank(selectedFeatureId, 'onLookupOption: Missing selectedFeatureId');
			configuredOptionSelection = findSelectionById(optionSelectionId);
			configuredProduct = ProductModel.loadWithOptionPrices(selectedProductId, accountId, pricebookId, currencyCode);
			assertNotBlank(configuredProduct, 'onLookupOption: Unrecognized product: ' + selectedProductId);
			configuredFeature = configuredProduct.getFeatureByKey(selectedFeatureId);
			assertNotBlank(configuredFeature, 'onLookupOption: Unrecognized feature: ' + selectedFeatureId);
		} catch (Exception e) {
			ApexPages.addMessages(e);
		}
		return null;
	}
	
	public PageReference onConfigureOption() {
		lastAjaxResult = true;
		overlayErrorOnly = true;
		try {
			overlayErrorOnly = true;
			assertNotBlank(optionSelectionId, 'onConfigureOption: Missing selection');
			configuredOptionSelection = findSelectionById(optionSelectionId);
			assertNotBlank(configuredOptionSelection, 'onConfigureOption: Unrecognized selection: ' + optionSelectionId);
			ProductOptionSelectionModel activeSelections = configuredOptionSelection.getNestedSelections();
			Id productId = activeSelections.productId;
			configuredProduct = ProductModel.loadWithOptionPrices(productId, accountId, pricebookId, currencyCode);
			assertNotBlank(configuredProduct, 'onConfigureOption: Unrecognized product: ' + activeSelections.productId);
			activeSelections.restoreProduct(configuredProduct);
		} catch (Exception e) {
			ApexPages.addMessages(e);
			lastAjaxResult = false;
		}
		return null;
	}
	
	
	public PageReference onSaveOptionConfiguration() {
		lastAjaxResult = true;
		overlayErrorOnly = true;
		try {
			restoreConfiguredProducts();
			assertNotBlank(selectedProductId, 'onSaveOptionConfiguration: Missing selected product');
			assertNotBlank(optionSelectionId, 'onSaveOptionConfiguration: Missing selection');
			configuredOptionSelection = findSelectionById(optionSelectionId);
			assertNotBlank(configuredOptionSelection, 'onSaveOptionConfiguration: Unrecognized selection: ' + optionSelectionId);
			ProductOptionSelectionModel activeSelections = configuredOptionSelection.getNestedSelections();
			assertNotBlank(activeSelections, 'onSaveOptionConfiguration: Missing configured selections');
			configuredProduct = ProductModel.loadById(activeSelections.productId, accountId, pricebookId, currencyCode);
			assertNotBlank(configuredProduct, 'onSaveOptionConfiguration: Unknown product: ' + activeSelections.productId);
			
			QuoteUtils.copyAdditionalInformation(activeSelections.getConfigurationData(), encodedAdditionalInfo);
			
			applyPriceRules(configuredOptionSelection);
			
			activeSelections.restoreProduct(configuredProduct);
			if (!activeSelections.validate()) {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid configuration. Please check your selections and try again.'));
				for (String err : activeSelections.validationMessages) {
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, err));
				}
				configuredOptionSelection.configured = false;
				lastAjaxResult = false;
			} else {
				configuredOptionSelection.configured = true;
				
				updateQuoteWithProduct(activeSelections);
				generateProvisioningSummaries();
			}
			
			// Make sure the currently selected feature is active on page rerender
			configuredOptionSelection.parent.activeFeatureKey = configuredOptionSelection.option.feature.getKey();
		} catch (Exception e) {
			ApexPages.addMessages(e);
			lastAjaxResult = false;
		}
		return null;
	}
	
	public PageReference onCopyCoreSelections() {
		lastAjaxResult = true;
		overlayErrorOnly = true;
		try {
			restoreConfiguredProducts();
			assertNotBlank(selectedProductId, 'onSaveOptionConfiguration: Missing selected product');
			assertNotBlank(optionSelectionId, 'onSaveOptionConfiguration: Missing selection');
			configuredOptionSelection = findSelectionById(optionSelectionId);
			assertNotBlank(configuredOptionSelection, 'onSaveOptionConfiguration: Unrecognized selection: ' + optionSelectionId);
			ProductOptionSelectionModel activeSelections = configuredOptionSelection.getNestedSelections();
			assertNotBlank(activeSelections, 'onSaveOptionConfiguration: Missing configured selections');
			configuredProduct = ProductModel.loadById(activeSelections.productId, accountId, pricebookId, currencyCode);
			assertNotBlank(configuredProduct, 'onSaveOptionConfiguration: Unknown product: ' + activeSelections.productId);
			activeSelections.restoreProduct(configuredProduct);
			Set<Id> coreProductIds = configuredProduct.getVO().getCoreProductIds();
			ProductOptionSelectionModel bundle = configuredOptionSelections[0];
			ProductOptionSelectionModel.Selection[] sels = bundle.findSelectionsByProductIds(coreProductIds);
			if (!sels.isEmpty()) {
				ProductOptionSelectionModel.Selection core = sels[0];
				Set<Id> selectedCoreProductIds = core.getNestedSelections().getSelectedProductIds();
				for (ProductOptionSelectionModel.Selection sel : activeSelections.getSelections()) {
					if (selectedCoreProductIds.contains(sel.optionalProductId)) {
						sel.selected = true;
					}
				}
				
			}
		} catch (Exception e) {
			ApexPages.addMessages(e);
			lastAjaxResult = false;
		}
		return null;
	}
	
	public PageReference onAddOptions() {
		overlayErrorOnly = true;
		lastAjaxResult = true;
		try {
			assertNotBlank(selectedProductId, 'onAddOptions: Missing selected product');
			assertNotBlank(selectedOptionId, 'onAddOptions: Missing options to add');
			
			if (selectionModelsByProductId.containsKey(selectedProductId)) {
				restoreConfiguredProducts();
			} else {
				configuredProduct = ProductModel.loadById(selectedProductId, accountId, pricebookId, currencyCode);
				assertNotBlank(configuredProduct, 'onAddOptions: Unrecognized product: ' + selectedProductId);
			}
			
			ProductOptionSelectionModel target = getCurrentSelections();
			Set<Id> productIds = new Set<Id>();
			String[] ids = selectedOptionId.split(',');
			ProductOptionModel[] addedOptions = new List<ProductOptionModel>();
			for (String optionId : ids) {
				ProductOptionModel option = target.product.getOptionById(optionId);
				assertNotBlank(option, 'onAddOptions: Unrecognized option: ' + optionId);
				productIds.add(option.optionalProductId);
				addedOptions.add(option);
			}
			
			// Load optional products with prices
			Map<Id,ProductVO> productsById = 
				ProductService.getInstance().findProductsWithPricesAndCosts(productIds, pricebookId, currencyCode);
			
			for (ProductOptionModel option : addedOptions) {
				ProductVO oproduct = productsById.get(option.optionalProductId);
				assertNotBlank(oproduct, 'onAddOptions: Unrecognized product: ' + option.optionalProductId);
				option.changeOptionalProduct(oproduct);
				ProductOptionSelectionModel.Selection sel = target.addSelection(option);
				
				// Make sure the currently selected feature is active on page rerender
				target.activeFeatureKey = option.feature.getKey();
			}
			
			updateQuoteWithProduct(getCurrentSelections());
			generateProvisioningSummaries();
		} catch (Exception e) {
			ApexPages.addMessages(e);
			lastAjaxResult = false;
		}
		return null;
	}
	
	public PageReference onCloneOption() {
		lastAjaxError = 'No option found for key: ' + selectedOptionId;
		return null;
	}
	
	public PageReference onDeleteOption() {
		try {
			restoreConfiguredProducts();
			assertNotBlank(selectedProductId, 'onDeleteOption: Missing selected product');
			assertNotBlank(optionSelectionId, 'onDeleteOption: Missing options selection');
			
			configuredOptionSelection = findSelectionById(optionSelectionId);
			assertNotBlank(configuredOptionSelection, 'onDeleteOption: Unrecognized selection: ' + optionSelectionId);
			
			ProductOptionSelectionModel selections = configuredOptionSelection.parent;
			if (configuredProduct != null) {
				// Load if it hasn't been loaded by restoreConfiguredProducts call
				configuredProduct = ProductModel.loadById(selections.productId, accountId, pricebookId, currencyCode);
			}
			assertNotBlank(configuredProduct, 'onDeleteOption: Unrecognized product: ' + selectedProductId);
			selections.restoreProduct(configuredProduct);
			selections.removeSelection(optionSelectionId);
			
			updateQuoteWithProduct(selections);
		} catch (Exception e) {
			ApexPages.addMessages(e);
		} 
		return null;
	}
	
	public PageReference onCustomButton() {
		CustomButton btn = customButtons[selectedCustomButtonIndex];
		if (btn.action.equalsIgnoreCase('save')) {
			if (executeSave() == null) {
				return null;
			}
		}
		return new PageReference(btn.url);
	}
	
	public abstract PageReference executeSave();
	
	public List<ProductOptionModel> getAvailableOptions() {
		try {
			if (configuredProduct != null) {
				ProductFeatureModel feature =  configuredProduct.getFeatureByKey(selectedFeatureId);
				if (feature != null) {
					return feature.getAvailableOptions();
				}
			}
		} catch (Exception e) {
			ApexPages.addMessages(e);
		}
		return new List<ProductOptionModel>();
	}
	
	public ProductOptionSelectionModel getConfiguredSelectionModel() {
		if (configuredOptionSelection != null) {
			return configuredOptionSelection.getNestedSelections();
		}
		return null;
	}
	
	private ProductOptionSelectionModel.Selection findSelectionById(String id) {
		for (ProductOptionSelectionModel model : selectionModelsByProductId.values()) {
			ProductOptionSelectionModel.Selection sel = model.findSelectionById(id);
			if (sel != null) {
				return sel;
			}
		}
		return null;
	}
	
	private void applyPriceRules(ProductOptionSelectionModel.Selection selection) {
		// Need to produce an option record that merges existing fields with editable values (quantity & price)
		SBQQ__ProductOption__c targetOption = selection.option.mergeWithSelection(selection.editableOption);
		// TODO: Get rid of this by making configurationData and editableOption one and the same.
		QuoteUtils.mapCustomFields(targetOption, selection.getNestedSelections().getConfigurationData(), selection.getNestedSelections().getConfigurationFieldNames());
		ProductOptionPriceRuleEvaluator priceRuleEvaluator = new ProductOptionPriceRuleEvaluator(quoteId, new SBQQ__ProductOption__c[] {targetOption});
		priceRuleEvaluator.evaluate();
		selection.editableOption.SBQQ__UnitPrice__c = targetOption.SBQQ__UnitPrice__c;
		selection.editableOption.SBQQ__Discount__c = targetOption.SBQQ__Discount__c;
		selection.editableOption.SBQQ__DiscountAmount__c = targetOption.SBQQ__DiscountAmount__c;
		selection.calculate();
	}
	
	private void assertNotBlank(Object value, String message) {
		if ((value == null) || (String.valueOf(value).trim().length() == 0)) {
			throw new InvalidStateException(message);
		}
	}
	
	private void registerSelectionModel(ProductOptionSelectionModel smodel) {
		configuredOptionSelections.add(smodel);
		selectionModelsByProductId.put(smodel.productId, smodel);
	}
	
	/**
	 * Restores products and attaches them to the selection models.
	 * Product data is required for rendering but is not kept in view state
	 * to avoid blowing the limit.
	 */
	protected void restoreConfiguredProducts() {
		// Load root products
		for (ProductModel pmodel : ProductModel.loadByIds(selectionModelsByProductId.keySet(), accountId, pricebookId, currencyCode)) {
			ProductOptionSelectionModel smodel = selectionModelsByProductId.get(pmodel.id);
			smodel.restoreProduct(pmodel);
		}
		
		// If selectedProductId happens to be one of the root products
		// set configuredProduct property since we've just loaded it anyway.
		if (selectionModelsByProductId.containsKey(selectedProductId)) {
			configuredProduct = selectionModelsByProductId.get(selectedProductId).product;
		}
	}
	
	protected void generateProvisioningSummaries() {
		// Load all additional infor records for the quote
    	QueryBuilder qb = new QueryBuilder(AdditionalInformation__c.sObjectType);
        for (Schema.SObjectField field : Schema.SObjectType.AdditionalInformation__c.fields.getMap().values()) {
            qb.getSelectClause().addField(field);
        }
        qb.getWhereClause().addExpression(qb.eq(AdditionalInformation__c.Quote__c, quoteId));
        qb.getOrderByClause().addAscending(AdditionalInformation__c.CreatedDate);
        
        // Index by quote line or selection ID
        Map<String,AdditionalInformation__c[]> provisioningMap = new Map<String,AdditionalInformation__c[]>();
        for (AdditionalInformation__c ai : Database.query(qb.buildSOQL())) {
        	String key = null;
        	if (ai.Quote_Line__c != null) {
        		key = ai.Quote_Line__c;
        	} else if (ai.Selection_Id__c != null) {
        		key = ai.Selection_Id__c;
        	}
        	AdditionalInformation__c[] ais = provisioningMap.get(key);
    		if (ais == null) {
    			ais = new AdditionalInformation__c[0];
    			provisioningMap.put(key, ais);
    		}
    		ais.add(ai);
        }
        
        for (ProductOptionSelectionModel posm : configuredOptionSelections) {
        	posm.generateProvisioningSummary(provisioningMap);
        }
	}
	
	private ProductOptionSelectionModel getCurrentSelections() {
		// Try root selections first
		ProductOptionSelectionModel smodel = selectionModelsByProductId.get(selectedProductId);
		if ((smodel == null) && !StringUtils.isBlank(optionSelectionId)) {
			configuredOptionSelection = findSelectionById(optionSelectionId);
			if (configuredOptionSelection != null) {
				smodel = configuredOptionSelection.getNestedSelections();
			}
		}
		assertNotBlank(smodel, 'getCurrentSelections: Unable to compute selection model: ' + selectedProductId + ',' + optionSelectionId);
		return smodel;
	}
	
	// Need to fully load optional products for selected options
	private void updateQuoteWithProduct(ProductOptionSelectionModel smodel) {
		Id productId = smodel.product.getId();
		ProductVO configuredProductVO = 
			ProductService.getInstance().findProductsWithSelectedOptionsAndPrices(
					new Set<Id>{productId}, smodel.getSelectedOptionIds(false), pricebookId, currencyCode).get(productId);
		ProductModel configuredProduct = new ProductModel(configuredProductVO);
		configuredProduct.setSelections(smodel);
		configuredProduct.updateQuote(quote, null, false);
	}
	
	private void initCustomButtons() {
		String countStr = ApexPages.currentPage().getParameters().get('cbc');
		if (!StringUtils.isBlank(countStr)) {
			customButtons = new CustomButton[Integer.valueOf(countStr)];
			for (Integer i=0;i<customButtons.size();i++) {
				String action = ApexPages.currentPage().getParameters().get('cba' + i);
				String label = ApexPages.currentPage().getParameters().get('cbl' + i);
				String url = ApexPages.currentPage().getParameters().get('cbu' + i);
				customButtons[i] = new CustomButton();
				customButtons[i].index = i;
				customButtons[i].label = label;
				customButtons[i].url = url;
				customButtons[i].action = (action == null) ? 'save' : action;
				customButtons[i].immediate = customButtons[i].action.equalsIgnoreCase('cancel');
			}
		}
	}
	
	public virtual class CustomButton {
		public Integer index {get; private set;}
		public String label {get; private set;}
		public Boolean immediate {get; private set;}
		public String url {get; private set;}
		public String action;
	}
	
	public class InvalidStateException extends Exception {}
}