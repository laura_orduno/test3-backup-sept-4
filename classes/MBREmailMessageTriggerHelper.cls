/**
* @author = Rauza Zhenissova, TOD. 10/19/2014
*/


public with sharing class MBREmailMessageTriggerHelper {

	
    public static boolean firstRun = true;
    private static List<EmailTemplate> REPLY_TEMPLATE = [Select Id from EmailTemplate where name =  'MBR case closed comment HTML'];
    private static List<OrgWideEmailAddress> NO_REPLY = [select Id from OrgWideEmailAddress where DisplayName = 'My Business Requests'];
    private static Map<Id, User> contactToUser = new Map<Id, User>();

    public static void preventDefault(List<EmailMessage> emails){
    	for(EmailMessage em : emails){
    		if(em.TextBody != null && !em.TextBody.contains('ref:_') && em.Incoming){
    			if(em.ToAddress != null && em.ToAddress.contains('mybusinessrequest')){
	    			em.addError('Cannot create a new case. Please add Thread Id');
	    		}
    		}
    	}
    }

	public static void updateCase(Map<Id, EmailMessage> messages){
        System.debug('updateCase(): messages: ' + messages);
		List<CaseComment> commentToInsert = new List<CaseComment>{};
		Map<Id, List<EmailMessage>> caseToEmail = mapCaseToEmail(messages.values());
		Map<Id, Case> caseList = new Map<Id, Case>( [
                           SELECT
                           Id, Status, isClosed, notifyCustomer__c, ClosedDate, CaseNumber, Contact.Id, Contact.Email, CreatedBy.Email, CreatedById
                           FROM Case
                           WHERE Id IN :caseToEmail.keySet() AND Origin = :Label.MBRCCI_Self_Serve]);
		Map<Id, List<CaseComment>> caseToComments = getComments(caseList.keySet());
		contactToUser = getUsers(caseList.values());
		for(Case tmpCase : caseList.values()){
			List<EmailMessage> emailList = caseToEmail.get(tmpCase.Id);
			for(EmailMessage tmp : emailList){
				if(tmp.TextBody.contains('ref:_')){
					CaseComment tmpComment = createNewComment(tmp, tmpCase); 

					if(tmpComment != null){
						commentToInsert.add(tmpComment);
					}
					if(tmpCase.isClosed && tmpCase.notifyCustomer__c){
						sendEmail(tmp.FromAddress, tmpCase, tmpCase.Contact.Id);
					}
				}
			}
		}

		if(commentToInsert != null){
			insert commentToInsert;
		}
	}

	public static CaseComment createNewComment(EmailMessage em, Case tmpCase){
		CaseComment newComment = new CaseComment(
										IsPublished = true, 
										commentBody = '');
		newComment.parentId = tmpCase.id;
		newComment.commentBody = em.FromAddress + ' - ';
		User caseUser = contactToUser.get(tmpCase.Contact.Id);


	    if(tmpCase.Contact.Email == em.FromAddress && caseUser!=null){
	    	newComment.commentBody += caseUser.Name;
	    }
	    else{
	    	newComment.commentBody += em.FromAddress.substringBefore('@');
	    }

	    newComment.commentBody += ' ' + Label.MBREmailCommentAuthor;

	    String commentBody = getCommentBody(em, newComment.commentBody.length());

		if(commentBody == null || commentBody.equals('')){
			return null;
		}
	    
	    newComment.commentBody += commentBody;

        return newComment;
	}

	public static String getCommentBody(EmailMessage em, Integer authorName){
		Integer allowedSize = 2000 - authorName;
		Integer authorLength = 0;
		String commentBody = '';
	    
	    if(em !=null && em.TextBody!= null){
	    	List<String> emailBody = em.TextBody.split('\n');

	    	for(String str : emailBody){
				if(str.containsIgnoreCase(Label.MBRReplyAbove) || str.startsWith('From:') || str.startsWith('> On') || str.contains('-Original Message-')){
	    			break;
	    		}
	    		else if(str.length() < allowedSize){

	    			commentBody += str.trim() + '\n';
	    			allowedSize -= str.trim().length();
	    		}
	    		else if(commentBody.length() == authorLength){
	    			commentBody += str.substring(0, allowedSize).trim();
	    			break;
	    		}
	    	}
	    }
        return commentBody;
	}


	public static Map<Id,  List<CaseComment>> getComments(Set<Id> caseIds){

		Map<Id, List<CaseComment>> caseToComment = new Map<Id, List<CaseComment>>();

		List<CaseComment> commentList = [
                           SELECT
                           Id, ParentId, CreatedDate
                           FROM CaseComment
                           WHERE ParentId IN :caseIds];
        for(CaseComment comm :commentList){
        	List<CaseComment> tmpList = caseToComment.get(comm.parentId);
        	if(tmpList == null){
        		tmpList = new List<CaseComment>{comm};
        	}
        	else{
        		tmpList.add(comm);
	        }
	        caseToComment.put(comm.parentId, tmpList);
        }
        return caseToComment;
	}

	public static Map<Id,  List<EmailMessage>> mapCaseToEmail(List<EmailMessage> emails){

		Map<Id, List<EmailMessage>> caseToEmail = new Map<Id, List<EmailMessage>>();

		for(EmailMessage em : emails){
        	List<EmailMessage> tmpList = caseToEmail.get(em.parentId);
			if(em.ParentId.getSObjectType() == Case.getSObjectType() && em.Incoming && em.Subject != null){
				if(tmpList == null){
        		tmpList = new List<EmailMessage>{em};
	        	}
	        	else{
	        		tmpList.add(em);
		        }
            	caseToEmail.put(em.ParentId, tmpList);
            }
		}

        return caseToEmail;
	}

	public static Map<Id, User> getUsers(List<Case> caseList){
		Map<Id, User> ctToUser = new Map<Id, User>();
		Set<Id> contactIds = new Set<Id>{};
		if(caseList != null){
			for(Case c : caseList){
				contactIds.add(c.ContactId);
			}
		}
		List<User> userList = [
								SELECT Id, Email, Name, ContactId
								FROM User
								WHERE ContactId IN :contactIds];
		if(userList != null){
			for(User ct : userList){
				ctToUser.put(ct.ContactId, ct);
			}
		}
		

		return ctToUser;
	}


	//return true if case is not closed or if its the first email sent to the closed case
	//public static Boolean firstEmailOnClosed(Case tmpCase, List<CaseComment> comments){
	//	if(tmpCase.isClosed){
	//		for(CaseComment comm : comments){
	//			if(tmpCase.ClosedDate < comm.CreatedDate){
	//				return false;
	//			}
	//		}
	//	}
	//	return true;
	//}

	private static void sendEmail(String emailAddress, Case tmpCase, Id userId){
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		if(!REPLY_TEMPLATE.isEmpty() && !Test.isRunningTest()){
			mail.setTemplateId(REPLY_TEMPLATE[0].Id);
			mail.setTargetObjectId(userId);
			mail.setWhatId(tmpCase.Id);
		}
		else{
			mail.setPlainTextBody('Your case is closed but new comment was still added.');
	    	mail.setSubject('R-'+tmpCase.caseNumber+'] ' + Label.MBRClosedCaseSubject);
	    	mail.setToAddresses(new List<String>{emailAddress});
		}

		if(!Test.isRunningTest() && !NO_REPLY.isEmpty()){
			mail.setOrgWideEmailAddressId(NO_REPLY[0].Id);
		}
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}
}