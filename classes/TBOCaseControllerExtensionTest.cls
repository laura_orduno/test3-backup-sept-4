/*
###########################################################################
# File..................: TBOCaseControllerExtensionTest
# Version...............: 1
# Created by............: Umesh Atry (TechM)
# Created Date..........: 14/03/2016 
# Last Modified by......: Adhir Parchure (TechM)
# Last Modified Date....: 25/05/2016 
# Description...........: Test class is to provide coverage for TBOCaseControllerExtension.
#
############################################################################
*/


@isTest

private Class TBOCaseControllerExtensionTest{
    
  public static testmethod void PositiveRun()
    {
        list<account> listTestacc = new list<account> ();
        Schema.DescribeSObjectResult tboSchema = Schema.SObjectType.Account;
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = tboSchema.getRecordTypeInfosByName(); 

        Schema.DescribeSObjectResult tboCaseSchema = Schema.SObjectType.Case;
        Map<String,Schema.RecordTypeInfo> caseRecordTypeInfo = tboCaseSchema.getRecordTypeInfosByName(); 

        Account RCIDAcc = new Account(Name='testTBORCID',recordtypeid=AccountRecordTypeInfo .get('RCID').getRecordTypeId(), RCID__C='RCID123',  Inactive__c=false,Billing_Account_Active_Indicator__c='Y' );
        insert RCIDAcc;

        Case tboCase= new Case(Subject='testTBO',Created_Date__c=system.TODAY(),status='Draft',
                                recordtypeid=caseRecordTypeInfo.get('TBO Request').getRecordTypeId(),
                                Accountid=RCIDAcc.id);
        insert tboCase;

        Account RCIDchild1= new account(Name='RCIDchild1',recordtypeid=AccountRecordTypeInfo.get('CAN').getRecordTypeId(), parentid=RCIDAcc.id,CAN__C='CAN001',BTN__c='CBN001',BCAN__c='111-CAN001',BAN_CAN__c='111-CAN001', BCAN_Pilot_Org_Indicator__c=1,Inactive__c=false,Billing_Account_Active_Indicator__c='Y',Billing_System_ID__c='129');
        insert RCIDchild1;
        listTestacc.add(RCIDchild1);
        
        Account CBNchild1= new account(Name='CBNchild1',recordtypeid=AccountRecordTypeInfo.get('CAN').getRecordTypeId(), parentid=RCIDchild1.id,CAN__C='CAN167',BTN__c='FRA001',BCAN__c='111-CAN001',BAN_CAN__c='111-CAN167', Inactive__c=false,Billing_Account_Active_Indicator__c='Y',Billing_System_ID__c='129');
        insert CBNchild1;
        listTestacc.add(CBNchild1);
        
        Account CBNchild2= new account(Name='CBNchild2',recordtypeid=AccountRecordTypeInfo.get('CAN').getRecordTypeId(), parentid=RCIDchild1.id,CAN__C='CAN233',BTN__c='FRA002',BCAN__c='111-CAN001',BAN_CAN__c='111-CAN233', BCAN_Pilot_Org_Indicator__c=2,Inactive__c=false,Billing_Account_Active_Indicator__c='Y',Billing_System_ID__c='129');
        insert CBNchild2;
        listTestacc.add(CBNchild2);
        
        Account CBNInnerChild1= new account(Name='CBNInnerChild1',recordtypeid=AccountRecordTypeInfo.get('CAN').getRecordTypeId(), parentid=CBNchild2.id,CAN__C='CAN789',BTN__c='FRA002',BCAN__c='111-CAN001',BAN_CAN__c='111-CAN789',Inactive__c=false, Billing_Account_Active_Indicator__c='Y',Billing_System_ID__c='129');
        insert CBNInnerChild1;
        listTestacc.add(CBNInnerChild1);    
    
    //different hierarchy
    Account RCIDAcc1 = new Account(Name='testanotherTBORCID',recordtypeid=AccountRecordTypeInfo .get('RCID').getRecordTypeId(), RCID__C='RCID12399');
        insert RCIDAcc1;

        
        Account RCIDchild199= new account(Name='RCIDchild1',recordtypeid=AccountRecordTypeInfo.get('CAN').getRecordTypeId(), parentid=RCIDAcc1.id,CAN__C='CAN00199',BTN__c='CBN00199',BCAN__c='111-CAN00199',BAN_CAN__c='111-CAN00199', BCAN_Pilot_Org_Indicator__c=1,Inactive__c=false,Billing_Account_Active_Indicator__c='Y',Billing_System_ID__c='129');
        insert RCIDchild199;
    
    Account RCIDchild1999= new account(Name='RCIDchild1',recordtypeid=AccountRecordTypeInfo.get('CAN').getRecordTypeId(), CAN__C='CAN001999',BTN__c='CBN001999',BCAN__c='111-CAN001999',BAN_CAN__c='111-CAN001999', BCAN_Pilot_Org_Indicator__c=1,Inactive__c=false,Billing_Account_Active_Indicator__c='Y',Billing_System_ID__c='129');
        insert RCIDchild1999;
        
    
    
        try{
            test.startTest();
            TBOCaseControllerExtension extTest = new TBOCaseControllerExtension(new ApexPages.StandardController(tboCase));
            ApexPages.StandardsetController setcontest = new apexpages.standardsetcontroller(listTestacc);
            extTest.setcon= setcontest;
            map<id,account> testAccmap= new map<id,account>(listTestacc);
            extTest.finalAccList= testAccmap;
            list<TBOCaseControllerExtension.wrapperAccResult> testResultList= New list<TBOCaseControllerExtension.wrapperAccResult>();
            
            extTest.accSearchType='CAN'; //set search type
            extTest.AccSearchNumber='CAN789'; //set accountsearch number
            
            //start search method call
            pagereference pg= extTest.searchAndAddSingleAccount();
            pagereference pg1= extTest.searchAccountsFromCBN();
            pagereference pg2= extTest.searchAllAtRCID();
            pagereference pg3= extTest.CaseStatus();
            pagereference pg53=extTest.showAccountsDetailsInPDf();
            system.debug('>>> extTest.hasprevious'+extTest.hasprevious);
            system.debug('>>> extTest.hasnext'+extTest.hasnext);
            if(extTest.hasprevious)
                extTest.previous();
            if(extTest.hasnext)
                extTest.next(); 
            if(exttest.pageNumber!=null)
                integer pgnum=exttest.pageNumber;
            //pagereference pgPDf= extTest.showAccountsDetailsInPDf();
            extTest.first();
            extTest.last();
            extTest.previous();
            extTest.next();
            pagereference pg4= extTest.save();
        //pagereference pg5= extTest.deleteSelectedAccFromResults();
        TBOCaseControllerExtension.wrapperAccResult wrapacc= new TBOCaseControllerExtension.wrapperAccResult();
        wrapacc.accObj=CBNchild2;
        WrapAcc.accChkbox=true;
        testResultList.add(WrapAcc);
        system.debug('>>>>> ResultList >>'+extTest.resultList);
        system.debug('>>>>> testResultList >>'+testResultList);
        extTest.resultList = testResultList;
        //extTest.resultList[0].accChkbox=true;
        //extTest.resultList[1].accChkbox=true;
        pagereference pg61= extTest.deleteSelectedAccFromResults();
        extTest.setcon= null;
        if(extTest.hasprevious)
           system.debug('Naah..');
        if(extTest.hasnext)
           system.debug('Naah..');
        extTest.next();
        extTest.previous();
        extTest.first();
        extTest.last();
       list<TBOCaseControllerExtension.wrapperAccResult> wrapaccTest= extTest.getresultList();
        if(extTest.pagenumber!=null)
           system.debug('Naah..');  
        String s= extTest.generateString();
        exttest.totalResults= 100000; 
        s= extTest.generateString();
      //check with blank accountsearch number
      extTest.AccSearchNumber='';
      pagereference pg11= extTest.searchAndAddSingleAccount();
      pagereference pg12=extTest.searchAccountsFromCBN();
      pagereference pg13= extTest.searchAllAtRCID();
      
      //check with invalid accountsearch number
      extTest.AccSearchNumber='1';
      pagereference pg21= extTest.searchAndAddSingleAccount();
            pagereference pg22= extTest.searchAccountsFromCBN();
            pagereference pg23= extTest.searchAllAtRCID();
      
      // check with different hierarchy data
      extTest.AccSearchNumber='CAN00199';
      pagereference pg31= extTest.searchAndAddSingleAccount();
            pagereference pg32= extTest.searchAccountsFromCBN();
            pagereference pg33= extTest.searchAllAtRCID();
      
      
      
      // check with different hierarchy data
      extTest.AccSearchNumber='CAN001999';
      pagereference pg34= extTest.searchAndAddSingleAccount();
     
      
      //check with no results
      extTest.finalAccList.clear();
      pagereference pg41= extTest.searchAndAddSingleAccount();
      pagereference pg42= extTest.searchAccountsFromCBN();
      pagereference pg43= extTest.searchAllAtRCID();
      pagereference pg44= extTest.save();
       
                
      
      //check CaseStatus
      pagereference pg51=extTest.CaseStatus();
      //tboCase
      tboCase.status='Closed';
      update tboCase;
      pagereference pg52=extTest.CaseStatus();
      
      
            
            
            test.stopTest();
        }
        catch(Exception e){
        }
    }
  
  public static testmethod void NegativeRun()
    {
        list<account> listTestacc = new list<account> ();
        Schema.DescribeSObjectResult tboSchema = Schema.SObjectType.Account;
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = tboSchema.getRecordTypeInfosByName(); 

        Schema.DescribeSObjectResult tboCaseSchema = Schema.SObjectType.Case;
        Map<String,Schema.RecordTypeInfo> caseRecordTypeInfo = tboCaseSchema.getRecordTypeInfosByName(); 

        Account RCIDAcc = new Account(Name='testTBORCID',recordtypeid=AccountRecordTypeInfo .get('RCID').getRecordTypeId(), RCID__C='RCID123',Inactive__c=false,Billing_Account_Active_Indicator__c='Y');
        insert RCIDAcc;
        //Updated status from 'Cancelled' to 'Draft' as case cannot be created in 'Cancelled' status.
        Case tboCase= new Case(Subject='testTBO',Created_Date__c=system.TODAY(),status='Draft',
                                recordtypeid=caseRecordTypeInfo.get('TBO Request').getRecordTypeId(),
                                accountid=RCIDAcc.id);
        insert tboCase;
        // update case to 'Cancelled'.
        tboCase.Status_Cancel_Case__c='Cancelled';
        update tboCase;
        
        //insert tboCase;
       // Boolean expectedExceptionThrown =  e.getMessage().contains('At least the Outgoing Customer Account Name or the Incoming Customer Account Name need to populated to save TBO Request as Draft.') ? true : false;
      

        Account RCIDchild1= new account(Name='RCIDchild1',recordtypeid=AccountRecordTypeInfo.get('CAN').getRecordTypeId(), parentid=RCIDAcc.id,CAN__C='CAN001',BTN__c='CBN001',BCAN__c='111-CAN001',BAN_CAN__c='111-CAN001', BCAN_Pilot_Org_Indicator__c=1,Inactive__c=false,Billing_Account_Active_Indicator__c='Y');
        insert RCIDchild1;
        listTestacc.add(RCIDchild1);
        
        Account CBNchild1= new account(Name='CBNchild1',recordtypeid=AccountRecordTypeInfo.get('CAN').getRecordTypeId(), parentid=RCIDchild1.id,CAN__C='CAN167',BTN__c='FRA001',BCAN__c='111-CAN001',BAN_CAN__c='111-CAN167', Inactive__c=false,Billing_Account_Active_Indicator__c='Y');
        insert CBNchild1;
        listTestacc.add(CBNchild1);
        
        Account CBNchild2= new account(Name='CBNchild2',recordtypeid=AccountRecordTypeInfo.get('CAN').getRecordTypeId(), parentid=RCIDchild1.id,CAN__C='CAN233',BTN__c='FRA002',BCAN__c='111-CAN001',BAN_CAN__c='111-CAN233', BCAN_Pilot_Org_Indicator__c=2,Inactive__c=false,Billing_Account_Active_Indicator__c='Y');
        insert CBNchild2;
        listTestacc.add(CBNchild2);
        
        Account CBNInnerChild1= new account(Name='CBNInnerChild1',recordtypeid=AccountRecordTypeInfo.get('CAN').getRecordTypeId(), parentid=CBNchild2.id,CAN__C='CAN789',BTN__c='FRA002',BCAN__c='111-CAN001',BAN_CAN__c='111-CAN789', Inactive__c=false,Billing_Account_Active_Indicator__c='Y');
        insert CBNInnerChild1;
        listTestacc.add(CBNInnerChild1);    
    
    //different hierarchy
    Account RCIDAcc1 = new Account(Name='testanotherTBORCID',recordtypeid=AccountRecordTypeInfo .get('RCID').getRecordTypeId(), RCID__C='RCID12399',Billing_Account_Active_Indicator__c='Y',Inactive__c=false);
        insert RCIDAcc1;

        
        Account RCIDchild199= new account(Name='RCIDchild1',recordtypeid=AccountRecordTypeInfo.get('CAN').getRecordTypeId(), parentid=RCIDAcc1.id,CAN__C='CAN00199',BTN__c='CBN00199',BCAN__c='111-CAN00199',BAN_CAN__c='111-CAN00199', BCAN_Pilot_Org_Indicator__c=1,Inactive__c=false,Billing_Account_Active_Indicator__c='Y');
        insert RCIDchild199;
    
    Account RCIDchild1999= new account(Name='RCIDchild1',recordtypeid=AccountRecordTypeInfo.get('CAN').getRecordTypeId(), CAN__C='CAN001999',BTN__c='CBN001999',BCAN__c='111-CAN001999',BAN_CAN__c='111-CAN001999', BCAN_Pilot_Org_Indicator__c=1,Inactive__c=false,Billing_Account_Active_Indicator__c='Y');
        insert RCIDchild1999;
        
    
    
        try{
            test.startTest();
            TBOCaseControllerExtension extTest1 = new TBOCaseControllerExtension(new ApexPages.StandardController(tboCase));
            ApexPages.StandardsetController setcontest = new apexpages.standardsetcontroller(listTestacc);
            extTest1.setcon= null;
            if(extTest1.hasprevious)
               system.debug('Naah..');
            if(extTest1.hasnext)
               system.debug('Naah..');
            extTest1.next();
            extTest1.previous();
            extTest1.first();
            extTest1.last();
            extTest1.accSearchType='CAN'; //set search type
            
      // check with different hierarchy data
      extTest1.AccSearchNumber='CAN001999';
      pagereference pg34= extTest1.searchAndAddSingleAccount();
      // validate with no outgoing account
      tboCase.account=null;
      update tboCase;
      extTest1 = new TBOCaseControllerExtension(new ApexPages.StandardController(tboCase));
            
      extTest1.AccSearchNumber='CAN789';
       pagereference pg35= extTest1.searchAndAddSingleAccount();
      //isvalid=extTest1.SearchAndValidateParentRCID('CAN789');
      pagereference pg51=extTest1.CaseStatus();
      //change case outgoing account back to rcid
      tboCase.accountid=RCIDAcc.id;
      update tboCase;
      
      
      /*//check with no results
      extTest1.finalAccList.clear();
      pagereference pg41= extTest1.searchAndAddSingleAccount();
      pagereference pg42= extTest1.searchAccountsFromCBN();
      pagereference pg43= extTest1.searchAllAtRCID();
      //pagereference pgPDf= extTest1.showAccountsDetailsInPDf();
      pagereference pg44= extTest1.save();
      
      //check CaseStatus
      pagereference pg51=extTest1.CaseStatus();
      //tboCase
      tboCase.status='Closed';
      update tboCase;
      pagereference pg52=extTest1.CaseStatus();
      
      extTest1.setcon=null;
            
      if(extTest1.hasprevious)
        extTest1.previous();
      if(extTest1.hasnext)
         extTest1.next(); 
      if(exttest1.pageNumber!=null)
         integer pgnum=exttest1.pageNumber;
    */  
    
      test.stopTest();
        }
        catch(Exception e){
        }
    }
  
}