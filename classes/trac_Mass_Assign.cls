global with sharing class trac_Mass_Assign {
    public class MassAssignException extends Exception {}

    public string newOwnerId {get; set;}
    public string newOwnerQueueId {get; set;}
    public SelectOption[] newOwnerOptions {get; private set;}
    // public SelectOption[] newOwnerQueueOptions {get; private set;}

    private Map<Id, Lead> leads {get; private set;}
    public Lead[] getLeads() { return (leads == null) ? new List<Lead>() : leads.values(); }
    public trac_Mass_Assign() {
        newOwnerOptions = new List<SelectOption>();
        // newOwnerQueueOptions = new List<SelectOption>();
        Set<String> ids = new Set<String>();
        if (ApexPages.currentPage().getParameters().get('ids') != null) {
            ids.addAll(ApexPages.currentPage().getParameters().get('ids').split(','));
        }
        leads = new Map<Id, Lead>([Select Id, Name, OwnerId From Lead Where Id In :ids Limit 1000]);
        if (leads == null) { return; }
        //addETP(String.valueOf(leads.size()));
        
        User[] users = [Select Name From User Where IsActive = true Limit 999];
        if (users == null) { return; }
        /*
        Group[] queues = null;
        QueueSobject[] qss = [Select Id, QueueId From QueueSobject Where SobjectType = :'Lead' Limit 999];
        if (qss != null && qss.size() > 0) {
            Set<Id> qids = new Set<id>();
            for (QueueSObject qs : qss) {
                qids.add(qs.QueueId);
            }
            queues = [Select Name From Group Where Id in :qids and Type = :'Queue' Limit 999];
        }
        */
        newOwnerOptions.add(new SelectOption('blank', '-- None --'));
        //newOwnerQueueOptions.add(new SelectOption('blank', '-- None --'));
        for (User u : users) {
            newOwnerOptions.add(new SelectOption(u.Id, u.Name));
        }
        /*
        if (queues != null) {
            for (Group q : queues) {
                newOwnerQueueOptions.add(new SelectOption(q.Id, q.Name));
            }
        }
        */
        newOwnerId = UserInfo.getUserId();
        //newOwnerId = 'blank';
        //newOwnerQueueId = 'blank';
    }

    public PageReference onAssign() {
        // Redirect the user back to the leads page
        Savepoint sp = Database.setSavepoint();
        try {
            if (newOwnerId == null/* && newOwnerQueueId == null*/) { addETP('You must choose a new owner for these leads.'); return null; }
            if (leads == null || leads.size() == 0) { addETP('No leads to reassign.'); return null; }
            Id newOwner = null;
            //Boolean newOwnerIsGroup = false;
            if (newOwnerId != 'blank') {
                newOwner = (Id) newOwnerId;
            } /*else if (newOwnerQueueId != 'blank') {
                newOwner = (Id) newOwnerQueueId;
                newOwnerIsGroup = true;
            } */
            if (newOwner != null) {
                Note[] relatedNotes = null;
                Task[] relatedTasks = null;
                Event[] relatedEvents = null;
                Attachment[] relatedAttachments = null;
                //if (newOwnerIsGroup == false) {
                    String query = '';
                    String activityQ = '';
                    Integer li = 0;
                    for (Lead l : leads.values()) {
                        if (String.valueOf(l.OwnerId).substring(0, 3) != '00G') {
                            if (li != 0) { query += 'OR '; activityQ += 'OR ';}
                            query += '(OwnerId = \'' + l.OwnerId + '\' AND ParentId = \'' + l.Id + '\') ';
                            activityQ += '(OwnerId = \'' + l.OwnerId + '\' AND WhoId = \'' + l.Id + '\') ';
                            li++;
                        }
                    }
                    String noteQuery = 'Select OwnerId From Note Where ' + query + ' Limit 50000';
                    //addETP(noteQuery);
                    String attachmentQuery = 'Select OwnerId From Attachment Where ' + query + ' Limit 50000';
                    
                    //addETP(attachmentQuery);
                    String taskQ = 'Select OwnerId From Task Where (' + activityQ + ') AND IsClosed = false Limit 50000';
                    //addETP(taskQ);
                    String eventQ = 'Select OwnerId From Event Where (' + activityQ + ') AND (ActivityDate >= TODAY or ActivityDate = null) Limit 50000';
                    //addETP(eventQ);
                    if (query != null && query != '') {
                    relatedNotes = Database.query(noteQuery);
                    relatedAttachments = Database.query(attachmentQuery);
                    relatedTasks = Database.query(taskQ);
                    relatedEvents = Database.query(eventQ);
                    }
                //}
                
                for (Lead l : leads.values()) { l.OwnerId = newOwner; }
                update leads.values();
                
                if (relatedNotes != null && relatedNotes.size() > 0) {
                    for (Note n : relatedNotes) { n.OwnerId = newOwner; }
                    update relatedNotes;
                }
                if (relatedAttachments != null && relatedAttachments.size() > 0) {
                    for (Attachment a : relatedAttachments) { a.OwnerId = newOwner; }
                    update relatedAttachments;
                }
                if (relatedTasks != null && relatedTasks.size() > 0) {
                    for (Task t : relatedTasks) { t.OwnerId = newOwner; }
                    update relatedTasks;
                }
                if (relatedEvents != null && relatedEvents.size() > 0) {
                    for (Event e : relatedEvents) { e.OwnerId = newOwner; }
                    update relatedEvents;
                }
                
            } else {
                addETP('You must choose a new owner for these leads.'); return null;
            }
            
            PageReference pageRef = new PageReference('/00Q/o'); //('/' + rId);
            pageRef.setRedirect(true);
            return pageRef;
        } catch (exception e) {Database.rollback(sp); quoteutilities.log(e); } return null;
    }
    
    private void addETP(String e) {
        ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, e));
    }
    
}