/**
 * @author Steve Doucette, Traction on Demand 
 * @date 2018-06-15
 */
@isTest
public with sharing class PurgerTest {

	@isTest
	public static void testPurger() {
		// Retrieve custom metadata type(s) that are active
		List<Purger_Setting__mdt> purgerSettings = [
				SELECT Id, DeveloperName, MasterLabel, Criteria__c, Exclusion_Field__c, Frequency__c, Last_Run__c, Retention_Time__c
				FROM Purger_Setting__mdt
				WHERE Active__c = TRUE
				LIMIT 10
		];

		// Create random items for the purger to remove
		Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
		List<SObject> randomItems = new List<SObject>();
		for (Purger_Setting__mdt ps : purgerSettings) {
			if (gd.containsKey(ps.MasterLabel)) {
				randomItems.add(gd.get(ps.MasterLabel).newSobject());
			}
		}
		Database.insert(randomItems, false);
		for (SObject sobj : randomItems) {
			Test.setCreatedDate(sobj.Id, DateTime.newInstance(1900, 1, 1));
		}

		Test.startTest();
		for (Purger_Setting__mdt ps : purgerSettings) {
			Database.executeBatch(new PurgerBatchJob(ps)); // Failsafe to run batch job for Purger Settings
		}
		System.schedule('Test Purger', '0 0 0 1 1 ? 2055', new PurgerScheduler());

		// Cover exception logging
		try{
			throw new NullPointerException();
		} catch (Exception e){
			System.debug(e);
			PurgerUtils.log(e);
			PurgerUtils.log('Error occured');
		}
		/*
			Cannot do any asserts as we can't guarantee creation of records of SObjects defined
			in the Purger Settings (due to validation rules, required fields, etc.)
		 */
		Test.stopTest();
	}
}