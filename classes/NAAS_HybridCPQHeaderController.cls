global class NAAS_HybridCPQHeaderController {
   Public String orderId {get;set;}
   //Id original_param_order_id = '80122000000Ckxx';
   public Order order {get;set;}
   public Order objOrder {get; set;}
   public String OrderNumber{get;set;}
   public string parentTabId{get;set;}
   public Boolean reSumbitOrder {get;set;}
   public boolean isCancelCompleted{get;set;}
   public boolean hasCancellationError{get;set;}
   public String RedirectPage{get;set;}
   public String previousOrderStatus{get;set;}
   public integer mockTestVariable=0;
   public Boolean displayCancelOrder{get;set;} 
    //Arpit : 19-May-2017 : Sprint-3 : Toggleing ENglish-French Logic
   public static string lang {get ; set;}
   public string Title{get;set;}
   public string Email{get;set;}
   public string Phone{get;set;}
  
   public string Owner{get;set;}
   public String OwnerId{get;set;}
   //public string MaxSpeed{get;set;}
   public string orderStatus{get;set;}
   public Boolean displayComponents{get;set;}
   public Boolean displayViewPdfBtn{get;set;}
   public Boolean disableViewPdfBtn{get;set;}
   public String smbCareAddrId{get;set;}
   public String moveOutAddrId{get;set;}

   
   Map<String, SMBCare_Address__c> addressToIDMap = new Map<String, SMBCare_Address__c>();
   public SMBCare_Address__c srvcAddress = new SMBCare_Address__c();

   
   //public Boolean srvcAddrssNotValid{get;set;}
  // public String ad;

   public  NAAS_HybridCPQHeaderController() {
    
    hasCancellationError = false;
    reSumbitOrder= false;
    parentTabId=ApexPages.currentPage().getParameters().get('parentTabID'); 
    orderId = ApexPages.currentPage().getParameters().get('OrderId') == null ? '' : ApexPages.currentPage().getParameters().get('OrderId');
    smbCareAddrId = ApexPages.currentPage().getParameters().get('smbCareAddrId') == null ? '' : ApexPages.currentPage().getParameters().get('smbCareAddrId');

    moveOutAddrId = ApexPages.currentPage().getParameters().get('smbCareAddrId') == null ? '' : ApexPages.currentPage().getParameters().get('moveOutAddrId');
    if(orderId == null || orderId == '')
        orderId = ApexPages.currentPage().getParameters().get('id') == null ? '' : ApexPages.currentPage().getParameters().get('id');

    if (orderId != null) {
        //Aruna: amended below SOQL to include field : CustomerAuthorizedById
        list<order> orderList = [select Name, OrderNumber, Status, Type, Account.Name, CustomerAuthorizedById, CustomerAuthorizedBy.Name, CustomerAuthorizedBy.Phone, RequestedDate__c,Service_Address__r.Address__c,Service_Address__r.Street_Address__c,Service_Address__r.City__c,Service_Address__r.FMS_Address_ID__c,Service_Address__r.Send_to_SACG__c,Service_Address_Text__c,Service_Address__r.Maximum_Speed__c, Service_Address__r.isTVAvailable__c,Service_Address__r.Forbone__c,Service_Address__r.Status__c, Maximum_Speed__c,Forbone__c, ContactTitle__c, ContactEmail__c, ContactPhone__c,MoveOutServiceAddress__c,MoveOutServiceAddress__r.Suite_Number__c,MoveOutServiceAddress__r.Address__c,MoveOutServiceAddress__r.City__c,MoveOutServiceAddress__r.Postal_Code__c, MoveOutServiceAddress__r.Country__c,MoveOutServiceAddress__r.Province__c,Move_Out_Service_Address_Text__c , Service_Address__r.Is_ILEC__c from Order where Id =: orderId];

        List<OrderItem> orderItemRecs = [Select Id, PriceBookEntry.product2.IncludeQuoteContract__c FROM OrderItem WHERE OrderId = :orderId and PriceBookEntry.product2.Sellable__c = true];
        system.debug('>>>>>> Order' + orderList);
        if(orderList.size()>0 && !orderList.isEmpty() ){
          order = orderList[0];

          Title = order.ContactTitle__c != null ? order.ContactTitle__c: '';
          Email = order.ContactEmail__c != null ? order.ContactEmail__c: '';
          Phone = order.ContactPhone__c != null ? order.ContactPhone__c: '';
          //Forborne =  order.Forbone__c == true ?  Label.OCOM_Forborne : Label.OCOM_Non_Forborne;
          //Type = order.Type != null ? order.Type:'';
          Owner = order.CustomerAuthorizedBy.Name != null ? order.CustomerAuthorizedBy.Name :'';
          //Aruna: amended below line to remove "Invalid Id" error.
          OwnerId = order.CustomerAuthorizedByID != null ? ''+order.CustomerAuthorizedById :'';
          //MaxSpeed = order.Service_Address__r.Maximum_Speed__c != null ? order.Service_Address__r.Maximum_Speed__c : '';
          OrderNumber = order.OrderNumber != null? order.OrderNumber : '';
          orderStatus = order.Status != null? order.Status:'';
          objOrder = [Select o.OrderNumber,o.Order_Number__c,o.Credit_Assessment__c,o.Credit_Assessment__r.CAR_Status__c,o.vlocity_cmt__AccountId__c,o.RecordTypeId, o.Name,o.Id,o.Reason_Code__c,o.General_Notes_Remarks__c,status from Order o where o.Id=:orderId limit 1];
          isCancelCompleted=false;

        }
        if(smbCareAddrId != null){
          set<string > addressIdSet = new Set<String>{smbCareAddrId,moveOutAddrId};

          for (SMBCare_Address__c serviceAddress : [SELECT ID, Service_Account_Id__c, Service_Account_Id__r.Id,
                                              FMS_Address_ID__c, Send_to_SACG__c, isTVAvailable__c, Forbone__c, Maximum_Speed__c, Status__c ,AddressText__c,Is_ILEC__c
                                            FROM SMBCare_Address__c WHERE ID =:addressIdSet]){
                                              addressToIDMap.put(serviceAddress.id, serviceAddress);
                                            }
          /*if(serviceAddressList.size() > 0 && !serviceAddressList.isEmpty())   {
               srvcAddress = serviceAddressList[0];
              if(srvcAddress != null){
                addrNotValid = (srvcAddress.Status__c != null && srvcAddress.Status__c != 'Valid')? true:false;
                tvAvailability = srvcAddress.isTVAvailable__c == true? Label.OCOM_TV_Available : Label.OCOM_TV_Not_Available ; 
                forborne = srvcAddress.Forbone__c == true? Label.OCOM_Forborne : Label.OCOM_Non_Forborne;
                maximumSpeed = srvcAddress.Maximum_Speed__c != null ? srvcAddress.Maximum_Speed__c : '';
                maxSpeedCopper = maximumSpeed == '' ? true:false;
             }
             else {
                tvAvailability = '' ; 
                forborne = '';
                maximumSpeed = '';
                maxSpeedCopper = false;
             }
          }*/
        }

        //Niran OCOM-403
        disableViewPdfBtn = false; // changed true to false by prashant
        if(orderItemRecs != null && orderItemRecs.size()>0)
        {
            for(OrderItem orderRec : orderItemRecs)
            {
                //if(orderRec.PriceBookEntry.product2.IncludeQuoteContract__c == true)
                        //disableViewPdfBtn = false; if and this line commented by prashant on 10-Jan-2017
            }
        }
      }

    }
     public string Type{get{
      if((Type == null || Type == '') && order != null){
          return  Type = order.Type != null ? order.Type:'';
        }
        else 
        return  Type ;
      }
      set;}
   
   /* public String genericAddress{
      get{
      if((genericAddress == null || genericAddress == '') && order != null){
        return genericAddress = order.Service_Address_Text__c != null? order.Service_Address_Text__c:'';
      } 
       else if(smbCareAddrId != null && addressToIDMap.ContainsKey(smbCareAddrId)){
        return genericAddress = addressToIDMap.get(smbCareAddrId).AddressText__c != null ? addressToIDMap.get(smbCareAddrId).AddressText__c:'';
       }
      else 
        return genericAddress; 
    }
    set;}*/

     public String genericAccountName{
      get{
      if(order != null){
        return genericAccountName = order.Account.Name != null ? order.Account.Name:'';
      } 
     
      else 
        return genericAccountName;
    }
    set;}

    /*public String moveOutAddress{
      get{
      if((moveOutAddress == null || moveOutAddress == '') && order != null && order.Move_Out_Service_Address_Text__c != null){
         String movoutAddressText;   
          movoutAddressText =    order.Move_Out_Service_Address_Text__c;
        return moveOutAddress = movoutAddressText != null && movoutAddressText != '' ? movoutAddressText :'';
        
      } 
      else if((moveOutAddress == null || moveOutAddress == '') && moveOutAddrId != null && addressToIDMap.ContainsKey(moveOutAddrId)){
          return genericAddress = addressToIDMap.get(moveOutAddrId).AddressText__c != null ? addressToIDMap.get(moveOutAddrId).AddressText__c :'';
      }
      else 
        return moveOutAddress; 
    }
    set;}

  /*   public Boolean srvcAddrssNotValid{get{
      if( order != null ){
        return srvcAddrssNotValid = (order.Service_Address__r.Status__c != null && order.Service_Address__r.Status__c != 'Valid')? true:false;
      } 
       else if(smbCareAddrId != null && addressToIDMap.ContainsKey(smbCareAddrId)){
        return srvcAddrssNotValid = (addressToIDMap.get(smbCareAddrId).Status__c != null && addressToIDMap.get(smbCareAddrId).Status__c != 'Valid') ? true  :false;
      }
      else 
        return srvcAddrssNotValid; 
     }
     set;}

     public String tvAvailabilityInfo{get{
      if((tvAvailabilityInfo == null || tvAvailabilityInfo == '' ) && order != null){
        return tvAvailabilityInfo = order.Service_Address__r.isTVAvailable__c == true ? Label.OCOM_TV_Available : Label.OCOM_TV_Not_Available;
      } 
      else if(smbCareAddrId != null && addressToIDMap.ContainsKey(smbCareAddrId)){
        return tvAvailabilityInfo = addressToIDMap.get(smbCareAddrId).isTVAvailable__c == true ? Label.OCOM_TV_Available : Label.OCOM_TV_Not_Available;
      }
      else 
        return tvAvailabilityInfo; 
     }
     set;}

     public string forborneInfo{get{
      if((forborneInfo == null || forborneInfo == '' ) && order != null){
        system.debug('>>>>>forborneInfo' + forborneInfo);
        return forborneInfo = order.Service_Address__r.Forbone__c == true ?  Label.OCOM_Forborne : Label.OCOM_Non_Forborne;
      } 
      else if(smbCareAddrId != null && addressToIDMap.ContainsKey(smbCareAddrId)){
        return forborneInfo = addressToIDMap.get(smbCareAddrId).Forbone__c == true ? Label.OCOM_Forborne : Label.OCOM_Non_Forborne;
      }
      else 
        return forborneInfo; 
      }set;}

      public string MaxSpeed{get{
      if((MaxSpeed == null || MaxSpeed == '' ) && order != null){
        system.debug('>>>>>MaxSpeed' + MaxSpeed);
        
        return MaxSpeed = order.Service_Address__r.Maximum_Speed__c != null ? order.Service_Address__r.Maximum_Speed__c : '';
      } 
      else if(smbCareAddrId != null && addressToIDMap.ContainsKey(smbCareAddrId)){
        return MaxSpeed = addressToIDMap.get(smbCareAddrId).Maximum_Speed__c != null ? addressToIDMap.get(smbCareAddrId).Maximum_Speed__c : '';
      }
      else {
        //maxSpdCopper = true;
        return MaxSpeed;
      }
      }set;}

     public Boolean maxSpdCopper{get{
        if( order != null)
         return maxSpdCopper =  MaxSpeed != '' ? false : true;

        else return maxSpdCopper;
      }set;}

   public Boolean isILEC{get{
        if( order != null)
            return isILEC =  order.Service_Address__r.Is_ILEC__c != null ? order.Service_Address__r.Is_ILEC__c : false;
         else if(smbCareAddrId != null && addressToIDMap.ContainsKey(smbCareAddrId)){
            return isILEC = addressToIDMap.get(smbCareAddrId).Is_ILEC__c != null ? addressToIDMap.get(smbCareAddrId).Is_ILEC__c : false;
         }
         else return true;
        //else return isILEC;
      }set;}*/

  
  
    public PageReference DoOk(){
        RedirectPage='';
        isCancelCompleted=false;
        
        if(Test.isRunningTest()){
            objOrder.General_Notes_Remarks__c='Test Remark';
        }
        if(String.isBlank(objOrder.Reason_Code__c) || String.isBlank(objOrder.General_Notes_Remarks__c) ){
            //RedirectPage='Error:One or More fields have been left Blank';
            apexpages.addmessage(new apexpages.message(apexpages.severity.error,'Reason Code and General Notes Remarks cannot be blank.'));
        }else {
            cancelOrder();
            if(!apexpages.hasmessages()){
                Boolean result=DoCancel(objOrder.id,objOrder.Reason_Code__c,objOrder.General_Notes_Remarks__c);
                
                if(result)
                { 
                    apexpages.addmessage(new apexpages.message(apexpages.severity.confirm,'Your Order has been cancelled successfully.  Please click the Close button.'));
                    isCancelCompleted=true;
                    
                }
                
            }        
        }
        return null;
    }
    
     public boolean DoCancel(string orderId, string reasonCode, string notes)
    {       
        Order ord = [Select change_type__c, reason_code__c,Status, General_Notes_Remarks__c,Credit_Assessment__r.CAR_Result__c,Credit_Assessment__r.CAR_Status__c,Credit_Assessment__c,Credit_Assessment__r.CAR_Code__c from Order where Id=:orderId];
        ord.Change_Type__c = 'Cancel Entire Order';
        ord.Reason_Code__c = reasonCode;
        ord.General_Notes_Remarks__c = notes;
        ord.Status ='Cancelled';
        ord.Activities__c='N/A';
        try{update ord;}catch(exception e){ apexpages.addmessage(new apexpages.message(apexpages.severity.error,'Please contact System Admin' + e.getMessage()));return false;}
        system.debug('inside car cancellation 1'+ord);
        //CAR Cancellation
        string carNo = ord.Credit_Assessment__c;
        string carStatus;
        string carResult;    
        if(string.isNotBlank(carNo)){
            carStatus =ord.Credit_Assessment__r.CAR_Status__c;
            carResult =ord.Credit_Assessment__r.CAR_Result__c;
        }
        string orderStatus =ord.Status;
        if(string.isnotblank(carNo) && string.isnotblank(orderStatus) && orderStatus=='Cancelled' && carStatus !='Cancelled' && carStatus !='Completed' && carResult!='Approved'){
            system.debug('inside car cancellation 1 '+ord);
            Credit_Assessment__c CAR= new Credit_Assessment__c();
            CAR.id=carNo;
            CAR.CAR_Status__c='Cancelled';     
            CAR.CAR_Code__c='Cancel order received: TELUS Initiated'; //o.Reason_Code__c;
            // need to add field for reason to cancel           
            try{update CAR; }catch(exception e){apexpages.addmessage(new apexpages.message(apexpages.severity.error,'Please contact System Admin' + e.getMessage()));return false;}
        } 
        return true;
    }
    
    public pagereference invalidForm(){
        apexpages.addmessage(new apexpages.message(apexpages.severity.error,' Reason Code, and Notes cannot be blank.'));
        if(String.isBlank(objOrder.Reason_Code__c) || String.isBlank(objOrder.General_Notes_Remarks__c)) {
            hasCancellationError = true;
        } 
        else {
             hasCancellationError = false;
        }
        
        return null;
    }
    public PageReference CancelButton()
    { 
        PageReference pageRef = new PageReference('/apex/NAAS_SubmitOrder?id='+objOrder.id);
        pageRef.setRedirect(true);
        return pageRef;
    }
    
     public void cancelOrder(){     
        System.debug('cancelOrder, previousOrderStatus: ' + previousOrderStatus);
        Boolean IsCancelFail= false;
        Boolean isOrderActivated=false;
        List<apexpages.message> messages = new List<apexpages.message>();        
        
        
        if (previousOrderStatus == 'In Progress' || previousOrderStatus == 'Completed' || previousOrderStatus == 'Error' || previousOrderStatus == 'Initiating' || previousOrderStatus == 'Activated' || previousOrderStatus == 'Delivery' || previousOrderStatus  == 'Submitted') {
           isOrderActivated=true;
           
            map<string,schema.recordtypeinfo> recordTypeInfoMap=webservice_integration_error_log__c.sobjecttype.getdescribe().getrecordtypeinfosbyname();
            Webservice_Integration_Error_Log__c log=new Webservice_Integration_Error_Log__c(apex_class_and_method__c='NAAS_CancelOrderController.cancelOrder',external_system_name__c='NC',webservice_name__c='Cancel Order',object_name__c='ORDER',sfdc_record_id__c=objOrder.id);
            
            try{
                CancelNCOrder(objOrder.id,log); }catch(calloutexception ce){ system.debug(ce.getmessage()); log.recordtypeid=recordTypeInfoMap.get('Failure').getrecordtypeid();log.error_code_and_message__c+=ce.getmessage();log.stack_trace__c=ce.getstacktracestring();system.debug('eRROR '+ce.getmessage());messages.add(new apexpages.message(apexpages.severity.error,'NetCracker Order Manager did not respond.  Salesforce received: ' + ce.getmessage()));IsCancelFail=true;
            }catch(CancelNCOrderException cnoe){system.debug(cnoe.getmessage());log.recordtypeid=recordTypeInfoMap.get('Failure').getrecordtypeid();log.error_code_and_message__c+=cnoe.getmessage();log.stack_trace__c=cnoe.getstacktracestring();apexpages.addmessage(new apexpages.message(apexpages.severity.error,cnoe.getmessage()));IsCancelFail =true;return;
            }catch(exception e){system.debug(e.getmessage());log.recordtypeid=recordTypeInfoMap.get('Failure').getrecordtypeid();log.error_code_and_message__c+=e.getmessage();log.stack_trace__c=e.getstacktracestring();messages.add(new apexpages.message(apexpages.severity.error,e.getmessage()));IsCancelFail =true;
            }finally{                            
                if(IsCancelFail){
                    try{
                        log.error_code_and_message__c=(string.isnotblank(log.error_code_and_message__c)&&log.error_code_and_message__c.length()>32767?log.error_code_and_message__c.substring(0,32767):log.error_code_and_message__c);
                        log.stack_trace__c=(string.isnotblank(log.stack_trace__c)&&log.stack_trace__c.length()>32767?log.stack_trace__c.substring(0,32767):log.stack_trace__c);
                        insert log;
                        if (Test.isRunningTest())
                        {
                            //Test code to throw exception
                            IsCancelFail=false;
                            messages.add(new apexpages.message(apexpages.severity.error,'For Test coverage'));                                 
                            Webservice_Integration_Error_Log__c toThrowException = new Webservice_Integration_Error_Log__c();
                            update toThrowException;
                            
                        }
                    }catch(exception ex){ 
                        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();List<String> lstEmail = new List<String>(); //use single custom setting for all data*/SMBCare_WebServices__c wsEmailAddress = SMBCare_WebServices__c.getValues('EmailAddress');string htmlBody = '<b>NAAS_CancelOrderController.cancelOrder</b><br/>'+ '<b>------------------------------------------------------</b><br/>'+'<b>===========================================================</b><br/>'+'<b>Unable to insert NC Cancellation Log for Order:'+objOrder.Id+'</b><br/><br/>'+ex.getmessage()+'<br/><br/>Stack Trace:<br/>'+ex.getstacktracestring()+'<br/>'+'<b> ---------------------------------------------</b><br/>';string eMail='andy.leung@telus.com';if (String.isNotBlank(wsEmailAddress.Value__c)){ eMail= wsEmailAddress.Value__c;}mail.setToAddresses(eMail.split(',', 0)); mail.setSenderDisplayName('noreply@salesforce.com');mail.setReplyTo('noreply@salesforce.com');mail.setSubject('Unable to insert NC Cancellation Log for Order');mail.setHtmlBody(htmlBody);Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                        }
                }
            }
        }
        system.debug('>>>>>isOrderActivated ::'+isOrderActivated );
        system.debug('>>>>>isOrderActivated::'+isOrderActivated);
        system.debug('IsCancelFail'+IsCancelFail);          
        //WFM booking cancellation
        if((isOrderActivated && !IsCancelFail) ||(!isOrderActivated)){
            try{
                //SMB_WebServiceHelper.NAAS_CancelWorkOrder(objOrder.id); 
                unreserveUsedTns(objOrder.id);
                system.debug('>>>>>cancelWFMBookingByOrderId::');
                NAAS_WFM_DueDate_Mgmt_Helper.cancelWFMBookingByOrderId(objOrder.id);
                

                if (Test.isRunningTest()) {
                    //Test code to throw exception
                    messages.add(new apexpages.message(apexpages.severity.error,'For Test coverage'));
                }
            }
            catch(exception e){apexpages.addmessage(new apexpages.message(apexpages.severity.error,'Please contact Adminstrator,WorkOrder cancellation is failed due to : '+e.getmessage()));}
        }
        if(!messages.isempty()){
            for(apexpages.message message:messages){
                if(!Test.isRunningTest()) { apexpages.addmessage(message);
                                          }
            }
            
        }
        
    }
    //27-Feb-2017 : Manpreet : Added public access modifier to make this method visible in test class
    public void cancelNCOrder(String orderID, webservice_integration_error_log__c log){
        
        OrdrCancelOrderManager cancelOrder= new OrdrCancelOrderManager();
        TpCommonBaseV3.CharacteristicValue[] CharacteristicValue;
        TpFulfillmentCustomerOrderV3.CustomerOrder cancelOrderResponse;
        list<string> Value = new list<string>();
        try{
            if(Test.isRunningTest())
            {  
                TpFulfillmentCustomerOrderV3.CustomerOrder cus = new TpFulfillmentCustomerOrderV3.CustomerOrder();
                List<TpCommonBaseV3.CharacteristicValue> CharacteristicValue1 = new List<TpCommonBaseV3.CharacteristicValue>();
                TpCommonBaseV3.CharacteristicValue cvalue= new TpCommonBaseV3.CharacteristicValue();
                List<string> str= new List<string>();
                str.add('str');cvalue.value=str;
                CharacteristicValue1.add(cvalue);
                cus.CharacteristicValue=CharacteristicValue1;
                if(mockTestVariable==0)
                {  cus.ErrorCode='error';
                 cus.ErrorDescription='errordesc';
                }else if(mockTestVariable==1){  cus.ErrorCode='';
                                              cus.ErrorDescription='';
                                             }
                cancelOrderResponse =cus;
                if(mockTestVariable==3){cancelOrderResponse =  OrdrCancelOrderManager.cancelOrder(orderID);}   
            } else{
                cancelOrderResponse =  OrdrCancelOrderManager.cancelOrder(orderID);             
            }
        }catch(exception e) { 
            throw new CancelNCOrderException('Please contact Adminstrator, Order didn\'t get cancelled due to mentioned Error:'+e.getmessage());
        }
        
        if(null!=cancelOrderResponse && null !=cancelOrderResponse.CharacteristicValue && cancelOrderResponse.CharacteristicValue.size()>0 && null != cancelOrderResponse.CharacteristicValue.size())
        { 
            log.error_code_and_message__c+='\n\nRequest:\n\n'+cancelOrderResponse.ErrorCode+'\n\nResponse:\n\n'+cancelOrderResponse.ErrorDescription;
            system.debug('cancelOrderResponse.ErrorCode--->>>>'+ cancelOrderResponse.ErrorCode);
            if(String.isBlank(cancelOrderResponse.ErrorCode))
            {  CharacteristicValue =cancelOrderResponse.CharacteristicValue;
             if(null!=CharacteristicValue[0] && CharacteristicValue[0].value.size()>0)
                 Value = CharacteristicValue[0].value;
             system.debug('response'+value);
             
            } 
            else
            {   //Arpit : 27-Feb-2017 : Added if-else condition for Test case run
                String response='EXCEPTION:-';              
                if(!Test.isRunningTest()){                    
                    response=+' '+cancelOrderResponse.ErrorCode+' : '+cancelOrderResponse.ErrorDescription;            
                    response=response+' : Please contact Administrator.'; 
                    system.debug('@@@'+response);
                    //if(cancelOrderResponse.ErrorCode.containsignorecase('110')){
                    throw new CancelNCOrderException(response);   
                }else {
                    response = '----Test Response----' ;
                    system.debug('TestResponse --->'+response);
                }       
                
        }    
        }
        
    }
    
     @remoteaction
    global static String vfrPrepareOrderCancellationRequest(string orderId, string reasonCode, string notes)
    {       
        Order ord = [Select change_type__c, reason_code__c,Status, General_Notes_Remarks__c,Credit_Assessment__r.CAR_Result__c,Credit_Assessment__r.CAR_Status__c,Credit_Assessment__c,Credit_Assessment__r.CAR_Code__c from Order where Id=:orderId];
        ord.Change_Type__c = 'Cancel Entire Order';
        ord.Reason_Code__c = reasonCode;
        ord.General_Notes_Remarks__c = notes;
        String previousStatus = ord.Status;
        /*system.debug('previousStatus '+previousStatus );
        if(string.isNotblank(previousStatus) && ('Activated'== previousStatus || 'Delivery'==previousStatus || 'Submitted'== previousStatus )){
            List<Asset> assetswithOrder=[select id,status from Asset where vlocity_cmt__OrderId__c=:orderId and status IN('Shipped','Installed')];
            if(null != assetswithOrder && assetswithOrder.size()>0) {
                apexpages.addmessage(new apexpages.message(apexpages.severity.error,'You cannot cancel this order.It has assets those are shipped or installed.'));
                return 'has assets';
            }
            return previousStatus;
        }else{
            return previousStatus;
        }*/
        return previousStatus;
    }
    
      @future(callout=true)
    public static void unreserveUsedTns(String orderId){
        System.debug('unreserveUsedTns---------->'+ orderId);
        List<String> tnList=OrdrUtilities.getUsedTns(orderId);
        System.debug('unreserveUsedTns---------->tnList:::'+ tnList);
        if(tnList.size()>0){
        OrdrTnReservationImpl.unreserveUsedTns(orderId,tnList);   
        }   
       
    }   
    
    public void updateOrderStatus() {  
        list<order> ordrList = [select OrderNumber, Status from Order where Id =: orderId]; 
        orderStatus = ordrList[0].Status != null? ordrList[0].Status:'';
        System.debug('orderId-------------->'+orderId);
        System.debug('orderStatus-------------->'+orderStatus);
    }
    
    
    @RemoteAction
  global static void setChoosenLanguageOnOrder( String orderId , String language){
      system.debug('orderId in setChoosenLanguageOnOrder() ------' + orderId );
      Order orderObj = [Select Id, Chosen_Language__c  from Order where  Id =: orderId];
      orderObj.Chosen_Language__c = language;
      
      update orderObj ;
      system.debug('orderObj in setChoosenLanguageOnOrder() ------' + orderObj );
      
  }
  
  
  @RemoteAction
  global static void callRemoteMethodForDefaultlanguage(String orderId){
      system.debug('orderId in callRemoteMethodForDefaultlanguage() ------' + orderId );
      Order orderObj = [Select Id, Chosen_Language__c, CustomerAuthorizedBy.Language_Preference__c  from Order where  Id =: orderId];
      
      orderObj.Chosen_Language__c = orderObj.CustomerAuthorizedBy.Language_Preference__c;
      update orderObj ;
      system.debug('orderObj in callRemoteMethodForDefaultlanguage() ------' + orderObj );
      
  }
}