/********************************************************************************************************************************
Class Name:     OC_CreateOrderController 
Purpose:        Controller for OC_CreateOrder Component       
Test class Name: OC_CreateOrderController_Test
Created by:     Aditya Jamwal(IBM)     20-June-2017     
*********************************************************************************************************************************
*/
global class OC_CreateOrderController {
    
  //public static String rid{get;set;}
  global static String customerSolutionId{get;set;} 
  
  @RemoteAction
    global static  string createSolutionOrder(String rcid, String orderType, String oppId){
        
        Map<string,Object> input = new Map<string,Object>();
        Map<string,Object> output = new Map<string,Object>();
        
        input.put('rcid',rcid);
        input.put('orderType',orderType);
        //RTA 617
        input.put('oppId',oppId);
        // end 617
        
        VlocityCPQUtil vlCPQutil= new VlocityCPQUtil();
        
        vlCPQutil.invokeMethod('createCustomerSolution', input, output, new Map<string,Object>());
        if(null != output.get('customerSolutionId')){
            return (String) output.get('customerSolutionId');
        }else if(null != output.get('error')){
            return (String) output.get('error');
        } 
        return null;
    }
    
   @RemoteAction
    global static  Object createCustomerOrders(List<VlocityCPQUtil.OrderWrapper> customerOrderList,string rcid){
        
        Map<string,Object> input = new Map<string,Object>();
        Map<string,Object> output = new Map<string,Object>();
        
        input.put('rcid',rcid);
        input.put('childOrders',customerOrderList);
        system.debug('childOrders '+customerOrderList);
        
        VlocityCPQUtil vlCPQutil= new VlocityCPQUtil();
        vlCPQutil.invokeMethod('createCustomerOrders', input, output, new Map<string,Object>());
        
        if(null != output && null != output.get('createOrderResultMap') ){
            return (Map<String,VlocityCPQUtil.OrderWrapper>)  output.get('createOrderResultMap');
        }else if(null != output && null != output.get('error')){
            return output.get('error'); 
        }     
        return null;
    }
    
   @RemoteAction
   global static  string createSolutionOrderAndCustomerOrders(List<VlocityCPQUtil.OrderWrapper> customerOrderList,string rcid, String orderType){
       
       Map<string,Object> input = new Map<string,Object>();
       Map<string,Object> output = new Map<string,Object>();
       
       input.put('rcid',rcid);
       input.put('childOrders',customerOrderList);
       
       system.debug('childOrders '+customerOrderList);
       
       VlocityCPQUtil vlCPQutil= new VlocityCPQUtil();
       vlCPQutil.invokeMethod('createCustomerSolutionAndCustomerOrders', input, output, new Map<string,Object>());
       
       if(null != output.get('customerSolutionId')){
           return (String) output.get('customerSolutionId');
       }else if(null != output.get('error')){
           return (String) output.get('error');
       }  
       return null;
    }
    
   @RemoteAction
   global static  Object deleteOrders(List<String> OrderIdSet){
       
        Map<string,Object> input = new Map<string,Object>();
        Map<string,Object> output = new Map<string,Object>();
      	
        input.put('deleteOrderIds',OrderIdSet);
        system.debug('deleteOrderIds '+OrderIdSet);
       
        VlocityCPQUtil vlCPQutil= new VlocityCPQUtil();
        vlCPQutil.invokeMethod('deleteCustomerOrders', input, output, new Map<string,Object>());  

        if(null != output && null != output.get('deleteOrderResult') ){
         Map<String,VlocityCPQUtil.OrderWrapper>  deleteOrderResultMap = (Map<String,VlocityCPQUtil.OrderWrapper>)  output.get('deleteOrderResult');
         system.debug('deleteOrderResultMap '+ deleteOrderResultMap);
        return deleteOrderResultMap;
        }else if(null != output && null != output.get('error')){
        return output.get('error'); 
        }
       return null;
    }
   
    // Start : BMPF-5
   @RemoteAction
   global static  Object  cancelOrders(List<String> OrderIdSet){
       
        Map<string,Object> input = new Map<string,Object>();
        Map<string,Object> output = new Map<string,Object>();
       
        input.put('cancelOrderIds',OrderIdSet);
        system.debug('cancelOrderIds '+ OrderIdSet);
       
        VlocityCPQUtil vlCPQutil= new VlocityCPQUtil();
        vlCPQutil.invokeMethod('cancelOrders', input, output, new Map<string,Object>());   
       
       if(null != output && null != output.get('cancelOrdersResult') ){
        Map<String,VlocityCPQUtil.OrderWrapper>  cancelOrderResultMap = (Map<String,VlocityCPQUtil.OrderWrapper>)  output.get('cancelOrdersResult');
        system.debug('cancelOrderResultMap '+ cancelOrderResultMap);
        return cancelOrderResultMap;
       }else if(null != output && null != output.get('error')){
        return output.get('error'); 
       } 
        return null;
    }
    // End : BMPF-5
    
   // Start : BMPF-44
   @RemoteAction
   global static  Object updateOrders(List<VlocityCPQUtil.OrderWrapper> orderList){
       
        Map<string,Object> input = new Map<string,Object>();
        Map<string,Object> output = new Map<string,Object>();
       
        input.put('orderList',orderList);
        system.debug('orderList '+ orderList);
       
        VlocityCPQUtil vlCPQutil= new VlocityCPQUtil();
        vlCPQutil.invokeMethod('updateOrders', input, output, new Map<string,Object>()); 
       
       if(null != output && null != output.get('updateOrdersResult') ){
           return (Map<String,VlocityCPQUtil.OrderWrapper>)  output.get('updateOrdersResult');           
       }else if(null != output && null != output.get('error')){
           return output.get('error'); 
       } 
       return null;
    }
   // End : BMPF-44
    
   // added method as per BMPF-19
   @RemoteAction
   global static  string checkServiceLocationsAndCreateSolutionOrder(String rcid){
        Map<string,Object> input = new Map<string,Object>();
        input.put('rcid',rcid);
        system.debug(' in select controller '+input);
        
        VlocityCPQUtil vlCPQutil= new VlocityCPQUtil();
        
        String customerSolutionId = (String) vlCPQutil.invokeMethod('checkServiceLocationsAndCreateSolutionOrder', input, new Map<string,Object>(), new Map<string,Object>());
        system.debug(' in select controller 1 '+customerSolutionId);    
        return customerSolutionId;
    }
}