@isTest
private class EmailNotificationForCustomStatusToolTest {
    
    private static testMethod void testEmailNotificationForGreenDistribution() {
        Segment__c seg = new Segment__c(
            Name = 'test',
            CBU_Abbreviated_Name__c = 'abrv_name',
            CBU_Code__c = 'aaa',
            CBU_Description__c = 'desc',
            CBU_Name__c = 'cbuName',
            CSH_Managing_Director__c = 'director',
            CSH_Segment_Name__c = 'csh_segment_name',
            CSH_Sub_Segment_Description__c = 'sub_seg_desc',
            Channel_Region__c = 'na',
            ILEC_Flag__c = 'flag',
            Marketing_Managing_Director__c = 'marketing_director',
            Marketing_Segment_Name__c = 'marketing_seg',
            Marketing_Sub_Segment_Description__c = 'marketing_sub_seg_desc',
            Marketing_Sub_Segment_Short_Description__c = 'marketing_sub_seg_short_desc'
        );
        insert seg;
        
        Cust_Business_Unit_Cust_ID__c c = new Cust_Business_Unit_Cust_ID__c(
            name = 'test',
            CBUCID__c = 'aaaaaaaaaa',
            CBU_Name__c = 'cbu_name',
            CBU_CD__c = 'aaa',
            Alt_Segment__c = 'test_alt_seg',
            Sub_Segment_Code__c = seg.id
        );
        insert c;
        
        AtoZ_CustStatusTool__c aToZ = new AtoZ_CustStatusTool__c(CBUCID__c = c.CBUCID__c);
        insert aToZ;
        
        Id PROFILEID = [SELECT id FROM Profile WHERE name='Standard User' LIMIT 1].id;
        
        User u = new User(
            username = Datetime.now().getTime() + '@TRACTIONSM.COM',
          email = 'employee@example.com',
          title = 'test',
          firstname = 'first',
          lastname = 'last',
          alias = 'test',
          TimezoneSIDKey = 'America/Los_Angeles',
          LocaleSIDKey = 'en_US',
          EmailEncodingKey = 'UTF-8',
          ProfileId = PROFILEID,
          LanguageLocaleKey = 'en_US'
      );
        insert u;
        
        Employee__c e = new Employee__c(
            Full_Name__c = 'first last',
            Email__c = 'employee@example.com',
            Manager_Email__c = 'manager@example.com',
            Director_Email__c = 'director@example.com',
            Employee_Id__c = '12345',
            user__c = u.id,
            TMODS_TEAM_MEMBER_ID__c = 98765
        );
        insert e;
        
        AtoZ_Assignment__c assign = new AtoZ_Assignment__c(
        CBUCID__c = c.id,
        AtoZ_Field__c = 'test_az_field',
        AtoZ_Contact__c = e.Id
      );
        insert assign; 
        
        Map<String, AtoZ_CustStatusTool__c> setCbucid = new Map<String, AtoZ_CustStatusTool__c>{c.CBUCID__c => aToZ};
        Map<String, String> previousColorStatus = new Map<String, String>();
        Set<String> mapModiName = new Set<String>{u.id};
        
        EmailNotificationForCustomStatusTool.emailNotificationForGreenDistribution(setCbucid, previousColorStatus, mapModiName);
    }
    
    private static testMethod void testEmailNotificationForGreenDistributionException() {
        Map<String, AtoZ_CustStatusTool__c> setCbucid;
        Map<String, String> previousColorStatus = new Map<String, String>();
        Set<String> mapModiName = new Set<String>();
        
        EmailNotificationForCustomStatusTool.emailNotificationForGreenDistribution(setCbucid, previousColorStatus, mapModiName);
    }
    
    private static testMethod void testEmailNotificationForYellowOrRedDistributionYellow() {
        Segment__c seg = new Segment__c(
            Name = 'test',
            CBU_Abbreviated_Name__c = 'abrv_name',
            CBU_Code__c = 'aaa',
            CBU_Description__c = 'desc',
            CBU_Name__c = 'cbuName',
            CSH_Managing_Director__c = 'director',
            CSH_Segment_Name__c = 'csh_segment_name',
            CSH_Sub_Segment_Description__c = 'sub_seg_desc',
            Channel_Region__c = 'na',
            ILEC_Flag__c = 'flag',
            Marketing_Managing_Director__c = 'marketing_director',
            Marketing_Segment_Name__c = 'marketing_seg',
            Marketing_Sub_Segment_Description__c = 'marketing_sub_seg_desc',
            Marketing_Sub_Segment_Short_Description__c = 'marketing_sub_seg_short_desc'
        );
        insert seg;
        
        Cust_Business_Unit_Cust_ID__c c = new Cust_Business_Unit_Cust_ID__c(
            name = 'test',
            CBUCID__c = 'aaaaaaaaaa',
            CBU_Name__c = 'cbu_name',
            CBU_CD__c = 'aaa',
            Alt_Segment__c = 'test_alt_seg',
            Sub_Segment_Code__c = seg.id
        );
        insert c;
        
        AtoZ_CustStatusTool__c aToZ = new AtoZ_CustStatusTool__c(CBUCID__c = c.CBUCID__c);
        insert aToZ;
        
        Employee__c e = new Employee__c(
            Full_Name__c = 'test',
            Email__c = 'employee@example.com',
            Manager_Email__c = 'manager@example.com',
            Director_Email__c = 'director@example.com',
            Employee_Id__c = '12345',
            TMODS_TEAM_MEMBER_ID__c = 98765
        );
        insert e;
        
        AtoZ_Assignment__c assign = new AtoZ_Assignment__c(
        CBUCID__c = c.id,
        AtoZ_Field__c = 'test_az_field',
        AtoZ_Contact__c = e.Id
      );
        insert assign; 
        
        Map<String, AtoZ_CustStatusTool__c> setCbucid = new Map<String, AtoZ_CustStatusTool__c>{c.CBUCID__c => aToZ};
        Map<String, String> previousColorStatus = new Map<String, String>();
        Set<String> mapModiName = new Set<String>();
        String status = 'Yellow';
        
        EmailNotificationForCustomStatusTool.emailNotificationForYellowOrRedDistribution(setCbucid, previousColorStatus, mapModiName, status);
    }
    
    private static testMethod void testEmailNotificationForYellowOrRedDistributionExceptionYellow() {
        Map<String, AtoZ_CustStatusTool__c> setCbucid;
        Map<String, String> previousColorStatus = new Map<String, String>();
        Set<String> mapModiName = new Set<String>();
        String status = 'Yellow';
        
        EmailNotificationForCustomStatusTool.emailNotificationForYellowOrRedDistribution(setCbucid, previousColorStatus, mapModiName, status);
    }
    
    private static testMethod void testEmailNotificationForYellowOrRedDistributionRed() {
        Segment__c seg = new Segment__c(
            Name = 'test',
            CBU_Abbreviated_Name__c = 'abrv_name',
            CBU_Code__c = 'aaa',
            CBU_Description__c = 'desc',
            CBU_Name__c = 'cbuName',
            CSH_Managing_Director__c = 'director',
            CSH_Segment_Name__c = 'csh_segment_name',
            CSH_Sub_Segment_Description__c = 'sub_seg_desc',
            Channel_Region__c = 'na',
            ILEC_Flag__c = 'flag',
            Marketing_Managing_Director__c = 'marketing_director',
            Marketing_Segment_Name__c = 'marketing_seg',
            Marketing_Sub_Segment_Description__c = 'marketing_sub_seg_desc',
            Marketing_Sub_Segment_Short_Description__c = 'marketing_sub_seg_short_desc'
        );
        insert seg;
        
        Cust_Business_Unit_Cust_ID__c c = new Cust_Business_Unit_Cust_ID__c(
            name = 'test',
            CBUCID__c = 'aaaaaaaaaa',
            CBU_Name__c = 'cbu_name',
            CBU_CD__c = 'aaa',
            Alt_Segment__c = 'test_alt_seg',
            Sub_Segment_Code__c = seg.id
        );
        insert c;
        
        AtoZ_CustStatusTool__c aToZ = new AtoZ_CustStatusTool__c(CBUCID__c = c.CBUCID__c);
        insert aToZ;
        
        Employee__c e = new Employee__c(
            Full_Name__c = 'test',
            Email__c = 'employee@example.com',
            Manager_Email__c = 'manager@example.com',
            Director_Email__c = 'director@example.com',
            Employee_Id__c = '12345',
            TMODS_TEAM_MEMBER_ID__c = 98765
        );
        insert e;
        
        AtoZ_Assignment__c assign = new AtoZ_Assignment__c(
        CBUCID__c = c.id,
        AtoZ_Field__c = 'test_az_field',
        AtoZ_Contact__c = e.Id
      );
        insert assign; 
        
        Map<String, AtoZ_CustStatusTool__c> setCbucid = new Map<String, AtoZ_CustStatusTool__c>{c.CBUCID__c => aToZ};
        Map<String, String> previousColorStatus = new Map<String, String>();
        Set<String> mapModiName = new Set<String>();
        String status = 'Red';
        
        EmailNotificationForCustomStatusTool.emailNotificationForYellowOrRedDistribution(setCbucid, previousColorStatus, mapModiName, status);
    }
    
    private static testMethod void testEmailNotificationForYellowOrRedDistributionExceptionRed() {
        Map<String, AtoZ_CustStatusTool__c> setCbucid;
        Map<String, String> previousColorStatus = new Map<String, String>();
        Set<String> mapModiName = new Set<String>();
        String status = 'Red';
        
        EmailNotificationForCustomStatusTool.emailNotificationForYellowOrRedDistribution(setCbucid, previousColorStatus, mapModiName, status);
    }
    
    private static testMethod void testSendEmail() {
        Map<String, Set<String>> mapEmailList = new Map<String, Set<String>>();
        Map<String, String> previousColorStatus = new Map<String, String>();
        Map<String, AtoZ_CustStatusTool__c> mapCustomTool = new Map<String, AtoZ_CustStatusTool__c>();
        Map<String, Map<String, String>> mapWlnWlsDetail = new Map<String, Map<String, String>>();
        Set<String> mapModiName = new Set<String>();
        Set<String> emailListForAddEmpYellowOrRed = new Set<String>();

        EmailNotificationForCustomStatusTool.sendEmail(mapEmailList, previousColorStatus, mapCustomTool, mapWlnWlsDetail, mapModiName, emailListForAddEmpYellowOrRed);
    }
    
    private static testMethod void testFindWlsWlnUserName() {
        Account a = new Account(name = 'test', cbucid__c = 'aaaaaaaaaa');
        insert a;
        
        Segment__c seg = new Segment__c(
            Name = 'test',
            CBU_Abbreviated_Name__c = 'abrv_name',
            CBU_Code__c = 'aaa',
            CBU_Description__c = 'desc',
            CBU_Name__c = 'cbuName',
            CSH_Managing_Director__c = 'director',
            CSH_Segment_Name__c = 'csh_segment_name',
            CSH_Sub_Segment_Description__c = 'sub_seg_desc',
            Channel_Region__c = 'na',
            ILEC_Flag__c = 'flag',
            Marketing_Managing_Director__c = 'marketing_director',
            Marketing_Segment_Name__c = 'marketing_seg',
            Marketing_Sub_Segment_Description__c = 'marketing_sub_seg_desc',
            Marketing_Sub_Segment_Short_Description__c = 'marketing_sub_seg_short_desc'
        );
        insert seg;
        
        Cust_Business_Unit_Cust_ID__c c = new Cust_Business_Unit_Cust_ID__c(
            name = 'test',
            CBUCID__c = 'aaaaaaaaaa',
            CBU_Name__c = 'cbu_name',
            CBU_CD__c = 'aaa',
            Alt_Segment__c = 'test_alt_seg',
            Sub_Segment_Code__c = seg.id
        );
        insert c;
        
        AtoZ_CustStatusTool__c aToZ = new AtoZ_CustStatusTool__c(CBUCID__c = c.CBUCID__c);
        insert aToZ;
        
        Sales_Assignment__c sa = new Sales_Assignment__c(Account__c = a.id, Account__r = a, AcctRole__c = 'test');
        insert sa;
        
        List<Sales_Assignment__c> listEmailToWlsAndWlnMgrDir = new List<Sales_Assignment__c>{sa};
        Map<String, Map<String, String>> mapWlnWlsDetailForGreen = new Map<String, Map<String, String>>();
        Map<String, AtoZ_CustStatusTool__c> mapCustomTool = new Map<String, AtoZ_CustStatusTool__c>{c.CBUCID__c => aToZ};
        
        Map<String, Map<String, String>> results =
            EmailNotificationForCustomStatusTool.findWlsWlnUserName(listEmailToWlsAndWlnMgrDir, mapWlnWlsDetailForGreen, mapCustomTool);
    }
    
    private static testMethod void testFindWlsWinUserEmailFromUserGreen() {
        Account a = new Account(name = 'test', cbucid__c = 'aaaaaaaaaa');
        insert a;
        
        Sales_Assignment__c sa = new Sales_Assignment__c(Account__c = a.id, Account__r = a, AcctRole__c = 'test');
        insert sa;
        
        
        List<Sales_Assignment__c> listEmailToWlsAndWlnMgrDir = new List<Sales_Assignment__c>{sa};
        Map<String, Set<String>> mapEmailListForYellowOrRed = new Map<String, Set<String>>();
        String strColor = 'Green';
        
        Map<String, Set<String>> results =
            EmailNotificationForCustomStatusTool.findWlsWlnUserEmailFromUser(listEmailToWlsAndWlnMgrDir, mapEmailListForYellowOrRed, strColor);
    }
    
    private static testMethod void testFindWlsWinUserEmailFromUserYellow() {
        Account a = new Account(name = 'test', cbucid__c = 'aaaaaaaaaa');
        insert a;
        
        Sales_Assignment__c sa = new Sales_Assignment__c(Account__c = a.id, Account__r = a, AcctRole__c = 'test');
        insert sa;
        
        
        List<Sales_Assignment__c> listEmailToWlsAndWlnMgrDir = new List<Sales_Assignment__c>{sa};
        Map<String, Set<String>> mapEmailListForYellowOrRed = new Map<String, Set<String>>();
        String strColor = 'Yellow';
        
        Map<String, Set<String>> results =
            EmailNotificationForCustomStatusTool.findWlsWlnUserEmailFromUser(listEmailToWlsAndWlnMgrDir, mapEmailListForYellowOrRed, strColor);
    }
    
    private static testMethod void testFindWlsWinUserEmailFromUserRed() {
        Account a = new Account(name = 'test', cbucid__c = 'aaaaaaaaaa');
        insert a;
        
        Segment__c seg = new Segment__c(
            Name = 'test',
            CBU_Abbreviated_Name__c = 'abrv_name',
            CBU_Code__c = 'aaa',
            CBU_Description__c = 'desc',
            CBU_Name__c = 'cbuName',
            CSH_Managing_Director__c = 'director',
            CSH_Segment_Name__c = 'csh_segment_name',
            CSH_Sub_Segment_Description__c = 'sub_seg_desc',
            Channel_Region__c = 'na',
            ILEC_Flag__c = 'flag',
            Marketing_Managing_Director__c = 'marketing_director',
            Marketing_Segment_Name__c = 'marketing_seg',
            Marketing_Sub_Segment_Description__c = 'marketing_sub_seg_desc',
            Marketing_Sub_Segment_Short_Description__c = 'marketing_sub_seg_short_desc'
        );
        insert seg;
        
        Cust_Business_Unit_Cust_ID__c c = new Cust_Business_Unit_Cust_ID__c(
            name = 'test',
            CBUCID__c = 'aaaaaaaaaa',
            CBU_Name__c = 'cbu_name',
            CBU_CD__c = 'aaa',
            Alt_Segment__c = 'test_alt_seg',
            Sub_Segment_Code__c = seg.id
        );
        insert c;
        
        Sales_Assignment__c sa = new Sales_Assignment__c(Account__c = a.id, Account__r = a, AcctRole__c = 'test');
        insert sa;
        
        
        List<Sales_Assignment__c> listEmailToWlsAndWlnMgrDir = new List<Sales_Assignment__c>{sa};
        Map<String, Set<String>> mapEmailListForYellowOrRed = new Map<String, Set<String>>{c.CBUCID__c => new Set<String>()};
        String strColor = 'Red';
        
        Map<String, Set<String>> results =
            EmailNotificationForCustomStatusTool.findWlsWlnUserEmailFromUser(listEmailToWlsAndWlnMgrDir, mapEmailListForYellowOrRed, strColor);
    }
    
    private static testMethod void testFindWlsWinUserEmailFromEmployee() {
        Segment__c seg = new Segment__c(
            Name = 'test',
            CBU_Abbreviated_Name__c = 'abrv_name',
            CBU_Code__c = 'aaa',
            CBU_Description__c = 'desc',
            CBU_Name__c = 'cbuName',
            CSH_Managing_Director__c = 'director',
            CSH_Segment_Name__c = 'csh_segment_name',
            CSH_Sub_Segment_Description__c = 'sub_seg_desc',
            Channel_Region__c = 'na',
            ILEC_Flag__c = 'flag',
            Marketing_Managing_Director__c = 'marketing_director',
            Marketing_Segment_Name__c = 'marketing_seg',
            Marketing_Sub_Segment_Description__c = 'marketing_sub_seg_desc',
            Marketing_Sub_Segment_Short_Description__c = 'marketing_sub_seg_short_desc'
        );
        insert seg;
        
        Cust_Business_Unit_Cust_ID__c c = new Cust_Business_Unit_Cust_ID__c(
            name = 'test',
            CBUCID__c = 'aaaaaaaaaa',
            CBU_Name__c = 'cbu_name',
            CBU_CD__c = 'aaa',
            Alt_Segment__c = 'test_alt_seg',
            Sub_Segment_Code__c = seg.id
        );
        insert c;
        
        Employee__c e = new Employee__c(
            Full_Name__c = 'test',
            Email__c = 'employee@example.com',
            Manager_Email__c = 'manager@example.com',
            Director_Email__c = 'director@example.com',
            Employee_Id__c = '12345',
            TMODS_TEAM_MEMBER_ID__c = 98765
        );
        insert e;
        
        AtoZ_Assignment__c assign = new AtoZ_Assignment__c(
        CBUCID__r = c,
        CBUCID__c = c.id,
        AtoZ_Field__c = 'test_az_field',
        AtoZ_Contact__c = e.Id
      );
        insert assign; 
        
        List<AtoZ_Assignment__c> listEmailToWlsAndWlnMgrDir = new List<AtoZ_Assignment__c>{assign};
        Map<String, Set<String>> mapEmailListForYellowOrRed = new Map<String, Set<String>>{c.CBUCID__c => new Set<String>()};
        
        Map<String, Set<String>> results =
            EmailNotificationForCustomStatusTool.findWlsWlnUserEmailFromEmployee(listEmailToWlsAndWlnMgrDir, mapEmailListForYellowOrRed);
    }
}