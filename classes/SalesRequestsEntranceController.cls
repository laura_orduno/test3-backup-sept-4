/**
Sales Requests Central.  Sales users submits multiple types of sales request to Care reps
to process such as Support, Contract, and Escalation.
*/
public with sharing class SalesRequestsEntranceController {
    public string requestType{get;set;}
    public list<contact> contacts{get;set;}
    public list<contact> multipleMe{get;set;}
    public string retURL{get;set;}
    public string selectedRecordType{get;set;}
    public string caseType{get;set;}
    public string subTypeFieldId{get;set;}
    public string subTypeValue{get;set;}
    public string fieldId{get;set;}
    public string productImpactedId{get;set;}
    public string productImpactedValue{get;set;}
    public string searchText{get;set;}
    public string accountId{get;set;}
    string fieldSend = '';
    static final map<string,string> iconRecordTypeMap=new map<string,string>{'Wireline/Voice/ADSL'=>'SMB Care TELUS Service','Wireless Billing Dispute'=>'SMB Care Billing','PRI/Data Expedites'=>'SMB Care TELUS Service','Held Orders'=>'SMB Care TELUS Service',
        'Billing'=>'SMB Care Billing','TBO'=>'SMB Care Billing','Expedite'=>'SMB Care TELUS Service','General'=>'SMB Care TELUS Service','Order'=>'SMB Care TELUS Service','Payment'=>'SMB Care TELUS Service'};
    static final map<string,string> iconCaseTypeMap=new map<string,string>{'Wireline/Voice/ADSL'=>'Move/Add/Change','Wireless Billing Dispute'=>'Billing Dispute','PRI/Data Expedites'=>'Move/Add/Change','Held Orders'=>'New'};
    static final map<string,string> iconProductAffectedMap=new map<string,string>{'Wireline/Voice/ADSL'=>'Voice','Wireless Billing Dispute'=>'Wireless','PRI/Data Expedites'=>'Data/Enhanced','Held Orders'=>'Voice'};
    public SalesRequestsEntranceController(){
        init();
    }
    void init(){
        caseType=apexpages.currentPage().getparameters().get('RequestType');
        retURL=apexpages.currentpage().getparameters().get('retURL');
        accountId=apexpages.currentpage().getparameters().get('AccountId');
        selectedRecordType='';
        productImpactedId='';
        if(string.isnotblank(caseType)){
            map<string,schema.recordtypeinfo> caseRecordTypeInfo=case.sobjecttype.getdescribe().getrecordtypeinfosbyname();
            system.debug('Selected Record Type:'+caseType);
            for(schema.recordtypeinfo info:caseRecordTypeInfo.values()){
                system.debug('record type:'+info.getName());
            }
            if(caseType.equalsignorecase('Profiler')||caseType.equalsignorecase('TLC')
            ||caseType.equalsignorecase('eBAM')||caseType.equalsignorecase('LBCO Calculator')){
                requestType='SMB Contracts/SRT';
            }else if(caseType.equalsignorecase('Voice or Data Billing')){
                requestType='SMB Care Billing';
                // This is done because SMB Care Billing Support doesn't preset the Type drop down, but it pre-check the Inuksuk Billing checkbox
                Inuksuk_Case_Field__c caseField=Inuksuk_Case_Field__c.getall().get('Inuksuk_Billing__c');
                fieldId=caseField.field_id__c;
            }else{
                requestType=caseType;
            }
            if(caseRecordTypeInfo.containskey(requestType)){
                selectedRecordType=caseRecordTypeInfo.get(requestType).getrecordtypeid();
            }else{
                if(iconRecordTypeMap.containskey(caseType)){
                    selectedRecordType=caseRecordTypeInfo.get(iconRecordTypeMap.get(requestType)).getrecordtypeid();
                }
                if(caseType.equalsignorecase('Billing')){
                    caseType='Billing Interpretation';
                }else if(caseType.equalsignorecase('Expedite')){
                    caseType='Escalation Status';
                }else if(caseType.equalsignorecase('General')){
                    caseType='Other';
                }else if(caseType.equalsignorecase('Order')){
                    caseType='Order Change';
                }else if(caseType.equalsignorecase('Payment')){
                    caseType='Payment';
                }else if(caseType.equalsignorecase('TBO')){
                    caseType='TBO';
                }else{
                    if(caseType.equalsignorecase('Held Orders')){
                        Inuksuk_Case_Field__c caseField=Inuksuk_Case_Field__c.getall().get('Subtype__c');
                        subTypeFieldId=caseField.field_id__c;
                        subTypeValue='Held Order';
                    }
                    if(iconProductAffectedMap.containskey(caseType)){
                        Inuksuk_Case_Field__c productImpactedField=Inuksuk_Case_Field__c.getall().get('Product_Affected__c');
                        productImpactedId=productImpactedField.field_id__c;
                        productImpactedValue=iconProductAffectedMap.get(caseType);
                    }
                    if(iconCaseTypeMap.containskey(caseType)){
                        caseType=iconCasetypeMap.get(caseType);
                    }
                }
            }
        }else{
            requestType='';
        }
        //contacts=[select id,name,firstname,lastname,accountid,account.name,email,title,active__c,account.recordtype.name,rcid__c from contact where active__c=true limit 10];
        contacts=new list<contact>();
        if(string.isnotblank(accountId)){
            contacts=database.query('select id,firstname,lastname,phone,account.recordtype.name,rcid__c,email,title,accountid,active__c,account.name from contact where accountid=:accountId limit 100');
        }
        multipleMe=new list<contact>();
    }
    public pagereference selectRequest(){
        pagereference returnPage=null;
        if(string.isnotblank(requestType)){
            if(requestType.equalsIgnoreCase('createcontract')){
                returnPage=Page.SalesRequestsAccountContactSelection;
                returnPage.getParameters().put('RequestType',requestType);
            }
        }
        return returnPage;
    }
    public pagereference searchAccountContact(){
        if(string.isnotblank(searchText)){
            if(contacts!=null){
                contacts.clear();
            }else{
                contacts=new list<contact>();
            }
            List<List<SObject>>searchList= [FIND :searchText in all fields returning Account(id limit 100),
                                            contact (firstname,lastname, Id,account.recordtype.name,rcid__c,Phone, account.name, accountId,active__c, email,title order by firstname limit 100)];
            list<account> searchedAccounts=(list<account>)searchList.get(0);
            list<contact> searchedContacts=(list<contact>)searchList.get(1);
            if(!searchedContacts.isempty()){
                contacts.addall(searchedContacts);
            }
            if(!searchedAccounts.isempty()){
                list<contact> accountContacts=database.query('select id,firstname,lastname,phone,account.recordtype.name,rcid__c,email,title,accountid,active__c,account.name from contact where accountid!=:searchedAccounts and id!=:searchedContacts limit 100');
                if(!accountContacts.isempty()){
                    contacts.addall(accountContacts);
                }
            }           
        }else{
            apexpages.addmessage(new apexpages.message(apexpages.severity.error,'Search text is empty.'));
        }
        return null;
    }
    /**
    Search for Telus Internal Contact for current User, and use it as Case's Contact.
    */
    public pagereference useMyTelusInternalContact(){
        string url='';
        pagereference nextPage=null;
        list<contact> myInternalContacts=database.query('select id,name,email,phone,title,active__c,accountid from contact where account.name=\'TELUS - Internal\' and account.recordtype.name=\'RCID\' and ownerid=\''+userinfo.getuserid()+'\' and firstname=\''+userinfo.getFirstName()+'\' and lastname=\''+userinfo.getlastname()+'\'');
        if(myInternalContacts.isempty()){
            //TODO - Create new internal contact
            //contact myInternalContact=new contact(firstname=userinfo.getfirstname(),lastname=userinfo.getlastname());
        }else if(myInternalContacts.size()==1){
            //TODO - Use this contact
            url=buildCaseCreationURL(selectedRecordType,myInternalContacts.get(0).id,myInternalContacts.get(0).accountid);
            nextPage=new pagereference(url);
        }else{
            multipleMe.clear();
            multipleMe.addall(myInternalContacts);
            nextPage=null;
        }
        return nextPage;
    }
    string buildCaseCreationURL(string recordTypeId,id contactId,id accountid){
        if(caseType.equalsignorecase('Billing')){
            caseType='Billing Interpretation';
        }else if(caseType.equalsignorecase('Expedite')){
            caseType='Escalation Status';
        }else if(caseType.equalsignorecase('General')){
            caseType='Other';
        }else if(caseType.equalsignorecase('Order')){
            caseType='Order Change';
        }else if(caseType.equalsignorecase('Payment')){
            caseType='Payment';
        }else if(caseType.equalsignorecase('TBO')){
            caseType='TBO';
        }else{
            if(caseType.equalsignorecase('Held Orders')){
                Inuksuk_Case_Field__c caseField=Inuksuk_Case_Field__c.getall().get('Subtype__c');
                subTypeFieldId=caseField.field_id__c;
                subTypeValue='Held Order';
            }
            system.debug('case type:'+caseType);
            if(iconProductAffectedMap.containskey(caseType)){
                Inuksuk_Case_Field__c productImpactedField=Inuksuk_Case_Field__c.getall().get('Product_Affected__c');
                productImpactedId=productImpactedField.field_id__c;
                productImpactedValue=iconProductAffectedMap.get(caseType);
            }
            if(iconCaseTypeMap.containskey(caseType)){
                caseType=iconCasetypeMap.get(caseType);
            }
        }
        if(caseType=='ENT Care Assure')
        {
            fieldSend = '&cas11=EMT Request&cas14=Escalation Request to EMT&';
        }
        
        string url='/500/e?'+(string.isnotblank(recordTypeId)?'RecordType='+recordTypeId+'&':'')+
        fieldSend +'&ent=Case&def_contact_id='+contactId+'&def_account_id='+accountid+'&retURL=%2F'+contactId+'&'+(string.isblank(fieldId)?'cas5':fieldId)+'='+
            (caseType.equalsignorecase('Voice or Data Billing')?'1':caseType+(string.isnotblank(subTypeValue)?'&'+subTypeFieldId+'='+subTypeValue:'')+(string.isnotblank(productImpactedId)?'&'+productImpactedId+'='+productImpactedValue:''));
        system.debug('String URL:'+url);
        return url;
    }
}