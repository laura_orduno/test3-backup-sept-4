/**
 * S2C_SearchCSIDBTNCtlr
 * @description Searchs and displays the results for either BTN, CAN, or CSID.
 * @author Thomas Tran, Traction on Demand
 * @date 06-10-2015
 */
public with sharing class S2C_SearchCSIDBTNCtlr {
    private static final String BTN                              = 'BTN';
    private static final String CSID                             = 'CSID';
    private static final String SERVICE_REQUEST_ID               = 'SRID';
    private static final String SERVICE_REQUEST_RECORD_TYPE_NAME = 'SRRTNAME';
    private static final String INSTALLPROVIDE                   = 'Provide/Install';
    private static final String CHANGE                           = 'Change';
    private static final String DISCONNECT                       = 'Disconnect';
    private static final String SR_ACCOUNT_ID                    = 'ACCTID';
    private static final String NONE = '--NONE--';
    public String searchValue                                    {get; set;}
    public String criteriaValue                                  {get; set;}
    public String btnSearchOption                                {get; set;}
    public List<BTNCAN> btnCanResults                            {get; set;}
    public String serviceId                                      {get; set;}
    public String srAcctId                                       {get; set;}

    public String demarcationLocation {get; set;}
    public String endCustomerName {get; set;}
    public String npaNXX {get; set;}
    public String location {get; set;}
    public String cli {get; set;}
    public String existingWorkingTN {get; set;}
    public String siteReady {get; set;}
    public String buildingName {get; set;}
    public String newBuidling {get; set;}
    public String floor {get; set;}
    public String cabinet {get; set;}
    public String rack {get; set;}
    public String shelf {get; set;}
    public String postalCode {get; set;}

    public Boolean isRecordSelected {get; set;}
    public Boolean isServiceLocation {get; set;}
    public String checkBoxStatus {get; set;}
    public String hasServiceLocationStatus {get; set;}
    public String srRecordTypeName {get; set;}

    public S2C_SearchCSIDBTNCtlr() {
        this.serviceId = ApexPages.currentPage().getParameters().get(SERVICE_REQUEST_ID);
        this.srRecordTypeName = ApexPages.currentPage().getParameters().get(SERVICE_REQUEST_RECORD_TYPE_NAME);
        srAcctId = ApexPages.currentPage().getParameters().get(SR_ACCOUNT_ID);
        btnCanResults = new List<BTNCAN>();
        btnSearchOption = 'false';
        checkBoxStatus = 'false';
        postalCode = '';
        existingWorkingTN = '';
    }

    /**
     * queryBTNCANInformation
     * @description Query the results based on CAN or BAN
     * @author Thomas Tran, Traction on Demand
     * @date 06-10-2015
     */
    public void queryBTNCANInformation(){
        List<Account> accountResults;
        btnCanResults = new List<BTNCAN>();

        //String queryCriteria = '%' + criteriaValue + '%';
        String queryCriteria = criteriaValue + '%';

        if(Boolean.valueOf(btnSearchOption)){
            accountResults = [SELECT Name, Operating_Name_DBA__c, Correct_Legal_Name__c, BillingStreet, BIllingCity, BillingState, BillingCountry, BillingPostalCode, BTN__c, CAN__c, BAN_CAN__c,
                                (SELECT Civic_Number__c, Street__c, Unit_Number__c, Street_Address__c, City__c, State__c, Postal_Code__c FROM Addresses1__r LIMIT 1) 
                              FROM Account 
                              WHERE Billing_Account_Active_Indicator__c = 'Y' AND BTN__c LIKE :queryCriteria
                              LIMIT 100];
        } else{
            accountResults = [SELECT Name, Operating_Name_DBA__c, Correct_Legal_Name__c, BillingStreet, BIllingCity, BillingState, BillingCountry, BillingPostalCode, BTN__c, CAN__c, BAN_CAN__c,
                                (SELECT Civic_Number__c, Street__c, Unit_Number__c, Street_Address__c, City__c, State__c, Postal_Code__c FROM Addresses1__r LIMIT 1) 
                              FROM Account 
                              WHERE Billing_Account_Active_Indicator__c = 'Y' AND CAN__c LIKE :queryCriteria
                              LIMIT 100];
        }

        if(!accountResults.isEmpty()){
            List<SMBCare_Address__c> accountSMBAddresses;

            String btnValue = '';
            String canValue = '';
            String bancanValue = '';
            String legalName = '';
            String tradeName = '';
            SMBCare_Address__c tempSMBAddressValues;
            Id smbAddressId;

            for(Account currentAccountTemp : accountResults){
                String serviceLocationValues = '';
                String billingAddressValues = '';

                accountSMBAddresses = currentAccountTemp.Addresses1__r;

                billingAddressValues += stringNullCheck(currentAccountTemp.Name) + '\n' +
                                        stringNullCheck(currentAccountTemp.BillingStreet) + '\n' +
                                        stringNullCheck(currentAccountTemp.BIllingCity) + ' ' + stringNullCheck(currentAccountTemp.BillingState) + '\n' + 
                                        stringNullCheck(currentAccountTemp.BillingCountry) + ', ' + stringNullCheck(currentAccountTemp.BillingPostalCode);

                btnValue = currentAccountTemp.BTN__c;
                canValue = currentAccountTemp.CAN__c;
                bancanValue = currentAccountTemp.BAN_CAN__c;
                legalName = currentAccountTemp.Correct_Legal_Name__c;
                tradeName = currentAccountTemp.Operating_Name_DBA__c;

                if(!accountSMBAddresses.isEmpty()){

                    for(SMBCare_Address__c currentSMBCareAddy :accountSMBAddresses){
                        smbAddressId = null;
                        serviceLocationValues = '';

                        smbAddressId = currentSMBCareAddy.Id;
                        serviceLocationValues = stringNullCheck(currentSMBCareAddy.Unit_Number__c) + ' ' + stringNullCheck(currentSMBCareAddy.Street_Address__c) + '\n' +
                                                 stringNullCheck(currentSMBCareAddy.City__c) + '\n' +
                                                 stringNullCheck(currentSMBCareAddy.State__c) + ' ' + stringNullCheck(currentSMBCareAddy.Postal_Code__c);

                        btnCanResults.add(new BTNCAN(false, billingAddressValues, serviceLocationValues, btnValue, canValue, currentAccountTemp.Id, smbAddressId, bancanValue, tradeName, legalName, currentSMBCareAddy.Postal_Code__c, currentSMBCareAddy.State__c));
                    }


                } else{
                    btnCanResults.add(new BTNCAN(false, billingAddressValues, serviceLocationValues, btnValue, canValue, currentAccountTemp.Id, smbAddressId, bancanValue, tradeName, legalName, '', ''));
                }

                
            }
        }
    }

    /**
     * checkboxCheck
     * @description Converts 
     * @author Thomas Tran, Traction on Demand
     * @date 07-14-2015
     */
    public void checkboxCheck(){
        isRecordSelected = Boolean.valueOf(checkBoxStatus);
        isServiceLocation = Boolean.valueOf(hasServiceLocationStatus);
    }

    /**
     * getSiteReadyValues
     * @description Queries all the picklist values.
     * @author Thomas Tran
     * @date 07-14-2015
     */
    public List<SelectOption> getSiteReadyValues(){
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = SRS_Service_Address__c.Site_Ready__c.getDescribe();
        
        options.add(new SelectOption('', NONE));

        for(Schema.PicklistEntry currentOptionTemp : fieldResult.getPicklistValues()){
            options.add(new SelectOption(currentOptionTemp.getLabel(), currentOptionTemp.getValue()));
        }

        return options;
    }

    /**
     * getNewBuildingValues
     * @description Queries all the picklist values.
     * @author Thomas Tran
     * @date 07-14-2015
     */
    public List<SelectOption> getNewBuildingValues(){
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = SRS_Service_Address__c.New_Building__c.getDescribe();
        
        options.add(new SelectOption('', NONE));

        for(Schema.PicklistEntry currentOptionTemp : fieldResult.getPicklistValues()){
            options.add(new SelectOption(currentOptionTemp.getLabel(), currentOptionTemp.getValue()));
        }

        return options;
    }

     /**
     * getLocationValues
     * @description Queries all the picklist values.
     * @author Thomas Tran
     * @date 07-14-2015
     */
    public List<SelectOption> getLocationValues(){
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = SRS_Service_Address__c.Location__c.getDescribe();
        
        options.add(new SelectOption('', NONE));

        for(Schema.PicklistEntry currentOptionTemp : fieldResult.getPicklistValues()){
            options.add(new SelectOption(currentOptionTemp.getLabel(), currentOptionTemp.getValue()));
        }

        return options;
    }

    /**
     * stringNullCheck
     * @description CHecks if the string is null otherwise return the original value
     * @author Thomas Tran, Traction on Demand
     * @date 06-18-2015
     */
    public String stringNullCheck(String currentString){
        return (String.isNotBlank(currentString)) ? currentString : '';
    }

    /**
     * resetSearchResults
     * @description Resets the search results to a new search state.
     * @author Thomas Tran, Traction on Demand
     * @date 06-11-2015
     */
    public void resetSearchResults(){
        btnCanResults = new List<BTNCAN>();
    }

    /**
     * getSearchValues
     * @description Prepopulates the picklist for search options.
     * @author Thomas Tran, Traction on Demand
     * @date 06-10-2015
     */
    public List<SelectOption> getSearchValues(){
        List<SelectOption> options = new List<SelectOption>();

        options.add(new SelectOption('', NONE));
        options.add(new SelectOption(BTN, 'BTN/CAN'));
        options.add(new SelectOption(CSID, CSID));

        return options;
    }

    /**
     * associateToSR
     * @description Updates the SR record with the proper values selected
     * @author Thomas Tran, Traction on Demand
     * @date 06-12-2015
     */
    public PageReference associateToSR(){
        BTNCAN currentBTNCAN;

        for(BTNCAN currentBTNCANTemp : btnCanResults){
            if(currentBTNCANTemp.isChecked){
                currentBTNCAN = currentBTNCANTemp;
                break;
            }
        }

        if(currentBTNCAN != null){

            try{
                Service_Request__c existingSR;

                if(srRecordTypeName.equals(INSTALLPROVIDE)){
                    existingSR = new Service_Request__c(
                        Id = serviceId,
                        Billing_Address__c = currentBTNCAN.billingAddress,
                        Billing_Account__c = currentBTNCAN.acctId
                    );
                } else if(srRecordTypeName.equals(CHANGE) || srRecordTypeName.equals(DISCONNECT)){
                    if(String.isNotBlank(currentBTNCAN.smbAddressId)){
                        
                        SMBCare_Address__c existingSMBAddy = [SELECT Civic_Number__c, Street__c, Type__c, Unit_Number__c, Street_Address__c, City__c, State__c, Postal_Code__c, SRS_Service_Location__c, NPA_NXX__c, Province__c, Address_Type__c, Suite_Number__c, Street_Number__c, Street_Name__c FROM SMBCare_Address__c WHERE Id = :currentBTNCAN.smbAddressId];
                        SMBCare_Address__c nextSMBCareAddress = existingSMBAddy.clone(false, true, false, false);

                        nextSMBCareAddress.Account__c = srAcctId;
                        nextSMBCareAddress.Address_Type__c = 'Standard';
                        nextSMBCareAddress.Province__c = nextSMBCareAddress.State__c;
                        nextSMBCareAddress.Suite_Number__c = nextSMBCareAddress.Unit_Number__c;
                        nextSMBCareAddress.Street_Number__c = nextSMBCareAddress.Civic_Number__c;
                        nextSMBCareAddress.Street_Name__c = nextSMBCareAddress.Street__c;
                        nextSMBCareAddress.Postal_Code__c = nextSMBCareAddress.Postal_Code__c;
                        nextSMBCareAddress.City__c = nextSMBCareAddress.City__c;
                        nextSMBCareAddress.Postal_Code__c = postalCode;

                        insert nextSMBCareAddress;
                        System.debug('Province :::: ' + nextSMBCareAddress.Province__c );
                        SYstem.debug('Number of Queries After New SMB CARE Address Insert :::: ' + Limits.getQueries());
                        SYstem.debug('Number of DML After New SMB CARE Address Insert :::: ' + Limits.getDmlStatements());
                        

                        SRS_Service_Address__c newServiceAddress = new SRS_Service_Address__c(
                            Service_Request__c = serviceId,
                            Address__c = nextSMBCareAddress.Id,
                            Demarcation_Location__c = demarcationLocation,
                            NPA_NXX__c = npaNXX,
                            Floor__c = floor,
                            Location__c = (String.isNotBlank(location)) ? location : 'Location A',
                            New_Building__c = newBuidling,
                            Rack__c = rack,
                            Shelf__c = shelf,
                            Site_Ready__c = siteReady,
                            CLLI__c = cli,
                            Cabinet__c = cabinet,
                            Building_Name__c = buildingName,
                            Existing_Working_TN__c = existingWorkingTN,
                            End_Customer_Name__c = endCustomerName
                        );

                        SYstem.debug('newServiceAddress ::::' + newServiceAddress);

                        insert newServiceAddress;
                        SYstem.debug('Number of Queries After New Service Insert :::: ' + Limits.getQueries());
                        SYstem.debug('Number of DML After New Service Insert :::: ' + Limits.getDmlStatements());
                    }

                    existingSR = new Service_Request__c(
                        Id = serviceId,
                        Billing_Address__c = currentBTNCAN.billingAddress,
                        Billing_Account__c = currentBTNCAN.acctId
                    );


                    existingSR.Service_Identifier_Type__c = BTN;
                    existingSR.Existing_Service_ID__c = currentBTNCAN.btnValue;
                    existingSR.Billing_Account__c = currentBTNCAN.acctId;


                }
                update existingSR;
                SYstem.debug('Number of Queries After SR Update :::: ' + Limits.getQueries());
                SYstem.debug('Number of DML After SR Update :::: ' + Limits.getDmlStatements());

                if(srRecordTypeName.equals(DISCONNECT)){
                    SYstem.debug('Executing');
                    updateRelatedSRs(existingSR);
                }


            } catch(Exception ex){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,ex.getMessage()));
                return null;
            }
        } else{
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select one of the results returned.'));
            return null;
        }

        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Service Request Record has been updated'));
        return null;
    }

    /**
     * updateRelatedSRs
     * @description Finds additional SR associated to the Disconnect SR
     * @author Thomas Tran, Traction on Demand
     * @date 08-05-2015
     */
    public static void updateRelatedSRs(Service_Request__c currentSR){
        try{
        Service_Request__c existingSR = [SELECT Id, Opportunity__c, Name FROM Service_Request__c WHERE Id = :currentSR.Id];
        List<Service_Request__c> srsToUpdate = new List<Service_Request__c>();

            Map<String, SRS_Service_Request_Related_Order__c> possibleMatch = new Map<String, SRS_Service_Request_Related_Order__c>();

            for(SRS_Service_Request_Related_Order__c currentSRROTemp : [SELECT Name, Reference_Number__c, Reference_Type__c, Comments__c, RO_Service_Request__c, RO_Service_Request__r.Name, RO_Service_Request__r.SRS_PODS_Product__r.Name
                                                                        FROM SRS_Service_Request_Related_Order__c 
                                                                        WHERE RO_Service_Request__c =:existingSR.Id 
                                                                                AND RecordTypeId IN (SELECT Id 
                                                                                                      FROM RecordType 
                                                                                                      WHERE SObjectType = 'SRS_Service_Request_Related_Order__c' 
                                                                                                            AND Name IN ('Reference Number'))]){
                possibleMatch.put(currentSRROTemp.Name, currentSRROTemp);
            }


            Map<Id, Service_Request__c> idToSrMap = new Map<Id, Service_Request__c>([SELECT Id
                                                                                     FROM Service_Request__c
                                                                                     WHERE Name IN :possibleMatch.keySet() AND Opportunity__c = :existingSR.Opportunity__c AND Service_Request_Status__c <> 'Completed' AND Service_Request_Status__c <> 'Cancelled' ]);

            for(SRS_Service_Request_Related_Order__c currentSRROTemp : [SELECT Name, Reference_Number__c, Reference_Type__c, Comments__c, RO_Service_Request__c, RO_Service_Request__r.Name, RO_Service_Request__r.SRS_PODS_Product__r.Name
                                                                        FROM SRS_Service_Request_Related_Order__c 
                                                                        WHERE RO_Service_Request__c IN :idToSrMap.keySet()
                                                                                AND Name = :existingSR.Name]){
                SRS_Service_Request_Related_Order__c originalSRRO = possibleMatch.get(String.valueOf(currentSRROTemp.RO_Service_Request__r.Name));
                SYstem.debug('Found Match 123');
                if(originalSRRO != null){
                    //Check if the return SRRO SR Id is the name on the current SRRO and the current SRRO SR Id is the name of the returned SRRO

                    if(String.valueOf(originalSRRO.RO_Service_Request__r.Name).equals(currentSRROTemp.Name) && (originalSRRO.Name).equals(String.valueOf(currentSRROTemp.RO_Service_Request__r.Name))){
                        Service_Request__c tempSR = idToSrMap.get(currentSRROTemp.RO_Service_Request__c);
                        
                        if(tempSR != null){
                            tempSR.Service_Identifier_Type__c = currentSR.Service_Identifier_Type__c;
                            tempSR.Existing_Service_ID__c = currentSR.Existing_Service_ID__c;
                            srsToUpdate.add(tempSR);
                            break;
                        }
                    }
                }
            }

            if(!srsToUpdate.isEmpty()){
                update srsToUpdate;
            }
} catch(Exception ex){
    System.debug('Exception ::: ' + ex.getLineNumber());
}
            
    }

    /**
     * BTNCAN
     * @description Inner class that helps group the data together.
     * @author Thomas Tran, Traction on Demand
     * @date 06-11-2015
     */
    public class BTNCAN {
        public Boolean isChecked      {get; set;}
        public String billingAddress  {get; set;}
        public String serviceLocation {get; set;}
        public String acctId          {get; set;}
        public String smbAddressId {get; set;}
        public String btnValue {get; set;}
        public String canValue {get; set;}
        public String canBan {get; set;}
        public String legalName {get; set;}
        public String tradeName {get; set;}
        public String postalCode {get; set;}
        public Boolean hasServiceLocation {get; set;}
        public String province {get; set;}

        public BTNCAN(Boolean isChecked, String billingAddress, String serviceLocation, String btnValue, String canValue, Id acctId, Id smbAddressId, String canBan, String tradeName, String legalName, String postalCode, String province){
            this.acctId = acctId;
            this.isChecked = isChecked;
            this.billingAddress = billingAddress;
            this.serviceLocation = serviceLocation;
            this.canValue = canValue;
            this.btnValue = btnValue;
            this.smbAddressId = smbAddressId;
            this.canBan = canBan;
            this.tradeName = tradeName;
            this.legalName = legalName;
            this.postalCode = postalCode;
            this.province = province;
            hasServiceLocation = (String.isNotBlank(serviceLocation)) ? true : false;
        }
    }
}