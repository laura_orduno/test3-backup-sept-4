/**
 * DTO that is used by integration service but it can be used by other purposes as well.
 */
public class ServiceDisassociationRequest {
    public string uuid{get;set;}
    public string requester{get;set;}
    public string partner{get;set;}
    public string can{get;set;}
    public ServiceDisassociationRequest(string uuid,string requester,string partner,string can){
        this.uuid=uuid;
        this.requester=requester;
        this.partner=partner;
        this.can=can;
    }
}