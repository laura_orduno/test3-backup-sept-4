/* Webservice Mock class for WFM Services
 * Project OCOM WP3
 * Created by Aditya Jamwal(IBM)
 * Date 22-OCT-2016
 * Updated by Aditya Jamwal(IBM)
 * Date 09-SEPT-2017
 */
@isTest
global class OCOM_WFMWebServiceTestMock implements WebServiceMock {
   global boolean isV3Service = false;
   global void doInvoke(
        Object stub,
        Object request,
        Map<String, Object> response,
        String endpoint,
        String soapAction,
        String requestName,
        String responseNS,
        String responseName,
        String responseType) {
            system.debug('!!requestName'+isV3Service+' '+requestName+ ' responseNS '+responseNS +' responseName '+responseName +' responseType '+responseType+ ' soapAction '+ soapAction);
            if(soapAction.endsWith('cancelWorkOrder')){
                if(isV3Service){
                    response.put('response_x', prepareCancelWorkOrderV3());
                }else{
                    response.put('response_x', prepareCancelWorkOrder());
                }
            }  
            if(soapAction.endsWith('searchAvailableAppointmentList')){
                response.put('response_x', preparesearchAvailableAppointmentList());
            }   
            if(soapAction.endsWith('searchWorkOrderList')){
                response.put('response_x', preparesearchWorkOrderList());
            }
            if(soapAction.endsWith('createWorkOrder')){
                response.put('response_x', preparecreateWorkOrder());
            }
            if(soapAction.endsWith('getWorkOrder')){
                response.put('response_x', preparegetWorkOrder());
            }
            if(soapAction.endsWith('updateWorkOrder')){
                response.put('response_x', prepareupdateWorkOrder());
            }
        }
        
        private  WFMFWAssignmentSvcReqResV3.GetWorkOrderResponse preparegetWorkOrder() {      
            WFMFWAssignmentSvcReqResV3.GetWorkOrderResponse response_x = new WFMFWAssignmentSvcReqResV3.GetWorkOrderResponse();
            response_x.workOrder = new WFMOrderTypesV3.WorkOrder();
      return response_x;
    }
    
      private  WFMFWAssignmentSvcReqResV3.UpdateWorkOrderResponse prepareupdateWorkOrder() {      
            WFMFWAssignmentSvcReqResV3.UpdateWorkOrderResponse  response_x = new WFMFWAssignmentSvcReqResV3.UpdateWorkOrderResponse ();
            response_x.workOrder = new WFMOrderTypesV3.WorkOrder();
      return response_x;
    }
    
        private  WFMFWAssignmentSvcReqResV3.CreateWorkOrderResponse preparecreateWorkOrder() {      
            WFMFWAssignmentSvcReqResV3.CreateWorkOrderResponse response_x = new WFMFWAssignmentSvcReqResV3.CreateWorkOrderResponse();
            response_x.workOrder = new WFMOrderTypesV3.WorkOrder();
      return response_x;
    }
    
        private WFMFWAssignmentSvcReqResV3.CancelWorkOrderResponse  prepareCancelWorkOrderV3() {
      WFMFWAssignmentSvcReqResV3.CancelWorkOrderResponse  resp = new WFMFWAssignmentSvcReqResV3.CancelWorkOrderResponse();
          resp.workOrderId = '12342112';
      return resp;
    }
    
      private SMB_AssignmentWFM_ServiceReqRes_v2.CancelWorkOrderResponse prepareCancelWorkOrder() {
      SMB_AssignmentWFM_ServiceReqRes_v2.CancelWorkOrderResponse resp = new SMB_AssignmentWFM_ServiceReqRes_v2.CancelWorkOrderResponse();
          resp.workOrderId = '12342112';
      return resp;
    }
        
        private WFMFWAppointmentSvcReqResV3.SearchAvailableAppointmentListResponse preparesearchAvailableAppointmentList() {
            WFMFWAppointmentSvcReqResV3.SearchAvailableAppointmentListResponse response_x;
            Map<String, WFMFWAppointmentSvcReqResV3.SearchAvailableAppointmentListResponse> response_map_x = new Map<String, WFMFWAppointmentSvcReqResV3.SearchAvailableAppointmentListResponse>();
            response_map_x.put('response_x', response_x);
            return response_x;
        }
        
        private WFMFWAssignmentSvcReqResV3.SearchWorkOrderListResponse preparesearchWorkOrderList() {            
            WFMFWAssignmentSvcReqResV3.SearchWorkOrderListResponse response_x = new WFMFWAssignmentSvcReqResV3.SearchWorkOrderListResponse();
            response_x.searchWorkOrderResponseList = new List<WFMOrderTypesV3.WorkOrderMap>();
            Map<String, WFMFWAssignmentSvcReqResV3.SearchWorkOrderListResponse> response_map_x = new Map<String, WFMFWAssignmentSvcReqResV3.SearchWorkOrderListResponse>();
             response_map_x.put('response_x',response_x);
            response_x = response_map_x.get('response_x');
            return response_x;
        }
}