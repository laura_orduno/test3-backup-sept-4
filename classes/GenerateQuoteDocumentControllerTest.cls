public with sharing class GenerateQuoteDocumentControllerTest {
	testMethod static void test() {
		Opportunity opp = new Opportunity(Name='Test',CloseDate=System.today(),StageName='Prospecting');
		insert opp;
		
		SBQQ__Quote__c quote = new SBQQ__Quote__c(SBQQ__Opportunity__c=opp.Id);
		insert quote;
		
		SBQQ__QuoteTemplate__c template = new SBQQ__QuoteTemplate__c(Name='Test');
		insert template;
		
		ApexPages.currentPage().getParameters().put('qids', quote.Id);
		ApexPages.currentPage().getParameters().put('tid', template.Id);
		GenerateQuoteDocumentController target = new GenerateQuoteDocumentController();
		System.assertEquals(1, target.quotes.size());
	}
}