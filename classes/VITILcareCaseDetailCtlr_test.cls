@isTest
private without sharing class VITILcareCaseDetailCtlr_test {
	static Case testCase { get; set; }

	@isTest(SeeAllData=false) static void testVITILcareCaseDetailPage1() {
		Test.startTest();

		ENTPTestUtils.createCaseRootCauseCronicSetting();
		ENTPTestUtils.createExternalToInternalCustomSettings();
		MBRTestUtils.createParentToChildCustomSettings();
		/* 	ENTPTestUtils.createCaseRootCauseCronicSetting();
			 ENTPTestUtils.createSMBCareWebServicesCustomSetting();
			 MBRTestUtils.createExternalToInternalCustomSettings();
			 MBRTestUtils.createParentToChildCustomSettings();*/

		String userName = 'test-user-1';
		String lang = 'en_US';
		Account a = new Account(Name = 'Unit Test Account');
		a.strategic__c = true;
		insert a;
		Contact c = new Contact(Email = userName + '@unit-test.com', LastName = 'Test', AccountId = a.Id);
		insert c;
		Profile p = [SELECT Id FROM Profile WHERE Name = 'Customer Community User Custom' AND UserType = 'CSPLitePortal' LIMIT 1];
		User u = new User(Alias = 'TestUser', Email = userName + '@unit-test.com', ContactId = c.Id,
				EmailEncodingKey = 'UTF-8', FirstName = 'Unit', LastName = 'Testing', LanguageLocaleKey = lang,
				LocaleSidKey = lang, CommunityNickname = userName + 'UNTST', ProfileId = p.Id,
				TimeZoneSidKey = 'America/Los_Angeles',
				UserName = userName + '@unit-test.com');
		u.Customer_Portal_Role__C = 'tps';
		insert u;

		System.runAs(u) {

			List<Case> testCases = ENTPTestUtils.createENTPCases(2, a.Id);
			//create a comment - not EMAIL Comment
			ENTPTestUtils.createComment(testCases[0].Id, false, false);
			//create a comment - EMAIL Comment
			ENTPTestUtils.createComment(testCases[0].Id, true, true);
			List<CaseComment> caseComments = [Select id,CommentBody,Createdby.LastName,Createdby.firstname, CreatedDate from CaseComment where ParentId = :testCases[0].Id];
			//Email message create
			EmailMessage eMsg = ENTPTestUtils.createEmail(testCases[0].Id, true, 'test incoming email ');
			EmailMessage eMsg2 = ENTPTestUtils.createEmail(testCases[0].Id, false, 'test outbound email ');

			//Create Attachments
			ENTPTestUtils.createAttachment(testCases[0].Id, 1, 'jpg', dateTime.now());
			ENTPTestUtils.createAttachment(testCases[0].Id, 1, 'zip', DateTime.now().addMinutes(-60));
			ENTPTestUtils.createAttachment(testCases[0].Id, 1, 'doc', DateTime.now().addHours(-20));
			ENTPTestUtils.createAttachment(testCases[0].Id, 1, 'xls', DateTime.now().addHours(-40));
			ENTPTestUtils.createAttachment(testCases[0].Id, 1, 'png', DateTime.now().addDays(-20));
			ENTPTestUtils.createAttachment(testCases[0].Id, 1, 'gif', DateTime.now().addDays(-31));
			ENTPTestUtils.createAttachment(testCases[0].Id, 1, 'pdf', DateTime.now().addYears(-2));

			//get list of attachments
			List<Attachment> caseAttachments = [SELECT Id, Name, CreatedDate, LastModifiedDate FROM Attachment WHERE ParentId = :testCases[0].Id];

			VITILcareCaseDetailCtlr.CommentWrapper comWrap = new VITILcareCaseDetailCtlr.CommentWrapper(caseComments[0]);
			comWrap = new VITILcareCaseDetailCtlr.CommentWrapper(caseComments[0]);
			comWrap = new VITILcareCaseDetailCtlr.CommentWrapper(caseComments[1]);
			VITILcareCaseDetailCtlr.AttachmentWrapper attWrap = new VITILcareCaseDetailCtlr.AttachmentWrapper(caseAttachments[0]);

			//caseAttachments[0].Name = 'test.zip.' + caseAttachments[0].Name;
			// update caseAttachments[0];
			attWrap = new VITILcareCaseDetailCtlr.AttachmentWrapper(caseAttachments[0]);
			attWrap = new VITILcareCaseDetailCtlr.AttachmentWrapper(caseAttachments[1]);
			attWrap = new VITILcareCaseDetailCtlr.AttachmentWrapper(caseAttachments[2]);
			attWrap = new VITILcareCaseDetailCtlr.AttachmentWrapper(caseAttachments[3]);
			attWrap = new VITILcareCaseDetailCtlr.AttachmentWrapper(caseAttachments[4]);
			attWrap = new VITILcareCaseDetailCtlr.AttachmentWrapper(caseAttachments[5]);
			attWrap = new VITILcareCaseDetailCtlr.AttachmentWrapper(caseAttachments[6]);

			List<Case> caseList = [SELECT createdby.firstname, createdby.lastname, NotifyCustomer__c, NotifyCollaboratorString__c, OwnerId, Resolution_Details__c, My_Business_Requests_Type__c, ParentId, isClosed, Contact.FirstName, Contact.LastName, id,contact.name,subject,description,lastmodifieddate,case_resolution__c,createddate, caseNumber, recordType.name, account.name, type, status, priority, isEscalated, Escalation_History__c, (select Active__c,Collaborator_Email__c, Collaborator_Name__c, Role__c from Collaborator__r order by createddate desc), (select createdby.firstname, createdby.lastname,Id,Name,Description,ParentId,CreatedDate,LastModifiedDate, ContentType from Attachments order by LastModifiedDate asc), (select createdby.firstname, createdby.lastname,Id,CommentBody,ParentId,CreatedDate,LastModifiedDate from casecomments where IsPublished = true order by CreatedDate asc), (select id, toaddress,fromaddress,fromname,ccaddress,messagedate,subject,htmlbody,textbody,hasattachment from emailmessages) FROM Case WHERE Id = :testCases[0].Id];
			Case cs = caseList[0];
			Case cs1 = testCases[1];

			cs.ParentId = cs1.Id;
			update cs;

			System.debug('cs.CaseNumber: ' + cs.CaseNumber);

			PageReference pageRef = Page.VITILcareCaseDetail;
			pageRef.getParameters().put('caseNumber', cs.CaseNumber);
			Test.setCurrentPage(pageRef);

			VITILcareCaseDetailCtlr ctlr = new VITILcareCaseDetailCtlr();
			ctlr.addComment();
			List<EmailMessage> emails = testCases[0].getsobjects('EmailMessages');
			Map<Id, List<Attachment>> emailIdToAttachs = new Map<Id, List<Attachment>>();
			if (emails != null) {
				List<Id> emailIds = new List<Id>();
				for (EmailMessage em : emails) {
					emailIds.add(em.id);
				}
				List<Attachment> emailAttachs = [select createdby.firstname, createdby.lastname,Id,Name,Description,ParentId,CreatedDate,LastModifiedDate from Attachment where parentid IN :emailIds order by LastmodifiedDate asc];
				for (Attachment a1 : emailAttachs) {
					if (emailIdToAttachs.get(a1.parentid) == null) {
						emailIdToAttachs.put(a1.parentid, new List<Attachment>());
					}
					emailIdToAttachs.get(a1.parentid).add(a1);
				}
				ctlr.emailAttachs = emailAttachs;
			}

			ctlr.caseNumber = cs.CaseNumber;
			ctlr.caseCreatedByLoggedInUser = true;
			ctlr.comments = caseComments;
			ctlr.ContactAddress = 'test';

			ctlr.testInput = 'test';
			// String DetailLink = ctlr.getDetailLink();

			ctlr.getLanguageLocaleKey();
			ctlr.attachedFile = NULL;

			Attachment attachedFile = new Attachment();
			attachedFile.Name = 'Unit Test Attachment';
			attachedFile.body = Blob.valueOf('Unit Test Attachment Body');
			attachedFile.ParentId = cs1.id;
			insert attachedFile;
			ctlr.attachedFile = attachedFile;

			String durSince = ctlr.getDurationSinceSubmitted();
			durSince = VITILcareCaseDetailCtlr.calculateDurationSince(1000000000, 2000000000);
			System.debug('durSince: ' + durSince);

			CaseComment cmt = ctlr.getNewComment();
			CaseComment newComment = new CaseComment();
			newComment.CommentBody = 'Comment Body - Test Comment';
			newComment.parentId = cs1.id;

			system.debug('line 106 - CaseDetail Test class - newComment.parentId-' + newComment.parentId);

			// ctlr.getAttachmentByCommentString(newComment.CommentBody);
			ctlr.getCollaboratorsList();
			// ctlr.CollaboratorWrapper;
			ctlr.getEmailList();
			ctlr.getCommentList();
			ctlr.getCommentWrapperList();
			ctlr.getCaseStatus();
			ctlr.getCaseType();
			ctlr.getDescription();
			ctlr.updateSubscription();
			ctlr.escalateCase();
			ctlr.closeCase();
			ctlr.reopenCase();

			// ctlr.upload();
			// ctlr.escalateCase();
			//ctlr.getDetailLink();

			ctlr.addCollaborator();

			Collaborators__c clb = ctlr.getNewCollab();
			List<Collaborators__c> collabs = new List<Collaborators__c>();
			ctlr.empty();

			//ctlr.upload();

			//MBRCaseDetailCtlr.changeCollabStatus(cs.Id, 0);

			ctlr.getCollaboratorWithIndex();

			// ctlr.escalateCase();

			pageRef.getParameters().put('caseId', cs.Id);
			ctlr.unsubscribe();

			// Causing error:
			// System.DmlException: Insert failed. First exception on row 0; first error: UNKNOWN_EXCEPTION, portal users cannot own partner accounts: []

			List<VITILcareCaseDetailCtlr.AttachmentWrapper> attWraps = ctlr.getAttachmentList();
			//   List<ENTPCaseDetail.CommentWrapper> comWraps = ctlr.getCommentWrapperList();
			// System.debug('MBRTestUtils.createENTPPortalUser start');
			// System.debug('MBRTestUtils.createENTPPortalUser end');

			Test.setCurrentPage(Page.VITILcareUserTemplate);

			ctlr.closeReason = 'test';

			// ctlr.onLoad();
/*    
                Collaborators__c newCollab = new Collaborators__c();
                //ctlr.onLoad();
                String userAccountId = [SELECT AccountId FROM User WHERE Id = :UserInfo.getUserId()].AccountId;
                system.assert(String.isNotEmpty(userAccountId));
                List<account> Accounts = [SELECT Strategic__c, ID from Account where ID =:userAccountId];
                
                
                String userLanguage = [SELECT LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()].LanguageLocaleKey;
                system.assert(String.isNotEmpty(userLanguage));
                //String CustRole = 'tps';
                //String CustRole1 = CustRole;
                String SFDCAccountId='test';
                System.assertEquals( SFDCAccountId, 'test');
                String CatListPull1='test';
                System.assertEquals( CatListPull1, 'test');
                String AddFuncPull1='test';
                System.assertEquals( AddFuncPull1, 'test');
                String DescriptionView1 = 'test';
                String ClientPhoneView ='test';
****/
		}

		Test.stopTest();
	}

	@isTest(SeeAllData=false) static void testLynxIntegration() {
		Test.startTest();
		// trac_TriggerHandlerBase.blockTrigger = true;
		ENTPTestUtils.createSMBCareWebServicesCustomSetting();

		// mock request 
//		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());

		VITILcareCaseDetailCtlr ctlr = new VITILcareCaseDetailCtlr();
		//ctlr.onLoad();

		account a = new account(name = 'test acct');
		insert a;

		List<Case> testCases = ENTPTestUtils.createENTPCases(2, a.Id);

		List<Case> caseList1 = [SELECT createdby.firstname, createdby.lastname, Lynx_Ticket_Number__c, NotifyCustomer__c, NotifyCollaboratorString__c, OwnerId, Resolution_Details__c, My_Business_Requests_Type__c, ParentId, isClosed, Contact.FirstName, Contact.LastName, id,contact.name,subject,description,lastmodifieddate,case_resolution__c,createddate, caseNumber, recordType.name, account.name, type, status, priority, isEscalated, Escalation_History__c, (select Active__c,Collaborator_Email__c, Collaborator_Name__c, Role__c from Collaborator__r order by createddate desc), (select createdby.firstname, createdby.lastname,Id,Name,Description,ParentId,CreatedDate,LastModifiedDate, ContentType from Attachments order by LastModifiedDate asc), (select createdby.firstname, createdby.lastname,Id,CommentBody,ParentId,CreatedDate,LastModifiedDate from casecomments where IsPublished = true order by CreatedDate asc), (select id, toaddress,fromaddress,fromname,ccaddress,messagedate,subject,htmlbody,textbody,hasattachment from emailmessages) FROM Case WHERE Id = :testCases[0].Id];
		Case cs1 = caseList1[0];
		ctlr.LynxTicketSearchRET(cs1.Lynx_Ticket_Number__c);
		ctlr.GetLynxTicketInfo(cs1.id);
		// trac_TriggerHandlerBase.blockTrigger = false;
		Test.stopTest();
	}

	/*@isTest(SeeAllData=false) static void testRemedyIntegration() {
         Test.startTest();
      //  trac_TriggerHandlerBase.blockTrigger = true;
		ENTPTestUtils.createSMBCareWebServicesCustomSetting();

        account a = new account(name = 'test acct');
        insert a;
        
        List<Case> testCases = ENTPTestUtils.createENTPCases(2, a.Id);
        ENTPCaseDetail ctlr = new ENTPCaseDetail();
        // ctlr.onLoad();
        
        List<Case> caseList1 = [SELECT createdby.firstname, createdby.lastname, NotifyCustomer__c, NotifyCollaboratorString__c, OwnerId, Resolution_Details__c, My_Business_Requests_Type__c, ParentId, isClosed, Contact.FirstName, Contact.LastName, id,contact.name,subject,description,lastmodifieddate,case_resolution__c,createddate, caseNumber, recordType.name, account.name, type, status, priority, isEscalated, Escalation_History__c, (select Active__c,Collaborator_Email__c, Collaborator_Name__c, Role__c from Collaborator__r order by createddate desc),(select createdby.firstname, createdby.lastname,Id,Name,Description,ParentId,CreatedDate,LastModifiedDate, ContentType from Attachments order by LastModifiedDate asc), (select createdby.firstname, createdby.lastname,Id,CommentBody,ParentId,CreatedDate,LastModifiedDate from casecomments where IsPublished = true order by CreatedDate asc), (select id, toaddress,fromaddress,fromname,ccaddress,messagedate,subject,htmlbody,textbody,hasattachment from emailmessages) FROM Case WHERE Id = :testCases[0].Id];
        Case cs1 = caseList1[0];
        ctlr.SendRemedyView(cs1.Id); 
       //  trac_TriggerHandlerBase.blockTrigger = false;
        Test.stopTest();
    }
    */
	@isTest(SeeAllData=false) static void testLynxUpdateActivity() {
		Test.startTest();
		//  trac_TriggerHandlerBase.blockTrigger = true;

		ENTPTestUtils.createSMBCareWebServicesCustomSetting();
		account a = new account(name = 'test acct');
		insert a;

		List<Case> testCases = ENTPTestUtils.createENTPCases(2, a.Id);
		List<Case> caseList1 = [SELECT createdby.firstname, createdby.lastname, NotifyCustomer__c, Lynx_Ticket_Number__c, NotifyCollaboratorString__c, OwnerId, Resolution_Details__c, My_Business_Requests_Type__c, ParentId, isClosed, Contact.FirstName, Contact.LastName, id,contact.name,subject,description,lastmodifieddate,case_resolution__c,createddate, caseNumber, recordType.name, account.name, type, status, priority, isEscalated, Escalation_History__c, (select Active__c,Collaborator_Email__c, Collaborator_Name__c, Role__c from Collaborator__r order by createddate desc), (select createdby.firstname, createdby.lastname,Id,Name,Description,ParentId,CreatedDate,LastModifiedDate, ContentType from Attachments order by LastModifiedDate asc), (select createdby.firstname, createdby.lastname,Id,CommentBody,ParentId,CreatedDate,LastModifiedDate from casecomments where IsPublished = true order by CreatedDate asc), (select id, toaddress,fromaddress,fromname,ccaddress,messagedate,subject,htmlbody,textbody,hasattachment from emailmessages) FROM Case WHERE Id = :testCases[0].Id];
		Case cs1 = caseList1[0];
		//create a comment - not EMAIL Comment
		ENTPTestUtils.createComment(testCases[0].Id, false, false);

		CaseComment fID = [Select id,CommentBody,Createdby.LastName,Createdby.firstname, CreatedDate from CaseComment where ParentId = :cs1.Id];
		ID CommentID = fID.Id;
		String CommentBodyGet = fID.CommentBody;

		LynxUpdateActivity.sendRequest smbCallOut = new LynxUpdateActivity.sendRequest();
		smbCallOut.SRequest(cs1.casenumber, cs1.Id, CommentBodyGet, CommentID, cs1.Lynx_Ticket_Number__c, 'test');

		//  trac_TriggerHandlerBase.blockTrigger = false;
		Test.stopTest();
	}
	@isTest(SeeAllData=false) static void testLynxTicketSearch() {
		Test.startTest();
		trac_TriggerHandlerBase.blockTrigger = true;

		ENTPTestUtils.createSMBCareWebServicesCustomSetting();
		account a = new account(name = 'test acct');
		insert a;

		List<Case> testCases = ENTPTestUtils.createENTPCases(2, a.Id);
		List<Case> caseList1 = [SELECT createdby.firstname, createdby.lastname, NotifyCustomer__c, Lynx_Ticket_Number__c, NotifyCollaboratorString__c, OwnerId, Resolution_Details__c, My_Business_Requests_Type__c, ParentId, isClosed, Contact.FirstName, Contact.LastName, id,contact.name,subject,description,lastmodifieddate,case_resolution__c,createddate, caseNumber, recordType.name, account.name, type, status, priority, isEscalated, Escalation_History__c, (select Active__c,Collaborator_Email__c, Collaborator_Name__c, Role__c from Collaborator__r order by createddate desc), (select createdby.firstname, createdby.lastname,Id,Name,Description,ParentId,CreatedDate,LastModifiedDate, ContentType from Attachments order by LastModifiedDate asc), (select createdby.firstname, createdby.lastname,Id,CommentBody,ParentId,CreatedDate,LastModifiedDate from casecomments where IsPublished = true order by CreatedDate asc), (select id, toaddress,fromaddress,fromname,ccaddress,messagedate,subject,htmlbody,textbody,hasattachment from emailmessages) FROM Case WHERE Id = :testCases[0].Id];
		Case cs1 = caseList1[0];
		try {
			Ticket_Event__c NewEvent = new Ticket_Event__c(
					Case_Number__c = cs1.caseNumber,
					Event_Type__c = 'New Ticket',
					Status__c = 'New',
					Failure_Reason__c = 'Ticket request returned an error.',                             //Lynx_Ticket_Number__c = ticketNum,
					Case_Id__c = cs1.Id
			);
			System.debug('LynxTicketCreate->NewEvent, CaseNumber(' + cs1.caseNumber + ')');
			insert NewEvent;

		} catch (DmlException e) {
			System.debug('LynxTicketCreate->TicketEvent, exception: ' + e.getMessage());
		}
		Ticket_Event__c tickEvent = [Select id from Ticket_Event__c where Case_Id__c = :cs1.Id];
		Id TicketeventID = tickEvent.id;
		LynxTicketSearch.sendRequest(cs1.Lynx_Ticket_Number__c, cs1.casenumber, cs1.Id, TicketeventID);
		LynxTicketSearch.sendRequest(cs1.Lynx_Ticket_Number__c, cs1.casenumber, cs1.Id, '');
		LynxTicketSearch.sendRequest('', cs1.casenumber, cs1.Id, TicketeventID);
		// LynxUpdateActivity.sendRequest smbCallOut = new LynxUpdateActivity.sendRequest();
		// smbCallOut.SRequest(cs1.casenumber, cs1.Id, CommentBodyGet, CommentID, cs1.Lynx_Ticket_Number__c, 'test');

		trac_TriggerHandlerBase.blockTrigger = false;
		Test.stopTest();
	}

	/*  @isTest(SeeAllData=false) static void testLynxTicketSearchNull() {
		   Test.startTest();
		   trac_TriggerHandlerBase.blockTrigger = true;

		   ENTPTestUtils.createSMBCareWebServicesCustomSetting();
		   account a = new account(name = 'test acct');
		   insert a;

		   List<Case> testCases = ENTPTestUtils.createENTPCases(2, a.Id);
		   List<Case> caseList1 = [SELECT createdby.firstname, createdby.lastname, NotifyCustomer__c, Lynx_Ticket_Number__c, NotifyCollaboratorString__c, OwnerId, Resolution_Details__c, My_Business_Requests_Type__c, ParentId, isClosed, Contact.FirstName, Contact.LastName, id,contact.name,subject,description,lastmodifieddate,case_resolution__c,createddate, caseNumber, recordType.name, account.name, type, status, priority, isEscalated, Escalation_History__c, (select Active__c,Collaborator_Email__c, Collaborator_Name__c, Role__c from Collaborator__r order by createddate desc),(select createdby.firstname, createdby.lastname,Id,Name,Description,ParentId,CreatedDate,LastModifiedDate, ContentType from Attachments order by LastModifiedDate asc), (select createdby.firstname, createdby.lastname,Id,CommentBody,ParentId,CreatedDate,LastModifiedDate from casecomments where IsPublished = true order by CreatedDate asc), (select id, toaddress,fromaddress,fromname,ccaddress,messagedate,subject,htmlbody,textbody,hasattachment from emailmessages) FROM Case WHERE Id = :testCases[0].Id];
		   Case cs1 = caseList1[0];
		   try{
			   Ticket_Event__c NewEvent = new Ticket_Event__c(
					   Case_Number__c = cs1.caseNumber,
					   Event_Type__c = 'New Ticket',
					   Status__c = 'New',
					   Failure_Reason__c = 'Ticket request returned an error.',                             //Lynx_Ticket_Number__c = ticketNum,
					   Case_Id__c = cs1.Id
			   );
			   System.debug('LynxTicketCreate->NewEvent, CaseNumber(' + cs1.caseNumber + ')');
			   insert NewEvent;

		   }
		   catch(DmlException e) {
			   System.debug('LynxTicketCreate->TicketEvent, exception: ' + e.getMessage());
		   }

		   Id TicketeventID = null;
		   LynxTicketSearch.sendRequest(cs1.Lynx_Ticket_Number__c, cs1.casenumber, cs1.Id, TicketeventID);
		   // LynxUpdateActivity.sendRequest smbCallOut = new LynxUpdateActivity.sendRequest();
		   // smbCallOut.SRequest(cs1.casenumber, cs1.Id, CommentBodyGet, CommentID, cs1.Lynx_Ticket_Number__c, 'test');

		  trac_TriggerHandlerBase.blockTrigger = false;
		   Test.stopTest();
	   }
   */

	@isTest(SeeAllData=false) static void testENTPDMLUtil() {
		Contact con = new Contact(LastName = 'Test', Email = 'Test@test.com');
		insert con;

		MBR_Case_Collaborator_Settings__c settings = MBR_Case_Collaborator_Settings__c.getOrgDefaults();
		settings.Dummy_Contact_id__c = con.Id;
		upsert settings;

		Test.startTest();
		//  trac_TriggerHandlerBase.blockTrigger = true;
		Account a = new Account(Name = 'Unit Test Account');
		a.strategic__c = true;
		insert a;
		List<Case> testCases = ENTPTestUtils.createENTPCases(1, a.Id);
		ENTPDMLutil.updatecaseStatus('Closed', '1111111111');
		//     trac_TriggerHandlerBase.blockTrigger = false;
		Test.stopTest();
	}
}