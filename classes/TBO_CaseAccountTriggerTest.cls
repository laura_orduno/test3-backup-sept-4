/*
###########################################################################
# File..................: TBO_CaseAccountTriggerTest
# Version...............: 1
# Created by............: Adhir Parchure (TechM)
# Created Date..........: 04/05/2016 
# Last Modified by......: Adhir Parchure (TechM)
# Last Modified Date....: 04/05/2016 
# Description...........: Test class is to provide coverage to case Account triggers.
#
############################################################################
*/


@isTest
public class TBO_CaseAccountTriggerTest {
	public static testmethod void testMethod1(){
    	list<account> listTestacc = new list<account> ();
		// Account schema
        Schema.DescribeSObjectResult tboAccSchema = Schema.SObjectType.Account;
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = tboAccSchema.getRecordTypeInfosByName(); 
		// case schema
        Schema.DescribeSObjectResult tboCaseSchema = Schema.SObjectType.Case;
        Map<String,Schema.RecordTypeInfo> caseRecordTypeInfo = tboCaseSchema.getRecordTypeInfosByName();
		
		// tbo case acccount schema
		Schema.DescribeSObjectResult tboCaseAccSchema = Schema.SObjectType.Case_account__c;
        Map<String,Schema.RecordTypeInfo> caseAccRecordTypeInfo = tboCaseAccSchema.getRecordTypeInfosByName();
		
		test.starttest();
		//RCID account for tbo case
		Account RCIDAcc = new Account(Name='testTBORCID',recordtypeid=AccountRecordTypeInfo .get('RCID').getRecordTypeId(), RCID__C='RCID123',  Inactive__c=false,Billing_Account_Active_Indicator__c='Y' );
        insert RCIDAcc;
		
		//Billing account1
        Account TestAcc1= new account(Name='TestAcc1',recordtypeid=AccountRecordTypeInfo.get('CAN').getRecordTypeId(), CAN__C='CAN001',BTN__c='CBN001',BCAN__c='111-CAN001',BAN_CAN__c='111-CAN001', BCAN_Pilot_Org_Indicator__c=1,Inactive__c=false,Billing_Account_Active_Indicator__c='Y');
        listTestacc.add(TestAcc1);
		
		//Billing account1
		Account TestAcc2= new account(Name='TestAcc2',recordtypeid=AccountRecordTypeInfo.get('CAN').getRecordTypeId(), CAN__C='CAN002',BTN__c='CBN002',BCAN__c='111-CAN001',BAN_CAN__c='111-CAN002', BCAN_Pilot_Org_Indicator__c=2,Inactive__c=false,Billing_Account_Active_Indicator__c='Y');
        listTestacc.add(TestAcc2);
		
		insert listTestacc;
		
		TestAcc2.parentid=TestAcc1.id;
		update TestAcc2;
		
		
		// TBO Case
        Case tboCase= new Case(Subject='testTBO',Created_Date__c=system.TODAY(),status='Draft',
                                recordtypeid=caseRecordTypeInfo.get('TBO Request').getRecordTypeId(),
                                Accountid=RCIDAcc.id);
        insert tboCase;
		
		SMBCare_Address__c serAddr= new SMBCare_Address__c(Account__c=TestAcc1.id,Type__c='Service',Street__c='TESTING ST ',City__c='City',State__c='State');
		insert serAddr;
		//Test case account 1
		Case_Account__c TestCaseAcc1= new Case_Account__c( recordtypeid=caseAccRecordTypeInfo.get('Existing').getRecordTypeId(),CAN__C='CAN001',BTN__c='CBN001', BCAN_Pilot_Org_Indicator__c=1, Case__c=tboCase.id, Account__c=TestAcc1.id);
        insert TestCaseAcc1;
		
		//Test case account 1
		Case_Account__c TestCaseAcc2= new Case_Account__c( recordtypeid=caseAccRecordTypeInfo.get('Existing').getRecordTypeId(),CAN__C='CAN002',BTN__c='CBN002', BCAN_Pilot_Org_Indicator__c=2, Case__c=tboCase.id, Account__c=TestAcc2.id);
        insert TestCaseAcc2;
        
        Case_Account__c TestCaseAcc3= new Case_Account__c( recordtypeid=caseAccRecordTypeInfo.get('Existing').getRecordTypeId(),CAN__C='CAN002',BTN__c='CBN002', BCAN_Pilot_Org_Indicator__c=2, Case__c=tboCase.id, Account__c=TestAcc2.id);
        insert TestCaseAcc3;
        
		try{
		delete TestCaseAcc2;
		test.stoptest();
		}
		catch(Exception e){
		//system.debug('>> Exception: '+e.message);
		}
        
		
		
    }
}