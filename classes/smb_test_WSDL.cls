/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class smb_test_WSDL {

    static testMethod void testSmb_CustomerService() {
    	smb_cust_base_types_2_0_new smb = new smb_cust_base_types_2_0_new();
    	smb_cust_base_types_2_0_new.Collection col = new smb_cust_base_types_2_0_new.Collection();
    	smb_cust_base_types_2_0_new.Quantity quant = new smb_cust_base_types_2_0_new.Quantity();
    	smb_cust_base_types_2_0_new.Money  money = new smb_cust_base_types_2_0_new.Money();
    	smb_cust_base_types_2_0_new.Party party = new smb_cust_base_types_2_0_new.Party();
    	smb_cust_base_types_2_0_new.Specification specs = new smb_cust_base_types_2_0_new.Specification();
    	smb_cust_base_types_2_0_new.RootEntity RootEntity = new smb_cust_base_types_2_0_new.RootEntity();
    	smb_cust_base_types_2_0_new.Request  Request  = new smb_cust_base_types_2_0_new.Request();
    	smb_cust_base_types_2_0_new.TimePeriod TimePeriod = new smb_cust_base_types_2_0_new.TimePeriod();
    	smb_cust_base_types_2_0_new.LanguageAbility LanguageAbility = new smb_cust_base_types_2_0_new.LanguageAbility();
    	smb_cust_base_types_2_0_new.Individual Individual = new smb_cust_base_types_2_0_new.Individual();
    	smb_cust_base_types_2_0_new.BusinessInteraction BusinessInteraction = new smb_cust_base_types_2_0_new.BusinessInteraction();
    	smb_cust_base_types_2_0_new.BusinessInteractionItem BusinessInteractionItem = new smb_cust_base_types_2_0_new.BusinessInteractionItem();
    	smb_cust_base_types_2_0_new.CostRate CostRate = new smb_cust_base_types_2_0_new.CostRate();
    	smb_cust_base_types_2_0_new.PartyRole PartyRole = new smb_cust_base_types_2_0_new.PartyRole();
    	smb_cust_base_types_2_0_new.Duration Duration = new smb_cust_base_types_2_0_new.Duration();
    	smb_cust_base_types_2_0_new.Language Language = new smb_cust_base_types_2_0_new.Language();
    	smb_cust_base_types_2_0_new.Rate Rate = new smb_cust_base_types_2_0_new.Rate();
    	smb_cust_base_types_2_0_new.Entity Entity = new smb_cust_base_types_2_0_new.Entity();
    	
    	smb_cust_common_domain_types_3_new smb1 = new smb_cust_common_domain_types_3_new();
    	smb_cust_common_domain_types_3_new.ExpiryDate ExpiryDate = new smb_cust_common_domain_types_3_new.ExpiryDate();
    	smb_cust_common_domain_types_3_new.CardNumberDisplay  CardNumberDisplay = new smb_cust_common_domain_types_3_new.CardNumberDisplay();
    	smb_cust_common_domain_types_3_new.PaymentInstrument PaymentInstrument = new smb_cust_common_domain_types_3_new.PaymentInstrument();
   		smb_cust_common_domain_types_3_new.TaxAmount    TaxAmount = new smb_cust_common_domain_types_3_new.TaxAmount();
   		
   		smb_cust_CreditCommon_v1_new smb2 = new smb_cust_CreditCommon_v1_new();
   		smb_cust_CreditCommon_v1_new.AdjudicationResult a = new smb_cust_CreditCommon_v1_new.AdjudicationResult();
   		smb_cust_CreditCommon_v1_new.BureauInformation a1 = new smb_cust_CreditCommon_v1_new.BureauInformation();
   		smb_cust_CreditCommon_v1_new.CreditAddress a2 = new smb_cust_CreditCommon_v1_new.CreditAddress();
   		smb_cust_CreditCommon_v1_new.CreditAssessmentResult a3 = new smb_cust_CreditCommon_v1_new.CreditAssessmentResult();
   		smb_cust_CreditCommon_v1_new.CreditBureauDocument a4 = new smb_cust_CreditCommon_v1_new.CreditBureauDocument();
   		smb_cust_CreditCommon_v1_new.CreditBureauResult a5 = new smb_cust_CreditCommon_v1_new.CreditBureauResult();
   		smb_cust_CreditCommon_v1_new.CreditCardCode a6 = new smb_cust_CreditCommon_v1_new.CreditCardCode();
   		smb_cust_CreditCommon_v1_new.CreditCustomerInfo a7 = new smb_cust_CreditCommon_v1_new.CreditCustomerInfo();
   		smb_cust_CreditCommon_v1_new.CreditDecision a8 = new smb_cust_CreditCommon_v1_new.CreditDecision();
   		smb_cust_CreditCommon_v1_new.CreditIdentification a9 = new smb_cust_CreditCommon_v1_new.CreditIdentification();
   		smb_cust_CreditCommon_v1_new.CreditProfileData a10 = new smb_cust_CreditCommon_v1_new.CreditProfileData();
   		smb_cust_CreditCommon_v1_new.CustomerGuarantor a11 = new smb_cust_CreditCommon_v1_new.CustomerGuarantor();
   		smb_cust_CreditCommon_v1_new.DriverLicense a12 = new smb_cust_CreditCommon_v1_new.DriverLicense();
   		smb_cust_CreditCommon_v1_new.FraudWarning a13 = new smb_cust_CreditCommon_v1_new.FraudWarning();
   		smb_cust_CreditCommon_v1_new.HealthCard a14 = new smb_cust_CreditCommon_v1_new.HealthCard();
   		smb_cust_CreditCommon_v1_new.Passport a15 = new smb_cust_CreditCommon_v1_new.Passport();
   		smb_cust_CreditCommon_v1_new.PersonalInfo a16 = new smb_cust_CreditCommon_v1_new.PersonalInfo();
   		smb_cust_CreditCommon_v1_new.PersonName a17 = new smb_cust_CreditCommon_v1_new.PersonName();
   		smb_cust_CreditCommon_v1_new.PhoneNumber a18 = new smb_cust_CreditCommon_v1_new.PhoneNumber();
   		smb_cust_CreditCommon_v1_new.ProductCategory a19 = new smb_cust_CreditCommon_v1_new.ProductCategory();
   		smb_cust_CreditCommon_v1_new.ProductCategoryQualification a20 = new smb_cust_CreditCommon_v1_new.ProductCategoryQualification();
   		smb_cust_CreditCommon_v1_new.ProvincialIdCard a21 = new smb_cust_CreditCommon_v1_new.ProvincialIdCard();
   		smb_cust_CreditCommon_v1_new.RiskIndicator a22 = new smb_cust_CreditCommon_v1_new.RiskIndicator();
   		smb_cust_CreditCommon_v1_new.ScoreCardAttribute a23 = new smb_cust_CreditCommon_v1_new.ScoreCardAttribute();
   		
   		smb_Customer_Prod_Inst_Sub_Domain_new smb3 = new smb_Customer_Prod_Inst_Sub_Domain_new();
   		smb_Customer_Prod_Inst_Sub_Domain_new.ProductInstance b1 = new   smb_Customer_Prod_Inst_Sub_Domain_new.ProductInstance();
   		smb_Customer_Prod_Inst_Sub_Domain_new.Agreement b2 = new   smb_Customer_Prod_Inst_Sub_Domain_new.Agreement();
   		smb_Customer_Prod_Inst_Sub_Domain_new.ProductParameter b3 = new   smb_Customer_Prod_Inst_Sub_Domain_new.ProductParameter();
   		smb_Customer_Prod_Inst_Sub_Domain_new.ProductResource b4 = new   smb_Customer_Prod_Inst_Sub_Domain_new.ProductResource();
   		smb_Customer_Prod_Inst_Sub_Domain_new.ServiceAndEquipment b5 = new   smb_Customer_Prod_Inst_Sub_Domain_new.ServiceAndEquipment();
   		
   		smb_Customer_v3_new smb4 = new smb_Customer_v3_new ();
   		smb_Customer_v3_new.BillingAccountNumberAndSourceSystem c1 = new smb_Customer_v3_new.BillingAccountNumberAndSourceSystem();
   		smb_Customer_v3_new.Consent c2 = new smb_Customer_v3_new.Consent();
   		smb_Customer_v3_new.Contact c3 = new smb_Customer_v3_new.Contact();
   		smb_Customer_v3_new.ContactAddress c4 = new smb_Customer_v3_new.ContactAddress();
   		smb_Customer_v3_new.ContactAddressType c5 = new smb_Customer_v3_new.ContactAddressType();
   		smb_Customer_v3_new.ContactPreference c6 = new smb_Customer_v3_new.ContactPreference();
   		smb_Customer_v3_new.Customer c7 = new smb_Customer_v3_new.Customer();
   		smb_Customer_v3_new.CustomerAddress c8 = new smb_Customer_v3_new.CustomerAddress();
   		smb_Customer_v3_new.CustomerAddressType c9 = new smb_Customer_v3_new.CustomerAddressType();
   		smb_Customer_v3_new.CustomerContact c10 = new smb_Customer_v3_new.CustomerContact();
   		smb_Customer_v3_new.CustomerCreditProfile c11 = new smb_Customer_v3_new.CustomerCreditProfile();
   		smb_Customer_v3_new.CustomerExtended c12 = new smb_Customer_v3_new.CustomerExtended();
   		smb_Customer_v3_new.CustomerHierarchyNode c13 = new smb_Customer_v3_new.CustomerHierarchyNode();
   		smb_Customer_v3_new.CustomerIdentification c14 = new smb_Customer_v3_new.CustomerIdentification();
   		smb_Customer_v3_new.CustomerName c15 = new smb_Customer_v3_new.CustomerName();
   		smb_Customer_v3_new.CustomerRelationship c16 = new smb_Customer_v3_new.CustomerRelationship();
   		smb_Customer_v3_new.ExternalKey c17 = new smb_Customer_v3_new.ExternalKey();
   		smb_Customer_v3_new.FeedbackSurveyConsent c18 = new smb_Customer_v3_new.FeedbackSurveyConsent();
   		smb_Customer_v3_new.IndividualProfile c19 = new smb_Customer_v3_new.IndividualProfile();
   		smb_Customer_v3_new.MailingAddress c20 = new smb_Customer_v3_new.MailingAddress();
   		smb_Customer_v3_new.MarketingConsent c21 = new smb_Customer_v3_new.MarketingConsent();
   		smb_Customer_v3_new.MatchReason c22 = new smb_Customer_v3_new.MatchReason();
   		smb_Customer_v3_new.NameType c23 = new smb_Customer_v3_new.NameType();
   		smb_Customer_v3_new.OrganizationProfile c24 = new smb_Customer_v3_new.OrganizationProfile();
   		smb_Customer_v3_new.PrivacySharingConsent c25 = new smb_Customer_v3_new.PrivacySharingConsent();
   		smb_Customer_v3_new.ProductDepositRequired c26 = new smb_Customer_v3_new.ProductDepositRequired();
   		smb_Customer_v3_new.ProductInstanceKeyAndSourceSystem c27 = new smb_Customer_v3_new.ProductInstanceKeyAndSourceSystem();
   		smb_Customer_v3_new.Profile c28 = new smb_Customer_v3_new.Profile();
   		smb_Customer_v3_new.TelephoneNumber c29 = new smb_Customer_v3_new.TelephoneNumber();
   		
   		smb_CustomerCommon_v3_new smb5 = new smb_CustomerCommon_v3_new ();
   		smb_CustomerCommon_v3_new.Address d1 = new smb_CustomerCommon_v3_new.Address();
   		smb_CustomerCommon_v3_new.CustomerMarketSegment d2 = new smb_CustomerCommon_v3_new.CustomerMarketSegment();
   		smb_CustomerCommon_v3_new.Name d3 = new smb_CustomerCommon_v3_new.Name();
   		
   		smb_CustomerCommon_v4_new smb6 = new smb_CustomerCommon_v4_new();
   		
   		smb_customer_info_ref_types_new smb7 = new smb_customer_info_ref_types_new();
   		smb_customer_info_ref_types_new.AccountType e1 = new smb_customer_info_ref_types_new.AccountType();
   		smb_customer_info_ref_types_new.MemoType e2 = new smb_customer_info_ref_types_new.MemoType();
   		smb_customer_info_ref_types_new.Reference e3 = new smb_customer_info_ref_types_new.Reference();
   		smb_customer_info_ref_types_new.Segmentation e4 = new smb_customer_info_ref_types_new.Segmentation();
   		
   		smb_CustomerMgmtSvcReqResp_v3_new smb8 = new smb_CustomerMgmtSvcReqResp_v3_new();
   		smb_CustomerMgmtSvcReqResp_v3_new.BillingAccountSummary  f1 = new smb_CustomerMgmtSvcReqResp_v3_new.BillingAccountSummary();
   		smb_CustomerMgmtSvcReqResp_v3_new.createCustomer_element  f2 = new smb_CustomerMgmtSvcReqResp_v3_new.createCustomer_element();
   		smb_CustomerMgmtSvcReqResp_v3_new.createCustomerResponse_element  f3 = new smb_CustomerMgmtSvcReqResp_v3_new.createCustomerResponse_element();
   		smb_CustomerMgmtSvcReqResp_v3_new.getCustomer_element  f4 = new smb_CustomerMgmtSvcReqResp_v3_new.getCustomer_element();
   		smb_CustomerMgmtSvcReqResp_v3_new.getCustomerResponse_element  f5 = new smb_CustomerMgmtSvcReqResp_v3_new.getCustomerResponse_element();
   		smb_CustomerMgmtSvcReqResp_v3_new.ProductInstanceSummary  f6 = new smb_CustomerMgmtSvcReqResp_v3_new.ProductInstanceSummary();
   		smb_CustomerMgmtSvcReqResp_v3_new.searchCustomer_element  f7 = new smb_CustomerMgmtSvcReqResp_v3_new.searchCustomer_element();
   		smb_CustomerMgmtSvcReqResp_v3_new.SearchCustomerCriteriaDto  f8 = new smb_CustomerMgmtSvcReqResp_v3_new.SearchCustomerCriteriaDto();
   		smb_CustomerMgmtSvcReqResp_v3_new.searchCustomerResponse_element  f9 = new smb_CustomerMgmtSvcReqResp_v3_new.searchCustomerResponse_element();
   		smb_CustomerMgmtSvcReqResp_v3_new.SearchCustomerResult  f10 = new smb_CustomerMgmtSvcReqResp_v3_new.SearchCustomerResult();
   		
   		smb_CustomerManagementCommonTypes_v4_new smb9 = new smb_CustomerManagementCommonTypes_v4_new();
   		smb_CustomerManagementCommonTypes_v4_new.ActivationOption g1 = new smb_CustomerManagementCommonTypes_v4_new.ActivationOption();
   		smb_CustomerManagementCommonTypes_v4_new.Address g2 = new smb_CustomerManagementCommonTypes_v4_new.Address();
   		smb_CustomerManagementCommonTypes_v4_new.AddressValidationResult g3 = new smb_CustomerManagementCommonTypes_v4_new.AddressValidationResult();
   		smb_CustomerManagementCommonTypes_v4_new.AuditInfo g4 = new smb_CustomerManagementCommonTypes_v4_new.AuditInfo();
   		smb_CustomerManagementCommonTypes_v4_new.BankAccount g5 = new smb_CustomerManagementCommonTypes_v4_new.BankAccount();
   		smb_CustomerManagementCommonTypes_v4_new.BillingAccountAssociation g6 = new smb_CustomerManagementCommonTypes_v4_new.BillingAccountAssociation();
   		smb_CustomerManagementCommonTypes_v4_new.BusinessCreditIdentity g7 = new smb_CustomerManagementCommonTypes_v4_new.BusinessCreditIdentity();
   		smb_CustomerManagementCommonTypes_v4_new.BusinessCreditInformation g8 = new smb_CustomerManagementCommonTypes_v4_new.BusinessCreditInformation();
   		smb_CustomerManagementCommonTypes_v4_new.Cheque g9 = new smb_CustomerManagementCommonTypes_v4_new.Cheque();
   		smb_CustomerManagementCommonTypes_v4_new.ConsumerName g10 = new smb_CustomerManagementCommonTypes_v4_new.ConsumerName();
   		smb_CustomerManagementCommonTypes_v4_new.CreditCard g11 = new smb_CustomerManagementCommonTypes_v4_new.CreditCard();
   		smb_CustomerManagementCommonTypes_v4_new.CreditCheckResult g12 = new smb_CustomerManagementCommonTypes_v4_new.CreditCheckResult();
   		smb_CustomerManagementCommonTypes_v4_new.CreditCheckResultDeposit g13 = new smb_CustomerManagementCommonTypes_v4_new.CreditCheckResultDeposit();
   		smb_CustomerManagementCommonTypes_v4_new.CreditDecision g14 = new smb_CustomerManagementCommonTypes_v4_new.CreditDecision();
   		smb_CustomerManagementCommonTypes_v4_new.DebtSummary g15 = new smb_CustomerManagementCommonTypes_v4_new.DebtSummary();
   		smb_CustomerManagementCommonTypes_v4_new.EligibilityResult g16 = new smb_CustomerManagementCommonTypes_v4_new.EligibilityResult();
   		smb_CustomerManagementCommonTypes_v4_new.FinancialHistory g17 = new smb_CustomerManagementCommonTypes_v4_new.FinancialHistory();
   		smb_CustomerManagementCommonTypes_v4_new.FollowUp g18 = new smb_CustomerManagementCommonTypes_v4_new.FollowUp();
   		smb_CustomerManagementCommonTypes_v4_new.FollowUpText g19 = new smb_CustomerManagementCommonTypes_v4_new.FollowUpText();
   		smb_CustomerManagementCommonTypes_v4_new.Memo g20 = new smb_CustomerManagementCommonTypes_v4_new.Memo();
   		smb_CustomerManagementCommonTypes_v4_new.MonthlyFinancialActivity g21 = new smb_CustomerManagementCommonTypes_v4_new.MonthlyFinancialActivity();
   		smb_CustomerManagementCommonTypes_v4_new.PaymentMethod g22 = new smb_CustomerManagementCommonTypes_v4_new.PaymentMethod();
   		smb_CustomerManagementCommonTypes_v4_new.PaymentMethodAuthorization g23 = new smb_CustomerManagementCommonTypes_v4_new.PaymentMethodAuthorization();
   		smb_CustomerManagementCommonTypes_v4_new.PersonalCreditInformation g24 = new smb_CustomerManagementCommonTypes_v4_new.PersonalCreditInformation();
   		smb_CustomerManagementCommonTypes_v4_new.ReferToCreditAnalyst g25 = new smb_CustomerManagementCommonTypes_v4_new.ReferToCreditAnalyst();
   		smb_CustomerManagementCommonTypes_v4_new.RewardRedemptionResult g26 = new smb_CustomerManagementCommonTypes_v4_new.RewardRedemptionResult();
   		smb_CustomerManagementCommonTypes_v4_new.ServiceDeclined g27 = new smb_CustomerManagementCommonTypes_v4_new.ServiceDeclined();
   		smb_CustomerManagementCommonTypes_v4_new.ValidationMessage g28 = new smb_CustomerManagementCommonTypes_v4_new.ValidationMessage();
   		
   		smb_cust_EndUserIdentity_v1_new smb10 = new smb_cust_EndUserIdentity_v1_new();
   		smb_cust_EndUserIdentity_v1_new.EndUserIdentityHeaderType h1 = new smb_cust_EndUserIdentity_v1_new.EndUserIdentityHeaderType();
   		
   		smb_cust_EnterpriseCommonTypes_v9_new smb11 = new smb_cust_EnterpriseCommonTypes_v9_new();
   		smb_cust_EnterpriseCommonTypes_v9_new.AuditInfo i1 = new smb_cust_EnterpriseCommonTypes_v9_new.AuditInfo();
   		smb_cust_EnterpriseCommonTypes_v9_new.BankAccount i2 = new smb_cust_EnterpriseCommonTypes_v9_new.BankAccount();
   		smb_cust_EnterpriseCommonTypes_v9_new.brandType i3 = new smb_cust_EnterpriseCommonTypes_v9_new.brandType();
   		smb_cust_EnterpriseCommonTypes_v9_new.CodeDescText i4 = new smb_cust_EnterpriseCommonTypes_v9_new.CodeDescText();
   		smb_cust_EnterpriseCommonTypes_v9_new.CreditCard i5 = new smb_cust_EnterpriseCommonTypes_v9_new.CreditCard();
   		smb_cust_EnterpriseCommonTypes_v9_new.Description i6 = new smb_cust_EnterpriseCommonTypes_v9_new.Description();
   		smb_cust_EnterpriseCommonTypes_v9_new.DriversLicense i7 = new smb_cust_EnterpriseCommonTypes_v9_new.DriversLicense();
   		smb_cust_EnterpriseCommonTypes_v9_new.Duration i8 = new smb_cust_EnterpriseCommonTypes_v9_new.Duration();
   		smb_cust_EnterpriseCommonTypes_v9_new.HealthCard i9 = new smb_cust_EnterpriseCommonTypes_v9_new.HealthCard();
   		smb_cust_EnterpriseCommonTypes_v9_new.IndividualName i10 = new smb_cust_EnterpriseCommonTypes_v9_new.IndividualName();
   		smb_cust_EnterpriseCommonTypes_v9_new.MarketSegment i11 = new smb_cust_EnterpriseCommonTypes_v9_new.MarketSegment();
   		smb_cust_EnterpriseCommonTypes_v9_new.Message i12 = new smb_cust_EnterpriseCommonTypes_v9_new.Message();
   		smb_cust_EnterpriseCommonTypes_v9_new.MessageType i13 = new smb_cust_EnterpriseCommonTypes_v9_new.MessageType();
   		smb_cust_EnterpriseCommonTypes_v9_new.MonetaryAmt i14 = new smb_cust_EnterpriseCommonTypes_v9_new.MonetaryAmt();
   		smb_cust_EnterpriseCommonTypes_v9_new.MultilingualCodeDescriptionList i15 = new smb_cust_EnterpriseCommonTypes_v9_new.MultilingualCodeDescriptionList();
   		smb_cust_EnterpriseCommonTypes_v9_new.MultilingualCodeDescTextList i16 = new smb_cust_EnterpriseCommonTypes_v9_new.MultilingualCodeDescTextList();
   		smb_cust_EnterpriseCommonTypes_v9_new.MultilingualDescriptionList i17 = new smb_cust_EnterpriseCommonTypes_v9_new.MultilingualDescriptionList();
   		smb_cust_EnterpriseCommonTypes_v9_new.MultilingualNameList i18 = new smb_cust_EnterpriseCommonTypes_v9_new.MultilingualNameList();
   		smb_cust_EnterpriseCommonTypes_v9_new.Name i19 = new smb_cust_EnterpriseCommonTypes_v9_new.Name();
   		smb_cust_EnterpriseCommonTypes_v9_new.OrganizationName i20 = new smb_cust_EnterpriseCommonTypes_v9_new.OrganizationName();
   		smb_cust_EnterpriseCommonTypes_v9_new.PartyName i21 = new smb_cust_EnterpriseCommonTypes_v9_new.PartyName();
   		smb_cust_EnterpriseCommonTypes_v9_new.Passport i22 = new smb_cust_EnterpriseCommonTypes_v9_new.Passport();
   		smb_cust_EnterpriseCommonTypes_v9_new.ProvincialIdCard i23 = new smb_cust_EnterpriseCommonTypes_v9_new.ProvincialIdCard();
   		smb_cust_EnterpriseCommonTypes_v9_new.Quantity i24 = new smb_cust_EnterpriseCommonTypes_v9_new.Quantity();
   		smb_cust_EnterpriseCommonTypes_v9_new.Rate i25 = new smb_cust_EnterpriseCommonTypes_v9_new.Rate();
   		smb_cust_EnterpriseCommonTypes_v9_new.ResponseMessage i26 = new smb_cust_EnterpriseCommonTypes_v9_new.ResponseMessage();
   		smb_cust_EnterpriseCommonTypes_v9_new.UnknownName i27 = new smb_cust_EnterpriseCommonTypes_v9_new.UnknownName();
   		
   		smb_cust_telus_em_header_v1_new smb12 = new smb_cust_telus_em_header_v1_new();
   		smb_cust_telus_em_header_v1_new.emContainerInfoType j1 = new smb_cust_telus_em_header_v1_new.emContainerInfoType();
   		smb_cust_telus_em_header_v1_new.emHeaderType j2 = new smb_cust_telus_em_header_v1_new.emHeaderType();
   		smb_cust_telus_em_header_v1_new.emRoutingRulesType j3 = new smb_cust_telus_em_header_v1_new.emRoutingRulesType();
   		
   		smb_cust_TelusCommonAddressTypes_v7_new smb13 = new smb_cust_TelusCommonAddressTypes_v7_new();
   		smb_cust_TelusCommonAddressTypes_v7_new.Address k1 = new smb_cust_TelusCommonAddressTypes_v7_new.Address();
   		smb_cust_TelusCommonAddressTypes_v7_new.AlbertaLotBlockPlanAddress k2 = new smb_cust_TelusCommonAddressTypes_v7_new.AlbertaLotBlockPlanAddress();
   		smb_cust_TelusCommonAddressTypes_v7_new.CanadaPostRestrictedCivicAddress k3 = new smb_cust_TelusCommonAddressTypes_v7_new.CanadaPostRestrictedCivicAddress();
   		smb_cust_TelusCommonAddressTypes_v7_new.CanadaPostRestrictedGeneralDeliveryAddress k4 = new smb_cust_TelusCommonAddressTypes_v7_new.CanadaPostRestrictedGeneralDeliveryAddress();
   		smb_cust_TelusCommonAddressTypes_v7_new.CanadaPostRestrictedInternationalAddress k5 = new smb_cust_TelusCommonAddressTypes_v7_new.CanadaPostRestrictedInternationalAddress();
   		smb_cust_TelusCommonAddressTypes_v7_new.CanadaPostRestrictedMilitaryAddress k6 = new smb_cust_TelusCommonAddressTypes_v7_new.CanadaPostRestrictedMilitaryAddress();
   		smb_cust_TelusCommonAddressTypes_v7_new.CanadaPostRestrictedPoBoxAddress k7 = new smb_cust_TelusCommonAddressTypes_v7_new.CanadaPostRestrictedPoBoxAddress();
   		smb_cust_TelusCommonAddressTypes_v7_new.CanadaPostRestrictedRuralRouteAddress k8 = new smb_cust_TelusCommonAddressTypes_v7_new.CanadaPostRestrictedRuralRouteAddress();
   		smb_cust_TelusCommonAddressTypes_v7_new.CanadaPostRestrictedUsaAddress k9 = new smb_cust_TelusCommonAddressTypes_v7_new.CanadaPostRestrictedUsaAddress();
   		smb_cust_TelusCommonAddressTypes_v7_new.CivicAddress k10 = new smb_cust_TelusCommonAddressTypes_v7_new.CivicAddress();
   		smb_cust_TelusCommonAddressTypes_v7_new.DominionLandSurveyAddress k11 = new smb_cust_TelusCommonAddressTypes_v7_new.DominionLandSurveyAddress();
   		smb_cust_TelusCommonAddressTypes_v7_new.GeneralDeliveryAddress k12 = new smb_cust_TelusCommonAddressTypes_v7_new.GeneralDeliveryAddress();
   		smb_cust_TelusCommonAddressTypes_v7_new.GeoSpatial k13 = new smb_cust_TelusCommonAddressTypes_v7_new.GeoSpatial();
   		smb_cust_TelusCommonAddressTypes_v7_new.MilitaryAddress k14 = new smb_cust_TelusCommonAddressTypes_v7_new.MilitaryAddress();
   		smb_cust_TelusCommonAddressTypes_v7_new.PoBoxAddress k15 = new smb_cust_TelusCommonAddressTypes_v7_new.PoBoxAddress();
   		smb_cust_TelusCommonAddressTypes_v7_new.ResourceLocationAddress k16 = new smb_cust_TelusCommonAddressTypes_v7_new.ResourceLocationAddress();
   		smb_cust_TelusCommonAddressTypes_v7_new.RestrictedServiceAddress k17 = new smb_cust_TelusCommonAddressTypes_v7_new.RestrictedServiceAddress();
   		smb_cust_TelusCommonAddressTypes_v7_new.RuralAddress k18 = new smb_cust_TelusCommonAddressTypes_v7_new.RuralAddress();
   		smb_cust_TelusCommonAddressTypes_v7_new.ServiceAddress k19 = new smb_cust_TelusCommonAddressTypes_v7_new.ServiceAddress();
   		smb_cust_TelusCommonAddressTypes_v7_new.StationAddress k20 = new smb_cust_TelusCommonAddressTypes_v7_new.StationAddress();
   		smb_cust_TelusCommonAddressTypes_v7_new.UnclassifiedAddress k21 = new smb_cust_TelusCommonAddressTypes_v7_new.UnclassifiedAddress();
   		
   		smb_cust_ping_v1_new smb14 = new smb_cust_ping_v1_new() ;
   		smb_cust_ping_v1_new.ping_element l1 = new smb_cust_ping_v1_new.ping_element();
   		smb_cust_ping_v1_new.pingResponse_element l2 = new smb_cust_ping_v1_new.pingResponse_element();
   		
   		smb_cust_common_types_2_1_new smb15 = new smb_cust_common_types_2_1_new();
   		smb_cust_common_types_2_1_new.ActivationOptionType m1= new smb_cust_common_types_2_1_new.ActivationOptionType();
   		smb_cust_common_types_2_1_new.Address m2= new smb_cust_common_types_2_1_new.Address();
   		smb_cust_common_types_2_1_new.AddressValidationResult m3= new smb_cust_common_types_2_1_new.AddressValidationResult();
   		smb_cust_common_types_2_1_new.BankAccount m4= new smb_cust_common_types_2_1_new.BankAccount();
   		smb_cust_common_types_2_1_new.BillingAccountAssociation m5= new smb_cust_common_types_2_1_new.BillingAccountAssociation();
   		smb_cust_common_types_2_1_new.BusinessCreditInformation m6= new smb_cust_common_types_2_1_new.BusinessCreditInformation();
   		smb_cust_common_types_2_1_new.Cheque m7= new smb_cust_common_types_2_1_new.Cheque();
   		smb_cust_common_types_2_1_new.CodeMessage m8= new smb_cust_common_types_2_1_new.CodeMessage();
   		smb_cust_common_types_2_1_new.Commitment m9= new smb_cust_common_types_2_1_new.Commitment();
   		smb_cust_common_types_2_1_new.ConsumerName m10= new smb_cust_common_types_2_1_new.ConsumerName();
   		smb_cust_common_types_2_1_new.CreditCard m11= new smb_cust_common_types_2_1_new.CreditCard();
   		smb_cust_common_types_2_1_new.CreditCheckResult m12= new smb_cust_common_types_2_1_new.CreditCheckResult();
   		smb_cust_common_types_2_1_new.CreditCheckResultDeposit m13= new smb_cust_common_types_2_1_new.CreditCheckResultDeposit();
   		smb_cust_common_types_2_1_new.DebtSummary m14= new smb_cust_common_types_2_1_new.DebtSummary();
   		smb_cust_common_types_2_1_new.EligibilityResultType m15= new smb_cust_common_types_2_1_new.EligibilityResultType();
   		smb_cust_common_types_2_1_new.FinancialHistory m16= new smb_cust_common_types_2_1_new.FinancialHistory();
   		smb_cust_common_types_2_1_new.FleetIdentity m17= new smb_cust_common_types_2_1_new.FleetIdentity();
   		smb_cust_common_types_2_1_new.FollowUp m18= new smb_cust_common_types_2_1_new.FollowUp();
   		smb_cust_common_types_2_1_new.FollowUpText m19= new smb_cust_common_types_2_1_new.FollowUpText();
   		smb_cust_common_types_2_1_new.MemberIdentity m20= new smb_cust_common_types_2_1_new.MemberIdentity();
   		smb_cust_common_types_2_1_new.Memo m21= new smb_cust_common_types_2_1_new.Memo();
   		smb_cust_common_types_2_1_new.MonthlyFinancialActivity m22= new smb_cust_common_types_2_1_new.MonthlyFinancialActivity();
   		smb_cust_common_types_2_1_new.PaymentMethod m23= new smb_cust_common_types_2_1_new.PaymentMethod();
   		smb_cust_common_types_2_1_new.PersonalCreditInformation m24= new smb_cust_common_types_2_1_new.PersonalCreditInformation();
   		smb_cust_common_types_2_1_new.PortConsumerNameInfo m25= new smb_cust_common_types_2_1_new.PortConsumerNameInfo();
   		smb_cust_common_types_2_1_new.ReferralReasonType m26= new smb_cust_common_types_2_1_new.ReferralReasonType();
   		smb_cust_common_types_2_1_new.SubscriptionRole m27= new smb_cust_common_types_2_1_new.SubscriptionRole();
   		smb_cust_common_types_2_1_new.URN m28= new smb_cust_common_types_2_1_new.URN();
   		smb_cust_common_types_2_1_new.ValidationMessage m29= new smb_cust_common_types_2_1_new.ValidationMessage();
   		 		
   		  
    }
    
    static testMethod void test_Smb_ContractService() {
    	
    	smb_ContractManagementServiceRequestResp smb1 = new smb_ContractManagementServiceRequestResp();
    	smb_ContractManagementServiceRequestResp.calculateTerminationCharges_element a1 = new smb_ContractManagementServiceRequestResp.calculateTerminationCharges_element();
    	smb_ContractManagementServiceRequestResp.calculateTerminationChargesResponse_element a2 = new smb_ContractManagementServiceRequestResp.calculateTerminationChargesResponse_element();
    	smb_ContractManagementServiceRequestResp.cancelContractSubmission_element a3 = new smb_ContractManagementServiceRequestResp.cancelContractSubmission_element();
    	smb_ContractManagementServiceRequestResp.cancelContractSubmissionResponse_element a4 = new smb_ContractManagementServiceRequestResp.cancelContractSubmissionResponse_element();
    	smb_ContractManagementServiceRequestResp.ContractData a5 = new smb_ContractManagementServiceRequestResp.ContractData();
    	smb_ContractManagementServiceRequestResp.createContractSubmission_element a6 = new smb_ContractManagementServiceRequestResp.createContractSubmission_element();
    	smb_ContractManagementServiceRequestResp.createContractSubmissionResponse_element a7 = new smb_ContractManagementServiceRequestResp.createContractSubmissionResponse_element();
    	smb_ContractManagementServiceRequestResp.findContractAmendmentsByAssociateNum_element a8 = new smb_ContractManagementServiceRequestResp.findContractAmendmentsByAssociateNum_element();
    	smb_ContractManagementServiceRequestResp.findContractAmendmentsByAssociateNumResponse_element a9 = new smb_ContractManagementServiceRequestResp.findContractAmendmentsByAssociateNumResponse_element();
    	smb_ContractManagementServiceRequestResp.findContractData_element a10 = new smb_ContractManagementServiceRequestResp.findContractData_element();
    	smb_ContractManagementServiceRequestResp.findContractDataResponse_element a11 = new smb_ContractManagementServiceRequestResp.findContractDataResponse_element();
    	smb_ContractManagementServiceRequestResp.findContractsByAgentId_element a12 = new smb_ContractManagementServiceRequestResp.findContractsByAgentId_element();
    	smb_ContractManagementServiceRequestResp.findContractsByAgentIdResponse_element a13 = new smb_ContractManagementServiceRequestResp.findContractsByAgentIdResponse_element();
    	smb_ContractManagementServiceRequestResp.findContractsByCustomerId_element a14 = new smb_ContractManagementServiceRequestResp.findContractsByCustomerId_element();
    	smb_ContractManagementServiceRequestResp.findContractsByCustomerIdResponse_element a15 = new smb_ContractManagementServiceRequestResp.findContractsByCustomerIdResponse_element();
    	smb_ContractManagementServiceRequestResp.getContract_element a16 = new smb_ContractManagementServiceRequestResp.getContract_element();
    	smb_ContractManagementServiceRequestResp.getContractDocument_element a17 = new smb_ContractManagementServiceRequestResp.getContractDocument_element();
    	smb_ContractManagementServiceRequestResp.getContractDocumentMetaData_element a18 = new smb_ContractManagementServiceRequestResp.getContractDocumentMetaData_element();
    	smb_ContractManagementServiceRequestResp.getContractDocumentMetaDataResponse_element a19 = new smb_ContractManagementServiceRequestResp.getContractDocumentMetaDataResponse_element();
    	smb_ContractManagementServiceRequestResp.getContractDocumentResponse_element a20 = new smb_ContractManagementServiceRequestResp.getContractDocumentResponse_element();
    	smb_ContractManagementServiceRequestResp.getContractResponse_element a21 = new smb_ContractManagementServiceRequestResp.getContractResponse_element();
    	smb_ContractManagementServiceRequestResp.replaceContractSubmission_element a22 = new smb_ContractManagementServiceRequestResp.replaceContractSubmission_element();
    	smb_ContractManagementServiceRequestResp.replaceContractSubmissionResponse_element a23 = new smb_ContractManagementServiceRequestResp.replaceContractSubmissionResponse_element();
    	smb_ContractManagementServiceRequestResp.TerminationChargesInformation a24 = new smb_ContractManagementServiceRequestResp.TerminationChargesInformation();
    	smb_ContractManagementServiceRequestResp.TerminationChargesParameter a25 = new smb_ContractManagementServiceRequestResp.TerminationChargesParameter();
    	smb_ContractManagementServiceRequestResp.triggerResendContract_element a26 = new smb_ContractManagementServiceRequestResp.triggerResendContract_element();
    	smb_ContractManagementServiceRequestResp.triggerResendContractResponse_element a27 = new smb_ContractManagementServiceRequestResp.triggerResendContractResponse_element();
    	smb_ContractManagementServiceRequestResp.updateContractSubmission_element a28 = new smb_ContractManagementServiceRequestResp.updateContractSubmission_element();
    	smb_ContractManagementServiceRequestResp.updateContractSubmissionResponse_element a29 = new smb_ContractManagementServiceRequestResp.updateContractSubmissionResponse_element();
    	smb_ContractManagementServiceRequestResp.verifyValidReplacement_element a30  = new smb_ContractManagementServiceRequestResp.verifyValidReplacement_element();
    	smb_ContractManagementServiceRequestResp.verifyValidReplacementResponse_element a31 = new smb_ContractManagementServiceRequestResp.verifyValidReplacementResponse_element();
    		
    	smb_ContractManagementSvcTypes_v2 smb2 = new smb_ContractManagementSvcTypes_v2();
    	smb_ContractManagementSvcTypes_v2.AssignedProductIdList b1 = new smb_ContractManagementSvcTypes_v2.AssignedProductIdList();
    	smb_ContractManagementSvcTypes_v2.BillingTelephoneNumList b2 = new smb_ContractManagementSvcTypes_v2.BillingTelephoneNumList();
    	smb_ContractManagementSvcTypes_v2.Contract b3 = new smb_ContractManagementSvcTypes_v2.Contract();
    	smb_ContractManagementSvcTypes_v2.ContractAmendment b4 = new smb_ContractManagementSvcTypes_v2.ContractAmendment();
    	smb_ContractManagementSvcTypes_v2.ContractAmendmentList b5 = new smb_ContractManagementSvcTypes_v2.ContractAmendmentList();
    	smb_ContractManagementSvcTypes_v2.ContractAttribute b6 = new smb_ContractManagementSvcTypes_v2.ContractAttribute();
    	smb_ContractManagementSvcTypes_v2.ContractAttributeList b7 = new smb_ContractManagementSvcTypes_v2.ContractAttributeList();
    	smb_ContractManagementSvcTypes_v2.ContractDetail b8 = new smb_ContractManagementSvcTypes_v2.ContractDetail();
    	smb_ContractManagementSvcTypes_v2.ContractDetailList b9 = new smb_ContractManagementSvcTypes_v2.ContractDetailList();
    	smb_ContractManagementSvcTypes_v2.ContractDocument b10 = new smb_ContractManagementSvcTypes_v2.ContractDocument();
    	smb_ContractManagementSvcTypes_v2.ContractDocumentList b11 = new smb_ContractManagementSvcTypes_v2.ContractDocumentList();
    	smb_ContractManagementSvcTypes_v2.ContractList b12 = new smb_ContractManagementSvcTypes_v2.ContractList();
    	smb_ContractManagementSvcTypes_v2.ContractRequest b13 = new smb_ContractManagementSvcTypes_v2.ContractRequest();
    	smb_ContractManagementSvcTypes_v2.ContractRequestDetail b14 = new smb_ContractManagementSvcTypes_v2.ContractRequestDetail();
    	smb_ContractManagementSvcTypes_v2.ContractRequestDetailList b15 = new smb_ContractManagementSvcTypes_v2.ContractRequestDetailList();
    	smb_ContractManagementSvcTypes_v2.CustomerSignor b16 = new smb_ContractManagementSvcTypes_v2.CustomerSignor();
    	smb_ContractManagementSvcTypes_v2.LegacyContractIdList b17 = new smb_ContractManagementSvcTypes_v2.LegacyContractIdList();
    	smb_ContractManagementSvcTypes_v2.OldAssignedProductIdList b18 = new smb_ContractManagementSvcTypes_v2.OldAssignedProductIdList();
    	smb_ContractManagementSvcTypes_v2.ServiceAddress b19 = new smb_ContractManagementSvcTypes_v2.ServiceAddress();
    	smb_ContractManagementSvcTypes_v2.ServiceAddressList b20 = new smb_ContractManagementSvcTypes_v2.ServiceAddressList();
    	smb_ContractManagementSvcTypes_v2.ServiceDetail b21 = new smb_ContractManagementSvcTypes_v2.ServiceDetail();    	
    	smb_ContractManagementSvcTypes_v2.ServiceDetailList b22 = new smb_ContractManagementSvcTypes_v2.ServiceDetailList();
    	
   		smb_EnterpriseCommonTypes_v6 smb3 = new smb_EnterpriseCommonTypes_v6();
   		smb_EnterpriseCommonTypes_v6.AuditInfo c1 = new smb_EnterpriseCommonTypes_v6.AuditInfo();
   		smb_EnterpriseCommonTypes_v6.brandType c2 = new smb_EnterpriseCommonTypes_v6.brandType();
   		smb_EnterpriseCommonTypes_v6.CodeDescText c3 = new smb_EnterpriseCommonTypes_v6.CodeDescText();   		
   		smb_EnterpriseCommonTypes_v6.CodeDescTextList c4 = new smb_EnterpriseCommonTypes_v6.CodeDescTextList();
   		smb_EnterpriseCommonTypes_v6.Description c5 = new smb_EnterpriseCommonTypes_v6.Description();
   		smb_EnterpriseCommonTypes_v6.Message c6 = new smb_EnterpriseCommonTypes_v6.Message();
   		smb_EnterpriseCommonTypes_v6.MessageType c7 = new smb_EnterpriseCommonTypes_v6.MessageType();
   		smb_EnterpriseCommonTypes_v6.MultilingualCodeDescriptiontList c8 = new smb_EnterpriseCommonTypes_v6.MultilingualCodeDescriptiontList();
   		smb_EnterpriseCommonTypes_v6.MultilingualCodeDescTextList c9 = new smb_EnterpriseCommonTypes_v6.MultilingualCodeDescTextList();
   		smb_EnterpriseCommonTypes_v6.MultilingualDescriptiontList c10 = new smb_EnterpriseCommonTypes_v6.MultilingualDescriptiontList();
   		smb_EnterpriseCommonTypes_v6.MultilingualNameList c11 = new smb_EnterpriseCommonTypes_v6.MultilingualNameList();
   		smb_EnterpriseCommonTypes_v6.Name c12 = new smb_EnterpriseCommonTypes_v6.Name();
   		smb_EnterpriseCommonTypes_v6.ResponseMessage c13 = new smb_EnterpriseCommonTypes_v6.ResponseMessage();
    	
    	smb_Ping_v1 smb4 = new smb_Ping_v1();
    	smb_Ping_v1.ping_element d1= new smb_Ping_v1.ping_element();
    	smb_Ping_v1.pingResponse_element d2 = new smb_Ping_v1.pingResponse_element();
    	
    	smb_ProductCommon_v1 smb5 = new smb_ProductCommon_v1();
    	smb_ProductCommon_v1.Product e1 = new smb_ProductCommon_v1.Product();
    	smb_ProductCommon_v1.ProductBasePrice e2 = new smb_ProductCommon_v1.ProductBasePrice();
    	smb_ProductCommon_v1.ProductChannelCost e3 = new smb_ProductCommon_v1.ProductChannelCost();    
    }
    
    static testMethod void test_Smb_CPService() {
    		
    	smb_ping_CP smb1 = new smb_ping_CP();    	
    	smb_ping_CP.ping_element a1 = new smb_ping_CP.ping_element();
    	smb_ping_CP.pingResponse_element a2 = new smb_ping_CP.pingResponse_element();
    	
    	smb_Exceptions_CP smb2 = new smb_Exceptions_CP();
    	smb_Exceptions_CP.PolicyException b1 = new smb_Exceptions_CP.PolicyException();
    	smb_Exceptions_CP.ServiceException b2 = new smb_Exceptions_CP.ServiceException();
    	
    	smb_EntAddressValidateSvcReq_CP smb3 = new smb_EntAddressValidateSvcReq_CP();
    	smb_EntAddressValidateSvcReq_CP.formatCanadianPostalAddress_element c1 =  new smb_EntAddressValidateSvcReq_CP.formatCanadianPostalAddress_element();
    	smb_EntAddressValidateSvcReq_CP.formatCanadianPostalAddressResponse_element c2 =  new smb_EntAddressValidateSvcReq_CP.formatCanadianPostalAddressResponse_element();
    	smb_EntAddressValidateSvcReq_CP.formattingParametersType c3 =  new smb_EntAddressValidateSvcReq_CP.formattingParametersType();
    	smb_EntAddressValidateSvcReq_CP.verificationErrorType c4 =  new smb_EntAddressValidateSvcReq_CP.verificationErrorType();
    	smb_EntAddressValidateSvcReq_CP.verificationResultStateType c5 =  new smb_EntAddressValidateSvcReq_CP.verificationResultStateType();
    	smb_EntAddressValidateSvcReq_CP.verificationResultType c6 =  new smb_EntAddressValidateSvcReq_CP.verificationResultType();
    	smb_EntAddressValidateSvcReq_CP.verifyCanadianPostalAddress_element c7 =  new smb_EntAddressValidateSvcReq_CP.verifyCanadianPostalAddress_element();
    	smb_EntAddressValidateSvcReq_CP.verifyCanadianPostalAddressResponse_element c8 =  new smb_EntAddressValidateSvcReq_CP.verifyCanadianPostalAddressResponse_element();
    	
    	smb_CustomerCommon_CP smb4 = new smb_CustomerCommon_CP();
    	smb_CustomerCommon_CP.Address d1 = new smb_CustomerCommon_CP.Address();
    	smb_CustomerCommon_CP.CustomerMarketSegment d2 = new smb_CustomerCommon_CP.CustomerMarketSegment();
    	smb_CustomerCommon_CP.Name d3 = new smb_CustomerCommon_CP.Name();
    	
    }
}