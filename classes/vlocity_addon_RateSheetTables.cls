global with sharing class vlocity_addon_RateSheetTables implements vlocity_cmt.VlocityOpenInterface{
    public Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {
        Boolean success = true;
        if(methodName == 'buildDocumentSectionContent')
        { 
            Id contractId = (Id) inputMap.get('contextObjId');
            system.debug('contractId:'+contractId);
            
            If(contractId != null){
                
                map<string,object> mapTableInfo=vlocity_addon_DynamicTableHelper.getRateSheetData(contractId);
                system.debug('mapTableInfo::'+mapTableInfo);   
                system.debug('mapTableInfo.size():'+mapTableInfo.size());
                if(mapTableInfo.size() > 0 && !mapTableInfo.isEmpty()){
                    
                    outMap.putAll(mapTableInfo);
                }
                else {
                    return false;
                }
            }else {
                return false ;
            }
        }       
        return success; 
    }
    
}