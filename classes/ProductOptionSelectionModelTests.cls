/**
 * Unit tests for <code>ProductOptionSelectionModel</code> class.
 * 
 * @author Max Rudman
 * @since 4/23/2011
 */
@isTest
private class ProductOptionSelectionModelTests {
	private static Product2 product;
	private static Product2 option1;
	private static Product2 option2;
	private static List<SBQQ__ProductOption__c> options;
	
	testMethod static void testGetters() {
		setUp();
		
		ProductModel pmodel = ProductModel.loadById(product.Id, null, null, null);
		ProductOptionModel option = pmodel.getAllOptions()[0];
		ProductOptionSelectionModel target = new ProductOptionSelectionModel(pmodel);
		String[] cfields = target.getConfigurationFieldNames();
		System.assertEquals(2, cfields.size());
		System.assertEquals('Field1',cfields[0]);
	}
	
	testMethod static void testAddAndRemove() {
		setUp();
		
		ProductModel pmodel = ProductModel.loadById(product.Id, null, null, null);
		ProductOptionModel option = pmodel.getAllOptions()[0];
		ProductOptionSelectionModel target = new ProductOptionSelectionModel(pmodel);
		ProductOptionSelectionModel.Selection sel = target.addSelection(option);
		System.assert(sel.selected);
		System.assert(sel == target.findSelectionById(sel.id));
		System.assert(target.getSelectedOptionIds().contains(option.id));
		
		target.removeSelections(option);
		System.assert(!target.getSelectedOptionIds().contains(option.id));
	}
	
	private static void setUp() {
		product = new Product2(Name='Test',SBQQ__ConfigurationFields__c='Field1,Field2');
		option1 = new Product2(Name='Option1',ProductCode='C1');
		option2 = new Product2(Name='Option2',ProductCode='C2');
		insert new List<Product2>{product,option1,option2};
		
		options = new List<SBQQ__ProductOption__c>();
		options.add(new SBQQ__ProductOption__c(SBQQ__ConfiguredSKU__c=product.Id,SBQQ__OptionalSKU__c=option1.Id,SBQQ__Number__c=1,SBQQ__UnitPrice__c=100));
		options.add(new SBQQ__ProductOption__c(SBQQ__ConfiguredSKU__c=product.Id,SBQQ__OptionalSKU__c=option2.Id,SBQQ__Number__c=2,SBQQ__UnitPrice__c=200));
		insert options;
	}
}