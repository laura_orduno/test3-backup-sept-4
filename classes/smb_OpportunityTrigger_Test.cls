@isTest (SeeAllData = true)
private class smb_OpportunityTrigger_Test {
    static testMethod void Test_smb_OpportunityTrigger() {

        //SMB_Order_Stage_for_WFM_Cancel__c custSettOrderStage =  new SMB_Order_Stage_for_WFM_Cancel__c(Name='Opportunity Stage',data__c='Quote Expired,Contract Acceptance Expired,Contract Expired');
        //insert custSettOrderStage;
        
        Account accObj = smb_test_utility.createAccount('', true);
        Contact contObj = smb_test_utility.createContact('', accObj.Id, true);
        Opportunity oppObj = smb_test_utility.createOpportunity('SMB_Care_Opportunity', accObj.Id, contObj.Id, true);
        

        Test.startTest();
        Product2 productObj = new Product2(name='AstadiaTestProduct',Sterling_Item_ID__c='p1',product_type__c='VOICE');
        insert productObj ;
        
        Pricebook2 pricebookObj = [select id from Pricebook2 where isStandard=true];
        
        PricebookEntry pbeObj = new PricebookEntry(pricebook2id = pricebookObj.id, product2id = productObj.id,unitprice=1.0,isActive=true);
        insert pbeObj ;
        
        Work_Order__c workOrderObj = new Work_Order__c(Job_Type__c = 'Test' ,Install_Type__c = 'Fieldwork', Status__c = 'Reserved', Product_Name__c = 'Test Product Name',
                         Scheduled_Datetime__c = DateTime.now() , Time_Slot__c = 'Test Slot', Scheduled_Due_Date_Location__c = 'Test Location', Permanent_Comments__c = 'test test'
                         );
    workOrderObj.Opportunity__c= oppObj.id;
        insert workOrderObj ;
        
        OpportunityLineItem oliObj = new OpportunityLineItem(OpportunityId = OppObj.Id, PriceBookEntryId = pbeObj .Id, Quantity = 1, UnitPrice = 1);
        oliObj.Service_Street__c  = 'Test Street 123';
    oliObj.Work_Order__c = workOrderObj.id;
        insert oliObj;
        
        oppObj.StageName = 'Contract Acceptance Expired';
        oppObj.Stage_update_time__c=system.now().addminutes(1);
        update oppObj;
        
        Test.stopTest();
          

    } 
    // Code changes for Bo enhancement by Amit Rana Dt 06 Nov 2015
     static testMethod void createOrder() 
    {        
        Account accObj = smb_test_utility.createAccount('', false);
        accObj.Inactive__c=true;
        accObj.CustProfId__c = '123456';  
        accObj.Marketing_SubSegDesc_Obsolete_Snapshot__c='test';
        insert accObj;
        
        Contact contObj = smb_test_utility.createContact('', accObj.Id, true);
       Credit_Profile__c objCARProfile = new Credit_Profile__c();
       objCARProfile.Mandatory_Assessment__c = true;
       objCARProfile.Delinquent_Accounts__c= true;
       objCARProfile.Valid_Legal_Name__c= 'Yes';
       objCARProfile.Corporate_Registry_Status__c= 'Active';
       objCARProfile.Risk_Indicator__c= 'MRC 5M - NRC 5M-EXEMPT-ENT-EXEMPT';
      // objCARProfile.Legal_Name_On_File__c= ;
       //objCARProfile.Trade_Name_on_File_O_A__c= ;
       objCARProfile.Legal_Name_Should_Be__c= 'test';
       objCARProfile.Last_Validation_Date__c= Date.today();
       Insert objCARProfile; 
 
        Credit_Assessment__c objCAR = New Credit_Assessment__c();
        objCAR.CAR_Status__c = 'In Progress';
        //objCAR.CAR_Result__c = 'Denied';
        objCAR.Originator_Email__c = 'Amit.rana@telus.com';
        objCAR.Originator_phone__c = '(999) 999-9999';
        objCAR.Legal_Entity_Type__c = 'Sole Proprietor (PR)';
        objCAR.Street_Address_Head_Office__c='25 york';
        objCAR.City_Head_Office__c='on';
        objCAR.Province_State_Head_Office__c='on';
        objCAR.Country_Head_Office__c='ca';
        objCAR.Postal_Code_Head_Office__c='m1w2m5';
        objCAR.Registration_Jurisdiction__c = 'Ontario';
        objCAR.Registration_jurisdiction_country__c = 'Canada';
        objCAR.TELUS_Health_Customer__c = 'yes';
        objCAR.Credit_Bureau_Check_Accepted__c = 'no';
        objCAR.CAR_Account__c = accObj.id;
        objCAR.Credit_Profile__c = objCARProfile.id;
        objCAR.Customer_Segmentation_Code__c = 'SM';
        insert objCAR;
        
        smb_test_utility.createCustomSettingData();
        SMB_DummyWebServiceResponse_Test.intializeMockWebservice();       
        Opportunity testOpp_1 = smb_test_utility.createOpportunity('SMB_Care_Opportunity', accObj.Id, contObj.Id, false); 
        testOpp_1.Type = 'New Provide/Upgrade Order';
        testOpp_1.Credit_Assessment__c = objCAR.id;
        testOpp_1.StageName = 'Order Request On Hold (TELUS)';
        insert testOpp_1;
       
       
        OpportunityContactRole opprole = new OpportunityContactRole(opportunityID = testOpp_1.Id,contactId = testOpp_1.Contract_Signor__c,isPrimary = true);
        insert opprole;
        system.debug(opprole);
        smb_test_utility.createPQLI('NewPQL',testOpp_1.Id);
        PageReference pageref = Page.SMB_WO_SelectAvailableDate;
        Test.setCurrentPage(pageref);        
        pageref.getparameters().put('OppID', testOpp_1.Id);
        ApexPages.StandardController sc_1 = new ApexPages.standardController(testOpp_1);
        Work_Order__c workOrder = new Work_Order__c(Opportunity__c=testOpp_1.id,Job_Type__c = 'Test' ,Install_Type__c = 'Fieldwork', Status__c = 'Reserved', Product_Name__c = 'Test Product Name',
                                 Scheduled_Datetime__c = DateTime.now() , Time_Slot__c = 'Test Slot', Unlinked_Work_Order__c = True ,WFM_Number__c = 'Test Number', Scheduled_Due_Date_Location__c = 'Test Location', Permanent_Comments__c = 'test test');
        insert workOrder;
       objCAR.CAR_Status__c = 'Completed';
        objCAR.CAR_Result__c = 'Approved';
        Test.startTest();
        Update objCAR;
        Test.stopTest(); 
    }
}