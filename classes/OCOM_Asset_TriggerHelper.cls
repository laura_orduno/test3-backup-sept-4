public without sharing class OCOM_Asset_TriggerHelper {
        Map<string,orderitem> orderItemIdToOrderItemRecMap = new Map<string,orderitem>();
        Map<string,string> addressdetailToAddressIDMap=new Map<string,string>();    
        Map<id,String> orderItemToFMSIDMap = new Map<id,string>();
        Map<string,ID> serviceAccToAddressIDMap=new Map<string,ID>();
        Set<id> RCIDIdsSet = new Set<id>();
        Set<string> smbCareAddressIdsSet = new Set<string>(); 
        List<asset> newAsset = new List<asset>();
    
    Public void UpdateAccountIdinAssetRecBeforeUpsert(Set<id>orderlineitemIdSet,List<Asset> updateAccountIdInAssetRecs)
    {             
       //Extract all OrderLineItems  records corresponding to Assets external Ids
        List<orderitem> OrderRecs =[select id,orderId,Pricebookentry.Product2id,orderMgmt_BPI_Id__c,Pricebookentry.Product2.Name,vlocity_cmt__RecurringCharge__c,order.AccountId,order.FMS_Address_ID__c,vlocity_cmt__ProvisioningStatus__c,orderMgmtId__c,vlocity_cmt__ServiceAccountId__c,vlocity_cmt__BillingAccountId__c,order.service_address__c from Orderitem where id =:orderlineitemIdSet];
        
        for(Orderitem orderItemRec:OrderRecs)
        {  
            string orderItemID=orderItemRec.id;
            if(orderItemID.length()>15)
            orderItemID=orderItemID.substring(0,15);
            orderItemIdToOrderItemRecMap.put(orderItemID,orderItemRec);
            smbCareAddressIdsSet.add(orderItemRec.order.service_address__c);            
            system.debug(' order service add'+orderItemRec);
        }    
        
         //set service location and service Account
           system.debug('smbCareAddressIdsSet '+ smbCareAddressIdsSet);  
         getServiceLocationAddressAndServiceAccount(smbCareAddressIdsSet);
         system.debug('serviceAccToAddressIDMap '+ serviceAccToAddressIDMap);        
         system.debug('orderItemIdToOrderItemRecMap '+ orderItemIdToOrderItemRecMap);
         system.debug('addressdetailToAddressIDMap'+ addressdetailToAddressIDMap);
         system.debug('outside asset OrderItem '+updateAccountIdInAssetRecs);
       for(Asset assetRec: updateAccountIdInAssetRecs){   
       
            Orderitem orderRec = orderItemIdToOrderItemRecMap.get(assetRec.vlocity_cmt__AssetReferenceId__c);
            system.debug('inside asset OrderItem '+orderRec);
            if((null != serviceAccToAddressIDMap) && (serviceAccToAddressIDMap.size()>0) && (null!= orderRec) && (null!= orderRec.order) &&  (null!= orderRec.order.service_address__c)){
             assetRec.vlocity_cmt__ServiceAccountId__c= serviceAccToAddressIDMap.get(orderRec.order.service_address__c);
            }
            if(null!= orderRec){
                 assetRec.vlocity_cmt__BillingAccountId__c= orderRec.vlocity_cmt__BillingAccountId__c;
             assetRec.orderMgmt_BPI_Id__c=orderRec.orderMgmt_BPI_Id__c; 
          //   assetRec.MCM_External_ID__c=null;     
             assetRec.vlocity_cmt__OrderId__c=orderRec.orderId;
             //assetRec.accountID=orderRec.orderId;
             assetRec.vlocity_cmt__OrderProductId__c=orderRec.Id;
            }
            
             
            if(null != addressdetailToAddressIDMap && addressdetailToAddressIDMap.size()>0 && (null!= orderRec) &&  null!= orderRec.order &&  null!= orderRec.order.service_address__c){
             assetRec.Service_Account_Address__c= addressdetailToAddressIDMap.get(orderRec.order.service_address__c);   
            }        
           }
           
    }   

    public map<string,string> getServiceLocationAddressAndServiceAccount(set<string> smbCareAddressIdsSet )
    { 
      for (smbCare_Address__c serviceAddress : [SELECT id, Unit_Number__c, Street_Address__c, City__c,State__c, Postal_Code__c,FMS_Address_ID__c,Account__c  FROM smbCare_Address__c WHERE id=:smbCareAddressIdsSet]) {
           system.debug('5555 '+serviceAddress );
              string address='';
                   if (string.isnotblank(serviceAddress.Unit_Number__c)) {
                        address += serviceAddress.Unit_Number__c;
                   if (string.isnotblank(serviceAddress.Street_Address__c)) {
                         address += ' - ';
                        }
                    }
                    if (string.isnotblank(serviceAddress.Street_Address__c)) {
                        address += serviceAddress.Street_Address__c + ' ';
                    }
                    if (string.isnotblank(serviceAddress.City__c)) {
                        address += serviceAddress.City__c + ',';
                    }
                    if (string.isnotblank(serviceAddress.State__c)) {
                        address += serviceAddress.State__c;
                    }
                    if (string.isnotblank(serviceAddress.Postal_Code__c)) {
                        address += '  ' + serviceAddress.Postal_Code__c;
                    }                    
                    system.debug(' inside address '+address);
                    addressdetailToAddressIDMap.put(serviceAddress.id,address);
                    serviceAccToAddressIDMap.put(serviceAddress.id,serviceAddress.Account__c);
                }           
        system.debug('3333 '+addressdetailToAddressIDMap);
        return addressdetailToAddressIDMap;
    
    }

}