//Generated by wsdl2apex

public class AsyncCms3_1ContractManagementSvc_3 {
    public class AsyncContractManagementSvcPort {
        public String endpoint_x = SMBCare_WebServices__c.getInstance('Contract_EndPoint_v3').Value__c;
       // public String endpoint_x = 'http://cm-smbsvc-west-dv01.tsl.telus.com/telus-ccm-cms-webservices/ContractManagementSvc/CMO/ContractMgmt/SelfServe/ContractManagementSvc_v3_0';
        public Map<String,String> inputHttpHeaders_x;
        public String clientCertName_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/Exceptions_v3', 'cms3_1Exceptions_v3', 'http://telus.com/wsdl/CMO/ContractMgmt/ContractManagementSvc_3', 'cms3_1ContractManagementSvc_3', 'http://xmlschema.tmi.telus.com/xsd/Product/Product/ProductCommon_v1', 'cms3_1ProductCommon_v1', 'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v1', 'cms3_1EnterpriseCommonTypes_v1', 'http://xmlschema.tmi.telus.com/xsd/Customer/Customer/ContractManagementSvcTypes_v3', 'cms3_1ContractManagementSvcTypes_v3', 'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/ping_v1', 'cms3_1ping_v1', 'http://xmlschema.tmi.telus.com/xsd/Customer/BaseTypes/CustomerCommon_v1', 'cms3_1CustomCommunityLoginController', 'http://xmlschema.tmi.telus.com/srv/CMO/ContractMgmt/ContractManagementServiceRequestResponse_v3', 'cms3_1ContractManagementSvcReqRes_v3'};
        public AsyncCms3_1ContractMgmtSvcReqRes_v3.verifyValidReplacementResponse_elementFuture beginVerifyValidReplacement(System.Continuation continuation,String assignedProductId,String contractTerm) {
            cms3_1ContractManagementSvcReqRes_v3.verifyValidReplacement_element request_x = new cms3_1ContractManagementSvcReqRes_v3.verifyValidReplacement_element();
            request_x.assignedProductId = assignedProductId;
            request_x.contractTerm = contractTerm;
            return (AsyncCms3_1ContractMgmtSvcReqRes_v3.verifyValidReplacementResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncCms3_1ContractMgmtSvcReqRes_v3.verifyValidReplacementResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              'verifyValidReplacement',
              'http://xmlschema.tmi.telus.com/srv/CMO/ContractMgmt/ContractManagementServiceRequestResponse_v3',
              'verifyValidReplacement',
              'http://xmlschema.tmi.telus.com/srv/CMO/ContractMgmt/ContractManagementServiceRequestResponse_v3',
              'verifyValidReplacementResponse',
              'cms3_1ContractManagementSvcReqRes_v3.verifyValidReplacementResponse_element'}
            );
        }
        public AsyncCms3_1ping_v1.pingResponse_elementFuture beginPing(System.Continuation continuation) {
            cms3_1ping_v1.ping_element request_x = new cms3_1ping_v1.ping_element();
            return (AsyncCms3_1ping_v1.pingResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncCms3_1ping_v1.pingResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              'ping',
              'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/ping_v1',
              'ping',
              'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/types/ping_v1',
              'pingResponse',
              'cms3_1ping_v1.pingResponse_element'}
            );
        }
        public AsyncCms3_1ContractMgmtSvcReqRes_v3.getContractResponse_elementFuture beginGetContract(System.Continuation continuation,String contractId) {
            cms3_1ContractManagementSvcReqRes_v3.getContract_element request_x = new cms3_1ContractManagementSvcReqRes_v3.getContract_element();
            request_x.contractId = contractId;
            return (AsyncCms3_1ContractMgmtSvcReqRes_v3.getContractResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncCms3_1ContractMgmtSvcReqRes_v3.getContractResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              'getContract',
              'http://xmlschema.tmi.telus.com/srv/CMO/ContractMgmt/ContractManagementServiceRequestResponse_v3',
              'getContract',
              'http://xmlschema.tmi.telus.com/srv/CMO/ContractMgmt/ContractManagementServiceRequestResponse_v3',
              'getContractResponse',
              'cms3_1ContractManagementSvcReqRes_v3.getContractResponse_element'}
            );
        }
        public AsyncCms3_1ContractMgmtSvcReqRes_v3.replaceContractSubmissionResponse_elementFuture beginReplaceContractSubmission(System.Continuation continuation,cms3_1ContractManagementSvcTypes_v3.ContractRequest contractRequest,cms3_1ContractManagementSvcTypes_v3.LegacyContractIdList legacyContractIdList) {
            cms3_1ContractManagementSvcReqRes_v3.replaceContractSubmission_element request_x = new cms3_1ContractManagementSvcReqRes_v3.replaceContractSubmission_element();
            request_x.contractRequest = contractRequest;
            request_x.legacyContractIdList = legacyContractIdList;
            return (AsyncCms3_1ContractMgmtSvcReqRes_v3.replaceContractSubmissionResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncCms3_1ContractMgmtSvcReqRes_v3.replaceContractSubmissionResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              'replaceContractSubmission',
              'http://xmlschema.tmi.telus.com/srv/CMO/ContractMgmt/ContractManagementServiceRequestResponse_v3',
              'replaceContractSubmission',
              'http://xmlschema.tmi.telus.com/srv/CMO/ContractMgmt/ContractManagementServiceRequestResponse_v3',
              'replaceContractSubmissionResponse',
              'cms3_1ContractManagementSvcReqRes_v3.replaceContractSubmissionResponse_element'}
            );
        }
        public AsyncCms3_1ContractMgmtSvcReqRes_v3.cancelContractSubmissionResponse_elementFuture beginCancelContractSubmission(System.Continuation continuation,String assignedProductId,String employeeId) {
            cms3_1ContractManagementSvcReqRes_v3.cancelContractSubmission_element request_x = new cms3_1ContractManagementSvcReqRes_v3.cancelContractSubmission_element();
            request_x.assignedProductId = assignedProductId;
            request_x.employeeId = employeeId;
            return (AsyncCms3_1ContractMgmtSvcReqRes_v3.cancelContractSubmissionResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncCms3_1ContractMgmtSvcReqRes_v3.cancelContractSubmissionResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              'cancelContractSubmission',
              'http://xmlschema.tmi.telus.com/srv/CMO/ContractMgmt/ContractManagementServiceRequestResponse_v3',
              'cancelContractSubmission',
              'http://xmlschema.tmi.telus.com/srv/CMO/ContractMgmt/ContractManagementServiceRequestResponse_v3',
              'cancelContractSubmissionResponse',
              'cms3_1ContractManagementSvcReqRes_v3.cancelContractSubmissionResponse_element'}
            );
        }
        public AsyncCms3_1ContractMgmtSvcReqRes_v3.triggerResendContractResponse_elementFuture beginTriggerResendContract(System.Continuation continuation,String orderId,String contactId,String deliveryMethod,String destination,String autoRenewRequired,String employeeId) {
            cms3_1ContractManagementSvcReqRes_v3.triggerResendContract_element request_x = new cms3_1ContractManagementSvcReqRes_v3.triggerResendContract_element();
            request_x.orderId = orderId;
            request_x.contactId = contactId;
            request_x.deliveryMethod = deliveryMethod;
            request_x.destination = destination;
            request_x.autoRenewRequired = autoRenewRequired;
            request_x.employeeId = employeeId;
            return (AsyncCms3_1ContractMgmtSvcReqRes_v3.triggerResendContractResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncCms3_1ContractMgmtSvcReqRes_v3.triggerResendContractResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              'triggerResendContract',
              'http://xmlschema.tmi.telus.com/srv/CMO/ContractMgmt/ContractManagementServiceRequestResponse_v3',
              'triggerResendContract',
              'http://xmlschema.tmi.telus.com/srv/CMO/ContractMgmt/ContractManagementServiceRequestResponse_v3',
              'triggerResendContractResponse',
              'cms3_1ContractManagementSvcReqRes_v3.triggerResendContractResponse_element'}
            );
        }
        public AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractsByAgentIdResponse_elementFuture beginFindContractsByAgentId(System.Continuation continuation,String agentId) {
            cms3_1ContractManagementSvcReqRes_v3.findContractsByAgentId_element request_x = new cms3_1ContractManagementSvcReqRes_v3.findContractsByAgentId_element();
            request_x.agentId = agentId;
            return (AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractsByAgentIdResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractsByAgentIdResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              'findContractsByAgentId',
              'http://xmlschema.tmi.telus.com/srv/CMO/ContractMgmt/ContractManagementServiceRequestResponse_v3',
              'findContractsByAgentId',
              'http://xmlschema.tmi.telus.com/srv/CMO/ContractMgmt/ContractManagementServiceRequestResponse_v3',
              'findContractsByAgentIdResponse',
              'cms3_1ContractManagementSvcReqRes_v3.findContractsByAgentIdResponse_element'}
            );
        }
        public AsyncCms3_1ContractMgmtSvcReqRes_v3.getContractDocumentResponse_elementFuture beginGetContractDocument(System.Continuation continuation,String contractDocumentId) {
            cms3_1ContractManagementSvcReqRes_v3.getContractDocument_element request_x = new cms3_1ContractManagementSvcReqRes_v3.getContractDocument_element();
            request_x.contractDocumentId = contractDocumentId;
            return (AsyncCms3_1ContractMgmtSvcReqRes_v3.getContractDocumentResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncCms3_1ContractMgmtSvcReqRes_v3.getContractDocumentResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              'getContractDocument',
              'http://xmlschema.tmi.telus.com/srv/CMO/ContractMgmt/ContractManagementServiceRequestResponse_v3',
              'getContractDocument',
              'http://xmlschema.tmi.telus.com/srv/CMO/ContractMgmt/ContractManagementServiceRequestResponse_v3',
              'getContractDocumentResponse',
              'cms3_1ContractManagementSvcReqRes_v3.getContractDocumentResponse_element'}
            );
        }
        public AsyncCms3_1ContractMgmtSvcReqRes_v3.getContractDocumentMetaDataResponse_elementFuture beginGetContractDocumentMetaData(System.Continuation continuation,String contractId) {
            cms3_1ContractManagementSvcReqRes_v3.getContractDocumentMetaData_element request_x = new cms3_1ContractManagementSvcReqRes_v3.getContractDocumentMetaData_element();
            request_x.contractId = contractId;
            return (AsyncCms3_1ContractMgmtSvcReqRes_v3.getContractDocumentMetaDataResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncCms3_1ContractMgmtSvcReqRes_v3.getContractDocumentMetaDataResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              'getContractDocumentMetaData',
              'http://xmlschema.tmi.telus.com/srv/CMO/ContractMgmt/ContractManagementServiceRequestResponse_v3',
              'getContractDocumentMetaData',
              'http://xmlschema.tmi.telus.com/srv/CMO/ContractMgmt/ContractManagementServiceRequestResponse_v3',
              'getContractDocumentMetaDataResponse',
              'cms3_1ContractManagementSvcReqRes_v3.getContractDocumentMetaDataResponse_element'}
            );
        }
        public AsyncCms3_1ContractMgmtSvcReqRes_v3.createContractSubmissionResponse_elementFuture beginCreateContractSubmission(System.Continuation continuation,cms3_1ContractManagementSvcTypes_v3.ContractRequest contractRequest) {
            cms3_1ContractManagementSvcReqRes_v3.createContractSubmission_element request_x = new cms3_1ContractManagementSvcReqRes_v3.createContractSubmission_element();
            request_x.contractRequest = contractRequest;
            return (AsyncCms3_1ContractMgmtSvcReqRes_v3.createContractSubmissionResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncCms3_1ContractMgmtSvcReqRes_v3.createContractSubmissionResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              'createContractSubmission',
              'http://xmlschema.tmi.telus.com/srv/CMO/ContractMgmt/ContractManagementServiceRequestResponse_v3',
              'createContractSubmission',
              'http://xmlschema.tmi.telus.com/srv/CMO/ContractMgmt/ContractManagementServiceRequestResponse_v3',
              'createContractSubmissionResponse',
              'cms3_1ContractManagementSvcReqRes_v3.createContractSubmissionResponse_element'}
            );
        }
        public AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractAmendmentsByAssociateNumberResponse_elementFuture beginFindContractAmendmentsByAssociateNumber(System.Continuation continuation,String contractNum,Integer contractAssociateNum) {
            cms3_1ContractManagementSvcReqRes_v3.findContractAmendmentsByAssociateNumber_element request_x = new cms3_1ContractManagementSvcReqRes_v3.findContractAmendmentsByAssociateNumber_element();
            request_x.contractNum = contractNum;
            request_x.contractAssociateNum = contractAssociateNum;
            return (AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractAmendmentsByAssociateNumberResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractAmendmentsByAssociateNumberResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              'findContractAmendmentsByAssociateNumber',
              'http://xmlschema.tmi.telus.com/srv/CMO/ContractMgmt/ContractManagementServiceRequestResponse_v3',
              'findContractAmendmentsByAssociateNumber',
              'http://xmlschema.tmi.telus.com/srv/CMO/ContractMgmt/ContractManagementServiceRequestResponse_v3',
              'findContractAmendmentsByAssociateNumberResponse',
              'cms3_1ContractManagementSvcReqRes_v3.findContractAmendmentsByAssociateNumberResponse_element'}
            );
        }
        public AsyncCms3_1ContractMgmtSvcReqRes_v3.calculateTerminationChargesResponse_elementFuture beginCalculateTerminationCharges(System.Continuation continuation,String contractId,cms3_1ContractManagementSvcReqRes_v3.TerminationChargesParameter[] terminationChargesParameterList) {
            cms3_1ContractManagementSvcReqRes_v3.calculateTerminationCharges_element request_x = new cms3_1ContractManagementSvcReqRes_v3.calculateTerminationCharges_element();
            request_x.contractId = contractId;
            request_x.terminationChargesParameterList = terminationChargesParameterList;
            return (AsyncCms3_1ContractMgmtSvcReqRes_v3.calculateTerminationChargesResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncCms3_1ContractMgmtSvcReqRes_v3.calculateTerminationChargesResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              'calculateTerminationCharges',
              'http://xmlschema.tmi.telus.com/srv/CMO/ContractMgmt/ContractManagementServiceRequestResponse_v3',
              'calculateTerminationCharges',
              'http://xmlschema.tmi.telus.com/srv/CMO/ContractMgmt/ContractManagementServiceRequestResponse_v3',
              'calculateTerminationChargesResponse',
              'cms3_1ContractManagementSvcReqRes_v3.calculateTerminationChargesResponse_element'}
            );
        }
      /*  public AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractAmendmentsByAssociateNumResponse_elementFuture beginFindContractAmendmentsByAssociateNum(System.Continuation continuation,Long contractNum,Integer contractAssociateNum) {
            cms3_1ContractManagementSvcReqRes_v3.findContractAmendmentsByAssociateNum_element request_x = new cms3_1ContractManagementSvcReqRes_v3.findContractAmendmentsByAssociateNum_element();
            request_x.contractNum = contractNum;
            request_x.contractAssociateNum = contractAssociateNum;
            return (AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractAmendmentsByAssociateNumResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractAmendmentsByAssociateNumResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              'findContractAmendmentsByAssociateNum',
              'http://xmlschema.tmi.telus.com/srv/CMO/ContractMgmt/ContractManagementServiceRequestResponse_v3',
              'findContractAmendmentsByAssociateNum',
              'http://xmlschema.tmi.telus.com/srv/CMO/ContractMgmt/ContractManagementServiceRequestResponse_v3',
              'findContractAmendmentsByAssociateNumResponse',
              'cms3_1ContractManagementSvcReqRes_v3.findContractAmendmentsByAssociateNumResponse_element'}
            );
        }*/
        public AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractsByCustomerIdResponse_elementFuture beginFindContractsByCustomerId(System.Continuation continuation,String customerId) {
            cms3_1ContractManagementSvcReqRes_v3.findContractsByCustomerId_element request_x = new cms3_1ContractManagementSvcReqRes_v3.findContractsByCustomerId_element();
            request_x.customerId = customerId;
            return (AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractsByCustomerIdResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractsByCustomerIdResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              'findContractsByCustomerId',
              'http://xmlschema.tmi.telus.com/srv/CMO/ContractMgmt/ContractManagementServiceRequestResponse_v3',
              'findContractsByCustomerId',
              'http://xmlschema.tmi.telus.com/srv/CMO/ContractMgmt/ContractManagementServiceRequestResponse_v3',
              'findContractsByCustomerIdResponse',
              'cms3_1ContractManagementSvcReqRes_v3.findContractsByCustomerIdResponse_element'}
            );
        }
        public AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractDataResponse_elementFuture beginFindContractData(System.Continuation continuation,String wirelineTelephoneNumber,Boolean activeContractInd,Integer pageStartingRecordNumber,Integer numberOfRecordsPerPage) {
            cms3_1ContractManagementSvcReqRes_v3.findContractData_element request_x = new cms3_1ContractManagementSvcReqRes_v3.findContractData_element();
            request_x.wirelineTelephoneNumber = wirelineTelephoneNumber;
            request_x.activeContractInd = activeContractInd;
            request_x.pageStartingRecordNumber = pageStartingRecordNumber;
            request_x.numberOfRecordsPerPage = numberOfRecordsPerPage;
            return (AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractDataResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractDataResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              'findContractData',
              'http://xmlschema.tmi.telus.com/srv/CMO/ContractMgmt/ContractManagementServiceRequestResponse_v3',
              'findContractData',
              'http://xmlschema.tmi.telus.com/srv/CMO/ContractMgmt/ContractManagementServiceRequestResponse_v3',
              'findContractDataResponse',
              'cms3_1ContractManagementSvcReqRes_v3.findContractDataResponse_element'}
            );
        }
        public AsyncCms3_1ContractMgmtSvcReqRes_v3.updateContractSubmissionResponse_elementFuture beginUpdateContractSubmission(System.Continuation continuation,String assignedProductId,String addressId,String employeeId) {
            cms3_1ContractManagementSvcReqRes_v3.updateContractSubmission_element request_x = new cms3_1ContractManagementSvcReqRes_v3.updateContractSubmission_element();
            request_x.assignedProductId = assignedProductId;
            request_x.addressId = addressId;
            request_x.employeeId = employeeId;
            return (AsyncCms3_1ContractMgmtSvcReqRes_v3.updateContractSubmissionResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncCms3_1ContractMgmtSvcReqRes_v3.updateContractSubmissionResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              'updateContractSubmission',
              'http://xmlschema.tmi.telus.com/srv/CMO/ContractMgmt/ContractManagementServiceRequestResponse_v3',
              'updateContractSubmission',
              'http://xmlschema.tmi.telus.com/srv/CMO/ContractMgmt/ContractManagementServiceRequestResponse_v3',
              'updateContractSubmissionResponse',
              'cms3_1ContractManagementSvcReqRes_v3.updateContractSubmissionResponse_element'}
            );
        }
    }
}