/**
 * Various utility methods.
 * 
 * @author Travis COte
 * @since 22/02/2007
 */
public class BulkLoadUtils {
	private static Opportunity createOpportunity(){
        Account a = new Account(Name = 'Unit Test Account');
        insert a;
        
        Opportunity opp = new Opportunity(Name = 'Test', StageName = 'Test', AccountId = a.id, CloseDate = Date.today());
        
        insert opp;        
                
        return opp;
	}
    
//	public static Bulk_Order_Load_File__c createBulkLoad(){        
	public static Bulk_Order_Load_File__c createBulkLoad(String testCSV){        
        Opportunity opp = createOpportunity();

        Bulk_Order_Load_File__c bl = new Bulk_Order_Load_File__c();

        bl.Opportunity__c = opp.id;
        
        insert bl;
            
        String data = BulkLoadUtils.getStaticResource(testCSV);
        Id attachmentId = BulkLoadUtils.createAttachment(bl.Id, data);        
        Attachment a = BulkLoadUtils.getAttachment(attachmentId);        
        
        Bulk_Order_Load_File__c returnBl = [SELECT id, opportunity__r.id, opportunity__r.Account.id FROM Bulk_Order_Load_File__c WHERE id=:bl.id LIMIT 1];         
        
        return returnBl;
	}
    
    public static String getStaticResource(String name){
        StaticResource sr = [select Body from StaticResource where Name = :name];

System.debug('BulkLoadUtils, getStaticResource: ' + sr.Body.toString());        
        
        return sr.Body.toString();
    }
    
    public static Id createAttachment(Id parentId, String data){
        Attachment attach = new Attachment();   
		attach.Name = 'Test Bulkload CSV';
		attach.parentId = parentId;
		Blob bodyBlob = Blob.valueOf(data);
		attach.body = bodyBlob;
		insert attach;
                
        return attach.id;
	}
    
    public static Attachment getAttachment(Id attachmentId){
        Attachment a = [SELECT id, name, body FROM Attachment WHERE id=:attachmentId LIMIT 1];   
                
        return a;
	}
 
    public static service_request__c createServiceRequest(Id oppId, Id accId){
		map<string,schema.recordtypeinfo> serviceRequestRecordTypeMap = schema.sobjecttype.service_request__c.getrecordtypeinfosbyname();
        string serviceRequestFieldPath='';
        

        service_request__c newServiceRequest = new service_request__c(opportunity__c=oppId, OwnerId = UserInfo.getUserId(), service_request_status__c = 'Ready to Submit', Fox_Submission_In_Progress__c = false, Reason_to_Send_Code__c='Because', Target_Date__c= Date.newInstance(2017, 12, 12));
        newServiceRequest.recordtypeid=serviceRequestRecordTypeMap.get('Install').getrecordtypeid();
//        System.debug('Bulkload: CommitBulkOrderLoad, createServiceRequest: ' + bulkOrderLoadRecord);
        
        newServiceRequest.account_name__c=accId;
        return newServiceRequest;
    }


    public static list<SRS_SubmitToFox.FoxResponse> createFoxResponseList(service_request__c curServiceRequest){
        list<SRS_SubmitToFox.FoxResponse> responseList=new list<SRS_SubmitToFox.FoxResponse>();
        
        if(curServiceRequest == null){
            return responseList;
        }
        
        list<Service_Request__c> srUpdateList=new list<Service_Request__c>();

        string TID='';
        string PODSProduct='';
        string EndCustomerName='';
        string SiteTelephone='';
        Boolean isCreate=false;
        Integer FOXOrderID=0;        
System.debug('BulkLoad: submitBulk');                

System.debug('BulkLoad: submitBulk, curServiceRequest: ' + curServiceRequest.name);                
        SRS_SubmitToFox.FoxResponse foxResponse=new SRS_SubmitToFox.FoxResponse(curServiceRequest);
        responseList.add(foxResponse);
        
        return responseList;
     }
    
    public static void createSMBCareWebServicesCustomSetting(){
		List<SMBCare_WebServices__c> lst = new List<SMBCare_WebServices__c>();
        
    	SMBCare_WebServices__c cis = new SMBCare_WebServices__c();
        cis.name = 'username';
        cis.value__c = 'APP_SFDC';
        insert cis;
    
    	cis = new SMBCare_WebServices__c();
        cis.name = 'username_pc';
        cis.value__c = 'APP_PRIVCLOUD';
        insert cis;
        
    	cis = new SMBCare_WebServices__c();
        cis.name = 'password';
        cis.value__c = 'soaorgid';
        insert cis;
        
    	cis = new SMBCare_WebServices__c();
        cis.name = 'TTODS_EndPoint';
        cis.value__c = 'https://xmlgwy-pt1.telus.com:9030/pt01/SMO/ProblemMgmt/TroubleTicketODSQueryService_v1_5_vs0';
        insert cis;
        
    	cis = new SMBCare_WebServices__c();
        cis.name = 'TTODS_Endpoint_Search_Lynx';
        cis.value__c = 'https://xmlgwy-pt1.telus.com:9030/pt01/SMO/ProblemMgmt/TroubleTicketODSQueryService_v1_5_vs0';
        insert cis;

    	cis = new SMBCare_WebServices__c();
        cis.name = 'ENDPOINT_Remedy_View';
        cis.value__c = 'https://xmlgwy-pt1.telus.com:9030/dv/SMO/ProblemMgmt/ExternalTicketViewService_v1_0_vs0';
        insert cis;

    	cis = new SMBCare_WebServices__c();
        cis.name = 'SRS_FOXEndPoint';
        cis.value__c = 'https://xmlgwy-pt1.telus.com:9030/dv01/RMO/OrderMgmt/FOXWebservice_v3_0_vs0';
        insert cis;
    }
    
    
    
    /**
     * Reference design copied from internet.  This is without handling newline within a single
     * field.
     */
    public static List<List<String>> parseCSV(String contents,Boolean skipHeaders) {
        Boolean log = false;
        
        List<List<String>> allFields = new List<List<String>>();
        
        // replace instances where a double quote begins a field containing a comma
        // in this case you get a double quote followed by a doubled double quote
        // do this for beginning and end of a field
        contents = contents.replaceAll(',"""',',"DBLQT').replaceall('""",','DBLQT",');
        // now replace all remaining double quotes - we do this so that we can reconstruct
        // fields with commas inside assuming they begin and end with a double quote
        contents = contents.replaceAll('""','DBLQT');
        // we are not attempting to handle fields with a newline inside of them
        // so, split on newline to get the spreadsheet rows
        List<String> lines = new List<String>();
        lines = contents.split('\r\n');
        Integer num = 0;
if(log)
System.debug('Util.parseCSV, lines.size(): ' + lines.size());            
        for(String line:lines) {
if(log)
System.debug('Util.parseCSV, line: ' + line);            
            // check for blank CSV lines (only commas)
            if (line.replaceAll(',','').trim().length() == 0) break;
            
			line = preserveCommasInQuotes(line);
if(log)
System.debug('Util.parseCSV, line after: ' + line);            
    
    		List<String> fields = null;                                
            if(!line.contains(',')){
if(log)
System.debug('Util.parseCSV, tab delimited!');                            
	            fields = line.split('\t');                                
            }
            else{
if(log)
System.debug('Util.parseCSV, comma delimited!');                            
	            fields = line.split(',');                
            }
            List<String> cleanFields = new List<String>();
            String compositeField;
            Boolean makeCompositeField = false;
            for(String field:fields) {
if(log)
System.debug('Util.parseCSV, field: ' + field);     
                field = restoreCommas(field);
if(log)
System.debug('Util.parseCSV, field restored: ' + field);     
                if (field.startsWith('"') && field.endsWith('"')) {
                    cleanFields.add(field.replaceAll('DBLQT','"'));
                } else if (field.startsWith('"')) {
                    makeCompositeField = true;
                    compositeField = field;
                } else if (field.endsWith('"')) {
                    compositeField += ',' + field;
                    cleanFields.add(compositeField.replaceAll('DBLQT','"'));
                    makeCompositeField = false;
                } else if (makeCompositeField) {
                    compositeField +=  ',' + field;
                } else {
                    cleanFields.add(field.replaceAll('DBLQT','"'));
                }
            }
            
if(log)
System.debug('Util.parseCSV, cleanFields: ' + cleanFields);            
            allFields.add(cleanFields);
        }
        if (skipHeaders) allFields.remove(0);
        return allFields;		
    }
    
    public static String preserveCommasInQuotes(String str){
Boolean log = false;        
        String result = str;
        Integer pos = 0;
        List<Integer> indexes = new List<Integer>(); 
if(log)
System.debug('str: ' + str);    
		Integer index = str.indexOfIgnoreCase('"', pos);        
if(log)
System.debug('index: ' + index);    
        while(index != -1){
//        if(index != -1){
            indexes.add(index);
            pos = index+1;
			index = str.indexOfIgnoreCase('"', pos);                    
if(log)
System.debug('index at ' + pos + ': ' + index);    
        }
if(log)
System.debug('indexes: ' + indexes);    
        List<String> find = new List<String>(); 
        List<String> replace = new List<String>(); 
        Integer left = -1;
        Integer right = -1;
        for(Integer i : indexes){
            if(left == -1){
                left = i;                    
            }
            else if(right == -1){
                right = i;                    
if(log)
System.debug('left: ' + left + ', right: ' + right);    
                String f = str.substring(left, right);        
                find.add(f);
                replace.add(f.replaceAll(',', '_comma_'));
                // clear left and right cursors for next time around
                left = -1;
                right = -1;
            }
        }
    
        for(Integer i=0;i<find.size();i++){
if(log)
System.debug('preserveCommasInQuotes, find: ' + find[i] + ', replace: ' + replace[i]);
            result = result.replace(find[i], replace[i]);            
        }
        
if(log)
System.debug('preserveCommasInQuotes, result: ' + result);
        
    	return result;
    }

    public static String restoreCommas(String str){
        return str.replaceAll('_comma_', ',');
    }    
}