global class SetContactSyncUsersService implements Messaging.InboundEmailHandler {

    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        String userIdsAsString = email.plainTextBody;
        updateUsers(userIdsAsString);
        return result;
    }

	public void updateUsers(String userIdsAsString) {	//Messaging.InboundEmail
		System.debug('---Before userIdsAsString = ' + userIdsAsString);
		if (userIdsAsString != null && userIdsAsString.indexOf('^') != -1 && userIdsAsString.indexOf('$') != -1) {
			userIdsAsString = userIdsAsString.substring(userIdsAsString.indexOf('$') + 1, userIdsAsString.indexOf('^'));
		}
		System.debug('---After userIdsAsString = ' + userIdsAsString);
        List<User> users = new List<User>();
        for (String userId : userIdsAsString.split(',')) {
        	System.debug('UserId = ' + userId);
        	//System.debug('UserId length = ' + userId.length());
            users.add(new User(Id = (Id)userId, uas__Sync_to_Contact__c = true)); 
        }
        if (! users.isEmpty()) update users;
		
	}
}