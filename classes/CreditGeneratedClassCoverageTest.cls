@isTest
public class CreditGeneratedClassCoverageTest {    

    
    @isTest
    public static void CreditProfileServicePing() {  
        CreditProfileServicePing.ping_element element = new   CreditProfileServicePing.ping_element();
        CreditProfileServicePing.pingResponse_element response = new   CreditProfileServicePing.pingResponse_element();
    }
    
    @isTest
    public static void CreditAssessmentBusinessTypes() {  
        CreditAssessmentBusinessTypes.BusinessCreditProfile element_1 = new CreditAssessmentBusinessTypes.BusinessCreditProfile();  
        CreditAssessmentBusinessTypes.BusinessCreditProfileBase element_2 = new CreditAssessmentBusinessTypes.BusinessCreditProfileBase();  
        CreditAssessmentBusinessTypes.BusinessCreditReportBaseMetaData element_3 = new CreditAssessmentBusinessTypes.BusinessCreditReportBaseMetaData();  
        CreditAssessmentBusinessTypes.BusinessCreditReportFullDetails element_4 = new CreditAssessmentBusinessTypes.BusinessCreditReportFullDetails();  
        CreditAssessmentBusinessTypes.BusinessCreditReportMetaData element_5 = new CreditAssessmentBusinessTypes.BusinessCreditReportMetaData();  
        CreditAssessmentBusinessTypes.BusinessCreditReportRequest element_6 = new CreditAssessmentBusinessTypes.BusinessCreditReportRequest();  
        CreditAssessmentBusinessTypes.BusinessCustomerProfile element_7 = new CreditAssessmentBusinessTypes.BusinessCustomerProfile();  
        CreditAssessmentBusinessTypes.BusinessProfile element_8 = new CreditAssessmentBusinessTypes.BusinessProfile();  
        CreditAssessmentBusinessTypes.BusinessProfile_associationsProfile_element element_9 = new CreditAssessmentBusinessTypes.BusinessProfile_associationsProfile_element();  
        CreditAssessmentBusinessTypes.BusinessProfile_associationsProfile_partnerBusinessProfileList_element element_10 = new CreditAssessmentBusinessTypes.BusinessProfile_associationsProfile_partnerBusinessProfileList_element();  
        CreditAssessmentBusinessTypes.BusinessProfile_associationsProfile_partnerIndividualProfileList_element element_11 = new CreditAssessmentBusinessTypes.BusinessProfile_associationsProfile_partnerIndividualProfileList_element();  
        CreditAssessmentBusinessTypes.BusinessProfile_associationsProfile_proprietorProfile_element element_12 = new CreditAssessmentBusinessTypes.BusinessProfile_associationsProfile_proprietorProfile_element();  
        CreditAssessmentBusinessTypes.CreditAuditInfo element_13 = new CreditAssessmentBusinessTypes.CreditAuditInfo();  
        CreditAssessmentBusinessTypes.CreditCheckAddress element_14 = new CreditAssessmentBusinessTypes.CreditCheckAddress();  
        CreditAssessmentBusinessTypes.CreditComment element_15 = new CreditAssessmentBusinessTypes.CreditComment();  
        CreditAssessmentBusinessTypes.DriverLicense element_16 = new CreditAssessmentBusinessTypes.DriverLicense();  
        CreditAssessmentBusinessTypes.HealthCard element_17 = new CreditAssessmentBusinessTypes.HealthCard();  
        CreditAssessmentBusinessTypes.IndividualCreditIdentification element_18 = new CreditAssessmentBusinessTypes.IndividualCreditIdentification();  
        CreditAssessmentBusinessTypes.IndividualCreditProfile element_19 = new CreditAssessmentBusinessTypes.IndividualCreditProfile();  
        CreditAssessmentBusinessTypes.IndividualCreditProfileBase element_20 = new CreditAssessmentBusinessTypes.IndividualCreditProfileBase();  
        CreditAssessmentBusinessTypes.IndividualCustomerProfile element_21 = new CreditAssessmentBusinessTypes.IndividualCustomerProfile();  
        CreditAssessmentBusinessTypes.IndividualPersonalInfo element_22 = new CreditAssessmentBusinessTypes.IndividualPersonalInfo();  
        CreditAssessmentBusinessTypes.IndividualProfile element_23 = new CreditAssessmentBusinessTypes.IndividualProfile();  
        CreditAssessmentBusinessTypes.Passport element_24 = new CreditAssessmentBusinessTypes.Passport();  
        CreditAssessmentBusinessTypes.ProvincialIdCard element_25 = new CreditAssessmentBusinessTypes.ProvincialIdCard();  
        CreditAssessmentBusinessTypes.RegistrationIncorporation element_26 = new CreditAssessmentBusinessTypes.RegistrationIncorporation();  
        
    }
    
    
    @isTest
    public static void CreditAssessmentRequestResponse() {  
        
        CreditAssessmentRequestResponse.attachCreditReport_element element_1 = new CreditAssessmentRequestResponse.attachCreditReport_element();
        CreditAssessmentRequestResponse.attachCreditReportResponse_element element_2 = new CreditAssessmentRequestResponse.attachCreditReportResponse_element();
        CreditAssessmentRequestResponse.AutoUpdateBusinessCreditProfileResult element_3 = new CreditAssessmentRequestResponse.AutoUpdateBusinessCreditProfileResult();
        CreditAssessmentRequestResponse.BusinessCreditAssessmentResult element_4 = new CreditAssessmentRequestResponse.BusinessCreditAssessmentResult();
        CreditAssessmentRequestResponse.BusinessCreditAssessmentResult_ruleEvaluationDetails_element element_5 = new CreditAssessmentRequestResponse.BusinessCreditAssessmentResult_ruleEvaluationDetails_element();
        CreditAssessmentRequestResponse.getBusinessCreditAssessmentList_element element_6 = new CreditAssessmentRequestResponse.getBusinessCreditAssessmentList_element();
        CreditAssessmentRequestResponse.getBusinessCreditAssessmentListResponse_element element_7 = new CreditAssessmentRequestResponse.getBusinessCreditAssessmentListResponse_element();
        CreditAssessmentRequestResponse.getCreditReport_element element_8 = new CreditAssessmentRequestResponse.getCreditReport_element();
        CreditAssessmentRequestResponse.getCreditReportList_element element_9 = new CreditAssessmentRequestResponse.getCreditReportList_element();
        CreditAssessmentRequestResponse.getCreditReportListResponse_element element_10 = new CreditAssessmentRequestResponse.getCreditReportListResponse_element();
        CreditAssessmentRequestResponse.getCreditReportResponse_element element_11 = new CreditAssessmentRequestResponse.getCreditReportResponse_element();
        CreditAssessmentRequestResponse.MatchedIndividualCustomer element_12 = new CreditAssessmentRequestResponse.MatchedIndividualCustomer();
        CreditAssessmentRequestResponse.MatchedIndividualCustomer_changedConsumerCreditIdentification_element element_13 = new CreditAssessmentRequestResponse.MatchedIndividualCustomer_changedConsumerCreditIdentification_element();
        CreditAssessmentRequestResponse.performAutoUpdateBusinessCreditProfile_carBusinessProfile_element element_14 = new CreditAssessmentRequestResponse.performAutoUpdateBusinessCreditProfile_carBusinessProfile_element();
        CreditAssessmentRequestResponse.performAutoUpdateBusinessCreditProfile_carBusinessProfile_updatedIndvidualProprietorProfile_element element_15 = new CreditAssessmentRequestResponse.performAutoUpdateBusinessCreditProfile_carBusinessProfile_updatedIndvidualProprietorProfile_element();
        CreditAssessmentRequestResponse.performAutoUpdateBusinessCreditProfile_element element_16 = new CreditAssessmentRequestResponse.performAutoUpdateBusinessCreditProfile_element();
        CreditAssessmentRequestResponse.performAutoUpdateBusinessCreditProfileResponse_element element_17 = new CreditAssessmentRequestResponse.performAutoUpdateBusinessCreditProfileResponse_element();
        CreditAssessmentRequestResponse.performBusinessCreditAssessment_element element_18 = new CreditAssessmentRequestResponse.performBusinessCreditAssessment_element();
        CreditAssessmentRequestResponse.performBusinessCreditAssessment_orderData_element element_19 = new CreditAssessmentRequestResponse.performBusinessCreditAssessment_orderData_element();
        CreditAssessmentRequestResponse.performBusinessCreditAssessmentResponse_element element_20 = new CreditAssessmentRequestResponse.performBusinessCreditAssessmentResponse_element();
        CreditAssessmentRequestResponse.removeCreditReport_element element_21 = new CreditAssessmentRequestResponse.removeCreditReport_element();
        CreditAssessmentRequestResponse.removeCreditReportResponse_element element_22 = new CreditAssessmentRequestResponse.removeCreditReportResponse_element();
        CreditAssessmentRequestResponse.RuleCondition element_23 = new CreditAssessmentRequestResponse.RuleCondition();
        CreditAssessmentRequestResponse.Ruleset element_24 = new CreditAssessmentRequestResponse.Ruleset();
        
    }
    
    @isTest
    public static void CreditProfileServiceTypes() {  
        CreditProfileServiceTypes.BusinessCreditProfile element_1 = new CreditProfileServiceTypes.BusinessCreditProfile();  
        CreditProfileServiceTypes.BusinessCreditProfileBase element_2 = new CreditProfileServiceTypes.BusinessCreditProfileBase();  
        CreditProfileServiceTypes.BusinessCreditReportBaseMetaData element_3 = new CreditProfileServiceTypes.BusinessCreditReportBaseMetaData();  
        CreditProfileServiceTypes.BusinessCreditReportFullDetails element_4 = new CreditProfileServiceTypes.BusinessCreditReportFullDetails();  
        CreditProfileServiceTypes.BusinessCreditReportMetaData element_5 = new CreditProfileServiceTypes.BusinessCreditReportMetaData();  
        CreditProfileServiceTypes.BusinessCreditReportRequest element_6 = new CreditProfileServiceTypes.BusinessCreditReportRequest();  
        CreditProfileServiceTypes.BusinessCustomerProfile element_7 = new CreditProfileServiceTypes.BusinessCustomerProfile();  
        CreditProfileServiceTypes.BusinessProfile element_8 = new CreditProfileServiceTypes.BusinessProfile();  
        CreditProfileServiceTypes.BusinessProfile_associationsProfile_element element_9 = new CreditProfileServiceTypes.BusinessProfile_associationsProfile_element();  
        CreditProfileServiceTypes.BusinessProfile_associationsProfile_partnerBusinessProfileList_element element_10 = new CreditProfileServiceTypes.BusinessProfile_associationsProfile_partnerBusinessProfileList_element();  
        CreditProfileServiceTypes.BusinessProfile_associationsProfile_partnerBusinessProfileList_partnerBusinessCreditProfile_element element_11 = new CreditProfileServiceTypes.BusinessProfile_associationsProfile_partnerBusinessProfileList_partnerBusinessCreditProfile_element();  
        CreditProfileServiceTypes.BusinessProfile_associationsProfile_partnerIndividualProfileList_element element_12 = new CreditProfileServiceTypes.BusinessProfile_associationsProfile_partnerIndividualProfileList_element();  
        CreditProfileServiceTypes.BusinessProfile_associationsProfile_proprietorProfile_element element_13 = new CreditProfileServiceTypes.BusinessProfile_associationsProfile_proprietorProfile_element();  
        CreditProfileServiceTypes.CreditAuditInfo element_14 = new CreditProfileServiceTypes.CreditAuditInfo();  
        CreditProfileServiceTypes.CreditCheckAddress element_15 = new CreditProfileServiceTypes.CreditCheckAddress();  
        CreditProfileServiceTypes.CreditComment element_16 = new CreditProfileServiceTypes.CreditComment();  
        CreditProfileServiceTypes.DriverLicense element_17 = new CreditProfileServiceTypes.DriverLicense();  
        CreditProfileServiceTypes.HealthCard element_18 = new CreditProfileServiceTypes.HealthCard();  
        CreditProfileServiceTypes.IndividualCreditIdentification element_19 = new CreditProfileServiceTypes.IndividualCreditIdentification();  
        CreditProfileServiceTypes.IndividualCreditProfile element_20 = new CreditProfileServiceTypes.IndividualCreditProfile();  
        CreditProfileServiceTypes.IndividualCreditProfileBase element_21 = new CreditProfileServiceTypes.IndividualCreditProfileBase();  
        CreditProfileServiceTypes.IndividualCustomerProfile element_22 = new CreditProfileServiceTypes.IndividualCustomerProfile();  
        CreditProfileServiceTypes.IndividualPersonalInfo element_23 = new CreditProfileServiceTypes.IndividualPersonalInfo();  
        CreditProfileServiceTypes.IndividualProfile element_24 = new CreditProfileServiceTypes.IndividualProfile();  
        CreditProfileServiceTypes.Passport element_25 = new CreditProfileServiceTypes.Passport();  
        CreditProfileServiceTypes.ProvincialIdCard element_26 = new CreditProfileServiceTypes.ProvincialIdCard();  
        CreditProfileServiceTypes.RegistrationIncorporation element_27 = new CreditProfileServiceTypes.RegistrationIncorporation();  
        
    }
    
    
    
    @isTest
    public static void CreditAssessmentEnterpriseTypes() {  
        CreditAssessmentEnterpriseTypes.AuditInfo 	auditinfo = new CreditAssessmentEnterpriseTypes.AuditInfo();
        CreditAssessmentEnterpriseTypes.BankAccount 	bankaccount = new CreditAssessmentEnterpriseTypes.BankAccount();
        CreditAssessmentEnterpriseTypes.brandType 	brandtype = new CreditAssessmentEnterpriseTypes.brandType();
        CreditAssessmentEnterpriseTypes.CodeDescText 	codedesctext = new CreditAssessmentEnterpriseTypes.CodeDescText();
        CreditAssessmentEnterpriseTypes.CreditCard 	creditcard = new CreditAssessmentEnterpriseTypes.CreditCard();
        CreditAssessmentEnterpriseTypes.Description 	description = new CreditAssessmentEnterpriseTypes.Description();
        CreditAssessmentEnterpriseTypes.DriversLicense 	driverslicense = new CreditAssessmentEnterpriseTypes.DriversLicense();
        CreditAssessmentEnterpriseTypes.Duration 	duration = new CreditAssessmentEnterpriseTypes.Duration();
        CreditAssessmentEnterpriseTypes.HealthCard 	healthcard = new CreditAssessmentEnterpriseTypes.HealthCard();
        CreditAssessmentEnterpriseTypes.IndividualName 	individualname = new CreditAssessmentEnterpriseTypes.IndividualName();
        CreditAssessmentEnterpriseTypes.MarketSegment 	marketsegment = new CreditAssessmentEnterpriseTypes.MarketSegment();
        CreditAssessmentEnterpriseTypes.Message 	message = new CreditAssessmentEnterpriseTypes.Message();
        CreditAssessmentEnterpriseTypes.MessageType 	messagetype = new CreditAssessmentEnterpriseTypes.MessageType();
        CreditAssessmentEnterpriseTypes.MonetaryAmt 	monetaryamt = new CreditAssessmentEnterpriseTypes.MonetaryAmt();
        CreditAssessmentEnterpriseTypes.MultilingualCodeDescriptionList 	multilingualcodedescriptionlist = new CreditAssessmentEnterpriseTypes.MultilingualCodeDescriptionList();
        CreditAssessmentEnterpriseTypes.MultilingualCodeDescTextList 	multilingualcodedesctextlist = new CreditAssessmentEnterpriseTypes.MultilingualCodeDescTextList();
        CreditAssessmentEnterpriseTypes.MultilingualDescriptionList 	multilingualdescriptionlist = new CreditAssessmentEnterpriseTypes.MultilingualDescriptionList();
        CreditAssessmentEnterpriseTypes.MultilingualNameList 	multilingualnamelist = new CreditAssessmentEnterpriseTypes.MultilingualNameList();
        CreditAssessmentEnterpriseTypes.Name 	name = new CreditAssessmentEnterpriseTypes.Name();
        CreditAssessmentEnterpriseTypes.OrganizationName 	organizationname = new CreditAssessmentEnterpriseTypes.OrganizationName();
        CreditAssessmentEnterpriseTypes.PartyName 	partyname = new CreditAssessmentEnterpriseTypes.PartyName();
        CreditAssessmentEnterpriseTypes.Passport 	passport = new CreditAssessmentEnterpriseTypes.Passport();
        CreditAssessmentEnterpriseTypes.ProvincialIdCard 	provincialidcard = new CreditAssessmentEnterpriseTypes.ProvincialIdCard();
        CreditAssessmentEnterpriseTypes.Quantity 	quantity = new CreditAssessmentEnterpriseTypes.Quantity();
        CreditAssessmentEnterpriseTypes.Rate 	rate = new CreditAssessmentEnterpriseTypes.Rate();
        CreditAssessmentEnterpriseTypes.ResponseMessage 	responsemessage = new CreditAssessmentEnterpriseTypes.ResponseMessage();
        CreditAssessmentEnterpriseTypes.UnknownName	unknownname	 =  new CreditAssessmentEnterpriseTypes.UnknownName();
    }
    
    
    
    @isTest
    public static void CreditProfileServiceRequestResponse() {  
        CreditProfileServiceRequestResponse.addBusinessPartner_element	element_1 =  new CreditProfileServiceRequestResponse.addBusinessPartner_element ();	
        CreditProfileServiceRequestResponse.addBusinessPartnerResponse_element	element_2 =  new CreditProfileServiceRequestResponse.addBusinessPartnerResponse_element ();	
        CreditProfileServiceRequestResponse.addIndividualPartner_element	element_3 =  new CreditProfileServiceRequestResponse.addIndividualPartner_element ();	
        CreditProfileServiceRequestResponse.addIndividualPartnerResponse_element	element_4 =  new CreditProfileServiceRequestResponse.addIndividualPartnerResponse_element ();	
        CreditProfileServiceRequestResponse.attachCreditReport_element	element_5 =  new CreditProfileServiceRequestResponse.attachCreditReport_element ();	
        CreditProfileServiceRequestResponse.attachCreditReportResponse_element	element_6 =  new CreditProfileServiceRequestResponse.attachCreditReportResponse_element ();	
        CreditProfileServiceRequestResponse.createBusinessCreditProfile_element	element_7 =  new CreditProfileServiceRequestResponse.createBusinessCreditProfile_element ();	
        CreditProfileServiceRequestResponse.createBusinessCreditProfileResponse_element	element_8 =  new CreditProfileServiceRequestResponse.createBusinessCreditProfileResponse_element ();	
        CreditProfileServiceRequestResponse.createIndividualCustomerWithCreditWorthiness_element	element_9 =  new CreditProfileServiceRequestResponse.createIndividualCustomerWithCreditWorthiness_element ();	
        CreditProfileServiceRequestResponse.createIndividualCustomerWithCreditWorthinessResponse_element	element_10 =  new CreditProfileServiceRequestResponse.createIndividualCustomerWithCreditWorthinessResponse_element ();	
        CreditProfileServiceRequestResponse.getBusinessCreditProfileSummary_element	element_11 =  new CreditProfileServiceRequestResponse.getBusinessCreditProfileSummary_element ();	
        CreditProfileServiceRequestResponse.getBusinessCreditProfileSummaryResponse_businessCreditProfileSummaryResult_associationsProfile_element	element_12 =  new CreditProfileServiceRequestResponse.getBusinessCreditProfileSummaryResponse_businessCreditProfileSummaryResult_associationsProfile_element ();	
        CreditProfileServiceRequestResponse.getBusinessCreditProfileSummaryResponse_businessCreditProfileSummaryResult_associationsProfile_partnerBusinessProfileList_element	element_13 =  new CreditProfileServiceRequestResponse.getBusinessCreditProfileSummaryResponse_businessCreditProfileSummaryResult_associationsProfile_partnerBusinessProfileList_element ();	
        CreditProfileServiceRequestResponse.getBusinessCreditProfileSummaryResponse_businessCreditProfileSummaryResult_associationsProfile_partnerIndividualProfileList_element	element_14 =  new CreditProfileServiceRequestResponse.getBusinessCreditProfileSummaryResponse_businessCreditProfileSummaryResult_associationsProfile_partnerIndividualProfileList_element ();	
        CreditProfileServiceRequestResponse.getBusinessCreditProfileSummaryResponse_businessCreditProfileSummaryResult_associationsProfile_proprietorProfile_element	element_15 =  new CreditProfileServiceRequestResponse.getBusinessCreditProfileSummaryResponse_businessCreditProfileSummaryResult_associationsProfile_proprietorProfile_element ();	
        CreditProfileServiceRequestResponse.getBusinessCreditProfileSummaryResponse_businessCreditProfileSummaryResult_element	element_16 =  new CreditProfileServiceRequestResponse.getBusinessCreditProfileSummaryResponse_businessCreditProfileSummaryResult_element ();	
        CreditProfileServiceRequestResponse.getBusinessCreditProfileSummaryResponse_element	element_17 =  new CreditProfileServiceRequestResponse.getBusinessCreditProfileSummaryResponse_element ();	
        CreditProfileServiceRequestResponse.getBusinessProfileResponse_element	element_18 =  new CreditProfileServiceRequestResponse.getBusinessProfileResponse_element ();	
        CreditProfileServiceRequestResponse.getCreditReport_element	element_19 =  new CreditProfileServiceRequestResponse.getCreditReport_element ();	
        CreditProfileServiceRequestResponse.getCreditReportList_element	element_20 =  new CreditProfileServiceRequestResponse.getCreditReportList_element ();	
        CreditProfileServiceRequestResponse.getCreditReportListResponse_element	element_21 =  new CreditProfileServiceRequestResponse.getCreditReportListResponse_element ();	
        CreditProfileServiceRequestResponse.getCreditReportResponse_element	element_22 =  new CreditProfileServiceRequestResponse.getCreditReportResponse_element ();	
        CreditProfileServiceRequestResponse.removePartner_element	element_23 =  new CreditProfileServiceRequestResponse.removePartner_element ();	
        CreditProfileServiceRequestResponse.removePartnerResponse_element	element_24 =  new CreditProfileServiceRequestResponse.removePartnerResponse_element ();	
        CreditProfileServiceRequestResponse.replaceBusinessProprietor_element	element_25 =  new CreditProfileServiceRequestResponse.replaceBusinessProprietor_element ();	
        CreditProfileServiceRequestResponse.replaceBusinessProprietorResponse_element	element_26 =  new CreditProfileServiceRequestResponse.replaceBusinessProprietorResponse_element ();	
        CreditProfileServiceRequestResponse.searchBusinessCreditProfile_element	element_27 =  new CreditProfileServiceRequestResponse.searchBusinessCreditProfile_element ();	
        CreditProfileServiceRequestResponse.searchBusinessCreditProfileResponse_businessProfileSearchResultList_element	element_28 =  new CreditProfileServiceRequestResponse.searchBusinessCreditProfileResponse_businessProfileSearchResultList_element ();	
        CreditProfileServiceRequestResponse.searchBusinessCreditProfileResponse_element	element_29 =  new CreditProfileServiceRequestResponse.searchBusinessCreditProfileResponse_element ();	
        CreditProfileServiceRequestResponse.searchIndivdualCreditProfile_element	element_30 =  new CreditProfileServiceRequestResponse.searchIndivdualCreditProfile_element ();	
        CreditProfileServiceRequestResponse.searchIndivdualCreditProfileResponse_element	element_31 =  new CreditProfileServiceRequestResponse.searchIndivdualCreditProfileResponse_element ();	
        CreditProfileServiceRequestResponse.updateBusinessCreditProfile_element	element_32 =  new CreditProfileServiceRequestResponse.updateBusinessCreditProfile_element ();	
        CreditProfileServiceRequestResponse.updateBusinessCreditProfileResponse_element	element_33 =  new CreditProfileServiceRequestResponse.updateBusinessCreditProfileResponse_element ();	
        CreditProfileServiceRequestResponse.updateIndivdualCreditProfile_element	element_34 =  new CreditProfileServiceRequestResponse.updateIndivdualCreditProfile_element ();	
        CreditProfileServiceRequestResponse.updateIndivdualCreditProfileResponse_element	element_35 =  new CreditProfileServiceRequestResponse.updateIndivdualCreditProfileResponse_element ();	
        CreditProfileServiceRequestResponse.voidCreditReport_element	element_36 =  new CreditProfileServiceRequestResponse.voidCreditReport_element ();	
        CreditProfileServiceRequestResponse.voidCreditReportResponse_element	element_37 =  new CreditProfileServiceRequestResponse.voidCreditReportResponse_element ();	        
    }
    /**/
    
    
}