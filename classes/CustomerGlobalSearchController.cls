public without sharing class CustomerGlobalSearchController {
    
    public static List<Account> doAccountSearchExtended(String searchClause) {

        try { 
             
            List<SObject> result = new List<SObject>();
            Integer count = 0;
            integer availableRoom = 0;
            List<List<SObject>>searchList= [FIND :searchClause
                                            IN ALL FIELDS RETURNING
                                            Account(Id, Name, ParentId, CBUCID_Name_formula__c, RecordType.DeveloperName, Inactive__c, Account_Status__c where /*Account_Status__c != 'Obsolete'
                                                    AND*/ CBU_Code_RCID__c != 'T1'
                                                    AND CBU_Code_RCID__c != 'X1'
                                                    AND CBU_Code_RCID__c != 'C1'                                                    
                                                    AND CBU_CD__c != 'T1'
                                                    AND CBU_CD__c != 'X1'
                                                    AND CBU_CD__c != 'C1'
                                                    AND RecordType.DeveloperName = 'RCID'
                                                    LIMIT 200)];
            count = searchList[0].size();
            result.addall(searchList[0]);
            system.debug('result size:'+result.size());
            
            if(count<200){
                availableRoom = 200-count;
                searchList= [FIND :searchClause
                                                IN ALL FIELDS RETURNING
                                                Account(Id, Name, ParentId, CBUCID_Name_formula__c, RecordType.DeveloperName, Inactive__c, Account_Status__c where /*Account_Status__c != 'Obsolete'
                                                        AND*/ CBU_Code_RCID__c != 'T1'
                                                        AND CBU_Code_RCID__c != 'X1'
                                                        AND CBU_Code_RCID__c != 'C1'                                                    
                                                        AND CBU_CD__c != 'T1'
                                                        AND CBU_CD__c != 'X1'
                                                        AND CBU_CD__c != 'C1'
                                                        AND RecordType.DeveloperName = 'CBUCID'
                                                        LIMIT :availableRoom )];
                count += searchList[0].size();
                result.addall(searchList[0]);
            }

            if(count<200){
                availableRoom = 200-count;
                searchList= [FIND :searchClause
                                                IN ALL FIELDS RETURNING
                                                Account(Id, Name, ParentId, CBUCID_Name_formula__c, RecordType.DeveloperName, Inactive__c, Account_Status__c where /*Account_Status__c != 'Obsolete'
                                                        AND*/ CBU_Code_RCID__c != 'T1'
                                                        AND CBU_Code_RCID__c != 'X1'
                                                        AND CBU_Code_RCID__c != 'C1'                                                    
                                                        AND CBU_CD__c != 'T1'
                                                        AND CBU_CD__c != 'X1'
                                                        AND CBU_CD__c != 'C1'
                                                        AND RecordType.DeveloperName != 'CBUCID'
                                                        AND RecordType.DeveloperName != 'RCID'     
                                                        LIMIT :availableRoom )];
                count += searchList[0].size();
                result.addall(searchList[0]);
            }
            
            return result;
        }
        catch (Exception ex) {
            System.debug(ex.getMessage() + ' ' + ex.getStackTraceString());
        }
        return null;
    }
    
    public static List<Account> processAccountResults(Map<Id, IdAndAccount> accountsMappedToRcid, List<Account> allAccounts, set<Id>theAddressAccounts) {

        List<Account> workingAccountList = [SELECT Id, ParentId, Name, CBUCID_Parent_Id__c, RecordType.DeveloperName,
                                                   Marketing_Sub_Segment_Description__c, CBU_Code_RCID__c, CBU_CD__c, CBU_Name__c,
                                                   Phone, BillingStreet, BillingCity, CBUCID_Name_formula__c,
                                                   BillingState, RCID__c, CAN__c, BTN__c, 
                                                   RecordType.Name, Billing_Account_Active_Indicator__c, Inactive__c, Account_Status__c, 
                                                    (SELECT Id, Unit_Number__c,
                                                            City__c,State__c,Postal_Code__c,
                                                            Street_Address__c
                                                     FROM Addresses1__r
                                                     LIMIT 1 )
                                            FROM Account
                                            WHERE (/*Id In :accountsMappedToRcid.keySet()
                                                    OR*/
                                                  Id In :allAccounts
                                                    OR
                                                  Id In :theAddressAccounts) 
                                                  /*AND Account_Status__c != 'Obsolete'*/
                                                  AND CBU_Code_RCID__c != 'T1'
                                                  AND CBU_Code_RCID__c != 'X1'
                                                  AND CBU_Code_RCID__c != 'C1'                                                    
                                                  AND CBU_CD__c != 'T1'
                                                  AND CBU_CD__c != 'X1'
                                                  AND CBU_CD__c != 'C1'
                                                  AND RecordType.DeveloperName = 'RCID'
                                            ORDER BY NAME, RCID__c NULLS last
                                            LIMIT 200];

        //system.debug('global workingAccountList size:'+workingAccountList.size());

        if(workingAccountList.size()<200){
            List<Account> tempCBUCIDs = [SELECT Id, ParentId, Name, CBUCID_Parent_Id__c, RecordType.DeveloperName,
                                                Marketing_Sub_Segment_Description__c, CBU_Code_RCID__c, CBU_CD__c, CBU_Name__c,
                                                Phone, BillingStreet, BillingCity, CBUCID_Name_formula__c,
                                                BillingState, RCID__c, CAN__c, BTN__c, 
                                                RecordType.Name, Billing_Account_Active_Indicator__c, Inactive__c, Account_Status__c, 
                                                 (SELECT Id, Unit_Number__c,
                                                         City__c,State__c,Postal_Code__c,
                                                         Street_Address__c
                                                  FROM Addresses1__r
                                                  LIMIT 1 )
                                         FROM Account
                                         WHERE (/*Id In :accountsMappedToRcid.keySet()
                                                 OR*/
                                                Id In :allAccounts
                                                 OR
                                                Id In :theAddressAccounts) 
                                                /*AND Account_Status__c != 'Obsolete'*/
                                                AND CBU_Code_RCID__c != 'T1'
                                                AND CBU_Code_RCID__c != 'X1'
                                                AND CBU_Code_RCID__c != 'C1'                                                    
                                                AND CBU_CD__c != 'T1'
                                                AND CBU_CD__c != 'X1'
                                                AND CBU_CD__c != 'C1'
                                                AND RecordType.DeveloperName = 'CBUCID'
                                         ORDER BY NAME, RCID__c NULLS last
                                         limit 200];

            integer availableRoom = workingAccountList.size();
            for(integer i=0;i<(200-availableRoom);i++){
                if(i<tempCBUCIDs.size()) workingAccountList.add(tempCBUCIDs.get(i));
            }
            //system.debug('global tempCBUCIDs size:'+tempCBUCIDs.size());     
            //system.debug('global 2nd workingAccountList size:'+workingAccountList.size());                                                                              
        }

        if(workingAccountList.size()<200){
            List<Account> tempBCANs = [SELECT Id, ParentId, Name, CBUCID_Parent_Id__c, RecordType.DeveloperName,
                                              Marketing_Sub_Segment_Description__c, CBU_Code_RCID__c, CBU_CD__c, CBU_Name__c,
                                              Phone, BillingStreet, BillingCity, CBUCID_Name_formula__c,
                                              BillingState, RCID__c, CAN__c, BTN__c, 
                                              RecordType.Name, Billing_Account_Active_Indicator__c, Inactive__c, Account_Status__c, 
                                               (SELECT Id, Unit_Number__c,
                                                       City__c,State__c,Postal_Code__c,
                                                       Street_Address__c
                                                FROM Addresses1__r
                                                LIMIT 1 )
                                       FROM Account
                                       WHERE (/*Id In :accountsMappedToRcid.keySet()
                                               OR*/
                                              Id In :allAccounts
                                               OR
                                              Id In :theAddressAccounts) 
                                              /*AND Account_Status__c != 'Obsolete'*/
                                              AND CBU_Code_RCID__c != 'T1'
                                              AND CBU_Code_RCID__c != 'X1'
                                              AND CBU_Code_RCID__c != 'C1'                                                    
                                              AND CBU_CD__c != 'T1'
                                              AND CBU_CD__c != 'X1'
                                              AND CBU_CD__c != 'C1'
                                              AND RecordType.DeveloperName != 'CBUCID'
                                              AND RecordType.DeveloperName != 'RCID'                                                      
                                       ORDER BY NAME, RCID__c NULLS last
                                       limit 200];

            integer availableRoom = workingAccountList.size();
            for(integer i=0;i<(200-availableRoom);i++){
                if(i<tempBCANs.size()) workingAccountList.add(tempBCANs.get(i));
                //system.debug('global loop '+i+' workingAccountList size:'+workingAccountList.size());                                                                           
            }
            //system.debug('global tempBCANs size:'+tempBCANs.size());                        
        }
 
        //system.debug('global final workingAccountList size:'+workingAccountList.size());                                                           
        return workingAccountList;
        
    }
    
}