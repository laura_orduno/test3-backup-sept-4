@isTest
public class DealerPortalUtilTest {
	/*@isTest
    private static void testDealerPortalUtil1(){
        
        Id rcidAccntRecordTypeId=Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('RCID').getRecordTypeId();
        Account acc = new Account(Name='test',BTN__c='9999999999', CAN__c='testcan1209', REcordTypeId=rcidAccntRecordTypeId);
        insert acc;
        Contact rcidcon1 = new Contact(LastName='last',accountid=acc.id,firstname='first',email='test@test.com');
        Map<String, Object> input = new Map<String, Object>();
        input.put('phoneNumber','9999999999');
        Map<String, Object> output = new Map<String, Object>();
        Map<String, Object> options = new Map<String, Object>();
        Test.StartTest();
        DealerPortalUtil obj = new DealerPortalUtil();
        obj.invokeMethod('getAccountsandContactsWithPhoneNumber',input,output,options);
        input.put('accountNumber','testcan1209');
        obj.invokeMethod('getAccountsandContactsWithAccountNumber',input,output,options);
    	DealerPortalUtil.getDealerAdminPricipalRoles();
        DealerPortalUtil.getDealerSalesRepIdsFromCache();
        Test.stopTest();
    }*/
    @isTest
    private static void testDealerPortalUtil2(){
        Account cbucid = new Account(
            Name = 'TestCBUCID',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CBUCID').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5'
        );
        insert cbucid;
              
        Id rcidAccntRecordTypeId=Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('RCID').getRecordTypeId();
        Account acc = new Account(Name='test',BTN__c='8888888888', CAN__c='cbuan1098-111', REcordTypeId=rcidAccntRecordTypeId,parentId=cbucid.id);
        insert acc;
        Account banacc = new Account(
            Name = 'TestCBUCID',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('BAN').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            BTN__c='8888888888',
            CAN__c='cbuan1098-222',
            parentId=acc.id
        );
        insert banacc;
        
        Contact rcidcon1 = new Contact(LastName='last',accountid=acc.id,firstname='first',homePhone='8888888888',mobilePhone='8888888888',otherPhone='',phone='8888888888',Fax='',assistantPhone='',email='test@test.com');
        Contact rcidcon2 = new Contact(LastName='last2',accountid=acc.id,firstname='first2',homePhone='9999999999',mobilePhone='9999999999',otherPhone='',phone='9999999999',Fax='',assistantPhone='',email='test@test.com');
        insert rcidcon1;
        insert rcidcon2;
        Map<String, Object> input = new Map<String, Object>();
        input.put('phoneNumber','8888888888');
        Map<String, Object> output = new Map<String, Object>();
        Map<String, Object> options = new Map<String, Object>();
        Test.StartTest();
        DealerPortalUtil obj = new DealerPortalUtil();
        obj.invokeMethod('getAccountsandContactsWithPhoneNumber',input,output,options);
        //input = new Map<String, Object>();
        //input.put('accountNumber','cbuan1098-222');
        //obj.invokeMethod('getAccountsandContactsWithAccountNumber',input,output,options);
        
        List<DealerPortalUtil.AccountWrapper> accountWrapperMap = (List<DealerPortalUtil.AccountWrapper>)output.get('accountWrapperList');
        Test.stopTest();
    }
    
    @isTest
    private static void testDealerPortalUtil3(){
        Account cbucid = new Account(
            Name = 'TestCBUCID',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CBUCID').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5'
        );
        insert cbucid;
              
        Id rcidAccntRecordTypeId=Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('RCID').getRecordTypeId();
        Account acc = new Account(Name='test',BTN__c='8888888888', REcordTypeId=rcidAccntRecordTypeId,parentId=cbucid.id);
        insert acc;
        Account banacc = new Account(
            Name = 'TestCBUCID',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('BAN').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            BTN__c='8888888888',
            CAN__c='9999888899',
            parentId=acc.id
        );
        insert banacc;
        
        Contact rcidcon1 = new Contact(LastName='last',accountid=acc.id,firstname='first',homePhone='8888888888',mobilePhone='8888888888',otherPhone='',phone='8888888888',Fax='',assistantPhone='',email='test@test.com');
        Contact rcidcon2 = new Contact(LastName='last2',accountid=acc.id,firstname='first2',homePhone='9999999999',mobilePhone='9999999999',otherPhone='',phone='9999999999',Fax='',assistantPhone='',email='test@test.com');
        insert rcidcon1;
        insert rcidcon2;
        Map<String, Object> input = new Map<String, Object>();
        Map<String, Object> output = new Map<String, Object>();
        Map<String, Object> options = new Map<String, Object>();
        Test.StartTest();
        DealerPortalUtil obj = new DealerPortalUtil();
        input.put('accountNumber','9999888899');
        obj.invokeMethod('getAccountsandContactsWithAccountNumber',input,output,options);
        
        List<DealerPortalUtil.AccountWrapper> accountWrapperMap = (List<DealerPortalUtil.AccountWrapper>)output.get('accountWrapperList');
        DealerPortalUtil.getDealerAdminPricipalRoles();
        DealerPortalUtil.getDealerSalesRepIdsFromCache();
        Test.stopTest();
    }
        @isTest
    private static void testDealerPortalUtil4(){
        Account cbucid = new Account(
            Name = 'TestCBUCID',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CBUCID').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5'
        );
        insert cbucid;
              
        Id rcidAccntRecordTypeId=Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('RCID').getRecordTypeId();
        Account acc = new Account(Name='test',BTN__c='8888888888', REcordTypeId=rcidAccntRecordTypeId,parentId=cbucid.id);
        insert acc;
        Account banacc = new Account(
            Name = 'TestCBUCID',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('BAN').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            BTN__c='8888888888',
            CAN__c='9999888890',
            parentId=acc.id
        );
        insert banacc;
         Account banacc1 = new Account(
            Name = 'TestCBUCID',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('BAN').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            BTN__c='8888888888',
            CAN__c='9999888899',
            parentId=banacc.id
        );
        insert banacc1;
        Contact rcidcon1 = new Contact(LastName='last',accountid=acc.id,firstname='first',homePhone='8888888888',mobilePhone='8888888888',otherPhone='',phone='8888888888',Fax='',assistantPhone='',email='test@test.com');
        Contact rcidcon2 = new Contact(LastName='last2',accountid=acc.id,firstname='first2',homePhone='9999999999',mobilePhone='9999999999',otherPhone='',phone='9999999999',Fax='',assistantPhone='',email='test@test.com');
        insert rcidcon1;
        insert rcidcon2;
        Map<String, Object> input = new Map<String, Object>();
        Map<String, Object> output = new Map<String, Object>();
        Map<String, Object> options = new Map<String, Object>();
        Test.StartTest();
        DealerPortalUtil obj = new DealerPortalUtil();
        input.put('accountNumber','9999888899');
        obj.invokeMethod('getAccountsandContactsWithAccountNumber',input,output,options);
        
        List<DealerPortalUtil.AccountWrapper> accountWrapperMap = (List<DealerPortalUtil.AccountWrapper>)output.get('accountWrapperList');
        DealerPortalUtil.getDealerAdminPricipalRoles();
        DealerPortalUtil.getDealerSalesRepIdsFromCache();
        Test.stopTest();
    }
        @isTest
    private static void testDealerPortalUtil5(){
        Account cbucid = new Account(
            Name = 'TestCBUCID',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CBUCID').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5'
        );
        insert cbucid;
              
        Id rcidAccntRecordTypeId=Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('RCID').getRecordTypeId();
        Account acc = new Account(Name='test',BTN__c='8888888888', REcordTypeId=rcidAccntRecordTypeId,parentId=cbucid.id);
        insert acc;
        Account banacc = new Account(
            Name = 'TestCBUCID',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('BAN').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            BTN__c='8888888888',
            CAN__c='9999888890',
            parentId=acc.id
        );
        insert banacc;
        Account banacc1 = new Account(
            Name = 'TestCBUCID',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('BAN').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            BTN__c='8888888888',
            CAN__c='9999888890',
            parentId=banacc.id
        );
        insert banacc1;
        
        Account banacc2 = new Account(
            Name = 'TestCBUCID',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('BAN').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            BTN__c='8888888888',
            CAN__c='9999888899',
            parentId=banacc1.id
        );
        insert banacc2;
        
        Contact rcidcon1 = new Contact(LastName='last',accountid=acc.id,firstname='first',homePhone='8888888888',mobilePhone='8888888888',otherPhone='',phone='8888888888',Fax='',assistantPhone='',email='test@test.com');
        Contact rcidcon2 = new Contact(LastName='last2',accountid=acc.id,firstname='first2',homePhone='9999999999',mobilePhone='9999999999',otherPhone='',phone='9999999999',Fax='',assistantPhone='',email='test@test.com');
        insert rcidcon1;
        insert rcidcon2;
        Map<String, Object> input = new Map<String, Object>();
        Map<String, Object> output = new Map<String, Object>();
        Map<String, Object> options = new Map<String, Object>();
        Test.StartTest();
        DealerPortalUtil obj = new DealerPortalUtil();
        input.put('accountNumber','9999888899');
        obj.invokeMethod('getAccountsandContactsWithAccountNumber',input,output,options);
        
        List<DealerPortalUtil.AccountWrapper> accountWrapperMap = (List<DealerPortalUtil.AccountWrapper>)output.get('accountWrapperList');
        DealerPortalUtil.getDealerAdminPricipalRoles();
        DealerPortalUtil.getDealerSalesRepIdsFromCache();
        Test.stopTest();
    }
            @isTest
    private static void testDealerPortalUtil6(){
        Account cbucid = new Account(
            Name = 'TestCBUCID',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CBUCID').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5'
        );
        insert cbucid;
              
        Id rcidAccntRecordTypeId=Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('RCID').getRecordTypeId();
        Account acc = new Account(Name='test',BTN__c='8888888888', REcordTypeId=rcidAccntRecordTypeId,parentId=cbucid.id);
        insert acc;
        Account banacc = new Account(
            Name = 'TestCBUCID',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('BAN').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            BTN__c='8888888888',
            CAN__c='9999888890',
            parentId=acc.id
        );
        insert banacc;
        Account banacc1 = new Account(
            Name = 'TestCBUCID',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('BAN').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            BTN__c='8888888888',
            CAN__c='9999888890',
            parentId=banacc.id
        );
        insert banacc1;
        
        Account banacc2 = new Account(
            Name = 'TestCBUCID',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('BAN').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            BTN__c='8888888888',
            CAN__c='9999888893',
            parentId=banacc1.id
        );
        insert banacc2;
        
          Account banacc3 = new Account(
            Name = 'TestCBUCID',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('BAN').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            BTN__c='8888888888',
            CAN__c='9999888899',
            parentId=banacc2.id
        );
        insert banacc3;
        Contact rcidcon1 = new Contact(LastName='last',accountid=acc.id,firstname='first',homePhone='8888888888',mobilePhone='8888888888',otherPhone='',phone='8888888888',Fax='',assistantPhone='',email='test@test.com');
        Contact rcidcon2 = new Contact(LastName='last2',accountid=acc.id,firstname='first2',homePhone='9999999999',mobilePhone='9999999999',otherPhone='',phone='9999999999',Fax='',assistantPhone='',email='test@test.com');
        insert rcidcon1;
        insert rcidcon2;
        Map<String, Object> input = new Map<String, Object>();
        Map<String, Object> output = new Map<String, Object>();
        Map<String, Object> options = new Map<String, Object>();
        Test.StartTest();
        DealerPortalUtil obj = new DealerPortalUtil();
        input.put('accountNumber','9999888899');
        obj.invokeMethod('getAccountsandContactsWithAccountNumber',input,output,options);
        
        List<DealerPortalUtil.AccountWrapper> accountWrapperMap = (List<DealerPortalUtil.AccountWrapper>)output.get('accountWrapperList');
        DealerPortalUtil.getDealerAdminPricipalRoles();
        DealerPortalUtil.getDealerSalesRepIdsFromCache();
        Test.stopTest();
    }
            @isTest
    private static void testDealerPortalUtil7(){
        Account cbucid = new Account(
            Name = 'TestCBUCID',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CBUCID').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5'
        );
        insert cbucid;
              
        Id rcidAccntRecordTypeId=Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('RCID').getRecordTypeId();
        Account acc = new Account(Name='test',BTN__c='8888888888', REcordTypeId=rcidAccntRecordTypeId,parentId=cbucid.id);
        insert acc;
        Account banacc = new Account(
            Name = 'TestCBUCID',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('BAN').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            BTN__c='8888888888',
            CAN__c='9999888890',
            parentId=acc.id
        );
        insert banacc;
        Account banacc1 = new Account(
            Name = 'TestCBUCID',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('BAN').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            BTN__c='8888888888',
            CAN__c='9999888890',
            parentId=banacc.id
        );
        insert banacc1;
        
        Account banacc2 = new Account(
            Name = 'TestCBUCID',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('BAN').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            BTN__c='8888888888',
            CAN__c='9999888893',
            parentId=banacc1.id
        );
        insert banacc2;
          Account banacc3 = new Account(
            Name = 'TestCBUCID',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('BAN').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            BTN__c='8888888888',
            CAN__c='9999888894',
            parentId=banacc2.id
        );
        insert banacc3;
          Account banacc4 = new Account(
            Name = 'TestCBUCID',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('BAN').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            BTN__c='8888888888',
            CAN__c='9999888899',
            parentId=banacc3.id
        );
        insert banacc4;
        Contact rcidcon1 = new Contact(LastName='last',accountid=acc.id,firstname='first',homePhone='8888888888',mobilePhone='8888888888',otherPhone='',phone='8888888888',Fax='',assistantPhone='',email='test@test.com');
        Contact rcidcon2 = new Contact(LastName='last2',accountid=acc.id,firstname='first2',homePhone='9999999999',mobilePhone='9999999999',otherPhone='',phone='9999999999',Fax='',assistantPhone='',email='test@test.com');
        insert rcidcon1;
        insert rcidcon2;
        Map<String, Object> input = new Map<String, Object>();
        Map<String, Object> output = new Map<String, Object>();
        Map<String, Object> options = new Map<String, Object>();
        Test.StartTest();
        DealerPortalUtil obj = new DealerPortalUtil();
        input.put('accountNumber','9999888899');
        obj.invokeMethod('getAccountsandContactsWithAccountNumber',input,output,options);
        
        List<DealerPortalUtil.AccountWrapper> accountWrapperMap = (List<DealerPortalUtil.AccountWrapper>)output.get('accountWrapperList');
        DealerPortalUtil.getDealerAdminPricipalRoles();
        DealerPortalUtil.getDealerSalesRepIdsFromCache();
        Test.stopTest();
    }
}