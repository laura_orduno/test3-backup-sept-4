/**
 * Created by dpeterson on 2/13/2018.
 */

public with sharing abstract class Portal_OverviewCtlr {

	public transient String pageError { get; set; } // Not Set
	public String categoryType { get; set; } // Not Set
	public String q { get; set; }  // Search Criteria
	public Case c { get; set; }
	public String caseNumber { get; set; }
	public Boolean morePages { get; set; } //set in Page Numbers
	public String searchError; // set during errors
	public Map<String, String> extStatusIntStatusMap { get; set; }
	public Map<String, String> intStatusExtStatusMap { get; set; }
	public String userAccountId;
	public string AddFuncPull1 { get; set; }
	public string CustRole1 { get; set; }
	public String categoryFilter { get; set; }
	public String statusFilter { get; set; }
	public String lynxTicketNo { get; set; }//Added By RobB
	public String CustTicketNum { get; set; }
	public String refNo { get; set; }
	//public String RCID {get; set;}
	public String telusID { get; set; }
	public String caseOriginValue { get; set; }
	private List<Case> allCases;
	public Integer totalCases; //used to show user the total size of the list
	@testVisible private Integer pageSize;
	public Integer totalPages { get; set; }
	public Integer selectedPage { get; set; }
	public String CasNumViaExistingLynxTicket { get; set; }

	// required parameters needed to call into LYNX ticket system
	public transient String tID { get; set; }
	public transient String RCID { get; set; }

	public List<CaseListing> topListings { get; set; }

	// COMMENT BACK IN private ENTP_Customer_Interface_Settings__c cis; // DIFF

	public Boolean restrictTechSupport { get; set; }
	public integer FutCount = 0; // Only ENTP

	public string RoleType { get; set; } // VITIL

	public string CatListPull { get; set; } // DIFF
	Public string CPRole { get; set; } // Only VITIL
	public string Identifier;

	private Portal_Interface_Settings cis; // DIFF

	public static String userLanguage {
		get {
			return PortalUtils.getUserLanguage();
		}
		set;
	}
	public Portal_OverviewCtlr() {

	}

	public Portal_OverviewCtlr(String identifier, String caseOrigin, Portal_Interface_Settings interfaceSettings) {
		this.Identifier = identifier;
		allCases = new List<Case>();

		try {
			tID = [select Team_TELUS_ID__c from User where id = :Userinfo.getUserId()].Team_TELUS_ID__c;
		} catch (QueryException e) {
			ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, e.getMessage()));
		} catch (Exception e) {
			ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, e.getMessage()));
		}

		// Determine cis here

		cis = interfaceSettings;
		pageSize = Integer.valueOf(cis.manageRequestListLimits); //sets the page size or number of rows
		String searchCriteria = ApexPages.currentPage().getParameters().get('search');
		q = '';
		if (String.isEmpty(q) && String.isNotEmpty(searchCriteria)) {
			q = searchCriteria;
		}

		// find the Case Origin string
		this.caseOriginValue = caseOrigin;
		if (cis.caseOriginValue != null) {
			this.caseOriginValue = cis.caseOriginValue;
		}

		System.debug('caseOriginValue: ' + caseOriginValue);

		if (!Test.isRunningTest()) {
			resetRequestView();
		}
		categoryFilter = '';
		statusFilter = '';
		// FIGURE THIS OUT restrictTechSupport = VITILcareUtils.restrictTechSupport(); | ENTPUtils.restrictTechSupport();

		List<ExternalToInternal__c> etis = ExternalToInternal__c.getAll().values();
		this.extStatusIntStatusMap = new Map<String, String>();
		this.intStatusExtStatusMap = new Map<String, String>();

		if (userLanguage == 'en') {
			for (ExternalToInternal__c eti : etis) {
				// Do we need this constants
				if (eti.Identifier__c.equals('onlineStatus')) {
					this.extStatusIntStatusMap.put(eti.External__c, eti.Internal__c);
					this.intStatusExtStatusMap.put(eti.Internal__c, eti.External__c);
				}
			}
		}

		if (userLanguage == 'fr' || userLanguage == 'fr_CA') {

			for (ExternalToInternal__c eti : etis) {
				if (eti.Identifier__c.equals('onlineStatus')) {
					this.extStatusIntStatusMap.put(eti.External_FR__c, eti.Internal__c);
					this.intStatusExtStatusMap.put(eti.Internal__c, eti.External_FR__c);
				}
			}

		}
		System.debug('this.intStatusExtStatusMap: ' + intStatusExtStatusMap);

		// make sure all case statuses have a valid mapping
		/*
		How is this ever not going to be 0 at this point??
		 */
		/*if (allCases.size() > 0) {
			for (Case c : allCases) {
				if (this.intStatusExtStatusMap.get(c.Status) == null) {
					this.intStatusExtStatusMap.put(c.Status, 'Unknown');
				}
			}
		}*/
	}

	public void searchCase() {
		if (!Test.isRunningTest()) {
			resetRequestView();
		}
	}

	public PageReference resetSearch() {
		q = '';
		if (!Test.isRunningTest()) {
			resetRequestView();
		}
		return null;
	}

	public void getRecentCases() {
		Integer caseLimit = Integer.valueOf(cis.overViewCaseLimit);
		List<Case> topCases = [
				SELECT RecordTypeId, Subject, LastModifiedDate, caseNumber, Status, My_Business_Requests_Type__c, Lynx_Ticket_Number__c, Trouble_Ticket__c, Contact.FirstName, Contact.LastName
				FROM Case
				Where My_Business_Requests_Type__c <> 'Complaint' and My_Business_Requests_Type__c <> 'Feedback' and My_Business_Requests_Type__c <> 'Compliment' and My_Business_Requests_Type__c <> 'Idea' and Origin = :caseOriginValue
				ORDER BY LastModifiedDate DESC
				LIMIT :caseLimit
		];
		topListings = new List<CaseListing>{
		};
		for (Case c : topCases) {
			topListings.add(new CaseListing(c, identifier));
		}
	}

	public List<Case> getPageOfCases(Integer counter, Integer size) {
		List<Case> listCases = new List<Case>();

		Boolean recount = false;
		listCases = executeSearch(counter, size, recount);

		return listCases;
	}

	public PageReference doFilterUpdate() {
		// Need to run two queries so page numbers are updated....  not ideal.  Should do a 'count' instead.
		if (!Test.isRunningTest()) {
			resetRequestView();
		}
		return null;
	}

	public abstract PageReference searchOverview();
	public abstract PageReference initCheck();
	public abstract PageReference createNewCase();
	public abstract PageReference getCaseDetails();
	public abstract List<SelectOption> getCategoryFilterOptions();
	public abstract List<SelectOption> getStatusFilterOptions();
	public abstract List<Case> executeSearch(Integer counter, Integer size, Boolean recount);

	public List<CaseListing> allListings {
		get {
			if (selectedPage == null || selectedPage <= 0) {
				selectedPage = 1;
			}
			Integer counter = pageSize * (selectedPage - 1);

			try {
				allListings = new List<CaseListing>{
				};
				//we have to catch query exceptions in case the list is greater than 2000 rows
				List<Case> cases = getPageOfCases(counter, pageSize);

				for (Case c : cases) {
					allListings.add(new CaseListing(c, identifier));
				}
				system.debug('allListings??: ' + allListings.size());
				return allListings;

			} catch (QueryException e) {
				ApexPages.addMessages(e);
				return new List<CaseListing>();
			}
		}
		set;
	}

	public List<Integer> pageNumbers {
		get {
			Integer lowerLimit = 1;
			if (selectedPage >= 4 && totalPages > 4) {
				Integer nearestEvenNumber = selectedPage - Math.mod(selectedPage, 2);
				lowerLimit = (totalPages - nearestEvenNumber) >= 3 ? nearestEvenNumber - 1 : totalPages - 3;
			}

			Integer upperLimit = (lowerLimit + 3 >= totalPages) ? totalPages : lowerLimit + 3;

			List<Integer> pageNumbers = new List<Integer>();
			for (Integer i = lowerLimit; i <= upperLimit; i++) {
				pageNumbers.add(i);
			}

			if (upperLimit < totalPages) {
				morePages = true;
			} else {
				morePages = false;
			}
			system.debug('pageNumbers??: ' + pageNumbers);
			return pageNumbers;
		}
		set;
	}

	@TestVisible public void resetRequestView() {
		Boolean recount = true;
		allCases = executeSearch(0, pageSize, recount);
		totalCases = allCases.size();
		totalPages = getTotalPages();
		selectedPage = 0;
	}

	@TestVisible private Integer getTotalPages() {
		Integer totalPages = totalCases / pageSize;
		if (math.mod(totalCases, pageSize) > 0) {
			totalPages += 1;
		}
		return totalPages;
	}

	public PageReference refreshGrid() { //user clicked a page number
		return null;
	}

	public PageReference Previous() { //user clicked previous button
		selectedPage--;
		return null;
	}

	public PageReference Next() { //user clicked next button
		selectedPage++;
		return null;
	}

	public Boolean getDisablePrevious() { //this will disable the previous and beginning buttons
		return selectedPage <= 1;
	}

	public Boolean getDisableNext() { //this will disable the next and end buttons
		return selectedPage >= totalPages;
	}

	public class CaseListing {
		public String status { get; set; }
		public String statusIcon { get; set; }
		public String refNo { get; set; }
		public String lynxTicketNo { get; set; }
		public String type { get; set; }
		public String subject { get; set; }
		public String timeDiff { get; set; }
		public String RecordTp { get; set; }
		public string CustTicketNum { get; set; }
		// public String FirstName {get; set;}
		// public String LastName {get; set;}
		public String CreatedBy { get; set; }
		public String identifier;

		public CaseListing(Case tmpCase, String identifier) {
			this.refNo = tmpCase.caseNumber;
			this.lynxTicketNo = tmpCase.Lynx_Ticket_Number__c;
			this.CustTicketNum = tmpCase.Trouble_Ticket__c;
			this.type = getRecordType(tmpCase);
			this.RecordTp = tmpCase.My_Business_Requests_Type__c;
			this.subject = tmpCase.Subject;
			this.status = tmpCase.status;
			this.CreatedBy = tmpCase.Contact.FirstName + ' ' + tmpCase.Contact.LastName;
			this.statusIcon = 'status-' + tmpCase.status;
			this.timeDiff = getTimeDiff(tmpCase.LastModifiedDate);
			this.identifier = identifier;
		}

		public String getRecordType(Case tmpCase) {
			String internalName = Schema.SObjectType.Case.getRecordTypeInfosById().get(tmpCase.RecordTypeId).getName();
			system.debug('internalName:-' + internalName);
			//List<ExternalToInternal__c> etis = ExternalToInternal__c.getAll().values();
			List<ExternalToInternal__c> etis = [Select Name, External__c, External_FR__c, Internal__c, Identifier__c from ExternalToInternal__c];
			system.debug('ExterToInternal:-' + etis);
			String typeName = tmpCase.My_Business_Requests_Type__c;
			if (internalName != null) {
				for (ExternalToInternal__c eti : etis) {
					if (eti.Identifier__c.equals(identifier) && eti.Internal__c.equals(internalName)) {
						typeName = eti.External__c;
					}
				}
			}
			if (typeName == '' || typeName == null) {
				typeName = Label.MBRUnknownRecordType;
			}
			return typeName;
		}

		public String getTimeDiff(DateTime modifDate) {
			String dateDif = '';
			DateTime todayDate = DateTime.now();
			if (modifDate != null) {
				Integer hourDif = Integer.valueOf((todayDate.getTime() - modifDate.getTime()) / (1000 * 60 * 60));
				Integer hoursInMonth = 24 * 30;
				if (userLanguage == 'en') {
					if (hourDif == 0) {
						dateDif = Label.VITILcareTimeNow;
					} else if (hourDif == 1) {
						dateDif = Label.VITILcare1HourAgo;
					} else if (hourDif < 24) {
						dateDif = hourDif + ' ' + Label.VITILcareHoursAgo;
					} else if (hourDif < 48) {
						dateDif = Label.VITILcare1day;
					} else if (hourDif < hoursInMonth) {
						dateDif = hourDif / 24 + ' ' + Label.VITILcareDaysago;
					} else if (hourDif < hoursInMonth * 2) {
						dateDif = Label.VITLcareOver1Mth;
					} else {
						dateDif = Label.VITILcareOver + ' ' + hourDif / hoursInMonth + ' ' + Label.VITILcareMonthsAgo;
					}
				}
				if (userLanguage == 'fr') {
					system.debug('GetTimeDiff(622):=' + userLanguage);
					if (hourDif == 0) {
						dateDif = Label.VITILcareTimeNow_FR;
					} else if (hourDif == 1) {
						dateDif = Label.VITILcare1HourAgo_FR;
					} else if (hourDif < 24) {
						dateDif = label.VITILcare_Time_FR + ' ' + hourDif + ' ' + Label.VITILcareHoursAgo_FR;
					} else if (hourDif < 48) {
						dateDif = Label.VITILcare1day_FR;
					} else if (hourDif < hoursInMonth) {
						dateDif = label.VITILcare_Time_FR + ' ' + hourDif / 24 + ' ' + Label.VITILcareDaysago_FR;
					} else if (hourDif < hoursInMonth * 2) {
						dateDif = Label.VITILcareOver1Mth_FR;
					} else {
						dateDif = Label.VITILcare_Time_FR_PLUS + ' ' + hourDif / hoursInMonth + ' ' + Label.VITILcareMonthsAgo_FR;
					}

				}

			}
			return dateDif;
		}
	}

	@TestVisible private string abbreviate(string text, integer length) {
		if (string.isBlank(text)) return '';

		return text.abbreviate(length);
	}
	public String getSearchError() {
		return searchError; //return searchError value
	}

	//String userLanguage;

	public String getLanguageLocaleKey() {
		return PortalUtils.getUserLanguage();
	}

	public static void switchLanguage() {
		PortalUtils.setUserLanguage();
	}
}