/* 2015-12-12 [Roderick de Vera]
 * Web Service Mock implementation for all ASF type of webservices
 */
@isTest
global class OrdrWsAsfMockImpl implements WebServiceMock {
    
    Boolean  isMockResponseSuccessful;  // set by constructor, used to vary the mockresponse
    
    public OrdrWsAsfMockImpl() {
        this.isMockResponseSuccessful  = true;
    }  
    
    public OrdrWsAsfMockImpl(Boolean isMockResponseSuccessful) {
        this.isMockResponseSuccessful  = isMockResponseSuccessful;
    }  
    
    global void doInvoke(
        Object stub,
        Object request,
        Map<String, Object> response,
        String endpoint,
        String soapAction,
        String requestName,
        String responseNS,
        String responseName,
        String responseType) {
            
            if (!isMockResponseSuccessful) {
                CalloutException e = (CalloutException) CalloutException.class.newInstance();
                e.setMessage('This is a simulated CalloutException');
                throw e;
            }
            
            System.debug('OrdrWsAsfMockImpl invoking soapAction: ' + soapAction);
            
            if(soapAction.endsWith('ping')){
                response.put('response_x', preparePingResponse());
            }
            else if (soapAction.endsWith('getRateGroupAndForborneInformationByNpaNxx')){
               response.put('response_x', prepareRateGroupAndForborneResponse());
            }
            else if (soapAction.endsWith('getRateGroupInfoByNpaNxx')){                
                response.put('response_x', prepareRateGroupResponse());
            }
            
        }
    
    private RateGroupInfo_EnterpriseB.pingResponse_element preparePingResponse() {
        
        RateGroupInfo_EnterpriseB.pingResponse_element response = new RateGroupInfo_EnterpriseB.pingResponse_element();
        response.version = '1.0';
       
        return response;
    }
        
        
    private RateGroupInfo_SrvCmoBillinga.getRateGroupAndForborneInformationByNpaNxxResponse_element prepareRateGroupAndForborneResponse() {
        
        RateGroupInfo_SrvCmoBillinga.getRateGroupAndForborneInformationByNpaNxxResponse_element response = new RateGroupInfo_SrvCmoBillinga.getRateGroupAndForborneInformationByNpaNxxResponse_element();
        RateGroupInfo_ResourceRes.RateGroupAndForborneAttribute attribute = new RateGroupInfo_ResourceRes.RateGroupAndForborneAttribute();
         
        RateGroupInfo_ResourceRes.RateGroupAttribute rateGroupAttribute = new RateGroupInfo_ResourceRes.RateGroupAttribute (); 
        RateGroupInfo_ResourceRes.RateGroupHistoryList rateGroupHistoryList = new RateGroupInfo_ResourceRes.RateGroupHistoryList();
        rateGroupHistoryList.rateGroupList = new List<RateGroupInfo_ResourceRes.RateGroup>();
        RateGroupInfo_ResourceRes.RateGroup rateGroup = new RateGroupInfo_ResourceRes.RateGroup();
        
        rateGroup.rateGroupAttribute = '13';
        rateGroup.rateBand = 'B';
        rateGroup.rateSubBand = 'B3';

        rateGroupHistoryList.rateGroupList.add(rateGroup);
        rateGroupAttribute.rateGroupHistoryList = rateGroupHistoryList;
        attribute.rateGroupAttribute = rateGroupAttribute;
        
        RateGroupInfo_ResourceRes.ExchangeForborneStatus exchangeForborneStatus = new RateGroupInfo_ResourceRes.ExchangeForborneStatus(); 
        exchangeForborneStatus.exchangeForborneStatusInd = true;
        attribute.exchangeForborneStatus = exchangeForborneStatus;
        
        response.rateGroupAndForborneAttribute = attribute;
        
        return response;
    }
    
    
	private RateGroupInfo_SrvCmoBillinga.getRateGroupInfoByNpaNxxResponse_element prepareRateGroupResponse() {
        
        RateGroupInfo_SrvCmoBillinga.getRateGroupInfoByNpaNxxResponse_element response = new RateGroupInfo_SrvCmoBillinga.getRateGroupInfoByNpaNxxResponse_element();
         
        RateGroupInfo_ResourceRes.RateGroupAttribute rateGroupAttribute = new RateGroupInfo_ResourceRes.RateGroupAttribute (); 
        RateGroupInfo_ResourceRes.RateGroupHistoryList rateGroupHistoryList = new RateGroupInfo_ResourceRes.RateGroupHistoryList();
        rateGroupHistoryList.rateGroupList = new List<RateGroupInfo_ResourceRes.RateGroup>();
        RateGroupInfo_ResourceRes.RateGroup rateGroup = new RateGroupInfo_ResourceRes.RateGroup();
        
        rateGroup.rateGroupAttribute = '13';
        rateGroup.rateBand = 'B';
        rateGroup.rateSubBand = 'B3';

        rateGroupHistoryList.rateGroupList.add(rateGroup);
        rateGroupAttribute.rateGroupHistoryList = rateGroupHistoryList;
        
        response.rateGroupAttribute = rateGroupAttribute;       
        return response;
    }
    

}