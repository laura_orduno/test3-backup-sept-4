@isTest
public class QuotePortalUtilsTest {

    private static Web_Account__c account;
    private static SBQQ__Quote__c quote;
    private static SBQQ__QuoteLine__c line;
    
    testMethod static void test() {
        setUp();
        
        PageReference pref = ApexPages.currentPage();
        pref.getParameters().put('step','');
        pref.getParameters().put('ms','');
        pref.getParameters().put('addon','');
        pref.getParameters().put('qid', quote.Id);
        QuotePortalUtils.addConfigurationSummaryParams(ApexPages.currentPage());
        QuotePortalUtils.isAssignedAccountsUser();
        QuotePortalUtils.getDealershipId();
        QuotePortalUtils.getChannelOutletId();
        QuotePortalUtils.updateQuoteAddress(account, account.Id, quote);
        QuotePortalUtils.loadQuoteLines(quote.Id, true);
        QuotePortalUtils.loadQuoteById(quote.Id);
        QuotePortalUtils.loadAccountById(account.Id);
        QuotePortalUtils.getAdditionalInfoFieldList();
    }
    
    private static testMethod void testAddReturnURL() {
        setUp();
        
        PageReference pref = ApexPages.currentPage();
        pref.getParameters().put('step','5');
        pref.getParameters().put('ms','1');
        pref.getParameters().put('addon','1');
        pref.getParameters().put('qid', quote.Id);
        QuotePortalUtils.addReturnURLParam(Page.QuoteSummary, Page.QuoteCompleteInformation);
    }
    
    /*
    private static testMethod void testSendProvisioningEmail() {
        setUp();
        QuotePortalUtils.sendProvisioningEmail(quote);
    }
    */

    private static testMethod void testUserInfo() {
        User quoter = [SELECT Id FROM User WHERE Channel__c != null and IsActive = true Limit 1];
        System.runas(quoter) {
            User me = [SELECT Quoting_Dealer__c, Quoting_Experience_Excellence__c, Channel__c, Contact.AccountId, Contact.Channel_Outlet_ID__c, Assigned_Accounts__c FROM User WHERE Id = :UserInfo.getUserID()];
        
            // Check the user's channel
            System.assertEquals(me.Channel__c, QuotePortalUtils.getChannel());
            
            // Check if the user is a dealer
            System.assertEquals(me.Quoting_Dealer__c == true, QuotePortalUtils.isUserDealer());
            
            // Check if the user is an EE user
            System.assertEquals(me.Quoting_Experience_Excellence__c == true, QuotePortalUtils.isExperienceExcellenceUser());
            
        }
    }
    
    
    private static void setUp(){
        account = new Web_Account__c(Name='UnitTest');
        insert account;
        
        Product2 p = new Product2(Name='Test',SBQQ__ConfigurationFields__c='Service_Term__c',Network__c = 'HSPA');
        insert p;
        
        Opportunity opp = new Opportunity(Name='Test',Web_Account__c=account.Id,CloseDate=System.today(),StageName='Prospecting');
        insert opp;
        
        quote = new SBQQ__Quote__c(SBQQ__Opportunity__c=opp.Id);
        quote.Rate_Band__c = 'F';
        quote.Province__c = 'BC';
        insert quote;
        
        line = new SBQQ__QuoteLine__c(SBQQ__Quote__c=quote.Id,SBQQ__Bundle__c=true,SBQQ__Product__c=p.Id);
        insert line;
    }

}