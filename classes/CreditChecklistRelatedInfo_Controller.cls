public with sharing class CreditChecklistRelatedInfo_Controller {

    public String rtBillingHistoricalRecord {get; set;}
    public String rtDelinquentAccount {get; set;}
    public String rtDirectorHistory {get; set;}
    public String rtOtherProfileRecord {get; set;}
    public String rtRelatedAccount {get; set;}
    public String rtTradeReference {get; set;}
    public String checklistFieldId {get;set;}
    public String checklistName {get;set;}
    public String creditProfileID {get;set;}

    public CreditChecklistRelatedInfo_Controller(ApexPages.StandardSetController controller) {
        Credit_ID_mapping__c Credit_Field_IDs = Credit_ID_Mapping__c.getInstance('Credit Checklist');
        checklistFieldId = Credit_Field_IDs.Field_ID__c;
        try {
          System.debug('..........before checklist name');
          Credit_Checklist__c chk =[select ID, Name, credit_assessment_ref_no__r.credit_profile__r.id from Credit_Checklist__c where ID=:ApexPages.currentPage().getParameters().get('id')];
          checklistName=chk.name;  
          creditProfileID=chk.Credit_Assessment_Ref_No__r.credit_profile__r.id;
          System.debug('..........'+checklistName);
        } catch (Exception e) {
            System.debug('Error in retrieving Checklist ID: ' + e.getMessage());
        }
            
    }  

    public CreditChecklistRelatedInfo_Controller(ApexPages.StandardController controller) {
    }
    
  /* Billing & Historical Records (BRS, ABCD, SAS) */
  public ApexPages.StandardSetController recsBHR {
    get {
      if (recsBHR == null) {
        try {
        recsBHR = new ApexPages.StandardSetController(Database.getQueryLocator([
          SELECT Id, Name, RecordTypeId,
              Billing_System__c, Account_ID__c, Account_Status__c, Comment__c, Account_Creation_Date__c
            FROM
              Credit_Related_Info__c
            WHERE
              RecordType.DeveloperName = 'Billing_Historical_Record' AND
              Credit_Checklist__c = :ApexPages.currentPage().getParameters().get('id')
            ORDER BY
              Name
          ]));
          } catch (Exception e) {
              System.debug ('Error in retrieving data from Credit_Related_Info__c for Billing Historiacl Records' + e.getMessage());
          }
      } 
      return recsBHR;
    }
    set;
  }

  public List<Credit_Related_Info__c> getBillingHistoricalRecords(){
    rtBillingHistoricalRecord = Schema.SObjectType.Credit_Related_Info__c.getRecordTypeInfosByName().get(Label.Billing_historical_records).getRecordTypeId();               
      return (List<Credit_Related_Info__c>) recsBHR.getRecords();
  } 

  /* Delinquent Accounts */
  public ApexPages.StandardSetController recsDA {
    get {
      if(recsDA == null) {
         try {
            recsDA = new ApexPages.StandardSetController(Database.getQueryLocator([
            SELECT Id, Name, RecordTypeId,
              Billing_System__c, Account_ID__c, Days_In_Arrears__c, Dispute_No__c, Comment__c
            FROM
              Credit_Related_Info__c
            WHERE
              RecordType.DeveloperName = 'Delinquent_Account' AND
              Credit_Checklist__c = :ApexPages.currentPage().getParameters().get('id')
            ORDER BY
              Name          
                  ]));
          } catch (Exception e) {
              System.debug ('Error in retrieving data from Credit_Related_Info__c for Delinquent Accounts' + e.getMessage());
          }
          
      } 
      return recsDA;
    }
    set;
  }

  public List<Credit_Related_Info__c> getDelinquentAccounts(){
    rtDelinquentAccount = Schema.SObjectType.Credit_Related_Info__c.getRecordTypeInfosByName().get(Label.Delinquent_Account).getRecordTypeId();               
    return (List<Credit_Related_Info__c>) recsDA.getRecords();
  } 

  /* Director History */
  public ApexPages.StandardSetController recsDH {
    get {
      if (recsDH == null) {          
         try {
          recsDh = new ApexPages.StandardSetController(Database.getQueryLocator([
          SELECT Id, Name, RecordTypeId,
              Director_Name__c, Title__c, Account_ID__c, Account_Status__c, Comment__c
            FROM
              Credit_Related_Info__c
            WHERE
              RecordType.DeveloperName = 'Director_History' AND
              Credit_Checklist__c = :ApexPages.currentPage().getParameters().get('id')
            ORDER BY
              Name
          ]));
          } catch (Exception e) {
              System.debug ('Error in retrieving data from Credit_Related_Info__c for Director History' + e.getMessage());
          }
      } 
      return recsDH;
    }
    set;
  }

  public List<Credit_Related_Info__c> getDirectorHistory(){
    rtDirectorHistory = Schema.SObjectType.Credit_Related_Info__c.getRecordTypeInfosByName().get(Label.Director_History).getRecordTypeId();               
    return (List<Credit_Related_Info__c>) recsDH.getRecords();
  } 

  /* Other Profile Records */
  public ApexPages.StandardSetController recsOPR {
    get {
      if(recsOPR == null) {
          try{
          recsOPR = new ApexPages.StandardSetController(Database.getQueryLocator([
          SELECT Id, Name, Credit_Checklist__c,
              Account_Name__c, RCID__c, Delinquent_Accounts__c, Risk_Indicator__c,
               Valid_Legal_Name__c, Last_Validation_Date__c, Comment__c,
              LastModifiedBy.name
            FROM
              Credit_Related_Info__c
            WHERE
              RecordType.DeveloperName = 'Other_Profile_Record' AND
              Credit_Checklist__c = :ApexPages.currentPage().getParameters().get('id')
            ORDER BY
              Name
          ]));
          } catch (Exception e) {
              System.debug ('Error in retrieving data from Credit_Related_Info__c for Other Profile Records' + e.getMessage());
          }
      } 
      return recsOPR;
    }
    set;
  }

  public List<Credit_Related_Info__c> getOtherProfileRecords(){
      rtOtherProfileRecord = Schema.SObjectType.Credit_Related_Info__c.getRecordTypeInfosByName().get(Label.Other_Profile_Records).getRecordTypeId();               
      return (List<Credit_Related_Info__c>) recsOPR.getRecords();
  } 

  /* Related Accounts */
  public ApexPages.StandardSetController recsRA {
    get {
      if(recsRA == null) {
          try {
          recsRA = new ApexPages.StandardSetController(Database.getQueryLocator([
          SELECT Id, Name, RecordTypeId,
              Account_Name__c, RCID__c, Delinquent_Accounts__c, Risk_Indicator__c, Comment__c
            FROM
              Credit_Related_Info__c
            WHERE
              RecordType.DeveloperName = 'Related_Account' AND
              Credit_Checklist__c = :ApexPages.currentPage().getParameters().get('id')
            ORDER BY
              Name
          ]));
          } catch (Exception e) {
              System.debug ('Error in retrieving data from Credit_Related_Info__c for Related Accounts' + e.getMessage());
          }
      } 
      return recsRa;
    }
    set;
  }

  public List<Credit_Related_Info__c> getRelatedAccounts(){
      
    rtRelatedAccount = Schema.SObjectType.Credit_Related_Info__c.getRecordTypeInfosByName().get(Label.Related_Accounts).getRecordTypeId();               
    return (List<Credit_Related_Info__c>) recsRA.getRecords();
  } 

  /* Trade References */
  public ApexPages.StandardSetController recsTR {
    get {
      if(recsTR == null) {
       Try{
            recsTR = new ApexPages.StandardSetController(Database.getQueryLocator([
          SELECT Id, Name, RecordTypeId, report_name__c,
              Reference_Name__c, Contact_Name__c, Contact_No__c, Fax_No__c, Contact_Email__c,Contacted_Via__c, Customer_Since__c,
              Frequency_Of_Billing__c, Contact_Date__c, Response_Date__c, Comment__c, Preview_Document__c, Document_ID__c
            FROM
              Credit_Related_Info__c
            WHERE
              RecordType.DeveloperName = 'Trade_Reference' AND
              Credit_Checklist__c = :ApexPages.currentPage().getParameters().get('id')
            ORDER BY
              Name
          ]));
          } catch (Exception e) {
              System.debug ('Error in retrieving data from Credit_Related_Info__c for Trade References' + e.getMessage());
          }

      } 
      return recsTR;
    }
    set;
  }

  public List<Credit_Related_Info__c> getTradeReferences(){
    rtTradeReference = Schema.SObjectType.Credit_Related_Info__c.getRecordTypeInfosByName().get(Label.Trade_Reference).getRecordTypeId();                     
    return (List<Credit_Related_Info__c>) recsTR.getRecords();
  } 
}