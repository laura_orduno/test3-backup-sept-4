/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class smb_calendar_Test {

    static testMethod void smb_calendar_eventItemTest() {
        datetime dt = datetime.now();
        SMB_CalenderEvents calenderEvents = new SMB_CalenderEvents(dt,dt.addDays(2),true,'Test');
        smb_calendar_eventItem calendar_eventItem = new smb_calendar_eventItem(calenderEvents);
        calendar_eventItem.getEv();
        calendar_eventItem.getFormatedDate();     
    }
    static testMethod void smb_calendar_monthTest() {
        Date dt = Date.today();
        smb_calendar_month calendar_month = new smb_calendar_month(dt); 
        smb_calendar_month smb_calendar_month2 = new smb_calendar_month(dt, dt.addDays(2));
        smb_calendar_month smb_calendar_month3 = new smb_calendar_month(dt, dt.addDays(2));
        smb_calendar_month.Day day = new smb_calendar_month.Day(dt ,1);
        smb_calendar_month.Week week = new smb_calendar_month.Week();
        calendar_month.getfirstDate();
        calendar_month.getMonthName();
        calendar_month.getValidDateRange();
        calendar_month.getWeekdayNames();
        calendar_month.getWeeks();
        calendar_month.getYearName();
        datetime dateTe = datetime.now();
        SMB_CalenderEvents calenderEvents = new SMB_CalenderEvents(dateTe ,dateTe.addDays(2),true,'Test');
        calendar_month.setEvents(new List<SMB_CalenderEvents>{calenderEvents});
    }
    static testMethod void SMB_CalenderEventsTest() {
        datetime dt = datetime.now();
        SMB_CalenderEvents calenderEvents = new SMB_CalenderEvents(dt,dt.addDays(2),true,'Test');
    }
}