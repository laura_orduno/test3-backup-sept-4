/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class cms3_1test_AyncCms3_1CtrtMgmtScRqRs_v3 {

static testMethod void AsyncCms3_1ContractMgmtSvcReqRes_v3() {
       AsyncCms3_1ContractMgmtSvcReqRes_v3 smb21 = new AsyncCms3_1ContractMgmtSvcReqRes_v3();
       AsyncCms3_1ContractMgmtSvcReqRes_v3.calculateTerminationChargesResponse_elementFuture a1 = new AsyncCms3_1ContractMgmtSvcReqRes_v3.calculateTerminationChargesResponse_elementFuture();
      try{
       a1.getvalue();
       } catch(Exception e) {}
       AsyncCms3_1ContractMgmtSvcReqRes_v3.verifyValidReplacementResponse_elementFuture a2 = new AsyncCms3_1ContractMgmtSvcReqRes_v3.verifyValidReplacementResponse_elementFuture();
     try{
       a2.getvalue();
       } catch(Exception e) {}
      
       AsyncCms3_1ContractMgmtSvcReqRes_v3.getContractResponse_elementFuture a3 = new AsyncCms3_1ContractMgmtSvcReqRes_v3.getContractResponse_elementFuture();
        try{
       a3.getValue();
       } catch(Exception e) {}
       AsyncCms3_1ContractMgmtSvcReqRes_v3.replaceContractSubmissionResponse_elementFuture a4 = new AsyncCms3_1ContractMgmtSvcReqRes_v3.replaceContractSubmissionResponse_elementFuture();
        try{
       a4.getvalue();
       } catch(Exception e) {}
       AsyncCms3_1ContractMgmtSvcReqRes_v3.cancelContractSubmissionResponse_elementFuture a5 = new AsyncCms3_1ContractMgmtSvcReqRes_v3.cancelContractSubmissionResponse_elementFuture();
        try{
       a5.getvalue();
       } catch(Exception e) {}
       AsyncCms3_1ContractMgmtSvcReqRes_v3.triggerResendContractResponse_elementFuture a6 = new AsyncCms3_1ContractMgmtSvcReqRes_v3.triggerResendContractResponse_elementFuture();
       try{
       a6.getvalue();
       } catch(Exception e) {}
       AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractsByAgentIdResponse_elementFuture a7 = new AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractsByAgentIdResponse_elementFuture();
       try{
       a7.getvalue();
       } catch(Exception e) {}
       AsyncCms3_1ContractMgmtSvcReqRes_v3.getContractDocumentResponse_elementFuture a8 = new AsyncCms3_1ContractMgmtSvcReqRes_v3.getContractDocumentResponse_elementFuture();
      try{
       a8.getvalue();
       } catch(Exception e) {}
       AsyncCms3_1ContractMgmtSvcReqRes_v3.getContractDocumentMetaDataResponse_elementFuture a9 = new AsyncCms3_1ContractMgmtSvcReqRes_v3.getContractDocumentMetaDataResponse_elementFuture();
       try{
       a9.getvalue();
       } catch(Exception e) {}
       AsyncCms3_1ContractMgmtSvcReqRes_v3.createContractSubmissionResponse_elementFuture a32 = new AsyncCms3_1ContractMgmtSvcReqRes_v3.createContractSubmissionResponse_elementFuture();
       try{
       a32.getvalue();
       } catch(Exception e) {}
       AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractAmendmentsByAssociateNumberResponse_elementFuture a33 = new AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractAmendmentsByAssociateNumberResponse_elementFuture();
       //AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractAmendmentsByAssociateNumberResponse_elementFuture a10 = new AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractData_element();
        try{
       a33.getvalue();
       } catch(Exception e) {}
       AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractAmendmentsByAssociateNumResponse_elementFuture a10 = new AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractAmendmentsByAssociateNumResponse_elementFuture();
        try{
       a10.getvalue();
       } catch(Exception e) {}
       AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractsByCustomerIdResponse_elementFuture a11 = new AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractsByCustomerIdResponse_elementFuture();
        try{
       a11.getvalue();
       } catch(Exception e) {}
       AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractDataResponse_elementFuture a12 = new AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractDataResponse_elementFuture();
       try{
       a12.getvalue();
       } catch(Exception e) {}
       AsyncCms3_1ContractMgmtSvcReqRes_v3.updateContractSubmissionResponse_elementFuture a13 = new AsyncCms3_1ContractMgmtSvcReqRes_v3.updateContractSubmissionResponse_elementFuture();
        try{
       a13.getvalue();
       } catch(Exception e) {}
       }
            
      /* AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractsByCustomerId_element a14 = new AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractsByCustomerId_element();
       AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractsByCustomerIdResponse_element a15 = new AsyncCms3_1ContractMgmtSvcReqRes_v3.findContractsByCustomerIdResponse_element();
       AsyncCms3_1ContractMgmtSvcReqRes_v3.getContract_element a16 = new AsyncCms3_1ContractMgmtSvcReqRes_v3.getContract_element();
       AsyncCms3_1ContractMgmtSvcReqRes_v3.getContractDocument_element a17 = new AsyncCms3_1ContractMgmtSvcReqRes_v3.getContractDocument_element();
       AsyncCms3_1ContractMgmtSvcReqRes_v3.getContractDocumentMetaData_element a18 = new AsyncCms3_1ContractMgmtSvcReqRes_v3.getContractDocumentMetaData_element();
       AsyncCms3_1ContractMgmtSvcReqRes_v3.getContractDocumentMetaDataResponse_element a19 = new AsyncCms3_1ContractMgmtSvcReqRes_v3.getContractDocumentMetaDataResponse_element();
       AsyncCms3_1ContractMgmtSvcReqRes_v3.getContractDocumentResponse_element a20 = new AsyncCms3_1ContractMgmtSvcReqRes_v3.getContractDocumentResponse_element();
       AsyncCms3_1ContractMgmtSvcReqRes_v3.getContractResponse_element a21 = new AsyncCms3_1ContractMgmtSvcReqRes_v3.getContractResponse_element();
       AsyncCms3_1ContractMgmtSvcReqRes_v3.replaceContractSubmission_element a22 = new AsyncCms3_1ContractMgmtSvcReqRes_v3.replaceContractSubmission_element();
       AsyncCms3_1ContractMgmtSvcReqRes_v3.replaceContractSubmissionResponse_element a23 = new AsyncCms3_1ContractMgmtSvcReqRes_v3.replaceContractSubmissionResponse_element();
       AsyncCms3_1ContractMgmtSvcReqRes_v3.TerminationChargesInformation a24 = new AsyncCms3_1ContractMgmtSvcReqRes_v3.TerminationChargesInformation();
       AsyncCms3_1ContractMgmtSvcReqRes_v3.TerminationChargesParameter a25 = new AsyncCms3_1ContractMgmtSvcReqRes_v3.TerminationChargesParameter();
       AsyncCms3_1ContractMgmtSvcReqRes_v3.triggerResendContract_element a26 = new AsyncCms3_1ContractMgmtSvcReqRes_v3.triggerResendContract_element();
       AsyncCms3_1ContractMgmtSvcReqRes_v3.triggerResendContractResponse_element a27 = new AsyncCms3_1ContractMgmtSvcReqRes_v3.triggerResendContractResponse_element();
       AsyncCms3_1ContractMgmtSvcReqRes_v3.updateContractSubmission_element a28 = new AsyncCms3_1ContractMgmtSvcReqRes_v3.updateContractSubmission_element();
       AsyncCms3_1ContractMgmtSvcReqRes_v3.updateContractSubmissionResponse_element a29 = new AsyncCms3_1ContractMgmtSvcReqRes_v3.updateContractSubmissionResponse_element();
       AsyncCms3_1ContractMgmtSvcReqRes_v3.verifyValidReplacement_element a30  = new AsyncCms3_1ContractMgmtSvcReqRes_v3.verifyValidReplacement_element();
       AsyncCms3_1ContractMgmtSvcReqRes_v3.verifyValidReplacementResponse_element a31 = new AsyncCms3_1ContractMgmtSvcReqRes_v3.verifyValidReplacementResponse_element();
            
          */  }