public without sharing class QuoteEmailController {
    public List<ContactSelector> contacts {get; private set;}
    
    public Boolean sent {get; private set;}
    public Boolean copyMe {get; set;}
    
    private String returnURL;
    private String quoteIds;
    private String templateId;
    private String groupId;
    
    public QuoteEmailController() {
        try {
        System.assert(Quote_Portal__c.getInstance() != null, 'Quote Portal settings are missing');
        
        returnURL = ApexPages.currentPage().getParameters().get('retURL');
        groupId = ApexPages.currentPage().getParameters().get('gid');
        quoteIds = ApexPages.currentPage().getParameters().get('qids');
        templateId = ApexPages.currentPage().getParameters().get('tid');
        if (templateId == null) {
            templateId = Quote_Portal__c.getInstance().Quote_Template_Id__c;
        }
        Id accountId = ApexPages.currentPage().getParameters().get('aid');
        copyMe = false;
        
        this.contacts = new List<ContactSelector>();
        for (Contact contact :
            [SELECT Id, FirstName, LastName, Role_c__c, Title, Phone, Email FROM Contact
             WHERE Web_Account__c = :accountId]) {
            contacts.add(new ContactSelector(new ContactVO(contact)));
        }
        return; } catch (Exception e) { QuoteUtilities.log(e); }
    }
    
    public PageReference onSend() {
        try {
        
        Id firstContactId = null;
        Set<Id> recipientIds = new Set<Id>();
        for (ContactSelector selector : contacts) {
            if (selector.selected) {
                if (firstContactId == null) {
                    firstContactId = selector.contact.Id;
                }
                recipientIds.add(selector.contact.id);
            }
        }
        
        SBQQ__QuoteTemplate__c template = [SELECT SBQQ__LogoDocumentId__c FROM SBQQ__QuoteTemplate__c WHERE Id = :templateId];
        QuoteDocumentGenerator qdg = new QuoteDocumentGenerator(template);
        Blob pdf = Test.isRunningTest() ? Blob.valueOf('') : qdg.generate(quoteIds);
        QuoteDocumentEmailer emailer = new QuoteDocumentEmailer(pdf);
        emailer.quoteId = quoteIds.split(',')[0];
        emailer.ccCurrentUser = copyMe;
        emailer.email('BA Quote', recipientIds);
        
        List<SBQQ__Quote__c> batch = new List<SBQQ__Quote__c>();
        for (String id : quoteIds.split(',')) {
            batch.add(new SBQQ__Quote__c(Id=id,SBQQ__Status__c=Quote_Portal__c.getInstance().Status_On_Email__c));
        }
        update batch;
        
        if ((groupId != null) && (firstContactId != null)) {
            Quote_Group__c grp = new Quote_Group__c(Id=groupId,Customer_Contact__c=firstContactId);
            update grp;
        }
        
        sent = true; } catch (Exception e) { QuoteUtilities.log(e); } return null;
    }
    
    public PageReference onContinue() {
        try {
        PageReference pref = new PageReference(returnURL);
        pref.setRedirect(true);
        return pref; } catch (Exception e) { QuoteUtilities.log(e); } return null;
    }
    
    public PageReference onCancel() {
        try {
        PageReference pref = new PageReference(returnURL);
        pref.setRedirect(true);
        return pref; } catch (Exception e) { QuoteUtilities.log(e); } return null;
    }
    
    public class ContactSelector {
        public ContactVO contact {get; private set;}
        public Boolean selected {get; set;}
        
        public ContactSelector(ContactVO contact) {
            this.contact = contact;
            selected = false;
        }
    }
    
}