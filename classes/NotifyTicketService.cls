//Generated by wsdl2apex

public class NotifyTicketService {
    public class NotifyTicketPort {
        public String endpoint_x = 'http://localhost:7001/web/services';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        public ebondingTelusCom.eBondingHeader eBondingHeader;
        private String eBondingHeader_hns = 'eBondingHeader=http://ebonding.telus.com';
        private String[] ns_map_type_info = new String[]{'http://ebonding.telus.com', 'ebondingTelusCom', 'http://assurance.ebonding.telus.com/notifyticketservice', 'NotifyTicketService'};
        public void acknowledgeTicketReq(ebondingTelusCom.Sender sender,DateTime creationDateTime,String signature,String messageId,ebondingTelusCom.Credentials credentials,String callbackURL,ebondingTelusCom.businessServiceRequest_element businessServiceRequest,String revision,String environment,String language,ebondingTelusCom.UserArea userArea) {
            ebondingTelusCom.eBondingHeader request_x = new ebondingTelusCom.eBondingHeader();
            request_x.sender = sender;
            request_x.creationDateTime = creationDateTime;
            request_x.signature = signature;
            request_x.messageId = messageId;
            request_x.credentials = credentials;
            request_x.callbackURL = callbackURL;
            request_x.businessServiceRequest = businessServiceRequest;
            request_x.revision = revision;
            request_x.environment = environment;
            request_x.language = language;
            request_x.userArea = userArea;
            ebondingTelusCom.OK response_x;
            Map<String, ebondingTelusCom.OK> response_map_x = new Map<String, ebondingTelusCom.OK>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://ebonding.telus.com',
              'eBondingHeader',
              'http://ebonding.telus.com',
              'OK',
              'ebondingTelusCom.OK'}
            );
            response_x = response_map_x.get('response_x');
        }
        public void notifyTicketActivityAdded(ebondingTelusCom.Sender sender,DateTime creationDateTime,String signature,String messageId,ebondingTelusCom.Credentials credentials,String callbackURL,ebondingTelusCom.businessServiceRequest_element businessServiceRequest,String revision,String environment,String language,ebondingTelusCom.UserArea userArea) {
            ebondingTelusCom.eBondingHeader request_x = new ebondingTelusCom.eBondingHeader();
            request_x.sender = sender;
            request_x.creationDateTime = creationDateTime;
            request_x.signature = signature;
            request_x.messageId = messageId;
            request_x.credentials = credentials;
            request_x.callbackURL = callbackURL;
            request_x.businessServiceRequest = businessServiceRequest;
            request_x.revision = revision;
            request_x.environment = environment;
            request_x.language = language;
            request_x.userArea = userArea;
            ebondingTelusCom.OK response_x;
            Map<String, ebondingTelusCom.OK> response_map_x = new Map<String, ebondingTelusCom.OK>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://ebonding.telus.com',
              'eBondingHeader',
              'http://ebonding.telus.com',
              'OK',
              'ebondingTelusCom.OK'}
            );
            response_x = response_map_x.get('response_x');
        }
        public void notifyTicketAdded(ebondingTelusCom.Sender sender,DateTime creationDateTime,String signature,String messageId,ebondingTelusCom.Credentials credentials,String callbackURL,ebondingTelusCom.businessServiceRequest_element businessServiceRequest,String revision,String environment,String language,ebondingTelusCom.UserArea userArea) {
            ebondingTelusCom.eBondingHeader request_x = new ebondingTelusCom.eBondingHeader();
            request_x.sender = sender;
            request_x.creationDateTime = creationDateTime;
            request_x.signature = signature;
            request_x.messageId = messageId;
            request_x.credentials = credentials;
            request_x.callbackURL = callbackURL;
            request_x.businessServiceRequest = businessServiceRequest;
            request_x.revision = revision;
            request_x.environment = environment;
            request_x.language = language;
            request_x.userArea = userArea;
            ebondingTelusCom.OK response_x;
            Map<String, ebondingTelusCom.OK> response_map_x = new Map<String, ebondingTelusCom.OK>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://ebonding.telus.com',
              'eBondingHeader',
              'http://ebonding.telus.com',
              'OK',
              'ebondingTelusCom.OK'}
            );
            response_x = response_map_x.get('response_x');
        }
        public void notifyTicketUpdated(ebondingTelusCom.Sender sender,DateTime creationDateTime,String signature,String messageId,ebondingTelusCom.Credentials credentials,String callbackURL,ebondingTelusCom.businessServiceRequest_element businessServiceRequest,String revision,String environment,String language,ebondingTelusCom.UserArea userArea) {
            ebondingTelusCom.eBondingHeader request_x = new ebondingTelusCom.eBondingHeader();
            request_x.sender = sender;
            request_x.creationDateTime = creationDateTime;
            request_x.signature = signature;
            request_x.messageId = messageId;
            request_x.credentials = credentials;
            request_x.callbackURL = callbackURL;
            request_x.businessServiceRequest = businessServiceRequest;
            request_x.revision = revision;
            request_x.environment = environment;
            request_x.language = language;
            request_x.userArea = userArea;
            ebondingTelusCom.OK response_x;
            Map<String, ebondingTelusCom.OK> response_map_x = new Map<String, ebondingTelusCom.OK>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://ebonding.telus.com',
              'eBondingHeader',
              'http://ebonding.telus.com',
              'OK',
              'ebondingTelusCom.OK'}
            );
            response_x = response_map_x.get('response_x');
        }
    }
}