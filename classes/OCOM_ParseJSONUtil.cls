/*
########################################################################################
# Created by............: Vlocity
# Created Date..........: 2-Feb-2016
# Last Modified by......: Vlocity
# Last Modified Date....: 
# Description...........: Parse JSON Util for Generating Quote PDF
########################################################################################
*/

public with sharing class OCOM_ParseJSONUtil {
	
	
	public static List<Teluschar> getListOfTeluschar(String JSONString){

	    JSONParser parser = JSON.createParser(JSONString);
	     List<Teluschar>  lisTel = new  List<Teluschar> ();
	    while (parser.nextToken() != null) {
	             if (parser.getCurrentToken() == JSONToken.START_ARRAY) {
	            while (parser.nextToken() != null) {
	                if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
	                	
	                    Teluschar tel = (Teluschar)parser.readValueAs(Teluschar.class);
	                    system.debug('#### Name: ' + tel.Name);
	                    system.debug('#### SegmentValue: ' + tel.SegmentValue);
	                    lisTel.add(tel);
	                    //parser.skipChildren();
	                }
	            }
	        }
	   
	    }

		return lisTel; 
	}
	// Key = attributeDisplayName
	// Value = value	
	public static Map<String, Teluschar> getMapOfTeluschar(String JSONString){
		Map<String, Teluschar> m = new Map<String, Teluschar> ();
	    for (Teluschar t: getListOfTeluschar(JSONString)){
	    	if(String.isNotEmpty(t.Name)){
	    		m.put(t.Name, t);
	    	}
	    }
		return m; 
	}


	
	public class Teluschar {
		Teluschar(String SegmentValue, String name){
			this.SegmentValue = SegmentValue;
			this.Name = name;
		}
		public String SegmentValue{get;set;}
		public String Name{get;set;}
		
		public attributeRunTimeInfo attributeRunTimeInfo{get;set;}		
	}
	
	public class attributeRunTimeInfo{
		public selectedItem selectedItem{get;set;}
	}	

	public class selectedItem{
		String displayText{get;set;}
	}
}