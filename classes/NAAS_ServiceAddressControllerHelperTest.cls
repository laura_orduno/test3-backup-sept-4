@isTest(seeAllData= false)
public class NAAS_ServiceAddressControllerHelperTest {
    
    public testmethod static void testUpdateAddressAndOrder(){
        List<Account> accList = TestDataCreationFactory.createRCIDAccounts(1);
        accList[0].RecordTypeId = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Service').getRecordTypeId();
        update accList[0];
        system.debug('accList[0]======>' + accList[0]);
        system.debug('accList[0].RecordTypeId======>' + accList[0].RecordTypeId);
        SMBCare_Address__c smbObj = TestDataCreationFactory.createSMBCareAddressOnAccount(accList[0]);
       // smbObj.Service_Account_Id__r.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
       // smbObj.Service_Account_Id__c
       
        
        system.debug('smbObj======>' + smbObj);
        system.debug('smbObj.Service_Account_Id__r.RecordTypeId======>' + smbObj.Service_Account_Id__r.RecordTypeId);
        Order orderObj  = TestDataCreationFactory.createOrder(accList[0]);
        ServiceAddressData saData = new ServiceAddressData();
        saData.address = 'testAddress' ; saData.city = 'Toronto' ; saData.province = 'Ontario'; 
        saData.postalCode = 'a1a 1a1' ; saData.country = 'Canada' ; saData.searchString = 'test'; saData.fmsId = '1233211';
        saData.isManualCapture  =  true ;
       
        Test.startTest();
        system.debug('smbObj.Service_Account_Id__c --->'+ smbObj.Service_Account_Id__c);
       //system.debug('smbObj.Service_Account_Id__r.RecordTypeId.equals(Service)  --->'+ smbObj.Service_Account_Id__r.RecordTypeId.equals(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId()));
        //system.debug('smbObj.Account__c == OrdToUpdate.AccountId  ------> ' + smbObj.Account__c == orderObj.AccountId);
        NAAS_ServiceAddressControllerHelper helperObj = new NAAS_ServiceAddressControllerHelper();
        helperObj.CreateServiceAcc(orderObj,saData);
        helperObj.CreateServiceAccount(orderObj, smbObj, saData);
        helperObj.UpdateAddressAndOrder(orderObj.Id, smbObj, saData);
        helperObj.UpdateServiceAccountAssociation(orderObj.Id, smbObj.Service_Account_Id__c);
        
        Test.stopTest();
    }
    public testmethod static void testUpdateAddressAndOrder3(){
        List<Account> accList = TestDataCreationFactory.createRCIDAccounts(1);
        accList[0].RecordTypeId = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Service').getRecordTypeId();
        update accList[0];
        system.debug('accList[0]======>' + accList[0]);
        system.debug('accList[0].RecordTypeId======>' + accList[0].RecordTypeId);
        Order orderObj  = TestDataCreationFactory.createOrder(accList[0]);
        SMBCare_Address__c smbObj = TestDataCreationFactory.createSMBCareAddressOnAccount(accList[0]);
        smbObj.Service_Account_Id__c = null ;
        smbObj.Account__c = orderObj.AccountId;
        smbObj.FMS_Address_ID__c = '1233211';
        Upsert smbObj;
        ServiceAddressData saData = new ServiceAddressData();
        saData.address = 'testAddress' ; saData.city = 'Toronto' ; saData.province = 'Ontario'; 
        saData.postalCode = 'a1a 1a1' ; saData.country = 'Canada' ; saData.searchString = 'test'; saData.fmsId = '1233211';
        saData.isManualCapture  =  true ;
       
        Test.startTest();
        system.debug('smbObj.Service_Account_Id__c --->'+ smbObj.Service_Account_Id__c);
        NAAS_ServiceAddressControllerHelper helperObj = new NAAS_ServiceAddressControllerHelper();
        helperObj.UpdateAddressAndOrder(orderObj.Id,smbObj,saData);
        
        
        Test.stopTest();
    }
    public testmethod static void testUpdateAddressAndOrder4(){
        List<Account> accList = TestDataCreationFactory.createRCIDAccounts(1);
        accList[0].RecordTypeId = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Service').getRecordTypeId();
        update accList[0];
        system.debug('accList[0]======>' + accList[0]);
        system.debug('accList[0].RecordTypeId======>' + accList[0].RecordTypeId);
        Order orderObj  = TestDataCreationFactory.createOrder(accList[0]);
        SMBCare_Address__c smbObj = TestDataCreationFactory.createSMBCareAddressOnAccount(accList[0]);
        smbObj.Service_Account_Id__c = null ;
        smbObj.Account__c = accList[0].Id;
        smbObj.FMS_Address_ID__c = '1211';
        ServiceAddressData saData = new ServiceAddressData();
        saData.address = 'testAddress' ; saData.city = 'Toronto' ; saData.province = 'Ontario'; 
        saData.postalCode = 'a1a 1a1' ; saData.country = 'Canada' ; saData.searchString = 'test'; saData.fmsId = '1233211';
        saData.isManualCapture  =  true ;
       
        Test.startTest();
        system.debug('smbObj.Service_Account_Id__c --->'+ smbObj.Service_Account_Id__c);
        NAAS_ServiceAddressControllerHelper helperObj = new NAAS_ServiceAddressControllerHelper();
        helperObj.UpdateAddressAndOrder(orderObj.Id,smbObj,saData);
        
        
        Test.stopTest();
    }
    
     public testmethod static void testUpdateAddressAndOrder5(){
        List<Account> accList = TestDataCreationFactory.createRCIDAccounts(1);
        accList[0].RecordTypeId = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Service').getRecordTypeId();
        update accList[0];
        system.debug('accList[0]======>' + accList[0]);
        system.debug('accList[0].RecordTypeId======>' + accList[0].RecordTypeId);
        Order orderObj  = TestDataCreationFactory.createOrder(accList[0]);
        SMBCare_Address__c smbObj = TestDataCreationFactory.createSMBCareAddressOnAccount(accList[0]);
        smbObj.Service_Account_Id__c = null ;
        smbObj.Account__c = accList[0].Id;
        smbObj.FMS_Address_ID__c = '1211';
        Upsert smbObj;
        orderObj.Service_Address__c = smbObj.Id;
        ServiceAddressData saData = new ServiceAddressData();
        saData.address = 'testAddress' ; saData.city = 'Toronto' ; saData.province = 'Ontario'; 
        saData.postalCode = 'a1a 1a1' ; saData.country = 'Canada' ; saData.searchString = 'test'; saData.fmsId = '1233211';
        saData.isManualCapture  =  false;
       
        Test.startTest();
        system.debug('smbObj.Service_Account_Id__c --->'+ smbObj.Service_Account_Id__c);
        NAAS_ServiceAddressControllerHelper helperObj = new NAAS_ServiceAddressControllerHelper();
        helperObj.CreateServiceAccount(orderObj,smbObj,saData);
        
        
        Test.stopTest();
    }
    public testMethod static void testUpdateAddressAndQuote(){
        List<Account> accList = TestDataCreationFactory.createAccounts(1, 'Service');
        PriceBook2 pBook = TestDataCreationFactory.createPriceBook();
        List<Opportunity> opptyList = TestDataCreationFactory.createOpprtunityOnAccount(1, accList[0].Id);
        Quote quoteObj = TestDataCreationFactory.createQuote(opptyList[0].Id, pBook.Id );
     
        OCOM_Quote_Settings__c qsObj =  new OCOM_Quote_Settings__c(Days_to_expiry__c =  10);
        insert qsObj ;
        update quoteObj ;
        SMBCare_Address__c smbObj = TestDataCreationFactory.createSMBCareAddressOnAccount(accList[0]);
        ServiceAddressData saData = new ServiceAddressData();
        saData.address = 'testAddress' ; saData.city = 'Toronto' ; saData.province = 'Ontario'; 
        saData.postalCode = 'a1a 1a1' ; saData.country = 'Canada' ; saData.searchString = 'test'; saData.fmsId = '1233211';
        saData.isManualCapture  =  false ;
        
        Test.startTest();
        NAAS_ServiceAddressControllerHelper helperObj = new NAAS_ServiceAddressControllerHelper();        
        helperObj.UpdateAddressAndQuote(quoteObj.Id, smbObj, saData);
        Test.stopTest();
                               
    }
    
    public testMethod static void testUpdateServiceAddress(){
        List<Account> accList = TestDataCreationFactory.createAccounts(1, 'Service');
        SMBCare_Address__c smbObj = TestDataCreationFactory.createSMBCareAddressOnAccount(accList[0]);
        //smbObj.Service_Account_Id__r.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        system.debug('smbObj======>' + smbObj);
        system.debug('smbObj.Service_Account_Id__r.RecordTypeId======>' + smbObj.Service_Account_Id__r.RecordTypeId);
        
        ServiceAddressData saData = new ServiceAddressData();
        saData.address = 'testAddress' ; saData.city = 'Toronto' ; saData.province = 'Ontario'; 
        saData.postalCode = 'a1a 1a1' ; saData.country = 'Canada' ; saData.searchString = 'test'; saData.fmsId = '1233211';
        saData.isManualCapture  =  true ; saData.buildingNumber =  '1'; saData.streetTypeSuffix = 'StreetSuffix' ; 
        saData.municipalityNameLPDS = 'testMunicipality' ; saData.dropFacilityCode = 'testCode' ; saData.dropTypeCode = 'testCodeType';
        saData.dropLength = 'testDropLength'; saData.dropExceptionCode = 'exceptionCode'; saData.currentTransportTypeCode = 'testTransportType';
        saData.futureTransportTypeCode = 'testFutureTransport'; saData.futurePlantReadyDate = 'testPlantReady'; 
        saData.futureTransportRemarkTypeCode = 'testTransportCode'; saData.gponBuildTypeCode = 'testgponBuildType'; 
        saData.provisioningSystemCode = 'testProvisioningCode';
        saData.street = 'testStreet';saData.streetTypeCode='testCode';
        saData.streetName = 'testStreetName';saData.streetName='testName';
        saData.streetName_fms = 'testStreetName_fms';
      
        Test.startTest();
        NAAS_ServiceAddressControllerHelper helperObj = new NAAS_ServiceAddressControllerHelper();
        helperObj.createServiceAddress(smbObj, saData, accList[0].Id, accList[0].Id ) ;
        helperObj.updateServiceAddress( smbObj, saData, (String)smbObj.id);
        Test.stopTest();
    }
    public testmethod static void testUpdateAddressAndOrder7(){
        List<Account> accList = TestDataCreationFactory.createRCIDAccounts(2);
        accList[0].RecordTypeId = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Service').getRecordTypeId();
        update accList[0];
         accList[1].RecordTypeId = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Service').getRecordTypeId();
        update accList[1];
         
              SMBCare_Address__c smbObj = TestDataCreationFactory.createSMBCareAddressOnAccount(accList[0]);
        Order orderObj  = TestDataCreationFactory.createOrder(accList[0]);
        ServiceAddressData saData = new ServiceAddressData();
        saData.address = 'testAddress' ; saData.city = 'Toronto' ; saData.province = 'Ontario'; 
        saData.postalCode = 'a1a 1a1' ; saData.country = 'Canada' ; saData.searchString = 'test'; saData.fmsId = '1233211';
        saData.isManualCapture  = false ;
        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'All Other Orders';
        insert testOpp_1;
        Quote quo = new Quote(Name = 'Test qoute', ServiceAccountId__c = accList[0].Id,OpportunityId =testOpp_1.Id);
        Insert quo;
        orderObj.vlocity_cmt__QuoteId__c = quo.Id;
        update orderObj;
        work_order__c wO = new work_order__c(status__c= Label.OCOM_WFMReserved,WFM_number__c='0001212',order__c=orderObj.Id); 
        Insert wO;
        Test.startTest();
        NAAS_ServiceAddressControllerHelper helperObj = new NAAS_ServiceAddressControllerHelper();
        helperObj.UpdateServiceAccountAssociation(orderObj.Id, accList[1].Id);
        Test.stopTest();
    }
    
     public testmethod static void testUpdateAddressAndOrder2(){
        List<Account> accList = TestDataCreationFactory.createRCIDAccounts(2);
        accList[0].RecordTypeId = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Service').getRecordTypeId();
        update accList[0];
         accList[1].RecordTypeId = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Service').getRecordTypeId();
        update accList[1];
        system.debug('accList[0]======>' + accList[0]);
        system.debug('accList[0].RecordTypeId======>' + accList[0].RecordTypeId);
      
              SMBCare_Address__c smbObj = TestDataCreationFactory.createSMBCareAddressOnAccount(accList[0]);
                 
        
        system.debug('smbObj======>' + smbObj);
        system.debug('smbObj.Service_Account_Id__r.RecordTypeId======>' + smbObj.Service_Account_Id__r.RecordTypeId);
        Order orderObj  = TestDataCreationFactory.createOrder(accList[0]);
         
        ServiceAddressData saData = new ServiceAddressData();
        saData.address = 'testAddress' ; saData.city = 'Toronto' ; saData.province = 'Ontario'; 
        saData.postalCode = 'a1a 1a1' ; saData.country = 'Canada' ; saData.searchString = 'test'; saData.fmsId = '1233211';
        saData.isManualCapture  = false ;
       
        Test.startTest();
        system.debug('smbObj.Service_Account_Id__c --->'+ smbObj.Service_Account_Id__c);
       //system.debug('smbObj.Service_Account_Id__r.RecordTypeId.equals(Service)  --->'+ smbObj.Service_Account_Id__r.RecordTypeId.equals(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId()));
        //system.debug('smbObj.Account__c == OrdToUpdate.AccountId  ------> ' + smbObj.Account__c == orderObj.AccountId);
        NAAS_ServiceAddressControllerHelper helperObj = new NAAS_ServiceAddressControllerHelper();
        helperObj.CreateServiceAcc(orderObj,saData);
        helperObj.CreateServiceAccount(orderObj, smbObj, saData);
        helperObj.UpdateAddressAndOrder(orderObj.Id, smbObj, saData);
        helperObj.UpdateServiceAccountAssociation(orderObj.Id, smbObj.Service_Account_Id__c);
        
        Test.stopTest();
    }
    public testmethod static void testcreateAddressAndOrder(){
        List<Account> accList = TestDataCreationFactory.createRCIDAccounts(2);
        accList[0].RecordTypeId = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Service').getRecordTypeId();
        update accList[0];
         accList[1].RecordTypeId = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Service').getRecordTypeId();
        update accList[1];
        system.debug('accList[0]======>' + accList[0]);
        system.debug('accList[0].RecordTypeId======>' + accList[0].RecordTypeId);
      
              SMBCare_Address__c smbObj = TestDataCreationFactory.createSMBCareAddressOnAccount(accList[0]);
                 
        
        system.debug('smbObj======>' + smbObj);
        system.debug('smbObj.Service_Account_Id__r.RecordTypeId======>' + smbObj.Service_Account_Id__r.RecordTypeId);
        Order orderObj  = TestDataCreationFactory.createOrder(accList[0]);
         
        ServiceAddressData saData = new ServiceAddressData();
        saData.address = 'testAddress' ; saData.city = 'Toronto' ; saData.province = 'Ontario'; 
        saData.postalCode = 'a1a 1a1' ; saData.country = 'Canada' ; saData.searchString = 'test'; saData.fmsId = '1233211';
        saData.isManualCapture  = true;
       
        Test.startTest();
        system.debug('smbObj.Service_Account_Id__c --->'+ smbObj.Service_Account_Id__c);
       //system.debug('smbObj.Service_Account_Id__r.RecordTypeId.equals(Service)  --->'+ smbObj.Service_Account_Id__r.RecordTypeId.equals(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId()));
        //system.debug('smbObj.Account__c == OrdToUpdate.AccountId  ------> ' + smbObj.Account__c == orderObj.AccountId);
        NAAS_ServiceAddressControllerHelper helperObj = new NAAS_ServiceAddressControllerHelper();
        helperObj.CreateServiceAcc(orderObj,saData);
        helperObj.CreateServiceAccount(orderObj, smbObj, saData);
        helperObj.UpdateAddressAndOrder(orderObj.Id, smbObj, saData);
        helperObj.UpdateServiceAccountAssociation(orderObj.Id, smbObj.Service_Account_Id__c);
        
        Test.stopTest();
    }
    public testmethod static void test2createAddressAndOrder(){
        List<Account> accList = TestDataCreationFactory.createRCIDAccounts(2);
        accList[0].RecordTypeId = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('RCID').getRecordTypeId();
        update accList[0];
         accList[1].RecordTypeId = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('RCID').getRecordTypeId();
        update accList[1];
        system.debug('accList[0]======>' + accList[0]);
        system.debug('accList[0].RecordTypeId======>' + accList[0].RecordTypeId);
      
              SMBCare_Address__c smbObj = TestDataCreationFactory.createSMBCareAddressOnAccount(accList[0]);
              
        
        system.debug('smbObj======>' + smbObj);
        system.debug('smbObj.Service_Account_Id__r.RecordTypeId======>' + smbObj.Service_Account_Id__r.RecordTypeId);
        Order orderObj  = TestDataCreationFactory.createOrder(accList[0]);
         
        ServiceAddressData saData = new ServiceAddressData();
        saData.address = 'testAddress' ; saData.city = 'Toronto' ; saData.province = 'Ontario'; 
        saData.postalCode = 'a1a 1a1' ; saData.country = 'Canada' ; saData.searchString = 'test'; saData.fmsId = '1233211';
        saData.isManualCapture  = true; saData.streetTypePrefix = '8452';
       
        Test.startTest();
        system.debug('smbObj.Service_Account_Id__c --->'+ smbObj.Service_Account_Id__c);
        NAAS_ServiceAddressControllerHelper helperObj = new NAAS_ServiceAddressControllerHelper();
        helperObj.CreateServiceAcc(orderObj,saData);
        helperObj.CreateServiceAccount(orderObj, smbObj, saData);
        
        
        Test.stopTest();
    }
    
}