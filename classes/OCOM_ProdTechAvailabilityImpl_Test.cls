/**
 * This class contains unit tests for validating the behavior of Apex classes (OCOM_ProductTechnicalAvailabilityImpl)
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 * Name - ExternalAuditLogHelper_Test
 * Created Date - 03 April 2017 - Sandip
 * 1. Modified By 
 */
@isTest(SeeAllData=false)
public class OCOM_ProdTechAvailabilityImpl_Test{
    private static testMethod void testProductTechnicalAvailabilityImpl1(){
        List<SMBCare_Address__c> addresses = OrdrTestDataFactory.createAddresses();
        Id serviceAddressId = addresses[0].Id;
        OrdrTestDataFactory.createOrderingWSCustomSettings('TechnicalAvailabilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/RMO/ResourceMgmt/CheckGenericTechnicalAvailabilitySvc_v2_2_vs1');
                OrdrTestDataFactory.createOrderingWSCustomSettings('ProductEligibilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/CMO/OrderMgmt/PerformCustomerOrderFeasibility_v2_0_vs0');    
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        
        PageReference pageref = Page.OCOM_InOrderFlowPAC;
        Test.setCurrentPage(pageref);        
        pageref.getparameters().put('smbCareAddrId', serviceAddressId);
        
        OCOM_OutOrderFlowPACController mainOutcontroller = new OCOM_OutOrderFlowPACController();
        OCOM_ProductTechnicalAvailabilityImpl extController1 = new OCOM_ProductTechnicalAvailabilityImpl(mainOutcontroller);
        OCOM_InOrderFlowPACController mainInController = new OCOM_InOrderFlowPACController();
        mainInController.isASFDown = true;
        OCOM_ProductTechnicalAvailabilityImpl extController = new OCOM_ProductTechnicalAvailabilityImpl(mainInController);
        extController.refreshAvailableProduct();
        List<OCOM_ProductTechnicalAvailabilityImpl.Characteristic> tstObj = new List<OCOM_ProductTechnicalAvailabilityImpl.Characteristic>();
        //extController.addUnconfirmedCharacterstics(tstObj);
        extController.getProductsTechnicalAvailability();
        mainInController.isASFDown = false;
        extController.getProductsTechnicalAvailability();
        Test.stopTest();
    }
    
    private static testMethod void testProductTechnicalAvailabilityImpl2(){
        Test.startTest();
        OCOM_InOrderFlowPACController mainController = new OCOM_InOrderFlowPACController();
        OCOM_ProductTechnicalAvailabilityImpl extController = new OCOM_ProductTechnicalAvailabilityImpl(mainController);
        extController.getRateGroupAndForbearanceInfo();
        Test.stopTest();
    }
    
    private static testMethod void testProductTechnicalAvailabilityImpl3(){
        List<SMBCare_Address__c> addresses = OrdrTestDataFactory.createAddresses();
        Id serviceAddressId = addresses[0].Id;
        OrdrTestDataFactory.createOrderingWSCustomSettings('ClearanceCheckEndpoint', 'https://xmlgwy-pt1.telus.com:9030/pt01/RMO/InventoryMgmt/FindResource_v2_0_vs1_PT');
		OrdrTestDataFactory.createOrderingWSCustomSettings('QueryResourceWirelineEndpoint', 'https://webservices.preprd.teluslabs.net:443/RMO/InventoryMgmt/queryResource');
        
        Test.startTest();
        PageReference pageref = Page.OCOM_InOrderFlowPAC;
        Test.setCurrentPage(pageref);        
        pageref.getparameters().put('smbCareAddrId', serviceAddressId);
        
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
       
        OCOM_InOrderFlowPACController mainController = new OCOM_InOrderFlowPACController();
        OCOM_ProductTechnicalAvailabilityImpl extController = new OCOM_ProductTechnicalAvailabilityImpl(mainController);
        extController.getServicePath();
        OC_SelectAndNewAddressController tstObj = new OC_SelectAndNewAddressController();
        OCOM_ProductTechnicalAvailabilityImpl tstObj1 = new OCOM_ProductTechnicalAvailabilityImpl(tstObj);
        apexpages.currentpage().getparameters().put('serviceAddressId',serviceAddressId);
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
     	tstObj1.getGponDropStatus();
        Test.stopTest();
    }
}