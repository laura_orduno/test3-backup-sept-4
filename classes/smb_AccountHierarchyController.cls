global with sharing class smb_AccountHierarchyController {

    public static final Id ACCT_RT_RCID = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' AND Name = 'RCID'].Id;
    public static final Id ACCT_RT_CBUCID = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' AND Name = 'CBUCID'].Id;
    public static final Id ACCT_RT_BAN = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' AND Name = 'BAN'].Id;
    public static final Id ACCT_RT_CAN = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' AND Name = 'CAN'].Id;
    public static final Integer ACCOUNT_LIMIT = (!Test.isRunningTest() ? 2000 : 5);
    public static final Integer LEVEL_LIMIT = (!Test.isRunningTest() ? 200 : 5);
    public static final Boolean SHOW_WTN = true;
    
    public boolean raiseEvents {get;set;}
    
    public transient Id accountId {get; set;}
    public transient Id ultimateParentId {get;set;}
    
    public transient Account SelectedAccount {get;set;}
    
    private transient AccountNode rootAccount;
    private transient Map<string, AccountNode> accountNodes;
    
    private final string rootNodeHtmlFormat = 
            '<ul id="hierarchy">' +
                '<img src="/s.gif" class="tree-logo cbucidlogo {0}"/>' +
                '&nbsp;' +
                '<span class="cbucid {0}">' +
                    '<a target="_top" href="/{1}" {2}>{3}</a>' +
                    ',&nbsp;{4}:&nbsp;{5}' +
                    ',&nbsp;{6}:&nbsp;{7}' +
                '</span>' + 
                '&nbsp;' +
                '{8}' +
                '{9}' +
            '</ul>';
               
    private final string childNodeHtmlFormat = 
            '<li class="{0}">' +
                '<img src="/s.gif" class="tree-logo"/>' +
                '&nbsp;' +
                '<span>' +
                    '<a target="_top" href="/{1}" {2}>{3}</a>' +
                    ',&nbsp;' +
                    '{4}:&nbsp;{5}' +
                    ',&nbsp;' +
                    '{6}:&nbsp;{7}' +
                '</span>' +
                '&nbsp;' +
                '{8}' +
                '{9}' +
            '</li>';
   
    //only for use in cases where RaiseEvents is false
    private final string currentrootNodeHtmlFormat = 
            '<ul id="hierarchy">' +
                '<img src="/s.gif" class="tree-logo cbucidlogo {0}"/>' +
                '&nbsp;' +
                '<span class="cbucid" style="font-weight:bold">' +
                    '{1}' +
                '</span>' +
                ',&nbsp;{2}:&nbsp;{3}' +
                ',&nbsp;{4}:&nbsp;{5}' + 
                '{6}' +
            '</ul>';
    
    //only for use in cases where RaiseEvents is false     
    private final string currentNodeHtmlFormat = 
            '<li class="{0}">' +
                '<img src="/s.gif" class="tree-logo"/>' +
                '&nbsp;' +
                '<span style="font-weight:bold">' +
                    '{1}' +
				'</span>' +
                ',&nbsp;' +
                '{2}:&nbsp;{3}' +
                ',&nbsp;' +
                '{4}:&nbsp;{5}' +
                '{6}' +
            '</li>';
            
	private final string phoneNumberNodeHtmlFormat = 
			'<li>' +
				'<img src="/s.gif" class="tree-logo"/>' +
                '&nbsp;' +
                '<span>' +
                    'WTN: {0}' +
				'</span>' +
            '</li>';
            
   /* private final string consolidatedNodeHtmlFormat = 
        '<li class="{0}">' +
            '<img src="/s.gif" class="tree-logo"/>'+
            '&nbsp;' + 
            '<span>' +
                '<a href="#" {1}>' + 
                    '{2}' +
                '</a>' +
                ', BCAN: {3}' +
            '</span>' +
            '{4}' +
        '</li>';*/
            
    private final string viewAccountHtmlFormat = 
        '[&nbsp;<a href="/{0}" {1}>View Account</a>&nbsp;]'; 
        
    private final string onAccountClickHtmlFormat = 
        ' onclick="return raiseAccountSelectedEvent(\'\'{0}\'\', \'\'{1}\'\', \'\'{2}\'\', \'\'{3}\'\', \'\'{4}\'\');"';
        
    private final string onViewAccountClickHtmlFormat = 
        ' onclick="return raiseViewAccountSelectedEvent(\'\'{0}\'\', \'\'{1}\'\', \'\'{2}\'\', \'\'{3}\'\', \'\'{4}\'\');"';
    
    public smb_AccountHierarchyController() {

    }
    
    public string selectedAccountNumber {
        get {
            if (SelectedAccount == null) return '';
            
            return smb_AccountUtility.getAccountNumber(SelectedAccount);
        }
    }
    
    
    
    private Account getAccount(Id accountId) {
        List<Account> accounts;
        
        if (SHOW_WTN) {
        	accounts = [SELECT Name, ParentId, RecordTypeId, CBUCID__c, CBUCID_Parent_Id__c, RCID__c, CAN__c, CustProfId__c,
                                            RecordType.Name, RecordType.DeveloperName, CBUCID_RCID__c, Phone, PTN__c, BTN__c,
                                            (SELECT Id, Name, Phone__c
                                            FROM Assets
                                            WHERE Type__c = 'WTN')
                                  FROM Account
                                  WHERE Id = :accountId];
        }
        else {
        	accounts = [SELECT Name, ParentId, RecordTypeId, CBUCID__c, CBUCID_Parent_Id__c, RCID__c, CAN__c, CustProfId__c,
                                            RecordType.Name, RecordType.DeveloperName, CBUCID_RCID__c, Phone, PTN__c, BTN__c
                                  FROM Account
                                  WHERE Id = :accountId];
        }
                                   
        if (accounts.size() > 0) 
            return accounts.get(0);
        
        return null;
    }
    
    private List<Account> getChildAccounts(List<Account> parentAccounts) {
    	if (SHOW_WTN) {
    		return [SELECT Name, ParentId, RecordTypeId, CBUCID__c, CBUCID_Parent_Id__c, RCID__c, CAN__c, CustProfId__c,
                                RecordType.Name, RecordType.DeveloperName, CBUCID_RCID__c, Phone, PTN__c, BTN__c,
                                (SELECT Id, Name, Phone__c
                                FROM Assets
                                WHERE Type__c = 'WTN'
                                LIMIT :LEVEL_LIMIT)
                       FROM Account
                       WHERE ParentId In :parentAccounts
                                AND
                             ParentId != null
                       LIMIT :LEVEL_LIMIT];
    	}
    	else {
    		return [SELECT Name, ParentId, RecordTypeId, CBUCID__c, CBUCID_Parent_Id__c, RCID__c, CAN__c, CustProfId__c,
                                RecordType.Name, RecordType.DeveloperName, CBUCID_RCID__c, Phone, PTN__c, BTN__c
                       FROM Account
                       WHERE ParentId In :parentAccounts
                                AND
                             ParentId != null
                       LIMIT :LEVEL_LIMIT];
    	}
    }
    
    private void fillAccounts() {
        
        accountNodes = new Map<string, accountNode>();
                
        if (accountId == null) return;
        
        this.selectedAccount = getAccount(this.accountId);
        
        if (this.selectedAccount == null) return;
        
        this.ultimateParentId = smb_AccountUtility.getUltimateParentId(accountId);
        
        if (ultimateParentId == null) return;
        
        Account ultimateParentAccount = getAccount(ultimateParentId);
        
        if (ultimateParentAccount == null || ultimateParentAccount.RecordTypeId != ACCT_RT_CBUCID) return;
                                   
        this.rootAccount = stuffAccount(ultimateParentAccount);
                
        integer counter = 0;
        
        List<Account> lastBatch = new List<Account> {ultimateParentAccount};

        while (counter < 4 && lastBatch != null && lastBatch.size() > 0 && AccountNodes.size() < ACCOUNT_LIMIT) {
            counter++;
            
            lastBatch = getChildAccounts(lastBatch);
            
            for (Account account : lastBatch) {
                stuffAccount(account);
            }
        }
        
        stuffAccount(this.selectedAccount);
    }
    
    private AccountNode stuffAccount(Account account) {
        if (account == null) { return null; }
        
        string key = account.Id;
        
        if (accountNodes.containsKey(key)) return accountNodes.get(key);
        
        AccountNode node = new AccountNode(account, account.Id == this.accountId);
        
        accountNodes.put(key, node);
        
        return node;
    }
    
    /*private AccountNode stuffConsolidatedAccount(Account account) {
        
        if (account.PTN__c == null || account.PTN__c == account.CAN__c) return null;
        
        string key = createKeyForAccountNumber(account.PTN__c);
        
        if (accountNodes.containsKey(key)) return accountNodes.get(key);
        
        ConsolidatedAccount consolidatedAccount = new ConsolidatedAccount(account.name, account.PTN__c);
        
        AccountNode consolidatedAccountNode = new AccountNode(consolidatedAccount, false);
        
        setParent(consolidatedAccountNode, account.parentId);
        
        accountNodes.put(key, consolidatedAccountNode);
        
        return consolidatedAccountNode;
    }*/
    
    private void createHierarchy() {
        
        for (AccountNode node : AccountNodes.values()) {
            
            setParent(node, node.account.parentId);
            
            /*AccountNode consolidatedParent = stuffConsolidatedAccount(node.account);
        
            if (consolidatedParent != null) {
                node.parent = consolidatedParent;
            }
            else if (account.ParentId != null) {
                
            }*/
        
        }
    }
    
    private void setParent(AccountNode node, Id parentId) {
        if (parentId == null) return;
        
        string parentKey = parentId;
            
        if (accountNodes.containsKey(parentKey)) {
            node.parent = accountNodes.get(parentKey);
        }
    }
    
    public String getTreeHTML() {
        
        fillAccounts();
        
        createHierarchy();
        
        String tc = '';
        
        if (AccountNodes == null || this.rootAccount == null) 
            { return '<p>This account is not part of an Account Tree.</p>'; }
        
        if (accountNodes.size() >= LEVEL_LIMIT) {
            tc += '<span style="display: inline-block; font-weight: bold; padding: 5px;">This CBUCID account has more than the ' +
                string.valueOf(LEVEL_LIMIT) + ' child records displayed below. To view them all, visit the associated RCID account and look at the Child Account related list.</span>';
        }

        try {
            if (raiseEvents || !this.rootAccount.selected) {
            	tc += renderAccount(this.rootAccount);
        	}
        	else {
        		tc += renderSelectedAccount(this.rootAccount);
        	}
        }
        catch (Exception ex) {
            tc += '<pre>' + ex.getMessage() + '\n' + ex.getStackTraceString() + '</pre>';
        }
        
        return tc;
        
    }
    
    private string renderNodes(AccountNode parentNode, boolean includeUlTags) {
        
        List<AccountNode> nodes = parentNode.children;
        List<Asset> assets = parentNode.account.assets;
        
        if ((nodes == null || nodes.size() == 0)
        	&& (assets == null || assets.size() == 0)) return '';
        
        string html = '';
        
        if (includeUlTags) html += '<ul>';
        
        if (nodes != null) {
	        nodes.sort();
	        
	        for (AccountNode node : nodes) {
	        	if (raiseEvents || !node.selected) {
	            	html += renderAccount(node);
	        	}
	        	else {
	        		html += renderSelectedAccount(node);
	        	}
	        }
        }
        
        if (assets != null && SHOW_WTN) {
        	for (Asset asset : assets) {
        		html += renderWtn(asset);
        	}
        }
        
        if (includeUlTags) html += '</ul>';
        
        return html;
    }
    
    /*private string renderConsolidatedAccount(AccountNode node) {

        string className = '';
        
        if (node.childSelected || node.selected) {
            className = 'open';
        }
        else {
            className = 'closed';
        }
            
        return string.format(
                        consolidatedNodeHtmlFormat,
                        new string[] { 
                            className,
                            renderOnAccountSelected(node.consolidatedAccount),
                            node.consolidatedAccount.accountName,
                            node.accountNumber, 
                            renderNodes(node.children, true)
                        });
    }*/
    
    private string renderAccount(AccountNode node) {
        
        string htmlFormat = this.childNodeHtmlFormat;
        
        string accountType = smb_AccountUtility.getAccountNumberShortLabel(node.account.RecordType.DeveloperName);
        string accountNumber = smb_AccountUtility.getAccountNumber(node.account);
        string secondFieldLabel = 'CUSTPROFID';
        string secondFieldValue = node.account.CustProfID__c;
        
        boolean renderUlTagsForChildren = true;
        
        List<string> classNames = new List<string>();
        
        if (node.childSelected || node.selected) {
            classNames.add('open');
            
            if (node.selected) {
                classNames.add('current');
            }
        }
        else {
            classNames.add('closed');
        }
        
        if (node.account.RecordTypeId == ACCT_RT_CBUCID) {
            htmlFormat = this.rootNodeHtmlFormat;
            renderUlTagsForChildren = false;
        }   
        else if (node.account.RecordTypeId == ACCT_RT_RCID) {
            classNames.add('rcid');
        }
        else if ((node.account.RecordTypeId == ACCT_RT_CAN || node.account.RecordTypeId == ACCT_RT_BAN) && node.children.size() > 0) {
            secondFieldLabel = 'Pilot Name';
            secondFieldValue = node.account.BTN__c;
        }
        else {
            secondFieldLabel = 'BTN';
            secondFieldValue = node.account.BTN__c;            
        }
            
        return string.format(htmlFormat,
                             new string[] {
                                 String.join(classNames, ' '),
                                 node.account.Id,
                                 renderOnAccountSelected(node),
                                 node.account.name,
                                 accountType,
                                 checkForNull(accountNumber, '(n/a)'),
                                 secondFieldLabel,
                                 checkForNull(secondFieldValue, '(n/a)'),
                                 renderViewAccount(node),
                                 renderNodes(node, renderUlTagsForChildren)
                             }
                            );  
    }
    
    private string renderSelectedAccount(AccountNode node) {
    	string htmlFormat = this.currentNodeHtmlFormat;
        
        string accountType = smb_AccountUtility.getAccountNumberShortLabel(node.account.RecordType.DeveloperName);
        string accountNumber = smb_AccountUtility.getAccountNumber(node.account);
        string secondFieldLabel = 'CUSTPROFID';
        string secondFieldValue = node.account.CustProfID__c;
        
        boolean renderUlTagsForChildren = true;
        
        List<string> classNames = new List<string>();
        
        if (node.childSelected || node.selected) {
            classNames.add('open');
            
            if (node.selected) {
                classNames.add('current');
            }
        }
        else {
            classNames.add('closed');
        }
        
        if (node.account.RecordTypeId == ACCT_RT_CBUCID) {
            htmlFormat = this.currentrootNodeHtmlFormat;
            renderUlTagsForChildren = false;
        }   
        else if (node.account.RecordTypeId == ACCT_RT_RCID) {
            classNames.add('rcid');
        }
        else if ((node.account.RecordTypeId == ACCT_RT_CAN || node.account.RecordTypeId == ACCT_RT_BAN) && node.children.size() > 0) {
            secondFieldLabel = 'Pilot Name';
            secondFieldValue = node.account.BTN__c;
        }
        else {
            secondFieldLabel = 'BTN';
            secondFieldValue = node.account.BTN__c;
        }
            
        return string.format(htmlFormat,
                             new string[] {
                                 String.join(classNames, ' '),
                                 node.account.name,
                                 accountType,
                                 checkForNull(accountNumber, '(n/a)'),
                                 secondFieldLabel,
                                 checkForNull(secondFieldValue, '(n/a)'),
                                 renderNodes(node, renderUlTagsForChildren)
                             }
                            );  
    }
    
    private string renderWtn(Asset asset) {
    	if (asset == null || string.isBlank(asset.Phone__c)) {
    		return '';
    	}
    	
		return string.format(phoneNumberNodeHtmlFormat,
							new string[] {
								asset.Phone__c
							});
    }
   
    private string renderViewAccount(AccountNode account) {
        
        if (!raiseEvents) return '';
        
        string viewAccountHtml = '';
        
        if ((string)account.account.Id != (string)accountId) { 
            viewAccountHtml = string.format(this.viewAccountHtmlFormat, 
                                            new string[] {
                                             account.account.Id,
                                             renderOnViewSelected(account)
                                            });
        }
        
        return viewAccountHtml;
    }
    
    private string renderOnAccountSelected(AccountNode account) {
        string accountNumber = smb_AccountUtility.getAccountNumber(account.account);
                
        return String.format(onAccountClickHtmlFormat,
                             new string[] {
                                 account.account.Id,
                                 jsEncode(account.account.Name),
                                 jsEncode(account.account.RecordType.DeveloperName),
                                 jsEncode(accountNumber),
                                 jsEncode(account.account.BTN__c)
                             }
                            );
    }
    
    /*private string renderOnAccountSelected(ConsolidatedAccount account) {
        return String.format(onAccountClickHtmlFormat,
                             new string[] {
                                 '',
                                 jsEncode(account.AccountName),
                                 jsEncode('BCAN'),
                                 jsEncode(account.AccountNumber)
                             }
                            );
    }*/
    
    private string renderOnViewSelected(AccountNode account) {
        string accountNumber = smb_AccountUtility.getAccountNumber(account.account);
                
        return String.format(onViewAccountClickHtmlFormat,
                             new string[] {
                                 account.account.Id,
                                 jsEncode(account.account.Name),
                                 jsEncode(account.account.RecordType.DeveloperName),
                                 jsEncode(accountNumber),
                                 jsEncode(account.account.BTN__c)
                             }
                            );  
    }
    
    private string jsEncode(string text) {
        if (string.isBlank(text)) return '';
        
        return text.escapeEcmaScript();
    }

    
    private static string checkForNull(string text, string defaultText) {
        
        if (String.isBlank(text)) return defaultText;
        
        return text;
        
    }
    
    global class accountNode implements Comparable {
        
        public Account account {get;set;}
        public List<accountNode> children {get;set;}
        public boolean selected {get;set;}
        public string accountNumber {get;set;}
        private integer accountNumberNumeric {get;set;}
        private boolean useNumericAccountNumberComparison {get;set;}
        
        public boolean childSelected {
            get; 
            set {
                this.childSelected = value;
                if (this.parent != null && this.childSelected) {
                    this.parent.childSelected = true;
                }
            }
        }
        
        public boolean selfOrChildSelected {
            get {
                return (this.selected || this.childSelected);
            }
        }
        
        public accountNode parent {
            get;
            set {
                this.parent = value;
                
                if (this.parent == null) return;
                
                this.parent.children.add(this);
                
                if (this.selected || this.childSelected) {
                    this.parent.childSelected = true;
                }
            }
        }
        
        public accountNode(Account account, boolean selected) {
            this.account = account;
            this.selected = selected;
            this.accountNumber = smb_AccountUtility.getAccountNumber(account);
            initialise();
        }
        
        private void initialise() {
            children = new List<accountNode>();
            childSelected = false;
            determineAccountNumberSortable();
        }
        
        private void determineAccountNumberSortable() {
            if (string.isBlank(this.accountNumber)) {
                this.accountNumber = '';
                this.accountNumberNumeric = 0;
                this.useNumericAccountNumberComparison = false;
                return;
            }
            
            try {
                this.accountNumberNumeric = integer.valueOf(accountNumber);
                this.useNumericAccountNumberComparison = true;
            }
            catch (Exception ex) {
                this.accountNumberNumeric = 0;
                this.useNumericAccountNumberComparison = false;
            }
        }
        
        global Integer compareTo(Object compareTo) {
            accountNode node = (accountNode)compareTo;
                        
            if (this.selfOrChildSelected && !node.selfOrChildSelected) return -1;
            
            if (node.selfOrChildSelected && !this.selfOrChildSelected) return 1;
            
            if (this.useNumericAccountNumberComparison && node.useNumericAccountNumberComparison) {
                if (this.accountNumberNumeric == node.accountNumberNumeric) return 0;
                
                if (this.accountNumberNumeric < node.accountNumberNumeric) return -1;
                
                return 1;
            }
            else {
                return this.accountNumber.compareTo(node.accountNumber);
            }
        }   
    }

}