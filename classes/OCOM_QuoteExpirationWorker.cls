global class OCOM_QuoteExpirationWorker implements Database.Batchable<sObject>
{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        system.debug('*#*Starting QuoteExpirationWorker*#*');
        String query = 'select id, ExpirationDate, Status, Activities__c from Quote where ExpirationDate < TODAY and status IN (\'In Progress\', \'Sent\')';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Quote> scope)
    {
        system.debug('*#*In QuoteExpirationWorker.execute()*#*');
        List<Quote> updateList = new List<Quote>();
        
        if (scope != null && scope.size() > 0)
        {
            system.debug('Found '+scope.size() +' Quotes passed their expiration date');
            for(Quote q : scope)
            {
                q.status = 'Expired';
              //   q.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Quote_Expired_record_Type').getRecordTypeId();
                String now = system.today().format();
                q.Activities__c = 'Expired on ' + now +';\r\n Action: Clone quote';
                updateList.add(q);
            }
        }
        
        if(updateList.size() > 0)
        {
            system.debug('Updating Expired Quotes: '+updateList.size());
            update updateList;
        }
        system.debug('*#*Ending QuoteExpirationWorker*#*');
    }
    global void finish(Database.BatchableContext BC){}
}