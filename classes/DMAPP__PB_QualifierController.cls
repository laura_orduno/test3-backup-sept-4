/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PB_QualifierController extends DMAPP.TASBaseController {
    @RemoteAction
    global static DMAPP.QualifierRestResource.GetResponse getQualification(String opportunityId) {
        return null;
    }
    @RemoteAction
    global static void logQuicklinkUsageHit(String quicklinkId) {

    }
    @RemoteAction
    global static DMAPP.QualifierRestResource.PutPostReponse putQualification(DMAPP.QualifierRestResource.PutPostParam params) {
        return null;
    }
}
