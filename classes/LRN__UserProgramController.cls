/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class UserProgramController {
    global UserProgramController() {

    }
    global static List<Account> accountLookupSearch(String param) {
        return null;
    }
    global static LRN.UserProgramController.Result completeUserActivity(String userActivityId, String programEnrollmentId) {
        return null;
    }
    global static List<Contact> contactLookupSearch(String param) {
        return null;
    }
    @RemoteAction
    global static LRN.UserProgramController.Result getAllProgramEnrollment(String programEnrollmentId, String userId) {
        return null;
    }
    global static List<Lead> leadLookupSearch(String param) {
        return null;
    }
    global static LRN.UserProgramController.Result markUserActivityComplete(String userActivityId, String programEnrollmentId, String objectId, String objectType) {
        return null;
    }
    global static List<Opportunity> opportunityLookupSearch(String param) {
        return null;
    }
global class AutoCompleteWrapper {
}
global class ProgramEnrollmentWrapper {
    global ProgramEnrollmentWrapper() {

    }
}
global class Result {
}
}
