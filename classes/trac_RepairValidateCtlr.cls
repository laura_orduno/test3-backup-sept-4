/*
 * Controller for the RepairValidate page
 * Part of the SMB Repairs project
 *
 * @author: Grant Adamson, Traction On Demand 
 */

public without sharing class trac_RepairValidateCtlr extends trac_RepairCaseCtlr{
	// list of error codes that should not allow the user to continue (manually links are hidden)
	private static final Set<String> HARD_STOP_CODES = new Set<String>{
		'PRODUCT_NOT_FOUND', 'EQUIP_REPORT_LOST', 'EQUIP_REPORT_STOLEN', 'APPLE_WARRANTY_EXCEPTION', 'INVALID_PRODUCT_RETIRED'
	};
	public static final String MESSAGE_TYPE_WARNING = 'WARNING';
	
	public Boolean showManualLink {get; private set;}
	public Boolean warningPresent {get; private set;}
	
	
	public trac_RepairValidateCtlr() {
		super();
		
		showManualLink = false;
		warningPresent = false;
	}
	
	
	public void doValidate() {
		trac_RepairCalloutResponse resp = trac_RepairCallout.validate(theCase);
		
		if(resp.success) {
			system.debug('Message Type:'+resp.messageType);
			if(resp.hasWarning()) {
				trac_PageUtils.addWarning(resp.messageBody);
				warningPresent = true;
			} else {
				success = true;
				update theCase;
			}
			system.debug('sccess:'+success);
			showManualLink = false;
		} else {
			trac_PageUtils.addError(resp.messageBody);
			showManualLink = isSoftStop(resp.errorCode);
		}
		
		complete = true;
	}
	
	
	public void doContinue() {
		update theCase;
	}
	
	
	private static Boolean isSoftStop(String errorCode) {
		if(HARD_STOP_CODES.contains(errorCode)) {
			return false;
		} else {
			return true;
		}
	}

}