/**
 * Runs as a batch that ensure Webservice Integration Log object keeps only
 * 3 months worth of history.  Any other logic on Webservice Integration Log
 * should also be kept here.
 */
global class WebserviceLogKeeper implements database.batchable<sobject>{
	global final string query;
    global WebserviceLogKeeper(string query){
        this.query=query;
    }
    global database.querylocator start(database.batchablecontext bc){
        return database.getquerylocator(query);
    }
    global void execute(database.batchablecontext bc,list<sobject> scope){
        try{
            list<sobject> deleteList=new list<sobject>();
            for(sobject obj:scope){
                deleteList.add(obj);
                if(deleteList.size()>9950){
                    delete deleteList;
                    deleteList.clear();
                }
            }
            if(!deleteList.isempty()){
                delete deleteList;
                deleteList.clear();
            }
        }catch(dmlexception dmle){
            system.debug(dmle.getmessage());
        }catch(exception e){
            system.debug(e.getmessage());
        }
    }
    global void finish(database.batchablecontext bc){}
}