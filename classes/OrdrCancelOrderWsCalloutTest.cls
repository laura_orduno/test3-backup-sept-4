@isTest
private class OrdrCancelOrderWsCalloutTest 
{  
    @testSetup static void testData() {
        OrdrTestDataFactory.createOrderingWSCustomSettings('CancelOrderEndpoint','http://testUrl');
    }
    
    @isTest
    private static void testCancelOrder() 
    {
        //leave the DML outside the Test.startTest();
        List<Order> orders = OrdrTestDataFactory.createOrdersWithServiceAddress(); 
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        //Call the method that invokes a callout
        TpFulfillmentCustomerOrderV3.CustomerOrder response = OrdrCancelOrderWsCallout.cancelOrder(orders[0].Id);
        System.assertNotEquals(null, response, 'response should not be null');
        System.assertNotEquals(null, response.CharacteristicValue[0], 'response.CharacteristicValue[0] should not be null');
        // Verify that a fake result is returned
        System.assertEquals(OrdrCancelOrderManager.SUCCESS_SYMBOL, response.CharacteristicValue[0].Value[0]);
        
        Test.stopTest();        
    }
    
    @isTest
    private static void testCancelOrderWithEmptyExternalId() 
    {
        //leave the DML outside the Test.startTest();
        List<Order> orders = OrdrTestDataFactory.createOrdersWithServiceAddress();         
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        //Call the method that invokes a callout
        try 
        {
               TpFulfillmentCustomerOrderV3.CustomerOrder response = OrdrCancelOrderWsCallout.cancelOrder('');
               System.assert(false, 'Exception expected');  
        }
        catch(Exception e) 
        {
            System.debug(e.getTypeName() + ': ' + e.getMessage());
        }
        
        Test.stopTest();        
    }
    

}