/**
 * Having enumerated values of the types of Number Reservation: WLN for Wireline, TOLLFREE, WLS for Wireless,  VOIP
 */

/**
 * @author Santosh Rath
 * Used for Telephone Number Reservation
 */
public Enum OrdrTnReservationType {
    WLN, TOLLFREE, WLS
}