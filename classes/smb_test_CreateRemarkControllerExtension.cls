/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class smb_test_CreateRemarkControllerExtension {

	@isTest(SeeAllData=true)
    static void myUnitTest() {
    	
    	User u = [SELECT Id 
    			  FROM User 
    			  WHERE (Profile.Name = 'System Admin TEAM'
    			  			OR
    			  		 Profile.Name = 'System Vendor')
    			  		 AND
    			  		 IsActive = true
    			  LIMIT 1];
    	
		System.runAs(u) {
    	
	        // TO DO: implement unit test
	       PageReference ref = new PageReference('/apex/smb_CreateRemark'); 
	       Test.setCurrentPage(ref);
	       
	       Task task = new Task(subject='test', ActivityDate=Date.Today(),Status='Not Started',Priority='Normal');
	       insert task;
	       
	       ApexPages.Standardcontroller std = new ApexPages.Standardcontroller(task);          
	       smb_CreateRemarkControllerExtension smb = new smb_CreateRemarkControllerExtension(std);
	       
	       smb.saveRecord(); 
		}
        
    }
}