public with sharing class  BillingAdjustmentReopenController {

    private Billing_Adjustment__c ba;
    
    // The extension constructor initializes the private member
    // variable acct by using the getRecord method from the standard
    // controller.
    
     public BillingAdjustmentReopenController(ApexPages.StandardController stdController) {
        this.ba = (Billing_Adjustment__c)stdController.getRecord();
        this.ba = [select id,name,CreatedById,Original_Recordtype__c from Billing_Adjustment__c where id =:this.ba.id]; 
       
    }
    
    public PageReference redirect() {
        PageReference pr = new PageReference('/' + this.ba.id);
        ba.Approval_Status__c=null;
        ba.Status__c ='Draft';
        ba.Closed_Status__c = '';
        RecordType recType = [Select Id From RecordType  Where SobjectType = 'Billing_Adjustment__c' and Name =:this.ba.Original_Recordtype__c];
        ba.RecordTypeId = recType.id;
        ba.Original_RecordType__c ='';
        ba.Last_Approver_User__c =null;
        update ba; 
        pr.setRedirect(true); 
        return pr; 
    }
    
 
    
}