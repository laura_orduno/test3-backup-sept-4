@isTest
private class OCOM_CreditAssessment_HelperTest {
    @testSetup
    static void testDataSetup() {
        String orderId= OrdrTestDataFactory.singleMethodForOrderId();
        Order ordObj=[select Account.Credit_Profile__c,AccountId, Credit_Check_Not_Required__c,RequestedDate__C,status,Rush__c,Ban__c,Shipping_Address__c FROM Order where id=:orderId];
        ordObj.status='Not Submitted';
        ordObj.Rush__c=true;
        ordObj.Ban__c='12345';
        ordObj.Shipping_Address__c='test address';
        ordObj.Credit_Check_Not_Required__c=false;
        Date day1 = Date.today();
        Date todayPlus = day1.addDays(4);
        ordObj.RequestedDate__C=todayPlus;
        update ordObj;
        
        Account anAccount = [select Account_Status__c from Account where id=:ordObj.AccountId limit 1];
        anAccount.Account_Status__c = 'ACTIVE';
        update anAccount;
        
        Credit_Assessment__c aCAR = new Credit_Assessment__c(CAR_Status__c='Pending');
        insert aCAR;
        
        Credit_Profile__c creditProfile=new Credit_Profile__c(name='CP-RCID', credit_reference_TL__c='123456', account__c=ordObj.accountid, RecordTypeId = Schema.SObjectType.Credit_Profile__c.getRecordTypeInfosByName().get('Business').getRecordTypeId(), Legal_Entity_Type__c='Incorporated (INC)');
        insert creditProfile;
        
        OrderItem anOrderItem=[select vlocity_cmt__RecurringTotal__c from OrderItem where OrderId=:orderId limit 1];
        anOrderItem.vlocity_cmt__RecurringTotal__c=400.00;
        update anOrderItem;
       
        ordObj.Account.Credit_Profile__c =creditProfile.id;
        ordObj.Credit_Assessment__c=aCAR.id;
        update ordObj;
    }
    
    @isTest
    // test scenario where order ID provided is invalid
    private static void creditCheckTest1() {
        Map<String, Object> outputMap = new Map<String, Object>();
        Test.startTest();
        OCOM_CreditAssessment_Helper.checkCredit('invalidOrderId', outputMap);
        Test.stopTest();
        system.assertEquals(outputMap.get('validationError'), true);
        system.assertEquals(outputMap.get('validationFailureMessage'), system.Label.Order_Not_Found);
    }
    
    @isTest
    // test scenario where credit assessment is required
    private static void creditCheckTest2() {
        Order ordObj = [SELECT Id, AccountId FROM Order WHERE orderMgmtId__c='1234567890' LIMIT 1];
        if (ordObj != null) {
            Map<String, Object> outputMap = new Map<String, Object>();
            Test.startTest();
            OCOM_CreditAssessment_Helper.checkCredit(String.valueOf(ordObj.id), outputMap);
            Test.stopTest();
            system.assertEquals(outputMap.get('isCarRequired'), true);
        }
    }

    @isTest
    // test scenario where account does not have credit profile
    private static void creditCheckTest3() {
     Test.startTest();
        Order ordObj = [SELECT Id, AccountId FROM Order WHERE orderMgmtId__c='1234567890' LIMIT 1];
        if (ordObj != null) {
            Account anAccount = [select Credit_Profile__c from Account where id=:ordObj.AccountId limit 1];
            anAccount.Credit_Profile__c = null;
            update anAccount;
            Map<String, Object> outputMap = new Map<String, Object>();
            
            OCOM_CreditAssessment_Helper.checkCredit(String.valueOf(ordObj.id), outputMap);
            
            system.assertEquals(outputMap.get('validationError'), true);
            system.assertEquals(outputMap.get('noCreditProfile'), true);
            Test.stopTest();

        }
    }
    
    @isTest
    // test scenario where credit assessment is not required
    private static void creditCheckTest4() {
    Test.startTest();
        Order ordObj = [SELECT Id, AccountId FROM Order WHERE orderMgmtId__c='1234567890' LIMIT 1];
        if (ordObj != null) {
            List<OrderItem> orderItemList=[select vlocity_cmt__RecurringTotal__c from OrderItem where OrderId=:ordObj.Id];
            for (OrderItem anOrderItem: orderItemList) {
                anOrderItem.vlocity_cmt__RecurringTotal__c=0.00;
                anOrderItem.vlocity_cmt__OneTimeTotal__c=0.00;
            }
            update orderItemList;
            Map<String, Object> outputMap = new Map<String, Object>();
            
            OCOM_CreditAssessment_Helper.checkCredit(String.valueOf(ordObj.id), outputMap);
            Test.stopTest();
        }
    }

    @isTest
    // test scenario where credit assessment already performed and is not required
    private static void creditCheckTest5() {
        Order ordObj = [SELECT Id, Credit_Check_Not_Required__c, Credit_Assessment__c FROM Order WHERE orderMgmtId__c='1234567890' LIMIT 1];
        if (ordObj != null) {
            ordObj.Credit_Assessment__c = null;
            ordObj.Credit_Check_Not_Required__c=true;
            update ordObj;
            Map<String, Object> outputMap = new Map<String, Object>();
            Test.startTest();
            OCOM_CreditAssessment_Helper.checkCredit(String.valueOf(ordObj.id), outputMap);
            Test.stopTest();
        }
    }

    @isTest
    // test scenario where CAR is required and missing
    private static void creditCheckTest6() {
        Order ordObj = [SELECT Id, Credit_Assessment__c FROM Order WHERE orderMgmtId__c='1234567890' LIMIT 1];
        if (ordObj != null) {
            ordObj.Credit_Assessment__c = null;
            update ordObj;
            Map<String, Object> outputMap = new Map<String, Object>();
            Test.startTest();
            OCOM_CreditAssessment_Helper.checkCredit(String.valueOf(ordObj.id), outputMap);
            Test.stopTest();
            system.assertEquals(outputMap.get('isCarRequired'), true);
            system.assertEquals(outputMap.get('validationError'), true);
        }
    }
    
    @isTest
    // test scenario for order disconnect
    private static void creditCheckTest7() {
        Order ordObj = [SELECT Id, Type FROM Order WHERE orderMgmtId__c='1234567890' LIMIT 1];
        if (ordObj != null) {
            ordObj.Type = 'Disconnect';
            update ordObj;
            Map<String, Object> outputMap = new Map<String, Object>();
            Test.startTest();
            OCOM_CreditAssessment_Helper.checkCredit(String.valueOf(ordObj.id), outputMap);
            Test.stopTest();
            system.assertEquals(outputMap.get('isCarRequired'), false);
        }
    }
    
    @isTest
    // test scenario where credit assessment is in draft status
    private static void creditCheckTest8() {
        Order ordObj = [SELECT Id, Credit_Assessment__c FROM Order WHERE orderMgmtId__c='1234567890' LIMIT 1];
        if (ordObj != null) {
            Credit_Assessment__c creditAssessment = [SELECT Id, CAR_Status__c FROM Credit_Assessment__c WHERE Id=:ordObj.Credit_Assessment__c LIMIT 1];
            creditAssessment.CAR_Status__c = 'DRAFT';
            update creditAssessment;
            Map<String, Object> outputMap = new Map<String, Object>();
            Test.startTest();
            OCOM_CreditAssessment_Helper.checkCredit(String.valueOf(ordObj.id), outputMap);
            Test.stopTest();
            system.assertEquals(outputMap.get('validationError'), true);
        }
    }
    
    @isTest
    // test scenario where order amend and line item $0.00
    private static void creditCheckTest9() {
    Test.startTest();
        Order ordObj = [SELECT Id, AccountId, orderMgmtId_Status__c FROM Order WHERE orderMgmtId__c='1234567890' LIMIT 1];
        if (ordObj != null) {
            ordObj.orderMgmtId_Status__c = 'Processing';
            update ordObj;
            OrderItem anOrderItem=[select vlocity_cmt__RecurringTotal__c from OrderItem where OrderId=:ordObj.Id limit 1];
            anOrderItem.vlocity_cmt__RecurringTotal__c=0.00;
            update anOrderItem;
            Map<String, Object> outputMap = new Map<String, Object>();
            
            OCOM_CreditAssessment_Helper.checkCredit(String.valueOf(ordObj.id), outputMap);
            Test.stopTest();
        }
    }

    @isTest
    // test scenario where order amend and credit assessment is required
    private static void creditCheckTest10() {
        Order ordObj = [SELECT Id, AccountId, orderMgmtId_Status__c, status FROM Order WHERE orderMgmtId__c='1234567890' LIMIT 1];
        if (ordObj != null) {
            ordObj.orderMgmtId_Status__c = 'Processing';
            ordObj.status = 'Not Submitted';
            update ordObj;
            Map<String, Object> outputMap = new Map<String, Object>();
            Test.startTest();
            OCOM_CreditAssessment_Helper.checkCredit(String.valueOf(ordObj.id), outputMap);
            Test.stopTest();
            system.assertEquals(outputMap.get('isCarRequired'), true);
        }
    }

    @isTest
    // test scenario where order amend, no CAR and credit assessment is required
    private static void creditCheckTest11() {
    Test.startTest();
        Order ordObj = [SELECT Id, AccountId, orderMgmtId_Status__c, Credit_Assessment__c, status FROM Order WHERE orderMgmtId__c='1234567890' LIMIT 1];
        if (ordObj != null) {
            ordObj.orderMgmtId_Status__c = 'Processing';
            ordObj.Credit_Assessment__c = null;
            ordObj.status = 'Not Submitted';
            update ordObj;
            List<OrderItem> orderItemList=[SELECT vlocity_cmt__OneTimeTotal__c FROM OrderItem WHERE OrderId=:ordObj.Id];
            for (OrderItem orderItemObj: orderItemList) {
                orderItemObj.vlocity_cmt__OneTimeTotal__c=0.00;
            }
            update orderItemList;
            OrderItem anOrderItem=[select amendStatus__c, vlocity_cmt__RecurringCharge__c, vlocity_cmt__OneTimeTotal__c from OrderItem where OrderId=:ordObj.Id limit 1];
            anOrderItem.amendStatus__c='Add';
            anOrderItem.vlocity_cmt__RecurringCharge__c=500.00;
            anOrderItem.vlocity_cmt__OneTimeTotal__c=1000.00;
            update anOrderItem;
            Map<String, Object> outputMap = new Map<String, Object>();
            OCOM_CreditAssessment_Helper.checkCredit(String.valueOf(ordObj.id), outputMap);
            Test.stopTest();
            system.assertEquals(outputMap.get('isCarRequired'), true);
        }
    }

    @isTest
    // test scenario where credit assessment is not required due to MRC/NRC/promotions below threshold
    private static void creditCheckTest12() {
    Test.startTest();
        Order ordObj = [SELECT Id, AccountId FROM Order WHERE orderMgmtId__c='1234567890' LIMIT 1];
        if (ordObj != null) {
            List<OrderItem> orderItemList=[select vlocity_cmt__RecurringTotal__c from OrderItem where OrderId=:ordObj.Id];
            for (OrderItem anOrderItem: orderItemList) {
                anOrderItem.vlocity_cmt__RecurringTotal__c=10.00;
                anOrderItem.vlocity_cmt__OneTimeTotal__c=10.00;
            }
            update orderItemList;
            Map<String, Object> outputMap = new Map<String, Object>();
            
            OCOM_CreditAssessment_Helper.checkCredit(String.valueOf(ordObj.id), outputMap);
            Test.stopTest();
        }
    }
	
    @isTest
    // test scenario where account does not have credit profile
    private static void creditCheckTest13() {
     	RecordType customerSolutionRecType=[SELECT Id FROM RecordType where SobjectType='Order' and Name='Customer Solution' limit 1];
        RecordType accntRecTypeId=[SELECT Id FROM RecordType where SobjectType='Account' and Name='RCID' limit 1];        
        Account accntObj = new Account(Name='TestAccount10',CAN__c='12345678',Company_Legal_Name__c='CompanyLegalName',Legal_Entity_Type__c='Corporate',Operating_Name_DBA__c='Telus',BillingCity='TestCity', BillingCountry='TestCity', BillingPostalCode='TestCity', BillingState='TestCity', BillingStreet='TestCity', CustProfId__c='12345', RecordTypeId =accntRecTypeId.id);
        insert accntObj;
        Id pricebookId= Test.getStandardPricebookId();
        String accountID=accntObj.id;
        Order ordObj=new Order();
        ordObj.AccountId=accountID;
        ordObj.RecordTypeId = customerSolutionRecType.Id;
        ordObj.Bill_To_a_New_Account__c=FALSE;
        ordObj.CurrencyIsoCode='CAD';
        ordObj.EffectiveDate=Date.today();
        ordObj.IsReductionOrder=FALSE;
        ordObj.Name='TestOrder';
        ordObj.Status='Not Submitted';
        ordObj.Type = 'New';
        ordObj.Pricebook2Id=pricebookId;
        insert ordObj;
        Test.startTest();
        Order ordObj1=new Order();
        ordObj1.AccountId=accountID;
        ordObj1.RecordTypeId = customerSolutionRecType.Id;
        ordObj1.ParentId__c = ordObj.id;
        ordObj1.Bill_To_a_New_Account__c=FALSE;
        ordObj1.CurrencyIsoCode='CAD';
        ordObj1.EffectiveDate=Date.today();
        ordObj1.IsReductionOrder=FALSE;
        ordObj1.Name='TestOrderChild';
        ordObj1.Status='Not Submitted';
        ordObj1.Type = 'New';
        ordObj1.Pricebook2Id=pricebookId;
        insert ordObj1;
        
        Product2 productInst = new Product2(name = 'Test__Product', productcode = 'TESTCODE', isActive = true);
        productInst.Sellable__c=true;
        productInst.orderMgmtId__c='921110';
        productInst.vlocity_cmt__TrackAsAgreement__c=false;
        insert productInst;
        PricebookEntry pbe = new PricebookEntry(product2id = productInst.id, unitprice = 1,  isactive = true, pricebook2id = pricebookId);
        insert pbe;
        
        list<OrderItem> odrItmlst = new list<OrderItem>(); 
       
        OrderItem ordrItem0=new OrderItem();
        ordrItem0.OrderId=ordObj1.id;
        ordrItem0.PricebookEntryId=pbe.id;
        ordrItem0.Quantity=1;
        ordrItem0.UnitPrice=100.00;
        ordrItem0.vlocity_cmt__RecurringTotal__c=0.00;
        ordrItem0.vlocity_cmt__OneTimeTotal__c=100.00;
        ordrItem0.vlocity_cmt__LineNumber__c='0001';
        ordrItem0.vlocity_cmt__ProvisioningStatus__c=null;
        ordrItem0.orderMgmtId__c='921110';
        ordrItem0.vlocity_cmt__JSONAttribute__c='{"TELUSCHAR":[{"$$AttributeDefinitionStart$$":null,"objectid__c":"01te0000003LV0nAAG","attributeid__c":"a6oe00000008UOrAAM","attributecategoryid__c":"a6ne00000008VAdAAM","categorycode__c":"TELUSCHAR","categoryname__c":"Characteristics","attributeuniquecode__c":"ATTRIBUTE-183","attributeconfigurable__c":true,"attributedisplaysequence__c":"9","attributefilterable__c":false,"isactive__c":true,"attributedisplayname__c":"Agent Role","displaysequence__c":null,"formatmask__c":null,"hasrule__c":false,"isconfigurable__c":true,"ishidden__c":false,"valueinnumber__c":null,"objecttype__c":"Product2","querycode__c":null,"isreadonly__c":false,"isquerydriven__c":false,"querylabel__c":null,"id":"a6me00000008dRmAAI","isrequired__c":true,"rulemessage__c":null,"uidisplaytype__c":"Text","value__c":null,"valuedatatype__c":"Text","valuedescription__c":null,"attributeRunTimeInfo":{"dataType":"Text","uiDisplayType":"Text","value":"y"},"Name":"Agent Role","Code":"ATTRIBUTE-183","Filterable":false,"SegmentValue":"y","$$hashKey":"07T","$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"objectid__c":"01te0000003LV0nAAG","attributeid__c":"a6oe00000008UOsAAM","attributecategoryid__c":"a6ne00000008VAdAAM","categorycode__c":"TELUSCHAR","categoryname__c":"Characteristics","attributeuniquecode__c":"ATTRIBUTE-184","attributeconfigurable__c":true,"attributedisplaysequence__c":"10","attributefilterable__c":false,"isactive__c":true,"attributedisplayname__c":"Comments","displaysequence__c":null,"formatmask__c":null,"hasrule__c":false,"isconfigurable__c":true,"ishidden__c":false,"valueinnumber__c":null,"objecttype__c":"Product2","querycode__c":null,"isreadonly__c":false,"isquerydriven__c":false,"querylabel__c":null,"id":"a6me00000008dRlAAI","isrequired__c":false,"rulemessage__c":null,"uidisplaytype__c":"Text","value__c":null,"valuedatatype__c":"Text","valuedescription__c":null,"attributeRunTimeInfo":{"dataType":"Text","uiDisplayType":"Text"},"Name":"Comments","Code":"ATTRIBUTE-184","Filterable":false,"$$hashKey":"07U","$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"objectid__c":"01te0000003LV0nAAG","attributeid__c":"a6oe00000008UQAAA2","attributecategoryid__c":"a6ne00000008VAdAAM","categorycode__c":"TELUSCHAR","categoryname__c":"Characteristics","attributeuniquecode__c":"ATTRIBUTE-197","attributeconfigurable__c":true,"attributedisplaysequence__c":"1","attributefilterable__c":false,"isactive__c":true,"attributedisplayname__c":"Download speed","displaysequence__c":null,"formatmask__c":null,"hasrule__c":false,"isconfigurable__c":true,"ishidden__c":false,"valueinnumber__c":null,"objecttype__c":"Product2","querycode__c":null,"isreadonly__c":false,"isquerydriven__c":false,"querylabel__c":null,"id":"a6me00000008czFAAQ","isrequired__c":true,"rulemessage__c":null,"uidisplaytype__c":"Text","value__c":"25 Mbps","valuedatatype__c":"Text","valuedescription__c":null,"attributeRunTimeInfo":{"dataType":"Text","uiDisplayType":"Text","default":"25 Mbps","value":"25 Mbps"},"Name":"Download speed","Code":"ATTRIBUTE-197","Filterable":false,"SegmentValue":"25 Mbps","$$hashKey":"07V","$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"objectid__c":"01te0000003LV0nAAG","attributeid__c":"a6oe00000008ULpAAM","attributecategoryid__c":"a6ne00000008VAdAAM","categorycode__c":"TELUSCHAR","categoryname__c":"Characteristics","attributeuniquecode__c":"ATTRIBUTE-063","attributeconfigurable__c":true,"attributedisplaysequence__c":"3","attributefilterable__c":false,"isactive__c":true,"attributedisplayname__c":"IP Address Assignment Type","displaysequence__c":null,"formatmask__c":null,"hasrule__c":false,"isconfigurable__c":true,"ishidden__c":false,"valueinnumber__c":null,"objecttype__c":"Product2","querycode__c":null,"isreadonly__c":false,"isquerydriven__c":false,"querylabel__c":null,"id":"a6me00000008dW4AAI","isrequired__c":true,"rulemessage__c":null,"uidisplaytype__c":"Text","value__c":"Dynamic","valuedatatype__c":"Text","valuedescription__c":null,"attributeRunTimeInfo":{"dataType":"Text","uiDisplayType":"Text","default":"Dynamic","value":"Dynamic"},"Name":"IP Address Assignment Type","Code":"ATTRIBUTE-063","Filterable":false,"SegmentValue":"Dynamic","$$hashKey":"07W","$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"objectid__c":"01te0000003LV0nAAG","attributeid__c":"a6oe00000008UOqAAM","attributecategoryid__c":"a6ne00000008VAdAAM","categorycode__c":"TELUSCHAR","categoryname__c":"Characteristics","attributeuniquecode__c":"ATTRIBUTE-182","attributeconfigurable__c":true,"attributedisplaysequence__c":"6","attributefilterable__c":false,"isactive__c":true,"attributedisplayname__c":"Max Usage","displaysequence__c":null,"formatmask__c":null,"hasrule__c":false,"isconfigurable__c":true,"ishidden__c":false,"valueinnumber__c":null,"objecttype__c":"Product2","querycode__c":null,"isreadonly__c":false,"isquerydriven__c":false,"querylabel__c":null,"id":"a6me00000008dRnAAI","isrequired__c":true,"rulemessage__c":null,"uidisplaytype__c":"Text","value__c":null,"valuedatatype__c":"Text","valuedescription__c":null,"attributeRunTimeInfo":{"dataType":"Text","uiDisplayType":"Text","value":"20"},"Name":"Max Usage","Code":"ATTRIBUTE-182","Filterable":false,"SegmentValue":"20","$$hashKey":"07X","$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"objectid__c":"01te0000003LV0nAAG","attributeid__c":"a6oe00000008UOoAAM","attributecategoryid__c":"a6ne00000008VAdAAM","categorycode__c":"TELUSCHAR","categoryname__c":"Characteristics","attributeuniquecode__c":"ATTRIBUTE-180","attributeconfigurable__c":true,"attributedisplaysequence__c":"4","attributefilterable__c":false,"isactive__c":true,"attributedisplayname__c":"Number of Dynamic IPs","displaysequence__c":null,"formatmask__c":null,"hasrule__c":false,"isconfigurable__c":true,"ishidden__c":false,"valueinnumber__c":null,"objecttype__c":"Product2","querycode__c":null,"isreadonly__c":false,"isquerydriven__c":false,"querylabel__c":null,"id":"a6me00000008dRpAAI","isrequired__c":true,"rulemessage__c":null,"uidisplaytype__c":"Text","value__c":null,"valuedatatype__c":"Text","valuedescription__c":null,"attributeRunTimeInfo":{"dataType":"Text","uiDisplayType":"Text","value":"3"},"Name":"Number of Dynamic IPs","Code":"ATTRIBUTE-180","Filterable":false,"SegmentValue":"3","$$hashKey":"07Y","$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"objectid__c":"01te0000003LV0nAAG","attributeid__c":"a6oe00000008UOpAAM","attributecategoryid__c":"a6ne00000008VAdAAM","categorycode__c":"TELUSCHAR","categoryname__c":"Characteristics","attributeuniquecode__c":"ATTRIBUTE-181","attributeconfigurable__c":true,"attributedisplaysequence__c":"5","attributefilterable__c":false,"isactive__c":true,"attributedisplayname__c":"Number of Included Email Accounts","displaysequence__c":null,"formatmask__c":null,"hasrule__c":false,"isconfigurable__c":true,"ishidden__c":false,"valueinnumber__c":null,"objecttype__c":"Product2","querycode__c":null,"isreadonly__c":false,"isquerydriven__c":false,"querylabel__c":null,"id":"a6me00000008dW3AAI","isrequired__c":true,"rulemessage__c":null,"uidisplaytype__c":"Text","value__c":null,"valuedatatype__c":"Text","valuedescription__c":null,"attributeRunTimeInfo":{"dataType":"Text","uiDisplayType":"Text","value":"2"},"Name":"Number of Included Email Accounts","Code":"ATTRIBUTE-181","Filterable":false,"SegmentValue":"2","$$hashKey":"07Z","$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"objectid__c":"01te0000003LV0nAAG","attributeid__c":"a6oe00000008UOnAAM","attributecategoryid__c":"a6ne00000008VAdAAM","categorycode__c":"TELUSCHAR","categoryname__c":"Characteristics","attributeuniquecode__c":"ATTRIBUTE-179","attributeconfigurable__c":true,"attributedisplaysequence__c":"8","attributefilterable__c":false,"isactive__c":true,"attributedisplayname__c":"Offering Type","displaysequence__c":null,"formatmask__c":null,"hasrule__c":false,"isconfigurable__c":true,"ishidden__c":false,"valueinnumber__c":null,"objecttype__c":"Product2","querycode__c":null,"isreadonly__c":false,"isquerydriven__c":false,"querylabel__c":null,"id":"a6me00000008dRqAAI","isrequired__c":false,"rulemessage__c":null,"uidisplaytype__c":"Text","value__c":"Business","valuedatatype__c":"Text","valuedescription__c":null,"attributeRunTimeInfo":{"dataType":"Text","uiDisplayType":"Text","default":"Business","value":"Business"},"Name":"Offering Type","Code":"ATTRIBUTE-179","Filterable":false,"SegmentValue":"Business","$$hashKey":"080","$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"objectid__c":"01te0000003LV0nAAG","attributeid__c":"a6oe00000008UOtAAM","attributecategoryid__c":"a6ne00000008VAdAAM","categorycode__c":"TELUSCHAR","categoryname__c":"Characteristics","attributeuniquecode__c":"ATTRIBUTE-185","attributeconfigurable__c":true,"attributedisplaysequence__c":"11","attributefilterable__c":false,"isactive__c":true,"attributedisplayname__c":"Provisioning Indicator","displaysequence__c":null,"formatmask__c":null,"hasrule__c":false,"isconfigurable__c":true,"ishidden__c":false,"valueinnumber__c":null,"objecttype__c":"Product2","querycode__c":null,"isreadonly__c":false,"isquerydriven__c":false,"querylabel__c":null,"id":"a6me00000008dRkAAI","isrequired__c":true,"rulemessage__c":null,"uidisplaytype__c":"Text","value__c":"Copper","valuedatatype__c":"Text","valuedescription__c":null,"attributeRunTimeInfo":{"dataType":"Text","uiDisplayType":"Text","default":"Copper","value":"Copper"},"Name":"Provisioning Indicator","Code":"ATTRIBUTE-185","Filterable":false,"SegmentValue":"Copper","$$hashKey":"081","$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"objectid__c":"01te0000003LV0nAAG","attributeid__c":"a6oe00000008UQ6AAM","attributecategoryid__c":"a6ne00000008VAdAAM","categorycode__c":"TELUSCHAR","categoryname__c":"Characteristics","attributeuniquecode__c":"ATTRIBUTE-195","attributeconfigurable__c":true,"attributedisplaysequence__c":"7","attributefilterable__c":false,"isactive__c":true,"attributedisplayname__c":"TISAA","displaysequence__c":null,"formatmask__c":null,"hasrule__c":false,"isconfigurable__c":true,"ishidden__c":false,"valueinnumber__c":null,"objecttype__c":"Product2","querycode__c":null,"isreadonly__c":false,"isquerydriven__c":false,"querylabel__c":null,"id":"a6me00000008dS7AAI","isrequired__c":true,"rulemessage__c":null,"uidisplaytype__c":"Text","value__c":"True","valuedatatype__c":"Text","valuedescription__c":null,"attributeRunTimeInfo":{"dataType":"Text","uiDisplayType":"Text","default":"True","value":"True"},"Name":"TISAA","Code":"ATTRIBUTE-195","Filterable":false,"SegmentValue":"True","$$hashKey":"082","$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"objectid__c":"01te0000003LV0nAAG","attributeid__c":"a6oe00000008UQ5AAM","attributecategoryid__c":"a6ne00000008VAdAAM","categorycode__c":"TELUSCHAR","categoryname__c":"Characteristics","attributeuniquecode__c":"ATTRIBUTE-194","attributeconfigurable__c":true,"attributedisplaysequence__c":"2","attributefilterable__c":false,"isactive__c":true,"attributedisplayname__c":"Upload speed","displaysequence__c":null,"formatmask__c":null,"hasrule__c":false,"isconfigurable__c":true,"ishidden__c":false,"valueinnumber__c":null,"objecttype__c":"Product2","querycode__c":null,"isreadonly__c":false,"isquerydriven__c":false,"querylabel__c":null,"id":"a6me00000008dS9AAI","isrequired__c":true,"rulemessage__c":null,"uidisplaytype__c":"Text","value__c":"5 Mbps","valuedatatype__c":"Text","valuedescription__c":null,"attributeRunTimeInfo":{"dataType":"Text","uiDisplayType":"Text","default":"5 Mbps","value":"5 Mbps"},"Name":"Upload speed","Code":"ATTRIBUTE-194","Filterable":false,"SegmentValue":"5 Mbps","$$hashKey":"083","$$AttributeDefinitionEnd$$":null}]}';
            ordrItem0.vlocity_cmt__ServiceAccountId__c=accntObj.id;
       odrItmlst.add(ordrItem0);
        insert odrItmlst;
        
        if (ordObj != null) {
            Map<String, Object> outputMap = new Map<String, Object>();
            OCOM_CreditAssessment_Helper.creditCheckValidation2(String.valueOf(ordObj.id), outputMap);
        }
        Test.stopTest();
    }
}