public class IC_NotificationHelper{  
   
   /*********** Email Template Name***********/
   private static String ICSReturnCustomer = 'ICS Return-Customer';
   private static String LoanerCancellation = 'Wireless Loaner Agreement Cancellation';
   private static String LoanerToDealer = 'Wireless Loaner Agreement To Dealer';
   private static String LoanerToShippingPrime = 'Wireless Loaner Agreement To Shipping Prime';
   private static String LoanerReturnToDealer = 'Wireless Loaner Return To Dealer';
   private static String LoanerAgreementReminder = 'Wireless Loaner Agreement Reminder';
   private static String LoanerAgreementDocument = 'LoanerAgreementDocument'; 
   
   /************ Temple array for the SOQL Quest ********/
   private static String[] emailTemplateNames = new String[]{ICSReturnCustomer,LoanerCancellation,LoanerToDealer,LoanerToShippingPrime,LoanerReturnToDealer,LoanerAgreementReminder,LoanerAgreementDocument};
   private static Map<String, EmailTemplate> emailTemplateMap = new Map<String,EmailTemplate>();                                        
   
   /* SFDC Context URL */
   private static String  SFDC_URL = URL.getSalesforceBaseUrl().toExternalForm() + '/';
   
   /* Customer Central URL */
   //private static String  CUSTCENTAL_URL = 'http://telus.force.com/';
   private static String  CUSTCENTAL_URL = 'https://telus.secure.force.com/';

   /* Training Doc URL */
   private static String  TRAINDOC_URL = 'https://telus.secure.force.com/';
   
   /* Org-wide email address for to use in the from email address */
   private static OrgWideEmailAddress orgwiseFromEmail = [select id, Address from OrgWideEmailAddress where Address='do_not_reply_care@telus.com'];
  
   /*Customer Central Program Training Document link  */
   private static String trainingDocLink=''; 
 /*
  * Initialse the Emmail Template Map with Template Name<Key> and the EmailTmplateObject<Value>
  */
  static{
       List<EmailTemplate> templates = [Select id, Name, Subject, HtmlValue, Body from EmailTemplate where Name in :emailTemplateNames];                                            
       for(EmailTemplate template : templates){
            emailTemplateMap.put(template.Name, template);
        }
      String trainingDocumentId='';
      try{
        Document trainingDocument =[select id from Document where name='Dealer ICS Loaner Processes' Limit 1];
        trainingDocumentId = trainingDocument.id;
      }catch(Exception c){
          
      }
      trainingDocLink = TRAINDOC_URL + 'servlet/servlet.FileDownload?file=' +trainingDocumentId;
  }
 
   /*
    * Send EmailNotification to Provider for New Loaner Device 
    */
    public static void sendNewDeviceNotificationEmail(IC_SolutionTriggerHandler.ICSItem icsItem){
        if(icsItem.ProviderDeliveryMethod=='Courier to Customer'){
            sendEmail(mergeTemplate(icsItem,LoanerToShippingPrime));       
        }else if(icsItem.ProviderDeliveryMethod=='Customer Pick-up'){
          
            Messaging.SingleEmailMessage emailMsg  =  mergeTemplate(icsItem,LoanerToDealer);
            Messaging.SingleEmailMessage emailMsgLoanerDoc  =  mergeLonerAgreementTemplate(icsItem);
            sendEmail(new Messaging.SingleEmailMessage [] {emailMsg,emailMsgLoanerDoc});
        }
    }

   /*
    * Send Email Notification to Provider for Cancel Loaner Device 
    */
    public static void sendCancelDeviceNotificationEmail(IC_SolutionTriggerHandler.ICSItem icsItem){
        sendEmail(mergeTemplate(icsItem,LoanerCancellation));
    }

   /*
    * Send Email Notification To Customer 
    */
    public static void sendCustomerNotificationEmail(IC_SolutionTriggerHandler.ICSItem icsItem){
         Messaging.SingleEmailMessage emailMsg  =  mergeTemplate(icsItem,ICSReturnCustomer);
         if(icsItem.CaseContactEmail!= null && icsItem.CaseContactEmail !='' ){
            emailMsg.toAddresses = new String[]{icsItem.CaseContactEmail};
         }
         sendEmail(emailMsg);
    }
    
   /*
    * Send Email Notification to Provder for Device Return case
    */
    public static void sendReturnDeviceNotificationEmail(IC_SolutionTriggerHandler.ICSItem icsItem){
         sendEmail(mergeTemplate(icsItem,LoanerReturnToDealer));
    }
    
    /*
    * Send Email Notification to Remind Provider that Solutions are in New Status more tham 72 Hour(3 Days)
    */
    public static void sendRemindNotificationEmail(IC_SolutionTriggerHandler.ICSItem icsItem){
        Messaging.SingleEmailMessage emailMsg  =  mergeTemplateForReminder(icsItem,LoanerAgreementReminder);
        emailMsg.ccAddresses = new String[]{'SMBICSRequests@telus.com'};
        sendEmail(emailMsg);
    }
  
   /*
    * Send Email helper method 
    */
    public static void sendEmail(Messaging.SingleEmailMessage emailMsg){
        // Sends the email
        if(emailMsg.toAddresses !=null){
            Messaging.SendEmailResult [] result = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {emailMsg});
            //System.debug('send email result' + result);
        } else{
            //System.debug('Email Not send due to Toaddress is Null');  
        }
    }
    
    /*
    * Send Email helper method - Message List
    */
    public static void sendEmail(List<Messaging.SingleEmailMessage> emailMsgs){
        // Sends the email
        for(Integer i=0;i<emailMsgs.size();i++){
            if(emailMsgs[i].toAddresses == null){
            emailMsgs.remove(i);
            }
        }
        Messaging.SendEmailResult [] result = Messaging.sendEmail(emailMsgs);
    }
    
   /*
    * Merge the email with the provided Email Template.
    */
    public static Messaging.SingleEmailMessage mergeTemplate(IC_SolutionTriggerHandler.ICSItem icsItem, String templateName){
        
       EmailTemplate template = emailTemplateMap.get(templateName);
       String htmlBody = template.HtmlValue;
       Datetime todayDatetime = Datetime.now();
       String datetimeStr = todayDatetime.format('MMMM d,  yyyy');
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       if(htmlBody!=null){
            htmlBody = htmlBody.replace('{!Account.Name}', icsItem.CustomerName);
            htmlBody = htmlBody.replace('{!InventoryItem}', icsItem.InventoryName);
            htmlBody = htmlBody.replace('{!Today}',datetimeStr );
            htmlBody = htmlBody.replace('{!link}', SFDC_URL + icsItem.InventoryId );
            htmlBody = htmlBody.replace('{!Solution.Link}', SFDC_URL + icsItem.Id );
            htmlBody = htmlBody.replace('{!Solution.Id}', icsItem.Name);
            htmlBody = htmlBody.replace('{!Case.Id}', icsItem.CaseNumber);
            htmlBody = htmlBody.replace('{!TainingDocLink}', trainingDocLink);
            htmlBody = htmlBody.replace('{!customercentral.Solution.Link}', CUSTCENTAL_URL + icsItem.Id );
            htmlBody = htmlBody.replace('{!customercentral.Inventory.Link}', CUSTCENTAL_URL + icsItem.InventoryId);
            
            mail.htmlBody = htmlBody;
       }
        String plainBody = template.Body;
        if(plainBody !=null){
            plainBody = plainBody.replace('{!Account.Name}', icsItem.CustomerName);
            plainBody = plainBody.replace('{!InventoryItem}', icsItem.InventoryName);
            plainBody = plainBody.replace('{!Today}',datetimeStr );
            plainBody = plainBody.replace('{!link}', SFDC_URL + icsItem.InventoryId);
            plainBody = plainBody.replace('{!Solution.Link}', SFDC_URL + icsItem.Id );
            plainBody = plainBody.replace('{!Solution.Id}', icsItem.Name);
            plainBody = plainBody.replace('{!Case.Id}', icsItem.CaseNumber);
            plainBody = plainBody.replace('{!TainingDocLink}', trainingDocLink);
            plainBody = plainBody.replace('{!customercentral.Solution.Link}', CUSTCENTAL_URL + icsItem.Id );
            plainBody = plainBody.replace('{!customercentral.Inventory.Link}', CUSTCENTAL_URL + icsItem.InventoryId);
            mail.PlainTextBody=plainBody;
        }
       
        mail.subject=template.Subject + ' - ' + icsItem.Name ;
        mail.toAddresses = icsItem.ProviderContactEmails;
        if(icsItem.OwnerEmail != null){
            mail.bccAddresses = new String[]{icsItem.OwnerEmail};
        }
        mail.setOrgWideEmailAddressId(orgwiseFromEmail.id); 
        mail.setUseSignature(false);
        return mail;
    }
   
    
    /*
    * Merge the email with the provided Email Template for Reminder notification.
    */
    public static Messaging.SingleEmailMessage mergeTemplateForReminder(IC_SolutionTriggerHandler.ICSItem icsItem, String templateName){
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
        EmailTemplate template = emailTemplateMap.get(templateName);    
        String htmlBody = template.HtmlValue;
        
       if(htmlBody!=null){
            htmlBody = htmlBody.replace('{!Solution.Link}', SFDC_URL + icsItem.Id );
            htmlBody = htmlBody.replace('{!Solution.Id}', icsItem.Name);
            htmlBody = htmlBody.replace('{!customercentral.Solution.Link}', CUSTCENTAL_URL + icsItem.Id );
            mail.htmlBody = htmlBody;
       }
        String plainBody = template.Body;
        if(plainBody !=null){
            plainBody = plainBody.replace('{!Solution.Link}', SFDC_URL + icsItem.Id );
            plainBody = plainBody.replace('{!Solution.Id}', icsItem.Name);
            plainBody = plainBody.replace('{!customercentral.Solution.Link}', CUSTCENTAL_URL + icsItem.Id );
            mail.PlainTextBody=plainBody;
        }
       
        mail.subject=template.Subject + ' - ' + icsItem.Name ;
        mail.toAddresses = icsItem.ProviderContactEmails;
        if(icsItem.OwnerEmail != null){
            mail.bccAddresses = new String[]{icsItem.OwnerEmail};
        }
        mail.setOrgWideEmailAddressId(orgwiseFromEmail.id);
        mail.setUseSignature(false);
        return mail;
    }
    
     /*
    * Merge the email with the Loaner Agreement Document Html Email Template.
    */
    public static Messaging.SingleEmailMessage mergeLonerAgreementTemplate(IC_SolutionTriggerHandler.ICSItem icsItem){
        
       EmailTemplate template = emailTemplateMap.get(LoanerAgreementDocument);
       String htmlBody = template.HtmlValue;
       Datetime todayDatetime = Datetime.now();
       String datetimeStr = todayDatetime.format('MMMM d,  yyyy');
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       if(htmlBody!=null){
            htmlBody = htmlBody.replace('{!Account.Name}', icsItem.CustomerName==null? '&nbsp':icsItem.CustomerName);
            htmlBody = htmlBody.replace('{!ProviderAddress}', icsItem.ProviderAddress==null? '&nbsp':icsItem.ProviderAddress);
            htmlBody = htmlBody.replace('{!CaseOrderOrTicketNumber}', icsItem.CaseOrderOrTicketNumber==null? '&nbsp':icsItem.CaseOrderOrTicketNumber);
            htmlBody = htmlBody.replace('{!InventoryType}', icsItem.InventoryType==null? '&nbsp':icsItem.InventoryType);
            htmlBody = htmlBody.replace('{!CaseContactName}', icsItem.CaseContactName==null? '&nbsp':icsItem.CaseContactName);
            htmlBody = htmlBody.replace('{!CaseContactPhone}', icsItem.CaseContactPhone==null? '&nbsp':icsItem.CaseContactPhone);
            htmlBody = htmlBody.replace('{!CaseOnsiteContactName}', icsItem.CaseOnsiteContactName==null? '&nbsp':icsItem.CaseOnsiteContactName);
            htmlBody = htmlBody.replace('{!CaseOnsiteContactPhone}', icsItem.CaseOnsiteContactPhone==null? '&nbsp':icsItem.CaseOnsiteContactPhone);
            htmlBody = htmlBody.replace('{!Today}',datetimeStr );
            mail.htmlBody = htmlBody;
       }
       
        mail.subject=template.Subject + ' - ' + icsItem.Name ;
        mail.toAddresses = icsItem.ProviderContactEmails;
        if(icsItem.OwnerEmail != null){
            mail.bccAddresses = new String[]{icsItem.OwnerEmail};
        }
        mail.setOrgWideEmailAddressId(orgwiseFromEmail.id);
        mail.setUseSignature(false);
        return mail;
    }

}