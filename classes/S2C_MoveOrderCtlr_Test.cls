/**
 * S2C_MoveOrderCtlr_Test
 * @description Test class for S2C_MoveOrderCtlr
 * @author Thomas Tran, Traction on Demand
 * @date 07-10-2015
 */
@isTest(seeAllData=false)
private class S2C_MoveOrderCtlr_Test {
	
	@isTest static void valid_save() {
		Test.setCurrentPage(initialDataSetup());

		S2C_MoveOrderCtlr ctlr = new S2C_MoveOrderCtlr();

		List<Product2> productsToInsert = new List<Product2>();
		Product2 newProduct1 = S2C_TestUtility.createProduct('Test Product', 'Test');
		Product2 newProduct2 = S2C_TestUtility.createProduct('Test Product2', 'Test');
		productsToInsert.add(newProduct1);
		productsToInsert.add(newProduct2);

		insert productsToInsert;

		List<SRS_PODS_Product__c> podsProductToInsert = new List<SRS_PODS_Product__c>();
		SRS_PODS_Product__c newSRSPodsProduct1 = S2C_TestUtility.createSRSPODSProduct(newProduct1.Product_Family__c);
		SRS_PODS_Product__c newSRSPodsProduct2 = S2C_TestUtility.createSRSPODSProduct(newProduct2.Product_Family__c);
		podsProductToInsert.add(newSRSPodsProduct1);
		podsProductToInsert.add(newSRSPodsProduct2);
		insert podsProductToInsert;

		ctlr.outServiceRequest.SRS_PODS_Product__c = newSRSPodsProduct1.Id;
		ctlr.inServiceRequest.SRS_PODS_Product__c = newSRSPodsProduct2.Id;
		ctlr.outRequestDate = '07/10/2015';
		ctlr.inRequestDate = '07/10/2015';
		ctlr.outExistingServiceId = '1234567890';
		ctlr.outServiceIdType = 'CSID';
		ctlr.inExistingServiceId = '0123456789';
		ctlr.inServiceIdType = 'BTN';

		Test.startTest();

		PageReference redirectRef = ctlr.save();

		Test.stopTest();
		PageReference comparisonURL = new PageReference('/' + ctlr.outServiceRequest.Opportunity__c);
		System.assertEquals(comparisonURL.getUrl(), redirectRef.getUrl());
	}
	
	@isTest static void invalid_save() {
		Test.setCurrentPage(initialDataSetup());

		S2C_MoveOrderCtlr ctlr = new S2C_MoveOrderCtlr();

		List<Product2> productsToInsert = new List<Product2>();
		Product2 newProduct1 = S2C_TestUtility.createProduct('Test Product', 'Test');
		Product2 newProduct2 = S2C_TestUtility.createProduct('Test Product2', 'Test');
		productsToInsert.add(newProduct1);
		productsToInsert.add(newProduct2);

		insert productsToInsert;

		List<SRS_PODS_Product__c> podsProductToInsert = new List<SRS_PODS_Product__c>();
		SRS_PODS_Product__c newSRSPodsProduct1 = S2C_TestUtility.createSRSPODSProduct(newProduct1.Product_Family__c);
		SRS_PODS_Product__c newSRSPodsProduct2 = S2C_TestUtility.createSRSPODSProduct(newProduct2.Product_Family__c);
		podsProductToInsert.add(newSRSPodsProduct1);
		podsProductToInsert.add(newSRSPodsProduct2);
		insert podsProductToInsert;

		ctlr.outServiceRequest.SRS_PODS_Product__c = newSRSPodsProduct1.Id;
		ctlr.inServiceRequest.SRS_PODS_Product__c = newSRSPodsProduct2.Id;
		ctlr.outServiceIdType = 'CSID';
		ctlr.inServiceIdType = 'BTN';
		ctlr.populateInProductField();
		ctlr.populateInDateField();
		ctlr.updateInProduct();
		ctlr.updateInDate();

		Test.startTest();

		PageReference redirectRef = ctlr.save();

		Test.stopTest();
		System.assertEquals(null, redirectRef);
	}

	@isTest static void load_page_values(){
		Test.startTest();

		List<SelectOption> serviceType = S2C_MoveOrderCtlr.getServiceTypeValues();

		Test.stopTest();

		System.assert(!serviceType.isEmpty());
	}

	private static PageReference initialDataSetup(){
		Account newAccount = new Account(Name = 'New Account 001');
		insert newAccount;

		Contact newContact = new Contact(FirstName = 'First Contact 001', LastName = 'Last Contact 001');
		insert newContact;

		Document newDocument = new Document(Name = 'Datepicker icon', FolderId = UserInfo.getUserId(), DeveloperName = 'Datepicker_icon');
		insert newDocument;

		Document existingDoc = [SELECT Name, DeveloperName FROM Document WHERE Id = :newDocument.Id];
		System.debug('Existing :::: ' + existingDoc);

		PageReference refPage = Page.S2C_MoveOrder;

		refPage.getParameters().put('OPP_AMOUNT', '1000.00');
		refPage.getParameters().put('OPP_ACCOUNT_ID', newAccount.Id);
		refPage.getParameters().put('OPP_TYPE', 'Complex Order');
		refPage.getParameters().put('OPP_STAGE_NAME', 'Originated');
		refPage.getParameters().put('OPP_NAME', newAccount.Name + ' - COMPLEX ORDER - ' + System.now());
		refPage.getParameters().put('CONTACT_ID', newContact.Id);

		return refPage;

	}
	
}