/**
* Test class to validate proper execution of the class UserSynchronisationUtilForTelus and the trigger UserSynchronisationTriggerForTelus.
* The Language was set to en_US as it seems en_CA is not supported.
* It is expected that the 'Chatter Free User' profile is available.
**/

@isTest
public class UserSynchronisationUtilForTelusTestClass {
    static testMethod void validateUserSynchronisationUtilForTelus() {
        Profile p = [select id from Profile where name='Chatter Free User'];
        
        User testCaseUser = new User(               
                Username='testcaseuserUsername@bogustesting.org', 
                LastName='testcaseuserLastname',
                Email='test@bogustesting.org',
                Alias='myAlias8',
                CommunityNickname='testcaseuserNickName',
                TimeZoneSidKey='GMT',
                LocaleSidKey='en_US',
                EmailEncodingKey='ISO-8859-1',
                ProfileId=p.id,
                LanguageLocaleKey='en_US'
        );        
        
        System.debug('User scpq__CurrentInCPQSystem__c flag on newly created must be false.');
        System.assertEquals(false, testCaseUser.scpq__CurrentInCPQSystem__c);
        
        insert testCaseUser;
        
        testCaseUser = [SELECT scpq__CurrentInCPQSystem__c FROM User WHERE Id =:testCaseUser.Id];
        testCaseUser.FirstName = 'testcaseuserFirstNameModified';
        testCaseUser.scpq__CurrentInCPQSystem__c = true;
        update testCaseUser;
        
        testCaseUser = [SELECT scpq__CurrentInCPQSystem__c FROM User WHERE Id =:testCaseUser.Id];
        System.debug('User scpq__CurrentInCPQSystem__c flag must now be false after update.');
        //System.assertEquals(false, testCaseUser.scpq__CurrentInCPQSystem__c);
    }
}