global with sharing class OCOM_Optimizd_PostProcessor implements vlocity_cmt.VlocityOpenInterface    {

  private final String CLASS_NAME = 'OCOM_PostProcessor';


  global Boolean invokeMethod(String methodName, Map<String, Object> inputMap, Map<String, Object> outMap, Map<String, Object> options) {
    Boolean success = true;
    string TargetExternalIdKeyName = OCOM_vlocityCustomUtilcls.PriceMatrixTargetExternalId;
    Map<Id, OrderItem> OliToUpdPriceMap = new Map<Id, OrderItem>();
    //system.debug('TargetExternalIdKeyName'+TargetExternalIdKeyName);



    try {
      if (methodName == 'calculate') {


        List<Object> objectList = (List<Object>)outMap.get('output');

        //system.debug(LoggingLevel.ERROR, ' ---- OCOMPostProcessor calculation results ' + JSON.serializePretty(objectList));
        Map<String, Object> savedIdMap = new Map<String, Object>();

        if (objectList != null && objectList.size() > 0 && !objectList.isEmpty()) {
          List<Object> saveList = new List<Object>();
          Map<String, Object> dataSet = (Map<String, Object>)inputMap.get('data');
          //system.debug('***dataSet '+JSON.serializePretty(dataSet));
          if (dataSet != null && !dataSet.isEmpty()) {
            // map input data row to external id. This assumes external id is unique across all line items.
            Map<String, Map<String, Object>> externalIdToDataRowMap = new Map<String, Map<String, Object>>();
            Map<String, Object> IdToDataRowMap = new Map<String, Object>();
            for (String key : dataSet.keySet()) {
              Map<String, Object> dataRow = (Map<String, Object>)dataSet.get(key);
              String mapKey = (String)dataRow.get('Reference External Id') + '_$$_' + (String)dataRow.get('ItemID');
              if (!externalIdToDataRowMap.containsKey(mapKey)) {
                externalIdToDataRowMap.put(mapKey, dataRow);
              }
            }
            // system.debug('***externalIdToDataRowMap '+JSON.serializePretty(externalIdToDataRowMap));

            map<string, map<string, Decimal>> extIdToCurrencyMap = new  map<string, map<string, Decimal>>();
            for (Object obj : objectList) {
              // This is a row in the calculation procedure output
              Map<String, Object> row = (Map<String, Object>)obj;

              // Get the External Id from the output row-- 'OCOMTextPriceMatrix__TargetExternalId'
              String outputExternalId ;
              if (row.ContainsKey(TargetExternalIdKeyName) )
                outputExternalId = (String)row.get(TargetExternalIdKeyName);
              String childItemId = (String)row.get('ID');
              //system.debug('***childID '+childItemId);
              Map<String, Object> childDataRow = (Map<String, Object>)dataSet.get(childItemId);
              //system.debug('***childDataRow '+childDataRow);
              String childLineNum;
              if (childDataRow != null) {
                childLineNum = (String)childDataRow.get('Line Number');
              }
              // system.debug('***childLineNum '+childLineNum + externalIdToDataRowMap.size() );
              system.debug('.KeyexternalIdToDataRowMapSet' + externalIdToDataRowMap.keyset());
              system.debug('outputExternalId_____' + outputExternalId);
              Map<String, Object> dataSetRow;
              if (outputExternalId != null ) {
                for (String key : externalIdToDataRowMap.keySet()) {

                  if (key.startsWith(outputExternalId)) {

                    dataSetRow = (Map<String, Object>)externalIdToDataRowMap.get(key);

                    if (childLineNum.startsWith((String)dataSetRow.get('Line Number'))) {
                      break;
                    }
                  }
                }
              }

              //system.debug('***dataSetRow '+dataSetRow);



              if (dataSetRow != null) {
                Map<String, Object> objToSaveMap = new Map<String, Object>();
                String itemID = (String)dataSetRow.get('ItemID');

                if (dataSetRow.containsKey('Quantity') && dataSetRow.get('Quantity') != null) {
                  Decimal Qty = Decimal.valueof((String)dataSetRow.get('Quantity'));
                  objToSaveMap.put('QTY', Qty);
                }
                objToSaveMap.put('ID', itemID);

                //THIS SECTION ADDS UP THE MRC AND NRC
                //Modified the code to Replace the MRC and NRC
                Decimal mrc;
                Decimal nrc;
                if (row.containsKey('MRC') && row.get('MRC') != null)
                  mrc =  Decimal.valueOf((String)row.get('MRC')).setScale(2, RoundingMode.HALF_UP);
                if (row.containsKey('NRC') && row.get('NRC') != null)
                  nrc =  Decimal.valueOf((String)row.get('NRC')).setScale(2, RoundingMode.HALF_UP);

                string childItemSize = (childLineNum.split('\\.')).Size() > 0 ? string.Valueof((childLineNum.split('\\.')).Size()) : '0';
                string orderID =  (String)row.get('ID');
                if (!extIdToCurrencyMap.containsKey(((String)row.get(TargetExternalIdKeyName)) + orderID)) {
                  extIdToCurrencyMap.put((String)row.get(TargetExternalIdKeyName) + orderID, new map<string, decimal> {'MRC' => mrc, 'NRC' => nrc});

                } else if (extIdToCurrencyMap.containsKey(((String)row.get(TargetExternalIdKeyName)) + orderID)) {

                  mrc += extIdToCurrencyMap.get(((String)row.get(TargetExternalIdKeyName)) + orderID).get('MRC');
                  nrc += extIdToCurrencyMap.get(((String)row.get(TargetExternalIdKeyName)) + orderID).get('NRC');
                  extIdToCurrencyMap.get(((String)row.get(TargetExternalIdKeyName)) + orderID).put('MRC', mrc);
                  extIdToCurrencyMap.get(((String)row.get(TargetExternalIdKeyName)) + orderID).put('NRC', nrc);

                }
                //system.debug('extIdToCurrencyMap____' +extIdToCurrencyMap);

                objToSaveMap.put('MRC', mrc);
                // system.debug(LoggingLevel.ERROR,'mrc_afterelse: ' + mrc);


                objToSaveMap.put('NRC', nrc);

                // system.debug(LoggingLevel.ERROR,'nrc_afterelse: ' + nrc);

                // save the rest of the fields from the output row
                for (String key : row.keySet()) {
                  if (!objToSaveMap.containsKey(key)) {
                    objToSaveMap.put(key, row.get(key));
                  }
                }
                saveList.add(objToSaveMap);
              }
            }

            String duplicateOli = 'x';
            for (Object savedItems : saveList) {
              String currentOlid = (String)((Map<String, Object>)savedItems).get('ID');
              Decimal currentMrc = (Decimal)((Map<String, Object>)savedItems).get('MRC');
              Decimal currentNrc = (Decimal)((Map<String, Object>)savedItems).get('NRC');
              if (!savedIdMap.containsKey(currentOlid)) {
                savedIdMap.put(currentOlid, savedItems);
              } else if (savedIdMap.containsKey(currentOlid)) {
                Decimal dupMRC = (Decimal)((Map<String, Object>)savedIdMap.get(currentOlid)).get('MRC');
                Decimal dupNRC = (Decimal)((Map<String, Object>)savedIdMap.get(currentOlid)).get('NRC');
                ((Map<String, Object>)savedIdMap.get(currentOlid)).put('MRC', dupMRC + currentMrc);
                ((Map<String, Object>)savedIdMap.get(currentOlid)).put('NRC', dupNRC + currentNrc);
              }
            }

          }
        }
        system.debug(LoggingLevel.ERROR, ' ---- OCOMPostProcessor objects to save ' + savedIdMap);

        // Vlocity Dec 27
        // Removing DR post instead calling DML directly to udpate
        Map<String, Object> updateOutput = updateOLIs(savedIdMap);
        if (updateOutput != null && updateOutput.size() > 0 && !updateOutput.isEmpty() )
          outMap.put('OliToUpdtePriceMap', updateOutput.get('OliToUpdtePriceMap'));
          system.debug('OliToUpdtePriceMap____' + updateOutput);
      }

    } catch (Exception e) {
      system.debug(LoggingLevel.ERROR, 'Exception is ' + e);
      system.debug(LoggingLevel.ERROR, 'Exception stack trace ' + e.getStackTraceString());
      //##################################################################
      //VlocityLogger.x(CLASS_NAME+'-'+ methodName);
      //##################################################################

      success = false;
      throw e;

    }

    //##################################################################
    //VlocityLogger.x(CLASS_NAME+'-'+ methodName);
    //##################################################################


    return success;
  }
  /**
     Update OLIs
     Vlocity Dec 27 2016

  */
  @TestVisible
  private Map<string, Object> updateOLIs(Map<String, Object> savedIdMap) {

    //################################################
    // construct list of OLIs that changed price by matrix
    //################################################
    Map<String, Object>updateOutput = new Map<String, Object>();
    List<SObject> itemList =  (list<SObject>)vlocity_cmt.FlowStaticMap.flowMap.get('leaningPricingItemList');

    Map<Id, Map<String, Decimal>> updatedOliToPriceMap = new Map<Id, Map<String, Decimal>>();
    Map<Id, OrderItem> tempOliToUpdate = new Map<Id, OrderItem>();
    // original implementation of SetManual and SetOverrideToZero use list
    List<Id> oliIds = new List<Id>();
    Map<Id, SOBJECT> sobjectIdToSobject = new Map<Id, SOBJECT>();
    // go through itemList
    if (itemList != null) {
      for (Sobject s : itemList) {
        // check if item is OrderItem
        if (s.getSObjectType().getDescribe().getName() == 'OrderItem') {
          OrderItem oi = (OrderItem)s;
          //orderId = oi.OrderId;
          oliIds.add(oi.Id);
          sobjectIdToSobject.put(oi.id, s);

          // compare NRC and MRC if different add to oliToUpdate list
          if (savedIdMap != null && savedIdMap.containsKey(oi.Id)) {

            //system.debug('oi.vlocity_cmt__OneTimeTotal__c' + oi.vlocity_cmt__OneTimeTotal__c);
            //system.debug('oi.vlocity_cmt__RecurringTotal__c' + oi.vlocity_cmt__RecurringTotal__c);

            if (((Decimal)((Map<String, Object>)savedIdMap.get(oi.Id)).get('NRC') != oi.vlocity_cmt__OneTimeTotal__c) ||
                ((Decimal)((Map<String, Object>)savedIdMap.get(oi.Id)).get('MRC') != oi.vlocity_cmt__RecurringTotal__c)) {
              Decimal qty = (Decimal)((Map<String, Object>)savedIdMap.get(oi.Id)).get('QTY');
              Decimal onetimeCharge = (Decimal)((Map<String, Object>)savedIdMap.get(oi.Id)).get('NRC');
              Decimal recCharge = (Decimal)((Map<String, Object>)savedIdMap.get(oi.Id)).get('MRC');
              system.debug('oi Name' + oi.id);
              Map<String, Object> outMap = new Map<String, Object>();
              outMap = getCalculatedPrice(oi, qty, onetimeCharge, recCharge);

              if (outMap != null  && outMap.size() > 0 && !outMap.isEmpty() ) {
                Decimal onetimeTotal = (Decimal)outMap.get('oneTimeTotal');
                Decimal recTotal = (Decimal)outMap.get('recTotal');
                oi.vlocity_cmt__OneTimeTotal__c = onetimeTotal;
                oi.vlocity_cmt__RecurringTotal__c = recTotal;
                tempOliToUpdate.put(oi.id, oi);
              }
              system.debug('tempOliToUpdate___' + tempOliToUpdate);

            }
          }
        }

      }
    }

    //################################################
    // call setOverRidePricesToZero and Manual Override
    //################################################

    List<OrderItem> otherOLIList = new List<OrderItem> ();
    if (oliIds != null && oliIds.size() > 0 && !oliIds.isEmpty()) {

      otherOLIList = OCOM_PricingRulesHelper.getListToUpdate(sobjectIdToSobject, oliIds, tempOliToUpdate);

      system.debug(LoggingLevel.ERROR, ' ---- otherOLIList ' + otherOLIList);
    }


    Map<Id, OrderItem> updateOLiMap = new Map<Id, OrderItem>();
    if (otherOLIList.size() > 0 && !otherOLIList.isEmpty()) {

      // Set<Id> oliIDSet = new Set<Id>();
      for (orderItem oli : otherOLIList) {
        //Set the Recurring and One time Manual DisCount
        oli.vlocity_cmt__OneTimeTotal__c  = oli.vlocity_cmt__OneTimeTotal__c != null ? (Decimal) oli.vlocity_cmt__OneTimeTotal__c : 0.00;
        oli.vlocity_cmt__RecurringTotal__c = oli.vlocity_cmt__RecurringTotal__c != null ? (Decimal) oli.vlocity_cmt__RecurringTotal__c : 0.00;
        oli.vlocity_cmt__EffectiveOneTimeTotal__c = oli.vlocity_cmt__EffectiveOneTimeTotal__c != null ? (Decimal)oli.vlocity_cmt__EffectiveOneTimeTotal__c : 0.00;
        oli.vlocity_cmt__EffectiveRecurringTotal__c = oli.vlocity_cmt__EffectiveRecurringTotal__c != null ? (Decimal)oli.vlocity_cmt__EffectiveRecurringTotal__c : 0.00;
        updateOLiMap.put(oli.id, oli);
      }
    }

    // LOGIC TO GET THE NON PRICED OLIS AND SET THE PRICE TO ZERO IF ITS NULL
    if ( itemList != null && !itemList.isEmpty() && itemList.size() > 0) {
      for (Sobject sObj : itemList) {
        // check if item is OrderItem
        if (sObj.getSObjectType().getDescribe().getName() == 'OrderItem') {
          OrderItem Oli = (OrderItem)sObj;
          String oliId = (String)sObj.get('Id');
          if ( !updateOLiMap.containsKey(oliId) && (oli.vlocity_cmt__OneTimeTotal__c == null || oli.vlocity_cmt__RecurringTotal__c == null)) {
            oli.vlocity_cmt__OneTimeTotal__c = oli.PricebookEntry.UnitPrice != null ? oli.PricebookEntry.UnitPrice : 0.00;
            oli.vlocity_cmt__RecurringTotal__c =  oli.PricebookEntry.vlocity_cmt__RecurringPrice__c != null ?  oli.PricebookEntry.vlocity_cmt__RecurringPrice__c : 0.00;
            //oli.vlocity_cmt__RecurringManualDiscount__c  = 0.00;
            //oli.vlocity_cmt__OneTimeManualDiscount__c = 0.00;
            oli.vlocity_cmt__EffectiveOneTimeTotal__c = oli.vlocity_cmt__EffectiveOneTimeTotal__c != null ? oli.vlocity_cmt__EffectiveOneTimeTotal__c : 0.00;
            oli.vlocity_cmt__EffectiveRecurringTotal__c = oli.vlocity_cmt__EffectiveRecurringTotal__c != null ? oli.vlocity_cmt__EffectiveRecurringTotal__c : 0.00;
            otherOLIList.add(oli);
          }
        }
      }
    }

    // Update all ADDED OLI Pricing
    if (otherOLIList.size() > 0 && !otherOLIList.isEmpty()) {
      try {
        update otherOLIList;
        for (orderItem oli : otherOLIList) {
          // ADD the Priced Item to the MAP To be USED IN UI to display pricing.
          Map<String, Decimal> priceMap = new Map<String, Decimal> {'MRC' => oli.vlocity_cmt__RecurringTotal__c, 'NRC' => oli.vlocity_cmt__OneTimeTotal__c };
          updatedOliToPriceMap.put(oli.id, priceMap);
        }

      } Catch(exception ex) {
        system.debug(LoggingLevel.ERROR, 'Exception is ' + ex);
        system.debug(LoggingLevel.ERROR, 'Exception stack trace ' + ex.getStackTraceString());
        throw ex;
      }

    }

    updateOutput.put('OliToUpdtePriceMap', updatedOliToPriceMap);
    return updateOutput;
  }

  public Map<String, Object> getCalculatedPrice(OrderItem oli, Decimal Qty, Decimal oneTimeCharge, Decimal recCharge) {
    Map<String, Object> outMap = new Map<String, Object>();

    Decimal otManDiscount =  oli.vlocity_cmt__OneTimeManualDiscount__c != null ? (Decimal) oli.vlocity_cmt__OneTimeManualDiscount__c : 0.00 ;
    Decimal recManDiscount =  oli.vlocity_cmt__RecurringManualDiscount__c != null ? (Decimal)oli.vlocity_cmt__RecurringManualDiscount__c : 0.00;

    Qty = (Qty != null) ? Qty : 1;
    oneTimeCharge = (oneTimeCharge != null) ? oneTimeCharge : Decimal.valueOf(100000000); // Default price is expensive.
    recCharge = (recCharge != null) ? recCharge : 0.0;

    otManDiscount = (otManDiscount != null) ? otManDiscount : 0.0;
    recManDiscount = (recManDiscount != null) ? recManDiscount : 0.0;

    Decimal oneTimeComputePrice = oneTimeCharge - oneTimeCharge * otManDiscount / 100;
    Decimal oneTimeTotal = onetimeComputePrice * Qty;

    Decimal recComputePrice = recCharge - recCharge * recManDiscount / 100;
    Decimal recTotal = recComputePrice * Qty;

    outMap.put('recTotal', recTotal);
    outMap.put('oneTimeTotal', oneTimeTotal);
    return outMap;
  }



}