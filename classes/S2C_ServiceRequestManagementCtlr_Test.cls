/**
 * Test class for S2C_ServiceRequestManagementCtlr
 *
 * @author Thomas Tran, Merisha Shim, Traction on Demand
 * @date 03-15-2015
 */
@isTest(seeAllData=false)
private class S2C_ServiceRequestManagementCtlr_Test {
    
    @isTest static void saveRecord_Test() {
        String resultId='';
        Contact newContact=null;
        Contracts__c newContract=null;
        System.runAs(new User(Id = UserInfo.getUserId())){
            insert S2C_TestUtility.createOrderType();

            Account newAccount = S2C_TestUtility.createAccount('Test Account');
            insert newAccount;

            Opportunity newOpportunity = S2C_TestUtility.createOpportunity(newAccount.Id, 'Test Opportunity');
            newOpportunity.CloseDate = Date.today();
            newOpportunity.StageName = S2C_TestUtility.ORIGINATED_STAGE_NAME;
            insert newOpportunity;

            newContact = S2C_TestUtility.createContact('Test');
            insert newContact;

            OpportunityContactRole newOpportunityContactRole = S2C_TestUtility.createOpportunityContactRole(newOpportunity.Id, newContact.Id, true);
            insert newOpportunityContactRole;

            Product2 newProduct2 = S2C_TestUtility.createProduct('Test Product', 'Test');
            insert newProduct2;

            Opp_Product_Item__c newOppProductItem1 = S2C_TestUtility.createOpportunityProductItem(newProduct2.Id, newOpportunity.Id);
            insert newOppProductItem1;

            SRS_PODS_Product__c newSRSPodsProduct = S2C_TestUtility.createSRSPODSProduct(newProduct2.Product_Family__c);
            insert newSRSPodsProduct;

            //S2C_OrderCreation.OpportunityProductItemInfo newOpiItem1 = new S2C_OrderCreation.OpportunityProductItemInfo(newOppProductItem1.Id, newOppProductItem1.Name, 'Prequal', newSRSPodsProduct.Id, '12', Date.valueOf('2015-01-01'));
            S2C_OrderCreation.OpportunityProductItemInfo newOpiItem1 = new S2C_OrderCreation.OpportunityProductItemInfo(newOppProductItem1.Id, newOppProductItem1.Name, 'Prequal', 'Prequal', '12', Date.valueOf('2015-01-01'),'True');
            List<S2C_OrderCreation.OpportunityProductItemInfo> opiInfoList = new List<S2C_OrderCreation.OpportunityProductItemInfo>();
            opiInfoList.add(newOpiItem1);

            String resultString = S2C_OrderCreation.createOrder(newOpportunity.Id, JSON.serialize(opiInfoList));

            resultId = resultString.remove('/');
            
            id contractRecordTypeId;
            list<recordtype> contractRecordTypes=[select id,developername from recordtype where sobjecttype='Contracts__c'];
            
            for(recordtype rt:contractRecordTypes){
                if(rt.developername.equalsignorecase('eContract')){
                    contractRecordTypeId=rt.id;
                }else if(rt.developername.equalsignorecase('legacy')){
                    contractRecordTypeId=rt.id;
                }
            }
            
            

            newContract = new Contracts__c(Opportunity__c = resultId, Type_of_Contract__c = 'New',recordtypeid = contractRecordTypeId );
            insert newContract;
            system.debug('newContract:' + newContract);

            List<Service_Request__c> srList = [
                SELECT Contract__c
                FROM Service_Request__c
                WHERE Opportunity__c = :resultId
            ];
            system.debug('SRList:' + srList);
            
            SMBCare_Address__c newSMBAddress = new SMBCare_Address__c(
                Account__c = newAccount.Id,
                Province__c = 'BC'
            );

            insert newSMBAddress;

            SRS_Service_Address__c newServiceAddress = new SRS_Service_Address__c(
                Address__c = newSMBAddress.Id,
                Service_Request__c = srList[0].Id
            );
        
            insert newServiceAddress;

            //srList[0].Service_Request_Status__c = 'Completed';

            //update srList[0]; 

            
        }
        if(string.isnotblank(resultId)&&newContract!=null){
            //System.runAs(new User(Id = UserInfo.getUserId())){
            PageReference pageRef = Page.S2C_ServiceRequestManagement;
            pageRef.getParameters().put('oppId', resultId);
            pageRef.getParameters().put('contractId', newContract.Id);
            Test.setCurrentPage(pageRef);
            Test.startTest();
            S2C_ServiceRequestManagementCtlr ctlr = new S2C_ServiceRequestManagementCtlr();
            if(ctlr.relatedSRList!=null&&!ctlr.relatedSRList.isempty()){
                ctlr.relatedSRList[0].isAssigned = true;
                ctlr.relatedSRList[0].serviceRequestStatus = 'Completed';
                //Test.startTest();
                ctlr.saveRecords();
                ctlr.closeWindow();
                Test.stopTest();
            }
            //}
        }
    }
    @isTest static void saveRecordError_Test() {
        
        
        System.runAs(new User(Id = UserInfo.getUserId())){
            insert S2C_TestUtility.createOrderType();

            Account newAccount = S2C_TestUtility.createAccount('Test Account');
            insert newAccount;

            Opportunity newOpportunity = S2C_TestUtility.createOpportunity(newAccount.Id, 'Test Opportunity');
            newOpportunity.CloseDate = Date.today();
            newOpportunity.StageName = S2C_TestUtility.ORIGINATED_STAGE_NAME;
            insert newOpportunity;

            Contact newContact = S2C_TestUtility.createContact('Test');
            insert newContact;

            OpportunityContactRole newOpportunityContactRole = S2C_TestUtility.createOpportunityContactRole(newOpportunity.Id, newContact.Id, true);
            insert newOpportunityContactRole;

            Product2 newProduct2 = S2C_TestUtility.createProduct('Test Product', 'Test');
            insert newProduct2;

            Opp_Product_Item__c newOppProductItem1 = S2C_TestUtility.createOpportunityProductItem(newProduct2.Id, newOpportunity.Id);
            insert newOppProductItem1;

            SRS_PODS_Product__c newSRSPodsProduct = S2C_TestUtility.createSRSPODSProduct(newProduct2.Product_Family__c);
            insert newSRSPodsProduct;

            //S2C_OrderCreation.OpportunityProductItemInfo newOpiItem1 = new S2C_OrderCreation.OpportunityProductItemInfo(newOppProductItem1.Id, newOppProductItem1.Name, 'Prequal', newSRSPodsProduct.Id, '12', Date.valueOf('2015-01-01'));
            S2C_OrderCreation.OpportunityProductItemInfo newOpiItem1 = new S2C_OrderCreation.OpportunityProductItemInfo(newOppProductItem1.Id, newOppProductItem1.Name, 'Prequal', 'Prequal', '12', Date.valueOf('2015-01-01'),'True');
            List<S2C_OrderCreation.OpportunityProductItemInfo> opiInfoList = new List<S2C_OrderCreation.OpportunityProductItemInfo>();
            opiInfoList.add(newOpiItem1);

            String resultString = S2C_OrderCreation.createOrder(newOpportunity.Id, JSON.serialize(opiInfoList));

            String resultId = resultString.remove('/');
            
            id contractRecordTypeId;
            list<recordtype> contractRecordTypes=[select id,developername from recordtype where sobjecttype='Contracts__c'];
            
            for(recordtype rt:contractRecordTypes){
                if(rt.developername.equalsignorecase('eContract')){
                    contractRecordTypeId=rt.id;
                }else if(rt.developername.equalsignorecase('legacy')){
                    contractRecordTypeId=rt.id;
                }
            }
            
            
            

            Contracts__c newContract = new Contracts__c(Opportunity__c = resultId, Type_of_Contract__c = 'New',recordtypeid = contractRecordTypeId );
            insert newContract;
            
            
            List<Service_Request__c> srList = [
                SELECT Contract__c
                FROM Service_Request__c
                WHERE Opportunity__c = :resultId
            ];
            
            PageReference pageRef = Page.S2C_ServiceRequestManagement;
            pageRef.getParameters().put('oppId', resultId);
            pageRef.getParameters().put('contractId', newContract.Id);
            Test.setCurrentPage(pageRef);
            
            S2C_ServiceRequestManagementCtlr ctlr = new S2C_ServiceRequestManagementCtlr();
            if(ctlr.relatedSRList!=null&&!ctlr.relatedSRList.isempty()){
                Service_Request__c tempSR = [SELECT Account_Name__c, Opportunity__c, PrimaryContact__c, Opportunity_Product_Item__c FROM Service_Request__c WHERE Id = :ctlr.relatedSRList[0].serviceRequest.Id];
                
                Service_Request__c newServiceRequest = new Service_Request__c(
                    Account_Name__c = tempSR.Account_Name__c,
                    Opportunity__c = tempSR.Opportunity__c,
                    //OwnerId = orderOpportunity.OwnerId,
                    PrimaryContact__c = tempSR.PrimaryContact__c,
                    Opportunity_Product_Item__c = tempSR.Opportunity_Product_Item__c,
                    Service_Request_Status__c = 'Completed'
                );
                
                //insert newServiceRequest;
                
                S2C_ServiceRequestManagementCtlr.ServiceRequest newSRM = new S2C_ServiceRequestManagementCtlr.ServiceRequest(newServiceRequest);
                system.debug('newSRM:' + newSRM);
                ctlr.relatedSRList.add(newSRM);
                
                
                ctlr.relatedSRList[0].isAssigned = true;
                ctlr.relatedSRList[0].serviceRequestStatus = 'Completed';
                
                Service_Request__c srTemp = ctlr.relatedSRList[0].serviceRequest;
                srTemp.Service_Request_Status__c = 'Completed';
                update srTemp;
                
                
                Test.startTest();
                ctlr.saveRecords();
                ctlr.closeWindow();
                Test.stopTest();
            }
        }

    }
}