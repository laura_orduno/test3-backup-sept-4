/*******************************************************************************
     * Project      : OCOM
     * Helper       : Order_PAC_Address_Controller
     * Test Class   : Order_PAC_Address_Controller_Test
     * Description  : Test class for Order_PAC_Address_Controller(Apex class written by Arvind to Update Order Addresses.)
     * Author       : Arvind Yadav
     * Date         : February-10-2016
     *
     * Modification Log:
     * ------------------------------------------------------------------------------
     * Ver      Date                Modified By           Description
     * ------------------------------------------------------------------------------
     * 1.0     
     
     *******************************************************************************/
@isTest
private class Order_PAC_Address_Controller_Test 
{
     static testMethod void OrderPacUnitTest() {
        // TO DO: implement unit test
      
        
        
        Account AcctRCID = new Account(Name='Test Account', RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId());
        insert AcctRCID;
        
        Account Acct = new Account(Name='Test Account', ParentId=AcctRCID.Id);
        insert Acct;
        
        Opportunity Opp= new Opportunity(Name='Test Opp',StageName='Discovery',accountId=AcctRCID.Id, CloseDate=System.today().addDays(5));
        insert Opp; 
        
        
        Quote Qt= new Quote(Name='Test Quote11',opportunityid=opp.id, ExpirationDate=System.today().addDays(5));
        insert Qt;
        
        Id BussRecordTypeId= Schema.SObjectType.Account.getRecordTypeInfosByName().get('Billing').getRecordTypeId();
        Account AcctBilling = new Account(ParentId=AcctRCID.Id,Name='Draft-000001', Ban__c='Ban 2', Can__c= 'Can 2', Ban_Can__c='BanCan4321', Billingstreet='Street1', RecordTypeId=BussRecordTypeId, Billing_Account_Active_Indicator__c='Y');
        insert AcctBilling ;

        Contract Ct = new Contract(AccountId = Acct.Id, Status = 'Draft',StartDate = Date.Today(), ContractTerm = 12);
        Insert Ct;
        
        Id standardPriceBookId = Test.getStandardPriceBookId();
        
        SMBCare_Address__c smbCareAddr = new SMBCare_Address__c(Account__c=Acct.Id, Postal_Code__c='M1T3N3', Street_Number__c='Street 1', Street_Name__c='Street Name 1', Province__c='ONT', Suite_Number__c='1001', COID__c='COID 1', Building_Number__c='Building 1', Country__c='CAN', FMS_Address_ID__c='FMSID 121');
        insert smbCareAddr;
        
        SMBCare_Address__c smbCareAddr1 = new SMBCare_Address__c(Account__c=AcctRCID.Id, Postal_Code__c='M1T3N3', Street_Number__c='Street 1', Street_Name__c='Street Name 1', Province__c='ONT', Suite_Number__c='1001', COID__c='COID 1', Building_Number__c='Building 1', Country__c='CAN', FMS_Address_ID__c='FMSID2121');
        
        SMBCare_Address__c smbCareAddr2 = new SMBCare_Address__c(Account__c=AcctRCID.Id, Postal_Code__c='M1T3N3', Street_Number__c='Street 1', Street_Name__c='Street Name 1', Province__c='ONT', Suite_Number__c='1001', COID__c='COID 1', Building_Number__c='Building 1', Country__c='CAN', FMS_Address_ID__c='FMSID212122');
        SMBCare_Address__c smbCareAddr3 = new SMBCare_Address__c(Account__c=AcctRCID.Id, Postal_Code__c='M1T3N3', Street_Number__c='Street 1', Street_Name__c='Street Name 1', Province__c='ONT', Suite_Number__c='1001', COID__c='COID 1', Building_Number__c='Building 1', Country__c='CAN', FMS_Address_ID__c='FMSID212122');
        
        Order Od = new Order(Name = 'ORD-000001', Service_Address__c = smbCareAddr.Id, Shipping_Address__c='2010 100 AV NW EDMONTON AB T5A3M1', Service_Address_Text__c='2010 100 AV NW EDMONTON AB T5A3M1' , FMS_Address_ID__c='FMSID 121', ShippingStreet='2010 100 AV', ShippingCity='EDMONTON', BAN__c='Ban 21' , PriceBook2Id =standardPriceBookId  , AccountId = Acct.Id, EffectiveDate = Date.Today(), ContractId = Ct.Id, Status = 'Draft', vlocity_cmt__QuoteId__c=Qt.Id);
        Insert Od;
        
                
        Product2 Pd = new Product2(Name='Porduct 1',isActive=true);
        insert Pd;

       
        PricebookEntry Pbe = new PricebookEntry(Pricebook2Id=standardPriceBookId, Product2Id=pd.Id, UnitPrice=99, isActive=true);
        insert Pbe;

        OrderItem ordPd = new OrderItem(PriceBookEntryId=Pbe.Id, OrderId=Od.Id, Quantity=1, UnitPrice=99);
        insert ordPd;
        Test.StartTest();
        PageReference pf = Page.Order_PAC_Address;
        Test.setCurrentPage(pf);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(Od);
        //sc.save();
        ApexPages.currentPage().getParameters().put('id',Od.id);
               
        Order_PAC_Address_Controller OrderPacController = new Order_PAC_Address_Controller(sc);
        OrderPacController.selectedAccId = AcctBilling.id;
      
        OrderPacController.SaveShipping();
        OrderPacController.SaveBillingAccount();
        OrderPacController.SaveBillingOnSelect();
        OrderPacController.SaveBillingOnSelect();
        OrderPacController.UpdateBlnPopup();
        OrderPacController.getFormTag();
        OrderPacController.getTextBox(); 
        OrderPacController.search();
        OrderPacController.SaveService();
        ServiceAddressData saData = new ServiceAddressData();
        saData.streetTypeSuffix='tst';
        saData.municipalityNameLPDS='tst';
        saData.rateCenter='tst';
        saData.dropFacilityCode='tst';
        saData.dropTypeCode='tst';
        saData.dropLength='tst';
        saData.dropExceptionCode='tst';
        saData.currentTransportTypeCode='tst';
        saData.futureTransportTypeCode='tst';
        saData.futurePlantReadyDate='tst';
        saData.futureTransportRemarkTypeCode='tst';
        saData.gponBuildTypeCode='tst';
        saData.provisioningSystemCode='tst';
        OCOM_ServiceAddressControllerHelper saControllerData = new OCOM_ServiceAddressControllerHelper();
        saData.fmsId='FMSID2121';
        saControllerData.UpdateAddressAndQuote(qt.Id,smbCareAddr1,saData);
        
        saControllerData.UpdateAddressAndQuote(qt.Id,smbCareAddr3,saData);
        saData.isManualCapture=true;
        //saData.fmsId='FMSID 121';

        saControllerData.UpdateAddressAndOrder(od.Id,smbCareAddr2,saData);
        Test.StopTest();
    }

}