public with sharing class CheckRecursion {
    private static Boolean runBeforeInsert = true;
    private static Boolean runAfterInsert = true;
    private static Boolean runBeforeUpdate = true;
    private static Boolean runAfterUpdate = true;
    
    public static boolean runOnceBeforeInsert(){
        if(runBeforeInsert) {
            runBeforeInsert=false;
            return true;
        }  else  {
            return runBeforeInsert;
        }
     }  
     
     public static boolean runOnceAfterInsert(){
        if(runAfterInsert) {
            runAfterInsert=false;
            return true;
        }  else  {
            return runAfterInsert;
        }
     }
     
     public static boolean runOnceBeforeUpdate(){
        if(runBeforeUpdate) {
            runBeforeUpdate=false;
            return true;
        }  else  {
            return runBeforeUpdate;
        }
     }
     
     public static boolean runOnceAfterUpdate(){
        if(runAfterUpdate) {
            runAfterUpdate=false;
            return true;
        }  else  {
            return runAfterUpdate;
        }
     }
}