public class TBOAccCategorizationController {
    
    public Id caseId{get;set;}
    public integer total{get;set;}
    public integer pagesize{get;set;}{pagesize=20;}
    public String showRecordsSize{get;set;}
  
    //public ApexPages.StandardSetController setCon {get;set;}
    public TBOAccCategorizationController() {
        this.caseId = apexpages.currentpage().getparameters().get('Id');
        system.debug('>>> Id from cat ctrl: '+caseId);
        
    }
     /*public TBOAccCategorizationController(apexpages.StandardSetController controller) {
        this.setcon=controller;
        //this.caseId = apexpages.currentpage().getparameters().get('Id');
        
    }*/
    
    public ApexPages.StandardSetController setCon{
    get {
            if(setCon == null) {
            setCon = new ApexPages.StandardSetController(Database.getQueryLocator(
            [SELECT Id,name, CAN__C, BTN__C,BCAN_Pilot_Org_Indicator__c, Service_type__c,Acc_Current__c,Dir_Adv__c,Billing_System__c  from Case_Account__c where case__c=:caseId and BCAN_Pilot_Org_Indicator__c!=1]));
            setcon.setpagesize(pagesize);
            total=setcon.getResultSize();


            }
            return setCon;
        }
     set;
    }
    
    public List<Case_Account__c> getCaseAccounts() {
        showRecordsSize= generateString();
        system.debug('>>> Id from cat ctrl: '+caseId);
        return (List<Case_Account__c>) setcon.getRecords();
        
    }
    
    public pagereference save(){
        
        pagereference pg ;
      if(setCon != null) {
           pg= setcon.save();
          
           ///system.debug('>>> before pagereference: '+pg);
           //pg= new pagereference(''+System.URL.getSalesforceBaseURL().toExternalForm()+'/'+caseId);
           //system.debug('>>> After pagereference: '+pg);
           //pg.setRedirect(true);
           //return pg;
       }
      // cancel();
     
       return null;
            
    }
    
    public pagereference Cancel(){
        pagereference pg= new pagereference(''+System.URL.getSalesforceBaseURL().toExternalForm()+'/'+caseId);
        return pg;
        //return null;
    }
    
    public void first() {
        if(setcon!=null){
           save();
            setcon.first();
           
        }
        
        
    }
    
    // returns the last page of records
    public void last() {
        if(setcon!=null){
            save();
            setcon.last();
           
        }
    }
    
    // returns the previous page of records
    public void previous() {
        if(setcon!=null){ 
            save();
            setcon.previous();
        }
    }
    
    // returns the next page of records
    public void next() {
        if(setcon!=null){
            save();
            setcon.next();
           }
    }    
     public Boolean hasNext {
         get {
             if(setCon!=null) system.debug('>>> From HasNext'+setCon.getHasNext());
             if(setCon!=null)
                 return setCon.getHasNext();
             else
                 return false;
         }
         set;
     }
    
    // indicates whether there are more records before the current page set.
    public Boolean hasPrevious {
        get {
            if(setCon!=null)
                return setCon.getHasPrevious();
            else
                return false;
        }
        set;
    }
    public string generateString(){
        if(setcon!=null && total>0){
            boolean lastpage= setCon.getHasNext();
            if(setcon.getPagenumber()*pagesize > total)
                return 'Showing '+(setcon.getPagenumber()*pagesize+1-pagesize)+'-'+total+' of '+ total+'';
            else
                return 'Showing '+(setcon.getPagenumber()*pagesize+1-pagesize)+'-'+(setcon.getPagenumber()*pagesize)+' of '+ total+'';
             
        }
        else if(setcon!=null && total<=0)
                return  'Showing 0 of 0';
        else
            return  'Showing 0 of 0';                       
    }
    
    
}