global with sharing class smb_CreateRemarkControllerExtension {
    
    @TestVisible
    private Task taskRecord;
    @TestVisible
    public final Supplemental_Activity_Data__c taskExtend {get;set;}
    public String taskFields { get; private set;}
    public String taskExtendFields { get; private set; }
    
    public Boolean isCallCentreUser {get;set;}
    public Boolean isSaved {get;set;}
    
    public static String callServiceTypeFld {get;set;}
    public static String callDispositionFld {get;set;}
    public static String callTypeFld {get;set;}
    public static String callTopicFld {get;set;}
    public static string userlanguage=Userinfo.getLanguage();
    
    // keyStr format is "servicetype[-disposition[-calltype[-calltopic]]]"
    public static map<string,string> pickListHTMLFormatted {get;}
    public static string pickListHTMLFormattedJSON {get;}
    
    public final static map<string,string> serviceTypeJSONPickListMap;
    public static string serviceTypeListJSON {get;}
    
    private final static String STATUS_SUBMIT = 'SUBMITTING REMARK';
    private final static String STATUS_GENERATE = 'GENERATING REMARK';
    
    static {
        serviceTypeJSONPickListMap = new map<string,string>();
        loadPicklistData();
    }
    
    public smb_CreateRemarkControllerExtension(ApexPages.StandardController stdController) {
        this.taskRecord = (Task)stdController.getRecord();
        this.taskExtend = new Supplemental_Activity_Data__c();
        initialize();
    }
    
    public void saveRecord() {
        if (validateLnR(STATUS_SUBMIT)) {
            isSaved = true;
            try {
                integer descLength = Schema.SObjectType.Task.fields.description.getLength();
                integer trackerLength = Schema.SObjectType.Task.fields.Call_Topic_Field__c.getLength();
                
                if(taskRecord.description != null) {
                    string trec = taskRecord.description;
                    taskRecord.description = trec.abbreviate(descLength);
                }
                if(callServiceTypeFld != null)
                    taskRecord.Call_Service_Type_Field__c = callServiceTypeFld.abbreviate(trackerLength);
                if(callDispositionFld != null)
                    taskRecord.Call_Disposition_Field__c = callDispositionFld.abbreviate(trackerLength);
                if(callTypeFld != null)
                    taskRecord.Call_Type_Field__c = callTypeFld.abbreviate(trackerLength);
                if(callTopicFld != null)
                    taskRecord.Call_Topic_Field__c = callTopicFld.abbreviate(trackerLength);
                
                if(callServiceTypeFld == '' || callDispositionFld == '' || callTypeFld == '' || callTopicFld == '') {
                    isSaved = false;
                }
                
                if(isSaved) {
                    upsert taskExtend;
                    
                    taskRecord.Supplemental_Activity_Data__c = taskExtend.Id;
                    upsert taskRecord;
                }
            }
            catch (System.Exception ex) {
                isSaved = false;
                throw ex;
            }
        }
    }
    
    /**
* Custom form validation for the LnR fields.
*
* @author Matt Freedman, Traction on Demand
* @date 2016-05-27
* @param callingState - to determine when validateLnR is called (either "SUBMITTING_REMARK" or "GENERATING_TEMPLATE")
*/
    public Boolean validateLnR(String callingState) {
        
        if (taskRecord.Call_Service_Type_Picklist__c != 'lnr wireline' && taskRecord.Call_Service_Type_Picklist__c != 'lnr wireless' &&
            taskRecord.Call_Service_Type_Picklist__c != 'lnr business connect' && taskRecord.Call_Service_Type_Picklist__c != 'lnr business anywhere') {
                return true;
            }
        
        List<String> errors = new List<String>();
        
        if (String.isBlank(taskRecord.Product_Subcategory__c)) {
            errors.add(Task.Product_Subcategory__c.getDescribe().getLabel() + ' is required');
        }
        //adding 3 validation rules
        // see lines #90 to #104 Brian L
        
        if (String.isNotBlank(callingState) && callingState != STATUS_GENERATE) {            
            if (taskRecord.Deactivation_Reason__c == 'Other') {
                if (String.isBlank(taskRecord.Description)) {
                    errors.add(Task.Description.getDescribe().getLabel() + ' is required');
                }
            }    
            if (taskRecord.Competitive_Threat__c == 'Other') {
                if (String.isBlank(taskRecord.Description)) {
                    errors.add(Task.Description.getDescribe().getLabel() + ' is required');
                }
            }
        }
        
        if (String.isNotBlank(taskRecord.Product_Subcategory__c)  && taskRecord.Call_Service_Type_Picklist__c == 'lnr wireless') {
            if (String.isBlank(String.Valueof(taskRecord.Ban__c))) {
                errors.add(Task.Ban__c.getDescribe().getLabel() + ' is required');
            }
        }
        if (String.isBlank(taskRecord.Call_Received_From__c)) {
            errors.add(Task.Call_Received_From__c.getDescribe().getLabel() + ' is required');
        }
        
        if (String.isBlank(taskRecord.Dealer_Direct_Sales_Detail__c)) {
            errors.add(Task.Dealer_Direct_Sales_Detail__c.getDescribe().getLabel() + ' is required');
        }
        
        if (String.isBlank(taskRecord.Request_Type__c)) {
            errors.add(Task.Request_Type__c.getDescribe().getLabel() + ' is required');
        }
        
        if (taskRecord.Request_Type__c == 'competitive threat') {
            if (String.isBlank(taskRecord.Competitive_Threat__c)) {
                errors.add(Task.Competitive_Threat__c.getDescribe().getLabel() + ' is required');
            }
            
            if (String.isBlank(taskRecord.Competitor_Name__c)) {
                errors.add(Task.Competitor_Name__c.getDescribe().getLabel() + ' is required');
            }
            else {
                if (taskRecord.Competitor_Name__c.containsIgnoreCase('other') && String.isBlank(taskRecord.Competitor_Name_Other__c)) {
                    errors.add(Task.Competitor_Name_Other__c.getDescribe().getLabel() + ' is required');   
                }
            }
            
            if (taskExtend.Comp_rate_plan_cost__c == null) {
                errors.add(Supplemental_Activity_Data__c.Comp_rate_plan_cost__c.getDescribe().getLabel() + ' is required');
            }
            
            if (taskExtend.Current_TLC__c == null) {
                errors.add(Supplemental_Activity_Data__c.Current_TLC__c.getDescribe().getLabel() + ' is required');
            }
            if (String.isBlank(taskExtend.Plan_Offered_SOC__c)) {
                errors.add(Supplemental_Activity_Data__c.Plan_Offered_SOC__c.getDescribe().getLabel() + ' is required');
            }            
        }
        
        if (taskRecord.Request_Type__c == 'soft shopping - no specific competitor') {
            if (String.isBlank(taskRecord.Soft_Shopper_No_Specific_Competitor__c)) {
                errors.add(Task.Soft_Shopper_No_Specific_Competitor__c.getDescribe().getLabel() + ' is required');
            }
        }
        if (taskRecord.Request_Type__c == 'escalation') {
            if (String.isBlank(taskRecord.Escalation_Reason__c)) {
                errors.add(Task.Escalation_Reason__c.getDescribe().getLabel() + ' is required');
            }
        }
        if (taskRecord.Request_Type__c == 'deactivation (cancel)') {
            if (String.isBlank(taskRecord.Deactivation_Reason__c)) {
                errors.add(Task.Deactivation_Reason__c.getDescribe().getLabel() + ' is required');
            }
        }
        if (taskRecord.Request_Type__c == 'general inquiry') {
            if (String.isBlank(taskRecord.General_Inquiry__c)) {
                errors.add(Task.General_Inquiry__c.getDescribe().getLabel() + ' is required');
            }
        }
        if (taskRecord.Request_Type__c == 'misdirect') {
            if (String.isBlank(taskRecord.Misdirect_Reason__c)) {
                errors.add(Task.Misdirect_Reason__c.getDescribe().getLabel() + ' is required');
            }
        }
        
        if (taskRecord.Call_Service_Type_Picklist__c== 'lnr wireless' || taskRecord.Call_Service_Type_Picklist__c == 'lnr business anywhere') {
            if (taskRecord.Request_Type__c == 'competitive threat') {
                if (taskExtend.Comp_port_credit__c == null) {
                    errors.add(Supplemental_Activity_Data__c.Comp_port_credit__c.getDescribe().getLabel() + ' is required');
                }            
            }        
        }
        if (taskRecord.Call_Service_Type_Picklist__c== 'lnr wireline' || taskRecord.Call_Service_Type_Picklist__c == 'lnr business anywhere') {
            if (String.isBlank(taskRecord.Offer_Duration_WLN_Business_Anywhere__c)) {
                errors.add(Task.Offer_Duration_WLN_Business_Anywhere__c.getDescribe().getLabel() + ' is required');
            }   
        }
        
        if (taskRecord.Request_Type__c == 'competitive threat' || taskRecord.Request_Type__c == 'escalation'
            || taskRecord.Request_Type__c == 'soft shopping - no specific competitor' || taskRecord.Request_Type__c == 'deactivation (cancel)') {
                if (String.isBlank(taskRecord.Save_Tools_Used__c)) {
                    errors.add(Task.Save_Tools_Used__c.getDescribe().getLabel() + ' is required');
                }
                else {
                    if (taskRecord.Save_Tools_Used__c.containsIgnoreCase('other') && String.isBlank(taskRecord.Save_Tools_Used_Other__c)) {
                        errors.add(Task.Save_Tools_Used_Other__c.getDescribe().getLabel() + ' is required');
                    }
                }
            }
        
        if (String.isBlank(taskRecord.Client_Region__c)) {
            errors.add(Task.Client_Region__c.getDescribe().getLabel() + ' is required');
        }
        
        if (String.isBlank(taskRecord.Churn_Threat_Gauge__c)) {
            errors.add(Task.Churn_Threat_Gauge__c.getDescribe().getLabel() + ' is required');
        }
        
        if (String.isBlank(taskRecord.Offer_Status__c)) {
            errors.add(Task.Offer_Status__c.getDescribe().getLabel() + ' is required');
        }
        
        if (taskRecord.No_Followup_Required__c == false && taskRecord.ActivityDate == null) {
            errors.add(Task.No_Followup_Required__c.getDescribe().getLabel() + ' or Offer Followup Schedule must be selected');
        }
        
        
        if (taskRecord.Call_Received_From__c == 'corporate stores' || taskRecord.Call_Received_From__c == 'direct sales' || taskRecord.Call_Received_From__c == 'Dealer Direct (Business Excellence)' || taskRecord.Call_Received_From__c == 'Dealer Direct (Non-Business Excellence)' || taskRecord.Call_Received_From__c == 'Dealer via Business Channel Care' || taskRecord.Call_Received_From__c == 'dealer direct') {
            if (String.isBlank(taskExtend.Dealer_Code__c)) {
                errors.add(Supplemental_Activity_Data__c.Dealer_Code__c.getDescribe().getLabel() + ' is required');
            }
            if (String.isBlank(taskExtend.Sales_Rep_Code__c)) {
                errors.add(Supplemental_Activity_Data__c.Sales_Rep_Code__c.getDescribe().getLabel() + ' is required');
            }
        }
        
        if (taskRecord.Request_Type__c == 'mid contract RPO') {
            if (String.isBlank(taskExtend.Mid_Contract_RPO_Reason__c)) {
                errors.add(Supplemental_Activity_Data__c.Mid_Contract_RPO_Reason__c.getDescribe().getLabel() + ' is required');
            }
            if (String.isBlank(taskExtend.Plan_Name_SOC__c)) {
                errors.add(Supplemental_Activity_Data__c.Plan_Name_SOC__c.getDescribe().getLabel() + ' is required');
            }
        }
        
        if (taskRecord.Request_Type__c == 'hardware exception') {
            if (String.isBlank(taskExtend.Device_Model__c)) {
                errors.add(Supplemental_Activity_Data__c.Device_Model__c.getDescribe().getLabel() + ' is required');
            }
            if (taskExtend.Renewal_Credit_Amount__c == null) {
                errors.add(Supplemental_Activity_Data__c.Renewal_Credit_Amount__c.getDescribe().getLabel() + ' is required');
            }
        }
        
        if (taskRecord.Request_Type__c == 'device balance exception') {
            if (String.isBlank(taskExtend.DB_Exception_reason__c)) {
                errors.add(Supplemental_Activity_Data__c.DB_Exception_reason__c.getDescribe().getLabel() + ' is required');
            }
            if (String.isBlank(taskExtend.DB_Waived__c)) {
                errors.add(Supplemental_Activity_Data__c.DB_Waived__c.getDescribe().getLabel() + ' is required');
            }
            if (taskExtend.Month_Remaining__c == null) {
                errors.add(Supplemental_Activity_Data__c.Month_Remaining__c.getDescribe().getLabel() + ' is required');
            }            
        }
        
        if (String.isBlank(taskExtend.Callback_Number__c)) {
            errors.add(Supplemental_Activity_Data__c.Callback_Number__c.getDescribe().getLabel() + ' is required');
        }
        if (String.isBlank(taskExtend.Service_Phone_Number_Worked_On__c)) {
            errors.add(Supplemental_Activity_Data__c.Service_Phone_Number_Worked_On__c.getDescribe().getLabel() + ' is required');
        }
        if (String.isBlank(taskExtend.Client_Tenure__c)) {
            errors.add(Supplemental_Activity_Data__c.Client_Tenure__c.getDescribe().getLabel() + ' is required');
        }
        
        
        if (errors.size() == 0) {
            return true;
        }
        else {
            for (String err : errors) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, err));
            }
            
            return false;
        }
        
    }
    
    private void initialize() {
        isCallCentreUser = getCurrentUserCallCentreStatus();
        
        taskRecord.RecordTypeId = getRecordType();
        taskRecord.WhatId = getAccountId();
        taskRecord.Status = 'Completed';
        taskRecord.ActivityDate = Date.today();
        
        isSaved = false;
    }
    
    @TestVisible
    private static Boolean getCurrentUserCallCentreStatus() {
        User user = [SELECT CallCenterId FROM User WHERE Id = :UserInfo.getUserId()];
        return (user.CallCenterId != null);
    }
    
    @TestVisible
    private static Id getAccountId() {
        return ApexPages.currentPage().getParameters().get('acctId');
    }
    
    @TestVisible
    private Id getRecordType() {
        String recordTypeName = 'SMB_Care_Remark';
        
        if (!isCallCentreUser) {
            recordTypeName = 'SMB_Care_Call_Tracker_Non_CTI';
        }
        
        List<RecordType> RecordTypes = [SELECT Id 
                                        FROM RecordType 
                                        WHERE DeveloperName = :recordTypeName
                                        AND SobjectType = 'Task'];
        
        if (RecordTypes.size() == 1) {
            return RecordTypes.get(0).Id;
        }
        
        return null;
    }
    
    private static void loadPicklistData() {
        List<Call_Tracker_Picklist_Data__c> picklistValues = new List<Call_Tracker_Picklist_Data__c>();
        pickListHTMLFormatted = new map<string,string>();
        list<string> serviceTypeList = new list<string>();
        //Added for Call Tracker
        if (userlanguage =='fr'){
            picklistValues =
                [SELECT name, Service_Type__c, Picklist_Values__c, Picklist_HTML__c
                 FROM Call_Tracker_Picklist_Data__c where Language__c IN('' ,'fr') ORDER BY Service_Type__c DESC];
            //FROM Call_Tracker_Picklist_Data__c ORDER BY Service_Type__c DESC];
        }
        else{
            picklistValues =
                [SELECT name, Service_Type__c, Picklist_Values__c, Picklist_HTML__c
                 FROM Call_Tracker_Picklist_Data__c where Language__c IN('' , 'en_us') ORDER BY Service_Type__c DESC];
            // FROM Call_Tracker_Picklist_Data__c ORDER BY Service_Type__c DESC];
        }
        for(Call_Tracker_Picklist_Data__c trkData : picklistValues) {
            serviceTypeJSONPickListMap.put(trkData.Service_Type__c,trkData.Picklist_HTML__c);
            serviceTypeList.add(trkData.Service_Type__c);
            
            if(trkData.Picklist_HTML__c != null) {
                pickListHTMLFormatted.putAll((map<string,string>) JSON.deserialize(trkData.Picklist_HTML__c,map<string,string>.class));
            }
        }
        
        pickListHTMLFormattedJSON = JSON.serialize(pickListHTMLFormatted);
        serviceTypeListJSON = JSON.serialize(serviceTypeList);
    }
    
    webService static string getPicklistHTML(string serviceType) {
        
        List<Call_Tracker_Picklist_Data__c> picklistValues =
            [SELECT name, Service_Type__c, Picklist_Values__c, Picklist_HTML__c
             FROM Call_Tracker_Picklist_Data__c where Service_Type__c = :serviceType limit 1];
        
        string errorStr;
        string messageStr;
        map<string,set<string>> dispositionTypePairs = new map<string,set<string>>();
        map<string,set<string>> typeTopicPairs = new map<string,set<string>>();
        
        if(picklistValues.size() == 1) {
            
            string valueBody = picklistValues[0].picklist_values__c;
            integer valueBodySize = valueBody.length();
            integer idxStart = 0;
            integer idxEnd = 0;
            
            while(idxStart < valueBodySize) {
                
                // step through picklist_values and generate HTML
                idxEnd = valueBody.indexof('\n',idxStart+1);
                if(idxEnd == -1) {
                    if(!valueBody.endsWith('\n') && !valueBody.endsWith('\r')){
                        idxEnd = valueBodySize;
                    }
                    else {
                        errorStr += 'Parse failed - Reached end of Picklist_Values unexpectedly\n';
                        break;
                    }
                }
                
                string tokenList = valueBody.substring(idxStart,idxEnd);
                idxStart = idxEnd; // set idxStart for the next iteration
                
                if(string.isEmpty(tokenList)) {
                    errorStr += 'Parse failed - field list line is empty' + '\n';
                    break;
                }
                
                if(tokenList.countMatches(',') != 2){
                    errorStr += 'Parse failed - field list must have 3 comma separated fields:' + tokenList + '\n';
                    break;
                }
                
                string tokenType = tokenList.substringBefore(',').normalizeSpace();
                string tokenFld1 = tokenList.substringBetween(',').normalizeSpace();
                string tokenFld2 = tokenList.substringAfterLast(',').normalizeSpace();
                
                if(tokenType.equalsIgnoreCase('disposition')){
                    if(dispositionTypePairs.containsKey(tokenFld1))
                        dispositionTypePairs.get(tokenFld1).add(tokenFld2);
                    else
                        dispositionTypePairs.put(tokenFld1,new set<string> {tokenFld2});
                }
                else if(tokenType.equalsIgnoreCase('calltype')) {
                    if(typeTopicPairs.containsKey(tokenFld1))
                        typeTopicPairs.get(tokenFld1).add(tokenFld2);
                    else
                        typeTopicPairs.put(tokenFld1,new set<string> {tokenFld2});
                }
                else {
                    errorStr += 'Parse failed - field_type not valid: ' + tokenType + ' :field_1=' + tokenFld1 + ' :field_2' + tokenFld2 + '\n';
                }
            }
            
            map<string,string> formattedPicklist = generatePicklistHTML(serviceType,dispositionTypePairs,typeTopicPairs);
            
            // Save lists to database
            picklistValues[0].Picklist_HTML__c = JSON.serialize(formattedPicklist);
            
            try {
                upsert picklistValues[0];
            }
            catch(queryexception qe) {
                errorStr=qe.getmessage();
            }
            
            messageStr = 'Loading picklist values for service type: ' + serviceType + '\n';
            if(string.isNotEmpty(errorStr)){
                messageStr += errorStr;
            }
            else{
                messageStr += 'Success - picklist values have been loaded. Disposition count: ' + 
                    dispositionTypePairs.size() + ' CallType count: ' + typeTopicPairs.size();
            }
        }
        else{
            messageStr += 'Call failed: Call_Tracker_Picklist_Data__c could not be retrieved for service type: ' + serviceType;
        }
        
        return messageStr;
    }
    
    private static map<string,string> generatePicklistHTML(string serviceType, map<string,set<string>> dispTypePairMap, map<string,set<string>> typeTopicPairMap){
        
        map<string,string> formattedPicklist = new map<string,string>();
        
        set<string> allDispList = new set<string>();
        set<string> allCtypeList = new set<string>();
        set<string> allTopicList = new set<string>();
        
        // Step through disposition/calltype pairings
        for(string dispTypeStr : dispTypePairMap.keyset()){
            set<string> ctypeList = dispTypePairMap.get(dispTypeStr);
            allDispList.add(dispTypeStr);
            
            // Step through calltypes
            for(string ctypeStr : ctypeList){
                
                // check for wildcards
                if(ctypeStr == '*'){
                    // all dispositions paired with all types
                    formattedPicklist.put(serviceType+'-'+dispTypeStr+'-*',ctypeStr);
                    ctypeList.remove(ctypeStr);
                }
                else {
                    allCtypeList.add(ctypeStr);
                    
                    // Step through callType/Topic pairings
                    if(typeTopicPairMap.containsKey(ctypeStr)) {
                        
                        set<string> topicList = typeTopicPairMap.get(ctypeStr);
                        
                        // Step through calltopics
                        for(string topicStr : topicList){
                            
                            // check for wildcards
                            if(topicStr == '*'){
                                // all dispositions paired with all types
                                formattedPicklist.put(serviceType+'-'+dispTypeStr+'-'+ctypeStr+'-*',topicStr);
                                topicList.remove(topicStr);
                            }
                            else {
                                allTopicList.add(topicStr);
                            }
                        }
                        formattedPicklist.put(serviceType+'-'+dispTypeStr+'-'+ctypeStr,createOptionHTML(topicList));
                    }
                }
            }
            formattedPicklist.put(serviceType+'-'+dispTypeStr,createOptionHTML(ctypeList));
        }
        
        // Add the rest of the calltype/topic pairings (not controlled by a specific disposition)
        // and Add the rest of the types and topics
        for(string ctypeStr : typeTopicPairMap.keyset()){
            set<string> topicList = typeTopicPairMap.get(ctypeStr);
            if(ctypeStr != '*'){
                allCtypeList.add(ctypeStr);
            }
            for(string topicStr : topicList){
                if(topicStr != '*'){
                    allTopicList.add(topicStr);
                }
            }
            formattedPicklist.put(serviceType+'-*-'+ctypeStr,createOptionHTML(topicList));
        }
        formattedPicklist.put(serviceType+'-*-*',createOptionHTML(allCtypeList));
        formattedPicklist.put(serviceType+'-*-*-*',createOptionHTML(allTopicList));
        
        formattedPicklist.put(serviceType+'-*',createOptionHTML(allDispList));
        
        return formattedPicklist;
    }
    
    private static string getOptionHTML(string str) {
        if(string.isNotBlank(str))
            return '<option value="' + str + '">' + str + '</option>';
        else
            return '';
    }
    
    private static string createOptionHTML(set<string> strList) {
        string result;
        for(string str : strList) {
            result += getOptionHTML(str);
        }
        return result;
    }
    
    // Reset all the fields that are dependent on LnR Request Type
    public void resetLnRRequestType() {
        taskRecord.Competitive_Threat__c = null;
        taskRecord.Soft_Shopper_No_Specific_Competitor__c = null;
        taskRecord.Escalation_Reason__c = null;
        taskRecord.Deactivation_Reason__c = null;
        taskRecord.General_Inquiry__c = null;
        taskRecord.Misdirect_Reason__c = null;
        taskRecord.Competitor_Name__c = null;
        taskRecord.Competitor_Name_Other__c = null;
        taskRecord.Save_Tools_Used__c = null;
        taskRecord.Save_Tools_Used_Other__c = null;
    }
    
    /** 
*  @description    Called by the RemarkGenerateNoteHelper in trac_RemarkGenerateNote.js to query for templates.
*  @date           2017-01-05
*  @author         Allison Ng, Traction on Demand
*/
    @RemoteAction
    public static Call_Tracker_Note_Template__c getTemplate(String callReceivedFrom, String requestType, Boolean inboxCheckbox, String callServiceType) {
        
        List<Call_Tracker_Note_Template__c> templates;
        // If inbox is checked, it takes precedence over a template that has a Call_Received_From of any value and Request Type
        if (inboxCheckbox) {
            templates = [  SELECT Call_Received_From__c, Handlebars_Template__c, Request_Type__c, Name
                         FROM Call_Tracker_Note_Template__c
                         WHERE Request_Type__c = :requestType AND 
                         Call_Service_Type__c = :callServiceType AND 
                         Inbox__c = :inboxCheckbox
                        ];
        } else {
            templates = [  SELECT Call_Received_From__c, Handlebars_Template__c, Request_Type__c, Name
                         FROM Call_Tracker_Note_Template__c
                         WHERE Call_Received_From__c = :callReceivedFrom AND 
                         Request_Type__c = :requestType AND 
                         Call_Service_Type__c = :callServiceType AND 
                         Inbox__c = :inboxCheckbox 
                        ];
        }
        
        
        System.debug('templates: ' + templates); 
        if (templates.size() >= 1) {
            return templates[0];
        } else {
            return null;
        }
    }
    
    /** 
*  @description    Run validation before generating note from Call_Tracker_Note_Template__c obj via RemarkGenerateNoteHelper.
*  @date           2017-01-05
*  @author         Allison Ng, Traction on Demand
*/
    public void validateBeforeCreatingRemark() {
        if (validateLnR(STATUS_GENERATE)) {
            // taskFields and taskExtendFields are serialized to make the data from the forms available in the Call_Tracker_Note_Template__c for template generation.
            taskFields = JSON.serialize(taskRecord);
            taskExtendFields = JSON.serialize(taskExtend);
        } else {
            taskFields = '';
            taskExtendFields = '';
        }
        
    }
    
}