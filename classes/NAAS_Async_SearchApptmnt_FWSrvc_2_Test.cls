@isTest
Public class NAAS_Async_SearchApptmnt_FWSrvc_2_Test {
  
   public static testmethod void insertCustomSetting(){
       
        
                /*SMBCare_WebServices__c testcustom = new SMBCare_WebServices__c();
                testcustom.Name = 'SearchDueDate_EndPoint';
                testcustom.Value__c = 'SearchDueDate_EndPoint';
                testcustom.Use_Soap_For_PT__c = false;
                insert testcustom;
                 String endpoint_x = SMBCare_WebServices__c.getInstance('SearchDueDate_EndPoint').Value__c;
                system.debug('>>>>>>::endpoint_x '+endpoint_x );*/
                
     }
    
     public static testmethod void executeHelperMethods(){
         SMBCare_WebServices__c SearchDueDate_EndPoint = new SMBCare_WebServices__c(Name='SearchDueDate_EndPoint',value__c='https://xmlgwy-pt1.telus.com:9030/is01/RMO/ProcessMgmt/FieldWorkAppointmentService_v2_0_vs0');
         insert SearchDueDate_EndPoint;
          test.startTest();
            
            Continuation cont = new Continuation(60);
            
            NAAS_Async_SearchAppointment_FWService_2.AsyncFieldWorkAppointmentService_v2_0_SOAP AFW = new NAAS_Async_SearchAppointment_FWService_2.AsyncFieldWorkAppointmentService_v2_0_SOAP();
            OCOM_Async_SearchAppointment_ping_v1.pingResponse_elementFuture pingResp = new OCOM_Async_SearchAppointment_ping_v1.pingResponse_elementFuture();
            pingResp = AFW.beginPing(cont);
            
            SMB_Appointment_WFMgmtOrderTypes_V2.InputHeader inputHeader = new SMB_Appointment_WFMgmtOrderTypes_V2.InputHeader();
            SMB_Appointment_WFMgmtOrderTypes_V2.WorkOrder workOrder = new SMB_Appointment_WFMgmtOrderTypes_V2.WorkOrder();
            String appointmentProfileName = 'testProfile';
            DateTime startDate =  Datetime.newInstance(2016, 11, 25);
            DateTime endDate = Datetime.newInstance(2016, 11, 27);
            Boolean gradeAppointmentInd = True;
            Boolean fullSearchInd = True;
            AFW.beginSearchAvailableAppointmentList(cont,inputHeader,workOrder,appointmentProfileName,startDate,endDate,gradeAppointmentInd,fullSearchInd);
        Test.stopTest();
     }           
}