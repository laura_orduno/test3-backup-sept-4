public without sharing class smb_ServiceRequestHelper {
    
    
       
    /*This method will create a new enhanced tracking record
    */
    public static void insertEnhanceTrackingRecord(list<Service_Request__c> lstSerRequests,map<ID,Service_Request__c> mapOldMAP,boolean isInsert)
    {
        list <Enhanced_Tracking__c> lstEnhanceTracking = new list<Enhanced_Tracking__c>();
        
        Set<Id> setUserId=new Set<Id>();
        for(Service_Request__c serReq :lstSerRequests)
        {
            setUserId.add(serReq.LastModifiedById);
        }
        
        Map<Id,User> mapUser=new Map<Id,User>([select ID,name from user where id in:setUserId]);
        if (lstSerRequests.size()>0)
        {
            for (Service_Request__c serReq :lstSerRequests )
            {
                
                if (isInsert || (mapOldMAP.size()>0 && serReq.Status__c != mapOldMAP.get(serReq.id).Status__c))
                
                    {
                        //For status
                        Enhanced_Tracking__c ET = new Enhanced_Tracking__c();                   
                        ET.Field__c = 'Status';
                        ET.Service_Request__c = serReq.Id;
                        ET.Changed_By__c = Userinfo.getUserId();
                        if(isInsert){
                            ET.Start_Value__c = serReq.Status__c;
                            ET.Start__c = serReq.LastModifiedDate;
                        }
                        else{
                             ET.Start_Value__c = mapOldMAP.get(serReq.id).Status__c;
                             ET.Start__c = mapOldMAP.get(serReq.id).LastModifiedDate;
                        }
                        ET.End_Value__c = serReq.Status__c;
                        ET.End__c = serReq.LastModifiedDate;
                        
                        lstEnhanceTracking.Add(ET);             
                    }
                
                    if (isInsert || (mapOldMAP.size()>0 && serReq.LastModifiedById != mapOldMAP.get(serReq.id).LastModifiedById))
                
                    {
                        //For Owner
                        Enhanced_Tracking__c ET1 = new Enhanced_Tracking__c();
                        ET1.Field__c = 'Owner';
                        ET1.Service_Request__c = serReq.Id;
                        ET1.Changed_By__c = Userinfo.getUserId();
                        try{
                        if(isInsert){
                            ET1.Start_Value__c=mapUser.get(serReq.LastModifiedById).name;
                            ET1.Start__c = serReq.LastModifiedDate;
                        }
                        else{
                            System.debug('serReq.id->'+serReq.id);
                            System.debug('mapOldMAP.get(serReq.id)->'+mapOldMAP.get(serReq.id));
                            System.debug('mapUser->'+mapUser);
                            ET1.Start_Value__c = mapUser.get(mapOldMAP.get(serReq.id).LastModifiedById).name;
                            ET1.Start__c = mapOldMAP.get(serReq.id).LastModifiedDate;
                        }
                               
                        ET1.End_Value__c =mapUser.get(serReq.LastModifiedById).name;
                        ET1.End__c = serReq.LastModifiedDate;       
                        lstEnhanceTracking.Add(ET1);
                        } catch (Exception ex)
                        {
                            System.debug('EXCEPTION-> One or more assignments have resulted in this error:-'+ex.getMessage());
                        }
                    }               
            }
        
            if(lstEnhanceTracking.size()>0)
                insert lstEnhanceTracking;
        }
     
    }
    
    
    public static void syncOrderRequestStatus(list<Service_Request__c> lstServiceRequest, Boolean isUpdate, Map<Id,Service_Request__c> mapServiceRequestOldValues){
        set<Id> setOrderRequest = new set<Id>();
        list<Order_Request__c> lstUpdateOrderRequest  = new list<Order_Request__c>();
        Order_Request__c objOrderRequest;
        Boolean isDelivery = false;
        Boolean isInProgress = false;
        Boolean isOnHoldTelus = false;
        Boolean isOnHoldCustomer = false;
        Boolean isNonDoable = false;
        Boolean isNeedsInfo = false;
        Boolean isAllCancelled = true;
        Boolean isAllNew = true;
        Boolean isAllCompleteOrCancelled = true;
        
        
        for(Service_Request__c objSR:lstServiceRequest){
            if(!isUpdate || (isUpdate && objSR.Status__c != mapServiceRequestOldValues.get(objSR.Id).Status__c)){
                setOrderRequest.add(objSR.Order_Request_Lookup__c);
            }
        }
        System.debug('========>setOrderRequest' + setOrderRequest);
        if(setOrderRequest.size()>0){
            list<Order_Request__c> lstOrderRequest = [Select Id, Order__c, (Select Id, Status__c from Service_Requests1__r) from Order_Request__c where Id IN:setOrderRequest];
            if(lstOrderRequest!= null && lstOrderRequest.size()>0){
                for(Order_Request__c objOR:lstOrderRequest){
                    if(objOR.Service_Requests1__r != null && objOR.Service_Requests1__r.size()>0){
                        isDelivery = false;
                        isInProgress = false;
                        isNonDoable = false;
                        isNeedsInfo = false;
                        isOnHoldTelus = false;
                        isOnHoldCustomer = false;
                        isAllCancelled = true;
                        isAllCompleteOrCancelled = true;
                        isAllNew = true;
                        
                        for(Service_Request__c objSR: objOR.Service_Requests1__r){
                            
                            /*
                            Code Change by Amit Rana Dt. 7 July 2015 for BR798062
                            Desc: to accept additional new status
                            Existing Logic: 
                            :Starts:
                                (objSR.Status__c.equalsIgnoreCase('Non-Doable')){
                            :Ends
                            */
                            if((objSR.Status__c.equalsIgnoreCase('Non-Doable')) || (objSR.Status__c.equalsIgnoreCase('Non-Doable: Re-Issue Agreement')) || (objSR.Status__c.equalsIgnoreCase('Non-Doable: Not Qualified'))){
                                isNonDoable = true;
                                isAllCancelled = false;
                                isAllCompleteOrCancelled = false;
                                isAllNew = false;
                                break;
                            }
                            /*
                            Code Change by Amit Rana Dt. 7 July 2015 for BR798062
                            Desc: to accept additional new status
                            Existing Logic: 
                            :Starts:
                                else if(objSR.Status__c.equalsIgnoreCase('Need Additional Information')){
                            :Ends
                            */
                            else if((objSR.Status__c.equalsIgnoreCase('Need Additional Information')) || 
                                    (objSR.Status__c.equalsIgnoreCase('Follow-up: Customer Required')) || 
                                    (objSR.Status__c.equalsIgnoreCase('Missing: Booking Details')) || 
                                    (objSR.Status__c.equalsIgnoreCase('Missing: Contact Details')) || 
                                    (objSR.Status__c.equalsIgnoreCase('Missing: Cost/Payment Details')) || 
                                    (objSR.Status__c.equalsIgnoreCase('Missing: Customer Details')) || 
                                    (objSR.Status__c.equalsIgnoreCase('Missing: Order Details')) || 
                                    (objSR.Status__c.equalsIgnoreCase('Missing: Product Error')) || 
                                    (objSR.Status__c.equalsIgnoreCase('Missing: Remarks Error'))){
                                isNeedsInfo = true;
                                isAllCancelled = false;
                                isAllCompleteOrCancelled = false;
                                isAllNew = false;
                            }
                            else if(objSR.Status__c.equalsIgnoreCase('In Progress')){
                                isInProgress = true;
                                isAllCancelled = false;
                                isAllCompleteOrCancelled = false;
                                isAllNew = false;
                            }
                            /*
                            Code Change by Amit Rana Dt. 7 July 2015 for BR798062
                            Desc: By this changes system can accept both status
                            Existing Logic: 
                            :Starts:
                                else if(objSR.Status__c.equalsIgnoreCase('Delivery')){
                            :Ends
                            */
                            else if(objSR.Status__c.equalsIgnoreCase('Delivery') || objSR.Status__c.equalsIgnoreCase('Order Entry is Completed') ){
                                isDelivery = true;
                                isAllCancelled = false;
                                isAllCompleteOrCancelled = false;
                                isAllNew = false;
                            }
                            /*
                            Code Change by Amit Rana Dt. 7 July 2015 for BR798062
                            Desc: By this changes system can accept both status
                            Existing Logic: 
                            :Starts:
                                else if(objSR.Status__c.equalsIgnoreCase('On Hold (Customer)')){
                            :Ends
                            */
                            else if((objSR.Status__c.equalsIgnoreCase('On Hold (Customer)')) || (objSR.Status__c.equalsIgnoreCase('Hold: Customer Requested'))){
                                isOnHoldCustomer = true;
                                isAllCancelled = false;
                                isAllCompleteOrCancelled = false;
                                isAllNew = false;
                            }
                            /*
                            Code Change by Amit Rana Dt. 7 July 2015 for BR798062
                            Desc: By this changes system can accept all status
                            Existing Logic: 
                            :Starts:
                                else if(objSR.Status__c.equalsIgnoreCase('On Hold (TELUS)')){
                            :Ends
                            */
                             else if((objSR.Status__c.equalsIgnoreCase('On Hold (TELUS)')) || 
                                     (objSR.Status__c.equalsIgnoreCase('Hold: Held Order')) ||
                                     (objSR.Status__c.equalsIgnoreCase('Hold: Back Office')) || 
                                     (objSR.Status__c.equalsIgnoreCase('Follow-up: ADSL Order')) ||
                                     (objSR.Status__c.equalsIgnoreCase('Follow-up: Porting Approval')) ||
                                     (objSR.Status__c.equalsIgnoreCase('Follow-up: SBTV Part 2')) ||
                                     (objSR.Status__c.equalsIgnoreCase('Hold: Credit Approval'))){
                                isOnHoldTelus = true;
                                isAllCancelled = false;
                                isAllCompleteOrCancelled = false;
                                isAllNew = false;
                            }
                            /*
                            Code Change by Amit Rana Dt. 7 July 2015 for BR798062
                            Desc: System accept both status Completed and Order entry is completed
                            Existing Logic: 
                            :Starts:
                                 else if(objSR.Status__c.equalsIgnoreCase('Completed')){
                            :Ends
                            */
                            else if(objSR.Status__c.equalsIgnoreCase('Completed') || objSR.Status__c.equalsIgnoreCase('Provisioning is Completed')){
                                isAllCancelled = false;
                                isAllNew = false;
                            }
                            else if(objSR.Status__c.equalsIgnoreCase('New')){
                                isAllCancelled = false;
                                isAllCompleteOrCancelled = false;
                            }
                        }
                        
                        objOrderRequest = new Order_Request__c(Id=objOR.Id);
                        if(isNonDoable){
                            objOrderRequest.Order__c = 'Non-Doable';
                            lstUpdateOrderRequest.add(objOrderRequest);
                        }
                        else if(isNeedsInfo && !isNonDoable){
                            objOrderRequest.Order__c = 'Need Additional Information';
                            lstUpdateOrderRequest.add(objOrderRequest);
                        }
                        else if(isInProgress){
                            objOrderRequest.Order__c = 'In Progress';
                            lstUpdateOrderRequest.add(objOrderRequest);
                        }
                        else if(isDelivery && !isInProgress){
                            objOrderRequest.Order__c = 'Delivery';
                            lstUpdateOrderRequest.add(objOrderRequest);
                        }
                        else if(isOnHoldTelus && !isInProgress && !isDelivery){
                            objOrderRequest.Order__c = 'On Hold (TELUS)';
                            lstUpdateOrderRequest.add(objOrderRequest);
                        }
                        else if(isOnHoldCustomer && !isInProgress && !isDelivery){
                            objOrderRequest.Order__c = 'On Hold (Customer)';
                            lstUpdateOrderRequest.add(objOrderRequest);
                        }
                        else if(isAllCancelled){
                            objOrderRequest.Order__c = 'Cancelled';
                            lstUpdateOrderRequest.add(objOrderRequest);
                        }
                        else if(!isAllCancelled && isAllCompleteOrCancelled){
                            objOrderRequest.Order__c = 'Completed'; 
                            lstUpdateOrderRequest.add(objOrderRequest);
                        }
                        else if(isAllNew){
                            objOrderRequest.Order__c = 'New'; 
                            lstUpdateOrderRequest.add(objOrderRequest);
                        }
                    }
                }
            
                if(lstUpdateOrderRequest.size() > 0){
                    update lstUpdateOrderRequest;
                }
            }
        }
    }
   /*
    public static void createTask(list<Service_Request__c> listServiceRequests, Boolean isUpdate, map<Id,Service_Request__c> mapOldServiceRequests){
        
        list<Task> listTasks = new list<Task>();
        set<Id> setOrderRequests = new set<Id>();
        for(Service_Request__c objSR:listServiceRequests){
                if((objSR.Status__c == 'Non-Doable' || objSR.Status__c == 'Needs Additional Information') && (!isUpdate || (mapOldServiceRequests.get(ObjSR.Id).Status__c <> 'Non-Doable' && mapOldServiceRequests.get(ObjSR.Id).Status__c <> 'Needs Additional Information')) ){
                    setOrderRequests.add(ObjSR.Order_Request_Lookup__c);
                }
        }
        //get the order requests
        map<Id,Order_Request__c> mapOrderRequest = new map<Id,Order_Request__c>([Select Id, Originating_Agent__c from Order_Request__c where Id IN: setOrderRequests]);
        //get the record type
        list<RecordType> listRecordType = [Select  r.Id From RecordType r where r.SobjectType = 'Task' and r.IsActive = true and r.DeveloperName = 'SMB_Care_FO_to_BO_Task'];
        if(listRecordType!= null && listRecordType.size()>0){
            for(Service_Request__c objSR:listServiceRequests){
                system.debug('### objSR.Status__c: ' + objSR.Status__c);
                if((objSR.Status__c == 'Non-Doable' || objSR.Status__c == 'Needs Additional Information') && (!isUpdate || (mapOldServiceRequests.get(ObjSR.Id).Status__c <> 'Non-Doable' && mapOldServiceRequests.get(ObjSR.Id).Status__c <> 'Needs Additional Information')) ){
                    system.debug('### objSR.Order_Request_Lookup__c: ' + objSR.Order_Request_Lookup__c);
                    if(mapOrderRequest.containsKey(objSR.Order_Request_Lookup__c) && mapOrderRequest.get(objSR.Order_Request_Lookup__c).Originating_Agent__c != null){
                    //if(mapOrderRequest.get(objSR.Order_Request_Lookup__c).Originating_Agent__c != null){
                        Task objTask = new Task();
                        objTask.WhatId = objSR.Id;
                        objTask.Description = objSR.Notes__c;
                        objTask.RecordTypeId = listRecordType[0].Id;
                        objTask.OwnerId = mapOrderRequest.get(objSR.Order_Request_Lookup__c).Originating_Agent__c;
                        if(objSR.Status__c == 'Non-Doable'){
                            objTask.Subject = 'Non-Doable Order Request';
                        }
                        else{
                            objTask.Subject = 'Order Request Needs Additional Information';
                            
                        }
                        listTasks.add(objTask);
                    }
                }
            }
            
            if(listTasks.size()>0){
                insert listTasks;
            }
        }
        
     
    }*/
    /*
    * This method is used to update the Legacy Order Id from Service REquest onto Opportunity field
    */
    public static void updateLegacyOrderOnOpportunity(List<Service_Request__c> lstServiceRequest)
    {
        Set<Id> setOpp=new Set<Id>();
        Map<Id,Opportunity> mapOppSerReq=new Map<Id,Opportunity>();
        for(Service_Request__c serReq:lstServiceRequest)
        {
            if(mapOppSerReq.containsKey(serReq.Opportunity__c)==false && String.isNotBlank(serReq.Legacy_Order_ID__c)&& String.isNotBlank(serReq.Opportunity__c))
                mapOppSerReq.put(serReq.Opportunity__c,new Opportunity(id=serReq.Opportunity__c,Legacy_Order_ID__c=serReq.Legacy_Order_ID__c));
        }
        List<Opportunity> lstOpp=[Select id from Opportunity where Legacy_Order_ID__c<>'' and id in:mapOppSerReq.keySet()];
        for(Opportunity opp:lstOpp)
        {
            mapOppSerReq.remove(opp.id);
        }

        if (!mapOppSerReq.isempty()) {
            Database.update(mapOppSerReq.values(),true);
        }
    }
    
    
    public static void populateOpportunityAndOrderRequest(List<Service_Request__c> lstServiceRequest)
    {
        Set<String> setOrderRequestNCID= new Set<String>();
        set<string> setOrderRequestId = new set<string>();
        for(Service_Request__c SR:lstServiceRequest)
        {
            if(null!= SR.Customer_Order_ID__c)
                setOrderRequestNCID.add(SR.Customer_Order_ID__c);
            if(null!= SR.Order_Request__c)
                setOrderRequestId.add(SR.Order_Request__c);
        }
        System.debug('setOrderRequestNCID->'+setOrderRequestNCID);
        //List<Order_Request__c> lstOrderRequest=[select id, opportunity__c,Opportunity__r.Order_ID__c,NC_Order_ID__c from order_request__c where NC_Order_ID__c in : setOrderRequestNCID];
        List<Order_Request__c> lstOrderRequest=[select id, opportunity__c,Opportunity__r.Order_ID__c,NC_Order_ID__c, createddate from order_request__c where Opportunity__r.Order_ID__c in : setOrderRequestId OR NC_Order_ID__c in : setOrderRequestNCID];
        System.debug('lstOrderRequest->'+lstOrderRequest);
        Map<String,Order_Request__c> mapOrderRequest=new Map<String,Order_Request__c>();
        Map<String,Order_Request__c> mapOrderOppRequest=new Map<String,Order_Request__c>();
        for(Order_Request__c orderRequest:lstOrderRequest)
        {
            if(orderRequest.NC_Order_ID__c != null)
                mapOrderRequest.put(orderRequest.NC_Order_ID__c,orderRequest);
            
            string OrderId = orderRequest.Opportunity__r.Order_ID__c;
            if(String.IsNotBlank(OrderId))
            {
                if(mapOrderOppRequest.containsKey(OrderId))
                {
                    if((mapOrderOppRequest.get(OrderId)).CreatedDate < orderRequest.createdDate)
                        mapOrderOppRequest.put(OrderId,orderRequest);
                    // Else don't do anything
                }
                else 
                    mapOrderOppRequest.put(OrderId,orderRequest);
            }
            // Else don't add anything    
        }
        
        for(Service_Request__c oSR:lstServiceRequest)
        {
            Order_Request__c objOR;
            if(null!=oSR.Customer_Order_ID__c && mapOrderRequest.containsKey(oSR.Customer_Order_ID__c))
                objOR = mapOrderRequest.get(oSR.Customer_Order_ID__c);
            else if(null!=oSR.Order_Request__c && mapOrderOppRequest.containsKey(oSR.Order_Request__c))
                objOR = mapOrderOppRequest.get(oSR.Order_Request__c);
                        
            if(objOR != null)
            {
                oSR.Order_Request_Lookup__c=objOR.id;
                oSR.opportunity__c=objOR.opportunity__c;   
            }
            // Else any error handling      
        }

    }
    
    
    public static void populateStatusOnServiceRequest(List<Service_Request__c> lstServiceRequest, boolean isUpdate, map<Id,Service_Request__c> mapOldServiceRequest){
        map<string,SMB_Service_Request_Mapping__c> smbSerRequestMapping = new map<string,SMB_Service_Request_Mapping__c>();    
        smbSerRequestMapping = SMB_Service_Request_Mapping__c.getAll();
        for(Service_Request__c SR:lstServiceRequest)
        {
            if(String.IsNotBlank(SR.Status__c) && (!isUpdate || (SR.Status__c <> mapOldServiceRequest.get(SR.ID).Status__c))){
                 if(smbSerRequestMapping.containsKey(SR.Status__c.trim().toLowerCase()))
                    SR.Status__c = smbSerRequestMapping.get(SR.Status__c.trim().toLowerCase()).ServiceRequestMapping__c;
                
                 /*
                Code Change by Amit Rana Dt. 7 July 2015 for BR798062
                Desc: If Status is Delivery or Order entry is completed Stamp date in Service_Request_Completed_Date__c
                Existing Logic: 
                :Starts:
                    if(SR.Status__c.equalsIgnoreCase('Non-Doable'))
                :Ends
                */
                 if(SR.Status__c.equalsIgnoreCase('Non-Doable') || SR.Status__c.equalsIgnoreCase('Non-Doable: Re-Issue Agreement') || SR.Status__c.equalsIgnoreCase('Non-Doable: Not Qualified'))
                    SR.Service_Request_Non_Doable_Date__c = DateTime.Now();
                 
                 if(SR.Status__c.equalsIgnoreCase('In Progress'))
                    SR.Service_Request_Assigned_Date__c = DateTime.Now();
                 
                 /*
                Code Change by Amit Rana Dt. 7 July 2015 for BR798062
                Desc: If Status is Delivery or Order entry is completed Stamp date in Service_Request_Completed_Date__c
                Existing Logic: 
                :Starts:
                    if(SR.Status__c.equalsIgnoreCase('Delivery'))
                :Ends
                */
                 if(SR.Status__c.equalsIgnoreCase('Delivery') || SR.Status__c.equalsIgnoreCase('Order Entry is Completed'))
                    SR.Service_Request_Completed_Date__c = DateTime.Now();
                 /*
                Code Change by Amit Rana Dt. 7 July 2015 for BR798062
                Desc: If Status is Delivery or Order entry is completed Stamp date in Service_Request_Completed_Date__c
                Existing Logic: 
                :Starts:
                    if(!SR.Status__c.equalsIgnoreCase('Non-Doable') && SR.Service_Request_Non_Doable_Date__c != null)
                :Ends
                */
                 if(!(SR.Status__c.equalsIgnoreCase('Non-Doable') || SR.Status__c.equalsIgnoreCase('Non-Doable: Re-Issue Agreement') || SR.Status__c.equalsIgnoreCase('Non-Doable: Not Qualified')) && SR.Service_Request_Non_Doable_Date__c != null)
                    SR.Service_Request_Resume_from_Non_Doable__c = DateTime.Now();
                   
                /*if(SR.Status__c.equalsIgnoreCase('Order Received')){
                    SR.Status__c = 'New';
                }
                else if(SR.Status__c.equalsIgnoreCase('Order Entry is Completed')){
                    SR.Status__c = 'Delivery';
                }
                else if(SR.Status__c.equalsIgnoreCase('Provisioning is Completed')){
                    SR.Status__c = 'Completed';
                }
                else if(SR.Status__c.equalsIgnoreCase('Critical Fulfillment Error')){
                    SR.Status__c = 'Non-Doable';
                }
                else if(SR.Status__c.equalsIgnoreCase('Order Details Missing')){
                    SR.Status__c = 'Need Additional Information';
                }
                else if(SR.Status__c.equalsIgnoreCase('BO Hold') || SR.Status__c.equalsIgnoreCase('On Hold')){
                    SR.Status__c = 'On Hold (TELUS)';
                }
                else if(SR.Status__c.equalsIgnoreCase('Suspended')){
                    SR.Status__c = 'On Hold (Customer)';
                }*/
                
            }
        }
    }

    
    public static void populateServiceRequestOwnerID(list<Service_Request__c> lstSerRequests,map<Id,Service_Request__c> mapOldServiceRequest)
    {

        Map <String,Id> mapTelusEmployeeId = New Map<String,Id>();
        set<String> setNCOwnerId=new set<String>();
        system.Debug('Inside Change Owner ');
        for(Service_Request__c serReq :lstSerRequests)
        {
            if(String.IsNotBlank(serReq.Netcracker_Owner_ID__c))
            {
                // Is this update
                System.Debug('Necracker OwnerId is not blank');
                if(mapOldServiceRequest != null)
                {
                    // Does the Map contains the Id and if the users donot match
                  if(mapOldServiceRequest.containsKey(serReq.Id) && mapOldServiceRequest.get(serReq.Id).Netcracker_Owner_ID__c <> serReq.Netcracker_Owner_ID__c)
                    {
                        setNCOwnerId.add(serReq.Netcracker_Owner_ID__c);
                    }
                }
                else
                {
                    System.Debug('Added Necracker OwnerId  to set');
                    setNCOwnerId.add(serReq.Netcracker_Owner_ID__c);
                }
            }
        }
            
        if(setNCOwnerId.size() > 0)
        {
            List<User> lstUser=new List<User>([select ID,EmployeeId__c,Team_TELUS_ID__c from user where Team_TELUS_ID__c in:setNCOwnerId and IsActive = true]);
            for(User mu : lstUser)
            {
                mapTelusEmployeeId.put(mu.Team_TELUS_ID__c.toUpperCase(),mu.id);
            }
        }
        
        System.debug('### mapTelusEmployeeId : ' + mapTelusEmployeeId);
        if (mapTelusEmployeeId.keyset().size()>0)
        {
            for (Service_Request__c serReq :lstSerRequests )
            {    
                if (serReq.Netcracker_Owner_ID__c <> null && serReq.Netcracker_Owner_ID__c <> '' && mapTelusEmployeeId.containsKey(serReq.Netcracker_Owner_ID__c.toUpperCase()))
                {    
                    try
                    {
                        serReq.OwnerID = mapTelusEmployeeId.get(serReq.Netcracker_Owner_ID__c.toUpperCase());
                    }
                    catch(Exception e)
                    {
                        system.debug('*** Exception in populateServiceRequestOwnerID: ' + e);
                    }
                }
            }  
        }
    
    }    
    
    public static void populateServiceRequestProduct (list<Service_Request__c> lstSerRequests)
    {
        map<string,id> mapSterlingProductId = new map<string,id>();
        set<string> setSterlingId = new set<string>();
        
        for(Service_Request__c SR : lstSerRequests)
        {
            if(String.isNotBlank(SR.Product_ID__c))
            {
                setSterlingId.add(SR.Product_ID__c);
            }
        }
        if(setSterlingId.size() > 0)
        {
            try
            {
                list<Product2> lstProduct = [SELECT Sterling_Item_ID__c,id from Product2 where Sterling_Item_ID__c in :setSterlingId];
                if(lstProduct.size() > 0)
                {
                    for(Product2 p2 : lstProduct)
                    {
                        mapSterlingProductId.put(p2.Sterling_Item_ID__c,p2.id);                 
                    }
                    
                    for(Service_Request__c SR : lstSerRequests)
                    {
                        try
                        {
                            if(String.isNotBlank(SR.Product_ID__c) && mapSterlingProductId.containsKey(SR.Product_ID__c))
                            {
                                SR.Product_Name__c = mapSterlingProductId.get(SR.Product_ID__c);
                            }
                        }
                        catch(Exception e)
                        {
                            system.debug('*** Exception in populateServiceRequestProduct : ' + e);
                        }
                    }
                }
            }
            catch(Exception e)
            {
                system.debug('*** Exception in populateServiceRequestProduct : ' + e);
            }
                
        }
    }
    
    public static void populateServiceRequestContact (list<Service_Request__c> lstSerRequests)
    {        
		system.debug('------------populateServiceRequestContact-----');
        
        List<Service_Request_Contact__c> lstSerRequestContacts = new List<Service_Request_Contact__c>();
        
        for(Service_Request__c SR : lstSerRequests){
			system.debug('------------populateServiceRequestContact-----SR.Name-----' + SR.Name );
			system.debug('------------populateServiceRequestContact-----SR.isClone()-----' + SR.isClone() );
            if(SR.isClone() == false && SR.PrimaryContact__c != null){
                Service_Request_Contact__c serRequestContact = new Service_Request_Contact__c();    
                
                serRequestContact.Service_Request__c = SR.id;
                serRequestContact.contact__c = SR.PrimaryContact__c;
				
				//START :: Non-TQ TF Case 1904646, BR: S2CP-82 :: added by Rajan ( JIRA URL : https://jira.tsl.telus.com/browse/S2CP-82 )
				
                serRequestContact.Contact_Type__c = System.label.SRS_ContactTypeRequestingCustomer;
                
				//END :: Non-TQ TF Case 1904646, BR: S2CP-82 :: added by Rajan
				
                lstSerRequestContacts.add(serRequestContact);
            }
        }
        
        if(lstSerRequestContacts.size() > 0){
            try{
                insert lstSerRequestContacts;
            }
            catch(DmlException e){
                throw e;
            }
        }
    }    
}