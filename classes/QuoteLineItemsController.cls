public without sharing class QuoteLineItemsController {
	/*
	   DEPRECATED as of QT R5
	*/
	/*
	public QuoteVO quote {get; set;}
	
	
	public List<QuoteLineVO> getFeatureItems() {
		List<QuoteLineVO> result = new List<QuoteLineVO>();
		for (SBQQ__QuoteLine__c line : quote.record.SBQQ__LineItems__r) {
			if (line.Category__c.equalsIgnoreCase('features')) {
				result.add(new QuoteLineVO(line));
			}
		}
		return result;
	}
	
	public List<QuoteLineVO> getServiceItems() {
		List<QuoteLineVO> result = new List<QuoteLineVO>();
		for (SBQQ__QuoteLine__c line : quote.record.SBQQ__LineItems__r) {
			if (line.Category__c.equalsIgnoreCase('services')) {
				result.add(new QuoteLineVO(line));
			}
		}
		return result;
	}
	
	public List<QuoteLineVO> getDeviceItems() {
		List<QuoteLineVO> result = new List<QuoteLineVO>();
		for (SBQQ__QuoteLine__c line : quote.record.SBQQ__LineItems__r) {
			if (line.Category__c.equalsIgnoreCase('devices')) {
				result.add(new QuoteLineVO(line));
			}
		}
		return result;
	}
	
	public List<QuoteLineVO> getCreditItems() {
		List<QuoteLineVO> result = new List<QuoteLineVO>();
		for (SBQQ__QuoteLine__c line : quote.record.SBQQ__LineItems__r) {
			if (line.Category__c.equalsIgnoreCase('credits')) {
				result.add(new QuoteLineVO(line));
			}
		}
		return result;
	}
	
	public Integer getFeatureItemCount() {
		return getFeatureItems().size();
	}
	
	public Integer getServiceItemCount() {
		return getServiceItems().size();
	}
	
	public Integer getDeviceItemCount() {
		return getDeviceItems().size();
	}
	
	public Integer getCreditItemCount() {
		return getCreditItems().size();
	}
	
	testMethod static void test() {
		Web_Account__c account = new Web_Account__c(Name='UnitTest');
		insert account;
		
		Opportunity opp = new Opportunity(Name='Test',Web_Account__c=account.Id,CloseDate=System.today(),StageName='Prospecting');
		insert opp;
		
		SBQQ__Quote__c quote = new SBQQ__Quote__c(SBQQ__Opportunity__c=opp.Id);
		quote.Rate_Band__c = 'F';
		quote.Province__c = 'BC';
		insert quote;
		
		SBQQ__QuoteLine__c l1 = new SBQQ__QuoteLine__c(SBQQ__Quote__c=quote.Id,Category__c='Devices');
		SBQQ__QuoteLine__c l2 = new SBQQ__QuoteLine__c(SBQQ__Quote__c=quote.Id,Category__c='Services');
		SBQQ__QuoteLine__c l3 = new SBQQ__QuoteLine__c(SBQQ__Quote__c=quote.Id,Category__c='Credits');
		insert new SBQQ__QuoteLine__c[]{l1,l2,l3};
		
		quote = [SELECT Id, (SELECT Category__c FROM SBQQ__LineItems__r) FROM SBQQ__Quote__c WHERE Id = :quote.Id];
		QuoteLineItemsController target = new QuoteLineItemsController();
		target.quote = new QuoteVO(quote);
		System.assertEquals(0, target.getDeviceItemCount());
		System.assertEquals(3, target.getServiceItemCount());
		System.assertEquals(0, target.getCreditItemCount());
		System.assertEquals(0, target.getFeatureItemCount());
	}
	*/
}