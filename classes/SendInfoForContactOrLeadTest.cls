@isTest
private class SendInfoForContactOrLeadTest {
// TESTS
   
	private static testMethod void testCtorEmptyParams() {
   	SendInfoForContactOrLead sender = new SendInfoForContactOrLead();
   	
   	system.assertEquals(null, sender.getGuid());
   	system.assert(sender.getErrorFirstName());
   	system.assertEquals(false, sender.getRenderLeadBlock());
   	system.assertEquals(false, sender.getRenderContactBlock());
   	
   	
  }
  
  @isTest(SeeAllData=true) // ADRA Lead Trigger issues
  private static void testCtorLead() {
   	final String GUID = 'test';
   	
   	Lead l = new Lead(firstname = 'first', lastname = 'last', company = 'test', guid__c = GUID);
   	insert l;
   	
   	ApexPages.currentPage().getParameters().put('leadId', l.id);
   	ApexPages.currentPage().getParameters().put('uid', GUID);
   	
   	SendInfoForContactOrLead sender = new SendInfoForContactOrLead();
   	
   	//system.assertEquals(false, sender.getErrorFirstName());
   	
   	system.assert(sender.getRenderLeadBlock());
   	
   	String style = sender.getStyleClassName();
   	
   	sender.updateLead();
   	
   	sender.doSubmit();
   	
   	l.firstname = '';
   	update l;
   	
   	sender = new SendInfoForContactOrLead();
   	
   	sender.updateLead();
   	
   	sender.doSubmit();
   	
   	// invalid id
   	ApexPages.currentPage().getParameters().put('leadId', '00Q4000000W6g0Z');
   	sender = new SendInfoForContactOrLead();
   	
   	//sender.updateLead();
   	
   	sender.doSubmit();
  }
  
  private static testMethod void testCtorContact() {
   	final String GUID = 'test';
   	
   	Contact c = new Contact(firstname = 'first', lastname = 'last', guid__c = GUID);
   	insert c;
   	
   	ApexPages.currentPage().getParameters().put('contactId', c.id);
   	ApexPages.currentPage().getParameters().put('uid', GUID);
   	
   	SendInfoForContactOrLead sender = new SendInfoForContactOrLead();
   	
   	//system.assertEquals(false, sender.getErrorFirstName());
   	
   	system.assert(sender.getRenderContactBlock());
   	
   	String style = sender.getStyleClassName();
   	
   	sender.doSubmit();
   	
   	// empty first name
   	
   	c.firstname = '';
   	update c;
   	
   	sender = new SendInfoForContactOrLead();
   	
   	sender.doSubmit();
   	
   	// invalid id
   	ApexPages.currentPage().getParameters().put('contactId', '003e00000056mNZ');
   	sender = new SendInfoForContactOrLead();
   	
   	//sender.doSubmit();
  }
  
  private static testMethod void testCheckMethods() {
  	final String GUID = 'test';
  	
  	ApexPages.currentPage().getParameters().put('uid', GUID);
  	
  	SendInfoForContactOrLead sender = new SendInfoForContactOrLead();
  	
  	system.assert(sender.isValidGuid(GUID));
  	system.assertEquals(false, sender.isValidGuid(''));
  	system.assert(sender.isLeadConverted(true));
  }
}