/* Test class for OCOM_ShippingAddrCPValdationUtility
 * Project OCOM WP3
 * Created by Aditya Jamwal(IBM)
 * Date 11-SEPT-2016
 */
@isTest 
private class OCOM_ShippingAddrCPValdationUtility_test {
 private static testmethod void testCanadaPostValidation() {
     
     PACConfig__c shippingdetails = new PACConfig__c(Name='shipping',
                                                     End_Point_URL__c='https://ws1.postescanada-canadapost.ca/AddressComplete/Interactive/Find/v2.10/json3.ws?callback=?',
                                                     Second_Point_URL__c='https://ws1.postescanada-canadapost.ca/AddressComplete/Interactive/Retrieve/v2.10/json3.ws?callback=?',
                                                     API_Key__c='ZJ21-RD89-UB86-RJ69',
                                                     Shipping_Utility_Second_URL__c= 'https://ws1.postescanada-canadapost.ca/AddressComplete/Interactive/Retrieve/v2.10/json3.ws?callback=?'); 
     shippingdetails.Source_System__c ='q';
     shippingdetails.Search_For__c='q'; shippingdetails.Max_Suggestions__c=2; shippingdetails.Max_Results__c=10;
     shippingdetails.DelayToAvoidMultipleHits__c='q';
     insert shippingdetails;
     
     OCOM_ShippingAddrCPValdationUtility clsObj = new OCOM_ShippingAddrCPValdationUtility();
     OCOM_ShippingAddrCPValdationUtility.serviceAddressData addSer = new OCOM_ShippingAddrCPValdationUtility.serviceAddressData();
     addSer.postalCode='T1A1T6';addSer.streetName='11 ST SE';addSer.buildingNumber='847';
    
      PACConfig__c custData= PACConfig__c.getValues('shipping');
      String url;String reteriveURl;
        if(null!=custData){
            string searchItem=EncodingUtil.urlEncode(addSer.postalCode+' '+addSer.buildingNumber+' '+addSer.streetName,'UTF-8');
            String CanadaPostKey = custData.API_Key__c;
            String findPreURl =  (custData.End_Point_URL__c.remove('callback=?'))+'&Key='+CanadaPostKey+'&SearchTerm=';
            String findPostURl = '&LastId=&SearchFor=Everything&Country=CAN&LanguagePreference=en&MaxSuggestions=0&MaxResults=1&OrderBy=UserLocation';
                   reteriveURl = (custData.Shipping_Utility_Second_URL__c.remove('callback=?'))+'&Key='+CanadaPostKey+'&Id=';
            url = findPreURl +searchItem+findPostURl ;
        } 
     Test.startTest();    
     Test.setMock(HttpCalloutMock.class, new CanadaPostFirstCallout());     
     HttpRequest req = new HttpRequest();
     req.setEndpoint(url);
     req.setMethod('GET');
     Http h = new Http();
     HttpResponse res = h.send(req);
     OCOM_ShippingAddrCPValdationUtility.validateAddress(addSer);       
     Test.stopTest();
    }
   
     private static testmethod void testCanadaPostValidationfail() {
     
     PACConfig__c shippingdetails = new PACConfig__c(Name='shipping',
                                                     End_Point_URL__c='https://ws1.postescanada-canadapost.ca/AddressComplete/Interactive/Find/v2.10/json3.ws?callback=?',
                                                     Second_Point_URL__c='https://ws1.postescanada-canadapost.ca/AddressComplete/Interactive/Retrieve/v2.10/json3.ws?callback=?',
                                                     API_Key__c='ZJ21-RD89-UB86-RJ69',
                                                     Shipping_Utility_Second_URL__c= 'https://ws1.postescanada-canadapost.ca/AddressComplete/Interactive/Retrieve/v2.10/json3.ws?callback=?');  shippingdetails.Source_System__c ='q';
     shippingdetails.Search_For__c='q'; shippingdetails.Max_Suggestions__c=2; shippingdetails.Max_Results__c=10;
     shippingdetails.DelayToAvoidMultipleHits__c='q';
     insert shippingdetails;
     
     OCOM_ShippingAddrCPValdationUtility clsObj = new OCOM_ShippingAddrCPValdationUtility();
     OCOM_ShippingAddrCPValdationUtility.serviceAddressData addSer = new OCOM_ShippingAddrCPValdationUtility.serviceAddressData();
     addSer.postalCode='T1A1T6';addSer.streetName='11 ST SE';addSer.buildingNumber='847';
    
      PACConfig__c custData= PACConfig__c.getValues('shipping');
      String url;String reteriveURl;
        if(null!=custData){
            string searchItem=EncodingUtil.urlEncode(addSer.postalCode+' '+addSer.buildingNumber+' '+addSer.streetName,'UTF-8');
            String CanadaPostKey = custData.API_Key__c;
            String findPreURl =  (custData.End_Point_URL__c.remove('callback=?'))+'&Key='+CanadaPostKey+'&SearchTerm=';
            String findPostURl = '&LastId=&SearchFor=Everything&Country=CAN&LanguagePreference=en&MaxSuggestions=0&MaxResults=1&OrderBy=UserLocation';
                   reteriveURl = (custData.Shipping_Utility_Second_URL__c.remove('callback=?'))+'&Key='+CanadaPostKey+'&Id=';
            url = findPreURl +searchItem+findPostURl ;
        } 
     Test.startTest();    
     Test.setMock(HttpCalloutMock.class, new CanadaPostSecondCallout());     
     HttpRequest req = new HttpRequest();
     req.setEndpoint(url);
     req.setMethod('GET');
     Http h = new Http();
     HttpResponse res = h.send(req);
     OCOM_ShippingAddrCPValdationUtility.validateAddress(addSer);       
     Test.stopTest();
    }
    
     private static testmethod void testCanadaPostValidationfail2() {
     OCOM_ShippingAddrCPValdationUtility clsObj = new OCOM_ShippingAddrCPValdationUtility();
     OCOM_ShippingAddrCPValdationUtility.serviceAddressData addSer = new OCOM_ShippingAddrCPValdationUtility.serviceAddressData();
     addSer.postalCode='T1A1T6';addSer.streetName='11 ST SE';addSer.buildingNumber='847';    
     Test.startTest();      
     OCOM_ShippingAddrCPValdationUtility.validateAddress(addSer);       
     Test.stopTest();
    } 
   public class CanadaPostFirstCallout implements HttpCalloutMock {
   		// Implement this interface method
   public HTTPResponse respond(HTTPRequest req) {
       PACConfig__c custData= PACConfig__c.getValues('shipping');
       String reteriveURl;
       if(null!=custData){
           String CanadaPostKey = custData.API_Key__c;
           reteriveURl = (custData.Shipping_Utility_Second_URL__c.remove('callback=?'))+'&Key='+CanadaPostKey+'&Id=';
       } 
       string id=EncodingUtil.urlEncode('CAN|B|10625245','UTF-8');
       string url=reteriveURl+id;
        System.assertEquals('GET', req.getMethod());        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
       system.debug('!!!getEndpoint '+req.getEndpoint());
       if(url != req.getEndpoint()){
       	 res.setBody('{"Items":[{"Id":"CAN|B|10625245","Text":"847 11 St Se, Medicine Hat, AB","Highlight":"","Cursor":0,"Description":"","Next":"Retrieve"}]}');    
       }else{
         res.setBody('{"Items":[{"Id":"CA|CP|B|10625245","DomesticId":"10625245","Language":"ENG","LanguageAlternatives":"ENG,FRE","Department":"","Company":"","SubBuilding":"","BuildingNumber":"847","BuildingName":"","SecondaryStreet":"","Street":"11 St SE","Block":"","Neighbourhood":"","District":"","City":"Medicine Hat","Line2":"","Line3":"","Line4":"","Line5":"","AdminAreaName":"","AdminAreaCode":"","Province":"AB","ProvinceName":"Alberta","ProvinceCode":"AB","PostalCode":"T1A 1T6","CountryName":"Canada","CountryIso2":"CA","CountryIso3":"CAN","CountryIsoNumber":124,"SortingNumber1":"","SortingNumber2":"","Barcode":"","POBoxNumber":"","Label":"847 11 St SEMEDICINE HAT AB T1A 1T6CANADA","Type":"Residential","DataLevel":"Premise"}]}');  
       }
       
        res.setStatusCode(200);
        return res;
    }
  } 
  public class CanadaPostSecondCallout implements HttpCalloutMock {
   public HTTPResponse respond(HTTPRequest req) {
       PACConfig__c custData= PACConfig__c.getValues('shipping');
       String reteriveURl;
       if(null!=custData){
           String CanadaPostKey = custData.API_Key__c;
           reteriveURl = (custData.Shipping_Utility_Second_URL__c.remove('callback=?'))+'&Key='+CanadaPostKey+'&Id=';
       } 
       string id=EncodingUtil.urlEncode('CAN|B|10625245','UTF-8');
       string url=reteriveURl+id;
        System.assertEquals('GET', req.getMethod());        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
       system.debug('!!!getEndpoint '+req.getEndpoint());
       if(url != req.getEndpoint()){
       	 res.setBody('{"Items":[{"Id":"CAN|B|10625245","Text":"847 11 St Se, Medicine Hat, AB","Highlight":"","Cursor":0,"Description":"","Next":"Retrieve"}]}');    
       }else{
         res.setBody('{"Items":[{"Id":"CA|CP|B|10625245","DomesticId":"10625245","Language":"ENG","LanguageAlternatives":"ENG,FRE","Department":"","Company":"","SubBuilding":"","BuildingNumber":"845","BuildingName":"","SecondaryStreet":"","Street":"11 St SE","Block":"","Neighbourhood":"","District":"","City":"Medicine Hat","Line2":"","Line3":"","Line4":"","Line5":"","AdminAreaName":"","AdminAreaCode":"","Province":"AB","ProvinceName":"Alberta","ProvinceCode":"AB","PostalCode":"T1A 1T6","CountryName":"Canada","CountryIso2":"CA","CountryIso3":"CAN","CountryIsoNumber":124,"SortingNumber1":"","SortingNumber2":"","Barcode":"","POBoxNumber":"","Label":"847 11 St SEMEDICINE HAT AB T1A 1T6CANADA","Type":"Residential","DataLevel":"Premise"}]}');  
       }
       
        res.setStatusCode(200);
        return res;
    }
 }
}