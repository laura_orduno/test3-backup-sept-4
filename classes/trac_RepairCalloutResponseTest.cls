@isTest
private class trac_RepairCalloutResponseTest {
private static testMethod void testExceptionMessageCtor() {
			final String TEST_MESSAGE = 'It broke'; 
			
			trac_RepairCalloutResponse resp;
			try {
				throw new trac_RepairCalloutResponse.TestException(TEST_MESSAGE);
			} catch (Exception e) {
				resp = new trac_RepairCalloutResponse(e);
			}
			/*
			system.assertEquals(trac_RepairCalloutResponse.MANUAL_ERROR_MSG_TYPE,resp.messageType);
			system.assertEquals(TEST_MESSAGE,resp.messageBody);
*/
		}
		
		
		private static testMethod void testCtorEmptyStatus() {
			trac_RepairCalloutResponse resp = 
				new trac_RepairCalloutResponse(new svc_RepairRequestValidationSvcRequestR_2.validateRepairRequestResponse_element());
			/*
			system.assertEquals(false,resp.success);
			system.assertEquals(trac_RepairCalloutResponse.MANUAL_ERROR_MSG_TYPE,resp.messageType);
			system.assertEquals(Label.UNKNOWN_RESPONSE,resp.messageBody);
*/
		}
		
		
		private static testMethod void testCtorSuccessStatus() {
			svc_RepairRequestValidationSvcRequestR_2.validateRepairRequestResponse_element validationResponse =
				new svc_RepairRequestValidationSvcRequestR_2.validateRepairRequestResponse_element();
				
			validationResponse.validationStatus = trac_RepairCalloutResponse.SUCCESS_STRING;
			
			trac_RepairCalloutResponse resp = new trac_RepairCalloutResponse(validationResponse);
			
			system.assertEquals(true,resp.success);
		}
		
		
		private static testMethod void testCtorWarningStatus() {
			system.runas(trac_RepairTestUtils.getTestUser()) {
				svc_RepairRequestValidationSvcRequestR_2.validateRepairRequestResponse_element validationResponse =
					new svc_RepairRequestValidationSvcRequestR_2.validateRepairRequestResponse_element();
					
				validationResponse.validationStatus = trac_RepairCalloutResponse.SUCCESS_STRING;
                validationResponse.validationMessageList=new svc_EnterpriseCommonTypes_v7.ValidationMessageList();
                validationResponse.validationMessageList.validationMessage=new list<svc_EnterpriseCommonTypes_v7.ResponseMessage>();
				svc_EnterpriseCommonTypes_v7.ResponseMessage rMessage = new svc_EnterpriseCommonTypes_v7.ResponseMessage();
	      rMessage.messageList = new List<svc_EnterpriseCommonTypes_v7.Message>();
	      rMessage.dateTimeStamp = Datetime.newInstance(2012,11,22,22,18,30);
	      rMessage.errorCode = 'SUBSCRIBER_NOT_FOUND';
	      rMessage.messageType = 'WARNING';
				validationResponse.validationMessageList.validationMessage.add(rMessage);
				svc_EnterpriseCommonTypes_v7.Message message = new svc_EnterpriseCommonTypes_v7.Message();
	      message.locale = 'EN_CA';
	      message.message = 'Subscriber is Not Found.';
	      
	      rMessage.messageList.add(message);
				
				trac_RepairCalloutResponse resp = new trac_RepairCalloutResponse(validationResponse);
				
				/*
				system.assertEquals(true,resp.success);
				system.assertEquals('WARNING',resp.messageType);
				system.assertEquals('SUBSCRIBER_NOT_FOUND',resp.errorCode);
				system.assertEquals('Subscriber is Not Found.',resp.messageBody);
*/
			}
		}
		
		
		private static testMethod void testCtorFailureStatus() {
			system.runas(trac_RepairTestUtils.getTestUser()) {
				svc_RepairRequestValidationSvcRequestR_2.validateRepairRequestResponse_element validationResponse =
					new svc_RepairRequestValidationSvcRequestR_2.validateRepairRequestResponse_element();
					
				validationResponse.validationStatus = trac_RepairCalloutResponse.FAILURE_STRING;
                validationResponse.validationMessageList=new svc_EnterpriseCommonTypes_v7.ValidationMessageList();
                validationResponse.validationMessageList.validationMessage=new list<svc_EnterpriseCommonTypes_v7.ResponseMessage>();
				svc_EnterpriseCommonTypes_v7.ResponseMessage rMessage = new svc_EnterpriseCommonTypes_v7.ResponseMessage();
	      rMessage.messageList = new List<svc_EnterpriseCommonTypes_v7.Message>();
	      rMessage.dateTimeStamp = Datetime.newInstance(2012,11,22,22,18,30);
	      rMessage.errorCode = 'PRODUCT_NOT_FOUND';
	      rMessage.messageType = 'ERROR';
	      
	      svc_EnterpriseCommonTypes_v7.Message message = new svc_EnterpriseCommonTypes_v7.Message();
	      message.locale = 'EN_CA';
	      message.message = 'Product Info is Not Found.';
	      
	      rMessage.messageList.add(message);
				
				trac_RepairCalloutResponse resp = new trac_RepairCalloutResponse(validationResponse);
				/*
				system.assertEquals(false,resp.success);
				system.assertEquals('ERROR',resp.messageType);
				system.assertEquals('PRODUCT_NOT_FOUND',resp.errorCode);
				system.assertEquals('Product Info is Not Found.',resp.messageBody);
*/
			}
		}
		
		
		private static testMethod void testCtorFailureStatusMissingMessage() {
			svc_RepairRequestValidationSvcRequestR_2.validateRepairRequestResponse_element validationResponse =
				new svc_RepairRequestValidationSvcRequestR_2.validateRepairRequestResponse_element();
				
			validationResponse.validationStatus = 'FAILED';
                validationResponse.validationMessageList=new svc_EnterpriseCommonTypes_v7.ValidationMessageList();

                validationResponse.validationMessageList.validationMessage=new list<svc_EnterpriseCommonTypes_v7.ResponseMessage>();
				svc_EnterpriseCommonTypes_v7.ResponseMessage rMessage = new svc_EnterpriseCommonTypes_v7.ResponseMessage();

	      rMessage.messageList = new List<svc_EnterpriseCommonTypes_v7.Message>();
      rMessage.dateTimeStamp = Datetime.newInstance(2012,11,22,22,18,30);
      rMessage.errorCode = 'PRODUCT_NOT_FOUND';
      rMessage.messageType = 'ERROR';
			
	      svc_EnterpriseCommonTypes_v7.Message message = new svc_EnterpriseCommonTypes_v7.Message();
	      message.locale = 'EN_CA';
	      message.message = 'Product Info is Not Found.';
	      rMessage.messageList.add(message);
			trac_RepairCalloutResponse resp = new trac_RepairCalloutResponse(validationResponse);
			/*
			system.assertEquals(false,resp.success);
			system.assertEquals('ERROR',resp.messageType);
			system.assertEquals('PRODUCT_NOT_FOUND',resp.errorCode);
			system.assertEquals('PRODUCT_NOT_FOUND',resp.messageBody);
*/
		}
		
		
		private static testMethod void testCtorFailureStatusMissingMessageAndErrorCode() {
			svc_RepairRequestValidationSvcRequestR_2.validateRepairRequestResponse_element validationResponse =
				new svc_RepairRequestValidationSvcRequestR_2.validateRepairRequestResponse_element();
				
			validationResponse.validationStatus = trac_RepairCalloutResponse.FAILURE_STRING;
                validationResponse.validationMessageList=new svc_EnterpriseCommonTypes_v7.ValidationMessageList();
                validationResponse.validationMessageList.validationMessage=new list<svc_EnterpriseCommonTypes_v7.ResponseMessage>();
				svc_EnterpriseCommonTypes_v7.ResponseMessage rMessage = new svc_EnterpriseCommonTypes_v7.ResponseMessage();
      rMessage.dateTimeStamp = Datetime.newInstance(2012,11,22,22,18,30);
      rMessage.messageType = 'ERROR';
			
			trac_RepairCalloutResponse resp = new trac_RepairCalloutResponse(validationResponse);
			/*
			system.assertEquals(false,resp.success);
			system.assertEquals('ERROR',resp.messageType);
			system.assertEquals(Label.UNKNOWN_RESPONSE,resp.messageBody);
*/
		}
		
		
		private static testMethod void testInitCreateRepairCtorSuccess() {
			rpm_EquipmentRepairManagementSvcRequestR.initCreateRepairResponse_element createResp =
				new rpm_EquipmentRepairManagementSvcRequestR.initCreateRepairResponse_element();
				
			createResp.requestKey = '12345';
			
			trac_RepairCalloutResponse resp = new trac_RepairCalloutResponse(createResp);
			
			//system.assertEquals(true,resp.success);
		}
		
		
		private static testMethod void testInitCreateRepairCtorFailure() {
			rpm_EquipmentRepairManagementSvcRequestR.initCreateRepairResponse_element createResp =
				new rpm_EquipmentRepairManagementSvcRequestR.initCreateRepairResponse_element();
				
			createResp.requestKey = '12345';
			createResp.errorCode = 'Anything here indicates an error';
			
			trac_RepairCalloutResponse resp = new trac_RepairCalloutResponse(createResp);
			
			//system.assertEquals(false,resp.success);
		}
		
		
		private static testMethod void testGetRepairCtorSuccess() {
			rpm_EquipmentRepairInformationSvcRequest.getRepairResponse_element getRepairResp = 
				new rpm_EquipmentRepairInformationSvcRequest.getRepairResponse_element();
			
			trac_RepairCalloutResponse resp = new trac_RepairCalloutResponse(getRepairResp);
			
			//system.assertEquals(true,resp.success);
		}
		
		
		private static testMethod void testGetRepairCtorFailure() {
			rpm_EquipmentRepairInformationSvcRequest.getRepairResponse_element getRepairResp = 
				new rpm_EquipmentRepairInformationSvcRequest.getRepairResponse_element();
			
			getRepairResp.errorCode = 'Anything here indicates an error';
			
			trac_RepairCalloutResponse resp = new trac_RepairCalloutResponse(getRepairResp);
			
			//system.assertEquals(false,resp.success);
		}
		
		
		private static testMethod void testUpdateRepairQuoteCtorSuccess() {
			rpm_EquipmentRepairManagementSvcRequestR.updateEquipmentRepairQuotationResponse_element quoteRepairResp = 
				new rpm_EquipmentRepairManagementSvcRequestR.updateEquipmentRepairQuotationResponse_element();
			
			trac_RepairCalloutResponse resp = new trac_RepairCalloutResponse(quoteRepairResp);
			
			//system.assertEquals(true,resp.success);
		}
		
		
		private static testMethod void testUpdateRepairQuoteCtorFailure() {
			rpm_EquipmentRepairManagementSvcRequestR.updateEquipmentRepairQuotationResponse_element quoteRepairResp = 
				new rpm_EquipmentRepairManagementSvcRequestR.updateEquipmentRepairQuotationResponse_element();
			
			quoteRepairResp.errorCode = 'Anything here indicates an error';
			
			trac_RepairCalloutResponse resp = new trac_RepairCalloutResponse(quoteRepairResp);
			
			//system.assertEquals(false,resp.success);
		}
}