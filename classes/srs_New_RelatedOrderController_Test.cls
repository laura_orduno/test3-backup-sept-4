/*******************************************************************************
 * Project      : SRS R3
 * Class        : srs_New_RelatedOrderController_Test 
 * Description  : Test class for SRS Related Order Search and create page Controller class
 * Author       : Aditya Jamwal, IBM
 * Date         : July-18-2014
 *
 * Modification Log:
 * ------------------------------------------------------------------------------
 * Ver      Date                Modified By           Description
 * ------------------------------------------------------------------------------
 * 
 * ------------------------------------------------------------------------------
 *******************************************************************************/
@isTest
private class srs_New_RelatedOrderController_Test {

        static testMethod void srs_New_RelatedOrderController_test() {
        contact c= new contact(firstname='aditya',lastname='jamwal');
        insert c;   
        
        System.assertEquals('aditya',c.firstname); 
        
        Opportunity opp= new Opportunity(Name='OppTest', StageName='Originated', CloseDate=system.today(),Primary_Order_Contact__c=c.id);
        insert Opp;
        Service_Request__c sereq  = new Service_Request__c(opportunity__c=opp.id,Service_Request_Status__c='completed');
        Service_Request__c sereq1  = new Service_Request__c(opportunity__c=opp.id,Service_Request_Status__c='completed');
        List<Service_Request__c> lstSR=new List<Service_Request__c>();
        lstSR.add(sereq);
        lstSR.add(sereq1);
        
        System.assertEquals('OppTest',opp.Name);     
         
        
        insert lstSR;
        List<SRS_Service_Request_Related_Order__c> lstRelOrd=new List<SRS_Service_Request_Related_Order__c>();
        
        SRS_Service_Request_Related_Order__c relatedOrder=new SRS_Service_Request_Related_Order__c();
        relatedOrder.seq__C='01';
        relatedOrder.Name='C6042784412';
        relatedOrder.isValid__C=True;
        relatedOrder.System__c='CRIS3-AB';
        relatedOrder.RO_Service_Request__c=sereq.id;
        relatedOrder.recordtypeid=Schema.SObjectType.SRS_Service_Request_Related_Order__c.getRecordTypeInfosByName().get('CRIS3-AB').getRecordTypeId();
        
        SRS_Service_Request_Related_Order__c relatedOrder1=new SRS_Service_Request_Related_Order__c();
        relatedOrder1.seq__C='01';
        relatedOrder1.Name='C6042784412';
        relatedOrder1.isValid__C=True;
        relatedOrder1.System__c='CRIS3-AB';
        relatedOrder1.RO_Service_Request__c=sereq1.id;
        relatedOrder1.recordtypeid=Schema.SObjectType.SRS_Service_Request_Related_Order__c.getRecordTypeInfosByName().get('CRIS3-AB').getRecordTypeId();
        
        lstRelOrd.add(relatedOrder);
        
        insert lstRelOrd;  
        
        System.assertEquals('completed',sereq.Service_Request_Status__c);
        
        ApexPAges.StandardController sc = new ApexPages.StandardController(relatedOrder);
        srs_New_RelatedOrderController src= new srs_New_RelatedOrderController(sc);
        
        srs_New_RelatedOrderController.RelatedOrder RO= new srs_New_RelatedOrderController.RelatedOrder();
        srs_New_RelatedOrderController.clsCheckROValidity objClsChkRoValidity=new srs_New_RelatedOrderController.clsCheckROValidity('test',RO,false);
        List<srs_New_RelatedOrderController.RelatedOrder> relatedOrders = new List<srs_New_RelatedOrderController.RelatedOrder>();
        list<smb_orders_trackingtypes_v4.OrderRequest> LegacyOrders= new list<smb_orders_trackingtypes_v4.OrderRequest>();
        List<smb_orders_trackingtypes_v4.OrderRequestAddress> LAdd = new List<smb_orders_trackingtypes_v4.OrderRequestAddress>();
        smb_orders_trackingtypes_v4.OrderRequestAddress add= new smb_orders_trackingtypes_v4.OrderRequestAddress();
        smb_orders_trackingtypes_v4.OrderRequestKey orderKey = new smb_orders_trackingtypes_v4.OrderRequestKey();
        smb_orders_trackingtypes_v4.Customer customer = new smb_orders_trackingtypes_v4.Customer();
        smb_orders_trackingtypes_v4.OrderRequestStatus orderStatusSet = new smb_orders_trackingtypes_v4.OrderRequestStatus();
        smb_orders_trackingtypes_v4.OrderRequestType orderTypeSet = new smb_orders_trackingtypes_v4.OrderRequestType();
        add.address='aa';
        add.city='asa';
        add.province='ON';
        LAdd.add(add);
        
        smb_orders_trackingtypes_v4.OrderRequest legOrder= new smb_orders_trackingtypes_v4.OrderRequest();
        
        orderKey.legacyOrderId='1232322';
        orderKey.sequenceNum='12';
        orderKey.systemName='CRSI3';
         orderKey.circuitNumber='sas';    
        customer.customerName='Abc';
        orderStatusSet.orderStatus='xyz';
        orderTypeSet.orderType='I';
        legOrder.customer=customer;
        legOrder.orderKey=orderKey;
        legOrder.orderAddress=LAdd;
        
        legOrder.orderStatusSet=orderStatusSet;
        legOrder.orderTypeSet=orderTypeSet;
		
		//START :: TQ TF Case 2199808, BR: S2CP-54 :: added by Rajan
		smb_orders_trackingtypes_v4.DueDate dueDateObj = new smb_orders_trackingtypes_v4.DueDate();
		dueDateObj.dueDate = System.now();
		legOrder.orderDueDateSet = dueDateObj;
		//END :: TQ TF Case 2199808, BR: S2CP-54 :: added by Rajan
		
        LegacyOrders.add(legOrder);
        RO.checked=true;
        relatedOrders.add(RO);        
        src.relatedOrders=relatedOrders;
        src.srId=sereq.id;
        //....Public Methods....
        src.getItems();
        src.Cancel();
        src.AddtoSR();
        src.isManualOrder();
        src.cancelRO();
        src.saveRO();
        src.selectedValue=Schema.SObjectType.SRS_Service_Request_Related_Order__c.getRecordTypeInfosByName().get('CRIS3-AB').getRecordTypeId();
        src.changeRecordType();
        src.getmapRecTypeIdWithName();
        src.getSystemValues();
        
        src.ConvertResponsetoRelatedOrder(LegacyOrders);
        src.SortwithColumn();
        src.getSearchTypeItems();
        src.getSortDirection();
        src.setSortDirection('Desc');   
        src.setCheckAll(); 
        src.redirectToRO();
        lstRelOrd.add(relatedOrder1);
        upsert lstRelOrd;   
        src.SaveRO();     
        //.......................
        List<srs_New_RelatedOrderController.RelatedOrder> ROList= new List<srs_New_RelatedOrderController.RelatedOrder>();
        //sorting by customerName  
        srs_New_RelatedOrderController.RelatedOrder RO1= new srs_New_RelatedOrderController.RelatedOrder();
        ro1.originDate= system.now();     
        ro1.customerName='rte';    
        ROList.add(ro1);    
        srs_New_RelatedOrderController.SORT_FIELD='customerName';
        srs_New_RelatedOrderController.SORT_DIR = 'ASC';  
        ROList.sort();
        //sorting by origindate        
        ROList.add(ro);    
        srs_New_RelatedOrderController.SORT_FIELD='customerName';
        srs_New_RelatedOrderController.SORT_DIR = 'ASC';        
        ROList.sort();    
        //sorting by origindateString        
        ROList.add(ro);    
        srs_New_RelatedOrderController.SORT_FIELD='originDateString';
        srs_New_RelatedOrderController.SORT_DIR = 'ASC';        
        ROList.sort();
        
        //sorting by origindate 
        try{       
        ROList.add(ro);    
        srs_New_RelatedOrderController.SORT_FIELD='originDate';
        srs_New_RelatedOrderController.SORT_DIR = 'ASC';        
        ROList.sort();
        }catch(exception e){}
        
        //sorting by seqNumber
        ROList.add(ro);    
        srs_New_RelatedOrderController.SORT_FIELD='seqNumber';
        srs_New_RelatedOrderController.SORT_DIR = 'ASC';        
        ROList.sort();
        
            //sorting by orderNumber        
        ROList.add(ro);    
        srs_New_RelatedOrderController.SORT_FIELD='orderNumber';
        srs_New_RelatedOrderController.SORT_DIR = 'ASC';        
        ROList.sort();
            //sorting by systemNum        
        ROList.add(ro);    
        srs_New_RelatedOrderController.SORT_FIELD='systemNum';
        srs_New_RelatedOrderController.SORT_DIR = 'ASC';        
        ROList.sort();
         //sorting by status        
        ROList.add(ro);    
        srs_New_RelatedOrderController.SORT_FIELD='status';
        srs_New_RelatedOrderController.SORT_DIR = 'ASC';        
        ROList.sort();  
         //sorting by serviceAddress        
        ROList.add(ro);    
        srs_New_RelatedOrderController.SORT_FIELD='serviceAddress';
        srs_New_RelatedOrderController.SORT_DIR = 'DESC';        
        ROList.sort();   
              //sorting by city        
        ROList.add(ro);    
        srs_New_RelatedOrderController.SORT_FIELD='city';
        srs_New_RelatedOrderController.SORT_DIR = 'DESC';        
        ROList.sort(); 
         //sorting by province        
        ROList.add(ro);    
        srs_New_RelatedOrderController.SORT_FIELD='province';
        srs_New_RelatedOrderController.SORT_DIR = 'DESC';        
        ROList.sort();     
        //sorting by originator        
        ROList.add(ro);    
        srs_New_RelatedOrderController.SORT_FIELD='originator';
        srs_New_RelatedOrderController.SORT_DIR = 'DESC';        
        ROList.sort();     
        //.. Filterset
        srs_New_RelatedOrderController.RelatedOrder r1= new srs_New_RelatedOrderController.RelatedOrder();
        r1.orderNumber='stir';
        r1.seqNumber='stir';
        r1.systemNum='stir';
        r1.customerName='stir';
        r1.status='stir';
        r1.OrderType='I';
        r1.serviceAddress='stir';
        r1.city='stir';
        r1.originator='stir';
        r1.originDate=Date.today();
        relatedOrders.add(r1);
            src.FilterSet[0].strValue='stir';
            src.FilterSet[1].strValue='stir';
            src.FilterSet[2].strValue='stir';
            src.FilterSet[3].strValue='stir';
            src.FilterSet[4].strValue='stir';
            src.FilterSet[5].strValue='stir';
            src.FilterSet[6].strValue='stir';
            src.FilterSet[7].strValue='stir';
            src.FilterSet[8].strValue='stir';
            src.FilterSet[9].dateValue=Date.today();
            src.FilterSet[9].dateString=String.valueof(Date.today());
            src.FilterSet[4].CompareString('stir');
            src.FilterSet[4].active=true;
            src.FilterSet[4].CompareString('stir');
            src.FilterSet[9].CompareDateTime(system.now());
            src.FilterSet[9].CompareDate(system.today());
            src.FilterSet[9].active=true;
            src.FilterSet[9].CompareDateTime(system.now());
            src.FilterSet[9].CompareDate(system.today());
            src.FilterSet.sort();
            src.filter();
            try{src.customerName='stir';
                src.searchLegacyOrder(); }
            catch(exception e)
            {}
             try{src.OrderNo='1234';
                 src.searchType='Order#';
                src.searchLegacyOrder(); }
            catch(exception e)
            {}
            SRS_Service_Request_Related_Order__c relOrd=new SRS_Service_Request_Related_Order__c();
             ApexPAges.StandardController stdcon = new ApexPages.StandardController(relOrd);
            srs_New_RelatedOrderController src1=new srs_New_RelatedOrderController(stdcon );
            src1.ManualRelatedOrder.isManual__c=true;
            src1.AddtoSR();
            src1.redirectToRO();
            src1.isManualOrder();
            src1.checkAll=true;
            src1.setUncheckAll();
            src1.SearchTypefun();
            src1.searchLegacyOrder();
            src1.redirectToRO();
            //src1.CustomerName='test';
            src1.searchtype='';
            src1.searchLegacyOrder();
            //src1.searchtype='Order#';
            //src1.OrderNo='123';
            //src1.searchLegacyOrder();
            src1.searchtype='Customer Name';
            src1.searchLegacyOrder();
            src1.CustomerName='test';
            src1.searchLegacyOrder();
            srs_New_RelatedOrderController.FilterWrapper fil= new srs_New_RelatedOrderController.FilterWrapper('T','N',1);
			fil.active=true;
			fil.CompareDate(null);
            
    }
    
    
}