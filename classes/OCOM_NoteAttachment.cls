/*
    *******************************************************************************************************************************
    Class Name:     OCOM_NoteAttachment
    Purpose:        Object to accommodate notes or attachments        
    Created by:     Thomas Su             30-Dec-2016     QC-9301       No previous version
    Modified by:    
    *******************************************************************************************************************************
*/

public class OCOM_NoteAttachment implements Comparable {
    public String id {get;set;}
    public String type {get;set;}
    public String typeDisplay {get;set;}
    public String title {get;set;}
    public String createdByName {get;set;}
    public Id createdBy {get;set;}
    public DateTime createdDate {get;set;}
    public String lastModifiedDate {get;set;}
    
    public Integer compareTo(Object compareTo) {
        OCOM_NoteAttachment that = (OCOM_NoteAttachment) compareTo;
        if(this.createdDate < that.createdDate)
            return 1;
        else if(this.createdDate > that.createdDate)
            return -1;
        else
            return 0;
    }
}