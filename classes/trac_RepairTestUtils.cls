/*
 * Generates data for testing
 * Part of the SMB Repairs project
 *
 * @author: Grant Adamson, Traction On Demand 
 */

@isTest
public class trac_RepairTestUtils {
	public static final String VALIDATE_ENDPOINT = 'www.example.com/validate';
	public static final String MANAGEMENT_ENDPOINT = 'www.example.com/management';
	public static final String INFORMATION_ENDPOINT = 'www.example.com/information';
	public static final String FRENCH_DEFECT_BEHAVIOUR_VALUE = 'Panne à chaud';
	
	public static final String REPAIR_ESN_SUCCESS = '000000000000000';
	public static final String REPAIR_ESN_WARNING = '000000000000001';
	public static final String REPAIR_ESN_NOT_FOUND = '000000000000002';
	public static final String REPAIR_ESN_NO_MATCH = '000000000000003';
	public static final String REPAIR_ESN_EXCEPTION = '000000000000004';
	public static final String REPAIR_ESN_FAILURE = '000000000000005';
	public static final String REPAIR_ESN_LONG_EXCEPTION = '000000000000006';
	
	public static final String REPAIR_ID_SUCCESS = '0000';
	public static final String REPAIR_ID_WARNING = '0001';
	public static final String REPAIR_ID_NOT_FOUND = '0002';
	public static final String REPAIR_ID_NO_MATCH = '0003';
	public static final String REPAIR_ID_EXCEPTION = '0004';
	public static final String REPAIR_ID_FAILURE = '0005';
	public static final String REPAIR_ID_LONG_EXCEPTION = '0006';
	public static final String REPAIR_ID_EMPTY = '0007';
	public static final String REPAIR_ID_USE_CASE_ID = '0008';
	
	
	public static void createTestEndpoint() {
		String postfix = (trac_RepairCallout.isProduction() ? '' : '_test');
		
		insert new Repair_WS_Endpoints__c(name = 'validate' + postfix,
																			endpoint__c = VALIDATE_ENDPOINT,
																			username__c = 'username',
																			password__c = 'password',
																			timeout__c = 20000);
		insert new Repair_WS_Endpoints__c(name = 'repairmgmt' + postfix,
																			endpoint__c = MANAGEMENT_ENDPOINT,
																			username__c = 'username',
																			password__c = 'password',
																			timeout__c = 10000);
		insert new Repair_WS_Endpoints__c(name = 'repairinfo' + postfix,
																			endpoint__c = INFORMATION_ENDPOINT,
																			username__c = 'username',
																			password__c = 'password',
																			timeout__c = 10000);			
	}
	

	public static User getTestUser() {
		Id PROFILEID = [SELECT id FROM Profile WHERE name='Standard User' OR name='Utilisateur standard' LIMIT 1].id;
		return new User(username = 'NOIDVMDFJREFSRI@TRACTIONSM.COM',
						  email = 'test@example.com',
						  title = 'test',
						  lastname = 'test',
						  alias = 'test',
						  TimezoneSIDKey = 'America/Los_Angeles',
						  LocaleSIDKey = 'en_US',
						  EmailEncodingKey = 'UTF-8',
						  ProfileId = PROFILEID,
						  LanguageLocaleKey = 'en_US',
						  Team_TELUS_Id__c = 'test');
	}
	
	public static String getFrenchDefectBehaviourTestBody() {
		return '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">\n' +
					 '<html><head></head><span id="page:field">' + FRENCH_DEFECT_BEHAVIOUR_VALUE + '</span></html>';
	}
}