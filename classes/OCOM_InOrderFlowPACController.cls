/*******************************************************************************
     * Project      : OCOM/US-1138
     * Helper       : OCOM_InOrderFlowPACController
     * Test Class   : OCOM_InOrderFlowPACController_Test
     * Description  : Apex class for OCOM_InOrderFlowPAC VF page that allows Agents to capture Address inside OrderFlow
     * Author       : Arvind Yadav
     * Date         : July-18-2016
     *
     * Modification Log:
     * ------------------------------------------------------------------------------
     * Ver      Date                Modified By           Description
     * ------------------------------------------------------------------------------
     * 1.0
     * 2.0      10-Aug-2016         Wael Elkhawass        JIRA: OCOM-1202
     * 3.0     15 March 2017            Sandip Chaudhari    Defect #11440, 11914     
     * 4.0     10 April 2017         Sandip Chaudhari        Defect #12343 - Added code to check ILEC and Non ILEC Province 
     
     *******************************************************************************/

global without sharing class OCOM_InOrderFlowPACController {
    public Account rcidAccount  {get;set;}
    // Added by Wael - Start OCOM-1202  & OCOM-1207
    public String smbCareAddrId {get;set;}
    public String moveOutAddrId {get;set;}
    public String contactId {get;set;}
    public String orderId {get;set;}
    public String src {get;set;}
    public String oppId {get;set;}
    public String primaryContactId {get;set;}
    // Wael - End OCOM-1202 & OCOM-1207
    
    // Added by Wael - Start OCOM-1222
    public String orderSmbCareAddrId {get;set;}
    // Wael - End OCOM-1222
    
    //Added by Arvind as part of display the additional fields receiving via ServiceAddressMgmtService V1.1 (US-OCOM-1101)

    public String dropInfo { 
        get{
            String retVal = String.isNotBlank(dropExceptionCode) ? dropExceptionCode : '-';
            retVal += '/' + (String.isNotBlank(dropFacilityCode) ? dropFacilityCode : '-');
            retVal += '/' + (String.isNotBlank(dropTypeCode) ? dropTypeCode : '-');
            retVal += '/' + (String.isNotBlank(dropLength) ? dropLength : '-');
            return retVal;
        }
        set; 
    }
    public String dropFacilityCode { get; set; }
    public String dropTypeCode { get; set; }
    public String dropLength { get; set; }
    public String dropExceptionCode { get; set; }
    public String currentTransportTypeCode { 
        get{
            return (currentTransportTypeCode == 'F') ? 'Fibre' : 'Copper';
        } 
        set; 
    }
    public String futureTransportTypeCode { get; set; }
    public String futurePlantReadyDate { get; set; }
    public String futureTransportRemarkTypeCode { get; set; }
    public String gponBuildTypeCode { 
        get{
            return (gponBuildTypeCode == 'E') ? 'Sell as you build' : 'No';
        }
        set; 
    }
    public String provisioningSystemCode { 
        get{
            return (provisioningSystemCode == 'F') ? 'FIFA' : 'Compass';
        } 
        set; 
    }
    public String ratingNpaNxx { get; set; }
    public String COID { get; set; }
    
    //
    
    public Account serviceAccount {get;set;}
    public Account moveOutServiceAccount {get;set;}
    //public string FLValue = null;
    public ServiceAddressData SAData {get;set;}
    public ServiceAddressData moveOutSAData {get;set;}
    public SMBCare_Address__c smbCareAddr {get;set;}
    public SMBCare_Address__c moveOutAddr {get;set;}
    public String ContId{get;set;}
    public static String orderPath{get;set;}
    public String existingServices{get;set;}
    

    
    public String orderType{get;set;}
    public List<String> errorMessages{get;set;}
    public Boolean showErrorMsg{
        get{
            return (errorMessages.size() > 0);
        }
        set;} 
    public static Id acctId;
    public Boolean blnGTACall = False; 
    public id oid{get;set;}
    public Boolean displayPageComp{get;set;}
    public Boolean addrNotValid{get;set;}
    public String tvAvailability{get;set;}
    public String forborne{get;set;}
    public String maximumSpeed{get;set;}
    public Boolean maxSpeedCopper{get;set;}

    private String caseIds;
    public Boolean isASFDown{get;set;} //31 March 2017 - Defect # 11733 - Added by Sandip
    
    public OCOM_InOrderFlowPACController(){
        System.debug('SAData##'+SAData);
         String acctIdStr = System.currentPageReference().getParameters().get('id');
        acctId=null;
        if(String.isNotBlank(acctIdStr) && acctIdStr instanceof Id){
            acctId=Id.valueOf(acctIdStr);
        }
        //acctId = System.currentPageReference().getParameters().get('id');
        // Added by Wael - Start OCOM-1202 & OCOM-1207
        String smbCareAddrIdStr=System.currentPageReference().getParameters().get('smbCareAddrId');
        smbCareAddrId=null;
        if(String.isNotBlank(smbCareAddrIdStr) && smbCareAddrIdStr instanceof Id){
            smbCareAddrId=smbCareAddrIdStr;
        }
        //smbCareAddrId = System.currentPageReference().getParameters().get('smbCareAddrId');
       
        String moveOutAddrIdStr = System.currentPageReference().getParameters().get('moveOutAddrId');
        moveOutAddrId=null;
        if(String.isNotBlank(moveOutAddrIdStr) && moveOutAddrIdStr instanceof Id){
            moveOutAddrId=moveOutAddrIdStr;
        }
        //moveOutAddrId = System.currentPageReference().getParameters().get('moveOutAddrId');
        String contactIdStr = System.currentPageReference().getParameters().get('contactId');
        contactId=null;
        if(String.isNotBlank(contactIdStr) && contactIdStr instanceof Id){
            contactId=contactIdStr;
        }
        //contactId = System.currentPageReference().getParameters().get('contactId');
        String orderIdStr = System.currentPageReference().getParameters().get('orderId');
        orderId=null;
        if(String.isNotBlank(orderIdStr) && orderIdStr instanceof Id){
            orderId=orderIdStr;
        }
        //orderId = System.currentPageReference().getParameters().get('orderId');
        String oppIdStr=System.currentPageReference().getParameters().get('oppId');
        oppId=null;
        if(String.isNotBlank(oppIdStr) && oppIdStr instanceof Id){
            oppId=oppIdStr;
        }
        //oppId=System.currentPageReference().getParameters().get('oppId');
        String primaryContactIdStr=System.currentPageReference().getParameters().get('primaryContactId');
        primaryContactId=null;
        if(String.isNotBlank(primaryContactIdStr) && primaryContactIdStr instanceof Id){
            primaryContactId=primaryContactIdStr;
        }
       // primaryContactId=System.currentPageReference().getParameters().get('primaryContactId');
        
        if(orderId != null && orderId != ''){
            oid = id.valueOf(orderId);
            displayPageComp =true;
        }else {
            displayPageComp =false;
            addrNotValid = false;
            tvAvailability = '';
            forborne = '';
            maximumSpeed = '';
            maxSpeedCopper = false;
        }
        src= System.currentPageReference().getParameters().get('src');
        
        if(src=='os' || (src!='os' && orderId !=null && orderId!='')){
            List<Order> order_list =  [SELECT Id, CustomerAuthorizedBy.Id, Service_Address__c, Service_Address__r.Id, AccountId FROM Order WHERE Id=: orderId ];
            Order theOrder;
            if(order_list.size()>0){
                theOrder = order_list.get(0);
                this.contactId = theOrder.CustomerAuthorizedBy.Id; 
                if( theOrder.Service_Address__c!=null){
                    if (src=='os') this.smbCareAddrId = theOrder.Service_Address__r.Id;
                    SMBCare_Address__c theAddress = [SELECT Id,FMS_Address_ID__c,Send_to_SACG__c,isTVAvailable__c,Forbone__c,Maximum_Speed__c,Status__c FROM SMBCare_Address__c WHERE Id=:this.smbCareAddrId LIMIT 1 ];
                    if(theAddress !=null){
                        //this.smbCareAddrId = theAddress.Id;
                        this.orderSmbCareAddrId = theAddress.Id;
                        addrNotValid = (theAddress.Status__c != null && theAddress.Status__c != 'Valid')? true:false;
                        tvAvailability = theAddress.isTVAvailable__c == true? Label.OCOM_TV_Available : Label.OCOM_TV_Not_Available ; 
                        forborne = theAddress.Forbone__c == true?  Label.OCOM_Forborne : Label.OCOM_Non_Forborne;
                        maximumSpeed = theAddress.Maximum_Speed__c != null ? theAddress.Maximum_Speed__c : '';
                        maxSpeedCopper = maximumSpeed == '' ? true:false;
                    } else {
                        this.smbCareAddrId = '';
                        this.orderSmbCareAddrId = '';
                        addrNotValid = false;
                        tvAvailability = '';
                        forborne = '';
                        maximumSpeed = '';
                        maxSpeedCopper = false;
                    }
                }
                acctId = theOrder.AccountId;
            }
        }
        
        // Wael - End OCOM-1202 & OCOM-1207
        
        if(acctId==null)
            acctId = System.currentPageReference().getParameters().get('aid');
        if(acctId!=null){
            rcidAccount = [select Id,parentId,Name from Account where id =  :acctId];
        }
        
        SAData  = new ServiceAddressData();
        SAData.isSACG = true;
        System.debug('SAData##'+SAData);
        moveOutSADATA = new ServiceAddressData();
        moveOutSADATA.isSACG = false;
        System.debug('moveOutSADATA##'+moveOutSADATA); 
        
        orderPath = 'Simple';
        orderType = 'New Services';
        caseIds = '';
        errorMessages = new List<String>();
        
    }
    
    public PageReference RedirectToContactPage(){
        
        processManualAddress();
        
        if(SAData.futureASfCall !=  True && smbCareAddr != null)
        {
            FutureCallServiceAddDetailsFMS(SAData.fmsId, SAData.province, smbCareAddr.Id);
        }
        
        if(blnGTACall == False && smbCareAddr != null)
        {
            FutureCallGTA(smbCareAddr.Id);
        }
        
        Id accountId = rcidAccount.id;
        if (orderPath == 'Complex') {
            return new PageReference('/apex/smb_newQuoteFromAccount?aid='+accountId+'&OrderPath='+orderPath+'&isdtp=vw');
        }else{
            // Wael added smbCareAddr, contactId and OrderId for OCOM-1202/OCOM-1207
            if(contactId==null){
                contactId=''; 
            }
            if(orderId==null || orderId=='null'){
                orderId='';
            }
            
            if(orderId==''){
                System.debug('OCOM_InOrderFlowPACController::RedirectToContactPage - No order created yet!');
            }
            
            // Wael added the below check for OCOM-1222 - The need to know if the address changed or not.
            if(orderId!='' && this.orderSmbCareAddrId != null && this.smbCareAddr!=null && this.orderSmbCareAddrId == this.smbCareAddr.Id){
                System.debug('OCOM_InOrderFlowPACController::RedirectToContactPage - Selected Address is the same as the address used in the order!');
                // TODO: Do Nothing actually, just make sure order's information continue to be the same as the address didn't get changed.
            } else if(orderId!='' && this.orderSmbCareAddrId != this.smbCareAddr.Id){
                try{
                    system.debug('!!!Change Address '+orderId);
                    OCOM_WFM_DueDate_Mgmt_Helper.cancelWFMBookingByOrderId(orderId);
                }catch(exception ex){
                    String exceptionMsg=' Exception Occured, while cancelling WFM Slots for OrderId: '+orderId+'\n\n'+ex.getstacktracestring()+'\n';
                    OCOM_WFM_DueDate_Mgmt_Helper.insertWebserviceLog('ClassName:OCOM_InOrderFlowPACController and MethodName:RedirectToContactPage ',exceptionMsg,ex,'Failure');
                }
                System.debug('OCOM_InOrderFlowPACController::RedirectToContactPage - Selected Address is different from the address used in the order or the order! - this.orderSmbCareAddrId = ' + this.orderSmbCareAddrId + ' - smbCareAddr.Id = ' + smbCareAddr.Id);
                // TODO: Call methods that clear from the order the information that is invalid for the new address, e.g. appointments.
            }
            // Wael - End of changes for OCOM-1222
            
            // Wael added smbCareAddr, contactId and OrderId for OCOM-1202/OCOM-1207 
            system.debug('***********!rcidAccount:' + rcidAccount);
            system.debug('***********!orderId:' + orderId);
            system.debug('***********!typeOfOrder:' + orderType);
            system.debug('***********!serviceAccount:' + serviceAccount);
            system.debug('***********!moveOutServiceAccount:' + this.moveOutServiceAccount);
            system.debug('***********!smbCareAddr:' + smbCareAddr);
            system.debug('***********!moveOutAddr:' + this.moveOutAddr);
            system.debug('***********!contactId:' + contactId);
            system.debug('***********!caseIds:' + caseIds);
            
            String moveOutAccountId = (this.moveOutServiceAccount == null) ? '' : this.moveOutServiceAccount.Id;
            String moveOutAddressId = (this.moveOutAddr == null) ? this.moveOutAddrId : this.moveOutAddr.Id;
            if(oppId==null || oppId=='null'){
                oppId='';
            }
            if(primaryContactId==null || primaryContactId=='null'){
                primaryContactId='';
            }
            return  new PageReference('/apex/OCOM_InOrderFlowContact?OrderId=' + orderId 
                                      + '&id=' + serviceAccount.Id
                                      + '&moveOutAccountId=' + moveOutAccountId
                                      + '&id=' + serviceAccount.Id
                                      + '&moveOutAccountId=' + moveOutAccountId
                                      + '&OrderType=' + orderType
                                      + '&ParentAccId=' + rcidAccount.id 
                                      + '&smbCareAddrId=' + this.smbCareAddr.Id 
                                      + '&moveOutAddrId=' + moveOutAddressId 
                                      + '&contactId=' + contactId
                                      + '&caseIds=' + caseIds
                                     + '&oppId=' + oppId
                                     +'&primaryContactId=' +primaryContactId);   
   
        }        
    }
    
    //overloaded function to support move out address
    public void CreateServiceAccount(){
        String addressType = System.currentPageReference().getParameters().get('addressType');
        CreateServiceAccount(addressType);
    }
    
    public void CreateServiceAccount(String addressType){
        
        try {
            // Defect # 11914 - clear error messages after selecting address in pac component
            errorMessages = new List<String>();
            //end
            system.debug('**********addressType='+addressType);
            ServiceAddressData svcData;
            Account svcAccount;
            system.debug('*********SAData =' + SAData);
            if (addressType == 'MOVE_OUT') {
                svcData = moveOutSAData;
            } else {
                svcData = SAData;
            }
            
            isASFDown = svcData.isASFDown; //31 March 2017 - Defect # 11733 - Added by Sandip
            system.debug('*********svcData =' + svcData);
            
            // here we will need to create service account
            Id accountId = rcidAccount.id;//'0015600000450H6';
            // Added by Arvind to dispaly additional Service detail fields..
            dropFacilityCode = svcData.dropFacilityCode;
            dropTypeCode = svcData.dropTypeCode;
            dropLength = svcData.dropLength;
            dropExceptionCode = svcData.dropExceptionCode;
            currentTransportTypeCode = svcData.currentTransportTypeCode;
            futureTransportTypeCode = svcData.futureTransportTypeCode;
            futurePlantReadyDate = svcData.futurePlantReadyDate;
            futureTransportRemarkTypeCode = svcData.futureTransportRemarkTypeCode;
            gponBuildTypeCode = svcData.gponBuildTypeCode;
            provisioningSystemCode = svcData.provisioningSystemCode;
            ratingNpaNxx = svcData.ratingNpaNxx;
            COID = svcData.COID;
            //Addition End
            
            
            system.debug('------svcData---' + svcData);
            // Added by Aditya jamwal (IBM) -start
            
            SMBCare_Address__c addressRecord = new SMBCare_Address__c();
            addressRecord.Address_Type__c = 'Standard'; // Service Address Type defaults to Standard.
            addressRecord.recordtypeid = Schema.SObjectType.SMBCare_Address__c.getRecordTypeInfosByName().get('Address').getRecordTypeId();
            //---End
            OCOM_MACD_ServiceAccountAndAddressExt ext= new OCOM_MACD_ServiceAccountAndAddressExt(this);
            
            if (rcidAccount != null ){
                if (svcData.isManualCapture) {
                    // Sandip - 15 March 2017 - Defect 11440
                    if (Country_Province_Codes__c.getValues(SAData.province) != null && Country_Province_Codes__c.getValues(svcData.province).is_ILEC__c) {
                        svcData.isILEC = true;
                    } else {
                        svcData.isILEC = false;
                        //svcData.locationId = OrdrUtilities.generateUniqueHashCode();
                        svcData.locationId = OrdrUtilities.generateUniqueHashCode(svcData.address,svcData.province,svcData.city,svcData.postalCode,svcData.country);
                    }
                    
                    svcAccount= ext.CreateServiceAccount(rcidAccount.Id, addressRecord, svcData);
                    
                } else {
                    system.debug('aa' +rcidAccount.Id + '--' +svcData  );
                    svcData.isILEC = true;
                    
                    svcAccount = ext.CreateServiceAccount(rcidAccount.Id, addressRecord, svcData);

                    addressRecord = [Select Service_Account_Id__c,id, Account__c, FMS_Address_ID__c, Status__c,Postal_Code__c , 
                                     Street_Number__c, city__c , Street__c, Street_Name__c, Street_Direction_Code__c , Suite_Number__c , 
                                     Province__c, COID__c , Building_Number__c , Country__c,  State__c, Civic_Number__c, Street_Type_Code__c , 
                                     Location_Id__c , Address__c , Address_Type__c ,  LocalRoutingNumber__c , NPA_NXX__c , SwitchNumber__c , 
                                     TerminalNumber__c, Service_Account_Id__r.RecordTypeId ,AddressText__c
                                     from SMBCare_address__c 
                                     where Service_Account_Id__c = :svcAccount.Id];
                    
                    system.debug('***************addressRecord:' + addressRecord);
                    system.debug('After Inside with Manual Capture:' + svcAccount);
                    
                    Id contactId = accountId;
                    if(ContId !=null){
                        contactId = ContId;
                    }
                }
                if (addressType == 'MOVE_OUT') {
                    moveOutAddr = addressRecord;
                    moveOutServiceAccount = svcAccount;
                } else {
                    smbCareAddr = addressRecord;
                    serviceAccount = svcAccount;
                }  
                system.debug('>>>>>>>>>moveOutAddr=' + moveOutAddr);
                system.debug('>>>>>>>>>moveOutServiceAccount=' + moveOutServiceAccount);
                system.debug('>>>>>>>>>smbCareAddr=' + smbCareAddr);
                system.debug('>>>>>>>>>serviceAccount=' + serviceAccount);
            }
        } catch (Exception e) {
            system.debug(Logginglevel.ERROR, e.getMessage());
            errorMessages.add(Label.ACCT0001);
        }
        
    }

    public List<SelectOption> getOrderPathValues() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Simple','Simple'));
        options.add(new SelectOption('Complex','Complex'));
        return options;
    }
    
    public List<SelectOption> getExistingServicesValues() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Yes','Yes'));
        options.add(new SelectOption('No','No'));
        return options;
    }   
    
    public List<SelectOption> getOrderTypeValues() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Change','Change/Add/Disconnect'));
        options.add(new SelectOption('New Services','New Services'));
        options.add(new SelectOption('Move','Move'));
        return options;
    } 
    

    /*
     * Wael - Start (9-Aug-2016)
     * JIRA Ref: OCOM-1202
     */
    public PageReference init(){
        
        initializeMoveAddress();

        if(smbCareAddrId!=null){
            List <SMBCare_address__c> the_smbCareAddr_list = [Select Id, Name, Service_Account_Id__c, Account__c, 
                                                              FMS_Address_ID__c, Status__c, Postal_Code__c , 
                                                              Street_Number__c, city__c , Street__c, Street_Name__c, 
                                                              Street_Direction_Code__c , Suite_Number__c , 
                                                              Province__c, Building_Number__c , Country__c,  State__c, 
                                                              Block__c, Civic_Number__c, Street_Type_Code__c , 
                                                              Location_Id__c , Address__c, Street_Address__c 
                                                              FROM SMBCare_address__c 
                                                              WHERE Id =: smbCareAddrId];
            if(the_smbCareAddr_list.size()==1){
                SMBCare_address__c the_smbCareAddr = the_smbCareAddr_list.get(0);
                String full_address = '';
                String block = '';
                
                if(the_smbCareAddr.Block__c!=null && the_smbCareAddr.Block__c!=''){
                    block = the_smbCareAddr.Block__c + ' ';
                }
                
                String building_number = '';
                if(the_smbCareAddr.Building_Number__c!=null && the_smbCareAddr.Building_Number__c!=''){
                    building_number = the_smbCareAddr.Building_Number__c + ' ';
                }
                String street_number = '';
                if(the_smbCareAddr.Street_Number__c!=null && the_smbCareAddr.Street_Number__c!=''){
                    street_number = the_smbCareAddr.Street_Number__c+ ' ';
                } else if ( the_smbCareAddr.Building_Number__c!=null && the_smbCareAddr.Building_Number__c!='') {
                    street_number = the_smbCareAddr.Building_Number__c+ ' ';
                }
                
                String street_name = '';
                if(the_smbCareAddr.Street_Name__c !=null && the_smbCareAddr.Street_Name__c !=''){
                    street_name = the_smbCareAddr.Street_Name__c + ' ';
                }
                if(street_name == '' && the_smbCareAddr.Street_Address__c !=null && the_smbCareAddr.Street_Address__c !=''){
                    street_name = the_smbCareAddr.Street_Address__c + ' ';
                }
                
                /*
                full_address = the_smbCareAddr.Street_Number__c + ' ' + the_smbCareAddr.Street_Name__c + ' ';
                full_address += the_smbCareAddr.City__c + ' ' + the_smbCareAddr.Province__c + ' ';
                full_address += the_smbCareAddr.Postal_Code__c ; //+ ' ' + the_smbCareAddr.Country__c;
                */
                
                String postal_code = '';
                if(the_smbCareAddr.Postal_Code__c !=null && the_smbCareAddr.Postal_Code__c !=''){
                    postal_code = the_smbCareAddr.Postal_Code__c + ' ';
                }
                
                if(the_smbCareAddr.Status__c=='Valid'){
                    full_address = street_number + street_name;
                    full_address += the_smbCareAddr.City__c + ' ' + the_smbCareAddr.Province__c + ' ';
                    full_address += postal_code  ; //+ ' ' + the_smbCareAddr.Country__c;
                } else {
                    full_address = the_smbCareAddr.Address__c + ' ';
                    full_address += the_smbCareAddr.City__c + ' ' + the_smbCareAddr.Province__c + ' ';
                    full_address += postal_code  ; //+ ' ' + the_smbCareAddr.Country__c;
                }
                
                
                saData.onLoadSearchAddress = full_address;
                saData.searchString = full_address;
            }
        }  
        return null;
    }
    
    // Wael End - OCOM-1202 
    
   
    private void initializeMoveAddress() {
        List<SmbCare_Address__c> addresses = new List<SmbCare_Address__c>();
        if (String.isNotBlank(this.orderId)) {
            Order order = [select type, service_address__c, moveOutServiceAddress__c from order where id = :this.orderId];
            if (order != null && String.isNotBlank(order.moveOutServiceAddress__c)) {
                addresses = [select service_account_Id__r.name, fms_address_id__c from SmbCare_Address__c where id = :order.moveOutServiceAddress__c];
            }
        } else if (String.isNotBlank(this.moveOutAddrId)) {
            addresses = [select service_account_Id__r.name, fms_address_id__c from SmbCare_Address__c where id = :this.moveOutAddrId];
        }
        if (addresses.size() > 0) {
            //this.orderType = order.type;
            String searchStr = addresses[0].service_account_Id__r.name;
            searchStr = searchStr.removeEnd(addresses[0].fms_address_id__c);
            this.moveOutSAData.searchString = searchStr;
            //this.moveOutSAData.onLoadSearchAddress = searchStr;
        }
        
    }
   
    
    private void processManualAddress(){
        system.debug('*****processManualAddress');
        system.debug('SAData.isManualCapture=' + SAData.isManualCapture);
        system.debug('SAData.isSACG=' +  SAData.isSACG);
        system.debug('moveOutSAData.isManualCapture=' + SAData.isManualCapture);
        system.debug('moveOutSAData.isSACG=' +  SAData.isSACG);
        
        if (SAData.isManualCapture) {
            // Sandip - 15 March 2017 - Defect 11440
            if(Country_Province_Codes__c.getValues(SAData.province) != null){
                SAData.isILEC = Country_Province_Codes__c.getValues(SAData.province).is_ILEC__c;
                if (SAData.isILEC) SAData.isSACG = true;
            } // End Defect 11440
            CreateServiceAccount('CREATE');
            //future call
            OrdrProductEligibilityManager.cacheAvailableOffers(smbCareAddr.Id);
        }
        if (moveOutSAData.isManualCapture) {
            CreateServiceAccount('MOVE_OUT');
        }
        
        List<ServiceAddressData> svcAddrData = new List<ServiceAddressData>();
        if (SAData.isSACG) {
            svcAddrData.add(SAData);
        }
        if (moveOutSAData.isSACG) {
            svcAddrData.add(moveOutSAData);
        }
        if (svcAddrData.size() > 0) {
            system.debug('*****sendToSACG');
            PACUtility utilObj = new PACUtility();
            utilObj.sendToSACG(svcAddrData);
        }
        
        for (ServiceAddressData sData : svcAddrData) {
            if (String.isNotBlank(sData.caseId)) {
                caseIds += ':' + sData.caseId;
            }
        }
        caseIds.removeStart(':');
        
    }  
    
    @future(callout=true)
    public Static void FutureCallGTA(String smbCareAddrId)
    {
        OrdrTechnicalAvailabilityManager.checkProductTechnicalAvailability(smbCareAddrId);      
    }
    
   
    @future(callout=true)
    public Static void FutureCallServiceAddDetailsFMS(String addressId, String regionId, String smbCareAddrId)
    {
        
        SMBCare_WebServices__c cs = SMBCare_WebServices__c.getInstance('username');
        String userId = cs.value__c;
        
        SRS_ServiceAddressManagementService_1.ServiceAddressManagementService_v1_1_SOAP asfObj = new SRS_ServiceAddressManagementService_1.ServiceAddressManagementService_v1_1_SOAP();  
        SRS_ServiceAddressMgmtRequestRes.ServiceAddress asfaddress  = new SRS_ServiceAddressMgmtRequestRes.ServiceAddress();
        SRS_ServiceAddressMgmtRequestRes.LikeMunicipalityItem LikeMunciplity = new SRS_ServiceAddressMgmtRequestRes.LikeMunicipalityItem ();
        SRS_ServiceAddressMgmtRequestRes.getServiceAddressDetails_element respDetails = new SRS_ServiceAddressMgmtRequestRes.getServiceAddressDetails_element();
        
        SRS_ServiceAddressCommonAddressTypes_v2.Address asfCommanaddress  = new SRS_ServiceAddressCommonAddressTypes_v2.Address();
        asfCommanaddress.addressId = addressId;
        asfCommanaddress.provinceStateCode = regionId;                
        
        SRS_ServiceAddressMgmtRequestRes.getServiceAddressDetails_element requestElem=new SRS_ServiceAddressMgmtRequestRes.getServiceAddressDetails_element();
        requestElem.address=asfCommanaddress;
        SRS_ServiceAddressMgmtRequestRes.ServiceAddress saRequestElem=new SRS_ServiceAddressMgmtRequestRes.ServiceAddress();
        
        respDetails = asfObj.getServiceAddressDetails(userId ,system.now(),requestElem);
        
        system.debug('respDetails ===> ' + respDetails);
        
        SMBCare_Address__c smbCareAddr = [select Service_Account_Id__c,id, Account__c, FMS_Address_ID__c, Status__c,Postal_Code__c , Street_Number__c, city__c , Street__c, Street_Name__c, Street_Direction_Code__c , Suite_Number__c , Province__c, COID__c , Building_Number__c , Country__c,  State__c, Civic_Number__c, Street_Type_Code__c , Location_Id__c , Address__c , Address_Type__c ,  LocalRoutingNumber__c , NPA_NXX__c , SwitchNumber__c , TerminalNumber__c, Service_Account_Id__r.RecordTypeId  from SMBCare_Address__c where Id =:smbCareAddrId]; 
        
        
        if(String.isNotBlank(respDetails.address.dropTypeCode)){ 
            smbCareAddr.Drop_TypeCode__c =respDetails.address.dropTypeCode;                     
        }
        
        if(String.isNotBlank(respDetails.address.dropLength)){ 
            smbCareAddr.Drop_Length__c =respDetails.address.dropLength;                     
        }
        
        if(String.isNotBlank(respDetails.address.dropExceptionCode)){ 
            smbCareAddr.Drop_ExceptionCode__c =respDetails.address.dropExceptionCode;                     
        }
        
        if(String.isNotBlank(respDetails.address.currentTransportTypeCode)){ 
            smbCareAddr.Current_TransportTypeCode__c =respDetails.address.currentTransportTypeCode;                     
        }
        
        if(String.isNotBlank(respDetails.address.futureTransportTypeCode)){ 
            smbCareAddr.Future_TransportTypeCode__c =respDetails.address.futureTransportTypeCode;                     
        }
        
        if(String.isNotBlank(respDetails.address.futurePlantReadyDate)){ 
            smbCareAddr.Future_PlantReadyDate__c =respDetails.address.futurePlantReadyDate;                     
        }
        
        if(String.isNotBlank(respDetails.address.futureTransportRemarkTypeCode )){ 
            smbCareAddr.Future_TransportRemarkTypeCode__c =respDetails.address.futureTransportRemarkTypeCode;                     
        }
        
        if(String.isNotBlank(respDetails.address.gponBuildTypeCode)){ 
            smbCareAddr.GPON_BuildTypeCode__c =respDetails.address.gponBuildTypeCode;                     
        }
        
        if(String.isNotBlank(respDetails.address.provisioningSystemCode)){ 
            smbCareAddr.Provisioning_SystemCode__c =respDetails.address.provisioningSystemCode;                     
        }
        
        if(string.isNotBlank(respDetails.address.streetName)){
            smbCareAddr.Street_Name__c=respDetails.address.streetName;
        }
        smbCareAddr.COID__c = respDetails.address.coid;
        smbCareAddr.Civic_Number__c= respDetails.address.civicNumber;
        
        if(String.isBlank(respDetails.address.streetTypeCode)){
            smbCareAddr.Street_Type_Code__c = 'Other';
        } else {
            smbCareAddr.Street_Type_Code__c = respDetails.address.streetTypeCode;
        }
        smbCareAddr.Address_Type__c = 'Standard'; // Service Address Type defaults to Standard.
        smbCareAddr.LocalRoutingNumber__c = respDetails.address.localRoutingNumber;
        smbCareAddr.NPA_NXX__c = respDetails.address.ratingNpaNxx;
        smbCareAddr.SwitchNumber__c = respDetails.address.switchNumber;
        smbCareAddr.TerminalNumber__c = respDetails.address.terminalNumber;
        smbCareAddr.coid__c = respDetails.address.coid;
        smbCareAddr.CLLI__c = respDetails.address.clliCode;
        smbCareAddr.id=smbCareAddrId;
        System.debug('smbCareAddrUpd###'+smbCareAddr);
        update smbCareAddr;
        
    }
    
    // Defect #12343 - 10 Apr 2017 - Sandip - Added below code to check ILEC and Non ILEC Province
    @RemoteAction
    global static boolean isILEC(String province){
        Boolean isILEC = false;
        if (Country_Province_Codes__c.getValues(province) != null && Country_Province_Codes__c.getValues(province).is_ILEC__c) {
            isILEC = true;
        } else {
            isILEC = false;
        }
        return isILEC;
    }
}