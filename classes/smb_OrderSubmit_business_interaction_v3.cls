//Generated by wsdl2apex

public class smb_OrderSubmit_business_interaction_v3 {
    public class BusinessEvent {
        public smb_OrderSubmit_Business_Int_Ext_v3.BusinessEventExtensions BusinessEventExtensions;
        public DateTime NotificationTime;
        public DateTime EffectiveTime;
        private String[] BusinessEventExtensions_type_info = new String[]{'BusinessEventExtensions','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','1','false'};
        private String[] NotificationTime_type_info = new String[]{'NotificationTime','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','1','false'};
        private String[] EffectiveTime_type_info = new String[]{'EffectiveTime','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.ibm.com/telecom/common/schema/business_interaction/v3_0','false','false'};
        private String[] field_order_type_info = new String[]{'BusinessEventExtensions','NotificationTime','EffectiveTime'};
    }
    public class BusinessInteractionEntity {
        public String Status;
        public smb_OrderSubmit_base_v3.EntityWithSpecification SiteId;
        public smb_OrderSubmit_base_v3.EntityWithSpecification[] ContactPersonId;
        public smb_OrderSubmit_Business_Int_Ext_v3.BusinessInteractionEntityExtensions BusinessInteractionEntityExtensions;
        public smb_OrderSubmit_Business_Int_Ext_v3.BusinessInteractionEntityChoice BusinessInteractionEntityChoice;
        private String[] Status_type_info = new String[]{'Status','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','1','false'};
        private String[] SiteId_type_info = new String[]{'SiteId','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','1','false'};
        private String[] ContactPersonId_type_info = new String[]{'ContactPersonId','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','-1','false'};
        private String[] BusinessInteractionEntityExtensions_type_info = new String[]{'BusinessInteractionEntityExtensions','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','1','false'};
        private String[] BusinessInteractionEntityChoice_type_info = new String[]{'BusinessInteractionEntityChoice','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.ibm.com/telecom/common/schema/business_interaction/v3_0','false','false'};
        private String[] field_order_type_info = new String[]{'Status','SiteId','ContactPersonId','BusinessInteractionEntityExtensions','BusinessInteractionEntityChoice'};
    }
    public class BusinessTimePeriod {
        public DateTime EndTime;
        private String[] EndTime_type_info = new String[]{'EndTime','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.ibm.com/telecom/common/schema/business_interaction/v3_0','false','false'};
        private String[] field_order_type_info = new String[]{'EndTime'};
    }
    public class Assignment {
        public smb_OrderSubmit_base_v3.EntityWithSpecification AssigneeId;
        public String Disposition;
        public String Description;
        private String[] AssigneeId_type_info = new String[]{'AssigneeId','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','1','false'};
        private String[] Disposition_type_info = new String[]{'Disposition','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','1','false'};
        private String[] Description_type_info = new String[]{'Description','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.ibm.com/telecom/common/schema/business_interaction/v3_0','false','false'};
        private String[] field_order_type_info = new String[]{'AssigneeId','Disposition','Description'};
    }
    public class StatusNotification {
        public smb_OrderSubmit_business_interaction_v3.StatusNotificationItem[] StatusNotificationItem;
        public smb_OrderSubmit_Business_Int_Ext_v3.StatusNotificationExtensions StatusNotificationExtensions;
        private String[] StatusNotificationItem_type_info = new String[]{'StatusNotificationItem','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','-1','false'};
        private String[] StatusNotificationExtensions_type_info = new String[]{'StatusNotificationExtensions','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.ibm.com/telecom/common/schema/business_interaction/v3_0','false','false'};
        private String[] field_order_type_info = new String[]{'StatusNotificationItem','StatusNotificationExtensions'};
    }
    public class Request {
        public DateTime ExpectedDeliveryDate;
        public DateTime RequestedDate;
        public smb_OrderSubmit_Party_v3.PartyRole[] PartyRole;
        public smb_OrderSubmit_Business_Int_Ext_v3.RequestExtensions RequestExtensions;
        private String[] ExpectedDeliveryDate_type_info = new String[]{'ExpectedDeliveryDate','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','1','false'};
        private String[] RequestedDate_type_info = new String[]{'RequestedDate','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','1','false'};
        private String[] PartyRole_type_info = new String[]{'PartyRole','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','-1','false'};
        private String[] RequestExtensions_type_info = new String[]{'RequestExtensions','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.ibm.com/telecom/common/schema/business_interaction/v3_0','false','false'};
        private String[] field_order_type_info = new String[]{'ExpectedDeliveryDate','RequestedDate','PartyRole','RequestExtensions'};
    }
    public class StatusNotificationItem {
        public String Status;
        public smb_OrderSubmit_business_interaction_v3.BusinessInteractionEntity BusinessInteractionEntity;
        public smb_OrderSubmit_business_interaction_v3.StatusNotificationItem[] ComponentStatusNotificationItem;
        public smb_OrderSubmit_Business_Int_Ext_v3.StatusNotificationItemExtensions StatusNotificationItemExtensions;
        private String[] Status_type_info = new String[]{'Status','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','1','false'};
        private String[] BusinessInteractionEntity_type_info = new String[]{'BusinessInteractionEntity','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','1','false'};
        private String[] ComponentStatusNotificationItem_type_info = new String[]{'ComponentStatusNotificationItem','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','-1','false'};
        private String[] StatusNotificationItemExtensions_type_info = new String[]{'StatusNotificationItemExtensions','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.ibm.com/telecom/common/schema/business_interaction/v3_0','false','false'};
        private String[] field_order_type_info = new String[]{'Status','BusinessInteractionEntity','ComponentStatusNotificationItem','StatusNotificationItemExtensions'};
    }
    public class BusinessInteraction {
        public DateTime InteractionDate;
        public String Description;
        public DateTime InteractionDateComplete;
        public String InteractionStatus;
        public String InteractionPriority;
        public String ErrorCode;
        public String ErrorDescription;
        public smb_OrderSubmit_Business_Int_Ext_v3.BusinessInteractionExtensions BusinessInteractionExtensions;
        private String[] InteractionDate_type_info = new String[]{'InteractionDate','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','1','false'};
        private String[] Description_type_info = new String[]{'Description','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','1','false'};
        private String[] InteractionDateComplete_type_info = new String[]{'InteractionDateComplete','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','1','false'};
        private String[] InteractionStatus_type_info = new String[]{'InteractionStatus','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','1','false'};
        private String[] InteractionPriority_type_info = new String[]{'InteractionPriority','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','1','false'};
        private String[] ErrorCode_type_info = new String[]{'ErrorCode','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','1','false'};
        private String[] ErrorDescription_type_info = new String[]{'ErrorDescription','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','1','false'};
        private String[] BusinessInteractionExtensions_type_info = new String[]{'BusinessInteractionExtensions','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.ibm.com/telecom/common/schema/business_interaction/v3_0','false','false'};
        private String[] field_order_type_info = new String[]{'InteractionDate','Description','InteractionDateComplete','InteractionStatus','InteractionPriority','ErrorCode','ErrorDescription','BusinessInteractionExtensions'};
    }
    public class BusinessInteractionItem {
        public String Action;
        public DateTime ExpectedDeliveryDate;
        public DateTime RequestedDate;
        public smb_OrderSubmit_base_v3.EntityWithSpecification[] AppointmentId;
        public smb_OrderSubmit_base_v3.EntityWithSpecification[] ReservationId;
        public String ErrorCode;
        public String ErrorDescription;
        public smb_OrderSubmit_Urban_Prop_Address_v3.UrbanPropertyAddress Address;
        public smb_OrderSubmit_Business_Int_Ext_v3.BusinessInteractionItemExtensions BusinessInteractionItemExtensions;
        private String[] Action_type_info = new String[]{'Action','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','1','false'};
        private String[] ExpectedDeliveryDate_type_info = new String[]{'ExpectedDeliveryDate','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','1','false'};
        private String[] RequestedDate_type_info = new String[]{'RequestedDate','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','1','false'};
        private String[] AppointmentId_type_info = new String[]{'AppointmentId','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','-1','false'};
        private String[] ReservationId_type_info = new String[]{'ReservationId','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','-1','false'};
        private String[] ErrorCode_type_info = new String[]{'ErrorCode','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','1','false'};
        private String[] ErrorDescription_type_info = new String[]{'ErrorDescription','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','1','false'};
        private String[] Address_type_info = new String[]{'Address','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','1','false'};
        private String[] BusinessInteractionItemExtensions_type_info = new String[]{'BusinessInteractionItemExtensions','http://www.ibm.com/telecom/common/schema/business_interaction/v3_0',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.ibm.com/telecom/common/schema/business_interaction/v3_0','false','false'};
        private String[] field_order_type_info = new String[]{'Action','ExpectedDeliveryDate','RequestedDate','AppointmentId','ReservationId','ErrorCode','ErrorDescription','Address','BusinessInteractionItemExtensions'};
    }
}