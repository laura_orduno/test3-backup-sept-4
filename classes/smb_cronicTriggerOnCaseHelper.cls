public class smb_cronicTriggerOnCaseHelper {
    /**
     * Implementated to replace smb_CronicTriggerOnCase.Trigger - Called from CaseTrigger
     *
     * Dane Peterson, Traction on demand
     * @date 2018-01-15
     * @param newCases - cases in the trigger context
     * @param oldMap - previous value of all cases in trigger context (if had previous values)
     */
    public static void executeUpdateAccounts(List<Case> newCases, Map<Id, Case> oldMap) {
        /* original implentation, susceptable to SOQL limits
        //    smb_cronicTriggerOnCaseHelper trgHelper = new smb_cronicTriggerOnCaseHelper();
        //    trgHelper.getCaseRootCauseCount(trigger.new,trigger.oldMap);
        */
        // utilizing static calls
        Set<Id> acctIds = smb_cronicTriggerOnCaseHelper.getAccountIds(newCases, oldMap);

        if (Test.isRunningTest()) {
            if (!System.isFuture() && !System.isBatch()) {
                smb_cronicTriggerOnCaseHelper.updateAccountsFuture(acctIds);
            }
        } else {
            smb_cronicTriggerOnCaseHelper.updateAccounts(acctIds);
        }
    }

    public static set<string> getConfigPickListSet() {
        set<string> setConfigPickLists = new set<string>();

        List<Case_RootCause_Cronic__C> mcs = Case_RootCause_Cronic__c.getall().values();
        for (Case_RootCause_Cronic__C cs : mcs) {
            if (string.isNotBlank(cs.PickList_Strings__c))
                setConfigPickLists.add(cs.PickList_Strings__c);
        }
        system.debug('-----setConfigPickLists---' + setConfigPickLists);

        return setConfigPickLists;
    }

    public static set<id> getAccountIds(list<case> lstCaseFromTrigger, map<id, case> mapCaseOldFromTrigger) {
        set<id> setAccIDs = new set<id>();
        set<string> setConfigPickLists = getConfigPickListSet();

        //Loop through each case and collect Account ID for further processing
        for (case caseFromTrigger : lstCaseFromTrigger) {
            //check if custom setting is filled
            if (!setConfigPickLists.isEmpty()) {
                system.debug('---caseFromTrigger---');
                if (trigger.isInsert) {
                    //root cause should not be blank and only check identified root causes
                    if (String.isnotBlank(caseFromTrigger.Root_Cause__c)
                            && setConfigPickLists.contains(caseFromTrigger.Root_Cause__c)) {
                        setAccIDs.add(caseFromTrigger.AccountID);
                    }
                }
                //In case of update we need to collect acc ID for increase or decrese in cronic count
                else if (trigger.isUpdate && caseFromTrigger.Root_Cause__c != mapCaseOldFromTrigger.get(caseFromTrigger.id).Root_Cause__c) {
                    if (String.isnotBlank(caseFromTrigger.Root_Cause__c))//root cause is not blank
                    {
                        //either new modified value is identified root cause or old value was.
                        if (setConfigPickLists.contains(caseFromTrigger.Root_Cause__c)
                                || setConfigPickLists.contains(mapCaseOldFromTrigger.get(caseFromTrigger.id).Root_Cause__c))
                            setAccIDs.add(caseFromTrigger.AccountID);
                    }
                    //in case of blank root cause, check for old value.
                    //if it was in identified root cause list then now it will be used to decrease the cronic count
                    else if (String.isnotBlank(mapCaseOldFromTrigger.get(caseFromTrigger.id).Root_Cause__c)
                            && setConfigPickLists.contains(mapCaseOldFromTrigger.get(caseFromTrigger.id).Root_Cause__c)) {
                        setAccIDs.add(caseFromTrigger.AccountID);
                    }

                }
            }
        }

        return setAccIDs;
    }

    @future
    public static void updateAccountsFuture(set<id> setAccIDs) {
        updateAccounts(setAccIDs);
    }

    public static void updateAccounts(set<id> setAccIDs) {
        string strAccountID = null;
        string strRootCause = null;
        integer countMax ;
        map<string, integer> mapCount = new map<string, integer>();

        set<string> setConfigPickLists = getConfigPickListSet();

        list<Account>lstAccountToUpdate = new list<Account>();

// pass the accountID set from above loop to aggregate query
//Aggregate result will give total cases count for each account, for each root cause
        AggregateResult[] groupedResults = [
                SELECT AccountId,Root_Cause__c, COUNT(CaseNumber) cnt
                FROM case
                where AccountId in :setAccIDs and CreatedDate = LAST_N_DAYS:30
                GROUP BY Root_Cause__c, AccountId
        ];

        for (AggregateResult ar : groupedResults) {
            strAccountID = (String) ar.get('AccountId');
            strRootCause = (String) ar.get('Root_Cause__c');
            countMax = integer.valueof(ar.get('cnt'));


            if (string.isNotblank(strRootCause) && setConfigPickLists.contains(strRootCause)) {
                if (mapCount.containsKey(strAccountID))//if Account ID was filled in previous iterations
                {
                    if (mapCount.get(strAccountID) < countMax)//we check for max count only
                        mapCount.put(strAccountID, countMax);
                } else {
                    mapCount.put(strAccountID, countMax);
                }
            } else if (!mapCount.containsKey(strAccountID))//a blank root cause for an account will nullify the cronic count.
            {
                system.debug('----mapCount.containsKey(strAccountID)----' + mapCount.containsKey(strAccountID));
                mapCount.put(strAccountID, 0);
            }
        }

//Find accounts and set No Cronic incidence value
        for (Account acc : [Select id, No_Chronic_Incidents__c from Account where id in :mapCount.keyset()]) {
            if (mapCount.containsKey(acc.id)) {
                acc.No_Chronic_Incidents__c = mapCount.get(acc.id);
                lstAccountToUpdate.add(acc);
            }
        }
        system.debug('---lstAccountToUpdate---' + lstAccountToUpdate);
        update lstAccountToUpdate;
    }


}