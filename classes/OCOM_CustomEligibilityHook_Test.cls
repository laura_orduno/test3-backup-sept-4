@isTest(seeAllData=false)
public class OCOM_CustomEligibilityHook_Test{
    private static List<PricebookEntry> pbeList = new List<PricebookEntry>();
    
    @isTest 
    public static void createcartitems_test_One() {
        vlocity_cmt.FlowStaticMap.FlowMap.put('callEligibilityManagerInTest',false);
        HookCpqTestData.setUpData_createCartItems();
        List<OrderItem> oilist = [Select Id,vlocity_cmt__Product2Id__r.orderMgmtId__c from OrderItem];
        List<PriceBookEntry> pbeList2 = [Select Id,Product2.orderMgmtId__c from PricebookEntry];
        Test.startTest(); 
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.httpMethod = 'POST';
        List<Order> orderList = [SELECT Id, Name, Pricebook2Id FROM Order];
        String priceBookEntryID = HookCpqTestData.customPrice.Id;
        String orderid = HookCpqTestData.ord.ID;
        System.debug('createcartitems_testOne_priceBookEntryID :: '+priceBookEntryID);
        System.debug('createcartitems_testOne_Pricebook2Id :: '+orderList[0].Pricebook2Id);
        system.debug('createcartitems_testOne_OrderID________:' + HookCpqTestData.ord.ID + '___'+orderid);
        request.requestURI = '/services/apexrest/vlocity_cmt/v2/cpq/carts/'+orderid+'/items';
        Map<String,Object> itemsDetails =  new Map<String,Object> {'itemId' => priceBookEntryID};
            List<Map<String,Object>> itemList = new List<Map<String,Object>>();
        itemList.add(itemsDetails);
        List<PriceBookEntry> pbe = [Select Id, Product2Id, Pricebook2Id, Product2.RecordTypeId, Product2.RecordType.DeveloperName, ISActive from PricebookEntry];
        System.debug('pbe is :: '+pbe);
        Map<String,Object> requestBodyBlob = new Map<String,Object>();
        requestBodyBlob.put('items',itemList);
        requestBodyBlob.put('hierarchy',20);
        requestBodyBlob.put('lastItemId','');
        requestBodyBlob.put('pagesize',20);
        requestBodyBlob.put('fields','PriceBookEntry.Product2.orderMgmtId__c'); 
        System.debug('createcartitems_testOne______***' + JSON.serialize(requestBodyBlob));
        RestContext.request = request;
        request.requestBody = Blob.valueOf(JSON.serialize(requestBodyBlob));
        system.debug('Request.reqBODY___' + request.requestBody );
        RestContext.response = response;
        list<OrderItem> olist = new List<OrderItem>();
        olist = [Select Id,vlocity_cmt__Product2Id__r.ProductSpecification__r.Name from OrderItem];
  
        vlocity_cmt.APIItemsCartsCpqV2.createCartItems();
        vlocity_cmt.APIProductsCartsCpqV2.getProducts(); 
        vlocity_cmt.APIItemsCartsCpqV2.getCartsItems();
        vlocity_cmt.APIItemsCartsCpqV2.updateCartItems();
        request.httpMethod = 'DELETE';
        request.requestURI = '/services/apexrest/vlocity_cmt/v2/cpq/carts/'+orderid+'/items';
        vlocity_cmt.APIItemsCartsCpqV2.deleteCartsItems(); 
        String responseBody = RestContext.response.responseBody.toString();
        System.assert(responseBody!=null, 'ResponseBody should never be null.');
        System.assertEquals(200,response.statusCode);
        System.debug('AAAA.test:createcartitems_test_One____'+responseBody);
        OCOM_CustomEligibilityHook customHook = new OCOM_CustomEligibilityHook();
        Map<Id, priceBookEntry> entryMap = new Map<Id, priceBookEntry>();
        entryMap.put(HookCpqTestData.childpe1.ID,HookCpqTestData.childpe1);
        Map<Id, OrderItem> topOfferMap = new Map<Id, OrderItem>();
        topOfferMap.put(HookCpqTestData.orderitem1.Id,HookCpqTestData.orderitem1);
        customHook.availablePriceBookEntryMap = entryMap;
        OCOM_CustomEligibilityHook customHook1 = new OCOM_CustomEligibilityHook();
        Test.stopTest();
    }
   
   
    @isTest 
    public static void createcartitems_test_Two() {
        vlocity_cmt.FlowStaticMap.FlowMap.put('callEligibilityManagerInTest',false);
        HookCpqTestData.setUpData_createCartItems();
        Test.startTest();
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.httpMethod = 'PUT';
        List<Order> orderList = [SELECT Id, Name, Pricebook2Id FROM Order];
        String priceBookEntryID = HookCpqTestData.customPrice.Id;
        String orderid = HookCpqTestData.ord.Id;
        request.requestURI = '/services/apexrest/vlocity_cmt/v2/cpq/carts/'+orderid+'/products';
        Map<String,Object> itemsDetails =  new Map<String,Object> {'itemId' => priceBookEntryID};
            List<Map<String,Object>> itemList = new List<Map<String,Object>>();
        itemList.add(itemsDetails);
        List<PriceBookEntry> pbe = [Select Id, Product2Id, Pricebook2Id, Product2.RecordTypeId, Product2.RecordType.DeveloperName, ISActive from PricebookEntry];
        System.debug('pbe is :: '+pbe);
        Map<String,Object> requestBodyBlob = new Map<String,Object>();
        requestBodyBlob.put('items',itemList);
        requestBodyBlob.put('hierarchy',20);
        requestBodyBlob.put('lastItemId','');
        requestBodyBlob.put('pagesize',20);
        RestContext.request = request;
        request.requestBody = Blob.valueOf(JSON.serialize(requestBodyBlob));
        RestContext.response = response;
        vlocity_cmt.APIProductsCartsCpqV2.getProducts(); 
        vlocity_cmt.APIItemsCartsCpqV2.getCartsItems();
        vlocity_cmt.APIItemsCartsCpqV2.createCartItems();
        vlocity_cmt.APIItemsCartsCpqV2.updateCartItems();
        String responseBody = RestContext.response.responseBody.toString();
        System.assert('createcartitems_testtwo'+responseBody!=null, 'ResponseBody should never be null.');
        System.assertEquals(200,response.statusCode);
        System.debug('createcartitems_testtwo_AAAA.test:__createcartitems_test_One__'+responseBody);   
        OCOM_CustomEligibilityHook customHook = new OCOM_CustomEligibilityHook();
        Map<Id, priceBookEntry> entryMap = new Map<Id, priceBookEntry>();
        entryMap.put(HookCpqTestData.childpe1.ID,HookCpqTestData.childpe1);
        entryMap.put(HookCpqTestData.customPrice.ID,HookCpqTestData.customPrice);
        Map<Id, OrderItem> topOfferMap = new Map<Id, OrderItem>();
        topOfferMap.put(HookCpqTestData.orderItem1.Id,HookCpqTestData.orderItem1);
        customHook.availablePriceBookEntryMap = entryMap;
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        OCOM_CustomEligibilityHook customHook1 = new OCOM_CustomEligibilityHook();
        Test.stopTest();
    }
    
    
    @isTest static void createCartItems_test_three(){  
        vlocity_cmt.FlowStaticMap.FlowMap.put('callEligibilityManagerInTest',false);
        HookCpqTestData.setUpData_createCartItems();
        //vlocity_cmt.FlowStaticMap.FlowMap.put('runHirerachyTest',true);
        Test.startTest();
        Map<Id, priceBookEntry> entryMap = new Map<Id, priceBookEntry>();
        entryMap.put(HookCpqTestData.customPrice.ID,HookCpqTestData.customPrice);
        entryMap.put(HookCpqTestData.childpe1.ID,HookCpqTestData.childpe1);
        vlocity_cmt.FlowStaticMap.FlowMap.put('test.availablePriceBookEntryMap',entryMap);
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.httpMethod = 'POST';
        system.debug('createcartitems_test_three_OrderID________:' + HookCpqTestData.ord +'+++++' + HookCpqTestData.customPrice.Id );
        request.requestURI = '/services/apexrest/vlocity_cmt/v2/cpq/carts/'+HookCpqTestData.ord.id+'/items';
        Map<String,Object> itemsDetails =  new Map<String,Object> {'itemId' => HookCpqTestData.customPrice.Id};
            List<Map<String,Object>> itemList = new List<Map<String,Object>>();
        itemList.add(itemsDetails);
        Map<String,Object> requestBodyBlob = new Map<String,Object>();
        requestBodyBlob.put('items',itemList);
        requestBodyBlob.put('hierarchy',20);
        requestBodyBlob.put('lastItemId','');
        requestBodyBlob.put('pagesize',20);
        System.debug('createcartitems_test_three______***' + JSON.serialize(requestBodyBlob));
        RestContext.request = request;
        request.requestBody = Blob.valueOf(JSON.serialize(requestBodyBlob));
        system.debug('Request.reqBODY___' + request.requestBody );
        RestContext.response = response;
        vlocity_cmt.APIItemsCartsCpqV2.createCartItems();
        String responseBody = RestContext.response.responseBody.toString();
        system.debug('AAAAtest_createcartitems_test_Two:::: ' + responseBody);
        Test.stopTest();
    }
    
    
    @isTest static void updatecartitems_test_one(){         
        vlocity_cmt.FlowStaticMap.FlowMap.put('callEligibilityManagerInTest',false);
        HookCpqTestData.setUpData_updateCartItems();
        Map <String, String> jsonMap = new Map <String, String> ();
        jsonMap.put('cartId', HookCpqTestData.ord.Id);
        map<String,Object> recordMap = new map<String,Object>();
        map<String,Object> rootitemMap = new map<String,Object>();
        rootitemMap.put('value',HookCpqTestData.orderItem1.Id);
        recordMap.put('vlocity_cmt__RootItemId__c',rootitemMap);
        map<String,Object> IdMap = new map<String,Object>();
        IdMap.put('value',HookCpqTestData.orderItem1.Id);
        recordMap.put('Id',IdMap);   
        List<map<String,Object>> recordList = new List<map<String,Object>>();
        recordList.add(recordMap);  
        Map<String,Object> parentRecord = new Map<String,Object>();
        parentRecord.put('records',recordList);
        Map<String,Object> itemMap = new Map<String,Object>();
        itemMap.put('itemId',HookCpqTestData.customPrice.Id);
        itemMap.put('parentRecord',parentRecord);
        List<Map<String,Object>> itemList = new List<Map<String,Object>>();
        itemList.add(itemMap);
        Map<String,Object> finalMap = new Map<String,Object>();
        finalMap.put('items',itemList);
        finalMap.put('cartId',HookCpqTestData.ord.Id);
        String oid = HookCpqTestData.orderItem2.Id;
        finalMap.put('id',oid);
        finalMap.put('itemId',oid);
        String jsonString = JSON.serialize( finalMap);
        Object test1 = jsonString;
        vlocity_cmt.FlowStaticMap.FlowMap.put('preInvoke.addedItem',test1);
        Map<String, Object> input = new Map<String, Object>();
        Map<String, Object> output = new Map<String, Object>();
        Map<String, Object> options = new Map<String, Object>();
        OCOM_CustomEligibilityHook customHook1 = new OCOM_CustomEligibilityHook();
        customHook1.invokeMethod('putCartsItems.postInvoke',finalMap,output,options);  
        customHook1.invokeMethod('deleteCartsItems.PreInvoke',finalMap,output,options);  
        Map<String,vlocity_cmt.JSONResult> chiprodMap  = new Map<String,vlocity_cmt.JSONResult>();
        chiprodMap.put('vlocity_cmt.JSONResult.JSON_KEY_CHILDPRODUCTS_RESULT',null);
        //customHook1.deleteNonAvailableJSONNode(chiprodMap,1,1);
        //customHook1.deleteNonAvailableJSONNode(chiprodMap,2,1); 
        system.debug('>>>>> test11111_updatecartitems_test_one' );
    }
    
  
     @isTest 
    public static void getcartproducts_test_One() {
        vlocity_cmt.FlowStaticMap.FlowMap.put('callEligibilityManagerInTest',false);
        HookCpqTestData.setUpData_createCartItems();
        List<OrderItem> oilist = [Select Id,vlocity_cmt__Product2Id__r.orderMgmtId__c from OrderItem];
        List<PriceBookEntry> pbeList2 = [Select Id,Product2.orderMgmtId__c from PricebookEntry];
        Test.startTest(); 
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.httpMethod = 'GET';
        List<Order> orderList = [SELECT Id, Name, Pricebook2Id FROM Order];
        
        String priceBookEntryID = HookCpqTestData.customPrice.Id;
        String orderid = HookCpqTestData.ord.ID;
       
        request.requestURI = 'https://cs27.salesforce.com/services/apexrest/vlocity_cmt/v2/cpq/carts/'+HookCpqTestData.ord.ID+'/products?fields=Product2.orderMgmtId__c&pagesize=20&includeAttachment=true&hierarchy=20';
        
        system.debug('Request URi___' +request.requestURI);
         Map<String,Object> requestBodyBlob = new Map<String,Object>();
            List<Map<String,Object>> itemList = new List<Map<String,Object>>();
        List<PriceBookEntry> pbe = [Select Id, Product2Id, Pricebook2Id, Product2.RecordTypeId, Product2.RecordType.DeveloperName, ISActive,Product2.orderMgmtId__c from PricebookEntry];
        System.debug('pbe is :: '+pbe); 
        System.debug('createcartitems_testOne______***' + JSON.serialize(requestBodyBlob));
        RestContext.request = request;
        request.requestBody = Blob.valueOf(JSON.serialize(requestBodyBlob));
        system.debug('Request.reqBODY___' + request.requestBody );
        RestContext.response = response;
        list<OrderItem> olist = new List<OrderItem>();
        olist = [Select Id,vlocity_cmt__Product2Id__r.ProductSpecification__r.Name from OrderItem];
         vlocity_cmt.APIProductsCartsCpqV2.getProducts();
       
        String responseBody = RestContext.response.responseBody.toString();
        System.assert(responseBody!=null, 'ResponseBody should never be null.');
        System.assertEquals(200,response.statusCode);
        System.debug('AAAA.test_____ getcartproducts_test_One:'+responseBody);

        OCOM_CustomEligibilityHook customHook = new OCOM_CustomEligibilityHook();
        Map<Id, priceBookEntry> entryMap = new Map<Id, priceBookEntry>();
        entryMap.put(HookCpqTestData.childpe1.ID,HookCpqTestData.childpe1);
        Map<Id, OrderItem> topOfferMap = new Map<Id, OrderItem>();
        customHook.availablePriceBookEntryMap = entryMap;
        OCOM_CustomEligibilityHook customHook1 = new OCOM_CustomEligibilityHook();
        Test.stopTest();
    }
    @isTest 
    public static void putcartproducts_test_One() {
        
        
        Map<String,Object> linenumberMap = new Map<String,Object>();
        linenumberMap.put('value','0001');
        Map<String,Object> updatedRecordLineNumMap = new Map<String,Object>();
       updatedRecordLineNumMap.put('vlocity_cmt__LineNumber__c',linenumberMap);
        List<Object> recordsInfoList = new List<Object>();
        recordsInfoList.add(updatedRecordLineNumMap);
        Map<String,Object> recordMap = new MAp<String,Object>();
        recordMap.put('records',recordsInfoList);
        Map<String,Object> inputMap = new Map<String,Object>();
        inputMap.put('items',recordMap);
        OCOM_CustomEligibilityHook customHook1 = new OCOM_CustomEligibilityHook();
        customHook1.getPutCartItemsInputInfo(inputMap);
        
        Map<String,Object> IDMap = new Map<String,Object>();
        IDMap.put('value','0001');
        Map<String,Object> updatedRecordIdMap = new Map<String,Object>();
        updatedRecordIdMap.put('Id',IDMap);
          recordsInfoList.clear();
        recordsInfoList.add(updatedRecordIdMap);
        recordMap.put('records',recordsInfoList);
        inputMap.put('items',recordMap);
        customHook1.getPutCartItemsInputInfo(inputMap);
        
        
        Map<String,Object> ProductMap = new Map<String,Object>();
        ProductMap.put('Name','Office Internet');
        ProductMap.put('orderMgmtId__c','0012232');
        Map<String,Object> updatedRecordProductMap = new Map<String,Object>();
        updatedRecordProductMap.put('Product2',ProductMap);
          recordsInfoList.clear();
        recordsInfoList.add(updatedRecordProductMap);
        recordMap.put('records',recordsInfoList);
        inputMap.put('items',recordMap);
        customHook1.getPutCartItemsInputInfo(inputMap);
        
    }
    
}