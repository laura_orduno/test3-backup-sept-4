global class trac_Forecast_Commit_Load_Schedule implements schedulable {

	global void execute(SchedulableContext sc) {
		runUpdate();
	}
	
	public void runUpdate() {
		
		trac_Forecast_Report myR = new trac_Forecast_Report();
		
		myR.getForecasts_Commit_Load();
	}
}