//Generated by wsdl2apex

public class AsyncRateGroupInfo_EnterpriseB {
    public class pingResponse_elementFuture extends System.WebServiceCalloutFuture {
        public String getValue() {
            RateGroupInfo_EnterpriseB.pingResponse_element response = (RateGroupInfo_EnterpriseB.pingResponse_element)System.WebServiceCallout.endInvoke(this);
            return response.version;
        }
    }
}