@isTest
public class OCOM_DocuSignUtilClass_Test {

    private static final string Agmt_StatusCategory_Request='Request';
    private static final string Agmt_StatusCategory_In_Signatures='In Signatures';
    private static final string Agmt_Status_Ready_for_Signatures='Ready for Signatures';
    private static final string Agmt_Status_Fully_Signed='Fully Signed';
    private static final string Agmt_Status_Other_Party_Signatures='Other Party Signatures';
    private static final string Custom_setting_name='Apttus Comply Custom API Settings';
    private static final string Document_Access_Level='Read Only';
    private static final string Document_Format='pdf';
    private static final string Document_Format_vlocity='docx';


   @isTest(OnInstall=false)
     static void testMethod_Negative_Tests() {

        TestDataHelper.testAccountCreation();
        TestDataHelper.testContactCreation();

          Apttus_Comply_Custom_API_Settings__c customSetting = Apttus_Comply_Custom_API_Settings__c.getValues('Apttus Comply Custom API Settings');
        if(customSetting == null) {
            customSetting = new Apttus_Comply_Custom_API_Settings__c(Name = 'Apttus Comply Custom API Settings'/*, Template_Name__c = 'test Template for Comply Custom API 1'*/);       
            insert customSetting;
        }
        
        dsfs__DocuSignAccountConfiguration__c dsAC = new dsfs__DocuSignAccountConfiguration__c();
        dsAC.dsfs__DSProSFUsername__c = 'docusignUserName';
        dsAC.dsfs__DSProSFPassword__c = 'docusignPassword';
        dsAC.dsfs__AccountId__c = TestDataHelper.testAccountObj.Id;
        dsAC.dsfs__OrganizationId__c = UserInfo.getOrganizationId();
        insert dsAC;
         
        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'All Other Orders';
        insert testOpp_1;

        //Contract testContract_1 = new Contact(firstname = 'TestConFirstName', LastName = 'TestConLastname', accountId = TestDataHelper.testAccountObj.Id , Phone = '123456' ,                                             fax = '1234567', Email = 'test@gmail.com',  vlocity_cmt__OpportunityId__c = testOpp_1.Id);
        Contract testContract_1  = new Contract(Contract_Type__c='Amendment',AccountId=TestDataHelper.testAccountObj.id,ContractTerm=12,StartDate=system.today(),vlocity_cmt__OpportunityId__c = testOpp_1.Id , status = 'Draft');
        insert testContract_1;
         
        Test.startTest();
        OCOM_DocuSignUtilClass.doCancel_vlocity(null);

        System.debug('testContract_1___obj::' +testContract_1);
        OCOM_DocuSignUtilClass.doCancel_vlocity(testContract_1);

        testContract_1.Status = 'Contract Sent';
        testContract_1.Envelop_Id__c = '8fd115ee-43c2-480a-bcd3-008910ec4988';
        update testContract_1;


        System.debug('testContract_1___obj22::' +testContract_1);
        OCOM_DocuSignUtilClass.doCancel_vlocity(testContract_1);

        customSetting.Account_Id__c = TestDataHelper.testAccountObj.Id;
        customSetting.Anchor_Tab_String__c = null;
        update customSetting;
        
         OCOM_DocuSignUtilClass.doCancel_vlocity(testContract_1);

        customSetting.Anchor_Tab_String__c = 's1';
        customSetting.Account_Id__c = TestDataHelper.testAccountObj.Id;
        customSetting.Integrators_Key__c = null;
        update customSetting;
        
        
         OCOM_DocuSignUtilClass.doCancel_vlocity(testContract_1);

        customSetting.Anchor_Tab_String__c = 's1';
        customSetting.Account_Id__c = TestDataHelper.testAccountObj.Id;
        customSetting.Integrators_Key__c = 'YAHO-25f09494-4ed0-461c-90d5-04638ceda3b4';
        //customSetting.Password__c = null;
        update customSetting;
        
       OCOM_DocuSignUtilClass.doCancel_vlocity(testContract_1);

        
        customSetting.Anchor_Tab_String__c = 's1';
        customSetting.Account_Id__c = TestDataHelper.testAccountObj.Id;
        customSetting.Integrators_Key__c = 'YAHO-25f09494-4ed0-461c-90d5-04638ceda3b4';
        //customSetting.Password__c = 'test1234';
        customSetting.Web_Service_Url__c = null;
        update customSetting;
        
       OCOM_DocuSignUtilClass.doCancel_vlocity(testContract_1);

        customSetting.Anchor_Tab_String__c = 's1';
        customSetting.Account_Id__c = TestDataHelper.testAccountObj.Id;
        customSetting.Integrators_Key__c = 'YAHO-25f09494-4ed0-461c-90d5-04638ceda3b4';
        //customSetting.Password__c = 'test1234';
        customSetting.Web_Service_Url__c = 'https://demo.docusign.net/api/3.0/dsapi.asmx';
        //customSetting.User_Id__c = null;
        update customSetting;


        
        OCOM_DocuSignUtilClass.doCancel_vlocity(testContract_1);


        customSetting.Anchor_Tab_String__c = 's1';
        customSetting.Account_Id__c = TestDataHelper.testAccountObj.Id;
        customSetting.Integrators_Key__c = 'YAHO-25f09494-4ed0-461c-90d5-04638ceda3b4';
        //customSetting.Password__c = 'test1234';
        customSetting.Web_Service_Url__c = 'https://demo.docusign.net/api/3.0/dsapi.asmx';
        customSetting.Anchor_Tab_Date_String__c = 'd1';
        update customSetting;

        testContract_1.Status = 'Contract Sent';
        testContract_1.Envelop_Id__c = '8fd115ee-43c2-480a-bcd3-008910ec4988';
        testContract_1.OwnerId= UserInfo.getUserId();
        update testContract_1;
        OCOM_DocuSignUtilClass.doCancel_vlocity(testContract_1);
         
        Test.stopTest();
     }

@isTest(OnInstall=false)
    static void doCancel_vlocity_Positive_Test(){

        TestDataHelper.testAccountCreation();
        TestDataHelper.testContactCreation();

          Apttus_Comply_Custom_API_Settings__c customSetting = Apttus_Comply_Custom_API_Settings__c.getValues('Apttus Comply Custom API Settings');
        if(customSetting == null) {
            customSetting = new Apttus_Comply_Custom_API_Settings__c(Name = 'Apttus Comply Custom API Settings'/*, Template_Name__c = 'test Template for Comply Custom API 1'*/);       
            insert customSetting;
        }
        
        dsfs__DocuSignAccountConfiguration__c dsAC = new dsfs__DocuSignAccountConfiguration__c();
        dsAC.dsfs__DSProSFUsername__c = 'docusignUserName';
        dsAC.dsfs__DSProSFPassword__c = 'docusignPassword';
        dsAC.dsfs__AccountId__c = TestDataHelper.testAccountObj.Id;
        dsAC.dsfs__OrganizationId__c = UserInfo.getOrganizationId();
        insert dsAC;
         
        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'All Other Orders';
        insert testOpp_1;

        //Contract testContract_1 = new Contact(firstname = 'TestConFirstName', LastName = 'TestConLastname', accountId = TestDataHelper.testAccountObj.Id , Phone = '123456' ,                                             fax = '1234567', Email = 'test@gmail.com',  vlocity_cmt__OpportunityId__c = testOpp_1.Id);
        Contract testContract_1  = new Contract(Contract_Type__c='Amendment',AccountId=TestDataHelper.testAccountObj.id,ContractTerm=12,StartDate=system.today(),vlocity_cmt__OpportunityId__c = testOpp_1.Id , status = 'Draft');
        insert testContract_1;
      
        System.debug('testContract_1___obj::' +testContract_1);
        
        testContract_1.Status = 'Contract Sent';
        testContract_1.Envelop_Id__c = '8fd115ee-43c2-480a-bcd3-008910ec4988';
        update testContract_1;


        System.debug('testContract_1___obj22::' +testContract_1);
       
        customSetting.Account_Id__c = TestDataHelper.testAccountObj.Id;
        customSetting.Anchor_Tab_String__c = 's1';
        customSetting.Account_Id__c = TestDataHelper.testAccountObj.Id;
        customSetting.Integrators_Key__c = 'YAHO-25f09494-4ed0-461c-90d5-04638ceda3b4';
      
        customSetting.Web_Service_Url__c = 'https://demo.docusign.net/api/3.0/dsapi.asmx';
       
        customSetting.Web_Service_Url__c = 'https://demo.docusign.net/api/3.0/dsapi.asmx';
        customSetting.Anchor_Tab_Date_String__c = 'd1';
        update customSetting;
        
        
        testContract_1.Status = 'Contract Sent';
        testContract_1.Envelop_Id__c = '8fd115ee-43c2-480a-bcd3-008910ec4988';
        testContract_1.OwnerId= UserInfo.getUserId();
        update testContract_1;

        Test.starttest();
          //update
        Test.setMock(WebServiceMock.class, new OCOM_DocusignStatusAPI_MockUp());
         //end 
         
        OCOM_DocuSignUtilClass.doCancel_vlocity(testContract_1);
         
        Test.stopTest();
    }



   
    @isTest(OnInstall=false)
     static void testMethod_Vlocity2() {

     
       
        Test.startTest();
        TestDataHelper.testAccountCreation();
        TestDataHelper.testContactCreation();

         Apttus_Comply_Custom_API_Settings__c customSetting = Apttus_Comply_Custom_API_Settings__c.getValues('Apttus Comply Custom API Settings');
        if(customSetting == null) {
            customSetting = new Apttus_Comply_Custom_API_Settings__c(Name = 'Apttus Comply Custom API Settings'/*, Template_Name__c = 'test Template for Comply Custom API 1'*/);       
            insert customSetting;
        }
        
        dsfs__DocuSignAccountConfiguration__c dsAC = new dsfs__DocuSignAccountConfiguration__c();
        dsAC.dsfs__DSProSFUsername__c = 'docusignUserName';
        dsAC.dsfs__DSProSFPassword__c = 'docusignPassword';
        dsAC.dsfs__AccountId__c = TestDataHelper.testAccountObj.Id;
        dsAC.dsfs__OrganizationId__c = UserInfo.getOrganizationId();
        insert dsAC;
         
        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'All Other Orders';
        insert testOpp_1;

        //Contract testContract_1 = new Contact(firstname = 'TestConFirstName', LastName = 'TestConLastname', accountId = TestDataHelper.testAccountObj.Id , Phone = '123456' ,                                             fax = '1234567', Email = 'test@gmail.com',  vlocity_cmt__OpportunityId__c = testOpp_1.Id);
        Contract testContract_1  = new Contract(Contract_Type__c='Amendment',AccountId=TestDataHelper.testAccountObj.id,ContractTerm=12,StartDate=system.today(),vlocity_cmt__OpportunityId__c = testOpp_1.Id);
        insert testContract_1;

          
  

         vlocity_cmt__ContractVersion__c testContractVersion = new vlocity_cmt__ContractVersion__c(Name='Test Version 1', vlocity_cmt__ContractId__c = testContract_1.Id);
        insert testContractVersion;
         
          Blob b1 = Blob.valueOf('Test Data');  
        Attachment attachment1 = new Attachment();  
        attachment1.ParentId = testContract_1.Id;  
        attachment1.Name = 'Test Attachment for Parent';  
        attachment1.Body = b1;  
        insert(attachment1);  
         
         //custom setting
        Apttus_Comply_Custom_API_Settings__c customSetting3 = Apttus_Comply_Custom_API_Settings__c.getValues('Apttus Comply Custom API Settings');
        if(customSetting3 != null) {
            //customSetting3.Template_Name__c = 'Template_Name***';
            //update customSetting3;
        } else {
            customSetting3 = new Apttus_Comply_Custom_API_Settings__c(Name = 'Apttus Comply Custom API Settings'/*, Template_Name__c = 'Template_Name***'*/);       
            insert customSetting3;
        }
        
        OCOM_DocuSignUtilClass.doSend_vlocity(null, attachment1.Id, testContractVersion.Id);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, null, testContractVersion.Id);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, attachment1.Id, null);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, attachment1.Id, testContractVersion.Id);
         
      Test.stopTest();



     }

@isTest(OnInstall=false)
     static void testMethod_doSend_vlocity_negative_Test() {

    
        
        TestDataHelper.testAccountCreation();
        TestDataHelper.testContactCreation();

         Apttus_Comply_Custom_API_Settings__c customSetting = Apttus_Comply_Custom_API_Settings__c.getValues('Apttus Comply Custom API Settings');
        if(customSetting == null) {
            customSetting = new Apttus_Comply_Custom_API_Settings__c(Name = 'Apttus Comply Custom API Settings'/*, Template_Name__c = 'test Template for Comply Custom API 1'*/);       
            insert customSetting;
        }
        
        dsfs__DocuSignAccountConfiguration__c dsAC = new dsfs__DocuSignAccountConfiguration__c();
        dsAC.dsfs__DSProSFUsername__c = 'docusignUserName';
        dsAC.dsfs__DSProSFPassword__c = 'docusignPassword';
        dsAC.dsfs__AccountId__c = TestDataHelper.testAccountObj.Id;
        dsAC.dsfs__OrganizationId__c = UserInfo.getOrganizationId();
        insert dsAC;
         
        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'All Other Orders';
        insert testOpp_1;

        //Contract testContract_1 = new Contact(firstname = 'TestConFirstName', LastName = 'TestConLastname', accountId = TestDataHelper.testAccountObj.Id , Phone = '123456' ,                                             fax = '1234567', Email = 'test@gmail.com',  vlocity_cmt__OpportunityId__c = testOpp_1.Id);
        Contract testContract_1  = new Contract(Contract_Type__c='Amendment',AccountId=TestDataHelper.testAccountObj.id,ContractTerm=12,StartDate=system.today(),vlocity_cmt__OpportunityId__c = testOpp_1.Id);
        insert testContract_1;

          
  

         vlocity_cmt__ContractVersion__c testContractVersion = new vlocity_cmt__ContractVersion__c(Name='Test Version 1', vlocity_cmt__ContractId__c = testContract_1.Id);
        insert testContractVersion;
         
          Blob b1 = Blob.valueOf('Test Data');  
        Attachment attachment1 = new Attachment();  
        attachment1.ParentId = testContract_1.Id;  
        attachment1.Name = 'Test Attachment for Parent';  
        attachment1.Body = b1;  
        insert(attachment1);  
         
         //custom setting
        Apttus_Comply_Custom_API_Settings__c customSetting3 = Apttus_Comply_Custom_API_Settings__c.getValues('Apttus Comply Custom API Settings');
        if(customSetting3 != null) {
            //customSetting3.Template_Name__c = 'Template_Name***';
            //update customSetting3;
        } else {
            customSetting3 = new Apttus_Comply_Custom_API_Settings__c(Name = 'Apttus Comply Custom API Settings'/*, Template_Name__c = 'Template_Name***'*/);       
            insert customSetting3;
        }

        testContract_1.CustomerSignedID  = TestDataHelper.testContactObj.id;
        update testContract_1;
         
         Test.startTest();
       OCOM_DocuSignUtilClass.doSend_vlocity(null, attachment1.Id, testContractVersion.Id);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, null, testContractVersion.Id);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, attachment1.Id, null);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, attachment1.Id, testContractVersion.Id);
         
         
        customSetting.Account_Id__c = null;
        update customSetting;
        
        OCOM_DocuSignUtilClass.doSend_vlocity(null, attachment1.Id, testContractVersion.Id);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, null, testContractVersion.Id);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, attachment1.Id, null);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, attachment1.Id, testContractVersion.Id);
        
        customSetting.Account_Id__c = TestDataHelper.testAccountObj.Id;
        customSetting.Anchor_Tab_String__c = null;
        update customSetting;
        
         OCOM_DocuSignUtilClass.doSend_vlocity(null, attachment1.Id, testContractVersion.Id);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, null, testContractVersion.Id);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, attachment1.Id, null);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, attachment1.Id, testContractVersion.Id);
        
        customSetting.Anchor_Tab_String__c = 's1';
        customSetting.Account_Id__c = TestDataHelper.testAccountObj.Id;
        customSetting.Integrators_Key__c = null;
        update customSetting;
        
        
        OCOM_DocuSignUtilClass.doSend_vlocity(null, attachment1.Id, testContractVersion.Id);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, null, testContractVersion.Id);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, attachment1.Id, null);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, attachment1.Id, testContractVersion.Id);
        
        
        customSetting.Anchor_Tab_String__c = 's1';
        customSetting.Account_Id__c = TestDataHelper.testAccountObj.Id;
        customSetting.Integrators_Key__c = 'YAHO-25f09494-4ed0-461c-90d5-04638ceda3b4';
        //customSetting.Password__c = null;
        update customSetting;
        
        OCOM_DocuSignUtilClass.doSend_vlocity(null, attachment1.Id, testContractVersion.Id);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, null, testContractVersion.Id);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, attachment1.Id, null);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, attachment1.Id, testContractVersion.Id);
        
        
        customSetting.Anchor_Tab_String__c = 's1';
        customSetting.Account_Id__c = TestDataHelper.testAccountObj.Id;
        customSetting.Integrators_Key__c = 'YAHO-25f09494-4ed0-461c-90d5-04638ceda3b4';
        //customSetting.Password__c = 'test1234';
        customSetting.Web_Service_Url__c = null;
        update customSetting;
            
            OCOM_DocuSignUtilClass.doSend_vlocity(null, attachment1.Id, testContractVersion.Id);
            OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, null, testContractVersion.Id);
            OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, attachment1.Id, null);
            OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, attachment1.Id, testContractVersion.Id);
        Test.stopTest();
        }

     
    @isTest(OnInstall=false)
     static void testMethod_doSend_vlocity() {

    
        
        TestDataHelper.testAccountCreation();
        TestDataHelper.testContactCreation();

         Apttus_Comply_Custom_API_Settings__c customSetting = Apttus_Comply_Custom_API_Settings__c.getValues('Apttus Comply Custom API Settings');
        if(customSetting == null) {
            customSetting = new Apttus_Comply_Custom_API_Settings__c(Name = 'Apttus Comply Custom API Settings'/*, Template_Name__c = 'test Template for Comply Custom API 1'*/);       
            insert customSetting;
        }
        
        dsfs__DocuSignAccountConfiguration__c dsAC = new dsfs__DocuSignAccountConfiguration__c();
        dsAC.dsfs__DSProSFUsername__c = 'docusignUserName';
        dsAC.dsfs__DSProSFPassword__c = 'docusignPassword';
        dsAC.dsfs__AccountId__c = TestDataHelper.testAccountObj.Id;
        dsAC.dsfs__OrganizationId__c = UserInfo.getOrganizationId();
        insert dsAC;
         
        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'All Other Orders';
        insert testOpp_1;

        //Contract testContract_1 = new Contact(firstname = 'TestConFirstName', LastName = 'TestConLastname', accountId = TestDataHelper.testAccountObj.Id , Phone = '123456' ,                                             fax = '1234567', Email = 'test@gmail.com',  vlocity_cmt__OpportunityId__c = testOpp_1.Id);
        Contract testContract_1  = new Contract(Contract_Type__c='Amendment',AccountId=TestDataHelper.testAccountObj.id,ContractTerm=12,StartDate=system.today(),vlocity_cmt__OpportunityId__c = testOpp_1.Id);
        insert testContract_1;

          
  

         vlocity_cmt__ContractVersion__c testContractVersion = new vlocity_cmt__ContractVersion__c(Name='Test Version 1', vlocity_cmt__ContractId__c = testContract_1.Id);
        insert testContractVersion;
         
          Blob b1 = Blob.valueOf('Test Data');  
        Attachment attachment1 = new Attachment();  
        attachment1.ParentId = testContract_1.Id;  
        attachment1.Name = 'Test Attachment for Parent';  
        attachment1.Body = b1;  
        insert(attachment1);  
         
         //custom setting
        Apttus_Comply_Custom_API_Settings__c customSetting3 = Apttus_Comply_Custom_API_Settings__c.getValues('Apttus Comply Custom API Settings');
        if(customSetting3 != null) {
            //customSetting3.Template_Name__c = 'Template_Name***';
            //update customSetting3;
        } else {
            customSetting3 = new Apttus_Comply_Custom_API_Settings__c(Name = 'Apttus Comply Custom API Settings'/*, Template_Name__c = 'Template_Name***'*/);       
            insert customSetting3;
        }

        testContract_1.CustomerSignedID  = TestDataHelper.testContactObj.id;
        update testContract_1;
         
         
       /* OCOM_DocuSignUtilClass.doSend_vlocity(null, attachment1.Id, testContractVersion.Id);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, null, testContractVersion.Id);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, attachment1.Id, null);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, attachment1.Id, testContractVersion.Id*/
         
         
        customSetting.Account_Id__c = null;
        update customSetting;
        
        /*OCOM_DocuSignUtilClass.doSend_vlocity(null, attachment1.Id, testContractVersion.Id);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, null, testContractVersion.Id);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, attachment1.Id, null);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, attachment1.Id, testContractVersion.Id*/
        
        customSetting.Account_Id__c = TestDataHelper.testAccountObj.Id;
        customSetting.Anchor_Tab_String__c = null;
        update customSetting;
        
         /*OCOM_DocuSignUtilClass.doSend_vlocity(null, attachment1.Id, testContractVersion.Id);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, null, testContractVersion.Id);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, attachment1.Id, null);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, attachment1.Id, testContractVersion.Id);*/
        
        customSetting.Anchor_Tab_String__c = 's1';
        customSetting.Account_Id__c = TestDataHelper.testAccountObj.Id;
        customSetting.Integrators_Key__c = null;
        update customSetting;
        
        
        /* OCOM_DocuSignUtilClass.doSend_vlocity(null, attachment1.Id, testContractVersion.Id);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, null, testContractVersion.Id);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, attachment1.Id, null);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, attachment1.Id, testContractVersion.Id*/
        
        
        customSetting.Anchor_Tab_String__c = 's1';
        customSetting.Account_Id__c = TestDataHelper.testAccountObj.Id;
        customSetting.Integrators_Key__c = 'YAHO-25f09494-4ed0-461c-90d5-04638ceda3b4';
        //customSetting.Password__c = null;
        update customSetting;
        
       /* OCOM_DocuSignUtilClass.doSend_vlocity(null, attachment1.Id, testContractVersion.Id);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, null, testContractVersion.Id);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, attachment1.Id, null);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, attachment1.Id, testContractVersion.Id);*/
        
        
        customSetting.Anchor_Tab_String__c = 's1';
        customSetting.Account_Id__c = TestDataHelper.testAccountObj.Id;
        customSetting.Integrators_Key__c = 'YAHO-25f09494-4ed0-461c-90d5-04638ceda3b4';
        //customSetting.Password__c = 'test1234';
        customSetting.Web_Service_Url__c = null;
        update customSetting;
        
       /* OCOM_DocuSignUtilClass.doSend_vlocity(null, attachment1.Id, testContractVersion.Id);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, null, testContractVersion.Id);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, attachment1.Id, null);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, attachment1.Id, testContractVersion.Id*/
        
        customSetting.Anchor_Tab_String__c = 's1';
        customSetting.Account_Id__c = TestDataHelper.testAccountObj.Id;
        customSetting.Integrators_Key__c = 'YAHO-25f09494-4ed0-461c-90d5-04638ceda3b4';
        //customSetting.Password__c = 'test1234';
        customSetting.Web_Service_Url__c = 'https://demo.docusign.net/api/3.0/dsapi.asmx';
        //customSetting.User_Id__c = null;
        update customSetting;
        
        //doSendTest(agreeement.Id, attachment.Id);
         Test.startTest();
       
          //update
        Test.setMock(WebServiceMock.class, new OCOM_DocuSignApi_MockUp());
         //end 
        
          OCOM_DocuSignUtilClass.doSend_vlocity(null, attachment1.Id, testContractVersion.Id);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, null, testContractVersion.Id);
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, attachment1.Id, null);
                                              
                                              
        OCOM_DocuSignUtilClass.doSend_vlocity(testContract_1.Id, attachment1.Id, testContractVersion.Id);

      Test.stopTest();



     }
     

}