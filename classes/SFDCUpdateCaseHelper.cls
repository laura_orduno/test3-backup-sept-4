public class SFDCUpdateCaseHelper {

	static public Case UpdateCase(String caseNumber, String caseStatus, String TicketNo, String errorCode, String CaseCommentId) {

		System.debug('SFDCUpdateCaseHelper->UpdateCase, CaseNumber(' + caseNumber + '), TicketNumber(' + TicketNo + ')');
		System.debug('SFDCUpdateCaseHelper->UpdateCase, CommentId(' + CaseCommentId + '), CaseStatus(' + CaseStatus + ')');
		System.debug('SFDCUpdateCaseHelper->UpdateCase, errorCode(' + errorCode + ')');
		if (caseNumber != 'TELUS-SFDC') {
			try {
				Case tmpCase = [
						SELECT Id, CaseNumber, Lynx_Ticket_Number__c, Status, NotifyCollaboratorString__c
						FROM Case
						WHERE CaseNumber = :caseNumber
				];
				//string LynxTicketNumber = tmpCase.Lynx_Ticket_Number__c;
				if (tmpCase != null && TicketNo != null) {
					tmpCase.Lynx_Ticket_Number__c = TicketNo;

					if (caseStatus != null) {
						tmpCase.Status = caseStatus;
					}
					System.debug('SFDCUpdateCaseHelper->UpdateCase, CaseNumber(' + tmpCase.CaseNumber + '), TicketNumber(' + TicketNo + ')');
					update tmpCase;
				}
				if (String.isBlank(TicketNo) || String.isNotBlank(errorCode)) {
					try {
						Ticket_Event__c NewEvent = new Ticket_Event__c(
								Case_Number__c = caseNumber,
								Failure_Reason__c = errorCode,
								Event_Type__c = 'New Ticket',
								Status__c = 'New',
								Case_Id__c = tmpCase.Id
						);
						System.debug('SFDCUpdateCaseHelper->NewEvent, CaseNumber(' + tmpCase.CaseNumber + '), TicketNumber(' + TicketNo + ')');
						insert NewEvent;

					} catch (DmlException e) {
						System.debug('SFDCUpdateCaseHelper->TicketEvent, exception: ' + e.getMessage());
					}

				}
				// Rob please check this condition here the first two will always be true
				if (String.isNotBlank(TicketNo) && String.isNotBlank(errorCode) && String.isNotBlank(CaseCommentId)) {
					System.debug('LINE 46:SFDCUpdateCaseHelper->TicketEvent, Ticket not = NULL: ' + TicketNo);
					List<CaseComment> getCommentID = [Select Id, CommentBody from CaseComment where ID = :CaseCommentId];
					system.debug('Line 48:' + getCommentID);

					if (getCommentID.size() > 0) {
						System.debug('LINE 48:SFDCUpdateCaseHelper->TicketEvent, COMMENTID not = NULL: ' + getCommentID);
						string caseCommentId1 = getCommentID[0].Id;
						string caseComment = getCommentID[0].CommentBody;
						try {
							Ticket_Event__c NewEvent = new Ticket_Event__c(
									Case_Number__c = caseNumber,
									Failure_Reason__c = errorCode,
									Event_Type__c = 'New Activity',
									Status__c = 'New',
									Lynx_Ticket_Number__c = TicketNo,
									Case_Comment_Id__c = caseCommentId1,
									Case_Comment__c = caseComment,
									Case_Id__c = tmpCase.Id
							);
							System.debug('SFDCUpdateCaseHelper->NewActivity, CaseNumber(' + tmpCase.CaseNumber + '), TicketNumber(' + TicketNo + ')');
							insert NewEvent;

						} catch (DmlException e) {
							System.debug('SFDCUpdateCaseHelper->NewActivity, exception: ' + e.getMessage());
						}
					} else {
						System.debug('LINE 70:SFDCUpdateCaseHelper->TicketEvent, COMMENTID is NULL: ' + getCommentID);
						string caseCommentId1 = '';
						string caseComment = '';
						try {
							Ticket_Event__c NewEvent = new Ticket_Event__c(
									Case_Number__c = caseNumber,
									Failure_Reason__c = errorCode,
									Event_Type__c = 'New Activity',
									Status__c = 'New',
									Lynx_Ticket_Number__c = TicketNo,
									Case_Comment_Id__c = caseCommentId1,
									Case_Comment__c = caseComment,
									Case_Id__c = tmpCase.Id
							);
							System.debug('SFDCUpdateCaseHelper->NewActivity, CaseNumber(' + tmpCase.CaseNumber + '), TicketNumber(' + TicketNo + ')');
							insert NewEvent;

						} catch (DmlException e) {
							System.debug('SFDCUpdateCaseHelper->NewActivity, exception: ' + e.getMessage());
						}
					}

				}

			} catch (Exception e) {
				System.debug('SFDCUpdateCaseHelper->UpdateCase, exception: ' + e.getMessage());
			}

		} else {
			if (String.isNotBlank(TicketNo)) {
				Case NumGetCase = [
						SELECT Id, CaseNumber, Lynx_Ticket_Number__c, Status, NotifyCollaboratorString__c
						FROM Case
						WHERE Lynx_Ticket_Number__c = :TicketNo
				];
				if (NumGetCase != null) {
					if (String.isNotBlank(CaseCommentId)) {
						try {
							Ticket_Event__c NewEvent = new Ticket_Event__c(
									Case_Number__c = NumGetCase.CaseNumber,
									Failure_Reason__c = 'Case update did not work, Lynx ticket status is: ' + caseStatus + ' , Case Number was not returned by sending App',
									Event_Type__c = 'New Activity',
									Status__c = 'New',
									Lynx_Ticket_Number__c = TicketNo,
									Case_Comment_Id__c = CaseCommentId,
									Case_Id__c = NumGetCase.Id
							);
							System.debug('SFDCUpdateCaseHelper->NewEvent, CaseNumber(' + NumGetCase.CaseNumber + '), TicketNumber(' + TicketNo + ')');
							insert NewEvent;

						} catch (DmlException e) {
							System.debug('SFDCUpdateCaseHelper->TicketEvent, exception: ' + e.getMessage());
						}
					}
					if (caseStatus == 'closed' || caseStatus == 'Closed') {
						NumGetCase.Status = 'Closed';
						update NumGetCase;
					}

				}
			}

		}
		return null;
	}

}