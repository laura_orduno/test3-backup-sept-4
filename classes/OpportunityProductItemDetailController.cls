/******************************************************************************
# File..................: OpportunityProductItemDetailController
# Version...............: 1
# Created by............: Sandip Chaudhari (IBM)
# Description...........: Class is to Create, Validate, Save Service Address for opportunity product. 
                          This class is main controller for detail page. Created as a part of PAC R2
******************************************************************************/

Public class OpportunityProductItemDetailController{
    public ServiceAddressData SAData  { get; set; }
    public String fullAddress { get; set; }
    Opp_Product_Item__c oppProductObj;
    public String isError{ get; set; }
    
    // Constructor to get required details like opportunity product item record, prepare full address, instantiate objects, mapping

    public OpportunityProductItemDetailController(ApexPages.StandardController controller){
        isError = 'onLoad';
        SAData = new ServiceAddressData();
        SAData.isAdressRequired = true;
        SAData.isProvinceRequired = true;
        SAData.isCityRequired = true;
        oppProductObj = (Opp_Product_Item__c)controller.getRecord();
        Opp_Product_Item__c objOpptyProd = [SELECT Building_Name__c, Suite_Unit__c, Street__c, Street_Direction__c,
                                            Opportunity__c, Opportunity__r.accountId, Opportunity__r.Primary_Order_Contact__c,
                                            City__c, State_Province__c, Postal_Code__c, Country__c, Floor__c,
                                            Street_Type__c, Is_SACG__c, Notes_for_SACG_team__c, New_Construction__c, 
                                            Temporary_Job_Phone__c, Service_Address_Status__c
                                            FROM Opp_Product_Item__c
                                            WHERE Id=: oppProductObj.Id];
        //Concatnate address to show on page
        if(objOpptyProd != null){
            fullAddress = '';
            
            if(objOpptyProd.Floor__c != null && objOpptyProd.Floor__c != 'null'){
                fullAddress = fullAddress  + ' ' + objOpptyProd.Floor__c;
            }
            if(objOpptyProd.Building_Name__c != null && objOpptyProd.Building_Name__c != 'null'){
                if(objOpptyProd.Suite_Unit__c != objOpptyProd.Building_Name__c){
                    fullAddress = fullAddress  + ' ' + objOpptyProd.Building_Name__c;
                }
            }
            if(objOpptyProd.Suite_Unit__c != null && objOpptyProd.Suite_Unit__c != 'null'){
                fullAddress = fullAddress  + ' ' + objOpptyProd.Suite_Unit__c;
            }
            if(objOpptyProd.Street__c != null && objOpptyProd.Street__c != 'null'){
                fullAddress = fullAddress  + ' ' + objOpptyProd.Street__c;
            }
            if(objOpptyProd.Street_Direction__c != null && objOpptyProd.Street_Direction__c != 'null'){
                fullAddress = fullAddress  + ' ' + objOpptyProd.Street_Direction__c;
            }
            
            SAData.address = fullAddress;
            
            if(objOpptyProd.City__c != null && objOpptyProd.City__c != 'null'){
                fullAddress = fullAddress  + ' ' + objOpptyProd.City__c;
            }
            if(objOpptyProd.State_Province__c != null && objOpptyProd.State_Province__c != 'null'){
                fullAddress = fullAddress  + ' ' + objOpptyProd.State_Province__c;
            }
            if(objOpptyProd.Postal_Code__c != null && objOpptyProd.Postal_Code__c != 'null'){
                fullAddress = fullAddress  + ' ' + objOpptyProd.Postal_Code__c;
            }
            if(objOpptyProd.Country__c != null && objOpptyProd.Country__c != 'null'){
                fullAddress = fullAddress  + ' ' + objOpptyProd.Country__c;
            }
            
            //Assigned to respective vaiables for AddressData class
            SAData.searchString = fullAddress;
           /* if(objOpptyProd.Building_Name__c != null && objOpptyProd.Building_Name__c != ''){
                SAData.address = objOpptyProd.Building_Name__c + ' ' + objOpptyProd.Street__c;
            }else{ */
            //    SAData.address = objOpptyProd.Street__c;
            //}
            SAData.street = objOpptyProd.Street__c;
            SAData.city = objOpptyProd.City__c;
            SAData.province = objOpptyProd.State_Province__c;
            SAData.canadaProvince = objOpptyProd.State_Province__c;
            SAData.usState = objOpptyProd.State_Province__c;
            SAData.country = objOpptyProd.Country__c;
            SAData.postalCode = objOpptyProd.Postal_Code__c;
            SAData.accountId = objOpptyProd.Opportunity__r.accountId;
            SAData.opportunityId = objOpptyProd.Opportunity__c;
            SAData.contactId = objOpptyProd.Opportunity__r.Primary_Order_Contact__c;
            SAData.isSACG = objOpptyProd.Is_SACG__c;
            System.debug('@@@On Load' + objOpptyProd.Is_SACG__c);
            SAData.notesForSACGTeam = objOpptyProd.Notes_for_SACG_team__c ;
            
            if(objOpptyProd.New_Construction__c == System.Label.PACNewConstruction){
                SAData.isNewConstruction = true;
            }
            
            if(objOpptyProd.Temporary_Job_Phone__c == System.Label.PACTempJobPhome){
                SAData.isTempJobPhone = true;
            }
            
            if(objOpptyProd.Service_Address_Status__c == System.Label.PACInValidCaptureNewAddrStatus){
                SAData.isManualCapture = true;
            }else{
                SAData.isManualCapture = false;
            }
        }
    }
    
    /*
    * Created by :  Sandip Chaudhari
    * Name - updateServiceLocation
    * Description - This method is map the PAC address values with the existing address object
    * and update the object.
    */
    public pageReference updateServiceLocation(){
        isError = 'inSave';
         
        system.debug('#### City' + SAData.city);
        system.debug('#### postalCode' + SAData.postalCode);
        oppProductObj.Building_Name__c = SAData.buildingNumber;
        oppProductObj.Floor__c = SAData.aptNumber; // as per observation, aptNumber hold the floor number
        oppProductObj.Street_Type__c = SAData.streetTypeCode;
        oppProductObj.Suite_Unit__c = SAData.suiteNumber;
        
        oppProductObj.Street__c = SAData.address;
        /*if(SAData.street!=null){
            if(SAData.vector != null && SAData.vector != ''){
                oppProductObj.Street__c = SAData.vector + ' ' + SAData.street;
            }else{
                oppProductObj.Street__c = SAData.street;
            }
        }else{
            oppProductObj.Street__c = SAData.address;
        }*/
        oppProductObj.Street_Direction__c = SAData.streetDirection;
        oppProductObj.City__c = SAData.city;
        oppProductObj.State_Province__c = SAData.province;
        oppProductObj.Postal_Code__c = SAData.postalCode;
        oppProductObj.Country__c = SAData.country;
        oppProductObj.Service_Address_Status__c = SAData.captureNewAddrStatus;
        System.debug('@@@SACG REquest' + SAData.isSACG);
        if(SAData.isSACG != null){
            oppProductObj.Is_SACG__c = SAData.isSACG;
        }else{
            oppProductObj.Is_SACG__c = false;
        }
        oppProductObj.Notes_for_SACG_team__c = SAData.notesForSACGTeam;
        if(SAData.isNewConstruction){
            oppProductObj.New_Construction__c = System.Label.PACNewConstruction;
        }else{
            oppProductObj.New_Construction__c = '';
        }
        if(SAData.isTempJobPhone){
            oppProductObj.Temporary_Job_Phone__c = System.Label.PACTempJobPhome;
        }else{
            oppProductObj.Temporary_Job_Phone__c = '';
        }
        
        try{
            update oppProductObj;
            processSACG();
        }catch(Exception ex){
            isError = 'InError';
            ApexPages.addMessages(ex);          
            return null;
        }
        return null;
        //PageReference redirectPage = new PageReference('/apex/OpportunityProductItemDetail?id='+ oppProductObj.Id +'&sfdc.override=1');
        //redirectPage.setRedirect(true);
        //return redirectPage;
    }
    /*
    * Created by :  Sandip Chaudhari
    * This method used to send address for validation through SACG process.
    */
     
    public void processSACG(){
        if(SAData.isSACG == true){
            PACUtility utilObj = new PACUtility();
            utilObj.sendToSACG(SAData);
        }
    }  
}