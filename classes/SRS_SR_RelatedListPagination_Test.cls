/*******************************************************************************
* Project      : SRS R3
* Class        : SRS_SR_RelatedList_Test
* Description  : Test class for SRS_SR_RelatedList Controller class
* Author       : Aditya Jamwal, IBM
* Date         : July-15-2014
*
* Modification Log:
* ------------------------------------------------------------------------------
* Ver      Date                Modified By           Description
* ------------------------------------------------------------------------------
* 
* ------------------------------------------------------------------------------
*******************************************************************************/

@isTest
private class SRS_SR_RelatedListPagination_Test {
    
    static testMethod void SRS_SR_RelatedList_test_1() {
        /*Custom setting add*/
        Holidays__c holidays = new Holidays__c();
        holidays.name = 'Leave';
        holidays.ON__c = TRUE;
        holidays.ISD_Dispatch__c = TRUE;
        holidays.Date__c = system.today();
        insert holidays;
        
        /*Custom setting add*/
        test.startTest();
        Id SrRecordtypeID =Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Provide/Install').getRecordTypeId();
        contact c= new contact(firstname='aditya',lastname='jamwal');
        insert c;
    
        Opportunity opp= new Opportunity(Name='OppTest', StageName='Originated', CloseDate=system.today(),Primary_Order_Contact__c=c.id);
        insert Opp;
        
        Account objacc=new Account(name='testAcc');
        insert objacc;
        
        List<Service_Request__c> SRList = new List<Service_Request__c>();
        
        Service_Request__c sereq  = new Service_Request__c(Account_Name__c=objAcc.id,opportunity__c=opp.id,Legacy_Orders_count__c='20',Fox__c='1234',Target_Date_Type__c='notnull',Service_Request_Status__c='completed',Target_Date__c=system.today(),Submitted_Date__c=system.now(),recordtypeid=SrRecordtypeID,DD_Scheduled__c=Date.newInstance(1960, 2, 17),Requested_Date__c=system.today(),Jeopardy_Description__c='test',Jeop_code__c='wewew',Condition_color__c='green', DAC_Scheduled__c = system.today().addDays(1), RTI_Scheduled__c = system.today().addDays(3), PTD_Scheduled__c = system.today().addDays(25), Workflow_Request_Type__c='Solution Control'); 
        
        System.assertEquals('OppTest',opp.Name);
        
        SRList.add(sereq);
        //insert sereq;
        System.assertEquals('completed',sereq.Service_Request_Status__c);
        
        Service_Request__c sereq1  = new Service_Request__c(Account_Name__c=objAcc.id,opportunity__c=opp.id,Legacy_Orders_count__c='20',Fox__c='1234',Target_Date_Type__c='notnull',Service_Request_Status__c='completed',Target_Date__c=system.today(),Submitted_Date__c=system.now(),recordtypeid=SrRecordtypeID,DD_Scheduled__c=Date.newInstance(1960, 2, 17),Requested_Date__c=system.today(),Condition_color__c='green', DAC_Scheduled__c = system.today().addDays(1), RTI_Scheduled__c = system.today().addDays(3), PTD_Scheduled__c = system.today().addDays(25), RTI_Actual__c = system.today().addDays(1), Workflow_Request_Type__c='Solution Control');  
        
        SRList.add(sereq1);
        //insert sereq1;
        
        Service_Request__c sereq2  = new Service_Request__c(Account_Name__c=objAcc.id,opportunity__c=opp.id,Legacy_Orders_count__c='20',Fox__c='1234',Target_Date_Type__c='notnull',Service_Request_Status__c='completed',Target_Date__c=system.today(),Submitted_Date__c=system.now(),recordtypeid=SrRecordtypeID,DD_Scheduled__c=Date.newInstance(1960, 2, 17),Requested_Date__c=system.today(),Condition_color__c='green', DAC_Scheduled__c = system.today().addDays(1), RTI_Scheduled__c = system.today().addDays(3), PTD_Scheduled__c = system.today().addDays(25), RTI_Actual__c = system.today().addDays(1), Workflow_Request_Type__c='Solution Control');  
        
        SRList.add(sereq2);
        //insert sereq2;
        
        Service_Request__c sereq3  = new Service_Request__c(Account_Name__c=objAcc.id,opportunity__c=opp.id,Legacy_Orders_count__c='20',Fox__c='1234',Target_Date_Type__c='notnull',Service_Request_Status__c='completed',Target_Date__c=system.today(),Submitted_Date__c=system.now(),recordtypeid=SrRecordtypeID,DD_Scheduled__c=Date.newInstance(1960, 2, 17),Requested_Date__c=system.today(),Condition_color__c='green', DAC_Scheduled__c = system.today().addDays(1), RTI_Scheduled__c = system.today().addDays(3), PTD_Scheduled__c = system.today().addDays(25), RTI_Actual__c = system.today().addDays(1), Workflow_Request_Type__c='Solution Control');  
        
        SRList.add(sereq3);
        
        insert SRList;
        
        SMBCare_Address__c objAddress=new SMBCare_Address__c(Account__c=objAcc.id,State__c='BC',Postal_Code__c='V6B 0M3',
                            Suite_Number__c='3rd',Address_Type__c='Standard', country__c = 'Canada',
                            Street_Number__c='12',Street_Name__c='testStr',street_Direction__c='left',
                            GPS_Coordinates__c='23',Google_Map_Link__c='gmail.com',
                            City__c='Vancouver',Province__c='BC'
                            );
        insert objAddress;
        
        List<SRS_Service_Address__c> lstObjSA=new List<SRS_Service_Address__c>();
        
        SRS_Service_Address__c objSA1=new SRS_Service_Address__c(Location__c='Location A',Building_Name__c='test_buld',
                        Service_Request__c=sereq.id,CLLI__c='123',Address__c=objAddress.id,
                        NPA_NXX__c='123321',Floor__c='2nd',New_Building__c='testNewBuld',Cabinet__c='2nd',Rack__c='23',Shelf__c='12'
            );
        lstObjSA.add(objSA1);
        SRS_Service_Address__c objSA2=new SRS_Service_Address__c(Location__c='Location A',Building_Name__c='test_buld',
                        Service_Request__c=sereq1.id,CLLI__c='123',Address__c=objAddress.id,
                        NPA_NXX__c='123321',Floor__c='2nd',New_Building__c='testNewBuld',Cabinet__c='2nd',Rack__c='23',Shelf__c='12'
            );
        lstObjSA.add(objSA2);
        
        SRS_Service_Address__c objSA3=new SRS_Service_Address__c(Location__c='Location A',Building_Name__c='test_buld',
                        Service_Request__c=sereq2.id,CLLI__c='123',Address__c=objAddress.id,
                        NPA_NXX__c='123321',Floor__c='2nd',New_Building__c='testNewBuld',Cabinet__c='2nd',Rack__c='23',Shelf__c='12'
            );
        lstObjSA.add(objSA3);
        
        insert lstObjSA;
        
        system.debug('@@test class'+sereq+'\n'+sereq1);
        System.assertEquals('1234',sereq1.Fox__c);
         SRS_PODS_Product__c Prod = new SRS_PODS_Product__c(  Single_Address__c= true,Name='ISDN PRI');
        insert Prod;
        SRS_PODS_Answer__c SRans= new SRS_PODS_Answer__c(Service_Request__c=sereq1.id,GroupSerial__c=1,SRS_Question_ID__c=1,SRS_Group_Question__c=1,FOX_PARAMETER__c='1',SRS_PODS_Answer__c='wer');
        insert SRans;
        SRS_Service_Address__c sadd = new SRS_Service_Address__c(location__c ='Location A',Service_Request__c=sereq.id);
        insert sadd;
        Contracts__c contract = new Contracts__c(Contract_Number__c='1234',Contract_Remarks__c='asgeys',Contract_Term_Months__c='6',Contract_Type__c='zs',Contract__c=sereq1.id,New_Record_Type__c='hh',Opportunity__c=opp.id, Existing_Contract_Number__c='23231',Existing_ECB_Contract_ID__c='12332');
		insert contract;
    
     
        test.stopTest();
        List<SRS_SR_RelatedListPaginationController.SerReq> SerReqL = new List<SRS_SR_RelatedListPaginationController.SerReq>();
        SRS_SR_RelatedListPaginationController.SerReq req= new SRS_SR_RelatedListPaginationController.SerReq();
		SRS_SR_RelatedListPaginationController.SerReq req2a= new SRS_SR_RelatedListPaginationController.SerReq(sereq1,false,'addr','c','p');
        req.SerReq=sereq;
        req.checked=true;
        req.Full_Address='street';
        req.city='markham';
        req.prov ='NH';
		req2a.checked=true;
        SerReqL.add(req);
		SerReqL.add(req2a);
        SerReqL.sort();
        
        Test.setCurrentPageReference(new PageReference('Page.SRS_SR_RelatedList')); 
        System.currentPageReference().getParameters().put('Show', '');
        ApexPAges.StandardController sc = new ApexPages.StandardController(opp);
        SRS_SR_RelatedListPaginationController src= new SRS_SR_RelatedListPaginationController(sc);
        src.Opp=Opp;
        src.MarkSelectedServiceRequestComplete();

        src.cloneMove = 'No';
        src.isMove = false;
        
        src.SRCompleteId=sereq.id;
        service_request__c CompleteSR = new service_request__c();
		src.CompleteSR=CompleteSR;
        src.CompleteSR.DD_Actual__c=null;
        src.MarkSelectedServiceRequestComplete();
		src.CompleteSR.DD_Actual__c=system.today();
        src.SRId=sereq1.id;    
        src.SerReqList=SerReqL;
        src.ActiveCloneTypePopUp();
		src.remark='remark';
		src.ReasontoCancel='reason';
		
		src.Issuredelete=true;
        src.cancelMSr();
        src.CancelSelectedServiceRequest();  
        src.CancelOnlyOriginatedRequest();
//        src.getpicklistoptions();
//        src.getSortDirection();
        src.SaveInlineEdit();
        src.SubmitServiceRequest();
        src.DeleteServiceRequest();
        src.MarkSelectedServiceRequestComplete();
        src.CancelSelectedServiceRequest();       
//        src.setCheckAll();
        src.checkAll= true;
//        src.setUnCheckAll();   
//        src.setCheckAll();
        src.SortRelatedListwithColumn();
        src.Submit();
        src.cancelSub();
        src.MarkSelectedServiceRequestComplete();
        src.SaveInlineEdit();
        src.cancelSub();
//        src.getSerReqList();
//        src.setSortDirection('DESC');
        SerReqL.sort();
        src.remark='remark';
        src.CancelSelectedServiceRequest();
        src.Submit();
        src.CompleteServiceRequest();
        src.isOnlyOriginatedSRSelected();
       // src.CancelOnlyOriginatedRequest();
        src.isAnyCompleteorCancelledSRSelected();
        src.OK();
//		src.isAnySRSelected();
		src.getItems();
		src.getNoOfClone();
		src.setNoOfClone(1);
//		src.getReasonToCanceloptions();
        src.Issuredelete=false;
        src.DeleteServiceRequest();
         List<Holidays__c> lstHolidays = Holidays__c.getAll().Values();
        src.findNoOfWorkingDays(system.today(), system.today().addDays(83));
        src.findNoOfHolidays(system.today(), system.today().addDays(83), 'ON', lstHolidays );
        //................Sorting by City....................   
        
        SRS_SR_RelatedListPaginationController.SORT_FIELD='city';
        SRS_SR_RelatedListPaginationController.SORT_DIR = 'ASC';
        SRS_SR_RelatedListPaginationController.SerReq req1= new SRS_SR_RelatedListPaginationController.SerReq();
        req1.SerReq=sereq;
        req1.checked=true;
        req1.Full_Address='street';
        req1.city='markham';
        req1.prov ='NH';
        SerReqL.add(req1);
        SerReqL.sort();
        src.updatecheck =true;
        
        //................Sorting by Prov....................   
        
        SRS_SR_RelatedListPaginationController.SORT_FIELD='prov';
        SRS_SR_RelatedListPaginationController.SerReq req2= new SRS_SR_RelatedListPaginationController.SerReq();
        req2.SerReq=sereq;
        req2.checked=true;
        req2.Full_Address='street';
        req2.city='markham';
        req2.prov ='NH';
        SerReqL.add(req2);
        SerReqL.sort();
        
        //................Sorting by Target Date....................    
        
        SRS_SR_RelatedListPaginationController.SORT_FIELD='Target_Date__c';
        SRS_SR_RelatedListPaginationController.SerReq req3= new SRS_SR_RelatedListPaginationController.SerReq();
        req3.SerReq=sereq1;
        req3.checked=true;
        req3.Full_Address='street';
        req3.city='markham';
        req3.prov ='NH';
        SerReqL.add(req3);
        SerReqL.sort();
        
        //................Sorting by Submitted Date.................... 
        
        SRS_SR_RelatedListPaginationController.SORT_FIELD='Submitted_Date__c';
        SRS_SR_RelatedListPaginationController.SerReq req4= new SRS_SR_RelatedListPaginationController.SerReq();
        req4.SerReq=sereq1;
        req4.checked=true;
        req4.Full_Address='street';
        req4.city='markham';
        req4.prov ='NH';
        SerReqL.add(req3);
        SerReqL.sort();        
        
        //................Sorting by Target Date Type....................   
        
        SRS_SR_RelatedListPaginationController.SORT_FIELD='Target_Date_Type__c';
        SRS_SR_RelatedListPaginationController.SerReq req5= new SRS_SR_RelatedListPaginationController.SerReq();
        req5.SerReq=sereq1;
        SerReqL.add(req5);
        SerReqL.sort();
        
        //................Sorting by Service Request Status.................... 
        
        SRS_SR_RelatedListPaginationController.SORT_FIELD='Service_Request_Status__c';
        SRS_SR_RelatedListPaginationController.SerReq req6= new SRS_SR_RelatedListPaginationController.SerReq();
        req6.SerReq=sereq1;
        SerReqL.add(req6);
        SerReqL.sort();
        
        //................Sorting by Service Request Type....................   
        
        SRS_SR_RelatedListPaginationController.SORT_FIELD='Recordtypename';
        SRS_SR_RelatedListPaginationController.SerReq req8= new SRS_SR_RelatedListPaginationController.SerReq();
        req8.SerReq=sereq1;
        req8.Recordtypename='prequal';      
        SerReqL.add(req8);
        SerReqL.sort();  
        
        //................Sorting by Fox....................            
        
        SRS_SR_RelatedListPaginationController.SORT_FIELD='Fox__c';
        SRS_SR_RelatedListPaginationController.SerReq req9= new SRS_SR_RelatedListPaginationController.SerReq();
        req9.SerReq=sereq1;
        SerReqL.add(req9);
        SerReqL.sort();
        
        //................Sorting by Name....................   
        
        SRS_SR_RelatedListPaginationController.SORT_FIELD='Name';
        SRS_SR_RelatedListPaginationController.SerReq req10= new SRS_SR_RelatedListPaginationController.SerReq();
        req10.SerReq=sereq1;
        SerReqL.add(req10);
        SerReqL.sort();
        
        //................Sorting by Last Modified Date....................
        
        SRS_SR_RelatedListPaginationController.SORT_FIELD='LastModifiedDate';
        SRS_SR_RelatedListPaginationController.SerReq req11= new SRS_SR_RelatedListPaginationController.SerReq();
        req11.SerReq=sereq1;
        SerReqL.add(req11);
        SerReqL.sort();
        
        //................Sorting by Legacy Order count....................
        
        SRS_SR_RelatedListPaginationController.SORT_FIELD='Legacy_Order_count__c';
        SRS_SR_RelatedListPaginationController.SerReq req12= new SRS_SR_RelatedListPaginationController.SerReq();
        req12.SerReq=sereq1;
        SerReqL.add(req12);
        SerReqL.sort();
        
         //................Sorting by PTD Scheduled ....................
        
        SRS_SR_RelatedListPaginationController.SORT_FIELD='PTD_Scheduled__c';
        SRS_SR_RelatedListPaginationController.SerReq req13= new SRS_SR_RelatedListPaginationController.SerReq();
        req12.SerReq=sereq1;
        SerReqL.add(req13);
        SerReqL.sort(); 
            
        //................Sorting by Full Address ....................
        
        SRS_SR_RelatedListPaginationController.SORT_FIELD='Full_Address';
        SRS_SR_RelatedListPaginationController.SerReq req14= new SRS_SR_RelatedListPaginationController.SerReq();
        req12.SerReq=sereq1;
        SerReqL.add(req14);
        SerReqL.sort();    
        
       //................Sorting by Condition ....................
        
        SRS_SR_RelatedListPaginationController.SORT_FIELD='Condition_Sort_Index__c';
        SRS_SR_RelatedListPaginationController.SerReq req15= new SRS_SR_RelatedListPaginationController.SerReq();
        req12.SerReq=sereq1;
        SerReqL.add(req15);
        SerReqL.sort(); 
        
        //................Sorting by Jeopdary ....................
        
        SRS_SR_RelatedListPaginationController.SORT_FIELD='Jeopdary_Image__c';
        SRS_SR_RelatedListPaginationController.SerReq req16= new SRS_SR_RelatedListPaginationController.SerReq();
        req12.SerReq=sereq1;
        SerReqL.add(req14);
        SerReqL.sort(); 
        //................Sorting by SRS PODS Product Name ....................
        
        SRS_SR_RelatedListPaginationController.SORT_FIELD='SRS_PODS_Product__r.Name';
        SRS_SR_RelatedListPaginationController.SerReq req17= new SRS_SR_RelatedListPaginationController.SerReq();
        req12.SerReq=sereq1;
        SerReqL.add(req17);
        SerReqL.sort();    
        
        SRS_SR_RelatedListPaginationController.SORT_FIELD='Description__c';
        SRS_SR_RelatedListPaginationController.SerReq req18= new SRS_SR_RelatedListPaginationController.SerReq();
        req12.SerReq=sereq1;
        SerReqL.add(req18);
        SerReqL.sort();
        
        SRS_SR_RelatedListPaginationController.SORT_FIELD='Critical_Notes__c';
        SRS_SR_RelatedListPaginationController.SerReq req19= new SRS_SR_RelatedListPaginationController.SerReq();
        req12.SerReq=sereq1;
        SerReqL.add(req19);
        SerReqL.sort();
        
        SRS_SR_RelatedListPaginationController.SORT_FIELD='Target_Date_Type__c';
        SRS_SR_RelatedListPaginationController.SerReq req20= new SRS_SR_RelatedListPaginationController.SerReq();
        req12.SerReq=sereq1;
        SerReqL.add(req20);
        SerReqL.sort();
        
        SRS_SR_RelatedListPaginationController.SORT_FIELD='Opportunity__r.name';
        SRS_SR_RelatedListPaginationController.SerReq req21= new SRS_SR_RelatedListPaginationController.SerReq();
        req12.SerReq=sereq1;
        SerReqL.add(req21);
        SerReqL.sort();
        
        SRS_SR_RelatedListPaginationController.SORT_FIELD='Contract__r.Name';
        SRS_SR_RelatedListPaginationController.SerReq req22= new SRS_SR_RelatedListPaginationController.SerReq();
        req12.SerReq=sereq1;
        SerReqL.add(req22);
        SerReqL.sort();
        
        SRS_SR_RelatedListPaginationController.SORT_FIELD='Next_to_work__c';
        SRS_SR_RelatedListPaginationController.SerReq req23= new SRS_SR_RelatedListPaginationController.SerReq();
        req12.SerReq=sereq1;
        SerReqL.add(req23);
        SerReqL.sort();
        
        SRS_SR_RelatedListPaginationController.SORT_FIELD='ERD_Scheduled__c';
        SRS_SR_RelatedListPaginationController.SerReq req24= new SRS_SR_RelatedListPaginationController.SerReq();
        req12.SerReq=sereq1;
        SerReqL.add(req24);
        SerReqL.sort();
        
        SRS_SR_RelatedListPaginationController.SORT_FIELD='DAC_Scheduled__c';
        SRS_SR_RelatedListPaginationController.SerReq req25= new SRS_SR_RelatedListPaginationController.SerReq();
        req12.SerReq=sereq1;
        SerReqL.add(req25);
        SerReqL.sort();
        
        SRS_SR_RelatedListPaginationController.SORT_FIELD='RTI_Scheduled__c';
        SRS_SR_RelatedListPaginationController.SerReq req26= new SRS_SR_RelatedListPaginationController.SerReq();
        req12.SerReq=sereq1;
        SerReqL.add(req26);
        SerReqL.sort();
        
        SRS_SR_RelatedListPaginationController.SORT_FIELD='PTD_Scheduled__c';
        SRS_SR_RelatedListPaginationController.SerReq req27= new SRS_SR_RelatedListPaginationController.SerReq();
        req12.SerReq=sereq1;
        SerReqL.add(req27);
        SerReqL.sort();
        
        SRS_SR_RelatedListPaginationController.SORT_FIELD='Service_Address__c'; 
        SRS_SR_RelatedListPaginationController.SerReq req28 = new SRS_SR_RelatedListPaginationController.SerReq();
        req28.SerReq=sereq1;
        SerReqL.clear();
        SerReqL.add(req28);
         
        SRS_SR_RelatedListPaginationController.SerReq req29 = new SRS_SR_RelatedListPaginationController.SerReq();
        req29.SerReq=sereq2;
        SerReqL.add(req29);
        SerReqL.sort();
        
        SRS_SR_RelatedListPaginationController.SORT_FIELD='City__c'; 
        SRS_SR_RelatedListPaginationController.SerReq req30 = new SRS_SR_RelatedListPaginationController.SerReq();
        req30.SerReq=sereq2;
        //SerReqL.clear();
        SerReqL.add(req30);
        SerReqL.sort();
         
        SRS_SR_RelatedListPaginationController.SORT_FIELD='Prov__c';
        SRS_SR_RelatedListPaginationController.SerReq req31 = new SRS_SR_RelatedListPaginationController.SerReq();
        req31.SerReq=sereq2;
        //SerReqL.clear();
        SerReqL.add(req31);         
        SerReqL.sort();
         
        SRS_SR_RelatedListPaginationController.SORT_FIELD='Workflow_Request_Type__c'; 
        SRS_SR_RelatedListPaginationController.SerReq req32 = new SRS_SR_RelatedListPaginationController.SerReq();
        SerReqL.add(req32);
        SerReqL.sort();
    }
	/* Original test method that failed SOQL 101
    static testMethod void SRS_SR_RelatedList_test_2() {
        contact c= new contact(firstname='aditya',lastname='jamwal');
        insert c;
        
        Opportunity opp= new Opportunity(Name='OppTest', StageName='Originated', CloseDate=system.today(),Primary_Order_Contact__c=c.id);
        insert Opp;
        list<service_request__c> newServiceRequests=new list<service_request__c>();
        Service_Request__c sereq  = new Service_Request__c(Service_Request_Status__c='completed',opportunity__c=opp.id);
        Service_Request__c sereq1  = new Service_Request__c(opportunity__c=opp.id,Legacy_Orders_count__c='20',Fox__c='1234',Target_Date_Type__c='notnull',Service_Request_Status__c='completed',Target_Date__c=system.today(),Submitted_Date__c=system.now());
        newServiceRequests.add(sereq);
        newServiceRequests.add(sereq1);
        test.startTest();
        insert newServiceRequests;
        SRS_PODS_Product__c Prod = new SRS_PODS_Product__c(  Single_Address__c= true,Name='ISDN PRI');
        insert Prod;
        SRS_PODS_Answer__c SRans= new SRS_PODS_Answer__c(Service_Request__c=sereq1.id,GroupSerial__c=1,SRS_Question_ID__c=1,SRS_Group_Question__c=1,FOX_PARAMETER__c='1',SRS_PODS_Answer__c='wer');
        insert SRans;
        SRS_Service_Address__c sadd = new SRS_Service_Address__c(Service_Request__c=sereq1.id);
        insert sadd;
        Contracts__c contract = new Contracts__c(Contract_Number__c='1234',Contract_Remarks__c='asgeys',Contract_Term_Months__c='6',Contract_Type__c='zs',Contract__c=sereq1.id,New_Record_Type__c='hh',Opportunity__c=opp.id,Existing_Contract_Number__c='23231',Existing_ECB_Contract_ID__c='12332');
        insert contract;
        test.stopTest();
        Test.setCurrentPageReference(new PageReference('Page.SRS_SR_RelatedList'));
        System.currentPageReference().getParameters().put('Show', 'All');
        List<SRS_SR_RelatedListPaginationController.SerReq> SerReqL = new List<SRS_SR_RelatedListPaginationController.SerReq>();
        SRS_SR_RelatedListPaginationController.SerReq req= new SRS_SR_RelatedListPaginationController.SerReq();
        req.SerReq=sereq;
        req.checked=true;
        req.Full_Address='street';
        req.city='markham';
        req.prov ='NH';
        SerReqL.add(req);
        SerReqL.sort();
        ApexPAges.StandardController sc = new ApexPages.StandardController(opp);
        SRS_SR_RelatedListPaginationController src= new SRS_SR_RelatedListPaginationController(sc);
        
        src.SRId=sereq1.id;
        src.noOfClone=1;       
        src.cloneType='Clone by Product';
        
        src.cloneType='Clone by Address';
        src.CloneeServiceRequest();
        src.cloneType='Clone by Product';
        src.CloneeServiceRequest();
        src.SendCancelServiceRequestToFox();
        
        Webservice_Integration_Error_Log__c newLog = new Webservice_Integration_Error_Log__c(
            Apex_Class_and_Method__c = 'Test',
            SFDC_Record_Id__c = src.SRId,
            Object_Name__c = 'Service_Request__c'
        );
        
        insert newLog;
        
        SRS_SR_RelatedListPaginationController.executePull(newLog.Id);
        
        
    }*/
    @istest
    static void cloneByAddress() {
        contact c= new contact(firstname='aditya',lastname='jamwal');
        insert c;
        Opportunity opp= new Opportunity(Name='OppTest', StageName='Originated', CloseDate=system.today(),Primary_Order_Contact__c=c.id);
        insert Opp;
        list<service_request__c> newServiceRequests=new list<service_request__c>();
        Service_Request__c sereq1  = new Service_Request__c(opportunity__c=opp.id,Legacy_Orders_count__c='20',Fox__c='1234',Target_Date_Type__c='notnull',Service_Request_Status__c='completed',Target_Date__c=system.today(),Submitted_Date__c=system.now());
        newServiceRequests.add(sereq1);
        test.startTest();
        insert newServiceRequests;
        SRS_PODS_Product__c Prod = new SRS_PODS_Product__c(  Single_Address__c= true,Name='ISDN PRI');
        insert Prod;
        SRS_PODS_Answer__c SRans= new SRS_PODS_Answer__c(Service_Request__c=sereq1.id,GroupSerial__c=1,SRS_Question_ID__c=1,SRS_Group_Question__c=1,FOX_PARAMETER__c='1',SRS_PODS_Answer__c='wer');
        insert SRans;
        SRS_Service_Address__c sadd = new SRS_Service_Address__c(Service_Request__c=sereq1.id);
        insert sadd;
        Contracts__c contract = new Contracts__c(Contract_Number__c='1234',Contract_Remarks__c='asgeys',Contract_Term_Months__c='6',Contract_Type__c='zs',Contract__c=sereq1.id,New_Record_Type__c='hh',Opportunity__c=opp.id,Existing_Contract_Number__c='23231',Existing_ECB_Contract_ID__c='12332');
        insert contract;
        test.stopTest();
        Test.setCurrentPageReference(new PageReference('Page.SRS_SR_RelatedList'));
        ApexPAges.StandardController sc = new ApexPages.StandardController(opp);
        SRS_SR_RelatedListPaginationController src= new SRS_SR_RelatedListPaginationController(sc);
        
        src.SRId=sereq1.id;
        src.noOfClone=1;
        src.cloneType='Clone by Address';
        src.CloneeServiceRequest();
        Webservice_Integration_Error_Log__c newLog = new Webservice_Integration_Error_Log__c(
            Apex_Class_and_Method__c = 'Test',
            SFDC_Record_Id__c = src.SRId,
            Object_Name__c = 'Service_Request__c'
        );
        
        insert newLog;
        
        SRS_SR_RelatedListPaginationController.executePull(newLog.Id);
    }
    @istest
    static void cloneByProduct() {
        contact c= new contact(firstname='aditya',lastname='jamwal');
        insert c;
        Opportunity opp= new Opportunity(Name='OppTest', StageName='Originated', CloseDate=system.today(),Primary_Order_Contact__c=c.id);
        insert Opp;
        list<service_request__c> newServiceRequests=new list<service_request__c>();
        Service_Request__c sereq1  = new Service_Request__c(opportunity__c=opp.id,Legacy_Orders_count__c='20',Fox__c='1234',Target_Date_Type__c='notnull',Service_Request_Status__c='completed',Target_Date__c=system.today(),Submitted_Date__c=system.now());
        newServiceRequests.add(sereq1);
        test.startTest();
        insert newServiceRequests;
        SRS_PODS_Product__c Prod = new SRS_PODS_Product__c(  Single_Address__c= true,Name='ISDN PRI');
        insert Prod;
        SRS_PODS_Answer__c SRans= new SRS_PODS_Answer__c(Service_Request__c=sereq1.id,GroupSerial__c=1,SRS_Question_ID__c=1,SRS_Group_Question__c=1,FOX_PARAMETER__c='1',SRS_PODS_Answer__c='wer');
        insert SRans;
        SRS_Service_Address__c sadd = new SRS_Service_Address__c(Service_Request__c=sereq1.id);
        insert sadd;
        Contracts__c contract = new Contracts__c(Contract_Number__c='1234',Contract_Remarks__c='asgeys',Contract_Term_Months__c='6',Contract_Type__c='zs',Contract__c=sereq1.id,New_Record_Type__c='hh',Opportunity__c=opp.id,Existing_Contract_Number__c='23231',Existing_ECB_Contract_ID__c='12332');
        insert contract;
        test.stopTest();
        Test.setCurrentPageReference(new PageReference('Page.SRS_SR_RelatedList'));
        ApexPAges.StandardController sc = new ApexPages.StandardController(opp);
        SRS_SR_RelatedListPaginationController src= new SRS_SR_RelatedListPaginationController(sc);
        
        src.SRId=sereq1.id;
        src.noOfClone=1;
        src.cloneType='Clone by Product';
        src.CloneeServiceRequest();
        Webservice_Integration_Error_Log__c newLog = new Webservice_Integration_Error_Log__c(
            Apex_Class_and_Method__c = 'Test',
            SFDC_Record_Id__c = src.SRId,
            Object_Name__c = 'Service_Request__c'
        );
        
        insert newLog;
        
        SRS_SR_RelatedListPaginationController.executePull(newLog.Id);
    }
    @istest
    static void testShowDifferentTypesSRS() {
        contact c= new contact(firstname='aditya',lastname='jamwal');
        insert c;
        
        Opportunity opp= new Opportunity(Name='OppTest', StageName='Originated', CloseDate=system.today(),Primary_Order_Contact__c=c.id);
        insert Opp;
        list<service_request__c> newServiceRequests=new list<service_request__c>();
        Service_Request__c sereq  = new Service_Request__c(Service_Request_Status__c='completed',opportunity__c=opp.id);
        Service_Request__c sereq1  = new Service_Request__c(opportunity__c=opp.id,Legacy_Orders_count__c='20',Fox__c='1234',Target_Date_Type__c='notnull',Service_Request_Status__c='completed',Target_Date__c=system.today(),Submitted_Date__c=system.now());
        newServiceRequests.add(sereq);
        newServiceRequests.add(sereq1);
        test.startTest();
        insert newServiceRequests;
        SRS_PODS_Product__c Prod = new SRS_PODS_Product__c(  Single_Address__c= true,Name='ISDN PRI');
        insert Prod;
        SRS_PODS_Answer__c SRans= new SRS_PODS_Answer__c(Service_Request__c=sereq1.id,GroupSerial__c=1,SRS_Question_ID__c=1,SRS_Group_Question__c=1,FOX_PARAMETER__c='1',SRS_PODS_Answer__c='wer');
        insert SRans;
        SRS_Service_Address__c sadd = new SRS_Service_Address__c(Service_Request__c=sereq1.id);
        insert sadd;
        Contracts__c contract = new Contracts__c(Contract_Number__c='1234',Contract_Remarks__c='asgeys',Contract_Term_Months__c='6',Contract_Type__c='zs',Contract__c=sereq1.id,New_Record_Type__c='hh',Opportunity__c=opp.id,Existing_Contract_Number__c='23231',Existing_ECB_Contract_ID__c='12332');
        insert contract;
        Test.setCurrentPageReference(new PageReference('Page.SRS_SR_RelatedList')); 
        System.currentPageReference().getParameters().put('Show', 'All');
        List<SRS_SR_RelatedListPaginationController.SerReq> SerReqL = new List<SRS_SR_RelatedListPaginationController.SerReq>();
        SRS_SR_RelatedListPaginationController.SerReq req= new SRS_SR_RelatedListPaginationController.SerReq();
        req.SerReq=sereq;
        req.checked=true;
        req.Full_Address='street';
        req.city='markham';
        req.prov ='NH';
        SerReqL.add(req);
        SerReqL.sort();
        test.stopTest();
    }
    /*
     * Deprecated to see if coverage is over 80%
     * 
     static testMethod void SRS_SR_RelatedList_test_3() {
        contact c= new contact(firstname='aditya',lastname='jamwal');
        insert c;
        
        Opportunity opp= new Opportunity(Name='OppTest', StageName='Originated', CloseDate=system.today(),Primary_Order_Contact__c=c.id);
        insert Opp;
        Service_Request__c sereq  = new Service_Request__c(Service_Request_Status__c='completed',opportunity__c=opp.id);
        test.startTest();
        insert sereq;
        Service_Request_Employee__c SE= new  Service_Request_Employee__c(Service_Request__c =sereq.id);
        insert SE;
        
        Service_Request_Contact__c RelatedContact =  new Service_Request_Contact__c(Service_Request__c=sereq.id);
        insert RelatedContact;
        test.stopTest();
        List<SRS_SR_RelatedListPaginationController.SerReq> SerReqL = new List<SRS_SR_RelatedListPaginationController.SerReq>();
        SRS_SR_RelatedListPaginationController.SerReq req= new SRS_SR_RelatedListPaginationController.SerReq();
        req.SerReq=sereq;
        req.checked=true;
        req.Full_Address='street';
        req.city='markham';
        req.prov ='NH';
        SerReqL.add(req);
        SerReqL.sort();
        
        ApexPAges.StandardController sc = new ApexPages.StandardController(opp);
        SRS_SR_RelatedListPaginationController src= new SRS_SR_RelatedListPaginationController(sc);
        src.cloneMove = 'No';
        src.isMove = false;
        src.SRId=sereq.id;
        src.noOfClone=1;
        src.cloneType='Clone by Address';
        src.CloneeServiceRequest();
        src.cloneType='Clone by Product';
        src.CloneeServiceRequest();
    }*/
    
    static testMethod void SRS_SR_RelatedList_test_4() {
        contact c= new contact(firstname='aditya',lastname='jamwal');
        insert c;
        
        Opportunity opp= new Opportunity(Name='OppTest', StageName='Originated', CloseDate=system.today(),Primary_Order_Contact__c=c.id);
        insert Opp;
        Service_Request__c sereq  = new Service_Request__c(Service_Request_Status__c='completed',opportunity__c=opp.id);
        
        test.startTest();
        insert sereq;
        System.assertEquals('completed',sereq.Service_Request_Status__c);
        
        Schema.DescribeSObjectResult dsr = Schema.SObjectType.SRS_Service_Request_Related_Order__c; 
        Map<String,Schema.RecordTypeInfo> rtMapByName1 = dsr.getRecordTypeInfosByName();
        Schema.RecordTypeInfo rtByName1 =  rtMapByName1.get('Reference Number');
        
        SRS_Service_Request_Related_Order__c Ro= new SRS_Service_Request_Related_Order__c(name='19870',recordtypeid=rtByName1.getRecordtypeid(),System__c='SIMS',Status__c='C',RO_Service_Request__c =sereq.id); 
        
        insert Ro;
        
        SRS_Service_Request_Charge__c RelatedBillingCharge =  new SRS_Service_Request_Charge__c(Service_Request__c=sereq.id);
        insert RelatedBillingCharge;
        test.stopTest();
        
        List<SRS_SR_RelatedListPaginationController.SerReq> SerReqL = new List<SRS_SR_RelatedListPaginationController.SerReq>();
        SRS_SR_RelatedListPaginationController.SerReq req= new SRS_SR_RelatedListPaginationController.SerReq();
        req.SerReq=sereq;
        req.checked=true;
        req.Full_Address='street';
        req.city='markham';
        req.prov ='NH';
        SerReqL.add(req);
        SerReqL.sort();
        
        ApexPAges.StandardController sc = new ApexPages.StandardController(opp);
        SRS_SR_RelatedListPaginationController src= new SRS_SR_RelatedListPaginationController(sc);
        Test.setCurrentPageReference(new PageReference('Page.SRS_SR_RelatedList')); 
        System.currentPageReference().getParameters().put('Show', 'All');
        src.getMoveItems();
        src.getCloneOptions();
        src.cloneMove = 'No';
        src.isMove = false;
        src.SRId=sereq.id;
        src.noOfClone=1;
        src.cloneType='Clone by Address';
        src.CloneeServiceRequest();
        src.cloneType='Clone by Product';
        src.CloneeServiceRequest();
//        src.getSerReqList();
        src.FoxResult='Service Request Cancelled Successfully';
        src.SendCancelServiceRequestToFox();
    }
    
    static testMethod void SRS_SR_RelatedList_test_5() {
        contact c= new contact(firstname='aditya',lastname='jamwal');
        insert c;
        
        Opportunity opp= new Opportunity(Name='OppTest', StageName='Originated', CloseDate=system.today(),Primary_Order_Contact__c=c.id);
        insert Opp;
        Service_Request__c sereq  = new Service_Request__c(Service_Request_Status__c='completed',opportunity__c=opp.id);
        
        test.startTest();
        insert sereq;
        System.assertEquals('completed',sereq.Service_Request_Status__c);
        
        Schema.DescribeSObjectResult dsr = Schema.SObjectType.SRS_Service_Request_Related_Order__c; 
        Map<String,Schema.RecordTypeInfo> rtMapByName1 = dsr.getRecordTypeInfosByName();
        Schema.RecordTypeInfo rtByName1 =  rtMapByName1.get('Reference Number');
        
        SRS_Service_Request_Related_Order__c Ro= new SRS_Service_Request_Related_Order__c(name='19870',recordtypeid=rtByName1.getRecordtypeid(),System__c='SIMS',Status__c='C',RO_Service_Request__c =sereq.id); 
        
        insert Ro;
        
        SRS_Service_Request_Charge__c RelatedBillingCharge =  new SRS_Service_Request_Charge__c(Service_Request__c=sereq.id);
        insert RelatedBillingCharge;
        test.stopTest();
        
        List<SRS_SR_RelatedListPaginationController.SerReq> SerReqL = new List<SRS_SR_RelatedListPaginationController.SerReq>();
        SRS_SR_RelatedListPaginationController.SerReq req= new SRS_SR_RelatedListPaginationController.SerReq();
        req.SerReq=sereq;
        req.checked=true;
        req.Full_Address='street';
        req.city='markham';
        req.prov ='NH';
        SerReqL.add(req);
        SerReqL.sort();
        
        ApexPAges.StandardController sc = new ApexPages.StandardController(opp);
        SRS_SR_RelatedListPaginationController src= new SRS_SR_RelatedListPaginationController(sc);
        Test.setCurrentPageReference(new PageReference('Page.SRS_SR_RelatedList')); 
        System.currentPageReference().getParameters().put('Show', 'All');
        src.getMoveItems();
        src.getCloneOptions();
        src.cloneMove = 'Yes';
        src.isMove = true;
        src.SRId=sereq.id;
        src.noOfClone=1;
        src.cloneType='Clone by Address';
        src.CloneeServiceRequest();
        src.cloneType='Clone by Product';
        src.CloneeServiceRequest();
//        src.getSerReqList();
        src.FoxResult='Service Request Cancelled Successfully';
        src.SendCancelServiceRequestToFox();
        
        // coverage on pagination methods
        src.getIsPagination();
        src.getPageCount();
        List<SRS_SR_RelatedListPaginationController.SerReq> srPageList = src.getSRPage();
        src.currentPageNumber();
        src.retrieveSerReqCount();
        src.getColumnHeaders();
    }    
}