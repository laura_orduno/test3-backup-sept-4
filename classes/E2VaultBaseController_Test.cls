/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 * This is the test class for E2VaultBaseController  
 */

@isTest(SeeAllData=true)

public class E2VaultBaseController_Test  implements HttpCalloutMock{
  private HttpResponse resp;
  
  public HTTPResponse respond(HTTPRequest req) {
        return resp;
  }
  
  public static testMethod void testE2Vault_RestCall(){   
        Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('BAN').getRecordTypeId();
    
        Account acc = new Account();
        acc.RecordTypeId = devRecordTypeId;
        acc.Name = 'Telus';
        acc.Phone = '1-800-888-9999';
        acc.CAN__c = '2312334';
        Insert acc;
        Test.startTest();
        
        String testBody = '{"billArchivesResponse":{"status":{"statusCd":"200","statusTxt":"OK"},"billArchiveList":[{"locatorId":"wlsprod&#124;10016063&#124;2016/02/09&#124;20160212005504-20160209-15641-2-wls-pcs&#124;00361479000CD6B5","billDt":"2016-02-09-05:00"},' +
                  '{"locatorId":"wlsprod&#124;10016063&#124;2016/01/09&#124;20160111214558-20160109-15281-2-wls-pcs&#124;00371F87000C790E","billDt":"2016-01-09-05:00"}]}}';
        PageReference pageRef = Page.E2VaultDetailPage;
        pageRef.getParameters().put('ACCID', acc.Id);
        Test.setCurrentPage(pageRef);
        HttpCalloutMock mock = new CallToE2VaultTest(testBody,200,'OK');
        Test.setMock(HttpCalloutMock.class, mock);
        E2VaultBaseController obj = new E2VaultBaseController();
        obj.createLog();
        Test.stopTest();
    
  }
  
  public static testMethod void testE2Vault_Rest(){      
        Account acc = new Account();
        acc.Name = 'Telus';
        acc.Phone = '1-800-888-9999';
        acc.CAN__c = '2312334';
        Insert acc;
        Test.startTest();
        
        String testBody = '{"billArchivesResponse":{"status":{"statusCd":"200","statusTxt":"OK"},"billArchiveList":[{"locatorId":"wlsprod&#124;10016063&#124;2016/02/09&#124;20160212005504-20160209-15641-2-wls-pcs&#124;00361479000CD6B5","billDt":"2016-02-09-05:00"},' +
                  '{"locatorId":"wlsprod&#124;10016063&#124;2016/01/09&#124;20160111214558-20160109-15281-2-wls-pcs&#124;00371F87000C790E","billDt":"2016-01-09-05:00"}]}}';
        PageReference pageRef = Page.E2VaultDetailPage;
        pageRef.getParameters().put('ACCID', acc.Id);
        Test.setCurrentPage(pageRef);
        HttpCalloutMock mock = new CallToE2VaultTest(testBody,200,'OK');
        Test.setMock(HttpCalloutMock.class, mock);
        E2VaultBaseController obj = new E2VaultBaseController();
        obj.createLog();
        Test.stopTest();
    
  }
  
  public static testMethod void testE2Vault_Rest1(){      
        Account acc = new Account();
        acc.Name = 'Telus';
        acc.Phone = '1-800-888-9999';
        acc.CAN__c = '2312334';
        acc.Billing_System_ID__c = '119';
        Insert acc;
        Test.startTest();
        
        String testBody = '{"billArchivesResponse":{"status":{"statusCd":"200","statusTxt":"OK"},"billArchiveList":[{"locatorId":"wlsprod&#124;10016063&#124;2016/02/09&#124;20160212005504-20160209-15641-2-wls-pcs&#124;00361479000CD6B5","billDt":"2016-02-09-05:00"},' +
                  '{"locatorId":"wlsprod&#124;10016063&#124;2016/01/09&#124;20160111214558-20160109-15281-2-wls-pcs&#124;00371F87000C790E","billDt":"2016-01-09-05:00"}]}}';
        PageReference pageRef = Page.E2VaultDetailPage;
        pageRef.getParameters().put('ACCID', acc.Id);
        Test.setCurrentPage(pageRef);
        HttpCalloutMock mock = new CallToE2VaultTest(testBody,200,'OK');
        Test.setMock(HttpCalloutMock.class, mock);
        E2VaultBaseController obj = new E2VaultBaseController();
        obj.createLog();
        Test.stopTest();
  }
   
  public static testMethod void testE2VaultBCANTQ_Rest(){      
      Account acc = new Account();
      acc.Name = 'Telus';
      acc.Phone = '1-800-888-9999';
      acc.BCAN__c = '119-0001291';
      acc.CAN__c = '0001291';
      Insert acc;
      Test.startTest();
          
      String testBody = '{"billArchivesResponse":{"status":{"statusCd":"400","statusTxt":"Not OK"}}}';
      HttpCalloutMock mock = new CallToE2VaultTest(testBody,400,'OK');
      Test.setMock(HttpCalloutMock.class, mock);
      Test.setCurrentPageReference(new PageReference('Page.myPage'));

      System.currentPageReference().getParameters().put('ACCID', acc.Id);
      E2VaultBaseController  obj = new E2VaultBaseController();
      obj.selectedBillDate = 'test';
      obj.sendRequestToGetPdfURL();
      Test.stopTest();
  }
  
  public static testMethod void testE2VaultBCANWLS_Rest(){      
      Account acc = new Account();
      acc.Name = 'Telus';
      acc.Phone = '1-800-888-9999';
      acc.BCAN__c = '130-2312334';
      acc.CAN__c = '2312334';
      Insert acc;
      Test.startTest();
          

      Test.setCurrentPageReference(new PageReference('Page.myPage'));

      System.currentPageReference().getParameters().put('ACCID', acc.Id);
      E2VaultBaseController  obj = new E2VaultBaseController();
      obj.selectedBillDate = 'test';
      obj.sendRequestToGetPdfURL();
      Test.stopTest();
    
  }
 
}